--Database table in package 'Viacell'

CREATE TABLE t205m_part_label_parameter
( c205m_part_label_param_id NUMBER(8,2) NOT NULL,
   C205_PART_NUMBER_ID               VARCHAR2(20) NOT NULL,
   c205m_generic_spec                VARCHAR2(4000),
   c205m_size                        VARCHAR2(200),
   c205m_size_format                 VARCHAR2(200),
   c205m_box_label                   VARCHAR2(200),
   c205m_patient_label               VARCHAR2(200),
   c205m_package_label               VARCHAR2(200),
   c205m_sample                      VARCHAR2(1),
   c205m_material_spec               VARCHAR2(200),
   c205m_part_drawing                VARCHAR2(200),
   c901_proc_spec_type               NUMBER(8,2),
   c901_contract_proc_client         NUMBER(8,2),
   c2550_last_updated_by             VARCHAR2(20),
   c2550_last_updated_date           DATE,
   c2550_void_fl                     VARCHAR2(1),
  c1900_company_id                   NUMBER NOT NULL,
  c5040_plant_id                     NUMBER NOT NULL
);
  
ALTER TABLE t205m_part_label_parameter ADD ( PRIMARY KEY (c205m_part_label_param_id));
ALTER TABLE t205m_part_label_parameter ADD ( FOREIGN KEY (C205_PART_NUMBER_ID) REFERENCES t205_part_number(C205_PART_NUMBER_ID));

CREATE SEQUENCE s205m_part_label_parameter START WITH 1 MAXVALUE 999999999999999999999999999 MINVALUE 1 NOCYCLE NOCACHE NOORDER;

 
 ALTER TABLE t2550_part_control_number ADD(
C901_LAST_UPDATED_WAREHOUSE_TYPE NUMBER,
C901_LAST_UPDATED_TRANS_TYPE NUMBER,  
C2550_LAST_UPDATED_TRANS_DATE DATE,
C901_LOT_CONTROLLED_STATUS NUMBER,
C704_ACCOUNT_NM VARCHAR2(255),
C701_DISTRIBUTOR_NAME VARCHAR2(255),
C2550_RESERVED_TRANS_ID VARCHAR2(20),
C901_SHIP_TO_TYPE NUMBER,
C5060_SHIP_TO_ID VARCHAR2(20),
C5060_SHIPPED_DT DATE,
C5060_CUSTOMER_PO VARCHAR2(100),
C2550_ERROR_FL VARCHAR2(1),
C901_REF_TYPE NUMBER,
C2550_REF_ID VARCHAR2(20));

ALTER TABLE t2550_part_control_number ADD (FOREIGN KEY (C2550_RESERVED_TRANS_ID) REFERENCES GLOBUS_APP.t5062_control_number_reserve (C5062_TRANSACTION_ID));
ALTER TABLE t2550_part_control_number ADD (FOREIGN KEY (C901_REF_TYPE) REFERENCES GLOBUS_APP.T901_CODE_LOOKUP (C901_CODE_ID));  
ALTER TABLE t2550_part_control_number ADD (FOREIGN KEY (C901_LAST_UPDATED_TRANS_TYPE) REFERENCES GLOBUS_APP.T901_CODE_LOOKUP (C901_CODE_ID));
ALTER TABLE t2550_part_control_number ADD (FOREIGN KEY (C901_LAST_UPDATED_WAREHOUSE_TYPE) REFERENCES GLOBUS_APP.T901_CODE_LOOKUP (C901_CODE_ID));
ALTER TABLE t2550_part_control_number ADD (FOREIGN KEY (C901_LOT_CONTROLLED_STATUS) REFERENCES GLOBUS_APP.T901_CODE_LOOKUP (C901_CODE_ID));
ALTER TABLE t2550_part_control_number ADD (FOREIGN KEY (C901_SHIP_TO_TYPE) REFERENCES GLOBUS_APP.T901_CODE_LOOKUP (C901_CODE_ID));
