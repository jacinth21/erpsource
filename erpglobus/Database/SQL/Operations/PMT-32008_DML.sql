INSERT INTO t901_code_lookup 
            (c901_code_id,c901_code_nm,c901_code_grp,c901_active_fl,c901_code_seq_no,c902_code_nm_alt,c901_control_type,c901_created_date,c901_created_by,c901_last_updated_date,c901_last_updated_by,c901_void_fl,c901_lock_fl,c901_last_updated_date_utc) 
     VALUES (107600,' ','CNCLT',0,161,null,null,to_date('07-FEB-19','DD-MON-RR'),'2423914',null,null,null,null,null);

UPDATE t901_code_lookup
   SET c902_code_nm_alt = 'RBPALN',c901_active_fl='1',c901_code_nm='Pending Acceptance Rollback',c901_last_updated_by=706328,c901_last_updated_date=sysdate
 WHERE c901_code_id='107600';

DELETE FROM t906_rules WHERE c906_rule_id='107600' AND c906_rule_grp_id='CMNCNCL';

INSERT INTO t906_rules (c906_rule_seq_id,c906_rule_id,c906_rule_desc,c906_rule_value,c906_created_date,c906_rule_grp_id,c906_created_by,c906_last_updated_date,c906_last_updated_by,c901_rule_type,c906_active_fl,c906_void_fl,c1900_company_id,c906_last_updated_date_utc)
     VALUES (S906_RULE.NEXTVAL,'107600','Pending Acceptance Rollback','gm_pkg_op_rfid_process.gm_rollback_pending_acceptance',sysdate,'CMNCNCL','706328',null,null,null,null,null,null,null);

INSERT INTO t901b_code_group_master
            (C901B_CODE_GRP_ID, C901B_CODE_GRP , C901B_CODE_DESCRIPTION ,
             C901B_LOOKUP_NEEDED, C901B_CREATED_BY , C901B_CREATED_DATE)
     VALUES (s901b_code_group_master.NEXTVAL,'RBPALN','Pending Acceptance Rollback','','3058724',CURRENT_DATE);
 

INSERT into T901_CODE_LOOKUP
            (C901_CODE_ID,C901_CODE_NM,
             C901_CODE_GRP,C901_ACTIVE_FL,
             C901_CODE_SEQ_NO,C902_CODE_NM_ALT,
             C901_CONTROL_TYPE,C901_CREATED_DATE,
             C901_CREATED_BY,C901_LAST_UPDATED_DATE,
             C901_LAST_UPDATED_BY,C901_VOID_FL,
             C901_LOCK_FL,C901_LAST_UPDATED_DATE_UTC) 
     values (107940,'Incorrect Distributor',
              'RBPALN','1',
              '1',null,
              null,SYSDATE,
              '3058724',null,
              null,null,
              null,null);


INSERT into T901_CODE_LOOKUP
            (C901_CODE_ID,C901_CODE_NM,
             C901_CODE_GRP,C901_ACTIVE_FL,
             C901_CODE_SEQ_NO,C902_CODE_NM_ALT,
             C901_CONTROL_TYPE,C901_CREATED_DATE,
             C901_CREATED_BY,C901_LAST_UPDATED_DATE,
             C901_LAST_UPDATED_BY,C901_VOID_FL,
             C901_LOCK_FL,C901_LAST_UPDATED_DATE_UTC) 
     values (107941,'Incorrect Set',
              'RBPALN','1',
              '2',null,
              null,SYSDATE,
              '3058724',null,
              null,null,
              null,null);

INSERT into T901_CODE_LOOKUP
            (C901_CODE_ID,C901_CODE_NM,
             C901_CODE_GRP,C901_ACTIVE_FL,
             C901_CODE_SEQ_NO,C902_CODE_NM_ALT,
             C901_CONTROL_TYPE,C901_CREATED_DATE,
             C901_CREATED_BY,C901_LAST_UPDATED_DATE,
             C901_LAST_UPDATED_BY,C901_VOID_FL,
             C901_LOCK_FL,C901_LAST_UPDATED_DATE_UTC) 
     values (107942,'Input Error',
              'RBPALN','1',
              '3',null,
              null,SYSDATE,
              '3058724',null,
              null,null,
              null,null);









