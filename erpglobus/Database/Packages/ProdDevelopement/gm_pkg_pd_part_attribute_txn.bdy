/* Formatted on 2019/04/12 09:56 (Formatter Plus v4.8.0) */
-- @"C:\PMT\BITBUCK\erpglobus\Database\Packages\ProdDevelopement\gm_pkg_pd_part_attribute_txn.bdy"

CREATE OR REPLACE PACKAGE BODY gm_pkg_pd_part_attribute_txn
IS
/*******************************************************
* Description : Procedure to use save the label param values
* PMT-29680
* Author   : RAJA
*******************************************************/
PROCEDURE gm_sav_part_params (
      p_num_id	               IN	T205M_PART_LABEL_PARAMETER.C205_PART_NUMBER_ID%TYPE
     , p_general_spec          IN   T205M_PART_LABEL_PARAMETER.c205m_generic_spec%TYPE
     , p_size                  IN   T205M_PART_LABEL_PARAMETER.c205m_size %TYPE
     , p_size_format           IN   T205M_PART_LABEL_PARAMETER.c205m_size %TYPE
     , p_box_label             IN   T205M_PART_LABEL_PARAMETER.c205m_box_label%TYPE             
     , p_patient_label         IN   T205M_PART_LABEL_PARAMETER.c205m_patient_label%TYPE  
     , p_package_label         IN   T205M_PART_LABEL_PARAMETER.c205m_package_label%TYPE  
     , p_sample                IN   T205M_PART_LABEL_PARAMETER.c205m_sample%TYPE
     , p_material_spec         IN   T205M_PART_LABEL_PARAMETER.c205m_material_spec%TYPE 
     , p_part_drawing          IN   T205M_PART_LABEL_PARAMETER.c205m_part_drawing%TYPE  
     , p_contract_proc_client  IN   T205M_PART_LABEL_PARAMETER.c901_contract_proc_client%TYPE 
     , p_proc_spec_type        IN   T205M_PART_LABEL_PARAMETER.c901_proc_spec_type%TYPE     
     , p_user_id	           IN	T205M_PART_LABEL_PARAMETER.C2550_LAST_UPDATED_BY%TYPE
     , p_attr_id               OUT   t205d_part_attribute.c2051_part_attribute_id%TYPE
  )
  AS 
   v_company_id             T205M_PART_LABEL_PARAMETER.C1900_COMPANY_ID%TYPE;    
   v_plant_id               T205M_PART_LABEL_PARAMETER.C5040_PLANT_ID%TYPE;
  BEGIN 
	
      SELECT s205m_part_label_parameter.NEXTVAL
        INTO p_attr_id
        FROM DUAL;
        
    -- to get the company id and plant id
      SELECT get_compid_frm_cntx (), get_plantid_frm_cntx ()
       	INTO v_company_id, v_plant_id
       	FROM dual;
		
		    INSERT INTO T205M_PART_LABEL_PARAMETER 
		                (c205m_part_label_param_id, 
		                c205_part_number_id,
		                c205m_generic_spec, 
		                c205m_size,
		                c205m_size_format,
		                c205m_box_label,
                        c205m_patient_label,
                        c205m_package_label,
                        c205m_sample,
                        c205m_material_spec,
                        c205m_part_drawing,
                        c901_proc_spec_type,
                        c901_contract_proc_client,
		                c2550_last_updated_date, 
		                c2550_last_updated_by,
		                c2550_void_fl,
                        c1900_company_id,
                        c5040_plant_id)             
                 VALUES (p_attr_id,
                         p_num_id,
                         p_general_spec,
                         p_size,
                         p_size_format,
                         p_box_label,
                         p_patient_label,
                         p_package_label,
                         p_sample,
                         p_material_spec,
                         p_part_drawing,
                         p_proc_spec_type,
                         p_contract_proc_client,                        
                         CURRENT_DATE,
                         p_user_id,
                         NULL,
                         v_company_id,
                         v_plant_id); 
          
END gm_sav_part_params;
END gm_pkg_pd_part_attribute_txn;
/
