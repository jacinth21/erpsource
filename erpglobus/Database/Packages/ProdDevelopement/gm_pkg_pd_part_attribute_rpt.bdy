/* Formatted on 2019/04/12 09:56 (Formatter Plus v4.8.0) */
-- @"C:\PMT\BITBUCK\erpglobus\Database\Packages\ProdDevelopement\gm_pkg_pd_part_attribute_rpt.bdy"

CREATE OR REPLACE PACKAGE BODY gm_pkg_pd_part_attribute_rpt
IS
/*******************************************************
* Description : Procedure to use fetch the label param values
* PMT-29680
* Author   : RAJA
*******************************************************/
PROCEDURE gm_fch_part_lbl_param (
        p_partnumber IN T205M_PART_LABEL_PARAMETER.C205_PART_NUMBER_ID%TYPE,
        p_report OUT TYPES.cursor_type)
AS
 v_company_id             T205M_PART_LABEL_PARAMETER.C1900_COMPANY_ID%TYPE;  
 v_plant_id               T205M_PART_LABEL_PARAMETER.C5040_PLANT_ID%TYPE;
 BEGIN
	 -- to get the company id and plant id
      SELECT get_compid_frm_cntx (), get_plantid_frm_cntx ()
       	INTO v_company_id, v_plant_id
       	FROM dual;
	 
    OPEN p_report 
    FOR  
    SELECT t205m.c205m_generic_spec GENERALSPEC,
           t205m.c205m_size LBLSIZE,
           t205m.c205m_size_format SIZEFMT,
           t205m.c205m_box_label BOXLABEL,
           t205m.c205m_patient_label PRINTLABEL,
           t205m.c205m_package_label PKGLABEL,
           t205m.c205m_sample SAMPLE,
           t205m.c205m_material_spec MATERIALSPECLABEL,
           t205m.c205m_part_drawing PARTDRAWLABEL, 
           t205m.c901_proc_spec_type PROSPECTYP,
           t205m.c901_contract_proc_client CPCID,
           t205m.c2550_last_updated_by UPDATEBY,
           t205m.c2550_last_updated_date UPDATEDATE,
           t205m.c2550_void_fl VOIDFL,
           t205m.c1900_company_id COMID,
           t205m.c5040_plant_id PLID
      FROM T205M_PART_LABEL_PARAMETER t205m 
     WHERE t205m.C205_PART_NUMBER_ID = p_partnumber
       AND t205m.c2550_void_fl IS NULL
       AND t205m.c1900_company_id=v_company_id
       AND t205m.c5040_plant_id=v_plant_id;
 END gm_fch_part_lbl_param;
END gm_pkg_pd_part_attribute_rpt;
/
