package com.globus.operations.inventory.actions;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.custservice.beans.GmSalesBean;
import com.globus.operations.beans.GmLotCodeRptBean;
import com.globus.operations.inventory.beans.GmInvLocationBean;
import com.globus.operations.inventory.forms.GmInvLocationForm;
import com.globus.operations.inventory.forms.GmLotTrackReportForm;

public class GmInvLocationAction extends GmDispatchAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());// Code

  /**
   * addInvLocation
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward  
   */
  public ActionForward addInvLocation(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    // Form Intitalization
    GmInvLocationForm gmInvLocationForm = (GmInvLocationForm) actionForm;
    // Bean Initalization
    GmInvLocationBean gmInvLocationBean = new GmInvLocationBean(getGmDataStoreVO());
    gmInvLocationForm.loadSessionParameters(request);
    HashMap hmParams = new HashMap();
    ArrayList alZones = new ArrayList();
    ArrayList alInvLocTypes = new ArrayList();
    ArrayList alWareHouse = new ArrayList();
    String strOpt = "";
    String strHAction = "";
    String strDeptId = "";
    String strWHRType = "";
    String strCodeGrp = "LOCWT";
    GmCommonClass gmCommonClass = new GmCommonClass();
    // Get Hidden Parameter's
    strOpt = gmCommonClass.parseNull(gmInvLocationForm.getStrOpt());
    strHAction = gmCommonClass.parseNull(gmInvLocationForm.getHaction());
    strDeptId = gmCommonClass.parseNull(gmInvLocationForm.getDeptId());
    alZones = gmCommonClass.getCodeList("LOCZT");
    alInvLocTypes = gmCommonClass.getCodeList("LOCTP");
    gmInvLocationForm.setAlZones(alZones);
    gmInvLocationForm.setAlInvLocTypes(alInvLocTypes);
    hmParams.put("TXNTYPE", strWHRType);
    hmParams.put("CODEGRP", strCodeGrp);
    hmParams.put("DEPTID", strDeptId);
    alWareHouse = GmCommonClass.parseNullArrayList(gmInvLocationBean.fetchWareHouseDtl(hmParams));
    gmInvLocationForm.setAlWareHouse(alWareHouse);

    // gmInvLocationForm.setUserId((String)request.getSession().getAttribute("strSessTodaysDate"));
    hmParams = GmCommonClass.getHashMapFromForm(gmInvLocationForm);
    if (strOpt.equals("save")) {
      String strType = GmCommonClass.parseNull((String) hmParams.get("TYPENUM"));
      String strStatus = "93202"; // default Status - Initiated
      // get the Rule Location type -based on the selected type.
      String strRuleTypeVal =
          GmCommonClass.parseNull(GmCommonClass.getRuleValue(strType, "LOC_TYPE"));
      // rule value is SET then, set the status as active and LocationMap is SetMap.
      if (strRuleTypeVal.equals("SET")) {
        gmInvLocationForm.setLocMap("SetMap"); // After fetch to re-direct to Set report screen.
        strStatus = "93310"; // Active
      }
      hmParams.put("STRSTATUS", strStatus);
      gmInvLocationBean.saveInvLocation(hmParams);
      gmInvLocationForm.setStatusNum(strStatus);
      gmInvLocationForm.setTypeNum(GmCommonClass.parseZero(strType));
      gmInvLocationForm.setUserFilter("YES");
      return actionMapping.findForward("Fetch");
    }

    return actionMapping.findForward("Add");
  }

  /**
   * fetchInvLocationByID
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   */
  public ActionForward fetchInvLocationByID(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    // Form Intitalization
    GmInvLocationForm gmInvLocationForm = (GmInvLocationForm) actionForm;
    // Bean Initalization
    GmInvLocationBean gmInvLocationBean = new GmInvLocationBean(getGmDataStoreVO());

    gmInvLocationForm.loadSessionParameters(request);

    GmCommonClass gmCommonClass = new GmCommonClass();
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());

    HashMap hmParams = new HashMap();
    HashMap hmTemplateParam = new HashMap();
    HashMap hmAccess = new HashMap();
    HashMap hmApplnParam = new HashMap();

    ArrayList alInvLocList = new ArrayList();
    ArrayList alStatus = new ArrayList();
    ArrayList alInvLocTypes = new ArrayList();
    ArrayList alParts = new ArrayList();
    ArrayList alWareHouse = new ArrayList();
    HashMap hmLocationDetails = new HashMap();
    String strLocationID =
        (request.getParameter("locationId")) == null ? gmInvLocationForm.getLocationID() : request
            .getParameter("locationId");
    String strWareHouseId =
        (request.getParameter("wid")) == null ? gmInvLocationForm.getWarehouseid() : request
            .getParameter("wid");
    String strOpt = "";
    String strHAction = "";
    String strWHRType = "";
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));



    // Get Hidden Parameter's
    strOpt = gmCommonClass.parseNull(gmInvLocationForm.getStrOpt());
    strHAction = gmCommonClass.parseNull(gmInvLocationForm.getHaction());

    alInvLocTypes = gmCommonClass.getCodeList("LOCTP");
    alStatus = gmCommonClass.getCodeList("LOCSS");
    alWareHouse = GmCommonClass.parseNullArrayList(gmInvLocationBean.fetchWareHouse(strWHRType));

    gmInvLocationForm.setAlInvLocTypes(alInvLocTypes);
    gmInvLocationForm.setAlStatus(alStatus);
    gmInvLocationForm.setAlWareHouse(alWareHouse);

    hmParams = GmCommonClass.getHashMapFromForm(gmInvLocationForm);
    /* Fetch Location Part mapping */
    alInvLocList =
        GmCommonClass.parseNullArrayList(gmInvLocationBean.fetchInvLocationPartMappingByID(
            strLocationID, "", strWareHouseId));
    gmInvLocationForm.setAlInvLocList(alInvLocList);

    /* Fetch Location Details */
    hmLocationDetails =
        gmInvLocationBean.fetchInvLocationDetailsById(strLocationID, strWareHouseId);

    gmInvLocationForm.setLocationID(strLocationID);

    gmInvLocationForm.setWarehouseid(strWareHouseId);
    gmInvLocationForm.setTypeNum(GmCommonClass.parseNull((String) hmLocationDetails
        .get("LOCATION_TYPE")));
    gmInvLocationForm
        .setStatusNum(GmCommonClass.parseNull((String) hmLocationDetails.get("STATUS")));
    gmInvLocationForm.setHlocationId(GmCommonClass.parseNull((String) hmLocationDetails.get("ID")));
    // gmInvLocationForm.setActiveFlag(GmCommonClass.parseNull((String)
    // hmLocationDetails.get("ACTIVE_FLAG")));
    /* --- */
    gmInvLocationForm.setRuleType(GmCommonClass.parseNull((String) hmLocationDetails
        .get("RULETYPE")));
    gmInvLocationForm.setStrOpt("EDIT");
    hmParams = GmCommonClass.getHashMapFromForm(gmInvLocationForm);
    gmInvLocationForm =
        (GmInvLocationForm) GmCommonClass.getFormFromHashMap(gmInvLocationForm, hmParams);
    /* --- */

    String strUserID = GmCommonClass.parseNull((String) hmParams.get("USERID"));
    // GmCommonClass.parseNull((String)request.getSession().getAttribute("strSessUserId"));
    hmAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserID,
            "3250007"));
    String strAccessFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    hmTemplateParam.put("ACCESSFLAG", strAccessFl);
    gmInvLocationForm.setAccessFlag(strAccessFl);

    // Get XML Grid Data
    hmTemplateParam.put("STRSESSCOMPANYLOCALE ", strSessCompanyLocale);
    hmTemplateParam.put("VMFILEPATH", "properties.labels.operations.inventory.GmInvLocationEdit");
    hmTemplateParam.put("TEMPLATE", "GmInventoryLocationEdit.vm");
    hmTemplateParam.put("TEMPLATEPATH", "operations/inventory/templates");
    hmTemplateParam.put("ALINVLOCLIST", alInvLocList);
    hmTemplateParam.put("PARTS", alParts);
    // MNTTASK - 2760 Location Part Mapping Curr Qty Filter for Bulk Location
    hmTemplateParam.put("STRCURRENTQTYFLAG", gmInvLocationForm.getChkCurrQtyFlag());
    hmTemplateParam.put("STRLOCATIONTYPE", gmInvLocationForm.getTypeNum());
    hmTemplateParam.put("STRSTATUS", gmInvLocationForm.getStatusNum());
    // XML String
    String strXmlString = gmInvLocationBean.getXmlGridData(hmTemplateParam);
    gmInvLocationForm.setGridXmlData(strXmlString);
    return actionMapping.findForward("Edit");

  }

  /**
   * saveInvLocationPartByID
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   */
  public ActionForward saveInvLocationPartByID(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    // Form Intitalization
    GmInvLocationForm gmInvLocationForm = (GmInvLocationForm) actionForm;
    // Bean Initalization
    GmInvLocationBean gmInvLocationBean = new GmInvLocationBean(getGmDataStoreVO());

    gmInvLocationForm.loadSessionParameters(request);

    GmCommonClass gmCommonClass = new GmCommonClass();

    HashMap hmParams = new HashMap();

    hmParams = GmCommonClass.getHashMapFromForm(gmInvLocationForm);

    gmInvLocationBean.saveInvLocationPartDetailById(hmParams);

    return actionMapping.findForward("FetchByID");

  }

  /**
   * fetchInvLocationPartMapping
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   */
  public ActionForward fetchInvLocationPartMapping(ActionMapping actionMapping,
      ActionForm actionForm, HttpServletRequest request, HttpServletResponse response)
      throws AppError, Exception {
    instantiate(request, response);
    // Form Intitalization
    GmInvLocationForm gmInvLocationForm = (GmInvLocationForm) actionForm;
    // Bean Initalization
    GmInvLocationBean gmInvLocationBean = new GmInvLocationBean(getGmDataStoreVO());

    gmInvLocationForm.loadSessionParameters(request);

    HashMap hmParams = new HashMap();
    ArrayList alInvLocList = new ArrayList();
    ArrayList alStatus = new ArrayList();
    ArrayList alInvLocTypes = new ArrayList();
    ArrayList alZones = new ArrayList();
    ArrayList alWareHouse = new ArrayList();
    String strOpt = "";
    String strHAction = "";
    String strForward = "";
    String strLocationMap = "";
    GmCommonClass gmCommonClass = new GmCommonClass();

    // Get Hidden Parameter's
    strOpt = gmCommonClass.parseNull(gmInvLocationForm.getStrOpt());
    strHAction = gmCommonClass.parseNull(gmInvLocationForm.getHaction());
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

    alInvLocTypes = gmCommonClass.getCodeList("LOCTP");
    alStatus = gmCommonClass.getCodeList("LOCSS");
    alZones = gmCommonClass.getCodeList("LOCZT");

    gmInvLocationForm.setAlZones(alZones);
    gmInvLocationForm.setAlInvLocTypes(alInvLocTypes);
    gmInvLocationForm.setAlStatus(alStatus);
    String strWHRType = "";
    alWareHouse = GmCommonClass.parseNullArrayList(gmInvLocationBean.fetchWareHouse(strWHRType));
    gmInvLocationForm.setAlWareHouse(alWareHouse);

    hmParams = GmCommonClass.getHashMapFromForm(gmInvLocationForm);
    strLocationMap = GmCommonClass.parseNull((String) hmParams.get("LOCMAP"));
    HashMap hmTemplateParams = new HashMap();
    if (strLocationMap.equals("SetMap")) {
      hmTemplateParams.put("SETMAP", "Y");
      strForward = "SetReport";
    } else {
      strForward = "PartReport";
    }
    if (strOpt.equals("load") || strOpt.equals("save") || strOpt.equals("view")) {
      alInvLocList =
          GmCommonClass.parseNullArrayList(gmInvLocationBean.fetchInvLocationPartMapping(hmParams));
      gmInvLocationForm.setAlInvLocList(alInvLocList);

      // Get XML Grid Data
      hmTemplateParams.put("STRSESSCOMPANYLOCALE ", strSessCompanyLocale);
      hmTemplateParams.put("VMFILEPATH",
          "properties.labels.operations.inventory.GmInvLocationPartMappingReport");
      hmTemplateParams.put("TEMPLATE", "GmInventoryLocationPartMappingReport.vm");
      hmTemplateParams.put("TEMPLATEPATH", "operations/inventory/templates");
      hmTemplateParams.put("ALINVLOCLIST", alInvLocList);
      hmTemplateParams.put("INVLOCTYPES", alInvLocTypes);
      hmTemplateParams.put("STATUS", alStatus);
      hmTemplateParams.put("STROPT", strOpt);
      // MNTTASK - 2760 Location Part Mapping Curr Qty Filter for Bulk Location
      hmTemplateParams.put("STRLOCATIONTYPE", gmInvLocationForm.getTypeNum());
      hmTemplateParams.put("STRSTATUS", gmInvLocationForm.getStatusNum());
      hmTemplateParams.put("STRCURRENTQTYFLAG", gmInvLocationForm.getChkCurrQtyFlag());

      // XML String
      String strXmlString = gmInvLocationBean.getXmlGridData(hmTemplateParams);
      gmInvLocationForm.setGridXmlData(strXmlString);
    }
    return actionMapping.findForward(strForward);
  }

  /**
   * saveInvLocationPartMapping
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   */
  public ActionForward saveInvLocationPartMapping(ActionMapping actionMapping,
      ActionForm actionForm, HttpServletRequest request, HttpServletResponse response)
      throws AppError, Exception {
    instantiate(request, response);
    // Form Intitalization
    GmInvLocationForm gmInvLocationForm = (GmInvLocationForm) actionForm;
    // Bean Initalization
    GmInvLocationBean gmInvLocationBean = new GmInvLocationBean(getGmDataStoreVO());

    gmInvLocationForm.loadSessionParameters(request);

    HashMap hmParams = new HashMap();

    GmCommonClass gmCommonClass = new GmCommonClass();

    hmParams = GmCommonClass.getHashMapFromForm(gmInvLocationForm);

    gmInvLocationBean.saveInvLocationPartDetails(hmParams);

    return actionMapping.findForward("Fetch");
  }

  /**
   * printInvLocation
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   */
  public ActionForward printInvLocation(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    // Form Intitalization
    GmInvLocationForm gmInvLocationForm = (GmInvLocationForm) actionForm;
    // Bean Initalization
    GmInvLocationBean gmInvLocationBean = new GmInvLocationBean(getGmDataStoreVO());

    gmInvLocationForm.loadSessionParameters(request);

    HashMap hmParams = new HashMap();
    ArrayList alInvLocList = new ArrayList();

    GmCommonClass gmCommonClass = new GmCommonClass();

    hmParams = GmCommonClass.getHashMapFromForm(gmInvLocationForm);
    hmParams.put("PRINTLOC", "PRINTLOC");
    alInvLocList =
        GmCommonClass.parseNullArrayList(gmInvLocationBean.fetchInvLocationPartMapping(hmParams));
    gmInvLocationForm.setAlInvLocList(alInvLocList);

    return actionMapping.findForward("Print");
  }

  /**
   * getPartNumDesc
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   */
  public ActionForward getPartNumDesc(ActionMapping actionMapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    // Form Intitalization
    GmInvLocationForm gmInvLocationForm = (GmInvLocationForm) form;
    // Bean Initalization
    GmInvLocationBean gmInvLocationBean = new GmInvLocationBean(getGmDataStoreVO());

    gmInvLocationForm.loadSessionParameters(request);

    String strPartNumDesc = gmInvLocationBean.getPartNumDesc(request.getParameter("partNum"));
    response.setContentType("text/plain");
    PrintWriter pw = response.getWriter();

    if (strPartNumDesc == null)
      pw.write("Fail");
    else
      pw.write(strPartNumDesc);
    pw.flush();
    return null;
  }

  /**
   * fetchPartReserveDetails: This method is used to fetch the Part Reserved Details for Ack Order
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   * @throws IOException
   * @throws ServletException
   */
  public ActionForward fetchPartReserveDetails(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmLotTrackReportForm gmLotTrackReportForm = (GmLotTrackReportForm) actionForm;
    gmLotTrackReportForm.loadSessionParameters(request);
    GmInvLocationBean gmInvLocationBean = new GmInvLocationBean(getGmDataStoreVO());
    HashMap hmParam = new HashMap();
    HashMap hmTemplateParams = new HashMap();
    ArrayList alResult = new ArrayList();
    ArrayList alExpDateRange = new ArrayList();
    ArrayList alWareHouse = new ArrayList();
    ArrayList alRemoveCodeIDs = new ArrayList();
    String strOpt = "";
    String strReleaseQty = "";
    String strShelfQty = "";
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
    hmParam = GmCommonClass.getHashMapFromForm(gmLotTrackReportForm);
    strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    if (strOpt.equals("ReservePart")) {
      gmInvLocationBean.savePartReserve(hmParam);
      strReleaseQty = GmCommonClass.parseNull((String) hmParam.get("RELEASEQTY"));
      strShelfQty = GmCommonClass.parseNull((String) hmParam.get("SHELFQTY"));
      strOpt = "ReLoad";
      gmLotTrackReportForm.setReleaseQty(strReleaseQty);
      gmLotTrackReportForm.setShelfQty(strShelfQty);
    }

    if (strOpt.equals("ReLoad")) {// Should fetch the details only if the Load button is pressed
      alResult =
          GmCommonClass.parseNullArrayList(gmInvLocationBean.fetchPartReserveDetails(hmParam));
      hmTemplateParams.put("TEMPLATE", "GmPartReserveReport.vm");
      hmTemplateParams.put("TEMPLATEPATH", "operations/inventory/templates");
      hmTemplateParams.put("alResult", alResult);
      hmTemplateParams.put("LABLESPATH", "properties.labels.custservice.GmOrderItemControl");
      hmTemplateParams.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);

      String strXmlString = getXmlGridData(hmTemplateParams);
      gmLotTrackReportForm.setGridXmlData(strXmlString);
    }
    return actionMapping.findForward("GmPartReserveReport");

  }

  /**
   * fetchReservedLotsDetails: This method is used to fetch the Reserved Lots details
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   */
  public ActionForward fetchReservedLotsDetails(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmLotTrackReportForm gmLotTrackReportForm = (GmLotTrackReportForm) actionForm;
    gmLotTrackReportForm.loadSessionParameters(request);
    GmInvLocationBean gmInvLocationBean = new GmInvLocationBean(getGmDataStoreVO());
    HashMap hmParam = new HashMap();
    HashMap hmTemplateParams = new HashMap();
    ArrayList alResult = new ArrayList();
    ArrayList alExpDateRange = new ArrayList();
    ArrayList alWareHouse = new ArrayList();
    ArrayList alRemoveCodeIDs = new ArrayList();
    String strOpt = "";
    String strPartNum = "";
    String strPartNumFormat = "";
    hmParam = GmCommonClass.getHashMapFromForm(gmLotTrackReportForm);
    strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
    alExpDateRange = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("CPRSN"));

    alRemoveCodeIDs.add("90190"); // to remove ">" from "Expiry date" drop down
    alRemoveCodeIDs.add("90193"); // to remove ">=" from "Expiry date" drop down
    alExpDateRange =
        GmCommonClass.parseNullArrayList(GmCommonClass.toRemoveCodeIdsFromArrayList(
            alRemoveCodeIDs, alExpDateRange));

    // Get the warehouse dropdown list
    alWareHouse = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("PTTXY"));

    gmLotTrackReportForm.setAlWareHouse(alWareHouse);
    gmLotTrackReportForm.setAlExpDateRange(alExpDateRange);

    if (strOpt.equals("ReLoad")) {// Should fetch the details only if the Load button is pressed
      String strSearch = GmCommonClass.parseNull(request.getParameter("Cbo_Search"));
      strPartNum = GmCommonClass.parseNull(gmLotTrackReportForm.getPartNum());
      strPartNumFormat = strPartNum;

      if (strSearch.equals("LIT")) {
        strPartNumFormat = ("^").concat(strPartNumFormat);
        strPartNumFormat = strPartNumFormat.replaceAll(",", "\\$|^");
        strPartNumFormat = strPartNumFormat.concat("$");
      } else if (strSearch.equals("LIKEPRE")) {
        strPartNumFormat = ("^").concat(strPartNumFormat);
        strPartNumFormat = strPartNumFormat.replaceAll(",", "|^");
      } else if (strSearch.equals("LIKESUF")) {
        strPartNumFormat = strPartNumFormat.replaceAll(",", "\\$|");
        strPartNumFormat = strPartNumFormat.concat("$");
      } else if (strSearch.equals("")) {
        strPartNumFormat = strPartNumFormat;
      } else {
        strPartNumFormat = strPartNumFormat.replaceAll(",", "|");
      }
      hmParam.put("PARTNUM", strPartNumFormat);
      alResult =
          GmCommonClass.parseNullArrayList(gmInvLocationBean.fetchReservedLotsDetails(hmParam));
      hmTemplateParams.put("STRSESSCOMPANYLOCALE ", strSessCompanyLocale);
      hmTemplateParams.put("LABLESPATH", "properties.labels.operations.receiving.GmReservedLots");
      hmTemplateParams.put("TEMPLATE", "GmReservedLots.vm");
      hmTemplateParams.put("TEMPLATEPATH", "operations/receiving/templates");
      hmTemplateParams.put("alResult", alResult);

      // XML String
      String strXmlString = getXmlGridData(hmTemplateParams);
      gmLotTrackReportForm.setGridXmlData(strXmlString);
      request.setAttribute("hSearch", strSearch);
    }
    return actionMapping.findForward("GmReservedLots");

  }

  /**
   * generateOutPut: For preparing string for vm file
   * 
   * @exception AppError
   */
  public String getXmlGridData(HashMap hmParam) throws AppError {
    String strTemplateName = GmCommonClass.parseNull((String) hmParam.get("TEMPLATE"));
    String strTemplatePath = GmCommonClass.parseNull((String) hmParam.get("TEMPLATEPATH"));
    String strSesscompanyLocale =
        GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
    ArrayList alResult = new ArrayList();
    alResult = GmCommonClass.parseNullArrayList((ArrayList) hmParam.get("alResult"));
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setDataList("alResult", alResult);
    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setTemplateSubDir(strTemplatePath);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        GmCommonClass.parseNull((String) hmParam.get("LABLESPATH")), strSesscompanyLocale));
    templateUtil.setTemplateName(strTemplateName);
    return templateUtil.generateOutput();
  }

  /**
   * fetchLotTrackingDetails: This method is used to fetch the Lot tracking details
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   */
  public ActionForward fetchLotTrackingDetails(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {

	    log.debug("Load the method fetchLotTrackingDetails>");
	    instantiate(request, response);
	    GmLotTrackReportForm gmLotTrackReportForm = (GmLotTrackReportForm) actionForm;
	    gmLotTrackReportForm.loadSessionParameters(request);
	    GmLotCodeRptBean gmLotCodeRptBean = new GmLotCodeRptBean(getGmDataStoreVO());
	    GmCustomerBean gmCustomerBean = new GmCustomerBean(getGmDataStoreVO());
	    GmSalesBean gmSales = new GmSalesBean(getGmDataStoreVO());
	    HashMap hmParam = new HashMap();
	    HashMap hmTemplateParams = new HashMap();
	    ArrayList alResult = new ArrayList();
	    ArrayList alExpDateRange = new ArrayList();
	    ArrayList alWareHouse = new ArrayList();
	    ArrayList alRemoveCodeIDs = new ArrayList();
	    ArrayList alStatus = new ArrayList();
	    ArrayList alFieldSales = new ArrayList();
	    ArrayList alAccounts = new ArrayList();
	    String strOpt = "";
	    String strPartNum = "";
	    String strPartNumFormat = "";
	    String strStatus = "";
	    hmParam = GmCommonClass.getHashMapFromForm(gmLotTrackReportForm);
	    strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
	    String strSessCompanyLocale = GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

	    alExpDateRange = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("CPRSN"));
	
	    alRemoveCodeIDs.add("90190"); // to remove ">" from "Expiry date" drop down
	    alRemoveCodeIDs.add("90193"); // to remove ">=" from "Expiry date" drop down
	    alExpDateRange = GmCommonClass.parseNullArrayList(GmCommonClass.toRemoveCodeIdsFromArrayList(alRemoveCodeIDs, alExpDateRange));
	    alStatus = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("LOTTYP"));// For status // dropdown
	    alFieldSales = GmCommonClass.parseNullArrayList(gmCustomerBean.getDistributorList("Active"));
	    alAccounts = GmCommonClass.parseNullArrayList(gmSales.reportAccount());
	    strStatus = GmCommonClass.parseNull((String) hmParam.get("LOTTYPE"));

	    if (strStatus.equals("")) {// Dropdown value should be unreserved by default
	      gmLotTrackReportForm.setLotType("104006"); // 104006:Un-Reserved
	      hmParam.put("LOTTYPE", "104006");
	    }

	    // Get the warehouse dropdown list
	    alWareHouse = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("PTTXY"));
	    gmLotTrackReportForm.setAlWareHouse(alWareHouse);
	    gmLotTrackReportForm.setAlExpDateRange(alExpDateRange);
	    gmLotTrackReportForm.setAlStatus(alStatus);
	    gmLotTrackReportForm.setAlAccounts(alAccounts);
	    gmLotTrackReportForm.setAlFieldSales(alFieldSales);
	    /*
	     * if(strOpt.equals("")){ gmLotTrackReportForm.setExpDateRange("90194");//By default the
	     * operator in Expiry date dropdown should be "<=" (90194)
	     * gmLotTrackReportForm.setExpDate((String
	     * )request.getSession().getAttribute("strSessTodaysDate"));//By default the Expiry date should
	     * be the current date }
	     */
	
	    // Commenting the below condition to load the unreserved reports from the left link
	    // if(strOpt.equals("ReLoad")){// Should fetch the details only if the Load button is pressed
	    String strSearch = GmCommonClass.parseNull(request.getParameter("Cbo_Search"));
	    strPartNum = GmCommonClass.parseNull(gmLotTrackReportForm.getPartNum());
	    strPartNumFormat = strPartNum;
	
	    if (strSearch.equals("LIT")) {
	      strPartNumFormat = ("^").concat(strPartNumFormat);
	      strPartNumFormat = strPartNumFormat.replaceAll(",", "\\$|^");
	      strPartNumFormat = strPartNumFormat.concat("$");
	    } else if (strSearch.equals("LIKEPRE")) {
	      strPartNumFormat = ("^").concat(strPartNumFormat);
	      strPartNumFormat = strPartNumFormat.replaceAll(",", "|^");
	    } else if (strSearch.equals("LIKESUF")) {
	      strPartNumFormat = strPartNumFormat.replaceAll(",", "\\$|");
	      strPartNumFormat = strPartNumFormat.concat("$");
	    } else if (strSearch.equals("")) {
	      strPartNumFormat = strPartNumFormat;
	    } else {
	      strPartNumFormat = strPartNumFormat.replaceAll(",", "|");
	    }
	    hmParam.put("PARTNUM", strPartNumFormat);
	    if (strOpt.equals("ReLoad")) {
	     alResult = GmCommonClass.parseNullArrayList(gmLotCodeRptBean.fetchLotTrackingDetails(hmParam));
	    }
	    hmTemplateParams.put("STRSESSCOMPANYLOCALE ", strSessCompanyLocale);
	    hmTemplateParams.put("LABLESPATH", "properties.labels.operations.inventory.GmLotTrackReport");
	    hmTemplateParams.put("TEMPLATE", "GmLotTrackReport.vm");
	    hmTemplateParams.put("TEMPLATEPATH", "operations/inventory/templates");
	    hmTemplateParams.put("alResult", alResult);
	    String wareHouseTypes = GmCommonClass.parseNull(GmCommonClass.getRuleValue("Lot_Track_Report_Shipping","Lot_Track_Report"));
	    boolean showShipDetsFlg = false;
	    if(wareHouseTypes != "" && gmLotTrackReportForm.getWarehouse() != "" ){
	    	showShipDetsFlg = wareHouseTypes.indexOf(gmLotTrackReportForm.getWarehouse()) != -1 ? true :false;
	    }
	    String strwarehouseTyp = "";
	    strwarehouseTyp = GmCommonClass.parseNull(gmLotTrackReportForm.getWarehouse());
	    hmTemplateParams.put("SHOWSHIPDETSLG", showShipDetsFlg);
	    hmTemplateParams.put("WAREHOUSETYPE", strwarehouseTyp);
	    // XML String
	    String strXmlString = getXmlGridData(hmTemplateParams);
	    gmLotTrackReportForm.setGridXmlData(strXmlString);
	    request.setAttribute("hSearch", strSearch);
    // }
    return actionMapping.findForward("GmLotTrackReport");

  }

}
