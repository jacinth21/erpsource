package com.globus.operations.actions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.operations.beans.GmPartAttributeBean;
import com.globus.operations.forms.GmPartAttributeSetupForm;
import com.globus.prodmgmnt.beans.GmPartNumBean;

public class GmPartAttributeSetupAction extends GmAction {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws AppError, ServletException, IOException {

    GmPartAttributeSetupForm gmPartAttributeSetupForm = (GmPartAttributeSetupForm) form;
    gmPartAttributeSetupForm.loadSessionParameters(request);
    instantiate(request, response);
    GmPartNumBean gmPartNumBean = new GmPartNumBean(getGmDataStoreVO());
    GmPartAttributeBean gmPartAttributeBean = new GmPartAttributeBean(getGmDataStoreVO());
    
    GmCommonBean gmCom = new GmCommonBean(getGmDataStoreVO());
    ArrayList alAttributeType = new ArrayList();
    ArrayList alLogVal = new ArrayList();
    ArrayList alLblParamFields = new ArrayList();
	ArrayList alCPCList = new ArrayList();
	ArrayList alProSpecType = new ArrayList();
    HashMap hmAttributeVal = new HashMap();
    HashMap hmParam = new HashMap();
    HashMap hmDropValues = new HashMap();
    String strOpt = "";
    String strAttributeId = "";
    String strAction = "";
    String strForward = "PartAttributeSetup"; // default
    String strPartNum = "";
    String strUserId = "";
    String strMriSafetyInfo = "";
    String strSizeFmt = "";
    ActionMessages amMessages = new ActionMessages();
    alAttributeType = GmCommonClass.getCodeList("PARTAT");

    gmPartAttributeSetupForm.setAlAttrType(alAttributeType);
    hmAttributeVal = GmCommonClass.getAltGroupDetails(alAttributeType);
    gmPartAttributeSetupForm.setHmAttrVal(hmAttributeVal);
    strOpt = gmPartAttributeSetupForm.getStrOpt();
    hmParam = GmCommonClass.getHashMapFromForm(gmPartAttributeSetupForm);
    strAction = GmCommonClass.parseNull((String) hmParam.get("HACTION"));
    strPartNum = GmCommonClass.parseNull((String) hmParam.get("PARTNUMBER"));
    strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    if (strAction.equals("PartUDIParameter")) {
      HashMap hmUDIParam = new HashMap();
      HashMap hmSaveUDIParm = new HashMap();
      ArrayList alMRISaftyInfo = new ArrayList();
      alMRISaftyInfo = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("MRISI"));

      if (strOpt.equals("SAVE_UDI_PARAM")) {
        strMriSafetyInfo = GmCommonClass.parseZero((String) hmParam.get("MRISAFETYINFORMATION"));
        if (strMriSafetyInfo.equals("0")) {
          strMriSafetyInfo = "";
        }
        hmSaveUDIParm.put("PARTNUMBER", strPartNum);
        hmSaveUDIParm.put("ATTRIBUTETYPE", "103341"); // MRI Safety Information
        hmSaveUDIParm.put("ATTRIBUTEVALUE", strMriSafetyInfo);
        hmSaveUDIParm.put("USERID", strUserId);
        strAttributeId = gmPartNumBean.savePartAttrribute(hmSaveUDIParm);
      }
      hmUDIParam = gmPartNumBean.fetchUDIParameterDtls(strPartNum);
      strMriSafetyInfo = GmCommonClass.parseZero((String) hmUDIParam.get("MRI_SAFETY_INFO"));
      gmPartAttributeSetupForm.setAlMriSafetyInfo(alMRISaftyInfo);
      gmPartAttributeSetupForm.setHmAttrVal(hmUDIParam);
      gmPartAttributeSetupForm.setMriSafetyInformation(strMriSafetyInfo);
      strForward = "PartUDIParameter";
    }
    if(strAction.equals("PartLabelParameter")){
	HashMap hmLblParm = new HashMap();
	ArrayList alLblFormat = new ArrayList();
	alLblFormat = GmCommonClass.parseNullArrayList((ArrayList)GmCommonClass.getCodeList("LBLFMT"));			
	if(strOpt.equals("SAVE_LBL_PARAM")){
		gmPartAttributeBean.savePartNumLblParams(hmParam);
	}
	hmDropValues = gmPartNumBean.loadPartNum(); 
	alCPCList = GmCommonClass.parseNullArrayList((ArrayList)hmDropValues.get("ALACCLIST")); //CPC dropdown
	alProSpecType = GmCommonClass.parseNullArrayList((ArrayList)hmDropValues.get("PROSPECTYPE")); //Proc Specification dropdown dropdown
	hmLblParm = gmPartAttributeBean.fetchPartLblParamDtls(strPartNum);
	gmPartAttributeSetupForm.setAlLblFormat(alLblFormat);			
	strSizeFmt = GmCommonClass.parseZero((String)hmLblParm.get("SIZEFMT"));
	gmPartAttributeSetupForm.setSizefmt(strSizeFmt);			
	gmPartAttributeSetupForm.setCpcID(GmCommonClass.parseNull((String)hmLblParm.get("CPCID")));
	gmPartAttributeSetupForm.setProSpecType(GmCommonClass.parseNull((String)hmLblParm.get("PROSPECTYP")));
	gmPartAttributeSetupForm.setHmAttrVal(hmLblParm);
	gmPartAttributeSetupForm.setAlCPCList(alCPCList);
	gmPartAttributeSetupForm.setAlProSpecType(alProSpecType);
	alLogVal = gmCom.getLog(strPartNum,"400547");
	request.setAttribute("hmLog",alLogVal);	
	strForward = "PartLabelParameter";
    }	
    if (strOpt.equals("save")) {
        strAttributeId = gmPartNumBean.savePartAttrribute(hmParam);
      // this class will take messages from properties file.
      ActionMessage success = new ActionMessage("message.success"); // Record Saved Successfully
      amMessages.add("success", success);
      saveMessages(request, amMessages);
    }
    log.debug("strAttributeId" + strAttributeId);
    gmPartAttributeSetupForm.setAttributevalue(gmPartAttributeSetupForm.getAttributevalue());
    return mapping.findForward(strForward);
  }
  
}
