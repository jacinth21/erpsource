/*
 * Module: GmPartAttributeBean.java Author: RAJA 
 * Project: Globus Medical App 
 * Date-Written: 23 Apr 2019 Security: Unclassified Description:
 * ---------------------------------------------------------
 */

package com.globus.operations.beans;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.StringTokenizer;
import oracle.jdbc.driver.OracleTypes;
import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogError;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.custservice.beans.GmSalesBean;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.valueobject.common.*;


public class GmPartAttributeBean extends GmBean {
  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to
  // Initialize
  // the
  // Logger
  // Class.

  GmCommonClass gmCommon = new GmCommonClass();

  // this will be removed all place changed with Data Store VO constructor
  public GmPartAttributeBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }
  
  /**
   * Constructor will populate company info.
   * @param gmDataStore
   */
  public GmPartAttributeBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }
  
  /**
   * This method used to fetch Label param values.
   * @param strPartNumber
   * @return
   * @throws AppError
   */

  public HashMap fetchPartLblParamDtls (String strPartNumber) throws AppError {
	    HashMap hmReturn = new HashMap();
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    gmDBManager.setPrepareString("gm_pkg_pd_part_attribute_rpt.gm_fch_part_lbl_param", 2);
	    gmDBManager.setString(1, strPartNumber);
	    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
	    gmDBManager.execute();
	    hmReturn = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
	    gmDBManager.close();
	    return hmReturn;
  }
  /**
   * This method used to save Label param values.
   * @param hmParam
   * @throws AppError
   */

  public void savePartNumLblParams(HashMap hmParam) throws AppError {
	    String strPartNumber = GmCommonClass.parseNull((String) hmParam.get("PARTNUMBER"));
	    String strLabelAttribute = GmCommonClass.parseNull((String) hmParam.get("HLABELATTRIBUTE"));
	    StringTokenizer stToken = new StringTokenizer(strLabelAttribute, "|");
	    String strGeneralSpec = "";  
	    String strSize = "";
	    String strSizeFormat = ""; 
	    String strBoxLabel = "";
	    String strPatientLabel = "";                
	    String strPackageLabel = "";    
	    String strSample = ""; 
	    String strMaterialSpec = "";                
	    String strPartDrawing = ""; 
	    String strContractProcClient="";     
	    String strProcSpecType="";   
	    String strTokenString = "";
	    String strArr[] = strLabelAttribute.split("\\|");
	 
		strGeneralSpec = GmCommonClass.parseNull((String)strArr[0]);
	    strSize = GmCommonClass.parseNull((String)strArr[1]);
	    strSizeFormat = GmCommonClass.parseNull((String)strArr[2]);
	    strBoxLabel = GmCommonClass.parseNull((String)strArr[3]);
	    strPatientLabel =GmCommonClass.parseNull((String)strArr[4]);
	    strPackageLabel = GmCommonClass.parseNull((String)strArr[5]);
	    strSample = GmCommonClass.parseNull((String)strArr[6]);
	    strMaterialSpec = GmCommonClass.parseNull((String)strArr[7]);
	    strPartDrawing = GmCommonClass.parseNull((String)strArr[8]);
	    strContractProcClient = GmCommonClass.parseNull((String)strArr[9]);
	    strProcSpecType = GmCommonClass.parseNull((String)strArr[10]);
	  
	    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
	    // Database Connection
	    log.debug("in savePartNumLblParams :");
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    gmDBManager.setPrepareString("GM_PKG_PD_PART_ATTRIBUTE_TXN.GM_SAV_PART_PARAMS", 14);    
	    gmDBManager.registerOutParameter(14, OracleTypes.VARCHAR);
	    gmDBManager.setString(1, strPartNumber);
	    gmDBManager.setString(2, strGeneralSpec);
	    gmDBManager.setString(3, strSize);
	    gmDBManager.setString(4, strSizeFormat);
	    gmDBManager.setString(5, strBoxLabel);
	    gmDBManager.setString(6, strPatientLabel);
	    gmDBManager.setString(7, strPackageLabel);
	    gmDBManager.setString(8, strSample);
	    gmDBManager.setString(9, strMaterialSpec);
	    gmDBManager.setString(10, strPartDrawing);
	    gmDBManager.setString(11, strContractProcClient);
	    gmDBManager.setString(12, strProcSpecType);
	    gmDBManager.setString(13, strUserId);
	    gmDBManager.execute();
	    String strAttribteId = GmCommonClass.parseNull(gmDBManager.getString(14));
	    log.debug("GM_PKG_PD_PART_ATTRIBUTE_TXN strAttribteId==> "+strAttribteId);
	    gmDBManager.commit();
  }
}
// end of class GmPartAttributeBean
   