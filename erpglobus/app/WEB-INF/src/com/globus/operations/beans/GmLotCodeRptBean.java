/**
 * @author arajan
 */
package com.globus.operations.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;


public class GmLotCodeRptBean extends GmBean {

  Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing log4j

  public GmLotCodeRptBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmLotCodeRptBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  /**
   * fetchLotCodeProdInfo - This method is used to fetch the product information
   * 
   * @param String
   * @return HashMap
   * @exception AppError
   **/
  public HashMap fetchLotCodeProdInfo(String strLotNum) throws AppError {
    HashMap hmReturn = new HashMap();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_lotcode_rpt.gm_fch_product_info", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strLotNum);
    gmDBManager.execute();
    hmReturn = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    return hmReturn;
  }

  /**
   * fetchLotCodeDonorInfo - This method is used to fetch the donor details to the screen
   * 
   * @param String
   * @return HashMap
   * @exception AppError
   **/
  public HashMap fetchLotCodeDonorInfo(String strLotNum) throws AppError {
    HashMap hmReturn = new HashMap();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_lotcode_rpt.gm_fch_donor_info", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strLotNum);
    gmDBManager.execute();
    hmReturn = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    return hmReturn;
  }

  /**
   * fetchLotCodeInfo - This method is used to fetch the lot code information
   * 
   * @param String
   * @return HashMap
   * @exception AppError
   **/
  public HashMap fetchLotCodeInfo(String strLotNum) throws AppError {
    HashMap hmReturn = new HashMap();
    String strExpDate = "";
    String strStatus = "";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_lotcode_rpt.gm_fch_lot_info", 3);
    gmDBManager.registerOutParameter(2, OracleTypes.VARCHAR);
    gmDBManager.registerOutParameter(3, OracleTypes.VARCHAR);
    gmDBManager.setString(1, strLotNum);
    gmDBManager.execute();
    strExpDate = GmCommonClass.parseNull(gmDBManager.getString(2));
    strStatus = GmCommonClass.parseNull(gmDBManager.getString(3));

    gmDBManager.close();
    hmReturn.put("EXPDATE", strExpDate);
    hmReturn.put("CURRSTATUS", strStatus);
    return hmReturn;
  }

  /**
   * fetchLotCodeRecInfo - This method is used to fetch the receiving information
   * 
   * @param String
   * @return HashMap
   * @exception AppError
   **/
  public ArrayList fetchLotCodeRecInfo(String strLotNum) throws AppError {
    ArrayList alResult = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_lotcode_rpt.gm_fch_receiving_info", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strLotNum);
    gmDBManager.execute();
    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    return alResult;
  }

  /**
   * fetchLotCodeOpentxn - This method is used to fetch the open transactions
   * 
   * @param String
   * @return HashMap
   * @exception AppError
   **/
  public ArrayList fetchLotCodeOpentxn(String strLotNum) throws AppError {
    ArrayList alResult = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_lotcode_rpt.gm_fch_open_txn", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strLotNum);
    gmDBManager.execute();
    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    return alResult;
  }

  /**
   * fetchLotCodeInvInfo - This method is used to fetch the inventory information
   * 
   * @param String
   * @return HashMap
   * @exception AppError
   **/
  public ArrayList fetchLotCodeInvInfo(String strLotNum) throws AppError {
    ArrayList alResult = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_lotcode_rpt.gm_fch_inventory_info", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strLotNum);
    gmDBManager.execute();
    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    return alResult;
  }

  /**
   * fetchLotCodeTxnHistory - This method is used to fetch the transaction history
   * 
   * @param String
   * @return HashMap
   * @exception AppError
   **/
  public ArrayList fetchLotCodeTxnHistory(String strLotNum) throws AppError {
    ArrayList alResult = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_lotcode_rpt.gm_fch_txn_history", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strLotNum);
    gmDBManager.execute();
    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    return alResult;
  }

  /**
   * fetchDHRDetails - This method is used to fetch vendor id and workorder id
   * 
   * @param String
   * @return HashMap
   * @exception AppError
   **/
  public HashMap fetchDHRDetails(String strDHRID) throws AppError {
    String strVendorId = "";
    String strWorkOrderId = "";
    strDHRID = strDHRID + "|";
    HashMap hmParam = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    // To get vendor id
    gmDBManager.setFunctionString("gm_pkg_op_donorlot_rpt.get_vendor_id", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setString(2, strDHRID);
    gmDBManager.execute();
    strVendorId = GmCommonClass.parseNull(gmDBManager.getString(1));
    // To get workorder id
    gmDBManager.setFunctionString("gm_pkg_op_donorlot_rpt.get_workorder_id", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setString(2, strDHRID);
    gmDBManager.execute();
    strWorkOrderId = GmCommonClass.parseNull(gmDBManager.getString(1));
    gmDBManager.close();

    hmParam.put("VENDORID", strVendorId);
    hmParam.put("WOID", strWorkOrderId);
    return hmParam;
  }

  /**
   * fetchLogDtls to fetch all the comments of the transactions to the lot code report screen
   * 
   * @param String
   * @return Object
   */
  public RowSetDynaClass fetchLogDtls(String strID) throws AppError {
    ArrayList alResult = new ArrayList();
    RowSetDynaClass rdResult = null;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_lotcode_rpt.gm_fch_txn_log_info", 2);
    gmDBManager.setString(1, strID);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();
    rdResult = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    return rdResult;
  }
  
  /**
   * fetchLotTrackingDetails - This method will fetch the lot tracking details
   * @param HashMap hmParams
   * @return ArrayList
   * @exception
   */
  public ArrayList fetchLotTrackingDetails(HashMap hmParams) throws AppError {
	    ArrayList alResult = new ArrayList();
	    StringBuffer sbQuery = new StringBuffer();
	    String strPart = GmCommonClass.parseNull((String) hmParams.get("PARTNUM"));
	    String strctrlNum = GmCommonClass.parseNull((String) hmParams.get("CTRLNUM"));
	    String strDonorNum = GmCommonClass.parseNull((String) hmParams.get("DONORNUM"));
	    String strWareHouse = GmCommonClass.parseZero((String) hmParams.get("WAREHOUSE"));
	    String strExpDateRange = GmCommonClass.parseZero((String) hmParams.get("EXPDATERANGE"));
	    String strExpDate = GmCommonClass.parseNull((String) hmParams.get("EXPDATE"));
	    String strInternationalUse = GmCommonClass.parseNull((String) hmParams.get("INTERNATIONALUSE"));
	    String strDateFmt = getGmDataStoreVO().getCmpdfmt();
	    strInternationalUse = strInternationalUse.equalsIgnoreCase("on") ? "YES" : "NO";
	    String strStatus = GmCommonClass.parseNull((String) hmParams.get("LOTTYPE"));
	    String strFieldSalesNm = GmCommonClass.parseZero((String) hmParams.get("FIELDSALESNM"));
	    String strAccountNm = GmCommonClass.parseZero((String) hmParams.get("ACCOUNTNM"));
	    String strConsigneeNm =
	        !strFieldSalesNm.equals("0") ? strFieldSalesNm : !strAccountNm.equals("0") ? strAccountNm
	            : "0";
	    String shippedFromDate = GmCommonClass.parseNull((String) hmParams.get("SHIPPEDFROMDATE"));
	    String shippedToDate = GmCommonClass.parseNull((String) hmParams.get("SHIPPEDTODATE"));
	    
	    HashMap hmTempComp = new HashMap(); // To set the comparison signs to corresponding code id
	    hmTempComp.put("90191", "<");
	    hmTempComp.put("90192", "=");
	    hmTempComp.put("90194", "<=");
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    sbQuery.append(" SELECT T2550.C205_PART_NUMBER_ID partnum, GET_PARTNUM_DESC(T2550.C205_PART_NUMBER_ID) partdesc, T2550.C2550_CONTROL_NUMBER ctrlnum ");
        sbQuery.append(" , TO_CHAR(T2550.C2550_EXPIRY_DATE, '" + strDateFmt + "') expdate");
        sbQuery.append(" , T2540.C2540_DONOR_NUMBER donornum, GET_CODE_NAME(T2540.C901_SEX) sex, T2540.C2540_AGE age ");
        sbQuery.append(" , 1 qty, DECODE(T2550.C901_REF_TYPE,103923, T2550.C704_ACCOUNT_NM,103922,  T2550.C701_DISTRIBUTOR_NAME , NULL) locationcode , get_code_name(T2550.C901_LAST_UPDATED_WAREHOUSE_TYPE) warehouse, get_code_name(T2550.C901_REF_TYPE) type ");
        sbQuery.append(" , DECODE(T2540.C901_INTERNATIONAL_USE,'1960','Y','1961','N',NULL) internationaluse ");	         
        sbQuery.append(" , NVL(T2550.C2550_CUSTOM_SIZE, T205M.c205m_size) customsize, get_code_name(t2540.c901_research) research");
        sbQuery.append(" , NULL reservQty, T2550.C2550_REF_ID refid,T2550.C901_SHIP_TO_TYPE shiptotype,GET_CS_SHIP_NAME(T2550.C901_SHIP_TO_TYPE,T2550.C5060_SHIP_TO_ID) shippingname, TO_CHAR(T2550.C5060_SHIPPED_DT, '" + strDateFmt + "') shipdate, T2550.C5060_CUSTOMER_PO customerpo , T2550.c2550_last_updated_trans_id transactionid ");
        sbQuery.append(" FROM T2550_PART_CONTROL_NUMBER T2550, T2540_DONOR_MASTER T2540, t205m_part_label_parameter T205M , T205_PART_NUMBER T205 ");
        sbQuery.append(" WHERE t2550.c205_part_number_id = t205.c205_part_number_id AND T2550.C205_PART_NUMBER_ID = T205M.C205_PART_NUMBER_ID ");
        sbQuery.append(" AND T2550.C2540_DONOR_ID = T2540.C2540_DONOR_ID(+) ");
        sbQuery.append(" AND T2540.C2540_VOID_FL(+) IS NULL "); 
        
         if (!strPart.equals("")) { // If part number is given in filter
            sbQuery.append(" AND REGEXP_LIKE(T2550.c205_part_number_id, CASE WHEN TO_CHAR('" + strPart
                + "') IS NOT NULL THEN TO_CHAR('" + strPart + "') ELSE T2550.c205_part_number_id END) ");
          }
          if (!strctrlNum.equals("")) {// If control number is given in filter
            sbQuery.append(" AND UPPER(T2550.C2550_CONTROL_NUMBER) = UPPER('" + strctrlNum + "') ");
          }
          if (!strDonorNum.equals("")) {// If donor number is given
            sbQuery.append(" AND UPPER(T2540.C2540_DONOR_NUMBER) = UPPER('" + strDonorNum + "') ");
          }
          if (!strWareHouse.equals("0")) { // If Warehouse value is selected in dropdown
            sbQuery.append(" AND T2550.C901_LAST_UPDATED_WAREHOUSE_TYPE = " + strWareHouse);  
          }
          else  {
        	  sbQuery.append(" AND t2550.c2550_lot_status = 105006 "); 
          }
          if (!strExpDate.equals("")) { // Filter based on Expiry date range is selected
            sbQuery.append(" AND TRUNC(T2550.C2550_EXPIRY_DATE) ");
            sbQuery.append(hmTempComp.get(strExpDateRange));
            sbQuery.append(" TRUNC(TO_DATE('" + strExpDate + "','" + strDateFmt + "'))");
          }
          if (strInternationalUse.equals("YES")) { // Should fetch only the records having international
                                                   // use value as yes
            sbQuery.append(" AND T2540.C901_INTERNATIONAL_USE = '1960'");// 1960: Yes [code lookup value]
          }
           sbQuery.append(" AND T2550.C1900_COMPANY_ID = "+ getGmDataStoreVO().getCmpid());
           sbQuery.append(" AND T2550.C5040_PLANT_ID = "+getGmDataStoreVO().getPlantid());
           
           if (!strConsigneeNm.equals("0")) {
        	      sbQuery
        	          .append(" AND DECODE(T2550.C901_REF_TYPE,103923, T2550.C704_ACCOUNT_NM,103922,  T2550.C701_DISTRIBUTOR_NAME) = '");
        	      sbQuery.append(strConsigneeNm);
        	      sbQuery.append("'");
        	    }
           
         //If warehouse type is Account Qty or Field Sales Qty
           if(strWareHouse.equals("4000339") || strWareHouse.equals("56002")){
       	    if (!shippedFromDate.equals("")){
       	    	sbQuery.append(" AND TRUNC(T2550.C5060_SHIPPED_DT)  >= TRUNC(TO_DATE('" + shippedFromDate + "','" + strDateFmt + "'))");
       	    } 
       	    
       	    if (!shippedToDate.equals("")){
       	    	sbQuery.append(" AND TRUNC(T2550.C5060_SHIPPED_DT)  <= TRUNC(TO_DATE('" + shippedToDate + "','" + strDateFmt + "'))");
       	    }
           }
      
           log.debug(" Query For LotcodeRptBean fetchLotTrackingDetails() : " + sbQuery.toString());
           gmDBManager.setPrepareString(sbQuery.toString());
           alResult = gmDBManager.queryMultipleRecord();
	   return alResult;
  }
 }
