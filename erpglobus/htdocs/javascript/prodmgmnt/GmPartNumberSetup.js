var Cbo_ContProClnt = '';

var validForm = '';
var duplicateFormFl = false;
var typefl = false;
	
function fnCreatePart(form)
{
	var parentform = document.getElementsByName('frmPartSetup')[0];// To get parent form name
	var obj = form;
	var fld1 = obj.Cbo_Field1.value;
	var fld2 = obj.Txt_Field2.value;
	var fld3 = obj.Cbo_Field3.value;
	var fld4 = obj.Txt_Field4.value;
	var partNum = fld1+fld2+"."+fld3+fld4;
	parentform.Txt_PartNum.value = partNum;
	parentform.Txt_PartNum.focus();
}

function fnShowCreator(form)
{
	var currentTab = form.hcurrentTab.value;
	if(currentTab == 'partdetails' || currentTab == ''){// Should show only if the tab is "Part Details"
		if (document.all.PerfDimDiv.style.display == "block")
		{
			document.all.PerfDimDiv.style.display = "none";
		}
		else
		{
			document.all.PerfDimDiv.style.display = "block";
		}
	}
	
}

function fnSetProjNum(obj)
{
	var form = document.getElementsByName('frmPartSetup')[1];// To get the form value
	if(form == undefined){
		form = validForm;
	}
	var val = obj.value;
	val = val.substring(5,7);
	form.Txt_Field2.value = val;
}

function fnSubmit(form)
{
	var parentform = document.getElementsByName('frmPartSetup')[0];
	var strPartFamily = form.Cbo_ProdFly.value;
	var hPnum= TRIM(parentform.hPartNum.value);
	var pnum = TRIM(parentform.Txt_PartNum.value);
	var strLotTrackFl = (form.lotTrackFl.checked == true)?"Y":"N";  //Lot Tracking Needed checkbox
	var strSerialNumFl = (form.serialNumFl.checked == true)?"Y":"N";  //Serial Number Needed checkbox
	if (form.Chk_TaggablePartFl.checked == true) {
		if (strPartFamily != '4052') {
			Error_Details(message[5676]);
		}
	}

	if (form.Txt_AFl.checked == true) {
		form.Txt_AFl.value = "Y";
	}
	if (form.Chk_TracImpl.checked == true) {
		form.Chk_TracImpl.value = "Y";
	}
	/*
	 * if(document.frmPartSetup.Chk_RelForSale.checked==true) {
	 * document.frmPartSetup.Chk_RelForSale.value="Y"; }
	 */

	if (form.Chk_SupplyFl.checked == true) {
		form.Chk_SupplyFl.value = "Y";
	}
	if (form.Chk_SubAssemblyFl.checked == true) {
		form.Chk_SubAssemblyFl.value = "Y";
	}
	if (form.Chk_TaggablePartFl.checked == true) {
		form.Chk_TaggablePartFl.value = "Y";
	}
	if (form.Chk_FixedSizeFl.checked == true) {
		form.Chk_FixedSizeFl.value = "Y";
	} else {
		form.Chk_FixedSizeFl.value = "N";
	}
	
	var valStatus = form.Cbo_PartStatus.value;
	var strMaterial = form.Cbo_ProdMat.value;
	var strStrcType = form.Cbo_StrucType.value;
	var strPresMethod = form.Cbo_PresMethod.value;
	var strShelfLife = form.Txt_ShelfLife.value;
	var objRegExp = /(^-?\d\d*$)/;
	if(strShelfLife){
		if((strShelfLife <= '0') || (!objRegExp.test(strShelfLife))){
			Error_Details(message[5677]);
		}
	}
	var strStorTemp = form.Cbo_StorTemp.value;
	//var strProcSpecType = form.Cbo_ProSpecType.value; Processing spec type section moved to Label parameter tab.
	var strMastProd = TRIM(form.Txt_MastProd.value);
//	var strCPClient = Cbo_ContProClnt.getSelectedValue(); Contract Processing Client section moved to Label parameters tab.
	var strFixedSizeFl = form.Chk_FixedSizeFl.value;
	var strTollerance = form.Txt_Tollernace.value;
	fnValidateTextFld(parentform.Txt_PartNum, message[5705]);
	fnValidateTextFld(form.Txt_PartDesc,message[5706]);
	fnLenValidation(form.Txt_PartDesc,message[5706],"4000");// to validate Part Description field for 4000 characters
	//fnLenValidation(form.Txt_DetailDesc," Detailed Description","4000");// to validate Detailed Description field for 4000 characters
	
	fnValidateDropDn('Cbo_Proj',message[5707]);
	fnValidateDropDn('Cbo_SalesGroup',lblsalesGroup);
	if ( valStatus!="20365" && valStatus!="20366")
	{ 
		fnValidateDropDn('Cbo_ProdFly',message[5709]);
		fnValidateDropDn('Cbo_Class',message[5710]);
		fnValidateDropDn('Cbo_ProdMat',message[5711]);
		fnValidateDropDn('Cbo_Mat',lblMaterialSpec);		
		fnValidateDropDn('Cbo_MesDev',lblMeasuringDevice);		
		fnValidateDropDn('Cbo_PartStatus',message[5714]);
		fnValidateDropDn('Cbo_UOM',lblUom); 
		fnValidateTextFld(form.Txt_PartDesc, lblPartDescription);
		fnValidateTextFld(form.Txt_Draw, lblDrawingNum);
		fnValidateTextFld(form.Txt_Rev,message[5718]);
		fnValidateTextFld(form.Txt_InsertId, lblInsertId);
		fnValidateTextFld(form.Txt_InspRev, message[5720]);
	}
	
	if (strMaterial=="100845" && (strStrcType=="0" || strPresMethod=="0")){
		fnValidateDropDn('Cbo_StrucType',lblStructuralType);
		fnValidateDropDn('Cbo_PresMethod',lblPreseveMethos);
	}
	if (strPresMethod!="0" && strStorTemp=="0"){
		fnValidateDropDn('Cbo_StorTemp',lblStorageTemp);
	}
	/*if ((strProcSpecType=="104531" || strProcSpecType=="104534") && strMastProd== ""){
		fnValidateTextFld(form.Txt_MastProd,' Master Product ');
	}*/
    /*if ((strProcSpecType=="104531" || strProcSpecType=="104532" || strProcSpecType=="104534") && (strCPClient=="0" || strCPClient == null)){		
		fnValidateDropDn('Cbo_ContProClnt',' Contract Processing Client ');
	}*/
	if(strMastProd == hPnum && strMastProd != ''){
		Error_Details(message[5678]);
	}
	if(hPnum !='' && hPnum != pnum && !partAvailable){
		Error_Details(message[5679]);
	}
	if(partAvailable && hPnum != pnum){
		parentform.hPartNum.value = pnum;
	}
	if (form.hActionForVali.value == "Lookup")	
	{
		fnValidateTextFld(form.Txt_LogReason,message[5550]);
	}

	var arChk = form.Chk_GrpRFS;
	var str = ''; 
	var len = arChk.length;
	for (i=0;i< len;i++ )
	{
		if (form.Chk_GrpRFS[i].checked)
		{
			str = str + form.Chk_GrpRFS[i].value + ',';
		}
	}
	len = str.length;
	str = str.substring(0,len-1);
	form.hChkRFS.value = str;
    
	var arr1 = pathwayId.split(",");
    var arr2 = pathwayGrp.split(",");
    var arr3 = pathwayNm.split(",");
    var pathStr = '';
    var partAttStr = '';

    intsize = pathwaySize;

     artifacts = form.Cbo_Artifacts.value;
     if(artifacts != '' && artifacts != '0'){
    	 partAttStr += "4000444^"+artifacts+"|"; //4000444 (Artifacts) Part Attribute type
     }
     // if no values selected then set the empty value.
     strStrcType = fnCorrectValDropdown(strStrcType);
     strPresMethod = fnCorrectValDropdown(strPresMethod);
     strStorTemp = fnCorrectValDropdown(strStorTemp);
    // strProcSpecType = fnCorrectValDropdown(strProcSpecType);
     /*if(strCPClient =='0'){
    	 strCPClient = '';
     }*/
     //104480: Lot tracking required ?; 106040: Serial Number Needed ?
  	     partAttStr += "103725^" + strShelfLife + "|" + "103726^" + strStrcType
			+ "|" + "103727^" + strPresMethod + "|" + "103728^" + strStorTemp
			+ "|" + "103730^" + strMastProd   + "|" + "103732^" + strFixedSizeFl
			+ "|" + "103733^" + strTollerance //+ "|";
			+ "|" + "104480^" + strLotTrackFl +"|" + "106040^" + strSerialNumFl +"|";
     form.hPartAttribute.value = partAttStr; 

	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	
	form.Cbo_Class.disabled = false;
	form.action = "/GmPartNumServlet?hOpt=FETCHCONTAINER&hAction=Save&Txt_PartNum="+pnum+"&hPartNum="+pnum;
	form.Btn_Submit.disabled=true; // To disable the Submit button
	fnStartProgress();
	form.submit();
}

function fnCallLookup(form)
{
	// validate the Part # filed
	fnValidateTextFld(form.Txt_PartNum,message[5705]);
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	/*form.hAction.value = "Lookup";
	form.strOpt.value = "FETCHCONTAINER";*/
	form.action = "/GmPartNumServlet?hOpt=FETCHCONTAINER&hAction=Lookup";
	form.Btn_Load.disabled=true; // To disable the Submit button
	fnStartProgress();
	form.submit();
}

function fnHtmlEditor(){
	tinyMCE.init({
		mode : "specific_textareas",
	    editor_selector : "HtmlTextArea",
		theme : "advanced",
		theme_advanced_path : false,
		height : "300",
		width: "500" 
	});
}
function fnLoad()
{
	fnHtmlEditor();
	validForm = document.getElementsByName('frmPartSetup')[1];
	duplicateFormFl = false;
	if(validForm == undefined){
		validForm = document.getElementsByName('frmPartSetup')[0];
		duplicateFormFl = true;
	}
	var form = validForm;
	if(form){		
		var val = form.hChkRFS.value;
		var arr = val.split(",");
		var rfs = form.Chk_GrpRFS;
		var valStatus = form.Cbo_PartStatus.value;
		
		for (i=0;i<arr.length ;i++ )
		{
			for (j=0;j<rfs.length ;j++ )
			{
				if (arr[i] == rfs[j].value)
				{
					form.Chk_GrpRFS[j].checked = true;
				}
			}
		}
	
		if ( valStatus!="20367") //20367: Approved
		{
			form.Txt_AFl.checked = false;
			form.Txt_AFl.disabled = true;
		}
		
		// to Sterility value changes
		fnChangeFamily(form,form.Cbo_ProdFly.value);	
		// to load the Auto list values.        
		//Cbo_ContProClnt = loadDXCombo('divCbo_ContProClnt','Cbo_ContProClnt',document.getElementById('hCbo_ContProClnt').value,'','',300,0);

		if(document.getElementById('tableHeader') != undefined && document.getElementById('tableHeader') != null){
			document.getElementById('tableHeader').focus();
		}
	}
}

function fnPartMapping(form)
{
	var parentform = document.getElementsByName('frmPartSetup')[0];
	var partNum = parentform.Txt_PartNum.value;
	fnValidateTextFld(parentform.Txt_PartNum, message[5705]);
	
	if(partAvailable){
		Error_Details(message[5680]);
	}
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	
	form.action = "/GmPartNumMapServlet?Txt_PartNum="+partNum+"&hAction=LoadPartDetails";
	form.submit();
}

function fnReset(form)
{
	form.action = "/GmPartNumServlet?hOpt=FETCHCONTAINER&hAction=Load"; 
	form.submit();
}


function fnPartPricing(form)
{
	var parentform = document.getElementsByName('frmPartSetup')[0];
	fnValidateTextFld(parentform.Txt_PartNum,message[5705]);
	
	if(partAvailable){
		Error_Details(message[5680]);
	}
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	form.PartNum.value = parentform.Txt_PartNum.value;
	form.hMode.value	= "Edit";
	form.hId.value	= form.Cbo_Proj.value;
	form.action = "/GmPartPriceEditServlet";
	form.submit();
}

function fnChkPartType(form, value){
	var pnum = TRIM(value);
	var hPnum= form.hPartNum.value;
	var cntr =0;	
	var childform = document.getElementsByName('frmPartSetup')[1];
	if(childform == undefined){
		childform = validForm;
	}
	for (var i = 0; i < value.length; i++) 
		{
			if(value.charAt(i) == '.')
			{
				cntr++;
			}
		}
		if (cntr > 1 && hPnum!=value)	
		{	//set default values 	
			childform.Cbo_Class.value='4031';
			//childform.Cbo_MatCert.value= 'A';
			//childform.Cbo_CompCert.value='A' ;
			childform.Cbo_MesDev.value= '4083'; 
			//childform.Cbo_Prim.value= '5102';
			//childform.Cbo_Sec.value='5102';
			childform.Txt_InsertId.value='N/A';
			//childform.Cbo_HardTest.value='A';
			childform.Cbo_SalesGroup.value='300.026';
		}	
		
		fnCheckPartNum(pnum);
}

function fnShowFilters(val)
{
	var obj = eval("document.all."+val);
	var obj1 = eval("document.all."+val+"img");
	if (obj.style.display == 'none')
	{
		obj.style.display = 'block';
		if(obj1){
			obj1.src = '/images/minus.gif';
		}
	}
	else
	{
		obj.style.display = 'none';	
		if(obj1){
			obj1.src = '/images/plus.gif';
		}
	}
}
 
function fnValidateActivefl(form)
{
	var valStatus = form.Cbo_PartStatus.value;
	if ( valStatus=="20367")
	{
		form.Txt_AFl.checked = true;
		form.Txt_AFl.disabled = false;
	}
	else
	{
		form.Txt_AFl.checked = false;
		form.Txt_AFl.disabled = true;
	}
	
}

// To check the availability of part number
function fnCheckPartNum(pnum){
	fnClearDiv();
	if (pnum != ''){
		partAvailable = false;
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/GmPartNumAjaxServlet?strOpt=CHECKPARTNUM&partNum='+ pnum + '&ramdomId=' + Math.random());
		dhtmlxAjax.get(ajaxUrl, fnShowImage);
		return false;
	}
}

// To Hide the image
function fnClearDiv(){
	document.getElementById("DivShowPartExists").style.display = 'none';
	document.getElementById("DivShowPartAvail").style.display = 'none';
}

// To Show the image 
function fnShowImage(loader) {
	var response = loader.xmlDoc.responseText;
	if(response != null){
		if(response == '0'){
			document.getElementById("DivShowPartExists").style.display = 'none';
			document.getElementById("DivShowPartAvail").style.display = 'block';
			partAvailable = true;
		}
		else{
			document.getElementById("DivShowPartAvail").style.display = 'none';
			document.getElementById("DivShowPartExists").style.display = 'block';
			partAvailable = false;
		}
	}
}

// To validate the text field
function fnValidateTextFld(form, fld){
	if(form.value == ''){
		Error_Details(Error_Details_Trans(message[5681],fld));
	}
}

//To assign the tab id to hidden field, to know which tag is currently open
function fnUpdateTabIndex(link) {	
	var linkObj = link.getElementsByTagName('a')[0];
	document.all.hcurrentTab.value = linkObj.id;	
}

//To limit the no. of characters entering into the text area
function fnLenValidation(obj,name,len){ 
	if(obj.value.length > len){
		var errArray=[len,name];
		Error_Details(Error_Details_Trans(message[5682],errArray));
	}
}
// to return the dropdown values
function fnCorrectValDropdown (selectedVal){
	var correctVal = selectedVal;
	if(correctVal == 0){
		correctVal = '';
	}
	return correctVal;
}

// this function used to disable the filed for Sterility
function fnChangeFamily(form,family_val){
	// Classification
	if(family_val == 0 || family_val == 4051 || family_val == 4052 || family_val == 4050 ||family_val == 4054 || family_val== 4055 
	|| family_val== 26240095 ){ // 26240095 Frequently Sold Instruments
		//form.Cbo_Class.value = 0;	// choose one
		form.Cbo_Class.disabled = false;
		fnChangeSterility (form, 'Y');
	}else{
		fnChangeSterility (form, 'N');
		form.Cbo_Class.value = 4033;	// N/A
		form.Cbo_Class.disabled = true;
	}	
}
// this function used to update the Mri information
function fnUDISubmit(form){
	fnValidateDropDn('mriSafetyInformation',lblmriSafetyInf);
	fnValidateTxtFld('comments', message[5550]);
	var partInputStr = '';
	var dateFmt = form.applnDateFmt.value;
	var objPackageDisconDt = form.packageDisconDt;
	var sterilePartFl = form.sterilePart.value;
	var expirationDtVal = form.expirationDt.value;
	var todayDate = form.hTodayDate.value;
	CommonDateValidation(objPackageDisconDt, dateFmt,message[5725] + message[611]);
	var dateDiffVal = dateDiff(todayDate,objPackageDisconDt.value,dateFmt);
	// validate the Sterile parts
	/*if (sterilePartFl == 'Y' && expirationDtVal != '20375') { // 20375	Yes
		Error_Details('Please select the valid options for sterile parts on <b>Expiration Date</b> field.');
	}*/
	// validate the date field.
	if(dateDiffVal < 0){
		Error_Details(message[5683]);
    }
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	// enable the disabled filed.
	form.lotOrBatchNum.disabled = false;
	form.manufaturingDt.disabled = false;
	form.serialNum.disabled = false;
	form.expirationDt.disabled = false;
	form.donationIdenNum.disabled = false;

	var partNum = form.partNumber.value;
	var mriInfo = form.mriSafetyInformation.value;
	var packDisconDtVal = objPackageDisconDt.value;
	var companyVal = form.overrideCompanyNm.value;
	var lotBatchVal = form.lotOrBatchNum.value;
	var manufactDtVal = form.manufaturingDt.value;
	var serialNumVal = form.serialNum.value;
	var expirationDtVal = form.expirationDt.value;
	var donationIdeNumVal = form.donationIdenNum.value;
	var userComments = form.txt_LogReason.value;
	// verify the dropdown filed.
	mriInfo = fnCorrectValDropdown(mriInfo);
	companyVal = fnCorrectValDropdown(companyVal);
	lotBatchVal = fnCorrectValDropdown(lotBatchVal);
	manufactDtVal = fnCorrectValDropdown(manufactDtVal);
	serialNumVal = fnCorrectValDropdown(serialNumVal);
	expirationDtVal = fnCorrectValDropdown(expirationDtVal);
	donationIdeNumVal = fnCorrectValDropdown(donationIdeNumVal);
	// to form the part attribute string
	partInputStr = "103341^" + mriInfo + "|103781^" + companyVal + "|103780^"
			+ packDisconDtVal + "|103782^" + lotBatchVal + "|103783^"
			+ manufactDtVal + "|103784^" + serialNumVal + "|103347^"
			+ expirationDtVal + "|103786^" + donationIdeNumVal + "|";
	var requestStr = '/gmPartParameterDtls.do?method=udiParameterDtls&partNumber='
			+ partNum
			+ '&strOpt=Save&inputStr='
			+ partInputStr
			+ '&txt_LogReason='
			+ userComments
			+ '&overrideCompanyNm='
			+ companyVal;
	// Date is not working after saved. so, to modified this tab as iframe.
	form.action = '/gmPartParameterDtls.do?method=udiParameterDtls';
	form.strOpt.value = 'Save';
	form.inputStr.value = partInputStr;
	form.submit();
}
// this function used to show the history details.
function fnShowHistory(pnum, auditId){
	windowOpener("/gmAuditTrail.do?auditId="+auditId+"&txnId="+ pnum, "","resizable=yes,scrollbars=yes,top=250,left=300,width=785,height=300,status=1");
}

//When do scan in bar code that time will call this function.
function fnCallGo() {
	if (event.keyCode == 13) {
		var formObj = document.getElementsByName('frmPartSetup')[0];// To get form obj
		var rtVal = fnCallLookup(formObj, true);
		if (rtVal == false) {
			if (!e)
				var e = window.event;

			e.cancelBubble = true;
			e.returnValue = false;

			if (e.stopPropagation) {
				e.stopPropagation();
				e.preventDefault();
			}
		}
	}
}
// this function used to reload the JSP page.
function fnPartReload(formObject) {
	formObject.hAction.value = "Lookup";
	formObject.strOpt.value = "FETCHCONTAINER";
	formObject.Btn_Load.disabled = true; // To disable the load button
	formObject.submit();
}

// this function used to change the values to falmily then show the releated values for Sterility
function fnChangeSterility(formObj, sterilFl){
	var value = '';
	var name = '';
	var oldVal = formObj.Cbo_Class.value;
	//alert(oldVal);
	formObj.Cbo_Class.options.length = 0;
	formObj.Cbo_Class.options[0] = new Option("[Choose One]","0");
	if(sterilFl =='Y'){
		formObj.Cbo_Class.options[1] = new Option("Sterile",4030);
		formObj.Cbo_Class.options[2] = new Option("Non-Sterile",4031);
		if(formObj.Cbo_ProdFly.value == 0){
			formObj.Cbo_Class.options[3] = new Option("N/A",4033);
		}
	}else{
		formObj.Cbo_Class.options[1] = new Option("N/A",4033);
	}
	if(oldVal !='' && oldVal != '4033'){
		formObj.Cbo_Class.value = oldVal;	
	}
}

// This function used to update the Label information
function fnLblSubmit(form) {	
	var labelAttr = '';
	var partNum = form.partnumber.value;
	var generalspec = form.genericspec.value;
	var size = form.size.value;
	var sizefmt = form.sizefmt.value;
	var boxlabel = form.boxlabel.value;
	var printlabel = form.patientlabel.value;
	var pkglabel = form.pkglabel.value;
	//CPC related fields are removed and so no need to pass with the string to save the details
	/*var cpcpatientno = form.cpcpatientno.value;
	var cpcpackageno = form.cpcpackageno.value;*/
	var sample = (form.sample.checked == true)?"Y":"N";	
	/*var cpclogopath = form.cpclogopath.value;*/
	var matspeclabel = form.matlabel.value;
	var partdrawlabel = form.partdrawlabel.value;
	var strCPClient = form.cpcID.value;
	var strProcSpecType = form.proSpecType.value;
	//Moved from part setup screen
	 if ((strProcSpecType=="104531" || strProcSpecType=="104532" || strProcSpecType=="104534") && (strCPClient=="0" || strCPClient == null)){		
			fnValidateDropDn('cpcID',' Contract Processing Client ');
		}
	
	var logval = form.Txt_LogReason.value;
	 if(strCPClient =='0'){
    	 strCPClient = '';
     }
	 strProcSpecType = fnCorrectValDropdown(strProcSpecType);
	labelAttr += generalspec + "|" + size + "|"
			+ sizefmt + "|"  + boxlabel + "|"
			+ printlabel + "|" + pkglabel + "|"
	        /*+ "103743^" + cpcpatientno + "|" + "103744^" + cpcpackageno + "|"*/
	        + sample + "|" 
	        /*+ "103746^" + cpclogopath + "|"*/
	       + matspeclabel + "|" +  partdrawlabel + "|" + strCPClient + "|" + strProcSpecType;
	        
	form.hLabelAttribute.value = labelAttr;	
	fnValidateTxtFld('genericspec',message[5726]);
	fnValidateTxtFld('size',message[5727]);
	fnValidateDropDn('sizefmt',message[5728]);
	/*fnValidateTxtFld('boxlabel',' BOX Label ');
	fnValidateTxtFld('patientlabel',' Patient Label ');
	fnValidateTxtFld('pkglabel',' Package Label ');
	fnValidateTxtFld('cpcpatientno',' CPC Patient No ');
	fnValidateTxtFld('cpcpackageno',' CPC Package No ');
	fnValidateTxtFld('cpclogopath',' CPC Logo Path ');*/
	fnValidateTxtFld('comments',message[5550]);	
		
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}

	var requestStr = '/gmPartAttribute.do?haction=PartLabelParameter&partnumber='
			+ partNum
			+ '&hLabelAttribute='
			+ labelAttr
			+ '&logval='
			+ logval
			+ '&strOpt=SAVE_LBL_PARAM';
	loadajaxpage(requestStr, 'ajaxdivcontentarea');	
}
// this function used to update the part number DHR requirement details.
function fnSaveDHRInfo(form) {
	var partNum = form.partNumber.value;
	var partAttStr = '';
	// validate the mandatory filed
	fnValidateDropDn('materialCert',lblMaterialCert);
	fnValidateDropDn('primaryPack',lblPrimaryPackaging);
	fnValidateDropDn('secondaryPack',lblSecondaryPackaging);
	fnValidateDropDn('complianceCert',lblComplianceCert);
	fnValidateTxtFld('comments', message[5550]);

	var materialCerVal = form.materialCert.value;
	var handnessVal = form.handnessCert.value;
	var primaryPackVal = form.primaryPack.value;
	var secondaryPackVal = form.secondaryPack.value;
	var complientCerVal = form.complianceCert.value;
	var userComments = form.txt_LogReason.value;
	var sterilePartFl = form.sterilePart.value;
	var vendorVal = form.hVendorDesign.value;
	var partStatusVal = form.partStatus.value;

	var udiReqVal = form.udiEtchReq.value;
	var diReqVal = form.diOnlyEtchReq.value;
	var piReqVal = form.piOnlyEtchReq.value;
	// Etch req. - more than one filed Yes then validate
	if ((udiReqVal == 103363 && (diReqVal == 103366 || piReqVal == 103384))
			|| (diReqVal == 103366 && (udiReqVal == 103363 || piReqVal == 103384))
			|| (piReqVal == 103384 && (udiReqVal == 103363 || diReqVal == 103366))) {
		Error_Details(message[5684]);
	}

	// Select any etch. req. as Yes then validate other etch req field.
	if (form.diNumberFl.checked == true) {
		fnValidateDropDn('udiEtchReq',lblUdiEtchReq);
		fnValidateDropDn('diOnlyEtchReq',lblDiOnlyEtchReq);
		fnValidateDropDn('piOnlyEtchReq',lblPiOnlyEtchReq);
		fnValidateDropDn('bulkPackage',lblBulkPackaging);
	}

	// to form the DHRPAR values
	var diNumberVal = form.diNumber.value;
	var diNumber = '';
	if (form.diNumberFl.checked == true) {
		form.diNumberFl.value = "Y";
		diNumber = 'Y';
	}
	if(diNumberVal !=''){
		diNumber = 'Y';
	}
	var woCreated = form.woCreated.value;
	var vendorDesign = form.vendorDesign.value;
	var issueAgency = form.issuingAgencyConf.value;
	var bulkPackageVal = form.bulkPackage.value;

	if(diNumberVal !='' && bulkPackageVal !=20375){ // 20375 - yes
		//Error_Details("Please select valid Bulk Package.");
	}
	if(issueAgency != 0 && diNumber !='Y' ){
		Error_Details(message[5685]);
	}
	if (diNumber =='Y' && issueAgency ==0){
		fnValidateDropDn('issuingAgencyConf',lblIssuingAgencyConfig);
	}
	woCreated = fnCorrectValDropdown(woCreated);
	udiReqVal = fnCorrectValDropdown(udiReqVal);
	diReqVal = fnCorrectValDropdown(diReqVal);
	piReqVal = fnCorrectValDropdown(piReqVal);
	vendorDesign = fnCorrectValDropdown(vendorDesign);
	issueAgency = fnCorrectValDropdown(issueAgency);
	bulkPackageVal = fnCorrectValDropdown(bulkPackageVal);
	// form the part attribute string
	partAttStr = "4000539^"+ issueAgency +"|"+ "103380^" + woCreated + "|" + "103381^" + udiReqVal + "|"
			+ "103382^" + diReqVal + "|" + "103383^" + piReqVal + "|"
			+ "4000517^" + vendorDesign + "|" +"4000540^" + bulkPackageVal +"|";

	if (diNumber == 'Y' && vendorDesign == 103420) { // 103420 - Yes
		Error_Details(message[5686]);
	}
	// 103363 - Yes - UDI Etch req.
	// 103366 - Yes - DI only Etch req.
	// 103384 - Yes - PI only Etch req.
	if (diNumber == '' && (udiReqVal == '103363' || diReqVal == '103366' || piReqVal == '103384')) {
		Error_Details(message[5738]);
	}
	if(diNumber == '' && bulkPackageVal =='20375'){ // 20375 -- Yes
		Error_Details(message[5687]);
	}
	if(vendorVal =='103420' && vendorDesign != '103420'){ //103420 - Yes
		Error_Details(message[5688]);
	}
	if(partStatusVal =='20369' && form.diNumber.value ==''){ // 20369 -Obsolete
		Error_Details(message[5689]);
	}
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}

	form.vendorDesign.disabled = false;
	var requestStr = '/gmPartParameterDtls.do?method=dhrRequirementDtls&partNumber='
			+ partNum
			+ '&strOpt=Save&materialCert='
			+ materialCerVal
			+ '&handnessCert='
			+ handnessVal
			+ '&primaryPack='
			+ primaryPackVal
			+ '&secondaryPack='
			+ secondaryPackVal
			+ '&complianceCert='
			+ complientCerVal
			+ '&inputStr='
			+ partAttStr
			+ '&txt_LogReason='
			+ userComments;
	//loadajaxpage(requestStr, 'ajaxdivcontentarea');
	form.strOpt.value = 'Save';
	form.inputStr.value = partAttStr;
	form.submit();
}

function fnSetDefaults(form){
	var valMaterial = form.Cbo_ProdMat.value;
	var structtype = (structType == '') ? "0" :  structType;
	var prevmethod = (prevMethod == '') ? "0" :  prevMethod;
	if(valMaterial != '100845'){
		form.Cbo_StrucType.value = '0';
		form.Cbo_PresMethod.value = '0';
		form.serialNumFl.checked = false;
	}else{
		form.Cbo_StrucType.value = structtype;
		form.Cbo_PresMethod.value = prevmethod;
		form.serialNumFl.checked = true;
	}
}
/* 
 * This function used to enable/disable the Di number checkbox
 */
function fnChangeIssueAgency(form) {
	var issueAgencyVal = form.issuingAgencyConf.value;
	var diNumberVal = form.diNumber.value;
	var vendorVal = form.hVendorDesign.value;
	if(vendorVal != 103420){ // 103420 - Yes
		// to enable the di Number check box
		if (issueAgencyVal != 0 && diNumberVal == '') {
			form.diNumberFl.checked = true;
			form.diNumberFl.disabled = false;
			if(form.hVendorDesign.value != 103420){ // 103420 - Yes
				form.vendorDesign.value = 103421;//103421 -No
				form.vendorDesign.disabled = true;
			}
		} else {
			form.diNumberFl.disabled = true;
		}
		// to uncheck the di Number check box
		if (issueAgencyVal == 0 && diNumberVal == '') {
			form.diNumberFl.checked = false;
			form.vendorDesign.disabled = false;
			form.diNumberFl.disabled = false;
		}
	}
}


/*
 * This function used to set the default values for UDI Parameter screen
 */
function fnLoadUdiParam() {
	var form = document.getElementsByName('frmPartParameterDtls')[0];
	var diNumVal = form.diNumber.value;
	var sterilePartFl = form.sterilePart.value;
	var expirationVal = '';
	var lotBatch_default_val = form.hRuleLotBatch.value;
	var manufact_default_val = form.hRuleManufact.value;
	var serialNum_default_val = form.hRuleSerialNum.value;
	var donationIden_default_val = form.hRuleDonationIden.value;
	var issueAgencyPatterVal = form.hAgencyPattern.value;
	// to set the default height
	if(parent.window.document.getElementById("ajaxdivcontentarea") != undefined){
  		parent.window.document.getElementById("ajaxdivcontentarea").style.height = "680";
  		parent.window.document.getElementById("ajaxdivcontentarea").style.width = "960";
  	}
	// DI number empty then to set the default values
	if (diNumVal == '') {
		// default to set as 20377	N/A
		form.lotOrBatchNum.value = '20377';
		form.manufaturingDt.value = '20377';
		form.serialNum.value = '20377';
		form.expirationDt.value = '20377';
		form.donationIdenNum.value = '20377';
		form.btn_Submit.disabled = true;
	}/* else {
		//20375	Yes
		//20376	No
		var objLotBatch = form.lotOrBatchNum;
		var objManufDt = form.manufaturingDt;
		var objSerialNum = form.serialNum;
		var objExpirationDt = form.expirationDt;
		var objDonationIdenNum = form.donationIdenNum;
		// validate empty field (first time)
		
		103940	DI
		103945	Donor Id
		103942	EXP date
		103943	LOT
		103941	MFG date
		103944	Serial #
		4000688	Supplemental serial #
		 
		if (issueAgencyPatterVal.indexOf(103943) != -1) {
			objLotBatch.value = 20375;
		}else{
			objLotBatch.value = 20375;
		}
		if (issueAgencyPatterVal.indexOf(103941) != -1) {
			objManufDt.value = 20375;
		}else{
			objManufDt.value = 20376;
		}
		if (issueAgencyPatterVal.indexOf(103944) != -1) {
			objSerialNum.value = 20375;
		}else{
			objSerialNum.value = 20376;
		}
		if ((issueAgencyPatterVal.indexOf(103942) != -1) && sterilePartFl =='Y') {
			objExpirationDt.value = '20375';
		}
		if(sterilePartFl !='Y' && objExpirationDt.value ==0){
			objExpirationDt.value = '20376';
		}
		
		if (issueAgencyPatterVal.indexOf(103945) != -1) {
			objDonationIdenNum.value = 20375;
		}else{
			objDonationIdenNum.value = 20376;
		}
	}*/
}
// This function used to when change the Vendor design as Yes - at the time to
// disable the DI flag
function fnChangeVendorDesign(form) {
	var vendorDesign = form.vendorDesign.value;
	if (vendorDesign == '103420') { // 103420 - Yes
		form.diNumberFl.checked = false;
		form.diNumberFl.disabled = true;
		form.issuingAgencyConf.value = 0;
		fnShowMandatoryField (false);
	} else {
		form.diNumberFl.disabled = false;
	}
}

function fnPDSubmit(form) {
	var strinputStr = "";
	// validation
	form.btn_submit.disabled = true;
	fnValidateDropDn('contains',lblContainsLatex);
	fnValidateDropDn('ReqSteril',lblRequiresSterilization);
	fnValidateDropDn('IsDevice',lblIsDevAvail);
	fnValidateTxtFld('comments',message[5550]);

	var strPartNum = form.partNumber.value;
	var strContainsLatex = fnCorrectValDropdown(form.contains.value);
	var strReqSteril = fnCorrectValDropdown(form.ReqSteril.value);
	var strSterilMthod = fnCorrectValDropdown(form.SterilMthod.value);
	var strIsDevice = fnCorrectValDropdown(form.IsDevice.value);
	var strSize1Type = fnCorrectValDropdown(form.Size1Type.value);
	var strSize1TypeText = form.size1_type.value;
	var strSize1Value = form.size1_value.value;
	var strSize1UOM = fnCorrectValDropdown(form.Size1UOM.value);
	var strSize2Type = fnCorrectValDropdown(form.Size2Type.value);
	var strSize2TypeText = form.size2_type.value;
	var strSize2Value = form.size2_value.value;
	var strSize2UOM = fnCorrectValDropdown(form.Size2UOM.value);
	var strSize3Type = fnCorrectValDropdown(form.Size3Type.value);
	var strSize3TypeText = form.size3_type.value;
	var strSize3Value = form.size3_value.value;
	var strSize3UOM = fnCorrectValDropdown(form.Size3UOM.value);
	var strSize4Type = fnCorrectValDropdown(form.Size4Type.value);
	var strSize4TypeText = form.size4_type.value;
	var strSize4Value = form.size4_value.value;
	var strSize4UOM = fnCorrectValDropdown(form.Size4UOM.value);
	var userComments = form.txt_LogReason.value;
	var typeSelectFl = false;
	//var strSerialNumFl = (form.serialNumFl.checked == true)?"Y":"N";  //Serial Number Needed checkbox

	if (strReqSteril == '80130') { // 80130 - Yes
		fnValidateDropDn('SterilMthod',lblSterilizationMethod);
	}
	
	// validate the size field.

	if(strIsDevice =='80130'){ // 80130 - Yes
		if(strSize1Type !='' && strSize1Type != 0){
			typeSelectFl = true;
			if (strSize1Type == '104047') {// 104047 Others, Specify
				fnValidateTxtFld('size1_type',message[5743]);
				if(strSize1Value != ''){
					Error_Details(message[5690]);
				}
				if(strSize1UOM != ''){
					Error_Details(message[5691]);
				}
			} else {
				fnValidateTxtFld('size1_value',message[5744] );
				fnValidateUOM(strSize1Type, strSize1UOM,message[5745] );
			}
		}
		

		//size 2
		if(strSize2Type !='' && strSize2Type != 0){
			typeSelectFl = true;
			if (strSize2Type == '104047') {// 104047 Others, Specify
				fnValidateTxtFld('size2_type',message[5746]);
				if(strSize2Value != ''){
					Error_Details(message[5692]);
				}
				if(strSize2UOM != ''){
					Error_Details(message[5693]);
				}	
			} else {
				fnValidateTxtFld('size2_value',message[5747]);
				fnValidateUOM(strSize2Type, strSize2UOM,message[5748]);
			}
		}
		//size 3
		if(strSize3Type !='' && strSize3Type != 0){
			typeSelectFl = true;
			if (strSize3Type == '104047') {// 104047 Others, Specify
				fnValidateTxtFld('size3_type',message[5749]);
				if(strSize3Value != ''){
					Error_Details(message[5694]);
				}
				if(strSize3UOM != ''){
					Error_Details(message[5695]);
				}	
			} else {
				fnValidateTxtFld('size3_value',message[5750]);
				fnValidateUOM(strSize3Type, strSize3UOM,message[5751]);
			}
		}
		//size 4
		if(strSize4Type !='' && strSize4Type != 0){
			typeSelectFl = true;
			if (strSize4Type == '104047') {// 104047 Others, Specify
				fnValidateTxtFld('size4_type',message[5752]);
				if(strSize4Value != ''){
					Error_Details(message[5696]);
				}
				if(strSize4UOM != ''){
					Error_Details(message[5697]);
				}
			} else {
				fnValidateTxtFld('size4_value',message[5753]);
				fnValidateUOM(strSize4Type, strSize4UOM,message[5754]);
			}
		}
		
	}

	if(strIsDevice =='80130' && !typeSelectFl){
		Error_Details(message[5698]);
	}
	if(strSize1Type!= '104047'){
	fnValidateSizeValue(strSize1Value,message[5755]);
	}
	if(strSize2Type!= '104047'){
	fnValidateSizeValue(strSize2Value,message[5756]);
	}
	if(strSize3Type!= '104047'){
	fnValidateSizeValue(strSize3Value,message[5757]);
	}
	if(strSize4Type!= '104047'){
	fnValidateSizeValue(strSize4Value,message[5758]);
	}

	
	
	if(strSize1Value !=''){
		round1Val = Math.round(strSize1Value * 100) / 100;
		// roundVal check
		if ((round1Val == 0 && strSize1Value != 0) || strSize1Value == 0){
			Error_Details(message[5699]);
		}
		strSize1Value = round1Val;
	}	
	if(strSize2Value !=''){
		round2Val = Math.round(strSize2Value * 100) / 100;
		// roundVal check
		if ((round2Val == 0 && strSize2Value != 0) || strSize2Value == 0){
			Error_Details(message[5700]);
		}
		strSize2Value = round2Val;
	}
	if(strSize3Value !=''){
		round3Val = Math.round(strSize3Value * 100) / 100;
		// roundVal check
		if ((round3Val == 0 && strSize3Value != 0) || strSize3Value == 0){
			Error_Details(message[5701]);
		}
		strSize3Value = round3Val;
	}
	if(strSize4Value !='' && strSize4Value ==0){
		round4Val = Math.round(strSize4Value * 100) / 100;
		// roundVal check
		if ((round4Val == 0 && strSize4Value != 0) || strSize4Value == 0){
			Error_Details(message[5702]);
		}
		strSize4Value = round4Val;
	}
	
	// to form the part attribute string
	strinputStr = "104460^" + strContainsLatex + "|104461^" + strReqSteril
			+ "|104462^" + strSterilMthod + "|104463^" + strIsDevice
			+ "|104464^" + strSize1Type + "|104465^" + strSize1TypeText
			+ "|104466^" + strSize1Value + "|104467^" + strSize1UOM
			+ "|104468^" + strSize2Type + "|104469^" + strSize2TypeText
			+ "|104470^" + strSize2Value + "|104471^" + strSize2UOM
			+ "|104472^" + strSize3Type + "|104473^" + strSize3TypeText
			+ "|104474^" + strSize3Value + "|104475^" + strSize3UOM
			+ "|104476^" + strSize4Type + "|104477^" + strSize4TypeText
			+ "|104478^" + strSize4Value + "|104479^" + strSize4UOM+"|";
			//+ "|104480^" + strSerialNumFl +"|";

	if (ErrorCount > 0) {
		form.btn_submit.disabled = false;
		Error_Show();
		Error_Clear();
		return false;
	}
	fnEnableDisableSizeField (form, false);
	form.haction.value = 'Save';
	form.strinputStr.value = strinputStr;
	form.submit();
}
// this function used to open the PD print screen
function fnPDPrint(form) {
	var strinputStr = "";
	var strPartNum = form.partNumber.value;
	var strprintPrjId = form.printProjectId.value;
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	windowOpener(
			"/gmUDIProductDevelopment.do?method=pdUDISetupPrint&partNumStr="
					+ strPartNum + "&partNumber=" + strPartNum + "&printProjectId=" + strprintPrjId, "Print",
			"resizable=yes,scrollbars=yes,top=200,left=300,width=970,height=700");
}
// this fucntion used to validate the UOM dropdown filed.
function fnValidateUOM(sizeType, sizeUOM, appendVal) {
	if (sizeType == '' || sizeType == 0) {
		return true;
	}
	// 104032 Circumference
	// 104033 Depth
	// 104035 Outer Diameter
	// 104036 Height
	// 104037 Length
	// 104038 Lumen/Inner Diameter
	// 104041 Width
	// 104044 Pore Size
	if (sizeType == 104032 || sizeType == 104033 || sizeType == 104035
			|| sizeType == 104036 || sizeType == 104037 || sizeType == 104038
			|| sizeType == 104041 || sizeType == 104044) {
		fnLoopArray(arrayLength, sizeUOM, appendVal);
	}
	// 104045 Area/Surface Area
	if (sizeType == 104045) {
		fnLoopArray(arrayArea, sizeUOM, appendVal);
	}
	// 104042 Weight
	if (sizeType == 104042) {
		fnLoopArray(arrayWeight, sizeUOM, appendVal);
	}
	// 104040 Total Volume
	if (sizeType == 104040) {
		fnLoopArray(arrayTotalVol, sizeUOM, appendVal);
	}
	// 104034 Catheter Gauge
	// 104039	Needle Gauge
	if (sizeType == 104034 || sizeType == 104039) {
		fnLoopArray(arrayGauge, sizeUOM, appendVal);
	}
	// 104046 Angle
	if (sizeType == 104046) {
		fnLoopArray(arrayAngle, sizeUOM, appendVal);
	}
	// 104043 Pressure
	if (sizeType == 104043) {
		fnLoopArray(arrayPressure, sizeUOM, appendVal);
	}
}
// this fucntion used to verify the correct UOM type
function fnLoopArray(arrayObj, codeId, appendVal) {
	var avilableFl = true;
	if ((codeId != '' || codeId != '0') && arrayObj.length != 0) {
		avilableFl = false;
		for ( var i = 0; i < arrayObj.length; i++) {
			arrayObjCol = arrayObj[i];
			arrayObjColArr = arrayObjCol.split(',');
			arrayCodeId = arrayObjColArr[0];
			arrayCodeNm = arrayObjColArr[1];
			if (arrayCodeId == codeId) {
				avilableFl = true;
				break;
			}
		}
	}
	if (!avilableFl) {
		Error_Details(Error_Details_Trans(message[5703],appendVal));
	}
	return avilableFl;
}
// this function used to validate the numbers
function fnValidateSizeValue(fieldVal, errorStr) {
	var wrongVal = '';
	if (fieldVal != '') {
		wrongVal = fieldVal.replace(/-?[.0-9]+/g, ''); // value text box
		if (wrongVal.length > 0 || isNaN (fieldVal)) {
			Error_Details(errorStr);
		}
	}
}
// this fucntion used to add the user array data
function putMyArray(val, arrayObjNm) {
	if (arrayObjNm == 'Length') {
		arrayLength.push(val);
	} else if (arrayObjNm == 'Area') {
		arrayArea.push(val);
	} else if (arrayObjNm == 'Weight') {
		arrayWeight.push(val);
	} else if (arrayObjNm == 'TotalVolume') {
		arrayTotalVol.push(val);
	} else if (arrayObjNm == 'Gauge') {
		arrayGauge.push(val);
	} else if (arrayObjNm == 'Angle') {
		arrayAngle.push(val);
	} else if (arrayObjNm == 'Pressure') {
		arrayPressure.push(val);
	}

}
// this function used to set the height of PD tab.
function fnPdOnLoad(){
	var form = document.getElementsByName('frmUDIProductDevelopment')[0];// To get the form valuealert(form);
	if(parent.window.document.getElementById("ajaxdivcontentarea") != undefined){
  		parent.window.document.getElementById("ajaxdivcontentarea").style.height = "900";
  		parent.window.document.getElementById("ajaxdivcontentarea").style.width = "960";
  	}
	// size filed check
	var deviceVal = form.IsDevice.value;
	if(deviceVal !='80130'){ // 80130 - Yes
		fnEnableDisableSizeField (form, true);
	}
	var strReqSteril = form.ReqSteril.value;
	var sterileFlVal = form.hSterileFl.value;
	
	if(sterileFlVal == 'Y'){
		if(form.ReqSteril.value == '0' || form.ReqSteril.value == ''){
			form.ReqSteril.value = 80131;
		}
		form.ReqSteril.disabled = true;
	}
	
	fnEnableTextField (form.Size1Type.value, form.size1_type);
	fnEnableTextField (form.Size2Type.value, form.size2_type);
	fnEnableTextField (form.Size3Type.value, form.size3_type);
	fnEnableTextField (form.Size4Type.value, form.size4_type);
	
	if(form.Size1Type.value == 0){
	form.size1_value.disabled = true;
	}else if(form.Size1Type.value == '104047')
	{
	form.size1_value.disabled = true;
	form.Size1UOM.disabled = true;
	}
	if(form.Size2Type.value == 0){
	form.size2_value.disabled = true;
	}else if(form.Size2Type.value == '104047')
	{
	form.size2_value.disabled = true;
	form.Size2UOM.disabled = true;
	}
	if(form.Size3Type.value == 0){
	form.size3_value.disabled = true;
	}else if(form.Size3Type.value == '104047')
	{
	form.size3_value.disabled = true;
	form.Size3UOM.disabled = true;
	}
	if(form.Size4Type.value == 0){
	form.size4_value.disabled = true;
	}else if(form.Size4Type.value == '104047')
	{
	form.size4_value.disabled = true;
	form.Size4UOM.disabled = true;
	}
}

function fnValidateProduct(value){	
	if (TRIM(value) != ''){
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/GmPartNumAjaxServlet?strOpt=CHECKPARTNUM&partNum='+ value + '&ramdomId=' + Math.random());
		dhtmlxAjax.get(ajaxUrl, fnvalidateField);
		return false;		
	}
}

function fnvalidateField(loader){
	var response = loader.xmlDoc.responseText;
	var parentform = document.getElementsByName('frmPartSetup')[0];
	var form = document.getElementsByName('frmPartSetup')[1];// To get the form value
	if(form == undefined){
		form = parentform;
	}
	var pnum = TRIM(parentform.Txt_PartNum.value);
	var masterProd = TRIM(form.Txt_MastProd.value);
	if(response != null){
		if(response == '0' || pnum == masterProd){
			Error_Details(message[5704]);			
		}
	}
	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}
}
// this function used to enable/disable the size field
function fnChangeDeviceSize(form, obj){
	var objVal = obj.value;
	if(objVal == 80130){ // Yes
		fnEnableDisableSizeField (form, false);
	}else{
		fnEnableDisableSizeField (form, true);
	}
}
function fnEnableDisableSizeField(form,disableFlag){
	// size 1
	form.Size1Type.disabled = disableFlag;
	form.Size1UOM.disabled = disableFlag;
	// size 2
	form.Size2Type.disabled = disableFlag;
	form.Size2UOM.disabled = disableFlag;
	// size 3
	form.Size3Type.disabled = disableFlag;
	form.Size3UOM.disabled = disableFlag;
	// size 4
	form.Size4Type.disabled = disableFlag;
	form.Size4UOM.disabled = disableFlag;
	
	// if device selected as No then to set the all values as empty
	if(disableFlag){
		form.Size1Type.value = 0;
		form.size1_type.value = '';
		form.size1_value.value = '';
		form.Size1UOM.value = 0;
		form.size1_type.disabled = disableFlag;
		form.size1_value.disabled = disableFlag;
		// size 2
		form.Size2Type.value = 0;
		form.size2_type.value = '';
		form.size2_value.value = '';
		form.Size2UOM.value = 0;
		form.size2_type.disabled = disableFlag;
		form.size2_value.disabled = disableFlag;
		// size 3
		form.Size3Type.value = 0;
		form.size3_type.value = '';
		form.size3_value.value = '';
		form.Size3UOM.value = 0;
		form.size3_type.disabled = disableFlag;
		form.size3_value.disabled = disableFlag;
		// size 4
		form.Size4Type.value = 0;
		form.size4_type.value = '';
		form.size4_value.value = '';
		form.Size4UOM.value = 0;
		form.size4_type.disabled = disableFlag;
		form.size4_value.disabled = disableFlag;
	}
}
// This function used to to set the UOM filed value based on Type
function fnChangeSizeType(form,typeObj, uomTypeNm, sizeTypeNm, sizeValueNm){
	// 104032 Circumference
	// 104033 Depth
	// 104035 Outer Diameter
	// 104036 Height
	// 104037 Length
	// 104038 Lumen/Inner Diameter
	// 104041 Width
	// 104044 Pore Size
	var sizeType = typeObj.value;
	var uomObj = eval('document.all.'+uomTypeNm);
	var sizeTextObj = eval('document.all.'+sizeTypeNm);
	var sizeValueObj = eval('document.all.'+sizeValueNm);
	if(sizeType == 0){
		uomObj.options.length = 0;
		uomObj.options[0] = new Option("[Choose One]","0");
		sizeValueObj.value = '';
		sizeValueObj.disabled = true;
		return true;
	}
	if (sizeType == 104032 || sizeType == 104033 || sizeType == 104035
			|| sizeType == 104036 || sizeType == 104037 || sizeType == 104038
			|| sizeType == 104041 || sizeType == 104044) {
		fnSetUomValues(arrayLength, uomObj);
	}else
	// 104045 Area/Surface Area
	if (sizeType == 104045) {
		fnSetUomValues(arrayArea, uomObj);
	}else
	// 104042 Weight
	if (sizeType == 104042) {
		fnSetUomValues(arrayWeight, uomObj);
	}else
	// 104040 Total Volume
	if (sizeType == 104040) {
		fnSetUomValues(arrayTotalVol, uomObj);
	}else
	// 104034 Catheter Gauge
	// 104039	Needle Gauge
	if (sizeType == 104034 || sizeType == 104039) {
		fnSetUomValues(arrayGauge, uomObj);
	}else
	// 104046 Angle
	if (sizeType == 104046) {
		fnSetUomValues(arrayAngle, uomObj);
	}else
	// 104043 Pressure
	if (sizeType == 104043) {
		fnSetUomValues(arrayPressure, uomObj);
	}
	// 104047 Other
if (form.Size1Type.value == 104047) {
	form.size1_value.disabled = true;
	form.Size1UOM.disabled = true;
}else if (form.Size1Type.value != 104047) {
	form.size1_value.disabled = false;
	form.Size1UOM.disabled = false;
	}
if (form.Size2Type.value == 104047) {
	form.size2_value.disabled = true;
	form.Size2UOM.disabled = true;
}else if (form.Size2Type.value != 104047) {
	form.size2_value.disabled = false;
	form.Size2UOM.disabled = false;
	}
if (form.Size3Type.value == 104047) {
	form.size3_value.disabled = true;
	form.Size3UOM.disabled = true;
}else if (form.Size3Type.value != 104047) {
	form.size3_value.disabled = false;
	form.Size3UOM.disabled = false;
    }
if (form.Size4Type.value == 104047) {
	form.size4_value.disabled = true;
	form.Size4UOM.disabled = true;
}else if (form.Size4Type.value != 104047) {
	form.size4_value.disabled = false;
	form.Size4UOM.disabled = false;
    }
	fnEnableTextField (sizeType, sizeTextObj);
}
// This functon used to set the UOM values
function fnSetUomValues (arrObject, uomObject){
	uomObject.options.length = 0;
	uomObject.options[0] = new Option("[Choose One]","0");
	for ( var i = 0; i < arrObject.length; i++) {
		arrayObjCol = arrObject[i];
		arrayObjColArr = arrayObjCol.split(',');
		arrayCodeId = arrayObjColArr[0];
		arrayCodeNm = arrayObjColArr[1];
		uomObject.options[i+1] = new Option(arrayCodeNm,arrayCodeId);
	}
}
// This function used to enable/disable the size text filed.
function fnEnableTextField(type, sizrTxtObj){
	if (type == 104047){ // 104047 Other
		sizrTxtObj.disabled = false;
	}else{
		sizrTxtObj.value = '';
		sizrTxtObj.disabled = true;
	}
}
//
function fnChangeReqSteril (form, reqObj){
	if(reqObj.value == 80130){ // Yes
		form.SterilMthod.disabled = false;
	}else{
		form.SterilMthod.value = 0;
		form.SterilMthod.disabled = true;
	}
}

//onLoad function for Regulator tab
function fnRegulatoryLoad(){

	var form = document.getElementsByName('frmPartParameterDtls')[0];// To get the form valuealert(form);
	var strUsRegPathway = fnCorrectValDropdown(form.cbo_Us_Reg_Pathway);
	var strCETechFileNo =  form.txt_Ce_Tech_File_Num;
	fnDisableAprrovalDate(form);
	fnSetCETechFileRev(form, strCETechFileNo);
	//function called for disbaled field after submit
	fnSetNTFReport(form, strUsRegPathway);
	
	if(parent.window.document.getElementById("ajaxdivcontentarea") != undefined){
  		parent.window.document.getElementById("ajaxdivcontentarea").style.height = "990";
  		parent.window.document.getElementById("ajaxdivcontentarea").style.width = "960";
	}
	if (ErrorCount > 0){
			Error_Show();
			Error_Clear();
			return false;
	}
}
// this function used to show the mandatory filed in DHR tab
function fnOnLoadDHRTab() {
	var form = document.getElementsByName('frmPartParameterDtls')[0];
	fnShowMandatoryField(form.diNumberFl.checked);
	if (parent.window.document.getElementById("ajaxdivcontentarea") != undefined) {
		parent.window.document.getElementById("ajaxdivcontentarea").style.height = "620";
		parent.window.document.getElementById("ajaxdivcontentarea").style.width = "960";
	}
}
// this functoin to call the DI Number checked/unchecked time
function fnDoCheck(form, obj) {
	fnShowMandatoryField(obj.checked);
}
// this function used to show/hide the mandatory (*) filed
function fnShowMandatoryField(showFl) {
	var issueAgencyMandatoryObj = document.getElementById("issueAgencyMandatory");
	var udiEtchMandatoryObj = document.getElementById("udiEtchMandatory");
	var diOnlyMandatoryObj = document.getElementById("diOnlyMandatory");
	var piOnlyMandatoryObj = document.getElementById("piOnlyMandatory");
	var bulkPackMandatoryObj = document.getElementById("bulkPackMandatory");
	if (showFl) { // true
		issueAgencyMandatoryObj.style.display = 'inline';
		udiEtchMandatoryObj.style.display = 'inline';
		diOnlyMandatoryObj.style.display = 'inline';
		piOnlyMandatoryObj.style.display = 'inline';
		bulkPackMandatoryObj.style.display = 'inline';
	} else {
		issueAgencyMandatoryObj.style.display = 'none';
		udiEtchMandatoryObj.style.display = 'none';
		diOnlyMandatoryObj.style.display = 'none';
		piOnlyMandatoryObj.style.display = 'none';
		bulkPackMandatoryObj.style.display = 'none';
	}
}
//Function will call on page load. It will append companyInfo in each tab href and initiate ajaxtab.
function fnAjaxTabLoad(){
	fnAjaxTabAppendCmpInfo("#maintab");
	fnMainTab();
}
//initiate ajaxtab
function fnMainTab(){
	var maintab=new ddajaxtabs("maintab", "ajaxdivcontentarea");				
	maintab.setpersist(false);								
	maintab.init();
	// To call the function on page load
	maintab.onajaxpageload=function(pageurl)
    {
    	if (pageurl.indexOf("GmPartNumServlet")!=-1){
    		fnAddCompanyParams();
    		fnLoad();
    	}
    	// to set the auto height for all the tab
    	if(document.getElementById("ajaxdivcontentarea") != undefined){
  			document.getElementById("ajaxdivcontentarea").style.height = "auto";
		}
		fnHtmlEditor();
		// to reload the JSP page.
		var clickedVal = document.all.hcurrentTab.value;
		if (clickedVal != '' && clickedVal == 'partdetails') {
			var formObj = document.getElementsByName('frmPartSetup')[0];// To get form obj
			fnPartReload(formObj);
		}
	}
}	
