

        function showtocnew() // Called to maximise the hidden Left Navigation Menu
        {

         top.fstMain.cols = "200,*";
         top.TopFrame.document.all("showtoc").style.display = "None";
         top.TopFrame.document.all("hideToc").style.display = "block";
        }//End of showtoc()

        function hidetocnew()
        {
        	top.fstMain.cols = "0,*";
        	top.TopFrame.document.all("showtoc").style.display = "block";
        	top.TopFrame.document.all("hideToc").style.display = "none";
        	
        }

        //This function is used to populate option from JSON
        function fnPopulateOptionsNew(dropDownObj,strJSON,codeId,codeNm,optionId,defaultId){
        	var resJSONObj = strJSON;//JSON.stringify(strJSON);
        	var bFlag = false;
        	dropDownObj.options.length = 0;
        	for(i= 0; i<resJSONObj.length;i++){
        		var itemJSON = resJSONObj[i];
        		dropDownObj.options[i] = new Option(itemJSON[codeNm], itemJSON[codeId]);
        		dropDownObj.options[i].id = itemJSON[optionId];
        		bFlag = ( itemJSON[codeId] == defaultId)?true:bFlag;
        	}
        	// for pmt-32727(Plant Defaults incorrectly) below condition is added!
        	if(codeId == 'COMPANYID' && bFlag && defaultId != null && defaultId != ""){
        	dropDownObj.value = defaultId;
        	}
        }

        
	function fnPageLoadNew()
	{ 
		
	  	fnComapanyAjaxNew();
	  	
	}
  	


	//This function is used to Hide the Company,Plant dropdown and country flag
	function fnToggleCompanyNew(hideCompanyFl){
		 
		 if(hideCompanyFl == 'Y'){	
			 document.getElementById("Company").style.display = 'none';
			 document.getElementById("Plant").style.display = 'none'; 	
			 //document.getElementById("country_flag").style.display = 'none';
		 }else{
			 document.getElementById("Company").style.display = 'block';
			 document.getElementById("Plant").style.display = 'block';
		 }
	}

//This is Ajax function to populate company option in drop down.
function fnComapanyAjaxNew(){ 
	var defaultCompId = "";
	dhtmlxAjax.get('/gmCorporateAjax.do?method=loadCompanyList&'+fnAppendCompanyInfo()+'&randomId='+Math.random(),function(loader){
		
		var compObj = document.getElementById("Cbo_Company");
		var resJSONObj = $.parseJSON(loader.xmlDoc.responseText); 
		if(!(top.gCompanyInfo == undefined || top.gCompanyInfo == null)){
		 defaultCompId =top.gCompanyInfo.cmpid; 
		}
		//to create duplicate tab icon
		if(resJSONObj != undefined){
			document.getElementById("logo-td").style.visibility ="visible";
			var logoTdObj = document.getElementById("logo-td");
			logoTdObj.innerHTML = '<a href="#" onclick="fnDuplicateTab();"  title="Click to Create Duplicate Tab">'+logoTdObj.innerHTML+'</a>';
			
		}
		fnPopulateOptionsNew(compObj,resJSONObj,"COMPANYID","COMPANYNM", "CMPJSFMT", defaultCompId);
		selIdx = document.getElementById("Cbo_Company").selectedIndex;
		fnPlantAjaxNew();
		//top.gCompJSDateFmt = compObj.options[compObj.selectedIndex].id;
		fnShowCountryFlagNew(compObj.options[compObj.selectedIndex].value);
		readTextFileNew("/header_message.txt");
	});
}
var selIdx;
function fnChangeCompanyNew(){
		if(confirm("The unsaved data will be lost. Do you want to continue?")){
			//document.getElementById("country_flag").src="";
			var compObj = document.getElementById("Cbo_Company");
			selIdx = compObj.selectedIndex;
			top.gCompanyInfo.cmpid = compObj.value;
			top.gCompJSDateFmt = compObj.options[compObj.selectedIndex].id;
			var prevUserGrpId =   document.getElementById("userGroupId").value; //"001_303012_FAV"; //prevUserGrpObj.value;
			var prevUserGrpNm = document.getElementById("userGroupName").value;  //"My Favorites"; //prevUserGrpObj[prevUserGrpObj.selectedIndex].text;
			var URL="/ajaxcalls/user/GmCompanyPage.html?companyId="+top.gCompanyInfo.cmpid+"&companynm="+compObj.options[compObj.selectedIndex].text+"&ramdomId="+Math.random();
			window.frames['RightFrame'].location = URL; 
			var leftURL  = "/gmUserNavigationNew.do?userGroup="+encodeURIComponent(prevUserGrpId)+"&userGrpName="+encodeURIComponent(prevUserGrpNm)+"&strPgToLoad=null&"+fnAppendCompanyInfo(); 
			$("#LeftFrame").load(leftURL);
			fnShowCountryFlagNew(compObj.value);
			readTextFileNew("/header_message.txt");
			fnPlantAjaxNew();
		}
		else {
			document.getElementById("Cbo_Company").selectedIndex = selIdx;
			return false;
		}
	}

//This is Ajax function to populate plant option based on company in plant drop down.
function fnPlantAjaxNew(){
	dhtmlxAjax.get('/gmCorporateAjax.do?method=loadPlantList&'+fnAppendCompanyInfo()+'&randomId='+Math.random(),function(loader){
		var plantObj = document.getElementById("Cbo_Plant");
		var resJSONObj = $.parseJSON(loader.xmlDoc.responseText);	
		fnPopulateOptionsNew(plantObj,resJSONObj,"PLANTID","PLANTNM", "PLANTCD", top.gCompanyInfo.plantid);
		top.gCompanyInfo.plantid = plantObj.value;
		document.getElementById("Cbo_Plant").value = plantObj.value;
	});
}

var selPlantIdx;
var selCompIdx;
//This function will call when plant drop down value change.
function fnChangePlantNew(){ 
	 if(confirm("The unsaved data will be lost. Do you want to continue?")){
			var compObj = document.getElementById("Cbo_Company");
			var plantObj  = document.getElementById("Cbo_Plant");
			selCompIdx = compObj.selectedIndex;
			selPlantIdx = plantObj.selectedIndex;
			top.gCompanyInfo.cmpid = compObj.value;
			top.gCompJSDateFmt = compObj.options[compObj.selectedIndex].id;
			top.gCompanyInfo.plantid = plantObj.value;
			var prevUserGrpId =   document.getElementById("userGroupId").value; //"001_303012_FAV"; //prevUserGrpObj.value;
			var prevUserGrpNm = document.getElementById("userGroupName").value;  //"My Favorites"; //prevUserGrpObj[prevUserGrpObj.selectedIndex].text;	
			var URL="/ajaxcalls/user/GmCompanyPage.html?companyId="+top.gCompanyInfo.cmpid+"&companynm="+compObj.options[compObj.selectedIndex].text+"&ramdomId="+Math.random(); 
			window.frames['RightFrame'].location = URL; 
			var leftURL  = "/gmUserNavigationNew.do?userGroup="+encodeURIComponent(prevUserGrpId)+"&userGrpName="+encodeURIComponent(prevUserGrpNm)+"&strPgToLoad=null&"+fnAppendCompanyInfo(); 
			$("#LeftFrame").load(leftURL);
	 }else{
			document.getElementById("Cbo_Company").selectedIndex = selCompIdx;
			document.getElementById("Cbo_Plant").selectedIndex = selPlantIdx;
			top.gCompanyInfo.plantid = document.getElementById("Cbo_Plant").value;
			return false;
		}
}

/* This function will show the Country flag based on the Country ID passed in.
 * 
 */
function fnShowCountryFlagNew(companyid){
	var rawFile = new XMLHttpRequest();
   // rawFile.open("GET", "/images/"+companyid+"_country_flag.png",false);
    
    //This code will show fire only when the Country flag exist.
    rawFile.onreadystatechange = function ()
    {
        if(rawFile.readyState == 4)
        {
            if(rawFile.status == 200 || rawFile.status == 0)
            {
            	//document.getElementById("country_flag").src="/images/"+companyid+"_country_flag.png";
            }
        }
    }
    //rawFile.send(null);
	
}

/* This Function will read the txt file available in Apache.
 * This file will have information like Test/Stage Environment,which will help the team know in which environment they are doing the testing. 
 * 
 */
function readTextFileNew(file)
{
	
	var rawFile = new XMLHttpRequest();
    rawFile.open("GET", file,false);
    document.getElementById("header_message").innerHTML = '';
    rawFile.onreadystatechange = function ()
    {
        if(rawFile.readyState == 4)
        {
            if(rawFile.status == 200 || rawFile.status == 0)
            {
                var allText = rawFile.responseText;
                document.getElementById("header_message").innerHTML = allText;
            }
        }
    }
    rawFile.send(null);
}

/* This function will hide the right side toggle .
 * 
 */
$(document).on("click", function(event){
    var $trigger = $(".right-side-toggle"); //alert(event.target);
    if($trigger !== event.target && !$trigger.has(event.target).length){
       $(".right-sidebar").removeClass("shw-rside");
    }           
});

//This function is used to create the duplicate tab
function fnDuplicateTab(){
		var topObj = getObjTop();
		window.open('/GmLogonServlet?hAction=DUP&moc='+topObj.gCompanyInfo.cmpid+'&rap='+topObj.gCompanyInfo.partyid+'&alp='+topObj.gCompanyInfo.plantid+'&tmf='+encodeURIComponent(topObj.gCompJSDateFmt)+'&ltp='+encodeURIComponent(topObj.gPageToLoad)+'&srn=newlook');
}
