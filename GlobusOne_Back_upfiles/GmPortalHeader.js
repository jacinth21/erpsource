//This function will call when company drop down value change.
//option's id will have the JS date format.
//The global gCompanyInfo's cmpid will be reset by selected company id.
//The global variable gmCompJSDateFmt will be reset by selected company's JS date format.
var selIdx;
function fnChangeCompany(){
	if(confirm("The unsaved data will be lost. Do you want to continue?")){
		document.getElementById("country_flag").src="";
		var compObj = document.getElementById("Cbo_Company");
		selIdx = compObj.selectedIndex;
		top.gCompanyInfo.cmpid = compObj.value;
		top.gCompJSDateFmt = compObj.options[compObj.selectedIndex].id;
		fnPlantAjax();
		var prevUserGrpObj = parent.LeftFrame.document.all("userGroup");
		var prevUserGrpId = prevUserGrpObj.value;
		var prevUserGrpNm = prevUserGrpObj[prevUserGrpObj.selectedIndex].text;
		parent.LeftFrame.location.href = "/gmUserNavigation.do?userGroup="+prevUserGrpId+"&userGrpName="+prevUserGrpNm+"&strPgToLoad=null&"+fnAppendCompanyInfo();
		parent.RightFrame.location.href = "/ajaxcalls/user/GmCompanyPage.html?companyId="+top.gCompanyInfo.cmpid+"&companynm="+compObj.options[compObj.selectedIndex].text+"&ramdomId="+Math.random();
		
		fnShowCountryFlag(compObj.value);
		readTextFile("/header_message.txt");
	}
	else {
		document.getElementById("Cbo_Company").selectedIndex = selIdx;
		return false;
	}
}

/* This function will show the Country flag based on the Country ID passed in.
 * 
 */
function fnShowCountryFlag(companyid){
	var rawFile = new XMLHttpRequest();
    rawFile.open("GET", "/images/"+companyid+"_country_flag.png",false);
    
    //This code will show fire only when the Country flag exist.
    rawFile.onreadystatechange = function ()
    {
        if(rawFile.readyState == 4)
        {
            if(rawFile.status == 200 || rawFile.status == 0)
            {
            	document.getElementById("country_flag").src="/images/"+companyid+"_country_flag.png";
            }
        }
    }
    rawFile.send(null);
	
}

/* This Function will read the txt file available in Apache.
 * This file will have information like Test/Stage Environment,which will help the team know in which environment they are doing the testing. 
 * 
 */
function readTextFile(file)
{
	
	var rawFile = new XMLHttpRequest();
    rawFile.open("GET", file,false);
    document.getElementById("header_message").innerHTML = '';
    rawFile.onreadystatechange = function ()
    {
        if(rawFile.readyState == 4)
        {
            if(rawFile.status == 200 || rawFile.status == 0)
            {
                var allText = rawFile.responseText;
                document.getElementById("header_message").innerHTML = allText;
            }
        }
    }
    rawFile.send(null);
}
var selPlantIdx;
var selCompIdx;
//This function will call when plant drop down value change.
function fnChangePlant(){
	 //top.gCompanyInfo.plantid = document.getElementById("Cbo_Plant").value;	 
	 if(confirm("The unsaved data will be lost. Do you want to continue?")){
			var compObj = document.getElementById("Cbo_Company");
			var plantObj  = document.getElementById("Cbo_Plant");
			selCompIdx = compObj.selectedIndex;
			selPlantIdx = plantObj.selectedIndex;
			top.gCompanyInfo.cmpid = compObj.value;
			top.gCompJSDateFmt = compObj.options[compObj.selectedIndex].id;
			top.gCompanyInfo.plantid = plantObj.value;
			var prevUserGrpObj = parent.LeftFrame.document.all("userGroup");
			var prevUserGrpId = prevUserGrpObj.value;
			var prevUserGrpNm = prevUserGrpObj[prevUserGrpObj.selectedIndex].text;
			parent.LeftFrame.location.href = "/gmUserNavigation.do?userGroup="+prevUserGrpId+"&userGrpName="+prevUserGrpNm+"&strPgToLoad=null&"+fnAppendCompanyInfo();
			parent.RightFrame.location.href = "/ajaxcalls/user/GmCompanyPage.html?companyId="+top.gCompanyInfo.cmpid+"&companynm="+compObj.options[compObj.selectedIndex].text+"&ramdomId="+Math.random();
		}else{
			document.getElementById("Cbo_Company").selectedIndex = selCompIdx;
			document.getElementById("Cbo_Plant").selectedIndex = selPlantIdx;
			top.gCompanyInfo.plantid = document.getElementById("Cbo_Plant").value;
			return false;
		}
}

//This function will call on portal header page load
function fnPageLoad(){
	fnComapanyAjax();
	
	}

//This is Ajax function to populate company option in drop down.
function fnComapanyAjax(){
	var defaultCompId = "";
	dhtmlxAjax.get('/gmCorporateAjax.do?method=loadCompanyList&'+fnAppendCompanyInfo()+'&randomId='+Math.random(),function(loader){
		
		var compObj = document.getElementById("Cbo_Company");
		var resJSONObj = $.parseJSON(loader.xmlDoc.responseText);
		if(!(top.gCompanyInfo == undefined || top.gCompanyInfo == null)){
		 defaultCompId =top.gCompanyInfo.cmpid;
		}
		//to create duplicate tab icon
		if(resJSONObj != undefined){
			document.getElementById("logo-td").style.visibility ="visible";
			var logoTdObj = document.getElementById("logo-td");
			logoTdObj.innerHTML = '<a href="#" onclick="fnDuplicateTab();"  title="Click to Create Duplicate Tab">'+logoTdObj.innerHTML+'</a>';
			
		}
		fnPopulateOptions(compObj,resJSONObj,"COMPANYID","COMPANYNM", "CMPJSFMT", defaultCompId);
		selIdx = document.getElementById("Cbo_Company").selectedIndex;
		fnPlantAjax();
		top.gCompJSDateFmt = compObj.options[compObj.selectedIndex].id;
		fnShowCountryFlag(compObj.options[compObj.selectedIndex].value);
		readTextFile("/header_message.txt");
	})
	
	
	
}

//This is Ajax function to populate plant option based on company in plant drop down.
function fnPlantAjax(){
	dhtmlxAjax.get('/gmCorporateAjax.do?method=loadPlantList&'+fnAppendCompanyInfo()+'&randomId='+Math.random(),function(loader){
		var plantObj = document.getElementById("Cbo_Plant");
		
		var resJSONObj = $.parseJSON(loader.xmlDoc.responseText);
		fnPopulateOptions(plantObj,resJSONObj,"PLANTID","PLANTNM", "PLANTCD", top.gCompanyInfo.plantid);
		top.gCompanyInfo.plantid = plantObj.value;

	});
}

//This function is used to populate option from JSON
function fnPopulateOptions(dropDownObj,strJSON,codeId,codeNm,optionId,defaultId){
	var resJSONObj = strJSON;//JSON.stringify(strJSON);
	var bFlag = false;
	dropDownObj.options.length = 0;
	for(i= 0; i<resJSONObj.length;i++){
		var itemJSON = resJSONObj[i];
		dropDownObj.options[i] = new Option(itemJSON[codeNm], itemJSON[codeId]);
		dropDownObj.options[i].id = itemJSON[optionId];
		bFlag = ( itemJSON[codeId] == defaultId)?true:bFlag;
	}
	// for pmt-32727(Plant Defaults incorrectly) below condition is added!
	if(codeId == 'COMPANYID' && bFlag && defaultId != null && defaultId != ""){
	dropDownObj.value = defaultId;
	}
}
//This function is used to create the duplicate tab
function fnDuplicateTab(){
	//if(multiCompFl){
		var topObj = getObjTop();
		window.open('/GmLogonServlet?hAction=DUP&moc='+topObj.gCompanyInfo.cmpid+'&rap='+topObj.gCompanyInfo.partyid+'&alp='+topObj.gCompanyInfo.plantid+'&tmf='+encodeURIComponent(topObj.gCompJSDateFmt)+'&ltp='+encodeURIComponent(topObj.gPageToLoad)+'&srn=oldlook');
	
	//}
}
