
<%
	/**********************************************************************************
	 * File		 		: Replacing the GmSSOLogin.jsp code in GmPortalExit for Keycloak Migration
	 * Desc		 		: This screen is used for the Keycloak Login Process
	 * Version	 		: 1.0 
	 * author			: Gomathi Palani
	 ************************************************************************************/
%>

<%

request.getRequestDispatcher("/GmLogonServlet").forward(request,response);
 
%>