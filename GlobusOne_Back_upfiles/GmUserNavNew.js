
function fnReloadNew(groupNm, groupId)
{  
	document.getElementById("userGroupId").value = groupId;
	document.getElementById("userGroupName").value = groupNm;
	val = groupId; //obj.value; 
	text = groupNm; //obj[obj.selectedIndex].text;
	sess = groupId; //obj[obj.selectedIndex].id;
	//Disable the Company,Plant dropdown is sales(100000000) module selected.
	if(val == '100000000' || val == '2005' || val == '2015'){
		fnToggleCompanyNew('Y');	
	}else{
		fnToggleCompanyNew('N');	
	}
	
	if(val == '100000142'){ // CRM PMT-10388
		top.location.href = "/GmLogonServlet?hAction=CRM";
	}
	else if(val != 2007)
	{
		var leftURL = "/gmUserNavigationNew.do?userGroup="+val+"&userGrpName='"+encodeURIComponent(text)+"'&strPgToLoad="+sess+"&"+fnAppendCompanyInfo() 
		$( "#LeftFrame" ).load(leftURL) ;

	}
	else
	{
		top.location.href = "/GmClinicalPortal.jsp?";
	}
}

function setSelectedId(functionId, functionName, access, isFolder,Stropt,url,sqid)
{ 
	document.getElementById("selectedFunctionId").value = functionId;
	document.getElementById("selectedFunctionName").value = functionName;
	document.getElementById("access").value = access;
	document.getElementById("isFolder").value = isFolder;	
	
	
	 var url = url;
	 var opt = Stropt;
	 var sqid = sqid;
	 var access = access;
	 var fnname = functionName;
	 var mobileurl = url;
	 //Hide the Company,Plant dropdown when sales modules links are loaded from My Favorites.
	 var hideCompanyFl = getParameterByName('hideCompany', url);
	 // When map the Task to module mapping screen called this function.
	 // At the time top frame not present (here checking condition)
		 if(url)
		{
				  if (access == 'true')
				 {
					  var vDelimiter= "&"; //(url.indexOf("?")>0)?"&":"?";
					  //Getting company info. as JSON string from GmEnterprisePortal.jsp and append to url.
					  url += vDelimiter + fnAppendCompanyInfo(); 
					  if (GLOBAL_ClientSysType == "IPAD")
					  {	
						   //Below condition is to enable the progress bar for the sales module - SpineIT - CRM : ipad 	
							if (hideCompanyFl == "Y") 
							{
								fnStartProgressNew('Y'); 
							}
							document.getElementById("RightFrame").src = "/GmPageControllerServlet?strPgToLoad="+url+"&strOpt="+opt ;
						  hidetoc();
					  } else if(opt == "GLOBUSMOBILEAPP"){//opt value GLOBUSMOBILEAPP needs to call GmMobileInterface.html to get the frame set and path for call the code location
						  	 fnSetUserInfo(mobileurl);//function to get the all UserInformation
						  	document.getElementById("RightFrame").src = "/GmMobileInterface.html";//set the screen in right frame location						   
					  } else
					  {
						//Below condition is to enable the progress bar for the sales module - SpineIT - Portal
							if (hideCompanyFl == "Y")
							{
							fnStartProgressNew('Y');
							}
							document.getElementById("RightFrame").src = "/GmPageControllerServlet?strPgToLoad="+url+"&strOpt="+opt ;
					  }
				 }
				 else
				 {
				 			Error_Details(Error_Details_Trans(message[10079],fnname));
							Error_Show();
							Error_Clear();
							return false;
				 }	
				  
	    } 
	 }

function fnRemFavNew() {
	  if (confirm("Do you want to remove your favourite?")) {
		  var funid=document.getElementById("selectedFunctionId").value;
			var grpid=document.getElementById("userGroupId").value;
			document.getElementById("strOpt").value = 'RemFav';
			 if(funid =="")
			 {
				  Error_Details(message[10083]);
				  Error_Show();
				  Error_Clear();
				  return false;
					
			 }
		  var leftURL = "\gmUserNavigationNew.do?grpId="+encodeURIComponent(grpid)+"&funId="+funid+"&strOpt=RemFav";
			 $( "#LeftFrame" ).load(leftURL);
	  }
	}

function fnFavPopNew(){
	var funid=document.getElementById("selectedFunctionId").value;
	var funnm=document.getElementById("selectedFunctionName").value;
	funnm = encodeURI(funnm);
	var isFolder = document.getElementById("isFolder").value;//(tree.getUserData(funid,"URL") == undefined)?true:false;
	var grpid=document.getElementById("userGroupId").value;
	document.getElementById("strOpt").value = 'AddFav';
	if(isFolder == true){
		  Error_Details(message[10080]);
		  Error_Show();
		  Error_Clear();
		  return false;
	}
	if(funid =="")
	 {
		  Error_Details(message[10081]);
		  Error_Show();
		  Error_Clear();
		  return false;
	 }
	 var access = document.getElementById("access").value;//tree.getUserData(funid,"ACCESS");
	 if (access != 'true')
	 {
		  Error_Details(message[10082]);
		  Error_Show();
		  Error_Clear();
		  return false;

	 }
	windowOpener("/gmUserNavigationNew.do?strOpt=RENAME&funId="+funid+"&funName="+funnm + '&grpId=' + grpid ,"RENAME","resizable=yes,scrollbars=no,top=150,left=150,width=370,height=150");
}

function fnAddFavNew(){	
	 var funnm=document.frmUserNavigationSetup.funName.value;
	 if(funnm.trim() == ''){
			document.getElementById("msg").innerHTML = "Please enter the Link Name";
			document.frmUserNavigationSetup.funName.value='';
			document.frmUserNavigationSetup.funName.focus();
			 return false;
	 }
	 var blVal = fnSkipSpecNew(document.frmUserNavigationSetup.funName);
	 if(blVal){
		 document.frmUserNavigationSetup.action = '\gmUserNavigationNew.do?grpId=' + grpid + '&funId=' + funid + '&funName=' + funnm +  '&strOpt=AddFav&randomId=' + Math.random();
		 document.frmUserNavigationSetup.submit();
	 }
}

function fnSkipSpecNew(obj) 
{
	var input = obj.value;
	var strSpecFound = input.replace(/[a-zA-Z->() 0-9]+/g,'');
	if(strSpecFound.length > 0){
		document.getElementById("msg").innerHTML = "Special characters are not allowed except ( -,>,() )";
		  obj.focus();
		  return false;
	}else{
		document.getElementById("msg").innerHTML = "";
		return true;
	}
}
