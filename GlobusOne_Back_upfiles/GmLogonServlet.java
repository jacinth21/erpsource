/**
 * 
 * @author $Author:$
 * @version $Revision:$ <br>
 *          $Date:$
 */

package com.globus.logon.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmSecurityBean;
import com.globus.common.crypt.AESEncryptDecrypt;
import com.globus.common.servlets.GmServlet;
import com.globus.common.util.GmCookieManager;
import com.globus.common.util.GmTokenManager;
import com.globus.it.beans.GmLdapBean;
import com.globus.logon.beans.GmLogonBean;
import com.globus.logon.beans.GmUserTokenBean;
import com.globus.valueobject.logon.GmUserVO;
import com.globus.webservice.common.resources.GmResource;
import java.util.Base64;
import com.globus.sales.event.beans.GmEventSetupBean;

import java.security.Principal;
import org.keycloak.KeycloakPrincipal;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.representations.IDToken;

public class GmLogonServlet extends GmServlet {
  /*
   * This is the service method of the servlet and it take two parameters i.e HttpServletRequest and
   * HttpServletResponse.
   */
  private static final String OWA_URL = "https://password.globusmedical.com";

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    String strServerIp = request.getRemoteHost();
    GmEventSetupBean gmEventSetupBean = new GmEventSetupBean(getGmDataStoreVO());

    //PMT-33439-Apache & PHP Changes on ERP server (3rd Set)
    //String strLocalServerName = InetAddress.getLocalHost().getHostName();
    String strLocalServerName = System.getProperty("HOST_NAME");
    log.debug("strLocalServerName"+strLocalServerName);//PMT-33439
    
    // log.debug(" Server InetAddress Name is " +strLocalServerName + " remote host " +
    // strServerIp);
    // log.debug("Loacale#################"+request.getLocale());
    String strClientIP = request.getHeader("X-FORWARDED-FOR");
    HashMap hmParam = new HashMap();
    AESEncryptDecrypt aesEncryptDecrypt = new AESEncryptDecrypt();
    GmSecurityBean gmSecurityBean = new GmSecurityBean();
    GmLogonBean gmLogonBean = new GmLogonBean(getGmDataStoreVO());
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
    GmCookieManager gmCookieManager = new GmCookieManager();
    String strAction = GmCommonClass.parseNull(request.getParameter("hAction"));
    if (strClientIP == null) {
      strClientIP = request.getRemoteAddr();
    }
    // //log.debug("strServerIp:"+strServerIp);
    HttpSession session = request.getSession(false);
    if (session != null && !strAction.equals("CRM") && !strAction.equals("DUP")) {
      session.invalidate();
    }
    session = request.getSession(true);
    String strPrimaryCmpyId = "";
    String strGen = null;
    String strDispatchTo = null; // path for destination jsp.
    String strUserName = null; // username from jsp
    String strPassword = null; // password from jsp
    String strComnPath = null; // UserName(Session Variable)
    String strOldPass = "";
    String strNewPass = "";
    String strTodaysDate = "";
    String strPgToLoad = "";
    String strEmail = "";
    String strSwitchUserFl = "";
    String strButtonDisable = "";
    String strReadOnlyAccess = "";
    String strSessionId = "";
    // setting locale as null to show portal labels in English
    String strSessCompanyLocale = "";
    // The below Language id is used to show the Japan Master data in english on sales sales reports
    // and in dropdowns for Non Globus Medical Japan as primary company
    String strSessCompanyLangId = "103097";
    String strScheduleAccess = null;
	String strKeyCloak = "N";
    boolean kcActiveFl = false;

    boolean blFlag = false;
    boolean doesNeedAuthentication = true;
    strUserName = GmCommonClass.parseNull(request.getParameter("Txt_Username")).toLowerCase();
    strPassword = GmCommonClass.parseNull(request.getParameter("Txt_Password"));

    String strReturnTo = GmCommonClass.parseNull(request.getParameter("return_to"));
 
    

	String strOpt = GmCommonClass.parseNull(request.getParameter("strOpt"));
	
	// below code use to get the user details from keycloak
	if (!strOpt.equals("SwitchUser")) {
    Principal userPrincipal = request.getUserPrincipal();
		if (userPrincipal instanceof KeycloakPrincipal) {
			KeycloakPrincipal<KeycloakSecurityContext> kp = (KeycloakPrincipal<KeycloakSecurityContext>) userPrincipal;
			IDToken token = kp.getKeycloakSecurityContext().getIdToken();
			String kcUserName = token.getPreferredUsername();
			String kcToken = token.getId();
			boolean kcTokenActiveFl = token.isActive();
			kcActiveFl = true;
			if (!kcTokenActiveFl && kcToken == null) {
				throw new AppError(AppError.APPLICATION, "1002");
			} else if(!strAction.equals("CRM") && !strAction.equals("DUP")) {
				strAction = "Login";
				strOpt = "Keycloak";
				strUserName = GmCommonClass.parseNull(kcUserName.toLowerCase());
				strKeyCloak = "Y";
			}
		}
    }
    session.setAttribute("strKeyCloak", strKeyCloak);
	session.setAttribute("strOpt", strOpt);
	
    if (!strReturnTo.equals("")) {
      try {
        hmParam.put("SESSID", request.getSession().getId());
        hmParam.put("CLIENTIP", strClientIP);
        redirectpage(session, request, response, hmParam);
        return;
      } catch (Exception e) {
        throw new ServletException(e);
      }
    } else if (strAction.equals("CRM")) {
        try {
            redirectCRM(session,
                GmCommonClass.parseNull((String) session.getAttribute("strSessUserId")), request,
                response);
            return;
          } catch (Exception e) {

        throw new ServletException(e);
      }
    }
    // below code is used to call the redirectDuplicateTab method when strAction is equal to DUP
    else if (strAction.equals("DUP")) {
      try {
        redirectDuplicateTab(session, request, response);
        return;
      } catch (Exception e) {
        throw new ServletException(e);
      }
    }
    response.setContentType("text/html");
    response.setHeader("control-cache", "no-cache");
    PrintWriter out = response.getWriter();
    String strServletPath = GmCommonClass.getString("GMSERVLETPATH");
    String strExternalServer = GmCommonClass.getString("EXTERNAL_SERVER");

    String strAdminPattern = "^.*dmin.*";
    boolean blDoesUserNeedADAuthN = false;
    String strCookieId = "";
    String strPassWordKey = "";
    

    try {
      strGen = getCookieValue(request, "gen");

      if (strGen == null && strAction == null) {
        gotoPageOld("/GmSSOLogin.jsp", request, response);
      } else if (strGen != null && strAction.equals("")) {
        if (strGen.indexOf("#DWPK") != -1) {
          strCookieId = strGen.substring(0, strGen.indexOf("#DWPK"));
          strPassWordKey = strGen.substring(strGen.indexOf("#DWPK"));
        }
        HashMap hmUser = gmSecurityBean.fetchUser(strCookieId);

        if (hmUser != null && hmUser.size() > 0) {
          strUserName = GmCommonClass.parseNull((String) hmUser.get("USERNM"));
          strPassword =
              GmCommonClass.parseNull(gmSecurityBean.decrypt((String) hmUser.get("PASSWD"),
                  strPassWordKey));
        } else {
          gmCookieManager.deleteCookie(response, "gen");
          gotoPageOld("/GmSSOLogin.jsp", request, response);
        }
      }
	 // Keycloak user functionality validation 
      if ((strUserName.equals("") || strPassword.equals("")) && !strOpt.equals("SwitchUser") && !strOpt.equals("Keycloak")
          && !strAction.equals("SendPassword") && !strAction.equals("ChangePass")
          && !strAction.equals("Change") && !strAction.equals("Forgot") && !strKeyCloak.equals("N")
	  ) {
        throw new AppError(AppError.APPLICATION, "1002");
      }



      strComnPath = GmCommonClass.getString("GMCOMMON");


      // //log.debug("strBrowserType-->"+strBrowserType);
      // log.debug("strClientSysType-->"+strClientSysType);

      if (strAction == null || strAction.equals("")) {
        strAction = (String) session.getAttribute("hAction");
      }
      strAction = (strAction == null || strAction.equals("")) ? "Login" : strAction;


      HashMap hmReturn = new HashMap();
      HashMap hmAccess = new HashMap();

      blDoesUserNeedADAuthN = gmLogonBean.doesUserNeedADAuthN(strUserName);
      // log.debug(" Does User "+ strUserName+" Need AD Authentication " + blDoesUserNeedADAuthN);
      if (strUserName.matches(strAdminPattern)) {
        throw new AppError(AppError.APPLICATION, "1008");
      }
      if (strAction.equals("Login")) {

        // log.debug("strUserName: " + strUserName);
        // log.debug("strPassword: " + strPassword);

        strSessionId = request.getSession().getId();
        hmParam.put("USERNM", strUserName);
        hmParam.put("PASSWD", strPassword);
        hmParam.put("SESSID", strSessionId);
        hmParam.put("CLIENTIP", strClientIP);
        hmParam.put("STROPT", strOpt);
        GmLdapBean gmLdapBean = new GmLdapBean();
        String strLdapId = gmLdapBean.getLdapIdFromUserName(strUserName);
        hmParam.put("LDAPID", strLdapId);
        // hmReturn = gmLogonBean.validateLogon(hmParam);
        doesNeedAuthentication = gmLogonBean.doesNeedAuthentication(strLocalServerName);
        // log.debug(" Does Need AuthN " + doesNeedAuthentication + " strOpt: " + strOpt);
        if (strOpt.equals("")) {
          if (doesNeedAuthentication) {
            gmLogonBean.authenticate(hmParam);
          }
        }
        // To set all informations in the session.
        setUserSessionInfo(session, request, hmParam);
        
        log.debug("setUserSessionInfo hmParam "+hmParam);
        // if (blDoesUserNeedADAuthN) {
        String strCipherPassword = aesEncryptDecrypt.encryptText(strPassword);
        String strCipherUserName = aesEncryptDecrypt.encryptText(strUserName);
        log.debug(" Cipher Password " + strCipherPassword+" strCipherUserName>> "+strCipherUserName);
        session.setAttribute("strJunk", strCipherPassword);
        session.setAttribute("strLogonName", strCipherUserName);
        // }

        String strAccessLevel =
            GmCommonClass.parseNull((String) session.getAttribute("strSessAccLvl"));
        log.debug("strAccessLevel "+strAccessLevel);        
        String strExternalAccFl =
            GmCommonClass.parseNull((String) request.getAttribute("strExtAccess"));
        log.debug("strExternalAccFl "+strExternalAccFl);
        int intAccessLevel = 0;
        intAccessLevel = Integer.parseInt(strAccessLevel);
        log.debug("intAccessLevel "+intAccessLevel);
        
        if (strExternalAccFl.equals("Y")) {
          log.debug("if strExternalAccFl");
          blFlag = true;
        }
        log.debug("strServerIp "+strServerIp+" strExternalServer>> "+strExternalServer);        
        if (!strServerIp.equals(strExternalServer)) {
          log.debug("strServerIp inside if ");
          blFlag = true;
        }
        log.debug("blFlag "+blFlag);        
        if (!blFlag) {
          log.debug("bflag inside if ");        	
          session.invalidate();
          session = null; // Makes the current session null after invalidating
          gotoPage(GmCommonClass.getString("GMCOMMON").concat("/GmPortalExit.jsp"), request,
              response);
          log.debug("after go to page ");          
          return;
        } else {
            log.debug("else ");
          String strRememberMe =
              GmCommonClass.parseNull(request.getParameter("Chk_Remember")).toLowerCase();
          log.debug("strRememberMe "+strRememberMe);
          // log.debug("strRememberMe : " + strRememberMe);
          if (strRememberMe.equals("on")) {
              log.debug("inside if strRememberMe ");        	  
            String struuid = gmSecurityBean.saveRememberUser(strUserName, strPassword);
            log.debug("struuid "+struuid);            
            // log.debug("uuid : " + struuid);
            // addCookie(response, "GENERAL", struuid);

            Cookie cookie = new Cookie("gen", struuid);
            // cookie.setSecure(true);
            cookie.setMaxAge(10 * 365 * 24 * 60 * 60);
            response.addCookie(cookie);
          }
          String strUserId =
              GmCommonClass.parseNull((String) session.getAttribute("strSessUserId"));
          log.debug("strUserId "+strUserId);
          // get the access permission for Read only access to portal
          hmAccess =
              GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
                  "READ_ONLY_ACCESS"));
          log.debug("hmAccess "+hmAccess);          
          strReadOnlyAccess = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
          log.debug("strReadOnlyAccess "+strReadOnlyAccess);
          // if Switch user then
          if (strOpt.equals("SwitchUser") || strReadOnlyAccess.equals("Y")) {
            // to get the button disable values from rule
              log.debug("if inside strOpt "+strUserId);
            strButtonDisable =
                GmCommonClass.parseNull(GmCommonClass.getRuleValue("BUTTON_DISABLE", "DISABLE"));
            log.debug("strButtonDisable "+strButtonDisable);
            if (strButtonDisable.equals("Y")) {
              strSwitchUserFl = "Y";
            }
          }

          session.setAttribute("strSwitchUserFl", strSwitchUserFl);
          strPrimaryCmpyId =
              GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyId"));
          log.debug("strPrimaryCmpyId "+strPrimaryCmpyId);
          // Getting
          String strMultiLangCmpyId =
              GmCommonClass.parseNull(GmCommonClass.getRuleValue(strPrimaryCmpyId, "MULTILINGUAL"));
          log.debug("strMultiLangCmpyId "+strMultiLangCmpyId);
          if (strMultiLangCmpyId.equals(strPrimaryCmpyId)) {
            strSessCompanyLocale =
                GmCommonClass.parseNull(GmCommonClass.getCompanyLocale(strPrimaryCmpyId));
            strSessCompanyLangId =
                GmCommonClass.parseNull(GmCommonClass.getCompanyLanguage(strPrimaryCmpyId));
          }

          // If the user who's primary company is Japan,logged in as Switch user , we will show all
          // labels in English on portal
          if (strSwitchUserFl.equals("Y")) {
            strSessCompanyLocale = "";
            strSessCompanyLangId = "103097";
          }
          
          session.setAttribute("strSessCompanyLocale", strSessCompanyLocale);
          session.setAttribute("strSessCompanyLangId", strSessCompanyLangId);

          String strDeptId =
              GmCommonClass.parseNull((String) session.getAttribute("strSessDeptId"));
          log.debug("strDeptId "+strDeptId);
          String strDeptSeq =
              GmCommonClass.parseNull((String) session.getAttribute("strSessDeptSeq"));
          log.debug("strDeptSeq "+strDeptSeq);
          if (strDeptId.equals("J")) {
            strPgToLoad = strServletPath.concat("GmAcctDashBoardServlet");
          } else if (strDeptId.equals("B")) {
            strPgToLoad = strServletPath.concat("GmManfDashBoardServlet");
          } else if (strDeptId.equals("C")) {
            strPgToLoad = strServletPath.concat("GmCustDashBoardServlet");
          } else if (strDeptId.equals("O") || strDeptId.equals("L") || strDeptId.equals("Q")
              || strDeptId.equals("F")) {
            strPgToLoad = strServletPath.concat("GmOperDashBoardServlet");
          } else if (strDeptId.equals("R")) {
              log.debug("inside else if  strDeptId "+strDeptId);
            // strPgToLoad = strServletPath.concat("GmClinicDashBoardServlet");
            strPgToLoad = strServletPath.concat("/gmClinicalStudyAction.do?");
          } else if (strDeptId.equals("S")) {
            strPgToLoad = strServletPath.concat("/gmSalesDashLoaner.do?method=loadSalesDashBoard");
          } else if (strDeptId.equals("E")) {
            strPgToLoad = strServletPath.concat("GmSaleDashBoardServlet");
          } else if (strDeptId.equals("D")) {
            strPgToLoad = strServletPath.concat("GmShippingReportServlet");
          } else if (strDeptId.equals("NEW")) {
            strPgToLoad = strServletPath.concat("/user/GmNewUser.jsp");
          } else {
            strPgToLoad = strServletPath.concat("/user/GmUserDefault.jsp");
          }
		  		  
          String strExpired = GmCommonClass.parseNull(gmLogonBean.checkExpired(strUserId));
          log.debug("strExpired"+strExpired);          
          session.setAttribute("strSessPgToLoad", strPgToLoad);
          // log.debug(" strExpired is ......" + strExpired);
          if (strExpired.equals("90890")) // password expired
          {
            session.setAttribute("strExpired", strExpired);
            strDispatchTo = strComnPath.concat("/GmChangePass.jsp");
          } else {
            if (strDeptSeq.equals("2007")) {
                log.debug("strDeptSeq"+strDeptSeq);            	
              strDispatchTo = "GmClinicalPortal.jsp";
            } else {
                log.debug("else ");            	
          	  strScheduleAccess = gmEventSetupBean.getEventAccess(GmCommonClass.parseNull((String) session.getAttribute("strSessPartyId")), "EVENT", "SPINEIT-REBRANDING");
              strDispatchTo = strScheduleAccess.equals("Y")? "GmGlobusOnePortal.jsp" :  "GmGlobusOnePortal.jsp";
              log.debug("strDispatchTo "+strDispatchTo);
            }
          }
        }
      } else {
        if (strAction.equals("Change")) {
          if (blDoesUserNeedADAuthN) {
            strDispatchTo = OWA_URL;
          } else {
            strDispatchTo = strComnPath.concat("/GmChangePass.jsp");
          }
        } else if (strAction.equals("Forgot")) {
          if (blDoesUserNeedADAuthN) {
            strDispatchTo = OWA_URL;
          } else {
            strDispatchTo = strComnPath.concat("/GmForgotPass.jsp");
          }
        }

        else if (strAction.equals("SendPassword")) {
          gmSecurityBean.saveForgotUser(strUserName);
          strEmail = GmCommonClass.parseNull(request.getParameter("Txt_email"));
          gmLogonBean.sendPassword(strUserName, strEmail);
          session.setAttribute("hType", "S");
          session.setAttribute("hChange", "change");
          String errType = (String) session.getAttribute("hType");
          throw new AppError("Your password has been sent to your email", "", 'S');
        } else if (strAction.equals("ChangePass")) {
          gmSecurityBean.saveForgotUser(strUserName);
          strOldPass = GmCommonClass.parseNull(request.getParameter("Txt_OldPass"));
          strNewPass = GmCommonClass.parseNull(request.getParameter("Txt_NewPass"));
          hmReturn = gmLogonBean.changePassword(strUserName, strOldPass, strNewPass);
          session.setAttribute("hType", "S");
          session.setAttribute("hChange", "change");
          throw new AppError("Password successfully changed", "", 'S');
        }
      }


      log.debug(" before gotopage old strDispatchTo......" + strDispatchTo);
      
      gotoPageOld(strDispatchTo, request, response);
      log.debug(" after gotopage old strDispatchTo......" + strDispatchTo);
      // gotoPage(strDispatchTo,request,response);

    } catch (AppError e) {
      // log.debug(" type " + e.getType() + " Message " +e.getMessage());

      /*
       * if(e.getMessage().indexOf("USER_SHOULD_CHANGE_PASSWORD") != -1){ strDispatchTo = OWA_URL;
       * gotoPageOld(strDispatchTo, request, response); }
       */

      /*
       * If user password is changed in outside the portal then his Remember me option (auto login)
       * always throws incorrect password error. In this case the below code delete existing
       * Remember me cookie.
       */
        log.debug("strGen " + strGen);    	
      if (strGen != null) {
          log.debug(" if " + strGen);
        gmCookieManager.deleteCookie(response, "gen");
        e = new AppError(AppError.APPLICATION, "1563");
        request.setAttribute("hRedirectURL", "/GmSSOLogin.jsp");
        request.setAttribute("hAppErrorMessage", e.getMessage());
        // forward method only send request attribute values to destination source
        log.debug(" before getRequestDispatcher "+strComnErrorPath);        
        request.getRequestDispatcher(strComnErrorPath).forward(request, response);
        log.debug(" after getRequestDispatcher ");
      } else {
          log.debug(" else part ");    	  
        session.setAttribute("hAppErrorMessage", e.getMessage());
        request.setAttribute("hType", e.getType());
        log.debug(" before gotoPageOld "+request);        
        gotoPageOld(strComnErrorPath, request, response);
        log.debug(" after gotoPageOld  "+response);        
      }
      return;
      // strDispatchTo = strComnPath + "/GmError.jsp";
      // dispatch(strDispatchTo,request,response);
    } catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      gotoPage(strComnErrorPath, request, response);
      // strDispatchTo = strComnPath + "/GmError.jsp";
      // dispatch(strDispatchTo,request,response);
    }
  }

  /**
   * setSessionInformation - This is use to set all user info's into session.
   * 
   * @param session
   * @param request
   * @param hmParam
   * @throws Exception
   */
  private void setUserSessionInfo(HttpSession session, HttpServletRequest request, HashMap hmParam)
      throws Exception {

    String strTodaysDate = "";
    String strUserAgent = "";
    String strClientSysType = "";
    String strBrowserType = "";
    GmLogonBean gmLogonBean = new GmLogonBean(getGmDataStoreVO());
    GmSecurityBean gmSecurityBean = new GmSecurityBean();
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
    Calendar cal = new GregorianCalendar();
    int year = cal.get(Calendar.YEAR);
    int month = cal.get(Calendar.MONTH);
    int day = cal.get(Calendar.DATE);
    month = month + 1;
    String strMonth = "" + month;
    String strDay = "" + day;
    if (strMonth.length() == 1) {
      strMonth = "0".concat(strMonth);
    }
    if (strDay.length() == 1) {
      strDay = "0".concat(strDay);
    }
    String strYear = "" + year;
    strTodaysDate = strMonth.concat("/").concat(strDay).concat("/").concat(strYear);

    // Browser and OS related code
    strUserAgent = GmCommonClass.parseNull(request.getHeader("user-agent"));
    strClientSysType = (strUserAgent.toUpperCase().indexOf("IPAD") != -1) ? "IPAD" : "PC";
    if (strUserAgent.toUpperCase().indexOf("SAFARI") > -1) {
      strBrowserType = "SAFARI";
    } else if (strUserAgent.toUpperCase().indexOf("MSIE") > -1) {
      strBrowserType = "IE";
    } else {
      strBrowserType = "OTHER";
    }

    HashMap hmReturn = gmLogonBean.fetchUserDetailsAfterAuthentication(hmParam);
    // log.debug(" details after logiob " + hmReturn);
    String strCompanyId = GmCommonClass.parseNull((String) hmReturn.get("COMPID"));
    log.debug("strCompanyId"+strCompanyId);
    String strPlantId = GmCommonClass.parseNull((String) hmReturn.get("PLANTID"));
    String strCompTimeZone = GmCommonClass.parseNull((String) hmReturn.get("CMPTIMEZONE"));
    String strCompDateFmt = GmCommonClass.parseNull((String) hmReturn.get("CMPDATEFMT"));

    if (strCompanyId.equals("0")) {
      log.debug("strCompanyId............." + strCompanyId);
      throw new AppError(AppError.APPLICATION, "1602");
    }
    String strUserShName = (String) hmReturn.get("SHNAME");
    String strUserNm = (String) hmReturn.get("USERNM");
    String strFName = (String) hmReturn.get("FNAME");
    String strLName = (String) hmReturn.get("LNAME");
    String strUserId = (String) hmReturn.get("USERID");
    String strDeptId = (String) hmReturn.get("DEPTID");
    String strPartyId = (String) hmReturn.get("PARTYID");
    String strRepDiv = (String) hmReturn.get("REPDIV");
    String strAccessLevel = (String) hmReturn.get("ACID");
    String strDeptSeq = GmCommonClass.parseNull((String) hmReturn.get("DEPTSEQ"));
    String strLoginTS = (String) hmReturn.get("LOGINTS");
    String strAlertsCnt = GmCommonClass.parseNull((String) hmReturn.get("ALERTCNT"));// To get the
                                                                                     // Unread alert
                                                                                     // count
    String strApplDateFmt = GmCommonClass.parseNull((String) hmReturn.get("APPLDATEFMT"));
    String strJSDateFmt = GmCommonClass.parseNull((String) hmReturn.get("JSDATEFMT"));
    // String strApplCurrFmt = GmCommonClass.parseNull(GmCommonClass.getRuleValue("CURRFMT",
    // "CURRFMT"));
    // String strSessCurrSymbol = GmCommonClass.parseNull(GmCommonClass.getRuleValue("CURRSYMBOL",
    // "CURRSYMBOL"));
    String strExternalAccFl = GmCommonClass.parseNull((String) hmReturn.get("EXTACCESS"));
    String strRptRepId = GmCommonClass.parseNull((String) hmReturn.get("REPID"));

    HashMap hmCurrDetails = GmCommonClass.getCurrSymbolFormatfrmComp(strCompanyId);
    String strApplCurrFmt = GmCommonClass.parseNull((String) hmCurrDetails.get("CMPCURRFMT"));
    String strSessCurrSymbol = GmCommonClass.parseNull((String) hmCurrDetails.get("CMPCURRSMB"));
    String strSessRptCurrSymbol =
        GmCommonClass.parseNull((String) hmCurrDetails.get("CMPRPTCURRSMB"));
    String strSessCurrType = GmCommonClass.parseNull((String) hmCurrDetails.get("CURRTYPE"));

    strApplDateFmt = strApplDateFmt.equals("") ? "mm/dd/YYYY" : strApplDateFmt;
    String strCompId = (String) hmReturn.get("COMPANYID");
    String strDivId = (String) hmReturn.get("DIVID");
    strJSDateFmt = strJSDateFmt.equals("") ? "%m/%d/%Y" : strJSDateFmt;
    strApplCurrFmt = strApplCurrFmt.equals("") ? "#,###,##0.00" : strApplCurrFmt;
    strSessCurrSymbol = strSessCurrSymbol.equals("") ? "$" : strSessCurrSymbol;
    // log.debug("strSessCurrSymbol~~~~~~~~~~~~~~~~~~~~"+strSessCurrSymbol);
    strTodaysDate =
        GmCommonClass.getStringFromDate(new java.util.Date(strTodaysDate), strApplDateFmt);
    // Overriding access level for other than sales module users
    String strOverrideAccessTo = GmCommonClass.parseNull((String) hmReturn.get("OVERRIDE_ACS_TO"));
    String strOverrideAccessId = GmCommonClass.parseNull((String) hmReturn.get("OVERRIDE_ACS_ID"));
    String strOverrideAccessLvl =
        GmCommonClass.parseNull((String) hmReturn.get("OVERRIDE_ACS_LVL"));

    String strSessMultiCompMap = GmCommonClass.parseNull((String) hmReturn.get("MULTICOMPANYMAP"));
    String strSessMultiRegionMap = GmCommonClass.parseNull((String) hmReturn.get("MULTIREGIONMAP"));

    // If the user was mapped with multiple companies or multiple regions then set the Currency Type
    // to Base(105460).
    if (strSessMultiCompMap.equals("YES") || strSessMultiRegionMap.equals("YES")) {

      strSessCurrType = "105460"; // 105460 - Base Currency Type

    }
    // User's mapped under with in this security group(SHOW_ADDNL_COMP_IN_SALES) , they can able to
    // see both the Globus and BBA sales
    HashMap hmAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
            "SHOW_ADNL_COMP_SALES"));
    String strBBASalesAcsFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
   
    /*
     * Fetching the ADDNL_COMP_IN_SALES (BBA company) value from rule and set it in the session
     *  @ Author: gpalani. PMT-20656 Jun 2018
     */
    String strAddnlCompFl =
            GmCommonClass
                .parseNull(GmCommonClass.getRuleValue(strCompanyId, "ADDNL_COMP_IN_SALES"));
    log.debug("strAddnlCompFl"+strAddnlCompFl);
    log.debug("strBBASalesAcsFl"+strBBASalesAcsFl);
    
    request.setAttribute("strExtAccess", strExternalAccFl);
    // if request foward issue fixed then COMPANYID AND COMPJSDFMT will be set into request
    session.setAttribute("strSessCompanyId", strCompanyId);
    session.setAttribute("strSessPlantId", strPlantId);
    // PC-4014 Scorecard and Field Inventory Report full access (mschmitt)    
    String strSessSalesFullAccess = GmCommonClass.parseNull((String) hmReturn.get("SALES_FILTER_ACCESS_FL"));
    session.setAttribute("strSessSalesFullAccess", strSessSalesFullAccess);    
    // ***************************************
    // To get the Login Security information
    // **************************************
    hmReturn = gmSecurityBean.loadAccessInformation(strUserId, strDeptSeq);
    log.debug(" Before setting the currency type values ****::" + strSessCurrType);
    // Setting value in the session variable
    session.setAttribute("strSessUserId", strUserId);
    session.setAttribute("strSessUserNm", strUserNm);
    session.setAttribute("strSessRepDiv", strRepDiv);
    session.setAttribute("strSessFName", strFName);
    session.setAttribute("strSessLName", strLName);
    session.setAttribute("strSessDeptId", strDeptId);
    session.setAttribute("strSessAccLvl", strAccessLevel);
    session.setAttribute("strSessShName", strUserShName);
    session.setAttribute("strSessTodaysDate", strTodaysDate);
    session.setAttribute("strSessLoginTS", strLoginTS);
    session.setAttribute("hmAccessLevel", hmReturn);
    session.setAttribute("strSessDeptSeq", strDeptSeq);
    session.setAttribute("strSessApplDateFmt", strApplDateFmt);
    session.setAttribute("strSessJSDateFmt", strJSDateFmt);
    session.setAttribute("strSessApplCurrFmt", strApplCurrFmt);
    session.setAttribute("strSessCurrSymbol", strSessCurrSymbol);
    session.setAttribute("strSessRptCurrSymbol", strSessRptCurrSymbol);
    session.setAttribute("strSessCurrType", strSessCurrType);
    session.setAttribute("strSessCompId", strCompId);
    session.setAttribute("strSessDivId", strDivId);
    session.setAttribute("strSessAlertsCnt", strAlertsCnt);
    session.setAttribute("strSessOverrideAcsTo", strOverrideAccessTo);
    session.setAttribute("strSessOverrideAcsId", strOverrideAccessId);
    session.setAttribute("strSessOverrideAcsLvl", strOverrideAccessLvl);
    session.setAttribute("strSessMultiCompMap", strSessMultiCompMap);
    session.setAttribute("strSessMultiRegionMap", strSessMultiRegionMap);
    session.setAttribute("strSessBBASalesAcsFl", strBBASalesAcsFl);
    session.setAttribute("strSessCmpTimeZone", strCompTimeZone);
    session.setAttribute("strSessCmpDateFmt", strCompDateFmt);
    session.setAttribute("strSessAddnlCompFl", strAddnlCompFl);

    session.setAttribute("strPrimaryCompID", strCompanyId);


    // ASS Rep's reporting RepId
    session.setAttribute("strSessRptRepId", strRptRepId);

    session.setAttribute("strSessPartyId", strPartyId);

    // Browser and OS related changes.
    session.setAttribute("strSessClientSysType", strClientSysType);
    session.setAttribute("strSessBrowserType", strBrowserType);
    session.setAttribute("strSessOverrideAcsTo", strOverrideAccessTo);

    ArrayList hmMicroCredentials = new ArrayList();
    hmMicroCredentials = GmCommonClass.parseNullArrayList(gmSecurityBean.fetchMicroAppSecurityGroups(strUserId));
	log.debug("hmMicroCredentials>> "+hmMicroCredentials);
	session.setAttribute("hmMicroCredetials", hmMicroCredentials);
  }

  /**
   * Any portal URL which is called from Mobile device will use this method. From device Token,
   * return URL has to be passed in. This method will validate the token and redirect to the
   * respective URL. The Portal URL that is passed in should be URL Encoded ,only then it will work.
   * 
   * @param request
   * @param response
   * @throws Exception
   */
  private void redirectpage(HttpSession session, HttpServletRequest request,
      HttpServletResponse response, HashMap hmParam) throws Exception {
    boolean blValidToken = (new GmResource()).validateToken(request.getParameter("token"));

    GmUserVO gmUserVO = GmTokenManager.getTokenValue(request.getParameter("token"));
    hmParam.put("USERNM", GmCommonClass.parseNull(gmUserVO.getUsernm()));
    hmParam.put("STROPT", GmCommonClass.parseNull(gmUserVO.getStropt()));

    // To set all the access level informations in this session.
    setUserSessionInfo(session, request, hmParam);
    log.debug("blValidToken "+blValidToken);
    String strReturnTo = GmCommonClass.parseNull(request.getParameter("return_to"));

    if (!strReturnTo.equals("")) {
      RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(strReturnTo);
      dispatcher.include(request, response);
    }
  }

  /**
   * This function is called only when the module is selected as CRM. It creates the token for the
   * user and sets it to the session. Then it redirects the page to gmCRMPortal.jsp which will load
   * CRM into the portal.
   * 
   * @param request
   * @param response
   * @throws Exception
   */
  private void redirectCRM(HttpSession session, String strUserIdentity, HttpServletRequest request,
      HttpServletResponse response) throws Exception {

    if ((GmCommonClass.parseNull((String) session.getAttribute("strSessToken"))).equals("")) {
      String strToken = GmTokenManager.getNewToken();
      // GmTokenManager.addToken(strToken, gmUserVO);
      HashMap hmTkn = new HashMap();
      hmTkn.put("TOKEN", strToken);
      hmTkn.put("STATUS", "103101"); // Active Token Status
      hmTkn.put("USERID", strUserIdentity);
      // This method will save the User Token
      (new GmUserTokenBean()).saveUserToken(hmTkn);
      session.setAttribute("strSessToken", strToken);
    }

    gotoPageOld("/GmMobilePortal.jsp", request, response);
  }

  /* Below Method is used to redirect the duplicate tab. */
  private void redirectDuplicateTab(HttpSession session, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    GmCookieManager gmCookieManager = new GmCookieManager();
    String strDispatchTo = ""; 
    String strReqCompId = GmCommonClass.parseNull(request.getParameter("moc"));
    String strReqJsDateFmt = GmCommonClass.parseNull(request.getParameter("tmf"));
    String strReqPartyId = GmCommonClass.parseNull(request.getParameter("rap"));
    String strReqPlantId = GmCommonClass.parseNull(request.getParameter("alp"));
    String strPgToLoad = GmCommonClass.parseNull(request.getParameter("ltp"));
    String strScreen = GmCommonClass.parseNull(request.getParameter("srn"));
    gmCookieManager.setCookieValue(response, "cCompId", strReqCompId);
    gmCookieManager.setCookieValue(response, "cPartyId", strReqPartyId);
    gmCookieManager.setCookieValue(response, "cPlantId", strReqPlantId);
    gmCookieManager.setCookieValue(response, "cJsDateFmt", strReqJsDateFmt);
    gmCookieManager.setCookieValue(response, "cPgToLoad", strPgToLoad);

    if(strScreen.equals("newlook")) {
    	strDispatchTo = "GmGlobusOnePortal.jsp";
    }else {
    	strDispatchTo = "GmEnterprisePortal.jsp";
    }
    session.setAttribute("strSessPgToLoad", strPgToLoad);

    log.debug("strPgToLoad>>>" + strPgToLoad);

    gotoPageOld(strDispatchTo, request, response);
  }
}