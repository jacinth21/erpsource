package com.globus.common.servlets;

import java.io.IOException;
import java.net.InetAddress;
import java.net.URLEncoder;
import java.util.Enumeration;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.util.GmWSUtil;

/**
 * Receive customer id & name of servlet and redirect in to that servlet.
 */
public class GmPageControllerServlet extends GmServlet {

  private static String LOCAL_SERVER_NAME = "";

  /***************************************************************************
   * calling init method.
   */
  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);
  }

  /**
   * Receive the request & send to JSP as response.
   * 
   * @param request - Request object
   * @param request - Response object
   * @exception IOException
   */

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    HttpSession session = request.getSession(false);
    String strDispatchTo = "";
    String strTopMenu = null;
    String strHidden = request.getParameter("strPgToLoad");
    log.debug(" Page to load " + strHidden);
    String strOpt = request.getParameter("strOpt");
    String strCompanyInfo = GmCommonClass.parseNull(request.getParameter("companyInfo"));
    String strToken = (new GmWSUtil()).getParamFromJsonString(strCompanyInfo, "token");

    log.debug("strToken==" + strToken);

    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strServletPath = GmCommonClass.getString("GMSERVLETPATH");
    String strRootPath = GmCommonClass.getString("GMROOT");
    boolean blFlag = strHidden.indexOf(".do") != -1 ? true : false;
    boolean blReportFlag = strHidden.indexOf(".rpt") != -1 ? true : false;
    // DateFmt is added for the Globalization of the Portal. - OUS Project
    
    /* Adding additional session variables strSessAddnlCompFl and strPrimaryCompID
    *  PMT-20656 Author:gpalani Jun 2018
    */
    
    String strSessionVars[] =
        {"strSessUserId", "strSessDeptId", "strSessAccLvl", "strSessShName", "strSessTodaysDate",
            "strSessSubMenu", "strSessPgToLoad", "hmAccessLevel", "strSessOpt", "strSessLoginTS",
            "strSessDeptSeq", "strSessPartyId", "strLogonName", "strJunk", "strSessApplDateFmt",
            "strSessJSDateFmt", "strSessApplCurrFmt", "strSessCurrSymbol", "strSessClientSysType",
            "strSessBrowserType", "strSessCompId", "strSessRptRepId", "strSessDivId",
            "strSwitchUserFl", "strSessCompanyId", "strSessPlantId", "strSessCurrType",
            "strSessOverrideAcsTo", "strSessOverrideAcsId", "strSessOverrideAcsLvl",
            "strSessRptCurrSymbol", "strSessMultiCompMap", "strSessMultiRegionMap",
            "strSessBBASalesAcsFl", "strSessToken", "strSessFName", "strSessLName",
            "strSessUserNm", "strSessCompanyLocale", "strSessCompanyLangId", "strSessAddnlCompFl","strPrimaryCompID",
            "strSessSalesFullAccess",
			"org.keycloak.adapters.undertow.KeycloakUndertowAccount","strKeyCloak","hmMicroCredetials"
			}; // PC-4014 Scorecard and Field Inventory Report full access (mschmitt)
    try {
      //PMT-33439-Apache & PHP Changes on ERP server (3rd Set)
      //LOCAL_SERVER_NAME = InetAddress.getLocalHost().getHostName().toLowerCase();
      LOCAL_SERVER_NAME = System.getProperty("HOST_NAME");
      log.debug("LOCAL_SERVER_NAME"+LOCAL_SERVER_NAME);//PMT-33439
      
      checkSession(response, session); // Checks if the current session is valid
      String strUserName = (String) session.getAttribute("strLogonName");
      String strPassword = (String) session.getAttribute("strJunk");

      Enumeration enumm = null;
      String strTemp;
      boolean bolFlag = false;

      enumm = session.getAttributeNames();

      while (enumm.hasMoreElements()) {
        bolFlag = false;
        strTemp = (String) enumm.nextElement();
        for (int i = 0; i < strSessionVars.length; i++) {
          if (strTemp.equals(strSessionVars[i])) {
            bolFlag = true;
          }
        }
        if (!bolFlag) {
          session.removeAttribute(strTemp);
        }
      }

      // Appending companyInfo in URI
      String strDelimiter = strHidden.indexOf("?") != -1 ? "&" : "?";
      // Encoding required otherwise F5 will fail.
      strHidden += strDelimiter + "companyInfo=" + URLEncoder.encode(strCompanyInfo);
      log.debug("strHidden=-" + strHidden);

      session.setAttribute("strSessOpt", strOpt);
      session.setAttribute("strSessPgToLoad", strHidden);

      if (blFlag) {
        gotoStrutsPage(strHidden, request, response);
      } else {
        if (blReportFlag) {
          strHidden = getReportURL(strHidden, strUserName, strPassword);
          log.debug(" URL for redirection " + strHidden);
          gotoPage(strHidden, request, response);
        } else {
          strHidden = "/" + strHidden;
          gotoPage(strServletPath.concat(strHidden), request, response);
        }

      }

    }// End of try
    catch (Exception e) {
      e.printStackTrace();
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method

  /**
   * 
   * @param strReportName
   * @param strUserName
   * @param strPassword
   * @return URL for BO App
   */
  private String getReportURL(String strReportName, String strUserName, String strPassword) {

    HashMap hmReportData =
        GmCommonClass.parseNullHashMap(getReportCredentialDetails(strUserName, strPassword));
    log.debug(" Report Data " + hmReportData);
    StringBuffer sbURL = new StringBuffer();
    sbURL.append("http://");
    sbURL.append(hmReportData.get("SERVER"));
    sbURL.append(":8080/BOInterfaceApp/GmBOViewerSDKServlet?REPORT_NAME=");
    sbURL.append(strReportName);
    sbURL.append("&REQUEST_SERVER=");
    sbURL.append(LOCAL_SERVER_NAME);
    sbURL.append("&param1=");
    sbURL.append(hmReportData.get("NAME"));
    sbURL.append("&param2=");
    sbURL.append(hmReportData.get("PASSWORD"));

    return sbURL.toString();
  }

  /**
   * @param userName
   * @param password
   * 
   *        Creates a Hashmap with details for constructing Report URL userName / password - if the
   *        requesting server is NOT production, then decrypt the username and password serverName -
   *        If requesting server is production, then serverName will be BO Production and so on..
   * 
   */
  private HashMap getReportCredentialDetails(String strUserName, String strPassword) {
    HashMap hmParam = new HashMap();
    String strServerName = "";

    try {
      strServerName = GmCommonClass.getBusinessObjectsProperty(LOCAL_SERVER_NAME.toLowerCase()); // "gmiboprod";
    } catch (Exception exp) {
      strServerName = GmCommonClass.getBusinessObjectsProperty("DEFAULT_SERVER");
    }
    hmParam.put("SERVER", strServerName);
    hmParam.put("NAME", strUserName);
    hmParam.put("PASSWORD", strPassword);

    return hmParam;
  }

}// End of GmPageControllerServlet
