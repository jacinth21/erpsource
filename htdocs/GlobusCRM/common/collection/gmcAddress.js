
define([
    'underscore',
    'backbone',
    'gmmAddress'
], 

function (_, Backbone, GMMAddress) {

    'use strict';

    GM.Collection.Address = Backbone.Collection.extend({

		    model: GMMAddress,

    		initialize: function(data) {
               this.add(data);
    		},

            getFinalData: function() {
                var arrFinal = new Array();

                this.each(function(model) {
                    var finalModel = _.clone(model);
                    finalModel.unset('userid');
                    finalModel.unset('token');
                    finalModel.unset('stropt');
                    arrFinal.push(finalModel.toJSON());
                });

                return arrFinal;
            }
	});

	return GM.Collection.Address;
});