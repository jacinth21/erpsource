/**********************************************************************************
 * File:        GmSaleItemCol.js
 * Description: Collection of SaleItem Models
 * Version:     1.0
 * Author:      praja
 **********************************************************************************/
define([
    'underscore',
    'backbone',
    'saleitemmodel'
], 

function (_, Backbone, SaleItemModel) {

	'use strict';

	var SaleItemCollection = Backbone.Collection.extend({

		model: SaleItemModel,

		initialize: function(data) {
           this.add(data);
		},

		search: function(type,letters){
			var words = letters.split(/\s+/);
            var models = _.clone(this.models);

            _.each(words, function(word) {
            	var pattern = new RegExp(word, "i");
                models = _.filter(models, function(model) {
                    return pattern.test(model.get(type));
                });
            });
            return models;
		},

        filterBy: function(key, arrList) {
            var models = _.clone(this.models);
            var result = new Array();

            if(typeof(arrList)!="object") {
                arrList = [arrList];
            }

            _.each(arrList, function(listItem) {      
                result = result.concat(_.filter(models, function(model) {     
                    return (listItem == model.get(key))
                }));
            });

            if(arrList.length>0)
                return result;  
            else
                return models;
        },

        toggleSelect: function(id) {
            this.each(function(model) {
                if(model.get("ID")==id) {
                    if(model.get("Selected"))
                        model.set("Selected",false);
                    else
                        model.set("Selected",true);
                }       
            });
        },

        selectOnly: function (arrID) {
            if(typeof(arrID)!="object") {
                arrID = [arrID];
            }
            this.each(function(model) {
                if(arrID.indexOf(model.get("ID")) >=0 )
                    model.set("Selected",true);
                else
                    model.set("Selected",false);
            });
        },

        clearAllSelected: function () {
            this.each(function(model) {
                model.set("Selected",false);
            });
        },

        getSelected: function() {
            var arrSelected = new Array();
            this.each(function(model) {
                if(model.get("Selected"))
                    arrSelected.push(model.get("ID"));
            });
            return arrSelected;
        },
		
		ammountTotal: function(key) {
            if(key==undefined)
                key="value";
            return (this.reduce(function(memo, value) { return memo + parseFloat(value.get(key)) }, 0)).toFixed(2);
        },

        sortAsc: function(sortField) {
            var key = sortField;
            this.comparator = function(model) {
                return model.get(key);
            };  
            this.sort();
        },

        sortDesc: function(sortField) {
            var key = sortField;
            this.comparator = function(a, b) {
                a = a.get(key);
                b = b.get(key);
                return a < b ?  1 : a > b ? -1 : 0;
            }
            this.sort();
        },
        
        getUniqBy: function(key) {
        	var colData = this.toJSON();
        	console.log(colData);
        	var arrUniqID = _.uniq(_.pluck(colData,key));
        	var resultData =  _.filter(colData,function(data) {
				if(_.contains(arrUniqID,data[key])) {
					arrUniqID = _.without(arrUniqID,data[key]);
					return true;
				}
			})
			
			return resultData;
        }
	});

	return SaleItemCollection;
});