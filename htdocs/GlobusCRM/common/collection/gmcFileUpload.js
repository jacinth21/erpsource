/**********************************************************************************
 * File:        gmcFileDelete.js
 * Description: Common File upload (Delete)
 * Version:     1.0
 * Author:     	Matt
 **********************************************************************************/
define([
    'underscore',
    'backbone',
    'gmmFileUpload'
],

    function (_, Backbone, GMMFileUpload) {

        'use strict';

        var FileCollection = Backbone.Collection.extend({
            model: GMMFileUpload
        });

        return FileCollection;
    });
