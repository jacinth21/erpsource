/* Global Variables */
var deviceModel = "";
var deviceUUID = "";
var gmvApp;
var locale;
var UpdateTimer, TimerMilliSeconds = 900000; //2 Minutes ---- Converting to 15 minutes as Auto Update is implemeted
var intDBStatus = 0,
    intLogin = 0,
    intOfflineMode = 0;
var arrPendingDOList = new Array(),
    arrUpdatedDO = new Array();
var boolUpdate = true,
    boolSyncing = false,
    boolAutoUpdateStatus = 0,
    autoUpdateCollision;
var mainview;
var Handlebars = require('handlebars');
var catalogInitial, catalogDetail, collateralInitial, collateralDetail, shareView, shareStatusScreen, dashmainview, domainview, invmainview, homeview, setlistview, pricemainview;
var salescallDays = 180;
var crmUserTabs; // It holds complex value and so keeping it alone and not in GM.Global

var accountIdVal = "";
var typeIdVal = "903107";
$.xhrPool = [];
var pricetypedisc;
var companyId = localStorage.getItem("cmpid");

Number.prototype.cformat = function (n, x) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
};

var URL_DO_Template = "sales/DO/template/";
var URL_Inv_Template = "sales/inv/template/";
var URL_Dashboard_Template = "sales/dashboard/template/";
var URL_Case_Template = "sales/case/template/";
var URL_Catalog_Template = "sales/catalog/template/";
var URL_Collateral_Template = "sales/collateral/template/";
var URL_Common_Template = "common/template/";
var URL_CSS = "css/";
var URL_Home_Template = "home/template/";
var URL_Login_Template = "user/template/";
var URL_Main_Template = "common/template/";
var URL_Setup_Template = "setup/template/";
var URL_Msg_Template = "message/template/";
var URL_LoginFailed_Template = "user/template/";
var URL_Logout_Template = "user/template/";
var URL_Settings_Template = "settings/template/";
//Pricing templates
var URL_Price_Template = "pricing/template/";
var tabData = {
    Dashboard: {
        taburl: "dashboard",
        tabcolor: "gmFontWhite",
        icon: ["fa fa-tachometer"],
        module: ""
    },
//    "Field Sales": {
//        taburl: "fieldsales",
//        tabcolor: "tabfieldsales gmFontWhite",
//        icon: ["fa-users fa-lg"],
//        title: "Sales Region",
//        url: "fieldsales/regterdis",
//        module: "fieldsales"
//    },
//    Lead: {
//        taburl: "lead",
//        tabcolor: "tablead gmFontWhite",
//        icon: ["fa-users fa-lg", "fa-search fa-lg"],
//        css: "font-size: 0.75em; margin-left: 5px; margin-right : 8px;",
//        title: "Lead",
//        url: "crmactivity/info",
//        voname: "gmCRMActivityVO",
//        module: "lead"
//    },
    "Sales Call": {
        taburl: "salescall",
        tabcolor: "tabsalescall gmFontWhite",
        icon: ["fa-comments-o fa-lg"],
        title: "Sales Call",
        url: "crmactivity/info",
        voname: "gmCRMActivityVO",
        module: "salescall"
    },
//    Training: {
//        taburl: "training",
//        tabcolor: "tabtraining gmFontWhite",
//        icon: ["fa fa-empire fa-lg"],
//        title: "Training",
//        url: "crmactivity/info",
//        voname: "crmactivity/info",
//        module: "training"
//    },
    Surgeon: {
        taburl: "surgeon",
        tabcolor: "tabsurgeon gmFontWhite",
        icon: ["fa-user-md fa-lg"],
        title: "Surgeon",
        url: "surgeon/info",
        voname: "gmCRMSurgeonlistVO",
        module: "surgeon"
    },
    "Physician Visit": {
        taburl: "physicianvisit",
        tabcolor: "tabphysicianvisit gmFontWhite",
        icon: ["fa-user fa-lg"],
        title: "Physician Visit",
        url: "crmactivity/info",
        voname: "gmCRMActivityVO",
        module: "physicianvisit"
    },
    MERC: {
        taburl: "merc",
        tabcolor: "tabmerc gmFontWhite",
        icon: ["fa-graduation-cap fa-lg"],
        title: "MERC",
        url: "merc/dash",
        voname: undefined,
        module: "merc"
    },
//    CRF: {
//        taburl: "crf",
//        tabcolor: "tabcrf gmFontWhite",
//        icon: ["fa-suitcase fa-lg"],
//        title: "CRF",
//        url: "crf/list",
//        voname: "gmCRMCRFVO",
//        module: "crf"
//    },
    Preceptorship: {
        taburl: "preceptorship",
        tabcolor: "tabpreceptorship gmFontWhite",
        icon: ["fa-suitcase fa-lg"],
        title: "Preceptorship",
        url: "preceptorship/list",
        voname: "gmCRMPSVO",
        module: "preceptorship"
    },
    Tradeshow: {
        taburl: "tradeshow",
        tabcolor: "tabtradeshow gmFontWhite",
        icon: ["fa-suitcase fa-lg"],
        title: "Tradeshow",
        url: "tradeshow/dash",
        voname: "gmCRMTradeshowVO",
        module: "tradeshow"
    },
    "Quick Reference": {
        taburl: "quickreference",
        tabcolor: "tabquickreference gmFontWhite",
        icon: ["fa-suitcase fa-lg"],
        title: "Quick Reference",
        url: "quickref/list",
        voname: "gmCRMQuickRefVO",
        module: "quickreference"
    }
};

var dashConfig = [
    {
        "SecGrp": "CRMDB-SREP",
        "Primary": "surgeon",
        "Secondary": ["physicianvisit", "merc","quickreference", "salescall","tradeshow"]
    },
    {
        "SecGrp": "CRMDB-SMGT",
        "Primary": "physicianvisit",
        "Secondary": ["salescall","merc","quickreference","tradeshow"]
    },
    {
        "SecGrp": "CRMDB-PHYS",
        "Primary": "physicianvisit",
        "Secondary": ["salescall", "merc","quickreference","tradeshow"]
    },
    {
        "SecGrp": "CRMDB-MERC",
        "Primary": "merc",
        "Secondary": ["physicianvisit","tradeshow","quickreference"]
    }
    /*,
    {
        "SecGrp": "CRMDB-CRF",
        "Primary": "crf",
        "Secondary": ["merc", "physicianvisit"]
    },
    {
        "SecGrp": "CRMDB-EXEC",
        "Primary": "crf",
        "Secondary": ["merc", "physicianvisit"]
    },
    {
        "SecGrp": "CRMDB-INTUSE",
        "Primary": "crf",
        "Secondary": ["merc", "training", "physicianvisit"]
    },
    {
        "SecGrp": "CRMDB-TRAINING",
        "Primary": "training",
        "Secondary": ["crf", "merc", "physicianvisit"]
    },
    {
        "SecGrp": "CRMDB-MKTG",
        "Primary": "lead",
        "Secondary": ["physicianvisit", "merc", "training", "crf"]
    }*/
]

/* Templates */
var URL_Common_Template = "common/template/";
var URL_CRMDash_Template = "crm/dashboard/template/";
var URL_Lead_Template = "crm/activity/template/lead/";
var URL_PhysicianVisit_Template = "crm/activity/template/physicianvisit/";
var URL_Surgeon_Template = "crm/surgeon/template/";
var URL_SalesCall_Template = "crm/activity/template/salescall/";
var URL_FieldSales_Template = "crm/fieldsales/template/";
var URL_Training_Template = "crm/activity/template/training/";
var URL_MERC_Template = "crm/activity/template/merc/";
var URL_CRF_Template = "crm/crf/template/";
var URL_CRMMerc_Template = "crm/merc/template/";
var URL_Preceptor_Template = "crm/preceptorship/template/";
var URL_CRMMerc_Template = "crm/merc/template/";
var URL_Account_Template = "crm/Account/template/";

/* Common URLs */
var URL_CSS = "css/";
var URL_Datafiles = "/GlobusCRM/datafiles/";


function arrayOf(data) {
    var arrdata = [];

    if (data == undefined || data.length == undefined) {
        arrdata.push(data);
        return arrdata;
    } else {
        return data;
    }
}
/* Load CSS */
function loadCSS(url) {
    var link = document.createElement("link");
    link.type = "text/css";
    link.rel = "stylesheet";
    link.href = url+"?v=" + new Date().getTime();
    document.getElementsByTagName("head")[0].appendChild(link);
}

$.support.cors = true;

/* Prepare Template */
function fnGetTemplate(strPath, strName) {
    if (Handlebars.templates === undefined || Handlebars.templates[strName] === undefined) {
        $.ajax({
            cache: false,
            url: strPath + strName + ".hbs",
            success: function (data) {
                if (Handlebars.templates === undefined) {
                    Handlebars.templates = {};
                }
                Handlebars.templates[strName] = Handlebars.compile(data);
            },
            async: false
        });
    }
    return Handlebars.templates[strName];
}

function formatCurrency(numdata, decimal) {
    if (decimal == undefined)
        decimal = 2;
    var amtdata = parseFloat(numdata).toFixed(decimal);
    return amtdata.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

/* Authenticate - Username and Password */
function fnAuthenticate(strUserID, strPasswd, callback) {
    var user = {
        "usernm": strUserID,
    };

    $.support.cors = true;

    $.ajax({
        url: URL_WSServer + "auth",
        method: "POST",
        //headers: {'Accept': 'application/json','Authorization':'Bearer ' + keycloak.token},
        contentType: "application/json",
        data: JSON.stringify(user),
        datatype: "json",
        success: function (data) {
            //console.log(data);
            callback(data);
        },
        error: function (jqXHR, status, error) {
            //console.log(error);
            callback(error);
        }
    });
}

/* Get Server Data through Web Service */
function fnGetWebServerData(serviceurl, voname, input, callback, keepCall) {
    //console.log("Calling fnGetWebServerData with the following data <br> " + JSON.stringify(input));
	// keycloak token validation for rest api
    var token = localStorage.getItem("token");
	var keycloaktoken = localStorage.getItem("keycloaktoken");
	console.log("token in fnGetWebServerData"+token);
	console.log("keycloaktoken in fnGetWebServerData"+keycloaktoken);

	if(serviceurl == "file/get"){
    	token = token+"#"+keycloaktoken;   
		console.log("token value"+token);

    }
	

		
    if (input == undefined)
	{
        input = {
            "token": token
        };

	
	console.log("input.token in if condition"+input.token);
	}
 
		
else
	{
    token = localStorage.getItem("token"); //To avoid duplicate token	
    //input.token = token;
	input.token = token+"@"+keycloaktoken;
	console.log("input.token in else condition"+input.token);
	}	
	
    
    //passing user's company info with all the web service calls
    input.cmpid = localStorage.getItem("cmpid");
    input.cmptzone = localStorage.getItem("cmptzone");
    input.cmpdfmt = localStorage.getItem("cmpdfmt");
    input.plantid = localStorage.getItem("plantid");
    $.support.cors = true;

    $.ajax({
        beforeSend: function (jqXHR) {
            if (keepCall == undefined)
                $.xhrPool.push(jqXHR);
        },
        url: URL_WSServer + serviceurl,
        method: "POST",
        //headers: {'Accept': 'application/json','Authorization':'Bearer ' + keycloak.token},
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify(input),
        success: function (data) {
            // console.log(JSON.stringify(data));
            if ((data != null) && (voname != undefined) && eval("data." + voname) != undefined) {
                callback(eval("data." + voname));
            } else {
                callback(data);
            }
        },
        error: function (model, response, errorReport) {
            console.log("Model ResponseText: " + model.responseText);
            //            console.log($.parseJSON(model.responseText))
            var string = true;
            try {
                $.parseJSON(model.responseText);
            } catch (e) {
                string = false;
            }
            if (model.responseText && string && $.parseJSON(model.responseText).message == "Invalid token, Please try login again.") {
                localStorage.removeItem("token");
                window.location.href = "#";
            } else if ((model.responseText != null) && string && (model.responseText != undefined) && (model.responseText != "") && $.parseJSON(model.responseText).message != "Record already exists. Please verify the entered record.")
                showError($.parseJSON(model.responseText).message);
            else
                console.log("Error serviceurl " + serviceurl);
            console.log("Error voname " + voname);
            console.log("Error input " + JSON.stringify(input));
            // showError("Something went wrong! Please try after some time");
            //Since all web service pass through this function error callback parametrs are added here (response,errorReport)
            callback(null, model, response, errorReport);
        }
    });
}

// CRM functions
function onrefresh() {
    var that = this;
    $(".mobile-menu-top-user-name").html(localStorage.getItem("fName") + " " + localStorage.getItem("lName"))
    var token = localStorage.getItem("token");
    $("#iPad-fname").html(localStorage.getItem("fName"));
    $("#iPad-lname").html(localStorage.getItem("lName"));
    $(".ul-language-name").addClass("hide");
    if (token != null) {
        $("#ul-header-right-pic").removeClass("hide");
        var partyID = window.localStorage.getItem("partyID");
        var input = { "token": localStorage.getItem("token"), "refid": partyID, "deletefl": "", "refgroup": "103112" };
        fnGetWebServerData("file/get", "gmCRMFileUploadVO", input, function (fileData) {
            console.log(fileData)

            if (fileData != null) {
                fileData = arrayOf(fileData);
                fileData = fileData[0];
                GM.Global.UserPicID = fileData.fileid;
                $("#ul-header-right-pic").find("img").removeAttr("onerror").css("display", "inline-block");
                $("#mobile-menu-top-user").find("img").removeAttr("onerror").css("display", "inline-block");
                $("#ul-header-right-pic").find("img").attr("src", URL_CRMFileServer + fileData.refgroup + "/" + fileData.refid + "/" + fileData.filename + "?num=" + Math.random());
                $("#mobile-menu-top-user").find("img").attr("src", URL_CRMFileServer + fileData.refgroup + "/" + fileData.refid + "/" + fileData.filename + "?num=" + Math.random());

            }
            $("#ul-header-right-pic").find("img").attr("onerror", "this.style.display = 'none'");
            $("#mobile-menu-top-user").find("img").attr("onerror", "this.style.display = 'none'");
        }, true);
    }
    
    GM.Global.SchdulerEvents = undefined;
}

function getImageDoc(partyID, callback) {
    var token = localStorage.getItem("token");
    var partyID = partyID;

    var input = {
        "token": token,
        "refid": partyID,
        "deletefl": "",
        "refgroup": "103112"
    };
    fnGetWebServerData("file/get", "gmCRMFileUploadVO", input, function (data) {
        console.log("PICDATA");
        console.log(data);
        if (data != "" && data != null) {
            var fileData = data[0];
            console.log(fileData)
            if (fileData != null) {
                console.log($("#mobile-menu-top-user").length);
                console.log($("#ul-header-right-pic").length);
                console.log($("#mobile-menu-top-user-name").length);
                var URL = URL_CRMFileServer + fileData.refgroup + "/" + fileData.refid + "/" + fileData.filename + "?num=" + Math.random();
                callback(URL);
            }
        }

    });

}


function openPicturePopup(parent, ImgCrpView) {
    //    this.template_picture = fnGetTemplate(URL_Common_Template, "gmtUserProfilePic");
    var userPartyId = window.localStorage.getItem("partyID");
    var that = parent;
    if (userPartyId == 0)
        alert("Please Save the Surgeon Details to Upload a Profile Pic");
    else {
        showPopup();
        $("#div-crm-overlay-content-container").find('.modal-footer').addClass("hide");

        $("#div-crm-overlay-content-container").find('.modal-title').html("Edit Profile Picture");

        var template_picture = fnGetTemplate(URL_Common_Template, "gmtUserProfilePic");
        $("#div-crm-overlay-content-container").find('.modal-body').html(template_picture());
        if ($(window).width() <= 480) {
            var userurl = $("#mobile-menu-top-user img").attr('src');
        } else {
            var userurl = $("#ul-header-right-pic img").attr('src');
        }
        $("#div-user-pic").append("<img class = 'image-user-pic'>");
        $(".image-user-pic").attr("src", userurl);
        $("#div-crm-overlay-content-container").find('.modal-body #input-user-picture').unbind('change').bind('change', function () {
            var file = document.getElementById('input-user-picture').files[0];
            var fileDisplayArea = document.getElementById('div-user-pic');
            var imageType = /image.*/;
            if (file.type.match(imageType)) {
                var reader = new FileReader();
                console.log(reader);
                reader.onload = function (e) {
                    var cropdata = { //declaring crop data and calling crop view 
                        'parent': that,
                        'src': reader.result,
                        'el': fileDisplayArea,
                        'callback': function (val) {
                            getCrop(val, parent);
                        }
                    }
                    var imagecrop = new ImgCrpView(cropdata);
                }
                reader.readAsDataURL(file);
            } else {
                fileDisplayArea.innerHTML = "File not supported!"
                that.picData = "";
            }

        });

        $("#div-crm-overlay-content-container").find('.modal-body #input-user-picture').unbind('click').bind('click', function () {
            //to reload original image on rerendering the same pic

            if (document.getElementById('input-user-picture').files[0] != "" || document.getElementById('input-user-picture').files[0] != undefined)
                $('#input-user-picture').val("");
        });
    }
}



function getCrop(data, parent) {
    var that = parent;
    var fileDisplayArea = document.getElementById('div-user-pic');
    fileDisplayArea.innerHTML = "";
    var img = new Image();
    img.src = data;
    img.className = "img-user-pic";
    fileDisplayArea.appendChild(img);
    $("#div-user-pic").append("<div id='div-crop-ok' class='div-btn-crop gmText100 gmBGphysicianvisit gmBtn gmFontWhite gmTextCenter gmText150'> Ok</div>");
    var dataURI = data;
    var binary = atob(dataURI.split(',')[1]);
    var array = [];
    for (var i = 0; i < binary.length; i++)
        array.push(binary.charCodeAt(i));
    var picture = new Blob([new Uint8Array(array)], {
        type: 'image/jpeg'
    });
    updatePic(picture);
}

function updatePic(picfile) {
    var that = this;
    var $form = $("#frm-user-picture");
    var params = $form.serializeArray();
    this.picData = new FormData();
    this.picData.append('file', picfile, window.localStorage.getItem("partyID") + "-ProfilePic.jpeg");
    $.each(params, function (i, val) {
        if (val.name != "file")
            that.picData.append(val.name, val.value);
    });
    console.log(JSON.stringify(that.picData))
    console.log($("#div-crop-ok").length)
    $("#div-crm-overlay-content-container #div-crop-ok").unbind('click').bind('click', function () {
        if (that.picData != "" && that.picData != undefined) {
            that.picData.append('refid', window.localStorage.getItem("partyID"));
            that.picData.append('refgrp', '103112');
            that.picData.append('token', localStorage.getItem('token'));
            that.picData.append('fileid', '');
            that.picData.append('reftype', '91182');
            that.picData.append('filetitle', 'User Profile Picture');
            console.log(this.picData)

            $.ajax({
                url: URL_WSServer + 'uploadedFile/crmfileupload',
                data: that.picData,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                success: function (result) {
                    console.log("Pic Uploaded >>> " + JSON.stringify(result));
                    var token = localStorage.getItem("token");
                    var input = {
                        "token": token,
                        "refid": window.localStorage.getItem("partyID"),
                        "filename": window.localStorage.getItem("partyID") + "-ProfilePic.jpeg",
                        "deletefl": "",
                        "refgroup": "103112",
                        "filetitle": "User Profile Picture - " + window.localStorage.getItem("userName")
                    };
                    console.log(GM.Global.UserPicID)
                    if (GM.Global.UserPicID)
                        input['fileid'] = GM.Global.UserPicID;
                    console.log(GM.Global.UserPicID);
                    console.log(input);
                    fnGetWebServerData("file/savefile", undefined, input, function (fileData) {
                        console.log(fileData);
                        if (fileData != null) {
                            console.log(fileData);
                            GM.Global.UserPicID = fileData.fileid;
                            $("#ul-header-right-pic").find("img").removeAttr("onerror").css("display", "inline-block");
                            $("#mobile-menu-top-user").find("img").removeAttr("onerror").css("display", "inline-block");
                            $("#ul-header-right-pic").find("img").attr("src", URL_CRMFileServer + fileData.refgroup + "/" + fileData.refid + "/" + fileData.filename + "?num=" + Math.random());
                            $("#mobile-menu-top-user").find("img").attr("src", URL_CRMFileServer + fileData.refgroup + "/" + fileData.refid + "/" + fileData.filename + "?num=" + Math.random());


                        }
                        $("#ul-header-right-pic").find("img").attr("onerror", "this.style.display = 'none'");
                        $("#mobile-menu-top-user").find("img").attr("onerror", "this.style.display = 'none'");
                    });
                },
                error: function (model, response, errorReport) {
                    console.log(model);
                    console.log("Pic Upload Failed >>> " + JSON.stringify(model));
                }
            });
        }

        hidePopup();

    });
}

function initiateHelpers() {
    Handlebars.registerHelper("math", function (lvalue, operator, rvalue, options) {
        lvalue = parseFloat(lvalue);
        rvalue = parseFloat(rvalue);

        return {
            "+": lvalue + rvalue,
            "-": lvalue - rvalue,
            "*": lvalue * rvalue,
            "/": lvalue / rvalue,
            "%": lvalue % rvalue
        }[operator];
    });
    Handlebars.registerHelper('ifEquals', function (arg1, arg2, options) {
        return (arg1 == arg2) ? options.fn(this) : options.inverse(this);
    });
    Handlebars.registerHelper('iftrue', function (v1, v2, options) {
        var inverse = options.inverse,
            fn = options.fn;

        if (v1 === v2) {
            return fn(this);
        }
        return inverse(this);
    });
    Handlebars.registerHelper('ifanytrue', function (v1, v2, options) {
        var inverse = options.inverse,
            fn = options.fn;

        if (_.contains(v2.split('/'), v1)) {
            return fn(this);
        }
        return inverse(this);
    });

    Handlebars.registerHelper('iffalse', function (v1, v2, options) {
        var inverse = options.inverse,
            fn = options.fn;

        if (v1 !== v2) {
            return fn(this);
        }
        return inverse(this);
    });

    Handlebars.registerHelper('ifgreater', function (v1, v2, options) {
        var inverse = options.inverse,
            fn = options.fn;

        if (v1 > v2) {
            return fn(this);
        }
        return inverse(this);
    });

    Handlebars.registerHelper("formatDate", function (dt) {
        if (dt != "" && dt != "null" && dt != undefined) {
            var dateArray = dt.split("-");
            //        console.log("dateArray"+dateArray);
            var year = dateArray[0];
            var month = dateArray[1];
            var date = dateArray[2];

            var fmtDate = month + "/" + date + "/" + year;
            if (month != undefined && date != undefined)
                return fmtDate;
            else
            if (dt == "aN/aN/NaN")
                return "";
            else
                return dateArray;
        }
    });

    Handlebars.registerHelper("formatPhoneNumber", function (phoneNumber) {
        return formatPhoneNumber(phoneNumber)
//        phoneNumber = phoneNumber.toString();
//        if (phoneNumber.indexOf("-") >= 0) {
//            return phoneNumber;
//        } else {
//            phoneNumber = phoneNumber.replace(/(\d\d\d)(\d\d\d)(\d\d\d\d)/, "$1-$2-$3");
//            return phoneNumber;
//        }

        //        return "(" + phoneNumber.substr(0,3) + ") " + phoneNumber.substr(3,3) + "-" + phoneNumber.substr(6,4);
    });

    Handlebars.registerHelper("AMPM", function (time) {
        if (time.value !== "") {
            var hours = time.split(":")[0];
            var minutes = time.split(":")[1];
            var suffix = hours >= 12 ? "PM" : "AM";
            hours = hours % 12 || 12;
            hours = hours < 10 ? "0" + hours : hours;

            var displayTime = hours + ":" + minutes + " " + suffix;
            return displayTime;
        }
    });

    //Price Format for Pricing Tool
    Handlebars.registerHelper("formatPrice", function (price) {

        price = price.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        return price;
    });
    // Function for return the Empty value if the value passed as null/undefined/empty.
    Handlebars.registerHelper("parseNull", function (value) {
        return parseNull(value);
    });
	
	 /* 
     ifavailable() -To compare the passing string with the GM.Global.PRSYSDIVISION global varaible for pricing module
     Author: Karthik
     */
     Handlebars.registerHelper('ifavailable', function (v1,v2, options) {
            var inverse = options.inverse;
            var fn = options.fn;
            var sysIndex = GM.Global.PRSYSDIVISION.indexOf(v1);
            var sysFlag= "";
            if (sysIndex < 0  ){
                sysFlag = "N";
            }else{
                sysFlag = "Y";
            }
            if (sysFlag === v2) {
                return fn(this);
            }
            return inverse(this);
        });
		
}

function formatPhoneNumber(phoneNumber) {
    phoneNumber = phoneNumber.toString();
    if (phoneNumber.indexOf("-") >= 0) {
        return phoneNumber;
    } else {
        phoneNumber = phoneNumber.replace(/(\d\d\d)(\d\d\d)(\d\d\d\d)/, "$1-$2-$3");
        return phoneNumber;
    }
}
    
// Function for return the Empty value if the value passed as null/undefined/empty.
function parseNull(value) {
    return (value == undefined || value == null || value == "") ? "" : value.trim();
}

function getDateByFormat(dt) {
    if (dt != "" && dt != "null" && dt != undefined) {
        if (dt.indexOf("-") >= 0)
            var dateArray = dt.split("-");
        else if (dt.indexOf("/") >= 0)
            var dateArray = dt.split("/");
        else if (dt.indexOf(",") >= 0)
            var dateArray = dt.split(",");
        //        console.log("dateArray"+dateArray);
        if (dateArray != undefined) {
            var year = dateArray[0];
            var month = dateArray[1];
            var date = dateArray[2];

            var fmtDate = month + "/" + date + "/" + year;
            if (month != undefined && date != undefined)
                return fmtDate;
            else
                return dateArray;
        }
    }
}

function convertTime(data) {
    var time = data.toString();
    var lastIndex = time.lastIndexOf(" ");
    time = time.substring(0, lastIndex);
    var d = new Date(time);
    console.log(d);
    var ndate = d.getDate();
    var nmonth = d.getMonth();
    var nyear = d.getFullYear();
    var hh = d.getHours();
    var m = d.getMinutes();
    var s = d.getSeconds();
    var dd = "AM";
    var h = hh;
    if (h >= 12) {
        h = hh - 12;
        dd = "PM";
    }
    if (h == 0) {
        h = 12;
    }
    h = h < 10 ? "0" + h : h;
    m = m < 10 ? "0" + m : m;
    s = s < 10 ? "0" + s : s;
    var pattern = new RegExp("0?" + hh + ":" + m + ":" + s);
    var replacement = ndate + "-" + nmonth + "-" + nyear + " " + h + ":" + m;
    //    replacement += " "+s;
    //    replacement += " "+dd;
    console.log(replacement);
    return replacement;

    //    var ampm = (d.getHours() >= 12) ? "PM" : "AM";
    //    var hours = (d.getHours() >= 12) ? d.getHours()-12 : d.getHours();
    //
    //    return hours+' : '+d.getMinutes()+' '+ampm;

}

function ampm(time) {
    if (time && time != "") {
        var hours = time.split(":")[0];
        var minutes = time.split(":")[1];
        var suffix = hours >= 12 ? "PM" : "AM";
        hours = hours % 12 || 12;
        hours = hours < 10 ? "0" + hours : hours;

        var displayTime = hours + ":" + minutes + " " + suffix;
        return displayTime;
    } else
        return "";
}

function dateToTime(data) {
    console.log("dateTOTime" + data)
    var date = data.toString();
    var lastIndex = date.lastIndexOf(" ");
    date = date.substring(0, lastIndex);
    var time = new Date(date);
    var hours = time.getHours();
    hours = ("0" + hours).slice(-2);
    var minutes = time.getMinutes();
    minutes = ("0" + minutes).slice(-2);
    var fmtTime = hours + ":" + minutes;

    if (fmtTime && fmtTime != "aN:aN")
        return fmtTime;
    else
        return "";
}

//To hide/show mobile menu bar
$(document).on("click", function (event) {
    if (event.target.id == "button-mobile-menu")
        $("#div-crm-tab").addClass("show-panel");
    else
        $("#div-crm-tab").removeClass("show-panel");
    
    // Move back while click on panel title
    if (event.target.id == "div-crm-panel-title") {
        if($("#crm-btn-back").is(':visible'))
           window.history.back();
    }
});

function getAllowedTasks(userid) {
    if (GM.Global.UserAccess != undefined) {

    }
}
// To show popup box
function showPopup() {
    $('.div-overlay').show();
    $('.div-crm-overlay-content-container').addClass('hide');
    $('#div-crm-overlay-content-container').removeClass('hide');
    $(".modal-footer").removeClass("hide");

    $('.div-crm-overlay-content-container button[data-dismiss="modal"]').each(function () {
        $(this).unbind('click').bind('click', function () {
            hidePopup();
        });
    });
}

// To hide popup box
function hidePopup() {
    $('.div-overlay').hide();
    $('.div-crm-overlay-content-container').addClass('hide');
    $('.hideDefaultBtn').addClass('hide');
}

// Create select options box
function fnCreateOptions(select, arrData) {
    arrData = arrayOf(arrData);
    for (var i = 0; i < arrData.length; i++) {
        var option = document.createElement('option');
        option.value = arrData[i].ID;
        option.innerHTML = arrData[i].Name;
        $(select).append(option);
    }
}

// Get surgeon detail info
function getSurgeonPartyInfo(npid, callback) {
    var input = {
        "token": localStorage.getItem("token"),
        "npid": npid
    };
    var surgeonData;
    fnGetWebServerData("search/npid", "gmPartyBasicVO", input, function (data) {
        callback(data);
    }, true);
}

// variable for success/error message
var op = 1,
    toErr;
var aniErr = setInterval(function () {
    if (op == 1) {
        op = 0.75;
    } else {
        op = 1;
    }
    $('.alert').animate({
        opacity: "" + op + ""
    }, "slow");
}, 1000);

// show failure message
function showError(errorMsg, hideAfter) {
    displayMessage(errorMsg, 'alertError', hideAfter);
    $("#fa-close-alert").removeClass("hide");
}

// show messages
function showSuccess(successMsg, hideAfter) {
    displayMessage(successMsg, 'alertSuccess', hideAfter);
    $("#fa-close-alert").removeClass("hide");
}

// show success message
function showMessages(alertMsg, hideAfter) {
    displayMessage(alertMsg, 'alertMessages', hideAfter);
    $("#fa-close-alert").removeClass("hide");
}

// Display Message from the page
function displayMessage(Message, Type, hideAfter) {
    window.clearTimeout(toErr);

    $('.alert').removeClass().addClass('alert ' + Type).html(Message.replace(/\n/g, '<br>'));
    toErr = setTimeout(function () {
        hideMessages();
    }, 5000)

}

// Hide Message from the page
function hideMessages() {
    //    window.clearTimeout(toErr);
    //    var timeOutTime = 0, animateTime = 100;
    //    if($('.alert').hasClass('alertSuccess')) {
    //        timeOutTime = 1000;
    //        animateTime = 1000;
    //    } 
    //        
    //    toErr = setTimeout(function() {
    $('.alert').slideUp('fast', function () {
        $(this).css("display", "block").addClass('hide');
        $("#fa-close-alert").addClass("hide");
    })
    //    }, timeOutTime)  
    //     

}

// Check the user is salesrep or not
function isRep() {

    var acid, deptid = "";

    var result = false;

    if (localStorage.getItem("acid"))
        acid = localStorage.getItem("acid");

    if (localStorage.getItem("deptid"))
        deptid = localStorage.getItem("deptid");

    if (deptid.toUpperCase() == "S") {
        switch (acid) {
            case "1":
            case "2":
                console.log("It is a Rep");
                result = true;
                break;
            default:
                console.log("It is NOT a Rep because acid is " + acid);
                result = false;
        }
    } else {
        console.log("It is NOT a Rep because deptid is " + deptid);
        result = false;
    }

    return result;
}
// Check the user is salesrep or not
function isADVP() {
    var acid, deptid = "";
    var result = false;
    if (localStorage.getItem("acid"))
        acid = localStorage.getItem("acid");
    if (localStorage.getItem("Dept"))
        Dept = localStorage.getItem("Dept");
    if (Dept == "2005") {
        switch (acid) {
            case "3": // AD Filter Condition
            case "4": // VP Filter Condition
            case "6": // other AD's like Kirk Tovey want to see other region
                result = true;
                break;
            default:
                result = false;
        }
    } else {
        result = false;
    }
    return result;
}

//Sets the right tab with focal look
function colorControl(e, groupclass, commonclass, changeclass) {
    var ctrl = e.currentTarget;

    if ($("." + groupclass).hasClass(commonclass))
        $("." + groupclass).removeClass(commonclass);

    $("." + groupclass).addClass(commonclass).removeClass(changeclass);
    $(ctrl).removeClass(commonclass).addClass(changeclass);
}

// Module level inside the modules
function fnGetAccessInfo(moduleID) {
    var userAccessInfo = $.parseJSON(localStorage.getItem('userTabs'));
    return _.findWhere(userAccessInfo, {
        "functionID": "CRM-" + moduleID
    });
}

function mailDetection(containerSubID) {
    $("#txt-popup-mail-" + containerSubID).bind("keyup", function (e) {
        var searchDataSpace = e.keyCode;
        var strSearchData = $(e.currentTarget).val();
        var intWordLength = strSearchData.length;
        var strSearchLastWord = strSearchData.slice(-1);

        if (searchDataSpace == 32 || searchDataSpace == 13 || strSearchLastWord == ',' || strSearchLastWord == ';' || strSearchLastWord == ' ') {
            console.log(strSearchData);
            strSearchData = strSearchData.replace(',', '').replace(';', '').replace(' ', '');
            var strEmailID = strSearchData;
            var atpos = strEmailID.indexOf("@");
            var dotpos = strEmailID.lastIndexOf(".");

            if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= strEmailID.length) {
                console.log("Not a valid e-mail address");
            } else {
                var parentDiv = $(document.createElement('div')).attr({
                    'email': strEmailID,
                    'class': 'div-parent-div div-parent-div-' + containerSubID + ' gmBtn gmAlignHorizontal gmMargin2p'
                });
                var parentDivSpan = $(document.createElement('span')).attr('class', 'spn-parent-span');
                var spanDivText = $(document.createElement('div')).attr('class', 'div-child-text gmAlignHorizontal');
                var spanDivClose = $(document.createElement('div')).attr('class', 'spn-spandiv-close fa fa-times-circle fa-lg gmAlignHorizontal gmMarginLeft5p');
                $(spanDivText).text(strEmailID);
                $("#li-popup-mail-" + containerSubID + "-container").append(parentDiv);
                $(parentDiv).append(parentDivSpan);
                $(parentDivSpan).append(spanDivText);
                $(parentDivSpan).append(spanDivClose);
                $(e.currentTarget).val('');
                $("#div-crm-overlay-content-container2 .modal-body .spn-spandiv-close").unbind("click").bind("click", function (event) {
                    event.stopPropagation();
                    $(event.currentTarget).parent().parent().remove();
                });
                $("#div-crm-overlay-content-container2 .div-parent-div-" + containerSubID).unbind("click").bind("click", function (event) {
                    $("#txt-popup-mail-" + containerSubID).val($(event.currentTarget).text().trim());
                    $(event.currentTarget).remove();
                });
            }
        }
    });
}

function commonSetUp() {
    GM.Global.UserData = localStorage.getItem("userData");
    GM.Global.SalesRep = isRep();
    GM.Global.UserName = window.localStorage.getItem("fName") + " " + window.localStorage.getItem("lName");
    crmUserTabs = $.parseJSON(localStorage.getItem("userTabs"));
}

/* Get Local Data through AJAX */
function fnGetLocalData(path, callback) {

    $.ajax({
        url: "datafiles/conf.json",
        method: "GET",
        datatype: "application/json",
        success: function (data) {
            console.log(JSON.stringify(data));
        },
        error: function () {
            console.log("Error");
        }
    });
}

// To show popup box
function showAddressPopup() {
    $('.div-overlay').show();
    $('.div-crm-overlay-content-container').addClass('hide');
    $('#div-crm-overlay-content-container3').removeClass('hide');
    $(".modal-footer").removeClass("hide");

    $('.div-crm-overlay-content-container button[data-dismiss="modal"]').each(function () {
        $(this).unbind('click').bind('click', function () {
            hideAddressPopup();
        });
    });
}

function showRepInfoPopup() {
    $('.div-overlay').show();
    $('.div-crm-overlay-content-container').addClass('hide');
    $('#div-crm-overlay-content-container4').removeClass('hide');


    $('.div-crm-overlay-content-container button[data-dismiss="modal"]').each(function () {
        $(this).unbind('click').bind('click', function () {
            hideRepInfoPopup();
        });
    });
}

function hideRepInfoPopup() {
    $('.div-overlay').hide();
    $('.div-crm-overlay-content-container').addClass('hide');
}
// To hide popup box
function hideAddressPopup() {
    $('.div-overlay').hide();
    $('.div-crm-overlay-content-container').addClass('hide');
}


function getSelectState() {
    var that = this;
    $.get("datafiles/state.json", function (data) {
        console.log(data)
        var arrSelectOpt = new Array();
        arrSelectOpt.push({
            "ID": "",
            "Name": "Select"
        });
        data = arrSelectOpt.concat(_.sortBy(data, function (data) {
            return data.codenm;
        }));
        var strSelectOptions = JSON.stringify(data).replace(/codeid/g, 'ID').replace(/codenm/g, 'Name');
        that.statenm = $.parseJSON(strSelectOptions);

        $('.select-address-state').each(function () {
            fnCreateOptions(this, that.statenm);
            $(this).val($(this).attr("value"));
        });

    });


}

function getSelectCountry() {
    var that = this;
    $.get("datafiles/country.json", function (data) {
        var arrSelectOpt = new Array();
        arrSelectOpt.push({
            "ID": "",
            "Name": "Select"
        });
        //   GM.Global.Country = arrSelectOpt.concat(GM.Global.Country);
        data = arrSelectOpt.concat(_.sortBy(data, function (data) {
            return data.codenm;
        }));
        var strSelectOptions = JSON.stringify(data).replace(/codeid/g, 'ID').replace(/codenm/g, 'Name');
        that.countrynm = $.parseJSON(strSelectOptions);
        $('.select-address-country').each(function () {
            fnCreateOptions(this, that.countrynm);
            $(this).val($(this).attr("value"));
        });
    });

}

function resizeFont() {
    $('.li-index-number').each(function (i, box) {
        var width = $(box).width(),
            html = '<span style="white-space:nowrap">',
            line = $(box).wrapInner(html).children()[0],
            n = 50;
        $(box).css('font-size', n);
        while ($(line).width() > width) {
            $(box).css('font-size', --n);
        }
        $(box).text($(line).text());
    });
}

function resizeHeadWidth(parentClass, rchild, lchild) {
    var fWidth = $(parentClass).width();
    var rWidth = $(rchild).width();
    var newlWidth = fWidth - rWidth - 2;
    $(lchild).css({
        "width": newlWidth + "px"
    });
    $(lchild).addClass("gmEllipses");
}


function createEvent(data) {
    if (GM.Global.Device) { // Only Devices!
        var sdate = new Date(data.actstartdate + " " + data.actstarttime);
        var edate = new Date(data.actenddate + " " + data.actendtime);
        var title = data.actnm;
        var address;
        var location = "Unavailable"
        if (data.arrgmcrmaddressvo[0] != undefined) {
            address = data.arrgmcrmaddressvo[0];
            location = address.add1 + " " + address.add2 + " " + address.city + " " + address.statenm + " " + address.zip;
        }
        var notes = "";
        data.arrgmactivityattrvo = arrayOf(data.arrgmactivityattrvo);
        if (data.actid != undefined && data.arrgmactivityattrvo.length > 0) {
            var notes = _.filter(data.arrgmactivityattrvo, function (data) {
                return data.attrtype == 105585;
            });
            if (notes.length > 0)
                notes = notes[0].attrval;
        }
        var success = function (message) {};
        var error = function (message) {};
        window.plugins.calendar.createEvent(title, location, notes, sdate, edate, success, error);
    }
}

function dateOfnPastDays(countOfDays) {
    var d = new Date();
    d.setDate(d.getDate() - countOfDays);

    var dd = d.getDate();
    var mm = d.getMonth() + 1;
    var yyyy = d.getFullYear();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    d = mm + '/' + dd + '/' + yyyy;
    return d;
}

// Function to setup user info on Entery into CRM
function userInfoSetup(data) {
    window.localStorage.setItem("token", data.token);
    window.localStorage.setItem("strShortName", data.shname);
    window.localStorage.setItem("userName", data.usernm);
    window.localStorage.setItem("userID", data.userid);
    window.localStorage.setItem("userCompany", data.companyid);
    window.localStorage.setItem("userDivision", data.divid);
    window.localStorage.setItem("partyID", data.partyid);
    window.localStorage.setItem("acid", data.acid);
    window.localStorage.setItem("Dept", data.deptseq);
    window.localStorage.setItem("deptid", data.deptid);
    window.localStorage.setItem("fName", data.fname);
    window.localStorage.setItem("lName", data.lname);
    window.localStorage.setItem("repDivision", data.repdiv);
    window.localStorage.setItem("cmpcurrsmb", data.cmpcurrsmb);
    GM.Global.Token = window.localStorage.getItem("token");
    GM.Global.SalesRep = isRep();

    GM.Global.UserName = window.localStorage.getItem("fName") + " " + window.localStorage.getItem("lName");
    GM.Global.UserID = window.localStorage.getItem("userID");
}

function prepareExcel(data, callback) {
    require(['jszip'], function (jszip) {
        window.JSZip = jszip;
        require(['xlsx'], function () {
            // Do what you want with XLSX
            var workbook = XLSX.read(data, {
                type: 'binary'
            });
            console.log(workbook)
            var XL_row_object = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[workbook.SheetNames[0]]); //read data from sheet 1 of the excel
            console.log("Data from Excel")
            console.log(JSON.stringify(XL_row_object));
            if (XL_row_object.length)
                callback(false, XL_row_object);
            else
                callback(true); // No Entry
        });
    });
}

function showInfoPopup() {
    $('.div-overlay').show();
    $('.div-price-overlay-content-container').addClass('hide');
    $('#div-price-overlay-content-container5').removeClass('hide');


    $('.div-price-overlay-content-container button[data-dismiss="modal"]').each(function () {
        $(this).unbind('click').bind('click', function () {
            hideInfoPopup();
        });
    });
}

function hideInfoPopup() {
    $('.div-overlay').hide();
    $('.div-price-overlay-content-container').addClass('hide');
}

function showAccountInfoPopup() {
    $('.div-overlay').show();
    $('.div-price-overlay-content-container').addClass('hide');
    $('#div-price-overlay-content-container6').removeClass('hide');


    $('.div-price-overlay-content-container button[data-dismiss="modal"]').each(function () {
        $(this).unbind('click').bind('click', function () {
            hideAccountInfoPopup();
        });
    });
}

function hideAccountInfoPopup() {
    $('.div-overlay').hide();
    $('.div-price-overlay-content-container').addClass('hide');
}

function showFileUploadPopup() {
    $('.div-overlay').show();
    $('.div-price-overlay-content-container').addClass('hide');
    $('#div-price-overlay-content-container11').removeClass('hide');


    $('.div-price-overlay-content-container button[data-dismiss="modal"]').each(function () {
        $(this).unbind('click').bind('click', function () {
            hideFileUploadPopup();
        });
    });
}

function hideFileUploadPopup() {
    $('.div-overlay').hide();
    $('.div-price-overlay-content-container11').addClass('hide');
}

function showLoaderPopup() {
    console.log("stop");
    $('.div-overlay').show();
    $('.div-price-overlay-content-container').addClass('hide');
    $('#div-price-overlay-content-container9').removeClass('hide');


    $('.div-price-overlay-content-container button[data-dismiss="modal"]').each(function () {
        $(this).unbind('click').bind('click', function () {
            hideLoaderPopup();
        });
    });
}

function hideLoaderPopup() {
    $('.div-overlay').hide();
    $('.div-price-overlay-content-container').addClass('hide');
}

function showGroupPartInfoPopup() {
    $('.div-overlay').show();
    $('.div-price-overlay-content-container').addClass('hide');
    $('#div-price-overlay-content-container7').removeClass('hide');


    $('.div-price-overlay-content-container button[data-dismiss="modal"]').each(function () {
        $(this).unbind('click').bind('click', function () {
            hideGroupPartInfoPopup();
        });
    });
}

function hideGroupPartInfoPopup() {
    $('.div-overlay').hide();
    $('.div-price-overlay-content-container').addClass('hide');
}

function showPriceindicatorPopup() {
    $('.div-overlay').show();
    $('.div-price-overlay-content-container').addClass('hide');
    $('#div-price-overlay-content-container14').removeClass('hide');


    $('.div-price-overlay-content-container button[data-dismiss="modal"]').each(function () {
        $(this).unbind('click').bind('click', function () {
            hidePriceindicatorPopup();
        });
    });
}

function hidePriceindicatorPopup() {
    $('.div-overlay').hide();
    $('.div-price-overlay-content-container').addClass('hide');
}

function fnGetIMSetsTest(url, txtSearch, callback) {
    var that = this;
    var token = localStorage.getItem("token");
    var searchText = $("#div-selected-system-list input").val();
    var input = {
        "token": token,
        "searchtext": searchText
    };
    fnGetWebServerData("pricingRequest/pricingSystems/", "gmSystemVO", input, function (data) {
        var results = [];
        if (data != null) {
            var data = arrayOf(data);
            $.each(data, function (i, val) {
                results.push({
                    "ID": val.systemId,
                    "Name": val.systemName
                });
            });
        }
        callback(results);

    });
}

function fnGetIMSystem(url, txtSearch, callback) {
    var that = this;
    var token = localStorage.getItem("token");
    var searchText = $("#div-acc-selected-system-list input").val();
    var input = {
        "token": token,
        "searchtext": searchText
    };
    fnGetWebServerData("pricingRequest/pricingSystems/", "gmSystemVO", input, function (data) {
        var results = [];
        if (data != null) {
            var data = arrayOf(data);
            $.each(data, function (i, val) {
                results.push({
                    "ID": val.systemId,
                    "Name": val.systemName
                });
            });
        }
        callback(results);

    });
}


function fnGetPricingAccounts(url, txtSearch, callback) {
    var that = this;
    var token = localStorage.getItem("token");
    var acid = localStorage.getItem("acid");
    var deptid = localStorage.getItem("deptid");
    var userID = localStorage.getItem("userID");
    var searchText = $("#div-rep-account-list input").val();
    var cmpid = localStorage.getItem("cmpid");
    var input = {
        "token": token,
        "typeid": typeIdVal,
        "acid": acid,
        "deptid": deptid,
        "userid": userID,
        "searchtext": searchText,
        "cmpid": localStorage.getItem("cmpid")
    };
    console.log(input);
    fnGetWebServerData("pricingRequest/accountDrpDownData/", "gmPriceAccountTypeVO", input, function (data) {
        var results = [];
        if (data != null) {
            var data = arrayOf(data);
            $.each(data, function (i, val) {
                if (typeIdVal == "903107")
                    results.push({
                        "ID": val.accid,
                        "Name": val.accname
                    });
                else
                    results.push({
                        "ID": val.partyid,
                        "Name": val.accname
                    });
            });
        } else
            data = [];
        callback(results);
    });
}

function fnGetCodeLookUpValues(input, callback) {
    fnGetWebServerData("common/codelookuplist/", "gmCodeLookupVO", input, function (data) {
        if (data != null) {
            var drpDwn = [];
            _.each(data, function (d, i) {
                drpDwn.push({
                    "ID": d.codeid,
                    "Name": d.codenm
                });
                if ((i + 1) == data.length)
                    callback(drpDwn);
            })
        } else
            callback([]);
    });
}

function showCancelPriceRequestPopup() {
    $('.div-overlay').show();
    $('.div-price-overlay-content-container').addClass('hide');
    $('#div-price-overlay-content-container8').removeClass('hide');


    $('.div-price-overlay-content-container button[data-dismiss="modal"]').each(function () {
        $(this).unbind('click').bind('click', function () {
            hideAccountInfoPopup();
        });
    });
}

function hideCancelPriceRequestPopup() {
    $('.div-overlay').hide();
    $('.div-price-overlay-content-container').addClass('hide');
}

function showImpactPopup() {
    $('.div-overlay').show();
    $('.div-price-overlay-content-container').addClass('hide');
    $('#div-price-overlay-content-container10').removeClass('hide');


    $('.div-price-overlay-content-container button[data-dismiss="modal"]').each(function () {
        $(this).unbind('click').bind('click', function () {
            hideInfoPopup();
        });
    });
}

function hideImpactPopup() {
    $('.div-overlay').hide();
    $('.div-price-overlay-content-container').addClass('hide');
}


function strToDate(dateStr) {
    // create a JavaScript date object from it
    var dateObj = new Date(dateStr);
    // use toString from datejs
    var formattedDate = dateObj.toDateString("mm/dd/yyyy");
    return formattedDate;
}

// to display custom msg on popup itself
function displayPopupMsg(Message) {
    window.clearTimeout(toErr);

    $('#div-crm-overlay-msg').removeClass('hide').html(Message.replace(/\n/g, '<br>'));
    toErr = setTimeout(function () {
        $('#div-crm-overlay-msg').addClass('hide').html("");
    }, 5000)

}

function ValidateFileType(filename) {
    var UPLOAD_WHITELIST = [".doc", ".docx", ".xls", ".xlsx", ".ppt", ".pptx", ".pdf", ".txt", ".rtf", ".jpg", ".gif", ".png", ".jpeg"]
    var bValid = true;
    var fileExtn = filename.slice((filename.lastIndexOf(".") - 1 >>> 0) + 1);
    if (UPLOAD_WHITELIST.indexOf(fileExtn) === -1) {
        bValid = false;
    }
    return bValid;

}

//function to convert copyright,registered and degree symbols
function uniToString(sName) {
    var retStr = sName;
    if (sName.indexOf("&reg;") > 0)
        retStr = sName.replace('&reg;', '\u00AE');
    else if (sName.indexOf("&copy;") > 0)
        retStr = sName.replace('&copy;', '\u00A9');
    else if (sName.indexOf("&deg;") > 0)
        retStr = sName.replace('&deg;', '\u00B0');

    return retStr;
}

function showMailInfoPopup() {
    $('.div-overlay').show();
    $('.div-price-overlay-content-container').addClass('hide');
    $('#div-price-overlay-content-container13').removeClass('hide');


    $('.div-price-overlay-content-container button[data-dismiss="modal"]').each(function () {
        $(this).unbind('click').bind('click', function () {
            hideGroupPartInfoPopup();
        });
    });
}

function hideshowMailInfoPopup() {
    $('.div-overlay').hide();
    $('.div-price-overlay-content-container').addClass('hide');
}

// FUNCTION TO SHOW THE PART GROUP DETAILS
function showPartDetailsPopup() {
    $('.div-overlay').show();
    $('.div-price-overlay-content-container').addClass('hide');
    $('#div-price-overlay-content-container12').removeClass('hide');


    $('.div-price-overlay-content-container button[data-dismiss="modal"]').each(function () {
        $(this).unbind('click').bind('click', function () {
            hidePartDetailsPopup();
        });
    });
}


function hidePartDetailsPopup() {
    $('.div-overlay').hide();
    $('.div-price-overlay-content-container').addClass('hide');
}

function hidePartSysDetailsPopup() {
    $('.div-overlay').hide();
    $('.div-price-overlay-content-container').addClass('hide');
}



/*fnGetRuleValues: function to get the rule value basedon the company
 * Parameter : Ruleid,RuleGroupid,company id
 */
function fnGetRuleValues(ruleid, rulegrpid, cmpid, callback) {
    var input = {
        "token": localStorage.getItem("token"),
        "ruleid": ruleid,
        "rulegrpid": rulegrpid,
        "cmpid": cmpid
    };
    fnGetWebServerData("common/fetchRuleValue", "gmRulesVO", input, function (data) {
        callback(data);
    });
}



function showSalesAnalysisPopup() {
    $('.div-overlay').show();
    $('.div-price-overlay-content-container').addClass('hide');
    $('#div-price-overlay-content-container-sales-analysis').removeClass('hide');


    $('.div-price-overlay-content-container button[data-dismiss="modal"]').each(function () {
        $(this).unbind('click').bind('click', function () {
            hideInfoPopup();
        });
    });
}

function hideSalesAnalysisPopup() {
    $('.div-overlay').hide();
    $('.div-price-overlay-content-container').addClass('hide');
}