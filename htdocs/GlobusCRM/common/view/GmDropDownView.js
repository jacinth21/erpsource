/**********************************************************************************
 * File:        GmDropDownView.js
 * Description: Subview 
 * Version:     1.0
 * Author:      praja
 **********************************************************************************/

define([
        'jquery', 'underscore', 'backbone', 'handlebars',
        'global','dropdownmodel','dropdowncollection'

        ], function ($, _, Backbone, Handlebars, Global, DropDownModel, DropDownCol) {

	'use strict';
	
	var DropDownView = Backbone.View.extend({

		events : {
		 "click .div-dropdown-main" : "toggleDropdown",
		 "click .div-dropdown-list li" : "select"
		 // "click div, i, ul, li" : "negligible"
			
		},
		
		template: fnGetTemplate(URL_Common_Template,"GmDropDown"),
		template_list: fnGetTemplate(URL_Common_Template,"GmDropDownList"),
		
		initialize: function(options) {
			this.el = options.el;
			this.ListData = options.data;
			this.DefaultID = options.DefaultID;
			this.DefaultName = options.DefaultName;
			this.render();    
		},
		
		

		render: function () {
			var that = this, defaultIndex, defaultName;
			$(this.el).html(this.template());
			this.$(".div-dropdown-list").hide();
	
			this.collectionList = new DropDownCol(this.ListData);
			var listModel = new DropDownModel();
			var target = this.$(".div-dropdown-list").find('ul');

			if(this.DefaultName!=undefined) {
				defaultIndex = "Name";
				defaultName = this.DefaultName;
				
			}
			else {
			 	defaultIndex = "ID";
			 	defaultName = this.DefaultID;
			}
			
			this.collectionList.each(function (model) {
				if(model.get(defaultIndex)!=defaultName) {
					target.append(that.template_list(model.toJSON()));
				}
				else {
					model.set("SelectedIndex", true);
					that.$(".div-dropdown-text").text(model.get("Name"));
				}

			});

			this.cls = parseInt(Math.random()*1000000).toString();

			this.$el.find("*").each(function() {
				$(this).addClass(that.cls);
			});

			$(document).bind("click."+that.cls, function(event) {
				if(!$(event.target).hasClass(that.cls)) {
					that.$(".div-arrow-dropdown i").removeClass("fa-caret-down").addClass("fa-caret-right");
					that.$(".div-dropdown-list").hide();
				}
				
			});

			return this;
		},

		toggleDropdown: function() {
			if(this.$(".div-arrow-dropdown i").hasClass("fa-caret-right")) {
				this.$(".div-arrow-dropdown i").removeClass("fa-caret-right").addClass("fa-caret-down");
				this.$(".div-dropdown-list").show();
			}
			else {
				this.$(".div-arrow-dropdown i").removeClass("fa-caret-down").addClass("fa-caret-right");
				this.$(".div-dropdown-list").hide();
			}
		},

		select: function(e) {
			var id = e.currentTarget.id, that = this;
			var target = this.$(".div-dropdown-list").find('ul');
			var SelectedId, SelectedName;

			target.empty();
			this.collectionList.each(function (model) {
				if(model.get("SelectedIndex")){
					model.set("SelectedIndex",false);
					target.append(that.template_list(model.toJSON()));
				}
				else if(model.get("ID") == id) {
					model.set("SelectedIndex", true);
					that.$(".div-dropdown-text").text(model.get("Name"));
					SelectedId = model.get("ID");
					SelectedName = model.get("Name");
				}
				else {
					target.append(that.template_list(model.toJSON()));
				}
			});

			this.toggleDropdown();

			(that.el).trigger("onchange", [SelectedId,SelectedName]);
			
		},

		getCurrentValue: function () {
			var SelectedId, SelectedName;
			var that = this;
			this.collectionList.each(function (model) {
				if(model.get("SelectedIndex")){
					SelectedId = model.get("ID");
					SelectedName = model.get("Name");
				}
			});

			return [SelectedId, SelectedName];
		},

		negligible: function(event) {
			event.stopImmediatePropagation();
		},

		close: function() {
			console.log("closing...");
			$(document).unbind("click."+this.cls);
			this.unbind();
			this.undelegateEvents();
		}
		
	});

	return DropDownView;
});
