/**********************************************************************************
 * File:        GmDateRangeView.js
 * Description: Subview 
 * Version:     1.0
 * Author:      ssethurajan
 **********************************************************************************/

define([
        'jquery', 'underscore', 'backbone', 'handlebars',
        'global','datemodel','fieldview'

        ], function ($, _, Backbone, Handlebars, Global, DateModel, FieldView) {

	'use strict';
	
	var DateRangeView = Backbone.View.extend({

		events : {
			 "change .div-item-date": "passDates",
			 "click .div-item-date": "focusDate",
			 "blur .dte-picker": "populateDate"
		},
		
		template: fnGetTemplate(URL_Common_Template,"GmDateRange"),
		
		initialize: function(options) {
			this.el = options.el;
			this.callback = options.callback;
			this.dateformat = options.dateformat;
			this.caption1 = options.caption1;
			this.caption2 = options.caption2;
			this.date1 = options.date1;
			this.date2 = options.date2;
			this.usage = options.usage;
			this.render(options);
            
		},
		

		render: function (options) {
			$(this.el).html(this.template(options));

			if (this.usage == 1)
				this.$("#div-item-todate").hide();
			this.setDateOf(this.$("#input-item-fromdate"),this.date1);
			this.setDateOf(this.$("#input-item-todate"),this.date2);
			return this;
		},

		focusDate: function (event) {
			if(!$(event.target).hasClass("dte-picker")) {
				var keyID = (event.currentTarget.id).replace("div-","input-");
				this.$("#txt-" + keyID).toggleClass("hide");
				this.$("#" + keyID).toggleClass("hide");

				console.log($("#txt-" + keyID).hasClass("hide"));
				if(this.$("#txt-" + keyID).hasClass("hide")) {
					this.$("#" + keyID).focus();
				}
				else {
					if(keyID.indexOf("from") >= 0) 
						this.$("#txt-" + keyID).val(this.getDates()[0]);
					else 
						this.$("#txt-" + keyID).val(this.getDates()[1]);
				}
			}
			
		},

		populateDate: function (event) {
			var keyID = event.currentTarget.id;
			this.$("#txt-" + keyID).toggleClass("hide");
			this.$("#" + keyID).toggleClass("hide");
			if(keyID.indexOf("from") >= 0) 
				this.$("#txt-" + keyID).val(this.getDates()[0]);
			else 
				this.$("#txt-" + keyID).val(this.getDates()[1]);
		},
		
		passDates: function () {
			var date1 = this.getDates()[0], date2 = this.getDates()[1];
			console.log(date1, date2);
            $(this.el).trigger("onchange", [date1, date2]);
			
		},

		setDateOf: function(elem,date) {
			var strDateVal = new Date().toISOString().substring(0, 10);
			if(date!=undefined) {
				var strDate, strMonth, strYear;
				var arrDate = date.split("-");

				switch(this.dateformat) {
		            case "mm-dd-yyyy":
		            	strDate = arrDate[1];
		            	strMonth = arrDate[0];
		            	strYear = arrDate[2];
						break;
		            case "mm-yyyy-dd":
		            	strDate = arrDate[2];
		            	strMonth = arrDate[0];
		            	strYear = arrDate[1];
						break;
		            case "dd-mm-yyyy":
		            	strDate = arrDate[0];
		            	strMonth = arrDate[1];
		            	strYear = arrDate[2];
						break;
		            case "dd-yyyy-mm":
		            	strDate = arrDate[0];
		            	strMonth = arrDate[2];
		            	strYear = arrDate[1];
						break;
		            case "yyyy-mm-dd":
		            	strDate = arrDate[2];
		            	strMonth = arrDate[1];
		            	strYear = arrDate[0];
						break;
		            case "yyyy-dd-mm":
		            	strDate = arrDate[0];
		            	strMonth = arrDate[2];
		            	strYear = arrDate[1];
						break;
	            }

	            strDateVal = strYear + "-" + strMonth + "-" + strDate;
			}
			$(elem).siblings("input").val(strDateVal);
            $(elem).val(strDateVal);
           
		},

		getDates: function() {
			var fdate = new Date(this.$("#input-item-fromdate").val());
            var tdate = new Date(this.$("#input-item-todate").val());
            var fmm = fdate.getMonth()+1, fdd = fdate.getDate(), fyyyy = fdate.getFullYear();
            var tmm = tdate.getMonth()+1, tdd = tdate.getDate(), tyyyy = tdate.getFullYear();
            var fromdate, todate;
            switch(this.dateformat) {
	            case "mm-dd-yyyy":
	            	fromdate = fmm + "-" + fdd + "-" + fyyyy;
	            	todate = tmm + "-" + tdd + "-" + tyyyy;
					break;
	            case "mm-yyyy-dd":
	            	fromdate = fmm + "-" + fyyyy + "-" + fdd;
	            	todate = tmm + "-" + tyyyy + "-" + tdd;
					break;
	            case "dd-mm-yyyy":
	            	fromdate = fdd + "-" + fmm + "-" +  fyyyy;
	            	todate = tdd + "-" + tmm + "-" +  tyyyy;
					break;
	            case "dd-yyyy-mm":
	            	fromdate = fdd  + "-" + fyyyy + "-" + fmm;
	            	todate = tdd  + "-" + tyyyy + "-" + tmm;
					break;
	            case "yyyy-mm-dd":
	            	fromdate =  fyyyy + "-" + fmm + "-" + fdd;
	            	todate =  tyyyy + "-" + tmm + "-" + tdd;
					break;
	            case "yyyy-dd-mm":
	            	fromdate = fyyyy + "-" + fdd + "-" + fmm;
	            	todate = tyyyy + "-" + tdd + "-" + tmm;
					break;
            }

            return [fromdate,todate];
		}
		
		
		
	});

	return DateRangeView;
});
