/**********************************************************************************
 * File:        GmFiledView.js
 * Description: Contains the Login validation .
 *              This view is initiated when the blur event triggers, after the Login View is
 *              loaded.
 * Version:     1.0.0
 * Author:      praja
 **********************************************************************************/

define([
    'jquery', 'underscore', 'backbone', 'handlebars',
    'global', 'notification'
], 

function ($, _, Backbone, Handlebars, Global, Notification) {

    'use strict';
    
    var FieldView = Backbone.View.extend({
    
        events: {
            blur: "validate",
            
        },

	    initialize: function() {
            this.name = this.$el.attr("name");
            this.$msg = $('#spn-' + this.name);
        },
        
        //validate the username and password
        validate: function() {
            this.model.set(this.name, this.$el.val(), {validate:true});
            this.$msg.text(this.model.errors[this.name] || '');
        }

    });

	return FieldView;
});
