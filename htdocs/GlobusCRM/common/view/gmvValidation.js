/*
Name : ValidationView
Type : View
Decription : Common View for all validations
Author : praja
*/

define([
    'jquery', 'underscore', 'backbone',  'global'
], 

function ($, _, Backbone, Global) {

    'use strict';
    
    GM.View.Validation = Backbone.View.extend({
    
        events: {
            "blur *" : "validateInit",  
            "onchange .gmValidationError"  : "validateInit",
            "keyup .gmValidationError"  : "validateInit"
        },

	    initialize: function() {
            
        },
        
        //Initiate Validations
        validateInit: function(event) {
            var validationType = $(event.currentTarget).attr("validate");
            if(validationType!=undefined) {
                if(validationType.indexOf("select") >= 0 )
                    this.validateSelect(event);
                if(validationType.indexOf("required") >=0 )
                   this.validateRequied(event); 
                if(validationType.indexOf("search") >= 0 )
                    this.validateSearch(event);
                if(validationType.indexOf("email") >= 0 )
                    this.validateEmail(event);
                if(validationType.indexOf("phone") >= 0 )
                    this.validatePhone(event);
                if(validationType.indexOf("number") >= 0 )
                    this.validateNumber(event);
                if(validationType.indexOf("text") >= 0 )
                    this.validateText(event);
                if(validationType.indexOf("selpopup") >= 0 )
                    this.validateSelectPopup(event);
                if(validationType.indexOf("zip") >= 0 )
                    this.validateZip(event);
                
            }
        },
        
        //Validate Only the Required
        validateRequied: function(event) {
            var elemValue = $.trim($(event.currentTarget).val()).length;
            if(elemValue=="") 
                $(event.currentTarget).addClass('gmValidationError');
            else
                $(event.currentTarget).removeClass('gmValidationError');
        },
        
        //Validate whether the search is matching with the saved result
        validateSearch: function(event) {
            var targetInput =  $(event.currentTarget).find('input[type=text]').val();
            var actualInput =  $(event.currentTarget).attr($(event.currentTarget).attr('validate-key'));
            var check = $(event.currentTarget).attr("pdtvalidate");
            if(check == "Y"){
                if(targetInput =="" )
                     $(event.currentTarget).addClass('gmValidationError');
                else
                     $(event.currentTarget).removeClass('gmValidationError');
            }
            else{
            if(targetInput.trim()!=actualInput.trim())
                 $(event.currentTarget).addClass('gmValidationError');
            else
                 $(event.currentTarget).removeClass('gmValidationError');
                }
        },
        
        validateSelect: function(event) {
            var val = $(event.currentTarget).val().trim();
            if(val == "" || val == 0){
                var valAlt = $(event.currentTarget).attr("value");
                if(valAlt && valAlt!="")
                    $(event.currentTarget).removeClass('gmValidationError');
                else
                    $(event.currentTarget).addClass('gmValidationError');
            }
            else
                $(event.currentTarget).removeClass('gmValidationError');
            
        },
         validateZip: function(event) {
            if($(event.currentTarget).val().length < $(event.currentTarget).val().replace(/ /g,'').length ) 
                $(event.currentTarget).val($(event.currentTarget).val().replace(/ /g,'').length )
            if($(event.currentTarget).val() == ""){
                $(event.currentTarget).addClass('gmValidationError');
                $(event.currentTarget).attr("message","Please provide Zip")
            }
            else if($(event.currentTarget).val().length >10){
               $(event.currentTarget).addClass('gmValidationError');
                $(event.currentTarget).attr("message","Maximum length of the Zip should be 10 characters")
            }
             else
               $(event.currentTarget).removeClass('gmValidationError');  
                 
        },
        
        validateEmail: function(event) {
            var emailPattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
            
                if ($(event.currentTarget).val().trim() != "" && emailPattern.test($(event.currentTarget).val())!=true){
                        $(event.currentTarget).addClass('gmValidationError');
                    }
                else
                    $(event.currentTarget).removeClass('gmValidationError');
        },
        
        validateNumber: function(event) {
            var numberPattern = /^\d*[0-9](|.\d*[0-9]|,\d*[0-9])?$/;
            console.log(numberPattern.test($(event.currentTarget).val()));
            if ($(event.currentTarget).val().trim() != "" && numberPattern.test($(event.currentTarget).val())!=true)
                $(event.currentTarget).addClass('gmValidationError');
            else
                $(event.currentTarget).removeClass('gmValidationError');
        },
        
        validateText: function(event) {
            var textPattern = /^[a-zA-Z]+$/;
            
                console.log(textPattern.test($(event.currentTarget).val()));
                if ($(event.currentTarget).val().trim() != "" && textPattern.test($(event.currentTarget).val())!=true)
                    $(event.currentTarget).addClass('gmValidationError');
                else
                    $(event.currentTarget).removeClass('gmValidationError');
        },
        
        validatePhone: function(event) {
            var phonePattern = new RegExp(/^[0-9- ]*$/);
            var val = $(event.currentTarget).val();
            var charTxt =  val.replace(/\D/g,'');
                if ($(event.currentTarget).val().trim() != "" && phonePattern.test($(event.currentTarget).val())!=true){
                        $(event.currentTarget).addClass('gmValidationError');
                    }
                else if ($(event.currentTarget).val().trim().length > 25)
                    $(event.currentTarget).addClass('gmValidationError');
                else if (parseNull(charTxt ) == "")
                    $(event.currentTarget).addClass('gmValidationError');
                else
                    $(event.currentTarget).removeClass('gmValidationError');
        },
        
        validateSelectPopup: function(event) {
             var value = $(event.currentTarget).text().trim().length;
            if (value<1)
                    $(event.currentTarget).addClass('gmValidationError');
            else
                $(event.currentTarget).removeClass('gmValidationError');
        },
        
        //Return boolean For all the whole. Is the whole form valid or not
        validate: function() {
            var err = this.getErrors();
            
            if(err=="")
                return true;
            else
                return false;
        },
        
        // Return All Available Errors in the $el
        getErrors: function() {
            var errors = "";
            this.$("*[validate]").trigger("blur");
            this.$('.gmValidationError').each(function() {
                if(!(errors.indexOf($(this).attr("message")) >= 0))
                    errors += $(this).attr("message") + "\n";
                 
            });
            
            return errors;
        },
        
        // Return All Available Errors in the $el with Html tags
        getErrorsTag: function() {
            var errors = "";
            this.$("*[validate]").trigger("blur");
            this.$('.gmValidationError').each(function() {
                if(!(errors.indexOf($(this).attr("message")) >= 0))
                    errors += "<span class='gmAlignVertical gmTextLeft gmMarginBottom5p'>" + $(this).attr("message") + "</span>";
                 
            });
            
            return errors;
        }
        
    });

	return GM.View.Validation;
});
