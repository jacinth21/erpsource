/**********************************************************************************
 * File                :     gmvCRMSelectPopup.js
 * Description   :     Common Multiple Selection in a Popup View
 * Version         :     1.0
 * Author          :     cskumar
 **********************************************************************************/
define([

        // External Libraries
        'jquery', 'underscore', 'backbone', 'handlebars',

        // Common
        'global', 'gmcItem', 'gmvItem',

        // Pre-Compiled Template
        'gmtTemplate'

        ], function ($, _, Backbone, Handlebars, Global, GMCItem, GMVItem, GMTTemplate) {

    'use strict';

    GM.Global.GMVCRMSelectPopup = Backbone.View.extend({

        container: "#div-crm-overlay-content-container",

        template: fnGetTemplate(URL_Common_Template, "gmtSelect"),
        template_select_list: fnGetTemplate(URL_Common_Template, "gmtPopupMultiSelect"),
        template_selected_items: fnGetTemplate(URL_Common_Template, "gmtPopupSelectedItem"),

        events: {
            "click .div-text-selected-crt": "toggleSelectedItems"
        },

        //Initialize and Config the View 
        initialize: function (Options) {
            //Required Paramters
            this.currentNode = Options.currentNode;
            this.title = Options.title; // Title for Popup
            this.storagekey = Options.storagekey;
            this.webservice = Options.webservice;
            this.resultVO = Options.resultVO;
            this.IDParameter = Options.IDParameter;
            this.NameParameter = Options.NameParameter;
            this.inputParameters = Options.inputParameters;
            this.codegrp = Options.codegrp;
            this.module = Options.module,
                this.event = Options.event;
            this.resultData = Options.resultData;
            this.parent = Options.parent;
            this.type = Options.type;
            this.moduleLS = Options.moduleLS;
            this.moduleCLS = Options.moduleCLS;
            this.defaultVals = Options.defaultVals;
            this.callback = Options.callback;
            this.sortby = Options.sortby;
            if (this.moduleLS != "" && typeof (this.moduleLS) != "object")
                this.moduleLS = $.parseJSON(this.moduleLS)
            if (this.moduleCLS != "" && typeof (this.moduleCLS) != "object")
                this.moduleCLS = $.parseJSON(this.moduleCLS)
            if (this.type == undefined)
                this.type = 1;
            if (this.type == 2)
                this.renderOne = Options.renderOne;
            this.render();
        },

        //Rendering templates when loading the view
        render: function () {
            var that = this;
            showPopup();
            $(this.container).find('.modal-title').html(this.title);
            $(this.container).find('.modal-body').html(this.template());
            $(".done-model").removeClass("hide");
            $(".load-model").addClass("hide");
            $(".save-model").addClass("hide");
            this.initializePopupSelect(this.event);
            $(this.container).find('.done-model').unbind('click').bind('click', function () {
                that.selecedDone();
            });
            $(".search-select-options").focus();
            $(this.container).find('.modal-body .search-select-options').unbind('keyup').bind('keyup', function () {
                that.searchPopupList();
            });
        },

        selecedDone: function () {
            var that = this;
            if (this.type == 1) {
                this.selectPopupValues(this.event, this.storagekey, this.title);
            } else {
                if (parseNull(this.inCutomVal) != "")
                    this.callback(this.currArrCnt, this.inCutomVal);
                else
                    this.callback(this.popupCollection.getSelected(), this.popupCollection.getSelectedName());
                hidePopup();
            }
        },

        // initialise the popup dropdown content in the page
        initializePopupSelect: function (event) {
            var that = this;
            var token = localStorage.getItem("token");
            var url = this.webservice;
            var voname = this.resultVO;
            var storagekey = this.storagekey;
            var idkey = this.IDParameter;
            var namekey = this.NameParameter;
            var inputkey = this.inputParameters;
            var input = inputkey;
            if (inputkey != "") {
                if (typeof (inputkey) != "object")
                    var input = $.parseJSON(inputkey);
            } else
                input = {};

            input["token"] = token;
            if (this.resultData != undefined) {
                that.popUpData(idkey, namekey, storagekey, this.resultData);
                //            }
                //            else if (input.codegrp == "CNTY") {
                //                $.get("datafiles/country.json", function (data) {
                //                    that.popUpData(idkey, namekey, storagekey, data);
                //                });
                //            } else if (input.codegrp == "STATE") {
                //                $.get("datafiles/state.json", function (data) {
                //                    that.popUpData(idkey, namekey, storagekey, data);
                //                });
            } else {
                fnGetWebServerData(url, voname, input, function (values) {

                    if (that.title == "Label") {
                        if (GM.Global.phoneConMode == undefined)
                            GM.Global.phoneConMode = [];
                        values = values.concat(GM.Global.phoneConMode);
                    }
                    that.popUpData(idkey, namekey, storagekey, values);
                });
            }
        },

        popUpData: function (idkey, namekey, storagekey, values) {
            var that = this;
            values = arrayOf(values);
            var regID = new RegExp(idkey, 'g');
            var regName = new RegExp(namekey, 'g');
            var jsnData = $.parseJSON(JSON.stringify(values).replace(regID, 'ID').replace(regName, 'Name'));

            that.popupCollection = new GM.Collection.Item(jsnData);
            that.currCollection = new GM.Collection.Item(jsnData);

            if (parseNull(this.sortby) == "N") {
                that.popupCollection.sortAsc("");
                that.currCollection.sortAsc("");
            } else {
                that.popupCollection.sortAsc("Name");
                that.currCollection.sortAsc("Name");
            }

            if (that.type == 1) {
                if (that.moduleLS != null)
                    var jsnInput = that.moduleLS;
                else
                    var jsnInput = {};

                var arrSelectedKeys = new Array();
                if (that.defaultVals == undefined && jsnInput != null)
                    arrSelectedKeys = jsnInput[storagekey];
                else
                    arrSelectedKeys = that.defaultVals;

                if (arrSelectedKeys != undefined) {
                    var selectedkey = arrSelectedKeys.split(",");
                    that.popupCollection.selectOnly(selectedkey);
                    that.currCollection.selectOnly(selectedkey);
                }
            } else {
                if (that.renderOne != undefined && that.renderOne != "") {
                    that.popupCollection.selectOnly(that.renderOne);
                    that.currCollection.selectOnly(that.renderOne);
                }

            }
            that.popupCollection.sortIntDesc("Selected");
            that.currCollection.sortIntDesc("Selected");
            that.renderPopupList();
        },

        //renders the data into popup
        renderPopupList: function () {
            var that = this;
            $(".select-options-content").html(this.template_select_list(this.currCollection.toJSON()));
            $("#ul-custom-label-popup").addClass("hide");
            if (this.title == "Label")
                $("#ul-custom-label").removeClass("hide");

            $(this.container).find('.modal-body .remove-custom-label').unbind('click').bind('click', function (event) {
                $("#ul-custom-label-popup").removeClass("hide");
                this.removeFl = "Y";
                $("#btn-custom-label-yes").unbind('click').bind('click', function () {
                    that.removeCustomLabel(event);
                    $("#ul-custom-label-popup").addClass("hide");
                });
                $("#btn-custom-label-no").unbind('click').bind('click', function () {
                    $("#ul-custom-label-popup").addClass("hide");
                });
            });

            $(this.container).find(".select-options-content .ul-select-options").unbind("click").bind("click", function (event) {
                if (!$("#ul-custom-label-popup").is(":visible")) {
                    var id = $(event.currentTarget).find("li div").attr("data-id");
                    if (that.type == 2) {
                        var existing = "";
                        if ($(event.currentTarget).find("li i").hasClass("fa-check"))
                            existing = "Y";
                        $(".select-options-content .ul-select-options").each(function () {
                            $(this).find("li i").removeClass("fa-check");

                        });
                        if (existing == "") {
                            $(event.currentTarget).find("li i").addClass("fa-check");
                            that.popupCollection.selectOnly(id);
                            that.currCollection.selectOnly(id);
                        } else {
                            that.popupCollection.clearAllSelected();
                            that.currCollection.clearAllSelected();
                        }
                    } else {
                        $(event.currentTarget).find("li i").toggleClass("fa-check");
                        that.popupCollection.toggleSelect(id);
                        that.currCollection.toggleSelect(id);
                    }
                    var ret = _.filter(that.popupCollection.toJSON(), function (data) {
                        return data.Selected === true;
                    });
                    var vet = _.filter(that.currCollection.toJSON(), function (data) {
                        return data.Selected === true;
                    });
                    if (that.currentNode != undefined) {
                        var arrID = new Array();
                        _.each(vet, function (data) {
                            arrID.push(data.ID);
                        });
                        that.currentNode.attr("data-values", arrID.toString());
                    }
                }
            });

            $(this.container).find('.modal-body #add-custom-option').unbind('click').bind('click', function () {
                that.addCustomLabel();
            });
        },

        // get selected poup values
        selectPopupValues: function (event, key, title) {
            var that = this;
            var jsnInput;
            var jsnInputName;
            if (this.moduleLS != null)
                jsnInput = this.moduleLS;
            else
                jsnInput = {};
            if (this.moduleCLS != null)
                jsnInputName = this.moduleCLS;
            else
                jsnInputName = {};


            jsnInput[key] = this.popupCollection.getSelected().join(",");
            jsnInputName[title] = this.popupCollection.getSelectedName().join(" / "); // join(", ") give space after comma in between JSON values

            _.each(jsnInput, function (key, value) {
                if (key === "" || key === null)
                    delete jsnInput[value];
            });
            _.each(jsnInputName, function (key, value) {
                if (key === "" || key === null)
                    delete jsnInputName[value];
            });

            var countVal = parseInt(this.popupCollection.getSelected().length);

            if (countVal > 0) {
                $(event.currentTarget).find("span").text("(" + countVal + ")"); // get number of items selected 
                $(event.currentTarget).css("background", "#FDF5E6");
            } else {
                $(event.currentTarget).css("background", "#fff");
                $(event.currentTarget).find("span").text("");
            }

            var tempArray = new Array();
            var result = $.parseJSON(JSON.stringify(jsnInputName));
            $.each(result, function (key, val) {
                if (val != "")
                    tempArray.push({
                        "title": key,
                        "value": val
                    });
            });
            var finalJSON = $.parseJSON(JSON.stringify(tempArray));
            if (finalJSON.length > 0) {
                this.parent.$(".list-category-item").html(this.template_selected_items(finalJSON));
                if (GM.Global.SwitchUser)
                    $("#div-criteia-save").addClass("hide");
                this.parent.$("#btn-category-search").show();
            } else {
                this.parent.$(".list-category-item").empty();
                this.parent.$("#btn-category-search").hide();
            }

            $(".div-text-selected-crt").addClass("gmFont" + this.module);
            this.callback(JSON.stringify(jsnInput), JSON.stringify(jsnInputName))
            hidePopup();
        },

        // popup search by given input 
        searchPopupList: function () {
            var that = this;
            var value = $(".search-select-options").val();
            var itemList = this.popupCollection.search("Name", value);
            this.currCollection = new GM.Collection.Item($.parseJSON(JSON.stringify(itemList)));
            this.renderPopupList();
        },

        toggleSelectedItems: function (event) {
            if ($("#criteria-selected-itmes").hasClass("gmMediaDisplayNone")) {
                $("#criteria-selected-itmes").removeClass("gmMediaDisplayNone");
                $("#selected-criteria-icon").find(".fa-chevron-down").addClass("fa-chevron-up").removeClass("fa-chevron-down");
            } else {
                $("#criteria-selected-itmes").addClass("gmMediaDisplayNone");
                $("#selected-criteria-icon").find(".fa-chevron-up").addClass("fa-chevron-down").removeClass("fa-chevron-up");
            }
        },
        addCustomLabel: function () {
            var that = this;
            var inCutomVal = $("#input-add-custom-label").val();
            var currArrCnt = GM.Global.phoneConMode.length;
            if (parseNull(inCutomVal) != "") {
                GM.Global.phoneConMode.push({
                    "ID": currArrCnt++,
                    "Name": inCutomVal,
                    "CUSID": "Y"
                });
                this.currArrCnt = currArrCnt;
                this.inCutomVal = inCutomVal;
                this.selecedDone();
            }
        },
        removeCustomLabel: function (event) {
            $(event.currentTarget).parent().parent().remove();
            var currId = $(event.currentTarget).attr("data-id");
            if (parseNull(currId) != "") {
                console.log(GM.Global.phoneConMode);
                var rmData = _.reject(GM.Global.phoneConMode, function (dt) {
                    return dt.ID == currId;
                });
                GM.Global.phoneConMode = rmData;
            }
        },

    });
    return GM.Global.GMVCRMSelectPopup;
});
