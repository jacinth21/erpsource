define([
        'jquery', 'underscore', 'backbone', 'handlebars',
        'global',

    	// Pre-Compiled Template
    	'gmtTemplate'
        ], function ($, _, Backbone, Handlebars, Global, GMTTemplate) {

	'use strict';
	GM.View.LocalDB = Backbone.View.extend({

		el: "#div-LocalDB",

        events: {
            
            "click #btn-sql": "execSQL",
            "click #btn-close": "closeLocalDB"
        },

        name: "Local DB View",
    
        initialize: function() {
			
			this.template = fnGetTemplate(URL_Common_Template, "gmtLocalDB");
            this.render();
        },
        
        render: function (options) {
//            console.log("Eshcol Data: " + JSON.stringify(this.model));
			this.$el.html(this.template());
			return this;
		},

		execSQL: function() {
			var sqlQuery = $("#txtSQL").val();

			db.transaction(function (tx, rs) {
				tx.executeSql(sqlQuery, [],
						function(tx,rs) {
					var i=0, results = [];
					for(i=0;i<rs.rows.length;i++) {
						results.push(rs.rows.item(i));
					}
					$("#txtOutput").val(JSON.stringify(results));
					console.log(results);
					console.log("SQL execution was Successful");
				},
				function(){
					showMsgView("009");
					console.log("SQL execution Failed");
				});
			});
		},

		closeLocalDB: function() {
			$("#div-LocalDB").addClass("hide");
		}
	});

	return GM.View.LocalDB;
});
