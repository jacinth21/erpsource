define([
        'jquery', 'underscore', 'backbone',  'handlebars', 'mapui', 'mapclusterer',
        'global',

    	// Pre-Compiled Template
    	'gmtTemplate'
        ], 

function ($, _, Backbone, Handlebars, MapUI, MapClusterer, Global, GMTTemplate) {

	'use strict';
	GM.View.CRMMap = Backbone.View.extend({

		el: "#div-crm-module",

		events : {
			
		},

		// Load the templates
		template_Map: fnGetTemplate(URL_Common_Template, "gmtCRMMap"),
		template_MapAccountInfo: fnGetTemplate(URL_FieldSales_Template, "gmtCRMMapAccountInfo"),
		
		initialize: function(options) {
			this.$el = options.el;
			this.repID = options.repID;
			// this.data = {"markers" : options.data};
			if(this.repID=="0")
				this.repID = localStorage.getItem('partyID');
			this.render();
		},

		render: function () {

			this.$("#div-main-content").html(this.template_Map());

			var marker = {};
			this.showMap($("#div-map"), "image/crm/firstaid.png");
			return this;
		},

		showMap: function(controlid, pinicon) {
			
			var that = this;

			var token = localStorage.getItem('token');

			var input = {"token" : token, "repid" : this.repID};
            
            // $("#btn-main-list a").attr("href",fsListData);      // to create link to list back the list view from mapview
			 
			$(controlid).gmap({'zoom':5, 'disableDefaultUI':true, 'center':'40.345573,-95.098326'}).bind('init', function(ev, map) {
            
         		fnGetWebServerData("fieldsales/accmapinfo", "gmAccountBasicVO", input, function(data){
				 	var strData = (JSON.stringify(data)).replace(/,"lat":/g,',"LATITUDE":').replace(/,"lon":/g,',"LONGITUDE":');
				 	data = $.parseJSON(strData);

	             	var mapdata = {"markers" : data};

	              	$.each( mapdata.markers, function(i, marker) {
	                
		                $(controlid).gmap('addMarker', { 
		                  'position': new google.maps.LatLng(marker.LATITUDE, marker.LONGITUDE),
		                  'animation': google.maps.Animation.DROP,
		                  'icon': pinicon
		                  // , 'bounds':true
		                }).click(function(){
		                	console.log(marker);
		                	$(controlid).gmap('openInfoWindow', {'content': that.template_MapAccountInfo(marker)}, this);
		                })

	              	});
	              
	              	$(controlid).gmap('set', 'MarkerClusterer', new MarkerClusterer($(controlid).gmap('get', 'map'), $(controlid).gmap('get', 'markers')));
            	});
          });
			// });

        },
        
		// Close the view and unbind all the events
		close: function() {
			this.unbind();
			this.undelegateEvents();
		}

	});	

	return GM.View.CRMMap;
});
