/**********************************************************************************
 * File:        GmItemView.js
 * Description: Subview for ItemListView to render a single row / item of the List
 * Version:     1.0
 * Author:      praja
 **********************************************************************************/

define([
        'jquery', 'underscore', 'backbone', 'handlebars',
        'global',
        'itemmodel'
        ], function ($, _, Backbone, Handlebars, Global, Item) {

	'use strict';
	var ItemView = Backbone.View.extend({

		tagName: "li",
		
		// Render the initial contents when the app starts
		render: function (options) {
			this.template = fnGetTemplate(options.template_URL,options.template),	
			this.$el.html(this.template(this.model.toJSON()));
			return this;
		}
	});

	return ItemView;
});
