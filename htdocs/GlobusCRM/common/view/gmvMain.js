define([
        'jquery', 'underscore', 'backbone', 'handlebars', 'global', 'gmvField', 'gmmUser','gmvImageCrop',

    	// Pre-Compiled Template
    	'gmtTemplate'
], 

function ($, _, Backbone, Handlebars, Global, GMVField, GMMUser, GMVImageCrop, GMTTemplate ) {

	'use strict';

	GM.View.Main = Backbone.View.extend({

		el: "body",
        
        container: "#div-crm-overlay-content-container",

		events: {
			"click #btn-logout": "logout",
            "click #mobile-menu-top-user-logout": "logout",
//			"click #div-crm-overlay-content-main" : "closeOverlay",
             "click #ul-header-right-pic,#mobile-menu-top-user" : "openPicturePopup",
             "click #fa-close-alert": "closeMsgBar",
            "click #spn-portal-back": "backToPortal"
		},

		/* Templates */
		template: fnGetTemplate(URL_Common_Template, "gmtMain"),
        template_picture: fnGetTemplate(URL_Common_Template, "gmtUserProfilePic"),
		
		initialize: function() { 
            
			$("body").removeClass("background-2").addClass("background-1 background-image");
			$("#div-header").removeClass("background-2").addClass("background-1 background-image");
			this.render();
          
            onrefresh();
            },

		render: function () {
			this.$el.html(this.template());
            if(GM.Global.Portal){
                $(".gmMainDiv").addClass("crmPortal");
                $(".portal").addClass("hide");
                $("#li-spine-it").removeClass("hide");
            }
                
			this.$("#div-sales").hide();
			this.$("#div-messagebar span").hide();
			this.$("#tmpAdminNon").hide();
            var lastScrollTop = 0;
			$(window).scroll(function() {
				var scrollPercentage = 100 * $(this).scrollTop() / ($(document).height()-$(this).height());
				var st = $(this).scrollTop();
			   if (st > lastScrollTop){
			   		if(scrollPercentage>20) {
			   			$("#div-header").slideUp('fast');
			   		}
			   }
			   else{
		   			$("#div-header").slideDown('fast');
			   }
			   lastScrollTop = st;
			})

			return this;
		},

        openPicturePopup: function(){
            if(!GM.Global.SwitchUser)
                openPicturePopup (this, GMVImageCrop);             
        },
        
		//keycloak logout function
		logout: function(e) {
			localStorage.clear();
            GM.Global = {};    
            $(".div-user-icon-pic img").attr("src","");  
			window.location.href = "#";
			var urlIndex = "https://erptest.globusone.com/GlobusCRM/";
            var keycloakConfig = {url: "https://logintest.globusone.com/auth", realm: "globus-medical", clientId: "erp-client"};
            var keycloakService = {};
            var config = keycloakConfig;
            window.parent.location.href = keycloakConfig.url+'/realms/'+keycloakConfig.realm+'/protocol/openid-connect/logout?redirect_uri='+urlIndex;
		},

		setAdmin: function() {
			$(".adminSurgeon").show();
			$(".edit").hide();
		},

		setUser: function() {
			$(".adminSurgeon").hide();
			$(".edit").hide();
		},

		closeOverlay: function(event) {
			if(event.target.id == "spn-crm-overlay-content-close" || event.target.id == "span-overlay-save") {
				this.$("#div-crm-overlay-content-main").hide();
				this.$("#div-crm-overlay").hide();
			}
		},
        
        validate: function(e){
            var user = new GMMUser();  
            new GMVField({el: $(e.currentTarget), model: user});
        },

        closeMsgBar: function() {
            hideMessages();
        },
        
        // Navigates back to GmEnterprisePortal.jsp 
        backToPortal: function() {
            window.parent.location.href = "/GmEnterprisePortal.jsp";
        }
	});
	return GM.View.Main;
});
