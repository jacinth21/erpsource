define([
        'jquery', 'underscore', 'backbone', 'handlebars',
        'global',
        'gmcItem', 'gmvItemList', 'gmvSurgeonDetail', 'gmvSearch',
        'gmmActivity',

        // Pre-Compiled Template
        'gmtTemplate'

        ],
    function ($, _, Backbone, Handlebars, Global, GMCItem, GMVItemList, GMVSurgeonDetail, GMVSearch, GMMActivity, GMTTemplate) {

        'use strict';

        GM.View.CRMData = Backbone.View.extend({


            el: "#div-crm-module",

            name: "CRM Data View",

            /* Templates */
            template_MainModule: fnGetTemplate(URL_Common_Template, "gmtCRMMainModule"),
            template_LeadAssignee: fnGetTemplate(URL_Lead_Template, "gmtLeadAssginee"),
            template_SurgeonMERC: fnGetTemplate(URL_Surgeon_Template, "gmtMERCListPopup"),
            template_selected_items: fnGetTemplate(URL_Common_Template, "gmtPopupSelectedItem"),

            events: {
                "click .li-lead-assignto": "assignee",
                "click #div-assignee-tap-close": "closeOverlay",
                "click #div-assignee-as-myself": "assignMyself",
                "click .li-surg-add-merc": "listUpcomingMERC",
                "click #btn-geo": "openMap",
                "click #div-save-favorite": "addFavorite",
                "click .li-edit-favorite": "editFavorite",
                "click .li-surgeon-calendar": "createSurActivity",
                "click .li-lead-calendar": "createleadActivity",
                "click #btn-act-physicianvisit": "createPhysicianVisit",
                "click .li-tradeshow-list-detail, .ul-ts-phn-list": "showTSDetail",
                "click #btn-main-create": "createActivity",
                "click .div-text-selected-crt": "toggleSelectedItems",
            },

            initialize: function (options) {

                this.module = options.module;
                this.favoriteData = {};
                this.arrAttribute = new Array();
                this.tempJSON = {};
                GM.Global.SCDATA = null;
                this.stateList = new Array();
                this.productsList = new Array();
                this.techqList = new Array();
                this.skillList = new Array();
                this.roleList = new Array();
                this.activityList = new Array();
                this.statusList = new Array();
                this.crfstatusList = new Array();
                this.tabAccess = _.pluck($.parseJSON(localStorage.getItem('userTabs')), "functionID");
                this.filterID = "";

                // upcoming events
                var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth() + 1; //January is 0!
                var yyyy = today.getFullYear();
                if (dd < 10) {
                    dd = '0' + dd
                };
                if (mm < 10) {
                    mm = '0' + mm
                };
                this.today = mm + '/' + dd + '/' + yyyy;
                console.log(GM.Global.Activity);
                if (GM.Global.Activity == undefined)
                    GM.Global.Activity = {};

                if (GM.Global.Status == undefined)
                    GM.Global.Status = {};
                console.log(GM.Global.Activity);
                if (GM.Global.Activity.Filter == null)
                    delete GM.Global.Activity.Filter;
                console.log(GM.Global.Activity.FilterID);
                console.log(GM.Global.Activity.criteriaid);
                GM.Global.Activity.Mode == undefined;
                if ($.isEmptyObject(GM.Global.Activity.FilterID) && GM.Global.Activity.FilterID != undefined && GM.Global.Activity.criteriaid == undefined && GM.Global.Activity.actnm == undefined)
                    GM.Global.Activity = {};

                if (GM.Global.Surgeon == undefined)
                    GM.Global.Surgeon = {};

                if (!$.isEmptyObject(GM.Global.Surgeon) && !$.isEmptyObject(GM.Global.Surgeon.FilterID) && GM.Global.Surgeon.criteriaid != "myrep")
                    delete GM.Global.Surgeon.criteriaid;

                this.userID = localStorage.getItem('userID');
            },

            render: function () {
                this.setList = {};
                var that = this;
                this.$el.html(this.template_MainModule({
                    module: this.module
                }));
                var template = fnGetTemplate(URL_Common_Template, "gmtSpinner");
                $("#div-main-content").html(template());
                console.log("Module Name >> " + this.module);

                //            console.log(GM.Global.SalesCall.FilterID);
                if (_.contains(GM.Global.Admin, this.module))
                    this.$("#li-create").removeClass("hide");

                if (_.contains(GM.Global.Admin, "physvisitrqst") && this.module == "physicianvisit")
                    this.$("#li-create").removeClass("hide");

                if (this.module == "fieldsales" || this.module == "crf")
                    this.$("#li-create").addClass("hide");

                var editAccess = _.findWhere(GM.Global.UserAccess, {
                    funcid: "CRM-SURGEON-EDIT"
                });

                if (editAccess != undefined) {
                    $("#li-create").removeClass("hide");
                } else {
                    $("#li-edit").addClass("hide");
                    $("#li-create").addClass("hide");
                }

                switch (this.module) {
                    case "surgeon":
                        this.listSurgeons();
                        break;
                    case "physicianvisit":
                        this.listPhysicianvisit();
                        break;
                    case "lead":
                        console.log(GM.Global.TradeShowList);
                        if (GM.Global.TradeShowList != undefined) {
                            this.listTradeShow();
                            GM.Global.TradeShowList = undefined;
                        } else {
                            this.$("#li-tradeshow").removeClass("hide");
                            this.listLead();
                        }
                        break;
                    case "fieldsales":
                        this.listFieldSales();
                        if (GM.Global.FieldSalesRegion != "Y")
                            $("#li-geo").removeClass('hide');
                        console.log(GM.Global.FieldSales);
                        if (GM.Global.FieldSales == undefined)
                            GM.Global.FieldSales = {};
                        break;
                    case "salescall":
                        this.listSalesCall()
                        break;
                    case "training":
                        this.listTraining()
                        break;
                    case "merc":
                        this.listMERC()
                        break;
                    case "crf":
                        this.listCRF();
                        break;
                    case "crf-pending":
                        this.pendingcrf = "Y";
                        this.listCRF();
                        break;
                    case "crf-reviewed":
                        this.reviewecrf = "Y";
                        this.listCRF();
                        break;
                }

                return this;
            },

            listSurgeons: function () {
                var that = this;
                var listSurg = {};
                $("#span-app-title").html("Surgeon");
                this.input = {};
                this.voname = "gmCRMSurgeonlistVO";
                this.sortoptions = [{
                    "DefaultSort": "firstnm",
                    "Elements": [{
                            "ID": "firstnm",
                            "DispName": "First Name",
                            "ClassName": "gmDashColHead gmFontsurgeon gmText200 sur-liheader-fname"
                        },
                        {
                            "ID": "midinitial",
                            "DispName": "M.I",
                            "ClassName": "gmDashColHead gmFontsurgeon gmText50 sur-liheader-minitial"
                        },
                        {
                            "ID": "lastnm",
                            "DispName": "Last Name",
                            "ClassName": "gmDashColHead gmFontsurgeon gmText200 sur-liheader-lname"
                        },
                        {
                            "ID": "city",
                            "DispName": "City",
                            "ClassName": "gmDashColHead gmFontsurgeon gmText125 sur-liheader-city"
                        },
                        {
                            "ID": "state",
                            "DispName": "State",
                            "ClassName": "gmDashColHead gmFontsurgeon gmText50 sur-liheader-state"
                        },
                        {
                            "ID": "country",
                            "DispName": "Country",
                            "ClassName": "gmDashColHead gmFontsurgeon gmText100 sur-liheader-country"
                        }
                                          ]
                          }];
                this.filterOptions = ["firstnm", "lastnm"];
                this.columnheader = 5;
                this.template_URL = URL_Surgeon_Template;
                this.itemtemplate = "gmtSurgeonList";
                this.controlID = "#div-main-content";
                this.url = "surgeon/info";
                this.color = "gmFontsurgeon";

                this.callbackOnReRender = this.checkSurgeonAccess();
                this.search = ["firstnm", "midinitial", "lastnm", "city", "state", "country"];
                console.log(GM.Global.Surgeon);
                if (GM.Global.Surgeon.Summary != undefined) {
                    this.$(".gmPanelTitle").html("My Surgeons");
                    $("#div-surgeon-phone-title").html("My Surgeons");
                    this.input = {
                        "token": GM.Global.Token,
                        "stropt": "summaryRpt",
                        "ref_id": GM.Global.Surgeon.Summary.RefID,
                        "accesslvl": GM.Global.Surgeon.Summary.AccessLvl,
                        "status": GM.Global.Surgeon.Summary.Status
                    };
                    console.log("Summary Input>>>>>>" + JSON.stringify(this.input));
                } else if (GM.Global.Surgeon.criteriaid == "myrep") {
                    this.$(".gmPanelTitle").html("My Surgeons");
                    $("#div-surgeon-phone-title").html("My Surgeons");
                    this.input["repid"] = localStorage.getItem('partyID');
                } else if (GM.Global.Surgeon.keywords != undefined) {
                    this.input = {
                        "token": GM.Global.Token,
                        "keywords": GM.Global.Surgeon.keywords
                    };
                    this.$(".gmPanelTitle").html("Surgeon list selected by Keyword containing " + " '" + GM.Global.Surgeon.keywords + "' ");
                    GM.Global.Surgeon.keywords = undefined;
                } else if ((GM.Global.Surgeon.firstnm || GM.Global.Surgeon.lastnm) != undefined) {
                    this.input = {
                        "token": GM.Global.Token,
                        "firstnm": GM.Global.Surgeon.firstnm,
                        "lastnm": GM.Global.Surgeon.lastnm
                    };
                    this.$(".gmPanelTitle").html("Surgeon list selected by Name containing " + " '" + GM.Global.Surgeon.firstnm + " " + GM.Global.Surgeon.lastnm + "' ");
                }else if (GM.Global.Surgeon.repid != undefined) {
                    this.input = {
                        "token": GM.Global.Token,
                        "repid": GM.Global.Surgeon.repid
                    };
                    this.$(".gmPanelTitle").html("Surgeon list selected by Rep " + " '" + GM.Global.Surgeon.repnm + "' ");
                }else if (GM.Global.Surgeon.criteriaid != undefined) {
                    if (GM.Global.Surgeon.criteriaid != 0) {
                        console.log(GM.Global.Surgeon.criteriaid)
                        this.$(".gmPanelTitle").html(GM.Global.Surgeon.HeaderTitle);
                        this.url = "surgeon/criterialist";
                        this.input = {
                            "token": GM.Global.Token,
                            "criteriaid": GM.Global.Surgeon.criteriaid,
                            "criteriatyp": "surgeon"
                        };
                        this.getCriteriaData(GM.Global.Surgeon.criteriaid);

                    }
                } else if (GM.Global.Surgeon.FilterID != undefined) {
                    console.log(GM.Global.Surgeon.FilterName)
                    if (typeof (GM.Global.Surgeon.FilterID) != "object")
                        GM.Global.Surgeon.FilterID = $.parseJSON(GM.Global.Surgeon.FilterID)
                    this.input = GM.Global.Surgeon.FilterID;
                    console.log(GM.Global.Token)
                    console.log(this.input)
                    this.input["token"] = GM.Global.Token;
                    if (typeof (GM.Global.Surgeon.FilterName) != "object")
                        GM.Global.Surgeon.FilterName = $.parseJSON(GM.Global.Surgeon.FilterName)
                    this.showFilterName(GM.Global.Surgeon.FilterName);
                    this.favoriteData = GM.Global.Surgeon.FilterID;
                    this.$(".gmPanelTitle").html("Surgeon list selected by categories");
                }

                if (GM.Global.Surgeon.criteriaid != undefined && GM.Global.Surgeon.criteriaid != 0)
                    this.csvFileName = "CRM_Surgeon_Criteria_" + this.$(".gmPanelTitle").html().replace(" ", "_") + ".csv";
                else if (GM.Global.Surgeon.criteriaid != undefined && GM.Global.Surgeon.criteriaid == 0)
                    this.csvFileName = "CRM_Surgeon_List_" + this.$(".gmPanelTitle").html().replace(" ", "_") + ".csv";

                this.input.token = GM.Global.Token;

                this.showCRMData();

            },

            listFieldSales: function () {
                var that = this;
                $("#span-app-title").html("Field Sales");
                this.input = {
                    "token": GM.Global.Token
                };
                this.voname = "gmCRMSalesRepListVO";
                this.sortoptions = [{
                    "DefaultSort": "firstnm",
                    "Elements": [{
                            "ID": "firstnm",
                            "DispName": "First Name",
                            "ClassName": "gmDashColHead gmFontfieldsales gmText200 fs-liheader-fname"
                        },
                        {
                            "ID": "midinitial",
                            "DispName": "M.I",
                            "ClassName": "gmDashColHead gmFontfieldsales gmText50 fs-liheader-minitial"
                        },
                        {
                            "ID": "lastnm",
                            "DispName": "Last Name",
                            "ClassName": "gmDashColHead gmFontfieldsales gmText200 fs-liheader-lname"
                        },
                        {
                            "ID": "phone",
                            "DispName": "Phone",
                            "ClassName": "gmDashColHead gmFontfieldsales gmText125 fs-liheader-city"
                        },
                        {
                            "ID": "email",
                            "DispName": "Email",
                            "ClassName": "gmDashColHead gmFontfieldsales gmText125 fs-liheader-state"
                        }
                                          ]
                          }];
                this.filterOptions = ["firstnm", "lastnm"];
                this.columnheader = 5;
                this.template_URL = URL_FieldSales_Template;
                this.itemtemplate = "gmtFieldSalesList";
                this.controlID = "#div-main-content";
                this.search = ["firstnm", "midinitial", "lastnm", "phone", "email"];
                this.url = "fieldsales/repinfo";
                this.color = "gmFontfieldsales";
                this.callbackOnReRender = this.checkSurgeonAccess();
                var inp = {};
                if (GM.Global.FieldSalesRegion == "Y") {
                    this.input = {
                        "token": localStorage.getItem("token"),
                        "repid": localStorage.getItem("userID"),
                        "regid": "y"
                    };
                    this.url = "fieldsales/regterdis";
                    this.$(".gmPanelTitle").html("Sales Region");
                    this.voname = "gmCRMSalesRepListVO";
                    this.sortoptions = [{
                        "DefaultSort": "firstnm",
                        "Elements": [{
                            "ID": "regnm",
                            "DispName": "Region",
                            "ClassName": "gmDashColHead gmFontfieldsales gmText200 fs-liheader-fname"
                        }]
                          }];


                } else if ((GM.Global.FieldSales.firstnm || GM.Global.FieldSales.lastnm) != undefined) {
                    this.input = {
                        "token": GM.Global.Token,
                        "firstnm": GM.Global.FieldSales.firstnm,
                        "lastnm": GM.Global.FieldSales.lastnm
                    };
                    this.$(".gmPanelTitle").html("Sales Reps list selected by Name " + " ' " + GM.Global.FieldSales.firstnm + " " + GM.Global.FieldSales.lastnm + " ' ");
                } else if (GM.Global.FieldSales.FilterID != undefined) {
                    console.log(GM.Global.FieldSales.FilterName)
                    if (typeof (GM.Global.FieldSales.FilterID) != "object")
                        GM.Global.FieldSales.FilterID = $.parseJSON(GM.Global.FieldSales.FilterID)
                    console.log("GM.Global.FieldSales.FilterID");
                    this.input = GM.Global.FieldSales.FilterID;

                    this.input.token = localStorage.getItem("token");

                    if (typeof (GM.Global.FieldSales.FilterName) != "object")
                        GM.Global.FieldSales.FilterName = $.parseJSON(GM.Global.FieldSales.FilterName)
                    this.showFilterName(GM.Global.FieldSales.FilterName);
                    this.favoriteData = GM.Global.FieldSales.FilterID;
                    this.$(".gmPanelTitle").html("Field Sales list selected by the categories");
                    GM.Global.FieldSales.FilterID = undefined;
                } else if (GM.Global.FieldSales.regid != undefined) {
                    console.log(GM.Global.FieldSales.regid)
                    this.$(".gmPanelTitle").html("Field Sales list selected by the Region");
                    this.input = {
                        "token": GM.Global.Token,
                        "regid": GM.Global.FieldSales.regid
                    };
                } else if (GM.Global.FieldSales.criteriaid != 0) {
                    console.log(GM.Global.FieldSales.criteriaid)
                    this.$(".gmPanelTitle").html(GM.Global.FieldSales.HeaderTitle);
                    this.url = "fieldsales/criterialist";
                    this.input = {
                        "token": GM.Global.Token,
                        "criteriaid": GM.Global.FieldSales.criteriaid,
                        "criteriatyp": "fieldsales"
                    };
                    this.getCriteriaData(GM.Global.FieldSales.criteriaid);
                }

                if (GM.Global.FieldSales.criteriaid != undefined && GM.Global.FieldSales.criteriaid != 0)
                    this.csvFileName = "CRM_Surgeon_Criteria_" + this.$(".gmPanelTitle").html().replace(" ", "_") + ".csv";
                else if (GM.Global.FieldSales.criteriaid != undefined && GM.Global.FieldSales.criteriaid == 0)
                    this.csvFileName = "CRM_Surgeon_List_" + this.$(".gmPanelTitle").html().replace(" ", "_") + ".csv";
                if (GM.Global.FieldSalesRegion != "Y") {
                    GM.Global.FieldSalesRegion = "N"; // Needed for List regions
                    //                $("#li-geo").addClass("hide");

                }
                this.showCRMData();

            },

            listPhysicianvisit: function () {
                var that = this;
                this.setList.actcategory = '105266';
                this.input = {
                    "token": GM.Global.Token,
                    "actcategory": "105266"
                };
                this.voname = "gmCRMActivityVO";
                this.sortoptions = [{
                    "DefaultSort": "",
                    "Elements": [{
                            "ID": "acttype",
                            "DispName": "Type of Visit",
                            "ClassName": "gmDashColHead gmFontphysicianvisit gmText150"
                        },
                        {
                            "ID": "actstartdate",
                            "DispName": "Visit Date & Time",
                            "ClassName": "gmDashColHead gmFontphysicianvisit gmText200"
                        },
                        {
                            "ID": "phynm",
                            "DispName": "Visitor(s) Name",
                            "ClassName": "gmDashColHead gmFontphysicianvisit gmText250"
                        },
                        {
                            "ID": "actstatus",
                            "DispName": "Status",
                            "ClassName": "gmDashColHead gmFontphysicianvisit gmText125"
                        },
                        {
                            "ID": "createddate",
                            "DispName": "Created Date",
                            "ClassName": "gmDashColHead gmFontphysicianvisit gmText125"
                        }

                        ]
                }];

                this.filterOptions = ["acttype", "phynm"];
                this.columnheader = 3;
                this.template_URL = URL_PhysicianVisit_Template;
                this.itemtemplate = "gmtPhysicianVisitList";
                this.controlID = "#div-main-content";
                this.search = ["acttype", "actstartdate", "phynm", "actstartdate", "actstarttime", "createddate"];
                this.url = "crmactivity/info";
                this.color = "gmFontphysicianvisit";
                $("#span-app-title").html("Physician Visit");
                // this.$(".gmBtnGroup #li-create").removeClass("hide");
                console.log(GM.Global.Activity.criteriaid);

                console.log(GM.Global.Activity.FilterID)

                if ((GM.Global.Activity.firstnm || GM.Global.Activity.lastnm) != undefined) {
                    this.input = {
                        "token": GM.Global.Token,
                        "actcategory": "105266",
                        "firstnm": GM.Global.Activity.firstnm,
                        "lastnm": GM.Global.Activity.lastnm
                    };
                    this.$(".gmPanelTitle").html("Physician Visit list selected by the" + " ' " + GM.Global.Activity.firstnm + " " + GM.Global.Activity.lastnm + " ' ");

                } else if (GM.Global.Activity.criteriaid != undefined) {
                    if (GM.Global.Activity.criteriaid == "surgeon") {
                        this.input = $.parseJSON(GM.Global["SurgeonActivity"]);
                        this.input["actcategory"] = "105266";
                    } else {
                        this.$(".gmPanelTitle").html(GM.Global.Activity.HeaderTitle);
                        this.url = "crmactivity/criterialist";
                        this.input = {
                            "token": GM.Global.Token,
                            "criteriaid": GM.Global.Activity.criteriaid,
                            "criteriatyp": "physicianvisit"
                        };
                        this.getCriteriaData(GM.Global.Activity.criteriaid);
                    }
                } else if (GM.Global.Activity.FilterID != undefined) {

                    if (typeof (GM.Global.Activity.FilterID) != "object")
                        GM.Global.Activity.FilterID = $.parseJSON(GM.Global.Activity.FilterID)
                    this.input = GM.Global.Activity.FilterID;
                    this.input["token"] = GM.Global.Token;
                    console.log(GM.Global.Activity.FilterName)
                    if (typeof (GM.Global.Activity.FilterName) != "object")
                        GM.Global.Activity.FilterName = $.parseJSON(GM.Global.Activity.FilterName);
                    this.input["actcategory"] = "105266";

                    this.showFilterName(GM.Global.Activity.FilterName);
                    this.favoriteData = GM.Global.Activity.FilterID;
                    this.$(".gmPanelTitle").html("Physician Visit list selected by the categories");
                } else {

                    this.$(".gmPanelTitle").html("Upcoming Physician Visits");
                    if (GM.Global.Status.listtype == "Requested") {
                        this.input["actstatus"] = "105760";
                        this.$(".gmPanelTitle").html("Requested Physician Visits");
                    } else {
                        if (GM.Global.Status.listtype == "Scheduled")
                            this.input["actstatus"] = "105761";

                    }
                    this.input["actstartdate"] = this.today;
                }

                this.showCRMData();

            },

            //To list the TradeShows
            listTradeShow: function () {
                var that = this;
                this.input = {};
                this.voname = "gmCRMActivityVO";
                this.sortoptions = [{
                    "DefaultSort": "actstartdate",
                    "Elements": [{
                            "ID": "actnm",
                            "DispName": "TradeShow",
                            "ClassName": "gmDashColHead gmFontlead gmText250"
                        },
                        {
                            "ID": "actstartdate",
                            "DispName": "Start Date",
                            "ClassName": "gmDashColHead gmFontlead gmText200"
                        },
                        {
                            "ID": "actenddate",
                            "DispName": "End Date",
                            "ClassName": "gmDashColHead gmFontlead gmText350"
                        }]
                }];
                this.filterOptions = ["actnm"];
                this.columnheader = 3;
                this.template_URL = URL_Lead_Template;
                this.itemtemplate = "gmtTradeShowList";
                this.controlID = "#div-main-content";
                this.url = "crmactivity/info";
                this.color = "gmFontlead";
                this.search = ["actnm", "actstartdate", "actenddate"];
                $("#span-app-title").html("Trade Show");
                // this.$(".gmBtnGroup #li-create").removeClass("hide");
                this.csvFileName = "CRM_TradeShow_List.csv";
                this.$(".gmPanelTitle").html("TradeShow list selected by the" + " ' " + GM.Global.Activity.actnm + " ' ");
                this.input = GM.Global.Activity;
                this.input.token = GM.Global.Token;
                this.input.actcategory = "105270";

                this.showCRMData();
            },

            //To list the leads
            listLead: function () {
                var that = this;
                $("#span-app-title").html("Lead");
                this.setList.actcategory = "105269";
                this.voname = "gmCRMActivityVO";
                this.input = {};

                this.sortoptions = [{
                    "DefaultSort": "actstartdate",
                    "Elements": [
                        {
                            "ID": "actstartdate",
                            "DispName": "Generated On",
                            "ClassName": "gmDashColHead gmFontlead gmText125 lead-date"
                        },
                        {
                            "ID": "phynm",
                            "DispName": "Name",
                            "ClassName": "gmDashColHead gmFontlead gmText150  lead-phynm"
                        },
                        {
                            "ID": "source",
                            "DispName": "Source",
                            "ClassName": "gmDashColHead gmFontlead gmText125 lead-source"
                        },
                        {
                            "ID": "products",
                            "DispName": "Products",
                            "ClassName": "gmDashColHead gmFontlead gmText250 lead-products"
                        },
                        {
                            "ID": "assignee",
                            "DispName": "Assignee",
                            "ClassName": "gmDashColHead gmFontlead gmText150 lead-rep"
                        }
								]
                }];
                this.filterOptions = ["phynm", "gendate", "assignee"];
                this.columnheader = 5;
                this.template_URL = URL_Lead_Template;
                this.itemtemplate = "gmtLeadList";
                this.controlID = "#div-main-content";
                this.url = "crmactivity/info";
                this.color = "gmFontlead";
                this.search = ["actstartdate", "phynm", "source", "products", "assignee"];
                this.callbackOnReRender = this.checkSurgeonAccess();
                if (isRep()) {
                    this.$(".gmPanelTitle").html("My Leads");

                    this.input["partyid"] = localStorage.getItem("partyID");
                    this.input.actcategory = "105269";
                } else if (!$.isEmptyObject(GM.Global.Activity)) {
                    console.log(GM.Global.Activity)

                    console.log(this.input)
                    if (!$.isEmptyObject(GM.Global.Activity.FilterID)) {

                        if (typeof (GM.Global.Activity.FilterID) != "object")
                            GM.Global.Activity.FilterID = $.parseJSON(GM.Global.Activity.FilterID)
                        this.input = GM.Global.Activity;
                        this.input = GM.Global.Activity.FilterID;
                        console.log(GM.Global.Activity.FilterName)
                        if (typeof (GM.Global.Activity.FilterName) != "object")
                            GM.Global.Activity.FilterName = $.parseJSON(GM.Global.Activity.FilterName)
                        this.showFilterName(GM.Global.Activity.FilterName);
                        this.favoriteData = GM.Global.Activity.FilterID;
                        this.input.actcategory = "105269";


                    } else if (GM.Global.Activity.criteriaid != undefined) {
                        console.log(this.input)
                        this.$(".gmPanelTitle").html(GM.Global.Activity.HeaderTitle);
                        this.url = "crmactivity/criterialist";
                        this.input = {
                            "token": GM.Global.Token,
                            "criteriaid": GM.Global.Activity.criteriaid,
                            "criteriatyp": "lead"
                        };
                        console.log(this.input)
                        this.getCriteriaData(GM.Global.Activity.criteriaid);

                    } else { //Routing issue when coming back from tradeshow to list when logged in as AD
                        this.input["areauserid"] = localStorage.getItem("userID");
                        this.input.actcategory = "105269";
                    }
                } else {
                    this.$(".gmPanelTitle").html("Leads in my Area");

                    this.input["areauserid"] = localStorage.getItem("userID");
                    this.input.actcategory = "105269";
                }
                this.input.token = GM.Global.Token;

                this.showCRMData();


            },
            listCRF: function () {
                var that = this;
                this.sortoptions = [{
                    "DefaultSort": "",
                    "Elements": [{
                            "ID": "actid",
                            "DispName": "Control ID",
                            "ClassName": "gmDashColHead gmFontcrf gmText150"
                        },
                        {
                            "ID": "actnm",
                            "DispName": "Title",
                            "ClassName": "gmDashColHead gmFontcrf gmText200"
                        },
                        {
                            "ID": "firstnm",
                            "DispName": "Surgeon",
                            "ClassName": "gmDashColHead gmFontcrf gmText175"
                        },
                        {
                            "ID": "statusnm",
                            "DispName": "Status",
                            "ClassName": "gmDashColHead gmFontcrf gmText125"
                        }]
                }];
                this.filterOptions = ["actid", "actnm", "firstnm", "statusnm"];
                this.itemtemplate = "gmtCRFList";
                this.columnheader = 4;
                this.voname = "gmCRMCRFVO";
                this.controlID = "#div-main-content";
                this.template_URL = URL_CRF_Template;
                this.color = "gmFontcrf";
                this.search = ["actnm", "firstnm", "statusnm"];
                this.input = {};
                this.url = "crf/list";
                console.log(GM.Global.Activity.criteriaid)
                if (GM.Global.Activity.actnm != undefined) {
                    this.input = {
                        "token": GM.Global.Token,
                        "actnm": GM.Global.Activity.actnm,
                        "crfid": ""
                    };
                    this.$(".gmPanelTitle").html("CRF list selected by the" + " ' " + GM.Global.Activity.actnm + " ' ");
                } else if (GM.Global.Activity.FilterID != undefined) {
                    console.log(GM.Global.Activity.FilterName)
                    if (typeof (GM.Global.Activity.FilterID) != "object")
                        GM.Global.Activity.Filter = $.parseJSON(GM.Global.Activity.FilterID)
                    this.input = GM.Global.Activity.FilterID;
                    this.input["token"] = GM.Global.Token;


                    if (typeof (GM.Global.Activity.FilterName) != "object")
                        GM.Global.Activity.FilterName = $.parseJSON(GM.Global.Activity.FilterName)
                    this.showFilterName(GM.Global.Activity.FilterName);
                    this.favoriteData = GM.Global.Activity.FilterID;
                    this.$(".gmPanelTitle").html("CRF list selected by the categories");

                } else if (GM.Global.Activity.criteriaid != undefined) {
                    console.log(GM.Global.Activity.criteriaid)
                    this.url = "crmactivity/criterialist";
                    this.input = {
                        "token": GM.Global.Token,
                        "criteriaid": GM.Global.Activity.criteriaid,
                        "criteriatyp": "crf"
                    };
                    this.getCriteriaData(GM.Global.Activity.criteriaid);
                } else {

                    this.$(".gmPanelTitle").html("CRF - All");

                    this.input["crfid"] = "";

                    if (this.pendingcrf == "Y") {
                        this.input["statusid"] = "105207"; // Pending Approval
                        this.$(".gmPanelTitle").html("CRF - Pending Approval");
                    } else if (this.reviewecrf == "Y") {
                        this.input["statusid"] = "105206"; // Pending Review 
                        this.$(".gmPanelTitle").html("CRF - Pending Review");
                    } else {
                        if (_.contains(GM.Global.Admin, "crfappr")) {
                            this.$(".gmPanelTitle").html("CRF - Pending Approval");
                            this.apprpartyid = localStorage.getItem("partyID");
                            this.input["statusid"] = "105207";
                        } else
                            this.input["statusid"] = "105205 ,105206, 105207, 105208";
                    }
                    this.input["token"] = GM.Global.Token;
                    this.crfMobileList = "Y";
                }

                this.showCRMData();

            },


            listSalesCall: function () {
                var that = this;
                this.setList.actcategory = "105267";
                this.input = {
                    "token": GM.Global.Token,
                    "actcategory": "105267"
                };
                this.voname = "gmCRMActivityVO";
                this.sortoptions = [{
                    "DefaultSort": "",
                    "Elements": [{
                            "ID": "phynm",
                            "DispName": "Surgeon",
                            "ClassName": "gmDashColHead gmFontsalescall gmText200"
                        },
                        {
                            "ID": "actstartdate",
                            "DispName": "Date & Time",
                            "ClassName": "gmDashColHead gmFontsalescall gmText225"
                        },
                        {
                            "ID": "products",
                            "DispName": "Product",
                            "ClassName": "gmDashColHead gmFontsalescall gmText350"
                        }]
                }];
                this.filterOptions = ["phynm", "products"];
                this.columnheader = 3;
                this.template_URL = URL_SalesCall_Template;
                this.itemtemplate = "gmtSalesCallList";
                this.controlID = "#div-main-content";
                this.url = "crmactivity/info";
                this.color = "gmFontsalescall";
                this.search = ["phynm", "actstartdate", "result", "actstarttime", "actendtime"];
                $("#span-app-title").html("Sales Call");
                console.log(GM.Global.Activity);

                console.log(GM.Global.Activity.FilterID);
                console.log(GM.Global.Activity.criteriaid);

                if ((GM.Global.Activity.firstnm || GM.Global.Activity.lastnm) != undefined) {
                    this.input = {
                        "token": GM.Global.Token,
                        "actcategory": "105267",
                        "firstnm": GM.Global.Activity.firstnm,
                        "lastnm": GM.Global.Activity.lastnm
                    };
                    this.$(".gmPanelTitle").html("Sales Calls list selected by the" + " ' " + GM.Global.Activity.firstnm + " " + GM.Global.Activity.lastnm + " ' ");
                } else if (GM.Global.Activity.criteriaid != undefined) {
                    if (GM.Global.Activity.criteriaid == "surgeon") {
                        this.input = $.parseJSON(GM.Global["SurgeonActivity"]);
                        this.input["actcategory"] = "105267";
                    } else {
                        this.$(".gmPanelTitle").html(GM.Global.Activity.HeaderTitle);
                        this.url = "crmactivity/criterialist";
                        this.input = {
                            "token": GM.Global.Token,
                            "criteriaid": GM.Global.Activity.criteriaid,
                            "criteriatyp": "salescall"
                        };
                        this.getCriteriaData(GM.Global.Activity.criteriaid);
                    }

                } else if (GM.Global.Activity.FilterID != undefined) {

                    if (typeof (GM.Global.Activity.FilterID) != "object")
                        GM.Global.Activity.FilterID = $.parseJSON(GM.Global.Activity.FilterID)
                    this.input = GM.Global.Activity.FilterID;
                    this.input["token"] = GM.Global.Token;
                    console.log(GM.Global.Activity.FilterName)
                    if (typeof (GM.Global.Activity.FilterName) != "object")
                        GM.Global.Activity.FilterName = $.parseJSON(GM.Global.Activity.FilterName);
                    this.input["actcategory"] = "105267";

                    this.showFilterName(GM.Global.Activity.FilterName);
                    this.favoriteData = GM.Global.Activity.FilterID;
                    this.$(".gmPanelTitle").html("Sales Call list selected by the categories");
                } else {
                    this.$(".gmPanelTitle").html("My Sales Calls");
                    if (GM.Global.Activity.criteriaid == "surgeon") {
                        this.input = $.parseJSON(GM.Global["SurgeonActivity"]);
                        this.input["actcategory"] = "105267";
                        GM.Global.Activity.criteriaid = undefined;
                    } else {
                        this.input["actstartdate"] = this.today;
                        this.input["actstatus"] = "105761";
                        //                        this.input["actstartdate"] = dateOfnPastDays(salescallDays);
                        this.$(".gmPanelTitle").html("Upcoming Sales Calls");
                    }
                }

                if (isRep()) {
                    this.input["userid"] = this.userID;
                }
                this.showCRMData();

            },
            listTraining: function () {
                var that = this;
                this.setList.actcategory = '105268';
                this.input = {
                    "token": GM.Global.Token,
                    "actcategory": "105268"
                };
                this.voname = "gmCRMActivityVO";
                this.sortoptions = [{
                    "DefaultSort": "actstartdate",
                    "Elements": [{
                            "ID": "actnm",
                            "DispName": "Title",
                            "ClassName": "gmDashColHead gmFonttraining li-activity-name"
                        },
                        {
                            "ID": "actstartdate",
                            "DispName": "StartDate",
                            "ClassName": "gmDashColHead gmFonttraining li-activity-date"
                        },
                        {
                            "ID": "actenddate",
                            "DispName": "EndDate",
                            "ClassName": "gmDashColHead gmFonttraining li-activity-date"
                        },
                        {
                            "ID": "location",
                            "DispName": "Location",
                            "ClassName": "gmDashColHead gmFonttraining gmText150"
                        },
                        {
                            "ID": "actstatus",
                            "DispName": "Result",
                            "ClassName": "gmDashColHead gmFonttraining gmText100"
                        }]
                }];
                this.filterOptions = ["actnm", "location", "actstatus"];
                this.columnheader = 5;
                this.template_URL = URL_Training_Template;
                this.itemtemplate = "gmtTrainingList";
                this.controlID = "#div-main-content";
                this.search = ["actnm", "actstartdate", "actenddate", "location", "actstatus"];
                this.url = "crmactivity/info";
                this.color = "gmFonttraining";
                $("#span-app-title").html("Training");
                // this.$(".gmBtnGroup #li-create").removeClass("hide");
                if (GM.Global.Activity.actnm != undefined) {
                    this.input = {
                        "token": GM.Global.Token,
                        "actnm": GM.Global.Activity.actnm,
                        "actcategory": "105268"
                    };
                    this.$(".gmPanelTitle").html("Training list selected by the" + " ' " + GM.Global.Activity.actnm + " ' ");
                } else if ((GM.Global.Activity.firstnm || GM.Global.Activity.lastnm) != undefined) {
                    this.input = {
                        "token": GM.Global.Token,
                        "actcategory": "105268",
                        "firstnm": GM.Global.Activity.firstnm,
                        "lastnm": GM.Global.Activity.lastnm
                    };
                    this.$(".gmPanelTitle").html("Training list selected by the" + " ' " + GM.Global.Activity.firstnm + " " + GM.Global.Activity.lastnm + " ' ");

                } else if (GM.Global.Activity.criteriaid != undefined) {
                    this.$(".gmPanelTitle").html(GM.Global.Activity.HeaderTitle);
                    this.url = "crmactivity/criterialist";
                    this.input["actcategory"] = "105268";
                    this.input = {
                        "token": GM.Global.Token,
                        "criteriaid": GM.Global.Activity.criteriaid,
                        "criteriatyp": "training"
                    };
                    this.getCriteriaData(GM.Global.Activity.criteriaid);
                } else if (GM.Global.Activity.FilterID != undefined) {

                    if (typeof (GM.Global.Activity.FilterID) != "object")
                        GM.Global.Activity.FilterID = $.parseJSON(GM.Global.Activity.FilterID)
                    this.input = GM.Global.Activity.FilterID;
                    this.input["token"] = GM.Global.Token;
                    console.log(GM.Global.Activity.FilterName)
                    if (typeof (GM.Global.Activity.FilterName) != "object")
                        GM.Global.Activity.FilterName = $.parseJSON(GM.Global.Activity.FilterName)

                    this.input["actcategory"] = "105268";

                    this.showFilterName(GM.Global.Activity.FilterName);
                    this.favoriteData = GM.Global.Activity.FilterID;
                    this.$(".gmPanelTitle").html("Training list selected by the categories");

                } else {
                    this.$(".gmPanelTitle").html("Upcoming Training");
                    this.input["actstartdate"] = this.today;
                }
                this.showCRMData();

            },


            listMERC: function () {
                var that = this;
                this.setList.actcategory = '105265';
                $("#span-app-title").html("MERC");
                this.input = {
                    "token": GM.Global.Token,
                    "actcategory": "105265"
                };
                this.voname = "gmCRMActivityVO";
                this.sortoptions = [{
                    "DefaultSort": "actstartdate",
                    "Elements": [{
                            "ID": "actnm",
                            "DispName": "Title",
                            "ClassName": "gmDashColHead gmFontmerc li-activity-name"
                        },
                        {
                            "ID": "actstartdate",
                            "DispName": " From Date",
                            "ClassName": "gmDashColHead gmFontmerc li-activity-date"
                        },
                        {
                            "ID": "actenddate",
                            "DispName": " To Date",
                            "ClassName": "gmDashColHead gmFontmerc li-activity-date"
                        },
                        {
                            "ID": "location",
                            "DispName": "Location",
                            "ClassName": "gmDashColHead gmFontmerc gmText150"
                        },
                        {
                            "ID": "actstatus",
                            "DispName": "Status",
                            "ClassName": "gmDashColHead gmFontmerc gmText100"
                        }]
                }];
                this.filterOptions = ["actnm", "actstartdate", "location", "actstatus"];
                this.columnheader = 5;
                this.template_URL = URL_MERC_Template;
                this.itemtemplate = "gmtMERCList";
                this.controlID = "#div-main-content";
                this.search = ["actnm", "actstartdate", "actenddate", "location", "actstatus"];
                this.url = "crmactivity/info";
                this.color = "gmFontmerc";

                console.log(GM.Global.Activity)
                console.log(GM.Global.Activity.actnm)
                if (GM.Global.Activity.actnm != undefined) {
                    this.input = {
                        "token": GM.Global.Token,
                        "actnm": GM.Global.Activity.actnm,
                        "actcategory": "105265"
                    };
                    this.$(".gmPanelTitle").html("MERC list selected by the" + " ' " + GM.Global.Activity.actnm + " ' ");

                } else if ((GM.Global.Activity.firstnm || GM.Global.Activity.lastnm) != undefined) {
                    this.input = {
                        "token": GM.Global.Token,
                        "actcategory": "105265",
                        "firstnm": GM.Global.Activity.firstnm,
                        "lastnm": GM.Global.Activity.lastnm
                    };
                    this.$(".gmPanelTitle").html("MERC list selected by the" + " ' " + GM.Global.Activity.firstnm + " " + GM.Global.Activity.lastnm + " ' ");
                } else if (GM.Global.Activity.criteriaid != undefined) {
                    console.log(GM.Global.Activity.criteriaid)
                    if (GM.Global.Activity.criteriaid == "surgeon") {
                        this.input = $.parseJSON(GM.Global["SurgeonActivity"]);
                        this.input["actcategory"] = "105265";
                    } else {
                        this.$(".gmPanelTitle").html(GM.Global.Activity.HeaderTitle);
                        this.url = "crmactivity/criterialist";
                        this.input = {
                            "token": GM.Global.Token,
                            "criteriaid": GM.Global.Activity.criteriaid,
                            "criteriatyp": "merc"
                        };
                        this.getCriteriaData(GM.Global.Activity.criteriaid);
                    }

                } else if (GM.Global.Activity.FilterID != undefined) {
                    console.log(GM.Global.Activity.FilterName)
                    if (typeof (GM.Global.Activity.FilterID) != "object")
                        GM.Global.Activity.FilterID = $.parseJSON(GM.Global.Activity.FilterID)
                    this.input = GM.Global.Activity.FilterID;
                    this.input["token"] = GM.Global.Token;
                    this.input["actcategory"] = "105265";
                    //                this.input["createdbyid"] = localStorage.getItem("partyID");
                    if (typeof (GM.Global.Activity.FilterName) != "object")
                        GM.Global.Activity.FilterName = $.parseJSON(GM.Global.Activity.FilterName)
                    this.showFilterName(GM.Global.Activity.FilterName);
                    this.favoriteData = GM.Global.Activity.FilterID;
                    this.$(".gmPanelTitle").html("MERC list selected by the categories");


                } else {
                    this.$(".gmPanelTitle").html("Upcoming MERC");

                    this.input["actstartdate"] = this.today;
                }

                if (GM.Global.Activity.criteriaid != undefined && GM.Global.Activity.criteriaid != 0)
                    this.csvFileName = "CRM_MERC_Criteria_" + this.$(".gmPanelTitle").html().replace(" ", "_") + ".csv";
                else if (GM.Global.Activity.criteriaid != undefined && GM.Global.Activity.criteriaid == 0)
                    this.csvFileName = "CRM_MERC_List_" + this.$(".gmPanelTitle").html().replace(" ", "_") + ".csv";

                this.showCRMData();

            },

            assignee: function (e) {
                var that = this;
                hideMessages();
                this.$("#div-main-content").children().bind('click', false);
                this.$el.append(that.template_LeadAssignee())
                console.log($(e.currentTarget));
                this.leadid = $(e.currentTarget).find('div').attr('data-leadid');
                console.log(this.leadid);

                this.searchRep();
            },
            searchRep: function () {
                var that = this;
                var searchOptions = {
                    'el': $("#div-assign-to-search"),
                    'url': URL_WSServer + 'search/rep',
                    'inputParameters': {
                        'token': localStorage.getItem('token')
                    },
                    'keyParameter': 'repname',
                    'IDParameter': 'repid',
                    'NameParameter': 'repname',
                    'resultVO': 'gmSalesRepQuickVO',
                    'minChar': 2,
                    'callback': function (result) {
                        that.getRepInfo(result.ID, result.Name, result.Element, result.partyid);
                    }
                };
                var searchView = new GMVSearch(searchOptions);
            },

            getRepInfo: function (ID, Name, Element, partyid) {
                var that = this;
                var input = {
                    "token": GM.Global.Token,
                    "actid": this.leadid,
                    "actcategory": 105269
                };
                fnGetWebServerData("crmactivity/info", "gmCRMActivityVO", input, function (data) {
                    console.log(data);
                    if (data != null) {
                        if (data.arrgmactivitypartyvo != undefined)
                            data.arrgmactivitypartyvo = arrayOf(data.arrgmactivitypartyvo);
                        else
                            data.arrgmactivitypartyvo = [];
                        if (data.arrgmcrmaddressvo != undefined)
                            data.arrgmcrmaddressvo = arrayOf(data.arrgmcrmaddressvo);
                        else
                            data.arrgmcrmaddressvo = [];
                        if (data.arrgmactivityattrvo != undefined)
                            data.arrgmactivityattrvo = arrayOf(data.arrgmactivityattrvo);
                        else
                            data.arrgmactivityattrvo = [];
                        if (data.arrgmcrmlogvo != undefined)
                            data.arrgmcrmlogvo = arrayOf(data.arrgmcrmlogvo);
                        else
                            data.arrgmcrmlogvo = [];
                        data.partyid = partyid;
                        data.partynm = Name;
                        data.token = GM.Global.Token;
                        var nm = Name.replace(" ", "");
                        nm = nm.toLowerCase();
                        nm = nm + "@globusmedical.com";
                        fnGetWebServerData("crmactivity/save", undefined, data, function (saveData) {
                            console.log(saveData);
                            if (saveData != null)
                                Backbone.history.loadUrl(Backbone.history.fragment);
                        });
                    }

                });

            },
            closeOverlay: function () {
                $("#div-lead-assginee-main").remove();
                this.$("#div-main-content").children().unbind('click', false);
            },

            showCRMData: function () {
                var that = this;
                if (GM.Global.Filtersave == "true") {
                    $(".li-exist-favorite").removeClass("hide");
                    if (!GM.Global.SwitchUser)
                        $(".li-edit-favorite").removeClass("hide");
                    $(".li-new-favorite").addClass("hide");
                    $("#div-favorite-title").html(GM.Global.FilterName);
                    GM.Global.Filtersave = "false";
                }
                console.log(this.url)
                console.log(this.voname)
                console.log(this.input)
                console.log(JSON.stringify(this.input));

                fnGetWebServerData(this.url, this.voname, this.input, function (data) {
                    console.log("showCRMData >>>>>>>>")
                    console.log(data);
                    if (that.url == "crmactivity/info" && data != null)
                        data = data.GmCRMListVO.gmCRMActivityVO;
                    console.log(data);
                    that.displayList(data);
                });
            },

            checkSurgeonAccess: function () {
                var userTabs = _.pluck($.parseJSON(localStorage.getItem('userTabs')), "functionID");
                if (!_.contains(GM.Global.Admin, "surgeon"))
                    this.$(".li-surg-add-merc").remove();
                if (!_.contains(GM.Global.Admin, "salescall"))
                    this.$(".li-surg-salescall").remove();
                if (!_.contains(GM.Global.Admin, "physicianvisit") || !_.contains(GM.Global.Admin, "physvisitrqst"))
                    this.$(".li-surg-physicianvisit").remove();
                console.log(isADVP())
                if (!isADVP())
                    this.$(".li-lead-assignto").remove();
            },


            showFilterName: function (data) {
                var that = this;
                var tempArray = new Array();
                $.each(data, function (key, val) {
                    if (val != "")
                        tempArray.push({
                            "title": key,
                            "value": val
                        });
                });
                var criteriaValues = $.parseJSON(JSON.stringify(tempArray));
                this.$(".div-category-items").html(this.template_selected_items(criteriaValues));
                if (GM.Global.SwitchUser)
                    $("#div-criteia-save").addClass("hide");
                $(".div-text-selected-crt").addClass("gmFont" + GM.Global.Module);
                $(".li-new-favorite").removeClass("hide");
                console.log(GM.Global.myFilter)
                if (GM.Global.myFilter != null && typeof (GM.Global.myFilter) != "object")
                    GM.Global.myFilter = $.parseJSON(GM.Global.myFilter);


                var FilterJson = GM.Global.myFilter;
                if (FilterJson != null) {
                    $("#div-module-main #input-add-favorite").val(FilterJson.Name);
                    that.filterID = FilterJson.ID;
                    //                    GM.Global.myFilter = null;
                }
            },
            getCriteriaData: function (criteriaid) {
                var that = this;
                this.criteriaID = criteriaid;
                var tempArray = new Array();
                var input = {
                    "token": GM.Global.Token,
                    "criteriaid": this.criteriaID
                };
                fnGetWebServerData("criteria/get", undefined, input, function (data) {
                    console.log(data);
                    if (data != null) {
                        data.listGmCriteriaAttrVO = arrayOf(data.listGmCriteriaAttrVO);
                        that.criteriaData(data);
                    }
                });

            },

            listModule: function (data) {
                var that = this;
                _.each(data.listGmCriteriaAttrVO, function (v) {
                    that.setList[v.key] = v.value;
                });
                switch (this.module) {
                    case "physicianvisit":
                        fnGetListActivity(this.setList, "", function (data) {
                            console.log("fnGetPhy" + JSON.stringify(data));
                            that.displayList(data);
                        });
                        break;
                }
            },

            criteriaData: function (data) {
                this.criterianm = data.criterianm;
                var that = this;
                GM.Global.criteriaAttr = [];
                GM.Global.criteriaAttr = arrayOf(data.listGmCriteriaAttrVO);
                _.each(data.listGmCriteriaAttrVO, function (data) {
                    if (data.key == "crffrmdate") {
                        that.frmdate = data.value;
                        that.tempJSON["From Date"] = that.frmdate;
                    }
                    if (data.key == "crftodate") {
                        that.todate = data.value;
                        that.tempJSON["To Date"] = that.todate;
                    }
                    if (data.key == "actstartdate") {
                        that.frmdate = data.value;
                        that.tempJSON["From Date"] = that.frmdate;
                    }
                    if (data.key == "actenddate") {
                        that.todate = data.value;
                        that.tempJSON["To Date"] = that.todate;
                    }
                    if (data.key == "actfrmdate") {
                        that.frmdate = data.value;
                        that.tempJSON["From Date"] = that.frmdate;
                    }
                    if (data.key == "acttodate") {
                        that.todate = data.value;
                        that.tempJSON["To Date"] = that.todate;
                    }


                    if (data.key == "systemid") {
                        that.productsList.push(data.value);
                    }

                    if (data.key == "state") {
                        that.stateList.push(data.value);
                    }
                    if (data.key == "procedure") {
                        that.techqList.push(data.value);
                    }
                    if (data.key == "specality") {
                        that.skillList.push(data.value);
                    }
                    if (data.key == "acttyp") {
                        that.activityList.push(data.value);
                    }
                    if (data.key == "role") {
                        that.roleList.push(data.value);
                    }
                    if (data.key == "acttypeid") {
                        that.activityList.push(data.value);
                    }
                    if (data.key == "actstatus") {
                        that.statusList.push(data.value);
                    }
                    if (data.key == "crfstatus") {
                        that.crfstatusList.push(data.value);
                    }

                    if (data.key == "regid") {
                        that.RegionList.push(data.value);

                    }
                    that.newArray = _.union(that.stateList, that.techqList, that.skillList, that.activityList, that.roleList, that.activityList, that.productsList, that.statusList, that.crfstatusList, that.RegionList);
                });
                console.log(that.newArray)
                if (that.newArray != "")
                    that.getCodeName(that.newArray);
                else if (that.productsList != "")
                    that.getSystemName(that.productsList);
                else
                    that.popUpTemplate();
            },

            // Edit favoriter selected data
            editFavorite: function () {
                var that = this;
                console.log(that.filterID);
                if (that.filterID != "")
                    window.location.href = "#crm/" + that.module + "/" + "filter" + "/" + that.filterID;
                else
                    window.location.href = "#crm/" + that.module + "/" + "filter" + "/" + that.criteriaID;
            },

            // Save as favorite current view list
            addFavorite: function (event) {
                var that = this;
                var criterianm
                that.jsnData = {};

                criterianm = $("#input-add-favorite").val();

                if (criterianm != "") {
                    that.jsnData.criterianm = criterianm;
                    if (that.filterID != "")
                        that.jsnData.criteriaid = that.filterID;
                    else
                        that.jsnData.criteriaid = "";
                    var key, value;
                    $.each(this.favoriteData, function (key, value) {
                        if (value != "" && key != "token")
                            that.arrAttribute.push({
                                "key": key,
                                "value": value,
                                "attrid": "",
                                "criteriaid": that.jsnData.criteriaid,
                                "voidfl": ""
                            });
                    });
                    console.log(this.favoriteData)
                    that.jsnData.criteriatyp = that.module;
                    that.jsnData.token = GM.Global.Token;
                    that.jsnData.listGmCriteriaAttrVO = $.parseJSON(JSON.stringify(that.arrAttribute));
                    console.log(JSON.stringify(that.jsnData));

                    if (GM.Global.Device) {
                        fnSaveCriteria(that.jsnData.criteriaid, that.jsnData, function (data, ID) {
                            if (data == "success") {
                                that.filterID = ID;
                                $(".li-exist-favorite").removeClass("hide");
                                if (!GM.Global.SwitchUser)
                                    $(".li-edit-favorite").removeClass("hide");
                                $(".li-new-favorite").addClass("hide");
                                $("#div-favorite-title").html(criterianm);
                                showSuccess("Favorite is added successfully");
                                hideMessages();
                            } else
                                showError("Favorite save is failed!");
                        });
                    } else {
                        fnGetWebServerData("criteria/save", undefined, that.jsnData, function (data) {
                            console.log(data)
                            if (data != null) {
                                that.filterID = data.criteriaid;
                                $(".li-exist-favorite").removeClass("hide");
                                if (!GM.Global.SwitchUser)
                                    $(".li-edit-favorite").removeClass("hide");
                                $(".li-new-favorite").addClass("hide");
                                $("#div-favorite-title").html(data.criterianm);
                                showSuccess("Favorite is added successfully");
                                hideMessages();
                            } else
                                showError("Favorite save is failed!");
                        });
                    }

                } else
                    showError("Please enter the Favorite Name");
            },

            // To Get system name for to process data in popup template
            getSystemName: function (sysid) {
                var that = this;
                var sysid = sysid.toString();
                var systemInput = {
                    "token": GM.Global.Token,
                    "sysid": sysid
                };
                fnGetWebServerData("search/system", "gmSystemBasicVO", systemInput, function (data) {
                    data = arrayOf(data);
                    if (sysid != undefined) {
                        var systemList = _.pluck(data, "sysnm");
                        var systemID = _.pluck(data, "sysid");
                        that.tempJSON["Product Used"] = systemList.toString().replace(/,/g, " / ");

                        if (systemList != null) {
                            var countVal = systemList.length;
                            that.defaultVals = systemID;
                            $(".div-surg-popup-system").attr("data-values", that.defaultVals);
                            $(".div-surg-popup-system").find("span").text("(" + countVal + ")");
                            $(".div-surg-popup-system").css("background", "#FDF5E6");
                        }
                    }
                    that.popUpTemplate();
                });
            },

            // get CodeLookUp names for to process data in popup template
            getCodeName: function (codeid) {
                var that = this;
                var codeid = codeid.toString();
                if (codeid != "") {
                    var input = {
                        "token": GM.Global.Token,
                        "codeid": codeid
                    };
                    fnGetWebServerData("search/codelookup", "gmCodeLookUpVO", input, function (data) {
                        console.log(data)
                        data = arrayOf(data);
                        if (that.stateList != "") {
                            var stateResult = _.filter(data, function (data) {
                                return data.codegrp == "STATE";
                            });
                            var stateList = _.pluck(arrayOf(stateResult), "codenm");
                            that.tempJSON["State"] = stateList.toString().replace(/,/g, " / ");

                        }
                        if (that.techqList != "") {
                            var techqResult = _.filter(data, function (data) {
                                return data.codegrp == "TECHQ";
                            });
                            var techqList = _.pluck(arrayOf(techqResult), "codenm");
                            that.tempJSON["Technique"] = techqList.toString().replace(/,/g, " / ");
                        }
                        if (that.skillList != "") {
                            var specalityResult = _.filter(data, function (data) {
                                return data.codegrp == "SGSPC";
                            });
                            var specalityList = _.pluck(arrayOf(specalityResult), "codenm");
                            that.tempJSON["Skillset"] = specalityList.toString().replace(/,/g, " / ");
                        }
                        if (that.roleList != "") {
                            var roleResult = _.filter(data, function (data) {
                                return data.codegrp == "ACTROL";
                            });
                            var roleList = _.pluck(arrayOf(roleResult), "codenm");
                            that.tempJSON["Role"] = roleList.toString().replace(/,/g, " / ");
                        }
                        if (that.activityList != "") {
                            var acttypResult = _.filter(data, function (data) {
                                return data.codegrp == "SGACTP";
                            });
                            var acttypList = _.pluck(arrayOf(acttypResult), "codenm");
                            that.tempJSON["Activity Type"] = acttypList.toString().replace(/,/g, " / ");
                        }
                        if (that.statusList != "") {
                            var statusResult = _.filter(data, function (data) {
                                return data.codegrp == "CRFSTS";
                            });
                            var statusList = _.pluck(arrayOf(statusResult), "codenm");
                            that.tempJSON["CRF Status"] = statusList.toString().replace(/,/g, " / ");
                        }
                        if (that.crfstatusList != "") {

                            var statusResult = _.filter(data, function (data) {
                                return data.codegrp == "CRFSTS";
                            });
                            var crfstatusList = _.pluck(arrayOf(statusResult), "codenm");
                            that.tempJSON["CRF Status"] = crfstatusList.toString().replace(/,/g, " / ");
                        }
                        if (that.productsList != "") {
                            that.getSystemName(that.productsList);
                        } else {
                            that.popUpTemplate();
                        }
                    });
                } else if (that.productsList != "") {
                    that.getSystemName(that.productsList);
                }
            },
            showItemListView: function (controlID, data, itemtemplate, columnheader, sortoptions, template_URL, filterOptions, csvFileName, color, callbackOnReRender) {
                //            var that = this
                data = arrayOf(data)
                console.log(data);
                console.log("SERSERSER")
                console.log(this.search)
                for (var i = 0; i < data.length; i++) { // This Code is put back to avoid writing it's web service for all lists
                    var row = data[i];
                    data[i].search = "";
                    if (this.search && this.search.length) {
                        _.each(this.search, function (srh) {
                            data[i].search += row[srh] + " ";
                        })
                    }
                }
                var items = new GMCItem(data);
                this.itemlistview = new GMVItemList({
                    el: controlID,
                    collection: items,
                    columnheader: columnheader,
                    itemtemplate: itemtemplate,
                    sortoptions: sortoptions,
                    template_URL: template_URL,
                    filterOptions: filterOptions,
                    csvFileName: csvFileName,
                    color: color,
                    callbackOnReRender: callbackOnReRender
                });


            },

            // To show and append selected criteria data in the template
            popUpTemplate: function () {
                var that = this;
                var tempArray = new Array();
                $.each(that.tempJSON, function (key, val) {
                    if (val != "") {
                        tempArray.push({
                            "title": key,
                            "value": val
                        });
                    }
                });
                var criteriaValues = $.parseJSON(JSON.stringify(tempArray));
                console.log(criteriaValues)
                $(".div-category-items").html(that.template_selected_items(criteriaValues));
                if (GM.Global.SwitchUser)
                    $("#div-criteia-save").addClass("hide");
                $(".div-text-selected-crt").addClass("gmFont" + GM.Global.Module);
                $(".li-new-favorite").addClass("hide");
                $(".li-exist-favorite").removeClass("hide");
                if (!GM.Global.SwitchUser)
                    $(".li-edit-favorite").removeClass("hide");
                $("#div-favorite-title").html(that.criterianm);
            },

            // Toggle slide for to create surgoen activity from surgeon list 
            createSurActivity: function (e) {
                console.log(e.currentTarget);
                if ($(e.currentTarget).hasClass("active-tab")) {
                    $(e.currentTarget).removeClass("active-tab");
                    $(".li-surg-create-activity").addClass("hide");
                } else {
                    $(".active-tab").removeClass("active-tab");
                    $(".li-surg-create-activity").addClass("hide");
                    $(e.currentTarget).addClass("active-tab");
                    $(e.currentTarget).parent().find(".li-surg-create-activity").removeClass("hide");
                }
            },

            createleadActivity: function (e) {
                console.log(e.currentTarget);
                if ($(e.currentTarget).hasClass("active-tab")) {
                    $(e.currentTarget).removeClass("active-tab");
                    $(".li-lead-create-activity").addClass("hide");
                } else {
                    $(".active-tab").removeClass("active-tab");
                    $(".li-lead-create-activity").addClass("hide");
                    $(e.currentTarget).addClass("active-tab");
                    $(e.currentTarget).parent().find(".li-lead-create-activity").removeClass("hide");
                }
            },

            createActivity: function () {
                if (GM.Global.Activity == undefined)
                    GM.Global.Activity = {};
                if (this.module != "surgeon")
                    GM.Global.Activity.Mode = "create";
                window.location.href = "#crm/" + this.module + "/id/0";
            },
            showTSDetail: function (e) {
                var id;
                if (!$(e.currentTarget).hasClass("ul-ts-phn-list"))
                    id = $(e.currentTarget).parent().attr("actid");
                else
                    id = $(e.currentTarget).attr("actid");
                window.location.href = "#crm/tradeshow/id/" + id;
            },

            openMap: function () {
                var that = this;
                console.log(GM.Global.FieldSales.Data);
                var fsData = GM.Global.FieldSales.Data;
                $("#li-geo").addClass("hide");
                if (fsData != null && fsData != undefined) {
                    hideMessages();
                    var repIDs = _.pluck(fsData, "repid").join(",");
                    console.log(repIDs);
                    var fsListData = window.location.hash;
                    console.log(window.location.hash)

                    console.log(fsListData);
                    window.location.href = "#crm/fieldsales/map/" + repIDs;
                } else {
                    showError("No data to List")
                }
            },

            listUpcomingMERC: function (event) {
                var that = this;
                if ($(event.currentTarget).hasClass("act-merc")) {
                    this.surgeonID = $(event.target).parent().parent().attr("data-partyid");
                    this.surgeonName = $(event.target).parent().parent().attr("data-partynm");
                } else {
                    this.surgeonID = $(event.currentTarget).attr("data-partyid");
                    this.surgeonName = $(event.currentTarget).attr("data-partynm");
                }
                showPopup();
                $("#div-crm-overlay-content-container .modal-body").html(this.template_SurgeonMERC());
                $(".modal-title").text($(event.currentTarget).attr("title"));
                $(".modal-footer").addClass("hide");

                var voname = "gmCRMActivityVO";
                var sortoptions = [{
                    "DefaultSort": "actstartdate",
                    "Elements": [{
                            "ID": "actnm",
                            "DispName": "Title",
                            "ClassName": "gmDashColHead gmFontmerc gmText150"
                        },
                        {
                            "ID": "actstartdate",
                            "DispName": " From Date",
                            "ClassName": "gmDashColHead gmFontmerc gmText125"
                        },
                        {
                            "ID": "actenddate",
                            "DispName": " To Date",
                            "ClassName": "gmDashColHead gmFontmerc gmText100"
                        },
                        {
                            "ID": "location",
                            "DispName": "Location",
                            "ClassName": "gmDashColHead gmFontmerc gmText125"
                        }]
                }];
                var filterOptions = ["actnm", "actstartdate", "location", "actstatus"];
                var itemtemplate = "gmtSurgeonMERCList";
                var columnheader = 6;
                var template_URL = URL_Surgeon_Template;
                var controlID = "#div-crm-overlay-content-container #div-popup-merc-list";
                var url = "crmactivity/info";
                var color = "gmFontmerc";
                that.search = ["actnm", "actstartdate", "actenddate", "location"]
                var today = new Date();
                var input = {
                    "token": GM.Global.Token,
                    "actcategory": "105265",
                    "actstartdate": this.today
                };
                console.log(JSON.stringify(input))
                fnGetWebServerData(url, voname, input, function (data) {
                    if (data != null) {
                        data = data.GmCRMListVO.gmCRMActivityVO;
                        data = arrayOf(data);
                        _.each(data, function (data) {
                            data.actstartdate = getDateByFormat(data.actstartdate);
                            data.actenddate = getDateByFormat(data.actenddate);
                            if (data.partyid != "" && data.partyid.indexOf(that.surgeonID) >= 0)
                                data["added"] = "Y";
                            if (data.attrpartyid != "" && data.attrpartyid.indexOf(that.surgeonID) >= 0)
                                data["reqstd"] = "Y";
                        });
                        that.showItemListView(controlID, data, itemtemplate, columnheader, sortoptions, template_URL, filterOptions, undefined, color);
                        $("#div-crm-overlay-content-container .modal-body .li-add-merc").bind("click", function (event) {
                            that.addParty(event);
                        });
                    } else
                        $(controlID).html("No Data Found");

                });
            },

            addParty: function (event) {
                var that = this;

                var input = {
                    "attrid": "",
                    "attrval": this.surgeonID,
                    "attrnm": this.surgeonName,
                    "actid": $(event.currentTarget).attr("actid"),
                    "attrtype": "105589",
                    "voidfl": ""
                };

                console.log("Add Party Input" + JSON.stringify(input));
                hidePopup();
                fnGetWebServerData("crmactivity/addattr", "gmCRMActivityAttrVO", input, function (data) {
                    if (data != null)
                        showSuccess(that.surgeonName + " requested for " + $(event.currentTarget).attr("actnm"), 5000);
                    else
                        showError("Request Failed");
                });

            },

            displayList: function (data) {
                var that = this;
                if (data != null) {

                    if (that.module == "crf" && !_.contains(GM.Global.Admin, "crfappr") && _.contains(GM.Global.Admin, "crf") && that.crfMobileList == "Y") {
                        var userData = _.filter(data, function (dt) {
                            return dt.createdby == that.userID;
                        });

                        if (userData != "")
                            data = arrayOf(userData);
                    }

                    data = arrayOf($.parseJSON(JSON.stringify(data)));
                    if (that.module == "lead") {
                        that.leadData = arrayOf(data);
                        _.each(data, function (data) {
                            if (data.assignee == "")
                                data.assignee = "Unassigned";
                        });
                    }
                    if ((that.module == "physicianvisit") && (GM.Global.Device)) {
                        _.each(data, function (data) {
                            data.phynm = data.title;
                            data.actid = data.id;
                        });
                    }

                    var userTabs = _.pluck($.parseJSON(localStorage.getItem('userTabs')), "functionID");

                    _.each(data, function (data, i) {
                        if ((_.contains(GM.Global.Admin, "physicianvisit")) || (_.contains(GM.Global.Admin, "physvisitrqst")))
                            data.phy = "Y";
                        if (_.contains(GM.Global.Admin, "surgeon"))
                            data.title = parseNull(data.salutenm) + " " + data.firstnm + " " + parseNull(data.midinitial) + " " + data.lastnm;
                        if (_.contains(GM.Global.Admin, "surgeon"))
                            data.merc = "Y";
                        if (_.contains(GM.Global.Admin, "salescall"))
                            data.scall = "Y";
                        if (_.contains(GM.Global.Admin, "lead") && !isRep())
                            data.lead = "Y";
                        //                        data["index"] = i + 1;
                        data.server = URL_CRMFileServer;
                        if (that.module == "salescall" || that.module == "physicianvisit") {
                            if (data.actstarttime != "")
                                data.actstarttime = ampm(data.actstarttime);
                            if (data.actendtime != "")
                                data.actendtime = ampm(data.actendtime);
                        }
                        if (that.module == "fieldsales") {
                            if (GM.Global.FieldSalesRegion == "Y") {
                                data.FSRegion = "Y";
                            }

                        }

                        var dd, mm, yyyy;
                        if (data.actstartdate && data.actstartdate != "") {
                            data.actstartdate = getDateByFormat(data.actstartdate);
                        }
                        if (data.actenddate && data.actenddate != "") {
                            data.actenddate = getDateByFormat(data.actenddate);
                        }


                    });



                    for (var i = 0; i < data.length; i++) { // This Code is put back to avoid writing it's web service for all lists
                        var row = data[i];
                        if (that.search && that.search.length) {
                            data[i].search = "";
                            _.each(that.search, function (srh) {
                                data[i].search += row[srh] + " ";
                            })
                        }
                    }

                    if ((that.module == "merc") || (that.module == "training")) { // To show program status in list
                        for (var i = 0; i < data.length; i++) {
                            if (arrayOf(data[i].actstartdate != (null || undefined))) {
                                var todayDate = new Date();
                                todayDate.setHours(0, 0, 0, 0);
                                var fromdate = new Date(data[i].actstartdate);
                                fromdate.setHours(0, 0, 0, 0);
                                var todate = new Date(data[i].actenddate);
                                todate.setHours(0, 0, 0, 0);
                                if (todayDate > todate) {
                                    data[i].actstatus = "Done";
                                } else if (todayDate < fromdate) {
                                    data[i].actstatus = "Pending";
                                } else {
                                    data[i].actstatus = "In Progress";
                                }
                            }
                        }
                    }




                    if (that.module == "fieldsales" && GM.Global.FieldSalesRegion != "Y")
                        GM.Global.FieldSales.Data = data;

                    console.log(data);
                    that.collection = new GMCItem(data);
                    that.gmvItemList = new GMVItemList({
                        el: that.controlID,
                        collection: that.collection,
                        columnheader: that.columnheader,
                        itemtemplate: that.itemtemplate,
                        sortoptions: that.sortoptions,
                        template_URL: that.template_URL,
                        filterOptions: that.filterOptions,
                        csvFileName: that.csvFileName,
                        color: that.color,
                        callbackOnReRender: that.callbackOnReRender
                    });

                    if (that.module == "lead" && !isADVP())
                        $(".li-lead-assignto").remove();
                } else
                    that.$("#div-main-content").html("No Data Found").addClass("gmTextAll gmTextCenter gmPaddingTop10p");
                resizeHeadWidth('.gmPanelHead', '.gmBtnGroup', '#panelhead-left');
            },

            toggleSelectedItems: function (event) {
                if ($("#criteria-selected-itmes").hasClass("gmMediaDisplayNone")) {
                    $("#criteria-selected-itmes").removeClass("gmMediaDisplayNone");
                    $("#selected-criteria-icon").find(".fa-chevron-down").addClass("fa-chevron-up").removeClass("fa-chevron-down");
                } else {
                    $("#criteria-selected-itmes").addClass("gmMediaDisplayNone");
                    $("#selected-criteria-icon").find(".fa-chevron-up").addClass("fa-chevron-down").removeClass("fa-chevron-up");
                }
            }


        });

        return GM.View.CRMData;
    });
