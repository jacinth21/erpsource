define([

    // External Libraries
    'jquery', 'underscore', 'backbone', 'handlebars',

    // Common
    'global',

    // Pre-Compiled Template
    'gmtTemplate'

],

    function ($, _, Backbone, Handlebars, Global, GMTTemplate) {

        'use strict';

        GM.View.CRMFileUploadView = Backbone.View.extend({

            template: fnGetTemplate(URL_Common_Template, "gmtCRMFileUpload"),

            events: {
                "click #btn-fileupload-popup-browse": "browseFile",
                "click #btn-fileupload-upload": "uploadFile",
                "change .txt-fileupload": "selectFile"
            },

            initialize: function (options) {
                var that = this;
                this.el = options.el;
                this.url = options.url;
                this.parent = options.parent;
                this.voname = options.voname;
                this.refid = options.refid;
                this.refgrp = options.refgrp;
                this.fileid = options.fileid;
                this.filetitle = options.filetitle;
                this.validation = options.validation;
                this.arrAcceptetFormat = options.arrAcceptetFormat;
                this.displayEl = options.displayEl;
                this.callback = options.callback;
                this.render();

            },

            render: function () {
                this.$el.html(this.template());
                this.$("#hdn-refid").val(this.refid);
                this.$("#hdn-refgrp").val(this.refgrp);
                this.$(".txt-doc-title").val(this.filetitle)
                if (parseInt(this.refid) == 0)
                    this.$("#btn-fileupload-upload").addClass("hide");
                else {
                    var that = this;
                    var input = { "refid": that.refid, "refgroup": that.refgrp };

                    fnGetWebServerData("file/get", "gmCRMFileUploadVO", input, function (fileData) {
                        if (fileData != null) {
                            that.serverFileData = fileData;
                            $(that.displayEl).text(that.serverFileData.filetitle);
                            $(that.displayEl).attr("href", URL_CRMFileServer + that.serverFileData.refgroup + "/" + that.serverFileData.refid + "/" + that.serverFileData.filename);
                            that.filetitle = that.serverFileData.filetitle;
                            that.fileid = that.serverFileData.fileid;
                        }
                    });
                }

                return this;
            },

            browseFile: function (event) {
                this.$("#txt-fileupload").val('');
                if (!GM.Global.Device) {
                    event.preventDefault();
                    this.$(".txt-fileupload").trigger("click");
                } else
                    showError("You Can Upload Profile Picture in Online Mode Only");
            },

            uploadFile: function (event) {
                if (event)
                    event.preventDefault();
                hideMessages();

                if (($("#span-fileupload-browse-filenm").text() != "" || this.dispData != undefined) && GM.Global.file != null && GM.Global.file != undefined) {
                    var that = this;
                    var input;
                    console.log(GM.Global.FormData);
                    console.log(this.parent.SurgeonFiles);
                    if (GM.Global.FormData != undefined && GM.Global.FormData != null && this.parent.SurgeonFiles == undefined) {
                        GM.Global.FormData.append('token', localStorage.getItem('token'));
                        input = GM.Global.FormData;
                    } else {
                        this.formData.append('token', localStorage.getItem('token'));
                        input = this.formData;
                    }
                    console.log(input);
                    $.ajax({
                        url: URL_WSServer + 'uploadedFile/crmfileupload',
                        data: input,
                        cache: false,
                        contentType: false,
                        processData: false,
                        type: 'POST',
                        success: function (result) {
                            console.log("File Uploaded >>> " + result);
                            that.uploadFileName();
                        },
                        error: function (model, response, errorReport) {
                            console.log("File Upload Failed >>> " + model);
                            if (event)
                                event.preventDefault();
                        }
                    });
                } else {
                    showError("*Please select a valid file");
                }

            },

            uploadFileName: function () {
                var that = this;
                var token = localStorage.getItem("token");
                if (that.dispData == undefined) {
                    var filename = this.$("#span-fileupload-browse-filenm").text();
                    var input = { "token": token, "refid": this.refid, "filename": filename, "deletefl": "", "refgroup": this.refgrp, "filetitle": this.filetitle};
                    if (this.fileid)
                        input['fileid'] = this.fileid;
                } else {
                    var input = that.dispData;
                    if (that.dispData.fileid)
                        input['fileid'] = that.dispData.fileid;
                    if (GM.Global.Filetitle != undefined && GM.Global.Filetitle != null) {
                        input.filename = GM.Global.Filename;
                        input.filetitle = GM.Global.Filetitle;
                        if (GM.Global.Fileid)
                            input.fileid = GM.Global.Fileid;
                    }
                    if (that.parent.module != undefined && that.parent.module == "lead") {
                        input.filetitle = "Lead File Upload";
                        input.refid = "105269";
                    }
                }
                
                delete input.server;
                delete input.path;
                delete input.search;
                
                console.log("uploadFileName input >>>>>>>> " + JSON.stringify(input));
                fnGetWebServerData("file/savefile", undefined, input, function (fileData) {
                    console.log("uploadFileName output >>>>>>>> " + JSON.stringify(fileData));
                    if (GM.Global.FileUploadViewData != undefined)
                        ++GM.Global.FileUploadViewData;
                    if (fileData != null) {
                        that.serverFileData = fileData;
                        $(that.displayEl).text(that.serverFileData.filetitle);
                        console.log(that.displayEl)
                        $(that.displayEl).attr("href", URL_CRMFileServer + that.serverFileData.refgroup + "/" + that.serverFileData.refid + "/" + that.serverFileData.filename);
                        that.filetitle = that.serverFileData.filetitle;
                        that.fileid = that.serverFileData.fileid;
                        that.formData = {};
                        $("#span-fileupload-browse-filenm").html("");
                        if (that.parent.module && that.parent.module != "lead")
                            that.getUploadFile();
                        if (that.callback) {
                            if (GM.Global.FileUploadData == undefined) {
                                that.callback(fileData.fileid);
                            } else if (GM.Global.FileUploadViewData == GM.Global.FileUploadData) { // Surgeon Document Save
                                GM.Global.FileUploadViewData = undefined;
                                GM.Global.FileUploadData = undefined;
                                that.callback();
                            }
                        }
                    }
                }, true);

            },
            getUploadFile: function () {
                var input = { "token": localStorage.getItem("token"), "refid": GM.Global.Activityid, "refgroup": "107994" };
                GM.Global.Activityid = null;
                console.log("gmCRMFileUploadVO" + JSON.stringify(input));
                if (!GM.Global.Device) {
                    fnGetWebServerData("file/get", "gmCRMFileUploadVO", input, function (File) {
                        console.log("gmCRMFileUploadVO result" + JSON.stringify(File));
                        if (File != null) {
                            var file;
                            if (File.length > 1)
                                file = File[0];
                            else
                                file = File;
                            GM.Global.Fileid = file.fileid;
                            console.log("GM.Global.Fileid" + GM.Global.Fileid)
                            console.log(URL_CRMFileServer + file.refgroup + "/" + file.refid + "/" + file.filename)
                            $(".a-service-desc-agenda-file-name").text(file.filetitle);
                            $(".a-service-desc-agenda-file-name").attr("href", URL_CRMFileServer + file.refgroup + "/" + file.refid + "/" + file.filename);
                            $(".a-service-desc-agenda-file-name").attr("data-fileid", file.fileid);
                        } else
                            $(".a-service-desc-agenda-file-name").html("Unavailable").addClass("gmFontItalic");
                    }, true);
                }

            },
            
            selectFile: function (e) {
                var that = this;
                var file = e.target.files[0];
                GM.Global.file = file;
                var format = _.last(file.name.split('.'));
                if (!_.contains(this.arrAcceptetFormat, format.toLowerCase())) {
                    $(e.currentTarget).parent().find(".spn-file-nm").html("");
                    showError("Invalid File Format. Please select any ." + this.arrAcceptetFormat.join(' or .') + " file");
                } else {
                    hideMessages();
                    $(e.currentTarget).parent().find(".spn-file-nm").html(file.name);
                    var $form = $(e.currentTarget).parent().parent(".frm-documents"),
                        params = $form.serializeArray(),
                        files = $form.find('[name="file"]')[0].files;
                    this.formData = new FormData();

                    $.each(files, function (i, file) {
                        that.formData.append('file', file);
                    });
                    $.each(params, function (i, val) {
                        that.formData.append(val.name, val.value);

                    });

                    GM.Global.FormData = that.formData;

                    if (this.validation != undefined)
                        this.validation(file);
                    if (that.dispData != undefined) {
                        that.dispData.filename = file.name;
                        if (this.parent.module) {
                            GM.Global.Filetitle = that.parent.actModel.get("actnm");
                            GM.Global.Filename = file.name;
                            this.parent.nameFile = false;
                        } else {
                            var name = parseNull(that.parent.model.get("salutenm")) + " " + that.parent.model.get("firstnm") + parseNull(that.parent.model.get("midinitial")) + " " + that.parent.model.get("lastnm");
                            that.dispData.filetitle = that.dispData.reftypenm + " - " + name;
                        }

                    }

                }
            },

            reRender: function (data) {
                //            console.log(this.$el.get(0));
                this.$el.html(this.template());
                $(".frm-documents .li-fileupload-upload").hide();
                $(".frm-documents .lb-spn-file-open").hide();
                this.$el.find(".spn-file-nm").html(data);
            },

            close: function () {
                this.unbind();
                this.undelegateEvents();
                this.$el.empty();
            }

        });

        return GM.View.CRMFileUploadView;
    });