define([
        'jquery', 'underscore', 'backbone', 'handlebars',
        'global',

        // Pre-Compiled Template
        'gmtTemplate'
        ], function ($, _, Backbone, Handlebars, Global, GMTTemplate) {

	'use strict';
    
	GM.View.CRMTab = Backbone.View.extend({

        template_tab: fnGetTemplate(URL_Common_Template, "gmtCRMTab"),
        
        events: {
            "click .gmTab" : "setTabColor"
        },
        
        initialize: function(options) {
            this.render(options);
        },
        
		render: function (options) {
            this.$el = options.el;
//            console.log(JSON.stringify(options.data))
            this.$el.html(this.template_tab(options.data));
			return this;
		},
        
        setTabColor: function(e) {
            $('.gmTab').removeClass().addClass('gmTab');
            var color = $(e.currentTarget).attr("color");
            $(e.currentTarget).addClass("gmTab " + color);

            GM.Global.Module = $(e.currentTarget).attr("module");
            GM.Global.Title = $(e.currentTarget).attr("title");
            if (GM.Global.FieldSales)
                GM.Global.FieldSales.Module = null;
        }
	});

	return GM.View.CRMTab;
});