/**********************************************************************************
 * File:        GmLoaderView.js
 * Description: Contains the full loader which stops user to use the app until a process completes
 * Version:     1.0.0
 * Author:      praja
 **********************************************************************************/

define([
    'jquery', 'underscore', 'backbone', 'handlebars',
    'global', 'notification'
], 

function ($, _, Backbone, Handlebars, Global, Notification) {

    'use strict';
    
    var LoaderView = Backbone.View.extend({
    
        el: "#div-main-loader",

	    initialize: function(options) {
            // this.template = fnGetTemplate(options.templateURL, options.template);
            this.render(options);
        },
        
        render: function(options) {
            this.$el.show().find("#loader-text").html(options.text);
        },

        close: function (argument) {
            this.$el.hide();
        }
    });

	return LoaderView;
});
