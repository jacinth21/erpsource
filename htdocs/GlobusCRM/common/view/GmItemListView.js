/**********************************************************************************
 * File:        GmItemListView.js
 * Description: This view contains the list items.
 * Version:     1.0
 * Author:      praja
 **********************************************************************************/

define([
    'jquery', 'underscore', 'backbone', 'handlebars',
    'global', 'notification',
    'itemcollection', 'itemmodel', 'itemview'
],

function ($, _, Backbone, Handlebars, Global, Notification, ItemCol, ItemModel, ItemView) {

	'use strict';

	var minCollapsible = 10;

	var ItemListView = Backbone.View.extend({

		tagName: "ul",

		events: {
			"blur #txt-itemlist-search": "showFooter",
			"click .btn-column-head": "sortItems",
            "click #div-detail-search-x": "clearKeyword",
			"click .li-letter-above": "collapseSystems",
			"keyup #txt-itemlist-search": "searchItemList",
			"change #sel-attrib, #sel-sort-by": "sortiPhoneItems",
			"focus #txt-itemlist-search": "hideFooter"
           },

		template: fnGetTemplate(URL_Common_Template,"GmItemList"),

		initialize: function(options) {
			this.itemtemplate = options.itemtemplate;
			this.columnheader = options.columnheader;
		    this.renderType = options.renderType;
            this.sortoptions = options.sortoptions;
            this.currentcollection = this.collection;
            this.intResultCount =this.collection.length;
            this.template_URL = options.template_URL;
            this.callbackOnReRender =options.callbackOnReRender;
            
            var that = this;
			this.currentcollection.bind('sort', function(){
				that.render();
				if(that.callbackOnReRender!=undefined)
					that.callbackOnReRender(that);
			});
             if(this.sortoptions != undefined) {
            	this.DefaultSort = options.sortoptions[0].DefaultSort;
 				this.sortoptions = this.sortoptions[0]["Elements"];
            }
		   	if(this.renderType != undefined) {
			   	this.currentcollection.sortAsc("Name");
			}
			else if(this.DefaultSort != undefined) {
				this.currentcollection.sortAsc(this.DefaultSort);
			}
		   	this.selectvalue = 0;
		   	this.render();
		   	this.sortFirst();
		},

		render: function () {
			var that = this;
			if(this.renderType == 1) {
				this.$el.append(this.template());
			}
			else {
				this.$el.html(this.template());
			}
			
			switch(this.columnheader) {
				case 0:
					this.$("#div-column").remove();
					this.$("#txt-itemlist-search").remove();
					break;

				case 1:
					this.$("#div-column").remove();
					break;

				case 2:
					this.$("#txt-itemlist-search").remove();
					break;
			}

			var item = new ItemModel();
			var Current_Alphabet;
			var appendArea = "#div-itemlist";
			this.currentcollection.each(function(item) {
				if((that.renderType == 2) && (that.currentcollection.length > minCollapsible)) {
					if((item.get("Name").substring(0,1)!=Current_Alphabet) || (Current_Alphabet==undefined)) {
						Current_Alphabet = item.get("Name").substring(0,1).toUpperCase();
						var li = document.createElement('li');
						li.innerHTML = Current_Alphabet;
						li.className = "li-letter-above";
						var spn = document.createElement('span');
						spn.className = "spn-collpase-count";
						var spnCollapse = document.createElement('span');
						spnCollapse.className = "spn-indicate-collpase";
						spnCollapse.innerHTML = "+";
						li.appendChild(spnCollapse);
						li.appendChild(spn);
						that.$("#div-itemlist").append(li);
						var ul = document.createElement('ul');
						ul.id = "ul-under-" + Current_Alphabet.toLowerCase();
						that.$("#div-itemlist").append(ul);
						appendArea = ul;
					}
				}
				else if((that.renderType == 2) && (that.itemtemplate == "GmSubItem")) {
					that.itemtemplate = "GmItem";
				}
				var itemview = new ItemView({ model: item });
				//that.$(appendArea).append(itemview.render({template: this.itemtemplate}).el);
				that.$(appendArea).append(itemview.render({template_URL : that.template_URL, template: that.itemtemplate}).el);
				}, this);

            if((that.renderType == 2) && (that.currentcollection.length > minCollapsible)) {
				$(".li-letter-above").each(function() {
					var liAbove = this;
					var alphabet = $(this).text().toLowerCase().replace('+','');
					var count = 0;
					$("#ul-under-"+alphabet).find('li').each(function() {
						$(liAbove).find('.spn-collpase-count').html(count+1);
						count++;
					});
					
				});
			 }

 			if(this.sortoptions!=undefined) {
 				
 				//for dropdown headers
       			var select = document.createElement('select');
        		select.id = "sel-attrib";
        		var rightSelect = document.createElement('select');
        		rightSelect.id = "sel-sort-by";
        		var rightSelectOption1 = document.createElement('option');
        		var rightSelectOption2 = document.createElement('option');
        		rightSelectOption1.value = "ascending";
        		rightSelectOption2.value = "descending";
        		rightSelectOption1.innerHTML = "Ascending";
        		rightSelectOption2.innerHTML = "Descending";
        		rightSelect.appendChild(rightSelectOption1);
        		rightSelect.appendChild(rightSelectOption2);

        		for(var i=0; i<this.sortoptions.length; i++) {
        		    var option = this.sortoptions[i];
					var anOption = document.createElement('option');
					anOption.innerHTML = option["DispName"];
					anOption.value = option["ID"];
					select.appendChild(anOption);
							
					this.$("#div-column").append(select);
					this.$("#div-column").append(rightSelect);
        		}
			   
        	   //for row header
			   for(var i=0; i<this.sortoptions.length; i++) {
					var option = this.sortoptions[i];
					var anOption = document.createElement('li');
					var divArrow = document.createElement('div');
					anOption.innerHTML = option["DispName"];
					$(anOption).append(divArrow);
					var ClassName = option["ClassName"];
					anOption.id = option["ID"];
					this.$("#div-column").append(anOption);
					$(anOption).addClass("btn-column-head");
					$(anOption).addClass(ClassName);
					$(anOption).addClass("ascending");
					$(divArrow).attr('ID','td-arrow-head');
					$(divArrow).addClass("arrow-down-sort");
				}
			}

 			
 			switch(this.itemtemplate){
 				case "GmItemGroup" : 
 						this.$("#txt-itemlist-search").attr("placeholder","Search for Groups");
 						$("#div-itemlist-count").show();
  						$("#span-view-type").html("Groups");
 			            $("#spn-itemlist-count").html("&nbsp;" + this.intResultCount + "&nbsp;");
						break;
 				case "GmItemSet" :
 						this.$("#txt-itemlist-search").attr("placeholder","Search for Sets");
 						$("#div-itemlist-count").show();
						$("#span-view-type").html("Sets");
 			            $("#spn-itemlist-count").html("&nbsp;" + this.intResultCount + "&nbsp;");
						break;
 				case "GmItemSetPart" :
 						this.$("#txt-itemlist-search").attr("placeholder","Search for Parts");
 						$("#div-itemlist-count").show();
						$("#span-view-type").html("Parts");
 			            $("#spn-itemlist-count").html("&nbsp;" + this.intResultCount + "&nbsp;");
						break;
 				case "GmItemGroupPart" :
						this.$("#txt-itemlist-search").attr("placeholder","Search for Parts");
						$("#div-itemlist-count").show();
						$("#span-view-type").html("Parts");
			            $("#spn-itemlist-count").html("&nbsp;" + this.intResultCount + "&nbsp;");
						break;
 			}
 			return this;
		},
       
		/*show the sorting arrow for the sorted headers*/
		sortFirst: function() {
			var that = this;
			this.$(".btn-column-head").each(function() {
					if($(this).attr("id")==that.DefaultSort) {
						that.$("#"+that.DefaultSort).find('div').show();
						that.$("#"+that.DefaultSort).removeClass("ascending").addClass("descending");
						that.$("#"+that.DefaultSort).css({backgroundColor: '#ccc', color: 'black'});
					}
				});
			this.$("#sel-attrib").find('option').each(function() {
				if($(this).val() == that.DefaultSort)
					$(this).attr("selected","selected");
			})
				that.DefaultSort = null;
		},
        /*triggers the keyup event in the search textbox*/
		clearKeyword: function(e) {
			e.preventDefault();
			this.$("#txt-itemlist-search").val("");
			this.$("#txt-itemlist-search").trigger("keyup");
		    $("#div-search-count").hide();
		},

        /*Shows and hides the close button in search textbox*/
		setX: function(wordLength) {
			if(wordLength == 0) {
				this.$("#div-detail-search-x").hide();
			}
			else {
				this.$("#div-detail-search-x").show();
			}
		},
		//footer hide on focus textbox
		hideFooter: function(){
			$("#div-detail-navigation").hide();
		},

		//footer show on focus textbox
		showFooter: function(){
			$("#div-detail-navigation").show();
		},
		
		/*Process the search and render the result*/
		searchItemList: function (e) {
			var intSearchCount = 0;
			var letters = $(e.currentTarget).val();
			var wordLength = letters.length;
			var that = this;
			this.letters = letters;
			this.setX(wordLength);
			var id = $(e.currentTarget).parent().parent().attr("id");
			var chkTechnique = $("#div-node").text();

            $("#div-itemlist").empty();

            var letters = $(e.currentTarget).val();
            if(letters.length >=1){
    			$("#div-search-count").show();
				var items = this.collection.search("Name",letters);
				this.currentcollection=new ItemCol(items);
				this.currentcollection.bind('sort', this.render, this);
				var item = new ItemModel();
				that.$("#div-itemlist").empty();
				_.each(items,function(item){
					var view = new ItemView({model:item});
				    //that.$("#div-itemlist").append(view.render({template: that.itemtemplate}).el);
				    that.$("#div-itemlist").append(view.render({template_URL : that.template_URL, template:  that.itemtemplate}).el);
				});
				
				intSearchCount = this.currentcollection.length;
			}
			else if(letters.length ==0) {
	   			$("#div-search-count").hide();
				this.currentcollection = this.collection;
				this.render();
				if((id=="div-sidepanel-list") && (chkTechnique != "TECHNIQUE")) {
					$(".btn-itemlist").each(function() {
						$(this).removeClass("btn-itemlist").addClass("btn-close-catalog");
					});
				}
				this.$("#txt-itemlist-search").focus();
			}

			if(that.$("#div-itemlist").text().trim().length<1) {
				that.$("#div-itemlist").html("<li class='li-no-display'>No Search Results Found!</li>");
			}
			
            $("#span-search-count").html("&nbsp;" + intSearchCount + "&nbsp;");
			
			if((id=="div-sidepanel-list") && (chkTechnique != "TECHNIQUE")) {
				$(".btn-itemlist").each(function() {
					$(this).removeClass("btn-itemlist").addClass("btn-close-catalog");
				});
			}
		},
        
		/* Sort items in Ascending/Descending order */
		sortItems: function(e) {
			var elem = e.currentTarget;
			var currentClass= $(elem).attr('class');
			
			if ( currentClass.indexOf("ascending")>0 ) {
				this.currentcollection.sortAsc(elem.id);
				this.$("#txt-itemlist-search").val(this.letters);

				if(this.letters!=undefined) {
					this.setX(this.letters.length);
				}
				this.$("#" + elem.id).css({backgroundColor: '#ccc','color': 'black'});
				this.$("#" + elem.id).removeClass("ascending").addClass("descending");
				this.$("#" + elem.id).find("#td-arrow-head").removeClass("arrow-up-sort").addClass("arrow-down-sort").show();
	 		}
			
			else {
				this.currentcollection.sortDesc(elem.id);
				this.$("#txt-itemlist-search").val(this.letters);
				if(this.letters!=undefined){
					this.setX(this.letters.length);
				}
				this.$("#" + elem.id).css({backgroundColor: '#ccc','color': 'black'});
				this.$("#" + elem.id).removeClass("descending").addClass("ascending");
			    this.$("#" + elem.id).find("#td-arrow-head").removeClass("arrow-down-sort").addClass("arrow-up-sort").show();
			}
			
		},
		
		// sort ascending/ descending order in dropdown select for iphone
		sortiPhoneItems: function(e) {
			
			var attrib = $("#sel-attrib").val();
			var sortBy = $("#sel-sort-by").val();
			if ( sortBy== "ascending" ) {
				this.currentcollection.sortAsc(attrib);
				this.$("#txt-itemlist-search").val(this.letters);

				if(this.letters!=undefined) {
					this.setX(this.letters.length);
				}
	 		}
			else {
				this.currentcollection.sortDesc(attrib);
				this.$("#txt-itemlist-search").val(this.letters);
				if(this.letters!=undefined){
					this.setX(this.letters.length);
				}
			}
			this.$("#sel-attrib").find('option').each(function() {
				if($(this).val() == attrib) {
					$(this).attr("selected","selected");
				}
			});
			this.$("#sel-sort-by").find('option').each(function() {
				if($(this).val() == sortBy) {
					$(this).attr("selected","selected");
				}
			});
		},

		
        /* Display the systems in alphabetic order in a collapsible list */
        collapseSystems: function(event) {
        	var that = this;
            var alphabet = $(event.currentTarget).text().split('')[0].toLowerCase();
            var indicator = $(event.currentTarget).find(".spn-indicate-collpase");
            var indicatorText = $(indicator).text();
            this.$(".li-letter-above").each(function() {
            	$(this).removeClass("li-border-bottom");
            });
            this.$(".spn-indicate-collpase").each(function() {
            	$(this).text("+");
            });
            if(indicatorText == "+") {
            	$(indicator).text("-");
	            $(event.currentTarget).addClass("li-border-bottom");
	            if(this.$("#ul-under-"+alphabet).css("display")=="none") {
	            	this.$("#div-itemlist ul").each(function() {
	            		$(this).slideUp("fast");
		           	});
		           this.$("#ul-under-"+alphabet).slideDown("fast",function() {
			           that.$("#div-itemlist").animate({
					       scrollTop: that.$("#div-itemlist").scrollTop() + $("#ul-under-"+alphabet).position().top - 192
					   }, "fast");
		           });
	            }
            }
            else {
            	this.$("#ul-under-"+alphabet).slideUp("fast");
            }
        },

        close: function() {
        	this.unbind();
        	this.undelegateEvents();
        	this.$el.empty();
        }
	});

	return ItemListView;
});


