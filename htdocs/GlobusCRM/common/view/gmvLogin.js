    define([
    'jquery', 'underscore', 'backbone', 'handlebars',
    'global', 'gmvField', 'gmmUser',

    // Pre-Compiled Template
    'gmtTemplate'
], 

function ($, _, Backbone, Handlebars, Global, GMVField, GMMUser, GMTTemplate) {

    'use strict';
    
    GM.View.Login = Backbone.View.extend({

        el: "#div-main",

        events: {
            "blur #txt-userid, #txt-password"              : "validate",
            "click #btn-login, #li-login-button"           : "authenticate",
            "keypress #txt-password"                       : "clickLogin",
            "click #txt-userid"                            : "hidealert"
           
        },

        name: "Login View",
    
        template_Login: fnGetTemplate(URL_Common_Template, "gmtLogin"),
        template_LoginFailed: fnGetTemplate(URL_Common_Template, 'gmtLoginFailed'),

        initialize: function() {
            $("body").removeClass("background-2").addClass("background-1 background-image");
            $("#div-sales").hide();
            $("#div-header-right").hide();
           
            //localStorage.removeItem('token');
            //localStorage.removeItem('shname');
            $("#div-crm-user").empty();
            $("#ul-header-right").addClass("hide");
            $("#ul-mobile-main-header").addClass("hide");
//            $("#txt-mobile-title").hide();
//            $("#ul-header-right-pic").find("img").addClass("hide");
            
            
            //this.render();
            
        },

        //render the initial content after mainview loaded 
        render: function () {
            this.$el.html(this.template_Login());
            $("#div-popup").html(this.template_LoginFailed());
            $("#txt-userid").focus();
//            $("#div-footer").addClass("gmMediaDisplayNone");
            
            return this;
        },

        //authenticate the username and password
        authenticate: function() {
            //var strUserID = this.$("#txt-userid").val().toLowerCase();
            //var strPasswd = this.$("#txt-password").val();
			// Get loginName from keycloak 
			var strUserID = window.localStorage.getItem("loginName");
            var strPasswd = "";
            var that = this;
			// remove globus login
            /*if(this.$("#btn-login").text().trim().length==5) {
                if((strUserID.length==0) || (strPasswd.length==0)) {
                    var gmmUser = new GMMUser();
                    this.$("input").each(function() {
                        var gmvField = new GMVField({el: this, model: gmmUser});
                        gmvField.validate();
                    });
                }
                else { */
                    this.$("#btn-login").html("Logging in...");
                    console.log("Inside Authenticate");
                    fnAuthenticate(strUserID, strPasswd, function(data) {
                        console.log(data);
                        GM.Global.UserData = data;
                        localStorage.setItem("userData", JSON.stringify(data));
                        that.authorize(data);
                    });
        //        }
        //    }
        },

        clickLogin: function(e) {
            if(e.keyCode == 13)
                this.authenticate();
        },
        
        //authorize the user 
        authorize: function(data) {
            var that = this;
            if(data=="Internal Server Error") {
               
                this.$("#btn-login").html("Login");
                showError("Invalid Username / Password");
                that.$("#txt-userid").val('');
                that.$("#txt-password").val('');
                $("#alert-div").show();

                 }
            else if(typeof(data)=="object") {
                window.localStorage.setItem("token", window.localStorage.getItem("token"));
                window.localStorage.setItem("strShortName", data.shname);
                window.localStorage.setItem("userName", data.usernm); 
                window.localStorage.setItem("userID", data.userid);
                window.localStorage.setItem("userCompany", data.companyid);
                window.localStorage.setItem("userDivision", data.divid);
                window.localStorage.setItem("partyID", data.partyid);
                window.localStorage.setItem("acid", data.acid);
                window.localStorage.setItem("Dept", data.deptseq);
                window.localStorage.setItem("deptid", data.deptid);
                window.localStorage.setItem("fName", data.fname);
                window.localStorage.setItem("lName", data.lname);
                window.localStorage.setItem("repDivision", data.repdiv);
                var token = localStorage.getItem("token");
                
                GM.Global.SalesRep = isRep();

                GM.Global.UserName = window.localStorage.getItem("fName") + " " + window.localStorage.getItem("lName");
                GM.Global.UserID = window.localStorage.getItem("userID");

                if (token != null) {
                    var partyID = window.localStorage.getItem("partyID");
                    var input = { "token": localStorage.getItem("token"), "refid": partyID, "deletefl": "", "refgroup": "103112" };
                    console.log(input)
                    fnGetWebServerData("file/get", "gmCRMFileUploadVO", input, function (fileData) {
                        if (fileData != null) {
                            fileData = arrayOf(fileData);
                            fileData = fileData[0];
                            console.log("fileData" + fileData);
                            GM.Global.UserPicID = fileData.fileid;
                            $("#ul-header-right-pic").find("img").removeAttr("onerror").css("display", "inline-block");
                            $("#ul-header-right-pic").find("img").attr("src", URL_CRMFileServer + fileData.refgroup + "/" + fileData.refid + "/" + fileData.filename + "?num=" + Math.random());
                        }
                        $("#ul-header-right-pic").find("img").attr("onerror", "this.style.display = 'none'");
                    }, true);
                }
               
                window.location.href = "#crm";
                
            }
            else {  
                this.$("#btn-login").html("Login");
            }
        },

        //validate the username and password
        validate: function() {
            var user = new GMMUser();  
            $("input").each(function() {
                new GMVField({el: this, model: user});
            });
        },
        hidealert: function(){
            $("#alert-div").hide();
           
        } ,
        

        close: function() {
            this.unbind();
        }
      });
 
  return GM.View.Login;
});