/**********************************************************************************
 * File:        GmNotification.js
 * Description: Contains all functions pertaining to notification
 * Version:     1.0
 * Author:      ejayabalan
 **********************************************************************************/

function fnShowAlert(strMessage, strTitle) {
	if (navigator.notification) {
		navigator.notification.alert(strMessage, null, strTitle, 'OK');
	} else {
		alert(strTitle ? (strTitle + ": " + strMessage) : strMessage);
	}
}

function fnShowStatus(strMessage) {

//	$("#div-messagebar").html(strMessage);
    showMessages(strMessage);
	if(document.getElementById("div-messagebar")!=undefined)
		document.getElementById("div-messagebar").style.color="#FFF";
}

function fnShowError(strMessage) {
//	$("#div-messagebar").html(strMessage);

    showError(strMessage);
	if(document.getElementById("div-messagebar")!=undefined)
		document.getElementById("div-messagebar").style.color="#FFF";
}

function fnShowWarning(strMessage) {

//	$("#div-messagebar").html(strMessage);
    showError(strMessage);
	if(document.getElementById("div-messagebar")!=undefined)
		document.getElementById("div-messagebar").style.color="#FFF";
}

function showMsgView(id,value){

	if(document.getElementById("div-messagebar")!=undefined) {
		document.getElementById("div-messagebar").style.color="#FFF";
	}

	fnGetMessages(id,function(data){
       var Message = data.Message;
	    if(value!=undefined) {
	    	var val = value.split('>');
	    	for(var i=0;i<val.length;i++) {
	    		Message = Message.replace('?',val[i]);
	    	}
		}
//		$("#div-messagebar").html(Message);
//        $("#div-messagebar").attr("msg-id",id);
//		$("#div-messagebar").attr("msg-value",value);
//		$("#div-messagebar").trigger("change");
        
        showMessages(Message);

	});
}

function showNativeAlert(Message, Title, btnArray, callback) {
	navigator.notification.alert(
        Message, // message
        callback,           
        Title,           // title
       btnArray     // buttonLabels
    );
}


function showNativeConfirm(Message, Title, btnArray, callback) {
	navigator.notification.confirm(
        Message, // message
        callback,           
        Title,           // title
       btnArray     // buttonLabels
    );
}

function showNativePrompt(Message, Title, btnArray, defaultText, callback) {
	navigator.notification.prompt(
        Message, // message
        callback,           
        Title,           // title
        btnArray,     // buttonLabels
		defaultText
    );
}

