// TEST SERVER PATHS
//  var URL_WSServer = "http://wstest.spineit.net/resources/";  //Test path for Web Service 
//  var URL_FileServer = "http://mobiletest.spineit.net/globusapp/";  //Test path for Images
//  var URL_CRMFileServer = "http://gmivmapptest/www/globuscrm/";  //Test path for CRM files and Images
//  var URL_Portal = "http://test.spineit/"; //Test Portal URL
//  strEnvironment = "Test"
//  var intServerCountryCode = '80120';

// STAGE SERVER PATHS
// var URL_WSServer = "http://wsstage.spineit.net/resources/";  //Stage path for Web Service
// var URL_FileServer = "http://mobilestage.spineit.net/globusapp/";  //Stage path for Images
// var URL_CRMFileServer = "http://mobilestage.spineit.net/globusapp/crm/"; //Stage path for CRM files and Images
// var URL_Portal = "http://stage.spineit/"; //Stage Portal URL
// strEnvironment = "Stage";
// var intServerCountryCode = '80120';

// LOCAL MACHINE SERVER PATHS
//var URL_WSServer = "http://192.168.15.116:8080/resources/";  //Development path for Web Service
//var URL_FileServer = "http://192.168.15.116/www/globusapp/";  //Test path for Images
//var URL_CRMFileServer =  "http://192.168.15.116/www/globuscrm/";
//var URL_Portal = "http://192.168.15.116/"; //Development Portal URL
//strEnvironment = "Dev";
//var intServerCountryCode = '80120';


// PREPROD SERVER PATHS
// var URL_WSServer = "https://wspreprod.spineit.net/resources/";  //Preprod path for Web Service
// var URL_FileServer = "http://mobilepreprod.spineit.net/globusapp/";  //Preprod path for Images
// var URL_CRMFileServer = "http://mobilepreprod.spineit.net/globusapp/crm/";  //Test path for CRM files and Images
// var URL_Portal = "https://preprod.spineit.net/"; //PreProd Portal URL
// strEnvironment = "PreProd"
// var intServerCountryCode = '80120';

// PRODUCTION SERVER PATHS
var URL_WSServer = "https://wbs.spineit.net/resources/";  //Production path for Web Service
 var URL_FileServer = "https://mobile.spineit.net/globusapp/";  //Production path for Images
 var URL_CRMFileServer = "https://mobile.spineit.net/globusapp/crm/";  //Production path for CRM files and Images
 var URL_Portal = "https://erp.spineit.net/"; //Production Portal URL
 strEnvironment = "Production"
 var intServerCountryCode = '80120';