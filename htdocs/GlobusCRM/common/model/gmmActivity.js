define([
    'underscore',
    'backbone',
],

    function (_, Backbone) {

        'use strict';

        GM.Model.Activity = Backbone.Model.extend({

            // Default values of the Activity model
            defaults: {
                "actid": "",
                "actstartdate": "",
                "actenddate": "",
                "actstarttime": "",
                "actendtime": "",
                "acttype": "",
                "actnm": "",
                "actstatus": "",
                "actcategory": "",
                "partyid": "",
                "partynm": "",
                "voidfl": "",
                "toppriority": "",
                "otherinstr": "",
                "division": "",
                "actnotes": "",
                "dietary": "",
                "tzone": "",
                "region": "",
                "pathlgy": "",
                "workflow": "",
                "workflownm": "",
                "segid": "",
                "segnm": "",
                "zone": "",
                "adminsts": "",
                "arrgmactivityattrvo": [],
                "arrgmactivitypartyvo": [],
                "arrgmcrmaddressvo": [],
                "arrgmcrmlogvo": []
            },

            initialize: function (data) {
                this.set("arrgmactivityattrvo", new Array());
                this.set("arrgmactivitypartyvo", new Array());
                this.set("arrgmcrmaddressvo", new Array());
                this.set("arrgmcrmlogvo", new Array());
            },

            validate: function (attrs) {
                var errors = this.errors = {};
                if (!attrs.actstartdate) errors.actstartdate = "* Activity Start Date is required";
                if (!attrs.actenddate) errors.actenddate = "* Activity End Date is required";
                if (!_.isEmpty(errors)) return errors;
            }

        });

        return GM.Model.Activity;

    });
