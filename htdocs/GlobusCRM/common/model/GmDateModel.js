/* File:        GmUserModel.js
 * Description: Date Model
 * Version:     1.0
 * Author:      
 **********************************************************************************/
define([
    'underscore', 'backbone',
], 

function (_, Backbone) {
    
    'use strict';
    var DateModel = Backbone.Model.extend({
    	
    	initialize: function(){
    		
    	},

        //Validate the username and password
        validate: function(attrs) {

            var errors = this.errors = {};
            if (!attrs.to) errors.to = '* To date is empty';
            if (!attrs.from) errors.from = '* From Date is empty';
            if (!_.isEmpty(errors)) return errors;
          }
      });

  return DateModel;
});