/**********************************************************************************
 * File:        GmItemModel.js
 * Description: Item Model
 * Version:     1.0
 * Author:      praja
 **********************************************************************************/
define([
    'underscore',
    'backbone',
],

function (_, Backbone) {

	'use strict';

	var SearchModel = Backbone.Model.extend({

		// Default values of the model
		defaults: {

			ID: "txt-search-keyword",
			Name: "txt-search-name",
			Class: "",
			PredefinedClass: "txt-search-keyword-initial",
			placeholder: "Search...",
			minChar: 3,
			AutoSearch: true,
			usage: 1,
			target: "",
			template : 'GmSearchResult',
			template_URL : "common/template/",
		}
	});	

	return SearchModel;
});
