/**********************************************************************************
 * File:        GmItemModel.js
 * Description: Item Model
 * Version:     1.0
 * Author:      praja
 **********************************************************************************/
define([
    'underscore',
    'backbone',
],

function (_, Backbone) {

	'use strict';

	var SelectModel = Backbone.Model.extend({

		// Default values of the model
		defaults: {

			ID: "checked-data-id",
			Name: "checked-data-name",
			Class: "",
			PredefinedClass: "checked-data-list",
			placeholder: "Search...",
			usage: 1,
			target: "",
			IDParameter : "ID",
			NameParameter : "Name",
			modalResult: false,
			template : 'GmSelectResult',
			template_URL : URL_Common_Template
		}
	});	

	return SelectModel;
});
