/**********************************************************************************
 * File:        gmmSearch.js
 * Description: Item Model
 * Version:     1.0
 * Author:      praja
 **********************************************************************************/
define([
    'underscore',
    'backbone',
],

function (_, Backbone) {

	'use strict';

	GM.Model.Search = Backbone.Model.extend({

		// Default values of the model
		defaults: {

			ID: "txt-search-keyword",
			Name: "txt-search-name",
			Class: "",
			PredefinedClass: "txt-search-keyword-initial",
			placeholder: "Search...",
			minChar: 3,
			autoSearch: true,
			usage: 1,
			target: "",
			IDParameter : "ID",
			NameParameter : "Name",
			modalResult: false,
			template : 'gmtSearchResult',
			template_URL : "common/template/"
		}
	});	

	return GM.Model.Search;
});
