define([
    'underscore',
    'backbone',
],

function (_, Backbone) {

	'use strict';

	var FileUpload = Backbone.Model.extend({

		// Default values of the model
		defaults: {
			"fileid": "",
			"refid": "",
			"filename": "",
			"deletefl": "",
			"reftype": "",
			"type": "",
			"refgroup": "",
			"filetitle": "",
			"updateddate": "",
			"updatedby": "",
			"reftypenm": ""
		},

		initialize: function(data) {
			
		}

	});	

	return FileUpload;

});
