
define([
    'underscore', 'backbone',
], 

function (_, Backbone) {
    
    'use strict';
    GM.Model.User = Backbone.Model.extend({

        //Validate the username and password
        validate: function(attrs) {
            console.log(attrs);
            var phonePattern = new RegExp(/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/);
            var emailPattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
            var errors = this.errors = {};
            if (!attrs.userid) errors.userid = '* Username is required';
            if (!attrs.password) errors.password = '* Password is required';
            if(!attrs.FirstName) errors.FirstName = "*FirstName is required";
            if(!attrs.LastName) errors.LastName = "*LastName is required";
            console.log(!attrs.popupsearch)
            if(!attrs.popupsearch) errors.popupsearch = "*Select a value from the search list";
            if(attrs.popupsearch){
                if($("#div-crm-overlay-content-main #spn-popupsearch").attr("datavalue")==undefined || $("#div-crm-overlay-content-main #spn-popupsearch").attr("datavalue")==""){
                    errors.popupsearch = "*Select a value from the search list";
                }
                else{
                    errors.popupsearch = "";
                }
            }
            if (!attrs.surgeonname) errors.surgeonname = '* Surgeon Name is required';         
            if (!attrs.scadd1) errors.scadd1 = '* Address1 is required';   
            if (!attrs.scdate) errors.scdate = '* Date is required';   
            if (!attrs.sccity) errors.sccity = '* City is required';   
            if (!attrs.scstate) errors.scstate = '* State is required';   
            if (!attrs.sczip) errors.sczip = '* Zip is required';   
            if (!attrs.filtername) errors.filtername = '* Filtername is required'; 
            if (!attrs.trainingstate) errors.trainingstate = '* State is required'; 
            if (!attrs.trainingno) errors.trainingno = '* Control ID is required'; 
            if (!attrs.trainingtitle) errors.trainingtitle = '* Title is required'; 
            if (!attrs.trainingsdate) errors.trainingsdate = '* Start Date is required'; 
            if (!attrs.trainingedate) errors.trainingedate = '* End Date is required';

            if (!attrs.add1) errors.add1 = '* Address1 is required';   
            if (!attrs.date) errors.date = '* Date is required';   
            if (!attrs.city) errors.city = '* City is required';   
            if (!attrs.state) errors.state = '* State is required';   
            if (!attrs.zip) errors.zip = '* Zip is required';   
            if (!attrs.actid) errors.actid = '* Control ID is required'; 
            if (!attrs.actnm) errors.actnm = '* Title is required'; 
            if (!attrs.fdate) errors.fdate = '* From Date is required'; 
            if (!attrs.tdate) errors.tdate = '* To Date is required';             
            if (!attrs.type) errors.type = '* Type is required';             
            
            
            if (!attrs.phynm) errors.phynm = '* Physician name is required';             
            
            if (!attrs.popuplocation) errors.popuplocation = '* Location must be entered first';
            if (!attrs.popupsdate) errors.popupsdate = '* Start Date must be entered';
            if (!attrs.popupedate) errors.popupedate = '* End Date must be entered';
            if(attrs.cphone){if (phonePattern.test(attrs.cphone)!=true) errors.cphone = '* Enter the correct Phone Format';}
            if(attrs.aphone){if (phonePattern.test(attrs.aphone)!=true) errors.aphone = '* Enter the correct Phone Format';}
            if(attrs.bphone){if (phonePattern.test(attrs.bphone)!=true) errors.bphone = '* Enter the correct Phone Format';}
            if(attrs.email){if (emailPattern.test(attrs.email)!=true) errors.email = '* Enter the correct Email Format';}
            if(attrs.aemail){if (emailPattern.test(attrs.aemail)!=true) errors.aemail = '* Enter the correct Email Format';}
            if(attrs.bemail){if (emailPattern.test(attrs.bemail)!=true) errors.bemail = '* Enter the correct Email Format';}
            if (!_.isEmpty(errors)) return errors;
          }
      });

  return GM.Model.User;
});