/**********************************************************************************
 * File:        GmCommonUtil.js
 * Description: Common function usage for App 
 * Version:     1.0
 * Author:      jgurunthan
 **********************************************************************************/

// Function to get month Name by passing number
function getMonthName(mid) {
    const monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];
    return monthNames[mid - 1];
}

// Function to formtting currency with symbol($)
function fmtPrice(total) {
    var neg = false;
    if (total < 0) {
        neg = true;
        total = Math.abs(total);
    }
    return (neg ? "-$" : '$') + parseFloat(total, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString();
}

// Function to formtting currency with symbol($) and change as red color text
function compPriceFmt(price) {
    var userData = JSON.parse(localStorage.getItem("userData"));
    var priceFormat = userData.cmpcurrsmb;
    var neg = false;
    if (price < 0) {
        neg = true;
        price = Math.abs(price);
    }
    if (neg) {
        return new Handlebars.SafeString('<span class="gmFontRed">' + priceFormat + '(' + parseFloat(price, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString() + ')');
    } else {
        return ("$") + parseFloat(price, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString();
    }
}

//within brackets number function
function bracketsNumber(num) {
    var neg = false;
    if (num < 0) {
        neg = true;
        num = Math.abs(num);
    }
    if (neg) {
        return new Handlebars.SafeString('<span class="gmFontRed">(' + num + ')');
    } else {
        return num;
    }
}

//within brackets number handlebar function
Handlebars.registerHelper("bracketsNumber", function (num) {
    return bracketsNumber(num);
});

//formtting currency with symbol($) and change as red color text
Handlebars.registerHelper("compPriceFmt", function (price) {
    return compPriceFmt(price);
});


//formtting currency
Handlebars.registerHelper("formatCurrency", function (total) {
    return formatCurrency(total);
});


//Company Date Format
Handlebars.registerHelper("compDateFmt", function (dt) {
    return compDateFmt(dt);
});


/* fomattedDate() -Common method to get the date format based on the company 
 */

function compDateFmt(dt) {
    //    var userData = JSON.parse(localStorage.getItem("userData"));
    //    var dateformat = userData.cmpdfmt;
    //    try {
    //        if (dt != "" && dt != null && dt != undefined &&
    //            dateformat != "" && dateformat != null && dateformat != undefined) {
    //            if (dateformat != "yyddMM" && dateformat != "yyMMdd") {
    //                var resultedDate = $.format.date(new Date(dt), dateformat);
    //                console.log(resultedDate);
    //                if (resultedDate != "" || resultedDate != null || resultedDate != undefined || resultedDate != "NaN/NaN/NaN") {
    //                    dt = resultedDate;
    //                }
    //            }
    //        }
    //    } catch (err) {
    //        console.log(err);
    //    }
    return getDateByFormat(dt);
}


//  Common Function to get CodeLookUp values i/p: code id, code name, code group, code name alt and code access id.

function getCodeLookUpVal(codeid, codenm, codegrp, codenmalt, codeaccid, callback) {
    var input = {
        "token": localStorage.getItem("token")
    };
    if (codeid != "")
        input["codeid"] = codeid;
    if (codenm != "")
        input["codenm"] = codenm;
    if (codegrp != "")
        input["codegrp"] = codegrp;
    if (codenmalt != "")
        input["codenmalt"] = codenmalt;
    if (codeaccid != "")
        input["codeaccid"] = codeaccid;
    
    console.log("getCodeLookUpVal input>>>>>>" + JSON.stringify(input));
    fnGetWebServerData("search/codelookup", "gmCodeLookUpVO", input, function (data) {
        console.log("getCodeLookUpVal output>>>>>>" + JSON.stringify(data));
        callback(data);

    });
}

//This function is used to sort hyplink columns
function sortLink(a,b,order){ 
	a = a.substring(a.indexOf(">")+1,a.lastIndexOf("<"));
	b = b.substring(b.indexOf(">")+1,b.lastIndexOf("<"));
	  return (a.toLowerCase()>b.toLowerCase()?1:-1)*(order=="asc"?1:-1);
};

//This function is used to sort hyper link column Id as number Ex. <a href="">HC-100</a> 
function sortLinkID(a,b,order){ 
	var x = parseInt(a.substring(a.indexOf("-")+1,a.lastIndexOf("<")));
	var y = parseInt(b.substring(b.indexOf("-")+1,b.lastIndexOf("<")));
	x = isNaN(x)?0:x;
	y = isNaN(y)?0:y;
	  return (x>y?1:-1)*(order=="asc"?1:-1);
};




// Common Function To Display DTMLX Grid, Following Parameters as Input
// divRef - Id of the element, where the grid has to be displayed
// gridHeight - Height of the Grid, if it is empty then as 500
// gridData - JSON Object, Grid Data
// setHeader - Array, Grid Header Data
// setInitWidths - String, Width of Each Column in the Grid
// setColAlign - String, Alignment data of Each Column in the Grid (right,left,center,justify)
// setColTypes - String, Type of Data in Each Column in the Grid(ro-read only, ed - editable, txt- text, txttxt- plain text etc.. )
// setColSorting - String, Sort based on type of data in Each Column in the Grid(str, int, date, na or function object for custom sorting)
// enableTooltips - String, True/False to enable or disable tooltips
// setFilter - String, filters like #text_filter, #numeric_filter,..
// footerArry - Array, Grid Footer Data
// footerStyles - Array, Footer Data Styles
// footerExportFL - Boolean, true/false to attach footer icon to export as exdel and pdf
// Returns Grid Object gObj

function loadDHTMLXGrid(divRef, gridHeight, gridData, setHeader, setInitWidths, setColAlign, setColTypes, setColSorting, enableTooltips, setFilter, footerArry, footerStyles, footerExportFL) {
    var gObj = new dhtmlXGridObject(divRef);
    gObj.enableColSpan(true);
    gObj.setImagePath("/extweb/dhtmlx/dhtmlxGrid/imgs/");
    if (setHeader != undefined && setHeader.length > 0) {
        gObj.setHeader(setHeader);
        var colCount = setHeader.length;
    }
    
    if (setInitWidths != "")
        gObj.setInitWidths(setInitWidths);
    if (setColAlign != "")
        gObj.setColAlign(setColAlign);
    if (setColTypes != "")
        gObj.setColTypes(setColTypes);
    if (setColSorting != "")
        gObj.setColSorting(setColSorting);
    if (enableTooltips != "")
        gObj.enableTooltips(enableTooltips);
    if (setFilter != "")
        gObj.attachHeader(setFilter);
    if (footerArry != undefined && footerArry.length > 0) {
        var footstr = eval(footerArry).join(",");
        if (footerStyles != undefined && footerStyles.length > 0)
            gObj.attachFooter(footstr, footerStyles);
        else
            gObj.attachFooter(footstr);
    }
    if (footerExportFL)
        fnAttachExptFooter(divRef, colCount, gObj);
    if (gridHeight != "")
        gObj.enableAutoHeight(true, gridHeight, true);
    else
        gObj.enableAutoHeight(true, 500, true);

    gObj.init();
    gObj.parse(gridData, "json");
    return gObj;
}



//To attach Export Grid to excel option to the DHTMLX grid
//ColCount - add the icon in last two column
//gridData - to attach footer with this arg
function fnAttachExptFooter(divRef, colCount, gridData) {
    var footerExport = [];
    for (var j = 0; j < colCount; j++) {
        footerExport[j] = "";
        if (j == colCount - 2)
            footerExport[j] = "<section><span id='" + divRef + "-export' class='export-to-excel gmAlignHorizontal'><strong style='vertical-align:top;'>Export Grid</strong><i class='gmFont15em gmMarginLeft10p fa fa-download' aria-hidden='true' title='Export as Excel'></i></span></section> ";
        if (j == colCount - 1)
            footerExport[j] = "#cspan";
    }
    gridData.attachFooter(footerExport);
}

//Export DHTMLX Grid as Excel
//gridData - denotes which grid i have to export
//deleteIndexArr - Array, which are the columns are not need to export
function exportExcel(e, gridData, deleteIndexArr) {
    if (deleteIndexArr != undefined && deleteIndexArr.length > 0)
        _.each(deleteIndexArr, function (ind) {
            gridData.setColumnHidden(ind, true);
        });
    gridData.toExcel('/phpapp/excel/generate.php');
    //    gridData.toExcel("https://dhtmlxgrid.appspot.com/export/excel");
    if (deleteIndexArr != undefined && deleteIndexArr.length > 0)
        _.each(deleteIndexArr, function (ind) {
            gridData.setColumnHidden(ind, false);
        });
}


// Get WS dropdown Code LookUp values to respective div
// render el, code id, code name, code group, code alt name, Code Access ID, repalce by value, model setl name, boolean validation required (true or false), Initail select 
// arg passed 10 input and 1 output callback
function getCLValToDiv(divID, codeID, codeNm, codeGrp, codeNmAlt, codeAccID, replaceBy, attrVal, validate, initSelect, callback) {
    getCodeLookUpVal(codeID, codeNm, codeGrp, codeNmAlt, codeAccID, function (data) {
        console.log(data);
        var arrSelectOpt = new Array();
        if (initSelect)
            arrSelectOpt.push({
                "ID": "",
                "Name": "Select"
            });
        data = arrSelectOpt.concat(_.sortBy(data, function (data) {
            return data.codenm;
        }));
        if (replaceBy == "codenmalt")
            var strSelectOptions = JSON.stringify(data).replace(/codenmalt/g, 'ID').replace(/codenm/g, 'Name');
        else if (replaceBy == "codenm")
            var strSelectOptions = JSON.stringify(data).replace(/codenm/g, 'ID').replace(/codenm/g, 'Name');
        else
            var strSelectOptions = JSON.stringify(data).replace(/codeid/g, 'ID').replace(/codenm/g, 'Name');

        var optionItems = $.parseJSON(strSelectOptions);
        $(divID).empty();
        $(divID).each(function () {
            fnCreateOptions(this, optionItems);
            $(this).find("option").attr("name", attrVal);
        });

        if (validate)
            $(divID).attr("validate", "required");

        callback(divID, attrVal);
    });
}

//Function for used to Validate Url
function UrlValid(url) {
    console.log(url);
    return /^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(url);
}

//Function For Number Field Check 
function isNumberKey(e){
    console.log(e)
     var curInputCnt = $(e.currentTarget).attr("data-length");
     var curInputVal = $(e.currentTarget).val();
    var charCode = (e.which) ? e.which : event.keyCode;
//    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
    if (e.which != 8 && e.which != 0 && e.which != 13 && (e.which < 48 || e.which > 57) && (e.which < 96 || e.which > 105)) {
            return false; 
    }
    if (curInputVal.length >= parseInt(curInputCnt)) {
            return false;
    }
    return true;
}    


function isPriceKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        var curInputCnt = $(evt.currentTarget).attr("data-length");
        var curInputVal = $(evt.currentTarget).val();
        if(charCode==46){
            if(!(curInputVal.indexOf(".") > -1)){   
                return true;
            }
        }
        if (charCode > 31 && (charCode < 48 || charCode > 57) )
            return false;
        if (curInputVal.length >= parseInt(curInputCnt)) {
            return false;
        }
        return true;   
}

function limitDecimal(el){
    var v = parseFloat(el.value);
    el.value = (isNaN(v)) ? '' : v.toFixed(2);
}

//to show Company list as dropdown
function companyDropdown(input,defaultid,mode,DropDownView,elemAdmin,cb){
    var that = this;
    var drpDwnArr = [];
    this.cmpDropDownAdmin = _.findWhere(GM.Global.UserAccess, {
        funcnm: "CRM-SURGEON-ADMIN"
    });
    if (this.cmpDropDownAdmin != undefined){
        var adminAccess = "Y";
        var defaultObj = {};
        defaultObj.ID = "0";
        defaultObj.Name = "Choose One";
        drpDwnArr.push(defaultObj);
        if(mode == "create" || mode == "")
            defaultid = "0";
    }
    else{
        var adminAccess = "N";
        $(elemAdmin).addClass("pointer-event-none");
    }
    console.log(drpDwnArr);
    
    fnGetWebServerData("company/complist", "gmCompanyVO", input, function (data) {
        console.log(JSON.stringify(data));
        _.each(data, function (item) {
            var drpDownObj = {};
            drpDownObj.ID = item.companyid;
            drpDownObj.Name = item.companynm;
            drpDwnArr.push(drpDownObj);
        });
        that.cmpDropDownView = new DropDownView({
            el: $(elemAdmin),
            data: drpDwnArr,
            DefaultID: defaultid
        });
        if(adminAccess != "Y")
            $(elemAdmin+ " .div-arrow-dropdown").addClass("hide");

        cb(defaultid);
        $(elemAdmin).bind('onchange', function (e, selectedId, SelectedName) {
            cb(selectedId);
        });
    });
}
