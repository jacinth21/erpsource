/**********************************************************************************
 * File:        GmDORouter.js
 * Description: Router specifically meant to route views within the 
 *              DO application
 * Version:     1.0
 * Author:      tvenkateswarlu
 **********************************************************************************/

define([
        'jquery', 'underscore', 'backbone',
        'pricemainview'
         
      ], 
    
    function ($, _, Backbone, PriceMainView) {

	'use strict';

	var PriceRouter = Backbone.Router.extend({

		routes: {
			"price": "price",
			"price/:viewType": "price",
			"price/:viewType/:priceRequestId": "price"
		},

		price: function(viewType,priceRequestId){           
			this.closeView(pricemainview);
			var option = [];
			option.push(viewType);
			option.push(priceRequestId);
			pricemainview = new PriceMainView(option);
			$("#div-main").html(pricemainview.el);  
		},
		
		
		closeView: function(view) {
			if(view!=undefined) {
				view.unbind();
				view.remove();
				view = undefined;
				$("#btn-back").unbind();
			}

		}
	});

	return PriceRouter;
});