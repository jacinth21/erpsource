/**********************************************************************************
 * File:        gmmImpactAnalysis.js
 * Description: Impact analysis Screen
 * Version:     1.0
 * Author:     	matt B
 **********************************************************************************/
define([
    'underscore',
    'backbone',
],

    function (_, Backbone) {

        'use strict';

        var ImpactAnlyModel = Backbone.Model.extend({

            // Default values of the model
            defaults: {
                systemname: "",
                systemid: "",
                accountid: "",
                accountname: "",
                partid: "",
                partdesc: "",
                quantity: "",
                orderamt: "",
                propprice: "",
                orgname: "",
                adname: "",
                currency: "",
                gendate: "",
                requestid: "",
                impactperc: "",
                titleRef: ""
            }
        });

        return ImpactAnlyModel;
    });
