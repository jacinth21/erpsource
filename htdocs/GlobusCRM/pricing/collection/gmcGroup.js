/**********************************************************************************
 * File:        gmcGroup.js
 * Description: Price Initiate Screen
 * Version:     1.0
 * Author:     	treddy
 **********************************************************************************/
define([
    'underscore',
    'backbone',
    'gmmGroup'
], 

function (_, Backbone, GMMGroup) {

	'use strict';

	var GroupCollection = Backbone.Collection.extend({
		model: GMMGroup
	});

	return GroupCollection;
});