/**********************************************************************************
 * File:        gmcBusinessQuestions.js
 * Description: BusinessQuestions collection File
 * Version:     1.0
 * Author:     	Jreddy
 **********************************************************************************/
define([
    'underscore',
    'backbone',
    'gmmBusinessQuestions'
], 

function (_, Backbone, GMMBusinessQuestions) {

	'use strict';

	var BusinessQuestionsCollection = Backbone.Collection.extend({
		model: GMMBusinessQuestions
	});

	return BusinessQuestionsCollection;
});