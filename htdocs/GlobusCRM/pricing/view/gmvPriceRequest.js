/**********************************************************************************
 * File:        GmvPriceMain.js
 * Description: Price Initiate Screen
 * Version:     1.0
 * Author:     	jreddy
 * Tasks:        PRT-1,PRT-66,PRT-3,PRT-40,PRT-290,PRT-273
 **********************************************************************************/

define([
        'jquery', 'underscore', 'backbone', 'handlebars',
        'global', 'notification', 'pricemainview', 'dropdownview', 'searchview',
	    'gmmGroup', 'gmmSystem', 'gmcGroup', 'gmcSystem', 'gmmAccountPriceRequest', 'gmmBusinessQuestions', 'gmcBusinessQuestions',
	    'priceStatusReport', 'gmtTemplate', 'loaderview', 'gmmFileUpload', 'gmcFileUpload'
        ],

    function ($, _, Backbone, Handlebars, Global, Notification, PriceMainView, DropDownView, SearchView,
        GMMGroup, GMMSystem, GMCGroup, GMCSystem, GMMAccountPriceRequest, GMMBusinessQuestions, GMCBusinessQuestions,
        PriceStatusReport, GMTTemplate, LoaderView, GMMFileUpload, GMCFileUpload) {

        'use strict';
        var tabid;
        var PriceRequest = Backbone.View.extend({

            events: {
                "click .fa-minus-circle": "removeSystemOnclickOfMinusCircle",
                "click .div-price-threshold-btn": "showThresholdQuestions",
                "click .fa-information-circle": "showPriceAccountDetails",
                "click .div-p-icon": "showGroupPartPricingDetails",
                "click .li-productName": "getSystemGroupsDataOnClickOfProductName",
                "keyup .proposedUnit": "getProposedExtnPrice",
                "change .proposedUnit": "getProposedExtnPrice",
                "change .div-price-percentage-value-input input": "calcListOffPrice",
                "change .div-price-group-dt-input input": "calcListOffPriceforIndividualGrp",
                "click #li-price-check-auto-populate .toggle-button": "calcCustomPrice",
                "click #li-price-check-load-all-groups .toggle-button": "showAllGroups",
                "click #div-price-report-load-btn": "loadPricereport",
                "click #btn-main-save": "saveOrUpdatePricingDetails",
                "click #btn-main-submit": "saveOrUpdatePricingDetails",
                "change #li-price-system-value input": "pricePercentDetails",
                "click #btn-price-submit-go-btn": "fnLoadPriceReqDetails",
                "click #btn-price-go-btn": "fnGetAccountName",
                "keyup .validateNumber input": "validateNumber",
                "click #btn-main-cancel": "cancelRequest",
                "click #li-price-check-load-all-groups .toggle-button,#li-price-check-extend-current .toggle-button": "toggle",
                "click .li-price-level-construct": "fnGetConstructFullName",
                "click #li-price-system-name": "fnGetGroupFullName",
                "click .div-price-impact-btn": "calcImpactAnaly",
                "click #div-price-upload-btn": "showFileUploadPopup",
                "click #li-price-system-Pruposed-unit,#li-price-system-name,.div-p-icon": "highlightDisplay",
                "change .div-projected-scale-value input": "priceFormat",
                "click #fa-cross-id": "fnGetPartDetails",
                "click #fa-usd-id": "fnGetPartSystemDetails",
                "click #div-error-icon": "fnOpenPriceIndicatorDetails",
                "click #div-error-icon-main": "fnOpenAllPriceIndicatorDetails",
                "click .div-price-sales-analysis": "generateSalesAnalysis"


            },

            /* Loading the templates */
            template: fnGetTemplate(URL_Price_Template, "gmtPriceMain"),
            templatePriceReport: fnGetTemplate(URL_Price_Template, "gmtPriceReport"),
            templatePriceSubmitReq: fnGetTemplate(URL_Price_Template, "gmtPriceSubmittedReq"),
            templatePriceThresholdQusetions: fnGetTemplate(URL_Price_Template, "gmtThresholdQuestions"),
            templateImpactAnalysis: fnGetTemplate(URL_Price_Template, "gmtImpactAnalysis"),
            templatePriceAccountDetails: fnGetTemplate(URL_Price_Template, "gmtPriceAccountDetails"),
            templateGroupPartPricingDetails: fnGetTemplate(URL_Price_Template, "gmtGroupPartPricingDetails"),
            templateGmGroup: fnGetTemplate(URL_Price_Template, "gmtGroup"),
            templateGmGroupConstruct: fnGetTemplate(URL_Price_Template, "gmtGroupConstruct"),
            templateGmGroupSpeciality: fnGetTemplate(URL_Price_Template, "gmtGroupSpeciality"),
            templateSystemName: fnGetTemplate(URL_Price_Template, "gmtSystemName"),
            templatePricingReportData: fnGetTemplate(URL_Price_Template, "gmtPricingReportData"),
            templatePricingSpeclGroup: fnGetTemplate(URL_Price_Template, "gmtSpecialityGroup"),
            tplSpinner: fnGetTemplate(URL_Common_Template, "GmProgressSpinner"),
            templateShowName: fnGetTemplate(URL_Price_Template, "gmtShowName"),
            templateFileUpload: fnGetTemplate(URL_Common_Template, "gmtFileUploadDialog"),
            templateGPBPriceAccountDetails: fnGetTemplate(URL_Price_Template, "gmtPriceGPBAccountDetails"),
            templatepriceIndicator: fnGetTemplate(URL_Price_Template, "gmtErrorIndicator"),

            /*Initialize*/
            initialize: function (options) {
                this.el = options.el;
                pricetypedisc = [];
                var that = this;
                $("body").removeClass();
                if (GM.Global.Browser)
                    $("body").addClass("browser");
                $("#btn-home").show();
                $("#btn-back").show();
                $("#div-app-title").html("Pricing Request");
                this.Typeid = "";
                this.disctypeId = "52020"; // set default price Type to Custom$
                this.disctypeSys = "";
                this.statusTypeId = "";
                this.priceRequestIdValue = undefined;
                this.questionCount = "";
                //                this.alertCancel = false;
                this.arn = false;
                this.rptSubmit = false;
                this.fnSetConstantValues();
                this.render(options);
            },

            // Render the initial contents when pricing view is instantiated
            render: function (options) {
                var that = this;
                typeIdVal = "903107"; //set Account as default Type
                this.gmcGroup = new GMCGroup();
                this.gmcSystem = new GMCSystem();
                this.gmcBusinessQuestions = new GMCBusinessQuestions();
                this.gmmAccountPriceRequest = new GMMAccountPriceRequest();
                this.gmcFileUpload = new GMCFileUpload();
                this.gmmFileUpload = new GMMFileUpload();
                var headerObj = new Object();
                this.fnGetConstructLevels();
                if (options != null && options.viewOption == "initiatePricing") {
                    headerObj.viewOption = "initiatePricing";
                    accountIdVal = "";
                } else if (options != null && options.viewOption == "pricingSubmittedRequests") {
                    headerObj.viewOption = "pricingSubmittedRequests";
                }
                var switchuserfl = localStorage.getItem("switchuserFl");
                var inputJSNData = $.parseJSON(JSON.stringify(headerObj));
                this.$el.html(this.template(inputJSNData));

                fnGetRuleValues('PRTINT001', 'APPMSG', localStorage.getItem("cmpid"), function (msg) {
                    console.log(msg.rulevalue);
                    $("#div-pricing-gpb-message").html(msg.rulevalue);
                });
                
                fnGetRuleValues('PR_SYSTEM', 'PR_SYS_DIV_LIST', localStorage.getItem("cmpid"), function (msg) {
                    console.log(msg.rulevalue);
                     GM.Global.PRSYSDIVISION = msg.rulevalue;
                });

                if (options != null && options.viewOption == "pricingSubmittedRequests") {
                    this.$(".div-pricing-info-change").hide();
                    this.$(".ul-price-save-btn").hide();
                    this.$("#li-price-info-system-lable").hide();
                    this.$(".div-price-submit-label1").hide();
                    this.$(".fa-information-circle").hide();
                    this.$("#btn-main-save").hide();
                    this.$("#btn-main-submit").hide();
                    this.$("#btn-main-cancel").hide();
                    this.$(".fa-crosshairs").hide();
                    this.$(".fa-usd").hide();
                }

                this.$(".ul-price-detail-info").hide();
                this.getTypeDrp();
                this.discountTypeDrp();
                this.accid = "";
                this.systemID = "";
                this.flag = "";
                this.saveflag = "";
                this.question = "";
                this.qusnfl = "";
                this.questionValidate = "";
                this.priceDataID = "";
                var accRenderOptions = {
                    "el": this.$('#div-rep-account-list'),
                    "ID": "txt-billing-account",
                    "Name": "txt-search-name",
                    "Class": "txt-keyword-account",
                    "minChar": "3",
                    "autoSearch": true,
                    "placeholder": "Key in at least 3 characters to search",
                    "fnName": fnGetPricingAccounts,
                    "usage": 1,
                    "template": 'GmSearchResult',
                    "template_URL": URL_Common_Template,
                    "callback": function (rowValue) {
                        that.getAccount(rowValue);
                        $("#btn-main-save").show();
                        $("#btn-main-submit").show();
                    }
                };
                var acctsearchview = new SearchView(accRenderOptions);

                var systemSearchOptions = {
                    "el": this.$('#div-selected-system-list'),
                    "ID": "txt-keyword",
                    "Name": "txt-search-name",
                    "Class": "txt-keyword-initial",
                    "minChar": "3",
                    "autoSearch": true,
                    "placeholder": "Key in at least 3 characters to search",
                    "fnName": fnGetIMSetsTest,
                    "usage": 2,
                    "template": 'GmSearchResult',
                    "template_URL": URL_Common_Template,
                    "callback": function (sList) {
                        that.getSelectedSystemDetails(sList);
                        $("#li-price-check-load-all-groups .toggle-button").addClass("val-selected toggle-button-selected");
                        if ($("#li-price-check-load-all-groups div").hasClass("val-selected"))
                            $("#li-price-check-load-all-groups div").trigger("click");
                        if ($("#li-price-check-auto-populate div").hasClass("toggle-button-selected"))
                            $("#li-price-check-auto-populate div").removeClass("toggle-button-selected")
                    }
                };
                var systemsearchview = new SearchView(systemSearchOptions);

                // Code added for populating the Price Request Details while comming form the Dashboard Screen				
                if (options != null && options.viewOption == "pricingSubmittedRequests" && options.priceRequestId != "" && options.priceRequestId != undefined) {
                    this.priceRequestIdVal = options.priceRequestId;
                    this.rptSubmit = true;
                    this.fnGetPriceRequestDetails();
                }
                return this;
            },

            //proj. 12 month scale price format
            priceFormat: function () {
                var val = $(".div-projected-scale-value input").val();
                val = val.replace(",", "");
                val = val.replace(/\D/g, '');
                var projPriceFormat = val.toString();
                $(".div-projected-scale-value input").val(projPriceFormat);
            },

            /*Set Constant Values */
            fnSetConstantValues: function () {
                var that = this;
                this.requestType = "52130";
                this.requestStatus = "52186";
                this.requestSubmitStatus = "52124";
                //            this.defaultTypeId = "903107" //set Account as default Type
                this.customdiscount = "52020"; // set default price Type to Custom$
                this.offList = "52021"; //Price type % off list;
            },

            //function to set toggle switch
            toggle: function (e) {
                $(e.currentTarget).toggleClass('toggle-button-selected');
            },

            /*Based Account/Party id getting Account/Party Names click on GO button*/
            fnGetAccountName: function () {
                if ($("#div-rep-account-id input").val() != "") {
                    var accID = $("#div-rep-account-id input").val();
                    var repId = localStorage.getItem("userID");
                    var token = localStorage.getItem("token");
                    var acid = localStorage.getItem("acid");
                    var deptid = localStorage.getItem("deptid");
                    var userID = localStorage.getItem("userID");
                    var searchText = $("#div-rep-account-list input").val();
                    var cmpid = localStorage.getItem("cmpid")
                    var input = {
                        "token": "token",
                        "accid": accID,
                        "typeid": typeIdVal,
                        "repid": repId,
                        "acid": acid,
                        "deptid": deptid,
                        "userid": userID,
                        "searchtext": searchText,
                        "cmpid": cmpid
                    };
                    fnGetWebServerData("pricingRequest/accountDrpDownData/", "gmPriceAccountTypeVO", input, function (data) {
                        if (data != "" && data != null) {
                            $("#div-rep-account-list input").val(data[0].accname);
                        } else {
                            showError("Invalid Account/Party Id");
                            $("#div-rep-account-list input").val("");
                        }
                    });
                } else {
                    showError("Please enter Account/Party Id");
                }

            },

            /*Clear all the data once click on Cancel button*/
            cancelRequest: function () {
                var that = this;
                if ($("#div-rep-account-list input").val() == "") {
                    that.fnSetDefaultValues();
                } else {
                    if (GM.Global.Browser) {
                        var rmv = confirm("Do you want to cancel..?");
                        if (rmv == true) {
                            $("#btn-main-save").show();
                            $("#btn-main-submit").show();
                            $("#active-button").css("background-color", "#fff8e4").css("color", "black");
                            $(".div-price-impact-btn").css("background-color", "#fff8e4").css("color", "black");
                            $(".div-price-sales-analysis").css("background-color", "#fff8e4").css("color", "black");
                            that.fnSetDefaultValues();
                            if ($("#div-pricing-request input").val() != "" && $("#div-pricing-request input").val() != undefined) {
                                that.hideSubmittedReq();
                                $("#div-pricing-request input").val("");
                            }
                        }
                    } else {
                        showNativeConfirm("Do you want to cancel..?", "Confirm", ["Yes", "No"], function (idx) {
                            if (idx == 1) {
                                $("#btn-main-save").show();
                                $("#btn-main-submit").show();
                                $("#active-button").css("background-color", "#fff8e4").css("color", "black");
                                $(".div-price-impact-btn").css("background-color", "#fff8e4").css("color", "black");
                                $(".div-price-sales-analysis").css("background-color", "#fff8e4").css("color", "black");
                                that.fnSetDefaultValues();
                                if ($("#div-pricing-request input").val() != "" && $("#div-pricing-request input").val() != undefined) {
                                    that.hideSubmittedReq();
                                    $("#div-pricing-request input").val("");
                                }
                            }
                        });
                    }
                }
            },

            //hiding details from submitted request screen
            hideSubmittedReq: function () {
                $(".fa-information-circle").hide();
                $(".div-pricing-info-change").hide();
                $(".ul-price-save-btn").hide();
                $("#li-price-info-system-lable").hide();
                $(".div-price-submit-label1").hide();
                $("#btn-main-save").hide();
                $("#btn-main-submit").hide();
                $("#btn-main-cancel").hide();
                $(".fa-crosshairs").hide();
                $(".fa-usd").hide();
            },

            /*Accept only numbers*/
            validateNumber: function (event) {
                var numberPattern = /^\d*[0-9](|.\d*[0-9]|,\d*[0-9])?$/;
                if (!numberPattern.test($(event.currentTarget).val())) {
                    //                showError("Please Enter whole number");
                    $(event.currentTarget).val("");
                }
            },


            /*Get type drop down*/
            getTypeDrp: function () {
                var that = this;
                var defaultid;
                var input = {
                    "codeGrp": "ACCTYP",
                    "token": localStorage.getItem("token")
                };
                fnGetCodeLookUpValues(input, function (drpDwn) {
                    that.typedropdownview = new DropDownView({
                        el: $("#div-pricing-accnt"),
                        data: drpDwn,
                        DefaultID: drpDwn[0].ID
                    });
                    that.$("#div-pricing-accnt").bind('onchange', function (e, selectedId, SelectedName) {
                        that.fnDropdown(e, selectedId, SelectedName);
                    });
                });
            },

            fnDropdown: function (e, selectedId, SelectedName) {
                var that = this;
                if (selectedId == "903108") {
                    $("#div-pricing-gpb-message").removeClass("hide");
                    $("#fa-usd-id").hide();
                } else {
                    $("#div-pricing-gpb-message").addClass("hide");
                    $("#fa-usd-id").show();
                }

                if ($("#div-rep-account-list input").val() == "") {

                    typeIdVal = selectedId;
                    that.typeName = SelectedName;

                } else {
                    if (GM.Global.Browser) {
                        var rmv = confirm("Changing Type will create a new request. Do you want to save before proceeding ?");
                        if (rmv == true) {
                            var event = {};
                            event.currentTarget = $("#btn-main-save");
                            that.saveOrUpdatePricingDetails(event);
                            that.fnSetDefaultValues();
                            typeIdVal = selectedId;
                            that.typeName = SelectedName;
                        } else {
                            that.fnSetDefaultValues();
                            typeIdVal = selectedId;
                            that.typeName = SelectedName;
                        }
                    } else {
                        showNativeConfirm("Changing Type will create a new request. Do you want to save before proceeding ?", "Confirm", ["Yes", "No"], function (idx) {
                            if (idx == 1) {
                                var event = {};
                                event.currentTarget = $("#btn-main-save");
                                that.saveOrUpdatePricingDetails(event);
                                that.fnSetDefaultValues();
                                typeIdVal = selectedId;
                                that.typeName = SelectedName;
                            } else {
                                that.fnSetDefaultValues();
                                typeIdVal = selectedId;
                                that.typeName = SelectedName;
                            }
                        });
                    }
                }
            },

            fnSetDefaultValues: function () {
                var that = this;
                $("#div-rep-account-list input").val("");
                $("#div-rep-account-id input").val("");
                $(".div-price-widget-details").html("");
                $("#groupId").html("");
                $(".div-projected-scale-value input").val("");
                $("#div-price-request-id-name").html("");
                $(".li-price-change-heading").html("");
                $("#div-selected-system-list input").val("");
                $(".div-price-percentage-value-input input").val("");
                $("#div-rep-account-list #div-common-search-x").trigger("click");
                $("#div-selected-system-list #div-common-search-x").trigger("click");
                $("#div-rep-account-list #ul-search-resultlist").html("");
                $("#div-selected-system-list #ul-search-resultlist").html("");
                that.gmcGroup = new GMCGroup();
                that.gmcSystem = new GMCSystem();
                that.gmcBusinessQuestions = new GMCBusinessQuestions();
                this.gmmAccountPriceRequest = new GMMAccountPriceRequest();
                that.gmcFileUpload = new GMCFileUpload();
                $(".div-price-group-dt-input input").val("");
            },


            /*For Price Discount Type DropDown*/
            discountTypeDrp: function () {
                var that = this;
                var defaultid;
                var input = {
                    "codeGrp": "PCHTY",
                    "token": localStorage.getItem("token")
                };

                fnGetCodeLookUpValues(input, function (drpDwn) {
                    that.discountdropdownview = new DropDownView({
                        el: $(".div-price-cls-input"),
                        data: drpDwn,
                        DefaultID: drpDwn[0].ID
                    });
                    $(".div-price-cls-input").bind('onchange', function (e, selectedId, SelectedName) {

                        if (that.disctypeSys == "")
                            var sys = $(".ul-product-background-col .li-productName").attr("setname");
                        else {
                            sys = that.disctypeSys;
                            that.disctypeSys = "";
                        }

                        that.disctypeId = selectedId;
                        that.disctypeName = SelectedName;
                        if (!that.arn) {
                            that.pricePercentDetails();
                            that.arn = false;
                        }
                        $(".ul-product-background-col").attr("pricetype", that.disctypeId);
                        $(".div-price-percentage-value-input input").val("");
                        var existSys = _.filter(pricetypedisc, function (a) {
                            return a.sys == sys;
                        })
                        if (existSys.length == 0)
                            pricetypedisc.push({
                                sys: sys,
                                disc: "52020"
                            })
                        else {
                            _.each(pricetypedisc, function (s) {
                                if (s.sys == sys) {
                                    s.disc = selectedId.toString();
                                }

                            });
                        }
                        if (that.disctypeId != "52020") {
                            $('.div-price-percentage-value-input input').prop('disabled', false);
                            $('.div-price-group-dt-input input').prop('disabled', false);
                        } else {
                            $('.div-price-percentage-value-input input').prop('disabled', true);
                            $('.div-price-group-dt-input input').prop('disabled', true);
                        }
                    });
                });
            },


            /*For save percentage details*/
            pricePercentDetails: function () {
                var systemId = $(".ul-product-background-col .li-productName").attr("productname");
                var priceChangeVal = $(".ul-product-background-col .li-productName").attr("changeValue");
                if (systemId != undefined && priceChangeVal != "") {
                    if (priceChangeVal == undefined)
                        priceChangeVal = "";
                    this.gmcSystem.findWhere({
                        "systemId": systemId
                    }).set("changetype", this.disctypeId).set("changevalue", priceChangeVal);
                }
            },



            //method to calculate priceType = %off list
            calcListOffPrice: function () {
                var that = this;
                var pval = $(".div-price-percentage-value-input input").val();
                if (pval != "" && pval < 100) {
                    var groupList = _.filter(that.gmcGroup.toJSON(), function (model) {
                        return (model.systemid == that.systemId)
                    });
                    var sysId = that.systemId;
                    _.each(groupList, function (data) {
                        if ((data.changetype != "52020" || data.proposedunitprice == "") && (data.statusid == "" || data.statusid != "52125")) { //ignore changeType = Custom$
                            var groupId = data.groupid;
                            var constructLevel = data.constructlevel;
                            var constructId = data.constructid;
                            var partnum = data.partnum;
                            var discount = (data.listprice * pval) / 100;
                            var propunitprice = Math.round((data.listprice - discount), 2);
                            var propextprice = data.quantity * propunitprice;
                            that.gmcGroup.findWhere({
                                    "systemid": that.systemId,
                                    "groupid": groupId,
                                    "constructlevel": constructLevel,
                                    "constructid": constructId,
                                    "partnum": partnum
                                })
                                .set("proposedunitprice", propunitprice.toString()).set("proposedextensionprice", propextprice.toString())
                                .set("changetype", that.offList).set("changevalue", pval.toString());

                        }
                    });
                    this.ToggleGroupsDisplay($("#li-price-check-load-all-groups").find(".toggle-button").hasClass("toggle-button-selected"));

                    _.each($(".ul-price-system-details"), function (t) {
                        var grpid = $(t).attr("groupid");
                        var reqdtlid = $(t).attr("reqdtlid");
                        _.each(groupList, function (status) {
                            if (status.statusid == "52125" && grpid == status.groupid && reqdtlid == status.reqdtlid) {
                                $(t).find("#li-price-system-Pruposed-unit input").prop('disabled', 'true').css("background-color", "#c4e8c4");
                            }
                        });
                    });
                }

                if (pval != "" && pval >= 100) {
                    showError("Please Enter %Off List Value less than 100");
                    $(".div-price-percentage-value-input input").val("");
                }
                _.each($(".li-productName"), function (data) {
                    if ($(data).attr("productname") == that.systemId)
                        $(data).attr("changeValue", pval)
                });
                /*Calculate %off list for Individual group list again with the input value available in the group level*/
                var event;
                _.each($(".div-price-group-dt-input input"), function (data) {
                    if (($(data).attr("grpid") != "" && $(data).attr("grpid") != null) && ($(data).val() != "" && $(data).val() != null)){
                            that.calcListOffPriceforIndividualGrp(event,$(data).attr("grpid"),$(data).val());
                    }
                 });
                    console.log("Extracted groupList : " + groupList);
                
              

            },


            //Method to calculate priceType  %off list for individual Groups
            calcListOffPriceforIndividualGrp: function (event,grpid,val) {
                var that = this;
                var pval = "";
                var eventGroupId = "";
                if (typeof(event) != "undefined" ){
                    if(typeof(event.currentTarget) != undefined)
                         pval = $(event.currentTarget).val();

                    if(typeof(event.currentTarget) != undefined)
                        eventGroupId = $(event.currentTarget).attr("grpid"); 
                }
                
                if(pval == undefined || pval == null || pval == ""){
                    pval = val;
                }
                if(eventGroupId == undefined || eventGroupId == null || eventGroupId == ""){
                    eventGroupId = grpid;
                }
                if (pval != "" && pval < 100) {
                    var groupList = _.filter(that.gmcGroup.toJSON(), function (model) {
                        return (model.systemid == that.systemId && model.groupid == eventGroupId )
                    });
                    var sysId = that.systemId;
                    _.each(groupList, function (data) {
                        if ((data.changetype != "52020" || data.proposedunitprice == "") && (data.statusid == "" || data.statusid != "52125")) { //ignore changeType = Custom$
                            var groupId = eventGroupId;
                            var constructLevel = data.constructlevel;
                            var constructId = data.constructid;
                            var partnum = data.partnum;
                            var discount = (data.listprice * pval) / 100;
                            var propunitprice = Math.round((data.listprice - discount), 2);
                            var propextprice = data.quantity * propunitprice;
                            console.log(" pval.toString()=="+ pval.toString());
                            that.gmcGroup.findWhere({
                                    "systemid": that.systemId,
                                    "groupid": groupId,
                                    "constructlevel": constructLevel,
                                    "constructid": constructId,
                                    "partnum": partnum
                                })
                                .set("proposedunitprice", propunitprice.toString()).set("proposedextensionprice", propextprice.toString())
                                .set("changetype", that.offList).set("individualgrpchangevalue", pval.toString());
                            console.log("   that.gmcGroup =="+  JSON.stringify(that.gmcGroup));

                        }
                    });
                    this.ToggleGroupsDisplay($("#li-price-check-load-all-groups").find(".toggle-button").hasClass("toggle-button-selected"));

                    _.each($(".ul-price-system-details"), function (t) {
                        var grpid = $(t).attr("groupid");
                        var reqdtlid = $(t).attr("reqdtlid");
                        _.each(groupList, function (status) {
                            if (status.statusid == "52125" && grpid == status.groupid && reqdtlid == status.reqdtlid) {
                                $(t).find("#li-price-system-Pruposed-unit input").prop('disabled', 'true').css("background-color", "#c4e8c4");
                            }
                        });
                    });
                }

                if (pval != "" && pval >= 100) {
                    showError("Please Enter %Off List Value less than 100");
                        $(".div-price-group-dt-input input").val("");
                }

            },
            
            /*
            //  method to calculate CustomPrice
            //  Requirements : http://confluence.spineit.net/display/PRJ/PRT_R104_Auto-populate+Proposed+PricePRT-314 
            //  Design       : http://confluence.spineit.net/display/PRJ/PRT_D104_Auto+Populate+Proposed+Price+UI+Layer
            */
            calcCustomPrice: function (e) {
                var that = this;

                //execute method only if pricetype = custom
                if (this.disctypeId != that.customdiscount) {
                    return;
                }

                if (!$("#li-price-check-auto-populate").find(".toggle-button").hasClass("toggle-button-selected")) {
                    $("#li-price-check-auto-populate").find(".toggle-button").addClass('toggle-button-selected');
                    //check that the principal flag has been updated
                    var validData = _.filter(this.gmcGroup.toJSON(), function (model) {
                        return (model.systemid == that.systemId && model.principalflag == "Y" && model.proposedunitprice == "")
                    });
                    if (validData.length > 0) {
                        showError("Please enter proposed price for all items under principle construct");
                        $("#li-price-check-auto-populate").find(".toggle-button").removeClass('toggle-button-selected');
                        return;
                    }
                    this.loaderview = new LoaderView({
                        text: "Calculating AutoPopulate Price."
                    });

                    //retrieve the pricpal flag data
                    var principleData = _.filter(this.gmcGroup.toJSON(), function (model) {
                        return (model.systemid == that.systemId && model.principalflag == "Y")
                    });

                    //get the proposed unit price total
                    var totListExtPrice = 0;
                    var totPropExtPrice = 0;
                    _.each(principleData, function (data) {
                        totPropExtPrice += (parseInt(data.proposedunitprice) * parseInt(data.quantity));
                        totListExtPrice += (parseInt(data.listprice) * parseInt(data.quantity));

                    })

                    var grpTypes = _.chain(that.gmcGroup.toJSON()).map(function (model) {
                        return model.grouptype
                    }).uniq().value();

                    _.each(grpTypes, function (grpTyp) {

                        var grpData = _.filter(that.gmcGroup.toJSON(), function (model) {
                            return (model.systemid == that.systemId && model.principalflag == "" && model.grouptype == grpTyp)
                        });

                        if (grpData.length > 0) {
                            var prRec = _.filter(principleData, function (model) {
                                return (model.grouptype == grpTyp);
                            });
                            //check if grouptype is part of principle construct and calculate discount%    
                            if (prRec.length > 0) {
                                var prpropprice = parseInt(prRec[0].proposedunitprice);
                                var prListPrice = parseInt(prRec[0].listprice);
                                var pval = parseFloat(((prListPrice - prpropprice) / prListPrice) * 100).toFixed(2);
                            } else { //if group type not part of principal construct
                                var pval = parseFloat(((totListExtPrice - totPropExtPrice) / totListExtPrice) * 100).toFixed(2);
                            };
                            if (pval == "NaN")
                                pval = "";
                            _.each(grpData, function (gdata) {
                                //only update data where proposed price is not blank
                                if (gdata.proposedunitprice == "") {
                                    var groupId = gdata.groupid;
                                    var constructLevel = gdata.constructlevel;
                                    var constructId = gdata.constructid;
                                    var partnum = gdata.partnum;

                                    var discount = (gdata.listprice * pval) / 100;
                                    var propunitprice = Math.round((gdata.listprice - discount), 0);

                                    var propextprice = gdata.quantity * propunitprice;

                                    that.gmcGroup.findWhere({
                                            "systemid": that.systemId,
                                            "groupid": groupId,
                                            "constructlevel": constructLevel,
                                            "constructid": constructId,
                                            "partnum": partnum
                                        })
                                        .set("proposedunitprice", propunitprice.toString()).set("proposedextensionprice", propextprice.toString())
                                        .set("changetype", that.customdiscount).set("changevalue", pval.toString()).set("autopop_fl", "Y");
                                }
                            });
                        }
                    });
                    _.each($(".li-productName"), function (data) {
                        if ($(data).attr("productname") == that.systemId)
                            $(data).attr("autofl", "true")
                    });
                    setTimeout(function () {
                        that.loaderview.close();
                    }, 500);
                }

                //if the autopopulate toggle is being turned off
                else if ($("#li-price-check-auto-populate").find(".toggle-button").hasClass("toggle-button-selected")) {
                    if (GM.Global.Browser) {
                        var rmv = confirm("Do you want undo auto populate ?");
                        if (rmv == true) {
                            $("#li-price-check-auto-populate").find(".toggle-button").removeClass('toggle-button-selected');
                            var aPopulateData = _.filter(that.gmcGroup.toJSON(), function (model) {
                                return (model.systemid == that.systemId && model.autopop_fl == "Y")
                            });
                            _.each(aPopulateData, function (gdata) {

                                var groupId = gdata.groupid;
                                var constructlevel = gdata.constructlevel;
                                var constructId = gdata.constructid;
                                var partnum = gdata.partnum;
                                that.gmcGroup.findWhere({
                                        "systemid": that.systemId,
                                        "groupid": groupId,
                                        "constructlevel": constructlevel,
                                        "constructid": constructId,
                                        "partnum": partnum
                                    })
                                    .set("proposedunitprice", "").set("proposedextensionprice", "")
                                    .set("changetype", "").set("changevalue", "").set("autopop_fl", "").set("checkfl", "");

                            });
                            _.each($(".li-productName"), function (data) {
                                if ($(data).attr("productname") == that.systemId)
                                    $(data).attr("autofl", "fslse")
                            });
                            $(".ul-product-background-col").find(".li-productName").trigger("click");
                        } else {
                            return;
                        }
                    } else {
                        showNativeConfirm("Do you want undo auto populate ?", "Confirm", ["Yes", "No"], function (idx) {
                            if (idx == 1) {
                                //                    var rmv = confirm("Do you want undo auto populate ?");
                                //                    if (rmv == true) {
                                $("#li-price-check-auto-populate").find(".toggle-button").removeClass('toggle-button-selected');
                                var aPopulateData = _.filter(that.gmcGroup.toJSON(), function (model) {
                                    return (model.systemid == that.systemId && model.autopop_fl == "Y")
                                });
                                _.each(aPopulateData, function (gdata) {

                                    var groupId = gdata.groupid;
                                    var constructlevel = gdata.constructlevel;
                                    var constructId = gdata.constructid;
                                    var partnum = gdata.partnum;
                                    that.gmcGroup.findWhere({
                                            "systemid": that.systemId,
                                            "groupid": groupId,
                                            "constructlevel": constructlevel,
                                            "constructid": constructId,
                                            "partnum": partnum
                                        })
                                        .set("proposedunitprice", "").set("proposedextensionprice", "")
                                        .set("changetype", "").set("changevalue", "").set("autopop_fl", "").set("checkfl", "");

                                });
                                _.each($(".li-productName"), function (data) {
                                    if ($(data).attr("productname") == that.systemId)
                                        $(data).attr("autofl", "fslse")
                                });
                                $(".ul-product-background-col").find(".li-productName").trigger("click");
                            } else {
                                return;
                            }
                        });
                    }
                    _.each($(".li-productName"), function (data) {
                        if ($(data).attr("productname") == that.systemId)
                            $(data).attr("autofl", "fslse")
                    });
                };
                this.ToggleGroupsDisplay($("#li-price-check-load-all-groups").find(".toggle-button").hasClass("toggle-button-selected"));

            },

            /*Search Account name from search box*/
            getAccount: function (rowValue) {
                var that = this;
                $("#div-itemlist").hide();
                $("#txt-billing-account").val(rowValue.Name);
                this.accid = rowValue.ID;
                $("#div-price-value-input").val(rowValue.ID);
                accountIdVal = rowValue.ID;
                that.accid = true;
                $(".div-price-widget-details").html("");
                $("#groupId").html("");
                $(".div-projected-scale-value input").val("");
                that.gmcGroup = new GMCGroup();
                this.gmmAccountPriceRequest = new GMMAccountPriceRequest();
            },

            /*Calculating PruposedExtn price and  Total ProposedExtn price based on entered proposed unit price*/
            getProposedExtnPrice: function (e) {
                var systemId = $(e.currentTarget).attr("systemid");
                var groupId = $(e.currentTarget).attr("groupid");
                var constructLevel = $(e.currentTarget).attr("constructlevel");
                var proposedUnitPrice = $(e.currentTarget).val();
                if ($(e.currentTarget).val() != "" && $(e.currentTarget).parent().parent().find("#li-price-system-changetype").find("input").val() == "") {
                    $(e.currentTarget).parent().parent().find("#li-price-system-changetype").find("input").val(this.disctypeId);
                    $(e.currentTarget).parent().parent().find("#li-price-system-changevalue").find("input").val("");
                }
                var partnum = $(e.currentTarget).attr("partnum");
                var constructid = $(e.currentTarget).attr("constructid");
                if (this.gmcGroup != null && this.gmcGroup.length > 0) {
                    var group = this.gmcGroup.findWhere({
                        "systemid": systemId,
                        "groupid": groupId,
                        "constructlevel": constructLevel,
                        "partnum": partnum,
                        "constructid": constructid
                    });
                    var quantity = group.get("quantity");
                    var propExtPrice = 0;
                    if (quantity != undefined && quantity != '' && quantity != "" && !isNaN(quantity) &&
                        proposedUnitPrice != undefined && proposedUnitPrice != '' && proposedUnitPrice != "" && !isNaN(proposedUnitPrice)) {
                        propExtPrice = parseInt(quantity) * parseInt(proposedUnitPrice);
                        propExtPrice = propExtPrice.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                    }
                    if (propExtPrice == 0) {
                        propExtPrice = "";
                    }
                    if ($(e.currentTarget).val().length <= 6) {
                        $(e.currentTarget).parent().next().html(propExtPrice);
                        group.set("proposedunitprice", proposedUnitPrice);
                        group.set("proposedextensionprice", propExtPrice);
                        group.set("changetype", $(e.currentTarget).parent().parent().find("#li-price-system-changetype").find("input").val());
                        group.set("changevalue", $(e.currentTarget).parent().parent().find("#li-price-system-changevalue").find("input").val());

                        /* Section that displays Check flags */
                        var tripwire = group.get("tripwireunitprice");
                        if (parseInt(proposedUnitPrice) != "" && parseInt(proposedUnitPrice) < parseInt(tripwire)) {
                            $(e.currentTarget).parent().parent().find("#li-price-system-flag").html("N").removeClass().addClass("colorN").addClass("li-price-group-details");
                            group.set("checkfl", "N");
                            $("#active-button").css("background-color", "rgba(255, 2, 2, 0.42)").css("color", "black");
                        } else if (parseInt(proposedUnitPrice) != "" && parseInt(proposedUnitPrice) >= parseInt(tripwire)) {
                            $(e.currentTarget).parent().parent().find("#li-price-system-flag").html("Y").removeClass().addClass("colorY").addClass("li-price-group-details");
                            group.set("checkfl", "Y");
                            $("#active-button").css("background-color", "#fff8e4").css("color", "black");
                        } else if (proposedUnitPrice == "") {
                            $(e.currentTarget).parent().parent().find("#li-price-system-flag").html("");
                            $("#active-button").css("background-color", "#fff8e4").css("color", "black");
                        }
                        _.each($(".ul-price-system-details"), function (data) {
                            if ($(data).find("#li-price-system-flag").html() == "N")
                                $("#active-button").css("background-color", "rgba(255, 2, 2, 0.42)").css("color", "black");
                        });
                        if (this.question == true) {
                            $("#active-button").css("background-color", "#b2ffb2").css("color", "black");
                        }
                        var groupList = this.gmcGroup.where({
                            "systemid": systemId,
                            "constructlevel": constructLevel,
                            "constructid": constructid
                        });
                        var totalPropExtPrice = 0;
                        _.each(groupList, function (group) {
                            var proposedExtensionPrice = group.get("proposedextensionprice").replace(',', '');
                            if (proposedExtensionPrice != undefined && proposedExtensionPrice != '' && proposedExtensionPrice != "" && !isNaN(proposedExtensionPrice)) {
                                totalPropExtPrice = parseInt(totalPropExtPrice) + parseInt(proposedExtensionPrice);
                            }
                        })

                        if (totalPropExtPrice == 0) {
                            totalPropExtPrice = "";
                        }
                        totalPropExtPrice = totalPropExtPrice.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                        var check = $(e.currentTarget).parent().parent().parent().next().find(".li-price-level-construct").html();
                        if (check != "Individual Groups" && check != "Groups")
                            $(e.currentTarget).parent().parent().parent().next().find(".li-price-pruposed-ext-price-total").html(totalPropExtPrice);

                        if ($(e.currentTarget).val() == "") {
                            $(e.currentTarget).parent().parent().find("#li-price-system-flag").removeClass().addClass("li-price-group-details").html("");
                            group.set("checkfl", "");
                        }
                        var Nflag;
                        var Yflag;
                        _.each($(e.currentTarget).parent().parent().parent().find(".ul-price-system-details"), function (data) {
                            if ($(data).find("#li-price-system-flag").html() == "Y")
                                Yflag = true;
                            if ($(data).find("#li-price-system-flag").html() == "N")
                                Nflag = true;
                        });
                        var checkFl = $(e.currentTarget).parent().parent().parent().next().find(".li-price-level-construct").html();
                        if (Yflag == true && Nflag != true && check != "Individual Groups" && check != "Groups")
                            $(e.currentTarget).parent().parent().parent().next().find(".li-price-approved-name").html("Y").removeClass().addClass("colorY").addClass("li-price-approved-name");
                        else if (Nflag == true && check != "Individual Groups" && check != "Groups")
                            $(e.currentTarget).parent().parent().parent().next().find(".li-price-approved-name").html("N").removeClass().addClass("colorN").addClass("li-price-approved-name");
                        else if (Yflag == undefined && Nflag == undefined && check != "Individual Groups" && check != "Groups")
                            $(e.currentTarget).parent().parent().parent().next().find(".li-price-approved-name").html("").removeClass().addClass("li-price-approved-name");
                    } else {
                        showError("Proposed Unit price field allows only 6 digits");
                        $(e.currentTarget).val("");
                        $(e.currentTarget).parent().next().html("");
                        $(e.currentTarget).parent().parent().find("#li-price-system-flag").removeClass().addClass("li-price-group-details").html("");
                        $(e.currentTarget).trigger("change");
                    }
                }
            },

            /*Get Consultant dropdown values*/
            getConsultantDrp: function (data) {
                var that = this;
                var defaultid = 0;
                var defaultname = "Choose One";
                var input = {
                    "codeGrp": "YES/NO",
                    "token": localStorage.getItem("token")
                };
                fnGetCodeLookUpValues(input, function (drpDwn) {
                    drpDwn.push({
                        "ID": defaultid,
                        "Name": defaultname
                    });
                    if (data[0].dropdownid != "" && data[0].dropdownvalue != "") {
                        defaultid = data[0].dropdownid;
                        defaultname = data[0].dropdownvalue;
                    }
                    that.consultantdropdownview = new DropDownView({
                        el: $("#div-price-overlay-content-container5 .modal-body #li-pricing-dropdown"),
                        data: drpDwn,
                        DefaultID: defaultid
                    });
                    $("#div-price-overlay-content-container5 .modal-body #li-pricing-dropdown").bind('onchange', function (e, selectedId, SelectedName) {
                        that.consultantId = selectedId;
                        that.consultantName = SelectedName;
                    });
                });
            },

            /*Validation for Business Questions*/
            validate: function () {
                var rs = true;
                var that = this;
                $(".pricing-answers").each(function () {
                    if (parseInt(this.id) == 18 && (that.consultantId == 0 || that.consultantId == undefined)) {
                        showError("Please select at least one value from <B>consultant</B> dropdown");
                        rs = false;
                        return false;
                    }
                    if ((this.value == "" && parseInt(this.id) != 23) && (parseInt(this.id) != 18 || that.consultantId == "80130")) {
                        showError("Answers should be mandatory for all the Questions");
                        rs = false;
                        return false;
                    }
                    if (this.value.length > 4000) {
                        showError("Length of answers should not exceed more than 4000 characters");
                        rs = false;
                        return false;
                    }
                });
                return rs;
            },

            /*Once Click on Business Questions*/
            showThresholdQuestions: function (e) {
                if (accountIdVal != "") {
                    this.accid = true;
                    this.systemID = true;
                }
                var that = this;
                _.each(that.gmcGroup.toJSON(), function (data) {
                    if (data.checkfl == "N") {
                        that.flag = true;
                    }
                });
                if (that.flag == true) {
                    that.flag = false;
                    var token = localStorage.getItem("token");
                    var requestid;
                    if (this.priceDataID != "")
                        requestid = that.priceDataID;
                    else
                        requestid = "";

                    if (requestid == "") {
                        requestid = $("#div-pricing-request input ").val();
                    }
                    var input = {
                        "token": "token",
                        "requestid": requestid
                    };
                    this.loaderview = new LoaderView({
                        text: "Loading Business Questions ."
                    });
                    fnGetWebServerData("pricingRequest/thresholdQuestions/", undefined, input, function (data) {
                        showInfoPopup();
                        that.loaderview.close();
                        $("#msg-overlay").show();
                        if (that.question == true && that.qusnfl == true) {
                            if (that.Questions[0].questionanswer != "" && that.Questions != undefined) {
                                /*Consultant drpdwn*/
                                $("#div-price-overlay-content-container5 .modal-body").html(that.templatePriceThresholdQusetions(that.Questions));
                                that.getConsultantDrp(that.Questions);
                            }
                        } else {
                            $("#div-price-overlay-content-container5 .modal-body").html(that.templatePriceThresholdQusetions(data.gmThresholdQuestionVO));
                            /*Consultant drpdwn*/
                            that.getConsultantDrp(data.gmThresholdQuestionVO);
                            _.each(data.gmThresholdQuestionVO, function (r) {
                                if (r.dropdownid != "")
                                    that.consultantId = r.dropdownid;
                            })
                        }
                        $("#div-price-overlay-content-container5").find('.modal-body #div-threshold-submit').unbind('click').bind('click', function () {
                            if (that.validate()) {
                                $("#active-button").css("background-color", "#b2ffb2").css("color", "black");
                                _.each(data.gmThresholdQuestionVO,
                                    function (data) {
                                        if (data.questiontype != "26240003") { //Dropdown question type
                                            var questionInput = {
                                                "gmThresholdQuestionVO": [{
                                                    "questionid": data.questionid,
                                                    "questionanswer": $("#" + data.questionid).val(),
                                                    "userid": localStorage.getItem("userID")
                                    }]
                                            };
                                            data.questionanswer = $("#" + data.questionid).val();
                                            data.userid = localStorage.getItem("userID");
                                        } else {
                                            var questionInput = {
                                                "gmThresholdQuestionVO": [{
                                                    "questionid": data.questionid,
                                                    "questionanswer": $("#" + data.questionid).val(),
                                                    "userid": localStorage.getItem("userID"),
                                                    "dropdownid": that.consultantId,
                                                    "dropdownvalue": that.consultantName
                                    }]
                                            };
                                            data.questionanswer = $("#" + data.questionid).val();
                                            data.userid = localStorage.getItem("userID");
                                            data.dropdownid = that.consultantId;
                                            data.dropdownvalue = that.consultantName;
                                        }
                                    });
                                that.gmcBusinessQuestions.set(data.gmThresholdQuestionVO);
                                that.Questions = data.gmThresholdQuestionVO;
                                that.question = true;
                                that.qusnfl = true;
                                hideInfoPopup();
                                $("#msg-overlay").hide();
                            }
                        });
                        $("#div-price-overlay-content-container5").find('.modal-body .li-threshold-cancel-button').unbind('click').bind('click', function () {
                            hideInfoPopup();
                            $("#msg-overlay").hide();
                        });
                        $("#div-price-overlay-content-container5").find('.modal-body #fa-close-alert').unbind('click').bind('click', function () {
                            hideMessages();
                        });

                    });
                }
            },

            /*Account Info PopUp (i icon)*/
            showPriceAccountDetails: function () {
                var that = this;
                var accID = $("#div-rep-account-id input").val();
                if (accID == undefined)
                    accID = accountIdVal;
                var repId = localStorage.getItem("userID");
                var token = localStorage.getItem("token");
                var acid = localStorage.getItem("acid");
                var deptid = localStorage.getItem("deptid");
                var userID = localStorage.getItem("userID");
                var partyID = localStorage.getItem("partyID");
                if (accID != "") {
                    var input1 = {
                        "token": "token",
                        "accid": accID,
                        "typeid": typeIdVal,
                        "repid": repId,
                        "acid": acid,
                        "deptid": deptid,
                        "userid": userID,
                        "partyid": partyID
                    };
                    var accinput = $('#div-rep-account-id input').val();
                    var subreqAccid = accountIdVal;
                    if (accinput == "" || accinput == undefined) {
                        accinput = subreqAccid;
                    }
                    var input2 = {
                        "token": token,
                        "accid": accinput,
                        "typeid": typeIdVal,
                        "partyid": partyID
                    };
                    showPriceAccountDetails(input1, input2, LoaderView);
                    //this functionality has been moved to gmvPricingCommon.js
                }
            },

            /*Click on P icon popUp*/
            showGroupPartPricingDetails: function (e) {
                showGroupPartPricingDetails(e);
            },

            /*Iterating to calculate total for each construct level*/
            setconstructLevelValues: function (groupList, constructLevel) {
                var totalPropExtPrice = 0;
                var totalTripWireExtensionPrice = 0;
                _.each(groupList, function (group) {
                    var proposedExtensionPrice = group.proposedextensionprice;
                    if (proposedExtensionPrice != undefined && proposedExtensionPrice != '' && proposedExtensionPrice != "" && !isNaN(proposedExtensionPrice)) {
                        totalPropExtPrice = parseInt(totalPropExtPrice) + parseInt(proposedExtensionPrice);
                    }

                    var tripWireExtensionPrice = group.tripwireextensionprice;
                    if (tripWireExtensionPrice != undefined && tripWireExtensionPrice != '' && tripWireExtensionPrice != "" && !isNaN(tripWireExtensionPrice)) {
                        totalTripWireExtensionPrice = parseInt(totalTripWireExtensionPrice) + parseInt(tripWireExtensionPrice);
                    }
                });
                if (totalPropExtPrice == 0) {
                    totalPropExtPrice = "";
                }
                if (totalTripWireExtensionPrice == 0) {
                    totalTripWireExtensionPrice = "";
                }
                var constructObj = new Object();
                constructObj.constructLevel = constructLevel;
                constructObj.totalPropExtPrice = totalPropExtPrice;
                constructObj.totalTripWireExtensionPrice = totalTripWireExtensionPrice.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                var inputJSNData = $.parseJSON(JSON.stringify(constructObj));
                this.$("#groupId").append(this.templateGmGroupConstruct(inputJSNData));
            },

            //getting system details while changing systems after selected
            getSystemGroupsDataOnClickOfProductName: function (e) {
                var that = this;
                $("#li-price-check-load-all-groups .toggle-button").addClass("val-selected toggle-button-selected");
                if ($("#li-price-check-load-all-groups .toggle-button").hasClass("toggle-button-selected"))
                    //                    $("#li-price-check-load-all-groups .toggle-button").trigger("click");
                    var autoflag = $(e.currentTarget).attr("autofl");
                if (autoflag == "true") {
                    $("#li-price-check-auto-populate .toggle-button").addClass("toggle-button-selected");
                }
                this.$(".ul-add-sys-values").removeClass('ul-product-background-col');
                $(e.currentTarget).parent().addClass('ul-product-background-col');
                this.$("#groupId").html("");
                var systemId = $(e.currentTarget).attr("productName");
                var setName = $(e.currentTarget).attr("setName");
                $(".li-price-change-heading").html(setName);
                that.systemId = systemId;
                var groupsData = this.gmcGroup;
                groupsData = groupsData.toJSON();
                GM.Global.allData = _.filter(groupsData, function (model) {
                    return (model.systemid == systemId)
                })
                var groupsData = _.filter(groupsData, function (model) {
                    return (model.systemid == systemId && (model.principalflag == "Y" || model.specialitygroup == "Y"))
                });
                this.showGroupData(groupsData); //showGroupData() to handle constructs levels based on data
                this.ToggleGroupsDisplay(true)
                _.each($(".ul-price-system-details"), function (data) {
                    $(data).find("#li-price-system-Pruposed-unit input").trigger("change");
                });
                _.each(pricetypedisc, function (s) {
                    if (s.sys == setName) {
                        $(".div-price-cls-input li").each(function () {
                            if ($(this).attr("id") == s.disc) {
                                that.arn = true;
                                $(this).trigger("click");
                            }
                        });
                    }
                });
                var systemData = this.gmcSystem.toJSON();
                _.each(systemData, function (data) {
                    if (data.systemId == $(".ul-product-background-col").find(".li-productName").attr("productname"))
                        $(".div-price-percentage-value-input input").val(data.changevalue)
                });
                if (this.priceRequestIdVal != undefined && this.priceRequestIdVal != "") {
                    var systemData = this.gmcSystem.toJSON();
                    _.each(systemData, function (data) {
                        if (that.systemId == data.systemId) {
                            $(".div-price-cls-input .div-dropdown-list li").each(function () {
                                if ($(this).find('a').html().trim() == data.changetypename) {
                                    that.arn = true;
                                    $(this).trigger('click');

                                }

                            })
                            that.$(".div-price-percentage-value-input input").val(data.changevalue)

                        }
                    });
                }
                if ($(".div-price-cls-input .div-dropdown-text").html() == "Custom") {
                    $('.div-price-percentage-value-input input').val("");
                }
                _.each($(".ul-price-system-details"), function (t) {
                    var grpid = $(t).attr("groupid");
                    var reqdtlid = $(t).attr("reqdtlid");
                    _.each(groupsData, function (status) {
                        if (status.statusid == "52125" && grpid == status.groupid && reqdtlid == status.reqdtlid) {
                            $(t).find("#li-price-system-Pruposed-unit input").prop('disabled', 'true').css("background-color", "#c4e8c4");
                        }
                    });
                });
                that.checkBusinessQns();
                that.fnShowPriceIndicatoricon(e);
                //After loading the template disable the secondary text box for Trauma Systems
                if ($(".div-price-cls-input .div-dropdown-text").html() == "Custom") {
                    $('.div-price-group-dt-input input').prop('disabled', true);
                }
            },

            /*Method to delete a delete a systems (minus Icon)*/
            removeSystemOnclickOfMinusCircle: function (e) {
                var that = this;
                console.log(e);
                if (GM.Global.Browser) {
                    var rmv = confirm("Do you want to remove System..?");
                    if (rmv == true) {
                        var systemId = $(e.currentTarget).attr("productName");
                        GM.Global.SavedInfo = [];
                        GM.Global.SavedInfo.push("systemid", systemId);
                        if (that.gmcGroup != null && that.gmcGroup.length > 0) {
                            that.gmcGroup.remove(that.gmcGroup.where({
                                "systemid": systemId
                            }));
                        }
                        if (that.gmcSystem != null && that.gmcSystem.length > 0) {
                            that.gmcSystem.remove(that.gmcSystem.where({
                                "systemId": systemId
                            }));
                        }
                        if (that.selectedSystem != null && that.selectedSystem.length > 0) {
                            console.log("selectedSystem");
                            _.reject(that.selectedSystem, function (dt) {
                                return dt.systemId == systemId;
                            });
                            //                            that.selectedSystem.remove(that.selectedSystem.where({
                            //                                "systemId": systemId
                            //                            }));
                        }
                        that.$("#groupId").html("");
                        if ($(e.currentTarget).parent().parent().next().length != 0)
                            $(e.currentTarget).parent().parent().next().find(".li-productName").trigger("click");
                        else
                            $(".ul-add-sys-values").first().find(".li-productName").trigger("click");
                        $(e.currentTarget).parent().parent().remove();
                        if ($(".li-productName").length < 1)
                            $(".li-price-change-heading").html("");
                    }
                } else {
                    showNativeConfirm("Do you want to remove System..?", "Confirm", ["Yes", "No"], function (idx) {
                        if (idx == 1) {
                            var systemId = $(e.currentTarget).attr("productName");
                            GM.Global.SavedInfo = [];
                            GM.Global.SavedInfo.push("systemid", systemId);
                            if (that.gmcGroup != null && that.gmcGroup.length > 0) {
                                that.gmcGroup.remove(that.gmcGroup.where({
                                    "systemid": systemId
                                }));
                            }
                            if (that.gmcSystem != null && that.gmcSystem.length > 0) {
                                that.gmcSystem.remove(that.gmcSystem.where({
                                    "systemId": systemId
                                }));
                            }
                            that.$("#groupId").html("");
                            if ($(e.currentTarget).parent().parent().next().length != 0)
                                $(e.currentTarget).parent().parent().next().find(".li-productName").trigger("click");
                            else
                                $(".ul-add-sys-values").first().find(".li-productName").trigger("click");
                            $(e.currentTarget).parent().parent().remove();

                            if ($(".li-productName").length < 1)
                                $(".li-price-change-heading").html("");
                        }
                    });
                }
            },

            /*Construct level group price details*/
            showGroupData: function (groupData) {
                var that = this;
                var cnt = 0;
                var constantname = "Construct";
                var constructs = GM.Global.Constructs;
                var tCnt = constructs.length;
                $("#groupId").html("");
                if (groupData != null && groupData.length > 0)
                    _.each(constructs, function (data) {
                        cnt = cnt + 1;
                        var groupList = _.filter(groupData, function (model) {
                            return (model.systemid == that.systemId && model.constructlevel == data.codeid)
                        });

                        var grpConstructId = _.chain(groupList).map(function (model) {
                            return model.constructid
                        }).uniq().value();

                        _.each(grpConstructId, function (consid) {
                            if (consid != null) {
                                var cdata = _.filter(groupData, function (model) {
                                    return (model.systemid == that.systemId && model.constructlevel == data.codeid && model.constructid == consid)
                                });
                                var inputJSNData = $.parseJSON(JSON.stringify(cdata));
                                that.$("#groupId").append(that.templateGmGroup(inputJSNData));
                                that.setconstructLevelValues(cdata, cdata[0].constructname);
                            }

                        });

                        //To ensure speciality group is always displayed at the end of construct level    
                        if (tCnt == cnt) {
                            //only show speciality groups not part of a construct    
                            var groupList = _.filter(groupData, function (model) {
                                return (model.systemid == that.systemId && model.specialitygroup == "Y" && model.constructlevel == "0")
                            })
                            if (groupList != null && groupList.length > 0) {
                                var inputJSNData = $.parseJSON(JSON.stringify(groupList));
                                that.$("#groupId").append(that.templatePricingSpeclGroup());
                                that.$("#groupId").append(that.templateGmGroup(inputJSNData));
                            }
                        }
                    });
            },

            checkBusinessQns: function () {
                var that = this;
                _.each(that.gmcGroup.toJSON(), function (checkflag) {
                    if (checkflag.checkfl == "N") {
                        $("#active-button").css("background-color", "rgba(255, 2, 2, 0.42)").css("color", "black");
                    }
                });
                if (that.question == true) {
                    $("#active-button").css("background-color", "#b2ffb2").css("color", "black");
                }
            },

            /*Show All groups toggle button ON/OFF*/
            showAllGroups: function (e) {
                var that = this;
                if (!$(e.currentTarget).hasClass("val-selected")) {
                    this.loaderview = new LoaderView({
                        text: "Loading Group Data."
                    });
                    $(e.currentTarget).addClass("val-selected");
                    that.ToggleGroupsDisplay(true);
                    that.checkBusinessQns();
                } else {
                    $(e.currentTarget).removeClass("val-selected");
                    that.ToggleGroupsDisplay(false); //ToggleGroupsDisplay(false) to handle constructs levels based on data
                    that.checkBusinessQns();
                }

            },


            //Display method that dispalys details based on show all toggle
            ToggleGroupsDisplay: function (isShowAll) {
                var that = this;
                if (isShowAll) {
                    this.showGroupData(this.gmcGroup.toJSON());
                    var groupList = _.filter(this.gmcGroup.toJSON(), function (model) {
                        return (model.systemid == that.systemId && model.pricetype == "52110" && model.constructlevel == "0" && model.specialitygroup == "");
                    })
                    if (groupList != null && groupList.length > 0) {
                        var constructObj = new Object();
                        constructObj.constructLevel = "Groups";
                        var inputJSNData = $.parseJSON(JSON.stringify(constructObj));
                        this.$("#groupId").append(this.templateGmGroupConstruct(inputJSNData));
                        var inputJSNData = $.parseJSON(JSON.stringify(groupList));
                        that.$("#groupId").append(that.templateGmGroup(inputJSNData));

                    }

                    var groupList = _.filter(this.gmcGroup.toJSON(), function (model) {
                        return (model.systemid == that.systemId && model.pricetype == "52111" && model.specialitygroup != "Y");
                    })
                    if (groupList != null && groupList.length > 0) {
                        var constructObj = new Object();
                        constructObj.constructLevel = "Individual Groups";
                        var inputJSNData = $.parseJSON(JSON.stringify(constructObj));
                        this.$("#groupId").append(this.templateGmGroupConstruct(inputJSNData));
                        
                       /*Trauma Chages********************************************************************************/
                        var systemDivisionID = _.uniq(_.pluck(groupList, 'systemdivisionid'));
                        systemDivisionID = systemDivisionID[0];
                        console.log(" $$$ systemDivisionID $$$$ "+systemDivisionID);
                        var individualGroupIds = "";
                        var indvidualGroupList = "";
                        var loadIndGroup = GM.Global.PRSYSDIVISION.indexOf(systemDivisionID);
                            if(loadIndGroup < 0 ){
                                  individualGroupIds = _.uniq(_.pluck(groupList, 'grouptype'));
                                    _.each(individualGroupIds, function (grouptype) {
                                         indvidualGroupList = _.filter(groupList, function (model) {
                                            return (model.grouptype == grouptype);
                                        });
                                        console.log("== IF == " + JSON.stringify(indvidualGroupList));
                                        that.$("#groupId").append(that.templateGmGroup(indvidualGroupList));
                                    });
                            }else{
                                 individualGroupIds = _.uniq(_.pluck(groupList, 'groupid'));
                                    _.each(individualGroupIds, function (groupid) {
                                         indvidualGroupList = _.filter(groupList, function (model) {
                                            return (model.groupid == groupid);
                                        });
                                        console.log("== ELSE == " + JSON.stringify(indvidualGroupList));
                                         that.$("#groupId").append(that.templateGmGroup(indvidualGroupList));
                                    });
                            }
                        /*
                        var individualGroupIds = _.uniq(_.pluck(groupList, 'grouptype'));

                        _.each(individualGroupIds, function (grouptype) {
                            var indvidualGroupList = _.filter(groupList, function (model) {
                                return (model.grouptype == grouptype);
                            });
                            that.$("#groupId").append(that.templateGmGroup(indvidualGroupList));
                        });
                        */
                         /*Trauma Chages End**************************************************************************/
                    }
                    var jslen = GM.Global.jsnData;
                    var groupidlen = $("#groupId").html();
                    if (jslen != undefined) {
                        if (groupidlen != "") {
                            that.loaderview.close();
                        } else {
                            that.loaderview.close();
                        }
                    }

                } else {
                    var principalFlData = this.gmcGroup.toJSON();
                    principalFlData = _.filter(principalFlData, function (model) {
                        return (model.principalflag == "Y" || model.specialitygroup == "Y");
                    })
                    this.showGroupData(principalFlData); //showGroupData() to handle constructs levels based on data
                }
                _.each($(".ul-price-system-details"), function (t) {
                    var grpid = $(t).attr("groupid");
                    var reqdtlid = $(t).attr("reqdtlid");
                    _.each(that.gmcGroup.toJSON(), function (status) {
                        if (status.statusid == "52125" && grpid == status.groupid && reqdtlid == status.reqdtlid) {
                            $(t).find("#li-price-system-Pruposed-unit input").prop('disabled', 'true').css("background-color", "#c4e8c4");
                        }
                    });
                });

                //iterate to show check flags
                _.each($(".proposedUnit"), function (data) {
                    $(data).trigger("change");
                });
                //            $(".li-price-group-details input").trigger("change"); 
            },

            /*Select System details from Search drop down for selected system*/
            getSelectedSystemDetails: function (sList) {
                var that = this;
                if ($("#div-rep-account-list input").val() != "") {
                    
                    var selectedSystemsDet = new GMCSystem();
                    var gmmSystem = new GMMSystem();
                    if (sList.length == undefined)
                        sList = arrayOf(sList);
                    _.each(sList, function (sL) {
                        var sysid = sL.ID;
                        sysid = sysid.replace(".", "");
                        gmmSystem.set("systemName", sL.Name);
                        gmmSystem.set("systemId", sL.ID);
                        gmmSystem.set("changetype", that.disctypeId);
                        gmmSystem.set("changevalue", $(".div-price-percentage-value-input input").val());
                        gmmSystem.set("errorsystemId", sysid);

                        that.systemName = sL.Name;
                        $(".li-price-change-heading").html(that.systemName);
                        var systemExist = false;
                        if (that.gmcSystem != null && that.gmcSystem.length > 0) {
                            var systemList = that.gmcSystem.where({
                                "systemId": sL.ID
                            });
                            if (systemList != null && systemList.length > 0) {
                                systemExist = true;
                            }
                        }
                        console.log("systemExist", systemExist);
                        if (systemExist == false) {
                            that.$(".ul-add-sys-values").removeClass('ul-product-background-col');
                            var inputJSNDataProduct = $.parseJSON(JSON.stringify(gmmSystem));
                            console.log("inputJSNDataProduct", inputJSNDataProduct);
                            that.$("#div-dash-price-overview").append(that.templateSystemName(inputJSNDataProduct));
                            that.gmcSystem.add($.parseJSON(JSON.stringify(gmmSystem.toJSON())));
                            selectedSystemsDet.add($.parseJSON(JSON.stringify(gmmSystem.toJSON())));
                            if ($(".div-price-cls-input .div-dropdown-text").html() == "Custom") {
                                pricetypedisc.push({
                                    sys: sL.Name,
                                    disc: "52020"
                                });
                                $('.div-price-percentage-value-input input').prop('disabled', true);
                                $('.div-price-group-dt-input input').prop('disabled', true);
                            } else {
                                $(".div-price-cls-input li").each(function () {
                                    if ($(this).attr("id") == "52020") {
                                        that.arn = false;
                                        $(this).trigger("click");
                                    }
                                });
                            }
                            that.systemId = sL.ID;
                        }
                    });
                    that.systemID = true;
                    if (selectedSystemsDet != null && selectedSystemsDet.length > 0) {
                        var AccountIdVal = $('#div-rep-account-id input').val();
                        if (AccountIdVal == undefined)
                            AccountIdVal = accountIdVal;
                        this.gmmAccountPriceRequest.set("cmpid", companyId);
                        this.gmmAccountPriceRequest.set("companyId", companyId);
                        this.gmmAccountPriceRequest.set("partyid", AccountIdVal);
                        this.gmmAccountPriceRequest.set("typeid", typeIdVal);
                        var selsystems = "";
                        _.each(selectedSystemsDet.toJSON(), function (data) {
                            selsystems = (selsystems.trim().length) ? (selsystems + " , " + data.systemId) : data.systemId;
                        });
                        $("#div-selected-system-list input").val("");
                        this.gmmAccountPriceRequest.set("selectedSystems", selsystems);
                        //sending single/multiple systems as input to get the system details
                        var input = {
                            "gmAccountPriceRequestVO": this.gmmAccountPriceRequest
                        };
                        GM.Global.jsnData = undefined;
                    $("#li-price-check-load-all-groups .toggle-button").addClass("val-selected toggle-button-selected");
                    if ($("#li-price-check-load-all-groups .toggle-button").hasClass("toggle-button-selected"))
                        $("#li-price-check-load-all-groups .toggle-button").trigger("click");
                        //calling web service for fetch the data for systems
                        fnGetWebServerData("pricingRequest/pricingSystemGroups/", "gmGroupVO", input, function (data) {
                            if (data != null) {
                                that.$("#groupId").html("");
                                var jsnData = $.parseJSON(JSON.stringify(data));
                                GM.Global.jsnData = jsnData.length;
                                //assigning jsnData to gmcGroup collection for fetched system details
                                that.gmcGroup.add(jsnData);
                                var groupData = that.gmcGroup.toJSON();
                                GM.Global.allData = groupData;
                                groupData = _.filter(groupData, function (model) {
                                    return (model.principalflag == "Y" || model.specialitygroup == "Y"); //returning only principalflag , specialitygroup = 'Y' as default
                                })
                                that.showGroupData(groupData); //showGroupData() to handle constructs levels based on data
                                that.ToggleGroupsDisplay(true);
                                //After loading the template disable the secondary text box for Trauma Systems
                                if ($(".div-price-cls-input .div-dropdown-text").html() == "Custom") {
                                    $('.div-price-group-dt-input input').prop('disabled', true);
                                }
                                
                            } else
                                that.$("#groupId").html("");
                        });
                    }
                } else
                    showError("Please select at least one Account");
                $("#div-selected-system-list input").val("");
            },

            /*Select System details from Search drop down for selected system - suganthi*/
            getSelectedSystems: function (sList) {
                var that = this;
                $("#li-price-check-load-all-groups .toggle-button").addClass("val-selected toggle-button-selected");
                if ($("#li-price-check-load-all-groups .toggle-button").hasClass("toggle-button-selected"))
                    $("#li-price-check-load-all-groups .toggle-button").trigger("click");
                var selectedSystemsDet = new GMCSystem();
                var gmmSystem = new GMMSystem();
                if (sList.length == undefined)
                    sList = arrayOf(sList);
                console.log(sList);
                _.each(sList, function (sL) {
                    var sysid = sL.ID;
                    sysid = sysid.replace(".", "");
                    gmmSystem.set("systemName", sL.Name);
                    gmmSystem.set("systemId", sL.ID);
                    gmmSystem.set("changetype", that.disctypeId);
                    gmmSystem.set("changevalue", $(".div-price-percentage-value-input input").val());
                    gmmSystem.set("errorsystemId", sysid);

                    that.systemName = sL.Name;
                    console.log(that.systemName);
                    that.$(".ul-add-sys-values").removeClass('ul-product-background-col');
                    var inputJSNDataProduct = $.parseJSON(JSON.stringify(gmmSystem));
                    console.log(inputJSNDataProduct);
                    $("#div-dash-price-system").append(that.templateSystemName(inputJSNDataProduct));
                    $("#div-price-overlay-content-container12").find('.modal-body #fa-acc-minus-price').unbind('click').bind('click', function (e) {
                        that.removeSystemOnclickOfMinusCircle(e);
                    });

                });
            },

            /*Save button Valition*/
            saveValidate: function (e) {
                var that = this;
                that.saveflag = "";
                that.proposedflag = "";
                that.proposedfl = "";
                that.parentgroup = "";
                var sysid = "";
                var rs = true;

                _.each(that.gmcGroup.toJSON(), function (checkflag) {
                    if (checkflag.checkfl == "N") {
                        that.saveflag = true;
                    }
                });

                var systemName = _.chain(that.gmcGroup.toJSON()).map(function (model) {
                    return model.systemname;
                }).uniq().value();
                _.each(systemName, function (sdata) {
                    var groupList = _.filter(that.gmcGroup.toJSON(), function (model) {
                        return (model.systemname == sdata && model.proposedunitprice != "")
                    });
                    if (groupList.length == 0) {
                        that.SystemNm = sdata;
                        that.proposedflag = true;
                    }
                });

                var groupList = _.filter(that.gmcGroup.toJSON(), function (model) {
                    return (model.groupid != "" && model.proposedunitprice != "")
                });

                _.each(groupList, function (data) {
                    _.each(groupList, function (D) {
                        if (data.groupid == D.groupid && data.partdesc == D.partdesc && data.proposedunitprice != D.proposedunitprice && data.systemid == D.systemid) {
                            that.GroupNm = data.groupname;
                            that.proposedfl = true;
                        }
                    });
                });

                /*Enabling the Pricing indicator icons while clicking save/sumbit*/
                that.fnShowPriceIndicatoricon(e);

                if ($("#div-rep-account-list input").val() == "") {
                    showError("Please select a Rep/Group account");
                    rs = false;
                } else if ($(".ul-add-sys-values").length < 1) {
                    showError("Please select at least one system");
                    rs = false;
                } else if (that.saveflag == true && that.question != true && GM.Global.btnName == "submit") {
                    showError("Update Business Questions");
                    rs = false;
                } else if ($(".div-price-percentage-value-input input").val() == "" && this.disctypeId == "52021") {
                    showError("Please enter value when select <b>%Off List</b>");
                    rs = false;
                } else if (GM.Global.btnName == "submit" && that.proposedfl == true) {
                    showError("<B>" + that.GroupNm + "</B> Proposed Unit Price mismatched in Construct Level");
                    rs = false;
                } else if (GM.Global.btnName == "submit" && that.parentgroup == true) {
                    this.fnOpenAllPriceIndicatorDetails(e);
                    rs = false;
                } else if (GM.Global.btnName == "submit" && that.proposedflag == true) {
                    showError("The field Proposed - Unit cannot be left blank for <B>" + that.SystemNm + "</B> ");
                    rs = false;
                }
                return rs;

            },

            fnOpenPriceIndicatorDetails: function (e) {
                var that = this;
                var templatepriceIndicator = fnGetTemplate(URL_Price_Template, "gmtErrorIndicator");
                var groupList = _.filter(that.gmcGroup.toJSON(), function (model) {
                    return (model.groupid != "" && model.proposedunitprice != "")
                });
                this.groupListData = [];
                this.groupData = [];
                var systemid = $(e.currentTarget).parent().parent().find(".li-productName").attr("productname");
                _.each(groupList, function (data) {
                    _.each(groupList, function (D) {
                        if ((data.proposedunitprice != "" && D.proposedunitprice != "") && data.proposedunitprice != D.proposedunitprice && systemid == data.systemid && data.parentgroupid == D.parentgroupid && data.partnum == D.partnum) {
                            that.groupData.push(D);
                            that.groupListData.push(data);

                        }
                    });
                });

                var newData = _.union(this.groupData, this.groupListData);
                var inputJSNData = JSON.stringify(newData);
                inputJSNData = arrayOf($.parseJSON(inputJSNData));
                showPriceindicatorPopup();
                $("#msg-overlay").show();
                $("#div-price-overlay-content-container14 .modal-body").html(that.templatepriceIndicator(inputJSNData));
                $("#div-price-overlay-content-container14").find('.modal-body .li-threshold-cancel-button').unbind('click').bind('click', function () {
                    hidePriceindicatorPopup();
                    $("#msg-overlay").hide();
                });

            },

            fnOpenAllPriceIndicatorDetails: function (e) {
                var that = this;
                var templatepriceIndicator = fnGetTemplate(URL_Price_Template, "gmtErrorIndicator");
                var groupList = _.filter(that.gmcGroup.toJSON(), function (model) {
                    return (model.groupid != "" && model.proposedunitprice != "")
                });
                this.groupListData = [];
                this.groupData = [];
                _.each(groupList, function (data) {
                    _.each(groupList, function (D) {
                        if ((data.proposedunitprice != "" && D.proposedunitprice != "") && data.proposedunitprice != D.proposedunitprice && data.parentgroupid == D.parentgroupid && data.partnum == D.partnum) {
                            that.groupData.push(D);
                            that.groupListData.push(data);
                        }
                    });
                });

                var newData = _.union(this.groupData, this.groupListData);
                var inputJSNData = JSON.stringify(newData);
                inputJSNData = arrayOf($.parseJSON(inputJSNData));
                showPriceindicatorPopup();
                $("#msg-overlay").show();
                $("#div-price-overlay-content-container14 .modal-body").html(that.templatepriceIndicator(inputJSNData));
                $("#div-price-overlay-content-container14").find('.modal-body .li-threshold-cancel-button').unbind('click').bind('click', function () {
                    hidePriceindicatorPopup();
                    $("#msg-overlay").hide();
                });

            },

            fnShowPriceIndicatoricon: function (e) {
                var that = this;
                var sysid = "";
                var groupList = _.filter(that.gmcGroup.toJSON(), function (model) {
                    return (model.groupid != "" && model.proposedunitprice != "")
                });
                $(".div-indicator-icon-main").addClass("hide");
                $(".div-triangle").addClass("hide");
                $(".div-indicator").addClass("hide");
                _.each(groupList, function (data) {
                    that.groupid = data.groupid;
                    $(".div-error-icon-" + that.groupid).removeClass("highlighted-error-detail");
                    _.each(groupList, function (D) {
                        if ((data.proposedunitprice != "" && D.proposedunitprice != "") && data.proposedunitprice != D.proposedunitprice && data.parentgroupid == D.parentgroupid && data.partnum == D.partnum) {
                            var newData = _.union(data, D);
                            _.each(newData, function (data) {
                                that.ParentGroupNm = data.parentgroupname
                                that.parentgroup = true;
                                that.systemname = data.systemname;
                                that.groupid = data.groupid;
                                sysid = data.systemid;
                                sysid = sysid.replace(".", "");
                                $(".div-error-icon-" + sysid).removeClass("hide");
                                $(".div-indicator-icon-main").removeClass("hide");
                                that.highlightSelectedForPriceIndicator(that.groupid);
                                that.updatePriceTypeForErrorSystems(sysid);
                            });

                        }

                    });
                });
            },

            updatePriceTypeForErrorSystems: function (sysid) {
                $(".ul-add-sys-values").each(function () {
                    var pricetype = $(this).attr("pricetype");
                    if (pricetype == "52021") {
                        $(this).find($(".div-error-icon-off-" + sysid)).removeClass("hide");
                        $(this).find($(".div-error-icon-custom-" + sysid)).addClass("hide");
                    } else {
                        $(this).find($(".div-error-icon-custom-" + sysid)).removeClass("hide");
                        $(this).find($(".div-error-icon-off-sysid-" + sysid)).addClass("hide");
                    }
                });
            },



            /*Saving Price information*/
            savePriceRequestDetails: function () {
                this.gmmAccountPriceRequest.set("salesRepId", localStorage.getItem("userID"));
                this.gmmAccountPriceRequest.set("cmpid", companyId);
                this.gmmAccountPriceRequest.set("companyId", companyId);
                this.gmmAccountPriceRequest.set("createdBy", localStorage.getItem("userID"));
                var accpartyId = $("#div-rep-account-id input").val();
                if (accountIdVal == "") {
                    this.gmmAccountPriceRequest.set("partyid", accpartyId);
                } else
                    this.gmmAccountPriceRequest.set("partyid", accountIdVal);
                this.gmmAccountPriceRequest.set("userid", localStorage.getItem("userID"));
                if (GM.Global.btnName == "save") {
                    this.gmmAccountPriceRequest.set("requestStatus", this.requestStatus);
                } else {
                    this.gmmAccountPriceRequest.set("requestStatus", this.requestSubmitStatus);
                    this.gmmAccountPriceRequest.set("stropt", localStorage.getItem("acid"));
                }
                var twelMnthPrjSale = $(".div-projected-scale-value input").val().replace(/\D/g, '');;
                this.gmmAccountPriceRequest.set("projected12MonthsSale", twelMnthPrjSale);
                this.gmmAccountPriceRequest.set("requestType", typeIdVal);
                this.gmmAccountPriceRequest.set("typeid", typeIdVal);

                var that = this;
                var selsystems = "";
                _.each(this.gmcSystem.toJSON(), function (data) {
                    selsystems = (selsystems.trim().length) ? (selsystems + " , " + data.systemId) : data.systemId;
                })


                //Remove Approved & implemented records before sending to back-end
                var removeStatuslist = _.filter(that.gmcGroup.toJSON(), function (model) {
                    return ((model.statusid != "52125") && (model.statusid != "52190"));
                });
                var jsnData = $.parseJSON(JSON.stringify(removeStatuslist));

                //assign to new GMCGroup array
                that.gmcGroup = new GMCGroup();
                that.gmcGroup.add(jsnData);
                this.gmmAccountPriceRequest.set("selectedSystems", selsystems);
                var input = {
                    "gmAccountPriceRequestVO": this.gmmAccountPriceRequest,
                    "gmSystemVO": this.gmcSystem,
                    "gmGroupVO": this.gmcGroup,
                    "gmThresholdQuestionVO": this.gmcBusinessQuestions
                };
                this.loaderview = new LoaderView({
                    text: "Saving Data."
                });
                fnGetWebServerData("pricingRequest/saveOrUpdatePricingDetails/", undefined, input, function (data) {
                    that.loaderview.close();
                    if (data != null) {
                        GM.Global.PricingStatus = data.pricerequeststatus;
                        that.priceDataID = data.pricerequestid;
                        if (data.gmAccountPriceRequestVO.typeid == typeIdVal) {
                            $("#div-price-request-id-name").html(data.pricerequestid);
                        } else {
                            $("#div-price-request-id-name").html("");
                        }
                        that.gmmAccountPriceRequest.set("pricerequestid", data.pricerequestid);
                        if (data.pricerequeststatus != 52186) {
                            $("#btn-main-save").hide();
                            $("#btn-main-submit").hide();
                            showSuccess("Price Request <B>" + data.pricerequestid + "</B> Submitted successfully");
                        } else
                            showSuccess("Price Request <B>" + data.pricerequestid + "</B> Saved successfully");
                    } else
                        showError("Exception during Price Request Initiation");
                });
            },

            /*Saving Price information*/
            saveOrUpdatePricingDetails: function (e) {
                var that = this;
                GM.Global.btnName = $(e.currentTarget).attr("btnname");
                that.fnSetConstantValues();
                var switchuserfl = localStorage.getItem("switchuserFl");
                if (switchuserfl != 'Y') {
                    if (that.saveValidate(e)) {
                        if (GM.Global.btnName == "submit") {
                            var that = this;
                            if (GM.Global.Browser) {
                                var rmv = confirm("Do you want to submit ?");
                                if (rmv == true) {
                                    that.savePriceRequestDetails();
                                }
                            } else {
                                showNativeConfirm("Do you want to submit ?", "Confirm", ["Yes", "No"], function (idx) {
                                    if (idx == 1) {
                                        that.savePriceRequestDetails();
                                    }
                                });
                            }
                        } else
                            that.savePriceRequestDetails();
                    }
                }
            },

            /*Method to load Price Request based on Id */
            fnLoadPriceReqDetails: function () {
                var newprid = $(".div-request-type input").val();
                if (this.rptSubmit == true) {
                    if (this.priceRequestIdValue != undefined && (this.priceRequestIdValue != $(".div-request-type input").val())) {
                        var that = this;
                        if (GM.Global.Browser) {
                            var rmv = confirm("Do you want to GO another PR id..?");
                            if (rmv == true) {
                                that.priceRequestIdVal = newprid;
                                that.fnGetPriceRequestDetails();
                            } else {
                                that.fnGetPriceRequestDetails();
                            }
                        } else {
                            showNativeConfirm("Do you want to GO another PR id..?", "Confirm", ["Yes", "No"], function (idx) {
                                if (idx == 1) {
                                    that.priceRequestIdVal = newprid;
                                    that.fnGetPriceRequestDetails();
                                } else {
                                    that.fnGetPriceRequestDetails();
                                }
                            });
                        }
                    }
                } else
                    this.fnGetPriceRequestDetails();

            },

            /* Method to get the specific pricing request details*/
            fnGetPriceRequestDetails: function () {
                if ($("#div-pricing-request input").val() != "") {
                    this.gmcGroup = new GMCGroup();
                    this.gmcSystem = new GMCSystem();
                    this.gmcBusinessQuestions = new GMCBusinessQuestions();
                    this.gmmAccountPriceRequest = new GMMAccountPriceRequest();
                    if (this.priceRequestIdVal != undefined && this.priceRequestIdVal != "") {
                        var priceRequestId = this.priceRequestIdVal;
                        $("#div-pricing-request-input").val(this.priceRequestIdVal);
                    } else
                        var priceRequestId = $("#div-pricing-request-input").val();
                    priceRequestId = priceRequestId.toUpperCase();
                    this.$(".div-price-request-id-label").hide();
                    this.priceRequestIdValue = priceRequestId;
                    var userID = localStorage.getItem("userID");
                    var acid = localStorage.getItem("acid");
                    var deptid = localStorage.getItem("deptid");
                    var input = {
                        "pricerequestid": priceRequestId,
                        "userid": userID,
                        "acid": acid,
                        "deptid": deptid
                    };
                    var that = this;
                    this.loaderview = new LoaderView({
                        text: "Loading price request " + priceRequestId + " ."
                    });
                    fnGetWebServerData("pricingRequest/priceRequestDetails/", undefined, input, function (data) {
                        that.loaderview.close();
                        if (data != null) {
                            GM.Global.PricingStatus = data.gmAccountPriceRequestVO.statusid;
                            if (data.gmAccountPriceRequestVO.statusid != "52186" && localStorage.getItem("acid") < 3) {
                                $("#div-price-delete-btn").removeClass("hide");
                                $("#div-price-delete-btn").parent().find("input").attr('disabled', 'disabled');
                            }
                            if (data.listGmSystemVO.changetype == "52020"){ 
                                $('.div-price-percentage-value-input input').prop('disabled', true);
                                $('.div-price-group-dt-input input').prop('disabled', true);
                            }
                            if (data.gmAccountPriceRequestVO.statusid != 52186 && data.gmAccountPriceRequestVO.statusid != 52120) {
                                $("#btn-main-save").hide();
                                $("#btn-main-submit").hide();
                                $("#div-selected-system-list input").prop('disabled', true);
                            } else {
                                $("#btn-main-save").show();
                                $("#btn-main-submit").show();
                                $("#div-selected-system-list input").prop('disabled', false);
                            }
                            //enable save and submit buttons for AD, Vp and Super AD's
                            if (data.gmAccountPriceRequestVO.statusid == "52187" && localStorage.getItem("acid") >= '3') {
                                $("#btn-main-save").show();
                                $("#btn-main-submit").show();
                            }
                            that.$(".div-pricing-info-change").show();
                            that.$(".ul-price-save-btn").show();
                            that.$("#li-price-info-system-lable").show();
                            that.$(".div-price-submit-label1").show();
                            that.$(".div-price-submit-label1").css("display", "inline-block");
                            that.$(".fa-information-circle").show();
                            that.$("#btn-main-cancel").show();
                            that.$(".fa-crosshairs").show();
                            var jsnData = $.parseJSON(JSON.stringify(data.listGmSystemVO));
                            GM.Global.jsnData = jsnData.length;
                            that.gmcSystem.add(jsnData);
                            var systemId;
                            that.$("#div-dash-price-overview").html("");
                            _.each(that.gmcSystem.toJSON(), function (system) {
                                systemId = system.systemId;
                                var inputJSNDataProduct = $.parseJSON(JSON.stringify(system));
                                that.$("#div-dash-price-overview").append(that.templateSystemName(inputJSNDataProduct));
                            });

                            var gmmAccountPriceRequestIns = $.parseJSON(JSON.stringify(data.gmAccountPriceRequestVO));
                            that.gmmAccountPriceRequest = new GMMAccountPriceRequest(gmmAccountPriceRequestIns);
                            accountIdVal = that.gmmAccountPriceRequest.get("partyid");
                            that.$("#div-price-submit-req-account-name").html(that.gmmAccountPriceRequest.get("partyname"));
                            that.$("#div-price-request-id-name").html(that.gmmAccountPriceRequest.get("pricerequestid"));
                            that.$(".div-projected-scale-value input").val(that.gmmAccountPriceRequest.get("projected12MonthsSale"));
                            typeIdVal = that.gmmAccountPriceRequest.get("typeid");
                            that.questionCount = that.gmmAccountPriceRequest.get("questioncnt");
                            if (that.questionCount > 0) {
                                that.question = true;
                            } else {
                                $("#active-button").css("background-color", "rgba(255, 2, 2, 0.42)").css("color", "black");
                                that.question = false;
                            }
                            jsnData = arrayOf(jsnData);
                            _.each(jsnData, function (pdata) {
                                pricetypedisc.push({
                                    sys: pdata.systemName,
                                    disc: pdata.changetype
                                })
                            });
                            $(".ul-product-background-col").removeClass("ul-product-background-col");
                            $(".ul-add-sys-values").first().addClass("ul-product-background-col");
                            _.each(jsnData, function (data) {
                                $(".div-price-cls-input .div-dropdown-list li").each(function () {
                                    if ($(this).find('a').html().trim() == data.changetypename) {
                                        that.disctypeId = data.changetype;
                                        that.disctypeSys = data.systemName;
                                        that.arn = true;
                                        $(this).trigger('click');
                                    }
                                    _.each($(".li-productName"), function (Data) {
                                        if ($(Data).attr("setname") == data.systemName)
                                            $(Data).attr("changeValue", data.changevalue);
                                    });
                                    that.$(".li-price-change-heading").html(data.systemName);
                                })
                            });

                            if (that.priceRequestIdVal != undefined && that.priceRequestIdVal != "") {
                                $("#div-pricing-request-input").val(that.priceRequestIdVal);
                            }
                            that.systemId = systemId;
                            var jsnData = $.parseJSON(JSON.stringify(data.listGmGroupVO));
                            that.gmcGroup.add(jsnData);
                            //                       $(".li-price-group-details input").trigger("change");
                            var groupData = that.gmcGroup.toJSON();
                            that.showGroupData(groupData);

                            var doccnt = that.gmmAccountPriceRequest.get("documentcount");
                            //display document count in upload button
                            if (doccnt > 0) {
                                $(".document_cnt").html(" (" + that.gmmAccountPriceRequest.get("documentcount") + ")");
                                $(".document_cnt").css("color", "red");
                            }
                            var impactcnt = that.gmmAccountPriceRequest.get("impactcount");
                            if (impactcnt > 0) {
                                $(".div-price-impact-btn").css("background-color", "#b2ffb2").css("color", "black");
                                $(".div-price-sales-analysis").css("background-color", "#b2ffb2").css("color", "black");
                            } else {
                                $(".div-price-impact-btn").css("background-color", "#fff8e4").css("color", "black");
                                $(".div-price-sales-analysis").css("background-color", "#fff8e4").css("color", "black");
                            }

                        } else {
                            showError("Entered Pricing Request <B>" + priceRequestId + "</B> is Invalid, please verfiy");
                            $("#div-pricing-request input").val("");
                            that.fnSetDefaultValues();
                            $("#div-price-submit-req-account-name").html("");
                            $(".div-price-percentage-value-input input").val("");
                            that.$(".div-dropdown-text").html("Custom");
                            that.hideSubmittedReq();
                        }
                        $(".ul-add-sys-values").first().find(".li-productName").trigger("click");
                        _.each($(".ul-price-system-details"), function (t) {
                            var grpid = $(t).attr("groupid");
                            var reqdtlid = $(t).attr("reqdtlid");
                            _.each(data.listGmGroupVO, function (status) {
                                if (status.statusid == "52125" && grpid == status.groupid && reqdtlid == status.reqdtlid) {
                                    $(t).find("#li-price-system-Pruposed-unit input").prop('disabled', 'true').css("background-color", "#c4e8c4");
                                }
                            });
                        });
                    });
                } else {
                    showError("Price Request # should not empty");
                }
            },

            /*method to get constructlevel*/
            fnGetConstructLevels: function () {
                var input = {
                    codegrp: "CNTLVL"
                };
                fnGetWebServerData("common/codelookuplist/", "gmCodeLookupVO", input, function (data) {
                    if (data != null)
                        GM.Global.Constructs = data;
                });
            },

            fnGetConstructFullName: function (e) {
                var that = this;
                var name = $(e.currentTarget).html();
                showAccountInfoPopup();
                $("#msg-overlay").show();
                $("#div-price-overlay-content-container6 .modal-body").html(that.templateShowName());
                $(".li-pricing-group-part-screen").html("Consrtuct Name");
                $(".div-show-construct-name").html(name).css("height", "30px").css("margin", "10px");
                $("#div-price-overlay-content-container6").find('.modal-body .li-group-part-cancel-button, .div-price-group-close-btn').unbind('click').bind('click', function () {
                    hideAccountInfoPopup();
                    $("#msg-overlay").hide();
                });
            },

            fnGetGroupFullName: function (e) {
                var that = this;
                $(e.currentTarget).attr("id", "")
                var originalwidth = $(e.currentTarget).width();
                $(e.currentTarget).attr("id", "li-price-system-name");
                var settedwidth = $(e.currentTarget).width();
                $(".highlighted-item").removeClass("highlighted-item");
                $(e.currentTarget).parent().addClass("highlighted-item");
                if (settedwidth < originalwidth)
                    fnGetGroupFullName(e);
                //this functionality has been moved to gmvPricingCommon.js
            },

            /*
                 //  method to calculate Impact Analysis (PRT-79)
                 //  Requirements : http://confluence.spineit.net/display/PRJ/PRT_R203-Generate+and+view+Impact+Analysis+for+a+price+request  
                 //  Design       : http://confluence.spineit.net/display/PRJ/PRT_D203+Generate+and+view+Impact+Analysis+for+a+price+request
                 */
            calcImpactAnaly: function (e, grpBy) {
                var that = this;
                var requestid = "";
                if (this.priceDataID != "")
                    requestid = that.priceDataID;
                if (requestid == "") {
                    requestid = $("#div-pricing-request input ").val();
                };
                if (requestid == undefined || requestid == "") {
                    showError("Price Request # cannot be empty");
                    return;
                };

                //Check if atleast one proposed price is entered before impactAnalysis
                var chkprice = _.filter(that.gmcGroup.toJSON(), function (model) {
                    return ((model.proposedunitprice > 0));
                });
                if (chkprice.length == 0) {
                    showError("Proposed Price Not Entered");
                    return;
                }
                calcImpactAnaly(e, that, requestid, LoaderView)
                //this function has been changed to gmvPricingCommon.js
            },

            /*
                 //  method to Upload files for a price request (PRT-2)
                 //  Requirements :  http://confluence.spineit.net/display/PRJ/PRT_R202+-+Upload+files+for+a+price+request 
                 //  Design       :  http://confluence.spineit.net/display/PRJ/PRT_D202+Upload+files+for+a+price+request
                 */
            showFileUploadPopup: function () {
                var that = this;
                var refid = "";
                var switchuserfl = localStorage.getItem("switchuserFl");
                if (this.priceDataID != "")
                    refid = that.priceDataID;
                if (refid == "") {
                    refid = $("#div-pricing-request input ").val() || $("div-price-request-id-name").val();
                };
                if (refid == undefined || refid == "") {
                    showError("Price request Id cannot be blank. Save the price request");
                    return;
                }
                var refid = refid.indexOf("PR-") >= 0 ? refid : "PR-" + refid;
                GM.Global.ID = refid;
                if (switchuserfl != 'Y') {
                    showPriceFileUploadPopup(that, refid, LoaderView, GMCFileUpload);
                }
                //this functionality has been moved to gmvPriceCommon.js
            },
            highlightSelected: function (e) {
                $(".highlighted-item").removeClass("highlighted-item");
                $(e.currentTarget).parent().parent().addClass("highlighted-item");
            },
            highlightDisplay: function (e) {
                $(".highlighted-item").removeClass("highlighted-item");
                var data;
                switch ($(e.currentTarget).attr("parent")) {
                    case "single":
                        data = $(e.currentTarget).parent();
                        break;
                    case "double":
                        data = $(e.currentTarget).parent().parent();
                        break;
                }

                $(data).addClass("highlighted-item");
            },

            fnGetPartDetails: function (e) {
                if ($("#div-rep-account-list input").val() == "") {
                    // showError("Please select a Rep/Group account");
                } else {
                    showPartDetails($(e.currentTarget), LoaderView);
                }
            },
            fnGetPartSystemDetails: function (e) {
                if ($("#div-rep-account-list input").val() == "") {
                    // showError("Please select a Rep/Group account");
                } else {
                    this.showPartSystemDetails($(e.currentTarget), LoaderView);
                }
            },
            highlightSelectedForPriceIndicator: function (Groupid) {
                $(".div-error-icon-" + Groupid).addClass("highlighted-error-detail");
            },

            generateSalesAnalysis: function (e) {
                var that = this;
                var requestid = "";
                if (this.priceDataID != "")
                    requestid = that.priceDataID;
                if (requestid == "") {
                    requestid = $("#div-pricing-request input ").val();
                };
                if (requestid == undefined || requestid == "") {
                    showError("Price Request # cannot be empty");
                    return;
                };

                generateSalesAnalysis(e, that, requestid, LoaderView);
            },

            showPartSystemDetails: function (e, LoaderView) {
                console.log(e);
                var that = this;
                this.showPartSysDetailsPopup();
                $("#msg-overlay").show();
                var templatePartDetails = fnGetTemplate(URL_Price_Template, "gmGroupPartSysPricing");
                var templateGroupTotalDetails = fnGetTemplate(URL_Price_Template, "gmtSysGroupPricingDetaills");
                $("#div-price-overlay-content-container12 .modal-body").html(templatePartDetails());

                var sysSearchOptions = {
                    "el": $('#div-acc-selected-system-list'),
                    "ID": "txt-keyword",
                    "Name": "txt-search-name",
                    "Class": "txt-keyword-initial",
                    "minChar": "3",
                    "autoSearch": true,
                    "placeholder": "Key in at least 3 characters to search",
                    "fnName": fnGetIMSystem,
                    "usage": 2,
                    "template": 'GmSearchResult',
                    "template_URL": URL_Common_Template,
                    "callback": function (sList) {
                        that.getSelectedSystems(sList);
                    }
                };

                var syssearchview = new SearchView(sysSearchOptions);


                $("#div-price-overlay-content-container12").find('.modal-body #btn-part-load-btn').unbind('click').bind('click', function () {

                    that.selectedSystem = [];
                    that.selectedSystemList = [];
                    $("#div-price-overlay-content-container12 .li-sys-productName").each(function (dt) {
                        var systemId = $(this).attr("productname");
                        that.selectedSystemList.push(systemId);
                        that.selectedSystem.push({
                            "systemId": systemId
                        });
                    });
                    if (that.selectedSystemList != "" || $("#div-part-value-input").val() != "") {
                        var input = {};
                        input.partNum = $("#div-part-value-input").val();
                        console.log(input.partNum);
                        input.system = that.selectedSystemList.toString();
                        console.log(input.system);
                        input.accId = $("#div-price-value-input").val();
                        input.token = localStorage.getItem("token");
                        input.userid = localStorage.getItem("userID");
                        GM.Global.Loader = new LoaderView({
                            text: "Loading Part Details."
                        });
                        fnGetWebServerData("pricingRequest/SystemPartPricing/", undefined, input, function (data) {
                            GM.Global.Loader.close();
                            if (data.listGmPartVO != undefined) {
                                var templatepartdata = JSON.stringify(data.listGmPartVO);
                                if (templatepartdata != undefined) {
                                    templatepartdata = arrayOf($.parseJSON(templatepartdata));
                                    $("#div-price-overlay-content-container12 .modal-body").find(".div-part-price-details").html(templateGroupTotalDetails(templatepartdata));
                                }

                            } else {
                                $("#div-price-overlay-content-container12 .modal-body").find(".div-part-price-details").html("No Data Found").addClass("textalign");
                            }

                        });
                    } else {
                        showError("Please provide valid input in system or part# to proceed further");
                    }
                });
                $("#div-price-overlay-content-container12").find('.modal-body .price-popup-part-close').unbind('click').bind('click', function () {
                    hidePartSysDetailsPopup();
                    $("#msg-overlay").hide();
                });
                $("#div-price-overlay-content-container12").find('.modal-body .li-threshold-cancel-button').unbind('click').bind('click', function () {
                    hidePartSysDetailsPopup();
                    $("#msg-overlay").hide();
                });
            },

            showPartSysDetailsPopup: function () {
                $('.div-overlay').show();
                $('.div-price-overlay-content-container').addClass('hide');
                $('#div-price-overlay-content-container12').removeClass('hide');


                $('.div-price-overlay-content-container button[data-dismiss="modal"]').each(function () {
                    $(this).unbind('click').bind('click', function () {
                        hidePartSysDetailsPopup();
                    });
                });
            },


        });

        return PriceRequest;
    });
