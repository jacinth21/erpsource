/**********************************************************************************
 * File:        gmvPricingCommon.js
 * Description: Contains global variables and functions commonly used across Pricing
 * Version:     1.0
 * Author:      Akumar
 **********************************************************************************/
function showGroupPartPricingDetails(e) {
    //                var that = this;
    var templateGroupPartPricingDetails = fnGetTemplate(URL_Price_Template, "gmtGroupPartPricingDetails");
    var token = localStorage.getItem("token");
    var grpid = $(e.currentTarget).attr("id");
    var grpname = $(e.currentTarget).attr("name");
    var input = {
        "groupid": grpid,
        "groupname": grpname,
        "token": token
    };
    fnGetWebServerData("pricingRequest/groupPartMapDetailsList/", undefined, input, function (data) {
        var templatedata = JSON.stringify(data.gmGroupPartMappingVO);
        templatedata = arrayOf($.parseJSON(templatedata));
        showGroupPartInfoPopup();
        $("#msg-overlay").show();
        $("#div-price-overlay-content-container7 .modal-body").html(templateGroupPartPricingDetails(templatedata));
        $(".span-list-price-symbol").html(localStorage.getItem("cmpcurrsmb"));
        $(".li-pricing-group-part-screen").html(grpname);
        $("#div-price-overlay-content-container7").find('.modal-body .li-group-part-cancel-button, .div-price-group-close-btn').unbind('click').bind('click', function () {
            hideGroupPartInfoPopup();
            $("#msg-overlay").hide();
        });
    });
}

function showPriceAccountDetails(input1, input2, LoaderView) {
    var that = this;
    var templatePriceAccountDetails = fnGetTemplate(URL_Price_Template, "gmtPriceAccountDetails");
    var templateGPBPriceAccountDetails = fnGetTemplate(URL_Price_Template, "gmtPriceGPBAccountDetails");
    fnGetWebServerData("pricingRequest/accountName/", "gmPriceAccountTypeVO", input1, function (data) {
        if (data.accname != "" || accID == undefined) {
            this.loaderview = new LoaderView({
                text: "Loading Account Details."
            });
            fnGetWebServerData("pricingRequest/accountDetailsList/", undefined, input2, function (data) {
                that.loaderview.close();
                if (data != null) {
                    var templatedata = JSON.stringify(data.gmAccountDetailsListVO);
                    showAccountInfoPopup();
                    $("#msg-overlay").show();
                    //903108 := Group Account
                    if (input1.typeid == "903108") {
                        console.log(data.gmAccountDetailsListVO);
                        data = arrayOf(data.gmAccountDetailsListVO);
                        if (data[0].accid == "")
                            data["empty"] = "Y";
                        $("#div-price-overlay-content-container6 .modal-body").html(templateGPBPriceAccountDetails(data));
                    } else {
                        $("#div-price-overlay-content-container6 .modal-body").html(templatePriceAccountDetails(data.gmAccountDetailsListVO[0]));
                    }
                } else {
                    showError("Invalid Account/Party Id");
                }
                $("#div-price-overlay-content-container6").find('.modal-body .li-threshold-cancel-button').unbind('click').bind('click', function () {
                    hideAccountInfoPopup();
                    $("#msg-overlay").hide();
                });
            });
        } else {
            showError("Please select a Rep Account");
        }
    });


}

function showPartDetails(e, LoaderView) {
    console.log(e);
    showPartDetailsPopup();
    $("#msg-overlay").show();
    var templatePartDetails = fnGetTemplate(URL_Price_Template, "gmGroupPartPricing");
    var templatePartTotalDetails = fnGetTemplate(URL_Price_Template, "gmtPartPricingDetails");
    var templateGroupTotalDetails = fnGetTemplate(URL_Price_Template, "gmtGroupPricingDetaills");
    $("#div-price-overlay-content-container12 .modal-body").html(templatePartDetails());
    $("#div-price-overlay-content-container12").find('.modal-body #btn-part-load-btn').unbind('click').bind('click', function () {
        if ($("#div-part-value-input").val() != "") {
            var input = {};
            input.partNum = $("#div-part-value-input").val();
            input.search = $(".sel-address-modea").val();
            input.token = localStorage.getItem("token");
            input.userid = localStorage.getItem("userID");

            console.log(input);
            GM.Global.Loader = new LoaderView({
                text: "Loading Part Details."
            });
            fnGetWebServerData("pricingRequest/PartPricing/", undefined, input, function (data) {
                GM.Global.Loader.close();
                console.log(data.listGmPartVO);
                console.log(data.listGmGruouVO);
                if (data.listGmPartVO != undefined || data.listGmGruouVO != undefined) {
                    var templatepartdata = JSON.stringify(data.listGmPartVO);
                    var templategroupdata = JSON.stringify(data.listGmGruouVO);
                    if (templategroupdata != undefined) {
                        templategroupdata = arrayOf($.parseJSON(templategroupdata));
                        $("#div-price-overlay-content-container12 .modal-body").find(".div-part-price-details").html(templateGroupTotalDetails(templategroupdata)).removeClass("textalign");
                    }
                    if (templatepartdata != undefined) {
                        templatepartdata = arrayOf($.parseJSON(templatepartdata));
                        $("#div-price-overlay-content-container12 .modal-body").find(".div-part-price-details").append(templatePartTotalDetails(templatepartdata));
                    }

                } else {
                    $("#div-price-overlay-content-container12 .modal-body").find(".div-part-price-details").html("No Data Found").addClass("textalign");
                }

            });
        } else {
            showError("Please enter the part number");
        }
    });
    $("#div-price-overlay-content-container12").find('.modal-body .price-popup-part-close').unbind('click').bind('click', function () {
        hidePartDetailsPopup();
        $("#msg-overlay").hide();
    });
}



function fnGetGroupFullName(e) {
    var that = this;
    var templateShowName = fnGetTemplate(URL_Price_Template, "gmtShowName");
    var name = $(e.currentTarget).html();
    var partnumber = $(e.currentTarget).attr("partnumber");
    showAccountInfoPopup();
    $("#msg-overlay").show();
    $("#div-price-overlay-content-container6 .modal-body").html(templateShowName());
    if (partnumber != "" && partnumber != undefined)
        $(".li-pricing-group-part-screen").html("Part Description");
    else
        $(".li-pricing-group-part-screen").html("Group Name");
    $(".div-show-construct-name").html(name).css("height", "30px").css("margin", "10px");
    $("#div-price-overlay-content-container6").find('.modal-body .li-group-part-cancel-button, .div-price-group-close-btn').unbind('click').bind('click', function () {
        hideAccountInfoPopup();
        $("#msg-overlay").hide();
    });
}

function showThresholdQuestions(e, parent) {

    //                showThresholdQuestions(e);
    var that = parent;
    var templatePriceThresholdQusetions = fnGetTemplate(URL_Price_Template, "gmtThresholdQuestions");
    var input = {
        "token": localStorage.getItem("token"),
        "requestid": $(".li-price-reqid-val").html()
    };
    fnGetWebServerData("pricingRequest/thresholdQuestions/", undefined, input, function (data) {
        showInfoPopup();
        $("#msg-overlay").show();
        $("#div-price-overlay-content-container5 .modal-body").html(templatePriceThresholdQusetions(data.gmThresholdQuestionVO));
        $("#div-threshold-submit").hide();
        /*Consultant drpdwn*/
        getConsultantDrp(data.gmThresholdQuestionVO, parent);
        _.each(data.gmThresholdQuestionVO, function (r) {
            if (r.dropdownid != "")
                that.consultantId = r.dropdownid;
        })
        $("#div-price-overlay-content-container5").find('.modal-body .li-threshold-cancel-button').unbind('click').bind('click', function () {
            hideInfoPopup();
            $("#msg-overlay").hide();
        });
        $("#div-price-overlay-content-container5").find('.modal-body #fa-close-alert').unbind('click').bind('click', function () {
            hideMessages();
        });


    });


}

function getConsultantDrp(data, DropDownView) {
    var that = this;
    var defaultid = 0;
    var defaultname = "Choose One";
    var input = {
        "codeGrp": "YES/NO",
        "token": localStorage.getItem("token")
    };
    fnGetCodeLookUpValues(input, function (drpDwn) {
        drpDwn.push({
            "ID": defaultid,
            "Name": defaultname
        });
        if (data[0].dropdownid != "" && data[0].dropdownvalue != "") {
            defaultid = data[0].dropdownid;
            defaultname = data[0].dropdownvalue;
        }
        var consultantdropdownview = new DropDownView({
            el: $("#div-price-overlay-content-container5 .modal-body #li-pricing-dropdown"),
            data: drpDwn,
            DefaultID: defaultid
        });
        $("#div-price-overlay-content-container5 .modal-body #li-pricing-dropdown").bind('onchange', function (e, selectedId, SelectedName) {
            that.consultantId = selectedId;
            that.consultantName = SelectedName;
        });
    });
}

function calcImpactAnaly(e, parent, prid, LoaderView) {
    var that = parent;
    var requestid = "";
    requestid = prid;
    console.log(requestid);
    var repId = localStorage.getItem("userID");
    var token = localStorage.getItem("token");
    var userid = localStorage.getItem("userID");
    var input = {
        "token": "token",
        "refid": requestid,
        "userid": userid,
    };
    var loaderview = new LoaderView({
        text: "Loading Impact Analysis."
    });
    //webservice call to get impact analysis data
    fnGetWebServerData("impactAnalysis/fetchImpactAnalysis/", "gmImpactAnalysisVO", input, function (data) {
        if (data == null) {
            loaderview.close();
            showError("No Sales Data Found");
            return;
        }
        data = arrayOf(data);
        loaderview.close();

        //display chart by default
        chkflag = true; //by default impacted checkbox should be checked
        fnImpactAnalysisCht(LoaderView, e, "system", data, requestid, parent, chkflag);
    });
}

//function to generate impact analysis chart
function fnImpactAnalysisCht(LoaderView, e, grpBy, data, requestId, parent, chkflag, order) {
    var that = parent;
    var allData = data;
    var ctitle = "";
    var xAxisName = "";
    var impChkd = false;

    if ($("#div-price-overlay-content-container10").find(".check-btn:checkbox:checked").length > 0 || (chkflag == true))
        impChkd = true;

    if (grpBy == undefined || grpBy == "system") {
        ctitle = "Impact Analysis by System";
        xAxisName = "Systems";
        if (impChkd == false) {
            var chartData = _.filter(data, function (model) {
                return (model.groupingid == 3) //systems
            });
        } else {
            var chartData = _.filter(data, function (model) {
                return (model.groupingid == 3 && model.projimpact != 0) //systems
            });
        }
    } else if (grpBy == "account") {
        ctitle = "Impact Analysis by Facility"
        xAxisName = "Facility";
        if (impChkd == false) {
            var chartData = _.filter(data, function (model) {
                return (model.groupingid == 5) //facility
            });
        } else {
            var chartData = _.filter(data, function (model) {
                return (model.groupingid == 5 && model.projimpact != 0) //systems
            });
        }
    }

    //sort the chartData obj.
    chartData.sort(function (a, b) {
        return parseFloat(b.orderamt) - parseFloat(a.orderamt);
    });
    //prepare data for chart
    var grandTotal = _.filter(allData, function (gtemp) {
        return gtemp.groupingid == '7';
    });

    var category = [];
    var sale = [];
    var neg_projImpact = [];
    var pos_projImpact = [];
    var currencysgn = allData[0].currency;
    var icnt = 0;
    var dataLength = Object.keys(chartData).length
    var othSale = 0;
    var othSalesTot = 0;
    var othImpTot = 0;
    _.each(chartData, function (Sdata) {

        //fill the daaset
        icnt += 1;
        if (icnt < 18) {


            if (grpBy == "system" || grpBy == undefined || grpBy == "impacted") {
                category.push({
                    label: ((Sdata.systemname.length > 20) ? uniToString(Sdata.systemname.substr(0, 20)) + '...' : uniToString(Sdata.systemname))
                });
            } else {
                category.push({
                    label: ((Sdata.accountname.length > 20) ? Sdata.accountname.substr(0, 20) + '...' : Sdata.accountname)
                });
            };
            sale.push({
                value: (parseInt(Sdata.propprice) > parseInt(Sdata.orderamt)) ? Sdata.propprice : Sdata.orderamt
            });

            //calculate percentage to display
            if (Sdata.projimpact != 0) {
                var totValue = ((parseInt(Sdata.propprice) > parseInt(Sdata.orderamt)) ? parseFloat(Sdata.propprice) : parseFloat(Sdata.orderamt)) + parseFloat(Sdata.projimpact);
                var impPercent = (100 / (totValue / Sdata.projimpact)).toFixed(2);
            };

            neg_projImpact.push({
                value: (Sdata.projimpact < 0 ? Math.abs(Sdata.projimpact) : ''),
                displayValue: currencysgn + (Sdata.projimpact < 0 ? formatCurrency(Sdata.projimpact, 0) + " (" + impPercent.toString() + "%)" : '')
            });

            pos_projImpact.push({
                value: (Sdata.projimpact > 0 ? Sdata.projimpact : ''),
                displayValue: currencysgn + (Sdata.projimpact > 0 ? formatCurrency(Sdata.projimpact, 0) + " (" + impPercent.toString() + "%)" : '')
            });
        } else {

            othSale = othSale + ((parseFloat(Sdata.propprice) > parseFloat(Sdata.orderamt)) ? parseFloat(Sdata.propprice) : parseFloat(Sdata.orderamt));

            if (Sdata.projimpact != 0) {
                othSalesTot = othSalesTot + ((parseInt(Sdata.propprice) > parseInt(Sdata.orderamt)) ? parseFloat(Sdata.propprice) : parseFloat(Sdata.orderamt) + Math.abs(parseFloat(Sdata.projimpact)));
                othImpTot = othImpTot + parseFloat(Sdata.projimpact);
            };

            if (icnt == dataLength) {
                category.push({
                    label: "Others"
                });

                sale.push({
                    value: othSale
                });

                var othImpactPerc = (100 / (othSalesTot / othImpTot)).toFixed(2)
                if (othSalesTot != 0) {
                    if (othSalesTot < 0) {
                        neg_projImpact.push({
                            value: othSalesTot,
                            displayValue: currencysgn + (Sdata.projimpact < 0 ? formatCurrency(othImpTot, 0) + " (" + othImpactPerc.toString() + "%)" : '')
                        });
                    } else {
                        pos_projImpact.push({
                            value: (Sdata.projimpact > 0 ? Sdata.projimpact : ''),
                            displayValue: currencysgn + (Sdata.projimpact > 0 ? formatCurrency(Sdata.projimpact, 0) + " (" + othImpactPerc.toString() + "%)" : '')
                        });
                    }
                }


            };
        }


    });
    showImpactPopup();
    $("#msg-overlay").show();
    var templateImpactAnalysis = fnGetTemplate(URL_Price_Template, "gmtImpactAnalysis");
    var templateImpactAnalysisChart = fnGetTemplate(URL_Price_Template, "gmtImpactAnalysisChart");
    $("#div-price-overlay-content-container10 .modal-body").html(templateImpactAnalysis());
    $("#div-price-overlay-content-container10 .modal-body").find(".div-impact-analysis").html(templateImpactAnalysisChart());
    //Adding color to chart tab
    if (impChkd == true)
        $("#li-impacted-heading").prop("checked", true);
    $("#div-impact_by_chart").addClass("active");
    $("#div-impact-by-facility").removeClass("active");
    $("#div-impact_by_sytem").removeClass("active");

    //inside chart buttons hilight
    $("#li-system-heading").addClass("activebtn");
    $("#li-facility-heading").removeClass("activebtn");

    var requestId = allData[0].requestid;
    var orgName = allData[0].orgname;
    var titleRef = "Impact Analysis : " + requestId + " - " + orgName;


    $("#li-prev12-monthsale").find(".li-prev12-monthsale").html(currencysgn + Math.abs(grandTotal[0].orderamt).cformat());
    $("#li-proj-propprice").find(".li-proj-propprice").html(currencysgn + Math.abs(grandTotal[0].propprice).cformat());

    $("#li-projected-impact").find(".li-projected-impact").html(Math.abs(grandTotal[0].projimpact).cformat());
    var gimpactPerc = 0;

    //Projected impact data
    if (grandTotal[0].projimpact < 0) {
        $("#li-projected-impact").find(".li-projected-impact").html("(" + currencysgn + Math.abs(grandTotal[0].projimpact).cformat() +
            ")");
        $("#li-projected-impact").find(".li-projected-impact").css("color", "red");
    } else {
        $("#li-projected-impact").find(".li-projected-impact").html(currencysgn + Math.abs(grandTotal[0].projimpact).cformat());
    }

    //calculate the Project Impact% (Same formula as used to caculate for percent against each bar on chart)
    var baseTotal = ((parseInt(grandTotal[0].propprice) > parseInt(grandTotal[0].orderamt)) ? parseFloat(grandTotal[0].propprice) : parseFloat(grandTotal[0].orderamt)) + parseFloat(grandTotal[0].projimpact);
    gimpactPerc = (parseFloat(grandTotal[0].projimpact) / parseFloat(baseTotal)) * 100;
    gimpactPerc = gimpactPerc.toFixed(2);
    if (gimpactPerc < 0) {
        $("#li-project-impact").find(".li-project-impact").html("(" + Math.abs(gimpactPerc) + "%" + ")");
        $("#li-project-impact").find(".li-project-impact").css("color", "red");
    } else {
        $("#li-project-impact").find(".li-project-impact").html(gimpactPerc + "%");
    }

    $(".titleRef").html(titleRef);
    $(".li-account-name").html(orgName);


    //generate the chart
    var impactChart = new FusionCharts({
        type: 'stackedbar2d',
        renderAt: 'chart-container',
        width: '950',
        height: '465',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": ctitle,
                "xAxisname": xAxisName,
                "yAxisName": "Sales",
                "paletteColors": "#16b1e7,#ff0000,#82b74b",
                "borderColor": "#fff8e4",
                "bgColor": "#fff8e4",
                "borderAlpha": "20",
                "showCanvasBorder": "0",
                "usePlotGradientColor": "0",
                "plotBorderAlpha": "10",
                "legendBorderAlpha": "0",
                "legendShadow": "0",
                "showXAxisLine": "1",
                "xAxisLineColor": "#999999",
                "divlineColor": "#999999",
                "divLineDashed": "1",
                "showAlternateVGridColor": "0",
                "subcaptionFontBold": "1",
                "subcaptionFontSize": "16",
                "showHoverEffect": "1",
                "showValues": "0",
                "dataLoadStartMessage": "Loading chart. Please wait...",
                "useEllipsesWhenOverflow": "1",
                "valueFontBold": "1",
                "formatNumberScale": "0",
                "valueFontColor": "#0000ff",
                "canvasBgColor": "#ffffff",
                "numberPrefix": currencysgn,
                "showPercentInToolTip": "1",
                "legendItemFontBold": "1",
                "exportenabled": "1",
                "exportatclient": "1",
            },
            "categories": [
                {
                    "category": category

        }
    ],
            "dataset": [
                {
                    "seriesname": "Sale",
                    "data": sale
                },
                {
                    "seriesname": "-ve Impact",
                    "data": neg_projImpact,
                    "showValues": "1",

                },
                {
                    "seriesname": "+ve Impact",
                    "data": pos_projImpact,
                    "showValues": "1",
                }
            ]
        }
    }).render();


    //Chart navigation bar - System button
    $("#div-price-overlay-content-container10").find('#li-system-heading').unbind('click').bind('click', function (e) {
        fnImpactAnalysisCht(LoaderView, e, "system", data, requestId, parent);
        //inside chart buttons hilight
        $("#li-system-heading").addClass("activebtn");
        $("#li-facility-heading").removeClass("activebtn");
    });

    //Chart navigation bar - account button
    $("#div-price-overlay-content-container10").find('#li-facility-heading').unbind('click').bind('click', function (e) {
        fnImpactAnalysisCht(LoaderView, e, "account", data, requestId, parent);
        //inside chart buttons hilight
        $("#li-system-heading").removeClass("activebtn");
        $("#li-facility-heading").addClass("activebtn");
    });


    //impacted checkbox
    $("#div-price-overlay-content-container10").find('#li-impacted-heading').unbind('click').bind('click', function (e) {
        var selBtn = "system"
        if ($("#li-facility-heading").hasClass("activebtn"))
            selBtn = "account";
        fnImpactAnalysisCht(LoaderView, e, selBtn, data, requestId, parent);
        if (selBtn == "system") {
            $("#li-system-heading").addClass("activebtn");
            $("#li-facility-heading").removeClass("activebtn");
        } else {
            $("#li-system-heading").removeClass("activebtn");
            $("#li-facility-heading").addClass("activebtn");

        }
    });



    //Click on Chart (menu bar)
    $("#div-price-overlay-content-container10").find('#div-impact_by_chart').unbind('click').bind('click', function (e) {
        GM.Global.SortPrice = "ascending";
        fnImpactAnalysisCht(LoaderView, e, "system", data, requestId, parent);
        setTimeout(function () {
            GM.Global.loaderview.close();
            $('.li-impact-pdf-button').addClass("hide")
        }, 500);

    });


    //Click on impact by system
    $("#div-price-overlay-content-container10").find('#div-impact_by_sytem').unbind('click').bind('click', function (e) {
        GM.Global.loaderview = new LoaderView({
            text: "Loading..."
        });
        GM.Global.SortPrice = "ascending";
        fnImpactAnalysisRpt(LoaderView, e, "systemname", data, requestId, parent);
        setTimeout(function () {
            GM.Global.loaderview.close();
            $('.li-impact-pdf-button').removeClass("hide")

        }, 500);

    });

    //Click on impact by facility
    $("#div-price-overlay-content-container10").find('#div-impact-by-facility').unbind('click').bind('click', function (e) {
        GM.Global.loaderview = new LoaderView({
            text: "Loading..."
        });
        GM.Global.SortPrice = "ascending";
        fnImpactAnalysisRpt(LoaderView, e, "accountname", data, requestId, parent);
        setTimeout(function () {
            GM.Global.loaderview.close();
            $('.li-impact-pdf-button').removeClass("hide")
        }, 500);
    });

    //Impact analysys close
    $("#div-price-overlay-content-container10").find('.li-threshold-cancel-button').unbind('click').bind('click', function (e) {
        $("#msg-overlay").hide();
        GM.Global.SortPrice = "ascending";
        hideImpactPopup();
        $(".div-price-impact-btn").css("background-color", "#b2ffb2").css("color", "black");

    });
}

function fnImpactAnalysisRpt(LoaderView, e, grpBy, data, requestId, parent, flag, order) {
    var that = parent;
    var allData = data;
    //Impact analysis all fields now seperating based on groupingid
    console.log(allData);
    if (grpBy == undefined || grpBy == "systemname") {
        var sortData = _.filter(data, function (model) {
            return (model.groupingid == 3)
        });
        var grpGrouped = _.chain(sortData).map(function (model) {
            return model.systemname;
        }).uniq().value();
    } else {
        var sortData = _.filter(data, function (model) {
            return (model.groupingid == 5)
        });
        var grpGrouped = _.chain(sortData).map(function (model) {
            return model.accountname
        }).uniq().value();

    }
    if (GM.Global.Sorting == true) {
        if ($('.price-sort').find(".fa").hasClass("fa-arrow-down"))
            grpGrouped = grpGrouped.reverse()
        GM.Global.Sorting = false;
    }

    var partsJson = [];
    _.each(grpGrouped, function (Sdata) {

        if (grpBy == undefined || grpBy == "systemname") {
            var partsdata = _.filter(data, function (model) {
                return (model.systemname == Sdata && model.groupingid == 2);
            });
            var systemName = uniToString(Sdata);
            var price = _.filter(allData, function (model) {
                return (model.systemname == Sdata)
            });
            price = arrayOf(price);
            var systemidd = price[0].systemid;
            that.active = 'Y';
        } else {
            var partsdata = _.filter(data, function (model) {
                return (model.accountname == Sdata && model.groupingid == 4);
            });
            var systemName = uniToString(Sdata);
            var price = _.filter(allData, function (model) {
                return (model.accountname == Sdata)
            });
            price = arrayOf(price);
            var systemidd = price[0].accountid;
            that.active = 'N';
        }

        partsJson.push({
            name: systemName,
            partNumber: partsdata,
            systemid: systemidd
        });
    })

    var templateImpactAnalysisDetail = fnGetTemplate(URL_Price_Template, "gmtImpactAnalysisDetail");
    $("#div-price-overlay-content-container10 .modal-body").find(".div-impact-analysis").html(templateImpactAnalysisDetail(partsJson));
    if (GM.Global.SortPrice == "descending") {
        $('.price-sort').find(".fa").removeClass("fa-arrow-down").addClass("fa-arrow-up")
    } else if (GM.Global.SortPrice == "ascending") {
        $('.price-sort').find(".fa").removeClass("fa-arrow-up").addClass("fa-arrow-down")
    }

    // Change Button Background color in impact analysis
    if (that.active == 'Y') {
        $('#changeName').text('System Name');
        $('#changeName').attr('name', "systemname");
        $("#div-impact_by_sytem").addClass("active");
        $("#div-impact-by-facility").removeClass("active");
        $("#div-impact_by_chart").removeClass("active");
    } else {
        $('#changeName').text('Account Name');
        $('#changeName').attr('name', "accountname");
        $("#div-impact_by_sytem").removeClass("active");
        $("#div-impact-by-facility").addClass("active");
        $("#div-impact_by_chart").removeClass("active");
    }

    var alength = $(".ul-pricing-impact-content .div-impact-total").length

    _.each($(".ul-pricing-impact-content .div-impact-total"), function (temp) {
        var maindata = data;

        //System level total
        var systemTotal = _.filter(allData, function (a) {
            return a.groupingid == '3' && $(temp).attr("systemid") == a.systemid;
        });
        if (systemTotal.length > 0) {
            $(temp).find(".vtotsale").html(Math.abs(systemTotal[0].orderamt).cformat() + ".00");
            $(temp).find(".vtotprop").html(Math.abs(systemTotal[0].propprice).cformat() + ".00");
            if (systemTotal[0].projimpact < 0) {
                $(temp).find(".vtotproj").html("(" + Math.abs(systemTotal[0].projimpact).cformat() + ".00" + ")");
                $(temp).find(".vtotproj").css("color", "red");
            } else {
                $(temp).find(".vtotproj").css("color", "#000");
                $(temp).find(".vtotproj").html(Math.abs(systemTotal[0].projimpact).cformat() + ".00");
            }
        }
        //Facility account level total
        var facilityTotal = _.filter(allData, function (a) {
            return a.groupingid == '5' && $(temp).attr("systemid") == a.accountid;
        });
        if (facilityTotal.length > 0) {
            $(temp).find(".vtotsale").html(Math.abs(facilityTotal[0].orderamt).cformat() + ".00");
            $(temp).find(".vtotprop").html(Math.abs(facilityTotal[0].propprice).cformat() + ".00");
            if (facilityTotal[0].projimpact < 0) {
                $(temp).find(".vtotproj").html("(" + Math.abs(facilityTotal[0].projimpact).cformat() + ".00" + ")");
                $(temp).find(".vtotproj").css("color", "red");
            } else {
                $(temp).find(".vtotproj").css("color", "#000");
                $(temp).find(".vtotproj").html(Math.abs(facilityTotal[0].projimpact).cformat() + ".00");
            }
        }

        //Checking for positive / negative value to show red color
        _.each($(temp).find(".div-parts-impact-amt"), function (Vdata) {
            var partProjImpact = $(Vdata).find(".totproj").html();
            if (partProjImpact < 0) {
                $(Vdata).find(".totproj").html("(" + Math.abs(partProjImpact).cformat() + ".00" + ")");
                $(Vdata).find(".totproj").css("color", "red");
            } else {
                $(Vdata).find(".totproj").css("color", "#000");
                $(Vdata).find(".totproj").html(Math.abs(partProjImpact).cformat() + ".00");
            }

        });

    });

    //Grand total System/Facility Account
    var grandTotal = _.filter(allData, function (gtemp) {
        return gtemp.groupingid == '7';
    });
    if (grandTotal.length > 0) {
        $(".gmPriceTotalFooter").find(".gtotsale").html(Math.abs(grandTotal[0].orderamt).cformat() + ".00");
        $(".gmPriceTotalFooter").find(".gtotprop").html(Math.abs(grandTotal[0].propprice).cformat() + ".00");
        if (grandTotal[0].projimpact < 0) {
            $(".gmPriceTotalFooter").find(".gtotproj").html("(" + Math.abs(grandTotal[0].projimpact).cformat() + ".00" + ")");
            $(".gmPriceTotalFooter").find(".gtotproj").css("color", "red");
            //Info on header section
            $(".gtotprojs").html("(" + Math.abs(grandTotal[0].projimpact).cformat() + ".00" + ")");
            $(".gtotprojs").css("color", "red");
        } else {
            $(".gmPriceTotalFooter").find(".gtotproj").html(Math.abs(grandTotal[0].projimpact).cformat() + ".00");
            $(".gmPriceTotalFooter").find(".gtotproj").css("color", "white");
            //Info on header section
            $(".gtotprojs").html(Math.abs(grandTotal[0].projimpact).cformat() + ".00");
            $(".gtotprojs").css("color", "#000");
        }
    }
    //Header section information
    $(".gtotsales").html(Math.abs(grandTotal[0].orderamt).cformat() + ".00");
    $(".gtotprops").html(Math.abs(grandTotal[0].propprice).cformat() + ".00");
    var orgName = allData[0].orgname;
    var ADName = allData[0].adname;
    var genDate = strToDate(allData[0].gendate);
    console.log(genDate);

    var requestId = allData[0].requestid;
    var acCurrency = allData[0].currency;
    var titleRef = "Impact Analysis : " + requestId;

    $(".titleRef").html(titleRef);
    $(".orgName").html(orgName);
    $(".acCurrency").html(acCurrency);
    $(".genDate").html(genDate);
    $(".adName").html(ADName);
    var gimpactPerc = 0;

    //calculate the Project Impact % (Same formula as used to caculate for percent against each bar on chart)
    var baseTotal = ((parseFloat(grandTotal[0].propprice) > parseFloat(grandTotal[0].orderamt)) ? parseFloat(grandTotal[0].propprice) : parseFloat(grandTotal[0].orderamt)) + parseFloat(grandTotal[0].projimpact);
    gimpactPerc = (parseFloat(grandTotal[0].projimpact) / parseFloat(baseTotal)) * 100;
    gimpactPerc = gimpactPerc.toFixed(2);
    if (gimpactPerc < 0) {
        $(".impactperc").html("(" + Math.abs(gimpactPerc) + "%" + ")");
        $(".impactperc").css("color", "red");
    } else {
        $(".impactperc").html(gimpactPerc + "%");
    }

    $("#div-price-overlay-content-container10").removeClass("hide");
    $("#div-price-overlay-content-container10").find('.impact-analy-system-name').unbind('click').bind('click', function (e) {
        e.currentTarget = $(e.currentTarget).parent().find(".fa");
        $(".div-impact-content-plus").removeClass("hide")
        $(".div-impact-content-minus").addClass("hide")
        console.log($(e.currentTarget));
        if ($(e.currentTarget).hasClass("fa-plus-circle")) {
            $(e.currentTarget).parent().parent().parent().addClass("hide");
            $(e.currentTarget).parent().parent().parent().next().removeClass("hide");
        } else {
            $(e.currentTarget).parent().parent().parent().addClass("hide");
            $(e.currentTarget).parent().parent().parent().prev().removeClass("hide");
        }
    });

    $("#div-price-overlay-content-container10").find('.price-sort').unbind('click').bind('click', function (e) {
        GM.Global.Sorting = true;
        if ($('.price-sort').find(".fa").hasClass("fa-arrow-down")) {
            GM.Global.SortPrice = "descending";
        } else {
            GM.Global.SortPrice = "ascending";
        }
        fnImpactAnalysisRpt(LoaderView, e, grpBy, data, requestId, parent);
    });

    $("#div-price-overlay-content-container10").find(".li_pdf_document").unbind('click').bind('click', function (e) {
        console.log("PDF");
        var input = {
            "token": localStorage.getItem("token"),
            "refid": requestId,
            "accountname": $(".orgName").text(),
            "currency": $(".acCurrency").text(),
            "pre12mnthsale": $(".gtotsales").text(),
            "propprice": $(".gtotprops").text(),
            "projimpact": $(".gtotprojs").text(),
            "projimpatprpprc": $(".impactperc").text(),
            "adname": $(".adName").text(),
            "gendate": $(".genDate").text()
        };
        input.checkbtn = "pdf";

        fnGetWebServerData("impactAnalysis/sendImpactMail/", undefined, input, function (data) {
            showSuccess("PDF Generated...");
        });


    });

    $("#div-price-overlay-content-container10").find(".email-pdf").unbind('click').bind('click', function (e) {
        var input = {
            "token": localStorage.getItem("token"),
            "refid": requestId,
            "accountname": $(".orgName").text(),
            "currency": $(".acCurrency").text(),
            "pre12mnthsale": $(".gtotsales").text(),
            "propprice": $(".gtotprops").text(),
            "projimpact": $(".gtotprojs").text(),
            "projimpatprpprc": $(".impactperc").text(),
            "adname": $(".adName").text(),
            "gendate": $(".genDate").text()
        };
        //PDF mail process
        var templateMailImpactPdf = fnGetTemplate(URL_Price_Template, "gmtMailImpactPdf");
        showMailInfoPopup();
        $("#msg-overlay").show();
        $("#div-price-overlay-content-container13 .modal-body").html(templateMailImpactPdf());
        $("#div-price-overlay-content-container13").find('.modal-body .div-pdf-mail-btn').unbind('click').bind('click', function () {
            input.from = $("#div-rep-from-id input").val();
            input.cc = $("#div-rep-cc-id input").val();
            input.checkbtn = "mail";
            if ($("#div-rep-from-id input").val() == "" && $("#div-rep-cc-id input").val() == "") {
                showError("Please From/CC Mail");
            } else {
                fnGetWebServerData("impactAnalysis/sendImpactMail/", undefined, input, function (data) {
                    showSuccess("PDF Mail Sent...")
                });
            }
        });
        $("#div-price-overlay-content-container13").find('.modal-body .li-threshold-cancel-button').unbind('click').bind('click', function () {
            hideshowMailInfoPopup();
            $("#msg-overlay").hide();
        });
    });

}


function deleteUploadedfile(parent, prid, LoaderView, GMCFileUpload) {
    console.log("global")
    var that = parent;
    var gmcDeletedList = new GMCFileUpload();

    var deletedlist = _.filter(that.gmcFileUpload.toJSON(), function (model) {
        return (model.deletefl == "Y")
    });

    if (deletedlist.length > 0) {
        var jsnData = $.parseJSON(JSON.stringify(deletedlist));
        //var jsnData = JSON.stringify(deletedlist);
        gmcDeletedList.add(jsnData);


        console.log(deletedlist);
        console.log(gmcDeletedList);
        var input = {
            "gmCommonFileUploadVO": gmcDeletedList
        };

        fnGetWebServerData("fileUpload/deleteUploadedFiles", undefined, input, function (data) {
            showPriceFileUploadPopup(parent, prid, LoaderView, GMCFileUpload);

        }, true);
    } else {
        var rmv = confirm("Select files to be removed");
        if (rmv == true) {
            return
        }
    }

}

function showPriceFileUploadPopup(parent, prid, LoaderView, GMCFileUpload) {
    var that = parent;
    var refid = "";
    var refid = prid;
    var refid = refid.indexOf("PR-") >= 0 ? refid : "PR-" + refid;
    GM.Global.ID = refid;

    var input = {
        "refid": refid,
        "type": "26230731"

    };
    this.loaderview = new LoaderView({
        text: "Loading Document."
    });

    fnGetWebServerData("fileUpload/fetchFileList/", "gmCommonFileUploadVO", input, function (data) {
        that.loaderview.close();
        var adata = arrayOf(data);
        if (data != null) {
            $(".document_cnt").html(" (" + adata.length + ")");
            $(".document_cnt").css("color", "red");
        } else {
            $(".document_cnt").html("");
            $(".document_cnt").css("color", "black");
        }

        that.gmmFileUpload = adata;
        var jsnData = $.parseJSON(JSON.stringify(adata));
        that.gmcFileUpload.add(jsnData);


        if (!GM.Global.Device) {
            var userPartyId = window.localStorage.getItem("partyID");
            showFileUploadPopup();
            $("#msg-overlay").show();

            var template_picture = fnGetTemplate(URL_Common_Template, "gmtFileUploadDialog");
            if (data == null) {
                $("#div-price-overlay-content-container11").find('.modal-body').html(template_picture({
                    empty: "Y"
                }));
            } else {
                $("#div-price-overlay-content-container11").find('.modal-body').html(template_picture(adata));
            }

            if (localStorage.getItem("acid") < 3 && GM.Global.PricingStatus != "52186") {
                $("#div-price-delete-btn").addClass("hide");
                $("#div-price-delete-btn").parent().find("input").attr('disabled', 'disabled');
            }


            var userurl = $("#mobile-menu-top-user img").attr('src');

            var screenTitle = "Documents : " + refid;
            $(".screenTitle").html(screenTitle);

            $("#div-user-pic").append("<img class = 'image-user-pic'>");
            $(".image-user-pic").attr("src", userurl);

            $("#div-price-overlay-content-container11").find('.modal-body #input-user-picture').unbind('change').bind('change', function (e) {
                var file = e.target.files[0];
                var fileDisplayArea = document.getElementById('div-user-pic');
                var reader = new FileReader();

                reader.onload = function (event) {

                    // The file's text will be printed here
                    $("#div-price-overlay-content-container11").attr("src", event.target.result);
                    fnSelect_File(file, parent, prid, LoaderView, GMCFileUpload);

                };

                reader.readAsDataURL(file);

            });

            $("#div-price-overlay-content-container11 .ul-group-price-details").unbind('click').bind('click', function () {

                var info = [];
                _.each($(".ul-group-price-details"), function (adata) {
                    _.each(that.gmmFileUpload, function (Sdata) {
                        if ($(adata).find("#li-group-price-file-name").html() == Sdata.filename)
                            if ($(adata).find(".check-btn:checkbox:checked").length > 0) {
                                info.push({
                                    "fileid": Sdata.fileid,
                                    "deletefl": "Y",
                                    "updatedby": localStorage.getItem("userID")
                                });
                                console.log(Sdata.fileid);

                                that.gmcFileUpload.findWhere({
                                    "fileid": Sdata.fileid
                                }).set("deletefl", "Y").set("updatedby", localStorage.getItem("userID"));

                            } else {
                                that.gmcFileUpload.findWhere({
                                    "fileid": Sdata.fileid
                                }).set("deletefl", "").set("updatedby", "");
                            }
                    });

                })
            });


            console.log(GM.Global.fileDeleteData);

            $("#div-price-overlay-content-container11 .div-delete-btn").unbind('click').bind('click', function () {
                deleteUploadedfile(parent, prid, LoaderView, GMCFileUpload);
            });

            $("#div-price-overlay-content-container11 #div-price-file-upld-check-main").unbind('click').bind('click', function (e) {
                if ($(".li-group-price-top-header").find(".check-btn").prop('checked') == true)
                    $(".check-btn").prop('checked', true);
                else
                    $(".check-btn").prop('checked', false);
                $("#div-price-overlay-content-container11 .ul-group-price-details").trigger("click")
            });

            $("#div-price-overlay-content-container11").find('.modal-body .li-group-part-cancel-button, .div-price-group-close-btn').unbind('click').bind('click', function () {
                hideFileUploadPopup();

                $("#msg-overlay").hide();
            });

            $("#div-price-overlay-content-container11").find('.modal-body .div-upload-file-details #li-group-price-file-name').unbind('click').bind('click', function (e) {
                var path = $(e.currentTarget).html();
                if (URL_FileServer + "PRT/" + path != undefined && URL_FileServer + "/PRT/" + path != "")
                    window.open(URL_FileServer + "PRT/" + GM.Global.ID + "/" + path, '_blank', 'location=no');

                console.log(URL_FileServer + "PRT/" + GM.Global.ID + "/" + path);

            });
        } else
            showError("Network Connection is Required");

    });

}

/* to select file for uploading*/
function fnSelect_File(picfile, parent, prid, LoaderView, GMCFileUpload) {

    var that = this;

    if (!ValidateFileType(picfile.name)) {

        var rmv = confirm("Invalid upload file format. Cannot upload");
        if (rmv == true) {
            showPriceFileUploadPopup(parent, prid, LoaderView, GMCFileUpload);
            return;
        }
    }
    console.log(picfile.name);
    //clicking Upload button
    $("#div-price-overlay-content-container11 #update-image").unbind('click').bind('click', function () {

        var input = {
            "refid": GM.Global.ID,
            "token": localStorage.getItem("token"),
            "filename": picfile.name,
            "reftype": "26240010"
        };

        //checking if file exists
        fnGetWebServerData("fileUpload/checkFileExists/", "gmCommonFileUploadVO", input, function (data) {
            if (data != null && data.message == "Y") {

                var rmv = confirm("File exists. Do you want to overwrite?");
                if (rmv == true) {
                    fnUploadSelected_File(picfile, parent, prid, LoaderView, GMCFileUpload);
                    return
                } else {
                    showPriceFileUploadPopup(parent, prid, LoaderView, GMCFileUpload);
                }
            } else
                fnUploadSelected_File(picfile, parent, prid, LoaderView, GMCFileUpload);

        });
    });

}

function fnUploadSelected_File(picfile, parent, prid, LoaderView, GMCFileUpload) {
    var that = this;
    var $form = $("#frm-file-upload");
    var params = $form.serializeArray();
    this.picData = new FormData();

    this.picData.append('file', picfile, picfile.name);

    console.log($("#update-image").length);

    if (that.picData != "" && that.picData != undefined) {
        that.picData.append('refid', GM.Global.ID);
        that.picData.append('refgrp', '26240010'); //Documents
        that.picData.append('token', localStorage.getItem('token'));
        that.picData.append('type', '26230731'); //Pricing upload
        that.picData.append('fileid', '');
        that.picData.append('reftype', '26240010'); //documents
        console.log(that.picData);

        that.loaderview = new LoaderView({
            text: "Uploading File.Please wait....."
        });

        $.ajax({
            url: URL_WSServer + 'fileUpload/uploadFileInfo/',
            data: that.picData,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            success: function (result) {
                console.log("fileUpload/uploadFileInfo/ success >>> " + JSON.stringify(result));
                that.loaderview.close();
                showPriceFileUploadPopup(parent, prid, LoaderView, GMCFileUpload);
            },
            error: function (model, response, errorReport) {
                console.log("<<<<< UPLOAD FAILED. Check if URL_DOCUMENTServer is defined in GmServer.js  >>> " + JSON.stringify(model));
                var rmv = confirm("File upload failed. Contact administrator");
                if (rmv == true) {
                    return
                }
                that.loaderview.close();
            }
        });
    }

    $("#div-price-overlay-content-container11").find('.modal-body #input-file-upload  .li-group-part-cancel-button, .div-price-group-close-btn').unbind('click').bind('click', function () {
        hideFileUploadPopup();

        $("#msg-overlay").hide();
    });
}
/******** Click Sales Analysis button amd load load 12 month sale by default **********************************************************************************************************************/
function generateSalesAnalysis(e, parent, prid, LoaderView) {

    var that = parent;
    var requestid = "";
    requestid = prid;
    var repId = localStorage.getItem("userID");
    var token = localStorage.getItem("token");
    var userid = localStorage.getItem("userID");
    var input = {
        "token": "token",
        "refid": requestid,
        "userid": userid,
    };
    var loaderview = new LoaderView({
        text: "Loading Sales Analysis."
    });
    //webservice call to get impact analysis data
    fnGetWebServerData("salesAnalysis/fetchSalesAnalysis/", "gmSalesAnalysisVO", input, function (data) {
        console.log("data in sales");
        console.log(data);
        if (data == null) {
            loaderview.close();
            showError("No 12 month Sales Data Found");
            return;
        }
        data = arrayOf(data);
        loaderview.close();
        fnSalesAnalysisChart(e, parent, LoaderView, data, requestid);
    });
}

function fnSalesAnalysisChart(e, parent, LoaderView, data, requestid) {
    //    var allData = data;
    var selectData;
    var reRenderFl;
    var category = [];
    var sale = [];
    var allData = data[0].gmsalesAnalysisVO;
    var result = [];

    allData.reduce(function (res, value) {
        if (!res[value.month]) {
            res[value.month] = {
                salesamount: 0,
                month: value.month
            };
            result.push(res[value.month])
        }
        if (res[value.month] != undefined) {
            res[value.month].salesamount += parseInt(value.salesamount);
            res[value.month].monthyear = value.monthyear;
        }
        return res;
    }, {});

    _.each(result, function (dt) {
        category.push({
            label: dt.month
        });

        sale.push({
            label: dt.monthyear,
            value: dt.salesamount
        });
    });

    showSalesAnalysisPopup();
    $("#msg-overlay").show();
    var templateSalesAnalysis = fnGetTemplate(URL_Price_Template, "gmtPriceSalesReport");
    var templateSalesAnalysisChart = fnGetTemplate(URL_Price_Template, "gmtPriceSalesChart");
    $("#div-price-overlay-content-container-sales-analysis .modal-body").html(templateSalesAnalysis());
    $("#div-price-overlay-content-container-sales-analysis .modal-body").find(".div-sales-analysis-report").html(templateSalesAnalysisChart());
    var titleRef = "Sales Analysis : " + requestid;
    $(".titleRef").html(titleRef);

    var orgName, acCurrency, last12monthsales, genDate, partynm;
    if (data[0].gmPricingSalesHeaderVO.last12monthsales != undefined) {
        orgName = data[0].gmPricingSalesHeaderVO.orgname;
        acCurrency = data[0].gmPricingSalesHeaderVO.accurrency;
        last12monthsales = data[0].gmPricingSalesHeaderVO.last12monthsales;
        last12monthsales = acCurrency + " " + formatCurrency(last12monthsales);
        genDate = strToDate(data[0].gmPricingSalesHeaderVO.gendate);
        partynm = data[0].gmPricingSalesHeaderVO.partynm;
    } else {
        orgName = data[0].gmPricingSalesHeaderVO[0].orgname;
        acCurrency = data[0].gmPricingSalesHeaderVO[0].accurrency;
        last12monthsales = data[0].gmPricingSalesHeaderVO[0].last12monthsales;
        last12monthsales = acCurrency + " " + formatCurrency(last12monthsales);
        genDate = strToDate(data[0].gmPricingSalesHeaderVO[0].gendate);
        partynm = data[0].gmPricingSalesHeaderVO[0].partynm;
    }
    $(".orgName").html(orgName);
    $(".acCurrency").html(acCurrency);
    $(".genDate").html(genDate);
    $(".partynm").html(partynm);
    $(".gtotsales").html(last12monthsales);

    //generate the chart
    var salesAnalysisChart = new FusionCharts({
        type: 'column3d',
        renderAt: 'sales-analysis-chart-container',
        width: '900',
        height: '450',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "",
                "subCaption": "",
                "xAxisName": "",
                "yAxisName": "",
                "valueFontColor": "#00000",
                "valueFontBold": "1",
                "valueFontSize": "11",
                "baseFont": "Verdana,Helvetica Neue,Arial",
                "baseFontSize": "11",
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "placeValuesInside": "0",
                "rotateValues": "1",
                "showShadow": "0",
                "divlineColor": "#999999",
                "divLineIsDashed": "1",
                "divlineThickness": "1",
                "divLineDashLen": "1",
                "divLineGapLen": "1",
                "canvasBgColor": "#ffffff",
                "palettecolors": "#0075c2",
                "toolTipColor": "#ffffff",
                "toolTipBorderThickness": "0",
                "toolTipBgColor": "#000000",
                "toolTipBgAlpha": "80",
                "toolTipBorderRadius": "2",
                "toolTipPadding": "5"
            },

            "categories": [
                {
                    "category": category
                }
            ],
            "dataset": [
                {
                    "seriesname": "12MonthSales",
                    "data": sale
                },
            ]
        }
    }).render();
    //Click on account 
    $("#div-price-overlay-content-container-sales-analysis").find('#li-account-level').unbind('click').bind('click', function (e) {
        //inside chart buttons hilight
        $("#li-account-level").addClass("activeSalesLevel");
        $("#li-segment-level").removeClass("activeSalesLevel");
        fnSalesAccountLevelChart(e, parent, LoaderView, data, requestid, selectData, reRenderFl);
    });


    //Click on segment 
    $("#div-price-overlay-content-container-sales-analysis").find('#li-segment-level').unbind('click').bind('click', function (e) {
        //inside chart buttons hilight
        $("#li-segment-level").addClass("activeSalesLevel");
        $("#li-account-level").removeClass("activeSalesLevel");
        generateSegmentSales(e, parent, requestid, LoaderView);
    });

    //Sales analysys close
    $("#div-price-overlay-content-container-sales-analysis").find('.li-threshold-cancel-button').unbind('click').bind('click', function (e) {
        $("#msg-overlay").hide();
        hideSalesAnalysisPopup();
        $(".div-price-overlay-content-container-sales-analysis").css("background-color", "#b2ffb2").css("color", "black");
        $(".div-price-sales-analysis").css("background-color", "#b2ffb2").css("color", "black");

    });

    //click on Home button
    $("#div-price-overlay-content-container-sales-analysis").find('.fa-home-icon').unbind('click').bind('click', function (e) {
        //inside chart buttons hilight
        $("#li-segment-level").removeClass("activeSalesLevel");
        $("#li-account-level").addClass("activeSalesLevel");
        generateSalesAnalysis(e, parent, requestid, LoaderView);
    });

    //click on Back button
    $("#div-price-overlay-content-container-sales-analysis").find('.fa-arrow-left').unbind('click').bind('click', function (e) {
        $("#msg-overlay").hide();
        hideSalesAnalysisPopup();
        $(".div-price-overlay-content-container-sales-analysis").css("background-color", "#b2ffb2").css("color", "black");
        $(".div-price-sales-analysis").css("background-color", "#b2ffb2").css("color", "black");
    });
}

//loading account level details -Click By account level
function fnSalesAccountLevelChart(e, parent, LoaderView, data, requestid, selectData, reRenderFl) {

    var category = [];
    var sale = [];
    var allData = data[0].gmsalesAnalysisVO;
    var result = [];
    if (reRenderFl != undefined) {
        selectData = selectData[0].gmsalesAnalysisVO;
        selectData.reduce(function (res, value) {
            if (!res[value.partyid] && value.partyid != "") {
                res[value.partyid] = {
                    salesamount: 0,
                    partyid: value.partyid
                };
                result.push(res[value.partyid])
            }
            if (res[value.partyid] != undefined) {
                res[value.partyid].salesamount += parseInt(value.salesamount);
                res[value.partyid].partynm = value.partynm;
            }
            return res;
        }, {});
    } else {
        allData.reduce(function (res, value) {
            if (!res[value.partyid] && value.partyid != "") {
                res[value.partyid] = {
                    salesamount: 0,
                    partyid: value.partyid
                };
                result.push(res[value.partyid])
            }
            if (res[value.partyid] != undefined) {
                res[value.partyid].salesamount += parseInt(value.salesamount);
                res[value.partyid].partynm = value.partynm;
            }
            return res;
        }, {});
    }
    /*if the data is more than 12 then sum all the sales amount and show in others in Graph*/
    var icnt = 0;
    var dataLength = Object.keys(result).length;
    var otherSale = 0;
    //sort the result based on the sales amount.
    result = result.sort(function (a, b) {
        return parseFloat(b.salesamount) - parseFloat(a.salesamount);
    });

    _.each(result, function (Sdata) {
        //fill the dataset
        icnt += 1;
        if (icnt < 12) {
            category.push({
                label: Sdata.partynm
            });

            sale.push({
                value: Sdata.salesamount
            });

        } else {
            otherSale = otherSale + Sdata.salesamount;
            if (icnt == dataLength) {
                category.push({
                    label: "Others"
                });

                sale.push({
                    value: otherSale
                });
            }

        }
    });

    //for currency formatting in .hbs file
    result = _.each(result, function (data) {
        data.salesamount = data.salesamount.toString();
    });

    if (reRenderFl != undefined) {
        var templatedata = JSON.stringify(GM.Global.AccountCharts);
    } else {
        GM.Global.AccountCharts = result;
        var templatedata = JSON.stringify(result);
    }
    templatedata = arrayOf($.parseJSON(templatedata));

    showSalesAnalysisPopup();
    $("#msg-overlay").show();

    var templateSalesAnalysis = fnGetTemplate(URL_Price_Template, "gmtPriceSalesReport");
    var templateSalesAccountLevelChart = fnGetTemplate(URL_Price_Template, "gmtPriceAccountLevelChart");
    $("#div-price-overlay-content-container-sales-analysis .modal-body").html(templateSalesAnalysis());
    $("#div-price-overlay-content-container-sales-analysis .modal-body").find(".div-sales-account-level").html(templateSalesAccountLevelChart(templatedata));

    if (reRenderFl != undefined) {
        $("#div-sales-accountlevel-detail-content #li-acc-div-detail-id").each(function () {
            var partyID = $(this).attr("partyid");
            var currDiv = $(this).find("input");
            _.each(result, function (dt) {
                if (dt.partyid == partyID) {
                    $(currDiv).prop("checked", true);
                }
            });
        });
    } else {
        $("#div-sales-accountlevel-detail-content #li-acc-div-detail-id").each(function () {
            $(this).find("input").prop("checked", true);
        });
    }

    var titleRef = "Sales Analysis : " + requestid;
    $(".titleRef").html(titleRef);
    var orgName, acCurrency, last12monthsales, genDate, partynm;
    if (data[0].gmPricingSalesHeaderVO.last12monthsales != undefined) {
        orgName = data[0].gmPricingSalesHeaderVO.orgname;
        acCurrency = data[0].gmPricingSalesHeaderVO.accurrency;
        last12monthsales = data[0].gmPricingSalesHeaderVO.last12monthsales;
        last12monthsales = acCurrency + " " + formatCurrency(last12monthsales);
        genDate = strToDate(data[0].gmPricingSalesHeaderVO.gendate);
        partynm = data[0].gmPricingSalesHeaderVO.partynm;
    } else {
        orgName = data[0].gmPricingSalesHeaderVO[0].orgname;
        acCurrency = data[0].gmPricingSalesHeaderVO[0].accurrency;
        last12monthsales = data[0].gmPricingSalesHeaderVO[0].last12monthsales;
        last12monthsales = acCurrency + " " + formatCurrency(last12monthsales);
        genDate = strToDate(data[0].gmPricingSalesHeaderVO[0].gendate);
        partynm = data[0].gmPricingSalesHeaderVO[0].partynm;
    }
    $(".orgName").html(orgName);
    $(".acCurrency").html(acCurrency);
    $(".genDate").html(genDate);
    $(".partynm").html(partynm);
    $(".gtotsales").html(last12monthsales);
    var totalsales;
    totalsales = "Total Sales" + " " + acCurrency;
    $(".li-total-sales").html(totalsales);

    //generate the chart
    var SalesAccountLevelChart = new FusionCharts({
        type: 'msbar3d',
        renderAt: 'account-level-chart-container',
        width: '670',
        height: '450',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "",
                "subCaption": "",
                "yAxisname": "",
                "numberPrefix": acCurrency,
                "paletteColors": "#0075c2,#1aaf5d",
                "bgColor": "#ffffff",
                "legendBorderAlpha": "0",
                "legendBgAlpha": "0",
                "legendShadow": "0",
                "placevaluesInside": "0",
                "valueFontColor": "#00000",
                "valueFontBold": "1",
                "valueFontSize": "11",
                "baseFont": "Verdana,Helvetica Neue,Arial",
                "baseFontSize": "11",
                "alignCaptionWithCanvas": "1",
                "showHoverEffect": "1",
                "canvasBgColor": "#ffffff",
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "divlineColor": "#999999",
                "divLineIsDashed": "1",
                "divLineDashLen": "1",
                "divLineGapLen": "1",
                "showAlternateHGridColor": "0",
                "toolTipColor": "#ffffff",
                "toolTipBorderThickness": "0",
                "toolTipBgColor": "#000000",
                "toolTipBgAlpha": "80",
                "toolTipBorderRadius": "2",
                "toolTipPadding": "5",
                "scrollColor": "#3a4660",
                "scrollHeight": "10",
                "scrollPadding": "5",
                "defaultAnimation": '0'
            },
            "categories": [
                {
                    "category": category
                }
            ],
            "dataset": [
                {
                    "seriesname": "Accounts",
                    "data": sale
                },
            ]
        }
    }).render();
    $("#li-by-account-level").addClass("activeSalesLevel");

    //Click on by rep account in Account level
    $("#div-price-overlay-content-container-sales-analysis").find('#li-by-rep-account-level').unbind('click').bind('click', function (e) {
        //inside chart buttons hilight
        $("#li-by-rep-account-level").addClass("activeSalesLevel");
        $("#li-by-account-level").removeClass("activeSalesLevel");
        fnSalesRepAccountLevelChart(e, parent, LoaderView, data, requestid);
    });

    //Sales analysys close
    $("#div-price-overlay-content-container-sales-analysis").find('.li-threshold-cancel-button').unbind('click').bind('click', function (e) {
        $("#msg-overlay").hide();
        hideSalesAnalysisPopup();
        $(".div-price-overlay-content-container-sales-analysis").css("background-color", "#b2ffb2").css("color", "black");
        $(".div-price-sales-analysis").css("background-color", "#b2ffb2").css("color", "black");
    });

    //Click on GO button
    $("#div-price-overlay-content-container-sales-analysis").find('#li-Go-account-button').unbind('click').bind('click', function (e) {
        var selectedRslt = [];
        $("#div-sales-accountlevel-detail-content #li-acc-div-detail-id").each(function () {
            var partyId = $(this).attr("partyid");
            var salesamt = $(this).attr("salesamount");
            var partynm = $(this).attr("partynm");
            if ($(this).find("li input").prop("checked"))
                selectedRslt.push({
                    "salesamount": salesamt,
                    "partyid": partyId,
                    "partynm": partynm
                });
        });
        var selectData = {
            "gmsalesAnalysisVO": selectedRslt
        };
        var arraydata = {
            "0": selectData
        };
        var reRenderFl = "Y";
        fnSalesAccountLevelChart(e, parent, LoaderView, data, requestid, arraydata, reRenderFl);
    });

    //Click on Clear Button
    $("#div-price-overlay-content-container-sales-analysis").find('#li-clear-account-button').unbind('click').bind('click', function (e) {
        $("#div-sales-accountlevel-detail-content #li-acc-div-detail-id").each(function () {
            $(this).find("li input").prop("checked", false);
        });
        var reRenderFl = "Y";
        var selectedRslt = [];
        var selectData = {
            "gmsalesAnalysisVO": selectedRslt
        };
        var arraydata = {
            "0": selectData
        };

        fnSalesAccountLevelChart(e, parent, LoaderView, data, requestid, arraydata, reRenderFl);
    });

    //click on Home button
    $("#div-price-overlay-content-container-sales-analysis").find('.fa-home-icon').unbind('click').bind('click', function (e) {
        //inside chart buttons hilight
        $("#li-segment-level").removeClass("activeSalesLevel");
        $("#li-account-level").addClass("activeSalesLevel");
        generateSalesAnalysis(e, parent, requestid, LoaderView);
    });

    //click on Back button
    $("#div-price-overlay-content-container-sales-analysis").find('.fa-arrow-left').unbind('click').bind('click', function (e) {
        //inside chart buttons hilight
        $("#li-segment-level").removeClass("activeSalesLevel");
        $("#li-account-level").addClass("activeSalesLevel");
        generateSalesAnalysis(e, parent, requestid, LoaderView);
    });
}

//loading rep account level details -Click By rep account level
function fnSalesRepAccountLevelChart(e, parent, LoaderView, data, requestid, selectData, reRenderFl) {

    var category = [];
    var sale = [];
    var allData = data[0].gmsalesAnalysisVO;
    var result = [];

    if (reRenderFl != undefined) {
        selectData = selectData[0].gmsalesAnalysisVO;
        selectData.reduce(function (res, value) {
            if (!res[value.accountid] && value.accountid != "") {
                res[value.accountid] = {
                    salesamount: 0,
                    accountid: value.accountid
                };
                result.push(res[value.accountid])
            }
            if (res[value.accountid] != undefined) {
                res[value.accountid].salesamount += parseInt(value.salesamount);
                res[value.accountid].accountnm = value.accountnm;
            }
            return res;
        }, {});
    } else {
        allData.reduce(function (res, value) {
            if (!res[value.accountid] && value.accountid != "") {
                res[value.accountid] = {
                    salesamount: 0,
                    accountid: value.accountid
                };
                result.push(res[value.accountid])
            }
            if (res[value.accountid] != undefined) {
                res[value.accountid].salesamount += parseInt(value.salesamount);
                res[value.accountid].accountnm = value.accountnm;
            }
            return res;
        }, {});
    }
    /*if the data is more than 12 then sum all the sales amount and show in others in Graph*/
    var icnt = 0;
    var dataLength = Object.keys(result).length;
    var otherSale = 0;
    //sort the result based on the sales amount.
    result = result.sort(function (a, b) {
        return parseFloat(b.salesamount) - parseFloat(a.salesamount);
    });
    _.each(result, function (Sdata) {
        //fill the dataset
        icnt += 1;
        if (icnt < 12) {
            category.push({
                label: Sdata.accountnm
            });

            sale.push({
                value: Sdata.salesamount
            });

        } else {
            otherSale = otherSale + Sdata.salesamount;
            if (icnt == dataLength) {
                category.push({
                    label: "Others"
                });

                sale.push({
                    value: otherSale
                });
            }

        }
    });

    //for currency formatting in .hbs file
    result = _.each(result, function (data) {
        data.salesamount = data.salesamount.toString();
    });

    if (reRenderFl != undefined) {
        var templatedata = JSON.stringify(GM.Global.RepAccountCharts);
    } else {
        GM.Global.RepAccountCharts = result;
        var templatedata = JSON.stringify(result);
    }
    templatedata = arrayOf($.parseJSON(templatedata));
    showSalesAnalysisPopup();
    $("#msg-overlay").show();
    var templateSalesAnalysis = fnGetTemplate(URL_Price_Template, "gmtPriceSalesReport");
    var templateSalesRepAccountLevelChart = fnGetTemplate(URL_Price_Template, "gmtPriceRepAccountLevelChart");
    $("#div-price-overlay-content-container-sales-analysis .modal-body").html(templateSalesAnalysis());
    $("#div-price-overlay-content-container-sales-analysis .modal-body").find(".div-sales-rep-account-level").html(templateSalesRepAccountLevelChart(templatedata));
    if (reRenderFl != undefined) {
        $("#div-sales-accountlevel-detail-content #li-acc-div-detail-id").each(function () {
            var accountid = $(this).attr("accountid");
            var currDiv = $(this).find("input");
            _.each(result, function (dt) {
                if (dt.accountid == accountid) {
                    $(currDiv).prop("checked", true);
                }
            });
        });
    } else {
        $("#div-sales-accountlevel-detail-content #li-acc-div-detail-id").each(function () {
            $(this).find("input").prop("checked", true);
        });
    }

    var titleRef = "Sales Analysis : " + requestid;
    $(".titleRef").html(titleRef);
    var orgName, acCurrency, last12monthsales, genDate, partynm;
    if (data[0].gmPricingSalesHeaderVO.last12monthsales != undefined) {
        orgName = data[0].gmPricingSalesHeaderVO.orgname;
        acCurrency = data[0].gmPricingSalesHeaderVO.accurrency;
        last12monthsales = data[0].gmPricingSalesHeaderVO.last12monthsales;
        last12monthsales = acCurrency + " " + formatCurrency(last12monthsales);
        genDate = strToDate(data[0].gmPricingSalesHeaderVO.gendate);
        partynm = data[0].gmPricingSalesHeaderVO.partynm;
    } else {
        orgName = data[0].gmPricingSalesHeaderVO[0].orgname;
        acCurrency = data[0].gmPricingSalesHeaderVO[0].accurrency;
        last12monthsales = data[0].gmPricingSalesHeaderVO[0].last12monthsales;
        last12monthsales = acCurrency + " " + formatCurrency(last12monthsales);
        genDate = strToDate(data[0].gmPricingSalesHeaderVO[0].gendate);
        partynm = data[0].gmPricingSalesHeaderVO[0].partynm;
    }
    $(".orgName").html(orgName);
    $(".acCurrency").html(acCurrency);
    $(".genDate").html(genDate);
    $(".partynm").html(partynm);
    $(".gtotsales").html(last12monthsales);
    var totalsales;
    totalsales = "Total Sales" + " " + acCurrency;
    $(".li-total-sales").html(totalsales);

    //generate the chart
    var SalesAccountLevelChart = new FusionCharts({
        type: 'msbar3d',
        renderAt: 'rep-acc-level-chart-container',
        width: '670',
        height: '450',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "",
                "subCaption": "",
                "yAxisname": "",
                "numberPrefix": acCurrency,
                "paletteColors": "#0075c2,#1aaf5d",
                "bgColor": "#ffffff",
                "legendBorderAlpha": "0",
                "legendBgAlpha": "0",
                "legendShadow": "0",
                "placevaluesInside": "0",
                "valueFontColor": "#00000",
                "valueFontBold": "1",
                "valueFontSize": "11",
                "baseFont": "Verdana,Helvetica Neue,Arial",
                "baseFontSize": "11",
                "alignCaptionWithCanvas": "1",
                "showHoverEffect": "1",
                "canvasBgColor": "#ffffff",
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "divlineColor": "#999999",
                "divLineIsDashed": "1",
                "divLineDashLen": "1",
                "divLineGapLen": "1",
                "showAlternateHGridColor": "0",
                "toolTipColor": "#ffffff",
                "toolTipBorderThickness": "0",
                "toolTipBgColor": "#000000",
                "toolTipBgAlpha": "80",
                "toolTipBorderRadius": "2",
                "toolTipPadding": "5",
                "scrollColor": "#3a4660",
                "scrollHeight": "10",
                "scrollPadding": "5",
                "defaultAnimation": '0'
            },
            "categories": [
                {
                    "category": category
                }
            ],
            "dataset": [
                {
                    "seriesname": "RepAccounts",
                    "data": sale
                },
            ]
        }
    }).render();
    $("#li-by-rep-account-level2").addClass("activeSalesLevel");

    //Click on account in Account level
    $("#div-price-overlay-content-container-sales-analysis").find('#li-by-account-level2').unbind('click').bind('click', function (e) {
        //inside chart buttons hilight
        $("#li-by-account-level2").addClass("activeSalesLevel");
        $("#li-by-rep-account-level2").removeClass("activeSalesLevel");
        fnSalesAccountLevelChart(e, parent, LoaderView, data, requestid);
    });

    //Click on GO button
    $("#div-price-overlay-content-container-sales-analysis").find('#li-Go-repaccount-button').unbind('click').bind('click', function (e) {
        var selectedRslt = [];
        $("#div-sales-accountlevel-detail-content #li-acc-div-detail-id").each(function () {
            var accountid = $(this).attr("accountid");
            var salesamt = $(this).attr("salesamount");
            var accountnm = $(this).attr("accountnm");
            if ($(this).find("li input").prop("checked"))
                selectedRslt.push({
                    "salesamount": salesamt,
                    "accountid": accountid,
                    "accountnm": accountnm
                });
        });
        var selectData = {
            "gmsalesAnalysisVO": selectedRslt
        };
        var arraydata = {
            "0": selectData
        };
        var reRenderFl = "Y";
        fnSalesRepAccountLevelChart(e, parent, LoaderView, data, requestid, arraydata, reRenderFl);
    });

    //Click on Clear Button
    $("#div-price-overlay-content-container-sales-analysis").find('#li-clear-repaccount-button').unbind('click').bind('click', function (e) {
        $("#div-sales-accountlevel-detail-content #li-acc-div-detail-id").each(function () {
            $(this).find("li input").prop("checked", false);
        });
        var reRenderFl = "Y";
        var selectedRslt = [];
        var selectData = {
            "gmsalesAnalysisVO": selectedRslt
        };
        var arraydata = {
            "0": selectData
        };
        fnSalesRepAccountLevelChart(e, parent, LoaderView, data, requestid, arraydata, reRenderFl);
    });

    //Sales analysys close
    $("#div-price-overlay-content-container-sales-analysis").find('.li-threshold-cancel-button').unbind('click').bind('click', function (e) {
        $("#msg-overlay").hide();
        hideSalesAnalysisPopup();
        $(".div-price-overlay-content-container-sales-analysis").css("background-color", "#b2ffb2").css("color", "black");
        $(".div-price-sales-analysis").css("background-color", "#b2ffb2").css("color", "black");

    });

    //click on Home button
    $("#div-price-overlay-content-container-sales-analysis").find('.fa-home-icon').unbind('click').bind('click', function (e) {
        //inside chart buttons hilight
        $("#li-segment-level").removeClass("activeSalesLevel");
        $("#li-account-level").addClass("activeSalesLevel");
        generateSalesAnalysis(e, parent, requestid, LoaderView);
    });

    //click on Back button
    $("#div-price-overlay-content-container-sales-analysis").find('.fa-arrow-left').unbind('click').bind('click', function (e) {
        //inside chart buttons hilight
        $("#li-segment-level").removeClass("activeSalesLevel");
        $("#li-account-level").addClass("activeSalesLevel");
        generateSalesAnalysis(e, parent, requestid, LoaderView);
    });
}

/******** Click segment level inside the home sales dash ******************************************************************************************************************************/

function generateSegmentSales(e, parent, prid, LoaderView) {

    var that = parent;
    var requestid = "";
    requestid = prid;
    console.log(requestid);
    var repId = localStorage.getItem("userID");
    var token = localStorage.getItem("token");
    var userid = localStorage.getItem("userID");
    var input = {
        "token": "token",
        "refid": requestid,
        "userid": userid,
    };
    var loaderview = new LoaderView({
        text: "Loading Sales Segment."
    });
    //webservice call to get impact analysis data
    fnGetWebServerData("salesAnalysis/fetchSalesSegmentdetails/", "gmSalesAnalysisVO", input, function (data) {
        console.log("data in segment");
        console.log(data);
        if (data == null) {
            loaderview.close();
            showError("No Sales Segment Data Found");
            return;
        }
        data = arrayOf(data);
        loaderview.close();
        fnSegmentSalesChart(e, parent, prid, LoaderView, data);
    });
}

function fnSegmentSalesChart(e, parent, prid, LoaderView, data) {

    var category = [];
    var sale = [];
    var allData = data[0].gmsalesAnalysisVO;
    var currentSegment;
    var selectData;
    var reRenderFl;

    var result = [];
    allData.reduce(function (res, value) {
        if (!res[value.segmentid]) {
            res[value.segmentid] = {
                salesamount: 0,
                segmentid: value.segmentid
            };
            result.push(res[value.segmentid])
        }
        res[value.segmentid].salesamount += parseInt(value.salesamount);
        res[value.segmentid].segmentnm = value.segmentnm;
        return res;
    }, {});

    //sort the result based on the sales amount.
    result = result.sort(function (a, b) {
        return parseFloat(b.salesamount) - parseFloat(a.salesamount);
    });

    _.each(result, function (dt) {

        category.push({
            label: dt.segmentnm
        });

        sale.push({
            label: dt.segmentnm,
            value: dt.salesamount
        });
    });
    //for currency formatting in .hbs file
    result = _.each(result, function (data) {
        data.salesamount = data.salesamount.toString();
    });

    var templatedata = JSON.stringify(result);
    templatedata = arrayOf($.parseJSON(templatedata));
    showSalesAnalysisPopup();
    $("#msg-overlay").show();
    var templateSalesAnalysis = fnGetTemplate(URL_Price_Template, "gmtPriceSalesReport");
    var templateSalesAnalysisChart = fnGetTemplate(URL_Price_Template, "gmtPriceSegmentlevelSalesChart");
    $("#div-price-overlay-content-container-sales-analysis .modal-body").html(templateSalesAnalysis());
    $("#div-price-overlay-content-container-sales-analysis .modal-body").find(".div-sales-analysis-report").html(templateSalesAnalysisChart(templatedata));
    var titleRef = "Sales Analysis : " + prid;
    $(".titleRef").html(titleRef);

    var orgName, acCurrency, last12monthsales, genDate, partynm;
    if (data[0].gmPricingSalesHeaderVO.last12monthsales != undefined) {
        orgName = data[0].gmPricingSalesHeaderVO.orgname;
        acCurrency = data[0].gmPricingSalesHeaderVO.accurrency;
        last12monthsales = data[0].gmPricingSalesHeaderVO.last12monthsales;
        last12monthsales = acCurrency + " " + formatCurrency(last12monthsales);
        genDate = strToDate(data[0].gmPricingSalesHeaderVO.gendate);
        partynm = data[0].gmPricingSalesHeaderVO.partynm;
    } else {
        orgName = data[0].gmPricingSalesHeaderVO[0].orgname;
        acCurrency = data[0].gmPricingSalesHeaderVO[0].accurrency;
        last12monthsales = data[0].gmPricingSalesHeaderVO[0].last12monthsales;
        last12monthsales = acCurrency + " " + formatCurrency(last12monthsales);
        genDate = strToDate(data[0].gmPricingSalesHeaderVO[0].gendate);
        partynm = data[0].gmPricingSalesHeaderVO[0].partynm;
    }
    $(".orgName").html(orgName);
    $(".acCurrency").html(acCurrency);
    $(".genDate").html(genDate);
    $(".partynm").html(partynm);
    $(".gtotsales").html(last12monthsales);

    var totalsales;
    totalsales = "Revenue" + " " + acCurrency;
    $(".li-total-sales").html(totalsales);
    //generate the chart
    var segmentSalesChart = new FusionCharts({
        type: 'pie2d',
        renderAt: 'sales-analysis-chart-container',
        width: '700',
        height: '450',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "",
                "subCaption": "",
                "numbersuffix": "$",
                "paletteColors": "#0075c2,#1aaf5d,#f2c500,#f45b00,#8e0000",
                "bgColor": "#ffffff",
                "showBorder": "0",
                "use3DLighting": "0",
                "showShadow": "0",
                "baseFont": "Verdana,Helvetica Neue,Arial",
                "baseFontSize": "11",
                "enableSmartLabels": "0",
                "startingAngle": "0",
                "showPercentValues": "1",
                "showPercentInTooltip": "0",
                "decimals": "1",
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "toolTipColor": "#ffffff",
                "toolTipBorderThickness": "0",
                "toolTipBgColor": "#000000",
                "toolTipBgAlpha": "80",
                "toolTipBorderRadius": "2",
                "toolTipPadding": "5",
                "showHoverEffect": "1",
                "showLegend": "1",
                "legendBgColor": "#ffffff",
                "legendBorderAlpha": '0',
                "legendShadow": '0',
                "legendItemFontSize": '10',
                "legendItemFontColor": '#666666'
            },
            "categories": [
                {
                    "category": category
                }
            ],
            "dataset": [
                {
                    "data": sale
                },
            ]
        }
    }).render();
    $("#li-segment-level").addClass("activeSalesLevel");

    //Click on account
    $("#div-price-overlay-content-container-sales-analysis").find('#li-account-level').unbind('click').bind('click', function (e) {
        //inside chart buttons hilight
        $("#li-account-level").addClass("activeSalesLevel");
        $("#li-segment-level").removeClass("activeSalesLevel");
        fnSalesAccountLevelChart(e, parent, LoaderView, data, prid);
    });

    //click on Segment name in the segment chart container div
    $("#div-price-overlay-content-container-sales-analysis").find('.li-segment-dtl').unbind('click').bind('click', function (e) {
        //inside chart buttons hilight
        $("#li-account-level").addClass("activeSalesLevel");
        $("#li-segment-level").removeClass("activeSalesLevel");
        fnSalesSystemLevelChart(e, parent, prid, LoaderView, data, currentSegment, selectData, reRenderFl);
    });


    //Sales analysys close
    $("#div-price-overlay-content-container-sales-analysis").find('.li-threshold-cancel-button').unbind('click').bind('click', function (e) {
        $("#msg-overlay").hide();
        hideSalesAnalysisPopup();
        $(".div-price-overlay-content-container-sales-analysis").css("background-color", "#b2ffb2").css("color", "black");
        $(".div-price-sales-analysis").css("background-color", "#b2ffb2").css("color", "black");
    });

    //click on Home button
    $("#div-price-overlay-content-container-sales-analysis").find('.fa-home-icon').unbind('click').bind('click', function (e) {
        //inside chart buttons hilight
        $("#li-segment-level").removeClass("activeSalesLevel");
        $("#li-account-level").addClass("activeSalesLevel");
        generateSalesAnalysis(e, parent, prid, LoaderView);
    });

    //click on Back button
    $("#div-price-overlay-content-container-sales-analysis").find('.fa-arrow-left').unbind('click').bind('click', function (e) {
        //inside chart buttons hilight
        $("#li-segment-level").removeClass("activeSalesLevel");
        $("#li-account-level").addClass("activeSalesLevel");
        generateSalesAnalysis(e, parent, prid, LoaderView);
    });

}

//click on system name in segemnt level chart
function fnSalesSystemLevelChart(e, parent, prid, LoaderView, data, currentSegment, selectData, reRenderFl) {

    var category = [];
    var sale = [];
    var allData = data[0].gmsalesAnalysisVO;
    if (currentSegment == undefined) {
        currentSegment = $(e.currentTarget).find("#div-sales-segmntnm").attr("value");
    }
    var currentsystem;
    var currentsystemname;
    var currentsystemprice;

    var result = [];
    if (reRenderFl != undefined) {
        selectData = selectData[0].gmsalesAnalysisVO;
        selectData.reduce(function (res, value) {
            if (!res[value.setid]) {
                res[value.setid] = {
                    salesamount: 0,
                    setid: value.setid
                };
                result.push(res[value.setid])
            }
            if (res[value.setid] != undefined) {
                res[value.setid].salesamount += parseInt(value.salesamount);
                res[value.setid].setnm = value.setnm;
            }
            return res;
        }, {});
    } else {
        allData.reduce(function (res, value) {
            if (!res[value.setid] && currentSegment == value.segmentid) {
                res[value.setid] = {
                    salesamount: 0,
                    setid: value.setid
                };
                result.push(res[value.setid])
            }
            if (res[value.setid] != undefined) {
                res[value.setid].salesamount += parseInt(value.salesamount);
                res[value.setid].setnm = value.setnm;
            }
            return res;
        }, {});
    }

    /*if the data is more than 12 then sum all the sales amount and show in others in Graph*/
    var icnt = 0;
    var dataLength = Object.keys(result).length;
    var otherSale = 0;
    //sort the result based on the sales amount.
    result = result.sort(function (a, b) {
        return parseFloat(b.salesamount) - parseFloat(a.salesamount);
    });

    _.each(result, function (Sdata) {
        //fill the dataset
        icnt += 1;
        if (icnt < 12) {
            category.push({
                label: Sdata.setnm
            });

            sale.push({
                label: Sdata.setnm,
                value: Sdata.salesamount
            });

        } else {
            otherSale = otherSale + Sdata.salesamount;
            if (icnt == dataLength) {
                category.push({
                    label: "Others"
                });

                sale.push({
                    label: "Others",
                    value: otherSale
                });
            }

        }
    });

    //for currency formatting in .hbs file
    result = _.each(result, function (data) {
        data.salesamount = data.salesamount.toString();
    });

    if (reRenderFl != undefined) {
        var templatedata = JSON.stringify(GM.Global.systemcharts);
    } else {
        GM.Global.systemcharts = result;
        var templatedata = JSON.stringify(result);
    }
    templatedata = arrayOf($.parseJSON(templatedata));

    showSalesAnalysisPopup();
    $("#msg-overlay").show();
    var templateSalesAnalysis = fnGetTemplate(URL_Price_Template, "gmtPriceSalesReport");
    var templateSalesAnalysisChart = fnGetTemplate(URL_Price_Template, "gmtPriceSystemlevelSalesChart");
    $("#div-price-overlay-content-container-sales-analysis .modal-body").html(templateSalesAnalysis());
    $("#div-price-overlay-content-container-sales-analysis .modal-body").find(".div-sales-system-segment-level").html(templateSalesAnalysisChart(templatedata));
    var titleRef = "Sales Analysis : " + prid;
    $(".titleRef").html(titleRef);

    var orgName, acCurrency, last12monthsales, genDate, partynm;
    if (data[0].gmPricingSalesHeaderVO.last12monthsales != undefined) {
        orgName = data[0].gmPricingSalesHeaderVO.orgname;
        acCurrency = data[0].gmPricingSalesHeaderVO.accurrency;
        last12monthsales = data[0].gmPricingSalesHeaderVO.last12monthsales;
        last12monthsales = acCurrency + " " + formatCurrency(last12monthsales);
        genDate = strToDate(data[0].gmPricingSalesHeaderVO.gendate);
        partynm = data[0].gmPricingSalesHeaderVO.partynm;
    } else {
        orgName = data[0].gmPricingSalesHeaderVO[0].orgname;
        acCurrency = data[0].gmPricingSalesHeaderVO[0].accurrency;
        last12monthsales = data[0].gmPricingSalesHeaderVO[0].last12monthsales;
        last12monthsales = acCurrency + " " + formatCurrency(last12monthsales);
        genDate = strToDate(data[0].gmPricingSalesHeaderVO[0].gendate);
        partynm = data[0].gmPricingSalesHeaderVO[0].partynm;
    }
    $(".orgName").html(orgName);
    $(".acCurrency").html(acCurrency);
    $(".genDate").html(genDate);
    $(".partynm").html(partynm);
    $(".gtotsales").html(last12monthsales);

    var totalsales;
    totalsales = "Sales" + " " + acCurrency;
    $(".li-total-sales").html(totalsales);

    if (reRenderFl != undefined) {
        $("#div-sales-segment-detail-content #li-sales-seg-detail").each(function () {
            var setid = $(this).attr("setid");
            var currDiv = $(this).find("input");
            _.each(result, function (dt) {
                if (dt.setid == setid) {
                    $(currDiv).prop("checked", true);
                }
            });
        });
    } else {
        $("#div-sales-segment-detail-content #li-sales-seg-detail").each(function () {
            $(this).find("input").prop("checked", true);
        });
    }

    $(".orgName").html(orgName);
    $(".acCurrency").html(acCurrency);
    $(".genDate").html(genDate);
    $(".partynm").html(partynm);
    //generate the chart
    var segmentSalesChart = new FusionCharts({
        type: 'pie2d',
        renderAt: 'sales-systemlevel-chart-container',
        width: '600',
        height: '450',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "",
                "subCaption": "",
                "numbersuffix": "$",
                "paletteColors": "#0075c2,#1aaf5d,#f2c500,#f45b00,#8e0000",
                "bgColor": "#ffffff",
                "showBorder": "0",
                "use3DLighting": "0",
                "showShadow": "0",
                "baseFont": "Verdana,Helvetica Neue,Arial",
                "baseFontSize": "11",
                "enableSmartLabels": "0",
                "startingAngle": "0",
                "showPercentValues": "1",
                "showPercentInTooltip": "0",
                "decimals": "1",
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "toolTipColor": "#ffffff",
                "toolTipBorderThickness": "0",
                "toolTipBgColor": "#000000",
                "toolTipBgAlpha": "80",
                "toolTipBorderRadius": "2",
                "toolTipPadding": "5",
                "showHoverEffect": "1",
                "showLegend": "1",
                "legendBgColor": "#ffffff",
                "legendBorderAlpha": '0',
                "legendShadow": '0',
                "legendItemFontSize": '10',
                "legendItemFontColor": '#666666'
            },
            "categories": [
                {
                    "category": category
                }
            ],
            "dataset": [
                {
                    "data": sale
                },
            ]
        }
    }).render();

    $("#li-segment-level").addClass("activeSalesLevel");
    //Click on account
    $("#div-price-overlay-content-container-sales-analysis").find('#li-account-level').unbind('click').bind('click', function (e) {
        //inside chart buttons hilight
        $("#li-account-level").addClass("activeSalesLevel");
        $("#li-segment-level").removeClass("activeSalesLevel");
        fnSalesAccountLevelChart(e, parent, LoaderView, data, prid);
    });


    //Click on GO button
    $("#div-price-overlay-content-container-sales-analysis").find('#li-Go-system-button').unbind('click').bind('click', function (e) {
        var selectedRslt = [];
        $("#div-sales-segment-detail-content #li-sales-seg-detail").each(function () {
            var setid = $(this).attr("setid");
            var salesamt = $(this).attr("salesamount");
            var setnm = $(this).attr("setnm");
            if ($(this).find("li input").prop("checked"))
                selectedRslt.push({
                    "salesamount": salesamt,
                    "setid": setid,
                    "setnm": setnm
                });
        });
        var selectData = {
            "gmsalesAnalysisVO": selectedRslt
        };
        var arraydata = {
            "0": selectData
        };
        var reRenderFl = "Y";
        fnSalesSystemLevelChart(e, parent, prid, LoaderView, data, currentSegment, arraydata, reRenderFl)
    });

    //Click on Clear Button
    $("#div-price-overlay-content-container-sales-analysis").find('#li-clear-system-button').unbind('click').bind('click', function (e) {
        $("#div-sales-segment-detail-content #li-sales-seg-detail").each(function () {
            $(this).find("li input").prop("checked", false);
        });
        var reRenderFl = "Y";
        var selectedRslt = [];
        var selectData = {
            "gmsalesAnalysisVO": selectedRslt
        };
        var arraydata = {
            "0": selectData
        };
        fnSalesSystemLevelChart(e, parent, prid, LoaderView, data, currentSegment, arraydata, reRenderFl)
    });

    //click on Systen name in the segment chart container div
    $("#div-price-overlay-content-container-sales-analysis").find('.div-sysnm').unbind('click').bind('click', function (e) {
        //inside chart buttons hilight
        $("#li-account-level").addClass("activeSalesLevel");
        $("#li-segment-level").removeClass("activeSalesLevel");
        fnSystemAccountLevelChart(e, parent, LoaderView, data, prid, currentsystem, currentSegment, currentsystemname, currentsystemprice);
    });


    //Sales analysys close
    $("#div-price-overlay-content-container-sales-analysis").find('.li-threshold-cancel-button').unbind('click').bind('click', function (e) {
        $("#msg-overlay").hide();
        hideSalesAnalysisPopup();
        $(".div-price-overlay-content-container-sales-analysis").css("background-color", "#b2ffb2").css("color", "black");
        $(".div-price-sales-analysis").css("background-color", "#b2ffb2").css("color", "black");
    });

    //click on Home button
    $("#div-price-overlay-content-container-sales-analysis").find('.fa-home-icon').unbind('click').bind('click', function (e) {
        //inside chart buttons hilight
        $("#li-segment-level").removeClass("activeSalesLevel");
        $("#li-account-level").addClass("activeSalesLevel");
        generateSalesAnalysis(e, parent, prid, LoaderView);
    });

    //click on Back button
    $("#div-price-overlay-content-container-sales-analysis").find('.fa-arrow-left').unbind('click').bind('click', function (e) {
        //inside chart buttons hilight
        $("#li-segment-level").removeClass("activeSalesLevel");
        $("#li-account-level").addClass("activeSalesLevel");
        fnSegmentSalesChart(e, parent, prid, LoaderView, data);
    });
}

//loading account level details in system segment
function fnSystemAccountLevelChart(e, parent, LoaderView, data, requestid, currentsystem, currentSegment, currentsystemname, currentsystemprice) {

    var category = [];
    var sale = [];
    var allData = data[0].gmsalesAnalysisVO;
    var currentsystemprice;
    if (currentsystem == undefined) {
        currentsystem = $(e.currentTarget).attr("value");
        currentsystemname = $(e.currentTarget).attr("setnm");
        currentsystemprice = $(e.currentTarget).attr("salesamount");
    }
    var result = [];
    allData.reduce(function (res, value) {
        if (!res[value.partyid] && currentsystem == value.setid) {
            res[value.partyid] = {
                salesamount: 0,
                partyid: value.partyid
            };
            result.push(res[value.partyid])
        }
        if (res[value.partyid] != undefined && currentsystem == value.setid) {
            res[value.partyid].salesamount += parseInt(value.salesamount);
            res[value.partyid].partynm = value.partynm;
        }
        return res;
    }, {});

    /*if the data is more than 12 then sum all the sales amount and show in others in Graph*/
    var icnt = 0;
    var dataLength = Object.keys(result).length;
    var otherSale = 0;
    //sort the result based on the sales amount.
    result = result.sort(function (a, b) {
        return parseFloat(b.salesamount) - parseFloat(a.salesamount);
    });
    _.each(result, function (Sdata) {
        //fill the dataset
        icnt += 1;
        if (icnt < 12) {
            category.push({
                label: Sdata.partynm
            });

            sale.push({
                value: Sdata.salesamount
            });

        } else {
            otherSale = otherSale + Sdata.salesamount;
            if (icnt == dataLength) {
                category.push({
                    label: "Others"
                });

                sale.push({
                    value: otherSale
                });
            }

        }
    });

    var systemresult = [];
    systemresult.push({
        "salesamount": currentsystemprice,
        "setnm": currentsystemname
    });
    //for currency formatting in .hbs file
    systemresult = _.each(systemresult, function (data) {
        data.salesamount = data.salesamount.toString();
    });
    var templatedata = JSON.stringify(systemresult);
    templatedata = arrayOf($.parseJSON(templatedata));
    showSalesAnalysisPopup();
    $("#msg-overlay").show();

    var templateSalesAnalysis = fnGetTemplate(URL_Price_Template, "gmtPriceSalesReport");
    var templateSalesSystemAccountLevelChart = fnGetTemplate(URL_Price_Template, "gmtPriceSystemAccountLevelChart");
    $("#div-price-overlay-content-container-sales-analysis .modal-body").html(templateSalesAnalysis());
    $("#div-price-overlay-content-container-sales-analysis .modal-body").find(".div-sales-account-level").html(templateSalesSystemAccountLevelChart(templatedata));
    var titleRef = "Sales Analysis : " + requestid;
    $(".titleRef").html(titleRef);

    var orgName, acCurrency, last12monthsales, genDate, partynm;
    if (data[0].gmPricingSalesHeaderVO.last12monthsales != undefined) {
        orgName = data[0].gmPricingSalesHeaderVO.orgname;
        acCurrency = data[0].gmPricingSalesHeaderVO.accurrency;
        last12monthsales = data[0].gmPricingSalesHeaderVO.last12monthsales;
        last12monthsales = acCurrency + " " + formatCurrency(last12monthsales);
        genDate = strToDate(data[0].gmPricingSalesHeaderVO.gendate);
        partynm = data[0].gmPricingSalesHeaderVO.partynm;
    } else {
        orgName = data[0].gmPricingSalesHeaderVO[0].orgname;
        acCurrency = data[0].gmPricingSalesHeaderVO[0].accurrency;
        last12monthsales = data[0].gmPricingSalesHeaderVO[0].last12monthsales;
        last12monthsales = acCurrency + " " + formatCurrency(last12monthsales);
        genDate = strToDate(data[0].gmPricingSalesHeaderVO[0].gendate);
        partynm = data[0].gmPricingSalesHeaderVO[0].partynm;
    }

    $(".orgName").html(orgName);
    $(".acCurrency").html(acCurrency);
    $(".genDate").html(genDate);
    $(".partynm").html(partynm);
    $(".gtotsales").html(last12monthsales);

    var totalsales;
    totalsales = "Sales" + " " + acCurrency;
    $(".li-total-sales").html(totalsales);

    //generate the chart
    var SalesAccountLevelChart = new FusionCharts({
        type: 'msbar3d',
        renderAt: 'account-level-chart-container',
        width: '700',
        height: '450',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "",
                "subCaption": "",
                "yAxisname": "",
                "numberPrefix": acCurrency,
                "paletteColors": "#0075c2,#1aaf5d",
                "bgColor": "#ffffff",
                "legendBorderAlpha": "0",
                "legendBgAlpha": "0",
                "legendShadow": "0",
                "placevaluesInside": "0",
                "valueFontColor": "#00000",
                "valueFontBold": "1",
                "valueFontSize": "11",
                "baseFont": "Verdana,Helvetica Neue,Arial",
                "baseFontSize": "11",
                "alignCaptionWithCanvas": "1",
                "showHoverEffect": "1",
                "canvasBgColor": "#ffffff",
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "divlineColor": "#999999",
                "divLineIsDashed": "1",
                "divLineDashLen": "1",
                "divLineGapLen": "1",
                "showAlternateHGridColor": "0",
                "toolTipColor": "#ffffff",
                "toolTipBorderThickness": "0",
                "toolTipBgColor": "#000000",
                "toolTipBgAlpha": "80",
                "toolTipBorderRadius": "2",
                "toolTipPadding": "5",
                "scrollColor": "#3a4660",
                "scrollHeight": "10",
                "scrollPadding": "5",
                "defaultAnimation": '0'
            },
            "categories": [
                {
                    "category": category
                }
            ],
            "dataset": [
                {
                    "seriesname": "Accounts",
                    "data": sale
                },
            ]
        }
    }).render();
    $("#li-by-account-level").addClass("activeSalesLevel");

    //Sales analysys close
    $("#div-price-overlay-content-container-sales-analysis").find('.li-threshold-cancel-button').unbind('click').bind('click', function (e) {
        $("#msg-overlay").hide();
        hideSalesAnalysisPopup();
        $(".div-price-overlay-content-container-sales-analysis").css("background-color", "#b2ffb2").css("color", "black");
        $(".div-price-sales-analysis").css("background-color", "#b2ffb2").css("color", "black");

    });

    //Click on by rep account in system level
    $("#div-price-overlay-content-container-sales-analysis").find('#li-by-rep-account-level').unbind('click').bind('click', function (e) {
        //inside chart buttons hilight
        $("#li-by-rep-account-level").addClass("activeSalesLevel");
        $("#li-by-account-level").removeClass("activeSalesLevel");
        fnSystemRepAccountLevelChart(e, parent, LoaderView, data, requestid, currentsystem, currentSegment, currentsystemname, currentsystemprice);
    });

    //click on Home button
    $("#div-price-overlay-content-container-sales-analysis").find('.fa-home-icon').unbind('click').bind('click', function (e) {
        //inside chart buttons hilight
        $("#li-segment-level").removeClass("activeSalesLevel");
        $("#li-account-level").addClass("activeSalesLevel");
        generateSalesAnalysis(e, parent, requestid, LoaderView);
    });
    //click on Back button
    $("#div-price-overlay-content-container-sales-analysis").find('.fa-arrow-left').unbind('click').bind('click', function (e) {
        //inside chart buttons hilight
        $("#li-segment-level").removeClass("activeSalesLevel");
        $("#li-account-level").addClass("activeSalesLevel");
        fnSalesSystemLevelChart(e, parent, requestid, LoaderView, data, currentSegment);
    });

}


//loading rep account level details in system segment
function fnSystemRepAccountLevelChart(e, parent, LoaderView, data, requestid, currentsystem, currentSegment, currentsystemname, currentsystemprice) {

    var category = [];
    var sale = [];
    var allData = data[0].gmsalesAnalysisVO;

    var result = [];
    allData.reduce(function (res, value) {
        if (!res[value.accountid] && currentsystem == value.setid) {
            res[value.accountid] = {
                salesamount: 0,
                accountid: value.accountid
            };
            result.push(res[value.accountid])
        }
        if (res[value.accountid] != undefined && currentsystem == value.setid) {
            res[value.accountid].salesamount += parseInt(value.salesamount);
            res[value.accountid].accountnm = value.accountnm;
        }
        return res;
    }, {});

    /*if the data is more than 12 then sum all the sales amount and show in others in Graph*/
    var icnt = 0;
    var dataLength = Object.keys(result).length;
    var otherSale = 0;
    //sort the result based on the sales amount.
    result = result.sort(function (a, b) {
        return parseFloat(b.salesamount) - parseFloat(a.salesamount);
    });
    _.each(result, function (Sdata) {
        //fill the dataset
        icnt += 1;
        if (icnt < 12) {
            category.push({
                label: Sdata.accountnm
            });

            sale.push({
                value: Sdata.salesamount
            });

        } else {
            otherSale = otherSale + Sdata.salesamount;
            if (icnt == dataLength) {
                category.push({
                    label: "Others"
                });

                sale.push({
                    value: otherSale
                });
            }

        }
    });
    var systemresult = [];
    systemresult.push({
        "salesamount": currentsystemprice,
        "setnm": currentsystemname
    });
    //for currency formatting in .hbs file
    systemresult = _.each(systemresult, function (data) {
        data.salesamount = data.salesamount.toString();
    });
    var templatedata = JSON.stringify(systemresult);
    templatedata = arrayOf($.parseJSON(templatedata));
    showSalesAnalysisPopup();
    $("#msg-overlay").show();

    var templateSalesAnalysis = fnGetTemplate(URL_Price_Template, "gmtPriceSalesReport");
    var templateSalesRepAccountLevelChart = fnGetTemplate(URL_Price_Template, "gmtPriceSystemRepAccountLevelChart");
    $("#div-price-overlay-content-container-sales-analysis .modal-body").html(templateSalesAnalysis());
    $("#div-price-overlay-content-container-sales-analysis .modal-body").find(".div-sales-rep-account-level").html(templateSalesRepAccountLevelChart(templatedata));
    var titleRef = "Sales Analysis : " + requestid;
    $(".titleRef").html(titleRef);
    var orgName, acCurrency, last12monthsales, genDate, partynm;
    if (data[0].gmPricingSalesHeaderVO.last12monthsales != undefined) {
        orgName = data[0].gmPricingSalesHeaderVO.orgname;
        acCurrency = data[0].gmPricingSalesHeaderVO.accurrency;
        last12monthsales = data[0].gmPricingSalesHeaderVO.last12monthsales;
        last12monthsales = acCurrency + " " + formatCurrency(last12monthsales);
        genDate = strToDate(data[0].gmPricingSalesHeaderVO.gendate);
        partynm = data[0].gmPricingSalesHeaderVO.partynm;
    } else {
        orgName = data[0].gmPricingSalesHeaderVO[0].orgname;
        acCurrency = data[0].gmPricingSalesHeaderVO[0].accurrency;
        last12monthsales = data[0].gmPricingSalesHeaderVO[0].last12monthsales;
        last12monthsales = acCurrency + " " + formatCurrency(last12monthsales);
        genDate = strToDate(data[0].gmPricingSalesHeaderVO[0].gendate);
        partynm = data[0].gmPricingSalesHeaderVO[0].partynm;
    }
    $(".orgName").html(orgName);
    $(".acCurrency").html(acCurrency);
    $(".genDate").html(genDate);
    $(".partynm").html(partynm);
    $(".gtotsales").html(last12monthsales);

    var totalsales;
    totalsales = "Sales" + " " + acCurrency;
    $(".li-total-sales").html(totalsales);

    //generate the chart
    var SalesAccountLevelChart = new FusionCharts({
        type: 'msbar3d',
        renderAt: 'rep-acc-level-chart-container',
        width: '700',
        height: '450',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "",
                "subCaption": "",
                "yAxisname": "",
                "numberPrefix": acCurrency,
                "paletteColors": "#0075c2,#1aaf5d",
                "bgColor": "#ffffff",
                "legendBorderAlpha": "0",
                "legendBgAlpha": "0",
                "legendShadow": "0",
                "placevaluesInside": "0",
                "valueFontColor": "#00000",
                "valueFontBold": "1",
                "valueFontSize": "11",
                "baseFont": "Verdana,Helvetica Neue,Arial",
                "baseFontSize": "11",
                "alignCaptionWithCanvas": "1",
                "showHoverEffect": "1",
                "canvasBgColor": "#ffffff",
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "divlineColor": "#999999",
                "divLineIsDashed": "1",
                "divLineDashLen": "1",
                "divLineGapLen": "1",
                "showAlternateHGridColor": "0",
                "toolTipColor": "#ffffff",
                "toolTipBorderThickness": "0",
                "toolTipBgColor": "#000000",
                "toolTipBgAlpha": "80",
                "toolTipBorderRadius": "2",
                "toolTipPadding": "5",
                "scrollColor": "#3a4660",
                "scrollHeight": "10",
                "scrollPadding": "5",
                "defaultAnimation": '0'
            },
            "categories": [
                {
                    "category": category
                }
            ],
            "dataset": [
                {
                    "seriesname": "RepAccounts",
                    "data": sale
                },
            ]
        }
    }).render();
    $("#li-by-rep-account-level2").addClass("activeSalesLevel");

    //Click on account in system level
    $("#div-price-overlay-content-container-sales-analysis").find('#li-by-account-level2').unbind('click').bind('click', function (e) {
        //inside chart buttons hilight
        $("#li-by-account-level2").addClass("activeSalesLevel");
        $("#li-by-rep-account-level2").removeClass("activeSalesLevel");
        fnSystemAccountLevelChart(e, parent, LoaderView, data, requestid, currentsystem, currentSegment, currentsystemname, currentsystemprice);
    });

    //Sales analysys close
    $("#div-price-overlay-content-container-sales-analysis").find('.li-threshold-cancel-button').unbind('click').bind('click', function (e) {
        $("#msg-overlay").hide();
        hideSalesAnalysisPopup();
        $(".div-price-overlay-content-container-sales-analysis").css("background-color", "#b2ffb2").css("color", "black");
        $(".div-price-sales-analysis").css("background-color", "#b2ffb2").css("color", "black");

    });

    //click on Home button
    $("#div-price-overlay-content-container-sales-analysis").find('.fa-home-icon').unbind('click').bind('click', function (e) {
        //inside chart buttons hilight
        $("#li-segment-level").removeClass("activeSalesLevel");
        $("#li-account-level").addClass("activeSalesLevel");
        generateSalesAnalysis(e, parent, requestid, LoaderView);
    });

    //click on Back button
    $("#div-price-overlay-content-container-sales-analysis").find('.fa-arrow-left').unbind('click').bind('click', function (e) {
        //inside chart buttons hilight
        $("#li-segment-level").removeClass("activeSalesLevel");
        $("#li-account-level").addClass("activeSalesLevel");
        fnSalesSystemLevelChart(e, parent, requestid, LoaderView, data, currentSegment);
    });
}
