/**********************************************************************************
 * File:        GmvPricingApproval.js
 * Description: Pricing Approval  Screen
 * Version:     1.0
 * Author:     	Akumar
 **********************************************************************************/

define([
        'jquery', 'underscore', 'backbone', 'handlebars',
        'global', 'notification', 'dropdownview', 'searchview', 'gmtTemplate', 'loaderview', 'gmvCRMSelectPopup'
        ],

    function ($, _, Backbone, Handlebars,
        Global, Notification, DropDownView, SearchView, GMTTemplate, LoaderView, GMVCRMSelectPopup
    ) {

        'use strict';
        var tabid;
        var pricingApproval = Backbone.View.extend({

            events: {
                "click #fa-times-tick": "openApproval",
                "click .div-p-icon": "showGroupPartPricingDetails",
                "click #fa-price-circle": "collapsePrice",
                "change #price-approval-prop-unit input": "calcChangeExtPrice",
                "click .div-price-approval-btn-questions": "showThresholdQuestions",
                "click .div-price-aproval-account-val": "showPriceAccountDetails",
                "click .ul-approval-status": "checkValue",
                "change .div-price-approval-ul .ul-approval-status": "checkValidations",
                "change .ul-approval-changetype": "calcCustom",
                "click .price-approval-groupname": "fnGetGroupFullName",
                "click #div-price-approval-save": "saveApproval",
                "click #div-price-approval-cancel": "cancelApproval",
                "change .ul-approval-changetype , .price-approval-change input": "updateString",
                "change .select-price-aproval-system-main-val": "updateSystemStatus",
            },

            /* Load the templates */
//            templateGroupPartPricingDetails: fnGetTemplate(URL_Price_Template, "gmtGroupPartPricingDetails"),
            template_approval_system: fnGetTemplate(URL_Price_Template, "gmtPricingApprovalSystemData"),
            template_approval: fnGetTemplate(URL_Price_Template, "gmtPricingApprovalData"),
            templatePriceThresholdQusetions: fnGetTemplate(URL_Price_Template, "gmtThresholdQuestions"),
            templatePriceAccountDetails: fnGetTemplate(URL_Price_Template, "gmtPriceAccountDetails"),
//            templateShowName: fnGetTemplate(URL_Price_Template, "gmtShowName"),
            templatePriceReport: fnGetTemplate(URL_Price_Template, "gmtPriceReport"),

            initialize: function (options) {
                this.reqid = options.reqid;
                this.el = options.el;
                this.render();
            },

            render: function () {
                //                console.log($(e.currentTarget).parent().find("#div-price-request-id a").text());
                GM.Global.saveApprovalData = "";
//                this.$el.html(this.templatePriceReport());
                this.loaderview = new LoaderView({
                    text: "Loading ...!"
                });
                var input = {
                    "token": localStorage.getItem("token"),
                    "codegrp": "PRQSTS"
                };
                fnGetWebServerData("search/codelookup", "gmCodeLookUpVO", input, function (data) {
                    data = arrayOf(data);
                    GM.Global.priceApprovalDpdn = [];
                    var Sdata = _.filter(data, function (model) {
                        return (model.codeid != 52122 && model.codeid != 52189 && model.codeid != 52190 && model.codeid != 52121 && model.codeid != 52188);
                    })
                    _.each(Sdata, function (dssata) {
                        GM.Global.priceApprovalDpdn.push({
                            "ID": dssata.codeid,
                            "Name": dssata.codenm
                        })
                    })
                });
                var input = {
                    "token": localStorage.getItem("token"),
                    "codegrp": "PCHTY"
                };
                fnGetWebServerData("search/codelookup", "gmCodeLookUpVO", input, function (datas) {
                    GM.Global.priceType = []
                    var dsata = arrayOf(datas);
                    _.each(dsata, function (dataa) {
                        GM.Global.priceType.push({
                            "ID": dataa.codeid,
                            "Name": dataa.codenm
                        })
                    })
                });
                var input = {
                    "pricerequestid": this.reqid,
                    "userid": localStorage.getItem("userID"),
                    "deptid": localStorage.getItem("deptid")
                };
                var that = this;
                fnGetWebServerData("pricingApproval/fetchPriceApprovalDetails/", undefined, input, function (data) {
                    console.log(data);
                    data.listGmSystemVO = arrayOf(data.listGmSystemVO);
                    that.loaderview.close();
                   that.$el.html(that.template_approval_system(data.listGmSystemVO));
                    $(".div-price-status-val").html(data.gmAccountPriceRequestVO.requestStatus)
                    $(".div-price-aproval-account-val").attr("typeid", data.gmAccountPriceRequestVO.typeid)
                    $(".li-price-reqid-val").html(data.listgmPricingApprovalHeaderVO.pricerequestid)
                    $(".div-price-initiated-val").html(data.listgmPricingApprovalHeaderVO.initiatedby)
                    $(".div-price-sales-val").html(data.listgmPricingApprovalHeaderVO.last12monthsales)
                    $(".div-price-date-val").html(data.listgmPricingApprovalHeaderVO.initiateddate)
                    $(".div-price-aproval-account-val").attr("name", data.listgmPricingApprovalHeaderVO.accountid)
                    _.each(data.listGmSystemVO, function (sata) {
                        var mod = _.filter(data.listGmGroupVO, function (model) {
                            return (model.systemid == sata.systemId);
                        });
                        mod = _.sortBy(mod, 'setordcnt');
                        var grpConstructId = _.chain(mod).map(function (model) {
                            return model.systemname;
                        }).uniq().value();
                        _.each(grpConstructId, function (name) {
                            var len = _.filter(data.listGmGroupVO, function (model) {
                                return (model.systemname == name && model.systemid == sata.systemId);
                            });
                            if (len.length > 0)
                                $("#approval-" + sata.systemName).append(that.template_approval(len));
                            that.loadSelect();
                            _.each(data.listGmGroupVO, function (temp) {
                                if (temp.systemname == name && temp.systemid == sata.systemId) {
                                    $("." + temp.groupid).find(".price-approval-type").val(temp.changetype)
                                    $("." + temp.groupid).find(".ul-approval-status").val(temp.statusid)
                                    $("." + temp.groupid).find(".ul-price-approval-prop-ext").text((temp.quantity * temp.proposedunitprice).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"))
                                    var listext = temp.quantity * temp.listprice;
                                    listext = listext.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")
                                    $("." + temp.groupid).find(".price-approval-listext").text(listext)
                                    $("." + temp.groupid).find(".price-approval-offlist").html((Math.round(((100 * (temp.listprice - temp.proposedunitprice) / temp.listprice) * 1000) / 10) / 100).toFixed(2))
                                    if (eval($("." + temp.groupid).find("#price-approval-change").val().replace(",", "")) < eval($("." + temp.groupid).find(".price-approval-tripwire").text().replace(",", ""))) {
                                        $("." + temp.groupid).find(".ul-price-approval-color").removeClass("green").addClass("red")
                                    } else {

                                        $("." + temp.groupid).find(".ul-price-approval-color").removeClass("red").addClass("green")
                                    }
                                }
                            });
                            that.calcConstructPrice();
                            $('.select-price-aproval-system-main-val').val(data.listGmGroupVO[0].statusid);
                            $(".div-price-approval-collapse").hide();
                        });
                    })
                });
            },
            showGroupPartPricingDetails: function (e) {
                showGroupPartPricingDetails(e);
                console.log("showGroupPartPricingDetails");
                console.log(e);
            },
            updateSystemStatus: function (e) {
                 console.log($(e.currentTarget).val());
                 var that = this;
                console.log($(e.currentTarget).val());
                if (localStorage.getItem("pr_status_upd_access") != "Y") {
                    showError("Not Authorised..");
                    $(e.currentTarget).val(GM.Global.previousValue);
                    $('.ul-approval-status').attr('disabled', 'disabled');
                    $('.ul-approval-changetype').attr('disabled', 'disabled');
                    $(".div-price-approval-ul input").attr('disabled','disabled');
                    
                } else {
                    if ($(e.currentTarget).val() == "52125") {
                        if (localStorage.getItem("pr_apprvl_access") != "Y") {
                            showError("Not Authorised..");
                            $(e.currentTarget).val(GM.Global.previousValue);
                             
                           
                        } else {
//                            $(e.currentTarget).parent().find("input").attr('disabled','disabled');
//                            $(e.currentTarget).parent().find('select').attr('disabled', 'disabled');
//                            $(e.currentTarget).attr('disabled', false);
                            
                            $(e.currentTarget).parent().parent().next().find(".ul-approval-status").val($(e.currentTarget).val()).trigger("change");
                                                    }

                    } else if ($(e.currentTarget).val() == "52120") {
                        
                        if (localStorage.getItem("pr_denied_access") != "Y") {
                            showError("Not Authorised..");
                             
                            $(e.currentTarget).val(GM.Global.previousValue);
                            
                        } else {
                            
                            $(e.currentTarget).parent().parent().next().find(".ul-approval-status").val($(e.currentTarget).val()).trigger("change");
                                                    }
                    }
                    else if($(e.currentTarget).val() == "52123"){
                         if (localStorage.getItem("pr_pc_access") != "Y") {
                            showError("Not Authorised..");
                              
                            $(e.currentTarget).val(GM.Global.previousValue);
                        } else {
                            $(e.currentTarget).parent().parent().next().find(".ul-approval-status").val($(e.currentTarget).val()).trigger("change");
                                                    }
                    }
                    else{
                        
                        $(e.currentTarget).parent().parent().next().find(".ul-approval-status").val($(e.currentTarget).val()).trigger("change");
                                            }                     
                }
            },
            collapsePrice: function (e) {
                $(".div-price-approval-collapse").hide();
                if ($(e.currentTarget).hasClass("fa-plus-circle")) {
                    $(".fa-minus-circle").removeClass("fa-minus-circle").addClass("fa-plus-circle");
                    $(e.currentTarget).removeClass("fa-plus-circle").addClass("fa-minus-circle").css("color:green");
                    $(e.currentTarget).parent().parent().parent().parent().find("#approval-" + $(e.currentTarget).attr("name")).show();
                } else {
                    $(e.currentTarget).removeClass("fa-minus-circle").addClass("fa-plus-circle").css("color:red");
                    $(e.currentTarget).parent().parent().parent().parent().find("#approval-" + $(e.currentTarget).attr("name")).hide();
                }
            },
            calcCustom: function (e) {
               if($(e.currentTarget).val()=="52020")
                   $(e.currentTarget).parent().find("#price-approval-changevalue input").val("");
            },
            saveApproval: function () {
                var that = this;
                var input = {
                    "token": localStorage.getItem("token"),
                    "inputstr":GM.Global.saveApprovalData,
                    "pricerequestid": $(".li-price-reqid-val").html(),
                    "implementstring": "",
                    "userid": localStorage.getItem("userID")
                };
                console.log(input);
                fnGetWebServerData("pricingApproval/updatePriceApprovalDetails", undefined, input, function (data) {
                    console.log(data);
                    showSuccess($(".li-price-reqid-val").html()+" saved Successfully ..!")
                    that.reqid = $(".li-price-reqid-val").html();
                    that.render();
                });
            },
            cancelApproval: function () {
                Backbone.history.loadUrl("#price/report");
            },
            updateString: function (e) {
                console.log(GM.Global.saveApprovalData);
                var data;
                switch ($(e.currentTarget).attr("case")) {
                    case "select":
                        data = $(e.currentTarget).parent()
                        break;
                    case "input":
                        data = $(e.currentTarget).parent().parent()
                        break;
                }
                var inputstr = $(data).attr("systemid") + "^" + $(data).find(".ul-approval-status").val() + "^" + $(data).find(".ul-approval-changetype").val() + "^" + $(data).attr("groupid") + "^" + $(data).find("#price-approval-changevalue input").val() + "^" + $(data).find("#price-approval-prop-unit input").val().replace(",", "") + "^" + "N" + "^" + $(data).attr("prcdetlid");
                if (GM.Global.saveApprovalData != "")
                    GM.Global.saveApprovalData = GM.Global.saveApprovalData + inputstr + "|"
                else
                    GM.Global.saveApprovalData = inputstr + "|";
                console.log(GM.Global.saveApprovalData);
            },
            loadSelect: function (e) {
                _.each($(".approval-construct"), function (data) {
                    $(data).html($(data).parent().prev().attr("id"))
                })
                $('.ul-approval-status').each(function () {
                    if ($(this).val() == null) {
                        $(this).html("");
                        fnCreateOptions(this, GM.Global.priceApprovalDpdn);
                        $(this).val($(this).attr("value"));
                    }

                });
                _.each($('.ul-approval-changetype'), function (hata) {
                    if ($(hata).val() == null) {
                        $(hata).html("");
                        fnCreateOptions(hata, GM.Global.priceType);
                        $(hata).val($(hata).attr("value"));
                    }
                });
            },
            calcConstructPrice: function (e) {
                _.each($(".div-price-aproval-group-name"), function (iter) {
                    var propval = "";
                    var listval = "";
                    _.each($(iter).find("div"), function (tData) {
                        if ($(tData).hasClass("div-price-approval-ul")) {
                            if (propval == "") {
                                propval = eval($(tData).find(".ul-price-approval-prop-ext").html().replace(",", ""));
                                listval = eval($(tData).find(".price-approval-listext").html().replace(",", ""));
                            } else {
                                propval = propval + eval($(tData).find(".ul-price-approval-prop-ext").html().replace(",", ""));
                                listval = eval(listval) + eval($(tData).find(".price-approval-listext").html().replace(",", ""));
                            }
                        }
                        if ($(tData).hasClass("div-price-aproval-contruct")) {
                            $(tData).find(".price-approval-total-prop-ext").html(propval.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"))
                            $(tData).find(".price-approval-total-list-ext").html(listval.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"))
                        }
                    })
                })
            },
            calcChangeExtPrice: function (e) {
                $(e.currentTarget).parent().parent().find(".ul-price-approval-prop-ext").html($(e.currentTarget).val().replace(",", "") * $(e.currentTarget).parent().parent().find(".price-approval-qty").html())
                if ($(e.currentTarget).parent().parent().find("#price-approval-change").val() != "" && $(e.currentTarget).parent().parent().find("#price-approval-change").val() != undefined && $(e.currentTarget).parent().parent().find(".price-approval-tripwire").text() != "" && $(e.currentTarget).parent().parent().find(".price-approval-tripwire").text() != undefined) {
                    if (eval($(e.currentTarget).parent().parent().find("#price-approval-change").val().replace(",", "")) < eval($(e.currentTarget).parent().parent().find(".price-approval-tripwire").text().replace(",", "")))
                        $(e.currentTarget).parent().parent().find(".ul-price-approval-color").removeClass("green").addClass("red")

                    else
                        $(e.currentTarget).parent().parent().find(".ul-price-approval-color").removeClass("red").addClass("green")

                }
                this.calcConstructPrice();

            },

            fnGetGroupFullName: function (e) {
                fnGetGroupFullName(e);
            },
            checkValidations: function (e) {
                var that = this;
                console.log($(e.currentTarget).val());
                if (localStorage.getItem("pr_status_upd_access") != "Y") {
                    showError("Not Authorised..");
                    $(e.currentTarget).val(GM.Global.previousValue);
                    $('.ul-approval-status').attr('disabled', 'disabled');
                    $('.ul-approval-changetype').attr('disabled', 'disabled');
                    $(".div-price-approval-ul input").attr('disabled','disabled');
                    
                } else {
                    if ($(e.currentTarget).val() == "52125") {
                        if (localStorage.getItem("pr_apprvl_access") != "Y") {
                            showError("Not Authorised..");
                            $(e.currentTarget).val(GM.Global.previousValue);
                             
                           
                        } else {
                            $(e.currentTarget).parent().find("input").attr('disabled','disabled');
                            $(e.currentTarget).parent().find('select').attr('disabled', 'disabled');
                            $(e.currentTarget).prop('disabled', false);
//                            $(e.currentTarget).parent().find('select').attr('disabled', 'disabled');
                            that.updateString(e);
                                                    }

                    } else if ($(e.currentTarget).val() == "52120") {
                        
                        if (localStorage.getItem("pr_denied_access") != "Y") {
                            showError("Not Authorised..");
                             
                            $(e.currentTarget).val(GM.Global.previousValue);
                            
                        } else {
                             $(e.currentTarget).parent().find("input").prop('disabled', false);
                            $(e.currentTarget).parent().find('select').prop('disabled', false);
                            that.updateString(e);
                                                    }
                    }
                    else if($(e.currentTarget).val() == "52123"){
                         if (localStorage.getItem("pr_pc_access") != "Y") {
                            showError("Not Authorised..");
                              
                            $(e.currentTarget).val(GM.Global.previousValue);
                        } else {
                            
                              $(e.currentTarget).parent().find("input").prop('disabled', false);
                            $(e.currentTarget).parent().find('select').prop('disabled', false);
                            that.updateString(e);
                                                    }
                    }
                    else{
                        
                        $(e.currentTarget).parent().find("input").prop('disabled', false);
                            $(e.currentTarget).parent().find('select').prop('disabled', false);
                        that.updateString(e);
                                            }                     
                }

            },
            checkValue: function (e) {
                GM.Global.previousValue = $(e.currentTarget).val();
            },
            showThresholdQuestions: function (e) {
                
//                showThresholdQuestions(e);
                var that = this;
                var input = {
                    "token": "token",
                    "requestid": $(".li-price-reqid-val").html()
                };
                fnGetWebServerData("pricingRequest/thresholdQuestions/", undefined, input, function (data) {
                    showInfoPopup();
                    $("#msg-overlay").show();
                    $("#div-price-overlay-content-container5 .modal-body").html(that.templatePriceThresholdQusetions(data.gmThresholdQuestionVO));
                    $("#div-threshold-submit").hide();
                    /*Consultant drpdwn*/
                    that.getConsultantDrp(data.gmThresholdQuestionVO);
                    _.each(data.gmThresholdQuestionVO, function (r) {
                        if (r.dropdownid != "")
                            that.consultantId = r.dropdownid;
                    })
                    $("#div-price-overlay-content-container5").find('.modal-body .li-threshold-cancel-button').unbind('click').bind('click', function () {
                        hideInfoPopup();
                        $("#msg-overlay").hide();
                    });
                    $("#div-price-overlay-content-container5").find('.modal-body #fa-close-alert').unbind('click').bind('click', function () {
                        hideMessages();
                    });


                });


            },
            getConsultantDrp: function (data) {
                var that = this;
                var defaultid = 0;
                var defaultname = "Choose One";
                var input = {
                    "codeGrp": "YES/NO",
                    "token": localStorage.getItem("token")
                };
                fnGetCodeLookUpValues(input, function (drpDwn) {
                    drpDwn.push({
                        "ID": defaultid,
                        "Name": defaultname
                    });
                    if (data[0].dropdownid != "" && data[0].dropdownvalue != "") {
                        defaultid = data[0].dropdownid;
                        defaultname = data[0].dropdownvalue;
                    }
                    that.consultantdropdownview = new DropDownView({
                        el: $("#div-price-overlay-content-container5 .modal-body #li-pricing-dropdown"),
                        data: drpDwn,
                        DefaultID: defaultid
                    });
                    $("#div-price-overlay-content-container5 .modal-body #li-pricing-dropdown").bind('onchange', function (e, selectedId, SelectedName) {
                        that.consultantId = selectedId;
                        that.consultantName = SelectedName;
                    });
                });
            },
            showPriceAccountDetails: function () {
                showPriceAccountDetails();
            }
        });
        return pricingApproval;
    });