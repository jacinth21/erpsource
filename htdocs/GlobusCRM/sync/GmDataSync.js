
var pageno = 1, totalpages = 0, percent, callbackFn, VO, totalsize=0, localSize=0, pagesize, refType, GroupPartDeleteCount, sql = "", master = 1, updateSql = "", fileIDs = [], arrAccGPOMapp = [], arrSyncedAccounts = [];

var updatepageno = 1, updatetotalsize = 0, updatetotalpages = 0, updatelocalSize=0, arrMain;

var customInput = "", clearedCases = new Array();

var casesync = false;

//Master Call to sync 
function DataSync_SL(VOname, input, callback) {
    var WSName = eval(VOname + ".url");
    var parentStatus =  eval(VOname+".parentStatus")
    if(parentStatus != undefined && parentStatus == "failure"){
    	callback("success");
    }else{
    
    VO = VOname;
    sql = "";
    if(input!=undefined)
      customInput = input;
    if(callback!=undefined) 
      callbackFn = callback;
    fnGetSyncInfo(eval(VOname + ".syncType"),function  (syncInfo) {
        if(eval(VOname + ".syncSeq") == "1")
          master = syncInfo;
        console.log(customInput);
        getServerChanges(WSName, function(ServerChanges) {
        	console.log("boolSyncing is "+boolSyncing);
          if(VOname == "listGmGroupPartVO" && !boolSyncing) {
            GroupPartDeleteCount = 0;
            chkGroupPartUpdate(VOname, ServerChanges, fnSyncStatusUpdate)
          }
          else if((VO == 'listGmRulesVO')) {
            changeLocalDB(0, VOname, ServerChanges, fnSyncStatusUpdate)
          }
          else if((VO == 'listGmCaseInfoVO') || (VO == 'listGmCaseAttributeVO')) 
            fnDeleteCases(VOname, ServerChanges);
          else if (VO == 'listGmAcctPartPriceVO' || VO == 'listGmAcctGPOPriceVO' )
            fnDelPrice(ServerChanges);
          else
            updateLocalDB(VOname, ServerChanges, fnSyncStatusUpdate);
        });
     });
    }
}
/*fnSyncStatusCheck: Check previous sync status from t9151 table.
 *if status is failure then clear local table and void sync detail in server for current sync type.
 *Start fresh sync for the sync type.
 */
function fnSyncStatusCheck(VOname,callback){
	var syncType = eval(VOname + ".code");
	var parentCode = eval(VOname + ".parentCode");
	var query =  "SELECT c901_sync_status status FROM t9151_device_sync_dtl WHERE c901_sync_type ='"+syncType+"'";
	 fnExecuteSelectQuery(query,function(data){
        var pre_status = "";
         
         if(data.length > 0)
            pre_status = data[0].status;
         
            if (pre_status == '103124'|| pre_status == '103125') {// Failure
            	resetServer(syncType,function(){
                   var querystr = "DELETE FROM " + eval(VOname + ".tableName");
            		/* 
            		 *SYSTEM & SET MASTER are using same table "T207_SET_MASTER".
            		 *For System, the c207_type is null.So, 
            		 *the below if-else condition to delete only SYSTEM records or set master records.
            		 */
                   if (VOname == "listGmSystemVO") {//SYSTEM
                       querystr += " WHERE c207_type IS NULL";
                   } else if (VOname == "listGmSetMasterVO") { //SET
                       querystr += " WHERE c207_type IS NOT NULL ";
                   }
                    
            		 fnExecuteSelectQuery(querystr, function(data){
	            		console.log("status failure");
	            		if(pre_status == '103124')
	            			fnUpdateSyncStatus(VOname,"103125");//sync status as WIP
		            	callback("success");
            		 });
            	});
            }else{
            	/*Check parent sync status before sync starting. If parent failure then skip the child sync
            	Ex. if part fails then skip part attribute sync*/
            	if(pageno == 1 && parentCode != undefined){
            		 var query =  "SELECT c901_sync_status status FROM t9151_device_sync_dtl WHERE c901_sync_type ='"+parentCode+"'";
            		 fnExecuteSelectQuery(query,function(data){
            	            console.log(data);
            	            var pre_status = data[0].status;
            	            console.log("pre_status="+pre_status);
            	            if (pre_status == '103124'|| pre_status == '103125') {// Failure
            	            	console.log("status success");
            	            	callback("failure");
            	            }else{
            	            	console.log("status success");
            	            	fnUpdateSyncStatus(VOname,"103125");//sync status as WIP
            	            	callback("success");
            	            }
            		 });
            	            
            	}else{
            	
	            	console.log("status success");
	            	fnUpdateSyncStatus(VOname,"103125");//sync status as WIP
	            	callback("success");
            	}
            }
           
     });
}

function fnUpdateSyncStatus(VOname,status){
	   var data =[];
       var json={"reftype":eval(VOname+".code"),"statusid":status,"syncgrp":eval(VOname+".syncGrp"),"syncseq":eval(VOname + ".syncSeq").toString()}
       data.push(json);
       updateLocalDB("GmProdCatlReqVO",data,function(){
           console.log("local sync table insert success  VOname="+eval(VOname+".code")+" :: status="+status);
      });
}

function fnSyncStatusUpdate(SyncStatus) {
    if(SyncStatus === "Success") {
        if(pageno!=1) {
            DataSync_SL(VO);
        }
        else {
        	 getLocalDBRecCount(function(localRecCount){
         acknowlegdeServer(SyncStatus,localRecCount);
        	 });
        } 
    }
}
var start_time;
var record_count;
function updateLocalDB(VOname, ResultSet, fnCallbackStatus) {
    this.start_time = (new Date()).getTime();
    //console.log(fnCallbackStatus);
    var ln = ResultSet.length;
    this.record_count=ln;
    var j, l, k;
    var bulk = 500;

    var iteration = Math.floor(ln/bulk);

    if((ln%bulk) > 0) 
      iteration+=1;

    var remaining = ln%bulk;
    var tblName;
    
    var start, end;

    start = 0;
    end = start + bulk;
    
    if(ln<end) {
      end = ln;
      iteration = 1;
    }

    intializePrepareNewQuery (0, iteration, start, end, bulk, VOname, ResultSet, fnCallbackStatus);
    
}


function intializePrepareNewQuery(iValue, iteration, start, end, bulk, VOname, ResultSet, fnCallbackStatus) {
  if(iValue<iteration) {
    console.log("start : " + start);
    console.log("end : " + end);
    console.log("bulk : " + bulk);

     var l = ResultSet.slice(start,end);
      console.log(l.length);
     prepareNewQuery(VOname, l, function(data) {
       start = end;
      end = end + bulk;
     
        if(end>ResultSet.length)
          end = ResultSet.length;

        tableName = eval(VOname + ".tableName");
        insertQuery(tableName, data, function() {
          intializePrepareNewQuery(iValue+1, iteration, start, end, bulk, VOname, ResultSet, fnCallbackStatus)
        });
      });
  }
  else
    fnCallbackStatus("Success")
  
}


function prepareNewQuery(VOname, ResultSet, callback) {


  // console.log("Inside prepareQuery");

  var i = 0;
  var f = 0; // Length of the FieldNames array
  var l = ResultSet.length;
  var row;

  // console.log("There are " + l + " records to work with");

  
  var newQuery = true, query = '';
  var FieldNames, FieldValues;
  var tableName = eval(VOname + ".tableName");

  getRow(tableName);

  localSize+=l;
  console.log(localSize);
  for(i=0; i<l; i++) {

    row = ResultSet[i];

    if (newQuery) {

        FieldNames = getFieldNames(VOname, row);
        FieldValues = getFieldValues(VOname, row);

        f = FieldNames.length;
        //INSERT OR REPLACE INTO
        query = "INSERT OR REPLACE INTO " + tableName + "(";

        for(j = 0; j < f; j++) {

          if(FieldNames[j] !== "ID") {
              query += ("'" + FieldNames[j] + "'");
              
            if(j < f-1) {
              query += ",";
            }
          }
        }

        query += ") SELECT " + getFieldValues(VOname, row);
        newQuery = false;
    }
    else {
        
        query += ' UNION ALL SELECT ' + getFieldValues(VOname, row);
    }

  }



  query = query.replace(/\"/g, "\&quot;");
 
  if (i==l) {
    callback(query)
    newQuery = true;
  }

 
}

function insertQuery(tableName, query, fnCallback) {
  
  db.transaction(function(tx,rs) {
//    console.log("The insert query is" + query);
    tx.executeSql(query,[], function(tx,rs) {
      if(tableName != "T9151_DEVICE_SYNC_DTL"){// No change in progress bar when inserting into t9151
      console.log(rs.rowsAffected + " Records were successfully inserted");
      var syncSeq = eval(VO + ".syncSeq"), totalSeq = eval(VO + ".totalSeq");
      var percentage = (localSize/totalsize) * 100;
      if(syncSeq!=undefined) {
          var percent = ((syncSeq-1) * (100/totalSeq)) + (percentage/totalSeq);
          $(".spn-current-progress").css("width",percent+ "%");
      }
      console.log(rs.rows.length + " Records were successfully retrieved");
    	}
      // console.log(fnCallback);
      fnCallback("Success");


      // callback("Success");
    }, function(tx,err) {
      console.log("Failed Query: " + query);
      console.log("Failure " + JSON.stringify(err));
      fnCallback("Fail");
    });
  });

}

function getQuery(tableName, callback) {
  var query = "SELECT * FROM " + tableName;
  db.transaction(function(tx,rs) {
    tx.executeSql(query,[], function(tx,rs) {
      // console.log("Successful Query: " + query);
      
      var syncSeq = eval(VO + ".syncSeq"), totalSeq = eval(VO + ".totalSeq");
      var percentage = (localSize/totalsize) * 100;
      console.log("localSize : " + localSize);
      console.log("totalsize : " + totalsize);
      // console.log("percentage : " + percentage);

      var percent = ((syncSeq-1) * (100/totalSeq)) + (percentage/totalSeq);
       console.log("syncSeq : " + syncSeq);
      console.log("totalSeq : " + totalSeq);
      console.log("Percent: " + percent);
      $(".spn-current-progress").css("width",percent+ "%");
      console.log(rs.rows.length + " Records were successfully retrieved");
      callback("Success");
    }, function(tx,err) {
      console.log("Failed Query: " + query);
      console.log("Failure " + JSON.stringify(err));
    });
  });

}

//Insertion into LocalDB is made
function changeLocalDB(iValue, VOname, ResultSet, callback) {

    var l = ResultSet.length;
    // var sql = "";
    var e;
    var TableName = eval(VOname + ".tableName");
    var now = new Date();

       db.transaction(
         function (tx, result) {
         if (iValue < l) {
             e = ResultSet[iValue];
             
             var LMDate = new Date(e.LastModified);
             if(sql=="")
               sql = LDB_InsertUpdateSQL(VOname, e);
             params = getFieldValuesForChangeDB(VOname, e);

             tx.executeSql(sql, params,
                 function(){

                    localSize++;
                    var syncSeq = eval(VO + ".syncSeq"), totalSeq = eval(VO + ".totalSeq");
                    var percentage = (localSize/totalsize) * 100;

                    var percent = ((syncSeq-1) * (100/totalSeq)) + (percentage/totalSeq);
                    $(".spn-current-progress").css("width",percent+"%");
                    if(VO == 'listGmFileVO') {
                      fnConfigPathFor(e.fileid,function () {
                          changeLocalDB(iValue+1, VOname, ResultSet, callback);
                      });
                    }
                    else {
                      changeLocalDB(iValue+1, VOname, ResultSet, callback);
                    }
                   
                 },
                 function(tx, err){

                    console.log(JSON.stringify(err));
                    callbackFn("fail");
                 }
                 );
             }
             else {
               if((VO == 'listGmFileVO') || (VO == 'listGmRulesVO')) {
                  fnRunShareRuleUpdate(function () {
                    if(VO == 'listGmFileVO') {
                      fnRefreshShareCart(function  () {
                        callback("Success");
                      });
                    }
                    else
                      callback("Success");
                  })
               }
               else
                callback("Success");
             }
         }, this.txErrorHandler,null
      ) 
}

//Get the data from the server
function getServerChanges(WSName, callback) {
    var token = localStorage.getItem("token");
    var postData;
    if(customInput=="")
      postData = {"token": token, "langid": langid, "pageno": pageno, "uuid" : deviceUUID, "countryid" : intServerCountryCode};
    else {
      customInput.token = token;
      customInput.langid = langid;
      customInput.pageno = pageno;
      customInput.uuid = deviceUUID;
      customInput.countryid = intServerCountryCode;
      postData = customInput;
    }
    fnGetWebServerData(WSName, undefined, postData,function(data,model,response,errorReport){
      console.log("dataaaaa"+data)  
      console.log(data)
      if(model == undefined )
           processServerData(data,callback);
        else{
            console.log(model.responseText);
            console.log(response);
            console.log("Nothing to be Sync'd from the Server with URL " + URL_WSServer + WSName );
            callbackFn("Fail");
        } 
    },true);
}

//Calulates the Pages / Deletes unwanted information from the server
function processServerData(data,callback) {
    refType = data.reftype;
    totalsize = data.totalsize;
    if(data.totalpages!="")
      totalpages = data.totalpages;
    else 
      totalpages = pageno;

    pagesize = parseInt(data.pagesize);
    // percent = parseInt((pageno/totalpages)*100);
    if(data.pageno!=data.totalpages) {
        pageno++;
    }
    else {
      pageno = 1;
    }
    if(data.totalpages!="") {
      var val;
      var valLength;
      var dataHolder = (eval(VO + ".dataHolder"))?eval(VO + ".dataHolder"):VO;
    
      if(eval("data."+dataHolder).length == undefined) {
        val = [];
        val.push(eval("data."+dataHolder))
      }

      else
        val = eval("data."+dataHolder);
      

        valLength = val.length;
        for(var i=0; i<valLength; i++) {
            var currRow = val[i];
            for (var column in currRow) {
              if(eval(VO + "." + column)==undefined) 
                delete currRow[column];
            }
        }

        if(VO=="listGmFileVO")
          fileIDs = fileIDs.concat(_.pluck(val,'fileid'));
        else if(VO=="listGmAcctGpoMapVO")
          arrAccGPOMapp = arrAccGPOMapp.concat(val);
        if(VO=="listGmAccountVO" && data.pageno==1)
            fnGetSyncedAccountPriceNameList(function(arrAccInfo) {
                arrSyncedAccounts = _.pluck(arrAccInfo,"ID");
                callback(val);
            });
        else
            callback(val);
        
    }
    else {
        var syncSeq = eval(VO + ".syncSeq"), totalSeq = eval(VO + ".totalSeq");
        var percent = (syncSeq / totalSeq)*100;
        $(".spn-current-progress").animate({"width":percent+ "%"},"fast");
        getLocalDBRecCount(function(localRecCount){
        	acknowlegdeServer("",localRecCount);
        });
        
    }
}

//Updates the Sync Status to the Server
function acknowlegdeServer(status,localRecCount) {
  pageno = 1;
  localSize = 0;
  sql = "";
  var token = localStorage.getItem("token");
    
  var postData = {"token": token, "langid": langid, "reftype": refType, "totalsize":localRecCount,"statusid":"103121", "uuid" : deviceUUID, "countryid" : intServerCountryCode};
  if(VO != "listGmDataUpdateVO") {
      console.log(URL_WSServer + "devicesync/status");
       fnGetWebServerData("devicesync/status", undefined, postData,function(data,model,response,errorReport){
      console.log("dataaaaa"+data)  
      console.log(data)
     if(model == undefined ){
          if(data!=undefined){
                    var currArray = new Array();
                   var tmpData = {};
                    tmpData.reftype = data.reftype;
                    tmpData.statusid = data.statusid;
                    tmpData.totalsize = data.totalsize;
                    tmpData.svrcnt = data.svrcnt;
                    tmpData.syncgrp =eval(VO + ".syncGrp");
                    console.log(eval(VO + ".syncSeq"));
                    tmpData.syncseq =eval(VO + ".syncSeq").toString();
                    tmpData.syncdt =  (new Date()).toString();
                    currArray.push(tmpData);
                    data = currArray;
                }
                
              updateLocalDB("GmProdCatlReqVO",data,function(){
                     console.log("local sync table insert success");
                });
            	//}
              
              localSize = 0;
              // fnShowStatus("Database Sync from Server to Device successful");
              if((VO != "listGmCaseAttributeVO") && (VO != "listGmFileVO") && (VO != "listGmAcctGpoMapVO") && (VO != "listGmAccountVO"))
                callbackFn("success");
              else if(VO == "listGmFileVO")
                fnConfigFilePath();
              else if(VO == "listGmAcctGpoMapVO")
                fnCheckForGPOPriceUpdate();
              else if(VO == "listGmAccountVO")
                  fnUpdatePriceSyncInfo(arrSyncedAccounts.join(","),"Y",function(){callbackFn("success");});
              else 
                fnCreateEvents();
      }
        else{
             console.log("error");
                console.log("Nothing to be Sync'd from the Server!");
                callbackFn("fail");
                showMsgView("027");
        } 
    },true);
  }
 
}

function getLocalDBRecCount(callback){
	var querystr = "select count(1) as COUNT FROM "+eval(VO + ".tableName");
	  if(VO == "listGmSystemVO") {
		  querystr += " WHERE c207_type IS NULL AND C207_VOID_FL = ''";
	  }else if (VO == "listGmSetMasterVO"){
		  querystr += " WHERE c207_type IS NOT NULL AND c901_status_id = '20367' AND C207_VOID_FL = ''";
	  }else if (VO =="listGmPartAttributeVO"){
		  querystr += " T205D, t205_part_number T205 where c205d_void_fl=''"+
		              " AND t205.c205_part_number_id =t205d.c205_part_number_id AND T205.c205_active_fl ='Y'";
	  }else if (VO =="listGmPartNumberVO"){
		  querystr += " WHERE C205_ACTIVE_FL ='Y'";
	  }else if (VO =="listGmSetPartVO"){
		  querystr += " t208, T207_SET_MASTER t207 WHERE t207.c207_set_id = t208.c207_set_id AND t208.c208_void_fl ='' AND t207.c901_status_id = '20367'";
	  }else if (VO =="listGmSetAttributeVO"){
		  querystr += " T207C, T207_SET_MASTER t207 WHERE t207.c207_set_id = t207c.c207_set_id AND T207C.c207c_void_fl ='' AND t207.c901_status_id = '20367'";
	  }else if (VO =="listGmSystemAttributeVO"){
		  querystr += " t207c, t207_set_master t207 WHERE t207.c207_set_id = t207c.c207_set_id AND c207_type IS NULL AND C207_VOID_FL = '' AND t207c.c207c_void_fl =''";
	  }else if (VO =="listGmAccountVO"){
		  querystr += " WHERE C704_ACTIVE_FL = 'Y' AND C704_VOID_FL = ''";
	  }else if (VO =="listGmAcctGpoMapVO"){
		  querystr += " t740, T704_ACCOUNT t704 WHERE t704.C704_ACCOUNT_ID = t740.C704_ACCOUNT_ID AND t740.c740_void_fl = '' AND t704.C704_ACTIVE_FL = 'Y' AND t704.C704_VOID_FL = ''";
	  }else if (VO =="listGmSalesRepVO"){
		  querystr += " WHERE C703_ACTIVE_FL = 'Y' AND C703_VOID_FL = ''";
	  }else if (VO =="listGmAssocRepMapVO"){
		  querystr += " WHERE C704A_ACTIVE_FL = 'Y' AND C704A_VOID_FL = ''";
	  }else if (VO =="listGmDistributorVO"){
		  querystr += " WHERE C701_ACTIVE_FL = 'Y' AND C701_VOID_FL = ''";
	  }else if (VO =="listGmAddressVO"){
		  querystr += " WHERE C106_INACTIVE_FL = '' AND C106_VOID_FL = ''";
	  }else if (VO =="listGmGroupVO"){
		  querystr += " WHERE C4010_PUBLISH_FL = 'Y' AND C4010_VOID_FL = ''";
	  }else if (VO =="listGmGroupPartVO"){
		  querystr += " t4011, T4010_GROUP t4010 WHERE T4010.C4010_GROUP_ID = T4011.C4010_GROUP_ID AND C4010_PUBLISH_FL = 'Y' AND C4010_VOID_FL = ''";
	  }else if (VO =="listGmPartyVO"){
		  querystr += " WHERE C101_VOID_FL = '' AND (C101_ACTIVE_FL = 'Y' OR C101_ACTIVE_FL = '')";
	  }else if (VO =="listGmPartyContactVO"){
		  querystr += " t107, t101_party t101 WHERE t101.c101_party_id =t107.c101_party_id "
			        +" AND t101.C101_VOID_FL = '' AND (t101.C101_ACTIVE_FL = 'Y' OR t101.C101_ACTIVE_FL = '') "
			        +" AND t107.C107_VOID_FL = '' AND t107.C107_INACTIVE_FL = ''";
	  }else if (VO =="listGmUserVO"){	
		  querystr += " WHERE C901_USER_STATUS = 311";//Active User
	  }	  
	  else if (eval(VO + ".voidfl")!= undefined){
		  querystr += " WHERE "+eval(VO + ".voidfl")+" =''";
	  }
	  
	  
	  
	  console.log(querystr);
	  
	if(eval(VO + ".tableName") != undefined){
		console.log("calling count");
		db.transaction(function (tx, rs) {
			tx.executeSql(querystr, [],
					function(tx,rs) {
				callback(rs.rows.item(0).COUNT);
			},null);

		});
	}else{
		callback("0");
	}
}
function fnCheckForGPOPriceUpdate() {
    if(master) {
        fnGetSyncedAccountPriceNameList(function(data) {
            if(data.length>0) {
                data = _.pluck(data,'ID');
                console.log(data);
                var mainCallback = callbackFn;
                var accID = new Array(), input = {};
                $(arrAccGPOMapp).each(function() {
                    if(_.contains(data,this.acctid) && this.voidfl=="" && this.partyid!="") 
                        accID.push(this.acctid);
                });
                if(accID.length>0) {
                    accID = _.uniq(accID);
                    input.acctid = accID.join(",");
                    input.reftype = 4000524;
                    delete listGmAcctGPOPriceVO.syncSeq;
                    DataSync_SL("listGmAcctGPOPriceVO",input, function() {
                        arrAccGPOMapp = [];
                        listGmAcctGPOPriceVO.syncSeq = 1;
                        fnClearUpdates("4000524",function() {
                          fnGetUpdates(function(length) {
                            if(length>0) {
                              $(".divUpdatesNo").html('');
                              $(".divUpdatesNo").html("&nbsp;" + length + "&nbsp;");
                              $(".divUpdatesNo").trigger("refresh");
                              document.getElementById("divUpdatesNo").style.display = "inline";
                            }
                            else {
                              $(".divUpdatesNo").text("");
                              document.getElementById("divUpdatesNo").innerHTML = "&nbsp;0&nbsp;";
                              $(".divUpdatesNo").addClass("hide");
                            }
                            mainCallback("success");
                          });
                      });
                    });
                }
                else
                    callbackFn("success");
            }
            else
                callbackFn("success");
        });
    }
    else
        callbackFn("success");
    
}

function resetServer(syncType,callback) {
  console.log(URL_WSServer + "devicesync/clear");
  var token = localStorage.getItem("token");
  var postData  = {"token": token, "langid": langid, "pageno": pageno, "refType":syncType, "uuid" : deviceUUID};
    fnGetWebServerData("devicesync/clear", undefined, postData,function(data,model,response,errorReport){
      console.log("dataaaaa"+data)  
      console.log(data)
      if(model == undefined ){
         if(syncType == ""){
	            fnResetDB(function() {
	                $(".divUpdatesNo").html('');
	                $(".divUpdatesNo").html("&nbsp;0&nbsp;");
	                document.getElementById("divUpdatesNo").style.display = "inline";
	                callback();
	            });
        	}else{
        		callback();
        	}
      }
        else{
            console.log("error");
            console.log("Error in Reset Call to Server");
            showMsgView("034");
        } 
    },true);

}


function chkGroupPartUpdate(VOname, ResultSet, callback){
    var len = ResultSet.length;
    if(GroupPartDeleteCount<len) {
        fnDeleteGroupPartData(ResultSet[GroupPartDeleteCount].groupid,function(){
            if(GroupPartDeleteCount==(len-1)) {
              updateLocalDB(VOname, ResultSet, callback);
            }
            else {
              GroupPartDeleteCount++;
              chkGroupPartUpdate(VOname, ResultSet, callback);
            }
        });
    }
}

//For Deleting the price of given Account ID / Part Number or Account ID / Group ID or GPO ID / Part Number or GPO ID / Group ID
function fnDelPrice(ResultSet) {
    
    var arrAcct = _.uniq(_.pluck(ResultSet,'acctid'));
    var arrGpo = _.uniq(_.pluck(ResultSet,'gpoid'));
    var arrPart = _.without(_.uniq(_.pluck(ResultSet,'partnum')),"");
    var arrGrp = _.without(_.uniq(_.pluck(ResultSet,'grpid')),"");
    var strQuery = null;

    if(VO == 'listGmAcctPartPriceVO') {
        strQuery = "DELETE FROM T705_ACCOUNT_PRICING WHERE C704_ACCOUNT_ID IN (" + arrAcct.join(',') + ")";
        if(arrPart.length > 0)
            strQuery += " AND C205_PART_NUMBER_ID IN ('" + arrPart.join("','") + "')";
        if(arrGrp.length > 0)
            strQuery += " AND C4010_GROUP_ID IN ('" + arrGrp.join("','") + "')";
    }
    else {
        strQuery = "DELETE FROM T705_ACCOUNT_PRICING WHERE C101_GPO_ID IN (" + arrGpo.join(',') + ")";
        if(arrPart.length > 0)
            strQuery += " AND C205_PART_NUMBER_ID IN ('" + arrPart.join("','") + "')";
        if(arrGrp.length > 0)
            strQuery += " AND C4010_GROUP_ID IN ('" + arrGrp.join("','") + "')";
    }
    

    fnExecuteDeleteQuery(strQuery, function() {
        //Get the maximum of the primary key (C705_ACCOUNT_PRICING_ID) and increment the ID by one for each record in ResulSet variable (Server Data)
        fnExecuteSelectQuery("SELECT MAX(C705_ACCOUNT_PRICING_ID) AS ID FROM T705_ACCOUNT_PRICING", function(arrMaxIDData) {
            var maxID = 0;
            if(arrMaxIDData[0].ID!=null) 
                maxID = arrMaxIDData[0].ID + 1;
            for(var i=0; i<ResultSet.length; i++)
                ResultSet[i].acctpriceid = (maxID++).toString();
            
            updateLocalDB(VO, ResultSet, fnSyncStatusUpdate);
        });
    });
   
}

function chkUpdate () {
    if(!boolSyncing){
    UpdateTimer = window.setTimeout(function() {
        // console.log(URL_WSServer + "devicesync/updatescnt");
        var token = localStorage.getItem("token");
        var postData  = {"token": token, "langid": langid, "pageno": 1, "uuid" : deviceUUID};
     
        var updatescnt = 0;
         fnGetWebServerData("devicesync/updatescnt", undefined, postData,function(data,model,response,errorReport){
      console.log("dataaaaa"+data)  
      console.log(data)
      if(model == undefined ){
                  var updateData = data;
                  if(data.totalsize > 0) {
                      $("#btn-updates").show();
                      $(".divUpdatesNo").text("");
                      document.getElementById("divUpdatesNo").innerHTML = "&nbsp;" + data.totalsize + "&nbsp;";
                      $(".divUpdatesNo").removeClass("hide");
                      fnAutoSyncAllUpdates();
                  }
                  else {
                    $(".divUpdatesNo").text("");
                    document.getElementById("divUpdatesNo").innerHTML = "&nbsp;0&nbsp;";
                    $(".divUpdatesNo").addClass("hide");
                    if(updateData.listAutoUpdates!=undefined) {
                        fnAutoUpdate(updateData);
                    }
                  }
              }
        else{
           console.log("Error in Checking Updates");
                  console.log(model.responseText);
                  chkUpdate();
        } 
    },true);

    },TimerMilliSeconds);
    }
}


function fnAutoSyncAllUpdates() {
   if(boolUpdate && !boolAutoUpdateStatus) {
       if(!GM.Global.Browser)
         window.clearTimeout(timerSplashScreen);
     boolAutoUpdateStatus = 1;
     $("#div-data-sync").trigger("collide");
     $("#div-updates").html("<li class='li-info-center'><img />&nbsp;Fetching Updates...</li>");
     autoUpdateCollision = window.setInterval(function() {
        $("#div-data-sync").html("<div id='div-settings-collision'>The App is being updated. This may take few seconds. <br/> Please wait...</div>");
        $("#div-wifi-note").hide();
        $("#div-updates").addClass("div-updates-updating");
     },100);


     getServerUpdates(function(){
       fnGetRequiredUpdates(function(refData) {
         if(refData.length>0) {
           fnAutoSyncVO(0,refData,function() {

            window.clearInterval(autoUpdateCollision);
            $("#div-updates").removeClass("div-updates-updating").slideUp(100);
            boolAutoUpdateStatus = 0;
            console.log("success");
           	fnTimerSplashScreen();
            chkUpdate();
           	
           })
         }
       });
   });
   }
   else 
     chkUpdate();

}

function fnAutoSyncVO(iValue, refData, callback) {
	  if(iValue<refData.length) {
	    var currRefData = _.pluck(refData,'TYPE');
	    var input = '';
	      if(currRefData[iValue]=="4000524" || currRefData[iValue]=="4000526") {
	          input = {};
	          fnGetSyncedAccountPriceNameList(function(data) {
	              input.acctid = _.pluck(data,'ID').join();
	              input.reftype = currRefData[iValue];
	              console.log(input);
	              DataSync_SL(jsnUpdateCode[currRefData[iValue]],input, function() {
	                  fnClearUpdates(currRefData[iValue],function() {
	                    fnGetUpdates(function(length) {
	                      if(length>0) {
	                        $(".divUpdatesNo").html('');
	                        $(".divUpdatesNo").html("&nbsp;" + length + "&nbsp;");
	                        $(".divUpdatesNo").trigger("refresh");
	                        document.getElementById("divUpdatesNo").style.display = "inline";
	                      }
	                      else {
	                        $(".divUpdatesNo").text("");
	                        document.getElementById("divUpdatesNo").innerHTML = "&nbsp;0&nbsp;";
	                        $(".divUpdatesNo").addClass("hide");
	                      }
	                      fnAutoSyncVO(iValue+1, refData, callback);
	                    });
	                });
	              });
	          });
	      }
	      else {
	        DataSync_SL(jsnUpdateCode[currRefData[iValue]],input, function() {
	            fnClearUpdates(currRefData[iValue],function() {
	              fnGetUpdates(function(length) {
	                if(length>0) {
	                  $(".divUpdatesNo").html('');
	                  $(".divUpdatesNo").html("&nbsp;" + length + "&nbsp;");
	                  $(".divUpdatesNo").trigger("refresh");
	                  document.getElementById("divUpdatesNo").style.display = "inline";
	                }
	                else {
	                  $(".divUpdatesNo").text("");
	                  document.getElementById("divUpdatesNo").innerHTML = "&nbsp;0&nbsp;";
	                  $(".divUpdatesNo").addClass("hide");
	                }
	                fnAutoSyncVO(iValue+1, refData, callback);
	              });
	          });
	        });

	      }

	      
	  }
	  else
	    callback();
	}


function fnAutoUpdate(data) {
  if(data.listAutoUpdates!=undefined) {
	  if(casesync == false) {
		  casesync = true;
		    DataSync_SL('listGmCaseInfoVO','', function() {
		        DataSync_SL('listGmCaseAttributeVO','', function() {
		        	casesync = false;
		        	chkUpdate();
		        });
		    });		  
	  }
	  else {
		  chkUpdate();
	  }
  }
  else {
	chkUpdate();
	}
}


function getServerUpdates(callback) {
    updateCallbackFn = callback;
	if(!boolSyncing){ 
    var token = localStorage.getItem("token");
    var postData  = {"token": token, "langid": langid, "pageno": updatepageno, "uuid" : deviceUUID};
    console.log(URL_WSServer + "devicesync/updates");
    console.log(JSON.stringify(postData));
    
    fnGetWebServerData("devicesync/updates", undefined, postData,function(data,model,response,errorReport){
      console.log("dataaaaa"+data)  
      console.log(data)
      if(model == undefined ){
                console.log(data);
            updatetotalsize = data.totalsize;
            $(".divUpdatesNo").html('');
            $(".divUpdatesNo").html("&nbsp;" + data.totalsize + "&nbsp;");
            $(".divUpdatesNo").removeClass("hide");
            if(data.pageno!=data.totalpages) {
                updatepageno++;
            }
            else {
                updatepageno = 1;
            }
            if(data.totalpages!="") {
              updatetotalpages = data.totalpages;
              var val;
              if(eval("data.listGmDataUpdateVO").length == undefined) {
                val = [];
                val.push(eval("data.listGmDataUpdateVO"))
              }

              else
                val = eval("data.listGmDataUpdateVO");

                 for(var i=0; i<val.length; i++) {
                    var currRow = val[i];
                    for (var column in currRow) {
                      if(eval("listGmDataUpdateVO." + column)==undefined) 
                        delete currRow[column];
                    }
                }
                fnLoadUpdates(0,val);
            }
            else {
                updatepageno = 1;
                updaetetotalpages = updatepageno;
                $(".divUpdatesNo").addClass("hide");
				updateCallbackFn("no data");
            }
        }
        else{
            console.log("error");
            console.log("Nothing to be Sync'd from the Server with URL ");
			updateCallbackFn("fail");
        } 
    },true);
 }
    else {
        updateCallbackFn("no data");
    }
}

function fnLoadUpdates(iValue, ResultSet) {

    var l = ResultSet.length;
    var sql = "";
    var e;
    var TableName = eval("listGmDataUpdateVO.tableName");
    var now = new Date();

       db.transaction(
         function (tx, result) {
         if (iValue < l) {
             e = ResultSet[iValue];
             
             var LMDate = new Date(e.LastModified);
             if(updateSql=="")
               sql = LDB_InsertUpdateSQL("listGmDataUpdateVO", e);
             params = getFieldValuesForChangeDB('listGmDataUpdateVO',e);
             tx.executeSql(sql, params,
                 function(){
                    // console.log("Local database " + TableName + " update Successful");
                    updatelocalSize++;
                    fnLoadUpdates(iValue+1, ResultSet);
                 },
                 function(tx, err){
                    // console.log("Local database " + TableName + " update failed");
                    console.log(JSON.stringify(err));
                    fnUpdateDataSync("fail");
                 }
                 );
             }
             else {
                fnUpdateDataSync("Success");
             }
         }, this.txErrorHandler,null
      ) 
}


function fnUpdateDataSync(SyncStatus) {
    if(SyncStatus === "Success") {
        if(updatepageno!=1) {
            getServerUpdates(updateCallbackFn);
        }
        else {
            updatepageno = 1;
            updateCallbackFn(SyncStatus);
        } 
    }
}

function fnGetReason() {

    var token = localStorage.getItem('token');

    if(arrPendingDOList.join(",")!="") {
      var postData = {"token" : token, "txnid" : arrPendingDOList.join(","), "txntype" : "90217"};

      console.log(JSON.stringify(postData));
         fnGetWebServerData("common/fetchlogdetails", undefined, postData,function(data,model,response,errorReport){
      console.log("dataaaaa"+data)  
      console.log(data)
      if(model == undefined ){
              var reasons = new Array();
                if(data.gmCommonLogVO.length==undefined)
                  reasons.push(data.gmCommonLogVO);
                else
                  reasons = data.gmCommonLogVO;

                for(var i=0;i<reasons.length;i++) {
                  fnUpdateRejectReason(reasons[i].txnid,reasons[i].txntype);
                }

                fnGetUpdatedDO(function(data) {
        console.log("DO : ");
        console.log(data);
        if(data.length>0) {
          var tobeCleared = _.pluck(data,'ID');
         arrPendingDOList = _.difference(arrPendingDOList,tobeCleared); 
        }
      });

          }
        else{
            console.log("error");
              console.log("Nothing to be Sync'd from the Server with URL ");  
        } 
    },true);

    }
    
}


function fnConfigFilePath() {
     var now = new Date();
     var thisMonth = ((now.getMonth() + 1)>9)?(now.getMonth() + 1):"0" + (now.getMonth() + 1);
     var prevMonth = (now.getMonth()>9)?now.getMonth():"0" + now.getMonth();
     var query = "UPDATE T903_UPLOAD_FILE_LIST SET C901_TYPE = '4000790' WHERE C901_TYPE IN ('4000787','4000788','4000789') AND C903_LAST_UPDATED_DATE NOT LIKE '" + now.getFullYear() + "-" + thisMonth + "-%' AND C903_LAST_UPDATED_DATE NOT LIKE '" + now.getFullYear() + "-" + prevMonth + "-%'";
    
     db.transaction(function (tx, rs) {
          tx.executeSql("UPDATE T903_UPLOAD_FILE_LIST SET C903_FILE_PATH = (SELECT T901.C901_CODE_NM FROM T901_CODE_LOOKUP T901 WHERE T903_UPLOAD_FILE_LIST.C901_REF_GRP = T901.C901_CODE_ID) || '/' || C903_REF_ID || '/' || C901_REF_TYPE || '/' || C901_TYPE || '/' || C903_FILE_NAME WHERE T903_UPLOAD_FILE_LIST.C901_TYPE NOT IN ('4000790')", [],
            function(tx,rs) {
                db.transaction(function (tx, rs) {
                    console.log(query);
                      tx.executeSql(query, [],
                        function(tx,rs) {
                           if(!master && fileIDs.length>0)  {
                              fnRemoveSyncedFiles();
                            }
                            else {
                              console.log('notOk');
                              callbackFn("success");
                            }
                      },
                      function(tx,err) {
                        console.log(JSON.stringify(err)); 
                        showNativeAlert("Please Sync App before syncing Files.","Sync Priority Clash");
                    });
                  });
          },
          function(tx,err) {
            console.log(JSON.stringify(err));
            showNativeAlert("Please Sync App before syncing Files.","Sync Priority Clash");
        });
      });


     
}

function fnRemoveSyncedFiles() {
    var searchTerm = [];
    var i=0;
    for(i=0; ((i<fileIDs.length) && (i<500)) ;i++) {
        searchTerm.push("C903_UPLOAD_FILE_LIST = '"+fileIDs[i]+"'");
    }
    var query = searchTerm.join(" OR ");

    query =  "SELECT C903_FILE_PATH FROM T903_UPLOAD_FILE_LIST WHERE " + query;

    db.transaction(function (tx, rs) {
        tx.executeSql(query, [],
          function(tx,rs) {
            var k=0; results = [];
            for(k=0;k<rs.rows.length;k++)
              fnRemoveFile(rs.rows.item(k).C903_FILE_PATH);
            fileIDs.splice(0,i);
            if(fileIDs.length>0)
              fnRemoveSyncedFiles()
            else {
              callbackFn("success");
            }
        },
        function(tx,err) {console.log(JSON.stringify(err)); alert('error');
      });
    });
}

// function fnConfigPathFor (currFile,callback) {
//        var originalPath = '';
//        db.transaction(function (tx, rs) {
//             tx.executeSql("SELECT T901.C901_CODE_NM || '/' || T903.C903_REF_ID || '/' || T903.C901_REF_TYPE || '/' || T903.C901_TYPE || '/' || T903.C903_FILE_NAME AS REFGROUP FROM T903_UPLOAD_FILE_LIST T903, T901_CODE_LOOKUP T901 WHERE T903.C901_REF_GRP = T901.C901_CODE_ID AND T903.C903_UPLOAD_FILE_LIST = ?", [currFile],
//               function(tx,rs) {
//                 if(rs.rows.length == 0) {
//                   showNativeAlert('Cannot Sync Files without Syncing App','Sync Priority Error','OK',callbackFn);
//                 }
//                 else {
//                   originalPath += rs.rows.item(0).REFGROUP;

//                   db.transaction(function (tx, rs) {
//                       tx.executeSql("UPDATE T903_UPLOAD_FILE_LIST SET C903_FILE_PATH = ? WHERE C903_UPLOAD_FILE_LIST = ?", [originalPath,currFile],
//                         function(tx,rs) {
//                           if(!master)
//                             fnRemoveFile(originalPath);
//                           callback();
//                       },
//                       function(tx,err) {console.log(JSON.stringify(err)); alert('error');
//                     });
//                   });

//               }
//             },
//             function(tx,err) {console.log(JSON.stringify(err)); alert('error');
//           });
//       });
// }


function fnRetrieveStatus(postData, callback) {
  console.log(postData);
  console.log(URL_WSServer + "share/sharedStatus");
      fnGetWebServerData("share/sharedStatus", undefined, postData,function(data,model,response,errorReport){
      console.log("dataaaaa"+data)  
      console.log(data)
     if(model == undefined ){
          console.log(data);
          arrMain = new Array();
          if(data.listGmShareEngineVO!=undefined) {
             processStatus(0,data.listGmShareEngineVO, callback);
          }
             // callback(0,data.listGmShareEngineVO);
          else {
            showNativeAlert("No Artifacts Shared with this E-Mail ID in the given date range", "No Emails!");
//            showMsgView("011");
          }
            
        }
        else{
           console.log("error");
            console.log("Nothing to be Sync'd from the Server with URL ");
            callback(response);
        } 
    },true);
    

}

function processStatus (iValue,data, callback) {

  var len;
  if(data.length==undefined){
    var currArray = new Array();
    currArray.push(data);
    data = currArray;
    len = 1;
  }
  else
    len = data.length;

  if(iValue < len) {
    var jsnMain = {};
    jsnMain.Subject = data[iValue].subject;

    var arrSentDate = data[iValue].emailsentdate.split('-');
    jsnMain.MailDate = arrSentDate[1] + "-" +  arrSentDate[2] + "-" + arrSentDate[0];

    fnGetCodeNM(data[iValue].sharestatus,function (status) {
      jsnMain.Status = status;
      prepareMailList(0, data[iValue].listGmSharedEmailVO, [], [], function(mailList) {
        jsnMain.MailList = mailList;
        prepareFileList(0,data[iValue].listGmSharedFileVO, [], [], [], function(fileList) {
          jsnMain.FileList = fileList;
          arrMain.push(jsnMain);

          processStatus(iValue+1,data,callback);
        });
      });

    })
  }
  else {
    console.log(arrMain);
    callback(arrMain);
  }
}

function prepareMailList(iValue, data, toList, ccList, callback) {

  var len;
  if(data.length==undefined){
    var currArray = new Array();
    currArray.push(data);
    data = currArray;
    len = 1;
  }
  else
    len = data.length;

  if(iValue < len) {
    var jsnMailList = {};
    jsnMailList.Email = data[iValue].emailid;
    var strStatus = ""
    if(data[iValue].emailstatus!="")
      strStatus = data[iValue].emailstatus

    fnGetCodeNM(data[iValue].recipienttype,function (type) {
      fnGetCodeNM(strStatus,function (status) {
        jsnMailList.Type = type;
        if(status!='')
          jsnMailList.status = status;
        else
          jsnMailList.status = 'Not Yet Processed';
        fnGetParty(data[iValue].refid, jsnMailList.Email, function (Name) {
          if(data[iValue].username != '')
            jsnMailList.Name = data[iValue].username + " - ";
          else if((Name!=undefined))
            jsnMailList.Name = Name.LastName + ", " + Name.FirstName + " - ";
          if(type=='TO') 
            toList.push(jsnMailList);
          else
            ccList.push(jsnMailList);
          prepareMailList(iValue+1, data, toList, ccList, callback);
        });
      });
    });
  } 
  else {
    var mailList = {};
    mailList.To = toList;
    mailList.Cc = ccList;
    callback([mailList]);
  }
}

function prepareFileList (iValue, data, documents, images, videos, callback) {
  
  var len;
  if(data.length==undefined){
    var currArray = new Array();
    currArray.push(data);
    data = currArray;
    len = 1;
  }
  else
    len = data.length;

  if(iValue < len) {
    fnGetFileDetail(data[iValue].updfileid, function (file) {
      if(file!=undefined) {
        var TypeID = file.TypeID;
        if((TypeID == '4000441') || (TypeID=='103114'))
          documents.push(file);
        else if(TypeID == '103112')
          images.push(file);
        else
          videos.push(file);
      }

      prepareFileList(iValue+1,data,documents,images,videos,callback);
    });
  }
  else {
    var fileList = {};
    fileList.Documents = documents;
    fileList.Images = images;
    fileList.Videos = videos;
    callback([fileList]);
  }
}

function sendEmail(JSONData,callback) {

  console.log(URL_WSServer + "share/shareEmail");
  console.log(JSONData);
    fnGetWebServerData( "share/shareEmail", undefined, JSONData,function(data,model,response,errorReport){
      console.log("dataaaaa"+data)  
      console.log(data)
       if(model == undefined ){
          console.log(data);
          callback(data);
           
        }
        else{
             console.log("error");
            console.log("Nothing to be Sync'd from the Server with URL ");
            console.log(response);
            console.log(model);
            callback("error");
            showMsgView("046");
        } 
    },true);

}


function reSendEmail(iValue, results) {

  if(iValue < results.length) {
    var currEmail = results[iValue];
    var JSONData = $.parseJSON(currEmail.JSON);
    JSONData.token = localStorage.getItem('token');
    var strJSONData = JSON.stringify(JSONData);

    fnUpdateJSON(currEmail.SHARE_ID, strJSONData, function() {
      sendEmail(strJSONData,function (data) {
        if(data!='error') {
          fnUpdateStatus(data.shextrefid,data.shareid,data.sharestatus, function  () {
            reSendEmail(iValue+1, results);
          });
        }
        else {
          reSendEmail(iValue+1, results);
        }
      });
    });
    
  }
}


function fnRunShareRuleUpdate(callback) {
  fnGetShareRules(function (rules) {
    fnUpdateShareRule(0,rules,callback);
  });
}

function fnUpdateShareRule(iValue, rules, callback) {
  if(iValue<rules.length) {
    db.transaction(function (tx, rs) {
        tx.executeSql("UPDATE T903_UPLOAD_FILE_LIST SET C903_SHARE_RULE = ? WHERE C901_TYPE = ?", [rules[iValue].VALUE,rules[iValue].ID],
          function(tx,rs) {
            fnUpdateShareRule(iValue+1,rules,callback);
          },
          function(e, err){alert('error');
          console.log(JSON.stringify(err));
        });
    });
  }
  else
    callback();
}

//Loads the Server Data for Sales Dashboard
function fnGetServerData(seviceurl,voname, input, callback) {
    var token = localStorage.getItem("token");
    if(input==undefined)
     input = {"token": token};

    console.log(JSON.stringify(input));

      fnGetWebServerData(seviceurl, voname, input,function(data,model,response,errorReport){
      console.log("dataaaaa"+data)  
      console.log(data)
     if(model == undefined ){
          if(eval("data."+voname)!=undefined) {
            callback(eval("data."+voname));
            console.log(eval("data."+voname));
          }
          else {
            // console.log(data);
            callback(data);
            
          }
        }
        else{
           console.log("Nothing to be Sync'd from the Server with URL " + URL_WSServer + seviceurl );
          console.log(errorReport);
          $(".div-dash-page-loader").hide();
          $(".div-detail").hide();
          $("#div-dash-nointernet-connection").show();
          $("#div-dash-nointernet-connection").html(" <div> <i class='fa fa-exclamation-triangle fa-lg'></i></div>No internet connection / Server is not reachable");
        } 
    },true);
    
}

//Achieving Handshake while coming from Offline to Online
function syncDOPdf(data) {
    var token = localStorage.getItem("token");
   if(data.length != 0) 
   _.each(data,function(data){ 
          var input = {};
    input.doid = data.doid;
    input.pdfname = data.pdfname;
    input.pdffl = data.pdffl;
          fnGetWebServerData("DOPdf/sync", undefined, input,function(data,model,response,errorReport){
      console.log("dataaaaa"+data)  
      console.log(data)
      if(model == undefined ){
              console.log(data);
               if(data.pdffl == "N")
                   iterateOffline(0,data)
          }
        else{
            console.log("Failure PDF");
            console.log(model.responseText);
        } 
    },true);
});
    
}
//If upload PDF -> save in Local table  else failure->try 3 times here so totally 6 and Update Localtable 
function iterateOffline(n,data) {
    uploadPDF(data.doid, function(uploadStatus) {
         console.log("uploadStatus***");
                            console.log("n = "+n);
                            var time = new Date();
                            console.log("time = "+time);
                            console.log(uploadStatus);
                        if(uploadStatus){
                           fnInsertPdfFlagInfo(data.doid, data.doid +".pdf",data.pdffl, n+3);
                        }
                        else if(n == eval(localStorage.getItem("PDFFAILURECNT"))-1 ){
                            fnInsertPdfFlagInfo(data.doid, data.doid +".pdf",data.pdffl, n+3);
                        }
                  else
                      iterateOffline(n+1,data);
                            }); 
}

//book DO
function bookDO(DO, callback) {
    var token = localStorage.getItem("token");
    var arrGmOrderVO = new Array();

    fnSplitDO(DO, function(finalDO) {
      if(finalDO.length!=undefined)
        arrGmOrderVO = finalDO;
      else
        arrGmOrderVO.push(finalDO);
      var dateFormat = localStorage.getItem("cmpdfmt");
      var postData = {"token" : token, "arrGmOrderVO" : arrGmOrderVO };
        postData.arrGmOrderVO[0].orderdate = formattedDate(postData.arrGmOrderVO[0].orderdate,localStorage.getItem("cmpdfmt"));
        postData.arrGmOrderVO[0].arrGmOrderAttrVO = _.each(postData.arrGmOrderVO[0].arrGmOrderAttrVO,function(data){
            if(data.attrtype == "103601") //Surgery Date on billing info tab
                data.attrvalue = formattedDate(data.attrvalue,dateFormat);
        });
        postData.arrGmOrderVO[0].arrDoSurgeryDetailVO = _.each(postData.arrGmOrderVO[0].arrDoSurgeryDetailVO,function(data){
            if(data.attrtype == "10304734") //AckOrderOn on Surgery Detail tab
                data.attrvalue = formattedDate(data.attrvalue,dateFormat);  
            if(data.attrtype == "400151") //Date of surgery on Surgery Detail tab
                data.attrvalue = formattedDate(data.attrvalue,dateFormat);  
        });
      console.log("bookDO"+ JSON.stringify(postData));
          fnGetWebServerData("DeliveredOrder/syncdo", undefined, postData,function(data,model,response,errorReport){
      console.log("dataaaaa"+data)  
      console.log(data)
      if(model == undefined ){
              console.log(data);
              callback(1,data);
          }
        else{
            console.log(model);
              callback(0,model.responseText);
        } 
    },true);
        

    });
    
}
//AutoBookDO will book all the DOS that are booked Offline one by One
function fnAutoBookDO(iValue, JSONData) {
    if(iValue<JSONData.length) {
        var parentJSON = $.parseJSON(JSONData[iValue].JSON);
        fnShowStatus("Booking DO " + parentJSON.orderid + "...");
                fnGeneratePDF(parentJSON.orderid, parentJSON.orderid, undefined, $.parseJSON(JSONData[iValue].OTHER_INFO), parentJSON.repid, function() {
                    //PMT-12763 Achieving Handshake For DO PDF sync
                    iterate(0,parentJSON,iValue,JSONData);
    });
    }
}
//If upload PDF ->Success->BooDO else failure->try 3 times totally to BookDO and if failure for three times allow user to BookDO() 
function iterate(n,parentJSON,iValue,JSONData){
      uploadPDF(parentJSON.orderid, function(uploadStatus) {
         console.log("uploadStatus***");
                            console.log("n = "+n);
                            var time = new Date();
                            console.log("time = "+time);
                            console.log(uploadStatus);
                        if(uploadStatus){
                            fnInsertPdfFlagInfo(parentJSON.orderid, parentJSON.orderid +".pdf",parentJSON.pdffl, n+1);
            parentJSON.arrGmOrderAttrVO.push({"orderid" : parentJSON.orderid, "attrtype" : "26230806", "attrvalue" : "Y"});
            parentJSON.arrGmOrderAttrVO.push({"orderid" : parentJSON.orderid, "attrtype" : "26230805", "attrvalue" : n+1});
                            saveDO(parentJSON,iValue,JSONData);
                        }
                        else if(n == eval(localStorage.getItem("PDFFAILURECNT"))-1 ){
            parentJSON.arrGmOrderAttrVO.push({"orderid" : parentJSON.orderid, "attrtype" : "26230806", "attrvalue" : "N"});
            parentJSON.arrGmOrderAttrVO.push({"orderid" : parentJSON.orderid, "attrtype" : "26230805", "attrvalue" : n+1});
                             saveDO(parentJSON,iValue,JSONData);
                        }
          else
              iterate(n+1,parentJSON,iValue,JSONData);
                    });
}
//Books DO 
function saveDO(parentJSON,iValue,JSONData){
    bookDO(parentJSON, function(status,data) {
            if(status) {
                        fnAutoBookDO(iValue+1, JSONData);
                    fnRemoveFile("DO/Signature/" + parentJSON.orderid + ".txt");
                    fnUpdateDOStatus(parentJSON.orderid,'Pending CS Confirmation');
                    fnShowStatus(parentJSON.orderid+ " booked successfully!");
            }
            else {
              data = $.parseJSON(data);
              if(data.message == "DO already entered in the System") {
                  var oldDOID = parentJSON.orderid;
                  var orderdate = parentJSON.orderdate;
                  var serverseq, localseq, seq;
                  var DOFormat = oldDOID.split("-")[0]+ "-" + oldDOID.split("-")[1] + "-" ;
                  fnCheckDOID(DOFormat, function(DOs) {
                      var currSeq = (DOs.length)?(_.max(_.pluck(DOs,'DOID'), function(id){ return parseInt(id.split('-')[2]); })):"0";
                      console.log(currSeq);
                      localseq = parseInt(currSeq.replace(DOFormat,"")) + 1;
                      var prefixordid = oldDOID.split("-")[0]+ "-" + oldDOID.split("-")[1];
                      var input = {
                          "token": localStorage.getItem("token"),
                          "orderdate": orderdate,   
                          "prefixordid": prefixordid
                      };
                      fnGetWebServiceData("DeliveredOrder/getlatestdo", undefined, input, function(status,data) {
                          if(status){
                              if(data.orderid!="") {
                                  var orderid = data.orderid;
                                  serverseq = orderid.split("-")[2];
                                  serverseq = parseInt(serverseq) + 1;
                              }
                              
                              else {
                                  serverseq = 1;
                              }

                              if(serverseq == localseq)
                                  seq = serverseq;
                              else if(serverseq > localseq)
                                  seq = serverseq;
                              else
                                  seq = localseq;
                          }
                          else {
                              seq = localseq;
                          }

                          var newDOID = oldDOID.split("-")[0]+ "-" + oldDOID.split("-")[1] + "-" + seq;
                          showNativeConfirm("DO ID "+ oldDOID + " already exists in the Server.\nWould you prefer to update the DO ID to "+ newDOID+"?","DO ID Conflict",["Yes","No"], function(index) {
                              if(index==1) {
                                  fnGeneratePDF(parentJSON.orderid, newDOID, undefined, $.parseJSON(JSONData[iValue].OTHER_INFO), $.parseJSON(JSONData[iValue].JSON).repid, function() {
                                      var exp = new RegExp(oldDOID,'g');
                                      var str = JSONData[iValue].JSON;
                                      JSONData[iValue].JSON = str.replace(exp,newDOID);
                                      fnDeleteDO(oldDOID);
                                      fnSaveDO(newDOID, parentJSON.acctid, str.replace(exp,newDOID), JSONData[iValue].JSONParts, JSONData[iValue].COVERINGREPFL, JSONData[iValue].SIGNATURE, JSONData[iValue].ADDITIONAL_INFO, JSONData[iValue].OTHER_INFO);
                                      fnUpdateDOStatus(newDOID,'Pending Sync');
                                       fnCheckPendingSync();
                                  });
                              }
                              else{
                                  fnUpdateDOStatus(oldDOID,'DRAFT');
//                                  fnShowStatus("");

                              }
                                  
                          });
                      });
                  });
              }
              else if(data.message.indexOf("A DO with the same Account-Part-Quantity combination already exists with ID")>=0) {
                    showNativeConfirm(data.message + "\nAre you sure you want to continue with this new DO?","POTENTIAL DUPLICATE ORDER",["Yes","No"], function(index) {
                      if(index==1) {
                        parentJSON.skipvalidatefl = 'Y'
                        fnSaveDO(parentJSON.orderid, parentJSON.acctid, JSON.stringify(parentJSON), JSONData[iValue].JSONParts, JSONData[iValue].COVERINGREPFL, JSONData[iValue].SIGNATURE, JSONData[iValue].ADDITIONAL_INFO, JSONData[iValue].OTHER_INFO);
                        fnUpdateDOStatus(newDOID,'Pending Sync');
                        fnCheckPendingSync();
                      }
                      else {
                        fnDeleteDO(parentJSON.orderid);
                      }
                    });
                  }
              else if(data.message.indexOf("Control Number")>=0) {
                fnUpdateDOStatus(parentJSON.orderid,'DRAFT');
                showNativeConfirm("Invalid Lot Number " + parentJSON.orderid +"\nDo you want to change it now?","Error on "+ parentJSON.orderid, ["Yes","No"], function(index) {
                  if(index==1) 
                    window.location.href = "#do/" + parentJSON.orderid;
                });
              }
              else if(data.message.indexOf("valid tag")>=0) {
                fnUpdateDOStatus(parentJSON.orderid,'DRAFT');
                showNativeConfirm("Invalid Tag ID" + parentJSON.orderid +"\nDo you want to change it now?","Error on "+ parentJSON.orderid, ["Yes","No"], function(index) {
                  if(index==1) 
                    window.location.href = "#do/" + parentJSON.orderid;
                });
              }
              else if(data.errorCode == "20643") {
                DataSync_SL('listGmAccountVO','',function(status) {
                  if(status!="success") {
                    fnAutoBookDO(iValue+1, JSONData);
                  }
                  else {
                    fnGetAccountInfo(parentJSON.acctid, function(data) {
                      var oldrep = parentJSON.repid;
                      parentJSON.repid = data.REP;
                      var oldID = parentJSON.orderid;
                      parentJSON.orderid = parentJSON.orderid.replace(oldrep, parentJSON.repid);
                      fnDeleteDO(oldID);
                      fnSaveDO(parentJSON.orderid, parentJSON.acctid, JSON.stringify(parentJSON), JSONData[iValue].JSONParts, JSONData[iValue].COVERINGREPFL, JSONData[iValue].SIGNATURE, JSONData[iValue].ADDITIONAL_INFO, JSONData[iValue].OTHER_INFO);
                      fnUpdateDOStatus(newDOID,'Pending Sync');
                      fnGeneratePDF(parentJSON.orderid, newDOID, data, $.parseJSON(JSONData[iValue].OTHER_INFO), parentJSON.repid, function() {
                        fnCheckPendingSync();
                      });
                    });
                  }
                });
              }
              else if((data.message!=undefined) && (data.message!="")) {
            	  fnUpdateDOStatus(parentJSON.orderid,'DRAFT');
            	  showNativeAlert(data.message +"\n The DO is in DRAFT. You can edit and book again.","Error in Booking DO",["OK"], function() {
            			  fnAutoBookDO(iValue+1, JSONData);
            	  });
              }
              else {
              		fnAutoBookDO(iValue+1, JSONData);
              }
            }

        });
}

function fnGeneratePDF(orderID, newDOID, RepInfo, otherInfo, repid, callback) {
    var template_wp = fnGetTemplate(URL_DO_Template, "GmDOPDFLayout"+localStorage.getItem("cmpid"));
    var template_wop = fnGetTemplate(URL_DO_Template, "GmDOPDFLayoutWithOutPrice"+localStorage.getItem("cmpid"));
        if(template_wp == undefined )
                template_wp = fnGetTemplate(URL_DO_Template, "GmDOPDFLayout1000");
        if(template_wop == undefined )
                template_wop = fnGetTemplate(URL_DO_Template, "GmDOPDFLayoutWithOutPrice1000");
    
    if(otherInfo.HCAAccount) {
        template_wp = fnGetTemplate(URL_DO_Template, "GmDOHCALayout");
        template_wop = fnGetTemplate(URL_DO_Template, "GmDOHCALayoutWithOutPrice");
    }
    fnReadFile("Globus/DO/Signature/" + orderID + ".txt", function(data) {
      var JSONPDF = $.parseJSON(data);
      // var newDOID = orderID.split("-")[0]+ "-" + orderID.split("-")[1] + "-" + (parseInt(orderID.split("-")[2]) + 1);
      JSONPDF[0].DOID = newDOID;
      if(RepInfo!=undefined) {
        JSONPDF[0].RepID = RepInfo.REP;
        JSONPDF[0].FirstName = RepInfo.RepName.split(" ")[0];
        JSONPDF[0].LastName = RepInfo.RepName.split(" ")[1];
      }
//      fnGetDORepContactInfo(repid, function(status, contactInfo) {
//          if(status) {
//              JSONPDF[0].email = contactInfo.email;
//              JSONPDF[0].phone = contactInfo.phone;
//          }
         createPDFPath(function() {
              window.html2pdf.create(template_wp(JSONPDF),"~/Documents/Globus/DO/PDF/WP/"+ JSONPDF[0].DOID +".pdf", // on iOS,
                   function(status) {
                  console.log("newDOID >>>> " + newDOID);
                     if(orderID!=JSONPDF[0].DOID)
                         fnRemoveFile("DO/PDF/WP/" + orderID + ".pdf");
                     window.html2pdf.create(template_wop(JSONPDF),"~/Documents/Globus/DO/PDF/WOP/"+ JSONPDF[0].DOID +".pdf", // on iOS,
                       function(status) {
                         if(orderID!=JSONPDF[0].DOID)
                            fnRemoveFile("DO/PDF/WOP/" + orderID + ".pdf");
                         console.log("DO PDF was generated successfully");
                         fnWriteFile("Globus/DO/Signature", newDOID + ".txt", JSON.stringify(JSONPDF), callback);
                       }, function(status) {
                         console.log(status);
                   });

                   }, function(status) {
                     console.log(status);
               });
            });
//      });
   
    });
}


//DO Split Logic
function fnSplitDO(DO, callback) {
  var NoOfShips = _.uniq(_.pluck(_.pluck(DO.gmTxnShipInfoVO,"ShipInfo"),"addressid"));
  var PartTypes = _.uniq(_.pluck(DO.arrGmOrderItemVO,"parttype"));
  
  // window.DO =DO;

  var OrderExp = new RegExp(DO.orderid,'g');
  var finalDO = new Array();

  // If the Shipping Address is more that one and atleast one part is in Consignment type (50300)
  if((NoOfShips.length>1) && (_.contains(PartTypes,'50300'))) {
  
    for(var i=0;i<NoOfShips.length;i++) {

      //Preparing Child Order
      if(i<(NoOfShips.length-1)) {
          var childDO = $.parseJSON(JSON.stringify(DO));

          delete childDO['arrGmOrderItemAttrVO'];
          delete childDO['arrGmOrderItemVO'];
          delete childDO['arrGmTagUsageVO'];
          delete childDO['gmTxnShipInfoVO'];

          //Prepare Shipping Info
          var ShippingInfo = {}, index = new Array();

          for(var s=0; s<DO.gmTxnShipInfoVO.length;s++) {
            if(DO.gmTxnShipInfoVO[s].ShipInfo.addressid==NoOfShips[i]) {
              ShippingInfo = $.parseJSON(JSON.stringify(DO.gmTxnShipInfoVO[s].ShipInfo));
              index.push(DO.gmTxnShipInfoVO[s].indexValue);
            }
          }
          delete ShippingInfo["indexValue"];
          delete ShippingInfo["carrierName"];
          delete ShippingInfo["modeName"];

          if(ShippingInfo.shipto != "4121")
            ShippingInfo.addressid = "";

          //Preparing Parts Info
          var PartsInfo = $.parseJSON(JSON.stringify(DO.arrGmOrderItemVO));
          var childParts = new Array();
          var total = 0;
          for(var p=0; p<PartsInfo.length;p++) {
            if(index.indexOf(PartsInfo[p].indexValue)>=0) { 
              childParts.push(PartsInfo[p]);
              var strChild = JSON.stringify(PartsInfo[p]);
              DO.arrGmOrderItemVO = $.parseJSON(JSON.stringify(DO.arrGmOrderItemVO).replace(strChild,'').replace(',,',',').replace('[,','[').replace(',]',']'));
              delete PartsInfo[p]["indexValue"];
              total += parseFloat(parseFloat(PartsInfo[p].price) * parseInt(PartsInfo[p].qty));
            }
          }
          console.log(_.difference(DO.arrGmOrderItemVO,childParts));

          //Preparing Parts Attr Info
          var PartsAttrInfo = $.parseJSON(JSON.stringify(DO.arrGmOrderItemAttrVO));
          var childAttr = new Array();
          for(var a=0; a<PartsAttrInfo.length;a++) {
            if(_.contains(index,PartsAttrInfo[a].indexValue)) {
              childAttr.push(PartsAttrInfo[a]);
              var strChild = JSON.stringify(PartsAttrInfo[a]);
              DO.arrGmOrderItemAttrVO = $.parseJSON(JSON.stringify(DO.arrGmOrderItemAttrVO).replace(strChild,'').replace(',,',',').replace('[,','[').replace(',]',']'));
              delete PartsAttrInfo[a]["indexValue"];
            }
          }

          //Preparing TAG Info
          var TagInfo = $.parseJSON(JSON.stringify(DO.arrGmTagUsageVO));
          var childTag = new Array();
          for(var t=0; t<TagInfo.length;t++) {
            if(index.indexOf(TagInfo[t].indexValue)) {
              childTag.push(TagInfo[t]);
              var strChild = JSON.stringify(TagInfo[t]);
              DO.arrGmTagUsageVO = $.parseJSON(JSON.stringify(DO.arrGmTagUsageVO).replace(strChild,'').replace(',,',',').replace('[,','[').replace(',]',']'));
              delete TagInfo[t]["indexValue"];
              delete TagInfo[t]["Name"];
            }
          }

          //Order Attr
          var orderAttr = $.parseJSON(JSON.stringify(DO.arrGmOrderAttrVO));
          for(var o=0;o<orderAttr.length;o++) {
            if(orderAttr[o].attrtype == '4000536')
              delete orderAttr.splice(o,1);
          }

          //Combining child Order data
          childDO.gmTxnShipInfoVO = ShippingInfo;
          childDO.arrGmOrderItemVO = childParts;
          childDO.arrGmOrderItemAttrVO = childAttr;
          childDO.arrGmTagUsageVO = childTag;
          childDO.arrGmOrderAttrVO = orderAttr;
          childDO.total = total;


          childDO = $.parseJSON(JSON.stringify(childDO).replace(OrderExp,''));
          childDO.parentorderid = DO.orderid;

          finalDO.push(childDO);
          console.log(DO);
      }

      //Preparing Parent Order
      else {
           //Prepare Shipping Info
          var ShippingInfo = {};

          for(var s=0; s<DO.gmTxnShipInfoVO.length;s++) {
            if(DO.gmTxnShipInfoVO[s].ShipInfo.addressid==NoOfShips[i]) {
              ShippingInfo = $.parseJSON(JSON.stringify(DO.gmTxnShipInfoVO[s].ShipInfo));
            }
          }
          delete ShippingInfo["indexValue"];
          delete ShippingInfo["carrierName"];
          delete ShippingInfo["modeName"];


          if(ShippingInfo.shipto != "4121")
            ShippingInfo.addressid = "";

          var total = 0;
          for(var x=0;x<DO.arrGmOrderItemVO.length;x++) {
              delete DO.arrGmOrderItemVO[x]["indexValue"];
              total+= parseFloat(parseFloat(DO.arrGmOrderItemVO[x].price) * parseInt(DO.arrGmOrderItemVO[x].qty));
          }
          DO.total = total;

          for(var x=0;x<DO.arrGmOrderItemAttrVO.length;x++) {
              delete DO.arrGmOrderItemAttrVO[x]["indexValue"];
          }

          for(var x=0;x<DO.arrGmTagUsageVO.length;x++) {
              delete DO.arrGmTagUsageVO[x]["indexValue"];
              delete DO.arrGmTagUsageVO[x]["Name"];
          }

          DO.gmTxnShipInfoVO = $.parseJSON(JSON.stringify(ShippingInfo));

          DO.total = total;

          finalDO.push(DO);
      }

    }
    console.log(finalDO);
    callback(finalDO.reverse());
  }
  else {                            // Creating Order with only one Shipping Address or All loaners (No Split incorporated)
      var total = 0;
      for(var x=0;x<DO.arrGmOrderItemVO.length;x++) {
          delete DO.arrGmOrderItemVO[x]["indexValue"];
          total+= parseFloat(parseFloat(DO.arrGmOrderItemVO[x].price) * parseInt(DO.arrGmOrderItemVO[x].qty));
      }
      DO.total = total;

      for(var x=0;x<DO.arrGmOrderItemAttrVO.length;x++) {
          delete DO.arrGmOrderItemAttrVO[x]["indexValue"];
      }

      for(var x=0;x<DO.arrGmTagUsageVO.length;x++) {
          delete DO.arrGmTagUsageVO[x]["indexValue"];
          delete DO.arrGmTagUsageVO[x]["Name"];
      }

      if((DO.gmTxnShipInfoVO.length!=undefined) && (DO.gmTxnShipInfoVO[0]!=undefined))
        DO.gmTxnShipInfoVO = DO.gmTxnShipInfoVO[0].ShipInfo;
      else 
        DO.gmTxnShipInfoVO = DO.gmTxnShipInfoVO;

      if(DO.gmTxnShipInfoVO.source==undefined) 
        DO.gmTxnShipInfoVO = {"source": "", "shipto": "","ship_to_id": "", "shipcarrier": "", "shipmode": "", "addressid": "","attnto": "", "shipinstruction" : ""};
      

      delete DO.gmTxnShipInfoVO["indexValue"];
      delete DO.gmTxnShipInfoVO["carrierName"];
      delete DO.gmTxnShipInfoVO["modeName"];


      if(DO.gmTxnShipInfoVO.shipto != "4121")
        DO.gmTxnShipInfoVO.addressid = "";

      finalDO.push(DO);
      callback(finalDO);

  }

}
//Loads the Server Data for DO
function fnGetWebServiceData(seviceurl,voname, input, callback) {
    var token = localStorage.getItem("token");
    if(input==undefined)
     input = {"token": token};
     input.cmpid  = localStorage.getItem("cmpid");
     input.cmptzone =localStorage.getItem("cmptzone");
     input.cmpdfmt  = localStorage.getItem("cmpdfmt");
     input.plantid =localStorage.getItem("plantid");
    console.log(JSON.stringify(input));

    $.ajax({
        url: URL_WSServer+seviceurl,
        method: "POST",
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify(input),
        success: function (data) {
        	if(voname == undefined)
        		{
        		 console.log(data);
        		 callback(1,data);
        		}
        	else if(data!=null && eval("data."+voname) !=undefined)
        		{
        		callback(1,eval("data."+voname));      		
        		}
        	else
        		{
        		 callback(0,data);  		
        		}
        	
        },
        error: function (model, response, errorReport) {
          console.log("Nothing to be Sync'd from the Server with URL " + URL_WSServer + seviceurl );
          console.log(errorReport);
          console.log(response);
          console.log(model.responseText);
          callback(0,model.responseText);
        }
    });
}



function fnGetReqExtn(passname, reqdtlids, callback) {
    
    var token = localStorage.getItem("token");
    var input = {"token": token, "passname":passname, "reqdtlids": reqdtlids};

      console.log(JSON.stringify(input));

      $.ajax({
        url: URL_WSServer + "loaners/dueloaners",
        method: "POST",
        contentType: "application/json",
        data: JSON.stringify(input),
        datatype: "json",
        success: function (data) {
            callback(eval("data.listGmLoanerDrilldownVO"));
            console.log(eval("data.listGmLoanerDrilldownVO"));
        },
         error: function (jqXHR,status,error) {
            console.log(error);
        }
    });
}


 function fnSubmitReqExtn(input, callback) {
    
      $.ajax({
        url: URL_WSServer + "loanerext/saveloanerext",
        method: "POST",
        contentType: "application/json",
        data: JSON.stringify(input),
        datatype: "json",
        success: function (data) {
            callback(data);
            console.log(data);
        },
        error: function (jqXHR,status,error) {
            console.log(jqXHR.responseText);
            if($.parseJSON(jqXHR.responseText).errorCode=="20644") {
              showNativeAlert("Cannot extend the whole Loaner request.\nSome sets associated to this request are not in Pending Return status.\nPlease make sure that all sets are in Pending Return status.", "Cannot extend", 'OK', function() {
                $("#btn-req-submit").text("Submit");
              });
            }
            else 
              showNativeAlert(error, "Loaner Extension", 'OK');
        }
    });
}



function fnPostCase(casedata,localcainfoid, callback) {
  console.log(casedata);
   var token = localStorage.getItem("token");
   var userid = localStorage.getItem("userID");
   if(casedata.caseid != "") {
      var caseid = casedata.caseid;
      var cainfoid = casedata.cainfoid;
   }
   else {
      var caseid = "";
      var cainfoid = "";
   }
   var surdt = casedata.surdt;
   var did = casedata.did;
   var repid = casedata.repid;
   var acid = casedata.acid;
   var voidfl = casedata.voidfl;
   var prnotes = casedata.prnotes;
   var type = 1006503;
   if(casedata.status !="")
	   var status = casedata.status;
   else
	   var status = "11091";
   var prtcaid = casedata.prtcaid;
   var skipvalidationfl = casedata.skipvalidationfl;
   var arrGmCaseAttributeVO = casedata.arrGmCaseAttributeVO;
   var now = new Date(casedata.sursdate+" "+casedata.surstime+":00");   
   var surtime = this.formatNumber(now.getUTCMonth()+1) +"/"+this.formatNumber(now.getUTCDate())+"/"+now.getUTCFullYear()+" "+this.formatNumber(now.getUTCHours())+":"+this.formatNumber(now.getUTCMinutes())+":"+this.formatNumber(now.getUTCSeconds())+" +00:00"; 
  console.log(surtime);
   var postData = {"token" : token, "userid" : userid, "cainfoid": cainfoid, "caseid": caseid, "surdt": surdt, "surtime": surtime,  "did": did, "repid": repid, "acid": acid, "voidfl": voidfl, "prnotes": prnotes, "type": type, "status": status, "prtcaid": prtcaid, "skipvalidationfl": skipvalidationfl,  "arrGmCaseAttributeVO": arrGmCaseAttributeVO };
    
     console.log(JSON.stringify(postData));
    
    fnGetWebServerData("request/saverequest", undefined, postData,function(data,model,response,errorReport){
      console.log("dataaaaa"+data)  
      console.log(data)
      if(model == undefined ){
             console.log(data);
             if(localcainfoid!= ""){
               fnUpadteCaseLocal(localcainfoid, data, function(){
                     callback(data);
                     fnUpdateCase(function(){
                       fnGetCaseInfoID(function(result){
                          fnDeleteCaseLocalTable(_.pluck(result,'ID'),function(){

                          });
                         });
                     });
                     
               });
             }
             else{
               callback(data);
             }
         }
        else{
            console.log(model.responseText);
             callback(0,model.responseText);
        } 
    },true);

}

function fnRemoveCaseLocal(iValue,result){
  if(iValue < result.length) {
    fnDeleteCaseLocalTable(result[iValue].ID,function(){
      iValue++;
      fnRemoveCaseLocal(iValue,result);
    });
  }
}

function formatNumber(num) {
  var s = num+"";
  while (s.length < 2) s = "0" + s;
  return s;
}


function fnShareCase(casedata, callback) {
   var token = localStorage.getItem("token");
   var arrGmCaseAttributeVO = casedata;     
   
   var postData = {"token" : token, "arrGmCaseAttributeVO": arrGmCaseAttributeVO };

     console.log(JSON.stringify(postData));
    fnGetWebServerData("request/savereqattr", undefined, postData,function(data,model,response,errorReport){
      console.log("dataaaaa"+data)  
      console.log(data)
      if(model == undefined ){
             console.log(data);
             callback(data);
         }
        else{
             console.log(model.responseText);
             callback(0,model.responseText);
        } 
    },true);

}

function fnUpdateCase(callback) {
	 if(!boolSyncing){
  console.log("updates");
   var token = localStorage.getItem("token");    
   
   var postData = {"token" : token, "langid": langid, "pageno": pageno, "uuid" : deviceUUID };

    console.log(JSON.stringify(postData));
     fnGetWebServerData("devicesync/updatescnt", undefined, postData,function(data,model,response,errorReport){
      console.log("dataaaaa"+data)  
      console.log(data)
     if(model == undefined ){
            console.log(data);
            if(data.listAutoUpdates != undefined) {
              console.log("true");
              autoSync(0,callback);
            }
            else {
            	callback();
            }
        }
        else{
           console.log(model.responseText);
        } 
    },true);     

       }else{
    	callback();
    }
}


function fnAutoBookCase(iValue, results) {
  console.log(results);
  if(iValue < results.length) {
    console.log(results[iValue].CASEDATA);
    fnPostCase($.parseJSON(results[iValue].CASEDATA),"", function(data){
      console.log(data);
//     fncreateEvent("update", data.cainfoid, results[iValue].CASE_INFO_ID, $.parseJSON(results[iValue].CASEDATA));
	    fnDeleteCaseLocal(results[iValue].CASE_INFO_ID, function(){
	      iValue++;
	      fnAutoBookCase(iValue, results);
	    });
    });
  }
  
  else {
    fnUpdateCase(function(){
      console.log("Updated");
    });
  }
}



function autoSync(iValue,callback){
  console.log("autoSync");
  var ws;
  var input = '';
  var syncElements = ["listGmCaseInfoVO", "listGmCaseAttributeVO"];
  ws = syncElements[iValue];
  console.log(ws);
  if(casesync == false) {
	  casesync = true;
	  DataSync_SL(ws,input,function(status) {
	  	casesync = false;
	    iValue++;
	    console.log(status);
	    if (iValue < syncElements.length) {
	      autoSync(iValue,callback);
	    }
	    else{
	      callback();
	    }
	});	  
  }
  else {
	  fnUpdateCase();
  }
}




function fncreateEvent(type, newtitle, oldtitle, data) {
  var dd, mm, yyyy;
  var newdate = new Date(data.sursdate);
  var month = new Array();
    month[0] = "January"; month[1] = "February";month[2] = "March"; month[3] = "April"; month[4] = "May"; month[5] = "June"; month[6] = "July"; month[7] = "August"; month[8] = "September"; month[9] = "October"; month[10] = "November"; month[11] = "December";
  dd = ("0" + newdate.getDate()).slice(-2), mm = month[newdate.getMonth()], yyyy = newdate.getFullYear();
  var startDate = new Date(mm+" "+dd+", "+yyyy+" "+data.surstime+":00");
  var endDate = new Date(data.suredate);

  var location = data.acname;
  var notes = data.prnotes;
  var success = function() { console.log("Success!"); };
  var error = function(message) { console.log("Error!"); console.log(message); };
  var title = data.sname+" - "+data.categoriename;
  
  if(type == "new") {
    // var title = newtitle+" Case Event";
//    window.plugins.calendar.deleteEvent(title,location,notes,startDate,endDate,success,error);
//    window.plugins.calendar.createEvent(title,location,notes,startDate,endDate, success, error);
  }
  else if (type == "delete") {
   // var title = newtitle+" Case Event";
//   window.plugins.calendar.deleteEvent(title,location,notes,startDate,endDate,success,error);
  }
  else {
    // var title = oldtitle+" Case Event";
    // var newTitle = newtitle+" Case Event";
//    window.plugins.calendar.modifyEvent(title,location,notes,startDate,endDate,title,location,notes,startDate,endDate,success,error);
  }
  
}

function fnSendICSFile(method,data,sharedEmails,filename,status) {
  console.log(data);
  var subject = filename+" "+status+" Case Event Notification";
  var toemail = sharedEmails;
  document.addEventListener("deviceready", onDeviceReady, false);

    function onDeviceReady() {
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);
    }

    function gotFS(fileSystem) {
        fileSystem.root.getFile(filename+".ics", {create: true, exclusive: false}, gotFileEntry, fail);
    }

    var sampleurl;
    function gotFileEntry(fileEntry) {
      sampleurl = fileEntry.toURL();
        fileEntry.createWriter(gotFileWriter, fail);
    }

    function gotFileWriter(writer) {
        writer.onwriteend = function(evt) {
            console.log("contents of file now 'some sample text'");
              window.plugin.email.open({
            to:      toemail,
            subject: subject,
            attachments: [sampleurl], 
            isHtml:  true

        });
               
        };
        writer.write("BEGIN:VCALENDAR"+"\n"+"VERSION:2.0"+"\n"+"PRODID:-//SYFADIS//PORTAIL FORMATION//FR"+"\n"+"METHOD:"+method+"\n"+data+"END:VCALENDAR");
    }

    function fail(error) {
        console.log(error.code);
    }
}

function fnAutoRequest(iValue, results) {
 
  

  if(iValue < results.length) {
    
      
    sendRequest((results[iValue].REQUESTDATA), function(status,data){
     

       showNativeAlert("Case ID - "+data.caseid +"  has been created Successfully","Success","OK", function() {
                       
                  
          });
        
     fnDeleteRequestLocal(results[iValue].REQUEST_ID, function(){
          iValue++;
         fnAutoRequest(iValue, results); 
       });
    });
  }
  
  
}


function splitRequest(requestData) {

	 var NoOfShips = _.uniq(_.pluck(_.pluck(requestData.arrGmCaseSetInfoVO,"gmTxnShipInfoVO"),"addressid"));
	 console.log(NoOfShips);
	 var arrCaseSetInfo = "";
	 var MainJSONData = new Array();
	 if(NoOfShips.length > 0){
		for(var i=0;i<NoOfShips.length;i++){
			var planShipDate =  _.uniq(_.pluck(requestData.arrGmCaseSetInfoVO,"planshipdt"));
			var reqDate =  _.uniq(_.pluck(requestData.arrGmCaseSetInfoVO,"reqdt"));
			MainJSONData.push({"casetid" : "","cainfoid"  : "", "setid" : "", "systp" : "103644", "reqdt" : reqDate[0], "planshipdt" : planShipDate[0]});
			arrCaseSetInfo = MainJSONData;
			arrCaseSetInfo[i].arrGmPartDetailVO =[];
			arrCaseSetInfo[i].gmTxnShipInfoVO ={};
			var arrShipInfo = _.pluck(_.where(requestData.arrGmCaseSetInfoVO,{"addressid" : NoOfShips[i]}),"gmTxnShipInfoVO");
			var arrPartsInfo = _.pluck(_.where(requestData.arrGmCaseSetInfoVO,{"addressid" : NoOfShips[i]}),"arrGmPartDetailVO");
			
			console.log(arrShipInfo);
			console.log(arrPartsInfo);

      
			
			arrCaseSetInfo[i].gmTxnShipInfoVO.source = arrShipInfo[0].source;
			arrCaseSetInfo[i].gmTxnShipInfoVO.shipto = arrShipInfo[0].shipto;
			arrCaseSetInfo[i].gmTxnShipInfoVO.ship_to_id = arrShipInfo[0].ship_to_id;
			arrCaseSetInfo[i].gmTxnShipInfoVO.shipcarrier = arrShipInfo[0].shipcarrier;
			arrCaseSetInfo[i].gmTxnShipInfoVO.shipmode = arrShipInfo[0].shipmode;
			arrCaseSetInfo[i].gmTxnShipInfoVO.addressid = arrShipInfo[0].addressid;
			arrCaseSetInfo[i].gmTxnShipInfoVO.attnto = arrShipInfo[0].attnto;
			console.log(arrPartsInfo.length);
			for(var len=0;len<arrPartsInfo.length;len++)
				{
					console.log(arrPartsInfo[len][0]);
					arrCaseSetInfo[i].arrGmPartDetailVO.push(arrPartsInfo[len][0]);							
				}
			
			
		}
		console.log($.parseJSON(JSON.stringify(arrCaseSetInfo)));
		 
	 }
	 return $.parseJSON(JSON.stringify(arrCaseSetInfo));
}

function sendRequest(requestdata, callback) {
	
	var data = $.parseJSON(requestdata);

     var token = localStorage.getItem("token");
     var userid = localStorage.getItem("userID");
     var caseid = data.caseid;
     var cainfoid = data.caseinfoid;
     var surdt = data.surdt;
     
     var did = data.did;
     
//     var repid = data.repid;
//     var assocrepid = data.asrepid;
     var acid =  data.acctid;
     var voidfl = data.voidfl;
     var prnotes = data.prnotes;
     var type = data.type;
     var status = "19524";
     var prtcaid = data.prtcaid;
     var skipvalidationfl = data.skipvalidationfl;
     var arrGmCaseAttributeVO = data.arrGmCaseAttributeVO;
     var repid = "";
     var assocrepid = "";
     if(type == 1006505)
    	 {
          repid = data.repid;
          if(data.asrepid != undefined)
        	  assocrepid = data.asrepid.toString();
    	 }
     
     var arrGmCaseSetInfoVO = "";
     if(type == 1006507){
        repid = data.repid;
    	 arrGmCaseSetInfoVO =  splitRequest(data)
      }
     else{
    	 arrGmCaseSetInfoVO = data.arrGmCaseSetInfoVO;
      }
     
     if(repid != undefined)
    	 repid = repid.toString();
     if(acid != undefined)
    	 acid = acid.toString();
     
     console.log(data['arrGmCaseAttributeVO']);
     console.log(data['arrGmCaseSetInfoVO']);
        
     for(var x=0;x<data.arrGmCaseSetInfoVO.length;x++) {
    	 
    	   delete data.arrGmCaseSetInfoVO[x]["indexValue"];
         delete data.arrGmCaseSetInfoVO[x].gmTxnShipInfoVO["indexValue"];
         delete data.arrGmCaseSetInfoVO[x].gmTxnShipInfoVO["carrierName"];
         delete data.arrGmCaseSetInfoVO[x].gmTxnShipInfoVO["modeName"];
     }

      for(l=0;l<arrGmCaseSetInfoVO.length;l++ )
       {
        if(arrGmCaseSetInfoVO[l].gmTxnShipInfoVO["shipto"] != "4121") // if Not Sales Rep Address id id should be empty.
          {
          arrGmCaseSetInfoVO[l].gmTxnShipInfoVO["ship_to_id"] = arrGmCaseSetInfoVO[l].gmTxnShipInfoVO["addressid"];
          arrGmCaseSetInfoVO[l].gmTxnShipInfoVO["addressid"] = "";        
          }    
       }
     
     delete data.arrGmPartDetailVO;
     delete data.gmTxnShipInfoVO;
     

     console.log(requestdata);
      // alert(requestdata);
     
     var postData = {"token" : token, "cainfoid": cainfoid, "caseid": caseid, "surdt": surdt, "surtime": "", "did": did, "repid": repid, "asrepid": assocrepid, "acid": acid, "voidfl": voidfl, "prnotes": prnotes, "type": type, "status": status, "prtcaid": prtcaid, "skipvalidationfl": skipvalidationfl,  "arrGmCaseAttributeVO": arrGmCaseAttributeVO ,"arrGmCaseSetInfoVO" : arrGmCaseSetInfoVO};
     console.log(postData);
       console.log(JSON.stringify(postData));
            // alert(JSON.stringify(postData));
    fnGetWebServerData("request/saverequest", undefined, postData,function(data,model,response,errorReport){
      console.log("dataaaaa"+data)  
      console.log(data)
      if(model == undefined ){
               console.log(data);
               callback(1,data);
           }
        else{
               console.log(model.responseText);
               callback(0,model.responseText);
           }
    },true);

}


function fnCreateEvents() {
  fnGetCalenderEventInfo('', function(data) {
    console.log("fnGetCalenderEventInfo data>>>>>" + data.length);
    callbackFn("success");
    fnCreateEvent(0, data, true, function() {
    	clearedCases = [];
    });
  });
}

function fnCreateEvent(iValue, data, actionFlag, callback) {
	if(iValue<data.length) {
		if((data[iValue].DURATION!="") && (data[iValue].ZONE!="") ) {

          var hr = data[iValue].DURATION.replace("h","");

          var newdate = new Date((data[iValue].surstime).split("+")[0]+"UTC");
          var stime = this.formatNumber(newdate.getHours())+":"+this.formatNumber(newdate.getMinutes());
          var dd,mm,yyyy;
          dd = ("0" + newdate.getDate()).slice(-2), mm = ("0" + (newdate.getMonth() + 1)).slice(-2), yyyy = newdate.getFullYear();
          var month = new Array();
          month[0] = "January"; month[1] = "February";month[2] = "March"; month[3] = "April"; month[4] = "May"; month[5] = "June"; month[6] = "July"; month[7] = "August"; month[8] = "September"; month[9] = "October"; month[10] = "November"; month[11] = "December";
          mm = month[newdate.getMonth()];
          var sursdate = mm+" "+dd+", "+yyyy;
          var starttimehours = (stime).split(":")[0];
          var starttimeminutes = (stime).split(":")[1];
          var endtimeminutes = parseInt(starttimeminutes);
          var endtimehours = parseInt(starttimehours)+ parseInt(hr);
          var date = new Date(sursdate);
          if( endtimehours >= 24) {
                endtimehours = endtimehours - 24;     
                date = new Date(date.getTime() + 24 * 60 * 60 * 1000);
            }
            if( endtimeminutes >= 60) {
                endtimehours = endtimehours +1;
                endtimeminutes = endtimeminutes-60;
             }

            dd = ("0" + date.getDate()).slice(-2), mm = ("0" + (date.getMonth() + 1)).slice(-2), yyyy = date.getFullYear();
            endtimehours = (endtimehours.toString().length == 1) ? "0" + endtimehours : endtimehours;
            endtimeminutes = (endtimeminutes.toString().length == 1) ? "0" + endtimeminutes : endtimeminutes;
            var suretime = yyyy+mm+dd+"T"+endtimehours+endtimeminutes+"00";

            mm = month[date.getMonth()];
            var suredate = mm+" "+dd+", "+yyyy+" "+endtimehours+":"+endtimeminutes+":00";

            newdate = new Date(sursdate);
            dd = ("0" + newdate.getDate()).slice(-2), mm = month[newdate.getMonth()], yyyy = newdate.getFullYear();

            var startDate = new Date(mm+" "+dd+", "+yyyy+" "+stime+":00");
            var endDate = new Date(suredate);

          var assignee = data[iValue].ASSIGNEE;

          var location = data[iValue].ACCOUNT_NM;
          var notes = data[iValue].prnotes;
          var success = function() { console.log("Success!"); fnCreateEvent(iValue+1, data, actionFlag, callback) };
          var error = function(message) { console.log("Error!"); console.log(message); fnCreateEvent(iValue+1, data, actionFlag, callback)};

       
            var title = data[iValue].SURGEON_NAME + " - " + data[iValue].CATEGORY;

            if(actionFlag) {
               window.plugins.calendar.deleteEvent(title,location,notes,startDate,endDate);
               window.plugins.calendar.createEvent(title,location,notes,startDate,endDate, success, error);
            }
            else {
              window.plugins.calendar.deleteEvent(title,location,notes,startDate,endDate, success, error);
            }
	    }
	    else
	      fnCreateEvent(iValue+1, data, actionFlag, callback);
	}
	else
		callback();
}
function fnDeleteCases(VOName, resultSet) {
	var IDs = _.difference(_.pluck(resultSet,'cainfoid'),clearedCases);
	fnProcessSubDeletion(0,IDs,function() {
		console.log("Completed!!!");
		updateLocalDB(VOName, resultSet, fnSyncStatusUpdate);
	})
	
}

function fnProcessSubDeletion(iValue, IDs, callback) {
	var currIDs, increment;
	if((iValue+500)<=IDs.length) {
		currIDs = IDs.splice(iValue,iValue+500);
		increment = 500;
	}
	else {
		currIDs = IDs.splice(iValue,IDs.length);
		increment = IDs.length - iValue;
	}
	
	fnGetCaseEventInfoFor(currIDs, function(data) {
		fnCreateEvent(0, data, false, function() {
			clearedCases = currIDs.concat(clearedCases);
			if(IDs.length>(iValue+increment))
				fnProcessSubDeletion(iValue+increment,IDs,callback);
			else
				callback();
	    });
	});
}

function fnSendFax(file, Number) {
  
   AuthorizedCode = "Z2xvYnVzcHJvZDpHbG9idXMhMjM0";
   
  if(navigator.onLine) {
    $.ajax({
        url: "https://rest.interfax.net/outbound/faxes?faxNumber=" + Number + "&pageOrientation=portrait&pageSize=a4",
        beforeSend: function(xhr) { 
          xhr.setRequestHeader("Authorization", "Basic " + AuthorizedCode); 
        },
        type: 'POST',
        dataType: 'text',
        contentType: 'application/pdf',
        processData: false,
        data: file,
        success: function (data) {
          showNativeAlert("Fax was successfully sent to " + Number + "\n","Fax Sent");
        },
        error: function(jqXHR,status,error){
          console.log(jqXHR);
          console.log(JSON.stringify(jqXHR));
          console.log(error);
          showNativeAlert("Error occurred when trying to send Fax\n" + jqXHR.statusText,"Failed");
        }
    });
  }
  else
    showNativeAlert("Cannot send Fax without Internet connection.\nPlease connect to internet","No Internet");
  
  
}




function fnFetchFieldSalesList() {
	if((navigator.onLine) && (!intOfflineMode)) {
		var input = {"token" : localStorage.getItem('token')};
        
         fnGetWebServerData('salesfilter/morefilter', undefined, input,function(data,model,response,errorReport){
      console.log("dataaaaa"+data)  
      console.log(data)
      if(model == undefined ){
	            if(data.listGmFieldSalesListVO!=undefined) {
	            	var arrData = new Array();
	            	if(data.listGmFieldSalesListVO.length==undefined)
	            		arrData.push(data.listGmFieldSalesListVO)
	            	else
	            		arrData = data.listGmFieldSalesListVO;
	            	
	            	var arrFieldSalesList = _.pluck(arrData,'fieldsalesid');
	            	console.log(arrFieldSalesList);
	            	localStorage.setItem('FieldSalesList',JSON.stringify(arrFieldSalesList));
	            	localStorage.setItem('dteFieldSalesList',new Date());
	            }
	        }
        else{
            console.log(model.responseText);
	            console.log(response);
	            console.log("Nothing to be Sync'd from the Server with URL " + URL_WSServer);
        } 
    },true);

	}
}


function fnCheckDOStatus() {
    if(arrPendingDOList.length!=0) {
        var token = localStorage.getItem("token");
        var doPostData = {"token": token, "stropt": "DOREPORT", "doid": arrPendingDOList.join(","), "passname" :"includevoidedorders", "accountid": "", "fromdate": "", "todate": ""};
          fnGetWebServerData("orders/submitted", undefined, doPostData,function(DOInfo,model,response,errorReport){
      console.log("dataaaaa"+DOInfo)  
      console.log(DOInfo)
      if(model == undefined ){
                  var DOStatus = new Array();
                  if(DOInfo.listGmOrdersDrilldownVO!=undefined) {
                      if(DOInfo.listGmOrdersDrilldownVO.length==undefined)
                        DOStatus.push(DOInfo.listGmOrdersDrilldownVO);
                      else
                        DOStatus = DOInfo.listGmOrdersDrilldownVO;

                      DOStatus = _.reject(DOStatus, function(obj){ return obj.status == 'Pending CS Confirmation'; });
                      arrUpdatedDO = _.pluck(DOStatus, 'donum');
                      console.log(DOStatus);
                      for(var i=0;i<DOStatus.length;i++)
                        fnUpdateDOStatus(DOStatus[i].donum,DOStatus[i].status);

                      fnGetReason();
                  }
                 
              }
        else{
           console.log("Error in Checking DO Updates");
                  console.log(model.responseText);
        } 
    },true);
 
    }
 
}


function fnGetDORepContactInfo(repid, callback) {
    var query = "SELECT T101.C101_USER_ID FROM T101_USER T101, T703_SALES_REP T703 WHERE T101.C101_PARTY_ID = T703.C101_PARTY_ID AND T703.C703_SALES_REP_ID = " + repid;
    fnExecuteSelectQuery(query, function(data) {
        var input = {"token" : localStorage.getItem("token")};
        input.userid = data[0].C101_USER_ID;
        fnGetWebServiceData("usercontactinfo",undefined,input,callback);
    });
}
