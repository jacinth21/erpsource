/**********************************************************************************
 * File:        GmMain.js
 * Description: Startup Logic. This file loads first when the app loads.
 *              1. This file has the code to load the necessary support-files for
 *                 the app to run.
 *              2. All CSS are loaded in here
 *              3. Device readiness is checked in this code
 *              4. Dependencies are set before the support-files are loaded
 * Version:     1.0
 * Author:      ejayabalan
 **********************************************************************************/

(function () {

    window.GM = {
        Model: {},
        Collection: {},
        View: {},
        Router: {},
        Template: {},
        Global: {}
    };

    'use strict';

    //Require.js allows us to configure shortcut alias
    require.config({
        waitSeconds: 0, // coded to test if load time out gets resolved // by cskumar

        // The shim config allows us to configure dependencies for
        // scripts that do not call define() to register a module
        shim: {
            underscore: {
                exports: '_'
            },

            backbone: {
                deps: ['underscore', 'jquery'],
                exports: 'Backbone'
            },

            fastclick: {
                deps: ['jquery'],
                exports: 'Fastclick'
            },

            jqueryui: {
                deps: ['jquery'],
                exports: 'jqueryui'
            },

            mapui: {
                deps: ['jquery'],
                exports: 'mapui'
            },

            mapclusterer: {
                deps: ['jquery', 'mapui'],
                exports: 'mapclusterer'
            },

            global: {
                deps: ['handlebars', 'handlebars.runtime'],
                exports: 'global'
            },

            gmtTemplate: {
                deps: ['handlebars.runtime'],
                exports: 'gmtTemplate'
            },

            commonutil: {
                deps: ['handlebars', 'handlebars.runtime', 'global'],
                exports: 'commonutil'
            },
            date: {
                deps: ['jquery'],
                exports: 'Date'
            }
        },

        /* Once these paths are defined, the pathnames are the ones referred in the rest of the application code */
        paths: {

            /* External Libraries */
            backbone: '../lib/backbone/backbone-min',
            fastclick: '../lib/fastclick/fastclick',
            handlebars: '../lib/handlebars/handlebars-v4.0.12',
            'handlebars.runtime': '../lib/handlebars/handlebars.runtime-v4.0.12',
            canvas: '../lib/threesixty/heartcode-canvasloader-min',
            jquery: '../lib/jquery/jquery-1.11.0.min',
            jqueryui: '../lib/jquery/jquery-ui.min',
            jcrop: '../lib/jcrop/jcrop',
            underscore: '../lib/underscore/underscore-min',
            mapclusterer: '../lib/map/markerclusterer',
            mapui: '../lib/map/mapui',
            jszip: '../lib/excel/jszip',
            xlsx: '../lib/excel/xlsx',
            timezoneJS: '../lib/timezone-js-master/src/date',
            date: '../lib/date/date',

            /* Common */
            global: '../common/gmGlobal',
            commonutil: '../common/GmCommonUtil',
            server: '../common/gmServer',
            //crm local db calls

            //		 crmdb:'../db/GmCRMDB',
            /* Routers */
            gmrMain: '../router/gmrMain',
            gmrActivity: '../crm/activity/router/gmrActivity',
            gmrPhysicianVisit: '../crm/physicianvisit/router/gmrPhysicianVisit',
            gmrSurgeon: '../crm/surgeon/router/gmrSurgeon',
            gmrLead: '../crm/lead/router/gmrLead',
            gmrMERC: '../crm/merc/router/gmrMERC',
            gmrSalesCall: '../crm/salescall/router/gmrSalesCall',
            gmrTraining: '../crm/training/router/gmrTraining',
            gmrCRF: '../crm/crf/router/gmrCRF',
            gmrFieldSales: '../crm/fieldsales/router/gmrFieldSales',
            gmrPS: '../crm/preceptorship/router/gmrPS',
            gmrMerc: '../crm/merc/router/gmrMerc',

            /* ----------- Application Specific ---------- */

            /* View */
            gmvApp: '../common/view/gmvApp',
            gmvCRMDashboard: '../crm/dashboard/view/gmvCRMDashboard',
            gmvCRMData: '../common/view/gmvCRMData',
            gmvCRMMain: '../common/view/gmvCRMMain',
            gmvCRMTab: '../common/view/gmvCRMTab',
            gmvField: '../common/view/gmvField',
            gmvItem: '../common/view/gmvItem',
            gmvLogin: '../common/view/gmvLogin',
            gmvMain: '../common/view/gmvMain',
            gmvCRMSelectPopup: '../common/view/gmvCRMSelectPopup',
            gmvSearch: '../common/view/gmvSearch',
            gmvActivityCriteria: '../crm/activity/view/gmvActivityCriteria',
            gmvActivityDetail: '../crm/activity/view/gmvActivityDetail',
            gmvActivityFilter: '../crm/activity/view/gmvActivityFilter',
            gmvCRMMap: '../common/view/gmvCRMMap',
            gmvMoreFilter: '../common/view/gmvMoreFilter',

            gmvPhysicianVisitCriteria: '../crm/physicianvisit/view/gmvPhysicianVisitCriteria',
            gmvPhysicianVisitDetail: '../crm/physicianvisit/view/gmvPhysicianVisitDetail',
            gmvPhysicianVisitFilter: '../crm/physicianvisit/view/gmvPhysicianVisitFilter',
            gmvLeadCriteria: '../crm/lead/view/gmvLeadCriteria',
            gmvLeadDetail: '../crm/lead/view/gmvLeadDetail',
            gmvLeadFilter: '../crm/lead/view/gmvLeadFilter',
            gmvTradeShowDetail: '../crm/lead/view/gmvTradeShowDetail',
            gmvMerc: '../crm/merc/view/gmvMerc',
            gmvMercCriteria: '../crm/merc/view/gmvMercCriteria',
            gmvMERCDetail: '../crm/merc/view/gmvMERCDetail',
            gmvMERCFilter: '../crm/merc/view/gmvMERCFilter',
            gmvTrainingCriteria: '../crm/training/view/gmvTrainingCriteria',
            gmvTrainingDetail: '../crm/training/view/gmvTrainingDetail',
            gmvTrainingFilter: '../crm/training/view/gmvTrainingFilter',
            gmvCRFCriteria: '../crm/crf/view/gmvCRFCriteria',
            gmvCRFDetail: '../crm/crf/view/gmvCRFDetail',
            gmvCRFFilter: '../crm/crf/view/gmvCRFFilter',
            gmvFieldSalesCriteria: '../crm/fieldsales/view/gmvFieldSalesCriteria',
            gmvFieldSalesDetail: '../crm/fieldsales/view/gmvFieldSalesDetail',
            gmvFieldSalesFilter: '../crm/fieldsales/view/gmvFieldSalesFilter',
            gmvFieldSalesData: '../crm/fieldsales/view/gmvFieldSalesData',
            gmvSalesCallCriteria: '../crm/salescall/view/gmvSalesCallCriteria',
            gmvSalesCallDetail: '../crm/salescall/view/gmvSalesCallDetail',
            gmvSalesCallFilter: '../crm/salescall/view/gmvSalesCallFilter',
            gmvSurgeonCriteria: '../crm/surgeon/view/gmvSurgeonCriteria',
            gmvSurgeonDetail: '../crm/surgeon/view/gmvSurgeonDetail',
            gmvSurgeonSurgicalActivity: '../crm/surgeon/view/gmvSurgeonSurgicalActivity',
            gmvSurgeonSurgicalUtil: '../crm/surgeon/view/gmvSurgeonSurgicalUtil',
            gmvSurgeonSummaryReport: '../crm/surgeon/view/gmvSurgeonSummaryReport',
            gmvSurgeonEducation: '../crm/surgeon/view/gmvSurgeonEducation',
            gmvSurgeonReport: '../crm/surgeon/view/gmvSurgeonReport',
            gmvValidation: '../common/view/gmvValidation',
            gmvItemList: '../common/view/gmvItemList',
            gmvCRMAddress: '../common/view/gmvCRMAddress',
            gmvCRMFileUpload: '../common/view/gmvCRMFileUpload',
            gmvMultiFileUpload: '../common/view/gmvMultiFileUpload',
            gmvImageCrop: '../common/view/gmvImageCrop',
            gmvSurgeonFilter: '../crm/surgeon/view/gmvSurgeonFilter',
            gmvSurgeonSurgicalCalendar: '../crm/surgeon/view/gmvSurgeonSurgicalCalendar',
            gmvAccountSurgeonRpt: '../crm/Account/view/gmvAccountSurgeonRpt',
            gmvAccountSurgicalCalendar: '../crm/Account/view/gmvAccountSurgicalCalendar',
            gmvAccountSurgicalActivity: '../crm/Account/view/gmvAccountSurgicalActivity',
            gmvPSCriteria: '../crm/preceptorship/view/gmvPSCriteria',
            gmvPSSchedule: '../crm/preceptorship/view/gmvPSSchedule',
            gmvPSHost: '../crm/preceptorship/view/gmvPSHost',
            gmvPSCase: '../crm/preceptorship/view/gmvPSCase',
            gmvPSUtil: '../crm/preceptorship/view/gmvPSUtil',
            gmvPSList: '../crm/preceptorship/view/gmvPSList',
            gmvMercReport: '../crm/merc/view/gmvMercReport',

            /* Model */
            gmmItem: '../common/model/gmmItem',
            gmmUser: '../common/model/gmmUser',
            gmmSurgeon: '../crm/surgeon/model/gmmSurgeon',
            gmmSearch: '../common/model/gmmSearch',
            gmmAddress: '../common/model/gmmAddress',
            gmmActivity: '../common/model/gmmActivity',
            gmmFileUpload: '../common/model/gmmFileUpload',
            gmmPS: '../crm/preceptorship/model/gmmPS',
            gmmMerc: '../crm/merc/model/gmmMerc',
            dropdownmodel: '../common/model/GmDropDownModel',

            /* Collection */
            gmcItem: '../common/collection/gmcItem',
            gmcAddress: '../common/collection/gmcAddress',
            gmtTemplate: '../template/template',
            gmvLocalDB: '../common/view/gmvLocalDB',
            dropdowncollection: '../common/collection/GmDropDownCol',

        }
    });

    require(
		[
	        'jquery', 'backbone', 'fastclick', 'global', 'commonutil', 'server', 'gmrMain', 'gmrSurgeon', 'gmrActivity', 'gmrCRF', 'gmrFieldSales', 'gmrPS','gmrMerc', 'gmvApp', 'timezoneJS',
            'date'
         ],
        function ($, Backbone, Fastclick, Global, Commonutil, Server, GMRMain, GMRSurgeon, GMRActivity, GMRCRF, GMRFieldSales, GMRPS,GMRMerc, GMVApp, timezoneJS, Date) {

            $(document).ready(function () {

                initiateHelpers();

                /* Loading Lib CSS */
                loadCSS(URL_CSS + "app/GmReset.css");
                loadCSS(URL_CSS + "lib/font-awesome.css");
                loadCSS(URL_CSS + "lib/jquery-ui.min.css");
                loadCSS(URL_CSS + "lib/jquery.Jcrop.css");
                loadCSS(URL_CSS + "lib/dhtmlxScheduler/dhtmlxscheduler.css");
                /* Loading CSS */

                loadCSS(URL_CSS + "app/GmCRMCommon.css");
                loadCSS(URL_CSS + "app/GmCRMDashboard.css");
                loadCSS(URL_CSS + "app/GmCRMLogin.css");
                loadCSS(URL_CSS + "app/GmCRMSalesCall.css");
                loadCSS(URL_CSS + "app/GmCRMFieldSales.css");
                loadCSS(URL_CSS + "app/GmCRMSurgeon.css");
                loadCSS(URL_CSS + "app/GmCRMSurgeonCalendar.css");
                loadCSS(URL_CSS + "app/GmCRMPhysicianVisit.css");
                loadCSS(URL_CSS + "app/GmCRMLead.css");
                loadCSS(URL_CSS + "app/GmCRMMERC.css");
                loadCSS(URL_CSS + "app/GmCRMTraining.css");
                loadCSS(URL_CSS + "app/GmCRMCRF.css");
                loadCSS(URL_CSS + "app/GmPreceptor.css");
                loadCSS(URL_CSS + "app/GmCRMGrid.css");

                //		document.addEventListener("deviceready", onDeviceReady, false);

                /* PhoneGap is loaded. It is now safe to make calls to PhoneGap functions */
                //		function onDeviceReady() {
                Fastclick.attach(document.body);
                //		}
                GM.Global = $.parseJSON(localStorage.getItem("GlobalVariables"));
                if (GM.Global == null)
                    GM.Global = {};
                // Check if code is running inside portal
                if (self != top) {
                    if (window.parent.crmInfo.token != localStorage.getItem("token")) {
                        GM.Global = {};
                        localStorage.clear();
                    }
                    GM.Global.Portal = true;
                    GM.Global.UserData = $.parseJSON(JSON.stringify(window.parent.crmInfo));
                    //Check if Switch User
                    if (window.parent.crmInfo.switchuser == "Y")
                        GM.Global.SwitchUser = true;
                    localStorage.setItem("userData", JSON.stringify(GM.Global.UserData));
                    userInfoSetup(GM.Global.UserData);
                }


                // Routers
                new GMRMain();
                new GMRActivity();

                $(window).unload(function () {
                    GM.Global.CRMMain = undefined;
                    GM.Global.FSViews = undefined;
                    seen = [];
                    localStorage.setItem("GlobalVariables", JSON.stringify(GM.Global, function (key, val) { // handling circular obj
                        if (val != null && typeof val == "object") {
                            if (seen.indexOf(val) >= 0) {
                                return;
                            }
                            seen.push(val);
                        }
                        return val;
                    }));

                });

                GM.Global.Url = [window.location.hash];
                $(window).on('hashchange', function () { // Used to set last and previous URL
                    if (GM.Global.Portal)
                        GM.Global.PortalUrl = window.location.hash;
                    if (GM.Global.Url == undefined)
                        GM.Global.Url = [];
                    GM.Global.Url.push(window.location.hash);
                    GM.Global.PreUrl = GM.Global.Url[(GM.Global.Url.length - 2)];
                });

                GM.Global.Browser = 1; // Signifies it's GlobusCRM version -- #refer with cskumar if making changes here

                new GMRCRF();
                new GMRSurgeon();
                new GMRFieldSales();
                new GMRPS();
                new GMRMerc();

                gmvApp = new GMVApp();

                Backbone.history.start();

            });
        });
})();
