/**********************************************************************************
 * File:        GmPortalMain.js
 * Description: Startup Logic. This file loads first when the app loads.
 *              1. This file has the code to load the necessary support-files for
 *                 the app to run.
 *              2. All CSS are loaded in here 
 *              3. Device readiness is checked in this code
 *              4. Dependencies are set before the support-files are loaded
 * Version:     1.0
 * Author:      Jreddy
 **********************************************************************************/

(function () {

    window.GM = {
        Model: {},
        Collection: {},
        View: {},
        Router: {},
        Template: {},
        Global: {}
    };

    'use strict';

    //Require.js allows us to configure shortcut alias
    require.config({
        waitSeconds: 0,

        // The shim config allows us to configure dependencies for
        // scripts that do not call define() to register a module
        shim: {
            underscore: {
                exports: '_'
            },

            backbone: {
                deps: ['underscore', 'jquery'],
                exports: 'Backbone'
            },

            fastclick: {
                deps: ['jquery'],
                exports: 'Fastclick'
            },

            jqueryui: {
                deps: ['jquery'],
                exports: 'jqueryui'
            },

            jqueryuitouch: {
                deps: ['jquery', 'jqueryui'],
                exports: 'jqueryuitouch'
            },

            timezoneJS: {
                deps: ['jquery'],
                exports: 'timezoneJS'
            },

            jqueryslider: {
                deps: ['jquery'],
                exports: 'jqueryslider'
            },

            server: {
                deps: ['global'],
                exports: 'server'
            },

            mapui: {
                deps: ['jquery'],
                exports: 'mapui'
            },

            mapclusterer: {
                deps: ['jquery', 'mapui'],
                exports: 'mapclusterer'
            },

            global: {
                deps: ['handlebars', 'handlebars.runtime'],
                exports: 'global'
            },

            pricingCommon: {
                deps: ['handlebars', 'handlebars.runtime'],
                exports: 'pricingApproval'
            },

            gmtTemplate: {
                deps: ['handlebars.runtime'],
                exports: 'gmtTemplate'
            },

            date: {
                deps: ['jquery'],
                exports: 'Date'
            },
            
            commonutil: {
                deps: ['handlebars', 'handlebars.runtime', 'global'],
                exports: 'commonutil'
            }
        },

        /* Once these paths are defined, the pathnames are the ones referred in the rest of the application code */
        paths: {

            /* External Libraries */
            timezoneJS: '../lib/timezone-js-master/src/date',
            backbone: '../lib/backbone/backbone-min',
            fastclick: '../lib/fastclick/fastclick',
            handlebars: '../lib/handlebars/handlebars-v4.0.5',
            'handlebars.runtime': '../lib/handlebars/handlebars.runtime-v4.0.5',
            canvas: '../lib/threesixty/heartcode-canvasloader-min',
            jquery: '../lib/jquery/jquery-1.11.0.min',
            jqueryui: '../lib/jquery/jquery-ui.min',
            jqueryuitouch: '../lib/jquery/jquery.ui.touch-punch.min',
            underscore: '../lib/underscore/underscore-min',
            jqueryscroll: '../lib/jquery/jquery.scrollTo.min',
            jqueryvisible: '../lib/jquery/jquery.visible.min',
            jqueryzoom: '../lib/jquery/jquery.panzoom.min',
            threesixty: '../lib/threesixty/threesixty',
            jqueryslider: '../lib/jquery/jquery.slides.min',
            jcrop: '../lib/jcrop/jcrop',
            mapclusterer: '../lib/map/markerclusterer',
            mapui: '../lib/map/mapui',
            date: '../lib/date/date',

            /* Common */
            global: '../common/gmGlobal',
            server: '../common/gmServer',
            notification: '../common/GmNotification',
            commonutil: '../common/GmCommonUtil',

            /* Database */


            //crm local db calls

            /* Synchronization */

            datasync: '../sync/GmDataSync',

            /* Device */

            /* Routers */
            mainrouter: '../router/gmrPortalMain',
            pricerouter: '../pricing/router/gmrPrice',


            /* ----------- Application Specific ---------- */

            /* Model */
            itemmodel: '../common/model/GmItemModel',
            usermodel: '../user/model/GmUserModel',
            dropdownmodel: '../common/model/GmDropDownModel',
            saleitemmodel: '../common/model/GmSaleItemModel',
            datemodel: '../common/model/GmDateModel',
            searchmodel: '../common/model/GmSearchModel',

            /* View */
            loaderview: '../common/view/GmLoaderView',
            fieldview: '../common/view/GmFieldView',
            homeview: '../home/view/GmHomeView',
            itemlistview: '../common/view/GmItemListView',
            itemview: '../common/view/GmItemView',
            mainview: '../common/view/GmMainView',
            dropdownview: '../common/view/GmDropDownView',
            searchview: '../common/view/GmSearchView',

            daterangeview: '../common/view/GmDateRangeView',

            pricemainview: '../pricing/view/gmvPriceMain',
            priceStatusReport: '../pricing/view/gmvPriceStatusReport',
            priceRequest: '../pricing/view/gmvPriceRequest',
            pricingCommon: '../pricing/view/gmvPricingCommon',

            /* Collection */
            itemcollection: '../common/collection/GmItemCol',
            dropdowncollection: '../common/collection/GmDropDownCol',
            saleitemcollection: '../common/collection/GmSaleItemCol',


            /* Locale */
            i18n: '../startup/i18n',

            // CRM Files
            /* Routers */
            /* gmrSurgeon: '../crm/surgeon/router/gmrSurgeon',*/

            /* ----------- Application Specific ---------- */

            /* View */
            gmvCRMDashboard: '../crm/dashboard/view/gmvCRMDashboard',
            gmvCRMData: '../common/view/gmvCRMData',
            gmvCRMMain: '../common/view/gmvCRMMain',
            gmvCRMTab: '../common/view/gmvCRMTab',
            gmvField: '../common/view/gmvField',

            gmvItem: '../common/view/gmvItem',
            gmvMain: '../common/view/gmvMain',

            gmvCRMSelectPopup: '../common/view/gmvCRMSelectPopup',
            gmvSearch: '../common/view/gmvSearch',
            gmvCRMMap: '../common/view/gmvCRMMap',

            /*gmvSurgeonCriteria: '../crm/surgeon/view/gmvSurgeonCriteria',
            gmvSurgeonDetail: '../crm/surgeon/view/gmvSurgeonDetail',*/
            gmvValidation: '../common/view/gmvValidation',
            gmvItemList: '../common/view/gmvItemList',
            gmvCRMAddress: '../common/view/gmvCRMAddress',
            gmvCRMFileUpload: '../common/view/gmvCRMFileUpload',

            gmvImageCrop: '../common/view/gmvImageCrop',
            /* gmvSurgeonFilter: '../crm/surgeon/view/gmvSurgeonFilter',*/

            /* Model */
            gmmItem: '../common/model/gmmItem',
            /*  gmmSurgeon: '../crm/surgeon/model/gmmSurgeon',*/
            gmmSearch: '../common/model/gmmSearch',
            gmmAddress: '../common/model/gmmAddress',
            gmmActivity: '../common/model/gmmActivity',

            /* Collection */
            gmcItem: '../common/collection/gmcItem',
            gmcAddress: '../common/collection/gmcAddress',

            /* Price Collection and models */
            gmmGroup: '../pricing/model/gmmGroup',
            gmmSystem: '../pricing/model/gmmSystem',
            gmmAccountPriceRequest: '../pricing/model/gmmAccountPriceRequest',
            gmmBusinessQuestions: '../pricing/model/gmmBusinessQuestions',
            gmcGroup: '../pricing/collection/gmcGroup',
            gmcSystem: '../pricing/collection/gmcSystem',
            gmcBusinessQuestions: '../pricing/collection/gmcBusinessQuestions',
            gmcFileUpload: '../common/collection/gmcFileUpload',
            gmmFileUpload: '../common/model/gmmFileUpload',

            gmtTemplate: '../template/template',

            gmvLocalDB: '../common/view/gmvLocalDB'

        }
    });

    //Getting Session Storage information
    GM.Global.portalUserInfo = JSON.parse(window.parent.parent.sessionStorage.userInfo);
    window.localStorage.setItem("token", GM.Global.portalUserInfo.token);
    window.localStorage.setItem("userID", GM.Global.portalUserInfo.userid);
    window.localStorage.setItem("acid", GM.Global.portalUserInfo.acid);
    window.localStorage.setItem("deptid", GM.Global.portalUserInfo.deptid);
    window.localStorage.setItem("cmpid", GM.Global.portalUserInfo.companyid);
    window.localStorage.setItem("cmptzone", GM.Global.portalUserInfo.cmptzone);
    window.localStorage.setItem("cmpdfmt", GM.Global.portalUserInfo.cmpdfmt);
    window.localStorage.setItem("plantid", GM.Global.portalUserInfo.plantid);
    window.localStorage.setItem("cmplangid", GM.Global.portalUserInfo.cmplangid);
    window.localStorage.setItem("Dept", GM.Global.portalUserInfo.deptseq);
    window.localStorage.setItem("cmpcurrsmb", GM.Global.portalUserInfo.cmpcurrsmb);
    window.localStorage.setItem("switchuserFl", GM.Global.portalUserInfo.switchuser);
    require({
        // The local storage variable is checked here to get the locale value, if assigned
        locale: localStorage.getItem('locale')
    }, [
	        'jquery',
	        'backbone',
	        'fastclick',
	        'datasync',
	        'global',
            'pricingCommon',
	        'server',
			'mainrouter',
	        'notification',
            'pricerouter',
			'timezoneJS',
            'date',
            'commonutil'
         ], function ($, Backbone, Fastclick, DataSync, Global, pricingCommon, Server, MainRouter, Notification,
        PriceRouter, timezoneJS, Date, Commonutil
    ) {

        $(document).ready(function () {
            initiateHelpers();

            /* Loading CSS */
            loadCSS(URL_CSS + "app/GmReset.css");
            loadCSS(URL_CSS + "app/GmCommonStyle.css");
            loadCSS(URL_CSS + "app/GmLoginStyle.css");
            loadCSS(URL_CSS + "app/GmCatalogStyle.css");
            loadCSS(URL_CSS + "app/GmCollateralStyle.css");
            loadCSS(URL_CSS + "app/GmDetailViewStyle.css");
            loadCSS(URL_CSS + "app/GmiPhoneStyle.css");
            loadCSS(URL_CSS + "app/GmThreeSixty.css");
            loadCSS(URL_CSS + "lib/font-awesome.css");
            loadCSS(URL_CSS + "app/GmDashboardStyle.css");
            loadCSS(URL_CSS + "app/GmSettingsStyle.css");
            loadCSS(URL_CSS + "lib/jquery-ui.min.css");
            loadCSS(URL_CSS + "lib/jquery.Jcrop.css");
            loadCSS(URL_CSS + "app/GmDOStyle.css");
            loadCSS(URL_CSS + "app/GmAddressStyle.css");
            loadCSS(URL_CSS + "app/GmCaseStyle.css");
            loadCSS(URL_CSS + "app/GmInvStyle.css");
            loadCSS(URL_CSS + "lib/dhtmlxScheduler/dhtmlxscheduler.css");

            /* Loading CRM CSS */

            loadCSS(URL_CSS + "app/GmCRMCommon.css");
            loadCSS(URL_CSS + "app/GmCRMDashboard.css");
            loadCSS(URL_CSS + "app/GmCRMLogin.css");
            loadCSS(URL_CSS + "app/GmCRMSalesCall.css");
            loadCSS(URL_CSS + "app/GmCRMFieldSales.css");
            loadCSS(URL_CSS + "app/GmCRMSurgeon.css");
            loadCSS(URL_CSS + "app/GmCRMPhysicianVisit.css");
            loadCSS(URL_CSS + "app/GmCRMLead.css");
            loadCSS(URL_CSS + "app/GmCRMMERC.css");
            loadCSS(URL_CSS + "app/GmCRMTraining.css");
            loadCSS(URL_CSS + "app/GmCRMCRF.css");
            loadCSS(URL_CSS + "app/GmCRMGrid.css");

            /* Loading Pricing CSS */
            loadCSS(URL_CSS + "app/GmPriceStyle.css");
            loadCSS(URL_CSS + "app/GmPriceCommon.css");

            document.addEventListener("deviceready", onDeviceReady, false); // on app in focus

            /* PhoneGap is loaded. It is now safe to make calls to PhoneGap functions */
            function onDeviceReady() {
                GM.Global = $.parseJSON(localStorage.getItem("GlobalVariables"));
                console.log("GM.GlobalGM.Global")
                console.log(GM.Global)
                if (GM.Global == null)
                    GM.Global = {};
                checkConnection();
                Fastclick.attach(document.body);
                document.addEventListener("online", onOnline, false);
                document.addEventListener("offline", onOffline, false);
                cordova.getAppVersion.getVersionNumber().then(function (appVersion) {
                    version = appVersion;
                });
            }

            if (deviceModel.toUpperCase().indexOf('IPAD') == -1) {
                $('head').append('<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, target-densityDpi=device-dpi" />');
            }

            if (!/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                GM.Global.Browser = 1;
                console.log(GM.Global.Browser)
            } else {
                GM.Global.Browser = 0;
            }



            window.location.href = GM.Global.portalUserInfo.sessionOption;

            new MainRouter();
            new PriceRouter();

            Backbone.history.start();


        });
    });
})();
