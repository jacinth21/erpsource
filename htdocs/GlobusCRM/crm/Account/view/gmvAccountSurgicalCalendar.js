/**********************************************************************************
 * File             :        gmvAccountSurgicalCalendar.js
 * Description      :        View to represent Account Surgical Calendar View
 * Path             :        /web/GlobusCRM/crm/Account/view/gmvAccountSurgicalCalendar.js
 * Author           :        tmuthusamy
 **********************************************************************************/
define([

    // External Libraries
    'jquery', 'underscore', 'backbone', 'handlebars',

    // Common
    'global', 'commonutil',

    // View
     'gmvAccountSurgicalActivity',

    //Model

    //Collection

    // Pre-Compiled Template
    'gmtTemplate'
],

    function ($, _, Backbone, Handlebars,
        Global, Commonutil, GMVAccountSurgicalActivity, GMTTemplate) {

        'use strict';

        GM.View.AccountSurgicalCalendar = Backbone.View.extend({

            name: "CRM Account Surgical Calendar View",

            el: ".surg-calendar-view",

            /* Load the templates */
            template_surgicalCalendarView: fnGetTemplate(URL_Surgeon_Template, "gmtSurgeonCalendarView"),
            template_surgicalSystemDetails: fnGetTemplate(URL_Surgeon_Template, "gmtSurgeonSystemDetails"),


            events: {
                "click .back-to-surg-activity a": "backTosurgActivity",
                "click .dhx_cal_prev_button ": "loadPrevData",
                "click .dhx_cal_next_button": "loadNextData",
                "click .dhx_cal_today_button": "loadToday"
            },

            initialize: function (options) {
                $(window).on('orientationchange', this.onOrientationChange);
                this.render(options);
                this.globalOpt = options;
                var fromDate = options.fromDt;
                var toDate = options.toDt;
                this.state_mode = "month_timeline";
            },

            render: function (options) {
                var that = this;
                var fromDt = options.fromDt;
                var toDt = options.toDt;
                
                this.scheduler1 = Scheduler.getSchedulerInstance();
                
                this.todaySymbol = "";
                this.nextData = "";
                that.fromDate = options.fromDt;
                that.toDate = options.toDt;

                $(".surg-calendar-view").html(this.template_surgicalCalendarView());
                var calInput = {
                    "token": localStorage.getItem("token"),
                    "partyid": GM.Global.Surgeon.Data.accpartyid,
                    "fromdate": fromDt,
                    "todate": toDt
                };
                
                fnGetWebServerData("accountrpt/surgcalinfo", undefined, calInput, function (data) {
                    var data_clr = _.each(data, function (item) {
                        item.segid = "seg" + item.segid;
                    });

                    that.showActivityCalendarView(data_clr, calInput);
                    that.loadSurgicalActCalendar(data_clr);

                });
                
                //Filter Sidebar Toggle function
                
                $(".fsr").addClass("active");

                $(".fsr").on("click", function () {
                    $(this).removeClass("active");
                    $(".fsl").addClass("active");
                    $(this).parent(".panel-adjust-bx").siblings(".controls").hide();
                    $(".search_form").addClass("active");
                    $("#search-panel").addClass("flex_active");
                    that.scheduler1.updateView();
                });

                $(".fsl").on("click", function () {
                    $(this).removeClass("active");
                    $(".fsr").addClass("active");
                    $(this).parent(".panel-adjust-bx").siblings(".controls").show();
                    $(".search_form").removeClass("active");
                    $("#search-panel").removeClass("flex_active");
                    that.scheduler1.updateView();
                });
                
                $(".dhx_cal_today_button").on('click',function(){
                    that.state_mode = that.scheduler1.getState().mode;
                });
                $(".dhx_cal_prev_button").on('click',function(){
                    that.state_mode = that.scheduler1.getState().mode;
                });
                $(".dhx_cal_next_button").on('click',function(){
                    that.state_mode = that.scheduler1.getState().mode;
                });

            },


            //Back button to change the view from calendar to surgical activity
            backTosurgActivity: function () {
                Backbone.history.loadUrl(Backbone.history.fragment);
            },
            
            //Load Spinner
            getLoader: function (divElem) {
                var template = fnGetTemplate(URL_Common_Template, "gmtSpinner");
                $(divElem).html(template());
            },
            
            
            //Load Today Function
            loadToday : function(){
                var that = this;
                that.getLoader(".dhx_cal_data");
                that.nextData = "";
                that.todaySymbol = "T";
                var fdt = that.globalOpt.fromDt;
                var tdt = that.globalOpt.toDt;
                
                that.fromDate = that.globalOpt.fromDt;
                that.toDate = that.globalOpt.toDt;
                
                var nxtInput = {
                        "token": localStorage.getItem("token"),
                        "partyid": GM.Global.Surgeon.Data.accpartyid,
                        "fromdate": fdt,
                        "todate": tdt
                };
                fnGetWebServerData("accountrpt/surgcalinfo", undefined, nxtInput, function (data) {
                    var data_clr = _.each(data, function (item) {
                        item.segid = "seg" + item.segid;
                    });
                    that.showActivityCalendarView(data_clr, nxtInput);
                    that.loadSurgicalActCalendar(data_clr);
                });
            },

            //click Past Button fetching Data
            loadPrevData: function () {
                var that = this;
                that.todaySymbol = "";
                that.nextData = "";
                function monthDiff(d1, d2) {
                    var months;
                    months = (d2.getFullYear() - d1.getFullYear()) * 12;
                    months -= d1.getMonth() + 1;
                    months += d2.getMonth() + 1;
                    return months <= 0 ? 0 : months;
                }

                var from_year = parseInt(that.fromDate.substring(0, 4));
                var from_month = parseInt(that.fromDate.substring(4));

                var to_year = parseInt(that.toDate.substring(0, 4));
                var to_month = parseInt(that.toDate.substring(4));
                var from_dt = ("0" + monthDiff(new Date(from_year, from_month, 1), new Date(to_year, to_month, 1))).slice(-2);

                var dateDiff = parseInt(that.toDate.substring(4)) - parseInt(that.fromDate.substring(4));

                var check_date = that.scheduler1.getState().date.getFullYear() + "" + ("0" + (that.scheduler1.getState().date.getMonth() + 1)).slice(-2);

                if (check_date < that.fromDate) {
                    $(".dhx_cal_prev_button").css({"visibility" : "hidden" , "opacity" : "0"});
                    that.getLoader(".dhx_cal_data");
                    var to_cal = that.scheduler1.getState().date;
                    var past_to_month = ("0" + (to_cal.getMonth() + 1)).slice(-2);
                    var past_to_day = ("0" + (to_cal.getDate())).slice(-2);
                    var past_to_year = to_cal.getFullYear();

                    var toDt = past_to_year + "" + past_to_month;


                    var from_cal = that.scheduler1.getState().date;
                    from_cal.setMonth(from_cal.getMonth() - dateDiff);
                    var past_from_month = ("0" + (from_cal.getMonth() + 1)).slice(-2);
                    var past_from_year = from_cal.getFullYear();

                    var fromDt = past_from_year + "" + past_from_month;

                    var nxtInput = {
                        "token": localStorage.getItem("token"),
                        "partyid": GM.Global.Surgeon.Data.accpartyid,
                        "fromdate": fromDt,
                        "todate": toDt
                    };

                    that.fromDate = nxtInput.fromdate;
                    that.toDate = nxtInput.todate;

                    fnGetWebServerData("accountrpt/surgcalinfo", undefined, nxtInput, function (data) {
                        
                        var data_clr = _.each(data, function (item) {
                            item.segid = "seg" + item.segid;
                        });
                        that.showActivityCalendarView(data_clr, nxtInput);
                        that.loadSurgicalActCalendar(data_clr);
                    });

                }
            },


            //click Next Button fetching Data
            loadNextData: function (event) {

                var that = this;
                that.todaySymbol = "";
                that.nextData = "";
                function monthDiff(d1, d2) {
                    var months;
                    months = (d2.getFullYear() - d1.getFullYear()) * 12;
                    months -= d1.getMonth() + 1;
                    months += d2.getMonth() + 1;
                    return months <= 0 ? 0 : months;
                }

                var from_year = parseInt(that.fromDate.substring(0, 4));
                var from_month = parseInt(that.fromDate.substring(4));

                var to_year = parseInt(that.toDate.substring(0, 4));
                var to_month = parseInt(that.toDate.substring(4));
                var from_dt = ("0" + monthDiff(new Date(from_year, from_month, 1), new Date(to_year, to_month, 1))).slice(-2);

                var dateDiff = parseInt(that.toDate.substring(4)) - parseInt(that.fromDate.substring(4));

                var check_date = that.scheduler1.getState().date.getFullYear() + "" + ("0" + (that.scheduler1.getState().date.getMonth() + 1)).slice(-2);

                if (check_date > that.toDate) {
                    $(".dhx_cal_next_button").css({"visibility" : "hidden" , "opacity" : "0"});
                    that.getLoader(".dhx_cal_data");
                    that.nextData = "Y";
                    var to_cal = that.scheduler1.getState().date;
                    var past_to_month = ("0" + (to_cal.getMonth() + 1)).slice(-2);
                    var past_to_day = ("0" + (to_cal.getDate())).slice(-2);
                    var past_to_year = to_cal.getFullYear();

                    var fromDt = past_to_year + "" + past_to_month;


                    var from_cal = that.scheduler1.getState().date;
                    from_cal.setMonth(from_cal.getMonth() + dateDiff);
                    var past_from_month = ("0" + (from_cal.getMonth() + 1)).slice(-2);
                    var past_from_year = from_cal.getFullYear();

                    var toDt = past_from_year + "" + past_from_month;

                    var nxtInput = {
                        "token": localStorage.getItem("token"),
                        "partyid": GM.Global.Surgeon.Data.accpartyid,
                        "fromdate": fromDt,
                        "todate": toDt
                    };

                    that.fromDate = nxtInput.fromdate;
                    that.toDate = nxtInput.todate;

                    fnGetWebServerData("accountrpt/surgcalinfo", undefined, nxtInput, function (data) {
                        var data_clr = _.each(data, function (item) {
                            item.segid = "seg" + item.segid;
                        });
                        that.showActivityCalendarView(data_clr, nxtInput);
                        that.loadSurgicalActCalendar(data_clr);
                    });

                } else{
                    that.nextData = "";
                }
                    
            },
            


            //Initial Load data leftsidebar menu and filter Section
            loadSurgicalActCalendar: function (data) {
                var that = this;
                
                //Getting Unique segment names for filter from data

                var container_filter = $("#legend_filters_wrapper");
                var unique_filter_leg = _.uniq(data, _.property('segnm'));
                var alldoArr = _.filter(unique_filter_leg, function (item) {
                    if (item.segnm == 'ALL DO') {
                        return item;
                    }
                });
                var otherSegArr = _.filter(unique_filter_leg, function (item) {
                    if (item.segnm == 'Other') {
                        return item;
                    }
                });
                var without_alldo = _.reject(unique_filter_leg, function (item) {
                    if (item.segnm == 'ALL DO' || item.segnm == 'Other') {
                        return item;
                    }
                });
                
                var sort_filter_seg = _.sortBy(without_alldo, _.property('segnm'));
                var union_filter = _.union(alldoArr, sort_filter_seg , otherSegArr);
                container_filter.empty();
                _.each(union_filter, function (x) {
                    container_filter.append('<label><div class="checkbox dhtml-filter-type"><input type="checkbox"' + 'name="' + x.segid + '"' + ' id="'+ x.segid +'"><span class="checkbox_marker"></span></div><span class="checkbox_text">' + x.segnm + '</span></label><br/>');
                });
                that.scheduler1.locale.labels.year_tab = "Year";
                that.scheduler1.config.readonly = true;


                // default values for filters
                var filters = {},
                    i = 0;
                var filter_obj = _.each(unique_filter_leg, function (item) {
                    filters[item.segid] = true;
                    i++;
                });

                var filter_inputs = $(".filters_wrapper").find("input");

                for (var i = 0; i < filter_inputs.length; i++) {
                    var filter_input = filter_inputs[i];

                    // set initial input value based on filters settings
                    filter_input.checked = filters[filter_input.name];

                    // attach event handler to update filters object and refresh view (so filters will be applied)
                    filter_input.onchange = function () {
                        filters[this.name] = !!this.checked;
                        that.scheduler1.updateView();
                    }
                }

                //filter function
                that.scheduler1.filter_week_timeline = that.scheduler1.filter_two_week_timeline = that.scheduler1.filter_month_timeline = function (id, event) {

                    // display event only if its segment is set to true in filters obj
                    // or it was not defined yet - for newly created event

                    var segCnt = $("#legend_filters_wrapper input:checked").length;

                    // Single filter is selected
                    if (segCnt > 0)
                        if (filters[event.segid])
                            return true;

                    // default, do not display event
                    return false;


                };


                //Popup Event attach for counts on the calendar
                that.scheduler1.attachEvent("onClick", function (id, e){
                    var target_date = e.target.attributes[0].nodeValue;
                    var sys_id = e.target.attributes[1].nodeValue;
                    var date_ord_input = compDateFmt(target_date);
                    if (sys_id != "true" && sys_id != "false") {
                        showPopup();
                        $(".modal-title").text('');
                        $("#div-crm-overlay-content-container").addClass("modalCalView");
                        $(".modalCalView .modal-header").css('border-bottom-width','0');
                        $(".modal-footer").addClass("hide");
                        that.getLoader("#div-crm-overlay-content-main .modal-body");
                        var sysInput = {
                            "token": localStorage.getItem("token"),
                            "partyid": GM.Global.Surgeon.Data.accpartyid,
                            "orddate": date_ord_input,
                            "systemid": sys_id
                        };
                        fnGetWebServerData("accountrpt/surgsysdodtl", undefined, sysInput, function (data) {
                            if(data != null){
                                that.showPopupSystem(data);
                            }
                        });
                    }

                });

            },

            //Initial Load data for Calendar Section counts
            showActivityCalendarView: function (data, nxtInput) {
                var that = this;
                $(".surg-calendar-view").removeClass("hide");
                $(".chartMainWrapper").addClass("hide");
                $("#sur-detail-surgical-actvty").addClass("segCalendar");
                var surg_cal_data = _.each(data, function (item) {
                    item.key = item.sysid;
                    item.start_date = item.surgdt;
                    item.end_date = item.surgdt;

                });
                that.showSurgicalActCalendar(surg_cal_data, nxtInput);
            },

            //System Details Popup Function
            showPopupSystem: function (data) {
                var that = this;
                $(".load-model").addClass("hide");
                var wsdata = _.each(data, function (dt) {
                    dt["surgeonnm"] = parseNull(dt.surgnm) != "" ?  "DO #: " + dt.parentord + " , " + dt.surgnm : "DO #: " + dt.parentord;
                });
                var grpBy = _.groupBy(wsdata, "surgeonnm");
                
                _.each(grpBy, function(dt){
                    var sum = dt.reduce((s, f) => {
                        return Number(s) + Number(f.total);
                    }, 0);
                    dt["gtotal"] = sum;
                    dt["parentord"] = dt[0]["parentord"];
                });
                
                var ggtotal=0;
                var grand_total = _.each(grpBy, function(item){
                    ggtotal +=item.gtotal;
                });
                if(!($(".modal-header").children("span").hasClass("grandTot"))){
                    $(".modal-header").find(".modal-title").before("<span class='grandTot'>"+ compPriceFmt(ggtotal) +"</span>");
                }
                
                if (data.length > 0) {
                    $("#div-crm-overlay-content-container .modal-body").html(that.template_surgicalSystemDetails(grpBy));
                    $(".modalCalView .modal-header").css('border-bottom-width','1px');
                    var ordDate = compDateFmt(data[0].surgdt);
                    $(".modal-title").html(ordDate);
                } else {
                    $("#div-crm-overlay-content-container .modal-body").html("No Data Found");
                }
                //close Popup remove class
                $(".modal-header .close").on('click',function(){
                    $("#div-crm-overlay-content-container").removeClass("modalCalView");
                    $(".modal-header").find(".grandTot").remove();
                    $(".modal-title").text('');
                });
               
                $(".ord_link").on("click", function (e) {
                    that.detailDO(e);
                });

            },



            //Timeline View for showing System names and Counts
            showSurgicalActCalendar: function (data, nxtInput) {
                var that = this;
                
                that.scheduler1.clearAll();
                
                /*Date Format Configuration*/
                that.scheduler1.config.xml_date = "%Y-%m-%d %H:%i";
                that.scheduler1.config.time_step = 60;
                that.scheduler1.config.fix_tab_position = false;
                that.scheduler1.xy.scale_height = 38;
                that.scheduler1.config.limit_drag_out = true;
                that.scheduler1.config.drag_resize= false;

                /*Filter Buttons Text Set*/
                that.scheduler1.locale.labels.dhx_cal_today_button = "TODAY";
                that.scheduler1.locale.labels.week_timeline_tab = "WEEK";
                that.scheduler1.locale.labels.two_week_timeline_tab = "2 WEEKS";
                that.scheduler1.locale.labels.month_timeline_tab = "MONTH";

                var y_unit_data = _.uniq(data, _.property('sysnm'));
                var alldoSys = _.filter(y_unit_data, function (item) {
                    if (item.sysnm == 'ALL DO') {
                        return item;
                    }
                });
                var without_alldoSys = _.reject(y_unit_data, function (item) {
                    if (item.sysnm == 'ALL DO') {
                        return item;
                    }
                });
                var sortSysArr = _.sortBy(without_alldoSys, 'sysnm');
                var sections = _.union(alldoSys,sortSysArr);


                /*Timeline for week*/
                that.scheduler1.createTimelineView({
                    fit_events: true,
                    name: "week_timeline",
                    y_property: "sysid",
                    render: "bar",
                    x_unit: "day",
                    x_date: "%D %j",
                    x_step: 1,
                    x_size: 7,
                    x_length: 7,
                    dx: 150,
                    dy: 41.4,
                    event_dy: 32,
                    section_autoheight: false,
                    full_event__dy: true,
                    y_unit: sections
                });

                /*Timeline for Two weeks*/
                that.scheduler1.createTimelineView({
                    fit_events: true,
                    name: "two_week_timeline",
                    y_property: "sysid",
                    render: "bar",
                    x_unit: "day",
                    x_date: "%D %j",
                    x_step: 1,
                    x_size: 14,
                    x_length: 7,
                    dx: 150,
                    dy: 41.4,
                    event_dy: 32,
                    section_autoheight: false,
                    full_event__dy: true,
                    y_unit: sections
                });


                /*Timeline for Month*/
                that.scheduler1.createTimelineView({
                    fit_events: true,
                    name: "month_timeline",
                    y_property: "sysid",
                    render: "bar",
                    x_unit: "day",
                    x_date: "%D %j",
                    x_step: 1,
                    x_size: 14,
                    x_length: 7,
                    dx: 150,
                    dy: 41.4,
                    event_dy: 32,
                    section_autoheight: false,
                    full_event__dy: true,
                    y_unit: sections
                });



                /*calendar columns set for week start or month start*/
                that.scheduler1.date.week_timeline_start = that.scheduler1.date.two_week_timeline_start = that.scheduler1.date.week_start;
                that.scheduler1.date.month_timeline_start = that.scheduler1.date.month_start;
                that.scheduler1.date.add_month_timeline = function (date, inc) {
                    return that.scheduler1.date.add(date, inc, "month");
                };


                /*Month Event*/
                that.scheduler1.attachEvent("onBeforeViewChange", function (old_mode, old_date, mode, date) {
                    if (mode == "month_timeline") {
                        var year = date.getFullYear();
                        var month = (date.getMonth() + 1);
                        var d = new Date(year, month, 0);
                        var days = d.getDate(); //numbers of day in month
                        that.scheduler1.matrix['month_timeline'].x_size = days;
                        that.scheduler1.matrix['month_timeline'].x_length = days;
                    }
                    return true;
                });

                var weekDateFormat = that.scheduler1.date.date_to_str("%D %j");
                that.scheduler1.templates.week_timeline_scale_date = function (date) {
                    return "<br/>" + weekDateFormat(date);
                };

                that.scheduler1.templates.two_week_timeline_scale_date = that.scheduler1.templates.month_timeline_scale_date = function (date) {
                    return getShortDayName(date) + "<br/>" + date.getDate();
                };

                /*Left calendar Column(y_unit) append values from array*/
                that.scheduler1.templates.week_timeline_scale_label =
                    that.scheduler1.templates.two_week_timeline_scale_label =
                    that.scheduler1.templates.month_timeline_scale_label = function (key, label, section) {
                        return "<div style=\"width:100%\" >\
                   <br/>\
                     <div class=\"event_left_text\" >" + section.sysnm + "</div>";
                    };



                /*Holidays (saturday,sunday) show grey color mode*/
                that.scheduler1.templates.week_timeline_cell_class =
                    that.scheduler1.templates.two_week_timeline_cell_class =
                    that.scheduler1.templates.month_timeline_cell_class = function (evs, date, section) {
                        var day = date.getDay();
                        return day == 0 || day == 6 ? "dhx_cell_holiday" : "";
                    };



                /*Day short name View*/
                function getShortDayName(date) {
                    switch (date.getDay()) {
                        case 0:
                            return "Su";
                        case 1:
                            return "Mo";
                        case 2:
                            return "Tu";
                        case 3:
                            return "We";
                        case 4:
                            return "Th";
                        case 5:
                            return "Fr";
                        case 6:
                            return "Sa";
                        default:
                            return "";
                    }
                }

                /*Event Template*/
                that.scheduler1.templates.event_bar_text = function (start, end, event) {
                    return "<span surgdt='" + event.surgdt + "'" + "sysid='" + event.sysid + "'" + "class='dhx_cal_event-txt" + "'>" + event.cnt + "</span> ";
                };
                
                
                /*scheduler data parsing and date view init function*/
                if (that.todaySymbol == "T") {
                    that.scheduler1.config.limit_start = new Date(1970, 5, 1);
                    that.scheduler1.config.limit_end = new Date();
                    that.scheduler1.config.limit_view = true;
                    that.scheduler1.init('scheduler_cal', new Date(), that.state_mode);
                    that.scheduler1.parse(data, "json");
                }else{
                    that.parseFunc(data, nxtInput);
                }
                
                
            },
            
            /*scheduler set date for initial load and clicking prev button*/
            parseFunc: function (data, nxtInput) {
                var that = this;
                that.scheduler1.config.limit_start = new Date(1970, 5, 1);
                that.scheduler1.config.limit_end = new Date();
                that.scheduler1.config.limit_view = true;
                if (that.nextData == "Y") {
                    var nxtyear = nxtInput.fromdate.substr(0, 4);
                    var nxtmonth = nxtInput.fromdate.substr(4, 6) - 1;
                    that.scheduler1.init('scheduler_cal', new Date(nxtyear, nxtmonth, 1), that.state_mode);
                }else {
                    var nxtyear = nxtInput.todate.substr(0, 4);
                    var nxtmonth = nxtInput.todate.substr(4, 6) - 1;
                    that.scheduler1.init('scheduler_cal', new Date(nxtyear, nxtmonth, 1), that.state_mode);
                }
                that.scheduler1.parse(data, "json");
                $(".dhx_cal_prev_button").css({"visibility" : "visible" , "opacity" : "1"});
                $(".dhx_cal_next_button").css({"visibility" : "visible" , "opacity" : "1"});
            },

            //Function to Show the DO Detail in a new window/popop on clicking the orderID
            detailDO: function (e) {
                var elem = e.currentTarget;
                if ($(elem).attr('parentord') != '') {
                    var ordID = $(elem).attr('parentord');
                    fnSummaryDO(ordID);
                }
            }

        });
        return GM.View.AccountSurgicalCalendar;
    });