/**********************************************************************************
 * File             :        gmvAccountSurgeonRpt.js
 * Description      :        View to represent Surgeon List for Account
 * Version          :        1.0
 * Author           :        tmuthusamy
 **********************************************************************************/
define([

    // External Libraries
    'jquery', 'underscore', 'backbone', 'handlebars',

    // Common
    'global'
],

    function ($, _, Backbone, Handlebars, Global) {

        'use strict';

        GM.View.AccountSurgeonRpt = Backbone.View.extend({

            el: "#div-crm-module",

            name: "CRM Account Surgeon List View",

            container: "#div-crm-overlay-content-container",

            /* Load the templates */

            template_AccntSurgRptList: fnGetTemplate(URL_Surgeon_Template, "gmtSurgeonReportList"),


            events: {
                "click #rpt-legend-icon": "showLegend"
            },

            initialize: function (options) {
                console.log(JSON.stringify(options.accid));
                this.accid = options.accid;
                this.render();
            },

            render: function () {
                var that = this;
                $("#li-edit").addClass("hide");
                $(".gmPanelTitle").html(GM.Global.Surgeon.accnm);
                $("#div-main-content").empty();
                $("#div-main-content").append("<div id='div-surgeon-report-main'><section id='surgeon-report-section' class='gmScrollY'></section></div>");
                var tempSpinner = fnGetTemplate(URL_Common_Template, "gmtSpinner");
                $("#surgeon-report-section").html(tempSpinner);
                var input = {
                    'token': localStorage.getItem('token')
                };
                input.accid = that.accid;
                console.log("Surgeon Report data" + input);
                fnGetWebServerData("accountrpt/surgeonrpt", undefined, input, function (data) {
                    if (data != null) {
                         _.each(data, function(dt){
                            dt["picurl"] = URL_WSServer+"file/src?refgroup=103112&refid=" + dt.partyid + "&reftype=null&num=" + Math.random();
                        });
                        console.log("Surgeon Report data" + JSON.stringify(data));
                        $("#surgeon-report-section").html(that.template_AccntSurgRptList(data));
                    }
                });
            },

            showLegend: function () {
                $("#rpt-legend-detail").slideToggle();
            },

            // Close view
            closeView: function (view) {
                if (view != undefined) {
                    view.unbind();
                    view.undelegateEvents();
                    view.$el.unbind();
                    view.$el.empty();
                    view = undefined;
                    $("#btn-back").unbind();
                }
            }

        });
        return GM.View.AccountSurgeonRpt;
    });