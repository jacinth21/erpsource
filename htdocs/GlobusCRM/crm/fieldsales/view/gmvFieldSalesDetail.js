/********************************************************************************************************************
 * File:                gmvFieldSalesDetail.js
 * Description:         View which loads the view with all activities of Sales Rep
 * Versionkey:          1.0
 * Author:              jgurunathan	 
 ********************************************************************************************************************/
define([

    // External Libraries
    'jquery', 'underscore', 'backbone', 'handlebars', 'jqueryui',

    // Common
    'global', 'gmvSearch',

    // View
    'gmvCRMData', 'gmvCRMSelectPopup', 'gmvFieldSalesData',

    // Model
    'gmmActivity',

    // Pre-Compiled Template
    'gmtTemplate'

],

    function ($, _, Backbone, Handlebars, JqueryUI,
        Global, GMVSearch, 
        GMVCRMData, GMVCRMSelectPopup, GMVFieldSalesData,
        GMMActivity, GMTTemplate) {

        'use strict';

        GM.View.FieldSalesDetail = Backbone.View.extend({

            el: "#div-crm-module",

            name: "CRM FieldSales Detail View",

            /* Load the templates */
            template_MainModule: fnGetTemplate(URL_Common_Template, "gmtCRMMainModule"),
            template_FieldSalesDetail: fnGetTemplate(URL_FieldSales_Template, "gmtFieldSalesDetail"),
            template_FilterButtons: fnGetTemplate(URL_Common_Template, "gmtFilterButtons"),
            
            
            events: {
                "click  .btn-listby": "openPopup",
                "click .gmRepTab li" : "showModule",
                "click .fieldsales-tab" : "toogleTabs",
                
            },

            initialize: function (options) {
                console.log(options);
                var that = this;
                this.repid = options.repid;
                this.userTabs = _.pluck($.parseJSON(localStorage.getItem('userTabs')), "functionID");
                $("#div-app-title").show();
                $("#span-app-title").html("FieldSales");
                this.tabAccess = fnGetAccessInfo("FIELDSALES");
                GM.Global.FSViews = [];
                if(GM.Global.FSViews && GM.Global.FSViews.length){
                    _.each(GM.Global.FSViews, function(v){
                        that.close(v);
                    })
                }
                hideMessages();
                this.productsList = new Array();
                
                 
            },

            render: function () {

                var that = this;
                this.$el.html(this.template_MainModule({ "module": "fieldsales" }));
                    if (!GM.Global.FieldSales)
                            GM.Global.FieldSales = {};
               this.$("#li-save").addClass("hide");
                 $(window).on('orientationchange', this.onOrientationChange);
                if (that.repid != 0) { //in case of edit/detail option
                    var token = localStorage.getItem("token");
                    var input = { "token": token, "repid": that.repid };
                    
                    console.log(input);
                    fnGetWebServerData("fieldsales/repinfo", "gmCRMSalesRepListVO", input, function (data) { //obtaining fieldsales data from server
			          if(data!=null){
                               
                             var userID = data.repid;
                                    getImageDoc(userID, function (data){
                                    console.log("getImageDoc" + JSON.stringify(data));
                                    $(".div-rep-pic-container").find("img").attr("src",data);
                                });
                            }
                       
                        
                        console.log("Sales Rep Info is " + JSON.stringify(data));
                        GM.Global.FieldSales.RepPartyID=data.partyid;
                        that.$("#div-main-content").html(that.template_FieldSalesDetail(data));
                        $(".gmPanelTitle").html(data.firstnm + " " + data.midinitial + " " + data.lastnm);
                        var today = new Date();
                        var dd = today.getDate();
                        var mm = today.getMonth()+1; //January is 0!
                        var yyyy = today.getFullYear();
                        if(dd<10) { dd='0'+dd }
                        if(mm<10) { mm='0'+mm }
                        today = mm+'/'+dd+'/'+yyyy;
                        var dbtoday = yyyy+'/'+mm+'/'+dd;
                        GM.Global.Today=today;
                        console.log(GM.Global.FieldSales.RepPartyID);
                        var surgeoninput={"token":localStorage.getItem("token"),"repid":GM.Global.FieldSales.RepPartyID}
                        var scinput={"token":localStorage.getItem("token"),"partyid":GM.Global.FieldSales.RepPartyID,"actcategory": "105267","actstartdate":today}
                        var pvinput={"token":localStorage.getItem("token"),"actcategory": "105266","actstartdate":today , actstatus: "105761"}
                        console.log(surgeoninput);
                        var leadinput={"token":localStorage.getItem("token"),"partyid":GM.Global.FieldSales.RepPartyID,"actcategory": "105269"}
                            console.log(surgeoninput);
                        if(GM.Global.Device){
                            fnGetMySurgeon(data.repid, GM.Global.FieldSales.RepPartyID, function(scData){
                                console.log(scData)
                                if(scData.length)
                                    $("#span-surgcount").html(scData.length);
                                else
                                    $("#span-surgcount").html(0);
                                var gmvFieldSalesData = new GMVFieldSalesData({ "module": "surgeon", "repid": data.partyid});
                                GM.Global.FSViews.push(gmvFieldSalesData)
                            });
                        }
                        else{
                            fnGetWebServerData("surgeon/info", "gmCRMSurgeonlistVO", surgeoninput, function (sData){
                                 sData=arrayOf(sData);
                                if(sData != "")
                                    $("#span-surgcount").html(sData.length);
                                else
                                    $("#span-surgcount").html(0);
                                var gmvFieldSalesData = new GMVFieldSalesData({ "module": "surgeon", "repid": data.partyid});
                                GM.Global.FSViews.push(gmvFieldSalesData)
                                
                            }, true);
                        }
                            
                            console.log(scinput);
                        if(GM.Global.Device){
                            fnGetMySC(GM.Global.FieldSales.RepPartyID, dbtoday, function(scData){
                                console.log(scData)
                                if(scData.length)
                                    $("#span-sccounts").html(scData.length);
                                else
                                    $("#span-sccounts").html(0);
                                var gmvFieldSalesData = new GMVFieldSalesData({ "module": "salescall", "repid": data.partyid});
                                GM.Global.FSViews.push(gmvFieldSalesData)
                            });
                        }
                        else{
                            fnGetWebServerData("crmactivity/info", "gmCRMActivityVO", scinput, function (scData){
                              scData=arrayOf(scData);
                                 if(scData != "")
                                    $("#span-sccounts").html(scData.length);
                                else
                                    $("#span-sccounts").html(0);
                                var gmvFieldSalesData = new GMVFieldSalesData({ "module": "salescall", "repid": data.partyid});
                                GM.Global.FSViews.push(gmvFieldSalesData)
                            }, true);
                        }
                                
                             console.log(pvinput);
                        if(GM.Global.Device){
                            var setList = {};
                            setList.today = dbtoday;
                            this.setList.actcategory = '105266';
                            fnGetListActivity(setList,"UPC", function(pvData){
                                console.log(pvData)
                                if(pvData.length)
                                    $("#span-pvcount").html(pvData.length);
                                else
                                    $("#span-pvcount").html(0);
                                var gmvFieldSalesData = new GMVFieldSalesData({ "module": "physicianvisit", "repid": data.partyid});
                                GM.Global.FSViews.push(gmvFieldSalesData)
                            });
                        }
                        else{
                            fnGetWebServerData("crmactivity/info", "gmCRMActivityVO", pvinput, function (pvData){
                              pvData=arrayOf(pvData);
                                 if(pvData != "")
                                    $("#span-pvcount").html(pvData.length);
                                else
                                    $("#span-pvcount").html(0);
                                var gmvFieldSalesData = new GMVFieldSalesData({ "module": "physicianvisit", "repid": data.partyid});
                                GM.Global.FSViews.push(gmvFieldSalesData)
                            }, true);
                        }
                        if(GM.Global.Device){
                            fnGetMyLead(GM.Global.FieldSales.RepPartyID, function(leadData){
                                console.log(leadData)
                                if(leadData.length)
                                    $("#span-leadcount").html(leadData.length);
                                else
                                    $("#span-leadcount").html(0);
                                var gmvFieldSalesData = new GMVFieldSalesData({ "module": "lead", "repid": data.partyid});
                                GM.Global.FSViews.push(gmvFieldSalesData)
                            });
                        }
                        else{
                            fnGetWebServerData("crmactivity/info", "gmCRMActivityVO", leadinput, function (leadData){
                              leadData=arrayOf(leadData);
                                 if(leadData != "")
                                    $("#span-leadcount").html(leadData.length);
                                else
                                    $("#span-leadcount").html(0);
                                var gmvFieldSalesData = new GMVFieldSalesData({ "module": "lead", "repid": data.partyid});
                                GM.Global.FSViews.push(gmvFieldSalesData)
                            }, true);
                        }                
                    });
                    this.$("#li-save").addClass("hide");
                }
               
            },

            showModule: function(e) {
                var that = this;
                var modulename = $(e.currentTarget).attr("name");
//                var gmvFieldSalesData = new GMVFieldSalesData({ module: modulename, repid: that.repid});
                $(".div-rep-module").addClass("hide");
                $("#div-rep-module-"+modulename).removeClass("hide");
                
            
            },
             onOrientationChange: function(event) {
                resizeHeadWidth('.gmPanelHead', '.gmBtnGroup', '#panelhead-left');
            },
            toogleTabs: function(e) {
//                colorControl(e, "fieldsales-tab", "gmBGLGrey", "gmBGDarkerGrey gmFontWhite");
                var target = $(e.currentTarget).attr("name");
               if ($(e.currentTarget).hasClass("opened")) {
                  $(e.currentTarget).removeClass("opened");
                   $(".fieldsales-tab").removeClass("gmMediaDisplayNone");
                   $(".ul-fs-datail-top").removeClass("gmMediaDisplayNone");
                   $("#div-rep-module").addClass("gmMediaDisplayNone");
               }
                else{
                   $(e.currentTarget).addClass("opened");
                    $(".fieldsales-tab").addClass("gmMediaDisplayNone");
                    $(e.currentTarget).removeClass("gmMediaDisplayNone");
                    $(".ul-fs-datail-top").addClass("gmMediaDisplayNone");
                    $("#div-rep-module").removeClass("gmMediaDisplayNone");
                }
                
            
            },

            backToPage: function () {
                if (this.repid == 0)
                    window.location.href = "#crm/fieldsales";
                else {
                    Backbone.history.loadUrl("#crm/fieldsales/id/" + this.repid);
                }

            },
            
            close: function(view){
                view.unbind();
                view.undelegateEvents();
                view = undefined;
            }

        });
        return GM.View.FieldSalesDetail;
    });