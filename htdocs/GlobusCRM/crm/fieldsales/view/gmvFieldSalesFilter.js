define([
     // External Libraries
    'jquery', 'underscore', 'backbone', 'handlebars', 'jqueryui',
    
     //Common
    'global',
    
    //View
    'gmvCRMSelectPopup',

    // Pre-Compiled Template
    'gmtTemplate'
], 

function ($, _, Backbone, Handlebars, JqueryUI,
           Global,
           GMVCRMSelectPopup, GMTTemplate) {

    'use strict';
    
    GM.View.FieldSalesFilter = Backbone.View.extend({

        el: "#div-crm-module",

        //Load the templates
        template_MainModule: fnGetTemplate(URL_Common_Template, "gmtCRMMainModule"),
        template_filter: fnGetTemplate(URL_FieldSales_Template, "gmtFieldSalesFilter"),
        template_selected_items: fnGetTemplate(URL_Common_Template, "gmtPopupSelectedItem"),
        
        events: {
            "click #btn-main-save": "saveFilter",
            "click .spn-name-x": "removeName",
            "click #btn-main-cancel": "close",
            "click  .btn-listby"             : "openPopup",
            "click #btn-category-search": "searchFilter",
            "click #btn-filter-selected-criteria": "showSelectedCriteria",
             "click .div-text-selected-crt":  "toggleSelectedItems"
        },

	    initialize: function(options) {
            console.log(options.filterid);  
            this.filterID = options.filterid;
            hideMessages();
            this.arrSelectedList = new Array();
            this.tempJSON = {};
        },
        
         // Render the initial contents when the Fieldsales Filter View is instantiated
        render: function() {
            this.$el.html(this.template_MainModule({"module":"fieldsales"}));
            this.$(".gmPanelHead").addClass("gmBGfieldsales");
            this.$(".gmPanelTitle").html("Create a new Filter");
            this.$("#crm-btn-back").removeClass("hide");
            this.$(".gmBtnGroup #li-save").removeClass("hide");
            this.$(".gmBtnGroup #li-cancel").removeClass("hide");
            $(window).on('orientationchange', this.onOrientationChange);
            this.$("#div-main-content").html(this.template_filter({userID: localStorage.getItem("userID")}));
            $("#div-app-title").show();
            $("#span-app-title").html("Field Sales");
            this.jsnData = {};
            this.jsnData.criteriatyp = "fieldsales";
            this.jsnData.token = localStorage.getItem('token');

           
            this.regionList=new Array();
            this.distributorList = new Array();


            if(this.filterID !=0)
                this.getFilterData();
            
            return this;
        },
        
        //load the data if edit into the view
        getFilterData: function () {
            this.arrAttribute = new Array();
            
            var that = this;
            var input = { "token": localStorage.getItem("token"), "criteriaid": this.filterID };
            fnGetWebServerData("criteria/get", undefined, input, function (data) {
                console.log(data);
                that.$(".gmPanelTitle").html(data.criterianm);
                that.$("#div-fieldsales-phone-title").html(data.criterianm);
                that.$(".txt-fltr-name").val(data.criterianm);
                GM.Global.myFilter = null;
                GM.Global.myFilter= JSON.stringify({"Name":data.criterianm,"ID":data.criteriaid});
                that.jsnData.criteriaid = data.criteriaid;
                data.listGmCriteriaAttrVO = arrayOf(data.listGmCriteriaAttrVO);
                GM.Global.criteriaAttr = [];
                GM.Global.criteriaAttr = data.listGmCriteriaAttrVO;
                _.each(data.listGmCriteriaAttrVO, function (data) {
                    if (data.key == "regid") {
                        that.regionList.push(data.value);
                    }

                    if (data.key == "distid") {
                        that.distributorList.push(data.value);
                    }
                   
                    that.newArray = _.union(that.regionList, that.distributorList);
                });
                
                var saveLocalJSON = {};
                if(that.regionList.toString()!="")
                    saveLocalJSON["regid"] = that.regionList.toString();
                if(that.distributorList.toString()!="")
                    saveLocalJSON["distid"] = that.distributorList.toString();
                
                if(GM.Global.FieldSales==undefined)
                    GM.Global.FieldSales = {"Filter":""};
                
                GM.Global.FieldSales.FilterID = saveLocalJSON;
                console.log(saveLocalJSON)
                GM.Global.FieldSales.Filter =  saveLocalJSON;
                
                if(that.regionList != "")
                    that.getCodeName(that.regionList);
                else if (that.distributorList != "")
                    that.getDistName(that.distributorList);
                else
                    that.popUpTemplate();

                
                _.each(data.listGmCriteriaAttrVO, function(data){
                    delete data.userid;
                    delete data.token;
                    delete data.stropt;
                });
                

            });
        },
         onOrientationChange: function(event) {
                resizeHeadWidth('.gmPanelHead', '.gmBtnGroup', '#panelhead-left');
            },
        getCodeName: function (codeid) {
            var that = this;
            var codeid = codeid.toString();
            if(codeid != "") {
                var input = { "token": localStorage.getItem("token"), "codeid": codeid };
                fnGetWebServerData("search/codelookup", "gmCodeLookUpVO", input, function (data) {
                    data = arrayOf(data);
                    if (that.regionList != "") {
                        var regionResult = _.filter(data, function (data) {
                            return data.codegrp == "REGN";
                        });
                        var regionList = _.pluck(arrayOf(regionResult), "codenm");
                        console.log(regionList.toString().replace(/,/g," / "));
                        var regionID = _.pluck(arrayOf(regionResult), "codeid");
                        that.tempJSON["Region"] = regionList.toString().replace(/,/g," / ");
                        var countVal = regionResult.length;
                        that.defaultVals = regionID;
                        $(".div-popup-region").attr("data-values", that.defaultVals);
                        $(".div-popup-region").find("span").text("(" + countVal + ")");
                        $(".div-popup-region").css("background", "#FDF5E6");

                    }
                    if (that.distributorList != "") {
                        that.getDistName(that.distributorList);
                    }
                    else {
                        that.popUpTemplate();
                    }

                }); 
                
            }
            if (that.distributorList != "")
                that.getDistName(that.distributorList);
        },

        getDistName: function (distid){
            var that = this;
            var distid = distid.toString();
            console.log(distid)
            var distInput = { "token": localStorage.getItem("token"), "repid": distid };
            fnGetWebServerData("search/dist", "gmSalesRepQuickVO", distInput, function (data) {
                    
                console.log(data)
                if (distid != undefined) {
                    data = arrayOf(data);
                    var distList = _.pluck(data, "dname");
                    var distID = _.pluck(data, "did");
                    that.tempJSON["Distributor"] = distList.toString().replace(/,/g," / ");

                    if (distList != null) {
                        var countVal = distList.length;
                        that.defaultVals = distID;
                        $(".div-popup-distributor").attr("data-values", that.defaultVals);
                        $(".div-popup-distributor").find("span").text("(" + countVal + ")");
                        $(".div-popup-distributor").css("background", "#FDF5E6");
                    }
                }
                that.popUpTemplate();
            });
        },


        
        popUpTemplate: function () {
            var that = this;
            var tempArray = new Array();
            GM.Global.FieldSales.FilterName = JSON.stringify(that.tempJSON); 
            console.log(GM.Global.FieldSales.FilterName)
            console.log(JSON.stringify(that.tempJSON))
            $.each(that.tempJSON, function (key, val) {
                if (val != "") {
                    tempArray.push({ "title": key, "value": val });
                }
            });
            var criteriaValues = $.parseJSON(JSON.stringify(tempArray));
            if(criteriaValues != ""){
                $(".list-category-item").html(that.template_selected_items(criteriaValues));
                if(GM.Global.SwitchUser)
                    $("#div-criteia-save").addClass("hide");
            }
                
            $(".div-text-selected-crt").addClass("gmFontfieldsales");
          
        },

        //cancels the filterview and redirects to criteria view
        close: function() {
            window.history.back();
        },
        openPopup: function(event){
            var that = this;
            
            that.systemSelectOptions = {                                                                                                              
                'title': $(event.currentTarget).attr("data-title"),
                'storagekey': $(event.currentTarget).attr("data-storagekey"),
                'webservice': $(event.currentTarget).attr("data-webservice"),   
                'resultVO': $(event.currentTarget).attr("data-resultVO"),
                'IDParameter': $(event.currentTarget).attr("data-IDParameter"),
                'NameParameter': $(event.currentTarget).attr("data-NameParameter"),
                'inputParameters': $(event.currentTarget).attr("data-inputParameters"),
                'module': "fieldSales",
                'event': event,
                'parent': this,
                'moduleLS': GM.Global.FieldSales.FilterID,
                'moduleCLS': GM.Global.FieldSales.FilterName,
                'callback': function(ID, Name){
                    GM.Global.FieldSales.FilterID = ID;
                    GM.Global.FieldSales.FilterName = Name;
                    console.log(ID);
                    console.log(Name)
                }
            };
            
            that.systemSelectView = new GMVCRMSelectPopup(that.systemSelectOptions);
        },   
        
        // Save currently selected data as favorite criteria
        saveFilter: function (event) {
           
            var that =this;
            
            console.log(GM.Global.FieldSales.FilterID);
            if(typeof(GM.Global.FieldSales.FilterID)!="object")
                GM.Global.FieldSales.FilterID = $.parseJSON(GM.Global.FieldSales.FilterID);
            this.favoriteData = GM.Global.FieldSales.FilterID;
            that.jsnData = {};
            var token = localStorage.getItem("token");
            
                var criterianm = this.$(".txt-fltr-name").val();
            console.log(criterianm);
            if (criterianm != "") {
                that.jsnData.criteriatyp = "fieldsales";
                that.jsnData.token = token;
                that.jsnData.criteriaid = that.filterID;
                that.jsnData.criterianm = criterianm;
                var key, value;
                $.each(this.favoriteData, function (key, value) {
                    if(value != "" && key != "token")
                        that.arrAttribute.push({"key": key, "value": value, "attrid": "", "criteriaid": that.jsnData.criteriaid, "voidfl": ""});
                });
                
                var temp1 = [];
                _.each(GM.Global.criteriaAttr, function(data){
                    
                    var chk = "";
                    _.each(that.arrAttribute, function(dt, i){
                        if(dt.key == data.key){
                            dt.attrid = data.attrid;
                            chk = "Y";
                        }
                    });
                    if(chk == ""){
                        data.voidfl = "Y";
                        temp1.push(data);
                    }
                    
                    delete data.cmpid;
                    delete data.cmptzone;
                    delete data.cmpdfmt;
                    delete data.plantid;
                    delete data.partyid;
                });
                if(temp1.length){
                    _.each(temp1, function(data){
                        that.arrAttribute.push(data);
                    });
                }  
                that.jsnData.criteriatyp = "fieldsales";
                that.jsnData.token =  GM.Global.Token;
                that.jsnData.listGmCriteriaAttrVO = $.parseJSON(JSON.stringify(that.arrAttribute));
                console.log(that.jsnData.listGmCriteriaAttrVO);
                fnGetWebServerData("criteria/save", undefined, that.jsnData, function (data) {
                    if (data != null) {
                        console.log(data);
                        GM.Global.myFilter = null;
                        GM.Global.myFilter= JSON.stringify({"Name":data.criterianm,"ID":data.criteriaid});
                        $(".li-new-favorite").addClass("hide");
                        $(".li-exist-favorite").addClass("hide");
                        GM.Global.FilterName = data.criterianm;
                         GM.Global.Filtersave = "true";
                        showSuccess("Favorite is updated successfully");
                        window.location.href = '#crm/fieldsales/list/'+that.filterID;
                        hideMessages();
                    } else
                        showError("Favorite save is failed!");
                });
            } 
            else
                showError("Please enter the Favorite Name");

        },
        // Search category vise
        searchFilter: function () {
            
            window.location.href = '#crm/fieldsales/list';
        },
        showSelectedCriteria: function (event) {
            if($(event.currentTarget).hasClass("details-opened")) {
                $(".details-opened").removeClass("details-opened");
               $(".list-category-item").addClass("hide"); 
            }
            else {
                $(event.currentTarget).addClass("details-opened");
                $(".list-category-item").removeClass("hide"); 
            }
        },
        
        toggleSelectedItems: function (event) {
            if ($("#criteria-selected-itmes").hasClass("gmMediaDisplayNone")) {
                $("#criteria-selected-itmes").removeClass("gmMediaDisplayNone");
                $("#selected-criteria-icon").find(".fa-chevron-down").addClass("fa-chevron-up").removeClass("fa-chevron-down");
            } else {
                $("#criteria-selected-itmes").addClass("gmMediaDisplayNone");
                $("#selected-criteria-icon").find(".fa-chevron-up").addClass("fa-chevron-down").removeClass("fa-chevron-up");
            }
        },
    });

	return GM.View.FieldSalesFilter;
});
