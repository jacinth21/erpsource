define([

  // External Libraries
  'jquery', 'underscore', 'backbone',

  //Common
  'gmvCRMData', 'gmvCRMMain',

  //Views
'gmvCRFCriteria', 'gmvCRFDetail', 'gmvCRFFilter'
],
    function ($, _, Backbone,
        GMVCRMData,  GMVCRMMain,
        GMVCRFCriteria, GMVCRFDetail, GMVCRFFilter) {

        'use strict';

        GM.Router.CRF = Backbone.Router.extend({

            //routes to its respective web page 
            routes: {
//                "crf": "viewCRF",
//                "crf/filter/:filterID"                : "showFilter",
//                "crf/list/:criteriaID"                : "listCRFbyFilter",
                "crf/:controlID/:partyID"             : "CRFDetail",
            },

            //things to execute before the routing
            execute: function(callback, args, name) {
                console.log(GM.Global.CRMMain)
                if(GM.Global.CRMMain==undefined){
                    commonSetUp();
                    GM.Global.Module = "crf"
                    GM.Global.CRMMain = true;
                    var gmvCRMMain = new GMVCRMMain();
                    gmvApp.showView(gmvCRMMain);
                }
                $('.gmTab').removeClass().addClass('gmTab');
                $("#crf").addClass("gmTab tabcrf gmFontWhite");
                $("#crf").addClass("gmTab tabcrf-pending gmFontWhite");
                $("#crf").addClass("gmTab tabcrf-reviewed gmFontWhite");
                if(!$(".alert").hasClass("alertSuccess"))
                    hideMessages();
                if (callback) callback.apply(this, args);
            },

            //View CRF
            viewCRF: function () {
                var gmvCRFCriteria = new GMVCRFCriteria();
                gmvApp.showView(gmvCRFCriteria);

            },
            
            //Show CRF Details by PartyID
            CRFDetail: function (controlID, partyID) {
                var gmvCRFDetail = new GMVCRFDetail({controlid : controlID, partyid: partyID});
                gmvApp.showView(gmvCRFDetail);
            },
            
            // List CRF by selected filter values
            listCRFbyFilter: function (criteriaID) {
                console.log(criteriaID);
                if (GM.Global.CRF == undefined) {
                    GM.Global.CRF = {}
                    GM.Global.CRF.Filter = null;
                }
                if (GM.Global.SalesRep)
                    GM.Global.CRF["criteriaid"] = "0";
                else
                    GM.Global.CRF["criteriaid"] = criteriaID;
                
                var gmvCRMData = new GMVCRMData();
                gmvApp.showView(gmvCRMData);
            },
            showFilter: function (FilterID) {
                var gmvTrainingCRF = new GMVCRFFilter({filterid: FilterID});
                gmvApp.showView(gmvCRFFilter);
            },
        });
        return GM.Router.CRF;
    });