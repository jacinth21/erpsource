define([
     // External Libraries
    'jquery', 'underscore', 'backbone', 'handlebars', 'jqueryui',
    
     //Common
    'global',
    
    //View
    'gmvCRMSelectPopup',

    // Pre-Compiled Template
    'gmtTemplate'
], 

function ($, _, Backbone, Handlebars, JqueryUI,
           Global,
           GMVCRMSelectPopup, GMTTemplate) {

    'use strict';
    
    GM.View.SurgeonFilter = Backbone.View.extend({

        el: "#div-crm-module",

        //Load the templates
        template_MainModule: fnGetTemplate(URL_Common_Template, "gmtCRMMainModule"),
        template_filter: fnGetTemplate(URL_Surgeon_Template, "gmtSurgeonFilter"),
        template_selected_items: fnGetTemplate(URL_Common_Template, "gmtPopupSelectedItem"),
        
        events: {
            "click #btn-main-save": "saveFilter",
            "click .spn-name-x": "removeName",
            "click #btn-main-cancel": "close",
            "click  .btn-listby"             : "openPopup",
            "click #btn-category-search": "searchFilter",
            "click #btn-filter-selected-criteria": "showSelectedCriteria",
            "change #surgeon-frmdate, #surgeon-todate": "setMinDate",
            "click .div-text-selected-crt":  "toggleSelectedItems"
        },

	    initialize: function(options) {
            $(window).on('orientationchange', this.onOrientationChange);
            this.filterID = options.filterid;
            hideMessages();
            this.arrSelectedList = new Array();
            this.tempJSON = {};
            $("#div-app-title").show();
            $("#span-app-title").html("Surgeon");
        },
        
         // Render the initial contents when the Surgeon Filter View is instantiated
        render: function() {
            this.$el.html(this.template_MainModule({"module":"surgeon"}));
            this.$(".gmPanelHead").addClass("gmBGsurgeon");
            this.$(".gmPanelTitle").html("Create a new Filter");
            this.$("#crm-btn-back").removeClass("hide");
            this.$(".gmBtnGroup #li-save").removeClass("hide");
            this.$(".gmBtnGroup #li-cancel").removeClass("hide");
             GM.Global.Filtersave = "false";

            this.$("#div-main-content").html(this.template_filter());
            this.jsnData = {};
            this.jsnData.criteriatyp = "surgeon";
            this.jsnData.token = localStorage.getItem('token');

            this.stateList = new Array();
            this.productsList = new Array();
            this.techqList = new Array();
            this.skillList = new Array();
            this.roleList = new Array();
            this.activityList = new Array();

            if(this.filterID !=0)
                this.getFilterData();
            resizeHeadWidth('.gmPanelHead', '.gmBtnGroup', '#panelhead-left');
            return this;
        },
        
        //load the data if edit into the view
        getFilterData: function () {
            this.arrAttribute = new Array();
            
            var that = this;
            var input = { "token": localStorage.getItem("token"), "criteriaid": this.filterID };
            fnGetWebServerData("criteria/get", undefined, input, function (data) {
                that.$(".gmPanelTitle").html(data.criterianm);
                that.$("#div-surgeon-phone-title").html(data.criterianm);
                that.$(".txt-sur-fltr-name").val(data.criterianm);
                GM.Global.myFilter = null;
                GM.Global.myFilter= JSON.stringify({"Name":data.criterianm,"ID":data.criteriaid});
                that.jsnData.criteriaid = data.criteriaid;
                data.listGmCriteriaAttrVO = arrayOf(data.listGmCriteriaAttrVO);
                GM.Global.criteriaAttr = [];
                GM.Global.criteriaAttr = data.listGmCriteriaAttrVO;
                _.each(data.listGmCriteriaAttrVO, function (data) {
                    
                    if (data.key == "actfrmdate") {
                        that.frmdate = data.value;
                        console.log(that.frmdate);
                        that.$("#surgeon-frmdate").val(data.value);
                        
//                        var frmdate = new Date(data.value);
//                        var day = ("0" + frmdate.getDate()).slice(-2);
//                        var month = ("0" + (frmdate.getMonth() + 1)).slice(-2);
//                        var actfrmdate = frmdate.getFullYear()+"-"+(month)+"-"+(day) ;
                        
                        var frmdt = data.value.split("/");
                        that.$("#surgeon-frmdate").val(frmdt[2]+"-"+frmdt[0]+"-"+frmdt[1]);
                        that.tempJSON["From Date"] = that.frmdate;
                    }
                    if (data.key == "acttodate") {
                        that.todate = data.value;
                        that.$("#surgeon-todate").val(data.value);
                        
//                        var todate = new Date(data.value);
//                        var day = ("0" + todate.getDate()).slice(-2);
//                        var month = ("0" + (todate.getMonth() + 1)).slice(-2);
//                        var crttomdate = todate.getFullYear()+"-"+(month)+"-"+(day) ;
                        
                        var todt = data.value.split("/");
                        that.$("#surgeon-todate").val(todt[2]+"-"+todt[0]+"-"+todt[1]);
                        that.tempJSON["To Date"] = that.todate;
                    }
                    
                    if (data.key == "systemid") {
                        that.productsList.push(data.value);
                    }

                    if (data.key == "state") {
                        that.stateList.push(data.value);
                    }
                    if (data.key == "procedure") {
                        that.techqList.push(data.value);
                    }
                    if (data.key == "specality") {
                        that.skillList.push(data.value);
                    }
                    if (data.key == "acttyp") {
                        that.activityList.push(data.value);
                    }
                    if (data.key == "role") {
                        that.roleList.push(data.value);
                    }
                    that.newArray = _.union(that.stateList, that.techqList, that.skillList, that.activityList, that.roleList);
                });
                
                var saveLocalJSON = {};
                if(that.productsList.toString()!="")
                    saveLocalJSON["systemid"] = that.productsList.toString();
                if(that.stateList.toString()!="")
                    saveLocalJSON["state"] = that.stateList.toString();
                if(that.techqList.toString()!="")
                    saveLocalJSON["procedure"] = that.techqList.toString();
                if(that.skillList.toString()!="")
                    saveLocalJSON["specality"] = that.skillList.toString();
                if(that.activityList.toString()!="")
                    saveLocalJSON["acttyp"] = that.activityList.toString();
                if(that.roleList.toString()!="")
                    saveLocalJSON["role"] = that.roleList.toString();
                if(that.frmdate!=undefined && that.frmdate.toString()!="")
                    saveLocalJSON["actfrmdate"] = that.frmdate;
                if(that.todate!=undefined && that.frmdate.toString()!="")
                    saveLocalJSON["acttodate"] = that.todate;
                if(GM.Global.Surgeon==undefined)
                    GM.Global.Surgeon = {"Filter":""};
                
                GM.Global.Surgeon.FilterID = saveLocalJSON;
                console.log(saveLocalJSON)
                
                if(that.newArray != "")
                    that.getCodeName(that.newArray);
                else if(that.productsList != "")
                    that.getSystemName(that.productsList);
                else
                    that.popUpTemplate();
                
                _.each(data.listGmCriteriaAttrVO, function(data){
                    delete data.userid;
                    delete data.token;
                    delete data.stropt;
                });
                

            });
        },
        getSystemName: function (sysid) {
            var that = this;
            var sysid = sysid.toString();
            var systemInput = { "token": localStorage.getItem("token"), "sysid": sysid };
            fnGetWebServerData("search/system", "gmSystemBasicVO", systemInput, function (data) {
                data = arrayOf(data);
                if (sysid != undefined) {
                    var systemList = _.pluck(data, "sysnm");
                    var systemID = _.pluck(data, "sysid");
                    that.tempJSON["Product Used"] = systemList.toString().replace(/,/g," / ");

                    if (systemList != null) {
                        var countVal = systemList.length;
                        that.defaultVals = systemID;
                        $(".div-popup-system").attr("data-values", that.defaultVals);
                        $(".div-popup-system").find("span").text("(" + countVal + ")");
                        $(".div-popup-system").css("background", "#FDF5E6");
                    }
                }
                that.popUpTemplate();
            });          
        },
        getCodeName: function (codeid) {
            var that = this;
            var codeid = codeid.toString();
            if(codeid != "") {
                var input = { "token": localStorage.getItem("token"), "codeid": codeid };
                fnGetWebServerData("search/codelookup", "gmCodeLookUpVO", input, function (data) {
                    console.log(data);
                    data = arrayOf(data);
                    console.log(that.stateList)
                    if (that.stateList != "") {
                        var stateResult = _.filter(data, function (data) {
                            return data.codegrp == "STATE";
                        });
                        var stateList = _.pluck(arrayOf(stateResult), "codenm");
                        console.log(stateList.toString().replace(/,/g," / "));
                        var stateID = _.pluck(arrayOf(stateResult), "codeid");
                        that.tempJSON["Location"] = stateList.toString().replace(/,/g," / ");
                        var countVal = stateResult.length;
                        that.defaultVals = stateID;
                        console.log(that.defaultVals)
                        $(".div-popup-state").attr("data-values", that.defaultVals);
                        $(".div-popup-state").find("span").text("(" + countVal + ")");
                        $(".div-popup-state").css("background", "#FDF5E6");

                    }
                    if (that.techqList != "") {
                        var techqResult = _.filter(data, function (data) {
                            return data.codegrp == "TECHQ";
                        });
                        var techqList = _.pluck(arrayOf(techqResult), "codenm");
                        var techqID = _.pluck(arrayOf(techqResult), "codeid");
                        that.tempJSON["Technique"] = techqList.toString().replace(/,/g," / ");
                        var countVal = techqResult.length;
                        that.defaultVals = techqID;
                        $(".div-popup-technique").attr("data-values", that.defaultVals);
                        $(".div-popup-technique").find("span").text("(" + countVal + ")");
                        $(".div-popup-technique").css("background", "#FDF5E6");
                    }
                    if (that.skillList != "") {
                        var specalityResult = _.filter(data, function (data) {
                            return data.codegrp == "SGSPC";
                        });
                        var specalityList = _.pluck(arrayOf(specalityResult), "codenm");
                        var specalityID = _.pluck(arrayOf(specalityResult), "codeid");
                        that.tempJSON["Skillset"] = specalityList.toString().replace(/,/g," / ");
                        var countVal = specalityResult.length;
                        that.defaultVals = specalityID;
                        $(".div-popup-skills").attr("data-values", that.defaultVals);
                        $(".div-popup-skills").find("span").text("(" + countVal + ")");
                        $(".div-popup-skills").css("background", "#FDF5E6");
                    }
                    if (that.roleList != "") {
                        var roleResult = _.filter(data, function (data) {
                            return data.codegrp == "ACTROL";
                        });
                        var roleList = _.pluck(arrayOf(roleResult), "codenm");
                        var roleID = _.pluck(arrayOf(roleResult), "codeid");
                        that.tempJSON["Role"] = roleList.toString().replace(/,/g," / ");
                        var countVal = roleResult.length;
                        that.defaultVals = roleID;
                        $(".div-popup-role").attr("data-values", that.defaultVals);
                        $(".div-popup-role").find("span").text("(" + countVal + ")");
                        $(".div-popup-role").css("background", "#FDF5E6");
                    }
                    if (that.activityList != "") {
                        var acttypResult = _.filter(data, function (data) {
                            return data.codegrp == "SGACTP";
                        });
                        var acttypList = _.pluck(arrayOf(acttypResult), "codenm");
                        var acttypID = _.pluck(arrayOf(acttypResult), "codeid");
                        that.tempJSON["Activity Type"] = acttypList.toString().replace(/,/g," / ");
                        var countVal = acttypResult.length;
                        that.defaultVals = acttypID;
                        $(".div-popup-activity").attr("data-values", that.defaultVals);
                        $(".div-popup-activity").find("span").text("(" + countVal + ")");
                        $(".div-popup-activity").css("background", "#FDF5E6");
                    }
                    if (that.productsList != ""){
                        that.getSystemName(that.productsList);
                    }
                    else {
                        that.popUpTemplate();
                    }
                }); 
            }
            else if (that.productsList != ""){
                constant.log(that.productsList);
                that.getSystemName(that.productsList);
            }
        },
        
        popUpTemplate: function () {
            var that = this;
            var tempArray = new Array();
            GM.Global.Surgeon.FilterName = JSON.stringify(that.tempJSON); 
            console.log(GM.Global.Surgeon.FilterName)
            console.log(JSON.stringify(that.tempJSON))
            $.each(that.tempJSON, function (key, val) {
                if (val != "") {
                    tempArray.push({ "title": key, "value": val });
                }
            });
            var criteriaValues = $.parseJSON(JSON.stringify(tempArray));
            if(criteriaValues != ""){
                $(".list-category-item").html(that.template_selected_items(criteriaValues));
                if(GM.Global.SwitchUser)
                    $("#div-criteia-save").addClass("hide");
            }
            else
                console.log("It is empty");

            $(".div-text-selected-crt").addClass("gmFontsurgeon");
            console.log(that.tempJSON);
            GM.Global.Surgeon.FilterName = that.tempJSON;
            console.log(GM.Global.Surgeon.FilterName);
            if(GM.Global.Surgeon.FilterName!="" && typeof(GM.Global.Surgeon.FilterName)!="object")
                GM.Global.Surgeon.FilterName = $.parseJSON(that.tempJSON);
                
        },

        //cancels the filterview and redirects to criteria view
        close: function() {
            window.history.back();
        },
        openPopup: function(event){
            var that = this;
            
            that.systemSelectOptions = {                                                                                                              
                'title': $(event.currentTarget).attr("data-title"),
                'storagekey': $(event.currentTarget).attr("data-storagekey"),
                'webservice': $(event.currentTarget).attr("data-webservice"),   
                'resultVO': $(event.currentTarget).attr("data-resultVO"),
                'IDParameter': $(event.currentTarget).attr("data-IDParameter"),
                'NameParameter': $(event.currentTarget).attr("data-NameParameter"),
                'inputParameters': $(event.currentTarget).attr("data-inputParameters"),
                'module': "surgeon",
                'event': event,
                'parent': this,
                'moduleLS': GM.Global.Surgeon.FilterID,
                'moduleCLS': GM.Global.Surgeon.FilterName,
                'callback': function(ID, Name){
                    GM.Global.Surgeon.FilterID = ID;
                    GM.Global.Surgeon.FilterName = Name;
                    if(GM.Global.Surgeon == undefined)
                        GM.Global.Surgeon = {};
                    GM.Global.Surgeon.FilterID = ID;
                    console.log(ID);
                    console.log(Name)
                }
            };
            
            that.systemSelectView = new GMVCRMSelectPopup(that.systemSelectOptions);
        },
         // search by activity date
        activityDate: function () {
            var that = this;
            var fromdate = $("#surgeon-frmdate").val();
            var todate = $("#surgeon-todate").val();
            console.log(fromdate, todate)
            
            if(fromdate !=undefined && todate !=undefined && fromdate !="" && todate !="") {
                var dd, mm, yyyy, date;
                date = new Date(fromdate);

                dd = ("0" + date.getDate()).slice(-2);
                mm = ("0" + (date.getMonth() + 1)).slice(-2);
                yyyy = date.getFullYear();
                
                if (GM.Global.Surgeon.FilterID == undefined)
                    GM.Global.Surgeon.FilterID = {};
                
                if(GM.Global.Surgeon.FilterName == undefined)
                    GM.Global.Surgeon.FilterName = {};
                
                if(GM.Global.Surgeon.FilterName!="" && typeof(GM.Global.Surgeon.FilterName)!="object")
                    GM.Global.Surgeon.FilterName = $.parseJSON(GM.Global.Surgeon.FilterName);
                if(GM.Global.Surgeon.FilterID!="" && typeof(GM.Global.Surgeon.FilterID)!="object")
                    GM.Global.Surgeon.FilterID = $.parseJSON(GM.Global.Surgeon.FilterID);

                GM.Global.Surgeon.FilterID["actfrmdate"] = mm + "/" + dd + "/" + yyyy;
                GM.Global.Surgeon.FilterName["From Date"] = mm + "/" + dd + "/" + yyyy;
                
                date = new Date(todate);
                dd = ("0" + date.getDate()).slice(-2);
                mm = ("0" + (date.getMonth() + 1)).slice(-2);
                yyyy = date.getFullYear();

                GM.Global.Surgeon.FilterID["acttodate"] = mm + "/" + dd + "/" + yyyy;
                GM.Global.Surgeon.FilterName["To Date"] = mm + "/" + dd + "/" + yyyy;
            }
        },    
        
        // Save currently selected data as favorite criteria
        saveFilter: function (event) {
             GM.Global.Filtersave = "false";
            this.activityDate();
            var that =this;
            
            console.log(GM.Global.Surgeon.FilterID);
//            console.log($.parseJSON(GM.Global.Surgeon.FilterID));
            if(GM.Global.Surgeon.FilterID!="" && typeof(GM.Global.Surgeon.FilterID)!="object")
                    GM.Global.Surgeon.FilterID = $.parseJSON(GM.Global.Surgeon.FilterID);
            this.favoriteData = GM.Global.Surgeon.FilterID;
            console.log(JSON.stringify(this.favoriteData));
            that.jsnData = {};
            var token = localStorage.getItem("token");
            
            var criterianm = this.$(".txt-sur-fltr-name").val();
            
            if (criterianm != "") {
                that.jsnData.criteriatyp = "surgeon";
                that.jsnData.token = token;
                that.jsnData.criteriaid = that.filterID;
                that.jsnData.criterianm = criterianm;
                var key, value;
                $.each(this.favoriteData, function (key, value) {
                    if(value != "" && key != "token")
                        that.arrAttribute.push({"key": key, "value": value, "attrid": "", "criteriaid": that.jsnData.criteriaid, "voidfl": ""});
                });
                var temp1 = [];
                _.each(GM.Global.criteriaAttr, function(data){
                    var chk = "";
                    _.each(that.arrAttribute, function(dt, i){
                        if(dt.key == data.key){
                            dt.attrid = data.attrid;
                            chk = "Y";
                        }
                    });
                    if(chk == ""){
                        data.voidfl = "Y";
                        temp1.push(data);
                    }
                    delete data.cmpid;
                    delete data.cmptzone;
                    delete data.cmpdfmt;
                    delete data.plantid;
                    delete data.partyid;
                });
                if(temp1.length){
                    _.each(temp1, function(data){
                        that.arrAttribute.push(data);
                    });
                }  
                that.jsnData.token =  GM.Global.Token;
                that.jsnData.listGmCriteriaAttrVO = $.parseJSON(JSON.stringify(that.arrAttribute));
                console.log(JSON.stringify(that.jsnData));
                fnGetWebServerData("criteria/save", undefined, that.jsnData, function (data) {
                    if (data != null) {
                        GM.Global.myFilter = null;
                        GM.Global.myFilter= JSON.stringify({"Name":data.criterianm,"ID":data.criteriaid});
                        $(".li-new-favorite").addClass("hide");
                        $(".li-exist-favorite").addClass("hide");
                       GM.Global.FilterName = data.criterianm;
                         GM.Global.Filtersave = "true";
                        showSuccess("Favorite is updated successfully");
                        window.location.href = '#crm/surgeon/list/'+that.filterID;
                        hideMessages();
                    } else
                        showError("Favorite save is failed!");
                });
            } else
                showError("Please enter the Favorite Name");

        },
        // Search category vise
        searchFilter: function () {
            this.activityDate();
            window.location.href = '#crm/surgeon/list/0';
        },

        showSelectedCriteria: function (event) {
            if($(event.currentTarget).hasClass("details-opened")) {
                $(".details-opened").removeClass("details-opened");
               $(".list-category-item").addClass("hide"); 
            }
            else {
                $(event.currentTarget).addClass("details-opened");
                $(".list-category-item").removeClass("hide");
            }
        },
        
        setMinDate: function(e){
            if($("#surgeon-todate").val()!="" && $("#surgeon-todate").val()!=undefined && $("#surgeon-frmdate").val()!=undefined && $("#surgeon-frmdate").val()>$("#surgeon-todate").val())
                    $("#surgeon-todate").val($("#surgeon-frmdate").val());
            var fromDate = $("#surgeon-frmdate").val();
            var toDate = $("#surgeon-todate").val();
            
            if($(e.currentTarget).attr("id") == "surgeon-frmdate")             
                $("#surgeon-todate").attr("min", fromDate);
//            var dd, mm, yyyy;
//            var date = new Date(fromDate);
//            dd = ("0" + date.getDate()).slice(-2), mm = ("0" + (date.getMonth() + 1)).slice(-2), yyyy = date.getFullYear();
//            var startDate = mm + "/" + dd + "/" + yyyy;
//            fromDate = startDate;
//
//            var date = new Date(toDate);
//            dd = ("0" + date.getDate()).slice(-2), mm = ("0" + (date.getMonth() + 1)).slice(-2), yyyy = date.getFullYear();
//            var endDate = mm + "/" + dd + "/" + yyyy;
//            toDate = endDate;
            if(GM.Global.Surgeon == undefined)
                    GM.Global.Surgeon = {};
            if(GM.Global.Surgeon.FilterID == undefined)
                GM.Global.Surgeon.FilterID = {};
            if(GM.Global.Surgeon.FilterName == undefined || GM.Global.Surgeon.FilterName == undefined)
                GM.Global.Surgeon.FilterName = {};
            else if(typeof(GM.Global.Surgeon.FilterID)!="object")
                GM.Global.Surgeon.FilterID = $.parseJSON(GM.Global.Surgeon.FilterID);
            if(typeof(GM.Global.Surgeon.FilterName)!="object")
                GM.Global.Surgeon.FilterName = $.parseJSON(GM.Global.Surgeon.FilterName);

            if (toDate != "" && toDate != "aN/aN/NaN" && fromDate != "" && fromDate != "aN/aN/NaN") {
                GM.Global.Surgeon.FilterName["From Date"] =  getDateByFormat(fromDate);
                GM.Global.Surgeon.FilterName["To Date"] =  getDateByFormat(toDate);
                GM.Global.Surgeon.FilterID["actfrmdate"] = getDateByFormat(fromDate);
                GM.Global.Surgeon.FilterID["acttodate"] = getDateByFormat(toDate);
            } else {
                GM.Global.Surgeon.FilterName["From Date"] =  "";
                GM.Global.Surgeon.FilterName["To Date"] =  "";
                GM.Global.Surgeon.FilterID["actfrmdate"] = "";
                GM.Global.Surgeon.FilterID["acttodate"] = "";
            }

            var tempArray = new Array();
            var result = $.parseJSON(JSON.stringify(GM.Global.Surgeon.FilterName));
            $.each(result, function(key, val) {
                console.log(key + ' is ' + val);
                if(val != "" && val != "aN/aN/NaN")
                tempArray.push({"title":key, "value":val});
            });
            console.log(tempArray);
            var finalJSON = $.parseJSON(JSON.stringify(tempArray));
            console.log(finalJSON);
            if (finalJSON.length > 0) {
                console.log(finalJSON)
                this.$(".list-category-item").html(this.template_selected_items(finalJSON));
                if(GM.Global.SwitchUser)
                    $("#div-criteia-save").addClass("hide");
                this.$("#btn-category-search").show();
            }
            else {
                this.$(".list-category-item").empty();
                this.$("#btn-category-search").hide();
            }

            $(".div-criteria").addClass("gmFontsurgeon");

        },
        
        toggleSelectedItems: function (event) {
            if ($("#criteria-selected-itmes").hasClass("gmMediaDisplayNone")) {
                $("#criteria-selected-itmes").removeClass("gmMediaDisplayNone");
                $("#selected-criteria-icon").find(".fa-chevron-down").addClass("fa-chevron-up").removeClass("fa-chevron-down");
            } else {
                $("#criteria-selected-itmes").addClass("gmMediaDisplayNone");
                $("#selected-criteria-icon").find(".fa-chevron-up").addClass("fa-chevron-down").removeClass("fa-chevron-up");
            }
        },
        
        //Handles the orietaion change in the iPad
        onOrientationChange: function(event) {
            resizeHeadWidth('.gmPanelHead', '.gmBtnGroup', '#panelhead-left');
        },
    });

	return GM.View.SurgeonFilter;
});