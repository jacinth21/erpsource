/**********************************************************************************
 * File             :        gmvSurgeonSurgicalActivity.js
 * Description      :        View to represent Surgeon Details
 * Path             :        /web/GlobusCRM/crm/surgeon/view/gmvSurgeonSurgicalActivity.js
 * Author           :        mahendrand
 **********************************************************************************/
define([

    // External Libraries
    'jquery', 'underscore', 'backbone', 'handlebars',

    // Common
    'global', 'commonutil', 'gmvSearch', 'gmvValidation',

    // View
     'gmvCRMSelectPopup', 'gmvItemList', 'gmvSurgeonSurgicalUtil', 'gmvSurgeonSurgicalCalendar',

    //Model
     'gmmSurgeon',

    //Collection
    'gmcItem',

    // Pre-Compiled Template
    'gmtTemplate'
],

    function ($, _, Backbone, Handlebars,
        Global, Commonutil, GMVSearch, GMVValidation,
        GMVCRMSelectPopup, GMVItemList, GMVSurgeonSurgicalUtil, GMVSurgeonSurgicalCalendar,
        GMMSurgeon,
        GMCItem, GMTTemplate) {

        'use strict';

        GM.View.SurgeonSurgicalActivity = Backbone.View.extend({

            name: "CRM Surgeon Surgical Chart View",

            el: "#sur-detail-surgical-actvty",

            /* Load the templates */
            template_surgicalDetail: fnGetTemplate(URL_Surgeon_Template, "gmtSurgeonSurgicalDetail"),
            template_surgicalLegend: fnGetTemplate(URL_Surgeon_Template, "gmtSurgeonSurgicalLegend"),


            events: {
                "click .ul-chartLegend li": "selectLegends",
                "click .ul-selectAll li": "selectAllLegends",
                "click .surgin-tab": "clickTabs",
                "click .surgin-dr-tab": "clickDateTabs",
                "click .chartHeading": "backToSegment",
                "click #btn-load-chart": "loadChartbyBtn",
                "click .calendar-surg-btn": "showActivityCalendarView",
                "click #btn-surgeon-surgical-account": "openAcctPopup",
                "click .barchart-btn, .piechart-btn, .treemap-btn, .linechart-btn, .stackedbarchart-btn": "selectLegends",
            },

            initialize: function () {
                $(window).on('orientationchange', this.onOrientationChange);
                this.render();
            },

            render: function () {
                var that = this;
                this.rpSelectedtab = "revenue";
                this.ssSelectedtab = "segment";
                this.otSelectedtab = "overall";
                this.SegmentStatus = "segment";
                this.SysSgmntStatus = "segment";
                this.procdata = [];
                this.revdata = [];
                this.mainInput = [];
                this.accountID = "";
                this.fromDate = "";
                this.toDate = "";
                this.$el.html(that.template_surgicalDetail);
                that.getLoader("#chart-container");
                monthData();
                yearData();
                $(".treemap-btn").addClass('active');
                var fromDt = new Date();
                var toDt = new Date();
                //For six months, subtract the month with five because of the start month not needed(if include the start month then that has 7 month Data)
                fromDt.setMonth(fromDt.getMonth() - 5);
                var fromDtmm = ("0" + (fromDt.getMonth() + 1)).slice(-2),
                    fromDtyyyy = fromDt.getFullYear();
                var toDtmm = ("0" + (toDt.getMonth() + 1)).slice(-2),
                    toDtyyyy = toDt.getFullYear();
                fromDt = fromDtyyyy + "" + fromDtmm;
                toDt = toDtyyyy + "" + toDtmm;
                that.fromDate = fromDt;
                that.toDate = toDt;
                var input = {
                    "token": localStorage.getItem("token"),
                    "partyid": GM.Global.Surgeon.Data.partyid,
                    "fromdate": fromDt,
                    "todate": toDt
                };
                this.procChartData(input, function (resData) {
                    that.procdata = resData;
                    console.log("PROCEDURE CHART MAIN DATA>>>" + JSON.stringify(resData));
                });

                this.revChartData(input, function (resData) {
                    that.revdata = resData;
                    console.log("REVENUE CHART MAIN DATA>>>" + JSON.stringify(resData));
                    that.loadtreemapChart('');
                });
            },

            //Change the view from surgical activity to Surgical Calendar view
            showActivityCalendarView: function () {
                var that = this;
                this.closeView(that.gmvSurgeonSurgicalCalendar);
                this.gmvSurgeonSurgicalCalendar = new GMVSurgeonSurgicalCalendar({
                    "fromDt": that.fromDate,
                    "toDt": that.toDate
                });
            },


            // Open Single / Multi Select box  
            // Display Account lists linked for this surgeon
            openAcctPopup: function () {
                var that = this;
                var surgicalOptions = {
                    'title': 'Account',
                    'storagekey': 'account',
                    'IDParameter': 'accid',
                    'NameParameter': 'accnm',
                    'type': 2,
                    'moduleLS': "",
                    'moduleCLS': "",
                    'inputParameters': "",
                    'resultData': GM.Global.hospitalList,
                    'parent': this,
                    'callback': function (ID, Name) {
                        $('#accid').val(ID);
                        $('#btn-surgeon-surgical-account span').html(Name);
                    }
                };

                this.systemSelectView = new GMVCRMSelectPopup(surgicalOptions);
            },

            getDateInputs: function (callback) {
                var dateRange, dr, fromDt, toDt;
                $('.surg-datadr .surgin-dr-tab').each(function (i) {
                    if ($(this).hasClass("selected")) {
                        dateRange = $(this).attr('id');
                    }
                    switch (dateRange) {
                        case '3M':
                            dr = 2;
                            break;
                        case '6M':
                            dr = 5;
                            break;
                        case '1Y':
                            dr = 11;
                            break;
                        case 'custom':
                            break;
                    }
                });
                if (dateRange == 'custom') {
                    var fromDtmm = $("#select-surg-surgstartmonth option:selected").text();
                    fromDtyyyy = $("#select-surg-surgstartyear option:selected").val();
                    var toDtmm = $("#select-surg-surgendmonth option:selected").text();
                    toDtyyyy = $("#select-surg-surgendyear option:selected").val();
                    if (fromDtmm == "MM")
                        fromDtmm = "";
                    if (toDtmm == "MM")
                        toDtmm = "";
                    fromDt = fromDtyyyy + "" + fromDtmm;
                    toDt = toDtyyyy + "" + toDtmm;

                } else {
                    fromDt = new Date();
                    toDt = new Date();
                    fromDt.setMonth(fromDt.getMonth() - dr);
                    var fromDtdd = ("0" + fromDt.getDate()).slice(-2),
                        fromDtmm = ("0" + (fromDt.getMonth() + 1)).slice(-2),
                        fromDtyyyy = fromDt.getFullYear();
                    var toDtdd = ("0" + toDt.getDate()).slice(-2),
                        toDtmm = ("0" + (toDt.getMonth() + 1)).slice(-2),
                        toDtyyyy = toDt.getFullYear();
                    fromDt = fromDtyyyy + "" + fromDtmm;
                    toDt = toDtyyyy + "" + toDtmm;
                }
                callback(fromDt, toDt);
            },

            //Display chart of giving account name, account id, from date, to date on clicking load button
            loadChartbyBtn: function () {
                var inputData = {},
                    that = this,
                    legData;
                that.getDateInputs(function (fromDt, toDt) {
                    if (fromDt.length == 6 && toDt.length == 6) {
                        if (fromDt <= toDt) {
                            console.log("fromDt>" + fromDt + "<<toDt>" + toDt)
                            that.getLoader("#chart-container");
                            inputData.token = localStorage.getItem("token");
                            inputData.partyid = GM.Global.Surgeon.Data.partyid;
                            inputData.fromdate = fromDt;
                            inputData.todate = toDt;
                            that.fromDate = fromDt;
                            that.toDate = toDt;

                            that.procChartData(inputData, function (resData) {
                                that.procdata = resData;
                                if (that.ssSelectedtab == 'system')
                                    legData = systemChartData(resData);
                                else
                                    legData = segmentChartData(resData, []);
                                if (that.rpSelectedtab == 'procedure') {
                                    that.loadLegendData(legData);
                                    that.viewChart();
                                }
                                console.log("PROCEDURE CHART MAIN DATA>>>" + JSON.stringify(that.procdata));
                            });
                            that.revChartData(inputData, function (resData) {
                                that.revdata = resData;
                                if (that.ssSelectedtab == 'system')
                                    legData = systemChartData(resData);
                                else
                                    legData = segmentChartData(resData, []);
                                if (that.rpSelectedtab == 'revenue') {
                                    that.loadLegendData(legData);
                                    that.viewChart();
                                }
                                console.log("REVENUE CHART MAIN DATA>>>" + JSON.stringify(that.revdata));

                            });
                        } else
                            showError("Please Select From Date is Less than To Date");
                    } else
                        showError("From Date or To Date may be Empty, Please Select the Date Fields");

                });

            },
            //Load Spinner
            getLoader: function (divElem) {
                var template = fnGetTemplate(URL_Common_Template, "gmtSpinner");
                $(divElem).html(template());
            },

            //Parameter chartData
            //Load Legend Data if the chartData has values is the chartData is empty then hide the div
            loadLegendData: function (chartData) {
                var that = this;
                if (chartData.length > 0) {
                    $('.chartLegend').show();
                    chartData = _.sortBy(chartData, 'label');
                    $('.chartLegend').html(that.template_surgicalLegend({
                        'legendData': chartData
                    }));
                } else {
                    $('.chartLegend').html("");
                    $('.chartLegend').hide();
                }
            },

            //WEB SERVICE CALL FOR PROCEDURE
            procChartData: function (input, callback) {
                var that = this;
                that.setMainInputs(input);
                input.stropt = "PRC";
                ProcRevByWS(input, function (resData) {
                    callback(resData);
                });
            },

            //WEB SERVICE CALL FOR REVENUE
            revChartData: function (input, callback) {
                var that = this;
                that.setMainInputs(input);
                input.stropt = "REV";
                ProcRevByWS(input, function (resData) {
                    callback(resData);
                });
            },

            //To Set Inputs to the global variable for this view
            setMainInputs: function (maininput) {
                var that = this;
                that.mainInput = maininput;
                that.accountID = $('#accid').val();
            },


            //Check system or segment status
            checkSysSgmntStatus: function () {
                var that = this;
                if (that.ssSelectedtab == 'system' || that.SegmentStatus == 'system')
                    that.SysSgmntStatus = "system";
                else
                    that.SysSgmntStatus = "segment";
            },

            //on clicking the tabs on system, segment, revenue, procedure, overall, trend, to view charts with chart data
            clickTabs: function (e) {
                var clickedID, legData, clickedClass, chartLegData, that = this;
                clickedID = $(e.currentTarget).attr('id');
                console.log("Clicked ID>>" + clickedID);
                clickedClass = $(e.currentTarget).attr('class');
                if ($(e.currentTarget).hasClass("gmBGGrey")) {
                    $('.surg-head-top li').each(function (i) {
                        var childDiv = $(this).children().children();
                        if ($(childDiv).hasClass(clickedClass)) {
                            $(childDiv).each(function (i) {
                                if (($(this).attr('id')) == clickedID) {
                                    $(this).addClass("gmBGDarkerGrey gmFontWhite selected");
                                    $(this).removeClass("gmBGGrey");
                                } else {
                                    $(this).removeClass("gmBGDarkerGrey gmFontWhite selected");
                                    $(this).addClass("gmBGGrey");
                                }
                            });
                        }
                    });

                }

                that.selectedTabs();
                if (clickedID == 'trend') {
                    that.getLoader("#chart-container");
                    $('.ul-chart-btn').children('.li-chart-btn').removeClass('hide');
                    $('.ul-chart-btn').children('.overall-btn').addClass('hide');
                    $('.li-chart-btn').removeClass('active');
                    $(".linechart-btn").addClass('active');
                    if (that.rpSelectedtab == 'procedure')
                        chartLegData = that.procdata;
                    else
                        chartLegData = that.revdata;
                    if (that.ssSelectedtab == 'system')
                        legData = systemChartData(chartLegData);
                    else
                        legData = segmentChartData(chartLegData, []);
                    that.SegmentStatus = 'segment';
                    if (!($('.chartHeading').hasClass('hide')))
                        $('.chartHeading').addClass('hide');
                    that.loadLegendData(legData);

                } else if (clickedID == 'overall') {
                    that.getLoader("#chart-container");
                    $('.ul-chart-btn').children('.li-chart-btn').removeClass('hide');
                    $('.ul-chart-btn').children('.trend-btn').addClass('hide');
                    $('.li-chart-btn').removeClass('active');
                    $(".treemap-btn").addClass('active');
                    that.SegmentStatus = 'segment';
                    if (!($('.chartHeading').hasClass('hide')))
                        $('.chartHeading').addClass('hide');
                } else if (clickedID == 'system') {
                    that.getLoader("#chart-container");
                    that.SegmentStatus = 'segment';
                    if (!($('.chartHeading').hasClass('hide')))
                        $('.chartHeading').addClass('hide');
                    if (that.rpSelectedtab == 'procedure')
                        legData = systemChartData(that.procdata);
                    else
                        legData = systemChartData(that.revdata);
                    that.loadLegendData(legData);
                } else if (clickedID == 'segment') {
                    that.getLoader("#chart-container");
                    that.SegmentStatus = 'segment';
                    if (!($('.chartHeading').hasClass('hide')))
                        $('.chartHeading').addClass('hide');
                    if (that.rpSelectedtab == 'procedure')
                        legData = segmentChartData(that.procdata, []);
                    else
                        legData = segmentChartData(that.revdata, []);
                    that.loadLegendData(legData);
                } else if (clickedID == 'procedure') {
                    that.getLoader("#chart-container");
                    if (that.SegmentStatus == 'segment') {
                        if (!($('.chartHeading').hasClass('hide')))
                            $('.chartHeading').addClass('hide');
                    }
                    if (that.ssSelectedtab == 'system')
                        legData = systemChartData(that.procdata);
                    else
                        legData = segmentChartData(that.procdata, []);
                    that.loadLegendData(legData);
                } else if (clickedID == 'revenue') {
                    that.getLoader("#chart-container");
                    if (that.SegmentStatus == 'segment') {
                        if (!($('.chartHeading').hasClass('hide')))
                            $('.chartHeading').addClass('hide');
                    }
                    if (!($('.chartHeading').hasClass('hide')))
                        $('.chartHeading').addClass('hide');
                    if (that.ssSelectedtab == 'system')
                        legData = systemChartData(that.revdata);
                    else
                        legData = segmentChartData(that.revdata, []);
                    that.loadLegendData(legData);
                }
                that.viewChart();

            },

            // Selected tabs on the screens are stored in that variables
            selectedTabs: function () {
                var that = this;
                $('.SHT-left .surgin-tab').each(function (i) {
                    if ($(this).hasClass("selected")) {
                        that.rpSelectedtab = $(this).attr('id');
                    }
                });
                $('.SHT-middle .surgin-tab').each(function (i) {
                    if ($(this).hasClass("selected")) {
                        that.ssSelectedtab = $(this).attr('id');
                    }
                });
                $('.SHT-right .surgin-tab').each(function (i) {
                    if ($(this).hasClass("selected")) {
                        that.otSelectedtab = $(this).attr('id');
                    }
                });
                console.log("rpSelectedtab>>>" + that.rpSelectedtab + ">>>>ssSelectedtab>>>>>>" + that.ssSelectedtab + ">>>>>otSelectedtab>>>>>>" + that.otSelectedtab);
            },

            // Click on Legends this function calls to add class 'selected' in li legends and vice versa.
            // Clicking on li to trigger checkbox as checked and vice versa.

            selectLegends: function (e) {
                var that = this;
                var elemCheck;

                if ($(e.currentTarget).data('chartname')) {
                    that.chartName = $(e.currentTarget).data('chartname');
                    $(".li-chart-btn").removeClass('active');
                    $("." + that.chartName + "-btn").addClass('active');
                }

                elemCheck = $(e.currentTarget).children('.legSelect').children('input[type=checkbox]').is(":checked");
                if ($(e.currentTarget).hasClass("selected")) {
                    $(e.currentTarget).removeClass("selected");
                    if (elemCheck == true) {
                        $(e.currentTarget).children('.legSelect').children('input[type=checkbox]').prop('checked', '');
                    }
                } else {
                    $(e.currentTarget).addClass("selected");
                    if (elemCheck == false) {
                        $(e.currentTarget).children('.legSelect').children('input[type=checkbox]').prop('checked', 'checked');
                    }
                }

                if ($('.ul-chartLegend .liLeg.selected').length === $('.ul-chartLegend .liLeg').length) {
                    $('.li-selectAll').children('.legSelectAll').children('input[type=checkbox]').prop('checked', 'checked');
                    $('.li-selectAll').addClass('selected');
                } else {
                    $('.li-selectAll').children('.legSelectAll').children('input[type=checkbox]').prop('checked', '');
                    $('.li-selectAll').removeClass('selected');
                }
                that.selectedLegData();

            },

            // To manipulate data based on checked legends
            // To call the chart function by which button class has active on the list.
            selectedLegData: function () {
                var that = this;
                var selectData = [];
                $('.ul-chartLegend li').each(function () {
                    var current = $(this);
                    var thisArr = [];
                    var check = current.children('.legSelect').children('input[type=checkbox]').is(":checked");
                    if (check) {
                        thisArr = {
                            "label": current.attr('data-name'),
                            "value": current.attr('data-value'),
                            "fillcolor": current.attr('data-fillcolor'),
                            "sysid": current.attr('data-sysid'),
                            "segmentnm": current.attr('data-segmentnm'),
                            "segmentid": current.attr('data-segmentid'),
                        };
                        selectData.push(thisArr);
                    }
                });
                if (that.ssSelectedtab == 'segment' && that.SegmentStatus == 'system') {
                    if ($('.chartHeading').hasClass('hide'))
                        $('.chartHeading').removeClass('hide');
                }
                //All Legends are not selected
                if (selectData.length == 0) {
                    $('#chart-container').html("No Data Found");
                    that.setCountZero();
                } else {
                    console.log("selectData" + JSON.stringify(selectData));
                    var chartName = checkChartNm();
                    switch (chartName) {
                        case "treemap":
                            that.loadtreemapChart(selectData);
                            break;
                        case "piechart":
                            that.loadPiechart(selectData);
                            break;
                        case "barchart":
                            that.loadBarchart(selectData);
                            break;
                        case "linechart":
                            that.loadLineSbarChart(selectData);
                            break;
                        case "stackedbarchart":
                            that.loadLineSbarChart(selectData);
                            break;
                    }
                }
            },

            // Function to call a chart
            viewChart: function () {
                console.log("VIEW CHART");
                var that = this;
                var chartSgmtdata = [],
                    chartMData = [],
                    resLSBData = [];
                var chartName = checkChartNm();
                if (that.rpSelectedtab == 'procedure') {
                    chartSgmtdata = segmentChartData(that.procdata, []);
                    chartMData = that.procdata;
                } else {
                    chartSgmtdata = segmentChartData(that.revdata, []);
                    chartMData = that.revdata;
                }
                console.log("chartMData>>>>>" + JSON.stringify(chartMData));
                if (chartMData.length > 0) {
                    switch (chartName) {
                        case "treemap":
                            if (that.SegmentStatus == 'system') {
                                var segNm = $('.chartHeading').text();
                                that.systemFilterbySegName(segNm);
                            } else
                                that.loadtreemapChart('');
                            break;
                        case "piechart":
                            if (that.SegmentStatus == 'system') {
                                var segNm = $('.chartHeading').text();
                                that.systemFilterbySegName(segNm);
                            } else
                                that.loadPiechart('');
                            break;
                        case "barchart":
                            if (that.SegmentStatus == 'system') {
                                var segNm = $('.chartHeading').text();
                                that.systemFilterbySegName(segNm);
                            } else
                                that.loadBarchart('');
                            break;
                        case "linechart":
                            if (that.SegmentStatus == 'system') {
                                var segNm = $('.chartHeading').text();
                                that.lineSbarSysChartFilter(segNm);
                            } else {
                                that.checkSysSgmntStatus();
                                resLSBData = LineSbarChartData(chartMData, [], that.SysSgmntStatus);
                                resLSBData = that.addHyphenDate(resLSBData);
                                that.lineChart(resLSBData["category"], resLSBData["dataset"], resLSBData["colorarr"]);
                            }
                            break;
                        case "stackedbarchart":
                            if (that.SegmentStatus == 'system') {
                                var segNm = $('.chartHeading').text();
                                that.lineSbarSysChartFilter(segNm);
                            } else {
                                that.checkSysSgmntStatus();
                                resLSBData = LineSbarChartData(chartMData, [], that.SysSgmntStatus);
                                resLSBData = that.addHyphenDate(resLSBData);
                                that.stackedbarChart(resLSBData["category"], resLSBData["dataset"], resLSBData["colorarr"]);
                            }
                            break;
                    }
                } else {
                    $('.chartLegend').hide();
                    $('#chart-container').html("No Data Found");
                    that.setCountZero();
                }

            },

            //Add hyphen on date for line chart and stacked bar chart
            addHyphenDate: function (LSChartData) {
                var LSChartDate = [],
                    i = 0;
                _.each(LSChartData["category"], function (chDate) {
                    LSChartDate[i] = {};
                    LSChartDate[i].label = chDate['label'].toString().replace(/(\d{4})(\d{2})/, "$2/$1");
                    i++;
                });
                LSChartData["category"] = [];
                LSChartData["category"] = LSChartDate;
                return LSChartData;
            },

            //Load Treemap chart and calculate total values
            loadtreemapChart: function (chartData) {
                var that = this,
                    ChartData = [],
                    treemapChartData = [];
                console.log("LOAD TREEMAP CHART");
                if (chartData.length > 0) { // If Parameter is not Empty   
                    that.treemapChart(chartData);
                } else { // If Parameter is Empty  
                    if (that.rpSelectedtab == 'procedure')
                        ChartData = that.procdata;
                    else
                        ChartData = that.revdata;
                    if (ChartData.length > 0) {
                        if (that.ssSelectedtab == 'system')
                            treemapChartData = systemChartData(ChartData);
                        else
                            treemapChartData = segmentChartData(ChartData, []);
                        that.loadLegendData(treemapChartData);
                        that.treemapChart(treemapChartData);
                    } else {
                        $('.chartLegend').hide();
                        $('#chart-container').html("No Data Found");
                        that.setCountZero();
                    }
                }
                if (that.ssSelectedtab == 'segment' && that.SegmentStatus == 'segment') {
                    if (!($('.chartHeading').hasClass('hide'))) {
                        $('.chartHeading').addClass('hide');
                    }
                }
            },

            //Load Bar Chart
            loadBarchart: function (chartData) {
                var barChartData, ChartData, that = this;
                console.log("LOAD BAR CHART");
                if (chartData.length > 0) { // If Parameter is not Empty                  
                    that.barChart(chartData);
                } else { // If Parameter is Empty
                    if (that.rpSelectedtab == 'procedure')
                        ChartData = that.procdata;
                    else
                        ChartData = that.revdata;
                    if (ChartData.length > 0) {
                        if (that.ssSelectedtab == 'system')
                            barChartData = systemChartData(ChartData);
                        else
                            barChartData = segmentChartData(ChartData, []);
                        that.loadLegendData(barChartData);
                        that.barChart(barChartData);

                    } else {
                        $('.chartLegend').hide();
                        $('#chart-container').html("No Data Found");
                        that.setCountZero();
                    }

                }
                if (that.ssSelectedtab == 'segment' && that.SegmentStatus == 'segment') {
                    if (!($('.chartHeading').hasClass('hide'))) {
                        $('.chartHeading').addClass('hide');
                    }
                }
            },

            //Load Pie Chart
            loadPiechart: function (chartData) {
                var pieChartData, ChartData, that = this;
                console.log("LOAD PIE CHART");
                if (chartData.length > 0) { // If Parameter is not Empty 
                    that.pieChart(chartData);
                } else { // If Parameter is Empty
                    if (that.rpSelectedtab == 'procedure')
                        ChartData = that.procdata;
                    else
                        ChartData = that.revdata;
                    if (ChartData.length > 0) {
                        if (that.ssSelectedtab == 'system')
                            pieChartData = systemChartData(ChartData);
                        else
                            pieChartData = segmentChartData(ChartData, []);
                        that.loadLegendData(pieChartData);
                        that.pieChart(pieChartData);
                    } else {
                        $('.chartLegend').hide();
                        $('#chart-container').html("No Data Found");
                        that.setCountZero();
                    }
                }
                if (that.ssSelectedtab == 'segment' && that.SegmentStatus == 'segment') {
                    if (!($('.chartHeading').hasClass('hide'))) {
                        $('.chartHeading').addClass('hide');
                    }
                }
            },

            //To set inputs of getting docount by using webservice
            setDoInputs: function (chartData, callback) {
                var that = this,
                    systemstr, segmentstr,
                    input = that.mainInput;
                input.systemstr = "";
                input.segmentstr = "";
                that.checkSysSgmntStatus();
                if (that.SysSgmntStatus == "system") {
                    systemstr = _.pluck(chartData, 'sysid');
                    systemstr = '\'' + systemstr.join('\',\'') + '\'';
                    input.systemstr = systemstr.toString();
                } else {
                    segmentstr = _.pluck(chartData, 'segmentid');
                    segmentstr = '\'' + segmentstr.join('\',\'') + '\'';
                    input.segmentstr = segmentstr.toString();
                }
                input.accountid = that.accountID;
                console.log("setDoInputs>>" + JSON.stringify(input));
                callback(input);
            },

            //To Display DO COUNT with parameters of chartData and sum
            showDoCount: function (chartData, sum) {
                var that = this;
                that.getLoader(".countActivity");
                that.setDoInputs(chartData, function (wsinput) {
                    DOCountByWS(wsinput, function (dt) {
                        if(dt !=null){
                        $(".countActivity").html('<li id="totalRP"></li><li id="totalDO"></li>');
                        if (that.rpSelectedtab == 'revenue')
                            $("#totalRP").html("<label class='labelRev'>Total Revenue</label> : <span>" + fmtPrice(sum) + "</span>");
                        else
                            $("#totalRP").html("<label class='labelProc'>No. of Procedures</label> : <span>" + sum + "</span>");
                        $("#totalDO").html("<label>No. of DO's</label> : <span>" + dt.docount + "</span>");
                        $("#totalDO span").width($("#totalRP span").width());
                        $("#totalDO label").width($("#totalRP label").width());
                        }

                    });
                });
            },

            // If no data found then set the docounts and total to zero
            setCountZero: function () {
                var that = this;
                $(".countActivity").html('<li id="totalRP"></li><li id="totalDO"></li>');
                if (that.rpSelectedtab == 'revenue')
                    $("#totalRP").html("<label class='labelRev'>Total Revenue</label> : <span>$0</span>");
                else
                    $("#totalRP").html("<label class='labelProc'>No. of Procedures</label> : <span>0</span>");
                $("#totalDO").html("<label>No. of DO's</label> : <span>0</span>");
                $("#totalDO span").width($("#totalRP span").width());
                $("#totalDO label").width($("#totalRP label").width());
            },

            //VIEW TREEMAP CHARTS
            treemapChart: function (chartData) {
                var that = this;
                chartData = _.sortBy(chartData, function (arr) { //To sort by values desc
                    return arr.value * -1;
                });
                var sum = 0;
                _.each(chartData, function (arr) {
                    sum += parseFloat(arr.value) || 0;
                });
                that.showDoCount(chartData, sum);
                _.each(chartData, function (dt) { // To Merge Label and value with comma
                    if (that.rpSelectedtab == 'revenue')
                        dt.label = dt.label + ", " + fmtPrice(dt.value) + " (" + ((dt.value * 100) / sum).toFixed(2) + "%)";
                    else
                        dt.label = dt.label + ", " + dt.value + " (" + ((dt.value * 100) / sum).toFixed(2) + "%)";
                });
                var tempDatavalues = [],
                    numberPrefix = '',
                    plotToolText, data;
                console.log("TREEMAP");
                plotToolText = "<div><b>$label</b></div>";
                var totalValues = 0;
                tempDatavalues = _.pluck(chartData, 'value');
                totalValues = tempDatavalues.reduce(function (a, b) {
                    return parseInt(a) + parseInt(b);
                }, 0);
                var visitTreemapChart = new FusionCharts({
                    type: 'treemap',
                    renderAt: 'chart-container',
                    width: '700',
                    height: '450',
                    dataFormat: 'json',
                    dataSource: {
                        "chart": {
                            "animation": "0",
                            "hideTitle": "1",
                            "plotToolText": plotToolText,
                            "horizontalPadding": "0",
                            "verticalPadding": "0",
                            "plotborderthickness": ".5",
                            "plotbordercolor": "#ffffff",
                            "chartTopMargin": "-30",
                            "chartBottomMargin": "0",
                            "labelGlow": "0",
                            "labelFontColor": "#ffffff",
                            "labelFontSize": "11",
                            "showTooltip": "1",
                            "showBorder": "0",
                            "bgColor": "#FFFFFF",
                            "algorithm": "squarified",
                            "theme": "zune",
                            "showParent": 0,
                            "showChildLabels": "1",
                            "parentLabelLineHeight": "15",
                            "maxDepth": '3',
                            //                            "showNavigationBar": "0",
                        },
                        "data": [{
                            "label": "Surgical Activity",
                            "fillcolor": "#838986",
                            "value": totalValues,
                            "data": chartData
                       }],
                    }
                }).render();
                if (that.ssSelectedtab == 'segment' && that.SegmentStatus == 'segment') {
                    visitTreemapChart.addEventListener("dataPlotClick", function (e, v) {
                        var res = (v.name).split(", ");
                        that.systemFilterbySegName(res[0]);
                    });
                } else {
                    visitTreemapChart.addEventListener("dataPlotClick", function (e, v) {
                        var res = (v.name).split(", ");
                        that.showDoListPopup(res[0]);
                    });
                }
            },

            //VIEW BAR CHARTS
            barChart: function (chartData) {
                chartData = _.sortBy(chartData, function (arr) { //To sort by values desc
                    return arr.value * -1;
                });
                var that = this,
                    numberPrefix = '';
                var sum = 0;
                _.each(chartData, function (arr) {
                    sum += parseFloat(arr.value) || 0;
                });
                that.showDoCount(chartData, sum);
                _.each(chartData, function (dt) { // To Merge Label and value with comma
                    if (that.rpSelectedtab == 'revenue') {
                        dt.displayValue = fmtPrice(dt.value) + " (" + ((dt.value * 100) / sum).toFixed(2) + "%)";
                        dt.toolText = dt.label + ", " + fmtPrice(dt.value) + " (" + ((dt.value * 100) / sum).toFixed(2) + "%)";
                    } else {
                        dt.displayValue = dt.value + " (" + ((dt.value * 100) / sum).toFixed(2) + "%)";
                        dt.toolText = dt.label + ", " + dt.value + " (" + ((dt.value * 100) / sum).toFixed(2) + "%)";
                    }
                });
                var plotToolText = "<div><b>$toolText</b></div>";
                var colorPalette = _.pluck(chartData, 'fillcolor');
                var colorArr = _.values(colorPalette);
                var xAxisName = 'System',
                    yAxisName = 'No. of Cases';
                if (that.rpSelectedtab == 'revenue') {
                    numberPrefix = '$';
                    xAxisName = 'System';
                    yAxisName = 'Revenue Amount';
                }

                var visitbarChart = new FusionCharts({
                    type: 'column2d',
                    renderAt: 'chart-container',
                    width: '700',
                    height: '450',
                    dataFormat: 'json',
                    dataSource: {
                        "chart": {
                            "caption": "",
                            "subCaption": "",
                            "labeldisplay": "rotate",
                            "slantlabels": "1",
                            "plotToolText": plotToolText,
                            "useellipseswhenoverflow": "1",
                            "maxLabelHeight": "80",
                            "xAxisName": xAxisName,
                            "yAxisName": yAxisName,
                            "numberPrefix": numberPrefix,
                            "paletteColors": colorArr.toString(),
                            "bgColor": "#ffffff",
                            "usePlotGradientColor": "1",
                            "showBorder": "0",
                            "plotBorderAlpha": "10",
                            "placevaluesInside": "0",
                            "rotatevalues": "1",
                            // "valueFontColor": "#ffffff",
                            "showXAxisLine": "1",
                            "xAxisLineColor": "#999999",
                            "divlineColor": "#999999",
                            "divLineDashed": "1",
                            "showAlternateHGridColor": "0",
                            "subcaptionFontSize": "14"
                        },
                        "data": chartData,
                    }
                }).render();
                if (that.ssSelectedtab == 'segment' && that.SegmentStatus == 'segment') {
                    visitbarChart.addEventListener("dataPlotClick", function (e, v) {
                        that.systemFilterbySegName(v.categoryLabel);
                    });
                } else {
                    visitbarChart.addEventListener("dataPlotClick", function (e, v) {
                        that.showDoListPopup(v.categoryLabel);
                    });
                }
            },

            // VIEW PIE CHARTS
            pieChart: function (chartData) {
                chartData = _.sortBy(chartData, function (arr) { //To sort by values desc
                    return arr.value * -1;
                });
                var that = this,
                    numberPrefix = '';
                var colorPalette = _.pluck(chartData, 'fillcolor');
                var colorArr = _.values(colorPalette);
                if (that.rpSelectedtab == 'revenue') {
                    numberPrefix = '$';
                }
                var sum = 0;
                _.each(chartData, function (arr) {
                    sum += parseFloat(arr.value) || 0;
                });
                that.showDoCount(chartData, sum);
                _.each(chartData, function (dt) { // To Merge Label and value with comma
                    if (that.rpSelectedtab == 'revenue') {
                        dt.displayValue = fmtPrice(dt.value) + " (" + ((dt.value * 100) / sum).toFixed(2) + "%)";
                        dt.toolText = dt.label + ", " + fmtPrice(dt.value) + " (" + ((dt.value * 100) / sum).toFixed(2) + "%)";
                    } else {
                        dt.displayValue = dt.value + " (" + ((dt.value * 100) / sum).toFixed(2) + "%)";
                        dt.toolText = dt.label + ", " + dt.value + " (" + ((dt.value * 100) / sum).toFixed(2) + "%)";
                    }
                });
                var plotToolText = "<div><b>$toolText</b></div>";
                var visitpieChart = new FusionCharts({
                    type: 'pie2d',
                    renderAt: 'chart-container',
                    width: '700',
                    height: '450',
                    dataFormat: 'json',
                    dataSource: {
                        "chart": {
                            "paletteColors": colorArr.toString(),
                            "bgColor": "#ffffff",
                            "numberPrefix": numberPrefix,
                            "plotToolText": plotToolText,
                            "showBorder": "0",
                            "use3DLighting": "0",
                            "showShadow": "0",
                            "showLabels": "0",
                            "enableSmartLabels": "1",
                            "startingAngle": "0",
                            "showPercentValues": "1",
                            "showPercentInTooltip": "0",
                            "decimals": "2",
                            "captionFontSize": "14",
                            "subcaptionFontSize": "14",
                            "subcaptionFontBold": "0",
                            "showHoverEffect": "1",
                            "showLegend": "0",
                            "legendBgColor": "#ffffff",
                            "legendBorderAlpha": '0',
                            "legendShadow": '0',
                            "legendItemFontSize": '10',
                            "legendItemFontColor": '#666666',
                        },
                        "data": chartData,
                    }
                }).render();
                if (that.ssSelectedtab == 'segment' && that.SegmentStatus == 'segment') {
                    visitpieChart.addEventListener('dataPlotClick', function (e, v) {
                        var resarr = v.toolText.split(", ");
                        that.systemFilterbySegName(resarr[0]);
                    });
                } else {
                    visitpieChart.addEventListener("dataPlotClick", function (e, v) {
                        var resarr = v.toolText.split(", ");
                        that.showDoListPopup(resarr[0]);
                    });
                }
            },

            //  On clicking Date tabs to highlight the tab
            clickDateTabs: function (e) {
                var dateRange = $(e.currentTarget).attr('id');
                $('.surgin-dr-tab').removeClass("gmBGDarkerGrey gmFontWhite selected");
                $('.surgin-dr-tab').removeClass("gmBGGrey");
                $('.surgin-dr-tab').addClass("gmBGGrey");
                $('#' + dateRange).removeClass("gmBGGrey");
                $('#' + dateRange).addClass("gmBGDarkerGrey gmFontWhite selected");
                if (dateRange == 'custom') {
                    $('.SHB-right').removeClass('hide');
                } else {
                    if (!($('.SHB-right').hasClass('hide')))
                        $('.SHB-right').addClass('hide');
                }

            },

            //DRILL DOWN CHART on clicking segment for BAR, PIE, TREE CHARTS
            systemFilterbySegName: function (segName) {
                var that = this;
                var chartSysdata, resData;
                if (that.rpSelectedtab == 'procedure')
                    chartSysdata = systemChartMainData(that.procdata);
                else
                    chartSysdata = systemChartMainData(that.revdata);
                that.SegmentStatus = 'system';
                resData = _.filter(chartSysdata, function (csd) {
                    return csd.segmentnm === segName;
                });
                if (resData.length > 0) {
                    that.loadLegendData(resData);
                    var chartName = checkChartNm();
                    switch (chartName) {
                        case "treemap":
                            that.treemapChart(resData);
                            break;
                        case "piechart":
                            that.pieChart(resData);
                            break;
                        case "barchart":
                            that.barChart(resData);
                            break;
                    }
                    $('.chartHeading').removeClass('hide');
                    $('.chartHeading').html(segName);
                } else {
                    $('.chartLegend').hide();
                    $('#chart-container').html("No Data Found");
                    that.setCountZero();
                }
            },

            // BACK TO SEGMENT CHART FROM DRILLDOWN SYSTEM CHART
            backToSegment: function () {
                var that = this,
                    chartSgmtdata, chartMData, resLSBData = [];
                that.SegmentStatus = 'segment';
                if (!($('.chartHeading').hasClass('hide')))
                    $('.chartHeading').addClass('hide');
                that.viewChart();
                if (that.rpSelectedtab == 'procedure') {
                    chartSgmtdata = segmentChartData(that.procdata, []);
                    chartMData = that.procdata;
                } else {
                    chartSgmtdata = segmentChartData(that.revdata, []);
                    chartMData = that.revdata;
                }
                that.loadLegendData(chartSgmtdata);
            },


            // LOAD LINE CHART AND STACKED BAR CHART
            loadLineSbarChart: function (chartData) {
                console.log("LOAD LINE BAR CHART");
                var that = this;
                var chartMdata, legMdata, resLegData, colorPalette, colorArr, resData, resLSBData = [];
                if (that.rpSelectedtab == 'procedure')
                    chartMdata = that.procdata;
                else
                    chartMdata = that.revdata;

                if (that.ssSelectedtab == 'system' || that.SegmentStatus == 'system') {
                    legMdata = systemChartData(chartMdata);
                } else {
                    legMdata = segmentChartData(chartMdata, []);
                }
                //For getting Color Array
                resLegData = _.filter(legMdata, function (lm) {
                    return _.find(chartData, function (cd) {
                        return cd.label === lm.label;
                    });
                });

                colorPalette = _.pluck(resLegData, 'fillcolor');
                colorArr = _.values(colorPalette);
                resData = _.filter(chartMdata, function (cm) {
                    return _.find(chartData, function (cd) {
                        if (that.ssSelectedtab == 'system' || that.SegmentStatus == 'system')
                            return cd.label === cm.sysnm;
                        else
                            return cd.label === cm.segmentnm;
                    });
                });
                that.checkSysSgmntStatus();
                resLSBData = LineSbarChartData(resData, colorArr, that.SysSgmntStatus);
                resLSBData = that.addHyphenDate(resLSBData);
                var chartName = checkChartNm();
                if (chartName == 'linechart') {
                    console.log("LOAD LINE CHART");
                    that.lineChart(resLSBData["category"], resLSBData["dataset"], resLSBData["colorarr"]);

                } else if (chartName == 'stackedbarchart') {
                    console.log("LOAD STACKED BAR CHART");
                    that.stackedbarChart(resLSBData["category"], resLSBData["dataset"], resLSBData["colorarr"]);
                }

            },

            //DrillDown Chart by click on the segments for LINECHART AND STACKED BAR CHART
            lineSbarSysChartFilter: function (segName) {
                var that = this;
                var chartMdata, legMdata, resLegData, colorPalette, colorArr, resData, resLSBData = [];
                if (that.rpSelectedtab == 'procedure')
                    chartMdata = that.procdata;
                else
                    chartMdata = that.revdata;
                legMdata = systemChartMainData(chartMdata);
                resData = _.filter(chartMdata, function (csd) {
                    return csd.segmentnm === segName;
                });
                //For getting Color Array
                resLegData = _.filter(legMdata, function (lm) {
                    return _.find(resData, function (cd) {
                        return cd.sysnm === lm.label;
                    });
                });
                colorPalette = _.pluck(resLegData, 'fillcolor');
                colorArr = _.values(colorPalette);
                that.loadLegendData(resLegData);
                that.SegmentStatus = 'system';
                $('.chartHeading').removeClass('hide');
                $('.chartHeading').html(segName);
                that.checkSysSgmntStatus();
                resLSBData = LineSbarChartData(resData, colorArr, that.SysSgmntStatus);
                resLSBData = that.addHyphenDate(resLSBData);
                var chartName = checkChartNm();
                if (chartName == 'linechart') {
                    that.lineChart(resLSBData["category"], resLSBData["dataset"], resLSBData["colorarr"]);

                } else if (chartName == 'stackedbarchart') {
                    that.stackedbarChart(resLSBData["category"], resLSBData["dataset"], resLSBData["colorarr"]);
                }

            },


            //VIEW LINE CHART
            lineChart: function (category, dataset, colorarr) {
                var that = this,
                    numberPrefix = '';
                if (that.rpSelectedtab == 'revenue') {
                    numberPrefix = '$';
                }
                var sum = 0;
                _.each(dataset, function (arr) {
                    _.each(arr.data, function (sibArr) {
                        sum += parseFloat(sibArr.value) || 0;
                    });
                });
                that.showDoCount(dataset, sum);

                var visitlineChart = new FusionCharts({

                    type: 'msline',
                    renderAt: 'chart-container',
                    width: '700',
                    height: '450',
                    dataFormat: 'json',
                    dataSource: {
                        "chart": {
                            "paletteColors": colorarr.toString(),
                            "numberPrefix": numberPrefix,
                            "bgcolor": "#ffffff",
                            "showBorder": "0",
                            "showShadow": "0",
                            "showCanvasBorder": "0",
                            "usePlotGradientColor": "0",
                            "legendBorderAlpha": "0",
                            "legendShadow": "0",
                            "showAxisLines": "0",
                            "showAlternateHGridColor": "0",
                            "divlineThickness": "1",
                            "divLineDashed": "1",
                            "divLineDashLen": "1",
                            "showValues": "0",
                            "anchoralpha": "0",
                            "drawCrossLine": "1",
                            "crossLineColor": "#0d0d0d",
                            "crossLineAlpha": "100",
                            "labeldisplay": "ROTATE",
                            "slantlabels": "1",
                            "showLegend": "0",

                        },
                        "categories": [{
                            category: category
                            }],
                        "dataset": dataset,

                    }
                }).render();
                if (that.ssSelectedtab == 'segment' && that.SegmentStatus == 'segment') {
                    visitlineChart.addEventListener("dataPlotClick", function (e, v) {
                        that.lineSbarSysChartFilter(v.datasetName);
                    });
                } else {
                    visitlineChart.addEventListener("dataPlotClick", function (e, v) {
                        that.showDoListPopup(v.datasetName);
                    });
                }
            },

            //VIEW STATCKED BAR CHART
            stackedbarChart: function (category, dataset, colorarr) {
                var that = this,
                    numberPrefix = '';
                if (that.rpSelectedtab == 'revenue') {
                    numberPrefix = '$';
                }
                var sum = 0;
                _.each(dataset, function (arr) {
                    _.each(arr.data, function (sibArr) {
                        sum += parseFloat(sibArr.value) || 0;
                    });
                });
                that.showDoCount(dataset, sum);
                var visitstackedbarChart = new FusionCharts({
                    type: 'stackedcolumn2d',
                    renderAt: 'chart-container',
                    width: '700',
                    height: '450',
                    dataFormat: 'json',
                    dataSource: {
                        "chart": {
                            "showSum": "1",
                            "numberPrefix": numberPrefix,
                            "labeldisplay": "ROTATE",
                            "slantlabels": "1",
                            "showLegend": "0",
                            "paletteColors": colorarr.toString(),
                            "bgcolor": "#ffffff",
                            "showBorder": "0",
                            "showShadow": "0",
                            "showCanvasBorder": "0",
                            "usePlotGradientColor": "0",
                            "legendBorderAlpha": "0",
                            "legendShadow": "0",
                            "showAxisLines": "0",
                            "showAlternateHGridColor": "0",
                            "showValues": "0",
                            "plotBorderAlpha": "0",
                        },

                        "categories": [{
                            "category": category
            }],

                        "dataset": dataset,

                    }
                }).render();
                if (that.ssSelectedtab == 'segment' && that.SegmentStatus == 'segment') {
                    visitstackedbarChart.addEventListener("dataPlotClick", function (e, v) {
                        that.lineSbarSysChartFilter(v.datasetName);
                    });
                } else {
                    visitstackedbarChart.addEventListener("dataPlotClick", function (e, v) {
                        that.showDoListPopup(v.datasetName);
                    });
                }

            },

            // Select and deselect all legends on clicking select all checkbox
            selectAllLegends: function (e) {
                var that = this;
                var elemCheck, elemCheckAll;

                //To check the checkbox if i click the outside checkbox
                elemCheckAll = $(e.currentTarget).children('.legSelectAll').children('input[type=checkbox]').is(":checked");
                if ($(e.currentTarget).hasClass("selected")) {
                    $(e.currentTarget).removeClass("selected");
                    if (elemCheckAll == true) {
                        $(e.currentTarget).children('.legSelectAll').children('input[type=checkbox]').prop('checked', '');
                    }
                } else {
                    $(e.currentTarget).addClass("selected");
                    if (elemCheckAll == false) {
                        $(e.currentTarget).children('.legSelectAll').children('input[type=checkbox]').prop('checked', 'checked');
                    }
                }

                //To Select and Deselect all legends on clicking the select all
                $('.liLeg').each(function () {
                    elemCheck = $(this).children('.legSelect').children('input[type=checkbox]').is(":checked");
                    elemCheckAll = $('.li-selectAll').children('.legSelectAll').children('input[type=checkbox]').is(":checked");
                    if (elemCheckAll == true) {
                        if ($(this).hasClass("selected")) {
                            if (elemCheck == false) {
                                $(this).children('.legSelect').children('input[type=checkbox]').prop('checked', 'checked');
                            }
                        } else {
                            $(this).addClass("selected");
                            if (elemCheck == false) {
                                $(this).children('.legSelect').children('input[type=checkbox]').prop('checked', 'checked');
                            }

                        }
                    } else {
                        if ($(this).hasClass("selected")) {
                            $(this).removeClass("selected");
                            if (elemCheck == true) {
                                $(this).children('.legSelect').children('input[type=checkbox]').prop('checked', '');
                            }
                        } else {
                            if (elemCheck == true) {
                                $(this).children('.legSelect').children('input[type=checkbox]').prop('checked', '');
                            }
                        }
                    }

                });
                //To populate the chart data based on selected checkbox
                that.selectedLegData();

            },

            //On clicking the System Data Plot in Chart, the Fn Retrieves the DO List Report for the System
            showDoListPopup: function (sysnm) {
                var that = this,
                    sysID;
                this.gridObj = "";
                var arruniqSysProc = _.filter(that.procdata, function (data) {
                    return data.sysnm === sysnm;
                });
                var arruniqSysRev = _.filter(that.revdata, function (data) {
                    return data.sysnm === sysnm;
                });
                if (arruniqSysProc && arruniqSysProc.length > 0)
                    sysID = arruniqSysProc[0].sysid;
                else
                    sysID = arruniqSysRev[0].sysid;

                var doInput = {
                    "token": localStorage.getItem("token"),
                    "partyid": GM.Global.Surgeon.Data.partyid,
                    "systemid": sysID,
                    "fromdt": that.fromDate,
                    "todt": that.toDate,
                    "acctid": that.accountID
                };

                console.log("doInput>>>>>>>" + JSON.stringify(doInput));
                showPopupDO();
                $("#div-crm-overlay-content-container .modal-title").text(sysnm);   
                 that.getLoader("#div-crm-overlay-content-container .modal-body");
                fnGetWebServerData("surgeon/surgsystemdo", "GmCRMSurgicalSystemDOVO", doInput, function (data) {
                    console.log("GmCRMSurgicalSystemDOVO>>>>>>>" + JSON.stringify(data));
                    var rows = [];
                    var i = 0;
                    _.each(data.gmCRMSurgicalSystemDOVO, function (griddt) {
                        rows[i] = {};
                        rows[i].id = i;
                        rows[i].data = [];
                        rows[i].data[0] = griddt.orderid;
                        rows[i].data[1] = griddt.orderdt;
                        rows[i].data[2] = griddt.partnum;
                        rows[i].data[3] = griddt.pdesc;
                        rows[i].data[4] = griddt.qty;
                        rows[i].data[5] = fmtPrice(griddt.price);
                        rows[i].data[6] = fmtPrice(griddt.total);
                        i++;
                    });
                    var dataDO = {};
                    dataDO.rows = rows;
                    $("#div-crm-overlay-content-container .modal-body").html("<div id='SurgeonDOList'></div>");                                     
                    var setGridHeight=450;
                    var setInitWidths = "100,100,80,*,80,100,120";
                    var setColAlign = "left,center,left,left,right,right,right";
                    var setColTypes = "ro,ro,ro,ro,ro,ro,ro";
                    var setColSorting = "str,str,str,str,int,str,str";
                    var setHeader = ["DO", "Date", "Part", "Description", "Quantity", "Unit Price", "Total"];
                    var enableTooltips = "true,true,true,true,true,true,true";
                    var attachFilter = "#text_filter,#text_filter,#text_filter,#text_filter,#numeric_filter,#text_filter,#text_filter";
                    var footerArr = ["Total", "", "", "", "", "", "<div id='totalc6'></div>"]; 
                    var footerExportFL=true;                  
                   
                    that.gridObj = loadDHTMLXGrid('SurgeonDOList', setGridHeight, dataDO, setHeader, setInitWidths, setColAlign, setColTypes, setColSorting, enableTooltips, attachFilter, footerArr, [], footerExportFL);
                    var rowCount=that.gridObj.getRowsNum();                    

                    for(var i=0; i<rowCount; i++){
                        that.gridObj.cells(i, 0).cell.className = 'viewDO gmReportLnk';
                    }                  
                    document.getElementById("totalc6").innerHTML = fmtPrice(that.sumColumn(that.gridObj, 6));                    
                    $(".viewDO").bind("click", function (event) {
                        that.openDO(event);
                    });
                    
                    if(footerExportFL){
                        var deleteIndexArr=[];       //include the index of the column in the array if u want to export                 
                    $("#SurgeonDOList-export").unbind("click").bind("click", function (event) {                        
                        exportExcel(event, that.gridObj, deleteIndexArr);
                    });
                    }

                });
            },

            // To manipulate column total
            sumColumn: function (myGrid, ind) {
                var out = 0;
                for (var i = 0; i < myGrid.getRowsNum(); i++) {
                    var valueWD = myGrid.cells2(i, ind).getValue();
                    var valueWOD = valueWD.replace(/[^0-9.]/g, '');
                    out = parseFloat(out) + parseFloat(valueWOD);
                }
                return out;
            },

            //Function to Show the DO Detail in a new window/popop on clicking the orderID
           openDO: function (event) {
                var elem = event.currentTarget;
                if ($(elem).text() != "") {
                    var ordID = $(elem).text();
                    fnSummaryDO(ordID);
                }
            },


            // Close view
            closeView: function (view) {
                if (view != undefined) {
                    view.unbind();
                    view.undelegateEvents();
                    view.$el.unbind();
                    view.$el.empty();
                    view = undefined;
                    $("#btn-back").unbind();
                }
            }

        });
        return GM.View.SurgeonSurgicalActivity;
    });