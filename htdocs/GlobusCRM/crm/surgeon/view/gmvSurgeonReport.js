    /**********************************************************************************
     * File             :        gmvSurgeonReport.js
     * Description      :        View to represent Surgeon Report List
     * Version          :        1.0
     * Author           :        jgurunathan
     **********************************************************************************/
    define([

    // External Libraries
    'jquery', 'underscore', 'backbone', 'handlebars', 'jqueryui',

    // Common
    'global', 'commonutil',

    // View
    'gmvCRMData', 'gmvCRMSelectPopup', 'gmvSurgeonSummaryReport',  'gmvSearch',

    // Pre-Compiled Template
    'gmtTemplate'

    //Model

],

        function ($, _, Backbone, Handlebars, JqueryUI, Global, Commonutil, GMVCRMData, GMVCRMSelectPopup, GMVSurgeonSummaryReport, GMVSearch, GMTTemplate) {

            'use strict';

            GM.View.SurgeonReport = Backbone.View.extend({

                el: "#div-crm-module",

                name: "CRM Surgeon Report View",

                /* Load the templates */
                template_MainModule: fnGetTemplate(URL_Common_Template, "gmtCRMMainModule"),
                template_SurgeonReport: fnGetTemplate(URL_Surgeon_Template, "gmtSurgeonReportMain"),
                template_SurgeonReportList: fnGetTemplate(URL_Surgeon_Template, "gmtSurgeonReportList"),
                template_SummaryReportMain: fnGetTemplate(URL_Surgeon_Template, "gmtSummaryReportMain"),
                template_SummaryReportList: fnGetTemplate(URL_Surgeon_Template, "gmtSummaryReportList"),

                events: {
                    "click  .btn-listby": "openPopup",
                    "click #btn-category-search": "searchCategory",
                    "click #btn-main-create": "createSurgeon",  
                    "click .li-chart-btn": "ToggleChart",  
                    "click #rpt-legend-icon" : "showLegend"
                },

                initialize: function (options) {
                    console.log(options);
                    this.viewby = options.reportby;
                    if(this.viewby=="summary"){
                        if(options.acslvl){
                            this.acslvl=options.acslvl;
                            this.refid=options.refid;
                        }
                        else if(options.grid)
                            this.grid=options.grid;   
                    }
                    $(window).on('orientationchange', this.onOrientationChange);
                    if(typeof(GM.Global.Surgeon.FilterID) != 'object')
                        GM.Global.Surgeon.FilterID=JSON.parse(GM.Global.Surgeon.FilterID);
                    if(typeof(GM.Global.Surgeon.FilterName) != 'object')
                        GM.Global.Surgeon.FilterName=JSON.parse(GM.Global.Surgeon.FilterName); 
                    GM.Global.myFilter = null;
                    $("#div-app-title").show();
                    $("#span-app-title").html("Surgeon");
                },

                render: function () {
                    var that = this;

                    this.$el.html(this.template_MainModule({ "module": "surgeon" }));
                    this.$("#crm-btn-back").show();
                    if (_.contains(GM.Global.Admin, "surgeon"))
                        this.$("#li-create").removeClass("hide");
                    GM.Global.Surgeon.FilterID["reporttype"] = this.viewby; 
                    this.$("#div-main-content").html(this.template_SurgeonReport(GM.Global.Surgeon.FilterID));

                    if (this.viewby == "education") {
                        this.renderEduSearch();
                        if(GM.Global.Surgeon.FilterID["level"]!="")
                            $("#select-surgeon-edulvl").html(GM.Global.Surgeon.FilterName["Level"]);
                        if(GM.Global.Surgeon.FilterID["designation"]!="")
                            $("#select-surgeon-desgn").html(GM.Global.Surgeon.FilterName["Designation"]);
                         if ((parseNull(GM.Global.Surgeon.FilterID["stropt"]) != "") && (GM.Global.Surgeon.FilterID["stropt"] == 'education'))
                            this.searchCategory();
                    }
                    if (this.viewby == "professional") {
                        this.renderProfSearch();
                        if ((parseNull(GM.Global.Surgeon.FilterID["stropt"]) != "") && (GM.Global.Surgeon.FilterID["stropt"] == 'professional'))
                            this.searchCategory();
                    }
					if (this.viewby == "consultant"){
                      this.renderConsSearch(); 
                        if ((parseNull(GM.Global.Surgeon.FilterID["stropt"]) != "") && (GM.Global.Surgeon.FilterID["stropt"] == 'consultant'))
                            this.searchCategory();
                    }
                    if (this.viewby == "contact") {
                        $(".btn-listby").each(function () {
                            var storageKey = $(this).data("storagekey");
                            if (parseNull(GM.Global.Surgeon.FilterID[storageKey]) != '') {
                                var resarr = (GM.Global.Surgeon.FilterID[storageKey]).split(",");
                                var rescnt = resarr.length;
                                if (rescnt > 0) {
                                    $(this).find("span").text("(" + rescnt + ")"); // get number of items selected 
                                    $(this).css("background", "#FDF5E6");
                                } else {
                                    $(this).css("background", "#fff");
                                    $(this).find("span").text("");
                                }
                            }

                        });
                    }
                    this.$(".gmPanelTitle").html(this.viewby).addClass("gmTextCapitalize");
                    if (this.viewby == "summary") {
                        this.closeView(this.gmvsurgeonsummaryreport);
                        this.gmvsurgeonsummaryreport = new GMVSurgeonSummaryReport(this);
                    }

                    
                    
                    if (GM.Global.Surgeon.FilterFl == "Y") {
                        this.searchCategory();
                        this.emptyFl = false;
                    }
                 
                },            

                // Open Single / Multi Select box        
                openPopup: function (event) {
                    var that = this;
                    var selectionTyp = 1;
                    if (this.viewby == "education") {
                        selectionTyp = 2;
                        var fieldName = $(event.currentTarget).attr("data-name");                         
                    }
                   
                    that.systemSelectOptions = {
                        'title': $(event.currentTarget).attr("data-title"),
                        'storagekey': $(event.currentTarget).attr("data-storagekey"),
                        'webservice': $(event.currentTarget).attr("data-webservice"),
                        'resultVO': $(event.currentTarget).attr("data-resultVO"),
                        'IDParameter': $(event.currentTarget).attr("data-IDParameter"),
                        'NameParameter': $(event.currentTarget).attr("data-NameParameter"),
                        'inputParameters': $(event.currentTarget).attr("data-inputParameters"),
                        'codegrp': $(event.currentTarget).attr("data-codegrp"),
                        'module': "surgeon",
                        'event': event,
                        'renderOne': $(event.currentTarget).attr("redata"),
                        'type': selectionTyp,
                        'parent': this,
                        'moduleLS': GM.Global.Surgeon.FilterID,
                        'moduleCLS': GM.Global.Surgeon.FilterName,
                        'callback': function (ID, Name) {
                            if (GM.Global.Surgeon == undefined)
                                GM.Global.Surgeon = {};              
                            if (that.viewby == "education") {
                                $(event.currentTarget).html(Name);
                                $(event.currentTarget).attr("redata", ID);                            
                                
                                if (fieldName == "level") {
                                    $(event.currentTarget).attr("levelid", ID);
                                    $(event.currentTarget).attr("level", Name);
                                }
                                if (fieldName == "designation") {
                                    $(event.currentTarget).attr("designationid", ID);
                                    $(event.currentTarget).attr("designation", Name);
                                }
                            } else {
                                GM.Global.Surgeon.FilterID = ID;
                                GM.Global.Surgeon.FilterName = Name;
                            }
                        }
                    };

                    that.systemSelectView = new GMVCRMSelectPopup(that.systemSelectOptions);
                },


                // Lookup Surgeons by Filters
                searchCategory: function () {
                    var that = this;
                    var input = {};
                    if (this.viewby == "education") {
                        
                        if (GM.Global.Surgeon.FilterFl != "Y")
                            this.getEducationFilters();                       
                        GM.Global.Surgeon.FilterID["stropt"] = "education";                            
                    }
                    if (this.viewby == "professional") {
                        if (GM.Global.Surgeon.FilterFl != "Y")
                            this.getProfessionalFilters();
                        GM.Global.Surgeon.FilterID["stropt"] = "professional";
                    }
                      if (this.viewby == "consultant") {
                         if (GM.Global.Surgeon.FilterFl != "Y")
                            this.getConsultantFilters();
                        GM.Global.Surgeon.FilterID["stropt"] = "consultant";
                    }

                    if (typeof (GM.Global.Surgeon.FilterID) != "object")
                        GM.Global.Surgeon.FilterID = $.parseJSON(GM.Global.Surgeon.FilterID);
                    if (typeof (GM.Global.Surgeon.FilterName) != "object")
                        GM.Global.Surgeon.FilterName = $.parseJSON(GM.Global.Surgeon.FilterName);
                    if (GM.Global.Surgeon == undefined)
                        GM.Global.Surgeon = {
                            "Filter": ""
                        };
                    GM.Global.Surgeon["Filter"] = GM.Global.Surgeon.FilterID;
                    GM.Global.Surgeon["FilterName"] = GM.Global.Surgeon.FilterName;

                    if(!this.emptyFl) {
                        if (GM.Global.Surgeon.FilterID != null || GM.Global.Surgeon.FilterName != null) {
                            if (this.viewby != "contact") {
                                input = GM.Global.Surgeon.FilterID;
                                console.log(input);
                                var tempSpinner = fnGetTemplate(URL_Common_Template, "gmtSpinner");
                                $("#surgeon-report-section").html(tempSpinner);
                                console.log(input);
                                fnGetWebServerData("surgeon/educationinfo", "gmCRMSurgeonlistVO", input, function (data) {                                  
                                    console.log(data);
                                    if (data != undefined) {
                                        _.each(data, function(dt){
                                            dt["picurl"] = URL_WSServer+"file/src?refgroup=103112&refid=" + dt.partyid + "&reftype=null&num=" + Math.random();
                                            if (that.viewby == "education"){
                                                var subt = [];
                                                subt=dt.classyear.split(",");
                                                var newst =_.map(subt, function(st){ 
                                                    var splitYr = st.split(":");
                                                 if(splitYr[1] == "" || splitYr[1] == " ")
                                                    return splitYr[0];                                                   
                                                 else
                                                     return st;                                                 
                                                });
                                                dt["subtitle"]=newst.join("<br>");                                                
                                            }
                                            if (that.viewby == "consultant")
                                                dt["subtitle"] = dt.projectname.split(",").join("<br>");
                                        });
                                        $("#surgeon-report-section").html(that.template_SurgeonReportList(data)); 

                                        if (that.viewby == "professional") {
                                            $("#surgeon-multi-result-count").removeClass("hide");
                                        }else {
                                            $("#surgeon-multi-result-count").addClass("hide");
                                        }
                                    }
                                    else
                                        $("#surgeon-report-section").html("<section class = 'gmTextCenter gmMarginTop20p gmPaddingTop10p'>No Data Found</section>");
                                }); 
                            }
                            else
                                window.location.href = '#crm/surgeon/list/0';
                        }
                    }                     
             

                },

                createSurgeon: function () {
                    window.location.href = "#crm/surgeon/id/0"
                },


                renderEduSearch: function () {
                    var that = this;


                    var searchOptions = {
                        'el': this.$("#surg-education-institutenm"),
                        'url': URL_WSServer + 'search/education',
                        'inputParameters': { 'token': localStorage.getItem('token'), "stropt": "INSTITUTENM" },
                        'keyParameter': 'institutenm',
                        'IDParameter': 'id',
                        'NameParameter': 'institutenm',
                        'resultVO': 'gmCRMSurgeonEducationVO',
                        'minChar': 3,
                        'callback': function (res) {
                            console.log(res);
                            $("#surg-education-institutenm input").val(res.Name);
                        }
                    };
                    var instituteSearch = new GMVSearch(searchOptions);
                    var institutenm = $("#surg-education-institutenm").attr("institutenm");
                    $("#surg-education-institutenm input").val(institutenm);


                    var searchOptions = {
                        'el': this.$("#surg-education-course"),
                        'url': URL_WSServer + 'search/education',
                        'inputParameters': { 'token': localStorage.getItem('token'), "stropt": "COURSE" },
                        'keyParameter': 'course',
                        'IDParameter': 'id',
                        'NameParameter': 'course',
                        'resultVO': 'gmCRMSurgeonEducationVO',
                        'minChar': 3,
                        'callback': function (res) {
                            console.log(res);
                            $("#surg-education-course input").val(res.Name);
                        }
                    };
                    var courseSearch = new GMVSearch(searchOptions);
                    var course = $("#surg-education-course").attr("course");
                    $("#surg-education-course input").val(course);

                    var start = 1900;
                    var end = new Date().getFullYear();
                    var options = [];
                    for (var year = start; year <= end; year++) {
                        options.push({
                            "ID": year,
                            "Name": year
                        });
                    }
                    options.reverse();

                    fnCreateOptions($("#surg-education-classyear"), options);
                    var classyear = $("#surg-education-classyear").attr("classyear");
                    $("#surg-education-classyear").val(classyear);

                },

                renderProfSearch: function () {
                    var that = this;
                    var proftitleOptions = {
                        'el': this.$("#search-surg-proftitle"),
                        'url': URL_WSServer + 'search/professional',
                        'inputParameters': { 'token': localStorage.getItem('token'), "stropt": "TITLE" },
                        'keyParameter': 'title',
                        'IDParameter': 'id',
                        'NameParameter': 'title',
                        'resultVO': 'gmCRMSurgeonProfessionalVO',
                        'resWidth' : '250px',
                        'minChar': 3,
                        'callback': function (res) {
                            console.log(res);
                            $("#search-surg-proftitle input").val(res.Name);
                        }
                    };
                    var proftitleSearch = new GMVSearch(proftitleOptions);
                    var proftitle = $("#search-surg-proftitle").attr("title");
                    $("#search-surg-proftitle input").val(proftitle);


                    var pcompnmOptions = {
                        'el': this.$("#search-surg-profcompanyname"),
                        'url': URL_WSServer + 'search/professional',
                        'inputParameters': { 'token': localStorage.getItem('token'), "stropt": "COMPANYNAME" },
                        'keyParameter': 'companyname',
                        'IDParameter': 'id',
                        'NameParameter': 'companyname',
                        'resultVO': 'gmCRMSurgeonProfessionalVO',
                        'minChar': 3,
                        'callback': function (res) {
                            console.log(res);
                            $("#search-surg-profcompanyname input").val(res.Name);
                        }
                    };
                    var pcompnmSearch = new GMVSearch(pcompnmOptions);
                    var companyname = $("#search-surg-profcompanyname").attr("companyname");
                    $("#search-surg-profcompanyname input").val(companyname);

                    var profgrpdOptions = {
                        'el': this.$("#search-surg-profgroupdivision"),
                        'url': URL_WSServer + 'search/professional',
                        'inputParameters': { 'token': localStorage.getItem('token'), "stropt": "GROUPDIVISION" },
                        'keyParameter': 'groupdivision',
                        'IDParameter': 'id',
                        'NameParameter': 'groupdivision',
                        'resultVO': 'gmCRMSurgeonProfessionalVO',
                        'minChar': 3,
                        'callback': function (res) {
                            console.log(res);
                            $("#search-surg-profgroupdivision input").val(res.Name);
                        }
                    };
                    var profgrpdSearch = new GMVSearch(profgrpdOptions);
                    var groupdivision = $("#search-surg-profgroupdivision").attr("groupdivision");
                    $("#search-surg-profgroupdivision input").val(groupdivision);


                    var profcityOptions = {
                        'el': this.$("#search-surg-profcity"),
                        'url': URL_WSServer + 'search/professional',
                        'inputParameters': { 'token': localStorage.getItem('token'), "stropt": "CITY" },
                        'keyParameter': 'city',
                        'IDParameter': 'id',
                        'NameParameter': 'city',
                        'resultVO': 'gmCRMSurgeonProfessionalVO',
                        'minChar': 3,
                        'callback': function (res) {
                            console.log(res);
                            $("#search-surg-profcity input").val(res.Name);
                        }
                    };
                    var profcitySearch = new GMVSearch(profcityOptions);
                    var city = $("#search-surg-profcity").attr("city");
                    $("#search-surg-profcity input").val(city);


                    var profcountryOptions = {
                        'el': this.$("#search-surg-profcountry"),
                        'url': URL_WSServer + 'search/codelookup',
                        'inputParameters': { 'token': localStorage.getItem('token'), 'CODEGRP': "CNTY" },
                        'keyParameter': 'codenm',
                        'IDParameter': 'codeid',
                        'NameParameter': 'codenm',
                        'resultVO': 'gmCodeLookUpVO',
                        'minChar': 3,
                        'callback': function (res) {
                            console.log(res);
                            $("#search-surg-profcountry").attr("countrynm", res.ID);
                            $("#search-surg-profcountry").attr("country", res.Name);
                            $("#search-surg-profcountry input").val(res.Name);
                        }
                    };
 
                    var profcountrySearch = new GMVSearch(profcountryOptions);
                    var country = $("#search-surg-profcountry").attr("country");
                    $("#search-surg-profcountry input").val(country);
                    


                    var profstateOptions = {
                        'el': this.$("#search-surg-profstate"),
                        'url': URL_WSServer + 'search/codelookup',
                        'inputParameters': { 'token': localStorage.getItem('token'), 'CODEGRP': "STATE" },
                        'keyParameter': 'codenm',
                        'IDParameter': 'codeid',
                        'NameParameter': 'codenm',
                        'resultVO': 'gmCodeLookUpVO',
                        'minChar': 3,
                        'callback': function (res) {
                            console.log(res);
                            $("#search-surg-profstate").attr("state", res.ID);
                            $("#search-surg-profstate").attr("statenm", res.Name);
                            $("#search-surg-profstate input").val(res.Name);
                        }
                    };
                    var profstateSearch = new GMVSearch(profstateOptions);
                    var statenm = $("#search-surg-profstate").attr("statenm");
                    $("#search-surg-profstate input").val(statenm);


                    var start = 1900;
                    var end = new Date().getFullYear();
                    var options = [];
                    for (var year = start; year <= end; year++) {
                        options.push({
                            "ID": year,
                            "Name": year
                        });
                    }
                    options.reverse();

                    fnCreateOptions($("#select-surg-profstartyear"), options);
                    var startyear = $("#select-surg-profstartyear").attr("startyear")
                    $("#select-surg-profstartyear").val(startyear);
                    
                    fnCreateOptions($("#select-surg-profendyear"), options);
                    var endyear = $("#select-surg-profendyear").attr("endyear");
                    $("#select-surg-profendyear").val(endyear);
                    

                    that.monthArr = "";
                    var input = { 'token': localStorage.getItem('token'), 'codegrp': 'MONTH' };
                    fnGetWebServerData("search/codelookup", "gmCodeLookUpVO", input, function (monthArr) {
                        var arrSelectOpt = new Array();
                        arrSelectOpt.push({ "ID": "", "Name": "Select" });
                        monthArr = arrSelectOpt.concat(monthArr);
                        $("#select-surg-profstartmonth").empty();
                        $("#select-surg-profendmonth").empty();
                        var strSelectOptions = JSON.stringify(monthArr).replace(/codeid/g, 'ID').replace(/codenmalt/g, 'Name');
                        var monthArr = $.parseJSON(strSelectOptions);
                        fnCreateOptions($("#select-surg-profstartmonth"), monthArr);
                        var startmonth = $("#select-surg-profstartmonth").attr("startmonth");
                        $("#select-surg-profstartmonth").val(startmonth);
                        
                        fnCreateOptions($("#select-surg-profendmonth"), monthArr);
                        var endmonth = $("#select-surg-profendmonth").attr("endmonth");
                        $("#select-surg-profendmonth").val(endmonth);
                    });

                    var stillinposition = $("#check-surg-profpresent input").attr("stillinposition");
                    if (stillinposition == "Y")
                        $("#check-surg-profpresent input").prop("checked", true);
                    else
                        $("#check-surg-profpresent input").prop("checked", false);

                },
                
         renderConsSearch: function () {             
                    var that = this;
                    var searchOptions = {
                        'el': this.$("#search-surg-roleid"),
                        'url': URL_WSServer + 'search/codelookup',
                        'inputParameters': { 'token': localStorage.getItem('token'), 'codegrp': 'SROLE,GROLE'},
                        'keyParameter': 'codenm',
                        'IDParameter': 'codeid',
                        'NameParameter': 'codenm',
                        'resultVO': 'gmCodeLookUpVO',
                        'minChar': 1,
                        'callback': function (res) {
                            console.log("counseltent=="+res);
                            $("#search-surg-roleid").attr("roleid", res.ID);
                            $("#search-surg-roleid").attr("rolenm", res.Name);
                            $("#search-surg-roleid input").val(res.Name);
                        }
                    };
                    var eduLevelSearch = new GMVSearch(searchOptions);
                    var level = $("#search-surg-roleid").attr("roleid");
                    $("#search-surg-roleid input").val(level);

                    var searchOptions = {
                        'el': this.$("#search-surg-projectnm"),
                        'url': URL_WSServer + 'search/project',                       
                        'inputParameters': { 'token': localStorage.getItem('token')},
                        'keyParameter': 'projectname',
                        'IDParameter': 'projectid',
                        'NameParameter': 'projectname',
                        'resultVO': 'gmProjectBasicVO',
                        'minChar': 3,
                        'callback': function (res) {
                            console.log(res);
                           $("#search-surg-projectid").attr("projectid", res.ID);
                            $("#search-surg-projectnm").attr("projectname", res.Name);
                            $("#search-surg-projectnm input").val(res.Name);

                        }
                    };
                   var projectnmDearch = new GMVSearch(searchOptions);
                    var projectnm = $("#search-surg-projectnm").attr("projectname");
                    $("#search-surg-projectnm input").val(projectnm);

                    
                    var searchOptions = {
                        'el': this.$("#search-surg-projectid"),
                        'url': URL_WSServer + 'search/project', 
                        'inputParameters': { 'token': localStorage.getItem('token')},
                        'keyParameter': 'projectid',
                        'IDParameter': 'projectid',
                        'NameParameter': 'ID',
                        'resultVO': 'gmProjectBasicVO',
                        'minChar': 3,
                        'callback': function (res) {
                            console.log(res);
                            $("#search-surg-projectid").attr("projectid", res.ID);
                            $("#search-surg-projectid").attr("projectname", res.Name);
                            $("#search-surg-projectid input").val(res.Name);
                        }
                    };
                    var projectidDearch = new GMVSearch(searchOptions);
                    var projectid = $("#search-surg-projectid").attr("projectid");
                    $("#search-surg-projectid input").val(projectid); 

         },
                   getConsultantFilters: function () {                  
                   var that = this;
                   var projectid,project,roleid,role,projectnmid,projectnm;
                    project=$("#search-surg-projectid").attr("projectid"); 
                    projectid = $("#search-surg-projectid input").val();
                    
                    projectnm=$("#search-surg-projectnm").attr("projectname");
                    projectnmid = $("#search-surg-projectnm input").val();                   
                    
                    role=$("#search-surg-roleid").attr("roleid")
                    roleid = $("#search-surg-roleid input").val();                       
                       
                      if(parseNull(roleid) == "")
                        role = "";
                    
                    
                      GM.Global.Surgeon.FilterName["projectid"] = parseNull(project);
                      GM.Global.Surgeon.FilterID["projectid"] = parseNull(projectid);
                    
                      GM.Global.Surgeon.FilterName["projectname"] = parseNull(projectnm);
                      GM.Global.Surgeon.FilterID["projectname"] = parseNull(projectnmid);
                     
                      GM.Global.Surgeon.FilterName["roleid"] = parseNull(roleid);
                      GM.Global.Surgeon.FilterID["roleid"] = parseNull(role);
                        if(parseNull(projectid) == ""  && parseNull(projectnmid) == ""  && parseNull(roleid) == "") {
                        this.emptyFl = true; 
                        showError("Please select atleast one filter");
                    }
                    else
                        this.emptyFl = false;
                     
                     
                     
                     
                  },
                // Get filter values from Education fields
                getEducationFilters: function () {
                    var that = this;
                    var eduLevelid, eduLevel, designationid, designation, institutenm, course, classyear;
                    eduLevelid = $("#select-surgeon-edulvl").attr("levelid");
                    eduLevel = $("#select-surgeon-edulvl").text();
                    designationid = $("#select-surgeon-desgn").attr("designationid");
                    designation = $("#select-surgeon-desgn").text();
                    institutenm = $("#surg-education-institutenm input").val();
                    course = $("#surg-education-course input").val();
                    classyear = $("#surg-education-classyear option:selected").val();
                    
                    if(parseNull(designation) == "")
                        designationid = "";
                    if(parseNull(eduLevel) == "")
                        eduLevelid = "";
                        
                    institutenm = institutenm.replace(/'/g, "''");
                    course = course.replace(/'/g, "''");
                    
                    GM.Global.Surgeon.FilterName["Level"] = parseNull(eduLevel);
                    GM.Global.Surgeon.FilterID["level"] = parseNull(eduLevelid);
                    GM.Global.Surgeon.FilterName["Designation"] = parseNull(designation);
                    GM.Global.Surgeon.FilterID["designation"] = parseNull(designationid);
                    GM.Global.Surgeon.FilterName["Institute Name"] = parseNull(institutenm);
                    GM.Global.Surgeon.FilterID["institutenm"] = parseNull(institutenm);
                    GM.Global.Surgeon.FilterName["Course"] = parseNull(course);
                    GM.Global.Surgeon.FilterID["course"] = parseNull(course);
                    GM.Global.Surgeon.FilterName["Class Year"] = parseNull(classyear);
                    GM.Global.Surgeon.FilterID["classyear"] = parseNull(classyear);
                    
                    if(parseNull(eduLevelid) == ""  && parseNull(designationid) == ""  && parseNull(institutenm) == ""  && parseNull(course) == ""  && parseNull(classyear) == "") {
                        this.emptyFl = true; 
                        showError("Please select atleast one filter");
                    }
                    else
                        this.emptyFl = false;
                },

                // Get filter values from Professional fields
                getProfessionalFilters: function () {
                    var that = this;
                    var prfTitle, startMonth, startMonthNm, startYear, endMonth, endMonthNm, endYear, stillinPosition, stillinPositionNm, groupDivision, companyName, city, state, stateNm, countrynm, country;
                    this.emptyFl = false;
                    prfTitle = $("#search-surg-proftitle input").val();
                    startMonth = $("#select-surg-profstartmonth option:selected").val();
                    if (parseNull(startMonth) != "")
                        startMonthNm = getMonthName(startMonth);
                    startYear = $("#select-surg-profstartyear option:selected").val();
                    endMonth = $("#select-surg-profendmonth option:selected").val();;
                    if (parseNull(startMonth) != "")
                        endMonthNm = getMonthName(startMonth);
                    endYear = $("#select-surg-profendyear option:selected").val();
                    
                    
                    stillinPosition = $("#check-surg-profpresent input").prop("checked");
                    console.log(stillinPosition)
                    if (stillinPosition) {
                        stillinPosition = "Y";
                        endMonth = "";
                        endMonthNm = "";
                        endYear = "";
                        stillinPositionNm = "Present";
                    } else {
                        stillinPosition = "";
                        stillinPositionNm = "";
                    }

                    groupDivision = $("#search-surg-profgroupdivision input").val();
                    companyName = $("#search-surg-profcompanyname input").val();

                    city = $("#search-surg-profcity input").val();
                    state = $("#search-surg-profstate").attr("state");
                    stateNm = $("#search-surg-profstate input").val();
                    
                    if(parseNull(stateNm) == "")
                        state = "";
                    
                    countrynm = $("#search-surg-profcountry").attr("countrynm");
                    country = $("#search-surg-profcountry input").val();
                    
                    if(parseNull(country) == "")
                        countrynm = "";
                    
                    prfTitle = prfTitle.replace(/'/g, "''");
                    groupDivision = groupDivision.replace(/'/g, "''");
                    companyName = companyName.replace(/'/g, "''");
                    city = city.replace(/'/g, "''");
                    
                    GM.Global.Surgeon.FilterName["Title"] = parseNull(prfTitle);
                    GM.Global.Surgeon.FilterID["title"] = parseNull(prfTitle);
                    GM.Global.Surgeon.FilterName["Start Month"] = parseNull(startMonthNm);
                    GM.Global.Surgeon.FilterID["startmonth"] = parseNull(startMonth);
                    GM.Global.Surgeon.FilterName["Start Year"] = parseNull(startYear);
                    GM.Global.Surgeon.FilterID["startyear"] = parseNull(startYear);
                    GM.Global.Surgeon.FilterName["End Month"] = parseNull(endMonthNm);
                    GM.Global.Surgeon.FilterID["endmonth"] = parseNull(endMonth);
                    GM.Global.Surgeon.FilterName["End Year"] = parseNull(endYear);
                    GM.Global.Surgeon.FilterID["endyear"] = parseNull(endYear);
                    GM.Global.Surgeon.FilterName["Still In Position"] = parseNull(stillinPositionNm);
                    GM.Global.Surgeon.FilterID["stillinposition"] = parseNull(stillinPosition);
                    GM.Global.Surgeon.FilterName["Group Division"] = parseNull(groupDivision);
                    GM.Global.Surgeon.FilterID["groupdivision"] = parseNull(groupDivision);
                    GM.Global.Surgeon.FilterName["Company Name"] = parseNull(companyName);
                    GM.Global.Surgeon.FilterID["companyname"] = parseNull(companyName);
                    GM.Global.Surgeon.FilterName["City"] = parseNull(city);
                    GM.Global.Surgeon.FilterID["city"] = parseNull(city);
                    GM.Global.Surgeon.FilterName["State"] = parseNull(stateNm);
                    GM.Global.Surgeon.FilterID["state"] = parseNull(state);
                    GM.Global.Surgeon.FilterName["Country"] = parseNull(country);
                    GM.Global.Surgeon.FilterID["countrynm"] = parseNull(countrynm);
                    
                    if(parseNull(prfTitle) == "" && parseNull(startMonth) == "" && parseNull(startYear) == "" && parseNull(endMonth) == "" && parseNull(endYear) == "" && parseNull(stillinPosition) == "" && parseNull(groupDivision) == "" && parseNull(companyName) == "" && parseNull(city) == "" && parseNull(state) == "" && parseNull(countrynm) == "") {
                        this.emptyFl = true; 
                        showError("Please select atleast one filter");
                    }
                    if (parseNull(startMonth) != "" && parseNull(startYear) == "") {
                        this.emptyFl = true; 
                        showError("Please select both Month and Year");
                    }
                    if (parseNull(endMonth) != "" && parseNull(endYear) == "") {
                        this.emptyFl = true; 
                        showError("Please select both Month and Year");
                    }
                    if (parseNull(startYear) != "" && parseNull(endYear) != "") {
                        if (startYear > endYear) {
                            this.emptyFl = true; 
                            showError("End date must be greater than start date");
                        } else if (startYear == endYear) {
                            if (startMonth > endMonth) {
                                this.emptyFl = true;
                                showError("End date must be greater than start date");
                            }
                        } 
                    }
                },
                  // Close view
            closeView: function (view) {
                if (view != undefined) {
                    view.unbind();
                    view.undelegateEvents();
                    view.$el.unbind();
                    view.$el.empty();                    
                    view = undefined;
                    $("#btn-back").unbind();
                }
            },
            
            showLegend: function () {
                $("#rpt-legend-detail").slideToggle();
            },

                //Handles the orietaion change in the iPad
                onOrientationChange: function (event) {
                    resizeHeadWidth('.gmPanelHead', '.gmBtnGroup', '#panelhead-left');
                }
            });
            return GM.View.SurgeonReport;
        });