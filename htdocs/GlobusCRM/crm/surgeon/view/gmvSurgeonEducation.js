/**********************************************************************************
* File             :           gmvSurgeonEducation.js
* Description      :           View to represent Surgeon Education and Professional Experience Details
* Version          :           1.0
* Author           :           jgurunathan
 **********************************************************************************/
define([

    // External Libraries
    'jquery', 'underscore', 'backbone', 'handlebars', 'jqueryui',

    // Common
    'global', 'gmvSearch',
    
    // View
    'gmvCRMData','gmvCRMSelectPopup',

    // Pre-Compiled Template
    'gmtTemplate', 'gmvSurgeonDetail', 'commonutil'
    
    //Model
    
], 

function ($, _, Backbone, Handlebars, JqueryUI, Global, GMVSearch, GMVCRMData, GMVCRMSelectPopup, GMTTemplate, GMVSurgeonDetail, Commonutil) {

    'use strict'; 
    
    GM.View.SurgeonEducation = Backbone.View.extend({

        name: "CRM Surgeon Criteria View",

        /* Load the templates */ 
        template: fnGetTemplate(URL_Surgeon_Template, "gmtSurgeonEducationEdit"),
        
        // events declaration
        events: {
            "click #surg-education-edit-icon, #surg-professional-edit-icon": "editEducation",
            "click #surg-education-save-icon": "saveEducation",
            "click #surg-professional-save-icon": "saveProfessional",
            "click #surg-education-remove-icon": "removeEducation",
            "click #surg-professional-remove-icon": "removeProfessional",
            "click #surg-new-education": "addEducation",
            "click #surg-new-professional": "addProfessional",
            "click  .btn-listby": "openPopup",
            "click #check-surg-profpresent": "addPresentDate",
        },
        
        initialize: function(options) {
            console.log(options);
            this.el = options.el;
            this.parent = options.parent;
            console.log(this.parent.model);
            
            var eduData = _.reject(this.parent.model.get("arrgmcrmeducationvo"), function (dt) {
                return dt.classyear == "" && dt.institutenm == "";
            });
            this.parent.model.set("arrgmcrmeducationvo", eduData);
            var profData = _.reject(this.parent.model.get("arrgmcrmprofessionalvo"), function (dt) {
                return dt.title == "" &&  dt.companyname == "";
            });
            this.parent.model.set("arrgmcrmprofessionalvo", profData);
            
            this.render();
        },

        render: function(e) {
            var that = this;
            console.log(e);
            var ediEducation = this.parent.model.toJSON();

            _.each(ediEducation.arrgmcrmeducationvo, function(data, i){
                data.index = i++;
            });
            _.each(ediEducation.arrgmcrmprofessionalvo, function(data, i){
                data.index = i++;
            });
            console.log(ediEducation.arrgmcrmeducationvo);
            console.log($(this.el))
            $(this.el).html(this.template(ediEducation));
            $(".education-eitem").addClass("hide");
            $(".professional-eitem").addClass("hide");
            if(e != undefined) {
                if($(e.currentTarget).attr("id") == "surg-new-education") {
                    $(".education-surgeon-data #ul-surg-education-list").each(function(dt){
                        console.log($(this).attr("data-id"));
                        if($(this).attr("data-id") == "") {
                            console.log($(this).find("#surg-education-edit-icon"))
                            var event = {};
                            event.currentTarget = $(this).find("#surg-education-edit-icon");
                            that.editEducation(event);
                        }
                    });

                } 
                if($(e.currentTarget).attr("id") == "surg-new-professional") {
                    $(".professional-surgeon-data #ul-surg-professional-list").each(function(dt){
                        console.log($(this).attr("data-id"));
                        if($(this).attr("data-id") == "") {
                            console.log($(this).find("#surg-professional-edit-icon"))
                            var event = {};
                            event.currentTarget = $(this).find("#surg-professional-edit-icon");
                            that.editEducation(event);
                        }
                    });

                } 
            }
        },
        
        
        // Open Single / Multi Select box        
        openPopup: function(event){
            var that = this;
            var fieldName = $(event.currentTarget).attr("data-name");
            console.log(fieldName);
            this.selectedID = [];
            this.selectedNm = [];
            var reData = $(event.currentTarget).attr("redata");
            that.systemSelectOptions = {                                                                                                              
                'title': $(event.currentTarget).attr("data-title"),
                'storagekey': $(event.currentTarget).attr("data-storagekey"),
                'webservice': $(event.currentTarget).attr("data-webservice"),   
                'resultVO': $(event.currentTarget).attr("data-resultVO"),
                'IDParameter': $(event.currentTarget).attr("data-IDParameter"),
                'NameParameter': $(event.currentTarget).attr("data-NameParameter"),
                'inputParameters': $(event.currentTarget).attr("data-inputParameters"),
                'codegrp': $(event.currentTarget).attr("data-codegrp"),
                'module': "surgeon",
                'event': event,
                'parent': this,
                'type' : 2,
                'renderOne': reData,
                'moduleLS': this.selectedID,
                'moduleCLS': this.selectedNm,
                'callback': function(ID, Name){
                    $(event.currentTarget).html(Name);
                    $(event.currentTarget).attr("redata", ID);
                    if(fieldName == "level") {
                        $(event.currentTarget).attr("levelid", ID);
                        $(event.currentTarget).attr("level", Name); 
                    }
                    if(fieldName == "designation") {
                        $(event.currentTarget).attr("designationid", ID);
                        $(event.currentTarget).attr("designation", Name); 
                    }
                }
            };
            that.systemSelectView = new GMVCRMSelectPopup(that.systemSelectOptions);
        },
        
        // render the Education serach box view
        renderEduSearch: function (e) {
            var that = this;
            var searchOptions = {
                'el': $(e.currentTarget).parent().find("#surg-education-institutenm"),
                'url': URL_WSServer + 'search/education',
                'inputParameters': {'token': localStorage.getItem('token'), "stropt" : "INSTITUTENM"},
                'keyParameter': 'institutenm',
                'IDParameter': 'id',
                'NameParameter': 'institutenm',
                'resultVO': 'gmCRMSurgeonEducationVO',
                'minChar': 3,
                'callback': function (res) {
                    console.log(res);
                    that.setInstitute(e, res);
                }
            };
            var instituteSearch = new GMVSearch(searchOptions);
            var institutenmVal = $(e.currentTarget).parent().find("#surg-education-institutenm").attr("institutenm");
            $(e.currentTarget).parent().find("#surg-education-institutenm input").val(institutenmVal);
            $(e.currentTarget).parent().find("#surg-education-institutenm input").attr("maxlength",'200');
            
            var searchOptions = {
                'el': $(e.currentTarget).parent().find("#surg-education-course"),
                'url': URL_WSServer + 'search/education',
                'inputParameters': {'token': localStorage.getItem('token'), "stropt" : "COURSE"},
                'keyParameter': 'course',
                'IDParameter': 'id',
                'NameParameter': 'course',
                'resultVO': 'gmCRMSurgeonEducationVO',
                'minChar': 3,
                'callback': function (res) {
                    console.log(res);
                    that.setCourse(e, res);
                }
            };
            var courseSearch = new GMVSearch(searchOptions);
            var courseVal = $(e.currentTarget).parent().find("#surg-education-course").attr("course");
            $(e.currentTarget).parent().find("#surg-education-course input").val(courseVal);
            $(e.currentTarget).parent().find("#surg-education-course input").attr("maxlength",'200');
            
            var start = 1900;
            var end = new Date().getFullYear();
            var options = [];
            for (var year = start; year <= end; year++) {
                options.push({"ID":year, "Name": year});
            }
            options.reverse();
                                      
            fnCreateOptions($(e.currentTarget).parent().find("#select-surg-classyear"), options);
            var classYear = $(e.currentTarget).parent().find("#select-surg-classyear").attr("classyear");
            if(parseNull(classYear) != "")
                $(e.currentTarget).parent().find("#select-surg-classyear").val(classYear);
            
        },
        
        // Render Professtion Experience search box view
        renderProfSearch: function (e) {
            var that = this;
            var proftitleOptions = {
                'el': $(e.currentTarget).parent().find("#search-surg-proftitle"),
                'url': URL_WSServer + 'search/professional',
                'inputParameters': {'token': localStorage.getItem('token'), "stropt" : "TITLE"},
                'keyParameter': 'title',
                'IDParameter': 'id',
                'NameParameter': 'title',
                'resultVO': 'gmCRMSurgeonProfessionalVO',
                'resWidth' : '250px',
                'minChar': 3,
                'callback': function (res) {
                    console.log(res);
                    that.setTitle(e, res);
                }
            };
            var proftitleSearch = new GMVSearch(proftitleOptions);
            var proftitleVal = $(e.currentTarget).parent().find("#search-surg-proftitle").attr("title");
            $(e.currentTarget).parent().find("#search-surg-proftitle input").val(proftitleVal);
            $(e.currentTarget).parent().find("#search-surg-proftitle input").attr("maxlength",'50');
            
            var pcompnmOptions = {
                'el': $(e.currentTarget).parent().find("#search-surg-profcompanyname"),
                'url': URL_WSServer + 'search/professional',
                'inputParameters': {'token': localStorage.getItem('token'), "stropt" : "COMPANYNAME"},
                'keyParameter': 'companyname',
                'IDParameter': 'id',
                'NameParameter': 'companyname',
                'resultVO': 'gmCRMSurgeonProfessionalVO',
                'minChar': 3,
                'callback': function (res) {
                    console.log(res);
                    that.setCompany(e, res);
                }
            };
            var pcompnmSearch = new GMVSearch(pcompnmOptions);
            var pcompnmVal = $(e.currentTarget).parent().find("#search-surg-profcompanyname").attr("companyname");
            $(e.currentTarget).parent().find("#search-surg-profcompanyname input").val(pcompnmVal);
            $(e.currentTarget).parent().find("#search-surg-profcompanyname input").attr("maxlength",'200');
            var profgrpdOptions = {
                'el': $(e.currentTarget).parent().find("#search-surg-profgroupdivision"),
                'url': URL_WSServer + 'search/professional',
                'inputParameters': {'token': localStorage.getItem('token'), "stropt" : "GROUPDIVISION"},
                'keyParameter': 'groupdivision',
                'IDParameter': 'id',
                'NameParameter': 'groupdivision',
                'resultVO': 'gmCRMSurgeonProfessionalVO',
                'minChar': 3,
                'callback': function (res) {
                    console.log(res);
                    that.setGroupDivision(e, res);
                }
            };
            var profgrpdSearch = new GMVSearch(profgrpdOptions);
            var profgrpdVal = $(e.currentTarget).parent().find("#search-surg-profgroupdivision").attr("groupdivision");
            $(e.currentTarget).parent().find("#search-surg-profgroupdivision input").val(profgrpdVal);
            $(e.currentTarget).parent().find("#search-surg-profgroupdivision input").attr("maxlength",'200');
            
            var profcityOptions = {
                'el': $(e.currentTarget).parent().find("#search-surg-profcity"),
                'url': URL_WSServer + 'search/professional',
                'inputParameters': {'token': localStorage.getItem('token'), "stropt" : "CITY"},
                'keyParameter': 'city',
                'IDParameter': 'id',
                'NameParameter': 'city',
                'resultVO': 'gmCRMSurgeonProfessionalVO',
                'minChar': 3,
                'callback': function (res) {
                    console.log(res);
                    that.setCity(e, res);
                }
            };
            var profcitySearch = new GMVSearch(profcityOptions);
            var profcityVal = $(e.currentTarget).parent().find("#search-surg-profcity").attr("city");
            $(e.currentTarget).parent().find("#search-surg-profcity input").val(profcityVal);
            $(e.currentTarget).parent().find("#search-surg-profcity input").attr("maxlength",'100');
            
            var profcountryOptions = {
                'el': $(e.currentTarget).parent().find("#search-surg-profcountry"),
                'url': URL_WSServer + 'search/codelookup',
                'inputParameters': {'token': localStorage.getItem('token'), 'CODEGRP' : "CNTY"},
                'keyParameter': 'codenm',
                'IDParameter': 'codeid',
                'NameParameter': 'codenm',
                'resultVO': 'gmCodeLookUpVO',
                'minChar': 3,
                'callback': function (res) {
                    console.log(res);
                    that.setCountry(e, res);
                }
            };
            var profcountrySearch = new GMVSearch(profcountryOptions);
            var profcountryVal = $(e.currentTarget).parent().find("#search-surg-profcountry").attr("country");
            $(e.currentTarget).parent().find("#search-surg-profcountry input").val(profcountryVal);
            
            var profstateOptions = {
                'el': $(e.currentTarget).parent().find("#search-surg-profstate"),
                'url': URL_WSServer + 'search/codelookup',
                'inputParameters': {'token': localStorage.getItem('token'), 'CODEGRP' : "STATE"},
                'keyParameter': 'codenm',
                'IDParameter': 'codeid',
                'NameParameter': 'codenm',
                'resultVO': 'gmCodeLookUpVO',
                'minChar': 3,
                'callback': function (res) {
                    console.log(res);
                    that.setState(e, res);
                }
            };
            var profstateSearch = new GMVSearch(profstateOptions);
            var profstateVal = $(e.currentTarget).parent().find("#search-surg-profstate").attr("state");
            $(e.currentTarget).parent().find("#search-surg-profstate input").val(profstateVal);
            
            if($(e.currentTarget).parent().find("#search-surg-profcountry").attr("countryid") != "1101")
                $(e.currentTarget).parent().find("#search-surg-profstate input").attr("disabled", "disabled");
            
            var start = 1900;
            var end = new Date().getFullYear();
            var options = [];
            for (var year = start; year <= end; year++) {
                options.push({"ID":year, "Name": year});
            }
            options.reverse();
                                      
            fnCreateOptions($(e.currentTarget).parent().find("#select-surg-profstartyear"), options);
            var startyearVal = $(e.currentTarget).parent().find("#select-surg-profstartyear").attr("startyear");
            console.log(startyearVal);
            if (parseNull(startyearVal) != "")
                $(e.currentTarget).parent().find("#select-surg-profstartyear").val(startyearVal);
            else
                $(e.currentTarget).parent().find("#select-surg-profstartyear").val("");
            
            fnCreateOptions($(e.currentTarget).parent().find("#select-surg-profendyear"), options);
            var endyearVal = $(e.currentTarget).parent().find("#select-surg-profendyear").attr("endyear");
            if (parseNull(endyearVal) != "")
                $(e.currentTarget).parent().find("#select-surg-profendyear").val(endyearVal);
            else
                $(e.currentTarget).parent().find("#select-surg-profendyear").val("");
            
            that.monthArr = "";
            var input = { 'token': localStorage.getItem('token'), 'codegrp': 'MONTH' };
            fnGetWebServerData("search/codelookup", "gmCodeLookUpVO", input, function (monthArr) {
                console.log(monthArr);
//                $(e.currentTarget).parent().find("#select-surg-profstartmonth").empty();
//                $(e.currentTarget).parent().find("#select-surg-profendmonth").empty();
                var strSelectOptions = JSON.stringify(monthArr).replace(/codeid/g, 'ID').replace(/codenmalt/g, 'Name');
                var monthArr = $.parseJSON(strSelectOptions);
                fnCreateOptions($(e.currentTarget).parent().find("#select-surg-profstartmonth"), monthArr);
                var startmonthVal = $(e.currentTarget).parent().find("#select-surg-profstartmonth").attr("startmonth");
                console.log(startmonthVal);
                if (parseNull(startmonthVal) != "")
                    $(e.currentTarget).parent().find("#select-surg-profstartmonth").val(startmonthVal);
                else
                    $(e.currentTarget).parent().find("#select-surg-profstartmonth").val("");
                fnCreateOptions($(e.currentTarget).parent().find("#select-surg-profendmonth"), monthArr);
                var endmonthVal = $(e.currentTarget).parent().find("#select-surg-profendmonth").attr("endmonth");
                if (parseNull(endmonthVal) != "")
                    $(e.currentTarget).parent().find("#select-surg-profendmonth").val(endmonthVal);
                else
                    $(e.currentTarget).parent().find("#select-surg-profendmonth").val("");
                });
            
            var stillinposition = $(e.currentTarget).parent().find("#check-surg-profpresent input").attr("stillinposition");
            if(stillinposition == "Y")
                $(e.currentTarget).parent().find("#check-surg-profpresent input").prop("checked", true);
            else
                $(e.currentTarget).parent().find("#check-surg-profpresent input").prop("checked", false);
            
        },
        
        setInstitute: function (e, res) {
            $(e.currentTarget).parent().find("#surg-education-institutenm input").val(res.Name);
        },
        
        setCourse: function (e, res) {
            $(e.currentTarget).parent().find("#surg-education-course input").val(res.Name);
        },
        
        setTitle: function (e,res) {
            $(e.currentTarget).parent().find("#search-surg-proftitle").attr("titleid", res.ID);
            $(e.currentTarget).parent().find("#search-surg-proftitle").attr("title", res.Name);
            $(e.currentTarget).parent().find("#search-surg-proftitle input").val(res.Name);
        },
        setCompany: function (e,res) {
            $(e.currentTarget).parent().find("#search-surg-profcompanyname").attr("companynameid", res.ID);
            $(e.currentTarget).parent().find("#search-surg-profcompanyname").attr("companyname", res.Name);
            $(e.currentTarget).parent().find("#search-surg-profcompanyname input").val(res.Name);           
        },
        setGroupDivision: function (e,res) {
            $(e.currentTarget).parent().find("#search-surg-profgroupdivision").attr("groupdivisionid", res.ID);
            $(e.currentTarget).parent().find("#search-surg-profgroupdivision").attr("groupdivision", res.Name);
            $(e.currentTarget).parent().find("#search-surg-profgroupdivision input").val(res.Name);            
        },
        setCity: function (e,res) {
            $(e.currentTarget).parent().find("#search-surg-profcity").attr("cityid", res.ID);
            $(e.currentTarget).parent().find("#search-surg-profcity").attr("city", res.Name);
            $(e.currentTarget).parent().find("#search-surg-profcity input").val(res.Name);            
        },
        setCountry: function (e,res) {
            $(e.currentTarget).parent().find("#search-surg-profcountry").attr("countryid", res.ID);
            $(e.currentTarget).parent().find("#search-surg-profcountry").attr("country", res.Name);
            $(e.currentTarget).parent().find("#search-surg-profcountry input").val(res.Name);
            
            if (res.ID == "1101") 
                $(e.currentTarget).parent().find("#search-surg-profstate input").removeAttr("disabled");
            else {
                $(e.currentTarget).parent().find("#search-surg-profstate input").attr("disabled", "disabled"); 
                $(e.currentTarget).parent().find("#search-surg-profstate input").val("");
            }
        },
        setState: function (e,res) {
            $(e.currentTarget).parent().find("#search-surg-profstate").attr("stateid", res.ID);
            $(e.currentTarget).parent().find("#search-surg-profstate").attr("state", res.Name);
            $(e.currentTarget).parent().find("#search-surg-profstate input").val(res.Name);            
        },
        
        addEducation: function (e) {
            var that = this;
            var newDataCnt = _.filter(this.parent.model.get("arrgmcrmeducationvo"), function(dt){
                return dt.classyear == "" && dt.institutenm == "";
            });
            if(newDataCnt.length == 0){
                this.parent.model.get("arrgmcrmeducationvo").push({
                    "index": "",
                    "id": "",
                    "level": "",
                    "levelid": "",
                    "institutenm": "",
                    "classyear": "",
                    "course": "",
                    "designation": "",
                    "designationid": "",
                    "voidfl": ""
                });
                this.render(e); 
            }
        },
        addProfessional: function(e) {
            var that = this;
            var newDataCnt = _.filter(this.parent.model.get("arrgmcrmprofessionalvo"), function(dt){
                return dt.title == "" &&  dt.companyname == "";
            });
            if(newDataCnt.length == 0){
                this.parent.model.get("arrgmcrmprofessionalvo").push({
                    "index": "",
                    "careerid": "",
                    "title": "",
                    "companyname": "",
                    "startmonth": "",
                    "startyear": "",
                    "endmonth": "",
                    "endyear": "",
                    "groupdivision": "",
                    "country": "",
                    "countryid": "",
                    "state": "",
                    "stateid": "",
                    "city": "",
                    "voidfl": ""
                });
                this.render(e); 
            }
        },
        
        editEducation: function (e) {
            var that = this;
            if($(e.currentTarget).attr("id") == "surg-education-edit-icon") {
                $(".education-eitem").addClass("hide");
                $(".education-vitem").removeClass("hide");
                $(e.currentTarget).parent().find(".education-eitem").removeClass("hide");
                $(e.currentTarget).parent().find(".education-vitem").addClass("hide");
                this.renderEduSearch(e);
            }
            if($(e.currentTarget).attr("id") == "surg-professional-edit-icon") {
                $(".professional-eitem").addClass("hide");
                $(".professional-vitem").removeClass("hide");
                $(e.currentTarget).parent().find(".professional-eitem").removeClass("hide");
                $(e.currentTarget).parent().find(".professional-vitem").addClass("hide"); 
                this.renderProfSearch(e);
            }
            
        },
        
        saveEducation: function (e) {
            var index, id, level, levelid, institutenm, classyear, course, designation, designationid;
            index = $(e.currentTarget).parent().find(".education-eitem").attr("index");
            id = $(e.currentTarget).parent().find(".education-eitem").attr("data-id");
            level = $(e.currentTarget).parent().find("#select-surgeon-edulvl").attr("level");
            levelid = $(e.currentTarget).parent().find("#select-surgeon-edulvl").attr("levelid");
            institutenm = $(e.currentTarget).parent().find("#surg-education-institutenm input").val();
            classyear = $(e.currentTarget).parent().find("#select-surg-classyear option:selected").val();
            course = $(e.currentTarget).parent().find("#surg-education-course input").val();
            designation = $(e.currentTarget).parent().find("#select-surgeon-desgn").attr("designation");
            designationid = $(e.currentTarget).parent().find("#select-surgeon-desgn").attr("designationid");
            console.log(level,levelid,institutenm,classyear,course,designation,designationid);
            if(level != "" && institutenm != "" && course != "" && designation != "") {
                $(".education-eitem").addClass("hide");
                $(e.currentTarget).parent().find(".education-vitem").removeClass("hide");
                var oldData = _.findWhere(this.parent.model.get("arrgmcrmeducationvo"), function (dt) {
                    return dt.index == index;
                });

                if (oldData != undefined)
                    if (oldData.length != 0) {
                        var newData = _.reject(this.parent.model.get("arrgmcrmeducationvo"), function (dt) {
                            return dt.index == index;
                        });
                        this.parent.model.set("arrgmcrmeducationvo", newData);
                    }
                this.parent.model.get("arrgmcrmeducationvo").push({
                    "index": index,
                    "id": parseNull(id),
                    "level": level,
                    "levelid": levelid,
                    "institutenm": institutenm,
                    "classyear": classyear,
                    "course": course,
                    "designation": designation,
                    "designationid": designationid,
                    "voidfl": ""
                });
                console.log(this.parent.model.get("arrgmcrmeducationvo"));
                this.render(e); 
            }
            else
                showError("Please enter information into all fields before saving");
        },
        
        
        saveProfessional: function (e) {
            var index, careerid, title, companyname, startmonth, startyear, endmonth, endyear, groupdivision, country, countryid, state, stateid, city, present, startdate, enddate, startmonthTxt, endmonthTxt, stillinposition, errorfl = "";
            index = $(e.currentTarget).parent().find(".professional-eitem").attr("index");
            careerid = $(e.currentTarget).parent().find(".professional-eitem").attr("data-id");
            title = $(e.currentTarget).parent().find("#search-surg-proftitle input").val();
            companyname = $(e.currentTarget).parent().find("#search-surg-profcompanyname input").val();
            startmonth = $(e.currentTarget).parent().find("#select-surg-profstartmonth option:selected").val();
            startmonthTxt = $(e.currentTarget).parent().find("#select-surg-profstartmonth option:selected").text();
            startyear = $(e.currentTarget).parent().find("#select-surg-profstartyear option:selected").text();
            endmonth = $(e.currentTarget).parent().find("#select-surg-profendmonth option:selected").val();
            endmonthTxt = $(e.currentTarget).parent().find("#select-surg-profendmonth option:selected").text();
            endyear = $(e.currentTarget).parent().find("#select-surg-profendyear option:selected").text();
            groupdivision = $(e.currentTarget).parent().find("#search-surg-profgroupdivision input").val();
            country = $(e.currentTarget).parent().find("#search-surg-profcountry input").val();
            countryid = $(e.currentTarget).parent().find("#search-surg-profcountry").attr("countryid");
            state = $(e.currentTarget).parent().find("#search-surg-profstate input").val();
            stateid = $(e.currentTarget).parent().find("#search-surg-profstate").attr("stateid");
            city = $(e.currentTarget).parent().find("#search-surg-profcity input").val();
            present = $(e.currentTarget).parent().find("#check-surg-profpresent input").prop("checked");
            
            if(parseNull(startmonth) != "")
                startdate = startmonthTxt +"/"+startyear;
            else
                startdate = startyear;
            
            if(parseNull(endmonth) != "")
                enddate = endmonthTxt+"/"+endyear;
            else
                enddate = endyear;
            
            if (present == true) {
                endmonth = "";
                endyear = "";
                stillinposition = "Y";
                enddate = "Present";                
            } else {
                stillinposition = "";
            }
            
            if(parseNull(startyear) != "" && parseNull(endyear) != "") {
                if (startyear > endyear) {
                    errorfl = "Y";
                    showError("End date must be greater than start date");
                }
				 else if (startyear == endyear) {
                    if (startmonth > endmonth) {
                        errorfl = "Y";
                        showError("End date must be greater than start date");
                    }
                }  else
                    errorfl = "";
                            }
            if((startmonth != "" && startyear == "") || (endmonth != "" && endyear == "")){
                errorfl = "Y";
                        showError("Please select the year");
            }
            
            if(parseNull(countryid) != '1101') { //Except US, other states are N/A
                state = 'N/A';
                stateid = '1057'
            }
            
            if(errorfl == "") {

                if(title != "" && companyname != ""  && groupdivision != "" && country != "" &&  state != "" &&  city != "") {
                $(".professional-eitem").addClass("hide");
                $(e.currentTarget).parent().find(".professional-vitem").removeClass("hide");

                    var oldData = _.findWhere(this.parent.model.get("arrgmcrmprofessionalvo"), function (dt) {
                        return dt.index == index;
                    });

                    if (oldData != undefined)
                        if (oldData.length != 0) {
                            var newData = _.reject(this.parent.model.get("arrgmcrmprofessionalvo"), function (dt) {
                                return dt.index == index;
                            });
                            this.parent.model.set("arrgmcrmprofessionalvo", newData);
                        }
                    this.parent.model.get("arrgmcrmprofessionalvo").push({
                        "index": index,
                        "careerid": careerid,
                        "title": title,
                        "companyname": companyname,
                        "startdate": startdate,
                        "enddate": enddate,
                        "startmonth": startmonth,
                        "startyear": startyear,
                        "endmonth": endmonth,
                        "endyear": endyear,
                        "groupdivision": groupdivision,
                        "country": country,
                        "countryid": countryid,
                        "state": state,
                        "stateid": stateid,
                        "city": city,
                        "stillinposition" : stillinposition,
                        "voidfl": ""
                    });
                    console.log(this.parent.model.get("arrgmcrmprofessionalvo"));
                    this.render(e); 
                } 
                else
                   showError("Please enter information into all fields before saving"); 
            }
        },
        
        addPresentDate: function(e){
           var checkPresent = $(e.currentTarget).find("input").prop("checked"); 
            if(checkPresent==true){
                $(e.currentTarget).parent().find("#select-surg-profendyear").val("");
                $(e.currentTarget).parent().find("#select-surg-profendmonth").val("");
            }
        },
        
        removeEducation: function (e) {
            var rmv = confirm("Are you sure you want to remove this row?");
            if (rmv == true) {
                var that = this;
                $(".education-eitem").addClass("hide");
                $(e.currentTarget).parent().find(".education-eitem").addClass("hide");
                $(e.currentTarget).parent().find(".education-vitem").removeClass("hide");

                var index = $(e.currentTarget).parent().find(".education-eitem").attr("index");
                var educationid = $(e.currentTarget).parent().find(".education-eitem").attr("data-id");

                if (parseNull(educationid) == "") {
                    var rmArr = _.reject(this.parent.model.get("arrgmcrmeducationvo"), function (dt) {
                        return dt.index == index;
                    });
                } else {
                    var rmArr = _.each(this.parent.model.get("arrgmcrmeducationvo"), function (dt) {
                        if (dt.id == educationid)
                            dt.voidfl = "Y";
                    });
                }
                this.parent.model.set("arrgmcrmeducationvo", rmArr);
                console.log(this.parent.model.get("arrgmcrmeducationvo"));
                $(e.currentTarget).parent().remove();
                this.render(e);
            }
        },
        
        removeProfessional: function (e) {
            var rmv = confirm("Are you sure you want to remove this row?");
            if (rmv == true) {
                var that = this;
                $(".professional-eitem").addClass("hide");
                $(e.currentTarget).parent().find(".professional-eitem").addClass("hide");
                $(e.currentTarget).parent().find(".professional-vitem").removeClass("hide");

                var index = $(e.currentTarget).attr("index");
                var professionalid = $(e.currentTarget).attr("data-id");


                if (parseNull(professionalid) == "") {
                    var rmArr = _.reject(this.parent.model.get("arrgmcrmprofessionalvo"), function (dt) {
                        return dt.index == index;
                    });
                } else {
                    var rmArr = _.each(this.parent.model.get("arrgmcrmprofessionalvo"), function (dt) {
                        if (dt.careerid == professionalid)
                            dt.voidfl = "Y";
                    });
                }
                this.parent.model.set("arrgmcrmprofessionalvo", rmArr);
                console.log(this.parent.model.get("arrgmcrmprofessionalvo"));
                $(e.currentTarget).parent().remove();
                this.render(e);
            }
        }
        
    });
    return GM.View.SurgeonEducation;
}); 
