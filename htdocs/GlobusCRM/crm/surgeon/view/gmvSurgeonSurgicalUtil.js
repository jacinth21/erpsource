/**********************************************************************************
 * File             :        gmvSurgeonSurgicalUtil.js
 * Description      :        Function for chart util functions
 * Path             :        /web/GlobusCRM/crm/surgeon/view/gmvSurgeonSurgicalUtil.js
 * Author           :        mahendrand
 **********************************************************************************/
var chartClrs = ['#01B8AA', '#6495ed', '#FD625E', '#F2C80F', '#9bb960', '#8AD4AF', '#FE9666', '#A66999', '#3599B8', '#DFBFBF', '#8C82C9', '#A4DDEE', '#6FB1BD', '#7F898A', '#FDAB89', '#8d6345', '#FDAB89', '#5F9EA0', '#9370DB', '#677d40', '#374649', '#5F6B6D'];
//var chartClrs1 = ['#01B8AA', '#374649', '#FD625E'];
var chartClrs1 = ['#5B9BD5', '#ED7D31', '#A5A5A5'];


//Web service call by procedure or revenue varies by input
function ProcRevByWS(input, callback) {
    fnGetWebServerData("surgeon/surigicalact", "gmCRMSurgicalActivityVO", input, function (chartdata) {
        var data = [],
            accID, resData = [];
        chartdata = _.sortBy(chartdata, 'yearmon').reverse();
        for (var i = 0; i < chartdata.length; i++) {
            data[i] = {};
            data[i]["sysnm"] = chartdata[i].grpnm2;
            data[i]["sysid"] = chartdata[i].grpid2;
            data[i]["value"] = chartdata[i].value;
            data[i]["yearmon"] = chartdata[i].yearmon;
            data[i]["segmentnm"] = chartdata[i].grpnm;
            data[i]["segmentid"] = chartdata[i].grpid;
            data[i]["accid"] = chartdata[i].grpid3;
        }
        accID = $('#accid').val();
        if (accID != '') {
            resData = _.filter(data, function (dt) {
                return dt.accid === accID;
            });
        } else
            resData = data;
        callback(resData);
    });

}

function ProcRevByArrayAggWS(input, callback) {
    fnGetWebServerData("accountrpt/surigicalact", "undefined", input, function (chartdata) {
        var data = [],
            accID, resData = [];
        chartdata = _.sortBy(chartdata, 'yearmon').reverse();
        for (var i = 0; i < chartdata.length; i++) {
            data[i] = {};
            data[i]["sysnm"] = chartdata[i].grpnm2;
            data[i]["sysid"] = chartdata[i].grpid2;
            data[i]["value"] = chartdata[i].value;
            data[i]["yearmon"] = chartdata[i].yearmon;
            data[i]["segmentnm"] = chartdata[i].grpnm;
            data[i]["segmentid"] = chartdata[i].grpid;
            data[i]["accid"] = chartdata[i].grpid3;
        }
        accID = $('#accid').val();
        if (accID != '') {
            resData = _.filter(data, function (dt) {
                return dt.accid === accID;
            });
        } else
            resData = data;
        callback(resData);
    });

}

// Get DOCOUNT by using Webservice by passing inputs and send the data through callback
function DOCountByWS(input, callback) {
    fnGetWebServerData("surgeon/surigicalactdocnt", "gmCRMSurgicalDOVO", input, function (data) {
        callback(data);
    });
}

// Get DOCOUNT by using Webservice by passing inputs and send the data through callback
function DOCountByJOBJWS(input, callback) {
    console.log(JSON.stringify(input));
    fnGetWebServerData("accountrpt/surigicalactdocnt","undefined", input, function (data) {
        callback(data);
    });
}

//System Formatted  Chart Data
function systemChartMainData(chartData) {
    var i = 0,
        resChartdata = [],
        grpChartData = [],
        chartName;
    grpChartData = _.groupBy(chartData, function (row) {
        return row.sysid;
    });
    _.each(grpChartData, function (rmvDupData) {
        var sysnm, sysid, segmentnm, accid, segmentid, totalvalue = 0,
            amt = 0;
        _.each(rmvDupData, function (chartDatas) {
            segmentid = chartDatas.segmentid;
            segmentnm = chartDatas.segmentnm;
            sysnm = chartDatas.sysnm;
            sysid = chartDatas.sysid;
            totalvalue = parseFloat(totalvalue) + parseFloat(chartDatas.value);
            accid = chartDatas.accid;
        });
        resChartdata[i] = {};
        resChartdata[i]["label"] = sysnm;
        resChartdata[i]["value"] = totalvalue;
        resChartdata[i]["fillcolor"] = chartClrs[i];
        resChartdata[i]["sysid"] = sysid;
        resChartdata[i]["segmentnm"] = segmentnm;
        resChartdata[i]["segmentid"] = segmentid;
        resChartdata[i]["accid"] = accid;
        i++;
    });
    return resChartdata;
}

//System data Filtered 20 
function systemChartData(chartData) {
    var resChartdata = [];
    resChartdata = systemChartMainData(chartData);
    resChartdata = resChartdata.slice(0, 20);
    return resChartdata;
}

// Segment Formatted Chart Data with filtered 20 datas only
function segmentChartData(chartData, chartColors) {
    var i = 0,
        sysChartData = [],
        grpSgmtChartData = [],
        sgmtChartData = [];
    sysChartData = systemChartMainData(chartData);
    grpSgmtChartData = _.groupBy(sysChartData, function (row) {
        return row.segmentid;
    });
    _.each(grpSgmtChartData, function (segData, label) {
        var segmentnm, segmentid, total = 0;
        _.each(segData, function (segDatas) {
            total = parseFloat(total) + parseFloat(segDatas.value);
            segmentnm = segDatas.segmentnm;
            segmentid = segDatas.segmentid;
        });
        if (chartColors.length > 0) {
            sgmtChartData[i] = {};
            sgmtChartData[i]['label'] = segmentnm;
            sgmtChartData[i]['value'] = total;
            sgmtChartData[i]["fillcolor"] = chartColors[i];
            sgmtChartData[i]["segmentid"] = segmentid;
        } else {
            sgmtChartData[i] = {};
            sgmtChartData[i]['label'] = segmentnm;
            sgmtChartData[i]['value'] = total;
            sgmtChartData[i]["fillcolor"] = chartClrs[i];
            sgmtChartData[i]["segmentid"] = segmentid;
        }
        i++;
    });
    sgmtChartData = sgmtChartData.slice(0, 20);
    return sgmtChartData;
}

// Line Chart and Stacked Bar Chart Formatted Data
function LineSbarChartData(chartData, colorArr, syssgmtStatus) {
    var lineChartDataTemp = [],
        systemTempData,
        arrUniqSysnm, i = 0,
        grpdateChartdata, grpdateChartdata1, grpdateChartdata2 = {},
        orderedChartData = [];
    lchartdata = [];
    lchartdata["category"] = [];
    lchartdata["dataset"] = [];
    lchartdata["colorarr"] = [];

    lineChartDataTemp = chartData;
    if (syssgmtStatus == 'system')
        systemTempData = systemChartData(chartData);
    else
        systemTempData = segmentChartData(chartData, '');
    if (!(colorArr.length > 0)) {
        var colorPalette = _.pluck(systemTempData, 'fillcolor');
        lchartdata["colorarr"] = _.values(colorPalette);
    } else {
        lchartdata["colorarr"] = colorArr;
    }

    grpdateChartdata = _.groupBy(lineChartDataTemp, function (row) {
        return row.yearmon;
    });

    _.each(grpdateChartdata, function (lineChartDatas, labeldate) {
        lchartdata["category"][i] = {};
        lchartdata["category"][i].label = labeldate;
        i++;
    });
    if (syssgmtStatus == 'system')
        arrUniqSysnm = _.uniq(_.pluck(lineChartDataTemp, 'sysnm'));
    else
        arrUniqSysnm = _.uniq(_.pluck(lineChartDataTemp, 'segmentnm'));
    for (var j = 0; j < arrUniqSysnm.length; j++) {
        lchartdata["dataset"][j] = {};
        lchartdata["dataset"][j].seriesname = arrUniqSysnm[j];
    }

    for (var j = 0; j < arrUniqSysnm.length; j++) {
        var k = 0,
            sysid, segmentid;
        lchartdata["dataset"][j]["data"] = [];
        _.each(grpdateChartdata, function (lineChartDatas) {
            var value = 0,
                arr;
            lchartdata["dataset"][j]["data"][k] = {};
            if (syssgmtStatus == 'system') {
                arr = _.where(lineChartDatas, {
                    sysnm: lchartdata["dataset"][j].seriesname
                });
            } else {
                arr = _.where(lineChartDatas, {
                    segmentnm: lchartdata["dataset"][j].seriesname
                });
            }
            if (arr.length > 0) {
                _.each(arr, function (arrval) {
                    value += parseFloat(arrval.value);
                    if (syssgmtStatus == 'system')
                        sysid = arrval.sysid;
                    else
                        segmentid = arrval.segmentid;
                });
            }
            lchartdata["dataset"][j]["data"][k].value = value;
            k++;
        });
        if (syssgmtStatus == 'system')
            lchartdata["dataset"][j].sysid = sysid;
        else
            lchartdata["dataset"][j].segmentid = segmentid;

    }
    for (var i = 0; i < systemTempData.length; i++) {
        var index = lchartdata["dataset"].findIndex(function (item) {
            return item.seriesname == systemTempData[i]['label'];
        });
        orderedChartData[i] = lchartdata["dataset"][index];
    }
    lchartdata["dataset"] = orderedChartData;
    return lchartdata;

}

// Check which chart is active
function checkChartNm() {
    var chartName;
    $('.ul-chart-btn li').each(function (i) {
        if ($(this).hasClass('active')) {
            chartName = $(this).data('chartname');
        }
    });
    return chartName;
}

// Get Month Data by webservice
function monthData() {
    var input = {
        'token': localStorage.getItem('token'),
        'codegrp': 'MONTH'
    };
    fnGetWebServerData("search/codelookup", "gmCodeLookUpVO", input, function (monthArr) {
        var strSelectOptions = JSON.stringify(monthArr).replace(/codeid/g, 'ID').replace(/codenmalt/g, 'Name');
        var monthArr = $.parseJSON(strSelectOptions);
        fnCreateOptions($("#select-surg-surgstartmonth"), monthArr);
        fnCreateOptions($("#select-surg-surgendmonth"), monthArr);
    });
}

// Get Year Data
function yearData() {
    var start = 2000;
    var end = new Date().getFullYear() + 1;
    var options = [];
    for (var year = start; year <= end; year++) {
        options.push({
            "ID": year,
            "Name": year
        });
    }
    options.reverse();
    fnCreateOptions($("#select-surg-surgstartyear"), options);
    var startyearVal = $("#select-surg-surgstartyear").attr("startyear");
    fnCreateOptions($("#select-surg-surgendyear"), options);
    var endyearVal = $("#select-surg-surgendyear").attr("endyear");
}


//Report Data by calling webservice
function summaryReportData(input, cb) {
    console.log("summaryReportData INPUT>>>>>>>>" + JSON.stringify(input));
    fnGetWebServerData("surgeon/summaryrpt", "gmCRMSummaryReportVO", input, function (data) {
        console.log("Webservice>>surgeon/summaryrpt>>>>>" + JSON.stringify(data));
        cb(data);
    });
}

//To Organise data for the stacked bar chart
function summaryStackchartData(acslvl, chartreportData, cb) {
    var resultReportData = [],
        activeData, inprogressData, openData, totalData, seriesname;
    var i = 0;
    var cname = acslvl + "_name";
    resultReportData["category"] = [];
    resultReportData["dataset"] = [];
    seriesname = ["active", "inprogress", "open"];
    _.each(chartreportData, function (tempchartreportData) {
        resultReportData["category"][i] = {};
        resultReportData["category"][i].label = tempchartreportData[cname];
        i++;
    });
    for (i = 0; i < seriesname.length; i++) {
        resultReportData["dataset"][i] = {};
        resultReportData["dataset"][i].data = [];
        resultReportData["dataset"][i].seriesname = seriesname[i];
        var j = 0;
        _.each(chartreportData, function (tempchartreportData) {
            resultReportData["dataset"][i]["data"][j] = {};
            resultReportData["dataset"][i]["data"][j].value = tempchartreportData[seriesname[i]];
            j++;
        });
    }
    cb(resultReportData);
}

//VIEW STATCKED BAR CHART For Report
function summaryreportSBchart(chartData) {
    var that = this,
        chartDataCat = [];
    if (chartData["category"].length > 0)
        chartDataCat = [{
            "category": chartData["category"]
        }];
    var visitstackedbarChart = new FusionCharts({
        type: 'stackedcolumn2d',
        renderAt: 'summary-report-chart',
        width: '850',
        height: '400',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "showSum": "1",
                "labeldisplay": "ROTATE",
                "slantlabels": "1",
                "showLegend": "1",
                "paletteColors": chartClrs1.toString(),
                "bgcolor": "#ffffff",
                "showBorder": "0",
                "showShadow": "0",
                "showCanvasBorder": "0",
                "usePlotGradientColor": "0",
                "legendBorderAlpha": "0",
                "legendShadow": "0",
                "showAxisLines": "0",
                "showAlternateHGridColor": "0",
                "showValues": "0",
                "plotBorderAlpha": "0",
                "maxLabelHeight": "80",
                "exportEnabled": "1",
                "toolbarPosition": "TL",
            },
            "categories": chartDataCat,
            "dataset": chartData["dataset"],

        }
    }).render();
}

//To change the property name of an object from old to new
function changePropName(chartData, oPropNm, nPropNm) {
    chartData = JSON.stringify(chartData);
    chartData = chartData.split(oPropNm).join(nPropNm);
    chartData = JSON.parse(chartData);
    return chartData;
}

// VIEW PIE CHARTS
function summaryreportPChart(acslvl, chartData) {
    var that = this;
    var cname = acslvl + "_name";
    chartData = JSON.stringify(chartData);
    chartData = chartData.split(cname).join('label');
    chartData = chartData.split('total').join('value');
    chartData = JSON.parse(chartData);
    var plotToolText = "<div><b>$label, $value</b></div>";
    var visitpieChart = new FusionCharts({
        type: 'pie2d',
        renderAt: 'summary-report-chart',
        width: '850',
        height: '400',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "bgColor": "#ffffff",
                "plotToolText": plotToolText,
                "paletteColors": chartClrs.toString(),
                "showBorder": "0",
                "use3DLighting": "0",
                "showShadow": "0",
                "showLabels": "1",
                "enableSmartLabels": "1",
                "startingAngle": "0",
                "showPercentValues": "1",
                "showPercentInTooltip": "0",
                "decimals": "2",
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "showHoverEffect": "1",
                "showLegend": "0",
                "legendBgColor": "#ffffff",
                "legendBorderAlpha": '0',
                "legendShadow": '0',
                "legendItemFontSize": '10',
                "legendItemFontColor": '#666666',
                "exportEnabled": "1",
                "toolbarPosition": "TL",
            },
            "data": chartData,
        }
    }).render();
}

//Report Data by calling webservice
function summaryRevenueData(input, cb) {
    console.log("summaryRevenueData INPUT>>>>>>>>" + JSON.stringify(input));
    fnGetWebServerData("surgeon/summaryrev", "GmCRMRevenueReportVO", input, function (data) {
        console.log("Webservice>>surgeon/summaryrev>>>>>" + JSON.stringify(data));
        cb(data);
    });
}

//Check the GridData array has partyid object, if it has then it returns the index of an object else return -1
function partyExist (jsonData, partyId) {
    var that=this;
     this.retindex=-1;
     $(jsonData).each(function(index, data){ 
        if(data.partyid == partyId){
           that.retindex=index;  
        }
    });
    return that.retindex;    
}

//Summary Report DO Function
function fnSummaryDO(ordID) {
    var that = this, id;
    var userData = localStorage.getItem("userData");
    userData = JSON.parse(userData);
    var companyInfo = {};
    companyInfo.cmpid = userData.cmpid;
    companyInfo.plantid = userData.plantid;
    companyInfo.token = "";
    companyInfo.partyid = userData.partyid;
    var StringifyCompanyInfo = JSON.stringify(companyInfo);
    var toEncode = "/GmEditOrderServlet?hMode=PrintPrice&hParantForm=SALESDASHBOARD&hOrdId=" + ordID + "&companyInfo=" + StringifyCompanyInfo;
    var URL = URL_Portal + "GmLogonServlet?token=" + localStorage.getItem("token") + "&return_to=" + encodeURIComponent(toEncode) + "&randomid=" + Math.random();
    var summaryWindow = window.open(URL, '_blank', 'location=no,width=950,height=600');
    summaryWindow.addEventListener('loadstop', function () {
        summaryWindow.insertCSS({
            code: "body {font-family: 'Trebuchet MS'!important; -webkit-overflow-scrolling: touch;} a{display: none;} .RightDashBoardHeader {background:#999;} .ShadeRightTableCaption {background:#ccc;} TR.ShadeBlueBk {background:#eee}" + " table { margin:auto!important; } .button-red{ display:none; }"
        });
        summaryWindow.executeScript({
            code: "var meta = document.createElement('meta'); meta.name = 'format-detection'; meta.content = 'telephone=no'; document.getElementsByTagName('head')[0].appendChild(meta); " + that.scriptOptions
        });
    });
    summaryWindow.addEventListener('loaderror', function () {
        showNativeAlert("Error Loading Summary. Please try after sometime", "Error!", "OK", function (index) {
            summaryWindow.close();
        });
    });
}

//To Show DO List In Popup on clicking the system dataplot in the chart and shows the popup and add the class popupDOList
function showPopupDO() {
    var that = this;
    $('.div-overlay').show();
    $('.div-crm-overlay-content-container').addClass('hide');
    $('#div-crm-overlay-content-container').addClass('popupList');
    $('#div-crm-overlay-content-container').removeClass('hide');
    $("#div-crm-overlay-content-container .modal-footer").addClass("hide");
    $('#div-crm-overlay-content-container button[data-dismiss="modal"]').each(function () {
        $(this).unbind('click').bind('click', function () {
            hidePopupDO();
        });
    });
}

//On clicking the close icon to hide the popup and remove the popupDOList class from the modal popup
function hidePopupDO() {
    $('.div-overlay').hide();
    $('#div-crm-overlay-content-container').removeClass('popupList');
    $('.div-crm-overlay-content-container').addClass('hide');
}