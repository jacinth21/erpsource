/**********************************************************************************
 * File             :        gmvSurgeonDetail.js
 * Description      :        View to represent Surgeon Details
 * Version          :        1.0
 * Author           :        cskumar
 **********************************************************************************/
define([

    // External Libraries
    'jquery', 'underscore', 'backbone', 'handlebars',

    // Common
    'global', 'gmvSearch', 'gmvValidation','/GlobusCRM/common/view/GmDropDownView.js',

    // View
    'gmvCRMFileUpload', 'gmvCRMAddress', 'gmvImageCrop', 'gmvCRMSelectPopup', 'gmvItemList', 'gmvSurgeonSurgicalActivity',

    //Model
     'gmmSurgeon',

    //Collection
    'gmcItem', 'gmvSurgeonEducation',

    // Pre-Compiled Template
    'gmtTemplate'
],

    function ($, _, Backbone, Handlebars,
        Global, GMVSearch, GMVValidation,DropDownView,
        GMVCRMFileUpload, GMVCRMAddress, GMVImageCrop, GMVCRMSelectPopup, GMVItemList, GMVSurgeonSurgicalActivity,
        GMMSurgeon,
        GMCItem, GMVSurgeonEducation, GMTTemplate) {

        'use strict';

        GM.View.SurgeonDetail = Backbone.View.extend({

            el: "#div-crm-module",

            name: "CRM Surgeon Detail View",

            container: "#div-crm-overlay-content-container",

            /* Load the templates */
            template_SurgeonMedia: fnGetTemplate(URL_Surgeon_Template, "gmtSurgeonMediaPopup"),
            template_MainModule: fnGetTemplate(URL_Common_Template, "gmtCRMMainModule"),
            template_SurgeonAddress: fnGetTemplate(URL_Common_Template, "gmtAddress"),
            template_address_list: fnGetTemplate(URL_Common_Template, "gmtAddressList"),
            template_user_quick_info: fnGetTemplate(URL_Common_Template, "gmtUserQuickInfo"),
            template_surg_merc: fnGetTemplate(URL_Surgeon_Template, "gmtMERCListPopup"),
            //            template_phone_surg_main: fnGetTemplate(URL_Surgeon_Template, "gmtPhoneSurgeonMain"),
            template_SurgeonEdit: fnGetTemplate(URL_Surgeon_Template, "gmtSurgeonEdit"),
            template_SurgeonContact: fnGetTemplate(URL_Surgeon_Template, "gmtSurgeonContact"),
            //            template_SurgeonHospital: fnGetTemplate(URL_Surgeon_Template, "gmtSurgeonHospital"),
            //            template_SurgeonHospital_List: fnGetTemplate(URL_Surgeon_Template, "gmtSurgeonHospitalList"),
            template_SurgeonDocument: fnGetTemplate(URL_Surgeon_Template, "gmtSurgeonDocument"),
            template_SurgeonDocument_List: fnGetTemplate(URL_Surgeon_Template, "gmtSurgeonDocumentList"),
            template_picture: fnGetTemplate(URL_Surgeon_Template, "gmtSurgeonProfilePic"),
            //            template_keyword: fnGetTemplate(URL_Surgeon_Template, "gmtSurgeonKeywords"),
            template_hospDetail: fnGetTemplate(URL_Surgeon_Template, "gmtSurgeonHospDetail"),
            //            template_surgicalDetail: fnGetTemplate(URL_Surgeon_Template, "gmtSurgeonSurgicalDetail"),
            template_globalDetail: fnGetTemplate(URL_Surgeon_Template, "gmtSurgeonGlobActDetail"),
            template_fileDetail: fnGetTemplate(URL_Surgeon_Template, "gmtSurgeonDocDetail"),
            template_SurgeonDetail: fnGetTemplate(URL_Surgeon_Template, "gmtSurgeonDetail"),
            template_SurgAddressList: fnGetTemplate(URL_Surgeon_Template, "gmtSurgAddressList"),
            template_SurgEducation: fnGetTemplate(URL_Surgeon_Template, "gmtSurgeonEducation"),
            template_AddRep: fnGetTemplate(URL_Surgeon_Template, "gmtSurgeonRepList"),
            template_SurgRptList: fnGetTemplate(URL_Surgeon_Template, "gmtSurgeonReportList"),


            events: {
                //                "click #div-surg-address": "showAddress",
                "click #li-edit,#btn-phone-main-edit": "editSurgeon",
                "click #li-cancel": "resetSurgeon",
                "click #li-save": "saveSurgeon",
                "click #div-surg-add-merc": "listUpcomingMERC",
                "click #div-surg-list-physicainvisit": "listSurgPhyVisit",
                "click #div-surg-list-salescall": "listSurgSalesCall",
                "click #div-surg-list-merc": "createSurgMerc",
                //                "click #spn-surg-view-comments, #spn-affl-view-comments": "initialSlide",
                "click #spn-surgeon-detail-rep, #spn-surgeon-detail-ad, #mob-rep-quick-info, #mob-ad-quick-info": "openRepQuickInfo",
                "click #btn-quick-info-close": "closeRepQuickInfo",
                "click #li-sur-detail-contact": "toggleInfo",
                "click #li-sur-detail-hos-affiliation": "toggleAffiliation",
                "click #li-sur-detail-education": "viewEducation",
                "click #li-sur-detail-surgical-actvty": "toogleSurgicalActivityInfo",
                "click #li-sur-detail-caption-document": "toogleDocumentInfo",
                "click #ul-sur-det-globusactvty": "toogleGlobusActivity",
                "click .li-sglobus-acttitle": "linkGlobus",
                //                "click .li-surg-surgical-comment-slide-down": "slideSurgicalComments",
                //                "click .li-surg-hospital-comment-slide-down": "slideHospitalComments",
                //                "click #div-crmitemlist-coldata .li-ph-surg-hospital-data": "toggleHospitalData",
                //                "click #div-crmitemlist-coldata .li-ph-surg-surgical-data": "toggleSurgicalData",
                "click .btn-surg-tabs, .btn-surg-phone-tabs": "loadSelectedTab",
                'change input, textarea': 'updateModel',
                "click #li-sur-pic-container, #ul-pic-edit-btn, #li-pic-edit-btn  ": "openPicturePopup",
                //                "click   .div-surg-affl-cmnts, .div-surg-surgical-cmnts, .div-surg-surgical-procedure": "openPopup",
                //                "click #div-surg-affl-add-accnt": "newAccnt",
                //                "click .i-sur-surg-acc-remove": "deleteSurgRecord",
                //                "click .i-sur-affl-acc-remove": "deleteHospRecord",
                //                "change .select-saffil-level": "checkLvl",
                "change #select-surgeon-salute": "partySalute",
                "click .i-sur-doc-remove": "deleteForm",
                //                "click .li-surg-phn-tabs": "togglePhoneDisplay",
                //                "click .li-phone-acc-name-list-item": "showAccDetail",
                //                "click .btn-surgeon-hosp-done": "addAccount",
                //                "click #div-add-surg-surgicalact": "showSystemBtn",
                //                "click #ul-add-hosp-affiliation": "showAccBtn",
                "blur #input-scontacts-npid": "checkNpid",
                "keypress #input-scontacts-npid, .sur-edit-hosaffl-cases": "checkNumber",
                "keypress .input-surg-phone": "checkPhoneNumber",
                //                "keypress #checknumcase": "checkcasedigit",
                "click #btn-main-void": "confirmVoid",
                "click .a-file-open, .gmWebLink span": "viewDocument",
                "click .link-docs": "openDoc",
                //                "blur #surgeon-assistant-content input,#surgeon-manager-content input": "validateAM",
                "click #div-surg-tabs li": "toogleTabs",
                "click #surgeon-add-new": "addMoreContacts",
                "click #ul-contact-add-more .fa-minus-circle,.remove-contact": "removeContact",
                "click #surgeon-edit-address, #surgeon-add-new-address": "editAddress",
                "click .remove-address": "removeAddress",
                "change .surg-phone-cbox, .surg-email-cbox": "valPrimary",
                "click .surg-phone-contype": "contactPopUps",
                "click .li-load-report": "loadSurgeonReport",
                "click #surgeon-add-new-rep": "addrep",
                "click .remove-rep": "removeRep",
                "change .surg-rep-cbox": "setPrimary",
                "click .socialMedia": "socialMediaEdit",
                "click .btn-filter": "filterActivity"


            },

            initialize: function (options) {
                this.partyid = options.partyid;
                this.checkUrlDone = true;
                $(window).on('orientationchange', this.onOrientationChange);
                this.userTabs = _.pluck($.parseJSON(localStorage.getItem('userTabs')), "functionID");
                $("#div-app-title").show();
                $("#span-app-title").html("Surgeon");
                //            $("#txt-mobile-title").html("Surgeon");
                GM.Global.Filetitle = undefined;
                if (this.partyid != 0) {
                    if (options.data.gmCRMSurgeonlistVO != undefined)
                        var data = options.data.gmCRMSurgeonlistVO[0];
                    else
                        var data = $.parseJSON(JSON.stringify(options.data));
                } else
                    var data = null;

                this.model = new GMMSurgeon(data);
                if (GM.Global.Surgeon == undefined) //Handling hard-reload
                    GM.Global.Surgeon = {
                        RepID: window.localStorage.getItem("userID")
                    };
                //                GM.Global.Surgeon.keyComp = [];
                if (this.partyid == 0)
                    this.model.set("picurl", "");
                if (this.partyid != 0)
                    this.surgACSFL = data.surgacsfl;

                GM.Global.Surgeon.Data = this.model.attributes;
                this.fileUploadViews = {};
                //                this.popupHComments = [];
                //                this.popupState = [];
                //                this.popupSComments = [];
                //                this.popupProcedure = [];
                //                this.popupProcedureNM = [];
                //                this.hospNm = [];
                //                this.delHosp = false;
                //                this.loopExit = true;
                this.void = "";

            },

            render: function () {
                var that = this;
                var modInput;
                if (this.partyid != 0) {
                    var server;
                    server = (this.model.get("picurl") == "") ? "" : URL_CRMFileServer;
                    modInput = {
                        "module": "surgeon",
                        "server": server,
                        "picurl": this.model.get("picurl") + "?num=" + Math.random(),
                        "firstnm": this.model.get("firstnm"),
                        "lastnm": this.model.get("lastnm")
                    };
                    this.model.set("picurl", (this.model.get("picurl") == "") ? "" : (this.model.get("picurl") + "?num=" + Math.random()));
                } else
                    modInput = {
                        "module": "surgeon"
                    };
                this.$el.html(this.template_MainModule(modInput));
                that.$(".gmPanelTitle").html("");
                $(".li-crm-module-phone-pic").removeClass("hide");

                if (this.partyid != 0) {

                    var name = parseNull(GM.Global.Surgeon.Data.salutenm) + " " + GM.Global.Surgeon.Data.firstnm + " " + parseNull(GM.Global.Surgeon.Data.midinitial) + " " + GM.Global.Surgeon.Data.lastnm;
                    that.$(".gmPanelTitle").html(name);

                    this.model.set("server", URL_CRMFileServer);

                    this.$("#div-main-content").empty();

                    this.$("#div-main-content").html(this.template_SurgeonDetail(this.model.toJSON()));

                    _.each(that.model.get("arrgmcrmsurgeonsocialvo"), function (data, i) {
                        $(".vitals").find("span").attr("href-url", data.vitals);
                        $(".openpayment").find("span").attr("href-url", data.openpayment);
                        $(".website").find("span").attr("href-url", data.website);
                        $(".google").find("span").attr("href-url", data.google);
                        $(".facebook").find("span").attr("href-url", data.facebook);
                        $(".twitter").find("span").attr("href-url", data.twitter);
                        $(".linkedin").find("span").attr("href-url", data.linkedin);
                    });

//                    var count = $(".socialIcons-details li").length;
//                    var elem = document.querySelector('.socialIcons-details');
//                    elem.style.margin = 'auto';
//                    if (count == 5 || count == 6) {
//                        elem.style.width = '190px';
//                    }
//                    if (count == 8) {
//                        elem.style.width = '85%';
//                    }

                    _.each(this.model.get("arrgmcrmfileuploadvo"), function (dt) {
                        if (dt.reftype == "105220")
                            $("#surgeon-cv-link").find("span").attr("href-url", URL_CRMFileServer + dt.refgroup + "/" + dt.refid + "/" + dt.filename);
                    });
                    if (that.surgACSFL == "N")
                        that.$("#li-sur-detail-surgical-actvty").hide();

                    if (!_.contains(that.userTabs, 'CRM-PHYSICIANVISIT'))
                        that.$("#div-surg-list-physicainvisit").parent().hide();

                    if (!_.contains(that.userTabs, 'CRM-SALESCALL'))
                        that.$("#div-surg-list-salescall").parent().hide();

                    if (_.contains(GM.Global.Admin, 'physvisitrqst') || _.contains(GM.Global.Admin, 'physicianvisit')) {
                        $(".div-surg-list-physicainvisit").removeClass("hide");
                    }
                    if (_.contains(GM.Global.Admin, 'salescall'))
                        $(".div-surg-list-salescall").removeClass("hide");

                    if (GM.Global.SwitchUser)
                        $("#div-surg-add-merc").addClass("hide");

                    var editAccess = _.findWhere(GM.Global.UserAccess, {
                        funcid: "CRM-SURGEON-EDIT"
                    });

                    if (editAccess != undefined) {
                        $("#li-edit").removeClass("hide");
                    } else {
                        $("#li-edit").addClass("hide");
                        $("#li-create").addClass("hide");
                    }

                    this.$("#sur-detail-education-content").html(this.template_SurgEducation(this.model.toJSON()));

                    var surgConsultantFl = _.findWhere(GM.Global.UserAccess, {
                        funcid: "CRM-SURGEON-CONSULT"
                    });
                    if (surgConsultantFl != undefined)
                        $("#surgeon-consultant-view").removeClass("hide");
                    else
                        $("#surgeon-consultant-view").addClass("hide");


                    // Below code is used for Surgical Activity account list dropdown
                    var surgeonaffiliation = [];
                    if (that.model.get("arrgmcrmsurgeonaffiliationvo").length)
                        surgeonaffiliation = arrayOf(that.model.get("arrgmcrmsurgeonaffiliationvo"));
                    else
                        that.model.set("arrgmcrmsurgeonaffiliationvo", new Array());
                    this.hospitalList = [];
                    _.each(that.model.get("arrgmcrmsurgeonaffiliationvo"), function (data, i) {
                        //                        that.popupHComments.push(data.comments);
                        //                        that.popupState.push(data.accstateid);
                        that.hospitalList.push({
                            accid: data.accid,
                            accnm: data.accnm
                        });
                    });
                    GM.Global.hospitalList = this.hospitalList;
                    console.log("surgeonaffiliation>>>>>>>>>>>>" + JSON.stringify(surgeonaffiliation));
                    //             Below condition used for surgeonaffiliation show in dhtml
                    if (surgeonaffiliation != "") {
                        var rows = [];
                        var i = 0;
                        _.each(surgeonaffiliation, function (griddt) {
                            rows[i] = {};
                            rows[i].id = i;
                            rows[i].data = [];
                            if(griddt.accessflag == 'Y'){
                            rows[i].data[0] = "<a href='javascript:;' title='"+ griddt.accid +"' data-accid='"+ griddt.accid +"' data-accnm='"+ griddt.parentacctnm +"' data-href='#crm/surgeon/account/id/"+griddt.accid+"' class='accSurgItems'><i class='fa fa-user-md fa-2x gmFontGreen'></i></a><a href='javascript:;'  data-accid='"+ griddt.accid +"' data-accnm='"+ griddt.parentacctnm +"' data-href='#crm/surgeon/account/surgactivity/id/"+griddt.accid+"' title='"+ griddt.accid +"' style='margin-left:8px;' class='accSurgItems'><i class='fa fa-heartbeat fa-2x gmFontphysicianvisit'></i></a>";
                            }else {
                                rows[i].data[0] = "<a href='javascript:;' title='"+ griddt.accid +"' data-accid='"+ griddt.accid +"' data-accnm='"+ griddt.parentacctnm +"' data-href='#crm/surgeon/account/id/"+griddt.accid+"' class='accSurgItems'><i class='fa fa-user-md fa-2x gmFontGreen'></i></a>";
                            }
                            rows[i].data[1] = griddt.parentacctnm;
                            rows[i].data[2] = griddt.acccity;
                            rows[i].data[3] = griddt.accstate;
                            i++;
                        });
                        var dataSurgeonAffil = {};
                        dataSurgeonAffil.rows = rows;
                        console.log("surgeonaffiliation>>>>>>>>>>>>" + JSON.stringify(dataSurgeonAffil));
                        var headerArr = ["","Hospital Name", "City", "State"];
                        var widthStr = "100,*,200,300";
                        var alignStr = "center,left,left,left";
                        var typeStr = "ro,ro,ro,ro";
                        var sortStr = "str,str,str,str";
                        var tooltipStr = "true,true,true,true";
                        var filterStr = ",#text_filter,#text_filter,#text_filter";
                        var footerExportFL = true;

                        that.hosGridObj = loadDHTMLXGrid('sur-detail-hos-affiliation', "", dataSurgeonAffil, headerArr, widthStr, alignStr, typeStr, sortStr, tooltipStr, filterStr, [], [], footerExportFL);
                        if (footerExportFL) {
                            var deleteIndexArr = []; //include the index of the column in the array if u want to export                 
                            $("#sur-detail-hos-affiliation-export").unbind("click").bind("click", function (event) {
                                exportExcel(event, that.hosGridObj, deleteIndexArr);
                            });
                        }
                        
                        $(".accSurgItems").on("click", function () {
                            var colAccid = $(this).data("accid");
                            GM.Global.Surgeon.accnm = $(this).data("accnm");
                            window.location.href = $(this).data("href");
                        });

                    } else {
                        $("#sur-detail-hos-affiliation").html("No Data Found");
                    }



                    that.$("#spn-affl-count").html(surgeonaffiliation.length);
                    that.toggleInfo({
                        currentTarget: $("#li-sur-detail-contact")
                    });
                    var controlID = that.$("#li-surg-phone-hospital");
                    var data = surgeonaffiliation;
                    if (data.length) {
                        _.each(data, function (data) {
                            data.title = data.accnm; // for sorting field
                        });
                        var itemtemplate = "GmPhoneSurgeonAffiliation";
                        var columnheader = 1;
                        var template_URL = URL_Surgeon_Template;
                        var filterOptions = ["accnm"];
                        var color = "";
                        var csvFileName = "" + $(".gmTitle").text().trim() + " Hospital Affiliation.csv";
                        var sortoptions = [{
                            "DefaultSort": "accnm"
                            }];
                        that.showItemListView(controlID, data, itemtemplate, columnheader, sortoptions, template_URL, filterOptions, csvFileName, color);
                        $(".li-filter-close").removeClass("hide");
                    } else
                        controlID.html("No Data Found").addClass("gmTextCenter");

                    var pagination, totalsize;
                    that.pagination = 0;
                    that.totalsize = 0;


                    var input = {
                        "token": localStorage.getItem("token"),
                        "partyid": that.partyid,
                        "pagination": that.pagination
                    };
                    fnGetWebServerData("surgeon/activityinfo/", "gmCRMSurgeonActivityVO", input, function (data) {

                        if (data != "") {
                            that.totalsize = data[0]["totalsize"];
                            $("#spn-globact-count").html(that.totalsize);


                            _.each(data, function (metdata) {
                                var todayDate = new Date();
                                var dd = ("0" + todayDate.getDate()).slice(-2),
                                    mm = ("0" + (todayDate.getMonth() + 1)).slice(-2),
                                    yyyy = todayDate.getFullYear();
                                var tdayDate = yyyy + "-" + mm + "-" + dd;

                                var diff = Date.parse(metdata.activitydate) - Date.parse(tdayDate);

                                if (diff < 0) {
                                    metdata.datefl = 0;
                                } else if (diff == 0) {
                                    metdata.datefl = 2;
                                } else {
                                    metdata.datefl = 1;
                                }
                                var typ = metdata.activitytype;
                                metdata.alink = typ.toLowerCase().replace(" ", "");
                                metdata.deptid = localStorage.getItem("deptid");
                                if ((metdata.activitytypeid == '105265') || (metdata.activitytypeid == '105266') || (metdata.activitytypeid == '105273')) {
                                    if (metdata.activitytodate != "") {
                                        var diff1 = Date.parse(metdata.activitytodate) - Date.parse(metdata.activitydate);

                                        if (diff1 == 0) {
                                            metdata.activitytodate = "";
                                            console.log("diff1>>>>" + diff1 + ">>metdata.activitytodate>>" + metdata.activitytodate);
                                        }

                                    }
                                } else {
                                    metdata.activitytodate = "";
                                }


                            });
                            console.log("ACTIVITY DATA>>>>>" + JSON.stringify(data));
                            that.$("#sur-det-globusactvty").html(that.template_globalDetail({
                                surgeonactivity: data
                            }));
                            $("#sur-det-globusactvty .surgGlobusActivity .surgActivityList .surg-globus-activty").css("display", "block");
                            $("#sur-det-globusactvty .surgGlobusActivity:last-child .surgActivityList:last-child .surg-globus-activty").css("display", "none");
                        } else {
                            $("#spn-globact-count").html(that.totalsize);
                            that.$("#sur-det-globusactvty").html(that.template_globalDetail({
                                surgeonactivity: ""
                            }));
                        }
                    });

                    $("#sur-det-globusactvty").on("scroll", function () {
                        var scrollPercentage = 100 * this.scrollTop / (this.scrollHeight - this.clientHeight);
                        if ((scrollPercentage >= 100) && ($('#sur-det-globusactvty ul ul li').length < that.totalsize)) {
                            that.pagination = that.pagination + 1;
                            var input = {
                                "token": localStorage.getItem("token"),
                                "partyid": that.partyid,
                                "pagination": that.pagination
                            };


                            fnGetWebServerData("surgeon/activityinfo/", "gmCRMSurgeonActivityVO", input, function (data) {

                                if (data != null) {
                                    _.each(data, function (metdata) {
                                        var todayDate = new Date();
                                        var dd = ("0" + todayDate.getDate()).slice(-2),
                                            mm = ("0" + (todayDate.getMonth() + 1)).slice(-2),
                                            yyyy = todayDate.getFullYear();
                                        var tdayDate = yyyy + "-" + mm + "-" + dd;

                                        var diff = Date.parse(metdata.activitydate) - Date.parse(tdayDate);

                                        if (diff < 0) {
                                            metdata.datefl = 0;
                                        } else if (diff == 0) {
                                            metdata.datefl = 2;
                                        } else {
                                            metdata.datefl = 1;
                                        }
                                        var typ = metdata.activitytype;
                                        metdata.alink = typ.toLowerCase().replace(" ", "");
                                        metdata.deptid = localStorage.getItem("deptid");
                                        if ((metdata.activitytypeid == '105265') || (metdata.activitytypeid == '105266') || (metdata.activitytypeid == '105273')) {
                                            if (metdata.activitytodate != "") {
                                                var diff1 = Date.parse(metdata.activitytodate) - Date.parse(metdata.activitydate);
                                                if (diff1 == 0) {
                                                    metdata.activitytodate = "";
                                                    console.log("diff1>>>>" + diff1 + ">>metdata.activitytodate>>" + metdata.activitytodate);
                                                }
                                            }
                                        } else {
                                            metdata.activitytodate = "";
                                        }


                                    });
                                    console.log("ACTIVITY DATA>>>>>" + JSON.stringify(data));
                                    that.$("#sur-det-globusactvty").append(that.template_globalDetail({
                                        surgeonactivity: data
                                    }));
                                    $("#sur-det-globusactvty .surgGlobusActivity .surgActivityList .surg-globus-activty").css("display", "block");
                                    $("#sur-det-globusactvty .surgGlobusActivity:last-child .surgActivityList:last-child .surg-globus-activty").css("display", "none");
                                }
                            });

                        }

                    });

                    var input = {
                        "token": GM.Global.Token,
                        "refid": this.partyid
                    };

                    var fileupload = [];
                    if (that.model.get("arrgmcrmfileuploadvo").length)
                        fileupload = arrayOf(that.model.get("arrgmcrmfileuploadvo"));
                    else
                        that.model.set("arrgmcrmfileuploadvo", new Array());

                    _.each(fileupload, function (data) {
                        data.server = URL_CRMFileServer;
                    })
                    that.$("#sur-detail-document").html(that.template_fileDetail({
                        fileupload: fileupload
                    }));
                    that.$("#spn-doc-count").html(fileupload.length);
                    that.toggleInfo({
                        currentTarget: $("#li-sur-detail-contact")
                    });
                    var controlID = that.$("#li-surg-phone-documents");
                    var fileDisp = fileupload;
                    if (fileDisp != "") {
                        var data = fileDisp;
                        var itemtemplate = "GmPhoneSurgeonDocument";
                        var columnheader = 1;
                        var template_URL = URL_Surgeon_Template;
                        var sortoptions = [{
                            "DefaultSort": ""
                            }];
                        var filterOptions = [];
                        var color = "";
                        var csvFileName = "" + $(".gmTitle").text().trim() + " Surgeon Documents.csv";

                        that.showItemListView(controlID, data, itemtemplate, columnheader, sortoptions, template_URL, filterOptions, csvFileName, color);
                    } else
                        controlID.html("No Data Found").addClass("gmTextCenter");

                } else {
                    that.$(".gmPanelTitle").html("Create a new Surgeon");
                    that.$("#div-surgeon-phone-title").html("Create a new Surgeon");
                    this.editSurgeon();
                }

                resizeHeadWidth('.gmPanelHead', '.gmBtnGroup', '#panelhead-left');

                //If save is clicked then it triggers an appropriate tab
                if (GM.Global.Surgeon.Edit == true) {
                    GM.Global.Surgeon.Edit = false;
                    $('li.surg-tab[name="' + GM.Global.CurrentTab + '"]').trigger("click");
                } else {
                    $('li.surg-tab[name="' + GM.Global.CurrentTab + '"]').trigger("click");
                }

                $(".gmWebLink").each(function (index, list) {
                    var elem = $(list).children('span').attr('href-url');
                    if (elem == "") {
                        $(list).children('span').css('opacity', '0.5');
                    }
                });

                //Initialze View to validate all surgeon details that require validation here            
            },
            // Function for set the input social media url value in the model
            socialMediaEdit: function (e) {
                var that = this;
                showPopup();
                $(".done-model").removeClass("hide");
                $(".clear-model").removeClass("hide");
                $(".load-model").addClass("hide");
                var arrays = that.model.get("arrgmcrmsurgeonsocialvo");
                if (arrays.length > 0)
                    arrays = arrays[0];
                $("#div-crm-overlay-content-container .modal-title").html("Change Websites");

                $("#div-crm-overlay-content-container .modal-body").html(this.template_SurgeonMedia(arrays));


                $("#div-crm-overlay-content-container .modal-footer .done-model").unbind("click").bind("click", function (event) {

                    that.checkUrlDone = true;
                    $("#div-Media .socialIcons-edit").each(function () {
                        var idval = $(this).find("input").val()
                        var checkurl = UrlValid(idval);
                        console.log(checkurl);
                        if (checkurl == false && idval != '') {
                            $(this).addClass("gmValidationError");
                            that.checkUrlDone = false;
                        } else {
                            $(this).removeClass("gmValidationError");
                        }

                    });
                    console.log(that.checkUrlDone);

                    if (that.checkUrlDone != false) {
                        var input = {};
                        input.partyid = GM.Global.Surgeon.Data.partyid;
                        input.facebook = $("#Facebook").val();
                        input.twitter = $("#Twitter").val();
                        input.google = $("#Google").val();
                        input.linkedin = $("#Linkedin").val();
                        input.vitals = $("#Vitals").val();
                        input.website = $("#Website").val();
                        input.openpayment = $("#Openpayment").val();
                        var arr = [];
                        arr[0] = input;
                        arr[0].partyid = GM.Global.Surgeon.Data.partyid;
                        that.model.set("arrgmcrmsurgeonsocialvo", arr);
                        console.log(that.model.get("arrgmcrmsurgeonsocialvo"));
                        hidePopup();
                    } else {
                        $("#socialUrl-validation-msg-bar").html("Please enter Valid Url").css({
                            "background-color": "#700",
                            "padding": "10px",
                            "color": "#fff"
                        }).slideDown(500).fadeOut(10000);
                    }
                });

                $(".clear-x").on("click", function () {
                    $(this).siblings("li").children("input:text").val('');
                });
                $(".clear-model").unbind("click").bind("click", function () {
                    $(this).parent().parent().find("input:text").val('');
                });

            },

            listUpcomingMERC: function () {

                var that = this;
                showPopup();
                $("#div-crm-overlay-content-container .modal-body").html(this.template_surg_merc());
                var name = parseNull(GM.Global.Surgeon.Data.salutenm) + " " + GM.Global.Surgeon.Data.firstnm + " " + parseNull(GM.Global.Surgeon.Data.midinitial) + " " + GM.Global.Surgeon.Data.lastnm;
                $(".modal-title").text("Add '" + name + "' to MERC");
                $(".modal-footer").addClass("hide");

                var voname = "gmCRMActivityVO";
                var sortoptions = [{
                    "DefaultSort": "actstartdate",
                    "Elements": [{
                            "ID": "actnm",
                            "DispName": "Title",
                            "ClassName": "gmDashColHead gmFontmerc gmText150"
                        },
                        {
                            "ID": "actstartdate",
                            "DispName": " From Date",
                            "ClassName": "gmDashColHead gmFontmerc gmText125"
                        },
                        {
                            "ID": "actenddate",
                            "DispName": " To Date",
                            "ClassName": "gmDashColHead gmFontmerc gmText100"
                        },
                        {
                            "ID": "location",
                            "DispName": "Location",
                            "ClassName": "gmDashColHead gmFontmerc gmText125"
                        }]
                }];
                var filterOptions = ["actnm", "actstartdate", "location"];
                var itemtemplate = "gmtSurgeonMERCList";
                var columnheader = 6;
                var template_URL = URL_Surgeon_Template;
                var controlID = "#div-crm-overlay-content-container #div-popup-merc-list";
                var url = "crmactivity/info";
                that.search = ["actnm", "actstartdate", "actenddate", "location"]
                var color = "gmFontRed";
                var today = new Date();
                var input = {
                    "token": GM.Global.Token,
                    "actcategory": "105265",
                    "actstartdate": (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear()
                };

                fnGetWebServerData(url, voname, input, function (data) {
                    if (data != null && data != undefined) {
                        data = data.GmCRMListVO.gmCRMActivityVO;
                        data = arrayOf(data);
                        _.each(data, function (data) {
                            data.actstartdate = getDateByFormat(data.actstartdate);
                            data.actenddate = getDateByFormat(data.actenddate);
                            if (data.partyid != "" && data.partyid.indexOf(that.partyid) >= 0)
                                data["added"] = "Y";
                            if (data.attrpartyid != "" && data.attrpartyid.indexOf(that.partyid) >= 0)
                                data["reqstd"] = "Y";
                        });
                        that.showItemListView(controlID, data, itemtemplate, columnheader, sortoptions, template_URL, filterOptions, undefined, color);
                        $("#div-crm-overlay-content-container .modal-body .li-add-merc").bind("click", function (event) {
                            that.addParty(event);
                        });
                    } else
                        $(controlID).text("No Data Found");
                });
            },

            //to add a party to the merc popup
            addParty: function (event) {
                var that = this;
                var surgeonName = parseNull(GM.Global.Surgeon.Data.salutenm) + " " + GM.Global.Surgeon.Data.firstnm + " " + parseNull(GM.Global.Surgeon.Data.midinitial) + " " + GM.Global.Surgeon.Data.lastnm;
                var surgeonID = GM.Global.Surgeon.Data.partyid;

                var input = {
                    "attrid": "",
                    "attrval": surgeonID,
                    "attrnm": surgeonName,
                    "actid": $(event.currentTarget).attr("actid"),
                    "attrtype": "105589",
                    "voidfl": ""
                };
                hidePopup();
                fnGetWebServerData("crmactivity/addattr", "gmCRMActivityAttrVO", input, function (data) {
                    if (data != null)
                        showSuccess(surgeonName + " requested for " + $(event.currentTarget).attr("actnm"), 5000);
                    else
                        showError("Request Failed");
                }, true);
            },

            showItemListView: function (controlID, data, itemtemplate, columnheader, sortoptions, template_URL, filterOptions, csvFileName, color, callbackOnRender) {
                data = arrayOf(data)
                for (var i = 0; i < data.length; i++) { // This Code is put back to avoid writing it's web service for all lists
                    var row = data[i];
                    data[i].search = "";
                    if (this.search && this.search.length) {
                        _.each(this.search, function (srh) {
                            data[i].search += row[srh] + " ";
                        })
                    }
                }
                var items = new GMCItem(data);
                this.itemlistview = new GMVItemList({
                    el: controlID,
                    collection: items,
                    columnheader: columnheader,
                    itemtemplate: itemtemplate,
                    sortoptions: sortoptions,
                    template_URL: template_URL,
                    filterOptions: filterOptions,
                    csvFileName: csvFileName,
                    color: color,
                    callbackOnReRender: callbackOnRender
                });


            },
            toogleTabs: function (event) {
                var name = $(event.currentTarget).attr("name");
                GM.Global.CurrentTab = name;

            },
            //to create a new salescall for the surgeon
            listSurgSalesCall: function (event) {
                if (this.model.get("salescallAct") != 0) {
                    var input = {
                        "surgpartyid": this.partyid,
                        "actcategory": "105267"
                    };
                    GM.Global["SurgeonActivity"] = JSON.stringify(input);
                    window.location.href = "#crm/salescall/list/surgeon";
                }

            },

            //to create a new merc for the surgeon
            createSurgMerc: function () {
                if (this.model.get("mercAct") != 0) {
                    var input = {
                        "surgpartyid": this.partyid,
                        "actcategory": "105265"
                    };
                    GM.Global["SurgeonActivity"] = JSON.stringify(input);
                    //                window.localStorage.setItem('merc', JSON.stringify(input));
                    window.location.href = "#crm/merc/list/surgeon";
                }
            },

            //to create a new physician visit for the surgeon
            listSurgPhyVisit: function (event) {

                if (this.model.get("phyAct") != 0) {
                    var input = {
                        "surgpartyid": this.partyid,
                        "actcategory": "105266"
                    };
                    GM.Global["SurgeonActivity"] = JSON.stringify(input);
                    window.location.href = "#crm/physicianvisit/list/surgeon";
                }
            },

            //to handle sliding of detail infos
            toggleInfo: function (e) {
                colorControl(e, "surg-tab", "gmBGLGrey", "gmBGDarkerGrey gmFontWhite");
                $(".surgeon-view-content").addClass("hide");
                $("#sur-detail-contact").removeClass("hide");
            },

            //to handle sliding of detail infos
            toggleAffiliation: function (e) {
                colorControl(e, "surg-tab", "gmBGLGrey", "gmBGDarkerGrey gmFontWhite");

                $(".li-surg-hospital-comment-slide-up").each(function () {
                    $(this).removeClass("slide");
                });
                $(".li-surg-hospital-comment-slide-down").each(function () {
                    $(this).find('i').removeClass("fa-chevron-up").addClass("fa-chevron-down");
                });
                $(".li-surg-hospital-comment-slide-down ").show();
                $(".li-surg-hospital-comment-slide-up ").hide();
                $(".surgeon-view-content").addClass("hide");
                $("#sur-detail-hos-affiliation").removeClass("hide");
            },

            //to handle sliding of detail infos
            viewEducation: function (e) {
                colorControl(e, "surg-tab", "gmBGLGrey", "gmBGDarkerGrey gmFontWhite");
                $(".surgeon-view-content").addClass("hide");
                $("#sur-detail-education").removeClass("hide");

            },
            //to slide Surgical Activity info in detail mode
            toogleSurgicalActivityInfo: function (e) {
                colorControl(e, "surg-tab", "gmBGLGrey", "gmBGDarkerGrey gmFontWhite");
                $(".li-surg-surgical-comment-slide-up").each(function () {
                    $(this).removeClass("slide");
                });
                $(".li-surg-surgical-comment-slide-down").each(function () {
                    $(this).find('i').removeClass("fa-chevron-up").addClass("fa-chevron-down");
                });

                $(".li-surg-surgical-comment-slide-down ").show();
                $(".li-surg-surgical-comment-slide-up").hide();
                //                $("#sur-detail-hos-affiliation").addClass("hide");
                $(".surgeon-view-content").addClass("hide");
                $("#sur-detail-surgical-actvty").removeClass("hide");
                this.closeView(this.gmvsurgeonsurgicalactivity);
                this.gmvsurgeonsurgicalactivity = new GMVSurgeonSurgicalActivity();
            },

            //to slide Documents info in detail mode
            toogleDocumentInfo: function (e) {
                colorControl(e, "surg-tab", "gmBGLGrey", "gmBGDarkerGrey gmFontWhite");
                $(".surgeon-view-content").addClass("hide");
                $("#sur-detail-document").removeClass("hide");
            },

            //to slide Globus Activity info in detail mode
            toogleGlobusActivity: function (e) {
                colorControl(e, "surg-tab", "gmBGLGrey", "gmBGDarkerGrey gmFontWhite");
                $(".surgeon-view-content").addClass("hide");
                $("#sur-det-globusactvty").removeClass("hide");
                //hide Globus Activity filter button hide
//                $(".btn-filter").each(function () {
//                    var btnname = $(this).attr("name");
//                     if(btnname!= "all" && !$("."+btnname+"-filter").is(":visible")){
//                 	   $(this).addClass("hide");
//                    }
//                 	   
//                 });
                
            },
            //to link from surgeon profile to the respective globus activities
            linkGlobus: function (e) {
                var tabNames = ["dashboard", "lead", "salescall", "training", "surgeon", "physicianvisit", "merc"];
                _.each(tabNames, function (data) {
                    $("." + data).removeClass("" + data);
                });
                var val = (".li-sglobus-acttitle").index($(e.currentTarget));
                if (this.globusData[val].activitytype == "MERC") {
                    $("#btn-crm-merc").addClass("merc");
                    window.location.href = "#merc/id/" + this.globusData[val].activityid;
                } else if (this.globusData[val].activitytype == "Physician Visit") {
                    this.globusData[val]["type"] = "physicianvisit";
                    $("#btn-crm-physicianvisit").addClass("physicianvisit");
                    window.location.href = "#physicianvisit/id/" + this.globusData[val].activityid;
                }
            },

            //to handle if not all items are having same slide
            //            initialSlide: function (e) {
            //                if ($(e.currentTarget).attr("id") == "spn-surg-view-comments") { //if it's about Surgical Activity
            //                    var s_up = "",
            //                        s_down = "";
            //                    $(".li-surg-surgical-comment-slide-up").each(function () {
            //                        if ($(this).html() != "") {
            //                            if ($(this).hasClass("slide"))
            //                                s_up = true;
            //                            else
            //                                s_down = true;
            //                        }
            //                    });
            //                    if (s_up && s_down) {
            //                        $(".li-surg-surgical-comment-slide-up").each(function () { // Slide down only those elements that are still in slide up
            //                            if ($(this).html() != "") {
            //                                $(this).slideDown("fast");
            //                                $(this).addClass("slide");
            //                            }
            //                        });
            //                    } else
            //                        this.viewSurgicalComments(); //calling respective slide fn
            //                } else if ($(e.currentTarget).attr("id") == "spn-affl-view-comments") { //if it's about Hospital Affl
            //                    var s_up = "",
            //                        s_down = "";
            //                    $(".li-surg-hospital-comment-slide-up").each(function () {
            //                        if ($(this).html() != "") {
            //                            if ($(this).hasClass("slide")) // handling when part of comments are in slide up and the other in slide down
            //                                s_up = true;
            //                            else
            //                                s_down = true;
            //                        }
            //                    });
            //                    if (s_up && s_down) {
            //                        $(".li-surg-hospital-comment-slide-up").each(function () {
            //                            if ($(this).html() != "") {
            //                                $(this).slideDown("fast");
            //                                $(this).addClass("slide")
            //                            }
            //                        });
            //                    } else
            //                        this.viewAfflComments();
            //                }
            //            },
            //Phone - hide and show of hospital affl
            //            toggleHospitalData: function (event) {
            //                if ($(event.currentTarget).hasClass("active-tab-affiliation")) {
            //                    $(".li-ph-surg-hospital-sub-data").addClass("hide");
            //                    $(".active-tab-affiliation").removeClass("active-tab-affiliation");
            //                    $(".li-ph-surg-hospital-data").removeClass("gmborderradius0p");
            //                } else {
            //                    $(".active-tab-affiliation").removeClass("active-tab-affiliation");
            //                    $(".li-ph-surg-hospital-sub-data").addClass("hide");
            //                    $(".li-ph-surg-hospital-data").addClass("gmborderradius0p");
            //                    $(event.currentTarget).addClass("active-tab-affiliation");
            //                    $(event.currentTarget).parent().find(".li-ph-surg-hospital-sub-data").removeClass("hide");
            //                }
            //            },

            // Phone, toggle surgical activity data detail list
            //            toggleSurgicalData: function (event) {
            //                if ($(event.currentTarget).hasClass("gmBGDarkGrey")) {
            //                    $(".li-ph-surg-surgical-sub-data").addClass("hide");
            //                    $(event.currentTarget).removeClass("gmBGDarkGrey gmFontWhite").addClass("gmBGLGrey");
            //                    $(".li-ph-surg-surgical-data").removeClass("gmborderradius0p");
            //                } else {
            //                    $(".li-ph-surg-surgical-data").removeClass("gmBGDarkGrey gmFontWhite").addClass("gmBGLGrey");
            //                    $(".li-ph-surg-surgical-sub-data").addClass("hide");
            //                    $(".li-ph-surg-surgical-data").addClass("gmborderradius0p");
            //                    $(event.currentTarget).addClass("gmBGDarkGrey gmFontWhite").removeClass("gmBGLGrey");
            //                    $(event.currentTarget).parent().find(".li-ph-surg-surgical-sub-data").removeClass("hide");
            //                }
            //            },

            //fn to trigger a fn to slide all Surgical Activity with comments
            //            viewSurgicalComments: function () {
            //                var that = this;
            //                $(".li-surg-surgical-comment-slide-up").each(function () {
            //                    if ($(this).html() != "") {
            //                        if ($(this).hasClass("slide")) {
            //                            $(this).slideUp("fast");
            //                            $(this).removeClass("slide")
            //                        } else {
            //                            $(this).slideDown("fast");
            //                            $(this).addClass("slide")
            //                        }
            //                    }
            //                });
            //            },

            //fn to trigger a fn to slide all Hospital Affl. with comments
            //            viewAfflComments: function () {
            //                var that = this;
            //                $(".li-surg-hospital-comment-slide-up").each(function () {
            //                    if ($(this).html() != "") {
            //                        if ($(this).hasClass("slide")) {
            //                            $(this).slideUp("fast");
            //                            $(this).removeClass("slide")
            //                        } else {
            //                            $(this).slideDown("fast");
            //                            $(this).addClass("slide")
            //                        }
            //                    }
            //                });
            //            },

            //to slide a particular Surgical Activity with comments
            //            slideSurgicalComments: function (e) {
            //                var index = $(e.currentTarget).index(".li-surg-surgical-comment-slide-down");
            //                if ($(".li-surg-surgical-comment-slide-up").eq(index).hasClass("slide")) {
            //                    $(".li-surg-surgical-comment-slide-up").eq(index).slideUp("fast");
            //                    $(".li-surg-surgical-comment-slide-up").eq(index).removeClass("slide")
            //                    $(e.currentTarget).find('i').removeClass('fa-chevron-up').addClass('fa-chevron-down');
            //
            //                } else {
            //                    $(".li-surg-surgical-comment-slide-up").eq(index).slideDown("fast");
            //                    $(".li-surg-surgical-comment-slide-up").eq(index).addClass("slide")
            //                    $(e.currentTarget).find('i').removeClass('fa-chevron-down').addClass('fa-chevron-up');
            //                }
            //            },

            //to slide a particular Hospital Affl with comments
            //            slideHospitalComments: function (e) {
            //                var index = $(e.currentTarget).index(".li-surg-hospital-comment-slide-down");
            //                if ($(".li-surg-hospital-comment-slide-up").eq(index).hasClass("slide")) {
            //                    $(".li-surg-hospital-comment-slide-up").eq(index).slideUp("fast");
            //                    $(".li-surg-hospital-comment-slide-up").eq(index).removeClass("slide")
            //                    $(e.currentTarget).find('i').removeClass('fa-chevron-up').addClass('fa-chevron-down');
            //                } else {
            //                    $(".li-surg-hospital-comment-slide-up").eq(index).slideDown("fast");
            //                    $(".li-surg-hospital-comment-slide-up").eq(index).addClass("slide");
            //                    $(e.currentTarget).find('i').removeClass('fa-chevron-down').addClass('fa-chevron-up');
            //                }
            //            },

            //to show a popup with the rep details
            openRepQuickInfo: function (event) {
                var that = this;
                showRepInfoPopup();
                if (GM.Global.Device) {
                    fnGetFSRepInfo($(event.currentTarget).attr("data-rep"), function (data) {
                        if (data.length) {
                            data = data[0];
                            that.repInfo(event, data);
                        } else {
                            data = null;
                            that.repInfo(event, data);
                        }
                    });
                } else {
                    var input = {
                        "repid": $(event.currentTarget).attr("data-rep")
                    };
                    fnGetWebServerData("fieldsales/repinfo", "gmCRMSalesRepListVO", input, function (data) {
                        that.repInfo(event, data);
                    });
                }



            },

            repInfo: function (event, data) {
                var that = this;
                if (data != null) {
                    $("#div-crm-overlay-content-container4 .modal-body").html(that.template_user_quick_info(data));
                    $("#div-crm-overlay-content-container4").find('.modal-body #div-user-quick-info #btn-quick-info-close').unbind('click').bind('click', function () {
                        $("#div-crm-overlay-content-container4").find('.modal-body #div-user-quick-info').html("");
                        hideRepInfoPopup();
                    });
                    $("#div-crm-overlay-content-container4").find('.modal-body #div-user-quick-info .li-user-quick-info-detail').unbind('click').bind('click', function () {
                        hideRepInfoPopup();
                    });
                    if (!_.contains(that.userTabs, 'CRM-FIELDSALES') || $(event.currentTarget).attr("id") != "spn-surgeon-detail-rep")
                        $("#div-crm-overlay-content-container4").find('.modal-body .li-user-quick-info-detail').addClass("hide");
                    var userID = data.repid;
                    getImageDoc(userID, function (data) {
                        $(".div-quick-info-pic-container").find("img").removeAttr("onerror").css("display", "inline-block");

                        $(".div-quick-info-pic-container").find("img").attr("src", data);
                        $(".div-quick-info-pic-container").find("img").attr("onerror", "this.style.display = 'none'");
                    });

                } else {
                    var name = $(event.currentTarget).html();
                    name = name.split(" ");
                    var len = name.length;
                    var data = {};
                    data.firstnm = name[0];
                    data.lastnm = name[len - 1];
                    $("#div-crm-overlay-content-container4 .modal-body").html(that.template_user_quick_info(data));
                    $("#div-crm-overlay-content-container4").find('.modal-body #div-user-quick-info #btn-quick-info-close').unbind('click').bind('click', function () {
                        $("#div-crm-overlay-content-container4").find('.modal-body #div-user-quick-info').html("");
                        hideRepInfoPopup();
                    });
                    $("#div-crm-overlay-content-container4").find('.modal-body #div-user-quick-info .li-user-quick-info-detail').unbind('click').bind('click', function () {
                        hideRepInfoPopup();
                    });
                    if (!_.contains(that.userTabs, 'CRM-FIELDSALES') || $(event.currentTarget).attr("id") != "spn-surgeon-detail-rep")

                        $("#div-crm-overlay-content-container4").find('.modal-body .li-user-quick-info-detail').addClass("hide");
                }

            },

            editSurgeon: function () {
                var that = this;
                GM.Global.Surgeon.Edit = true;
                $("#li-edit").addClass("hide");
                $("#li-save").removeClass("hide");
                if (this.partyid != 0) {
                    var voidAccess = _.findWhere(GM.Global.UserAccess, {
                        funcid: "CRM-SURGEON-VOID"
                    });
                    if (voidAccess != undefined) {
                        $("#li-void").removeClass("hide");
                    } else {
                        $("#li-void").addClass("hide");
                    }
                }
                $("#li-cancel").removeClass("hide");
                this.$("#div-main-content").html(this.template_SurgeonEdit());

                resizeHeadWidth('.gmPanelHead', '.gmBtnGroup', '#panelhead-left');
                if (GM.Global.CurrentTab != "" && GM.Global.CurrentTab != undefined && GM.Global.CurrentTab != "surgical" && GM.Global.CurrentTab != "globus") {
                    colorControl({
                        currentTarget: "#div-surg-" + GM.Global.CurrentTab + "-tab"
                    }, "btn-surg-tabs", "gmBGLGrey gmFontBlack", "gmBGDarkerGrey gmFontWhite");
                    $("#div-surg-" + GM.Global.CurrentTab + "-tab").trigger("click");
                } else {
                    colorControl({
                        currentTarget: "#div-surg-contact-tab"
                    }, "btn-surg-tabs", "gmBGLGrey gmFontBlack", "gmBGDarkerGrey gmFontWhite");
                    GM.Global.CurrentTab = "";
                }

                this.surgEditData = this.model.toJSON();

                var contIndex = 0;
                _.each(this.surgEditData.arrgmsurgeoncontactsvo, function (data, i) {
                    data.index = contIndex++;
                    if (data.conmode == "90450") {
                        var convalue = data.convalue;
                        //data.convalue = convalue.replace(/\D/g, '');
                    }
                });
                _.each(this.surgEditData.arrgmsurgeonsaddressvo, function (data, i) {
                    data.index = i++;
                });

                _.each(this.surgEditData.arrgmcrmsurgeonrepvo, function (data, i) {
                    data.index = i++;
                });



                this.$("#div-surg-contact").html(this.template_SurgeonContact(this.surgEditData));

                _.each(this.surgEditData.arrgmcrmsurgeonrepvo, function (dt) {
                    that.searchRep(dt.index, dt.repname);
                });
                
                var userData = JSON.parse(localStorage.getItem("userData"));
                //Input for fetching company List for company dropdown
                var input = {
                    'token': localStorage.getItem('token'),
                    'companyid': '',
                    'partyid': localStorage.getItem('partyID')
                };
                
                if (this.partyid != 0){
                    var defaultid = that.model.get("companyid");
                    var mode = "edit";
                }
                else{
                    var defaultid = userData.cmpid;
                    var mode = "create";
                }
                
                var elemAdmin = "#surg-company-dropdown";
                companyDropdown(input,defaultid,mode,DropDownView,elemAdmin,function(selectedId){
                    if(that.model.get("companyid") == ""){
                        $("#surg-company-admin").val(userData.cmpid);
                        $("#surg-company-admin").trigger("change");
                    }
                    $("#surg-company-admin").val(selectedId);
                    $("#surg-company-admin").trigger("change");
                    if(selectedId == undefined || selectedId == "0")
                        $("#surg-company-admin").val("");
                });
                

                this.updAddressDiv(this.surgEditData);
                this.setSelect("#select-surgeon-salute", this.model.get("salute"));
                $(".surg-contact-primary-Y").prop("checked", true);

                // PC-3360 :: Ability to add or update Sales rep to Surgeon Profile
                // if (isRep()) {
                //     $(".surgeon-rep-content").find("input").attr("disabled", "disabled");
                //     $("#surgeon-add-new-rep").addClass("hide");
                //     $(".remove-rep").addClass("hide");
                // }

                this.$("#div-surg-address").html(this.template_SurgeonAddress({
                    "module": "surg"
                }));
                //                this.$("#div-surg-hospital").html(this.template_SurgeonHospital());
                //                this.$("#li-saffil-content").html(this.template_SurgeonHospital_List(arrayOf(this.model.get("arrgmcrmsurgeonaffiliationvo"))));

                //                _.each(arrayOf(this.model.get("arrgmcrmsurgeonaffiliationvo")), function (data, i) {
                //                    if (data.priority == "1") //setting select of hospital affl in edit mode
                //                        $(".select-saffil-level").eq(i).val(1);
                //                    else if (data.priority == "2")
                //                        $(".select-saffil-level").eq(i).val(2);
                //                    else
                //                        $(".select-saffil-level").eq(i).val();
                //                });
                this.$("#div-surg-document").html(this.template_SurgeonDocument());
                //                this.renderSearchView();

                this.gmvValidation = new GMVValidation({
                    "el": this.$(".gmPanelContent")
                });
                $("#tab-surg-contact").find(".fa-chevron-right").removeClass("fa-chevron-right").addClass("fa-chevron-down");
                if ($("iPhone").css("display") != "none") { // removing bg color from even listed records in Phone format
                    $("#li-ssurgical-content").addClass("surg-list-no-bgcolor");
                    $("#li-saffil-content").addClass("surg-list-no-bgcolor");
                }
                if (this.fileTypes)
                    this.renderDocList();
                else
                    this.getFileTypes();

                if (this.partyid == 0) //Removing access to upload Documents for iPad users
                    $("#div-surg-document-tab").addClass("hide")

                this.contacts();
                //                this.hospital();

            },
            //            renderSearchView: function () {
            //                var that = this;
            //                var fnName = ""
            //                if (GM.Global.Device)
            //                    fnName = fnSearchAccnt;
            //                var searchOptions = {
            //                    'el': that.$("#div-sur-affl-account-search"),
            //                    'url': URL_WSServer + 'search/account',
            //                    'inputParameters': {
            //                        'token': localStorage.getItem('token')
            //                    },
            //                    'keyParameter': 'accnm',
            //                    'IDParameter': 'accid',
            //                    'NameParameter': 'accnm',
            //                    'fnName': fnName,
            //                    'resultVO': 'gmAccountBasicVO',
            //                    'minChar': 2,
            //                    'callback': function (result) {
            //                        that.getAccnt(result.ID, result.Name, result.Element);
            //                    }
            //                };
            //                var searchView1 = new GMVSearch(searchOptions);
            //            },
            resetSurgeon: function () {

                if (GM.Global.Surgeon.Edit == true && this.partyid != 0) {
                    Backbone.history.loadUrl(Backbone.history.fragment);
                } else
                    window.history.back();

            },

            //            validateAM: function (e) {
            //                if ($("#input-scontacts-asstnm").val() == "" && $(e.currentTarget).parent().parent().attr("Id") == "surgeon-assistant-content")
            //                    $(e.currentTarget).parent().parent().find("input").val("").trigger("change");
            //                if ($("#input-scontacts-mngrnm").val() == "" && $(e.currentTarget).parent().parent().attr("Id") == "surgeon-manager-content")
            //                    $(e.currentTarget).parent().parent().find("input").val("").trigger("change");
            //            },

            loadSelectedTab: function (e) {
                var tab = $(e.currentTarget).attr("name");
                GM.Global.CurrentTab = tab;
                if ($(e.currentTarget).hasClass("btn-surg-tabs")) {
                    colorControl(e, "btn-surg-tabs", "gmBGLGrey gmFontBlack", "gmBGDarkerGrey gmFontWhite");
                    $(".btn-surg-tabs").each(function () {
                        if ($(this).attr("name") != tab)
                            $("#div-surg-" + $(this).attr("name")).addClass("hide")
                        else
                            $("#div-surg-" + $(this).attr("name")).removeClass("hide")
                    });
                } else {
                    if (!$("#div-surg-" + tab).hasClass("hide")) { // if opened
                        $("#div-surg-" + tab).addClass("hide")
                        $(e.currentTarget).find(".fa-chevron-down").removeClass("fa-chevron-down").addClass("fa-chevron-right");
                        $(".btn-surg-phone-tabs").removeClass("hide");
                        //                    $(".btn-surg-phone-tabs").removeClass("gmFontLightGrey gmFontBold").addClass("gmBGLGrey");
                    } else { // if closed
                        $(".btn-surg-phone-tabs").addClass("hide");
                        $(e.currentTarget).find(".fa-chevron-right").removeClass("fa-chevron-right").addClass("fa-chevron-down");
                        $("#tab-surg-" + tab).removeClass("hide");
                        $("#div-surg-" + tab).removeClass("hide");
                        //                    $(".btn-surg-phone-tabs").addClass("gmFontLightGrey gmFontBold").removeClass("gmBGLGrey");
                    }
                }
                switch ($(e.currentTarget).attr("name")) {
                    case "contact":
                        this.contacts();
                        break;
                        //                    case "hospital":
                        //                        this.hospital();
                        //                        break;
                    case "education":
                        this.closeView(this.gmvSurgeonEducation)
                        this.gmvSurgeonEducation = new GMVSurgeonEducation({
                            'el': $('#div-surg-education'),
                            'parent': this
                        });
                        break;
                }
            },

            contacts: function () {
                if (GM.Global.Surgeon.Data.picurl != "" && GM.Global.Surgeon.Data.picurl != null)
                    $("#li-sur-pic-container img").attr("src", URL_CRMFileServer + GM.Global.Surgeon.Data.picurl + "?num=" + Math.random());
                this.getSelectBoxData({
                    "type": "contact"
                });
            },

            getSelectBoxData: function (data) {
                var that = this;
                var input = {
                    'token': localStorage.getItem('token'),
                    'codeaccid': '4000522'
                };
                fnGetWebServerData("search/codelookup", "gmCodeLookUpVO", input, function (arrAddDt) {
                    if (arrAddDt != null) {
                        var arrSelectOpt = new Array();
                        var strSelectOptions = JSON.stringify(arrAddDt).replace(/codeid/g, 'ID').replace(/codenm/g, 'Name');
                        var arrAddDt = $.parseJSON(strSelectOptions);
                        $('#select-surgeon-addrtype').each(function () {
                            $(this).empty();
                            fnCreateOptions(this, arrAddDt);
                            var currVal = $(this).attr("data-value");
                            if (parseNull(currVal) != "")
                                $(this).val(currVal);
                            else
                                $(this).val(arrAddDt[0].ID);
                        });
                    }
                });
            },

            //            hospital: function () {
            //                var that = this;
            //                if ($(".iPhone").css("display") != "none") {
            //                    $(".phone-hospital-aff-list").html("");
            //                    if ($(".phone-hospital-aff-list").hasClass("hide"))
            //                        $(".phone-hospital-aff-list").removeClass("hide");
            //                    _.each(that.model.get("arrgmcrmsurgeonaffiliationvo"), function (data) {
            //                        $(".phone-hospital-aff-list").append("<ul class='ul-phone-acc-name-list'><li class='li-phone-acc-name-list-item gmAlignHorizontal' data-acc-id=" + data.accid + ">" + data.accnm + "</li><li class = 'gmAlignHorizontal gmVerticalMiddle'><i class='fa fa-minus-circle i-sur-affl-acc-remove gmBtnLarge gmAlignHorizontal'></i></li</ul>");
            //                    });
            //                    $(".ul-saffil-edit").each(function () {
            //                        $(this).addClass("hide");
            //
            //                    })
            //                }
            //                this.showPhoneHospital();
            //                $("#li-globus-account").trigger("click");
            //
            //                $("#li-new-account").on("click", function () {
            //                    $("#li-new-account").addClass("gmFontWhite").removeClass("gmFontBlack");
            //                    $("#li-new-account").addClass("gmBGDarkerGrey").removeClass("gmBGLGrey");
            //                    $("#li-globus-account").removeClass("gmFontWhite").addClass("gmFontBlack");
            //                    $("#li-globus-account").removeClass("gmBGDarkerGrey").addClass("gmBGLGrey");
            //                    $("#li-new-account-btn").removeClass("hide");
            //                    $(".li-sur-affl-account-search").addClass("hide");
            //                });
            //                $("#li-globus-account").on("click", function () {
            //                    $("#li-new-account").addClass("gmFontBlack").removeClass("gmFontWhite");
            //                    $("#li-new-account").addClass("gmBGLGrey").removeClass("gmBGDarkerGrey");
            //                    $("#li-globus-account").removeClass("gmFontBlack").addClass("gmFontWhite");
            //                    $("#li-globus-account").removeClass("gmBGLGrey").addClass("gmBGDarkerGrey");
            //                    $(".li-sur-affl-account-search").removeClass("hide");
            //                    $("#li-new-account-btn").addClass("hide");
            //                });
            //
            //
            //                if ($(".input-saffil-acccity").length != 0) {
            //                    $(".span-saffil-state-search").each(function () {
            //                        var spn = this;
            //                        var searchOptions = {
            //                            'el': this,
            //                            'url': URL_WSServer + 'search/codelookup',
            //                            'inputParameters': {
            //                                'token': localStorage.getItem('token'),
            //                                'codegrp': 'STATE'
            //                            },
            //                            'keyParameter': 'codenm',
            //                            'IDParameter': 'codeid',
            //                            'NameParameter': 'codenm',
            //                            'resultVO': 'gmCodeLookUpVO',
            //                            'minChar': 2,
            //                            'callback': function (val) {
            //                                that.getStateValaffl(val, spn);
            //                            }
            //                        };
            //                        var searchView2 = new GMVSearch(searchOptions);
            //                        $(this).find("input").val($(this).parent().find(".data-accstate").attr("value"));
            //                        $(this).find("input").addClass("gmText100");
            //                    });
            //                }
            //                $("#div-sur-affl-account-search").find("input").attr("placeholder", "Search Account");
            //                $("#div-sur-affl-account-search").find("input").addClass("gmText500");
            //
            //                if ($(".iPhone").css("display") != "none") {
            //                    $("#ul-sur-affil-accnt-header").addClass("hide");
            //                    $("#ul-add-hosp-affiliation").addClass("btn-phone-hosp-add").removeClass("hide");
            //                }
            //            },

            addrep: function (e) {
                var that = this;
                var inx = $(".span-scontacts-salesrep").length - 1;
                inx++;
                $("#surgeon-add-new-rep").before(this.template_AddRep({
                    index: inx
                }));
                this.searchRep(inx, "");
            },

            searchRep: function (inx, repnm) {
                var that = this;
                var searchOptions = {
                    'el': that.$("#div-surgeon-search-rep-" + inx),
                    'url': URL_WSServer + 'search/rep',
                    'inputParameters': {
                        'token': localStorage.getItem('token')
                    },
                    'keyParameter': 'repname',
                    'IDParameter': 'repid',
                    'NameParameter': 'repname',
                    'resultVO': 'gmSalesRepQuickVO',
                    'minChar': 3,
                    'callback': function (result) {
                        that.getRep(result.ID, result.Name, result.Element, result.AD, result.VP, result.partyid, inx);
                    }
                };
                this.searchView = new GMVSearch(searchOptions);
                $("#div-surgeon-search-rep-" + inx).find('input').val(repnm);
                $("#div-surgeon-search-rep-" + inx).find("input").attr("placeholder", "Search Rep...");

                //hOSPITAL AFFFL    

                //Surgical Activity

            },

            //callback function which displays Surgeon's Rep Name
            getRep: function (ID, Name, Element, AD, VP, partyid, inx) {
                $("#div-surgeon-search-rep-" + inx).find('input').val(Name);
                var value = _.filter(this.model.get("arrgmcrmsurgeonrepvo"), function (data) {
                    return data.index == inx;
                });
                if (value == "") {
                    this.model.get("arrgmcrmsurgeonrepvo").push({
                        "plinkid": "",
                        "repid": partyid,
                        "index": inx,
                        "primaryfl": "",
                        "voidfl": ""
                    });
                } else {
                    var repArr = _.each(this.model.get("arrgmcrmsurgeonrepvo"), function (data) {
                        if (data.index == inx) {
                            data.repid = partyid;
                        }
                    });
                    this.model.set("arrgmcrmsurgeonrepvo", repArr);
                }
                this.model.get("arrgmcrmsurgeonrepvo");

                $(".div-scontacts-ad-" + inx).html(AD);
                $(".div-scontacts-vp-" + inx).html(VP);
            },
            //
            //            showPhoneHospital: function () {
            //                var that = this;
            //                if (!$("#tab-surg-hospital").hasClass("hide")) {
            //                    $(".li-hosp-account-name").remove();
            //                    $(".li-hosp-account-name").addClass("hide");
            //                } else {
            //                    $(".li-phone-hosp-account-name").remove();
            //                    $(".li-phone-hosp-account-name").addClass("hide");
            //                    $(".li-phone-label").remove();
            //                }
            //            },

            //Phone - shows data in panel
            showPhoneSurgical: function () {
                var that = this;
                if (!$("#tab-surg-surgical").hasClass("hide")) {
                    $("#div-ssurgical-header").remove();
                } else {
                    $("#div-add-surg-surgicalact").remove();
                    $(".li-phone-label").remove();
                    $(".div-surgeon-surgicalact-done").remove();
                }
            },

            //call web service to get all file types to be considered
            getFileTypes: function () {
                var that = this;
                var reftypeArr = [];
                var reftypenmArr = [];
                this.type = [];
                var input = {
                    "token": localStorage.getItem("token"),
                    "codegrp": "SGFILE"
                };
                fnGetWebServerData("search/codelookup", "gmCodeLookUpVO", input, function (data) {
                    if (data != null) {
                        _.each(data, function (data1) {
                            reftypeArr.push(data1.codeid);
                            reftypenmArr.push(data1.codenm);
                            that.type.push({
                                "reftype": data1.codeid,
                                "reftypenm": data1.codenm
                            })
                        });
                        that.docCount = that.type.length;
                        that.fileTypes = true;
                        that.renderDocList();
                    }
                });
            },

            //renders the fileupload option according to no of reftypes
            renderDocList: function () {
                var that = this;
                var fileData = this.model.get("arrgmcrmfileuploadvo");
                var docData = [];
                var len = that.type.length;
                for (var i = 0; i < len; i++) {
                    docData[i] = _.filter(fileData, function (data) {
                        if (data.reftype != undefined) {
                            if (data.reftype == that.type[i].reftype)
                                return data;
                        }
                    });
                    if (docData[i] == "")
                        docData[i] = {
                            "fileid": "",
                            "refid": that.partyid,
                            "filename": "",
                            "deletefl": "",
                            "reftype": that.type[i].reftype,
                            "type": "",
                            "refgroup": "103112",
                            "filetitle": "",
                            "updateddate": "",
                            "updatedby": "",
                            "reftypenm": that.type[i].reftypenm
                        };
                    else
                        docData[i] = docData[i][0];
                }
                var dispDoc = $.parseJSON(JSON.stringify(docData));
                _.each(dispDoc, function (data) {
                    data.server = URL_CRMFileServer;
                });
                that.$("#div-document-list").html(that.template_SurgeonDocument_List(dispDoc));
                _.each(docData, function (data, index) {
                    var formData;
                    var filename;
                    if (that.fileUploadViews[data.reftype] != undefined) {
                        formData = that.fileUploadViews[data.reftype].formData;
                        filename = that.fileUploadViews[data.reftype].dispData.filename
                        _.each(docData, function (datax) {
                            if (datax.reftype == data.reftype)
                                datax.filename = filename;
                        });
                        that.fileUploadViews[data.reftype].close();
                    }
                    that.fileUploadViews[data.reftype] = new GMVCRMFileUpload({
                        "el": $("#ul-surg-doc-" + data.reftype).find(".li-surg-file-upload"),
                        "refid": that.partyid,
                        "refgrp": "103112",
                        "arrAcceptetFormat": ['doc', 'docx', 'pdf', 'txt'],
                        "parent": that,
                        callback: function () {
                            that.saveCall();
                        }
                    });
                    that.fileUploadViews[data.reftype].dispData = _.filter(docData, function (datax) {
                        return datax.reftype === data.reftype;
                    });
                    that.fileUploadViews[data.reftype].dispData = that.fileUploadViews[data.reftype].dispData[0];
                    $(".frm-documents .li-fileupload-upload").hide();
                    $(".frm-documents .lb-spn-file-open").hide();
                    if (formData != undefined) {
                        that.$("#ul-surg-doc-" + data.reftype).find("#span-fileupload-browse-filenm").text(filename);
                        that.fileUploadViews[data.reftype].formData = formData;
                    }
                });
                this.SurgeonFiles = that.fileUploadViews;
            },

            //callback function which displays Surgeon's account
            //            getAccnt: function (ID, Name, Element) {
            //                var that = this;
            //                var valExist = _.filter(arrayOf(that.model.get("arrgmcrmsurgeonaffiliationvo")), function (data) {
            //                    return data.accnm == Name
            //                });
            //                if (valExist.length == 0) {
            //                    hideMessages();
            //                    if ($(".iPhone").css("display") != "none") {
            //                        $("#div-surg-hospital #ul-phone-affiliation-header").addClass("hide");
            //                        $("#div-surg-hospital .li-sur-affl-account-search").addClass("hide");
            //                    }
            //                    $("#div-sur-affl-account-search").find('input').val("");
            //                    var input = {
            //                        "token": localStorage.getItem("token"),
            //                        "accid": ID
            //                    };
            //
            //                    fnGetWebServerData("search/account", "gmAccountBasicVO", input, function (data) {
            //
            //                        data = data[0];
            //                        var Account = arrayOf(data);
            //                        that.model.set("arrgmcrmsurgeonaffiliationvo", arrayOf(that.model.get("arrgmcrmsurgeonaffiliationvo")));
            //                        that.model.get("arrgmcrmsurgeonaffiliationvo").push({
            //                            "accid": data.accid,
            //                            "accnm": data.accnm,
            //                            "priority": 2,
            //                            "casepercent": "",
            //                            "accstate": data.accstate,
            //                            "acccity": data.acccity,
            //                            "accstateid": data.accstateid,
            //                            "comments": "",
            //                            "voidfl": "",
            //                            "afflid": ""
            //                        });
            //                        that.$("#li-saffil-content").append(that.template_SurgeonHospital_List(Account));
            //                        that.showPhoneHospital();
            //                        $(".span-saffil-state-search").each(function () {
            //                            var spn = this;
            //                            var searchOptions = {
            //                                'el': this,
            //                                'url': URL_WSServer + 'search/codelookup',
            //                                'inputParameters': {
            //                                    'token': localStorage.getItem('token'),
            //                                    'codegrp': 'STATE'
            //                                },
            //                                'keyParameter': 'codenm',
            //                                'IDParameter': 'codeid',
            //                                'NameParameter': 'codenm',
            //                                'resultVO': 'gmCodeLookUpVO',
            //                                'minChar': 2,
            //                                'callback': function (val) {
            //                                    that.getStateValaffl(val, spn);
            //                                }
            //                            };
            //                            var searchView = new SearchView(searchOptions);
            //                            $(this).find("input").val($(this).parent().find(".data-accstate").attr("value"));
            //                            $(this).find("input").addClass("gmText100");
            //                        });
            //                    });
            //                    $(".input-saffil-accnm").on("change", function () {
            //                        $(this).parent().find(".data-accnm").val($(this).val());
            //                    });
            //                } else
            //                    showError("Account already exists");
            //
            //            },

            //callback function which displays Surgeon's Hospital Affil. State Name
            //            getStateValaffl: function (val, elem) {
            //                $(elem).find('input').val(val.Name);
            //                this.$(elem).parent().find(".data-accstate").val(val.Name);
            //                this.$(elem).parent().find(".data-accstateid").val(val.ID);
            //                this.$(elem).find("#div-common-search-x").addClass("hide");
            //            },

            //to open a popup with picture uploading facility
            openPicturePopup: function (e) {
                var that = this;
                showPopup();
                $(this.container).find('.modal-footer').addClass("hide");
                $(this.container).find('.modal-title').html("Change Picture");
                $(this.container).find('.modal-body').html(that.template_picture());
                
                $(".li-btn-delete-photo").on("click",function(){
                    dhtmlx.confirm({
                        title: "Confirm",
                        ok: "Yes",
                        cancel: "No",
                        type: "confirm",
                        text: "Are you sure want to delete picture? ",
                        callback: function (result) {
                            if(result)
                                that.removePic();
                        }
                    });
                });

                if (GM.Global.Surgeon.Data.picurl != "") {
                    $("#div-surgeon-pic").append("<img class = 'img-surgeon-pic'>");
                    $(".img-surgeon-pic").attr("src", URL_CRMFileServer + GM.Global.Surgeon.Data.picurl + "?num=" + Math.random());
                    $(".li-btn-delete-photo").removeClass("hide");
                }

                $(this.container).find('.modal-body #input-surgeon-picture').unbind('change').bind('change', function () {
                    var file = document.getElementById('input-surgeon-picture').files[0];
                    var fileDisplayArea = document.getElementById('div-surgeon-pic');
                    var imageType = /image.*/;
                    if (file.type.match(imageType)) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            var cropdata = { //declaring crop data and calling crop view 
                                'parent': that,
                                'src': reader.result,
                                'el': fileDisplayArea,
                                'callback': function (val) {
                                    that.getCrop(val);
                                }
                            }

                            var imagecrop = new GMVImageCrop(cropdata);
                        }
                        reader.readAsDataURL(file);
                    } else {
                        fileDisplayArea.innerHTML = "File not supported!"
                        that.picData = "";
                    }

                });

                $(this.container).find('.modal-body #input-surgeon-picture').unbind('click').bind('click', function () {
                    //to reload original image on rerendering the same pic
                    if (document.getElementById('input-surgeon-picture').files[0] != "" || document.getElementById('input-surgeon-picture').files[0] != undefined)
                        $('#input-surgeon-picture').val("");

                });
                $(this.container).find('.modal-body .li-btn-change-photo').unbind('click').bind('click', function () {
                    $('.modal-body #input-surgeon-picture').trigger('click');
                });

            },

            //call back fn to crop data
            getCrop: function (data) {
                var that = this;
                var fileDisplayArea = document.getElementById('div-surgeon-pic');
                fileDisplayArea.innerHTML = "";
                var img = new Image();
                img.src = data;
                img.className = "img-surgeon-pic";
                fileDisplayArea.appendChild(img);
                $("#div-surgeon-pic").append("<div id='div-crop-ok' class='div-btn-crop gmText100 gmBGphysicianvisit gmBtn gmFontWhite gmTextCenter gmText150'> Ok</div>")
                var dataURI = data;
                var binary = atob(dataURI.split(',')[1]);
                var array = [];
                for (var i = 0; i < binary.length; i++)
                    array.push(binary.charCodeAt(i));
                var picture = new Blob([new Uint8Array(array)], {
                    type: 'image/jpeg'
                });
                that.updatePic(picture);
            },

            //to update data of picture when size is changed
            updatePic: function (picfile) {
                var that = this;
                var $form = $("#frm-sur-picture");
                var params = $form.serializeArray();
                this.picData = new FormData();
                this.picfile = picfile;
                $.each(params, function (i, val) {
                    if (val.name != "file")
                        that.picData.append(val.name, val.value);
                });
                $("#div-crm-overlay-content-container #div-crop-ok").unbind('click').bind('click', function () {
                    if (that.partyid == 0) {
                        hidePopup();
                    } else {
                        that.uploadImage(that.model.get("partyid"));
                    }

                    hidePopup();
                });
            },
            
            removePic: function(){
                var that = this;
                var input = {
                    "refid": that.partyid,
                    "filename": that.partyid + "-ProfilePic.jpeg",
                    "deletefl": "Y",
                    "refgroup": "103112",
                    "filetitle": "Surgeon Profile Picture"
                };
                console.log(input);
                if (parseNull(GM.Global.Surgeon.Data.picfileid) != "")
                    input['fileid'] = GM.Global.Surgeon.Data.picfileid;

                console.log(JSON.stringify(input));
                fnGetWebServerData("file/savefile", undefined, input, function (fileData) {
                    console.log("FILE>>>>" + JSON.stringify(fileData));
                    if (fileData != null && fileData.deletefl=='Y'){
                        GM.Global.Surgeon.Data["picfileid"] = "";
                        GM.Global.Surgeon.Data["picurl"]="";
                        $("#div-surgeon-pic").html("");
                        $("#div-sur-pic-container #div-name-container").find("img").attr("src","");
                        $(".li-btn-delete-photo").addClass("hide");
                        hidePopup();
                        showSuccess("Profile Picture Removed Successfully");
                    }
                    else{
                        displayPopupMsg("Profile Picture Removed Failed");
                    }

                });
            },

            uploadImage: function (partyid) {
                var that = this;
                this.partyid = partyid;
                this.picData.append('file', this.picfile, partyid + "-ProfilePic.jpeg");
                if (this.picData != "" && this.picData != undefined) {
                    this.picData.append('refid', partyid);
                    this.picData.append('refgrp', '103112');
                    this.picData.append('token', localStorage.getItem('token'));
                    this.picData.append('fileid', parseNull(GM.Global.Surgeon.Data.picfileid));
                    this.picData.append('reftype', '91182');
                    this.picData.append('filetitle', 'Surgeon Profile Picture');
                    console.log(this.picData);
                    $.ajax({
                        url: URL_WSServer + 'uploadedFile/crmfileupload',
                        data: this.picData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        type: 'POST',
                        success: function (result) {
                            console.log(result);
                            var token = localStorage.getItem("token");
                            var input = {
                                "token": token,
                                "refid": that.partyid,
                                "filename": that.partyid + "-ProfilePic.jpeg",
                                "deletefl": "",
                                "refgroup": "103112",
                                "filetitle": "Surgeon Profile Picture"
                            };
                            console.log(input);
                            if (parseNull(GM.Global.Surgeon.Data.picfileid) != "")
                                input['fileid'] = GM.Global.Surgeon.Data.picfileid;

                            console.log(JSON.stringify(input));
                            fnGetWebServerData("file/savefile", undefined, input, function (fileData) {
                                console.log("FILE>>>>" + JSON.stringify(fileData));
                                if (fileData != null) {
                                    GM.Global.Surgeon.Data["picfileid"] = fileData.fileid;
                                    GM.Global.Surgeon.Data["picurl"] = fileData.refgroup + "/" + fileData.refid + "/" + fileData.filename;
                                    setTimeout(function () {
                                        $("#li-sur-pic-container").find("img").removeAttr("onerror").css("display", "inline-block");
                                        $("#li-sur-pic-container").find("img").attr("src", URL_CRMFileServer + fileData.refgroup + "/" + fileData.refid + "/" + fileData.filename + "?num=" + Math.random())
                                        $("#phone-det-header").attr("src", URL_CRMFileServer + fileData.refgroup + "/" + fileData.refid + "/" + fileData.filename + "?num=" + Math.random());
                                        $("#li-sur-pic-container").find("img").attr("onerror", "this.style.display = 'none'");
                                    }, 0);

                                }

                            });
                        },
                        error: function (model, response, errorReport) {}
                    });
                }
            },


            //popup providing single-select option
            //            openPopup: function (event) {
            //                var that = this;
            //                var item = "";
            //                var reData = [];
            //                var type = 2; // Single Select type
            //                var moduleLS = "",
            //                    moduleCLS = "";
            //                if ($(event.currentTarget).hasClass("div-saffil-accstate-popup")) {
            //                    item = "state";
            //                    reData = that.popupState;
            //                    var index = $(".el-saffil-state").index($(event.currentTarget));
            //                } else if ($(event.currentTarget).hasClass("div-surg-affl-cmnts")) {
            //                    item = "comments";
            //                    if (that.popupHComments == "")
            //                        reData[0] = "";
            //                    else
            //                        reData = that.popupHComments;
            //                    var index = $(".div-surg-affl-cmnts").index($(event.currentTarget));
            //                } else if ($(event.currentTarget).hasClass("div-surg-surgical-cmnts")) {
            //                    item = "comments";
            //                    if (that.popupSComments == "")
            //                        reData[0] = "";
            //                    else
            //                        reData = that.popupSComments;
            //                    var index = $(".div-surg-surgical-cmnts").index($(event.currentTarget));
            //                } else if ($(event.currentTarget).hasClass("div-surg-surgical-procedure")) {
            //                    item = "procedure";
            //                    type = 1; // Multi Select type
            //                    var index = $(".div-surg-surgical-procedure").index($(event.currentTarget));
            //                    if (that.popupProcedure.length < 1) {
            //                        moduleLS = {};
            //                        moduleCLS = {};
            //                    } else {
            //                        moduleLS = that.popupProcedure[index];
            //                        moduleCLS = that.popupProcedureNM[index];
            //                    }
            //
            //                }
            //
            //                that.systemSelectOptions = {
            //                    'title': $(event.currentTarget).attr("data-title"),
            //                    'storagekey': $(event.currentTarget).attr("data-storagekey"),
            //                    'webservice': $(event.currentTarget).attr("data-webservice"),
            //                    'resultVO': $(event.currentTarget).attr("data-resultVO"),
            //                    'IDParameter': $(event.currentTarget).attr("data-IDParameter"),
            //                    'NameParameter': $(event.currentTarget).attr("data-NameParameter"),
            //                    'inputParameters': $(event.currentTarget).attr("data-inputParameters"),
            //                    'module': "surgeon",
            //                    'renderOne': reData[index],
            //                    'event': event,
            //                    'type': type,
            //                    'parent': this,
            //                    'moduleLS': moduleLS,
            //                    'moduleCLS': moduleCLS,
            //                    'callback': function (data1, data2) {
            //                        $(event.currentTarget).html(data2);
            //                        if (item == "state") {
            //                            that.model.get("arrgmcrmsurgeonaffiliationvo")[index].accstateid = data1[0];
            //                            that.model.get("arrgmcrmsurgeonaffiliationvo")[index].accstate = data2[0];
            //                            that.popupState[index] = data1[0];
            //                            if ($.isEmptyObject(data1))
            //                                $(event.currentTarget).html("");
            //                            else
            //                                $(event.currentTarget).html(data2[0]);
            //                        } else if (item == "comments") {
            //                            var d1 = "",
            //                                d2 = "";
            //                            if (data1[0] != undefined)
            //                                var d1 = data1[0];
            //                            if (data2[0] != undefined)
            //                                var d2 = data2[0];
            //                            if ($(event.currentTarget).hasClass("div-surg-affl-cmnts")) {
            //                                that.model.get("arrgmcrmsurgeonaffiliationvo")[index].commentnm = d2;
            //                                that.model.get("arrgmcrmsurgeonaffiliationvo")[index].comments = d1;
            //                                that.popupHComments[index] = d1;
            //                            }
            //                            if ($.isEmptyObject(data1))
            //                                $(event.currentTarget).html("");
            //                            else
            //                                $(event.currentTarget).html(d2);
            //                        } else if (item == "procedure") {
            //                            data1 = $.parseJSON(data1);
            //                            data2 = $.parseJSON(data2);
            //                            that.popupProcedure[index] = data1;
            //                            that.popupProcedureNM[index] = data2;
            //                            if ($.isEmptyObject(data1)) {
            //                                data1 = "";
            //                                data2 = "";
            //                                $(event.currentTarget).html("");
            //                            } else {
            //                                $(event.currentTarget).html(data2.Procedure);
            //                                data1 = data1.procedure;
            //                                data2 = data2.Procedure;
            //                            }
            //                        }
            //                    }
            //                };
            //                that.systemSelectView = new GMVCRMSelectPopup(that.systemSelectOptions);
            //            },

            //to dislay the added new account
            //            newAccnt: function () {
            //
            //                var that = this;
            //                var value = $("#input-surg-affl-add-accnt").val().trim();
            //                var valExist = _.filter(arrayOf(that.model.get("arrgmcrmsurgeonaffiliationvo")), function (data) {
            //                    return data.accnm == value
            //                });
            //                $("#input-surg-affl-add-accnt").val("");
            //                if (value != "") {
            //                    if (valExist.length == 0) {
            //                        hideMessages();
            //                        if ($(".iPhone").css("display") != "none") {
            //                            $("#div-surg-hospital #ul-phone-affiliation-header").addClass("hide");
            //                            $("#div-surg-hospital #li-new-account-btn").addClass("hide");
            //                        }
            //                        var Account = arrayOf({
            //                            "accid": "",
            //                            "accnm": value,
            //                            "priority": 2
            //                        });
            //                        that.model.set("arrgmcrmsurgeonaffiliationvo", arrayOf(that.model.get("arrgmcrmsurgeonaffiliationvo")));
            //                        that.model.get("arrgmcrmsurgeonaffiliationvo").push({
            //                            "accid": "",
            //                            "accnm": value,
            //                            "priority": 2,
            //                            "casepercent": "",
            //                            "accstate": "",
            //                            "acccity": "",
            //                            "accstateid": "",
            //                            "comments": "",
            //                            "voidfl": "",
            //                            "afflid": ""
            //                        });
            //                        if (that.popupState != undefined)
            //                            that.popupState.push("");
            //                        else
            //                            that.popupState = [""];
            //                        this.$("#li-saffil-content").append(this.template_SurgeonHospital_List(Account));
            //                        this.showPhoneHospital();
            //                        $(".span-saffil-state-search").each(function () {
            //                            var spn = this;
            //                            var searchOptions = {
            //                                'el': this,
            //                                'url': URL_WSServer + 'search/codelookup',
            //                                'inputParameters': {
            //                                    'token': localStorage.getItem('token'),
            //                                    'codegrp': 'STATE'
            //                                },
            //                                'keyParameter': 'codenm',
            //                                'IDParameter': 'codeid',
            //                                'NameParameter': 'codenm',
            //                                'resultVO': 'gmCodeLookUpVO',
            //                                'minChar': 2,
            //                                'callback': function (val) {
            //                                    that.getStateValaffl(val, spn);
            //                                }
            //                            };
            //                            var searchView3 = new GMVSearch(searchOptions);
            //
            //                            $(this).find("input").val($(this).parent().find(".data-accstate").attr("value"));
            //                            $(this).find("input").addClass("gmText100");
            //
            //                        });
            //                    } else
            //                        showError("Account Already Exists");
            //                }
            //
            //            },

            //removes a record in hospital affiliation
            //            deleteHospRecord: function (event) {
            //                var that = this;
            //                if ($(event.currentTarget).parent().parent().hasClass("ul-phone-acc-name-list")) { //phone
            //                    var pindex = $(".i-sur-affl-acc-remove").index($(event.currentTarget));
            //                    var attrVal = $(".ul-saffil-edit").eq(pindex).find(".data-afflid").val();
            //                    if (attrVal != "") {
            //                        var rmv = confirm("Do you want to delete this Account?");
            //                        if (rmv == true) {
            //                            this.model.get("arrgmcrmsurgeonaffiliationvo")[pindex].voidfl = "Y";
            //                            this.saveSurgeon(true);
            //                        }
            //                    } else {
            //                        var lvl = $(".select-saffil-level option:selected").eq(pindex).val();
            //
            //                        if (that.hospNm.length > 0 && lvl == 1) { // checking if setting an account as primary when a primary account already exists
            //
            //                            var setThis = [];
            //                            setThis.push(that.hospNm[that.hospNm.length - 1]);
            //                            var len = arrayOf(this.model.get("arrgmcrmsurgeonaffiliationvo"));
            //                            len = len.length;
            //                            _.each(this.model.get("arrgmcrmsurgeonaffiliationvo"), function (data, i) {
            //                                if (_.contains(setThis, data.accnm)) {
            //                                    data.priority = 1;
            //                                    if (len != 1)
            //                                        that.hospNm.pop(data.accnm);
            //                                    that.delHosp = true;
            //                                    $(".select-saffil-level").eq(i).val(1).trigger("change");
            //                                }
            //                            });
            //
            //
            //                        } else if (that.hospNm.length > 0 && lvl != 1) {
            //                            that.hospNm = _.without(that.hospNm, this.model.get("arrgmcrmsurgeonaffiliationvo")[pindex].accnm)
            //                        }
            //                        var val = _.filter(arrayOf(this.model.get("arrgmcrmsurgeonaffiliationvo")), function (data, i) {
            //                            return i != pindex;
            //                        });
            //                        this.model.set("arrgmcrmsurgeonaffiliationvo", val)
            //
            //                        $(event.currentTarget).parent().parent().remove();
            //                        $(".ul-saffil-edit").eq(pindex).remove();
            //                        this.popupHComments.splice(pindex, 1);
            //                    }
            //                } else { //browser
            //                    var attrVal = $(event.currentTarget).parent().parent().find(".data-afflid").val();
            //                    var index = $(".ul-saffil-edit").index($(event.currentTarget).parent().parent());
            //                    if (attrVal != "") {
            //                        var rmv = confirm("Do you want to delete this Account?");
            //                        if (rmv == true) {
            //                            this.model.set("arrgmcrmsurgeonaffiliationvo", arrayOf(this.model.get("arrgmcrmsurgeonaffiliationvo")))
            //                            this.model.get("arrgmcrmsurgeonaffiliationvo")[index].voidfl = "Y";
            //                            this.saveSurgeon(true);
            //                        }
            //                    } else {
            //
            //                        if ($(event.currentTarget).parent().parent().find(".div-saffil-accstate-popup").length > 0) { //to handle value of state in popup
            //                            var position = $(".div-saffil-accstate-popup").index($(event.currentTarget).parent().parent().find(".div-saffil-accstate-popup"));
            //                            if (this.popupState[(position)] != undefined)
            //                                this.popupState.splice(position, 1);
            //                        }
            //
            //                        var lvl = $(".select-saffil-level option:selected").eq(index).val();
            //                        if (that.hospNm.length > 0 && lvl == 1) { // checking if setting an account as primary when a primary account already exists
            //
            //                            var setThis = [];
            //                            setThis.push(that.hospNm[that.hospNm.length - 1]);
            //                            var len = arrayOf(this.model.get("arrgmcrmsurgeonaffiliationvo"));
            //                            len = len.length;
            //                            _.each(this.model.get("arrgmcrmsurgeonaffiliationvo"), function (data, i) {
            //                                if (_.contains(setThis, data.accnm)) {
            //                                    data.priority = 1;
            //                                    if (len != 1)
            //                                        that.hospNm.pop(data.accnm);
            //                                    that.delHosp = true;
            //                                    $(".select-saffil-level").eq(i).val(1).trigger("change");
            //                                }
            //                            });
            //
            //
            //                        } else if (that.hospNm.length > 0 && lvl != 1) {
            //                            that.hospNm = _.without(that.hospNm, this.model.get("arrgmcrmsurgeonaffiliationvo")[index].accnm);
            //                        }
            //                        this.popupHComments.splice(index, 1);
            //                        var val = _.filter(arrayOf(this.model.get("arrgmcrmsurgeonaffiliationvo")), function (data, i) {
            //                            return i != index;
            //                        });
            //                        this.model.set("arrgmcrmsurgeonaffiliationvo", val)
            //                        $(event.currentTarget).parent().parent().remove();
            //                        if (!$("#tab-surg-hospital").hasClass("hide")) {
            //                            $("#div-surg-hospital #ul-add-hosp-affiliation i").removeClass("hide");
            //                            $("#div-surg-hospital #ul-add-hosp-affiliation").addClass("btn-phone-hosp-add").removeClass("hide");
            //                            $(".li-phone-minus-icon-affiliation").eq(index).addClass("hide");
            //                            $("#div-surg-hospital .phone-hospital-aff-list").removeClass("hide");
            //                            $("#div-surg-hospital .ul-phone-acc-name-list").removeClass("hide");
            //                            $("#div-surg-hospital .ul-phone-acc-name-list").eq(index).remove();
            //                        }
            //                    }
            //                }
            //            },

            //function which select Surgeon's Salutation
            partySalute: function (e) {
                var selectedVal = $("#select-surgeon-salute").val();
                $("#surgeon-salute").val(selectedVal);
                $("#surgeon-salute").trigger("change");
            },
            setSelect: function (controlid, selectedvalue) {
                $(controlid).val(selectedvalue);
            },
            setChecked: function (controlid, selectedvalue) {
                $(controlid).prop("chekced", true);
            },
            //to void out a form type
            deleteForm: function (e) {
                var that = this;
                var position = $(e.currentTarget).parent().parent().attr("id").replace("ul-surg-doc-", "");

                if (that.fileUploadViews[position] != undefined && that.fileUploadViews[position].dispData != undefined && that.fileUploadViews[position].dispData.filename != "") {
                    var rmv = confirm("Do you want to delete the Document?");
                    if (rmv == true) {
                        if (that.fileUploadViews[position].dispData.fileid) {
                            that.fileUploadViews[position].dispData.deletefl = "Y";
                            var input = that.fileUploadViews[position].dispData;
                            input["token"] = localStorage.getItem("token");
                            input['fileid'] = that.fileUploadViews[position].dispData.fileid;
                            that.fileUploadViews[position].dispData = "";
                            delete input.server;
                            delete input.search;
                            fnGetWebServerData("file/savefile", undefined, input, function (fileData) {
                                if (fileData != null) {
                                    showSuccess("File is Deleted")
                                    that.resetSurgeon();
                                }
                            });
                        } else {
                            that.fileUploadViews[position].dispData = "";
                            if (that.fileUploadViews[position].formData)
                                that.fileUploadViews[position].formData = undefined;
                            that.$("#ul-surg-doc-" + position).find("#span-fileupload-browse-filenm").text("");
                        }
                    }
                }
            },

            //            togglePhoneDisplay: function (e) {
            //                var tab = $(e.currentTarget).attr("name");
            //                if ($("#li-surg-phone-" + tab).hasClass("hide")) {
            //                    $(".li-surg-phn-tabs").addClass("hide");
            //                    $(e.currentTarget).removeClass("hide");
            //                    $(".li-surg-phone-cnts").addClass("hide");
            //                    $("#li-surg-phone-" + tab).removeClass("hide");
            //                    $(e.currentTarget).find(".fa-chevron-right").removeClass("fa-chevron-right").addClass("fa-chevron-down");
            //                    //                $("#tab-surg-"+tab).addClass("gmFontLightGrey gmFontBold").removeClass("gmBGLGrey");
            //                } else {
            //                    $(".li-surg-phone-cnts").addClass("hide");
            //                    $(".li-surg-phn-tabs").removeClass("hide");
            //                    $(e.currentTarget).find(".fa-chevron-down").removeClass("fa-chevron-down").addClass("fa-chevron-right");
            //                    //                $("#tab-surg-"+tab).removeClass("gmFontLightGrey gmFontBold").addClass("gmBGLGrey");
            //                }
            //            },

            //to expand the item details
            /*showAccDetail: function (event) {
                var index = $(".li-phone-acc-name-list-item").index($(event.currentTarget));
                $("#ul-sur-affil-accnt-header").removeClass("hide");
                $("#ul-sur-affil-accnt-header").find(".ul-saffil-edit").eq(index).removeClass("hide").addClass("active-detail");
                $(".phone-hospital-aff-list").addClass("hide");
                $("#div-surg-hospital #ul-phone-affiliation-header").addClass("hide");
                $("#div-surg-hospital .li-sur-affl-account-search").addClass("hide");
                $("#div-surg-hospital #ul-add-hosp-affiliation i").addClass("hide");
            },*/

            //to add a new globus account
            //            addAccount: function (event) {
            //
            //                if ($(event.currentTarget).parent().hasClass("active-detail")) {
            //                    this.closeAccDetail();
            //                    $(".active-detail").removeClass("active-detail");
            //                } else {
            //                    $("#li-globus-account").trigger("click");
            //                    var AccName = $(event.currentTarget).parent().find(".data-of-accnm").val();
            //                    var AccID = $(event.currentTarget).parent().find(".data-accid").val();
            //                    $(".phone-hospital-aff-list").append("<ul class='ul-phone-acc-name-list'><li class='li-phone-acc-name-list-item gmAlignHorizontal' data-acc-id=" + AccID + ">" + AccName + "</li><li class = 'gmAlignHorizontal gmVerticalMiddle'><i class='fa fa-minus-circle i-sur-affl-acc-remove gmBtnLarge gmAlignHorizontal'></i></li</ul>");
            //                    $("#div-surg-hospital .ul-saffil-edit").addClass("hide");
            //                    $("#div-surg-hospital #ul-sur-affil-accnt-header").addClass("hide");
            //                    $("#div-surg-hospital .ul-phone-acc-name-list").removeClass("hide");
            //                    $("#ul-add-hosp-affiliation").removeClass("hide").addClass("btn-phone-hosp-add");
            //                    $(".phone-hospital-aff-list").removeClass("hide");
            //                    $("#div-surg-hospital #ul-add-hosp-affiliation i").removeClass("hide");
            //                }
            //            },

            //closes account details
            //            closeAccDetail: function () {
            //                $("#ul-add-hosp-affiliation i").removeClass("hide");
            //                $("#div-surg-hospital .ul-saffil-edit").addClass("hide");
            //                $(".phone-hospital-aff-list").removeClass("hide");
            //            },
            //            showSystemBtn: function () {
            //                $("#div-surg-surgical #ul-sur-surg-system-header").removeClass("hide");
            //                $("#div-surg-surgical .ul-phone-surgicalact-list").addClass("hide");
            //                $("#div-surg-surgical #div-add-surg-surgicalact i").addClass("hide");
            //                //                $("#div-surg-surgical #ul-phone-affiliation-header").removeClass("hide");
            //                $("#div-surg-surgical .li-sur-affl-account-search").removeClass("hide");
            //                //                $("#div-surg-surgical #ul-add-hosp-affiliation").removeClass("btn-phone-hosp-add").addClass("hide");
            //                $("#div-surg-surgical #ul-sur-affil-accnt-header").removeClass("hide");
            //            },

            // show account button in mobile
            //            showAccBtn: function () {
            //                $("#div-surg-hospital #div-surgeon-affl").removeClass("hide");
            //                $("#div-surg-hospital .ul-phone-acc-name-list").addClass("hide");
            //                $("#div-surg-hospital #ul-phone-affiliation-header").removeClass("hide");
            //                $("#div-surg-hospital .li-sur-affl-account-search").removeClass("hide");
            //                $("#div-surg-hospital #ul-add-hosp-affiliation").removeClass("btn-phone-hosp-add").addClass("hide");
            //                $("#div-surg-hospital #ul-sur-affil-accnt-header").removeClass("hide");
            //            },
            //validation for npid
            checkNumber: function (e) {
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) { 
                    if ($(e.currentTarget).attr("upd") == "npid")
                        showError("NPI should contain numeric characters only and should be 10-digit number");
                    else if ($(e.currentTarget).attr("upd") == "casepercent") {
                        showError("Number of Case(s) should contain numeric characters only and should be 4-digit number");
                        e.preventDefault();
                    } else
                        showError("Please enter Phone Number and should be 10-digit number");
                } else
                    hideMessages();
            },
            
            // New Method to allow only the valid characters like Numbers, - and spaces
            checkPhoneNumber: function(e){ 
                if((e.which == 32 || e.which == 45) || ((e.which > 47) && (e.which < 58))){
                   return true;       
                }
                else{
                    e.preventDefault();
                }
            },

            checkNpid: function (e, callback) {
                var that = this;
                var val = $("#input-scontacts-npid").val();
                var textNum = val.replace(/\D/g, '');
                var re = /[^0-9]/g;
                var error = re.test(textNum);
                if (parseNull(val) != "") {
                    if (error == true || textNum.length != 10) { //npid should be numberic and with max length of 10
                        showError("NPI should contain numeric characters only and should be 10-digit number");
                        this.$("#input-scontacts-npid").focus();
                        if (callback)
                            callback(false);
                    } else {
                        hideMessages();
                        if (val != "") {
                            getSurgeonPartyInfo(val, function (surgeonData) { //calling surgeon's party information of typed in npid
                                //that.chkNPI == 1 already allocated;  that.chkNPI == 0 npid verified
                                if (surgeonData != undefined) {
                                    surgeonData = surgeonData[0];
                                    if (that.partyid == 0) { // If new surgeon and npi exists - Fail
                                        $(".npi-detection.fa-check-circle-o").remove();
                                        if ($(".npi-detection.fa-times-circle").length == 0)
                                            $("#li-surg-chknpid").append("<span class='fa npi-detection fa-times-circle'></span>");
                                        if (callback)
                                            callback(false);
                                    } else {
                                        if (surgeonData.partyid == that.partyid) { // existing surgeon and npi exists and npi is for same surgeon - Success
                                            $(".npi-detection.fa-times-circle").remove();
                                            if ($(".npi-detection.fa-check-circle-o").length == 0)
                                                $("#li-surg-chknpid").append("<span class='fa npi-detection fa-check-circle-o'></span>");
                                            if (callback)
                                                callback(true);
                                        } else { // existing surgeon and npi exists and npi is for Another surgeon - Fail
                                            $(".npi-detection.fa-check-circle-o").remove();
                                            if ($(".npi-detection.fa-times-circle").length == 0)
                                                $("#li-surg-chknpid").append("<span class='fa npi-detection fa-times-circle'></span>");
                                            if (callback)
                                                callback(false);
                                        }
                                    }
                                } else { //fresh npid - Success
                                    $(".npi-detection.fa-times-circle").remove();
                                    if ($(".npi-detection.fa-check-circle-o").length == 0)
                                        $("#li-surg-chknpid").append("<span class='fa npi-detection fa-check-circle-o'></span>");
                                    if (callback)
                                        callback(true);
                                }

                            }, true);
                        } else { //empty input - Success
                            if ($(".npi-detection.fa-times-circle").length != 0)
                                $(".npi-detection.fa-times-circle").remove();
                            if ($(".npi-detection.fa-check-circle-o").length != 0)
                                $(".npi-detection.fa-check-circle-o").remove();
                            if (callback)
                                callback(true);
                        }
                    }

                } else {
                    if (callback)
                        callback(true);
                    $("#li-surg-chknpid ").find("span").remove();
                }
            },

            updateContactVO: function (e, conid, index) {
                var that = this;
                var contype = $(e.currentTarget).attr("contype");
                var conmode = $(e.currentTarget).attr("conmode");
                var convalue = $(e.currentTarget).val();
                var conid = $(e.currentTarget).attr("conid");
                var primaryfl = $(e.currentTarget).attr("primaryfl");
                var voidfl = $(e.currentTarget).attr("voidfl");
                var index = $(e.currentTarget).attr("index");

                if (parseNull(primaryfl) != "")
                    _.each(this.model.get("arrgmsurgeoncontactsvo"), function (dt) {
                        if (dt.primaryfl == "Y" && dt.index != index && dt.conmode == conmode) {
                            dt.primaryfl = "";
                        }
                    });


                var contVOArr = _.filter(this.model.get("arrgmsurgeoncontactsvo"), function (data) {
                    return data.index == index;

                });
                if (contVOArr.length) {
                    var contArr = _.each(this.model.get("arrgmsurgeoncontactsvo"), function (data) {
                        var a = _.filter(that.model.get("arrgmsurgeoncontactsvo"), function (data) {
                            return data.index == index;
                        });
                        if (data.index == index) {
                            data.convalue = convalue;
                            data.contype = contype;
                            data.primaryfl = primaryfl;
                        }
                    });
                    this.model.set("arrgmsurgeoncontactsvo", contArr);
                } else {
                    this.model.get("arrgmsurgeoncontactsvo").push({
                        "contype": contype,
                        "conmode": conmode,
                        "convalue": convalue,
                        "conid": "",
                        "primaryfl": primaryfl,
                        "voidfl": "",
                        "index": index
                    });
                }
                console.log(this.model.get("arrgmsurgeoncontactsvo"));
            },

            // Update primayFl if the use is not selecting anything
            checkPrimary: function () {
                var contactArr = this.model.get("arrgmsurgeoncontactsvo");

//                var phoneCL = _.filter(contactArr, function (dt) {
//                    return dt.conmode == "90450" && dt.voidfl == "";
//                });
                var emailCL = _.filter(contactArr, function (dt) {
                    return dt.conmode == "90452" && dt.voidfl == "";
                });
//                if (phoneCL.length > 0) {
//                    var phonePFLCnt = _.filter(contactArr, function (dt) {
//                        return dt.primaryfl == "Y" && dt.conmode == "90450" && dt.voidfl == "";
//                    });
//
//                    if (phonePFLCnt.length == 0)
//                        $("#surg-contact-phone-hprimary").val();
//                    else
//                        $("#surg-contact-phone-hprimary").val("Y");
//                } else
//                    $("#surg-contact-phone-hprimary").val("Y");

                if (emailCL.length > 0) {
                    var emailPFLCnt = _.filter(contactArr, function (dt) {
                        return dt.primaryfl == "Y" && dt.conmode == "90452" && dt.voidfl == "";
                    });

                    if (emailPFLCnt.length == 0)
                        $("#surg-contact-email-hprimary").val("");
                    else
                        $("#surg-contact-email-hprimary").val("Y");
                } else
                    $("#surg-contact-email-hprimary").val("Y");

                var addressArr = this.model.get("arrgmsurgeonsaddressvo");
                var addressCL = _.filter(addressArr, function (dt) {
                    return dt.voidfl == "";
                });

                if (addressCL.length > 0) {
                    var addressPFLCnt = _.filter(addressArr, function (dt) {
                        return dt.primaryfl == "Y" && dt.voidfl == "";
                    });

                    if (addressPFLCnt.length == 0)
                        $("#surg-contact-address-hprimary").val("");
                    else
                        $("#surg-contact-address-hprimary").val("Y");
                } else
                    $("#surg-contact-address-hprimary").val("Y");

                var repArr = this.model.get("arrgmcrmsurgeonrepvo");
                if (repArr.length > 0) {
                    var repPFLCnt = _.filter(repArr, function (dt) {
                        return dt.primaryfl == "Y" && dt.voidfl == "";
                    });
                    if (repPFLCnt.length == 0)
                        $("#surg-contact-rep-hprimary").val("");
                    else
                        $("#surg-contact-rep-hprimary").val("Y");
                } else
                    $("#surg-contact-rep-hprimary").val("Y");

            },

            updateModel: function (e, data1, data2, data3) {
                var that = this;
                var attribute = $(e.currentTarget).attr("upd");
                var currVal = $(e.currentTarget).val();
                var VO = $(e.currentTarget).attr("vo"); // Can be changed using closest()
                if (data1 != undefined)
                    var attribute2 = $(e.currentTarget).attr("upd2");
                var index = $(".data-" + attribute).index($(e.currentTarget));

                if (VO == "arrgmsurgeoncontactsvo") { //for contactsVO
                    this.updateContactVO(e);
                }
                if (VO == "arrgmcrmsurgeonaffiliationvo") { //for Affiliation
                    this.model.get(VO)[index][attribute] = currVal;
                } else if (parseNull(attribute) != "" || data3 != undefined) {
                    if (VO != undefined) {
                        var index = $(".data-" + VO).find(".data-" + attribute).index($(e.currentTarget));
                        if (data1 != undefined || VO == "arrgmcrmsurgeonattrvo") {
                            if (data3 != undefined) {
                                _.each(this.model.get(VO), function (data) { //for attributeVO
                                    if (data.attrtype == data3) {
                                        data.attrvalue = data1;
                                        data.attrnm = data2;
                                    }
                                });
                            }
                        }

                    } else {
                        this.model.set(attribute, $(e.currentTarget).val());
                    }
                } else if ($(e.currentTarget).hasClass("txt-fileupload")) {
                    var fileData = [];
                    _.each(this.fileUploadViews, function (file) {
                        fileData.push(file.dispData);
                    });
                    this.model.set("arrgmcrmfileuploadvo", fileData);
                }
            },

            saveSurgeon: function () {
                var that = this;
                this.checkNpid('', function (a) { // Making sure Save is called after the webservice check if npid
                    if (a) {

                        var url = "surgeon/save";
                        that.model.unset('arrgmcrmsurgeonactivityvo');
                        that.model.unset('server');
                        that.model.unset('surgeonaffiliation');
                        that.model.unset('arrgmcrmsurgeonaffiliationvo');
                        that.model.unset('surgeonactivity');
                        that.model.unset('keys');
                        that.model.unset('fileupload');
                        that.model.unset('salescallAct');
                        that.model.unset('mercAct');
                        that.model.unset('phyAct');
                        that.model.unset("address");
                        that.model.unset("spl");
                        that.model.unset("phn");
                        that.model.unset("email");
                        that.model.unset("aphn");
                        that.model.unset("aemail");
                        that.model.unset("bphn");
                        that.model.unset("bemail");

                        if (that.model.get('arrgmsurgeonsaddressvo') && (that.model.get('arrgmsurgeonsaddressvo')[0] == undefined || that.model.get('arrgmsurgeonsaddressvo')[0] == null))
                            that.model.set('arrgmsurgeonsaddressvo', []);
                        if (that.model.get('arrgmcrmsurgeonactivityvo') && (that.model.get('arrgmcrmsurgeonactivityvo')[0] == undefined || that.model.get('arrgmcrmsurgeonactivityvo')[0] == null))
                            that.model.set('arrgmcrmsurgeonactivityvo', []);
                        //                        if (that.model.get('arrgmcrmsurgeonaffiliationvo') && (that.model.get('arrgmcrmsurgeonaffiliationvo')[0] == undefined || that.model.get('arrgmcrmsurgeonaffiliationvo')[0] == null))
                        //                            that.model.set('arrgmcrmsurgeonaffiliationvo', []);
                        if (that.model.get('arrgmcrmsurgeonrepvo') && (that.model.get('arrgmcrmsurgeonrepvo')[0] == undefined || that.model.get('arrgmcrmsurgeonrepvo')[0] == null))
                            that.model.set('arrgmcrmsurgeonrepvo', []);
                        if (that.model.get('arrgmsurgeoncontactsvo') && (that.model.get('arrgmsurgeoncontactsvo')[0] == undefined || that.model.get('arrgmsurgeoncontactsvo')[0] == null))
                            that.model.set('arrgmsurgeoncontactsvo', []);
                        if (that.model.get('arrgmcrmfileuploadvo') && (that.model.get('arrgmcrmfileuploadvo')[0] == undefined || that.model.get('arrgmcrmfileuploadvo')[0] == null))
                            that.model.set('arrgmcrmfileuploadvo', []);
                        if (that.model.get('arrgmcrmeducationvo') && (that.model.get('arrgmcrmeducationvo')[0] == undefined || that.model.get('arrgmcrmeducationvo')[0] == null))
                            that.model.set('arrgmcrmeducationvo', []);
                        if (that.model.get('arrgmcrmprofessionalvo') && (that.model.get('arrgmcrmprofessionalvo')[0] == undefined || that.model.get('arrgmcrmprofessionalvo')[0] == null))
                            that.model.set('arrgmcrmprofessionalvo', []);
                        if (that.model.get('arrgmcrmsurgeonsocialvo') && (that.model.get('arrgmcrmsurgeonsocialvo')[0] == undefined || that.model.get('arrgmcrmsurgeonsocialvo')[0] == null))
                            that.model.set('arrgmcrmsurgeonsocialvo', []);


                        //                        _.each(that.model.get("arrgmcrmsurgeonaffiliationvo"), function (data) {
                        //                            delete data.title;
                        //                            delete data.search;
                        //                        });
                        //                        _.each(that.model.get("arrgmcrmsurgeonrepvo"), function (data) {
                        //                            console.log("<<<<<<>>>>>>>>>>>" + data);
                        //                        });

                        var rmArr = _.reject(that.model.get("arrgmsurgeoncontactsvo"), function (data) {
                            return data.convalue == "";
                        });
                        that.model.set("arrgmsurgeoncontactsvo", rmArr);

                        var eduData = _.reject(that.model.get("arrgmcrmeducationvo"), function (dt) {
                            return dt.classyear == "" && dt.institutenm == "";
                        });
                        that.model.set("arrgmcrmeducationvo", eduData);
                        var profData = _.reject(that.model.get("arrgmcrmprofessionalvo"), function (dt) {
                            return dt.title == "" && dt.companyname == "";
                        });
                        that.model.set("arrgmcrmprofessionalvo", profData);

                        that.checkPrimary();
                        that.fn = that.model.get("firstnm");
                        that.ln = that.model.get("lastnm");
                        that.mn = that.model.get("midinitial");
                        if (that.gmvValidation.validate()) {
                            //Auto Rep Allocation
                            _.each(that.model.get("arrgmsurgeoncontactsvo"), function (data) {
                                delete data.index;
                            });
                            _.each(that.model.get("arrgmsurgeonsaddressvo"), function (data) {
                                delete data.index;
                            });
                            _.each(that.model.get("arrgmcrmeducationvo"), function (data) {
                                delete data.index;
                            });
                            _.each(that.model.get("arrgmcrmprofessionalvo"), function (data) {
                                delete data.index;
                            });
                            _.each(that.model.get("arrgmcrmsurgeonrepvo"), function (data) {
                                delete data.index;
                            });

                            _.each(that.model.get("arrgmcrmfileuploadvo"), function (data) {
                                delete data.path;
                                delete data.server;
                                delete data.search;
                            });

                            if (GM.Global.SalesRep && that.partyid == 0) {
                                var rep = _.filter(that.model.get("arrgmcrmsurgeonrepvo"), function (data) {
                                    return data.attrtype == 11509;
                                });
                                if (rep.length == 0) {
                                    that.model.get("arrgmcrmsurgeonrepvo").push({
                                        "repid": localStorage.getItem("partyID"),
                                        "primaryfl": "",
                                        "plinkid": "",
                                        "voidfl": ""
                                    });
                                }
                            }

                            var input = that.model.toJSON();
                            var tempID = window.location.hash.split('/');
                            var size = tempID.length; // taking id from url 'coz we use temp ids for create type data
                            var id = tempID[size - 1];
                            if (GM.Global.Device) { // Saving Surgeon in local table
                                //Code here
                                fnSaveSurgeon(that.partyid, input, function (data, ID) {
                                    if (data == "success") {
                                        if (id == 0) {
                                            showSuccess("Surgeon Saved")
                                            window.location.href = "#crm/surgeon/id/" + ID;
                                        } else {
                                            showSuccess("Surgeon Saved")
                                            Backbone.history.loadUrl("#crm/surgeon/id/" + ID);
                                        }
                                    } else
                                        showError("Save Failed")
                                });

                            } else {
                                if (!$.isEmptyObject(that.fileUploadViews) && that.fileUploadViews != undefined && that.fileUploadViews != "" && that.fileUploadViews != null) {

                                    var fileDefined = false;
                                    GM.Global.FileUploadData = 0; // Holds count of no of files to upload
                                    GM.Global.FileUploadViewData = 0; // Hplds count of no of files uploaded in CRMFileUploadView
                                    _.each(that.fileUploadViews, function (fileUploadView) {
                                        if (fileUploadView.formData != undefined)
                                            ++GM.Global.FileUploadData;
                                    });
                                    var index = 0;
                                    _.each(that.fileUploadViews, function (fileUploadView) {
                                        if (fileUploadView.formData != undefined) {
                                            fileDefined = true;
                                            fileUploadView.uploadFile();
                                        }
                                        ++index;
                                        if ((index + 1) == that.docCount && !fileDefined)
                                            that.saveCall(); // Only when no file is uploaded
                                    });
                                } else
                                    that.saveCall();
                            }
                        } else
                            showError(that.gmvValidation.getErrors());
                    } else
                        showError("Entered NPI is linked to an existing Surgeon");
                });
            },

            saveCall: function () {
                var that = this;
                var url = "surgeon/save";
                var input = this.model.toJSON();
                console.log("Save Input >>>>>>>>> ");
                console.log(input);
                fnGetWebServerData(url, undefined, input, function (data, model) {
                    console.log("Save Output >>>>>>>>> ");
                    console.log(data)
                    if (data != null) {
                        showSuccess("Surgeon Detail created / updated");
                        if (that.partyid != 0) {
                            showSuccess("Surgeon Saved")
                            Backbone.history.loadUrl(Backbone.history.fragment);
                        } else if (that.partyid == 0) { //new entry
                            showSuccess("Surgeon Saved")
                            window.location.href = "#crm/surgeon/id/" + data.partyid;
                        }

                        if (that.partyid == 0 && that.picfile != undefined)
                            that.uploadImage(data.partyid);

                    } else if (model && model.responseText && $.parseJSON(model.responseText).message == "Record already exists. Please verify the entered record.") {
                        var input = {
                            "token": localStorage.getItem("token"),
                            "firstname": that.fn,
                            "lastname": that.ln,
                            "middleinitial": that.mn
                        };
                        fnGetWebServerData("surgeon/info/", undefined, input, function (data) {
                            if (data != null)
                                showError(that.fn + " " + parseNull(that.mn) + " " + that.ln + "'s information already exists");
                            else
                                showError("Save Failed");
                        });
                    } else
                        showError("Save Failed");
                });
            },

            // resetting other accounts' priority to secondary when setting current account as primary
            //            checkLvl: function (e) {
            //                var that = this;
            //                if (!this.delHosp && that.loopExit) {
            //                    var index = $(".select-saffil-level").index($(e.currentTarget));
            //                    _.each(this.model.get("arrgmcrmsurgeonaffiliationvo"), function (data) {
            //                        if (data.priority == 1) {
            //                            if (!_.contains(that.hospNm, data.accnm))
            //                                that.hospNm.push(data.accnm);
            //                        }
            //                    });
            //
            //                    var lvl = $(".select-saffil-level option:selected").eq(index).val();
            //
            //                    if (that.hospNm.length > 0 && lvl == 1) { // checking if setting an account as primary when a primary account already exists
            //                        var len = $(".select-saffil-level").length - 1;
            //                        $(".select-saffil-level").each(function (i) {
            //                            if (i != index) {
            //                                that.loopExit = false;
            //                                $(this).val(2).trigger("change");
            //                            }
            //                            if (i == len)
            //                                that.loopExit = true;
            //                        });
            //                    }
            //                }
            //                this.updateModel(e);
            //                this.delHosp = false;
            //            },

            //  To delele details from the list
            confirmVoid: function (event) {
                var that = this;
                var bolVoid = confirm("Do you want to void this details?");

                if (bolVoid) {
                    var surgeonInput = {
                        "token": localStorage.getItem("token"),
                        "partyid": this.model.get("partyid"),
                        "companyid": this.model.get("companyid"),
                        "npid": this.model.get("npid"),
                        "firstnm": this.model.get("firstnm"),
                        "lastnm": this.model.get("lastnm"),
                        "partynm": "",
                        "midinitial": "",
                        "asstnm": "",
                        "mngrnm": "",
                        "salute": "",
                        "activefl": "N",
                        "arrgmsurgeonsaddressvo": [],
                        "arrgmsurgeoncontactsvo": [],
                        //                        "arrgmcrmsurgeonaffiliationvo": [],
                        "arrgmcrmsurgeonactivityvo": [],
                        "arrgmcrmsurgeonrepvo": [],
                        "arrgmcrmprofessionalvo": [],
                        "arrgmcrmeducationvo": [],
                        "arrgmcrmfileuploadvo": []
                    };
                    fnGetWebServerData("surgeon/save", undefined, surgeonInput, function (data) {
                        if (data != null)
                            showSuccess("Surgeon voided ");
                        window.history.back();
                    });
                } else
                    this.void = "";
            },

            viewDocument: function (event) {
                var path = $(event.currentTarget).attr('href-url');
                if (parseNull(path) != "")
                    window.open(path, '_blank', 'location=no');
            },

            openDoc: function (e) {
                var that = this;
                var path = $(e.currentTarget).attr("name");
                var docID = $(e.currentTarget).attr("docID");

                fnResolvePath(path, function (status, devicepath) {
                    if (status) {
                        var DOCViewer = window.open(decodeURI(devicepath), '_blank', 'location=no');
                    } else {
                        var connectionType = (navigator.connection.type).toLowerCase();
                        if ((connectionType.indexOf('cellular') >= 0) && that.parent.confirmDownload) {
                            showNativeConfirm("This device is connected to internet through Data plan. Do you really want to use Data plan to download the file?", "Warning", ["Yes", "Yes, Don't ask me again", "No"], function (btnIndex) {
                                if (btnIndex == 1)
                                    that.download(URL_CRMFileServer, devicepath, path, docID);
                                else if (btnIndex == 2) {
                                    that.parent.confirmDownload = false;
                                    that.download(URL_CRMFileServer, devicepath, path, docID);
                                }
                            })
                        } else
                            that.download(URL_CRMFileServer, devicepath, path, docID);

                    }
                });
            },

            download: function (URL_CRMFileServer, devicepath, path, docID) {
                fnDownload(URL_CRMFileServer + path, devicepath + path, function (percent) {
                    $('#spn-prg-' + docID).show();
                    $('#spn-prg-' + docID).find('progress').val(percent * 100);
                }, function () {
                    $('#spn-prg-' + docID).hide();
                    var DOCViewer = window.open(decodeURI(devicepath) + path, '_blank', 'location=no');

                });
            },
            //Handles the orietaion change in the iPad
            onOrientationChange: function (event) {
                resizeHeadWidth('.gmPanelHead', '.gmBtnGroup', '#panelhead-left');
            },

            addMoreContacts: function (e) {

                var that = this;
                var currEle = $(e.currentTarget).attr("name");
                var conmode = "90450";
                var inputFormat = "text";
                var MaxChar = 25;
                var placeHoderTxt = "&#xf095;&nbsp;&nbsp;&nbsp;Phone *";
                var type = "phone";
                var message = "Valid Phone Number";
                var onKeyPress = "onKeyPress='if(this.value.length==25) return false;'";
                if (currEle == "email") {
                    conmode = "90452";
                    inputFormat = "text";
                    placeHoderTxt = "&#xf0e0;&nbsp;&nbsp;&nbsp;Email *";
                    MaxChar = 250;
                    onKeyPress = "";
                    type = "email";
                    message = "Email Correct Format";
                }
                var contCnt = this.model.get("arrgmsurgeoncontactsvo").length;
                var primaryflCnt = "<li class='gmAlignHorizontal'> <input type='checkbox' class='surgeon-phone-primary surg-" + type + "-cbox surgeon-update-cont'  index=" + contCnt + " name='primaryfl' data-conttype=" + type + "> </li>";
                var validClass = "";
                if(currEle == "phone"){
                    primaryflCnt = "";
                    validClass = "input-surg-phone";
                }
                console.log(contCnt);
                contCnt++;
                $("#surgeon-add-new-" + currEle).before("<ul class='gmAlignHorizontal'> <i class='fa fa-minus-circle remove-contact gmFontRed gmBtnLarge' index=" + contCnt + "></i> <li class='gmSectionValue gmMediaWidth100 gmAlignHorizontal'> <div conmode=" + conmode + " id='div-surg-phone-contype' class ='surg-phone-contype gmAlignHorizontal category-btn popup-icon gmWidth100m  gmInputText gmVerticalMiddle' data-param='contype' data-keyParameter='codenm' data-codegrp='ADDTP' data-IDParameter='codeid' data-NameParameter='codenm' data-resultVO='gmCodeLookUpVO' data-webservice='search/codelookup' data-storagekey='contype' data-title='Label' contype='{{contype}}' index=" + contCnt + " name='contype'><span></span></div> </li> <li class='gmSectionListItem gmSectionValue gmAlignHorizontal data-cont gmVerticalTop gmMediaWidth100'> <input id='input-surg-" + currEle + "' type=" + inputFormat + " min='0' class='"+validClass+" input-surg-contact gmWidth180m  gmInputText' " + onKeyPress + " placeholder=" + placeHoderTxt + " name='convalue' vo='arrgmsurgeoncontactsvo' conmode=" + conmode + " contype='Home' primaryfl='' value='' validate=" + currEle + " index=" + contCnt + " maxlength=" + MaxChar + " message='Please Enter " + message + "'> </li>"+primaryflCnt+"</ul>");

                this.getSelectBoxData({
                    "type": "contact"
                });
            },

            removeContact: function (e) {
                var contId = $(e.currentTarget).attr("conid");
                var contIndex = $(e.currentTarget).attr("index");
                var rmArr;
                if (parseNull(contId) != "") {
                    rmArr = _.each(this.model.get("arrgmsurgeoncontactsvo"), function (num) {
                        if (num.conid == contId)
                            num.voidfl = "Y";
                    });
                } else {
                    rmArr = _.reject(this.model.get("arrgmsurgeoncontactsvo"), function (num) {
                        return num.index == contIndex;
                    });
                }
                $(e.currentTarget).parent().remove();
                this.model.set("arrgmsurgeoncontactsvo", rmArr);

            },
            setPrimary: function (e) {
                var that = this;
                var currType = $(e.currentTarget).attr("data-conttype");
                var plinkID = $(e.currentTarget).attr("plinkid");
                var repIndex = $(e.currentTarget).attr("index");
                var rmArr = [];
                var primaryFl = "";
                $(e.currentTarget).attr("primaryfl", "Y");
                var currFl = $(e.currentTarget).prop("checked");
                if (currFl)
                    primaryFl = "Y";

                console.log(currFl);
                $(".surg-" + currType + "-cbox").not($(e.currentTarget)).prop('checked', false);
                $(".surg-" + currType + "-cbox").not($(e.currentTarget)).prop('primaryfl', '');
                if (parseNull(plinkID) != "") {
                    rmArr = _.each(this.model.get("arrgmcrmsurgeonrepvo"), function (num) {
                        num.primaryfl = "";
                        if (num.plinkid == plinkID)
                            num.primaryfl = primaryFl;
                    });
                } else {
                    rmArr = _.each(this.model.get("arrgmcrmsurgeonrepvo"), function (num) {
                        num.primaryfl = "";
                        if (num.index == repIndex)
                            num.primaryfl = primaryFl;
                    });
                }
                this.model.set("arrgmcrmsurgeonrepvo", rmArr);

            },
            removeRep: function (e) {
                var that = this;
                var plinkID = $(e.currentTarget).attr("plinkid");
                var repIndex = $(e.currentTarget).attr("index");
                var rmArr;
                if (parseNull(plinkID) != "") {
                    rmArr = _.each(this.model.get("arrgmcrmsurgeonrepvo"), function (num) {
                        if (num.plinkid == plinkID)
                            num.voidfl = "Y";
                    });
                } else {
                    rmArr = _.reject(this.model.get("arrgmcrmsurgeonrepvo"), function (num) {
                        return num.index == repIndex;
                    });
                }
                $(e.currentTarget).parent().remove();
                this.model.set("arrgmcrmsurgeonrepvo", rmArr);

            },
            changeContact: function (e, resid, resnm) {
                var that = this;
                var lableName = $(e.currentTarget).attr("name");
                console.log($(e.currentTarget));
                console.log(lableName);
                switch (lableName) {
                    case "contype":
                        $(e.currentTarget).parent().parent().find(".input-surg-contact").attr("contype", resnm);
                        $(e.currentTarget).parent().parent().find(".input-surg-contact").trigger("change");
                        break;
                    case "primaryfl":
                        if ($(e.currentTarget).prop("checked")) {
                            $(e.currentTarget).parent().parent().find(".input-surg-contact").attr("primaryfl", "Y");
                            $(e.currentTarget).parent().parent().find(".input-surg-contact").trigger("change");
                        } else {
                            $(e.currentTarget).parent().parent().find(".input-surg-contact").attr("primaryfl", "");
                            $(e.currentTarget).parent().parent().find(".input-surg-contact").trigger("change");
                        }
                        break;
                }
            },
            editAddress: function (e) {
                var that = this;
                e.preventDefault();
                $("#surgeon-address-popup").removeClass("hide");
                var addIndex = $(e.currentTarget).parent().find("input").attr("addIndex");
                var addid = $(e.currentTarget).parent().find("input").attr("addid");
                var addrtyp = $(e.currentTarget).parent().find("input").attr("addrtyp");
                var addrtypnm = $(e.currentTarget).parent().find("input").attr("addrtyp");
                var add1 = $(e.currentTarget).parent().find("input").attr("add1");
                var add2 = $(e.currentTarget).parent().find("input").attr("add2");
                var zip = $(e.currentTarget).parent().find("input").attr("zip");
                var city = $(e.currentTarget).parent().find("input").attr("city");
                var statenm = $(e.currentTarget).parent().find("input").attr("statenm");
                var stateid = $(e.currentTarget).parent().find("input").attr("stateid");
                var countrynm = $(e.currentTarget).parent().find("input").attr("countrynm");
                var countryid = $(e.currentTarget).parent().find("input").attr("countryid");
                var primaryfl = $(e.currentTarget).parent().find("input").attr("primaryfl");
                var targetId = $(e.currentTarget).attr("id");
                var newAddIndex = this.model.get("arrgmsurgeonsaddressvo").length;
                newAddIndex++;
                if (targetId == "surgeon-edit-address")
                    var addData = {
                        "addIndex": addIndex,
                        "addid": addid,
                        "addrtyp": addrtyp,
                        "addrtypnm": addrtypnm,
                        "add1": add1,
                        "add2": add2,
                        "zip": zip,
                        "city": city,
                        "statenm": statenm,
                        "stateid": stateid,
                        "countryid": countryid,
                        "countrynm": countrynm,
                        "primaryfl": primaryfl
                    };
                else
                    var addData = {
                        "addIndex": newAddIndex,
                        "addid": "",
                        "addrtyp": "",
                        "addrtypnm": "",
                        "add1": "",
                        "add2": "",
                        "city": "",
                        "zip": "",
                        "statenm": "",
                        "stateid": "",
                        "countryid": "",
                        "countrynm": "",
                        "primaryfl": ""
                    };
                $("#surgeon-address-popup").html(this.template_address_list(addData));
                if(addData.countryid == "1101" || addData.countryid == ""){
                                var zipnm = $(".ul-phone-surgeon-address-zip li:first-child").text().replace("*","");
                                var statenm = $(".ul-surgeon-address-state li:first-child").text().replace("*","");
                                $(".ul-phone-surgeon-address-zip li:first-child").text("");
                                $(".ul-surgeon-address-state li:first-child").text("");
                                $(".ul-phone-surgeon-address-zip li:first-child").text("* " + zipnm);
                                $(".ul-surgeon-address-state li:first-child").text("* " + statenm);
                            }
                            else{
                              
                                var zipnm = $(".ul-phone-surgeon-address-zip li:first-child").text().replace("*","");
                                var statenm = $(".ul-surgeon-address-state li:first-child").text().replace("*","");
                                $(".ul-phone-surgeon-address-zip li:first-child").text(zipnm);
                                $(".ul-surgeon-address-state li:first-child").text(statenm);
                            }
                $(".surg-address-primaryfl-Y").prop("checked", true);
                this.getSelectBoxData({
                    "type": "address"
                });

                $("#surg-address-cancel").unbind("click").bind("click", function () {
                    $("#surgeon-address-popup").html("");
                    $("#surgeon-address-popup").addClass("hide");
                });
                $("#surg-address-done").unbind("click").bind("click", function (e) {
                    that.updateAddressVO(e);
                });
                $(".div-module-country, .div-module-state").unbind("click").bind("click", function (e) {
                    that.contactPopUps(e);
                });
            },
            contactPopUps: function (event) {
                var that = this;
                var item = "";
                var reData = [];
                var inputParameters = $(event.currentTarget).attr("data-inputParameters");

                if ($(event.currentTarget).hasClass("div-module-state")) {
                    item = "state";
                    reData = that.addrState;
                } else if ($(event.currentTarget).hasClass("div-module-country")) {
                    item = "country";
                    reData = that.addrCountry;
                } else if ($(event.currentTarget).hasClass("surg-phone-contype")) {
                    item = "contype";
                    var currType = $(event.currentTarget).attr("contype");
                    var currConMode = $(event.currentTarget).attr("conmode");
                    inputParameters = {
                        codegrp: "ADDTP",
                        codeaccid: currConMode
                    };
                }
                var index = $(".div-module-" + item).index($(event.currentTarget));
                that.systemSelectOptions = {
                    'title': $(event.currentTarget).attr("data-title"),
                    'storagekey': $(event.currentTarget).attr("data-storagekey"),
                    'webservice': $(event.currentTarget).attr("data-webservice"),
                    'resultVO': $(event.currentTarget).attr("data-resultVO"),
                    'IDParameter': $(event.currentTarget).attr("data-IDParameter"),
                    'NameParameter': $(event.currentTarget).attr("data-NameParameter"),
                    'inputParameters': inputParameters,
                    'codegrp': $(event.currentTarget).attr("data-codegrp"),
                    'module': "surgeon",
                    'event': event,
                    'type': 2,
                    'renderOne': "",
                    'parent': this,
                    'moduleLS': "",
                    'moduleCLS': "",
                    'callback': function (resid, resnm) {
                        console.log(resid, resnm);
                        if (item == "state") {
                            $(event.currentTarget).attr("statenm", resnm);
                            $(event.currentTarget).attr("stateid", resid);
                        } else if (item == "country") {
                            $(event.currentTarget).attr("countryid", resid);
                            $(event.currentTarget).attr("countrynm", resnm);
                            if(resid == "1101"){
                                var zipnm = $(".ul-phone-surgeon-address-zip li:first-child").text().replace("*","");
                                var statenm = $(".ul-surgeon-address-state li:first-child").text().replace("*","");
                                $(".ul-phone-surgeon-address-zip li:first-child").text("");
                                $(".ul-surgeon-address-state li:first-child").text("");
                                $(".ul-phone-surgeon-address-zip li:first-child").text("* " + zipnm);
                                $(".ul-surgeon-address-state li:first-child").text("* " + statenm);
                            }
                            else{
                                var zipnm = $(".ul-phone-surgeon-address-zip li:first-child").text().replace("*","");
                                var statenm = $(".ul-surgeon-address-state li:first-child").text().replace("*","");
                                $(".ul-phone-surgeon-address-zip li:first-child").text(zipnm);
                                $(".ul-surgeon-address-state li:first-child").text(statenm);
                            }
                        } else if (item == "contype") {
                            if (resnm == "")
                                resnm = currType;
                            $(event.currentTarget).attr("contype", resnm);
                            $(event.currentTarget).attr("contypeid", resid);
                            that.changeContact(event, resid, resnm);
                        }
                        $(event.currentTarget).html(resnm);
                    }
                };
                that.systemSelectView = new GMVCRMSelectPopup(that.systemSelectOptions);
            },

            updateAddressVO: function (e) {
                var that = this;
                var index = $(e.currentTarget).parent().parent().find("input").attr("addIndex");
                var addid = $(e.currentTarget).parent().parent().find("input").val();
                var addrtyp = $(e.currentTarget).parent().parent().find("#select-surgeon-addrtype option:selected").val();
                var addrtypnm = $(e.currentTarget).parent().parent().find("#select-surgeon-addrtype option:selected").text();
                var add1 = $(e.currentTarget).parent().parent().find("#surg-address-add1").val();
                var add2 = $(e.currentTarget).parent().parent().find("#surg-address-add2").val();
                var city = $(e.currentTarget).parent().parent().find("#surg-address-city").val();
                var statenm = $(e.currentTarget).parent().parent().find("#surg-address-state").attr("statenm");
                var stateid = $(e.currentTarget).parent().parent().find("#surg-address-state").attr("stateid");
                var countrynm = $(e.currentTarget).parent().parent().find("#surg-address-country").attr("countrynm");
                var countryid = $(e.currentTarget).parent().parent().find("#surg-address-country").attr("countryid");
                var zip = $(e.currentTarget).parent().parent().find("#surg-address-zip").val();
                var primaryfl = "";
                var voidfl = $(e.currentTarget).parent().parent().find("#surg-address-voidfl").val();
                if ($(e.currentTarget).parent().parent().find("#surg-address-primaryfl").prop("checked"))
                    primaryfl = "Y";

                var oldData = _.findWhere(this.model.get("arrgmsurgeonsaddressvo"), function (dt) {
                    return dt.index == index;
                });
                var valAddress = true;

                if (parseNull(addrtyp) == "") {
                    valAddress = false;
                    $("#select-surgeon-addrtype").addClass("gmValidationError");
                }
                if (parseNull(add1) == "") {
                    valAddress = false;
                    $("#surg-address-add1").addClass("gmValidationError");
                }
                if (parseNull(city) == "") {
                    valAddress = false;
                    $("#surg-address-city").addClass("gmValidationError");
                }
                if(parseNull(countryid) =='1101' || parseNull(countryid) == ""){
                if (parseNull(zip) == "") {
                    valAddress = false;
                    $("#surg-address-zip").addClass("gmValidationError");
                }else{
                     $("#surg-address-zip").removeClass("gmValidationError");
                }
                if (parseNull(stateid) == "") {
                    valAddress = false;
                    $("#surg-address-state").addClass("gmValidationError");
                }else{
                     $("#surg-address-state").removeClass("gmValidationError");
                }
                }
                else
                    {
                       $("#surg-address-zip").removeClass("gmValidationError"); 
                       $("#surg-address-state").removeClass("gmValidationError"); 
                    }

                if (parseNull(countryid) == "") {
                    valAddress = false;
                    $("#surg-address-country").addClass("gmValidationError");
                }
                else{
                     $("#surg-address-country").removeClass("gmValidationError");
                }
                

                if (parseNull(addrtyp) != "" || parseNull(city) != "" || parseNull(zip) != "" || parseNull(stateid) != "" || parseNull(countryid) != "") {
                    $("#div-addres-edit gmInputText").removeClass("gmValidationError");
                    $("#select-surgeon-addrtype").removeClass("gmValidationError");
                }

                if (valAddress == true) {
                    if (primaryfl == "Y") {
                        _.each(this.model.get("arrgmsurgeonsaddressvo"), function (dt) {
                            return dt.primaryfl = "";
                        });
                    }
                    if (oldData != undefined)
                        if (oldData.length != 0) {
                            var newData = _.reject(this.model.get("arrgmsurgeonsaddressvo"), function (dt) {
                                return dt.index == index;
                            });
                            this.model.set("arrgmsurgeonsaddressvo", newData);
                        }
                    this.model.get("arrgmsurgeonsaddressvo").push({
                        "index": index,
                        "addid": parseNull(addid),
                        "addrtyp": addrtyp,
                        "addrtypnm": addrtypnm,
                        "add1": add1,
                        "add2": add2,
                        "city": city,
                        "statenm": statenm,
                        "stateid": stateid,
                        "countrynm": countrynm,
                        "countryid": countryid,
                        "zip": zip,
                        "primaryfl": primaryfl,
                        "voidfl": parseNull(voidfl)
                    });
                    $("#surgeon-address-popup").addClass("hide");
                    this.updAddressDiv(this.model.attributes);
                } else {
                    $("#address-validation-msg-bar").html("Please enter mandatory(*) fields").css({
                        "background-color": "#700",
                        "padding": "10px",
                        "color": "#fff"
                    }).slideDown(500).fadeOut(10000);       
                }
            },

            // Function to remove address 
            removeAddress: function (e) {
                var addID = $(e.currentTarget).parent().find("input").attr("addid");
                var index = $(e.currentTarget).parent().find("input").attr("addIndex");
                if (parseNull(addID) == "") {
                    var rmArr = _.reject(this.model.get("arrgmsurgeonsaddressvo"), function (dt) {
                        return dt.index == index;
                    });
                } else {
                    var rmArr = _.each(this.model.get("arrgmsurgeonsaddressvo"), function (dt) {
                        if (dt.addid == addID)
                            dt.voidfl = "Y";
                    });
                }
                this.model.set("arrgmsurgeonsaddressvo", rmArr);
                $(e.currentTarget).parent().remove();
                this.updAddressDiv(this.model.attributes);
            },

            updAddressDiv: function (data) {
                $("#ul-surg-addres").html(this.template_SurgAddressList(data));
                $("#surgeon-address-primaryfl-Y").prop("checked", true);
            },

            // validate primary contact 
            valPrimary: function (e) {
                var that = this;
                var currType = $(e.currentTarget).attr("data-conttype");
                var conmode = $(e.currentTarget).attr("conmode");
                $(".surg-" + currType + "-cbox").not($(e.currentTarget)).prop('checked', false);
                this.changeContact(e);
            },

            // load surgoen list report by selected filter
            loadSurgeonReport: function (e) {
                var listBy = $(e.currentTarget).attr("data-listby");
                var currText = $(e.currentTarget).text();
                var reportKey = $(e.currentTarget).attr("data-key");
                var reportVal = $(e.currentTarget).attr("data-val");
                var reportKey2 = $(e.currentTarget).attr("data-key2");
                var reportVal2 = $(e.currentTarget).attr("data-val2");
                var reportKey3 = $(e.currentTarget).attr("data-key3");
                var reportVal3 = $(e.currentTarget).attr("data-val3");
                GM.Global.Surgeon.FilterID = {};
                GM.Global.Surgeon.FilterName = {};

                reportVal = reportVal.replace(/'/g, "''");

                GM.Global.Surgeon.FilterID[reportKey] = reportVal;
                if (parseNull(reportVal2) != "")
                    GM.Global.Surgeon.FilterID[reportKey2] = reportVal2;
                if (parseNull(reportVal3) != "")
                    GM.Global.Surgeon.FilterID[reportKey3] = reportVal3;
                console.log(currText);
                if (parseNull(currText) != "") {
                    GM.Global.Surgeon.FilterFl = "Y";
                    window.location.href = "#crm/surgeon/filter/report/" + listBy;
                }
            },

            // Close view
            closeView: function (view) {
                if (view != undefined) {
                    view.unbind();
                    view.undelegateEvents();
                    view.$el.unbind();
                    view.$el.empty();
                    view = undefined;
                    $("#btn-back").unbind();
                }
            },
            
            filterActivity: function (e) {
                var curName = $(e.currentTarget).attr("name");
                $(".btn-filter").find("div").removeClass("gmBGDarkerGrey gmFontWhite").addClass("gmBGLGrey");
                $(e.currentTarget).find("div").addClass("gmBGDarkerGrey gmFontWhite").removeClass("gmBGLGrey");
                if(curName == "all"){
                    $(".surgActivityList ").removeClass("hide");
                    $(".surgActivityList").find(".surg-globus-activty").show();
                    $(".surgActivityList").last().find(".surg-globus-activty").hide();
                }else{
                     $(".surgActivityList ").addClass("hide");
                     $("."+curName+"-filter").removeClass("hide");
                     $("."+curName+"-filter").last().find(".surg-globus-activty").hide();
                }
            }

        });
        return GM.View.SurgeonDetail;
    });
