/**********************************************************************************
 * File:        GmSurgeonModel.js
 * Description: Model to represent Surgeon overall data
 * Version:     1.0
 * Author:     
 **********************************************************************************/
define([

    //External Libraries
    'underscore', 'backbone',

    //Common
    'global',

    //Collection
    'gmcItem', 'gmcAddress'
],

    function (_, Backbone,
        Global, GMCItem, GMCAddress
    ) {

        'use strict';

        GM.Model.Surgeon = Backbone.Model.extend({

            // Default values of the model
            defaults: {
                "partyid": "",
                "npid": "",
                "firstnm": "",
                "lastnm": "",
                "partynm": "",
                "midinitial": "",
                "asstnm": "",
                "mngrnm": "",
                "companyid": "",
                "arrgmsurgeonsaddressvo": "",
                "arrgmsurgeoncontactsvo": "",
                "arrgmcrmsurgeonaffiliationvo": "",
                "arrgmcrmsurgeonactivityvo": "",
                "arrgmcrmsurgeonrepvo": "",
                "arrgmcrmfileuploadvo": "",
                "arrgmcrmeducationvo": "",
                "arrgmcrmprofessionalvo": "",
                "arrgmcrmsurgeonsocialvo": "",
                "voidfl": "",
                "activefl": ""
            },

            //Initial configurations
            initialize: function (data) {
                if (data != undefined) { //setting the models based on data
                    if (data.arrgmsurgeonsaddressvo == undefined)
                        data.arrgmsurgeonsaddressvo = [];
                    if (data.arrgmcrmsurgeonrepvo == undefined)
                        data.arrgmcrmsurgeonrepvo = [];
                    if (data.arrgmcrmsurgeonaffiliationvo == undefined)
                        data.arrgmcrmsurgeonaffiliationvo = [];
                    if (data.arrgmcrmsurgeonactivityvo == undefined)
                        data.arrgmcrmsurgeonactivityvo = [];
                    if (data.arrgmcrmfileuploadvo == undefined)
                        data.arrgmcrmfileuploadvo = [];
                    if (data.arrgmsurgeoncontactsvo == undefined)
                        data.arrgmsurgeoncontactsvo = [];
                    if (data.arrgmcrmeducationvo == undefined)
                        data.arrgmcrmeducationvo = [];
                    if (data.arrgmcrmprofessionalvo == undefined)
                        data.arrgmcrmprofessionalvo = [];
                    if (data.arrgmcrmsurgeonsocialvo == undefined)
                        data.arrgmcrmsurgeonsocialvo = [];
                    this.set("arrgmsurgeonsaddressvo", new arrayOf(data.arrgmsurgeonsaddressvo));
                    this.set("arrgmcrmsurgeonrepvo", new arrayOf(data.arrgmcrmsurgeonrepvo));
                    this.set("arrgmcrmsurgeonaffiliationvo", new arrayOf(data.arrgmcrmsurgeonaffiliationvo));
                    this.set("arrgmcrmfileuploadvo", new arrayOf(data.arrgmcrmfileuploadvo));
                    this.set("arrgmsurgeoncontactsvo", new arrayOf(data.arrgmsurgeoncontactsvo));
                    this.set("arrgmcrmsurgeonactivityvo", new arrayOf(data.arrgmcrmsurgeonactivityvo));
                    this.set("arrgmcrmeducationvo", new arrayOf(data.arrgmcrmeducationvo));
                    this.set("arrgmcrmprofessionalvo", new arrayOf(data.arrgmcrmprofessionalvo));
                    this.set("arrgmcrmsurgeonsocialvo", new arrayOf(data.arrgmcrmsurgeonsocialvo));
                } else {
                    this.set("arrgmsurgeonsaddressvo", new Array());
                    this.set("arrgmcrmsurgeonrepvo", new Array());
                    this.set("arrgmcrmsurgeonaffiliationvo", new Array());
                    this.set("arrgmcrmfileuploadvo", new Array());
                    this.set("arrgmsurgeoncontactsvo", new Array());
                    this.set("arrgmcrmsurgeonactivityvo", new Array());
                    this.set("arrgmcrmeducationvo", new Array());
                    this.set("arrgmcrmprofessionalvo", new Array());
                    this.set("arrgmcrmsurgeonsocialvo", new Array());
                }
            },


            //to set those data that alone are editable
            getEditableData: function () {
                var surgeon = $.extend(true, {}, this);
            },

            //setting all surgeon's models without Vo names based on template classes
            setModalValue: function (elem) {
                var model = $.extend(true, {}, this);
                var that = this;
                _.each(model.keys(), function (attribute) {
                    if ($(elem).find('.data-' + attribute).get(0) != undefined)
                        that.set(attribute, $(elem).find('.data-' + attribute).val());
                });
            },

            //setting all surgeon's models with Vo names based on template classes
            setValues: function (elem, selector, attrib) {
                var that = this;
                this.get(attrib).reset();
                this.get(attrib).add({});
                var model = this.get(attrib).pop();
                $(elem).find(selector).each(function () {
                    var jsnModel = {};
                    var target = this;
                    _.each(model.keys(), function (attribute) {
                        jsnModel[attribute] = $(target).find('.data-' + attribute).val();
                    });
                    that.get(attrib).add(jsnModel);
                });
            },

            //to get final model data as JSON
            getFinalData: function () {
                var finalModel = $.extend(true, {}, this);
                finalModel.unset('userid');
                finalModel.unset('token');
                finalModel.unset('stropt');
                finalModel.unset('adid');
                finalModel.unset('adname');
                finalModel.unset('repid');
                finalModel.unset('repname');
                finalModel.unset('speciality');
                finalModel.unset('Hospitalnm');
                finalModel.unset('arrgmcrmsurgeonactivityvo'); //to handel addressVo with only add1 data
                if (this.get("arrgmsurgeonsaddressvo").at(0) != undefined) {
                    if (this.get("arrgmsurgeonsaddressvo").at(0).get("add1") == "")
                        this.get("arrgmsurgeonsaddressvo").at(0).destroy();
                }
                finalModel.set("arrgmsurgeonsaddressvo", this.get("arrgmsurgeonsaddressvo"));
                finalModel.set("arrgmcrmsurgeonrepvo", this.get("arrgmcrmsurgeonrepvo"));
                finalModel.set("arrgmcrmsurgeonaffiliationvo", this.get("arrgmcrmsurgeonaffiliationvo"));
                finalModel.set("arrgmcrmfileuploadvo", this.get("arrgmcrmfileuploadvo"));
                finalModel.set("arrgmsurgeoncontactsvo", this.get("arrgmsurgeoncontactsvo"));
                finalModel.set("arrgmcrmeducationvo", this.get("arrgmcrmeducationvo"));
                finalModel.set("arrgmcrmprofessionalvo", this.get("arrgmcrmprofessionalvo"));
                finalModel.set("arrgmcrmsurgeonsocialvo", this.get("arrgmcrmsurgeonsocialvo"));
                return finalModel.toJSON();
            }

        });

        return GM.Model.Surgeon;

    });
