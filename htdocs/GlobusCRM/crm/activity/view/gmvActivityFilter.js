define([
     // External Libraries
    'jquery', 'underscore', 'backbone', 'handlebars', 'jqueryui',
    
     //Common
    'global',
    
    //View
    'gmvCRMSelectPopup',

    // Pre-Compiled Template
    'gmtTemplate'
], 

function ($, _, Backbone, Handlebars, JqueryUI,
           Global,
           GMVCRMSelectPopup, GMTTemplate) {

    'use strict';
    
    GM.View.ActivityFilter = Backbone.View.extend({

        el: "#div-crm-module",

        //Load the templates
        template_MainModule: fnGetTemplate(URL_Common_Template, "gmtCRMMainModule"),
        template_MERCFilter: fnGetTemplate(URL_MERC_Template, "gmtMERCFilter"),
        template_LeadFilter: fnGetTemplate(URL_Lead_Template, "gmtLeadFilter"),
        template_PhysicianVisitFilter: fnGetTemplate(URL_PhysicianVisit_Template, "gmtPhysicianVisitFilter"),
        template_SalesCallFilter: fnGetTemplate(URL_SalesCall_Template, "gmtSalesCallFilter"),
        template_SurgeonFilter: fnGetTemplate(URL_Surgeon_Template, "gmtSurgeonFilter"),
        template_TrainingFilter: fnGetTemplate(URL_Training_Template, "gmtTrainingFilter"),
        template_CRFFilter: fnGetTemplate(URL_CRF_Template, "gmtCRFFilter"),
        template_FieldSalesFilter: fnGetTemplate(URL_FieldSales_Template, "gmtFieldSalesFilter"),
        template_selected_items: fnGetTemplate(URL_Common_Template, "gmtPopupSelectedItem"),
        
        events: {
            "click #btn-main-save": "saveFilter",
            "click .spn-name-x": "removeName",
            "click #btn-main-cancel": "close",
            "click  .btn-listby"             : "openPopup",
            "click #btn-category-search": "searchFilter",
            "click #btn-filter-selected-criteria": "showSelectedCriteria",
            "change #activity-frmdate, #activity-todate": "setMinDate",
            "click .div-text-selected-crt":  "toggleSelectedItems"
        },

	    initialize: function(options) {
                console.log(options.filterid);
                $(window).on('orientationchange', this.onOrientationChange);
                this.filterID = options.filterid;
                hideMessages();
                this.arrSelectedList = new Array();
                this.tempJSON = {};
                GM.Global.Activity = {};
                GM.Global.Activity.FilterID = {};
                GM.Global.Activity.FilterName = {};
                GM.Global.myFilter = null;
//                GM.Global.Activity.FilterID = null;
//                GM.Global.Activity.FilterName = null;
                this.module = options.module;

        },
        
         // Render the initial contents when the Activity Filter View is instantiated
        render: function() {
            
             var that = this;
//            $("#txt-mobile-title").css({"display":"inline-block"});
            this.$el.html(this.template_MainModule({"module": this.module}));
            console.log(this.module);
//            $("#txt-mobile-title").html(this.module);
            switch (this.module) {
                case "fieldsales":
                    this.template = this.template_FieldSalesFilter({userID: localStorage.getItem("userID")});
                    break;
                case "lead":
                    this.template = this.template_LeadFilter();
                    break;
                case "merc":
                    this.template = this.template_MERCFilter();
                    break;
                case "physicianvisit":
                    this.template = this.template_PhysicianVisitFilter();
                    break;
                case "salescall":
                    this.template = this.template_SalesCallFilter();
                    break;
                case "surgeon":
                    this.template = this.template_SurgeonFilter();
                    break;
                case "training":
                    this.template = this.template_TrainingFilter();
                    break;
                case "crf":
                    this.template = this.template_CRFFilter();
                    break;
            }

            this.$("#div-main-content").html(this.template);
            this.$(".gmPanelTitle").html("Lookup " + GM.Global.Title);
            
            this.$(".gmBtnGroup #li-save").removeClass("hide");
            this.$(".gmBtnGroup #li-cancel").removeClass("hide");
            this.jsnData = {};
            this.jsnData.criteriatyp = this.module;
            console.log(this.jsnData.criteriatyp);
            this.jsnData.token = localStorage.getItem('token');

            this.stateList = new Array();
            this.productsList = new Array();
            this.techqList = new Array();
            this.skillList = new Array();
            this.roleList = new Array();
            this.activityList = new Array();
            this.statusList=new Array();
            this.crfstatusList=new Array();
            this.RegionList=new Array();
            if(this.filterID !=0)
                this.getFilterData();
            resizeHeadWidth('.gmPanelHead', '.gmBtnGroup', '#panelhead-left');
            return this;
        },
        
        //load the data if edit into the view
        getFilterData: function () {
            this.arrAttribute = new Array();
            
            var that = this;
            var input = { "token": localStorage.getItem("token"), "criteriaid": this.filterID };
          
                if(GM.Global.SaveDB)
                    GM.Global.SaveDB = undefined;
                fnGetWebServerData("criteria/get", undefined, input, function (data) {
                    if(data!=null)
                        that.criteriaData(data);
                });
            
            
        },
        
        criteriaData: function(data){
            var that = this;
            console.log(JSON.stringify(data));
            console.log(data)
            that.$(".gmPanelTitle").html(data.criterianm);
            that.$("#div-salescall-phone-title").html(data.criterianm);
            that.$(".txt-fltr-name").val(data.criterianm);

            GM.Global.myFilter = null;
            GM.Global.myFilter= JSON.stringify({"Name":data.criterianm,"ID":data.criteriaid});
            that.jsnData.criteriaid = data.criteriaid;
            data.listGmCriteriaAttrVO = arrayOf(data.listGmCriteriaAttrVO);
            GM.Global.criteriaAttr = [];
            GM.Global.criteriaAttr = data.listGmCriteriaAttrVO;
            if(data.listGmCriteriaAttrVO[0] != undefined )
            _.each(data.listGmCriteriaAttrVO, function (data) {

                if (data.key == "actstartdate") {

                    that.frmdate = data.value;
                    console.log(that.frmdate);
                    that.$("#activity-frmdate").val(data.value);
                    var frmdate = new Date(data.value);
                    var day = ("0" + frmdate.getDate()).slice(-2);
                    var month = ("0" + (frmdate.getMonth() + 1)).slice(-2);
                    var actfrmdate = frmdate.getFullYear()+"-"+(month)+"-"+(day) ;
                    that.$("#activity-frmdate").val(actfrmdate);
                    that.tempJSON["From Date"] = that.frmdate;
                }
                if (data.key == "actenddate") {

                    that.todate = data.value;
                    that.$("#activity-todate").val(data.value);
                    var todate = new Date(data.value);
                    var day = ("0" + todate.getDate()).slice(-2);
                    var month = ("0" + (todate.getMonth() + 1)).slice(-2);
                    var crttomdate = todate.getFullYear()+"-"+(month)+"-"+(day) ;
                    that.$("#activity-todate").val(crttomdate);
                    that.tempJSON["To Date"] = that.todate;
                }
                if (data.key == "crffrmdate") {

                    that.frmdate = data.value;
                    console.log(that.frmdate);
                    that.$("#activity-frmdate").val(data.value);
                    var frmdate = new Date(data.value);
                    var day = ("0" + frmdate.getDate()).slice(-2);
                    var month = ("0" + (frmdate.getMonth() + 1)).slice(-2);
                    var actfrmdate = frmdate.getFullYear()+"-"+(month)+"-"+(day) ;
                    that.$("#activity-frmdate").val(actfrmdate);
                    that.tempJSON["From Date"] = that.frmdate;
                }
                if (data.key == "crftodate") {

                    that.todate = data.value;
                    that.$("#activity-todate").val(data.value);
                    var todate = new Date(data.value);
                    var day = ("0" + todate.getDate()).slice(-2);
                    var month = ("0" + (todate.getMonth() + 1)).slice(-2);
                    var crttomdate = todate.getFullYear()+"-"+(month)+"-"+(day) ;
                    that.$("#activity-todate").val(crttomdate);
                    that.tempJSON["To Date"] = that.todate;
                }

               if (data.key == "systemid") {
                    that.productsList.push(data.value);
                }

                if (data.key == "state") {
                    that.stateList.push(data.value);
                }
                if (data.key == "procedure") {
                    that.techqList.push(data.value);
                }
                if (data.key == "specality") {
                    that.skillList.push(data.value);
                }
                if (data.key == "acttyp") {
                    that.activityList.push(data.value);
                }
                if (data.key == "role") {
                    that.roleList.push(data.value);
                }
                 if (data.key == "acttypeid") {
                    that.activityList.push(data.value);
                }
                if(data.key =="actstatus"){
                   that.statusList.push(data.value); 
                }
                if(data.key =="crfstatus"){
                   that.crfstatusList.push(data.value); 
                }

                if(data.key =="regid"){
                    console.log("region");
                    that.RegionList.push(data.value);

                }   
                that.newArray = _.union(that.stateList, that.techqList, that.skillList, that.activityList, that.roleList, that.activityList,that.productsList,that.statusList,that.crfstatusList,that.RegionList);
            });

            var saveLocalJSON = {};
            if(that.productsList.toString()!="")
                saveLocalJSON["systemid"] = that.productsList.toString();
            if(that.stateList.toString()!="")
                saveLocalJSON["state"] = that.stateList.toString();
            if(that.techqList.toString()!="")
                saveLocalJSON["procedure"] = that.techqList.toString();
            if(that.skillList.toString()!="")
                saveLocalJSON["specality"] = that.skillList.toString();
            if(that.activityList.toString()!="")
                saveLocalJSON["acttypeid"] = that.activityList.toString();
            if(that.statusList.toString()!="")
                saveLocalJSON["actstatus"] = that.statusList.toString();
            if(that.crfstatusList.toString()!="")
                saveLocalJSON["crfstatus"] = that.crfstatusList.toString();
            if(that.RegionList.toString()!="")
                saveLocalJSON["regid"] = that.RegionList.toString();
            if(that.roleList.toString()!="")
                saveLocalJSON["role"] = that.roleList.toString();

            if(that.frmdate!=undefined && that.frmdate.toString()!="")
                saveLocalJSON["actstartdate"] = that.frmdate;
            if(that.todate!=undefined && that.frmdate.toString()!="")
                saveLocalJSON["actenddate"] = that.todate;

            GM.Global.Activity.FilterID = saveLocalJSON;
            console.log(saveLocalJSON)
            console.log(that.newArray);
            if(that.newArray != "")
                    that.getCodeName(that.newArray);
            else if(that.productsList != "")
                that.getSystemName(that.productsList);
            else
                that.popUpTemplate();

            _.each(data.listGmCriteriaAttrVO, function(data){
                delete data.userid;
                delete data.token;
                delete data.stropt;
            });    
        },
        
        getSystemName: function (sysid) {
            var that = this;
            var sysid = sysid.toString();
            var systemInput = { "token": localStorage.getItem("token"), "sysid": sysid };
            fnGetWebServerData("search/system", "gmSystemBasicVO", systemInput, function (data) {
                data = arrayOf(data);
                if (sysid != undefined) {
                    var systemList = _.pluck(data, "sysnm");
                    var systemID = _.pluck(data, "sysid");
                    that.tempJSON["Product Used"] = systemList.toString().replace(/,/g," / ");

                    if (systemList != null) {
                        var countVal = systemList.length;
                        that.defaultVals = systemID;
                        $(".div-popup-system").attr("data-values", that.defaultVals);
                        $(".div-popup-system").find("span").text("(" + countVal + ")");
                        $(".div-popup-system").css("background", "#FDF5E6");
                    }
                }
                that.popUpTemplate();
            });          
        },
        getCodeName: function (codeid) {
            var that = this;
            var codeid = codeid.toString();
            if(codeid != "") {
                var input = { "token": localStorage.getItem("token"), "codeid": codeid };
                fnGetWebServerData("search/codelookup", "gmCodeLookUpVO", input, function (data) {

                    data = arrayOf(data);
                    if (that.stateList != "") {
                        var stateResult = _.filter(data, function (data) {
                            return data.codegrp == "STATE";
                        });
                        var stateList = _.pluck(arrayOf(stateResult), "codenm");
                        console.log(stateList.toString().replace(/,/g," / "));
                        var stateID = _.pluck(arrayOf(stateResult), "codeid");
                        that.tempJSON["Location"] = stateList.toString().replace(/,/g," / ");
                        var countVal = stateResult.length;
                        that.defaultVals = stateID;
                        $(".div-popup-state").attr("data-values", that.defaultVals);
                        $(".div-popup-state").find("span").text("(" + countVal + ")");
                        $(".div-popup-state").css("background", "#FDF5E6");

                    }
                    if (that.techqList != "") {
                        var techqResult = _.filter(data, function (data) {
                            return data.codegrp == "TECHQ";
                        });
                        var techqList = _.pluck(arrayOf(techqResult), "codenm");
                        var techqID = _.pluck(arrayOf(techqResult), "codeid");
                        that.tempJSON["Technique"] = techqList.toString().replace(/,/g," / ");
                        var countVal = techqResult.length;
                        that.defaultVals = techqID;
                        $(".div-popup-technique").attr("data-values", that.defaultVals);
                        $(".div-popup-technique").find("span").text("(" + countVal + ")");
                        $(".div-popup-technique").css("background", "#FDF5E6");
                    }
                    if (that.skillList != "") {
                        var specalityResult = _.filter(data, function (data) {
                            return data.codegrp == "SGSPC";
                        });
                        var specalityList = _.pluck(arrayOf(specalityResult), "codenm");
                        var specalityID = _.pluck(arrayOf(specalityResult), "codeid");
                        that.tempJSON["Skillset"] = specalityList.toString().replace(/,/g," / ");
                        var countVal = specalityResult.length;
                        that.defaultVals = specalityID;
                        $(".div-popup-skills").attr("data-values", that.defaultVals);
                        $(".div-popup-skills").find("span").text("(" + countVal + ")");
                        $(".div-popup-skills").css("background", "#FDF5E6");
                    }
                    if (that.roleList != "") {
                        var roleResult = _.filter(data, function (data) {
                            return data.codegrp == "ACTROL";
                        });
                        var roleList = _.pluck(arrayOf(roleResult), "codenm");
                        var roleID = _.pluck(arrayOf(roleResult), "codeid");
                        that.tempJSON["Role"] = roleList.toString().replace(/,/g," / ");
                        var countVal = roleResult.length;
                        that.defaultVals = roleID;
                        $(".div-popup-role").attr("data-values", that.defaultVals);
                        $(".div-popup-role").find("span").text("(" + countVal + ")");
                        $(".div-popup-role").css("background", "#FDF5E6");
                    }
                    if (that.activityList != "") {
                        var acttypResult = _.filter(data, function (data) {
                            return data.codegrp == "SGACTP";
                        });
                        var acttypList = _.pluck(arrayOf(acttypResult), "codenm");
                        var acttypID = _.pluck(arrayOf(acttypResult), "codeid");
                        that.tempJSON["Activity Type"] = acttypList.toString().replace(/,/g," / ");
                        var countVal = acttypResult.length;
                        that.defaultVals = acttypID;
                        $(".div-popup-activity").attr("data-values", that.defaultVals);
                        $(".div-popup-activity").find("span").text("(" + countVal + ")");
                        $(".div-popup-activity").css("background", "#FDF5E6");
                    }
                    if (that.statusList != "") {
                     
                        var statusResult = _.filter(data, function (data) {
                            return data.codegrp == "CRFSTS";
                        });
                        var statusList = _.pluck(arrayOf(statusResult), "codenm");
                        var statusID = _.pluck(arrayOf(statusResult), "codeid");
                        that.tempJSON["CRF Status"] = statusList.toString().replace(/,/g," / ");
                        var countVal = statusResult.length;
                        that.defaultVals = statusID;
                        $(".div-popup-status").attr("data-values", that.defaultVals);
                        $(".div-popup-status").find("span").text("(" + countVal + ")");
                        $(".div-popup-status").css("background", "#FDF5E6");
                    }
                    if (that.crfstatusList != "") {
                      
                        var statusResult = _.filter(data, function (data) {
                            return data.codegrp == "CRFSTS";
                        });
                        var crfstatusList = _.pluck(arrayOf(statusResult), "codenm");
                        var statusID = _.pluck(arrayOf(statusResult), "codeid");
                        that.tempJSON["CRF Status"] = crfstatusList.toString().replace(/,/g," / ");
                        var countVal = statusResult.length;
                        that.defaultVals = statusID;
                        $(".div-popup-status").attr("data-values", that.defaultVals);
                        $(".div-popup-status").find("span").text("(" + countVal + ")");
                        $(".div-popup-status").css("background", "#FDF5E6");
                    }
                    if (that.RegionList != "") {
                        var RegionResult = _.filter(data, function (data) {
                            return data.codegrp == "REGN";
                        });
                        var RegionList = _.pluck(arrayOf(RegionResult), "codenm");
                        var RegionID = _.pluck(arrayOf(RegionResult), "codeid");
                        that.tempJSON["Activity Type"] = RegionList.toString().replace(/,/g," / ");
                        var countVal = RegionResult.length;
                        that.defaultVals = RegionID;
                        console.log(that.defaultVals);
                        $(".div-popup-region").attr("data-values", that.defaultVals);
                        $(".div-popup-region").find("span").text("(" + countVal + ")");
                        $(".div-popup-region").css("background", "#FDF5E6");
                    }
                    
                    if (that.productsList != ""){
                        that.getSystemName(that.productsList);
                    }
                    else {
                        that.popUpTemplate();
                    }
                }); 
            }
            else if (that.productsList != ""){
                constant.log(that.productsList);
                that.getSystemName(that.productsList);
            }
        },
        
        popUpTemplate: function () {
            var that = this;
            var tempArray = new Array();
            GM.Global.Activity.FilterName = JSON.stringify(that.tempJSON); 
            console.log(GM.Global.Activity.FilterName)
            console.log(JSON.stringify(that.tempJSON))
            $.each(that.tempJSON, function (key, val) {
                if (val != "") {
                    tempArray.push({ "title": key, "value": val });
                }
            });
            var criteriaValues = $.parseJSON(JSON.stringify(tempArray));
            if(criteriaValues != ""){
                $(".list-category-item").html(that.template_selected_items(criteriaValues));
                if(GM.Global.SwitchUser)
                    $(".div-criteria").addClass("hide");
            }
                
            $(".div-text-selected-crt").addClass("gmFont"+this.module);
          
        },

        //cancels the filterview and redirects to criteria view
        close: function() {
            window.history.back();
        },
        openPopup: function(event){
            var that = this;
            console.log( GM.Global.Activity.FilterID);
            console.log(GM.Global.Activity.FilterName);
            that.systemSelectOptions = {                                                                                                              
                'title': $(event.currentTarget).attr("data-title"),
                'storagekey': $(event.currentTarget).attr("data-storagekey"),
                'webservice': $(event.currentTarget).attr("data-webservice"),   
                'resultVO': $(event.currentTarget).attr("data-resultVO"),
                'IDParameter': $(event.currentTarget).attr("data-IDParameter"),
                'NameParameter': $(event.currentTarget).attr("data-NameParameter"),
                'inputParameters': $(event.currentTarget).attr("data-inputParameters"),
                'module': this.module,
                'event': event,
                'parent': this,
                'moduleLS': GM.Global.Activity.FilterID,
                'moduleCLS': GM.Global.Activity.FilterName,
                'callback': function(ID, Name){
                    if(ID == "{}" && Name =="{}"){
                        showError("Please select to save favorite");
                    GM.Global.FilterValue = "true";
                    }
                    else
                    {        
                        GM.Global.FilterValue = "false"; 
                    GM.Global.Activity.FilterID = ID;
                    GM.Global.Activity.FilterName = Name;
                    console.log(ID);
                    console.log(Name)
                    }
                }
            };
            
            that.systemSelectView = new GMVCRMSelectPopup(that.systemSelectOptions);
        },
        
        
        
        
         activityDate: function () {
            var that = this;
            var fromdate = $("#activity-frmdate").val();
            var todate = $("#activity-todate").val();
            console.log(todate)
            
            if(fromdate !=undefined && todate !=undefined && fromdate !="" && todate !="") {
                if (GM.Global.Activity.FilterID == undefined)
                    GM.Global.Activity.FilterID = {};
                
                if(GM.Global.Activity.FilterName == undefined)
                    GM.Global.Activity.FilterName = {};
                
                if(GM.Global.Activity.FilterName!="" && typeof(GM.Global.Activity.FilterName)!="object")
                    GM.Global.Activity.FilterName = $.parseJSON(GM.Global.Activity.FilterName);
                if(GM.Global.Activity.FilterID!="" && typeof(GM.Global.Activity.FilterID)!="object")
                    GM.Global.Activity.FilterID = $.parseJSON(GM.Global.Activity.FilterID);
                 console.log(GM.Global.Activity.FilterID);
                if(this.module == "crf"){
                        GM.Global.Activity.FilterID["crffrmdate"] = getDateByFormat(fromdate);
                        GM.Global.Activity.FilterName["From Date"] = getDateByFormat(fromdate);
                        GM.Global.Activity.FilterID["crftodate"] = getDateByFormat(todate);
                        GM.Global.Activity.FilterName["To Date"] = getDateByFormat(todate);
                }
                else{
                        GM.Global.Activity.FilterID["actstartdate"] = getDateByFormat(fromdate);
                        GM.Global.Activity.FilterName["From Date"] = getDateByFormat(fromdate);
                        GM.Global.Activity.FilterID["actenddate"] = getDateByFormat(todate);
                        GM.Global.Activity.FilterName["To Date"] = getDateByFormat(todate);
                    } 
            }
             console.log(GM.Global.Activity.FilterID);
        },    
        
        // Save currently selected data as favorite criteria
        saveFilter: function (event) {
            if(GM.Global.FilterValue == "false"){
            this.activityDate();
            var that =this;
            
            console.log(GM.Global.Activity.FilterID);
//            console.log($.parseJSON(GM.Global.Activity.FilterID));
            if(GM.Global.Activity.FilterID!="" && typeof(GM.Global.Activity.FilterID)!="object")
                    GM.Global.Activity.FilterID = $.parseJSON(GM.Global.Activity.FilterID);
            this.favoriteData = GM.Global.Activity.FilterID;
            console.log(JSON.stringify(this.favoriteData));
            that.jsnData = {};
            var token = localStorage.getItem("token");
            
            var criterianm = this.$(".txt-fltr-name").val();
            
            if (criterianm != "") {
                that.jsnData.criteriaid = that.filterID;
                that.jsnData.criterianm = criterianm;
                var key, value;
                $.each(this.favoriteData, function (key, value) {
                    if(value != "" && key != "token")
                        that.arrAttribute.push({"key": key, "value": value, "attrid": "", "criteriaid": that.jsnData.criteriaid, "voidfl": ""});
                });
                
                var temp1 = [];
                _.each(GM.Global.criteriaAttr, function(data){
                    
                    var chk = "";
                    _.each(that.arrAttribute, function(dt, i){
                        if(dt.key == data.key){
                            dt.attrid = data.attrid;
                            chk = "Y";
                        }
                    });
                    if(chk == ""  ){
                        data.voidfl = "Y";
                        temp1.push(data);
                    }
                    
                    delete data.cmpid;
                    delete data.cmptzone;
                    delete data.cmpdfmt;
                    delete data.plantid;
                    delete data.partyid;
                });
                if(temp1.length){
                    _.each(temp1, function(data){
                        that.arrAttribute.push(data);
                    });
                }
                that.jsnData.criteriatyp = this.module;
                that.jsnData.token = token;
                that.jsnData.listGmCriteriaAttrVO = $.parseJSON(JSON.stringify(that.arrAttribute));
               GM.Global.CriteriaValue = "true";
                
                
                if(GM.Global.Device && GM.Global.CriteriaValue == "true"){
                    fnSaveCriteria(that.jsnData.criteriaid, that.jsnData, function(data){
                        if(data.length){
                            data = data[0];
                            GM.Global.myFilter = null;
                            GM.Global.myFilter= JSON.stringify({"Name":data.criterianm,"ID":data.criteriaid});
                            $(".li-new-favorite").addClass("hide");
                            $(".li-exist-favorite").addClass("hide");
                            $("#div-favorite-title").html(criterianm);
                            showSuccess("Favorite is updated successfully");
                            window.location.href = "#crm/"+that.module+"/list";
                            hideMessages();
                        }
                        else
                            showError("Favorite save is failed!");
                    });
                }
                else if(GM.Global.CriteriaValue == "true"){
                    fnGetWebServerData("criteria/save", undefined, that.jsnData, function (data) {
                        if (data != null) {
                            GM.Global.myFilter = null;
                            GM.Global.myFilter= JSON.stringify({"Name":data.criterianm,"ID":data.criteriaid});
                            $(".li-new-favorite").addClass("hide");
                            $(".li-exist-favorite").addClass("hide");
                            $("#div-favorite-title").html(criterianm);
                            showSuccess("Favorite is updated successfully");
                           
                            window.location.href = "#crm/"+that.module+"/list";
                            GM.Global.Filtersave = "true";
			    GM.Global.FilterName = data.criterianm;
                            hideMessages();
                            
                        } else
                            showError("Favorite save is failed!");
                    });
                }
                
            } else if(GM.Global.CriteriaValue != "true")
                showError("Please select filter values");
            else
                showError("Please enter the Favorite Name");

        }
            else
                showError("Please select any data");
        },
        // Search category vise
        searchFilter: function () {
            console.log(this.module)
            this.activityDate();
            window.location.href = "#crm/" + this.module + "/list";
        },

        showSelectedCriteria: function (event) {
            if($(event.currentTarget).hasClass("details-opened")) {
                $(".details-opened").removeClass("details-opened");
               $(".list-category-item").addClass("hide"); 
            }
            else {
                $(event.currentTarget).addClass("details-opened");
                $(".list-category-item").removeClass("hide");
            }
        },
        
        setMinDate: function(e){
            if($("#activity-todate").val()!="" && $("#activity-todate").val()!=undefined && $("#activity-frmdate").val()!=undefined && $("#activity-frmdate").val()>$("#activity-todate").val())
                    $("#activity-todate").val($("#activity-frmdate").val());
            var fromDate = $("#activity-frmdate").val();
            var toDate = $("#activity-todate").val();
            
            if($(e.currentTarget).attr("id") == "activity-frmdate")             
                $("#activity-todate").attr("min", fromDate);
//                var dd, mm, yyyy;
//                var date = new Date(fromDate);
//                dd = ("0" + date.getDate()).slice(-2), mm = ("0" + (date.getMonth() + 1)).slice(-2), yyyy = date.getFullYear();
//                var startDate = mm + "/" + dd + "/" + yyyy;
//                fromDate = startDate;
                
//                var date = new Date(toDate);
//                dd = ("0" + date.getDate()).slice(-2), mm = ("0" + (date.getMonth() + 1)).slice(-2), yyyy = date.getFullYear();
//                var endDate = mm + "/" + dd + "/" + yyyy;
//                toDate = endDate;
                if(GM.Global.Activity.FilterID == undefined)
                    GM.Global.Activity.FilterID = {};
                if(GM.Global.Activity.FilterName == undefined || GM.Global.Activity.FilterName == undefined)
                    GM.Global.Activity.FilterName = {};
                else if(typeof(GM.Global.Activity.FilterID)!="object")
                    GM.Global.Activity.FilterID = $.parseJSON(GM.Global.Activity.FilterID);
                if(typeof(GM.Global.Activity.FilterName)!="object")
                    GM.Global.Activity.FilterName = $.parseJSON(GM.Global.Activity.FilterName);
                if (toDate != "" && toDate != "aN/aN/NaN" && fromDate != "" && fromDate != "aN/aN/NaN") {
                    if(this.module == "crf" ){
                        GM.Global.Activity.FilterName["From Date"] = getDateByFormat(fromDate);
                        GM.Global.Activity.FilterID["crffrmdate"] = getDateByFormat(fromDate);
                        GM.Global.Activity.FilterID["crftodate"] = getDateByFormat(toDate);
                        GM.Global.Activity.FilterName["To Date"] = getDateByFormat(toDate);
                        }
                    else
                        {
                            GM.Global.Activity.FilterName["From Date"] = getDateByFormat(fromDate);
                        GM.Global.Activity.FilterID["actstartdate"] = getDateByFormat(fromDate);
                        GM.Global.Activity.FilterID["actenddate"] = getDateByFormat(toDate);
                        GM.Global.Activity.FilterName["To Date"] = getDateByFormat(toDate);
                        }
                } 
            else {
                 if(this.module == "crf" ){
                     GM.Global.Activity.FilterName["From Date"] = "";
                    GM.Global.Activity.FilterID["crffrmdate"] = "";
                    GM.Global.Activity.FilterID["crftodate"] = "";
                    GM.Global.Activity.FilterName["To Date"] = "";
                 }
                else
                    {
                    GM.Global.Activity.FilterName["From Date"] = "";
                    GM.Global.Activity.FilterID["actstartdate"] = "";
                    GM.Global.Activity.FilterID["actenddate"] = "";
                    GM.Global.Activity.FilterName["To Date"] = "";
                }
            }
                var tempArray = new Array();
                var result = $.parseJSON(JSON.stringify(GM.Global.Activity.FilterName));
                $.each(result, function(key, val) {
                    console.log(key + ' is ' + val);
                    if(val != "" && val != "aN/aN/NaN")
                        tempArray.push({"title":key, "value":val});
                });
                console.log(tempArray);
                var finalJSON = $.parseJSON(JSON.stringify(tempArray));
                console.log(finalJSON);
                if (finalJSON != "") {
                    this.$(".list-category-item").html(this.template_selected_items(finalJSON));
                    if(GM.Global.SwitchUser)
                        $("#div-criteia-save").addClass("hide");
                    
                    this.$("#btn-category-search").show();
                } else {
                    this.$(".list-category-item").empty();
                    this.$("#btn-category-search").hide();
                }

                $(".div-criteria").addClass("gmFont"+this.module);
            
        },
        
        toggleSelectedItems: function (event) {
            if ($("#criteria-selected-itmes").hasClass("gmMediaDisplayNone")) {
                $("#criteria-selected-itmes").removeClass("gmMediaDisplayNone");
                $("#selected-criteria-icon").find(".fa-chevron-down").addClass("fa-chevron-up").removeClass("fa-chevron-down");
            } else {
                $("#criteria-selected-itmes").addClass("gmMediaDisplayNone");
                $("#selected-criteria-icon").find(".fa-chevron-up").addClass("fa-chevron-down").removeClass("fa-chevron-up");
            }
        },
        
            //Handles the orietaion change in the iPad
            onOrientationChange: function(event) {
                resizeHeadWidth('.gmPanelHead', '.gmBtnGroup', '#panelhead-left');
            },
    });

	return GM.View.ActivityFilter;
});
