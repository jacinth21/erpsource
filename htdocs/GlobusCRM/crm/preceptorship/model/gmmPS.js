/**********************************************************************************
 * File:        GmmPS.js
 * Description: Model to represent Case Request
 * Version:     1.0
 * Author:     
 **********************************************************************************/
define([

    //External Libraries
    'underscore', 'backbone',

    //Common
    'global',

    //Collection
    'gmcItem', 'gmcAddress'
],

    function (_, Backbone,
        Global, GMCItem, GMCAddress
    ) {

        'use strict';

        GM.Model.PS = Backbone.Model.extend({

            // Default values of the model
            defaults: {
                "casereqid": "",
                "segid": "",
                "actid": "",
                "surgid": "",
                "frmdt": "",
                "todt": "",
                "status": "",
                "type": "",
                "note": "",
                "createdby": "",
                "createdon": "",
                "hotlnm": "",
                "hotlprice": "",
                "trvlnm": "",
                "trvlprice": "",
                "pcprcrf": "",
                "trfstatis": "",
                "labinv": "",
                "labadd": "",
                "pcmninv": "",
                "survfl": "",
                "crdfl": "",
                "division": "",
                "workflow":"",
                "pstype":"",
                "clobj":"",
                "region":"",
                "zone": "",
                "adminsts": "",
                "atndtyp": "",
                "atndnm": "",
                "webinarfl": "",
                "arrcrmpscaseassvo": [],
                "arrcrmpscasereqvo": [],
                "arrcrmpscaseattrvo": []
            },

            //Initial configurations
            initialize: function (data) {
                this.set("arrcrmpscaseassvo", new Array());
                this.set("arrcrmpscasereqvo", new Array());
                this.set("arrcrmpscaseattrvo", new Array());
            },


            //to set those data that alone are editable
            getEditableData: function () {
                var preceptor = $.extend(true, {}, this);
            },

            //setting all surgeon's models without Vo names based on template classes
            setModalValue: function (elem) {
                var model = $.extend(true, {}, this);
                var that = this;
                _.each(model.keys(), function (attribute) {
                    if ($(elem).find('.data-' + attribute).get(0) != undefined)
                        that.set(attribute, $(elem).find('.data-' + attribute).val());
                });
            },

            //setting all surgeon's models with Vo names based on template classes
            setValues: function (elem, selector, attrib) {
                var that = this;
                this.get(attrib).reset();
                this.get(attrib).add({});
                var model = this.get(attrib).pop();
                $(elem).find(selector).each(function () {
                    var jsnModel = {};
                    var target = this;
                    _.each(model.keys(), function (attribute) {
                        jsnModel[attribute] = $(target).find('.data-' + attribute).val();
                    });
                    that.get(attrib).add(jsnModel);
                });
            },

            //to get final model data as JSON
            getFinalData: function () {
                
            }

        });

        return GM.Model.PS;

    });