/**********************************************************************************
 * File             :        gmvPSHost.js
 * Description      :        View to represent Preceptor Host Case
 * Path             :        /web/GlobusCRM/crm/preceptorship/view/gmvPSHost.js
 * Author           :        mahendrand
 **********************************************************************************/
define([

    // External Libraries
    'jquery', 'underscore', 'backbone', 'handlebars',

    // Common
    'global', 'commonutil', 'gmvSearch', 'gmvValidation','/GlobusCRM/common/view/GmDropDownView.js',

    // View
    'gmvPSUtil',

    //Model
     'gmmActivity'

],

    function ($, _, Backbone, Handlebars,
        Global, Commonutil, GMVSearch, GMVValidation,DropDownView,
        GMVPSUtil, GMMActivity) {

        'use strict';

        GM.View.PSHost = Backbone.View.extend({

            name: "CRM Preceptorship Host",

            el: "#div-crm-module",

            /* Load the templates */
            template_MainModule: fnGetTemplate(URL_Common_Template, "gmtCRMMainModule"),
            template_preceptorHost: fnGetTemplate(URL_Preceptor_Template, "gmtPSHostEdit"),
            template_preceptorHostInfo: fnGetTemplate(URL_Preceptor_Template, "gmtPSHostInfo"),
            template_CreateSurgeon: fnGetTemplate(URL_Common_Template, "gmtCreateSurgeon"),
            template_VisitCancelReq: fnGetTemplate(URL_PhysicianVisit_Template, "gmtCancelRequest"),
            template_preceptorHostClone: fnGetTemplate(URL_Preceptor_Template, "gmtPSHostClone"),


            events: {
                "change input, select, textarea": "updateModel",
                "click #li-cancel": "backToPage",
                "click #li-save": "saveHost",
                "click #lookupHostRequest": "showHostRqReport",
                "click #btn-main-edit": "editHost",
                "click #preceptorTypeNfn": "openNotifyPSType",
                "click #create-surgeon": "createSurgeon",
                "click .remove-participants": "removeParticipants",
                "click .span-remove-extend": "cancelAttribute",
                "click .span-remove-direct": "removeDirectVal",
                "change #divtyp": "changeDivType",
				"click #div-host-case-cancel": "cancelHost",
                "click #lookupCaseRequest": "showLkupCaseReq",
                "click #span-edit-title": "changeTitle",
                "click #span-reset-title": "resetTitle",
                "click #span-set-title": "setTitle",
                "click #span-cancel-title": "cancelTitle",
                "click #segSysList" : "fetchSegSysList",
                "click .a-file-open": "viewDocument",
                "click #li-clone": "cloneHost",
                "click #btn-add-new-acnt": "addNewAcnt",
                "change #region":"changeZoneVal",
            },

            initialize: function (options) {
                var that = this;
                console.log(options);
                this.module = options.module;
                this.actid = options.actid;
                this.data = options.data;
                
                console.log(this.module);
                if (GM.Global.PSHost == undefined)
                    GM.Global.PSHost = {};

                if (this.actid && this.actid != 0) {
                    this.HCModel = new GMMActivity(this.data);
                    this.HCModel.set("arrgmactivitypartyvo", arrayOf(this.data.arrgmactivitypartyvo));
                    this.HCModel.set("arrgmactivityattrvo", arrayOf(this.data.arrgmactivityattrvo));
                    this.HCModel.set("arrgmcrmlogvo", arrayOf(this.data.arrgmcrmlogvo));
                    this.HCModel.set("arrgmcrmaddressvo", arrayOf(this.data.arrgmcrmaddressvo));

                    if (this.HCModel.get('arrgmcrmaddressvo')[0] == null || this.HCModel.get('arrgmcrmaddressvo')[0] == undefined)
                        this.HCModel.set('arrgmcrmaddressvo', []);
                    if (this.HCModel.get('arrgmcrmlogvo')[0] == null || this.HCModel.get('arrgmcrmlogvo')[0] == undefined)
                        this.HCModel.set('arrgmcrmlogvo', []);
                    if (this.HCModel.get('arrgmactivityattrvo')[0] == null || this.HCModel.get('arrgmactivityattrvo')[0] == undefined)
                        this.HCModel.set('arrgmactivityattrvo', []);
                    if (this.HCModel.get('arrgmactivitypartyvo')[0] == null || this.HCModel.get('arrgmactivitypartyvo')[0] == undefined)
                        this.HCModel.set('arrgmactivitypartyvo', []);
                    this.HCModel.set($.parseJSON(JSON.stringify(this.HCModel)));
                    GM.Global.PSHost.Mode = "view";
                    var chkData=this.HCModel.toJSON();
//                    this.FSAttendee = _.findWhere(chkData.arrgmactivitypartyvo, {
//                    "roleid": "105561", "voidfl": "", "partyid": localStorage.getItem("partyID")
//                    });
                } else if (this.actid == 0 || this.actid == undefined) {
                    this.HCModel = new GMMActivity();
                    GM.Global.PSHost.Mode = "create";
                }

            },

            render: function () {
                var that = this;
                this.cancelActSts=false;
                this.$el.html(this.template_MainModule({
                    "module": that.module
                }));
                var defDivision = localStorage.getItem("repDivision");
                var defRegion = localStorage.getItem("userDivision");
                if(parseNull(defDivision) == "")
                   defDivision = "2000";
                if(parseNull(defRegion) != "100824")
                   defRegion = "100823";
                
                $("#preceptorship").addClass("tabpreceptorship");
                
                this.psHostCaseAdmin = _.findWhere(GM.Global.UserAccess, {
                    funcnm: "PRECEPTOR-ADMIN"
                }); 

                
                if (GM.Global.PSHost.Mode == 'create') {
                    console.log(this.HCModel.toJSON());
                    $(".gmPanelContent").html(this.template_preceptorHost(this.HCModel.toJSON()));
                    $(".gmPanelTitle").html("Create Host Case");
                    $("#li-save").removeClass("hide");
                    $("#li-cancel").removeClass("hide");
                    $("#ul-act-title").addClass("hide");                    
                    that.searchSurgeon();
                    that.searchParticipant();
                    that.searchProduct();
                    that.searchProcedure();
                    that.searchHospital();
                    
                    getCLValToDiv("#divtyp", "", "", "DIVLST", "", "105272", "codenmalt", "division", false, false, function (divID, attrVal) {
                        if (that.HCModel.get(attrVal) != "")
                            that.setSelect(divID, that.HCModel.get(attrVal));
                        else {
                            that.setSelect(divID, defDivision);  // Default Set to Spine
                            $(divID).trigger("change");
                        }
                        
                        if (that.HCModel.get("division") == "2004"){ // If Division Type is INR
                             $(".PStypeDiv").removeClass("hide");
                            that.showHideWorkflow("INR");
                            that.selectPSType("INR");                          
                        }
                        else{                                
                            $(".PStypeDiv").addClass("hide");
                            that.showHideWorkflow("NINR");
                        }
                    });

                    this.tzoneCodeLkupVal();

                    getCLValToDiv("#region", "", "", "DIV", "", "", "codeid", "region", false, false, function (divID, attrVal) {
                        if (that.HCModel.get(attrVal) != "")
                            that.setSelect(divID, that.HCModel.get(attrVal));
                        else {
                            that.setSelect(divID, defRegion);
                            $(divID).trigger("change");
                        }
                    });
                    //Add Field Sales as Logged in Uses as Default                    
                 
                    var newParticipants = _.findWhere(this.HCModel.get("arrgmactivitypartyvo"), {
                        roleid: "105561"
                    });
                    if (newParticipants == undefined) {
                        var partyinfo = {
                            "ID": localStorage.getItem("partyID"),
                            "Name": localStorage.getItem("fName") + " " + localStorage.getItem("lName"),
                        };
                        this.setPartyInfo(partyinfo, "7006", "105561");
                    }
                    
                    that.getZoneVal(defRegion,true);
                    getCLValToDiv("#admin-status", "", "", "PCTADS", "", "", "codeid", "adminsts", false, true, function (divID, attrVal) {
                        if (that.HCModel.get(attrVal) != "")
                            that.setSelect(divID, that.HCModel.get(attrVal));
                        else {
                            that.setSelect(divID, ""); //set default value
                            $(divID).trigger("change");
                        }
                    });

                
                    
                } else if (GM.Global.PSHost.Mode == "view") {
                    console.log(this.HCModel.toJSON());                        
                    $(".gmPanelTitle").html(this.HCModel.get("actnm"));
                    _.each(this.HCModel.get("arrgmactivitypartyvo"), function (data) {
                        data.server = URL_CRMFileServer;
                    });
                    this.hostCaseFl=this.HCModel.toJSON().accessfl;
                    $(".gmPanelContent").html(this.template_preceptorHostInfo(this.HCModel.toJSON()));
                    $("#li-clone").removeClass("hide");
                    
                    if (that.psHostCaseAdmin != undefined) {
                            $("#li-edit").removeClass("hide");
                            $("#lookupCaseRequest").removeClass("hide");
                    }
                    else if((GM.Global.UserID == this.HCModel.get("createdbyid")) || (this.hostCaseFl == "Y") ){
                            $("#li-edit").removeClass("hide");
                            $("#lookupCaseRequest").addClass("hide");
                    }
                    else{
                        $("#li-edit").addClass("hide");
                        $("#lookupCaseRequest").addClass("hide");
                     } 
                    if(this.HCModel.get("actstatus")=='105762'){                        
                        $("#lookupCaseRequest").addClass("hide");
                        if (that.psHostCaseAdmin == undefined) 
                            $("#li-edit").addClass("hide");
                    }
                    that.showAttendSurgDetailGrid();
                } else if (GM.Global.PSHost.Mode == 'edit') {
                    if (this.HCModel.get("actstartdate") != "") {
                        var frmdt = "";
                        frmdt = this.HCModel.get("actstartdate");
                        if (frmdt != "" && frmdt != null) {
                            if (frmdt.indexOf("-") > 0) {
                                frmdt = this.HCModel.get("actstartdate");
                                this.HCModel.set("actstartdate", frmdt);
                            } else {
                                frmdt = frmdt.split("/");
                                this.HCModel.set("actstartdate", frmdt[2] + "-" + frmdt[0] + "-" + frmdt[1]);
                            }
                        }
                    }
                    this.HCModel.set("actenddate", this.HCModel.get("actstartdate"));
                    console.log(this.HCModel.toJSON());
                    $(".gmBtnGroup #li-cancel").removeClass("hide");
                    $(".gmBtnGroup #li-save").removeClass("hide");

                    if (_.contains(GM.Global.Admin, this.module))
                        $(".gmBtnGroup #li-void").removeClass("hide");
                    $(".gmPanelTitle").html(this.HCModel.get("actnm"));
                    $(".gmPanelContent").html(this.template_preceptorHost(this.HCModel.toJSON()));
                    if(this.HCModel.get("actstatus")!="105762")
                        $("#div-host-case-cancel").removeClass("hide");
                    that.searchSurgeon();
                    that.searchParticipant();
                    that.searchProduct();
                    that.searchProcedure();
                    that.searchHospital();
                    getCLValToDiv("#divtyp", "", "", "DIVLST", "", "105272", "codenmalt", "division", false, false, function (divID, attrVal) {
                        if (that.HCModel.get(attrVal) != "")
                            that.setSelect(divID, that.HCModel.get(attrVal));

                        else {
                            that.setSelect(divID, defDivision);
                            $(divID).trigger("change");
                        }
                        if (that.HCModel.get("division") == "2004") {
                             $(".PStypeDiv").removeClass("hide");
                            that.selectPSType("INR");
                            that.showHideWorkflow("INR");
                            that.removeAccValidation("INR");
                        } else {
                             $(".PStypeDiv").addClass("hide");
                            that.showHideWorkflow("NINR");
                            that.removeAccValidation("NINR");
                        }
                    });

                    this.tzoneCodeLkupVal();

                    getCLValToDiv("#region", "", "", "DIV", "", "", "codeid", "region", false, false, function (divID, attrVal) {
                        if (that.HCModel.get(attrVal) != "")
                            that.setSelect(divID, that.HCModel.get(attrVal));
                        else {
                            that.setSelect(divID, defRegion);
                            $(divID).trigger("change");
                        }
                    });
                    
                    that.getZoneVal(defRegion,true);
                    
                    getCLValToDiv("#admin-status", "", "", "PCTADS", "", "", "codeid", "adminsts", false, true, function (divID, attrVal) {
                        if (that.HCModel.get(attrVal) != "")
                            that.setSelect(divID, that.HCModel.get(attrVal));
                        else {
                            that.setSelect(divID, ""); //set default value
                            $(divID).trigger("change");
                        }
                    });
                    
                    if (isRep()) {
                        $("#li-preceptorship-title").removeClass("hide");
                        $("#li-sc-title").addClass("hide");

                    } else {
                        if (!$("#li-sc-title").hasClass("hide")) {
                            $("#span-edit-title").addClass("hide");
                            $("#li-preceptorship-title").addClass("hide");
                        } else {
                            $("#span-edit-title").removeClass("hide");
                            $("#li-preceptorship-title").removeClass("hide");
                        }
                    }

                }
                
                if (that.psHostCaseAdmin != undefined) {
                   $(".adminStatus").removeClass("hide");
                }

                if (GM.Global.SchedulerFL == "Y") {
                    GM.Global.PSHost.Mode = "edit";
                    GM.Global.SchedulerFL = "";
                    setTimeout(function () {
                        $("#btn-main-edit").trigger("click");
                    });
                }
                
                this.gmvValidation = new GMVValidation({
                    "el": this.$(".gmPanelContent")
                });
                
            },
            
            getZoneVal: function (region,trig) {
                var that = this;
                getCLValToDiv("#zone", "", "", "ZONE", "", region, "codeid", "zone", false, true, function (divID, attrVal) {
                    if (that.HCModel.get(attrVal) != "")
                         that.setSelect(divID, that.HCModel.get(attrVal));  
                    else {
                        if (trig) {   
                            var input = {
                                "token": localStorage.getItem("token")
                            };
                            fnGetWebServerData("psrpt/fetchsrepzone", undefined, input, function (data) { // To get the sales rep zone
                                if (data != null) {
                                    that.setSelect(divID, data.ID);
                                    if(!$("#zone option").is(":selected"))
                                        that.setSelect(divID, "");
                                    $(divID).trigger("change");
                                }  
                                else {
                                    that.setSelect(divID, ""); //set default value
                                    $(divID).trigger("change");
                                }
                            });
                        }
                        else {
                            that.setSelect(divID, ""); //set default value
                            $(divID).trigger("change");
                        }
                    }
                });
            },
            
            viewDocument: function (event) {
                var path = $(event.currentTarget).attr('href-url');
                if (parseNull(path) != "")
                    window.open(path, '_blank', 'location=no');
            },

            changeTitle: function () {
                $("#li-sc-title").removeClass("hide");
                $("#li-preceptorship-title").addClass("hide");
                $("#span-edit-title").addClass("hide");
                $("#li-title-set-btn").removeClass("hide");
            },
            setTitle: function () {
                var that = this;
                $("#li-sc-title").addClass("hide");
                $("#span-edit-title").removeClass("hide");
                $("#li-preceptorship-title").removeClass("hide");
                $("#li-title-set-btn").addClass("hide");

                var inputText = $("#input-preceptorship-title").val();
                $("#li-preceptorship-title").text(inputText);
                $(".gmPanelTitle").text(inputText);
                this.HCModel.set("actnm", inputText);
            },
            resetTitle: function () {
                this.setActTitle();
            },

            cancelTitle: function () {
                $("#li-preceptorship-title").removeClass("hide");
                $("#li-sc-title").addClass("hide");
                $("#span-edit-title").removeClass("hide");
                $("#li-title-set-btn").addClass("hide");
            },
            
            cloneHost: function () {
                var that = this;
                that.cloneDate = "";
                showPopup();
                $("#div-crm-overlay-content-container .modal-body").html(this.template_preceptorHostClone());
                $(".modal-footer .gmBtn").removeClass("hide").addClass("hide");
                $(".modal-footer .save-model").removeClass("hide");
                $(".modal-footer .cancel-model").removeClass("hide");
                $(".modal-header .modal-title").text("Clone Host Case");
                $("#clone-req-error").css("display", "none");
                $("#div-crm-overlay-content-container").find('.save-model').unbind('click').bind('click', function (e) {
                    var clfrmdt = $("preceptorship-clonedate").val();
                });
                $(".hostCloneForm input").on("change",function(e){
                    that.updateModel(e);
                });
                $(".modal-footer .save-model").on("click",function(){
                    if($("#preceptorship-actstartdate").val() != ""){
                        that.cloneDate = "Y";
                    }
                    else{
                        $("#clone-req-error").html("Please Select date").slideDown('fast').css("display", "block").delay(4000).slideUp('slow');
                        return false;
                    }
                    var input = "";
                    var clfrmdt = $(".preceptorship-clonedate").val(); 
                    var clfrmtime = $(".preceptorship-clone-starttime").val();
                    var clendtime = $(".preceptorship-clone-endtime").val();
                    that.HCModel.set("actstarttime",clfrmtime);
					that.HCModel.set("actendtime", clendtime);
					that.HCModel.set("actstartdate", clfrmdt);
					that.HCModel.set("actenddate", "");
					that.HCModel.actid =0;
					that.HCModel.set("actid", "");
					that.HCModel.set("actnotes", "");

                     that.HCModel.unbind("change");
                     
                     var input = that.HCModel;
                     
                    
                     var fromDate = that.HCModel.get("actstartdate");
                     var fromTime = that.HCModel.get("actstarttime");
                     var toTime = that.HCModel.get("actendtime");

                     var date = getDateByFormat(fromDate);
                        
                     if (parseNull(this.actid) == 0)
                         that.HCModel.set("actstatus", "105765");

                     var acttzone = that.HCModel.get("tzone");

                     var phyStatus = that.HCModel.get("actstatus");
                     if (phyStatus == "105765" && parseNull(this.actid) == 0)
                         this.mailStatus = 1; // request

                     if (parseNull(this.actid) != 0 && event.currentTarget.id == "li-save") {
                         this.mailStatus = 2; // update
                     }

                     if (GM.Global.PSHost.Mode == "create")
                         that.setActTitle();

                    

                     
                     input.set("token", localStorage.getItem("token"));
                     input.set("actcategory", "105272");
                     if (input.get('arrgmcrmaddressvo')[0] == null || input.get('arrgmcrmaddressvo')[0] == undefined)
                         input.set('arrgmcrmaddressvo', []);
                     if (input.get('arrgmcrmlogvo')[0] == null || input.get('arrgmcrmlogvo')[0] == undefined)
                         input.set('arrgmcrmlogvo', []);
                     if (input.get('arrgmactivityattrvo')[0] == null || input.get('arrgmactivityattrvo')[0] == undefined)
                         input.set('arrgmactivityattrvo', []);
                     if (input.get('arrgmactivitypartyvo')[0] == null || input.get('arrgmactivitypartyvo')[0] == undefined)
                         input.set('arrgmactivitypartyvo', []);
                     _.each(input.get('arrgmactivitypartyvo'), function (data) {
                         delete data.server;
                     });
                    
                    
                     if (that.gmvValidation.validate() && that.cloneDate == "Y") {

                         var getDt = compDateFmt(input.get("actstartdate"));
                         var gettime = input.get("actstarttime") + ":" + "00";
                         that.chkDt(getDt + " " + gettime, function (val) {
                             if (val == true) {
                                 if (date && date != "aN/aN/NaN")
                                     input.set("actstartdate", date);
                                 input.set("actenddate", input.get("actstartdate"));
                                 input.set("actstarttime", fromTime + " " + acttzone);
                                 input.set("actendtime", toTime + " " + acttzone);
                                 fnGetWebServerData("crmactivity/save", undefined, input, function (data) {
                                     console.log("Acitivty Save OUTPUT" + JSON.stringify(data));

                                     if (data != null) {
                                         showSuccess("Host Case saved successfully");
                                         var id = data.actid;
                                         //                            that.eMailConfirm(id);
                                         that.sendHostEmail(id, "N");

                                         window.location.href = "#crm/" + that.module + "/host/id/" + id;
                                         
                                         GM.Global.PSHost.Mode = "view";

                                     } else
                                         showError("Activity saved Failed");
                                 });
                             }
                         });
                     } else
                         showError(that.gmvValidation.getErrors());

                        hidePopup();
                    });
			},

            setActTitle: function () {
                var that = this;
                this.actSurgeon = "";
                this.actParticipants = "";
                this.actOtherVisitors = "";
                var actTypeNm = "";
                _.each(this.HCModel.get("arrgmactivitypartyvo"), function (data) {
                    if (data.roleid != 105561 && data.voidfl != "Y")
                        that.actSurgeon += data.partynm + ", ";
                });
                var procedure = this.HCModel.get("segnm");
                var pathology = this.HCModel.get("pathlgy");
                var divisionName = $("#divtyp option:selected").text();
                var valCheck = "";
                if (parseNull(pathology) == "")
                    valCheck = " ";
                else
                    valCheck = ", ";

                
                    //     <Division>: <Host Case Surgeon(s)>,<Procedure>,<Pathology (if entered)>
                $("#input-preceptorship-title").val(divisionName.toUpperCase() + ": " + parseNull(that.actSurgeon) + " " + parseNull(procedure) +valCheck+ parseNull(pathology));
                $("#li-preceptorship-title").text(divisionName.toUpperCase() + ": " + parseNull(that.actSurgeon) + " " + parseNull(procedure) +valCheck+ parseNull(pathology));
                $(".gmPanelTitle").text(divisionName.toUpperCase() + ": " + parseNull(that.actSurgeon) + " " + parseNull(procedure) +valCheck+ parseNull(pathology));

                this.HCModel.set("actnm", $("#input-preceptorship-title").val());
            },
            
            //call preceptorship type codeloopup values based on division
            selectPSType: function (divtype) {
                var that = this;
                getCLValToDiv("#pstyp", "", "", "PCTSTY", divtype, "", "codeid", "acttype", false, true, function (divID, attrVal) {
                    that.setSelect(divID, that.HCModel.get(attrVal));

                    if (that.HCModel.get("division") == "2004" && isRep()) {
                        $("#pstyp option[value='107264']").remove(); // in iPad make it as disable 
                        $("#rpt-legend-detail-inr p:last-child").addClass("hide");
                    }
                });
            },
            
			//Show or Hide workflow and pathology based on division type
            showHideWorkflow: function (divtype) {
                var that=this;
                if (divtype == 'INR') {
                    if ($("#ul-workflow").hasClass("hide"))
                        $("#ul-workflow").removeClass("hide");
                    if (!$("#ul-pathology").hasClass("hide"))
                        $("#ul-pathology").addClass("hide");
                    getCLValToDiv("#workflow", "", "", "PRWKFL", "", "", "codeid", "workflow", false, true, function (divID, attrVal) {
                        that.setSelect(divID, that.HCModel.get(attrVal));
                    });
                    this.HCModel.set("pathlgy", "");
                } else{
                if ($("#ul-pathology").hasClass("hide"))
                    $("#ul-pathology").removeClass("hide");
                if (!$("#ul-workflow").hasClass("hide"))
                    $("#ul-workflow").addClass("hide");
                    this.HCModel.set("workflow", ""); 
                }
            },
            
            // Remove Account Validations If INR in Division Field is selected
            removeAccValidation:function(divtype){
                if (divtype == 'INR') {
                    $(".gmHospiatalLbl").html("HOSPITAL");  
                    $(".gmPStypeLbl").html("*PRECEPTORSHIP TYPE");
                    if($("#preceptorship-hospital-search input").hasClass("gmValidationError"))
                        $("#preceptorship-hospital-search input").removeClass("gmValidationError");
                }
                else{
                    $(".gmHospiatalLbl").html("*HOSPITAL"); 
                    $(".gmPStypeLbl").html("PRECEPTORSHIP TYPE");
                }
                       
            },

            //On Change Division
            changeDivType: function (e) {
                var that = this;
                var newDivision = $(e.currentTarget).find("option:selected").val();
                if (parseNull(this.oldDivision) != parseNull(newDivision)) {
                    $('.preceptTypeNfnBx').css('display', 'none');
                    that.HCModel.set("acttype", "");
                    if (that.HCModel.get("division") == "2004"){
                         $(".PStypeDiv").removeClass("hide");
                        that.selectPSType("INR");
                        that.showHideWorkflow("INR");
                        that.removeAccValidation("INR");
                    }
                    else{
                         $(".PStypeDiv").addClass("hide");
                        that.showHideWorkflow("NINR"); 
                        that.removeAccValidation("NINR");
                    }
                } 
            },

            //Time Zone Field codelookup values
            tzoneCodeLkupVal: function () {
                var that = this;
                getCodeLookUpVal("", "", "TIMZNE", "", "", function (data) {
                    var arrSelectOpt = new Array();
                    data = arrSelectOpt.concat(_.sortBy(data, function (data) {
                        return data.codenm;
                    }));
                    _.each(data, function (dt) {
                        if (dt.codenm) {
                            dt.codeidvalue = dt.codenm.toUpperCase();
                            if(dt.codenmalt)
                            dt.codenmvalue = dt.codenm+" ("+dt.codenmalt+")";
                            else
                            dt.codenmvalue = dt.codenm;    
                        }
                    });

                    var strSelectOptions = JSON.stringify(data).replace(/codeidvalue/g, 'ID').replace(/codenmvalue/g, 'Name');

                    var optionItems = $.parseJSON(strSelectOptions);
                    $("#timezn").empty();
                    $("#timezn").each(function () {
                        fnCreateOptions(this, optionItems);
                        $(this).find("option").attr("name", "tzone");
                    });
                    if(parseNull(that.HCModel.get("tzone"))==""){
                        var defaultTZ=Intl.DateTimeFormat().resolvedOptions().timeZone;
                        defaultTZ = defaultTZ.toUpperCase();
                        that.setSelect("#timezn", defaultTZ);
                        $("#timezn").trigger("change");
                    }
                    else
                    that.setSelect("#timezn", that.HCModel.get("tzone"));
                });
            },

            // Search option for the surgeon
            searchSurgeon: function () {
                var that = this;
                var fnName = "";
                var searchOptions = {
                    'el': $("#" + this.module + "-hc-surgeon-search"),
                    'url': URL_WSServer + 'search/rsparty',
                    'inputParameters': {
                        'token': localStorage.getItem('token'),
                        "partytype": "7000"
                    },
                    'placeholder': 'Key in at least 3 characters to search',
                    'keyParameter': 'partynm',
                    'IDParameter': 'id',
                    'NameParameter': 'name',
                    'resultVO': 'gmPartyBasicVO',
                    'fnName': fnName,
                    'minChar': 3,
                    'callback': function (result) {
                        that.setPartyInfo(result, "7000", "105564"); //Attendee
                    }
                };
                var searchView = new GMVSearch(searchOptions);
            },


            // Search Options for participants
            searchParticipant: function () {
                var that = this;
                var searchOptions3 = {
                    'el': $("#" + this.module + "-participants-search"),
                    'url': URL_WSServer + 'search/rsparty',
                    'inputParameters': {
                        'token': localStorage.getItem('token'),
                        "partytype": "7002,7003,7004,7005,7006"
                    },
                    'placeholder': 'Key in at least 3 characters to search',
                    'keyParameter': 'partynm',
                    'IDParameter': 'id',
                    'NameParameter': 'name',
                    'resultVO': 'gmPartyBasicVO',
                    'minChar': 3,
                    'callback': function (result) {
                        that.setPartyInfo(result, "7006", "105561"); // 105561 - Participant
                    }
                };

                var searchView = new GMVSearch(searchOptions3);
            },


            // Search options for Procedures
            searchProcedure: function () {
                var that = this;

                var input = {
                    'token': localStorage.getItem('token')
                };

                var searchOptions1 = {
                    'el': $("#" + this.module + "-procedure-search"),
                    'url': URL_WSServer + 'search/segment',
                    'inputParameters': input,
                    'keyParameter': 'segnm',
                    'IDParameter': 'segid',
                    'NameParameter': 'segnm',
                    'resultVO': 'gmSegmentBasicVO',
                    'minChar': 2,
                    'callback': function (val) {
                        console.log(val);
                        that.setProcedureInfo(val);
                    }
                };

                var searchView4 = new GMVSearch(searchOptions1);

            },

            // Set Procedure Values in the Search box
            setProcedureInfo: function (data) {
                var that = this;
                $("#" + this.module + "-hprocedureid").val(data.ID);
                $("#" + this.module + "-hprocedurenm").val(data.Name);
                $("#" + this.module + "-hprocedureid").trigger("change");
                $("#" + this.module + "-hprocedurenm").trigger("change");
                this.render();
            },

            // Search option for Products
            searchProduct: function () {
                var that = this;

                var input = {
                    'token': localStorage.getItem('token')
                };

                var searchOptions1 = {
                    'el': $("#" + this.module + "-product-search"),
                    'url': URL_WSServer + 'search/system',
                    'inputParameters': input,
                    'keyParameter': 'sysnm',
                    'IDParameter': 'sysid',
                    'NameParameter': 'sysnm',
                    'resultVO': 'gmSystemBasicVO',
                    'minChar': 2,
                    'callback': function (val) {
                        that.setProductInfo(val);
                    }
                };

                var searchView4 = new GMVSearch(searchOptions1);

            },

            // Set Product Values in Product Search box
            setProductInfo: function (data) {
                var that = this;
                $("#" + this.module + "-product-search").find("input").val("");
                $("#" + this.module + "-hproduct").attr("attrtype", 105582);
                $("#" + this.module + "-hproduct").val(data.ID);
                $("#" + this.module + "-hproduct").attr("attrnm", data.Name);
                $("#" + this.module + "-hproduct").trigger("change");
                this.render();
            },

            // Set Surgeon Values in Surgeon Search box
            setPartyInfo: function (data, partytype, roleid) {
                var that = this;
                $("#" + this.module + "-participants-search").find("input").val("");
                $("#" + this.module + "-hpartytype").val(partytype);
                $("#" + this.module + "-hpartyrole").val(roleid);
                $("#" + this.module + "-hpartynm").val(data.Name);
                $("#" + this.module + "-hpartynm").attr("partyid", data.ID);
                $("#" + this.module + "-hpartytype").attr("partyid", data.ID);
                $("#" + this.module + "-hpartyrole").attr("partyid", data.ID);
                $("#" + this.module + "-hpartyrole").trigger("change");
                $("#" + this.module + "-hpartynm").trigger("change");
                $("#" + this.module + "-hpartytype").trigger("change");


                $("#" + this.module + "-hpartyid").val(data.ID); //Need to replace partyid also for single attendee entery in leads
                $("#" + this.module + "-hpartyid").attr("partyid", data.ID);
                $("#" + this.module + "-hpartyid").trigger("change");

                this.render();
            },


			//Remove Participants after click a close icon
            removeParticipants: function (event) {
                var partyid = $(event.currentTarget).parent().attr("data-partyid");
                var roleid = $(event.currentTarget).parent().attr("data-roleid");
                var actpartyid = $(event.currentTarget).parent().attr("data-actpartyid");

                if (parseNull(actpartyid) != "") {
                    _.each(this.HCModel.get("arrgmactivitypartyvo"), function (data) {
                        if (data.actpartyid == actpartyid)
                            data.voidfl = "Y";
                    });
                } else {
                    var voidParty = _.reject(this.HCModel.get("arrgmactivitypartyvo"), function (data) {
                        return data.partyid == partyid && data.roleid == roleid;
                    });

                    this.HCModel.set("arrgmactivitypartyvo", voidParty);
                }
                this.render();
            },

            //Remove Product Values on clicking the close icon
            removeDirectVal: function (event) {
                var attrval = $(event.currentTarget).parent('.div-sc-extn').attr("data-attrval");
                var attrnm = $(event.currentTarget).parent('.div-sc-extn').attr("data-attrnm");
                var attrid = this.HCModel.get(attrnm);
                if((attrnm=="segid") && (attrid==attrval)){
                    this.HCModel.set("segid", "");
                    this.HCModel.set("segnm", "");
                    this.render();
                }
                else if((attrnm=="partyid") && (attrid==attrval)){
                    this.HCModel.set("partyid", "");
                    this.HCModel.set("partynm", "");
                    this.render();
                }
            },

            //Remove Product Values on clicking the close icon
            cancelAttribute: function (event) {
                var that = this;
                var attrid = $(event.currentTarget).parent('.div-sc-extn').attr("data-attrid");
                var attrtype = $(event.currentTarget).parent('.div-sc-extn').attr("data-attrtype");
                var attrval = $(event.currentTarget).parent('.div-sc-extn').attr("data-attrval");
                var removeFl = $(event.currentTarget).attr("removeFl");
                var indexVal = $(event.currentTarget).parent().find('input').attr("index");

                if (attrid != "") {
                    _.each(this.HCModel.get("arrgmactivityattrvo"), function (data) {
                        if (data.attrid == attrid)
                            data.voidfl = "Y";
                    });
                } else {
                    var voidAttr = _.reject(this.HCModel.get("arrgmactivityattrvo"), function (data) {
                        if (parseNull(indexVal) != "") {
                            return data.attrtype == attrtype && data.index == indexVal;
                        } else {
                            return data.attrtype == attrtype && data.attrval == attrval;
                        }
                    });
                    this.HCModel.set("arrgmactivityattrvo", voidAttr);
                }
                if (removeFl == "Y")
                    $(event.currentTarget).parent('.div-sc-extn').remove();
                else
                    this.render();
            },

            // Search Option for the hospital field
            searchHospital: function () {
                var that = this;
                var fnName = "";

                var searchOptions = {
                    'el': $("#" + this.module + "-hospital-search"),
                    'url': URL_WSServer + 'search/account',
                    'inputParameters': {
                        'token': localStorage.getItem('token'),
                        'stropt': 'parent'
                    },
                    'keyParameter': 'accnm',
                    'IDParameter': 'accid',
                    'NameParameter': 'accnm',
                    'resultVO': 'gmAccountBasicVO',
                    'minChar': 3,
                    'callback': function (result) {
                        that.getAccntInfo(result.ID, result.Name, result.Element);
                    }
                };
                this.search3 = new GMVSearch(searchOptions);
            },

            //Set Hospital Values in the hospital search box
            getAccntInfo: function (ID, Name, Element) {
                $("#" + this.module + "-hospital-search input").val(Name);
                $("#" + this.module + "-hhospitalnm").val(Name);
                $("#" + this.module + "-hhospitalid").val(ID);
                $("#" + this.module + "-hhospitalnm").trigger("change");
                $("#" + this.module + "-hhospitalid").trigger("change");
                this.render();
            },
            
             // Add new Hospital value
            addNewAcnt: function(){
                var that = this;
                var newAcntName = $("#preceptorship-new-account-search").val();
                if(that.newAcntName != ''){
	                $("#" + this.module + "-hhospitalnm").val(newAcntName);
	                $("#" + this.module + "-hhospitalid").val("");
	                $("#" + this.module + "-hhospitalnm").trigger("change");
	                $("#" + this.module + "-hhospitalid").trigger("change");
	                this.render();
                }
            },

            // Back to Previous scrren on clicking the back icon
            backToPage: function () {
                if (GM.Global.PSHost.Mode == "edit")
                    Backbone.history.loadUrl(Backbone.history.fragment);
                else
                    window.history.back();
            },

            // place the selected values in the dropdown 
            setSelect: function (controlid, selectedvalue) {
                $(controlid).val(selectedvalue);
            },

            // Select Date from the dropdown calendar
            selectDate: function (e) {
                var target = $(e.currentTarget).attr("id");
                var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth() + 1;
                var yyyy = today.getFullYear();
                if (dd < 10)
                    dd = '0' + dd
                if (mm < 10)
                    mm = '0' + mm

                var hours = today.getHours();
                var minutes = today.getMinutes();
                var currntTime = hours + ':' + minutes;
                today = yyyy + '-' + mm + '-' + dd;
                var startdate = $("#" + this.module + "-actstartdate").val();
                var starttime = $("#" + this.module + "-actstarttime").val();
                var endtime = $("#" + this.module + "-actendtime").val();


                if (startdate != "" && starttime == "" && endtime == "") {
                    $("#" + this.module + "-actstarttime").val("09:00").trigger("change");
                    $("#" + this.module + "-actendtime").val("15:00").trigger("change");
                }
                if (starttime != "" && endtime != "" && endtime < starttime)
                    $("#" + this.module + "-actendtime").val(starttime).trigger("change");

            },

            //Update Model on change in Input, select and dropdown values
            updateModel: function (e) {
                var that = this;
                if ($(e.currentTarget).attr("name") == "actstartdate" || "actstarttime" || "actendtime")
                    that.selectDate(e);
                var control;
                if (e.currentTarget.tagName == "SELECT") {
                    control = $(e.currentTarget).find("option:selected");
                } else {
                    control = $(e.currentTarget);
                }
                var attribute = control.attr("name");
                var partyid = control.attr("partyid");
                var VO = control.attr("VO");
                var value = control.val();
                var attrval = control.val();
                var attrtype = control.attr("attrtype");
                var attrnm = control.attr("attrnm");
                var attrid = control.attr("attrid");
                var logid = control.attr("data-logid");
                var updAfl = control.attr("updAfl");
                var index = control.attr("index");

                this.oldDivision = this.HCModel.get("division");
                this.oldActType = this.HCModel.get("acttype");

                if ((attribute != undefined) || (value != undefined)) {
                    if (VO == undefined) {
                        this.HCModel.set(attribute, value);
                    } else {
                        if (VO == "arrgmactivitypartyvo") {
                            this.updatePartyVO(partyid, attribute, value);
                        }
                        if (VO == "arrgmactivityattrvo") {
                            this.updateAttrVO(attrid, attrtype, attrval, attrnm, updAfl, index);
                        }
                    }
                }

            },

            // Update Party VO Surgeon Info and Field Sales Attendees
            updatePartyVO: function (partyid, attribute, value) {

                var that = this;

                var partyVO = [];
                var partyVOItem;
                var actpartyid = "";
                
                // Select Single Surgeon Instead of Multiple
//                partyVOItem = _.findWhere(this.HCModel.get("arrgmactivitypartyvo"), {
//                    "roleid": "105564", "voidfl": ""
//                });
//                if (partyVOItem) {
//                    if (parseNull(partyVOItem.actpartyid) != "") {
//                        _.each(this.HCModel.get("arrgmactivitypartyvo"), function (data) {
//                            if (data.actpartyid == partyVOItem.actpartyid)
//                                data.voidfl = "Y";
//                        });
//                        partyVOItem = undefined;
//                    }
//                    
//                }                
//                //END
//                if (!partyVOItem){
                partyVOItem = _.findWhere(this.HCModel.get("arrgmactivitypartyvo"), {
                    "partyid": partyid
                });
//                }
                
                var obj = _.findWhere(this.HCModel.get("arrgmactivitypartyvo"), {
                    "partyid": partyid,
                    "voidfl": "Y"
                });

                if (!partyVOItem && !obj) {
                    partyVOItem = {
                        "actpartyid": actpartyid,
                        "actid": "",
                        "partyid": partyid,
                        "partynpid": "",
                        "partynm": "",
                        "partytype": "",
                        "roleid": "",
                        "rolenm": "",
                        "document": "",
                        "statusid": "",
                        "statusnm": "",
                        "voidfl": ""
                    };
                } else if (obj) {
                    _.each(this.HCModel.get("arrgmactivitypartyvo"), function (a) {
                        if (a.partyid == partyid)
                            a.voidfl = "";
                    })
                }

                partyVO = _.without(this.HCModel.get("arrgmactivitypartyvo"), partyVOItem);
                partyVOItem[attribute] = value;
                partyVO.push(partyVOItem);


                this.HCModel.get("arrgmactivitypartyvo").length = 0;
                partyVO = arrayOf(partyVO);
                $.each(partyVO, function (index, item) {
                    that.HCModel.get("arrgmactivitypartyvo").push(item);
                });


            },

            // Update Attribute VO Hospital and Product Info
            updateAttrVO: function (attrid, attrtype, attrval, attrnm, updAfl, index) {                
                var that = this;
                var attrVO = [];
                var attrVOItem;
                this.HCModel.set("arrgmactivityattrvo", arrayOf(this.HCModel.get("arrgmactivityattrvo")));

                attrVOItem = _.findWhere(this.HCModel.get("arrgmactivityattrvo"), {
                            "attrval": attrval
                        });
                
                if (attrVOItem) {
                    attrVO = _.without(this.HCModel.get("arrgmactivityattrvo"), attrVOItem);

                    attrVOItem["attrnm"] = attrnm;
                    attrVOItem["attrtype"] = attrtype;

                    if (parseNull(updAfl) == "")
                        attrVOItem["attrval"] = attrval;

                    attrVO.push(attrVOItem);

                    this.HCModel.get("arrgmactivityattrvo").length = 0;

                    $.each(attrVO, function (index, item) {
                        that.HCModel.get("arrgmactivityattrvo").push(item);
                    });

                } else {
                    this.HCModel.get("arrgmactivityattrvo").push({
                        "attrid": "",
                        "attrnm": attrnm,
                        "attrval": attrval,
                        "attrtype": attrtype,
                        "voidfl": "",
                        "index": index
                    });
                }
                if(!that.cancelActSts){
                that.cancelActSts=false;
                this.render();
                }
            },
            
            // Check the date entered is past Would you like to continue return true or false
            chkDt: function (gvndt, cb) {
                var that = this;
                var currentDt = new Date();
                var givenDt = new Date(gvndt); 
                if (givenDt > currentDt)
                    cb(true);
                else {
                    dhtmlx.confirm({
                                title: "Host Case Date Notificaion",
                                ok: "Yes",
                                cancel: "No",
                                type: "confirm",
                                text: "The Host Case Date you entered is past. Would you like to continue? ",
                                callback: function (result) {
                                    cb(result);
                                }
                });
                }                  
            },

            // Save Host Case Activity
            saveHost: function (event) {
                this.HCModel.unbind("change");
                var that = this;
                var input = "";

                var fromDate = this.HCModel.get("actstartdate");
                var fromTime = this.HCModel.get("actstarttime");
                var toTime = this.HCModel.get("actendtime");
                
                var date = getDateByFormat(fromDate);
                
                if(parseNull(this.actid) == 0)
                    this.HCModel.set("actstatus", "105765");
                
                var acttzone = this.HCModel.get("tzone");

                var phyStatus = this.HCModel.get("actstatus");
                if (phyStatus == "105765" && parseNull(this.actid) == 0)
                    this.mailStatus = 1; // request

                if (parseNull(this.actid) != 0 && event.currentTarget.id == "li-save") {
                    this.mailStatus = 2; // update
                }
                
                if (GM.Global.PSHost.Mode == "create") 
                    that.setActTitle();
                


                var input = this.HCModel;
                input.set("token", localStorage.getItem("token"));
                input.set("actcategory", "105272");
                if (input.get('arrgmcrmaddressvo')[0] == null || input.get('arrgmcrmaddressvo')[0] == undefined)
                    input.set('arrgmcrmaddressvo', []);
                if (input.get('arrgmcrmlogvo')[0] == null || input.get('arrgmcrmlogvo')[0] == undefined)
                    input.set('arrgmcrmlogvo', []);
                if (input.get('arrgmactivityattrvo')[0] == null || input.get('arrgmactivityattrvo')[0] == undefined)
                    input.set('arrgmactivityattrvo', []);
                if (input.get('arrgmactivitypartyvo')[0] == null || input.get('arrgmactivitypartyvo')[0] == undefined)
                    input.set('arrgmactivitypartyvo', []);
                _.each(input.get('arrgmactivitypartyvo'), function (data) {
                    delete data.server;
                });
                console.log(input);
                that.searchValidations();
                if (this.gmvValidation.validate() && that.srchValid) {
                    var getDt = compDateFmt(input.get("actstartdate"));
                    var gettime = input.get("actstarttime") + ":" + "00";
                    that.chkDt(getDt +" "+ gettime, function(val){
                    if(val == true){                 
                    if (date && date != "aN/aN/NaN")
                    input.set("actstartdate", date);
                input.set("actenddate", input.get("actstartdate"));
                    input.set("actstarttime", fromTime + " " + acttzone);
                    input.set("actendtime", toTime + " " + acttzone);
                    fnGetWebServerData("crmactivity/save", undefined, input, function (data) {
                        console.log("Acitivty Save OUTPUT" + JSON.stringify(data));

                        if (data != null) {
                            showSuccess("Host Case saved successfully");
                            var id = data.actid;
                          
//                            that.eMailConfirm(id);
                            that.sendHostEmail(id, "N");
                            
                            if (event.currentTarget.id == "btn-main-void")
                                window.location.href = "#crm/" + that.module;
                            else if (that.actid == undefined || that.actid == 0)
                                window.location.href = "#crm/" + that.module + "/host/id/" + id;
                            else
                                Backbone.history.loadUrl(Backbone.history.fragment);

                            GM.Global.PSHost.Mode = "view";

                        } else
                            showError("Activity saved Failed");
                    });
                    }                    
                    });
                } else
                    showError(this.gmvValidation.getErrors());

            },
            
            // Validations for Search Fields host case surgeon, procedures
            searchValidations: function () {
                var that=this;
                var surgeonChilds = $(".surgeon-items").children().length;
                var hospitalChilds = $(".hospital-items").children().length;
                var procedureChilds = $(".procedure-items").children().length;

                this.srchValid = true;
                if ((surgeonChilds == 0) || (procedureChilds == 0))
                    this.srchValid = false;
                if (that.HCModel.get("division") != "2004"){  // If Division Type is NOT Equal to INR
                if(hospitalChilds == 0)
                    this.srchValid = false;
                
                if (hospitalChilds == 0) {
                    $("#preceptorship-hospital-search input").addClass("gmValidationError");
                    $("#preceptorship-hospital-search input").attr("message", "Please provide Hospital");
                } else {
                    $("#preceptorship-hospital-search input").removeClass("gmValidationError");
                }  
                }
                
                if(that.HCModel.get("division") == "2004"){
                    //Preceptor Type Validation when choose INR Division
                    if ($("#pstyp").val() == "") {
                        $("#pstyp").attr("validate", "required");
                        $("#pstyp").attr("message", "Please provide Preceptorship Type");
                        $("#pstyp").addClass("gmValidationError");
                    }
                }
                else {
                        $("#pstyp").removeClass("gmValidationError");
                        $("#pstyp").attr("validate", "");
                }
                
                if (surgeonChilds == 0) {
                    $("#preceptorship-hc-surgeon-search input").addClass("gmValidationError");
                    $("#preceptorship-hc-surgeon-search input").attr("message", "Please provide Host Case Surgeon");
                } else {
                    $("#preceptorship-hc-surgeon-search input").removeClass("gmValidationError");
                }
                if (procedureChilds == 0) {
                    $("#preceptorship-procedure-search input").addClass("gmValidationError");
                    $("#preceptorship-procedure-search input").attr("message", "Please provide Procedure");
                } else {
                    $("#preceptorship-procedure-search input").removeClass("gmValidationError");
                }
            },

            //Edit Host Case 
            editHost: function () {
                var that = this;
                GM.Global.PSHost.Mode = "edit";
                this.render();
            },

            //On clicking view Report Request List In Popup
            showHostRqReport: function () {

            },

            //Toggle Preceptorship Type Info
            openNotifyPSType: function () {
                if (this.HCModel.get("division") == "2004") {
                    $("#rpt-legend-detail-inr").slideToggle();
                } else {
                    $("#rpt-legend-detail").slideToggle();
                }
            },

            //validation for npid
            checkNumber: function (e) {
                if (e.which != 8 && e.which != 0 && e.which != 13 && (e.which < 48 || e.which > 57) && (e.which < 96 || e.which > 105)) {
                    $(e.currentTarget).val("");
                }
            },
            
            // New Method to allow only the valid characters like Numbers, + -,()        
            checkPhoneNumber: function(e){ 
                if((e.which == 32 || e.which == 40 || e.which == 41 || e.which == 43 || e.which == 45)||((e.which > 47) && (e.which < 58))){
                   return true;       
                }
                else{
                    e.preventDefault();
                }
            },

            //validation for npid
            checkNpid: function (e) {
                var that = this;
                var val;
                $("#img-npid-spinner").toggleClass('hide');
                $(e.currentTarget).toggleClass('hide');
                if (this.newSurgeon == "true") {
                    val = $("#input-new-surgeon-npid").val();
                    this.newSurgeon = "false"
                } else
                    val = $("#input-lead-contacts-npid").val();
                var re = /[^0-9]/g;
                var error = re.test(val);
                //            hideMessages();
                if (val != "") {
                    if (error == true) { //npid should be numberic and with max length of 10
                        $("#popup-create-surgeon-validation").html("NPI Should be Numeric");
                        $("#img-npid-spinner").toggleClass('hide');
                        $(e.currentTarget).toggleClass('hide');
                        $(e.currentTarget).val("").focus();
                    } else {
                        $("#popup-create-surgeon-validation").html("");
                        getSurgeonPartyInfo(val, function (surgeonData) {
                            var partyid;
                            if (that.newSurgeon == "true")
                                partyid = 0;
                            else
                                partyid = GM.Global.SurgeonPartyId; //calling surgeon's party information of typed in npid                                                                                                                                                        //that.chkNPI == 1 already allocated;  that.chkNPI == 0 npid verified

                            if (surgeonData != undefined) {
                                surgeonData = surgeonData[0];
                                if (partyid == 0) {
                                    $(".npi-detection.fa-check-circle-o").remove();
                                    if ($(".npi-detection.fa-times-circle").length == 0)
                                        $(".li-popup-npid-create").append("<span class='fa npi-detection fa-times-circle'></span>");
                                    that.chkNPI = 1;
                                } else {
                                    if (surgeonData.partyid == partyid) {
                                        $(".npi-detection.fa-times-circle").remove();
                                        if ($(".npi-detection.fa-check-circle-o").length == 0)
                                            $(".li-popup-npid-create").append("<span class='fa npi-detection fa-check-circle-o'></span>");
                                        that.chkNPI = 0;
                                    } else {
                                        $(".npi-detection.fa-check-circle-o").remove();
                                        if ($(".npi-detection.fa-times-circle").length == 0)
                                            $(".li-popup-npid-create").append("<span class='fa npi-detection fa-times-circle'></span>");
                                        that.chkNPI = 1;
                                    }
                                }
                            } else {
                                $(".npi-detection.fa-times-circle").remove();
                                if ($(".npi-detection.fa-check-circle-o").length == 0)
                                    $(".li-popup-npid-create").append("<span class='fa npi-detection fa-check-circle-o'></span>");
                                that.chkNPI = 0;
                            }
                            $("#img-npid-spinner").toggleClass('hide');
                            $(e.currentTarget).toggleClass('hide');
                        });
                    }

                } else { //empty
                    $("#popup-create-surgeon-validation").html("");
                    if ($(".npi-detection.fa-times-circle").length != 0)
                        $(".npi-detection.fa-times-circle").remove();
                    if ($(".npi-detection.fa-check-circle-o").length != 0)
                        $(".npi-detection.fa-check-circle-o").remove();
                    $("#img-npid-spinner").toggleClass('hide');
                    $(e.currentTarget).toggleClass('hide');
                    that.chkNPI = 0;
                }
            },



            // Create a New Surgeon in the Popup
            createSurgeon: function () {
                var that = this;
                var newSurgeonValidation = "true";
                showPopup();
                $("#div-crm-overlay-content-container").find('.load-model').addClass("hide");
                $("#div-crm-overlay-content-container").find('.done-model').addClass("hide");
                $("#div-crm-overlay-content-container").find('.save-model').removeClass("hide");
                $("#div-crm-overlay-content-container").find('.modal-title').text("Key in a new Surgeon");
                $("#div-crm-overlay-content-container .modal-body").html(this.template_CreateSurgeon());

                //getSelectState();
                getCLValToDiv(".select-address-state", "", "", "STATE", "", "", "codeid", "", false, "Y", function (divID, attrVal) {});
                
                //getSelectCountry();
                getCLValToDiv(".select-address-country", "", "", "CNTY", "", "", "codeid", "", false, "Y", function (divID, attrVal) {});
                
                var userData = JSON.parse(localStorage.getItem("userData"));
                //Input for fetching company List for company dropdown
                var input = {
                    'token': localStorage.getItem('token'),
                    'companyid': '',
                    'partyid': localStorage.getItem('partyID')
                };
                var defaultid = userData.cmpid;
                var elemAdmin = "#surg-company-dropdown";
                
                companyDropdown(input,defaultid,"",DropDownView,elemAdmin,function(selectedId){
                    that.companyid = selectedId;
                });
                
                $("#div-crm-overlay-content-container .close").on('click', function () {
                    $("#div-crm-overlay-content-container").find(".modal-body").empty();
                });
                
                $("#div-crm-overlay-content-container").find('#input-new-surgeon-npid').unbind('blur').bind('blur', function (e) {
                    that.newSurgeon = "true";
                    that.checkNpid(e);
                });
                $("#div-crm-overlay-content-container").find('.input-number-validation').unbind('keyup').bind('keyup', function (e) {
                    that.checkNumber(e);
                });
                 $("#div-crm-overlay-content-container").find('.input-phonenumber-validation').unbind('keypress').bind('keypress', function (e) {
                    that.checkPhoneNumber(e);
                });
                $("#div-crm-overlay-content-container").find('.save-model').unbind('click').bind('click', function () {
                    if ($("#img-npid-spinner").hasClass('hide') ) { // Denying save when NPID is validating

                        var str = "";
                        var emailPattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
                        var numberPattern = new RegExp(/^[0-9- +()]*$/);
                        var newSurgNPI = $("#input-new-surgeon-npid").val();

                        if ($("#input-new-surgeon-firstnm").val().trim() == "")
                            str = "Firstname";
                        if ($("#input-new-surgeon-lastnm").val().trim() == "")
                            str = (str.trim().length) ? (str + " / " + "Lastname") : "Lastname";
                        if (that.companyid == "undefined" || that.companyid == "0")
                            str = (str.trim().length) ? (str + " / " + "Company") : "Company";
                        if (newSurgNPI.trim() != "" && newSurgNPI.length != 10)
                            str = (str.trim().length) ? (str + " / " + "NPI should contain numeric characters only and should be 10-digit number") : "NPI should contain numeric characters only and should be 10-digit number";
                        if ($("#input-new-surgeon-email").val().trim() != "" && !emailPattern.test($("#input-new-surgeon-email").val().trim()))
                            str = (str.trim().length) ? (str + " / " + "Wrong Email Fomat") : "Wrong Email Fomat";
                        var newSurgPhNo = $("#input-new-surgeon-phone").val();
                        if (newSurgPhNo.trim() != "" && !numberPattern.test($("#input-new-surgeon-phone").val().trim()) && newSurgPhNo.length > 25)
                            str = (str.trim().length) ? (str + " / " + "Please enter Phone Number and should be 10-digit number") : "Please enter Phone Number and should be 10-digit number";
                        var addr = "";
                        if ($("#input-new-surgeon-add1").val().trim() != "" || $("#input-new-surgeon-city").val() != "" || $("#div-new-surgeon-content .select-address-state").val().trim() != "" || $("#input-new-surgeon-zip").val().trim() != "" || $("#div-new-surgeon-content .select-address-country").val().trim() != "") {
                            var confirm = true;
                            if ($("#input-new-surgeon-add1").val().trim() == "")
                                addr = (addr.trim().length) ? (addr + " / " + "Address1") : "Address1";
                            if ($("#input-new-surgeon-city").val().trim() == "")
                                addr = (addr.trim().length) ? (addr + " / " + "City") : "City";
                            
                            if($("#div-new-surgeon-content .select-address-country").val().trim() == '1101'){
                            if ($("#div-new-surgeon-content .select-address-state").val().trim() == "")
                                addr = (addr.trim().length) ? (addr + " / " + "State") : "State";
                            if ($("#input-new-surgeon-zip").val().trim() == "")
                                addr = (addr.trim().length) ? (addr + " / " + "Zip Code") : "Zip Code";
                            if ($("#input-new-surgeon-zip").val().trim() != "" && $("#input-new-surgeon-zip").val().trim().length > 10)
                                addr = (addr.trim().length) ? (addr + " / " + "Zip can have maximun of 10 characters") : "Zip can have maximun of 10 characters";
                            }

                            if ($("#div-new-surgeon-content .select-address-country").val().trim() == "")
                                addr = (addr.trim().length) ? (addr + " / " + "Country") : "Country";
                        }
                        if (str.length)
                            str = (addr.length && addr.length) ? (str + " / " + addr) : str;
                        else if (addr.length)
                            str = addr;
                        var surgstr = " Mandatory Fields: " + str;
                        var firstName = $("#input-new-surgeon-firstnm").val().trim();
                        var lastName = $("#input-new-surgeon-lastnm").val().trim();
                        var midName = $("#input-new-surgeon-midinitial").val().trim();
                        if (firstName != "" && lastName != "") {
                            var input = {
                                "token": localStorage.getItem("token"),
                                "firstname": firstName,
                                "lastname": lastName,
                                "middleinitial": midName
                            };
                            fnGetWebServerData("surgeon/info", "gmCRMSurgeonlistVO", input, function (data) {
                                console.log("Surgeon Data >>>>>>>>" + JSON.stringify(data));
                                if (data != undefined && data.length > 0)
                                    $("#popup-create-surgeon-validation").text(firstName + " " + lastName + (midName ? (" " + midName) : "") + "'s information already exists");
                                else if (str.length)
                                    $("#popup-create-surgeon-validation").text(surgstr);
                                else {
                                    var surgeonInput = {
                                        "token": localStorage.getItem("token"),
                                        "partyid": "",
                                        "npid": newSurgNPI,
                                        "firstnm": $("#input-new-surgeon-firstnm").val(),
                                        "lastnm": $("#input-new-surgeon-lastnm").val(),
                                        "partynm": "",
                                        "midinitial": $("#input-new-surgeon-midinitial").val(),
                                        "asstnm": "",
                                        "mngrnm": "",
                                        "companyid": that.companyid,

                                        "arrgmsurgeonsaddressvo": [],
                                        "arrgmsurgeoncontactsvo": [],
                                        "arrgmcrmsurgeonaffiliationvo": [],
                                        "arrgmcrmsurgeonsurgicalvo": [],
                                        "arrgmcrmsurgeonactivityvo": [],
                                        "arrgmcrmsurgeonattrvo": [],
                                        "arrgmcrmfileuploadvo": []
                                    };
                                    if (isRep()) {
                                        surgeonInput.arrgmcrmsurgeonattrvo.push({
                                            "attrtype": 11509,
                                            "attrvalue": localStorage.getItem("partyID"),
                                            "attrnm": localStorage.getItem("fName") + " " + localStorage.getItem("lName"),
                                            "attrid": "",
                                            "voidfl": ""
                                        });
                                    }

                                    if (addr == "" && confirm == true) {
                                        surgeonInput.arrgmsurgeonsaddressvo.push({
                                            "addrtyp": "90400",
                                            "addid": "",
                                            "add1": $("#input-new-surgeon-add1").val(),
                                            "add2": $("#input-new-surgeon-add2").val(),
                                            "city": $("#input-new-surgeon-city").val(),
                                            "statenm": $(".select-address-state option:selected").text(),
                                            "stateid": $(".select-address-state option:selected").val(),
                                            "zip": $("#input-new-surgeon-zip").val(),
                                            "countrynm": $(".select-address-country option:selected").text(),
                                            "countryid": $(".select-address-country option:selected").val(),
                                            "primaryfl": "Y"
                                        });
                                    }
                                    if(newSurgPhNo != ""){
                                        surgeonInput.arrgmsurgeoncontactsvo.push({
                                                "contype": "Home",
                                                "conmode": "90450",
                                                "convalue": newSurgPhNo,
                                                "conid": "",
                                                "primaryfl": "Y",
                                                "voidfl": ""
                                        });
                                    }
                                    if($("#input-new-surgeon-email").val() != ""){
                                        surgeonInput.arrgmsurgeoncontactsvo.push({
                                                "contype": "Home",
                                                "conmode": "90452",
                                                "convalue": $("#input-new-surgeon-email").val(),
                                                "conid": "",
                                                "primaryfl": "Y",
                                                "voidfl": ""
                                        });
                                    }
                                    hidePopup();
                                    console.log(JSON.stringify(surgeonInput));
                                    fnGetWebServerData("surgeon/save", undefined, surgeonInput, function (data) {
                                        if (data != null) {
                                            data.Name = parseNull(data.salutenm) + " " + data.firstnm + " " + parseNull(data.midinitial) + " " + data.lastnm;
                                            data.ID = data.partyid;
                                            showSuccess("Surgeon Detail created successfully");
                                            if (that.HCModel.get("arrgmactivityattrvo")[0] != undefined) {
                                                _.each(that.HCModel.get("arrgmactivityattrvo"), function (data) {
                                                    if (data.attrtype == 7800)
                                                        data.attrval = "";
                                                })
                                                _.each(that.HCModel.get("arrgmactivitypartyvo"), function (pData) {
                                                    if (pData.roleid == 105564)
                                                        pData.partynpid = "";
                                                })
                                            }
                                            that.setPartyInfo(data, "7000", "105564");
                                        } else
                                            showError("Save Failed");
                                    });

                                    $("#div-crm-overlay-content-container").find('.modal-title').text("");
                                    $("#div-crm-overlay-content-container").find(".modal-body").empty();
                                }
                            });
                        } else
                            $("#popup-create-surgeon-validation").text(surgstr);
                    } 
//                    else if (that.chkNPI != 0)
//                        $("#popup-create-surgeon-validation").text("Enter valid NPID");
                });
            },

            
            // Confirm mail box show/hide 
            eMailConfirm: function (actid) {
                var that = this;
                if (this.mailStatus != 4) {                    
                dhtmlx.confirm({
                title:"Host Case Email Notificaion",
                ok:"Yes", cancel:"No",
                type:"confirm",
                text:"Do you want to send an update notification to users? ",
                callback:function(result){
                    if(result)
                        that.sendHostEmail(actid, "Y");
                    else
                        that.sendHostEmail(actid, "N");
                }
                });
                } else {
                    this.sendHostEmail(actid, "Y");
                }
            },
            
            // Send mail to Jwebservice
            // mailstatus : 1-Request, 2-Update, 3-Busy, 4-Cancel
            sendHostEmail: function (actid, notifyfl) {
                var that = this;
                var input = {
                    "token": GM.Global.Token,
                    "content": "",
                    "toaddr": "",
                    "ccaddr": "",
                    "subj": "",
                    "actid": actid,
                    "isevent": "Y",
                    "mailstatus": this.mailStatus,
                    "notifyfl": notifyfl,
                    "includesurgeon": parseNull(this.includeSurgeonFl),
                    "actcategory" : "105272",
                    "emailfl" : "Y",
                    "eventfl" : "Y"
                };
                console.log(JSON.stringify(input));
                fnGetWebServerData("crmmail/activity", undefined, input, function (data) {
                    console.log(JSON.stringify(data));
                    if (data.mstatus == "success") {
                        var msg = (notifyfl == "Y") ? "Invite Sent!" : "Event updated in the calendar";
                        showSuccess(msg);
                    } else
                        showError("Failed sending invitation. Please try again later");
                }, true);
            },

            cancelHost: function (e) {
                var that = this;
                showPopup();
                $("#div-crm-overlay-content-container").find('.submit-model').removeClass("hide");
                $("#div-crm-overlay-content-container").find('.cancel-model').removeClass("hide");
                $("#div-crm-overlay-content-container").find('.done-model').addClass("hide");
                $("#div-crm-overlay-content-container").find('.load-model').addClass("hide");
                $("#div-crm-overlay-content-container").find('.modal-title').text("REASON FOR CANCELLATION");
                $("#div-crm-overlay-content-container .modal-body").html(this.template_VisitCancelReq());
                getCLValToDiv("#canreson", "", "", "HOSCAN", "", "", "codeid", "canreson", false, true, function(divID, attrVal){});

                $("#cancel-req-error").css("display", "none");
                $("#div-crm-overlay-content-container").find('.submit-model').unbind('click').bind('click', function () {
                    var reasonval = $("#canreson option:selected").val();
                    var commentVal = $("#textarea-visit-cancel-req").val();
                    if (parseNull(reasonval) != "" && parseNull(commentVal) != "") {
                        $("#div-crm-overlay-content-container").find('.submit-model').addClass("hide");
                        $("#div-crm-overlay-content-container").find('.cancel-model').addClass("hide");
                        var reasonNm = $("#canreson option:selected").text();
                        var reasonID = $("#span-visit-reason").attr("data_attrid");
                        that.cancelActSts=true;
                        that.updateAttrVO(reasonID, "107582", reasonval, reasonNm);
                        var commentID = $("#span-visit-comment").attr("data_attrid");
                        that.updateAttrVO(commentID, "26240350", commentVal, "cancmt");
                        hidePopup();
                        that.HCModel.set("actstatus", "105762");
                        that.mailStatus = 4; // cancel
                        that.saveHost(e);
                    } else if (parseNull(reasonval) == "")
                        $("#cancel-req-error").html("Please select reason").slideDown('fast').css("display", "block").delay(4000).slideUp('slow');
                    else if (parseNull(commentVal) == "")
                        $("#cancel-req-error").html("Please enter the comment").slideDown('fast').css("display", "block").delay(4000).slideUp('slow');
                });
                $("#div-crm-overlay-content-container").find('.cancel-model').unbind('click').bind('click', function () {
                    $("#div-crm-overlay-content-container").find('.submit-model').addClass("hide");
                    $("#div-crm-overlay-content-container").find('.cancel-model').addClass("hide");
                    hidePopup();
                    that.backToPage();
                });
            },
            
            //Show Attend Surgeon Details Grid
             showAttendSurgDetailGrid: function () {
                var that = this;
                this.attndSurgObj = "";
                 var lkinput = {
                    'token': localStorage.getItem('token')
                };
                lkinput.segid=that.HCModel.get("segid");
                lkinput.actid=that.HCModel.get("actid");
                 fnGetWebServerData("psrpt/fetchlkupcreq",undefined, lkinput, function (data) {
                    console.log(data)
                    var schdldData = _.filter(data, function (dt) {
                        return (dt.status == 105761 || dt.status == 105764);
                     });
                     console.log(schdldData)
                     if(schdldData.length>0){
                var rows = [], i = 0;
                            _.each(schdldData, function (griddt) {                             
                                rows[i] = {};
                                rows[i].id = i;
                                rows[i].data = [];
                                if(griddt.atndtyp != 109000 && griddt.atndtyp != null)
                                    rows[i].data[0] = '<span class="mob-surg-pass" data-atndtyp="'+griddt.atndtyp+'">'+griddt.atndnm+'</span>';
                                else
                                    rows[i].data[0] = '<span class="mob-surg-pass" data-surgid="'+griddt.surgid+'">'+griddt.surgnm+'</span>';
                                rows[i].data[1] = griddt.casereqid;
                                i++;                        
                            });
                var surgData = {};
                     surgData.rows = rows;
                var setInitWidths = "400,*";
                var setColAlign = "left,left";
                var setColTypes = "ro,ro";
                var setColSorting = "str,str";
                var setHeader = ["Name", "Request ID"];
                var enableTooltips = "true,true";
                var footerArry = [];
                var gridHeight = "210";
                var setFilter = "";
                var footerStyles = [];
                var footerExportFL = false;
                that.attndSurgObj = loadDHTMLXGrid('gridAttendSurgDetails', gridHeight, surgData, setHeader, setInitWidths, setColAlign, setColTypes, setColSorting, enableTooltips, setFilter, footerArry, footerStyles, footerExportFL);               
                     for (var i = 0; i < that.attndSurgObj.getRowsNum(); i++) {
                         var nameCellObj = that.attndSurgObj.cells(i, 0).getValue();
                         if($(nameCellObj).attr("data-atndtyp") != undefined)
                             that.attndSurgObj.cells(i, 0).cell.className = 'gmEllipses';
                         else
                             that.attndSurgObj.cells(i, 0).cell.className = 'gmReportLnk gmEllipses';
                        that.attndSurgObj.cells(i, 1).cell.className = 'gmReportLnk gmEllipses';
                     }                
                that.attndSurgObj.attachEvent("onRowSelect", function (id, index) {
                    if (index == 0){
                        var element=that.attndSurgObj.cells(id, 0).getValue();
                        if($(element).attr("data-surgid") != undefined)
                            window.location.href = '#crm/surgeon/id/'+$(element).data('surgid');
                    }
                    else if(index==1)
                        window.location.href = '#crm/preceptorship/case/id/'+that.attndSurgObj.cells(id, 1).getValue();
                });
                     }
                     else
                         {
                             $("#gridAttendSurgDetails").html("No Data Found");
                         }
                            });

            },
            
              selectedDone: function(){
                var that=this;
                var input = {
                    'token': localStorage.getItem('token')
                };
                
                  var linkreqs=[], unlinkreqs=[],existFl=0;
                  for (var i = 0; i < that.gridObj.getRowsNum(); i++) {
                        var isCheck = that.gridObj.cells(i, 0).getValue();
                        var element = that.gridObj.cells(i, 4).getValue();
                        var hcid = $(element).data('hcid');
                        var casereq = $(element).text();
                      
                        //how many checked
                        existFl +=isCheck;
                        
                      if((isCheck==1) && (parseNull(hcid) == ""))
                          linkreqs.push(casereq);
                      else if((isCheck==0) && (parseNull(hcid) != ""))
                         unlinkreqs.push(casereq);
                    };
                   console.log("linkreqs>>>>>>>"+linkreqs);
                   console.log("unlinkreqs>>>>>>>>"+unlinkreqs);
                  input.linkreqs=linkreqs;
                  input.unlinkreqs=unlinkreqs;
                  input.actid=that.HCModel.get("actid");
                  if(linkreqs.length>0 || unlinkreqs.length>0){
                      console.log("Case Request Mapping Input >>>" + JSON.stringify(input));
                    fnGetWebServerData("pstxn/casereqmap",undefined, input, function (data) {  
                        console.log("Case Request Mapping Output >>>" + JSON.stringify(data));
                        if(data != null){
                        hidePopupPSReport();
                            var message=""
                            if(linkreqs.length>0)
                                message+= linkreqs.toString() + " Successfully Assigned to " + input.actid;
                            if(linkreqs.length>0 && unlinkreqs.length>0)
                                message+= " , ";
                            if(unlinkreqs.length>0)
                                message+= unlinkreqs.toString() + " Successfully Unassigned to " + input.actid;
                        showSuccess(message);
                        Backbone.history.loadUrl();
                        }
                        else{
                             hidePopupPSReport();
                         showError("Save Failed");  
                        }
                    });
                }
                  else
                  	if(existFl>0){
                        displayPopupMsg("Nothing changed to Save");
                  	}else{
                  	 displayPopupMsg("Select Atleast One Case Request Before Clicking Done Button");
                  	}
                
            },
            
            // Fetch Case Request to display in lookup case request popup
            showLkupCaseReq: function (){
                
                 var that = this;
                
                console.log(that.HCModel.toJSON());
                
                var lkinput = {
                    'token': localStorage.getItem('token')
                };
                lkinput.segid=that.HCModel.get("segid");
                lkinput.actid=that.HCModel.get("actid");
                
                var title=that.HCModel.get("actnm");
                
                showPopupPSReport();
                $("#div-crm-overlay-content-container .modal-title").text(title);
                $("#div-crm-overlay-content-container .modal-body").html("<div id='gridPreceptorRpt'></div>");
                getLoader("#gridPreceptorRpt");
                $("#div-crm-overlay-content-container").find('.done-model').unbind('click').bind('click', function () {
                that.selectedDone();
                });
                
                this.gridObj = "";
                console.log("Fetch Lookup Case Request List INPUT>>>>>>>>>>"+JSON.stringify(lkinput));
                fnGetWebServerData("psrpt/fetchlkupcreq",undefined, lkinput, function (data) {
                 console.log("Fetch Lookup Case Request List OUTPUT>>>>>>>>>>"+JSON.stringify(data));
                if(data != null){
                var rows = [], i = 0;
                            _.each(data, function (griddt) {
                                var rowid="creqid"+i;
                                rows[i] = {};
                                rows[i].id = i;
                                rows[i].data = [];  
                                if(that.HCModel.get("actid")==griddt.actid)
                                rows[i].data[0] = true;
                                else
                                rows[i].data[0] = false;
                                if(griddt.atndtyp != 109000 && griddt.atndtyp != null)
                                    rows[i].data[1] = '<span class="mob-surg-pass" data-atndtyp="'+griddt.atndtyp+'">'+griddt.atndnm+'</span>';
                                else
                                    rows[i].data[1] = '<span class="mob-surg-pass" data-surgid="'+griddt.surgid+'">'+griddt.surgnm+'</span>';
                                rows[i].data[2] = griddt.segnm;
                                rows[i].data[3] = griddt.products;
                                rows[i].data[4] = "<span id='"+rowid+"' data-hcid='"+griddt.actid+"' data-crid='"+griddt.casereqid+"'>"+griddt.casereqid+"</span>";
                                rows[i].data[5] = griddt.frmdt;
                                rows[i].data[6] = griddt.todt;
                                rows[i].data[7] = griddt.note;
                                i++;
                            });
                            var dataHost = {};
                            dataHost.rows = rows;
                $(".done-model").removeClass("hide");
                $(".cancel-model").removeClass("hide");
                var setInitWidths = "30,150,100,150,100,180,*";
                var setColAlign = "center,left,left,left,left,center,center,left";
                var setColTypes = "ch,ro,ro,ro,ro,ro,ro,ro";
                var setColSorting = "str,sortLink,str,str,sortLinkID,date,date,str";
                var setHeader = ["", "Name", "Procedure", "Product(s)", "Request-ID", "From Date","To Date", "Notes"];
                var setFilter = ["", "#text_filter", "#text_filter", "#text_filter", "#text_filter", "#text_filter","#text_filter", "#text_filter"];
                var enableTooltips = "true,true,true,true,true,true,true,true";
                var footerArry = [];
                var gridHeight = "";
                var setFilter = "";
                var footerStyles = [];
                var footerExportFL = true;
                that.gridObj = loadDHTMLXGrid('gridPreceptorRpt', gridHeight, dataHost, setHeader, setInitWidths, setColAlign, setColTypes, setColSorting, enableTooltips, setFilter, footerArry, footerStyles);           
                     for (var i = 0; i < that.gridObj.getRowsNum(); i++) {
                         var nameCellObj = that.gridObj.cells(i, 1).getValue();
                         if($(nameCellObj).attr("data-atndtyp") != undefined)
                             that.gridObj.cells(i, 1).cell.className = 'gmEllipses';
                         else
                             that.gridObj.cells(i, 1).cell.className = 'gmReportLnk gmEllipses';
                         that.gridObj.cells(i, 2).cell.className = 'gmEllipses';
                         that.gridObj.cells(i, 3).cell.className = 'gmEllipses';
                         that.gridObj.cells(i, 4).cell.className = 'gmReportLnk gmEllipses';
                         that.gridObj.cells(i, 6).cell.className = 'gmEllipses';
                     }                
                that.gridObj.attachEvent("onRowSelect", function (id, index) {
                    if(index==0){
                      var chkVal=that.gridObj.cells(id, 0).getValue();  
                        if(chkVal==1)
                            that.gridObj.cells(id, 0).setValue(false);
                        else
                            that.gridObj.cells(id, 0).setValue(true);
                    }
                    if (index == 1){
                        var element=that.gridObj.cells(id, 1).getValue();
                        if($(element).attr("data-surgid") != undefined){
                            hidePopupPSReport();
                            window.location.href = '#crm/surgeon/id/'+$(element).data('surgid');
                        }
                    }
                    else if(index==4){
                        var element=that.gridObj.cells(id, 4).getValue();
                        hidePopupPSReport();
                        window.location.href = '#crm/preceptorship/case/id/'+$(element).data('crid');
                    }
                });
                } else {
                    $("#gridPreceptorRpt").html("No Data Found");
                }
                });
            },
            
            
            fetchSegSysList: function() {
                var that = this;
                showPopupSegSysList();
                $("#div-crm-overlay-content-container .modal-title").text("Procedure Product Lookup");
                $("#div-crm-overlay-content-container .modal-body").html("<div id='gridSegSysRpt'></div>");
                getLoader("#gridSegSysRpt");
                
                var sysinput = {
                    'token': localStorage.getItem('token')
                };
                
                this.gridObj = "";
                fnGetWebServerData("psrpt/fetchsegsys",undefined, sysinput, function (data) {
                    if(data != null){
                        console.log("Sys and Segment List>>>>>>>> " + JSON.stringify(data));
                        var rows = [], i = 0;
                            _.each(data, function (griddt) {
                                rows[i] = {};
                                rows[i].id = i;
                                rows[i].data = [];  
                                rows[i].data[0] = '<span data-segid="'+griddt.segid+'"  data-segnm="'+griddt.segnm+'">'+griddt.segnm+'</span>';
                                rows[i].data[1] = griddt.sysnm;
                                i++;
                            });
                            var dataSys = {};
                            dataSys.rows = rows;
                            var setInitWidths = "250,*";
                            var setColAlign = "left,left";
                            var setColTypes = "ro,ro";
                            var setColSorting = "str,str";
                            var setHeader = ["Procedure", "Product"];
                            var setFilter = ["#text_filter", "#text_filter"];
                            var enableTooltips = "true,true";
                            var footerArry = [];
                            var gridHeight = "";
                            var footerStyles = [];
                            var footerExportFL = true;
                            that.gridObj = loadDHTMLXGrid('gridSegSysRpt', gridHeight, dataSys, setHeader, setInitWidths, setColAlign, setColTypes, setColSorting, enableTooltips, setFilter, footerArry, footerStyles);  
                    }
                    for (var i = 0; i < that.gridObj.getRowsNum(); i++) {
                         that.gridObj.cells(i, 0).cell.className = 'gmEllipses';
                         that.gridObj.cells(i, 1).cell.className = 'gmEllipses';
                    }
                    that.gridObj.attachEvent("onRowSelect", function (id, index) {
                        var chkVal = that.gridObj.cells(id, 0).getValue();
                        var setSegid = $(chkVal).data('segid');
                        var setSegnm = $(chkVal).data('segnm');
                        var pcrData = {
                            "ID" : setSegid,
                            "Name" : setSegnm
                        }
                        that.setProcedureInfo(pcrData);
                        hidePopupSegSysList();
                    });
                });
            },
            
            changeZoneVal: function(e){
                var that=this;
                var selRegion = $(e.currentTarget).find("option:selected").val();
                that.setSelect("#zone", ""); //set default value
                $("#zone").trigger("change");
                that.getZoneVal(selRegion,false);
            }



        });
        return GM.View.PSHost;
    });
