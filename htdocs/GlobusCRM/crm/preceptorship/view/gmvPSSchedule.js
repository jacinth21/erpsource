/**********************************************************************************
 * File             :        gmvPSSchedule.js
 * Description      :        View to represent Preceptorship Criteria
 * Path             :        /web/GlobusCRM/crm/preceptorship/view/gmvPSCriteria.js
 * Author           :        tmuthusamy
 **********************************************************************************/
define([

    // External Libraries
    'jquery', 'underscore', 'backbone', 'handlebars',

    // Common
    'global', 'commonutil', 'gmvSearch', 'gmvValidation', 'gmvPSUtil','gmvMoreFilter'
],

    function ($, _, Backbone, Handlebars,
        Global, Commonutil, GMVSearch, GMVValidation, GMVPSUtil,GMVMoreFilter) {

        'use strict';

        GM.View.PSSchedule = Backbone.View.extend({

            name: "CRM Preceptorship",

            el: "#div-crm-module",

            /* Load the templates */
            template_MainModule: fnGetTemplate(URL_Common_Template, "gmtCRMMainModule"),
            template_Scheduler: fnGetTemplate(URL_Preceptor_Template, "gmtPSScheduler"),
            template_SchedulerEventInfo: fnGetTemplate(URL_Preceptor_Template, "gmtPSScdlrEvntInfo"),
            template_SchedulerMoreFilter: fnGetTemplate(URL_Common_Template, "gmtMoreFilter"),


            events: {

                "click #hostCaseSchedule, #li-schedule": "hostCaseSchedule",
                "click #li-cancel,#li-close": "backToPage",
                "click #scheduler-legend-icon": "showLegend",
                "click #lookupCaseRequest": "showLkupCaseReq",
                "click #li-create": "createHostCase",
                "click .more_filter": "showMoreFiltersPopup",
                "click .filterDone": "viewFiltersResult",
                "click .deleteFilter": "deleteSelectedFilter",
                "click .clearAllFilter": "clearAllFilter",
                "click .filterpop-close button": "closeFilterPopup",
            },

            initialize: function (options) {
                $(window).on('orientationchange', this.onOrientationChange);
                this.view = options.view;
                GM.Global.SchedulerFL = "";
                
                if(GM.Global.PSScheduler == undefined){
                    GM.Global.PSScheduler={};
                    GM.Global.PSScheduler.filterCheck={};   
//                    GM.Global.PSScheduler.Mode="";
		            GM.Global.PSScheduler.productArr = [];
                    GM.Global.PSScheduler.filterCheck["status"]= [
                            {"id":105765,"text":"Open","category":"status"},
                            {"id":105766,"text":"Closed","category":"status"}
                    ];
                    GM.Global.PSScheduler.filterCheck["division"]= [
                        {"id":2000,"text":"Spine","category":"division"},
                        {"id":2004,"text":"INR","category":"division"},
                        {"id":2005,"text":"Trauma","category":"division"}
                    ];
                }
                
                this.PSScheduler = Scheduler.getSchedulerInstance();
            },

            render: function () {
                var that = this;
                this.hostCaseFl="N";
                this.$el.html(this.template_MainModule({
                    "module": "preceptorship"
                }));
                $("#preceptorship").addClass("tabpreceptorship");
                this.$(".gmPanelTitle").html("Host Case Schedule");
                $(".gmPanelContent").html("<div id='div-pshostschedule'><div class='scheduler-main-container'></div></div>");
                $(".scheduler-main-container").html(this.template_Scheduler());
            	
//                this.oldSearchTxtCnt = 0;
                
                this.hcData = "";               
                
                var psCaseReqAdmin = _.findWhere(GM.Global.UserAccess, {
                    funcnm: "PRECEPTOR-ADMIN"
                });
                
                 var psHostCreate = _.findWhere(GM.Global.UserAccess, {
                    funcnm: "PRECEPTOR-HOST"
                });
                
                if ((psHostCreate != undefined)||(psCaseReqAdmin != undefined) ) {
                  $("#li-create").removeClass("hide");
                }
                
                $("#cmnSchedulerFilterSection").append(this.template_SchedulerMoreFilter());
                this.searchProduct();
                $(".divFilterStatus").removeClass("hide");
                var filterInput = {
                    "token": localStorage.getItem("token"),
                    "region": localStorage.getItem("userDivision")
                }
                var webServicePath = "psrpt/fetchfilters";

                var customTabs = ["product"];
                filterValuesLoadInit(webServicePath,filterInput,customTabs,function(data){
                    $(".mFilterheaderLinks").append('<li class="filterLinks" data-showDiv="divFilterProduct">Product</li>');
                    $(".filterLinks[data-showdiv='divFilterStatus']").addClass("activeTab");
                    that.showCaseSchedule();
                });
            },


            deleteSelectedFilter: function (e) {
                getLoader(".show_filters");
                var that = this;
                removeFilters(e,GM.Global.PSScheduler.filterCheck,function(filterres){
                    GM.Global.PSScheduler.filterCheck = filterres;
                    that.showCaseSchedule();
                });
                var removeProductId = $(e.currentTarget).data("id");
                GM.Global.PSScheduler.productArr = _.reject(GM.Global.PSScheduler.productArr, function (dt) {
                    return dt.ID == removeProductId;
                });
                $(".filteredListProduct").empty();
                _.each(GM.Global.PSScheduler.productArr, function (dl) {
                    $(".filteredListProduct").append("<div class='liSelFilter gmAlignHorizontal' id='" + dl.ID + "'><span class='filterTxt'>" + dl.Name + "</span><span class='deleteFilter'> x</span></div>");
                });
            },

            clearAllFilter: function () {
               var that = this;
               removeAllFilters(function(){
                    GM.Global.PSScheduler.filterCheck = {};
                    that.showCaseSchedule();
               });
               GM.Global.PSScheduler.productArr = [];
               $(".filteredListProduct").empty();
            },
            
            closeFilterPopup:function(){
                GM.Global.PSScheduler.productArr = [];
                if (GM.Global.PSScheduler.filterCheck["product"] == undefined)
                    GM.Global.PSScheduler.filterCheck["product"] = [];
                if (GM.Global.PSScheduler.filterCheck["product"].length > 0) {
                    for (var i = 0; i < GM.Global.PSScheduler.filterCheck["product"].length; i++) {
                        GM.Global.PSScheduler.productArr[i] = {};
                        GM.Global.PSScheduler.productArr[i].ID = GM.Global.PSScheduler.filterCheck["product"][i].id;
                        GM.Global.PSScheduler.productArr[i].Name = GM.Global.PSScheduler.filterCheck["product"][i].text;
                        GM.Global.PSScheduler.productArr[i].category = "product";
                    }
                }
                $(".moreFilterLists ").addClass("hide");
                showSelectedFilters(GM.Global.PSScheduler.filterCheck);
            },

            //Function For use t Display the calander schedule view
            showCaseSchedule: function () {
                var that = this;
                this.eventArray = [];
                
                
                this.partyID = localStorage.getItem("partyID");
                this.createdById = localStorage.getItem("userID");
                $(".gmBtnGroup #li-close").removeClass("hide");
                //                scheduler.clearAll();
                getLoader("#scheduler_here .dhx_cal_data");
                var input = {};

                input = {
                    "acttypeid": "",
                    "token": localStorage.getItem("token"),
                    "actcategory": "105272",
                    "stropt": "visitschedule"
                };
                
                var filterres = GM.Global.PSScheduler.filterCheck;
                input.division = _.pluck(filterres.division, "id");
                input.division = input.division.toString();
                input.actstatus = _.pluck(filterres.status, "id");
                input.actstatus = input.actstatus.toString();
                input.zone = _.pluck(filterres.zone, "id");
                input.zone = input.zone.toString();
                input.procedure = _.pluck(filterres.procedure, "id");
                input.procedure = input.procedure.toString();
                input.workflow = _.pluck(filterres.workflow, "id");
                input.workflow = input.workflow.toString();
                input.setid = _.pluck(filterres.product, "id");
                input.setid = input.setid.toString();

                console.log(input);
                    
                fnGetWebServerData("crmactivity/scheduler", "gmCRMActivityVO", input, function (data) {
		            showSelectedFilters(GM.Global.PSScheduler.filterCheck);
                    that.PSScheduler.clearAll();
                    that.PSScheduler.config.hour_date = "%h:%i:%A";
                    that.PSScheduler.config.first_hour = 8;
                    that.PSScheduler.config.last_hour = 19;

                    that.PSScheduler.attachEvent("onTemplatesReady", function () {
                         that.PSScheduler.templates.event_bar_date = function (start, end, ev) {
                             return "";
                         };
                         that.PSScheduler.templates.event_header = function (start, end, event) {
                             return "";
                         };
                         that.PSScheduler.templates.event_bar_text = function (start, end, event) {
                             return "• <span class='dhx_cal_event-txtY'>" + event.text + "</span> ";
                         };
                     });
                    
                    that.PSScheduler.attachEvent("onViewChange", function (new_mode , new_date){
                      setCookie('CRMSchedulerMode', new_mode,null);
                    });

                    var schedulerMode= getCookie('CRMSchedulerMode');
                    if (parseNull(schedulerMode) != "") {
                        that.PSScheduler.init('scheduler_here', new Date(),schedulerMode);
                        
                        //Update the scheduler because of data not filtered on year mode
                        if(parseNull(schedulerMode) == "year")
                            that.PSScheduler.updateView();
                    }
                    else{
                       that.PSScheduler.init('scheduler_here', new Date(), "month");
                    }

                    that.PSScheduler.locale.labels.year_tab = "Year";

                    that.PSScheduler.config.readonly = true;
                    if (data != undefined) {
                        data = data.GmCRMListVO.gmCRMActivityVO;
                        _.each(data, function (val, index) {
                            
                            if (val.actstatusid == '105765') {
                                if (val.division == "2004") {
                                    if(val.acttypeid == "107263"){
                                        that.bgColor =  (val.region == "100824") ? 'repeating-linear-gradient(135deg, #fff 9px, #fff 11px, #FF8C00 10px, #FF8C00 15px)' :
                                        'repeating-linear-gradient(135deg, #fff 9px, #fff 11px, #f7d7a0 10px, #f7d7a0 15px)';
                                    }
                                    else if (val.acttypeid == "107264") {
                                        that.bgColor =  (val.region == "100824") ? 'repeating-linear-gradient(135deg, #fff 9px, #fff 11px, #ffbcff 10px, #ffc3ff 15px)' : 'repeating-linear-gradient(135deg, #fff 9px, #fff 11px, #fee979 10px, #ffe767 15px)';
                                    }
                                }
                                else if (val.division == "2000") {
                                    that.bgColor = (val.region == "100824") ? 'repeating-linear-gradient(135deg, #fff 9px, #fff 11px, #7f92ff 10px, #95a4ff 15px)' : 'repeating-linear-gradient(135deg, #fff 9px, #fff 11px, #c5ecfd 10px, #7cd2f8 15px)';
                                } else if (val.division == "2005") {
                                    that.bgColor = (val.region == "100824") ? 'repeating-linear-gradient(135deg, #fff 9px, #fff 11px, #70da70 10px, #a5e8a5 15px)' : 'repeating-linear-gradient(135deg, #fff 9px, #fff 11px, #a2f129 10px, #bcfe57 15px)';
                                }
                            }
                            
                            if (val.actstatusid == '105766') {
                                if (val.division == "2004") {
                                     if (val.acttypeid == "107263") {
                                        that.bgColor = (val.region == "100824") ? '#ff8c00' : '#cfb485'; 
                                     } else if (val.acttypeid == "107264") {
                                        that.bgColor = (val.region == "100824") ? '#ffafff' : '#ffd700'; 
                                     }
                                } else if (val.division == "2000") {
                                    that.bgColor = (val.region == "100824") ? '#bdbdff' : '#40C4FF';
                                } else if (val.division == "2005") {
                                    that.bgColor = (val.region == "100824") ? '#8ce88c' : '#a7f236';
                                }
                            }

                            if (val.actstatusid == "105762"){
//                                that.bgColor = (val.accessfl == "Y") ? "#ff8d83" : "#dfdce0";
                                that.bgColor = "#ff8d83";
                            }

                            that.eventArray.push({
                                "id": val.actid,
//                                "text": (val.accessfl == "Y") ? (val.division == 2004) ? "INR: " + val.partynm : val.actnm : "",
//                                "text": (val.accessfl == "Y") ? val.actnm : "",
                                "text": val.actnm,
                                "start_date": getDateByFormat(val.actstartdate) + " " + val.actstarttime,
                                "end_date": getDateByFormat(val.actenddate) + " " + val.actendtime,
//                                "status": (val.accessfl == "Y") ? "status" + val.actstatusid : "statusAll",
                                "status": val.actstatusid,
                                "color": that.bgColor,
                                "textColor": "#000",
                                "accessfl": val.accessfl,
                                "division": val.division
                            });

                        });
                        console.log(that.eventArray);

                        that.PSScheduler.parse(that.eventArray, "json");
                    }
                });

                if (GM.Global.SchdulerEvents != undefined) {
                    that.PSScheduler.detachEvent(GM.Global.SchdulerEvents);
                }
                GM.Global.SchdulerEvents = that.PSScheduler.attachEvent("onClick", function (id, e) {
                    hideMessages();
                    var input = { "token": localStorage.getItem("token"), "actid": id };
                    
                    fnGetWebServerData("crmactivity/info", "gmCRMActivityVO", input, function (data) {
                        
                        data = data.GmCRMListVO.gmCRMActivityVO; 
                        if (data != undefined && data.length > 0) {
                            var dataObj = data[0];                            
                            that.hostCaseFl = dataObj.accessfl;
                            _.each(dataObj.arrgmactivitypartyvo, function (data) {
                                //Code_ID-103112,	Code_Nm-Images	Code_GRP-FTYPE
                                data["imgurl"] = URL_WSServer + "file/src?refgroup=103112&refid=" + data.partyid + "&reftype=null&num=" + Math.random();
                            });
                            dataObj.deptid=localStorage.getItem("deptid");
                            that.hcData = dataObj;
                            that.showAttendSurgDetail(function (resSurgObj) {
                                dataObj.dataSurgObj = resSurgObj;                                                 
                                that.showCalendarInfo(dataObj);
                                $("#scheduler-info-main-header").css({background : that.getSchedulerBGColor(dataObj)});
                                $("#lookupCaseRequest").unbind('click').bind('click', function () {
                                    that.showLkupCaseReq();
                                });
                            });


                        } else {
                            showError("No access to view this Event");
                            //                            that.closeCalendarInfo();
                            return true;
                        }
                    });
                });
            },
            //   Function use to SearchProduct in to the  calander schedule view
            searchProduct: function () {
                var that = this;
                var searchProduct = "1";
                var searchOptions = {
                    'el': $("#divfilter-product"),
                    'url': URL_WSServer + 'search/system',
                    'inputParameters': {
                        'token': localStorage.getItem('token')
                    },
                    'keyParameter': 'sysnm',
                    'IDParameter': 'sysid',
                    'NameParameter': 'sysnm',
                    'resultVO': 'gmSystemBasicVO',
                    'fnName': "",
                    'minChar': 3,
                    'usage': 2,
                    'placeholder': "Product...",
                    'callback': function (data) {
                        addProducts(data);
                    }
                };
                this.searchView5 = new GMVSearch(searchOptions);
            },

            // show calendar popinfo
            showCalendarInfo: function (data) {
                var that = this;
                var userID = localStorage.getItem("userID");
                var partyID = localStorage.getItem("partyID");
                var userDivision = localStorage.getItem("repDivision");
                that.closeCalendarInfo();
//                var FSAttendee = _.findWhere(data.arrgmactivitypartyvo, {
//                    "roleid": "105561", "voidfl": "", "partyid": partyID
//                });
                var currDate = new Date();
                var actDate = data.actstartdate + " " + data.actstarttime;
                actDate = new Date(actDate);
                var diffDays = Math.ceil((actDate - currDate) / (1000 * 3600 * 24));
                if ((currDate < actDate) && (diffDays > 7 ))
                    data.isfuture = true;        
                $("#scheduler-info-container").html(that.template_SchedulerEventInfo(data)); 
                var psCaseReqAdmin1 = _.findWhere(GM.Global.UserAccess, {
                    funcnm: "PRECEPTOR-ADMIN"
                });
//                if(that.psCaseReqAdmin == undefined){
//                if(data.accessfl=='N')
//                   $("#attndHostSurgDetails").html('<div class="gmMarginTop10p gmTextCenter">No Data Found</div>');  
//                
//                if(data.dataSurgObj.length>0 ){
//                var attndSurgarr=data.dataSurgObj;
//                    
//                var origData = _.where(attndSurgarr, {
//                        accessfl: 'Y'
//                    });
//                    if(origData.length==0)
//                        $("#attndSurgDetails").html('<div class="gmMarginTop10p gmTextCenter">No Data Found</div>');
//                }
//                }
                
                $("#scheduler-button-view").removeClass("gmVisibilityHidden");
                if (psCaseReqAdmin1 != undefined) {
                    $("#scheduler-button-edit").removeClass("gmVisibilityHidden");
                    $("#lookupCaseRequest").removeClass("gmVisibilityHidden");
                } else if ((data.createdbyid == userID) || (that.hostCaseFl == 'Y')) {
                    $("#scheduler-button-edit").removeClass("gmVisibilityHidden");
                    $("#lookupCaseRequest").addClass("gmVisibilityHidden");     
                } else {
                    $("#scheduler-button-edit").addClass("gmVisibilityHidden");
                    $("#scheduler-button-view").addClass("gmVisibilityHidden");
                    $("#lookupCaseRequest").addClass("gmVisibilityHidden");
                }
                if (data.actstatus == "105762") { // cancelled host 
                    if (psCaseReqAdmin1 != undefined)
                    $("#scheduler-button-edit").removeClass("gmVisibilityHidden");
                    else
                    $("#scheduler-button-edit").addClass("gmVisibilityHidden");
                    $("#lookupCaseRequest").addClass("gmVisibilityHidden");
                }
                $("#scheduler-info-container").removeClass("hide");
                
                 $("#btn-add-register").unbind("click").bind("click", function () {
                    GM.Global.SchedulerFL = "Y";
                    GM.Global.PSHostActivity=data;
                    window.location.href = "#crm/preceptorship/case/id/0";
                });
                
                $("#scheduler-button-edit").unbind("click").bind("click", function () {
                    GM.Global.SchedulerFL = "Y";
                    window.location.href = "#crm/preceptorship/host/id/" + data.actid;
                });

                $("#scheduler-button-close").unbind("click").bind("click", function (e) {
                    that.closeCalendarInfo();
                });
            },

            closeEventInfo: function () {
                var that = this;
                $("#scheduler-info-view").addClass("hide");
            },

            closeCalendarInfo: function () {
                var that = this;
                $("#scheduler-info-container").addClass("hide");
                this.closeEventInfo();
            },
            backToPage: function () {
                window.history.back();
            },
            showLegend: function () {
                 $("#scheduler-legend-info").toggle();
            },

            //Show Attend Surgeon Details Grid
            showAttendSurgDetail: function (cb) {
                var that = this;
                var lkinput = {
                    'token': localStorage.getItem('token')
                };
                lkinput.segid = that.hcData.segid;
                lkinput.actid = that.hcData.actid;
                fnGetWebServerData("psrpt/fetchlkupcreq", undefined, lkinput, function (data) {
                    var schdldData = _.filter(data, function (dt) {
                        return (dt.status == 105761 || dt.status == 105764);
                     });
                    if (schdldData.length > 0) {
                        _.each(schdldData, function (data) {
                            //Code_ID-103112,	Code_Nm-Images	Code_GRP-FTYPE
                            var psCaseReqAdmin2 = _.findWhere(GM.Global.UserAccess, {
                            funcnm: "PRECEPTOR-ADMIN"
                            });
                            if (psCaseReqAdmin2 != undefined || that.hostCaseFl == 'Y') {
                                data.accessfl = "Y";
                             }
                            data["imgurl"] = URL_WSServer + "file/src?refgroup=103112&refid=" + data.surgid + "&reftype=null&num=" + Math.random();
                        });
                        cb(schdldData);
                    } else
                        cb([]);
                });

            },

            selectedDone: function () {
                var that = this;
                var input = {
                    'token': localStorage.getItem('token')
                };

                var linkreqs = [],
                    unlinkreqs = [], existFl = 0;
                for (var i = 0; i < that.gridObj.getRowsNum(); i++) {
                    var isCheck = that.gridObj.cells(i, 0).getValue();
                    var element = that.gridObj.cells(i, 4).getValue();
                    var hcid = $(element).data('hcid');
                    var casereq = $(element).text();

                    //how many checked
                    existFl +=isCheck;

                    if ((isCheck == 1) && (parseNull(hcid) == ""))
                        linkreqs.push(casereq);
                    else if ((isCheck == 0) && (parseNull(hcid) != ""))
                        unlinkreqs.push(casereq);
                };
                console.log("linkreqs>>>>>>>" + linkreqs);
                console.log("unlinkreqs>>>>>>>>" + unlinkreqs);
                input.linkreqs = linkreqs;
                input.unlinkreqs = unlinkreqs;
                input.actid = that.hcData.actid;
                if (linkreqs.length > 0 || unlinkreqs.length > 0) {
                    console.log("Case Request Mapping Input >>>" + JSON.stringify(input));
                    fnGetWebServerData("pstxn/casereqmap", undefined, input, function (data) {
                        console.log("Case Request Mapping Output >>>" + JSON.stringify(data));
                        if (data != null) {
                            hidePopupPSReport();
                            var message = ""
                            if (linkreqs.length > 0)
                                message += linkreqs.toString() + " Successfully Assigned to " + input.actid;
                            if (linkreqs.length > 0 && unlinkreqs.length > 0)
                                message += " , ";
                            if (unlinkreqs.length > 0)
                                message += unlinkreqs.toString() + " Successfully Unassigned to " + input.actid;
                            showSuccess(message);
                            that.showAttendSurgDetail(function (resSurgObj) {
                                var headInfobgcolor = "";
                                headInfobgcolor = (that.hcData.division == "2000") ? "gmBGBlue" : (that.hcData.division == "2004") ? "bgGray" : (that.hcData.division == "2005") ? "gmBGGreen" : "bgOrange";
                                that.hcData.dataSurgObj = resSurgObj;
                                that.showCalendarInfo(that.hcData);
                                $("#scheduler-info-main-header").addClass(headInfobgcolor);

                            });
                            //                        Backbone.history.loadUrl();
                        } else {
                            hidePopupPSReport();
                            showError("Save Failed");
                        }
                    });
                }
                else{
                	if(existFl>0){
                      displayPopupMsg("Nothing changed to Save");
                	}else{
                	 displayPopupMsg("Select Atleast One Case Request Before Clicking Done Button");
                	}
                }

            },

            // Fetch Case Request to display in lookup case request popup
            showLkupCaseReq: function () {

                var that = this;

                console.log(that.hcData);

                var lkinput = {
                    'token': localStorage.getItem('token')
                };
                lkinput.segid = that.hcData.segid;
                lkinput.actid = that.hcData.actid;

                var title = that.hcData.actnm;

                showPopupPSReport();
                $("#div-crm-overlay-content-container .modal-title").text(title);
                $("#div-crm-overlay-content-container .modal-body").html("<div id='gridPreceptorRpt'></div>");
                getLoader("#gridPreceptorRpt");
                $("#div-crm-overlay-content-container").find('.done-model').unbind('click').bind('click', function () {
                    that.selectedDone();
                });

                this.gridObj = "";
                console.log("Fetch Lookup Case Request List INPUT>>>>>>>>>>" + JSON.stringify(lkinput));
                fnGetWebServerData("psrpt/fetchlkupcreq", undefined, lkinput, function (data) {
                    console.log("Fetch Lookup Case Request List OUTPUT>>>>>>>>>>" + JSON.stringify(data));
                    if (data != null) {
                        var rows = [],
                            i = 0;
                        _.each(data, function (griddt) {
                            var rowid = "creqid" + i;
                            rows[i] = {};
                            rows[i].id = i;
                            rows[i].data = [];
                            if (that.hcData.actid == griddt.actid)
                                rows[i].data[0] = true;
                            else
                                rows[i].data[0] = false;
                            rows[i].data[1] = '<span data-surgid="' + griddt.surgid + '">' + griddt.surgnm + '</span>';
                            rows[i].data[2] = griddt.segnm;
                            rows[i].data[3] = griddt.products;
                            rows[i].data[4] = "<span id='" + rowid + "' data-hcid='" + griddt.actid + "' data-crid='" + griddt.casereqid + "'>" + griddt.casereqid + "</span>";
                            rows[i].data[5] = griddt.frmdt;
                            rows[i].data[6] = griddt.todt;
                            rows[i].data[7] = griddt.note;
                            i++;
                        });
                        var dataHost = {};
                        dataHost.rows = rows;
                        $(".done-model").removeClass("hide");
                        $(".cancel-model").removeClass("hide");
                        var setInitWidths = "30,150,100,150,100,180,*";
                        var setColAlign = "center,left,left,left,left,center,center,left";
                        var setColTypes = "ch,ro,ro,ro,ro,ro,ro,ro";
                        var setColSorting = "str,sortLink,str,str,sortLinkID,date,date,str";
                        var setHeader = ["", "Surgeon Name", "Procedure", "Product(s)", "Request-ID", "From Date","To Date", "Notes"];
                        var setFilter = ["", "#text_filter", "#text_filter", "#text_filter", "#text_filter", "#text_filter","#text_filter", "#text_filter"];
                        var enableTooltips = "true,true,true,true,true,true,true,true";
                        var footerArry = [];
                        var gridHeight = "";
                        var setFilter = "";
                        var footerStyles = [];
                        var footerExportFL = true;
                        that.gridObj = loadDHTMLXGrid('gridPreceptorRpt', gridHeight, dataHost, setHeader, setInitWidths, setColAlign, setColTypes, setColSorting, enableTooltips, setFilter, footerArry, footerStyles);
                        for (var i = 0; i < that.gridObj.getRowsNum(); i++) {
                            that.gridObj.cells(i, 1).cell.className = 'gmReportLnk gmEllipses';
                            that.gridObj.cells(i, 2).cell.className = 'gmEllipses';
                            that.gridObj.cells(i, 3).cell.className = 'gmEllipses';
                            that.gridObj.cells(i, 4).cell.className = 'gmReportLnk gmEllipses';
                            that.gridObj.cells(i, 6).cell.className = 'gmEllipses';
                        }
                        that.gridObj.attachEvent("onRowSelect", function (id, index) {
                            if (index == 0) {
                                var chkVal = that.gridObj.cells(id, 0).getValue();
                                if (chkVal == 1)
                                    that.gridObj.cells(id, 0).setValue(false);
                                else
                                    that.gridObj.cells(id, 0).setValue(true);
                            }
                            if (index == 1) {
                                var element = that.gridObj.cells(id, 1).getValue();
                                hidePopupPSReport();
                                window.location.href = '#crm/surgeon/id/' + $(element).data('surgid');
                            } else if (index == 4) {
                                var element = that.gridObj.cells(id, 4).getValue();
                                hidePopupPSReport();
                                window.location.href = '#crm/preceptorship/case/id/' + $(element).data('crid');
                            }
                        });
                    } else
                        $("#gridPreceptorRpt").html("No Data Found");
                });

            },

            createHostCase: function () {
                window.location.href = "#crm/preceptorship/host/id/0";
            },
            
            getSchedulerBGColor: function (data) {
                if (data.actstatus == "105762") {
                    return '#ff8d83';
                } else if (data.division == "2004") {
                    if (data.acttype == "107263") {
                        return (data.region == "100824") ? '#ff8c00' : '#cfb485';
                    } else if (data.acttype == "107264") {
                        return (data.region == "100824") ? '#ffafff' : '#ffd700';
                    }
                } else if (data.division == "2000") {
                    return (data.region == "100824") ? '#bdbdff' : '#40C4FF';
                } else if (data.division == "2005") {
                    return (data.region == "100824") ? '#8ce88c' : '#a7f236';
                } else {
                    return '#9E9E9E';
                }
            },
            showMoreFiltersPopup: function () {
                var that = this;
                var filterData = GM.Global.PSScheduler.filterCheck;
                showMoreFilters(filterData,function(chkFilters){
                    that.chkFilters = chkFilters;
                    console.log("GM.Global.PSScheduler.filterCheck" + JSON.stringify(GM.Global.PSScheduler.filterCheck));
                });
                
                
                $(".divFilter").each(function () {
                    $(this).find("li").each(function () {
                        var checkElem = $(this).children("input");
                        _.each(GM.Global.filterTabs, function (dt) {
                            _.each(filterData[dt], function (item) {
                                if (item.id == checkElem.attr("name")) {
                                    checkElem.prop("checked", "true");
                                    checkElem.trigger("change");
                                }
                            });
                        });
                    });
                });
                
                $(".filteredListProduct").empty();
                _.each(GM.Global.PSScheduler.filterCheck["product"], function (dl) {
                    $(".filteredListProduct").append("<div class='liSelFilter gmAlignHorizontal' id='" + dl.id + "'><span class='filterTxt'>" + dl.text + "</span><span class='deleteFilter'> x</span></div>");
                });
                
                $(".deleteFilter").unbind('click').bind('click', function (e) {
                    var removeProductId = $(e.currentTarget).parent(".liSelFilter").attr("id");
                    GM.Global.PSScheduler.productArr = _.reject(GM.Global.PSScheduler.productArr, function (dt) {
                        return dt.ID == removeProductId;
                    });
                    $(e.currentTarget).parent(".liSelFilter").remove();

                });
            },

            viewFiltersResult: function (e) {
                $(".filterpdctInput").prop('checked', 'true');
                $(".filterpdctInput").trigger("change");
                $('.moreFilterLists').addClass('hide');
                GM.Global.PSScheduler.filterCheck = this.chkFilters;
                this.showCaseSchedule();
            }

        });
        return GM.View.PSSchedule;
    });
