/**********************************************************************************
 * File             :        gmvPSList.js
 * Description      :        View to represent Preceptor Host Case
 * Path             :        /web/GlobusCRM/crm/preceptorship/view/gmvPSHost.js
 * Author           :        RANJITH
 **********************************************************************************/
define([

    // External Libraries
    'jquery', 'underscore', 'backbone', 'handlebars',

    // Common
    'global', 'commonutil'
],

    function ($, _, Backbone, Handlebars,
        Global, Commonutil) {


        GM.View.PSList = Backbone.View.extend({
            name: "CRM Preceptorship",

            el: "#div-crm-module",
            template_MainModule: fnGetTemplate(URL_Common_Template, "gmtCRMMainModule"),
            template_preceptorReport: fnGetTemplate(URL_Preceptor_Template, "gmtPSRqstList"),
            events: {
                
                "click .surgin-dr-tab": "clickDateTabs",
                "click #btn-load-chart": "loadBtnClicked",
            },

            initialize: function (options) {
                $(window).on('orientationchange', this.onOrientationChange);
                this.view = options.view;
                this.firstnm = options.firstnm;
                this.sdate = options.sdate;
                this.edate = options.edate;
                this.reqsdate = options.reqsdate;
                this.reqedate = options.reqedate;
                this.name = options.name;
                this.divisionid = options.divisionid;
                this.statusid = options.statusid;

                GM.Global.SchedulerFL = "";
                
                var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth() + 1; //January is 0!
                var yyyy = today.getFullYear();
                if (dd < 10) {
                    dd = '0' + dd
                };
                if (mm < 10) {
                    mm = '0' + mm
                };
                this.today = mm + '/' + dd + '/' + yyyy;
            },

            render: function () {
                // Status Open-105765,Cancel-105762,Closed-105766,Completed-105764,Scheduled-105761
                var that = this;
                this.$el.html(this.template_MainModule({
                    "module": "preceptorship"
                }));
                $("#preceptorship").addClass("tabpreceptorship");
                var input = {};
                var firstnm = that.firstnm;
                var fromdate = that.sdate;
                var todate = that.edate;
                var name = that.name;
                var reqfromdate = that.reqsdate;
                var reqtodate = that.reqedate;

                //Fetch the value  given  by Search Name Host Case
                if (!firstnm == "") {
                    input = {
                        "firstname": firstnm,
                        // "actstartdate": this.today,
                        "actcategory": "105272",
                    };
                    var title = "Open Host Cases Selected By '" + firstnm + "'";
                    that.showHostCaseReport(input, title);
                }
                //Fetch the value  given Search Name for Case request
                if (!name == "") {
                    input = {
                        "name": name,
                        // "actstartdate": this.today,
                        "actcategory": "105273",
                    };
                    var title = "Open Cases Request Selected By '" + name + "'";
                    that.openCaseRequests(input, title);
                }
                //Fetch the value given by fromdate and todate
                if (!fromdate == "" && !todate == "") {
                    input = {
                        "actstartdate": getDateByFormat(fromdate),
                        "actenddate": getDateByFormat(todate),
                        "actcategory": "105272",
                    };
                    var title = "Selected By Date From '" + fromdate + "'" + " To '" + todate + "'";
                    that.showHostCaseReport(input, title);
                }
                if (!reqfromdate == "" && !reqtodate == "") {
                    input = {
                        "actstartdate": getDateByFormat(reqfromdate),
                        "actenddate": getDateByFormat(reqtodate),
                        "actcategory": "105273",
                    };
                    var title = "Selected By Date From '" + reqfromdate + "'" + " To '" + reqtodate + "'";
                    that.openCaseRequests(input, title);
                }

                switch (this.view) {
                    case "upcoming":
                        input = {
                            //"actstatusid": "105765,105766,105762",
                            "actstatusid": "105765,105766",
                            "actcategory": "105272",
                            "actstartdate": this.today
                        };
                        var title = "Upcoming Host Case";
                        that.showHostCaseReport(input, title);

                        break;
                    case "open":
                        input = {
                            "actstatusid": "105765",
                            "actcategory": "105272",
                            "actstartdate": this.today
                        };
                        var title = "Open Host Case";
                        that.showHostCaseReport(input, title);
                        break;
                    case "opencase":
                        input = {
                            "actstatusid": "105765",
                            "actcategory": "105273",
                             "actstartdate": ''
                        };
                        var title = "Open Case Request";
                        that.openCaseRequests(input, title);
                        break;
                    case "upcmghcdetails":
                        input = {
                            "actstatusid": "105765,105766",
                            "actcategory": "105272",
                            "stropt": "ADMIN_UPCOME",
                            "actstartdate": this.today
                        };
                        var title = "Upcoming Host Cases Details";
                        that.showHostReport(input, title);
                        break;
                    case "allhost":
                        input = {
                            "actstatusid": "105765,105766,105762,105764",
                            "stropt": "ADMIN_All",
                            "actcategory": "105272",
                        };
                        var title = "All Host Cases Details";
                        that.showHostReport(input, title);
                        break;
                    case "allcase":
                        input = {
                            "actstatusid": "105765,105766,105762,105764",
                            "stropt": "ADMIN_All",
                            "actcategory": "105273",
                        };
                        var title = "All Case Request Details";
                        that.showCaseReport(input, title);
                        break;
                    case "HC":
                        input = {
                            "adminsts": that.statusid,
                            "stropt": "pschart",
                            "division": that.divisionid,
                            "actcategory": "105272"
                        };
                        var title = "Host Cases Details By Admin Status";
                        that.showHostReport(input, title);
                        break;
                    case "CR":
                        input = {
                            "adminsts": that.statusid,
                            "stropt": "pschart",
                            "division": that.divisionid,
                            "actcategory": "105273"
                        };
                        var title = "Case Request Details By Admin Status";
                        that.showCaseReport(input, title);
                        break;
                }


            },
            
             //Get Date From Which date Range is Selected in the Filter callback to the called function    
            getDateInputs: function(callback){               
                var dateRange, dr, fromDt, toDt;
                this.$('.surg-datadr .surgin-dr-tab').each(function (i) {
                    if ($(this).hasClass("selected")) {
                        dateRange = $(this).attr('id');
                    }                 
                });
                    switch (dateRange) {
                        case '3M':
                            dr = 2;
                            break;
                        case '6M':
                            dr = 5;
                            break;
                        case '1Y':
                            dr = 11;
                            break;
                        case 'custom':
                            break;
                    }                    
                if (dateRange == 'custom') {
                    var fromDtmm = $("#select-surg-surgstartmonth option:selected").text();
                    fromDtyyyy = $("#select-surg-surgstartyear option:selected").val();
                    var toDtmm = $("#select-surg-surgendmonth option:selected").text();
                    toDtyyyy = $("#select-surg-surgendyear option:selected").val();
                    if(fromDtmm=="MM")
                        fromDtmm="";
                    if(toDtmm=="MM")
                        toDtmm="";
                    fromDt = fromDtyyyy + "" + fromDtmm;
                    toDt = toDtyyyy + "" + toDtmm;
                } else {
                    fromDt = new Date();
                    toDt = new Date();
                    fromDt.setMonth(fromDt.getMonth() - dr);
                    var fromDtdd = ("0" + fromDt.getDate()).slice(-2),
                        fromDtmm = ("0" + (fromDt.getMonth() + 1)).slice(-2),
                        fromDtyyyy = fromDt.getFullYear();
                    var toDtdd = ("0" + toDt.getDate()).slice(-2),
                        toDtmm = ("0" + (toDt.getMonth() + 1)).slice(-2),
                        toDtyyyy = toDt.getFullYear();
                    fromDt = fromDtyyyy + "" + fromDtmm;
                    toDt = toDtyyyy + "" + toDtmm;
                }
                callback(fromDt, toDt);
            },
            
            //  On clicking Date tabs to highlight the tab
            clickDateTabs: function (e) {
                var dateRange = $(e.currentTarget).attr('id');
                $('.surgin-dr-tab').removeClass("gmBGDarkerGrey gmFontWhite selected");
                $('.surgin-dr-tab').removeClass("gmBGGrey");
                $('.surgin-dr-tab').addClass("gmBGGrey");
                $('#' + dateRange).removeClass("gmBGGrey");
                $('#' + dateRange).addClass("gmBGDarkerGrey gmFontWhite selected");
                if (dateRange == 'custom') {
                    $('.SHB-right').removeClass('hide');
                } else {
                    if (!($('.SHB-right').hasClass('hide')))
                        $('.SHB-right').addClass('hide');
                }
            },
            
            formatDt: function(dt, dtabbr){
                var yr=dt.substring(0, 4);
                var mth=dt.substring(4, 6);
                if(dtabbr=='from')
                this.actstartdt = mth + '/01/' + yr;
                else if(dtabbr=='to')
                this.actenddt = mth + '/' + lastday(yr,mth) + '/' + yr;                   
            },
            
            
            
            //on Load button clicked the function called and it loads the data as given filter date range
             loadBtnClicked: function(){
                    var that=this;
                 if(that.varAllRpt == 'host'){
                     var catId = '105272';
                 }
                 else if(that.varAllRpt == 'case'){
                     var catId = '105273';
                 }
                    
                    this.getDateInputs(function(fromDt, toDt){
                    if (fromDt.length == 6 && toDt.length == 6) {
                        if (fromDt <= toDt) {
                            that.formatDt(fromDt, "from");
                            that.formatDt(toDt, "to");
                            var date1 = new Date(that.actstartdt);
                            var date2 = new Date(that.actenddt);
                            var timeDiff = Math.abs(date2.getTime() - date1.getTime());
                            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
                            if (diffDays <= 365) {
                                input = {
                                    "actstatusid": "105765,105766,105762,105764",
                                    "stropt": "ADMIN_All",
                                    "actcategory": catId,
                                    "actstartdate": that.actstartdt,
                                    "actenddate": that.actenddt,
                                };
                                if (that.varAllRpt == 'host') {
                                    that.loadHostReport(input);
                                } else if (that.varAllRpt == 'case') {
                                    that.loadCaseRequests(input);
                                }
                            } else
                                showError("Please Select the Date Range Less than a Year");
                        } else
                            showError("Please Select From Date is Less than To Date");                                
                }
                 else
                    showError("From Date or To Date may be Empty, Please Select the Date Fields");
                    
                });      
             },

//            listSurgeons: function (input) {
//                var that = this;
//                this.$("#div-main-content").html(that.template_preceptorReport());
//                console.log(input);
//                getLoader("#ps-report-list");
//
//                fnGetWebServerData("psrpt/listhost", undefined, input, function (data) {
//                    if (!data == "" || !data == undefined || !data == null) {
//                        console.log(data);
//                        var rows = [],
//                            i = 0;
//                        _.each(data, function (griddt) {
//                            rows[i] = {};
//                            rows[i].id = i;
//                            rows[i].data = [];
//                            rows[i].data[0] = "<a class='gmReportLnk' href='#crm/preceptorship/host/id/" + griddt.actid + "'>" + griddt.actid + "</a>";
//                            rows[i].data[1] = "<a class='gmReportLnk' href='#crm/surgeon/id/" + griddt.partyid + "'>" + griddt.partynm + "</a>";;
//                            rows[i].data[2] = griddt.actstartdate;
//                            rows[i].data[3] = griddt.segnm;
//                            rows[i].data[4] = griddt.pathology;
//                            rows[i].data[5] = griddt.prodnm;
//                            rows[i].data[6] = griddt.region;
//                            rows[i].data[7] = griddt.division;
//                            rows[i].data[8] = griddt.acttypeid;
//                            rows[i].data[9] = griddt.fieldsales;
//                            rows[i].data[10] = griddt.hospital;
//                            rows[i].data[12] = griddt.actstatus;
//                            rows[i].data[13] = griddt.actnotes;
//                            i++;
//                        });
//                        var dataHost = {};
//                        dataHost.rows = rows;
//                        $(".yes-model").addClass("hide");
//                        $(".no-model").addClass("hide");
//                        $(".done-model").removeClass("hide");
//                        $(".cancel-model").removeClass("hide");
//                        var setInitWidths = "150,150,100,200,100,150,100,100,100,100,100,50,*";
//                        var setColAlign = "left,left,center,left,left,left,left,left,left,left,left,left,left";
//                        var setColTypes = "ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro";
//                        var setColSorting = "sortLinkID,sortLink,str,str,str,str,str,str";
//                        var setHeader = ["Host Case ID", "Surgeon Name", "Case Date", "Procedure", "Pathology", "Product(s)",
//                                         "Region", "Division", "Preceptorship type", "Fieldsales", "Hospital", "Status", "Notes"];
//                        var setFilter = ["#text_filter", "#text_filter", "#text_filter", "#text_filter", "#text_filter", "#text_filter", "#text_filter", "#text_filter", "#text_filter", "#text_filter", "#text_filter", "#text_filter", "#text_filter"];
//                        var enableTooltips = "true,true,true,true,true,true,true,true,true,true,true,true";
//                        var footerArry = [];
//                        var gridHeight = "";
//                        var footerStyles = [];
//                        var footerExportFL = true;
//                        that.gridObj = loadDHTMLXGrid('ps-report-list', gridHeight, dataHost, setHeader, setInitWidths, setColAlign, setColTypes, setColSorting, enableTooltips, setFilter, footerArry, footerStyles, footerExportFL);
//                        for (var i = 0; i < that.gridObj.getRowsNum(); i++) {
//                            that.gridObj.cells(i, 0).cell.className = 'gmEllipses';
//                            that.gridObj.cells(i, 1).cell.className = 'gmEllipses';
//                            that.gridObj.cells(i, 2).cell.className = 'gmEllipses';
//                            that.gridObj.cells(i, 3).cell.className = 'gmEllipses';
//                            that.gridObj.cells(i, 4).cell.className = 'gmEllipses';
//                            that.gridObj.cells(i, 6).cell.className = 'gmEllipses';
//                            that.gridObj.cells(i, 7).cell.className = 'gmEllipses';
//                        }
//                        if (footerExportFL) {
//                            var deleteIndexArr = []; //include the index of the column in the array if u want to export                 
//                            $("#ps-report-list-export").unbind("click").bind("click", function (event) {
//                                exportExcel(event, that.gridObj, deleteIndexArr);
//                            });
//                        }
//                    } else {
//                        $("#ps-report-list").html("No data found");
//                    }
//                });
//            },

            showHostReport: function (input, title) {
                var that = this;                
                this.$("#div-main-content").html(that.template_preceptorReport());
                
                console.log(input);
                
                $(".gmPanelTitle").text(title);
                console.log(input.stropt);
                if(input.stropt=="ADMIN_All"){     
                   $("#psreports").removeClass("hide");
                    monthData();
                    yearData();
//                    that.loadBtnClicked()
                    that.varAllRpt = 'host';
                }
                else if(input.stropt == 'pschart'){
                   $("#psreports").addClass("hide");
                    that.loadHostReport(input);
                }
                else{
                   $("#psreports").addClass("hide");
                    that.loadHostReport(input);
                }
               
            },
            
            showCaseReport: function (input, title) {
                var that = this;                
                this.$("#div-main-content").html(that.template_preceptorReport());
                
                console.log(input);
                
                $(".gmPanelTitle").text(title);
                console.log(input.stropt);
                if(input.stropt=="ADMIN_All"){     
                   $("#psreports").removeClass("hide");
                    monthData();
                    yearData();
                    that.varAllRpt = 'case';
                }
//                else{
//                   $("#psreports").addClass("hide");
//                    that.showCaseRequests(input);
//                }
                else if(input.stropt == 'pschart'){
                    $("#psreports").addClass("hide");
                    that.loadCaseRequests(input);
                }
               
            },
            
            loadHostReport: function(input){
                var that=this;
                getLoader("#ps-report-list");
               fnGetWebServerData("psrpt/listhost", "gmCRMActivityVO", input, function (data) {
                    console.log(data);
                    if (!data == "" || !data == undefined || !data == null) {
                        var rows = [],
                            i = 0;
                        _.each(data, function (griddt) {
                            var partyidarr = parseNull(griddt.partyid).split(" / ");
                            var partynmarr = parseNull(griddt.partynm).split(" / ");
                            var partyData="";
                            for(j=0; j<partyidarr.length; j++){
                                if(partyData !="")
                                    partyData += " / ";
                                partyData += "<a class='gmReportLnk' href='#crm/surgeon/id/" + partyidarr[j] + "'>" + partynmarr[j] + "</a>";
                            }
                            rows[i] = {};
                            rows[i].id = i;
                            rows[i].data = [];
                            rows[i].data[0] = "<a class='gmReportLnk' data-adminsts='" + griddt.adminsts + "' href='#crm/preceptorship/host/id/" + griddt.actid + "'>" + griddt.actid + "</a>";
//                            rows[i].data[1] = "<a class='gmReportLnk' href='#crm/surgeon/id/" + griddt.partyid + "'>" + griddt.partynm + "</a>";
                            rows[i].data[1] = partyData;
                            rows[i].data[2] = griddt.actstartdate;
                            rows[i].data[3] = griddt.createby;
                            rows[i].data[4] = griddt.createdate;
                            rows[i].data[5] = griddt.segnm;
                            rows[i].data[6] = griddt.pathology;
                            rows[i].data[7] = griddt.prodnm;
                            rows[i].data[8] = griddt.actstatus;
                            rows[i].data[9] = griddt.adminstsnm;
                            rows[i].data[10] = griddt.region;
                            rows[i].data[11] = griddt.zone;
                            rows[i].data[12] = griddt.division;
                            rows[i].data[13] = griddt.acttypeid;
                            rows[i].data[14] = griddt.hospital;
                            rows[i].data[15] = griddt.fieldsales;
                            rows[i].data[16] = griddt.actnotes;
                            rows[i].data[17] = griddt.surgnm;
                            if (griddt.casereqid) {
                                rows[i].data[18] = "<a class='gmReportLnk' href='#crm/preceptorship/case/id/" + griddt.casereqid + "'>" + griddt.casereqid + "</a>";
                            } else {
                                rows[i].data[18] = "";
                            }

                            rows[i].data[19] = griddt.hotlnm;

                            if (griddt.hotlprice) {
                                rows[i].data[20] = fmtPrice(griddt.hotlprice);
                            } else {
                                rows[i].data[20] = "";
                            }

                            rows[i].data[21] = griddt.trvlnm;

                            if (griddt.trvlprice) {
                                rows[i].data[22] = fmtPrice(griddt.trvlprice);
                            } else {
                                rows[i].data[22] = "";
                            }

                            rows[i].data[23] = griddt.crfnm;
                            rows[i].data[24] = griddt.trfnm;
                            rows[i].data[25] = griddt.labinv;
                            rows[i].data[26] = griddt.pcmninv;
                            rows[i].data[27] = griddt.crdfl;
                            rows[i].data[28] = griddt.survfl;
                            
                            i++;
                        });
                        var dataHost = {};
                        dataHost.rows = rows;
                        $(".yes-model").addClass("hide");
                        $(".no-model").addClass("hide");
                        $(".done-model").removeClass("hide");
                        $(".cancel-model").removeClass("hide");
                        var setInitWidths = "150,150,100,150,120,250,150,150,120,120,120,150,150,150,150,150,100,150,100,150,150,150,150,150,150,150,150,150,150";
                        var setColAlign = "left,left,center,left,center,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,right,left,right,left,left,left,left,center,center";
                        var setColTypes = "ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro";
                        var setColSorting = "sortLinkID,sortLink,date,str,date,str,str,str,str,str,str,str,str,str,str,str,str,str,sortLinkID,str,str,str,str,str,str,str,str,str,str";
                        var setHeader = ["Host Case ID", "Host Surgeon Name", "Case Date","Created By", "Created Date", "Procedure", "Pathology", "Product(s)", "Status", "Admin Status", "Region", "Zone", "Division", "Preceptorship Type", "Hospital", "Fieldsales",
                            "Notes", "Attending Surgeon Name", "Case Request ID", "Hotel Details", "Hotel Price", "Flight/Transportation Details", "Flight/Transportation Price", "PCRF/CRF Status", "TRF Status", "Lab Invoice", "Specimen Invoice", "Credentials Required?", "Survey Sent?"];
                        var setFilter = ["#text_filter", "#text_filter", "#text_filter","#text_filter","#text_filter", "#text_filter", "#text_filter", "#text_filter", "#select_filter","#select_filter", "#select_filter", "#select_filter", "#select_filter", "#select_filter", "#text_filter", "#text_filter", "#text_filter", "#text_filter", "#text_filter", "#text_filter", "#text_filter", "#text_filter", "#text_filter", "#select_filter", "#select_filter", "#text_filter", "#text_filter", "#text_filter", "#text_filter"];
                        var enableTooltips = "true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true";
                        var footerArry = [];
                        var gridHeight = 750;
                        var footerStyles = [];
                        var footerExportFL = true;
                        that.gridObj = loadDHTMLXGrid('ps-report-list', gridHeight, dataHost, setHeader, setInitWidths, setColAlign, setColTypes, setColSorting, enableTooltips, setFilter, footerArry, footerStyles, footerExportFL);
                        for (var i = 0; i < that.gridObj.getRowsNum(); i++) {
                            that.gridObj.cells(i, 0).cell.className = 'gmEllipses';
                            that.gridObj.cells(i, 1).cell.className = 'not_m_line';
                            that.gridObj.cells(i, 2).cell.className = 'gmEllipses';
                            that.gridObj.cells(i, 3).cell.className = 'gmEllipses';
                            that.gridObj.cells(i, 4).cell.className = 'gmEllipses';
                            that.gridObj.cells(i, 5).cell.className = 'gmEllipses';
                            that.gridObj.cells(i, 6).cell.className = 'gmEllipses';
                            that.gridObj.cells(i, 7).cell.className = 'gmEllipses';
                            that.gridObj.cells(i, 8).cell.className = 'gmEllipses';
                            that.gridObj.cells(i, 10).cell.className = 'gmEllipses';
                            that.gridObj.cells(i, 11).cell.className = 'gmEllipses';
                            that.gridObj.cells(i, 12).cell.className = 'gmEllipses';
                            that.gridObj.cells(i, 13).cell.className = 'gmEllipses';
                            that.gridObj.cells(i, 14).cell.className = 'gmEllipses';
                            that.gridObj.cells(i, 15).cell.className = 'gmEllipses';
                            that.gridObj.cells(i, 16).cell.className = 'gmEllipses';
                            that.gridObj.cells(i, 17).cell.className = 'gmEllipses';
                            that.gridObj.cells(i, 18).cell.className = 'gmEllipses';
                            that.gridObj.cells(i, 19).cell.className = 'gmEllipses';
                            that.gridObj.cells(i, 20).cell.className = 'gmEllipses';
                            that.gridObj.cells(i, 21).cell.className = 'gmEllipses';
                            that.gridObj.cells(i, 22).cell.className = 'gmEllipses';
                            that.gridObj.cells(i, 23).cell.className = 'gmEllipses';
                            that.gridObj.cells(i, 24).cell.className = 'gmEllipses';
                            that.gridObj.cells(i, 25).cell.className = 'gmEllipses';
                            that.gridObj.cells(i, 26).cell.className = 'gmEllipses';
                            that.gridObj.cells(i, 27).cell.className = 'gmEllipses';
                            that.gridObj.cells(i, 28).cell.className = 'gmEllipses';
                        }
                           //that.gridObj.setRowColor(id, color);
                        for (var rowcnt=0; rowcnt<that.gridObj.getRowsNum(); rowcnt++){
                            var statusCol='0';  // to set the status column  
                            var statusVal = that.gridObj.cells(rowcnt, statusCol).getValue();    // to set the status
                            var statusValId = $(statusVal).data('adminsts');
                            if(statusValId == '108982')                         //C – Blue/Complete
                                that.gridObj.setRowColor(rowcnt,"#beffff");   
                            else if(statusValId == '108983')                    //P – Yellow/Pending
                                that.gridObj.setRowColor(rowcnt,"#ffff66");
                            else if(statusValId == '108981')                    //C – Red/Cancelled
                                that.gridObj.setRowColor(rowcnt,"#feb469");
                            else if(statusValId == '108980')                    //B – Green/Booked
                                that.gridObj.setRowColor(rowcnt,"#bed86f");
                        }
                        if (footerExportFL) {
                            var deleteIndexArr = []; //include the index of the column in the array if u want to export                 
                            $("#ps-report-list-export").unbind("click").bind("click", function (event) {
                                exportExcel(event, that.gridObj, deleteIndexArr);
                            });
                        }

                    } else {
                        $("#ps-report-list").html("No data found");
                    }

                });  
            },

            //Reports
            showHostCaseReport: function (input, title) {
                var that = this;
                this.$("#div-main-content").html(that.template_preceptorReport());
                console.log(input);
                getLoader("#ps-report-list");
                $(".gmPanelTitle").text(title);
                fnGetWebServerData("psrpt/listhost", undefined, input, function (data) {
                    console.log(data);

                    if (!data == "" || !data == undefined || !data == null) {
                        var rows = [],
                            i = 0;
                        _.each(data, function (griddt) {
                            var partyidarr =  parseNull(griddt.partyid).split(" / ");
                            var partynmarr =  parseNull(griddt.partynm).split(" / ");
                            var partyData="";
                            for(j=0; j<partyidarr.length; j++){
                                if(partyData !="")
                                    partyData+=" / ";
                                partyData+="<a class='gmReportLnk' href='#crm/surgeon/id/" + partyidarr[j] + "'>" + partynmarr[j] + "</a>";
                            }
                            rows[i] = {};
                            rows[i].id = i;
                            rows[i].data = [];
                            rows[i].data[0] = "<a class='gmReportLnk' href='#crm/preceptorship/host/id/" + griddt.actid + "'>" + griddt.actid + "</a>";                            
                            rows[i].data[1] = partyData;
//                            rows[i].data[1] = "<a class='gmReportLnk' href='#crm/surgeon/id/" + griddt.partyid + "'>" + griddt.partynm + "</a>";
                            rows[i].data[2] = griddt.actstartdate;
                            rows[i].data[3] = griddt.createdby;
                            rows[i].data[4] = griddt.createdate;
                            rows[i].data[5] = griddt.segnm;
                            rows[i].data[6] = griddt.pathology;
                            rows[i].data[7] = griddt.prodnm;
                            rows[i].data[8] = griddt.region;
                            rows[i].data[9] = griddt.zone;
                            rows[i].data[10] = griddt.division;
                            rows[i].data[11] = griddt.acttypeid;
                            rows[i].data[12] = griddt.fieldsales;
                            rows[i].data[13] = griddt.hospital;
                            rows[i].data[14] = griddt.actstatus;
                            rows[i].data[15] = griddt.actnotes;

                            i++;
                        });
                        var dataHost = {};
                        dataHost.rows = rows;
                        $(".yes-model").addClass("hide");
                        $(".no-model").addClass("hide");
                        $(".done-model").removeClass("hide");
                        $(".cancel-model").removeClass("hide");
                        var setInitWidths = "100,150,100,150,120,150,100,100,120,150,120,150,100,150,120,200";
                        var setColAlign = "left,left,center,left,center,left,left,left,left,left,left,left,left,left,left,left";
                        var setColTypes = "ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro";
                        var setColSorting = "sortLinkID,sortLink,date,str,date,str,str,str,str,str,str,str,str,str,str,str";
                        var setHeader = ["Host Case ID", "Surgeon Name", "Case Date", "Created by", "Created Date", "Procedure", "Pathology", "Product(s)", "Region", "Zone", "Division", "Preceptorship type", "Fieldsales", "Hospital", "Status", "Notes"];
                        var setFilter = ["#text_filter", "#text_filter", "#text_filter", "#text_filter", "#text_filter", "#text_filter", "#text_filter", "#text_filter", "#select_filter", "#select_filter", "#select_filter", "#select_filter", "#text_filter", "#text_filter", "#select_filter", "#text_filter"];
                        var enableTooltips = "true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true";
                        var footerArry = [];
                        var gridHeight = "";
                        var footerStyles = [];
                        var footerExportFL = true;
                        that.gridObj = loadDHTMLXGrid('ps-report-list', gridHeight, dataHost, setHeader, setInitWidths, setColAlign, setColTypes, setColSorting, enableTooltips, setFilter, footerArry, footerStyles, footerExportFL);

                        for (var i = 0; i < that.gridObj.getRowsNum(); i++) {
                            that.gridObj.cells(i, 0).cell.className = 'gmEllipses';
                            that.gridObj.cells(i, 1).cell.className = 'not_m_line';
                            that.gridObj.cells(i, 2).cell.className = 'gmEllipses';
                            that.gridObj.cells(i, 3).cell.className = 'gmEllipses';
                            that.gridObj.cells(i, 4).cell.className = 'gmEllipses';
                            that.gridObj.cells(i, 5).cell.className = 'gmEllipses';
                            that.gridObj.cells(i, 6).cell.className = 'gmEllipses';
                            that.gridObj.cells(i, 7).cell.className = 'gmEllipses';
                            that.gridObj.cells(i, 8).cell.className = 'gmEllipses';
                            that.gridObj.cells(i, 9).cell.className = 'gmEllipses';
                            that.gridObj.cells(i, 10).cell.className = 'gmEllipses';
                            that.gridObj.cells(i, 11).cell.className = 'gmEllipses';
                            that.gridObj.cells(i, 12).cell.className = 'gmEllipses';
                            that.gridObj.cells(i, 13).cell.className = 'gmEllipses';
                            that.gridObj.cells(i, 14).cell.className = 'gmEllipses';
                            that.gridObj.cells(i, 15).cell.className = 'gmEllipses';

                        }

                        if (footerExportFL) {
                            var deleteIndexArr = []; //include the index of the column in the array if u want to export                 
                            $("#ps-report-list-export").unbind("click").bind("click", function (event) {
                                exportExcel(event, that.gridObj, deleteIndexArr);
                            });
                        }
                    } else {
                        $("#ps-report-list").html("No data found");
                    }

                });

            },


            openCaseRequests: function (input, title) {
                var that = this;
                this.$("#div-main-content").html(that.template_preceptorReport());
                console.log(input);
                getLoader("#ps-report-list");
                $(".gmPanelTitle").text(title);
                fnGetWebServerData("psrpt/listcaserequest", undefined, input, function (data) {
                    console.log(data);
                    if (!data == "" || !data == undefined || !data == null) {
                        var rows = [],
                            i = 0;
                        _.each(data, function (griddt) {
                            rows[i] = {};
                            rows[i].id = i;
                            rows[i].data = [];
                            if (griddt.casereqid)
                                rows[i].data[0] = "<a class='gmReportLnk' href='#crm/preceptorship/case/id/" + griddt.casereqid + "'>" + griddt.casereqid + "</a>";
                            else
                                rows[i].data[0] = "";
                            if(griddt.atndtyp == 109000 || griddt.atndtyp == null)
                                rows[i].data[1] = "<a class='gmReportLnk' href='#crm/surgeon/id/" + griddt.surgid + "'>" + griddt.surgnm + "</a>";
                            else
                                rows[i].data[1] = "<a class='gmEllipses'>" + griddt.atndnm + "</a>";
                            rows[i].data[2] = griddt.atndtypnm;
                            rows[i].data[3] = griddt.hospital;
                            rows[i].data[4] = griddt.segnm;
                            rows[i].data[5] = griddt.region;
                            rows[i].data[6] = griddt.zone;
                            // rows[i].data[3] = griddt.pathology;
                            rows[i].data[7] = griddt.setnm;
                            rows[i].data[8] = griddt.fromdt;
                            rows[i].data[9] = griddt.todt;
                            rows[i].data[10] = griddt.createby;
                            rows[i].data[11] = griddt.createdate;
                            rows[i].data[12] = griddt.assnm;
                            rows[i].data[13] = griddt.division;
                            rows[i].data[14] = griddt.status;
                            rows[i].data[15] = griddt.ptype;
                            rows[i].data[16] = griddt.note;
                            i++;
                        });
                        var dataHost = {};
                        dataHost.rows = rows;
                        $(".yes-model").addClass("hide");
                        $(".no-model").addClass("hide");
                        $(".done-model").removeClass("hide");
                        $(".cancel-model").removeClass("hide");
                        var setInitWidths = "130,150,150,200,200,200,150,200,100,100,150,120,200,120,120,150,200";
                        var setColAlign = "left,left,left,left,left,left,left,left,center,center,left,center,left,left,left,left,left";
                        var setColTypes = "ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro";
                        var setColSorting = "sortLinkID,sortLink,str,str,str,str,str,str,date,date,str,date,str,str,str,str,str";
                        var setHeader = ["Case Request ID", "Name", "Type", "Hospital", "Procedure", "Region", "Zone", "Product(s)", "From Date", "To Date","Created By","Created Date", "Requesting Field Sales", "Division", "Status", "Preceptorship type", "Notes"];
                        var setFilter = ["#text_filter", "#text_filter", "#select_filter", "#text_filter", "#text_filter", "#select_filter", "#select_filter", "#text_filter", "#text_filter","#text_filter", "#text_filter","#text_filter", "#text_filter", "#select_filter", "#select_filter", "#select_filter", "#text_filter"];
                        var enableTooltips = "true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true";
                        var footerArry = [];
                        var gridHeight = "";
                        var footerStyles = [];
                        var footerExportFL = true;
                        that.crGridObj = loadDHTMLXGrid('ps-report-list', gridHeight, dataHost, setHeader, setInitWidths, setColAlign, setColTypes, setColSorting, enableTooltips, setFilter, footerArry, footerStyles, footerExportFL);

                        for (var i = 0; i < that.crGridObj.getRowsNum(); i++) {
                            that.crGridObj.cells(i, 0).cell.className = 'gmEllipses';
                            that.crGridObj.cells(i, 1).cell.className = 'gmEllipses';
                            that.crGridObj.cells(i, 2).cell.className = 'gmEllipses';
                            that.crGridObj.cells(i, 3).cell.className = 'gmEllipses';
                            that.crGridObj.cells(i, 4).cell.className = 'gmEllipses';
                            that.crGridObj.cells(i, 6).cell.className = 'gmEllipses';
                            that.crGridObj.cells(i, 7).cell.className = 'gmEllipses';
                            that.crGridObj.cells(i, 8).cell.className = 'gmEllipses';
                            that.crGridObj.cells(i, 9).cell.className = 'gmEllipses';
                            that.crGridObj.cells(i, 10).cell.className = 'gmEllipses';
                            that.crGridObj.cells(i, 11).cell.className = 'gmEllipses';
                            that.crGridObj.cells(i, 12).cell.className = 'gmEllipses';
                            that.crGridObj.cells(i, 14).cell.className = 'gmEllipses';
                            that.crGridObj.cells(i, 15).cell.className = 'gmEllipses';
                            that.crGridObj.cells(i, 16).cell.className = 'gmEllipses';

                        }
                        if (footerExportFL) {
                            var deleteIndexArr = []; //include the index of the column in the array if u want to export                 
                            $("#ps-report-list-export").unbind("click").bind("click", function (event) {
                                exportExcel(event, that.crGridObj, deleteIndexArr);
                            });
                        }
                    } else {
                        $("#ps-report-list").html("No data found");
                    }

                });
            },
            
            //All Case Request Report
            loadCaseRequests: function (input) {
                var that=this;
                getLoader("#ps-report-list");
                fnGetWebServerData("psrpt/listcaserequest", undefined, input, function (data) {
                    console.log(data);
                    if (!data == "" || !data == undefined || !data == null) {
                        var rows = [],
                            i = 0;
                        _.each(data, function (griddt) {
                            rows[i] = {};
                            rows[i].id = i;
                            rows[i].data = [];
                            if (griddt.casereqid)
                                rows[i].data[0] = "<a class='gmReportLnk' href='#crm/preceptorship/case/id/" + griddt.casereqid + "'>" + griddt.casereqid + "</a>";
                            else
                                rows[i].data[0] = "";
                            if(griddt.atndtyp == 109000 || griddt.atndtyp == null){
                                rows[i].data[1] = "<a class='gmReportLnk' data-adminsts='" + griddt.adminsts + "' href='#crm/surgeon/id/" + griddt.surgid + "'>" + griddt.surgnm + "</a>";
                            } 
                            else
                                rows[i].data[1] = "<a class='gmEllipses' data-adminsts='" + griddt.adminsts + "'>" + griddt.atndnm + "</a>";
                            rows[i].data[2] = griddt.atndtypnm;
                            rows[i].data[3] = griddt.hospital;
                            rows[i].data[4] = griddt.status;
                            rows[i].data[5] = griddt.adminstsnm;
                            if (griddt.actid)
                                rows[i].data[6] = "<a class='gmReportLnk' href='#crm/preceptorship/host/id/" + griddt.actid + "'>" + griddt.actid + "</a>";
                            else
                                rows[i].data[6] = '';
                            rows[i].data[7] = griddt.region;
                            rows[i].data[8] = griddt.zone;
                            rows[i].data[9] = griddt.division;
                            rows[i].data[10] = griddt.pstype;
                            rows[i].data[11] = griddt.segnm;
                            rows[i].data[12] = griddt.setnms;
                            rows[i].data[13] = griddt.fromdt;
                            rows[i].data[14] = griddt.todt;
                            rows[i].data[15] = griddt.fieldattnd;
                            rows[i].data[16] = griddt.createdby;
                            rows[i].data[17] = griddt.createdate;
                            rows[i].data[18] = griddt.hotlnm;
                            if (griddt.hotlprice) {
                                rows[i].data[19] = fmtPrice(griddt.hotlprice);
                            } else {
                                rows[i].data[19] = "";
                            }
                            rows[i].data[20] = griddt.trvlnm;
                            if (griddt.trvlprice) {
                                rows[i].data[21] = fmtPrice(griddt.trvlprice);
                            } else {
                                rows[i].data[21] = "";
                            }
                            rows[i].data[22] = griddt.crfnm;
                            rows[i].data[23] = griddt.trfnm;
                            rows[i].data[24] = griddt.labinv;
                            rows[i].data[25] = griddt.pcmninv;
                            rows[i].data[26] = griddt.surfl;
                            rows[i].data[27] = griddt.crdfl;
                            rows[i].data[28] = griddt.note;
                            
                            i++;
                        });
                        var dataHost = {};
                        dataHost.rows = rows;
                        $(".yes-model").addClass("hide");
                        $(".no-model").addClass("hide");
                        $(".done-model").removeClass("hide");
                        $(".cancel-model").removeClass("hide");
                        var setInitWidths = "130,150,150,150,120,120,120,120,150,120,200,200,200,120,120,150,150,150,150,100,200,100,150,150,180,180,150,200,150";
                        var setColAlign = "left,left,left,left,left,left,left,left,left,left,left,left,left,center,center,left,left,center,left,right,left,right,left,left,left,left,left,left,left";
                        var setColTypes = "ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro";
                        var setColSorting = "sortLinkID,sortLink,str,str,str,str,sortLinkID,str,str,str,str,str,str,date,date,str,str,date,str,str,str,str,str,str,str,str,str,str,str";
                        var setHeader = ["Case Request ID", "Name", "Type", "Hospital", "Status", "Admin Status" ,"HC-ID" ,"Region", "Zone", "Division", "Preceptorship type", "Procedure",  "Product(s)", "From Date", "To Date", "Requesting Field Sales",    "Created By" , "Created Date" , "Hotel Name", "Hotel Price", "Travel Name",
                        "Travel Price","PCRF/CRF Status", "TRF Status", "Lab Invoice", "Specimen Invoice", "Survey Sent?", "Credential Required?", "Notes"];
                        var setFilter = ["#text_filter", "#text_filter", "#select_filter", "#text_filter", "#select_filter" ,"#select_filter", "#text_filter", "#select_filter", "#select_filter", "#select_filter", "#select_filter", "#text_filter", "#text_filter", "#text_filter", "#text_filter", "#text_filter", "#text_filter","#text_filter","#text_filter","#text_filter","#text_filter","#text_filter","#select_filter","#select_filter","#text_filter","#text_filter","#text_filter","#text_filter","#text_filter"];
                        var enableTooltips = "true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true";
                        var footerArry = [];
                        var gridHeight = "";
                        var footerStyles = [];
                        var footerExportFL = true;
                        that.crGridObj = loadDHTMLXGrid('ps-report-list', gridHeight, dataHost, setHeader, setInitWidths, setColAlign, setColTypes, setColSorting, enableTooltips, setFilter, footerArry, footerStyles, footerExportFL);

                        for (var i = 0; i < that.crGridObj.getRowsNum(); i++) {
                            that.crGridObj.cells(i, 0).cell.className = 'gmEllipses';
                            that.crGridObj.cells(i, 1).cell.className = 'gmEllipses';
                            that.crGridObj.cells(i, 2).cell.className = 'gmEllipses';
                            that.crGridObj.cells(i, 3).cell.className = 'gmEllipses';
                            that.crGridObj.cells(i, 4).cell.className = 'gmEllipses';
                            that.crGridObj.cells(i, 5).cell.className = 'gmEllipses';
                            that.crGridObj.cells(i, 6).cell.className = 'gmEllipses';
                            that.crGridObj.cells(i, 7).cell.className = 'gmEllipses';
                            that.crGridObj.cells(i, 8).cell.className = 'gmEllipses';
                            that.crGridObj.cells(i, 9).cell.className = 'gmEllipses';
                            that.crGridObj.cells(i, 10).cell.className = 'gmEllipses';
                            that.crGridObj.cells(i, 11).cell.className = 'gmEllipses';
                            that.crGridObj.cells(i, 12).cell.className = 'gmEllipses';
                            that.crGridObj.cells(i, 13).cell.className = 'gmEllipses';
                            that.crGridObj.cells(i, 14).cell.className = 'gmEllipses';
                            that.crGridObj.cells(i, 15).cell.className = 'gmEllipses';
                            that.crGridObj.cells(i, 17).cell.className = 'gmEllipses';
                            that.crGridObj.cells(i, 19).cell.className = 'gmEllipses';
                            that.crGridObj.cells(i, 23).cell.className = 'gmEllipses';
                            that.crGridObj.cells(i, 24).cell.className = 'gmEllipses';
                            that.crGridObj.cells(i, 26).cell.className = 'gmEllipses';
                            that.crGridObj.cells(i, 27).cell.className = 'gmEllipses';
                            that.crGridObj.cells(i, 28).cell.className = 'gmEllipses';

                        }
                         //that.gridObj.setRowColor(id, color);
                        for (var rowcnt=0; rowcnt<that.crGridObj.getRowsNum(); rowcnt++){
                            var statusCol='1';  // to set the status column  
                            var statusVal = that.crGridObj.cells(rowcnt, statusCol).getValue();    // to set the status
                            var statusValId = $(statusVal).data('adminsts');
                            if(statusValId == '108982')                         //C – Blue/Complete
                                that.crGridObj.setRowColor(rowcnt,"#beffff");   
                            else if(statusValId == '108983')                    //P – Yellow/Pending
                                that.crGridObj.setRowColor(rowcnt,"#ffff66");
                            else if(statusValId == '108981')                    //C – Red/Cancelled
                                that.crGridObj.setRowColor(rowcnt,"#feb469");
                            else if(statusValId == '108980')                    //B – Green/Booked
                                that.crGridObj.setRowColor(rowcnt,"#bed86f");
                        }
                        
                        if (footerExportFL) {
                            var deleteIndexArr = []; //include the index of the column in the array if u want to export                 
                            $("#ps-report-list-export").unbind("click").bind("click", function (event) {
                                exportExcel(event, that.crGridObj, deleteIndexArr);
                            });
                        }
                    } else {
                        $("#ps-report-list").html("No data found");
                    }

                });
            },




        });
        return GM.View.PSList;
    });
