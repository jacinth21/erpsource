/**********************************************************************************
 * File:        GmMainRouter.js
 * Description: Main Router common to all applications
 * Version:     1.0
 * Author:      ejayabalan
 **********************************************************************************/

define(['jquery', 'underscore', 'backbone', 'mainview'
],

    function ($, _, Backbone, MainView) {

        'use strict';

        var MainRouter = Backbone.Router.extend({

            routes: {
                "price": "price",
                "price/report": "price",
                "price/pricingSubmittedRequests": "price"
            },

            initialize: function () {
                this.firstPage = true;
                var view = new MainView();
                view.render();
            },
        });

        return MainRouter;
    });
