/* Main Router */
define([
  'jquery', 'underscore', 'backbone', 'global',
  'gmvApp', 'gmvCRMDashboard', 'gmvCRMMain', 'gmvLogin', 'gmvMain'
], 
    
function ($, _, Backbone, Global, GMVApp, GMVCRMDashboard, GMVCRMMain, GMVLogin, GMVMain) {

    'use strict';
    
    GM.Router.Main = Backbone.Router.extend({
    
        routes: {
            ""                      : "login",
            "crm"                   : "crm"
        },
        
        initialize: function() {
	       
            var gmvMain = new GMVMain();
            GM.Global.SalesRep = isRep();
            
        },

        login: function() {
			var that = this;
            if(window.localStorage.getItem("token")==null) {
                //var gmvLogin = new GMVLogin();
                gmvApp.showView(gmvLogin);
			} else {
				// globus login authentication using keycloak values
				var gmvLogin = new GMVLogin();
            	gmvLogin.authenticate(); 
                if(GM.Global.PortalUrl)
                    window.location.href = GM.Global.PortalUrl
                else
                    window.location.href = "#crm";
            }

        },
		
 crm: function() {

            GM.Global.SalesRep = isRep();
            this.getTabs(function(){
                GM.Global.CRMMain = true;
                var gmvCRMMain = new GMVCRMMain();
                gmvCRMMain.render();
                // gmvApp.showView(gmvCRMMain);

                window.location.href = "#crm/dashboard";
            });

            
        },

        getTabs: function(callback) {
            fnGetWebServerData("crm/usertabs","gmCRMTabsVO",{token : GM.Global.Token}, function(arrTabs) {
                // console.log("UserTabs is as follows " + JSON.stringify(arrTabs));
                var userTabs = new Array();
                var userAccess = new Array();
                GM.Global.Admin = [];
                userTabs.push(tabData["Dashboard"]);
                userTabs[0].tabcaption = "Dashboard";
                if(arrTabs!=null) {
                    // console.log("ArrTABS is " + arrTabs);
                    arrTabs = arrayOf(arrTabs);
                    
                    //console.log("UserTabs ARRAY is as follows " + JSON.stringify(arrTabs));
                    var innerTabs = [];
                    for(var i=0;i<arrTabs.length && arrTabs.length>0;i++) {
                        var tab = tabData[arrTabs[i].funcnm];
                        
                        if(tab!=undefined) {
                            tab.tabcaption = arrTabs[i].funcnm;
                            tab.readAcc = arrTabs[i].readacc;
                            tab.updateAcc = arrTabs[i].updateacc;
                            tab.voidAcc = arrTabs[i].voidacc;
                            tab.functionID = arrTabs[i].funcid;
                            console.log(tab);
                            if(!_.contains(innerTabs, tab.taburl))
                                userTabs.push(tab);
                            innerTabs.push(tab.taburl)
                        }
                        else 
                            userAccess.push(arrTabs[i]);
                    }
                }
                else {
                    userTabs.push(tabData["Surgeon"]);
                    userTabs[1].tabcaption = "Surgeon";
                }
                
                crmUserTabs = userTabs;
                GM.Global.UserAccess = userAccess;
                console.log(userAccess);
//                if (_.contains(userAccess, "CRMADMIN-" + module.toUpperCase())) {
                    _.each(userAccess, function(data){
                        console.log(data.funcid.indexOf("CRMADMIN-"))
                        if(data.funcid.indexOf("CRMRQST-")>=0){
                            var module = data.funcid.replace("CRMRQST-", "");
                            module = module.toLowerCase();
                            
                            if(module == "physvisit"){
                                GM.Global.Admin.push("physvisitrqst");
                            }
                            else if(!_.contains(GM.Global.Admin, module))    // Loading all accessed modules into global array without duplicates.
                                GM.Global.Admin.push(module);
                            

                        }
                        if(data.funcid.indexOf("CRMADMIN-")>=0){
                            var module = data.funcid.replace("CRMADMIN-", "");
                            module = module.toLowerCase();
                            if(module == "lead"){
                                if(!_.contains(GM.Global.Admin, "lead"))   
                                    GM.Global.Admin.push("lead");
                                if(!_.contains(GM.Global.Admin, "tradeshow"))    // Loading tradeshow if we have lead
                                    GM.Global.Admin.push("tradeshow");
                            }
                            else if(module == "physvisit"){
                                GM.Global.Admin.push("physicianvisit");
                            }
                            else if(!_.contains(GM.Global.Admin, module))    // Loading all accessed modules into global array without duplicates.
                                GM.Global.Admin.push(module);
                        }
                    });
                if(GM.Global.SwitchUser)
                    GM.Global.Admin = [];
                console.log("Final UserTabs ARRAY is as follows " + JSON.stringify(userTabs));
                console.log("Final UserAccess ARRAY is as follows " + JSON.stringify(GM.Global.UserAccess));
                window.localStorage.setItem("userTabs", JSON.stringify(userTabs));
                window.localStorage.setItem("userAccess", JSON.stringify(userAccess));
                if(GM.Global.Admin.length)
                    window.localStorage.setItem("userAdmin", JSON.stringify(GM.Global.Admin));
                if(callback!=undefined)
                callback();
            });
        }
    });

    return GM.Router.Main;
});