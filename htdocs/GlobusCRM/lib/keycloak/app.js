    var accessTokenLifespanInMinutes = 5; // In Minutes accessTokenLifespanInMinutes
    var keycloakConfig = {url: "https://logintest.globusone.com/auth", realm: "globus-medical", clientId: "erp-client"};
    var keycloakService = {};
    var config = keycloakConfig;
	// init keycloak variables
    keycloakService.keycloak = Keycloak(config);
    keycloakService.token;
    keycloakService.parsedToken;
    keycloakService.userInfo;
    keycloakService.userProfile;

    const KeycloakService = keycloakService;
	// update keycloak token
    keycloakService.updateToken = function () {
        setInterval(function(){
            keycloakService.keycloak.updateToken(10).success(function () {
                console.log('Token updated');
                console.log(keycloakService.getCurrentTOken());
                //localStorage.setItem('token', keycloakService.getCurrentTOken());
            }).error(function (e) {
                console.log(e)
            });
        }, (((accessTokenLifespanInMinutes * 60) - 10) * 1000));
    }
	// keycloak authentication and get token & values from keycloak
    keycloakService.onAuthentication = function (authenticated) {
        keycloakService.updateToken();
        keycloakService.token = keycloakService.getCurrentTOken();
        console.log(keycloakService.token);
        localStorage.setItem('keycloaktoken', keycloakService.token);
        keycloakService.keycloak.loadUserProfile();
        keycloakService.parsedToken = keycloakService.keycloak.tokenParsed;
        keycloakService.keycloak.loadUserInfo().success(function (userInfo) {
            keycloakService.userInfo = userInfo;
            console.log(keycloakService);
        }).error(function (e) {
            console.log(e);
        });
		// get userdetails from keycloak
        keycloakService.keycloak.loadUserProfile().success(function (userProfile) {
            keycloakService.userProfile = userProfile;
            console.log(keycloakService);
            var varUserName = keycloakService.userProfile.firstName +" "+ keycloakService.userProfile.lastName;
            $("#headerUsername").html(varUserName);
            localStorage.setItem("userName", varUserName);
            var varLoginName = keycloakService.userProfile.username;
            localStorage.setItem("loginName", varLoginName);   
            localStorage.setItem('token', varLoginName);
            
        }).error(function (e) {
            console.log(e)
        });
    }
	// keycloak authentication fail method
    keycloakService.onFail = function (e) {
        alert("fail");
        console.log(e);
    }
	// keycloak get current token
    keycloakService.getCurrentTOken = function () {
        return keycloakService.keycloak.token;
    }
	// keycloak init functionality
    keycloakService.keycloak.init({ onLoad: 'login-required',checkLoginIframe: false}).success(function() {
    	  keycloakService.onAuthentication();
        //app initialize 
    }).error(keycloakService.onFail);   