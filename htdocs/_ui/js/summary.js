/*******************************************************************************

	CSS on Sails Framework
	Title: Case and Set Management Phase 2
	Author: XHTMLized (http://www.xhtmlized.com/)
	Date: November 2011

*******************************************************************************/

var SummaryUi = {
    modalScroll: '',
    isIpad: false,
	init: function(){
        SummaryUi.isiPad = navigator.userAgent.match(/iPad/i) != null;
	   
		$('.btn-more').click(function(e){
			e.preventDefault();
			
			// get modal dialog id
			var id = $(this).attr('data-id');
			
			// show lightbox now
			$('#modal-' + id).lightbox_me({
				centered: true,
				zIndex: 9999,
				onLoad: function() { 
                    if (SummaryUi.isiPad) {
    				    $('body').bind('touchmove',function(e){
    				        e.preventDefault();
                        });
            			SummaryUi.modalScroll = new iScroll('modal-scroll-' + id, {scrollbarClass: 'modal-content-scrollbar'});
                    }
				},
				onClose: function() {
				    if (SummaryUi.isiPad) {
    				    $('body').unbind('touchmove');
    				}
				}
			});
		});
	}
};

$(document).ready(function(){
	SummaryUi.init();
});