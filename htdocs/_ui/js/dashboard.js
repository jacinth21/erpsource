/*******************************************************************************

	CSS on Sails Framework
	Title: Case and Set Management Phase 2
	Author: XHTMLized (http://www.xhtmlized.com/)
	Date: November 2011

*******************************************************************************/

var DashboardUi = {
	init: function(){
		$('#where-am-i').hide();
	}
};

$(document).ready(function(){
	DashboardUi.init();
});