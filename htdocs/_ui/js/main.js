/*******************************************************************************

	CSS on Sails Framework
	Title: Case and Set Management Phase 2
	Author: XHTMLized (http://www.xhtmlized.com/)
	Date: November 2011

*******************************************************************************/
var Navigation = {
	scrollPane: null,
	
	init: function(){
		$('.btn-open-nav').click(function(e){
			e.preventDefault();
			
			$('#navigation').hide();
			$('#navigation-expanded').show();
		});
		
		$('.btn-close-nav').click(function(e){
			e.preventDefault();
			
			$('#navigation').show();
			$('#navigation-expanded').hide();
		});
		
		// build tree
		$('#navigation-tree').
		bind('open_node.jstree close_node.jstree', function (e) {
		}).
		bind('loaded.jstree', function (event, data){
			// jscroll pane
			Navigation.scrollPane = $('#navigation-tree').jScrollPane({
				showArrows: true,
				verticalDragMinHeight: 84,
				verticalDragMaxHeight: 84,
				horizontalDragMinWidth: 28,
				horizontalDragMaxWidth: 28,
				autoReinitialise: true
			});
		}).
		jstree({
		'themes' : {
			'theme' : 'default',
			'dots' : true,
			'icons' : false
		}, 
		'plugins' : ['themes', 'html_data']
		});
	}
};

$(document).ready(function(){
	Navigation.init();
});