/*******************************************************************************

	CSS on Sails Framework
	Title: Case and Set Management Phase 2
	Author: XHTMLized (http://www.xhtmlized.com/)
	Date: November 2011

*******************************************************************************/
var LoginUi = {
	init: function(){
		$('#login-form').submit(function(){
			// get values
			var username = $.trim($('#username').val());
			var password = $.trim($('#password').val());
			
			if(!username.length) {
				alert('Please enter your username');
				return false;
			}
			
			if(!password.length) {
				alert('Please enter your password');
				return false;
			}
			
			return true;
		});
	}
};

$(document).ready(function(){
	LoginUi.init();
});