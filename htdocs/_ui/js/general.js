/*******************************************************************************

	CSS on Sails Framework
	Title: Case and Set Management Phase 2
	Author: XHTMLized (http://www.xhtmlized.com/)
	Date: November 2011

*******************************************************************************/

var GeneralUi = {
	init: function(){
		jQuery('input[type="checkbox"]').JSizedFormCheckbox();

		jQuery('input[type="radio"]').JSizedFormRadio();

		jQuery('select').JSizedFormSelect();
		
		// init date picker
		$('#survey-date').datepicker({
			showOn: 'both',
			buttonImage: '_ui/images/general/ico_calendar.jpg',
			buttonImageOnly: true
		});
		
		// init spinner
		$('#number-of-levels').spinner({
			min: 1, 
			max: 100, 
			increment: 1,
			width: 31
		});
	}
};

$(document).ready(function(){
	GeneralUi.init();
});