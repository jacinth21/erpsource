var split_At = '';
var w1,dhxWins;
 


function setDefalutGridProperties(divRef,footerArry)
{
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	if(footerArry != undefined && footerArry.length >0)
	gObj.attachFooter(footerArry);
	gObj.enableMultiline(true);	
	return gObj;
}

function initializeDefaultGrid(gObj, gridData, split_At)
{	
	gObj.init();
	if(split_At !='' || split_At != 'undefined')
	{
		gObj.splitAt(split_At);
	}
	gObj.enableDistributedParsing(true);
	gObj.loadXMLString(gridData);	
	return gObj;
}

function initGrid(divRef,gridData,split_At,footerArry){
	var gObj = setDefalutGridProperties(divRef,footerArry);
	return initializeDefaultGrid(gObj,gridData,split_At);
}
 

