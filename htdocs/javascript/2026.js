/*
	Pricing Links
*/
var TREE_ITEMS = [
	['Home', 'GmSaleDashBoardServlet\',\''],
	['Setup', null,
		['Account Setup', 'GmAccountServlet\',\''],
		['Part Number Pricing', 'GmPartPriceReportServlet\',\''],
		['Group Part Pricing', 'gmGroupPartPricingDAction.do?method=loadSystemPrice\',\''],
		['Account Pricing',null,
				['By Project', 'GmAccountPriceReportServlet\',\'Project@Account'],
				['Batch Update', 'GmAccountPriceReportServlet\',\'Batch@Account'],
		],
		['GPO Pricing',null,
				['By Project', 'GmAccountPriceReportServlet\',\'Project@GPO'],
				['Batch Update', 'GmAccountPriceReportServlet\',\'Batch@GPO'],
		],
	],
	['Transactions', null,
		['Modify Order', 'GmOrderVoidServlet\',\''],
	],
	['Reports', null,
		['Part Number Search', 'GmPartSearchServlet\',\''],
		['Field Sales',null,
			['Field Sales Report', 'gmDistributorReport.do\',\''],
			['Field Sales/Accounts Report', 'GmAccountReportServlet\',\'ALL'],
			['Field Sales/Reps Report', 'GmAccountReportServlet\',\'REPALL'],
		],
			['Accounts Report', 'GmAccountReportServlet\',\''],
			['Detailed Monthly Sales', 'GmDetailedSalesServlet\',\''],
			['Monthly Dashboard', 'GmMonthlyDashboardServlet\',\'LoadStateSales'],
			['Sales By State', 'GmDetailStateSalesServlet\',\'LoadStateSales'],
			['Sales By Project', 'GmSalesByProjectServlet\',\'LoadProjectSales'],
			['Pricing', 'GmSalesAcctPriceReportServlet\',\'CurrentPrice'],
			['Pricing: w/ Discount %', 'GmSalesAcctPriceReportServlet\',\'DTL'],
			['Pricing: w/ Increase %', 'GmSalesAcctPriceReportServlet\',\'PriceIncrease'],
			['Held Orders', 'GmPendingOrdersPOServlet\',\'HELDRPT'],
		['Historical Sales',null,
			['By Field Sales', 'GmSalesYTDReportServlet\',\'LoadDistributorA'],
			['By Spine Specialist', 'GmSalesYTDReportServlet\',\'LoadRepA'],
			['By Account', 'GmSalesYTDReportServlet\',\'LoadAccountA'],
			['By System', 'GmSetYTDReportServlet\',\'LoadGroupA'],
			['By Part', 'GmSalesYTDReportServlet\',\'LoadPartA'],
		],
	],
	['Documents', null,
		['Forms', 'GmSalesForms.html'],
		['Product Info', 'GmProdInfo.html'],
		['Registrations/Certificates', 'GmRegnCert.html'],
		['Inserts', 'GmProdInserts.html'],
		['510(k)', '510k.html'],
		['How To', 'GmHowTo.html'],
		['Product Demo', 'GmProdDemo.html']
	],
];