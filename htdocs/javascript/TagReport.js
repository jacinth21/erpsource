function fnGetNames()
{	

var val = document.frmTagReport.locationType.value;

var values =document.frmTagReport.locationId.value;

if (val == 4120) 
{
	document.all.name.options.length = 0;
	document.all.name.options[0] = new Option("[Choose One]","0");
	for (var i=0;i<DistLen;i++)
	{
		arr = eval("DistArr"+i);
		arrobj = arr.split(",");
		id = arrobj[0];
		name = arrobj[1];
		document.all.name.options[i+1] = new Option(name,id);
		if ( id == values ) {
           document.all.name.options[i+1].selected = true;
		}
	}		
	//document.getElementById('addressid').disabled = true
}


if (val == 40033) //In-house
{
	document.all.name.options.length = 0;
	document.all.name.options[0] = new Option("[Choose One]","0");	
	for (var i=0;i<EmpLen;i++)
	{
		arr = eval("EmpArr"+i);
		arrobj = arr.split(",");
		id = arrobj[0];
		name = arrobj[1];
		document.all.name.options[i+1] = new Option(name,id);
		if ( id == values ) {
           document.all.name.options[i+1].selected = true;
		}
	}	
}
/*alert("test"+values);
 if(values!='' &&value!='0')
	{
	document.getElementById('name').value = values;
	}*/

if(val == 4124 || val == 0 ){	
	document.all.name.options.length = 0;
	document.all.name.options[0] = new Option("[Choose One]","0");
}	
	//document.getElementById('addressid').options.length = 0;


}
function fnOnPageLoad(){
	var status = document.frmTagReport.hStatusGrp.value;
	if (status != '')
	{
		objgrp = document.frmTagReport.Chk_GrpStatus;
		fnCheckSelections(status,objgrp);
	}
}

function fnCheckSelections(val,objgrp)
{
	var valobj = val.split(",");
	var sval = '';
	var arrlen = objgrp.length;
	
	if (arrlen > 0)
	{
		for (var j=0;j< valobj.length;j++ )
		{
			sval = valobj[j];
			for (var i=0;i< arrlen;i++ )
			{
				if (objgrp[i].value == sval)
				{
					objgrp[i].checked = true;
					break;
				}
			}
		}
	}
	else
	{
		objgrp.checked = true;
	}
}
function fnGo()
{
	var compID = document.frmTagReport.companyNm.value;
	var plantNameID = document.frmTagReport.palntNm.value;
	var objStat = document.frmTagReport.Chk_GrpStatus;
	var statlen = objStat.length;
	var statstr = '';
	var statgrpstr = '';	
	var temp = '';
	var statusType = '';

	for (var i=0;i<statlen;i++)
	{
		if (objStat[i].checked == true)
		{
			
			temp = objStat[i].value;			
			statgrpstr = statgrpstr + objStat[i].value + ',';			
			statstr = statstr + temp + ',';	
			
			statusType = statstr.substr(0,statstr.length-1);

			if((statusType == '51010' || statusType == '51011') && compID != '0'){
				Error_Details(" Please change the <B>Company</B> to [Choose one] as <B>Status</B> selected as <B>Available/Released</B> ");
			}
			if (ErrorCount > 0){
				Error_Show();
				Error_Clear();
				return false;
			}
		}
	}

	if(document.frmTagReport.voidedTags.checked == false){		
	if(document.frmTagReport.tagIdFrom.value == "" && document.frmTagReport.tagIdTo.value != ""){
		Error_Details(" Please select a Tag Range From: ");
		}
	if(document.frmTagReport.setID.value == '' && document.frmTagReport.partNum.value == ''  &&statusType=='' &&document.frmTagReport.inventoryType.value =='0'  &&document.frmTagReport.locationType.value =='0' &&document.frmTagReport.name.value =='0'&&document.frmTagReport.tagIdFrom.value == '' && document.frmTagReport.tagIdTo.value == '' && document.frmTagReport.companyNm.value == '0' && document.frmTagReport.palntNm.value == '0'){
		Error_Details(" Please select the filter condition");
		}
	}	
	
	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}else{
		document.frmTagReport.action = '/gmTagReport.do?strOpt=reload&status='+statstr.substr(0,statstr.length-1); 	
		fnStartProgress('Y');
		document.frmTagReport.submit();
	}	
}
function fnTagHistory(tagid)
{ 	 
	windowOpener("/gmTagHistory.do?tagID="+tagid,"TagHistory","resizable=yes,scrollbars=yes,top=250,left=300,width=1050,height=400");
} 
function fnReload()
{
	document.frmTagReport.strOpt.value = "reload";
	document.frmTagReport.submit();
}
function fnEnter()
{
		if (event.keyCode == 13)
		{ 
			fnGo();
		}
}
function fnTagEdit(tagId,status)
{
	//var status = document.frmTagReport.typeStatus.value;
	document.frmTagReport.action = '/gmTagTransfer.do?tagId='+tagId+"&status="+status;
	document.frmTagReport.strOpt.value='fetch';
	
	document.frmTagReport.submit();	 
}

function fnVoidTag(tagId, tagStatus){
		if(tagStatus == '102401'){ // Transferred
			Error_Details("Cannot perform this action on a set in <b>Transferred</b> status");
		}
		if (ErrorCount > 0) {
			Error_Show();
			Error_Clear();
			return false;
		}else{
			document.frmTagReport.hTxnId.value = tagId;
			document.frmTagReport.action ="/GmCommonCancelServlet";
			document.frmTagReport.hAction.value = "Load";		
			document.frmTagReport.hCancelType.value = 'VFTAG';
			document.frmTagReport.hRedirectURL.value =  "\gmTagReport.do?strOpt=Load";
			document.frmTagReport.hDisplayNm.value = "Tag Report Screen";
			document.frmTagReport.submit();
		}		
}

function fnUnVoidTag(tagId)
{
		document.frmTagReport.hTxnId.value = tagId;
		document.frmTagReport.action ="/GmCommonCancelServlet";
		document.frmTagReport.hAction.value = "Load";		
		document.frmTagReport.hCancelType.value = 'UVFTAG';
		document.frmTagReport.hRedirectURL.value =  "\gmTagReport.do?strOpt=Load";
		document.frmTagReport.hDisplayNm.value = "Tag Report Screen";
		document.frmTagReport.submit();
}

function fnGetPlantList(obj) {
	var compnyId = obj.value;
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmTagReport.do?strOpt=fetchCompanyPlants&companyNm=' + TRIM(compnyId) +'&ramdomId=' + Math.random());
	dhtmlxAjax.get(ajaxUrl, fnSetPlantDetails);
}

//Description : This function used to load the Plant Drop down details based on the COmpany ID Selection
function fnSetPlantDetails(loader) {
	var response = loader.xmlDoc.responseXML;	
	var plantDetails = response.getElementsByTagName("pnum");
	var defaultMode = response.getElementsByTagName("defplant");
	document.frmTagReport.palntNm.options.length = 0;
	document.frmTagReport.palntNm.options[0] = new Option("[Choose One]","0");
	if(plantDetails.length == 0){
		for (var i=0;i<PlantLength;i++){
			arr = eval('AllPlantArr'+i);
			arrobj = arr.split(",");
			id = arrobj[0];
			name = arrobj[1];
			document.frmTagReport.palntNm.options[i+1] = new Option(name,id);
		}
		document.frmTagReport.palntNm.value = '0';
	}else{
		for (var x=0; x<plantDetails.length; x++){
		   	strPlantID = parseXmlNode(plantDetails[x].childNodes[0].firstChild);
		   	strPlantName = parseXmlNode(plantDetails[x].childNodes[1].firstChild);
		   	document.frmTagReport.palntNm.options[x+1] = new Option(strPlantName,strPlantID);	   	
		}	
		var defaultMode = parseXmlNode(response.getElementsByTagName("defplant")[0].firstChild);
		document.frmTagReport.palntNm.value = defaultMode;	
	}	
}
function fnDownloadXLS(){
		gridObj.toExcel('/phpapp/excel/generate.php');	
}
