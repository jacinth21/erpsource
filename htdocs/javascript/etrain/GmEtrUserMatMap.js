function fnOpenForm(Id, name, sesnMatUserId, matCmpltnDt)
{
	document.frmEtrUserMatMap.fileNm.value = name;
	document.frmEtrUserMatMap.fileId.value = Id;
	document.frmEtrUserMatMap.sesnUserMatId.value = sesnMatUserId;
	document.frmEtrUserMatMap.matCmpltnDt.value = matCmpltnDt;
	var	sessionId = document.frmEtrUserMatMap.sessionId.value;
	var moduleId = document.frmEtrUserMatMap.moduleId.value;
	windowOpener("/etrain/GmEtrModelWindow.jsp?fileId="+Id+"&sesnUserMatId="+sesnMatUserId+"&matCmpltnDt="+matCmpltnDt+"&sessionId="+sessionId+"&moduleId="+moduleId,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=600,height=50,status=1");
	/* other solution......
	windowOpener("/GmCommonFileOpenServlet?sId="+Id+"&uploadString=ETRUPLOADHOME","PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=775,height=500,status=1");
	
	var vagree=confirm("Are you agree on completion of document?");
	if (vagree)
		document.frmEtrUserMatMap.trcmpltn.value = "on";
	else
		document.frmEtrUserMatMap.trcmpltn.value = "";
	document.frmEtrUserMatMap.submit();
	*/
} 

function fnCloseFile()
{
	//alert(document.frmEtrUserMatMap.sesnUserMatId.value+'..'+document.frmEtrUserMatMap.userMatTrackId.value);
	document.frmEtrUserMatMap.submit();
}

function fnSubmit()
{
	/* For pushing of record*/
	var chk_length =  document.frmEtrUserMatMap.Chk_matid.length;
	var inputString = "";
	if (chk_length == undefined)
	{
	 if(document.frmEtrUserMatMap.Chk_matid.checked)
		 {  	 			
	        var val = document.frmEtrUserMatMap.Chk_matid.id;
			objRefType = eval("document.frmEtrUserMatMap.reftype"+val);
			inputString = document.frmEtrUserMatMap.Chk_matid.id+ "^" +objRefType.value+"|";		
		 }			 	
	}
	else
	{
	  for (i = 0; i < chk_length; i++) 
	  {		  
			if (document.frmEtrUserMatMap.Chk_matid[i].checked ) 
			{
				var val = document.frmEtrUserMatMap.Chk_matid[i].id;
				objRefType = eval("document.frmEtrUserMatMap.reftype"+val);
				inputString += document.frmEtrUserMatMap.Chk_matid[i].id+ "^" +objRefType.value+"|";		     
						    
			}
	   }   	   
	}
	/*for pull out of record..*/
	var chk_length1 =  document.frmEtrUserMatMap.Chk_matid1.length;
	var inputString1 = "";
	if (chk_length1 == undefined)
	{
	 if(document.frmEtrUserMatMap.Chk_matid1.checked)
		 {  	 			
	        inputString1 = document.frmEtrUserMatMap.Chk_matid1.id+"|";		
		 }			 	
	}
	else
	{
	  for (i = 0; i < chk_length1; i++) 
	  {		  
			if (document.frmEtrUserMatMap.Chk_matid1[i].checked ) 
			{
				inputString1 += document.frmEtrUserMatMap.Chk_matid1[i].id+"|";		     
						    
			}
	   }  
	   	   
	}
	if(inputString1 == "" && inputString == "" )
	    {
	      Error_Details("Please Select atleast one record to Push/Pull. ");
	    }
	/*end of both*/
	if (ErrorCount > 0)
		 {
			Error_Show();
			Error_Clear();
			return false;
		}

	document.frmEtrUserMatMap.hinputString.value = inputString;
	document.frmEtrUserMatMap.hinputString1.value = inputString1;
	document.frmEtrUserMatMap.submit();
}

function fnSelectAll()
{
				objSelAll = document.frmEtrUserMatMap.selectAll;
				if (objSelAll) {
				var objChk_matid = document.frmEtrUserMatMap.Chk_matid;
				var chk_length =  objChk_matid.length; 
				if (objSelAll.checked)
				{
					for(i = 0; i < chk_length; i ++) 
					{	
						objChk_matid[i].checked = true;
					}
				}
				else {										
					for(i = 0; i < chk_length; i ++) 
					{	
						objChk_matid[i].checked = false;
					}
				}
				}
	}

	function fnApplyAll()
{
				objSelAll = document.frmEtrUserMatMap.applyAll;
				if (objSelAll) {
				var objChk_matid1 = document.frmEtrUserMatMap.Chk_matid1;
				var chk_length =  objChk_matid1.length; 
				if (objSelAll.checked)
				{
					for(i = 0; i < chk_length; i ++) 
					{	
						objChk_matid1[i].checked = true;
					}
				}
				else {										
					for(i = 0; i < chk_length; i ++) 
					{	
						objChk_matid1[i].checked = false;
					}
				}
				}
	}

	function fnChkStat(obj, matStat)
	{
		if (matStat == 'Included')
		{
			Error_Details('Record already Pushed In.');
			obj.checked=false;
		}

		if (ErrorCount > 0)
		 {
			Error_Show();
			Error_Clear();
			return false;
		}

	}

	
