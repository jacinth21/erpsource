function fnEdtSeq(seqId)
{
	document.frmEtrSesnMatMap.seqNo.value = seqId;
	document.frmEtrSesnMatMap.strOpt.value = 'edit';
	document.frmEtrSesnMatMap.submit();
}

function fnSubmit()
{
	document.frmEtrSesnMatMap.strOpt.value = 'save';
	document.frmEtrSesnMatMap.submit();
}

function fnVoidChk()
{
	var	strSeqNo=document.frmEtrSesnMatMap.seqNo.value;
	document.frmEtrSesnMatMap.voidBtn.disabled = true;
	if (strSeqNo != '')
	{
		document.frmEtrSesnMatMap.voidBtn.disabled = false;
	}

}