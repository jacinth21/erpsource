/*
function fnReset()
{
	document.frmEtrSessionSetup.sessionId.value = '';
	document.frmEtrSessionSetup.sessionDesc.value = '';
	document.frmEtrSessionSetup.sessionStartDt.value = '';
	document.frmEtrSessionSetup.sessionEndDt.value = '';
	document.frmEtrSessionSetup.sessionOwner.value = '[Choose One]';
	document.frmEtrSessionSetup.sessionLcnType.value= '[Choose One]';
	document.frmEtrSessionSetup.sessionLcn.value = '';
	document.frmEtrSessionSetup.duration.value = '';
	document.frmEtrSessionSetup.minUser.value = '';
	document.frmEtrSessionSetup.maxUser.value = '';
	document.frmEtrSessionSetup.status.value = '[Choose One]';		
}
*/
function fnMapMod()
{
	fnValidateTxtFld("sessionId"," Session ID ");
	if (ErrorCount > 0)
		 {
			Error_Show();
			Error_Clear();
			return false;
		}
	document.frmEtrSessionSetup.strOpt.value =  'load';
	document.frmEtrSessionSetup.action ="/gmETSesnModMap.do";
	document.frmEtrSessionSetup.submit();
}

function fnAddUser()
{
	fnValidateTxtFld("sessionId"," Session ID ");
	document.frmEtrSessionSetup.action ="/gmETSesnUserInvite.do";
	document.frmEtrSessionSetup.submit();
}

function fnSubmit()
{
	fnValidateDate('sessionStartDt','sessionEndDt','End Date');
	fnValidateTxtFld('sessionDesc',' Session Descrtiption ');
	fnValidateTxtFld('sessionStartDt',' Start Date ');
	DateValidate(document.frmEtrSessionSetup.sessionStartDt,message[1]);
	fnValidateDropDn('sessionOwner',' Session Owner ');
	fnValidateDropDn('sessionLcnType',' Type of Location ');
	fnValidateInteger('duration',' Session Duration ');
	fnValidateNumber('minUser',' Minimum User for Session ');
	fnValidateNumber('maxUser',' Maximum User for Session ');
	fnValidateDropDn('status',' Status of Session ');
	if(document.frmEtrSessionSetup.sessionId.value != "")
	{
		fnValidateTxtFld('txt_LogReason',' Comments ');
	}
	if (ErrorCount > 0)
		 {
			Error_Show();
			Error_Clear();
			return false;
		}
	document.frmEtrSessionSetup.strOpt.value =  'save'; //'save';
	document.frmEtrSessionSetup.submit();
}

function fnValidateDate(val1, val2, fld)
{	
	var startDt = eval("document.all."+val1);
	var endDt = eval("document.all."+val2);
	var startDtVal = startDt.value;
	var endDtVal = endDt.value;
	if (startDtVal > endDtVal)
	{
		Error_Details("<b>"+fld+"</b> cannot be less then start date. Please enter valid data");
	}	
}

function fnValidateInteger(val,fld)
{
	var blDisabled;
	var obj = eval("document.all."+val);
	var objval = TRIM(obj.value);
	var objRegExp  = /(^-?\d\d*\.\d*$)|(^-?\d\d*$)|(^-?\.\d\d*$)/;

	if(!objRegExp.test(objval))
	{	
		Error_Details("<b>"+fld+"</b> cannot be other than numeric. Please enter valid data");
	}
}

function fnValidateNumber(val,fld)
{
	var blDisabled;
	var obj = eval("document.all."+val);
	var objval = TRIM(obj.value);
	var objRegExp  =/(^-?\d\d*$)/;

	if(!objRegExp.test(objval))
	{	
		Error_Details("<b>"+fld+"</b> cannot be other than number. Please enter valid data");
	}
}

function fnSelLcn(objLcnType)
{	
	document.frmEtrSessionSetup.strOpt.value =  "";
	if(objLcnType.value == 60311)
	{	
		document.frmEtrSessionSetup.sessionLcn.value="";		
	}
	if(objLcnType.value == 60310)
	{
		document.frmEtrSessionSetup.strOpt.value =  'loadLcn';		
	}
	document.frmEtrSessionSetup.submit();
}

function fnVoid()
{
	fnValidateTxtFld("sessionId"," Session ID ");
		if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
		document.frmEtrSessionSetup.action ="/GmCommonCancelServlet";
		document.frmEtrSessionSetup.hTxnId.value = document.frmEtrSessionSetup.sessionId.value;
		document.frmEtrSessionSetup.hTxnName.value = document.frmEtrSessionSetup.sessionDesc.value;
		document.frmEtrSessionSetup.submit();
}

function fnStrMatch(strVal)
{
	var match = /http:/i.test(strVal)
	if(!match)
	{
		Error_Details("Enter a valid URL for online training.");
		document.frmEtrSessionSetup.sessionLcn.value="";
	}
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
}
