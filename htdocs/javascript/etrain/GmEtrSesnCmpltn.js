function fnSubmit()
{
	var chk_length =  document.frmEtrSesnCmpltn.ModEndDate.length;
	var inputString = "";
	if(chk_length ==undefined)
	{
		var val = 1;
		objDate = eval("document.frmEtrSesnCmpltn.compltnDt"+val);		
		if (objDate.value!="")
		{
					inputString = inputString + document.frmEtrSesnCmpltn.ModEndDate.value + "^" +objDate.value+"|";
					if(objDate.value < document.frmEtrSesnCmpltn.sessionStartDt.value)
					{
						Error_Details("Completion date cannot be less then session start date.");
					}
		}		
	}
	for(var n=0;n<chk_length;n++)
	{
		var val = n+1;
		objDate = eval("document.frmEtrSesnCmpltn.compltnDt"+val);		
		if (objDate.value!="")
		{
					inputString = inputString + document.frmEtrSesnCmpltn.ModEndDate[n].value + "^" +objDate.value+"|";
					if(objDate.value < document.frmEtrSesnCmpltn.sessionStartDt.value)
						{							
							Error_Details("Completion date cannot be less then session start date.");
							break;
						}
		}			
		
	}
	if(inputString == "")
	    {
	      Error_Details("Please fill atleast one user record.");
	    }
		if (ErrorCount > 0)
		 {
			Error_Show();
			Error_Clear();
			return false;
		}
	document.frmEtrSesnCmpltn.hinputString.value = inputString;
	document.frmEtrSesnCmpltn.strOpt.value =  'save'; 
	document.frmEtrSesnCmpltn.submit();

}

function fnSelectDate(strDate)
{
	document.frmEtrSesnCmpltn.strTempDate.value = strDate;
}

function fnSelectAll()
{
	objSelAll = document.frmEtrSesnCmpltn.selectAll;
	if (objSelAll) 
	{
		var objChk_ModEndDate = document.frmEtrSesnCmpltn.ModEndDate;
		var chk_length =  objChk_ModEndDate.length;	
		var objDate;
		if (objSelAll.checked)
		{					
			if (chk_length == undefined && document.frmEtrSesnCmpltn.applyAllDt.value != "")
			{
				objDate = eval("document.frmEtrSesnCmpltn.compltnDt"+1);
				objDate.value = document.frmEtrSesnCmpltn.applyAllDt.value;
			}
			for(i = 0; i < chk_length; i ++) 
			{	
						
				var val = i+1;
				objDate = eval("document.frmEtrSesnCmpltn.compltnDt"+val);
				if (objDate.value=="")
				{
					objDate.value = document.frmEtrSesnCmpltn.applyAllDt.value;		
				}

			}
		}
		else 
		{										
			document.frmEtrSesnCmpltn.submit();
		}
	} 				
}

function fnManageUser()
{
	var strAction = 'load';
	var sesnId = document.frmEtrSesnCmpltn.sessionId.value;
	if(document.frmEtrSesnCmpltn.strSesnOwner.value == document.frmEtrSesnCmpltn.userId.value)
	{
		document.frmEtrSesnCmpltn.action ="/gmETSesnUserInvite.do?sessionId="+sesnId+"&strOpt="+strAction;
		document.frmEtrSesnCmpltn.submit();
	}
	else
		Error_Details("You are not authorised to do this operation.");
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}

}

