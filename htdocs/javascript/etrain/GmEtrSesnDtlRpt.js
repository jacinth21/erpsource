function fnLoad()
{
	document.frmEtrSesnDtlRpt.strOpt.value = 'load';
	document.frmEtrSesnDtlRpt.submit();
}
function fnSelect(setSesnId, setSesnOwner)
{	
	document.frmEtrSesnDtlRpt.strSessionId.value = setSesnId;
	document.frmEtrSesnDtlRpt.strSesnOwner.value = setSesnOwner;
}

function fnSubmit()
{	
	fnValidateDropDn('Cbo_Opt',' Action ');
	
	var strAction = document.frmEtrSesnDtlRpt.Cbo_Opt.value;//options[document.frmEtrSesnDtlRpt.Cbo_Opt.selectedIndex].value;
	var sesnId= document.frmEtrSesnDtlRpt.strSessionId.value;
	var sesnOwner = document.frmEtrSesnDtlRpt.strSesnOwner.value;
	var loggedUser = document.frmEtrSesnDtlRpt.userId.value;
	if(sesnId == "")
	{
		Error_Details("Select a session for respective action.");
	}
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	if(loggedUser == sesnOwner)
	{
		if(strAction == 'load')
		{		
			document.frmEtrSesnDtlRpt.action ="/gmETSessionSetup.do?sessionId="+sesnId+"&strOpt="+strAction;
			document.frmEtrSesnDtlRpt.submit();
		}
		else if(strAction == 'void')
		{
			fnVoid();
		}
		else if(strAction == 'close')
		{
			
			document.frmEtrSesnDtlRpt.action ="/gmETSesnCmpltn.do?strSesnOwner="+sesnOwner;
			document.frmEtrSesnDtlRpt.submit();
		}
		else if(strAction == 'manageUser')
		{
			strAction = 'load';
			document.frmEtrSesnDtlRpt.action ="/gmETSesnUserInvite.do?sessionId="+sesnId+"&strOpt="+strAction;
			document.frmEtrSesnDtlRpt.submit();
		}
		else if(strAction == "manageMod")
		{
			strAction = 'load';
			document.frmEtrSesnDtlRpt.action ="/gmETSesnModMap.do?sessionId="+sesnId+"&strOpt="+strAction;
			document.frmEtrSesnDtlRpt.submit();
		}
	}
	else
		Error_Details("You are not authorised to do this operation.");
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
}
function fnVoid()
{
		document.frmEtrSesnDtlRpt.action ="/GmCommonCancelServlet";
		document.frmEtrSesnDtlRpt.hTxnId.value = document.frmEtrSesnDtlRpt.strSessionId.value;
		document.frmEtrSesnDtlRpt.hTxnName.value = document.frmEtrSesnDtlRpt.strSessionId.value;
		document.frmEtrSesnDtlRpt.submit();
}

function fnViewCourse(val){
	document.frmEtrSesnDtlRpt.strSessionId.value = val;
	document.frmEtrSesnDtlRpt.strOpt.value ="load";
	document.frmEtrSesnDtlRpt.action ="/gmETCourseDtlRpt.do";  
 	document.frmEtrSesnDtlRpt.submit();
}

function fnViewModule(val){
	document.frmEtrSesnDtlRpt.strSessionId.value = val;
	document.frmEtrSesnDtlRpt.strOpt.value ="load";
	document.frmEtrSesnDtlRpt.action ="/gmETModuleDtlRpt.do";  
 	document.frmEtrSesnDtlRpt.submit();
}
function fnViewUser(val){
	document.frmEtrSesnDtlRpt.strSessionId.value = val;
	document.frmEtrSesnDtlRpt.strOpt.value ="load";
	document.frmEtrSesnDtlRpt.action ="/gmETUserDtlRpt.do";  
 	document.frmEtrSesnDtlRpt.submit();
}
function fnPrintRecord(val1, val2){
	windowOpener("/GmPageControllerServlet?strPgToLoad=gmETSesnDtlRpt.do&strSessionId="+val1+"&strSessionStartDt="+val2+"&strOpt=printrecords","PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=800,height=550,status=1");
}