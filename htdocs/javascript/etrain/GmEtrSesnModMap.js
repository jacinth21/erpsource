//function to call modlookup jsp
function fnLookup()
{
 windowOpener("/GmPageControllerServlet?strPgToLoad=gmETModLookup.do","PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=775,height=500,status=1");
}

function fnEdtSessMap(strSesnModSeq)
{	
	document.frmEtrSesnModMap.sessModSeq.value = strSesnModSeq;	
	document.frmEtrSesnModMap.voidBtn.disabled = false;
	document.frmEtrSesnModMap.strOpt.value = 'load';
	document.frmEtrSesnModMap.submit();
}
/*
function fnReset()
{
	document.frmEtrSesnModMap.moduleId.value = '[Choose One]';	
	document.frmEtrSesnModMap.reason.value = '[Choose One]';
	document.frmEtrSesnModMap.trainerId.value = '[Choose One]';
	document.frmEtrSesnModMap.details.value = '';
	document.frmEtrSesnModMap.durationHrs.value = '';
	document.frmEtrSesnModMap.statusMap.value = '[Choose One]';
}
*/
function fnSubmit()
{		
	fnValidateDropDn('moduleId',' Module ID ');
	fnValidateDropDn('reason',' Reason ');
	fnValidateDropDn('trainerId',' Trainer ');
	fnValidateDropDn('statusMap',' Status ');
	fnValidateTxtFld('durationHrs',' Module Duration ');	
	if(document.frmEtrSesnModMap.alLength.value > 0)
	{
		var chk_length =  document.frmEtrSesnModMap.modId2.length;
		if(chk_length==undefined)
		{
			if(document.frmEtrSesnModMap.modId2.value==document.frmEtrSesnModMap.moduleId.value)
			{
				Error_Details("Module Name already exist.");				
			}
		}
		for(var n=0;n<chk_length;n++)
		{
			if((document.frmEtrSesnModMap.modId2[n].value==document.frmEtrSesnModMap.moduleId.value) && document.frmEtrSesnModMap.sessModSeq.value == "")
			{
				Error_Details("Module Name already exist.");
				break;
			}
		}
	}
	
	if (ErrorCount > 0)
		 {
			Error_Show();
			Error_Clear();
			return false;
		}	
	document.frmEtrSesnModMap.strOpt.value =  'save'; 
	document.frmEtrSesnModMap.moduleId.disabled = false;
	document.frmEtrSesnModMap.submit();
}

function fnVoid()
{
		document.frmEtrSesnModMap.action ="/GmCommonCancelServlet";	
		document.frmEtrSesnModMap.hTxnId.value = document.frmEtrSesnModMap.sessModSeq.value;				
		document.frmEtrSesnModMap.hTxnName.value = document.frmEtrSesnModMap.moduleId.value;				
		document.frmEtrSesnModMap.submit();		
}

function fnVoidChk()
{
	var	strSesnModSeq=document.frmEtrSesnModMap.sessModSeq.value;
	document.frmEtrSesnModMap.voidBtn.disabled = true;
	document.frmEtrSesnModMap.moduleId.disabled = false;
	if (strSesnModSeq != '')
	{
		document.frmEtrSesnModMap.voidBtn.disabled = false;
		document.frmEtrSesnModMap.moduleId.disabled = true;
	}

}

function fnOpenMatForm(strSesnModSeq, strModId)
{	
	document.frmEtrSesnModMap.sessModSeq.value = strSesnModSeq;	
	strSessionId=document.frmEtrSesnModMap.sessionId.value;	
	windowOpener("/GmPageControllerServlet?strPgToLoad=gmETUserMatMap.do?sessionId="+strSessionId+"&moduleId="+strModId+"&flag=matList","PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=775,height=500,status=1");
	//document.frmEtrSesnModMap.submit();
}