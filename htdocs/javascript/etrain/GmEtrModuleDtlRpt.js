function fnVoid()
{
		document.frmEtrModuleDtlRpt.action ="/GmCommonCancelServlet";
		document.frmEtrModuleDtlRpt.hTxnId.value = document.frmEtrModuleDtlRpt.moduleId.value;
		document.frmEtrModuleDtlRpt.hTxnName.value = document.frmEtrModuleDtlRpt.moduleName.value;
		document.frmEtrModuleDtlRpt.strOpt.value = 'cancel';		
		document.frmEtrModuleDtlRpt.submit();
}
function fnLoad()
{
	document.frmEtrModuleDtlRpt.strOpt.value = 'load';
	document.frmEtrModuleDtlRpt.submit();
}
function fnSelect(setModuleId, setSesnOwner)
{	
	document.frmEtrModuleDtlRpt.moduleId.value = setModuleId;	
	document.frmEtrModuleDtlRpt.strOwner.value = setSesnOwner;
}
function fnSubmit()
{	
	fnValidateDropDn('Cbo_Opt',' Action ');
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	var strAction = document.frmEtrModuleDtlRpt.Cbo_Opt.value;
	var moduleId= document.frmEtrModuleDtlRpt.moduleId.value;
	var strOwner = document.frmEtrModuleDtlRpt.strOwner.value;
	var loggedUser = document.frmEtrModuleDtlRpt.userId.value;
	if(strOwner == loggedUser)
	{
		if(strAction == 'load')
		{		
		document.frmEtrModuleDtlRpt.strOpt.value = 'load';
		document.frmEtrModuleDtlRpt.action ="/gmETModuleSetup.do";
		document.frmEtrModuleDtlRpt.submit();
		}
		if(strAction == 'void')
		{
			fnVoid();
		}
	}
	else
		Error_Details("You are not authorised to do this operation.");
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
}
function fnViewSession(val){

 	document.frmEtrModuleDtlRpt.action ="/gmETSesnDtlRpt.do?moduleId="+val;  
 	document.frmEtrModuleDtlRpt.submit();
}

function fnViewUser(val){
	document.frmEtrModuleDtlRpt.strOpt.value ="load";
	document.frmEtrModuleDtlRpt.action ="/gmETUserDtlRpt.do?moduleId="+val;  
	document.frmEtrModuleDtlRpt.submit();
}
function fnViewCourse(val){
	document.frmEtrModuleDtlRpt.action ="/gmETCourseDtlRpt.do?moduleId="+val;  
 	document.frmEtrModuleDtlRpt.submit();
}