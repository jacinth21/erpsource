var val = '';
function fnVoid()
{
	if(document.frmEtrModuleSetup.moduleId.value == 0 || document.frmEtrModuleSetup.moduleId.value =="")
	{
			Error_Details("Select Module for Voiding.");
	}

		if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
		document.frmEtrModuleSetup.action="/GmCommonCancelServlet";
		document.frmEtrModuleSetup.hTxnId.value = document.frmEtrModuleSetup.moduleId.value;
		document.frmEtrModuleSetup.hTxnName.value = document.frmEtrModuleSetup.moduleName.value;
		document.frmEtrModuleSetup.submit();
}

function fnSubmit(){
	fnValidateTxtFld('moduleName',' Module Name');
	fnValidateTxtFld('description',' Module Description');
	fnValidateDropDn('moduleOwner',' Module Owner');
	fnValidateDropDn('courseId',' Program');
	fnValidateDropDn('moduleStatus',' Module Status');

	/*
	var moduleOwner = document.frmEtrModuleSetup.moduleOwner.value;
	var loggedUser = document.frmEtrModuleSetup.userId.value;
	var moduleStatus = document.frmEtrModuleSetup.moduleStatus.value;
	*/
	if(document.frmEtrModuleSetup.moduleId.value != "" && document.frmEtrModuleSetup.moduleId.value != 0)
	{
		fnValidateTxtFld('txt_LogReason',' Comments ');
	}
	/*
	if(moduleOwner != loggedUser && moduleStatus == 60291 )
	{
		Error_Details("You are not authorised to deactivate the module.");
	}
	*/
	if (ErrorCount > 0)
		 {
			Error_Show();
			Error_Clear();
			return false;
		}
document.frmEtrModuleSetup.strOpt.value ='save';
document.frmEtrModuleSetup.submit();
}

function fnLoad(val){

document.frmEtrModuleSetup.strOpt.value ='load';
//document.frmEtrModuleSetup.ModuleId.value = val;
document.frmEtrModuleSetup.submit();
}
function fnReset(){
	document.frmEtrModuleSetup.strOpt.value ='reset';
	document.frmEtrModuleSetup.submit();
}

function fnChngStatus()
{
	var oldStatusVal = document.frmEtrModuleSetup.moduleStatus.options[document.frmEtrModuleSetup.moduleStatus.selectedIndex].value;
	var newStatusVal = document.frmEtrModuleSetup.moduleStatus.options[1].value;
	var moduleOwner = document.frmEtrModuleSetup.moduleOwner.value;
	var loggedUser = document.frmEtrModuleSetup.userId.value;
	var moduleId = document.frmEtrModuleSetup.moduleId.value;

	document.frmEtrModuleSetup.moduleStatus.value =newStatusVal;
	if (oldStatusVal != newStatusVal && oldStatusVal!=0)
	{
		document.frmEtrModuleSetup.moduleStatus.value = oldStatusVal;
	}
	if(moduleOwner != loggedUser && moduleId != 0 )
	{
		document.frmEtrModuleSetup.moduleOwner.disabled = true;
		document.frmEtrModuleSetup.moduleStatus.disabled = true;
		document.frmEtrModuleSetup.moduleName.disabled = true;
		document.frmEtrModuleSetup.description.disabled = true;
		document.frmEtrModuleSetup.courseId.disabled = true;
	}
}