var varCbo_Opt='';

function fnEdtUsrDtl(usrid, usrnm)
{
	document.frmEtrSesnUserInvite.strOpt.value = 'load';
	document.frmEtrSesnUserInvite.action ="/gmETUserModMap.do?sesnUserId="+usrid+"&sesnUserNm="+usrnm;
	document.frmEtrSesnUserInvite.submit();
}
function fnPreview()
{
 	//varSessId = document.frmEtrSesnUserInvite.sessionId.value; 	
	var varAction = 'load';
	var chk_length =  document.frmEtrSesnUserInvite.Chk_invid.length;
	var inviteString = "";
	if (chk_length == undefined)
	{		
	 if(document.frmEtrSesnUserInvite.Chk_invid.checked) 
	   {      
	           inviteString = document.frmEtrSesnUserInvite.Chk_invid.id+"|";			   
		 }
		else
		{ 
		   Error_Details("Only user id having status 'Added' should be selected to send mail. ");
		}	 	
	}
	else
	{
		
	  for (i = 0; i < chk_length; i++) 
	  {			
			if (document.frmEtrSesnUserInvite.Chk_invid[i].checked) 
			{
			    inviteString += document.frmEtrSesnUserInvite.Chk_invid[i].id+ "|";				     
			}
	   }	   
	   if(inviteString == "")
	    {
	      Error_Details("Only user id having status 'Added' should be selected to send mail. ");
	    }	   
	} 
	if (ErrorCount > 0)
		 {
			Error_Show();
			Error_Clear();
			return false;
		}		
	document.frmEtrSesnUserInvite.hinviteList.value = inviteString;	
 	windowOpener("/GmPageControllerServlet?strPgToLoad=gmETUserMailInvite.do&hinviteList="+inviteString+"&strOpt="+varAction,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=785,height=500,status=1");
 }


function fnFetch()
{	
	fnReset();
	document.frmEtrSesnUserInvite.strOpt.value = 'edit';		
	document.frmEtrSesnUserInvite.submit();
}

function fnLoad()
{
	document.frmEtrSesnUserInvite.strOpt.value = 'loadUser';
	document.frmEtrSesnUserInvite.submit();
}
function fnSubmit()
{
	var chk_length =  document.frmEtrSesnUserInvite.Chk_tagid.length;
	var inputString = "";
	if (chk_length == undefined)
	{
	 if(document.frmEtrSesnUserInvite.Chk_tagid.checked)
		 {  	 			
	        inputString = document.frmEtrSesnUserInvite.Chk_tagid.id+"|";
			if(document.frmEtrSesnUserInvite.Chk_tagid.id==document.frmEtrSesnUserInvite.sessionOwner.value)
				{
					Error_Details("Session Owner cannot be same as trainee. ");					
				}
		 }
		else
		{ 
		   Error_Details("Please Select atleast one User id to send mail. ");
		}	 	
	}
	else
	{
	  for (i = 0; i < chk_length; i++) 
	  {		  
			if (document.frmEtrSesnUserInvite.Chk_tagid[i].checked ) 
			{
				inputString += document.frmEtrSesnUserInvite.Chk_tagid[i].id+ "|";				     
				if(document.frmEtrSesnUserInvite.Chk_tagid[i].id==document.frmEtrSesnUserInvite.sessionOwner.value)
				{
					Error_Details("Session Owner cannot be same as trainee. ");
					break;
				}			    
			}
	   }	   
	   if(inputString == "")
	    {
	      Error_Details("Please Select atleast one user record. ");
	    }	   
	} 
	
	if (ErrorCount > 0)
		 {
			Error_Show();
			Error_Clear();
			return false;
		}
	document.frmEtrSesnUserInvite.hinputString.value = inputString;
	document.frmEtrSesnUserInvite.strOpt.value =  'save'; 
	document.frmEtrSesnUserInvite.submit();
	
}

function fnSelectAll()
{
				objSelAll = document.frmEtrSesnUserInvite.selectAll;
				if (objSelAll) {
				var objChk_tagid = document.frmEtrSesnUserInvite.Chk_tagid;
				var chk_length =  objChk_tagid.length; 
				if (objSelAll.checked)
				{
					for(i = 0; i < chk_length; i ++) 
					{	
						objChk_tagid[i].checked = true;
					}
				}
				else {										
					for(i = 0; i < chk_length; i ++) 
					{	
						objChk_tagid[i].checked = false;
					}
				}
				}
	}

function fnInviteAll()
{
				objInvtAll = document.frmEtrSesnUserInvite.inviteAll;
				if (objInvtAll) {
				var objInvt_tagid = document.frmEtrSesnUserInvite.Chk_invid;
				var chk_length =  objInvt_tagid.length;			
				if (objInvtAll.checked)
				{
					for(i = 0; i < chk_length; i ++) 
					{	
						objInvt_tagid[i].checked = true;
					}
				}
				else {
										
					for(i = 0; i < chk_length; i ++) 
					{	
						objInvt_tagid[i].checked = false;
					}
				}
				}
	}

function fnVoid(userIdVal, userSeqVal)
{
		if(document.frmEtrSesnUserInvite.userId.value != document.frmEtrSesnUserInvite.sessionOwner.value)
		{
			Error_Details("You are not authorised to do this operation. ");	
			
		}
		if (ErrorCount > 0)
		 {
			Error_Show();
			Error_Clear();
			return false;
		}
		document.frmEtrSesnUserInvite.action ="/GmCommonCancelServlet?strSessUserId="+userIdVal;
		document.frmEtrSesnUserInvite.hTxnId.value = userSeqVal;
		document.frmEtrSesnUserInvite.hTxnName.value = 'User ID-'+ userIdVal;
		document.frmEtrSesnUserInvite.submit();
		
		
}

function fnChngUsrStatus(statusIdVal, userSeqVal, userIdVal)
{
	if(statusIdVal == 60332 || statusIdVal == 60333) //check for the status "completed" and "added"
	{
		Error_Details("Status Cannot be changed.");
	}
	if(document.frmEtrSesnUserInvite.userId.value != document.frmEtrSesnUserInvite.sessionOwner.value)
		{
			Error_Details("You are not authorised to do this operation. ");	
			
		}
	if (ErrorCount > 0)
		 {
			Error_Show();
			Error_Clear();
			return false;
		}

	document.frmEtrSesnUserInvite.statusId.value = statusIdVal;
	document.frmEtrSesnUserInvite.userSeq.value = userSeqVal;
	document.frmEtrSesnUserInvite.strUserId.value = userIdVal;
	document.frmEtrSesnUserInvite.strOpt.value =  'chngStat'; 
	document.frmEtrSesnUserInvite.submit();
}
