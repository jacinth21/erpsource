function fnVoid()
{
		document.frmEtrCourseDtlRpt.action ="/GmCommonCancelServlet";
	//	document.frmEtrCourseDtlRpt.action ="<%=strServletPath%>/GmCommonCancelServlet";
		document.frmEtrCourseDtlRpt.hTxnId.value = document.frmEtrCourseDtlRpt.courseId.value;
		document.frmEtrCourseDtlRpt.hTxnName.value = document.frmEtrCourseDtlRpt.courseName.value;
		document.frmEtrCourseDtlRpt.strOpt.value = 'cancel';
		document.frmEtrCourseDtlRpt.submit();
}
function fnLoad()
{
	document.frmEtrCourseDtlRpt.strOpt.value = 'load';
	document.frmEtrCourseDtlRpt.submit();
}
function fnSelect(setCourseId, setSesnOwner)
{	
	document.frmEtrCourseDtlRpt.courseId.value = setCourseId;
	document.frmEtrCourseDtlRpt.strOwner.value = setSesnOwner;
}

function fnSubmit()
{	
	fnValidateDropDn('Cbo_Opt',' Action ');	
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	var strAction = document.frmEtrCourseDtlRpt.Cbo_Opt.value;
	var courseId= document.frmEtrCourseDtlRpt.courseId.value;
	var strOwner = document.frmEtrCourseDtlRpt.strOwner.value;
	var loggedUser = document.frmEtrCourseDtlRpt.userId.value;
	if(strOwner == loggedUser)
	{
		if(strAction == 'load')
		{		
			document.frmEtrCourseDtlRpt.strOpt.value = 'load';	
			document.frmEtrCourseDtlRpt.action ="/gmETCourseSetup.do";
			document.frmEtrCourseDtlRpt.submit();
		}
		if(strAction == 'void')
		{
			fnVoid();
		}
	}
	else
		Error_Details("You are not authorised to do this operation.");
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	
}
function fnViewSession(val){
	 document.frmEtrCourseDtlRpt.strOpt.value ="load";
	 document.frmEtrCourseDtlRpt.action ="/gmETSesnDtlRpt.do?courseId="+val;  
	 document.frmEtrCourseDtlRpt.submit();
}
function fnViewUser(val){	
	document.frmEtrCourseDtlRpt.strOpt.value ="load";
	document.frmEtrCourseDtlRpt.action ="/gmETUserDtlRpt.do?courseId="+val;  
	document.frmEtrCourseDtlRpt.submit();
}


function fnViewModule(val){
	document.frmEtrCourseDtlRpt.strOpt.value ="load";
	document.frmEtrCourseDtlRpt.action ="/gmETModuleDtlRpt.do?courseId="+val;  
	document.frmEtrCourseDtlRpt.submit();
}