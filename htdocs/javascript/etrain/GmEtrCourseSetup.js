var val = '';

function fnVoid()
{	
	if(document.frmEtrCourseSetup.courseId.value == 0 || document.frmEtrCourseSetup.courseId.value=="")
	{
		Error_Details("Select Program for Voiding.");
	}
		if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
		document.frmEtrCourseSetup.action="/GmCommonCancelServlet";
		document.frmEtrCourseSetup.hTxnId.value = document.frmEtrCourseSetup.courseId.value;
		document.frmEtrCourseSetup.hTxnName.value = document.frmEtrCourseSetup.courseName.value;
		document.frmEtrCourseSetup.submit();
}

function fnSubmit(){
	fnValidateTxtFld('courseName',' Program Name');
	fnValidateTxtFld('description',' Program Description');
	fnValidateDropDn('courseOwner',' Program Owner');
	fnValidateDropDn('courseStatus',' Program Status');
	
	/*
	var courseOwner = document.frmEtrCourseSetup.courseOwner.value;
	var loggedUser = document.frmEtrCourseSetup.userId.value;
	var courseStatus = document.frmEtrCourseSetup.courseStatus.value;
	*/
	if(document.frmEtrCourseSetup.courseId.value != "" && document.frmEtrCourseSetup.courseId.value != 0)
	{
		fnValidateTxtFld('txt_LogReason',' Comments ');
	}
	/*
	if(courseOwner != loggedUser && courseStatus == 60281 )
	{
		Error_Details("You are not authorised to deactivate the program.");
	}*/
	if (ErrorCount > 0)
		 {
			Error_Show();
			Error_Clear();
			return false;
		}
document.frmEtrCourseSetup.strOpt.value ='save';
document.frmEtrCourseSetup.submit();
}

function fnLoad(val){

document.frmEtrCourseSetup.strOpt.value ='load';
document.frmEtrCourseSetup.submit();
}
function fnReset(){
	document.frmEtrCourseSetup.strOpt.value ='reset';
	document.frmEtrCourseSetup.submit();
}

function fnMapDept()
{
var vcouserId ;
var vcourseName;

	fnValidateTxtFld('courseId',' Program ID');
	if (document.frmEtrCourseSetup.courseId.value == 0)
	{
		Error_Details("No Program has been selected.");
	}
	if (ErrorCount > 0)
		 {
			Error_Show();
			Error_Clear();
			return false;
		}
		vcouserId = document.frmEtrCourseSetup.courseId.value;
		vcouserName = document.frmEtrCourseSetup.courseName.value;
	document.frmEtrCourseSetup.strOpt.value =  'load';
	document.frmEtrCourseSetup.action ="/gmETCourseDeptMap.do?courseMapId=" + vcouserId +"&courseName="+vcouserName;
	document.frmEtrCourseSetup.submit();
}

function fnChngStatus()
{
	var oldStatusVal = document.frmEtrCourseSetup.courseStatus.options[document.frmEtrCourseSetup.courseStatus.selectedIndex].value;
	var newStatusVal = document.frmEtrCourseSetup.courseStatus.options[1].value;
	var courseOwner = document.frmEtrCourseSetup.courseOwner.value;
	var loggedUser = document.frmEtrCourseSetup.userId.value;
	var courseId = document.frmEtrCourseSetup.courseId.value;
	document.frmEtrCourseSetup.courseStatus.value =newStatusVal ;

	if (oldStatusVal != newStatusVal && oldStatusVal!=0)
	{
		document.frmEtrCourseSetup.courseStatus.value = oldStatusVal;
	}

	if(courseOwner != loggedUser && courseId != 0)
	{
		document.frmEtrCourseSetup.courseName.disabled = true;
		document.frmEtrCourseSetup.description.disabled = true;
		document.frmEtrCourseSetup.courseOwner.disabled = true;
		document.frmEtrCourseSetup.courseStatus.disabled = true;
	}

}