function fnVoid()
{
	fnValidateDropDn('deptMapId',' Department ');
	fnValidateDropDn('mandFlType',' Type ');
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
		document.frmEtrCourseDeptMap.action ="/GmCommonCancelServlet";	
		document.frmEtrCourseDeptMap.hTxnId.value = document.frmEtrCourseDeptMap.courseMapSeq.value;				
		document.frmEtrCourseDeptMap.hTxnName.value = document.frmEtrCourseDeptMap.courseMapId.value+'-'+document.frmEtrCourseDeptMap.courseName.value;				
		document.frmEtrCourseDeptMap.strOpt.value = 'cancel';				
		document.frmEtrCourseDeptMap.submit();		
}
function fnVoidChk()
{
	var	strCourseSeq=document.frmEtrCourseDeptMap.courseMapSeq.value;
	document.frmEtrCourseDeptMap.voidBtn.disabled = true;
	document.frmEtrCourseDeptMap.deptMapId.disabled = false;
	if (strCourseSeq != '')
	{
		document.frmEtrCourseDeptMap.voidBtn.disabled = false;
		document.frmEtrCourseDeptMap.deptMapId.disabled = true;
	}

}

function fnEditCourseMap(strCourseSeq)
{
	document.frmEtrCourseDeptMap.courseMapSeq.value = strCourseSeq;	
	document.frmEtrCourseDeptMap.strOpt.value = 'loadCourseDtl';
	document.frmEtrCourseDeptMap.submit();
}
function fnSubmit()
{
	fnValidateDropDn('deptMapId',' Department ');
	fnValidateDropDn('mandFlType',' Type ');
	if (ErrorCount > 0)
		 {
			Error_Show();
			Error_Clear();
			return false;
		}
	document.frmEtrCourseDeptMap.deptMapId.disabled = false;
	document.frmEtrCourseDeptMap.courseMapSeq.value = "";
	document.frmEtrCourseDeptMap.strOpt.value =  'save'; 
	document.frmEtrCourseDeptMap.submit();
}