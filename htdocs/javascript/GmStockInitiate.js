//this function is used to save the stock transfer Initiate
function fnStockReqSubmit(){
	var str = document.getElementById("frmCart").contentWindow.fnCreateInputString();
	fnValidateDropDn('fulfillCompId',fulfillComplbl);
	fnValidateTxtFld('txt_LogReason', message[5550]);
	var reqDateObj = document.frmStockInitiate.requiredDate;
	// Past Date validation check for Required Date field
	var date_diff = dateDiff(todayDate, reqDateObj.value, dateFmt);
    if(date_diff <0){
		Error_Details(message[10539]);
	 }
	var cnt = 0;
	for (var i=0;i<PlantLength;i++)
	{
		
		arr = eval("PlantArray"+i);
		
		arrobj = arr.split(",");
		value = arrobj[0]; //Plantid
		name = arrobj[1]; 

		if(value == document.all.names.value){
			cnt++;
		}
		
	}	
	if(cnt != 1){
		Error_Details(message[10186]);
	}
	

	if(str == ''){
			Error_Details(message[5034]);
	}
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	
	var refids = "";
	var refNm = "";
		document.frmStockInitiate.hinputStr.value = str;
		document.frmStockInitiate.strOpt.value = 'save';
		document.frmStockInitiate.Btn_Submit.disabled = true;
		document.all.shipTo.disabled = false;
		fnStartProgress();
		document.frmStockInitiate.submit();

 
}

// This function is used for on page load
function fnOnPageLoad() {

	document.all.shipTo.value = '26240419';  // Plant
	fnGetNames(document.all.shipTo);
	document.all.shipTo.disabled = true;
	document.all.names.value = plantId;
	
} 