this.keyArray = new Array(); 
this.valArray = new Array();
this.put = put; 
this.get = get; 
this.findIt = findIt;


function put(key,val) 
{ 
    var elementIndex = this.findIt(key); 
    if( elementIndex == (-1) ) 
    { 
        this.keyArray.push( key ); 
        this.valArray.push( val ); 
    } 
    else 
    { 
        this.valArray[ elementIndex ] = val; 
    } 
} 
 
function get(key) 
{ 
    var result = null; 
    var elementIndex = this.findIt( key ); 
    if( elementIndex != (-1) ) 
    {    
        result = this.valArray[ elementIndex ];
    } 
    else {
    	result = "";
    }
    return result; 
}
function findIt(key) 
{ 
    var result = (-1); 
 
    for( var i = 0; i < this.keyArray.length; i++ ) 
    { 
        if( this.keyArray[ i ] == key ) 
        { 
            result = i; 
            break; 
        } 
    } 
    return result; 
} 

function fnEnter(e){
	 if (typeof e == 'undefined' && window.event) { e = window.event; }
     if (e.keyCode == 13)
     {
    	 fnRptReload();
     }
}
function fnRptReload()
{
	var dtFrom = document.frmStatusLog.startDate.value;
	var dtTo = document.frmStatusLog.endDate.value;
	var diff = dateDiff(dtFrom,dtTo,format);
	var startDtDiff = dateDiff(dtFrom,currentDate,format);
	var endDtDiff = dateDiff(dtTo,currentDate,format);
	
	if(dtFrom == '' || dtTo == ''){
		Error_Details(message[5125]);
	}else if (diff > ruleVal)
	    {
			Error_Details(message[5107]);
		}
	else{
		if(diff < 0){
			Error_Details(message[5103]);
		}
		if(startDtDiff < 0){
			Error_Details(message[5104]);
		}
		if(endDtDiff < 0){
			Error_Details(message[5105]);
		}
	}
	if (ErrorCount > 0)  
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	document.frmStatusLog.haction.value = "rptReload";
	fnStartProgress('Y');
	document.frmStatusLog.submit();
}
function fnPrintRef(val, val2){
var status = val2 ;

	if(status =='Returns'){
 	fnPrintRA(val);
 	}
	else if (status =='Consignments' || status == 'InhouseLoanerItems'){
 	windowOpener("/GmConsignItemServlet?hAction=PicSlip&hRemVoidFl=Y&hConsignId="+val,"SetPic","resizable=yes,scrollbars=yes,top=150,left=150,width=750,height=500");
 	}
 	else if (status =='Orders'){
 	windowOpener('/GmOrderItemServlet?hAction=PICSlip&hId='+val,"Pack","resizable=yes,scrollbars=yes,top=150,left=150,width=760,height=610");	
 	}
 	else if (status =='Loaners'){
	windowOpener("/GmInHouseSetServlet?hAction=ViewSet&hConsignId="+val,"Con1","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
 	}
 	else if (status =='LoanersExtn' || status == 'Allocation'){
 	windowOpener("/GmConsignInHouseServlet?hAction=PicSlip&hRemVoidFl=Y&hConsignId="+val,"SetPic","resizable=yes,scrollbars=yes,top=150,left=150,width=750,height=500");
 	}
 	else if (status =='MaterialRequest'){
 	alert('Material Request.');
 	}
}

function fnGo()
{
if(TRIM( eval(document.all.refID).value ) == ''){
	 Error_Details(message[5106]);
}	 
	if (ErrorCount > 0)  
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	document.frmStatusLog.haction.value = "reload";
	fnStartProgress('Y');
	document.frmStatusLog.submit();
}
function fnSave()
{
document.frmStatusLog.haction.value = "saveLocations";
	document.frmStatusLog.submit();
}

function fnTypes(val) { 
	document.all.source.options.length = 0;
	var len = get(val); 
	document.all.source.options[0] = new Option("[Choose One]",0);
	for (var i=0;i<len;i++)
	{
		arr = get(val+i);
		arrobj = arr.split(",");
		codeid = arrobj[0];
		codenm = arrobj[1];
		document.all.source.options[i+1] = new Option(codenm,codeid);
		if(selectID == codeid){
			document.all.source.options[i+1].selected = true;
		}
	}
	
}

function fnPageLoad(){ 
	var type = document.frmStatusLog.typeNum.value;
	if(type != 0){ 
		fnTypes(type);
	}
}
