var Cbo_AddIdNmarr = new Array();
var Cbo_AddIdIDarr = new Array();
var ajax = new Array();
var repid;
var accid;
var distid;
var chkName =false;
var addIdReturned;
var carrierCnt = 0;
var carrierFl;
var loadScreen;
var distId = '';
var vCarrier = '';

function fnGetAssocRepNamesTissue(obj,dist){
	var AssocRep = ""; 
	var shipToID = "";	
	
		if(document.all.Cbo_LoanToASSORep){
			AssocRep = document.all.Cbo_LoanToASSORep;
		}
	/*
	 * In the below if condition checking for Assoc Rep obj only if it is defined, we can get Assoc Rep values 
	 */
		
	if(AssocRep != undefined){
	var assocRepId = AssocRep.value;
	
	/*
	 * If there is value for Rep Id and if the ship to value is sales Rep ,we need to check the check box.
	 * We need display all sales Rep name in the Names Column.
	 * We need to fetch the Address for the Sales Rep.
	 */
	if(assocRepId != '' && assocRepId != undefined && assocRepId != '0' && document.all.tissueShipTo.value == '4121'){
		if(document.all.tissueChkNames!= undefined)
		 {
		document.all.tissueChkNames.checked = true;
		 }
		fnGetAllNamesTissue();
		
		if(document.all.tissueNames!= undefined)
		{
		document.all.tissueNames.value = assocRepId;
		
		}
		if(document.all.tissueNames.value == '')
	    	document.all.tissueNames.value= 0;
	}
	else if (document.all.tissueShipTo.value != '4120' || dist != 'init'){
		
		if(document.all.tissueChkNames!= undefined)
			{
		document.all.tissueChkNames.checked = false;
			}
		fnGetNamesTissue(obj);
	}
	}
	// Hiding and Showing the Third Paty Account Name fields based on the Ship To Values during OnChnage Event
	
	if(document.all.tissueNames!= undefined)
	 {

	shipToID = document.all.tissueNames.value ;	
	
	 }
	if(obj.value != '4122'){
		if(document.getElementById("thirdPartyField") != undefined && document.getElementById("thirdPartyValue") != undefined){			
			document.getElementById("partyAccountFiled").style.display = 'none';
			document.getElementById("partyAccount").style.display = 'none';			
		}
	}else{		
		if(document.getElementById("thirdPartyField") != undefined && document.getElementById("thirdPartyValue") != undefined){			
			document.getElementById("partyAccount").style.display = 'block';
			document.getElementById("partyAccountFiled").style.display = 'block';
			document.getElementById("partyAccount").innerHTML = '';
			var dhxUrl = fnAjaxURLAppendCmpInfo('/GmCartAjaxServlet?hAction=fetchThirdPatyAccount&hPartyId='+shipToID+'&ramdomId='+Math.random());
			dhtmlxAjax.get(dhxUrl,fnSetThirdPatyNameTissue);
		}
	}
	
	
}
function fnGetAllNamesTissue(){
		
		if(document.all.tissueShipTo.value == 4121 || document.all.tissueShipTo.value == 4122)
		fnGetNamesTissue(document.all.tissueShipTo);
		
}

function fnGetNamesTissue(obj)
{	
	if(document.all.tissueShipCarrier != undefined){
		var car = document.all.tissueShipCarrier.value;
		if(car.trim() != ''){
			carrierFl = 'Y';
			fnGetShipModesTissue(document.all.tissueShipCarrier,'');
		}
	}
	if(addid == 0)
		addid ='';
val = eval(obj.value);

//names = document.all.names;
//alert(document.all.screenType.value);
if(document.all.tissueChkNames!= undefined){
	chkName = document.all.tissueChkNames.checked;
	}
j=0;
if (val == 4120) //dist
{
	
	document.all.tissueNames.options.length = 0;
	document.all.tissueNames.options[0] = new Option("[Choose One]","0");
	for (var i=0;i<DistLen;i++)
	{
		arr = eval("DistArr"+i);
		arrobj = arr.split(",");
		value = arrobj[0];
		//nameValue = arrobj[1] instead of name = arrobj[1] changes in PC-3659 Edge Browser fixes for Cust Service Module
		nameValue = arrobj[1];
		document.all.tissueNames.options[i+1] = new Option(nameValue,value);		
	}		
	document.getElementById('tissueAddressId').disabled = true
}

if (val == 4122 && !chkName && distid!=null && distid != '') //Hospital
{ 
	document.all.tissueNames.options.length = 0;
	document.all.tissueNames.options[0] = new Option("[Choose One]","0");		
	for (var i=0;i<AccLen;i++)
	{
		arr = eval("AccArr"+i);
		arrobj = arr.split(",");
		id = arrobj[0];
		nameValue = arrobj[1];
		fsid = arrobj[2]; // distid
		if(distid == fsid){
			j++;
			document.all.tissueNames.options[j] = new Option(nameValue,id); 
		}
	}		
	document.getElementById('tissueAddressId').disabled = true
}else if(val == 4122)
{
	document.all.tissueNames.options.length = 0;
	document.all.tissueNames.options[0] = new Option("[Choose One]","0");	

	for (var i=0;i<AccLen;i++)
	{
		arr = eval("AccArr"+i);
		arrobj = arr.split(",");
		id = arrobj[0];
		nameValue = arrobj[1];
		document.all.tissueNames.options[i+1] = new Option(nameValue,id);
	}		
	document.getElementById('tissueAddressId').disabled = true
}

if (val == 4123) //Employee
{
	document.all.tissueNames.options.length = 0;
	document.all.tissueNames.options[0] = new Option("[Choose One]","0");	
	for (var i=0;i<EmpLen;i++)
	{
		arr = eval("EmpArr"+i);
		arrobj = arr.split(",");
		value = arrobj[0];
		nameValue = arrobj[1];
		id=arrobj[2]; //partyid	
		document.all.tissueNames.options[i+1] = new Option(nameValue,value);
		document.all.tissueNames.options[i+1].id=id;
	}	
	document.getElementById('tissueAddressId').disabled = true
}
if (val == 4121	&& !chkName && distid!=null && distid != '' ) //Sales Rep	
{
	document.all.tissueNames.options.length = 0;
	document.all.tissueNames.options[0] = new Option("[Choose One]","0");	
	for (var i=0;i<RepLen;i++)
	{
		arr = eval("RepArr"+i);
		arrobj = arr.split(",");
		value = arrobj[0]; //repid
		nameValue = arrobj[1]; 
		id=arrobj[2]; //partyid	
		fsid = arrobj[3]; // distid
		if(distid == fsid){
			j++;
			document.all.tissueNames.options[j]= new Option(nameValue,value);
			document.all.tissueNames.options[j].id=id; 
		}
	}	
	document.getElementById('tissueAddressId').disabled = false;
}else if(val == 4121){
	document.all.tissueNames.options.length = 0;
	document.all.tissueNames.options[0] = new Option("[Choose One]","0");	
	for (var i=0;i<RepLen;i++)
	{
		arr = eval("RepArr"+i);
		arrobj = arr.split(",");
		value = arrobj[0]; //repid
		nameValue = arrobj[1]; 
		id=arrobj[2]; //partyid	
			document.all.tissueNames.options[i+1]= new Option(nameValue,value);
			document.all.tissueNames.options[i+1].id=id; 
	}		
	document.getElementById('tissueAddressId').disabled = false;
}else if(val == 26240419) //Plant	
{
	document.all.tissueNames.options.length = 0;
	document.all.tissueNames.options[0] = new Option("[Choose One]","0");	
	for (var i=0;i<PlantLen;i++)
	{
		
		arr = eval("PlantArr"+i);
		
		arrobj = arr.split(",");
		value = arrobj[0]; //repid
		nameValue = arrobj[1]; 

			document.all.tissueNames.options[i]= new Option(nameValue,value);
		
	}		
	document.getElementById('tissueAddressId').disabled = true;
	
}else if(val == 107801) //Dealer	
{
	document.all.tissueNames.options.length = 0;
	document.all.tissueNames.options[0] = new Option("[Choose One]","0");	
	for (var i=0;i<DealerLen;i++)
	{
		
		arr = eval("dlrar"+i);
		
		arrobj = arr.split(",");
		value = arrobj[0]; 
		nameValue = arrobj[1]; 

			document.all.tissueNames.options[i]= new Option(nameValue,value);
		
	}		
	document.getElementById('tissueAddressId').disabled = false;
	
}
//4000642 shipping account
 if(val == 4000642){ 
	document.all.tissueNames.options.length = 0;
	document.all.tissueNames.options[0] = new Option("[Choose One]","0");	
	for (var i=0;i<ShipacctLen;i++)
	{
		arr = eval("ShipAcctArr"+i);
		arrobj = arr.split(",");
		value = arrobj[0];  
		nameValue = arrobj[1];  
		document.all.tissueNames.options[i+1]= new Option(nameValue,value); 
		document.all.tissueNames.options[i+1].id=value; 
	} 	
	document.getElementById('tissueAddressId').disabled = false;
}	
if(val == 4124 || val == 0 ){	
	document.all.tissueNames.options.length = 0;
	document.all.tissueNames.options[0] = new Option("[Choose One]","0");
}	
	document.getElementById('tissueAddressId').options.length = 0;

// below functions are called to reset the values back from db when names dropdown is changed
	
	if (document.all.screenType.value == "Order")
	{
		setValuesForOrderTissue(val);
	}
	if (document.all.screenType.value == "Case")
	{
		setValuesForCaseTissue(val);
		carrierCnt++; 
		if(carrierCnt == 1){
			carrierFl = 'N';
		}
	}
if (document.all.screenType.value == "Consign")
{		
	setValuesForConsignTissue(val);
	// PMT-4359 - Need to display the saved ship carrier what we mention at the time of Item Initiation in Modify Request Edit screen.
	// so we are using count flag and first time this is One. this time we are setting carrier fl as N, for not calling new function to display preferred carrier value.
	// so after loading the screen, ie we chnage the TO/Names/Address dropdowns, need to call new function to display preferred carrier value.
	// this Carrier Flag is going as 'Y' and fetch the appropriate Carrier values.
	carrierCnt++; 
	if(carrierCnt == 1){
		carrierFl = 'N';
	}
}
if (document.all.screenType.value == "Initiate")
{		
	setValuesForInitiateTissue(val);
}
if(document.all.screenType.value == "LoanerXtn")
{
	
	setValuesForLoanerXtnTissue(val)
}
if(document.all.screenType.value == "EditShip" || document.all.screenType.value == "Loaner" )
{
		setValuesForEditShipTissue(val);
}
// Account item initiate screen
if(document.all.screenType.value == "HospitalInitiate")
{
	if(val == 107801){
		setDealerShipValueTissue(val);
	}
}

	// PMT-4359 --Function to fetch and setting the default preferred carrier values for the Rep and Account.
	// for rest of the types , we are serring Rule Carrier values.
fnGetCarrierTissue(val,document.all.tissueNames.value,'',carrierFl);

}

//Function to display the Ship Mode values based on the Ship Carriers.
function fnGetShipModesTissue(obj,screen){
	var carrier = obj.value;	
	var accountid = '';
	if(document.all.Txt_AccId != undefined){
		accountid = document.all.Txt_AccId.value;
	}
	var shiModeObj = document.all.tissueShipMode;
	
	if(screen == 'PROCCLOSURE'){
		shiModeObj = document.all.Cbo_ShipMode;
		document.all.Cbo_ShipCarr.value = carrier;
		loadScreen = 'PROCCLOSURE';
	}
	var previousMode = shiModeObj.value;
	if(carrier == '0' && screen != 'PROCCLOSURE')
		document.all.tissueShipMode.value = '0';

 	if(carrier != '')
 		var dhxUrl = fnAjaxURLAppendCmpInfo('/GmCartAjaxServlet?hAction=fetchCarrierModes&hCarrier='+carrier+'&hAcctID='+accountid+'&ramdomId='+Math.random());
		dhtmlxAjax.get(dhxUrl,fnSetShipModeTissue);	
}
function fnSetShipModeTissue(loader){
	var responseAll   = loader.xmlDoc.responseXML;	
	var response = responseAll.getElementsByTagName("data")[0];	
	var defaultMode = parseXmlNode(responseAll.getElementsByTagName("mode")[0].firstChild);
	var shiModeObj = (loadScreen == 'PROCCLOSURE') ? document.all.Cbo_ShipMode : document.all.tissueShipMode;	
	var codelist   = response.getElementsByTagName("pnum");
	
	if(codelist.length > 0){
		shiModeObj.options.length = 0;
		shiModeObj.options[0] = new Option("[Choose One]","0");
		for (var x=0; x<codelist.length; x++){
		   	strCodeID = parseXmlNode(codelist[x].childNodes[0].firstChild);
		   	strCodeNm = parseXmlNode(codelist[x].childNodes[1].firstChild); 
		   	shiModeObj.options[x+1] = new Option(strCodeNm,strCodeID);
		}
		if(defaultMode == ''){
			shiModeObj.value = '0';	
		}else{
			shiModeObj.value = defaultMode;	
		}
	}else{
		for (var i=0;i<shipModeLength;i++){
			arr = eval('AllModeArr'+i);
			arrobj = arr.split(",");
			id = arrobj[0];
			nameValue = arrobj[1];
			shiModeObj.options[i+1] = new Option(nameValue,id);
		}
		shiModeObj.value = '0';
	}
	var carrier = (loadScreen == 'PROCCLOSURE') ? document.all.Cbo_ShipCarr.value : document.all.tissueShipCarrier.value;	
	// Setting back the saved Ship Mode value for that particuler transaction on change of Saved Ship carrier	
	var previousMode = shiModeObj.value;	
	if(carrier == shipCarrier && shipmode != ''){		
		shiModeObj.value = shipmode;
		var count = 0;
		for(var x=0; x<codelist.length; x++){
		   	strCodeID = parseXmlNode(codelist[x].childNodes[0].firstChild);		   
		   	if(strCodeID != shipmode) count++;		   	
		}		
		// Setting Choose One if the transaction having SHip Mode which is not available in corresponding Ship carrier List.
		if(count == codelist.length && shiModeObj.value == '')
			shiModeObj.value = '0';
	}
	fnCalcShipChargeTissue();
}

function setValuesForLoanerXtnTissue(val){
	
	   if (val == 4122 && accid !="") {//Hospital
			document.all.tissueNames.value =accid;
			if(document.all.tissueNames.value == '')
		    	document.all.tissueNames.value= 0;
			document.all.tissueNames.disabled = false;
		}
		else if (val == 4120 && distid !="") { //dist	
			document.all.tissueNames.value = distid;
			document.all.tissueNames.disabled = true;
		}
		else if (val == 4121 && repid != null && repid !=""){
			var AssocRep = ""; 
			if(document.all.Cbo_LoanToASSORep){
				AssocRep = document.all.Cbo_LoanToASSORep;
				var assocRepId = AssocRep.value;
				document.all.tissueNames.value=assocRepId;
			}else{
			document.all.tissueNames.value=repid;
			}
			if(document.all.tissueNames.value == '')
		    	document.all.tissueNames.value= 0;
			fnGetAddressListTissue(document.all.tissueNames,'');
			document.all.tissueNames.disabled = false;
		}
		else {
				document.all.tissueNames.disabled = false;
		}
	}
function setValuesForConsignTissue(val){	
	if (document.all.tissueShipTo.value == '4120' ){
		var obj  = document.all.Cbo_BillTo;		
		if (obj == null)
		{
			var obj  = document.all.reqTo;
		}
		if (obj.value != '01')
		{	
			document.all.tissueNames.disabled = true		
			distId = obj.options[obj.selectedIndex].value;
			document.all.tissueNames.value=distId;
		}
	}
	else {
		document.all.tissueNames.disabled = false
	}	
	if (val == 4121 && tissueShipto == 4121 ) // sales rep
	{
		document.all.tissueNames.disabled = false;
		// PMT-4359 -- Here Y is passing as 3ed parameter to restore already saved Ship carrier from the parent screen.
		// if not, it will call new function to display preferred carrier value instead of saved ship carrier.
		fnGetAddressListTissue(document.all.tissueNames,addid,'Y');
	}
	else if (val == 4120 && tissueShipto == 4120 )//dist	
	{		
		document.all.tissueNames.value=tissueNamesid;
		document.all.tissueNames.disabled = true;
	}
	else if (val == 4122 && tissueShipto == 4122 ) // hospital
	{
		document.all.tissueNames.value=tissueNamesid;
	}
	else if (val == 4123 && tissueShipto == 4123 ) //employee
	{
		document.all.tissueNames.value=tissueNamesid;
	}
	else if (val == 4000642 && tissueShipto == 4000642 ) // 4000642:shipping account
	{
		document.all.tissueNames.value=repid;
		if(document.all.tissueNames.value == '')
	    	document.all.tissueNames.value= 0;
		document.all.tissueNames.disabled = false;

		fnGetAddressListTissue(document.all.tissueNames,addid,'Y');
	}
	
	if (val == 4122 && accid !="") {//Hospital
		document.all.tissueNames.value =accid;
		if(document.all.tissueNames.value == '')
	    	document.all.tissueNames.value= 0;
		document.all.tissueNames.disabled = false;
	}
	else if (val == 4120 && distid !="") { //dist	
		document.all.tissueNames.value = distid;
		if(document.all.tissueNames.value == ''){
			document.all.tissueNames.value= 0;
			document.all.tissueNames.disabled = false;
		}else{
			document.all.tissueNames.disabled = true;
		}	
	}
	else if (val == 4121 && repid != null && repid !=""){
		document.all.tissueNames.value=repid;
		if(document.all.tissueNames.value == '')
	    	document.all.tissueNames.value= 0;
		// PMT-4359 -- Here Y is passing as 3ed parameter to restore already saved Ship carrier from the parent screen.
		// if not, it will call new function to display preferred carrier value instead of saved ship carrier.
		fnGetAddressListTissue(document.all.tissueNames,addid,'Y');
		document.all.tissueNames.disabled = false;
	}
}

function fnGetAddressListTissue (sel,dbval,inActiveFl) {
	if(document.all.tissueShipCarrier != undefined){
		var car = document.all.tissueShipCarrier.value;
		if(car.trim() != ''){
			fnGetShipModesTissue(document.all.tissueShipCarrier,'');
		}
	}
	var AddCode = 0;
	var URL = '';
	// To avoid sel index comming -1.
	if(sel.selectedIndex!=-1)
		AddCode = sel.options[sel.selectedIndex].value;
	if(inActiveFl == undefined)
		inActiveFl='';
	document.getElementById('tissueAddressId').options.length = 0;
	var val = document.all.tissueShipTo.value;
	// PMT-4359 -- If we need to display already saved Carrier, then we should not call the fnGetCarrier() function.
	// for that, we have to add following condition.
	if(inActiveFl == ''){
		fnGetCarrierTissue(val,AddCode,'','');
	}
	// Enabling the below block of code as it is needed for the sales rep dropdown to re-load the Address
	// 4122 chnage done for showing the Third Party Account Name after Show All field only in Modify Shipping Detial Screen for Hospital
	
	if (val == 4121 || val == 4123 || val == 4000642 || (val == 4122 && screenName == 'EditShip') || val == 107801){ // sales rep or shipping account or Hospital or Dealer
		var index = ajax.length;		
		ajax[index] = new sack();	

		if (val == 4121){
			 URL = fnAjaxURLAppendCmpInfo('/GmCartAjaxServlet?hAction=fetchAdd&hRepId='+AddCode+"&hInActiveFl="+inActiveFl) ;
		}else if (val == 4123){
			URL = fnAjaxURLAppendCmpInfo('/GmCartAjaxServlet?hAction=fetchEmpAdd&hEmpId='+AddCode+"&hInActiveFl="+inActiveFl) ;
		}else if (val == 4000642 || val ==107801){//107801, dealer
			URL = fnAjaxURLAppendCmpInfo('/GmCartAjaxServlet?hAction=fetchShipAdd&hPartyId='+AddCode+"&hInActiveFl="+inActiveFl) ;
		}else if (val == 4122 && screenName == 'EditShip'){ 
			var dhxUrl = fnAjaxURLAppendCmpInfo('/GmCartAjaxServlet?hAction=fetchThirdPatyAccount&hPartyId='+sel.value+'&ramdomId='+Math.random());
			dhtmlxAjax.get(dhxUrl,fnSetThirdPatyName);
		}
		ajax[index].requestFile = URL ;	// Specifying which file to get				
		ajax[index].onCompletion = function(){ createAddressesTissue(index,dbval) };	// Specify function that will be executed after file has been found
		ajax[index].runAJAX();		// Execute AJAX function
		document.getElementById('tissueAddressId').disabled = false;
		if (document.all.tracknos)
		{
			if (document.frmShipDetails.trackingNo.value == document.frmShipDetails.tracknos.value)
			{
				//enabledisableFields(true);
				document.getElementById('tissueAddressId').disabled = true;
			}
		}
		if(sel.selectedIndex==-1)
			document.getElementById('tissueNames').options.value = 0;
		
	}
	else{
		document.getElementById('tissueAddressId').disabled = true;
	}
	
	
}
	function fnSetCarrTissue(vcarr){// To set the carrier if any default carrier is added for account
		vCarrier = vcarr;
	}
	function fnResetCarr(){// To reset the carrier
		vCarrier = '';
	}
function setValuesForInitiateTissue(val){	
	
	if(document.all.Cbo_LoanToRep){
		var objRep  = document.all.Cbo_LoanToRep;
		repid = objRep.options[objRep.selectedIndex].value;
	}
	if(document.all.Cbo_LoanToAcc){
		var objAcc  = document.all.Cbo_LoanToAcc;
		accid = objAcc.options[objAcc.selectedIndex].value;
	}
	
	 if (document.all.tissueShipTo.value == '4120' ){ // dist
	  var obj  = document.all.distributorId;
		if (obj.value != '01')
		{	
			document.all.tissueNames.disabled = true;		
			var distId = obj.options[obj.selectedIndex].value;
			document.all.tissueNames.value=distId;
		}
		
	}
	else if (document.all.tissueShipTo.value == '4121' ){// sales rep
		var AssocRep = ""; 
		if(document.all.Cbo_LoanToASSORep){
			AssocRep = document.all.Cbo_LoanToASSORep;
			var assocRepId = AssocRep.value;
		}
		if(assocRepId != '0'){
				document.all.tissueNames.value=assocRepId;
		}else{
			document.all.tissueNames.value=repid;
		}
		    
		    if(document.all.tissueNames.value == '')
		    	document.all.tissueNames.value= 0;
		    fnGetAddressListTissue(document.all.tissueNames,'');
			document.all.tissueNames.disabled = false;
	}
	else if (document.all.tissueShipTo.value == '4122' ){ // hospital
		
		    document.all.tissueNames.value=accid;
		    if(document.all.tissueNames.value == '')
		    	document.all.tissueNames.value= 0;
			document.all.tissueNames.disabled = false;
	}
	else if (document.all.tissueShipTo.value == '4123' || document.all.tissueShipTo.value == '4000642' ){ // employee or shipping account
			document.all.tissueNames.disabled = false;
	}else if(document.all.tissueShipTo.value == 107801){//dealer- Loaner initiate screen
	     setDealerShipValueTissue(val);
	}
}

//Function to set delaer ship to address
function setDealerShipValueTissue(val){
		document.all.tissueNames.value = dealerid;
		if(document.all.tissueNames.value == '')
	    	document.all.tissueNames.value= 0;
		fnGetAddressListTissue(document.all.tissueNames,'');
		document.all.tissueNames.disabled = false;
}

// PMT-4359 -- Function to fetch the Default Preferred Carrier for a Rep, Account
//for rest of the types , we are serring Rule Carrier values.
function fnGetCarrierTissue(shipto,repaccid,repaccaddrid,carrierFl){
	var accountid = '';
	if(document.all.Txt_AccId != undefined){
		accountid = document.all.Txt_AccId.value;
	}
	if(carrierFl != 'N'  && vCarrier == ''){// If the carrier is already set, no need to fetch it agian
		var dhxUrl = fnAjaxURLAppendCmpInfo('/GmCartAjaxServlet?hAction=fetchdefaultcarrier'+'&hAcctID='+accountid+'&hRepAccID='+repaccid+'&tissueShipTo='+shipto+'&hTissueAddrId='+repaccaddrid+'&ramdomId='+Math.random());
		dhtmlxAjax.get(dhxUrl,fnSetPrefCarrierTissue);
	}
}
//PMT-4359 -- Function to set the Default Preferred Carrier for a Rep, Account
//for rest of the types , we are serring Rule Carrier values.
function fnSetPrefCarrierTissue(loader){
	var response = TRIM(loader.xmlDoc.responseText);
	if(response){
		document.all.tissueShipCarrier.value = response;
		fnGetShipModesTissue(document.all.shipCarrier,'');
	}
}

function fnOpenAddressTissue()
{
	fnValidateDropDn('tissueShipTo',message[5563]);
	fnValidateDropDn('tissueNames',message[5564]);

	shipto = document.all.tissueShipTo.value;
	shiptoid = document.all.tissueNames.value;
	shiptoidid = document.all.tissueNames.options[document.all.tissueNames.selectedIndex].id;

	//if (shipto == '0')
	//{
	//	 Error_Details(message[62]);
	//}
	//if (shipto == '4121' && shiptoidid =='0')
	//{
//		 Error_Details(message[63]);
//	}
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	if ((shipto == '4121' && shiptoid !='0') || (scrtype == 'InhouseLoaner' && shipto == '4123' && shiptoid !='0'))
	{	
		windowOpener("/GmPageControllerServlet?strPgToLoad=gmAddressSetup.do&method=loadAddressSetup&includedPage=false&transactPage=true&partyId="+shiptoidid,"PO2","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=650");
		
	}
	else if ( shipto == '4000642' && shiptoid !='0')
	{	
		windowOpener("/GmPageControllerServlet?strPgToLoad=gmAddressSetup.do&method=loadAddressSetup&includedPage=false&transactPage=true&partyId="+shiptoid,"PO2","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=650");
		
	}
	else { // only pass the shipto and shiptoid	
		windowOpener("/GmSearchServlet?hAction=getAddress&hShip="+shipto+"&hShipId="+shiptoid+"&hScrType="+scrtype,"Address","resizable=yes,scrollbars=yes,top=340,left=420,width=300,height=135");
	}
}
function createAddressesTissue(index,dbval)
{
	var obj = document.getElementById('tissueAddressId');
	eval(ajax[index].response);	// Executing the response from Ajax as Javascript code	
  //  alert('addIdReturned '+document.all.addIdReturned.value);
	// PMT-288 if any address modified/added address dropdown will show choose one. 
	if(document.all.addIdReturned.value =='0' || addIdReturned =='0' || dbval == '0'){
		document.all.addIdReturned.value=0;
	}
	if (document.all.addIdReturned.value != '')
	{
			obj.value = document.all.addIdReturned.value;
			document.all.addIdReturned.value = '';
	}
	else if (dbval != '')
	{
		obj.value = dbval;
	}
	
	//document.all.hAddrId.value=obj.value;
}
function setValuesForEditShipTissue(val){
	var loanerTo = '';
	   if (val == 4122 && accid !="") {//Hospital
		   var accName;
		   if(document.all.screenType.value == "Loaner"){
		       // Code modified for Existing issue fix
			   if(document.all.Cbo_LoanTo != undefined){
				   loanerTo = document.all.Cbo_LoanTo.value;
			   }
			   if(loanerTo == '50171'){
				   obj = document.all.Cbo_ValuesCon;
			   		accName = obj.options[obj.selectedIndex].text;
			      if(accName!='' && !chkName){
			    	  document.all.tissueNames.options.length = 0;
			    	  document.all.tissueNames.options[0] = new Option(accName,accid);
			      }	  
			   }
		   	}
			document.all.tissueNames.value =accid;
			if(document.all.tissueNames.value == '')
		    	document.all.tissueNames.value= 0;
			document.all.tissueNames.disabled = false;
		}
		else if (val == 4120 && distid !="" && distid !=null) { //dist
			document.all.tissueNames.value = distid;
			//document.all.names.disabled = true;
		}
		else if (val == 4121 && repid != null && repid !=""){
			document.all.tissueNames.value=repid;
			if(document.all.tissueNames.value == '')
		    	document.all.tissueNames.value= 0;
			fnGetAddressList(document.all.tissueNames,addid);
			document.all.tissueNames.disabled = false;
		}
		else {
				document.all.tissueNames.disabled = false;
		}
}
function setValuesForCaseTissue(val){	

    if (val == 4122 && accid !="") {//Hospital
		document.all.tissueNames.value =accid;
		if(document.all.tissueNames.value == '')
	    	document.all.tissueNames.value= 0;
		document.all.tissueNames.disabled = false;
	}
	else if (val == 4120 && distid !="") { //dist	
		document.all.tissueNames.value = distid;
		document.all.tissueNames.disabled = true;
	}
	else if (val == 4121 && repid != null && repid !=""){
		document.all.tissueNames.value=repid;
		if(document.all.tissueNames.value == '')
	    	document.all.tissueNames.value= 0;
		// PMT-4359 -- Here Y is passing as 3ed parameter to restore already saved Ship carrier from the parent screen.
		// if not, it will call new function to display preferred carrier value instead of saved ship carrier.
		fnGetAddressListTissue(document.all.tissueNames,addid,'Y');
		document.all.tissueNames.disabled = false;
	}
	else {
			document.all.tissueNames.disabled = false;
	}
}
function setValuesForOrderTissue(val){	

if (val == 4122 && accid !="") {//Hospital
	document.all.tissueNames.value =accid;
	if(document.all.tissueNames.value == '')
    	document.all.tissueNames.value= 0;
	document.all.tissueNames.disabled = false;
}
else if (val == 4120 && distid !="") { //dist	
	document.all.tissueNames.value = distid;
	document.all.tissueNames.disabled = true;
}
else if (val == 4121 && repid != null && repid !=""){
	if(repid.charAt(0) == 0)
	     repid = repid.substring(1,repid.length);
	if(repid.charAt(0) == 0)
	     repid = repid.substring(1,repid.length);
	document.all.tissueNames.value=repid;
	if(document.all.tissueNames.value == '')
    	document.all.tissueNames.value= 0;
	fnGetAddressListTissue(document.all.tissueNames,'');
	document.all.tissueNames.disabled = false;
}else if(val == 107801){//dealer
	document.all.tissueNames.value = dealerid;
	if(document.all.tissueNames.value == '')
    	document.all.tissueNames.value= 0;
	fnGetAddressListTissue(document.all.tissueNames,'');
	document.all.tissueNames.disabled = false;
}
else {
		document.all.tissueNames.disabled = false;
}
}
//Function to display the Third Part Account name for the Selected Account fot Hospital besides the Show All Check box in Modify SHipping Details Screen only.
function fnSetThirdPatyNameTissue(loader){
	var responseAll   = loader.xmlDoc.responseXML;	
	var response = responseAll.getElementsByTagName("data")[0];	
	var thirdPartyName = parseXmlNode(responseAll.getElementsByTagName("thirdparty")[0].firstChild);
	var freightAmt  = parseXmlNode(responseAll.getElementsByTagName("freightamt")[0].firstChild);
	document.getElementById("partyAccount").innerHTML = thirdPartyName;
}
function fnGetAccCurrSymbTissue(accid){
	var dhxAccCurrUrl = fnAjaxURLAppendCmpInfo('/GmOrderAjaxServlet?opt=GETACCTCURR&acc='+accid+'&ramdomId=' + Math.random());
	dhtmlxAjax.get(dhxAccCurrUrl,function(loader){
		   var accCurrDtl;
		   var response = loader.xmlDoc.responseText; 
	          if((response != null || response != '') ){
	        	  accCurrDtl =response.split("|");
	        	  currencyFmt = accCurrDtl[1];
	        	  if(document.all.lblShipCharge)
	         			document.all.lblShipCharge.innerHTML = currencyFmt + '&nbsp;' + '0.00';
	          }
	   });
	}

function fnChangeAddressTissue(ObjAddr){
	document.all.hTissueAddrId.value = ObjAddr.value;
	var shipto = document.all.tissueShipTo.value;
	var repid = document.all.tissueNames.value;
	if(document.all.tissueShipCarrier != undefined){
		var car = document.all.tissueShipCarrier.value;
		if(car.trim() != ''){
			fnGetCarrier(shipto,repid,ObjAddr.value,'');
		}
	}
}
//To calculate the shipping charge
function fnCalcShipChargeTissue(){
	// The value will be 'Y' for BBA and not for US
	// For BBA we do not need to calculate the ship cost, whatever value is giving in textbox will be saved
	if(editChrgFl != 'Y'){
		var checkFl = false;
		var orderType = '';
		if(screenName != 'EditShip'){// No need to calculate the shipping charge while changing the values from modify shipping details screen
			if(document.all.Btn_PlaceOrd)
				document.all.Btn_PlaceOrd.disabled = true;// Should be enabled only after calculating the shipping cost
			if(document.all.Cbo_OrdType)
				orderType = document.all.Cbo_OrdType.value;// Get the order type, only for Bill & ship and Ack order the shipping charge will be calculated
			
	       var accid = (document.all.Txt_AccId)? document.all.Txt_AccId.value:'';
	       var shipTo = (document.all.tissueShipTo)? document.all.tissueShipTo.value:'';
	       var shipToId = (document.all.tissueNames)? document.all.tissueNames.value:'';
	       var addressId = (document.all.tissueAddressId)? document.all.tissueAddressId.value:'';
	       var shipCarrier = (document.all.tissueShipCarrier)? document.all.tissueShipCarrier.value:'';
	       var shipMode = (document.all.tissueShipMode)? document.all.tissueShipMode.value:'';
	       var creditHld = (document.all.hIsCreditHold) ? document.all.hIsCreditHold.value : '';
	       if (((shipTo != '' && shipTo != '0') && (shipToId != '' && shipToId != '0') || (addressId != '' && addressId != '0')) && (shipCarrier != '' && shipCarrier != '0') && (shipMode != '' && shipMode != '0')){
	       		checkFl = true;
	       }
	       fnGetAccCurrSymbTissue(accid);
	       // 2521: Bill & ship; 101260: Ack Order
	       if(checkFl && (orderType == '2521' || orderType == '101260')){
	    	   var dhxUrl = fnAjaxURLAppendCmpInfo('/gmIncShipDetails.do?strOpt=calcShpChrg&accID='+accid+'&names='+shipToId+'&shipTo='+shipTo +'&addressid='+addressId+'&shipCarrier='+shipCarrier+'&shipMode='+shipMode+ '&ramdomId=' + Math.random());
		       dhtmlxAjax.get(dhxUrl,function(loader){
		              var response = loader.xmlDoc.responseText; 
		              if((response != null || response != '') ){
		                     document.all.shipCharge.value = response;
		                     if(document.all.lblShipCharge){
		                           document.all.lblShipCharge.innerHTML = currencyFmt + '&nbsp;' +response;
		                     }
		                     //After shipping charge calculation the button should be enabled only if the swithUser flag is not 'Y' and the account is not credit hold
		                     if((document.all.Btn_PlaceOrd) && SwitchUserFl !='Y' && creditHld != "80131"){// 80131: No
		                     	document.all.Btn_PlaceOrd.disabled = false;
		                     }
		              }
		       });
	       
	       }else{
	       		document.all.shipCharge.value = '0.00';
	       		if(document.all.lblShipCharge)
	       			document.all.lblShipCharge.innerHTML = currencyFmt + '&nbsp;' + '0.00';
	    		//After shipping charge calculation the button should be enabled only if the swithUser flag is not 'Y' and the account is not credit hold
	       		if((document.all.Btn_PlaceOrd) && SwitchUserFl !='Y' && creditHld != "80131"){
	       			document.all.Btn_PlaceOrd.disabled = false;
	       		}
	       }
		}
	}else{
		document.all.Btn_PlaceOrd.disabled = false;
	}
}