//Global Variables Declaration

var includeFlag = false;
var eliminateFlag = false;

//This function is used to bring popup windows back to focus

function windowOpener(url, name, args)
{
	//One Portal Change
	var vDelimiter= (url.indexOf("?")>0)?"&":"?";
	  //Getting company info. as JSON string from GmEnterprisePortal.jsp and append to url.
	  
	//Commented and Added for the PMT-18670-Comments not displaying full timestamp from Pending PO Report - By Vendor popup screen 
	//url += vDelimiter + fnAppendCompanyInfo();
	
	if((url.indexOf("companyInfo="))<0){
		  url += vDelimiter + fnAppendCompanyInfo();
		}  

	 if( typeof GLOBAL_ClientSysType != 'undefined' && GLOBAL_ClientSysType == 'IPAD') {
		/* split_At  variable is declared in GmDhtmlxWindowPoup.js. 
		 * So,we check whether the file is include or not.
		 * If it is included then the popup will open as DHTMLX Popup
		 */
		if(typeof split_At != 'undefined'){
			w1 = createDhtmlxWindowFull(1,1,1025,700,true);
		    w1.setText(name); 
			w1.attachURL('/sales/DashBoard/GmIPADWindowPopup.jsp?' + url+ '&ramdomId=' + Math.random());
			return false;
		}
	 }
	popupWin = name;
	if (typeof(popupWin) != "object")
	{
	  popupWin = window.open(url,name,args);
	}
	else
	{
		if (!popupWin.closed)
		{
			popupWin.location.href = url;
		}
		else
		{
			popupWin = window.open(url, name,args);

		}
	}
	popupWin.focus();
}

/*function fnForgotPass(path)
{
   path = path + '/GmForgotPass.jsp';
   windowOpener(path, 'ForgotPass','WIDTH=320,HEIGHT=150 left=200 top=200 ,menubars=0 scrollbar=yes');
}

function fnChangePass(path)
{
   windowOpener(path+'/GmChangePass.jsp', 'ForgotPass','WIDTH=600,HEIGHT=250 left=300 top=300 ,resizable=yes, menubars=0 scrollbar=yes');
}*/

function fnForgotPass()
{
	document.fLogon.hAction.value = "Forgot";
	document.fLogon.action = "/GmLogonServlet";
	document.fLogon.submit();
}

function fnChangePass()
{
	 
	document.fLogon.hAction.value = "Change";
	document.fLogon.action = "/GmLogonServlet";
	document.fLogon.submit();
}

 

function changeBgColor(object,BgColor) {
   var color = BgColor;
   object.style.background = color;
}

function changeTRBgColor(val,BgColor) {
   var obj = eval("document.all.tr"+val);
   obj.style.background = BgColor;
}

function fnCallLink(obj)
{
	parent.RightFrame.location.href=obj;
}

var bw=new lib_bwcheck();

function lib_bwcheck(){ //Browsercheck (needed)
this.ver=navigator.appVersion
this.agent=navigator.userAgent
this.dom=document.getElementById?1:0
this.opera5=this.agent.indexOf("Opera 5")>-1
this.ie5=(this.ver.indexOf("MSIE 5")>-1 && this.dom && !this.opera5)?1:0; 
this.ie6=(this.ver.indexOf("MSIE 6")>-1 && this.dom && !this.opera5)?1:0;
this.ie4=(document.all && !this.dom && !this.opera5)?1:0;
this.ie=this.ie4||this.ie5||this.ie6
this.mac=this.agent.indexOf("Mac")>-1
this.ns6=(this.dom && parseInt(this.ver) >= 5) ?1:0; 
this.ns4=(document.layers && !this.dom)?1:0;
this.bw=(this.ie6 || this.ie5 || this.ie4 || this.ns4 || this.ns6 || this.opera5)
return this
}

function show(object){
  if(!bw.ns4){
    if(document.getElementById && document.getElementById(object) != null)
      node = document.getElementById(object).style.visibility='visible';
    else if(document.layers && document.layers[object] != null)
      document.layers[object].visibility = 'visible';
    else if(document.all)
      document.all[object].style.visibility = 'visible';
  }
}

function hide(object){
  if(document.getElementById && document.getElementById(object) != null)
    node = document.getElementById(object).style.visibility='hidden';
  else if(document.layers && document.layers[object] != null)
    document.layers[object].visibility = 'hidden';
  else if(document.all)
    document.all[object].style.visibility = 'hidden';
}

function closeOnTimeout(object){
  if(!bw.ns4)
    hide(object);
}

function isNumeric(ptxt_txt,msgval)
{
    if (isNaN(ptxt_txt))
    {
    	Error_Details(msgval);
		return false;
    }
    else
    {
    	return checkForPlus(ptxt_txt,msgval);
    }
}

function RemoveComma(word)
{
	var i ;
	var strNewValue ;

	strNewValue  = "";
  	for ( i = (word.length - 1) ; i >= 0 ; i-- )
  	{
		if (word.charAt(i) != ",")
		{
			strNewValue = word.charAt(i) + strNewValue;
		}
  	}
  	return strNewValue;
}

function TRIM(word)
{
	var strtrim = word.replace(/^\s*|\s*$/g,"");

	return strtrim;
}

function formatNumber(num) 
{
	num = num.toString().replace(/\$|\,/g,'');
	if(isNaN(num))
	{
		num = "0";
	}
	sign = (num == (num = Math.abs(num)));
	num = Math.floor(num*100+0.50000000001);
	cents = num%100;
	num = Math.floor(num/100).toString();
	if(cents<10)
	{
		cents = "0" + cents;
	}
	for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
		num = num.substring(0,num.length-(4*i+3))+','+
		num.substring(num.length-(4*i+3));

	return (((sign)?'':'-') +  num + '.' + cents);
}




// Use this function to validate dropdown options.
// Checks if the selected option is [Choose One]
// First Param is name of object; second is name of the field
function fnValidateDropDn(val,fld)
{
	var blDisabled;
	var obj = eval("document.all."+val);
	if(obj != undefined){
		var objval = obj.value;
	//	alert(obj.disabled);
		if (obj.disabled == true)
		{
			blDisabled = true;
		}
	//	alert(blDisabled);
		if (!blDisabled)
		{
			if (objval == '0' || objval == '') 
			{
				Error_Details("Please choose a valid option for the field:<b>"+fld+"</b>");	
			}
		}
	}else{
		obj = eval(val);
		objval = obj.getSelectedValue();
		if (obj._disabled == true)
		{
			blDisabled = true;
		}
		if (!blDisabled)
		{
			if (objval == 0 || objval == null) 
			{
				Error_Details("Please choose a valid option for the field:<b>"+fld+"</b>");	
			}
		}
	}
}


// Use this function to validate textfiled.
// First Param is name of object; second is name of the field
function fnValidateTxtFld(val,fld)
{
	var blDisabled;
	var obj = eval("document.all."+val);
	// Chrome is not support the textarea att. name. Here handle the issue
	if(obj == undefined){
		obj = document.getElementsByName(val);
		obj = obj[0];
	}
	var objval = TRIM(obj.value);
	if (obj.disabled == true)
	{
		blDisabled = true;
	}
	if (!blDisabled)
	{
		if(objval == '')
		{
			Error_Details("<b>"+fld+"</b> cannot be left blank. Please enter valid data");
		}
	}

}


/* http://www.alistapart.com/articles/zebratables/ */
function removeClassName (elem, className) {
	elem.className = elem.className.replace(className, "").trim();
}

function addCSSClass (elem, className) {
	removeClassName (elem, className);
	elem.className = (elem.className + " " + className).trim();
}

String.prototype.trim = function() {
	return this.replace( /^\s+|\s+$/, "" );
}
/** 
 * This function is used to download the piwik.js in to 
 * the page that this function is getting called. 
 * Usually, this function gets called along with fnTracker.
**/

function downloadPiwikJS()
{
/*	try
	{
		 var strLocation = window.location.toString().toLowerCase();		 
		 
		 if( strLocation.search("globusmedical.info") != -1 || strLocation.search("us.spineit.net") != -1 || strLocation.search("gmivmprod") != -1 )
		 {
			 var pkBaseURL = (("https:" == document.location.protocol) ? "https://gmivmtbox:90/piwik/" : "http://gmivmtbox:90/piwik/");
			 document.write(unescape("%3Cscript src='" + pkBaseURL + "piwik.js' type='text/javascript'%3E%3C/script%3E"));
		 }
		 
	} catch ( err ) {		
		/* Suppressing the exception because if the piwik server is down, 
		   we don't wan't to have javascript error on our page 
	}*/	
}

/** 
  * This function is used to track a page in the piwik server, 
  * call this function in a webpage that need to be tracked
 **/

function fnTracker(screen, userName)
{	
 /*	try {
		var strLocation = window.location.toString().toLowerCase();
		
		if( strLocation.search("globusmedical.info") != -1 || strLocation.search("us.spineit.net") != -1 || strLocation.search("gmivmprod") != -1)
		 {
			var pkBaseURL = (("https:" == document.location.protocol) ? "https://gmivmtbox:90/piwik/" : "http://gmivmtbox:90/piwik/");
			var piwikTracker = Piwik.getTracker(pkBaseURL + "piwik.php", 3);
			piwikTracker.setDocumentTitle(document.title);
			piwikTracker.setCustomVariable(1,screen +' - User Details', userName ,'page');
			piwikTracker.trackPageView();
			piwikTracker.enableLinkTracking();
		 }	
	} catch( err ) { 
		/* Suppressing the exception because if the piwik server is down, 
		   we don't wan't to have javascript error on our page. 
	}*/
}

function getInputString(x){
var Form = x;
var Elements = Form.getElementsByTagName("input");
var Element;
var returnString = "";
var appendArray = new Array();

	for (i = 0; i < Elements.length; i++){
	Element = Elements[i]; 			

			if(Element != null && Element.name.indexOf("seq") != -1 && Element.value != null && Element.value != "" ) {
		
			var name =  Element.name;
			var seqNo = parseInt(name.substr(name.length -1,name.length-1)); 

			    if(Element.name.indexOf("man") != -1) {
					if(Element.value == "zero") 
						{
							eliminateFlag = true;
					}
			         if(Element.value != null && Element.value != "undefined" && Element.value != "" && !eliminateFlag ) {			          
			           includeFlag = true;
			         }
			    }	
				if(appendArray[seqNo] != null && appendArray[seqNo] != "undefined") {
				      returnString += formString(appendArray);			
				      appendArray = new Array();
					  appendArray[seqNo] = Element.value;
				}
				else {
			
				  appendArray[seqNo] = Element.value; 
				}					
			}
			if(i == Elements.length-1 && Element.value != "zero" ) {
				returnString += formString(appendArray);
			}
		}
	
	return returnString;
}


function formString(appendArray) {
var appendedString ="";
var arrayLength = appendArray.length;
var delemiterChar="^";
if(includeFlag){
 for(k=0;k<arrayLength;k++){
		  if (k == (arrayLength -1)) {
		    delemiterChar = "|";
		  }
 		  
   appendedString += appendArray[k] + delemiterChar;  
   includeFlag = false;
 }
 }
 else if(!eliminateFlag) {
	 //This has to be generalised	  
   	  appendedString += appendArray[0] + "^0|";
	  /*****************///
 
 }
 eliminateFlag = false;
 return appendedString;
}


function fnHelp(wikiPath,wikiTitle){
	var wikiPage = wikiPath+"/"+wikiTitle;
	windowOpener(wikiPage,"Prntwiki","resizable=yes,scrollbars=yes,top=250,left=50,width=950,height=650");	
}

function fnNumbersOnly(e)
{	
var unicode=e.charCode? e.charCode : e.keyCode
if (unicode!=8){ //if the key isn't the backspace key (which we should allow)
if (unicode<48||unicode>57) //if not a number
return false //disable key press
}
}

function fnPriceOnly(e)
{	
var unicode=e.charCode? e.charCode : e.keyCode;
	if (unicode!=8){ //if the key isn't the backspace key (which we should allow)
		if (unicode == 43 || unicode == 45 || unicode == 46 ){//allows +,-,dot(.)
			return true;
		}else if (unicode<48||unicode>57){ //if not a number
			return false; //disable key press
		}
	}
}

function fnPrintRA(val)
{
	windowOpener('/GmPrintCreditServlet?hAction=PRINT&hRAID='+val,"Returns","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=850");
}

/* this method is called to include a js inside a js.
keeping this as a reference*/
function incJavascript(jsname) {
var th = document.getElementsByTagName('head')[0];
var s = document.createElement('script');
s.setAttribute('type','text/javascript');
s.setAttribute('src',jsname);
th.appendChild(s);
} 

incJavascript("/javascript/GmCommonValidation.js");

function fnCallClinicalLink(actionUrl)
{
	location.href=actionUrl;	
}

function fnShowDivTag(){
		 var tblHideComments = document.getElementById("tblHideComments");
		 var divShow = document.getElementById("divShow");
	       if(tblHideComments.style.display == 'block'){
		        tblHideComments.style.display = 'none';
		        divShow.innerHTML = "&nbsp;<a href=\"javascript:fnShowDivTag()\"><img alt=\"Click to view all comments\" border=\"0\" src=\"images/plus.gif\"/>&nbsp;"+message[10580]+"</a>";
	        }else{
	        	tblHideComments.style.display = 'block';
		        divShow.innerHTML = "&nbsp;<a href=\"javascript:fnShowDivTag()\"><img alt=\"Click to hide comments\" border=\"0\" src=\"images/minus.gif\"/>&nbsp;"+message[10581]+"</a>";
	        }
}
function formatCurrency(num,sessCurrency) {
	if(sessCurrency == null || sessCurrency == undefined){
		sessCurrency = '$';
	}
	num = num.toString().replace(/\$|\,/g,'');
	if(isNaN(num))
	num = "0";
	sign = (num == (num = Math.abs(num)));
	num = Math.floor(num*100+0.50000000001);
	cents = num%100;
	num = Math.floor(num/100).toString();
	if(cents<10)
	cents = "0" + cents;
	for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
	num = num.substring(0,num.length-(4*i+3))+','+
	num.substring(num.length-(4*i+3));
	return (((sign)?'':'-') + sessCurrency +' ' + num + '.' + cents);
	}
function checkMaxLength(obj,maxChars, field)
{
      if(TRIM(obj).length >= maxChars) {
         Error_Details('Please enter less than '+maxChars+' characters in '+field+' field.');
         Error_Show();
		 Error_Clear();
		 return false;
      }
}
function fnJulianLotValidation(julianDay ,julianRule){
	
	julianRule = parseInt(julianRule);
	if (julianDay < 0 || (julianDay > 366 && julianDay < julianRule+1) || (julianDay > (julianRule+ 366) && julianDay < (julianRule+ 400)) 
			|| julianDay > (julianRule+ 600) || isNaN(julianDay))
	{
		return true;
	}else{
		return false;
	}
}

/*Function is used for Entering Date in Different Formats such as (mmddyy,mmddyyyy), while tab out of the field it will display the date with separator*/
function fnDateFmtOnBlur(objDt,format) {
	   var val = objDt.value;
		var validformat='';
		var splitby='';
	      var yy;
	    var full_date=new Array();
	    //If format value is empty or null, it will set the default format as 'MM/dd/yyyy'
	    format=(format==null || format=='')?'MM/dd/yyyy':format;
	    if(val==''|| val==null)
	    	{
	    		return null;
	    	}
	    else{
	    	val= TRIM(objDt.value);
	    	
	    }
	    // This below expression for accepting 6 or 8 digit number or 2digits month-day/2digits month-day/2 or 4 digits year
	    // var regexp = /^((\d{6}$|\d{8}$)|(\d{2}\/\d{2}\/(\d{2}|\d{4})$))/;
	    // d{6}- 010114
	    // d{8}- 01012014
	    // d{2}\/\d{2}\/(\d{2}- 01/01/14
	    // d{2}\/\d{2}\/(\d{4}- 01/01/2014
		switch(format){
		case 'MM/dd/yyyy': 
			validformat=/^((\d{6}$|\d{8}$)|(\d{2}\/\d{2}\/(\d{2}|\d{4})$))/; 
			splitby="/";
			break;
		case 'dd.MM.yyyy': 
			validformat=/^((\d{6}$|\d{8}$)|(\d{2}\.\d{2}\.(\d{2}|\d{4})$))/; 
			splitby=".";
			break;
		case 'dd/MM/yyyy': 
			validformat=/^((\d{6}$|\d{8}$)|(\d{2}\/\d{2}\/(\d{2}|\d{4})$))/; 
			splitby="/";
			break;
		}
	    if(val.match(validformat)){ // Here Code checked the mentioned date formats above.

		    if (val.indexOf(splitby) != -1) {// If date comes with any symbol / or .
		        full_date = val.split(splitby);
		    } else {
		    	full_date[0] = val.substring(0, 2);
		    	full_date[1]= val.substring(2, 4);
		    	full_date[2]= val.substring(4);
	          }
	        yy = full_date[2];
	        yy = yy.length == 2 ? '20' + yy : yy;//If year 2 digit 
	        full_date[2] = yy;
	     objDt.value = full_date[0] + splitby + full_date[1] + splitby + full_date[2];
	    }else{
	    	 alert(message[10529]);
	    }
	}
// This function used to check the xml node and node present then return the values
function parseXmlNode(nodeObj){
	var returnVal = '';
	if(nodeObj != undefined){
		returnVal = nodeObj.nodeValue;
	}
	if(returnVal == null || returnVal ==''){
		return returnVal;
	}else{
		return returnVal;
	}
	  
}

 //This function used to get company info JSON from GmEnterprisePortal.jsp and set as form hidden fields
 //It will be calling in GmFooter.inc
 function fnAddCompanyParams(){
	 //after doucment ready then only this code will call to append hidden field for companyInfo.
	 $( document ).ready(function() {
		 $('<input>').attr('type','hidden').attr('name','companyInfo').attr('value',fnGetCompanyInfo()).appendTo('form'); 
		});
 }
 
 
 
 //This function will used to get companyInfo all places. It will easy to handle future change.
 function fnGetCompanyInfo(){
	 var vCompanyInfo= "";
	 var topObj = getObjTop();
	    //check JSON Object is declared before use it.
	 	if((typeof JSON == 'object') && !(topObj.gCompanyInfo == undefined || topObj.gCompanyInfo == null) ){
	 		vCompanyInfo =JSON.stringify(topObj.gCompanyInfo);
	 	}
		return vCompanyInfo;
}
 //This function will used to get top object.
 function getObjTop(){
	 // Login page (Forgot/Reset) there is no top (header) page - So, catch the execption and return the values.
	 try {
		 if(top.gCompanyInfo != undefined){
			 return top;
		 }else if(self.opener.top.gCompanyInfo != undefined){ 
			 //window opener
			 return self.opener.top;
		 }else if(parent.top.gCompanyInfo != undefined){
			 return parent.top;
		 }else{
			 return top;
		 }
	 }catch(err) {
		 return top;
	}
	 
 }
 
 //This function will used to get companyInfo all places. It will easy to handle future change.
 function fnAppendCompanyInfo(){
	 var vCompanyInfo = fnGetCompanyInfo();
	 var vCompanyInfoParam = (vCompanyInfo !=  "")?encodeURIComponent(fnGetCompanyInfo()):"";
	 return "companyInfo="+vCompanyInfoParam;
 }
 
 // This function will append company info in Location href.
 function fnLocationHref(strURL,targetObj){
		var vDelimiter= fnDelimiter(strURL);
		strURL += vDelimiter + fnAppendCompanyInfo(); 
		if(targetObj != null){
			targetObj.location.href = strURL;
		}else{
			location.href = strURL;
		}
}
 
 
 //This function will used to append companyInfo in ajaxtab href url
 function fnAjaxTabAppendCmpInfo(vMainTab){
		$(vMainTab+" a").each(function() {
			var url = this.href;
			  var vDelimiter= fnDelimiter(url);
			  url += vDelimiter + fnAppendCompanyInfo(); 
			  this.href = url;
		});
}
 //This function will used determine delimiter to append company info.
 function fnDelimiter(strUrl){
	 return (strUrl.indexOf("?")>0)?"&":"?";
 }
 //This function will used to append companyInfo in ajax url
 function fnAjaxURLAppendCmpInfo(strAjaxUrl){
	  var vDelimiter= fnDelimiter(strAjaxUrl);
	  strAjaxUrl += vDelimiter + fnAppendCompanyInfo();
	  return strAjaxUrl;
 }
 
//This function is used to get the Parameters by name from the given url.
 function getParameterByName(paramName, url) {
 	
 	paramName = paramName.replace(/[\[\]]/g, "\\$&");
     var regex = new RegExp("[?&]" + paramName + "(=([^&#]*)|&|#|$)"),
         results = regex.exec(url);
     if (!results) return null;
     if (!results[2]) return '';
     return decodeURIComponent(results[2].replace(/\+/g, " "));
 }
 
//This function is used to Hide the Company,Plant dropdown and country flag
 function fnToggleCompany(hideCompanyFl){
	 
	 if(hideCompanyFl == 'Y'){	
		 window.parent.TopFrame.document.getElementById("Company").style.display = 'none';
		 window.parent.TopFrame.document.getElementById("Plant").style.display = 'none'; 	
		 window.parent.TopFrame.document.getElementById("country_flag").style.display = 'none';
	 }else{
		 window.parent.TopFrame.document.getElementById("Company").style.display = 'block';
		 window.parent.TopFrame.document.getElementById("Plant").style.display = 'block';
		 window.parent.TopFrame.document.getElementById("country_flag").style.display = 'block';
	 }
 }
	 
 /*
  * This function used to Populate the Action Form infromation to Text Fields etc (Using AJAX)
  */

 function fnPopulateOptions(dropDownObj, strJSON, codeId, codeNm, optionId,
 		defaultId) {
 	var resJSONObj = strJSON;
 	var bFlag = false;
 	dropDownObj.options.length = 1;
 	for (i = 0, j = 1; i < resJSONObj.length; i++, j++) {
 		var itemJSON = resJSONObj[i];
 		dropDownObj.options[j] = new Option(itemJSON[codeNm], itemJSON[codeId]);
 		dropDownObj.options[j].id = itemJSON[optionId];
 		bFlag = (itemJSON[codeId] == defaultId) ? true : bFlag;
 	}
 	if (bFlag && defaultId != null && defaultId != "") {
 		dropDownObj.value = defaultId;
 	}
 }
 
 
 /*
  * This function used to set table alter color (oddshade or evenshade)
  */

 function fnSetTableOddEvenShade(tblId) {
		
		if (document.getElementsByTagName) {	
			var table = document.getElementById(tblId);
			var rows = table.getElementsByTagName("tr");		
			for (i = 0, j = 1; i < rows.length; i++) {
				// manipulate rows	
				if (rows[i].style.display!="none") {			
					if (j % 2 == 0) {
						rows[i].className = "oddshadetd";
					} else {
						rows[i].className = "evenshadetd";
					}
					j++;
				}
			}
		}
	}
 
 // common functions to save key & values in array
 this.keyMapArray = new Array(); 
 this.valMapArray = new Array();
 this.putMapValue = putMapValue; 
 this.getMapValue = getMapValue; 
 this.findItMapValue = findItMapValue;

// function to add key and value in array
 function putMapValue(key,val) 
 { 
     var elementIndex = this.findItMapValue(key); 
     if( elementIndex == (-1) ) 
     { 
         this.keyMapArray.push( key ); 
         this.valMapArray.push( val ); 
     } 
     else 
     { 
         this.valMapArray[ elementIndex ] = val; 
     } 
 } 
  
  // function to get value by key
 function getMapValue(key) 
 { 
     var result = null; 
     var elementIndex = this.findItMapValue( key ); 
     if( elementIndex != (-1) ) 
     {    
         result = this.valMapArray[ elementIndex ];
     } 
     else {
     	result = "";
     }
     return result; 
 }

 function findItMapValue(key) 
 { 
     var result = (-1); 
  
     for( var i = 0; i < this.keyMapArray.length; i++ ) 
     { 
         if( this.keyMapArray[ i ] == key ) 
         { 
             result = i; 
             break; 
         } 
     } 
     return result; 
 } 
 
 // Remove special characters[.,+,/,-,space] in PartNumber
 
 function replacePartSpecialchars(pnum)
 {
	  pnum = pnum.replace(/\./g,""); 
	  pnum = pnum.replace(/\s/g,"");
      pnum = pnum.replace(/\//g,"");
      pnum = pnum.replace(/\-/g,"");
      pnum = pnum.replace(/\+/g,""); 
      
      return pnum;
	 
 }