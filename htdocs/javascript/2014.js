var TREE_ITEMS = [
	['Home', 'GmOperDashBoardServlet\',\''],	
	['Dashboards', null,
		['Built Set', 'GmOperDashBoardServlet\',\'DashBoard'],
	],
	['Transactions', null,
		['Part Number Search', 'GmPartSearchServlet\',\''],		
		['Reprocess Material',null,
			['Repackaging', 'GmConsignItemServlet\',\'Repack'],
			['Shelf --> Quarantine', 'GmConsignItemServlet\',\'QuaranIn'],
			['Quarantine --> Scrap', 'GmConsignItemServlet\',\'QuaranOut'],	
			['Quarantine --> Shelf', 'GmConsignItemServlet\',\'InvenIn'],	
			['Shelf --> Bulk', 'GmConsignInHouseServlet\',\'BulkOut'],	
			['Bulk --> Shelf', 'GmConsignInHouseServlet\',\'BulkIn'],
		],
		['Loaners',null,
			['Manage Loaners', 'GmInHouseSetServlet\',\'4127'],
			['Inhouse Loaner - by Set', 'GmInHouseSetServlet\',\'4119'],
			['Usage-By Distributor', 'GmLoanerPartRepServlet\',\'LoanerByDist'],
			['Usage-By Set', 'GmLoanerPartRepServlet\',\'LoanerByDistBySet'],
			['Overall-By Status', 'GmLoanerPartRepServlet\',\'LoanerStatusBySet'],
			['Reconcilation',null,
				['Summary', 'GmLoanerPartRepServlet\',\''],
				['Transaction', 'gmLoanerReconciliation.do\',\''],
			],
		],
		['Process Request',null,
			['Set Initiate', 'gmSetInitiate.do\',\''],
			['Item Initiate', 'gmRequestInitiate.do\',\''],
			['Set', 'gmRequestMaster.do\',\''],
			['Item', 'gmRequestMaster.do\',\'part'],
			['Loaners', 'GmLoanerPartRepServlet\',\'LoanerRequests'],
			['Request Swap', 'gmRequestSwap.do\',\''],
        ],
		['Process Returns',null,
			['Initiate Returns', 'GmProcessConsRtnServlet\',\''],	
			['Receive Returns', 'GmReturnSummaryServlet\',\''],
		],  
		['Update DHR', 'GmPOReceiveServlet\',\'DHR'],
	],
	['Setup', null,
		['Create Rules', 'gmRuleCondition.do\',\''],
		['Manual Adjustment', 'gmManualAdjTxn.do\',\'Manual'],
		['Order Planning',null,
			['Group-Part Mapping', 'gmGroupPartMap.do\',\'40045'],
			['Demand Sheet setup', 'demandSheetSetup.do\',\''],
			['Group-Part Growth', 'gmGrowthSetup.do\',\''],
			['TTP Setup', 'gmTTPGroupMap.do\',\''],
			['TTP Finalizing', 'gmTTPMonthlyTrans.do\',\''],
			['Reports',null,
				['Monthly Sheet Summary', 'GmTTPMonthlySheetServlet\',\''],
				['Demand Sheet Summary', 'gmDSSummary.do\',\''],
				['Growth Report', 'GmGrowthReportServlet\',\''],
				['TTP Summary', 'gmTTPMonthlySummary.do\',\''],
				['Demand Report', 'gmDemandSheetReport.do?method=demandSheetReport\',\''],
				['TTP Report', 'gmTTPReport.do?method=demandTTPReport\',\''],
			],
	    ],
	],	
	['Reports', null,
		['MA Reports',null,
			['MA Summary Report', 'gmMASummary.do?method=reportMASummary\',\'']
		],
		['Transactions Log', 'GmTranLogReportServlet\',\'LoadNCMR'],
		['Part Fulfill Report', '/gmLogisticsReport.do?method=loadPartFulfillReport\',\''],
		['Trend', 'gmLogisticsReport.do?method=loadTrendReport\',\''],
		['Reprocess Material Report', 'GmReprocessMaterialServlet\',\'Report'],
		['Inhouse Summary List', 'GmSetSummaryReportServlet\',\''],
		['Inventory Reports',null,
			['Main Inventory', 'GmPartInvReportServlet\',\'Main'],
			['Locked Inventory', 'GmPartInvReportServlet\',\'LockInv'],
			['Built Sets',null,
				['By Sets', 'GmInvListReportServlet\',\'BuiltSet'],
				['By Parts', 'GmInvListReportServlet\',\'BuiltSetParts'],
			],
			['Quarantine Inventory', 'GmInvListReportServlet\',\'Quarantine'],
			['Rework Inventory', 'GmInvListReportServlet\',\'Rework'],
			['Returns Hold',null,
				['Inventory', 'GmInvListReportServlet\',\'ReturnsHold'],
				['By Type', 'GmInvListReportServlet\',\'Returns'],
				['By Part', 'GmInvListReportServlet\',\'ReturnsPart'],
			],
			['Loaners',null,
				['By Sets', 'GmInvListReportServlet\',\'Loaner'],
				['By Parts', 'GmInvListReportServlet\',\'LoanerPart'],
			],
			['InHouse',null,
				['By Sets', 'GmInvListReportServlet\',\'InHouse'],
				['By Parts', 'GmInvListReportServlet\',\'InHousePart'],
			],
			['Set Status', 'gmLogisticsReport.do?method=loadSetStatusReport\',\''],
			['Set Overview', 'gmLogisticsReport.do?method=loadSetOverview\',\''],
		],
		['Back Orders', null,
			['Sales', 'GmOrderItemServlet\',\'ProcBO'],
			['Items from Shipped Sets', 'GmOrderItemServlet?Cbo_Type=50262\',\'ProcBO'],
			['Items', 'GmOrderItemServlet?Cbo_Type=50263\',\'ProcBO'],
			['Sets', 'GmOrderItemServlet?Cbo_Type=50264\',\'ProcBO'],
			['Loaners', 'GmOrderItemServlet?Cbo_Type=50265\',\'ProcBO'],
		],
		['Cancellations',null,
			['View Voided Requests', 'GmCommonCancelServlet\',\'cancelreport@VDREQ'],
			['Rollback Received', 'GmCommonCancelServlet\',\'cancelreport@RBRTN'],
			['Rollback Credited', 'GmCommonCancelServlet\',\'cancelreport@CRBTN'],			
	   ],
		['Sets Transactions Report', 'GmSetTransReportServlet\',\''
	   ],
		['Status Log',null,
			['Detail', 'gmstatuslog.do\',\'DETAIL'],
			['Report', 'gmstatuslog.do\',\'REPORT'],
		],
		['Rule Report', 'gmRuleReport.do\',\''],
		['Voided Rules', 'GmCommonCancelServlet\',\'cancelreport@VRULE'],
		['Tag Report', 'gmTagReport.do\',\'Load'],
		['Vendor Info Report', 'gmOPVendorInfoReport.do\',\''],
		['Set Log Report', 'gmSetLogReport.do\',\''],
	]
		
];