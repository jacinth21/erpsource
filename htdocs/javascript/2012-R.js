var TREE_ITEMS = [	
	['Reports', null,
			['All Accounts Report', 'GmAccountReportServlet\',\'ALL'],
		['Cancel Report',null,
			['Rollback Set Consignment Report', 'GmCommonCancelServlet\',\'cancelreport@RBSCN'],
			['Void Item Consignment Report', 'GmCommonCancelServlet\',\'cancelreport@VODCN'],
		],
		['Part Number Transaction', 'GmPartNumSearchServlet\',\''],
		['Consignments Reports',null,
			['Sets', 'GmConsignmentReportServlet\',\'Sets'],
			['Items', 'GmConsignmentReportServlet\',\'Items'],
		],
		['Returns Reports',null,
			['Sets', 'GmReturnsReportServlet\',\'Sets'],
			['Items', 'GmReportCreditsServlet\',\'Items'],
		],
	]
];