

//code for common date validation
function CommonDateValidation(dateObj,format,msg)
{
	
	var returnval=false;
	var validformat='';
	var splitby='';
	var mon=0;
	var day=0;
	var year=0;
	var dhtmlxFl = 'N';
	//Add for PMT-16743 date format validation 
	var dateVal=dateObj.value;
	
	//To get date value from DHTMLX by using getValue() 
	if(dateVal == undefined){
		dateVal = dateObj.getValue();
		dhtmlxFl = 'Y';
	}
	if(dateVal == "" || dateVal==undefined)
		return true;
	
	switch(format){
	case 'MM/dd/yyyy': 
		validformat=/^\d{2}\/\d{2}\/\d{4}$/; 
		splitby="/";
		mon=0;
		day=1;
		year=2;
		break;
	case 'dd.MM.yyyy': 
		validformat=/^\d{2}\.\d{2}\.\d{4}$/; 
		splitby=".";
		mon=1;
		day=0;
		year=2;
		break;
	case 'dd/MM/yyyy': 
		validformat=/^\d{2}\/\d{2}\/\d{4}$/; 
		splitby="/";
		mon=1;
		day=0;
		year=2;
		break;
	case 'MM.dd.yyyy': 
		validformat=/^\d{2}\.\d{2}\.\d{4}$/; 
		splitby=".";
		mon=0;
		day=1;
		year=2;
		break;	
	case 'yyyy/MM/dd': 
		validformat=/^\d{4}\/\d{2}\/\d{2}$/;
		splitby="/";
		mon=1;
		day=2;
		year=0;
		break;
	}
	
		
	
	if (!validformat.test(dateVal))
	{
		Error_Details(msg);
	}
	else
	{
		var monthfield=dateVal.split(splitby)[mon];
		var dayfield=dateVal.split(splitby)[day];
		var yearfield=dateVal.split(splitby)[year];
		var intyearfield = parseInt(yearfield);
		if(intyearfield < 1900)
			Error_Details(msg);
		
		var dayobj = new Date(yearfield, monthfield-1, dayfield);
		if ((dayobj.getMonth()+1!=monthfield)||(dayobj.getDate()!=dayfield)||(dayobj.getFullYear()!=yearfield))
			Error_Details(msg);
		else
			returnval=true;
	}
	if (returnval==false)
	{
		
		if(dhtmlxFl == 'N'){
			dateObj.select();
		}
		return returnval;
	}
}

// code for date validation
function DateValidateMMDDYYYY(dateObj, msg)
{
	if(dateObj.value == "")
		return true;
	
	var validformat=/^\d{2}\/\d{2}\/\d{4}$/; //Basic check for format validity
	var returnval=false;
	if (!validformat.test(dateObj.value))
	{
		Error_Details(msg);
	}
	else
	{
		var monthfield=dateObj.value.split("/")[0];
		var dayfield=dateObj.value.split("/")[1];
		var yearfield=dateObj.value.split("/")[2];
		var intyearfield = parseInt(yearfield);
		if(intyearfield < 1900)
			Error_Details(msg);
		
		var dayobj = new Date(yearfield, monthfield-1, dayfield);
		if ((dayobj.getMonth()+1!=monthfield)||(dayobj.getDate()!=dayfield)||(dayobj.getFullYear()!=yearfield))
			Error_Details(msg);
		else
			returnval=true;
	}
	if (returnval==false)
	{
		dateObj.select();
		return returnval;
	}
}

function DateValidateMMDDYY(DtFldHndl, msgval)
{
	if(DtFldHndl == '')
	{
	}else{
            ret = new Boolean(true);
            fromdate = DtFldHndl;
			var c_Date = new String(DtFldHndl);
            //fromdate = trim(fromdate);
            if ((!(DtFldHndl.length >= 6)) || (!(DtFldHndl.length <= 10)))
            {
                        Error_Details(msgval);
                        ret = false;
                     
                        return ret;
            }
            else
            {
                        fromdate = new String(DtFldHndl);
                        if(DtFldHndl.length >= 6)
                        {
							if (!DtFldHndl.length == 8)
							{
								Error_Details(msgval);
								ret = false;
                     
								return ret;
							}
                                    var searchMe = '/';
                                    var firstIndex = c_Date.indexOf(searchMe);
 
                                    var lastIndex = c_Date.lastIndexOf(searchMe);
 
                                    var yyyy = new String(c_Date.substring(lastIndex+1,c_Date.length));
                                    if(yyyy.length == 2)
                                    {
                                                if (!isNaN(yyyy))
                                                {
                                                            var intYY = parseInt(yyyy);
                                                            if(intYY > 50)
                                                            {
                                                                        yyyy = '19'+ yyyy;
                                                            }
                                                            else if (intYY <= 50)
                                                            {
                                                                        yyyy = '20' + yyyy;
                                                            }
                                                }
                                                else
                                                {
                                                            Error_Details(msgval);
                                                            ret = false;
                                                           
                                                            return ret;
                                                }
 
                                    }else{  Error_Details(msgval);
                                                            ret = false;
                                                           
                                                            return ret;
									}
 
                                    if(firstIndex != -1)
                                    {
                                                var mm = new String(c_Date.substring(0,firstIndex));
                                                if(mm.length == 1)
                                                {
                                                            mm = '0' + mm ;
                                                }
                                    }
 
                                    if(lastIndex != -1)
                                    {
                                                var dd = new String(c_Date.substring(firstIndex+1,lastIndex));
                                                if(dd.length == 1)
                                                {
                                                            dd = '0' + dd ;
                                                }
                                    }
 
 
                                    var r_Date = mm + '/' + dd + '/' + yyyy ;
                                    fromdate = r_Date;
                        }
 
            var dateRegExpr = new RegExp("^\\d\\d?/\\d\\d?/\\d\\d\\d\\d$");
 
            fromdd=new String(fromdate.substring(3,5));
            frommm=new String(fromdate.substring(0,2));
            fromyy=new String(fromdate.substring(6,10));
 
            fdate=new Date(fromdate);
            var fyy=fromyy;
            var leap=0;
            if ((fyy % 4) ==0 )
            {
                        leap=1;
            }
            if(fromyy == '0000' || frommm == '00' || fromdd == '00')
            {
                         Error_Details(msgval);
                  
                     ret=false;
                                    
                                     return ret;
            }
            if (fromdate.search(dateRegExpr)<0)
            {
                       
                        Error_Details(msgval);
                        ret=false;
                      
                        return ret;
            }
            else if (frommm > '12')
            {
                     
                        Error_Details(msgval);
                        ret=false;
                        
                        return ret;
            }
            else if ( frommm =='04' || frommm == '06' || frommm== '09' || frommm == '11')
            {
                        if(fromdd > '30')
                        {
 
                                    Error_Details(msgval);
                                  
                                    ret=false;
                                   
                                    return ret;
                        }
            }
                        else if ( frommm == '02')
                {
                                                if( (leap ==0) && (fromdd > '28') )
                                    {
                                               
                                                            Error_Details(msgval);
                                                           
                                                            ret=false;
                                                            return ret;
                                                }
                                    else if( (leap ==1)  && (fromdd>'29') )
                                    {
 
                                                Error_Details(msgval);
                                                            
                                                ret=false;
                                                          
                                                            return ret;
                                                }
 
                                    }
                                    else
                                                {
                                                            if (fromdd >'31')
                                                            {
                                                                      
                                                                        Error_Details(msgval);
                                                                        ret=false;
                                                                       
                                                                        return ret;
                                                            }
                                                }
                                    }
                        DtFldHndl = fromdate;
                        return ret;
	}
            }

function DateValidate(DtFldHndl, msgval)
{	
	DtFldHndl = DtFldHndl.value;
	if(DtFldHndl == '')
	{
	}else{
            ret = new Boolean(true);
            fromdate = DtFldHndl;
			var c_Date = new String(DtFldHndl);
            //fromdate = trim(fromdate);
            if ((!(DtFldHndl.length >= 6)) || (!(DtFldHndl.length <= 10)))
            {
                        Error_Details(msgval);
                        ret = false;
                     
                        return ret;
            }
            else
            {
                        fromdate = new String(DtFldHndl);
                        if(DtFldHndl.length >= 6)
                        {
                                    var searchMe = '/';
                                    var firstIndex = c_Date.indexOf(searchMe);
 
                                    var lastIndex = c_Date.lastIndexOf(searchMe);
 
                                    var yyyy = new String(c_Date.substring(lastIndex+1,c_Date.length));
                                    if(yyyy.length < 4)
                                    {
                                                            Error_Details(msgval);
                                                            ret = false;
                                                           
                                                            return ret;
                                             
 
                                    }
 
                                    if(firstIndex != -1)
                                    {
                                                var mm = new String(c_Date.substring(0,firstIndex));
                                                if(mm.length == 1)
                                                {
                                                            mm = '0' + mm ;
                                                }
                                    }
 
                                    if(lastIndex != -1)
                                    {
                                                var dd = new String(c_Date.substring(firstIndex+1,lastIndex));
                                                if(dd.length == 1)
                                                {
                                                            dd = '0' + dd ;
                                                }
                                    }
 
 
                                    var r_Date = mm + '/' + dd + '/' + yyyy ;
                                    fromdate = r_Date;
                        }
 
            var dateRegExpr = new RegExp("^\\d\\d?/\\d\\d?/\\d\\d\\d\\d$");
 
            fromdd=new String(fromdate.substring(3,5));
            frommm=new String(fromdate.substring(0,2));
            fromyy=new String(fromdate.substring(6,10));
 
            fdate=new Date(fromdate);
            var fyy=fromyy;
            var leap=0;
            if ((fyy % 4) ==0 )
            {
                        leap=1;
            }
            if(fromyy == '0000' || frommm == '00' || fromdd == '00')
            {
                         Error_Details(msgval);
                  
                     ret=false;
                                    
                                     return ret;
            }
            if (fromdate.search(dateRegExpr)<0)
            {
                       
                        Error_Details(msgval);
                        ret=false;
                      
                        return ret;
            }
            else if (frommm > '12')
            {
                     
                        Error_Details(msgval);
                        ret=false;
                        
                        return ret;
            }
            else if ( frommm =='04' || frommm == '06' || frommm== '09' || frommm == '11')
            {
                        if(fromdd > '30')
                        {
 
                                    Error_Details(msgval);
                                  
                                    ret=false;
                                   
                                    return ret;
                        }
            }
                        else if ( frommm == '02')
                {
                                                if( (leap ==0) && (fromdd > '28') )
                                    {
                                               
                                                            Error_Details(msgval);
                                                           
                                                            ret=false;
                                                            return ret;
                                                }
                                    else if( (leap ==1)  && (fromdd>'29') )
                                    {
 
                                                Error_Details(msgval);
                                                            
                                                ret=false;
                                                          
                                                            return ret;
                                                }
 
                                    }
                                    else
                                                {
                                                            if (fromdd >'31')
                                                            {
                                                                      
                                                                        Error_Details(msgval);
                                                                        ret=false;
                                                                       
                                                                        return ret;
                                                            }
                                                }
                                    }
                        DtFldHndl = fromdate;
                        return ret;
	}
            }

			// code for date validation

function DateValidateMMYYYY(DtFldHndl, msgval)
{if(DtFldHndl == '')
	{
	}else{
            ret = new Boolean(true);
            fromdate = DtFldHndl;
            var c_Date = new String(DtFldHndl);
           
            if (!DtFldHndl.length == 7 || DtFldHndl.length < 7 || DtFldHndl.length > 7)
            { 
                        Error_Details(msgval);
                        ret = false;
                       
                      
            }
            else
            {
                        fromdate = new String(DtFldHndl);
                        if(DtFldHndl.length >= 6)
                        {
                                    var searchMe = '/';
                                    var firstIndex = c_Date.indexOf(searchMe);
 
                                    var lastIndex = c_Date.lastIndexOf(searchMe);
 
                                    var yyyy = new String(c_Date.substring(lastIndex+1,c_Date.length));
                                    if(yyyy.length == 2)
                                    {
                                                if (!isNaN(yyyy))
                                                {
                                                            var intYY = parseInt(yyyy);
                                                            if(intYY > 50)
                                                            {
                                                                        yyyy = '19'+ yyyy;
                                                            }
                                                            else if (intYY <= 50)
                                                            {
                                                                        yyyy = '20' + yyyy;
                                                            }
                                                }
                                                else
                                                {
                                                            Error_Details(msgval);
                                                            ret = false;
                                                           
                                                            return ret;
                                                }
 
                                    }
 
                                    if(firstIndex != -1)
                                    {
                                                var mm = new String(c_Date.substring(0,firstIndex));
                                                if(mm.length == 1)
                                                {
                                                            mm = '0' + mm ;
                                                }
                                    }
 
                                   
 
                                    var r_Date = mm + '/' + yyyy ;
                                    fromdate = r_Date;
                        }
 
            var dateRegExpr = new RegExp("^\\d\\d?/\\d\\d\\d\\d$");
 
           
            frommm=new String(fromdate.substring(0,2));
            fromyy=new String(fromdate.substring(3,7));
 
            fdate=new Date(fromdate);
            var fyy=fromyy;
            var leap=0;
            if ((fyy % 4) ==0 )
            {
                        leap=1;
            }
            if(fromyy == '0000' || frommm == '00')
            {
                         Error_Details(msgval);
                    
                     ret=false;
                                   
                                     return ret;
            }
            if (fromdate.search(dateRegExpr)<0)
            {
                        
                        Error_Details(msgval);
                        ret=false;
                       
                        return ret;
            }
            else if (frommm > '12')
            {
                        
                        Error_Details(msgval);
                        ret=false;
                        
                        return ret;
            }
              
                              
                        DtFldHndl = fromdate;
                       
       }    
	     return ret;
	}
}

function findAge(DtFldHndl, msgval)
{
   var dtAsOfDate;
   var dtBirth;
   var dtAnniversary;
   var intSpan;
   var intYears;
   var intMonths;
   var intWeeks;
   var intDays;
   var intHours;
   var intMinutes;
   var intSeconds;
   var strHowOld;
	var varAsOfDate=new Date();
	var varBirthDate=DtFldHndl;
   // get born date
   dtBirth = new Date(varBirthDate);
   
   // get as of date
   dtAsOfDate = new Date(varAsOfDate);

   // if as of date is on or after born date
   if ( dtAsOfDate >= dtBirth )
      {

      // get time span between as of time and birth time
      intSpan = ( dtAsOfDate.getUTCHours() * 3600000 +
                  dtAsOfDate.getUTCMinutes() * 60000 +
                  dtAsOfDate.getUTCSeconds() * 1000    ) -
                ( dtBirth.getUTCHours() * 3600000 +
                  dtBirth.getUTCMinutes() * 60000 +
                  dtBirth.getUTCSeconds() * 1000       )

      // start at as of date and look backwards for anniversary 

      // if as of day (date) is after birth day (date) or
      //    as of day (date) is birth day (date) and
      //    as of time is on or after birth time
      if ( dtAsOfDate.getUTCDate() > dtBirth.getUTCDate() ||
           ( dtAsOfDate.getUTCDate() == dtBirth.getUTCDate() && intSpan >= 0 ) )
         {

         // most recent day (date) anniversary is in as of month
         dtAnniversary = 
            new Date( Date.UTC( dtAsOfDate.getUTCFullYear(),
                                dtAsOfDate.getUTCMonth(),
                                dtBirth.getUTCDate(),
                                dtBirth.getUTCHours(),
                                dtBirth.getUTCMinutes(),
                                dtBirth.getUTCSeconds() ) );

         }

      // if as of day (date) is before birth day (date) or
      //    as of day (date) is birth day (date) and
      //    as of time is before birth time
      else
         {

         // most recent day (date) anniversary is in month before as of month
         dtAnniversary = 
            new Date( Date.UTC( dtAsOfDate.getUTCFullYear(),
                                dtAsOfDate.getUTCMonth() - 1,
                                dtBirth.getUTCDate(),
                                dtBirth.getUTCHours(),
                                dtBirth.getUTCMinutes(),
                                dtBirth.getUTCSeconds() ) );

         // get previous month
         intMonths = dtAsOfDate.getUTCMonth() - 1;
         if ( intMonths == -1 )
            intMonths = 11;

         // while month is not what it is supposed to be (it will be higher)
         while ( dtAnniversary.getUTCMonth() != intMonths )

            // move back one day
            dtAnniversary.setUTCDate( dtAnniversary.getUTCDate() - 1 );

         }

      // if anniversary month is on or after birth month
      if ( dtAnniversary.getUTCMonth() >= dtBirth.getUTCMonth() )
         {

         // months elapsed is anniversary month - birth month
         intMonths = dtAnniversary.getUTCMonth() - dtBirth.getUTCMonth();

         // years elapsed is anniversary year - birth year
         intYears = dtAnniversary.getUTCFullYear() - dtBirth.getUTCFullYear();

         }

      // if birth month is after anniversary month
      else
         {

         // months elapsed is months left in birth year + anniversary month
         intMonths = (11 - dtBirth.getUTCMonth()) + dtAnniversary.getUTCMonth() + 1;

         // years elapsed is year before anniversary year - birth year
         intYears = (dtAnniversary.getUTCFullYear() - 1) - dtBirth.getUTCFullYear();

         }

      // to calculate weeks, days, hours, minutes and seconds
      // we can take the difference from anniversary date and as of date

      // get time span between two dates in milliseconds
      intSpan = dtAsOfDate - dtAnniversary;

      // get number of weeks
      intWeeks = Math.floor(intSpan / 604800000);

      // subtract weeks from time span
      intSpan = intSpan - (intWeeks * 604800000);
      
      // get number of days
      intDays = Math.floor(intSpan / 86400000);

      // subtract days from time span
      intSpan = intSpan - (intDays * 86400000);

      // get number of hours
      intHours = Math.floor(intSpan / 3600000);
    
      // subtract hours from time span
      intSpan = intSpan - (intHours * 3600000);

      // get number of minutes
      intMinutes = Math.floor(intSpan / 60000);

      // subtract minutes from time span
      intSpan = intSpan - (intMinutes * 60000);

      // get number of seconds
      intSeconds = Math.floor(intSpan / 1000);

      // create output string     
      if ( intYears > 0 )
         if ( intYears > 1 )
            strHowOld = intYears.toString() + ' Years';
         else
            strHowOld = intYears.toString() + ' Year';
      else
         strHowOld = '';

      if ( intMonths > 0 )
         if ( intMonths > 1 )
            strHowOld = strHowOld + ' ' + intMonths.toString() + ' Months';
         else
            strHowOld = strHowOld + ' ' + intMonths.toString() + ' Month';
           
      if ( intWeeks > 0 )
         if ( intWeeks > 1 )
            strHowOld = strHowOld + ' ' + intWeeks.toString() + ' Weeks';
         else
            strHowOld = strHowOld + ' ' + intWeeks.toString() + ' Week';

      if ( intDays > 0 )
         if ( intDays > 1 )
            strHowOld = strHowOld + ' ' + intDays.toString() + ' Days';
         else
            strHowOld = strHowOld + ' ' + intDays.toString() + ' Day';

      if ( intHours > 0 )
         if ( intHours > 1 )
            strHowOld = strHowOld + ' ' + intHours.toString() + ' Hours';
         else
            strHowOld = strHowOld + ' ' + intHours.toString() + ' Hour';
 
      if ( intMinutes > 0 )
         if ( intMinutes > 1 )
            strHowOld = strHowOld + ' ' + intMinutes.toString() + ' Minutes';
         else
            strHowOld = strHowOld + ' ' + intMinutes.toString() + ' Minute';

      if ( intSeconds > 0 )
         if ( intSeconds > 1 )
            strHowOld = strHowOld + ' ' + intSeconds.toString() + ' Seconds';
         else
            strHowOld = strHowOld + ' ' + intSeconds.toString() + ' Second';

      }
   else
      intYears = 0

   // return string representation
   
return intYears+'.'+intMonths;
}

function BirthDateValidateInRange(DtFldHndl, msgval, range1, range2)
{ 
	if(DtFldHndl.value == '')
	{
	}else{
	ret = new Boolean(true);
	if(DateValidate(DtFldHndl, msgval))
	{
		var age=findAge(DtFldHndl.value, msgval);
		
		if (age<range1 || age>range2)
			{					
				Error_Details(msgval);
                ret = false;
				return ret;
			}
	}
	return ret;
}}

function BirthDateValidate(DtFldHndl, msgval, range)
{
	if(DtFldHndl.value == '')
	{
	}else{
	ret = new Boolean(true);

	if(DateValidate(DtFldHndl, msgval))
	{
		var age=findAge(DtFldHndl.value, msgval);
		
		if (age<range)
		{
			Error_Details(msgval);
            ret = false;
            return ret;
		}
	}
	return ret;
	}
}

function SurDateValidate(DtFldHndl, DtSur, msgval)
{
 ret = new Boolean(true);
	var varDtFldHndl=DtFldHndl;
	var varDtSur=DtSur;
   // get born date
   dtBirth = new Date(varDtSur);
   
   // get as of date
   dtAsOfDate = new Date(varDtFldHndl);

   // if as of date is on or after born date
   if ( dtAsOfDate >= dtBirth )
      {

	  }else{
						Error_Details(msgval);
                        ret = false;
                        DtFldHndl.select();
                        return ret;
		
	}
	return ret;
}


function ValidationNull(val, msgval)
{
	var ret = new Boolean(true);
    if(val==null){Error_Details(msgval);
						ret = false;
						return ret;}
    if (val.length==0){Error_Details(msgval);
						ret = false;
						return ret;}
return ret;
}

function NumberValidation(val, msgval, decimal)
{
	var ret = new Boolean(true);
	
		var charCount = 0;
		var chars2check="0123456789";
		var DecimalFound = false 
	    for (var i = 0; i < val.length; i++) 
			{
				var ch = val.charAt(i)
				if (i == 0 && ch == "-") 
					{
						continue
					}
				if (ch == "." && !DecimalFound) 
					{
						DecimalFound = true
						continue
					}
				if(DecimalFound && charCount<decimal && chars2check.indexOf(ch)!=-1)
					{
						charCount++
						continue
					}else if(!DecimalFound && chars2check.indexOf(ch)!=-1)
							{
								continue	 
							}
							Error_Details(msgval);
							ret = false;
							return ret;
	  
			} 
	
      return ret;
}

function AlphaNumWithDashValidation(val, msgval, chars2check, chara, charNum)
{
	// var chars2check="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	//charNum=2
	//chara='-'
   var ret = new Boolean(true);

  
		var charCount = 0;
	
		for (var i = 0; i < val.length; i++) 
			{
				var ch = val.charAt(i)
				
				if (ch == chara && charCount<charNum) {
				  charCount++;
                  continue
				} 
				if(chars2check.indexOf(ch)!=-1){
				 continue	 
				}
						Error_Details(msgval);
						ret = false;
						return ret;
	  
			} 
          
	
      return ret;
}

function Number2DigitValidation(val, msgval, charCount, chars2check)
{
   var ret = new Boolean(true);
	
	if (val.length>charCount){Error_Details(msgval);
						ret = false;
						return ret;}
     fnCheckString(val, msgval, chars2check);

      return ret;
}

function validateDecimals(val, msgval, decimal, chars2check)
{
	 var ret = new Boolean(true);

   
	var charCount = 0;
	//var chars2check="0123456789";
	//decimal=2
      var DecimalFound = false 
      for (var i = 0; i < val.length; i++) {
            var ch = val.charAt(i)
				
             if (ch == "." && !DecimalFound) {
                  DecimalFound = true
                  continue
            }
			if(DecimalFound && charCount<decimal && chars2check.indexOf(ch)!=-1)
		  {
			
			charCount++
			continue
		  }else	if(!DecimalFound && chars2check.indexOf(ch)!=-1){
				 continue	 
			}
			 Error_Details(msgval);
						ret = false;
						return ret;
	  
      } 
          

      return ret;

}


function fnCheckString(val, msgval, chars2check)
{
	// whole number, Alphabetic and Alphanumeric Validation
	//var chars2check="0123456789";
	//var chars2check="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefgnijklmnopqrstuvwxwz";
	//var chars2check="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefgnijklmnopqrstuvwxwz0123456789";
  var ret = new Boolean(true);

 
      for (var i = 0; i < val.length; i++) {
		  var ch = val.charAt(i)
			
            if(chars2check.indexOf(ch)!=-1){
				 continue	 
			}
			 Error_Details(msgval);
						ret = false;
						return ret;
      }
	
      return ret;
}

function partValidation(val, msgval, chars2check, beforeDec, total, charType)
{
   var ret = new Boolean(true);


		var charCount = 1;
		var DecimalFound = false 
	
		if(!val.length==0 && !val.length==total && beforeDec==3)
		{
						Error_Details(msgval);
						ret = false;
						return ret;
		}
		if(!val.length==0 && !val.length==total && beforeDec==1)
		{
						Error_Details(msgval);
						ret = false;
						return ret;
		}
		for (var i = 0; i < val.length; i++) 
			{
				var ch = val.charAt(i);
					
				if(i==beforeDec && ch != charType)
					{
						Error_Details(msgval);
						ret = false;
						return ret;
					}
					if(i<beforeDec && ch == charType)
					{
						
						Error_Details(msgval);
						ret = false;
						return ret;
					}
				if (ch == charType && !DecimalFound) 
					{
						DecimalFound = true
						continue
					}
			
				if(DecimalFound && charCount<total-beforeDec && chars2check.indexOf(ch)!=-1)
					{
						
						charCount++
						continue
					}else	if(!DecimalFound && chars2check.indexOf(ch)!=-1)
					{
							continue	 
					}
				
				Error_Details(msgval);
				ret = false;
				return ret;
	  
			} 
          
	
      return ret;
}

function compareDates(dt1, dt2)
{
	var dateRegEx = /^([01]?\d)[\.\-\/\s]?([0123]?\d)[\.\-\/\s]?(\d{4})$/;
	var result1 = dt1.match(dateRegEx);
	var result2 = dt2.match(dateRegEx);
	if(result1 != null){
	     var month1 = result1[1];
	     var day1 = result1[2];
	     var year1 = result1[3];
	}
	if(result2 != null){
	     var month2 = result2[1];
	     var day2 = result2[2];
	     var year2 = result2[3];
	}
	if(result1 && result2){
	     var dif = 0;
	     dif += (month2 - month1);
	     dif += ((year2 - year1) * 12);
	     dif += (day2 - day1 < 0) ? ((dif > 0) ? -1 : 0) : 0;
	     //alert("Difference is: " + dif);
	     return dif;
	
	}
	return null;
}


/*
Compares 2 dates and returns the difference in no of days. the date format send is mm/dd/yyyy
*/
function dateDiff(dt1, dt2, format)
{
	var one_day=1000*60*60*24;
	var date1;
	var date2;
	var Diff;
	if(format==undefined || format==null || format=='' || format=='dd/MM/yyyy' || format=='MM/dd/yyyy'){
		Arrdt1 =  dt1.split("/");
		Arrdt2 =  dt2.split("/");	
	}else{
		Arrdt1 =  dt1.split(".");
		Arrdt2 =  dt2.split(".");
	}
	
	if(format!=undefined && (format=='dd/MM/yyyy' || format=='dd.MM.yyyy')){
		date1 = new Date (Arrdt1[2],Arrdt1[1]-1,Arrdt1[0]); // doing -1 for month as in js the month is from 0-11 and not 1-12
		date2 = new Date (Arrdt2[2],Arrdt2[1]-1,Arrdt2[0]);		
		Diff=Math.ceil((date2.getTime()-date1.getTime())/(one_day)); 
	}else{	
		date1 = new Date (Arrdt1[2],Arrdt1[0]-1,Arrdt1[1]); // doing -1 for month as in js the month is from 0-11 and not 1-12
		date2 = new Date (Arrdt2[2],Arrdt2[0]-1,Arrdt2[1]);		
		Diff=Math.ceil((date2.getTime()-date1.getTime())/(one_day)); 
	}
	return  Diff;	
}

function fnValidateShipping(){	
	fnValidateDropDn('distributorId',message[5567]);
	fnValidateDropDn('shipTo',message[5620]);
	fnValidateDropDn('names',message[5564]);
	var shipto = document.all.shipTo.value;
	var shiptoidid = document.all.addressid.value;
	if (shipto == '4121' && shiptoidid == '')
	{
		Error_Details(message[32]);
	}	
}
function fnValidateTissueShipping(){	
	fnValidateDropDn('distributorId',message[5567]);
	fnValidateDropDn('tissueShipTo',message[5620]);
	fnValidateDropDn('tissueNames',message[5564]);
	var shipto = document.all.tissueShipTo.value;
	var shiptoidid = document.all.tissueAddressId.value;
	if (shipto == '4121' && shiptoidid == '')
	{
		Error_Details(message[10625]);
	}	
}
function fnStrMatch(strVal)
{
	var match = /http:/i.test(strVal)
	if(!match)
	{
		return false;
	}
	return true;
}

function fnValidateNumber(val,fld)
{
	var blDisabled;
	var obj = eval("document.all."+val);
	var objval = TRIM(obj.value);
	var objRegExp  =/(^-?\d\d*$)/;

	if(!objRegExp.test(objval))
	{	
		Error_Details("<b>"+fld+"</b> cannot be other than number. Please enter valid data");
	}
}

function fnSetCursorPosition(elemId)
{
	var elem = document.getElementById(elemId);
    if(elem != null)
	{
        if(elem.createTextRange)
		{
			var range = elem.createTextRange();
            range.move('character', 25);
            range.select();
        }
        else 
		{
            if(elem.selectionStart) 
			{
                elem.focus();
				elem.setSelectionRange(25,25);
            }
            else
                elem.focus();
        }
    }
}

function fnRoundNumber(number, digits) {
	var multiple = Math.pow(10, digits);   
	var rndedNum = Math.round(number * multiple) / multiple;
	return rndedNum;        
}

//common function to validate undefined/null value
function fnValidateObject(strVal)
{
      if(document.getElementById(strVal)!= undefined && document.getElementById(strVal)!= null && document.getElementById(strVal)!= '') {
            return true;
      }else{
            return false;
      }
}
