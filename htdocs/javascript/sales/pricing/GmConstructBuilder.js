var allGroupsHtml;
function fnFilterGroups() {
	var val = document.frmConstructBuilder.groupTypeId.value;
	var sysval = 0;
	var valallGrps = document.getElementById("showAllGroups").checked;

	if(!valallGrps)
	{
		sysval = document.frmConstructBuilder.systemListId.value;
	}
	if( (val==0 && sysval==0) || (val==0 && valallGrps))
	{			
		document.getElementById("Div_Group").innerHTML = allGroupsHtml;
		return;
	}
	document.getElementById("Div_Group").innerHTML = '';
	var newhtml = '';
	var arrGrpLen = arrGroupInp.length;
	var arrFilterGps = new Array();
	var k = 0;
	for (var j=0;j<arrGrpLen ;j++ )
	{	
		if(!valallGrps && val!=0 && sysval!=0)
		{
			if (arrGroupInp[j][2] == val && arrGroupInp[j][3] == sysval)
			{
				var idStr =arrGroupInp[j][0];
				var nmStr=arrGroupInp[j][1];
				newhtml = newhtml + fnCreateChkBox(idStr,nmStr,arrGroupInp[j][2], 'fnGroupDetail('+idStr+');');
				arrFilterGps[k] = arrGroupInp[j][0];
				k++;
			}
		}
		else if(!valallGrps && val==0 && sysval!=0){
			
			if (arrGroupInp[j][3] == sysval)
			{
				var idStr =arrGroupInp[j][0];
				var nmStr=arrGroupInp[j][1];
				newhtml = newhtml + fnCreateChkBox(idStr,nmStr,arrGroupInp[j][2], 'fnGroupDetail('+idStr+');');
				arrFilterGps[k] = arrGroupInp[j][0];
				k++;
			}
		}		
		else
		{			
			if (arrGroupInp[j][2] == val)
			{
				var idStr =arrGroupInp[j][0];
				var nmStr=arrGroupInp[j][1];
				newhtml = newhtml + fnCreateChkBox(idStr,nmStr,arrGroupInp[j][2], 'fnGroupDetail('+idStr+');');
				arrFilterGps[k] = arrGroupInp[j][0];
				k++;
			}
		}
	}
	if (newhtml =='')
	{
		document.getElementById("Div_Group").innerHTML = '<span class=RightTextRed>No Groups in this Type</span>';
	}else
	{
		document.getElementById("Div_Group").innerHTML = newhtml;
	}
}
// To create checkboxes dynamically 
function fnCreateChkBox(id,nm,pid,jsfunc) {
	var partDetailStr = "&nbsp;<a title='Click to open part details'><img id='imgEdit' style='cursor:hand' src='/images/product_icon.gif' onClick='javascript:"+jsfunc+"'/></a>";
	var html = "<input type='checkbox' name='checkGroup' id='"+pid+"' value='"+id+"'>"+nm+partDetailStr+"<br>";
	return html;
}
function fnSystemFilterGroups() {
	var valallGrps = document.getElementById("showAllGroups").checked; 

	if(!valallGrps)
	{
		var val = document.frmConstructBuilder.systemListId.value;
		if(val==0)
		{
			document.getElementById("Div_Group").innerHTML = allGroupsHtml;
			return;
		}
		document.getElementById("Div_Group").innerHTML = '';
		var newhtml = '';
		var arrGrpLen = arrGroupInp.length;
		var arrFilterGps = new Array();
		var k = 0;
		for (var j=0;j<arrGrpLen ;j++ )
		{		
			if (arrGroupInp[j][3] == val)
			{
				var idStr =arrGroupInp[j][0];
				var nmStr=arrGroupInp[j][1];
				newhtml = newhtml + fnCreateChkBox(idStr,nmStr,arrGroupInp[j][2], 'fnGroupDetail('+idStr+');');
				arrFilterGps[k] = arrGroupInp[j][0];
				k++;
			}
		}
		if (newhtml =='')
		{
			document.getElementById("Div_Group").innerHTML = '<span class=RightTextRed>No Groups in this Type</span>';
		}else
		{
			document.getElementById("Div_Group").innerHTML = newhtml;
		}
	}
}


function fnGetGroupTypeName(grpTypeId) {
	var obj = document.frmConstructBuilder.groupTypeId.options;
	var optionLen = obj.length;
	var grpTypeName;
	for(var i=0;i<optionLen;i++)
	{	
		if(obj[i].value==grpTypeId)
		{
			grpTypeName = obj[i].text;
			return (grpTypeName=='' || grpTypeName==undefined)?"Others":grpTypeName;
		}
	}
	return "Others";
}
function fnAddGroup() {	
	
	var arrAllGrp = document.frmConstructBuilder.checkGroup;	
	var arrAllLen = arrAllGrp.length;	

	var groupInputStr='';
	for (var i=0;i< arrAllLen;i++ )
	{
		if (arrAllGrp[i].checked==true)
		{					
			if(gridObj.getRowId(gridObj.getRowIndex(arrAllGrp[i].value))==undefined)
			{
				groupInputStr=groupInputStr+','+arrAllGrp[i].value;
			}
		}
	}
	if(arrAllGrp !=undefined)
	{
		groupInputStr = arrAllLen==undefined?arrAllGrp.value:groupInputStr;
	}
	if(groupInputStr !='')
	{	
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmPricingAjaxDispatchAction.do?method=getGroupsPricing&groupInputStr='+groupInputStr+'&strConstructId=0'+'&ramdomId='+Math.random());
		dhtmlxAjax.get(ajaxUrl,fnAddGroupsData);
	}
}
function fnAddGroupsData(loader) {	
	 var response = loader.xmlDoc.responseXML;	 
	 if(response != null)
	 {					 	
		var xmlDocChilds = response.documentElement.childNodes;
		var len = xmlDocChilds.length;
		var grpId, grpName, grpQty, grpType, listprice,tripwire;
		for(var i=1;i<len;i++) 
		{
	    	grpId = xmlDocChilds[i].attributes[0].nodeValue;
	    	grpName= parseXmlNode(xmlDocChilds[i].childNodes[1].firstChild);
	    	grpQty= parseXmlNode(xmlDocChilds[i].childNodes[2].firstChild);
	    	listprice= parseXmlNode(xmlDocChilds[i].childNodes[3].firstChild);	    	
	    	tripwire= parseXmlNode(xmlDocChilds[i].childNodes[4].firstChild);
	    	grpType= parseXmlNode(xmlDocChilds[i].childNodes[5].firstChild);
	    	if(gridObj.getRowId(gridObj.getRowIndex(grpId))==undefined)
			{
    			gridObj.addRow(grpId,['/images/btn_remove12.gif',grpName,grpQty,listprice,tripwire,grpType]);
    		}		
    	}
    }
}



function fnFetchConstructs() {
	var systemId = document.frmConstructBuilder.systemListId.value;
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmPricingAjaxDispatchAction.do?method=getConstructData&systemId='+systemId+'&ramdomId='+Math.random());
	dhtmlxAjax.get(ajaxUrl,fnFillConstructData);
	fnFillConstructName();
}


function fnFillConstructData(loader) {
	var obj = document.frmConstructBuilder.constructId;
	obj.options.length = 0;
	obj.options[0] =  new Option("[Choose One]","0"); 
	var response = loader.xmlDoc.responseText;
    if (response != null && response.length>0)
    {   
    	var arrobj, id, name,j=0;  
        arrobj = response.split(":");
        var len = arrobj.length;
        for(var i=0;i<len;i=i+2)
        {
        	j++;
			id = arrobj[i];
			name = arrobj[i+1];
       		obj.options[j] =  new Option(name, id);
        }
   	}
}
function fnClearSelections() {
	gridObj.clearAll();
	gridObj.groupBy(5,["#title","#cspan","#cspan","#cspan","#cspan","#cspan"]);
	var arrAllGrp = document.frmConstructBuilder.checkGroup;	
	if(arrAllGrp!=undefined)
	{
		var arrAllLen = arrAllGrp.length;	
		for (var i=0;i< arrAllLen;i++ )
		{
			if (arrAllGrp[i].checked==true)
			{
				arrAllGrp[i].checked= false;
			}
		}
	}
	fnCaluclateConstructPrice();

}
function fnFetchDetails() {
	document.frmConstructBuilder.haction.value =  'load'; 
	document.frmConstructBuilder.submit();
}

function fnShowAddConstruct() {
	if(document.frmConstructBuilder.newConstructFlag.checked)
	{
		document.getElementById("combo_constructName").style.display="none";
		document.getElementById("add_Construct").style.display="inline";
		fnFillConstructName();
	}
	else
	{
		document.getElementById("combo_constructName").style.display="inline";
		document.getElementById("add_Construct").style.display="none";	
	}		
}
function fnFillConstructName() {
		var obj =document.frmConstructBuilder.systemListId;
		var seltext ='';
		if(obj.selectedIndex !=0)
		{
			seltext = obj.options[obj.selectedIndex].text;
			//seltext = seltext.substring(seltext.indexOf('-')+1,seltext.length);
			//seltext =  seltext.replace(/[^a-zA-Z 0-9]+/g,'');
			seltext =  seltext.replace(/[^a-zA-Z 0-9\.-]+/g,'');
		}		
		obj =document.frmConstructBuilder.constLevelId;
		if(obj.selectedIndex !=0)
		{
			seltext = seltext+ '-'+obj.options[obj.selectedIndex].text+' Construct';
		}
		
		document.getElementById("conStaticName").value = seltext;
}
function fnCaluclateConstructPrice() {
		
		var listPriceConstruct = 0;		
		var twPriceConstruct = 0;
		
		var listprice,  tw;
		gridObj.forEachRow(function(id)	{			
				var qty = gridObj.cellById(id, 2).getValue();
				listprice = fnGetPriceNum(gridObj.cellById(id, 3).getValue());				
								
				tw = fnGetPriceNum(gridObj.cellById(id, 4).getValue());
				
				qty= isNaN(qty)?0:qty;
				
				listPriceConstruct = listPriceConstruct+qty*listprice;				
				twPriceConstruct = twPriceConstruct+qty*tw;						
		});
	
		document.getElementById("idListPrice").value = fnDollarFormat(listPriceConstruct);		
		document.getElementById("idTwLimit").value = fnDollarFormat(twPriceConstruct);
	
}

function fnReloadGridXml(loader) {
	 var response = loader.xmlDoc.responseXML;	 
	 if(response != null)
	 {			
		gridObj.clearAll();
		gridObj.parse(response);			 
		gridObj.groupBy(5,["#title","#cspan","#cspan","#cspan","#cspan","#cspan"]);
		fnCaluclateConstructPrice();
	 }
}

function doOnCellEdit(stage, rowId, cellInd, nValue, oValue) {		
	if(cellInd==2)
	{
		if (stage==1){
			var c=this.editor.obj;
			if(c!=null) c.select();
			return true;
		}		
		
		return true;
	}
	else 
	{
		return false;
	}
}

function doOnCellChange(id,ind, val) {
	
	if (ind==2) 
	{			
 		fnCaluclateConstructPrice();
 	}
}

function fnValidateQty() {
	var isValid = true;
	gridObj.forEachRow(function(id)
	{		
		var val = gridObj.cellById(id, 2).getValue();

		if(isValid && val.length !=0 && !(/^ *[0-9]+ *$/.test(val)))
		{
			Error_Details(message_sales[133]);	
			isValid =false;
			return;
		}
     
   });
}


function fnGroupDetail(groupID) {   
        windowOpener("/gmGroupPartMap.do?strPartListFlag=Y&hviewEdit=view&groupId="+groupID+"&groupNM=","","resizable=yes,scrollbars=yes,top=250,left=300,width=950,height=400");
}


function fnValidateOnSubmit() {
	gridObj.editStop();
	var obj =document.frmConstructBuilder.constructId;
	
	if(!document.frmConstructBuilder.newConstructFlag.checked)
	{
		if(obj.selectedIndex ==0)
		{
			Error_Details(message_sales[134]);	
		}
	}
	else
	{
		fnValidateDropDn('systemListId', lblSystem);
		fnValidateDropDn('constLevelId', lblLevel);
		fnValidateDropDn('statusId', lblStatus);
		
	}	
	var constructLength = document.frmConstructBuilder.constructId.options.length;	
	var consTextVal2 = ((document.getElementById("conStaticName2").value).toUpperCase()).replace(/(^\s+|\s+$)/g, ''); 
	var ErrCnt=0;

			for(var i=1;i<=constructLength-1;i++){
			      var constDropVal = ((document.frmConstructBuilder.constructId.options[i].text).toUpperCase()).replace(/(^\s+|\s+$)/g, '');
			      var consTextVal = ((document.getElementById("conStaticName").value).toUpperCase()).replace(/(^\s+|\s+$)/g, '');
			      var newConsValue="";
				      if(consTextVal2!=''){
				       newConsValue = consTextVal+' '+consTextVal2;
				      }else{
				    	  newConsValue = consTextVal		    	  
				      }
			                if(constDropVal==newConsValue){
			                	ErrCnt=1;
			                	break;
			                }
			}
			if(ErrCnt>0){
				Error_Details("Name already exists cannot add construct name");	
			}
	fnValidateQty();
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}

	fnSubmit();
}
function fnSubmit() {

	var rowIds = gridObj.getAllRowIds();
	var idsArr = rowIds.split(",");
	var arrLen = idsArr.length;
	var strInputString = '';
	for (var i=0;i<arrLen;i++)
	{
		strInputString = strInputString+idsArr[i]+'^'+ gridObj.cells(idsArr[i], 2).getValue()+'|';
	}
	document.frmConstructBuilder.hinputString.value =  strInputString; 
	document.frmConstructBuilder.hconstructName.value =  document.getElementById("conStaticName").value+' '+document.getElementById("conStaticName2").value; 
	document.frmConstructBuilder.haction.value =  'save'; 
	fnStartProgress("Y");
	document.frmConstructBuilder.submit();
}


function fnVoid() {	
	fnValidateDropDn('constructId', message_sales[188]);
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	 	var cancelType = 'SYSCON';
		
		var obj =document.frmConstructBuilder.constructId;
		
		document.frmConstructBuilder.hTxnId.value = obj.value;	
		document.frmConstructBuilder.hTxnName.value = obj.options[obj.selectedIndex].text;
		document.frmConstructBuilder.action="/GmCommonCancelServlet?hTxnName="+document.frmConstructBuilder.hTxnName.value+"&hCancelType="+cancelType+"&hTxnId="+document.frmConstructBuilder.hTxnId.value;
		document.frmConstructBuilder.submit();
}	