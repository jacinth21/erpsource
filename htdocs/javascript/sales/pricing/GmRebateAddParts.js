var hPartInputStr ='';
//To Save the rebate parts using ajax call
function fnSubmit(){
	partNumberValidation();
	dateValidation();
	fnValidateTxtFld('txt_LogReason',message_accounts[203]);
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	
	fnStartProgress('Y');
	document.frmRebateSetup.partInputString.value=hPartInputStr
	document.frmRebateSetup.action = "/gmRebateSetup.do?method=saveRebatepartMappingDtls";
	document.frmRebateSetup.submit();
	
}

//Reset the field
function fnReset() {
	document.frmRebateSetup.batchPartNum.value = '';
	document.frmRebateSetup.effectivePartDate.value = '';
	document.frmRebateSetup.txt_LogReason.value = '';

}

//Effective date validation
function dateValidation(){
	var objEffDt='';
	var objToDt='';
	objToDt=document.frmRebateSetup.hcontractToDate;
	objEffDt=document.frmRebateSetup.effectivePartDate;
	toDate = objToDt.value;
	EffDt=objEffDt.value;
	
	fnValidateTxtFld('effectivePartDate','<b>Effective Date</b>');
/*	if( EffDt ==''){
		Error_Details('Please provide <b>Effective Date</b>');
	}*/
	if(EffDt != ''){
		CommonDateValidation(objEffDt,format,'Please Enter Valid Date Format');
	}
	var toDtDiff = dateDiff(toDate,EffDt,format);
	 if (toDtDiff > 0) {
	Error_Details('<b>Effective Date</b> must be less than <b>Contract To Date</b>');
		}
	var toDtFuture = dateDiff(today_dt,EffDt,format);
	if (toDtFuture < 0) {			
	Error_Details('<b>Effective Date</b> must be current or future date');
			}
}


//Partnumber validation
function partNumberValidation(){
	
	var objval = TRIM(document.frmRebateSetup.batchPartNum.value);
	
	document.frmRebateSetup.batchPartNum.value = objval;
	var objlen = objval.length;
	var strTemp = objval;
	strTemp = strTemp.replace(/(\r\n|\n|\r)/gm, "|");
	var arrLines = strTemp.split('|');
	objval = objval.replace(/(\r\n|\n|\r)/gm, ',');
	var part_id = '';
	var dupPartids = '';
	objval = objval + ',';
	if (objlen == 0) {
		Error_Details("Please Enter Part Number to Proceed");
	}
	for (var i = 0; i < arrLines.length; i++) {
		part_id = objval.substring(0, objval.indexOf(','));
		objval = objval.substring((part_id.length) + 1, objval.length);
		part_id = TRIM(part_id);
		str = part_id;
		part_id = str.toUpperCase();
		
		if (part_id != '') {
			if (hPartInputStr.indexOf(part_id + ",") == -1) {
				hPartInputStr += (part_id + ",");
			}
	
	}
	}	
}

