function onAnlToolBar1(id, state){
	 dtype_15_g = gridObjGroup.getColIndexById("hdtype");
	 dchange_16_g = gridObjGroup.getColIndexById("dchange");
	 dprice_17_g = gridObjGroup.getColIndexById("dprice");
	 fDate_18_g = gridObjGroup.getColIndexById("fDate");
	 tDate_19_g = gridObjGroup.getColIndexById("tDate");
	 
	if(id=="ditms"){
		if(anltb1.getItemState(id)){
			gridObjGroup.setColumnHidden(dtype_15_g, false);
			gridObjGroup.setColumnHidden(dchange_16_g, false);
			gridObjGroup.setColumnHidden(dprice_17_g, false);
			gridObjGroup.setColumnHidden(fDate_18_g, false);
			gridObjGroup.setColumnHidden(tDate_19_g, false);
			anltb1.setItemText("ditms",message_sales[82]);
			anltb1.setItemImage("ditms","page.gif");
		}else{
			gridObjGroup.setColumnHidden(dtype_15_g, true);
			gridObjGroup.setColumnHidden(dchange_16_g, true);
			gridObjGroup.setColumnHidden(dprice_17_g, true);
			gridObjGroup.setColumnHidden(fDate_18_g, true);
			gridObjGroup.setColumnHidden(tDate_19_g, true);
			anltb1.setItemText("ditms",message_sales[83]);
			anltb1.setItemImage("ditms","page_range.gif");
		}
	}else if(id=="ex"){
		if(anltb1.getItemState(id)){
			gridObjGroup.expandAll();
			anltb1.setItemText("ex",message_sales[84]);
			anltb1.setItemImage("ex","page.gif");
		}else{
			gridObjGroup.collapseAll();
			anltb1.setItemText("ex",message_sales[85]);
			anltb1.setItemImage("ex","page_range.gif");
		}
	}
}
function onAnlToolBar(id, state){
	 select_2_g = gridObjGroup.getColIndexById("select");
	
	 approve_0_a = gridObjAnalysis.getColIndexById("approve");
	 name_1_a = gridObjAnalysis.getColIndexById("name");
	 adjusted_5_a = gridObjAnalysis.getColIndexById("adjusted");
	 lvlstrid_9_a = gridObjAnalysis.getColIndexById("lvlstrid");
	/* ad_12_a = gridObjAnalysis.getColIndexById("ad");
	 vp_13_a = gridObjAnalysis.getColIndexById("vp");*/
	 pricons_14_a = gridObjAnalysis.getColIndexById("pricons");
	 prigrp_15_a = gridObjAnalysis.getColIndexById("prigrp");
	 dfromdt_20_a = gridObjAnalysis.getColIndexById("dfromdt");
	 dtodt_21_a = gridObjAnalysis.getColIndexById("dtodt");
	 dprice_22_a = gridObjAnalysis.getColIndexById("dprice");
	if(id=="ex")
	{
		if(anltb.getItemState(id))
		{
			gridObjAnalysis.expandAll();
			anltb.setItemText("ex",message_sales[84]);
			anltb.setItemImage("ex","page.gif");
		}
		else
		{
			gridObjAnalysis.collapseAll();
			anltb.setItemText("ex",message_sales[85]);
			anltb.setItemImage("ex","page_range.gif");
		}
	}
	else if(id=="pen")
	{
		if(anltb.getItemState(id))
		{
			gridObjAnalysis.filterBy(approve_0_a, "52027");
			gridObjAnalysis.expandAll();
			anltb.setItemText("pen",message_sales[86]);
			anltb.setItemImage("pen","page.gif");
		}
		else
		{
			gridObjAnalysis.filterBy(approve_0_a,"");
			anltb.setItemText("pen",message_sales[87]);
			anltb.setItemImage("pen","page_range.gif");
		}
	}
/*	else if(id=="adp"){
		if(anltb.getItemState(id)){
			gridObjAnalysis.setColumnHidden(ad_12_a,false);
			anltb.setItemText("adp","Hide AD Limit");
			anltb.setItemImage("adp","page.gif");
		}else{
			gridObjAnalysis.setColumnHidden(ad_12_a,true);
			anltb.setItemText("adp","Show AD Limit");
			anltb.setItemImage("adp","page_range.gif");
		}
	}
	else if(id=="vpp"){
		if(anltb.getItemState(id)){
			gridObjAnalysis.setColumnHidden(vp_13_a,false);
			anltb.setItemText("vpp","Hide VP Limit");
			anltb.setItemImage("vpp","page.gif");
		}else{
			gridObjAnalysis.setColumnHidden(vp_13_a,true);
			anltb.setItemText("vpp","Show VP Limit");
			anltb.setItemImage("vpp","page_range.gif");
		}
	}*/
	else if(id=="pc"){
		if(anltb.getItemState(id)){
			gridObjAnalysis.filterBy(pricons_14_a, "Y");
			anltb.setItemText("pc",message_sales[88]);
			anltb.setItemImage("pc","page.gif");
		}else{
			gridObjAnalysis.filterBy(pricons_14_a, "");
			anltb.setItemText("pc",message_sales[89]);
			anltb.setItemImage("pc","page_range.gif");
		}
	}
	else if(id=="pg"){
		if(anltb.getItemState(id)){
			gridObjAnalysis.filterBy(prigrp_15_a, "Y");
			anltb.setItemText("pg",message_sales[90]);
			anltb.setItemImage("pg","page.gif");
		}else{
			gridObjAnalysis.filterBy(prigrp_15_a, "");
			anltb.setItemText("pg",message_sales[91]);
			anltb.setItemImage("pg","page_range.gif");
		}
	}
	else if(id=="ditms"){
		if(anltb.getItemState(id)){
			gridObjAnalysis.setColumnHidden(dfromdt_20_a, false);
			gridObjAnalysis.setColumnHidden(dtodt_21_a , false);
			gridObjAnalysis.setColumnHidden(dprice_22_a, false);
			anltb.setItemText("ditms",message_sales[92]);
			anltb.setItemImage("ditms","page.gif");
		}else{
			gridObjAnalysis.setColumnHidden(dfromdt_20_a, true);
			gridObjAnalysis.setColumnHidden(dtodt_21_a , true);
			gridObjAnalysis.setColumnHidden(dprice_22_a, true);
			anltb.setItemText("ditms",message_sales[93]);
			anltb.setItemImage("ditms","page_range.gif");
		}
	}
	else if(id=="approve"){
		flgChkSubmit = true;
		if(anltb.getItemState(id)){
			anltb.setItemText("approve",message_sales[94]);
			anltb.setItemImage("approve","page.gif");
			var totapp = 0;
			var ErrorCount='0';
			var strAppend = ''; 
			var arr=new Array();
			var i=0;
			aprlvl_7_a = gridObjAnalysis.getColIndexById("aprlvl");

			gridObjGroup.forEachRow( function(rowId){ 
				if(gridObjGroup.getLevel(rowId)==1 || (gridObjGroup.getLevel(rowId)==2 && gridObjGroup.cellById(rowId,select_2_g).isChecked()==true)){
					var flgcntgrp = false;
					gridObjAnalysis.forEachRow( function(id)   { 
					if(gridObjAnalysis.getLevel(id)>=1 && gridObjAnalysis.cellById(id,lvlstrid_9_a).getValue()==rowId){
						var strStatus = gridObjAnalysis.cellById(id,approve_0_a).getValue();
						if(strStatus == 52027  || strStatus == '52150' || strStatus == '52151')	{		
							var limit=0;
							var adjustedPrice=gridObjAnalysis.cellById(id,adjusted_5_a).getValue();
							/*var adlimit=gridObjAnalysis.cellById(id,ad_12_a).getValue();
							var vplimit=gridObjAnalysis.cellById(id,vp_13_a).getValue();   
							if(userdesc=='AD'){
								limit = adlimit;
							}else if(userdesc=='VP'){
								limit = vplimit; 
							}*/
							var strApprLvl = gridObjAnalysis.cellById(id, aprlvl_7_a).getValue();
							var strApprLvlSta = userdesc+'_'+strApprLvl;
							
							if(userdesc != 'PC'){
								
								if(adjustedPrice !='' && ( adjustedPrice < limit ))
								{
									
									if(strAppend==''){
										strAppend = gridObjAnalysis.cellById(id,name_1_a).getValue();
									}else{
										strAppend =strAppend+"<br>"+gridObjAnalysis.cellById(id,name_1_a).getValue();
									}
									ErrorCount++;
								}
							 }
							
							 if(adjustedPrice>0 && (adjustedPrice > limit) && (strApprLvlSta != 'AD_PC' && strApprLvlSta != 'VP_PC')){ 
									arr[i]=gridObjAnalysis.cellById(id,lvlstrid_9_a).getValue();
									if(!flgcntgrp){
										++totapp;
									}
								}
							 flgcntgrp=true;
						}
					}i++;
					});
					}
				});
			
			var con = confirm(Error_Details_Trans(message_sales[69],totapp));
			if(con==true){
				fnApproveAll(arr);
				if (ErrorCount > 0){
					var array =[userdesc,strAppend];
					Error_Details(Error_Details_Trans(message_sales[70],array));
					Error_Show();
					Error_Clear();
					return false;
				}
				//Prompting for Submit only when we have at least one group to approve.
				if(totapp>0)
					fnSubmitForm();
			}else{
				unSelectAll();
			}
		}else{
			anltb.setItemText("approve",message_sales[95]);
			anltb.setItemImage("approve","page_range.gif");
			unSelectAll();
		}
	}else if(id=="den"){
		flgChkSubmit = true;
		if(anltb.getItemState(id)){
			var con = confirm(message_sales[71]);
			if(con==true){
				anltb.setItemText("den",message_sales[96]);
				anltb.setItemImage("den","page.gif");
				fnSelectAllFun(52029);
				fnSubmitForm();
			}

		}else{
			var con = confirm(message_sales[72]);
			if(con==true){
				anltb.setItemText("den",message_sales[97]);
				anltb.setItemImage("den","page_range.gif");
				fnUnSelectAllFun(52029,52027);
			}
		}
	}else if(id=="subVp"){
		if(fnValidateUser(52150)){
			
		flgChkSubmit = true;
		if(anltb.getItemState(id)){
			var con = confirm(message_sales[73]);
			if(con){
				anltb.setItemText("subVp",message_sales[98]);
				anltb.setItemImage("subVp","page.gif");
				fnSelectAllFun(52150,52027);
				fnSubmitForm();
			}
		}else{
			var con = confirm(message_sales[74]);
			if(con){
				anltb.setItemText("subVp",message_sales[99]);
				anltb.setItemImage("subVp","page_range.gif");
				fnUnSelectAllFun(52150,52027);
			}
		}
		}
	}else if(id=="subPc"){
		if(fnValidateUser(52151)){
		flgChkSubmit = true;
		if(anltb.getItemState(id)){
			var con = confirm(message_sales[75]);
			if(con){
				anltb.setItemText("subPc",message_sales[300]);
				anltb.setItemImage("subPc","page.gif");
				fnSelectAllFun(52151,52027);
				fnSubmitForm();
			}
		}else{
			var con = confirm(message_sales[78]);
			if(con){
				anltb.setItemText("subPc",message_sales[301]);
				anltb.setItemImage("subPc","page_range.gif");
				fnUnSelectAllFun(52151,52027);
				}
			}
		}
	}
}

//This function will prompt the user if they want to submit the changes when they do Approve All or Move to VP or PC.
//This way, user is not expected to hit submit button explictly.
function fnSubmitForm()
{
	var con = confirm(message_sales[79]);
	if(con==true){
		fnOnSave('Submit')
	}
}
function fnApproveAll(arr){
	var i=0;
	 select_2_g = gridObjGroup.getColIndexById("select");
	 
	 approve_0_a = gridObjAnalysis.getColIndexById("approve"); 
	 ActiveFlg_23_a = gridObjAnalysis.getColIndexById("ActiveFlg");
	 aprlvl_7_a = gridObjAnalysis.getColIndexById("aprlvl");
		
	gridObjGroup.forEachRow( function(rowId){ 
					if(gridObjGroup.getLevel(rowId)==1 || (gridObjGroup.getLevel(rowId)==2 && gridObjGroup.cellById(rowId,select_2_g).isChecked()==true)){
						var flgcntgrp = false;
						gridObjAnalysis.forEachRow( function(id)   { 
						if(gridObjAnalysis.getLevel(id)>=1 && arr[i]==rowId){
							var strApprLvl = gridObjAnalysis.cellById(id, aprlvl_7_a).getValue();
							//alert('strApprLvl'+strApprLvl);
							var strApprLvlSta = userdesc+'_'+strApprLvl;
							if((strApprLvlSta != 'AD_PC' && strApprLvlSta != 'VP_PC')){
								// When we initiate a request for 2 systems in that 1 system with all groups are Above Trip Pirce and other system with
								// all groups are Below Trip Price,when we changed the request(below Trip) from Pending to Submit for VP and then approving the request(above trip)by clicking
								// Approve All status,52028 number displays in Approval status.
								if(strApprLvlSta == 'AD_AD'){  
									var combo = gridObjAnalysis.getCombo(approve_0_a);
									combo.remove(52028);
									combo.put(52028,'Approved');
								}
								gridObjAnalysis.cellById(id,approve_0_a).setValue(52028); 
		 						//gridObjAnalysis.cellById(id,ActiveFlg_23_a).setValue(false);
		 						//gridObjAnalysis.cellById(id,ActiveFlg_23_a).setDisabled(false);
								if(( gpoID != 0 && userdesc == 'PC') ||(contfl == 'YES' && userdesc == 'PC') || (contfl == 'NO'&& gpoID ==0))
								{
		 						gridObjAnalysis.cellById(id,ActiveFlg_23_a).setValue(false);
		 						gridObjAnalysis.cellById(id,ActiveFlg_23_a).setDisabled(false);
								}
							}
						}
						i++;
						});
					}	
	});
}

function unSelectAll(){
	select_2_g = gridObjGroup.getColIndexById("select");
	approve_0_a = gridObjAnalysis.getColIndexById("approve"); 
	lvlstrid_9_a = gridObjAnalysis.getColIndexById("lvlstrid");
	ActiveFlg_23_a = gridObjAnalysis.getColIndexById("ActiveFlg");
		gridObjGroup.forEachRow( function(rowId){
			if((gridObjGroup.getLevel(rowId)==1) || (gridObjGroup.getLevel(rowId)==2 && gridObjGroup.cellById(rowId,select_2_g).isChecked()==true))
			{
			gridObjAnalysis.forEachRow( function(id){
				if(gridObjAnalysis.getLevel(id)>=1 && gridObjAnalysis.cellById(id,lvlstrid_9_a).getValue()==rowId){	
					if(!gridObjAnalysis.cellById(id,approve_0_a).getValue()=='' && gridObjAnalysis.cellById(id,approve_0_a).getValue()==52028){
						gridObjAnalysis.cellById(id,approve_0_a).setValue(52027);
						gridObjAnalysis.cellById(id,ActiveFlg_23_a).setChecked(false);
						gridObjAnalysis.cellById(id,ActiveFlg_23_a).setDisabled(true);
					}
				}
			});
			}
		});
}

function fnCallDeleteStatus(rowId, cellInd){
	approve_0_a = gridObjAnalysis.getColIndexById("approve"); 
	aprlvl_7_a = gridObjAnalysis.getColIndexById("aprlvl");
	var combo1 = gridObjAnalysis.getCombo(approve_0_a);
    var strApprLvl = gridObjAnalysis.cellById(rowId, aprlvl_7_a).getValue();
	var strApprLvlSta = userdesc+'_'+strApprLvl;
	// remove the Approved, Submit for VP and Submit for PC	
		combo1.remove(52028);
		combo1.remove(52150);
	    combo1.remove(52151);

		if(strApprLvlSta == 'AD_AD' || strApprLvlSta == 'VP_AD' || strApprLvlSta == 'PC_AD' || strApprLvlSta == 'PC_PC'){ //52150,52151 'Submit for VP,Submit for PC';
			combo1.put(52028,'Approved');
		}else if(strApprLvlSta == 'AD_PC'){ //'52028,52151'; //'Approved,Submit for PC';
			combo1.put(52150,'Submit for VP');
		}else if(strApprLvlSta == 'VP_PC'){ // '52028,52150'; //'Approved,Submit for VP';
			combo1.put(52150,'Submit for VP');
			combo1.put(52151,'Submit for PC');
		}
		if(strApprLvlSta == 'PC_PC'){
			combo1.put(52150,'Submit for VP');
			combo1.put(52151,'Submit for PC');
		}
		combo1.save();
}
function  doOnCellEditDisable(rowId, stage, cellInd, nValue, oValue){
	ActiveFlg_23_a = gridObjAnalysis.getColIndexById("ActiveFlg");
	//alert("cellInd:"+rowId+stage+cellInd+nValue+oValue);
		
	if (cellInd ==0){
		if(stage==2 && nValue != ''){
		
				var Status = gridObjAnalysis.cellById(rowId, approve_0_a).getValue();
			/*	var VPLimit = gridObjAnalysis.cellById(rowId, vp_13_a).getValue();
				var ADLimit = gridObjAnalysis.cellById(rowId, ad_12_a).getValue();*/
				var Adjusted = gridObjAnalysis.cellById(rowId, adjusted_5_a).getValue();
				var Limit = 0;
	
				/*if(userdesc =='AD'){	
					Limit = ADLimit;
				}else if(userdesc == 'VP'){
					Limit=VPLimit;
				}*/
				if(userdesc !='PC'){
					if(Adjusted < Limit && Status==52028){
						var array = [userdesc,Limit,Adjusted];
						Error_Details(Error_Details_Trans(message_sales[80],array));
						if (ErrorCount > 0){
							Error_Show();
							Error_Clear();
							return false;
						}
						gridObjAnalysis.cellById(rowId, approve_0_a).setValue(oValue);
					}
				}
           		if(Status!= 52028){
        	   		gridObjAnalysis.cellById(rowId, ActiveFlg_23_a).setValue("");
        	   		gridObjAnalysis.cellById(rowId, ActiveFlg_23_a).setDisabled(true);
           		}/*  else{
        	   		gridObjAnalysis.cellById(rowId, ActiveFlg_23_a).setValue("");
					gridObjAnalysis.cellById(rowId, ActiveFlg_23_a).setDisabled(false);
           		}*/
           		else if((gpoID != 0 && userdesc == 'PC') ||(contfl == 'YES' && userdesc == 'PC')){
           			gridObjAnalysis.cellById(rowId, ActiveFlg_23_a).setValue(false);
					gridObjAnalysis.cellById(rowId, ActiveFlg_23_a).setDisabled(false);
           		}
	  	}
		if(cellInd == 0){
			fnCallDeleteStatus(stage,rowId,cellInd);
		}
		
	 }
	 /*else if((cellInd == ActiveFlg_23_a) && ((gpoID != 0 && userdesc != 'PC') ||(contfl == 'YES' && userdesc != 'PC'))){
		Error_Details(" To Publish a Group/Part please contact Pricing Team ");
				 if (ErrorCount > 0){
					Error_Show();
					Error_Clear();
					return false;
	 }
	 }*/
	 return true;
}

function doOnCellEdit(stage, rowId, cellInd, nValue, oValue) {

	 status_1_g = gridObjGroup.getColIndexById("GrpStatus");
	 select_2_g = gridObjGroup.getColIndexById("select");
	 type_6_g = gridObjGroup.getColIndexById("type");
	 change_7_g = gridObjGroup.getColIndexById("change");
	 adjusted_8_g = gridObjGroup.getColIndexById("adjusted");
	 dtype_12_g = gridObjGroup.getColIndexById("dtype");
	 effDate_13_g = gridObjGroup.getColIndexById("effDate");
	 DemoFl_14_g = gridObjGroup.getColIndexById("DemoFL");
	 dtype_15_g = gridObjGroup.getColIndexById("hdtype");
	 dchange_16_g = gridObjGroup.getColIndexById("dchange");
	 dprice_17_g = gridObjGroup.getColIndexById("dprice");
	 fDate_18_g = gridObjGroup.getColIndexById("fDate");
	 tDate_19_g = gridObjGroup.getColIndexById("tDate");
	 var applvl = gridObjGroup.getColIndexById("AppLvl");
	 discount_9_g = gridObjGroup.getColIndexById("discount");

	 gridObjGroup.enableBlockSelection(false);
	if(cellInd==select_2_g || cellInd==type_6_g || cellInd==change_7_g || cellInd==dtype_12_g || cellInd==effDate_13_g || cellInd==DemoFl_14_g || 
		cellInd==dtype_15_g || cellInd==dchange_16_g  || cellInd==dprice_17_g || cellInd==18 || cellInd==tDate_19_g)
	{
		
		if (cellInd==change_7_g && stage==1){
			var c=this.editor.obj;			
			if(c!=null) c.select();
			return true;
		}
		else if ((cellInd==change_7_g || cellInd==type_6_g) && stage==2){
			gridObjGroup.enableBlockSelection(true);
			if(gridObjGroup.getLevel(rowId) == 0)
			{
					fnApplySystemChanges(rowId);
					fnApplyApprLevel(rowId);
			}
			else if (gridObjGroup.getLevel(rowId) == 1)
			{   
			   if(fnCalcAdjPrice(rowId,type_6_g,change_7_g,adjusted_8_g))
				{
					//added for task 5169
					//'YES' is set as a value for the check flag in openStartTreeItem with that value some part of coding will be executed 
					onOpenStartTreeItem(rowId,'YES');	
				}
				fnApplyApprLevel(rowId);
			}
			else if(cellInd==change_7_g || cellInd==type_6_g)
			{
				if(gridObjGroup.cellById(rowId,status_1_g).getValue()!="Approved"){
					gridObjGroup.cellById(rowId,select_2_g).setChecked(true);
					fnCalcAdjPrice(rowId,type_6_g,change_7_g,adjusted_8_g);
					
					fnApplyApprLevel(rowId);
				}
			}
		}
		else if((cellInd==dtype_15_g || cellInd==dchange_16_g ) && stage==2){
			
			if(gridObjGroup.getLevel(rowId) == 0)
			{	
					fnApplyDemoSystemChanges(rowId);
			}
			else if (gridObjGroup.getLevel(rowId) == 1)
			{
				if(gridObjGroup.cellById(rowId,status_1_g).getValue()!="Approved"){
					fnCalcAdjPrice(rowId,dtype_15_g,dchange_16_g,dprice_17_g);
				}
			}
		}
		else if(cellInd==select_2_g && stage==1)	
		{	
			gridObjGroup.selectRowById(rowId,true,true,true);		
		}
		
		if((cellInd==type_6_g || cellInd==dtype_15_g) && stage==0)
		{
			//var combo = gridObjGroup.getCombo(cellInd);
			// Commented for BUG-307
	       /* if (gridObjGroup.getLevel(rowId) == 0) {
	            combo.save();
	            combo.remove('52020');	          
	        }*/	        
		}
		else if ((cellInd == type_6_g || cellInd==dtype_15_g) && (stage == 2))
		{
  			 //for finishing edit;
 	  		gridObjGroup.getCombo(cellInd).restore();
 	  		
 	  		// Added for BUG-307 
 	  		var chngType = gridObjGroup.cellById(rowId,type_6_g).getValue();
 	  		if(gridObjGroup.getLevel(rowId) == 1){
 	  			if(chngType == 52020){
 	  				gridObjGroup.cellById(rowId,change_7_g).setValue('');
 	  				gridObjGroup.cellById(rowId,adjusted_8_g).setValue('');
 	  				gridObjGroup.cellById(rowId,applvl).setValue('');
 	  				gridObjGroup.cellById(rowId,discount_9_g).setValue('');
 	  			}
			}
 	  		// Added for BUG-307 
 	  		if(gridObjGroup.getLevel(rowId) == 0){//this will get executed at the system level
	  			gridObjGroup.cellById(rowId,change_7_g).setValue('');
	  			fnGroupClearValues(rowId);
	  		}else if(gridObjGroup.getLevel(rowId) == 1){//this will get executed at the group level
	  			fnPartClearValues(rowId);
	  		}else if(gridObjGroup.getLevel(rowId) == 2){//this will get executed at the Part level
	  			if(chngType == 52024){
	  				gridObjGroup.cellById(rowId,select_2_g).setChecked(false);
	  			}
	  			if(chngType == 52020){
	  				fnPartClearIndividual(rowId);
	  			}
	  		}
  	  	}
		/*
		if(cellInd==15 && stage==0)
		{
			var combo = gridObjGroup.getCombo(15);
	                
		}
		else if ((cellInd == 15) && (stage == 2))
		{
  			 //for finishing edit;
 	  		gridObjGroup.getCombo(15).restore();
 	  	}
		*/
		else if((cellInd == DemoFl_14_g) && (stage ==1)){ //checkbox for demo purpose
			if(gridObjGroup.getLevel(rowId) == 0)
			{	
					fnApplyDemoChkBox(rowId);
			}	
		}
		else if((cellInd==fDate_18_g || cellInd==tDate_19_g) && stage==2){
				if(gridObjGroup.getLevel(rowId) == 0){
					fnApplyDemoDates(rowId,cellInd);
				}
		}
		else if((cellInd==effDate_13_g || cellInd==dtype_12_g)){
			gridObjGroup.enableBlockSelection(true);
			disenDates(rowId,cellInd);
			if(stage==2){
				fnApplyEffTpDt(rowId,cellInd);
			}
			
				
		}else if(cellInd==change_7_g && stage == 0){
			chngType = gridObjGroup.cellById(rowId,type_6_g).getValue();
		  if(!(chngType == 52020 || chngType == 52021 || chngType == 52022 || chngType == 52023)){				
			  return false;
			}
		  
		  if(gridObjGroup.getLevel(rowId) == 0){
			  if(chngType == 52020){
				  return false;
			  }
		  }
		 }
		gridObjGroup.enableBlockSelection(true);
		return true;
	}
	else 
	{
		return false;
	}
}
function fnApplyEffTpDt(rowId,cellInd){
	var date = gridObjGroup.cellById(rowId, cellInd).getValue();
	var groups = gridObjGroup.getSubItems(rowId).split(",");
    var len = groups.length;
    //disable enable for the system row
    disenDates(rowId,cellInd);
    status_1_g = gridObjGroup.getColIndexById("GrpStatus");
    
    for(var i=0;i<len;i++)
    {
    	if(gridObjGroup.cellById(groups[i],status_1_g).getValue()!="Approved"){
	    	gridObjGroup.cellById(groups[i],cellInd).setValue(date);
	    	disenDates(groups[i],cellInd); //disable enable for child rows
    	}
	    	var parts = gridObjGroup.getSubItems(groups[i]).split(",");
	    	if(parts!=null && parts!=''){
		    	var l = parts.length;
		    	for(var j=0;j<l;j++)
		        {	
		    		if(gridObjGroup.cellById(parts[j],status_1_g).getValue()!="Approved")
		        	gridObjGroup.cellById(parts[j],cellInd).setValue(date);
		        	disenDates(parts[j],cellInd); //disable enable for child rows
		        }
	    	}
    }
}
function disenDates(rowId,cellInd){
	dtype_12_g = gridObjGroup.getColIndexById("dtype");
	effDate_13_g = gridObjGroup.getColIndexById("effDate");
	if((cellInd==dtype_12_g && gridObjGroup.cellById(rowId, dtype_12_g).getValue()==53000)
			||(cellInd==effDate_13_g && gridObjGroup.cellById(rowId, dtype_12_g).getValue()==53000)){
		gridObjGroup.cellById(rowId, effDate_13_g).setValue('');
		//gridObjGroup.cellById(rowId,13).setDisabled(true);
		gridObjGroup.setColumnExcellType(effDate_13_g,"ro");
	}
	else if((cellInd==dtype_12_g && gridObjGroup.cellById(rowId, dtype_12_g).getValue()==53001)
			||(cellInd==effDate_13_g && gridObjGroup.cellById(rowId, dtype_12_g).getValue()==53001)){
		//gridObjGroup.cellById(rowId,13).setDisabled(false);
		gridObjGroup.setColumnExcellType(effDate_13_g,"dhxCalendar");
	}
}
function fnLoadWorkflowData() {
	if(reqid!='')
	{
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmPricingRequestDAction.do?method=getWorkflowDetails&strRequestId='+reqid+'&ramdomId='+new Date().getTime());
		var loader = dhtmlxAjax.getSync(ajaxUrl);
		fillWorkflowData(loader);
	}
}
function fillWorkflowData(loader) {
	var response = loader.xmlDoc.responseXML
	if(response!= null);
	 {			
		gridObjWorkflow.clearAll();
		gridObjWorkflow.parse(response);
     }
}
function fnApplyDemoDates(rowId,cellInd){
	var date = gridObjGroup.cellById(rowId, cellInd).getValue();
	var groups = gridObjGroup.getSubItems(rowId).split(",");
    var len = groups.length;
    //disable enable for the system row
    //disenDates(rowId,cellInd);
    status_1_g = gridObjGroup.getColIndexById("GrpStatus");
    for(var i=0;i<len;i++)
    {	
    	if(gridObjGroup.cellById(groups[i],status_1_g).getValue()!="Approved"){
    		gridObjGroup.cellById(groups[i],cellInd).setValue(date);
    	}
    	//disenDates(groups[i],cellInd); //disable enable for child rows
    }
}

function fnApplyDemoChkBox(rowId){
	 status_1_g = gridObjGroup.getColIndexById("GrpStatus");
	 DemoFl_14_g = gridObjGroup.getColIndexById("DemoFL");
	var chngVal = gridObjGroup.cellById(rowId, DemoFl_14_g).getValue();

	var groups = gridObjGroup.getSubItems(rowId).split(",");
    var len = groups.length;
    for(var i=0;i<len;i++)
    {	
    	if(gridObjGroup.cellById(groups[i],status_1_g).getValue()!="Approved"){
    		gridObjGroup.cellById(groups[i],DemoFl_14_g).setValue(chngVal);
    	}
    }
}
function fnApplySystemChanges(rowId) {
	 
	 status_1_g = gridObjGroup.getColIndexById("GrpStatus");
	 type_6_g = gridObjGroup.getColIndexById("type");
	 change_7_g = gridObjGroup.getColIndexById("change");
	 adjusted_8_g = gridObjGroup.getColIndexById("adjusted");
	 
	var chngType = gridObjGroup.cellById(rowId, type_6_g).getValue();
	var chngVal = gridObjGroup.cellById(rowId, change_7_g).getValue();
	if(!validateChangeValue(chngVal))
	{
		gridObjGroup.cellById(rowId,change_7_g).setValue('');
		return false;
	}
	
	if(chngType!='' && confirm(message_sales[81]))
	{
 		var groups = gridObjGroup.getSubItems(rowId).split(",");
        var len = groups.length;
        for(var i=0;i<len;i++)
        {	
        	if(gridObjGroup.cellById(groups[i],status_1_g).getValue()!="Approved"){
        		gridObjGroup.cellById(groups[i],type_6_g).setValue(chngType);
        		if(chngVal!=''){
        			gridObjGroup.cellById(groups[i],change_7_g).setValue(chngVal);
        			
        		}
        		
        		fnCalcAdjPrice(groups[i],type_6_g,change_7_g,adjusted_8_g);
        		fnApplyApprLevel(groups[i]);
        		onOpenStartTreeItem(groups[i],'YES');
        	}

			 if(gridObjGroup.getOpenState(groups[i]))
			{
				 onOpenStartTreeItem(groups[i],'YES');
			}
        }
    }
	
 }
 function fnApplyDemoSystemChanges(rowId) {
	 status_1_g = gridObjGroup.getColIndexById("GrpStatus");
	 dtype_15_g = gridObjGroup.getColIndexById("hdtype");
	 dchange_16_g = gridObjGroup.getColIndexById("dchange");
	 dprice_17_g = gridObjGroup.getColIndexById("dprice");

	    var chngType = gridObjGroup.cellById(rowId, dtype_15_g).getValue();
		var chngVal = gridObjGroup.cellById(rowId, dchange_16_g).getValue();
		if(!validateChangeValue(chngVal))
		{
			gridObjGroup.cellById(rowId,dchange_16_g).setValue('');
			return false;
		}
		if(chngType!='' && confirm(message_sales[302]))
		{
	 		var groups = gridObjGroup.getSubItems(rowId).split(",");
	        var len = groups.length;
	        //alert('len->'+len);
	        for(var i=0;i<len;i++)
	        {	
	        	if(gridObjGroup.cellById(groups[i],status_1_g).getValue()!="Approved"){
		        	 gridObjGroup.cellById(groups[i],dtype_15_g).setValue(chngType);
		        	 gridObjGroup.cellById(groups[i],dchange_16_g).setValue(chngVal);	
		        	 fnCalcAdjPrice(groups[i],dtype_15_g,dchange_16_g,dprice_17_g);
	        	}
				 
	        }
	    }
 }

function fnCalcAdjPrice(rowId,chgtype,change,adjprice) {
	fnApplyApprLevel(rowId);
	list_4_g = gridObjGroup.getColIndexById("list");
	current_5_g = gridObjGroup.getColIndexById("current");
	discount_9_g = gridObjGroup.getColIndexById("discount");
		var chng = gridObjGroup.cellById(rowId,change).getValue();		
		if(!validateChangeValue(chng))
		{
			gridObjGroup.cellById(rowId,change).setValue('');
			return false;
		}
		var chngType = gridObjGroup.cellById(rowId,chgtype).getValue();
		var curr = gridObjGroup.cellById(rowId,current_5_g).getValue();
		var list = gridObjGroup.cellById(rowId,list_4_g).getValue();
		var isChanged = false;
		
		gridObjGroup.cellById(rowId, discount_9_g).setValue('');
		if(chngType==52020)
		{			
			if(chng !='')
				gridObjGroup.cellById(rowId, adjprice).setValue(Math.round(chng));
			else
				gridObjGroup.cellById(rowId, adjprice).setValue('');
			// MNTTASK - 5171.
			if(list != null && list != '' && list != 0 && chng !=''){
				var discountVal =  Math.round(100-(chng/list*100));
				if(discountVal == 0)
					gridObjGroup.cellById(rowId, discount_9_g).setValue(discountVal+'%');
				else
					gridObjGroup.cellById(rowId, discount_9_g).setValue(discountVal);
			}
			 isChanged = true;
		}
		else if(chngType==52021)
		{
		
			gridObjGroup.cellById(rowId, adjprice).setValue(Math.round(list - list * chng *0.01));
			isChanged = true;
		}
		else if(chngType==52022)
		{
			curr = (curr=='' || curr==0)?list:curr;
			gridObjGroup.cellById(rowId, adjprice).setValue(Math.round(curr - curr * chng *0.01));
			isChanged = true;
		}
		else if(chngType==52023)
		{
			curr = (curr=='' || curr==0)?list:curr;
			gridObjGroup.cellById(rowId, adjprice).setValue(Math.round(curr + curr * chng *0.01));
			isChanged = true;
		}
		else if(chngType==52024)
		{			
			gridObjGroup.cellById(rowId, change).setValue('');
			
			curr = (curr=='' || curr==0)?list:curr;
			
			gridObjGroup.cellById(rowId, adjprice).setValue('');			 
			gridObjGroup.setColumnExcellType(change,"ro");
			//isChanged = true;
		}
		if(isChanged)
		{
			gridObjGroup.setColumnExcellType(change,"edn");			
		}
		return true;
		
}


function twChkRenderer(rowId){
	adjusted_5_a = gridObjAnalysis.getColIndexById("adjusted");
	tw_6_a = gridObjAnalysis.getColIndexById("tw");
	twcomp_7_a = gridObjAnalysis.getColIndexById("twcomp");
	var adj = gridObjAnalysis.cellById(rowId, adjusted_5_a).getValue();
	var tw = gridObjAnalysis.cellById(rowId, tw_6_a).getValue();
	var level = gridObjAnalysis.getLevel(rowId);
	//alert('rowId : '+rowId+' level: '+level+' adj: '+adj+' tw: '+tw);
	if(level>0 && adj!='' && adj<tw)
	{
		gridObjAnalysis.cellById(rowId, twcomp_7_a).setValue('<span style="color:red;font-weight:bold;">NO</span>');
	}
	else if(level>0 && adj!='')
	{
		gridObjAnalysis.cellById(rowId, twcomp_7_a).setValue('<span style="color:green;font-weight:bold;">YES</span>');
	}
	else
	{
		gridObjAnalysis.cellById(rowId, twcomp_7_a).setValue('');
	}
	
}
function fnGetSelectionIds(level){
	var strSelectionIds = '';
	gridObjGroup.forEachRow( function(id){
		if(gridObjGroup.getLevel(id)==level)
		{
			strSelectionIds = strSelectionIds+","+id;
		}
	});
	if(strSelectionIds!='')
	{
		strSelectionIds = strSelectionIds.substring(1,strSelectionIds.length);
	}
	return strSelectionIds;
}
function onOpenStartTreeItem(id,checkFlag){
	//fnApplyApprLevel(id);

	status_1_g = gridObjGroup.getColIndexById("GrpStatus");
	 select_2_g = gridObjGroup.getColIndexById("select");
	 type_6_g = gridObjGroup.getColIndexById("type");
	 change_7_g = gridObjGroup.getColIndexById("change");
	 adjusted_8_g = gridObjGroup.getColIndexById("adjusted");
	 if(gridObjGroup.getLevel(id)==1)
	{
		var chngType = gridObjGroup.cellById(id, type_6_g).getValue();
		var chngVal = gridObjGroup.cellById(id, change_7_g).getValue();
		//alert("change value"+chngVal);
		var parts = gridObjGroup.getSubItems(id).split(",");
        var len = parts.length;
        var preType="";
        if(len==1 && parts[0]=='')return true;
        for(var i=0;i<len;i++)
        {		
        	//added for task 5169
         if( gridObjGroup.cellById(parts[i],status_1_g).getValue()!="Approved"){    	
    	  	  if(chngType != null && chngType != '' && chngType != '0'){
    	    	  gridObjGroup.cellById(parts[i],type_6_g).setValue(chngType);
  	     	      gridObjGroup.cellById(parts[i],change_7_g).setValue(chngVal);	
    	  }
    	  if(gridObjGroup.cellById(parts[i],select_2_g ).isChecked()!=true)
	        	{
    		   		 fnCalcAdjPrice(parts[i],type_6_g,change_7_g,adjusted_8_g);
		        	//added for task 5169
		        	 if(checkFlag == "YES"  && chngType != ''){
		         		gridObjGroup.cellById(parts[i],select_2_g).setChecked(true);
		         		}
	        	}
				else
				{  
					 fnCalcAdjPrice(parts[i],type_6_g,change_7_g,adjusted_8_g);
				}
        	}
   	         	 
       	 fnApplyApprLevel(parts[i]);
       	 
        }
       
        
    }
    return true;
}
function setTreeItem(id){
	dtype_15_g = gridObjGroup.getColIndexById("hdtype");
	dchange_16_g = gridObjGroup.getColIndexById("dchange");
	dprice_17_g = gridObjGroup.getColIndexById("dprice");
	 
	if(gridObjGroup.getLevel(id)==1)
	{
		var chngType = gridObjGroup.cellById(id, dtype_15_g).getValue();
		var chngVal = gridObjGroup.cellById(id, dchange_16_g).getValue();
 		var parts = gridObjGroup.getSubItems(id).split(",");
        var len = parts.length;
        if(len==1 && parts[0]=='')return true;
        for(var i=0;i<len;i++)
        {	
        	      	
        		 gridObjGroup.cellById(parts[i],dtype_15_g).setValue(chngType);
	        	 gridObjGroup.cellById(parts[i],dchange_16_g).setValue(chngVal);	
	        	 fnCalcAdjPrice(parts[i],dtype_15_g,dchange_16_g,dprice_17_g);
        }
    }
    return true;
}


function fnUpdateGrpConstPrice(){	
	gridObjAnalysis.forEachRow( function(id){
		if(gridObjAnalysis.getLevel(id)==1)
		{
			fnUpdateConstructPrice(id);
		}
	
	});
}


function fnUpdateConstructPrice(id){
   	var listPriceConstruct = 0;
   	var currPriceConstruct = 0;
   	var adjPriceConstruct = 0;	
	var twPriceConstruct = 0;
	var padjPriceConstruct = 0;
	var discountConstruct = '';
	var discountCol ='';
	
	qty_2_a = gridObjAnalysis.getColIndexById("qty");
	list_3_a = gridObjAnalysis.getColIndexById("list");
	current_4_a = gridObjAnalysis.getColIndexById("current");
	adjusted_5_a = gridObjAnalysis.getColIndexById("adjusted");
	tw_6_a = gridObjAnalysis.getColIndexById("tw");
	adjustedconst_5_b = gridObjAnalysis.getColIndexById("padjusted");
	discountCol = gridObjAnalysis.getColIndexById("discount");
	
	var qty=0,listprice=0,cur=0, adj=0, tw=0, padj=0;
		gridObjAnalysis._h2.forEachChild(id,function(element){
		if(element.parent.id == id)
		{
			qty = fnGetPriceNum(gridObjAnalysis.cellById(element.id,qty_2_a).getValue());
			listprice = fnGetPriceNum(gridObjAnalysis.cellById(element.id,list_3_a).getValue());
			cur = fnGetPriceNum(gridObjAnalysis.cellById(element.id,current_4_a).getValue());
			adj = fnGetPriceNum(gridObjAnalysis.cellById(element.id,adjusted_5_a).getValue());
			tw = fnGetPriceNum(gridObjAnalysis.cellById(element.id,tw_6_a).getValue());	
			padj = fnGetPriceNum(gridObjAnalysis.cellById(element.id,adjustedconst_5_b).getValue());			
			if(adj=='' || adj==0){
				if(cur==''){
					cur=0;
				}
				//adj = cur;// Commented Adj Price by velu for MNTTASK-4967, Because when initiate the price i will not give initiate all the groups. But displaying adj price.
			}
			
			//alert('qty: '+qty+' curr: '+cur+' list: '+listprice+' adj: '+adj+' tw: '+tw+' vp: '+vp+' ad: '+ad);
			
			listPriceConstruct = listPriceConstruct+qty*listprice;
			currPriceConstruct = currPriceConstruct+qty*cur;			
			twPriceConstruct = twPriceConstruct+qty*tw;
			adjPriceConstruct = adjPriceConstruct+qty*adj; 
			padjPriceConstruct = padjPriceConstruct+padj*1;
			
			var constantPrc ='';// adjPriceConstruct;
			//if Proposed price is null taking current price
			if(constantPrc == '' || constantPrc =='0'){
				 constantPrc = currPriceConstruct;
			}
			//if both current and Proposed price is null setting discount to null
			if(constantPrc == '' || constantPrc =='0'){
				discountConstruct = '';
			}
			//caluclating Discount
			if(listPriceConstruct != null && listPriceConstruct != '' && listPriceConstruct != 0 
					&& constantPrc !='' && constantPrc !='0'){
				discountConstruct =  Math.round(100-(padjPriceConstruct/listPriceConstruct*100));
			}
				
			// to set the group values null if its 0
			gridObjAnalysis.cellById(element.id,list_3_a).setValue(listprice==0?'':listprice);
			gridObjAnalysis.cellById(element.id,current_4_a).setValue(cur==0?'':cur);
			gridObjAnalysis.cellById(element.id,adjusted_5_a).setValue(adj==0?'':adj); 
			gridObjAnalysis.cellById(element.id,tw_6_a).setValue(tw==0?'':tw);
		}
    });
	//alert('listPriceConstruct: '+listPriceConstruct+' currPriceConstruct: '+currPriceConstruct+' adjPriceConstruct: '+adjPriceConstruct+' twPriceConstruct: '+twPriceConstruct+' vpPriceConstruct: '+vpPriceConstruct+' adPriceConstruct: '+adPriceConstruct);
	gridObjAnalysis.cellById(id,list_3_a).setValue(listPriceConstruct==0?'':listPriceConstruct);
	gridObjAnalysis.cellById(id,current_4_a).setValue(currPriceConstruct==0?'':currPriceConstruct);
	gridObjAnalysis.cellById(id,adjusted_5_a).setValue(adjPriceConstruct==0?'':adjPriceConstruct); 
	gridObjAnalysis.cellById(id,adjustedconst_5_b).setValue(padjPriceConstruct==0?'':padjPriceConstruct); 
	gridObjAnalysis.cellById(id,discountCol).setValue((discountConstruct=='' || discountConstruct < 0)?'':discountConstruct); 
	gridObjAnalysis.cellById(id,tw_6_a).setValue(twPriceConstruct==0?'':twPriceConstruct);
	
	twChkRenderer(id);
}
function fnLoadAcct(obj) {
	document.frmPricingRequestForm.haction.value = 'acntChange';
	document.frmPricingRequestForm.action =  '/gmPricingRequestDAction.do?method=loadPriceRequest';
	document.frmPricingRequestForm.submit();
}

function fnValidateVal(src,bstatus){

	Error_Clear();
	var val = document.frmPricingRequestForm.strAccountId;
	 select_2_g = gridObjGroup.getColIndexById("select");
	 name_3_g = gridObjGroup.getColIndexById("Name");
	 adjusted_8_g = gridObjGroup.getColIndexById("adjusted");
	 dtype_12_g = gridObjGroup.getColIndexById("dtype");
	 effDate_13_g = gridObjGroup.getColIndexById("effDate");
	 DemoFl_14_g = gridObjGroup.getColIndexById("DemoFL");
	 dprice_17_g = gridObjGroup.getColIndexById("dprice");
	 fDate_18_g = gridObjGroup.getColIndexById("fDate");
	 tDate_19_g = gridObjGroup.getColIndexById("tDate");
	 
	if(val != undefined && val.value==0)
	{
		Error_Details(message_sales[303]);		
	}
	if(gridObjGroup.getRowsNum()==0 && reqstatus=='' && src!='add') 
	{
		Error_Details(message_sales[304]);	
	}
	if(reqstatus=='' && src!='add')
	{
			
		var nochange = true;
		gridObjGroup.forEachRow(function(id)
		{
			if(gridObjGroup.getLevel(id)==1 || gridObjGroup.getLevel(id)==2)
			{
				if(gridObjGroup.cellById(id,adjusted_8_g).getValue()!='')
				{
					nochange=false;
				}
			}
		});
		if(nochange)
		{
			Error_Details(message_sales[305]);	
		}
	}
	/* validation from demo to date can't be greater than effective date*/
	if(src=='save' && bstatus=='Submit'){
		var strSaveValues = '';
		var strFromDate ='';
		var strToDate = '';
		var strEffectiveType ='';
		var strEffectiveDate = '';
		var strDemoFlg = '';
		var strDemoPrice = '';
		var strDType = '';
		var strDChange = '';
		var strAdjPri = 0;
		var strDemoadj = 0;
		var strMsgET = '';
		var strMsgED = '';
		var strMsgEDP = '';
		var strMsgDP = '';
		var strMsgDD = '';
		var strMsgDFDT = '';
		var strMsgDTED = '';
		gridObjGroup.forEachRow( function(id){
			
			if(gridObjGroup.getLevel(id)==1 || (gridObjGroup.getLevel(id)==3 && gridObjGroup.cellById(id,select_2_g).isChecked()==true))
			{
				if(gridObjGroup.getLevel(id)==1){
					strEffectiveType = gridObjGroup.cellById(id,dtype_12_g).getValue();
					strEffectiveDate = gridObjGroup.cellById(id,effDate_13_g).getValue();
					strDemoFlg = gridObjGroup.cellById(id,DemoFl_14_g).getValue();
					strFromDate = gridObjGroup.cellById(id,fDate_18_g).getValue();
					strToDate = gridObjGroup.cellById(id,tDate_19_g).getValue();
					strAdjPri = gridObjGroup.cellById(id,adjusted_8_g).getValue();
					strDemoadj = gridObjGroup.cellById(id,dprice_17_g).getValue();
				}else{
					strEffectiveType = gridObjGroup.cellById(id,dtype_12_g).getValue();
					strEffectiveDate = gridObjGroup.cellById(id,effDate_13_g).getValue();
					strAdjPri = gridObjGroup.cellById(id,adjusted_8_g).getValue();
				}
				
				if(strAdjPri > 0){
					if(strEffectiveType==''){
						strMsgET = strMsgET + '<LI>' + gridObjGroup.cellById(id,name_3_g).getValue();
					}
					else if(strEffectiveType==53001){  // if selected type is as on
						if(strEffectiveDate==''){
							strMsgED = strMsgED + '<LI>' + gridObjGroup.cellById(id,name_3_g).getValue();
						}
						else{                //if effective date is present and less than sysdate
							var daydiff = dateDiff(currDate,strEffectiveDate);
							if(daydiff<0){
								strMsgEDP = strMsgEDP + '<LI>' + gridObjGroup.cellById(id,name_3_g).getValue();
							}
							
						}  // end of effective date is present and less than sysdate
					} //end of selected type is as on
				}
				if(strDemoFlg==1){
					if(strDemoadj<=0 || strDemoadj==''){
						strMsgDP = strMsgDP + '<LI>' + gridObjGroup.cellById(id,name_3_g).getValue();
						
					}else if(strFromDate=='' || strToDate==''){
						strMsgDD = strMsgDD + '<LI>' + gridObjGroup.cellById(id,name_3_g).getValue();
					}else{
						var daydiff = dateDiff(strFromDate,strToDate);
						if(daydiff<0){
							strMsgDFDT = strMsgDFDT + '<LI>' + gridObjGroup.cellById(id,name_3_g).getValue();
						}
						var effdiff = dateDiff(strToDate,strEffectiveDate);
						if(effdiff<=0){
							strMsgDTED = strMsgDTED + '<LI>' + gridObjGroup.cellById(id,name_3_g).getValue();
						}
					}
				}
			}
		});
		if(strMsgET.length > 0){
			Error_Details(message_sales[306]+strMsgET);
		}
		if(strMsgED.length > 0){
			Error_Details(message_sales[307]+strMsgED);
		}
		if(strMsgEDP.length > 0){
			Error_Details(message_sales[308]+strMsgEDP);
		}
		if(strMsgDP.length > 0){
			Error_Details(message_sales[309]+strMsgDP);
		}
		if(strMsgDD.length > 0){
			Error_Details(message_sales[310]+strMsgDD);
		}
		if(strMsgDFDT.length > 0){
			Error_Details(message_sales[311]+strMsgDFDT);
		}
		if(strMsgDTED.length > 0){
			Error_Details(message_sales[312]+strMsgDTED);
		}
		
		
		//need to check whether part has demo price or not
		
	}
	if (ErrorCount > 0)
	{
		
			Error_Show();
			Error_Clear();
			return false;
	}else{
		errorMsg = '';
		document.getElementById("msg").innerHTML = errorMsg;
	}
	return true;
}

function fnDeleteSystem(rowId){
	removeSysIds = removeSysIds + ',' + rowId;
	gridObjGroup.deleteRow(rowId);
	grpGridChanged = true;
	var arrAllSystem = document.frmPricingRequestForm.checkSystems;	
	var arrAllLen = arrAllSystem.length;
	for (var i=0;i< arrAllLen;i++ )
	{
		if (arrAllSystem[i].checked==true && arrAllSystem[i].value ==rowId)
		{		
			arrAllSystem[i].checked=false;
		}
	}
}
function fnUnClearDiv(){	
	document.getElementById("processimg").style.display='none';
	document.getElementById("Btn_Add").disabled=false;
	document.getElementById("Btn_UnPrcSys").disabled=false;
	document.getElementById("Btn_UnPrcGrp").disabled=false;
}
function fnClearDiv(){	
	document.getElementById("processimg").style.display='block';
	document.getElementById("Btn_Add").disabled=true;
	document.getElementById("Btn_UnPrcSys").disabled=true;	
	document.getElementById("Btn_UnPrcGrp").disabled=true;	
	}

function fnUnCheckSystem(){
	var arrAllSystem = document.frmPricingRequestForm.checkSystems;	
	var arrAllLen = arrAllSystem.length;
	for (var i=0;i< arrAllLen;i++ )
	{

		if (arrAllSystem[i].checked)
		{				
			arrAllSystem[i].checked = false;
		}
	}
}
function fnAddSytems(){

	if(!fnValidateVal('add')) return;
	var arrAllSystem = document.frmPricingRequestForm.checkSystems;	
	var arrAllLen = arrAllSystem.length;	
	var gpoID = '';
	var systemInputStr='';
	var intErrorCount = 0;
	var flag = false;
	for (var i=0;i< arrAllLen;i++ )
	{

		if (arrAllSystem[i].checked)
		{				
			if(gridObjGroup.getRowId(gridObjGroup.getRowIndex(arrAllSystem[i].value))==undefined)
			{
				fnClearDiv();
				systemInputStr=systemInputStr+','+arrAllSystem[i].value;
				flag = true;
			}
			}else{
			intErrorCount++;
			}
	}
	if(systemInputStr == ''){
		 fnUnCheckSystem();
		 dhxLayout.cells("b").collapse();
		 dhxLayout.cells("a").collapse();
	}
	 //Added for BUG-320	
	if(intErrorCount == arrAllLen){
		 dhxLayout.cells("a").collapse();
		 dhxLayout.cells("b").expand();
		 Error_Details(message_sales[313]);	
		 Error_Show();
     	 Error_Clear();
     	 return false;
   }
	if(arrAllSystem !=undefined)
	{
		systemInputStr = arrAllLen==undefined?arrAllSystem.value:systemInputStr;
	}
	if(systemInputStr!='')
	{
		systemInputStr = systemInputStr.substring(1,systemInputStr.length);
		//var effectiveDate = document.frmPricingRequestForm.effectiveDate.value;		
		
/*
 		var val = document.frmPricingRequestForm.strAccountId;
 		var strAccountId;
		if(val!=undefined)
		{
			strAccountId = val.value;
		}	
*/		
		if(document.frmPricingRequestForm.strAccType.value == 903108){
			gpoID = document.frmPricingRequestForm.strGroupAcc.value;
		}

		//var loader = dhtmlxAjax.getSync('/gmPricingRequestDAction.do?method=getSysReqDetails&strRequestId='+strRequestId+'&systemInputStr='+systemInputStr+'&strAccountId='+strAccountId+'&strGroupAcc='+gpoID+'&ramdomId='+Math.random());
		//fnAddSystemData(loader);
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmPricingRequestDAction.do?method=getSysReqDetails&strRequestId='+strRequestId+'&systemInputStr='+systemInputStr+'&strAccountId='+strAccountId+'&strGroupAcc='+gpoID+'&ramdomId='+Math.random());
		var loader = dhtmlxAjax.get(ajaxUrl,fnAddSystemData);		
		grpGridChanged = true;		
	}
	
}

function fnAddSystemData(loader){
	//alert(' COMING TO fnAddSystemData');
	var arrAllSystem = document.frmPricingRequestForm.checkSystems;	
	var arrAllLen = arrAllSystem.length;
	if (loader.xmlDoc.responseXML != null);
	 {	
		 var isFirstSys = false;
		 if(gridObjGroup.getRowsNum()==0)
		{
			isFirstSys=true;
		}
		if(gridObjGroup.getRowsNum()>0)
		{
			gridObjGroup._refresh_mode=[true,true,false];
		}
		//alert('>>>>>>>'+loader.doSerialization());
		gridObjGroup.loadXMLString(loader.doSerialization());
		if(tabbar.getActiveTab()!="group")
		{
			tabbar.setTabActive("group");
		}
		if(isFirstSys)
		{
			gridObjGroup.attachHeader("<label><input type='checkbox' value='no'  onClick='javascript:fnChangedFilter(this);'/>Show Changed</label>,#cspan,#cspan,#cspan,#cspan" +
					",#cspan,#cspan,#cspan,#cspan,#cspan,#cspan,#cspan,#cspan,#cspan,#cspan,#cspan,#cspan,#cspan,#cspan,#cspan,#cspan,#cspan,#cspan,#cspan,#cspan,#cspan");
		}
		
		document.getElementById("idSave").disabled=false;
		
     }
	 fnUnClearDiv();
	 fnUnCheckSystem();
	 dhxLayout.cells("b").collapse();
	 dhxLayout.cells("a").collapse();
	}
	
function fnActiveStatus(rowId){
	lvlstrid_9_a = gridObjAnalysis.getColIndexById("lvlstrid");
	ActiveFlg_23_a = gridObjAnalysis.getColIndexById("ActiveFlg"); 
	var pubFl = 'N';
		gridObjAnalysis.forEachRow( 
			function(id){
				if(gridObjAnalysis.getLevel(id)>=1 && gridObjAnalysis.cellById(id,lvlstrid_9_a).getValue()==rowId){
					if(gridObjAnalysis.cellById(id,ActiveFlg_23_a).isChecked()){
						pubFl = 'Y';
					}
				}
		});
		//alert('pubFl:::1::'+pubFl);
		return pubFl;
}
function fnSaveValues(rstatus,bstatus,statusFl){
	//alert('fnsave'+bstatus+"REQID::");
	var accGPO = document.frmPricingRequestForm.strAccGPO.value;
	//alert("acc"+accGPO);
	var strSaveValues = '';
	var strFromDate ='';
	var strToDate = '';
	var strEffectiveType ='';
	var strEffectiveDate = '';
	var strDemoFlg = '';
	var strDemoPrice = '';
	var strDType = '';
	var strDChange = '';
    var strPubFl = '';
        //alert('strToDate ');
    gridObjGroup.forEachRow( function(id){
		if(gridObjGroup.getLevel(id)==0){				
		}
		if(gridObjGroup.getLevel(id)==1)
		{
		//alert(gridObjGroup.cellById(id,12).getValue());
		//if(gridObjGroup.cellById(id,12).getValue()=='Y'){
		//	alert('4---->'+gridObjGroup.cellById(id,5).getValue());
		//	Error_Details(" Please enter valid number for <b>Change Value</b>");		
			
		//}
			 status_1_g = gridObjGroup.getColIndexById("GrpStatus");
			 select_2_g = gridObjGroup.getColIndexById("select");
			 type_6_g = gridObjGroup.getColIndexById("type");
			 change_7_g = gridObjGroup.getColIndexById("change");
			 adjusted_8_g = gridObjGroup.getColIndexById("adjusted");
			 dtype_12_g = gridObjGroup.getColIndexById("dtype");
			 effDate_13_g = gridObjGroup.getColIndexById("effDate");
			 DemoFl_14_g = gridObjGroup.getColIndexById("DemoFL");
			 dtype_15_g = gridObjGroup.getColIndexById("hdtype");
			 dchange_16_g = gridObjGroup.getColIndexById("dchange");
			 dprice_17_g = gridObjGroup.getColIndexById("dprice");
			 fDate_18_g = gridObjGroup.getColIndexById("fDate");
			 tDate_19_g = gridObjGroup.getColIndexById("tDate");
			 appLvl_10_g = gridObjGroup.getColIndexById("AppLvl");
			var appSts ='';
			strAdjusted = gridObjGroup.cellById(id,adjusted_8_g).getValue();
			strStatus = gridObjGroup.cellById(id,status_1_g).getValue();
			strChangeType = gridObjGroup.cellById(id,type_6_g).getValue();
			//if(userdataacl >2){
			appSts = fnGetApproveStatus(rstatus, id,bstatus);
			
			//alert('appSts::'+appSts+":::"+strChangeType);
			if(appSts==''){
				if((strChangeType==52020||strChangeType==52021||strChangeType==52022||strChangeType==52023) && strAdjusted > 0){
					if(bstatus=='Submit'  || bstatus=='Save'){
						if(strStatus=='Pending'){
							appSts = 52027;
						}else if(strStatus=='Approved'){
							appSts = 52028;
						}else if(strStatus=='Denied'){
							appSts = 52029;
						}
						if( bstatus=='Save'){
							appSts = 52186;
						}
						if( bstatus=='Submit'){
							appSts = 52027;
						}
					}
				}
			}
			var change;// = gridObjGroup.cellById(id,7).getValue();
			if(gridObjGroup.cellById(id,type_6_g).getValue()==52024){
				change = '';
			}else{
				change = gridObjGroup.cellById(id,change_7_g).getValue();
			}
			strEffectiveType = gridObjGroup.cellById(id,dtype_12_g).getValue();
			strEffectiveDate = gridObjGroup.cellById(id,effDate_13_g).getValue();
			strDemoFlg = gridObjGroup.cellById(id,DemoFl_14_g).getValue();
			strDType = gridObjGroup.cellById(id,dtype_15_g).getValue();
			if(gridObjGroup.cellById(id,dtype_15_g).getValue()==52024){
				strDChange = '';
			}else{
				strDChange = gridObjGroup.cellById(id,dchange_16_g).getValue();
			}	
			strDemoPrice = gridObjGroup.cellById(id,dprice_17_g).getValue();
			strFromDate = gridObjGroup.cellById(id,fDate_18_g).getValue();
			strToDate = gridObjGroup.cellById(id,tDate_19_g).getValue();
			strAppLvl = gridObjGroup.cellById(id,appLvl_10_g).getValue();
			
			if(strAppLvl=='AD')
				strAppLvlVal = 52200;
			else if(strAppLvl=='PC')
				strAppLvlVal = 52201;
			else
				strAppLvlVal = 0;
			
			/*
			 * 903108 : Group Account
			 * 903107 : Account
			 * 52028  : Approved
			 */
			// If the request is for GPO, the active flag should not be set to 'Y' by default.
			
			if((contfl=='YES' || reqType == '903108') || (reqType == '903107' && accGPO!='' && accGPO!=0 && (userdesc=='AD' || userdesc=='VP'))) //GPO
				strPubFl = 	fnActiveStatus(id);
			else if(appSts=='52028' && contfl=='NO')
				strPubFl = 'Y';
			else
				strPubFl = '';
			//alert('appSts ' +appSts + ' strPubFl '+strPubFl );
			
			/*
			 * Add a condition to check if the "Approve Do not Implement" check box is checked, if yes, then set strPubFl='N'.
			 */
			if(strPubFl=='Y' &&
			  (document.frmPricingRequestForm.chk_DntImplm!=undefined &&
			   document.frmPricingRequestForm.chk_DntImplm.checked)){
				strPubFl = 'N';
			}
			
			if(statusFl==false && appSts!=52029){
					//alert('appSts'+appSts+'statusFl::'+statusFl);
					strSaveValues = strSaveValues+id+'^'+'52000^'+gridObjGroup.cellById(id,type_6_g).getValue()+'^'+change+'^'+
							gridObjGroup.cellById(id,adjusted_8_g).getValue()+'^'+appSts+'^'+strEffectiveType+'^'+strEffectiveDate+'^'+
							strDemoFlg+'^'+strDType+'^'+strDChange+'^'+strDemoPrice+'^'+strFromDate+'^'+strToDate+'^'+strPubFl+'^'+strAppLvlVal+'|';
				}
				if(statusFl==true && appSts==52029){
					//alert('appSts'+appSts+'statusFl::'+statusFl);
					strSaveValues = strSaveValues+id+'^'+'52000^'+gridObjGroup.cellById(id,type_6_g).getValue()+'^'+change+'^'+
							gridObjGroup.cellById(id,adjusted_8_g).getValue()+'^'+appSts+'^'+strEffectiveType+'^'+strEffectiveDate+'^'+
							strDemoFlg+'^'+strDType+'^'+strDChange+'^'+strDemoPrice+'^'+strFromDate+'^'+strToDate+'^'+strPubFl+'^'+strAppLvlVal+'|';
				}
				
			}

	});
	//need to check whether part has demo price or not
	gridObjGroup.forEachRow( function(id){
		appLvl_10_g = gridObjGroup.getColIndexById("AppLvl");
		if(gridObjGroup.getLevel(id)==1){
			
			strDemoFlg = gridObjGroup.cellById(id,DemoFl_14_g).getValue();
			strDType = gridObjGroup.cellById(id,dtype_15_g).getValue();
			strDChange = gridObjGroup.cellById(id,dchange_16_g).getValue();
			strDemoPrice = gridObjGroup.cellById(id,dprice_17_g).getValue();
			strFromDate = gridObjGroup.cellById(id,fDate_18_g).getValue();
			strToDate = gridObjGroup.cellById(id,tDate_19_g).getValue();
		}
		if(gridObjGroup.getLevel(id)==2 && gridObjGroup.cellById(id,select_2_g).isChecked()==true)
		{
			//alert('getLevel'+gridObjGroup.getLevel(id)+'isChecked::'+gridObjGroup.cellById(id,2).isChecked());
			var appSts ='';
			strAdjusted = gridObjGroup.cellById(id,adjusted_8_g).getValue();
			strStatus = gridObjGroup.cellById(id,status_1_g).getValue();
			//alert('strAdjusted::'+strAdjusted);
			strChangeType = gridObjGroup.cellById(id,type_6_g).getValue();
			//if(userdataacl >2){
			appSts = fnGetApproveStatus(rstatus, id,bstatus);
			
			if(appSts==''){
				if((strChangeType==52020||strChangeType==52021||strChangeType==52022||strChangeType==52023) && strAdjusted > 0){
					if(bstatus=='Submit'  || bstatus=='Save'){
						if(strStatus=='Pending'){
							appSts = 52027;
						}else if(strStatus=='Approved'){
							appSts = 52028;
						}else if(strStatus=='Denied'){
							appSts = 52029;
						}
						if( bstatus=='Save'){
							appSts = 52186;
						}
						if( bstatus=='Submit'){
							appSts = 52027;
						}
					}
				}
			}
			
			strEffectiveType = gridObjGroup.cellById(id,dtype_12_g).getValue();
			strEffectiveDate = gridObjGroup.cellById(id,effDate_13_g).getValue();
			
			var change = gridObjGroup.cellById(id,change_7_g).getValue();
			strAdjusted = gridObjGroup.cellById(id,adjusted_8_g).getValue();
			strChangeType = gridObjGroup.cellById(id,type_6_g).getValue();
			strAppLvl = gridObjGroup.cellById(id,appLvl_10_g).getValue();
			
			strAppLvlVal = 0;
			//alert('strAppLvl::'+strAppLvl);
			if(strAppLvl=='AD')
				strAppLvlVal = 52200;
			else if(strAppLvl=='PC')
				strAppLvlVal = 52201;
			else
				strAppLvlVal = 0;
			
			/*
			 * 903108 : Group Account
			 * 903107 : Account
			 * 52028  : Approved
			 */
			if((contfl=='YES' || reqType == '903108') || (reqType == '903107' && accGPO!='' && accGPO!=0 && (userdesc=='AD' || userdesc=='VP'))) //GPO
				strPubFl = fnActiveStatus(id);
			else if(appSts=='52028' && contfl=='NO')
				strPubFl = 'Y';
			else
				strPubFl = '';
			
			/*
			 * Add a condition to check if the "Approve Do not Implement" check box is checked, if yes, then set strPubFl='N'.
			 */
			if(strPubFl=='Y' &&
			   (document.frmPricingRequestForm.chk_DntImplm!=undefined &&
			   document.frmPricingRequestForm.chk_DntImplm.checked)){
				strPubFl = 'N';
			}
			
			if(statusFl==false && appSts!=52029){
				strSaveValues = strSaveValues+id+'^'+'52001^'+gridObjGroup.cellById(id,type_6_g).getValue()+'^'+change+'^'+
					gridObjGroup.cellById(id,adjusted_8_g).getValue()+'^'+appSts+'^'+strEffectiveType+'^'+strEffectiveDate+'^'+
					strDemoFlg+'^'+strDType+'^'+strDChange+'^'+strDemoPrice+'^'+strFromDate+'^'+strToDate+'^'+strPubFl+'^'+strAppLvlVal+'|';
			}
			if(statusFl==true && appSts==52029){
				//appSts = 52186; // MNTTASK-6115. When do denied parts should be displayed in PricingBy Tab and Appr Tab. 
				strSaveValues = strSaveValues+id+'^'+'52001^'+gridObjGroup.cellById(id,type_6_g).getValue()+'^'+change+'^'+
						gridObjGroup.cellById(id,adjusted_8_g).getValue()+'^'+appSts+'^'+strEffectiveType+'^'+strEffectiveDate+'^'+
						strDemoFlg+'^'+strDType+'^'+strDChange+'^'+strDemoPrice+'^'+strFromDate+'^'+strToDate+'^'+strPubFl+'^'+strAppLvlVal+'|';
			}
		}
	});
	
	return strSaveValues;
}

function fnGetApproved(bstatus,strAdjusted,strStatus){
	var appSts=''
	 if(bstatus=='Submit' && strAdjusted > 0 && strStatus=='Approved'){
		 appSts='52028';
	 }
	 return appSts;
}


function validateChangeValue(change){
		if(isNaN(change))
		{
			Error_Details(message_sales[314]);		
			Error_Show();
			Error_Clear();
			return false;
		}
		return true;
}
function validateDemoValue(change){
		if(isNaN(change))
		{
			Error_Details(message_sales[315]);		
			Error_Show();
			Error_Clear();
			return false;
		}
		return true;
}
function validateDbStr(str){

	 var strLen = str.length;
	 if(strLen>0 && ((((strLen/35)*5)+strLen) >4000))
	 {
	 	//Error_Details(" Selected Number of Groups is too large. Please select less than 100 Groups");
				
	}					  
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	return true;
}
function fnChngVal(rowId,cellIndex){
	approve_0_a = gridObjAnalysis.getColIndexById("approve");
	gridObjAnalysis.forEachRow( function(id){	
		if(id==rowId)
		{
			gridObjAnalysis.cellById(id,approve_0_a).setValue(gridObjAnalysis.cellById(rowId,approve_0_a).getValue()); 
		}
	});
}
function fnGetApproveStatus(status, rowId,bstatus){
	//alert("status  "+status+"userdesc  "+userdesc);
	var isAprStatus = '';
	var flgSelected = false;
	var publishStatus = '';
	approve_0_a = gridObjAnalysis.getColIndexById("approve");
	lvlstrid_9_a = gridObjAnalysis.getColIndexById("lvlstrid");
	if(status!='52190')
	{
			gridObjAnalysis.forEachRow( function(id){
			if(gridObjAnalysis.getLevel(id)>=1 && gridObjAnalysis.cellById(id,lvlstrid_9_a).getValue()==rowId && !flgSelected){	
				publishStatus = gridObjAnalysis.cellById(id,approve_0_a).getValue();
				if(publishStatus=='52028'){ // 52027 Pending 28 Approved 29 Denied 
					flgSelected = true;
				}
				isAprStatus = publishStatus;
				if(publishStatus=='52028' || publishStatus=='Approved'){
					isAprStatus = 52028;
					flgSelected = true;
				}else if(publishStatus=='52027' || publishStatus=='Pending'){
					isAprStatus = 52027;
				}else if(publishStatus=='52029' || publishStatus=='Denied')	{
					isAprStatus = 52029;
				}	
				
				if(publishStatus == "Initiated" && bstatus == 'Save'){
					isAprStatus = 52186; // Initiated
				}else if(publishStatus == "Initiated" && bstatus == 'Submit'){
					isAprStatus = 52027; // Pending
				}	
			}
		});
	}
	
	return isAprStatus;
}
function fnChangedFilter(obj){
	adjusted_8_g = gridObjGroup.getColIndexById("adjusted");
	if(obj.checked)
	{		
		 gridObjGroup.filterBy(adjusted_8_g,function(data){
				return   data.toString().length>0;  
	   });
	  
	}
	else
	{
		gridObjGroup.filterBy(adjusted_8_g,"");			
	}

}
function fnChangeEffectiveDate(){
	if(gridObjGroup.getRowsNum()>0)
	{
			Error_Details(message_sales[316]);		
			Error_Show();
			Error_Clear();
			return false;
	}
	show_calendar('frmPricingRequestForm.effectiveDate');
}
function fnShowDate(fromDate,statusId)
{
	if(statusId!=undefined){
		if(gridObjGroup.getRowsNum()>0 && statusId=='oldreq'){
			Error_Details(message_sales[316]);		
			Error_Show();
			Error_Clear();
			return false;
			}
	}
		
	var obj=document.getElementById("dhtmlxCalendar");
	if(fromDate!='')
	mCal.setDate(fromDate);
	mCal.draw();
	if(obj.style.display=="none"){
		obj.style.display="inline";
	}
	else{ 
		obj.style.display="none";	
	}
}

function fnVoid(){		
	document.frmPricingRequestForm.action ="/GmCommonCancelServlet";
	document.frmPricingRequestForm.hTxnId.value = document.frmPricingRequestForm.hreqid.value;	
	document.frmPricingRequestForm.hTxnName.value = document.frmPricingRequestForm.hreqid.value;
	document.frmPricingRequestForm.hDisplayNm.value = 'Pricing Dashboard';
	document.frmPricingRequestForm.hRedirectURL.value = '/gmPricingStatusReport.do?method=pricingStatusReport';
	document.frmPricingRequestForm.submit();
}	

function fnOnSave(bstatus){
	var rstatus= document.frmPricingRequestForm.hstatusId.value;
	var accGPO = document.frmPricingRequestForm.strAccGPO.value;
	submitClicked = true;
	submitMsg = false;
	var answer = '';
	if(!fnValidateVal('save',bstatus)) return;
		//alert('ApprLvl_fl'+ApprLvl_fl+'::'+bstatus+'::'+errorMsg);
		if(bstatus=='Save'){
			var inputAnswer = saveAnswer();
			if(!inputAnswer){
				Error_Clear();
			}
		}
	    if(bstatus=='Submit' && (ApprLvl_fl)){ //(rstatus=='52187' && bstatus=='Submit') && errorMsg!='' ||    
	    	var inputAnswer = saveAnswer();
	    	if(!inputAnswer){
				Error_Show();
				Error_Clear();
				dhxLayout.cells("d").expand(); // Show the Q & A
				return false;
			}Error_Clear();
		}
		
	/*	var ObjGroup = gridObjGroup.getChangedRows(true);
		var ObjAnalysis = gridObjAnalysis.getChangedRows(true);
		if(ObjGroup == '' && ObjAnalysis == ''){
			Error_Details('Please make any changes before submitting.');
		}
	*/
	/*progressOn(true);
	fnDisableEl("idSaveNew");
	fnDisableEl("idVoid");
	fnDisableEl("idSave");
	fnDisableEl("idExport");*/
	//get the values to save for Question Answers
	document.frmPricingRequestForm.hinputAnswer.value =  inputAnswer; 
	//alert('hinputAnswer->'+document.frmPricingRequestForm.hinputAnswer.value);
	if((rstatus==52186 ||rstatus == '') && bstatus =='Submit'){
		submitMsg = true;
	}
	/*if((ObjGroup == '' || ObjAnalysis == '') && !submitMsg && !gridValueChanged){
		Error_Details('Please make any changes before submitting.');	
	}
	if(ErrorCount > 0){
			Error_Show();
			Error_Clear();
			return false;
	}*/
	//get the values to save for current status
	document.frmPricingRequestForm.hinputStr.value =   fnSaveValues(rstatus,bstatus,false,submitMsg);
	//alert('hinputStr->'+document.frmPricingRequestForm.hinputStr.value);
	document.frmPricingRequestForm.hDeniedinputStr.value  = fnSaveValues(rstatus,bstatus,true,submitMsg);
	var DeniedInputStr = document.frmPricingRequestForm.hDeniedinputStr.value;
	//alert('hDeniedinputStr hinputStr->'+document.frmPricingRequestForm.hDeniedinputStr.value);
	if((rstatus!=52189 || rstatus!=52190)&& !validateDbStr(document.frmPricingRequestForm.hinputStr.value)) return;
	
	//get the new status
	if(rstatus=='' && bstatus =='Save') rstatus = 52186; //52120
	else if((rstatus==52186 ||rstatus == '') && bstatus =='Submit'){
		 rstatus = 52187;
	}
	
	document.frmPricingRequestForm.hstatusId.value = rstatus;
	document.frmPricingRequestForm.action =  '/gmPricingRequestDAction.do?method=savePriceRequest';
	//document.frmPricingRequestForm.idSave.disabled = true;
	//alert('ApprLvl_fl::'+ApprLvl_fl);
	if(ApprLvl_fl)
		document.frmPricingRequestForm.apprlvl.value = 52201; // PC
	else
		document.frmPricingRequestForm.apprlvl.value = 52200 // AD
	
	//alert('Appr Value::'+document.frmPricingRequestForm.apprlvl.value)
	/*if(rstatus=='' || rstatus=='52186'){
		document.frmPricingRequestForm.idSaveNew.disabled = true;
	}*/
	if(DeniedInputStr.length>0){
		//document.frmPricingRequestForm.action ="/gmPricingRequestDAction.do?method=savePriceRequest";
		// MNTTASK-6115 - Once back to Pricing by group tab when denied the request, need to be add account type. 
		document.frmPricingRequestForm.strAccType.disabled = false;
		var strAccType = document.frmPricingRequestForm.strAccType.value;
		document.frmPricingRequestForm.hTxnId.value = document.frmPricingRequestForm.hreqid.value;	
		document.frmPricingRequestForm.hTxnName.value = document.frmPricingRequestForm.hreqid.value;
		document.frmPricingRequestForm.hCancelType.value = 'DNYRST';
		document.frmPricingRequestForm.hSkipCmnCnl.value = 'YES';
		document.frmPricingRequestForm.hRedirectURL.value = "/gmPricingRequestDAction.do?method=openPriceRequest&fchAllSysFl=Y&strRequestId="+reqid+"&strAccType="+strAccType;
		document.frmPricingRequestForm.hDisplayNm.value = 'Pricing By Group';
	}
	var strAccType = document.frmPricingRequestForm.strAccType.value;
	document.frmPricingRequestForm.strRemoveSysIds.value = removeSysIds.substr(1,removeSysIds.length)+',';

	//When Approve Do not implement check box is selected, we should set the publish override flag.This will be saved in t7000_account_price_request 
	if((document.frmPricingRequestForm.chk_DntImplm!=undefined &&
			   document.frmPricingRequestForm.chk_DntImplm.checked)){
				document.frmPricingRequestForm.strPublOvrRide.value='Y';
			}else {
				document.frmPricingRequestForm.strPublOvrRide.value='';	
	}
	
	//alert('rstatus::'+rstatus+'bstatus ::'+bstatus+'::strAccType ::'+strAccType);
	if(submitMsg && strAccType == '903108'){
		answer = confirm(message_sales[317]);
	}else if(submitMsg && strAccType == '903107'  && accGPO !=0 && accGPO !=''){
		answer = confirm(message_sales[318]);
	}else{
		answer = true;
	}
	if(answer){
		// Add progress bar on click save & submit
	    progressOn(true);
		fnDisableEl("idSaveNew");
		fnDisableEl("idVoid");
		fnDisableEl("idSave");
		fnDisableEl("idExport");
		document.frmPricingRequestForm.strAccType.disabled = false;
		
		if (document.frmPricingRequestForm.strGroupAcc!=undefined)
			document.frmPricingRequestForm.strGroupAcc.disabled = false;
		document.frmPricingRequestForm.submit();
	}
	
}
//below two function Activate & Deactivate progress bar.
function progressOn(fullLayout) {
    if (fullLayout) {
        dhxLayout.progressOn();
    } else {
        dhxLayout.items[getInd()].progressOn();
    }
}
function progressOff(fullLayout) {
    if (fullLayout) {
        dhxLayout.progressOff();
    } else {
        dhxLayout.items[getInd()].progressOff();
    }
}


var errdropdn='';
function saveAnswer(){
	var varRows = ansSize;
	var inputAnswer ='';
	var errques='';
	var strPrevQues='';
	var strPrevAns='';
	var intselectcount = 0;
	var intErrCount = 0;
    errdropdn='';
	
	 for (var k=1; k < varRows+1; k++) 
	{
		objAns = eval("document.frmPricingRequestForm.hAnsGrpId"+k);
		objQues = eval("document.frmPricingRequestForm.hQuestionID"+k);
	 	if(objAns!=undefined)
		{
	 		
	 		if(objAns.type  == "select" )
			{
				fnValidateDropDown('hAnsGrpId'+k,' Question ' + objQues.value); 
				strPrevAns = objAns.value;
	 			intselectcount++;
				if(objAns.value == 2 || objAns.value == 3){
					    	intErrCount ++; 
				}
			}
	 		 //Added for MNTTASK-4645
	 		if(objAns.value==""){
	 			if(strPrevQues == objQues.value){
	 				if(strPrevAns == 1 || strPrevAns == 2){
	 					errques=errques + objQues.value +',';
	 				}
	 			}
	 			
	 			else{
	 			     errques=errques + objQues.value +','; 
	 			}
	 		}
	 		//Added for MNTTASK 4645
	 		strPrevQues = objQues.value;

			if(objAns.id =='') 
			{
			    	listID = objAns.value;
				    ansDesc = '';
			}				
			else 
			{
				listID = '';
				ansDesc = objAns.value;
			}		
			inputAnswer = inputAnswer + objQues.value + '^'+  k +  '^' + listID + '^'+ ansDesc  + '|';
		}
	}
	
	var errquesarr = errques.split(",");
	if(errquesarr.length >= QuestionSize && errdropdn.split(",").length > intselectcount && intErrCount == 0){
		Error_Details(message_sales[319]);
	}else{
	if(errdropdn!='' ){
		Error_Details(Error_Details_Trans(message_sales[320],errdropdn));
	}
	if (errques!='' )
	{
		Error_Details(Error_Details_Trans(message_sales[3201],errques));
	}
	} 
	if(ErrorCount > 0){
		return false;
	}else{
		return inputAnswer;
	}
}
function fnValidateDropDown(val,fld)
{
	var blDisabled;
	var obj = eval("document.all."+val);
	if(obj != undefined){
		var objval = obj.value;
		if (obj.disabled == true)
		{
			blDisabled = true;
		}
		if (!blDisabled)
		{
			if (objval == '1' || objval == ''||objval == '4') 
			{
				errdropdn=errdropdn + fld +",";
				//Error_Details("Please choose a valid option for the field:<b>"+fld+"</b>");	
			}
		}
	}else{
		obj = eval(val);
		objval = obj.getSelectedValue();
		if (obj._disabled == true)
		{
			blDisabled = true;
		}
		if (!blDisabled)
		{
			if (objval == 1 || objval == null || objval == '4') 
			{
				errdropdn=errdropdn + fld +",";
				//Error_Details("Please choose a valid option for the field:<b>"+fld+"</b>");	
			}
		}
	}
}
function fnExport(){
	approve_0_a = gridObjAnalysis.getColIndexById("approve");
	name_1_a = gridObjAnalysis.getColIndexById("name");
	qty_2_a = gridObjAnalysis.getColIndexById("qty");
	list_3_a = gridObjAnalysis.getColIndexById("list");
	adjusted_5_a = gridObjAnalysis.getColIndexById("adjusted");
	twcomp_7_a = gridObjAnalysis.getColIndexById("twcomp");
	rule_8_a = gridObjAnalysis.getColIndexById("rule");
	apprby_10_a = gridObjAnalysis.getColIndexById("apprby");
	asp_11_a = gridObjAnalysis.getColIndexById("asp");
	effdate_18_a = gridObjAnalysis.getColIndexById("effdate");
	aprlvl_7_a = gridObjAnalysis.getColIndexById("aprlvl");
	tw_6_a = gridObjAnalysis.getColIndexById("tw");
	current_4_a = gridObjAnalysis.getColIndexById("current");
	
	var html="";
	gridObjAnalysis.filterBy(approve_0_a,"");
	gridObjAnalysis.expandAll();
    var numRows=gridObjAnalysis.getRowsNum();
    var numCols=gridObjAnalysis.getColumnsNum();
    html=html+"<table border=2><tr height='40' valign='center'>";
	html=html+"<td align='center' class='Line'>System</td>";
	html=html+"<td align='center' class='Line'>Construct/Group/Part</td>";
    for(i=qty_2_a;i<=rule_8_a;i++)html=html+"<td align='center' class='Line'>"+gridObjAnalysis.getColumnLabel(i)+"</td>";
	html=html+"<td align='center' class='Line'>Rule Desc</td>";
	html=html+"<td align='center' class='Line'>ASP</td>";
	html=html+"<td align='center' class='Line'>Effective Date</td>";
	html=html+"<td align='center' class='Line'>"+gridObjAnalysis.getColumnLabel(approve_0_a)+"</td>";
	html=html+"<td align='center' class='Line'>Approve By</td>";
	
    html=html+"</tr>";

	var sysNm, conNm, conInd=1;
    for(i=0;i<numRows;i++)
    {

		var levl = gridObjAnalysis.getLevel(gridObjAnalysis.getRowId(i));
		var nameVal = gridObjAnalysis.cellByIndex(i,name_1_a).getValue();
		
		sysNm = levl==0?nameVal:sysNm;
		conNm = levl==1?nameVal:conNm;

		if(levl==1)
		{			
			conInd = i;
		}		
		if(levl>1)
		{
			html=html+"<tr";		
			/*if(gridObjAnalysis.cellByIndex(i,0).getValue()!='')
			{
				html=html+" bgcolor='#E3EFFF'";
			}
			*/
										
			html=html+">";			
			html=html+"<td align=left>"+sysNm+"</td>";	
			html=html+"<td align=left";
			if(levl==2)
			{	
				html=html+" style='color:#5555FF'";					
			}
			else if(levl==3)
			{	
				html=html+" style='color:#006AD5;'";					
			}	
			html=html+">";
			html=html+nameVal+"</td>";	
			for(j=qty_2_a;j<=rule_8_a;j++)
			{
				html=html+"<td align=right >";
				var txt="";
				var val = gridObjAnalysis.cellByIndex(i,j).getValue();
				
				if(val!=null)
				{
					if(j==adjusted_5_a && gridObjAnalysis.cellByIndex(i,approve_0_a).getValue()=="Pending"){
						txt="<font color='red'>"+val+"</font>";	
					}else{
						txt=val;										
					}									
				}
				if(txt!="" && txt!=null && (j == list_3_a || j == adjusted_5_a || j == tw_6_a || j == current_4_a))
					html=html+"$"+txt+"</td>";
				else
					html=html+txt+"</td>";
			}

			html=html+"<td align=right >"+gridObjAnalysis.cellByIndex(i,rule_8_a).getAttribute("title")+"</td>";
			html=html+"<td align=right>"+gridObjAnalysis.cellByIndex(i,asp_11_a).getValue()+"</td>";
			var val = gridObjAnalysis.cellByIndex(i,approve_0_a).getValue();
			var txt="";
			html=html+"<td align=left>"+gridObjAnalysis.cellByIndex(i,effdate_18_a).getValue()+"</td>";
			txt= val=="52027"?"<font color='red'>Pending</font>":(val=="52028"?"<font color='green'>Approved</font>":val);   
			html=html+"<td align=left>"+txt+"</td>";	
			html=html+"<td align=left>"+gridObjAnalysis.cellByIndex(i,apprby_10_a).getValue()+"</td>";	
			

			var nextLev = gridObjAnalysis.getLevel(gridObjAnalysis.getRowId(i+1));				
			if(nextLev<2)
			{				
					html=html+"</tr>";
					html=html+"<tr style='color:red;'>";	
					html=html+"<td align=left>"+sysNm+"</td>";
					html=html+"<td align=right style='color:red;' class='Line'>"+conNm+"</td>";	
					html=html+"<td align=left class='Line'></td>";	
					for(k=list_3_a;k<=twcomp_7_a;k++)
					{
						html=html+"<td align=right >";
						var txt="";
						var val = gridObjAnalysis.cellByIndex(conInd,k).getValue();
						
						if(val!=null)
						{
							txt=val;										
						}
						if(txt!="" && txt!=null && (k == list_3_a || k == adjusted_5_a || k == tw_6_a || k == current_4_a))
							html=html+"$"+txt+"</td>";
						else
							html=html+txt+"</td>";
					
					}
					html=html+"<td align=left class='Line'></td>";
					html=html+"<td align=left class='Line'></td>";					
					html=html+"<td align=left class='Line'></td>";
					html=html+"<td align=left class='Line'></td>";
					html=html+"<td align=left class='Line'></td>";
					html=html+"<td align=left class='Line'></td>";
					html=html+"</tr><tr bgcolor=#CCCCCC><td class='Line' colspan=13></td>";
			}

			html=html+"</tr>";
		}
    }
    html=html+"</table>";
   // gridObjAnalysis.setSerializationLevel(false,false,true,false,false,false);
   // gridObjAnalysis.setSerializableColumns("true,true,true,true,true,true,true,true,true,true,false");
   // var xmlStr = gridObjAnalysis.serialize();
    document.frmPricingRequestForm.strAccType.disabled = false;
    document.frmPricingRequestForm.anlData.value=html;
    document.frmPricingRequestForm.method='POST';
  	document.frmPricingRequestForm.action ="/gmPricingRequestDAction.do?method=exportToExcel";
    //document.frmPricingRequestForm.target='_blank';
    document.frmPricingRequestForm.submit();
	
}

function fnOpenGroupComments(id)
{                
	   id = document.frmPricingRequestForm.hreqid.value+'-'+id;
       windowOpener("/GmCommonLogServlet?hType=1252&hID="+id,"PRGrp","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
}
function fnOpenSysComments(id)
{                
       windowOpener("/GmCommonLogServlet?hType=1252&hID="+id,"PRSys","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
}
function fnReqStatusLog(id)
{
	windowOpener("/GmPageControllerServlet?strPgToLoad=/gmAuditTrail.do&auditId=1034&txnId="+id,"","resizable=yes,scrollbars=yes,top=250,left=300,width=785,height=300,status=1");
}

function fnLoadAcctType(obj) {
	document.frmPricingRequestForm.haction.value = '';
	document.frmPricingRequestForm.hstatusId.value = '';
	document.frmPricingRequestForm.action =  '/gmPricingRequestDAction.do?method=loadPriceRequest';
	document.frmPricingRequestForm.submit();
}

function fnLoadGroupAcct(obj){
	//document.frmPricingRequestForm.strAccountId.value = '';
	document.frmPricingRequestForm.haction.value = '';
	document.frmPricingRequestForm.hstatusId.value = '';
	document.frmPricingRequestForm.action =  '/gmPricingRequestDAction.do?method=loadPriceRequest';
	document.frmPricingRequestForm.submit();
}

function fnSelectAll(varControl,varControlToSelect,varCmd)
{	
				objSelAll = varControl;
				//alert(varControl.name);
				if (objSelAll ) {
					//objCheckSiteArr = document.all.checkFieldSales;
					var objCheckSiteArr = eval("document.frmPricingRequestForm."+varControlToSelect);

					if ((objSelAll.checked || varCmd == 'selectAll') && objCheckSiteArr)
					{
						objSelAll.checked = true;
						objCheckSiteArr.checked = true;
						objCheckSiteArrLen = objCheckSiteArr.length;
						for(i = 0; i < objCheckSiteArrLen; i ++) 
						{	
							objCheckSiteArr[i].checked = true;
						}
					}
					else if (!objSelAll.checked  && objCheckSiteArr ){
						objCheckSiteArrLen = objCheckSiteArr.length;
						objCheckSiteArr.checked = false;
						for(i = 0; i < objCheckSiteArrLen; i ++) 
						{	
							objCheckSiteArr[i].checked = false;
						}
					}
				}
}

function fnUnpricedSystem(){
	var strAccType = document.frmPricingRequestForm.strAccType.value;
	var strGPOId = '';
	if(strAccType == 903108)
		strGPOId = document.frmPricingRequestForm.strGroupAcc.value;
	

		var arrAllSystem = document.frmPricingRequestForm.checkSystems;	
		if(arrAllSystem.length != null){
			var arrAllLen = arrAllSystem.length;	
		
		var systemInputStr='';
		for (var i=0;i< arrAllLen;i++ )
		{
			//if(arrAllSystem[i].checked){
				systemInputStr=systemInputStr+','+arrAllSystem[i].value;
			//}
			
		}
		if(arrAllSystem !=undefined)
		{
			systemInputStr = arrAllLen==undefined?arrAllSystem.value:systemInputStr;
		}
		if(systemInputStr!='')
		{
			systemInputStr = systemInputStr.substring(1,systemInputStr.length);
			  w1 =  createDhtmlxWindow(915,700,true);
			  w1.setText("UnPriced System(S)"); 
			  w1.attachURL('/gmPricingRequestDAction.do?method=UnPricingSystemReport&strGroupAcc='+strGPOId+'&strAccType='+strAccType+'&sysTranId='+systemInputStr+'&strAccountId='+strAccountId,"PRICE");
			  return false;
			//windowOpener('/gmPricingRequestDAction.do?method=UnPricingSystemReport&strGroupAcc='+strGPOId+'&strAccType='+strAccType+'&sysTranId='+systemInputStr+'&strAccountId='+strAccountId,"PRICE","resizable=yes,scrollbars=yes,top=150,left=200,width=910,height=660");
		}
	}
}

function fnUnpricedGroup(){
	var strAccType = document.frmPricingRequestForm.strAccType.value;
	var strGPOId = '';
	if(strAccType == 903108)
		strGPOId = document.frmPricingRequestForm.strGroupAcc.value;
	
	  w2 =  createDhtmlxWindow(920,710,true);
	  w2.setText("UnPriced Group(S)"); 
	  w2.attachURL('/gmPricingRequestDAction.do?method=UnPricingGroupReport&strGroupAcc='+strGPOId+'&strAccType='+strAccType+'&strAccountId='+strAccountId,"PRICE");
	  return false;
	//windowOpener('/gmPricingRequestDAction.do?method=UnPricingGroupReport&strGroupAcc='+strGPOId+'&strAccType='+strAccType+'&strAccountId='+strAccountId,"PRICE","resizable=yes,scrollbars=yes,top=150,left=200,width=910,height=660");
}

function fnGroupAccountPopUp(){
	if (gpoID == ''){
		if(document.frmPricingRequestForm.strAccGPO != undefined)
			gpoID = document.frmPricingRequestForm.strAccGPO.value;
	}

	  w3 =  createDhtmlxWindow(1000,450,true);
	  w3.setText("Group Account Details"); 
	  w3.attachURL("/gmPricingRequestDAction.do?method=GroupAccountDetails&hGpoFl=Y&hrpoId="+gpoID,"GPO");
	  return false;	
	  
	//windowOpener("/gmPricingRequestDAction.do?method=GroupAccountDetails&hGpoFl=Y&hrpoId="+gpoID,"GPO","resizable=yes,scrollbars=yes,top=150,left=200,width=1190,height=500");
}

function fnGetPartDetails(rowid,cellInd){
	var systemInputStr = '';
	var loader = dhtmlxAjax.getSync('/gmPricingRequestDAction.do?method=getPartDetails&systemInputStr='+systemInputStr+'&strAccountId='+strAccountId+'&groupStr='+rowid+'&strRequestId='+reqid+'&ramdomId='+Math.random());
	fnAddSystemData(loader);
	grpGridChanged = true; 	
}

function fnAjaxPopUpRst(reqid,accid,systemid,groupid){
	var varFchAllSysFl = '';
	var gpoID = '';

	if(reqid != '' ){
	varFchAllSysFl = 'Y';
	}
	if(groupid==undefined){
		groupid = '';
	}
	if(document.frmPricingRequestForm.strAccType.value == 903108){
		gpoID = document.frmPricingRequestForm.strGroupAcc.value;
	}
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmPricingRequestDAction.do?method=getSysReqDetails&systemInputStr='+systemid+'&strAccountId='+accid+'&groupid='+groupid+'&strGroupAcc='+gpoID+'&ramdomId='+Math.random());
	var loader = dhtmlxAjax.getSync(ajaxUrl);
	fnAddSystemData(loader);
	grpGridChanged = true;	
}

function fnApplyApprLevel(id){
	 var applvl = gridObjGroup.getColIndexById("AppLvl");
	var chngType = gridObjGroup.cellById(id, gridObjGroup.getColIndexById("type")).getValue();
	var chngVal = gridObjGroup.cellById(id, gridObjGroup.getColIndexById("change")).getValue();
	var adjpr = gridObjGroup.cellById(id, gridObjGroup.getColIndexById("adjusted")).getValue();
	if(chngVal !='' && adjpr !=0){
		
		var tripwir =gridObjGroup.cellById(id, gridObjGroup.getColIndexById("tw")).getValue();
	 //gridObjGroup.cellById(id,6).setValue(chngType);
		if(tripwir > adjpr ){
			if (gridObjGroup.getLevel(id) == 1)
			{
			put(id,'PC');
			}
			gridObjGroup.cellById(id,applvl).setValue('PC');
			ApprLvl_fl = true;
			//alert("validate approval fl");
		}else {
			gridObjGroup.cellById(id,applvl).setValue('AD');
			var chkStatusFl = get(id);
    		if(chkStatusFl == 'PC'){
    			remove(id); // to remove the PC
    			ApprLvl_fl = false;
    		}
			fnTestValidate(id);
		}
	}
}
function fnTestValidate(id){
		var parts = gridObjGroup.getSubItems(id).split(",");
        var len = parts.length;
        if(len==1 && parts[0]=='')return true;
        for(var i=0;i<len;i++)
        {
        	var aLevel = gridObjGroup.cellById(parts[i], 21).getValue();
//        	alert(aLevel);
        	if(aLevel != 'PC'){
        		//ApprLvl_fl = false;
        		remove(id);
        		var tt = this.keyArray.length;
//        		alert('Size is '+tt);
        	}
        }
}
//members
this.keyArray = new Array(); // Keys
this.valArray = new Array(); // Values
    
// methods
this.put = put;
this.get = get;
this.showMe = showMe;   // returns a string with all keys and values in map.
this.findIt = findIt;
this.remove = remove;


function put(key,val) 
{ 
    var elementIndex = this.findIt(key); 
    if( elementIndex == (-1) ) 
    { 
        this.keyArray.push( key ); 
        this.valArray.push( val ); 
    } 
    else 
    { 
        this.valArray[ elementIndex ] = val; 
    } 
} 
 
function get(key) 
{ 
    var result = null; 
    var elementIndex = this.findIt( key ); 
 
    if( elementIndex != (-1) ) 
    {    
        result = this.valArray[ elementIndex ]; 
    } 
    else {
    	result = "<option value= >[Choose One]</option>";
    }
    return result; 
}
function findIt(key) 
{ 
    var result = (-1); 
 
    for( var i = 0; i < this.keyArray.length; i++ ) 
    { 
        if( this.keyArray[ i ] == key ) 
        { 
            result = i; 
            break; 
        } 
    } 
    return result; 
} 
function remove(key)
{
    var result = null;
    var elementIndex = this.findIt( key );
    if( elementIndex != (-1) )
    {
        this.keyArray = this.keyArray.removeAt(elementIndex);
        this.valArray = this.valArray.removeAt(elementIndex);
    }  
    return ;
}
function showMe()
{
    var result = "";
    
    for( var i = 0; i < this.keyArray.length; i++ )
    {
        result += "Key: " + this.keyArray[ i ] + "\tValues: " + this.valArray[ i ] + "\n";
    }
    return result;
}
function removeAt( index )
{
  var part1 = this.slice( 0, index);
  var part2 = this.slice( index+1 );

  return( part1.concat( part2 ) );
}
Array.prototype.removeAt = removeAt;

function fnSelectAllFun(val){
	approve_0_a = gridObjAnalysis.getColIndexById("approve");
	aprlvl_7_a = gridObjAnalysis.getColIndexById("aprlvl");
	
	
	gridObjAnalysis.forEachRow( function(id)   { 
		var strStatus = gridObjAnalysis.cellById(id,approve_0_a).getValue();
		if(gridObjAnalysis.getLevel(id)>=1 && (strStatus==52027 || strStatus == 52028 || strStatus == 52151 || strStatus == 52150)){
			var allFl = false;
			var strApprLvl = gridObjAnalysis.cellById(id, aprlvl_7_a).getValue();
			var strApprLvlSta = userdesc+'_'+strApprLvl;
			var combo = gridObjAnalysis.getCombo(approve_0_a);
			if(val == 52150 && (strApprLvlSta == 'AD_PC' || strApprLvlSta == 'PC_PC')){
				combo.remove(52150);
				combo.put(52150,'Submit for VP');
				allFl = true;
			}else if(val == 52151 && (strApprLvlSta == 'VP_PC' || strApprLvlSta == 'PC_PC')){
				combo.remove(52151);
				combo.put(52151,'Submit for PC');
				allFl = true;
			}else if(val == 52029) {
				combo.remove(52029);
				combo.put(52029,'Denied');
				allFl = true;
			}
			
			if(allFl){
				gridObjAnalysis.cellById(id,approve_0_a).setValue(val); 
			}
		}
		});
}
function fnUnSelectAllFun(val,oldVal){
	approve_0_a = gridObjAnalysis.getColIndexById("approve");
	gridObjAnalysis.forEachRow( function(id)   { 
		if(gridObjAnalysis.getLevel(id)>=1 && gridObjAnalysis.cellById(id,approve_0_a).getValue()==val){
				gridObjAnalysis.cellById(id,approve_0_a).setValue(oldVal); 
				 						
		}
		});
}
function fnGroupDetail(groupID) { 
	w3 =  createDhtmlxWindow(900,400,true);
	  w3.setText("Group Account Details"); 
	  w3.attachURL("/gmGroupPartMap.do?strDisableLink=Y&strPartListFlag=Y&hviewEdit=view&groupId="+groupID+"&groupNM=","Part Details");
	  return false;	
	  
	 // windowOpener("/gmGroupPartMap.do?strPricingByGroupFl=Y&strPartListFlag=Y&hviewEdit=view&groupId="+groupID+"&groupNM=","","resizable=yes,scrollbars=yes,top=250,left=300,width=950,height=400");
}
function fnValidateUser(val){
	if(val == 52150 && userdesc != 'AD' && userdesc != 'PC'){
		Error_Details(message_sales[322]);
	}
	if(val == 52151 && userdesc != 'VP' && userdesc != 'PC'){
		Error_Details(message_sales[322]);
	}
	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}
  return true;
}

function fnOpenPricingRules(ruleid){
	windowOpener("/gmPricingRequestDAction.do?method=loadPricingRequestRules&hReqID="+strRequestId+"&hRuleID="+ruleid,"PricingRules","resizable=yes,scrollbars=yes,top=250,left=300,width=785,height=300");
	return false;
}

function onClickSelect(Id,cellInd,state){
	ActiveFlg_23_a = gridObjAnalysis.getColIndexById("ActiveFlg");
	approve_0_a = gridObjAnalysis.getColIndexById("approve");
	var gpoID = document.frmPricingRequestForm.strAccGPO.value;

	// When we click the check box it should occur validation.
	if((cellInd == ActiveFlg_23_a) && ((gpoID != 0 && userdesc != 'PC') ||(contfl == 'YES' && userdesc != 'PC'))){
		gridObjAnalysis.cellById(Id,ActiveFlg_23_a).setChecked(false);
		Error_Details(message_sales[323]);
				 if (ErrorCount > 0){
					Error_Show();
					Error_Clear();
					return false;
		}
	}
	var assign_state = '';
	if(state){
		assign_state = true;
	}else{
		assign_state = false;
	}
	// to set the appr fl as System and Group level
	if(gridObjAnalysis.getLevel(Id) == 0 || gridObjAnalysis.getLevel(Id) == 1 || gridObjAnalysis.getLevel(Id) == 2){	
		var groups = gridObjAnalysis.getSubItems(Id).split(",");
		var len = groups.length;
		for(var i=0;i<len;i++){
			if(gridObjAnalysis.getLevel(Id) == 0){
				fnApprGroupLevel(groups[i],cellInd,assign_state);
			}
		}
	}
}

function fnApprGroupLevel(Id,cellInd,state){
	ActiveFlg_23_a = gridObjAnalysis.getColIndexById("ActiveFlg");
	approve_0_a = gridObjAnalysis.getColIndexById("approve");
	var groups = gridObjAnalysis.getSubItems(Id).split(",");
//	alert('groups::'+groups);
    var len = groups.length;
    for(var i=0;i<len;i++){
    	status = gridObjAnalysis.cellById(groups[i],approve_0_a).getValue();
    	if(status == 52028 || status=='Approved')
    		gridObjAnalysis.cellById(groups[i],ActiveFlg_23_a).setChecked(state);
    	
    	fnPartLvlActiveFlChkBox(groups[i],cellInd,state);
    }
}
//function to disable four buttons save,submit,void,excel
function fnDisableEl(id){
	var obj = eval("document.frmPricingRequestForm."+id);
	if(obj!=null ){
		obj.disabled=true;
	}
	
}

function fnPartLvlActiveFlChkBox(Id,cellInd,state){
	ActiveFlg_23_a = gridObjAnalysis.getColIndexById("ActiveFlg");
	approve_0_a = gridObjAnalysis.getColIndexById("approve");
	
	var parts = gridObjAnalysis.getSubItems(Id).split(",");
	var len = parts.length;
	if(len==1 && parts[0]=='')return true;
	for(var i=0;i<len;i++){
    	status = gridObjAnalysis.cellById(parts[i],approve_0_a).getValue();
    	if(status == 52028 || status=='Approved')
    		gridObjAnalysis.cellById(parts[i],ActiveFlg_23_a).setChecked(state);

    }
}
//function to get the Analysis view data
function fnGetAnalysisViewDetails(){
    var ids = gridObjAnalysisView.getAllRowIds(); // Only One time to fetch the data.
 	if(strAnaView == "NO" && strAccountId != 0 && strAccountId != '' && ids == ''){
 		var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmPricingRequestDAction.do?method=getAnalysisViewGridDetails&strRequestId='+reqid+'&strAccountId='+strAccountId+'&strGroupAcc='+gpoID+'&ramdomId='+new Date().getTime());
		var loader = dhtmlxAjax.getSync(ajaxUrl);
		fillAnalysisViewData(loader);
	}
}

function fillAnalysisViewData(loader) {
	var response = loader.xmlDoc.responseXML;
	if(response!= null){                                             
		gridObjAnalysisView.clearAll();
		gridObjAnalysisView.init();
        gridObjAnalysisView.parse(response);
	}
}
//function to get the Activity Log data
function fnGetActivityLogDetails(){
	 var ids = gridObjActivity.getAllRowIds(); // Only One time to fetch the data.
 	if(strActivityLog == "NO" && strAccountId != 0 && strAccountId != '' && ids == ''){
 		var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmPricingRequestDAction.do?method=getActivityGridDetails&strRequestId='+reqid+'&strAccountId='+strAccountId+'&strGroupAcc='+gpoID+'&ramdomId='+new Date().getTime());
		var loader = dhtmlxAjax.getSync(ajaxUrl);
		fillActivityLogViewData(loader);
	}
}

function fillActivityLogViewData(loader) {
	var response = loader.xmlDoc.responseXML;
	if(response!= null){                                             
		gridObjActivity.clearAll();
		gridObjActivity.parse(response);
       	}
}

//Added for BUG-307 
function fnGroupClearValues(Id){
	var applvl ='';
	change_7_g = gridObjGroup.getColIndexById("change");
	adjusted_8_g = gridObjGroup.getColIndexById("adjusted");
	applvl = gridObjGroup.getColIndexById("AppLvl");
	type_6_g = gridObjGroup.getColIndexById("type");
	discount_9_g = gridObjGroup.getColIndexById("discount");
	var groups = gridObjGroup.getSubItems(Id).split(",");
    var len = groups.length;
    for(var i=0; i< len; i++){
   		gridObjGroup.cellById(groups[i],change_7_g).setValue('');
   		gridObjGroup.cellById(groups[i],adjusted_8_g).setValue('');
   		gridObjGroup.cellById(groups[i],applvl).setValue('');
   		gridObjGroup.cellById(groups[i],discount_9_g).setValue('');
   		fnPartClearValues(groups[i]);
    }
}
function fnPartClearValues(Id){
	var select_2_g = gridObjGroup.getColIndexById("select");
	var parts = gridObjGroup.getSubItems(Id).split(",");
	change_7_g = gridObjGroup.getColIndexById("change");
	adjusted_8_g = gridObjGroup.getColIndexById("adjusted");
	applvl = gridObjGroup.getColIndexById("AppLvl");
	discount_9_g = gridObjGroup.getColIndexById("discount");
    var len = parts.length;
    if(len==1 && parts[0]=='')return true;
    for(var i=0; i< len; i++){
   		//gridObjGroup.cellById(parts[i],select_2_g).setChecked(false);
   		gridObjGroup.cellById(parts[i],change_7_g).setValue('');
   		gridObjGroup.cellById(parts[i],adjusted_8_g).setValue('');
   		gridObjGroup.cellById(parts[i],applvl).setValue('');
   		gridObjGroup.cellById(parts[i],discount_9_g).setValue('');
    }
}


function onKeyPressed(code,ctrl,shift){
	var colIndex ='';
	type_6_g = gridObjGroup.getColIndexById("type");
	change_7_g = gridObjGroup.getColIndexById("change");
	adjusted_8_g = gridObjGroup.getColIndexById("adjusted");
	select_2_g = gridObjGroup.getColIndexById("select");
	var applvl = gridObjGroup.getColIndexById("AppLvl");
	discount_9_g = gridObjGroup.getColIndexById("discount");
	
	if(gridObjGroup._selectionArea!=null){
		colIndex = gridObjGroup._selectionArea.LeftTopCol;
	}

	//I will do copy particular row. The 67 values cntrl + C.
	
	if(code==67 && ctrl && colIndex == type_6_g){
		gridObjGroup.setCSVDelimiter("\t");
		 gridObjGroup.copyBlockToClipboard();
			if(gridObjGroup._selectionArea!=null){
				var area = gridObjGroup._selectionArea
				var leftTopCol = area.LeftTopCol;
				var leftTopRow = area.LeftTopRow;
				var rightBottomCol = area.RightBottomCol;
				var rightBottomRow = area.RightBottomRow;
				for (var i=leftTopRow; i<=rightBottomRow; i++){
					for (var j=leftTopCol; j<=rightBottomCol; j++){
						setValuefromClipboard = gridObjGroup.cellByIndex(i,j).getValue();
					}
				}
				gridObjGroup._HideSelection();
			}
	}else if(code==67 && ctrl && colIndex == change_7_g){
		gridObjGroup.setCSVDelimiter("\t");
		 gridObjGroup.copyBlockToClipboard();
		 if(gridObjGroup._selectionArea!=null){
				var area = gridObjGroup._selectionArea
				var leftTopCol = area.LeftTopCol;
				var leftTopRow = area.LeftTopRow;
				var rightBottomCol = area.RightBottomCol;
				var rightBottomRow = area.RightBottomRow;
				for (var i=leftTopRow; i<=rightBottomRow; i++){
					for (var j=leftTopCol; j<=rightBottomCol; j++){
						setChangeValuefromClipboard = gridObjGroup.cellByIndex(i,j).getValue();
					}
				}
				gridObjGroup._HideSelection();
			}
	}	
	// I will do paste particular row. The 67 values cntrl + C.
	if(code==86 && ctrl){
		
		if(gridObjGroup._selectionArea!=null){
			var colIndex = gridObjGroup._selectionArea.LeftTopCol;		
			
			// Which colum we need paste option so I get index in particular column
			if(colIndex!=undefined && colIndex != type_6_g && colIndex != change_7_g){
				alert(message_sales[324]);
			}else if(colIndex == type_6_g){// && columnnameType == true){
				if(gridObjGroup._selectionArea != null){
					var area = gridObjGroup._selectionArea
					var leftTopCol = area.LeftTopCol;
					var leftTopRow = area.LeftTopRow;
					var rightBottomCol = area.RightBottomCol;
					var rightBottomRow = area.RightBottomRow;
					for (var i=leftTopRow; i<=rightBottomRow; i++){
						for (var j=leftTopCol; j<=rightBottomCol; j++){
							var rowID = gridObjGroup.getRowId(i);
							gridObjGroup.cellByIndex(i,j).setValue(setValuefromClipboard);
							fnCalcAdjPrice(rowID,type_6_g,change_7_g,adjusted_8_g);

							if(gridObjGroup.cellById(rowID,type_6_g).getValue() == 52020){
								fnPartClearIndividual(rowID);
							}
							gridObjGroup.cellById(rowID,select_2_g).setChecked(true);
						}
					}
					gridObjGroup._HideSelection();
				}
			}else if (colIndex == change_7_g){
				if(gridObjGroup._selectionArea != null){
					//gridObjGroup.pasteBlockFromClipboard();
					var area = gridObjGroup._selectionArea
					var leftTopCol = area.LeftTopCol;
					var leftTopRow = area.LeftTopRow;
					var rightBottomCol = area.RightBottomCol;
					var rightBottomRow = area.RightBottomRow;
					for (var i=leftTopRow; i<=rightBottomRow; i++){
						for (var j=leftTopCol; j<=rightBottomCol; j++){
							var rowID = gridObjGroup.getRowId(i);
							//To Paste data to multiple rows on the "Change Column".
							gridObjGroup.cellByIndex(i,j).setValue(setChangeValuefromClipboard);
							
							fnCalcAdjPrice(rowID,type_6_g,change_7_g,adjusted_8_g);
							fnApplyApprLevel(rowID);
							gridObjGroup.cellById(rowID,select_2_g).setChecked(true);
						}
					}
					gridObjGroup._HideSelection();
				}
			}
		}else{
			alert(message_sales[325]);
		}
	}
	return true;
}
function fnPartClearIndividual(rowId){
	change_7_g = gridObjGroup.getColIndexById("change");
	adjusted_8_g = gridObjGroup.getColIndexById("adjusted");
	var applvl = gridObjGroup.getColIndexById("AppLvl");
	discount_9_g = gridObjGroup.getColIndexById("discount");
	gridObjGroup.cellById(rowId, change_7_g).setValue('');
	gridObjGroup.cellById(rowId, discount_9_g).setValue('');
	gridObjGroup.cellById(rowId, applvl).setValue('');
	gridObjGroup.cellById(rowId, adjusted_8_g).setValue('');
}

//MNTTASK-6864. While click the S icon,will be displayed Historical Sales Group by Month - Account
function fnHistoricalSalesAccRpt(){
	  w2 =  createDhtmlxWindow(1100,680,true);
	  w2.setText("Historical Sales By Group By Month");
	  w2.attachURL('/GmSetYTDReportServlet?&hAction=LoadGroupA&hAccountID='+strAccountId+'&SetNumber=null',"Historical Sales Account Report");
	  return false;
}
//MNTTASK-6864. While click the S icon,will be displayed Historical Sales Group by Month - Group Account
function fnHistoricalSalesGrpAccSysRpt(){
	  var GroupAccId = document.frmPricingRequestForm.strGroupAcc.value;
	  w2 =  createDhtmlxWindow(1100,680,true);
	  w2.setText("Historical Sales By Group By Month"); 
	  w2.attachURL('/GmSetYTDReportServlet?hGroupName='+strGroupName+'&hAction=LoadGroupA&hAccountID=null&SetNumber=null&hGroupAccId='+GroupAccId,"Historical Sales Group Account Report");
	  return false;
}
//MNTTASK-6864. While click the S icon,will be displayed Historical Sales Group by Month - Group Account
function fnHistoricalSalesGrpAccRpt(){
	  var GroupAccId = document.frmPricingRequestForm.strGroupAcc.value;
	  w2 =  createDhtmlxWindow(1100,680,true);
	  w2.setText("Historical Sales By Account By Month");
	  w2.attachURL('/GmSalesYTDReportServlet?hAction=LoadAccountA&hGroupAccId='+GroupAccId,"Historical Sales Group Account Report");
	  return false;
}
function fnPartNumberSearch(){
	  w2 =  createDhtmlxWindow(1300,680,true);
	  w2.setText("Part Detail Report");
	  w2.attachURL('/GmPageControllerServlet?strPgToLoad=GmPartSearchServlet&PricingReqFlag=Y&strOpt=',"Part Detail Report");
	  return false;
	  //windowOpener("/GmPageControllerServlet?strPgToLoad=GmPartSearchServlet&PricingReqFlag=Y&strOpt=","Con","resizable=yes,scrollbars=yes,top=150,left=200,width=680,height=600");
}