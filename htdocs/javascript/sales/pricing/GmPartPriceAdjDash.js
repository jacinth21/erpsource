function fnPageLoad(){
	var statusids = document.frmPartPriceAdj.hStatus.value;
	if (statusids != ''){
		objgrp = document.frmPartPriceAdj.Chk_GrpStatus;
		fnCheckSelections(statusids,objgrp);
	}	
	fnChangeDisplay();
}

//Marks the status checkboxes as 'checked' on PageLoad
function fnCheckSelections(val,objgrp)
{
	var valobj = val.split(",");
	var sval = '';
	var arrlen = objgrp.length;
	
	if (arrlen > 0){
		for (var j=0;j< valobj.length;j++ ){
			sval = valobj[j];
			for (var i=0;i< arrlen;i++ ){
				if (objgrp[i].value == sval){
					objgrp[i].checked = true;
					break;
				}
			}
		}
	}
	else{
		objgrp.checked = true;
	}
}

function Toggle(val){
	var obj = eval("document.all.div"+val);	
	var trobj = eval("document.all.tr"+val);
	var tabobj = eval("document.all.tab"+val);	
	
	if (obj.style.display == 'none')	{
		obj.style.display = '';
		trobj.className= "ShadeRightTableCaptionBlue";
		tabobj.style.background="#ecf6fe";
	}else{
		obj.style.display = 'none';
		trobj.className= "";
		tabobj.style.background="#ffffff";
	}
}

function fnEditTxn(txnId,partyId){
	fnStartProgress('Y');
	document.frmPartPriceAdj.action = "/gmPartPriceAdjLoad.do?method=fetchPartPriceDtls&partyId="+partyId+"&txnId="+txnId+"&type=&accId=";
	document.frmPartPriceAdj.submit();
}

function fnOpenLog(id){
	windowOpener("/GmCommonLogServlet?hType=4000898&hID="+id,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
}

function fnLoadDash(){
	fnValidateDates();	
	var type = document.all.type.value;
	var accID = '';
	if(type == 903110){ // Account
		accID = document.all.accName.value;
	}else if(type == 903111){ // Group Price Book
		accID = document.all.grpAccName.value;
	}	
	var lblName = (type == 903110) ? '  <B>Account Name</B>' : ' <B>Group Book Name</B>';
	
	if((accID == '' || accID == '0') && (type != '0')){
		Error_Details(Error_Details_Trans(message_sales[148],lblName));
	}	
	document.frmPartPriceAdj.strOpt.value = 'ReLoad';
	fnCheckSelectStatus();
	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}else{
		fnStartProgress('Y');
		document.frmPartPriceAdj.action = "/gmPartPriceAdjDash.do?method=loadPartPriceAdjDtls";
		document.frmPartPriceAdj.submit();
	}
}
function fnCheckSelectStatus(){
	var objChkStatus = document.frmPartPriceAdj.Chk_GrpStatus;
	var chkstr = '';
	
	if (objChkStatus){
		var statuslen = objChkStatus.length;
	}	
	if (objChkStatus){	
		for (var i=0 ; i < statuslen ; i++){
			if (objChkStatus[i].checked == true){				
				chkstr += objChkStatus[i].value + ',';
			}
		}
		document.all.hStatus.value = chkstr.substr(0,chkstr.length-1);
	}
}

function fnValidateDates(){
	var dateField = document.frmPartPriceAdj.dateFld.value;
	var fieldName = '';
	if(dateField == '105300'){
		fieldName = 'Initiated ';
	}else{
		fieldName = 'Implemented ';
	}	
	objStartDt = document.frmPartPriceAdj.startDt;
	objEndDt = document.frmPartPriceAdj.endDt;
	
	var dateDiffVal = dateDiff(todaysDate,objStartDt.value,format);
	var fromToDiff = dateDiff(objStartDt.value, objEndDt.value, format);
	var fromCurrDiff = dateDiff(objStartDt.value, todaysDate, format);
	var toCurrDiff  = dateDiff(objEndDt.value, todaysDate, format);
	
	
	if(objStartDt.value != ""){
		var array=[fieldName,format];
		CommonDateValidation(objStartDt,format,Error_Details_Trans(message_sales[149],array));
	}
	if(objEndDt.value != ""){
		var array=[fieldName,format];
		CommonDateValidation(objEndDt,format,Error_Details_Trans(message_sales[150],array));
	}
	var dtFrom = document.frmPartPriceAdj.startDt.value;
    var dtTo = document.frmPartPriceAdj.endDt.value;

     if(fromToDiff < 0){
    	 var array=[fieldName,format];
    	 Error_Details(Error_Details_Trans(message_sales[151],array));
	 }	
}