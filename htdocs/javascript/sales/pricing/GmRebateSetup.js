function fnOnPageLoad() {
	var partyId = document.frmRebateSetup.partyId.value;
	if(partyId == ""){
	document.frmRebateSetup.rebateTypeId.value = '107485';
	document.frmRebateSetup.rebatePartTypeId.value = '107481';
    }
	if(activeFlag == "Y"){
		document.frmRebateSetup.activeFl.checked = true;
	}else{
		document.frmRebateSetup.activeFl.checked = false;
	}
	fnChange();
	fnRebateParts();
}
//Show and Hide Tier Setup Screen
function fnChange(){
	var category = document.frmRebateSetup.rebateCategoryId.value;
	var divTierSetup = document.getElementById("divTierSetup")
		if(category == "107483"){
			divTierSetup.style.display = "block";
		}else{
			divTierSetup.style.display = "none";
			document.frmRebateSetup.tier1Percent.value="";
			document.frmRebateSetup.tier1FromAmt.value="";
			document.frmRebateSetup.tier1ToAmt.value="";
			document.frmRebateSetup.tier2Percent.value="";
			document.frmRebateSetup.tier2FromAmt.value="";
			document.frmRebateSetup.tier2ToAmt.value="";
			document.frmRebateSetup.tier3Percent.value="";
			document.frmRebateSetup.tier3FromAmt.value="";
			document.frmRebateSetup.tier3ToAmt.value="";
		}
	}
//to Validate Each Field
	function fnValidateRebateSetup(){
		
		var contractFromDate = document.frmRebateSetup.rebateFromDate;
		var contractToDate = document.frmRebateSetup.rebateToDate;
		var effectiveDate = document.frmRebateSetup.rebateEffectiveDate;
		var fromDt=contractFromDate.value;
		var toDate=contractToDate.value;
		var effectiveDt =effectiveDate.value;
		var rebateRate=  TRIM(document.frmRebateSetup.rebateRate.value);
		var category = document.frmRebateSetup.rebateCategoryId.value;
		var tier1Per=  parseInt(document.frmRebateSetup.tier1Percent.value);
		var tier1FrmAmt=  parseInt(document.frmRebateSetup.tier1FromAmt.value);
		var tier1ToAmt=  parseInt(document.frmRebateSetup.tier1ToAmt.value);
		var tier2Per=  parseInt(document.frmRebateSetup.tier2Percent.value);
		var tier2FrmAmt=  parseInt(document.frmRebateSetup.tier2FromAmt.value);
		var tier2ToAmt=  parseInt(document.frmRebateSetup.tier2ToAmt.value);
		var tier3Per=  parseInt(document.frmRebateSetup.tier3Percent.value);
		var tier3FrmAmt=  parseInt(document.frmRebateSetup.tier3FromAmt.value);
		var tier3ToAmt=  parseInt(document.frmRebateSetup.tier3ToAmt.value);
		var rebateId = document.frmRebateSetup.rebateId.value;

		fnValidateDropDn('groupTypeId',"<b>Account Type</b>");
		fnValidateDropDn('rebatePartTypeId',"<b>Rebate Part Type</b>");
		fnValidateDropDn('rebateCategoryId',"<b>Rebate Category Id</b>");
		fnValidateDropDn('rebateTypeId'," <b>Rebate Type</b>");
		fnValidateTxtFld('rebateRate',"<b>Rebate Rate</b>");
		fnValidateTxtFld('partyId',"<b>Name</b>");
		fnValidateTxtFld('rebateEffectiveDate','<b>Effective Date</b>');
		fnValidateTxtFld('rebateFromDate',"<b>Contract From Date</b>");
		fnValidateTxtFld('rebateToDate',"<b>Contract To Date</b>");
		fnValidateTxtFld('txt_LogReason',message_accounts[203]);
		
		NumberValidation(tier1Per,"Please Enter Numeric Values for  Tier1 Percentage");
		NumberValidation(tier1FrmAmt,"Please Enter Numeric Values for  Tier1 From Amount");
		NumberValidation(tier1ToAmt,"Please Enter Numeric Values for  Tier1 To amount");
		NumberValidation(tier2Per,"Please Enter Numeric Values for  Tier2 Percentage");
		NumberValidation(tier2FrmAmt,"Please Enter Numeric Values for  Tier2 From amount");
		NumberValidation(tier2ToAmt,"Please Enter Numeric Values for  Tier2 To amount");
		NumberValidation(tier3Per,"Please Enter Numeric Values for  Tier3 Percentage");
		NumberValidation(tier3FrmAmt,"Please Enter Numeric Values for  Tier3 From Amount");
		NumberValidation(tier3ToAmt,"Please Enter Numeric Values for  Tier3 To amount");
		
		if (rebateRate <= 0) {
			Error_Details("<b>Rebate Rate</b> must be greater than 0");
		} 
		
		if(rebateRate >= 100){
			Error_Details("<b>Rebate Rate</b> must be less than 100");
		}
		
		var exp = /^\d+(\.\d{1,2})?$/;
		var objval = exp.test(rebateRate);
		if(rebateRate !=""){
			if (isNaN(rebateRate)) {
				Error_Details("Please Enter Numeric Values for <b>Rebate Rate</b>");
			}else if(!objval){	
			Error_Details("Please enter Rebate Rate with maximum of 2 decimal values");
			} 
		}
	   if((fromDt == "") && (toDate != "")){
			fnValidateTxtFld('rebateFromDate',"<b>Contract From Date<b/>");
		}else if((fromDt != "") && (toDate == "")){
			fnValidateTxtFld('rebateToDate',"<b>Contract To Date<b/>");
		}
	  
		CommonDateValidation(contractFromDate,format,'Please Enter Valid <b>Contract From Date<b/> Format');
		CommonDateValidation(contractToDate,format,'Please Enter Valid <b>Contract To Date</b> Format');
		CommonDateValidation(effectiveDate,format,'Please Enter Valid <b>Effective Date</b>');
		
		if (dateDiff(fromDt, toDate, format) < 0)
		{
			Error_Details("<b>Contract From Date</b> cannot be greater than <b>Contract To Date</b>");
		}
		if (dateDiff(effectiveDt, toDate, format) < 0)
		{
			Error_Details("<b>Effective Date</b> must be less than <b>Contract To Date</b>");
		}
		
		if(dateDiff(todayDate,effectiveDt,format)< 0 && (rebateId =="" ||(rebateEffectiveDt != effectiveDt))){
			Error_Details("<b>Effective Date</b> must be current or future date");
		}
		
		if((tier1FrmAmt > tier1ToAmt)||(tier2FrmAmt > tier2ToAmt)||(tier3FrmAmt > tier3ToAmt)){
			Error_Details("<b>Tier From Amount</b> Should  be Less than <b>Tier To Amount</b> ");
		}
		
	}
	//to save rebate Account Details
	function fnSubmit(){
		fnValidateRebateSetup();
		if (ErrorCount > 0)
		{
			Error_Show();
			Error_Clear();
			return false;
		}
		document.frmRebateSetup.action="/gmRebateSetup.do?method=saveRebateManagmentDtls";
		fnStartProgress();
		document.frmRebateSetup.submit();   
	}
	//load the Account details 
	function fnLoadPartDtls()
	{
	  var partyId = document.frmRebateSetup.partyId.value;

      document.frmRebateSetup.strOpt.value="Load";
      document.frmRebateSetup.action="/gmRebateSetup.do?method=loadRebateManagmentDtls&partyId="+partyId;
	  fnStartProgress();
	  document.frmRebateSetup.submit();
		
	}	
	//to reset the values.
	function fnReset(obj){
		location.href= '/gmRebateSetup.do?method=loadRebateManagmentDtls';
	}
	
	function fnRebateParts(){
		var rebatePartType =document.frmRebateSetup.rebatePartTypeId.value;
		if(rebatePartType=='107481'){
			document.frmRebateSetup.BTN_ADD_PARTS.disabled = false;
			document.frmRebateSetup.BTN_VIEW_PARTS.disabled = false;
		}else{
			document.frmRebateSetup.BTN_ADD_PARTS.disabled = true;
			document.frmRebateSetup.BTN_VIEW_PARTS.disabled = true;
		}
	}
	//Add new Button:To add parts
	function fnAddNewParts(){

		var rebateId =document.frmRebateSetup.rebateId.value;
		if(rebateId!=''&&rebateId!=undefined&&rebateId!="0"){
			
			var accName=document.frmRebateSetup.groupTypeId.options[document.frmRebateSetup.groupTypeId.selectedIndex].text;
			document.frmRebateSetup.action = '/gmRebateSetup.do?method=loadRebatepartMappingDtls&haccountTypeName='+accName+'&rebateId='+rebateId;
			document.frmRebateSetup.submit();	
		}else{
			Error_Details("Rebate Transaction should be submitted before adding parts");
		}
		if (ErrorCount > 0)
		{
			Error_Show();
			Error_Clear();
			return false;
		}
	}
	
	function fnViewParts(){
		var rebateId =document.frmRebateSetup.rebateId.value;
	    if(rebateId!=''&&rebateId!=undefined&&rebateId!="0"){
		var groupTypeName=document.frmRebateSetup.groupTypeId.options[document.frmRebateSetup.groupTypeId.selectedIndex].text;
		var partyName = document.frmRebateSetup.searchpartyId.value;
		var effectiveDate = document.frmRebateSetup.rebateEffectiveDate.value;
		var contractToDate = document.frmRebateSetup.rebateToDate.value;
		windowOpener('/gmRebateReport.do?method=viewRebateparts&rebateId='+rebateId+'&groupTypeName='+groupTypeName+'&partyName='+partyName+'&effectiveDate='+effectiveDate+'&contractToDate='+contractToDate, "Pack","resizable=yes,scrollbars=yes,top=150,left=150,width=850,height=610");
		}else{
			Error_Details("Rebate Transaction should be submitted before viewing parts");
		}
		if (ErrorCount > 0)
		{
			Error_Show();
			Error_Clear();
			return false;
		}
	}
	
// to reset the value
	function fnChangeAccSource(grpType){
		location.href = '/gmRebateSetup.do?method=loadRebateManagmentDtls&groupTypeId='+grpType;	
	} 

	/*
	 * This function used to load the Rebate Audit trail information
	 * Parameter Type - Based on Rebate field - Type passed dynamically
	 * 
	 */

	function fnHistory(type) {
		var rebateId = document.frmRebateSetup.rebateId.value;
		
		windowOpener(
				"/GmPageControllerServlet?strPgToLoad=/gmAuditTrail.do&auditId=''&txnId="
						+ rebateId
						+ "&strDynamicAuditTrailFl=Y&strProcedureRuleId=REBATE_MASTER&strAuditType="
						+ type, "",
				"resizable=yes,scrollbars=yes,top=250,left=300,width=785,height=300,status=1");
	}

