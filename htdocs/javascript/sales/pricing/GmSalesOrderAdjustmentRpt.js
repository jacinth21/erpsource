function initGridData(divRef,gridData){
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);
	gObj.enableTooltips("true,true,true,true,true,true,true,true");
	gObj.init();	
	gObj.loadXMLString(gridData);	
	return gObj;	
}

function fnOnPageLoad() {
	alert();
	if (objGridData != '') {
		gridObj = initGridData('AdjustmentReport',objGridData);
	}
}

function fnSetCostHistory(setid){		
		windowOpener("/GmPageControllerServlet?strPgToLoad=/gmAuditTrail.do&auditId=1048&txnId="+ setid, "","resizable=yes,scrollbars=yes,top=250,left=300,width=785,height=300,status=1");

}

function fnDownloadXLS(){
		gridObj.toExcel('/phpapp/excel/generate.php');	
	
function fnClosePopup(){
	window.close();
}
