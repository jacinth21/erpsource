function fnSubmit(screenType) {
	var wasted = document.getElementById('wasted').value;
	var revision = document.getElementById('revision').value;
	var wastedPrice = document.getElementById('wastedPrice').value;
	var revisionPrice = document.getElementById('revisionPrice').value;
	var gPo = '';
	var grpBookId = '';
	var priceAdjInputStr = '';
	var objAdFee =/^\d{0,2}(\.\d{1,2})?$/;
	var strId = '';
	var strDetails = '';
	var hlthId = '';
	var rpc = '';
	fnValidateTxtFld('txt_LogReason', message_sales[185]);
	// Validation for not allowing the Text, spaces and special characters for
	// Adjustment Value.
	var objRegExp = /(^-?\d\d*$)/;

	if ((!wasted == '' && !objRegExp.test(wasted))
			|| (!revision == '' && !objRegExp.test(revision))) {
		Error_Details(message_sales[180]);
	}

	// Validation for not allowing more than 0 and less than 0 numbers and Price
	// List drop-down validation.
	if (wasted != '') {
		fnValidateDropDn('wastedPrice', ' Wasted Price List ');
		if (wasted < 0 || wasted > 100) {
			Error_Details(message_sales[181]);
		}
	} else {
		if (wastedPrice != '0')
			Error_Details(message_sales[182]);
	}
	if (revision != '') {
		fnValidateDropDn('revisionPrice', ' Revision Price List ');
		if (revision < 0 || revision > 100) {
			Error_Details(message_sales[245]);
		}
	} else {
		if (revisionPrice != '0')
			Error_Details(message_sales[183]);
	}

	if ((accountID == '0' || accountID == '')
			&& (partyID == '0' || partyID == '')) {
		Error_Details(message_sales[184]);
	}

	if (screenType != 'groupAccountSetup') {
		
		
		 gPo = document.frmPricingParamsSetup.gpo.value;
		 grpBookId = document.frmPricingParamsSetup.grouppricebook.value;
		 strId = document.frmPricingParamsSetup.strId.value;
		 strDetails = document.frmPricingParamsSetup.strDetails.value;
		 //PC-2039 Add new fields on the Pricing Parameter tab in Account Setup screen
		 rpc = document.frmPricingParamsSetup.rpc.value;
		 hlthId = document.frmPricingParamsSetup.hlthId.value;
		 //commenting the code related to admin fee and admin fee flag 
	
	}


	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return true;
	}
	if (screenType != 'groupAccountSetup') {
		// Account Affiliation values

		var iDn = document.frmPricingParamsSetup.idn.value;
		var contrVal = document.frmPricingParamsSetup.strContract.value;
		var strCollectorId = '';
		var accGroup = '';
		var AccAttInputStr = '';
		// 1006432:Group Pricing Affiliation,1006433:Health Care Affiliation,
		// 903103:Contract Flag, 7008:Group
		if(iDn=='0'){
			iDn='';
		}
		if(gPo=='0'){
			gPo='';
		}
		if(contrVal=='0'){
			contrVal='';
		}
		if(rpc=='0'){
			rpc='';
		}
		if(hlthId=='0'){
			hlthId='';
		}
		AccAttInputStr = iDn + '|' + gPo + '|' + contrVal + '|' + rpc
				+ '|' + hlthId + '|' + strId + '|' + strDetails + '|';
				
		document.frmPricingParamsSetup.hAccAttInputStr.value = AccAttInputStr;
	}
	// Preparing Input String for Wasted Adjustment Values[107970] and Revision
	// Adjustment Value[107971].
	priceAdjInputStr = priceAdjInputStr + '107970' + '^' + wasted + '^'
			+ wastedPrice + '^' + wstPrcID + '|' + '107971' + '^' + revision
			+ '^' + revisionPrice + '^' + reivPrcID + '|';
	document.frmPricingParamsSetup.hPriceAdjInputStr.value = priceAdjInputStr;
	document.frmPricingParamsSetup.action = '/gmPricingParamsSetup.do?method=savePricingParams&accid='
			+ accountID
			+ "&partyId="
			+ partyID
			+ "&grouppricebook="
			+ grpBookId;
	fnStartProgress('Y');
	document.frmPricingParamsSetup.submit();
}

function fnWasteHistory() {
	windowOpener(
			"/gmPricingParamsSetup.do?method=fetchPriceParamHystory&auditTrailId=1234&auditTrailTransID="
					+ wstPrcID, "",
			"resizable=yes,scrollbars=yes,top=400,left=500,width=785,height=300");
}

function fnRevisionHistory() {
	windowOpener(
			"/gmPricingParamsSetup.do?method=fetchPriceParamHystory&auditTrailId=1234&auditTrailTransID="
					+ reivPrcID, "",
			"resizable=yes,scrollbars=yes,top=500,left=300,width=785,height=300");
}
