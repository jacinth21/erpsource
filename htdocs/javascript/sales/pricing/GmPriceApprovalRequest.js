var systemLists;
var projectLists;

//Grid Initialization
function initGridData(divRef,gridData){
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableTooltips("true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true");
	//FUnction for making the Status, Change Type, CHnage Value and Proposed
	//columns disabled for System Name Grouped Rows.
	gObj.attachEvent("onRowCreated", function(rowId){		
		status_rowId  = gObj.getColIndexById("statusid");
		priceType_rowId  = gObj.getColIndexById("changeType");
		priceTypeVal_rowId  = gObj.getColIndexById("changeValue");
		propPrice_rowId = gObj.getColIndexById("ProposePart");
		
		//Disabling the Status Cell for System Group Headers
		var status_cell = gObj.cells(rowId,status_rowId);
	    if (status_cell.getAttribute("disabled")){
	    	status_cell.setDisabled(true);
	    }
	    //Disabling the Change Type for System Group Headers
	    var priceType_cell = gObj.cells(rowId,priceType_rowId);
	    if (priceType_cell.getAttribute("disabled")){
	    	priceType_cell.setDisabled(true);
	    }
	    //Disabling the Change Value for System Group Headers
	    var priceVal_cell = gObj.cells(rowId,priceTypeVal_rowId);
	    if (priceVal_cell.getAttribute("disabled")){
	    	priceVal_cell.setDisabled(true);
	    }
	    //Disabling the Proposed Price Cell for System Group Headers
	    var propPrice_cell = gObj.cells(rowId,propPrice_rowId);
	    if (propPrice_cell.getAttribute("disabled")){
	    	propPrice_cell.setDisabled(true);
	    }	  
	}); 
	//Code for showing the Status dropdown at Header level
	gObj._in_header_custom_drpdown = function(tag,index,data){   //this
																	//function
																	//is to add
																	//the
																	//dropdown
																	//in grid
																	//header
		optionstr = '<option value=""></option>';
        optionstr = optionstr + "<option value=52186>Initiated</option>";
        optionstr = optionstr + "<option value=52187>Pending AD Approval</option>";
        optionstr = optionstr + "<option value=52123>Pending PC Approval</option>";
        optionstr = optionstr + "<option value=52125>Approved</option>";
        optionstr = optionstr + "<option value=52120>Denied</option>";
        optionstr = optionstr + "<option value=52124>Pending</option>";
		tag.innerHTML='<select name=masterStatus class=RightText tabindex="-1" onChange="fnPopulateValue(this);">' + optionstr +'</select>';
     }
	gObj.init();	
	gObj.loadXMLString(gridData);
	gObj.enableHeaderMenu();
	return gObj;	
}

//To populate the value in dropdown based on the value selected in header
//dropdown
function fnPopulateValue(obj){
	var value = obj.value;
	var statusName = '';
	if(statUpdAccFl != 'Y'){
		Error_Details("You do not have access to perform <B>Status</B> update action");		
	}else{
		if(value == 52125){ //APPROVED
			statusName = 'Approve';
		}else if(value == 52120){ //DENIED
			statusName = 'Denied';
		}else if(value == 52123){ //DENIED
			statusName = 'Pending PC Approval';
		}else if(value == 52187){ //DENIED
			statusName = 'Pending AD Approval';
		}	
		if((value == 52125 && apprvlAccess != 'Y') || (value == 52120 && denyAccess != 'Y') || (value == 52123 && pcAccess != 'Y') || (value == 52187 && advpAccess != 'Y')){
			Error_Details("You do not have access to perform <B> "+ statusName + " </B> action");
		}
	}	
	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}
	gridObj.forEachRow(function(rowId){
	//set all the value to approve; 1400: Approve
	status_rowId  = gridObj.getColIndexById("statusid");
	implCHK_rowId = gridObj.getColIndexById("Implementcheck");
	var status_id = gridObj.cellById(rowId, status_rowId).getValue();
	savedstatusid = gridObj.getUserData(rowId,"hStatusId");
	if(status_id != '' && (savedstatusid != '52125' )){
		gridObj.cellById(rowId, 0).setValue(value);
		gridObj.cells(rowId, status_rowId).cell.wasChanged=true;
		if(value == '52125' && apprvlAccess == 'Y'){
			gridObj.cellById(rowId, implCHK_rowId).setDisabled(false);
		}else{
			gridObj.cellById(rowId, implCHK_rowId).setDisabled(true);
		}
	}
	});
}

//Description : This function to populate the Price Request details by hitting
//the LOAD button
function fnLoad(){
	var priceReqId = document.frmPriceApprovalRequest.priceRequestId.value;
	var rebateVal = document.frmPriceApprovalRequest.rebate.value;	
	if(priceReqId == '' || priceReqId == '0'){
		Error_Details("Please enter valid <B>Pricing Request ID</B>");
	}
	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}else{
		fnStartProgress();
		document.frmPriceApprovalRequest.action = "/gmPriceApprovalRequestAction.do?method=fetchPriceReqDetails&strOpt=Load&priceRequestId="+priceReqId.toUpperCase();
		document.frmPriceApprovalRequest.submit();
	}
}

//Description : This function to load the Grid
function fnOnPageLoad() {
	document.frmPriceApprovalRequest.priceRequestId.focus();
	if (objGridData != '') {
		var rebateVal = document.frmPriceApprovalRequest.rebate.value;
		gridObj = initGridData('PriceReqDetails',objGridData);
		if(rebateVal == '0' || rebateVal == ''){
			gridObj.attachHeader('#custom_drpdown,#rspan,#rspan,#rspan,#rspan,Part,Construct,#rspan,Part,Construct,CHK,Part,Construct,% Off List,Part,Construct,'+"<input type='checkbox' value='no' name='selectImplAll' onClick='javascript:fnChangedFilter(this);'/>");			
		}else{
			gridObj.attachHeader('#custom_drpdown,#rspan,#rspan,#rspan,#rspan,Part,Construct,Part,Construct,Part,Construct,CHK,Part,Construct,% Off List,Part,Construct,#rspan,'+"<input type='checkbox' value='no' name='selectImplAll' onClick='javascript:fnChangedFilter(this);'/>");
		}		
		gridObj.attachEvent("onEditCell", doOnCellEdit);
		gridObj.attachEvent("onCheckbox", doOnCheck);
	}
	
	system_rowId  = gridObj.getColIndexById("Systemid");
	status_rowId  = gridObj.getColIndexById("statusid");
	propPrice_rowId = gridObj.getColIndexById("ProposePart");
	propPriceCons_rowId = gridObj.getColIndexById("ProposeConstruct");
	priceType_rowId  = gridObj.getColIndexById("changeType");
	priceTypeVal_rowId  = gridObj.getColIndexById("changeValue");
	listPrice_rowId = gridObj.getColIndexById("ListPart");
	qty_rowId = gridObj.getColIndexById("priceQty");
	currentPrice_rowId = gridObj.getColIndexById("CurrentPart");
	currentPriceconst_rowId = gridObj.getColIndexById("CurrentConstruct");
	tripPrice_rowId = gridObj.getColIndexById("TripwirePart");
	tripPriceCHK_rowId = gridObj.getColIndexById("Tripwirecheckflag");
	propRebatePrice_rowId = gridObj.getColIndexById("ProposeRebatePart");
	propRebatePriceCons_rowId = gridObj.getColIndexById("ProposeRebateConstruct");
	listOffList_rowId = gridObj.getColIndexById("ListOffList");
	listprice_rowId = gridObj.getColIndexById("ListPart");
	listConstruct_rowId = gridObj.getColIndexById("ListConstruct");
	groupid_rowId = gridObj.getColIndexById("groupid");
	implCHK_rowId = gridObj.getColIndexById("Implementcheck");
	implementChkid_rowId = gridObj.getColIndexById("implementChkid");
	constructlevel_rowId = gridObj.getColIndexById("constructID");
	constructName_rowId  = gridObj.getColIndexById("constructName");
	tripConstruct_rowId  = gridObj.getColIndexById("TripwireConstruct");
	log_rowId            = gridObj.getColIndexById("log");
	
	gridObj.forEachRow(function(rowId) {		
		gridObj.cellById(rowId, implCHK_rowId).setDisabled(true);
		gridObj.setItemImage(rowId,"");
		status_id = gridObj.cellById(rowId, status_rowId).getValue();
		impl_chkval = gridObj.cellById(rowId, implCHK_rowId).getValue();
		gridObj.setRowTextStyle(rowId,"height:10px;font-size: 11px;");
		chkFlid = gridObj.getUserData(rowId,"hImplChkFl");        //gridObj.cellById(rowId,
																	//implementChkid_rowId).getValue();
		if((status_id == 52125 && apprvlAccess == 'Y')){ //52125 Approved
															//52189 Approved
															//Not Implemented
			gridObj.cellById(rowId, implCHK_rowId).setDisabled(false);
			gridObj.cellById(rowId, priceType_rowId).setDisabled(true);
			gridObj.cellById(rowId, priceTypeVal_rowId).setDisabled(true);
			gridObj.cellById(rowId, propPrice_rowId).setDisabled(true);		
			if(chkFlid == 'Y'){
				gridObj.cellById(rowId, implCHK_rowId).setChecked(true);
				gridObj.cellById(rowId, status_rowId).setDisabled(true);
				gridObj.cellById(rowId, implCHK_rowId).setDisabled(true);
			}else{
				gridObj.cellById(rowId, implCHK_rowId).setChecked(false);
				gridObj.cellById(rowId, status_rowId).setDisabled(false);	
			}
		}else{
			gridObj.cellById(rowId, priceType_rowId).setDisabled(false);
			gridObj.cellById(rowId, priceTypeVal_rowId).setDisabled(false);
			gridObj.cellById(rowId, propPrice_rowId).setDisabled(false);		
		}
		
		var tripPrice = parseInt(gridObj.cellById(rowId, tripPrice_rowId).getValue().trim());
		var propPrice = parseInt(gridObj.cellById(rowId, propPrice_rowId).getValue().trim());
		
		if(propPrice == '' || propPrice == '0' ){
			gridObj.cellById(rowId, tripPriceCHK_rowId).setValue("");
			gridObj.setCellTextStyle(rowId,tripPriceCHK_rowId,"color:;font-weight:bold;background-color:white;");
		}else if(propPrice < tripPrice){			
			gridObj.cellById(rowId, tripPriceCHK_rowId).setValue("NO");
			gridObj.setCellTextStyle(rowId,tripPriceCHK_rowId,"color:red;background-color:white;");
			fnPopulateTripwireChkFlag(rowId);	
		}else if(propPrice >= tripPrice){
			gridObj.cellById(rowId, tripPriceCHK_rowId).setValue("YES");
			gridObj.setCellTextStyle(rowId,tripPriceCHK_rowId,"color:green;background-color:white;");
			fnPopulateTripwireChkFlag(rowId);			
		}
		var chkFl = gridObj.cellById(rowId, tripPriceCHK_rowId).getValue();
		if(chkFl != ''){
			gridObj.setCellTextStyle(rowId,qty_rowId,"font-weight:bold;background-color:white;");
		}else{
			gridObj.setCellTextStyle(rowId,qty_rowId,"font-weight:bold;background-color:white;");
		}		
		var changeType = gridObj.cellById(rowId, priceType_rowId).getValue();
		if(changeType == '52020')
			gridObj.cellById(rowId, priceTypeVal_rowId).setDisabled(true);
				
		var status = gridObj.cellById(rowId, status_rowId).getValue();				
		var child_status = gridObj.cellById(rowId, status_rowId).getValue();
		if(status == 'Parent'){
			gridObj.cellById(rowId, status_rowId).setValue("");
			fnSetRowTypes(rowId);
			fnSetRowStyles(rowId);
		}
		if(child_status == ''){
			fnSetRowTypes(rowId);
		}
		
		if(status_id == '52125')
			gridObj.cellById(rowId, status_rowId).setDisabled(true);

		
	}); 
	
}

function validateAccess(newStatus,oldStatus,rowID){
	var statusName = '';
	if(statUpdAccFl != 'Y'){
		Error_Details("You do not have access to perform <B>Status</B> update action");
		gridObj.cellById(rowID, status_rowId).setValue(oldStatus);
	}else{
		if(newStatus == 52125){ //APPROVED
			statusName = 'Approve';
		}else if(newStatus == 52120){ //DENIED
			statusName = 'Denied';
		}else if(newStatus == 52123){ //DENIED
			statusName = 'Pending PC Approval';
		}else if(newStatus == 52187){ //DENIED
			statusName = 'Pending AD Approval';
		}	
		if((newStatus == 52125 && apprvlAccess != 'Y') || (newStatus == 52120 && denyAccess != 'Y') || (newStatus == 52123 && pcAccess != 'Y') || (newStatus == 52187 && advpAccess != 'Y')){
			Error_Details("You do not have access to perform <B> "+ statusName + " </B> action");
		}
	}
	
	if (ErrorCount > 0){
		gridObj.cellById(rowID, status_rowId).setValue(oldStatus);
		Error_Show();
		Error_Clear();
		return false;
	}
}

//Description : This function to populate the Group individual details
function fnGroupDetail(groupID) { 
	w3 =  createDhtmlxWindow(800,350,true);
	w3.setText("Group Part Mapping"); 
	w3.attachURL("/gmGroupPartMap.do?strDisableLink=Y&strPartListFlag=Y&hviewEdit=view&groupId="+groupID+"&groupNM=","Part Details");
	return false;	
	//windowOpener("/gmGroupPartMap.do?strPricingByGroupFl=Y&strPartListFlag=Y&hviewEdit=view&groupId="+groupID+"&groupNM=","","resizable=yes,scrollbars=yes,top=250,left=300,width=950,height=400");
}

//Description : This function to check the Log comments details
function fnOpenPriceRequestLog(groupid,priceReqId){	
	var grpPriceId = groupid +'@'+ priceReqId ;
	windowOpener("/GmCommonLogServlet?hType=1252&hID="+grpPriceId,"TransLog","resizable=yes,scrollbars=yes,top=550,left=910,width=690,height=300");
}

//Description : This function to update the modified price details
function fnGo(){	
	var actionType = document.frmPriceApprovalRequest.Cbo_Action1.value;
	
	if(actionType == '0'){
		Error_Details("Please select at least one value from <B> Choose Action </B>");
	}else if(actionType == 'EXPEXCEL'|| (actionType == 'EXPEXCELOUS')){
		document.frmPriceApprovalRequest.action ="/gmPriceApprovalRequestAction.do?method=pricingDetailsExportToExcel&actionType="+actionType;
		document.frmPriceApprovalRequest.submit();
    }else if((actionType == 'SUBAPPR') || (actionType == 'PRCIMPL')){		
		fnSubmit();	
	}else if(actionType == 'GPOREF'){		
		Error_Details("Selected action is under progress.");
	}else if(actionType == 'GRTPRICE'){
		if(genPrtFile != 'Y'){
			Error_Details("You do not have access to generate price file");
		}
		else{
		   var priceReqId = document.frmPriceApprovalRequest.priceRequestId.value;    
		   windowOpener("/gmPriceApprovalRequestAction.do?method=fetchGeneratePriceFile&priceRequestId=" + priceReqId.toUpperCase(),"GeneratePriceFile","resizable=yes,scrollbars=yes,top=250,left=300,width=1025,height=600");
		}
    }
	else if(actionType == 'GPBCOMPARISON')
	{
		 var priceReqId = document.frmPriceApprovalRequest.priceRequestId.value;
		windowOpener("/gmPriceApprovalRequestAction.do?method=gpoComparison&priceRequestId=" + priceReqId.toUpperCase(),"GPOComparison","resizable=yes,scrollbars=yes,top=250,left=300,width=1000,height=600");
	
	}
	else if(actionType == 'VOIDPRICEREQUEST')
	{
		var priceReqId = document.frmPriceApprovalRequest.priceRequestId.value;
		 if(reqStatus == 52123 || reqStatus == 52121)
	    	{
			    document.frmPriceApprovalRequest.hTxnId.value = priceReqId;
		        document.frmPriceApprovalRequest.hAction.value = "Load";                              
		        document.frmPriceApprovalRequest.hCancelType.value = 'VDPRRS';  
			    document.frmPriceApprovalRequest.action ="/GmCommonCancelServlet"; 
	            document.frmPriceApprovalRequest.submit();
	    	}
	    else
	     {
	    		Error_Details(message[5290]);
	     }
	
	}
	else if(actionType == 'INTSTATUS')
	{
	    var priceReqId = document.frmPriceApprovalRequest.priceRequestId.value;
	   if(reqStatus != 52121)
	    	{
	    	Error_Details("Pricing request cannot rollback.");
	    	}
	   else
	    {
		    document.frmPriceApprovalRequest.hTxnId.value = priceReqId;
	        document.frmPriceApprovalRequest.hAction.value = "Load";                              
	        document.frmPriceApprovalRequest.hCancelType.value = 'RLBPRT';  
		    document.frmPriceApprovalRequest.action ="/GmCommonCancelServlet"; 
            document.frmPriceApprovalRequest.submit();
	}
	}
	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}
	
}

function fnFileUpload()
{
	var priceReqId = document.frmPriceApprovalRequest.priceRequestId.value;    
	windowOpener("/gmPriceApprovalRequestAction.do?method=fetchUploadFileList&priceRequestId=" + priceReqId.toUpperCase(),"UploadFile","resizable=yes,scrollbars=yes,top=250,left=300,width=1050,height=600");
}


//Description : This function to update the modified price details
function fnSubmit(){	
	var priceReqId = document.frmPriceApprovalRequest.priceRequestId.value;    
	var gridrows = gridObj.getChangedRows(",");
	if(apprvlAccess != 'Y'){
		Error_Details("You do not have access to perform <B> Submit </B> action");
		Error_Show();
		Error_Clear();
		return false;
	}else{
		if(gridrows.length==0){
			inputStr = '';
		}else{
			var gridrowsarr = gridrows.toString().split(",");
			var gridrowsarrLen = gridrowsarr.length;		
				
			var gridrowid;
			var pendQty =0; 
			var systemid = '';
			var statusid = '';
			var priceType = '';
			var groupId = '';
			var changeType = '';
			var changeValue = '';
			var proposedPrice = '';
			var implFl   = '';
			var inputStr  = '';
			var implInputStr = '';
			var implementFl = 'N';
			for(var rowid = 0; rowid < gridrowsarr.length; rowid++){
				gridrowid = gridrowsarr[rowid];
				systemid = gridObj.getUserData(gridrowid,"hSetId"); //gridObj.cellById(gridrowid,
																	//system_rowId).getValue();
				statusid = gridObj.cellById(gridrowid, status_rowId).getValue();
				priceType =  gridObj.cellById(gridrowid, priceType_rowId).getValue();
				groupId = gridObj.getUserData(gridrowid,"hGroupId"); //gridObj.cellById(gridrowid,
																		//groupid_rowId).getValue();
				prcDetailId = gridObj.getUserData(gridrowid,"hprcDtilId");
				priceValue = gridObj.cellById(gridrowid, priceTypeVal_rowId).getValue();
				proposedPrice = gridObj.cellById(gridrowid, propPrice_rowId).getValue();
				implFl   = gridObj.cellById(gridrowid, implCHK_rowId).getValue();
							
				if(implFl == '1'){
					implementFl = 'Y';
					implInputStr += systemid + '^' + groupId  + '^'+ priceReqId.toUpperCase()  + '^'+ implementFl +  '|';
				}else{
					implementFl = 'N';
					//implInputStr += systemid + '^' + groupId + '^'+
					//priceReqId.toUpperCase() + '^'+ implementFl + '|';
				}
				inputStr += systemid + '^' + statusid + '^'+ priceType  + '^'+ groupId  + '^'+ priceValue.replace(" %","").trim()  + '^'+ proposedPrice  + '^'+ implementFl + '^' + prcDetailId +  '|';
				
			}
		}
		if(inputStr == ''){
			Error_Details("No Group(s) has been edited for updating.");
		}
		if (ErrorCount > 0){
			Error_Show();
			Error_Clear();
			return false;
		}else{
			fnStartProgress();
			document.frmPriceApprovalRequest.inputString.value=inputStr;
			document.frmPriceApprovalRequest.implementString.value=implInputStr;
			//document.frmPriceApprovalRequest.action =
			//"/gmPriceApprovalRequestAction.do?method=updatePriceDetails&priceRequestId="+priceReqId.toUpperCase()+"&inputString="+inputStr+"&implementString="+implInputStr;
			document.frmPriceApprovalRequest.action = "/gmPriceApprovalRequestAction.do?method=updatePriceDetails&priceRequestId="+priceReqId.toUpperCase();
			document.frmPriceApprovalRequest.submit();
		}
	}
}

//Description : This function to fetch the Price Request Details based on the
//mentioned Rebate value
function fnApplyRebate(){
	var rebateVal = document.frmPriceApprovalRequest.rebate.value;
	var priceReqId = document.frmPriceApprovalRequest.priceRequestId.value;
	var objRegExp = /(^-?\d\d*$)/;
	if(!rebateVal == '' && !objRegExp.test(rebateVal)){
		Error_Details("<b>Rebate %</b> cannot be other than number. Please enter valid data.");
	}
	if(rebateVal == ''){
		Error_Details("<b>Rebate %</b> cannot be empty to perform this action or Click on <B>Load</B> to view Price Request details without <B>Rebate %</B>");
	}
	if(rebateVal != '' && priceReqId == ''){
		Error_Details("<b>Pricing Request ID</b> cannot be empty to perform this action.");
	}
	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}else{
		fnStartProgress();
		document.frmPriceApprovalRequest.action = "/gmPriceApprovalRequestAction.do?method=fetchPriceReqDetails&strOpt=Load&priceRequestId="+priceReqId.toUpperCase()+"&rebate="+rebateVal;
		document.frmPriceApprovalRequest.submit();
	}	
}

//Description : This function to fire the fetch of Price Request details by
//clicking on the Enter Key
function fnLoadPriceReqDetails(){
	if(event.keyCode == 13)	{	 
		var HiddenPriceReqId = document.frmPriceApprovalRequest.hiddenPriceReqID.value;
		var priceReqID       = document.frmPriceApprovalRequest.priceRequestId.value;
		if (HiddenPriceReqId != priceReqID){
			var rtVal = fnLoad();
		}else{
			var rtVal = true;
		}
		if(rtVal == false){
			if(!e) var e = window.event;

	        e.cancelBubble = true;
	        e.returnValue = false;

	        if (e.stopPropagation) {
	        	e.stopPropagation();
	            e.preventDefault();
	        }
		}
	}
}

//Description : This function to fire the cell edit event, to modify the price
//details
function doOnCellEdit(stage, rowId, cellInd, nValue, oValue) {
	rowID = rowId;	
	
	if (cellInd == priceType_rowId) {		
		if (stage == 2 && nValue != '0' && nValue != oValue) {
			fnSetPriceValue(nValue,oValue,'PRICETYPE');
		}		
	}else if(cellInd == priceTypeVal_rowId){
		if (stage == 2 && nValue != '0' && nValue != oValue) {
			fnSetPropPrice(nValue,oValue,'PERCENTAGE');
		}		
	}else if(cellInd == propPrice_rowId){		
		if (stage == 2 && nValue != '0' && nValue != oValue) {
			fnSetPropPrice(nValue,oValue,'PROPEDIT');
		}		
	}else if(cellInd == status_rowId){
		if (stage == 2 && nValue != '0' && nValue != oValue) {
			validateAccess(nValue,oValue,rowID);
			fnSetImpleChk(nValue,oValue,rowID);
		}
	}
	/*
	 * if (stage == 2 && cellInd == propPrice_rowId){ //code for modifying the
	 * Total amount by editing the Proposed Price fnreCalculateTotalPrice
	 * (oValue,nValue); }
	 */
	return true;
}

//Description : This function used modify the Proposed , Tripwire CHK flag
//values based on the Price Type selection.
function fnSetPriceValue(newpriceType,oldpriceType,columnType) {
	var priceValue = gridObj.cellById(rowID, priceTypeVal_rowId).getValue();
	var totalQty  = gridObj.cellById(rowID, qty_rowId).getValue();
	priceValue = priceValue.replace(" %","").trim();
	var oldProposedPrice = parseInt(gridObj.cellById(rowID, propPrice_rowId).getValue());
	var parentID = rowID.substring(0,rowID.lastIndexOf("."));
	var totPropPrice = parseInt(gridObj.cellById(parentID, propPriceCons_rowId).getValue());
	
	if(newpriceType == '52020' || newpriceType == '52023' || newpriceType == '52024'){  //52020
																						//[Custom
																						//$] ,
																						//52023
																						//[%
																						//Increase]
																						//,
																						//52024
																						//[ No
																						//Change]
		gridObj.cellById(rowID, propPrice_rowId).setValue('');
		gridObj.cellById(rowID, priceTypeVal_rowId).setValue('');
		gridObj.cellById(rowID, propPriceCons_rowId).setValue('');
		gridObj.cellById(rowID, tripPriceCHK_rowId).setValue('');
		gridObj.setCellTextStyle(rowID,tripPriceCHK_rowId,"background-color:white;");
		gridObj.cellById(rowID, priceTypeVal_rowId).setDisabled(true);
		if(oldProposedPrice != ''){
			totPropPrice = totPropPrice - (totalQty * oldProposedPrice);
		}
		gridObj.cellById(parentID, propPriceCons_rowId).setValue(roundNumber(totPropPrice,2));
	}else if((newpriceType == '52021' || newpriceType  == '52022')){ //52021
																		//[%
																		//Off
																		//List]
																		//,
																		//52022
																		//[% of
																		//Current]
		gridObj.cellById(rowID, priceTypeVal_rowId).setDisabled(false);
		fnSetPropPrice(priceValue,oldpriceType,columnType);
	}else{
		gridObj.cellById(rowID, priceTypeVal_rowId).setDisabled(false);
	}
}

//Description : This function used modify the Proposed , Tripwire CHK flag
//values based on the Price Type selection.
function fnSetPropPrice(newPriceVal,oldPriceVal,culumnType){
	//Validating the entered value from the Proposed Price text box and Chnage
	//value Text box
	fnValidatePrice(newPriceVal,culumnType);
	if (ErrorCount > 0){
		if(culumnType == 'PROPEDIT'){
			gridObj.cellById(rowID, propPrice_rowId).setValue(roundNumber(oldPriceVal,2));
		}else{
			gridObj.cellById(rowID, priceTypeVal_rowId).setValue(oldPriceVal);
		}
		Error_Show();
		Error_Clear();
		return false;
	}else{		
		var oldProposedPrice = parseInt(gridObj.cellById(rowID, propPrice_rowId).getValue());
		var rebateVal = document.frmPriceApprovalRequest.rebate.value;
		newPriceVal = newPriceVal.replace(" %","").trim();
		
		var selectPriceType = gridObj.cellById(rowID, priceType_rowId).getValue();
		var tripPrice = parseInt(gridObj.cellById(rowID, tripPrice_rowId).getValue());
		var totalQty  = gridObj.cellById(rowID, qty_rowId).getValue();
		var listPrice = '';
		//if we select the Off List option we need to get the List Price for
		//rest of the calculation
		if(selectPriceType == '52021'){  ////%Off List
			var actualPrice = parseInt(gridObj.cellById(rowID, listPrice_rowId).getValue());
			if(isNaN(actualPrice))
				actualPrice = 0;
			listPrice = actualPrice;
		}else if(selectPriceType == '52022'){ //if we select the Off Current
												//option we need to get the
												//List Price for rest of the
												//calculation
			var actualPrice = parseInt(gridObj.cellById(rowID, currentPrice_rowId).getValue());	
			if(isNaN(actualPrice))
				actualPrice = 0;
		}else{	//Otherwise we need to catchup the entered Price from the
				//Proposed Price Text box if we select Custom $ from change
				//type
			var actualPrice = parseInt(gridObj.cellById(rowID, propPrice_rowId).getValue());	
			if(isNaN(actualPrice))
				actualPrice = 0;
		}
		var calProposedPrice = 0 ;
		//Need to consider the mentioned Proposed price if we select the Custom
		//$ from the Price Type or directlly if we edit the Proposed Price Text
		//box as its need not to caliculated any percentage.
		if(selectPriceType == '52020' || culumnType == 'PROPEDIT'){
			calProposedPrice = parseInt(newPriceVal);
		}else{
			calProposedPrice = actualPrice  - (actualPrice * newPriceVal /100);
		}
		if(isNaN(calProposedPrice)){
			gridObj.cellById(rowID, propPrice_rowId).setValue('');
			gridObj.cellById(rowID, propPriceCons_rowId).setValue('');
		}else{
			var totalPrice = totalQty * calProposedPrice;	
			//Setting the caliculated Proposed Price and Proposed COnstruct
			//Price to the Grid
			gridObj.cellById(rowID, propPrice_rowId).setValue(roundNumber(calProposedPrice,2));
			gridObj.cellById(rowID, propPriceCons_rowId).setValue(roundNumber(totalPrice,2));
			
		}		
		
		//During Editing the Price Type and Price Type Values cells, we need
		//consider the Old Price and New Price as we setted the above price to
		//the Grid to caliculate the Cell sub totals.
		if(culumnType == 'PERCENTAGE' || culumnType == 'PRICETYPE' ){
			oldPriceVal = oldProposedPrice;
			newPriceVal = calProposedPrice;
		}
		//Populating the CHK cell values as YES/NO based on the below condition
		if(calProposedPrice < tripPrice){
			gridObj.cellById(rowID, tripPriceCHK_rowId).setValue("NO");
			gridObj.setCellTextStyle(rowID,tripPriceCHK_rowId,"color:red;background-color:white;");
		}else if(calProposedPrice > tripPrice){
			gridObj.cellById(rowID, tripPriceCHK_rowId).setValue("YES");
			gridObj.setCellTextStyle(rowID,tripPriceCHK_rowId,"color:green;background-color:white;");
		}
		//Calicualting the Proposed Price and Proposed Construct Price based on
		//the Mentioned Rebate Percentage if any.
		if(rebateVal != '' && rebateVal != '0' ){
			var oldRebatePrice = parseInt(gridObj.cellById(rowID, propRebatePrice_rowId).getValue());
			var propRebatePrice = calProposedPrice - (calProposedPrice * rebateVal /100);
			var totPropRebatePrice = totalQty * propRebatePrice;	
			gridObj.cellById(rowID, propRebatePrice_rowId).setValue(roundNumber(propRebatePrice,2));
			gridObj.cellById(rowID, propRebatePriceCons_rowId).setValue(roundNumber(totPropRebatePrice,2));
			//For populating the cell total amount by editing the Proposed
			//Price.
			fnreCalculateRebateTotalPrice(oldRebatePrice,propRebatePrice);		
			if(selectPriceType == '52021'){ //%Off List
				var listPrceOffList = ((listPrice - propRebatePrice)/ listPrice ) * 100;
				listPrceOffList = roundNumber(listPrceOffList,2)
				gridObj.cellById(rowID, listOffList_rowId).setValue(listPrceOffList);	
			}
		}else{
			var listPrice = parseInt(gridObj.cellById(rowID, listPrice_rowId).getValue());
			if( listPrice != null && newPriceVal != null && listPrice != '' && newPriceVal != ''){
				var listPrceOffList = ((listPrice - newPriceVal)/ listPrice ) * 100;
				listPrceOffList = roundNumber(listPrceOffList,2);
				gridObj.cellById(rowID, listOffList_rowId).setValue(listPrceOffList);	
			}else{
				gridObj.cellById(rowID, listOffList_rowId).setValue('');	
			}
			
						
		}
		if(isNaN(oldPriceVal))
			oldPriceVal = 0;
		if(isNaN(newPriceVal))
			newPriceVal = 0;		
		//For populating the cell total amount by editing the Proposed Price.
		fnreCalculateTotalPrice(oldPriceVal,newPriceVal);		
	}	
}

//Description : This function used to round up the yielded result.
function roundNumber(number, digits) {
	var multiple = Math.pow(10, digits);            
	var rndedNum = Math.round(number * multiple) / multiple;
	return rndedNum;        
}

//For populating the Last 12 Month sales about the correcsponding Account
function fnViewAccountSales(accountid){
	windowOpener("/GmSalesYTDReportServlet?hideCompany=Y&hAction=LoadAccountA&hAccountID="+accountid,"SalesLog","resizable=yes,scrollbars=yes,top=350,left=750,width=1050,height=300");
}

//For populating the cell total amount by editing the Proposed Price.
function fnreCalculateTotalPrice(oValue,nValue){
	var parentID = rowID.substring(0,rowID.lastIndexOf("."));
	var no = parentID.indexOf(".", 0);
	var master = parentID.substring(0,no);
	var child = parseInt(parentID.substring(no+1,parentID.length))+1;
	parentID = master+"."+child;
	var totPropPrice = parseInt(gridObj.cellById(parentID, propPriceCons_rowId).getValue());
	var totTripPrice = parseInt(gridObj.cellById(parentID, tripConstruct_rowId).getValue());
	var totalQty  = gridObj.cellById(rowID, qty_rowId).getValue();
	if(oValue != ''){
		totPropPrice = totPropPrice - (totalQty * oValue);}
	if(nValue != ''){
		totPropPrice = totPropPrice + (totalQty * nValue);}
	
	gridObj.cellById(parentID, propPriceCons_rowId).setValue(roundNumber(totPropPrice,2));	
	if(totPropPrice < totTripPrice){
		gridObj.cellById(parentID, tripPriceCHK_rowId).setValue("NO");
		gridObj.setCellTextStyle(parentID,tripPriceCHK_rowId,"color:red;font-weight:bold;background-color:white;");
	}else{
		gridObj.cellById(parentID, tripPriceCHK_rowId).setValue("YES");
		gridObj.setCellTextStyle(parentID,tripPriceCHK_rowId,"color:green;font-weight:bold;background-color:white;");
	}
}

//For populating the cell total amount by editing the Proposed Price.
function fnPopulateTripwireChkFlag(rowId){
	var parentID = rowId.substring(0,rowId.lastIndexOf("."));
	var rebateVal = document.frmPriceApprovalRequest.rebate.value;	
	gridObj.setCellTextStyle(rowId,status_rowId,"background-color:white;height:10px;font-size: 11px;");
	gridObj.setCellTextStyle(rowId,propPrice_rowId,"background-color:white;height:10px;font-size: 11px;");
	gridObj.setCellTextStyle(rowId,propPriceCons_rowId,"background-color:white;height:10px;font-size: 11px;");
	gridObj.setCellTextStyle(rowId,constructName_rowId,"color:#0330FA;background-color:white;font-size: 11px;");
	gridObj.setCellTextStyle(rowId,qty_rowId,"font-weight:bold;background-color:white;font-size: 11px;");
	gridObj.setCellTextStyle(rowId,priceType_rowId,"background-color:white;font-size: 11px;");
	gridObj.setCellTextStyle(rowId,priceTypeVal_rowId,"background-color:white;font-size: 11px;");
	gridObj.setCellTextStyle(rowId,tripPrice_rowId,"background-color:white;font-size: 11px;");
	gridObj.setCellTextStyle(rowId,tripConstruct_rowId,"background-color:white;font-size: 11px;");
	if(rebateVal != '' && rebateVal != '0' ){
		gridObj.setCellTextStyle(rowId,propRebatePrice_rowId,"background-color:white;font-size: 11px;");
		gridObj.setCellTextStyle(rowId,propRebatePriceCons_rowId,"background-color:white;font-size: 11px;");
	}
	gridObj.setCellTextStyle(rowId,currentPrice_rowId,"background-color:white;font-size: 11px;");
	gridObj.setCellTextStyle(rowId,currentPriceconst_rowId,"background-color:white;font-size: 11px;");
	gridObj.setCellTextStyle(rowId,listOffList_rowId,"background-color:white;font-size: 11px;");
	gridObj.setCellTextStyle(rowId,listprice_rowId,"background-color:white;font-size: 11px;");
	gridObj.setCellTextStyle(rowId,listConstruct_rowId,"background-color:white;font-size: 11px;");
	gridObj.setCellTextStyle(rowId,implCHK_rowId,"background-color:white;font-size: 11px;");
	gridObj.setCellTextStyle(rowId,log_rowId,"background-color:white;font-size: 11px;");
}

//For populating the cell total amount by editing the Proposed Price.
function fnreCalculateRebateTotalPrice(oValue,nValue){
	var parentID = rowID.substring(0,rowID.lastIndexOf("."));
	var totPropPrice = parseInt(gridObj.cellById(parentID, propPriceCons_rowId).getValue());
	var totalQty  = gridObj.cellById(rowID, qty_rowId).getValue();
	if(oValue != ''){
		totPropPrice = totPropPrice - (totalQty * oValue);}
	if(nValue != ''){
		totPropPrice = totPropPrice + (totalQty * nValue);}
	
	gridObj.cellById(parentID, propRebatePriceCons_rowId).setValue(roundNumber(totPropPrice,2));
}

//TO rmove the system from the Pricing Request ID
function fnRemoveSystems(Setid,prcReqId,systemName){	
	if (confirm("Are you sure you want to remove System "+ systemName +" from this Request "+prcReqId+" ?"))	{
		fnStartProgress();
		document.frmPriceApprovalRequest.action = "/gmPriceApprovalRequestAction.do?method=removeSystems&priceRequestId="+prcReqId.toUpperCase()+"&systemId="+Setid;
		document.frmPriceApprovalRequest.submit();	
	}	
}

//For validating the entered Propoased Price and Change Percentage.
function fnValidatePrice(priceVal,culumnType){
	var objRegExp = /(^-?\d\d*$)/;
	var lblName = "Change Value";
		
	if(culumnType == 'PROPEDIT'){
		lblName = "Proposed Price";
	}
	if(!priceVal == '' && !objRegExp.test(priceVal)){
		Error_Details("<b>"+lblName+" </b> cannot be other than number. Please enter valid data.");
	}	
}

//Description : This function to fetch the Price Request Details based on the
//mentioned Price Instruments value
function fnApplyPriceInstrument(){
	var prcInstrument = document.frmPriceApprovalRequest.priceInstruments.value;
	var priceReqId = document.frmPriceApprovalRequest.priceRequestId.value;
	var objRegExp = /(^-?\d\d*$)/;
	if(!prcInstrument == '' && !objRegExp.test(prcInstrument)){
		Error_Details("<b>Price Instruments</b> cannot be other than number. Please enter valid data.");
	}
	if(prcInstrument == ''){
		Error_Details("<b>Price Instruments</b> cannot be empty to perform this action.");
	}
	if(prcInstrument != '' && priceReqId == ''){
		Error_Details("<b>Pricing Request ID</b> cannot be empty to perform this action.");
	}
	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}else{
		Error_Details("Selected action is under progress.");
	}	
}

function fnChangedFilter(obj){
	implCHK_rowId = gridObj.getColIndexById("Implementcheck");
	status_rowId  = gridObj.getColIndexById("statusid");
	if(obj.checked)	{		
		gridObj.forEachRow(function(rowId) {			
			check_id = gridObj.cellById(rowId, implCHK_rowId).getValue();
			status_id = gridObj.cellById(rowId, status_rowId).getValue();
			chkFlid = gridObj.getUserData(rowId,"hImplChkFl"); 
			if(status_id == 52125 && chkFlid != 'Y'){ //Approved
				gridObj.cellById(rowId, implCHK_rowId).setChecked(true);
				gridObj.cells(rowId, implCHK_rowId).cell.wasChanged = true;
			}
	   });
	}else{
		gridObj.forEachRow(function(rowId) {
			check_id = gridObj.cellById(rowId, implCHK_rowId).getValue();
			status_id = gridObj.cellById(rowId, status_rowId).getValue();
			chkFlid = gridObj.getUserData(rowId,"hImplChkFl"); 
			if(chkFlid != 'Y' && status_id == 52125){
				gridObj.cellById(rowId, implCHK_rowId).setChecked(false);
				gridObj.cells(rowId, implCHK_rowId).cell.wasChanged = true;
			}			
	   });			
	}
}

//Description : This function to check the select all (master check box)
function doOnCheck(rowId, cellInd, stage) {
	rowID = rowId;	
	if(cellInd == implCHK_rowId) {
		fnSelectAllToggle('selectImplAll',rowId);
	}
	return true;
}
//Description : This function to check the select all check box
function fnSelectAllToggle( varControl ,rowId){
	var objControl = eval("document.frmPriceApprovalRequest."+varControl);

	var all_row = 0 ;
	var all_rowLen;
	var checked_row = gridObj.getCheckedRows(implCHK_rowId);
	var impl_row = '';
	var finalval;	
	gridObj.forEachRow(function(rowId) {
		status_id = gridObj.cellById(rowId, status_rowId).getValue();
		check_id = gridObj.cellById(rowId, implCHK_rowId).getValue();
		if(check_id == 1){
			all_row = all_row + 1 ;
		}		
		/*
		 * if(status_id == 52125){ //Approved impl_row = impl_row + rowId +','; }
		 */
   });
	//finalval = eval(checked_row.length) + eval(impl_row.length);
	if(all_row == totrowCount){
		objControl.checked = true;//select all check box to be checked
	}else{
		objControl.checked = false;//select all check box un checked.
	}
	gridObj.cells(rowId, implCHK_rowId).cell.wasChanged=true;
}
//Function for making the Implement Check box available for Approved Sttaus on
//changing the Status Type.
function fnSetImpleChk(newStatus,oldStatus,statusRowId){
	if(newStatus == '52125' && apprvlAccess == 'Y'){ //Approved
		gridObj.cellById(statusRowId, implCHK_rowId).setDisabled(false);
		gridObj.cellById(statusRowId, priceType_rowId).setDisabled(true);
		gridObj.cellById(statusRowId, priceTypeVal_rowId).setDisabled(true);
		gridObj.cellById(statusRowId, propPrice_rowId).setDisabled(true);	
	}else{
		gridObj.cellById(statusRowId, implCHK_rowId).setDisabled(true);
		gridObj.cellById(statusRowId, priceType_rowId).setDisabled(false);
		gridObj.cellById(statusRowId, priceTypeVal_rowId).setDisabled(false);
		gridObj.cellById(statusRowId, propPrice_rowId).setDisabled(false);	
	}
}

//The following code has been overridden for proper DHTMLX popup Alignment
var split_At = '';
var w1,dhxWins,Yval;

function createDhtmlxWindow(){
	Yval= getMouseXY(event)-200;
	if(arguments.length==0)
		return(	initDhtmlxWindow(750, 480,false,Yval) );
	if(arguments.length==2)
		return(	initDhtmlxWindow(arguments[0], arguments[1] ,false,Yval) );
	if(arguments.length==3)
		return(	initDhtmlxWindow(arguments[0], arguments[1],arguments[2],Yval));
}

function initDhtmlxWindow(width,height,enableView,Yval){
	if(dhxWins!=undefined){
			dhxWins.forEachWindow(function(win) {
				dhxWins.setViewport(0, Yval , 0,0);  
			    win.close();
			 });
		}	
	dhxWins = new dhtmlXWindows();
	dhxWins.enableAutoViewport(enableView);
	dhxWins.setImagePath("extweb/dhtmlx/dhtmlxWindows/imgs");
	
	if(Yval<=0)
		Yval=10;
	if (GLOBAL_ClientSysType == "IPAD")	{
		dhxWins.setViewport(100, Yval , width+50, height+50);
		w1 = dhxWins.createWindow("w1", 50, 40,width,height);
	}else{
		dhxWins.setViewport(60, Yval , width+50, height+50);
		w1 = dhxWins.createWindow("w1", 450, 350,width,height);
	}
	w1.button("park").hide();
	w1.button("minmax1").hide();
	w1.button("close").hide();
	w1.addUserButton("close", "close", "close", "close");
	w1.button("close").attachEvent("onClick", CloseParentDhtmlxWindow);
	w1.keepInViewport(true);
	return w1;
}

function fnSetRowTypes(rowId){
	gridObj.setCellExcellType(rowId,implCHK_rowId,"ron");
	gridObj.cellById(rowId, implCHK_rowId).setValue("");
	gridObj.setCellExcellType(rowId,priceType_rowId,"ron");
	gridObj.cellById(rowId, priceType_rowId).setValue("");
	gridObj.setCellExcellType(rowId,priceTypeVal_rowId,"ron");
	gridObj.cellById(rowId, priceTypeVal_rowId).setValue("");	
	gridObj.setCellExcellType(rowId,propPrice_rowId,"ron");
	gridObj.cellById(rowId, propPrice_rowId).setValue("");	
}

function fnSetRowStyles(rowId){	
	gridObj.setRowTextStyle(rowId, "background-color: #DDE1E2; color:black;text-valign: middle;");	
}

function fnViewAccountDetails(){
	var priceReqId = document.frmPriceApprovalRequest.priceRequestId.value;
	if(priceReqId == '' || priceReqId == '0'){
		Error_Details("Please enter valid <B>Pricing Request ID</B>");
	}
	
	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
	} else{
		windowOpener('/gmPriceApprovalRequestAction.do?method=fetchAccountInfoDetails&priceRequestId='+priceReqId,'',"resizable=yes,scrollbars=yes,top=150,left=200,width=620,height=450");

	}
}

function fnBusinessQuestions(){
	var priceReqId = document.frmPriceApprovalRequest.priceRequestId.value;
	if(priceReqId == '' || priceReqId == '0'){
		Error_Details("Please enter valid <B>Pricing Request ID</B>");
	}
	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
	}else{
		windowOpener('/gmPriceApprovalRequestAction.do?method=fetchBusinessQuestions&priceRequestId='+priceReqId,'',"resizable=yes,scrollbars=yes,top=150,left=200,width=1080,height=600");
	}
	
}
