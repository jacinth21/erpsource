var oldChangedRows;
var mygrid = '';
function fnSystemFilter(){
	var sysId = sysCombo.getSelectedValue();
	if(sysId=="0" || sysId=='')
	{
		mygrid.filterBy(mygrid.getColIndexById("sysid"), "",true);
		grpCombo.clearAll();
		grpCombo.loadXMLString(groupData);
	}
	else
	{		
		grpCombo.clearAll();
		grpCombo.addOption('0','[Choose One]');

		var arrGrpLen = arrGroupInp.length;		
		for (var j=0;j<arrGrpLen ;j++ )
		{		
			if (arrGroupInp[j][3] == sysId)
			{
				grpCombo.addOption(arrGroupInp[j][0],arrGroupInp[j][1]);
			}
		}
		grpCombo.selectOption(0);
		mygrid.filterBy(mygrid.getColIndexById("sysid"), sysId,true);
	}
	fnFilter(true);
}  
function fnSystemFilterWithoutGroupLoad(){	
	var sysId = sysCombo.getSelectedValue();

	if(sysId=="0" || sysId=='')
	{
		mygrid.filterBy(mygrid.getColIndexById("sysid"), "",true);
	}
	else
	{		
		mygrid.filterBy(mygrid.getColIndexById("sysid"), sysId,true);
	}
}
function fnGrpFilter(){
	
	var grpId = grpCombo.getSelectedValue();	

	if(grpId=="0" || grpId=='' || grpId==null)
	{
		mygrid.filterBy(mygrid.getColIndexById("grpid"), "",true);
	}
	else
	{
		mygrid.filterBy(mygrid.getColIndexById("grpid"), function(data)
		{			
            return   data.toString()==grpId;  
		},true);
	}
}
function fnGrpTypeFilter(){
	
	var grTypepId = grpTypCombo.getSelectedValue();
	if(grTypepId=="0" || grTypepId=='')
	{
		mygrid.filterBy(mygrid.getColIndexById("grptypid"), "",true);
	}
	else
	{
		mygrid.filterBy(mygrid.getColIndexById("grptypid"), grTypepId,true);
	}
}
function fnPartFilter(){
	
	var pnum = document.frmGroupPartPricingForm.partNumbers.value;
	if(pnum=="")
	{
		mygrid.filterBy(0, "",true);
	}
	else
	{
		mygrid.filterBy(0, function(data)
		{			
            return   data.toString().indexOf(pnum)!=-1;  
		},true);
	}
}
function fnPricedFilter() {
	
	var pricedType = document.frmGroupPartPricingForm.pricedTypeId.value;
	var w = document.frmGroupPartPricingForm.pricedTypeId.selectedIndex;
	var selected_text = document.frmGroupPartPricingForm.pricedTypeId.options[w].text;
  
	if(pricedType=="0")
	{
		mygrid.filterBy(mygrid.getColIndexById("pricedby"), "",true);
	}
	else
	{
		if(pricedType=="52110"){
			mygrid.filterBy(mygrid.getColIndexById("pricedby"), selected_text,true);
		}
		else{
			mygrid.filterBy(mygrid.getColIndexById("pricedby"), selected_text,true);
		}		
		
	}
}
// MNTTASK-7037 : Commented function. Not Working Properly Active Filter
/*
function fnStatusFilter(){
	var statusList = document.frmGroupPartPricingForm.statusList.value;
	var w = document.frmGroupPartPricingForm.statusList.selectedIndex;
	var selected_text = document.frmGroupPartPricingForm.statusList.options[w].text;
	
	if(statusList=="0")
	{
		mygrid.filterBy(mygrid.getColIndexById("activeflag"), "",true);
	}
	else if(statusList=="1006431"){
	
			mygrid.filterBy(mygrid.getColIndexById("activeflag"),selected_text,true);
		}		
		else if(statusList=="1006430"){
	
			mygrid.filterBy(mygrid.getColIndexById("activeflag"),selected_text,true);//if Active is given its not working so along with Active space is added in the selected text
		}
		 
	}	
*/

function fnFilter(){	
	mygrid.filterBy(0,""); 
	if(sysCombo.getSelectedValue()!=0)
		fnSystemFilterWithoutGroupLoad();
	if(grpCombo.getSelectedValue()!=0)	
		fnGrpFilter();
	if(document.frmGroupPartPricingForm.pricedTypeId.value!=0)
		fnPricedFilter();
	if(grpTypCombo.getSelectedValue()!=0)
		fnGrpTypeFilter();
	// MNTTASK-7037 - For Commented. Not Working Properly Active Filter
	/*if(document.frmGroupPartPricingForm.statusList.value!=0){
		fnStatusFilter();
	}*/
	fnPartFilter();	
	
}
function initMyGrid(divRef,gridData,split_At){
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.init();
	gObj.enableSmartRendering(true);
	if(split_At!='' && split_At != undefined){
		gObj.splitAt(split_At);
	}
	gObj.loadXMLString(gridData);
	return gObj;
}
function initializeGrid(gridData,advpaccess){
		
		mygrid = initGridWithDistributedParsing('mygrid_container',gridData);		
		mygrid.enableBlockSelection(true);
		if(advpaccess=='Y'){
			mygrid.setSerializableColumns("true,true,true,true,true,true,true,true,true,false,false,false,false,false,false,false,false,false");
		}else{
			mygrid.setSerializableColumns("true,true,true,true,false,false,true,true,true,false,false,false,false,false,false,false,false,false");
		}
		mygrid.copyBlockToClipboard();
		mygrid.enableEditEvents(false, true, true);
		mygrid.setImagePath("/images/dhtmlxGrid/imgs/");
		mygrid.attachEvent("onRowPaste",function(id){
             mygrid.forEachCell(id,function(obj){
                   obj.cell.wasChanged = true;
                   });
			});
		mygrid.enableUndoRedo();
		mygrid.enableMultiselect(true);
		mygrid.enableTooltips("false,false,false,false,false,false,true");
		mygrid.enableAutoHeight(true,500,true);
		mygrid.attachEvent("onEditCell",doOnCellEdit);
		mygrid.attachEvent("onRowSelect",doOnRowSelect);
		// MNTTASK-7037 - Added Filter for Active Filter
		mygrid.attachHeader('#rspan,#rspan,#rspan,#rspan,#rspan,#rspan,#rspan,#rspan,#rspan,rspan,#select_filter_strict,#rspan,#rspan,#rspan,#rspan,#rspan,#rspan,#rspan,#rspan');
		
		
}

 function doOnRowSelect(rowId,cellIndex){
	 
		  if (cellIndex==mygrid.getColIndexById("status")){
			 if(mygrid.cellById(rowId,mygrid.getColIndexById("pricedby")).getValue()=='Group'){
				 fnOpenGroupStatusReport(rowId,mygrid.cellById(rowId,mygrid.getColIndexById("listprice")).getValue(),mygrid.cellById(rowId,mygrid.getColIndexById("vplimit")).getValue());
			 }else{
				 fnOpenPartStatusReport(rowId);
			 }
			}
		 
		}  

function fnOpenGroupStatusReport(id,lp,twp)
{   
	   windowOpener("/gmGroupPartStatusReport.do?method=groupPartStatusReport&htype=group&hid="+id+"&listp="+lp+"&tripwirep="+twp,"statusRpt","resizable=yes,scrollbars=yes,top=250,left=300,width=750,height=300");
}
function fnOpenPartStatusReport(id)
{            
		windowOpener("/gmGroupPartStatusReport.do?method=partStatusReport&htype=group&hid="+id,"statusRpt","resizable=yes,scrollbars=yes,top=250,left=300,width=750,height=300");
}


dhtmlXGridObject.prototype._in_header_master_checkbox=function(t,i,c){
	t.innerHTML=c[0]+"<input type='checkbox' />"+c[1];
	var self=this;
	t.firstChild.onclick=function(e){
		//any custom logic here
		self._build_m_order();
		var j=self._m_order?self._m_order[i]:i;
		var val=this.checked?1:0;
		self.forEachRowA(function(id){
			var c=this.cells(id,j);			
			if (c.isCheckbox())
			{ 
				c.setValue(val);
				setRowAsModified(id,true);
			}
		});
		(e||event).cancelBubble=true;
	}
}


function enableMarker(){

	mygrid.enableMarkedCells();
}
function disableMarker(){
	mygrid.enableMarkedCells(false);
}

function doundo(){
	mygrid.doUndo();
}
function doredo(){
	mygrid.doRedo();
}

function docopy(){  
	mygrid.setCSVDelimiter("\t");
	mygrid.copyBlockToClipboard();   
	mygrid._HideSelection();
}
function pasteToGrid(){
	
	if(mygrid._selectionArea!=null){
		var colIndex = mygrid._selectionArea.LeftTopCol; 
		if(colIndex!=undefined && colIndex !=0){
			mygrid.pasteBlockFromClipboard();
			mygrid._HideSelection();
			commentsValFl = true;
		}else{
			alert(message_sales[135]);
		}
	}else{
		alert(message_sales[136]);
	}
}

function addRowFromClipboard() {
	 var cbData = mygrid.fromClipBoard();   		
	var rowCount = cbData.split("\n"); 
	var ignoredRows =message_sales[328];
	var ignoredRowCount = 0;
	var lp,twp;
	for (var i=0; i<rowCount.length-1; i++){
		var rowData = rowCount[i].split("\t");
		var cb_columnName=rowData[0];
		var isUpdated = false;
		var rowUpdated =false;
		var valFind = mygrid.findCell(TRIM(cb_columnName), 0,true);		
		if(valFind.length!=0){
			var row_id= valFind[0][0];
			if(row_id!='')
			{
				isUpdated = false;
				lp=rowData[2];				
				twp= rowData[3];
				if(lp!='' && !isNaN(lp)){
					mygrid.cellById(row_id, mygrid.getColIndexById("listprice")).setValue(lp);
					isUpdated = true;
				}
				
				if(lp!='' && !isNaN(twp)){
					mygrid.cellById(row_id, mygrid.getColIndexById("vplimit")).setValue(twp);
					isUpdated = true;
				}
				
				if(isUpdated){					
					commentsValFl = true;
					//mygrid.setRowAttribute(row_id,"row_updated","Y");
				}									
				rowUpdated=true;
				setRowAsModified(row_id,rowUpdated)
			}
		}	
		if(!rowUpdated){
			ignoredRows = ignoredRows+cb_columnName+'<br>';
			ignoredRowCount++;
		}
	}

	if(ignoredRowCount>0){
		Error_Clear();
		Error_Details(ignoredRows);				
		Error_Show();
		Error_Clear();
	}
  
}

function removeSelectedRow() {

	var gridrows=mygrid.getSelectedRowId();
	if(gridrows!=undefined){
		var gridrowsarr = gridrows.toString().split(",");
		for (var rowid = 0; rowid < gridrowsarr.length; rowid++) {
			gridrowid = gridrowsarr[rowid];
			mygrid.deleteRow(gridrowid);
		}
	}else{
		Error_Clear();
		Error_Details(message_sales[137]);				
		Error_Show();
		Error_Clear();
	}
}
function applyToMarkedCell() {
	var markedArray = mygrid.getMarked();
	
	var priceValue = TRIM(document.forms[0].applyToMarked.value);
	if(!isNaN(priceValue))	
	{
		if(priceValue.length>0){
			if(markedArray.length==0){
			alert(message_sales[138]);
			}
			for (var i = 0; i < markedArray.length; i++) {
				var cellMark = markedArray[i].toString().split(",");
				var cellObj = mygrid.cellById(cellMark[0], cellMark[1]);
				
				if(cellObj.cell._cellType != 'ro'){
					cellObj.setValue(priceValue);
					commentsValFl = true;
				}
				 setRowAsModified(cellMark[0],true);
			}	
			mygrid.unmarkAll();
			mygrid.enableMarkedCells(false);
			document.forms[0].applyToMarked.value="";
		}
	}
}
var commentsValFl =false;
function doOnCellEdit(stage, rowId, cellInd, nValue, oValue) {		
	if(mygrid.getColumnId(cellInd)!='select')
	{
		if (stage==1){
			var c=this.editor.obj;
			if(c!=null) c.select();
			return true;
		}						
	}
	//Below condition is added, to validate comments only when the price is changed.
	if (stage == 2 && nValue != oValue ){
			commentsValFl = true;
	}
	//This validation checking,List price or Trip Wire price should not be zero when click publish check box. 
	if(mygrid.getColumnId(cellInd)=='select'){
		if (stage==1){
			var listprice = mygrid.cellById(rowId,mygrid.getColIndexById("listprice")).getValue();		
			var vplimit = mygrid.cellById(rowId,mygrid.getColIndexById("vplimit")).getValue();
			//Added for MNTTASK-5082
			var pricedType = mygrid.cellById(rowId,mygrid.getColIndexById("priceType")).getValue();
			if((listprice=='0' || vplimit=='0' || listprice=='' || vplimit=='') && pricedType == '52110'){
				Error_Details(message_sales[139]);
				if (ErrorCount > 0){
					mygrid.cellById(rowId,mygrid.getColIndexById("select")).setValue(0);
					Error_Show();
					Error_Clear();
					return false;
				}
				
			}
		}
	}
	return true;
}
function fnSubmit(){
	document.getElementById("status").src=null;
	document.getElementById("msg").innerHTML="";
	mygrid.editStop();
	var gridrows =mygrid.getChangedRows(",");
	if(gridrows==''){alert(message_sales[329]);return;};
	
	var gridrowsarr = gridrows.toString().split(",");

	//alert('gridrowsarr:'+gridrowsarr);

	var len = gridrowsarr.length;
	
	//get group data
	var hGroupStr='';
	var hPartStr='';	
	var rowId='';
	var lp,twp;
	var isInValidPr =false;
	var twgtrlp = false;
	var grpNm ='';
	for(var i=0;i<len;i++)
	{
		var parseListPrice='';
		var parseTwPrice='';
		rowId=gridrowsarr[i];
		lp=mygrid.cellById(rowId,mygrid.getColIndexById("listprice")).getValue();
		twp=mygrid.cellById(rowId,mygrid.getColIndexById("vplimit")).getValue();
	
		parseListPrice = (lp!='')?parseFloat(lp):0;
		parseTwPrice = (twp!='')?parseFloat(twp):0;
		
		if(parseListPrice < parseTwPrice){
			if(twgtrlp){
				grpNm = grpNm + ' , ' + mygrid.cellById(rowId,0).getValue();	
			}else{
				twgtrlp=true;
				grpNm = mygrid.cellById(rowId,0).getValue();	
			}
			mygrid.setRowTextStyle(rowId,'background-color:#FF9595');

		}
		if(isNaN(lp) || isNaN(twp) )
		{
			isInValidPr=true;
			mygrid.setRowTextStyle(rowId,'background-color:#FF9595');
		}
		if(mygrid.cellById(rowId,mygrid.getColIndexById("flag")).getValue()=='Group')
		{
			hGroupStr=hGroupStr+rowId+'^'+lp+'^'+twp+'^'+mygrid.cellById(rowId,mygrid.getColIndexById("select")).getValue()+'^'+mygrid.cellById(rowId,mygrid.getColIndexById("sel")).getValue()+'|'; 
		}
		else
		{
			hPartStr=hPartStr+rowId+'^'+lp+'^'+twp+'|';
		}
	}	

	if(isInValidPr)
	{
		Error_Details(message_sales[140]);
		//alert('Rows highlighted in red have Invalid Price(s). Please enter valid  value(s). ');
	}
	if(twgtrlp){
		Error_Details(message_sales[141]+grpNm);
	}
	//fnValidateTxtFld('comments',' Comments ');
	var Comments = eval("document.all.comments");
	if(Comments && commentsValFl){
		var Commentsval = TRIM(Comments.value);
		if(Commentsval == ''){
			Error_Details(message_sales[142]);
		}	
	}
	if (ErrorCount > 0)
		{
			Error_Show();
			Error_Clear();
			return false;
		}else
		{
			if(confirm(message_sales[143])){
				fnStartProgress('Y');
				document.frmGroupPartPricingForm.hinputStringGroup.value = hGroupStr;
				document.frmGroupPartPricingForm.hinputStringPart.value = hPartStr;
				document.frmGroupPartPricingForm.action = "/gmGroupPartPricingDAction.do?method=saveGroupPartPrice&strOpt=save";
				document.frmGroupPartPricingForm.submit();	
		   }
		}
    //Below code in commented for PMT-2479.
	//document.getElementById("progress").style.display="inline";
	//document.getElementById("status").src="images/Loading.gif";
	//var loader = dhtmlxAjax.getSync('/gmGroupPartPricingDAction.do?method=saveGroupPartPrice&hinputStringGroup='+hGroupStr+'&hinputStringPart='+hPartStr+'&ramdomId='+Math.random());
	//alert('done:'+loader);
	//fnUpadtedData(loader,gridrowsarr);
	//alert('1---->'+loader.xmlDoc.responseText);
}
function fnValidateComments(){
	var Comments = eval("document.all.comments");
	if(Comments && commentsValFl){
		var Commentsval = TRIM(Comments.value);
		if(Commentsval == ''){
			Error_Details(message_sales[144]);
			Error_Show();
			Error_Clear();
			return false;
		}	
	}
	return true;
}
function fnUpadtedData(loader,gridrowsarr) {
	//alert('2----->'+loader.xmlDoc.responseText);
	var response =loader.xmlDoc.responseText;
	
	//alert('response:'+response);

	if (response != null)
	 {	
		var splitStrResponse = response.split('#');
		var resError = splitStrResponse[0];
		var resSuccess = splitStrResponse[1];
		
		var splitStrErr = resError.split('$');
		var errStr = splitStrErr[0];
		var errGIds = splitStrErr[1];
		
		var splitStrSuccess = resSuccess.split('$');
		var successStr = splitStrSuccess[0];
		var successGIdDtls = splitStrSuccess[1];		
       
       	if(errGIds!=null){
			gIds = errGIds.split('^');
		}


		   if(oldChangedRows !=undefined )
			{
				for(var i=0;i<oldChangedRows.length;i++)
				{
				 mygrid.setRowTextStyle(oldChangedRows[i],'background-color:');
				}
			}
	
	 		oldChangedRows = gridrowsarr;
	 		var len = gridrowsarr.length;
			if(successStr=='success' && successGIdDtls!='')
			{
				var allSuccessGroups = successGIdDtls.split('|');				
				var allSuccGroupLen = allSuccessGroups.length;
					for(var i=0;i<len;i++)
					{
						
						for(var j=0;j<(allSuccGroupLen-1);j++){

							var groupDetails = allSuccessGroups[j].split('^');
							
							var groupId = groupDetails[0];
							var adPrice = groupDetails[1];
							var vpPrice = groupDetails[2];

							if(groupId==gridrowsarr[i]){								
								mygrid.setRowTextStyle(gridrowsarr[i],'background-color:#DFFFDF');
								setRowAsModified(gridrowsarr[i],false);
								mygrid.cellById(gridrowsarr[i], mygrid.getColIndexById("adprice")).setValue(adPrice);
								mygrid.cellById(gridrowsarr[i], mygrid.getColIndexById("vpprice")).setValue(vpPrice);
							}
						}
					}
				
					 			 	
			}
			if(errStr=='error' && errGIds!='')
			{
				
				for(var i=0;i<len;i++)
				{
					for(var j=0;j<(gIds.length-1);j++){
						if(gIds[j].substring(1)==gridrowsarr[i]){
							mygrid.setRowTextStyle(gridrowsarr[i],'background-color:#FF9595');
							setRowAsModified(gridrowsarr[i],false);
							if(gIds[j].charAt(0)!='P'){
								mygrid.cellById(gridrowsarr[i], mygrid.getColIndexById("select")).setValue(0);
							}
						}
					}
				}
				
			}			
			if(errStr=='error' && errGIds!='')
			{
				document.getElementById("status").src="images/Past_Due.gif";
				document.getElementById("msg").style.color="red";
				document.getElementById("msg").style.display="inline";			
				document.getElementById("msg").innerHTML=message_sales[330];
			}else{
				document.getElementById("status").src="images/success_y.gif";
				document.getElementById("msg").style.color="blue";
				document.getElementById("msg").style.display="inline";		
				document.getElementById("msg").innerHTML=message_sales[331];
			}
	 }
	/*if (response != null)
	 {	
		var splitStr = response.split('|');
		var res = splitStr[0];
		var grpIds = splitStr[1];
		var gIds;
		if(grpIds!=null){
			gIds = grpIds.split('^');
		}
		if(oldChangedRows !=undefined )
			{
				for(var i=0;i<oldChangedRows.length;i++)
				{
					mygrid.setRowTextStyle(oldChangedRows[i],'background-color:');
				}
			}
	
	 		oldChangedRows = gridrowsarr;
	 		var len = gridrowsarr.length;
			if(res=='success' && grpIds=='')
			{
				
				
				for(var i=0;i<len;i++)
				{
					for(var j=0;j<(gIds.length-1);j++){
						if(gIds[j]==gridrowsarr[i]){
							mygrid.setRowTextStyle(gridrowsarr[i],'background-color:#DFFFDF');
							setRowAsModified(gridrowsarr[i],false);
						}
					}
				}
				document.getElementById("status").src="images/success_y.gif";
				document.getElementById("msg").style.color="blue";
				document.getElementById("msg").style.display="inline";		
				document.getElementById("msg").innerHTML="Successfully Updated.";	 			 	
			}
			else
			{
				
				for(var i=0;i<len;i++)
				{
					for(var j=0;j<(gIds.length-1);j++){
						if(gIds[j].substring(1)==gridrowsarr[i]){
							mygrid.setRowTextStyle(gridrowsarr[i],'background-color:#FF9595');
							setRowAsModified(gridrowsarr[i],false);
							if(gIds[j].charAt(0)!='P'){
								mygrid.cellById(gridrowsarr[i], 8).setValue(0);
							}
						}
					}
				}
				document.getElementById("status").src="images/Past_Due.gif";
				document.getElementById("msg").style.color="red";
				document.getElementById("msg").style.display="inline";			
				document.getElementById("msg").innerHTML="Failed to Update, Check highlight groups to see reasons.";
			}
	 }*/
}

function setRowAsModified(rowid,modified) {
	mygrid.forEachCell(rowid,function(obj){
       obj.cell.wasChanged = modified;
		});
}
function checkForDuplicateRow(row_id){
	if(mygrid.doesRowExist(row_id)){
		return true;
	}
	return false;
}


function fnGroupDetail(groupID) { 
	  w3 =  createDhtmlxWindow(915,400,true);
	  w3.setText(message_sales[145]); 
	  var ajaxUrl = fnAjaxURLAppendCmpInfo("/gmGroupPartMap.do?strHPartPriceFl=Y&strPartListFlag=Y&hviewEdit=view&groupId="+groupID+"&groupNM=&ramdomId="+Math.random());
	  w3.attachURL(ajaxUrl,"Part Details");
	  
	  return false;	
	  
	//  windowOpener("/gmGroupPartMap.do?strHPartPriceFl=Y&strPricingByGroupFl=Y&strPartListFlag=Y&hviewEdit=view&groupId="+groupID+"&groupNM=","","resizable=yes,scrollbars=yes,top=250,left=300,width=950,height=400");
}


function fnShowPriceHistory(grpPartId)
{
	  w4 =  createDhtmlxWindow(1000,350,true);
	  w4.setText(message_sales[146]);
	  var ajaxUrl = fnAjaxURLAppendCmpInfo("/gmAuditPriceLog.do?method=fetchPriceHistory&auditId=1030&txnId="+grpPartId+"&ramdomId="+Math.random());
	  w4.attachURL(ajaxUrl,message_sales[334]);	  
	  return false;	
}



function fnUserNotification(){
	if (strErrorGrpIds != null && strOpt == 'Reload')
	 {		
		var gridrows = mygrid.getAllRowIds(",");
		var gridrowsarr = gridrows.toString().split(",");
 		var len = gridrowsarr.length;
 		
		var splitStrErr = strErrorGrpIds.split('$');
		var errStr = splitStrErr[0];
		var errGIds = splitStrErr[1];
		var errorFl= false;
		if(errGIds!='' && errGIds!= undefined){
			gIds = errGIds.split('^');
			errorFl = true;
		}

		if(errStr=='error' && errorFl )
		{
			for(var i=0;i<len;i++)
			{
				for(var j=0;j<(gIds.length-1);j++){
					if(gIds[j].substring(1)==gridrowsarr[i]){
						mygrid.setRowTextStyle(gridrowsarr[i],'background-color:#FF9595');
						setRowAsModified(gridrowsarr[i],false);
						if(gIds[j].charAt(0)!='P'){
							mygrid.cellById(gridrowsarr[i], mygrid.getColIndexById("select")).setValue(0);
						}
					}
				}
			}
			
		}		
		if(errStr=='error' &&  errorFl )
		{
			document.getElementById("progress").style.display="inline";
			document.getElementById("status").src="images/Past_Due.gif";
			document.getElementById("msg").style.color="red";
			document.getElementById("msg").style.display="inline";			
			document.getElementById("msg").innerHTML=message_sales[332];
		}else if(errStr=='error'){
			document.getElementById("progress").style.display="inline";
			document.getElementById("status").src="images/success_y.gif";
			document.getElementById("msg").style.color="blue";
			document.getElementById("msg").style.display="inline";		
			document.getElementById("msg").innerHTML=message_sales[333];
		}
			 
	}
}
function onBlur(){
	if( this.getSelectedValue()== null)
	{
		this.setComboValue(0);
	}
}

function onOpenComboFunc() {
	if(this.getSelectedValue()==0)
	{
		this.setComboText("");
	}
}

//Enter key functionality.
function fnEnter(){	
	if(event.keyCode == 13)
	{	 
		var rtVal = fnReload();

		//below code is when fnReload() return false, we need to stop the jsp, from submitting.
		if(rtVal == false){
			if(!e) var e = window.event;

	        e.cancelBubble = true;
	        e.returnValue = false;

	        if (e.stopPropagation) {
	                e.stopPropagation();
	                e.preventDefault();
	        }
		}
	}		
}


function fnReload(){	
	var pnum = TRIM(document.frmGroupPartPricingForm.partNumbers.value);
	var search = document.frmGroupPartPricingForm.Cbo_Search.value;
	var sysId = systemListId.getSelectedValue();
	var pricedById  = document.frmGroupPartPricingForm.pricedTypeId.value;
	if(pnum == "" && search != '0')	{
		Error_Details(message_sales[147]);
	}	
	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}

	document.frmGroupPartPricingForm.hsearch.value = search;
	fnStartProgress('Y');
	document.frmGroupPartPricingForm.strOpt.value = "Reload";		
	document.frmGroupPartPricingForm.action = "/gmGroupPartPricingDAction.do?method=loadSystemPrice";
	document.frmGroupPartPricingForm.submit();	
}

