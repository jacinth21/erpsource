var gridObj;
var gObj;
function fnEnter() {
	if (event.keyCode == 13) {
		fnLoad();
	}
}
//To Load Rebate Management Details
function fnLoad(){
	
	var fromDateObj = document.frmRebateReport.rebateFromDate;
	var toDateObj = document.frmRebateReport.rebateToDate;
	var fromDt = fromDateObj.value;
	var toDate =toDateObj.value;
	var accType=document.frmRebateReport.groupTypeId.value;
	var partyId = document.frmRebateReport.partyId.value;
	var rebateType=document.frmRebateReport.rebateTypeId.value;
	var rebatePartType=document.frmRebateReport.rebatePartTypeId.value;
	var rebateCategory = document.frmRebateReport.rebateCategoryId.value;
    var rebateFromRate=document.frmRebateReport.rebateFromRate.value;
    var rebateToRate=document.frmRebateReport.rebateToRate.value;
	var dateType=document.frmRebateReport.dateType.value;
	
	var rebateFrmRt=parseInt(rebateFromRate);
	var rebatetoRt=parseInt(rebateToRate);
	
	
	if(dateType != "0"){
		if((fromDt == "")){
			Error_Details("Please Select <b>From Date</b> ");
		}
		if(toDate ==""){
			Error_Details("Please Select <b>To Date</b>");
		}
		
	}
	if((rebateFrmRt) > (rebatetoRt)){
	
		Error_Details("<b>Rebate To Rate</b> must be greater than <b>Rebate From Rate</b>");
	}
	
	if (isNaN(rebateFromRate)) {
		Error_Details("Please Enter Numeric Values for <b>Rebate From Rate</b>");
		} 
	if (isNaN(rebateToRate)) {
		Error_Details("Please Enter Numeric Values for <b>Rebate To Rate</b>");
		} 
	var exp = /^[+-]?[0-9]{1,9}(?:\.[0-9]{1,2})?$/;
	var objval = exp.test(rebateFromRate);
	if((rebateFromRate !="")){	
		if(!objval){	
		Error_Details("Please enter <b>Rebate From Rate</b> with maximum of 2 decimal values");
		} 
		if(rebateFromRate <= 0 || rebateFromRate > 100){
			Error_Details("'<b>Rebate From Rate</b>' must be greater than 0 or less than 100");
		}
	}
	objval = exp.test(rebateToRate);
	if((rebateToRate !="")){	
		if(!objval){	
		Error_Details("Please enter <b>Rebate To Rate</b> with maximum of 2 decimal values");
		} 
		if(rebateToRate <= 0 || rebateToRate > 100){
			Error_Details("<b>Rebate To Rate</b> must be greater than 0 or less than 100");
		}
	}
	
	if (dateDiff(fromDt, toDate, format) < 0)
	{
		Error_Details("<b>From Date<b/> should be greater than or equal to <b>To Date</b>");
	}
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
 // Using Ajax call for load the report
	//var loader;
	var response;
	fnStartProgress();
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmRebateReport.do?method=reloadRebateManagmentReport&partyId='+partyId+'&groupTypeId='+accType+'&rebateTypeId='+rebateType+'&rebatePartTypeId='+rebatePartType+'&rebateCategoryId='+rebateCategory+'&rebateFromRate='+rebateFromRate+'&rebateToRate='+rebateToRate+'&rebateFromDate='+fromDt+'&rebateToDate='+toDate+'&dateType='+dateType+'&ramdomId='+new Date().getTime());
	dhtmlxAjax.get(ajaxUrl,fnLoadRebateAjax);

}

function fnLoadRebateAjax (loader){
	var rebateId = '';
	var account_type = '';
	var party_name = '';
	var rebate_type = '';
	var rebate_parts = '';
	var rebate_category = '';
	var rebate_rate = '';
	var eff_date = '';
	var contract_frm_date = '';
	var contract_to_date = '';
	var tier_1_rate = '';
	var tier_1_frm_amt = '';
	var tier_1_to_amt = '';
	var tier_2_rate = '';
	var tier_2_frm_amt = '';
	var tier_2_to_amt = '';
	var tier_3_rate = '';
	var tier_3_frm_amt = '';
	var tier_3_to_amt = '';
	var activeFl = '';
	var response = loader.xmlDoc.responseText;
	
  if(response != "") {
//to form the grid string 
	var rows = [], i=0;
	var JSON_Array_obj = "";
	 JSON_Array_obj = JSON.parse(response);
	$.each(JSON_Array_obj, function(index,jsonObject){
	    rebateId = jsonObject.rebate_id;
	    account_type = jsonObject.grouptypename;
	    party_name = jsonObject.partyname;
		rebate_type = jsonObject.rebatetypename;
		rebate_parts = jsonObject.rebateparttypename;
		rebate_category = jsonObject.rebatecategoryname;
		rebate_rate = jsonObject.rebaterate;
		eff_date = jsonObject.rebateeffectivedate;
		contract_frm_date = jsonObject.rebatefromdate;
		contract_to_date = jsonObject.rebatetodate;
		tier_1_rate = jsonObject.tier1percent;
		tier_1_frm_amt = jsonObject.tier1fromamt;
		tier_1_to_amt = jsonObject.tier1toamt;
		tier_2_rate = jsonObject.tier2percent;
		tier_2_frm_amt = jsonObject.tier2fromamt;
		tier_2_to_amt = jsonObject.tier2toamt;
		tier_3_rate = jsonObject.tier3percent;
		tier_3_frm_amt = jsonObject.tier3fromamt;
		tier_3_to_amt = jsonObject.tier3toamt;
		activeFl = jsonObject.activefl;
		// default array
        rows[i] = {};
       rows[i].id = rebateId;
        rows[i].data = [];
        //
        rows[i].data[0] = "<a href='#' onClick=fnEdit('"+ rebateId +"')><img alt='Edit Rebate' src='/images/edit_icon.gif' border='0'></a>";
        rows[i].data[1] = account_type;
        rows[i].data[2] = party_name;
        rows[i].data[3] = rebate_type;
        
        rows[i].data[4] = rebate_parts;
        rows[i].data[5] = rebate_category;
        rows[i].data[6] = rebate_rate;
        rows[i].data[7] = eff_date;
        rows[i].data[8] = contract_frm_date;
        rows[i].data[9] = contract_to_date;
        rows[i].data[10] = activeFl; 
        rows[i].data[12] = tier_1_rate;
        rows[i].data[13] = tier_1_frm_amt;
        rows[i].data[14] = tier_1_to_amt;
        rows[i].data[15] = tier_2_rate;
        rows[i].data[16] = tier_2_frm_amt;
        rows[i].data[17] = tier_2_to_amt;
        rows[i].data[18] = tier_3_rate;
        rows[i].data[20] = tier_3_frm_amt;
        rows[i].data[21] = tier_3_to_amt;
     // set part list column value
        if(rebate_parts == "All Parts"){
        	rows[i].data[11] = "N/A"; 
        }else{
        	rows[i].data[11] = ""; 
        }
        i++;
     
        
	});
	 
	
	var setInitWidths = "25,80,*,80,80,70,50,70,70,70,50,50,50,50,50,50,50,50,50";
    var setColAlign = "left,left,left,left,left,left,right,center,center,center,center,center,right,right,right,right,right,right,right,right";
    var setColTypes = "ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro";
    var setColSorting = ",str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str";
    var setHeader = ["","Account Type", "Name", "Rebate Type", "Rebate Parts", "Rebate Category",
                     "Rebate Rate", "Effective Date", "Contract From Date", "Contract To Date", "Active" ,"Part List", "Tier1 Rate", "Tier1 From Amount", "Tier1 To Amount",
                     "Tier2 Rate", "Tier2 From Amount", "Tier2 To Amount", "Tier3 Rate", "Tier3 From Amount", "Tier3 To Amount"];
    var setFilter = ["", "#select_filter", "#text_filter", "#select_filter", "#select_filter", "#select_filter", "#text_filter", "#select_filter", "#select_filter", "#select_filter", "#text_filter", "#text_filter", "#text_filter", "#text_filter", "#text_filter", "#text_filter", "#text_filter", "#text_filter", "#text_filter", "#text_filter", "#text_filter"];
    var setColIds = "";
    var enableTooltips = "true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true";
    var footerArry = [];
    var gridHeight = "";
    var footerStyles = [];
    var footerExportFL = true;
    var dataHost = {};
    //dataHost.head = setHeader;
    dataHost.rows = rows;
   // console.log (' data host len '+ dataHost.rows.length);
	document.getElementById("dataGridDiv").style.display = 'block';
	document.getElementById("DivNothingMessage").style.display = 'none';
	document.getElementById("DivExportExcel").style.display = 'block';

    gridObj = loadDHTMLXGrid('dataGridDiv', gridHeight, dataHost, setHeader, setInitWidths, setColAlign, setColTypes, setColSorting, setColIds, enableTooltips, setFilter, footerArry, footerStyles, footerExportFL);
    gridObj.setColumnHidden(11,true);
    gridObj.setColumnHidden(12,true);
    gridObj.setColumnHidden(13,true);
    gridObj.setColumnHidden(14,true);
    gridObj.setColumnHidden(15,true);
    gridObj.setColumnHidden(16,true);
    gridObj.setColumnHidden(17,true);
    gridObj.setColumnHidden(18,true);
    gridObj.setColumnHidden(19,true);
    gridObj.setColumnHidden(20,true);
 
    gridObj.enableHeaderMenu();
    
  }else{
	document.getElementById("dataGridDiv").style.display = 'none';
	document.getElementById("DivExportExcel").style.display = 'none';
	document.getElementById("DivNothingMessage").style.display = 'block';
}	
  fnStopProgress();	
}

function fnOnPageLoad() {
	document.getElementById("dataGridDiv").style.display = 'none';
	document.getElementById("DivExportExcel").style.display = 'none';
	document.getElementById("DivNothingMessage").style.display = 'block';
	
	if(document.frmRebateReport.groupTypeId.value == "0"){
		document.frmRebateReport.groupTypeId.value = '107461';
	}

}

//This Function Used to download Excel Sheet
function fnDownloadXLS() {
	gridObj.setColumnHidden(0,true);
	gridObj.toExcel('/phpapp/excel/generate.php');
	gridObj.setColumnHidden(0,false);
	
}
// this function disabled the Name field asper Account type
function fnAccType(grpType){
	// reset the field and submit the form
	document.frmRebateReport.partyId.value = "";
	document.frmRebateReport.searchpartyId.value = "";
	
}

function fnEdit(rebateId){
	document.frmRebateReport.strOpt.value="Load";
    document.frmRebateReport.action="/gmRebateSetup.do?method=loadRebateManagmentDtls&rebateId="+rebateId;
	fnStartProgress();
	document.frmRebateReport.submit();
}

