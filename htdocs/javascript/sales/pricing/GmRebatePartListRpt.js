var gridObj;
function fnOnPageLoad() {
	document.getElementById("dataGridDiv").style.display = 'none';
	document.getElementById("DivNothingMessage").style.display = 'block';
	document.getElementById("DivExportExcel").style.display = 'none';
	document.getElementById("pagingArea").style.display = 'block';

	if(document.frmRebateReport.groupTypeId.value == "0"){
		document.frmRebateReport.groupTypeId.value = '107461';
	}
}

//This Function Used to download Excel Sheet
function fnDownloadXLS() {
	gridObj.toExcel('/phpapp/excel/generate.php');
}
function fnEnter() {
	if (event.keyCode == 13) {
		fnLoad();
	}
}
//to Load Part List Details
function fnLoad(){
	var groupTypeId = document.frmRebateReport.groupTypeId.value;
	var partyId = document.frmRebateReport.partyId.value;
	var partNum = document.frmRebateReport.batchPartNum.value;
	var fromDateObj = document.frmRebateReport.rebateFromDate;
	var toDateObj = document.frmRebateReport.rebateToDate;
	var fromDt = fromDateObj.value;
	var toDate =toDateObj.value;
	var dateType=document.frmRebateReport.dateType.value;
	var pnumSuffix =document.frmRebateReport.pnumSuffix.value;
	
	if((groupTypeId=='0')&&(fromDt=="" &&toDate== "" )){
		Error_Details("Please choose any one of the following: Account Type,From Date , To date");
	}
	if(dateType != "0"){
		if((fromDt == "")){
			Error_Details("Please Select <b>From Date</b> ");
		}
		if(toDate ==""){
			Error_Details("Please Select <b>To Date</b>");
		}
		
	}
	if (dateDiff(fromDt, toDate, format) < 0)
	{
		Error_Details("<b>From Date<b/> should be greater than or equal to <b>To Date</b>");
	}
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	
	fnStartProgress();
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmRebateReport.do?method=reloadRebatePartReportsDtls&groupTypeId='+groupTypeId +'&partyId='+partyId+'&rebateFromDate='+fromDt+'&rebateToDate='+toDate+'&batchPartNum='+encodeURIComponent(partNum)+'&dateType='+dateType+'&pnumSuffix='+pnumSuffix+'&ramdomId='+new Date().getTime());
	dhtmlxAjax.get(ajaxUrl,fnLoadRebateAjax)
	
}

function fnLoadRebateAjax (loader){
	var rebateId = '';
	var account_type = '';
	var party_name = '';
	var rebate_type = '';
	var rebate_parts = '';
    var partNum ='';
	var eff_date = '';
	var contract_to_date = '';
	var updated_by = '';
	var updated_date = '';

	var response = loader.xmlDoc.responseText;
	fnStopProgress();
	if(response != ""){
		
	var rows = [], i=0;
	var JSON_Array_obj = JSON.parse(response);
		
	$.each(JSON_Array_obj, function(index,jsonObject){
	    rebateId = jsonObject.rebate_id;
	    account_type = jsonObject.grouptypename;
	    party_name = jsonObject.partyname;
		rebate_type = jsonObject.rebatetypename;
		partNum = jsonObject.pnum;
		updated_by = jsonObject.lastupdatedby;
		updated_date=jsonObject.lastupdateddate;
		rebate_category = jsonObject.rebatecategoryname;
		eff_date = jsonObject.rebateeffectivedate;
		contract_to_date = jsonObject.rebatetodate;
		
		// default array
        rows[i] = {};
        rows[i].id = i;
        rows[i].data = [];
    
        rows[i].data[0] = account_type;
        rows[i].data[1] = party_name;
        rows[i].data[2] = rebate_type;
        rows[i].data[3] = rebate_category;
        rows[i].data[4] = partNum;
        rows[i].data[5] = eff_date;
        //rows[i].data[6] = contract_to_date;
        rows[i].data[6] = updated_by;
        rows[i].data[7] = updated_date;
      i++;  
	});

	var setInitWidths = "100,*,100,100,100,100,110,70,80";
    var setColAlign = "left,left,left,left,left,center,center,left,center";
    var setColTypes = "ro,ro,ro,ro,ro,ro,ro,ro,ro";
    var setColSorting = ",str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str";
    var setHeader = ["Account Type", "Name", "Rebate Type", "Rebate Category", "Part Number",
                      "Effective Date","Updated By", "Updated date"];
    var setFilter = ["#select_filter", "#text_filter", "#select_filter", "#select_filter", "#text_filter", "#select_filter", "#select_filter","#select_filter"];
    var setColIds = "";
    var enableTooltips = "true,true,true,true,true,true,true,true,true,true,true";
    var footerArry = [];
    var gridHeight = "";
    var footerStyles = [];
    var footerExportFL = true;
    var dataHost = {};
    var pagination ="";
    pagination = "Y";
    //dataHost.head = setHeader;
    dataHost.rows = rows;
   // console.log ('data host len '+ dataHost.rows.length);
	document.getElementById("dataGridDiv").style.display = 'block';
	document.getElementById("DivNothingMessage").style.display = 'none';
	gridObj = loadDHTMLXGrid('dataGridDiv', gridHeight, dataHost, setHeader, setInitWidths, setColAlign, setColTypes, setColSorting, setColIds, enableTooltips, setFilter, footerArry, footerStyles, footerExportFL,pagination);
	document.getElementById("DivExportExcel").style.display = 'block';
	document.getElementById("pagingArea").style.display = 'block';
	}else{
		document.getElementById("dataGridDiv").style.display = 'none';
		document.getElementById("DivExportExcel").style.display = 'none';
		document.getElementById("DivNothingMessage").style.display = 'block';
		document.getElementById("pagingArea").style.display = 'none';
	}
		
}
//Disable party name field when choose one selected
function fnOnChange(groupTypeId){
	// reset the field and submit the form
	document.frmRebateReport.partyId.value = "";
	document.frmRebateReport.searchpartyId.value = "";
	
}