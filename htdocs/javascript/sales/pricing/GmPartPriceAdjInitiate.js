var mygrid;
var part_colId, adjCode_colId, adjVal_colId, partDesc_colId, unitPrice_colId, netPrice_colId, dtlId_colId, err_colId, err_flId;
var redColFl = 0;
var yellowColFl = 0;

function fnOnPageLoad(){
	
	fnChangeDisplay(); // To change the Account/Group Account dropdown based on the type
	var txnId = (document.all.txnId)?document.all.txnId.value:'';
	var status = (document.all.statusid)?document.all.statusid.value:'';
	var drpOptions = document.forms['frmPartPriceAdj']['actions'].options;
	
	if(objGridData){
		mygrid = initGrid('dataGridDiv',objGridData);
		var gridrows   = mygrid.getAllRowIds();
		var gridrowsarr = gridrows.split(",");	
		
		if(gridrowsarr == ''){
			for(var i=1 ;i<19 ;i++){// to add rows on load
				mygrid.addRow(mygrid.getUID(),'');
		    }
			drpOptions[2].disabled = true; //Disable the Submit button if there are no saved records
		}
		mygrid.attachHeader("#text_filter,#text_filter,#select_filter,,,");
		mygrid.enableTooltips("true,true,true,true,true,true");
		mygrid.attachEvent("onEditCell",doOnCellEdit);
		mygrid.attachEvent("onKeyPress", onKeyPressed);
		mygrid.enableBlockSelection(true);
		
		part_colId = mygrid.getColIndexById("partnum");
		adjCode_colId = mygrid.getColIndexById("adjcode");
		adjVal_colId = mygrid.getColIndexById("adjval");
		partDesc_colId = mygrid.getColIndexById("partdesc");
		unitPrice_colId = mygrid.getColIndexById("unitprice");
		netPrice_colId = mygrid.getColIndexById("nunitprice");
		dtlId_colId = mygrid.getColIndexById("dtl_id");
		err_colId = mygrid.getColIndexById("err_id");
		err_flId = mygrid.getColIndexById("err_fl");
		
		mygrid.enableEditTabOnly(true); // Tab functionality for Editable fields only
		
		fnValidateGrid();// To validate the values inside the grid
	}
	
	// To Enable or disable the dropdown values based on status
	fnDisableOptions();
	
	if(TRIM(document.all.accId.value) != '')
		fnLoadAccountName(document.all.accId);
}

// To Initiate the grid
function initGrid(divRef,gridData){
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.init();
	gObj.loadXMLString(gridData);	
	return gObj;	
}

// To Enable or disable the dropdown values based on status
function fnDisableOptions(){
	var status = (document.all.statusid)?document.all.statusid.value:'';
	var drpOptions = document.forms['frmPartPriceAdj']['actions'].options;
	// 105285	Initiated;	105286	Pending Approval;	105287	Implemented
	
	switch(status){
		case "" : 
			drpOptions[2].disabled = true;
			drpOptions[3].disabled = true;
			drpOptions[4].disabled = true;
			drpOptions[5].disabled = true;
			drpOptions[6].disabled = true;
			break;
		case "105285" :
			drpOptions[5].disabled = true;
			drpOptions[6].disabled = true;
			break;
		case "105286" :
			drpOptions[1].disabled = true;
			drpOptions[2].disabled = true;
			break;
		case "105287" :
			drpOptions[1].disabled = true; //Save
			drpOptions[2].disabled = true; //Submit
			drpOptions[3].disabled = true; //Void
			drpOptions[5].disabled = true; //Approved
			drpOptions[6].disabled = true; //Denied
	}

}

//Funciton will be called while editing the columns in the grid
function doOnCellEdit(stage, rowId, cellInd, nValue, oValue){
	var adjCode,adjValue;

	if(cellInd == part_colId && (oValue != '' || nValue != '') && stage == 2){//once the value in part number column
		mygrid.setRowAttribute(rowId,"row_modified","Y");
 	}else if(cellInd == adjCode_colId && (oValue != '' || nValue != '') && stage == 2){//once the value in Adjustment code column
 		mygrid.setRowAttribute(rowId,"row_modified","Y");
 	}else if(cellInd == adjVal_colId && (oValue != '' || nValue != '') && stage == 2){//once the value in Adjustment Value column
 		mygrid.setRowAttribute(rowId,"row_modified","Y");
 	}
 	
 	return true;
}


// Use the below function to copy data inside the screen.
function docopy(){
      mygrid.setCSVDelimiter("\t");
      mygrid.copyBlockToClipboard();
      mygrid._HideSelection();
}

// To paste the data to the grid
function pasteToGrid(){
	if(mygrid._selectionArea!=null){
		var enterCnt = 0;
		var partNum, adjCode, adjValue;
		var topRowCnt, rowID;
		var cbData = mygrid.fromClipBoard();
		if (cbData == null) {
			fnStopProgress();
			Error_Details(message_sales[153]);
			Error_Show();
        	Error_Clear();
        	return;
		}
		var emptyRowCnt = 0;
		var partExist,added_row;
		var rowData = cbData.split("\n");
		rowcolumnData = rowData[0].split("\t");
		
		if(rowcolumnData.length > 1){
			for (var i=0; i< rowData.length-1; i++){
				rowcolumnData = rowData[i].split("\t");
			
				if(i == 0){
					topRowCnt = mygrid.getSelectedBlock().LeftTopRow;// Get the top index of selected row
				}else{
					topRowCnt++;
				}
				rowID=mygrid.getRowId(topRowCnt); // Get the row id to paste the data
				
				if(enterCnt == 0){// check whether there are enough rows to paste the record or not
					var gridrowsarr = '';
					var gridrows=mygrid.getAllRowIds();
					if(gridrows!=undefined) gridrowsarr = gridrows.toString().split(",");
					// Get the empty rows count to paste the record
					for(var j=topRowCnt; j< gridrowsarr.length; j++){
						emptyRowCnt++;
					}

					if(emptyRowCnt < rowData.length-1){
						fnStopProgress();
						Error_Details(message_sales[154]);
						Error_Show();
			        	Error_Clear();
			        	return;
					}
					enterCnt++;
				}
							
				partNum = rowcolumnData[0];
				if(rowcolumnData.length>3){
					adjCode = rowcolumnData[2];
					adjValue = rowcolumnData[3];
				}else{
					adjCode = rowcolumnData[1];
					adjValue = rowcolumnData[2];
				}
				adjCode = adjCode.equalsIgnoreCase("Dollar") ? 105283 : adjCode.equalsIgnoreCase("Percentage")? 105284: adjCode; //105284: Percentage; 105283: Dollar
				
				mygrid.cellById(rowID, part_colId).setValue(partNum);
				mygrid.cellById(rowID, adjCode_colId).setValue(adjCode);
				mygrid.cellById(rowID, adjVal_colId).setValue(adjValue);
				
				mygrid.setRowAttribute(rowID,"row_modified","Y");
			
			    mygrid._HideSelection();
			    //document.all.Btn_Save.disabled = false;
			}
		}else{
			mygrid.pasteBlockFromClipboard();
		    mygrid._HideSelection();
		}
			
	}else{
		Error_Details(message_sales[155]);
		Error_Show();
        Error_Clear();
	}
}

function addRow(){
	mygrid.addRow(mygrid.getUID(),'');
}

var delRowIds = '';
// Use the below function to remove the rows from the grid.
function removeSelectedRow(){
      var gridrows=mygrid.getSelectedRowId();   
      if(gridrows!=undefined)
      {
            var NameCellValue ;
            var gridrowsarr = gridrows.toString().split(",");
            var added_row;
            var dtlId;
            for (var rowid = 0; rowid < gridrowsarr.length; rowid++)
            {
                  gridrowid = gridrowsarr[rowid]; 
                  dtlId = mygrid.cellById(gridrowid, dtlId_colId).getValue();
                  if(dtlId != '')
                	  delRowIds = delRowIds + dtlId +',';
                  
                  mygrid.setRowAttribute(gridrowid,"row_modified","N");
                  mygrid.deleteRow(gridrowid);                                
            }
      }else{
            Error_Clear();
            Error_Details(message_sales[156]);                       
            Error_Show();
            Error_Clear();
      }
}

var removeEmptyFl = false;
var confirmMsg = true;
//dynamically add the rows to grid based on the copied data
function addRowFromClipboard(){

	var cbData = mygrid.fromClipBoard();
	if (cbData == null) {
		Error_Details(message_sales[157]);
		Error_Show();
        Error_Clear();
        return;
	}
	
	fnSetGridRow(cbData);
	

}

//To set value to grid, while copy from clipboard
function fnSetGridRow(cbData){
	fnStartProgress();
	var j = 0;
	var rowData = cbData.split("\n");
	var rowcolumnData = '';
	if(rowData[j]){
		mygrid.addRow(mygrid.getUID(),'');
	}
	var gridrows   = mygrid.getAllRowIds();
	var gridrowsarr = gridrows.split(",");	
	var partNum, adjValue, adjCode;	
	var partExist;
	
	if(rowData.length-1 > 1000){
		fnStopProgress();
		Error_Details(message_sales[158]);
		Error_Show();
        Error_Clear();
        return;
	}
	
	setTimeout(function() {	
	mygrid.startFastOperations();
		for (var i=0; i < gridrowsarr.length; i++){
			row_id = gridrowsarr[i];
			var added_row = mygrid.getRowAttribute(row_id,"row_modified");
			//partExist = mygrid.cellById(row_id, part_colId).getValue();
			partExist = mygrid.cellById(row_id, dtlId_colId).getValue();
			var rowcolumnData = '';
			if( added_row != "Y" && partExist == ''){				
				if(rowData[j]){
					rowcolumnData = rowData[j].split("\t");
					partNum = rowcolumnData[0];
					if(rowcolumnData.length>3){
						adjCode = rowcolumnData[2];
						adjValue = rowcolumnData[3];
					}else if(rowcolumnData.length == 3){
						adjCode = rowcolumnData[1];
						adjValue = rowcolumnData[2];
					}else{
						fnStopProgress();
						Error_Details(message_sales[159]);
						Error_Show();
				        Error_Clear();
					}
					
					adjCode = adjCode.equalsIgnoreCase("Dollar") ? 105283 : adjCode.equalsIgnoreCase("Percentage")? 105284: adjCode; //105284: Percentage; 105283: Dollar
					
					mygrid.cellById(row_id, part_colId).setValue(partNum);
					mygrid.cellById(row_id, adjCode_colId).setValue(adjCode);
					mygrid.cellById(row_id, adjVal_colId).setValue(adjValue);
								
					
					mygrid.setRowAttribute(row_id,"row_modified","Y");
					
					if(j < rowData.length-2){
						mygrid.addRow(mygrid.getUID(),'');
					}
					gridrows   = mygrid.getAllRowIds();
					gridrowsarr = gridrows.split(",");	
				}
				j++;
			}
		
		}
		mygrid.stopFastOperations();
		fnStopProgress();
	},100);
}


//this function used to remove the empty rows
function fnRemoveEmptyRow() {
	mygrid.forEachRow(function(row_id) {
		var part_val = mygrid.cellById(row_id, part_colId).getValue();
		if (part_val == '') {
			mygrid.deleteRow(row_id);
		}
	});
}

// To validate the grid and highlight the cell
function fnValidateGrid(){
	var dtl_id, err_val;
	var errStr, errStrLen;
	var err_id, indexof;
	mygrid.forEachRow(function(row_id) {
		dtl_id = mygrid.cellById(row_id, dtlId_colId).getValue(); 
		err_val = mygrid.cellById(row_id, err_colId).getValue();
		errStr = err_val;
		if(dtl_id != '' && err_val != '0'){
			
			if(err_val == 1 || err_val == '1D'){
				fnValidatePartNum(row_id, err_id);
			}else{
				while (errStr.indexOf('^') >= 0){
					if(errStr != 1){
						errStrLen = errStr.length;
						indexof = errStr.indexOf('^');
						if(indexof == 0){
							err_id = errStr.substring(1,2);
							errStr = errStr.substring(indexof+2,errStrLen);
						}else{
							err_id = errStr.substring(0,indexof);
							errStr = errStr.substring(indexof+1,errStrLen)+'^';
						}
					}else{
						err_id = errStr;
						errStr = null;
					}
					if(err_id == 1 || err_id == '1D'){
						fnValidatePartNum(row_id, err_id);
					}
					else if(err_id == 2){
						fnValidateAdjCode(row_id);
					}
					else if(err_id == 3){
						fnValidateAdjValue(row_id);
					}
				}
			}
		}
		if(dtl_id != ''){
			fnValidateUnitPrice(row_id);
			fnValidateNetUnitPrice(row_id);	
		}
	});
}

function fnValidatePartNum(row_id, err_id){
	var title = (err_id == 1) ? message_sales[269]:message_sales[270];
	redColFl++;
	mygrid.cells(row_id,part_colId).setAttribute("title",title);
	mygrid.cells(row_id,part_colId).setBgColor('#ff4c4c');
}

function fnValidateAdjCode(row_id){
		redColFl++;
		mygrid.cells(row_id,adjCode_colId).setAttribute("title",message_sales[271]);
		mygrid.cells(row_id,adjCode_colId).setBgColor('#ff4c4c');
}

function fnValidateAdjValue(row_id){
	var adjCode = mygrid.cellById(row_id, adjCode_colId).getValue();
	var adjVal = mygrid.cellById(row_id, adjVal_colId).getValue();
	
	if(adjVal == '' || adjVal < 0 || isNaN(adjVal)){
		var title = (adjVal < 0) ? message_sales[272] : message_sales[273];
		redColFl++;
		mygrid.cells(row_id,adjVal_colId).setAttribute("title",title);
		mygrid.cells(row_id,adjVal_colId).setBgColor('#ff4c4c');
	}else if(adjCode == '105283'){// if adjustment code is Dollar, there should not be decimal in adjustment value
		if(adjVal%1 != 0 ){
			redColFl++;
			mygrid.cells(row_id,adjVal_colId).setAttribute("title",message_sales[274]);
			mygrid.cells(row_id,adjVal_colId).setBgColor('#ff4c4c');
		}
	}
}

function fnValidateUnitPrice(row_id){
	var unitPrice = mygrid.cellById(row_id, unitPrice_colId).getValue();
	if(unitPrice == ''){
		redColFl++;
		mygrid.cells(row_id,unitPrice_colId).setAttribute("title",message_sales[275]);
		mygrid.cells(row_id,unitPrice_colId).setBgColor('#ff4c4c');
	}
}

function fnValidateNetUnitPrice(row_id){
	var adjCode = mygrid.cellById(row_id, adjCode_colId).getValue();
	var netUnitPrice = mygrid.cellById(row_id, netPrice_colId).getValue();
	var unitPrice = mygrid.cellById(row_id, unitPrice_colId).getValue();
	if(netUnitPrice <= 0){
		var title = (adjCode != '105284') ?  message_sales[276]:message_sales[277] ;
		redColFl++;
		mygrid.cells(row_id,netPrice_colId).setAttribute("title",title);
		mygrid.cells(row_id,netPrice_colId).setBgColor('#ff4c4c');
	}else{
		if(adjCode == '105283'){// if adjustment code is Dollar, there should not be decimal in adjustment value 
			if(netUnitPrice%1 != 0 ){
				redColFl++;
				mygrid.cells(row_id,netPrice_colId).setAttribute("title",message_sales[278]);
				mygrid.cells(row_id,netPrice_colId).setBgColor('#ff4c4c');
			}
		}
		perDiff = ((unitPrice-netUnitPrice)/unitPrice) * 100;// get the percentage difference
		
		if((netUnitPrice) && perDiff > 10){
			yellowColFl++;
			mygrid.cells(row_id,netPrice_colId).setAttribute("title",message_sales[279]);
			mygrid.cells(row_id,netPrice_colId).setBgColor('#e5e500');
		}
	}
}


// If the partnumber is already available in the grid
function fnValidateDupPartNum(row_id,partNum){
	var tempPartNum;
	var dupPartNum = false;
	mygrid.forEachRow(function(rowId){
		// get the selected value from the grid
		if(row_id != rowId){
			tempPartNum = mygrid.cellById(rowId, part_colId).getValue();
			if(partNum == tempPartNum){
				dupPartNum = true;
				return dupPartNum;
			}	
		}
	});
	return dupPartNum;
}

// While clicking on Submit button
function fnSubmitAction(obj){	
	
	var actions = document.frmPartPriceAdj.actions.value;
	//105302: Approved;  105303: Denied; 105306: Save; 105307:Submit; 105308: Void; 105309: Download Excel  
	switch(actions){
		case "105302" : fnApprove(); break;
		case "105303" :	fnDeny(); break;
		case "105306" : fnSave(); break;
		case "105307" : fnSubmit(); break;
		case "105308" : fnVoid(); break;
		case "105309" : fnExcelExport(); break;
		default : Error_Details(message_sales[186]);
	}
	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}
}

function fnCheckAnyRowsAdded()
 {
var count = mygrid.getRowsNum(); 
var valFound = false;

for(var i=0;i<count;i++){        
	  var val = mygrid.cellByIndex(i,0).getValue();
	  if ( val != '')
		  {
			  valFound = true;
			  break;
		  }
    }

	return valFound;
 }

// To save the record
function fnSave(){
	mygrid.editStop();
	var confirmMsg = true;
	
	if(!fnCheckAnyRowsAdded())
		{
			Error_Details(message_sales[160]);
			fnStopProgress();
			Error_Show();
	    	Error_Clear();
			return false;		 
		}
	
	fnStartProgress();
	inputstr = fnCreateGridOrderString();
	document.all.hInputString.value = inputstr;
	document.all.hRemString.value = delRowIds; // Deleted rows to be removed from database
	
	if(document.all.accId.value == '' || document.all.accId.value == '0'){
		Error_Details(message_sales[161]);
		fnStopProgress();
		Error_Show();
    	Error_Clear();
		return false;
	}
	
	document.frmPartPriceAdj.action = "/gmPartPriceAdjSave.do?method=savePartPriceAdj";
	document.frmPartPriceAdj.submit();
}

// To submit the transaction to pending approval
function fnSubmit(){
	mygrid.editStop();
	var confirmMsg = true;
	fnStartProgress();
	inputstr = fnCreateGridOrderString();
	document.all.hInputString.value = inputstr;
	document.all.hRemString.value = delRowIds; // Deleted rows to be removed from database

	if(document.all.accId.value == '' || document.all.accId.value == '0'){
		Error_Details(message_sales[161]);
		fnStopProgress();
		Error_Show();
    	Error_Clear();
		return false;
	}
		
	if(TRIM(inputstr) != '' || TRIM(delRowIds) != ''){
		Error_Details(message_sales[162]);
	}else if(redColFl > 0){
		Error_Details(message_sales[163]);
	}else if (yellowColFl >0){
		fnStopProgress();
		confirmMsg = confirm(message_sales[164]);
	}
	
	if(ErrorCount >0){
		fnStopProgress();
		Error_Show();
    	Error_Clear();
    	return false;
	}
	
	if(!confirmMsg){
    	return false;
    }else{
    	fnStartProgress();
    	document.frmPartPriceAdj.action = "/gmPartPriceAdjSave.do?method=submitPartPriceAdj";
    	document.frmPartPriceAdj.submit();
    }

}


// Create string from grid
function fnCreateGridOrderString()
{
	var j;
	var inputstr = '';
	var added_row;
	var pnum, adjCode, adjVal, unitPrice, nUnitPrice, dtlId, rowIndex;
	
	var gridrows = mygrid.getChangedRows(true);
	var rowsarr = gridrows.toString().split(",");
	
	if (rowsarr.length > 0 && rowsarr != '') {
			for (var k = 0; k < rowsarr.length; k++) {
				var rowcurrentid = rowsarr[k];
				rowIndex=mygrid.getRowIndex(rowcurrentid);
				dtlId = mygrid.cells(rowcurrentid, dtlId_colId).getValue();
				pnum = mygrid.cells(rowcurrentid, part_colId).getValue();
				adjCode =  mygrid.cells(rowcurrentid, adjCode_colId).getValue();
				adjVal =  mygrid.cells(rowcurrentid, adjVal_colId).getValue();
				if(pnum != '')
					inputstr = inputstr + dtlId + '^' + pnum + '^' + adjCode + '^'+ adjVal + '|';
			}
		}
	
	return inputstr;
}

// To void the transaction
function fnVoid(){
	var txnId = document.all.txnId.value;
	var status = document.all.statusid.value;
	
	if(txnId == ''){
		Error_Details(message_sales[165]);
	}else if(status == '105287'){// Implemented
		Error_Details(message_sales[166]);
	}
	
	if (ErrorCount > 0){
		Error_Show();
    	Error_Clear();
    	return;
	}
	
	document.frmPartPriceAdj.action ="/GmCommonCancelServlet?hAction=Load&hCancelType=VDPADJ&hTxnId="+txnId;
	document.frmPartPriceAdj.hDisplayNm.value = 'Part Price Adjustment Initiate';
	document.frmPartPriceAdj.hRedirectURL.value = '/gmPartPriceAdjLoad.do?method=loadPartPriceDtls';
    document.frmPartPriceAdj.submit();
}

// To generate excel
function fnExcelExport(){
	mygrid.editStop();
	var confirmMsg = true;

	inputstr = fnCreateGridOrderString();
	if(inputstr != '' || delRowIds != ''){
		confirmMsg = confirm(message_sales[167]);
	}
	if(!confirmMsg){
    	return false;
    }else{
		mygrid.setColumnHidden(err_flId,false);
		mygrid.toExcel('/phpapp/excel/generate.php');
		mygrid.setColumnHidden(err_flId,true);
    }
}

// For doing copy and paste using keyboard short cut
function onKeyPressed(code,ctrl,shift){
   if(code==67&&ctrl){
      // ctrl-c
	   docopy();
   }
   if(code==86&&ctrl){
      //ctrl-v
	   pasteToGrid();
   }
   return true;
}

// To approve the transaction	
function fnApprove(){
	var transId = document.all.txnId.value;
	var status = document.all.statusid.value;
	
	if(status != '105286'){ //105286: Pending Approval
		Error_Details(message_sales[168]);
	}
	
	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}
		
	fnStartProgress('Y');
	document.frmPartPriceAdj.action = "/gmPartPriceAdjSave.do?method=approvePartPriceAdj";
	document.frmPartPriceAdj.submit();
}

//To deny the transaction	
function fnDeny(){
	var transId = document.all.txnId.value;
	var status = document.all.statusid.value;
	fnValidateTxtFld('txt_LogReason',message_sales[185]);	
	
	if(status != '105286'){ //105286: Pending Approval
		Error_Details(message_sales[169]);
	}
	
	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}
	
	fnStartProgress('Y');
	document.frmPartPriceAdj.action = "/gmPartPriceAdjSave.do?method=denyPartPriceAdj";
	document.frmPartPriceAdj.submit();
}

String.prototype.equalsIgnoreCase = function(otherString) {
    return (this.toUpperCase() == otherString.toUpperCase()) ;
};


