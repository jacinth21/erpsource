var response = '';
var systemId='';
var segmentId='';
var releaseFL ='';
function fnSubmit(){
	
	fnValidate();
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
	}
	else{
		var submitFl='Y';
		systemId= document.getElementById('sys_AccId').value;
		releaseFL = document.frmPriceRequestApprovalRequest.releaseFl.value;
		segmentId = document.frmPriceRequestApprovalRequest.segmentId.value;
		fnStartProgress();
		response = '';
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmAddOrRemoveSystem.do?method=addReleasePRTSystem&systemId='+systemId+'&submitFl='+submitFl+'&releaseFl='+releaseFL+'&segmentId='+segmentId+'&ramdomId=' + Math.random());
		var loader = dhtmlxAjax.getSync(ajaxUrl);
		fnLoader(loader);
	}
	
}
function fnOpenGP() {
	var  systemId= document.getElementById('sys_AccId').value;
	windowOpener('/gmGroupPartPricingDAction.do?method=loadSystemPrice&hsysID='+systemId, "Search","resizable=yes,scrollbars=yes,top=200,left=250,width=1050,height=670");
}
function fnValidate(){
	 systemId= document.getElementById('sys_AccId').value;
	releaseFL = document.frmPriceRequestApprovalRequest.releaseFl.value;
	 segmentId = document.frmPriceRequestApprovalRequest.segmentId.value;
    if(systemId == ''){
    	Error_Details('Please Select System Name');
    }
    if(releaseFL == 'Y' && segmentId=='0'){
    	Error_Details('Please Select Segment Name');
    }
}
//function is used ,when the text box is changed,account id will come in account dorpdown.
function fnGetSystemId()
{		
	document.getElementById("successMsg").innerHTML ='';
	var sysID = document.frmPriceRequestApprovalRequest.systemId.value;
	if (sysID != '0' && sysID != null){
		document.getElementById('sys_AccId').value = sysID ;
	}
	else{
		document.getElementById('sys_AccId').value = '' ;
	}
}
function fnLoader(loader){
	response = loader.xmlDoc.responseText;
	fnStopProgress();
	var success = document.getElementById("successMsg");
	if(response !=''){
		success.innerHTML = 'Record Saved Sucessfully';
	}
}

function fnClear(){
	document.frmPriceRequestApprovalRequest.segmentId.value = '0';
	document.frmPriceRequestApprovalRequest.releaseFl.value= 'Y';
	document.getElementById("searchsystemId").value="";
	document.getElementById('sys_AccId').value = '';
}