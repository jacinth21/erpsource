function fnDollarFormat(num) { 
    return ('$'+ formatNumber(num)); 
} 

function fnGetPriceNum(val) { 
  
	 val = val.toString().replace(/[^0-9.]+/g,'');
	 val= (isNaN(val)||val=='')?0:val;
    return val;
} 

var split_At = '';
var w1,dhxWins,Yval;

// MNTTASK-3518. Stack Overflow. The following functions already is there in dhtmlxwindow.js. I removed there and put here.

function createDhtmlxWindow()
{
	Yval= getMouseXY(event)-200;
	if(arguments.length==0)
		return(	initDhtmlxWindow(750, 480,false,Yval) );
	if(arguments.length==2)
		return(	initDhtmlxWindow(arguments[0], arguments[1] ,false,Yval) );
	if(arguments.length==3)
		return(	initDhtmlxWindow(arguments[0], arguments[1],arguments[2],Yval));
}


function initDhtmlxWindow(width,height,enableView,Yval)
{
	if(dhxWins!=undefined)
		{
			dhxWins.forEachWindow(function(win) {
				dhxWins.setViewport(0, Yval , 0,0);  
			    win.close();
			 });
		}
	
	dhxWins = new dhtmlXWindows();
	dhxWins.enableAutoViewport(enableView);
	dhxWins.setImagePath("extweb/dhtmlx/dhtmlxWindows/imgs");
	
	if(Yval<=0)
		Yval=10;
	if (GLOBAL_ClientSysType == "IPAD")
	{
		dhxWins.setViewport(100, Yval , width+50, height+50);
		w1 = dhxWins.createWindow("w1", 50, 40,width,height);
	}else
	{
		dhxWins.setViewport(60, Yval , width+50, height+50);
		w1 = dhxWins.createWindow("w1", 30, 40,width,height);
	}
	w1.button("park").hide();
	w1.button("minmax1").hide();
	w1.button("close").hide();
	w1.addUserButton("close", "close", "close", "close");
	w1.button("close").attachEvent("onClick", CloseParentDhtmlxWindow);
	w1.keepInViewport(true);
	return w1;
}


function CloseDhtmlxWindow()
{
	
	if (GLOBAL_BrowserType == 'IE') 
	{
    	parent.dhxWins.enableAutoViewport(true);
    }else
    {
    	parent.dhxWins.setViewport(0, 0 , 0, 0);               
    }
	parent.dhxWins.window("w1").close();
}


function CloseParentDhtmlxWindow()
{
	
    if (GLOBAL_BrowserType == 'IE') 
    {
    	dhxWins.enableAutoViewport(true);
    }else
    {
    	dhxWins.setViewport(0, 0 , 0, 0);              
    }
    w1.close();
    
}


function getMouseXY(e) 
{
	  if (GLOBAL_BrowserType == 'IE') 
	  { // grab the x-y pos.s if browser is IE
	    tempX = event.clientX + document.body.scrollLeft
	    tempY = event.clientY + document.body.scrollTop
	  } else {  // grab the x-y pos.s if browser is NS
	    tempX = e.pageX
	    tempY = e.pageY
	  }  
	  // catch possible negative values in NS4
	  if (tempX < 0){tempX = 0}
	  if (tempY < 0){tempY = 0}  
	  // show the position values in the form named Show
	  // in the text fields named MouseX and MouseY
	 
	  return tempY;
	}

//ipad scroll issue
addEvent(window, 'load', setIPADScroll);

function setIPADScroll()
{
	  
	if (GLOBAL_ClientSysType == "IPAD" && parent.dhxWins.window("w1")!=null)
	{
		var body = '<div id="dhxwindow" style="width:100%; height:100%; overflow:auto">'+String(document.body.innerHTML)+' </div> ';   
		document.body.innerHTML = body;
	}		 
}

function addEvent(obj, evType, fn)
{ 
	 if (obj.addEventListener)
	 { 
	   obj.addEventListener(evType, fn, false); 
	   return true; 
	 } else if (obj.attachEvent)
	 { 
	   var r = obj.attachEvent("on"+evType, fn); 
	   return r; 
	 } else 
	 { 
	   return false; 
	 } 
}

		  


