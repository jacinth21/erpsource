//This Function used close the popup window
function fnClose() {
	window.close();
}

//This Function used load the multiple checkbox
function fnOnPageLoad() {
	gridObj = initGridData('dataGridDiv', objGridData);
	gridObj.enablePaging(true, 100, 10, "pagingArea", true);
	gridObj.setPagingSkin("bricks");
}
// This Function used to Initiate the grid
function initGridData(divRef, gridData) {
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.enableDistributedParsing(true);
	gObj.setImagePath("/extweb/dhtmlx/dhtmlxGrid/imgs/");
	gObj.attachHeader("#text_filter,#text_filter,#select_filter,#select_filter,#select_filter");
	// checking account output obj is undefined, to add select all check box
	gObj.enableTooltips("true,true,true,true,true,true");
	gObj.init();
	gObj.loadXMLString(gridData);
	return gObj;
}

//This function will call the excel download
function fnExport()
{	
	gridObj.toExcel('/phpapp/excel/generate.php');
}
