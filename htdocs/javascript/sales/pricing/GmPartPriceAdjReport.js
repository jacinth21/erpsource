var systemLists;
var projectLists;

function initGridData(divRef,gridData){
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);
	gObj.enableTooltips("true,true,true,true,true,true,true,true,true,true,true,true");
	gObj.init();	
	gObj.enablePaging(true, 100, 10, "pagingArea", true, "infoArea");
	gObj.setPagingSkin("bricks");
	gObj.loadXMLString(gridData);	
	return gObj;	
}

function fnChangeDisplay(){
    var type = document.all.type.value;
    document.getElementById("header").innerHTML = (type == 903110)
                                                                                             ? "<font color=red>*</font>"+message_sales[280]
                                                                                                           : (type == 903111)
                                                                                                                  ? "<font color=red>*</font>"+message_sales[281]
    
                                                                                                                		  : "<font color=red>*</font>"+message_sales[282] ;
    
    if(type == 903110){ //Account
           var objAccBlk = eval('document.all.acc_name.style');
           objAccBlk.display = 'block';
           var objGrpAccBlk = eval('document.all.grp_acc_name.style');
           objGrpAccBlk.display = 'none';
           document.all.accId.value = '';
           document.all.accName.value = '0';
           fnLoadAcctType(document.all.accId);
    }else{
           var objAccBlk = eval('document.all.acc_name.style');
           objAccBlk.display = 'none';
           var objGrpAccBlk = eval('document.all.grp_acc_name.style');
           objGrpAccBlk.display = 'block';           
           document.all.accId.value = '';
           fnLoadAcctType(document.all.accId);
    }
}

//Function for loading the default values on loading the JSP.
	function fnOnPageLoad(){		
		if (objGridData != '') {
			gridObj = initGridData('PartPriceAdjReport',objGridData);
		}
				
		var accID = document.frmPartPriceAdjRpt.accId.value;
		var accNm = document.frmPartPriceAdjRpt.accName.value; 
		if(accID != '0' && accID != ''){			
			var objAccDetails = document.getElementById('headerDetails').style;
			objAccDetails.display = 'block';
			fnLoadAcctType(document.frmPartPriceAdjRpt.accId);
		}
		fnChangeDisplay();
		fnCallType();
		document.frmPartPriceAdjRpt.accId.value = accID;
		if(document.frmPartPriceAdjRpt.accId.value == ''){
			document.frmPartPriceAdjRpt.accName.value = '0';
		}else{
			document.frmPartPriceAdjRpt.accName.value = accID;
		}	
		
		var qyeryType = document.frmPartPriceAdjRpt.queryByType.value;
		if(qyeryType == 120001){  // System Name
			arr = document.all.Chk_GrpQuerySystemList;
			fnCheckSelections(systemString,arr,document.all.selectSystemQueryList);
		}else{  // Project Name
			arr = document.all.Chk_GrpQueryProjectList;
			fnCheckSelections(projectString,arr,document.all.selectSystemQueryList);
		}
		
	}
	
	
//Function for loading the Account Basic details and Resetting the same based on the Type Drop-down.
	
	function fnLoadAcctType(obj){						 
		var accountID = obj.value;
		var type = document.frmPartPriceAdjRpt.type.value;
		if(type == 903110){ // Account
			document.all.accName.value = obj.value;
			if(TRIM(document.all.accName.value) =='' && TRIM(obj.value) != ''){
				Error_Details(Error_Details_Trans(message_sales[152],obj.value));
			}else if(TRIM(obj.value) == ''){
				document.all.accName.value = 0;
			}
		}else if(type == 903111){
			document.all.grpAccName.value = obj.value;
			if(TRIM(document.all.grpAccName.value) == ''  && TRIM(obj.value) != ''){
				Error_Details(Error_Details_Trans(message_sales[152],obj.value));
			}else if(TRIM(obj.value) == ''){
				document.all.grpAccName.value = 0;
			}
		}
		if(ErrorCount >0){
			fnStopProgress();
			Error_Show();
	        Error_Clear();
	        document.all.accId.value = '';
	        document.all.grpAccName.value = 0;
	        document.all.accName.value = 0;
		}else{
			if(accountID != '' && accountID != '0'){
				document.frmPartPriceAdjRpt.accId.value = accountID;	
				if(type == 903110){
					document.all.accName.value = obj.value;
				}else{
					document.all.grpAccName.value = obj.value;
				}
				fnCallAjax('GETACC',accountID);
			}else{
				document.frmPartPriceAdjRpt.accId.value = '';
				document.frmPartPriceAdjRpt.accName.value = '0';
				var objAccDetails = document.getElementById('headerDetails').style;
				objAccDetails.display = 'none';
			}
		}			
	}
	
	//Function for Getting the Account Basic Information through Ajax Call.
	function fnCallAjax(val,accountID){
		var repid = '';
		var typeVal = document.frmPartPriceAdjRpt.type.value;
	    var url = "/gmPartPriceAdjustmentRptAction.do?method=fetchHeaderDetails&accId="+accountID+"&type="+typeVal+"&accName="+accountID+"&systemList="+systemString+"&projectList="+projectString;	    
	    if (typeof XMLHttpRequest != "undefined"){
		   req = new XMLHttpRequest();
		}else if (window.ActiveXObject){
		   req = new ActiveXObject("Microsoft.XMLHTTP");
		}
		req.open("GET", url, true);	
		req.onreadystatechange = callback;		
		req.send(null);
	}
	
	//Function for Getting the Account Basic Information xml response.
	function callback(){		
	    if (req.readyState == 4) {
	        if(req.status == 200) {
		        var xmlDoc = req.responseXML;		       
				parseMessage(xmlDoc);
	        }else{
	        	setErrMessage('<span class=RightTextBlue>'+message_sales[283]+'</span>');
	        }
	    }
	}
	
	//Function for getting the Account Basic Information from xml response.
	function parseMessage(xmlDoc){
		var pnum = xmlDoc.getElementsByTagName("acc");
		
		for (var x=0; x<pnum.length; x++){
			var rgname = parseXmlNode(pnum[x].childNodes[0].firstChild);
			var adname = parseXmlNode(pnum[x].childNodes[1].firstChild);
			var dname = parseXmlNode(pnum[x].childNodes[2].firstChild);
			var trname = parseXmlNode(pnum[x].childNodes[3].firstChild);
			var rpname = parseXmlNode(pnum[x].childNodes[4].firstChild);
			repid = parseXmlNode(pnum[x].childNodes[5].firstChild);
			var prname = parseXmlNode(pnum[x].childNodes[6].firstChild);
			var prid = parseXmlNode(pnum[x].childNodes[7].firstChild);
			var gpocatid = parseXmlNode(pnum[x].childNodes[8].firstChild);
			var acname = parseXmlNode(pnum[x].childNodes[9].firstChild);
			var accid = parseXmlNode(pnum[x].childNodes[10].firstChild);
			distid = parseXmlNode(pnum[x].childNodes[11].firstChild);
			vmode =  parseXmlNode(pnum[x].childNodes[12].firstChild);			
			rpname = rpname + ' ('+repid+')';
			//Account Affiliation
			var gpraff = parseXmlNode(pnum[x].childNodes[13].firstChild);
			var hlcaff =  parseXmlNode(pnum[x].childNodes[14].firstChild);
			// get the account type
			accountType = parseXmlNode(pnum[x].childNodes[15].firstChild);
			var accComments = '';
			if(pnum[x].childNodes[16]!=undefined && pnum[x].childNodes[16]!=null)
			{
			   accComments =  parseXmlNode(pnum[x].childNodes[16].firstChild);
			}
			vcarr =  parseXmlNode(pnum[x].childNodes[18].firstChild);
		}
	
		if (accid == '' || rpname == ''){
			Error_Details(message_sales[170]);
			Error_Show();	
			Error_Clear();
			document.frmPartPriceAdjRpt.accId.value = '';
			document.frmPartPriceAdjRpt.accName.value = '0';
			var objAccDetails = document.getElementById('headerDetails').style;
			objAccDetails.display = 'none';
			return false;
		}else{
			document.frmPartPriceAdjRpt.accName.value = accid;
			document.frmPartPriceAdjRpt.grpAccName.value = accid;	
			setMessage(rgname,adname,dname,trname,rpname,prname,prid,gpocatid,accid,gpraff,hlcaff,accComments);									
		}
	}
	
	//Function for setting the Account Basic Information xml response.
	function setMessage(rgname,adname,dname,trname,rpname,prname,prid,gpocatid,accid,gpraff,hlcaff,accComments){
		 var type = document.all.type.value;
		var obj = eval("document.all.Lbl_Regn");
		obj.innerHTML = "&nbsp;" + rgname;

	    obj = eval("document.all.Lbl_AdName");
	    obj.innerHTML = "&nbsp;" + adname;

	    obj = eval("document.all.Lbl_DName");
	    obj.innerHTML = "&nbsp;" + dname;

	    obj = eval("document.all.Lbl_TName");
	    obj.innerHTML = "&nbsp;" + trname;
	    
	    if( type == 903110){
	    	 obj = eval("document.all.Lbl_RName");
	 	     obj.innerHTML = "&nbsp;" + rpname;
	    }else{
	    	 obj = eval("document.all.Lbl_RName");
	 	     obj.innerHTML = "&nbsp;" + '';
	    }	   

	    obj = eval("document.all.Lbl_PName");
	    obj.innerHTML = "&nbsp;" + prname;
	    
	    // Account Affiliation
	    obj = eval("document.all.Lbl_GPRAffName");
	    if(obj!=undefined && obj!=null)
	    	obj.innerHTML = "&nbsp;" + gpraff;

	    obj = eval("document.all.Lbl_HlthCrAffName");
	    if(obj!=undefined && obj!=null)
	    	obj.innerHTML = "&nbsp;" + hlcaff;
	    document.frmPartPriceAdjRpt.accName.value = accid;	  
	    document.frmPartPriceAdjRpt.grpAccName.value = accid;
	    var objAccDetails = document.getElementById('headerDetails').style;
		objAccDetails.display = 'block';
	}

	
	//Function for Multiple values Checkbox selections.
	function fnSelectSystemName(){		
		var k = 0;
		var objCheckInvArr = document.all.Chk_GrpQuerySystemList;
		var len = objCheckInvArr.length;
		for( var j=0; j<len; j++){
			if(objCheckInvArr[j].checked){
				k++;
			}else{
				 document.all.selectSystemQueryList.checked = false;
				 return;
			}	
		}
		if(k == len){
			document.all.selectSystemQueryList.checked = true;
		}		
	}	
	
	
	//Function for Multiple values Checkbox selections.
	function fnSelectProjectName(){		
		var k = 0;
		var objCheckInvArr = document.all.Chk_GrpQueryProjectList;
		var len = objCheckInvArr.length;
		for( var j=0; j<len; j++){
			if(objCheckInvArr[j].checked){
				k++;
			}else{
				 document.all.selectProjectQueryList.checked = false;
				 return;
			}	
		}
		if(k == len){
			document.all.selectProjectQueryList.checked = true;
		}		
	}
	
	//Function for Multiple values Checkbox selections.
	function fnSelectAllSystemLists(varControl,varControlToSelect,varCmd){
		objSelAll = varControl;		
		if (objSelAll ) {
		var objCheckSiteArr = eval("document.all."+varControlToSelect);
		if ((objSelAll.checked || varCmd == 'selectAll') && objCheckSiteArr){
			objSelAll.checked = true;
			objCheckSiteArr.checked = true;
			objCheckSiteArrLen = objCheckSiteArr.length;
			for(i = 0; i < objCheckSiteArrLen; i ++){	
				objCheckSiteArr[i].checked = true;
			}
		}else if (!objSelAll.checked  && objCheckSiteArr ){
			objCheckSiteArrLen = objCheckSiteArr.length;
			objCheckSiteArr.checked = false;
			for(i = 0; i < objCheckSiteArrLen; i ++){
				objCheckSiteArr[i].checked = false;
			}
		}
		}
		if (varControl.name == 'selectSystemQueryList'){
			if(document.frmPartPriceAdjRpt.Chk_GrpQuerySystemList){
				document.frmPartPriceAdjRpt.Chk_GrpQuerySystemList.checked = document.frmPartPriceAdjRpt.Chk_GrpQuerySystemList.checked;
				fnSelectAll(document.frmPartPriceAdjRpt.Chk_GrpQuerySystemList,'Chk_GrpQuerySystemList','toggle');
			}			
		}		
	}
	
	
	//Function for Multiple values Checkbox selections.
	function fnSelectAllProjectLists(varControl,varControlToSelect,varCmd){
		objSelAll = varControl;
		if (objSelAll ) {
		var objCheckSiteArr = eval("document.all."+varControlToSelect);
		if ((objSelAll.checked || varCmd == 'selectAll') && objCheckSiteArr){
			objSelAll.checked = true;
			objCheckSiteArr.checked = true;
			objCheckSiteArrLen = objCheckSiteArr.length;
			for(i = 0; i < objCheckSiteArrLen; i ++){	
				objCheckSiteArr[i].checked = true;
			}
		}else if (!objSelAll.checked  && objCheckSiteArr ){
			objCheckSiteArrLen = objCheckSiteArr.length;
			objCheckSiteArr.checked = false;
			for(i = 0; i < objCheckSiteArrLen; i ++){
				objCheckSiteArr[i].checked = false;
			}
		}
		}
		if (varControl.name == 'selectProjectQueryList'){
			document.all.Chk_GrpQueryProjectList.checked = document.all.Chk_GrpQueryProjectList.checked;
			fnSelectAll(document.all.Chk_GrpQueryProjectList,'Chk_GrpQueryProjectList','toggle');
		}		
	}
	
	// Function for getting the Account Basic Information ontabout the Account ID textbox.
	// Calling from GmPartPriceAdjFilter.jsp
	function fnLoadAccDetails(obj){		
		fnLoadAcctType(obj);
		fnClearProjects();
		fnClearSystems();
	}
	
	// Function to fetch the Part Price Adjustment report based on the selected filters
	function fnLoadPartPriceData(){	
		var selectedSystemList = '';
		var selectedProjectList = '';
		
		var accID = document.frmPartPriceAdjRpt.accId.value;
		var accName = document.frmPartPriceAdjRpt.accName.value;
		var gpnmae = document.frmPartPriceAdjRpt.grpAccName.value;
		var typeId = document.frmPartPriceAdjRpt.type.value;
		var partNum = document.frmPartPriceAdjRpt.partNum.value;
		var queryType = document.frmPartPriceAdjRpt.queryByType.value;
		var partLike = document.frmPartPriceAdjRpt.partLike.value;
		
		if(partNum == '' && (partLike != '' && partLike != '0')){
			Error_Details(message_sales[171]);
		}
		
		var lblName = (typeId == 903110) ? '  <B>'+message_sales[280]+'</B>' : ' <B>'+message_sales[281]+'</B>';
		if((accName == '' || accName == '0') && (accID == '' )){
			Error_Details(Error_Details_Trans(message_sales[172],lblName));
		}
		// Preparing the selected system names input string
		if(document.all.Chk_GrpQuerySystemList){
			arr = document.all.Chk_GrpQuerySystemList;		
			for(var i=0;i<arr.length;i++){
				if(arr[i].checked){		
					selectedSystemList = selectedSystemList + arr[i].id +',';
				}			
			}
		}
		// Preparing the selected project names input string
		if(document.all.Chk_GrpQueryProjectList){
			arr = document.all.Chk_GrpQueryProjectList;		
			for(var i=0;i<arr.length;i++){
				if(arr[i].checked){		
					selectedProjectList = selectedProjectList + arr[i].id +',';
				}			
			}
		}	
		if (ErrorCount > 0){
			Error_Show();
			Error_Clear();
			return false;
		}else{
			document.frmPartPriceAdjRpt.partNum.value = partNum;
			document.frmPartPriceAdjRpt.partLike.value = partLike;
			document.frmPartPriceAdjRpt.queryByType.value = queryType;
			document.frmPartPriceAdjRpt.action = '/gmPartPriceAdjustmentRptAction.do?method=fetchPartPriceAdjDetails&accid='+accID+"&systemList="+selectedSystemList+"&queryByType="+queryType+"&projectList="+selectedProjectList;
			fnStartProgress();
		    document.frmPartPriceAdjRpt.submit();
		}			
	}
	
	// Preparing the Selected 120001S/PROJECT NAMES as comma separated string.
	function fnCallType(){	
		var qyeryType = document.frmPartPriceAdjRpt.queryByType.value;
		if(qyeryType == 120001){  // SYSTEM NAME
			var objQueryList = document.getElementById('systemList').style;
		    objQueryList.display = 'block';
		    var objQueryList = document.getElementById('projectlist').style;
		    objQueryList.display = 'none';
		    fnClearProjects();
		}else{  // PROJECT NAME
			var objQueryList = document.getElementById('systemList').style;
		    objQueryList.display = 'none';
		    var objQueryList = document.getElementById('projectlist').style;
		    objQueryList.display = 'block';
		    fnClearSystems();
		}		
	}
	
	function fnClearProjects(){
		if(document.all.Chk_GrpQueryProjectList){
			var objCheckSiteArr = eval("document.all.Chk_GrpQueryProjectList");
			objCheckSiteArrLen = objCheckSiteArr.length;
			objCheckSiteArr.checked = false;
			for(i = 0; i < objCheckSiteArrLen; i ++){
				objCheckSiteArr[i].checked = false;
			}
		}
		document.all.selectProjectQueryList.checked = false;
	}
	
	function fnClearSystems(){
		if(document.all.Chk_GrpQuerySystemList){
			var objCheckSiteArr = eval("document.all.Chk_GrpQuerySystemList");
			objCheckSiteArrLen = objCheckSiteArr.length;
			objCheckSiteArr.checked = false;
			for(i = 0; i < objCheckSiteArrLen; i ++){
				objCheckSiteArr[i].checked = false;
			}
		}
		document.all.selectSystemQueryList.checked = false;
	}
	
	function fnCheckSelections(val,arr,selAllObj){
		var valobj = val.split(",");
		var sval = '';
		var arrlen = arr.length;
		var selItemCount = 0;
		if (arrlen > 0)	{
			for (var j=0;j< valobj.length;j++ ){
				sval = valobj[j];
				for (var i=0;i< arrlen;i++ ){
					if (arr[i].id == sval){
						arr[i].checked = true;
						selItemCount++;
						break;
					}
				}
			}		
			if(selItemCount==arrlen)
				selAllObj.checked = true;		
		}else{
			arr.checked = true;
			selAllObj.checked = true;
		}
	}
	 
	function fnOpenHistory(partyid,partnum){
		windowOpener("/gmPartPriceAdjustmentRptAction.do?method=fetchPartPriceHystoryDetails&partyId="+partyid+"&partNum="+encodeURIComponent(partnum),"","resizable=yes,scrollbars=yes,top=200,left=300,width=1150,height=350");		
	}
	
	function fnDownloadXLS(){
		gridObj.toExcel('/phpapp/excel/generate.php');
	}