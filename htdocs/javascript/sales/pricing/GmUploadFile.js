function fnFileUpload(form)
{

var filename = document.frmPriceApprovalRequest.file.value;

if(filename=="" ||filename==undefined){
	Error_Details(message[5530]);
	
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
}else{
	var response="";
	var ajaxUrl=fnAjaxURLAppendCmpInfo('/gmPriceApprovalRequestAction.do?method=checkFileExist&fileUpload='+filename+'&priceRequestId=' + priceRequestId.toUpperCase()+'&randomId='+Math.random());
	dhtmlxAjax.get(ajaxUrl, function(loader){
		response = loader.xmlDoc.responseText;
		 if(response == "Y")
		    {
				 confirmSubmit = confirm("File exists. Do you want to overwrite?");
				 if(confirmSubmit)
					 {
					 document.frmPriceApprovalRequest.action = "/gmPriceApprovalRequestAction.do?method=saveUploadFile&fileUpload="+filename+"&priceRequestId=" + priceRequestId.toUpperCase();	
					 form.setAttribute('enctype','multipart/form-data');
					 fnStartProgress('Y');
					 form.submit();
					 }
				 else
				 {
					 fnStopProgress();
					 return false;
				 }
			 }
			 else
				 {
		document.frmPriceApprovalRequest.action = "/gmPriceApprovalRequestAction.do?method=saveUploadFile&fileUpload="+filename+"&priceRequestId=" + priceRequestId.toUpperCase();	
		form.setAttribute('enctype','multipart/form-data');
		fnStartProgress('Y');
		form.submit();
				 }
		});
	
      }
 }

function fnVoid(form)
{
	var upload_file_id="";	
	gridObj.forEachRow(function(rowId) {
	chk_upload_rowID = gridObj.getColIndexById("chk_box");
	checked = gridObj.cellById(rowId, chk_upload_rowID).getValue();
	  if (checked == '1') { // Checked
			var id=gridObj.getColIndexById("Upload_id");
			var upload_id = gridObj.cellById(rowId, id).getValue();
			upload_file_id = upload_file_id+upload_id+'|';
		}
	});
	document.frmPriceApprovalRequest.action = "/gmPriceApprovalRequestAction.do?method=deleteUpdateFile&priceRequestId="+ priceRequestId.toUpperCase()+"&UploadFileId="+upload_file_id;
	fnStartProgress('Y');
	form.submit();
	
	
}
function fnFileView(fileid,fileRefid){
	
	document.frmPriceApprovalRequest.haction.value = '';
	document.frmPriceApprovalRequest.action = '/gmPriceApprovalRequestAction.do?method=fileView&fileID='+fileid+'&typeID='+fileRefid;
	document.frmPriceApprovalRequest.submit();
	
}
function fnClose(){	
	window.close();	
	fnRefreshParent();
}

function fnRefreshParent(){
	if(window.opener != null && !window.opener.closed){
		window.opener.location.reload();
	}
}

