
function fnLoadAcctId(obj){// To set the account id while changing the Name dropdown
	document.all.accId.value = (obj.value == '0') ? '' : obj.value;
}

// To load the account name in grid, while tabout from account id
function fnLoadAccountName(obj){
	var type = document.all.type.value;
	
	if(type == 903110){ //Account
		document.all.accName.value = obj.value;
		if(TRIM(document.all.accName.value) =='' && TRIM(obj.value) != ''){
			Error_Details(Error_Details_Trans(message_sales[152],obj.value));
		}else if(TRIM(obj.value) == ''){
			document.all.accName.value = 0;
		}
	}else if(type == 903111){ // Group Price book
		document.all.grpAccName.value = obj.value;
		if(TRIM(document.all.grpAccName.value) == ''  && TRIM(obj.value) != ''){
			Error_Details(Error_Details_Trans(message_sales[152],obj.value));
		}else if(TRIM(obj.value) == ''){
			document.all.grpAccName.value = 0;
		}
	}
	
	if(ErrorCount >0){
		fnStopProgress();
		Error_Show();
        Error_Clear();
        document.all.accId.value = '';
        document.all.grpAccName.value = 0;
        document.all.accName.value = 0;
	}
}

//To show/hide dropdown based on the type
function fnChangeDisplay(){
	
	var type = document.all.type.value;
	var accId, grpId;
	accId = document.all.accName.value;
	grpId = document.all.grpAccName.value;
	var accIdFl = false;
	if(accId == 0 && grpId == 0 && document.all.accId.value != ''){
		accIdFl = true;
	}
	document.getElementById("header").innerHTML = (screen == 'REPORT' && type == 903110)
														? "<font color=red>*</font> Account Name:"
																: (screen == 'REPORT')
																	? "<font color=red>*</font> Group Book Name:"
																			: (screen == 'Dashboard')
																				? "Name:"
																						:"<font color=red>*</font> Name:";
	
	if(type == 903110){ //Account
		var objAccBlk = eval('document.all.acc_name.style');
  		objAccBlk.display = 'block';
  		var objGrpAccBlk = eval('document.all.grp_acc_name.style');
  		objGrpAccBlk.display = 'none';
  		var objEmptyBlk = eval('document.all.empty_list.style');
  		objEmptyBlk.display = 'none';
  		document.all.grpAccName.value = 0;
	}else if(type == 903111){//Group Account
		var objGrpAccBlk = eval('document.all.grp_acc_name.style');
  		objGrpAccBlk.display = 'block';
		var objAccBlk = eval('document.all.acc_name.style');
  		objAccBlk.display = 'none';
  		var objEmptyBlk = eval('document.all.empty_list.style');
  		objEmptyBlk.display = 'none';
  		document.all.accName.value = 0;
	}else{
		var objAccBlk = eval('document.all.acc_name.style');
  		objAccBlk.display = 'none';
  		var objGrpAccBlk = eval('document.all.grp_acc_name.style');
  		objGrpAccBlk.display = 'none';
  		var objEmptyBlk = eval('document.all.empty_list.style');
  		objEmptyBlk.display = 'block';
  		document.all.grpAccName.value = 0;
  		document.all.accName.value = 0;
	}
	
	accId = document.all.accName.value;
	grpId = document.all.grpAccName.value;
	if(accId == 0 && grpId == 0 && !accIdFl){
		document.all.accId.value = '';
	}
		
}

// When click on load button
function fnLoad(obj){
	var newAccId = document.all.accId.value;
	var btnVal = (obj) ? obj.value : '';
	
	//Should reload the page only if the Load button is clicked or if the account id is changed
	if( btnVal == 'Load' || (prevAccId != newAccId && btnVal != 'Load')){
		fnStartProgress();
		var type = document.all.type.value;
		if(document.all.txnId){
			document.all.txnId.value = '';
		}
		if(document.all.statusid){
			document.all.statusid.value = '';
		}
		fnValidateDropDn('type',lblType);
		if(type == '903110' && screen != 'Dashboard') //903110: Account
			fnValidateDropDn('accName',message_sales[187]);
		else if(type == '903111' && screen != 'Dashboard') //903111: Group account
			fnValidateDropDn('grpAccName',message_sales[187]);
		
		if(ErrorCount > 0){
			fnStopProgress();
			if(btnVal == 'Load'){// Should show the error only while clicking on load button
				Error_Show();
				Error_Clear();
				return;
			}else{
				Error_Clear();
				return;
			}
		}else{
			document.frmPartPriceAdj.action = "/gmPartPriceAdjLoad.do?method=fetchPartPriceDtls";
			document.frmPartPriceAdj.submit();
		}
	}
	
}




