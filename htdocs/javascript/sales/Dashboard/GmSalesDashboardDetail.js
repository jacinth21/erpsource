var selectedReqIds = "";
var selectedRowCnt = "";
var requestid = "";
var rowid = '';
var pendapp_rowId = '';
//PC-2365 Add Excel Export Option in Missing Parts Loaner Details - Sales Dashboard
var gObj;
function initGridData(divRef,gridData)
{
	gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);
	gObj.init();
	if(strOpt == "detail"){
		gObj.attachFooter(",,,,"+message_sales[32]+":,<div id='divTotal'>0</div>",["background-color:#DCDCDC;border:none;","background-color:#DCDCDC;border:none;","background-color:#DCDCDC;border:none;","background-color:#DCDCDC;border:none;","background-color:#DCDCDC;border:none;;font-weight:bold;border:none;","background-color:#DCDCDC;border:none;;font-weight:bold;border:none;"]);
	}else{
		gObj.enablePaging(true, 15, 10, "pagingArea", true, "infoArea");
		gObj.setPagingSkin("bricks");
	}
	gObj.loadXMLString(gridData);	
	return gObj;	
}

function fnOnPageLoad()
{  			
		gridObj = initGridData('GridLayout',objGridData);
		gridObj.attachEvent("onFilterStart",fnCustomFilter);
		gridObj.attachEvent("onCheckbox",onClickSelect);		
		if(strOpt == "detail"){
		    var total = 0;
		    for (var i = 0; i < gridObj.getRowsNum(); i++) {
		    	var val = gridObj.cellById(i,5).getValue();
		    	val=val.replace(/<[^>]*>/g,"").replace(eval("/\\"+currSign+"|\,/g"),'');
		    	if(val.indexOf("(")!=-1){
		    		val=val.replace(/\(|\)/g,'');
		    		total -= parseFloat(val);
		    	}else{
		    		total += parseFloat(val);
		    	}
		    	
		    }
		    var divTotal = document.getElementById("divTotal");
		   divTotal.innerHTML = formatCurrency(total);
		}
		pendapp_rowId = gridObj.getColIndexById("pendApprove");
		reqStatus_rowId = gridObj.getColIndexById("reqStatus");
		for (var i = 0; i < gridObj.getRowsNum(); i++) {
			var check_Ind = gridObj.getColIndexById("check");
			check_id = gridObj.cellById(i, check_Ind).getValue();
			pendaprr_id = gridObj.cellById(i, pendapp_rowId).getValue();
			reqStatus_id = gridObj.cellById(i, reqStatus_rowId).getValue();
			if (pendaprr_id == 'Y') {
				gridObj.cellById(i, check_Ind).setDisabled(true);
			}
			if (reqStatus_id < 40){
				document.getElementById("Btn_Extend").style.visibility = "hidden";
			}
		}
}
function onClickSelect(id,cellInd,state){
	var checkInd = gridObj.getColIndexById("check");
	var allRowCount = gridObj.getRowsNum();
	request_rowId = gridObj.getColIndexById("detail");
	
	var request_id = gridObj.cellById(id, request_rowId).getValue();
		if(state){
			selectedReqIds += ','+request_id;
			selectedRowCnt++;
			if(selectedRowCnt == allRowCount){
				document.all.mastercheck.checked = true ;	
			} 			
		}else{
			selectedReqIds=selectedReqIds.replace(','+request_id,'');
			document.all.mastercheck.checked = false ;
			selectedRowCnt--;			
		}		
		requestid = selectedReqIds.substr(1,selectedReqIds.length);
 }
function onClickSelectAll(val){
	var checkInd = gridObj.getColIndexById("check");
	if(val.checked == true && checkInd == 0 ){
		for (var i = 0; i < gridObj.getRowsNum(); i++) {
			pendaprr_id = gridObj.cellById(i, pendapp_rowId).getValue();
			reqStatus_id = gridObj.cellById(i, reqStatus_rowId).getValue();
			if (pendaprr_id != 'Y'){
				gridObj.cellById(i,checkInd).setChecked(true);
				selectedReqIds += ','+i;
				selectedRowCnt++;				
			}
		 }
	}else{ 
		for (var i = 0; i < gridObj.getRowsNum(); i++) {
			pendaprr_id = gridObj.cellById(i, pendapp_rowId).getValue();
			reqStatus_id = gridObj.cellById(i, reqStatus_rowId).getValue();
			if (pendaprr_id != 'Y'){			
				gridObj.cellById(i,checkInd).setChecked(false);
				selectedReqIds = '';
				selectedRowCnt ='';				
			}
	 }
	}
}

function formatCurrency(num) {
	num = num.toString();
	if(isNaN(num))
		num = "0";
	sign = (num == (num = Math.abs(num)));
	num = Math.floor(num*100+0.50000000001);
	cents = num%100;
	num = Math.floor(num/100).toString();
	if(cents<10)
		cents = "0" + cents;
	for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
		num = num.substring(0,num.length-(4*i+3))+','+num.substring(num.length-(4*i+3));
	if(sign){
		return currSign+'&nbsp;' + num+ '.' + cents;
	}else{
		return '<font color=red>'+currSign+'&nbsp;(' + num+ '.' + cents+')</font>'; //(((sign)?'':'-') +
	}
	}

function fnCustomFilter(ord,data){
/* This custom filter to avoid hyper link content column filter issue. 
 * Because default filter consider all text with in the tag. 
 * Ex. Let us assume xml as <cell><a ..>text</a></cell>.  If user try to file 'a' text then all rows will show. */
	
	for(i=0;i<data.length;i++){
		if(data[i]!="" && data[i] != undefined && (""+data[i]).indexOf("anonymous")==-1){
			var original=data[i];
			original = original.toLowerCase();
			data[i]=function(value){
						if (value.toString().replace(/<[^>]*>/g,"").toLowerCase().indexOf(original)!=-1)
							return true;
						return false;
						};
		}
			
	}
	gridObj.filterBy(ord,data);
	return false;
}

function fnClose()
{
	if(strOpt == "todaysales"){
		parent.dhxWins.window("w1").close();
	}else{
		parent.dhxWins.window("popupWindow").close();
	}
}

function fnOpenDetailPopup(url,popupName)
{
	 dhxWins = new dhtmlXWindows();
	 dhxWins.enableAutoViewport(false);
     dhxWins.attachViewportTo(document.body);
     dhxWins.setImagePath("extweb/dhtmlx/dhtmlxWindows/imgs");
     //open popup window	 
     if (!dhxWins.window("newcontact_win"))
     {
    	 var popupXY = 0;
    	 var popupH = 500;
    	 var popupW = 950;

    	 if (GLOBAL_ClientSysType == "IPAD"){
        	 popupXY = 0;
			 popupH = 425;
        	 popupW = 920;
    	 }
    	 
    	 if(strOpt == "todaysales"){//Popup width and height is changed for todaysales.Because it will popup as a third nested popup
    		 popupW = 880;
    		 popupH = 350;
    	 }
    	 popupDtlWin = dhxWins.createWindow("w1", popupXY, popupXY, popupW, popupH);
    	 url=fnAjaxURLAppendCmpInfo('/sales/DashBoard/GmSalesDashDtlPopup.jsp?'+url+'&ramdomId='+Math.random());
    	 popupDtlWin.setText(popupName);
    	 popupDtlWin.attachURL(url);
    	 popupDtlWin.center();
    	 popupDtlWin.setModal(true);
     }
}

function fnOpenTrack(val, shippingCarrier){
	
	var screenW = 640, screenH = 480;
	if (parseInt(navigator.appVersion)>3) {
	 screenW = screen.width;
	 screenH = screen.height;
	}
	else if (navigator.appName == "Netscape" 
	    && parseInt(navigator.appVersion)==3
	    && navigator.javaEnabled()
	   ) 
	{
	 var jToolkit = java.awt.Toolkit.getDefaultToolkit();
	 var jScreenSize = jToolkit.getScreenSize();
	 screenW = jScreenSize.width;
	 screenH = jScreenSize.height;
	}
	screenH = screenH - 90;
	screenW = screenW - 10;
	
	var replaceStr = ' ';
	val = val.replace(/~/g,replaceStr);
    if(shippingCarrier == '5001'){ //fedex
    	windowOpener('https://www.fedex.com/fedextrack?ascend_header=1&clienttype=dotcom&cntry_code=us&language=english&tracknumbers='+val,"Track","resizable=yes,minimize=no,scrollbars=yes,top=0,left=0,height="+screenH+",width="+screenW);
    }      
    else if(shippingCarrier == '5000'){}
}

function alink(a,b,order){ 
	/* If hyperlink added then default sorting is not working in dhtmlx grid*/
    a=a.replace(/<[^>]*>/g,"");
    b=b.replace(/<[^>]*>/g,"");
    if(order=="asc")
        return a>b?1:-1;
     else
        return a<b?1:-1;
}

function lnkprice(a,b,order){ 
    a=a.replace(/<[^>]*>/g,"");
    b=b.replace(/<[^>]*>/g,"");
    
	var aValue = parsePrice(a);
	var bValue = parsePrice(b);
    
    if(order=="asc")
        return aValue>bValue?1:-1;
     else
        return aValue<bValue?1:-1;
}

function price(a,b,order){ 
	/* If hyperlink added then default sorting is not working in dhtmlx grid*/
	var aValue = parsePrice(a);
	var bValue = parsePrice(b);
    
    if(order=="asc")
        return aValue>bValue?1:-1;
     else
        return aValue<bValue?1:-1;
}

function parsePrice(val){
	val=val.replace(/\$|\,/g,'');
    if(val.indexOf("(")!= -1){
    	val = val.replace(/\(|\)/g,'');
    	return(parseInt(val) * -1) ;
    }else{
    	return (parseInt(val));
    }
}

function fnApproval(){
	var Approved_str = '';
	var consign_str = '';
	var chk = gridObj.getCheckedRows(0).split(",");	
	if(chk == ''){
		Error_Details(message_sales[28]);
	}else{
		for (var i=0;i<chk.length;i++){
			consinid_id = '';
			check_rowId  = gridObj.getColIndexById("Consign");
			detail_rowId = gridObj.getColIndexById("detailid"); 
			consinid_id  = gridObj.cellById(chk[i], check_rowId).getValue();
			details_id   = gridObj.cellById(chk[i], detail_rowId).getValue();
			consign_str  = consign_str + consinid_id + ',';	
			Approved_str = Approved_str + details_id + ',';
		}
	}
	if (ErrorCount > 0) {
		Approved_str = '';
		Error_Show();
		Error_Clear();
		return false;
	}else{
		document.frmLoanerExtApprl.inputString.value = Approved_str;
		document.frmLoanerExtApprl.action = '/gmLoanerExtApprl.do';
		document.frmLoanerExtApprl.strOpt.value = "Save";
		document.frmLoanerExtApprl.consignId.value = consign_str;
		if (confirm(message_sales[29])){
			fnStartProgress();
			document.frmLoanerExtApprl.submit();
			document.frmLoanerExtApprl.Btn_Approve.disabled = true;
		}
	}
}

function fnExtend(){
	var extended_str = '';	
	var chk = gridObj.getCheckedRows(0).split(",");
	if(chk == ''){
		Error_Details(message_sales[30]);
	}else{
		for (var i=0;i<chk.length;i++){		
			detail_rowId = gridObj.getColIndexById("detail"); 
			details_id   = gridObj.cellById(chk[i], detail_rowId).getValue();
			extended_str = extended_str + details_id + ',';
		}	
	}
		
	if (ErrorCount > 0) {
		extended_str = '';
		Error_Show();
		Error_Clear();
		return false;
	}else{		
		if (confirm(message_sales[31])){
			windowOpener("/gmSalesDashLoanerExt.do?method=fetchLoanerExt&strOpt=fetchReqDetails&inputString="+ extended_str, "","resizable=yes,scrollbars=yes,top=250,left=300,width=860,height=540,status=1");
		}
	}	
}
//PC-2365 Add Excel Export Option in Missing Parts Loaner Details - Sales Dashboard
//This function used to download the Loaner Details to excel file
function fnExcelExport() {
	gObj.toExcel('/phpapp/excel/generate.php');
}