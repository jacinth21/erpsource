// this function is used to show Donut chart
// which is calling from two places : GmSalesHome.jsp , GmDailySalesReport.jsp
function fnDoughnut(){
	//When the data is empty don't render the chart.
	//Render the chart only when the data array is not null & total sales is in Positive value 
    if(doughnutDataArray !='' && doughnutTotalSales > 0){
	//Chart Type declaration
	var doughnutObj = {
		    type: 'doughnut2d',
		    renderAt: 'divDonutContainer',
		    width: width,
		    height: '450',
		    dataFormat: 'json'
		};
		
	//Chart parameters declaration
	var doughnutchart={
              "caption": caption,
              "captionFontColor":"#000000",
              "captionFont":"verdana, arial, sans-serif",
              //"subCaption": "Last year",
              "numberPrefix": currSymbol,
              //"paletteColors": "#0075c2,#1aaf5d,#f2c500,#f45b00,#8e0000",
              "bgColor": "#eeeeee",
              "showBorder": "0",
              "use3DLighting": "0",
              "showShadow": "0",
              "enableSmartLabels": "0",
              "labelFontColor": "#000000", 
              "valueFontColor": "#000000",//baseFontColor 
              "startingAngle": "90",
              "showLabels": "0",
  	          "showPercentValues":"1",
  	          "showPercentInToolTip":"1",
              "showLegend": "0",              
              "defaultCenterLabel": message_sales[267]+formatCurrency(total,0),
              "labelDisplay": "none",
              
              "manageLabelOverflow":"1",
              "useEllipsesWhenOverflow":"1",
            	  
              "toolTipColor":"#000000",
              "toolTipBorderThickness":"0",
              "toolTipBgColor":"#000000",
              "toolTipBgAlpha":"80",
              "toolTipBorderRadius":"2",
              "toolTipPadding":"5",

      		  "showHoverEffect": "1",
              "formatNumberScale": "0",
              "decimals": "0",
              "forceDecimals": "1",             
              "legendShadow": "0",
              "legendBorderAlpha": "0",
              "legendPosition":"RIGHT",
              "centerLabel": "$value <br> $percentValue <br> $label",
              "centerLabelBold": "1",

              "showTooltip": "0",
              
              "chartLeftMargin":"0",
              "chartRightMargin":"0",
              "chartTopMargin":"5",
              "chartBottomMargin":"5",
              

              "captionFontSize": "14",
              "subcaptionFontSize": "14",
              "subcaptionFontBold": "0",
  			  "exportenabled" : "1",
  			  "exportatclient" : "0",
  			  "animation":"0",
              //"showLabels": "0", //hide the labels
 			  "showValues": "0", //hide the values
  			  "exporthandler" : fcExportServer,
  			  "html5exporthandler" : fcExportServer
          };

	var doughnutDataSrc={};
	doughnutDataSrc.chart=doughnutchart;
	
	//Assigning Chart data
	doughnutDataSrc.data=doughnutDataArray.reverse();//To display the doughnut pieces in clockwise direction, data array is reversed as there is no default option. 
	doughnutObj.dataSource=doughnutDataSrc;
	
	//Fusion chart rendering.
    var doughnutChart = new FusionCharts(doughnutObj);
    doughnutChart.render();
    }else{
    	var divDChartContainer = document.getElementById("divDonutContainer");
    	if(divDChartContainer){
    		divDChartContainer.style.width='480';
    		divDChartContainer.style.valign='center';
    		divDChartContainer.innerHTML =  "<font style='font-family: verdana, arial, sans-serif;  font-size: 11px; color:#FF0000; text-decoration: None;'>"+message_sales[27]+"</font>";
    	}
    }
}