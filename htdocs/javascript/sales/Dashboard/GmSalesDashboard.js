function doOnLoad() 
	{
		var tab_title = message_sales[259];
	   	var strdaySales=document.frmGmSalesDash.strDaySales.value;
		var strMonthSales=document.frmGmSalesDash.strMonthSales.value;
		// based on the account item report show flag , change the Pop up name
		if(strAccItemConRptFl == 'Y'){
			tab_title = message_sales[429];
		}
		
		//Create Layout
		dhxLayout_1 = new dhtmlXLayoutObject("salesDash_1", "4W");
		dhxLayout_2 = new dhtmlXLayoutObject("salesDash_2", "3W");
		dhxLayout_3 = new dhtmlXLayoutObject("salesDash_3", "2U");

		//Set the title
		dhxLayout_1.cells("a").setText("<a onclick='fnReloadCell(\"Loaners\")' 		style=\"cursor:hand;\"><img src=\"/images/icon_btn_refresh.png\" title=\"Refresh\"></a> <b>&nbsp;"+message_sales[254]+"</b>");
		dhxLayout_1.cells("b").setText("<a onclick='fnReloadCell(\"Orders\")'  		style=\"cursor:hand;\"><img src=\"/images/icon_btn_refresh.png\" title=\"Refresh\"></a> <b>&nbsp;"+message_sales[255]+"</b>");
		dhxLayout_1.cells("c").setText("<a onclick='fnReloadCell(\"Returns\")' 		style=\"cursor:hand;\"><img src=\"/images/icon_btn_refresh.png\" title=\"Refresh\"></a> <b>&nbsp;"+message_sales[256]+"</b>");
		dhxLayout_1.cells("d").setText("<a onclick='fnReloadCell(\"CasesSched\")'	style=\"cursor:hand;\"><img src=\"/images/icon_btn_refresh.png\" title=\"Refresh\"></a> <b>&nbsp;"+message_sales[257]+"</b>");

		dhxLayout_2.cells("a").setText("<a onclick='fnReloadCell(\"CNSets\")'  		style=\"cursor:hand;\"><img src=\"/images/icon_btn_refresh.png\" title=\"Refresh\"></a> <b>&nbsp;"+message_sales[258]+"</b>");
		dhxLayout_2.cells("b").setText("<a onclick='fnReloadCell(\"CNItems\")' 		style=\"cursor:hand;\"><img src=\"/images/icon_btn_refresh.png\" title=\"Refresh\"></a> <b>&nbsp;"+tab_title+"</b>");
		dhxLayout_2.cells("c").setText("<a onclick='fnReloadCell(\"Quota_Growth\")' style=\"cursor:hand;\"><img src=\"/images/icon_btn_refresh.png\" title=\"Refresh\"></a> <b>&nbsp;"+message_sales[260]+"</b>");

		dhxLayout_3.cells("a").setText("<a onclick='fnReloadCell(\"DTDSale\")' 		style=\"cursor:hand;\"><img src=\"/images/icon_btn_refresh.png\" title=\"Refresh\"></a> <b>&nbsp;"+message_sales[261]+" </b>  <span id=\"divDaysales\">"+strdaySales+"</span>&nbsp;&nbsp;<span>"+userType.innerHTML+"</span>");
		dhxLayout_3.cells("b").setText("<a onclick='fnReloadCell(\"MTDSale\")' 		style=\"cursor:hand;\"><img src=\"/images/icon_btn_refresh.png\" title=\"Refresh\"></a> <b>&nbsp;"+message_sales[262]+" </b>  <span id=\"divMonthsales\">"+strMonthSales+"</span>");

		
		//Attach URL
		dhxLayout_1.cells("a").attachURL("/ajaxcalls/sales/Dashboard/GmSalesDatagrid.html?objCell=Loaners&ramdomId="+Math.random());
		dhxLayout_1.cells("b").attachURL("/ajaxcalls/sales/Dashboard/GmSalesDatagrid.html?objCell=Orders&ramdomId="+Math.random());
		dhxLayout_1.cells("c").attachURL("/ajaxcalls/sales/Dashboard/GmSalesDatagrid.html?objCell=Returns&ramdomId="+Math.random());
        dhxLayout_1.cells("d").attachURL("/ajaxcalls/sales/Dashboard/GmSalesDatagrid.html?objCell=CasesSched&ramdomId="+Math.random());

		dhxLayout_2.cells("a").attachURL("/ajaxcalls/sales/Dashboard/GmSalesDatagrid.html?objCell=CNSets&ramdomId="+Math.random());
		dhxLayout_2.cells("b").attachURL("/ajaxcalls/sales/Dashboard/GmSalesDatagrid.html?objCell=CNItems&ramdomId="+Math.random());
		dhxLayout_2.cells("c").attachURL("/ajaxcalls/sales/Dashboard/GmSalesDatagrid.html?objCell=Quota_Growth&ramdomId="+Math.random());
		
		dhxLayout_3.cells("a").attachURL("/ajaxcalls/sales/Dashboard/GmSalesDatagrid.html?objCell=DTDSale&ramdomId="+Math.random()+"&todaySalesType="+todaySalesSltVal);
		dhxLayout_3.cells("b").attachURL("/ajaxcalls/sales/Dashboard/GmSalesDatagrid.html?objCell=MTDSale&ramdomId="+Math.random());
	
	}	

function fnFetchTodaySales(val){
	todaySalesSltVal=val;
	document.frmGmSalesDash.todaySalesType.value = todaySalesSltVal;
	dhxLayout_3.cells("a").attachURL("/ajaxcalls/sales/Dashboard/GmSalesDatagrid.html?objCell=DTDSale&ramdomId="+Math.random()+"&todaySalesType="+val);
}
	function initWindow(url,popupName)
	{
   	 	 if(dhxWins!=undefined && dhxWins.isWindow("popupWindow")){ 
   	 		dhxWins.window("popupWindow").close();
   	 	 }
		 dhxWins = new dhtmlXWindows();
		 dhxWins.enableAutoViewport(false);
	     dhxWins.attachViewportTo(document.body);
	     dhxWins.setImagePath("extweb/dhtmlx/dhtmlxWindows/imgs");
	     //open popup window	  
	     if (!dhxWins.window("newcontact_win"))
	     {
	    	 var popupX = 100;
	    	 var popupY = 100;
	    	 //PC-2365 Add Excel Export Option in Missing Parts Loaner Details - Sales Dashboard
	    	 var popupH = 600;
	    	 var popupW = 1030;
	    	 if (GLOBAL_ClientSysType == "IPAD"){
		    	 popupX = 30;
		    	 popupY = 40;
		    	 popupH = 480;
		    	 popupW = 960;
		     }
	    	 if(url.indexOf("GmDetailedSalesServlet")!=-1){
	    		 popupW = 730;
	    	 }
	    	 popupWindow = dhxWins.createWindow("popupWindow", popupX, popupY, popupW, popupH);
		     popupWindow.setText(popupName);
		     popupWindow.attachURL(url+'&ramdomId='+Math.random());
		     popupWindow.setModal(true);
	     }
 	}
	function fnReloadCell(objCellTitle)
	{
	
		//If you find some better way for this pls change it.
		//objLayout.cells(objCell).attachURL("GmSalesDatagrid.html?objCell="+objCell);
		if (objCellTitle == "Loaners")
		{
			//dhxLayout_1.cells("a").attachURL("http://docs.dhtmlx.com/doku.php");
			dhxLayout_1.cells("a").attachURL("/ajaxcalls/sales/Dashboard/GmSalesDatagrid.html?objCell=Loaners&popupName=LoanerDetails&ramdomId="+Math.random());
		}
		if (objCellTitle == "Orders")
		{
			//dhxLayout_1.cells("a").attachURL("http://docs.dhtmlx.com/doku.php");
			dhxLayout_1.cells("b").attachURL("/ajaxcalls/sales/Dashboard/GmSalesDatagrid.html?objCell=Orders&popupName=Order Details&ramdomId="+Math.random());
		}		
		if (objCellTitle == "Returns")
		{
			dhxLayout_1.cells("c").attachURL("/ajaxcalls/sales/Dashboard/GmSalesDatagrid.html?objCell=Returns&popupName=Return Details&ramdomId="+Math.random());
		}
		if (objCellTitle == "CasesSched")
		{
			dhxLayout_1.cells("d").attachURL("/ajaxcalls/sales/Dashboard/GmSalesDatagrid.html?objCell=CasesSched&popupName=Cases Scheduled Details&ramdomId="+Math.random());
		}
		if (objCellTitle == "CNSets")
		{
			dhxLayout_2.cells("a").attachURL("/ajaxcalls/sales/Dashboard/GmSalesDatagrid.html?objCell=CNSets&popupName=Consignment Set Details&ramdomId="+Math.random());
		}		
		if (objCellTitle == "CNItems")
		{
			dhxLayout_2.cells("b").attachURL("/ajaxcalls/sales/Dashboard/GmSalesDatagrid.html?objCell=CNItems&popupName=Consignment Items&ramdomId="+Math.random());
		}
		if (objCellTitle == "Quota_Growth")
		{
			dhxLayout_2.cells("c").attachURL("/ajaxcalls/sales/Dashboard/GmSalesDatagrid.html?objCell=Quota_Growth&popupName=Quota Growth&ramdomId="+Math.random());
		}
		if (objCellTitle == "DTDSale")
		{
			if (todaySalesSltVal == undefined)
			{
				todaySalesSltVal = '';
			}
			dhxLayout_3.cells("a").attachURL("/ajaxcalls/sales/Dashboard/GmSalesDatagrid.html?objCell=DTDSale&ramdomId="+Math.random()+"&todaySalesType="+todaySalesSltVal);
			//ajax call for refresh header sales amount
			dhtmlxAjax.get('/gmSalesDashLoaner.do?&method=loadSalesDashBoard&strOpt=AjaxDaySales&ramdomId='+Math.random(),function(loader){
				if (loader.xmlDoc.responseText != null)
				{
					document.getElementById("divDaysales").innerHTML = loader.xmlDoc.responseText;
					
				}
				});
		}
		if (objCellTitle == "MTDSale")
		{
			dhxLayout_3.cells("b").attachURL("/ajaxcalls/sales/Dashboard/GmSalesDatagrid.html?objCell=MTDSale&ramdomId="+Math.random());
			//ajax call for refresh header sales amount
			dhtmlxAjax.get('/gmSalesDashLoaner.do?&method=loadSalesDashBoard&strOpt=AjaxMonthSales&ramdomId='+Math.random(),function(loader){
				if (loader.xmlDoc.responseText != null)
				{
					document.getElementById("divMonthsales").innerHTML = loader.xmlDoc.responseText;
					
				}
				});
		}		
		
	}
	
function fnSelectedTab(cell,tab){
	if(cell == "MTDSale"){
		document.frmGmSalesDash.monthSalesTab.value = tab;
	}else{
		document.frmGmSalesDash.todaySalesTab.value = tab;
	}
}
function getSelectedTab(cell){
	var monthSalesTab = document.frmGmSalesDash.monthSalesTab.value;
	var todaySalesTab =document.frmGmSalesDash.todaySalesTab.value;
	var selectedTab = (cell == "MTDSale")?monthSalesTab:todaySalesTab;
	return (selectedTab == "")?"dataSet":selectedTab;
}
