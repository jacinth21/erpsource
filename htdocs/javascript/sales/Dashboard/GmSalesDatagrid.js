var chartConfigPie =({
	  view: "pie3D",
	  radius: 110,
      value: function(data){
		  var per = parseFloat(data.percent);
		  if(per < 0){
			  return (per * -1);
		  }else{
			  return per;
		  }
      },
      pieInnerText: "",
      color: "#color#",
      tooltip:{
    	  template:function(data){
	    		  var per = parseFloat(data.percent);
	    		  if(per < 0){
	    			  return "<span style='color:red'>("+(per * -1)+" %)</span>";
	    		  }else{
	    			  return per+" %";
	    		  }
    	  		}
      		},     
      legend:{ align:"right",
    	  	   valign: "top", 
    	  	   marker:{type: "square", width:10},
    	  	   template:function(data) {
    	  		    var name = data.DName;
    	  		    if(name.length>18){
    	  		    	name = name.substring(0,18)+"...";
    	  		    }
    				return "<font size=1>"+name+"</font>";
    	  	   	}
			  },
      padding:{
			left:0
		}});

var chartConfigBar =({
      view: "bar",
      value: "#sales#",
      gradient: true,
      color:"#FF8200",
      width: 15,
      tooltip:{
      		template: function(data) {
	    		var val = parseFloat(data.sales);
	    		var currSign = data.currsign;
	    		if(val < 0){
	    			return "<span style='color:red;font-size=10;'> "+currSign+" ("+formatCurrency(val)+")</span>";
	    		}else{
	    			return "<span style='font-size=10;'>"+currSign+" "+formatCurrency(val)+"</span>";
	    		}
          		
      		},
            dx:-30,
            dy:-20
      }, 
      origin:0,
      	xAxis:{
			title:"Sales by date",
			template:function(data) {
          		return "<font size=1>"+data.day+"</font>";
      		}
		},
		yAxis:{
            template: function(obj) {
	         return "<font size=1>"+formatCurrency(obj,true)+"</font>";
	      }, 
			title:""
	},
    padding:{
		left:70
	}
      });
      
function formatCurrency(num,inclsign) {
	num = num.toString().replace(/\$|\,/g,'');
	if(isNaN(num))
		num = "0";
	sign = (num == (num = Math.abs(num)));
	num = Math.floor(num*100+0.50000000001);
	cents = num%100;
	num = Math.floor(num/100).toString();
	if(cents<10)
		cents = "0" + cents;
	for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
		num = num.substring(0,num.length-(4*i+3))+','+num.substring(num.length-(4*i+3));
	return (((inclsign)?((sign)?'':'-'):'') + ((inclsign)?'$ ':'') + num );
	}
	
function getParams() {
	var idx = document.URL.indexOf('?');
	if (idx != -1) 
	{
		var tempParams = new Object();
		var pairs = document.URL.substring(idx+1,document.URL.length).split('&');
		for (var i=0; i<pairs.length; i++) 
		{
			nameVal = pairs[i].split('=');
			tempParams[nameVal[0]] = nameVal[1];			
		}
		return tempParams;
	}
}

function fnDoOnLoad()
{			
			if(objCell == 'DTDSale' || objCell == 'MTDSale'){
				fnDoOnLoadChart();
			}else{
				document.getElementById("salesDash_").style.width= (objCell == 'Quota_Growth' || objCell == 'CNSets' || objCell == 'CNItems')?"310px":"230px";
				dhxLayout_ = new dhtmlXLayoutObject("salesDash_", "1C");
				fnLoadLayout(dhxLayout_,"a",objCell);
			}
}

function fnDoOnLoadChart(){
	document.getElementById("salesDash_").style.width= (objCell == 'DTDSale')?"469px":"468px"; 	
	document.getElementById("salesDash_").style.height= "290px";
	
	tabbar = new dhtmlXTabBar("salesDash_", "bottom");
	tabbar.setSkin('dhx_skyblue');
	tabbar.setImagePath("/javascript/dhtmlx/dhtmlxTabbar/imgs/");
	tabbar.addTab("dataSet", "Data Set", "100px");
	tabbar.addTab("chart", "Chart", "100px");
	
	var layoutDataSet = tabbar.cells("dataSet").attachLayout("1C","dhx_skyblue");
	var layoutChart = tabbar.cells("chart").attachLayout("1C","dhx_skyblue");
	
    //on select fuction for tab selection
    tabbar.attachEvent("onSelect", function(id,last_id){
	    if(id=='dataSet'){
	    	if(objCell == 'MTDSale'){//Attach average status bar for monthly sales
	    		layoutDataSet.cells("a").detachStatusBar();
	    		statusBar = layoutDataSet.cells("a").attachStatusBar();
			}
				fnLoadLayout(layoutDataSet,"a",objCell+"Grid");
			
		}else{//chart
			if(objCell == 'MTDSale'){
				layoutChart.cells("a").detachStatusBar();
	    		statusBar =  layoutChart.cells("a").attachStatusBar();
			}
			fnLoadLayout(layoutChart,"a",objCell);
		}
	    parent.fnSelectedTab(objCell,id);//store the selected tab to reload again
	    return true;
    });
    
	tabbar.setTabActive(parent.getSelectedTab(objCell));//active tab which was selected early	
}

function fnLoadLayout(objLayout,objLayoutCell, objCell)
	{
		objLayout.cells(objLayoutCell).hideHeader();
		objLayout.cells(objLayoutCell).progressOn();
		if (objCell == 'DTDSale')
		{		
				var dhxPieChart = objLayout.cells(objLayoutCell).attachChart(chartConfigPie);						
				fnAjaxChardData(dhxPieChart,objCell,objLayout,objLayoutCell);
				
		}
		else if (objCell == 'MTDSale')
		{
	
				var dhxBarChart = objLayout.cells(objLayoutCell).attachChart(chartConfigBar);
				fnAjaxChardData(dhxBarChart,objCell,objLayout,objLayoutCell);
	
		}
		else
		{ 

			dhxGrid = objLayout.cells(objLayoutCell).attachGrid();
			dhxGrid.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
			fnAjaxGridData(dhxGrid, objCell,objLayout,objLayoutCell);
		}
		
	}
	
function fnAjaxChardData(dhxChart, objCell,objLayout,objLayoutCell)
{	
	if (objCell == 'DTDSale')
	{	
		    
			var atchURL =  fnAjaxURLAppendCmpInfo('/gmSalesDashBoard.do?method=todaySales&ramdomId='+Math.random()+'&todaySalesType='+((params["todaySalesType"]!= undefined) ? params["todaySalesType"]:""));
			dhtmlxAjax.get(atchURL,function(loader){
			if (loader.xmlDoc.responseText != null)
			{
				var dataChartXML = loader.xmlDoc.responseText;
				dhxChart.parse(dataChartXML,"xml");
				objLayout.cells(objLayoutCell).progressOff();	
				if (dataChartXML.length < 14)
				{
					document.getElementById("salesDash_").innerHTML = '<span style=\"color:red;font-weight:12px;\"><BR><BR><BR>&nbsp;&nbsp;<b>'+fnLayoutName()+ '</b> details are not available.</span>';
				}				
			}
			});
	}	
	if (objCell == 'MTDSale')
	{	
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmSalesDashBoard.do?method=monthlySales&ramdomId='+Math.random());
		var loader = dhtmlxAjax.get(ajaxUrl,function(loader){
			if (loader.xmlDoc.responseText != null)
			{
				var dataChartXML = loader.xmlDoc.responseText;
				var dataArry = dataChartXML.split("|");
				// To show currency symbol for the selected company in y axis
				chartConfigBar.yAxis.template = function(obj) {
			         return "<font size=1>"+dataArry[2]+" "+formatCurrency(obj)+"</font>";
				}
				dhxChart= objLayout.cells(objLayoutCell).attachChart(chartConfigBar);
				dhxChart.parse(dataArry[0],"xml");
				statusBar.setText("<b>Average Daily Sales "+dataArry[1]+"</b>");
				
				objLayout.cells(objLayoutCell).progressOff();
				if (dataChartXML.length < 14)
				{
					document.getElementById("salesDash_").innerHTML = '<span style=\"color:red;font-weight:12px;\"><BR><BR><BR>&nbsp;&nbsp;<b>'+fnLayoutName()+ '</b> details are not available.</span>';
				}	
			}
			
			});
	}			
}				


function fnAjaxGridData(ojbMygrid, objCell, objLayout,objLayoutCell)
{
	if (objCell == 'Loaners')
	{
		
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmSalesDashLoaner.do?&method=loanersCount&ramdomId='+Math.random());
		dhtmlxAjax.get(ajaxUrl,function(loader){
			if (loader.xmlDoc.responseText != null)
			{
				var datagridXML = loader.xmlDoc.responseText;
				ojbMygrid.loadXMLString(datagridXML);	
				objLayout.cells(objLayoutCell).progressOff();	
			}
			});
		
	}
	if (objCell == 'Orders')
	{
	   var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmSalesDashOrders.do?&method=ordersCount&ramdomId='+Math.random());
	   dhtmlxAjax.get(ajaxUrl,function(loader){
			if (loader.xmlDoc.responseText != null)
			{
				var datagridXML = loader.xmlDoc.responseText;
				ojbMygrid.loadXMLString(datagridXML);	
				objLayout.cells(objLayoutCell).progressOff();	
			}
			});
	}
	if (objCell == 'Quota_Growth')
	{
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmSalesDashQuota.do?method=QuotaPerformance&reportType=Historical&ramdomId='+Math.random());//PMT-33183 Quota Performance
		dhtmlxAjax.get(ajaxUrl,function(loader){
		if (loader.xmlDoc.responseText != null)
		{
			var datagridXML = loader.xmlDoc.responseText;
			ojbMygrid.loadXMLString(datagridXML);	
			objLayout.cells(objLayoutCell).progressOff();	
		}
		});		 		   
	}
	if (objCell == 'CNSets')
	{
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmSalesDashCongSets.do?&method=congSetsCount&ramdomId='+Math.random());
	    dhtmlxAjax.get(ajaxUrl,function(loader){
			if (loader.xmlDoc.responseText != null)
			{
				var datagridXML = loader.xmlDoc.responseText;
				ojbMygrid.loadXMLString(datagridXML);	
				objLayout.cells(objLayoutCell).progressOff();	
			}
			});				 
	}
	if (objCell == 'CNItems')
	{
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmSalesDashCongItems.do?&method=congItemsCount&ramdomId='+Math.random());
	    dhtmlxAjax.get(ajaxUrl,function(loader){
			if (loader.xmlDoc.responseText != null)
			{
				var datagridXML = loader.xmlDoc.responseText;
				ojbMygrid.loadXMLString(datagridXML);	
				objLayout.cells(objLayoutCell).progressOff();	
			}
			});			 
	}
	if (objCell == 'Returns')
	{
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmSalesDashReturns.do?&method=returnCount&ramdomId='+Math.random());
	    dhtmlxAjax.get(ajaxUrl,function(loader){
			if (loader.xmlDoc.responseText != null)
			{
				var datagridXML = loader.xmlDoc.responseText;
				ojbMygrid.loadXMLString(datagridXML);	
				objLayout.cells(objLayoutCell).progressOff();	
			}
			});
	}
	
	if (objCell == 'CasesSched')
	{
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmSalesDashCaseSchd.do?&method=casesScheduledCount&ramdomId='+Math.random());
	    dhtmlxAjax.get(ajaxUrl,function(loader){
			if (loader.xmlDoc.responseText != null)
			{
				var datagridXML = loader.xmlDoc.responseText;
				ojbMygrid.loadXMLString(datagridXML);	
				objLayout.cells(objLayoutCell).progressOff();	
			}
			});
	}

	if (objCell == 'MTDSaleGrid')
	{
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmSalesDashBoard.do?method=monthlySales&strOpt=dataset&ramdomId='+Math.random());
	    dhtmlxAjax.get(ajaxUrl,function(loader){
			if (loader.xmlDoc.responseText != null)
			{
				var datagridXML = loader.xmlDoc.responseText;
				var dataArry = datagridXML.split("|");
				statusBar.setText("<b>Average Daily Sales "+dataArry[1]+"</b>");
				
				if(datagridXML.indexOf("cell")==-1){
					document.getElementById("salesDash_").innerHTML = '<span style=\"color:red;font-weight:12px;\"><BR><BR><BR>&nbsp;&nbsp;<b>'+fnLayoutName()+ '</b> details are not available.</span>';
				}else{ 
					ojbMygrid.loadXMLString(dataArry[0]);
				}
				objLayout.cells(objLayoutCell).progressOff();	
			}
			}); 
	}
	if (objCell == 'DTDSaleGrid')
	{
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmSalesDashBoard.do?method=todaySales&strOpt=dataset&ramdomId='+Math.random()+'&todaySalesType='+((params["todaySalesType"]!= undefined) ? params["todaySalesType"]:""));
 	    dhtmlxAjax.get(ajaxUrl,function(loader){
			if (loader.xmlDoc.responseText != null)
			{
				var datagridXML = loader.xmlDoc.responseText;
				if(datagridXML.indexOf("cell")==-1){
					document.getElementById("salesDash_").innerHTML = '<span style=\"color:red;font-weight:12px;\"><BR><BR><BR>&nbsp;&nbsp;<b>'+fnLayoutName()+ '</b> details are not available.</span>';
				}else{ 
					ojbMygrid.loadXMLString(datagridXML);
				}
				objLayout.cells(objLayoutCell).progressOff();	
			}
			}); 
	}
}
function fnOpenPopup(url){
	var dhxCmpUrl = fnAjaxURLAppendCmpInfo(url);
	var popupName = fnLayoutName() ;
	popupName = popupName + ' Detail';
	parent.initWindow(dhxCmpUrl,popupName);
}



function fnLayoutName(){
	var layoutName;

		 if (objCell == 'Loaners')		{  layoutName = 'Loaners';	}
	else if (objCell == 'Orders')		{  layoutName = 'Orders';	}
	else if (objCell == 'CNSets')		{  layoutName = 'Consignment Sets';	}
	else if (objCell == 'CNItems')		{  layoutName = 'Consignment Items';	}
	else if (objCell == 'Quota_Growth')	{  layoutName = 'Quota/Growth';	}
	else if (objCell == 'Returns')		{  layoutName = 'Returns';	}
	else if (objCell == 'CasesSched')	{  layoutName = 'Cases Scheduled';	}
	else if (objCell == 'DTDSale')		{  layoutName = 'Today Sales';	}
	else if (objCell == 'MTDSale')		{  layoutName = 'Monthly Sales';	}
	return layoutName;
}

function fnBookNewCase() {
	parent.location.href = "/gmCaseBookSetup.do?"+ fnAppendCompanyInfo();
}
