var consinid = '';
var reqdtl = '';
var check_rowId= '';
function initGridData(divRef,gridData){
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(false);
	gObj.enableTooltips("false,false,true,true,true,true,true,true,true,true,true,true,true");
	gObj.init();		
	gObj.checkAll(false);
	gObj.loadXMLString(gridData);
	return gObj;	
}	
function fnOnPageLoad() {
	if (objGridData != '') {
		gridObj = initGridData('dataGridDiv',objGridData);
	}
	var gridrows = gridObj.getAllRowIds(",");
	
	day_rowId = gridObj.getColIndexById("dayextend");	
	check_rowId = gridObj.getColIndexById("wholeextn");
	
	
	gridObj.attachEvent("onEditCell", doOnCellEdit);
	//gridObj.attachEvent("onCheckbox", doOnCheck);
	needFl_colid = gridObj.getColIndexById("needFl");
	gridObj.forEachRow(function(rowId) {
		day_extn = gridObj.cellById(rowId, day_rowId).getValue();
		if(day_extn == ''){
			gridObj.cellById(rowId, day_rowId).setValue("0");
		}
		request_rowId = gridObj.getColIndexById("requestid");
		reqdtl_rowid = gridObj.getColIndexById("detailid");
		check_rowId = gridObj.getColIndexById("wholeextn");
		new_date  = gridObj.getColIndexById("newexprtndate");
		day_ext_rowid = gridObj.getColIndexById("dayextend");
		consign_rowid = gridObj.getColIndexById("consignid");
		surgdt_rowid = gridObj.getColIndexById("surgdt");
		loandt_rowid = gridObj.getColIndexById("loandt");
		expecteddate_rowid = gridObj.getColIndexById("exprtndate");

		extnCnt = gridObj.cellById(rowId, needFl_colid).getValue();
		if(extnCnt >0){
			gridObj.cellById(rowId, check_rowId).setDisabled(true);
		}
	});
}

function doOnCellEdit(stage, rowId, cellInd, nValue, oValue) {
	rowID = rowId;
	reqDtlID = gridObj.cellById(rowId, reqdtl_rowid).getValue();	
	checkVal1 = gridObj.cellById(rowID, surgdt_rowid).getValue();
	if (cellInd == day_rowId) {
		if (stage == 2 && nValue != '0' && nValue != oValue) {
			fnSetNewExtDate(nValue,reqDtlID,rowID);
		}
		if (nValue == '0'){
			gridObj.cellById(rowId, new_date).setValue("");
		}		
		checkVal = gridObj.cellById(rowID, check_rowId).getValue();
		if(checkVal ==1){
		    fnAutoSelectChildExtn(rowID, checkVal);
		}
	}else{
		if(cellInd == '4' && day_rowId == '5'){
			if (stage == 2 && nValue != '0' && nValue != oValue) {
				day_count = gridObj.cellById(rowId, day_ext_rowid).getValue();
				fnSetNewExtDate(day_count,reqDtlID,rowID);
			}
		}	
	}
	if (cellInd == check_rowId && stage == 1 ) {
		checkVal = gridObj.cellById(rowID, check_rowId).getValue();		
		fnAutoSelectChildExtn(rowID, checkVal);
	}
	return true;
}

function fnAutoSelectChildExtn(rowID, checkVal){
		
	var parentID = gridObj.cellById(rowID, request_rowId).getValue();
	var day_count = gridObj.cellById(rowID, day_ext_rowid).getValue();
	var newDate = gridObj.cellById(rowID, new_date).getValue();
	var parentWEXTNFL = gridObj.cellById(rowID, check_rowId).getValue();
	if(checkVal == 1){			
		gridObj.forEachRow(function(varrowId){
			request_Id = gridObj.cellById(varrowId, request_rowId).getValue();
			if(parentID == request_Id && rowID != varrowId ){
				gridObj.cellById(varrowId, day_ext_rowid).setValue(day_count);
				gridObj.cellById(varrowId, day_ext_rowid).setDisabled(true);
				gridObj.cellById(varrowId, new_date).setValue(newDate);
				gridObj.cellById(varrowId, check_rowId).setDisabled(true);					
			}
		});
	  }else{
		  gridObj.forEachRow(function(varrowId){
			request_Id = gridObj.cellById(varrowId, request_rowId).getValue();
			if(parentID == request_Id && rowID != varrowId ){
				gridObj.cellById(varrowId, day_ext_rowid).setValue("0");
				gridObj.cellById(varrowId, day_ext_rowid).setDisabled(false);
				gridObj.cellById(varrowId, new_date).setValue("");
				gridObj.cellById(varrowId, check_rowId).setDisabled(false);					
			}
		 });				
	  }
}

function fnSetNewExtDate(day_extn,reqDtlID,rowID){
	var surgdt = gridObj.cellById(rowID, 4).getValue();
	var newExtnDate=fnAddTwoWorkingDays(surgdt,day_extn);
	gridObj.cellById(rowID, new_date).setValue(newExtnDate);
	
	/*var dayalt = fnGetCodeAlt(day_extn);
	for (var i=0;i<newExtnDateSize;i++){
		arr = eval("newExtnDate"+i);
		arrobj = arr.split(",");
		reqdtlid = arrobj[0];
		dayExtnId = arrobj[1];
		newExtnDate = arrobj[2];
		reqdtl = reqdtlid;
		if(reqDtlID == reqdtlid && dayExtnId == dayalt){
			gridObj.cellById(rowID, new_date).setValue(newExtnDate);
		}
	}*/
}


//this method get the date format in MM/dd/yyyy
function getDateFormatMMDDYYYY(today){
	 
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!

    var yyyy = today.getFullYear();
    if (dd < 10) {
      dd = '0' + dd;
    } 
    if (mm < 10) {
      mm = '0' + mm;
    } 
 return   today = mm + '/' + dd + '/' + yyyy;
}
//this method get the date format in dd/MM/yyyy
function getDateFormatDDMMYYYY(today){
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!

    var yyyy = today.getFullYear();
    if (dd < 10) {
      dd = '0' + dd;
    } 
    if (mm < 10) {
      mm = '0' + mm;
    } 
 return   today = dd + '/' + mm + '/' + yyyy;
}
//this method get the date format in dd.MM.yyyy
function getDotDateFmtDDMMYYYY(today){
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!

    var yyyy = today.getFullYear();
    if (dd < 10) {
      dd = '0' + dd;
    } 
    if (mm < 10) {
      mm = '0' + mm;
    } 
 return   today = dd + '.' + mm + '.' + yyyy;
}

function fnAddTwoWorkingDays(dateObj,day_extn){
    var dateParts='';
    var surgerydt = dateObj;
     date      = new Date(surgerydt);
     var today = new Date();
     day       = date.getDay();
     if(dateFormat == 'MM/dd/yyyy'){
        Sur_date = getDateFormatMMDDYYYY(date);   
        Cur_date = getDateFormatMMDDYYYY(today);
        Current_date = new Date();
        next_dt = fnAddDate(Cur_date,Sur_date,day,day_extn);//this function for adding next two working day
        formatted = padNumber(next_dt.getMonth() +1) + '/' + padNumber(next_dt.getDate()) + '/' + padNumber(next_dt.getFullYear())
       
    }else if(dateFormat == 'dd/MM/yyyy'){
         dateParts = surgerydt.split("/");
         date = new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0]);
         day    = date.getDay();
         Sur_date = getDateFormatDDMMYYYY(date);   
         Cur_date = getDateFormatDDMMYYYY(today);
         dateCur_date = Cur_date.split("/");
         Current_date = new Date(+dateCur_date[2], dateCur_date[1] - 1, +dateCur_date[0]);
         next_dt = fnAddDate(Cur_date,Sur_date,day,day_extn);//this function for adding next two working day
         formatted = padNumber(next_dt.getDate()) + '/' + padNumber(next_dt.getMonth() + 1) + '/' + padNumber(next_dt.getFullYear())
     }else if(dateFormat == 'dd.MM.yyyy'){
         dateParts = surgerydt.split(".");
         date = new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0]);
         Sur_date = getDotDateFmtDDMMYYYY(date);   
         Cur_date = getDotDateFmtDDMMYYYY(today);
         dateCur_date = Cur_date.split(".");
         Current_date = new Date(+dateCur_date[2], dateCur_date[1] - 1, +dateCur_date[0]);
         next_dt = fnAddDate(Cur_date,Sur_date,day,day_extn); //this function for adding next two working day
         formatted = padNumber(next_dt.getDate()) + '.' + padNumber(next_dt.getMonth() + 1) + '.' + padNumber(next_dt.getFullYear())
     }
     return formatted;
}
function padNumber(number) {
    var string  = '' + number;
    string      = string.length < 2 ? '0' + string : string;
    return string;
}
//this function for adding next two working day
function fnAddDate(Cur_date,Sur_date,day,day_extn){
     var today = new Date();
     var curent_day = today.getDay();
     var next_date;
	 if (day_extn == 1931 && day != 5 && day != 6) {
		next_date = new Date(date.setDate(date.getDate() + 1));
	} else if ((day_extn == 1931 && day == 5) || (day_extn == 1933 && day == 1)|| (day_extn == 1933 && day == 2) || (day_extn == 1932 && day == 6)
			|| (day_extn == 1933 && day == 0)) {
		next_date = new Date(date.setDate(date.getDate() + 3));
	} else if ((day_extn == 1932 && day == 4) || (day_extn == 1932 && day == 5)|| (day_extn == 1934 && day == 1) || (day_extn == 1934 && day == 0)
			|| (day_extn == 1933 && day == 6)) {
		next_date = new Date(date.setDate(date.getDate() + 4));
	} else if ((day_extn == 1932 && day == 1) || (day_extn == 1932 && day == 2)|| (day_extn == 1932 && day == 3) || (day_extn == 1931 && day == 6)
			|| (day_extn == 1932 && day == 0)) {
		next_date = new Date(date.setDate(date.getDate() + 2));
	} else if ((day_extn == 1933 && day == 3) || (day_extn == 1933 && day == 4)|| (day_extn == 1933 && day == 5) || (day_extn == 1934 && day == 6)
			|| (day_extn == 1935 && day == 0)) {
		next_date = new Date(date.setDate(date.getDate() + 5));
	} else if ((day_extn == 1934 && day != 1 && day != 6)|| (day_extn == 1935 && day == 6)) {
		next_date = new Date(date.setDate(date.getDate() + 6));
	} else if ((day_extn == 1935)&& (day == 1 || day == 2 || day == 3 || day == 4 || day == 5)) {
		next_date = new Date(date.setDate(date.getDate() + 7));
	}
return next_date;
}

function fnExtend(){
	var err_newExpDate = '';
	var err_expRtnDate = '';
	var err_newsurgDate = '';
	var err_newloanDate = '';
	var reqids = document.frmSalesDashLoanerExt.inputString.value;
	var allExtn = 'N';
	var inputString = "";
	var requestdtlid = "";
	var parentids_str="";
	var oldrequest_Id = '';
	var surgDtFmt = '';
	//Collecting the Whole extn flag checked records into parentids_str
	gridObj.forEachRow(function(rowId){
		check_id = gridObj.cellById(rowId, check_rowId).getValue();
		allExtn = (check_id == 1)?'Y':'N';
		request_Id = gridObj.cellById(rowId, request_rowId).getValue();
		if(allExtn == 'Y'){
			parentids_str = parentids_str + request_Id + ',' ;
		}
	});
	gridObj.forEachRow(function(rowId){
		check_id = gridObj.cellById(rowId, check_rowId).getValue();
		allExtn = (check_id == 1)?'Y':'N';
		request_Id = gridObj.cellById(rowId, request_rowId).getValue();
		reqdtl_id = gridObj.cellById(rowId, reqdtl_rowid).getValue();
		new_extndate = gridObj.cellById(rowId, new_date).getValue();
		day_count = gridObj.cellById(rowId, day_ext_rowid).getValue();
		consignid = gridObj.cellById(rowId, consign_rowid).getValue();
		surgdt = gridObj.cellById(rowId, surgdt_rowid).getValue();
		loandt = gridObj.cellById(rowId, loandt_rowid).getValue();
		expctedDate = gridObj.cellById(rowId, expecteddate_rowid).getValue();	
		surgDtFmt = gridObj.cellById(rowId, surgdt_rowid);
		var dayalt = fnGetCodeAlt(day_count);
		//For the below condition,we should include the child requests in the input string.
		//1. When parent req Id value is not there in parentids_str  OR 
		//2. When parent req Id value is there in parentids_str and the whole Flag ='Y'
		if((parentids_str.indexOf(request_Id) == -1 )|| ( parentids_str.indexOf(request_Id) != -1 && allExtn == 'Y')){
			inputString = inputString + request_Id + "^" + reqdtl_id + "^" +consignid +"^"+ new_extndate + "^" + allExtn + "^" +surgdt + "|";		
			requestdtlid = requestdtlid + reqdtl_id + ",";
		}
		if(expctedDate == ''){	
			err_expRtnDate = err_expRtnDate +"<br>"+request_Id;
			Error_Clear();
		}
		if(new_extndate == ''){
			err_newExpDate = err_newExpDate +"<br>"+request_Id;
			Error_Clear();
		}	

		if(oldrequest_Id != request_Id && surgdt == ''){
			err_newsurgDate = err_newsurgDate +"<br>"+request_Id;
			Error_Clear();
		}
		
		if(oldrequest_Id != request_Id){
			if((dateDiff(today_dt,surgdt,dateFormat)<0) || (dateDiff(loandt,surgdt,dateFormat)<0)){
				err_newloanDate = err_newloanDate +"<br>"+request_Id; 		 
			}
		}
		if(surgdt != ''){
			CommonDateValidation(surgDtFmt,dateFormat,message[10529]);
		}
		
		oldrequest_Id = request_Id;
	});
		
	// Error message for Empty Extension Return Date..
	if (err_expRtnDate != ''){
		Error_Details(Error_Details_Trans(message_sales[33],err_expRtnDate));
		Error_Show();
		Error_Clear();
		return false;
	}	
	// Error message for Empty New Extension Return Date..
	if (err_newExpDate != ''){
		Error_Details(Error_Details_Trans(message_sales[34],err_newExpDate));
		Error_Show();
		Error_Clear();
		return false;
	}
	if (err_newsurgDate != ''){
		Error_Details(message_sales[432]);
		Error_Show();
		Error_Clear();
		return false;
	}if (err_newloanDate != ''){
		Error_Details(message_sales[433]);	
		Error_Show();
		Error_Clear();
		return false;
	}
	fnValidateTxtFld('txt_LogReason',' Comments ');
		
	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}else{		
		document.frmSalesDashLoanerExt.reqiestDtlID.value = requestdtlid;
		document.frmSalesDashLoanerExt.action = "/gmSalesDashLoanerExt.do?method=saveLoanerExt&extendString="+ encodeURI(inputString);
		fnStartProgress();
		document.frmSalesDashLoanerExt.submit();
		//Below time out is set, to refresh the parent page after updating the Extend count in DB. 
		setTimeout(fnRefreshParent, 1000);
	}
}

function fnClose(){
	window.close();
}

window.onbeforeunload = fnRefreshParent;
function fnRefreshParent(){
	if(window.opener != null && !window.opener.closed){
		window.opener.location.href = window.opener.location.href + "&randomId="+Math.random();
	}	
}

function fnGetCodeAlt(day_count){
	var codealt = '';
	if(day_count == '1931'){
		codealt = '1';
	}else if(day_count == '1932'){
		codealt = '2';
	}else if(day_count == '1933'){
		codealt = '3';
	}else if(day_count == '1934'){
		codealt = '4';
	}else if(day_count == '1935'){
		codealt = '5';
	}
	return codealt;
}

