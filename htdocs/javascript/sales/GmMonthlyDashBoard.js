function fnTotalAvg(){
	document.getElementById("spnTotal").innerHTML = fnStdCurrFmt(totalSales,currSymbol);
	var avgSales = (workingDays == 0)?0:totalSales/workingDays;
	document.getElementById("spnAvg").innerHTML = fnStdCurrFmt(avgSales,currSymbol);
}


function fnStdCurrFmt(val,currSym){
	if(val < 0){
		return "<span style='color:red;'> "+currSym+" ("+formatCurrency(val)+")</span>";
	}else{
		return "<span>"+currSym+" "+formatCurrency(val)+"</span>";
	}
}

function formatCurrency(num,inclsign) {
	num = num.toString().replace(/\$|\,/g,'');
	if(isNaN(num))
		num = "0";
	sign = (num == (num = Math.abs(num)));
	num = Math.floor(num*100+0.50000000001);
	cents = num%100;
	num = Math.floor(num/100).toString();
	if(cents<10)
		cents = "0" + cents;
	for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
		num = num.substring(0,num.length-(4*i+3))+','+num.substring(num.length-(4*i+3));
	return (((inclsign)?((sign)?'':'-'):'') + ((inclsign)?'$ ':'') + num );
}


function fnTopDistBarChart() {

	// When the data is empty dont render the chart.
	if (true) {
		// Chart type declaration
		var topDistBarObj = {
			type : 'bar2d',
			renderAt : 'divTopDistBarContainer',
			width : '458',
			height : '249',
			dataFormat : 'json'
		};
		var topDistBarDataSrc = {};

		// Chart parameters declaration
		topDistBarDataSrc.chart = {
			"caption" : "Top 5 Distributors",
			"captionFontColor" : "#000000",
			"captionFont" : "verdana, arial, sans-serif",
			// "subCaption": "Harry's SuperMart",
			// "xAxisName": "Month",
			// "yAxisName": "Revenues (In USD)",
			"numberPrefix" : currSymbol,
            "paletteColors": "#45abf5",
			"bgColor" : "#eeeeee",
			// "canvasbgColor": "#eeeeee",
			"borderAlpha" : "20",
			"valueFontColor" : "#000000",
			"labelfontcolor" : "#000000",
			"outCnvBaseFontColor" : "#000000",
			"canvasBorderAlpha" : "0",
			"usePlotGradientColor" : "0",
			"plotBorderAlpha" : "10",
			"placevaluesInside" : "0",
			"rotatevalues" : "1",
			"showValues" : "0",

			"formatNumberScale" : "1",
			// "decimals": "2",
			// "forceDecimals": "1",

			"chartBottomMargin" : "60",

			"toolTipColor" : "#ffffff",
			"toolTipBorderThickness" : "0",
			"toolTipBgColor" : "#000000",
			"toolTipBgAlpha" : "80",
			"toolTipBorderRadius" : "2",
			"toolTipPadding" : "5",
			//"yAxisMaxValue" : highestDistVal,// adjust y axis max val range for
											// highest trend line
			"showTrendlineLabels" : "0",
			"showBorder" : "0",
			"labelDisplay" : "auto",
			"adjustDiv" : "0", // adjust y axis devision range for highest
								// trend line values.
			// "numDivLines": "5",
			"showXAxisLine" : "1",
			"xAxisLineColor" : "#999999",
			"divlineColor" : "#999999",
			"divLineIsDashed" : "0",
			"showAlternateVGridColor" : "0",
			"subcaptionFontBold" : "0",
			"subcaptionFontSize" : "14",
			"showPercentValues" : "1",
			"showPercentInToolTip" : "1"
			/*"exportenabled" : "1",
			"exportatclient" : "0",
			"showHoverEffect" : "1",
			"exporthandler" : fcExportServer,
			"html5exporthandler" : fcExportServer*/
		};
		// Assigning the data array values
		topDistBarDataSrc.data = topDistBarDataArray.data;

		topDistBarObj.dataSource = topDistBarDataSrc;

		// Fusion chart rendering.
		var topDistBarChart = new FusionCharts(topDistBarObj);
		topDistBarChart.render();
	}
}

function fnTopAcctBarChart() {

	// When the data is empty dont render the chart.
	if (true) {
		// Chart type declaration
		var topAcctBarObj = {
			type : 'bar2d',
			renderAt : 'divTopAcctBarContainer',
			width : '458',
			height : '249',
			dataFormat : 'json'
		};
		var topAcctBarDataSrc = {};

		// Chart parameters declaration
		topAcctBarDataSrc.chart = {
			"caption" : "Top 5 Accounts",
			"captionFontColor" : "#000000",
			"captionFont" : "verdana, arial, sans-serif",
			// "subCaption": "Harry's SuperMart",
			// "xAxisName": "Month",
			// "yAxisName": "Revenues (In USD)",
			"numberPrefix" : currSymbol,
            "paletteColors": "#7F6E4C",
			"bgColor" : "#eeeeee",
			// "canvasbgColor": "#eeeeee",
			"borderAlpha" : "20",
			"valueFontColor" : "#000000",
			"labelfontcolor" : "#000000",
			"outCnvBaseFontColor" : "#000000",
			"canvasBorderAlpha" : "0",
			"usePlotGradientColor" : "0",
			"plotBorderAlpha" : "10",
			"placevaluesInside" : "0",
			"rotatevalues" : "1",
			"showValues" : "0",

			"formatNumberScale" : "1",
			// "decimals": "2",
			// "forceDecimals": "1",

			"chartBottomMargin" : "60",

			"toolTipColor" : "#ffffff",
			"toolTipBorderThickness" : "0",
			"toolTipBgColor" : "#000000",
			"toolTipBgAlpha" : "80",
			"toolTipBorderRadius" : "2",
			"toolTipPadding" : "5",
			//"yAxisMaxValue" : highestAcctVal,// adjust y axis max val range for
											// highest trend line
			"showTrendlineLabels" : "0",
			"showBorder" : "0",
			"labelDisplay" : "auto",
			"adjustDiv" : "0", // adjust y axis devision range for highest
								// trend line values.
			// "numDivLines": "5",
			"showXAxisLine" : "1",
			"xAxisLineColor" : "#999999",
			"divlineColor" : "#999999",
			"divLineIsDashed" : "0",
			"showAlternateVGridColor" : "0",
			"subcaptionFontBold" : "0",
			"subcaptionFontSize" : "14",
			"showPercentValues" : "1",
			"showPercentInToolTip" : "1"
	/*		"exportenabled" : "1",
			"exportatclient" : "0",
			"showHoverEffect" : "1",
			"exporthandler" : fcExportServer,
			"html5exporthandler" : fcExportServer*/
		};
		// Assigning the data array values
		topAcctBarDataSrc.data = topAcctBarDataArray.data;

		topAcctBarObj.dataSource = topAcctBarDataSrc;

		// Fusion chart rendering.
		var topAcctBarChart = new FusionCharts(topAcctBarObj);
		topAcctBarChart.render();
	}
}


function fnDailyBarChart() {

	// When the data is empty dont render the chart.
	if (true) {
		// Chart type declaration
		var topDailyBarObj = {
			type : 'line',
			renderAt : 'divDailyContainer',
			width : '458',
			height : '400',
			dataFormat : 'json'
		};
		var topDailyBarDataSrc = {};

		// Chart parameters declaration
		topDailyBarDataSrc.chart = {
			"caption" : "Daily Sales",
			"captionFontColor" : "#000000",
			"captionFont" : "verdana, arial, sans-serif",
			// "subCaption": "Harry's SuperMart",
			// "xAxisName": "Month",
			// "yAxisName": "Revenues (In USD)",
			"numberPrefix" : currSymbol,
            "paletteColors": "#116600",
			"bgColor" : "#eeeeee",
			// "canvasbgColor": "#eeeeee",
			"borderAlpha" : "20",
			"valueFontColor" : "#000000",
			"labelfontcolor" : "#000000",
			"outCnvBaseFontColor" : "#000000",
			"canvasBorderAlpha" : "0",
			"usePlotGradientColor" : "0",
			"plotBorderAlpha" : "10",
			"placevaluesInside" : "0",
			"rotatevalues" : "1",
			"showValues" : "0",

			"formatNumberScale" : "1",
			// "decimals": "2",
			// "forceDecimals": "1",

			"chartBottomMargin" : "60",

			"toolTipColor" : "#ffffff",
			"toolTipBorderThickness" : "0",
			"toolTipBgColor" : "#000000",
			"toolTipBgAlpha" : "80",
			"toolTipBorderRadius" : "2",
			"toolTipPadding" : "5",
			"yAxisMaxValue" : highestDailyVal,// adjust y axis max val range for
											// highest trend line
			"showTrendlineLabels" : "0",
			"showBorder" : "0",
			"labelDisplay" : "auto",
			"adjustDiv" : "0", // adjust y axis devision range for highest
								// trend line values.
			// "numDivLines": "5",
			"showXAxisLine" : "1",
			"xAxisLineColor" : "#999999",
			"divlineColor" : "#999999",
			"divLineIsDashed" : "0",
			"showAlternateHGridColor" : "0",
			"subcaptionFontBold" : "0",
			"subcaptionFontSize" : "14",
			"showPercentValues" : "1",
			"showPercentInToolTip" : "1"
	/*		"exportenabled" : "1",
			"exportatclient" : "0",
			"showHoverEffect" : "1",
			"exporthandler" : fcExportServer,
			"html5exporthandler" : fcExportServer*/
		};
		// Assigning the data array values
		topDailyBarDataSrc.data = topDailyBarDataArray.data;

		topDailyBarObj.dataSource = topDailyBarDataSrc;

		// Fusion chart rendering.
		var topDailyBarChart = new FusionCharts(topDailyBarObj);
		topDailyBarChart.render();
	}
}


function fnProductPieChart(){
	//When the data is empty don't render the chart.
	//Render the chart only when the data array is not null & total sales is in Positive value 
    if(productDataArray !=''){
	//Chart Type declaration
	var productObj = {
		    type: 'doughnut2d',
		    renderAt: 'divProductContainer',
		    width: '458',
		    height: '400',
		    dataFormat: 'json'
		};
		
	//Chart parameters declaration
	var productChart={
              "caption": "Top 20 Product Sales",
              "captionFontColor":"#000000",
              "captionFont":"verdana, arial, sans-serif",
              //"subCaption": "Last year",
              "numberPrefix": currSymbol,
              //"paletteColors": "#0075c2,#1aaf5d,#f2c500,#f45b00,#8e0000",
              "bgColor": "#eeeeee",
              "showBorder": "0",
              "use3DLighting": "0",
              "showShadow": "0",
              "enableSmartLabels": "0",
              "labelFontColor": "#000000", 
              "valueFontColor": "#000000",//baseFontColor 
              "startingAngle": "90",
              "showLabels": "0",
  	          "showPercentValues":"1",
  	          "showPercentInToolTip":"1",
              "showLegend": "0",              
              "labelDisplay": "none",
              
              "manageLabelOverflow":"1",
              "useEllipsesWhenOverflow":"1",
            	  
              "toolTipColor":"#000000",
              "toolTipBorderThickness":"0",
              "toolTipBgColor":"#000000",
              "toolTipBgAlpha":"80",
              "toolTipBorderRadius":"2",
              "toolTipPadding":"5",

      		  "showHoverEffect": "1",
              "formatNumberScale": "0",
              "decimals": "0",
              "forceDecimals": "1",             
              "legendShadow": "0",
              "legendBorderAlpha": "0",
              "legendPosition":"RIGHT",
              "centerLabel": "$value <br> $percentValue <br> $label",
              "centerLabelBold": "1",

              "showTooltip": "0",
              
              "chartLeftMargin":"0",
              "chartRightMargin":"0",
              "chartTopMargin":"5",
              "chartBottomMargin":"5",
              

              "captionFontSize": "14",
              "subcaptionFontSize": "14",
              "subcaptionFontBold": "0",
  			  //"exportenabled" : "1",
  			 // "exportatclient" : "0",
  			  "animation":"0",
              //"showLabels": "0", //hide the labels
 			  "showValues": "0" //hide the values
  			  //"exporthandler" : fcExportServer,
  			 // "html5exporthandler" : fcExportServer
          };

	var productDataSrc={};
	productDataSrc.chart=productChart;
	
	//Assigning Chart data
	productDataSrc.data=productDataArray.data.reverse();//To display the doughnut pieces in clockwise direction, data array is reversed as there is no default option. 
	productObj.dataSource=productDataSrc;
	
	//Fusion chart rendering.
    var productChart = new FusionCharts(productObj);
    productChart.render();
    }
}

function formatCurrency(num) {
	num = num.toString();
	if (isNaN(num))
		num = "0";
	sign = (num == (num = Math.abs(num)));
	num = Math.floor(num * 100 + 0.50000000001);
	cents = num % 100;
	num = Math.floor(num / 100).toString();
	if (cents < 10)
		cents = "0" + cents;
	for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
		num = num.substring(0, num.length - (4 * i + 3)) + ','
				+ num.substring(num.length - (4 * i + 3));

	return num + '.' + cents;
}
