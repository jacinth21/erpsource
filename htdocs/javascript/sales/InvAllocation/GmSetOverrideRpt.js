
function fnSelectedTagID(obj){
	document.frmSetOverrideRpt.tagId.value = TRIM(obj.value);
	var vTagDistObj = eval('document.all.tagDistId_'+obj.value);
	document.frmSetOverrideRpt.tagDistId.value = vTagDistObj.value;
}
function fnMapSelectedTagID(tagid,distid){
	document.frmSetOverrideRpt.tagId.value = tagid;
	document.frmSetOverrideRpt.tagDistId.value = distid;
	fnSubmit();
}
function fnSubmit(){
	var tagID = document.frmSetOverrideRpt.tagId.value;
	var vLockFl = '';
	var vDecomFl = '';
	if (tagID == '')
	{
		Error_Details(message_sales[114]);
	}else{
		obj = eval('document.all.Lock_'+tagID);
		if(obj!=null)
		{
		vLockFl = obj.value;
		if(vLockFl == 'Y'){
			Error_Details(message_sales[112]);
		}
		}
		obj	= eval('document.all.Decomsin_'+tagID);
		if(obj!=null)
		{
		vDecomFl = obj.value;
		if(vDecomFl== 'Y'){
			Error_Details(message_sales[113]);
		}
		}
	}
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	document.frmSetOverrideRpt.strOpt.value = 'save';
	document.frmSetOverrideRpt.submit();
}

function fnLoad(){
	document.frmSetOverrideRpt.strOpt.value = 'load';
	document.frmSetOverrideRpt.submit();
}

function fnLoadMap(){
	
	document.frmSetOverrideRpt.strOpt.value = 'loadMap';
//	document.frmMap.action = '/gmMap.do?method=getMap';
	document.frmSetOverrideRpt.submit();
}	

function displayDiv()
{	
	var buttonVal = document.getElementById('load').value;
	if(buttonVal == 'map')
	{
		document.getElementById('load').style.display = 'none';
		document.getElementById('map').style.display = '';
	}		
	else
	{
		document.getElementById('map').style.display = 'none';
		document.getElementById('load').style.display = '';
	}
	
}

function fnPageLoad(){
	
	if(document.frmSetOverrideRpt.mapFlag.value == "Y")
	{
		document.frmSetOverrideRpt.btn_Map.disabled = false;
	}else{
		document.frmSetOverrideRpt.btn_Map.disabled = true;
	}
	if(document.frmSetOverrideRpt.strOpt.value == 'reload'){
	   var vParentFrm = document.frmSetOverrideRpt.parentFrmName.value
	   if(vParentFrm == 'frmPendAllocation' || vParentFrm == 'frmCasePost' ){
		   eval('parent.window.document.'+vParentFrm).submit(); 
	   }else{
		   parent.window.history.go(0);
	   }
	   parent.dhxWins.window("w1").close();
	   return false;
	}
}

function popitup(tagId) {
	
		w1 = createDhtmlxWindow(850,300);
	    w1.setText(message_sales[115]); 
		w1.attachURL("/GmSetImgMgtServlet?TAGID="+tagId+"&hAction=View");
		return false;
	
}

function fnClose(){
	   CloseDhtmlxWindow();
	   return false;
}