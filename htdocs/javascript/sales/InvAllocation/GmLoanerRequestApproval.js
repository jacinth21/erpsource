var selectedReqIds = "";
var selectedRowCnt="";
var apprQty="";
var inputStr="";
var reqId ="";
var partNo ="";
var priceNoteDisplayFL = false;
var selectfl = false;
var confirmMsg = true;
var returnqty = "";
var distid = "";
var gridObj="";
var partInputstr = '';
var raqtyInputstr = '';
var temppartreturnstr = '';
var temppartQtystr = '';
var repid = '';
function fnSelectReqID(chk){
	if(chk.checked){
		selectedReqIds += ','+chk.value;
	}else{
		selectedReqIds=selectedReqIds.replace(','+chk.value,'');
	}
}

var HashMap = function(){
    var map={};
    this.put = function(key,val){
       
        if(map[key] != undefined){
            map[key] = map[key] +"^"+val;
        }else{
            map[key] = val;
        }
    };
    this.get = function(key){
        return map[key];
    };
    this.getInputStr= function(){
        var string = "";
        for(var attributename in map){
        string+=attributename+"^"+map[attributename]+"|";
        }
        return string;
    };
} 

var partstr = new HashMap();
var raqtystr =  new HashMap();
function fnCreateApprovalString(){
	var gridrows = gridObj.getAllRowIds();
	var rowsarr = gridrows.toString().split(",");
	distrow_id = gridObj.getColIndexById("d_id");
	var reprow_id = gridObj.getColIndexById("salesrep_id");
	var userType = document.frmLoanerRequestApproval.userType.value;
	inputStr = '';
	var reqNameStr = '';
	if (rowsarr.length > 0) {
		disableChkFl = gridObj.getColIndexById("disableFl");
		for ( var rowid = 0; rowid < rowsarr.length; rowid++) {
			var rowcurrentid = rowsarr[rowid];			
			if(disableChkFl != undefined && disableChkFl != ""){
				 needDisableFl = gridObj.cellById(rowcurrentid, disableChkFl).getValue();	 
			}else{
				 needDisableFl = 'N';
			}
			if(needDisableFl == 'N' ){						 
				var checkInd = gridObj.cells(rowcurrentid,gridObj.getColIndexById("check")).getValue();
				
				
				reqId = gridObj.cells(rowcurrentid, gridObj.getColIndexById("req_id")).getValue();
				partNo =  gridObj.cells(rowcurrentid, gridObj.getColIndexById("part_num")).getValue();
				apprQty =  gridObj.cells(rowcurrentid, gridObj.getColIndexById("appr_qty")).getValue();
				if(userType == "PD"){
				returnqty = gridObj.cells(rowcurrentid, gridObj.getColIndexById("retn_qty")).getValue();
				}
				repid = gridObj.cells(rowcurrentid,reprow_id).getValue();
				distid = gridObj.cells(rowcurrentid,distrow_id).getValue();
				
				if(checkInd !='0'){
					reqName = gridObj.cells(rowcurrentid, gridObj.getColIndexById("rep_name")).getValue();
					reqNameStr += (reqNameStr.indexOf(reqName) != -1)? "":", "+reqName;
					inputStr += reqId+','+partNo+','+apprQty+'|';
					if(returnqty != "" && returnqty != "0"){
						partstr.put(distid+'@'+repid,partNo);
						raqtystr.put(distid+'@'+repid,returnqty);
					}
				}
			}
		}
		partInputstr = partstr.getInputStr();
		raqtyInputstr = raqtystr.getInputStr();

	}
	reqNameStr = reqNameStr.substring(1, reqNameStr.length);
	if(reqNameStr.indexOf(',')!= -1){
		document.frmLoanerRequestApproval.requestor_name.value = '';
	}else{
		document.frmLoanerRequestApproval.requestor_name.value = reqNameStr;
	}
	document.frmLoanerRequestApproval.partString.value = partInputstr;
	document.frmLoanerRequestApproval.raqtyString.value = raqtyInputstr;
	return inputStr;
}
function onClickSelect(id,cellInd,state){
	selectfl = false;
	var checkInd = gObj.getColIndexById("check");
	var allRowCount = gObj.getRowsNum();
	var parReqId='';
	var setId='';
	document.getElementById("approve").disabled = false;		
	if(state && cellInd == 0 ){
				selectedReqIds += ','+id;
				selectedRowCnt++;
				if(selectedRowCnt == allRowCount){
				document.frmLoanerRequestApproval.mastercheck.checked = true ;
			} 
		}else{
		    selectedReqIds=selectedReqIds.replace(','+id,'');
			document.frmLoanerRequestApproval.mastercheck.checked = false ;
			selectedRowCnt--;
			
		}
	var disableApprBtnFl=false;
	gObj.forEachRow( function(id){
		var needDisableFl = gObj.cellById(id,  gObj.getColIndexById("disableFl")).getValue();
		var checkFl = gridObj.cells(id,gObj.getColIndexById("check")).getValue();
		if(needDisableFl=='Y' && checkFl=='1'){
			disableApprBtnFl=true;
	}
	});
	 if(disableApprBtnFl==true)
	   {
	      document.getElementById("approve").disabled = true;
	   }
		
}
function validateQty(status){
	var apprQtyfl = false;
	var apprQtyNumFl = false;
	var returnQtyNumFl = false;
	var negQtyfl = false;
	var negQtyReturnfl = false; 
	var reQty = '';
	var reqQtyfl = false;
	var errReqQtyfl = false;
	var qtychkfl = false;
	var gridrows = gridObj.getAllRowIds();
	var rowsarr = gridrows.toString().split(",");
	var userType = document.frmLoanerRequestApproval.userType.value;
	if (rowsarr.length > 0) {
		disableChkFl = gridObj.getColIndexById("disableFl");
		for ( k = 0; k < rowsarr.length; k++) {		
			var rowcurrentid = rowsarr[k];	
			 reqQtyfl = false;
			 if(disableChkFl != undefined && disableChkFl != ""){
				 needDisableFl = gridObj.cellById(rowcurrentid, disableChkFl).getValue();	 
			 }else{
				 needDisableFl = 'N';
			 }			 
			 if(needDisableFl == 'N' ){
				var checkInd = gridObj.cells(rowcurrentid,gridObj.getColIndexById("check")).getValue();
				apprQty = parseInt(gObj.cells(rowcurrentid, gObj.getColIndexById("appr_qty")).getValue());
				reqQty = parseInt(gObj.cells(rowcurrentid, gObj.getColIndexById("req_qty")).getValue());
				parts = gridObj.cells(rowcurrentid,gridObj.getColIndexById("part_num")).getValue();
				
				if(userType == "PD"){
					returnqty = gridObj.cells(rowcurrentid, gridObj.getColIndexById("retn_qty")).getValue();
					}
				if(checkInd !='0'){				
					if((returnqty > apprQty) && status == "Approve"){
						qtychkfl = true;
						temppartreturnstr += parts + ',';
					}
					if(apprQty > reqQty){
						reqQtyfl = true;
						errReqQtyfl  = true;
						temppartQtystr += parts + ',';						
					}
					if(isNaN(returnqty)){
						returnQtyNumFl = true;
					}else{
						if(returnqty < 0){
							negQtyReturnfl =true;
						}
					}
					if(isNaN(apprQty)){
						apprQtyNumFl = true;
					}else{
						if(apprQty < 0){
							negQtyfl =true;
						}
					}
					if(apprQty == '' || apprQty == '0'){
						apprQtyfl =true;
					}
					if(apprQtyfl || isNaN(apprQty) || reqQtyfl) {
						gObj.setRowColor(rowcurrentid,"pink");
					}else{
						gObj.setRowColor(rowcurrentid,"");
					}
				}else{
				   gObj.setRowColor(rowcurrentid,"");
				}
			 }
		}
		temppartreturnstr = temppartreturnstr.substring(0, temppartreturnstr.length - 1);
		temppartQtystr = temppartQtystr.substring(0, temppartQtystr.length - 1);
		if((qtychkfl) && status == "Approve") {
			Error_Details(Error_Details_Trans(message[5063],temppartreturnstr));
			temppartreturnstr = '';
		}
		if(apprQtyfl){
			Error_Details(message[5064]);
		}
		if(errReqQtyfl){
			Error_Details(Error_Details_Trans(message[5065],temppartQtystr));
			temppartQtystr = '';
		}
		if(apprQtyNumFl){
			Error_Details(message[5066]);
		}
		if(negQtyfl){
			Error_Details(message[5067]);
		}
		if(returnQtyNumFl){
			Error_Details(message[5068]);
		}
		if(negQtyReturnfl){
			Error_Details(message[5069]);
		}
	}
	return ErrorCount;
}
function fnApprove(ScreenName){
	
	//alert("ScreenName *** "+ScreenName);
	var userType = document.frmLoanerRequestApproval.userType.value;
	if(ScreenName == "Request"){
	if(selectedReqIds != ''){
		// if type is 400088 getting inputstring and if approve qty is more than req qty throwing validations.
		if(strTxnType == '400088'){ //400088 - Consignment Item
			
			ErrorCount = validateQty('Approve');
			inputStr = fnCreateApprovalString();
			document.frmLoanerRequestApproval.hApproveInputString.value = inputStr;
		}
		document.frmLoanerRequestApproval.strReqIds.value = selectedReqIds.substr(1,selectedReqIds.length);
	}else{
		Error_Details(message[5070]);
	}
	}
	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}else{
		if(ScreenName == "Request"){

		if(selectfl == true){
			confirmMsg =  fnConfirmMessage();
		}else{
			confirmMsg = true;
		}
		}else{
			confirmMsg = true;
		}
		if(confirmMsg){
			
				
				if(ScreenName == "Comments"){
					raqtyInputstr = parent.document.frmLoanerRequestApproval.raqtyString.value;
				}
				if(raqtyInputstr == ""){
					document.frmLoanerRequestApproval.strOpt.value = 'save';
				}
				else{
					document.frmLoanerRequestApproval.strOpt.value = 'appwithra';
				}
				if(ScreenName == "Request"){
				if(strOptVal =='PDREJLNREQAPPR'){
						document.frmLoanerRequestApproval.haction.value = 'PDREJLNREQAPPR';
				}else{
						document.frmLoanerRequestApproval.haction.value = 'Approved';	
				}
				}
				
		if(ScreenName == "Request"){
			if(userType == "PD" && strTxnType == '400088'){
				var chkbox =document.frmLoanerRequestApproval.chk_ShowArrowFl.checked;
				if(chkbox == true){
					fnApprWithComments();
					return false;
				}
		}
		fnStartProgress('Y');
		document.frmLoanerRequestApproval.submit();
		}else if(ScreenName == "Comments"){
			var to = document.frmLoanerRequestApproval.email_to.value;
			var cc = document.frmLoanerRequestApproval.email_cc.value;
			var comments = document.frmLoanerRequestApproval.email_comments.value;
    		
    		validateEmails( to, "," ,"To ");
			validateEmails( cc, "," ,"Cc ");

			if(to == ''){			
				Error_Details(message[5071]);
			}
			if(comments == ''){			
				Error_Details(message[5072]);
			}
			
			if (ErrorCount > 0){
				Error_Show();
				Error_Clear();
				return false;
			}else{
				fnAssignData();
				fnStartProgress('Y');
				document.frmLoanerRequestApproval.submit();
				}
			
		}
		

		}
	}
}

function fnReject(){
	if(selectedReqIds == ''){
		Error_Details(message[5073]);
	}
	
	if(strTxnType == '400088'){
		ErrorCount = validateQty('reject');
	}
	
	if (ErrorCount > 0){
		
		Error_Show();
		Error_Clear();
		return false;
	}else{
		if(selectfl == true){
			confirmMsg =  fnConfirmMessage();
		}else{
			confirmMsg = true;
		}
		
		if(confirmMsg){
		
		if(strTxnType == '4127'){  // 4127 - Product Loaner
		    document.frmLoanerRequestApproval.action="/GmCommonCancelServlet?hCancelType=VLNRQ&hTxnId="+selectedReqIds.substr(1,selectedReqIds.length);
		}
		else if (strTxnType == '4119'){   // 4119 - Inhouse Loaners
			document.frmLoanerRequestApproval.action="/GmCommonCancelServlet?hCancelType=VILNRQ&hTxnId="+selectedReqIds.substr(1,selectedReqIds.length);	
		}
		else if (strTxnType == '400088'){  //400088 - Consignment Item
			document.frmLoanerRequestApproval.action="/GmCommonCancelServlet?hCancelType=ITEMRQ&hTxnId="+selectedReqIds.substr(1,selectedReqIds.length);	
		}
		else if (strTxnType == '400087'){  //400087 - Consignment Set
			document.frmLoanerRequestApproval.action="/GmCommonCancelServlet?hCancelType=SETRQ&hTxnId="+selectedReqIds.substr(1,selectedReqIds.length);	
		}
		document.frmLoanerRequestApproval.submit();  
	}
	}
}
function fnSalesReport(setid , adid ,sysid)
{  
	 w1 = createDhtmlxWindow(1050, 500);
     w1.setText(message_sales[105]);
     var ajaxUrl = fnAjaxURLAppendCmpInfo('/GmSalesConsignmentServlet?hAction=LoadDCONS&hAreaDirector='+adid+'&SetNumber='+sysid+'&Cbo_SalesType=50302&Cbo_TypeCon=50401&Cbo_Turns=3&Loaner_app=YES&randomId='+Math.random());
     w1.attachURL(ajaxUrl);
}

function fnOnload(){
	if (strTxnType == '400088'){ 
	var dist_id = fieldSales.getSelectedValue();
	if(dist_id != "0"){
		var disablesalesrepid = false; 
		fnDistFilterReps(dist_id,disablesalesrepid);
	}
	}
	var txtRefId = document.frmLoanerRequestApproval.requestId;
	if(txtRefId.value == ''){
		txtRefId.focus();
		txtRefId.select();
	}
	if (objGridData != '') {
		gridObj = initGridData1('loanerPendingRpt', objGridData);
		gridObj.enableHeaderMenu();
		gridObj.enableMultiline(false);
    }
	
}

function fnLoadRequset(){
	if(event.keyCode == 13)
	{	 
		
		var rtVal = fnLoad();
		if(rtVal == false){
			if(!e) var e = window.event;

	        e.cancelBubble = true;
	        e.returnValue = false;

	        if (e.stopPropagation) {
	                e.stopPropagation();
	                e.preventDefault();
	        }
		}
	}
}

//Description: This function will call onclick load button. check the validation & pass control to action.
function fnLoad() {

	objStartDt = document.frmLoanerRequestApproval.startDt;
	objEndDt = document.frmLoanerRequestApproval.endDt;
		
	/* Front-end validation added for the Entered Request ID as part of PMT-1281*/
	var ReqId = TRIM(document.frmLoanerRequestApproval.requestId.value);
	var objRegExp = /(^-?\d*$)/;
	if(!ReqId == '' && !objRegExp.test(ReqId)){
		Error_Details(message[5074]);
	}
	if(objStartDt.value != "" && objEndDt.value == ""){
		Error_Details(message[5075]);
	}
	if(objEndDt.value != "" && objStartDt.value == ""){
		Error_Details(message[5076]);
	}
	if(objStartDt.value != ""){ 
		CommonDateValidation(objStartDt, format, message_sales[100]+format);
	}
	if(objEndDt.value != ""){ 
		CommonDateValidation(objEndDt, format, message_sales[101]+format);
	}
	var dateDifference = dateDiff(objStartDt.value,objEndDt.value,format);
	if(dateDifference < 0){		
			Error_Details(message[5077]);			
	}
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	
	document.frmLoanerRequestApproval.requestId.value = ReqId;
	document.frmLoanerRequestApproval.strOpt.value = "load";
	document.frmLoanerRequestApproval.haction.value = strOptVal;
	//fnStartProgress('Y');
	fnReloadSales(null,null);
	//document.frmLoanerRequestApproval.submit();
}
function doOnCellEdit(stage, rowId, cellInd, nValue, oValue) {
	
	if(strTxnType == '400088'){
	disableChkFl = gObj.getColIndexById("disableFl");
    needDisableFl = gObj.cellById(rowId, disableChkFl).getValue();	
	if(stage==0&&cellInd=='8'&&needDisableFl=='Y'){
	return false;
	}
    if(stage==0&&cellInd=='9'&& needDisableFl=='Y'){
	return false;
	}
	}
	if (stage == 1 && cellInd == '7' ){
		var c=this.editor.obj;			
		if(c!=null) c.select();
	}
	return true;
}
function initGridData1(divRef, gridData) {
		gObj = new dhtmlXGridObject(divRef);
		gObj.setSkin("dhx_skyblue");
		gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
		//During page onLoad for CN Item we are disabling the check box. with distributed parsing we cant do this.
		//So disabling the Distributed Parsing for CN Item screen.
		if(strTxnType != '400088'){
			gObj.enableDistributedParsing(true);
		}
		if(strTxnType == '4127'){
		gObj.enableTooltips("false,false,true,true,true,true,true,true,true,true,true,true,true");
		}
		gObj.init();
		gObj.loadXMLString(gridData);
		gObj.attachEvent("onCheckbox",onClickSelect);
		gObj.attachEvent("onEditCell",doOnCellEdit);
		if(strTxnType == '4127'){
			gObj.attachHeader("<input type='checkbox' value='no' id='mastercheck' onClick='javascript:onClickSelectAll(this);'/>");
		}else if (strTxnType == '4119'){
			gObj.attachHeader("<input type='checkbox' value='no' id='mastercheck' onClick='javascript:onClickSelectAll(this);'/>");
		}
		if(strTxnType == '400088' || strTxnType == '400087'){  //400088 - Consignment Item 400087 - Consignment Set
			gObj.attachHeader("<input type='checkbox' value='no' id='mastercheck' onClick='javascript:onClickSelectAll(this);'/>");
			//When price is 0 or less, need to block user to approve respective record this.
			disableChkFl = gObj.getColIndexById("disableFl");
			if(appvlAccessFl == 'Y'){
				gObj.forEachRow(function(rowId) {	
					needDisableFl = gObj.cellById(rowId, disableChkFl).getValue();	
					//Setting background color to the editable column.
					gObj.cellById(rowId,8).setBgColor('#ccdd99');
					gObj.cellById(rowId,9).setBgColor('#ecffaf');
					
					if(needDisableFl =='Y'){
						gObj.setCellTextStyle(rowId,gObj.getColIndexById("disableFl"),"color:red;border:1px solid red;");
						gObj.setRowColor(rowId,"#F8E0E0");//this is for changing the row color of needdisableFl='Y' rows
						gObj.cellById(rowId,8).setBgColor('#F8E0E0');
						gObj.cellById(rowId,9).setBgColor('#F8E0E0');
						gObj.cellById(rowId, 0).setDisabled(false);
						priceNoteDisplayFL = true;
					}
				});
			}
			if(priceNoteDisplayFL){
				document.getElementById("pricenote").style.display="block";
			}
		}
		return gObj;
	}

function onClickSelectAll(val){
	var partStr="";
	var checkInd = gObj.getColIndexById("check");
	disableChkFl = gObj.getColIndexById("disableFl");
	if(val.checked == true && checkInd == 0 ){
		 gObj.forEachRow( function(id){
			 if(disableChkFl != undefined && disableChkFl != ""){
				 needDisableFl = gObj.cellById(id, disableChkFl).getValue();	 
			 }else{
				 needDisableFl = 'N';
			 }
			if(needDisableFl == 'Y' ){
				partNum= gObj.cellById(id,gObj.getColIndexById("part_num")).getValue();
				eventName= gObj.cellById(id,gObj.getColIndexById("case_event")).getValue();
			    partStr += partNum+'-'+eventName+',     ';
			    document.getElementById("approve").disabled = true;
			}

				 gObj.cellById(id,checkInd).setChecked(true);			 
				 selectedReqIds += ','+id;
				 selectedRowCnt++;
		 });
		 selectfl = true;
	}else{ 
		 gObj.forEachRow( function(id){
			 if(disableChkFl != undefined && disableChkFl != ""){
				 needDisableFl = gObj.cellById(id, disableChkFl).getValue();	 
			 }else{
				 needDisableFl = 'N';
			 }
				 document.getElementById("approve").disabled = false;
			     gObj.cellById(id,checkInd).setChecked(false);
				 selectedReqIds = '';
				 selectedRowCnt ='';
				}
		 );
		 
		 
	}
	partStr = partStr.substring(0, partStr.length - 1);
	if(partStr!=''){
		Error_Details(Error_Details_Trans(message_sales[102],partStr));
		partStr='';
	}
		if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
}

function fnConfirmMessage(){
	confirmMsg = confirm(message_sales[104]);
	if(confirmMsg)	return true;
	else 			return false;
} 

function fnExportToExcel(){
	gObj.detachHeader(1);
	gObj.setColumnHidden(0,true);	
	gObj.toExcel('/phpapp/excel/generate.php');	
	if(strTxnType == '4127'){
		gObj.attachHeader("<input type='checkbox' value='no' id='mastercheck' onClick='javascript:onClickSelectAll(this);'/>,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#select_filter,#rspan,#select_filter,#text_filter,#text_filter");
	}else if (strTxnType == '4119'){
		gObj.attachHeader("<input type='checkbox' value='no' id='mastercheck' onClick='javascript:onClickSelectAll(this);'/>,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#select_filter,#select_filter,#select_filter,#text_filter");
	} 
	if(strTxnType == '400088' || strTxnType == '400087'){  //400088 - Consignment Item  400087 - Consignment Set
		gObj.attachHeader("<input type='checkbox' value='no' id='mastercheck' onClick='javascript:onClickSelectAll(this);'/>,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter");
	}
	gObj.setColumnHidden(0,false);	
}
//The Function is used to filter the Sales Rep Dropdown based on Field Sales
function fnDistFilterReps(distid,flag)
{ 
	var distname = fieldSales.getSelectedText();
	var distid = fieldSales.getSelectedValue();
	var salesrepname = salesRep.getSelectedText();
	var salesrepid = salesRep.getSelectedValue();
	salesRep.clearAll();
	salesRep[0]= salesRep.addOption("0","[Choose One]","");
	var k = 0;
	if(flag != false){
	salesRep.setComboValue("0");
	}
	if(distid != ''){
		for (var i=0;i<repLen;i++)
		{
			arr = eval("RepArr"+i);
			arrobj = arr.split("^");
			id = arrobj[0];
			rid = arrobj[2];
			name = arrobj[3];
		
			if((id == distid) )
			{
				salesRep[k+1] = salesRep.addOption(rid,name,"");
				k++;
			}
		
		}
	}

} 
//The Function is used to Called When we Click on Inventory Lookup Button.
function fnInventoryLookup()
{
	var strPart = fnPartString();
	if(strPart==''){
		return false;
	}
	dhxWins = new dhtmlXWindows();
    dhxWins.enableAutoViewport(true);
    w1 = dhxWins.createWindow("w1", 30, 250, 1280, 470);
    w1.setText(message_sales[106]);
    var ajaxUrl = fnAjaxURLAppendCmpInfo('/GmPartInvReportServlet?hAction=Go&hOpt=MultipleParts&hScreenType=BACKORDER&hPartNum='+encodeURIComponent(strPart));
    w1.attachURL(ajaxUrl,"Pack","resizable=yes,scrollbars=yes,top=250,left=250,width=960,height=510");	
	
}

//The Function is used to Called When we Click on Trend Report Button.
function fnTrendLookup()
{
	var strPart = fnPartString();
	 if(strPart==''){
			return false;
		}
	dhxWins = new dhtmlXWindows();
    dhxWins.enableAutoViewport(true);
    w1 = dhxWins.createWindow("w1", 30, 250,1180, 470);
    w1.setText(message_sales[107]);
    var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmLogisticsReport.do?method=loadTrendReport&salesGrpType=50271&strOpt=reload&trendperiod= 3&trendtype = 50277&checkSelectedTrend=40020&checkSelectedTrend=40021&checkSelectedTrend=40022&trendDuration=50282&partNum='+encodeURIComponent(strPart));
    w1.attachURL(ajaxUrl,"Pack","resizable=yes,scrollbars=yes,top=250,left=250,width=960,height=510");
}

//The Function is used to Called When we Save Comments in Pending Item Request Screen.
function fnOpenOrdLog(strlnreqid,type){
	windowOpener("/GmCommonLogServlet?hType="+type+"&hID="+strlnreqid,"PrntInv", "resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
}

//PMT-6432 this function is for Consignment part by id button
function fnConsignmentByPart(){ 
	Vpartstr = fnPartString();
	if(Vpartstr==''){
		return false;
	}
	dhxWins = new dhtmlXWindows();
    dhxWins.enableAutoViewport(true);
    w1 = dhxWins.createWindow("w1", 30, 250, 880, 470);
    w1.setText(message_sales[108]);
    var ajaxUrl = fnAjaxURLAppendCmpInfo('/GmSalesConsignDetailsServlet?hAction=LoadPart&Cbo_Type=0&hPartNumber='+encodeURIComponent(Vpartstr));
    w1.attachURL(ajaxUrl,"Consignment","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
}

//PMT-6432 for part number validsation
function fnPartString(){
	var chkflag = '';
	var strPart = '';
	gridObj.forEachRow(function(rowId){
		chkflag = TRIM(gridObj.cellById(rowId,gridObj.getColIndexById("check")).getValue());
		partNo =  gridObj.cells(rowId, gridObj.getColIndexById("part_num")).getValue();
		if(chkflag == 1){
			strPart +=partNo +',';
		}
	});
	if(strPart == ''){
		Error_Details(message[5078]);
	}
	if (ErrorCount > 0){
		Error_Show();
	    Error_Clear();
		return false;
	}
return strPart;
}

function fnApprWithComments(){

	var reqname = document.frmLoanerRequestApproval.requestor_name.value;
	var checkbox_display = 'true';
	
	dhxWins = new dhtmlXWindows();
    dhxWins.enableAutoViewport(false);
    dhxWins.attachViewportTo(document.body);
    dhxWins.setImagePath("extweb/dhtmlx/dhtmlxWindows/imgs");
    
    w1 = dhxWins.createWindow("w1", 300, 150, 760, 500);
    var ajaxUrl = fnAjaxURLAppendCmpInfo('gmLoanerRequestApproval.do?&ramdomId='+Math.random()+'&strOpt=Approvalview&requestor_name='+reqname+'&chkbox_display='+checkbox_display);
    w1.attachURL(ajaxUrl,"ApprovalwithComments","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
   w1.setText(message_sales[109]);
   w1.setModal(true);
}
function fnCancel(){
	CloseDhtmlxWindow();
	return false;
}

function fnClose(){
	this.parent.fnLoad();
}

function fnAssignData(){
	document.frmLoanerRequestApproval.strReqIds.value = parent.document.frmLoanerRequestApproval.strReqIds.value;
	document.frmLoanerRequestApproval.strTxnType.value = parent.document.frmLoanerRequestApproval.strTxnType.value;
	document.frmLoanerRequestApproval.haction.value = parent.document.frmLoanerRequestApproval.haction.value;
	document.frmLoanerRequestApproval.hApproveInputString.value = parent.document.frmLoanerRequestApproval.hApproveInputString.value;
	document.frmLoanerRequestApproval.userType.value = parent.document.frmLoanerRequestApproval.userType.value;
	document.frmLoanerRequestApproval.raqtyString.value = parent.document.frmLoanerRequestApproval.raqtyString.value;
	document.frmLoanerRequestApproval.partString.value = parent.document.frmLoanerRequestApproval.partString.value;
}

/*Below function used to validaet the Email Ids entered in the UI.
Input Object - Input Object which has to be validaetd
delimiter - This is the Email ID delimiter in the Field. 
Errmessage field- This is the Field control to be dispalyed Error msg.
EMail ID Format: anystring@anystring.anystring */
function validateEmails(obj, delimiter, errField) {
	var objRegExp = /\S+@\S+\.\S+/;
	var toemail = obj.split(delimiter);
	var errMailIDs='';
	var errFl = false;
	for(var i=0;i<toemail.length;i++){
		toemail[i] = (toemail[i].replace(/^\s+/,'')).replace(/\s+$/,'');
		if(toemail[i] != '' && toemail[i].search(objRegExp) == -1){			
			errFl = true;
			errMailIDs =  errMailIDs + toemail[i] +' ,';
		}
	}
	if(errFl){
		var array=[errField,errMailIDs.substr(0,errMailIDs.length-1)]
		Error_Details(Error_Details_Trans(message_sales[103],array));
	}
}
// when we press enter key after giving Rule Group Id,it will load
function fnEnter(key){
	if(key.keyCode ==  13){
		fnLoad();
	}
}
