function checkIsNumeric(val) {

	   if( val.trim() == '' ) 
	   {
	      Error_Details(message[400]);
		  return false;
	   }


	   if( val.charAt(	val.length-1 )  == '%' )
	   	   val = val.substring(0, val.length-1 ) ; 
	   	   
	   
	   if (val != "" && isNaN(val))
    	{    	
    	  Error_Details(message[400]);
		  return false;
   		}
   		 
   		else {
   		    var str = val;
   		     if(str.indexOf(".") != -1) {
	   		    var subStr = str.substring(str.indexOf(".") +1);
	   		    if(subStr.length > 1)
	   		     {
	   		    	 Error_Details(message[402]);
		 			 return false;
	   		     }
	   		   }  
   		 }
	 
	 return true;

}
function isNumericPercent(val)
{
	
	//var regexp = /[0-9]*%|[0-9]*|[0-9]*.[0-9]%|[0-9]*.[0-9]/ ;
	
	//var regexp = /[0-9]*.[0-9]%$/ ;
	var regexp = /d+/g ;
	
	//[0-9]+%$//	
	
	
	if(regexp.test(val)) 
		return true ;
		
	else 
		return false;

}

function fnGo()
{
	var percentIncrease  ; 
	var option=document.frmAccount.hOpt.value;
	var queryby = document.frmAccount.Cbo_QueryBy.value;
	var accountName = document.frmAccount.Cbo_AccId.value;
	var accountId = TRIM(document.frmAccount.Txt_AccId.value);
	if( accountName == '' && accountId == '' )
		{
		Error_Details(message_sales[65]);
		}
	
	if( option == ("PriceIncrease"))
	{
		percentIncrease = document.frmAccount.Txt_PerIncrease.value ;
		 
    	if ( ! checkIsNumeric(percentIncrease) )
    	{  	
    		if (ErrorCount > 0)
		 	{
				Error_Show();
				Error_Clear();
				return false;
			}       	
    	}
	} 

	// accountType is Global variable (its come from PhoneOrder js file).
	if(type == 'Account' && accountType == '70114'){ // 70114 - ICS Account
		Error_Details(message_sales[66]);
	}
	
	var objProject = document.frmAccount.Chk_GrpProj;
	var setlen = objProject.length;
	var setstr = '';
	
	var temp = '';
		
	for (var i=0;i<setlen;i++)
	{
		if (objProject[i].checked == true)
		{
			setstr = setstr + objProject[i].value + ',';
		}
		document.frmAccount.hProjIds.value = setstr.substr(0,setstr.length-1);
	}

	if (ErrorCount > 0)
 	{
		Error_Show();
		Error_Clear();
		return false;
	}
	// When click load , hMode should not have value, need to add empty.
	document.frmAccount.Cbo_AccId.value = accountId;
	document.frmAccount.hMode.value = "";
	document.frmAccount.hAction.value = "Reload";
	//Progress bar enabled for Portal Loading also.
	//if(GLOBAL_ClientSysType=="IPAD"){
	fnStartProgress('Y');
	//}
	document.frmAccount.submit();
}

function fnPrint(valPrint)
{
  	var val    = document.frmAccount.hOpt.value;  
  	var acctId = document.frmAccount.Cbo_AccId.value ;
  
  	var objProject = document.frmAccount.Chk_GrpProj;
  	var setlen = objProject.length;
	var setstr = '';
	
	var temp = '';
		
	for (var i=0;i<setlen;i++)
	{
		if (objProject[i].checked == true)
		{
			setstr = setstr + objProject[i].value + ',';
		}
		document.frmAccount.hProjIds.value = setstr.substr(0,setstr.length-1);
	}
	
	
  
    var projIds = document.frmAccount.hProjIds.value ;
    val = "ACC_PRICE_XLS";
    var perIncrease = document.frmAccount.Txt_PerIncrease.value ;
  
   	windowOpener(strServletPath+'/GmSalesAcctPriceReportServlet?hAction=PRINT&hDTYPE='+valPrint+'&Cbo_AccId='+acctId+'&hProjIds='+projIds+'&Txt_PerIncrease='+perIncrease+'&hOpt='+val,"PI","resizable=yes,scrollbars=yes,top=0,left=200,width=1050,height=850");
}

// Marks the checkboxes as 'checked'
function fnCheckSelections(val,objgrp)
{
	var valobj = val.split(",");
	var sval = '';
	var arrlen = objgrp.length;
	
	if (arrlen > 0)
	{
		for (var j=0;j< valobj.length;j++ )
		{
			sval = valobj[j];
			for (var i=0;i< arrlen;i++ )
			{
				if (objgrp[i].value == sval)
				{
					objgrp[i].checked = true;
					break;
				}
			}
		}
	}
	else
	{
		objgrp.checked = true;
	}
}

function fnSelectAll(){
   var queryBy= document.frmAccount.Cbo_QueryBy.value;
   if(queryBy == 'Project Name')
	   {
	   totalProjects = projLen;	
	   }
   else
	   {
	   totalProjects = systemLen;	      
	   }
    var objgrp = document.frmAccount.Chk_GrpProj;
   if (totalProjects >0){
		for (var i=0;i< totalProjects;i++ )
		{
			objgrp[i].checked = document.frmAccount.Chk_selectAll.checked;	
		}
   }
}

function fnLoad()
{
	var projList = projectIds ;   
	fnCheckSelections(projList,document.frmAccount.Chk_GrpProj);
   	var hAction=document.frmAccount.hAction.value;
    var queryBy= document.frmAccount.Cbo_QueryBy.value;
    if(queryBy == 'Project Name')
 	   {
    	intSetLen = projLen; 	
 	   }
    else
 	   {
    	intSetLen = systemLen; 	   
 	   }   	
	if( hAction == ("Load")){
    totalProjects = intSetLen;
    var objgrp = document.frmAccount.Chk_GrpProj;
 	document.frmAccount.Chk_selectAll.checked = true;
	  if (totalProjects > 0 ){
			for (var i=0; i< totalProjects; i++ )
			{
				objgrp[i].checked = true;	
			}
	   }
	  }  
	fnSetDefaultValues();
}
/*
function fnHelp()
{
  windowOpener("/html/GmPrintHelp.pdf");	
}
*/
function fnCallAcIdBlur(obj){
	if (type == "Account") {	
		var accDropDownVal = document.frmAccount.Cbo_AccId.value;
		var accId = document.frmAccount.Txt_AccId.value;
		var accVal = obj.value;
		//When invalid numbers entered in text field, No need to fire AJAX call.
		if(isNaN(accVal)){
			Error_Details(message_sales[67]);
			Error_Show();	
			Error_Clear();
			document.all.Txt_AccId.value = '';
			return false;
		}
		if(accDropDownVal != accVal){
			fnAcIdBlur(obj);
			fnParentAcInfo(obj.value,enablePriceLabel);
		}
	}
	//When Txt AccId filed is not empty, zero that time only it should select the Account Dropdown.
	if( TRIM(obj.value) != '' && TRIM(obj.value) != '0'){
		document.frmAccount.Cbo_AccId.value = obj.value;
		fnCallAjax_txt(accId);
	}
}

function fnSetDefaultValues()
{	
	
	if (document.frmAccount.Cbo_AccId.value != 0)
	{
		
		document.frmAccount.Cbo_QueryBy.value = document.frmAccount.hQueryBy.value;
		if (type == "Account") {
			fnAcBlur(document.frmAccount.Cbo_AccId);
			fnParentAcInfo(document.frmAccount.Cbo_AccId.value,enablePriceLabel)
			
		}
		else{
			document.frmAccount.Txt_AccId.value = document.frmAccount.Cbo_AccId.value;
			
		}
	}
}

function fnEnter()
{
		if (event.keyCode == 13)
		{ 
			fnCallAcIdBlur(document.frmAccount.Txt_AccId);
			fnGo();
		}
}
//When click Download to Excel button , it will be called following function.
function fnExportExcel(){
	var accid = document.frmAccount.Cbo_AccId.value;
	var objProject = document.frmAccount.Chk_GrpProj;
	var setlen = objProject.length;
	var setstr = '';
	for (var i = 0; i < setlen; i++){
		if (objProject[i].checked == true){
			setstr = setstr + objProject[i].value + ',';
		}
		document.frmAccount.hProjIds.value = setstr.substr(0,setstr.length-1);
	}
	if(accid != '' && accid != undefined){
		document.frmAccount.hMode.value = "ACC_PRICE";
		document.frmAccount.hAction.value = "Reload";
		document.frmAccount.action = "/GmSalesAcctPriceReportServlet";
		document.frmAccount.submit();
	}
}



function fnCallType(object) {
	var val = object.value;
	var v = '<ul style="list-style-type: none; padding: 0; margin: 0;">';
	var v1 = '</ul';
	var all= '';
	if (val == 'Project Name') {
		for ( var i = 0; i < projLen; i++) {
			arr = eval("projArr" + i);
			arrobj = arr.split(",");
			id = arrobj[0];
			name = arrobj[1];
			all+= "<li><input class='RightInput' type='checkbox' name='Chk_GrpProj' id='' value="+id+">&nbsp;<span >"+name+"</span><BR></li>";
		}
		document.getElementById('Div_Proj').innerHTML = v+all+v1;
		
	} else {
		for ( var i = 0; i < systemLen; i++) {
			arr = eval("systemArr" + i);
			arrobj = arr.split(",");
			id = arrobj[0];
			name = arrobj[1];
			all+= "<li><input class='RightInput' type='checkbox' name='Chk_GrpProj' id='' value="+id+">&nbsp;<span >"+name+"</span><BR></li>";
		}
		document.getElementById('Div_Proj').innerHTML = v+all+v1;
	}
	document.frmAccount.Chk_selectAll.checked = 'True';
	fnSelectAll();
}

function fnAcIdBlur(obj){	
	if (obj.value != ''){	
		fnCallAjax('GETACC');
	}
}

function fnCallAjax(val){
	if(val =='GETACC'){
		accountType = '';
	}	
	var allParts = '';
	var accid = document.all.Txt_AccId.value;
	if(document.all.Txt_OrdId){
		var doid = document.all.Txt_OrdId.value;
	}
	// When submit in place order it will get the all parts from the below function and pass in ajax.
	if(window.frames.fmCart)
		allParts = window.frames.fmCart.fnCreateAllPartsWithCtrlNums();
	// The PhoneOrder.js file calling in many places. Need to be check undefined.
	var orderType = (document.frmPhOrder) ? document.frmPhOrder.Cbo_OrdType.value:'';
	var strShipTo = (document.all.shipTo) ? document.all.shipTo.value:'';
	var strName = (document.all.names) ? document.all.names.value:'';
	var strAddress = (document.all.addressid) ? document.all.addressid.value:'';	
    var url = fnAjaxURLAppendCmpInfo("/GmOrderAjaxServlet?orderType="+encodeURIComponent(orderType)+"&opt=" + encodeURIComponent(val)+ "&acc=" + encodeURIComponent(accid)+ "&rep="+encodeURIComponent(repid)+"&doid="+trim(doid)+ "&PartNums="+encodeURIComponent(allParts)+"&shipTo=" +strShipTo + "&names=" +  strName + "&addressid=" + strAddress);
	if (typeof XMLHttpRequest != "undefined"){
	   req = new XMLHttpRequest();
	} else if (window.ActiveXObject){
	   req = new ActiveXObject("Microsoft.XMLHTTP");
	}
	req.open("GET", url, true);
	if (val=='CHECKDOID'){             //MNTTASK-7099 -- For displaying the avail/exists images.
		req.onreadystatechange = showimage;
	}else if(val == 'validateControlNumber'){
		req.onreadystatechange = callCtrlNum;
	}else{
		req.onreadystatechange = callback;
	}
	req.send(null);
}
 
function callback() {
    if (req.readyState == 4) {
        if (req.status == 200) {
	        var xmlDoc = req.responseXML;
			parseMessage(xmlDoc);
        } else{
	        setErrMessage('<span class=RightTextBlue>Account does not exist</span>');
        }
    }
}

function parseMessage(xmlDoc) {
	var pnum = xmlDoc.getElementsByTagName("acc");	
	var cons = xmlDoc.getElementsByTagName("const");
	for (var x=0; x<pnum.length; x++){
			var rgname = parseXmlNode(pnum[x].childNodes[0].firstChild);		
			var adname = parseXmlNode(pnum[x].childNodes[1].firstChild);
			var dname = parseXmlNode(pnum[x].childNodes[2].firstChild);
			var trname = parseXmlNode(pnum[x].childNodes[3].firstChild);
			var rpname = parseXmlNode(pnum[x].childNodes[4].firstChild);
			repid = parseXmlNode(pnum[x].childNodes[5].firstChild);
			var prname = parseXmlNode(pnum[x].childNodes[6].firstChild);
			var prid = parseXmlNode(pnum[x].childNodes[7].firstChild);
			var gpocatid = parseXmlNode(pnum[x].childNodes[8].firstChild);
			var acname = parseXmlNode(pnum[x].childNodes[9].firstChild);
			accid = parseXmlNode(pnum[x].childNodes[10].firstChild);
			distid = parseXmlNode(pnum[x].childNodes[11].firstChild);
			vmode =  parseXmlNode(pnum[x].childNodes[12].firstChild);		
			rpname = rpname + ' ('+repid+')';
			//Account Affiliation
			var gpraff = parseXmlNode(pnum[x].childNodes[13].firstChild);
			var hlcaff =  parseXmlNode(pnum[x].childNodes[14].firstChild);
			// get the account type
			accountType = parseXmlNode(pnum[x].childNodes[15].firstChild);
			var contType =  '';
			
			if(pnum[x].childNodes[18]!=undefined && pnum[x].childNodes[18]!=null)
			{
				contType = parseXmlNode(pnum[x].childNodes[18].firstChild); 
			}
		
	}
	
	if (accid == ''){
		Error_Details(message_sales[68]);
		Error_Show();	
		Error_Clear();
		document.all.Txt_AccId.value = '';
		//when Txt AccId field is invalid, set the Account dropdwon as default.
		if(document.all.Cbo_AccId){
			document.all.Cbo_AccId.value = '0';
		}
		return false;
	}else{		
		setMessage(rgname,adname,dname,trname,rpname,prname,prid,gpocatid,acname,gpraff,hlcaff,contType);		
	}

}
 
function setMessage(rgname,adname,dname,trname,rpname,prname,prid,gpocatid,acname,gpraff,hlcaff,contType){
	var obj = eval("document.all.Lbl_Regn");
	obj.innerHTML = "&nbsp;" + rgname;

    obj = eval("document.all.Lbl_AdName");
    obj.innerHTML = "&nbsp;" + adname;

    obj = eval("document.all.Lbl_DName");
    obj.innerHTML = "&nbsp;" + dname;

    obj = eval("document.all.Lbl_TName");
    obj.innerHTML = "&nbsp;" + trname;

    obj = eval("document.all.Lbl_RName");
    obj.innerHTML = "&nbsp;" + rpname;

    obj = eval("document.all.Lbl_PName");
    obj.innerHTML = "&nbsp;" + prname;

    // Account Affiliation
    obj = eval("document.all.Lbl_GPRAffName");
    if(obj!=undefined && obj!=null)
    	obj.innerHTML = "&nbsp;" + gpraff;

    obj = eval("document.all.Lbl_HlthCrAffName");
    if(obj!=undefined && obj!=null)
    	obj.innerHTML = "&nbsp;" + hlcaff;   
    
    obj = eval("document.all.Lbl_ContractType");
    if(obj!=undefined && obj!=null)
    	obj.innerHTML = "&nbsp;" + contType;
    
    
    
	obj = document.all.tabAcSum;
	obj.style.display = 'block';
}

function fnCallAjax_txt(txt_AccId){
	var xmlDoc;
	var pnum;
 	var acname = '';
	var url = fnAjaxURLAppendCmpInfo("/GmOrderAjaxServlet?opt=GETACC" + "&acc=" + encodeURIComponent(txt_AccId)+'&ramdomId=' + Math.random());
	if (typeof XMLHttpRequest != "undefined") 
	{
	   reqacc = new XMLHttpRequest();
	} 
	else if (window.ActiveXObject) 
	{
		reqacc = new ActiveXObject("Microsoft.XMLHTTP");
	}
	reqacc.open("GET", url, true);
	
	reqacc.onreadystatechange = function(){
		if (reqacc.readyState == 4) {
	        if (reqacc.status == 200) {
		        xmlDoc = reqacc.responseXML;
		        pnum = xmlDoc.getElementsByTagName("acc");
		    	for (var x=0; x<pnum.length; x++){
		    		acname = parseXmlNode(pnum[x].childNodes[9].firstChild);
		    	}
		    	if(acname != null && acname != ''){
					document.frmAccount.searchCbo_AccId.value = acname; 
					document.frmAccount.hPartNum.focus();
				}else{
					// Need to throw validation if the account id is invalid
					document.frmAccount.searchCbo_AccId.value = '';
					Error_Details(message[5101]);
					Error_Show();
					Error_Clear();
					return false;
				}
	        }
	    }
	}
	reqacc.send(null);
}

function fnDisplayAccID(obj) {
	var accId = document.frmAccount.Cbo_AccId.value;
	if (accId != '') {
		document.frmAccount.Txt_AccId.value = accId;		
	} else {		
		document.frmAccount.Txt_AccId.value = '';
	}
}
