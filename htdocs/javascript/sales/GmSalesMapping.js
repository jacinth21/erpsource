function fnSubmit() {
	// if (fnValidate() != false) {
	if (document.getElementById("regName").value == "") {
		Error_Details(message_sales[63]);
	} else if (document.getElementById("zoneType").value == "0") {
		Error_Details(message_sales[64]);
	} else if (document.getElementById("companyType").value == "0") {
		Error_Details(message_sales[434]);
	}  else if (document.getElementById("divisionType").value == "0") {
		Error_Details(message_sales[435]);
	}  else if (document.getElementById("countryType").value == "0") {
		Error_Details(message_sales[436]);
	}  
	
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	else {
		var url = "/gmRegionMappingAction.do";
		document.frmRegionSetupForm.divisionType.disabled=false;
		document.frmRegionSetupForm.strOpt.value="Save";
		document.getElementById("form").action = url;
		fnStartProgress();
		
		if(document.frmRegionSetupForm.newADFL.checked){
			document.frmRegionSetupForm.newADFL.value = "Y";
		}
		if(document.frmRegionSetupForm.newVPFL.checked){
			document.frmRegionSetupForm.newVPFL.value = "Y";
		}

		document.getElementById("form").submit();
	}
}

function fnDisable()
{
	var frm=document.frmRegionSetupForm;
	var str=frm.reg_id.value;
	frm.adPlus.value = '0';
	
	if(str!=''){
		frm.regName.disabled=true;
	}
	if (document.frmRegionSetupForm.newADFL.value == 'Y') {
		document.frmRegionSetupForm.adType.disabled = true;
	}
	if (document.frmRegionSetupForm.newVPFL.value == 'Y') {
		document.frmRegionSetupForm.vpID.disabled = true;
	}
}

function fnOnPageLoad() {
	
	if(gridObjData!="")
	{
		   	mygrid = initGrid('dataGridDiv',gridObjData);
		   	mygrid.attachHeader('#rspan,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter');
	}
}

function fnEdit(str){
	console.log("fnSubmit");
	var url = "/gmRegionMappingAction.do?reg_id="+str;
	document.frmRegionSetupForm.strOpt.value="Edit";
	document.getElementById("form").action = url;
	document.getElementById("form").submit();
}

function fnVoid(region_id){
	document.getElementById("form").action="/GmCommonCancelServlet?hTxnName="+region_id + "&hCancelType=VREGN&hTxnId="+region_id;
	document.getElementById("form").submit();
}

function fnNewADUser(obj) {// Create Open Ad Disabled User field
	if (obj.checked) {
		document.frmRegionSetupForm.adType.disabled = true;
		document.frmRegionSetupForm.newADFL.value = 'Y';
	} else {
		document.frmRegionSetupForm.adType.disabled = false;
		document.frmRegionSetupForm.newADFL.value = '';
	}
}

function fnNewVPUser(obj) {// Create Open Ad Disabled User field
	if (obj.checked) {
		document.frmRegionSetupForm.vpID.disabled = true;
		document.frmRegionSetupForm.newVPFL.value = 'Y';
	} else {
		document.frmRegionSetupForm.vpID.disabled = false;
		document.frmRegionSetupForm.newVPFL.value = '';
	}
}

