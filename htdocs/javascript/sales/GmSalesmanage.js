function fnReplaceAD() {

	// Fetch the form value and pass it to the action.

	var zone = document.frmSalesManageForm.zoneid.value;
	var region = document.frmSalesManageForm.regionid.value;
	var user = document.frmSalesManageForm.newadid.value;
	fnValidateDropDn('zoneid', message_sales[404]);
	fnValidateDropDn('regionid', message_sales[405]);
	fnValidateDropDn('newadid', message_sales[406]);

	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}

	var frm = document.frmSalesManageForm;
	frm.action = "/gmSalesManage.do?";
	frm.strOpt.value = "ReplaceAD";
	if (frm.Create_OpenAD.checked) {
		frm.hAction.value = "createad";
		frm.hRegionNm.value = frm.regionid.options[frm.regionid.selectedIndex].text;
		frm.hZoneNm.value = frm.zoneid.options[frm.zoneid.selectedIndex].text;
	} else {
		frm.hAction.value = "save";
		frm.hRegionNm.value = frm.regionid.options[frm.regionid.selectedIndex].text;
		frm.hZoneNm.value = frm.zoneid.options[frm.zoneid.selectedIndex].text;
		frm.hUserNm.value = frm.newadid.options[frm.newadid.selectedIndex].text;
	}

	frm.submit();

}

function fnShowHideUser(obj) {// Create Open Ad Disabled User field
	if (obj.checked) {
		document.frmSalesManageForm.newadid.disabled = true;
		var checkedFl = "Y";
		document.frmSalesManageForm.checkedFL.value = checkedFl;

	} else {
		document.frmSalesManageForm.newadid.disabled = false;
	}
}

function fnChangeAcctType() {
	var zoneid = document.frmSalesManageForm.zoneid.value;
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmSalesManage.do?strOpt=ReplaceAD&hAction=fetchRegion&method=fetchRegionList&zoneid='
			+ zoneid + '&randomId=' + Math.random());
	dhtmlxAjax.get(ajaxUrl, fnSetChangeAcctType);
}

/*
 * This function used to Populate the Dealer infromation (Using AJAX)
 */
function fnSetChangeAcctType(loader) {
	var resJSONObj = $.parseJSON(loader.xmlDoc.responseText);
	var obj = document.frmSalesManageForm.regionid;
	fnPopulateOptions(obj, resJSONObj, "ADID", "REGNAME", "");
}

function fnShowDropdown() {// Checked Flag
	var checkedFl = "N";
	document.frmSalesManageForm.checkedFL.value = checkedFl;
}
