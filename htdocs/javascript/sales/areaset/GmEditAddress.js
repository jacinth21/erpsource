var addid;
function SetDefaultValues(){ 
	//var add = '';
	document.all.shipTo.value=shipType;
	document.all.chkNames.disabled = false;
	var boxes=document.all.shipTo;
	for (var i = 0; i < boxes.length; i++){
		//enabling only for distributor and sales rep
		if (boxes[i].value !='4120' && boxes[i].value !='4121' && boxes[i].value != '4122')
		{
			document.all.shipTo[i].disabled=true;
		}
	}
	
	addid = addID;
	//document.all.names.value= repID;
	//fnGetAddressList(document.all.names,addID);
	fnGetNames(document.all.shipTo);
	if(document.all.shipTo.value == '4120'){
		document.all.names.disabled=true;
	}
	if(haction == 'reload'){
		   var vParentFrm = document.frmAreaSet.parentFrmName.value;
		   
		   if(vParentFrm == 'frmCasePost' ){
			   eval('parent.window.document.'+vParentFrm).submit(); 
		   }else{
			   parent.window.history.go(0);
		   }
		   parent.dhxWins.window("w1").close();
		   return false;
		}
}
function fnGo(){
	
	fnValidateDropDn('shipTo',' ShipTo');
	fnValidateDropDn('names',' Names');
	fnValidateDropDn('shipMode',' Mode');
	
	var newRepID = document.all.names.value;
	var newShipType = document.all.shipTo.value;
	var newAdd = document.all.addressid.value;
	var appfl =  document.all.chkAppFl.checked;
	if(newShipType == '4121'){//sales rep address mandatory
		fnValidateDropDn('addressid',' Address');
	}
	if(appfl==true){
		appfl = 'Y';
	}else{
		appfl ='N';
	}
		
	if(newShipType!='4120' && newShipType!='4121' && newShipType!='4122'){
		
		Error_Details(Error_Details_Trans(message_sales[204],document.all.shipTo.options[document.all.shipTo.selectedIndex].text));
	}	
	
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	
	if(oldAddID!=null&&oldAddID!='')
	{
		addID=oldAddID;
	}
    document.frmAreaSet.action = "/gmAreaSetShipping.do?method=SaveAddress&addID="+addID+"&repID="+repID+"&refID="+refID+"&newAddID="+newAdd+"&strOpt="+strOpt+"&shipType="+shipType+"&newRepID="+newRepID+"&newShipType="+newShipType+"&parentFrmName=frmCasePost&chkAppFl=" + appfl + "&randomId=" + Math.random();
    document.frmAreaSet.submit();
	
}
function fnClose(){
	   CloseDhtmlxWindow();
	   return false;
}

function fnAreasetOpenAddress()
{
	fnValidateDropDn('shipTo',' ShipTo');
	fnValidateDropDn('names',' Names');

	shipto = document.all.shipTo.value;
	shiptoid = document.all.names.value;
	shiptoidid = document.all.names.options[document.all.names.selectedIndex].id;
	shipmode = document.all.shipMode.value;
	
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	if (shipto == '4121' && shiptoid !='0')
	{
		var hAddredId =document.all.hAddrId.value;
		document.frmAreaSet.action="/GmPageControllerServlet?strPgToLoad=gmAddressSetup.do&method=loadAddressSetup&includedPage=false&transactPage=areaset&partyId="+shiptoidid+"&repID="+shiptoid+"&refID="+refID+"&fromStrOpt="+strOpt+"&oldAddID="+addID+"&shipMode="+shipmode+"&caseDistID="+distid+"&caseRepID="+repid+"&caseAccID="+accid+"&hAddrId="+hAddredId;
		
	}
	else { // only pass the shipto and shiptoid
		//document.frmAreaSet.action="/GmSearchServlet?hAction=getAddress&hShip="+shipto+"&hShipId="+shiptoid;
		// code is added by Hreddi for showing the primary Address Details for the particular Distributor or Hospital in new popUp window.
		//w1 = createDhtmlxWindow(300,200);
		dhxWins = new dhtmlXWindows();
	    dhxWins.enableAutoViewport(true);
		w1 = dhxWins.createWindow("w1",200, 170,300,200);
		w1.button("park").hide();
		w1.button("minmax1").hide();
	    w1.setText("Address Lookup"); 
		w1.attachURL("/GmSearchServlet?hAction=getAddress&hShip="+shipto+"&hShipId="+shiptoid);	   
	    return false;
	}
	document.frmAreaSet.submit();
}
