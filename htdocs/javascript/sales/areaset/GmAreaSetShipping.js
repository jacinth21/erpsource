



function fnMoreInfo(detailInfo) {
	fnValidate();
	
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	
	var status = document.frmAreaSet.status.value;
	var tagFrom = document.frmAreaSet.tagFrom.value;
	var tagTo = document.frmAreaSet.tagTo.value;
	var carrier = document.frmAreaSet.carrier.value;
	var actualStatus = document.frmAreaSet.actualStatus.value;
	var dates = document.frmAreaSet.dates.value;
	var txtFromDate = document.frmAreaSet.txtFromDate.value;
	var txtToDate = document.frmAreaSet.txtToDate.value;
	
	var detail=detailInfo.split("~");
	
    w1 = createDhtmlxWindow();
    w1.setText(message_sales[200]); 
	w1.attachURL("/gmAreaSetShipping.do?method=moreInfo&strOpt="+detail[0]+"&shipID="+detail[1]+"&refID="+detail[2]+"&status="+status+"&tagFrom="+tagFrom+"&tagTo="+tagTo+"&carrier="+carrier+"&actualStatus="+actualStatus+"&dates="+dates+"&txtFromDate="+txtFromDate+"&txtToDate="+txtToDate+"&ramdomId="+Math.random());
	return false;
}

function fnClose(){
	   parent.window.fnLoad();
	   parent.dhxWins.window("w1").close();
	   return false;
}
function fnShipTo(){
	   var inputString="";
	   var stropt =document.frmAreaSet.strOpt.value; 
	   inputString=fnGetCheckedValues();
	   if (ErrorCount > 0)
		{
			Error_Show();
			Error_Clear();
			return false;
		}
	   w1 = createDhtmlxWindow();
	    w1.setText(message_sales[201]); 
		w1.attachURL( "/gmAreaSetShipping.do?method=shipTo&strOpt="+stropt+"&hTxnVal="+inputString+"&ramdomId="+Math.random());
		return false;
	   
}

function popitup(tagId) {
	
			
		w1 = createDhtmlxWindow();
	    w1.setText(message_sales[202]); 
		w1.attachURL("/GmSetImgMgtServlet?TAGID="+tagId+"&hAction=View&ramdomId="+Math.random());
		return false;
	
}



function fnStatusUpdate(status){
	 var frm=document.frmAreaSet;
	 var inputString="";
	

	 inputString=fnGetCheckedValues();
	
	
	 if(inputString.length>4000)
	 {
		 Error_Details(message_sales[1]);	
	 }
		 
	 	if (ErrorCount > 0)
		{
			Error_Show();
			Error_Clear();
			return false;
		}

		var flag=confirm(Error_Details_Trans(message_sales[2],status),message_sales[3],message_sales[4]);
		if(flag==false)
		{
			return false;
		}else{
     	
	 	document.frmAreaSet.hTxnVal.value = inputString;
         document.frmAreaSet.action='/gmAreaSetShipping.do?method=status';
         document.frmAreaSet.submit();
		}
		 
	return false;
		 
	
}

function fnGetCheckedValues()
{
	 var frm=document.frmAreaSet;
	 var chkdvals=""; ;
	 var unchkdvals ="";
	 var chk=0;	
	 var notchk=0;
	
	
	
	 for (var i=0;i<TotalRows ;i++ )
		{	
			obj = eval("document.frmAreaSet.Chk_tagid"+i);	
			
			if(obj!=null)
				{
				if (obj.checked == true){
					chkdvals += obj.value+'^';	
					obj = eval("document.frmAreaSet.Hdn_shipid"+i);
					chkdvals += obj.value+'^';
					obj = eval("document.frmAreaSet.Hdn_refid"+i);
					chkdvals += obj.value+'|';
						chk ++;			
					}
				else if(obj.checked != true){
					unchkdvals += obj.value+','	;
					notchk ++;
				}	
				}
	 }

	 if(chk<=0)
	 {
		 Error_Details(message_sales[5]);
	 }

	 return chkdvals;
	 	
}

function fnReject(){
	
	   var inputString="";
	   var stropt =document.frmAreaSet.strOpt.value; 
	   inputString=fnRejectCheckedValues();
	   if (ErrorCount > 0)
		{
			Error_Show();
			Error_Clear();
			return false;
		}
	   
	   var caseid =document.frmAreaSet.caseID.value; 
	  
	   w1 = createDhtmlxWindow();
	    w1.setText(message_sales[203]); 
		w1.attachURL( "/gmAreaSetShipping.do?method=reject&strOpt="+stropt+"&hTxnVal="+inputString+"&caseID="+caseid+"&ramdomId="+Math.random());
		return false;
	   
}

function fnRejectCheckedValues()
{
	 var frm=document.frmAreaSet;
	 var chkdvals=""; ;
	 var unchkdvals ="";
	 var chk=0;	
	 var notchk=0;
	 var status=true;
	 var precaseid="";
	 var caseid="";
	
	
	 for (var i=0;i<TotalRows ;i++ )
		{	
			obj = eval("document.frmAreaSet.Chk_tagid"+i);	
			
			if(obj!=null)
				{
				if (obj.checked == true){
					chkdvals += obj.value+'^';	
					obj = eval("document.frmAreaSet.Hdn_shipid"+i);
					chkdvals += obj.value+'^';
					obj = eval("document.frmAreaSet.Hdn_refid"+i);
					chkdvals += obj.value+'^';
					obj = eval("document.frmAreaSet.Hdn_caseid"+i);
					caseid = obj.value;
					if(precaseid != caseid && chk > 0)
					{
						status=false;
					}
					obj = eval("document.frmAreaSet.Hdn_shipfromid"+i);
					chkdvals += obj.value+'|';
						chk ++;	
					precaseid=caseid;
					}
				else if(obj.checked != true){
					unchkdvals += obj.value+','	;
					notchk ++;
				}	
				}
	 }
	 
	 if(chk<=0)
	 {
		 Error_Details(message_sales[5]);
	 }
	 if(!status)
	 {
		 Error_Details(message_sales[6]);
	 }else
	 {
		 frm.caseID.value=caseid;
	 }
		 

	 return chkdvals;
	 	
}




function fnValidate()
{
	var frm=document.frmAreaSet
	var dates=frm.dates.value;
	var fromDate=frm.txtFromDate.value;
 	var toDate=frm.txtToDate.value;
 	var tagIdFrm=document.frmAreaSet.tagFrom.value;
	var tagIdTo=document.frmAreaSet.tagTo.value;
	
		if(dates != '0')
		{
		 		 
		 if(fromDate =='' || toDate =='')
		 {
	 		Error_Details(message_sales[7]);
		 }else
		 {				 
		  var date1 = new Date(fromDate); 
		  var date2 = new Date(toDate); 
		 	if(date1 > date2) 
		    { 
			 Error_Details(message_sales[8]); 
		    }
		 }

		
		}else
		{
			if(fromDate !='' || toDate !='')
			 {
		 		Error_Details(message_sales[9]);
			 }
		}
		
		
		if((tagIdFrm == '' && tagIdTo != '') || ( tagIdFrm != '' && tagIdTo == '') || (parseInt(eval(tagIdFrm)) > parseInt(eval(tagIdTo)))){
			Error_Details(message_sales[10]);
		} 
			
	return false;
}

function isNumberKey(evt)
{
   var charCode = (evt.which) ? evt.which : event.keyCode
   if (charCode > 31 && (charCode < 48 || charCode > 57))
      return false;

   return true;
}


function fnLoad() {
	fnValidate();
		
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	document.frmAreaSet.action='/gmAreaSetShipping.do?method=load';
	document.frmAreaSet.submit();
	return false;
}



