// javascript\sales\GmSalesEfficiencyScoreCardByLoanerReport.js

//This function is onPageLoad used to Ajax call used fetch exp. Loaner usage report
function fnOnPageLoad(){
	// to get the drill down values
	var systemIdVal = document.frmSalesEfficiencyScoreCardByLoaner.systemId.value;
	var distIdVal = document.frmSalesEfficiencyScoreCardByLoaner.distributorId.value;
	var adIdVal = document.frmSalesEfficiencyScoreCardByLoaner.adId.value;
	
	// to pass the drilldown values
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmSalesEfficiencyScoreCardByLoaner.do?method=fetchLoanerUsageScoreCardDtls&systemId='
			+ systemIdVal
			+ '&distributorId='
			+ distIdVal
			+ '&adId='
			+ adIdVal
			+ '&ramdomId=' + Math.random());
	
	var loader = dhtmlxAjax.getSync(ajaxUrl);
	fnLoadScoreCardByConsCallback(loader);
}

//This function used to load the SP details to grid.
function fnLoadScoreCardByConsCallback(loader) {
	response = loader.xmlDoc.responseText;
	if (response !=''){
		var setInitWidths = "100,100,100,60,100,*,80,120,120,60,80,60,60,60,50,50,70,70";
		var setColAlign = "left,left,left,left,left,left,left,left,left,left,left,left,center,center,right,right,right,right";
		var setColTypes = "ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ron,ron,ron,ron";
		var setColSorting = "str,str,str,str,str,str,str,str,str,str,str,str,date,date,int,int,int,int";
		var setHeader = [ "AD", "Field Sales", "System", "Type", "Set Id", "Set Name", "Loaner Id", 
		                  "Etch Id",  "Consignment ID", "Rep", "Assoc. Rep", "Status", "Placement Date", "Return Date",
		                  "Loaner Cmpl", "Loaner Last 2 Mo.", "Loaner Proj", "Loaner Tot Proj"];
		
		var setFilter = [ "#select_filter", "#select_filter", "#select_filter","#select_filter","#text_filter","#select_filter","#text_filter",
				          "#text_filter", "#text_filter", "#select_filter","#select_filter", "#select_filter","#text_filter", "#text_filter",
				          "#numeric_filter","#numeric_filter","#numeric_filter","#numeric_filter" ];
		

		var setColIds = "ad_name,dist_name,system_name,system_type_name,set_id,set_name,"
						+ "loaner_id,etch_id,cn_id,rep_name,ass_rep_name,status,placement_date,return_date,ln_cmpl,ln_last_2_mon,ln_proj,ln_tot_proj";
			
		var enableTooltips = "true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true";
		var gridHeight = "600";

		// to set auto height
		if($(document).height() !=undefined || $(document).height() !=null){
			gridHeight =  $(document).height() - 120 ;
		}
		
		// showing footer total 
		var footerArry=new Array();	
		footerArry[0]="";
		footerArry[1]="";
		footerArry[2]="";
		footerArry[3]="";
		footerArry[4]="";
		footerArry[5]="";
		footerArry[6]="";
		footerArry[7]="";
		footerArry[8]="";
		footerArry[9]="";
		footerArry[10]="";
		footerArry[11]="";
		footerArry[12]="";
		footerArry[13]="<b>Total<b>";
		footerArry[14]="<b>{#stat_total}<b>";
		footerArry[15]="<b>{#stat_total}<b>";
		footerArry[16]="<b>{#stat_total}<b>";
		footerArry[17]="<b>{#stat_total}<b>";
		
		var format = ""
		var dataHost = {};
		var footerStyles = [];

		document.getElementById("DivNothingMessage").style.display = 'none';

		// split the functions to avoid multiple parameters passing in single
		// function
		gridObj = initGridObject('dataGridDiv');
		//gridObj.startFastOperations();
		// report taking long  time to load - comment below code
		//gridObj.enableDistributedParsing(true); 
		
		gridObj = fnCustomTypes(gridObj, setHeader, setColIds, setInitWidths,
				setColAlign, setColTypes, setColSorting, enableTooltips,
				setFilter, gridHeight, format,footerArry,footerStyles);
		// to set the decimal format
		gridObj.setNumberFormat("0,000.0",14);
		gridObj.setNumberFormat("0,000.0",15);
		gridObj.setNumberFormat("0,000.0",16);
		gridObj.setNumberFormat("0,000.0",17);

		//PC-3528 to enable distribute parsing
		gridObj.enableDistributedParsing(true);
		
		gridObj = loadDhtmlxGridByJsonData(gridObj, response);
		
		//gridObj.stopFastOperations();
		
		// freezes 4 columns
		//gridObj.splitAt(3);
		
	   } else {
		document.getElementById("dataGridDiv").style.display = 'none';
		document.getElementById("DivNothingMessage").style.display = 'block';
		document.getElementById("DivExportExcel").style.display = 'none';
	}
}


//This Function Used to set custom types conditions in Grid
function fnCustomTypes(gObj, setHeader, setColIds, setInitWidths, setColAlign,
		setColTypes, setColSorting, enableTooltips, setFilter, gridHeight,
		format,footerArry,footerStyles) {
		gObj.setHeader(setHeader);
		var colCount = setHeader.length;
	
		gObj.setInitWidths(setInitWidths);
		gObj.setColAlign(setColAlign);
		gObj.setColTypes(setColTypes);
		gObj.setColSorting(setColSorting);
		gObj.enableTooltips(enableTooltips);
		gObj.attachHeader(setFilter);
		gObj.setColumnIds(setColIds);
	    if (footerArry != undefined && footerArry.length > 0) {
			var footstr = eval(footerArry).join(",");
			if (footerStyles != undefined && footerStyles.length > 0)
			gObj.attachFooter(footstr, footerStyles);
			else
			gObj.attachFooter(footstr);
			}
		gObj.enableAutoHeight(true, gridHeight, true);

	return gObj;
}

//This function used to download the score details to excel file
function fnExportDtls() {
	gridObj.toExcel('/phpapp/excel/generate.php');
}

