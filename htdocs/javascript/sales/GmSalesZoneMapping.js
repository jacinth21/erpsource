function fnSubmit(){
	var frm=document.frmSalesMapping;
	fnValidateTxtFld('zoneNM',message_sales[62]); 
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	frm.zoneNM.disabled=false;
	frm.strOpt.value='save';
	fnStartProgress();
	frm.submit();	
}

function fnOnPageLoad(){
	if (objGridData != '')
	{
		mygrid = initGrid('mygrid_container',objGridData);
	 	mygrid.attachHeader('#rspan,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter');
	}
}

function fnDisable(){
	var frm=document.frmSalesMapping;
	var str=frm.zoneID.value;
	if(str!=''){
		frm.zoneNM.disabled=true;
	}
}
function fnEdit(id){
	var frm=document.frmSalesMapping;
	frm.zoneID.value=id;
	frm.strOpt.value='edit';
	frm.submit();
	
}

function fnVoid()
{
	 var frm = document.frmSalesMapping; 
	 var zone_id = frm.zoneID.value;
	 frm.action = "/GmCommonCancelServlet?hTxnName="+zone_id+"&hCancelType=VZONE&hTxnId="+zone_id;
	 frm.submit();
}