// javascript\sales\GmSalesEfficiencyScoreCardReport.js

var strEffiCompletedId = '';
var strEffiProjId = '';
var strEffiScoreCompletedId = '';
var strEffiScoreProjId = '';
//
var strProcedureCompId = '';
var strProcedureProjId = '';
var strUsageCompledId = '';
var strUsageCompledProjId = '';
// 
var strCNProjId = '';
var strLNProjId = '';

var myTab = "";
var allowedTag = "";

// to display dynamic images
var dril_down_1_images = '';
var dril_down_2_images = '';

// PC-5101: Scorecard DO classification changes
function fnChangeAllowedTags() {
	fnOnPageLoad(screenTypeVal);
}

// This function is onPageLoad used to Ajax call used fetch diff. Screen Name
function fnOnPageLoad(screenName){
	var ajaxUrl = '';
	// to get the values from hidden field
	
	var hDistVal= document.frmSalesEfficiencyScoreCard.distId.value;
	var hSystemVal= document.frmSalesEfficiencyScoreCard.systemId.value;
	var hAdVal= document.frmSalesEfficiencyScoreCard.adId.value;
	
	// Added allowed tag field according to PC-5101: Scorecard DO classification changes
	allowedTag = document.getElementById("allowedTag").value;
	
	if(screenName =='SYSTEM'){
		  // to reset the value	
		  document.frmSalesEfficiencyScoreCard.systemId.value = '';	

		  // Added allowed tag field according to PC-5101: Scorecard DO classification changes
		  ajaxUrl = fnAjaxURLAppendCmpInfo('/gmSalesEfficiencyScoreCard.do?method=fetchSystemScoreCardDtls&distId='+hDistVal+'&adId='+hAdVal+'&systemId=&drillDownName='+hDrillDownVal +'&ramdomId=' + Math.random() + '&allowedTag=' + allowedTag);
	}else if(screenName =='FS'){
		  // to reset the value	
		  document.frmSalesEfficiencyScoreCard.distId.value = '';

		  // Added allowed tag field according to PC-5101: Scorecard DO classification changes
		  ajaxUrl = fnAjaxURLAppendCmpInfo('/gmSalesEfficiencyScoreCard.do?method=fetchFieldSalesScoreCardDtls&systemId='+hSystemVal+'&adId='+hAdVal+'&distId=&drillDownName='+hDrillDownVal+'&ramdomId=' + Math.random() + '&allowedTag=' + allowedTag);
	}else if(screenName =='AD'){
		  // to reset the value	
		  document.frmSalesEfficiencyScoreCard.adId.value = '';
		  
		  // Added allowed tag field according to PC-5101: Scorecard DO classification changes
		  ajaxUrl = fnAjaxURLAppendCmpInfo('/gmSalesEfficiencyScoreCard.do?method=fetchADScoreCardDtls&systemId='+hSystemVal+'&distId='+hDistVal+'&adId=&drillDownName='+hDrillDownVal+'&ramdomId=' + Math.random() + '&allowedTag=' + allowedTag);
	}
	
	var loader = dhtmlxAjax.getSync(ajaxUrl);
	fnLoadScoreCardCallback(loader);
}

//This function used to load the SP details to grid.
function fnLoadScoreCardCallback(loader) {
	response = loader.xmlDoc.responseText;
	if (response !=''){
		var setInitWidths = "20,20,20,20,20,*,50,60,70,70,50,70,60,70,70,80,80,70,70";
		
		var setColAlign = "center,center,center,center,center,left,right,right,right,right,right,right,right,right,right,right,right,right,right";
		
		var setColTypes = "drill_icon_1,drill_icon_2,cn_drilldown_icon,ln_drilldown_icon,actual_drilldown_icon,ro,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron";
		
		var setColSorting = "na,na,na,na,na,str,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int";
		
		
		var setHeader = [ "", "", "", "", "", "Name", "Sys Cnt", 
				"Cons Exp Usage", "Cons Exp Usage Proj", 
				"Loaner Exp Usage", "Loaner Exp Usage Proj",
				"Total Exp Usage", "Total Exp Usage Proj", "Proc Cmpl",
				"Proc Cmpl Proj", "Eff Score",
				"Eff Score Proj", "Eff %","Eff % Proj" ];
		
		var setFilter = ["#rspan","#rspan", "#rspan", "#rspan", "#rspan", "#text_filter",
				"#numeric_filter", "#numeric_filter",
				 "#numeric_filter", "#numeric_filter",
				"#numeric_filter", "#numeric_filter",
				"#numeric_filter", "#numeric_filter",
				"#numeric_filter", "#numeric_filter",
				"#numeric_filter", "#numeric_filter",
				"#numeric_filter"];
		
		// reconGridObj.attachHeader('#rspan,#rspan,#rspan,#rspan,Writeoff,Return
		// to IH');
		var setColIds = "id,id,id,id,id,name,system_count,cn_completed,cn_tot_proj,ln_completed,ln_tot_proj"
				+ ",exp_usage_completed,exp_usage_proj,proc_completed,proc_tot_proj,effi_completed,effi_proj,effi_score_completed"
				+ ",effi_score_proj";
		
		var enableTooltips = "true,true,true,true,true,true,true,true,true";
		var gridHeight = "600";
		var format = ""
		var dataHost = {};

		// to set auto height
		if($(document).height() !=undefined || $(document).height() !=null){
			gridHeight =  $(document).height() - 160 ;
		}

		// showing footer total 
		var footerArry=new Array();	
		footerArry[0]="";
		footerArry[1]="";
		footerArry[2]="";
		footerArry[3]="";
		footerArry[4]="";
		footerArry[5]="<b>Total<b>";
		footerArry[6]="<b>{#stat_total}<b>";
		footerArry[7]="<b>{#stat_total}<b>";
		footerArry[8]="<b>{#stat_total}<b>";
		footerArry[9]="<b>{#stat_total}<b>";
		footerArry[10]="<b>{#stat_total}<b>";
		footerArry[11]="<b>{#stat_total}<b>";
		footerArry[12]="<b>{#stat_total}<b>";
		footerArry[13]="<b>{#stat_total}<b>";
		footerArry[14]="<b>{#stat_total}<b>";
		footerArry[15]="<b>{#stat_total}<b>";
		footerArry[16]="<b>{#stat_total}<b>";
		footerArry[17]="<b>{#stat_total}<b>";
		footerArry[18]="<b>{#stat_total}<b>";
		
		
		var footerStyles = [];
		
		document.getElementById("DivNothingMessage").style.display = 'none';

		// split the functions to avoid multiple parameters passing in single
		// function
		gridObj = initGridObject('dataGridDiv');
		gridObj = fnCustomTypes(gridObj, setHeader, setColIds, setInitWidths,
				setColAlign, setColTypes, setColSorting, enableTooltips,
				setFilter, gridHeight, format);
		// footer added
		gridObj.attachFooter(footerArry, footerStyles);
		// number format
		gridObj.setNumberFormat("0,000",6);
		gridObj.setNumberFormat("0,000.0",7);
		
		// to set the decimal format
		gridObj.setNumberFormat("0,000.0",8);
		gridObj.setNumberFormat("0,000.0",9);
		gridObj.setNumberFormat("0,000.0",10); // LN Proj.
		gridObj.setNumberFormat("0,000.0",11);
		gridObj.setNumberFormat("0,000.0",12); // Total Proj.
		gridObj.setNumberFormat("0,000.0",13);
		gridObj.setNumberFormat("0,000.0",14); // Procedure Proj.
		gridObj.setNumberFormat("0,000.0",15);
		gridObj.setNumberFormat("0,000.0",16);
		// Procedure - only int values
		gridObj.setNumberFormat("0,000.0",17);
		gridObj.setNumberFormat("0,000.0",18);
		
		//PC-3528 to enable distribute parsing
		// to disable the parsing (footer values not calculated)
		//gridObj.enableDistributedParsing(true);
		
		gridObj = loadDhtmlxGridByJsonData(gridObj, response);
		
		// negative sign check numeric fields 
		var strEffiCompletedVal = "";
		var strEffiProjVal ="";
		var strEffiScoreCompletedVal ="";
		var strEffiScoreProjVal ="";
		
		
        strEffiCompletedId  = gridObj.getColIndexById("effi_completed");
        strEffiProjId = gridObj.getColIndexById("effi_proj");
        strEffiScoreCompletedId = gridObj.getColIndexById("effi_score_completed");
        strEffiScoreProjId = gridObj.getColIndexById("effi_score_proj");
        //
        strProcedureCompId = gridObj.getColIndexById("proc_completed");
        strProcedureProjId = gridObj.getColIndexById("proc_tot_proj");
        strUsageCompledId = gridObj.getColIndexById("exp_usage_completed");
        strUsageCompledProjId = gridObj.getColIndexById("exp_usage_proj");
        // 
        strCNProjId = gridObj.getColIndexById("cn_tot_proj");
        strLNProjId = gridObj.getColIndexById("ln_tot_proj");
        
     // freezes 4 columns
		//gridObj.splitAt(4);
		
             gridObj.forEachRow(function(rowId) {
            	 // eff.
            	 strEffiCompletedVal = gridObj.cellById(rowId, strEffiCompletedId).getValue();
            	 strEffiProjVal = gridObj.cellById(rowId, strEffiProjId).getValue();
            	 // eff. score
            	 strEffiScoreCompletedVal = gridObj.cellById(rowId, strEffiScoreCompletedId).getValue();
            	 strEffiScoreProjVal = gridObj.cellById(rowId, strEffiScoreProjId).getValue();
            	 
            	
                 fnNegativeNumberUpd(strEffiCompletedVal, rowId, strEffiCompletedId);

                 fnNegativeNumberUpd(strEffiProjVal, rowId, strEffiProjId);

                 fnNegativeNumberUpd(strEffiScoreCompletedVal, rowId, strEffiScoreCompletedId);
 			   
 			     fnNegativeNumberUpd(strEffiScoreProjVal, rowId, strEffiScoreProjId);
                
           });
          
          // PC-3528: based on access (rep) to hide the AD and Field sales
             if (hUserAccessLevel == 2){
            	 if(screenTypeVal =='SYSTEM'){
                	 gridObj.setColumnHidden(0, true); // AD drill down
                     gridObj.setColumnHidden(1, true); // FS drill down
                 }else if(screenTypeVal =='AD'){
                     gridObj.setColumnHidden(1, true); // FS drill down
                 }else if(screenTypeVal =='FS'){
                	 gridObj.setColumnHidden(0, true); // AD drill down
                 }
             }
             
             

          // to hide the projection column.
             fnDisableProjectData ();

          // set Footer Efficiency
       	  var procCmpl = gridObj.getFooterLabel(strProcedureCompId).replace(",", "");
       	  var expUsgCmpl = gridObj.getFooterLabel(strUsageCompledId).replace(",", "");
       	       
       	  var procTotPrj = gridObj.getFooterLabel(strProcedureProjId).replace(",", "");
       	  var expUsgPrj = gridObj.getFooterLabel(strUsageCompledProjId).replace(",", "");
       	  var effScoreCmpl = (parseFloat(procCmpl) / parseFloat(expUsgCmpl)) *100 ;
       	  var effScorePrj = (parseFloat(procTotPrj) / parseFloat(expUsgPrj)) *100 ;
       	                
       	  gridObj.setFooterLabel(strEffiScoreCompletedId,'<b>'+parseFloat(effScoreCmpl.toFixed(1))+'</b>');
       	  gridObj.setFooterLabel(strEffiScoreProjId,'<b>'+parseFloat(effScorePrj.toFixed(1))+'</b>');
       	  
		
	} else {
		document.getElementById("dataGridDiv").style.display = 'none';
		document.getElementById("DivNothingMessage").style.display = 'block';
		document.getElementById("DivExportExcel").style.display = 'none';
	}

}


//This Function Used to set custom types conditions in Grid
function fnCustomTypes(gObj, setHeader, setColIds, setInitWidths, setColAlign,
		setColTypes, setColSorting, enableTooltips, setFilter, gridHeight,
		format) {
		gObj.setHeader(setHeader);
		var colCount = setHeader.length;
	
		gObj.setInitWidths(setInitWidths);
		gObj.setColAlign(setColAlign);
		gObj.setColTypes(setColTypes);
		gObj.setColSorting(setColSorting);
		gObj.enableTooltips(enableTooltips);
		gObj.attachHeader(setFilter);
		gObj.setColumnIds(setColIds);
		gObj.enableAutoHeight(true, gridHeight, true);

	return gObj;
}

//This function used to download the score details to excel file
function fnExportDtls() {
	gridObj.toExcel('/phpapp/excel/generate.php');
}

//This function used to negative sign check numeric fields 
function fnNegativeNumberUpd(scoreVal,rowId,scoreCellId) {
	   if(scoreVal < 0){
	                scoreVal = scoreVal.slice(1); 
	                //gridObj.cells(rowId,scoreCellId).setValue("("+scoreVal+")");
	                gridObj.setCellTextStyle(rowId,scoreCellId, "color:red;");
	               }
	}


// This function for show Consignment drilldown icon
function eXcell_cn_drilldown_icon(cell) {
	// the eXcell name is defined here
	if (cell) { // the default pattern, just copy it
		this.cell = cell;
		this.grid = this.cell.parentNode.grid;
	}

	this.setValue = function(val) {
		this.setCValue(
						"<a href=\"#\" onClick=fnLoadConUsageRpt('"
								+ val
								+ "')><img alt=\"Viewing Consignment Usage Report\" src=\"/images/consignment.gif\" border=\"0\"></a>",
						val);
	}
	this.getValue = function() {
		return this.cell.childNodes[0].innerHTML; // gets the value
	}
}
eXcell_cn_drilldown_icon.prototype = new eXcell;

// This function for show Loaner drilldown icon
function eXcell_ln_drilldown_icon(cell) {
	// the eXcell name is defined here
	if (cell) { // the default pattern, just copy it
		this.cell = cell;
		this.grid = this.cell.parentNode.grid;
	}

	this.setValue = function(val) {
		this.setCValue(
						"<a href=\"#\" onClick=fnLoadLoanerUsageRpt('"
								+ val
								+ "')><img alt=\"Viewing Loaner Usage Report\" src=\"/images/location.png\" border=\"0\"></a>",
						val);
	}
	this.getValue = function() {
		return this.cell.childNodes[0].innerHTML; // gets the value
	}
}
eXcell_ln_drilldown_icon.prototype = new eXcell;

// This function for show Actual Procedure drilldown icon
function eXcell_actual_drilldown_icon(cell) {
	// the eXcell name is defined here
	if (cell) { // the default pattern, just copy it
		this.cell = cell;
		this.grid = this.cell.parentNode.grid;
	}

	this.setValue = function(val) {
		this.setCValue(
						"<a href=\"#\" onClick=fnLoadActualProcRpt('"
								+ val
								+ "')><img alt=\"Viewing Actual Procedure\" src=\"/images/packslip.gif\" border=\"0\"></a>",
						val);
	}
	this.getValue = function() {
		return this.cell.childNodes[0].innerHTML; // gets the value
	}
}
eXcell_actual_drilldown_icon.prototype = new eXcell;

// this function used to load Exp. consignment usage screen drill down values
function fnLoadConUsageRpt(refId) {
	// to get the ref id parameter values
	var drillDownRefIdVal = fnGetActionParam(refId);
	document.frmSalesEfficiencyScoreCard.action = '/gmSalesEfficiencyScoreCardByCon.do?method=loadConUsageScoreCardDtls&hideCompany=Y'
			+ drillDownRefIdVal;
	document.frmSalesEfficiencyScoreCard.submit();
}

// this function used to load Exp. Loaner usage screen drill down values
function fnLoadLoanerUsageRpt(refId) {
	// to get the ref id parameter values
	var drillDownRefIdVal = fnGetActionParam(refId);
	document.frmSalesEfficiencyScoreCard.action = '/gmSalesEfficiencyScoreCardByLoaner.do?method=loadLoanerUsageScoreCardDtls&hideCompany=Y'
			+ drillDownRefIdVal;
	document.frmSalesEfficiencyScoreCard.submit();
}

// this function used to load Actual Procedure screen drill down values
function fnLoadActualProcRpt(refId) {
	// to get the ref id parameter values
	var drillDownRefIdVal = fnGetActionParam(refId);
	console.log("allowedTag"+allowedTag);
	// Added hAllowedTag hidden field according to PC-5101: Scorecard DO classification changes
	document.frmSalesEfficiencyScoreCard.action = '/gmScoreCardActualProceduresReport.do?method=loadActualProcUsageScoreCardDtls&hideCompany=Y&hAllowedTag='+allowedTag
			+ drillDownRefIdVal;
	document.frmSalesEfficiencyScoreCard.submit();
}

// this function used to form the action parameter values - based on screen
// type.
function fnGetActionParam(refId) {
	var actionVal = '';
	if (screenTypeVal == "SYSTEM") {
		actionVal = '&systemId=' + refId;
	} else if (screenTypeVal == "FS") {
		actionVal = '&distributorId=' + refId;
	} else if (screenTypeVal == "AD") {
		actionVal = '&adId=' + refId;
	}
	return actionVal;
}

// This function used to enable the effi. projection data.
function fnDisableProjectData() {
	// to disable the projection column.
	gridObj.setColumnHidden(strCNProjId, true);
	gridObj.setColumnHidden(strLNProjId, true);
	gridObj.setColumnHidden(strUsageCompledProjId, true);
	gridObj.setColumnHidden(strProcedureProjId, true);
	gridObj.setColumnHidden(strEffiProjId, true);
	gridObj.setColumnHidden(strEffiScoreProjId, true);

	/*// enable the button
	document.getElementById("Btn_ScoreCard").style.display = "none";
	document.getElementById("Btn_ScoreCard_With_Proj").style.display = "block";*/
}

// This function used to enable the Effi. projection data.
function fnEnableProjectData() {
	// to enable the projection column.
	gridObj.setColumnHidden(strCNProjId, false);
	gridObj.setColumnHidden(strLNProjId, false);
	gridObj.setColumnHidden(strUsageCompledProjId, false);
	gridObj.setColumnHidden(strProcedureProjId, false);
	gridObj.setColumnHidden(strEffiProjId, false);
	gridObj.setColumnHidden(strEffiScoreProjId, false);

	/*// enable the button
	document.getElementById("Btn_ScoreCard").style.display = "block";
	document.getElementById("Btn_ScoreCard_With_Proj").style.display = "none";*/
}


//This function for show Consignment drilldown icon
function eXcell_drill_icon_1(cell) {
	// the eXcell name is defined here
	if (cell) { // the default pattern, just copy it
		this.cell = cell;
		this.grid = this.cell.parentNode.grid;
	}

	this.setValue = function(val) {
		fnGetDrildownImages (val);
		this.setCValue(dril_down_1_images, val);
	}
	this.getValue = function() {
		return this.cell.childNodes[0].innerHTML; // gets the value
	}
}
eXcell_drill_icon_1.prototype = new eXcell;


//This function for show Consignment drilldown icon
function eXcell_drill_icon_2(cell) {
	// the eXcell name is defined here
	if (cell) { // the default pattern, just copy it
		this.cell = cell;
		this.grid = this.cell.parentNode.grid;
	}

	this.setValue = function(val) {
		fnGetDrildownImages (val);
		this.setCValue(dril_down_2_images, val);
	}
	this.getValue = function() {
		return this.cell.childNodes[0].innerHTML; // gets the value
	}
}
eXcell_drill_icon_2.prototype = new eXcell;


//this function used to form images based on screen
function fnGetDrildownImages (refId) {
	if (screenTypeVal == "SYSTEM") {
		// image (AD and Field Sales)
		dril_down_1_images = "<a href=\"#\" onClick=fnDrilldownRpt('loadADScoreCardDtls&systemId=','"+ refId+ "')><img alt=\"Viewing Scorecard AD Report\" src=\"/images/a.gif\" border=\"0\"></a>";
		dril_down_2_images = "<a href=\"#\" onClick=fnDrilldownRpt('loadFieldSalesScoreCardDtls&systemId=','"+ refId+ "')><img alt=\"Viewing Scorecard Fieldsales Report\" src=\"/images/icon-letter.png\" border=\"0\"></a>";
		
	} else if (screenTypeVal == "FS") {
		// image (AD and System)
		dril_down_1_images = "<a href=\"#\" onClick=fnDrilldownRpt('loadADScoreCardDtls&distId=','"+ refId+ "')><img alt=\"Viewing Scorecard AD Report\" src=\"/images/a.gif\" border=\"0\"></a>";
		dril_down_2_images = "<a href=\"#\" onClick=fnDrilldownRpt('loadSystemScoreCardDtls&distId=','"+ refId+ "')><img alt=\"Viewing Scorecard System Report\" src=\"/images/ordsum.gif\" border=\"0\"></a>";
	} else if (screenTypeVal == "AD") {
		// image (System and Field Sales)
		dril_down_1_images = "<a href=\"#\" onClick=fnDrilldownRpt('loadSystemScoreCardDtls&adId=','"+ refId+ "')><img alt=\"Viewing Scorecard System Report\" src=\"/images/ordsum.gif\" border=\"0\"></a>";
		dril_down_2_images = "<a href=\"#\" onClick=fnDrilldownRpt('loadFieldSalesScoreCardDtls&adId=','"+ refId+ "')><img alt=\"Viewing Scorecard Fieldsales Report\" src=\"/images/icon-letter.png\" border=\"0\"></a>";
	}
	
}

// this function used to call the reports - based on the screen
function fnDrilldownRpt (methodName, refId){
	var drilldownName = gridObj.cellById(refId, 5).getValue();
	
	// Added allowed tag field according to PC-5101: Scorecard DO classification changes
	
	document.frmSalesEfficiencyScoreCard.action = '/gmSalesEfficiencyScoreCard.do?method='+methodName + refId+'&drillDownName='+ drilldownName+'&allowedTag='+ allowedTag ;
	fnStartProgress();
	document.frmSalesEfficiencyScoreCard.submit();
	
}
