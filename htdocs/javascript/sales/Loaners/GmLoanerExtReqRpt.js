// This function used to page onLoad 
function fnOnPageLoad() {
	document.getElementById("DivNothingMessage").style.display = 'none';
	arr = document.all.Chk_GrpZone;
	fnLoadZone(arr);
   
	var arrList = document.all.Chk_GrpZone;
    var filterCookie= document.all.hZoneGrp.value;
    fnCheckBoxSelections(filterCookie,arrList);   
  
    arrList = document.all.Chk_GrpRegn;   
    filterCookie=document.all.hRegionGrp.value;
    if(zone !=''){   
    fnLoadRegn(zone.split(","));
    }
    fnCheckBoxSelections(filterCookie,arrList);

    filterCookie=document.all.hFieldSales.value;
    arrList = document.all.Chk_GrpDist;
    if(region !=''){
    fnLoadDist(region.split(","));
    }
    fnCheckBoxSelections(filterCookie,arrList);
}

//this function used to Export the Excel
function fnExport(){
	gridObj.setColumnHidden(0,true);
	gridObj.setColumnHidden(15,true);
	gridObj.setColumnHidden(18,true);
	gridObj.toExcel('/phpapp/excel/generate.php');
	gridObj.setColumnHidden(0,false);
	gridObj.setColumnHidden(15,false);
	gridObj.setColumnHidden(18,false);
}
//this function used to load the report As per filter selection
function fnLoad(){

	var strZone = fnGetCheckedValues(document.all.Chk_GrpZone);
	var strRegion = fnGetCheckedValues(document.all.Chk_GrpRegn);
	var strFilesSales = fnGetCheckedValues(document.all.Chk_GrpDist);
	var fromDtObj = document.frmLoanerExtReqRpt.fromDt;
	var toDtObj = document.frmLoanerExtReqRpt.toDt;
	var fromDt = fromDtObj.value;
	var toDt = toDtObj.value;
	if(fromDtObj != undefined && fromDtObj.value != ""){
		CommonDateValidation(fromDtObj,format,Error_Details_Trans(message[5535],format));
	}
	if(toDtObj != undefined && toDtObj.value != ""){
		CommonDateValidation(toDtObj,format,Error_Details_Trans(message[5536],format));
	}	
	if (dateDiff(fromDt, toDt, format) < 0) {
		Error_Details(message_operations[504]);
	}
	if(dateDiff(fromDt, toDt, format) > 90){
		Error_Details("Date range cannot exceed more than 3 months.");
	}
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	fnStartProgress();
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmLoanerExtReqRpt.do?method=fchLoanerExtnReqRpt&strZone='+strZone+ '&strRegion='+strRegion+'&strFieldSales='+strFilesSales+'&fromDt='+fromDt+'&toDt='+toDt+'&ramdomId=' + new Date().getTime());
	dhtmlxAjax.get(ajaxUrl, fnLoadExtReqRptDtls);
}
//call back function Loaner extn Request Details Report 
function fnLoadExtReqRptDtls(loader) {
	response = loader.xmlDoc.responseText;
	fnStopProgress('Y');
		if (response != "" && response != undefined){	
			fnLoadExtReqRptDtl();
		}else{
			document.getElementById("dataGridDiv").style.display = 'none';
			document.getElementById("pagingArea").style.display = 'none';
			document.getElementById("DivNothingMessage").style.display = 'block';
			document.getElementById("DivExportExcel").style.display = 'none';			
		}
	}
//this function used to Genarate the Gird Data 
function fnLoadExtReqRptDtl(){
	var strJsonString =  response;
	//to form the grid string 
	var rows = [], i = 0;
	var JSON_Array_obj = "";
	JSON_Array_obj = JSON.parse(strJsonString);
	$.each(JSON_Array_obj, function(index,jsonObject) {
		reqdtlid = jsonObject.reqdtlid;
		reqid = jsonObject.reqid;
		cnid = jsonObject.cnid;
		comments = jsonObject.comments;
		setid = jsonObject.setid;
		setname = jsonObject.setname;
		repname = jsonObject.repname;
		acctname = jsonObject.acctname;
		requname = jsonObject.requname;
		loanerdt = jsonObject.loanerdt;
		oldexpdt = jsonObject.oldexpdt;
		extnreqdt = jsonObject.extnreqdt;
		surgdt = jsonObject.surgdt;
		extnreqretdt=jsonObject.extnreqretdt;
		apprretdt = jsonObject.apprretdt;
		extcount = jsonObject.extcount;
		statusnm = jsonObject.statusnm;
		statusid = jsonObject.statusid;
		hisfl = jsonObject.hisfl;
		expdayout = dateDiff(loanerdt, apprretdt, format);
		expdayout = expdayout+1;
		replnFl = jsonObject.replnfl;
		etchid = jsonObject.etchid;
	
		// default array
		rows[i] = {};
		rows[i].id = reqdtlid;
		rows[i].data = [];
		
		if(statusid == '1902' || statusid == '1903'){
			rows[i].data[0] = "<a href=\"#\" onClick=fnOpenLog('"+ reqdtlid + "','N')><img alt=\"Set Priority Upload\" src=\"/images/phone-icon_ans.gif\" border=\"0\"  height=\"15\" width=\"15\" disabled=\"true\"></a>";
		}
			
			rows[i].data[1] = reqid;
			rows[i].data[2] = cnid;
			rows[i].data[3] = etchid;
			rows[i].data[4] = comments;
			rows[i].data[5] = setid;
			rows[i].data[6] = setname;
			rows[i].data[7] = repname;
			rows[i].data[8] = acctname;
			rows[i].data[9] = requname;
			rows[i].data[10] = loanerdt;
			rows[i].data[11] = oldexpdt;
			rows[i].data[12] = extnreqdt;
			rows[i].data[13] = surgdt;
			rows[i].data[14] = extnreqretdt;
			if(hisfl != '0'){
			rows[i].data[15] = "<a href=\"#\" onClick=fnOpenLog('"+ cnid + "','Y')><img alt=\"History Details\" src=\"/images/icon_History.gif\" border=\"0\"  height=\"15\" width=\"15\" disabled=\"true\"></a>";
			}
			rows[i].data[16] = apprretdt;

			rows[i].data[17] = extcount;
			if(extcount > '0'){
			rows[i].data[18]= "<a href=\"#\" onClick=fnLoanerExtLog('"+ reqdtlid + "')><img alt=\"History Details\" src=\"/images/icon_History.gif\" border=\"0\"  height=\"15\" width=\"15\" disabled=\"true\"></a>";	
		}
			rows[i].data[19] = expdayout;
			rows[i].data[20] = replnFl;
			if(statusid == '1902' || statusid == '1903'){
				rows[i].data[21] = statusnm;
			}else{
				rows[i].data[21] = '';
			}
		
		i++;

	});

// to load other than open status details in work order revision update report
		
	var setInitWidths = "20,55,83,65,90,70,90,90,100,90,70,70,70,70,70,20,65,50,20,40,50,*";
	var setColAlign = "center,left,left,left,left,left,left,left,left,center,center,center,center,left,center,center,right,right,center,left,left,left";
	var setColTypes = "ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro";
	var setColSorting = "na,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str";
	var setHeader = ["","Parent Req ID", "Txn ID","Etch ID", "Comment",
		  				"Set ID", "Set Name", "Sales Rep", "Account", "Requestor Name",
		  				"Loaned On","Exp. Return Date","Extn. Req Date","Surgery Date","Extn. Req. Return Dt"," ","Appr Return Date","Previous Extend Count","","Exp. Days Out","Repln","Status"];
	var setFilter = [ "#rspan", "#numeric_filter", "#text_filter", "#text_filter","#text_filter",
	  				"#text_filter", "#text_filter", "#text_filter",
	  				"#text_filter", "#text_filter", "#text_filter","#text_filter",
	  				"#text_filter", "#text_filter", "#text_filter","#rspan","#text_filter","#numeric_filter","","#numeric_filter","#text_filter" ,"#text_filter"];
		var setColIds = "";
		var enableTooltips = "true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true";
		var footerArry = [];
		var gridHeight = "";
		var footerStyles = [];
		var footerExportFL = false;
		var pagination = "Y";
		var dataHost = {};
	
		dataHost.rows = rows;
		document.getElementById("dataGridDiv").style.display = 'block';
		document.getElementById("DivNothingMessage").style.display = 'none';
		document.getElementById("DivExportExcel").style.display = 'block';
		gridObj = loadDHTMLXGrid('dataGridDiv', gridHeight, dataHost,
				setHeader, setInitWidths, setColAlign, setColTypes,
				setColSorting, setColIds, enableTooltips, setFilter,
				footerArry, footerStyles, footerExportFL, pagination);
}
//this function used to open the comments details 
function fnOpenLog(id,fl)
{
	windowOpener("/gmLoanerExtReqRpt.do?method=fetchHistoryDtls&ReddtlId="+id+'&fl='+fl,"Search","resizable=yes,scrollbars=yes,top=150,left=150,width=540,height=300");
}
//PMT-50520
function fnLoanerExtLog(reqdtlid){
	windowOpener("/gmLoanerExtReqRpt.do?method=fetchLoanerHistory&ReddtlId="+reqdtlid,"Search","resizable=yes,scrollbars=yes,top=150,left=150,width=1050,height=300");
}
//this function used for Sorting the Region
function sortFunction(a, b) {
	var x = a[1].toLowerCase();
	var y = b[1].toLowerCase();
	return ((x < y) ? -1 : ((x > y) ? 1 : 0));
	}
//this function used to Close the window 
function fnClose()
{
	window.close();
}