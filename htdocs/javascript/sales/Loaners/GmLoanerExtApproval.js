var consinid = '';
var chkboxRefId = '';
var reqDetId_rowId = '';
var reqId_rowId = '';
var apprRetnDt_rowId = '';
var check_id ='';
var showImg_id = '';
var dateFormat='';
var selAllChk='';
//This function used to page onLoad 
function fnOnPageLoad() {
	document.getElementById("DivNothingMessage").style.display = 'none';
	arr = document.all.Chk_GrpZone;
	fnLoadZone(arr);
	
	var arrList = document.all.Chk_GrpZone;
    var filterCookie= document.all.hZoneGrp.value;
    fnCheckBoxSelections(filterCookie,arrList);   
  
    arrList = document.all.Chk_GrpRegn;   
    filterCookie=document.all.hRegionGrp.value;
    if(zone !=''){   
    fnLoadRegn(zone.split(","));
    }
    fnCheckBoxSelections(filterCookie,arrList);

    filterCookie=document.all.hFieldSales.value;
    arrList = document.all.Chk_GrpDist;
    if(region !=''){
    fnLoadDist(region.split(","));
    }
    fnCheckBoxSelections(filterCookie,arrList);
  

	var strZone = fnGetCheckedValues(document.all.Chk_GrpZone);
	var strRegion = fnGetCheckedValues(document.all.Chk_GrpRegn);
	var strFilesSales = fnGetCheckedValues(document.all.Chk_GrpDist);
	fnStartProgress();
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmLoanerExtApprl.do?method=fetchLoanerExtApprl&strZone='+strZone+ '&strRegion='+strRegion+'&strFieldSales='+strFilesSales+'&ramdomId=' + new Date().getTime());
	dhtmlxAjax.get(ajaxUrl, fnLoadPenExtDtls);
}

function fnSalesReport(setid , adid ,sysid){
	 w1 = createDhtmlxWindow(1050, 500);
     w1.setText(message_sales[128]);
     var ajaxUrl = fnAjaxURLAppendCmpInfo('/GmSalesConsignmentServlet?hAction=LoadDCONS&hAreaDirector='+adid+'&SetNumber='+sysid+'&Cbo_SalesType=50302&Cbo_TypeCon=50401&Cbo_Turns=3&Loaner_app=YES&randomId='+Math.random());
     w1.attachURL(ajaxUrl);
}

//this function used to select all option (check box)
function fnChangedFilter(obj) {
	if (obj.checked) {
		gridObj.forEachRow(function(rowId) {
		   var check_disable_fl = gridObj.cellById(rowId, check_rowId).isDisabled();
		    if (!check_disable_fl) {
				gridObj.cellById(rowId, check_rowId).setChecked(true);
			}
		});
	}else{
		gridObj.forEachRow(function(rowId) {
			check_id = gridObj.cellById(rowId, check_rowId).getValue();
			if (check_id == 1) {
				gridObj.cellById(rowId, check_rowId).setChecked(false);
			}

		});
	}
}

//Description : This function to check the select all (master check box)
function doOnCheck(rowId, cellInd, stage){
	rowID = rowId;	
	if(cellInd == check_rowId) {
		fnSelectAllToggle('mastercheck');
	}
	return true;
}
//Description : This function to check the select all check box
function fnSelectAllToggle( varControl){
	var objControl = eval("document.all."+varControl);
	var all_row = gridObj.getAllRowIds(",");
	var all_rowLen = all_row.length; // get the rows id length
	var checked_row = gridObj.getCheckedRows(check_rowId);
	var finalval;
	finalval = eval(checked_row.length);
	if(all_rowLen == finalval){
		objControl.checked = true;// select all check box to be checked
	}else{
		objControl.checked = false;// select all check box un checked.
	}
}
//For Approving the Loaner Extension Request
function fnApproval(){
	var checkedRowID = '';
	var reqDetId_value = '';
	var reqId_value = '';
	var apprRetnDt_value = '';
	var reqDtlIdStr=''; 
	checkedRowID = gridObj.getCheckedRows(chkboxRefId);
	if(checkedRowID == '' && checkedRowID != undefined){
		Error_Details(message_sales[129]);
	}
	
	var approvalCmntsObj = document.frmLoanerExtApprl.strApprovalComments;
	var approvalComment = approvalCmntsObj.value;
	gridObj.forEachRow(function(rowId) {
		check_id = gridObj.cellById(rowId, chkboxRefId).getValue();
		if(check_id == 1) {
			reqDetId_value = gridObj.cellById(rowId, reqDetId_rowId).getValue();
			reqId_value = gridObj.cellById(rowId, reqId_rowId).getValue();
			apprRetnDt_value = gridObj.cellById(rowId, apprRetnDt_rowId).getValue();
			surgDt_value = gridObj.cellById(rowId, surgdt_rowId).getValue();
			var apprDateObj = gridObj.cellById(rowId, apprRetnDt_rowId);
			CommonDateValidation(apprDateObj,format,message[10529]);
			if (ErrorCount > 0){
				Error_Show();
				Error_Clear();
				return false;
			}
			 var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmLoanerExtApprl.do?method=saveLoanerExtApprl&strRequestDetId=' + encodeURIComponent(reqDetId_value) +'&strRequestId=' + encodeURIComponent(reqId_value) + '&strApprReturnDate=' + encodeURIComponent(apprRetnDt_value)   
					  +'&strApprovalComments=' + encodeURIComponent(approvalComment)+ '&strSurgeryDate=' + encodeURIComponent(surgDt_value)+'&ramdomId=' + Math.random() +"&"+ fnAppendCompanyInfo());
		          fnStartProgress('Y');
			      dhtmlxAjax.get(ajaxUrl, function(loader){
			      response = loader.xmlDoc.responseText;
			    if (response == 'Y' ){
			    	gridObj.cellById(rowId,showImg_id).setValue("<img  title='Approved' height='24' width='25' src='/images/success.gif'/ border='0'>");
			    	gridObj.cellById(rowId,0).setDisabled(true); //To make the checkbox disabled after approved an order
			    	gridObj.setRowTextStyle(rowId, "background-color:rgb(175, 175, 175);"); //To highlight the entire saved Row
			    	document.getElementById("strApprovalComments").value = '';
			    	fnStopProgress('Y');
			    	fnMouseOver(gridObj);
			    	return true;
			    }
			    else{
			    	gridObj.cellById(rowId,showImg_id).setValue("<img id='fail"+rowId+"' title='"+response+"' alt='Approve Failure' src='/images/delete.gif'/ border='0'>");
			    	fnMouseOver(gridObj);
			        fnStopProgress('Y');
			    }
		    });
				gridObj.cellById(rowId, 0).setChecked(false);
				reqDtlIdStr=reqDtlIdStr+reqDetId_value+',';
		} 
    });
	//to send Loner Extn Email Notification function Call
	if (reqDtlIdStr != ''){
		reqDtlIdStr = reqDtlIdStr.substring(0, reqDtlIdStr.length - 1); // to remove last Comma
		fnMail(reqDtlIdStr,'1902');    			
	}  		
	
	
	 if (ErrorCount > 0){
			Error_Show();
			Error_Clear();
			return false;
		}
}
	
// FOr Rejecting the Loaner Extension Request
function fnReject(){
	var checkedRowID = '';
	var reqId_value = '';
	var reqDetId_value = '';
	var apprRetnDt_value = ''; 
	var reqDtlIdStr='';
	checkedRowID = gridObj.getCheckedRows(chkboxRefId);
	if(checkedRowID == '' && checkedRowID != undefined){
		Error_Details(message_sales[131]);
	}
	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}

	var approvalCmntsObj = document.frmLoanerExtApprl.strApprovalComments;
	if(approvalCmntsObj != undefined && approvalCmntsObj.value !=''){
	var approvalComment = approvalCmntsObj.value;
	gridObj.forEachRow(function(rowId) {
		check_id = gridObj.cellById(rowId, chkboxRefId).getValue();
		if(check_id == 1) {
			reqDetId_value = gridObj.cellById(rowId, reqDetId_rowId).getValue();
			reqId_value = gridObj.cellById(rowId, reqId_rowId).getValue();
			apprRetnDt_value = gridObj.cellById(rowId, apprRetnDt_rowId).getValue();
			surgDt_value = gridObj.cellById(rowId, surgdt_rowId).getValue();
			 var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmLoanerExtApprl.do?method=rejectLoanerExtApprl&strRequestDetId=' + encodeURIComponent(reqDetId_value) +'&strRequestId=' + encodeURIComponent(reqId_value) + '&strApprReturnDate=' + encodeURIComponent(apprRetnDt_value)   
					  +'&strApprovalComments=' + encodeURIComponent(approvalComment) + '&ramdomId=' + Math.random() +"&"+ fnAppendCompanyInfo());
		          fnStartProgress('Y');
			      dhtmlxAjax.get(ajaxUrl, function(loader){
			      response = loader.xmlDoc.responseText;
			    if (response == 'Y' ){
			    	gridObj.cellById(rowId,showImg_id).setValue("<img  title='Rejected' height='15px' width='15px' src='/images/red_btn_minus.gif'/ border='0'>");
			    	gridObj.cellById(rowId,0).setDisabled(true); //To make the checkbox disabled after approved an order
			    	gridObj.setRowTextStyle(rowId, "background-color:rgb(175, 175, 175);"); //To highlight the entire saved Row
			    	document.getElementById("strApprovalComments").value = '';
			    	fnStopProgress('Y');
			    	fnMouseOver(gridObj);
			    	return true;
			    }
			    else{
			    	gridObj.cellById(rowId,showImg_id).setValue("<img id='fail"+rowId+"' title='"+response+"' alt='Reject Failure' src='/images/delete.gif'/ border='0'>");
			    	fnMouseOver(gridObj);
			        fnStopProgress('Y');
			    }
		    });
				gridObj.cellById(rowId, 0).setChecked(false);
				reqDtlIdStr=reqDtlIdStr+reqDetId_value+',';
				
		} 
    });
	//to send Loner Extn Email Notification function Call
	if (reqDtlIdStr != ''){
		reqDtlIdStr = reqDtlIdStr.substring(0, reqDtlIdStr.length - 1);// to remove last Comma
		fnMail(reqDtlIdStr,'1903');    			
	} 
	}else{
		Error_Details(message_sales[117]);
	}
	
	 if (ErrorCount > 0){
			Error_Show();
			Error_Clear();
			return false;
		}
}

function fnMouseOver(gridObj){
	gridObj.attachEvent("onMouseOver",function()
		{
			return false;
		});
}
//to Export Excel 
function fnExport(){
	gridObj.setColumnHidden(0,true);
	gridObj.setColumnHidden(1,true);
	gridObj.setColumnHidden(16,true);
	gridObj.toExcel('/phpapp/excel/generate.php');
	gridObj.setColumnHidden(0,false);
	gridObj.setColumnHidden(1,true);
	gridObj.setColumnHidden(16,false);
}
function fnLoad(){
	var strZone = fnGetCheckedValues(document.frmLoanerExtApprl.Chk_GrpZone);
	var strRegion = fnGetCheckedValues(document.frmLoanerExtApprl.Chk_GrpRegn);
	var strFilesSales = fnGetCheckedValues(document.all.Chk_GrpDist);
	fnStartProgress();
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmLoanerExtApprl.do?method=fetchLoanerExtApprl&strZone='+strZone+ '&strRegion='+strRegion+'&strFieldSales='+strFilesSales+'&ramdomId=' + new Date().getTime());
	dhtmlxAjax.get(ajaxUrl, fnLoadPenExtDtls);
}
//this Function Used to Send the Loner Extn Email Notification 
function fnMail(reqID,statusID){
	setTimeout(function(){ 
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmLoanerExtApprl.do?method=sendLoanerExtnEmail&strRequestDetId='+encodeURIComponent(reqID)+'&strStatusId='+statusID+'&ramdomId='+new Date().getTime());
	dhtmlxAjax.get(ajaxUrl, function(loader){});
	}, 10000);
}

//Called to load zone list dynamically based on the selected regions in the Regions list
function fnLoadZone(arrChk)
{
	var j = 0;
	var k = 0;
	var arr = document.all.Chk_GrpDiv;
	var arrChk = new Array();
	var arrComp = new Array();
	var arrZone = new Array();
	
		document.all.Div_Zone.innerHTML = '';
		var newhtml = '';
		var arrChklen = arrChk.length;
		var arrzonelen = ZAr.length;
		var chkzone = '';
		var chkcomp = '';
		if(arrzonelen >= 1){
				for (var j=0;j<arrzonelen ;j++)
				{		
						if (newhtml.indexOf(ZAr[j][0]) < 0)
						{
						newhtml = newhtml + fnCreateChkBox('Zone',ZAr[j][0],ZAr[j][1],ZAr[j][2],'fnLoadRegn(this)');
						arrZone[k] = ZAr[j][0];
						k++;
						}
				}
		}
        
		document.all.Div_Zone.innerHTML = newhtml;
		if (newhtml =='')
		{
			document.all.Div_Zone.innerHTML = '<span class=RightTextRed>No Zone</span>';
			document.all.Div_Regn.innerHTML = '<span class=RightTextRed>No Region</span>';			
		}else
		{
			document.all.Div_Zone.innerHTML = newhtml;
			fnLoadRegn(arrZone);	
		}
    
        if(frmObj.Chk_Grp_ALL_Zone.checked == true){
            frmObj.Chk_Grp_ALL_Zone.checked = false;
        }
}

//Function to check all checkbox by Regn,Zone,Dist
function fnSelectAll(val){   
    var allContArr = new Array();
    var allCheckObj = '';
    var divAllVal = '';

    if (val == 'Zone') {
        divAllVal = document.all.Div_Zone.innerHTML;
        allCheckObj = frmObj.Chk_Grp_ALL_Zone.checked;
    }
    if (val == 'Regn') {
        divAllVal = document.all.Div_Regn.innerHTML;
        allCheckObj = frmObj.Chk_Grp_ALL_Regn.checked;
    }
    if(val == 'Dist'){
		divAllVal =document.all.Div_Dist.innerHTML;
		allCheckObj = frmObj.Chk_Grp_ALL_Dist.checked;
	}

    getChkAll(allCheckObj, divAllVal, val, function () {

        if (val == 'Zone') {
            selAllChk = 'Zone';
            frmObj.Chk_Grp_ALL_Regn.checked = false;
            frmObj.Chk_Grp_ALL_Dist.checked = false;
            fnLoadRegn(allContArr);
            selAllChk = '';
        }
        if (val == 'Regn') {
            selAllChk = 'Regn';
            frmObj.Chk_Grp_ALL_Dist.checked = false;
            fnLoadDist(allContArr);
            selAllChk = '';
        }
    });
}

//Function to check all checkbox by passing object,array
 function getChkAll(allCheckObj,divAllVal,val,callback){
	 var parentObj=document.getElementById("Div_"+val);
	 var cnt=0;
	 var arrLen=divAllVal.split('Chk_Grp'+val).length-1;
	 for(var i=0;i<arrLen;i++){
		 if(allCheckObj == true){
			 parentObj.children[cnt].checked=true;
		 cnt +=3;
		 }else{
			 parentObj.children[cnt].checked=false;
			 cnt +=3;
		 }
	 }
	 callback();
}

//Function to get the unchecked values from the object passed
function fnGetUnCheckedValues(obj)
{
	var str = '';
	var arr = obj;
	 
	if (obj)
	{
		var arrlen = arr.length;
		if (arrlen > 0)
		{
			for (var i=0;i< arrlen;i++ )
			{
				if (arr[i].checked == false)
				{
					str = str + arr[i].id + ',';
				}
			}
		
			if (str != '')
			{
				str = str.substr(0,str.length-1);
			}
		}
		else if(arr.checked == false)
		{
			str = arr.id;
		}
	}
	return str;
}



function fnLoadRegn(arrChk)
{
	var j = 0;
	var k = 0;
	var arrRegn = new Array();
	var arrResult = new Array();
	if (arrChk != null && arrChk.length > 0)
	{
	// does nothing ... but dont remove		
	}
	else if( document.all.Chk_GrpZone != null ) //for VP,AD the ZONE filter is not available
	{
		var arr = document.all.Chk_GrpZone;
		var arrChk = new Array();
		var arrlen = arr.length;
		if(arrlen == undefined && arr.checked == true)
		{
			arrChk[j] = arr.id;
			j++;
		}
		else
		{
			for (var i=0;i< arrlen;i++ )
			{
				if (arr[i].checked == true)
				{
					arrChk[j] = arr[i].id;
					j++;
				}
			}
		}
		if(j==0 && document.all.Chk_GrpDiv != null){
			var arrayValues = getArrayOfValues(document.all.Chk_GrpDiv);
			if(arrayValues.length > 0){
				fnLoadZone(arrayValues);
			}
			return;
		}
	}else{
		/*	
		    If the above 'else if' is not executed, the arrChk will throws error. 
			If we declared arrChk variable at the top of the method then 
			the filter methods are calling cyclically it will throws out of memory error 
		*/
		var arrChk = new Array(); 
	}

		document.all.Div_Regn.innerHTML = '';
		var newhtml = '';
		var arrChklen = arrChk.length;
		var arrregnlen = RAr.length;
		var chkzone = '';

		if(arrChklen > 0)
		{
			for (var i=0;i<arrChklen ;i++ )
			{
				chkzone = arrChk[i];
				for (var j=0;j<arrregnlen ;j++ )
				{		
					if (RAr [j][2] == chkzone)//Code Id For All
					{
						arrResult[k] = new Array();
						arrResult[k] = RAr[j];
						arrRegn[k] = RAr[j][0];
						k++;
					}
				}
			}
		}
		else {
			for (var j=0;j<arrregnlen ;j++ )
			{		
					arrResult[k] = new Array();
					arrResult[k] = RAr[j];
					arrRegn[k] = RAr[j][0];
					k++;
			}
	}
		arrResult.sort(sortFunction);
    

		for (var l=0;l<k ;l++ )
		{
			if (newhtml.indexOf("id='"+arrResult[l][0]+"'") < 0)
			{
			newhtml = newhtml + fnCreateChkBox('Regn',arrResult[l][0],arrResult[l][1],arrResult[l][2],'fnLoadDist(this)');
			}
		}
		document.all.Div_Regn.innerHTML = newhtml;
		if (newhtml =='')
		{
			document.all.Div_Regn.innerHTML = '<span class=RightTextRed>No Region</span>';		
		}else
		{
			document.all.Div_Regn.innerHTML = newhtml;	
			fnLoadDist(arrRegn);
		}

        if(frmObj.Chk_Grp_ALL_Zone.checked == true && selAllChk!='Zone'){
            frmObj.Chk_Grp_ALL_Zone.checked = false;
        }
        if(frmObj.Chk_Grp_ALL_Regn.checked == true){
            frmObj.Chk_Grp_ALL_Regn.checked = false;
        }
        if(fnGetUnCheckedValues(frmObj.Chk_GrpZone)==''){
            frmObj.Chk_Grp_ALL_Zone.checked = true;
        }
}
//Called to load Distributor list dynamically based on the selected regions in the Regions list
function fnLoadDist(arrChk)
{
	var j = 0;
	var k = 0;
	var arrDist = new Array();
	var arrResult = new Array();
	if (arrChk != null && arrChk.length > 0)
	{
	// does nothing ... but dont remove		
	}
	else if( document.all.Chk_GrpRegn != null ) //for VP,AD the ZONE filter is not available  Chk_GrpZone
	{
		var arr = document.all.Chk_GrpRegn;
		var arrChk = new Array();
		var arrlen = arr.length;
		if(arrlen == undefined && arr.checked == true)
		{
			arrChk[j] = arr.id;
			j++;
		}
		else
		{
			for (var i=0;i< arrlen;i++ )
			{
				if (arr[i].checked == true)
				{
					arrChk[j] = arr[i].id;
					j++;
				}
			}
		}
		if(j==0 && document.all.Chk_GrpZone != null){
			var arrayValues = getArrayOfValues(document.all.Chk_GrpZone);
			if(arrayValues.length > 0){
				fnLoadRegn(arrayValues);
			}
			return;
		}
	}else{
		/*	
		    If the above 'else if' is not executed, the arrChk will throws error. 
			If we declared arrChk variable at the top of the method then 
			the filter methods are calling cyclically it will throws out of memory error 
		*/
		var arrChk = new Array(); 
	}

		document.all.Div_Dist.innerHTML = '';
		var newhtml = '';
		var arrChklen = arrChk.length;
		var arrDistlen = DAr.length;
		var chkregn = '';

		if(arrChklen > 0)
		{
			for (var i=0;i<arrChklen ;i++ )
			{
				chkregn = arrChk[i];
				for (var j=0;j<arrDistlen ;j++ )
				{		
					if (DAr [j][2] == chkregn)//Code Id For All
					{
						arrResult[k] = new Array();
						arrResult[k] = DAr[j];
						arrDist[k] = DAr[j][0];
						k++;
					}
				}
			}
		}
		else {
			for (var j=0;j<arrDistlen ;j++ )
			{		
					arrResult[k] = new Array();
					arrResult[k] = DAr[j];
					arrDist[k] = DAr[j][0];
					k++;
			}
	}
		arrResult.sort(sortFunction);
    

    
		for (var l=0;l<k ;l++ )
		{
			if (newhtml.indexOf("id='"+arrResult[l][0]+"'") < 0)
			{
			newhtml = newhtml + fnCreateChkBox('Dist',arrResult[l][0],arrResult[l][1],arrResult[l][2],'fnLoadTerr(this)');
			}
		}
		document.all.Div_Dist.innerHTML = newhtml;
		if (newhtml =='')
		{
			document.all.Div_Dist.innerHTML = '<span class=RightTextRed>No Distributors</span>';
		}else
		{
			document.all.Div_Dist.innerHTML = newhtml;	
		
		} 
    
        if (frmObj.Chk_Grp_ALL_Regn.checked == true && selAllChk != 'Regn') {
            frmObj.Chk_Grp_ALL_Regn.checked = false;
        }
        if (frmObj.Chk_Grp_ALL_Dist.checked == true) {
            frmObj.Chk_Grp_ALL_Dist.checked = false;
        }
        if (fnGetUnCheckedValues(frmObj.Chk_GrpRegn) == '') {
            frmObj.Chk_Grp_ALL_Regn.checked = true;
        }
}

function fnLoadTerr(arrChk){
	if(frmObj.Chk_Grp_ALL_Dist.checked == true && selAllChk!='Dist'){
		frmObj.Chk_Grp_ALL_Dist.checked = false;
	}
	if(fnGetUnCheckedValues(frmObj.Chk_GrpDist)==''){
		frmObj.Chk_Grp_ALL_Dist.checked = true;
	}
}
function fnCreateChkBox(grp,id,nm,pid,jsfunc)
{
	var html = "<input class='RightInput' type='checkbox' name='Chk_Grp"+grp+"' id='"+id+"' value='"+pid+"' onClick='javascript:"+jsfunc+";'>&nbsp;<span class='RightTextAS'>"+nm+"</span><BR>";
	return html;
}

//Function to get the checked values from the object passed
function fnGetCheckedValues(obj)
{
	var str = '';
	var arr = obj;
	 
	if (obj)
	{
		var arrlen = arr.length;
		//alert(arrlen);
		if (arrlen > 0)
		{
			for (var i=0;i< arrlen;i++ )
			{
				if (arr[i].checked == true)
				{
					str = str + arr[i].id + ',';
				}
			}
		
			if (str != '')
			{
				str = str.substr(0,str.length-1);
			}
		}
		else if(arr.checked == true)
		{
			str = arr.id;
		}
	}
	return str;
}
function sortFunction(a, b) {
	var x = a[1].toLowerCase();
	var y = b[1].toLowerCase();
	return ((x < y) ? -1 : ((x > y) ? 1 : 0));
}

function fnLoadPenExtDtls(loader) {
response = loader.xmlDoc.responseText;
fnStopProgress();
	if(format == 'MM/dd/yyyy'){//convert into JS Date format 
		dateFormat ='%m/%d/%Y';	
	}else if(format == 'dd/MM/yyyy'){
		dateFormat ='%d/%m/%Y';	
	}else if(format == 'dd.MM.yyyy'){
		dateFormat ='%d.%m.%Y';	
	}

			if (response != "" && response != undefined){
				document.getElementById("dataGridDiv").style.display = 'none';
				fnLoadPenExtDetails();
				/*  document.getElementById("dataGridDiv").style.display = 'block'; */
				document.getElementById("pagingArea").style.display = 'block';
				document.getElementById("hideExcel").style.display = 'block';
				document.getElementById("hideButtons").style.display = 'block';
				document.getElementById("comments").style.display = 'block';
				document.getElementById("hideTextarea").style.display = 'block';
				document.getElementById("hideline1").style.display = 'block';
				document.getElementById("hideline2").style.display = 'block';
			}else{
				document.getElementById("dataGridDiv").style.display = 'none';
				document.getElementById("pagingArea").style.display = 'none';
				document.getElementById("hideExcel").style.display = 'none';
				document.getElementById("hideButtons").style.display = 'none';
				document.getElementById("comments").style.display = 'none';
				document.getElementById("cmts").style.height = '0px';
				document.getElementById("hideTextarea").style.display = 'none';
				document.getElementById("hideline1").style.display = 'none';
				document.getElementById("hideline2").style.display = 'none';
				document.getElementById("DivNothingMessage").style.display = 'block';			
			}
		}

function fnLoadPenExtDetails(){
var strJsonString =  response;
//to form the grid string 
var rows = [], i = 0;
var JSON_Array_obj = "";
JSON_Array_obj = JSON.parse(strJsonString);
$.each(JSON_Array_obj, function(index,jsonObject) {
	reqdtlid = jsonObject.reqdtlid;
	reqid = jsonObject.reqid;
	cnid = jsonObject.cnid;
	comments = jsonObject.comments;
	setid = jsonObject.setid;
	setname = jsonObject.setname;
	repname = jsonObject.repname;
	acctname = jsonObject.acctname;
	requname = jsonObject.requname;
	loanerdt = jsonObject.loanerdt;
	oldexpdt = jsonObject.oldexpdt;
	extnreqdt = jsonObject.extnreqdt;
	surgdt = jsonObject.surgdt;
	extnreqretdt=jsonObject.extnreqretdt;
	apprretdt = jsonObject.apprretdt;
	extcount = jsonObject.extcount;
	adid = jsonObject.adid;
	sysid = jsonObject.sysid;
	hisfl = jsonObject.hisfl;
 	expdayout = dateDiff(loanerdt, extnreqretdt, format);
 	expdayout = expdayout+1;
//    expdayout = "10";
 	replnFl = jsonObject.replnfl;
	etchid = jsonObject.etchid;
	// default array
	rows[i] = {};
	rows[i].id = reqdtlid;
	rows[i].data = [];
	

				rows[i].data[0] = " ";
				rows[i].data[1] = reqdtlid;
				rows[i].data[2] = "<a href=\"#\" onClick=fnSalesReport('"+setid+"','"+adid+"','"+sysid+"')><img  src=\"/images/s.gif\" alt=\"View Sales Report\" style=\"cursor:hand\" " +
				"border=\"0\" disabled=\"true\"></a> "+reqdtlid;
				rows[i].data[3] = reqid;
				rows[i].data[4] = cnid;
				rows[i].data[5] = etchid;
				rows[i].data[6] = comments;
				rows[i].data[7] = setid;
				rows[i].data[8] = setname;
				rows[i].data[9] = repname;
				rows[i].data[10] = acctname;
				rows[i].data[11] = requname;
				rows[i].data[12] = loanerdt;
				rows[i].data[13] = oldexpdt;
				rows[i].data[14] = extnreqdt;
				rows[i].data[15] = surgdt;
				rows[i].data[16] = extnreqretdt;
				if (hisfl != '0'){
				rows[i].data[17] = "<a href=\"#\" onClick=fnHistoryLog('"+ cnid + "','Y')><img alt=\"History Details\" src=\"/images/icon_History.gif\" border=\"0\"  height=\"15\" width=\"15\" disabled=\"true\"></a>";
				}
				rows[i].data[18] = extnreqretdt;
				rows[i].data[19] = extcount;
				if(extcount > '0'){
					rows[i].data[20]= "<a href=\"#\" onClick=fnLoanerExtLog('"+ reqdtlid + "')><img alt=\"History Details\" src=\"/images/icon_History.gif\" border=\"0\"  height=\"15\" width=\"15\" disabled=\"true\"></a>";	
				}
				rows[i].data[21] = expdayout;
				rows[i].data[22] = replnFl;
				rows[i].data[23] = " ";
				

	i++;

});

//to load other than open status details in work order revision update report
	
var setInitWidths = "40,60,60,58,88,60,84,70,90,90,90,90,78,70,70,70,70,20,67,50,20,60,43,*";
var setColAlign = "center,left,left,left,left,left,left,left,left,left,left,center,center,center,center,center,center,center,right,right,center,center";
var setColTypes = "ch,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,dhxCalendarA,ro,ro,ro,ro,ro";
var setColSorting = "na,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,na,str,date,str,str,str";
var setHeader = ["","","Req ID","Parent Req ID", "Txn ID","Etch ID", "Comment",
	  				"Set ID", "Set Name", "Sales Rep", "Account", "Requestor Name",
	  				"Loaned On","Exp. Return Date","Extn. Req Date","Surgery Date","Extn. Req. Return Dt"," ","Appr Return Date","Previous Extend Count","","Exp. Days Out","Repln",""];
var setFilter = [ "<input type='checkbox'  value='no' name='mastercheck' onClick='javascript:fnChangedFilter(this);'/>", "#text_filter", "#numeric_filter", "#numeric_filter",
  				"#text_filter", "#text_filter", "#text_filter","#text_filter",
  				"#text_filter", "#text_filter", "#text_filter","#text_filter",
  				"#text_filter", "#text_filter", "#text_filter","#text_filter","#text_filter","#rspan","#text_filter","#numeric_filter","","#numeric_filter","#text_filter","#rspan" ];
	var setColIds = "checkBoxId,reqDetId,sreqdelid,reqId,Consign,etchid,comments,setid,setnm,salesrep,accnm,reqernm,loanon,expRetDate,expReturn,surgerydate,extrreqdate,hisfl,appretdate,pcnt,historylog,eout,replnfl,showImg";
	var enableTooltips = "true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true";
	var footerArry = [];
	var gridHeight = "";
	var footerStyles = [];
	var footerExportFL = false;
	var pagination = "Y";  
	var dataHost = {};
	dataHost.rows = rows;
	document.getElementById("dataGridDiv").style.display = 'block';

	document.getElementById("cmts").style.height = '24px';
	document.getElementById("DivNothingMessage").style.display = 'none';	
	gridObj = initGridData('dataGridDiv', gridHeight, dataHost,
			setHeader, setInitWidths, setColAlign, setColTypes,
			setColSorting, setColIds, enableTooltips, setFilter,
			footerArry, footerStyles, footerExportFL, pagination);
	
	gridObj.attachEvent("onCheckbox", doOnCheck);
	check_rowId = gridObj.getColIndexById("checkBoxId");
	chkboxRefId = gridObj.getColIndexById("checkBoxId");
	reqDetId_rowId = gridObj.getColIndexById("reqDetId");
	reqId_rowId = gridObj.getColIndexById("reqId");
	apprRetnDt_rowId = gridObj.getColIndexById("appretdate");
	surgdt_rowId = gridObj.getColIndexById("surgerydate");
	showImg_id = gridObj.getColIndexById("showImg");
	gridObj.setColumnHidden(1,true);
}
//This function is called while initiating the grid with customized dropdown
function initGridData(divRef, gridHeight, gridData, setHeader, setInitWidths,
		setColAlign, setColTypes, setColSorting, setColIds, enableTooltips,
		setFilter, footerArry, footerStyles, footerExportFL, pagination) {

	var gObj = new dhtmlXGridObject(divRef);
	gObj.enableColSpan(true);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/extweb/dhtmlx/dhtmlxGrid/imgs/");
	gObj.setDateFormat(dateFormat);

	if (setHeader != undefined && setHeader.length > 0) {
		gObj.setHeader(setHeader);
		var colCount = setHeader.length;
	}
	
	if (setInitWidths != "")
		gObj.setInitWidths(setInitWidths);
	if (setColAlign != "")
		gObj.setColAlign(setColAlign);
	if (setColTypes != "")
		gObj.setColTypes(setColTypes);
	if (setColSorting != "")
		gObj.setColSorting(setColSorting);
	if (enableTooltips != "")
		gObj.enableTooltips(enableTooltips);
	if (setFilter != "")
		gObj.attachHeader(setFilter);
	if (footerArry != undefined && footerArry.length > 0) {
		var footstr = eval(footerArry).join(",");
		if (footerStyles != undefined && footerStyles.length > 0)
			gObj.attachFooter(footstr, footerStyles);
		else
			gObj.attachFooter(footstr);
	}

	if (setColIds != "") {
		gObj.setColumnIds(setColIds);
	}

	if (gridHeight != "")
		gObj.enableAutoHeight(true, gridHeight, true);
	else
		gObj.enableAutoHeight(true, 400, true);

	gObj.init();
	if (pagination == "Y") {
		gObj.enablePaging(true, 50, 10, "pagingArea", true);
		gObj.setPagingSkin("bricks");
	}

	gObj.parse(gridData, "json");
	return gObj;
}

function fnCheckBoxSelections(val,arr)
{
	var valobj = val.split(",");
	var sval = '';
	var arrlen = arr.length;
	if (arrlen > 0)
	{
		for (var j=0;j< valobj.length;j++ )
		{
			sval = valobj[j];
			for (var i=0;i< arrlen;i++ )
			{
				if (arr[i].id == sval)
				{
					
					arr[i].checked = true;
					break;
				}
			}
		}
	}
	else
	{
		arr.checked = true;
	}
}

function getArrayOfValues(obj){
	var arr = obj;
	var j =0;
	var arrChk = new Array();
	if(obj == null){
		return arrChk;
	}
	var arrlen = arr.length;
	if(arrlen == undefined && arr.checked == true)
	{
		arrChk[j] = arr.id;
		j++;
	}else{
		for (var i=0;i< arrlen;i++ )
		{
			if (arr[i].checked == true)
			{
				arrChk[j] = arr[i].id;
				j++;
			}
		}
	}
	return arrChk;
}
//PMT-50520
function fnLoanerExtLog(reqdtlid){
	windowOpener("/gmLoanerExtReqRpt.do?method=fetchLoanerHistory&ReddtlId="+reqdtlid,"Search","resizable=yes,scrollbars=yes,top=150,left=150,width=1050,height=300");
}
function fnHistoryLog(id,fl)
{
	windowOpener("/gmLoanerExtReqRpt.do?method=fetchHistoryDtls&ReddtlId="+id+'&fl='+fl,"Search","resizable=yes,scrollbars=yes,top=150,left=150,width=540,height=300");
}