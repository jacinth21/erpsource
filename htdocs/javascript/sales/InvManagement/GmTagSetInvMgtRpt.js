var selectedTagIds = "";

//Description: function will call when enter key pressed.
function fnEnter(){
		if (event.keyCode == 13){ 
			fnLoad();
		}
}

function isNumberKey(evt)
{
   var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
      return false;

   return true;
}

function fnPageLoad(){
	
	var vZone = document.frmGmTagSetInvMgt.zone.value; 
	var vRegion = document.frmGmTagSetInvMgt.region.value; 
	var vDist = document.frmGmTagSetInvMgt.distributorId.value;
	var vRep = document.frmGmTagSetInvMgt.salesRepId.value;
	var vDecom = document.frmGmTagSetInvMgt.decommission.value;
	var vLock = document.frmGmTagSetInvMgt.lockInv.value;
	var vDivison = document.frmGmTagSetInvMgt.divison.value;
	var vstrOpt = document.frmGmTagSetInvMgt.strOpt.value;
	
	if(vZone != '0' || vRegion != '0'|| vRep != '0'|| vDecom != '0'|| vLock != '0'){
		fnShowMoreFilter();
	}
	if (vZone != '0'){
		fnFilterRegion(document.frmGmTagSetInvMgt.zone, 'VP');
	}
	if(vRegion != '0'){
		fnRegnFilterDist(document.frmGmTagSetInvMgt.region, 'AD');
	}
	if (vDist != '0'){
		fnDistFilterReps(document.frmGmTagSetInvMgt.distributorId, 'DIST');
	}
	if(vRep != '0'){
		fnFilterAccts(document.frmGmTagSetInvMgt.salesRepId, 'REP');	
	}
	
	//OnLoad pre-select US-Globus Medical Inc
	if (vDivison != undefined && vDivison==0 && vstrOpt != "Reload"){
		document.frmGmTagSetInvMgt.divison.value ="100823";
	}
	if(document.frmGmTagSetInvMgt.pageType.value=="report"){
		fnInitGrid();
	}
	
}



function fnInitGrid() {
	if(gridObjData!='')
	{
		mygrid = initGridWithDistributedParsing('dataGridDiv',gridObjData);		
		mygrid.attachHeader('#text_filter,#select_filter,#select_filter,#text_filter');

	}
}

//this function used to export the excel report
function fnExcelExport(){
	mygrid.toExcel('/phpapp/excel/generate.php');
}

function fnLoad(){
	var tagIdFrm = 	document.frmGmTagSetInvMgt.tagIdFrom.value;
	var tagIdTo  = 	document.frmGmTagSetInvMgt.tagIdTo.value;
	if((tagIdFrm == '' && tagIdTo != '') || ( tagIdFrm != '' && tagIdTo == '') || (parseInt(eval(tagIdFrm)) > parseInt(eval(tagIdTo)))){
		alert(message_sales[119]);
		return false;
	}
	fnStartProgress('Y'); 
	document.frmGmTagSetInvMgt.strOpt.value = 'Reload';
	document.frmGmTagSetInvMgt.action = '\gmTagSetInvMgt.do?method=tagSetInvReport';
	document.frmGmTagSetInvMgt.submit();
}

function fnTagID(chk){
	if(chk.checked){
		selectedTagIds += ','+chk.value;
	}else{
		selectedTagIds=selectedTagIds.replace(','+chk.value,'');
	}
}

function fnEditLoction(){
	
	if(selectedTagIds != ''){
	   fnStartProgress('Y');
		var strTagIds =  selectedTagIds.substr(1,selectedTagIds.length);
		var arrayTagID = strTagIds.split(',');
		var errTagIds = '';
		
		/* begin of validation - locked set */
		for(var i=0;i<arrayTagID.length;i++){
			var vTagId = arrayTagID[i];
			var obj = eval("document.frmGmTagSetInvMgt.Lock_"+vTagId);
			if(obj.value == 'Y'){
				errTagIds += ','+vTagId; 
			}
		}
		if(errTagIds != ''){
			Error_Details(Error_Details_Trans(message_sales[118],errTagIds.substring(1, errTagIds.length)));
			fnStopProgress();
			Error_Show();
			Error_Clear();
			return false;
		}
		/* End of validation - locked set */
		
		if(arrayTagID.length == 1){
			document.frmGmTagSetInvMgt.strOpt.value = 'load';
		}else{
			document.frmGmTagSetInvMgt.strOpt.value = '';
		}
		document.frmGmTagSetInvMgt.strTagIds.value = selectedTagIds.substr(1,selectedTagIds.length);
		document.frmGmTagSetInvMgt.action = '\gmTagSetInvMgt.do?method=tagSetLocation';
		document.frmGmTagSetInvMgt.submit();
	}else{
		alert(message_sales[120]);
		return false;
	}
}

function fnLockInv(){
	
	var strTagIds =  selectedTagIds.substr(1,selectedTagIds.length);
	var arrayTagID = strTagIds.split(',');
	var status = '';
	var vSubLocId = '';
	var errTagIds = '';
	if(strTagIds != ''){
		fnStartProgress('Y');
		for(var i=0;i<arrayTagID.length;i++){
			var vTagId = arrayTagID[i];
			var obj = eval("document.frmGmTagSetInvMgt.Lock_"+vTagId);
			if(i==0){
				status = obj.value;
			}else if(obj.value != status){
				errTagIds += ','+vTagId; 
			}
		}
		
		if(errTagIds != ''){
			var msg = (status=='Y')? 'Unlocked': 'Locked';
			var array=[msg,errTagIds.substring(1, errTagIds.length)]
			Error_Details(Error_Details_Trans(message_sales[121],array));
			fnStopProgress();
			Error_Show();
			Error_Clear();
			return false;
		}
		if(arrayTagID.length == 1){
			document.frmGmTagSetInvMgt.strOpt.value = 'load';
		}else{
			document.frmGmTagSetInvMgt.strOpt.value = '';
		}
		document.frmGmTagSetInvMgt.tagAttrStatus.value = status;
		document.frmGmTagSetInvMgt.tagAttrType.value = '11305';//lock set type
		document.frmGmTagSetInvMgt.cancelType.value = 'SETLCK';
		document.frmGmTagSetInvMgt.strTagIds.value = strTagIds;
		document.frmGmTagSetInvMgt.action = '\gmTagSetInvMgt.do?method=tagSetAttribute';
		document.frmGmTagSetInvMgt.submit();
	}else{
		alert(message_sales[122]);
		return false;
	}
}


function fnDecomission(){
	var strTagIds =  selectedTagIds.substr(1,selectedTagIds.length);
	var arrayTagID = strTagIds.split(',');
	var status = '';
	var errTagIds = '';
	
	if(strTagIds != ''){
	    fnStartProgress('Y');
		for(var i=0;i<arrayTagID.length;i++){
			var obj = eval("document.frmGmTagSetInvMgt.Decomsin_"+arrayTagID[i]);
			if(i==0){
				status = obj.value;
			}else if(obj.value != status){
				var msg = (status=='Y')? 'Active': 'Decommission';
				errTagIds += ','+arrayTagID[i]; 
			}
		}
		if(errTagIds != ''){
			fnStopProgress();
			var array=[msg,errTagIds.substring(1, errTagIds.length)]
			Error_Details(Error_Details_Trans(message_sales[121],array));
			Error_Show();
			Error_Clear();
			return false;
		} 
		if(arrayTagID.length == 1){
			document.frmGmTagSetInvMgt.strOpt.value = 'load';
		}else{
			document.frmGmTagSetInvMgt.strOpt.value = '';
		}
		document.frmGmTagSetInvMgt.tagAttrStatus.value = status;
		document.frmGmTagSetInvMgt.tagAttrType.value = '11310';//Decommission set type
		document.frmGmTagSetInvMgt.cancelType.value = 'SETDCM';
		document.frmGmTagSetInvMgt.strTagIds.value = strTagIds;
		document.frmGmTagSetInvMgt.action = '\gmTagSetInvMgt.do?method=tagSetAttribute';
		document.frmGmTagSetInvMgt.submit();
	}else{
		alert(message_sales[123]);
		return false;
	}
}




function fnTagImage(tagId){

	
    w1 =  createDhtmlxWindow();
    w1.setText(message_sales[124]); 
	w1.attachURL('/GmSetImgMgtServlet?TAGID='+tagId+'&hAction=Load');
	return false;
	
	//document.frmGmTagSetInvMgt.action = '/GmSetImgMgtServlet?TAGID='+tagId+'&hAction=Load';
	//document.frmGmTagSetInvMgt.submit();
}


function fnShowMoreFilter(){
	var vshow = (navigator.userAgent.indexOf("Safari")!=-1)?'table-row':'block';
	 var rowF1= document.getElementById("rowF1");
	 var divShow = document.getElementById("divShow");
	 var divLoad = document.getElementById("btnLoad");
      if(rowF1.style.display == vshow){
    	    document.getElementById("rowF1").style.display = 'none';
    	    document.getElementById("rowF2").style.display = 'none';
    	    document.getElementById("rowL").style.display = 'none';
	        divShow.innerHTML = "&nbsp;<a href=\"javascript:fnShowMoreFilter()\"><img alt=\"Click to view all Filters\" border=\"0\" src=\"images/plus.gif\"/>&nbsp;Show Filters</a>";
	        divLoad.innerHTML = loadInnerHtml;
       }else{
    	   document.getElementById("rowF1").style.display = vshow;
    	   document.getElementById("rowF2").style.display = vshow;
    	   document.getElementById("rowL").style.display = vshow;
	       divShow.innerHTML = "&nbsp;<a href=\"javascript:fnShowMoreFilter()\"><img alt=\"Click to hide Filters\" border=\"0\" src=\"images/minus.gif\"/>&nbsp;Hide Filters</a>";
	       divLoad.innerHTML = "";
       }
}

function fnFilterRegion(obj, type)
{ 
	var val = TRIM(obj.value);
	var selRegnId = document.all.region.value;
	document.all.region.options.length = 0;
	document.all.region.options[0] = new Option("[Choose One]","0");
	var k = 0;
	for (var i=0;i<regionLen;i++)
	{
		arr = eval("RegionArr"+i);
		arrobj = arr.split(",");
		id = arrobj[0];
		rid = arrobj[2];
		name = arrobj[3];
		if(id == val || val == '0')
		{
		document.all.region.options[k+1] = new Option(name,rid);
		k++;
		}
	}
	document.all.region.value = selRegnId;
	if(document.all.region.value == ''){
		document.all.region.value = '0';
	}
	fnRegnFilterDist(obj, 'VP');
} 

function fnRegnFilterDist(obj, type)
{ 
	var val = '0';
	if(type == 'VP'){
	val = TRIM(obj.value);
	}else{
		for(var j=0;j<regionLen;j++){
			arr = eval("RegionArr"+j);
			arrobj = arr.split(",");
			vpid = arrobj[0];
			adid = arrobj[1];
			rid = arrobj[2];
			name = arrobj[3];
			if(rid == TRIM(obj.value)){
				val = adid ;
				break;
			}
		}
	}
	var selDistId = document.all.distributorId.value;
	document.all.distributorId.options.length = 0;
	document.all.distributorId.options[0] = new Option("[Choose One]","0");
	var k = 0;
	for (var i=0;i<distLen;i++)
	{
		arr = eval("DistArr"+i);
		arrobj = arr.split(",");
		if(type == 'VP'){
			id = arrobj[0];
		}else{
			id = arrobj[1];
		}
		did = arrobj[2];
		name = arrobj[3];
		if(id == val || val == '0')
		{
		document.all.distributorId.options[k+1] = new Option(name,did);
		k++;
		}
	}
	document.all.distributorId.value = selDistId;
	if(document.all.distributorId.value == ''){
		document.all.distributorId.value = '0';
	}
}

function fnDistFilterReps(obj, type)
{ 
	var val = TRIM(obj.value);
	var selRepId = document.all.salesRepId.value;
	document.all.salesRepId.options.length = 0;
	document.all.salesRepId.options[0] = new Option("[Choose One]","0");
	var k = 0;
	for (var i=0;i<repLen;i++)
	{
		arr = eval("RepArr"+i);
		arrobj = arr.split(",");
		id = arrobj[0];
		rid = arrobj[2];
		name = arrobj[3];
		if(id == val || val == '0')
		{
		document.all.salesRepId.options[k+1] = new Option(name,rid);
		k++;
		}
	}	
	document.all.salesRepId.value = selRepId;
	if(document.all.salesRepId.value == ''){
		document.all.salesRepId.value = '0';
	}
	fnFilterAccts(obj, type);
	
} 

function fnFilterAccts(obj, type)
{ 
	var val = TRIM(obj.value);
	var selAccId = document.all.accountId.value;
	document.all.accountId.options.length = 0;
	document.all.accountId.options[0] = new Option("[Choose One]","0");
	var k = 0;
	for (var i=0;i<acctLen;i++)
	{
		arr = eval("AcctArr"+i);
		arrobj = arr.split(",");
		if(type=='DIST')
		{
			id = arrobj[0];
		}
		else 
		{
			id = arrobj[1];
		}
		aid = arrobj[2];
		name = arrobj[3];
		if(id == val || val == '0')
		{
		document.all.accountId.options[k+1] = new Option(name,aid);
		k++;
		}
	}
	document.all.accountId.value = selAccId;
	if(document.all.accountId.value == ''){
		document.all.accountId.value = '0';
	}
}
