var vshow = 'block'; 
if((navigator.userAgent.indexOf("IE")==-1)) {
	vshow = 'table-row';
}

function fnPageLoad(){
	var vAction = document.frmUploadTag.hAction.value;
	if (vAction != 'Large'){
		document.frmUploadTag.hFwdAction.value = vAction;
	}
	var objUpload = eval("document.all.imageUpload");
	var objIpadFun = eval("document.all.ipadFunction");
	
	if(navigator.userAgent.indexOf("iPad")==-1){
		objUpload.style.display = vshow;
	}else{
		objIpadFun.style.display = vshow;
	}
}

function fnOpenLarge(Id)
{
	document.frmUploadTag.hAction.value =  "Large";
	document.frmUploadTag.hImgId.value = Id;
	document.frmUploadTag.submit();
}
function fnVoid(){
	var inputstr = '';
	for(i=0;i<totalsize;i++){
		obj = eval("document.frmUploadTag.chk"+i);		
		if (obj.checked == true){
			inputstr = inputstr+obj.id+'|';
		}
	}
	if(inputstr ==''){
		Error_Details(message_sales[125]);
		Error_Show();
		Error_Clear();
		return false;
	}
	var VoidConfirm = confirm(message_sales[126]);
    if (VoidConfirm==false)
       return false;
	
	document.frmUploadTag.hAction.value =  "Void";
	document.frmUploadTag.hInputStr.value = inputstr;
	document.frmUploadTag.submit();
}
function fnUpload()
{
	var ImgName = document.frmUploadTag.uploadfile.value;
	

  if(ImgName == "")
  { 
	  Error_Details(message_sales[127]); 
	  Error_Show();
	  Error_Clear();
	  return false;
  }else{
	    document.frmUploadTag.hAction.value =  "Save";
	    document.frmUploadTag.submit();
    }
}
function fnBack(){
	var vFwdAction = document.frmUploadTag.hFwdAction.value;
	document.frmUploadTag.hAction.value = vFwdAction;
	document.frmUploadTag.action ="/GmSetImgMgtServlet";
	document.frmUploadTag.submit();
}
function fnClose()
{ 
	var vAction = document.frmUploadTag.hAction.value;
	if (vAction == 'Load'){
		 parent.window.fnLoad();
	}
	CloseDhtmlxWindow();
	return false;
}

function fnSetInvRpt(){
	document.frmUploadTag.strOpt.value = 'Reload';
	document.frmUploadTag.action = '\gmTagSetInvMgt.do?method=tagSetInvReport';
	document.frmUploadTag.submit();
}