function fnLoad(){
	var sysInputStr = '';
	var setsInputStr = '';
	var sysSel =  system.getSelectedValue();      
	var setName = document.all.setId.value;
	var distId = document.all.distributorId.value;
	var typeId = document.all.typeid.value;
	
	if((sysSel == '' || sysSel == '0' || sysSel == null) && (setName == '' || setName == '0') && (distId == '' || distId == '0') && (typeId == '' || typeId == '0')){
		Error_Details(message_sales[116]);
	}
	
	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}	
		
	document.all.strOpt.value = 'reload';
	document.all.action = '\gmTagSetInvMgt.do?method=fetchSetInvReport';
	
	fnStartProgress('Y');
	fnReloadSales();		
}
function fnOnPageload(){		
	if (objGridData != '') {
		gridObj = initGridData1('setInventoryRpt', objGridData);
		gridObj.enableHeaderMenu();
    }
	loadSets();		
}
function objStatusChk(obj)
{
	if(obj != null){	
		if (obj.style.display == 'none')
		{
			obj.style.display = 'block';
		}
		else
		{
			obj.style.display = 'none';	
		}
	}		
}
function initGridData1(divRef, gridData) {
	gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(false);
	gObj.enableTooltips("true,false,true,true,true,true,true,true,true,true,true,true");	
	gObj.init();
	gObj.loadXMLString(gridData);	
	return gObj;
}
function fnExportToExcel(){		
	gObj.toExcel('/phpapp/excel/generate.php');		
}

// this function is used to load sets based on system and type selected.
function loadSets(){
	var systemSel = system.getSelectedValue();
	var k=0;
	var flag = false;
	if(systemSel !='' && systemSel !='0' && systemSel != null){
		document.all.setId.options.length = 0;
		document.all.setId.options[k++] = new Option("[Choose One]","0");
		for (var i=0;i<SetArr.length;i++)
		{
			if(systemSel == SetArr[i][3]){
				document.all.setId.options[k++] = new Option(SetArr[i][1],SetArr[i][0]);
			}
			if(selectsetval == SetArr[i][0] && selectedSys == systemSel){
				flag = true;
			}
					
		}
		if(flag){
			document.all.setId.value=selectsetval;
		}
	}else{
		document.all.setId.options.length = 0;
		document.all.setId.options[k++] = new Option("[Choose One]","0");
		for (var i=0;i<SetArr.length;i++)
		{
			document.all.setId.options[k++] = new Option(SetArr[i][1],SetArr[i][0]);
		}
	}
}