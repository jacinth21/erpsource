var vshow = (navigator.userAgent.indexOf("Safari")!=-1)?'table-row':'block';
function fnChangeLoc(dval){
	var vCurLoc = document.frmGmTagSetInvMgt.curLocId.value;
	fnNameDrpDwn(document.frmGmTagSetInvMgt.curLocId,dval);
	if(vCurLoc == '50222'){
		fnGetAddressList(vAddId);
	}else if(vCurLoc == '50221' || vCurLoc == '11301'){
		fnGetAddressList(document.frmGmTagSetInvMgt.subLocId.value);
	}else{
		document.getElementById("subLocRow").style.display = 'none';
		document.getElementById("addRow").style.display = 'none';
	}
	
}

function fnChangeName(){
	var vCurLoc = document.frmGmTagSetInvMgt.curLocId.value;
	if(vCurLoc == '50222'){
		fnGetAddressList('0');
	}else{
		var val = document.frmGmTagSetInvMgt.subLocId.value;
		fnGetAddressList(val);
	}
}

function fnGetAddressList(dbval) {
	var vCurLoc = document.frmGmTagSetInvMgt.curLocId.value;
	var val = document.frmGmTagSetInvMgt.subLocId.value;

	if (val != '0')
	{
		reqFile ='/gmTagSetInvMgt.do?method=repAddressList&RefID='+val+'&Entity='+vCurLoc;
		var index = ajax.length;		
		ajax[index] = new sack();				
		ajax[index].requestFile = reqFile // Specifying which file to get				
		ajax[index].onCompletion = function(){ createAddresses(index,dbval) };	// Specify function that will be executed after file has been found
		ajax[index].runAJAX();	
	}else{
		fnEmptyDrpDwn(document.frmGmTagSetInvMgt.addressId);
	}
}

function fnEmptyDrpDwn(obj){
	obj.options.length = 0;
	obj.options[0]= new Option("[Choose One]","0");
}

function createAddresses(index,dbval)
{
	var obj = document.frmGmTagSetInvMgt.addressId;
	fnEmptyDrpDwn(obj);
	eval(ajax[index].response);	// Executing the response from Ajax as Javascript code	
	if (dbval != '')
	{
		obj.value = dbval;
	}
	var valAdd = document.frmGmTagSetInvMgt.addressId.value; 
	if(valAdd == ''){
		document.frmGmTagSetInvMgt.addressId.value = '0';
	}
	document.getElementById("addRow").style.display = vshow;
}

function fnSaveLoction(){
	
	
	fnValidateDropDn('curLocId',lblEntity);
	fnValidateDropDn('subLocId',lblName);
	fnValidateDropDn('addressId',lblLocation);
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	fnStartProgress('Y');
	document.frmGmTagSetInvMgt.strOpt.value = 'save';
	document.frmGmTagSetInvMgt.action = '\gmTagSetInvMgt.do?method=tagSetLocation';
	document.frmGmTagSetInvMgt.submit();
}

function fnSetInvRpt(){
	var vParentFrm = document.frmGmTagSetInvMgt.parentFrmName.value;
	var tagId = document.frmGmTagSetInvMgt.strTagIds.value;
	if(vParentFrm == '' ){ //go to Set Inventory report screen.
		fnStartProgress('Y');
		document.frmGmTagSetInvMgt.strOpt.value = 'Reload';
		document.frmGmTagSetInvMgt.action = '\gmTagSetInvMgt.do?method=tagSetInvReport';
		document.frmGmTagSetInvMgt.submit();
	}else if(vParentFrm =='frmTagTransfer'){ //go to Edit tag screen.
		document.frmGmTagSetInvMgt.action = '/gmTagTransfer.do?strOpt=fetch&tagId='+tagId;
		document.frmGmTagSetInvMgt.submit();
	}
	
}

function fnNameDrpDwn(obj,dval)
{ 
	//var val = TRIM(document.frmGmTagSetInvMgt.distId.value);
	var type = obj.value;
	document.all.subLocId.options.length = 0;
	document.all.subLocId.options[0] = new Option("[Choose One]","0");
	var k = 0;
		var arrLen = 0;
		var arrName = '';
		if(type == '50221'){
			arrLen = distLen;
			arrName = 'DistArr'; 
		}else if(type == '50222'){
			arrLen = repLen;
			arrName = 'RepArr'; 
		}else if(type == '11301'){
			arrLen = acctLen;
			arrName = 'AcctArr'; 			
		}
		 
		for (var i=0;i<arrLen;i++)
		{
			arr = eval(arrName+i);
			arrobj = arr.split(",");
			id = arrobj[0];
			name = arrobj[1];
			//if(id == val || val == '')
			//{
			document.all.subLocId.options[k+1] = new Option(name,id);
			k++;
			//}
		}
	document.all.subLocId.value = dval;
	document.getElementById("subLocRow").style.display = vshow;
}
