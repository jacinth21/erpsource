function fnSaveAttribute(){
    var vType = document.frmGmTagSetInvMgt.tagAttrType.value;
    var vAttStatus= document.frmGmTagSetInvMgt.tagAttrStatus.value;

    if(vType == '11305' && vAttStatus != 'Y'){
		fnValidateDropDn('lockAccId',' Lock Account ');
	}
	fnValidateDropDn('tagAttrReason',' Reason ');
	if (TRIM(document.frmGmTagSetInvMgt.tagAttrCmt.value) == ''){
		Error_Details(message_sales[117]);
	}
	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}
	fnStartProgress('Y');
	document.frmGmTagSetInvMgt.tagAttrValue.value = (vAttStatus == 'Y')? '':'Y';
	document.frmGmTagSetInvMgt.strOpt.value = 'save';
	document.frmGmTagSetInvMgt.action = '\gmTagSetInvMgt.do?method=tagSetAttribute';
	document.frmGmTagSetInvMgt.submit();
}

function fnSetInvRpt(){
	var vType = document.frmGmTagSetInvMgt.tagAttrType.value;
	fnStartProgress('Y');
	document.frmGmTagSetInvMgt.strOpt.value = 'Reload';
	document.frmGmTagSetInvMgt.action = '\gmTagSetInvMgt.do?method=tagSetInvReport';
	document.frmGmTagSetInvMgt.submit();
}
