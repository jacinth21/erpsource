

function fnSubmit()
{
	document.frmMain.submit();
}

function fnCallDetail(val)
{
	document.frmMain.DistributorID.value = val;
	document.frmMain.hAction.value = "LoadAccount";
	document.frmMain.submit();
}

function fnCountryName(val)
{
  
}

function fnLoad()
{
	document.frmMain.Cbo_FromMonth.value = fromMonth;
	document.frmMain.Cbo_ToMonth.value = toMonth;
	document.frmMain.Cbo_FromYear.value = fromYear;
	document.frmMain.Cbo_ToYear.value = toYear;
	fnLoadMapChart();
}
function formatCurrency(num) {
	num = num.toString();
	if(isNaN(num))
		num = "0";
	sign = (num == (num = Math.abs(num)));
	num = Math.floor(num*100+0.50000000001);
	cents = num%100;
	num = Math.floor(num/100).toString();
	if(cents<10)
		cents = "0" + cents;
	for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
		num = num.substring(0,num.length-(4*i+3))+','+num.substring(num.length-(4*i+3));
	
	return num+ '.' + cents;
}

function fnLoadMapChart() {    
    //When the data is empty dont render the chart.
  if(eval(mapchartDataArray.data) !=''){
  //USA state chart type declaration 
    var mapChartObj=  {
                "type": "usa",
                "renderAt": "chartContainer",
                "width": "950",
                "height": "500",
                "dataFormat": "json"
    };
    
  //Chart parameters declaration
    var mapchartOptions = {
            "entityFillHoverColor": "#cccccc",                
            "numberPrefix": currSymbol,
            "showLabels": "1",
            "caption": message_sales[263],
            "subcaption":message_sales[264]+" "+currSymbol +" "+totalSales,
            "subcaptionFontColor": subCaptionFontcolor,
            "showPercentValues":"1",
            "showPercentInToolTip":"1",
          "valueFontColor": "#000000",
          "showTooltip": "1",
          "showcanvasborder": "0",
          "toolTipColor":"#ffffff",
          "toolTipBorderThickness":"0",
          "toolTipBgColor":"#000000",
          "toolTipBgAlpha":"80",
          "toolTipBorderRadius":"2",
          "toolTipPadding":"5",
          
          "exportenabled" : "1",
          "exportatclient" : "0",
          "exporthandler" : fcExportServer,
          "html5exporthandler" : fcExportServer
    };  
            
    //Defining the color range for this USA Map chart     
  var mapcombinedChartData ={};    
  // var mapcombinedChartData ={"chart" : chartOptions, "colorrange" : [], "data": []};
  mapcombinedChartData.chart=mapchartOptions;
  mapcombinedChartData.data= eval(mapchartDataArray.data);  
  mapcombinedChartData.colorrange=mapchartDataArray.colorrange;
  mapChartObj.dataSource = mapcombinedChartData;
  
    var salesByStateChart = new FusionCharts( mapChartObj );    
  salesByStateChart.render();    
  }else{
	var divChartContainer = document.getElementById("chartContainer");
	if(divChartContainer){
			divChartContainer.innerHTML =  "<font style='font-family: verdana, arial, sans-serif;  font-size: 11px; color:#FF0000; text-decoration: None;'>"+message_sales[265]+"</font>";
	}
  }
}
