 function fnLoad(){
	    objStartDt = document.frmCaseListRpt.fromDt;
		objEndDt = document.frmCaseListRpt.toDt;
		objCaseId = document.frmCaseListRpt.caseId
		
		
		if(objStartDt.value == "")
		{
			if(objCaseId.value == "" || objEndDt.value != "" )
			{
				fnValidateTxtFld('fromDt',lblFromDate);
			}
		}
		if(objEndDt.value == '')
		{
			if(objCaseId.value == "" || objStartDt.value != "" )
			{
				fnValidateTxtFld('toDt',lblToDate);
			}
		}
		//Common Date Validation added
		if(objStartDt.value != ""){
			CommonDateValidation(objStartDt, format, Error_Details_Trans(message_sales[25],format));
		}
		if(objEndDt.value != ""){
			CommonDateValidation(objEndDt, format, Error_Details_Trans(message_sales[26],format));
		}
		if (ErrorCount > 0)
		{
			Error_Show();
			Error_Clear();
			return false;
		}
	 	document.frmCaseListRpt.strOpt.value = "load"
		document.frmCaseListRpt.submit();
 }
 
function fnUpperCase(obj){
	obj.value = obj.value.toUpperCase();
}