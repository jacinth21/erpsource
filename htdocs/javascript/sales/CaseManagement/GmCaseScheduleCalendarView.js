// This function used to page onLoad 
function fnOnPageLoad(sessApplDateFmt,data) {
    if(sessApplDateFmt!='' && data!='' ){
	 var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmCaseScheduleCalendar.do?method=fetchCaseSchedulerCalendarInfo&strOpt='+data+'&applDateFmt='+sessApplDateFmt);
	 dhtmlxAjax.get(ajaxUrl,function(loader){
		 fnSchedulerCal(loader);
			});
   }
}

// This function used to view calendar information 
function fnSchedulerCal(loader){
	scheduler.clearAll();
	var resJSONObj = $.parseJSON(loader.xmlDoc.responseText);
	var newobj=[];
    for(i= 0; i<resJSONObj.length;i++){
    newobj[i]={};
    newobj[i].text=resJSONObj[i].ACCOUNTNM;
    newobj[i].case_Id = resJSONObj[i].CASE_ID;
    newobj[i].status = resJSONObj[i].STATUS;
    newobj[i].start_date=resJSONObj[i].START_DATE;
    newobj[i].end_date=resJSONObj[i].END_DATE;
    newobj[i].id=resJSONObj[i].INFOID;
    newobj[i].surgery_date=resJSONObj[i].SURGERY_DATE;
    newobj[i].repnm=resJSONObj[i].REPNM;
    newobj[i].distnm=resJSONObj[i].DISTNM;
    }
    var events=JSON.stringify(newobj);
	var objMode = document.frmCaseScheduleCalendar.haction;
     
	if(objMode.value == ''){
		clendarMode = 'month';
	}else{
		clendarMode = objMode.value;
	}
	scheduler.config.fix_tab_position = false;
	scheduler.config.multi_day = true;
	scheduler.config.touch_tooltip = true;
	if(sessApplDateFmt== 'dd/MM/yyyy'){
		scheduler.config.xml_date="%d/%m/%Y %H:%i";
		var datecheck = fnDateFmtOnBlurCheck(fromDate,sessApplDateFmt);
		scheduler.init('caseScheduleCalender', datecheck, clendarMode);
	}else{
		scheduler.config.xml_date="%m/%d/%Y %H:%i";
		scheduler.init('caseScheduleCalender', new Date(fromDate), clendarMode);
	}	
    scheduler.parse(events,"json");           
      
    // Tool tip start
    if(sessApplDateFmt== 'dd/MM/yyyy')
       var format = scheduler.date.date_to_str("%d/%m/%Y"); 
    else
       var format = scheduler.date.date_to_str("%m/%d/%Y"); 
       
	   scheduler.templates.tooltip_text = function(start,end,event) {
		    return "<p><h3><span style='background: #ff4d4d; font-weight: bold; border-width:10px; border-style:solid; border-color:#ff4d4d; display: block;'>"+event.text+"</span></h3> <br/>" +
		    		"<b>Surgery Date : </b> "+(event.surgery_date).split(' ')[0]+"<br/><b>Ship Date :</b> "+ format(event.start_date)
		    +"<br/><b>Return Date :</b> "+format(event.end_date)+
		    "<hr  style='background-color: #333;'><b>Field Sales: </b>"+event.distnm+
		    "<br/><b>Sales Rep: </b>"+event.repnm+"<br/><a href='#' onclick=fnCaseDetails('"+event.case_Id+"','"+event.status+"')>Case Details</a></p>";
		};
		scheduler.dhtmlXTooltip.config.delta_x = 2; 
		scheduler.dhtmlXTooltip.config.delta_y = -2;
	// tool tip end
}

function fnDateFmtOnBlurCheck(objDt,format) {
	   var val = objDt;
		var validformat='';
		var splitby='';
	      var yy;
	    var full_date=new Array();
	    //If format value is empty or null, it will set the default format as 'MM/dd/yyyy'
	    format=(format==null || format=='')?'MM/dd/yyyy':format;
	    if(val==''|| val==null)
	    	{
	    		return null;
	    	}
	    else{
	    	val= TRIM(objDt);
	    	
	    }
	    // This below expression for accepting 6 or 8 digit number or 2digits month-day/2digits month-day/2 or 4 digits year
	    // var regexp = /^((\d{6}$|\d{8}$)|(\d{2}\/\d{2}\/(\d{2}|\d{4})$))/;
	    // d{6}- 010114
	    // d{8}- 01012014
	    // d{2}\/\d{2}\/(\d{2}- 01/01/14
	    // d{2}\/\d{2}\/(\d{4}- 01/01/2014
		switch(format){
		case 'MM/dd/yyyy': 
			validformat=/^((\d{6}$|\d{8}$)|(\d{2}\/\d{2}\/(\d{2}|\d{4})$))/; 
			splitby="/";
			break;
		case 'dd.MM.yyyy': 
			validformat=/^((\d{6}$|\d{8}$)|(\d{2}\.\d{2}\.(\d{2}|\d{4})$))/; 
			splitby=".";
			break;
		case 'dd/MM/yyyy': 
			validformat=/^((\d{6}$|\d{8}$)|(\d{2}\/\d{2}\/(\d{2}|\d{4})$))/; 
			splitby="/";
			break;
		}
	    if(val.match(validformat)){ // Here Code checked the mentioned date formats above.

		    if (val.indexOf(splitby) != -1) {// If date comes with any symbol / or .
		        full_date = val.split(splitby);
		    } else {
		    	full_date[0] = val.substring(0, 2);
		    	full_date[1]= val.substring(2, 4);
		    	full_date[2]= val.substring(4);
	          }
	        yy = full_date[2];
	        yy = yy.length == 2 ? '20' + yy : yy;//If year 2 digit 
	        full_date[2] = yy;
	        objDt = full_date[0] + splitby + full_date[1] + splitby + full_date[2];
	   return objDt.value;
	    }else{
	    	Error_Details(message[10529]);
	    	Error_Show();
			Error_Clear();
			return false;	    	
	    }
	}

// This is used to calendar icon view Mini Calendar 
  function show_minical(){
		if (scheduler.isCalendarVisible())
			scheduler.destroyCalendar();
		else
			scheduler.renderCalendar({
				position:"dhx_minical_icon",
				date:scheduler._date,
				navigation:true,
				handler:function(date,calendar){
					scheduler.setCurrentView(date);
					dtSchedule = convert(date);
					document.frmCaseScheduleCalendar.fromDt.value = dtSchedule;
					document.frmCaseScheduleCalendar.toDt.value = dtSchedule;
					scheduler.destroyCalendar()
				}
			});
	}

function fnCaseDetails(CaseId,status){
	 windowOpener("\gmCaseSchedular.do?caseId="+CaseId+"&status="+status+"&companyId="+companyId,"Search","resizable=no,scrollbars=yes,top=150,left=150,width=835,height=600");
}

//This is used to Case Filter buttons 
function fnBasedOnTerm(sessApplDateFmt,paramTerm){
	if(sessApplDateFmt!='' && paramTerm!=''){	
	 var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmCaseScheduleCalendar.do?method=fetchCaseSchedulerCalendarInfo&strOpt='+paramTerm+'&applDateFmt='+sessApplDateFmt);
	 dhtmlxAjax.get(ajaxUrl,function(loader){
		 fnSchedulerCal(loader);
	 });
	}
}

//This is used to Active Case Filter buttons
function fnActiveButton(e) {
	  if (document.querySelector('#groupBtn a.actives') !== null) {
	    document.querySelector('#groupBtn a.actives').classList.remove('actives');
	  }
	  e.target.className = "actives";
	}
