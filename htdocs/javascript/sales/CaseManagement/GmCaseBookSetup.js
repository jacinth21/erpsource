
function fnValidation()
 {
	fnValidateTxtFld('surgeryDate',lblSurgeryDate);
 	fnValidateDropDn('surgeryTimeHour',message_sales[237]);
 	fnValidateDropDn('surgeryTimeMin',message_sales[238]);
 	fnValidateDropDn('surgeryAMPM',message_sales[239]);
 	fnValidateDropDn('distributorId',lblFieldSalesName);
 	fnValidateDropDn('repId',lblSalesRep);
	/*
	var currdate = new Date(strToday);
	var surgerydate = new Date(document.frmCaseBookSetup.surgeryDate.value); 
	if (surgerydate <  currdate){
		Error_Details("Surgery date can not be earlier than Today.");
	}
 	*/
 }

function fnSubmit()
{
	fnValidation();
	if (ErrorCount > 0)
	 {
		Error_Show();
		Error_Clear();
		return false;
	}
	document.frmCaseBookSetup.strOpt.value = 'save';
	fnStartProgress('Y');
	document.frmCaseBookSetup.submit();
}


 function fnNext()
 {
	fnValidation();
	if (ErrorCount > 0)
	 {
		Error_Show();
		Error_Clear();
		return false;
	}
 	document.frmCaseBookSetup.strOpt.value = 'next';
 	document.frmCaseBookSetup.submit();
 } 

function fnDistFilterReps(obj, type)
	{ 
		//var val = document.all.accountId.value;
		var val = TRIM(obj.value);
 
		document.all.repId.options.length = 0;
		document.all.repId.options[0] = new Option("[Choose One]","0");
		var k = 0;
		for (var i=0;i<repLen;i++)
		{
			arr = eval("RepArr"+i);
			arrobj = arr.split(",");
			id = arrobj[0];
			rid = arrobj[2];
			name = arrobj[3];
			if(id == val)
			{
			document.all.repId.options[k+1] = new Option(name,rid);
			k++;
			}
		}	
		fnFilterAccts(obj, type);
		
	} 

function fnFilterAccts(obj, type)
	{ 
		var selectedVal = document.all.accountId.value;
		var val = TRIM(obj.value);
		 
		document.all.accountId.options.length = 0;
		document.all.accountId.options[0] = new Option("[Choose One]","0");
		var k = 0;
		for (var i=0;i<acctLen;i++)
		{
			arr = eval("AcctArr"+i);
			arrobj = arr.split(",");
			if(type=='DIST')
			{
				id = arrobj[0];
			}
			else 
			{
				id = arrobj[1];
			}
			aid = arrobj[2];
			name = arrobj[3];
			if(id == val)
			{
				document.all.accountId.options[k+1] = new Option(name,aid);
				if(selectedVal == aid){
					document.all.accountId.options[k+1].selected = true;
				}
				k++;
			}
		}
	}
  


function fnShowAllFilterAccts()
	{ 
		var distid = TRIM(document.all.distributorId.value);
		var repid = TRIM(document.all.repId.value);
		var objrep = document.all.repId;
		if(distid =='0'){ alert(message_sales[18]); document.all.checkSelectedAll.checked = false;return false;}
		if(document.all.checkSelectedAll.checked)
		{
				
			document.all.accountId.options.length = 0;
			document.all.accountId.options[0] = new Option("[Choose One]","0");
			var k = 0;
			for (var i=0;i<acctLen;i++)
			{
				arr = eval("AcctArr"+i);
				arrobj = arr.split(",");
				id = arrobj[0];
				aid = arrobj[2];
				name = arrobj[3];
				if(id == distid)
				{
				document.all.accountId.options[k+1] = new Option(name,aid);
				k++;
				}
			}	
		}
		else if (repid!='')
		{
			fnFilterAccts(objrep, 'REP');
		}
	}

function fnPageLoad(){
	var obj = document.all.repId;
	if (obj.value != ""){
		fnFilterAccts(obj, "REP");
	}
} 
