

function doOnLoad(setid,setnm) {
 
	w1 = createDhtmlxWindow(930,700);
    w1.setText(message_sales[206]); 
	w1.attachURL("/GmSetReportServlet?hOpt=SETRPT&hAction=Drilldown&hSetId="+setid+ "&hSetNm="+setnm);
	return false;
}

 
function fnEdit()
{ 	  
    document.frmCasePost.action = "gmCaseBookSetup.do?strOpt=edit&caseInfoID="+strCaseInfoID ;
	document.frmCasePost.submit();
}
function fnLoad()
{ 	  
    document.frmCasePost.action = "gmCasePost.do?caseInfoID="+strCaseInfoID ;
	document.frmCasePost.submit();
}
function fnReschedule()
{
	document.frmCasePost.action="gmCaseBookReschedule.do?strOpt=editresch&caseInfoID="+strCaseInfoID ;
	document.frmCasePost.submit();
}
function fnFavourite()
{
	document.frmCasePost.action="gmFavouriteCase.do?caseID="+strCaseID+"&caseInfoID="+strCaseInfoID;
	document.frmCasePost.submit();
}


function viewImg(tagId) {
	
		w1 = createDhtmlxWindow(850,400);
	    w1.setText(message_sales[207]); 
		w1.attachURL("/GmSetImgMgtServlet?TAGID="+tagId+"&hAction=View");
		return false;
	
}

function fnSetOverride(vSetId,vSetName,vCaseDistId,vCaseSetId,vCaseInfoId,vLockFromDt,vLockToDt){
	var vURL = '';
	w1 = createDhtmlxWindow(1020,700);
    w1.setText(message_sales[208]); 
    vURL = "/gmSetOverrideRpt.do?strOpt=load&setId="+vSetId+"&setName="+vSetName+"&lockFromDt="+vLockFromDt+"&lockToDt="+vLockToDt+"&caseDistId="+vCaseDistId+"&caseSetId="+vCaseSetId+"&caseInfoId="+vCaseInfoId+"&parentFrmName=frmCasePost";
	w1.attachURL(vURL);
	return false;
}
function fnBookNewDO(){
	if(strPhoneOrder == 'Y'){
		var targetUrlStr ="/GmPageControllerServlet?strPgToLoad=/gmIncShipDetails.do?screenType=Order&RE_FORWARD=gmOrderItemServlet&strOpt=PHONE&accId="+accountID+"&caseInfoId="+caseInfoID+"&caseId="+strCaseID;
		var parentLcn = parent.location.href;
		if(parentLcn.indexOf("GmEnterprisePortal") != -1)
		{
			location.href = targetUrlStr;
		}
		
		else if(parentLcn.indexOf("GmGlobusOnePortal")!= -1)
		{
		location.href = targetUrlStr;
		}
		else
		{
			parent.location.href = targetUrlStr;
		}
	}else{
		document.frmCasePost.action = "gmOrderDetails.do?method=saveUsageDeatils&caseID="+strCaseID+"&repID="+accountRepID+"&accountNm="+accountNm+"&accountID="+accountID+"&caseInfoID="+caseInfoID+"&surgeryDt="+surgeryDt;
		document.frmCasePost.submit();
	}
}
function fnClose(){
	   parent.window.fnLoad();
	   parent.dhxWins.window("w1").close();
	   return false;
}
function fnAddress(shipindetail,setloctype){
	var vOpt = (setloctype == "11378")?"11401":setloctype
	 	 	
	   	var  shipin=shipindetail.split("~");
	   	w1 = createDhtmlxWindow(810,500);
	    w1.setText(message_sales[209]); 
		w1.attachURL("/gmAreaSetShipping.do?method=EditAddress&parentFrmName=frmCasePost&repID="+shipin[1]+"&addID="+shipin[3]+"&refID="+shipin[0]+"&shipType="+shipin[2]+"&shipMode="+shipin[4]+"&strOpt="+vOpt+"&caseDistID="+distID+"&caseRepID="+repID+"&caseAccID="+accountID);
	   
	   return false;
}
function fnPageLoad(){
	var msg = '';
	if(eval(document.all.hTagDelFl)!= undefined && eval(document.all.hTagDelFl).value == 'Y'){
		if (strStatusID == '11093')
		{
			msg = '<font color="red">'+message_sales[20]+'</font>';
		}else{
			msg = '<font color="red">'+message_sales[21]+'</font>';
		}
		document.getElementById("spnMsg").innerHTML=msg;
	}
	/* below code is used to show reschedule success message */
	if(document.all.strOpt.value == "reschedule" && strStatusID == 11091){
		msg = (msg != '')? msg+'<br><br>':''; // add breaks if msg has some warning message
		msg += '<font color="green" style="font-weight:bold;"><center>&nbsp;'+message_sales[19]+'</center></font>';
		document.getElementById("spnMsg").innerHTML=msg;
	}
	if(document.frmCasePost.btnInitLoan != undefined && SwitchUserFl != 'Y'){
		if(document.getElementById("hLoanerFlag") != undefined  && document.getElementById("hLoanerFlag").value == 'Y'){
			document.frmCasePost.btnInitLoan.disabled = false;
			document.getElementById("spnSelAll").innerHTML="<input type=checkbox name='chkSelAll' onClick='fnSelectAll(this);'/>";
		}else{
			document.frmCasePost.btnInitLoan.disabled = true;
		}
	}
}

function fnSelectAll(objAll){
	var i=0;
	var obj = eval("document.all.chkLoan"+i);
	while(obj!=undefined){
		obj.checked = objAll.checked;
		obj = eval("document.all.chkLoan"+i++);
	}
}

function fnChkLoaner(obj){
	var objAll = document.all.chkSelAll;
	if(obj.checked){
		var i=0,k=0;
		var obj = eval("document.all.chkLoan"+i);
		while(obj!=undefined){
			k=(obj.checked)?k+1:k;
			obj = eval("document.all.chkLoan"+i++);
		}
		if(k==i){
			objAll.checked = true;
		}
	}else if(objAll.checked){
		objAll.checked = false;
	}
}

function fnInitiateLoaner(){
	var i=0;
	var inputString = '';
	var obj = eval("document.all.chkLoan"+i);
	while(obj!=undefined){
		if(obj.checked){
			var val = obj.value;
			var tag = eval("document.all.hTag"+val);
			var set = eval("document.all.hSet"+val);
			var tagVal = tag.value;
			var setVal = set.value;
			
			if(tagVal != "" && tagVal != "TBD"){
				var array=[tagVal,setVal];
				var confirmval = confirm(Error_Details_Trans(message_sales[22],array));
				if (!confirmval)
					return false;
			}
			inputString += val+",";
		}
		i++;
		obj = eval("document.all.chkLoan"+i);
	}
	if(inputString == ''){
		Error_Details(message_sales[23]);
		Error_Show();
		Error_Clear();
		return false;
	}
	document.frmCasePost.inputString.value = inputString;
	document.frmCasePost.strOpt.value = "initLoaner";
	document.frmCasePost.btnInitLoan.disabled = true;
	document.frmCasePost.submit();
}

function fnActiveEdit()
{ 	  
    document.frmCasePost.action = "/gmCaseBuilder.do?strOpt=edit&caseInfoID="+ caseInfoID+ "&caseID="+ strCaseID;
	document.frmCasePost.submit();
}

function fnCaseVoid(){
	 
    document.frmCasePost.hCancelType.value = 'CLCASE';           
    document.frmCasePost.hTxnId.value = strCaseInfoID; 
    document.frmCasePost.hTxnName.value = strCaseID; 
    document.frmCasePost.action ="/GmCommonCancelServlet";            
    document.frmCasePost.hAction.value = "Load";                     
    document.frmCasePost.submit();
}

function fnFetchDOSummary(orderId){
document.frmCasePost.action ='/GmEditOrderServlet?hMode=PrintPrice&hOrdId='+orderId+'&hParantForm=CASE&hidePartPrice=N';
document.frmCasePost.submit();
}