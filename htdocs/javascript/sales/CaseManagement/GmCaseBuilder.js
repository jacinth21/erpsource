
 function fnCheckSelections()
{
	objCheckApprArrLen = 0;
	objCheckRegionArrLen = 0; 
	
	var appObj = sCategories.split(",");
	objCheckCategoriesArr = document.frmCaseBuilder.checkCategories;
	if(objCheckCategoriesArr) {
	 
		objCheckCategoriesArrLen = objCheckCategoriesArr.length;
		if(document.frmCaseBuilder.favouriteCaseID.value !=0 || strCaseInfoID !=''){
							
		for(k = 0; k < objCheckCategoriesArrLen; k ++) 
		{	
			 
				objCheckCategoriesArr[k].checked = false; 
		}

 		for (var j=0;j< appObj.length;j++ )
 		{ 
			for(i = 0; i < objCheckCategoriesArrLen; i ++) 
				{	
					if (objCheckCategoriesArr[i].value == appObj[j])
	 				{
					objCheckCategoriesArr[i].checked = true;
	 				}
				
				}
 		}
		
		}//end if favouriteCaseID

	} 
  
}	 

function fnSelectAll(obj){
	var objCheckCategoriesArr = document.frmCaseBuilder.checkCategories;
	for( var j=0; j<categoriesLen; j++)
	{
		objCheckCategoriesArr[j].checked= (obj.checked)?true:false;	
	}
}

function fnSelectCategories(){
	var k = 0;
	var objCheckCategoriesArr = document.frmCaseBuilder.checkCategories;
	for( var j=0; j<categoriesLen; j++)
	{
		if(objCheckCategoriesArr[j].checked){
			k++;
		}else{
			 document.frmCaseBuilder.checkSelectedAll.checked = false;
			 return;
		}	
	}
	if(k == categoriesLen){
		document.frmCaseBuilder.checkSelectedAll.checked = true;
	}
}


	
 function fnBack()
 {  
	document.frmCaseBuilder.action = "gmCaseBookSetup.do?strOpt=edit&caseInfoID="+strCaseInfoID ;
 	 
 	document.frmCaseBuilder.submit();
 }


 function fnValidation()
 {
	var objCheckCategoriesArr = document.frmCaseBuilder.checkCategories;
	var categoryCheck = '0';
    for( var j=0; j< objCheckCategoriesArr.length; j++)
 	{ 
		if(objCheckCategoriesArr[j].checked)	
		{
			categoryCheck = '1';	
		}
	}
    if( categoryCheck =='0')
 	{
    	return true;
 	}
    return false;
 }
 
 function fnSubmit()
 {
	 var optval = document.frmCaseBuilder.strOpt.value;
	 if (optval =='fetchFavorite' && strCaseInfoID =='')
	{
		fnValidateTxtFld('favouriteCaseNM',message_sales[245]);
	}
	if(fnValidation()){
		if (optval =='fetchFavorite' && strCaseInfoID =='')
		{
			Error_Details(message_sales[24]);
		}else{
			/*	Comment for MNTTASK-7882
			var confirmval = confirm("Category is not selected. Do you still want to proceed for system selection?")
			if (!confirmval)
				return false;
				*/
		}
	}
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}

 	document.frmCaseBuilder.strOpt.value = 'save';
 	fnStartProgress('Y');
 	document.frmCaseBuilder.submit();
 }


 function fnNext(val)
 {  
	 if(val=='ProfileNext')
	 {
		var optval = document.frmCaseBuilder.strOpt.value;
		if (optval =='fetchFavorite')
		{
			fnValidateTxtFld('favouriteCaseNM',lblFavoriteName); 
		}
	 }
	 if(fnValidation()){
		 if (optval =='fetchFavorite' && strCaseInfoID =='')
		 {
			 Error_Details(message_sales[24]);
		 }else{
/*			 Comment for MNTTASK-7882
				var confirmval = confirm("Category is not selected. Do you still want to proceed for system selection?")
			    if (!confirmval)
			       return false;*/
		 }
	 }
	if (ErrorCount > 0)
	 {
		Error_Show();
		Error_Clear();
		return false;
	}
	document.frmCaseBuilder.strOpt.value = val;
 	document.frmCaseBuilder.submit();
 }

 function fnReset(){
	 var obj = document.frmCaseBuilder.checkSelectedAll;
	 obj.checked = false;
	 fnSelectAll(obj);
} 