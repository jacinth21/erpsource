	/**********************************************************************************
	 * File		 		: GmItemRequestApproval.js
	 * Desc		 		: For Item Request Approval.
	 * author			: SaravananM
	 ************************************************************************************/
var selectedReqIds = "";
var selectedRowCnt="";
var apprQty="";
var inputStr="";
var reqId ="";
var partNo ="";
var priceNoteDisplayFL = false;
var selectfl = false;
var confirmMsg = true;
var returnqty = "";
var distid = "";
var gridObj="";
var partInputstr = '';
var raqtyInputstr = '';
var temppartreturnstr = '';
var temppartQtystr = '';
var repid = '';
var qty = '';
function fnSelectReqID(chk){
	if(chk.checked){
		selectedReqIds += ','+chk.value;
	}else{
		selectedReqIds=selectedReqIds.replace(','+chk.value,'');
	}
}

var HashMap = function(){
    var map={};
    this.put = function(key,val){
       
        if(map[key] != undefined){
            map[key] = map[key] +"^"+val;
        }else{
            map[key] = val;
        }
    };
    this.get = function(key){
        return map[key];
    };
    this.getInputStr= function(){
        var string = "";
        for(var attributename in map){
        string+=attributename+"^"+map[attributename]+"|";
        }
        return string;
    };
} 

var partstr = new HashMap();
var raqtystr =  new HashMap();
var inpApprvString = '';
var inpBoString = '';
function fnCreateApprovalString(){
	var gridrows = gridObj.getAllRowIds();
	var rowsarr = gridrows.toString().split(",");
	distrow_id = gridObj.getColIndexById("FIELDSALESID");
	var reprow_id = gridObj.getColIndexById("REPID");
	var userType = document.frmItemRequestApproval.userType.value;
	inputStr = '';
	var reqNameStr = '';
	var selectedApprvReqDtl = {};
	var selectedBoReqDtl = {};
	var selectedApprvReqArr = [];
	var selectedBoReqArr = [];
	var boQty = '';
	var apprvObjIndex = 0;
	var boObjIndex = 0;
	var lnReqId='';
		inpApprvString = '';
		inpBoString = '';
	if (rowsarr.length > 0) {
		disableChkFl = gridObj.getColIndexById("disableFl");
		for ( var rowid = 0; rowid < rowsarr.length; rowid++) {
			var rowcurrentid = rowsarr[rowid];			
			if(disableChkFl != undefined && disableChkFl != ""){
				 needDisableFl = gridObj.cellById(rowcurrentid, disableChkFl).getValue();	 
			}else{
				 needDisableFl = 'N';
			}
			if(needDisableFl == 'N' ){						 
				var checkInd = gridObj.cells(rowcurrentid,gridObj.getColIndexById("check")).getValue();
				
				
				lnReqId = gridObj.cells(rowcurrentid, gridObj.getColIndexById("LNREQID")).getValue();
				reqId = gridObj.cells(rowcurrentid, gridObj.getColIndexById("REQUESTID")).getValue();
				partNo =  gridObj.cells(rowcurrentid, gridObj.getColIndexById("PNUM")).getValue();
				apprQty =  gridObj.cells(rowcurrentid, gridObj.getColIndexById("APPROVE_QTY")).getValue();
				qty =  gridObj.cells(rowcurrentid, gridObj.getColIndexById("QTY")).getValue();
				boQty =  gridObj.cells(rowcurrentid, gridObj.getColIndexById("BO_QTY")).getValue();
				if(userType == "PD"){
				returnqty = gridObj.cells(rowcurrentid, gridObj.getColIndexById("RETNQTY")).getValue();
				}
				repid = gridObj.cells(rowcurrentid,reprow_id).getValue();
				distid = gridObj.cells(rowcurrentid,distrow_id).getValue();
				
				if(checkInd !='0'){
					reqName = gridObj.cells(rowcurrentid, gridObj.getColIndexById("REPNAME")).getValue();
					reqNameStr += (reqNameStr.indexOf(reqName) != -1)? "":", "+reqName;
					if(apprQty > 0){
						inputStr += reqId+','+partNo+','+apprQty+'|';
						//PC-3849 - back order for item request
						//To create Approve Json
						window["apprvObj"+apprvObjIndex] = new Object();
						window["apprvObj"+apprvObjIndex].strReqId = lnReqId;
						window["apprvObj"+apprvObjIndex].strPartNo = partNo;
						window["apprvObj"+apprvObjIndex].strApprvQty = apprQty;
						selectedApprvReqArr.push(window["apprvObj"+apprvObjIndex]);
						inpApprvString = JSON.stringify(selectedApprvReqArr);
						apprvObjIndex++;
					}
					
					//PC-3849 - back order for item request 
					//To create Bo json
					if(boQty > 0){
		          window["boObj"+boObjIndex] = new Object();
							window["boObj"+boObjIndex].strReqId = lnReqId;
							window["boObj"+boObjIndex].strPartNo = partNo;
							window["boObj"+boObjIndex].strBoQty = boQty;
							selectedBoReqArr.push(window["boObj"+boObjIndex]);
							inpBoString = JSON.stringify(selectedBoReqArr);
							boObjIndex++;
					}
					
					
					if(returnqty != "" && returnqty != "0"){
						partstr.put(distid+'@'+repid,partNo);
						raqtystr.put(distid+'@'+repid,returnqty);
					}
				}
			}
		}
		partInputstr = partstr.getInputStr();
		raqtyInputstr = raqtystr.getInputStr();

	}
	document.frmItemRequestApproval.apprvString.value = inpApprvString;
	document.frmItemRequestApproval.boString.value = inpBoString;
	reqNameStr = reqNameStr.substring(1, reqNameStr.length);
	if(reqNameStr.indexOf(',')!= -1){
		document.frmItemRequestApproval.requestor_name.value = '';
	}else{
		document.frmItemRequestApproval.requestor_name.value = reqNameStr;
	}
	document.frmItemRequestApproval.partString.value = partInputstr;
	document.frmItemRequestApproval.raqtyString.value = raqtyInputstr;
	return inputStr;
}



function onClickSelect(id,cellInd,state){
	selectfl = false;
	var checkInd = gridObj.getColIndexById("check");
	var allRowCount = gridObj.getRowsNum();
	var parReqId='';
	var setId='';
	var LNRequestId='';
	document.getElementById("approve").disabled = false;
	var LNReqId = gridObj.getColIndexById("LNREQID");
	LNRequestId = gridObj.cellById(id, LNReqId).getValue();
	if(state && cellInd == 0 ){
				selectedReqIds += ','+LNRequestId;
				selectedRowCnt++;
				if(selectedRowCnt == allRowCount){
				document.frmItemRequestApproval.mastercheck.checked = true ;
			} 
		}else{
		    selectedReqIds=selectedReqIds.replace(','+LNRequestId,'');
			document.frmItemRequestApproval.mastercheck.checked = false ;
			selectedRowCnt--;
			
		}
	var disableApprBtnFl=false;
	gridObj.forEachRow( function(id){
		var needDisableFl = gridObj.cellById(id,  gridObj.getColIndexById("disableFl")).getValue();
		var checkFl = gridObj.cells(id,gridObj.getColIndexById("check")).getValue();
		if(needDisableFl=='Y' && checkFl=='1'){
			disableApprBtnFl=true;
	}
	});
	 if(disableApprBtnFl==true)
	   {
	      document.getElementById("approve").disabled = true;
	   }
		
}


function validateQty(status){
	var apprQtyfl = false;
	var apprQtyNumFl = false;
	var returnQtyNumFl = false;
	var negQtyfl = false;
	var negQtyReturnfl = false; 
	var reQty = '';
	var reqQtyfl = false;
	var errReqQtyfl = false;
	var qtychkfl = false;
	var boQtychkfl = false;
	var gridrows = gridObj.getAllRowIds();
	var rowsarr = gridrows.toString().split(",");
	var userType = document.frmItemRequestApproval.userType.value;
	if (rowsarr.length > 0) {
		disableChkFl = gridObj.getColIndexById("disableFl");
		for ( k = 0; k < rowsarr.length; k++) {		
			var rowcurrentid = rowsarr[k];	
			 reqQtyfl = false;
			 if(disableChkFl != undefined && disableChkFl != ""){
				 needDisableFl = gridObj.cellById(rowcurrentid, disableChkFl).getValue();	 
			 }else{
				 needDisableFl = 'N';
			 }			 
			 if(needDisableFl == 'N' ){
				var checkInd = gridObj.cells(rowcurrentid,gridObj.getColIndexById("check")).getValue();
				apprQty = parseInt(gridObj.cells(rowcurrentid, gridObj.getColIndexById("APPROVE_QTY")).getValue());
				reqQty = parseInt(gridObj.cells(rowcurrentid, gridObj.getColIndexById("QTY")).getValue());
				parts = gridObj.cells(rowcurrentid,gridObj.getColIndexById("PNUM")).getValue();
				
				
				if(userType == "PD"){
					returnqty = gridObj.cells(rowcurrentid, gridObj.getColIndexById("RETNQTY")).getValue();
					}
				if(checkInd !='0'){				
					if((returnqty > apprQty) && status == "Approve"){
						qtychkfl = true;
						temppartreturnstr += parts + ',';
					}
					if(apprQty > reqQty){
						reqQtyfl = true;
						errReqQtyfl  = true;
						temppartQtystr += parts + ',';						
					}
					if(isNaN(returnqty)){
						returnQtyNumFl = true;
					}else{
						if(returnqty < 0){
							negQtyReturnfl =true;
						}
					}
					if(isNaN(apprQty)){
						apprQtyNumFl = true;
					}else{
						if(apprQty < 0){
							negQtyfl =true;
						}
					}
					if(apprQtyfl || isNaN(apprQty) || reqQtyfl) {
						gridObj.setRowColor(rowcurrentid,"pink");
					}else{
						gridObj.setRowColor(rowcurrentid,"");
					}
				}else{
					gridObj.setRowColor(rowcurrentid,"");
				}
			 }
		}
		temppartreturnstr = temppartreturnstr.substring(0, temppartreturnstr.length - 1);
		temppartQtystr = temppartQtystr.substring(0, temppartQtystr.length - 1);
		if((qtychkfl) && status == "Approve") {
			Error_Details(Error_Details_Trans(message[5063],temppartreturnstr));
			temppartreturnstr = '';
		}
		if(apprQtyfl){
			Error_Details(message[5064]);
		}
		if(errReqQtyfl){
			Error_Details(Error_Details_Trans(message[5065],temppartQtystr));
			temppartQtystr = '';
		}
		if(apprQtyNumFl){
			Error_Details(message[5066]);
		}
		if(negQtyfl){
			Error_Details(message[5067]);
		}
		if(returnQtyNumFl){
			Error_Details(message[5068]);
		}
		if(negQtyReturnfl){
			Error_Details(message[5069]);
		}
	}
	return ErrorCount;
}
var reqIds = '';
function fnApprove(ScreenName){
	var ReqId = TRIM(document.frmItemRequestApproval.requestId.value);
	var strTnxType= document.frmItemRequestApproval.strTxnType.value ;
	var fieldSales =document.frmItemRequestApproval.fieldSales.value ; 
	var salesRep =document.frmItemRequestApproval.salesRep.value ;  
	var sheetOwner =document.frmItemRequestApproval.sheetOwner.value ; 
	var systemid =document.frmItemRequestApproval.systemid.value ; 
	var StartDt = document.frmItemRequestApproval.startDt.value;
	var EndDt = document.frmItemRequestApproval.endDt.value;
	var strOptVal = '';
	var hApprInpString = "";
	//alert("ScreenName *** "+ScreenName);
	var userType = document.frmItemRequestApproval.userType.value;
	if(ScreenName == "Request"){
	if(selectedReqIds != ''){
		// if type is 400088 getting inputstring and if approve qty is more than req qty throwing validations.
		if(strTxnType == '400088'){ //400088 - Consignment Item
			
			ErrorCount = validateQty('Approve');
			inputStr = fnCreateApprovalString();
			document.frmItemRequestApproval.hApproveInputString.value = inputStr;
		}
		document.frmItemRequestApproval.strReqIds.value = selectedReqIds.substr(1,selectedReqIds.length);
	}else{
		Error_Details(message[5070]);
	}
	}
	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}else{
		if(ScreenName == "Request"){

		if(selectfl == true){
			confirmMsg =  fnConfirmMessage();
		}else{
			confirmMsg = true;
		}
		}else{
			confirmMsg = true;
		}
		if(confirmMsg){
			
				
				if(ScreenName == "Comments"){
					raqtyInputstr = parent.document.frmItemRequestApproval.raqtyString.value;
				}
				if(raqtyInputstr == ""){
					document.frmItemRequestApproval.strOpt.value = 'save';
				}
				else{
					document.frmItemRequestApproval.strOpt.value = 'appwithra';
				}
				if(ScreenName == "Request"){
				if(strOptVal =='PDREJLNREQAPPR'){
						document.frmItemRequestApproval.haction.value = 'PDREJLNREQAPPR';
				}else{
						document.frmItemRequestApproval.haction.value = 'Approved';	
				}
				}
		if(ScreenName == "Request"){
			if(userType == "PD" && strTxnType == '400088'){
				var chkbox =document.frmItemRequestApproval.chk_ShowArrowFl.checked;
				if(chkbox == true){
					fnApprWithComments();
					return false;
				}
		}
			reqIds = document.frmItemRequestApproval.strReqIds.value;
			strOptVal = document.frmItemRequestApproval.strOpt.value;
			//PC-3849 - back order for item request
			fnStartProgress();
			var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmItemRequestApproval.do'
					+'?strOpt='
					+strOptVal
					+'&strTxnType='
					+strTnxType
					+'&requestId='
					+ReqId
					+'&systemid='
					+systemid
					+'&sheetOwner='
					+sheetOwner
					+'&fieldSales='
					+fieldSales
					+'&salesRep='
					+salesRep
					+'&startDt='
					+StartDt
					+'&endDt='
					+EndDt
					+'&hApproveInputString='
					+inputStr
					+'&apprvString='
					+encodeURIComponent(inpApprvString)
					+'&boString='
					+encodeURIComponent(inpBoString)
					+'&strReqIds='
					+reqIds
					+'&partString='
					+partInputstr
					+'&raqtyString='
					+raqtyInputstr
					+'&strResLoad=Y'
					+'&ramdomId=' + new Date().getTime());
			var loader = dhtmlxAjax.getSync(ajaxUrl);
			var response = loader.xmlDoc.responseText;
			var status = loader.xmlDoc.status;
			console.log('Approve status=>'+status)
			if(status == 200){
				fnReportResponse(response);
				document.frmItemRequestApproval.strReqIds.value="";
				reqIds = "";
				selectedReqIds = "";
			}else{
				document.frmItemRequestApproval.strReqIds.value="";
				reqIds = "";
				selectedReqIds = "";
				document.getElementById("trDiv").style.display = 'none';	
				document.getElementById("NothingFound").style.display = 'table-row';
				document.getElementById("excelExport").style.display = 'none';
				document.getElementById("gridObjData").style.display = 'none';
				document.getElementById("gridButtons").style.display = 'none';
				document.getElementById("pricenote").style.display='none';
				fnStopProgress();
				Error_Details("Reponse has been Failed.Please Try again Later");
				if (ErrorCount > 0) {
					Error_Show();
					Error_Clear();
					return false;
				}
			} 
				
		}else if(ScreenName == "Comments"){
			var to = document.frmItemRequestApproval.email_to.value;
			var cc = document.frmItemRequestApproval.email_cc.value;
			var comments = document.frmItemRequestApproval.email_comments.value;
    		
    		validateEmails( to, "," ,"To ");
			validateEmails( cc, "," ,"Cc ");

			if(to == ''){			
				Error_Details(message[5071]);
			}
			if(comments == ''){			
				Error_Details(message[5072]);
			}
			
			if (ErrorCount > 0){
				Error_Show();
				Error_Clear();
				return false;
			}else{
				fnAssignData();
				fnStartProgress('Y');
				document.frmItemRequestApproval.submit();
				}
		}
		}
	}
}

function fnReject(){
	if(selectedReqIds == ''){
		Error_Details(message[5073]);
	}
	
	if(strTxnType == '400088'){
		ErrorCount = validateQty('reject');
	}
	
	if (ErrorCount > 0){
		
		Error_Show();
		Error_Clear();
		return false;
	}else{
		if(selectfl == true){
			confirmMsg =  fnConfirmMessage();
		}else{
			confirmMsg = true;
		}
		
		if(confirmMsg){
		
		if(strTxnType == '4127'){  // 4127 - Product Loaner
		    document.frmItemRequestApproval.action="/GmCommonCancelServlet?hCancelType=VLNRQ&hTxnId="+selectedReqIds.substr(1,selectedReqIds.length);
		}
		else if (strTxnType == '4119'){   // 4119 - Inhouse Loaners
			document.frmItemRequestApproval.action="/GmCommonCancelServlet?hCancelType=VILNRQ&hTxnId="+selectedReqIds.substr(1,selectedReqIds.length);	
		}
		else if (strTxnType == '400088'){  //400088 - Consignment Item
			document.frmItemRequestApproval.action="/GmCommonCancelServlet?hCancelType=ITEMRQ&hTxnId="+selectedReqIds.substr(1,selectedReqIds.length);	
		}
		else if (strTxnType == '400087'){  //400087 - Consignment Set
			document.frmItemRequestApproval.action="/GmCommonCancelServlet?hCancelType=SETRQ&hTxnId="+selectedReqIds.substr(1,selectedReqIds.length);	
		}
		document.frmItemRequestApproval.submit();  
	}
	}
}
function fnSalesReport(setid , adid ,sysid)
{  
	 w1 = createDhtmlxWindow(1050, 500);
     w1.setText(message_sales[105]);
     var ajaxUrl = fnAjaxURLAppendCmpInfo('/GmSalesConsignmentServlet?hAction=LoadDCONS&hAreaDirector='+adid+'&SetNumber='+sysid+'&Cbo_SalesType=50302&Cbo_TypeCon=50401&Cbo_Turns=3&Loaner_app=YES&randomId='+Math.random());
     w1.attachURL(ajaxUrl);
}

function fnOnload(){
	document.getElementById("gridObjData").style.display = 'none';
	if (strTxnType == '400088'){ 
	var dist_id = fieldSales.getSelectedValue();
	if(dist_id != "0"){
		var disablesalesrepid = false; 
		fnDistFilterReps(dist_id,disablesalesrepid);
	}
	}
	var txtRefId = document.frmItemRequestApproval.requestId;
	if(txtRefId.value == ''){
		txtRefId.focus();
		txtRefId.select();
	}
	if(objGridData != ''){
		document.getElementById("gridObjData").style.display = 'revert';
		fnReportResponse(objGridData);
	}
}

function fnLoadRequset(){
	if(event.keyCode == 13)
	{	 
		
		var rtVal = fnLoad();
		if(rtVal == false){
			if(!e) var e = window.event;

	        e.cancelBubble = true;
	        e.returnValue = false;

	        if (e.stopPropagation) {
	                e.stopPropagation();
	                e.preventDefault();
	        }
		}
	}
}

//Description: This function will call onclick load button. check the validation & pass control to action.
function fnLoad() {

	objStartDt = document.frmItemRequestApproval.startDt;
	objEndDt = document.frmItemRequestApproval.endDt;
		
	/* Front-end validation added for the Entered Request ID as part of PMT-1281*/
	var ReqId = TRIM(document.frmItemRequestApproval.requestId.value);
	var objRegExp = /(^-?\d*$)/;
	if(!ReqId == '' && !objRegExp.test(ReqId)){
		Error_Details(message[5074]);
	}
	if(objStartDt.value != "" && objEndDt.value == ""){
		Error_Details(message[5075]);
	}
	if(objEndDt.value != "" && objStartDt.value == ""){
		Error_Details(message[5076]);
	}
	if(objStartDt.value != ""){ 
		CommonDateValidation(objStartDt, format, message_sales[100]+format);
	}
	if(objEndDt.value != ""){ 
		CommonDateValidation(objEndDt, format, message_sales[101]+format);
	}
	var dateDifference = dateDiff(objStartDt.value,objEndDt.value,format);
	if(dateDifference < 0){		
			Error_Details(message[5077]);			
	}
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	
	document.frmItemRequestApproval.requestId.value = ReqId;
	document.frmItemRequestApproval.strOpt.value = "load";
	document.frmItemRequestApproval.haction.value = strOptVal;
	var strTnxType= document.frmItemRequestApproval.strTxnType.value ;
	var fieldSales =document.frmItemRequestApproval.fieldSales.value ; 
	var salesRep =document.frmItemRequestApproval.salesRep.value ;  
	var sheetOwner =document.frmItemRequestApproval.sheetOwner.value ; 
	var systemid =document.frmItemRequestApproval.systemid.value ; 
	var StartDt = document.frmItemRequestApproval.startDt.value;
	var EndDt = document.frmItemRequestApproval.endDt.value;
	//PC-3849 - back order for item request
	fnStartProgress();
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmItemRequestApproval.do?strOpt=load&strTxnType='
			+strTnxType
			+'&requestId='
			+ReqId
			+'&systemid='
			+systemid
			+'&sheetOwner='
			+sheetOwner
			+'&fieldSales='
			+fieldSales
			+'&salesRep='
			+salesRep
			+'&startDt='
			+StartDt
			+'&endDt='
			+EndDt
			+'&strResLoad=Y'
			+'&ramdomId=' + new Date().getTime());
	var loader = dhtmlxAjax.getSync(ajaxUrl);
	var response = loader.xmlDoc.responseText;
	var status = loader.xmlDoc.status;
	console.log('Load Status'+status);
	if(status == 200){
		fnReportResponse(response);
	}else {
		document.getElementById("trDiv").style.display = 'none';	
		document.getElementById("NothingFound").style.display = 'table-row';
		document.getElementById("excelExport").style.display = 'none';
		document.getElementById("gridObjData").style.display = 'none';
		document.getElementById("gridButtons").style.display = 'none';
		document.getElementById("pricenote").style.display='none';
		fnStopProgress();
		Error_Details("Reponse has been Failed.Please Try again Later");
		if (ErrorCount > 0) {
			Error_Show();
			Error_Clear();
			return false;
		}
	}
}

//PC-3849 - back order for item request
//Populate report data 
function fnReportResponse(response){
fnStopProgress();
if (response != ""){
var setInitWidths = "30,60,60,90,70,120,100,40,40,40,40,80,30,100,100,80,80,20,40,60,80,80,80,20,*,20,10,10,10,10,10";
var setColAlign = "right,right,right,right,right,left,left,right,right,right,right,left,right,left,right,left,left,,right,left,left,left,left,left,left,left,right,left,left,right,right";
var setColTypes = "ch,ro,ro,ro,ro,ro,ro,ro,ed,ro,ed,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro";
var setColSorting = "na,int,int,str,int,str,str,int,int,int,int,str,int,str,int,str,date,,int,str,date,str,str,,str,str,int,int,str,int,int";
var setHeader = ["<input type='checkbox' value='no' id='mastercheck' onClick='javascript:onClickSelectAll(this);'/>","Request ID", "Parent Request ID","Case#/Event#", "Part", "System Name", "Discription", "Req Qty","Appr Qty","Apprv & BO", "Retn Qty","Field Sales","Field SalesId","Sheet Owner","Requestor Id","Requestor Name","Requested Date","Price Disable Flag","System Id","Sheet Owner Name","Ship Date","Shipping Carrier","Shipping Mode","","Comment","","CONSPRICE","Status","shipTo","shiptID","AddID"];
var setColIds = "check,LNREQID,REQUESTID,CASEID,PNUM,SYSTEMNAME,PDESC,QTY,APPROVE_QTY,BO_QTY,RETNQTY,FIELDSALES,FIELDSALESID,SHEET_OWNER,REPID,REPNAME,REQDATE,disableFl,SYSTEM_ID,SHEET_OWNER,SHIPDT,DELIVERYCARRIER,DELIVERYMODE,OPENADD,LASTCOMMENTS,OPENORD,CONSPRICE,STATUSID,SHIP_TO,SHIP_ID,ADDID";
var enableTooltips = "false,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true";
var gridHeight ="600";
// to set auto height
if($(document).height() !=undefined || $(document).height() !=null){
	gridHeight = $(document).height() - 200 ;
}

document.getElementById("trDiv").style.display = "table-row";
document.getElementById("NothingFound").style.display = 'none';
 document.getElementById("excelExport").style.display = "table-row";
document.getElementById("gridObjData").style.display = 'revert';
document.getElementById("gridButtons").style.display = 'revert';

// split the functions to avoid multiple parameters passing in single function
gridObj = initGridObject('dataGridDiv');
gridObj = fnCustomTypes(gridObj,setHeader, setColIds, setInitWidths, setColAlign, setColTypes,setColSorting, enableTooltips,gridHeight);
if(strTxnType != '400088'){
	gridObj.enableDistributedParsing(true);
}
if(strTxnType == '4127'){
	gridObj.enableTooltips("false,false,true,true,true,true,true,true,true,true,true,true,true");
	}
gridObj = loadDhtmlxGridByJsonData(gridObj,response);
gridObj.attachEvent("onCheckbox",onClickSelect);
gridObj.attachEvent("onEditCell",doOnCellEdit);
gridObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");


console.log('gridObj=>'+gridObj);
var consprice = gridObj.getColIndexById("CONSPRICE");
var statusID = gridObj.getColIndexById("STATUSID");
var disableFl = gridObj.getColIndexById("disableFl");
var shipTo = gridObj.getColIndexById("SHIP_TO");
var shipId = gridObj.getColIndexById("SHIP_ID");
var addId = gridObj.getColIndexById("ADDID");
var openOrd = gridObj.getColIndexById("OPENORD");
var openAdd = gridObj.getColIndexById("OPENADD");
var caseId = gridObj.getColIndexById("CASEID");
var requestId = gridObj.getColIndexById("REQUESTID");
var lastComment = gridObj.getColIndexById("LASTCOMMENTS");
var apprvQty = gridObj.getColIndexById("APPROVE_QTY");
var reqQty = gridObj.getColIndexById("QTY");
var boQty = gridObj.getColIndexById("BO_QTY");
var LNReqId = gridObj.getColIndexById("LNREQID");

var strConsprice="";
var strStatusId="";
var strdisableFl="";
var strcaseId="";
var strReqId="";
var reqId="0";
var apprvedQty="";
var returnQty="";
var boApprvQty="";
gridObj.forEachRow(function(rowId) {
  	strConsprice = gridObj.cellById(rowId, consprice).getValue();
  	strStatusId = gridObj.cellById(rowId, statusID).getValue();
  	strdisableFl = gridObj.cellById(rowId, disableFl).getValue();
  	strShipTo = gridObj.cellById(rowId, shipTo).getValue();
  	strShipId = gridObj.cellById(rowId, shipId).getValue();
  	strAddId = gridObj.cellById(rowId, addId).getValue();
  	strcaseId = gridObj.cellById(rowId, caseId).getValue();
  	strReqId = gridObj.cellById(rowId, requestId).getValue();
  	apprvedQty = gridObj.cellById(rowId, apprvQty).getValue();
  	requiredQty = gridObj.cellById(rowId, reqQty).getValue();
  	LNRequestId = gridObj.cellById(rowId, LNReqId).getValue();

  	if(strStatusId == '5'){
  		if(strConsprice > 0){
  			gridObj.cells(rowId,disableFl).setValue("N");
  		}
  		else
  			gridObj.cells(rowId,disableFl).setValue("Y");
  	}else
  		gridObj.cells(rowId,disableFl).setValue("N");
  	
	gridObj.cellById(rowId, openAdd).setValue(
			'<a href="javascript:void(0);" onClick="fnOpenAdd('+strShipTo+','+strShipId+','+strAddId+');"><img alt="set" src="/images/Address-Book-icon.png" height="15" width="15" border="0"></a>');

	if(appvlAccessFl != 'Y'){
		gridObj.setColumnHidden(0,true);	
	}
	if(reqId != strReqId){
		
		gridObj.cellById(rowId, openOrd).setValue("<img alt='Order Log' onClick='fnOpenOrdLog(\""+strcaseId+"\",4000760)' src='/images/phone-icon_ans.gif' height='15' width='15' border='0'/>");
	}else
		gridObj.cells(rowId,lastComment).setValue(" ");
		
		reqId=strReqId;
});

gridObj.setColumnHidden(1,true);
gridObj.setColumnHidden(12,true);
gridObj.setColumnHidden(13,true);
gridObj.setColumnHidden(14,true);
gridObj.setColumnHidden(17,true);
gridObj.setColumnHidden(18,true);
gridObj.setColumnHidden(19,true);
gridObj.setColumnHidden(26,true);
gridObj.setColumnHidden(27,true);
gridObj.setColumnHidden(28,true);
gridObj.setColumnHidden(29,true);
gridObj.setColumnHidden(30,true);



if(strTxnType == '400088' || strTxnType == '400087'){  //400088 - Consignment Item 400087 - Consignment Set
	//	gridObj.attachHeader("<input type='checkbox' value='no' id='mastercheck' onClick='javascript:onClickSelectAll(this);'/>");
		//When price is 0 or less, need to block user to approve respective record this.
		disableChkFl = gridObj.getColIndexById("disableFl");
		if(appvlAccessFl == 'Y'){
			gridObj.forEachRow(function(rowId) {	
				needDisableFl = gridObj.cellById(rowId, disableChkFl).getValue();	
				//Setting background color to the editable column.
				gridObj.cellById(rowId,8).setBgColor('#ccdd99');
				gridObj.cellById(rowId,9).setBgColor('#ecffaf');
				gridObj.cellById(rowId,10).setBgColor('#e6faa7');
				if(needDisableFl =='Y'){
					gridObj.setCellTextStyle(rowId,gridObj.getColIndexById("disableFl"),"color:red;border:1px solid red;");
					gridObj.setRowColor(rowId,"#F8E0E0");//this is for changing the row color of needdisableFl='Y' rows
					gridObj.cellById(rowId,8).setBgColor('#F8E0E0');
					gridObj.cellById(rowId,10).setBgColor('#F8E0E0');
					gridObj.cellById(rowId, 0).setDisabled(false);
					priceNoteDisplayFL = true;
				}
			});
		}
		if(priceNoteDisplayFL){
			document.getElementById("pricenote").style.display="block";
		}
	}


}else{
document.getElementById("trDiv").style.display = 'none';	
document.getElementById("NothingFound").style.display = 'table-row';
document.getElementById("excelExport").style.display = 'none';
document.getElementById("gridObjData").style.display = 'none';
document.getElementById("gridButtons").style.display = 'none';
document.getElementById("pricenote").style.display="none";
}


}
//This Function Used to set custom types conditions in Grid
function fnCustomTypes(gObj,setHeader, setColIds, setInitWidths, setColAlign, setColTypes,setColSorting, enableTooltips,gridHeight){

if (setHeader != undefined && setHeader.length > 0) {
gObj.setHeader(setHeader);
var colCount = setHeader.length;	
}    

if (setInitWidths != "")
gObj.setInitWidths(setInitWidths);

if (setColAlign != "")
gObj.setColAlign(setColAlign);

if (setColTypes != "")
gObj.setColTypes(setColTypes);

if (setColSorting != "")
gObj.setColSorting(setColSorting);

if (enableTooltips != "")
gObj.enableTooltips(enableTooltips);

if(setColIds != "")
gObj.setColumnIds(setColIds);

if (gridHeight != "")
gObj.enableAutoHeight(true, gridHeight, true);

return gObj;
}

function doOnCellEdit(stage, rowId, cellInd, nValue, oValue) {
	var requiredQtyValue='';
	var approvedQtyValue='';
	var boApprvQty='';
	if(strTxnType == '400088'){
	disableChkFl = gridObj.getColIndexById("disableFl");
    needDisableFl = gridObj.cellById(rowId, disableChkFl).getValue();	
	if(stage==0&&cellInd=='8'&&needDisableFl=='Y'){
	return false;
	}
    if(stage==0&&cellInd=='9'&& needDisableFl=='Y'){
	return false;
	}
    
	var reqQty = gridObj.getColIndexById("QTY");
	var apprQty = gridObj.getColIndexById("APPROVE_QTY");
	var boQty = gridObj.getColIndexById("BO_QTY");
	requiredQtyValue = gridObj.cellById(rowId, reqQty).getValue();
	approvedQtyValue = gridObj.cellById(rowId, apprQty).getValue();
	boApprvQty = requiredQtyValue - approvedQtyValue;
	
	if(boApprvQty > 0){
		gridObj.cells(rowId,boQty).setValue(boApprvQty);
	}
	else
		{
		gridObj.cells(rowId,boQty).setValue("");
		}
	
	}
	if (stage == 1 && cellInd == '7' ){
		var c=this.editor.obj;			
		if(c!=null) c.select();
	}
	return true;
}

function onClickSelectAll(val){
	var partStr="";
	var checkInd = gridObj.getColIndexById("check");
	disableChkFl = gridObj.getColIndexById("disableFl");
	if(val.checked == true && checkInd == 0 ){
		gridObj.forEachRow( function(id){
			 if(disableChkFl != undefined && disableChkFl != ""){
				 needDisableFl = gridObj.cellById(id, disableChkFl).getValue();	 
			 }else{
				 needDisableFl = 'N';
			 }
			if(needDisableFl == 'Y' ){
				partNum= gridObj.cellById(id,gridObj.getColIndexById("PNUM")).getValue();
				eventName= gridObj.cellById(id,gridObj.getColIndexById("CASEID")).getValue();
			    partStr += partNum+'-'+eventName+',     ';
			    document.getElementById("approve").disabled = true;
			}

			gridObj.cellById(id,checkInd).setChecked(true);			 
				 selectedReqIds += ','+id;
				 selectedRowCnt++;
		 });
		 selectfl = true;
	}else{ 
		gridObj.forEachRow( function(id){
			 if(disableChkFl != undefined && disableChkFl != ""){
				 needDisableFl = gridObj.cellById(id, disableChkFl).getValue();	 
			 }else{
				 needDisableFl = 'N';
			 }
				 document.getElementById("approve").disabled = false;
				 gridObj.cellById(id,checkInd).setChecked(false);
				 selectedReqIds = '';
				 selectedRowCnt ='';
				}
		 );
		 
		 
	}
	partStr = partStr.substring(0, partStr.length - 1);
	if(partStr!=''){
		Error_Details(Error_Details_Trans(message_sales[102],partStr));
		partStr='';
	}
		if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
}

function fnConfirmMessage(){
	confirmMsg = confirm(message_sales[104]);
	if(confirmMsg)	return true;
	else 			return false;
} 

function fnExportToExcel(){
	gridObj.detachHeader(1);
	gridObj.setColumnHidden(0,true);	
	gridObj.toExcel('/phpapp/excel/generate.php');	
	if(strTxnType == '4127'){
		gridObj.attachHeader("<input type='checkbox' value='no' id='mastercheck' onClick='javascript:onClickSelectAll(this);'/>,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#select_filter,#rspan,#select_filter,#text_filter,#text_filter");
	}else if (strTxnType == '4119'){
		gridObj.attachHeader("<input type='checkbox' value='no' id='mastercheck' onClick='javascript:onClickSelectAll(this);'/>,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#select_filter,#select_filter,#select_filter,#text_filter");
	} 
	/*if(strTxnType == '400088' || strTxnType == '400087'){  //400088 - Consignment Item  400087 - Consignment Set
		gridObj.attachHeader(",#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter");
	}*/
	gridObj.setColumnHidden(0,false);	
}
//The Function is used to filter the Sales Rep Dropdown based on Field Sales
function fnDistFilterReps(distid,flag)
{ 
	var distname = fieldSales.getSelectedText();
	var distid = fieldSales.getSelectedValue();
	var salesrepname = salesRep.getSelectedText();
	var salesrepid = salesRep.getSelectedValue();
	salesRep.clearAll();
	salesRep[0]= salesRep.addOption("0","[Choose One]","");
	var k = 0;
	if(flag != false){
	salesRep.setComboValue("0");
	}
	if(distid != ''){
		for (var i=0;i<repLen;i++)
		{
			arr = eval("RepArr"+i);
			arrobj = arr.split("^");
			id = arrobj[0];
			rid = arrobj[2];
			name = arrobj[3];
		
			if((id == distid) )
			{
				salesRep[k+1] = salesRep.addOption(rid,name,"");
				k++;
			}
		
		}
	}

} 
//The Function is used to Called When we Click on Inventory Lookup Button.
function fnInventoryLookup()
{
	var pnums = '';
	var strPart = fnPartString();
	if(strPart==''){
		return false;
	}
	if(strPart != undefined && strPart !=''){
		 pnums = fnRemoveDuplicateValues(strPart);	
	}
	dhxWins = new dhtmlXWindows();
    dhxWins.enableAutoViewport(true);
    w1 = dhxWins.createWindow("w1", 30, 250, 1280, 470);
    w1.setText(message_sales[106]);
    var ajaxUrl = fnAjaxURLAppendCmpInfo('/GmPartInvReportServlet?hAction=Go&hOpt=MultipleParts&hScreenType=BACKORDER&hPartNum='+encodeURIComponent(pnums));
    w1.attachURL(ajaxUrl,"Pack","resizable=yes,scrollbars=yes,top=250,left=250,width=960,height=510");	
	
}

//The Function is to remove duplicate values
function fnRemoveDuplicateValues(inValue) {
	var items = inValue.split(",");
	var arrlen = items.length - 1;
	var i = 0;
	var outValue = "";
	var prevItem;
	while (i <= arrlen) {
		if (outValue.length > 0) {
			if (prevItem != items[i].trim()) {
				var str = items[i].trim();
				outValue = outValue.replace(prevItem + ",", "");
				outValue += "," + str;
			}
		} else {
			outValue = items[i].trim();
		}
		prevItem = items[i].trim();
		i++;
	}
	return outValue;
}
//The Function is used to Called When we Click on Trend Report Button.
function fnTrendLookup()
{
	var strPart = fnPartString();
	 if(strPart==''){
			return false;
		}
	dhxWins = new dhtmlXWindows();
    dhxWins.enableAutoViewport(true);
    w1 = dhxWins.createWindow("w1", 30, 250,1180, 470);
    w1.setText(message_sales[107]);
    var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmLogisticsReport.do?method=loadTrendReport&salesGrpType=50271&strOpt=reload&trendperiod= 3&trendtype = 50277&checkSelectedTrend=40020&checkSelectedTrend=40021&checkSelectedTrend=40022&trendDuration=50282&partNum='+encodeURIComponent(strPart));
    w1.attachURL(ajaxUrl,"Pack","resizable=yes,scrollbars=yes,top=250,left=250,width=960,height=510");
}

//The Function is used to Called When we Save Comments in Pending Item Request Screen.
function fnOpenOrdLog(strlnreqid,type){
	windowOpener("/GmCommonLogServlet?hType="+type+"&hID="+strlnreqid,"PrntInv", "resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
}
//This fuction is used to see ship address
function fnOpenAdd(p_ShipTo,p_ShipToId,p_addid) 
{

if (p_ShipTo == '0')
{
		alert(message_operations[720]);
		return;
}
if (p_ShipTo == '4121' && p_ShipToId =='0')
{
		alert(message_operations[718]);
		return;
}
if (p_ShipTo == '4121')
{
	p_ShipToId = p_addid;
}
windowOpener("/GmOrderItemServlet?hAction=SAddress&hShip="+p_ShipTo+"&hShipId="+p_ShipToId,"Address","resizable=yes,scrollbars=yes,top=340,left=420,width=300,height=135");
//windowOpener("/gmLoanerRequestApproval.do?strOpt=Address&hShip="+p_ShipTo+"&hShipId="+p_ShipToId,"Address","resizable=yes,scrollbars=yes,top=340,left=420,width=300,height=135");
}

//PMT-6432 this function is for Consignment part by id button
function fnConsignmentByPart(){ 
	Vpartstr = fnPartString();
	if(Vpartstr==''){
		return false;
	}
	dhxWins = new dhtmlXWindows();
    dhxWins.enableAutoViewport(true);
    w1 = dhxWins.createWindow("w1", 30, 250, 880, 470);
    w1.setText(message_sales[108]);
    var ajaxUrl = fnAjaxURLAppendCmpInfo('/GmSalesConsignDetailsServlet?hAction=LoadPart&Cbo_Type=0&hPartNumber='+encodeURIComponent(Vpartstr));
    w1.attachURL(ajaxUrl,"Consignment","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
}

//PMT-6432 for part number validsation
function fnPartString(){
	var chkflag = '';
	var strPart = '';
	gridObj.forEachRow(function(rowId){
		chkflag = TRIM(gridObj.cellById(rowId,gridObj.getColIndexById("check")).getValue());
		partNo =  gridObj.cells(rowId, gridObj.getColIndexById("PNUM")).getValue();
		if(chkflag == 1){
			strPart +=partNo +',';
		}
	});
	if(strPart == ''){
		Error_Details(message[5078]);
	}
	if (ErrorCount > 0){
		Error_Show();
	    Error_Clear();
		return false;
	}
return strPart;
}

function fnApprWithComments(){

	var reqname = document.frmItemRequestApproval.requestor_name.value;
	var checkbox_display = 'true';
	
	dhxWins = new dhtmlXWindows();
    dhxWins.enableAutoViewport(false);
    dhxWins.attachViewportTo(document.body);
    dhxWins.setImagePath("extweb/dhtmlx/dhtmlxWindows/imgs");
    
    w1 = dhxWins.createWindow("w1", 300, 150, 760, 500);
    var ajaxUrl = fnAjaxURLAppendCmpInfo('gmItemRequestApproval.do?&ramdomId='+Math.random()+'&strOpt=Approvalview&requestor_name='+reqname+'&chkbox_display='+checkbox_display);
    w1.attachURL(ajaxUrl,"ApprovalwithComments","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
   w1.setText(message_sales[109]);
   w1.setModal(true);
}
function fnCancel(){
	CloseDhtmlxWindow();
	return false;
}

function fnClose(){
	this.parent.fnLoad();
}

function fnAssignData(){
	document.frmItemRequestApproval.strReqIds.value = parent.document.frmItemRequestApproval.strReqIds.value;
	document.frmItemRequestApproval.strTxnType.value = parent.document.frmItemRequestApproval.strTxnType.value;
	document.frmItemRequestApproval.haction.value = parent.document.frmItemRequestApproval.haction.value;
	document.frmItemRequestApproval.hApproveInputString.value = parent.document.frmItemRequestApproval.hApproveInputString.value;
	document.frmItemRequestApproval.userType.value = parent.document.frmItemRequestApproval.userType.value;
	document.frmItemRequestApproval.raqtyString.value = parent.document.frmItemRequestApproval.raqtyString.value;
	document.frmItemRequestApproval.partString.value = parent.document.frmItemRequestApproval.partString.value;
	document.frmItemRequestApproval.apprvString.value = parent.document.frmItemRequestApproval.apprvString.value;
	document.frmItemRequestApproval.boString.value = parent.document.frmItemRequestApproval.boString.value;
}

/*Below function used to validaet the Email Ids entered in the UI.
Input Object - Input Object which has to be validaetd
delimiter - This is the Email ID delimiter in the Field. 
Errmessage field- This is the Field control to be dispalyed Error msg.
EMail ID Format: anystring@anystring.anystring */
function validateEmails(obj, delimiter, errField) {
	var objRegExp = /\S+@\S+\.\S+/;
	var toemail = obj.split(delimiter);
	var errMailIDs='';
	var errFl = false;
	for(var i=0;i<toemail.length;i++){
		toemail[i] = (toemail[i].replace(/^\s+/,'')).replace(/\s+$/,'');
		if(toemail[i] != '' && toemail[i].search(objRegExp) == -1){			
			errFl = true;
			errMailIDs =  errMailIDs + toemail[i] +' ,';
		}
	}
	if(errFl){
		var array=[errField,errMailIDs.substr(0,errMailIDs.length-1)]
		Error_Details(Error_Details_Trans(message_sales[103],array));
	}
}
// when we press enter key after giving Rule Group Id,it will load
function fnEnter(key){
	if(key.keyCode ==  13){
		fnLoad();
	}
}
