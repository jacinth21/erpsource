function fnOnPageLoad() {
	//objStartDt = document.frmCaseCalendar.fromDt.value;
	var objMode = document.frmCaseCalendar.haction;
	if(objMode.value == ''){
		clendarMode = 'month';
	}else{
		clendarMode = objMode.value;
	}
	scheduler.config.multi_day = true;
	scheduler.config.details_on_create = false;
	scheduler.config.details_on_dblclick = false;
	scheduler.config.full_day = true;
	scheduler.config.xml_date="%m/%d/%Y %H:%i";
	scheduler.init('caseCalender',new Date(fromDate),clendarMode);
	scheduler.config.cascade_event_display = true; 
    scheduler.config.cascade_event_count = 4;
    scheduler.config.cascade_event_margin = 10;
	scheduler.parse(gridData);
	scheduler.config.show_loading =true;
	scheduler.config.readonly_form = true;
	scheduler.templates.tooltip_text = function(start,end,event) {
		return message_sales[284]+event.personalnote+event.acctnm+message_sales[285]+event.salesrep+message_sales[286]+event.distributor+"<br/><b>"+event.hyperlink+"</b>";
	};
}


function show_minical(){
	if (scheduler.isCalendarVisible())
		scheduler.destroyCalendar();
	else
		scheduler.renderCalendar({
			position:"dhx_minical_icon",
			date:scheduler._date,
			navigation:true,
			handler:function(date,calendar){
				scheduler.setCurrentView(date);
				dtSchedule = convert(date);
				document.frmCaseCalendar.fromDt.value = dtSchedule;
				document.frmCaseCalendar.toDt.value = dtSchedule;
				scheduler.destroyCalendar()
			}
		});
}

function fnCalendarCaseInfo(infoid) {
	  
/*		w1 = createDhtmlxWindow(960, 570);
	    w1.setText("case book summary"); 
		//w1.attachURL("/gmCaseCalendarAction.do?method=calendarCaseInfo&caseInfoId="+infoid);gmCasePost.do?caseinfoID
	    w1.attachURL("/gmCasePost.do?caseInfoID="+infoid);
	    w1.attachEvent('onClose', Refresh);
	    return false;*/
	document.frmCaseCalendar.action = "/gmCasePost.do?caseInfoID="+infoid;
	document.frmCaseCalendar.submit();
}
function Refresh(){
	   document.frmCaseCalendar.haction.value = scheduler._mode;
	   if(scheduler._mode == 'week' || scheduler._mode == 'day'){
		   document.frmCaseCalendar.strOpt.value= "";
	   }
	   document.frmCaseCalendar.submit();
	   }

scheduler._click={
		dhx_cal_data:function(e){
			var trg = e?e.target:event.srcElement;
			var id = scheduler._locate_event(trg);
			
			e = e || event;
			if ((id && !scheduler.callEvent("onClick",[id,e])) ||scheduler.config.readonly) return;
			
			if (id) {		
				scheduler.select(id);
				var mask = trg.className;
				if (mask.indexOf("_icon")!=-1)
					scheduler._click.buttons[mask.split(" ")[1].replace("icon_","")](id);
			} else
				scheduler._close_not_saved();
		},
		dhx_cal_prev_button:function(){
			scheduler._click.dhx_cal_next_button(0,-1);
		},
		dhx_cal_next_button:function(dummy,step){
			
			if(scheduler._mode == 'month'){
			var dtSchedule = scheduler._date;
			 var dtMonth = dtSchedule.getMonth();
			 var dtDay   = '01';
			 var dtYear  = dtSchedule.getFullYear();
			 if(step == undefined){
				 step = 1; 
			 }
			 
			 dtMonth =dtMonth+step;
			 if(dtMonth > 12){
				 dtMonth = dtMonth-12;
				 dtYear = dtYear+1;
			 }
			
			dtSchedule = convert(dtSchedule);
			document.frmCaseCalendar.strOpt.value="load";
			document.frmCaseCalendar.haction.value = scheduler._mode;
			document.frmCaseCalendar.fromDt.value=dtSchedule;
			document.frmCaseCalendar.toDt.value=dtSchedule;
			document.frmCaseCalendar.ccMonth.value=dtMonth;
			document.frmCaseCalendar.ccYear.value=dtYear;
			document.frmCaseCalendar.submit();
			return false;
			}
			if(scheduler._mode == 'week'){
				var dd=scheduler.date[scheduler._mode+"_start"](new Date(scheduler._date.valueOf()));
				var ed=scheduler.date.add(dd,1,scheduler._mode);
				if(step == undefined){
				step = 1;
				strDate = convert(ed);
				strEndDate = convert(ed.setDate(ed.getDate()+7));
				 }else{
				 strEndDate = convert(dd);
				 strDate = convert(dd.setDate(dd.getDate()-7)); 
				 }
				document.frmCaseCalendar.haction.value = scheduler._mode;
				document.frmCaseCalendar.fromDt.value=strDate;
				document.frmCaseCalendar.toDt.value=strEndDate;
				//document.frmCaseCalendar.submit();
				
				}
			if(scheduler._mode == 'day'){
				var dd=scheduler.date[scheduler._mode+"_start"](new Date(scheduler._date.valueOf()));
				var ed=scheduler.date.add(dd,1,scheduler._mode);
				if(step == undefined){
				step = 1;
				strDate = convert(dd.setDate(dd.getDate()+1));
				strEndDate = strDate;
				}else{
				strDate = convert(dd.setDate(dd.getDate()-1));
				strEndDate = strDate;	
				}
				document.frmCaseCalendar.haction.value = scheduler._mode;
				document.frmCaseCalendar.fromDt.value=strDate;
				document.frmCaseCalendar.toDt.value=strEndDate;
				//document.frmCaseCalendar.submit();
				
			}
			scheduler.setCurrentView(scheduler.date.add( //next line changes scheduler._date , but seems it has not side-effects
				scheduler.date[scheduler._mode+"_start"](scheduler._date),(step||1),scheduler._mode));
		},
		dhx_cal_today_button:function(){
			scheduler.setCurrentView(new Date());
		},
		dhx_cal_tab:function(){
			var mode = this.getAttribute("name").split("_")[0];
			scheduler.setCurrentView(scheduler._date,mode);
		},
		buttons:{
			"delete":function(id){ var c=scheduler.locale.labels.confirm_deleting; if (!c||confirm(c)) scheduler.deleteEvent(id); },
			edit:function(id){ scheduler.edit(id); },
			save:function(id){ scheduler.editStop(true); },
			details:function(id){ scheduler.showLightbox(id); },
			cancel:function(id){ scheduler.editStop(false); }
		}
	}

function convert(str) {
    var date = new Date(str),
        mnth = ("0" + (date.getMonth()+1)).slice(-2),
        day  = ("0" + date.getDate()).slice(-2);
    return [ mnth, day ,date.getFullYear()].join("/");
}