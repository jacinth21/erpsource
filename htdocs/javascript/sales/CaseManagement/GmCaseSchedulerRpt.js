// Report
function fnOnPageLoad(){
	var status = document.frmCaseScheduleRpt.hStatusGrp.value;
	if (status != '')
	{
		objgrp = document.frmCaseScheduleRpt.Chk_GrpStatus;
		fnCheckSelections(status,objgrp);
	}
	if(objGridData != ''){		
		gridObj = initGridData('caseListRpt', objGridData);
		gridObj.attachHeader(',#text_filter,#text_filter,#select_filter,#text_filter,#text_filter,#text_filter,#text_filter');
		gridObj.enableTooltips(",false,false,false,false,false,false");
		gridObj.attachEvent("onMouseOver", function(id,ind){
				    if (ind == 0){
				    	var caseId = gridObj.getColIndexById("caseId_info");
				    		var paramCaseId=gridObj.cellById(id, caseId).getValue();
							 w1 = createDhtmlxWindow(785,374,true);
							 w1.setPosition(150,250);
							 w1.setText("Inventory Info - "+ paramCaseId);
							 w1.button("close").show();
							 w1.button("park").hide();
							 w1.button("minmax1").hide();
							 w1.denyResize(); 
							 var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmCaseScheduleReport.do?method=fetchCaseInventoryInfo&paramCaseId='+paramCaseId+'&ramdomId=' + Math.random());
							 w1.attachURL(ajaxUrl);
				    }
				  else
					        w1.hide();
				});
	}
}

//Marks the checkboxes as 'checked'
function fnCheckSelections(val,objgrp)
{
	var valobj = val.split(",");
	var sval = '';
	var arrlen = objgrp.length;
	
	if (arrlen > 0)
	{
		for (var j=0;j< valobj.length;j++ )
		{
			sval = valobj[j];
			for (var i=0;i< arrlen;i++ )
			{
				if (objgrp[i].value == sval)
				{
					objgrp[i].checked = true;
					break;
				}
			}
		}
	}
	else
	{
		objgrp.checked = true;
	}
}
function initGridData(divRef, gridData) {
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/extweb/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);
	gObj.enableTooltips("true,true,true,true,true");
	gObj.init();
	gObj.loadXMLString(gridData);
    return gObj;
}

function fnExcel(){ 
	gridObj.setColumnHidden(0,true);
	gridObj.toExcel('/phpapp/excel/generate.php');
	gridObj.setColumnHidden(0,false);
}

function fnLoadCaseRpt(){
	var objFromDt = document.frmCaseScheduleRpt.fromDt;
	var objToDt = document.frmCaseScheduleRpt.toDt;
	var objStat = document.frmCaseScheduleRpt.Chk_GrpStatus;
	var statlen = objStat.length;
	var statstr = '';
	var statgrpstr = '';	
	var temp = '';

	for (var i=0;i<statlen;i++)
	{
		if (objStat[i].checked == true)
		{
			
			temp = objStat[i].value;			
			statgrpstr = statgrpstr + objStat[i].value + ',';			
			statstr = statstr + temp + ',';	
		}
	}
	
	if(objFromDt.value == '' || objToDt.value == ''){
		 Error_Details(message_sales[425]);	
		}
	
	var dateRange = dateDiff(objFromDt.value,objToDt.value,gCmpDateFmt);
	if(!isNaN(dateRange) && dateRange < 0){
		 Error_Details(message_operations[117]);
	}
	
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}	
	document.frmCaseScheduleRpt.action = '/gmCaseScheduleReport.do?method=caseSchedulerListRpt&strOpt=fetch&status='+statstr.substr(0,statstr.length-1); 	
	document.frmCaseScheduleRpt.submit();
} 

function fnViewCaseDetails(caseId,status){
		windowOpener("\gmCaseSchedular.do?caseId="+caseId+"&status="+status,"Search","resizable=no,scrollbars=yes,top=150,left=150,width=835,height=600");
}

function check(){	
	if(document.frmCaseScheduleRpt.longTerm.title == "off"){
		document.frmCaseScheduleRpt.longTerm.value="on";
		document.frmCaseScheduleRpt.longTerm.title = "on";	
	}else{		
		document.frmCaseScheduleRpt.longTerm.value="off";
		document.frmCaseScheduleRpt.longTerm.title = "off";	
	}
}