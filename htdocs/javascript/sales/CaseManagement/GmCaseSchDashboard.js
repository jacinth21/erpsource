
function fnKitLoad(){
	document.frmGmCaseSchedulerDash.action = "/gmCaseSchedulerDash.do?method=fetchKitData";
	document.frmGmCaseSchedulerDash.submit();
}

function fnCaseLoad(){
	document.frmGmCaseSchedulerDash.action = "/gmCaseSchedulerDash.do?method=fetchCaseData";
	document.frmGmCaseSchedulerDash.submit();
}

function fnKitShipOver(startdt,enddate){
	windowOpener("/gmCaseScheduleReport.do?method=KitShipDshRpt&strAction=KITOVERDSH&kitOverstDt="+startdt+"&kitOverendDt="+enddate,"Search","resizable=no,scrollbars=yes,top=150,left=150,width=1050,height=750");
}
function fnKitShiptoday(startdt,enddate){
	windowOpener("/gmCaseScheduleReport.do?method=KitShipDshRpt&strAction=KITTODAYDSH&kitTodaystDt="+startdt+"&kitTodayendDt="+enddate,"Search","resizable=no,scrollbars=yes,top=150,left=150,width=1050,height=750");	
}
function fnKitShiptmw(startdt,enddate){
	windowOpener("/gmCaseScheduleReport.do?method=KitShipDshRpt&strAction=KITTMWDSH&kitTmwstDt="+startdt+"&kitTmwendDt="+enddate,"Search","resizable=no,scrollbars=yes,top=150,left=150,width=1050,height=750");
}

function fnKitRetover(startdt,enddate){
	windowOpener("/gmCaseScheduleReport.do?method=KitRetDshRpt&strAction=KITOVERDSH&kitOverstDt="+startdt+"&kitOverendDt="+enddate,"Search","resizable=no,scrollbars=yes,top=150,left=150,width=1050,height=750");
}
function fnKitRettoday(startdt,enddate){
	windowOpener("/gmCaseScheduleReport.do?method=KitRetDshRpt&strAction=KITTODAYDSH&kitTodaystDt="+startdt+"&kitTodayendDt="+enddate,"Search","resizable=no,scrollbars=yes,top=150,left=150,width=1050,height=750");	
}
function fnKitRettmw(startdt,enddate){
	windowOpener("/gmCaseScheduleReport.do?method=KitRetDshRpt&strAction=KITTMWDSH&kitTmwstDt="+startdt+"&kitTmwendDt="+enddate,"Search","resizable=no,scrollbars=yes,top=150,left=150,width=1050,height=750");
}

function fnCaseShipOver(startdt,enddate){
	windowOpener("/gmCaseScheduleReport.do?method=CaseShipDshRpt&strAction=CASEOVERDSH&caseOverstDt="+startdt+"&caseOverendDt="+enddate,"Search","resizable=no,scrollbars=yes,top=150,left=150,width=1050,height=750");
}
function fnCaseShiptoday(startdt,enddate){
	windowOpener("/gmCaseScheduleReport.do?method=CaseShipDshRpt&strAction=CASETODAYDSH&caseTodaystDt="+startdt+"&caseTodayendDt="+enddate,"Search","resizable=no,scrollbars=yes,top=150,left=150,width=1050,height=750");	
}
function fnCaseShiptmw(startdt,enddate){
	windowOpener("/gmCaseScheduleReport.do?method=CaseShipDshRpt&strAction=CASETMWDSH&caseTmwstDt="+startdt+"&caseTmwendDt="+enddate,"Search","resizable=no,scrollbars=yes,top=150,left=150,width=1050,height=750");
}

function fnCaseRetover(startdt,enddate){
	windowOpener("/gmCaseScheduleReport.do?method=CaseRetDshRpt&strAction=CASEOVERDSH&caseOverstDt="+startdt+"&caseOverendDt="+enddate,"Search","resizable=no,scrollbars=yes,top=150,left=150,width=1050,height=750");
}
function fnCaseRettoday(startdt,enddate){
	windowOpener("/gmCaseScheduleReport.do?method=CaseRetDshRpt&strAction=CASETODAYDSH&caseTodaystDt="+startdt+"&caseTodayendDt="+enddate,"Search","resizable=no,scrollbars=yes,top=150,left=150,width=1050,height=750");	
}
function fnCaseRettmw(startdt,enddate){
	windowOpener("/gmCaseScheduleReport.do?method=CaseRetDshRpt&strAction=CASETMWDSH&caseTmwstDt="+startdt+"&caseTmwendDt="+enddate,"Search","resizable=no,scrollbars=yes,top=150,left=150,width=1050,height=750");
}

var myPieChart;
function fnKitShipUpLoad() {
	myPieChart = new dhtmlXChart({
		view:"donut",
		container:"KitShipUpcoming",
		value:"#count#",
		pieInnerText:"#count#",
		color:"#color#",
		legend:{
			width: 140,
			align:"right",
			valign:"middle",
			template:"#month#"				
		},
		shadow:false
	});
	myPieChart.parse(Kit_ShipUpcoming_dataset,"json");	
	myPieChart.attachEvent("onItemclick", function (id, ev, trg){	
		if(id == 'KITSHIPTW'){
			windowOpener("/gmCaseScheduleReport.do?method=KitShipDshRpt&strAction=KITSHIPTW&kitupstDt="+strKitTWStDt+"&kitupendDt="+strKitTWendDt,"Search","resizable=no,scrollbars=yes,top=150,left=150,width=1050,height=750");
		}if(id == 'KITSHIPNW'){
			windowOpener("/gmCaseScheduleReport.do?method=KitShipDshRpt&strAction=KITSHIPNW&kitupstDt="+strKitNWStDt+"&kitupendDt="+strKitNWendDt,"Search","resizable=no,scrollbars=yes,top=150,left=150,width=1050,height=750");
		}if(id == 'KITSHIPTM'){
			windowOpener("/gmCaseScheduleReport.do?method=KitShipDshRpt&strAction=KITSHIPTM&kitupstDt="+strKitTMStDt+"&kitupendDt="+strKitTMendDt,"Search","resizable=no,scrollbars=yes,top=150,left=150,width=1050,height=750");
		}
});
}

function fnKitRetUpLoad(){
	myPieChart = new dhtmlXChart({
		view:"donut",
		container:"KitRetUpcoming",
		value:"#count#",
		color:"#color#",
		pieInnerText:"#count#",
		legend:{
			width: 140,
			align:"right",
			valign:"middle",
			template:"#month#"				
		},
		shadow:false
	});
	myPieChart.parse(Kit_RetUpcoming_dataset,"json");
	myPieChart.attachEvent("onItemclick", function (id, ev, trg){
		if(id == 'KITRETTW'){
			windowOpener("/gmCaseScheduleReport.do?method=KitRetDshRpt&strAction=KITRETTW&kitupstDt="+strKitRetTWStDt+"&kitupendDt="+strKitRetTWendDt,"Search","resizable=no,scrollbars=yes,top=150,left=150,width=1050,height=750");
		}if(id == 'KITRETNW'){
			windowOpener("/gmCaseScheduleReport.do?method=KitRetDshRpt&strAction=KITRETNW&kitupstDt="+strKitRetNWStDt+"&kitupendDt="+strKitRetNWendDt,"Search","resizable=no,scrollbars=yes,top=150,left=150,width=1050,height=750");
		}if(id == 'KITRETTM'){
			windowOpener("/gmCaseScheduleReport.do?method=KitRetDshRpt&strAction=KITRETTM&kitupstDt="+strKitRetTMStDt+"&kitupendDt="+strKitRetTMendDt,"Search","resizable=no,scrollbars=yes,top=150,left=150,width=1050,height=750");
		}	
		});
}
function fnCaseShipUpLoad() {
	myPieChart = new dhtmlXChart({
		view:"donut",
		container:"CaseShipUpcoming",
		pieInnerText:"#count#",
		value:"#count#",
		color:"#color#",
		legend:{
			width: 140,
			align:"right",
			valign:"middle",
			template:"#month#"				
		},
		shadow:false
	});
	myPieChart.parse(Case_ShipUpcoming_dataset,"json");	
	myPieChart.attachEvent("onItemclick", function (id, ev, trg){	
		if(id == 'CASESHIPTW'){
			windowOpener("/gmCaseScheduleReport.do?method=CaseShipDshRpt&strAction=CASESHIPTW&caseupstDt="+strCaseTWStDt+"&caseupendDt="+strCaseTWendDt,"Search","resizable=no,scrollbars=yes,top=150,left=150,width=1050,height=750");
		}if(id == 'CASESHIPNW'){
			windowOpener("/gmCaseScheduleReport.do?method=CaseShipDshRpt&strAction=CASESHIPNW&caseupstDt="+strCaseNWStDt+"&caseupendDt="+strCaseNWendDt,"Search","resizable=no,scrollbars=yes,top=150,left=150,width=1050,height=750");
		}if(id == 'CASESHIPTM'){
			windowOpener("/gmCaseScheduleReport.do?method=CaseShipDshRpt&strAction=CASESHIPTM&caseupstDt="+strCaseTMStDt+"&caseupendDt="+strCaseTMendDt,"Search","resizable=no,scrollbars=yes,top=150,left=150,width=1050,height=750");
		}
});
}

function fnCaseRetUpLoad(){
	myPieChart = new dhtmlXChart({
		view:"donut",
		container:"CaseRetUpcoming",
		pieInnerText:"#count#",
		value:"#count#",
		color:"#color#",
		legend:{
			width: 140,
			align:"right",
			valign:"middle",
			template:"#month#"				
		},
		shadow:false
	});
	myPieChart.parse(Case_RetUpcoming_dataset,"json");
	myPieChart.attachEvent("onItemclick", function (id, ev, trg){
		if(id == 'CASERETTW'){
			windowOpener("/gmCaseScheduleReport.do?method=CaseRetDshRpt&strAction=CASERETTW&caseupstDt="+strCaseRetTWStDt+"&caseupendDt="+strCaseRetTWendDt,"Search","resizable=no,scrollbars=yes,top=150,left=150,width=1050,height=750");
		}if(id == 'CASERETNW'){
			windowOpener("/gmCaseScheduleReport.do?method=CaseRetDshRpt&strAction=CASERETNW&caseupstDt="+strCaseRetNWStDt+"&caseupendDt="+strCaseRetNWendDt,"Search","resizable=no,scrollbars=yes,top=150,left=150,width=1050,height=750");
		}if(id == 'CASERETTM'){
			windowOpener("/gmCaseScheduleReport.do?method=CaseRetDshRpt&strAction=CASERETTM&caseupstDt="+strCaseRetTMStDt+"&caseupendDt="+strCaseRetTMendDt,"Search","resizable=no,scrollbars=yes,top=150,left=150,width=1050,height=750");
		}	
		});
}

