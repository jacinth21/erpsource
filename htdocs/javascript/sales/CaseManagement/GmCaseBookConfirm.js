function doOnLoad(setid,setnm) {
 
	w1 = createDhtmlxWindow(930,700);
    w1.setText(message_sales[205]); 
	w1.attachURL("/GmSetReportServlet?hOpt=SETRPT&hAction=Drilldown&hSetId="+setid+ "&hSetNm="+setnm);
	return false;
}



function fnBack()
{   
	var vCatList =  document.frmCaseBookSetDtls.categoryList.value ;
	arrayCat = vCatList.split(",");
	arrayCat.sort(sortfunc);
	fwdProdType = arrayCat[arrayCat.length-1];
	strCaseInfoID =  document.frmCaseBookSetDtls.caseInfoID.value ;
	strcaseID =  document.frmCaseBookSetDtls.caseID.value ;
	if(fwdProdType == ""){
		document.frmCaseBookSetDtls.action = "gmCaseBuilder.do?strOpt=edit&caseInfoID="+strCaseInfoID;
	}else{
		document.frmCaseBookSetDtls.action ="/gmCaseBookSetDtls.do?strOpt=edit&fromscreen=confirm&caseInfoID="+ strCaseInfoID+"&caseID="+strcaseID+"&fwdProductType="+fwdProdType;
	}
	document.frmCaseBookSetDtls.submit();
}

function sortfunc(a,b)
{
return a - b;
}

function fnProfileBack()
{   
	var vCatList =  document.frmCaseBookSetDtls.categoryList.value ;
	arrayCat = vCatList.split(",");
	arrayCat.sort(sortfunc);
	fwdProdType = arrayCat[arrayCat.length-1];
	strFavCaseID = document.frmCaseBookSetDtls.favouriteCaseID.value ;
	document.frmCaseBookSetDtls.action ="/gmCaseBookSetDtls.do?strOpt=ProfileEdit&fromscreen=confirm&favouriteCaseID="+ strFavCaseID+"&fwdProductType="+fwdProdType;
	document.frmCaseBookSetDtls.submit();
}


function fnPost()
{    
	if (strAccount == "")
	{
		Error_Details(message_sales[15]);
		Error_Show();
		Error_Clear();
		return false;
	}
	strCaseInfoID =  document.frmCaseBookSetDtls.caseInfoID.value ;
	document.frmCaseBookSetDtls.action ="/gmCasePost.do?strOpt=post&caseInfoID="+ strCaseInfoID ;
	fnStartProgress('Y');
    document.frmCaseBookSetDtls.submit();
}


function fnCaseVoid(){
	 
            document.frmCaseBookSetDtls.hCancelType.value = 'VDCASE';           
            document.frmCaseBookSetDtls.hTxnId.value = strCaseInfoID; 
            document.frmCaseBookSetDtls.hTxnName.value = strcaseID; 
            document.frmCaseBookSetDtls.action ="/GmCommonCancelServlet";            
            document.frmCaseBookSetDtls.hAction.value = "Load";                     
            document.frmCaseBookSetDtls.submit();
      }