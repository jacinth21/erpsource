// case Inventory 
function fnOnPageLoad_Case(){
	if(objGridInvData != ""){
		gridInvObj = initGridInvData('caseInventoryInfo', objGridInvData);
	}
}
function initGridInvData(divRef, gridData) {
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/extweb/dhtmlx/dhtmlxGrid/imgs/");
	gObj.init();
	gObj.loadXMLString(gridData);
    return gObj;
}
