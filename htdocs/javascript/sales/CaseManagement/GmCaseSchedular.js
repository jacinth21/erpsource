function fncheckdate(){
	var surgeryDt=document.frmCaseSchedularForm.surgeryDt;

	if(gCmpDateFmt=='MM/dd/yyyy')
		CommonDateValidation(surgeryDt, gCmpDateFmt,  message[1]);
	if(gCmpDateFmt=='dd/MM/yyyy')
		CommonDateValidation(surgeryDt, gCmpDateFmt,  message[5316]);		
	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}else{
		fnAddShipDate(surgeryDt,gCmpDateFmt);
		fnAddReturnDate(surgeryDt,gCmpDateFmt);
	}
}

function padNumber(number) {
    var string  = '' + number;
    string      = string.length < 2 ? '0' + string : string;
    return string;
}

function getDateFormatMMDDYYYY(today){
 
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!

    var yyyy = today.getFullYear();
    if (dd < 10) {
      dd = '0' + dd;
    } 
    if (mm < 10) {
      mm = '0' + mm;
    } 
 return   today = mm + '/' + dd + '/' + yyyy;
}

function getDateFormatDDMMYYYY(today){
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!

    var yyyy = today.getFullYear();
    if (dd < 10) {
      dd = '0' + dd;
    } 
    if (mm < 10) {
      mm = '0' + mm;
    } 
 return   today = dd + '/' + mm + '/' + yyyy;
}

/*function WeekEndDDMMYYYY(today,i){
    var dd = today.getDate()+i;
    var mm = today.getMonth() + 1; //January is 0!

    var yyyy = today.getFullYear();
    if (dd < 10) {
      dd = '0' + dd;
    } 
    if (mm < 10) {
      mm = '0' + mm;
    } 
 return   today = dd + '/' + mm + '/' + yyyy;
}

function WeekEndMMDDYYYY(today,i){
    var dd = today.getDate()+i;
    var mm = today.getMonth() + 1; //January is 0!

    var yyyy = today.getFullYear();
    if (dd < 10) {
      dd = '0' + dd;
    } 
    if (mm < 10) {
      mm = '0' + mm;
    } 
 return   today = mm + '/' + dd + '/' + yyyy;
}*/
// 
function fnAddShipDate(surgeryDt,gCmpDateFmt){  
     date      = new Date(surgeryDt.value);
     var today = new Date();
     day       = date.getDay();     
     
     if(gCmpDateFmt == 'MM/dd/yyyy'){
    	 var   Sur_date = getDateFormatMMDDYYYY(date);    
         var   Cur_date = getDateFormatMMDDYYYY(today); 
         var curent_day = today.getDay();
         
         //var dateCur_date = Cur_date.split("/");
         var Current_date = new Date(); 
     	 var NextDayoneAdd = new Date(Current_date.setDate(Current_date.getDate()+1));	
     	 var NextDayone = padNumber(NextDayoneAdd.getMonth()+1) + '/' + padNumber(NextDayoneAdd.getDate()) + '/' + padNumber(NextDayoneAdd.getFullYear());
     	 var Current_date = new Date(); 
     	 var NextDaytwoAdd = new Date(Current_date.setDate(Current_date.getDate()+2));
     	 var NextDaytwo = padNumber(NextDaytwoAdd.getMonth()+1) + '/' + padNumber(NextDaytwoAdd.getDate()) + '/' + padNumber(NextDaytwoAdd.getFullYear());
     	 var Current_date = new Date(); 
    	 var NextDaythreeAdd = new Date(Current_date.setDate(Current_date.getDate()+3));	
     	 var NextDaythree = padNumber(NextDaythreeAdd.getMonth()+1) + '/' + padNumber(NextDaythreeAdd.getDate()) + '/' + padNumber(NextDaythreeAdd.getFullYear());
     	 var Current_date = new Date(); 
     	 var NextDayfourAdd = new Date(Current_date.setDate(Current_date.getDate()+4));	
     	 var NextDayfour = padNumber(NextDayfourAdd.getMonth()+1) + '/' + padNumber(NextDayfourAdd.getDate()) + '/' + padNumber(NextDayfourAdd.getFullYear());
         
         if((Cur_date == Sur_date && day != 5) || (Cur_date == Sur_date && day == 5)){
        	 next_date = new Date(date.setDate(date.getDate()));
         }else if((Sur_date == NextDayone && curent_day != 4) || (Sur_date == NextDaythree && curent_day != 4)|| (Sur_date == NextDayfour && curent_day != 4 && day != 5)){
        	 Current_date = new Date(); 
        	 next_date = new Date(Current_date.setDate(Current_date.getDate()));
         }else if(Sur_date == NextDayone && curent_day == 4){
        	 Current_date = new Date(); 
        	 next_date = new Date(Current_date.setDate(Current_date.getDate()));
         }else if(Sur_date == NextDaytwo && curent_day == 3) {
        	 Current_date = new Date(); 
        	 next_date = new Date(Current_date.setDate(Current_date.getDate()));
         }else if((Sur_date == NextDaytwo && curent_day != 3) || (Sur_date == NextDaytwo && curent_day != 4)) {
        	 Current_date = new Date(); 
        	 next_date = new Date(Current_date.setDate(Current_date.getDate()));
         }else if(day==1){
        	 next_date = new Date(date.setDate(date.getDate() - 4));
         }else if(day==2 || day==3 ){
        	 next_date = new Date(date.setDate(date.getDate() - 5));
         }else if(day==4 || day==5){
        	 next_date = new Date(date.setDate(date.getDate() - 3));
         }
         if(day == 6 || day == 0){
        	 document.all.shipDt.value ='';
         }else{
        	 formatted = padNumber(next_date.getMonth() +1) + '/' + padNumber(next_date.getDate()) + '/' + padNumber(next_date.getFullYear())
        	 document.all.shipDt.value = formatted;
         }
         
     }else if(gCmpDateFmt == 'dd/MM/yyyy'){
    	var surgeryDt = fnDateFmtOnBlurCheck(surgeryDt,gCmpDateFmt);    	
    	var dateParts = surgeryDt.split("/");

    	// month is 0-based, that's why we need dataParts[1] - 1
    	date = new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0]); 
    	day       = date.getDay();

    	 var   Sur_date = getDateFormatDDMMYYYY(date);    
         var   Cur_date = getDateFormatDDMMYYYY(today); 

         var dateCur_date = Cur_date.split("/");
         Current_date = new Date(+dateCur_date[2], dateCur_date[1] - 1, +dateCur_date[0]); 
     	 var NextDayoneAdd = new Date(Current_date.setDate(Current_date.getDate()+1));	
     	 var NextDayone = padNumber(NextDayoneAdd.getDate()) + '/' + padNumber(NextDayoneAdd.getMonth() + 1) + '/' + padNumber(NextDayoneAdd.getFullYear());
     	 Current_date = new Date(+dateCur_date[2], dateCur_date[1] - 1, +dateCur_date[0]); 
     	 var NextDaytwoAdd = new Date(Current_date.setDate(Current_date.getDate()+2));
     	 var NextDaytwo = padNumber(NextDaytwoAdd.getDate()) + '/' + padNumber(NextDaytwoAdd.getMonth() + 1) + '/' + padNumber(NextDaytwoAdd.getFullYear());
     	 Current_date = new Date(+dateCur_date[2], dateCur_date[1] - 1, +dateCur_date[0]); 
    	 var NextDaythreeAdd = new Date(Current_date.setDate(Current_date.getDate()+3));	
     	 var NextDaythree = padNumber(NextDaythreeAdd.getDate()) + '/' + padNumber(NextDaythreeAdd.getMonth() + 1) + '/' + padNumber(NextDaythreeAdd.getFullYear());
     	 Current_date = new Date(+dateCur_date[2], dateCur_date[1] - 1, +dateCur_date[0]); 
     	 var NextDayfourAdd = new Date(Current_date.setDate(Current_date.getDate()+4));	
     	 var NextDayfour = padNumber(NextDayfourAdd.getDate()) + '/' + padNumber(NextDayfourAdd.getMonth() + 1) + '/' + padNumber(NextDayfourAdd.getFullYear());
        	
         var curent_day = today.getDay();
         if((Cur_date == Sur_date && day != 5) || (Cur_date == Sur_date && day == 5)){
        	 next_date = new Date(date.setDate(date.getDate()));
         }else if((Sur_date == NextDayone && curent_day != 4) || (Sur_date == NextDaythree && curent_day != 4)|| (Sur_date == NextDayfour && curent_day != 4 && day != 5)){
        	 Current_date = new Date(+dateCur_date[2], dateCur_date[1] - 1, +dateCur_date[0]); 
        	 next_date = new Date(Current_date.setDate(Current_date.getDate()));
         }else if(Sur_date == NextDayone && curent_day == 4){
        	 Current_date = new Date(+dateCur_date[2], dateCur_date[1] - 1, +dateCur_date[0]); 
        	 next_date = new Date(Current_date.setDate(Current_date.getDate()));
         }else if(Sur_date == NextDaytwo && curent_day == 3) {        	 
        	 Current_date = new Date(+dateCur_date[2], dateCur_date[1] - 1, +dateCur_date[0]); 
        	 next_date = new Date(Current_date.setDate(Current_date.getDate()));
         }else if((Sur_date == NextDaytwo && curent_day != 3) || (Sur_date == NextDaytwo && curent_day != 4)) {
        	 Current_date = new Date(+dateCur_date[2], dateCur_date[1] - 1, +dateCur_date[0]); 
        	 next_date = new Date(Current_date.setDate(Current_date.getDate()));
         }else if(day==1){
        	 next_date = new Date(date.setDate(date.getDate() - 4));
         }else if(day==2 || day==3 ){
        	 next_date = new Date(date.setDate(date.getDate() - 5));
         }else if(day==4 || day==5){
        	 next_date = new Date(date.setDate(date.getDate() - 3));
         }
         if(day == 6 || day == 0){
        	 document.all.shipDt.value ='';
         }else{
        	 formatted = padNumber(next_date.getDate()) + '/' + padNumber(next_date.getMonth() + 1) + '/' + padNumber(next_date.getFullYear())
             document.all.shipDt.value = formatted;  
         }
     } 
}

function fnAddReturnDate(surgeryDt){  
	date      = new Date(surgeryDt.value);
    var today = new Date();
    day       = date.getDay();
    if(gCmpDateFmt == 'MM/dd/yyyy'){ 
   	 	var   Sur_date = getDateFormatMMDDYYYY(date);    
     	var   Cur_date = getDateFormatMMDDYYYY(today);
        var curent_day = today.getDay();
        var Current_date = new Date(); 
    	var NextDayoneAdd = new Date(Current_date.setDate(Current_date.getDate()+1));	
    	var NextDayone = padNumber(NextDayoneAdd.getMonth()+1) + '/' + padNumber(NextDayoneAdd.getDate()) + '/' + padNumber(NextDayoneAdd.getFullYear());
    	var Current_date = new Date(); 
    	var NextDaytwoAdd = new Date(Current_date.setDate(Current_date.getDate()+2));
    	var NextDaytwo = padNumber(NextDaytwoAdd.getMonth()+1) + '/' + padNumber(NextDaytwoAdd.getDate()) + '/' + padNumber(NextDaytwoAdd.getFullYear());
        
     	   if(Cur_date == Sur_date && day != 5 && day != 4){
          	 next_date = new Date(date.setDate(date.getDate() + 2));
           }else if(Cur_date == Sur_date && day != 5 && day != 4){
          	 next_date = new Date(date.setDate(date.getDate() + 2));
           }else if(Sur_date == NextDayone && curent_day == 4){
           	next_date = new Date(date.setDate(date.getDate() + 3));
           }else if(Sur_date == NextDaytwo && curent_day == 3) {
          	 next_date = new Date(date.setDate(date.getDate() +3));
           }else if(day==1 || day==2 || day==3){
          	 next_date = new Date(date.setDate(date.getDate() + 2));
           }else if(day==4){
          	 next_date = new Date(date.setDate(date.getDate() + 4));
           }else if(day==5 || (Cur_date == Sur_date && day == 5)){
          	 next_date = new Date(date.setDate(date.getDate() + 3));
           }else if((Cur_date == Sur_date && day == 4)){
          	 next_date = new Date(date.setDate(date.getDate() + 4));
           }
     		if(day == 6 || day == 0){
     			document.all.retDt.value ='';
     		}else{
     			formatted = padNumber(next_date.getMonth() +1) + '/' + padNumber(next_date.getDate() ) + '/' + padNumber(next_date.getFullYear())
     			document.all.retDt.value = formatted;
         }
     }else if(gCmpDateFmt == 'dd/MM/yyyy'){
    	  	var surgeryDt = fnDateFmtOnBlurCheck(surgeryDt,gCmpDateFmt);    	
        	var dateParts = surgeryDt.split("/");
        	// month is 0-based, that's why we need dataParts[1] - 1
        	date = new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0]); 
        	day       = date.getDay();
   	 	var   Sur_date = getDateFormatDDMMYYYY(date);    
        var   Cur_date = getDateFormatDDMMYYYY(today); 
        var curent_day = today.getDay();
        var dateCur_date = Cur_date.split("/");
        Current_date = new Date(+dateCur_date[2], dateCur_date[1] - 1, +dateCur_date[0]); 
    	var NextDayoneAdd = new Date(Current_date.setDate(Current_date.getDate()+1));	
    	var NextDayone = padNumber(NextDayoneAdd.getDate()) + '/' + padNumber(NextDayoneAdd.getMonth() + 1) + '/' + padNumber(NextDayoneAdd.getFullYear());
    	Current_date = new Date(+dateCur_date[2], dateCur_date[1] - 1, +dateCur_date[0]); 
    	var NextDaytwoAdd = new Date(Current_date.setDate(Current_date.getDate()+2));
    	var NextDaytwo = padNumber(NextDaytwoAdd.getDate()) + '/' + padNumber(NextDaytwoAdd.getMonth() + 1) + '/' + padNumber(NextDaytwoAdd.getFullYear());
        
        if(Cur_date == Sur_date && day != 5 && day != 4){
       	 next_date = new Date(date.setDate(date.getDate() + 2));
        }else if(Sur_date == NextDayone && curent_day == 4){
        	next_date = new Date(date.setDate(date.getDate() + 3));
        }else if(Sur_date == NextDaytwo && curent_day == 3) {
       	 next_date = new Date(date.setDate(date.getDate() +3));
        }else if(day==1 || day==2 || day==3){
       	 next_date = new Date(date.setDate(date.getDate() + 2));
        }else if(day==4){
       	 next_date = new Date(date.setDate(date.getDate() + 4));
        }else if(day==5 || (Cur_date == Sur_date && day == 5)){
       	 next_date = new Date(date.setDate(date.getDate() + 3));
        }else if((Cur_date == Sur_date && day == 4)){
       	 next_date = new Date(date.setDate(date.getDate() + 4));
        }
        
        if(day == 6 || day == 0){
        	document.all.retDt.value ='';
        }else{
        	formatted = padNumber(next_date.getDate()) + '/' + padNumber(next_date.getMonth() + 1) + '/' + padNumber(next_date.getFullYear())
        	document.all.retDt.value = formatted;
        }
    } 
}
function process(date){
	   var parts = date.split("/");
	   return new Date(parts[2], parts[1] - 1, parts[0]);
}

function check(){	
	var status = parent.document.frmCaseSchedularForm.status.value
if(document.frmCaseSchedularForm.longTerm.title == "off"){	
	document.frmCaseSchedularForm.longTerm.value="on";
	document.frmCaseSchedularForm.longTerm.title = "on";
/*	$("#RetDt *").removeAttr("disabled");*/
}else{
	document.frmCaseSchedularForm.longTerm.value="off";
	document.frmCaseSchedularForm.longTerm.title = "off";	
	/*if(status == '107161')
	document.getElementById("retDt").disabled = true;
	$("#RetDt *").attr("disabled", "disabled").off('click');*/
}
}

function fnCaseSubmit(frm){
    var ship_date = document.frmCaseSchedularForm.shipDt.value;
	var return_date = document.frmCaseSchedularForm.retDt.value;
	var surgery_date = document.frmCaseSchedularForm.surgeryDt.value;
	var fieldsales = document.frmCaseSchedularForm.distributor.value;
	var salesrep = document.frmCaseSchedularForm.salesrep.value;
	var account = document.frmCaseSchedularForm.account.value;
	var division = document.frmCaseSchedularForm.division.value;
	var status = parent.document.frmCaseSchedularForm.status.value;
	var case_id = parent.document.all.caseId.innerHTML;
	var longterm = document.all.longTerm.title;
	var comments = document.frmCaseSchedularForm.comments.value;
	if(longterm == 'on'){
		longterm = 'Y';
	}else{
		longterm = 'N';
	}
	if(gCmpDateFmt == "dd/MM/yyyy"){
		var today = new Date(); 
        var   Cur_date = getDateFormatDDMMYYYY(today); 
		if(case_id==''){  
		if(process(surgery_date) < process(Cur_date)){
			Error_Details(message_sales[427]);	
		}}if(surgery_date == ''){
			Error_Details(message_sales[410]);	
		}else if(process(surgery_date) < process(ship_date)){
			Error_Details(message_sales[416]);	
		}if(ship_date == ''){
			Error_Details(message_sales[411]);	
		}if(return_date == ''){
			Error_Details(message_sales[412]);	
		}else if((process(surgery_date) || process(ship_date) || process(Cur_date) ) > process(return_date)){
			Error_Details(message_sales[417]);	
		}
	}else{
		var today = new Date();
		today = getDateFormatMMDDYYYY(today);
		if(case_id==''){
//		var today = new Date();
//		today = getDateFormatMMDDYYYY(today);
		date      = new Date(surgery_date);
		date = getDateFormatMMDDYYYY(date);
		if(date < today){
			Error_Details(message_sales[427]);	
		}}if(surgery_date == ''){
			Error_Details(message_sales[410]);	
		}else if(surgery_date < ship_date){
			Error_Details(message_sales[416]);	
		}if(ship_date == ''){
			Error_Details(message_sales[411]);	
		}if(return_date == ''){
			Error_Details(message_sales[412]);	
		}else if((process(surgery_date) || process(ship_date) || process(Cur_date) ) > process(return_date)){
			Error_Details(message_sales[417]);	
		}
	}
	if(fieldsales == '0'){
		Error_Details(message_sales[413]);
	}
	if(comments == ''){
		Error_Details(message_sales[117]);	
	}
	fnValidateDropDn('division',message_prodmgmnt[320]);
	fnValidateDropDn('account',message_sales[415]);
	fnValidateDropDn('salesrep',message_sales[414]);
	//fnValidateDropDn('salesfield',message_sales[413]);
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	} else {
		if(case_id != ''){	
			var ajaxUrl = fnAjaxURLAppendCmpInfo('gmCaseSchedular.do?strOpt=checkStatus&case_id='+case_id+'&return_date='+return_date+'&gCmpDateFmt='+gCmpDateFmt+'&ramdomId=' + Math.random());	
			dhtmlxAjax.get(ajaxUrl,function(loader){
					response = loader.xmlDoc.responseText;
					var mySplitResult = response.split("^");
					if(response!=null && response!=''){ 
				   			checkStatus = mySplitResult[0]; 
				   			checkInv = mySplitResult[1];	
				   			checkReturnVal = mySplitResult[2];
					}
					var RetrunDateExistingKit = checkReturnVal.split(",");
					kitcaseid = RetrunDateExistingKit[0];			
					kitcasename = RetrunDateExistingKit[1];
					if(RetrunDateExistingKit != '' && checkStatus == 107161){
						Error_Details("Entered <b>Return Date</b> will conflict with upcoming case <b>"+kitcaseid+"</b> for Kit <b>"+kitcasename+"</b>");
					}
					/*if(checkStatus != 107160){
						Error_Details(message_sales[419]);
					}*/if(checkStatus != 107161 && status == 107162)	{
						Error_Details(message_sales[420]);
					}if(checkStatus != 107160 && status == 107163)	{
						Error_Details(message_sales[421]);
					}if(checkStatus == 107161 && status == 107160){
						Error_Details(message_sales[426]);
					}if(checkInv == 'Y' && ((status == 107161) || (status == 107162))){
						Error_Details(message_sales[424]);
					}if (ErrorCount > 0){
						Error_Show();
						Error_Clear();
						return false;
					}else{
						if((process(surgery_date) || process(ship_date) || process(Cur_date) ) > process(return_date)){
							Error_Details(message_sales[417]);
							Error_Show();
							Error_Clear();
							return false;
						}
						var ajaxUrl = fnAjaxURLAppendCmpInfo('gmCaseSchedular.do?strOpt=saveCaseDetails&case_id='+case_id+'&surgery_date='+surgery_date +'&ship_date='+ship_date+'&return_date='+return_date+'&division='+division+'&field_sales='+fieldsales+'&sales_rep='+salesrep+'&account='+account+'&status='+status+'&longterm='+longterm+'&gCmpDateFmt='+gCmpDateFmt+'&companyId='+companyId+'&companyInfo='+encodeURIComponent(compInfObj)+'&comments='+comments+'&ramdomId=' + Math.random());	
						dhtmlxAjax.get(ajaxUrl,function(loader){
								var resJSONObj = $.parseJSON(loader.xmlDoc.responseText);
								$(".TRLOG").remove();
								$(".NODATA").remove();
								$(".LOADMORE").remove();	
								for(i= 0; i<resJSONObj.length-1;i++){
									itemJSON = resJSONObj[i];								
									var UNAME= itemJSON["UNAME"];
									var DT= itemJSON["DT"];
									var CMTS= itemJSON["COMMENTS"];
									fnAddCaseLog(i);
									document.getElementById("DTC"+i).innerHTML = DT;
									document.getElementById("UNAMEC"+i).innerHTML = UNAME; 
									document.getElementById("COMMENTSC"+i).innerHTML = CMTS;									
								}
								var response = resJSONObj[resJSONObj.length-1];
								parent.document.all.caseId.innerHTML = response;
								document.all.SucMsg.innerHTML = "<b>"+response +" Saved Successfully</b>";
						});	
						if(status == 107162 || status == 107163){
							document.getElementById("surgeryDt").disabled = true;
							document.getElementById("shipDt").disabled = true;
						/*	if(status == 107161 && longterm == "Y"){
								document.getElementById("retDt").disabled = false;
							}
							else{
								document.getElementById("retDt").disabled = true;
								document.getElementById("divretDt").disabled = true;
							}*/ 	
							if(status == 107161 && longterm == "N"){
								//document.getElementById("retDt").disabled = false;
								document.all.longTerm.disabled = false;
							}
							document.getElementById("division").disabled = true;
							document.getElementById("salesrep").disabled = true;
							document.getElementById("account").disabled = true;
							document.getElementById("distributor").disabled = true;
							//document.all.longTerm.disabled = true;	
							if(status == 107162 || status == 107163){
								document.all.longTerm.disabled = true;	
							}
							if(status == 107162 && process(Cur_date)  < process(return_date)){
				                document.frmCaseSchedularForm.retDt.value = Cur_date;
							}					
						}
						if(status == 107162 || status == 107163){
							document.getElementById("retDt").disabled = true;
						}
						document.getElementById("comments").value='';
					}
				
			});
		}else{
			if(status != '107160'){
				Error_Details(message_sales[418]);
				Error_Show();
				Error_Clear();
				return false;
			}else{
		var ajaxUrl = fnAjaxURLAppendCmpInfo('gmCaseSchedular.do?strOpt=saveCaseDetails&case_id='+case_id+'&surgery_date='+surgery_date +'&ship_date='+ship_date+'&return_date='+return_date+'&division='+division+'&field_sales='+fieldsales+'&sales_rep='+salesrep+'&account='+account+'&status='+status+'&longterm='+longterm+'&comments='+comments+'&ramdomId=' + Math.random());	
		dhtmlxAjax.get(ajaxUrl,function(loader){
			var resJSONObj = $.parseJSON(loader.xmlDoc.responseText);
			$(".NODATA").remove();
			for(i= 0; i<resJSONObj.length-1;i++){
				itemJSON = resJSONObj[i];
			
				var UNAME= itemJSON["UNAME"];
				var DT= itemJSON["DT"];
				var CMTS= itemJSON["COMMENTS"];
				fnAddCaseLog(i);
				document.getElementById("DTC"+i).innerHTML = DT;
				document.getElementById("UNAMEC"+i).innerHTML = UNAME; 
				document.getElementById("COMMENTSC"+i).innerHTML = CMTS;
				//document.getElementById("tagnum"+cnt).innerHTML = tagNum;
			}
			
			    var response = resJSONObj[resJSONObj.length-1];
				parent.document.all.caseId.innerHTML = response;	
				document.all.SucMsg.innerHTML = "<b>"+response +" Saved Successfully</b>";
		});
		document.getElementById("comments").value='';
			}
		}
	}
}

function fnAddCaseLog(cnt){
	
	var tbody = document.getElementById("CASELOG").getElementsByTagName("TBODY")[0];
	var row = document.createElement("TR")
    row.setAttribute("name", "TRCLOG");
	row.setAttribute("id", "TRCLOG"+cnt);
    row.setAttribute("class", "TRLOG");
	
    var td0 = document.createElement("TD")
    td0.innerHTML = '<span id=DTC'+cnt+'>';
    td0.height="20";
	td0.align = "left";

	var td1 = document.createElement("TD")
    td1.innerHTML = '<span id=UNAMEC'+cnt+'>';
	td1.align = "left";
	
    var td2 = document.createElement("TD")
    td2.innerHTML = '&nbsp;<span id=COMMENTSC'+cnt+'>';
	td2.align = "left";
  
	row.appendChild(td0);
	row.appendChild(td1);
	row.appendChild(td2);
    tbody.appendChild(row);	
}
function fnFetechKitDetails(){
	val = document.all.kitID.value;
	kitname = document.all.searchkitID.value;
	caseid = parent.document.all.caseId.innerHTML;
	var ajaxUrl = fnAjaxURLAppendCmpInfo("gmCaseSchedular.do?strOpt=fetchKitDetails&kitId="+val+"&caseid="+caseid+"&KitName="+kitname+"&companyInfo="+compInfObj+"&ramdomId=" + Math.random());  
	dhtmlxAjax.get(ajaxUrl, function(loader){	
		var resJSONObj = $.parseJSON(loader.xmlDoc.responseText);
		var response = resJSONObj[resJSONObj.length-1];	
		var kitName= resJSONObj[0]["KITNM"];
		var kitId = resJSONObj[0]["KITID"];
		var kitstatusval = resJSONObj[0]["KITSTS"];
        var kitstatus = '';
        var strCaseVal= '';
        var mySplitResult = kitstatusval.split("^");
		if(kitstatusval!=null && kitstatusval!=''){ 
			 kitstatus = mySplitResult[0]; 
			 strCaseVal = mySplitResult[1]; 
		}		
		if(kitstatus != 'N'){
			if(resJSONObj.length>0){
				cnt = document.all.hRowCnt.value;
		var tbody = document.getElementById("KitMapping").getElementsByTagName("TBODY")[0];
			
		var row1 = document.createElement("TR");
		    row1.setAttribute("id", "TR-"+kitName);
		    row1.setAttribute("class","RightTableCaption "+kitName);
		    row1.style.backgroundColor = "#e6e6e6";
		    row1.style.height = "20";
		    
			var td4 = document.createElement("TD");
			td4.setAttribute("colspan","4");
		    td4.innerHTML = '&nbsp;&nbsp;<a href=javascript:fnRmeKit("'+kitId+'","'+caseid+'","'+encodeURIComponent(kitName)+'"); tabindex=\'-1\'><img border=0  Alt=Remove  title=Remove  valign=left src=/images/red_btn_minus.gif height=10 width=9></a>&nbsp;<span id=kitnm'+cnt+'>';
			td4.align = "left";
			row1.appendChild(td4);
			tbody.appendChild(row1);

			document.getElementById("kitnm"+cnt).innerHTML = kitName;
			cnt++;
			document.all.hRowCnt.value = cnt;	
	}
			$(".TRLOG").remove();
			$(".NODATA").remove();
			$(".LOADMORE").remove();
			$(".theader").remove();
			$(".theaderLine").remove();	
			fnAddInvHeader();				
			for(i= 0; i<response.length;i++){
				 itemJSON = response[i];
					var Date = itemJSON["DT"];
					var Uname = itemJSON["UNAME"];
					var Comments = itemJSON["COMMENTS"];					
					fnAddInv(i);
					document.getElementById("DTC"+i).innerHTML = Date;
					document.getElementById("UNAMEC"+i).innerHTML = Uname; 
					document.getElementById("COMMENTSC"+i).innerHTML = Comments;
				}
			document.all.searchkitID.value = '';
			document.all.kitID.value = '';
			document.all.validSetError.style.display = "none";
		for(i= 0; i<resJSONObj.length-1;i++){
			itemJSON = resJSONObj[i];
		
			kitNm= itemJSON["KITNM"];
			var setId= itemJSON["SETID"];
			var setDesc= itemJSON["SETNM"];
			var setQty= itemJSON["QTY"];
			var tagNum= itemJSON["TAGID"];
			cnt = document.all.hRowCnt.value;	
			fnAdd(kitNm,cnt,i);
			document.getElementById("setid"+cnt).innerHTML = setId;
			document.getElementById("setnm"+cnt).innerHTML = setDesc; 
			document.getElementById("setqty"+cnt).innerHTML = setQty;
			document.getElementById("tagnum"+cnt).innerHTML = tagNum;
		}		 
		if(resJSONObj[resJSONObj.length-1] == '107161'){
			parent.document.all.status.value = '107161';		
		}
	}else{
		 document.all.validSetError.style.display = "inline";
		 document.all.msg.title = kitName + " is not available for this case, Case Dates overlap with "+strCaseVal;
	}
	});
}

function fnAddInvHeader(){
	var tbody = document.getElementById("CASELOG").getElementsByTagName("TBODY")[0];
	
	var row3 = document.createElement("TR")
	row3.setAttribute("class", "theaderLine");
	var td8 = document.createElement("TD");
    td8.colspan = '3'; 
	td8.height = "70";
		
	var row2 = document.createElement("TR")
    row2.setAttribute("bgcolor", "#EEEEEE");
    row2.setAttribute("class", "RightTableCaption theader");
    
    var td5 = document.createElement("TD");
    td5.innerHTML = 'Date';
    td5.class="RightTableCaption";
	td5.width = "60";
	td5.height = "30";
	
	var td6 = document.createElement("TD");
	td6.innerHTML = 'User Name';
	td6.class="RightTableCaption";
    td6.width = "140";
		
	var td7 = document.createElement("TD");
	td7.innerHTML = 'Comments';
	td7.class="RightTableCaption";
	td7.width = "500";
	
	row3.appendChild(td8);
	row2.appendChild(td5);
	row2.appendChild(td6);
	row2.appendChild(td7);
	tbody.appendChild(row3);
	tbody.appendChild(row2);	
}
function fnAddInv(cnt){
	var tbody = document.getElementById("CASELOG").getElementsByTagName("TBODY")[0];
	var row = document.createElement("TR")
    row.setAttribute("name", "TRCLOG");
    row.setAttribute("class", "TRLOG");
	
    var td0 = document.createElement("TD")
    td0.innerHTML = '<span id=DTC'+cnt+'>';
    td0.height="20";
	td0.align = "left";

	var td1 = document.createElement("TD")
    td1.innerHTML = '<span id=UNAMEC'+cnt+'>';
	td1.align = "left";
	
    var td2 = document.createElement("TD")
    td2.innerHTML = '&nbsp;<span id=COMMENTSC'+cnt+'>';
	td2.align = "left";
  
	row.appendChild(td0);
	row.appendChild(td1);
	row.appendChild(td2);
    tbody.appendChild(row);	
}
function fnAdd(kitName,cnt,i){
 	var tbody = document.getElementById("KitMapping").getElementsByTagName("TBODY")[0];
    var row = document.createElement("TR")
     row.setAttribute("name", "TR"+kitName);
    row.setAttribute("id", "TR"+cnt);
    if(i % 2 == 0)
    	row.setAttribute("class", kitName+" Shade");
    else
    	row.setAttribute("class", kitName);
    row.style.height = "20";
	
    var td0 = document.createElement("TD")
    td0.innerHTML = '<span id=setid'+cnt+'>';
	td0.align = "left";

    var td1 = document.createElement("TD")
    td1.innerHTML = '<span id=setnm'+cnt+'>';
	td1.align = "left";
	
    var td2 = document.createElement("TD")
    td2.innerHTML = '&nbsp;<span id=setqty'+cnt+'>';
	td2.align = "center";
	
    var td3 = document.createElement("TD")
    td3.innerHTML = '<span id=tagnum'+cnt+'>';
	td3.align = "center";
	
	row.appendChild(td0);
	row.appendChild(td1);
	row.appendChild(td2);
	row.appendChild(td3);
    tbody.appendChild(row);
    
    cnt++;
	document.all.hRowCnt.value = cnt;		
}

function fnRmeKit(kitid,caseid,val){
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmCaseSchedular.do?strOpt=RemoveKit&kitId='+kitid+'&caseId='+caseid+'&KitName='+val+'&companyId='+companyId+'&gmCmpDtFmt='+gCompDateFmt+'&ramdomId=' +Math.random());
	dhtmlxAjax.get(ajaxUrl,function(loader)
	{
		var resJSONObj = $.parseJSON(loader.xmlDoc.responseText);
		var checkdate = resJSONObj[resJSONObj.length-1];
		var count = resJSONObj[resJSONObj.length-2];
		if(checkdate == 'Y'){
			 Error_Details(message_sales[423]);
		 }else if(count == '1'){
			 Error_Details(message_sales[422]);
		 }else if(count != '1'){
				var elements = document.getElementsByClassName(val);
			    while(elements.length > 0){
			        elements[0].parentNode.removeChild(elements[0]);
			    }
			    $(".TRLOG").remove();
				$(".NODATA").remove();
				$(".LOADMORE").remove();			
				for(i= 0; i<resJSONObj.length-2;i++){
					 itemJSON = resJSONObj[i];
						var Date = itemJSON["DT"];
						var Uname = itemJSON["UNAME"];
						var Comments = itemJSON["COMMENTS"];
						fnAddInv(i);
						document.getElementById("DTC"+i).innerHTML = Date;
						document.getElementById("UNAMEC"+i).innerHTML = Uname; 
						document.getElementById("COMMENTSC"+i).innerHTML = Comments;
					}
		 }if(ErrorCount > 0){
			Error_Show();
			Error_Clear();
			return false;
		 }
	});
}

function fnCaseNoteSubmit(frm){
	caseid = parent.document.all.caseId.innerHTML;
	document.frmCaseSchedularForm.action = '/gmCaseSchedular.do?strOpt=saveCaseNotes&caseId='+caseid+'&gmCmpDtFmt='+gCompDateFmt;
	document.frmCaseSchedularForm.submit();
}

function fnRemoveItem(kitid,caseid,kitnm){
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmCaseSchedular.do?strOpt=RemoveKit&kitId='+kitid+'&caseId='+caseid+'&KitName='+kitnm+'&companyId='+companyId+'&gmCmpDtFmt='+gCompDateFmt+'&ramdomId=' +Math.random());
	dhtmlxAjax.get(ajaxUrl,function(loader){
		var resJSONObj = $.parseJSON(loader.xmlDoc.responseText);
		var checkdate = resJSONObj[resJSONObj.length-1];
		var count = resJSONObj[resJSONObj.length-2];
		if(checkdate == 'Y'){
			 Error_Details(message_sales[423]);
		 }else if(count == '1'){
			 Error_Details(message_sales[422]);
		 }else if(count != '1'){
			 var rows = document.getElementById("tr"+kitnm);
				rows.parentNode.removeChild(rows);
				var row = document.getElementById("div"+kitnm);
				row.parentNode.removeChild(row);
				$(".TRLOG").remove();
				$(".NODATA").remove();
				$(".LOADMORE").remove();			
				for(i= 0; i<resJSONObj.length-2;i++){
					 itemJSON = resJSONObj[i];
						var Date = itemJSON["DT"];
						var Uname = itemJSON["UNAME"];
						var Comments = itemJSON["COMMENTS"];
						fnAddInv(i);
						document.getElementById("DTC"+i).innerHTML = Date;
						document.getElementById("UNAMEC"+i).innerHTML = Uname; 
						document.getElementById("COMMENTSC"+i).innerHTML = Comments;
					}
		 }if(ErrorCount > 0){
			Error_Show();
			Error_Clear();
			return false;
		 }
	});	
}

function fnCaseReset(frm){
	  document.frmCaseSchedularForm.shipDt.value ='';
	  document.frmCaseSchedularForm.retDt.value ='';
	  document.frmCaseSchedularForm.surgeryDt.value ='';
	  document.frmCaseSchedularForm.distributor.value='0';
	  document.frmCaseSchedularForm.salesrep.value='0';
	  document.frmCaseSchedularForm.account.value='0';
	  document.frmCaseSchedularForm.division.value='2000';
}

function fnLoad(){
	if(status == 107162 || status == 107163 || status == 107161){		
		document.getElementById("surgeryDt").disabled = true;
		document.getElementById("shipDt").disabled = true;
		/*if(status == 107161 && strLongTerm == "Y"){
			document.getElementById("retDt").disabled = false;
		}
		else{
			document.getElementById("retDt").disabled = true;
			$("#RetDt *").attr("disabled", "disabled").off('click');
		}*/
		document.getElementById("division").disabled = true;
		document.getElementById("salesrep").disabled = true;
		document.getElementById("account").disabled = true;
		document.getElementById("distributor").disabled = true;
		if(status == 107161 && strLongTerm == "N"){
			//document.getElementById("retDt").disabled = false;
			document.all.longTerm.disabled = false;
		}else if(status == 107161 && strLongTerm == "Y"){
			//document.getElementById("retDt").disabled = false;
			document.all.longTerm.disabled = false;
		}else if(status == 107162 || status == 107163){
			document.all.longTerm.disabled = true;
		}
				
	}
	if(status == 107162 || status == 107163){
		document.getElementById("retDt").disabled = true;
		$("#RetDt *").attr("disabled", "disabled").off('click');
	}
	//var longTerm =  document.frmCaseSchedularForm.longTerm.value;
	if(strLongTerm == "Y"){		
		document.all.longTerm.checked = true;
		document.all.longTerm.title="on";
	}else if(strLongTerm == "N"){		
		document.all.longTerm.checked = false;	
		document.all.longTerm.title="off";
	}
	else{		
		document.frmCaseSchedularForm.longTerm.value="off";
	}
}

function fnOnLoad(){
	if(status == 107162 || status == 107163 || status == 107161){
		document.all.searchkitID.disabled = true;
	}
}

function fnOnloadpage(){
	var myTabbar = new dhtmlXTabBar("my_tabbar");
	myTabbar.setSkin("winscarf");
	myTabbar.setImagePath("/extweb/dhtmlx/dhtmlxTabbar/imgs/"); 
	myTabbar.addTab("caseDetails",message_sales[407]);
	myTabbar.addTab("inventorySelection",message_sales[408]);
	myTabbar.addTab("notes",message_sales[409]);
	myTabbar.attachEvent("onSelect", function(id,last_id){	
		global_param = "&companyInfo="+strCompanyInfo;
		if(id=='caseDetails'){
			document.getElementById("my_tabbar").style.height = "430px";  
			href="/gmCaseSchedular.do?strOpt=caseDetails&caseId="+caseId.innerHTML+global_param;
		} else if(id=='inventorySelection'){
			document.getElementById("my_tabbar").style.height = "500px";
			href="/gmCaseSchedular.do?strOpt=invSelection&caseId="+caseId.innerHTML+global_param;	
		} else if(id=='notes'){
		 	document.getElementById("my_tabbar").style.height = "450px";
		  	href="/gmCaseSchedular.do?strOpt=notes&caseId="+caseId.innerHTML+global_param;	   
	    } 
		myTabbar.cells(id).attachURL(href); //Loading the contents in tab	
	    return true;
	 });
	myTabbar.setTabActive("caseDetails");	
}

function fnNoteReset(frm){
	document.getElementById("comments").value='';
}

function fnDateFmtOnBlurCheck(objDt,format) {
	   var val = objDt.value;
		var validformat='';
		var splitby='';
	      var yy;
	    var full_date=new Array();
	    //If format value is empty or null, it will set the default format as 'MM/dd/yyyy'
	    format=(format==null || format=='')?'MM/dd/yyyy':format;
	    if(val==''|| val==null)
	    	{
	    		return null;
	    	}
	    else{
	    	val= TRIM(objDt.value);
	    	
	    }
	    // This below expression for accepting 6 or 8 digit number or 2digits month-day/2digits month-day/2 or 4 digits year
	    // var regexp = /^((\d{6}$|\d{8}$)|(\d{2}\/\d{2}\/(\d{2}|\d{4})$))/;
	    // d{6}- 010114
	    // d{8}- 01012014
	    // d{2}\/\d{2}\/(\d{2}- 01/01/14
	    // d{2}\/\d{2}\/(\d{4}- 01/01/2014
		switch(format){
		case 'MM/dd/yyyy': 
			validformat=/^((\d{6}$|\d{8}$)|(\d{2}\/\d{2}\/(\d{2}|\d{4})$))/; 
			splitby="/";
			break;
		case 'dd.MM.yyyy': 
			validformat=/^((\d{6}$|\d{8}$)|(\d{2}\.\d{2}\.(\d{2}|\d{4})$))/; 
			splitby=".";
			break;
		case 'dd/MM/yyyy': 
			validformat=/^((\d{6}$|\d{8}$)|(\d{2}\/\d{2}\/(\d{2}|\d{4})$))/; 
			splitby="/";
			break;
		}
	    if(val.match(validformat)){ // Here Code checked the mentioned date formats above.

		    if (val.indexOf(splitby) != -1) {// If date comes with any symbol / or .
		        full_date = val.split(splitby);
		    } else {
		    	full_date[0] = val.substring(0, 2);
		    	full_date[1]= val.substring(2, 4);
		    	full_date[2]= val.substring(4);
	          }
	        yy = full_date[2];
	        yy = yy.length == 2 ? '20' + yy : yy;//If year 2 digit 
	        full_date[2] = yy;
	        objDt.value = full_date[0] + splitby + full_date[1] + splitby + full_date[2];
	   return objDt.value;
	    }else{
	    	Error_Details(message[10529]);
	    	Error_Show();
			Error_Clear();
			return false;	    	
	    }
	}

function fnLoadRef(){
	var field_sales = document.frmCaseSchedularForm.distributor.value;
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmCaseSchedular.do?strOpt=checkSalesRep&DisId='+field_sales+'&ramdomId=' +Math.random());
	dhtmlxAjax.get(ajaxUrl,function(loader){
		var response = loader.xmlDoc.responseText;	
		var mySplitResult = response.split("^");
		 if(response!=null && response!=''){ 
	   			salesrepName = mySplitResult[0]; 
	   			salesrepId = mySplitResult[1]; 
		 }
	document.frmCaseSchedularForm.searchsalesrep.value=salesrepName;
	document.frmCaseSchedularForm.salesrep.value = salesrepId;	
	document.frmCaseSchedularForm.distributor.value = field_sales;
	});	
}

function fnSetShipTo(){
	var loanobjval = document.all.distributor.value; 
	var obj = document.all.salesrep; 
	fnSetRepValues(obj);
	var accobj = document.all.account;
	fnSetAccValues(accobj);		
}
function fnSetRepValues(obj){
	var loanobjval = document.all.distributor.value; 
	var repID = '';
	var repName = '';
	obj.disabled = false;
	obj.options.length = 0;
	obj.options[0] = new Option("[Choose One]","0");
	j=0;
	for (var i=0;i<RepLen;i++)
	{
		/*
		 * First we are getting Rep Array ,it consists of Rep id,Rep Name,party id and Distributor Id .
		 */
		arr = eval("RepArr"+i);
		arrobj = arr.split(",");
		id = arrobj[0];	//Rep id	
		name = arrobj[1];//Rep name
		did = arrobj[3];//Distributor Id
		/*
		 * We are in need to display Sales Rep based on the Distributor ,here we are filtering the sales Rep.
		 */
			if (did == loanobjval)
			{
				j++;			
				obj.options[j] = new Option(name,id);
				if(j == 1){
					repID = id;
					repName = name;				
				}
			}			
	}	
	if((j == 1)){
		obj.value=repID;
	}	
}
function fnSetAccValues(obj){
		j = 0;
		var loanobjval = document.all.distributor.value;
		obj.disabled = false;
		obj.options.length = 0;
		obj.options[0] = new Option("[Choose One]","0");
		for (var i=0;i<AccLen;i++)
		{
			arr = eval("AccArr"+i);
			arrobj = arr.split(",");
			id = arrobj[0];
			name = arrobj[1];
			did = arrobj[2];
			if (did == loanobjval)
				{
					j++;
					obj.options[j] = new Option(name,id);
				}		
		}
}

function showSingleCalendar(calendardiv,textboxref,dateformat){

	onSetStyleToCalendar(calendardiv);
	var topObj = getObjTop();
	dateformat = topObj.gCompJSDateFmt;
	if(gCmpDateFmt == 'dd/MM/yyyy'){
		dateformat = '%d/%m/%Y';
	}
	var jsdatefmt=null;
	 try
		{
		jsdatefmt=self.opener.top.jsdatefmt;
		}catch(err) {
			jsdatefmt=null;
		}
	if (dateformat == null){
		if( jsdatefmt != null && jsdatefmt != '' && jsdatefmt != undefined){
			    dateformat=jsdatefmt;
			}else{
				dateformat='%m/%d/%Y';	}	
	}
	
	if(!calendars[calendardiv]) 
		calendars[calendardiv]= initCal(calendardiv,textboxref,dateformat);
	
	//This line for today date will display in BOLD and this method will be in dhtmlxcalendar.js
	calendars[calendardiv].enableTodayHighlight(true); // or false
	
	if(document.getElementById(textboxref).value.length>0){
		calendars[calendardiv].setDate(document.getElementById(textboxref).value);
	}

	if(calendars[calendardiv].isVisible())
		calendars[calendardiv].hide();
	else 
		calendars[calendardiv].show();
}