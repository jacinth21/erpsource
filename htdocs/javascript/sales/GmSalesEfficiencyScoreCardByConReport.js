// javascript\sales\GmSalesEfficiencyScoreCardByConReport.js

//This function is onPageLoad used to Ajax call used fetch 
function fnOnPageLoad(){
	// to get the drill down values
	var systemIdVal = document.frmSalesEfficiencyScoreCardByCon.systemId.value;
	var distIdVal = document.frmSalesEfficiencyScoreCardByCon.distributorId.value;
	var adIdVal = document.frmSalesEfficiencyScoreCardByCon.adId.value;
	
	// to pass the drill down values
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmSalesEfficiencyScoreCardByCon.do?method=fetchConUsageScoreCardDtls&systemId='
			+ systemIdVal
			+ '&distributorId='
			+ distIdVal
			+ '&adId='
			+ adIdVal
			+ '&ramdomId=' + Math.random());
	
	var loader = dhtmlxAjax.getSync(ajaxUrl);
	fnLoadScoreCardByConsCallback(loader);
}

//This function used to load the SP details to grid.
function fnLoadScoreCardByConsCallback(loader) {
	response = loader.xmlDoc.responseText;
	if (response !=''){
	
		var setInitWidths = "100,100,70,150,70,85,*,120,80,60,60,40,40,70,70,70,70";
		var setColAlign = "left,left,left,left,left,left,left,left,center,center,right,right,right,right,right,right";
		var setColTypes = "ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ron,ron,ron,ron,ron,ron";
		var setColSorting = "str,str,str,str,str,str,str,str,date,date,int,int,int,int,int,int";

		var setHeader = [ "Ad Name", "Field Sales", "Tag Id", 
				"System", "Type", "Set Id", "Set Name", "Consignment Id",
				"Placement Date", "Return Date", "Days Kept",
				"Last 2 Mo. Days Kept", "Proj days kept", "Cons Compl",
				"Last 2 Mon. Cmpl", "Cons Tot Proj" ];

		var setFilter = [ "#select_filter", "#select_filter",
				"#text_filter", "#select_filter",
				"#select_filter", "#text_filter", "#select_filter",
				"#text_filter", "#text_filter", "#text_filter",
				"#numeric_filter", "#numeric_filter", "#numeric_filter",
				"#numeric_filter", "#numeric_filter", "#numeric_filter" ];

		var setColIds = "ad_name,dist_name,tag_id,system_name,system_type_name,set_id,set_name,cn_id,placement_date,return_date,"
				+ "days_kept,last_2_d_kept,proj_d_kept,cn_cmpl,last_2_cn_cmpl,cn_tot_proj";

		var enableTooltips = "true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true";
		var gridHeight = "600";
		
		// to set auto height
		if($(document).height() !=undefined || $(document).height() !=null){
			gridHeight =  $(document).height() - 120 ;
		}
		
		// showing footer total
		var footerArry=new Array();	
		footerArry[0]="";
		footerArry[1]="";
		footerArry[2]="";
		footerArry[3]="";
		footerArry[4]="";
		footerArry[5]="";
		footerArry[6]="";
		footerArry[7]="";
		footerArry[8]="";
		footerArry[9]="";
		footerArry[10]="";
		footerArry[11]="";
		footerArry[12]="<b>Total<b>";
		footerArry[13]="<b>{#stat_total}<b>";
		footerArry[14]="<b>{#stat_total}<b>";
		footerArry[15]="<b>{#stat_total}<b>";
		
		var format = ""
		var dataHost = {};
		var footerStyles = [];
		document.getElementById("DivNothingMessage").style.display = 'none';

		// split the functions to avoid multiple parameters passing in single
		// function
		gridObj = initGridObject('dataGridDiv');
		// Showing JS error - to comment the fast operation
		//gridObj.startFastOperations();
		
		// system taking long time to load - comment the code now
		//gridObj.enableDistributedParsing(true);
		
		gridObj = fnCustomTypes(gridObj, setHeader, setColIds, setInitWidths,
				setColAlign, setColTypes, setColSorting, enableTooltips,
				setFilter, gridHeight, format,footerArry,footerStyles);
		
		gridObj.setNumberFormat("0,000",10);
		gridObj.setNumberFormat("0,000",11);
		gridObj.setNumberFormat("0,000",12);
		
		// to set the decimal format
		gridObj.setNumberFormat("0,000.0",13);
		gridObj.setNumberFormat("0,000.0",14);
		gridObj.setNumberFormat("0,000.0",15);
		
		//PC-3528 to enable distribute parsing
		gridObj.enableDistributedParsing(true);
		
		gridObj = loadDhtmlxGridByJsonData(gridObj, response);
		//gridObj.stopFastOperations();
		
		// freezes 3 columns
		//gridObj.splitAt(3);
		
	} else {
		document.getElementById("dataGridDiv").style.display = 'none';
		document.getElementById("DivNothingMessage").style.display = 'block';
		document.getElementById("DivExportExcel").style.display = 'none';
	}
}


//This Function Used to set custom types conditions in Grid
function fnCustomTypes(gObj, setHeader, setColIds, setInitWidths, setColAlign,
		setColTypes, setColSorting, enableTooltips, setFilter, gridHeight,
		format,footerArry,footerStyles) {
		gObj.setHeader(setHeader);
		var colCount = setHeader.length;
	
		gObj.setInitWidths(setInitWidths);
		gObj.setColAlign(setColAlign);
		gObj.setColTypes(setColTypes);
		gObj.setColSorting(setColSorting);
		gObj.enableTooltips(enableTooltips);
		gObj.attachHeader(setFilter);
		gObj.setColumnIds(setColIds);
		gObj.enableAutoHeight(true, gridHeight, true);
		
		if (footerArry != undefined && footerArry.length > 0) {
			var footstr = eval(footerArry).join(",");
			if (footerStyles != undefined && footerStyles.length > 0)
			gObj.attachFooter(footstr, footerStyles);
			else
			gObj.attachFooter(footstr);
			}

	return gObj;
}

//This function used to download the score details to excel file
function fnExportDtls() {
	gridObj.toExcel('/phpapp/excel/generate.php');
}

