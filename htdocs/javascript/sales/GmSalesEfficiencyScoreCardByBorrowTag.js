//javascript\sales\GmSalesEfficiencyScoreCardByBorrowTag.js

//PC-2479 Scorecard - Borrowed Tag Information
function fnBorrowTagOnPageLoad(){
// to get the drill down values
	var systemIdVal = document.frmActualProceduresReportScoreCard.systemId.value;
	var distIdVal = document.frmActualProceduresReportScoreCard.distributorId.value;
	var adIdVal = document.frmActualProceduresReportScoreCard.adId.value;
	
	// Added allowed tag field according to PC-5101: Scorecard DO classification changes
	var allowedTag = document.frmActualProceduresReportScoreCard.allowedTag.value;
	
	var ajaxUrl = '';
		ajaxUrl = fnAjaxURLAppendCmpInfo('/gmScoreCardActualProceduresReport.do?method=fetchBorrowTagScoreCardDtls&systemId='
				+ systemIdVal
				+ '&distributorId='
				+ distIdVal
				+ '&adId='
				+ adIdVal
				+ '&allowedTag=' 
				+ allowedTag
				+ '&ramdomId=' 
				+ Math.random());
	
	var loader = dhtmlxAjax.getSync(ajaxUrl);
	fnLoadBorrowTagCallback(loader);
}

//PC-2479 Scorecard - Borrowed Tag Information
//This function used to load the BT details to grid.
function fnLoadBorrowTagCallback(loader) {
	response = loader.xmlDoc.responseText;
	
	if (response !=''){
		// Added allowed tag field according to PC-5101: Scorecard DO classification changes
		var setInitWidths = "100,120,125,250,100,100,125,125,100";
		var setColAlign = "left,left,left,left,left,left,left,left,left";
		var setColTypes = "ro,ro,ro,ro,ro,ro,ro,ro,ro";
		var setColSorting = "str,str,str,str,str,str,str,str,str";
		var setHeader = [ "AD", "Tag ID", "Set ID", "Set Name", "Order ID", "Order Date", "Order Owner", "Tag Owner", "Allowed Tag"];
		var setFilter = [ "#select_filter", "#text_filter", "#text_filter", "#select_filter", "#text_filter", "#text_filter", "#select_filter", "#select_filter", "#select_filter"];
		
		var setColIds = "ad_name,tag_id,set_id,set_name,order_id,order_date,order_owner,tag_owner,allowed_tag";
		var enableTooltips = "true,true,true,true,true,true,true,true,true";
		var gridHeight = "600";
		var format = ""
		var dataHost = {};
		var footerArry=new Array();

		//To set auto height
		if($(document).height() != undefined || $(document).height() != null){
			gridHeight = $(document).height() - 120;	
		}
			
		var footerStyles = [];
		var footerExportFL = true;
		
		document.getElementById("DivNothingMessage").style.display = 'none';

		// split the functions to avoid multiple parameters passing in single function
		gridObj = initGridObject('dataGridDiv');
		gridObj = fnCustomTypes(gridObj, setHeader, setColIds, setInitWidths,
				setColAlign, setColTypes, setColSorting, enableTooltips,
				setFilter, gridHeight, format, footerStyles);
		
		//PC-3528 to enable distribute parsing
		gridObj.enableDistributedParsing(true);
		
		gridObj = loadDhtmlxGridByJsonData(gridObj, response);
		
		
	} else {
		document.getElementById("dataGridDiv").style.display = 'none';
		document.getElementById("DivNothingMessage").style.display = 'block';
		document.getElementById("DivExportExcel").style.display = 'none';
	}
}

//This Function Used to set custom types conditions in Grid
function fnCustomTypes(gObj, setHeader, setColIds, setInitWidths, setColAlign,
		setColTypes, setColSorting, enableTooltips, setFilter, gridHeight,
		format, footerArry, footerStyles) {
		gObj.setHeader(setHeader);
		var colCount = setHeader.length;
	
		gObj.setInitWidths(setInitWidths);
		gObj.setColAlign(setColAlign);
		gObj.setColTypes(setColTypes);
		gObj.setColSorting(setColSorting);
		gObj.enableTooltips(enableTooltips);
		gObj.attachHeader(setFilter);
		gObj.setColumnIds(setColIds);
		if (footerArry != undefined && footerArry.length > 0) {
		var footstr = eval(footerArry).join(",");
			if (footerStyles != undefined && footerStyles.length > 0)
				gObj.attachFooter(footstr, footerStyles);
			else
				gObj.attachFooter(footstr);
	}  
		gObj.enableAutoHeight(true, gridHeight, true);

		return gObj;
}

//This function used to download the score details to excel file
function fnExportDtls() {
	gridObj.toExcel('/phpapp/excel/generate.php');
}
