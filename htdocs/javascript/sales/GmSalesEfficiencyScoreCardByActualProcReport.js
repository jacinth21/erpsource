// javascript\sales\GmSalesEfficiencyScoreCardByActualProcReport.js

//This function is onPageLoad used to Ajax call used fetch Actual Procedure Screen Name
function fnOnPageLoad(){
	// to get the drill down values
	var systemIdVal = document.frmActualProceduresReportScoreCard.systemId.value;
	var distIdVal = document.frmActualProceduresReportScoreCard.distributorId.value;
	var adIdVal = document.frmActualProceduresReportScoreCard.adId.value;
	
	// Added hAllowedTag hidden field according to PC-5101: Scorecard DO classification changes
	var hAllowedTag = document.frmActualProceduresReportScoreCard.hAllowedTag.value;
	
	if(hAllowedTag){
		if(hAllowedTag == "0") {
			console.log("0");
			document.getElementById("allowedTag").selectedIndex = 0;	
		}
		else if(hAllowedTag == "Y") {
			console.log("Y");
			document.getElementById("allowedTag").selectedIndex = 1;	
		}
		else {
			console.log("N");
			document.getElementById("allowedTag").selectedIndex = 2;	
		}
	}
	// Added Allowed tag field according to PC-5101: Scorecard DO classification changes
	var allowedTag = document.getElementById("allowedTag").value;
	document.frmActualProceduresReportScoreCard.hAllowedTag.value = "";
	
	var ajaxUrl = '';
		ajaxUrl = fnAjaxURLAppendCmpInfo('/gmScoreCardActualProceduresReport.do?method=fetchActualProcUsageScoreCardDtls&systemId='
				+ systemIdVal
				+ '&distributorId='
				+ distIdVal
				+ '&adId='
				+ adIdVal
				+ '&allowedTag=' 
				+ allowedTag
				+ '&ramdomId=' 
				+ Math.random());
	
	var loader = dhtmlxAjax.getSync(ajaxUrl);
	fnLoadScoreCardProcCallback(loader);
}

//This function used to load the SP details to grid.
function fnLoadScoreCardProcCallback(loader) {
	response = loader.xmlDoc.responseText;
	
	if (response !=''){
		// Added allowed tag field according to PC-5101: Scorecard DO classification changes
		var setInitWidths = "130,0,120,*,120,100,85,120,120,80,75,75,75";
		var setColAlign = "left,left,left,left,left,left,left,left,left,left,right,right,left";
		var setColTypes = "ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ron,ron,ro";
		var setColSorting = "str,str,str,str,str,str,str,str,str,str,int,int,str";
		var setHeader = [ "AD", "System", "Set ID", "Set Name", "Order ID", "Tag ID", "Order Date", "Order Owner", "Credit Goes To", "Type", "Proc Cmpl",  "Proc Total Proj", "Allowed Tag"];
		var setFilter = [ "#select_filter", "#select_filter", "#text_filter", "#select_filter", "#text_filter","#text_filter", "#text_filter", "#select_filter", "#select_filter", "#select_filter", "#numeric_filter", "#numeric_filter","#select_filter"];
		
		var setColIds = "ad_name,system_name,set_id,set_name,order_id,tag_id,order_date,order_owner,credit_owner,type,proc_cmpl,proc_tot_proj,allowed_tag";
		var enableTooltips = "true,true,true,true,true,true,true,true,true,true,true,true,true";
		var gridHeight = "600";
		var format = ""
		var dataHost = {};

		//To set auto height
		if($(document).height() != undefined || $(document).height() != null){
			gridHeight = $(document).height() - 120;	
		}
		
		var footerArry=new Array();	
		footerArry[0]="";
		footerArry[1]="";
		footerArry[2]="";
		footerArry[3]="";
		footerArry[4]="";
		footerArry[5]="";
		footerArry[6]="";
		footerArry[7]="";
		footerArry[8]="";
		footerArry[9]="<b>Total<b>";
		footerArry[10]="<b>{#stat_total}<b>";
		footerArry[11]="<b>{#stat_total}<b>";

		
		
		var footerStyles = [];
		var footerExportFL = true;
		
		document.getElementById("DivNothingMessage").style.display = 'none';

		// split the functions to avoid multiple parameters passing in single
		// function
		gridObj = initGridObject('dataGridDiv');
		gridObj = fnCustomTypes(gridObj, setHeader, setColIds, setInitWidths,
				setColAlign, setColTypes, setColSorting, enableTooltips,
				setFilter, gridHeight, format, footerArry, footerStyles);

		gridObj.setNumberFormat("0,000.0",10);
		gridObj.setNumberFormat("0,000.0",11);
		
		//PC-3528 to enable distribute parsing
		gridObj.enableDistributedParsing(true);
		
		gridObj = loadDhtmlxGridByJsonData(gridObj, response);
		
		// to hide the system column
		gridObj.setColumnHidden(1, true);
		
	} else {
		document.getElementById("dataGridDiv").style.display = 'none';
		document.getElementById("DivNothingMessage").style.display = 'block';
		document.getElementById("DivExportExcel").style.display = 'none';
	}
}


//This Function Used to set custom types conditions in Grid
function fnCustomTypes(gObj, setHeader, setColIds, setInitWidths, setColAlign,
		setColTypes, setColSorting, enableTooltips, setFilter, gridHeight,
		format, footerArry, footerStyles) {
		gObj.setHeader(setHeader);
		var colCount = setHeader.length;
	
		gObj.setInitWidths(setInitWidths);
		gObj.setColAlign(setColAlign);
		gObj.setColTypes(setColTypes);
		gObj.setColSorting(setColSorting);
		gObj.enableTooltips(enableTooltips);
		gObj.attachHeader(setFilter);
		gObj.setColumnIds(setColIds);
		if (footerArry != undefined && footerArry.length > 0) {
		var footstr = eval(footerArry).join(",");
			if (footerStyles != undefined && footerStyles.length > 0)
				gObj.attachFooter(footstr, footerStyles);
			else
				gObj.attachFooter(footstr);
	}  
		gObj.enableAutoHeight(true, gridHeight, true);

		return gObj;
}

//This function used to download the score details to excel file
function fnExportDtls() {
	gridObj.toExcel('/phpapp/excel/generate.php');
}

//PC-2479 Scorecard - Borrowed Tag Information
function fnViewBorrow(){
	// to get the drill down values
	var systemIdVal = document.frmActualProceduresReportScoreCard.systemId.value;
	var distIdVal = document.frmActualProceduresReportScoreCard.distributorId.value;
	var adIdVal = document.frmActualProceduresReportScoreCard.adId.value;
	// Added allowed tag field according to PC-5101: Scorecard DO classification changes
	var allowedTag = document.getElementById("allowedTag").value;
	
	// Added allowed tag field according to PC-5101: Scorecard DO classification changes
	windowOpener("/gmScoreCardActualProceduresReport.do?method=loadBorrowTagScoreCardDtls&systemId="+systemIdVal+"&distributorId="+distIdVal+"&adId="+adIdVal+'&allowedTag='+allowedTag
			,"Search","resizable=yes,scrollbars=yes,top=250,left=300,width=1100,height=600");
}

//PC-3676: Bulk DO changes
function fnViewBulkDO (){

// to get the drill down values
	var systemIdVal = document.frmActualProceduresReportScoreCard.systemId.value;
	var distIdVal = document.frmActualProceduresReportScoreCard.distributorId.value;
	var adIdVal = document.frmActualProceduresReportScoreCard.adId.value;
	// Added allowed tag field according to PC-5101: Scorecard DO classification changes
	var allowedTag = document.getElementById("allowedTag").value;
	
	windowOpener("/gmScoreCardActualProceduresReport.do?method=loadBulkDOScoreCardDtls&systemId="+systemIdVal+"&distributorId="+distIdVal+"&adId="+adIdVal+'&allowedTag='+allowedTag
			,"Search","resizable=yes,scrollbars=yes,top=250,left=300,width=1100,height=600");
}


function fnChangeAllowedTags() {
	fnOnPageLoad();
}


