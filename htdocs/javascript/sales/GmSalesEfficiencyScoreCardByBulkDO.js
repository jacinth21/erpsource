//javascript\sales\GmSalesEfficiencyScoreCardByBorrowTag.js

//PC-3676 Scorecard - Bulk DO 
function fnBulkDOOnPageLoad(){
// to get the drill down values
	var systemIdVal = document.frmActualProceduresReportScoreCard.systemId.value;
	var distIdVal = document.frmActualProceduresReportScoreCard.distributorId.value;
	var adIdVal = document.frmActualProceduresReportScoreCard.adId.value;
	
	// Added allowed tag field according to PC-5101: Scorecard DO classification changes
	var allowedTag = document.frmActualProceduresReportScoreCard.allowedTag.value;
	
	var ajaxUrl = '';
		ajaxUrl = fnAjaxURLAppendCmpInfo('/gmScoreCardActualProceduresReport.do?method=fetchBulkDOScoreCardDtls&systemId='
				+ systemIdVal
				+ '&distributorId='
				+ distIdVal
				+ '&adId='
				+ adIdVal
				+ '&allowedTag=' 
				+ allowedTag
				+ '&ramdomId=' 
				+ Math.random());
	
	var loader = dhtmlxAjax.getSync(ajaxUrl);
	fnLoadBulkDOCallback(loader);
}


//This function used to load the bulk DO details to grid.
function fnLoadBulkDOCallback(loader) {
	response = loader.xmlDoc.responseText;
	
	if (response !=''){
		// Added allowed tag field according to PC-5101: Scorecard DO classification changes
		var setInitWidths = "150,*,100,150,150,100,100,100";
		var setColAlign = "left,left,left,left,left,right,right,left";
		var setColTypes = "ro,ro,ro,ro,ro,ron,ron,ro";
		var setColSorting = "str,str,date,str,str,int,int,str";
		var setHeader = [ "AD", "System", "Month", "Order Owner", "Credit Goes To", "Proc Cmpl", "Proc Tot Proj", "Allowed Tag"];
		var setFilter = [ "#text_filter", "#text_filter", "#select_filter", "#select_filter", "#select_filter", "#numeric_filter", "#numeric_filter", "#select_filter"];
		
		var setColIds = "ad_name,system_name,bulk_do_mon,order_owner,tag_owner,proc_name,proc_tot_proj,allowed_tag";
		var enableTooltips = "true,true,true,true,true,true,true,true,true";
		var gridHeight = "600";
		var format = ""
		var dataHost = {};
		
		// showing footer total 
		var footerArry=new Array();	
		footerArry[0]="";
		footerArry[1]="";
		footerArry[2]="";
		footerArry[3]="";
		footerArry[4]="<b>Total<b>";
		footerArry[5]="<b>{#stat_total}<b>";
		footerArry[6]="<b>{#stat_total}<b>";		

		//To set auto height
		if($(document).height() != undefined || $(document).height() != null){
			gridHeight = $(document).height() - 120;	
		}
			
		var footerStyles = [];
		var footerExportFL = true;
		
		document.getElementById("DivNothingMessage").style.display = 'none';

		
		// split the functions to avoid multiple parameters passing in single function
		gridObj = initGridObject('dataGridDiv');
		gridObj = fnCustomTypes(gridObj, setHeader, setColIds, setInitWidths,
				setColAlign, setColTypes, setColSorting, enableTooltips,
				setFilter, gridHeight, format, footerStyles);

		// footer added
		gridObj.attachFooter(footerArry, footerStyles);
		
		// number format
		gridObj.setNumberFormat("0,000.0",5);
		gridObj.setNumberFormat("0,000.0",6);
				
				
		//PC-3528 to enable distribute parsing
		gridObj.enableDistributedParsing(true);
		
		gridObj = loadDhtmlxGridByJsonData(gridObj, response);
		
		
	} else {
		document.getElementById("dataGridDiv").style.display = 'none';
		document.getElementById("DivNothingMessage").style.display = 'block';
		document.getElementById("DivExportExcel").style.display = 'none';
	}
}

//This Function Used to set custom types conditions in Grid
function fnCustomTypes(gObj, setHeader, setColIds, setInitWidths, setColAlign,
		setColTypes, setColSorting, enableTooltips, setFilter, gridHeight,
		format, footerArry, footerStyles) {
		gObj.setHeader(setHeader);
		var colCount = setHeader.length;
	
		gObj.setInitWidths(setInitWidths);
		gObj.setColAlign(setColAlign);
		gObj.setColTypes(setColTypes);
		gObj.setColSorting(setColSorting);
		gObj.enableTooltips(enableTooltips);
		gObj.attachHeader(setFilter);
		gObj.setColumnIds(setColIds);
		if (footerArry != undefined && footerArry.length > 0) {
		var footstr = eval(footerArry).join(",");
			if (footerStyles != undefined && footerStyles.length > 0)
				gObj.attachFooter(footstr, footerStyles);
			else
				gObj.attachFooter(footstr);
	}  
		gObj.enableAutoHeight(true, gridHeight, true);

		return gObj;
}

//This function used to download the score details to excel file
function fnExportDtls() {
	gridObj.toExcel('/phpapp/excel/generate.php');
}
