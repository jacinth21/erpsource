//The Below Function is added for PMT-5662 The Below Function will return all the values in the company drop down.
//For Now the array will have code id of 100800, 100801
var selAllChk='';
function getCompArr(){
	var arrCompOpt = new Array();
	var arrComp = new Array();
	arrCompOpt = frmObj.Comp.options;
	for(i=0,j=0;i<arrCompOpt.length;i++){
		if(arrCompOpt[i].value != '100803'){//Code Id For All
			arrComp[j++] = arrCompOpt[i].value;
		}
	}
	
	return arrComp;
}


//Called to load zone list dynamically based on the selected regions in the Regions list
function fnLoadDiv(arrChk)
{
	var j = 0;
	var k = 0;
	var arr = frmObj.Comp;
	var arrChk = new Array();
	var arrDiv = new Array();
	var arrval = arr.value;
	
	if (arrval>-1)
	{
		arrChk[0] = arrval;
		j++;
	}
	if(arrval == '100803'){//Code Id for All
		arrChk  = getCompArr();
		
	}
	
	if (j >0)
	{
		document.all.Div_Div.innerHTML = '';
		var newhtml = '';
		var arrChklen = arrChk.length;
		var arrdivlen = DivArr.length;
		var chkdiv = '';
		for (var i=0;i<arrChklen ;i++ )
		{
			chkdiv = arrChk[i];
			for (var j=0;j<arrdivlen ;j++ )
			{		
				if (DivArr[j][2] == chkdiv)
				{
					if (newhtml.indexOf("id='"+DivArr[j][0]+"'") < 0)
					{
						
						if(arrval == '100803'){//Code Id for All.
					     newhtml = newhtml + fnCreateChkBox('Div',DivArr[j][0],DivArr[j][1],DivArr[j][2],'fnLoadZone(this)');
						}else{
							newhtml = newhtml + fnCreateChkBox('Div',DivArr[j][0],DivArr[j][1]+'-'+DivArr[j][3],DivArr[j][2],'fnLoadZone(this)');
						}
					arrDiv[k] = DivArr[j][0];
					k++;
					}
				}
			}
		}
		
		document.all.Div_Zone.innerHTML = newhtml;
		if (newhtml =='')
		{
			document.all.Div_Div.innerHTML = '<span class=RightTextRed>No Division</span>';
			document.all.Div_Zone.innerHTML = '<span class=RightTextRed>No Zone</span>';
			document.all.Div_Regn.innerHTML = '<span class=RightTextRed>No Region</span>';
			
			
			
		}else
		{
			document.all.Div_Div.innerHTML = newhtml;
			fnLoadZone(arrDiv);
			
		}

	}
	else
	{
		document.all.Div_Div.innerHTML = frmObj.hDivInnerHtml.value;
		document.all.Div_Zone.innerHTML = frmObj.hZoneInnerHtml.value;
		document.all.Div_Regn.innerHTML = frmObj.hRegnInnerHtml.value;
				
	}
	
	if(frmObj.Chk_Grp_ALL_Div.checked == true){
		frmObj.Chk_Grp_ALL_Div.checked = false;
	}
	if(frmObj.Chk_Grp_ALL_Zone.checked == true){
		frmObj.Chk_Grp_ALL_Zone.checked = false;
	}
	if(frmObj.Chk_Grp_ALL_Regn.checked == true){
		frmObj.Chk_Grp_ALL_Regn.checked = false;
	}
	if(frmObj.Chk_Grp_ALL_Dist.checked == true){
		frmObj.Chk_Grp_ALL_Dist.checked = false;
	}	
}

//Called to load zone list dynamically based on the selected regions in the Regions list
function fnLoadZone(arrChk)
{
	var j = 0;
	var k = 0;
	var arr = frmObj.Chk_GrpDiv;
	var arrChk = new Array();
	var arrComp = new Array();
	var arrZone = new Array();
	var arrlen = arr.length;
	
	if(frmObj.Comp == undefined || frmObj.Comp == null)
	{
		vcompanay = comp;
	}
	else
	{
		vcompanay = frmObj.Comp.value;
	}
	
	if(arrlen == undefined && arr.checked == true)
	{
		arrChk[j] = arr.id;
		j++;
	}
	else
	{
		for (var i=0;i< arrlen;i++ )
		{
			if (arr[i].checked == true)
			{
				arrChk[j] = arr[i].id;
				j++;
			}
		}
	}
	if (j >0 || vcompanay != null)
	{
		document.all.Div_Zone.innerHTML = '';
		var newhtml = '';
		var arrChklen = arrChk.length;
		var arrzonelen = ZAr.length;
		var chkzone = '';
		var chkcomp = '';
		if (arrChklen > 0)
		{
			for (var i=0;i<arrChklen ;i++ )
			{
				chkzone = arrChk[i];
				for (var j=0;j<arrzonelen ;j++)
				{		
					if (ZAr[j][2] == chkzone && (((ZAr[j][3] == vcompanay) && vcompanay != '100803')|| vcompanay == '100803'))//Code Id For All
					{
						if (newhtml.indexOf(ZAr[j][0]) < 0)
						{
						newhtml = newhtml + fnCreateChkBox('Zone',ZAr[j][0],ZAr[j][1],ZAr[j][2],'fnLoadRegn(this)');
						arrZone[k] = ZAr[j][0];
						k++;
						}
					}
				}
			}
		}
		else if(vcompanay != null)
		{
			for (var j=0;j<arrzonelen ;j++)
			{		
				if ((ZAr[j][3] == vcompanay && vcompanay != '100803') || vcompanay == '100803')//Code Id For All
				{
					if (newhtml.indexOf("id='"+ZAr[j][0]+"'") < 0)
					{
					newhtml = newhtml + fnCreateChkBox('Zone',ZAr[j][0],ZAr[j][1],ZAr[j][2],'fnLoadRegn(this)');
					arrZone[k] = ZAr[j][0];
					k++;
					}
				}
			}
		}
		
		document.all.Div_Zone.innerHTML = newhtml;
		if (newhtml =='')
		{
			document.all.Div_Zone.innerHTML = '<span class=RightTextRed>No Zone</span>';
			document.all.Div_Regn.innerHTML = '<span class=RightTextRed>No Region</span>';
			
			
		}else
		{
			document.all.Div_Zone.innerHTML = newhtml;
			fnLoadRegn(arrZone);
			
		}

	}
	else
	{
		document.all.Div_Zone.innerHTML = frmObj.hZoneInnerHtml.value;
		document.all.Div_Regn.innerHTML = frmObj.hRegnInnerHtml.value;
				
	}
	if(frmObj.Chk_Grp_ALL_Div.checked == true && selAllChk!='Div'){
		frmObj.Chk_Grp_ALL_Div.checked = false;
	}
	if(frmObj.Chk_Grp_ALL_Zone.checked == true){
		frmObj.Chk_Grp_ALL_Zone.checked = false;
	}
	if(fnGetUnCheckedValues(frmObj.Chk_GrpDiv)==''){
		frmObj.Chk_Grp_ALL_Div.checked = true;
	}
}

//Called to load regn list dynamically based on the selected regions in the Regions list
function fnLoadRegn(arrChk)
{
	var j = 0;
	var k = 0;
	var arrRegn = new Array();
	var arrResult = new Array();
	if(frmObj.Comp == undefined || frmObj.Comp == null)
	{
		vcompanay = comp;
	}
	else
	{
		vcompanay = frmObj.Comp.value;
	}
	
	if (arrChk != null && arrChk.length > 0)
	{
		// does nothing ... but dont remove		
	}
	else if( frmObj.Chk_GrpZone != null ) //for VP,AD the ZONE filter is not available
	{
		var arr = frmObj.Chk_GrpZone;
		var arrChk = new Array();
		var arrlen = arr.length;
		if(arrlen == undefined && arr.checked == true)
		{
			arrChk[j] = arr.id;
			j++;
		}
		else
		{
			for (var i=0;i< arrlen;i++ )
			{
				if (arr[i].checked == true)
				{
					arrChk[j] = arr[i].id;
					j++;
				}
			}
		}
		if(j==0 && frmObj.Chk_GrpDiv != null){
			var arrayValues = getArrayOfValues(frmObj.Chk_GrpDiv);
			if(arrayValues.length > 0){
				fnLoadZone(arrayValues);
			}
			return;
		}
	}else{
		/*	
		    If the above 'else if' is not executed, the arrChk will throws error. 
			If we declared arrChk variable at the top of the method then 
			the filter methods are calling cyclically it will throws out of memory error 
		*/
		var arrChk = new Array(); 
	}
	if (j >0 || vcompanay != null)
	{
		document.all.Div_Regn.innerHTML = '';
		var newhtml = '';
		var arrChklen = arrChk.length;
		var arrregnlen = RAr.length;
		var chkzone = '';
		if(arrChklen > 0)
		{
			for (var i=0;i<arrChklen ;i++ )
			{
				chkzone = arrChk[i];
				for (var j=0;j<arrregnlen ;j++ )
				{		
					if (RAr [j][2] == chkzone && (((RAr [j][3] == vcompanay) && vcompanay != '100803')|| vcompanay == '100803'))//Code Id For All
					{
						arrResult[k] = new Array();
						arrResult[k] = RAr[j];
						arrRegn[k] = RAr[j][0];
						k++;
					}
				}
			}
		}
		else if( vcompanay != null ){
			for (var j=0;j<arrregnlen ;j++ )
			{		
				if ((RAr [j][3] == vcompanay && vcompanay != '100803') || vcompanay == '100803')//Code Id For All
				{
					arrResult[k] = new Array();
					arrResult[k] = RAr[j];
					arrRegn[k] = RAr[j][0];
					k++;
				}
			}
		}
		arrResult.sort(sortFunction);
		for (var l=0;l<k ;l++ )
		{
			if (newhtml.indexOf("id='"+arrResult[l][0]+"'") < 0)
			{
			newhtml = newhtml + fnCreateChkBox('Regn',arrResult[l][0],arrResult[l][1],arrResult[l][2],'fnLoadDist(this)');
			}
		}
		document.all.Div_Regn.innerHTML = newhtml;
		if (newhtml =='')
		{
			document.all.Div_Regn.innerHTML = '<span class=RightTextRed>No Region</span>';
			document.all.Div_Dist.innerHTML = '<span class=RightTextRed>No Distributors</span>';
			
			
		}else
		{
			document.all.Div_Regn.innerHTML = newhtml;
			fnLoadDist(arrRegn);
			
		} 

	}
	else
	{
		document.all.Div_Regn.innerHTML = frmObj.hRegnInnerHtml.value;
		document.all.Div_Dist.innerHTML = frmObj.hDistInnerHtml.value;
		
		
	}
	
	if(frmObj.Chk_Grp_ALL_Zone.checked == true && selAllChk!='Zone'){
		frmObj.Chk_Grp_ALL_Zone.checked = false;
	}
	if(frmObj.Chk_Grp_ALL_Regn.checked == true){
		frmObj.Chk_Grp_ALL_Regn.checked = false;
	}
	if(fnGetUnCheckedValues(frmObj.Chk_GrpZone)==''){
		frmObj.Chk_Grp_ALL_Zone.checked = true;
	}
		
}

// Called to load Distributor list dynamically based on the selected regions in the Regions list
function fnLoadDist(arrChk)
{
	var j = 0;
	var k = 0;
	var arrDist = new Array();
	var arrResult = new Array();
	if(frmObj.Comp == undefined || frmObj.Comp == null)
	{
		vcompanay = comp;
	}
	else
	{
		vcompanay = frmObj.Comp.value;
	}
	if (arrChk != null && arrChk.length > 0)
	{
		// does nothing ... but dont remove		
	}
	else if( frmObj.Chk_GrpRegn != null )
	{
		var arr = frmObj.Chk_GrpRegn;
		var arrChk = new Array();
		var arrlen = arr.length;
		if(arrlen == undefined && arr.checked == true)
		{
			arrChk[j] = arr.id;
			j++;
		}
		else
		{
			for (var i=0;i< arrlen;i++ )
			{
				if (arr[i].checked == true)
				{
					arrChk[j] = arr[i].id;
					j++;
				}
			}
		}
		if(j==0 && frmObj.Chk_GrpZone != null ){
			var arrayValues = getArrayOfValues(frmObj.Chk_GrpZone);
			if(arrayValues.length > 0){
				fnLoadRegn(arrayValues);
			}
			if(selAllChk =='Regn'){
				getChkAll(false,document.all.Div_Dist.innerHTML,'Dist',function(){});
			}
			return;
		}
	}else{
		var arrChk = new Array();
	}
	if (j >0 || vcompanay != null)
	{
		document.all.Div_Dist.innerHTML = '';
		var newhtml = '';
		var arrChklen = arrChk.length;
		var arrdistlen = DAr.length;
		var chkregn = '';
		if(arrChklen >0)
		{
			for (var i=0;i<arrChklen ;i++ )
			{
				chkregn = arrChk[i];
				for (var j=0;j<arrdistlen ;j++ )
				{		
					if (DAr [j][2] == chkregn && (((DAr [j][3] == vcompanay) && vcompanay != '100803')|| vcompanay == '100803'))//Code Id For All
					{
						arrResult[k] = new Array();
						arrResult[k] = DAr[j];
						arrDist[k] = DAr[j][0];
						k++;
					}
				}
			}
		}else if (vcompanay != null){
			for (var j=0;j<arrdistlen ;j++ )
			{		
				if ((DAr [j][3] == vcompanay && vcompanay != '100803') || vcompanay == '100803')//Code Id For All
				{
					arrResult[k] = new Array();
					arrResult[k] = DAr[j];
					arrDist[k] = DAr[j][0];
					k++;
				}
			}
		}
		arrResult.sort(sortFunction);
		for (var l=0;l<k ;l++ )
		{
			if (newhtml.indexOf("id='"+arrResult[l][0]+"'") < 0)
			{			
			newhtml = newhtml + fnCreateChkBox('Dist',arrResult[l][0],arrResult[l][1],arrResult[l][2],'fnLoadTerr(this)');
			}
		}
		document.all.Div_Dist.innerHTML = newhtml;
		if (newhtml =='')
		{
			document.all.Div_Dist.innerHTML = '<span class=RightTextRed>No Distributors</span>';
			
		}else
		{
			document.all.Div_Dist.innerHTML = newhtml;
			if(document.all.Div_Terr != null){
				fnLoadTerr(arrDist);
			}
		}

	}
	else
	{
		
		document.all.Div_Dist.innerHTML = frmObj.hDistInnerHtml.value;
		
	}
	if(frmObj.Chk_Grp_ALL_Regn.checked == true && selAllChk!='Regn'){
		frmObj.Chk_Grp_ALL_Regn.checked = false;
	}
	if(frmObj.Chk_Grp_ALL_Dist.checked == true){
		frmObj.Chk_Grp_ALL_Dist.checked = false;
	}
	if(fnGetUnCheckedValues(frmObj.Chk_GrpRegn)==''){
		frmObj.Chk_Grp_ALL_Regn.checked = true;
	}
}

//Called to load Territory list dynamically based on the selected distributors in the Distributors list
function fnLoadTerr(arrChk)
{ 
	if(document.all.Div_Terr != null){
		var j = 0;
		var k = 0;
		var newhtml = '';
		var arrterrlen = TAr.length;
		var arrResult = new Array();
		if(frmObj.Comp == undefined || frmObj.Comp == null)
		{
			vcompanay = comp;
		}
		else
		{
			vcompanay = frmObj.Comp.value;
		}
		if (arrChk != null && arrChk.length > 0)
		{
			// does nothing ... but dont remove		
		}
		else if( frmObj.Chk_GrpDist != null )
		{
			var arr = frmObj.Chk_GrpDist;
			var arrChk = new Array();
			var arrlen = arr.length;
			if(arrlen == undefined && arr.checked == true)
			{
				arrChk[j] = arr.id;
				j++;
			}else{
				for (var i=0;i< arrlen;i++ )
				{
					if (arr[i].checked == true)
					{
						arrChk[j] = arr[i].id;
						j++;
					}
				}
			}
			if(j==0 && frmObj.Chk_GrpRegn != null ){
				var arrayValues = getArrayOfValues(frmObj.Chk_GrpRegn);
				if(arrayValues.length > 0){
					fnLoadDist(arrayValues);
				}
				return;
			}
		}else{
			var arrChk = new Array();
		}
		if (j >0 || vcompanay != null)
		{
			j = 0;
			k = 0;
			var arrTerr = new Array();
		
			var inhtml = document.all.Div_Terr.innerHTML;
			if (arrChk != null && arrChk.length >0)
			{
				document.all.Div_Terr.innerHTML = '';
				var arrChklen = arrChk.length;
				var chkdist = '';
				for (var i=0;i<arrChklen ;i++ )
				{
					chkdist = arrChk[i];
					for (var j=0;j<arrterrlen ;j++ )
					{		
						if (TAr[j][2] == chkdist && TAr[j][3] == vcompanay)
						{
							arrResult[k] = new Array();
							arrResult[k] = TAr[j];
							arrTerr[k] = TAr[j][0];
							k++;
						}
					}
				}
			}else if (vcompanay != null){
				for (var j=0;j<arrterrlen ;j++ )
				{	
					if (TAr[j][3] == vcompanay)
					{
						arrResult[k] = new Array();
						arrResult[k] = TAr[j];
						arrTerr[k] = TAr[j][0];
						k++;
					}
				}
			}
			arrResult.sort(sortFunction);
			for (var l=0;l<k ;l++ )
			{
				if (newhtml.indexOf("id='"+TAr[l][0]+"'") < 0)
				{
				newhtml = newhtml + fnCreateChkBox('Terr',arrResult[l][0],arrResult[l][1],arrResult[l][2],'fnLoadAcct(this)');
				}
			}
			if (newhtml =='')
			{
				document.all.Div_Terr.innerHTML = '<span class=RightTextRed>No Territories</span>';
			}else
			{
				document.all.Div_Terr.innerHTML = newhtml;
			}
		}
		else
		{
			document.all.Div_Terr.innerHTML = document.frmMain.hTerrInnerHtml.value;
		}
	}
	if(frmObj.Chk_Grp_ALL_Dist.checked == true && selAllChk!='Dist'){
		frmObj.Chk_Grp_ALL_Dist.checked = false;
	}
	if(fnGetUnCheckedValues(frmObj.Chk_GrpDist)==''){
		frmObj.Chk_Grp_ALL_Dist.checked = true;
	}
}

// Not used now
function fnLoadAcct(obj)
{
	//alert(obj.id);
}

// To create checkboxes dynamically 
function fnCreateChkBox(grp,id,nm,pid,jsfunc)
{
	var html = "<input class='RightInput' type='checkbox' name='Chk_Grp"+grp+"' id='"+id+"' value='"+pid+"' onClick='javascript:"+jsfunc+";'>&nbsp;<span class='RightTextAS'>"+nm+"</span><BR>";
	return html;
}

// Function resets the lists and the selections
function fnResetSales()
{
	
	// PMT-25353 (We removed the multi check box on page load. So, no need to assign the Div values (from hidden field))
	
	if(document.all.Div_Div != null)
	{
	//document.all.Div_Div.innerHTML = frmObj.hDivInnerHtml.value;
	//frmObj.hDivInnerHtml.value = document.all.Div_Div.innerHTML;
	fnLoadDiv(null);
	}
	if(document.all.Div_Zone != null)
	{
	//document.all.Div_Zone.innerHTML = frmObj.hZoneInnerHtml.value;
	//frmObj.hZoneInnerHtml.value = document.all.Div_Zone.innerHTML;
	fnLoadZone(null);
	}
	if(document.all.Div_Regn != null)
	{
	//document.all.Div_Regn.innerHTML = frmObj.hRegnInnerHtml.value;
	//frmObj.hRegnInnerHtml.value = document.all.Div_Regn.innerHTML;
	fnLoadRegn(null);
	}
	if(document.all.Div_Dist != null)
	{
	//document.all.Div_Dist.innerHTML = frmObj.hDistInnerHtml.value;
	//frmObj.hDistInnerHtml.value = document.all.Div_Dist.innerHTML;
	fnLoadDist(null);
	}
	if(document.all.Div_Terr != null)
	{
	//document.all.Div_Terr.innerHTML = frmObj.hTerrInnerHtml.value;
	//frmObj.hTerrInnerHtml.value = document.all.Div_Terr.innerHTML;
	fnLoadTerr(null);
	}
	
	
	if(document.all.Div_Pgrp != null)
	{
	document.all.Div_Pgrp.innerHTML = frmObj.hPgrpInnerHtml.value;
	frmObj.hPgrpInnerHtml.value = document.all.Div_Pgrp.innerHTML;
	}
	
	
	if(frmObj.Chk_Grp_ALL_Div.checked == true){
		frmObj.Chk_Grp_ALL_Div.checked = false;
	}
	if(frmObj.Chk_Grp_ALL_Zone.checked == true){
		frmObj.Chk_Grp_ALL_Zone.checked = false;
	}
	if(frmObj.Chk_Grp_ALL_Regn.checked == true){
		frmObj.Chk_Grp_ALL_Regn.checked = false;
	}
	if(frmObj.Chk_Grp_ALL_Dist.checked == true){
		frmObj.Chk_Grp_ALL_Dist.checked = false;
	}
	
	//frmObj.reset();
	//var obj=frmObj.Chk_GrpRegn;
	//uncheckAll(obj);
}

//uncheck all the check box in the group
function uncheckAll(field)
{
	for (i = 0; i < field.length; i++)
	{
		field[i].checked = false ;
	}
}

// Function to get the checked values from the object passed
function fnGetCheckedValues(obj)
{
	var str = '';
	var arr = obj;
	 
	if (obj)
	{
		var arrlen = arr.length;
		//alert(arrlen);
		if (arrlen > 0)
		{
			for (var i=0;i< arrlen;i++ )
			{
				if (arr[i].checked == true)
				{
					str = str + arr[i].id + ',';
				}
			}
		
			if (str != '')
			{
				str = str.substr(0,str.length-1);
			}
		}
		else if(arr.checked == true)
		{
			str = arr.id;
		}
	}
	return str;
}


// Function to set selections from filters onto hidden fields in the Main JSP
function fnSetSelections()
{
	// Setting the following values to the 4 hidden fields in the Main Form
	var vcompanay = '';
	var vdivision = '';
	var vzone = '';
	// PMT-25353 - Sales more filter - reduce the memory and improve the performance

	//Hidden filed - only reset when click the more filter hyper link 
	// Otherwise no need to reset the hidden filed.

	if(expendMoreFilterFl){
		frmObj.hRegnFilter.value = fnGetCheckedValues(frmObj.Chk_GrpRegn);
		frmObj.hDistFilter.value = fnGetCheckedValues(frmObj.Chk_GrpDist);
		
		if(frmObj.Chk_GrpTerr != null){
		frmObj.hTerrFilter.value = fnGetCheckedValues(frmObj.Chk_GrpTerr);
		}
		
		if(frmObj.Chk_GrpPgrp != null){
		frmObj.hPgrpFilter.value = fnGetCheckedValues(frmObj.Chk_GrpPgrp);
		}
		
	}	
	
	
	if(frmObj.Comp == undefined || frmObj.Comp == null)
	{
		vcompanay = comp;
	}
	else
	{
		vcompanay = frmObj.Comp.value;
	}
	
	frmObj.hCompFilter.value = vcompanay;
	
	// only reset the values when click the more filter 
	// Otherwise no need to reset the hidden filed.
	if(expendMoreFilterFl){
		frmObj.hDivFilter.value = fnGetCheckedValues(frmObj.Chk_GrpDiv);
		frmObj.hZoneFilter.value = fnGetCheckedValues(frmObj.Chk_GrpZone);
	}
	
	// To clear all the extra filters to be subimited
	frmObj.hZoneInnerHtml.value = "";
	frmObj.hRegnInnerHtml.value = "";
	frmObj.hDistInnerHtml.value = "";
	frmObj.hAcctInnerHtml.value = "";
	frmObj.hDivInnerHtml.value = "";
	frmObj.hPgrpInnerHtml.value = "";
	frmObj.hTerrInnerHtml.value = "";

}

//this function is called from GmItemRequestApproval.js to set sales filter values on frmObj (
function fnItemRequestReloadSales()
{

	fnSetSelections();
	frmObj.hCookieFlag.value = "NO";
	
	//below code to find distributor count of checked and unchecked values,to pass lesser value to avoid 1000 limit exception in query
	if(frmObj.Chk_Grp_ALL_Dist.checked == false){
	var distCheckVal=fnGetCheckedValues(frmObj.Chk_GrpDist);
	var distUnCheckVal =fnGetUnCheckedValues(frmObj.Chk_GrpDist);
	var arrDistCheck = new Array();
	var arrDistUnCheck = new Array();
	var arrDistCheckLen = 0;
	var arrDistUnCheckLen = 0;
	
	if(distCheckVal.indexOf(',') > 0){
	   arrDistCheck=distCheckVal.split(",");
	   arrDistCheckLen=arrDistCheck.length;
	}else if(distCheckVal!=''){
		arrDistCheckLen = 1;
	}
	
	if(distUnCheckVal.indexOf(',') > 0){
		arrDistUnCheck=distUnCheckVal.split(",");
		arrDistUnCheckLen=arrDistUnCheck.length;
	}else if(distUnCheckVal!=''){
		arrDistUnCheckLen = 1;
		}
	//if distributor checked value is greater than distributor unchecked value, we passing unchecked value 
	if(arrDistCheckLen > arrDistUnCheckLen){
		frmObj.hDistFilterUnChk.value=distUnCheckVal;
		frmObj.hDistFilter.value='';
	}else{
		frmObj.hDistFilterUnChk.value='';
	}
	}else if(frmObj.Chk_Grp_ALL_Dist.checked == true){
		frmObj.hDistFilter.value='';
		frmObj.hDistFilterUnChk.value='';
	}
	
	fnStartProgress();

}

// Called when Go button is clicked from the Sales Filters sub-section
function fnReloadSales(haction,url)
{ 	 
	fnSetSelections();
	if(haction!= '' && haction!=null && haction!=undefined)
	{
	frmObj.hAction.value = haction;
	}
	if(url!='' && url!=null && url!=undefined)
	{ 
		frmObj.action = url;
	}
	frmObj.hCookieFlag.value = "NO";
	
	//below code to find distributor count of checked and unchecked values,to pass lesser value to avoid 1000 limit exception in query
	if(frmObj.Chk_Grp_ALL_Dist.checked == false){
	var distCheckVal=fnGetCheckedValues(frmObj.Chk_GrpDist);
	var distUnCheckVal =fnGetUnCheckedValues(frmObj.Chk_GrpDist);
	var arrDistCheck = new Array();
	var arrDistUnCheck = new Array();
	var arrDistCheckLen = 0;
	var arrDistUnCheckLen = 0;
	
	if(distCheckVal.indexOf(',') > 0){
	   arrDistCheck=distCheckVal.split(",");
	   arrDistCheckLen=arrDistCheck.length;
	}else if(distCheckVal!=''){
		arrDistCheckLen = 1;
	}
	
	if(distUnCheckVal.indexOf(',') > 0){
		arrDistUnCheck=distUnCheckVal.split(",");
		arrDistUnCheckLen=arrDistUnCheck.length;
	}else if(distUnCheckVal!=''){
		arrDistUnCheckLen = 1;
		}
	//if distributor checked value is greater than distributor unchecked value, we passing unchecked value 
	if(arrDistCheckLen > arrDistUnCheckLen){
		frmObj.hDistFilterUnChk.value=distUnCheckVal;
		frmObj.hDistFilter.value='';
	}else{
		frmObj.hDistFilterUnChk.value='';
	}
	}else if(frmObj.Chk_Grp_ALL_Dist.checked == true){
		frmObj.hDistFilter.value='';
		frmObj.hDistFilterUnChk.value='';
	}
	
	fnStartProgress();
	frmObj.submit();
}

// Retrieves values sent from Servlet, creates the lists dynamically and sets selections
function fnLoadSelections()
{
	
	var arrlen;
	var arr;

	if(frmObj.Comp != null)
	{
	arr = frmObj.Comp;
	if (comp != '')
	{
		arr.value=comp;
		vFilterShowFl = true;
		fnLoadDiv(arr);
	}
	}
	
	if(frmObj.Chk_GrpDiv != null)
	{
	arr = frmObj.Chk_GrpDiv;
	if (div != '')
	{
		fnCheckBoxSelections(div,arr);
		fnLoadZone(arr);
		vFilterShowFl = true;
	}
	}
	
	if(frmObj.Chk_GrpZone != null)
	{
	arr = frmObj.Chk_GrpZone;
	if (zone != '')
	{
		fnCheckBoxSelections(zone,arr);
		fnLoadRegn(zone.split(","));
		vFilterShowFl = true;
	}
	}
	
	if(frmObj.Chk_GrpRegn != null)
	{
	arr = frmObj.Chk_GrpRegn;
	arrlen = arr.length; 
	if (regn != '')
	{
		fnCheckBoxSelections(regn,arr);
		fnLoadDist(regn.split(","));
		vFilterShowFl = true;
	}
	}
	
	if(frmObj.Chk_GrpDist != null)
	{
	arr = frmObj.Chk_GrpDist;
	if (dist != '')
	{	 
		if(document.all.Div_Terr != null){
			fnLoadTerr(dist.split(","));
		}
		fnCheckBoxSelections(dist,arr);
		vFilterShowFl = true;
	}else{
		if(distAll == 'Y'){
			getChkAll(true,document.all.Div_Dist.innerHTML,'Dist',function(){});
		}else if(distUnChk != ''){
			getChkAll(true,document.all.Div_Dist.innerHTML,'Dist',function(){});
			fnUnCheckBoxVal(distUnChk);
		}
	}
	}
	if(frmObj.Chk_GrpTerr != null)
	{
	arr = frmObj.Chk_GrpTerr;
	if (terr != '')
	{
		fnCheckBoxSelections(terr,arr);
		vFilterShowFl = true;
	}
	} 
	if(frmObj.Chk_GrpPgrp != null)
	{
	arr = frmObj.Chk_GrpPgrp;
	if (pgrp != '')
	{
		fnCheckBoxSelections(pgrp,arr);
		vFilterShowFl = true;
	}
	} 
	
	if(divAll == 'Y'){
		frmObj.Chk_Grp_ALL_Div.checked = true;
	}
	if(zoneAll == 'Y'){
		frmObj.Chk_Grp_ALL_Zone.checked = true;
	}
	if(regnAll == 'Y'){
		frmObj.Chk_Grp_ALL_Regn.checked = true;
	}
	if(distAll == 'Y'){
		frmObj.Chk_Grp_ALL_Dist.checked = true;
	}
	
}


// Marks the checkboxes as 'checked'
function fnCheckBoxSelections(val,arr)
{
	var valobj = val.split(",");
	var sval = '';
	var arrlen = arr.length;
	
	if (arrlen > 0)
	{
		for (var j=0;j< valobj.length;j++ )
		{
			sval = valobj[j];
			for (var i=0;i< arrlen;i++ )
			{
				if (arr[i].id == sval)
				{
					arr[i].checked = true;
					break;
				}
			}
		}
	}
	else
	{
		arr.checked = true;
	}
}


function fnShowFilters()
{
	var obj = document.all.tabFilter;
	objStatus(obj);
}

function objStatus(obj)
{
	if(obj != null){	
		if (obj.style.display == 'none')
		{
			// to load the all the Hierarchy changes.

			fnLoadSelections();

			// to set the expend flag
			expendMoreFilterFl = true;
			obj.style.display = 'block';
			
		}
		else
		{
			obj.style.display = 'none';	
		}
	}		
}
function getArrayOfValues(obj){
	var arr = obj;
	var j =0;
	var arrChk = new Array();
	if(obj == null){
		return arrChk;
	}
	var arrlen = arr.length;
	if(arrlen == undefined && arr.checked == true)
	{
		arrChk[j] = arr.id;
		j++;
	}else{
		for (var i=0;i< arrlen;i++ )
		{
			if (arr[i].checked == true)
			{
				arrChk[j] = arr[i].id;
				j++;
			}
		}
	}
	return arrChk;
}

function sortFunction(a, b) {
	var x = a[1].toLowerCase();
	var y = b[1].toLowerCase();
	return ((x < y) ? -1 : ((x > y) ? 1 : 0));
	}

//Function to check all checkbox by passing object,array
 function getChkAll(allCheckObj,divAllVal,val,callback){
	 var parentObj=document.getElementById("Div_"+val);
	 var cnt=0;
	 var arrLen=divAllVal.split('Chk_Grp'+val).length-1;
	 for(var i=0;i<arrLen;i++){
		 if(allCheckObj == true){
			 parentObj.children[cnt].checked=true;
		 cnt +=3;
		 }else{
			 parentObj.children[cnt].checked=false;
			 cnt +=3;
		 }
	 }
	 callback();
}

//Function to check all checkbox by Div,Regn,Zone,Dist
function fnLoadAllCont(val){
	var allContArr= new Array();
	var allCheckObj= '';
	 var divAllVal='';
	
	
	if(val == 'Div'){
		divAllVal =document.all.Div_Div.innerHTML;
		allCheckObj = frmObj.Chk_Grp_ALL_Div.checked;
	}
	if(val == 'Zone'){
		divAllVal =document.all.Div_Zone.innerHTML;
		allCheckObj = frmObj.Chk_Grp_ALL_Zone.checked;
	}
	if(val == 'Regn'){
		divAllVal =document.all.Div_Regn.innerHTML;
		allCheckObj = frmObj.Chk_Grp_ALL_Regn.checked;
	}
	if(val == 'Dist'){
		divAllVal =document.all.Div_Dist.innerHTML;
		allCheckObj = frmObj.Chk_Grp_ALL_Dist.checked;
	}
	
	getChkAll(allCheckObj,divAllVal,val,function (){
	
	if(val == 'Div'){
		selAllChk='Div';
		frmObj.Chk_Grp_ALL_Zone.checked=false;
		frmObj.Chk_Grp_ALL_Regn.checked=false;
		frmObj.Chk_Grp_ALL_Dist.checked=false;
		fnLoadZone(allContArr);
		selAllChk='';
	}
	if(val == 'Zone'){
		selAllChk='Zone';
		frmObj.Chk_Grp_ALL_Regn.checked=false;
		frmObj.Chk_Grp_ALL_Dist.checked=false;
		fnLoadRegn(allContArr);
		selAllChk='';
	}
	if(val == 'Regn'){
		selAllChk='Regn';
		frmObj.Chk_Grp_ALL_Dist.checked=false;
		fnLoadDist(allContArr);
		selAllChk='';
	}
	if(val == 'Dist'){
		if(document.all.Div_Terr != null){
			selAllChk='Dist';
			fnLoadTerr(allContArr);
			selAllChk='';
		}
	}
	});
	
}


//Function to get the unchecked values from the object passed
function fnGetUnCheckedValues(obj)
{
	var str = '';
	var arr = obj;
	 
	if (obj)
	{
		var arrlen = arr.length;
		if (arrlen > 0)
		{
			for (var i=0;i< arrlen;i++ )
			{
				if (arr[i].checked == false)
				{
					str = str + arr[i].id + ',';
				}
			}
		
			if (str != '')
			{
				str = str.substr(0,str.length-1);
			}
		}
		else if(arr.checked == false)
		{
			str = arr.id;
		}
	}
	return str;
}

//Function to uncheck a values  passing in a array 
function fnUnCheckBoxVal(val)
{
	var valobj = val.split(",");
	if (valobj.length > 0)
	{
		for (var j=0;j< valobj.length;j++ )
		{
			document.getElementById(valobj[j]).checked =false;
		}
	}
}