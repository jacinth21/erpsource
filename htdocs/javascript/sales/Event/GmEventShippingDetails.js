function fnBack()
{   
	var vCatList =  document.frmCaseBookSetDtls.categoryList.value ;
	arrayCat = vCatList.split(",");
	arrayCat.sort(sortfunc);
	fwdProdType = arrayCat[arrayCat.length-1];
	strCaseInfoID =  document.frmCaseBookSetDtls.caseInfoID.value ;
	strcaseID =  document.frmCaseBookSetDtls.caseID.value ;
	if(fwdProdType == ""){
		document.frmCaseBookSetDtls.action = "/gmEventSetup.do?method=addEvent&strOpt=edit&caseInfoID="+strCaseInfoID;
		
	}else{
		if(fwdProdType == "19533"){//item
			document.frmCaseBookSetDtls.action ="/gmEventItemDtl.do?strOpt=load&fromscreen=eventsetup&productionType="+fwdProdType;
		}else{
			document.frmCaseBookSetDtls.action ="/gmCaseBookSetDtls.do?strOpt=editEvent&fromscreen=eventsetup&eventsetup=confirm&caseInfoID="+ strCaseInfoID+"&caseID="+strcaseID+"&fwdProductType="+fwdProdType;
		}
	}
	document.frmCaseBookSetDtls.submit();
}

function sortfunc(a,b)
{
	return a - b;
}

function fnNext()
{   
	
	objPlannedDt = document.frmCaseBookSetDtls.plannedShipDate;
	objExpectedDt   = document.frmCaseBookSetDtls.expReturnDate;
	
	var shipto = document.all.shipTo.value;
	if (shipto != '0')
	{
		fnValidateDropDn('names', lblNames);
	}
	if(objPlannedDt.value == "" && objExpectedDt.value == ""){
		Error_Details(message_sales[212]);
	}	
	if(objPlannedDt.value != "" && objExpectedDt.value == ""){
		Error_Details(message_sales[213]);
	}
	if(objExpectedDt.value != "" && objPlannedDt.value == ""){
		Error_Details(message_sales[214]);
	}	
	if(objPlannedDt.value != ""){
		//DateValidateMMDDYYYY(objPlannedDt, '<b>Planned Shipped Date - </b>'+ message[1]);
		CommonDateValidation(objPlannedDt, format, Error_Details_Trans(message_sales[210],format));
	}
	if(objExpectedDt.value != ""){		
		//DateValidateMMDDYYYY(objExpectedDt, '<b>Expected Return Date - </b>'+ message[1]);
		CommonDateValidation(objExpectedDt, format,Error_Details_Trans(message_sales[211],format) );
	}
	var dtPlannedShip        = document.frmCaseBookSetDtls.plannedShipDate.value;
    var dtExpectedRtn        = document.frmCaseBookSetDtls.expReturnDate.value;  
    var dtEventStrtDt        = document.frmCaseBookSetDtls.eventStartDate.value;
    var dtEventEndDt         = document.frmCaseBookSetDtls.eventEndDate.value;       
    var ShippDtDiff 		 = dateDiff(dtEventStrtDt,dtPlannedShip,format); 
    var ExpetDtDiff			 = dateDiff(dtEventEndDt,dtExpectedRtn,format); 
    var currentdate          = document.frmCaseBookSetDtls.currentdate.value;   
    var shipTo				 = document.all.shipTo.value;
    var confirmMsg 			 = true;
    
    if(dtPlannedShip != "" && ErrorCount ==0){
    	if(ShippDtDiff > 0){
    		Error_Details(message_sales[215]);
    	}
    	ShippDtDiff = dateDiff(currentdate,dtPlannedShip,format);
    	if(ShippDtDiff < 0 ){
			Error_Details(message_sales[216]);
	    }
    }
    if(dtExpectedRtn != ""  && ErrorCount ==0){
    	if(ExpetDtDiff < 0){
    		Error_Details(message_sales[217]);
    	}    	
    	ExpetDtDiff = dateDiff(currentdate,dtExpectedRtn,format);
    	if(ExpetDtDiff < 0 && ErrorCount ==0){
			Error_Details(message_sales[218]);
	    }
    }
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	
	if(shipTo == '0'){
    	confirmMsg = confirm(message_sales[219]); 
    }
    if(!confirmMsg){
    	return false;
    }else{
	     document.frmCaseBookSetDtls.strOpt.value = "eventshipping"; 
	     document.frmCaseBookSetDtls.haction.value = "Save"; 
	     //document.frmCaseBookSetDtls.action = '/gmIncShipDetails.do?screenType=Loaner&RE_FORWARD=gmEventShipAction&strOpt=Load&haction=Save';
	     document.frmCaseBookSetDtls.submit(); 
	}  
}