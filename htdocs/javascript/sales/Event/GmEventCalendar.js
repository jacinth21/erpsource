function fnOnPageLoad() {
	//objStartDt = document.frmEventRpt.fromDt.value;	
	var objMode = document.frmEventRpt.haction;	
	if(objMode.value == ''){
		clendarMode = 'month';
	}else{
		clendarMode = objMode.value;
	}	
	var topObj = getObjTop();
	var dateformat = topObj.gCompJSDateFmt;
	var xmldtfmt = dateformat+" %H:%i";
	scheduler.config.multi_day = true;
	scheduler.config.details_on_create = false;
	scheduler.config.details_on_dblclick = false;
	scheduler.config.full_day = true;
	scheduler.config.xml_date = xmldtfmt;
	scheduler.init('eventCalender',new Date(),clendarMode);
	scheduler.config.cascade_event_display = true; 
    scheduler.config.cascade_event_count = 4;	
    scheduler.config.cascade_event_margin = 10;   
	scheduler.parse(gridData);
	scheduler.config.show_loading =true;
	scheduler.config.readonly_form = true;
	scheduler.templates.tooltip_text = function(start,end,event) {		
		return "<b> "+message_sales[36]+" : "+event.eventnm+"</b><br/><b> "+message_sales[37]+" : "+event.eventpurpose+"</b><br/><b> "+message_sales[38]+" : </b>"+event.loanto+"<br/><b>"+message_sales[39]+" : </b>"+event.loantoname+"<br/><b>"+event.hyperlink+"</b>";
	};
}

function show_minical(){
	if (scheduler.isCalendarVisible())
		scheduler.destroyCalendar();
	else
		scheduler.renderCalendar({
			position:"dhx_minical_icon",
			date:scheduler._date,
			navigation:true,
			handler:function(date,calendar){
				scheduler.setCurrentView(date);
				dtSchedule = convert(date);
				document.frmEventRpt.eventStartDt.value = dtSchedule;
				document.frmEventRpt.eventEndDt.value = dtSchedule;
				scheduler.destroyCalendar()
			}
		});
}

function fnCalendarEventInfo(infoid) {	
	if (GLOBAL_ClientSysType == "IPAD"){
		document.frmEventRpt.action = "/gmEventReport.do?method=downloadSummaryAsPDF&strOpt=HTML&caseInfoID="+infoid;
	}else{
		document.frmEventRpt.action = "/gmEventReport.do?method=downloadSummaryAsPDF&strOpt=HTML&caseInfoID="+infoid;
	}
	document.frmEventRpt.submit();
}
function Refresh(){
	   document.frmEventRpt.haction.value = scheduler._mode;
	   if(scheduler._mode == 'week' || scheduler._mode == 'day'){
		   document.frmEventRpt.strOpt.value= "";
	   }
	   document.frmEventRpt.submit();
	   }

scheduler._click={
		dhx_cal_data:function(e){
			var trg = e?e.target:event.srcElement;
			var id = scheduler._locate_event(trg);
			
			e = e || event;
			if ((id && !scheduler.callEvent("onClick",[id,e])) ||scheduler.config.readonly) return;
			
			if (id) {		
				scheduler.select(id);
				var mask = trg.className;
				if (mask.indexOf("_icon")!=-1)
					scheduler._click.buttons[mask.split(" ")[1].replace("icon_","")](id);
			} else
				scheduler._close_not_saved();
		},
		dhx_cal_prev_button:function(){
			scheduler._click.dhx_cal_next_button(0,-1);
		},
		dhx_cal_next_button:function(dummy,step){
			
			if(scheduler._mode == 'month'){
			var dtSchedule = scheduler._date;
			 var dtMonth = dtSchedule.getMonth();
			 var dtDay   = '01';
			 var dtYear  = dtSchedule.getFullYear();
			 if(step == undefined){
				 step = 1; 
			 }
			 
			 dtMonth =dtMonth+step;
			 if(dtMonth > 12){
				 dtMonth = dtMonth-12;
				 dtYear = dtYear+1;
			 }
			
			dtSchedule = convert(dtSchedule);
			document.frmEventRpt.strOpt.value="load";
			document.frmEventRpt.haction.value = scheduler._mode;
			document.frmEventRpt.eventStartDt.value=dtSchedule;
			document.frmEventRpt.eventEndDt.value=dtSchedule;
			document.frmEventRpt.ecMonth.value=dtMonth;
			document.frmEventRpt.ecYear.value=dtYear;
			document.frmEventRpt.submit();
			return false;
			}
			if(scheduler._mode == 'week'){
				var dd=scheduler.date[scheduler._mode+"_start"](new Date(scheduler._date.valueOf()));
				var ed=scheduler.date.add(dd,1,scheduler._mode);
				if(step == undefined){
				step = 1;
				strDate = convert(ed);
				strEndDate = convert(ed.setDate(ed.getDate()+7));
				 }else{
				 strEndDate = convert(dd);
				 strDate = convert(dd.setDate(dd.getDate()-7)); 
				 }
				document.frmEventRpt.haction.value = scheduler._mode;
				document.frmEventRpt.eventStartDt.value=strDate;
				document.frmEventRpt.eventEndDt.value=strEndDate;
				//document.frmEventRpt.submit();
				
				}
			if(scheduler._mode == 'day'){
				var dd=scheduler.date[scheduler._mode+"_start"](new Date(scheduler._date.valueOf()));
				var ed=scheduler.date.add(dd,1,scheduler._mode);
				if(step == undefined){
				step = 1;
				strDate = convert(dd.setDate(dd.getDate()+1));
				strEndDate = strDate;
				}else{
				strDate = convert(dd.setDate(dd.getDate()-1));
				strEndDate = strDate;	
				}
				document.frmEventRpt.haction.value = scheduler._mode;
				document.frmEventRpt.eventStartDt.value=strDate;
				document.frmEventRpt.eventEndDt.value=strEndDate;
				//document.frmEventRpt.submit();
				
			}
			scheduler.setCurrentView(scheduler.date.add( //next line changes scheduler._date , but seems it has not side-effects
				scheduler.date[scheduler._mode+"_start"](scheduler._date),(step||1),scheduler._mode));
		},
		dhx_cal_today_button:function(){
			scheduler.setCurrentView(new Date());
		},
		dhx_cal_tab:function(){
			var mode = this.getAttribute("name").split("_")[0];
			scheduler.setCurrentView(scheduler._date,mode);
		},
		buttons:{
			"delete":function(id){ var c=scheduler.locale.labels.confirm_deleting; if (!c||confirm(c)) scheduler.deleteEvent(id); },
			edit:function(id){ scheduler.edit(id); },
			save:function(id){ scheduler.editStop(true); },
			details:function(id){ scheduler.showLightbox(id); },
			cancel:function(id){ scheduler.editStop(false); }
		}
	}

function convert(str) {
    var date = new Date(str),
        mnth = ("0" + (date.getMonth()+1)).slice(-2),
        day  = ("0" + date.getDate()).slice(-2);
    return [ mnth, day ,date.getFullYear()].join("/");
}