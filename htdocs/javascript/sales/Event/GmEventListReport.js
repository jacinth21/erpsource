//Description: This function used for initializing the filter values gridData.
function fnOnPageLoad(){
	gridObj = initGridWithDistributedParsing('GridData',objGridData);
	document.frmEventRpt.dtType.value = strDtType;
}

function fnLoadLoanToNames(obj){
	var loanto = obj.value;	
	if(loanto == '0'){
		document.getElementById("loanToName").value = '0';	
		document.frmEventRpt.loanToName.disabled = true;
	}
	if(loanto != '0' && loanto != '' ){
		document.frmEventRpt.loanToName.disabled = false;
		if (loanto == '19518') 
		{
			document.frmEventRpt.loanToName.options.length = 0;
			document.frmEventRpt.loanToName.options[0] = new Option("[Choose One]","0");
			for (var i=0;i<RepLen;i++)
			{
				arr = eval("alRepListArr"+i);
				arrobj = arr.split(",");
				id = arrobj[0];
				name = arrobj[1];
				document.frmEventRpt.loanToName.options[i+1] = new Option(name,id);
			}
			//document.getElementById("eventLoanToName").value = '0';
		}else if(loanto == '19523'){
			
			document.frmEventRpt.loanToName.options.length = 0;
			document.frmEventRpt.loanToName.options[0] = new Option("[Choose One]","0");
			for (var i=0;i<RepLen;i++)
			{
				arr = eval("alOUSListArr"+i);
				arrobj = arr.split(",");
				id = arrobj[0];
				name = arrobj[1];
				document.frmEventRpt.loanToName.options[i+1] = new Option(name,id);
			}
		}
		else if(loanto == '106400')
		{
			document.frmEventRpt.loanToName.options.length = 0;
			document.frmEventRpt.loanToName.options[0] = new Option("[Choose One]","0");
			for (var i=0;i<EmpLen;i++)
			{
				arr = eval("alEmpListArr"+i);
				arrobj = arr.split(",");
				id = arrobj[0];
				name = arrobj[1];
				document.frmEventRpt.loanToName.options[i+1] = new Option(name,id);
			}
		}
	}
}

//Description: This function will call onclick load button. check the validation & pass control to action.
function fnLoad() {
	
	objEventName = document.frmEventRpt.eventName.value;
	objPupEvent = document.frmEventRpt.pupEvent.value;
	objLoanTo = document.frmEventRpt.loanTo.value;
	objLoanToName = document.frmEventRpt.loanToName.value;
	objDtType = document.frmEventRpt.dtType.value;
	objStartDt = document.frmEventRpt.startDt;
	objEndDt = document.frmEventRpt.endDt;
	
	if( (objDtType == "0" && (objStartDt.value != "" && objEndDt.value != "")) || (objDtType == "0" && (objStartDt.value == "" && objEndDt.value != ""))
			|| (objDtType == "0" && (objStartDt.value != "" && objEndDt.value == ""))){
		Error_Details(message_sales[46]);
	}
	if( objDtType == "ESD" && (objStartDt.value == "" && objEndDt.value == "")){
		Error_Details(message_sales[47]);
	}
	if((objDtType == "ESD" && (objStartDt.value != "" && objEndDt.value == "")) || (objStartDt.value != "" && objEndDt.value == "")){
		Error_Details(message_sales[48]);
	}
	if((objDtType == "ESD" && (objStartDt.value == "" && objEndDt.value != "")) || (objStartDt.value == "" && objEndDt.value != "")){
		Error_Details(message_sales[49]);
	}
	if( objDtType == "EED" && (objEndDt.value == "" && objStartDt.value != "")){
		Error_Details(message_sales[50]);
	}
	if( objDtType == "EED" && (objEndDt.value != "" && objStartDt.value == "")){
		Error_Details(message_sales[51]);
	}
	if(objStartDt.value != "" ){
		CommonDateValidation(objStartDt, format, Error_Details_Trans(message_sales[25],format));
	}
	if(objEndDt.value != ""){
		CommonDateValidation(objStartDt, format, Error_Details_Trans(message_sales[26],format));
	}
	
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
    document.frmEventRpt.strOpt.value = "Reload"
	document.frmEventRpt.action = "/gmEventReport.do?method=listEvent";
    fnStartProgress("Y");
	document.frmEventRpt.submit();
}

//Description: This function will call onclick on eventId & control transfer to Favourite Event screen.
function fnEventSummaryScreen(cid,eid){
	var event_name = "";
	gridObj.forEachRow(function(rowId) {
		if(rowId == cid){
		event_name = gridObj.cellById(rowId, 1).getValue();
		}
	});	
    document.frmEventRpt.action="/gmEventReport.do?method=downloadSummaryAsPDF&strOpt=HTML&caseInfoID="+cid;	
	document.frmEventRpt.submit();
}

//Description: This function will used to generate excel.
function fnDownloadXLS() {	
	gridObj.toExcel('/phpapp/excel/generate.php');
}