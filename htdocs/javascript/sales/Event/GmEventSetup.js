
function fnCheckSelections()
{	
	objCheckApprArrLen = 0;
	objCheckRegionArrLen = 0; 
	var favCaseId = document.frmEventSetup.favouriteCaseID.value;
	var appObj = sCategories.split(",");
	objCheckCategoriesArr = document.frmEventSetup.eventRequestedMeterial;
	if(objCheckCategoriesArr) {
	 
		objCheckCategoriesArrLen = objCheckCategoriesArr.length;
		if(favCaseId !=0 || strCaseInfoID !=''){
							
		for(k = 0; k < objCheckCategoriesArrLen; k ++) 
		{	
			 
				objCheckCategoriesArr[k].checked = false; 
		}

 		for (var j=0;j< appObj.length;j++ )
 		{ 
			for(i = 0; i < objCheckCategoriesArrLen; i ++) 
				{	
					if (objCheckCategoriesArr[i].value == appObj[j])
	 				{
						objCheckCategoriesArr[i].checked = true;
						if((favCaseId == 0 && strCaseInfoID !='') || (favCaseId !=0)){
							objCheckCategoriesArr[i].disabled= true;
						}
						if(favCaseId == 0 && strCaseInfoID ==''){
							objCheckCategoriesArr[i].disabled= false;
						}
	 				}				
				}
 		}
		}//end if favouriteCaseID
	}
	if( document.frmEventSetup.strOpt.value != 'reschedule'){
		var eventLoanTo = document.frmEventSetup.eventLoanTo.value;
		if( eventLoanTo != undefined && eventLoanTo == '0'){
			document.frmEventSetup.eventLoanToName.disabled = true;
		}else{
			document.frmEventSetup.eventLoanToName.disabled = false;
		}
		var eventLoanToName=document.frmEventSetup.eventLoanToName.value;
		if( eventLoanToName != undefined && eventLoanToName == '0'){
			document.getElementById("eventLoanToName").value = eventLoanToName;	
		}
	}
}	 

function fnSelectAll(){
	var objCheckCategoriesArr = document.frmEventSetup.eventRequestedMeterial;
	var categoriesLen = objCheckCategoriesArr.length;
	for( var j=0; j<categoriesLen; j++){
		objCheckCategoriesArr[j].checked= false;
		objCheckCategoriesArr[j].disabled = false;
	}
}

function fnSelectedMaterialslist(){
	var k = 0;
	var objCheckCategoriesArr = document.frmEventSetup.eventRequestedMeterial;
	for( var j=0; j<categoriesLen; j++){
		if(objCheckCategoriesArr[j].checked){
			k++;
		}
	}	
}

function fnLoadLoanToNames(obj){
	var loanto = obj.value;	
	if(loanto == '0'){
		document.getElementById("eventLoanToName").value = '0';	
		document.frmEventSetup.eventLoanToName.disabled = true;
	}
	if(loanto != '0' && loanto != '' ){
		document.frmEventSetup.eventLoanToName.disabled = false;
		if (loanto == '19518') 
		{
			document.frmEventSetup.eventLoanToName.options.length = 0;
			document.frmEventSetup.eventLoanToName.options[0] = new Option("[Choose One]","0");
			for (var i=0;i<RepLen;i++)
			{
				arr = eval("alRepListArr"+i);
				arrobj = arr.split(",");
				id = arrobj[0];
				name = arrobj[1];
				document.frmEventSetup.eventLoanToName.options[i+1] = new Option(name,id);
			}
			//document.getElementById("eventLoanToName").value = '0';
		}else if(loanto == '19523')
			{
			document.frmEventSetup.eventLoanToName.options.length = 0;
			document.frmEventSetup.eventLoanToName.options[0] = new Option("[Choose One]","0");
			for (var i=0;i<OUSLen;i++)
			{
				arr = eval("alOUSListArr"+i);
				arrobj = arr.split(",");
				id = arrobj[0];
				name = arrobj[1];
				document.frmEventSetup.eventLoanToName.options[i+1] = new Option(name,id);
			}
		}
		else if(loanto == '106400')
		{
			document.frmEventSetup.eventLoanToName.options.length = 0;
			document.frmEventSetup.eventLoanToName.options[0] = new Option("[Choose One]","0");
			for (var i=0;i<EmpLen;i++)
			{
				arr = eval("alEmpListArr"+i);
				arrobj = arr.split(",");
				id = arrobj[0];
				name = arrobj[1];
				document.frmEventSetup.eventLoanToName.options[i+1] = new Option(name,id);
			}
			//document.getElementById("eventLoanToName").value = '0';		
		}
	}
}
function fnNext(){
	var strOpt = document.frmEventSetup.strOpt.value;	
	fnValidateTxtFld('eventName',lblEventName);
	fnValidateTxtFld('eventVenue',message_sales[297]);	
	fnValidateDropDn('eventPurpose',lblPurposeofEvent);
	fnValidateDropDn('eventLoanTo',lblLoanTo);
	fnValidateDropDn('eventLoanToName',lblLoanToName);	
	objStartDt = document.frmEventSetup.eventStartDate;
	objEndDt   = document.frmEventSetup.eventEndDate;
	
	if(objEndDt.value == ""){
		Error_Details(message_sales[52]);
	}
	if(objStartDt.value == ""){
		Error_Details(message_sales[53]);
	}	
	if(objStartDt.value != ""){
		//DateValidateMMDDYYYY(objStartDt, '<b>Start Date - </b>'+ message[1]);
		CommonDateValidation(objStartDt,format,Error_Details_Trans(message_sales[54],format));
	}
	if(objEndDt.value != ""){		
		//DateValidateMMDDYYYY(objEndDt, '<b>End Date - </b>'+ message[1]);
		CommonDateValidation(objEndDt,format,Error_Details_Trans(message_sales[55],format));
	}
	var dtFrom        = document.frmEventSetup.eventStartDate.value;
    var dtTo          = document.frmEventSetup.eventEndDate.value;  
    var currentDate   = document.frmEventSetup.currentdate;
    var startdatediff = dateDiff(currentDate.value,objStartDt.value,format);
   	if(dtFrom != '' && dtTo != ''){	
		if(dateDiff(dtFrom,dtTo,format) < 0){
			Error_Details(message_sales[56]);
		}		
		if(strOpt != 'edit'){			
			if(startdatediff < 0){
				Error_Details(message_sales[57]);
			}			
		}
	}	
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	fnEnableCategory();
	
	if(strOpt == "edit"){
		document.frmEventSetup.favouriteCaseID.value = 0;
	}
	
	document.frmEventSetup.strOpt.value = 'next';
	document.frmEventSetup.action="/gmEventSetup.do?method=addEvent&backScreen=setupscreen";
	document.frmEventSetup.submit();
}

function fnSubmit(){
	var strOpt = document.frmEventSetup.strOpt.value;	
	fnValidateTxtFld('eventName',lblEventName);
	fnValidateTxtFld('eventVenue',message_sales[297]);	
	fnValidateDropDn('eventPurpose',lblPurposeofEvent);
	fnValidateDropDn('eventLoanTo',lblLoanTo);
	fnValidateDropDn('eventLoanToName',lblLoanToName);	
	objStartDt = document.frmEventSetup.eventStartDate;
	objEndDt   = document.frmEventSetup.eventEndDate;
	
	if(objEndDt.value == ""){
		Error_Details(message_sales[52]);
	}
	if(objStartDt.value == ""){
		Error_Details(message_sales[53]);
	}	
	if(objStartDt.value != ""){
		//DateValidateMMDDYYYY(objStartDt, '<b>Start Date - </b>'+ message[1]);
		CommonDateValidation(objStartDt,format,Error_Details_Trans(message_sales[54],format));
	}
	if(objEndDt.value != ""){		
		//DateValidateMMDDYYYY(objEndDt, '<b>End Date - </b>'+ message[1]);
		CommonDateValidation(objEndDt,format,Error_Details_Trans(message_sales[55],format));
	}
	var dtFrom        = document.frmEventSetup.eventStartDate.value;
    var dtTo          = document.frmEventSetup.eventEndDate.value;  
    var currentDate   = document.frmEventSetup.currentdate;
    var startdatediff = dateDiff(currentDate.value,objStartDt.value,format); 
    var enddatediff = dateDiff(dtFrom,dtTo,format); 
   	if(dtFrom != '' && dtTo != ''){	
		if(enddatediff < 0){
			Error_Details(message_sales[56]);
		}		
		if(strOpt != 'edit'){			
			if(startdatediff < 0){
				Error_Details(message_sales[57]);
			}			
		}
	}	
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	if(strOpt == 'edit'){
		document.frmEventSetup.caseInfoID.value = caseInfoID;
		document.frmEventSetup.favouriteCaseID.value = 0;
	}
	fnEnableCategory();
	var loantoname = document.frmEventSetup.eventLoanToName.value;
	document.frmEventSetup.eventLoanToName.value = loantoname; 	
	document.frmEventSetup.action = "/gmEventSetup.do?method=addEvent";
	document.frmEventSetup.strOpt.value = 'save';	
	document.frmEventSetup.submit();
}
function fnFavorite(){
	var favouriteCaseID = document.frmEventSetup.favouriteCaseID.value;
	
	if(favouriteCaseID == 0){
		fnSelectAll();
	}else{
	document.frmEventSetup.action = "/gmEventSetup.do?method=addEvent";
	document.frmEventSetup.strOpt.value = "fetchFavorite";
	document.frmEventSetup.submit();
	}
	//var loader = dhtmlxAjax.getSync('gmEventSetupAction.do?method=editEvent&stropt=fetchFavorite&favouriteCaseID='+favId+"&time="+new Date().getTime());
}

function fnReschedule(){	
	fnValidateDropDn('rescheduleReason',' Reason');
	objStartDt = document.frmEventSetup.eventStartDate;
	objEndDt   = document.frmEventSetup.eventEndDate;
	objPlannedShipDt = document.frmEventSetup.plannedShipeddate;
	objExpectedRtnDt = document.frmEventSetup.expectedReturnDt;
	
	if(objStartDt.value == ""){
		Error_Details(message_sales[53]);
	}
	if(objEndDt.value == "" ){
		Error_Details(message_sales[52]);
	}	
	if(planshipFl != 'N'){
		if(objPlannedShipDt.value == ""){
			Error_Details(message_sales[58]);
		}
		if(objExpectedRtnDt.value == ""){
			Error_Details(message_sales[59]);
		}
		if(objPlannedShipDt.value != ""){		
			//DateValidateMMDDYYYY(objPlannedShipDt, '<b>Planned Shiped Date - </b>'+ message[1]);
			CommonDateValidation(objPlannedShipDt,format,Error_Details_Trans(message_sales[60],format));
		}
		if(objExpectedRtnDt.value != ""){		
			//DateValidateMMDDYYYY(objExpectedRtnDt, '<b>Expected Return Date - </b>'+ message[1]);
			CommonDateValidation(objExpectedRtnDt,format,Error_Details_Trans(message_sales[60],format));
		}
	}
	if(objStartDt.value != ""){
		//DateValidateMMDDYYYY(objStartDt, '<b>Start Date - </b>'+ message[1]);
		CommonDateValidation(objStartDt,format,Error_Details_Trans(message_sales[54],format));
	}
	if(objEndDt.value != ""){		
		//DateValidateMMDDYYYY(objEndDt, '<b>End Date - </b>'+ message[1]);
		CommonDateValidation(objEndDt,format,Error_Details_Trans(message_sales[55],format));
	}
	
	var dtFrom        = document.frmEventSetup.eventStartDate.value;
    var dtTo          = document.frmEventSetup.eventEndDate.value;  
    var currentDate   = document.frmEventSetup.currentdate;
    if(planshipFl != 'N'){
	    var startdatediff = dateDiff(currentDate.value,objStartDt.value,format); 
	    var enddatediff = dateDiff(currentDate.value,objEndDt.value,format); 
	    var DateDiff      = dateDiff(objPlannedShipDt.value,objExpectedRtnDt.value,format);
	   
	   	if(dtFrom != '' && dtTo != ''){	
			if(!(new Date(dtFrom).valueOf() <= new Date(dtTo).valueOf())){
				Error_Details(message_sales[229]);
			}		
			if(startdatediff < 0){
				Error_Details(message_sales[230]);
			}	
			if(enddatediff < 0){
				Error_Details(message_sales[231]);
		    }
		}  	
	    if(objPlannedShipDt.value != ""){
	    	var PlannedShipDtDiff = dateDiff(currentDate.value,objPlannedShipDt.value,format);
	    	if(PlannedShipDtDiff < 0){
				Error_Details(message_sales[232]);
		    }
	    	PlannedShipDtDiff 		 = dateDiff(objStartDt.value,objPlannedShipDt.value,format); 
	    	if(PlannedShipDtDiff > 0){
	    		Error_Details(message_sales[233]);
	    	}
	    }
	    
	    if(objExpectedRtnDt.value != ""){
	    	var objExpectedRtnDtDiff 	 = dateDiff(objEndDt.value,objExpectedRtnDt.value,format); 
	    	if(objExpectedRtnDtDiff < 0){
	    		Error_Details(message_sales[234] );
	    	}
	    	objExpectedRtnDtDiff = dateDiff(currentDate.value,objExpectedRtnDt.value,format);
	    	if(objExpectedRtnDtDiff < 0 && ErrorCount == 0){
				Error_Details(message_sales[235]);
		    }
	    }
	   	if(DateDiff < 0 && ErrorCount == 0){
	   		Error_Details(message_sales[236]);
	   	}   
    }
    
   	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	var stropt = document.frmEventSetup.strOpt.value;	
	document.frmEventSetup.caseInfoID.value = caseInfoID;	
	document.frmEventSetup.action = "/gmEventSetup.do?method=rescheduleEvent";
	document.frmEventSetup.strOpt.value = 'savereschedule';	
	document.frmEventSetup.submit();
}

function fnEnableCategory(){
	var objCheckCategoriesArr = document.frmEventSetup.eventRequestedMeterial;
	var categoriesLen = objCheckCategoriesArr.length;
	for( var j=0; j<categoriesLen; j++){
		objCheckCategoriesArr[j].disabled = false;	
	}
}
/* The following code is added for the BUG-2153, this is code is copied form date-picker.js file and
 * did the few changes which is for implementing the dhtmlx calendar changes for Event Setup Screen only ...  */
var tempDate = "";
function showSingleCalendar(calendardiv,textboxref,dateformat){
	dateformat = getObjTop().gCompJSDateFmt;
	if (dateformat == null){
		dateformat='%m/%d/%Y';
	}
	if(!calendars[calendardiv])
		calendars[calendardiv]= initCal(calendardiv,textboxref,dateformat);

	//This line for today date will display in BOLD and this method will be in dhtmlxcalendar.js
	calendars[calendardiv].enableTodayHighlight(true); // or false
	
	if(document.getElementById(textboxref).value.length>0 || tempDate !=''){
		calendars[calendardiv].setDate(document.getElementById(textboxref).value);
		if(tempDate !=''){
			calendars[calendardiv].setDate(tempDate);
		}
	}
	if(calendars[calendardiv].isVisible())
		calendars[calendardiv].hide();
	else 
		calendars[calendardiv].show();
}

/* The below code commented PMT-2885 DHTMLX 3.5 new version changes from date picker. Here changes for
Event Setup only and using below method. */

/*function initCal(calendardiv,textboxref,dateformat){	
	if((navigator.userAgent.match(/iPhone/i))!=null || (navigator.userAgent.match(/iPad/i))!=null){
		mCal = new dhtmlxCalendarObject(calendardiv, false, {
		isMonthEditable: false,
		isYearEditable: false });
	}else{
		mCal = new dhtmlxCalendarObject(calendardiv, false, {
		isMonthEditable: true,
		isYearEditable: true});
	}
	mCal.setYearsRange(1970, 2500);
	mCal.draw();
	mCal.disableIESelectFix(true);
	mCal.setDateFormat(dateformat);
	mCal.setOnClickHandler(function(date,obj,type){
		document.getElementById(textboxref).value = this.getFormatedDate(dateformat, date);
		tempDate = this.getFormatedDate(dateformat, date);
		this.hide();
		return true;
	});
	mCal.hide();
	return mCal;
}
*/

// The initCalendar method written for DHTMLX 3.5 new version 
function initCal(calendardiv,textboxref,dateformat){
	//Below code used for dhtmlx 3.5 new version. In old dhtmlx version we can't edit month and year in IPAD. Now we can edit.
	mCal = new dhtmlXCalendarObject(calendardiv);
	
	// This range only able to select date. 
	mCal.setSensitiveRange("1970-01-01", "2500-31-12"); 

	// Set date format based on login user.
	mCal.setDateFormat(dateformat);

	// Hide the time in new dhtmlx version 3.5
	mCal.hideTime();
	
	// Changed the below code for new version DHTMLX. When click calendar icon and set the date in textbox.
	mCal.attachEvent("onClick", function(d) {
		document.getElementById(textboxref).value =  mCal.getFormatedDate(dateformat, d);
		tempDate = this.getFormatedDate(dateformat, d);
		calendars[calendardiv].hide();
		return true;
	 });
	mCal.hide();
	return mCal;
}