
	
function fnSubmit()
{  
	var val = eval(document.frmEventRpt.dnEventAction).value;
	var statusId =  eval(document.frmEventRpt.strStstusID).value;
	var accessFl =  eval(document.frmEventRpt.accessFl).value;
	var eventName = document.frmEventRpt.eventName.value ;	
	var eventID = document.frmEventRpt.eventID.value ;
	//closed and cancelled event can only download pdf
	if (( statusId == '19525' || statusId == '19527')&& val != '1006605')
	{
		Error_Details(message_sales[220]);	
		Error_Show();
		Error_Clear(); 
		return false;
	}
	
	if(val == '1006600' ){  // Edit Event
		if (accessFl == 'Y'){
			Error_Details(message_sales[221]);
		}else if ( statusId == '19534' || statusId == '19524') {           //Edit Event only for Active & In-complete status
			document.frmEventRpt.action = '/gmEventSetup.do?method=addEvent&strOpt=edit&caseInfoID='+strCaseInfoID+"&backScreen=setupscreen";
		}else{
			Error_Details(message_sales[222]);	
		}
	}else if(val == '1006601'){      //Reschedule Event
		if (accessFl == 'Y'){
			Error_Details(message_sales[223]);
		}else{
			document.frmEventRpt.action = '/gmEventSetup.do?method=rescheduleEvent&strOpt=reschedule&caseInfoID='+strCaseInfoID;
		}
	}else if(val == '1006602') {     //Cancel Event
		if (accessFl == 'Y')
		{
			Error_Details(message_sales[224]);
		}else{
		document.frmEventRpt.action ="/GmCommonCancelServlet?hCancelType=CANEVE&hTxnId="+strCaseInfoID+"&hTxnName="+eventID;
		}
	}else if(val == '1006603'){      //Add as Favorites
		if ( statusId == '19534') {  //incomplete event should not add as Favorites
			Error_Details(message_sales[225]);	
		}
		document.frmEventRpt.action ="/gmFavouriteCase.do?strOpt=eventfav&caseID="+eventName+"&caseInfoID="+strCaseInfoID;		
	}else if(val == '1006604'){      //Edit Request
		if (accessFl == 'Y')
		{
			Error_Details(message_sales[226]);
		}else if ( statusId == '19534' ) { 
			Error_Details(message_sales[227]);			
		}else{
			document.frmEventRpt.action ='/gmOprLoanerReqEdit.do?txnType=4119&requestId='+strRequestID;	
		}
	}else if(val == '1006605'){      //Download pdf		
		document.frmEventRpt.action ='/gmEventReport.do?method=downloadSummaryAsPDF&strOpt=PDF&caseInfoID='+strCaseInfoID;		
	}else if(val == '1006619'){ // Add/Modify Inventory		
		if (accessFl == 'Y'){
			Error_Details(message_sales[228]);
		}else{
			arryCategories = selectedCategories.split(",");
			arryCategories.sort();
			var strFwdProductType = arryCategories[0];	
			if(strFwdProductType != ''){
				if(strFwdProductType != '19533'){
					document.frmEventRpt.action = "/gmCaseBookSetDtls.do?strOpt=editEvent&fromscreen=eventsetup&backScreen=summaryscreen&caseInfoID="+strCaseInfoID+"&caseID=&fwdProductType="+strFwdProductType+"&categoryList="+selectedCategories;
				}else{
					document.frmEventRpt.action ="/gmEventItemDtl.do?strOpt=load&fromscreen=eventsetup&backScreen=summaryscreen&caseInfoID="+strCaseInfoID+"&categoryList="+selectedCategories+"&productionType="+strFwdProductType;
				}
			}else{
				document.frmEventRpt.action = '/gmEventSetup.do?method=addEvent&strOpt=edit&caseInfoID='+strCaseInfoID+"&backScreen=setupscreen";
			}
		}
	
	}else {
		Error_Details(message[588]);	
	}
	if(ErrorCount>0)
	{
		Error_Show();
		Error_Clear();
		return;
	}
	
	document.frmEventRpt.submit();
	return false;
}


