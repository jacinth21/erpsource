//this function used to load the grid.

function initGridData(divRef,gridData)
{
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);
	gObj.enableTooltips("true,true,true");	
	gObj.init();	
	gObj.loadXMLString(gridData);	
	return gObj;	
}
function fnOnPageLoad(){
	gridObj = initGridData('dataGrid',objGridData);
}

//this function is used to add the gird rows
function addRow(){
	gridObj.addRow(gridObj.getUID(),'');
}

function fnOpenPart()
{
	pnum = document.frmEventItemDtl.Txt_PartNum.value;
	var strArray = pnum.split(',');	
	if(strArray.length > 1){
		Error_Details(message_sales[40]);
		Error_Show();
		Error_Clear();
		return false;
	}else{		 	
		windowOpener("/GmSearchServlet?hAction=SearchPart&Txt_PartNum="+encodeURIComponent(pnum),"PartLookUp","resizable=yes,scrollbars=yes,top=150,left=150,width=750,height=500");    	
	}
}

function fnAddtoCart(){
	var gridrows = gridObj.getAllRowIds(","); 	
	var pnum = document.frmEventItemDtl.Txt_PartNum.value;	
	if(pnum == ''){
		Error_Details(message_sales[41]);
		Error_Show();
		Error_Clear();
		return false;
	}		
	dhtmlxAjax.get("/GmCartAjaxServlet?pnum=" + encodeURIComponent(pnum)+"&acc=0&rep=0&gpoid=0&ramdomId="+Math.random()+"&"+fnAppendCompanyInfo(),fnCallBack);
}

function fnCallBack(loader){
	var xmlDoc =loader.xmlDoc.responseXML;
	var pnum = xmlDoc.getElementsByTagName("pnum");
	var data = xmlDoc.getElementsByTagName("data");
	var datalength = data[0].childNodes.length;
	var pnumlen = pnum.length;
	var dupliPartNumFl = false;
	if(datalength == 0){		
		Error_Details(message_sales[42]);
		Error_Show();
		Error_Clear();
		return false;
	}
	for (var x=0; x<pnumlen; x++)  /*PMT-38061*/
	{
		/*Data get swapped in GmXMLParserUtility.createXMLString arraylist
		 * get the partnum from getElementsByTagName()
		 * */
			var part = parseXmlNode(xmlDoc.getElementsByTagName("partnum")[0].firstChild)
			var pdesc = parseXmlNode(xmlDoc.getElementsByTagName("pdesc")[0].firstChild) 
		var gridrows = gridObj.getAllRowIds();
		var gridrowsarr = gridrows.split(",");		
		var arrLen = gridrowsarr.length;
		for (var i=0;i<arrLen;i++){
			PartNum = '';
	        rowId = gridrowsarr[i];		       
	        if (gridObj.getRowIndex(rowId) != -1){
		       PartNum  = gridObj.cellById(rowId,0).getValue();
	        }
	        if(part == PartNum ){
		       dupliPartNumFl = true;		        		        	
		    }        
		}
		if(dupliPartNumFl){
			Error_Details(Error_Details_Trans(message_sales[43],part));
    		Error_Show();
    		Error_Clear();
    		return false;  		
		}else{
			rowID = gridObj.getUID();
		 	gridObj.addRow(rowID,'');
        	gridObj.cellById(rowID, 0).setValue(part);
    		gridObj.cellById(rowID, 1).setValue(pdesc);
    		gridObj.cellById(rowID, 2).setValue(1);
        }	   		
	}
}
function fnSubmit(){	
	var screen = document.frmEventItemDtl.fromscreen.value;
	var inputString = '';
	var partString  = '';
	if(gridObj!= null){		
		var gridrows = gridObj.getAllRowIds();
		if(gridrows != undefined){
			var gridrowsarr = gridrows.toString().split(",");					
			var arrLen = gridrowsarr.length;				
			for (var i=0;i<arrLen;i++){
		        rowId = gridrowsarr[i];		       
		        if (gridObj.getRowIndex(rowId) != -1){
			        PartNum  = gridObj.cellById(rowId,0).getValue();		
			        partDesc = gridObj.cellById(rowId,1).getValue(); 	
			        reqQty   = gridObj.cellById(rowId,2).getValue();       		       
			        partString = partString + PartNum + ',';
			        if(isNaN(reqQty) || reqQty == '' ||  reqQty <= 0){
						Error_Details(Error_Details_Trans(message_sales[44],PartNum));
			    		Error_Show();
			    		Error_Clear();
			    		return false;  
					}
					inputString = inputString + PartNum + ',' + reqQty + ',|';					
		        }
			}		
		}		
		document.frmEventItemDtl.partNumInputString.value = partString;
		document.frmEventItemDtl.inputString.value = inputString;
		document.frmEventItemDtl.strOpt.value =  (screen == "eventsetup")?'eventNextConfirm':'nextConfirm';
		document.frmEventItemDtl.submit();
	}	
}

function removeSelectedRow(){
      var gridrows = gridObj.getSelectedRowId();   
      if(gridrows!=undefined){
          var NameCellValue ;
          var gridrowsarr = gridrows.toString().split(",");
          var added_row;
          for (var rowid = 0; rowid < gridrowsarr.length; rowid++){
             gridrowid = gridrowsarr[rowid];                 
             gridObj.deleteRow(gridrowid);                                
          }
      }else{
         Error_Clear();
         Error_Details(message_sales[45]);                       
         Error_Show();
         Error_Clear();
      }
}
function fnBack()
{    
	
	 var vCatList =  document.frmEventItemDtl.categoryList.value ;
	 var prodType = document.frmEventItemDtl.productionType.value;
	 var screen = document.frmEventItemDtl.fromscreen.value;
	 var caseInfoID = document.frmEventItemDtl.caseInfoID.value;
	 var backscreen = document.frmEventItemDtl.backScreen.value; 
	 var fwdProdType = "";
	 
	 arrayCat = vCatList.split(",");
	 arrayCat.sort(sortfunc);
	 for(var i=arrayCat.length-1;i>=0;i--){
		 if(eval(prodType)> arrayCat[i]){
			 fwdProdType = arrayCat[i];
			 break;
		 }
	 }
	 if(fwdProdType == ""){
		 if(screen == "eventsetup"){
			 if(backscreen == 'summaryscreen'){
				 document.frmEventItemDtl.action = "/gmEventReport.do?method=downloadSummaryAsPDF&strOpt=HTML&caseInfoID="+caseInfoID+"&eventRequestedMeterial="+vCatList;
			 }else{
				 document.frmEventItemDtl.action = "/gmEventSetup.do?method=addEvent&strOpt=edit&caseInfoID="+caseInfoID;
			 }			
		 }else{
			 document.frmCaseBookSetDtls.action = "gmCaseBuilder.do?strOpt=edit&caseInfoID="+caseInfoID; 
		 }
	 }else{
		 document.frmEventItemDtl.fwdProductType.value = fwdProdType;
		 document.frmEventItemDtl.strOpt.value = (screen == "eventsetup")?'editEvent':'edit';
		 document.frmEventItemDtl.action="gmCaseBookSetDtls.do?";
	 }
	 document.frmEventItemDtl.submit();
}
function sortfunc(a,b)
{
return a - b;
}
