
function Toggle(node)
{
	// Unfold the branch if it isn't visible
	if (node.nextSibling.style.display == 'none')
	{
		// Change the image (if there is an image)
		if (node.children.length > 0)
		{
			if (node.children.item(0).tagName == "IMG")
			{
				node.children.item(0).src = "/Images/minus.gif";
			}
		}

		node.nextSibling.style.display = '';
	}
	// Collapse the branch if it IS visible
	else
	{
		// Change the image (if there is an image)
		if (node.children.length > 0)
		{
			if (node.children.item(0).tagName == "IMG")
			{
				node.children.item(0).src = "/Images/plus.gif";
			}
		}

		node.nextSibling.style.display = 'none';
	}

}

// To Expand the node
function ExpandAll(node)
{
	// Unfold the branch if it isn't visible
	if (node.nextSibling.style.display == 'none')
	{
		// Change the image (if there is an image)
		if (node.children.length > 0)
		{
			if (node.children.item(0).tagName == "IMG")
			{
				node.children.item(0).src = "/Images/minus.gif";
			}
		}

		node.nextSibling.style.display = '';
	}
}


// To collapse the menu
function CollapseAll(node)
{
		// Change the image (if there is an image)
		if (node.children.length > 0)
		{
			if (node.children.item(0).tagName == "IMG")
			{
				node.children.item(0).src = "/Images/plus.gif";
			}
		}

		node.nextSibling.style.display = 'none';
}
// To Expand the node
function ExpandP(node)
{
	// Unfold the branch if it isn't visible
	if (node.nextSibling.style.display == 'none')
	{
		// Change the image (if there is an image)
		if (node.children.length > 0)
		{
			if (node.children.item(0).tagName == "IMG")
			{
				node.children.item(0).src = "/Images/blank.gif";
			}
		}

		node.nextSibling.style.display = '';
	}
}


// To collapse the menu
function CollapseP(node)
{
		// Change the image (if there is an image)
		if (node.children.length > 0)
		{
			if (node.children.item(0).tagName == "IMG")
			{
				node.children.item(0).src = "/Images/blank.gif";
			}
		}

		node.nextSibling.style.display = 'none';
}

function CollapseTL(node)
{
	node.nextSibling.style.display = 'none';
}

