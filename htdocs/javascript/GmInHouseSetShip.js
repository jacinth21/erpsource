function fnCallValues(obj)
{
	loanval = document.frmVendor.Cbo_ValuesCon.value;
	objid = "Con";
	j=0;
	//obj = document.frmVendor.Cbo_ShipTo;
	distid=0;
	repid=0;
	accid=0;
	var obj2 = eval("document.frmVendor.Cbo_ValuesCon");
	var obj3 = eval("document.frmVendor.Cbo_LoanToRep");
	var obj4 = eval("document.frmVendor.Cbo_LoanToAcc");

	if (obj.options[obj.selectedIndex].text == 'Distributor')
	{
		
		obj2.disabled = false;
		obj2.options.length = 0;
		obj2.options[0] = new Option("[Choose One]","0");
		for (var i=0;i<DistLen;i++)
		{
			arr = eval("DistArr"+i);
			arrobj = arr.split(",");
			id = arrobj[0];
			name = arrobj[1];
			obj2.options[i+1] = new Option(name,id);
		}
		 
		obj2.focus();
	}
	else if (obj.options[obj.selectedIndex].text == 'Sales Rep')
	{
	 
		obj2.disabled = false;
		obj2.options.length = 0;
		obj2.options[0] = new Option("[Choose One]","0");
		for (var i=0;i<RepLen;i++)
		{
			arr = eval("RepArr"+i);
			arrobj = arr.split(",");
			value = arrobj[0]; //repid
			name = arrobj[1]; 
			id=arrobj[2]; //partyid	
			names.options[i]= new Option(name,value);
			names.options[i].id=id; 
		}
		 
		obj2.focus();
	}

	else if (obj.options[obj.selectedIndex].text == 'Hospital')
	{	
		obj3.value = 0;
		obj3.disabled = true;
		obj4.value = 0;
		obj4.disabled = true;

		obj2.disabled = false;
		obj2.options.length = 0;
		obj2.options[0] = new Option("[Choose One]","0");
		for (var i=0;i<AccLen;i++)
		{
			arr = eval("AccArr"+i);
			arrobj = arr.split(",");
			id = arrobj[0];
			name = arrobj[1];
			did = arrobj[2];
			if (objid == 'Con')
			{
				obj2.options[i+1] = new Option(name,id);
			}
			else
			{
			//	if (did == loanval)
			//	{
					j++;
					obj2.options[j] = new Option(name,id);
			//	}
			}			
		}

		obj2.focus();

	}
	else if (obj.options[obj.selectedIndex].text == 'Employee')
	{
		obj3.value = 0;
		obj3.disabled = true;
		obj4.value = 0;
		obj4.disabled = true;
		
		obj2.disabled = false;
		obj2.options.length = 0;
		obj2.options[0] = new Option("[Choose One]","0");
		for (var i=0;i<EmpLen;i++)
		{
			arr = eval("EmpArr"+i);
			arrobj = arr.split(",");
			id = arrobj[0];
			name = arrobj[1];
			obj2.options[i+1] = new Option(name,id);
		}
		obj2.focus();
	}
	else
	{
		obj2.options.length = 0;
		obj2.options[0] = new Option("[Choose One]","0");
		obj2.disabled = true;
	}
}

function fnSubmit()
{
   if(document.frmVendor.Cbo_LoanTo.value == "0" || document.frmVendor.Cbo_ValuesCon.value == "0" )
      {
         Error_Details("The Following Fields are Mandatory <br> Loan To, Values ");
      }

	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	
	document.frmVendor.hAction.value = 'ShipOut';
	fnStartProgress('Y');
	document.frmVendor.submit();
}


function fnSelectPart(obj)
{
	
	reqdetobj = eval("document.frmVendor.reqdetid"+obj.value);
	document.frmVendor.hReqId.value = reqdetobj.value;
	
	loanobj = eval("document.frmVendor.shipto"+obj.value);
	conobj = eval("document.frmVendor.reqforid"+obj.value);	
	shipobj = eval("document.frmVendor.shiptoid"+obj.value);
	carrobj = eval("document.frmVendor.shipcarr"+obj.value);
	modeobj = eval("document.frmVendor.shipmode"+obj.value);
	addressobj = eval("document.frmVendor.shipaddid"+obj.value);

	document.frmVendor.Cbo_LoanTo.value=50170;
	document.all.shipTo.value=loanobj.value;
	fnCallValues(document.frmVendor.Cbo_LoanTo);
	document.frmVendor.Cbo_ValuesCon.value=conobj.value;
	
	fnGetNames(document.all.shipTo);
	if (shipobj.value != '')
	{
		document.all.names.value=shipobj.value;
	}
	if (document.all.shipTo.value == 4121) //sales rep
	{
		fnGetAddressList(document.all.names,addressobj.value);
	}
	if (carrobj.value != '')
	{
			document.all.shipCarrier.value=carrobj.value;
	}
	if (modeobj.value != '')
	{
		document.all.shipMode.value=modeobj.value;
	}	 
	fnSetRepValues(document.frmVendor.Cbo_LoanToRep);
	fnSetAccValues(document.frmVendor.Cbo_LoanToAcc);
	if (document.all.shipTo.value == 4121) //sales rep
	{		
		document.frmVendor.Cbo_LoanToRep.value=shipobj.value ;
	}
	if (document.all.shipTo.value == 4122) {// hospital
		document.frmVendor.Cbo_LoanToAcc.value  = shipobj.value;
	}
}


function fnShow(obj)
{
	obj=document.all.filter;

	if (obj.style.display == 'none')
	{
		obj.style.display = 'block';
	}
	else
	{
		obj.style.display = 'none';
	}
}
		
function fnSetDistValues()
{	
	obj = document.frmVendor.Cbo_LoanTo;
	if (obj.options[obj.selectedIndex].text == 'Distributor')

	{	
		distid = document.all.Cbo_ValuesCon.value;
		var repobj = document.frmVendor.Cbo_LoanToRep;
		fnSetRepValues(repobj);
		var accobj = document.frmVendor.Cbo_LoanToAcc;
		fnSetAccValues(accobj);
	}
	if (obj.options[obj.selectedIndex].text == 'Hospital')
	{
		accid = document.all.Cbo_ValuesCon.value;
	}
}
		
function fnSetRepValues(obj)
{
	j = 0;
	var loanobjval = document.frmVendor.Cbo_ValuesCon.value;

	obj.disabled = false;
	obj.options.length = 0;
	obj.options[0] = new Option("[Choose One]","0");	
	for (var i=0;i<RepLen;i++)
	{
		arr = eval("RepArr"+i);
		arrobj = arr.split(",");
		id = arrobj[0];
		
		name = arrobj[1];
		did = arrobj[3];
		if (did == loanobjval)

		{
			j++;			
			obj.options[j] = new Option(name,id);

		}
	}
}

function fnSetAccValues(obj)
{
	j = 0;
	var loanobjval = document.frmVendor.Cbo_ValuesCon.value;

	obj.disabled = false;
	obj.options.length = 0;
	obj.options[0] = new Option("[Choose One]","0");
	for (var i=0;i<AccLen;i++)
	{
		arr = eval("AccArr"+i);
		arrobj = arr.split(",");
		id = arrobj[0];
		name = arrobj[1];
		did = arrobj[2];
		if (did == loanobjval)
		{
			j++;
			obj.options[j] = new Option(name,id);

		}
	}
}

function fnOnLoad(){
	document.all.shipCarrier.value= 5001; //defaulted to fedex
	document.all.shipMode.value = 5004; // defaulted to OverNight
}
function fnSetRepValue()
{
	repid = document.all.Cbo_LoanToRep.value;
}
function fnSetAccValue()
{
	accid = document.all.Cbo_LoanToAcc.value;
}