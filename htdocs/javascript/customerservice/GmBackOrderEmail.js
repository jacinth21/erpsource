//Click on load button
function fnLoad(){
	var distType = document.frmSalesBackOrderMail.distType.value;
	var emailType = document.frmSalesBackOrderMail.emailType.value;
		
	objStartDt = document.frmSalesBackOrderMail.strFromDate;
	objEndDt = document.frmSalesBackOrderMail.strToDate;
	
	var fromToDiff = dateDiff(objStartDt.value, objEndDt.value, dateFmt);
	var fromCurrDiff = dateDiff(objStartDt.value, todaysDate, dateFmt);
	var toCurrDiff  = dateDiff(objEndDt.value, todaysDate, dateFmt);
	
	if(objStartDt.value != "" && objEndDt.value == ""){
		Error_Details(message[5533]);
	}
	if(objStartDt.value == "" && objEndDt.value != ""){
		Error_Details(message[5534]);
	}
	
	if(objStartDt.value != ""){
		CommonDateValidation(objStartDt,dateFmt,Error_Details_Trans(message[5535],dateFmt));
	}
	if(objEndDt.value != ""){
		CommonDateValidation(objEndDt,dateFmt,Error_Details_Trans(message[5536],dateFmt));
	}	
	
	if(fromToDiff < 0){
		Error_Details(message[5537]);
	}
	if(fromCurrDiff < 0){
		Error_Details(message[5538]);
	}
	if(toCurrDiff < 0){
		Error_Details(message[5539]);
	}
	
	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}

	document.frmSalesBackOrderMail.strOpt.value = "load";
	fnStartProgress();
	document.frmSalesBackOrderMail.submit();
}

function Toggle(val){
	var obj = eval("document.all.div"+val);	
	var trobj = eval("document.all.tr"+val);
	var tabobj = eval("document.all.tab"+val);

	if (obj.style.display == 'none')	{
		obj.style.display = '';
		trobj.className= "ShadeRightTableCaptionBlue";
		tabobj.style.background="#ecf6fe";
	}else{
		obj.style.display = 'none';
		trobj.className= "";
		tabobj.style.background="#ffffff";
	}
}

//When click on Master check box
function fnSelectAll(obj){

	var chkLength =0;
	var repId, objRepChk;
	var objRepChkLen = 0;
	objChkAll = document.all.divSelectAll;
	if(objChkAll != undefined)
		chkLength = objChkAll.length;
	if(chkLength == undefined && obj.checked == true && objChkAll){// If only one sales rep is there and check box is checked
		objChkAll.checked = true;
		repId = objChkAll.id;
		objRepChk = eval("document.all.divSelectOrder"+repId);
		if(objRepChk != undefined)
			objRepChkLen = objRepChk.length;
		if(objRepChk != undefined && objRepChkLen!= undefined){
			for(var j=0; j<objRepChk.length;j++){
				objRepChk[j].checked = true;
			}
		}
		else{
			objRepChk.checked = true;
		}
	}else if(chkLength == undefined && obj.checked == false && objChkAll){// If only one sales rep is there and check box is not checked
		objChkAll.checked = false;
		repId = objChkAll.id;
		objRepChk = eval("document.all.divSelectOrder"+repId);
		if(objRepChk != undefined)
			objRepChkLen = objRepChk.length;
		if(objRepChk != undefined && objRepChkLen!= undefined){
			for(var j=0; j<objRepChk.length;j++){
				objRepChk[j].checked = false;
			}
		}
		else{
			objRepChk.checked = false;
		}
	}else if(obj.checked == true){// If more sales reps are there and check box is checked
		for(i =0 ;i< chkLength ; i++){
			objChkAll[i].checked = true;
			repId = objChkAll[i].id;
			
			objRepChk = eval("document.all.divSelectOrder"+repId);
			if(objRepChk != undefined)
				objRepChkLen = objRepChk.length;
			if(objRepChk != undefined && objRepChkLen!= undefined){
				for(var j=0; j<objRepChk.length;j++){
					objRepChk[j].checked = true;
				}
			}
			else{
				objRepChk.checked = true;
			}
		}
	}else {// If more sales reps are there and check box is not checked
		for(i =0 ;i< chkLength ; i++){
			objChkAll[i].checked = false;
			repId = objChkAll[i].id;
			objRepChk = eval("document.all.divSelectOrder"+repId);
			if(objRepChk != undefined)
				objRepChkLen = objRepChk.length;
			if(objRepChk != undefined && objRepChkLen!= undefined){
				for(var j=0; j<objRepChk.length;j++){
					objRepChk[j].checked = false;
				}
			}	
			else{
				objRepChk.checked = false;
			}
		}
	}		
}

// When click on checkbox corresponding to the rep ids
function fnSelectAllSub(val, obj){
	var chkLength =0;
	objChkAll = eval("document.all.divSelectOrder"+val);
	if(objChkAll != undefined)
		chkLength = objChkAll.length;
	if(chkLength == undefined && obj.checked == true && objChkAll){ // If only one record is there and check box is checked
		objChkAll.checked = true;
	}else if(chkLength == undefined && obj.checked == false && objChkAll){// If only one record is there and check box is not checked
		objChkAll.checked = false;
	}else if(obj.checked == true){// If more records are there and check box is checked
		for(i =0 ;i< chkLength ; i++){
			objChkAll[i].checked = true;
		}
	}else {// If more records are there and check box is not checked
		for(i =0 ;i< chkLength ; i++){
			objChkAll[i].checked = false;
		}
	}	
}

//When click on Send Email button
function fnSubmit(){
	var salesRepIds = document.frmSalesBackOrderMail.hSalesRepIDs.value;
	var arrSalesRepId = new Array();
	arrSalesRepId = salesRepIds.split(',');
	var salesArrayLen = arrSalesRepId.length;
	var salesrep;
	var repCheck;
	var objChkOrder, backOrderId;
	var chkLength = 0;
	var inputStr = ''; 
	var backOrdStr = '';
	for(var i=0;i<salesArrayLen-1;i++){
		salesrep = arrSalesRepId[i];
		repCheck = document.getElementById(salesrep).checked;

			objChkOrder = eval("document.all.divSelectOrder"+salesrep);
			if(objChkOrder != undefined)
				chkLength = objChkOrder.length;
			
			if(objChkOrder != undefined && chkLength == undefined){ // if only one record is there
				if(objChkOrder.checked){
					backOrderId = objChkOrder.id;
					backOrdStr = backOrdStr + backOrderId+',';
				}
			}else{// for more records
				for(var j=0; j<chkLength;j++){
					
					if(objChkOrder[j].checked){
						backOrderId = objChkOrder[j].id;
						backOrdStr = backOrdStr + backOrderId+',';
					}
				}
			}
			
			if(backOrdStr != ''){
				backOrdStr = backOrdStr.substring(0,backOrdStr.length-1);
				inputStr = inputStr+salesrep+'^'+backOrdStr+'|';
				backOrdStr = '';
			}
			
	}
	
	if(inputStr == ''){
		Error_Details(message[5540]);
	}
	
	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}
	
	document.frmSalesBackOrderMail.hInputString.value = inputStr;
	document.frmSalesBackOrderMail.strOpt.value = 'save';
	fnStartProgress();
	document.frmSalesBackOrderMail.submit();
	
}

function fnReqHistory(attrid){// Click on History icon
	windowOpener("/GmPageControllerServlet?strPgToLoad=/gmAuditTrail.do&auditId=4000764&txnId="+ attrid,"","resizable=yes,scrollbars=yes,top=250,left=300,width=785,height=300,status=1");
}

// when selecting the BO checkbox, should check the sales rep checkbox automatically
function fnSelectMasterChkBox(obj, repId){
	var chkLength, objChkOrder;
	var uncheckCnt = 0;
	var checkCnt = 0;
	document.getElementById(repId).checked = false;
	
	objChkOrder = eval("document.all.divSelectOrder"+repId);
	if(objChkOrder != undefined)
		chkLength = objChkOrder.length;
	if(objChkOrder != undefined && chkLength != undefined){
		for(var j=0; j<chkLength;j++){
			if(objChkOrder[j].checked){
				checkCnt++;
			}else{
				uncheckCnt++;
			}
		}			
		if(chkLength == uncheckCnt){
			document.getElementById(repId).checked = false;
		}
		if(chkLength == checkCnt){
			document.getElementById(repId).checked = true;
		}
	}else if(objChkOrder != undefined && chkLength == undefined){
		if(objChkOrder.checked){
			return;
		}else{
			document.getElementById(repId).checked = false;
		}
	}
}