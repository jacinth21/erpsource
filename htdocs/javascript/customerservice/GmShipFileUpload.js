var confirmMsg;
//When click confirm message Y , it will be overwrite the file in that folder. 
function fnUpload(){
	fnStartProgress('Y');
	 var strRefId = document.frmShipFileUpload.strRefID.value;
	 var strRefType = document.frmShipFileUpload.refType.value;
	 document.frmShipFileUpload.action = '/gmShipFileUploadAction.do?method=uploadFiles&strOpt=Upload&strRefID='+strRefId;
	 document.frmShipFileUpload.Submit.disabled = true;
	 document.frmShipFileUpload.submit();
}

//When click upload button this function will be called and check file having DB. If it is there give flag value Y.
function fnValidateUpload(){
	var uploadFile = document.frmShipFileUpload.file.value;
	var strRefId = document.frmShipFileUpload.strRefID.value;	
	if(uploadFile != ''){
		fnStartProgress('Y');
		document.frmShipFileUpload.Submit.disabled = true;
		dhtmlxAjax.get('/gmShipFileUploadAction.do?method=uploadFiles&fileName='+encodeURIComponent(uploadFile)+'&refType=4000715&strRefID='+strRefId+'&strOpt=Validate&ramdomId='+Math.random(),fnValideFile);
	}else{ 
	      Error_Details(message[5530]);
	     if(ErrorCount > 0){
	  		Error_Show();
	  	    Error_Clear();
	  		return false;
	  	}
	}
}
//Once ajax called following function will be called.
function fnValideFile(loader){
	var response = loader.xmlDoc.responseText;
	if(response == 'Y')	
		confirmMsg = fnUploadConfirmMessage();
	else
		fnUpload();
	
	if(confirmMsg){
		fnUpload();
	}else {
		fnStopProgress();
	}
	document.frmShipFileUpload.Submit.disabled = false;
}

//It will ask message when upload the file if we have file in the folder.
function fnUploadConfirmMessage(){
	confirmMsg = confirm(message[5531]);
	if(confirmMsg)	return true;
	else 			return false;
}

function fnValidateTransaction(obj){
	var transId = obj.value;
	if(transId != ''){
		dhtmlxAjax.get('/gmShipFileUploadAction.do?method=attachFiles&strOpt=validateTrans&strRefID='+ transId + '&ramdomId=' + Math.random(), fnShowImage);
		return false;
	}	
}

function fnShowImage(loader){
	var response = loader.xmlDoc.responseText;
	if(response != null){
		if(response == 0){		
			document.getElementById("DivErrorMessage").style.display = 'block';
			document.frmShipFileUpload.Submit.disabled = true;
			if(document.getElementById("DivShowFileUploadSuccess")){
				document.getElementById("DivShowFileUploadSuccess").style.display = 'none';
			}
		}else{		
			document.getElementById("DivErrorMessage").style.display = 'none';
			document.frmShipFileUpload.Submit.disabled = false;
		}
	}
}

function fnOnLoad(){
	if(message == ''){
		document.frmShipFileUpload.Submit.disabled = true;
	}	
	document.getElementById("DivErrorMessage").style.display = 'none';			
}
