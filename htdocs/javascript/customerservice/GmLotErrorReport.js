//GmLotErrorReport.js
var gridObj;
var gObj;

function fnExcel(){ 
	gridObj.toExcel('/phpapp/excel/generate.php');
}

//This function will call on page load. disable other fields when the status is Resolved for PMT-29681
function fnOnLoad(){
	 var status = document.frmLotErrorReport.status.value;
	 if(status == "Resolved"){
	 document.getElementById("resolutionType").disabled='true';
	 document.getElementById("new_lot_tex_tbox").disabled='true';
	 document.getElementById("comments").disabled='true'; 
	 document.getElementById("comments").style.color = 'dimgray';
	 document.getElementById("comments").value=comments;
	
	 }
	
}
//PMT-29681 Load Lot Error Details
function fnLoadLotDetails(strErrorInfoID){
   windowOpener('/gmLotErrorReport.do?method=fetchLotErrorDetail&strErrorInfoID='+ strErrorInfoID+'&companyInfo='+companyInfoObj, "Search","resizable=yes,scrollbars=yes,top=200,left=250,width=1050,height=670");
}
// PMT-29681 To Disable dropdown
function fnOnChange() {
	
	var resolutionType = document.frmLotErrorReport.resolutionType.value;
	if(resolutionType==107960 || resolutionType==107962){
		document.getElementById("new_lot_tex_tbox").disabled='true';
		document.getElementById("new_lot_tex_tbox").value='';
		document.getElementById("new_lot_tex_tbox").disabled='true';
	}
	else {
		document.getElementById("new_lot_tex_tbox").disabled='';
	}
	
}
//PMT-29681 To Load order summary
function fnLoadSummary() {
	var transactionId = document.frmLotErrorReport.transactionId.value;
	var transType = document.frmLotErrorReport.transactionType.value;
	var frmObj=document.frmLotErrorReport;
	fnStartProgress();
	if(transType == 'Order'){	
		frmObj.action ='/GmEditOrderServlet?hMode=PrintPrice&hOrdId='+transactionId+'&companyInfo='+companyInfoObj;
		frmObj.submit();
		}else if(transType == 'Return'){
			frmObj.action ='/GmPrintCreditServlet?hAction=PRINT&hRAID='+transactionId+'&companyInfo='+companyInfoObj;
			frmObj.submit();
		}
}

function fnLoadSum() {
	
	var errorMessage = document.frmLotErrorReport.errorMessge.value;
	var errMsgStr = errorMessage.indexOf("order")+5;
	var transId = errorMessage.substring(errMsgStr, errorMessage.length+1);
	fnStartProgress();
	var frmObj=document.frmLotErrorReport;
	frmObj.action ='/GmEditOrderServlet?hMode=PrintPrice&hOrdId='+transId+'&companyInfo='+companyInfoObj;
	frmObj.submit();
}
//PMT-29681 To Save Lot Error Details
function fnSavLotDetails() {
	
	var resolutionType = document.frmLotErrorReport.resolutionType.value;
	var updLotNumber = document.getElementById("new_lot_tex_tbox").value;
	var partNumber = document.frmLotErrorReport.partNumber.value;
	var lotNumber = document.frmLotErrorReport.lotNumber.value;
	var comments = document.frmLotErrorReport.comments.value;
	var strErrorInfoID = document.frmLotErrorReport.strErrorInfoID.value;
	var errortype = document.frmLotErrorReport.errortype.value;

	
	if((resolutionType==107960 || resolutionType==107962) && errortype==107972) {
		Error_Details('New Lot Number is required to resolve Invalid Lot Error Type');
	       if (ErrorCount > 0) {
	   		Error_Show();
	   		Error_Clear();
	   		return false;
	   		}
		}
	if((resolutionType==107960 || resolutionType==107962) && errortype==107974) {
		Error_Details('New Lot Number is required to resolve Invalid Inventory Error Type');
	       if (ErrorCount > 0) {
	   		Error_Show();
	   		Error_Clear();
	   		return false;
	   		}
		}
	if(resolutionType == '0') {
		Error_Details('Please select a Resolution Type to proceed');
	       if (ErrorCount > 0) {
	   		Error_Show();
	   		Error_Clear();
	   		return false;
	   		}
	}
	
	//if resolution type is proceed with same lot
	if(resolutionType==107960)
		{
		if(comments == "") {
			Error_Details('Please enter mandatory Comments to proceed');
		       if (ErrorCount > 0) {
		   		Error_Show();
		   		Error_Clear();
		   		return false;
		   		}
		}
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmLotErrorReport.do?method=saveExistLot&partNumber='
				+ encodeURIComponent(partNumber)
				+ '&lotNumber='
				+ lotNumber
				+ '&comments='
				+ comments
				+ '&strErrorInfoID='
				+ strErrorInfoID
				+ '&resolutionType='
				+ resolutionType
				+ '&ramdomId='
				+ new Date().getTime());
		dhtmlxAjax.get(ajaxUrl, fnLoadLotMessageAjax);
		}
	//if resolution type is proceed with new lot
	if(resolutionType==107961)
		{
		if(updLotNumber == "" && comments == "") {
				Error_Details('Please enter New Lot number and Comments to proceed');
		       if (ErrorCount > 0) {
		   		Error_Show();
		   		Error_Clear();
		   		return false;
		   		}
		}
		if(comments == "") {
			Error_Details('Please enter mandatory Comments to proceed');
	       if (ErrorCount > 0) {
	   		Error_Show();
	   		Error_Clear();
	   		return false;
	   		}
	}
		if(updLotNumber == "") {
			Error_Details('Please enter New Lot number to proceed');
	       if (ErrorCount > 0) {
	   		Error_Show();
	   		Error_Clear();
	   		return false;
	   		}
	}
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmLotErrorReport.do?method=saveNewLot&partNumber='
				+ encodeURIComponent(partNumber)
				+ '&updLotNumber='
				+ updLotNumber
				+ '&comments='
				+ comments
				+ '&strErrorInfoID='
				+ strErrorInfoID
				+ '&resolutionType='
				+ resolutionType
				+ '&ramdomId='
				+ new Date().getTime());
		dhtmlxAjax.get(ajaxUrl, fnLoadLotMessageAjax);
			
		}
	//if resolution type is proceed with mark as resolved
	if(resolutionType==107962)
		{
		if(comments == "") {
			Error_Details('Please enter mandatory Comments to proceed');
		       if (ErrorCount > 0) {
		   		Error_Show();
		   		Error_Clear();
		   		return false;
		   		}
		}
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmLotErrorReport.do?method=saveAsResolved&partNumber='
				+ encodeURIComponent(partNumber)
				+ '&lotNumber='
				+ lotNumber
				+ '&comments='
				+ comments
				+ '&strErrorInfoID='
				+ strErrorInfoID
				+ '&resolutionType='
				+ resolutionType
				+ '&ramdomId='
				+ new Date().getTime());
		dhtmlxAjax.get(ajaxUrl, fnLoadLotMessageAjax);
		}
}
// PMT-29681 To Show error message on screen
function fnLoadLotMessageAjax(loader) {
	var response = loader.xmlDoc.responseText;
	
	if(response != ''){
		document.getElementById("successCont").innerHTML = response;
				
	}
	else{
		document.getElementById("successCont").innerHTML = "<font color='green' style='font-weight:bold;'><center>Records Saved Successfully</center></font>";
		document.getElementById("resolutionType").disabled='true';
		document.getElementById("new_lot_tex_tbox").disabled='true';
		document.getElementById("comments").style.color='dimgray';
		document.getElementById("comments").disabled='true'; 
		document.getElementById("comments").style.backgroundColor="lightGrey";
		document.frmLotErrorReport.button_submit.disabled = true;
		document.frmLotErrorReport.button_reset.disabled = true;
		
	}

}
//PMT-29681 To load data on report
function fnLoadRpt(){
	var fieldSales = document.frmLotErrorReport.fieldSales.value;
	var transactionId = document.frmLotErrorReport.transactionId.value;
    var lotNumber = document.frmLotErrorReport.lotNumber.value;
    var partNumber = document.frmLotErrorReport.partNumber.value;
    var trasactionId = document.frmLotErrorReport.transactionId.value;
    var transactionType = document.frmLotErrorReport.transactionType.value;
    var transFromDate = document.frmLotErrorReport.transFromDate.value;
    var transToDate = document.frmLotErrorReport.transToDate.value;
    var status = document.frmLotErrorReport.status.value;
    var resolutionType = document.frmLotErrorReport.resolutionType.value;
    var resolutionFromDate = document.frmLotErrorReport.resolutionFromDate.value;
    var resolutionToDate = document.frmLotErrorReport.resolutionToDate.value;
    
    if(!transFromDate == '' || !transToDate == ''){
	var dateRange = dateDiff(transFromDate,transToDate,gCmpDateFmt);
	if(!isNaN(dateRange) && dateRange < 0){
		 Error_Details(message_operations[117]);
	  }
    }
	
    if(!resolutionFromDate == '' || !resolutionToDate == ''){
    	var dateRange = dateDiff(resolutionFromDate,resolutionToDate,gCmpDateFmt);
    	if(!isNaN(dateRange) && dateRange < 0){
    		 Error_Details(message_operations[117]);
    	  }
        }
	
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	// Using Ajax call for load the report
	var response;
	fnStartProgress();
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmLotErrorReport.do?method=fetchLotErrorReportInfo&fieldSales='+fieldSales+'&partNumber='+encodeURIComponent(partNumber)+'&lotNumber='+lotNumber+'&transactionId='+transactionId+'&transactionType='+transactionType+
			                             '&transFromDate='+transFromDate+'&transToDate='+transToDate+'&status='+status+'&resolutionType='+resolutionType+'&resolutionFromDate='+resolutionFromDate+'&resolutionToDate='+resolutionToDate+'&companyInfo='+companyInfoObj+'&ramdomId='+new Date().getTime());
	dhtmlxAjax.get(ajaxUrl,fnLoadLotAjax);
}


function fnLoadLotAjax (loader){
	var errorInfoId = '';
	var partNum = '';
	var partDesc = '';
	var lotNum = '';
	var transId = '';
	var transDate = '';
	var transtype = '';
	var comments = '';
	var resolvedby = '';
	var resolveddate = '';
	var updatecontnm = '';
	var errorType = '';
	var ertransid = '';
	var ertranstype = ''
	var errorfl = '';
	var resoltype ='';	
	var status = '';
	var fieldSales = '';
	var consignFS = '';
	var response = loader.xmlDoc.responseText;

  if(response != "") {
  //to form the grid string 
	var rows = [], i=0;
	var JSON_Array_obj = "";
	 JSON_Array_obj = JSON.parse(response);
	
	$.each(JSON_Array_obj, function(index,jsonObject){
		resoltype = jsonObject.RESOLTYPE;
		ertransid = jsonObject.ERTRANSID;   // ErrorMessge
		errorInfoId = jsonObject.ERRORINFOID;
		partNum = jsonObject.PARTNM;
		partDesc = jsonObject.PARTDESC;
		lotNum = jsonObject.LOTNM;
		transId = jsonObject.TRANSID;
		transtype = jsonObject.TRANSTYPE;
		transDate = jsonObject.TRANSDATE;
		errorType = jsonObject.ERRORTYPE;
		status = jsonObject.STATUS;
		resolvedby = jsonObject.RESOLVEDBY;
		resolveddate = jsonObject.RESOLVEDDATE;
		fieldSales = jsonObject.FIELDSALES;
		consignFS = jsonObject.CONSIGNEDFS;
		updatecontnm = jsonObject.UPDATEDLOT;
		comments = jsonObject.COMMENTS;
    	
		// default array
        rows[i] = {};
    //    rows[i].id= lotNum;
        rows[i].id=errorInfoId;
        rows[i].data = [];
        //
        rows[i].data[0] = "<a href='#' onclick=fnLoadLotDetails('"+errorInfoId+"')><img alt='Edit ID' src='/images/edit.jpg' border='0'></a>";
        rows[i].data[1] = errorInfoId;   // Error Info Id
        rows[i].data[2] = partNum;     // Part Number 
        rows[i].data[3] = partDesc;     // Part Description
        rows[i].data[4] = "<a href='#' style='color: #0000FF;' onclick=javascript:fnOpenLT('"+partNum+"','"+lotNum+"');>LT</a>";  // LT   
        rows[i].data[5] = "<a href='#' style='color: #0000FF;' onclick=javascript:fnOpenLC('"+lotNum+"');>LC</a>"; // LC
        rows[i].data[6] =  lotNum;   // Lot Number
        rows[i].data[7] = "<a href='#' style='color: #0000FF;' onclick= javascript:fnOpenComments('"+ transId +"','"+transtype+"')>"+transId+"</a>";   // Trans. ID
        rows[i].data[8] = transtype;   // Trans. Type
        rows[i].data[9] = transDate;   // Trans. Date 
        rows[i].data[10] = fieldSales;  // Field Sales
        rows[i].data[11] = consignFS;  // Consigned Field Sales
        rows[i].data[12] = ertransid// Error Message
        rows[i].data[13] = errorType;  // Error Type
        rows[i].data[14] = status;  // Status
        rows[i].data[15] = resolvedby; // Resolved By
        rows[i].data[16] = resolveddate; // Resolved Date
        rows[i].data[17] = updatecontnm;  // Update Lot
        rows[i].data[18] = comments;  // Comments
        	
        i++;
        
	});
	
	var setInitWidths = "50,100,80,100,30,30,80,100,100,80,160,140,*,100,100,80,100,100,120";
	
    var setColAlign = "left,left," +
    		          "left,left," +
    		          "center,center," +
    		          "left,left," +
    		          "left,left," +
    		          "left,left," +
    		          "left,left," +
    		          "left,left," +
    		          "left,center," +
    		          "left";
    var setColTypes = "ro,ro," +
    		          "ro,ro," +
    		          "ro,ro," +
    		          "ro,ro," +
    		          "ro,ro," +
    		          "ro,ro," +
    		          "ro,ro," +
    		          "ro,ro," +
    		          "ro,ro," +
    		          "ro";
    var setColSorting = "str,str," +
    		            "str,str," +
    		            "str,str," +
    		            "str,str," +
    		            "str,str," +
    		            "str,str," +
    		            "str,str," +
    		            "str,str," +
    		            "str,str," +
    		            "str";
   
    var setHeader = [ "","Error Info ID",
                      "Part #","Part Desc",
                      "","",
                      "Lot #","Trans.ID",
                      "Trans.Type","Trans. Date", 
                      "Field Sales","Consigned Field Sales",
                      "Error Message","Error Type",
                      "Status","Resolved By",
                      "Resolved Date","Updated Lot #",
                      "Comments"];
  
    var setFilter = ["","#text_filter",
                     "#text_filter","#text_filter",
                     "","",
                     "#text_filter","#text_filter",
                     "#text_filter","#text_filter",
                     "#text_filter","#text_filter",
                     "#text_filter","#text_filter",
                     "#text_filter","#text_filter",
                     "#text_filter","#text_filter",
                     "#text_filter"];
    var setColIds = "";
 
    var enableTooltips = "true,true," +
    		             "true,true," +
    		             "true,true," +
    		             "true,true," +
    		             "true,true," +
    		             "true,true," +
    		             "true,true," +
    		             "true,true," +
    		             "true,true," +
    		             "true";
    var footerArry = [];
    var gridHeight = "";
    var footerStyles = [];
    var footerExportFL = true;
    var dataHost = {};
    dataHost.rows = rows;
	document.getElementById("lotErrorReport").style.display = 'block';
	document.getElementById("DivNothingMessage").style.display = 'none';
	document.getElementById("DivExportExcel").style.display = 'block';
    gridObj = loadDHTMLXGrid('lotErrorReport', gridHeight, dataHost, setHeader, setInitWidths, setColAlign, setColTypes, setColSorting, setColIds, enableTooltips, setFilter, footerArry, footerStyles, footerExportFL);
   

    gridObj.setColumnHidden(3,true);
    gridObj.setColumnHidden(8,true);
    gridObj.setColumnHidden(11,true);
    gridObj.setColumnHidden(14,true);
    gridObj.setColumnHidden(16,true);
    gridObj.setColumnHidden(18,true);
    gridObj.enableHeaderMenu();
 
    }else{
	document.getElementById("lotErrorReport").style.display = 'none';
	document.getElementById("DivExportExcel").style.display = 'none';
	document.getElementById("DivNothingMessage").style.display = 'block';
}	
  fnStopProgress();	
}

function fnOnPageLoad() {
	document.getElementById("lotErrorReport").style.display = 'none';
	document.getElementById("DivExportExcel").style.display = 'none';
	document.getElementById("DivNothingMessage").style.display = 'block';
	
	if(strFlag=="Y") {
		var lotNumber = document.frmLotErrorReport.lotNumber.value;
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmLotErrorReport.do?method=fetchLotErrorReportInfo&lotNumber='+lotNumber+'&companyInfo='+companyInfoObj+'&ramdomId='+new Date().getTime());
		dhtmlxAjax.get(ajaxUrl,fnLoadLotAjax);
}
}
//To open the window to display the comments 
function fnOpenComments(transID,type){
	if(type == 'Order'){
	windowOpener('/GmOrderItemServlet?hAction=PICSlip&hId='+transID,"Pack","resizable=yes,scrollbars=yes, top=150,left=150,width=760,height=610");
	}else if(type == 'Return'){
        windowOpener('/GmPrintCreditServlet?hAction=PRINT&hRAID='+transID,"Returns","resizable=yes,scrollbars=yes,top=150,left=200,width=750,height=450");
	}else if(type == 'Consignment'){
    windowOpener("/GmConsignItemServlet?hAction=PicSlip&hConsignId="+transID+"&ruleSource="+type,"SetPic","resizable=yes,scrollbars=yes,top=150,left=150,width=760,height=600");
	}
}
//PMT-29681 TO open Lot Tracking report in popup
function fnOpenLT(partNum,lotNum){
 windowOpener('/gmLotTrackReport.do?method=fetchLotTrackingDetails&partNum='+ encodeURIComponent(partNum) + '&ctrlNum=' +lotNum + '&warehouse=0' + '&strOpt=ReLoad', "Search","resizable=yes,scrollbars=yes,top=200,left=250,width=1050,height=670");

}
//PMT-29681 TO open Lot Code report in popup
function fnOpenLC(strLotNM){
	
	windowOpener(" /gmLotCodeRptAction.do?method=loadLotCodeProdInfo&lotNum="+strLotNM+"&companyInfoObj="+companyInfoObj,"LotCOdeReport","resizable=yes,scrollbars=yes,top=150,left=150,width=1020,height=600");
}
function fnReset(){
	document.frmLotErrorReport.resolutionType.value = '0';
	document.getElementById("new_lot_tex_tbox").value='';
	document.frmLotErrorReport.comments.value = '';
}
