var rmVoidCount = 0;
// To Submit Acknowledgment details
function fnAckSubmit(){
	if (document.frmVendor.hQuoteCnt)
 	{
	 	var strQuoteStr = fnCreateQuoteString();// Prepare the quote string, if any
	 	document.frmVendor.hQuoteStr.value = strQuoteStr;
		if (ErrorCount > 0){
			Error_Show();
			Error_Clear();
			return false;
		}
 	}
	Error_Clear();
	// Create ACK order string
	fnCreateAckString();
	if (document.frmVendor.Txt_Reason.value == "")
	{
		Error_Details(message[59]);//"Please enter a valid reason"
	}
	hcnt = parseInt(document.frmVendor.hCnt.value);
	hcnt = hcnt+cnt;
	document.frmVendor.hCnt.value = hcnt;
	document.frmVendor.hAction.value = 'SaveAckOrder';
	fnLenValidation(document.frmVendor.Txt_Comments,"Comments","500");
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	} 
	if(rmVoidCount > 0){//if remove qty in all parts is same as orginal qty then void the whole order  
		Error_Details(message_operations[234]);
		document.frmVendor.Btn_VoidOrd.disabled=false;
		Error_Show();
		Error_Clear();
		return false;
	}else{
		fnStartProgress();
		document.frmVendor.submit();
	}
}

// Function to created ACK order string
function fnCreateAckString(){
	var partDtObj = '';
	var pnumObj = '';
	var releaseQtyObj = '';
	var reserveQtyObj = '';
	var rmQtyObj = '';
	var pendShipQtyObj = '';
	var pnumqtystr = '';
	var pendShipQtyStr = '';
	var reserveQtyStr = '';
	var rmQtyStr = '';
	var hcnt = parseInt(document.frmVendor.hCnt.value);//number of part no.s
	var errMsgCnt = 0;
	var rowCount = 0;
	var relQty = 0;
	var reserveQty = 0;
	rmVoidCount = 0;
	for(var i=0; i <=hcnt; i++){
		var voidFl = '';
		var pNumStr = '';
		var rmQty = '';
		var boQty = '';
		var orgQty = '';
		var price = '';

		partDtObj = eval('document.frmVendor.hPartDt'+i);// part number details, strItemQty+"^"+strItemOrdId+"^"+strPartNumHidden+"^"+strType+"^"+strPrice
		rmQtyObj = eval('document.frmVendor.Txt_RmQty'+i);
		pnumObj = eval('document.frmVendor.hpNum'+i);// part number
		pendShipQtyObj = eval('document.frmVendor.hpendShipQty'+i);// ship qty
		releaseQtyObj = eval('document.frmVendor.hRelQty'+i);
		reserveQtyObj = eval('document.frmVendor.hReserveQty'+i);
		relQty = (releaseQtyObj.value == '')? 0 : eval(releaseQtyObj.value);
		reserveQty = (reserveQtyObj.value == '')? 0 : eval(reserveQtyObj.value);
		
		if(partDtObj){ 
			rowCount += 1;
			pNumStr = partDtObj.value;
	
			orgQty = pNumStr.substr(0,pNumStr.indexOf("^"));
			rmQty = (rmQtyObj.value == '')? 0 : eval(rmQtyObj.value);
			if(eval(orgQty) < eval(rmQty)){
					pnumqtystr = pnumqtystr.search(pnumObj.value) == -1? pnumqtystr + pnumObj.value +',' : pnumqtystr;			
			}
			
			if(eval(orgQty) - eval(relQty) < eval(rmQty))
			{
				pendShipQtyStr = pendShipQtyStr.search(pnumObj.value) == -1? pendShipQtyStr + pnumObj.value +',' : pendShipQtyStr;		
			}
			
			if(rmQty >= 0)
			{
				rmQtyStr = rmQtyStr+pNumStr+'^'+rmQty+'^'+relQty+'|';			
			}
			
			//If the lot numbers are reserved, then should not edit the qty
			if(reserveQty > 0 && rmQty > 0){
				reserveQtyStr = reserveQtyStr.search(pnumObj.value) == -1? reserveQtyStr + pnumObj.value +',' : reserveQtyStr;
			}
			
			if(eval(orgQty) == eval(rmQty)){
				rmVoidCount += 1;
			}
			
		} //end of	if(partDtObj.value)	
	}
	
	if(reserveQtyStr != ''){
		reserveQtyStr = reserveQtyStr.substring(0,reserveQtyStr.lastIndexOf(','));
		Error_Details(message_operations[235]+reserveQtyStr);
	}
		
	if (pnumqtystr != '')
	{
		pnumqtystr = pnumqtystr.substring(0,pnumqtystr.lastIndexOf(','));
		Error_Details(message_operations[236]+pnumqtystr);
		errMsgCnt++;
	}
	if(pendShipQtyStr != '' && errMsgCnt == 0){
		pendShipQtyStr = pendShipQtyStr.substring(0,pendShipQtyStr.lastIndexOf(','));
		Error_Details(message_operations[237]+pendShipQtyStr);
	}
	
	if (rmVoidCount == rowCount){
		rmVoidCount = 1;
	}else{
		rmVoidCount = 0;
	}
	document.frmVendor.hRmQtyStr.value = rmQtyStr; 
}

//To void the Ack order
function fnVoidAckOrder(){
	var ordid = document.frmVendor.hOrdId.value;
	document.frmVendor.action = servletPath+"/GmCommonCancelServlet?hTxnName="+ordid+"&hCancelType=VDACK&hTxnId="+ordid+"&hCancelReturnVal=true";
	document.frmVendor.hAction.value = "Load";
	document.frmVendor.submit();
}