
 function fnPrint(){
	window.print();
}

function fnPrintDO()
{
	var screenW = 750, screenH = 880;
	if(GLOBAL_ClientSysType == 'IPAD') 
	{
		pophtml = '<html><body><form name="frmDODashboard" method="post" action="/gmDOReport.do?method=printDOBooking">';
		pophtml +='<input type=hidden name=orderID value="'+ordId+'"/>';
		pophtml +='<input type=hidden name=haction value="'+hidePartPrice+'"/>';
		pophtml +='</form></body><script>document.frmDODashboard.submit();</script></html>';
		winpPop=window.open("","script","toolbars=no,top= 100,left=100,height=450,innerHeight=450,width=1100,innerWidth=400,scrollbars=yes,resizeable=no,status=");
		winpPop.document.writeln(pophtml);
		winpPop.show;
	}
	else
	{
		w1 = createDhtmlxWindow(900, 750);
		w1.setText(message[5779]);
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmDOReport.do?method=printDOBooking&orderID='+ordId+'&haction='+hidePartPrice);
		w1.attachURL(ajaxUrl);
	}
}

function hidePrint()
{
	buttonTdObj=document.getElementById("buttonTd");
	buttonTdObj.style.display='none';
}

function showPrint()
{
	buttonTdObj=document.getElementById("buttonTd");
	buttonTdObj.style.display='block';
}
function fnPicSlip()
{
	windowOpener("/GmOrderItemServlet?hId="+ordId+"&hAction=PICSlip&hOpt=None&","PICSlip","resizable=yes,scrollbars=yes,top=150,left=200,width=770,height=600");
}

function fnConfirmDO(){
	// Sterile parts validation for sales rep.
	var confirmStr = true;
	if(PartNumInputStr != undefined && PartNumInputStr != '')
		PartNumInputStr = PartNumInputStr.substring(PartNumInputStr,(PartNumInputStr.length)-1);
	if(blControlNumDiscrepancy == "true"){
		if(intControlSize > 0 && PartNumInputStr != '' && lotOverrideAccess == 'H'){
			confirmStr =  confirm(Error_Details_Trans(message[5780],PartNumInputStr)); 
		}else{
			Error_Details(Error_Details_Trans(message[5781],PartNumInputStr));
		}		
		if(ErrorCount > 0){
			Error_Show();
			Error_Clear();
			return false;
		}
	}			
	if(!confirmStr)	return false;	
	document.frmOrder.action="/gmOrderDetails.do?method=confirmDO&orderID="+ordId+"&accountID="+accountId;
	
	if(document.frmOrder.Btn_Confirm !=undefined)
	{
	document.frmOrder.Btn_Confirm.disabled = true;
	}
	
	if(document.frmOrder.Sales_Submit !=undefined)
	{
		document.frmOrder.Sales_Submit.disabled = true;
	}
	
	fnStartProgress();
	document.frmOrder.submit();
}

function fnNextOrder()
{
	document.frmOrder.hOpt.value = "PHONE";
	document.frmOrder.hAction.value = "Load";
	// alert("document.frmOrder.hOpt.value ----"  + document.frmOrder.hOpt.value);
	document.frmOrder.submit();	
}

function fnSaveComments()
{
	document.frmOrder.hAction.value = "SaveOrderComments";
	document.frmOrder.submit();	

}

function fnClose()
{
	window.close();
}

function fnMoveToHold()
{
	document.frmOrder.hCancelType.value = "OHOLD";
	document.frmOrder.action = "/GmCommonCancelServlet";
	document.frmOrder.hTxnId.value = ordId;
	document.frmOrder.hAction.value = "Load";
	document.frmOrder.submit();
}

function fnEmailDO(){
	dhxWins = new dhtmlXWindows();
    dhxWins.enableAutoViewport(true);
    w1 = dhxWins.createWindow("w1", 30, 250, 860, 470);
    w1.setText(message[5782]);
    var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmDOReport.do?method=emailOrderSummary&orderID='+ordId+'&caseID='+caseId+'&accountID='+accountId+'&accountNm='+accountNm+'&to='+repEmail+'&haction='+hidePartPrice);
    w1.attachURL(ajaxUrl);
}
function fnShowFilters(val)
{
	var obj = eval("document.all."+val);
	var obj1 = eval("document.all."+val+"img");
	if (obj.style.display == 'none')
	{
		obj.style.display = 'block';
		if(obj1){
			obj1.src = '/images/minus.gif';
		}
	}
	else
	{
		obj.style.display = 'none';	
		if(obj1){
			obj1.src = '/images/plus.gif';
		}
	}
}
function fnPageOnLoad(){

	//To show/hide the Individual shipping Edit column in Shipping details section.
	if(varShowIndShipEdit == 'true'){
		fnShowIndShipEdit();
	}else if(ordStatusId != '7'){
		//When order is not in Pending CS confirmation status for alignment issues.
		if(document.getElementById("grdTotal")){
			document.getElementById("grdTotal").colSpan = "12";
		}
		if(document.getElementById("grdTotal1")){
			document.getElementById("grdTotal1").colSpan = "12";
		}
		if(document.getElementById("invTotal")){
			document.getElementById("invTotal").colSpan = "12";
		}
		if(document.getElementById("taxTotal")){
			document.getElementById("taxTotal").colSpan = "12";
		}
	}
	//To show/hide the Replenish Req column in Usage details section.
	if(varShowRepReqClmn == 'true'){
		fnShowRepReqColumn();
	}
//check account is on credit hold. if yes show warning msg and hide place order button	
fnValidateCreditHold(accountId,["Btn_Confirm"],"frmOrder");

}
//To show/hide the Replenish Req column in Usage details section.
//By default in JSP display = none. and it is dispalyed for DO's booked from App. 
function fnShowRepReqColumn(){		
	var shipHdrid = document.getElementById('repReqHdr');
	if(shipHdrid.style.display == "none"){
		shipHdrid.style.display="block";
        } 
	var shipHdrLine = document.getElementById('repReqHdrLine');
	if(shipHdrLine.style.display == "none"){
		shipHdrLine.style.display="block";
        } 
	for (i=0;i<usageRowCnt;i++){
		var shipDtlRow = document.getElementById('repReqClmn'+i);
		if(shipDtlRow.style.display == "none"){
			shipDtlRow.style.display="block";
	    } 
		var shipDtlLine = document.getElementById('repReqClmnLine'+i);
		if(shipDtlLine.style.display == "none"){
			shipDtlLine.style.display="block";
	    } 
	}	
}

//To show/hide the Individual shipping Edit column in Shipping details section.
//By default in JSP display = none. and it is dispalyed for DO's booked from App. 
function fnShowIndShipEdit(){
	var shipHdrid = document.getElementById('shipHdrEdit');
	if(shipHdrid.style.display == "none"){
		shipHdrid.style.display="block";
    } 
	var shipHdrLine = document.getElementById('shipHdrEditLine');
	if(shipHdrLine.style.display == "none"){
		shipHdrLine.style.display="block";
    } 
	for (i=0;i<shipRowCnt;i++){
		var shipDtlRow = document.getElementById('shipDtlEdit'+i);
		if(shipDtlRow != undefined){
		if(shipDtlRow.style.display == "none"){
			shipDtlRow.style.display="block";
	    } 
		var shipDtlLine = document.getElementById('shipDtlEditLine'+i);
		if(shipDtlLine.style.display == "none"){
			shipDtlLine.style.display="block";
	    } 
		}
	}
}
function fnEditDetails(val){
	document.frmOrder.action ='/gmDOSummaryEdit.do?method=edit'+val+'&orderID='+ordId+'&caseID='+caseId+'&accountNm='+accountNm+'&accountID='+accountId+'&surgeryDt='+surDt+'&orderTypeNm='+OrderType+'&custPO='+customerPO+'&caseInfoID='+caseinfoID+'&parentOrderID='+parentOrder;
	document.frmOrder.submit();
}
//PC-2151 Enable Edit Option in DO Approval Pending Confirmation Screen
//DO APP = 103521
function fnEditUsageDetails(){
	if(orderSource == '103521'){
		document.frmOrder.action ='/gmOrderDetails.do?method=editUsageDeatils&orderMode=IPAD'+'&orderID='+ordId+'&caseID='+caseId+'&accountNm='+accountNm+'&accountID='+accountId+'&repID='+repId+'&surgeryDt='+OrdSurgeryDt+'&orderTypeNm='+OrderType+'&caseInfoID='+caseinfoID+'&parentOrderID='+parentOrder+'&custPO='+escape(customerPO)+'&cbo_repName='+strDistRepNm;
	}else{
	document.frmOrder.action ='/gmOrderDetails.do?method=editUsageDeatils&orderID='+ordId+'&caseID='+caseId+'&accountNm='+accountNm+'&accountID='+accountId+'&repID='+repId+'&surgeryDt='+surDt+'&orderTypeNm='+OrderType+'&caseInfoID='+caseinfoID+'&parentOrderID='+parentOrder+'&custPO='+escape(customerPO);
	}
	document.frmOrder.submit();
}
function fnEditShippDetails(val){
	fnEditShippInfo(val,ordId);
}
function fnEditShippInfo(val,varOrderID){
	//passing the parentOrderID, to identify which DO that is currently being modified.
	if(val == 'single'){
	document.frmOrder.action ='/gmDOSummaryEdit.do?method=editShipDetails&orderID='+varOrderID+'&parentOrderID='+ordId+'&caseID='+caseId+'&accountID='+accountId+'&orderTypeNm='+OrderType+'&custPO='+customerPO+'&caseInfoID='+caseinfoID;
	//document.frmOrder.action ='/gmDOSummaryEdit.do?method=editShipDetails&orderID='+varOrderID+'&caseID='+caseId+'&accountID='+accountId+'&orderTypeNm='+OrderType+'&custPO='+customerPO+'&caseInfoID='+caseinfoID;
	}else{
	document.frmOrder.action ='/gmDOSummaryEdit.do?method=editMultipleShipDetails&orderID='+varOrderID+'&caseID='+caseId+'&accountID='+accountId+'&orderTypeNm='+OrderType+'&custPO='+customerPO+'&caseInfoID='+caseinfoID;
	}
	document.frmOrder.submit();
}
function fnCheckSubmit(){
	if(document.frmOrder.Chk_Submit.checked && SwitchUserFl != 'Y'){
		document.frmOrder.Sales_Submit.disabled = false;
	}else{
		document.frmOrder.Sales_Submit.disabled = true;
	}
}
function fnVoidDO(){
	document.frmOrder.action ="/GmCommonCancelServlet?hAction=Load&hCancelType=VDRES&hOrderType=2518&hTxnId="+ordId;
    document.frmOrder.submit();
}
function fnShowPrice(){
	document.frmOrder.action='/GmEditOrderServlet?hMode=PrintPrice&hOrdId='+ordId+'&hParantForm=CASE&hidePartPrice=Y';
	document.frmOrder.submit();
}
function fnHidePrice(){
	document.frmOrder.action='/GmEditOrderServlet?hMode=PrintPrice&hOrdId='+ordId+'&hParantForm=CASE&hidePartPrice=N';
	document.frmOrder.submit();
}
function fnCloseSummary(val){
	document.frmOrder.action ='/gmDOReport.do?method=loadDODashboard&ordStatus='+val;
	document.frmOrder.submit();
}
function fnBack(){
	if(fwdFlag == "YES"){
		document.frmOrder.action = "gmOrderDetails.do?method=editUsageDeatils&orderID="+ordId+"&caseID="+caseId+"&repID="+repId+"&accountNm="+accountNm+"&accountID="+accountId+"&caseInfoID="+caseinfoID+"&parentOrderID="+parentOrder+"&custPO="+customerPO+"&surgeryDt="+surDt;
		document.frmOrder.submit();
	}else{
		history.go(-1);
	}
}


//This Function will call the Print Proforma invoice button
function fnProformaInv() { 
	    var strOpt = 'PROFORMA';
		windowOpener("/gmProformaInvoice.do?orderid="
				+ ordId + "&strOpt="+ strOpt, "Print",
				"resizable=yes,scrollbars=yes,top=150,left=150,width=800,height=750");
}

function fnPrintBODRecipt(){
	fnPrintBOD("PrintBOD");
	fnPrintBOD("PrintBODforAnotherCopy");
	fnPrintReceipt();
}

function fnPrintBOD(hOpt){
	var strOpt = 'BOD';
	var BOD_fl='';
	if(hOpt=='PrintBODforAnotherCopy'){
		BOD_fl='Y';
	}
	windowOpener("/gmProformaInvoice.do?orderid="
			+ ordId + "&strOpt="+ strOpt+"&BOD_COPY_FL="+BOD_fl, hOpt,
			"resizable=yes,scrollbars=yes,top=150,left=150,width=800,height=750");

}

function fnPrintReceipt(){
	var strOpt = 'DORECEIPT';
	windowOpener("/gmProformaInvoice.do?orderid="
			+ ordId + "&strOpt="+ strOpt, "PrintDOReceipt",
			"resizable=yes,scrollbars=yes,top=150,left=150,width=800,height=750");
}

function fnOpenAdjustment(orderid){    
	windowOpener('/gmPartPriceAdjustmentRptAction.do?method=fetchAdjustmentDetails&accId='+ orderid , "AdjustmentDetails","resizable=yes,scrollbars=yes,top=250,left=300,width=1065,height=490");	
}

//This function used to export Ipad PDF File. PC-4960 - Order summary
function fnExportFile(fileID){
	var uploadDir="DOFILEPUSHLOC";
	windowOpener("/GmCommonFileOpenServlet?strOpt=IPADORDERS&compLocale="+companyLocale+"&uploadString="+uploadDir+"&fileName="+fileID+".pdf");
}