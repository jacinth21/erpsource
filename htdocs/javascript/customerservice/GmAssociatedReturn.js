function fnLoad()
{
	document.frmAssociatedReturn.strOpt.value = "load";
	fnStartProgress();
	document.frmAssociatedReturn.submit();
}

function fnEnter()
{
		if (event.keyCode == 13)
		{ 
			fnLoad();
		}
}

function fnPrintReturnDetails(val)
{
	windowOpener('/GmPrintCreditServlet?hAsstFlag=true&hAction=PRINT&hRAID='+val,"Returns","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=850");
}
