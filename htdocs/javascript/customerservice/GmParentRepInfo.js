//Function used to display Parent rep Info
function fnParentAcInfo(acctIdval,enablePriceLabel){

	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmParentRepAcctInfo.do?method=loadParentRepAccountInfo&acctId='+acctIdval+'&randomId='+Math.random());
	
	dhtmlxAjax.get(ajaxUrl,function(loader){
		var resJSONObj = $.parseJSON(loader.xmlDoc.responseText);
		var tableOpen = '<table border="0" width="100% >';
		var tableClose = '</table>';
		var trOpen = '<tr height="25">';
		var trClose ='</tr>';
		var tdLabelCols1 = '<td class="RightTableCaption" align="Right" colspan ="1">';
		var tdLabelCols2 = '<td class="RightTableCaption" align="Right" colspan ="2">';
		var tdLabelCols3 = '<td class="RightTableCaption" align="Right" colspan ="3">';
		var tdLabel2 =	'</td>';
		var tdLabelleftCols2 = '<td class="RightTableCaption" align="left" colspan ="2">';
		var tdDataOpen = '<td  class="RightText">';
		var tdDataOpenCols1 = '<td  class="RightText" colspan = "1">';
		var tdDataOpenCols2 = '<td   class="RightText" colspan = "2">';
		var tdDataOpenCols3 = '<td  class="RightText" colspan = "3">';
		var tdDataClose = '</td>';
		var trNew = '<tr height="15">'
		var tdNew = '<td>'
		var space ='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
		var parentDiv= document.getElementById("parentDiv");
		var itemJSON;
		var htmlStrAcctId;
		var htmlStrDistId;
		var htmlStrRepNm;
		var tablerow ='';
		var partyName;
		var parentHTML;
		var msgRow='';
	
		var lineRow ='<tr><td colspan="6" class="LLine"></td></tr>';
		
		
	
		if(resJSONObj.length==undefined)
			resJSONObj = (new Array()).push(resJSONObj);
		
		if (resJSONObj.length == 0){
			
			msgRow = trOpen+tdDataOpen+message_operations[463]+tdDataClose+trClose;
			parentDiv.innerHTML =tableOpen+msgRow+lineRow+tableClose;
		}else{
			
			for(i= 0; i<resJSONObj.length;i++){
				itemJSON = resJSONObj[i];

				 partyName= itemJSON["PARENTACCTNM"];
				var repAcName= itemJSON["REPACNM"];
				var repId= itemJSON["REPACID"];
				var dName= itemJSON["DNAME"];
				var repName= itemJSON["RPNAME"];
				
				htmlStrAcctId = trOpen+tdDataOpenCols2+space+repAcName+'('+repId+')'+tdDataClose;
				htmlStrDistId =tdLabelCols1+message_operations[464]+tdLabel2+tdDataOpenCols1+dName+tdDataClose;
				htmlStrRepNm = tdLabelCols1+message_operations[465]+tdLabel2+tdDataOpenCols1+repName+tdDataClose+trClose;
				
				tablerow = tablerow+ htmlStrAcctId+htmlStrDistId+htmlStrRepNm;
			}
			
			parentHTML = trOpen+tdLabelCols1+message_operations[466]+tdLabel2+ tdDataOpenCols3+ partyName+tdDataClose+trClose ;
			msgRow = trOpen+tdLabelleftCols2+space+message_operations[467]+tdLabel2+trClose;
			
			if(enablePriceLabel=='N'){
				msgRow ='';
			}
					
			parentDiv.innerHTML = tableOpen+trOpen+tdNew+tableOpen+parentHTML+tableClose+tdDataClose+trClose+tablerow+msgRow+lineRow+tableClose;
		}
		
	});
}