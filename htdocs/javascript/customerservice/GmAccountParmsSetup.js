function fnSubmit1(form) {
	//var frm = document.frmRuleParamsSetup;
	var inputString = '';
	var vldInvGrp='';
	var varIPVldcnt = 0;
	form.subBtn.disabled = true;
	// validate the Comments
	fnValidateTxtFld('txt_LogReason',message[5120]);
	var vStr = document.getElementById("hValidationStr");
	var vlidationStr = vStr.value;
	var varRows = document.getElementById("hVarRows");
	for ( var i = 0; i < varRows.value; i++) {
		obj = eval("document.frmRuleParamsSetup.hControlTyp" + i);
		varContrlType = obj.value;
		obj = eval("document.frmRuleParamsSetup.hLableNm" + i);
		varLableNm = obj.value;
		if (varContrlType != 'HEADER') {
			obj1 = eval("document.frmRuleParamsSetup.hLabelID" + i);
			varLabelID = obj1.value;
			// 92000 Create XML
			if(createXMLFl !='Y' && varLabelID == 92000){
				continue;			
			}
			obj = eval("document.frmRuleParamsSetup.attrValue" +varLabelID);
			varAttrValue = obj.value;
			if ((varLabelID == '91985' && varAttrValue != '')  || (varLabelID == '92001' && varAttrValue != '')) { //  code id hard coded for Email validation
				varAttrValue = varAttrValue.replace(/;/g, ',');
				varAttrValue = varAttrValue.replace(/\s/g, '');
				if (fnValidateEmail(varAttrValue)) {
					Error_Details(Error_Details_Trans(message[5123],varLableNm)); 
				}
			}
			if (varContrlType == 'CHKBOX') {
				if (obj.checked) {
					varAttrValue = "Y";
				} else if (obj.checked == false) {
					varAttrValue = "N";
				}
				if(varLabelID==91983){
					vldInvGrp = varAttrValue;  //to validate Invoice Parameters
				}
			}
			if(vldInvGrp == 'Y'){ 
			//vlidationStr from rule
			if (vlidationStr.indexOf(varLabelID) != -1) {
				if (varContrlType == 'SELECT') {
					fnValidateDropDn("attrValue" + varLabelID, varLableNm);
				} else {
					fnValidateTxtFld("attrValue" + varLabelID, varLableNm);
				}
			}
			if(varLabelID=='91986') //CSV Template name
			{
				var varEFFormat =document.frmRuleParamsSetup.attrValue91984;
				if(varEFFormat && varEFFormat.value != '91994'){ // 91994 PDF value of 91984 Electronic File Format
					fnValidateDropDn("attrValue" + varLabelID, varLableNm);
				}
			}
			//91984 Electronic File Format, 91985 Recipients Email , 91986 CSV Template Name making empty when Electronic Email unchecked
		  }else if(vldInvGrp == 'N' && (varLabelID == '91984' || varLabelID== '91985' || varLabelID == '91986')){
			  if(varAttrValue!='' && varAttrValue!='0')
			  varIPVldcnt++;
		  }
			//If the attribute vale is Zero,then it is setting as zero for DB zero/null value issue
			if(varAttrValue==0){
				varAttrValue='';				
			}
			inputString = inputString + varLabelID + '^' + varAttrValue + '|';
		}
	}
	if(varIPVldcnt > 0){
		Error_Details(message[5121]);
	}
	
	var codeGrp = document.getElementById("codeGrpId");
	var codeGrpId = codeGrp.value;
	var ruleGrp = document.getElementById("ruleGrpId"); 		
	var commnets = document.all.txt_LogReason.value;
	if(ruleGrp.value =='0' || ruleGrp.value == null || ruleGrp.value == ''){
		Error_Details(message[5122]);
	}	
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		form.subBtn.disabled = false;
		return false;
	}
	
	var requestStr = '/gmRuleParamSetup.do?codeGrpId=SHPPM&ruleGrpId='
		+ ruleGrp.value
		+ '&strInputString='
		+ inputString
		+ '&codeGrpId='
		+ codeGrpId
		+ '&strOpt=save'
		+ '&txt_LogReason='
		+ commnets;
	//loadajaxpage(requestStr, 'ajaxdivcontentarea');
	form.strInputString.value = inputString;

	form.strOpt.value = "save";
	fnStartProgress();
	form.submit();
}

   //not needed in Accountsetup container
//function fnClose() {  
	//parent.window.close();
//}


function fnValidateEmail(inputvalue) {
	var pattern = /(^[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$)/;
	var error = false;
	var email = inputvalue.split(',');
	for ( var i = 0; i < email.length; i++) {
		if (!pattern.test(email[i])) {
			error = true;
			break;
		}
	}
	return error;
}


function fnChange(objChng)
{  var form = document.getElementsByName('frmRuleParamsSetup')[0]; 
	if(parent.window.document.getElementById("ajaxdivcontentarea") != undefined){
			parent.window.document.getElementById("ajaxdivcontentarea").style.height = "400";
		}
	var varEFFormat =document.all.attrValue91984; // 91984 Electronic File Format
	var templateNm =document.all.attrValue91986; // 91986 CSV Template Name
	if(varEFFormat.value =='91994'){ //91994 PDF - Electronic File Format
		templateNm.value ='0';
		templateNm.disabled = true;
	}else if(accountType != "26230710"){//Hospital Dealer
		templateNm.disabled = false;
	}
}