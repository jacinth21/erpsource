// this function used to click the load button - fire the function
function fnLoad() {
	var fromToDiff;
	objStartDt = document.frmAccount.Txt_FromDate;
	objEndDt = document.frmAccount.Txt_ToDate;
	if (objStartDt.value != "") {
		CommonDateValidation(document.frmAccount.Txt_FromDate, dateFmt,Error_Details_Trans(message[10523],dateFmt));
	}

	if (objEndDt.value != "") {
		CommonDateValidation(document.frmAccount.Txt_ToDate, dateFmt,Error_Details_Trans(message[10524],dateFmt));
	}
	if (objStartDt.value == "" && objEndDt.value != "") {
		Error_Details(message[10535]);
	}
	if (objStartDt.value != "" && objEndDt.value == "") {
		Error_Details(message[10536]);
	}
	if (objStartDt.value != "" && objEndDt.value != "") {
		// Checking for Valid date range. To date always greater than or equals
		// to From date.
		fromToDiff = dateDiff(document.frmAccount.Txt_FromDate.value,
				document.frmAccount.Txt_ToDate.value, dateFmt);
		if (fromToDiff < 0) {
			Error_Details(message[10525]);
		}
	}

	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	document.frmAccount.hCompanyId.value = compyid;
	document.frmAccount.hDtFmt.value = appleDateFmt;
	document.frmAccount.hAction.value = "Go";
	fnStartProgress();
	document.frmAccount.submit();
}
// this function used drill down  the part details - to click the part number hyper link
function Toggle(val) {

	var obj = document.getElementById("div" + val);
	var trobj = document.getElementById("tr" + val);
	var tabobj = document.getElementById("tab" + val);

	if (obj.style.display == 'none') {
		obj.style.display = '';
		trobj.className = "ShadeRightTableCaptionBlue";
		tabobj.style.background = "#ecf6fe";
	} else {
		obj.style.display = 'none';
		trobj.className = "";
		tabobj.style.background = "#ffffff";
	}
}

function fnCallDrill(pnum, id, type, reportType) {
	var frmDate = document.frmAccount.Txt_FromDate.value;
	var toDate = document.frmAccount.Txt_ToDate.value;
	var cnum = document.frmAccount.Txt_CNum.value;
	
	windowOpener("/GmPartNumSearchServlet?hId=" + id
			+ "&hAction=Drill&Txt_PartNum=" + encodeURIComponent(pnum) + "&hType=" + type
			+ "&Txt_FromDate=" + frmDate + "&Txt_ToDate=" + toDate
			+ "&hDtFmt=" + appleDateFmt + "&Txt_CNum=" + cnum +"&reportType="+reportType, "Drill",
			"resizable=yes,scrollbars=yes,top=300,left=300,width=520,height=300");
}