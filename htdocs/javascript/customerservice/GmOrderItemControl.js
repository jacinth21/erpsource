
var CustPOValidFl = true;

var partArr = new Array(); 

//Disable PO date field if do is already invoiced
function fnOnPageLoad(){
	if(document.frmVendor.hInvoiceId.value!=''){
			 document.getElementById("Txt_PODate").disabled = true;
			 document.getElementById("Img_Date").disabled = true;
	}	
}

function fnCreditHold(){
	var optVal=document.getElementById('cbo_ack_action').value;
	if(optVal=='26240236' || optVal=='26240233' || optVal=='26240232' || optVal =='26240587' || optVal =='26240588'){
		var accid = document.frmVendor.hAccId.value;
		fnValidateCreditHold(accid,["Btn_Submit"],"frmVendor");
	}else{
        document.getElementById("Btn_Submit").disabled=false;
 	}	
}

function fnAckSubmit(optVal) {
	 var optVal=document.getElementById('cbo_ack_action').value;
	if((validatFlag == 'Y' && optVal=='26240236') ||  (validatFlag == 'Y' && optVal=='26240233') || (validatFlag =='Y' && optVal=='26240232')){
		Error_Details(message[5787]);
	}
	
	if((validatFlag != 'Y' && optVal=='26240587') ||  (validatFlag != 'Y' && optVal=='26240588')){
		Error_Details(message[5787]);	
	}
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	
	// validate the choose action drop down
	fnValidateDropDn('cbo_ack_action', message[5507]);
	if (optVal == 106349) { // Save Changes (call existing function)
		fnSubmit();
	} else if (optVal == 106350) { // save comments
		fnValidateTxtFld('Txt_LogReason',message[5550]); 
		if (ErrorCount > 0) {
			Error_Show();
			Error_Clear();
			return false;
		}
		fnSaveComments();

	} else if(optVal == 26240586){
		fnCommercialInv();
	}else if(optVal == 26240587){ // Add/Generate Ous Distributor
		fnAddGenerateOusDistributor();
	}else if(optVal == 26240588){  //Release Ous Distributor
		fnReleaseOusDistributor();
	}
	else { // Gen (Direct/Intercompany/ bill and hold)
		fnGenOrder();
	}
} 


function fnSubmit()
{
	document.getElementById("Txt_PODate").disabled = false;
	hcntObj = document.getElementById("hCnt");
	if(hcntObj != undefined && hcntObj!= null){
		hcnt = parseInt(document.frmVendor.hCnt.value);	
		hcnt = hcnt+cnt;
		document.frmVendor.hCnt.value = hcnt;
	}
	var raReason = document.frmVendor.hRaReason.value;
	Error_Clear();
	var strErrorMore = '';
	var strErrorLess = '';
	var confirmMsg = true; // If Po Message is empty , should submit without confirmation message.
	
	if (document.frmVendor.hAction.value == 'EditControl')
	{
		var theinputs = document.getElementsByTagName("input");
	    var arr = document.frmVendor.pnums.value.split(',');
	    var blk = '';
	    var cnum = '';
	    var qtycnt = 0;
	    hcnt++;
	    var itype = '';
	    for(var i=0;i<arr.length;i++)
	    {
	       qid = arr[i];
	       pnum1 = qid.replace('.','');
	       blk = eval("document.frmVendor.hQty"+pnum1);
		    for(var j=0;j<hcnt;j++)
		    {
				pnumobj = eval("document.frmVendor.hPartNum"+j);
				itype = eval("document.frmVendor.hType"+j);
				//alert(itype.value);
				if (pnumobj && itype.value == 50300)
				{
					if (pnumobj.value == qid)
					{
						qtyobj = eval("document.frmVendor.Txt_Qty"+j);
						cnumobj = eval("document.frmVendor.Txt_CNum"+j);
						if(!isNaN(qtyobj.value))
						{
							qty = parseInt(qtyobj.value);
							cnum = TRIM(cnumobj.value);
							if (qty > 0)
							{
								qtycnt = qtycnt + qty;
							}
						}
						else
						{
							alert("Non Numeric");
						}
					}
				} // IF Valid object
			} // END of Inner FOR loop
	
			if (qtycnt > parseInt(blk.value) && raReason != '3316'  )
			{
				strErrorMore  = strErrorMore + ","+ qid+"/"+cnumobj.value;
			}
			if (qtycnt < parseInt(blk.value) )
			{
				strErrorLess  = strErrorLess + ","+ qid+"/"+cnumobj.value;
			}
			
			qtycnt = 0;
		} // End of outer FOR loop		
		
		if (strErrorMore != '')
		{
			Error_Details(Error_Details_Trans(message[5522],strErrorMore.substr(1,strErrorMore.length)));
		}
		if (strErrorLess != '')
		{
			Error_Details(Error_Details_Trans(message[5523],strErrorLess.substr(1,strErrorLess.length)));
		}
		if (ErrorCount > 0)
		{
			Error_Show();
			return false;
		}else{
			document.frmVendor.hAction.value = 'SaveControl';
			if(document.frmVendor.Chk_ShipFl.checked == true)
			{
				document.frmVendor.txnStatus.value = 'VERIFY';
			}
			document.frmVendor.action = "/gmRuleEngine.do";
			fnStartProgress();
			document.frmVendor.submit();
	  	}
		
	}
	else if (document.frmVendor.hAction.value == 'EditShip')
	{
		document.frmVendor.Cbo_ShipTo.disabled = false;
		document.frmVendor.Txt_SDate.disabled = false;
		document.frmVendor.Cbo_ShipCarr.disabled = false;
		document.frmVendor.Cbo_ShipMode.disabled = false;
		document.frmVendor.Txt_SCost.disabled = false;
		document.frmVendor.Txt_Track.disabled = false;
			
		document.frmVendor.hAction.value = 'SaveShip';
		fnStartProgress();
		document.frmVendor.submit();
	}
	else if (document.frmVendor.hAction.value == 'EditPO' || document.frmVendor.hAction.value == 'ACKORD')
	{  		

		var objCopay = document.frmVendor.Txt_Copay;
		var objCapAmount = document.frmVendor.Txt_Cap_Amount;
		var regExp = /^(?=.*?[1-9])[0-9()-]+(\.\d{1,2})?$/;
		
		if(objCopay != undefined){
			var Copay = TRIM(objCopay.value);
			if(Copay !=''){
				  if(Copay.indexOf('%')!=-1){
			        var Copayval=Copay.split('%');
			        var percentCopay = (Copayval[0]);
			           if (regExp.test(percentCopay)==false || isNaN(percentCopay) || percentCopay < 0 || percentCopay > 100) {
			          Error_Details(Error_Details_Trans(message[46],lblCopay));
			           }
			   }else{
			       var  nonpercentCopay= parseFloat(Copay);
			          if (regExp.test(nonpercentCopay)==false || isNaN(nonpercentCopay) || nonpercentCopay < 0 || nonpercentCopay > 100) {
			            Error_Details(Error_Details_Trans(message[46],lblCopay));
			}        
			     }
			}
		}
        if(objCapAmount != undefined){
        	var CapAmount = TRIM(objCapAmount.value);
		        	if(CapAmount != '' && regExp.test(CapAmount)==false){
		                  Error_Details(Error_Details_Trans(message[47],lblCapAmount));
		         }
		        }  
			

		
		var text_PO = TRIM(document.frmVendor.Txt_PO.value);
		if(document.frmVendor.Txt_ReqDate != undefined){
			CommonDateValidation(document.frmVendor.Txt_ReqDate,format,Error_Details_Trans(message[10538],format));
			var reqDT = document.frmVendor.Txt_ReqDate.value;
			if(dateDiff( currDate,reqDT, format) < 0){
				Error_Details(message[5015]);
			}
		}
		document.frmVendor.Txt_PO.value = text_PO;
		if(document.frmVendor.hInvoiceId.value!='' && (text_PO != custpo)){
			Error_Details(message[696]);
			document.frmVendor.Txt_PO.value = custpo;
		}
		document.frmVendor.hInvoiceId.value = TRIM(document.frmVendor.hInvoiceId.value);
		
if(document.frmVendor.Txt_POAmt!= undefined)
		{
			var text_POAmt = TRIM(document.frmVendor.Txt_POAmt.value);
			if(!IsNumeric(text_POAmt))
			{
				Error_Details(message[400] + message[5187]);
			}
			document.frmVendor.Txt_POAmt.value = text_POAmt;
	
		
		}	


		
		
		if(orderType!='2520'){// We can give space for customer ref 
			fnValidatePOSpace (text_PO);
		}
		// When enter Customer PO more than 20 characters it should throw validation.
		if(TRIM(document.frmVendor.Txt_PO.value) != '' && document.frmVendor.Txt_PO != undefined){
			var custPoLen = TRIM(document.frmVendor.Txt_PO.value).length;

			if(custPoLen > 50 && orderType!='2520'){// 2520: Quote, custPoLen increased to 50 for PC-846
				Error_Details(message[5016]);
			}else if(custPoLen > 100){//Quote should accept 100 characters
				Error_Details(message[5017]);
			}
		}
		if(!CustPOValidFl){
			Error_Details(message[5018]);
		}
		//--PC-3880 New field in record PO Date in GM Italy Added Common Date Validation    
    	var objPODt='';
				objPODt=document.frmVendor.Txt_PODate;
				PODt=objPODt.value;
        if(PODt != ''){
				CommonDateValidation(objPODt,format,'Please Enter Valid Date Format');
			}
		if (ErrorCount > 0)
		{
			
			if(document.frmVendor.hInvoiceId.value!=''){
			 document.getElementById("Txt_PODate").disabled = true;
			 document.getElementById("Img_Date").disabled = true;
			 }
			Error_Show();
			return false;
		}else{
			if(typeof gridObj != "undefined"){
			fnCreateString();
			}
			if(POMessage == 'DUPLICATE'){
				confirmMsg = confirm(message[5525]);
			}
			
			if(!confirmMsg){
				return false
			}else if(document.frmVendor.hAction.value == 'ACKORD'){
				document.frmVendor.Txt_LogReason.value = "";
				document.frmVendor.hAction.value = 'AckSavePO';
				document.frmVendor.hInputString.value = strOrdAtbId+'^'+strOrdCodeId+'^'+reqDT+'|' ; // saving Required Date
				fnStartProgress();
			  	document.frmVendor.submit();
			}else{
				document.frmVendor.hAction.value = 'SavePO';
				fnStartProgress();
				document.frmVendor.submit();
			}
	  	}
	}
	
}

function fnCreateString(){
	var inputString="";
	var POStatus = "";
	var DOStatus = "";
	
	//Removed the changed row values for PMT-53710
	var trStatus = gridObj.getColIndexById("status");
	var trTypeId = gridObj.getColIndexById("trTypeID");
	var trOAID = gridObj.getColIndexById("trOAID");
	gridObj.forEachRow(function(rowId) {
		 	var status = gridObj.cellById(rowId, trStatus).getValue();
			var attbtyp = gridObj.cellById(rowId, trTypeId).getValue();
			var ordAtbId = gridObj.cellById(rowId, trOAID).getValue();
			if(attbtyp == '53017'){
				POStatus = status;
			}else{
				DOStatus = status;
			}
			inputString+=ordAtbId+'^'+attbtyp+'|';
	});
	document.frmVendor.hInputString.value = "";
    document.frmVendor.POStatus.value = POStatus;
    document.frmVendor.DOStatus.value = DOStatus;
}
var cnt = 0;
function fnSplit(val,pnum)
{
	cnt++;
	var hcnt = parseInt(document.frmVendor.hCnt.value);

	hcnt = hcnt+cnt;
	var priceobj = eval("document.frmVendor.hPrice"+val);
	var price = priceobj.value;

	var itemobj = eval("document.frmVendor.hType"+val);
	var itype = itemobj.value;
	
	var capobj = eval("document.frmVendor.hCapFl"+val);
	var capfl = capobj.value;
	
	var NewQtyHtml = "<span id='Sp"+cnt+"'><BR>&nbsp;&nbsp;&nbsp;<input type='hidden' name='hPartNum"+hcnt+"' value='"+pnum+"'><input type='hidden' name='hPrice"+hcnt+"' value='"+price+"'><input type='hidden' name='hType"+hcnt+"' value='"+itype+"'><input type='hidden' name='hCapFl"+hcnt+"' value='"+capfl+"'><input type='text' size='3' value='' name='Txt_Qty"+hcnt+"' class='InputArea' onFocus=changeBgColor(this,'#AACCE8'); onBlur=changeBgColor(this,'#ffffff'); tabindex=1>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='text' size='12' value='' name='Txt_CNum"+hcnt+"' class='InputArea' onFocus=changeBgColor(this,'#AACCE8'); onBlur=changeBgColor(this,'#ffffff'); tabindex=1>&nbsp;<a href=javascript:fnDelete("+cnt+");><img src=/images/btn_remove.gif border=0></a></span>";
	var QtyObj = eval("document.all.Cell"+val);

	QtyObj.insertAdjacentHTML("beforeEnd",NewQtyHtml);
}

function fnDelete(val)
{
	obj = eval("document.all.Sp"+val);
	obj.innerHTML = "";
}

function fnCallShip()
{
	obj = document.frmVendor.Cbo_ShipTo;
	var obj2 = document.frmVendor.Cbo_Rep;
	if (obj.options[obj.selectedIndex].text == 'Sales Rep')
	{
		obj2.disabled = false;
		obj2.focus();
	}
	else
	{
		obj2.disabled = true;
	}
}

function fnChangeTrack(obj)
{
	changeBgColor(obj,'#ffffff');
	var val = obj.value;
	var len = val.length;

	if (len == 32) // for FedEX Express Shipping
	{
		val = val.substring(16,28);
		obj.value = val;
	}
	else if (len == 22) // for FedEX Ground Shipping
	{
		val = val.substring(7,22);
		obj.value = val;
	}
}

function fnPrintPack()
{
	var val = document.frmVendor.hOrderId.value;
	windowOpener(servletPath+'/GmEditOrderServlet?hMode=PrintPack&hOrdId='+val,"Pack","resizable=yes,scrollbars=yes,top=150,left=150,width=760,height=610");	
}

function fnSetValue()
{
	var obj5 = document.frmVendor.Cbo_Rep;
	for (var j=0;j<obj5.length;j++)
	{
		if (obj5.options[j].value == ShipToId)
		{
			obj5.options[j].selected = true;
			break;
		}
	}
}

// AR2-11 - Check customer PO in DB. 
function fnCheckPO(val){
		var custPoId = document.frmVendor.Txt_PO.value;
		if(TRIM(custPoId) == ''){
			POMessage = '';
			return false; // When PO not entered in cust po text box should not call ajax.
		}
		POMessage = '';
		CustPOValidFl = false;
	    var url = fnAjaxURLAppendCmpInfo("/GmOrderAjaxServlet?&opt="+ encodeURIComponent(val)+"&Txt_PO="+custPoId);
		if (typeof XMLHttpRequest != "undefined") 
		{
		   req = new XMLHttpRequest();
		} 
		else if (window.ActiveXObject) 
		{
		   req = new ActiveXObject("Microsoft.XMLHTTP");
		}
		req.open("GET", url, true);
		if (val == 'CHECKPO'){             //MNTTASK-7099 -- For displaying the avail/exists images.
			req.onreadystatechange = showimage;
		}
		req.send(null);
}

// AR2-11 - Once Available/Dublicate customer PO ID , will be called this method for displaye image.
function showimage() {
    if (req.readyState == 4) {
        if (req.status == 200) {
	        var xmlDoc = req.responseText;	             
	        if(xmlDoc.indexOf("DUPLICATE")!= -1){
	            document.getElementById("DivShowPOIDExists").style.display='inline';
	            POMessage = 'DUPLICATE';
	        }else if(xmlDoc.indexOf("AVAILABLE")!= -1){  
	            document.getElementById("DivShowPOIDAvail").style.display='inline';
	            POMessage = 'AVAILABLE';
	        }
        }
        CustPOValidFl = true;
    }
}

//AR2-11 - When focus cursoe Customer Po text box, will be cleared image.
function fnClearDiv(){
	  document.getElementById("DivShowPOIDExists").style.display='none';
      document.getElementById("DivShowPOIDAvail").style.display='none';	
}

//AR-2-11 - When enter Customer Po and if already exists then L icon will be displayed, Click L icon will be displayed order details in popup window. 
function fnModifyOrder(){
	var strCustPo = document.frmVendor.Txt_PO.value;
	windowOpener('/GmOrderVoidServlet?hMode=Reload&strOpt=CUSTPO&Txt_CustPO='+strCustPo,'OrderDetails',"resizable=yes,scrollbars=yes,top=475,left=420,width=1200,height=300");
}

//AR-2-11 - When enter Customer Po and click tab out 
function fnEditStatus(){
		document.frmVendor.Txt_LogReason.focus();
} 

function fnDisableSubmitButton(val){
	document.frmVendor.Btn_Submit.disabled = val;
}

function fnEnter(){
	var npiflag = 'N';
	var actoinVal = document.frmVendor.hAction.value;	
	if(document.getElementById("hFlag") != null && document.getElementById("hFlag") != 'undefined')
	{
		npiflag = document.getElementById("hFlag").value;
		
	}	
	if(event.keyCode == 13 && actoinVal != 'EditShip' && npiflag == 'N')
	{	 
		var rtVal = false;
			if(!e) var e = window.event;
	        e.cancelBubble = true;
	        e.returnValue = false;
	        if (e.stopPropagation) {
	                e.stopPropagation();
	                e.preventDefault();
	        }
	}

	document.frmVendor.Txt_LogReason.tabIndex= "95";
}
// validate the space - Customer PO
function fnValidatePOSpace(CustPO) {
	var regexp = /\s/g;
	if (regexp.test(CustPO)) {
		Error_Details(message[5019]);
	}
}

function fnSaveComments(){
	if(document.frmVendor.hAction.value == "ACKORD"){
		document.frmVendor.hAction.value = 'AckSavePO';
	}else{
		document.frmVendor.hAction.value = 'SavePO';
	}
	document.frmVendor.hOpt.value = 'SaveComments';
	fnStartProgress();
	document.frmVendor.submit();
}

function fnLoad(){	
	var gridrows = mygrid.getAllRowIds();
    var gridrowsarr = gridrows.toString().split(",");
    var gridrowid;
    var pendQty =0; 
    var partnum = ''; 
    partInv_rowId  = mygrid.getColIndexById("partnum");
    for(var rowid = 0; rowid < gridrowsarr.length; rowid++){	 
		gridrowid = gridrowsarr[rowid];	  
		partnum = mygrid.cellById(gridrowid, partInv_rowId).getValue();
		pendQty = eval(mygrid.cellById(gridrowid, 7).getValue());	 
		partArr[rowid]=new Array(2);
	 	partArr[rowid][0] = partnum;	 	
	 	partArr[rowid][1] = pendQty;
   	}
  }

function fnCalculateTotalQty(pnum){
	var totalPendQty = 0;
	var arrLen = partArr.length;	 
	for (var j=0;j<arrLen ;j++ ){		
		if (partArr[j][0] == pnum){
			totalPendQty = totalPendQty + partArr[j][1];
		}
	}
	return 	totalPendQty;
}

function fnReservePart(partnum,transId,releaseQty,shelfQty){
	totalPendQty = fnCalculateTotalQty(partnum);	 
	windowOpener('/gmLotTrackReport.do?method=fetchPartReserveDetails&strOpt=ReLoad&partNum='+encodeURIComponent(partnum)+'&transId='+transId+'&releaseQty='+totalPendQty+'&shelfQty='+shelfQty,'partreservedetails',"resizable=yes,scrollbars=yes,top=475,left=420,width=1315,height=480");	
}

//This function used to export Ipad PDF File. PMT-10580. 

function fnExportFile(fileID){

	var uploadDir="DOFILEARACSLOC";
	windowOpener("/GmCommonFileOpenServlet?strOpt=IPADORDERS&compLocale="+companyLocale+"&uploadString="+uploadDir+"&fileName="+fileID+".pdf");
}

function IsNumeric(strString) //  check for valid numeric strings	
{
	if(!/\D/.test(strString)) return true;//IF NUMBER
	else if(/^\d+\.\d+$/.test(strString)) return true;//IF A DECIMAL NUMBER HAVING AN INTEGER ON EITHER SIDE OF THE DOT(.)
	else return false;
}

//This function used when click Enter for form submit. PMT-13104. 
function fnSubmitForm(){
	if(event.keyCode == 13)
	{	
		var focusedElement = document.activeElement.value;
		if(focusedElement == 'Submit'){
			fnSubmit();
		}
	}
}

function fnSetNPINum(obj){
	var surgeonnm = document.getElementById("Txt_Surgeon").value;
	document.getElementById("searchTxt_Npi").value = surgeonnm;
	fnSaveParentRow('');
}

function fnSetSurgeonNm(obj){
	var npiId = document.getElementById("Txt_Npi").value;
	document.getElementById("searchTxt_Surgeon").value = npiId;
	fnSaveParentRow('');
}

//While tabout from NPI #/Surgeon name field
function fnAutoFocus(obj){
	// After Tabout from NPI#, it should focus on Surgeon name
	// Aftet tabout from Surgeon name, the value should be saved
	var npiId = document.getElementById("searchTxt_Npi").value;
	var surgeonnm = document.getElementById("searchTxt_Surgeon").value;
	var acceptNum;
	var errorFl;
	// NPI field should accept only numbers and surgeon field should accept only characters with dot
	if(npiId != ''){
		acceptNum = /[^0-9]/g;
        errorFl = acceptNum.test(npiId);
        if (errorFl) {  /*Accept only Digits in NPI# field*/
        	Error_Details("Please provide NPI# in digits");
        }
	}
	if(surgeonnm != ''){
		acceptNum = /^[a-zA-Z\.\, ]*$/;
        errorFl = acceptNum.test(surgeonnm);
        if (!errorFl) {  /*Accept only Digits in NPI# field*/
        	Error_Details("Please provide surgeon name in letters");
        }
	}
	
	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	} 
	if(obj.id == 'searchTxt_Npi'){
		document.getElementById("searchTxt_Surgeon").focus();
	}else if(npiId != '' || surgeonnm!= ''){
		fnSaveParentRow('');
	}
}
/*fnOpenDoFile :TO Open Uploaded DO file information*/
function fnOpenDOFile(strOrderId){

	if(strOrderId !=''){
		  w4 =  createDhtmlxWindow(1000,400,true);
		  w4.setText("Upload DO Document Information");
		  var ajaxUrl = fnAjaxURLAppendCmpInfo("/gmDODocumentUpload.do?method=loadOrderUpload&strOpt=Load&haction=View&uploadTypeId=26240412&strOrderId="+strOrderId);
		  w4.attachURL(ajaxUrl,"Upload Files");	  
		  return false;	
}
}

/*fnUploadFile :TO  Upload DO files */
function fnUploadFile(strOrderId){

    document.frmVendor.action ='/gmDODocumentUpload.do?method=loadOrderUpload&strOpt=Load&uploadTypeId=26240412&strOrderId='+strOrderId;
	fnStartProgress();
	document.frmVendor.submit();
	
}
//function is used to open adjustments
function fnOpenAdjustment(orderid){    
	windowOpener('/gmPartPriceAdjustmentRptAction.do?method=fetchAdjustmentDetails&accId='+ orderid , "AdjustmentDetails","resizable=yes,scrollbars=yes,top=250,left=300,width=1065,height=470");	
}

/*fnCommercialInv : TO Generate Commercial Invoice*/
function fnCommercialInv() {
	var ordId = document.frmVendor.hOrderId.value;
	windowOpener("/gmProformaInvoice.do?orderid=" + ordId
			+ "&strOpt=PROFORMA", "Print",
			"resizable=yes,scrollbars=yes,top=150,left=150,width=800,height=750");
}


