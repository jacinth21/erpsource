function fnLoad(){
	
	var npiNum = document.frmNPIProcess.searchnpinum.value;
	// NPI number should accept only numbers
	var acceptNum = /[^0-9]/g;
    var errorFl = acceptNum.test(npiNum);
    if (errorFl) {  /*Accept only Digits in NPI# field*/
    	Error_Details("Please provide NPI# in digits");
    }
    
	objStartDt = document.frmNPIProcess.npiFromDt;
	objEndDt = document.frmNPIProcess.npiToDt;
	var fromToDiff = dateDiff(document.frmNPIProcess.npiFromDt.value, document.frmNPIProcess.npiToDt.value, dateFmt);

	if(objStartDt.value != ""){
	
		CommonDateValidation(document.frmNPIProcess.npiFromDt,dateFmt,Error_Details_Trans(message[10523],dateFmt));	 
	}

	if(objEndDt.value != ""){ 
		
		CommonDateValidation( document.frmNPIProcess.npiToDt,dateFmt,Error_Details_Trans(message[10524],dateFmt));	 
	}
	if(objStartDt.value != '' && objEndDt.value == ''){
		Error_Details(message_prodmgmnt[152]);
	}else if(objStartDt.value == '' && objEndDt.value != '' ){
		Error_Details(message_prodmgmnt[153]);
	}else if (objStartDt.value != '' && objEndDt.value != '' && fromToDiff<0){
		Error_Details(message_prodmgmnt[154]);
	}
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	

	fnStartProgress('Y');
	document.frmNPIProcess.strOpt.value = "Reload";
	document.frmNPIProcess.submit();
}

var gridObj;
function fnOnPageLoad(){

	if(objGridData.value != ''){ 
		gridObj = initGridData('dataGridDiv',objGridData);
		var colIndex=gridObj.getColIndexById("orderID");
		var colNpiIndex=gridObj.getColIndexById("npiNumber");
		gridObj.groupBy(colIndex);//For type ID
		gridObj.collapseAllGroups();
		gridObj.expandAllGroups();
		gridObj.setColumnHidden(1,true);
		if(colNpiIndex != undefined){
			gridObj.attachHeader('#text_filter,,#select_filter,#select_filter,#text_filter,#select_filter,#select_filter,#text_filter,#text_filter');
		}else{
			gridObj.attachHeader('#text_filter,,#select_filter,#text_filter,#select_filter,#select_filter,#text_filter,#text_filter');
		}
		gridObj.enableAutoHeight(true);
		gridObj.objBox.style.overflowX = "hidden";
	}	

}

//This function is used to initiate grid
function initGridData(divRef,gridData){
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);
	gObj.enableTooltips("true,true,true,true,true,true,true,true");
	gObj.init();
	gObj.loadXMLString(gridData);	
	return gObj;	
}

// This function is used to open the edit npi screen
function fnOpenEditScreen(orderid, accid, accname, repname){
	document.getElementById("npidtls").style.display = 'inline';
	$("#hRefId").val(orderid);
	$("#hAccountNm").val(repname);
	$("#hRepNm").val(accname);
	$("#hAccountId").val(accid);
	$("#myModal").modal('show');
}

//This function is used to display the order id in the excel
function fnExport(str){
	gridObj.setColumnHidden(1,false);
	gridObj.setColumnHidden(0,true);
	if(str=="excel"){
		gridObj.toExcel('/phpapp/excel/generate.php');
	}else{
		gridObj.toExcel('/phpapp/pdf/generate.php');
	}
	gridObj.setColumnHidden(1,true);
	gridObj.setColumnHidden(0,false);
}