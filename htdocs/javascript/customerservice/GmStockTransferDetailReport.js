var gridObj;
//To load report 
function fnOnPageLoad() {

	if (objGridData.value != '') {
		gridObj = initGridData('stockTranferDtlRpt', objGridData);
	}
}
//To initiate the dhtmlx grid
function initGridData(divRef, gridData) {
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.init();
    gObj.loadXMLString(gridData);
	gObj.attachHeader('#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter');

	return gObj;
}

//To download excel;
function fnExcel(){
	gridObj.toExcel('/phpapp/excel/generate.php');
}

//To load the report
function fnLoad(){
	var objFromDt = "";
	var objToDt="";
	var fromDate = "";
	var toDate="";
	var varianceQty ="";
	var variance="";
	
	var errDate="";
	var errVariance="";
	var errVarianceQty="";
	
	objFromDt = document.frmStockTransferReport.fromDate;
	objToDt = document.frmStockTransferReport.toDate;
	
	fromDate=objFromDt.value;
	toDate = objToDt.value;
	if((fromDate == "") && (toDate != "")){
		fnValidateTxtFld('fromDate',message[10527]);
	}else if((fromDate != "") && (toDate == "")){
		fnValidateTxtFld('toDate',message[10528]);
	}
	
	varianceQty =document.frmStockTransferReport.varianceQty.value;
	variance=document.frmStockTransferReport.variance.value;
	
	CommonDateValidation(objFromDt,date_format,message[10507]);
	CommonDateValidation(objToDt,date_format,message[10508]);
	 
    if(varianceQty == '' && variance != "0"){
       Error_Details(message[5302]);
    }
	if((varianceQty != '' && variance =="0" )){
		  Error_Details(message[5303]);
	}
	if (dateDiff(fromDate,toDate,date_format) < 0){
		Error_Details(message[10579]);
	}
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false; 
	}
	fnStartProgress("Y");
    document.frmStockTransferReport.submit();
    
}
