var combobox;
var codeId,codeNm;
var partNum ='';
var partDesc = '';
var liPrice = '';
var lPrice = '';
var cPrice = '';
var ePrice = '';
function fnSubmit(){
	
	gridObj.editStop();// return value from editor(if presents) to cell and closes editor
	var gridrowid;	
	var inputString	= '';	
	var gridrows 	= gridObj.getChangedRows(",");
	var gridrowsarr = gridrows.toString().split(",");
		if(gridrows.length==0){
			inputString = "";
		}else{
			var columnCount = gridObj.getColumnsNum();
														
			for (var rowid 	= 0; rowid < gridrowsarr.length; rowid++){
			gridrowid 	= gridrowsarr[rowid];				
					
				for(var col=0;col<columnCount;col++){					
					inputString+=gridObj.cellById(gridrowid,col).getValue()+'^';
				}
				inputString+="|";
			}			
		}	
	
	var strMode		= document.frmPartPriceUpdate.hMode.value;	
	var projectId 	= document.all.Cbo_ProjId.value;
	var i;
	var count 		= 0;	
	var error_msg	= '';
	
	if(inputString == ''){
		Error_Details(message_operations[430]);
	}	
	var rowArray = inputString.split("|");
	var strErrMessage ="";	
	if(gridrows.length != 0){
		
		for ( var rowid = 0; rowid < gridrowsarr.length; rowid++){
			var cellArray = rowArray[rowid].split("^");	
			var rowCount =0;
			 for(i=2;i<=5;i++){	
				
				if((cellArray[i]<0)||isNaN(cellArray[i])){	
					rowCount++;		
				}			
			}
			 if(rowCount >0){
				 count = count+rowCount;
				 strErrMessage = strErrMessage+cellArray[0]+"<br>";
			 }
		}		
		
		if(count>0){		
			Error_Details(Error_Details_Trans(message_operations[431],strErrMessage));
		}
		
	}
	
	if (ErrorCount > 0){		
		Error_Show();
		Error_Clear();
		return false;
	}
	document.frmPartPriceUpdate.hProjectId.value	= projectId;
	document.frmPartPriceUpdate.hInputString.value	= inputString;	
	document.frmPartPriceUpdate.hMode.value			= strMode;
	
	document.frmPartPriceUpdate.submit();
}
function fnBatchSubmit(){
	
	var objval 		= TRIM(document.all.Txt_Batch.value);
	var projectId 	= document.frmPartPriceUpdate.Cbo_ProjId.value;
	document.frmPartPriceUpdate.hProjectId.value	= projectId;
	document.frmPartPriceUpdate.hInputString.value	= objval;
    count = 0;
    var rownum;
	var errParts ='';
	var inStringArray = objval.split("\n") ;
	for(var i=0 ;i< inStringArray.length; i++){
		var inRowArray = inStringArray[i].split(",");
		
		if(inRowArray.length > 5){
			count++;
		errParts =  errParts+inRowArray[0]+"<BR>";
			rownum =i+1;
		}
	}
	if(count > 0){
		Error_Details(Error_Details_Trans(message_operations[432],errParts));
	}
	var objlen 		= objval.length;	
	if (objlen == 0){
		Error_Details(message_operations[433]);
	}
	if (objlen > 35000){
		Error_Details(message_operations[434]);
	}		
	if (ErrorCount > 0){
			Error_Show();
			Error_Clear();
			return false;
	}else{
		if (confirm(Error_Details_Trans(message_operations[435],projectId)));	{
			document.frmPartPriceUpdate.submit();
		}
	}
}
function fnOnLoad(){	
	if(objGridData != ''){
		initializeGrid();
   	}
}
function fnCallAcBlur(obj){	
	var type='';
	if (type == "Account") {
		fnAcBlur(obj);
	}else {
		if (obj.value == 0){
			document.all.Cbo_ProjId.value = '';
			}else		
			document.all.Cbo_ProjId.value = obj.value;
	}
}
function fnLoad(){
	var projectId = document.all.Cbo_ProjId.value;
	if(projectId == "" || projectId ==0){
		Error_Details(message_operations[436]);
		}
	if (ErrorCount > 0){
			Error_Show();
			Error_Clear();
			return false;
	}	
	document.frmPartPriceUpdate.hAction.value 		= "report";
	document.frmPartPriceUpdate.hCol.value 			= "";
	document.frmPartPriceUpdate.hProjectId.value	= projectId;
	document.frmPartPriceUpdate.submit();
}
function enableMarker(){
	gridObj.enableMarkedCells();
}
function disableMarker(){
	gridObj.enableMarkedCells(false);
}
function doundo(){
	gridObj.doUndo();
}
function doredo(){
	gridObj.doRedo();
}
function docopy(){
	gridObj.setCSVDelimiter("\t");
    	gridObj.copyBlockToClipboard();
	//gridObj._HideSelection();
}
function addRow(){
	gridObj.addRow(gridObj.getUID(),'');
	var divHeight=document.getElementById('dataGridDiv').offsetHeight;
	document.getElementById('dataGridDiv').style.height 	= (divHeight)+'px';
	document.getElementById('dataGridDiv').style.overflow 	= 'auto';	
}
function addRowFromClipboard(){
	
	   	var cbData 			= gridObj.fromClipBoard();   		
		var rowCount		= cbData.split("\n"); 
		var ignoredRows 	=message_operations[462];
		var ignoredRowCount = 0;
		
		for (var i=0; i<rowCount.length; i++){
			var rowData = rowCount[i].split("\t");
			
			for(var j=0;j<rowData.length;j++){
				cb_columnName=rowData[0];
				break;
			}
			var dropDownVal =varNameDropDownValues.split("#@#");
			if((varNameDropDownValues.toUpperCase()).indexOf(TRIM(cb_columnName.toUpperCase()))== -1){
				ignoredRows = ignoredRows+cb_columnName+'<br>';
				ignoredRowCount++;
			}

			var db_colName;
			for (var dd=0; dd<dropDownVal.length; dd++){
			
				db_colName = dropDownVal[dd];
				var row_id= TRIM(db_colName.substring(db_colName.indexOf("#")+1,db_colName.length));
				
				db_colName = db_colName.substring(0,db_colName.indexOf("#"));
				
				if(row_id.length>0 && !checkForDuplicateRow(row_id) && TRIM(cb_columnName.toUpperCase()) == TRIM(db_colName.toUpperCase())){
					gridObj.setCSVDelimiter("\t");
					gridObj.setDelimiter("\t");
					gridObj.addRow(row_id,rowCount[i]);
					gridObj.setRowAttribute(row_id,"row_added","Y");
					gridObj.setDelimiter(",");
				}
			}
		}
		if(ignoredRowCount>0){
			Error_Clear();
			Error_Details(ignoredRows);				
			Error_Show();
			Error_Clear();
		}
	}
function enableSelectedRow(){
	var gridrows	= gridObj.getSelectedRowId();

	if(gridrows!=undefined){
		
		var gridrowsarr = gridrows.toString().split(",");
		
		for (var rowid = 0; rowid < gridrowsarr.length; rowid++) {
			gridrowid = gridrowsarr[rowid];
			gridObj.lockRow(gridrowid,false);
			gridObj.setRowColor(gridrowid,"");
			gridObj.setRowAttribute(gridrowid,"row_deleted","");
			
			setRowAsModified(gridrowid,true);
		}
	}else{
		Error_Clear();
		Error_Details(message_operations[437]);				
		Error_Show();
		Error_Clear();
	}
}
function keyPressed(code, ctrl, shift) {
    if (code == 67 && ctrl) {
        /*if (!gridObj._selectionArea)
            return alert("You need to select a block area in grid first");
            */
    	gridObj.setCSVDelimiter("\t");
    	gridObj.copyBlockToClipboard();
    }
    
    if (code == 86 && ctrl) {
    	if(gridObj._selectionArea!=null){
			var colIndex = gridObj._selectionArea.LeftTopCol;
			var cb_columnName = '';
			
			if(colIndex!=undefined && colIndex==0){
				alert(message_operations[438]);
			}else{
					gridObj.setCSVDelimiter("\t");
				gridObj.pasteBlockFromClipboard();
			}
			//gridObj._HideSelection();
		}else{
			alert(message_operations[439] );
		}
    }
	// This code is to block delete the content from the grid.
	if(code==46&&!ctrl&&!shift){
		
		if(gridObj._selectionArea!=null){
			var area=gridObj._selectionArea
			var leftTopCol=area.LeftTopCol;
			var leftTopRow=area.LeftTopRow;
			var rightBottomCol=area.RightBottomCol;
			var rightBottomRow=area.RightBottomRow;
			
			for (var i=leftTopRow; i<=rightBottomRow; i++){
				
				setRowAsModified(gridObj.getRowId(i),true);

				for (var j=leftTopCol; j<=rightBottomCol; j++){
					gridObj.cellByIndex(i,j).setValue("");
				}
			}
			gridObj._HideSelection();
		}
	}
    return true;
}

function setRowAsModified(rowid,modified){
	gridObj.forEachCell(rowid,function(obj){
       obj.cell.wasChanged = modified;
		});
}
function removeSelectedRow(){

	var gridrows=gridObj.getSelectedRowId();
	if(gridrows!=undefined){
		var NameCellValue ;
		var gridrowsarr = gridrows.toString().split(",");
		var added_row;
		for (var rowid = 0; rowid < gridrowsarr.length; rowid++) {
			gridrowid = gridrowsarr[rowid];
			
			NameCellValue = gridObj.cellById(gridrowid, 0).getValue();
			added_row = gridObj.getRowAttribute(gridrowid,"row_added");
			
			if(TRIM(NameCellValue).length==0 || (added_row!=undefined &&added_row=="Y")){
				gridObj.deleteRow(gridrowid);
			}else{
				gridObj.lockRow(gridrowid,true);
				gridObj.setRowColor(gridrowid,"pink");
				gridObj.setRowAttribute(gridrowid,"row_deleted","Y");
			}
			setRowAsModified(gridrowid,true);
		}
	}else{
		Error_Clear();
		Error_Details(message_operations[440]);				
		Error_Show();
		Error_Clear();
	}
}
function pasteToGrid(){
	
	if(gridObj._selectionArea!=null){
		var colIndex = gridObj._selectionArea.LeftTopCol; 
		if(colIndex!=undefined && colIndex !=0){
			gridObj.pasteBlockFromClipboard();
			//gridObj._HideSelection();
		}else{
			alert(message_operations[441]);
			// If the user is trying to paste values to Part# column,call addRowFromClipboard.
			/*   var cbData = gridObj.fromClipBoard();   		
				var rowCount = cbData.split("\n"); 
				var row_id;		
				for (var i=0; i<rowCount.length; i++){
					var rowData = rowCount[i].split("\t");
				
					for(var j=0;j<rowData.length;j++){
						cb_columnName=rowData[0];
						break;
					}
					row_id=checkBeforeAddRow(cb_columnName);
					
					if(row_id.length>0){
						gridObj.setCSVDelimiter("\t");
						gridObj.setDelimiter("\t");
						//Changing the row id with the newly selected value from name drop down.
					
						gridObj.changeRowId(gridObj.getSelectedRowId(),row_id);
			
						gridObj.setRowAttribute(row_id,"row_added","Y");
						gridObj.setDelimiter(",");
					}
				}*/
		}
	}else{
		alert(message_operations[442]);
	}
}
function fnResetBatch(){
	self.location.href = "/gmPartNumPriceUpdate.do?strOpt=Batch";
}
function fnReset(){
	self.location.href = "/gmPartNumPriceUpdate.do?strAction=report";
}

function checkForDuplicateRow(row_id){
	if(gridObj.doesRowExist(row_id)){
		return true;
	}
}
function initializeGrid(){		


	    var dataHost = {};
	    var rows = [], i = 0;
	    var JSON_Array_obj = "";
	JSON_Array_obj = JSON.parse(jsonData);
	$.each(JSON_Array_obj, function(index, jsonObject) {
		partNum = jsonObject.ID;
		partDesc = jsonObject.PDESC;
		liPrice = jsonObject.LIPRICE;
		lPrice = jsonObject.LPRICE;
		cPrice = jsonObject.CPRICE;
		ePrice = jsonObject.EPRICE;

		rows[i] = {};
		rows[i].id = partNum;
		rows[i].data = [];
		
		rows[i].data[0] = partNum;
		rows[i].data[1] = partDesc;
		rows[i].data[2] = liPrice;
		rows[i].data[3] = lPrice;
		rows[i].data[4] = cPrice;
		rows[i].data[5] = ePrice;
	
		i++;
	});
   dataHost.rows = rows;
		//gridObj = initGrid('dataGridDiv',objGridData);
		gridObj = new dhtmlXGridObject("dataGridDiv");
		gridObj.setImagePath("/extweb/suite/5.2.0/codebase/imgs/");
		gridObj.setHeader("Part Number,Description,Tier 1 Price, Tier 2 Price,Tier 3 Price,Tier 4 Price");
		gridObj.setInitWidths("100,329,100,100,100,100");
		gridObj.setColAlign("left,left,right,right,right,right");
		gridObj.setColTypes("coro,ro,ed,ed,ed,ed");
		gridObj.setColSorting("str,str,int,str,str,str");
		gridObj.setColumnIds("partNum,partDesc,liPrice,lPrice,cPrice,ePrice");
		
		
		gridObj.enableEditEvents(false, true, true);
		gridObj.attachEvent("onKeyPress", keyPressed);
		gridObj.attachEvent("onEditCell",doOnCellEdit);
		
		gridObj.attachEvent("onRowPaste",function(id){
        gridObj.forEachCell(id,function(obj){
                   obj.cell.wasChanged = true;
                   });
			});		
		
		//set drop down values for grid body to show pttype
	    combobox = gridObj.getCombo(0);
		combobox.clear();

		for(var i=0;i<partNumListLen;i++){
			arr=eval("partArr"+i);
			arrobj = arr.split(",");
			codeId = arrobj[0];
			codeNm =  arrobj[1];
			combobox.put("0","[Choose one]");
		combobox.put(codeId,codeNm);
		}
		combobox.save();
		gridObj.init();	
			
		gridObj.enableAlterCss("even","uneven");
		//gridObj.load(objGridData);
		gridObj.parse(dataHost,"json");
		gridObj.setAwaitedRowHeight(200);
		gridObj.enableBlockSelection(true);
		gridObj.copyBlockToClipboard(true);
		gridObj.submitOnlyChanged(true);
		gridObj.submitOnlyRowID(false);
		gridObj.enableAutoHeight(true,300,true);		
		gridObj.enableUndoRedo();
		gridObj.enableMultiselect(true);
		
		
} 	
function doOnCellEdit(stage, rowId, cellInd, nValue, oValue) {
	
	if(stage==2 && nValue!=oValue){		
		// This piece of code will be executed only when the modified column is Part Number drop down.
		// 0 Column Index corresponds to the Part Number drop down.
		if(cellInd == 0){
			if(checkForDuplicateRow(nValue)){
				Error_Details(message_operations[443]);	
				}
		}
		}		
		if(ErrorCount > 0){
			Error_Show();
			Error_Clear();
			return false;
		}
		return true;
}
function fnCheckNull(data)
{
	if(data==null || data=='null')
		return '';
	else
		return data;
}
function isChar(nval){
		var objval = TRIM(nval);
		var objRegExp  =/(^(.*)-?\d\d*$)/;
		var objDecimal= /^[0-9]+(\.[0-9]{1,2})?$/;
		if(!objDecimal.test(objval)||!objRegExp.test(objval)){	
			return true;
		}
	}
function isNegative(nval){	
	if((nval < 0 ) || (nval = '')){
		return true;
	}
}
