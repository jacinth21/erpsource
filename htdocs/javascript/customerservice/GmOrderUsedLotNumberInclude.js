
// this function will fire the AJAX (on page load - Initiate Returns and Issue credit/debit)

function fnFetchOrderUsageDtls(orderid, pnum, invoiceId) {
	// Using Ajax call for load the report
	var response;
	// fnStartProgress();
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmOrderUsageLotRpt.do?method=fetchOrderUsageDtls&orderId='
			+ orderid
			+ '&partNumber='
			+ pnum
			+ '&invoiceId='
			+ invoiceId
			+ '&ramdomId='
			+ new Date().getTime());
	
	response = dhtmlxAjax.get(ajaxUrl, fnFetchUsedLOTCallback);
}

// this function used to show the used lot number details (call back)
function fnFetchUsedLOTCallback(loader) {
	var response = loader.xmlDoc.responseText;
	if (response != "") {
		// to set the global value (used lot data flag)
		usageLotDataFl = 'Y';
		// to init grid

		var setHeader = [ lblpartNum, lblpartDesc, lblcurrQty,	lblqty, lblLotNum, lblDDTNum ];
		var setColIds = "pnum,pdesc,usageqty,returnqty,usagelot,ddtid";
		var setColTypes = "ro,ro,ro,ed,ro,ro";
		var setColAlign = "left,left,right,right,left,left";
		var setColSorting = "str,str,int,int,str,str";
		var enableTooltips = "true,true,true,true,true,true";
		var setInitWidths = "120,*,80,80,100,150";
		var gridHeight = "";

		document.getElementById("dataGridDiv").style.display = 'block';
		document.getElementById("DivNothingMessage").style.display = 'none';
		//
		gridObj = initGridObject('dataGridDiv');
		gridObj = fnCustomTypes(gridObj, setHeader, setColIds, setInitWidths,
				setColAlign, setColTypes, setColSorting, enableTooltips, 600);

		gridObj = loadDhtmlxGridByJsonData(gridObj, response);
		gridObj.enableResizing("true,true,false,false,true,false");
		firstTimeFl = true;
		// to set the header id
		partNum_cellId = gridObj.getColIndexById("pnum");
		partDesc_cellId = gridObj.getColIndexById("pdesc");
		usageQty_cellId = gridObj.getColIndexById("usageqty");
		usageLot_cellId = gridObj.getColIndexById("usagelot");
		ddtId_cellId = gridObj.getColIndexById("ddtid");
		returnQty_cellId = gridObj.getColIndexById("returnqty");

		// based on DDT flag to hide the data (japan company)
		if (orderDDTFl == '') {
			gridObj.setColumnHidden(ddtId_cellId, true);
		}

		// For Single click focus to Qty field.
		gridObj.enableEditEvents(true);

		// For enable the tab order
		gridObj.enableEditTabOnly(true);

	} else {
		document.getElementById("dataGridDiv").style.display = 'none';
		document.getElementById("DivNothingMessage").style.display = 'block';

	} // response end
} 

// This Function Used to set custom types conditions in Grid

function fnCustomTypes(gObj, setHeader, setColIds, setInitWidths, setColAlign,
		setColTypes, setColSorting, enableTooltips, gridHeight) {

	gObj.setHeader(setHeader);
	var colCount = setHeader.length;

	gObj.setInitWidths(setInitWidths);
	gObj.setColAlign(setColAlign);
	gObj.setColTypes(setColTypes);
	gObj.setColSorting(setColSorting);
	gObj.enableTooltips(enableTooltips);
	// gObj.attachHeader(setFilter);
	gObj.setColumnIds(setColIds);
	gObj.enableAutoHeight(true, gridHeight, true);

	return gObj;
}



//this function used to put all the Returns qty.
function fnPutUsedLotQtyMap (){
	// to loop the grid
	gridObj.forEachRow(function(rowId) {
		part_val = gridObj.cellById(rowId, partNum_cellId).getValue();
		
		// PC-2388: Unable to proceed Initiate Return
		// Currently below object consider as string so Used LOT qty more then
		// 10 not consider as a number format. To convert as a number and compare the data.
		
		usedLotQty_val = fnGetNumberVal(gridObj.cellById(rowId, returnQty_cellId).getValue());
		recordedQty_val = fnGetNumberVal(gridObj.cellById(rowId, usageQty_cellId).getValue()); 
		
		if(usedLotQty_val != 0 && usedLotQty_val <= recordedQty_val){
			// to get the values from hashmap
			mapUsedLot_val = usedLotMap.get (part_val);
			
			if(mapUsedLot_val == undefined){
				mapUsedLot_val = parseInt(0);
			}
			// to add the returns qty values.
			mapUsedLot_val = mapUsedLot_val + parseInt(usedLotQty_val);
			
			// set the values to map
			usedLotMap.set(part_val, mapUsedLot_val);
		}
	});	
}


// this function used to compare the returns/issue qty and Used Lot qty
function fnCompareUsedLotQty() {
	var dataValidFl = true;
	var errorPartStr = '';

	var totalReturnPartCnt = returnsQtyMap.size;
	var totalUsedLotPartCnt = usedLotMap.size;
	
		// loop the returns map
		returnsQtyMap
				.forEach(function(m_returnQty, m_partNum) {
					console.log(m_partNum + ' = ' + m_returnQty);
					// to get the used lot qty and compare
					var m_usedLotQtyVal = usedLotMap.get(m_partNum);
					if (m_usedLotQtyVal == undefined
							|| m_usedLotQtyVal != m_returnQty) {
						errorPartStr += m_partNum + ', ';
					}
				});

		if (errorPartStr != '') {
			dataValidFl = false;
			errorPartStr = errorPartStr.substring(0, errorPartStr.length - 2);
			Error_Details(Error_Details_Trans(message_operations[848],errorPartStr));
		}

	return dataValidFl;
}

// this function used to creat the input string - based on user entered qty
function fnCreateUsedLotInputStr (){
	var usageStr = '';
	gridObj.forEachRow(function(rowId) {
		//orderId_val = gridObj.cellById(rowId, orderId_cellId).getValue();
		part_val = gridObj.cellById(rowId, partNum_cellId).getValue();
		
		// PC-2388: Unable to proceed Initiate Return
		// Currently below object consider as string so Used LOT qty more then
		// 10 not consider as a number format. To convert as a number and compare the data.
		
		reutnQty_val = fnGetNumberVal (gridObj.cellById(rowId, returnQty_cellId).getValue());
		usageLot_val = gridObj.cellById(rowId, usageLot_cellId).getValue();
		ddt_val = gridObj.cellById(rowId, ddtId_cellId).getValue();
		recordedQty_val = fnGetNumberVal (gridObj.cellById(rowId, usageQty_cellId).getValue());
		
		//
		if(reutnQty_val != 0 && reutnQty_val <= recordedQty_val){
			//Part ^ Qty ^ Usage Lot ^ DDT number|
			usageStr += part_val +'^' + reutnQty_val +'^' + usageLot_val +'^' + ddt_val +'|';
		}
	});
	// to set the used Lot string 
	document.getElementById ('hUsedLotStr').value = usageStr;
}

// this function used to return the number values. if not an number then always
// returns to 0
function fnGetNumberVal(qty_val) {
	var return_qty_val = 0;
	// check not an number and parseInt
	if (!isNaN(qty_val)) {
		return_qty_val = parseInt(qty_val);
	}

	// return the values
	return return_qty_val;
}