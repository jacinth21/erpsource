
// functions for GmOrderTagRecord.jsp

function fnOnPageLoad()
{
	document.frmOrderTagRecord.setId.value = "";
	document.frmOrderTagRecord.tagId.value = "";
	 document.all.searchsetId.value = "";
	document.frmOrderTagRecord.missing.checked = false;
	document.all.searchsetId.disabled = true;
	var frmTagId = document.frmOrderTagRecord.tagId.value;
	var frmSetId = document.frmOrderTagRecord.setId.value;
	     document.frmOrderTagRecord.setId.disabled = true;
		 document.frmOrderTagRecord.missing.disabled = false;  
		if (objGridData.value!= '') {
				gridObj = initGridData('addingtagsRpt', objGridData);
			} 
}

function initGridData(divRef, gridData) {
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.init();
	gObj.loadXMLString(gridData);
    return gObj;
}
//
function fnMissingCheck(check){
   if(check.checked==true) {
	   document.all.searchsetId.disabled = false;
	    document.frmOrderTagRecord.tagId.value = '';
	    document.frmOrderTagRecord.tagId.disabled = true;
	    } else {
	   document.frmOrderTagRecord.tagId.disabled = false;
	   document.all.searchsetId.disabled = '';
	   document.all.searchsetId.value = '';
	   document.all.searchsetId.disabled = true;
	   }
   }

// function to delete the tag details
function fnDelete(paramTagID,paramRefID,paramAccID)
{ 
	if(paramTagID != '' && paramRefID != '' && paramAccID != ''){
       dhtmlxAjax.get('/gmOrderTagRecordAction.do?method=cancelTagDetails&tagInfoId='+paramTagID +'&ramdomId='+ Math.random(),function(loader1){});
       document.frmOrderTagRecord.action = "/gmOrderTagRecordAction.do?method=fetchTagRecordInfo&tagRefId="+paramRefID+"&accountId="+paramAccID; 
	   document.frmOrderTagRecord.submit();
	}
}
function fnProcessRecordTags() {
	var frmSetId = '';
	if(document.frmOrderTagRecord.setId.value!= undefined) {
		frmSetId = document.frmOrderTagRecord.setId.value;
	}
	var frmTagId = '';
	if(document.frmOrderTagRecord.tagId.value!= undefined) {
		frmTagId = document.frmOrderTagRecord.tagId.value;
	}
	var frmMissing = '';
	if(document.frmOrderTagRecord.missing.checked!= undefined) {
		frmMissing = document.frmOrderTagRecord.missing.checked;
	}
	var paramMissing = "";
	  if(frmMissing == true){ 
		  var frmSetId=document.frmOrderTagRecord.setId.value;
		  if(frmSetId.length > 0){
			  paramMissing = "Y";
			  var setId=document.frmOrderTagRecord.setId.value;
				document.frmOrderTagRecord.tagRefID.value = strRefDoId;
				document.frmOrderTagRecord.action = "/gmOrderTagRecordAction.do?method=processRecordTags&pMissing="+paramMissing+"&accountId="+accountId+"&tagRefId="+tagRefId+"&setId="+setId+"&tempId="+tempId+"&"+fnAppendCompanyInfo(); 
				fnStartProgress();	
				document.frmOrderTagRecord.submit();
				document.frmOrderTagRecord.tagId.value = "";
				document.frmOrderTagRecord.setId.value = "";
				document.frmOrderTagRecord.missing = false;
		    }
	  }else{
			var frmTagId=document.frmOrderTagRecord.tagId.value;
				 if(frmTagId.length > 0){
				  paramMissing = "N";
				  var frmTagId=document.frmOrderTagRecord.tagId.value;
				  var frmSetId=document.frmOrderTagRecord.setId.value;
				  var ordid = window.parent.document.getElementById("txtOrdId");
					document.frmOrderTagRecord.tagRefID.value = strRefDoId;
					document.frmOrderTagRecord.action = "/gmOrderTagRecordAction.do?method=processRecordTags&pMissing="+paramMissing+"&accountId="+accountId+"&tagRefId="+tagRefId+"&tempId="+tempId+"&"+fnAppendCompanyInfo(); 
					fnStartProgress();
					document.frmOrderTagRecord.submit();
					document.frmOrderTagRecord.tagId.value = "";
					document.frmOrderTagRecord.setId.value = "";
					document.frmOrderTagRecord.missing = false;
				   } 
	  }
	}
