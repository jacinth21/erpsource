function fnGo()
{
	fnValidateDropDn('Cbo_AccId',TypeName+' Name ');
	// accountType is Global variable (its come from PhoneOrder js file).
	if(TypeName == 'Account' && accountType == '70114'){ // 70114 - ICS Account
		Error_Details(message_operations[195]);
	}
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	document.frmAccount.hMode.value = "Reload";
	fnStartProgress();
	document.frmAccount.submit();
}

function fnCallProject(val)
{
	document.frmAccount.hId.value = val;
	document.frmAccount.submit();
}

function fnCallEdit(val)
{
	document.frmAccount.PartNum.value = val;
	document.frmAccount.hMode.value = "EditAccPrice";
	document.frmAccount.hType.value = type;
	document.frmAccount.action = "/GmPartPriceEditServlet";
	document.frmAccount.submit();
}

function fnLoadSubmit()
{
	document.frmAccount.hMode.value = "LoadPrices";	
	document.frmAccount.submit();
}

function fnBatchUpdate()
{
	Error_Clear();
	document.frmAccount.hMode.value = "BatchUpdate";
	var obj = document.frmAccount.Txt_Batch;
	var objval = TRIM(document.frmAccount.Txt_Batch.value);
	document.frmAccount.Txt_Batch.value = objval;
	var objlen = objval.length;
	var acname = document.frmAccount.Cbo_AccId.value;
	fnValidateDropDn('Txt_AccId', lblName);
	if (objlen == 0)
	{
		Error_Details(message_operations[196]);
	}
	if (objlen > 35000)
	{
		Error_Details(message_operations[197]);
	}

    // If either of the first two validations fail stop here
	if (ErrorCount > 0)
	{
			Error_Show();
			return false;
	}

	// Validate that entry format is Part Number , Price (comma separated)
	var strTemp = objval;
	//This javascript code removes all 3 types of line breaks
    strTemp = strTemp.replace(/(\r\n|\n|\r)/gm, "|");

	// Get each line and validate
	var arrLines = strTemp.split('|');

	// List of valid characters
    var regularexp = /^[A-Za-z.0-9+/,\t\- ]+$/i;

	var error_cnt = 0;
    var comma_cnt = 0;
    var strTestLine = "";
    var negPrice = "";

	for (var i = 0; i < arrLines.length; i++)
	{
	    strTestLine = TRIM(arrLines[i]);
        comma_cnt = 0;

        // validate the price filed.
        var strPrice = strTestLine.split (',');
        if(strPrice != undefined){
        	var priceVal = TRIM(strPrice[1].replace(/[-.0-9]+/g,''));
        	// Price - number validation
    		if (priceVal.length >0)
    	    {
    			negPrice = negPrice + strPrice[0] +', ';
    	    }
        }
        // Check characters that are allowed
		if (regularexp.exec(strTestLine) == null) // Special character found
		{
			// Only include the message once
            if (error_cnt == 0) {
      			Error_Details(message_operations[198]);
			}
			error_cnt++
			// Include the error line
            Error_Details(strTestLine);

		}

		// Check required number of commas per line (must have 1 and only 1)
        comma_cnt = (strTestLine.split(",").length - 1);
		if (comma_cnt != 1)
		{
			// Only include the message once
            if (error_cnt == 0) {
      			Error_Details(message_operations[199]);
			}
			error_cnt++
			// Include the error line
			Error_Details(strTestLine);
		}
	}
	if(negPrice !=''){
		negPrice = negPrice.substring(0,(negPrice.length)-2);
		Error_Details(Error_Details_Trans(message_operations[200],negPrice));
	}
	// accountType is Global variable (its come from PhoneOrder js file).
	if(TypeName == 'Account' && accountType == '70114'){ // 70114 - ICS Account
		Error_Details(message_operations[201]);
	}
	
	if (ErrorCount > 0)
	{
			Error_Show();
			return false;
	}
	else
	{
		var array=[TypeName,acname]
		if (confirm(Error_Details_Trans(message_operations[202],array)))
		{
			fnStartProgress();
			document.frmAccount.submit();
		}
	}
}

function fnCallHistory(val)
{
	windowOpener("/GmPageControllerServlet?strPgToLoad=/gmPriceHistoryLog.do&txnId="+val+"&auditId="+strmodeCheck+"&strButtonAccessFl="+varBtnAccessFl,"","resizable=yes,scrollbars=yes,top=250,left=300,width=785,height=300,status=1");
}

function fnSetDefaultValues()
{
	if (document.frmAccount.Cbo_AccId.value != 0)
	{		
    	if (type == "Account") {
    		if(document.getElementById("Cbo_QueryBy") != undefined){
    		document.frmAccount.Cbo_QueryBy.value = document.frmAccount.hQueryBy.value;
    		}
			fnAcBlur(document.frmAccount.Cbo_AccId);
			fnParentAcInfo(document.frmAccount.Cbo_AccId.value,enablePriceLabel);
		}
		else{
			document.frmAccount.Txt_AccId.value = document.frmAccount.Cbo_AccId.value;
		}
	}
}

function fnReset()
{
	self.location.href = "/GmAccountPriceReportServlet?strOpt=Batch";
}
function fnCallAcIdBlur(obj){
	

	if (type == "Account") {
		var accDropDownVal = document.frmAccount.Cbo_AccId.value;
		
		var accVal = obj.value;
		if(accDropDownVal != accVal){
			fnAcIdBlur(obj);
			fnParentAcInfo(obj.value,enablePriceLabel);
			
		}
		
	}
	else {
		document.frmAccount.Cbo_AccId.value = obj.value;		
		if (document.frmAccount.Cbo_AccId.value == ''){
			document.frmAccount.Cbo_AccId.value = 0;
			Error_Details(message_operations[203]);
			Error_Show();
		}
	}
}

function fnCallAcBlur(obj){	
	
	if (type == "Account") {
		//alert(obj.value);

		fnAcBlur(obj);
		fnParentAcInfo(obj.value,enablePriceLabel);
	}
	else {
		if (obj.value == 0){
			document.frmAccount.Txt_AccId.value = '';
			}
		else		
			document.frmAccount.Txt_AccId.value = obj.value;
	}
}

function fnGroupDetail(groupID) { 
	var ajaxUrl = fnAjaxURLAppendCmpInfo("/gmGroupPartMap.do?strPartListFlag=Y&hviewEdit=view&groupId="+groupID+"&groupNM=","Part Details");
	  w3 =  createDhtmlxWindow(900,500);
	  w3.setText(message_operations[204]); 
	  w3.attachURL(ajaxUrl);
	  return false;	
}


function fnCallType(object) {
	var val = object.value;
	document.all.Cbo_ProjId.options.length = 0;
	document.all.Cbo_ProjId.options[0] = new Option("[Choose One]", "0");
	if (val == 'Project Name') {
		for ( var i = 0; i < projLen; i++) {
			arr = eval("projArr" + i);
			arrobj = arr.split(",");
			id = arrobj[0];
			name = arrobj[1];
			document.all.Cbo_ProjId.options[i + 1] = new Option(name, id);
		}

	} else {
		for ( var i = 0; i < systemLen; i++) {
			arr = eval("systemArr" + i);
			arrobj = arr.split(",");
			id = arrobj[0];
			name = arrobj[1];
			document.all.Cbo_ProjId.options[i + 1] = new Option(name, id);
		}
	}

}
var inputStringRevert = '';
var inputStringVoid = '';
//This function is used to check or uncheck which part number details need to revert or void.
function fnClickCheckBox(intcount,pricingID,unitPrice){
	var chkststus = eval("document.all.chk_rowwise" +intcount).checked;
 	if (chkststus){
 		inputStringRevert = inputStringRevert + pricingID + "|" + unitPrice + ",";
 		inputStringVoid = inputStringVoid + pricingID + ",";
 	}else{
 		removeRevertInputStr = pricingID + "|" + unitPrice + ","; 
 		inputStringRevert = inputStringRevert.replace(removeRevertInputStr, "");
 		
 		removeVoidInputStr = pricingID + ",";
 		inputStringVoid = inputStringRevert.replace(removeVoidInputStr, "");
 	}
 	//document.frmAccount.hInputString.value = inputStringRevert;
}

//This function used to update the current part price to the previous price
function fnSave() {
    var accountPricingId = '';
    var varUnitPrice = '';
    var inputString = '';
    var i = 0;
    inputString = inputStringRevert;

  /*  while (i < hmSize) {
    
        var chkststus = eval("document.all.chk_rowwise" + i).checked;
        if (chkststus) {
            if (eval("document.all.hAPPrice" + i)) {
                accountPricingId = eval("document.all.hAPPrice" + i).value;
            }
            if (eval("document.all.hUPrice" + i)) {
                varUnitPrice = eval("document.all.hUPrice" + i).value;
            }
            inputString = inputString + accountPricingId + "|" + varUnitPrice + ",";
        }
        i++;
    }*/

    if (inputString != '') {
        confirmMsg = confirm("Revert the selected Part prices to the previous price?");
        if (!confirmMsg) {
            return false
        }
        if (confirmMsg) {
            document.frmAccount.hInputStr.value = inputString;
            document.frmAccount.hMode.value = "Save";
            fnStartProgress('Y');
            document.frmAccount.submit();
        }
    } else {
        Error_Details("Please check at least one part to proceed.");
    }
    if (ErrorCount > 0) {
        Error_Show();
	    Error_Clear();
        return false;
    }

}
//This function used to Void the selected part price.
function fnVoid() {
    var accountPricingId = '';
    var varUnitPrice = '';
    var inputString = '';
    var i = 0;
    inputString = inputStringVoid;
    
  /*  while (i < hmSize) {
        var chkststus = eval("document.all.chk_rowwise" + i).checked;

        if (chkststus) {
            if (eval("document.all.hAPPrice" + i)) {
                accountPricingId = eval("document.all.hAPPrice" + i).value;
            }
            if (eval("document.all.hUPrice" + i)) {
                varUnitPrice = eval("document.all.hUPrice" + i).value;
            }
            inputString = inputString + accountPricingId + ",";
        }
        i++;
    }*/

    if (inputString != '') {
        confirmMsg = confirm("Void the selected Part Price?");
        if (!confirmMsg) {
            return false
        }
        if (confirmMsg) {
            fnStartProgress('Y');
            document.frmAccount.hInputStr.value = inputString;
            document.frmAccount.hMode.value = "Void";
            fnStartProgress();
            document.frmAccount.submit();
        }
    } else {
        Error_Details("Please check at least one part to proceed.");
    }
    if (ErrorCount > 0) {
        Error_Show();
	    Error_Clear();
        return false;
    }
}