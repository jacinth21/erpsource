//Function while selecting the action in Modify order screen
function fnCallEdit()
{
	var len = document.frmAccount.hLen.value;
	var cnt = 0;
	var sfl = "";
	var objThis = "";
	var objThisOrder = "";
	var orderType = "";
	var searchMe = '|';
	var invfl = '';
	var holdfl = '';
	var refId = ''; 
	var quoteId = '';
	var subRefId = '';
	var ret = new Boolean(false);
	var actionVal = document.frmAccount.Cbo_Action1.value;
	var arrOrdDet = '';
	var varParentOrdId = '';
	
	
	var varAccId = '';
	if ( len == 1)
	{
		var radvalstring = document.frmAccount.rad.value;
		if(radvalstring != ''){//to get the order id,inv id , parentorderid
			arrOrdDet = radvalstring.split('|');
			varOrdId = arrOrdDet[0];
			varInvId = arrOrdDet[1];
			varParentOrdId = arrOrdDet[2];
			varAccId = arrOrdDet[3];
		}

		            
	    if (varInvId == '')
		{
			varInvId = '|';
		}
        document.frmAccount.hOrdId.value = varOrdId;
        document.frmAccount.hParentOrdId.value = varParentOrdId;
		objThis = eval("document.frmAccount.GM"+document.frmAccount.rad.id);
		objThisOrder = eval("document.frmAccount.GM"+document.frmAccount.rad.id+'Type');
		sfl = objThis.value;
		orderType = objThisOrder.value;
		objThis = eval("document.frmAccount.GM"+document.frmAccount.rad.id+'HoldFl');
		holdfl = objThis.value;
		objThis = eval("document.frmAccount.GM"+document.frmAccount.rad.id+'RaStatusFl');
		RAStatusFl  = objThis.value;
		objThis = eval("document.frmAccount.GM"+document.frmAccount.rad.id+'REFID'); 
		refId  = objThis.value;
		objThis = eval("document.frmAccount.GM"+document.frmAccount.rad.id+'QuoteId'); 
		quoteId  = objThis.value;
	}
	else if ( len >1)
	{
		var arr = document.frmAccount.rad;

		for (var i=0;i<arr.length ;i++ )
		{
			if (arr[i].checked == true)
			{
				var radvalstring = arr[i].value;
				if(radvalstring != ''){
					arrOrdDet = radvalstring.split('|'); // Split the part number and attribute value to the array
					varOrdId = arrOrdDet[0];
					varInvId = arrOrdDet[1];
					varParentOrdId = arrOrdDet[2];
				}

				// if Invoice id not available then set the "|" symbol (PMT-23323)
				if (varInvId == '')
				{
					varInvId = '|';
				}
		            
				document.frmAccount.hOrdId.value = varOrdId;
				document.frmAccount.hParentOrdId.value = varParentOrdId;
								
				objThis = eval("document.frmAccount.GM"+arr[i].id);
				sfl = objThis.value;
				objThisOrder = eval("document.frmAccount.GM"+arr[i].id+'Type'); 
				orderType = objThisOrder.value;
				objThis = eval("document.frmAccount.GM"+arr[i].id+'InvFl');
				invfl = objThis.value;
				objThis = eval("document.frmAccount.GM"+arr[i].id+'HoldFl');
				holdfl = objThis.value;
				objThis = eval("document.frmAccount.GM"+arr[i].id+'RaStatusFl');
				RAStatusFl  = objThis.value;
				objThis = eval("document.frmAccount.GM"+arr[i].id+'REFID');
				refId  = objThis.value;
				objThis = eval("document.frmAccount.GM"+arr[i].id+'QuoteId');
				quoteId  = objThis.value;
				cnt++;
				break;
			}

		}
		if (cnt == 0)
		{
			Error_Details(message[10582]);
		}
	}
	if(refId != ''){
	  subRefId = refId.substring(0,4);
	}
	// Check if the order id marked for duplicate entry ; Duplicate Order
	if (orderType  == "2522")
	{
		//remove validation for  actions:  change PO details,   remove the Hold Status ,  Edit Order , Change Shipping Details
		
		//105961	Change PO Details
		//105968	Remove Hold Status
		//105960	Change Pricing
		//105964	Edit Order
		if (actionVal != "105961" && actionVal != "105968" && actionVal != "105960" && actionVal != "105964")
				{
					Error_Details(message[55]);//This selected Order is marked as a Duplicate order, please contact IT Department
				}
	}
	//105962 Change Lot # Details
	//105965	Initiate Return
	//105967	Move to Hold Status
	//105968	Remove Hold Status
	// Should Not Edit the Order,Initiate Return,Change LOT # Details , Move to Hold Status and Remove Hold Status for Ack Order Type
	if(orderType == "101260" && ( actionVal == '105962' || actionVal =='105965' || actionVal =='105967' || actionVal =='105968')){
		Error_Details(message[6001]);//Cannot perform the selected action, as this Order is Acknowledgement order
	}
	
	
	// Check if the selected order is an return order	
	if (orderType  == "2523")
	{
		Error_Details(message[56]);//Unable to Perform the selected action, as the selected Order is a return order.  Please contact IT Department
	}

	// Check if the selected order is an return order	
	if (orderType  == "2524")
	{
		Error_Details(message[58]);//Cannot perform the selected action, as this Order is Price Adjusted order. Please contact System Administrator
	}
	
	// Check if the selected order is an ICT	
	if (orderType  == "2533")
	{
		Error_Details(message[694]);//Cannot perform this action for the selected order since the type of the order is ICS. Please select a valid order
	}
	
	// Check if the selected order is OUS Distributor	
	if (orderType  == "102080" &&  subRefId != "ACK-" && quoteId == '')
	{
		Error_Details(message[697]);//Cannot perform this action for the selected order since the type of the order is OUS Distributor. Please select a valid order
	}
	//commented for issue#8349 Maintenance Request: Option to edit the PO# on discount orders generated for HPG orders
	//if (orderType  == "2535")
	//{
	//	Error_Details(message[695]);
	//}
	
	// Checking for basic condition 

	if (document.frmAccount.hDeptId.value =="2001" || document.frmAccount.hDeptId.value =="2020" || document.frmAccount.hDeptId.value =="2026")
	{
		ret = true;

		if (document.frmAccount.hDeptId.value =="2026")
		{

			//105960	Change Pricing
			//105968	Remove Hold Status
			//105967	Move to Hold Status
			//26240644      Add/Edit the Recorded Tags
			if (actionVal != "105967" && actionVal != "105968" && actionVal != "105960" && actionVal != "26240644")
			{
				ret = false;
			}
		}
	}
	
	
	if (!ret)
	{
		Error_Details(message[5102]);
	}

	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	
	var hmValues = new HashMap();
	hmValues.put('ACTIONVAL', actionVal);
	hmValues.put('SFL', sfl);
	hmValues.put('ORDTYPE', orderType);
	hmValues.put('RASTATUSFL',RAStatusFl);
	hmValues.put('VARINVID',varInvId);
	hmValues.put('VARORDID',varOrdId);
	hmValues.put('VARHOLDFL',holdfl);
	hmValues.put('VARACCID',varAccId);

	actionVal = actionVal == 0? '': actionVal;
	
	// Get the function name to be called while selecting the choose option value
	var fnAction = eval('fnDo'+actionVal+'Action');
	fnAction(hmValues);
	
	if(actionVal != 26240341 && actionVal!=26240644){// Edit Surgery Info
		if (ErrorCount > 0)
		{
				Error_Show();
				Error_Clear();
				return false;
		}
		document.frmAccount.hMode.value = "LoadOrder";
		//105963	Change Shipping Details
		if(actionVal != '105963'){
			fnStartProgress("Y");
			document.frmAccount.submit();
		}
	}
}

// Change Pricing
function fnDo105960Action(hmValues){
	var sfl = hmValues.get('SFL');
	if (sfl>3)
	{
		Error_Details(message[8]);//This order has already been Shipped/Invoiced, please contact System Administrator
	}
	else
	{
		document.frmAccount.hAction.value = "EditPrice";
		document.frmAccount.hChangePrice.value = "CHANGEPRICE"; //for PMT-38606
		document.frmAccount.action = "GmOrderItemServlet";
	}
}
//Change PO Details
function fnDo105961Action(hmValues){
	var sfl = hmValues.get('SFL');
	if (sfl>10)
	{
		Error_Details(message[8]);//This order has already been Shipped/Invoiced, please contact System Administrator
	}
	else
	{
		document.frmAccount.hAction.value = "EditPO";
		document.frmAccount.action = "GmOrderItemServlet";
	}
}

//Change Lot# Details
function fnDo105962Action(hmValues){
	var sfl = hmValues.get('SFL');
	var varOrdId = hmValues.get('VARORDID');
	var orderType = hmValues.get('ORDTYPE');
	var OrderTypeIndex = strRuleOrderType.indexOf(orderType);
	if (sfl>1 && OrderTypeIndex == -1 )
	{
		Error_Details(message[7]);//This order has already been Controlled/Shipped/Invoiced, please contact System Administrator
	}
	else
	{
		document.frmAccount.action = "/gmItemControl.do?haction=EditControl&txnid="+varOrdId+"&mode=CONTROL&txntype=50261&loctype=93343";
	}
}

//Change Shipping Details
function fnDo105963Action(hmValues){
	var sfl = hmValues.get('SFL');
	var varOrdId = hmValues.get('VARORDID');
	if (sfl>1)
	{
		Error_Details(message[7]);//This order has already been Controlled/Shipped/Invoiced, please contact System Administrator
	}
	else
	{	
		windowOpener("/gmOPEditShipDetails.do?refId="+varOrdId+"&source=50180&strOpt=modify",this.window,"");
		return;		
	}
}

//Edit Order
function fnDo105964Action(hmValues){
	var orderType = hmValues.get('ORDTYPE');
	var statusfl = hmValues.get('SFL');
	// if the status of the ACK order (101260) is 3, then should not allow to edit the screen
	if(orderType == '101260' && statusfl == '3' ){
		Error_Details(message[10583]);
	}else{
		document.frmAccount.hAction.value = "EditOrder";
		document.frmAccount.action = "GmOrderEditServlet";
	}
}
//Update DDT
function fnDo26230627Action(hmValues){
	
	var sfl = hmValues.get('SFL');
	var varOrdId = hmValues.get('VARORDID');
	var orderType = hmValues.get('ORDTYPE');
	var OrderTypeIndex = strRuleOrderType.indexOf(orderType);
	
	document.frmAccount.action ="/gmOrderDDTAction.do?method=loadDDTNumber&orderId="+varOrdId;
	
}

//Initiate Return
function fnDo105965Action(hmValues){
	document.frmAccount.hAction.value = "ReturnPart";
	document.frmAccount.action = "GmOrderReturnServlet";
}

//Void Order
function fnDo105966Action(hmValues){
	var orderType = hmValues.get('ORDTYPE');
	var invfl = hmValues.get('INVFL');
	var RAStatusFl = hmValues.get('RASTATUSFL');
	var varInvId = hmValues.get('VARINVID');
	var varOrdId = hmValues.get('VARORDID');
	
	if(orderType == "101260"){
		fnVoidAckOrder(varOrdId);
		return;
	}
	
	if (orderType == "2535")
	{
		Error_Details(message[612]);//GPO discount child order cannot be voided
	}

	if (invfl == '1')
	{
		Error_Details(message[54]);//Cannot void an order that is a selected Order Shipped from warehouse.
	}
    if (RAStatusFl == 'Y')
	{
		Error_Details(message[589]);//Cannot perform the selected action for this order
	}
	if ( varInvId != '|')
	{
		Error_Details(message[580]);//Cannot Void an Order which has been Invoiced, please contact Accounting team to void the Invoice
	}
	
	else
	{
		document.frmAccount.hCancelType.value = "VDRES";
		document.frmAccount.action = servletPath+"/GmCommonCancelServlet";
        document.frmAccount.hTxnId.value = varOrdId;
		document.frmAccount.hAction.value = "Load";
	}
}

//Move to Hold Status
function fnDo105967Action(hmValues){
	var varholdFl = hmValues.get('VARHOLDFL');
	var varOrdId = hmValues.get('VARORDID');
	if(varholdFl == 'Y'){ 
		Error_Details(message[10585]);
	}
		
		document.frmAccount.hCancelType.value = "OHOLDM";
		document.frmAccount.action = servletPath+"/GmCommonCancelServlet";
        document.frmAccount.hTxnId.value = varOrdId;
		document.frmAccount.hAction.value = "Load";	
}

//Remove Hold Status
function fnDo105968Action(hmValues){
	var varOrdId = hmValues.get('VARORDID');
	document.frmAccount.hCancelType.value = "RHOLD";
	document.frmAccount.action = servletPath+"/GmCommonCancelServlet";
    document.frmAccount.hTxnId.value = varOrdId;
	document.frmAccount.hAction.value = "Load";	
}

//Move Back to Back order
function fnDo105969Action(hmValues){
	var varOrdId = hmValues.get('VARORDID');
	var orderType = hmValues.get('ORDTYPE');
	if(orderType  == "2525"){ 
		document.frmAccount.hCancelType.value = "MBTBO";
		document.frmAccount.action = servletPath+"/GmCommonCancelServlet";
        document.frmAccount.hTxnId.value = varOrdId;
		document.frmAccount.hAction.value = "Load";	
    }else{
    	Error_Details(message[5000]);// Message:"Type should be backorder in order to move it to backorder"	
    }
}

function fnDoAction(hmValues){
	Error_Details(message[10586]);
}

// To void the Ack order
function fnVoidAckOrder(orderId){
	document.frmAccount.hCancelType.value = "VDACK";
	document.frmAccount.action = servletPath+"/GmCommonCancelServlet?hCancelReturnVal=true";
    document.frmAccount.hTxnId.value = orderId;
	document.frmAccount.hAction.value = "Load";
}

var HashMap = function(){
    var map={};
    this.put = function(key,val){
       
        if(map[key] != undefined){
            map[key] = map[key] +"^"+val;
        }else{
            map[key] = val;
        }
    };
    this.get = function(key){
        return map[key];
    };
    this.getInputStr= function(){
        var string = "";
        for(var attributename in map){
        string+=attributename+"^"+map[attributename]+"|";
        }
        return string;
    };
} 

// While selecting Edit Surgery Info option
function fnDo26240341Action(hmValues){
	var varOrdId = hmValues.get('VARORDID');
	
	document.getElementById("npidtls").style.display = 'inline';
	$("#hRefId").val(varOrdId);
	$("#hScreen").val('EditVal');
	$("#myModal").modal('show');
	
}

//While selecting Add/Edit the Recorded tags options
function fnDo26240644Action(hmValues)
{
	var actionVal = hmValues.get('ACTIONVAL');
	var refOrderID = hmValues.get('VARORDID');
	var accoutID =  hmValues.get('VARACCID');
	if(actionVal == 26240644){
		if (ErrorCount > 0)
		{
				Error_Show();
				Error_Clear();
				return false;
		}
		
		var txtOrdId = refOrderID;
		var txtAccountId = "";
		 if(txtOrdId!=null && txtAccountId!=null)
			 {
			 windowOpener('/gmOrderTagRecordAction.do?method=fetchTagRecordInfo&tagRefId='+txtOrdId+'&accountId='+txtAccountId,"AddTagRecordInfo","resizable=yes,scrollbars=yes,top=250,left=300,width=830,height=500");
		     } 
	  }
}