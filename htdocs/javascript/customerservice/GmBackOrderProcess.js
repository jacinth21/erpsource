var checkInValidTag=false;
function fnSubmit()
{
	if (rpt == '50261' || rpt == '50266')
	{
		if (document.frmAccount.hOrderId.value == ""){
			Error_Details(message_operations[249]);
			if (ErrorCount > 0)
			{
				Error_Show();
				 Error_Clear();
				return false;
			}
		}else if(rpt == '50266'){
			var cboAct = document.frmAccount.Act_Type.value;
			var OrderId = document.frmAccount.hOrderId.value;
			if(cboAct == '50268'){
			document.frmAccount.action="/GmCommonCancelServlet?hTxnId="+OrderId+"&hCancelType=VLNIT&hAction=Load";
			document.frmAccount.submit();
			}
			else
			{
			  document.frmAccount.submit();
			}
		}
		else{
			document.frmAccount.submit();
		}
	}
	else
	{
		alert(message_operations[250]);
	}
}
	  	
function Toggle(val)
{
	var obj = document.getElementById("div"+val);
	var trobj = document.getElementById("tr"+val);
	var tabobj = document.getElementById("tab"+val);

	if (obj.style.display == 'none')
	{
		obj.style.display = '';
		trobj.className= "ShadeRightTableCaptionBlue";
		tabobj.style.background="#ecf6fe";
	}
	else
	{
		obj.style.display = 'none';
		trobj.className= "";
		tabobj.style.background="#ffffff";
	}
}
//fnBackOrderFilter used to expand all the item back order reports
function fnBackOrderFilter(){
	var totalAccount = document.frmAccount.allExpand.value;
	var imgObj = eval("document.all.backOrderRptImg");
	// Change the image style
	if(expandNum%2==0){
		if(imgObj){
			imgObj.src = '/images/plus.gif';
		}
	}else{
		if(imgObj){
			imgObj.src = '/images/minus.gif';
		}
	}
	for(i=0;i<totalAccount;i++){
		var accountObj = document.getElementById("hPartNumID"+i);
		if(accountObj != null){
			var account = accountObj.value;
			// to expand or collapse the dynamic view
			var obj = document.getElementById("div"+account);
			var trobj = document.getElementById("tr"+account);
			var tabobj = document.getElementById("tab"+account);
			if(expandNum%2!=0){
				obj.style.display = '';
				trobj.className="ShadeRightTableCaptionBlue";
				tabobj.style.background="#ecf6fe";	
			}else{
				obj.style.display = 'none';
				trobj.className="";
				tabobj.style.background="#ffffff";
			}
		}		
	}
	expandNum++;
}

/*
function fnViewOrder(val)
{
	if (rpt == '50261')
	{
		windowOpener('/GmEditOrderServlet?hMode=PrintPack&hOrdId='+val,"Pack","resizable=yes,scrollbars=yes,top=150,left=150,width=760,height=610");
	}
	else
	{
		windowOpener("/gmRequestMaster.do?strOpt=print&requestId="+val,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=920,height=600");
	}	
}*/

function fnViewOrder(val,varType)
{
	intType = 0;
	
	if(varType!='') 
	{
		intType = parseInt(varType);
	}
		
	if (intType > 4116){
	windowOpener("/GmReprocessMaterialServlet?hAction=ViewInHouse&hId="+val,"Con1","resizable=yes,scrollbars=yes,top=250,left=200,width=800,height=700");
  }else{
	 // temp = val.substring(0,5);
temp = val.substring(val.lastIndexOf('-')-2,val.lastIndexOf('-'));
	  
	  if(temp=='RQ')
	  { 
	 // if(temp=='GM-RQ')
	  //{
	  windowOpener("/gmRequestMaster.do?strOpt=print&requestId="+val,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=920,height=600");
	  }
	  else
	  {
	  windowOpener('/GmEditOrderServlet?hMode=PrintPack&hOrdId='+val,"Pack","resizable=yes,scrollbars=yes,top=150,left=150,width=760,height=610");
	  }

	 
	}
}

function fnLookup(partNm)
{
	var strPartNm ='';
	var chkLength =0;
	fnStartProgress('Y');
	if(partNm != undefined && partNm !=''){
		strPartNm = partNm+",";
	}else{
		objChkAll = document.frmAccount.chkInvlookup;
		if(objChkAll != undefined)
		chkLength = objChkAll.length;
		
		if(chkLength != undefined && chkLength > 0){
			for(i =0 ;i< chkLength ; i++){
				if(objChkAll[i].checked == true )
					strPartNm = strPartNm + objChkAll[i].id +",";
			}
		}else if(chkLength == undefined && objChkAll && objChkAll.checked == true){
			strPartNm = strPartNm + objChkAll.id +",";
		}
	}
	if(strPartNm == ''){
		Error_Details(message_operations[251]);
	}
	if (ErrorCount > 0)
	{
		fnStopProgress();
		Error_Show();
		Error_Clear();
		return false;
	}
	
	windowOpener('/GmPartInvReportServlet?hAction=Go&hOpt=MultipleParts&hScreenType=BACKORDER&hPartNum='+strPartNm,"Pack","resizable=yes,scrollbars=yes,top=250,left=250,width=960,height=510");
	fnStopProgress();
}

function fnloadPartFulfillReport()
{
	fnStartProgress('Y');
	var val=document.frmAccount.Cbo_Type.value;
	var str = document.all.hPartNums.value;
	str = str.substr(0,str.length-1);
	windowOpener("/gmLogisticsReport.do?method=loadPartFulfillReport&BackOrderType="+val+"&hPartNum="+str,"Pack","resizable=yes,scrollbars=yes,top=250,left=250,width=1020,height=600");
    fnStopProgress();
}

function fnReload()
{
	if(checkInValidTag){
		document.frmAccount.Txt_TagID.value ='';
	}
	fnStartProgress ('Y'); 
	fnReloadSales(null,'/GmOrderItemServlet?hOpt=ProcBO&hAction=Load');
}

function fnLoad()
{
	document.frmAccount.Cbo_Type.value = rpt;
	document.frmAccount.Cbo_Aging.value =agType;
	if (typeVal!= '' || source!= '' )
	{
		document.frmAccount.Cbo_Source_Type.value = source;
		document.frmAccount.Cbo_Type_List.value = typeVal;
	}
	if(rpt=='50263'){
		       document.getElementById("divHideButton").style.display = 'block';
			}else{
		       document.getElementById("divHideButton").style.display = 'none';
			}
	if(rpt== '50266' || rpt== '50262'){
	       document.getElementById("showTagId").style.display = 'block';
		}
	else{
		document.getElementById("showTagId").style.display = 'none';
	}
	}
//OnChange function used for enabling source & type fields when select "Items" in Report Type drop down
function fnChange(val)
{
	if(val== '50263'){
	       document.getElementById("divHideButton").style.display = 'block';
		}else{
	       document.getElementById("divHideButton").style.display = 'none';
		}
	if(val== '50266' || val== '50262'){
		   document.frmAccount.Txt_TagID.value ='';
		   document.getElementById('DivShowTagAvl').style.display='none';
		   document.getElementById('DivShowTagNotExists').style.display='none';
	       document.getElementById("showTagId").style.display = 'block';
		}
	else{
		document.getElementById("showTagId").style.display = 'none';
	}
}

function fnSelectRad(obj)
{
	document.frmAccount.hOrderId.value=obj.value;
}

function fnOpenOrdLog(id)
{
	windowOpener("/GmCommonLogServlet?hType=1233&hID="+id,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
}


function fnOpenOrdLogInv(id)
{
	windowOpener("/GmCommonLogServlet?hType=1234&hID="+id,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
}

function fnViewDetails(pnum,act)
{
	windowOpener("/GmPartInvReportServlet?hAction=DrillDown&hOpt="+act+"&hPartNum="+pnum,"INVPOP","resizable=yes,scrollbars=yes,top=300,left=300,width=710,height=200");
}
function fnSelectAll(obj)
{
	var chkLength =0;
	objChkAll = document.frmAccount.chkInvlookup;
	if(objChkAll != undefined)
		chkLength = objChkAll.length;
	if(chkLength == undefined && obj.checked == true && objChkAll){
		objChkAll.checked = true;
	}else if(chkLength == undefined && obj.checked == false && objChkAll){
		objChkAll.checked = false;
	}else if(obj.checked == true){
		for(i =0 ;i< chkLength ; i++){
			objChkAll[i].checked = true;
		}
	}else {
		for(i =0 ;i< chkLength ; i++){
			objChkAll[i].checked = false;
		}
	}		
}

// When click on Update to Bill Only button
function fnUpdateToBillOnly(){
	var orderId = document.frmAccount.hOrderId.value;
	if(orderId == ""){
		Error_Details(message_operations[252]);
		Error_Show();
		Error_Clear();
		return false;
	}else{
		fnStartProgress();
		document.frmAccount.action = "/GmOrderItemServlet?hAction=UpdateToBillOnly&hOrderId="+orderId;
		document.frmAccount.submit();
	}

}
//this function is used to validate the tag id
function fnValidateTag(tagId){
	tagId=tagId.replace(/(^\s+|\s+$)/g, '');
	if(tagId!=''){
	var ajaxUrl = fnAjaxURLAppendCmpInfo("/gmValdiateTagId.do?tagId="+tagId);
	dhtmlxAjax.get(ajaxUrl,function(loader)
			{
				var response = loader.xmlDoc.responseText;
				if(response == '1'){
					document.getElementById('DivShowTagAvl').style.display='inline-block';
					document.getElementById('DivShowTagNotExists').style.display='none';

				}else{
					document.getElementById('DivShowTagNotExists').style.display='inline-block';
					document.getElementById('DivShowTagAvl').style.display='none';
					checkInValidTag=true;

				}
			});
	}
}

//this function is used  to open tag report in new window

function fnTagLookup (){
	
	windowOpener("/GmPageControllerServlet?strPgToLoad=gmTagReport.do&strOpt=Load","TAGRPT","resizable=yes,scrollbars=yes,top=300,left=300,width=1100,height=400");
	
}

//this function is used  to hide validation on click

function fnToggleTag (){
	document.getElementById('DivShowTagAvl').style.display='none';
	document.getElementById('DivShowTagNotExists').style.display='none';
}
//Used to reload the CN on Process Transaction
function fnReloadCN(refId)
{
	targetUrlStr = document.frmAccount.action+"/gmModifyControlNumber.do?REFID="+refId;
	parent.location.href = "/gmModifyControlNumber.do?REFID="+refId+"&"+fnAppendCompanyInfo();

}
