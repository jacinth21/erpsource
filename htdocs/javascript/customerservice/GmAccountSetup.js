var resetClicked = false;
var dhxWins,popupWindow,w1;

var validateFl = false;

var resShipAdd1 = "";
var resShipAdd2 = "";
var resShipCity = "";
var resShipState = "";
var resShipCountry = "";
var resShipzipCode = "";
var addRes = '';

function fnValidateAdd (){	
	var shipAdd1 = document.frmAccount.Txt_ShipAdd1.value;
	var shipAdd2 = document.frmAccount.Txt_ShipAdd2.value;
	var shipCity = document.frmAccount.Txt_ShipCity.value;
	var shipState = document.frmAccount.Cbo_ShipState.value;
	var shipCountry = document.frmAccount.Cbo_ShipCountry.value;
	var shipzipCode = document.frmAccount.Txt_ShipZipCode.value;

	fnValidateTxtFld('Txt_ShipName',message_operations[205]);
	fnValidateTxtFld('Txt_ShipAdd1',message_operations[206]);
	fnValidateTxtFld('Txt_ShipCity',message_operations[207]);
	fnValidateDropDn('Cbo_ShipState',message_operations[208]);
	fnValidateDropDn('Cbo_ShipCountry',message_operations[209]);
	fnValidateTxtFld('Txt_ShipZipCode',message_operations[210]);
	
	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}
	
	fnStartProgress();
	dhtmlxAjax.get('/gmAddressSetup.do?method=validateAccountShipAdd&hAction=AvaTax_Add_Validate&Address1=' + TRIM(shipAdd1)+'&City='+ shipCity+'&State='+ shipState+'&Country='+ shipCountry+'&ZipCode='+ shipzipCode+'&ramdomId=' + Math.random(), fnShowValidAdd);
}

function fnShowValidAdd (loader){	
	validateFl = true;
	var response = loader.xmlDoc.responseText;
	fnStopProgress();
	
	if(response != null){
		var resString = response; 
		
		var resString_array = resString.split('^^');
		
		for(var i = 0; i < resString_array.length; i++) {
		   // Trim the excess whitespace.
		   resString_array[i] = resString_array[i].replace(/^\s*/, "").replace(/\s*$/, "");
		}
		addRes = resString_array[0];

		var validAddObj= document.getElementById("showAddValidate");
		var inValidAddObj= document.getElementById("showAddInValidate");
		var validAddTxtObj= document.getElementById("showAddValidateTxt");
		var inValidAddTxtObj= document.getElementById("showAddInValidateTxt");
		var validImg= document.getElementById("validImg");
		
		validAddObj.style.display='none';
		validAddTxtObj.style.display='none';
		inValidAddObj.style.display='none';
		inValidAddTxtObj.style.display='none';
			
		if(addRes =='Y'){
				
			resShipAdd1 = resString_array[1];
			resShipCity = resString_array[3];
			resShipState = resString_array[4];
			resShipCountry = resString_array[5];
			resShipzipCode = resString_array[6];
			
			document.frmAccount.Txt_ShipAdd1.value = resShipAdd1;
			document.frmAccount.Txt_ShipCity.value = resShipCity;
			document.frmAccount.Cbo_ShipState.value = resShipState;
			document.frmAccount.Cbo_ShipCountry.value = resShipCountry;
			document.frmAccount.Txt_ShipZipCode.value = resShipzipCode;
					
			validAddObj.style.display='block';
			validAddTxtObj.style.display='block';
			document.frmAccount.hShippingAddVerified.value='Y';
			alert(message_operations[211]);
		}else{
			Error_Details(addRes);
			inValidAddObj.style.display='block';
			inValidAddTxtObj.style.display='block';
			document.frmAccount.hShippingAddVerified.value='';
		}
	}
	if (ErrorCount > 0){
			Error_Show();
			Error_Clear();
			return false;
	}
	
}


function fnSubmit()
{
     var strAlphaNum = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
     var action = document.frmAccount.hAction.value;
     var strCreditRating = document.frmAccount.Txt_CreditRating.value;
     var accGroup = '';
     
     var txtShipAdd1 = document.frmAccount.Txt_ShipAdd1.value;
 	 var txtShipAdd2 = document.frmAccount.Txt_ShipAdd2.value;
 	 var txtShipCity = document.frmAccount.Txt_ShipCity.value;
 	 var txtShipState = document.frmAccount.Cbo_ShipState.value;
 	 var txtShipCountry = document.frmAccount.Cbo_ShipCountry.value;
 	 var txtShipzipCode = document.frmAccount.Txt_ShipZipCode.value;
 	 var shipAddVerifyFl= document.frmAccount.hShippingAddVerified.value;
 	 var internalRep= "";
 	 var shipAddConfirmFl1 = true;
 	 var shipAddConfirmFl = false;
	 var paymentTerms = '';
	 
     if(strCreditRating.length > 20){
          Error_Details(message_operations[212]);
     }
     //CreditRating is not mandatory field
     //fnValidateTxtFld('Txt_CreditRating',' CreditRating ');
     //AlphaNumWithDashValidation(strCreditRating, "Please enter alphanumeric values for <b>CreditRating</b>", strAlphaNum,'+',10);
     
     fnValidateTxtFld('Txt_Rep_AccNm',message_operations[213]);
     //If the Rep Account Name(En) is empty will store the Rep Account Name as Rep Account Name(En) value
     var accNameEn = document.frmAccount.Txt_Rep_AccNm.value;
     
     if(document.frmAccount.Txt_Rep_AccNmEn!=undefined){
         fnValidateTxtFld('Txt_Rep_AccNmEn',message_operations[723]);
         accNameEn = document.frmAccount.Txt_Rep_AccNmEn.value;
     }
     
     fnValidateDropDn('Cbo_CompId',lblCompany);
     fnValidateDropDn('Cbo_DistId',lblDistributorName);
     fnValidateDropDn('Cbo_RepId',message_operations[214]);
     fnValidateTxtFld('Txt_IncDate',message_operations[215]);
     fnValidateDropDn('Cbo_Currency',message_operations[216]);
     //Common Date validation for US and OUS
     CommonDateValidation(document.frmAccount.Txt_IncDate,format,Error_Details_Trans(message_operations[217],format));
     fnValidateTxtFld('Txt_Parent_AccNm',message_operations[218]);
     fnValidateDropDn('Cbo_AccType',message_operations[219]);
     fnValidateTxtFld('Txt_ContPer',message_operations[220]);
     
     fnValidateTxtFld('Txt_BillName',message_operations[221]);
     fnValidateTxtFld('Txt_ShipName',message_operations[205]);
     
     fnValidateTxtFld('Txt_BillAdd1',message_operations[222]);
     fnValidateTxtFld('Txt_ShipAdd1',message_operations[206]);
     
     fnValidateTxtFld('Txt_BillCity',message_operations[223]);
     fnValidateTxtFld('Txt_ShipCity',message_operations[207]);
     
     fnValidateDropDn('Cbo_BillState',message_operations[224]);
     fnValidateDropDn('Cbo_ShipState',message_operations[208]);
     
     fnValidateDropDn('Cbo_BillCountry',message_operations[225]);
     fnValidateDropDn('Cbo_ShipCountry',message_operations[209]);

     fnValidateTxtFld('Txt_BillZipCode',message_operations[226]);
     fnValidateTxtFld('Txt_ShipZipCode',message_operations[210]);
     fnValidateDropDn('Cbo_Payment',message_operations[227]);     

     fnValidateTxtFld('Txt_LogReason',message_operations[547]);
 	if(document.frmAccount.Cbo_AccType.value == "26230710"){
	 
	  fnValidateDropDn('Cbo_Dealer_Id', message_operations[726]);
    	
     }
     var accName =  document.frmAccount.Txt_Parent_AccNm.value;
     if (accName.indexOf(",")>-1)
     {
          Error_Details(message[45]);
     }
     
     // validation for Pricing team not to change collector Id values
     if(pricingUser == 'Y'){
          var selCollectorId = '';
          if(document.frmAccount.Cbo_CollectorId){
              selCollectorId =  document.frmAccount.Cbo_CollectorId.value;
          }
          curCollectorId = (curCollectorId=='') ? '0':curCollectorId;
          
          if(action == 'Add'){
              if(defaultCollectorID != selCollectorId){
                   Error_Details(message_operations[228]);
                   document.frmAccount.Cbo_CollectorId.value = defaultCollectorID;
              }
          }else if(action == 'Edit'){
              if(curCollectorId != selCollectorId){
                   Error_Details(message_operations[228]);
                   document.frmAccount.Cbo_CollectorId.value = curCollectorId;
              }
          }
     }
     // validation for Pricing team not to change source values
     if(pricingUser == 'Y' && acctSrc == 'Y'){
          var selSourceId =  document.frmAccount.Cbo_Source.value;      
          curSourceId = (curSourceId=='') ? '0':curSourceId;
          
          if(action == 'Add' || action == 'Edit'){
              if(curSourceId != selSourceId){
                   Error_Details(message_operations[229]);
                   document.frmAccount.Cbo_Source.value = curSourceId;
              }
          }
     }
		if(document.frmAccount.Cbo_Payment!=undefined){
			paymentTerms = fnCorrectDropDownVal(document.frmAccount.Cbo_Payment.value);
		}
		if(document.frmAccount.Cbo_InternalRep != undefined ){
		fnValidateDropDn('Cbo_InternalRep',message_operations[865]);
		  internalRep= document.frmAccount.Cbo_InternalRep.value;
		}
     if (ErrorCount > 0)
     {
              Error_Show();
              Error_Clear();
              return false;
     }
     
     if (document.frmAccount.Chk_ActiveFl.checked)
     {
          document.frmAccount.Chk_ActiveFl.value = 'Y';
     }else{
    	 document.frmAccount.Chk_ActiveFl.value = 'N';
     }
     
     var hAcc = parent.document.frmAccount.hAccountId.value; //AccountId From Container JSP
     var cboacc = parent.document.frmAccount.Cbo_AccId.value;
     
     //Account Affiliation values
     //var strGrpAff = document.frmAccount.Cbo_GrpAff.value;
     //var strHlcAff = document.frmAccount.Cbo_HlcAff.value;
     //var contractFl = document.frmAccount.contractFl.value;
     var strCollectorId ="";
     if(document.frmAccount.Cbo_CollectorId){
          strCollectorId = fnCorrectDropDownVal(document.frmAccount.Cbo_CollectorId.value);
     }
     var AccAttInputStr = '';     
     if (action == 'Edit' && hAcc != cboacc) {
          alert(message_operations[230]);
          return;
     }
     
   //Sales Tax address validation.To notify the user when the address is not verified. 1101-United States
 	if(avaTaxFl =='Y' && (txtShipCountry == '1101' || prevShipCountry == '')){ 	
 		if(shipAddVerifyFl !='Y'){
 				validateFl = true;
 				shipAddConfirmFl = true;
 		}
 		if((prevShipAdd1 != txtShipAdd1 || prevShipCity != txtShipCity || prevShipState != txtShipState || prevShipCountry != txtShipCountry || prevShipZip != txtShipzipCode) && validateFl == false){
 			shipAddConfirmFl = true;
 		} 		
 		if((resShipAdd1 != txtShipAdd1 || resShipCity != txtShipCity || resShipState != txtShipState || resShipCountry != txtShipCountry || resShipzipCode != txtShipzipCode) && addRes == 'Y'){
 			shipAddConfirmFl = true;
 		} 		 
 		if(shipAddConfirmFl){
 			shipAddConfirmFl1 = confirm(message_operations[231]);
 				if(shipAddConfirmFl1){
 					shipAddVerifyFl = '';
 				}	
 		}

 	}
 	
 	if(shipAddConfirmFl1){
 		 //  103050:Collector Attribute   7008: Account Group Type
 		// 101191: Shipping Address Verified?
 		var category = '';
 		var accGroup = '';
 		var accGroupStr = '';
 		 if(document.frmAccount.Cbo_Category != undefined){
 			 var category = document.frmAccount.Cbo_Category.value;
 		 }else{
 			category = '0'; 
 		 }
 		
 		 if (document.frmAccount.Cbo_AccGrp != undefined){
          accGroup =  document.frmAccount.Cbo_AccGrp.value;
          if(accGroup!='0'){
        	  accGroupStr = '7008'+'^'+accGroup+'|';
          }
 		 }
 		// Tax type for India 
 		if (document.frmAccount.Cbo_TaxType != undefined){
 			var accTaxType = document.frmAccount.Cbo_TaxType.value;
 			AccAttInputStr = AccAttInputStr +'101170'+'^'+accTaxType+'|';	
 		}
 		 AccAttInputStr = AccAttInputStr+'103050'+'^'+strCollectorId+'|'+'101191'+'^'+shipAddVerifyFl+'|'+'5504'+'^'+ fnCorrectDropDownVal(category) +'|'+accGroupStr;
 	     // If Source is showing in UI while submit we have to append this values to inputstring
 	     if(acctSrc == 'Y'){
 	          //104800:Account Source
 	          AccAttInputStr = AccAttInputStr + '104800'+'^'+ document.frmAccount.Cbo_Source.value +'|';
 	          
 	     }
 	     parent.document.frmAccount.hAccountId.value = parent.document.frmAccount.Cbo_AccId.value; //changed from child to parent JSP
 	     document.frmAccount.hAccAttInputStr.value = AccAttInputStr;
 	     document.frmAccount.Btn_Submit.disabled=true; // To disable the Submit button
 	     if(cboacc == '0' || cboacc == null){ // To reload the saved details to main container
 	          document.frmAccount.hReloadCount.value = '0';
 	     } 
 		// repacctid^repacctnm^compid^ repid^acctshnm^actfl^inceptdate
 	     var tmp_accId = document.frmAccount.hAccountId.value;
 	     if(tmp_accId == ''){
 	    	tmp_accId = fnCorrectDropDownVal(parent.document.frmAccount.Cbo_AccId.value);
 	     }
 		document.frmAccount.hRepAccInputStr.value = tmp_accId
 				+ '^'
 				+ document.frmAccount.Txt_Rep_AccNm.value
 				+ '^'
 				+ accNameEn
 				+ '^'
 				+ fnCorrectDropDownVal(document.frmAccount.Cbo_CompId.value)
 				+ '^'
 				+ fnCorrectDropDownVal(document.frmAccount.Cbo_RepId.value)
 				+ '^'
 				+ document.frmAccount.Txt_ShName.value
 				+ '^'
 				+ document.frmAccount.Chk_ActiveFl.value
 				+ '^'
 				+ document.frmAccount.Txt_IncDate.value
 				+ '^'
 				+ fnCorrectDropDownVal(document.frmAccount.Cbo_Currency.value)
 				+ '^'
 				+ fnCorrectDropDownVal(document.frmAccount.Cbo_Dealer_Id.value)
 				+ '|';
 		// repacctid,parentacctid,acctnm,accttp,contactpernm,phone,fax,acctinfo
 		// billnm,billadd1,billadd2,billcity,billstate,billcountry,billzip
 		// shipnm,shipadd1,shipadd2,shipcity, shipstate,shipcountry,shipzip
 		// paytermsid,creditratenm
 		document.frmAccount.hParentAccInputStr.value = fnCorrectDropDownVal(document.frmAccount.Cbo_Parent_Account_Id.value)
 				+ '^'
 				+ document.frmAccount.Txt_Parent_AccNm.value
 				+ '^'
 				+ fnCorrectDropDownVal(document.frmAccount.Cbo_AccType.value)
 				+ '^'
 				+ document.frmAccount.Txt_ContPer.value
 				+ '^'
 				+ document.frmAccount.Txt_Phn.value
 				+ '^'
 				+ document.frmAccount.Txt_Fax.value
 				+ '^'
 				+ document.frmAccount.Txt_Comments.value
 				+ '^'
 				+ document.frmAccount.Txt_BillName.value
 				+ '^'
 				+ document.frmAccount.Txt_BillAdd1.value
 				+ '^'
 				+ document.frmAccount.Txt_BillAdd2.value
 				+ '^'
 				+ document.frmAccount.Txt_BillCity.value
 				+ '^'
 				+ fnCorrectDropDownVal(document.frmAccount.Cbo_BillState.value)
 				+ '^'
 				+ fnCorrectDropDownVal(document.frmAccount.Cbo_BillCountry.value)
 				+ '^'
 				+ document.frmAccount.Txt_BillZipCode.value
 				+ '^'
 				+ document.frmAccount.Txt_ShipName.value
 				+ '^'
 				+ document.frmAccount.Txt_ShipAdd1.value
 				+ '^'
 				+ document.frmAccount.Txt_ShipAdd2.value
 				+ '^'
 				+ document.frmAccount.Txt_ShipCity.value
 				+ '^'
 				+ fnCorrectDropDownVal(document.frmAccount.Cbo_ShipState.value)
 				+ '^'
 				+ fnCorrectDropDownVal(document.frmAccount.Cbo_ShipCountry.value)
 				+ '^'
 				+ document.frmAccount.Txt_ShipZipCode.value
 				+ '^'
 				+ paymentTerms
 				+ '^'
 				+ document.frmAccount.Txt_CreditRating.value 
 				+ '^'
				+ internalRep + '|';     
 	        	
			var old_dealer_id = document.frmAccount.hDealerIDStr.value;
			var oldAccType = document.frmAccount.holdAccType.value;
	    	 if(old_dealer_id != "" && old_dealer_id !=document.frmAccount.Cbo_Dealer_Id.value&&document.frmAccount.Cbo_AccType.value == "26230710"){
	    		 if (confirm(message_operations[727])){ 
	    			 fnStartProgress('Y');
	    			 document.frmAccount.submit(); 		
	    		 }
	    		 else
	    		{
	    			 document.frmAccount.Btn_Submit.disabled=false;
	    		}
	    	 }else if(oldAccType!=""&&oldAccType!=document.frmAccount.Cbo_AccType.value){
	    		 if (confirm(message_operations[732])){ 
		    			 fnStartProgress('Y');
		    			 document.frmAccount.submit(); 		
		    		 }
	    	 }
	    	 else
	    	{
	    		 fnStartProgress('Y');
	    		 document.frmAccount.submit(); 		
	    	}
			

 	}    
}
function fnReset()
{
    fnStartProgress('Y');
     document.frmAccount.hAction.value = 'Load';
     parent.document.frmAccount.Cbo_AccId.value = '0';
     parent.document.frmAccount.Txt_AccId.value = '';
     document.frmAccount.submit();
}

function fnRemoveCombo() {
	var select = parent.document.frmAccount.Cbo_AccId;
    var value = select.selectedIndex;
    select.removeChild(select[value]);
    return value;
}

function addCombo(val) {
	
    var accId = parent.document.frmAccount.hAccountId.value;
    var accName = document.frmAccount.Txt_Rep_AccNm.value;
    if(strCompLangId == '103097' && document.frmAccount.Txt_Rep_AccNmEn!=undefined) {
    	accName = document.frmAccount.Txt_Rep_AccNmEn.value;
    }
    var combo = parent.document.frmAccount.Cbo_AccId;
    
    var option = document.createElement("option");
    option.text = accName;
    option.value = accId;
    combo.add(option, val); //Standard 
    parent.document.frmAccount.Cbo_AccId.value = accId;
    parent.document.frmAccount.Txt_AccId.value = accId;
}


function fnLoadRep(val,action)
{	
     var dist = document.frmAccount.Cbo_DistId.value;
     var obj = document.frmAccount.Cbo_RepId;
     var j = 0;
     obj.options.length = 0;
     for (i=0;i<len ;i++ )
     {
          arr = eval("arr"+i);
          arrobj = arr.split(",");
          distid = arrobj[0];
          if (distid == dist)
          {
              j++;
              repid = arrobj[1];
              repname = arrobj[2];
              obj.options[j] = new Option(repname,repid);
              if (action == 'Reload' && document.frmAccount.hRepId.value == repid)
              {
            	  // IE compatability Fix - Adding the options before setting the selection.
            	  obj.options[0] = new Option("[Dummy Rep]","-1");
                  obj.options[j].selected = true;
              }
          }
     }
     if (j>0 && action == 'Load')
     {
          obj.options[0] = new Option("[Choose One]","0");
          obj.options[0].selected = true;
     }
     else if(j == 0)
     {
          obj.options[0] = new Option("[Dummy Rep]","-1");
     }
}


function fnCallOnLoad()
{  

	//to disable all the fields based on the access flag
	fnDisableFields();

	var repAccNm=document.frmAccount.Txt_Rep_AccNm.value;
	
	var accId ='';
	if(parent.document.frmAccount.Cbo_AccId != undefined){
		// to check the account name english object available.
	    if(strCompLangId == '103097' && document.frmAccount.Txt_Rep_AccNmEn!=undefined) {
	    	repAccNm = document.frmAccount.Txt_Rep_AccNmEn.value;
	    }
		parent.document.all.searchCbo_AccId.value =repAccNm;		
		accId = parent.document.frmAccount.Cbo_AccId.value; //ComboAccount Id From parent JSP
	}
	else
	{
		 accId = parent.document.frmAccount.hAccountId.value;
	}
     action = document.frmAccount.hAction.value ;

 	 var lblAccID = document.frmAccount.hAccountId.value;
 	if(parent.document.frmAccount.Txt_AccId != undefined){
 		parent.document.frmAccount.Txt_AccId.value = lblAccID;
 	}
 	 
 	 
     if (action == 'Edit')
     {
          fnLoadRep(document.frmAccount.Cbo_DistId,'Reload');
     }    
     showButton =document.frmAccount.hShowButton.value;
     
     //Below codes are commented to fix the alignment issue and included these conditions in jsp
     if(mapShipAccess == 'Y' && strCSAccess != 'disabled=disabled'){ // Map access then only we have the button
          if (showButton == "true"){
               //document.getElementById("MapShpParams").style.visibility='';
              //if(strCSAccess != 'disabled=disabled')
              document.getElementById("Collector").style.display = 'block';
          }
          else{
               //document.getElementById("MapShpParams").style.visibility='hidden';
              //if(strCSAccess != 'disabled=disabled')
              document.getElementById("Collector").style.display = 'none';
          }
     }
     // for newly created Accounts Setting Default CollectorId
     if(action == 'Add'){
          if(document.frmAccount.Cbo_CollectorId){
              document.frmAccount.Cbo_CollectorId.value = defaultCollectorID;
          }
     }   
     
     if(strHideButton== 'True'){
            document.getElementById("divHideButton").style.display = 'none';
     }else{
            document.getElementById("divHideButton").style.display = 'block';
     }
     
     if (strShipSetupUpdFl == "Y" && lblAccID == ''){// Customer Service user only can see button
          //document.getElementById("Btn_ShipSetup").style.visibility='';
          //if (accId == '0'){
              if(document.getElementById("Btn_ShipSetup"))
                   document.getElementById("Btn_ShipSetup").disabled = true;
          //}
     }/*else{
          document.getElementById("Btn_ShipSetup").style.visibility='hidden';
     }*/   
     if(avaTaxFl == 'Y'){
    	 fnShowHideValidShipAdd ();
     }
     fnChangeAcctType();
     fnSetTableOddEvenShade("tblAcctInfo");
     fnSetTableOddEvenShade("tblParAcctInfo");
     fnSetTableOddEvenShade("tblAcctInfor");   
 }

function fnCheckAcct(obj)
{

if(obj!='check'){
var accId=document.frmAccount.Txt_AccId.value;
document.frmAccount.hAccountId.value =accId
var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmAccountInfo.do?method=getAccountInfo&AccId='+accId+'&ramdomId='+Math.random());
dhtmlxAjax.get(ajaxUrl, fnValidateAccount);
}
else
{
	var accId=document.getElementById("Cbo_AccId").value;
	document.frmAccount.hAccountId.value =accId
	document.frmAccount.Txt_AccId.value = accId;
	fnOnloadpage();
}
}



//To  validatate Account Id
function fnValidateAccount(loader) { 
	        var xmlDoc = loader.xmlDoc.responseText;
	        if(xmlDoc.indexOf("N")!= -1){
	        	
	        	Error_Details(message_operations[736]);
	    		if (ErrorCount > 0)
	    		 {
	    			Error_Show();
	    			Error_Clear();
	    			return false;
	    		}	
	        }else if(xmlDoc.indexOf("Y")!= -1){  
	        	 var accid = document.frmAccount.hAccountId.value;
	        	 document.frmAccount.Cbo_AccId.value = accid;
	        	 document.frmAccount.Txt_AccId.value = accid;
	             fnOnloadpage();        	
	        	
	        }
}

function fnLoadAcct(obj)
{
     fnStartProgress('Y');
     document.frmAccount.hAccountId.value = obj.value;
     var new_action = 'Reload';

     if (document.frmAccount.hAccountId.value == '0')
     {
          new_action = 'Load';
     }
     if (ErrorCount > 0)
     {
          Error_Show();
          Error_Clear();
          return false;
     }

     document.frmAccount.action = "/GmAccountServlet?hOpt=FETCHCONTAINER&hAction="+new_action; //for [Choose One] action,Reload  chaged to new_action 
     document.frmAccount.submit();
}

function fnLoadSubmit()
{
     fnValidateTxtFld('hAccountId',message_operations[233]);
     if (ErrorCount > 0)
     {
              Error_Show();
              Error_Clear();
              return false;
     }
     
     document.frmAccount.hMode.value = "LoadPrices";
     document.frmAccount.hOpt.value = "Project";
     document.frmAccount.action = '/GmAccountPriceReportServlet';  
     document.frmAccount.submit();
}

function fnShipParams(){
     fnValidateTxtFld('hAccountId',message_operations[233]);
     if (ErrorCount > 0)
     {
              Error_Show();
              Error_Clear();
              return false;
     }    
     var strRuleGrpId = document.frmAccount.hAccountId.value;
//   windowOpener("/gmRuleParamSetup.do?codeGrpId=SHPPM&ruleGrpId="+strRuleGrpId+"&header=Map Shipping Parameters","Rule","resizable=yes,scrollbars=yes,top=150,left=150,width=770,height=500");
     windowOpener("/gmPartyShipParamsSetup.do?method=setPartyShipParam&screenType=button&accountId="+strRuleGrpId+"&partyID="+partyId,"Rule","resizable=yes,scrollbars=yes,top=150,left=150,width=980,height=470");

}

// The below function open the window for enter map account params value for OUS. 
function fnAccParams(){
     fnValidateTxtFld('hAccountId',message_operations[233]);
     if (ErrorCount > 0)
     {
              Error_Show();
              Error_Clear();
              return false;
     }    
     var strRuleGrpId = document.frmAccount.hAccountId.value;
     windowOpener("/GmAccountServlet?hAccountId="+strRuleGrpId+"&hAction=LoadMapAccParams"+"&hideSubmitButton="+strHideButton,"Rule","resizable=yes,scrollbars=yes,top=150,left=150,width=750,height=500");
}

function fnLoadHistory(colId,patId)
{
     windowOpener("/gmAuditTrail.do?auditId="+ colId +"&txnId="+ patId,"","resizable=yes,scrollbars=yes,top=250,left=300,width=785,height=300,status=1");
}

// To click the reset button - then assign the true flag. Its used to avoid the error message.
function fnResetClicked(){
     resetClicked = true;
}

function fnShipSetup(){
     var accid = document.frmAccount.hAccountId.value;
     windowOpener("/gmAccountShipSetup.do?strOpt=reload&accID="+accid,"","resizable=yes,scrollbars=yes,top=250,left=300,width=600,height=325");
}
var gridObj;
var assocRepID ='';
var assocRepName = '';
var voidinputstr = '';

function fnAssocRepMapping(){
     
     var accid = document.frmAccount.hAccountId.value;
     windowOpener("/GmAccountServlet?hAccountId="+accid+"&hAction=assocrepmap","AssocRepMapping","resizable=yes,scrollbars=yes,top=250,left=300,width=800,height=600");
}

/**
   * fnAccountCredit() - This method is used to open the DHTMLX pop up window of account credit hold setup screen.
   * 
   */

function fnAccountCredit(){	 
	 var accid = document.frmAccount.hAccountId.value;
   /* dhxWins = new dhtmlXWindows();
   dhxWins.enableAutoViewport(true);
   dhxWins.attachViewportTo(document.body);
   dhxWins.setImagePath("javascript/dhtmlx/dhtmlxWindows/imgs");

   popupWindow = dhxWins.createWindow("w1", 150, 250, 900, 550);
   popupWindow.attachURL('/gmCreditHold.do?method=fetchAcctCreditHold&accountId='+ accid , "AccountCreditHold","resizable=yes,scrollbars=yes,top=250,left=300,width=1200,height=800");
   popupWindow.setText("Account Credit Hold Setup");*/
   //popupWindow.setModal(true);
   windowOpener('/gmCreditHold.do?method=fetchAcctCreditHold&accountId='+ accid , "AccountCreditHold","resizable=yes,scrollbars=yes,top=250,left=300,width=880,height=510");
}
/* Function to Show the Rep Account Associated with the Parent Account.
*
*/
function fnShowRepAcc(){	 
	 var accid = document.frmAccount.hAccountId.value;
 	 windowOpener('/custservice/GmRepAccountInfo.jsp?repAccountId='+ accid , "RepAccountInfo","resizable=yes,scrollbars=yes,top=250,left=300,width=950,height=190");
}
/*
 * function - This method used to reload the selected account detail
 * 
 * */
function fnReloadContainer(){ //Function used for reload container after saving
     var accID = document.frmAccount.hAccountId.value;
     if(accID != '' && rCunt == '0'){    	
        //  parent.location.href = '/GmAccountServlet?method=loadEditAccount&hAction=Reload&hOpt=FETCHCONTAINER&hAccountId='+accID+'&hReloadCount=1';
     }
}

/*
 * This function used to show/hide the valid address information
 */
function fnShowHideValidShipAdd() {
	var lblAccID = document.frmAccount.hAccountId.value;
	var shipAddVerifyFl = document.frmAccount.hShippingAddVerified.value;
	var validAddObj = document.getElementById("showAddValidate");
	var inValidAddObj = document.getElementById("showAddInValidate");
	var validAddTxtObj = document.getElementById("showAddValidateTxt");
	var inValidAddTxtObj = document.getElementById("showAddInValidateTxt");
	var shipCountry = document.frmAccount.Cbo_ShipCountry.value;
	// To show the Sales Tax Address Validation Details
	if (shipCountry == '1101' && lblAccID != '') {
		if (shipAddVerifyFl == 'Y') {
			inValidAddObj.style.display = 'none';
			inValidAddTxtObj.style.display = 'none';
			validAddObj.style.display = 'block';
			validAddTxtObj.style.display = 'block';
		} else {
			validAddObj.style.display = 'none';
			validAddTxtObj.style.display = 'none';
			inValidAddObj.style.display = 'block';
			inValidAddTxtObj.style.display = 'block';
		}
	} else {
		validAddObj.style.display = 'none';
		validAddTxtObj.style.display = 'none';
		inValidAddObj.style.display = 'none';
		inValidAddTxtObj.style.display = 'none';
	}
}
/*
 * This function used to load the parent account infromation (Using AJAX)
 */
function fnLoadParentAcctInfo(obj) {
	
	var paraccid=document.frmAccount.Cbo_Parent_Account_Id.value;
	if (paraccid != 0) {
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/GmAccountServlet?hAction=loadParentInfo&Cbo_Parent_Account_Id=' + paraccid + '&randomId=' + Math.random());
		dhtmlxAjax.get(ajaxUrl, fnSetParentAcctDtls);
	} else {
		// setting the default values to JSP
		document.frmAccount.Txt_Parent_AccNm.value = '';
		document.frmAccount.Cbo_AccType.value = 0;
		document.frmAccount.Txt_ContPer.value = '';
		document.frmAccount.Txt_Phn.value = '';
		document.frmAccount.Txt_Fax.value = '';
		document.frmAccount.Txt_Comments.value = '';
		// bill to address
		document.frmAccount.Txt_BillName.value = '';
		document.frmAccount.Txt_BillAdd1.value = '';
		document.frmAccount.Txt_BillAdd2.value = '';
		document.frmAccount.Txt_BillCity.value = '';
		document.frmAccount.Cbo_BillState.value = 0;
		document.frmAccount.Cbo_BillCountry.value = 1101;
		document.frmAccount.Txt_BillZipCode.value = '';
		// ship to address
		document.frmAccount.Txt_ShipName.value = '';
		document.frmAccount.Txt_ShipAdd1.value = '';
		document.frmAccount.Txt_ShipAdd2.value = '';
		document.frmAccount.Txt_ShipCity.value = '';
		document.frmAccount.Cbo_ShipState.value = 0;
		document.frmAccount.Cbo_ShipCountry.value = 1101;
		document.frmAccount.Txt_ShipZipCode.value = '';
		document.frmAccount.Cbo_PrefCarrier.value = 0;
		document.frmAccount.Cbo_InternalRep.value = 0; 
		// Other
		document.frmAccount.Cbo_Payment.value = 4100; // Net 30 Days
		document.frmAccount.Txt_CreditRating.value = '';
		if(acctSrc == 'Y'){
			document.frmAccount.Cbo_Source.value = 0;
		}
		document.frmAccount.Cbo_CollectorId.value = 0;
		document.frmAccount.hShippingAddVerified.value = '';
		if(avaTaxFl == 'Y'){
			fnShowHideValidShipAdd();
		}
	}
}

/*
 * This function used to get the response value and set to form field
 */
function fnSetParentAcctDtls(loader) {
	var resJSONObj = $.parseJSON(loader.xmlDoc.responseText);
	// setting the values to JSP
	var acctType = document.frmAccount.Cbo_AccType.value;
	
	document.frmAccount.Txt_Parent_AccNm.value = resJSONObj["PARTYNM"];
	if (acctType != '26230710') { 
	document.frmAccount.Cbo_AccType.value = fnAddDropDown(resJSONObj["ACCTYPE"]);
	document.frmAccount.Txt_ContPer.value = resJSONObj["CPERSON"];
	document.frmAccount.Txt_Phn.value = resJSONObj["PHONE"];
	document.frmAccount.Txt_Fax.value = resJSONObj["FAX"];
	document.frmAccount.Txt_Comments.value = resJSONObj["COMMENTS"];
	// bill to address
	document.frmAccount.Txt_BillName.value = resJSONObj["BNAME"];
	document.frmAccount.Txt_BillAdd1.value = resJSONObj["BADD1"];
	document.frmAccount.Txt_BillAdd2.value = resJSONObj["BADD2"];
	document.frmAccount.Txt_BillCity.value = resJSONObj["BCITY"];
	document.frmAccount.Cbo_BillState.value = fnAddDropDown(resJSONObj["BSTATE"]);
	document.frmAccount.Cbo_BillCountry.value = fnAddDropDown(resJSONObj["BCOUNTRY"]);
	document.frmAccount.Txt_BillZipCode.value = resJSONObj["BZIP"];
	document.frmAccount.Cbo_InternalRep.value = fnAddDropDown(resJSONObj["INTERNAL_REP_ID"]);
	}
	// ship to address
	document.frmAccount.Txt_ShipName.value = resJSONObj["CNAME"];
	document.frmAccount.Txt_ShipAdd1.value = resJSONObj["SADD1"];
	document.frmAccount.Txt_ShipAdd2.value = resJSONObj["SADD2"];
	document.frmAccount.Txt_ShipCity.value = resJSONObj["SCITY"];
	document.frmAccount.Cbo_ShipState.value = fnAddDropDown(resJSONObj["SSTATE"]);
	document.frmAccount.Cbo_ShipCountry.value = fnAddDropDown(resJSONObj["SCOUNTRY"]);
	document.frmAccount.Txt_ShipZipCode.value = resJSONObj["SZIP"];
	document.frmAccount.Cbo_PrefCarrier.value = fnAddDropDown(resJSONObj["CARRIER"]);
	
	// to update the global variable (address validate)
	prevShipAdd1 = document.frmAccount.Txt_ShipAdd1.value;
	prevShipAdd2 = document.frmAccount.Txt_ShipAdd2.value;
	prevShipCity = document.frmAccount.Txt_ShipCity.value;
	prevShipState = document.frmAccount.Cbo_ShipState.value;
	prevShipCountry = document.frmAccount.Cbo_ShipCountry.value;
	prevShipZip = document.frmAccount.Txt_ShipZipCode.value;
	//
	document.frmAccount.Cbo_Payment.value = fnAddDropDown(resJSONObj["PTERM"]);
	document.frmAccount.Txt_CreditRating.value = resJSONObj["CREDITRATING"];
	if(acctSrc == 'Y'){
		document.frmAccount.Cbo_Source.value = fnAddDropDown(resJSONObj["ACCTSOURCE"]);
	}
	document.frmAccount.Cbo_CollectorId.value = fnAddDropDown(resJSONObj["COLLECTORID"]);
	document.frmAccount.hShippingAddVerified.value = resJSONObj["SHIPADDFL"];
	if(document.frmAccount.Cbo_Category != undefined){
	document.frmAccount.Cbo_Category.value = fnAddDropDown(resJSONObj["ACCTCATVALUE"]);
	}
	if(avaTaxFl == 'Y'){
		fnShowHideValidShipAdd();
	}
}
/*
 * This function used to return empty when drop down is 0
 */
function fnCorrectDropDownVal(val) {
	if (TRIM(val) == '0') {
		val = '';
	}
	return val;
}
/*
 * This function used to return 0 when drop down is empty
 */
function fnAddDropDown(val) {
	if (val == undefined || val == '') {
		val = '0';
	}
	return val;
}


/*
 * fnChangeAcctType()- This method is used to Load Dealer Name - ATEC-223.
 * 
 */

function fnChangeAcctType() {
	var acctType = document.frmAccount.Cbo_AccType.value;
	if (acctType == '26230710') { // Hospital-Dealer
//		document.getElementById("showDealerNameLn").style.display = 'block';
		document.getElementById("showDealerName").style.display = 'block';
		 fnSetTableOddEvenShade("tblParAcctInfo");
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmDealerSetup.do?method=fetchDealerList&Cbo_AccType='
				+ acctType + '&randomId=' + Math.random());
		dhtmlxAjax.get(ajaxUrl, fnSetChangeAcctType);		
	} else {
//		document.getElementById("showDealerNameLn").style.display = 'none';
		document.getElementById("showDealerName").style.display = 'none';
		 fnSetTableOddEvenShade("tblParAcctInfo");
		fnChangeDealer();
	}

}



/*
 * This function used to Populate the Dealer infromation (Using AJAX)
 */
function fnSetChangeAcctType(loader) {
	var compObj = document.getElementById("Cbo_Dealer_Id");
	var resJSONObj = $.parseJSON(loader.xmlDoc.responseText);
	var DealerId = document.frmAccount.Cbo_Dealer_Id.value;
	if (DealerId != "0") {
		fnDealerInfo(DealerId);
	} 
	document.frmAccount.hDealerIDStr.value = fnCorrectDropDownVal(document.frmAccount.Cbo_Dealer_Id.value);

}

/*
 * fnAccountDealerMap()- This method is used to open the Account Info window of Basic Info
 * screen ATEC-223.
 * 
 */

function fnAccountDealerMap() {
	var dealerId = document.frmAccount.Cbo_Dealer_Id.value;
	windowOpener("/gmDealerSetup.do?method=fetchDealerAccounMappingReport&dealerID="
			+ dealerId + "&strOpt=AccountMappingPopup", "DealerAccountDetails",
			"resizable=yes,scrollbars=yes,top=250,left=300,width=950,height=190");
}

/*
 * This function used to Show/Hide the Dealer Info 
 */
function fnChangeDealer() {
	var DealerId = document.frmAccount.Cbo_Dealer_Id.value;
	if (DealerId != "0") {
		fnDisable(true);
	} else {
		fnDisable(false);
	}

}

/*
 * This function used to hide/show the Dealer Info and set to form field
 */

function fnDisable(flag) {

	if(flag == false && accEditUpdAcsFl != 'Y' && accAddrsEditAcsFl != 'Y'){//disabling fields based on the access flag
		flag = true;
	}
	document.frmAccount.Cbo_Payment.disabled = flag;
	document.frmAccount.Txt_CreditRating.readOnly = flag;
	document.frmAccount.Cbo_CollectorId.disabled = flag;
	document.frmAccount.Txt_ContPer.readOnly = flag;
	document.frmAccount.Txt_Phn.readOnly = flag;
	document.frmAccount.Txt_Fax.readOnly = flag;
	document.frmAccount.Txt_BillName.readOnly = flag;
	document.frmAccount.Txt_BillAdd1.readOnly = flag;
	document.frmAccount.Txt_BillAdd2.readOnly = flag;
	document.frmAccount.Txt_BillCity.readOnly = flag;
	document.frmAccount.Cbo_BillState.disabled = flag;
	document.frmAccount.Cbo_BillCountry.disabled = flag;
	document.frmAccount.Txt_BillZipCode.readOnly = flag;

}


/*
 * This function used to load the Dealer account infromation (Using AJAX)
 */
function fnDealerInfo(obj) {
	var DealerId = document.frmAccount.Cbo_Dealer_Id.value;
	
	if (DealerId != 0 && DealerId !='') {
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmDealerSetup.do?method=fetchDearlerInfo&Cbo_Dealer_Id='
				+ DealerId + '&randomId=' + Math.random());
		dhtmlxAjax.get(ajaxUrl, fnSetDealerAcctDtls);
		fnChangeDealer();
	} else {
		fnChangeDealer();
	}
}

/*
 * This function used to get the response value and set to form field
 */
function fnSetDealerAcctDtls(loader) {

	var resJSONObj = $.parseJSON(loader.xmlDoc.responseText);
	document.frmAccount.Txt_BillName.value = fnParseUndefined(resJSONObj["BILLNAME"]);
	document.frmAccount.Txt_BillAdd1.value = fnParseUndefined(resJSONObj["ADD1"]);
	document.frmAccount.Txt_BillAdd2.value = fnParseUndefined(resJSONObj["ADD2"]);
	document.frmAccount.Txt_BillCity.value = fnParseUndefined(resJSONObj["CITY"]);
	document.frmAccount.Cbo_BillState.value = fnAddDropDown(resJSONObj["STATEID"]);
	document.frmAccount.Cbo_BillCountry.value = fnAddDropDown(resJSONObj["COUNTRYID"]);
	document.frmAccount.Txt_BillZipCode.value = fnParseUndefined(resJSONObj["ZIP"]);
	document.frmAccount.Cbo_Payment.value = fnAddDropDown(resJSONObj["PAYMENTTERMS"]);
	document.frmAccount.Txt_CreditRating.value = fnParseUndefined(resJSONObj["CREDITRATING"]);
	document.frmAccount.Cbo_CollectorId.value = fnAddDropDown(resJSONObj["COLLETORID"]);
	document.frmAccount.Txt_ContPer.value = fnParseUndefined(resJSONObj["CPERSON"]);
	document.frmAccount.Txt_Phn.value = fnParseUndefined(resJSONObj["CONTACTNUMBER"]);
	document.frmAccount.Txt_Fax.value = fnParseUndefined(resJSONObj["FAX"]);
	var obj1 = resJSONObj["ACCTCOUNT"];
	var DealerId = document.frmAccount.Cbo_Dealer_Id.value;
	if (DealerId == "0" || fnAddDropDown(resJSONObj["ACCTCOUNT"]) == "0") {
		document.getElementById("imgAccDisp").style.display = 'none';
	} else {
		document.getElementById("imgAccDisp").style.display = 'inline-block';
	}
}
function fnParseUndefined(val) {
	if (val == undefined || val ==null) {
		val = '';
	}
	return val;
}
// this function is used to disable all the fields in the table 
function disableTableFields(tableId){

	var inputTag = tableId.getElementsByTagName('input'); 
	var inputSize = inputTag.length;
	for(var i=0;i<inputSize;i++){
		if(inputTag[i].type != 'button'){
			inputTag[i].disabled = true; 
		}
	}
	var selectTag = tableId.getElementsByTagName('select'); 
	var selectSize = selectTag.length;
	for(var i=0;i<selectSize;i++){
		
		selectTag[i].disabled = true; 
		
	}
	
}

//to disable all the fields in table based on the access flag
function fnDisableFields(){
	
	if(accRepEditAcsFl != 'Y' && accEditUpdAcsFl != 'Y'){//disabling rep account information
		disableTableFields(document.getElementById('tblAcctInfo'));
	}
	if(accEditUpdAcsFl != 'Y'){//disabling parent account information
		disableTableFields(document.getElementById('tblParAcctInfo'));
	}
	if(accAddrsEditAcsFl != 'Y' && accEditUpdAcsFl != 'Y'){ // disabling address info and account info
		disableTableFields(document.getElementById('tblAddressInfo'));	
		disableTableFields(document.getElementById('tblAcctInfor'));	
	}
	if(accAddrsEditAcsFl == 'Y'){//enabling rep account name
		document.frmAccount.Txt_Rep_AccNm.disabled = false;
		if(document.frmAccount.Txt_Rep_AccNmEn != undefined)
		document.frmAccount.Txt_Rep_AccNmEn.disabled = false;
	}
}
