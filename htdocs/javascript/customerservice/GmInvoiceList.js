
function fnGo()
{
	Error_Clear();
	var sourceType = document.frmAccount.Cbo_InvSource.value;
	var accID = document.frmAccount.Cbo_AccId.value;
	var txtAccID  = document.frmAccount.Txt_AccId.value;
	var parentFl  = document.frmAccount.Chk_ParentFl.checked;
	var statusType = document.frmAccount.Cbo_Type.value;

		if(document.frmAccount.Txt_PO.value == '' && document.frmAccount.Txt_InvoiceID.value == '' && document.frmAccount.Txt_OrderID.value == ''){
				var dateRange = dateDiff(document.frmAccount.Txt_FromDate.value,document.frmAccount.Txt_ToDate.value,format);
				if(isNaN(dateRange) || dateRange > 30 || dateRange < 0){
					Error_Details(message[5281]);
				}
			}

    var accountNm  = document.frmAccount.Cbo_AccId.value;
	if(txtAccID == '' && (accountNm != '0' || accountNm != ''))
 	{
 		document.frmAccount.Txt_AccId.value = accountNm;
 	}else{
 		document.frmAccount.Cbo_AccId.value = txtAccID;
 	}

	if (ErrorCount > 0)
	{
		Error_Show();
		return false;
	}

	fnStartProgress('Y');
	document.frmAccount.hAction.value = "Reload";
	document.frmAccount.submit();
}
function fnLoad()
{
	var varSource = '<%=strInvSource%>';
	var varIdAccount = '<%=strIdAccount%>';
	
	if (varSource != '')
	{
		document.all.Cbo_InvSource.value = varSource;
	}
	if (varIdAccount!='')
	{
		document.all.Cbo_AccId.value = varIdAccount;
	}
	
	var obj = document.frmAccount.Cbo_Action;
	for (var i=0;i<obj.length ;i++ )
	{
		if (document.frmAccount.hMode.value == ooptions[i].value)
		{
			obj.options[i].selected = true;
			break;
		}
	}

}
function fnCallEdit()
{
	Error_Clear();
	
	var len = document.frmAccount.hLen.value;
	var cnt = 0;
	var sfl = "";
	var obj = "";
	

	if ( len == 1)
	{
		document.frmAccount.hPO.value = document.frmAccount.rad.value;
		document.frmAccount.hInvId.value = document.frmAccount.rad.id;
		//obj = eval("document.frmAccount."+document.frmAccount.hPO.value);
		//sfl = obj.value;
	}
	else if ( len >1)
	{
		var arr = document.frmAccount.rad;
	
			for (var i=0;i<arr.length ;i++ )
		{
			if (arr[i].checked == true)
			{
				document.frmAccount.hPO.value = arr[i].value;
				document.frmAccount.hInvId.value = arr[i].id;
				//obj = eval("document.frmAccount."+arr[i].value);
				//Error_Details(message[obj);
				//sfl = obj.value;
				cnt++;
				break;
			}

		}
		if (cnt == 0)
		{
			Error_Details(message[584]);
		}
	
	}
	if (document.frmAccount.Cbo_Action1.value == "IC" || document.frmAccount.Cbo_Action1.value == "EC")
	{
		if ((document.frmAccount.Cbo_InvSource.value!=  '50255' )&& (document.frmAccount. Cbo_InvSource.value != 50253)&& (document.frmAccount. Cbo_InvSource.value != '26240213') )	
			{
			Error_Details(message[585]);
			}
	  
			document.frmAccount.hAction.value = "IssueCredit";
			document.frmAccount.action = "GmProcessCreditsServlet";
	}
	else if (document.frmAccount.Cbo_Action1.value == "PP")
	{
	
		if ((document.frmAccount.Cbo_InvSource.value!=  '50255' )&& (document.frmAccount. Cbo_InvSource.value != 50253) && (document.frmAccount. Cbo_InvSource.value != '26240213'))	
			{
			Error_Details(message[586]);
			}
			document.frmAccount.hAction.value = "PAY";
			document.frmAccount.action = "GmInvoiceServlet";
	}	
	
		else if (document.frmAccount.Cbo_Action1.value == "VO")
	{
			if ((document.frmAccount.Cbo_InvSource.value!=  '50255' )&& (document.frmAccount. Cbo_InvSource.value != 50253) && (document.frmAccount. Cbo_InvSource.value != '26240213'))
			{
				Error_Details(message[587]);
			}
					document.frmAccount.action = "/GmCommonCancelServlet";
			        document.frmAccount.hTxnId.value = document.frmAccount.hInvId.value;
					document.frmAccount.hCancelType.value = 'VDINV';
					document.frmAccount.hAction.value = "Load";
					document.frmAccount.hCancelReturnVal.value = "true";
	}else if (document.frmAccount.Cbo_Action1.value == "IDB"){ 
		 document.frmAccount.hAction.value = "IssueDebit"; 
		 document.frmAccount.action = "GmProcessCreditsServlet"; 
	}else if (document.frmAccount.Cbo_Action1.value == "IU"){
		// to append the company info
		document.frmAccount.action = "/gmInvoiceUpload.do?strOpt=load&invoiceId="+ document.frmAccount.hInvId.value + "&" + fnAppendCompanyInfo();
	}else if(document.frmAccount.Cbo_Action1.value == "IL"){// For Invoice Layout
		document.frmAccount.hAction.value = "PAY";
		document.frmAccount.hScreen.value = "Layout";
		document.frmAccount.action = "GmInvoiceServlet";
	}
	else if(document.frmAccount.Cbo_Action1.value == 0)
	{
			Error_Details(message[588]);
	}
	 
	if (ErrorCount > 0)
	{
			Error_Show();
			return false;
	}
	document.frmAccount.hMode.value = "LoadOrder";
	document.frmAccount.submit();
}


function fnPrintInvoice(varinv)
{
windowOpener('/GmInvoiceInfoServlet?hAction=Print&hInv='+varinv,"PrntInv","resizable=yes,scrollbars=yes,top=150,left=200,width=750,height=600");
}

function fnPrintVoidInvoice(varinv)
{
windowOpener('/GmInvoiceInfoServlet?hAction=Print&hVoidFlag=VoidInvoice&hInv='+varinv,"PrntInv","resizable=yes,scrollbars=yes,top=150,left=200,width=750,height=500");
}

