
function fnAddNewRow(val, pnum) {
	count++;
	var hcnt = parseInt(eval("document.all.hTextboxCnt").value);
	hcnt = hcnt + count;
	usedLotSize = usedLotSize +1;
	//var hCnumFl = eval("document.all.hCnumFl"+val).value;	
	var NewRemoveHtml = "<span border=1 id='IconRemove"
			+ hcnt
			+ "' align='top'><BR>&nbsp;&nbsp; <input type='hidden' name='hUsagePartNum"
			+ hcnt
			+ "' value='"
			+ pnum
			+ "'  id='hUsagePartNum"
			+ hcnt
			+ "'/> <input type='hidden' name='hPartNum"
			+ hcnt
			+ "' id='hPartNum"
			+ hcnt
			+ "' value='"
			+ pnum
			+ "' /><a href=javascript:fnDelete("
			+ hcnt
			+ ");><img src=/images/btn_remove.gif border=0 height=13 width=13 align='right' valign='top' ></a> </span>";
	var QtyObj11 = eval("document.all.CellRemove" + val);
	QtyObj11.insertAdjacentHTML("beforeEnd", NewRemoveHtml);

	var currQty = eval("document.all.hLotQty" + val);
	var currQtyVal = currQty.value;
	var NewQtyHtml = "<span border=1 id='RmQty"
			+ hcnt
			+ "'><BR>&nbsp;<input type='text' maxlength='2' size='2' value='' name='txt_usage_qty"
			+ hcnt
			+ "' id='txt_usage_qty"
			+ hcnt
			+ "' class='InputArea' onFocus=changeBgColor(this,'#AACCE8'); onBlur=changeBgColor(this,'#ffffff'); ></span>";
	var QtyObj = eval("document.all.Qty" + val);
	QtyObj.insertAdjacentHTML("beforeEnd", NewQtyHtml);

	var NewControlNumHtml = "<span border=1 id='RmControl" + hcnt
			+ "'><BR>&nbsp;&nbsp;<select name='Cbo_controlNum"+ hcnt+"' id='Cbo_controlNum"+hcnt+"' style='width:150px'>"+ get(pnum)+"</select>";			
			+" </span><span id='DivShowCnumAvl"+hcnt+"' style='vertical-align:middle; display: none;'>  <img height=16 width=19 title=Control number available src=/images/success.gif></img> </span>"
			+"<span id='DivShowCnumNotExists"+hcnt+"' style=' vertical-align:middle; display: none;'>  <img height=12 width=12 title=Control number does not exists src=/images/delete.gif></img></img>&nbsp;<a href=javascript:fnDelete("+hcnt+")><img height=15 border=0 title=\'Lot Code Report\' src=/images/location.png></img></a> </span>";
	var ControlNumObj = eval("document.all.controlNum" + val);
	ControlNumObj.insertAdjacentHTML("beforeEnd", NewControlNumHtml);

}

function fnDelete(val) {
		
		var ObjIcon = eval("document.all.IconRemove" + val);
		var ObjQty = eval("document.all.RmQty" + val);
		var ObjCtrl = eval("document.all.RmControl" + val);
		ObjIcon.innerHTML = "";
		ObjQty.innerHTML = "";
		ObjCtrl.innerHTML = "";
	
}

function fnCreateUsedLotStr (pnum, qty){
	var usageLotStr = "";
	var totalCnt = parseInt(eval("document.all.hTextboxCnt").value);
	if(totalCnt !=0 ){
		var errorDataFl = fnValidateQty (pnum, qty);
		if(!errorDataFl){
			for (i = 1; i <= usedLotSize; i++) {
				var objPnum = eval("document.all.hUsagePartNum" + i);
				if(objPnum != undefined || objPnum != null){
					var objQty = eval("document.all.txt_usage_qty" + i);
					var QtyVal = TRIM(objQty.value);
					var objUsageLot = eval("document.all.Cbo_controlNum" + i);

					if(pnum == objPnum.value){
						usageLotStr += objPnum.value +'^'+ QtyVal + '^' + objUsageLot.value+'|';
					}
				}
			}
		}
	}
	return usageLotStr;
}

function fnValidateQty (pnum, totQty){
	var totalCnt = parseInt(eval("document.all.hTextboxCnt").value);
	var usageQty = 0;
	var errorFl = false;
	for (i = 1; i <= usedLotSize; i++) {
		var objPnum = eval("document.all.hUsagePartNum" + i);
		if(objPnum != undefined || objPnum != null){
			var objQty = eval("document.all.txt_usage_qty" + i);
			var QtyVal = TRIM(objQty.value);
			var objUsageLot = eval("document.all.Cbo_controlNum" + i);
			var isDisabled = objUsageLot.disabled;
			
			if(pnum == objPnum.value){
				if(objUsageLot.value == 0 && objUsageLot.value != '' && isDisabled == false){
					errorFl = true;
					Error_Details(Error_Details_Trans(message_operations[426],pnum));
				}
				if(objUsageLot.value != 0 && QtyVal ==''){
					errorFl = true;
					Error_Details(Error_Details_Trans(message_operations[427] ,pnum));
				}
				var contolSelect =  objUsageLot.options[objUsageLot.selectedIndex].text;
				var indexsize=contolSelect.lastIndexOf( ')' )-1;
				var indexFirst=contolSelect.lastIndexOf( '(' )+1;
				var ctrlnumQty=parseInt(contolSelect.substr(indexFirst,indexsize));
								
				intQtyVal = parseInt(QtyVal);
				if(ctrlnumQty !='' && intQtyVal > ctrlnumQty){
					
					errorFl = true;
					Error_Details(Error_Details_Trans(message_operations[428],pnum));
					
				}
				if(QtyVal !=''){
					usageQty += parseInt(QtyVal);
				}			
			}
		}
		
		
	}
	
	if(totQty !=  usageQty && usageQty != 0){
		errorFl = true;
		Error_Details(Error_Details_Trans(message_operations[429],pnum));
	}
	return errorFl;
}

function fnValidateCtrlNum (screenNm){
	fnSubmit('');
}



var tempCntlNum;
var errparts ;
var tissueControls = '';
var checkVeriFl = "";
function put(key,val) 
{ 
    var elementIndex = this.findIt(key); 
     
    if( elementIndex == (-1) ) 
    { 
        this.keyArray.push( key ); 
        this.valArray.push( val ); 
    } 
    else 
    { 
        this.valArray[ elementIndex ] = val; 
    } 
} 
 
function get(key) 
{ 
    var result = null; 
    var elementIndex = this.findIt( key ); 
 
    if( elementIndex != (-1) ) 
    {    
        result = this.valArray[ elementIndex ]; 
    } 
    else {
    	result = "<option value= >[Choose One]</option>";
    }
    return result; 
}
function findIt(key) 
{ 
    var result = (-1); 
 
    for( var i = 0; i < this.keyArray.length; i++ ) 
    { 
        if( this.keyArray[ i ] == key ) 
        { 
            result = i; 
            break; 
        } 
    } 
    return result; 
} 
function fnShowFilters(val) {
	var obj = eval("document.all." + val);
	var obj1 = eval("document.all." + val + "img");
	if (obj.style.display == 'none') {
		obj.style.display = 'block';
		if (obj1) {
			obj1.src = '/images/minus.gif';
		}
	} else {
		obj.style.display = 'none';
		if (obj1) {
			obj1.src = '/images/plus.gif';
		}
	}
}

