var gridObj;
var assocRepID ='';
var assocRepName = '';
var voidinputstr = '';

function fnOnLoad(){ 
	
	if (objGridData.value != '') {
		gridObj = initGridData('ASSOCREPDATA',objGridData);
		assocRepID = gridObj.getColIndexById("AssocRepid");
		assocRepName = gridObj.getColIndexById("AssocRepName");
	    
	}
}
//This function is used to initiate grid
function initGridData(divRef,gridData){
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);
	gObj.init();
	gObj.loadXMLString(gridData);	
	return gObj;	
}

function fnRepMap(){
	var assocRepName = Cbo_AssocRepId.getSelectedText();
	var assocRepId = Cbo_AssocRepId.getSelectedValue();
	var assocrepadded = 'Y';
	if(assocRepId!='0' && assocRepId != null ){
	assocrepsize = assocrepsize + 1;
	gridObj.addRow(assocrepsize,'');	
	gridObj.cells(assocrepsize,0).setValue(assocRepId);
	gridObj.cells(assocrepsize,1).setValue(assocRepName);
	gridObj.cells(assocrepsize,2).setValue('<a href="#" onClick=fndelete('+assocrepsize+')><img alt="Click to Delete Row"  width="17" height="17" src="/images/dhtmlxGrid/delete.gif" border="0"></a>');
	gridObj.cells(assocrepsize,3).setValue(assocrepadded);		
	}
}

function fndelete(rowid){
	var Repid = '';	
	//var assocRepid  = gridObj.getColIndexById("AssocRepid");
	var voidflagchk = gridObj.getColIndexById("Voidflag");
	var voidfl = TRIM(gridObj.cellById(rowid, voidflagchk).getValue());
	Repid = TRIM(gridObj.cellById(rowid, assocRepID).getValue());
	
	if(voidfl != 'Y'){
	voidinputstr += acctid + '^'+ Repid+ '|';
	}
	gridObj.deleteRow(rowid);
	document.frmAccountRepMap.hAccountId.value = acctid;
	document.frmAccountRepMap.hVoidString.value = voidinputstr;
}
//This function is used to activate Load button while pressing the enter key
function fnEnter() {
	if(event.keyCode == 13){
		fnRepMap();
	}
}

function fnSaveAssocRepMap(){
	var inputstr = '';
	var Repid = '';
	var cnt = 0;
	var count = 0;
	var validatestring = '';
	var assocrepnm = ''
	var voidstring = document.frmAccountRepMap.hVoidString.value;
	gridObj.forEachRow(function(rowId){
		Repid = TRIM(gridObj.cellById(rowId, assocRepID).getValue());
		if(validatestring.indexOf(Repid+",")==-1){
			validatestring += (Repid + ",");	 
		}else{
			
			assocrepnm = TRIM(gridObj.cellById(rowId, assocRepName).getValue());
			count = count+1;
		}
		if(Repid == repid){
			cnt = cnt+1;
		}
		
		if(Repid != ''){
		inputstr += acctid + '^'+ Repid+'|';
		}
	});
	
	if(cnt>0){
		Error_Details(message_operations[246]);
	}
	if(count>0){
		Error_Details(Error_Details_Trans(message_operations[247],assocrepnm));
	}
	if(voidstring == '' && inputstr == ''){
		Error_Details(message_operations[248]);
	}
	if (ErrorCount > 0){
		Error_Show();
	    Error_Clear();
		return false;
	}
	document.frmAccountRepMap.hAccountId.value = acctid;
	document.frmAccountRepMap.hstrOpt.value = "Save";
	document.frmAccountRepMap.hInputString.value = inputstr;
	document.frmAccountRepMap.hAction.value = "assocrepmap";
	document.frmAccountRepMap.action.value = '/GmAccountServlet';
	document.frmAccountRepMap.submit();
}