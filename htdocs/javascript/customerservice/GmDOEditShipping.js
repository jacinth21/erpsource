function fnOnLoad(){
   document.frmDOSummaryEdit.shipTo.value = shippedTO;
   var shipid =document.frmDOSummaryEdit.shipTo;
   repid = shippedName; // assign selected value
   fnGetNames(shipid);
   //document.frmDOSummaryEdit.names.value = shippedName;
   repid = caseRepID; // assign case sale rep id
   addid = ''; // reset to empty otherwise on change other rep, address will show empty.
}
function fnEditMultiShipping(){
	document.frmDOSummaryEdit.action ='/gmDOSummaryEdit.do?method=editMultipleShipDetails&orderID='+ordId+'&orderTypeNm='+orderTypeNm+'&accountID='+accid+'&custPO='+custPo;
	document.frmDOSummaryEdit.submit();
}
function fnSave(val){
	
	var saleRepID = '';
	var repAddID ='';
	if(val == 'other'){
	fnValidateDropDn('shipTo',message_operations[271]);	
	fnValidateDropDn('names',lblNames);
	fnValidateDropDn('shipMode',lblMode);
	if(document.frmDOSummaryEdit.shipTo.value == 4121){
		fnValidateDropDn('addressid',lblAddress);
	}
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	}
	document.frmDOSummaryEdit.orderID.value = ordId;
	//document.frmDOSummaryEdit.shipToID.value = ; //salesrep
	if(val == 'other'){
		saleRepID = document.frmDOSummaryEdit.names.value;
		document.frmDOSummaryEdit.shipId.value =saleRepID;
		repAddID = document.frmDOSummaryEdit.addressid.value;
	    document.frmDOSummaryEdit.repAddressID.value =repAddID;
	}else{
		document.frmDOSummaryEdit.shipTo.value = 4121;
		document.frmDOSummaryEdit.shipId.value =repid;
		document.frmDOSummaryEdit.repAddressID.value = val;	
	}
	document.frmDOSummaryEdit.source.value = "11389";
	document.frmDOSummaryEdit.shipCarrier.value = ""; //Passing Empty due to need to save corresponding Ship Carrier instead off FedEx for All the Ship Mode values.
	//document.frmDOSummaryEdit.shipMode.value = "5004";
	document.frmDOSummaryEdit.strOpt.value = "save";
	document.frmDOSummaryEdit.caseInfoID.value = caseInfoID;
	document.frmDOSummaryEdit.action ='/gmDOSummaryEdit.do?method=editShipDetails&'+fnAppendCompanyInfo();
	document.frmDOSummaryEdit.submit();
}


function fnAddress(){
	fnValidateDropDn('shipTo',message_operations[271]);
	fnValidateDropDn('names',lblNames);

	shipto = document.all.shipTo.value;
	shiptoid = document.all.names.value;
	shiptoidid = document.all.names.options[document.all.names.selectedIndex].id;

	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
		//	return false;
	}
	else
	{
		dhxWins = new dhtmlXWindows();
	    dhxWins.enableAutoViewport(true);
	    
		if (shipto == '4121' && shiptoid !='0')
		{
			w1 = dhxWins.createWindow("w1", 30, 100, 780, 700);
			w1.button("park").hide();
			w1.button("minmax1").hide();
		    w1.setText(message_operations[272]);
			w1.attachURL("/GmPageControllerServlet?strPgToLoad=gmAddressSetup.do&method=loadAddressSetup&includedPage=false&transactPage=CASE&partyId="+shiptoidid);
			
		}
		else { // only pass the shipto and shiptoid
			w1 = dhxWins.createWindow("w1", 30, 100, 350, 250);
			w1.button("park").hide();
			w1.button("minmax1").hide();
		    w1.setText(message_operations[272]);
			w1.attachURL("/GmSearchServlet?hAction=getAddress&hShip="+shipto+"&hShipId="+shiptoid);
		}
	}
}
function fnCancel(){

	var orderID = ordId;
	var parentOrdId = document.frmDOSummaryEdit.parentOrderID.value;
	if(parentOrdId!=''){
		orderID = parentOrdId;
	}
	
	document.frmDOSummaryEdit.action = "/GmEditOrderServlet?hMode=PrintPrice&hParantForm=CASE&hOrdId="+orderID;
	document.frmDOSummaryEdit.submit();
}