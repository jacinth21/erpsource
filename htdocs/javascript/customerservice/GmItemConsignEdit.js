
var errparts ;
var tissueControls = "";

function fnSubmit()
{
	tissueControls = '';
	hcnt = parseInt(document.frmVendor.hCnt.value);
	hcnt = hcnt+cnt;
	document.frmVendor.hCnt.value = hcnt;
	Error_Clear();
	var strErrorMore = '';
	var strErrorLess = '';
	var errorTissueParts = "";
	var shipfl = document.frmVendor.Chk_ShipFl.checked;
	document.frmVendor.hAction.value = 'SaveControl';
    
	if (document.frmVendor.hShipFl.value == '0' && document.frmVendor.Chk_ShipFl.checked == true)
	{
		document.frmVendor.Chk_ShipFl.value = '4';
	}
	else if (document.frmVendor.hShipFl.value == '1' && document.frmVendor.Chk_ShipFl.checked == true)
	{
		document.frmVendor.Chk_ShipFl.value = '3';
	}
	else
	{
		document.frmVendor.Chk_ShipFl.value = '2';
	}

	if (document.frmVendor.hAction.value == 'SaveControl')
	{
		var theinputs = document.getElementsByTagName("input");
	    var arr = document.frmVendor.pnums.value.split(',');
	    var blk = '';
	    var cnum = '';
	    var qtycnt = 0;
	    var pnumMaterial = "";
	    hcnt++;
		var blCnumFlag = false;
		for(var i=0;i<arr.length;i++)
	    {
	       qid = arr[i];
	       // For remove special characters in Part Number
	       pnum1 = replacePartSpecialchars(qid);
//	       blk = eval("document.frmVendor.hQty"+pnum1);
           //By below code for getting blk value as getting the total qty, we checked with multiple split also, its working properly (PC-5469)
            
                var blkArr = eval("document.frmVendor.hQty" + pnum1);
                if (blkArr.length > 1) {
                        blk = eval("document.frmVendor.hQty" + pnum1 + "[0]");
                }
                else {
                    blk = eval("document.frmVendor.hQty" + pnum1);
                }  
            
			    for(var j=0;j<hcnt;j++)
			    {
					pnumobj = eval("document.frmVendor.hPartNum"+j);
					if (pnumobj)
					{
						if (pnumobj.value == qid)
						{
							qtyobj = eval("document.frmVendor.Txt_Qty"+j);
							cnumobj = eval("document.frmVendor.Txt_CNum"+j);
							if(!isNaN(qtyobj.value))
							{
								qty = parseFloat(qtyobj.value);
								cnum = TRIM(cnumobj.value.toUpperCase());
								document.getElementById("Txt_CNum"+j).value = cnum;// to set converted uppercase control number back to screen					
								if (qty > 0)
								{
									qtycnt = qtycnt + qty;
								}
							}
							else
							{
								alert(message[10136]);
							}
							// Validation for not allowing Scanning same COntrol Number Twice for Tissue Parts
							if(document.getElementById("hPartMaterialType"+i) != undefined){
								var pnumMaterialObj = document.getElementById("hPartMaterialType"+i);
								pnumMaterial = pnumMaterialObj.value;
								}			
							var errparts = fnValidateAllControlNo(pnumobj.value,cnum,qty,pnumMaterial);	
							if(errparts != ''){
								//errorTissueParts +=  ", <BR>"+errparts;
								errorTissueParts = errorTissueParts + "<br>" +errparts;
							}
							
							if (shipfl)
							{
								if((cnum == 'TBE') || (cnum == ''))
								{
									blCnumFlag = true;
									break;
								}
							}
						}
					} // IF Valid object
				} // END of Inner FOR loop
				
			if (qtycnt > blk.value )
			{
				strErrorMore  = strErrorMore + ","+ qid+"/"+cnumobj.value;
			}
			if (qtycnt < blk.value )
			{
				strErrorLess  = strErrorLess + ","+ qid+"/"+cnumobj.value;
			}
			
			qtycnt = 0;
		}		
		
		if (blCnumFlag){
			Error_Details(message[10004]);
		}
		if(errorTissueParts != ""){
			Error_Details(Error_Details_Trans(message[10005],errorTissueParts));
			
		}
		if (strErrorMore != '')
		{
			Error_Details(Error_Details_Trans(message[10006],strErrorMore.substr(1,strErrorMore.length)));
		}
		if (strErrorLess != '')
		{
			Error_Details(Error_Details_Trans(message[10007],strErrorLess.substr(1,strErrorLess.length)));
		}
		if (ErrorCount > 0)
		{
			Error_Show();
			return false;
		}else{
			document.frmVendor.hAction.value = 'SaveControl';
			document.frmVendor.hMode.value = "CONTROL";
			
			if ((consignType == '4110'||consignType == '4114' || consignType=='4113')|| consignType=='4116' || consignType == '4112' || consignType == '4115' || strRuleAjax == 'YES') // For Consignment/Re-package
			{				
				document.frmVendor.txnStatus.value ='PROCESS';
				if (shipfl == true)
				{
					document.frmVendor.txnStatus.value = 'VERIFY';
				}				
			}
			/* if(includeRuleEng != 'NO'){
				document.frmVendor.action = '/gmRuleEngine.do';
			} */
			document.frmVendor.chkRule.value = 'Yes';
			fnStartProgress();
		  	document.frmVendor.submit();
	  	}
	 }	
}

var cnt = 0;
function fnGenerate(pnum,controlno){
	var cnum = '';
	var val='';
	var control = new Object();
	var cnumstatus = false;
	var qtyobj = new Object();
	var qty = 0;
	var qtycnt = 0;
	var orgqty = 0;
	var vPnumProdMetType = '';
	var pnum1 = pnum.replace(/\./g,'');
	pnum1 = pnum1.replace(/\-/g,'');
	var blk = document.getElementById("hQty"+pnum1);
	if(blk != null){
		orgqty = parseInt(blk.value);
	}
	var pnumlistobj = document.getElementById(pnum); 
	if(pnumlistobj != null){
	var pnumlist = pnumlistobj.value;
	var pnumlist = pnumlist.substring(0,pnumlist.length-1);
	var cnumarr= pnumlist.split(",");
	var cnumsize = cnumarr.length;
	//couting all sum of qty for passing part
		if(cnumsize > 0){
			for(i=0; i<cnumsize; i++){
				 qtyobj = document.getElementById("Txt_Qty"+cnumarr[i]);	
				 var cnumobj = document.getElementById('Txt_CNum'+cnumarr[i]);
				 if(qtyobj != null){
					 qty = parseInt(qtyobj.value);
					 if(cnumobj != null){
						 cnum = cnumobj.value;
					 }
					 if (qty > 0 && qty != "" && cnum != null && cnum != '' && cnum != 'TBE'){
						 qtycnt = qtycnt + qty;
					 }
				 }
			}
			if(qtycnt >= orgqty){
				Error_Details(Error_Details_Trans(message[10008],pnum));
				return false;
			}else{
				// updating control number and qty in existing row
				for(i=0; i<cnumsize; i++){
					val = cnumarr[0];
					var cnumobj = document.getElementById('Txt_CNum'+cnumarr[i]); 					
					if(cnumobj != null){
						qtyobj = document.getElementById("Txt_Qty"+cnumarr[i]);
						qty = parseInt(qtyobj.value);
						cnum = cnumobj.value;
						 if (cnum == controlno){
								 if(qty+1 > orgqty){
									 Error_Details(Error_Details_Trans(message[10008],pnum));
									 return false;
								 }else{
									 if(validateObject(document.getElementById("partMaterialType"))){// Getting Part num Product Material type
										   vPnumProdMetType = document.getElementById("partMaterialType").value;	
									 }
									 if(vPnumProdMetType == '100845'){ // Tissue Part
										 var array=[pnum,cnum];
										 Error_Details(Error_Details_Trans(message[10009],array));
										  return false;
									 }else{
										 qtyobj.value = qty + 1;
									 }
								 }
							 cnumstatus = false;
							 break;	
						 }else if(cnum == null || cnum == '' || cnum == 'TBE'){
							 cnumobj.value = controlno;
							 qtyobj.value = 1;
							 cnumstatus = false;
							 break;
						 }else{
							 cnumstatus = true;
						 }
					}
				}
				
			}
		}
	}else{
		Error_Details(Error_Details_Trans(message[10010],pnum));
		return false;
	}
	//updating control number and qty in newly added row
	if(cnumstatus){
		fnSplit(val,pnum,'Y');
		var count = parseInt(document.getElementById("hNewCnt").value);	
		var hcount = parseInt(document.getElementById("hCnt").value);
		hcount = hcount+count;
		var cnumobj = document.getElementById('Txt_CNum'+hcount); 
		cnumobj.value = controlno;				 		
		qtyobj = document.getElementById("Txt_Qty"+hcount);
		qtyobj.value = 1;
	}
	
	if (ErrorCount > 0 ){
		Error_Show();
	    Error_Clear();
	    return false;
	}else{
		fnScannedPartCount(pnum);
		//caling rule validation
		var strAction = document.getElementById("hAction").value;
		if(strAction != 'EditVerify' && vIncludeRuleEng != 'NO'){
			var ruleTransType =document.getElementById("RE_TXN").value;
			var TransTypeID =document.getElementById("TXN_TYPE_ID").value; 
			control.value = controlno;
			fnFetchMessages(control,pnum,'PROCESS',ruleTransType,TransTypeID);
		}
	}
	}

var cnt = 0;
function fnSplit(val,pnum,scanned)
{	
	cnt = parseInt(document.getElementById("hNewCnt").value);	
	cnt++;
	document.getElementById("hNewCnt").value = cnt;
	var hcnt = parseInt(document.getElementById("hCnt").value);
	hcnt = hcnt+cnt;
	var pnumobj = document.getElementById(pnum);
	if(pnumobj != null){		
		pnumobj.value = pnumobj.value+hcnt+",";
	}
	var priceobj = eval("document.frmVendor.hPrice"+val);
	var price = priceobj.value;
	var pnumProdMaterialObj = document.getElementById("hPartMaterialType"+val);
	if ( pnumProdMaterialObj != null && pnumProdMaterialObj != undefined ){
	var pnumProdMaterialVal =  pnumProdMaterialObj.value;	
	}
	var vFetchMsg = (includeRuleEng != 'NO')?"fnFetchMessages(this,'"+pnum+"','PROCESS',Type,TransTypeID);":"";
	var NewQtyHtml = "<span id='Sp"+cnt+"'><BR>&nbsp;&nbsp;&nbsp;<input type='hidden' name='hPartNum"+hcnt+"'  id='hPartNum"+hcnt+"' value='"+pnum+"'><input type='hidden' name='hPrice"+hcnt+"' id='hPrice"+hcnt+"' value='"+price+"'><input type='hidden' name='hPartMaterialType"+hcnt+"' id='hPartMaterialType"+hcnt+"' value='"+pnumProdMaterialVal+"'><input type='text' size='3' value='' name='Txt_Qty"+hcnt+"' id='Txt_Qty"+hcnt+"' class='InputArea Controlinput' onFocus=chgTRBgColor("+hcnt+",'#AACCE8'); onBlur=chgTRBgColor("+hcnt+",'#ffffff'); tabindex=1>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='text' size='22' value='' id='Txt_CNum"+hcnt+"' name='Txt_CNum"+hcnt+"' class='InputArea Controlinput' onFocus=fnBlankLine("+hcnt+",this.value); onBlur=fnBlankTBE("+hcnt+",this.value); onChange='"+vFetchMsg+"' tabindex=1>&nbsp;<a href=\"javascript:fnDelete('"+pnum+"',"+cnt +",'"+scanned+"')\";><img src=/images/btn_remove.gif border=0></a></span>";
	var QtyObj = eval("document.all.Cell"+val);
	QtyObj.insertAdjacentHTML("beforeEnd",NewQtyHtml);
}

function fnDelete(pnum,val,scanned){
	obj = eval("document.all.Sp"+val);
	obj.innerHTML = "";
	fnDeleteScanned(pnum,scanned); 
}

function fnInitiate()
{
	document.frmVendor.hAction.value = 'Initiate';
  	document.frmVendor.submit();
}

function fnPicSlip()
{
	val = document.frmVendor.hConsignId.value;	
	windowOpener("/GmSetBuildServlet?hAction=PicSlip&hConsignId="+val,"SetPic","resizable=yes,scrollbars=yes,top=150,left=150,width=670,height=500");
}


function fnPrintVer() 
{
windowOpener("/GmConsignSetServlet?hAction=PrintVersion&hId=consignId","Con","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnAgree()
{
windowOpener("/GmConsignSetServlet?hId=consignId&hAction=Ack&","Ack","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");

}

function fnPackslip()
{
windowOpener("/GmConsignSetServlet?hAction=PackVersion&hId=consignId","Con","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnVoidCsg()
{
		document.frmVendor.action ="/GmCommonCancelServlet";
		document.frmVendor.hAction.value = "Load";
		document.frmVendor.submit();	
}

var prevtr = 0;

function chgTRBgColor(val,BgColor) {
   var obj1 = eval("document.all.tr"+prevtr);
   if(typeof obj1!="undefined"){
   	obj1.style.background = "";
   }
   var obj = eval("document.all.tr"+val);
   prevtr = val;
   if(typeof obj!="undefined"){
   	obj.style.background = BgColor;
   }
}

function fnBlankLine(varCounter,value)
{
	if(value == 'TBE'){
		obj = eval("document.frmVendor.Txt_CNum"+varCounter);
		obj.value = '';
	}
	chgTRBgColor(varCounter,'#AACCE8');
}

function fnBlankTBE(varCounter,value)
{
	value = TRIM(value);
		if(value == ''){
				obj = eval("document.frmVendor.Txt_CNum"+varCounter);
				obj.value = 'TBE';
			}
			
			chgTRBgColor(varCounter,'#AACCE8');
	}
	
function fnVRule()
{
	document.frmVendor.hAction.value ="Load";
	document.frmVendor.action ="/GmCommonCancelServlet";
	document.frmVendor.hCancelType.value="VINTX";
	document.frmVendor.submit();
}

function fnOpenTag(strPnum,strConId)
{
	windowOpener("/gmTag.do?strOpt=reload&pnum="+encodeURIComponent(strPnum)+"&refID="+strConId+"&refType=51000","PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=920,height=600");
}

function fnValidateAllControlNo(pnum,ctrlNum,qty,pnumMaterial,checkVeriFl){	
	var errparts = "";
	if(pnumMaterial == '100845' && qty > 1){ // 100845[Tissue Part]
		if(ctrlNum == ''){
			errparts += pnum + "  " + ctrlNum;
		}else{
			errparts += pnum + " --- " + ctrlNum;
		}		
	}
	// For validating the Duplicate COntrol Number on Submit
	if(pnumMaterial == '100845' && qty == 1 && ctrlNum != ''){ // 100845[Tissue Part]
		if(tissueControls.indexOf("|" + pnum +ctrlNum + "|") == -1){
			tissueControls = tissueControls + "|" + pnum +ctrlNum + "|";
		}else{
			errparts += pnum + " --- " + ctrlNum;
		}
	}
	return errparts;
}