var gObj;
function fnOnPageLoad() {
	if (objGridData.value != '') {
			gObj = initGridData('dataGridDiv', objGridData);
	}
}

function initGridData(divRef, gridData) {
	var gObj = new dhtmlXGridObject(divRef);
	gObj = new dhtmlXGridObject("dataGridDiv");
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);
	gObj.enableTooltips("true,true,true,true,true,true,true,true,true");
	gObj.attachHeader(',#text_filter,#text_filter,#text_filter,#text_filter,#select_filter');
	gObj.init();
	gObj.enablePaging(true, 20, 5, "pagingArea", true, "infoArea");
	gObj.setPagingSkin("bricks");
	gObj.loadXMLString(gridData);
	return gObj;
}

function fnLoad(){
	var toEmailId = document.forms["frmEmailTrackRpt"]["toEmailId"].value; 
	var subject = document.forms["frmEmailTrackRpt"]["subject"].value;
	var fdate = document.frmEmailTrackRpt.fromDate;
	var tdate = document.frmEmailTrackRpt.toDate;
	
	var dateRange = dateDiff(document.frmEmailTrackRpt.fromDate.value,document.frmEmailTrackRpt.toDate.value,format);
	var sublen = subject.length;
	var alphaNum=/^[A-Za-z0-9_@.-]+$/;
	
    diff = dateDiff(fdate.value,tdate.value);
	if(toEmailId == '' && subject == '' && fdate.value == '' && tdate.value == ''){
	    Error_Details(message[5260]);
	}
	if(toEmailId != "" &&(!alphaNum.test(toEmailId) || toEmailId.split('@').length > 2)){
        Error_Details(message[5262]);
    }	
	if(subject !='' && sublen>100){
    	Error_Details(message[5261]);
    }
	
	
	 if(fdate.value == "" && tdate.value != "")
	{
		fnValidateTxtFld('fromDate',lblFromDate);
	}
	else if(tdate.value == "" && fdate.value != "")
	{
		fnValidateTxtFld('toDate',lblToDate);
	}
	
	if(fdate.value !='' && tdate.value !=''){
		if(diff < 0)
	   	{
	   		Error_Details(message[5277]);						
	   	}
	else if(isNaN(dateRange) || dateRange > 30 || dateRange < 0){
		    Error_Details(message[5259]);
		   }
	}
	//Common Date Validation added
	if(fdate.value != ""){

		CommonDateValidation(fdate, format, Error_Details_Trans(message[10507],format));
	}
	if(tdate.value != ""){
		CommonDateValidation(tdate, format, Error_Details_Trans(message[10508],format));
	}
	
	
	if (ErrorCount > 0){
	Error_Show();
    Error_Clear();
	return false;
	}
    fnStartProgress();
	document.frmEmailTrackRpt.strOpt.value = 'Load';	
	document.frmEmailTrackRpt.submit();
}

function fnViewEmail(emailLogId)
{	
		windowOpener('/gmEmailTrackRpt.do?method=fetchEmailMessage&emailLogId='+emailLogId,"Emailcontent","resizable=yes,scrollbars=yes,top=250,left=250,width=965,height=450");	
}
		
function fnExportExcel() {
	gObj.toExcel('/phpapp/excel/generate.php');
}