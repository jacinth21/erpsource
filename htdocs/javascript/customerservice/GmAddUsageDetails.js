
this.keyArray = new Array(); 
this.valArray = new Array();
this.put = put; 
this.get = get; 
this.findIt = findIt;


function put(key,val) 
{ 
    var elementIndex = this.findIt(key); 
    if( elementIndex == (-1) ) 
    { 
        this.keyArray.push( key ); 
        this.valArray.push( val ); 
    } 
    else 
    { 
        this.valArray[ elementIndex ] = val; 
    } 
} 
 
function get(key) 
{ 
    var result = null; 
    var elementIndex = this.findIt( key ); 
    if( elementIndex != (-1) ) 
    {    
        result = this.valArray[ elementIndex ];
    } 
    else {
    	result = "";
    }
    return result; 
}
function findIt(key) 
{ 
    var result = (-1); 
 
    for( var i = 0; i < this.keyArray.length; i++ ) 
    { 
        if( this.keyArray[ i ] == key ) 
        { 
            result = i; 
            break; 
        } 
    } 
    return result; 
} 

//end Array Search

function fnClearCart() {
	var varRowCnt = document.frmOrderDetails.hRowCnt.value;
	for (val = 0; val < varRowCnt; val++) {
		obj = eval("document.frmOrderDetails.Txt_Part" + val);
		obj.value = '';
		obj = eval("document.frmOrderDetails.Txt_Qty" + val);
		obj.value = '0';
		obj = eval("document.all.Lbl_Desc" + val);
		obj.innerHTML = "&nbsp;";
		if (sessDeptID == 'S') {
			obj = eval("document.all.Lbl_PriceEA" + val);
			obj.innerHTML = "0.00";
		} else {
			obj = eval("document.frmOrderDetails.Txt_PriceEA" + val);
			obj.value = '0.00'
		}
		obj = eval("document.all.Lbl_ExtPrice" + val);
		var extpric = formatCurrency(0.00,sessCurrSym);
		obj.innerHTML = extpric;
		obj = eval("document.frmOrderDetails.usage" + val);
		obj.value = '2560';
		obj = eval("document.frmOrderDetails.type" + val);
		obj.value = '50300';
	}
	fnCalculateTotal();
}

function fnDecresQty(val, obj) {
	objPart = eval("document.frmOrderDetails.Txt_Part" + val);
	var objCurrQty = eval("document.frmOrderDetails.Txt_Qty" + val);
	if (objPart.value == '') {
		var obj = eval("document.all.Lbl_Desc" + val);
		obj.innerHTML = '<span class=RightTextRed>Please Enter the Part Number</span>';
	} else {
		var currQty = objCurrQty.value;
		if (currQty > 0) {
			eval("document.frmOrderDetails.Txt_Qty" + val).value = currQty - 1;
		} else {
			eval("document.frmOrderDetails.Txt_Qty" + val).value = 0;
		}
		validate(val, obj);
	}
}
function fnIncrQty(val, obj) {
	objPart = eval("document.frmOrderDetails.Txt_Part" + val);

	if (objPart.value == '') {
		var obj = eval("document.all.Lbl_Desc" + val);
		obj.innerHTML = '<span class=RightTextRed>Please Enter the Part Number</span>';
	} else{
		var objCurrQty = eval("document.frmOrderDetails.Txt_Qty" + val);
		var currQty = objCurrQty.value;
		if(currQty == '' || currQty < 0){
		eval("document.frmOrderDetails.Txt_Qty" + val).value = 0;
		}else{
		eval("document.frmOrderDetails.Txt_Qty" + val).value = eval(currQty) + 1;
		}
		validate(val, obj);	
	}
}
function fnSaveUsage() {

	//fnValidateDropDn('surgeryNo', 'Surgery No');

	var varRowCnt = document.frmOrderDetails.hRowCnt.value;
	var k;
	var obj;
	var str;
	var token = '^';
	var parttype = '';
	var cnum = '';
	var status = true;
	var inputstr = '';
    var intCount = 0;
    var orderId = '';
    var pricCount = 0;
    var parts = '';
    var blPrice = true;
	for (k = 0; k < varRowCnt; k++) {
		obj = eval("document.frmOrderDetails.Txt_Part" + k);
		if (obj.value != '') {
			str = obj.value + token;
			objQty = eval("document.frmOrderDetails.Txt_Qty" + k);
			str = str + objQty.value + token;
			if (sessDeptID == 'S') {
				objPrice = eval("document.frmOrderDetails.Txt_PriceEA" + k);
				if(objPrice == undefined || objPrice == null){
					objPrice = eval(document.getElementById('Lbl_PriceEA'+k));
					str = str + RemoveComma(TRIM(objPrice.innerHTML.replace(/\&nbsp\;/g,' '))) + token;
				}else{
					arr = get(obj.value);
					if(arr !=''){
						arrobj = arr.split("^");
						if(arrobj.length == 1)
							blPrice = false;
					}
					if(RemoveComma(objPrice.value) <= 0 && blPrice){
						pricCount++;
						parts = parts+obj.value+',';
					}
					blPrice = true;
					str = str + RemoveComma(objPrice.value) + token;
				}
			} else {
				objPrice = eval("document.frmOrderDetails.Txt_PriceEA" + k);
				str = str + RemoveComma(objPrice.value) + token;
			}
			objUsage = document.getElementById("usage" + k);//
			//The Existing I/P String is Changed for MDO-109.Additional Parameter is added for Part Type.
			//str = str + objUsage.value +token+parttype+ '|';	
			str = str + objUsage.value +token+parttype;
			objtype = eval("document.frmOrderDetails.type" + k);
			str = str + objtype.value + '|';
			if(objQty.value <= 0){
				intCount++;
				var obj = eval("document.all.Lbl_Desc" + k);
				obj.innerHTML = message_operations[281];
			}
			inputstr = inputstr + str;
		}
	}
	if(intCount > 0){
		
		return false;
		
	}
	var text_PO = TRIM(document.frmOrderDetails.custPO.value);
	fnValidatePOSpace (text_PO);
	if (inputstr == '') {
		Error_Details(message_operations[241]);
	}
	if(pricCount > 0){
		Error_Details(Error_Details_Trans(message_operations[242],parts));
	}
	//code change for PMT-39389
	if(vNPIMandatoryFl == 'Y'){
		if (document.getElementById("searchTxt_Npi") != undefined && document.getElementById("searchTxt_Surgeon") != undefined){
			var vnpiId = document.getElementById("searchTxt_Npi").value;
			var vsurgeonname = document.getElementById("searchTxt_Surgeon").value;
			if(vnpiId == '' || vsurgeonname == ''){
				Error_Details('Please provide NPI details to proceed further');
			}
		}
	}
	//Code changes for PC-4892-EGPS in Edit order and Add EGPS in case booking
	 if(egpsFl != ''){
		 if(document.frmOrderDetails.cboegpsusage != undefined){
		     var egpsVal = document.frmOrderDetails.cboegpsusage.value;
		      document.frmOrderDetails.cboegpsusage.value = egpsVal;
		      if(egpsFl == 'Y' && egpsVal == '0'){
			      Error_Details("Please provide <b>Capital Equp. Used</b> details to proceed further");	
		      }
		 }
	 }
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	var retval = checkMaxLength(document.getElementById("comments").value,4000, "Comments");
	//alert(retval);
	if(retval == false){
		return false;
	}
	if (strOpt == 'edit') {
		document.frmOrderDetails.haction.value = "EDITDO";
		document.frmOrderDetails.orderID.value = orderid;
	} else if(strOpt != 'edit' && parentOrdId != ''){
		document.frmOrderDetails.haction.value = "NEWDO";
		document.frmOrderDetails.orderID.value = orderid;
	}else{
		document.frmOrderDetails.haction.value = "NEWDO";
		orderId = document.getElementById("ordId").innerHTML.replace(/\&nbsp\;/g,' ');
		var surgeryno = document.frmOrderDetails.surgeryNo.options[document.frmOrderDetails.surgeryNo.selectedIndex].text;
		orderid = orderId+surgeryno;
		document.frmOrderDetails.orderID.value = orderid;
	}
	var total = document.frmOrderDetails.hTotal.value;
	if(total == ''){
		total = document.frmOrderDetails.totalPrice.value;
	}
	document.frmOrderDetails.strOpt.value = "save";
	document.frmOrderDetails.total.value = total;
	document.frmOrderDetails.accountID.value = accID;
	if(orderMode == 'IPAD'){
		var id = document.getElementById("Cbo_RepId").value;
		var surgDtObj = document.frmOrderDetails.surgeryDt;
		if(id == ''){
			Error_Details(message_operations[866]);
		}
		if(surgDtObj.value == ''){
			Error_Details(message_operations[821]);
		}
		CommonDateValidation(surgDtObj, strApplnDateFmt,Error_Details_Trans(message_operations[754],strApplnDateFmt));
		if(ErrorCount > 0){
			Error_Show();
			Error_Clear();
			return false;
		}else{
			document.frmOrderDetails.repID.value = id;
		}		
	}else{
		document.frmOrderDetails.repID.value = repId;
	}
	document.frmOrderDetails.inputStr.value = inputstr;
	responseText = document.getElementById("ErrMessage").innerHTML;
	if(TRIM(responseText) == ""){
	document.frmOrderDetails.submit();
	}
}
function fnValidateErrMessage(loader){
	responseText =loader.xmlDoc.responseText;
	if(TRIM(responseText) != ""){
		document.frmOrderDetails.surgeryNo.disabled = false;
		}
	else{
		document.frmOrderDetails.surgeryNo.disabled = true;
	}
		
	document.getElementById("ErrMessage").innerHTML = responseText;
}
function fnAddRow(id) {
    var preSelect ='';
	var dropDownVal ='';
	var preSelectType ='';
	var dropDownValType ='';
	for(var i =0; i<usageLen; i++){
		usageArr = eval("UsageArr"+i);
		usageArrObj = usageArr.split(",");
		if(usageArrObj[0] == usageSelect){
			preSelect ='selected';
		}
		dropDownVal=dropDownVal+'<option  '+preSelect+' value='+usageArrObj[0]+'>'+usageArrObj[1]+'</option>';
		preSelect = '';
	}
 	for(var i =0; i<alSetTypeLen; i++){
		usageArrType = eval("TypeArr"+i);
		usageArrObjType = usageArrType.split(",");
		if(usageArrObjType[0] == setTypeSelect){
			preSelectType ='selected';
		}
	    dropDownValType=dropDownValType+'<option  '+preSelectType+' value='+usageArrObjType[0]+'>'+usageArrObjType[1]+'</option>';
		preSelectType = '';
	}
	var rowno = count;
	var TBODY = document.getElementById(id).getElementsByTagName("TBODY")[0];
	var row = document.createElement("TR");

	var td0 = document.createElement("TD");
	td0.innerHTML = count + 1;

	var td1 = document.createElement("TD");
	td1.innerHTML = '<a href=javascript:fnRemoveItem('
			+ rowno
			+ ');><img border=0 Alt=Remove from cart valign=left src=/images/btn_remove.gif height=20 width=18></a>';

	var td2 = document.createElement("TD");
	td2.innerHTML = '<input type=text size=15  value=\'\' class=InputArea name=Txt_Part'
			+ rowno
			+ ' onBlur=fnSetPartSearch('
			+ rowno
			+ ',this); onFocus=changeBgColor(this,\'#AACCE8\'); onchange= fnUpperCase(this);>';
	td2.align = "Center";

	var td3 = document.createElement("TD");
	td3.innerHTML ='<a href=javascript:fnDecresQty('
			+ rowno
			+ ',this); tabindex=-1><img '
			+ rowno
			+ ' align="top" border=0 src=/images/minus.gif height=20 width=18></a>&nbsp;&nbsp;<input type=text name=Txt_Qty'
			+ rowno
			+ ' value=0 maxlength=3 size=2 align="middle" onBlur=fnPartSet('
			+ rowno
			+ ',this);>&nbsp;<a href=javascript:fnIncrQty('
			+ rowno
			+ ',this); tabindex=-1> <img  border=0 align="top" src=/images/plus.gif height=20 width=18></a>'
			+ '<input type=hidden value=0.00 name=hPriceEA'
			+ rowno
			+ ' />';	
	td3.align = "center";

	var td4 = document.createElement("TD");
	td4.id = "Lbl_Desc" + rowno;
	td4.align = "left";
	td4.innerHTML = "&nbsp;";

	var td5 = document.createElement("TD");
	if (sessDeptID == 'S') {
		td5.id = "Lbl_PriceEA" + rowno;
		td5.innerHTML = '0.00' + "&nbsp;";
		td5.align = "Right";
	} else {
		td5.innerHTML = '<input type=text size=8  style=\'text-align: right\';  value=0.00 class=InputArea name=Txt_PriceEA'
				+ rowno
				+ ' onBlur=fnCalExtPrice(this,'
				+ rowno
				+ '); onFocus=changeBgColor(this,\'#AACCE8\');>';
		td5.align = "center";
	}
	

	var td6 = document.createElement("TD");
	td6.id = "Lbl_ExtPrice" + rowno;
	td6.align = "Right";
	var extpric = formatCurrency('0.00',sessCurrSym);
	td6.innerHTML = extpric;

	var td7 = document.createElement("TD");
	td7.innerHTML = '<select  name=usage' 
			+ rowno +' id=usage'+rowno
			+ ' class=RightText  onFocus=changeBgColor(this,\'#AACCE8\'); onBlur=changeBgColor(this,\'#ffffff\'); onchange="fnUsage(this,'+rowno+');">'+dropDownVal;
	
	var td8 = document.createElement("TD");
	td8.innerHTML = '<select  name=type' 
			+ rowno +' id=type'+rowno
			+ ' class=RightText  onFocus=changeBgColor(this,\'#AACCE8\'); onBlur=changeBgColor(this,\'#ffffff\'); onchange="fnUsage(this,'+rowno+');">'+dropDownValType;

	row.height = "30";
	row.appendChild(td0);
	row.appendChild(td1);
	row.appendChild(td2);
	row.appendChild(td3);
	row.appendChild(td4);
	row.appendChild(td8);
	row.appendChild(td5);
	row.appendChild(td6);
	row.appendChild(td7);

	TBODY.appendChild(row);
	count++;
	document.frmOrderDetails.hRowCnt.value = count;
	
}
var objPartCell;
var varPartNum;
function fnSearchPart() {
	if(objPartCell == null || objPartCell == undefined){
	 varPartNum =""	
	}else{
	varPartNum = objPartCell.value;
	}
	var ajaxUrl = fnAjaxURLAppendCmpInfo("/GmSearchServlet?hAction=LoadPartSearch&Txt_PartNum="+encodeURIComponent(varPartNum)+"&hExcSubComp=Y");
	dhxWins = new dhtmlXWindows();
    dhxWins.enableAutoViewport(true);
    w1 = dhxWins.createWindow("w1", 30, 40, 750, 720);
    w1.setText(message_operations[243]);
    w1.attachURL(ajaxUrl);
}
function fnRemoveItem(val) {
	obj = eval("document.frmOrderDetails.Txt_Part" + val);
	// window.parent.Reload(obj.value, '-1', '' );
	obj.value = '';
	obj = eval("document.frmOrderDetails.Txt_Qty" + val);
	obj.value = '0';
	obj = eval("document.all.Lbl_Desc" + val);
	obj.innerHTML = "&nbsp;";
	if (sessDeptID == 'S') {
		obj = eval("document.all.Lbl_PriceEA" + val);
		obj.innerHTML = "0.00";
	} else {
		obj = eval("document.frmOrderDetails.Txt_PriceEA" + val);
		obj.value = "0.00";
	}
	obj = eval("document.all.Lbl_ExtPrice" + val);
	var extpric = formatCurrency(0.00,sessCurrSym);
	obj.innerHTML = extpric;
	obj = eval("document.frmOrderDetails.usage" + val);
	obj.value = '2560';
	obj = eval("document.frmOrderDetails.type" + val);
	obj.value = '50300';
	fnCalculateTotal();
}

function fnChangeDoId() {
	if (strOpt != 'edit') {
	var surgeryno = document.frmOrderDetails.surgeryNo.options[document.frmOrderDetails.surgeryNo.selectedIndex].text;
	var surgeryDt = document.frmOrderDetails.surgeryDt.value;
	var month = surgeryDt.split("/")[0];
	var day = surgeryDt.split("/")[1];
	var year = surgeryDt.split("/")[2];
	year = year.substring(2, year.length);
	if (repId.length == 2)
	{
		repId = '0'+ repId;
	}
	else if (repId.length == 1)
	{
		repId = '00'+ repId;
	}
	var orderID = repId + '-' + year + month + day + '-'; //+ surgeryno;
	var ajaxUrl = fnAjaxURLAppendCmpInfo('gmOrderDetails.do?method=validateDOAjax&orderID='+orderID+surgeryno+'&ramdomId='+Math.random());
	dhtmlxAjax.get(ajaxUrl,fnValidateErrMessage);
	document.getElementById("ordId").innerHTML = orderID;
	document.getElementById("hordId").innerHTML = "<input type='hidden' name='Txt_OrdId' id='Txt_OrdId' value='"+orderID+surgeryno+"'/>";
	}
}
function fnLoad() {
	if (strOpt == 'edit' || parentOrdId != '') {
		//document.frmOrderDetails.surgeryNo.disabled = true;
		//document.frmOrderDetails.surgeryDt.disabled = true;
	} 
	else 
	{
		var caseId =  document.frmOrderDetails.caseID.value;
		var surNoTemp = caseId.substring(caseId.lastIndexOf("-")+1);
		var surNo = document.frmOrderDetails.surgeryNo;
		for(var i=0; i<surNo.options.length; i++) 
		{
		  if ( surNo.options[i].text == surNoTemp ) 
		  {
			  surNo.selectedIndex = i;
			  break;
		  }

		}
		fnChangeDoId();
		/*var surgeryno = document.frmOrderDetails.surgeryNo.options[document.frmOrderDetails.surgeryNo.selectedIndex].text;
		var orderID = orderid + surgeryno;
		document.getElementById("ordId").innerHTML = orderID;*/
	}
}
function fnSetPartSearch(count, obj) {
	
	changeBgColor(obj, '#ffffff');
	objPartQty = eval("document.frmOrderDetails.Txt_Qty" + count);
	objPartCell = eval("document.frmOrderDetails.Txt_Part" + count);
	if(objPartQty.value != '' && objPartQty.value != '0'){
		validate(count, obj);
	}	
}
function fnClearValue(index){
	
	objPartCell = eval("document.frmOrderDetails.Txt_Part" + index);
	objPartQty = eval("document.frmOrderDetails.Txt_Qty" + index);
	if (sessDeptID == 'S') {
	objPriceEA = document.getElementById("Lbl_PriceEA" +index);
	objPriceEA.innerHTML ='0.00';
	}else{
	objPriceEA = eval("document.frmOrderDetails.Txt_PriceEA"+index);
	objPriceEA.value = '0.00';	
	}
	objExtPrice = eval("document.all.Lbl_ExtPrice" +index);
	objExtPrice.innerHTML = formatCurrency('0.00',sessCurrSym);
	objUsage = eval("document.frmOrderDetails.usage" + index);
	objUsage.value = '2560';
	objType = eval("document.frmOrderDetails.type" + index); 
	objType.value = '50300'; 
}

function fnPartSet(count,obj){
	changeBgColor(obj, '#ffffff');
	validate(count, obj);
}
function validate(cnt, obj) {
	var pnum = '';
	var qty = '';
	trcnt = cnt;
	var pnumobj = eval("document.all.Txt_Part" + cnt);
	var qtyobj = eval("document.all.Txt_Qty" + cnt);
	var pprice =  eval("document.all.Txt_PriceEA" + cnt);
	if (gpoid == '')
	{
		gpoid = '0';
	}
	var pdesc = '';
   //Array Search
	arr = get(pnumobj.value);
	if(arr !=''){
			arrobj = arr.split("^");
			if(arrobj.length > 1){
			objQty = eval("document.all.Txt_Qty" + cnt);
			pprice = arrobj[1];
			pdesc = arrobj[0];
			amount = pprice * objQty.value;
			setMessage(pdesc, pprice, amount);
			}
	}
	
	// search end		
		if(pnumobj.value != '' && qtyobj.value != '' && pdesc == ''){
		  if(pprice != undefined){
			if(pprice.value != '0.00'){
				totalprice = qtyobj.value * RemoveComma(pprice.value);	
				document.getElementById("Lbl_ExtPrice" + cnt).innerHTML=formatCurrency(totalprice, sessCurrSym);
				fnCalculateTotal();
			}else{
				pnum = pnumobj.value;
				qty = qtyobj.value;
				tempPnum = pnum;
				var url = fnAjaxURLAppendCmpInfo("/GmCartAjaxServlet?pnum=" + encodeURIComponent(pnum)
					+ "&qty=" + encodeURIComponent(qty) + "&acc="
					+ encodeURIComponent(accID) + "&rep="
					+ encodeURIComponent(repId) + "&gpoid="
					+ encodeURIComponent(gpoid) + "&regexp=NO");

				if (typeof XMLHttpRequest != "undefined") {
					req = new XMLHttpRequest();
				} else if (window.ActiveXObject) {
					req = new ActiveXObject("Microsoft.XMLHTTP");
				}
				req.open("GET", url, true);
				req.onreadystatechange = callback;
				req.send(null);
			}
		}
	 }
 }
function callback() {
	if (req.readyState == 4) {
		if (req.status == 200) {
			var xmlDoc = req.responseXML;
			parseMessage(xmlDoc);
		} else {
			setErrMessage(message_operations[244]);
			fnClearValue(trcnt);
		}
	}
}
function parseMessage(xmlDoc) {
	var strarr = 'pdesc,price,lprice';
	var pnum = xmlDoc.getElementsByTagName("pnum");

	if (pnum.length == 0) {
		setErrMessage(message_operations[244]);
		fnClearValue(trcnt);
		return;
	}

	var strobj = strarr.split(',');

	var strlen = strobj.length;
	var strresult = '';

	for ( var i = 0; i < strlen; i++) {
		var str = strobj[i];
		for ( var x = 0; x < pnum[0].childNodes.length; x++) {
			xmlstr = pnum[0].childNodes[x].tagName;
			if (xmlstr == str) {
				strresult = strresult + '^' + parseXmlNode(pnum[0].childNodes[x].firstChild);
				break;
			}
		}
	}

	strresult = strresult.substr(1, strresult.length);
	var strobj = strresult.split('^');

	var pdesc = strobj[0];
	var price = strobj[1];
	var lprice = strobj[2];
	obj = eval("document.all.Txt_Qty" + trcnt);
	qty = parseInt(obj.value);

	var amount = qty * price;

	if (pdesc == null) {
		setErrMessage(message_operations[244]);
		fnClearValue(trcnt);
		return;
	}

	setMessage(pdesc, price, amount ,qty);

	// window.parent.Reload(tempPnum, '1', ordertype);

}
function setMessage(pdesc, price, amount ,qty) {
	var obj = eval("document.all.Lbl_Desc" + trcnt);
	obj.innerHTML = "&nbsp;" + pdesc;
	if (sessDeptID == 'S') {
		objUsage = document.getElementById("usage" + trcnt);
		if(objUsage.value == '2563'){
			//price = eval("document.all.hPriceEA"+trcnt).value;
			obj = eval("document.frmOrderDetails.Txt_PriceEA" + trcnt);
			obj.value = formatNumber(price);
		}else{
			obj = eval("document.all.Lbl_PriceEA" + trcnt);
			obj.innerHTML = formatNumber(price) + "&nbsp;";
		}
		
	} else {
		obj = eval("document.frmOrderDetails.Txt_PriceEA" + trcnt);
		obj.value = formatNumber(price);
	}
	obj = eval("document.all.hPriceEA"+trcnt);
	obj.value = formatNumber(price);
	obj = eval("document.all.Lbl_ExtPrice" + trcnt);
	obj.innerHTML = formatCurrency(qty*price,sessCurrSym);
	fnCalculateTotal();
}
function setErrMessage(msg) {
	var obj = eval("document.all.Lbl_Desc" + trcnt);
	obj.innerHTML = msg;

	obj = eval("document.all.Txt_Qty" + trcnt);
	obj.value = '0';

	obj = eval("document.all.Txt_Part" + trcnt);
	obj.value = '';
	obj.focus();
}
function fnCalculateTotal() {
	var varRowCnt = document.frmOrderDetails.hRowCnt.value;
	var k;
	var obj;
	var qty = 0;
	var price = 0.0;
	var total = 0.0;

	for (k = 0; k <= varRowCnt; k++) {
		obj = eval("document.frmOrderDetails.Txt_Part" + k);
		if (obj) {
			if (obj.value != '') {

				obj = eval("document.frmOrderDetails.Txt_Qty" + k);
				qty = parseInt(obj.value);
				if (sessDeptID == 'S') {
					extprice = document.getElementById("Lbl_PriceEA" + k).innerHTML;
					if(isNaN(TRIM(RemoveComma(extprice.replace(/\&nbsp\;/g,' '))))){
						extprice = eval("document.frmOrderDetails.Txt_PriceEA" + k).value;
					}
				} else {
					extprice = eval("document.frmOrderDetails.Txt_PriceEA" + k).value;
				}
				price = parseFloat(RemoveComma(extprice));
				total = total + (price * qty);
			}
		}
	}
	document.frmOrderDetails.hTotal.value = total;
	document.all.Lbl_Total.innerHTML = '<b>' + formatCurrency(total, sessCurrSym)
			+ '</b>&nbsp;';
}

function fnShowFilters(val) {
	var obj = eval("document.all." + val);
	var obj1 = eval("document.all." + val + "img");
	if (obj.style.display == 'none') {
		obj.style.display = 'block';
		if (obj1) {
			obj1.src = '/images/minus.gif';
		}
	} else {
		obj.style.display = 'none';
		if (obj1) {
			obj1.src = '/images/plus.gif';
		}
	}
}
function fnCalExtPrice(obj, cnt) {
	var qty = 0;
	var price = 0.0;
	var pobj = eval("document.frmOrderDetails.Txt_Qty" + cnt);
	qty = parseInt(pobj.value);

	if (!isNaN(qty)) {
		price = parseFloat(RemoveComma(obj.value));
		var eprice = price * qty;
		var pobj = eval("document.all.Lbl_ExtPrice" + cnt);
		pobj.innerHTML = formatCurrency(eprice, sessCurrSym) + "&nbsp;";
		obj.value = formatNumber(price);
		fnCalculateTotal();
	}
	changeBgColor(obj, '#ffffff');
}

function fnCancel(){
	document.frmOrderDetails.action = "/GmEditOrderServlet?hMode=PrintPrice&hParantForm=CASE&hOrdId="+orderid;
	document.frmOrderDetails.submit();
}

function fnBackDOActions(){
	document.frmOrderDetails.action = "/gmOrderDetails.do?method=loadDOActions&hOrdId="+orderid;
	document.frmOrderDetails.submit();
}
function fnUsage(obj , id){
	if (sessDeptID == 'S') {
		/*var arrCount =0;
		partobj = eval("document.frmOrderDetails.Txt_Part" + id);
		arrFreeParts = get(partobj.value);
		if(arrFreeParts !=''){
			arrobjParts = arrFreeParts.split("^");
			arrCount = arrobjParts.length;
		}*/
	if(obj.value == '2563'){
		var priceObj = eval(document.getElementById('Lbl_PriceEA'+id));
		price = RemoveComma(priceObj.innerHTML.replace(/\&nbsp\;/g,' '));
		priceObj.innerHTML ='';
		NewOtherTagHtml = '<input type=text size=8  style=\'text-align: right\';  value= '+price+' class=InputArea name=Txt_PriceEA'
			+ id
			+ ' onBlur=fnCalExtPrice(this,'
			+ id
			+ ');'
			+ ' onkeypress="return fnPriceOnly(event);" '
			+ ' onFocus=changeBgColor(this,\'#AACCE8\');>';
		priceObj.insertAdjacentHTML("beforeEnd",NewOtherTagHtml);
		
	}else{
		var priceObj = eval(document.getElementById('Lbl_PriceEA'+id));
		var hpriceVal = eval("document.all.hPriceEA"+id).value;
		priceObj.innerHTML ='';
		priceObj.innerHTML = hpriceVal+ "&nbsp;";
		var qtyobj = eval("document.frmOrderDetails.Txt_Qty" + id);
		var pobj = eval("document.all.Lbl_ExtPrice" + id);
		pobj.innerHTML = formatCurrency(RemoveComma(hpriceVal)*qtyobj.value,sessCurrSym) + "&nbsp;";
		fnCalculateTotal();
	}
	}
}
function fnUpperCase(obj){
	obj.value = obj.value.toUpperCase();
}

function fnCaseActionBack()
{  
	document.frmOrderDetails.action = "gmCasePost.do?strOpt=CaseAction&caseInfoID=" + caseinfoid;
	document.frmOrderDetails.submit();
}
//validate the space - Customer PO
function fnValidatePOSpace(CustPO) {
	var regexp = /\s/g;
	if (regexp.test(CustPO)) {
		Error_Details(message_operations[245]);
	}
}
function fnSetNPINum(obj){
	var surgeonnm = document.getElementById("Txt_Surgeon").value;
	document.getElementById("searchTxt_Npi").value = surgeonnm;
	fnSaveParentRow('');
}

function fnSetSurgeonNm(obj){
	var npiId = document.getElementById("Txt_Npi").value;
	document.getElementById("searchTxt_Surgeon").value = npiId;
	fnSaveParentRow('');
}

//While tabout from NPI #/Surgeon name field
function fnAutoFocus(obj){
	// After Tabout from NPI#, it should focus on Surgeon name
	// Aftet tabout from Surgeon name, the value should be saved
	var npiId = document.getElementById("searchTxt_Npi").value;
	var surgeonnm = document.getElementById("searchTxt_Surgeon").value;
	var acceptNum;
	var errorFl;
	// NPI field should accept only numbers and surgeon field should accept only characters with dot
	if(npiId != ''){
		acceptNum = /[^0-9]/g;
        errorFl = acceptNum.test(npiId);
        if (errorFl) {  /*Accept only Digits in NPI# field*/
        	Error_Details("Please provide NPI# in digits");
        }
	}
	if(surgeonnm != ''){
		acceptNum = /^[a-zA-Z\.\, ]*$/;
        errorFl = acceptNum.test(surgeonnm);
        if (!errorFl) {  /*Accept only Digits in NPI# field*/
        	Error_Details("Please provide surgeon name in letters");
        }
	}
	
	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}
	if(obj.id == 'searchTxt_Npi'){
		document.getElementById("searchTxt_Surgeon").focus();
	}else if(npiId != '' || surgeonnm!= ''){
		fnSaveParentRow('');
	}
}

//check for valid numeric strings
function fnIsNumeric(evt)  
{
	var iKeyCode = (evt.which) ? evt.which : evt.keyCode
    if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
        return false;
    return true;
}

//function to open add tag screen in popup PMT-24435
function fnAddTags(){
	var txtOrderIdObj = '';
	//PC-2151 for PBUG-4539
	// Passing Btn_AddTags since its passing the value from frontend and this function is working for both add and edit usage screens
	if(document.all.Btn_AddTags!= undefined) {
		txtOrderIdObj = document.all.Btn_AddTags.value;
	}
	//commenting the below lines since Txt_OrdId is not passed from jsp i.e add tags popup window will work for add usage details screen 
//	if(document.all.Txt_OrdId.value!= undefined) {
//		txtOrderIdObj = document.all.Txt_OrdId.value;
//	}
	 if(txtOrderIdObj!=null && accID!=null){
		 windowOpener('/gmOrderTagRecordAction.do?method=fetchTagRecordInfo&tagRefId='+txtOrderIdObj+'&accountId='+accID,"AddTagRecordInfo","resizable=yes,scrollbars=yes,top=250,left=300,width=830,height=500");
	    } 
}

//function to validate rep id
function fnValidateRep(obj){
	var repID = document.getElementById("Cbo_RepId").value;
	var repName = document.getElementById("searchCbo_RepId").value;
	if(repID != undefined && repName != undefined && repID != ''){
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmOrderDetails.do?method=validateRepIdAjax&accountID='+accID+
				'&repID='+repID+'&ramdomId='+Math.random());
		dhtmlxAjax.get(ajaxUrl,function(loader)
		{
			var response = loader.xmlDoc.responseText;
			if(response == null || response ==''){
				fnLoadRepID(repID,repName);
			}else{
				Error_Details(response);
				Error_Show();
				Error_Clear();
				return false;
			}
		});
	}
}
//function is used to set the repid based on rep name form redis
function fnLoadRepID(id,repName){
	document.frmOrderDetails.repID.value = id;
	document.all.strRepId.innerHTML = '&nbsp;'+id;
}
