/**
 * 
 */
var part_rowId = '';
var partDes_rowId = '';
var miss_rowId = '';
var recon_rowId = '';
var sold_rowId = '';
var consig_rowId = '';
var write_rowId = '';
var repack_rowId = '';
var ih_rowId = '';
var price_rowId = '';
var arrClientErr = new Array('Bad Request - due to malformed syntax',
		'Unauthorized or Session Expired', 'Payment Required',
		'Request Forbidden', 'Request-URI not found', 'Method Not Allowed',
		'Content characteristics not acceptable',
		'Proxy Authentication Required', 'Request Timeout',
		'Request could not be completed due to a conflict',
		'Requested resource is no longer available at the server',
		'Server refuses to accept the request', 'Precondition Failed',
		'Request Entity Too Large', 'Request-URI Too Long',
		'Unsupported Media Type', 'Requested Range Not Satisfiable',
		'Expectation Failed');
var arrServerErr = new Array(
		'Internal Server Error',
		'Server does not support the functionality required to fulfill the request',
		'Bad Gateway', 'Service Unavailable', 'Gateway Timeout',
		'HTTP Version Not Supported');
var xmlReq;

function initGridData(divRef, gridData) {
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);
	gObj.init();
	gObj.loadXMLString(gridData);
	return gObj;
}
//Added for filters not visible PC-4909 
function initGridDatas(divRef, gridData) {
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);
		
	if (transType == '9112' || transType == '1006571') // IH Loaner Items
		gObj.attachHeader('#rspan,#rspan,#rspan,#rspan,Writeoff,Return to IH');
	else if (transType == '9115') // Product Loaner Items
		gObj.attachHeader('#rspan,#rspan,#rspan,#rspan,'+message[10074]+','+message[10075]+','+message[10076]+','+message[10077]+',#rspan');
	else
		// Product Loaner Sets/ In-House Loaner Set
		gObj.attachHeader('#rspan,#rspan,#rspan,#rspan,#rspan,'+message[10074]+','+message[10075]+','+message[10077]+','+message[10625]+',#rspan');	
	
	gObj.init();
	gObj.loadXMLString(gridData);
	return gObj;
}
function fnOnPageLoad() {
	if (objReconData != '') {
		reconGridObj = initGridDatas('div_rec', objReconData);
		reconGridObj.enableTooltips("true,true,true,true,true,true,true,true");
	}

	part_rowId = reconGridObj.getColIndexById("part");
	partDes_rowId = reconGridObj.getColIndexById("part_des");
	miss_rowId = reconGridObj.getColIndexById("qty_missing");
	recon_rowId = reconGridObj.getColIndexById("qty_recon");
	sold_rowId = reconGridObj.getColIndexById("sold");
	consig_rowId = reconGridObj.getColIndexById("consigned");
	write_rowId = reconGridObj.getColIndexById("writeoff");
	repack_rowId = reconGridObj.getColIndexById("return_repack");
	ih_rowId = reconGridObj.getColIndexById("return_ih");
	price_rowId = reconGridObj.getColIndexById("price");

	reconGridObj.forEachRow(function(rowId) {
		reconiled_qty = '';
		qty_miss = reconGridObj.cellById(rowId, miss_rowId).getValue();
		qty_recon = reconGridObj.cellById(rowId, recon_rowId).getValue();

		reconiled_qty = parseInt(qty_miss) - parseInt(qty_recon);
		if (reconiled_qty == '0') {
			if(eval("document.frmVendor.consign" + rowId))
				eval("document.frmVendor.consign" + rowId).disabled = true;
			if(eval("document.frmVendor.write" + rowId))
				eval("document.frmVendor.write" + rowId).disabled = true;
			if(eval("document.frmVendor.return" + rowId))
				eval("document.frmVendor.return" + rowId).disabled = true;
			if (eval("document.frmVendor.returnih" + rowId))
				eval("document.frmVendor.returnih" + rowId).disabled = true;
				if (eval("document.frmVendor.quarantine" + rowId)) //PC-622-LRQN -Add New column in reconciliation qty
				eval("document.frmVendor.quarantine" + rowId).disabled = true;
		}
	});

	var rowcount = reconGridObj.rowsCol.length;
	if (rowcount == '0') {
		reconGridObj
				.addRow(
						1,
						[
								"",
								"<font color='red'; size='2';>"+message[5672]+"</font>",
								"", "", "", "" ]);
	}

	if (objReconTransData != '') {
		reconTransGridObj = initGridData('div_ReconTrans', objReconTransData);
	}
	var rowCnt = reconTransGridObj.rowsCol.length;
	if (rowCnt == '0') {
		reconTransGridObj
				.addRow(
						1,
						[
								"",
								"",
								"",
								"",
								"<font color='red'; size='2';>"+message[5672]+"</font>",
								"", "", "" ]);
		reconTransGridObj.setColWidth(0, "60");
		reconTransGridObj.setColWidth(1, "60");
		reconTransGridObj.setColWidth(2, "100");
		reconTransGridObj.setColWidth(3, "80");
		reconTransGridObj.setColWidth(4, "230");
		reconTransGridObj.setColWidth(5, "66");
		reconTransGridObj.setColWidth(6, "80");
		reconTransGridObj.setColWidth(6, "80");
		reconTransGridObj.setColWidth(6, "100");
	}

	// expand and collapse the header based on the action.
	var my_action = document.frmVendor.hAction.value;
	if (my_action == 'LoanRecon') {
		fnShowFilters('tabUnRecon');
		document.getElementById("Cbo_action").value = '1006450';
	} else if (my_action == 'LoadUnRecon') {
		fnShowFilters('tabRecon');
		fnShowFilters('tabOrd');
		// document.getElementById("Btn_Load").disabled=true;
		document.getElementById("Cbo_action").value = '1006451';
	}
	document.getElementById("Cbo_Status").value = '1006442';

	document.getElementById("Cbo_Rep").value = document
			.getElementById("hRepid").value;
	document.getElementById("Cbo_Dist").value = document
			.getElementById("hDistid").value;
}

function fnSubmit() {

	Error_Clear();
	var pnumobj = '';
	var soldobj = '';
	var conobj = '';
	var priceobj = '';
	var conqty = 0;
	var soldqty = 0;
	var strErrorMore = '';
	var strErrorTotal = '';
	var strErrorStock = '';
	var strErrPartMsg = '';
	// var cnt = document.frmVendor.hCnt.value;
	var str = '';
	var retobj = '';
	var writeobj = '';
	var unreconobj = '';
	var repobj = '';
	var retqty = 0
	var writeqty = 0;
	var ihqty = 0;
	var unreconqty = 0;
	var repqty = 0;
	var retstr = '';
	var writestr = '';
	var total = 0;
	var retstr = '';
	var writestr = '';
	var ihstr = '';
	var constr = '';
	var cnum = 'TBE';

	var ordid = '';
	var part = '';
	var ordQty = '';
	var reason = '';
	var recpart = '';
	var strRecInp = '';
	var total_unRecon = 0;
	var ord_partNum = '';
	//PC-622-LRQN -Add New column in reconciliation qty
	var quarnQty =0;
	var quarnstr = '';

	fnValidateDropDn('Cbo_action',message[5507]);
	var actionType = document.frmVendor.Cbo_action.value;
	var trans_id = document.frmVendor.hConsignId.value;

	var totReconQty = 0;
	var errStrRecon = '';
	var blFlg = false;
	var gridrowsarr = '';
	var gridrows;
	var strErrNeg = '';
	var my_action = document.frmVendor.hAction.value;
	if (actionType == '1006450') { // Reconcile Process
		if (gridObj) {
			gridrows = gridObj.getCheckedRows(0);
			if (gridrows.length > 0)
				gridrowsarr = gridrows.toString().split(",");
		}
		reconGridObj.forEachRow(function(rowId) {
			total = 0;
			total_unRecon = 0;
			partNum = reconGridObj.cellById(rowId, part_rowId).getValue();
			qty_miss = reconGridObj.cellById(rowId, miss_rowId).getValue();
			qty_recon = reconGridObj.cellById(rowId, recon_rowId).getValue();
			// conqty = reconGridObj.cellById(rowId, consig_rowId).getValue();
			// retqty= reconGridObj.cellById(rowId, repack_rowId).getValue();
			// writeqty = reconGridObj.cellById(rowId, write_rowId).getValue();
			price = reconGridObj.cellById(rowId, price_rowId).getValue();
			if (transType == '9112' || transType == '1006571') {
				writeqty = eval("document.frmVendor.write" + rowId).value;
				ihqty = eval("document.frmVendor.returnih" + rowId).value;
				cnum = reconGridObj.getUserData(rowId,"hCnum");
			} else if (transType == '9115') {
				conqty = eval("document.frmVendor.consign" + rowId).value;
				retqty = eval("document.frmVendor.return" + rowId).value;
				writeqty = eval("document.frmVendor.write" + rowId).value;
				ihqty = eval("document.frmVendor.returnih" + rowId).value;
				cnum = reconGridObj.getUserData(rowId,"hCnum");
			} else {
				conqty = eval("document.frmVendor.consign" + rowId).value;
				retqty = eval("document.frmVendor.return" + rowId).value;
				writeqty = eval("document.frmVendor.write" + rowId).value;
				quarnQty = eval("document.frmVendor.quarantine" + rowId).value;
				cnum = 'NOC#';
			}
			// alert(' conqty '+conqty+' writeqty '+writeqty+' retqty '+retqty);
			// alert(' conqty '+conqty.value+' writeqty '+writeqty.value+'
			// retqty '+retqty.value);

			unreconqty = parseInt(qty_miss) - parseInt(qty_recon);
			total_unRecon = parseInt(total_unRecon) + parseInt(unreconqty);

			/*
			 * if (!isNaN(soldqty)) { total = parseInt(total) + soldqty; str =
			 * str + pnumobj.value + ','+ soldqty +
			 * ','+cnum+','+priceobj.value+'|'; }
			 */

			if (conqty != '') {
				NumberValidation(conqty,
						message[10053], 0);
				if(conqty != '0'){
					total = parseInt(total) + parseInt(conqty);
					constr = constr + partNum + ',' + conqty + ',' + cnum + ','+ price + '|';
				}
			}

			if (writeqty != '') {
				NumberValidation(writeqty,
						message[10054], 0);
				if(writeqty != '0'){
					total = parseInt(total) + parseInt(writeqty);
					writestr = writestr + partNum + ',' + writeqty + ',' + cnum + ',' + price + '|';
				}
			}
			//PC-622-LRQN -Add New column in reconciliation qty
			if (quarnQty != '') {
				NumberValidation(quarnQty,message[10055],0);
		        if(quarnQty != '0'){
					total = parseInt(total) + parseInt(quarnQty);
					quarnstr = quarnstr + partNum + ',' + quarnQty + ',' + cnum + ',' + price + '|';
		
				}
							
			}
			if (retqty != '') {
				NumberValidation(retqty,
						message[10055],
						0);
				if(retqty != '0'){
					total = parseInt(total) + parseInt(retqty);
					retstr = retstr + partNum + ',' + retqty + ',' + cnum + ',' + price + '|';
				}
				
			}
			
			if (ihqty != '') {
				NumberValidation(ihqty,
						message[10056],
						0);
				if(ihqty != 0){
					total = parseInt(total) + parseInt(ihqty);
					ihstr = ihstr + partNum + ',' + ihqty + ',' + cnum + ',|';
				}
			}
		
			if (total > total_unRecon) {
				strErrorMore = strErrorMore + "," + partNum;
			}
			if (soldqty < 0 || conqty < 0 || writeqty < 0 || retqty < 0 || ihqty < 0 || quarnQty < 0) {
				strErrNeg += "," + partNum;
			}

			var pnums = '';
			var ordid = '';
			for ( var rowid = 0; rowid < gridrowsarr.length; rowid++) {
				gridrowid = gridrowsarr[rowid];
				pnums = gridObj.cellById(gridrowid, 2).getValue();
				ordid = gridObj.cellById(gridrowid, 1).getValue();
				recpart = document.getElementById("Txt_Rec" + gridrowid).value;
				if (recpart != '') {
					pnums = recpart;
				}
				if (partNum == pnums) {
					if (total == total_unRecon) {
						strErrorMore += "," + pnums;
					}
				}
			}
			// Logic for the Partial Qty Reconcile for the same parts in an
			// Order
			var ord_qty = 0;
			var blFlg = false;
			var tot_qty = total_unRecon;
			for ( var rowid = 0; rowid < gridrowsarr.length; rowid++) {
				gridrowid = gridrowsarr[rowid];
				pnums = gridObj.cellById(gridrowid, 2).getValue();
				ordid = gridObj.cellById(gridrowid, 1).getValue();
				recpart = document.getElementById("Txt_Rec" + gridrowid).value;
				ord_qty = parseInt(gridObj.cellById(gridrowid, 3).getValue());
				if (recpart != '') {
					pnums = recpart;
				}
				if (partNum == pnums) {
					if (tot_qty > 0) {
						tot_qty -= ord_qty;
					} else {
						tot_qty = 0;
						blFlg = true;
					}
				}
			}
			if (blFlg == true) {
				strErrPartMsg += "," + partNum;
			}// Logic Ends
		});
		var strErrMsg = '';
		var strErrInvPart = '';
		for ( var ids = 0; ids < gridrowsarr.length; ids++) {
			gridrowid = gridrowsarr[ids];
			ordid = gridObj.cellById(gridrowid, 1).getValue();
			part = gridObj.cellById(gridrowid, 2).getValue();
			ordQty = gridObj.cellById(gridrowid, 3).getValue();
			reason = document.getElementById("Cbo_reason" + gridrowid).value;
			recpart = document.getElementById("Txt_Rec" + gridrowid).value;

			str += ordid + '^' + part + '^' + ordQty + '^' + recpart + '^' + reason + '|';
			if ((recpart == '' && reason != '0') || (recpart != '' && reason == '0')) {
				strErrMsg += ',' + ordid;
			}
			reconGridObj.forEachRow(function(rowId) {
				partNum = reconGridObj.cellById(rowId, part_rowId).getValue();
				if (recpart != '') {
					part = recpart;
				}
				if (partNum == part) {
					blFlg = true;
				}
			});
			if (blFlg == false) {
				errStrRecon += "," + part;
			}
		}
		if (strErrPartMsg != '') {
			Error_Details(Error_Details_Trans(message[10057],strErrPartMsg.substr(1, strErrPartMsg.length)));
		}
		if (strErrNeg != '') {
			Error_Details(Error_Details_Trans(message[10058],strErrNeg.substr(1, strErrNeg.length)));
		}
		if (strErrMsg != '') {
			Error_Details(Error_Details_Trans(message[10059],strErrMsg.substr(1, strErrMsg.length)));
		}
		if (strErrorMore != '') {
			Error_Details(Error_Details_Trans(message[10060],strErrorMore.substr(1, strErrorMore.length)));
		}
		if (errStrRecon != '') {
			Error_Details(Error_Details_Trans(message[10061],errStrRecon.substr(1, errStrRecon.length)));
		}
	} else if (actionType == '1006451') { // UnReconcile Process
		// declare for Reconciled Transaction grid
		var partNo_rowId = reconTransGridObj.getColIndexById("part");
		var partQty_rowId = reconTransGridObj.getColIndexById("part_qty");
		var orderID_rowId = reconTransGridObj.getColIndexById("orderid");
		var ordpart_rowId = reconTransGridObj.getColIndexById("orderpart");
		var unRecon_rowId = reconTransGridObj.getColIndexById("unRecon");

		reconTransGridObj.forEachRow(function(rowId) {

			unRecon_Val = reconTransGridObj.cellById(rowId, unRecon_rowId).getValue();
			if (unRecon_Val == '1') { // radio button selected
				/*
				 * partNum = reconTransGridObj.cellById(rowId,
				 * partNo_rowId).getValue(); ord_partNum =
				 * reconTransGridObj.cellById(rowId, ordpart_rowId).getValue();
				 * part_Qty = reconTransGridObj.cellById(rowId,
				 * partQty_rowId).getValue(); order_ID =
				 * reconTransGridObj.cellById(rowId, orderID_rowId).getValue();
				 * status_Val = reconTransGridObj.getUserData(rowId,"hStatus");
				 * //reconTransGridObj.cellById(rowId, status_rowId).getValue();
				 */
				strRecInp = rowId;
			}
		});
		if (strRecInp == '') {
			Error_Details(message[10062]);
		}
	} // end action type (Reconcile or UnReconcile)
	
if( retstr != '' || quarnstr !=''){//If retstr or quaratine str is not empty append the quarnstr with symbol of ~
	 retstr = retstr+'~'+quarnstr;
}
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	} else {
		// alert("STR is:"+str);
		// alert("CON STR is:"+constr);
		// alert("WRITE STR is:"+writestr);
		// alert("RET STR is:"+retstr);
		if (actionType == '1006450') { // Reconcile
			if (str == '' && constr == '' && writestr == '' && retstr == '' && ihstr == ''&& quarnstr =='') {
				Error_Details(message[10063]);
			}
			if (ErrorCount > 0) {
				Error_Show();
				Error_Clear();
				return false;
			}
               
			document.frmVendor.hAction.value = 'SaveRecon';
			document.frmVendor.hRecDoStr.value = str;
			document.frmVendor.hInputConStr.value = constr;
			document.frmVendor.hInputWriteStr.value = writestr;
			document.frmVendor.hInputRetStr.value = retstr;
			document.frmVendor.hInputihStr.value = ihstr;
			document.frmVendor.hRedirectURL.value = "/GmLoanerReconServlet?hAction=LoanRecon&hId="
					+ trans_id;
			document.frmVendor.hDisplayNm.value = "Loaner Reconciliation Screen";
		}
		if (actionType == '1006451') { // UnReconcile
			document.frmVendor.hTxnId.value = strRecInp;
			document.frmVendor.hTxnName.value = strRecInp;
			document.frmVendor.action = "/GmCommonCancelServlet";
			document.frmVendor.hAction.value = "Load";
			document.frmVendor.hCancelType.value = 'LRREC';
			document.frmVendor.hLogReason.value = document
					.getElementById("comments").value;
			document.frmVendor.hRedirectURL.value = "/GmLoanerReconServlet?hAction=LoadUnRecon&hId="
					+ trans_id;
			document.frmVendor.hDisplayNm.value = "Loaner Reconciliation Screen";
		}
		document.getElementById("Btn_Submit").disabled = true;
		fnStartProgress("Y");
		document.frmVendor.submit();
	}
}

function fnPicSlip(val, type, refid) {
	windowOpener("/GmConsignInHouseServlet?hAction=PicSlip&hConsignId=" + val
			+ "&ruleSource=" + type + "&refId=" + refid, "SetPic",
			"resizable=yes,scrollbars=yes,top=150,left=150,width=750,height=500");
}

function fnShowFilters(val) {
	var obj = eval("document.all." + val);
	var obj1 = eval("document.all." + val + "img");
	if (obj.style.display == 'none') {
		obj.style.display = 'inline-table';
		if (obj1) {
			obj1.src = '/images/minus.gif';
		}
	} else {
		obj.style.display = 'none';
		if (obj1) {
			obj1.src = '/images/plus.gif';
		}
	}
}
function fnFilterLoad() {
	var frm = document.frmVendor;
	var Pnum = frm.Txt_pnum.value;
	var repid = frm.Cbo_Rep.value;
	var Accid = frm.Cbo_Acc.value;
	var distid = frm.Cbo_Dist.value;
	var ordid = frm.Txt_ord.value;
	var status = frm.Cbo_Status.value;
	var startdate = frm.startDate.value;
	var enddate = frm.endDate.value;
	var strOpt = "0";

	var dtFrom = document.frmVendor.startDate.value;
	var dtTo = document.frmVendor.endDate.value;
	var startDate = document.frmVendor.startDate;
	var endDate = document.frmVendor.endDate;
	
	var fromToDiff = dateDiff(startDate.value, endDate.value, format);
	var fromCurrDiff = dateDiff(startDate.value, todaysDate, format);
	var toCurrDiff  = dateDiff(endDate.value, todaysDate, format);
	
	if ((dtFrom != '' && dtTo == '') || (dtFrom == '' && dtTo != '')) {
		Error_Details(message[10064]);
	}
	if (dtFrom != '' && dtTo != '') {
		CommonDateValidation(startDate,format,Error_Details_Trans(message[10065],format));
		CommonDateValidation(endDate,format,Error_Details_Trans(message[10066],format));
		
		if(fromToDiff < 0){
	    	 Error_Details(message[10067]);
		 }
		 if(fromCurrDiff < 0){
			 Error_Details(message[10068]);
		 }
		 if(toCurrDiff < 0){
			 Error_Details(message[10069]);
		 }
	}
	if (Pnum == '' && repid == '0' && Accid == '0' && distid == '0'
			&& ordid == '' && (dtFrom == '' && dtTo == '')) {
		var errMsgstr = message[5673];
		Error_Details(errMsgstr);
	}
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	document.getElementById("dataGridDiv").innerHTML = "<br><br><br><center><img src='/images/loader.gif'> <br><b>Loading...</b></center>";
	if (status == '1006441')
		strOpt = "RECON";
	else if (status == '1006442')
		strOpt = "UNRECON";
	document.getElementById("Btn_Load").disabled = true;
	var url = "\GmLoanerReconServlet?companyInfo="+companyInfoObj;
	var dateType = document.frmVendor.Cbo_Type.value;
	var parameters = "strOpt=" + strOpt + "&hAction=LoadFilter&PNUM=" + Pnum
			+ "&REPID=" + repid + "&ACCID=" + Accid + "&DISTID=" + distid
			+ "&ORDID=" + ordid + "&STARTDATE=" + startdate + "&ENDDATE="
			+ enddate + "&DTTYPE=" + dateType + "&rndId=" + Math.random();
	
	if (typeof XMLHttpRequest != "undefined") {
		xmlReq = new XMLHttpRequest();
	} else if (window.ActiveXObject) {
		xmlReq = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlReq.open("POST", url, true);

	// Send the proper header information along with the request
	xmlReq
			.setRequestHeader("Content-type",
					"application/x-www-form-urlencoded");
//	xmlReq.setRequestHeader("Content-length", parameters.length);
//	xmlReq.setRequestHeader("Connection", "close");

	xmlReq.onreadystatechange = fnCallback;
	xmlReq.send(parameters);
}

function fnCallback() {
	if (xmlReq.readyState == 4) {
		if (xmlReq.status == 200) {
			var strRetVal = xmlReq.responseText;
			if (strRetVal.indexOf('session has expired') >= 0) {
				document.getElementById("dataGridDiv").innerHTML = arrClientErr[1];
				document.getElementById("Btn_Load").disabled = false;
			} else {
				fnLoadData(strRetVal);
			}
			// document.getElementById("dataGridDiv").innerHTML = "Nothing to
			// Display";
		} else if (xmlReq.status >= 500) {
			document.getElementById("dataGridDiv").innerHTML = arrServerErr[xmlReq.status - 500];
			document.getElementById("Btn_Load").disabled = false;
		} else if (xmlReq.status >= 400) {
			document.getElementById("dataGridDiv").innerHTML = arrClientErr[xmlReq.status - 400];
			document.getElementById("Btn_Load").disabled = false;
		}
	} else {
		document.getElementById("dataGridDiv").innerHTML = 'Nothing to display';
		document.getElementById("Btn_Load").disabled = false;
	}
}
function fnLoadData(objData) {
	if (objData.indexOf("cell", 0) != -1) {
		gridObj = initGridData('dataGridDiv', objData);
		gridObj
				.attachHeader('#rspan,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#rspan,#rspan');
		document.getElementById("Btn_Load").disabled = false;
	} else {
		document.getElementById("dataGridDiv").style.color = "red";
		document.getElementById("dataGridDiv").style.height = "25";
		document.getElementById("dataGridDiv").innerHTML = "Nothing found to display";
		document.getElementById("Btn_Load").disabled = false;
	}
}

function fnChangeDate(obj) {
	var type = obj.value; 
	if(type == '26240273') { // 26240273 = LoanDt
	document.all.startDate.value = LoanDt;
	}else{
	document.all.startDate.value = fromDt;
	}
	document.all.endDate.value = fromDt;
	}
