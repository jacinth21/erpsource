//PMT-45746
function fnUpdateShipCost(){
	var shipCost = document.frmVendor.Txt_SCost.value;
	var orderId = document.all.hOrderId.value;
	var comments = document.frmVendor.Txt_LogReason.value;
	fnValidateTxtFld('Txt_LogReason',message[5550]);
	if( parseFloat(shipCost) < 0 || isNaN(parseFloat(shipCost))){  // Only Numeric values allow - PC-3458 - Allow to update zero dollor shipping charge
		Error_Details(message[5632]);
	}
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	else
	{
		
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmAdditionalCharges.do?method=updateShipCost&orderId='+orderId+'&shipCost='+shipCost+'&comments='+comments+'&randomId='+Math.random()+'&'+fnAppendCompanyInfo());
		dhtmlxAjax.get(ajaxUrl,function(loader){
			var resJSONObj =loader.xmlDoc.responseText;
			fnStartProgress();
			fnOnReload();
		});
	}
	fnStopProgress();
}

function fnOnReload(){
	var orderId = document.all.hOrderId.value;
	document.frmVendor.hAction.value = 'EditPrice';
	document.frmVendor.hOrdId.value = orderId;
	document.frmVendor.action = "GmOrderItemServlet";	
    document.frmVendor.submit();
}