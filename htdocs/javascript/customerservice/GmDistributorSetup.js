function fnSubmit()
{
	if (document.frmDistributor.Chk_ActiveFl.checked)
	{
		document.frmDistributor.Chk_ActiveFl.value = 'Y';
	}
	fnValidateForm();
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	fnStartProgress();
	document.frmDistributor.submit();
}
function fnValidateCheckForm(){
	fnValidateDropDn('Cbo_PartyPric',message_operations[254]);
}

function fnValidateForm(){
	
	var selIdx1 = document.frmDistributor.Cbo_DistTyp.selectedIndex;
	var value = document.frmDistributor.Cbo_DistTyp.options[selIdx1].value;
	var comperc = document.frmDistributor.Txt_ComnPerc.value;
	objStartDt = document.frmDistributor.Txt_StartDate;
	objEndDt = document.frmDistributor.Txt_EndDate;
	format= document.frmDistributor.format.value;
	fnValidateDropDn('Cbo_Region',message_operations[255]);
	fnValidateDropDn('Cbo_DistTyp',message_operations[256]);
	
	if (value == '70103' || value == '70104' || value == '70105'  || value == '70106' ||value=='26240143'){		// if selected value is ICT/ICS/FD/SWISS/Owner Company
		fnValidateDropDn('Cbo_PartyPric',message_operations[254]);
	}
	if(value =='70105'){ //ICS Type
		fnValidateDropDn('Cbo_ICT_Account',lblIcpAccount);
	}
	fnValidateDropDn('Cbo_BillState',message_operations[257]);
	fnValidateDropDn('Cbo_BillCountry',message_operations[258]);
	
	fnValidateTxtFld('Txt_DistNm',message_operations[259]);
	if(document.frmDistributor.Txt_DistNmEn!=undefined){
	fnValidateTxtFld('Txt_DistNmEn',message_operations[724]);
	}
	fnValidateTxtFld('Txt_DistNames',lblPrincipalName);
	fnValidateTxtFld('Txt_ContPer',lblContactPerson);
	
	// Validate the Commission Percent is Number or Blank
	if (comperc != '')
	{
		if (comperc > 100.00)
		{		
			Error_Details(" Commission % cannot be greater than 100%");
		}

        var perLen = comperc.length;
        var decPos = comperc.indexOf('.')+1;
        var decimalPlaces = perLen - decPos;
        if (decimalPlaces > 2)
        {
	        Error_Details(message_operations[261]);
        }

		NumberValidation(comperc, message_operations[262], 5)
	}
    
    fnValidateDropDn('Cbo_CommType',lblCommissionType);

	fnValidateTxtFld('Txt_BillName',message_operations[263]);
	fnValidateTxtFld('Txt_BillAdd1',message_operations[264]);
	fnValidateTxtFld('Txt_BillCity',message_operations[265]);
	fnValidateTxtFld('Txt_BillZipCode',message_operations[266]);
	fnValidateTxtFld('Txt_StartDate',message_operations[267]);
	fnValidateTxtFld('Txt_LogReason',message_operations[547]);
	if(objStartDt.value != ""){
		CommonDateValidation(objStartDt,format,message_operations[268]+ message[611]);
		
	}
	if(objEndDt.value != ""){
		CommonDateValidation(objEndDt,format,message_operations[269]+ message[611]);
		
	}
}
function fnReset()
{
	document.frmDistributor.hAction.value = "Load";
	document.frmDistributor.submit();
}

function fnLoadRep() {
	var distId = document.frmDistributor.Cbo_DistId.value;
	var strActions = 'EditLoad';
	if(distId == ''){
		strActions = 'Load';
	}

	document.frmDistributor.hAction.value = strActions;
	fnStartProgress();
	document.frmDistributor.submit();
}



function fnchkICT(){
	if(skipRFS != 'Y'){// if SKIP_RFS_FLAG is 'Y' then  avoid AddICTParameter button.
		document.getElementById("AddICTParams").style.visibility='hidden';
		var selIdx1 = document.frmDistributor.Cbo_DistTyp.selectedIndex;
		var value = document.frmDistributor.Cbo_DistTyp.options[selIdx1].value;
		if (value == '70103' || value == '70104' || value == '70105'  || value == '70106' || value == '26240143'){   //26240143=Owner Company
			document.frmDistributor.Cbo_PartyPric.disabled=false;
			document.getElementById("AddICTParams").style.visibility='';
			// if type is ICS then - only enable ICT account 
			if(value == '70105'){
				document.frmDistributor.Cbo_ICT_Account.disabled= false;
		    }else{
				document.frmDistributor.Cbo_ICT_Account.selectedIndex="";
				document.frmDistributor.Cbo_ICT_Account.disabled= true;
				}		
		}
	else {
		document.getElementById("AddICTParams").style.visibility='hidden' ;
		document.frmDistributor.Cbo_PartyPric.selectedIndex="";
		document.frmDistributor.Cbo_PartyPric.disabled=true;
		document.frmDistributor.Cbo_ICT_Account.selectedIndex="";
		document.frmDistributor.Cbo_ICT_Account.disabled=true;
		}
	}

}


function fnICTVar(){
	var selIdx2 = document.frmDistributor.Cbo_DistTyp.selectedIndex;
	var strInvSource = document.frmDistributor.Cbo_DistTyp.options[selIdx2].value;
	var selIdx1 = document.frmDistributor.Cbo_PartyPric.selectedIndex;
	var strRuleGrpId = document.frmDistributor.Cbo_PartyPric.options[selIdx1].value;
	fnValidateCheckForm();
		if (ErrorCount > 0){
				Error_Show();
				Error_Clear();
				return false;
		}else{
	    	windowOpener("/gmRuleParamSetup.do?codeGrpId=ICPTY&ruleGrpId="+strRuleGrpId+"&strInvSource="+strInvSource,"Rule","resizable=yes,scrollbars=yes,top=150,left=150,width=750,height=500");
 		}

}
// This function used to copy all the data (Bill to Address) to Ship to Address.
function fnCopyAddress() {
	document.frmDistributor.Txt_ShipName.value = document.frmDistributor.Txt_BillName.value;
	document.frmDistributor.Txt_ShipAdd1.value = document.frmDistributor.Txt_BillAdd1.value;
	document.frmDistributor.Txt_ShipAdd2.value = document.frmDistributor.Txt_BillAdd2.value;
	document.frmDistributor.Txt_ShipCity.value = document.frmDistributor.Txt_BillCity.value;
	var selIdx1 = document.frmDistributor.Cbo_BillState.selectedIndex;
	document.frmDistributor.Cbo_ShipState.selectedIndex = selIdx1;
	var selIdx2 = document.frmDistributor.Cbo_BillCountry.selectedIndex;
	document.frmDistributor.Cbo_ShipCountry.selectedIndex = selIdx2;
	document.frmDistributor.Txt_ShipZipCode.value = document.frmDistributor.Txt_BillZipCode.value;
	document.getElementById('Copy_Undo').innerHTML = '<a href="#" style="text-decoration: none" onClick="fnUndoAddress()"><img src="/images/dhtmlxGrid/undo.gif" alt="undo" style="border: none;" height="14"></a>';

}
// This function used to remove all the data for Ship to Address.
function fnUndoAddress() {
	document.frmDistributor.Txt_ShipName.value = "";
	document.frmDistributor.Txt_ShipAdd1.value = "";
	document.frmDistributor.Txt_ShipAdd2.value = "";
	document.frmDistributor.Txt_ShipCity.value = "";
	document.frmDistributor.Cbo_ShipState.selectedIndex = "";
	document.frmDistributor.Cbo_ShipCountry.selectedIndex = "";
	document.frmDistributor.Txt_ShipZipCode.value = "";
	document.getElementById('Copy_Undo').innerHTML = '<a href="#" style="text-decoration: none" onClick="fnCopyAddress()"><img src="/images/dhtmlxGrid/copy.gif" alt="copy" style="border: none;" height="14"></a>';
}
//This Function is to open a new window and to validate skipRFS Flag.
function fnSkipRFS(){
	var distId = document.frmDistributor.Cbo_DistId.value;
	windowOpener("/GmDistributorServlet?Cbo_DistId="+distId+"&hAction=MapParam","MapParameters","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
}
//This function is used to store the value for check box.
function fnSaveDistParams(){
	var distId = document.frmDistributor1.Cbo_DistId.value;
	var concat = "";
	if (document.frmDistributor1.Chk_UpdateFl.checked)
	{
		document.frmDistributor1.Chk_UpdateFl.value = 'Y';
	}else{
		document.frmDistributor1.Chk_UpdateFl.value = 'N';
	}
	if(strChecked != document.frmDistributor1.Chk_UpdateFl.value){
	concat = (distId)+"^"+(document.frmDistributor1.Chk_UpdateFl.value)+"|";
	document.frmDistributor1.hInputString.value = concat;
	document.frmDistributor1.hAction.value = "saveDistParam";
	fnStartProgress();
	document.frmDistributor1.submit();
	}
	else
		{
		Error_Details(message_operations[270]);
		Error_Show();
	    Error_Clear();
		return false;
		}
}
function fnClose(){
	window.close();	
}
//This function is used to enable the value for check box after it has been saved.
function fnonLoad(){
	if(strChecked == 'Y'){
		document.frmDistributor1.Chk_UpdateFl.checked = true;
	}else{
		document.frmDistributor1.Chk_UpdateFl.checked = false;
		}
}

