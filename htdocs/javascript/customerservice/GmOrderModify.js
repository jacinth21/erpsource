function fnSubmit(inputStr)
{	
	if (document.frmVendor.hOrdStatus != undefined){
	var ordfl = document.frmVendor.hOrdStatus.value;
	if(ordfl=='Shipped')//order status coming as shipped only
		{
		var hcnt = parseInt(document.frmVendor.hCnt.value);//number of part no.s
		var rmQtyObj = '';
		var rmQty = '';
		  for(var i=0; i <=hcnt; i++){
			 rmQtyObj = eval('document.frmVendor.Txt_RmQty'+i); //get the remove Qty
			 rmQty = rmQtyObj.value;
			  if(rmQty.length > 0){
				Error_Details("Cannot remove the parts from the order");	
			  }
			 if (ErrorCount > 0) {
			   		Error_Show();
			   		Error_Clear();
			   		return false;
			 }
		  }
	  }
	}
    
    if(document.frmVendor.Cbo_egpsusage != undefined){
    	var egpsusageVal = document.frmVendor.Cbo_egpsusage.value;  
	if(epgsFl =='Y' && egpsusageVal =='0'){
		Error_Details("Please provide <b>Capital Equp. Used</b> details to proceed further");	
		 if (ErrorCount > 0) {
		   		Error_Show();
		   		Error_Clear();
		   		return false;
		 }
	  }
    }
	splitAction = '';
	if (document.frmVendor.hQuoteCnt)
 	{
 	var strQuoteStr = fnCreateQuoteString();
 	document.frmVendor.hQuoteStr.value = strQuoteStr;
		if (ErrorCount > 0){
			Error_Show();
			Error_Clear();
			return false;
		}
 	}
	Error_Clear();
	fnSplitDO();
	if(splitAction == 1){//if remove qty in all parts is same as orginal qty then void the whole order  
		Error_Details(message[5025]);
		document.frmVendor.Btn_VoidOrd.disabled=false;
		document.frmVendor.Btn_BoOrd.disabled= true;
	}else if(splitAction == 2){
		Error_Details(message[5026]);
		document.frmVendor.Btn_BoOrd.disabled=false;
		document.frmVendor.Btn_VoidOrd.disabled=true;
	}else{
		var txt_order_date = document.frmVendor.Txt_OrderDate.value;
		var surgery_date = document.frmVendor.Txt_SurgDate.value;
		if (surgery_date != '' && surgery_date != undefined ){

			CommonDateValidation(document.frmVendor.Txt_SurgDate, dateFmt,
					'<b>'+lblSurgeryDt+' - </b>' + message[611]);
			diff = dateDiff(txt_order_date, surgery_date, dateFmt);
				if (diff > 0) {
					Error_Details(Error_Details_Trans(message[5280],lblSurgeryDt));
				}
			
		}
		//the below to validate the edited order date
		// Once Order date modified then validate 
		if (hOrdDate != txt_order_date && txt_order_date != '' && txt_order_date != undefined ){

			CommonDateValidation(document.frmVendor.Txt_OrderDate, dateFmt,
					'<b>'+lblOrderDt+' - </b>' + message[611]);
			diff = dateDiff(todaysDate, txt_order_date, dateFmt);
				if (diff > 0) {
					Error_Details(Error_Details_Trans(message[5284],lblOrderDt));
				}

				//to validate the edit order date range
				var editDaysNum = parseInt("-"+orderEditDays)+1; 
				if(isNaN(diff) || diff < editDaysNum){
					var arrayMsg = [lblOrderDt,orderEditDays];
					Error_Details(Error_Details_Trans(message[5285],arrayMsg));
				}
			
		}
		
		if (document.frmVendor.Txt_Reason.value == "")
		{
			Error_Details(message[59]);
		}
		hcnt = parseInt(document.frmVendor.hCnt.value);
		hcnt = hcnt+cnt;
		document.frmVendor.hCnt.value = hcnt;
		document.frmVendor.hCnumStr.value = inputStr;
		document.frmVendor.hAction.value = 'SaveOrder';
	}
	fnLenValidation(document.frmVendor.Txt_Comments,message_operations[547],"500");
	
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	} 
	if(strQuoteStr != false){
		document.frmVendor.Txt_OrderDate.disabled=false ;
		document.frmVendor.Btn_Submit.disabled =true;
		fnStartProgress('Y');
  		document.frmVendor.submit();
	}else{
		document.frmVendor.Btn_Submit.disabled =false;
	}
	
}
function fnVoidOrd(){
	var ordid = document.frmVendor.hOrdId.value;
	var ordInvFl = document.frmVendor.hOrdInvFl.value;

	if (ordInvFl == '1')
	{
		Error_Details(message[54]);
	}
	document.frmVendor.action = "/GmCommonCancelServlet?hTxnName="+ordid+"&hCancelType=VDRES&hTxnId="+ordid;
	document.frmVendor.hAction.value = "Load";
	document.frmVendor.submit();
}

function fnSinglePartBoOrder(){
	var ordid = document.frmVendor.hOrdId.value;
	var orderType = document.frmVendor.hOrdType.value;
	
	document.frmVendor.action = "/GmCommonCancelServlet?hTxnName="+ordid+"&hCancelType=MBTBO&hTxnId="+ordid;
	document.frmVendor.hAction.value = "Load";
	document.frmVendor.submit();
}

function fnSplitDO(){
	var partDtObj = '';
	var voidFlObj = '';
	var rmQtyObj = '';
	var boQtyObj = '';
	var cnumObj = '';
	var pnumObj = '';
	var hcnt = parseInt(document.frmVendor.hCnt.value);//number of part no.s
	var rmQtyStr = '';
	var BOQtyStr = '';
	var cnum = '';
	var rmVoidCount = 0;
	var rmBackCount = 0;
	var pnumqtystr = '';
	var pendShipQtyObj = '';
	var pendShipQtyStr = '';
	var rowCount = 0;
	var previousPart="";
	var previousQty="";
	var partString = new Array();
	var j=0;
	var usedLotStr = '';
	
	for(var i=0; i <=hcnt; i++){
		var voidFl = '';
		var pNumStr = '';
		var rmQty = '';
		var boQty = '';
		var orgQty = '';
		var price = '';

		partDtObj = eval('document.frmVendor.hPartDt'+i);
		rmQtyObj = eval('document.frmVendor.Txt_RmQty'+i);
		boQtyObj = eval('document.frmVendor.Txt_BoQty'+i);
		cnumObj = eval('document.frmVendor.hcNum'+i);
		pnumObj = eval('document.frmVendor.hpNum'+i);
		pendShipQtyObj = eval('document.frmVendor.hpendShipQty'+i);
		var partNumber = pnumObj.value;
		var partQty=rmQtyObj.value;
		if(partDtObj){ 
			rowCount += 1;
			pNumStr = partDtObj.value;
	
			orgQty = pNumStr.substr(0,pNumStr.indexOf("^"));
			rmQty = (rmQtyObj.value == '')? 0 : eval(rmQtyObj.value);
			boQty = (boQtyObj.value == '')? 0 : eval(boQtyObj.value);
			cnum = cnumObj.value;
			if(eval(orgQty)<(eval(rmQty)+eval(boQty))){
				pnumqtystr = pnumqtystr + pnumObj.value +',';			
			}
	
			if(eval(orgQty) - (pendShipQtyObj.value) < (eval(rmQty)+eval(boQty)))
			{
				pendShipQtyStr = pendShipQtyStr + pnumObj.value +',';		
			}		
			
			if(eval(orgQty) == eval(rmQty)){
				rmVoidCount += 1;
			}else if(eval(orgQty) == eval(boQty)){
				rmBackCount +=1;
			}
			if(rmQty >= 0)
			{
				rmQtyStr = rmQtyStr+pNumStr+'^'+rmQty+'^'+cnum+'|';
				if(usedLotFl == 'Y' && rmQty >0 ){
					if(i==0){
						 partString[j]=partNumber+'^'+parseInt(partQty);
						 previousQty=parseInt(partQty);
							previousPart=partNumber;
						}else if(partNumber==previousPart){
							previousQty=previousQty+parseInt(partQty);
							partString[j]=partNumber+'^'+previousQty;
						}else{
							j++;
							if(partString[0] == undefined){
								j--;
							}
							 partString[j]=partNumber+'^'+parseInt(partQty);
								previousPart=partNumber;
								previousQty=parseInt(partQty);
						}
				}
			}
			if(boQty >= 0)
			{
					BOQtyStr = BOQtyStr+pNumStr+'^'+boQty+'^'+cnum+'|';			
			}
		} //end of	if(partDtObj.value)	
	}
	for (var k = 0; k < partString.length; k++) {
				
	    var QtyPartStr = partString[k];
	    var qtyPartArray = QtyPartStr.split('^');
	    
	  usedLotStr += fnCreateUsedLotStr (qtyPartArray[0], qtyPartArray[1]);
	    //Do something
	}
	if (pnumqtystr != '' && dept!== 'J')	// skip the validation for A/R,  since A/R can only change comment section.
	{
		Error_Details(Error_Details_Trans(message[5027],pnumqtystr));
	}
	if (pendShipQtyStr != '')
	{
		Error_Details(Error_Details_Trans(message[5028],pendShipQtyStr));
	}
	if( rmVoidCount == (rowCount)){
		splitAction = 1;
	}else if( (rmBackCount == (rowCount)) && (rmBackCount==1)){
		splitAction = 2;
	}
	document.frmVendor.hRmQtyStr.value = rmQtyStr; 
	document.frmVendor.hBOQtyStr.value = BOQtyStr;
	if(usedLotFl == 'Y'){
		document.frmVendor.hUsedLotStr.value = usedLotStr;
	}
}

function fnNumbersOnly(e){
	var unicode=e.charCode? e.charCode : e.keyCode
	if (unicode!=8){ //if the key isn't the backspace key (which we should allow)
	if (unicode<48||unicode>57) //if not a number
	return false //disable key press
	}
}

var cnt = 0;
function fnSplit(val,pnum)
{
	cnt++;
	var hcnt = parseInt(document.frmVendor.hCnt.value);

	hcnt = hcnt+cnt;
	var priceobj = eval("document.frmVendor.hPrice"+val);
	var price = priceobj.value;

	var NewQtyHtml = "<span id='Sp"+cnt+"'><BR>&nbsp;&nbsp;&nbsp;<input type='hidden' name='hPartNum"+hcnt+"' value='"+pnum+"'><input type='hidden' name='hPrice"+hcnt+"' value='"+price+"'><input type='text' size='3' value='' name='Txt_Qty"+hcnt+"' class='InputArea' onFocus=changeBgColor(this,'#AACCE8'); onBlur=changeBgColor(this,'#ffffff'); tabindex=1>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='text' size='8' value='' name='Txt_CNum"+hcnt+"' class='InputArea' onFocus=changeBgColor(this,'#AACCE8'); onBlur=changeBgColor(this,'#ffffff'); tabindex=1>&nbsp;<a href=javascript:fnDelete("+cnt+");><img src=/images/btn_remove.gif border=0></a></span>";
	var QtyObj = eval("document.all.Cell"+val);

	QtyObj.insertAdjacentHTML("beforeEnd",NewQtyHtml);
}

function fnDelete(val)
{
	obj = eval("document.all.Sp"+val);
	obj.innerHTML = "";
}

function fnPrintPack()
{
	var val = document.frmVendor.hOrdId.value;
	windowOpener(servletPath+'/GmEditOrderServlet?hMode=PrintPack&hOrdId='+val,"Pack","resizable=yes,scrollbars=yes,top=150,left=150,width=760,height=610");	
}

function fnCallShip()
{
	obj = document.frmVendor.Cbo_ShipTo;
	var obj2 = document.frmVendor.Cbo_Rep;
	if (obj.options[obj.selectedIndex].text == 'Sales Rep')
	{
		var repid = document.frmVendor.Cbo_Rep[document.frmVendor.Cbo_Rep.selectedIndex].id;
		for (var j=0;j<obj2.length;j++)
		{
			if (obj2.options[j].value == repid)
			{
				obj2.options[j].selected = true;
				break;
			}
		}
		obj2.disabled = false;
	}
	else
	{
		obj2.disabled = true;
	}
}


function fnSetValue()
{
	var obj5 = document.frmVendor.Cbo_Rep;
	for (var j=0;j<obj5.length;j++)
	{
		if (obj5.options[j].value == ShipToId)
		{
			obj5.options[j].selected = true;
			break;
		}
	}
}
function fnLoad()
{
	var ShipToFl = '';
	if (ShipToFl != '4120')
	{
		fnCallShip();
		fnSetValue();
	}
	fnCallPerson()
	document.frmVendor.Cbo_BillTo.focus();
}

function fnChangeTrack(obj)
{
	changeBgColor(obj,'#ffffff');
	var val = obj.value;
	var len = val.length;

	if (len == 32) // for FedEX Express Shipping
	{
		val = val.substring(16,28);
		obj.value = val;
	}
	else if (len == 22) // for FedEX Ground Shipping
	{
		val = val.substring(7,22);
		obj.value = val;
	}
}

function fnCallPerson()
{
	obj = document.frmVendor.Cbo_Mode;
	var obj2 = document.frmVendor.Txt_PNm;
	
	if (obj.options[obj.selectedIndex].text != 'FAX')
	{
		obj2.disabled = false;
		obj2.focus();
	}
	else
	{
		obj2.disabled = true;
	}
	
	if(obj.value == '5017')
	{
		document.frmVendor.Chk_HardPO.disabled = true;
	}else{
		document.frmVendor.Chk_HardPO.disabled = false;
	}

}

function fnSetAccIdValue(val)
{
	document.frmVendor.Txt_AccId.value=val;
}

function fnLoadBtn(){
	document.frmVendor.Btn_VoidOrd.disabled=true;
	document.frmVendor.Btn_BoOrd.disabled=true;
	// direct order changes
	var orderType = document.frmVendor.hOrdType.value;
	if(orderType == 26240232 || orderType == 26240232){
		if(usedLotFl != 'Y'){
			// direct and intercompany orders
			var TRcnt = parseInt(document.all.hTextboxCnt.value);
			TRcnt = TRcnt + count;
			for (i = 1; i <= TRcnt; i++) {
				var objControlNum = eval("document.all.controlNumTxt" + i);
				objControlNum.disabled = true;
			}
		}
	}
	//once invoice is raised should disable surgery date
	if(invoiceId != ""){
			document.frmVendor.Txt_SurgDate.disabled = true;
			document.frmVendor.Img_Date[1].disabled = true;
	}else{
			document.frmVendor.Txt_SurgDate.disabled = false;
			document.frmVendor.Img_Date[1].disabled = false;
	}
}
function fnCreateQuoteString()
{
	var cnt = document.frmVendor.hQuoteCnt.value;	
	var str;
	var strquote = '';
	var strSetIdFlag = false
	var strOtherFlag = false
	for (i=0;i<cnt;i++)
	{
		var obj = eval("document.frmVendor.Txt_Quote"+i);
		var ordobj = eval("document.frmVendor.attrId"+i);
		id = obj.id;
		val = obj.value;
		ordid = ordobj.value;
		// 10304731: Patient Name code id of Poland
		if((id == '400145' || id == '400108' || id == '10304731')  && val != '' && val.length > 100){//400145: Patient name in surgery attribute, 400108: Patient Name in quotation attribute
				Error_Details(message[5029]);
		}
		
		//if (val != '') // When no enter the data , it will be update empty data. So removed if condition. 
		//{
		// In OUS will enter SET ID in surgery details in new order screen. Once entere,d after should not add more than one set id in modify order screen.	
		if(id == '4000098' && val != ''){ // Set ID
			var atpos =  val.indexOf(".");
			var atDotArr =  val.split(".");
			var atposComma =  val.indexOf(",");
			var array =  val.split(",");
			var length = array.length;
			var atDotArrlen = atDotArr.length;
			var Arrval = array[1];
			var dotArrVal = atDotArr[2];

			// Do not enter multiple set id in for one order id.
			if(Arrval != '' && Arrval != undefined ){
				strSetIdFlag = true;
			}
			// Do not enter more that one ./,/ 
			if(atpos == -1 || atposComma != -1 || length == 3 || atDotArrlen == 3 || (dotArrVal != '' && dotArrVal != undefined)){
				strOtherFlag = true;
			}
		}
		if(id == '400151' && val != ''){ // Date of Surgery 
            CommonDateValidation(obj,dateFmt,Error_Details_Trans(message[10554],dateFmt));
		}
		str = ordid+'^' + id + '^' + val + '|';
		strquote = strquote + str;
		//}
	}
	if(strSetIdFlag)
		Error_Details(message[5030]);
	if(strOtherFlag)
		Error_Details(message[5031]);
	
	/*if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}else{*/
		return strquote;
	//}
}
//To limit the no. of characters entering into the text area
function fnLenValidation(obj,name,len){ 
	if(obj.value.length > len){
		Error_Details(Error_Details_Trans(message[5032],name));
	}
}
//PMT-32396 Back Order Email
function fnCheckProdFmly(partNum,obj,index)
{
	//	Ajax call to fetch the part number and product family	
    var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmOrderDetails.do?method=fetchProductFamily&strPartNum='+partNum+'&ramdomId=' + Math.random());
	dhtmlxAjax.get(ajaxUrl, function(loader){
	    var response = loader.xmlDoc.responseText;
	    if (response == '26240096')
		{
			obj.disabled =true;
			document.getElementById("DivShowUsageCode"+index).style.display='';		
			obj.value = '';
		}
	});
}

//PMT-32396


