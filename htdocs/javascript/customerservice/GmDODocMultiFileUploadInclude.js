var fileCount = 1;


function fnAddElementMoreFiles(id) 
{
	

	var TBODY = document.getElementById(id).getElementsByTagName("TBODY")[0];
    
	for(var j=1; j<5;j++ ){
		
		var row = document.createElement("TR")

	    var td0 = document.createElement("TD")
	    td0.align = "right";
		td0.className = "RightTableCaption";
	    td0.innerHTML = '<font color="red">*</font>&nbsp;'+lblType+':&nbsp;';
		
	    var td1 = document.createElement("TD")
	    td1.innerHTML = '<select name="fileTypeId'+fileCount+'" id="fileTypeId'+fileCount+'" class=RightText  onChange="fnArtifactChange('+fileCount+');">'+optionString+'</select>';
	   
		var td4 = document.createElement("TD")
	    td4.innerHTML = '&nbsp;'+lblComments+':&nbsp;';
		td4.align = "right";
		td4.className = "RightTableCaption";
		
		var td5 = document.createElement("TD")
		var ctrlString ='<input type=text style="width:200px" name="fileTitle'+fileCount+'" id="fileTitle'+fileCount+'" class=InputArea  onFocus=changeBgColor(this,\'#AACCE8\'); value=\'\'>'
		                 + '<input type="hidden" name="titleType'+fileCount+'" id="titleType'+fileCount+'" value=""/>';
	    td5.innerHTML ='<span id="fileTitleSpan'+fileCount+'">'+ctrlString+'</span>';
		
		var td6 = document.createElement("TD")
	    td6.innerHTML = '<input type="file" name="theFile['+fileCount+']" id="theFile['+fileCount+']" value="">';

		row.height = "30";
		row.appendChild(td0);
		row.appendChild(td1);
		row.appendChild(td4);
		row.appendChild(td5);
		row.appendChild(td6);

		TBODY.appendChild(row);
		fileCount++;
	}

	 document.getElementById('attachMoreFile').innerHTML='';
	
} 

function fnFileUpload(form)

{
	
	
	var strFileName = "";
	var strFileType = "";
	var strFileTitle = "";
	var strSubType = "";
	var strTitleType = "";	
	var fileTypeInputstr = "";
	var fileTitleInputstr = "";
	var fileErrorInputstr = "";
	var strFille = "";
	var strFileId = "";

	var fileArr = new Array();
	
	fnValidateFilename();
	
	var stropt =  document.all.strOpt.value;
	var strOrderId = document.all.strOrderId.value;
	if(strOrderId == '' || strOrderId == undefined){
		Error_Details(message[5299]);
		
	}
	

	if (objGridData.indexOf("cell") !='-1') {
		var fileList = fnGetAllFiles();
	    var validate = fnValidateGridData(fileList);
	    if(validate == false){
	    	return false;
	    }
	}
	
	for (i=0; i < fileCount; i ++)
	{
		objFile = document.getElementById('theFile['+i+']');
		objType = document.getElementById('fileTypeId'+i);
		objTitle = document.getElementById('fileTitle'+i);
		strFileName = objFile.value;
		strFileType = objType.value;
		strFileTitle = objTitle.value;
		strFileId = (i==0)?document.all.hfileId.value:"";
		
		strFille = strFileName.substring(strFileName.lastIndexOf("\\")+1 );
			if(strFileType != '0' || strFileName != '') {
				fnValidateDropDn('fileTypeId'+i,lblType);
				if(strFileName == '' || strFileName == undefined)
				{
					Error_Details(message[5179]);
				}
			}
			if (ErrorCount > 0)
			{
				Error_Show();
				Error_Clear();
				return false;
			}
						
  	
		if(strFileType != '0' && strFileName != '')
		{	
		var count = 0;
			var strFileExt = strFileName.substring(strFileName.lastIndexOf("."));
			var strFileCase=strFileExt.toLowerCase();
			if(strWhiteList.indexOf(",")!=-1){//This will execute if more than one accept formats.
				var arrayFmt =  strWhiteList.split(',');
				for(j=0; j < arrayFmt.length; j ++)
				{
					var fileExtSplit = arrayFmt[j];
					fileExtSplit = fileExtSplit.substring(fileExtSplit.lastIndexOf("."));
					if(fileExtSplit==strFileCase){
						count++;
					}
				}
			}else{
				if(strWhiteList==strFileCase){
					count++;
				}
			}
			
			if(count<=0){
				Error_Details('File type(extension) is not supported.<br>Allowed extension(s): <b>'+ strWhiteList +'</b>');	 
				if (ErrorCount > 0)
				{
					Error_Show();
					Error_Clear();
					return false;	
				}
			}
			
			fileTypeInputstr = fileTypeInputstr + strFileType + '^'+strFileTitle + '^'+ strFille + '^'+26230732 + '^'+ 26240412+'^'+strFileId+'|';
			
		}
		
	}
	
	var hide_filename = document.all.hfilename.value;
	
	if (hide_filename != ''){
		var editFileFullPath = document.getElementById('theFile[0]').value;
		var editFileName= editFileFullPath.substring(editFileFullPath.lastIndexOf("\\")+1 );
		if(hide_filename != editFileName){
			Error_Details(Error_Details_Trans(message[5181],hide_filename));
		}
	}
	// to validate the file string
	
	if(fileTypeInputstr == ''){
		Error_Details('Please select the files to upload ');
	}
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;	
	}	
	form.action = "/gmDODocumentUpload.do?method=UploadDODocument";
	form.strOpt.value = 'UploadFiles';
	form.fileTypeList.value = fileTypeInputstr;
	fnStartProgress('Y');
	form.submit();
}

function fnValidateFilename()
{
	filecount = 0;
	var strNewFille = '';
	for (i=0; i < fileCount; i++)
	{
		objType = document.getElementById('fileTypeId'+i);
		strFileType = objType.value;
		objOldFile = document.getElementById('theFile['+i+']');
		strOldFileName = objOldFile.value;
		if(strOldFileName != ''){
			strOldFille = strOldFileName.substring(strOldFileName.lastIndexOf("\\")+1 );
		
			for(j=0; j < fileCount; j++){
				objNewFile = document.getElementById('theFile['+j+']');
				strNewFileName = objNewFile.value;
				strNewFille = strNewFileName.substring(strNewFileName.lastIndexOf("\\")+1 );
				//Upload Valid File in Upload DO Document
				var specialChar=/[*|\":<>[\]{}`\\()';~+_%#@&$]/;
				if (specialChar.test(strNewFille))
				{
				 Error_Details(message[10627]);
                 document.getElementById('theFile['+i+']').value = "";	
                 return false;		
                 }
				if(strNewFille != ''){					
					if(strNewFille == strOldFille && i != j){
						filecount++;
					}
				}
			}
		}
	}
	if (filecount>1){
		Error_Details(message[5182]);
	}
	
}

function fnGetAllFiles()
{
	gridfilecount = 0;
	var filleArr = new Array;
	for (i=0; i < fileCount; i++)
	{
		objType = eval(document.getElementById('fileTypeId'+i));
		strFileType = objType.value;
		objOldFile = document.getElementById('theFile['+i+']');
		strFileName = objOldFile.value;
		if(strFileName != ''){
			strFilleNm = strFileName.substring(strFileName.lastIndexOf("\\")+1 );
			filleArr.push(strFilleNm);
		}
	}
	return filleArr;
}



