
function fnPageLoad(){
	if(parent.dhxWins == undefined){
		if(document.frmOrderDetails.Btn_close != undefined){
		document.frmOrderDetails.Btn_close.style.visibility = 'hidden';
	}
	}
}

function fnLoad(){
fnValidateTxtFld('orderID',message_operations[277]);
if (ErrorCount > 0)
{
		Error_Show();
		Error_Clear();
		return false;
}
document.frmOrderDetails.strOpt.value="load";
document.frmOrderDetails.action ='/gmOrderDetails.do?method=loadCustPO';
document.frmOrderDetails.submit();
}

function fnSave(){
	fnValidateTxtFld('custPO',message_operations[278]);
	var text_PO = TRIM(document.frmOrderDetails.custPO.value);
	
	fnValidatePOSpace (text_PO);
	
	var custPoLen = text_PO.length;

	if(custPoLen > 20){
		Error_Details(message_operations[279]);
	}
	
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	var orderid = document.frmOrderDetails.hOrderID.value;
	document.frmOrderDetails.orderID.value = orderid;
	document.frmOrderDetails.action ='/gmOrderDetails.do?method=editCustPO';
	document.frmOrderDetails.submit();
}
function fnClose(){
	parent.dhxWins.window("w1").close();
}

//validate the space - Customer PO
function fnValidatePOSpace(CustPO) {
	var regexp = /\s/g;
	if (regexp.test(CustPO)) {
		Error_Details(message_operations[280]);
	}
}