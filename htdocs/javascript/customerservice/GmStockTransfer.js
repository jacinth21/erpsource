var gridObj;
// To load report 
function fnOnPageLoad() {

	if (objGridData.value != '') {
		gridObj = initGridData('stockTranferRpt', objGridData);
	}
}
//To initiate the dhtmlx grid
function initGridData(divRef, gridData) {
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.init();
    gObj.loadXMLString(gridData);
	gObj.attachHeader('#text_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#text_filter,#text_filter,#text_filter');

	return gObj;
}
//To download excel;
function fnExcel(){
	gridObj.toExcel('/phpapp/excel/generate.php');
}

//hyperlink call to process the request to fullfill
function fnProcessRQ(reqID){
	
	document.frmStockTransferReport.action = "/gmStockTransferAction.do?method=stockTransferReleaseControl";
	urlStr = "&transId="+reqID+"&"+fnAppendCompanyInfo();
	
	targetUrlStr = document.frmStockTransferReport.action+urlStr;
	parentLcn = parent.location.href;
	if(parentLcn.indexOf("GmEnterprisePortal") != -1)
	{
		location.href = targetUrlStr;
	}
	
	else if(parentLcn.indexOf("GmGlobusOnePortal")!= -1)
	 {
			location.href = targetUrlStr;
	}
	else
	{
		parent.location.href = targetUrlStr;
	}
	
	
}
//Set the release from to shelf when qty to release is zero
function fnChangeReleaseFrom(release_qty,index) {
	
	var releaseFromObj = eval("document.all.Cbo_Releasefrom"+index);
	
	if(release_qty == "0"){
		releaseFromObj.value = '106740';
	}
	
}


//Ajax call to get the part available qty details
function fnCallAjax(partnum,val,ind) 
{	
	index = ind;
	var opt =0;
	
	// Release from : Shelf (106740) - 100040 (FG) ,Release from : Bulk (106741) - 100041 (Bulk) 
	opt = (val == 106740)?100040:(val == 106741)?100041:opt;
	
	
	var ajaxUrl = fnAjaxURLAppendCmpInfo("/GmLoanerAjaxServlet?opt=" + encodeURIComponent(opt)+ "&partNum=" + encodeURIComponent(partnum) + "&ramdomId=" + Math.random());
	dhtmlxAjax.get(ajaxUrl,
			function(loader){
				var responseText = loader.xmlDoc.responseXML;
				if (responseText != null) {
					parseMessage(responseText);
				}
			});
	
}
	

function parseMessage(xmlDoc) 
{
	var pnum = xmlDoc.getElementsByTagName("pnum");
	for (var x=0; x<pnum.length; x++) 
	{
		var qty = parseXmlNode(pnum[x].childNodes[0].firstChild);
	}

	var QtyObj = eval("document.all.Txt_Stock"+index);
 
	  QtyObj.value = qty  ;

}

//To create the stock transfer consignments
function fnSubmit(){

var fgInputStr = "";
var blInputStr = "";

var partNumObj = "";
var qtyToReleaseObj = "";
var releaseFrmObj = "";
var requestedQtyObj = "";
var availableQtyObj = "";

var partNum = "";
var qtyToRelease = "";
var releaseFrm = "";
var requestedQty = "";
var availableQty = "";
var errReqQty = "";
var errAvailQty = "";
var errQtyToRealease = "";
var errReleaseFrm="";
var qtyModiifedFlag =  false;
var submitFl;

for (k=0; k <intSize; k ++)
    {
		partNumObj = eval("document.frmStockTransferReport.hPartNum"+k);
		partNum = (partNumObj != undefined)?  partNumObj.value:"";
		
		qtyToReleaseObj = eval("document.frmStockTransferReport.Txt_Qty_To_Release"+k);
		qtyToRelease = (qtyToReleaseObj != undefined)?  qtyToReleaseObj.value:"";

		releaseFrmObj = eval("document.frmStockTransferReport.Cbo_Releasefrom"+k);
		releaseFrm = (releaseFrmObj != undefined)?  releaseFrmObj.value:"";
		
		requestedQtyObj = eval("document.frmStockTransferReport.hrequestQty"+k);
		requestedQty = (requestedQtyObj != undefined)?  requestedQtyObj.value:"";
		
		availableQtyObj = eval("document.frmStockTransferReport.Txt_Stock"+k);
		availableQty = (availableQtyObj != undefined)?  availableQtyObj.value:"";
		
		
		var qtyToRel = parseInt(qtyToRelease);
		var reqQty = parseInt(requestedQty);
		var availQty = parseInt(availableQty);
		//FG input string  Release from : Shelf(106740) - Action 106703(FG-ST) ,Release from : Bulk(106741) - Action 106704(BL-ST) 
		if(releaseFrm == '106740'){
			fgInputStr += partNum+','+qtyToRelease+','+'106703'+'|';
		}
		if (releaseFrm == '106741') {
			blInputStr += partNum+','+qtyToRelease+','+'106704'+'|';
		}
		
		//Validation for Release From
		if(releaseFrm == "0" || releaseFrm == ""){
			errReleaseFrm +=partNum+",";
		}
		
		//Validation Qty
		var regexp = /(^-?\d\d*$)/;
		var qtyToReleaseExp = regexp.test(qtyToRel);
		 //Release from should be selected
		 if((isNaN(qtyToRel)) || qtyToRel < 0 || qtyToRelease == ""){
			errQtyToRealease += partNum+",";
		 }
		 // Qty to release should not be greater than requested qty  
		 if((isNaN(qtyToRel)) || (qtyToRel > reqQty)){
			errReqQty +=partNum+",";
	     }
		 // Qty to release should not be greater than available qty  
		 if((isNaN(qtyToRel)) || (qtyToRel > availQty)){
			errAvailQty +=partNum+",";
		 }
		 
		 if(reqQty != qtyToRel) {
			 qtyModiifedFlag = true;
		 }
	     
	    }

	if(errReleaseFrm != ''){
		Error_Details(message[5307]+errReleaseFrm.substring(0,errReleaseFrm.length-1));
	}else if(errQtyToRealease != ''){
		Error_Details(message[5306]+errQtyToRealease.substring(0,errQtyToRealease.length-1));
	}else if(errReqQty != ''){
		Error_Details(message[5304]+errReqQty.substring(0,errReqQty.length-1));
	}else if(errAvailQty != ''){
		Error_Details(message[5305]+errAvailQty.substring(0,errAvailQty.length-1));
	}
    
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false; 
	}
	
	if(qtyModiifedFlag){
		submitFl = confirm(message[5309]);
	}else{
		submitFl = true;
	}
	
	document.frmStockTransferReport.fgInputStr.value = fgInputStr;
	document.frmStockTransferReport.blInputStr.value = blInputStr;
	document.frmStockTransferReport.strOpt.value = 'save';
	if(submitFl == true){
		fnStartProgress("Y");
		document.frmStockTransferReport.submit();
	}
	
	

}

//To load the report
function fnLoad(){
	var objFromDt = "";
	var objToDt="";
	var fromDt = "";
	var toDate="";
	var errDate ="";
	
	
	objFromDt = document.frmStockTransferReport.fromDate;
	objToDt = document.frmStockTransferReport.toDate;
	
	fromDt=objFromDt.value;
	toDate = objToDt.value;
	if((fromDt == "") && (toDate != "")){
		fnValidateTxtFld('fromDate',message[10527]);
	}else if((fromDt != "") && (toDate == "")){
		fnValidateTxtFld('toDate',message[10528]);
	}
	
	CommonDateValidation(objFromDt,date_format,message[10507]);
	CommonDateValidation(objToDt,date_format,message[10508]);
	//From date should not be greater than To Date
	if (dateDiff(fromDt, toDate,date_format) < 0){
	    Error_Details(message[10579]);
	}
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false; 
	}
	
    fnStartProgress("Y");
    document.frmStockTransferReport.submit();
    
}
//back to stock transfer request report  
function fnBack(){
	
	var url = fnAjaxURLAppendCmpInfo('/gmStockTransferAction.do?method=stockTransferReport');
	document.frmStockTransferReport.action = url;
	document.frmStockTransferReport.submit();
	
}
//this function is used to print the stock transfer invoice
function fnStockTransferInvPrint(stockConsignID,stockInvID){
	
	   if(printCountFl == 3){
		   for(i=1;i<=printCountFl;i++){
			 recipientLbl = ((i==1)?"ORG_RECIPIENT":(i==2)?"DUP_TRANSPORTER":"TRIP_SUPPLIER");
			 fnInvPrint(recipientLbl,stockConsignID,stockInvID);
		}
	   }else{
		   windowOpener('/gmStockTransferAction.do?method=stockTransferInvoicePrint&fulfillTrnasctionId='+stockConsignID+'&invoiceId='+stockInvID+'&invPrintLbl='+printPurpose,"Print","resizable=yes,scrollbars=yes,top=150,left=200,width=790,height=700");
	   }
	}
//this function is used to print the stock transfer invoice
function fnInvPrint(printPurpose,stockConsignID,stockInvID){

	windowOpener('/gmStockTransferAction.do?method=stockTransferInvoicePrint&fulfillTrnasctionId='+stockConsignID+'&invoiceId='+stockInvID+'&invPrintLbl='+printPurpose,printPurpose,"resizable=yes,scrollbars=yes,top=150,left=200,width=790,height=700");

	
}
//this function is used to print the stock transfer delivery challen
function fnPrintSlip(val){
	
	   if(printCountFl == 3){
		   for(i=1;i<=printCountFl;i++){
			 recipientLbl = ((i==1)?"ORG_RECIPIENT":(i==2)?"DUP_TRANSPORTER":"TRIP_SUPPLIER");
			 fnPackSlipChallan(recipientLbl,val);
		}
	   }else{
		   windowOpener("/GmConsignSetServlet?hAction=PackVersion&hId="+val,"Con","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600"); 
	   }
}
//this function is used to print the stock transfer delivery challen
function fnPackSlipChallan(printPurpose,val){
	windowOpener("/GmConsignSetServlet?hAction=PackVersion&hId="+val+"&printPurpose="+recipientLbl,recipientLbl,"resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}