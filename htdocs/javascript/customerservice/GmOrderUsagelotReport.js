var gridObj;

// This function used to load grid at the page load time
function fnOnPageLoad() {
	if (objGridData.value != '') {
		gridObj = initGridWithDistributedParsing('dataGridDiv',objGridData);
	} 

}

// This function used to create the DHTMLX object and load the XML string
function initGridWithDistributedParsing(divRef, gridData) {
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/extweb/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableTooltips("true,true,true,true,true,true,true,true,true,true,true,true,true");
	gObj.init();
	gObj.attachHeader('#text_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#text_filter,#text_filter,#text_filter,#text_filter,#select_filter,#select_filter,#select_filter');
	gObj.enablePaging(true, 100, 10, "pagingArea", true);
	gObj.setPagingSkin("bricks");
	gObj.loadXMLString(gridData);
	return gObj;
}
//This Function Used to download Excel Sheet
function fnDownloadXLS() {
	gridObj.toExcel('/phpapp/excel/generate.php');

}
//This Function used to load usagelot details
function fnLoad(){
	
   var objFromDt ='';
   var objToDt='';
   var dealer = '';
   var account = '';
   var accountId = '';
   var partNumber = '';
   var distributor = '';
   var salesRepNm = '';
   var projectID = '';
   var lotNumber = '';
   var bProjectLotEmptyFl = false;
  
  
   dealer = document.frmUsageLotRpt.dealer.value;
   account = document.frmUsageLotRpt.account.value;
   accountId=document.frmUsageLotRpt.accountId.value;
   partNumber= document.frmUsageLotRpt.partNumber.value;
   objFromDt = document.frmUsageLotRpt.fromDate;
   objToDt = document.frmUsageLotRpt.toDate;
   if(strHideFSOrdUsageRptFl !='YES'){  //PC-3894 - Sterile - Order usage report
    distributor = document.frmUsageLotRpt.fieldSales.value;
    salesRepNm = document.frmUsageLotRpt.salesRep.value;
    projectID = document.frmUsageLotRpt.projectID.value;
    lotNumber = document.frmUsageLotRpt.lotNumber.value; 
   }
	fromDt=objFromDt.value;
	toDate = objToDt.value;
	if(strHideFSOrdUsageRptFl !='YES'){
	   if((salesRepNm == '') && (distributor == '') && (projectID =='') && (lotNumber =='')){
		   bProjectLotEmptyFl = true;
	   }
	   if( bProjectLotEmptyFl && (account == '' )&&(accountId == '' )&& (fromDt == '' )&&(toDate == '' )&&(partNumber == '' )){
			  Error_Details(message[5313]);
		}
	}else{
      if((dealer == '' ) && (account == '' )&&(accountId == '' )&& (fromDt == '' )&&(toDate == '' )&&(partNumber == '' )){
		Error_Details(message[5313]);
	  }
	}
	
	if((fromDt == "") && (toDate != "")){
		fnValidateTxtFld('fromDate',message[10527]);
	}else if((fromDt != "") && (toDate == "")){
		fnValidateTxtFld('toDate',message[10528]);
	}
	
	CommonDateValidation(objFromDt,format,message[10507],format);
	CommonDateValidation(objToDt,format,message[10508],format);
	if (dateDiff(fromDt, toDate, format) < 0)
	{
		Error_Details(message[10579]);
	}
	if (ErrorCount > 0){
		Error_Show();
	    Error_Clear();
		return false;
	}
	document.frmUsageLotRpt.strOpt.value = 'Load';
	document.frmUsageLotRpt.action = '/gmOrderUsageLotRpt.do?method=orderUsageLotRpt';
	fnStartProgress("Y");
    document.frmUsageLotRpt.submit();
}
//This function used to set account id in accountId Field
function fnChange(form){
	form.accountId.value = document.frmUsageLotRpt.account.value;
	
}
//this function used for display DO Summary Report
function fnViewOrder(val)
{	
	windowOpener('/GmEditOrderServlet?hMode=PrintPrice&hOrdId='+val,"Pack","resizable=yes,scrollbars=yes,top=250,left=250,width=965,height=450");	
}
//Description : Load the account name information
function fnDisplayAccNm(obj){
    var ajaxUrl = '';
	var accId = TRIM(obj.value);
	if( accId !=''){
	// Get the account name if the account id is given in the text field
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/GmOrderAjaxServlet?opt=GETACC&acc=' + + encodeURIComponent(accId) + '&ramdomId=' + Math.random());
	dhtmlxAjax.get(ajaxUrl, fnLoadAccAjax);
	}
	if(accId==""){
		Error_Details(message_accounts[193]);
	}
	if (ErrorCount > 0)
	 {		
		Error_Show();
		Error_Clear();
		return false;
	 }
	
	
}

function fnLoadAccAjax(loader) {
	var errFlag = false;
	var xmlDoc;
	var accountName;
	var acname = '';
	
	var response = loader.xmlDoc.responseXML;
	if (response != null) {
		accountName = response.getElementsByTagName("acc");
		for (var x = 0; x < accountName.length; x++) {
			acname = parseXmlNode(accountName[x].childNodes[9].firstChild);
		}
		if (acname != null && acname != '') {
			document.frmUsageLotRpt.searchaccount.value = acname;
			document.frmUsageLotRpt.account.value = document.frmUsageLotRpt.accountId.value;
		}else{
			// Need to throw validation if the account id is invalid
			document.frmUsageLotRpt.accountId.value = '';
			Error_Details(message[5314]);
			Error_Show();
			Error_Clear();
			return false;
		}
	}
	
}



