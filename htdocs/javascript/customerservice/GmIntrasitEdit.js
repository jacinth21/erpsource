
function fnPrintVersionLetter(printtype){
	val = consignId;
	var shipTo=shipToname;
	strPerforma="INTRAPERFORMA";	
	//PMT-20074 Stock transfer , if type is Stock transfer then show stock transfer invoice
	if(consignType == '106703' || consignType == '106704'){
		windowOpener('/gmStockTransferAction.do?method=stockTransferInvoicePrint&fulfillTrnasctionId='+val+'&invPrintLbl=ORG_RECIPIENT','ORG_RECIPIENT',"resizable=yes,scrollbars=yes,top=150,left=200,width=790,height=700");
	}else{
		windowOpener("/GmConsignSetServlet?hAction=PrintVersionLetter&hId="+val+"&hShipTo="+shipTo+"&hopt="+printtype+"&status="+status+"&hstrPerforma="+strPerforma,"Con","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
	}
}

function fnSplit(val,pnum)
{	
	var cnumObj =document.getElementById("Txt_CNum"+val); 
	var cnum = cnumObj.value;
	cnt = parseInt(document.getElementById("hNewCnt").value);	
	cnt++;
	document.getElementById("hNewCnt").value = cnt;
	var hcnt = parseInt(document.getElementById("hCnt").value);
	hcnt = hcnt+cnt;
	var pnumobj = document.getElementById(pnum);
	
	if(pnumobj != null){		
		pnumobj.value = pnumobj.value+hcnt+",";
	}
	var priceobj = eval("document.frmVendor.hPrice"+val);
	var price = priceobj.value;
	var pnumProdMaterialObj = document.getElementById("hPartMaterialType"+val);
	
	if ( pnumProdMaterialObj != null && pnumProdMaterialObj != undefined ){
	var pnumProdMaterialVal =  pnumProdMaterialObj.value;	
	}

	var vFetchMsg = (includeRuleEng != 'NO')?"fnFetchMessages(this,'"+pnum+"','PROCESS',Type,TransTypeID);":"";
	var NewQtyHtml = "<span id='Sp"+cnt+"'><BR>&nbsp;&nbsp;&nbsp;<input type='hidden' name='hPartNum"+hcnt+"'  id='hPartNum"+hcnt+"' value='"+pnum+"'><input type='hidden' name='hPrice"+hcnt+"' id='hPrice"+hcnt+"' value='"+price+"'>" +
			"<input  type='hidden' name='hPartMaterialType"+hcnt+"' id='hPartMaterialType"+hcnt+"' value='"+pnumProdMaterialVal+"'>" +
					"<input tabindex='' type='text' size='3' value='' name='Txt_Qty"+hcnt+"' id='Txt_Qty"+hcnt+"' class='InputArea Controlinput' onFocus=chgTRBgColor("+hcnt+",'#AACCE8'); onBlur=chgTRBgColor("+hcnt+",'#ffffff'); tabindex=1>" +
							"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
	"<input  tabindex='' type='text' size='22' value='"+cnum+"' id='Txt_CNum"+hcnt+"' name='Txt_CNum"+hcnt+"' class='InputArea Controlinput' maxlength = '40' onFocus=fnBlankLine("+hcnt+",this.value); onBlur=fnBlankTBE("+hcnt+",this.value); onChange='"+vFetchMsg+"' tabindex=1>&nbsp;&nbsp;&nbsp;&nbsp;" +
	
	"<select name='Cbo_Action"+hcnt+"' tabindex='' class='RightText' id='Cbo_Action"+hcnt+"' style = 'WIDTH: 180px' onchange=''> " +
			"<option value='106521'>Release to Finished Goods</option>" +
					"<option  value='106522'>Release to Quarantine</option>" +
							"<option  value='106523'>Release to Repack</option></select>" +
									"<a href=\"javascript:fnDelete('"+pnum+"','"+cnt +"')\";><img src=/images/btn_remove.gif border=0></a></span>";
	
	var QtyObj = eval("document.all.Cell"+val);

	QtyObj.insertAdjacentHTML("beforeEnd",NewQtyHtml);
}

function fnDelete(pnum,val){
	var Obj=eval("document.all.Sp"+val);
	Obj.innerHTML = "";
	//fnDeleteScanned(pnum,scanned); 
}

function fnSubmitIntrasit(pickLocRule) {
	
	var hcnt = parseInt(document.getElementById("hCnt").value);
	cnt = parseInt(document.getElementById("hNewCnt").value);
	hcnt = hcnt+cnt;
	var pnumobjval = "";
	for(var i = 0; i <= hcnt; i++ ){
		if(document.getElementById("hPartNum"+i) != undefined){
		pnumobjval += document.getElementById("hPartNum"+i).value+",";	
		}
	}
	var strErrorMore = '';
	var strErrorLess = '';
	var errQty = '';
	var arr=pnumobjval.split(',');
	if (setId ==''){	
		
				for(var i=0;i<arr.length-1;i++){
					var qtycnt=0;
					   qid = arr[i];
					   pnum1 = qid.replace(/\./g,'').replace(/\-/g,'');
					   blk = document.getElementById("hQty"+pnum1);
					   for(var j=0;j<=hcnt;j++) {
						   pnumobj = document.getElementById("hPartNum"+j);						
						   qtyobj = document.getElementById("Txt_Qty"+j);
						   		if (pnumobj&&qtyobj){
									if (pnumobj.value == qid) {
										qty = parseInt(qtyobj.value);
											if (qty > 0 && qty != "") {
												qtycnt = qtycnt + qty;
											} else {
												errQty += (errQty.indexOf(pnumobj.value) != -1)? "":", "+pnumobj.value;	
										}
									}
								} // IF Valid object
							} // END of Inner FOR loop   
					   if (qtycnt > parseInt(blk.value)){
						   var position = strErrorMore.search(qid);
						   if((position == -1) || (strErrorMore == "")){
							   strErrorMore  = strErrorMore +", "+ qid;
						   }else{
							   strErrorMore = strErrorMore;
						   }	
				       }
				       if(qtycnt < parseInt(blk.value)){
				    	   var position = strErrorLess.search(qid);
				    	   if((position == -1) || (strErrorLess == "")){
				    		   strErrorLess  = strErrorLess +", "+ qid;
						   }else{
							   strErrorLess = strErrorLess;
						   }						   
				       }
						 
						qtycnt = 0;
				} 		 
		}else {
			document.frmVendor.action = '/GmConsignItemServlet?hAction=IntransBuildSet&hConsignId='+consignId+'&hSetId='+setId;
		}

	var rpInputString='';
	var fgInputString='';
	var qnInputString='';	
	var strType = '40057';
	if(document.getElementById("TXN_TYPE_ID"))
		TxnType = document.getElementById("TXN_TYPE_ID").value;
	for(var i = 0; i <= hcnt; i++ ){
		var pnumobj = document.getElementById("hPartNum"+i);
		var priceObj = document.getElementById("hPrice"+i); 
		var qtyobj = document.getElementById("Txt_Qty"+i);
		var ctrlNumObj = document.getElementById("Txt_CNum"+i); 
		var	action = document.getElementById("Cbo_Action"+i);
		var pnum = "";
		var pnumredesig = "";
		var price = "";
		var qty = "";
		var ctrlNum = "";
		var actionval = "";

		if(pnumobj&&qtyobj){
		
			pnum = pnumobj.value;
		 	price = priceObj.value;
		 	qty = qtyobj.value;
		 	ctrlNum = ctrlNumObj.value;
            actionval = action.value;
		 	if(qty > 0) {
		 		if ((qty > 0) && (actionval == '106521')){
		 		fgInputString += pnum+"^"+ctrlNum+"^"+qty+"^"+price+"|";
		 		} if((qty > 0) && (action.value == '106522')){
		 		qnInputString += pnum+"^"+ctrlNum+"^"+qty+"^"+price+"|";
		 		}if((qty > 0) &&(action.value == '106523')){ 
		 		rpInputString  += pnum+"^"+ctrlNum+"^"+qty+"^"+price+"|";
		 		}
		 	}		
		 } 	
	}
	if(errQty!=''){
		Error_Details("Please enter a <b>Qty</b> in the Qty Box for the following parts: <b>"+errQty.substr(1,errQty.length)+"</b>");
	}
	
	if(strErrorMore != '') {
		Error_Details("<b>Qty</b> entered cannot be more than <b>Consign Qty</b> for the following: <b>"+strErrorMore.substr(1,strErrorMore.length)+"</b>");
	}	
	if(strErrorLess != '') {	
		Error_Details("<b>Qty</b> entered cannot be less than <b>Consign Qty</b> for the following: <b>"+strErrorLess.substr(1,strErrorLess.length)+"</b>");
	}
	if((strErrorMore == '') && (errQty =='') && (strErrorLess == '') && (setId == '')){
		document.frmVendor.action = '/GmConsignItemServlet?hAction=SplitValues&strTypeval='+strType+'&hConsignId='+consignId;		
	}
	if(ErrorCount>0){
		Error_Show();
		Error_Clear();
		return false;
	}
	document.getElementById("fgInpStr").value = fgInputString;
	document.getElementById("qnInpuStr").value = qnInputString;
	document.getElementById("rpInpStr").value = rpInputString;
	fnStartProgress('Y');
	document.frmVendor.submit();
	
}
	
function fnOpenTag(strPnum,strConId)
{
	windowOpener("/gmTag.do?strOpt=reload&pnum="+encodeURIComponent(strPnum)+"&refID="+strConId+"&refType=51000","PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=920,height=600");
}
