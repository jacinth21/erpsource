function fnOnLoad() {
	if(ObjData!='')
	{
		if(ObjData.indexOf("<cell>")!=-1){
			mygrid = setDefalutGridProperties('dataGridDiv1');
			var strFmt = curSymbol + "0,000.00";
			mygrid.setNumberFormat(strFmt,8,".",",");
			mygrid.setNumberFormat(strFmt,9,".",",");
			mygrid = initGridData('dataGridDiv1',ObjData);
		   	mygrid.enableLightMouseNavigation(true);
		   	mygrid.attachEvent("onEditCell", doOnCellEdit);
		   	if(releaseQtyFlag !='Y'){ // PMT-30867 - ACK Screen Changes 
		   	   mygrid.forEachRow(function(rowId) {
                   mygrid.cellById(rowId, 7).setValue('0');
			  });
		   	}
			fnCalcTotal();
			
		}else{
			mygrid = initGridData('dataGridDiv1',ObjData);
		}
	}
	if(ObjData1 !=''){
		objGrid = initGridData('dataGridDiv2',ObjData1);
		objGrid.enableLightMouseNavigation(true);
		objGrid.attachEvent("onEditCell", doOnCellEdit);
	}
	if(ObjData2 != undefined)
		{
		if(ObjData2 !=''){
			objGrid = initGridData('dataGridDiv3',ObjData2);
		}
		}
}

function doOnCellEdit(stage, rowId, cellInd, nValue, oValue) {
	//alert(stage);
	if (stage == 0) {
        //User starting cell editing: row id is" + rowId + ", cell index is " + cellInd
    } else if (stage == 1) {
        //Cell editor opened
    	
    } else if (stage == 2) {
        //Cell editor closed
        fnvalidate(rowId,cellInd);
        
    }
	if (ErrorCount > 0){
   		mygrid.enableEditTabOnly(false);
		Error_Show();
		Error_Clear();
		return false;
   	}else{
   		mygrid.enableEditTabOnly(true);
   	}
    return true;
}

function initGridData(divRef,gridData){
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);
	gObj.enableTooltips("true,true,true,true,true,true,true,true");
	gObj.setColTypes("ro,ro,ro,ro,ro,ro,ed,ro,ro");
	gObj.init();	
	gObj.loadXMLString(gridData);	
	return gObj;	
}

function fnvalidate(rowId,cellInd){
	    if(cellInd == '1')
		{
			fnGetPartDetails(rowId);
			return false;
		}else if (cellInd == '4')
		{
			fnSavePart(rowId);
			return false;
		}
		else if(cellInd == '0' || cellInd == '8' || cellInd == '6')
		{			
			return false;
		}

	
	partInv_rowId  = mygrid.getColIndexById("partnum");
	resrQty_cellId = mygrid.getColIndexById("reservedQty");
	var shfQty = eval(mygrid.cellById(rowId, 3).getValue());
	var cellQty = mygrid.cellById(rowId, cellInd).getValue();
	
	reserv_qty = eval(mygrid.cellById(rowId, resrQty_cellId).getValue());

	shfQty = parseInt(shfQty) + parseInt(reserv_qty);

	var objRegExp = /(^-?\d\d*$)/;

	if(isNaN(cellQty) || (!cellQty == '' && !objRegExp.test(cellQty))){
		Error_Details(Error_Details_Trans(message[5513],mygrid.cellById(rowId, partInv_rowId).getValue()));
	}else{
	    var orgQty = eval(mygrid.cellById(rowId, 4).getValue());
	    if(shfQty<cellQty && cellQty != 0){
	    	Error_Details(Error_Details_Trans(message[5514],mygrid.cellById(rowId, partInv_rowId).getValue()));
	    }
	    var pendQty = orgQty - eval(mygrid.cellById(rowId, 5).getValue());
	    if(cellQty <= pendQty){
	    	if(cellQty > 0){
		        var cellPrice = eval(mygrid.cellById(rowId, 8).getValue());
		        var total_amt = cellQty * cellPrice;
		        mygrid.cellById(rowId, 9).setValue(total_amt);
		        fnCalcTotal();
	    	}
	    }else{
	    	Error_Details(Error_Details_Trans(message[5515],mygrid.cellById(rowId, partInv_rowId).getValue()));
	    	mygrid.cellById(rowId, 7).setValue(pendQty);
	    }
	    if(cellQty < 0){
	    	Error_Details(Error_Details_Trans(message[5516], mygrid.cellById(rowId, partInv_rowId).getValue()));
	    }
	}
	//alert('fnvalidate2');
	
}
function fnCalcTotal(){
	var totalAmt = 0;
	var tot_amt = 0;
	var gridrows = mygrid.getAllRowIds();
    var gridrowsarr = gridrows.toString().split(",");
    var gridrowid;
    var pendQty =0;
    var shelfQty = 0;
    var ordQty = 0;
    var activeFl = 'N';
    var totalPendQty = 0;
    var shipCharge = (document.all.shipCharge)?document.all.shipCharge.value:0.00;
    for (var rowid = 0; rowid < gridrowsarr.length; rowid++) {
		gridrowid = gridrowsarr[rowid];
		pendQty = eval(mygrid.cellById(gridrowid, 7).getValue());
		shelfQty = eval(mygrid.cellById(gridrowid, 3).getValue());
		reqQty = eval(mygrid.cellById(gridrowid, 4).getValue());
		shipedQty = eval(mygrid.cellById(gridrowid, 5).getValue());
		activeFl = mygrid.getUserData(gridrowid,"hActiveFl");
		if(pendQty != '' && pendQty != 0 && activeFl !='N'){
			totalAmt += eval(mygrid.cellById(gridrowid, 9).getValue());
			totalPendQty += eval(mygrid.cellById(gridrowid, 7).getValue());
		}
		
		//if(pendQty > shelfQty){
		if ((reqQty-shipedQty) > (shelfQty)){  
			mygrid.setRowColor(gridrowid,"red");
		}
		
		tot_amt += eval(mygrid.cellById(gridrowid, 9).getValue());
    }
	//changed below condition for MNTTASK-4742.
//    if(totalPendQty <=0){
//    	document.getElementById("Btn_Submit").disabled="disable";
//    }else{
//    	document.getElementById("Btn_Submit").disabled=false;
//    }
    document.getElementById("hTotal").value=totalAmt;
    tot_amt = tot_amt + parseFloat(RemoveComma(shipCharge)); // Add the ship cost with total amount
    document.getElementById("total").innerHTML=lblTotal+": " + curSymbol + " " + tot_amt.formatMoney(2, '.', ',') + "&nbsp;&nbsp;";
}
Number.prototype.formatMoney = function(c, d, t){
	var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
	   return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};
 
function fnGenOrder(){
		document.frmVendor.hAction.value = "SAVEDELNOTE";
		var str="";
		var gridrows = mygrid.getAllRowIds();
	    var gridrowsarr = gridrows.toString().split(",");
	    var gridrowid;
	    var pendQty =0;
	    var shelfQty = 0;
	    var activeFl = 'N';
	    var ordQty = 0;
	    var confirmMsg = true;
	    var txt_PO = TRIM(document.frmVendor.Txt_PO.value);
	    var txt_OrderComments = document.frmVendor.Txt_OrderComments.value;
	    var partNumArr = '';
	    partInv_rowId  = mygrid.getColIndexById("partnum");
	    partNumArr = fnGetPartNumArray(gridrowsarr); // To get all the part numbers
	    fnValidateReleaseQty(gridrowsarr,partNumArr); // To validate whether the Qty to Release is greater than shelf qty or not
	    for (var rowid = 0; rowid < gridrowsarr.length; rowid++) {
			gridrowid = gridrowsarr[rowid];
			pendQty = mygrid.cellById(gridrowid, 7).getValue();
			shelfQty = eval(mygrid.cellById(gridrowid, 3).getValue());
			activeFl = mygrid.getUserData(gridrowid,"hActiveFl");
			fnvalidate(gridrowid,7);
			if (ErrorCount > 0)	{
				    document.frmVendor.hAction.value = "ACKORD";
 	    			Error_Show();
 	    			Error_Clear();
 	    			return false;
 	    			break;
 	    	}
			// MNTTASK-4744 - Discount Calculation - Added Base price
			if(pendQty != '' && pendQty != 0 && activeFl !='N')
				str += mygrid.cellById(gridrowid, partInv_rowId).getValue() + "," + mygrid.cellById(gridrowid, 7).getValue() + ",NOC#,50300,," + mygrid.getUserData(gridrowid,"hcapFl") + "," + mygrid.cellById(gridrowid, 8).getValue() + "," + mygrid.getUserData(gridrowid,"BasePrice")+"|";
	    }
	    if(str == ""){
	    	Error_Details(message[5517]);
	    }
	    fnValidatePOSpace(txt_PO);
	    if(!CustPOValidFl){
			Error_Details(message[5518]);
		}
	    
	    if(txt_PO != '' && custpo!=txt_PO || txt_OrderComments != '' && txt_OrderComments!= strComments){
	    	Error_Details(message[5519]);
		} 
	    if (ErrorCount > 0)
    	{
	    	    document.frmVendor.hAction.value = "ACKORD";
    			Error_Show();
    			Error_Clear();
    			return false;
     	}
	    fnStartProgress();
	    document.frmVendor.hInputString.value = str; 
		document.frmVendor.action = "/GmOrderItemServlet";
		document.frmVendor.submit();  
		
}

// To get the distinct part numbers in the grid
function fnGetPartNumArray(gridrowsarr){
	var partNum;
	var partNumArr = '|';
	var shelfQty = 0;
	var resrQty_cellId = mygrid.getColIndexById("reservedQty");
	for (var rowid = 0; rowid < gridrowsarr.length; rowid++) {
		gridrowid = gridrowsarr[rowid];
		partNum = mygrid.cellById(gridrowid, 1).getValue();
		shelfQty = eval(mygrid.cellById(gridrowid, 3).getValue());
		// Reserved qty also should be calculated with shelf qty to get the total shelf qty
		reserv_qty = eval(mygrid.cellById(gridrowid, resrQty_cellId).getValue());
		shelfQty = parseInt(shelfQty) + parseInt(reserv_qty);
		if(partNumArr.indexOf(partNum) == -1){
			partNumArr = partNumArr + partNum+ '^' +shelfQty + '|';
		}
	}
	partNumArr = partNumArr.substring(1, partNumArr.length);
	return partNumArr;
}

// To validate whether the total number of part number qty is greater than shelf qty or not
function fnValidateReleaseQty(gridrowsarr,partNumArr){
	var pendQty = 0;
	var shelfQty = 0;
	var gridPartNum;
	var partNum;
	var tempArr = partNumArr;
	var i=0;
	var totalQty = 0;
	while(tempArr.indexOf("|") != -1){
		partNum = tempArr.substring(0,tempArr.indexOf("^"));
		shelfQty = tempArr.substring(partNum.length+1,tempArr.indexOf("|"));
		
		for (var rowid = 0; rowid < gridrowsarr.length; rowid++) {
			gridrowid = gridrowsarr[rowid];
			pendQty = mygrid.cellById(gridrowid, 7).getValue();
			gridPartNum = mygrid.cellById(gridrowid, 1).getValue();
			if(partNum == gridPartNum){
				totalQty = parseInt(totalQty)+parseInt(pendQty);
			}
		}
		if(totalQty > shelfQty){
			Error_Details(Error_Details_Trans(message[5520],partNum));
		}
		totalQty = 0;
		tempArr = tempArr.substring(tempArr.indexOf("|")+1,partNumArr.length);
	}
}

function fnDOSummary(strOrdid){
	windowOpener('/GmEditOrderServlet?hMode=PrintPrice&hOrdId='+strOrdid,"Summ","resizable=yes,scrollbars=yes,top=250,left=250,width=760,height=410");
}
//To check the space between customer PO
function fnValidatePOSpace(CustPO) {
	var regexp = /\s/g;
	if (regexp.test(CustPO)) {
		Error_Details(message[5521]);
	}
}

//For OUS Distributor

function fnAddGenerateOusDistributor(){
	var accId = ousAccId;
	var release_order_id = objGrid.getColIndexById("release_order_id");
    var releaseOrder = '';
	objGrid.forEachRow(function(rowId) {
 	 releaseOrder = objGrid.cellById(rowId, release_order_id).getValue();
	});
	
	var release_order_type = objGrid.getColIndexById("release_order_type");
    var releaseOrderType = '';
    
    if(release_order_type != ''){
         	objGrid.forEachRow(function(rowId) {
    		releaseOrderType = objGrid.cellById(rowId, release_order_type).getValue();    	
         	});
    	if(releaseOrderType == "OUS Distributor"){
    		//Error_Details(message[5789]);
    		releaseOrder = '';
    	}
    	
     }	
	document.frmVendor.hAction.value = "OUSDistributor";
	if(releaseOrder == ''){
		document.frmVendor.hOpt.value = "Generate";	
	}else{
		document.frmVendor.hOpt.value = "Add";		
	}
	var str="";
	var gridrows = mygrid.getAllRowIds();
    var gridrowsarr = gridrows.toString().split(",");
    var gridrowid;
    var pendQty =0;
    var shelfQty = 0;
    var activeFl = 'N';
    var ordQty = 0;
    var confirmMsg = true;
    var txt_PO = TRIM(document.frmVendor.Txt_PO.value);
    var txt_OrderComments = document.frmVendor.Txt_OrderComments.value;
    var partNumArr = '';
    partInv_rowId  = mygrid.getColIndexById("partnum");
    partNumArr = fnGetPartNumArray(gridrowsarr); // To get all the part numbers
    fnValidateReleaseQty(gridrowsarr,partNumArr); // To validate whether the Qty to Release is greater than shelf qty or not
    for (var rowid = 0; rowid < gridrowsarr.length; rowid++) {
		gridrowid = gridrowsarr[rowid];
		pendQty = mygrid.cellById(gridrowid, 7).getValue();
		shelfQty = eval(mygrid.cellById(gridrowid, 3).getValue());
		activeFl = mygrid.getUserData(gridrowid,"hActiveFl");
		fnvalidate(gridrowid,7);
		if (ErrorCount > 0)	{
			    document.frmVendor.hAction.value = "ACKORD";
	    			Error_Show();
	    			Error_Clear();
	    			return false;
	    			break;
	    	}
		// MNTTASK-4744 - Discount Calculation - Added Base price   part no and qty
		if(pendQty != '' && pendQty != 0 && activeFl !='N')
			str += mygrid.cellById(gridrowid, 1).getValue()  + "," + mygrid.cellById(gridrowid, 7).getValue() + "," + mygrid.cellById(gridrowid, 8).getValue() + "|";  
    }
    if(str == ""){
    	Error_Details(message[5517]);
    }
   
    fnValidatePOSpace(txt_PO);
    if(!CustPOValidFl){
		Error_Details(message[5518]);
	}
    
    if(txt_PO != '' && custpo!=txt_PO || txt_OrderComments != '' && txt_OrderComments!= strComments){
    	Error_Details(message[5519]);
	} 
    if (ErrorCount > 0)
	{
    	    document.frmVendor.hAction.value = "ACKORD";
			Error_Show();
			Error_Clear();
			return false;
 	}
    fnStartProgress();
    document.frmVendor.hReleaseOrder.value = releaseOrder;
    document.frmVendor.hOusAccId.value = accId;
    document.frmVendor.hInputString.value = str; 
	document.frmVendor.action = "/GmOrderItemServlet";
	document.frmVendor.submit();  
	
}


function fnReleaseOusDistributor(){
	var accId = ousAccId;
	var release_order_id = objGrid.getColIndexById("release_order_id");
    var releaseOrder = '';
    objGrid.forEachRow(function(rowId) {
    	 releaseOrder = objGrid.cellById(rowId, release_order_id).getValue();
   	});
   	
    if(releaseOrder == ''){
		Error_Details(message[5786]);
	}
	    
    var release_order_type = objGrid.getColIndexById("release_order_type");
    var releaseOrderType = '';
    
    if(release_order_type != ''){
         	objGrid.forEachRow(function(rowId) {
    		releaseOrderType = objGrid.cellById(rowId, release_order_type).getValue();
    	});
    	
    	if(releaseOrderType == "OUS Distributor"){
    		Error_Details(message[5788]);
    	}
     }
    	
	document.frmVendor.hAction.value = "ReleaseOusDistributor";
	document.frmVendor.hOpt.value = "ReleaseDistributor";
	    
    if (ErrorCount > 0)  
	{
    	    document.frmVendor.hAction.value = "ACKORD";
			Error_Show();
			Error_Clear();
			return false;
 	}
    fnStartProgress();
    document.frmVendor.hOusAccId.value = accId;
    document.frmVendor.hReleaseOrder.value = releaseOrder; 
	document.frmVendor.action = "/GmOrderItemServlet";
	document.frmVendor.submit();  
	
}

function fnAddRow()
{	
	var rowId = mygrid.getUID(); 
	mygrid.addRow(rowId,'');
	mygrid.setCellExcellType(rowId,1,"ed");
	mygrid.setCellExcellType(rowId,4,"ed");
	mygrid.setCellExcellType(rowId,6,"ed");
	mygrid.setCellExcellType(rowId,7,"ed");
	mygrid.setCellExcellType(rowId,8,"ed");
	
}
function fnGetPartDetails(rowId)
{
	var pnum = ''; 
	var qty = '';
	var accid = ousAccId;	
	 if (accid == '')
	    {
	    	alert(message[10564]);
	    }
	 else
		 {
			var pnum = mygrid.cellById(rowId, 1).getValue();
	 		var qty =  eval(mygrid.cellById(rowId, 4).getValue());
	 		var ajaxUrl = fnAjaxURLAppendCmpInfo("/GmCartAjaxServlet?pnum=" + encodeURIComponent(pnum)+ "&qty= null" + "&acc="+encodeURIComponent(accid)+ "&regexp=NO"+"&"+fnAppendCompanyInfo());
	 		dhtmlxAjax.get(ajaxUrl,function (loader){
	    			var response = loader.xmlDoc.responseXML;	    			
	    			var partDetails = response.getElementsByTagName("pnum");	    			
	    			if (partDetails.length == 0)
	    			{
	    				// mygrid.cellById(rowId, 2).setValue("Invalid Part Number");
	    				 //mygrid.cellById(rowId,4).setDisabled(true);
	    				 mygrid.deleteRow(rowId);
	    				 Error_Details("Invalid part number" + " - "+ pnum);	    		    
				    	    if (ErrorCount > 0)  
				    		{
				    	    	    Error_Show();
				    				Error_Clear();				    				
				    				return false;
				    	 	}	    				 
	    				
	    			}else{	    			
	    			var strdesc = parseXmlNode(partDetails[0].childNodes[6].firstChild);
	    			var strstock = parseXmlNode(partDetails[0].childNodes[7].firstChild);
	    			var strprice = parseXmlNode(partDetails[0].childNodes[4].firstChild);	    			
	    			  mygrid.cellById(rowId, 2).setValue(strdesc); 
	    			  mygrid.cellById(rowId, 3).setValue(strstock);
	    			  mygrid.cellById(rowId, 8).setValue(strprice);	    			 
	    			}
	    				
	    		});
		 }	
	 return false;
}

function fnSavePart(rowId)
{
var accid = ousAccId;
var inputstr = "";
var itemorderid = "";
var ackid =  document.frmVendor.hOrderId.value;
var pnum = mygrid.cellById(rowId, 1).getValue();
var qty = eval(mygrid.cellById(rowId, 4).getValue());
var price = eval(mygrid.cellById(rowId, 8).getValue());
var qtyRel = eval(mygrid.cellById(rowId, 6).getValue());
var shelfQty = eval(mygrid.cellById(rowId, 3).getValue());

var extPrice = "";
//PMT-30867 - ACK Screen Changes -Autofill check box for Add part 
if(qty > shelfQty){ //BUG-12070
   mygrid.cellById(rowId, 12).setValue(shelfQty); //hidden column
}else{
    mygrid.cellById(rowId, 12).setValue(qty);
}

if(document.frmVendor.Chk_ReleaseQty_Fl.checked == true){
	if(qty > shelfQty){ 
	     mygrid.cellById(rowId, 7).setValue(shelfQty);
	}else{
	   mygrid.cellById(rowId, 7).setValue(qty);
	}
	
}else{
	mygrid.cellById(rowId, 7).setValue('0');
}

if(price == null || price == undefined){
	Error_Details("Invalid Price for "+pnum);
	if (ErrorCount > 0)  
	{
	  Error_Show();
	  Error_Clear();				    				
	  return false;
	 }
}

if(qty != null && qty != undefined){
if(qty < 0)
	{
	  Error_Details("Invalid part qty for "+pnum);
		if (ErrorCount > 0)  
		{
		  Error_Show();
		  Error_Clear();				    				
		  return false;
		 }
	}
else
	{
	
	inputstr += encodeURIComponent(pnum) + "," +encodeURIComponent(qty) + "," + encodeURIComponent(price) + "|";
	 
  var ajaxUrl = fnAjaxURLAppendCmpInfo("/GmOrderAjaxServlet?opt=addParts&orderID="+ackid+ "&hInputString=" + inputstr+"&acc="+accid+"&"+fnAppendCompanyInfo());   
	
	dhtmlxAjax.get(ajaxUrl,function (loader){
		var response = TRIM(loader.xmlDoc.responseText);		
		mygrid.cellById(rowId,4).setDisabled(true);
		mygrid.cellById(rowId,1).setDisabled(true);
		mygrid.cellById(rowId,8).setDisabled(true);
		mygrid.cellById(rowId,5).setValue("0");
		extPrice = qtyRel * price;
		mygrid.cellById(rowId,9).setValue(extPrice);
		mygrid.cellById(rowId,0).setValue("<img alt='Remove Part Number' onClick='fnRemovePart("+rowId+","+response+")' src='/images/dhtmlxGrid/delete.gif' border='0'/>");
		//fnAddRow();		
		
	});	
	return false;
	}
}
	
}


function fnRemovePart(rowID,ItemOrdId)
{
	var accid = ousAccId;
	var ackid =  document.frmVendor.hOrderId.value;
	var releaseOrderType = '';
	var pnum = eval(mygrid.cellById(rowID, 1).getValue());
	var qty = eval(mygrid.cellById(rowID, 4).getValue());	
	//OrgQty^ItemOrdId^PNUM^PType^RmQty
	inputstr = encodeURIComponent(qty) + "^"+ItemOrdId+"^" +encodeURIComponent(pnum) + "^^^" +encodeURIComponent(qty)  + "^|";
	  var ajaxUrl = fnAjaxURLAppendCmpInfo("/GmOrderAjaxServlet?opt=removeParts&orderID=" + encodeURIComponent(ackid)+ "&hInputString=" + inputstr);
		dhtmlxAjax.get(ajaxUrl,function (loader){
			mygrid.deleteRow(rowID);
		});
}
//Check box onclick Function
function fnRealeseQty(){
	if(document.frmVendor.Chk_ReleaseQty_Fl.checked == true){
	mygrid.forEachRow(function(rowId) {
		var ShipQty =mygrid.cellById(rowId, 12).getValue();
       mygrid.cellById(rowId, 7).setValue(ShipQty);
	  });
	}else{
		mygrid.forEachRow(function(rowId) {
            mygrid.cellById(rowId, 7).setValue('0');
		  });
	}
}