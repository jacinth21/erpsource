function fnUpdateOrder(val)
{

	var shipDateObj = '';
	document.frmShip.hAction.value = val;
	var intramapid = partyId;
	
	if(validateObject(document.frmShip.Txt_SDate) && shipDateObj.value != '' && shipDateObj.value != null){
		shipDateObj = document.frmShip.Txt_SDate;
		CommonDateValidation(shipDateObj,dateFmt,Error_Details_Trans(message[10019],dateFmt));
	}
	
	if( intramapid == ''){	
		if (document.frmShip.Txt_Track.value == '' )
		{
			Error_Details(message[701]);
		}
		fnSubmit();
	}
	else{
		var ans = confirm(message[1016]);
		if (ans == true ){
				if( intramapid != ''){	
					if (document.frmShip.Txt_FreightAmt.value == '' )
						{
							Error_Details(message[10020]);
						}
						
					if (isNaN(document.frmShip.Txt_FreightAmt.value)== true)	
						{	
							Error_Details(message[10021]);
						}
				}	
				
				fnSubmit();
			}
		}
	
	
}
function fnUpdateDummyOrder(val)
{
	document.frmShip.hAction.value = val;
	document.frmShip.hMode.value = "DummyVerify";
	fnSubmit();
}
function fnVoidDummyConsignment()
{       
		document.frmShip.action = servletPath+"/GmCommonCancelServlet";
		document.frmShip.hAction.value = "Load";
		document.frmShip.hOpt.value = "";
		document.frmShip.hCancelType.value = "VCOND";
		document.frmShip.submit();
}
function fnPrintVer()
{
windowOpener("/GmConsignSetServlet?hAction=PrintVersion&hId="+consignId,"Con","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnPrintDummyVer(val)
{
windowOpener("/GmConsignSetServlet?hAction=Print"+val+"&hId="+consignId,"Con","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}
function fnAgree()
{
windowOpener("/GmConsignSetServlet?hId="+consignId+"&hAction=Ack&","Ack","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");

}

function fnChangeTrack(obj)
{
	changeBgColor(obj,'#ffffff');
	var val = obj.value;
	var len = val.length;

	if (len == 32) // for FedEX Express Shipping
	{
		val = val.substring(16,28);
		obj.value = val;
	}
	else if (len == 22) // for FedEX Ground Shipping
	{
		val = val.substring(7,22);
		obj.value = val;
	}
}

function fnPackslip()
{
windowOpener("/GmConsignSetServlet?hAction=PackVersion&hId="+consignId,"Con","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnRollback()
{
		document.frmShip.action = servletPath+"/GmCommonCancelServlet";
		document.frmShip.hAction.value = "Load";
		document.frmShip.submit();	
}

function fnVoidItemCsg()
{
		document.frmShip.action = servletPath+"/GmCommonCancelServlet";
		document.frmShip.hAction.value = "Load";
		document.frmShip.hOpt.value = "";
		document.frmShip.hCancelType.value = "VODCN";
		document.frmShip.submit();	
}

function fnReleaseForSale()
{
	var partNumList = document.frmShip.hPartNum.value;
	partNumList = partNumList.substr(0,partNumList.length-1);
	windowOpener('/GmProdReportServlet?hAction=Part&hId=0&Txt_PartNum='+encodeURIComponent(partNumList),"Pack","resizable=yes,scrollbars=yes,top=250,left=50,width=960,height=510");
}

function fnSubmit()
{
if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	document.frmShip.Btn_Update.disabled=true;
	document.frmShip.submit();
}