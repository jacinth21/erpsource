
function initGrid(divRef,gridData)
{
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);
	gObj.enableTooltips("true,true,true,true,true,true");
	gObj.init();	
	gObj.loadXMLString(gridData);	
	return gObj;	
}
function fnOnLoad(){
	gridObj = initGrid('dataGrid', objGridData);
    var gridrows = gridObj.getAllRowIds(",");
    if (gridrows.length == 0) {
    	document.getElementById("excelrow").style.display = 'none';
    	document.getElementById("dataGrid").style.height = "22"; //Header height given to support Edge PC-3659 
		document.getElementById("ShowDetails").style.color = "red";
		document.getElementById("ShowDetails").style.height = "20";
		document.getElementById("ShowDetails").innerHTML = message[5638];
    }
   
}

function fnDrilldown(caseid,surgDate,distid,status){
    // Get the parentCaseId, Surgery Date, and Distributor values and set it in request.
    var strhAction  = 'Reload';
    var strOpt     = 'Requests';
    var caseID   = caseid;
    var distId   = distid; // The selected caseid's distributor id
    var dateType = 'SG'; // Surgery Date
	var fromDate = surgDate; // Case's Surgery Date
	var toDate   = surgDate; // Case's Surgery Date
	document.frmCaseSumReport.hStatus.value = '10,20,30,40';	
	
	// THIS GOES TO BOOKING DETAILS REPORT
    document.frmCaseSumReport.Cbo_DistId.value = distId;
    //document.frmCaseSumReport.dtType.value = dateType;
	document.frmCaseSumReport.fromDt.value = fromDate;
	document.frmCaseSumReport.toDt.value = toDate;
	document.frmCaseSumReport.hAction.value = strhAction;
    document.frmCaseSumReport.strOpt.value = strOpt;
    document.frmCaseSumReport.hOpt.value = strOpt;
	document.frmCaseSumReport.hCaseId.value = caseID;
    // Submit the form.
    document.frmCaseSumReport.submit();
}

function fnLoadRpt(){
    var strOpt   = 'LoadCaseSummaryRpt';
	var strhAction = 'Reload';
    var distId   = document.frmCaseSumReport.Cbo_DistId.value;
    //var dateType = document.frmCaseSumReport.dtType.value;
	var fromDate = document.frmCaseSumReport.fromDt.value;
	var toDate   = document.frmCaseSumReport.toDt.value;
	var surFromDt = document.frmCaseSumReport.fromDt;
	var surToDt   = document.frmCaseSumReport.toDt;
	
	if(fromDate != "" && toDate == ""){
		Error_Details(message[5089]);
	}
	if(toDate != "" && fromDate == ""){
		Error_Details(message[5090]);
	}	
	if(fromDate != ""){
		CommonDateValidation(surFromDt,format,Error_Details_Trans(message[5636],format));
	}
	if(toDate != ""){		
		CommonDateValidation(surToDt,format,Error_Details_Trans(message[5637],format));
	}
	var dateDiffVal = dateDiff(fromDate,toDate,format);
	if(fromDate != '' || toDate != ''){
		if(dateDiffVal < 0){
			Error_Details(message[5091]);
		}
	}
    // Validate for selecting at least one field in the screen.
	if ((distId == '' || distId == '0') && (fromDate == '' && toDate == '')){
		Error_Details(message[5092]);
	}
    // Display error messages if exist
    if (ErrorCount > 0){
		Error_Show();
        Error_Clear();
		return false;
    }
	//document.frmCaseSumReport.dtType.value = dateType;
	document.frmCaseSumReport.fromDt.value = fromDate;
    document.frmCaseSumReport.toDt.value = toDate;
	document.frmCaseSumReport.hAction.value = strhAction;
	document.frmCaseSumReport.strOpt.value = strOpt;
	document.frmCaseSumReport.hOpt.value = strOpt;
	fnStartProgress();
	// Submit the form.
    document.frmCaseSumReport.submit();
    
}

function fnExcelExport(){
    gridObj.editStop();
    gridObj.toExcel('/phpapp/excel/generate.php');
}
