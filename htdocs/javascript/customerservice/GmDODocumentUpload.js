var gridObj;
function initGridFile(divRef,gridData){
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);
	gObj.init();	
	gObj.loadXMLString(gridData);	
	return gObj;	
}

function fnOnPageLoad()
{
	var stropt =  document.all.strOpt.value;
	var refid = document.all.refId.value;
	// to validate the DO document
	if(document.frmDODocumentUpload.strOrderId != undefined){
		fnValidateDO (document.frmDODocumentUpload.strOrderId);
	}
	
	if (objGridData != '') {
		gridObj = initGridFile('uploadedFilesGrid',objGridData);
		gridObj.attachHeader(""+'#text_filter,#select_filter,#text_filter,#text_filter,#select_filter,#select_filter');
		fileName_hide_rowId = gridObj.getColIndexById("fileName_hidden");
		
	}
	
}

// fnLoad() will call when you select second dropdown / hit the load button
function fnLoad()
{
	fnValidateTxtFld('strOrderId', 'Order ID');
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
    document.frmDODocumentUpload.strOpt.value = 'Load';
	document.forms[0].action = '/gmDODocumentUpload.do?method=loadOrderUpload';
	fnStartProgress('Y');
	document.frmDODocumentUpload.submit();
}

// fnDownloadFile() to download file
function fnDownloadFile(fileid){
	document.frmDODocumentUpload.haction.value = '';
	document.frmDODocumentUpload.action = '/gmDODocumentUpload.do?method=downloadFiles&strOpt=Download&fileID='+fileid+'&strOrderId='+document.frmDODocumentUpload.refId.value;
	document.frmDODocumentUpload.submit();	 
	
}

function fnClose(){
	  CloseDhtmlxWindow();
		 return false; 
}

//Description : This function used validate the DO
function fnValidateDO (objOrderID){
	var orderId = TRIM(objOrderID.value);
	if(orderId != ''){
		var ajaxUrl = fnAjaxURLAppendCmpInfo("/GmOrderAjaxServlet?opt=CHECKDOID&doid="+orderId);
		var loader = dhtmlxAjax.getSync(ajaxUrl);
		fnCheckDO (loader);
	}else{
	       document.getElementById("DivShowDOIDExists").style.display='none';
           document.getElementById("DivShowDOIDUnAvail").style.display='none';
	}
	
}

//Description : This function used to show/hide the tick mark
function fnCheckDO (loader) {
	var response = loader.xmlDoc.responseText;
	if(response!= null);
	 { 
		 if(response.indexOf("DUPLICATE")!= -1){
	            document.getElementById("DivShowDOIDExists").style.display='inline-block';
	            document.getElementById("DivShowDOIDUnAvail").style.display='none';
		 }else{
			 document.getElementById("DivShowDOIDUnAvail").style.display='inline-block';
			 document.getElementById("DivShowDOIDExists").style.display='none';
		 }
		 }
}



function fnValidateGridData(fileList)
{
	var fileArrLength = fileList.length;
	var gridfileName = '',returnFl=true;
	for (i=0; i < fileArrLength; i++)
	{
		gridfilecount = 0;
		var updfilename = fileList[i];
		gridObj.forEachRow(function(rowId) {
			gridfileName = gridObj.cellById(rowId, fileName_hide_rowId).getValue();
			if(gridfileName == updfilename){
				gridfilecount++;
				if (gridfilecount>0){
			       var  alert = confirm(Error_Details_Trans(message[5301],gridfileName));
			      
			       if (alert == false) {
			    	   returnFl=false;
			       } 
				}
				
			}
	   });
	}
	return(returnFl);
}

