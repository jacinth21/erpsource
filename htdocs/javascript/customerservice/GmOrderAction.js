

function fnDisplayAccID(vAction)
{		
	//var accID = Cbo_AccId.getSelectedValue();
	var accID = document.frmAccount.Cbo_AccId.value;
	if (accID != '0' && accID != null && accID != ''){
		document.frmAccount.Txt_AccId.value = accID ;
		fnSetAccCurrSym(accID,document.frmAccount.Cbo_AccCurrId);
	}else{
		document.frmAccount.Txt_AccId.value = '';
		fnResetAccCurrSymb(vAction);
	}
}


function fnResetAccCurrSymb(vAction){
	if(vAction != undefined && vAction == "reload") return;
	var accCurrObj = document.frmAccount.Cbo_AccCurrId;
	for ( var i = 0; i < accCurrObj.options.length; i++ ) {
        if ( accCurrObj.options[i].id == compCurrSign ) {
        	accCurrObj.options[i].selected = true;
            return;
        }
    }
}

function fnSetAccCurrSym(accID,targetObj){
	var dhxAccCurrUrl = fnAjaxURLAppendCmpInfo('/GmOrderAjaxServlet?opt=GETACCTCURR&acc='+accID+'&ramdomId=' + Math.random());
    dhtmlxAjax.get(dhxAccCurrUrl,function(loader){
    	   var accCurrDtl;
    	   var response = loader.xmlDoc.responseText;    
              if((response != null || response != '') ){
            	  accCurrDtl =response.split("|");
            	//  currencyFmt = accCurrDtl[1];
            	  if(targetObj != undefined){
            		  targetObj.value =accCurrDtl[0];
            	  }
              }
       });
}

function fnDisplayAccNm()
{
	var accID = document.frmAccount.searchCbo_AccId.value;
	var txt_AccId = TRIM(document.frmAccount.Txt_AccId.value);
	var oldAccountId = document.frmAccount.hAccId.value;
	var ajaxUrl = '';
	// Get the account name if the account id is given in the text field
	if (txt_AccId != '' && txt_AccId != oldAccountId){
		//Call the function to get the account name using Ajax call
		fnCallAjax(txt_AccId);
			
		fnSetAccCurrSym(txt_AccId,document.frmAccount.Cbo_AccCurrId);
		document.getElementById("hAccId").value = txt_AccId;
		document.frmAccount.Cbo_AccId.value = txt_AccId;
	}
	//Cbo_AccId.DOMelem_input.focus();
	if(txt_AccId != '' && (isNaN(txt_AccId) || accID == txt_AccId))
	{
		document.frmAccount.searchCbo_AccId.value = '';
		document.frmAccount.Cbo_AccId.value = '';
		document.frmAccount.Txt_AccId.value = '';
		fnResetAccCurrSymb();
		Error_Details(message[5101]);
		Error_Show();
		Error_Clear();
		return false;
	}
}

function fnGo()
{
	// Disable Load Button on click
	document.frmAccount.LoadBtn.disabled = true;
	
	fnDisplayAccNm();
			
	// Validate text Fields and From Date Field
	var FrmDT = document.frmAccount.Txt_FromDate.value;
	var ToDT = document.frmAccount.Txt_ToDate.value;
	var CustPO = TRIM(document.frmAccount.Txt_CustPO.value);
	var parentFl  = document.frmAccount.Chk_ParentFl.checked;
	
	var OrdID = TRIM(document.frmAccount.Txt_OrdId.value);
	var ParentDOID = TRIM(document.frmAccount.Txt_ParentOrd.value);
	var InvID = TRIM(document.frmAccount.Txt_InvoiceId.value);
    var holdCbx = new Boolean(false);
	holdCbx = document.frmAccount.Chk_HoldFl.checked;

	var accID = document.all.searchCbo_AccId.value;
	var txt_AccId = TRIM(document.frmAccount.Txt_AccId.value);
	var searchAccIdObj = document.frmAccount.searchCbo_AccId;
	var searchRepIdObj = document.frmAccount.searchCbo_RepId;
	var searchOrdByObj = document.frmAccount.searchCbo_OrderBy;
	
	if(accID != '' && accID!= null && txt_AccId == ''){// If account id is not coming for the selected Account
		 Error_Details(message[5101]);
	}
	
    if (holdCbx)
    {
		// Clear all filters
		// Cbo_AccId is cleared in the servlet, others are cleared here
		document.frmAccount.Cbo_RepId.value = '';
		document.frmAccount.Txt_OrdId.value = '';
		document.frmAccount.Txt_FromDate.value = '';
		document.frmAccount.Txt_ToDate.value = '';
        document.frmAccount.Txt_CustPO.value = '';
		document.frmAccount.Txt_ParentOrd.value = '';
        document.frmAccount.Txt_ShipFromDate.value = '';
        document.frmAccount.Txt_ShipToDate.value = '';
		document.frmAccount.Txt_InvoiceId.value = '';
		document.frmAccount.Cbo_OrderBy.value = '';
		document.frmAccount.Txt_Total.value = '';
		document.frmAccount.Txt_Track.value = '';
		searchAccIdObj.value = '';
		searchRepIdObj.value = '';
		searchOrdByObj.value = '';
    } else {
		// Do other validations
		// check correct month format entered
    	CommonDateValidation(document.frmAccount.Txt_FromDate,format, message_operations[610]+format);
    	CommonDateValidation(document.frmAccount.Txt_ToDate,format,message_operations[611]+format);

		if (CustPO == '' && OrdID == '' && ParentDOID == '' && InvID == '' && FrmDT == '')
		{
			Error_Details(message[5001]);
		}

		if (CustPO != '' || OrdID != '' || ParentDOID != '' || InvID != '')
		{
			document.frmAccount.Txt_FromDate.value = '';
			FrmDT = '';
		}

		if (FrmDT != '' && ToDT == '')
		{
			Error_Details(message[5002]);
		}

		// Validate Order From/To Dates in proper order
		if (dateDiff(FrmDT, ToDT, format) < 0)
		{
			Error_Details(message[5003]);
		}

		// Validate Shipped From/To Dates in proper order
		FrmDT = document.frmAccount.Txt_ShipFromDate.value;
		ToDT = document.frmAccount.Txt_ShipToDate.value;
		if (FrmDT != '' && ToDT == '')
		{
			Error_Details(message[5004]);
		}
		// check correct month format entered
		CommonDateValidation(document.frmAccount.Txt_ShipFromDate,format,message_operations[721]+format);
		CommonDateValidation(document.frmAccount.Txt_ShipToDate,format,message_operations[722]+format);
		if (dateDiff(FrmDT, ToDT, format) < 0)
		{
			Error_Details(message[5005]);
		}
	}

	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		// Enable Load Button on error
	    document.frmAccount.LoadBtn.disabled = false;
		return false;
	}
	document.getElementById("hAccNm").value = searchAccIdObj.value;
	document.getElementById("hRepNm").value = searchRepIdObj.value;
	document.getElementById("hOrderByNm").value = searchOrdByObj.value;
	document.frmAccount.action = "/GmOrderVoidServlet";
	document.frmAccount.hMode.value = "Reload";
	fnStartProgress();
	document.frmAccount.submit();
}

function fnLoad()
{
	var ovrideCurr = '';
	if(document.getElementById('hCurrSign'))
		ovrideCurr = document.getElementById('hCurrSign').value;
	
	currSign = (ovrideCurr != '' && ovrideCurr != null) ? ovrideCurr: currSign;
	var lblTotal = document.getElementById('label').innerHTML;
	
	document.getElementById('label').innerHTML  = lblTotal+currSign;
	// Enable Load Button on load
	document.frmAccount.LoadBtn.disabled = false;
	
	fnDisplayAccID("reload");
	
	var objThis = document.frmAccount.Cbo_Action1;
	if (document.frmAccount.hAction.value != '')
	{
		for (var i=0;i<objThis.length ;i++ )
		{
			if (document.frmAccount.hAction.value == objThis.options[i].value)
			{
				objThis.options[i].selected = true;
				break;
			}
		}
	}

    var objThis2 = document.frmAccount.Cbo_OrderBy;
	for (var i=0;i<objThis2.length ;i++ )
	{
		if (objThis2.options[i].value == 9999)
		{
			objThis2.options[i].disabled = true;
		}
	}
	if(parAccfl == 'false'){
		document.frmAccount.Chk_ParentFl.checked = false;
	}
}

function fnPrintPack(val)
{
	windowOpener(servletPath+'/GmEditOrderServlet?hMode=PrintPack&hOrdId='+val,"Pack","resizable=yes,scrollbars=yes,top=150,left=150,width=760,height=610");	
}
function fnPrintCreditMemo(val)
{
	windowOpener(servletPath+'/GmPrintCreditServlet?hAction=PRINT&hOrderId='+val,"Credit","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnPrintCashAdjustMemo(val)
{
	windowOpener(servletPath+'/GmReportCreditsServlet?hAction=PrintCashAdj&hOrderId='+val,"Credit","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnPrintPick(val)
{
	windowOpener(servletPath+'/GmOrderItemServlet?hAction=PICSlip&hId='+val,"Pack","resizable=yes,scrollbars=yes,top=150,left=150,width=760,height=610");	
}

function fnOpenTrack(val,carrier)
{
	if(carrier == '5001'){ //fedex
		windowOpener('https://www.fedex.com/fedextrack?ascend_header=1&clienttype=dotcom&cntry_code=us&language=english&tracknumbers='+val,"Track","resizable=yes,scrollbars=yes,top=150,left=150,width=760,height=610");
	}
}

function fnOpenOrdLog(id)
{
	windowOpener("/GmCommonLogServlet?hType=1200&hID="+id,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
}

function fnOpenInvLog(id)
{
	windowOpener("/GmCommonLogServlet?hType=1201&hID="+id,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
}

function fnViewOrder(val)
{	
	windowOpener('/GmEditOrderServlet?hMode=PrintPrice&hOrdId='+val,"Pack","resizable=yes,scrollbars=yes,top=250,left=250,width=965,height=450");	
}

function fnPrintInvoice(varinv)
{
windowOpener('/GmInvoiceInfoServlet?hAction=Print&hInv='+varinv,"PrntInv","resizable=yes,scrollbars=yes,top=150,left=200,width=750,height=500");
}

function changeTRBgColor(val,BgColor) {
   // Get this Row obj
   var objThis = eval("document.all.tr"+val);
   var objPrev = null;

   if (clickCnt == 0)
   {
	   // First row select, save this rows properties
       prevtr = val;
       if (val%2 == 0)
       {
          prevColor = '#FFFFFF'; // evenShade (Blank)
       } else {
          prevColor = '#E3EFFF'; // oddShade (Shaded)
 	   }
   } else {
	   // All other row selects, reset previous row
       objPrev = eval("document.all.tr"+prevtr);
       objPrev.style.background = prevColor;

	   // save this rows properties
       prevtr = val;
       if (val%2 == 0)
       {
          prevColor = '#FFFFFF'; // evenShade (Blank)
       } else {
          prevColor = '#E3EFFF'; // oddShade (Shaded)
 	   }
   }

   // Set click counter to 1 = clicked at least once
   clickCnt = 1;
   // Shade this Row
   objThis.style.background = BgColor;
}
// this function used to export the files
function fnExportFile(fileID){
	var uploadDir="BATCH_INVOICE";
	document.frmAccount.action = "/GmCommonFileOpenServlet?sId="+fileID+"&uploadString="+uploadDir;
	document.frmAccount.submit();
}

//Ajax call to get the account name
function fnCallAjax(txt_AccId){
	var xmlDoc;
	var pnum;
 	var acname = '';
	var url = fnAjaxURLAppendCmpInfo("/GmOrderAjaxServlet?opt=GETACC" + "&acc=" + encodeURIComponent(txt_AccId));
	if (typeof XMLHttpRequest != "undefined") 
	{
	   req = new XMLHttpRequest();
	} 
	else if (window.ActiveXObject) 
	{
	   req = new ActiveXObject("Microsoft.XMLHTTP");
	}
	req.open("GET", url, true);
	
	req.onreadystatechange = function(){
		if (req.readyState == 4) {
	        if (req.status == 200) {
		        xmlDoc = req.responseXML;
		        pnum = xmlDoc.getElementsByTagName("acc");
		    	for (var x=0; x<pnum.length; x++){
		    		acname = parseXmlNode(pnum[x].childNodes[9].firstChild);
		    	}
		    	if(acname != null && acname != ''){
					document.frmAccount.searchCbo_AccId.value = acname;
				}else{
					// Need to throw validation if the account id is invalid
					document.frmAccount.searchCbo_AccId.value = '';
					Error_Details(message[5101]);
					Error_Show();
					Error_Clear();
					return false;
				}
	        }
	    }
	}
	req.send(null);
}

function fnViewDODocument(strOrdId){
	if(strOrdId !=''){
		  w4 =  createDhtmlxWindow(1000,400,true);
		  w4.setText("Upload DO Document Information");
		  var ajaxUrl = fnAjaxURLAppendCmpInfo("/gmDODocumentUpload.do?method=loadOrderUpload&strOpt=Load&haction=View&uploadTypeId=26240412&strOrderId="+strOrdId);
		  w4.attachURL(ajaxUrl,"Upload Files");	  
		  return false;	
}

}

