var gridObj = '';
var div_rowId = '';
var type_rowId = '';

function fnOnLoad(){
	
	 gridObj = initGrid('dataGridDiv',objGridData); 
}

function addRow(){
	var uid = gridObj.getUID();
	gridObj.addRow(uid,'');
	gridObj.setRowAttribute(uid,"row_added","Y");
}

function removeSelectedRow(){
	
	var gridrows=gridObj.getSelectedRowId();
	var isExistRow=false;
	if(gridrows!=undefined){

		var gridrowsarr = gridrows.toString().split(",");
		var added_row;
		for (var rowid = 0; rowid < gridrowsarr.length; rowid++) {
			gridrowid = gridrowsarr[rowid];
			added_row = gridObj.getRowAttribute(gridrowid,"row_added");
			if(added_row!=undefined && added_row=="Y"){
				gridObj.deleteRow(gridrowid);
			}else{
				isExistRow=true;
			}
		}
		if(isExistRow){
			Error_Clear();
			Error_Details(message[501]);				
			Error_Show();
			Error_Clear();
			
		}
		gridObj.clearSelection();
	}else{
		Error_Clear();
		Error_Details(message[502]);				
		Error_Show();
		Error_Clear();
	}

}


function fnSubmit()
{
	fnValidateTxtFld('Txt_FirstNm',lblFirstName);
	fnValidateTxtFld('Txt_LastNm',lblLastName);
	
	if(document.frmVendor.Txt_RepNm_En!=undefined ){
		fnValidateTxtFld('Txt_RepNm_En',message_operations[725]);
	}
	
	fnValidateTxtFld('Cbo_DistId',message_operations[451]);
	fnValidateDropDn('Cbo_CatId',lblCategory);
	fnValidateTxtFld('Txt_StDate',lblStartDate);
	objStartDt = document.frmVendor.Txt_StDate;
	objEndDt = document.frmVendor.Txt_EndDate;
	CommonDateValidation(objStartDt,format, message_operations[458]+ message[611]);
	CommonDateValidation(objEndDt,format, message_operations[459]+ message[611]);
	
	if(document.frmVendor.Cbo_CatId.disabled==false && document.frmVendor.Cbo_CatId.value == 4021){
		  	Error_Details(message_operations[452]);
			Error_Show();
			Error_Clear();
			return false;
	}
	if(strAccess == 'Y'){
		if(document.frmVendor.Chk_Login_Cr.checked){
			document.frmVendor.Chk_Login_Cr.value = 'Y';
		}
		if(document.frmVendor.Chk_Login_Cr.checked ){
			fnValidateTxtFld('UserName',lblUserName);
		}
	}
	if( document.frmVendor.hAction.value == 'Edit' ) 
	{
	   fnValidateTxtFld('Txt_LogReason',message_operations[457]);		
	}
	if(document.frmVendor.Cbo_Dsgn.disabled)
	{
		document.frmVendor.Cbo_Dsgn.value = '0';
	}
	else
	{
		fnValidateDropDn('Cbo_Dsgn',lblDesignation);
	}
	
	
	if (ErrorCount > 0)
		{
			Error_Show();
			Error_Clear();
			return false;
		}	
	

	if (document.frmVendor.Chk_ActiveFl.checked)
	{
		document.frmVendor.Chk_ActiveFl.value = 'Y';
	}else if(eval(document.frmVendor.hQuotaCnt.value) > 0){
		alert(message_operations[453]);
	}
	
	var repid = document.frmVendor.Cbo_RepId.value;
	div_rowId = gridObj.getColIndexById("divid");
    type_rowId = gridObj.getColIndexById("type");
	var gridrows = gridObj.getAllRowIds(",");

	if((gridrows==undefined || gridrows==null || gridrows=="") || count == 0) {
		Error_Clear();
		Error_Details(message_operations[454]);				
		Error_Show();
		Error_Clear();
	}else{

	gridObj.editStop();
	var gridrowsarr = gridrows.toString().split(",");
	var arrLen = gridrowsarr.length;
	var strInputString = '';
	var count = 0;
	var cnt = 0;
	var cnt1 = 0;
	var INR_count=0;
	var trauma_count=0;
	var jom_count=0;
	var errorFl = false;
	var primaryDivId = "";
	
	for (var i=0;i<arrLen;i++)
		{
	        rowId = gridrowsarr[i];
	        divid = gridObj.cellById(rowId,div_rowId).getValue();
	        type = gridObj.cellById(rowId,type_rowId).getValue();
	        
	        if(divid == '' || type == ''){
	        	errorFl = true;
	        }
	        // 2000 -Spine, 2001 - Algea, 2002 - TTOT,  2004 - I-N-ROBOTICS, 2005 - Trauma,2006- JOM
	        // 105442 - Primary , 105443 - Secondary. 
	        // Adding the I-N-Robotics division id in the validation.
	        // Trauma division Addition PMT: 14096 Author: gpalani Date: 09/11/2017
	        
	        if(type == '105442'){
	        	count = count+1;
	        }
	        
	        if(((divid == '2000') || (divid == '2002')) && (type == '105442' || type == '105443')){
	        	cnt = cnt + 1; 
	        }
	        
	        if((divid == '2001') && (type == '105442' || type == '105443')){
	        	cnt1 = cnt1 + 1; 
	        }
	        
	        if((divid == '2004') && (type == '105442' || type == '105443')){
	        	INR_count = INR_count + 1; 
	        }
	        
	      //  Trauma division Addition PMT: 14096 Author: gpalani Date: 09/11/2017
	        
	        if((divid == '2005') && (type == '105442' || type == '105443')){
	        	trauma_count = trauma_count + 1; 
	        }
	        
	        if((divid == '2006') && (type == '105442' || type == '105443')){
	        	jom_count = jom_count + 1; 
	        }



	        if(type == '105442'){
	        	
	        	primaryDivId = divid;
	        }
	        
	        
			strInputString = strInputString+divid+'^'+type+'|';
		}

		if( count == 0 || errorFl == true) {
			Error_Clear();
		    Error_Details(message_operations[454]);
			Error_Show();
			Error_Clear();
			
			// Below Validation is to check the same division filter is not selected for more than one type (Primary and Secondary)
			
		}else if(cnt > 1 || cnt1 > 1||INR_count>1||trauma_count>1||jom_count>1){
			Error_Clear();
			Error_Details(message_operations[455]);
			Error_Show();
			Error_Clear();
			
			
		}else if(count > 1){
			Error_Clear();
			Error_Details(message_operations[454]);	
			Error_Show();
			Error_Clear();
	    }else{
	    	
	    document.frmVendor.hPrimaryDivID.value = primaryDivId;
		document.frmVendor.hRepDivInputStr.value = strInputString;
		document.frmVendor.Cbo_CatId.disabled = false;
		fnStartProgress();
		document.frmVendor.submit();
		}
	}
	
}

function fnReset()
{
	document.frmVendor.hAction.value = "Load";
	document.frmVendor.submit();
}

function fnLoadRep()
{
		document.all.hAction.value = "EditLoad";
		fnStartProgress();
		document.frmVendor.submit();
	
}


function fnOpenAddress()
{
	val = document.frmVendor.hPartyId.value;
	if (document.frmVendor.Cbo_RepId.value==0){	
		fnValidateDropDn('Cbo_RepId',message_operations[456]);
	if (ErrorCount > 0)
		{
			Error_Show();
			Error_Clear();
			return false;
		}
	}
	else
	{
		windowOpener('/GmPageControllerServlet?strPgToLoad=gmAddressSetup.do&method=loadAddressSetup&includedPage=false&partyId='+val,"PO2","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=650");
	}
}

function fnOpenContact()
{
	val = document.frmVendor.hPartyId.value;
	if (document.frmVendor.Cbo_RepId.value==0)
	{	
		fnValidateDropDn('Cbo_RepId',lblSalesRep);
		if (ErrorCount > 0)
		{
			Error_Show();
			Error_Clear();
			return false;
		}
	}
	else
	{	
		windowOpener('/GmPageControllerServlet?strPgToLoad=gmContactSetup.do?method=loadContactSetup&includedPage=false&partyId='+val,"PO2","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=650");
	}
}

function fnShowFilters(val)
{
	document.getElementById("DivShowUNMExists").style.display='none';	
    document.getElementById("DivShowUNMYes").style.display='none';
    document.getElementById("processimg").style.display='none';
	var obj = eval("document.all."+val);
	var obj1 = eval("document.all."+val+"img");
	if (obj.style.display == 'none')
	{
		obj.style.display = 'block';
		if(obj1){
			obj1.src = '/images/minus.gif';
		}
	}
	else
	{
		obj.style.display = 'none';	
		if(obj1){
			obj1.src = '/images/plus.gif';
		}
	}
}

function fnEnableDesg()
{
	if(strLoginChecked.length > 0 && strAccess == 'Y'){
		fnShowFilters('tabRecv');
		document.frmVendor.UserName.disabled = true;
		document.frmVendor.Chk_Login_Cr.disabled = true;
	}
	obj = eval(document.frmVendor.Cbo_CatId);
	if(obj.value == 0 || obj.value != 4021)
	{
		document.frmVendor.Cbo_Dsgn.disabled = "disabled";
		document.frmVendor.Cbo_Dsgn.value = '0';
	}
	if(document.frmVendor.Cbo_RepId.value!= 0 && obj.value == 4021 && strAccess == 'Y')
	{
		document.frmVendor.UserName.disabled = true;
		document.frmVendor.Chk_Login_Cr.disabled = true;
	}
}
function fnLoadUserName(){
	fnClearDiv();
	var username = TRIM(document.getElementById("UserName").value.toLowerCase());
	if(username!=''){
		document.getElementById("processimg").style.display='block';	
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmUserListAction.do?userLoginName='+username+'&strOpt=checkusernm&ramdomId='+Math.random());
		dhtmlxAjax.get(ajaxUrl,fnShowUserName);
	}	
}

function fnShowUserName(loader){
	var username = TRIM(document.getElementById("UserName").value.toLowerCase());	
	   var response = loader.xmlDoc.responseText;
	   	if(username == response){
	    	 document.getElementById("DivShowUNMExists").style.display = 'block';
	    	 document.getElementById("UserName").value = "";
	   	}else{
	    	 document.getElementById("DivShowUNMYes").style.display = 'block';	
	   	}
	   document.getElementById("processimg").style.display = 'none';
}

function fnClearDiv(){
	 document.getElementById("DivShowUNMExists").style.display = 'none';	
	 document.getElementById("DivShowUNMYes").style.display = 'none';	
	 document.getElementById("processimg").style.display = 'none';
}

function fnDisableCategory(){	
	obj = eval(document.frmVendor.Cbo_CatId);
	if(obj.value == 4021)
	{
		document.frmVendor.Cbo_CatId.disabled = true;
		if(strAccess == 'Y'){
			document.frmVendor.UserName.disabled = true;
			document.frmVendor.Chk_Login_Cr.disabled = true;
		}
	}
	
}

function fnchkDist(){
	if(document.frmVendor.searchCbo_DistId.value == ''){
		document.frmVendor.Cbo_DistId.value ='';
	}
}