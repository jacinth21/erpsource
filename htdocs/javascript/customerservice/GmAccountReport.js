/**
 * 
 */

// this function used to load the DHTMLX data (on page load)
function fnOnPageLoad() {
	if (parentFl == 'false') {
		document.frmAccount.Chk_ParentFl.checked = false;
	}
	if (objGridData != '') {
		gridObj = initGridWithDistributedParsing('GridData', objGridData);
		if (strOpt == 'ACCTRPT') {
			if (arRptByDealerFlag == 'YES') {
				gridObj
						.attachHeader("#text_filter,#text_filter,#text_filter,#text_filter,#select_filter,#select_filter,#select_filter,#select_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter");
			} else {
				gridObj
						.attachHeader("#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#select_filter,#select_filter,#select_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter");
			}

		}
		gridObj.enableHeaderMenu();
		// Hide Ship to Zip Code Column by default.
		// gridObj.setColumnHidden(7,true);
	}
}

// This function used to reload the page (when click the Load button)
function fnSubmit() {
	if (strOpt == 'ACCTRPT') {
		var parentFl = document.frmAccount.Chk_ParentFl.checked;
		document.frmAccount.hAction.value = 'Reload';
	}
	fnReloadSales();
}

// This function used to load the account details
function fnViewDetails(hAccountId) {

	windowOpener("/GmAccountServlet?hAction=Reload&hideButton=True&hAccountId="
			+ hAccountId, "AccountSetup",
			"resizable=yes,scrollbars=yes,top=300,left=300,width=750,height=600");
}
// This function used to load/set the account id text box (when change the Rep
// account text box)
function fnDisplayAccID(obj) {
	var accId = document.frmAccount.Cbo_AccId.value;
	if (accId != '') {
		document.frmAccount.Txt_AccId.value = accId;
		fnSetAccCurrSym(accId, document.frmAccount.Cbo_Comp_Curr);
	} else {
		fnResetAccCurrSymb();
		document.frmAccount.Txt_AccId.value = '';
	}
}
// This function used to load/set the account name (when change the Rep account
// id text box)
function fnDisplayAccNm(obj) {
	document.frmAccount.Cbo_AccId.value = TRIM(obj.value);
	var accId = document.frmAccount.Cbo_AccId.value;
	if (accId != '' && isNaN(accId) != true) {
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/GmOrderAjaxServlet?opt=GETACC&acc='
				+ encodeURIComponent(accId) + '&ramdomId=' + Math.random());
		dhtmlxAjax.get(ajaxUrl, fnLoadAccAjax);

		fnSetAccCurrSym(accId, document.frmAccount.Cbo_Comp_Curr);
	} else {
		fnResetAccCurrSymb();
	}

	if (accId == "" || isNaN(accId) == true) {
		Error_Clear();
		Error_Details(message_operations[702]);
	}
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}

}
// to reset the Account currency symbol
function fnResetAccCurrSymb() {
	var accCurrObj = document.frmAccount.Cbo_Comp_Curr;
	console.log(' compCurrSign '+compCurrSign);
	
	for (var i = 0; i < accCurrObj.options.length; i++) {
		if (accCurrObj.options[i].value == compCurrSign) {
			accCurrObj.options[i].value = compCurrSign;
			accCurrObj.options[i].selected = true;
			return;
		}
	}
}

// to fetch the currency symbol based on the account and set to currency drop
// down.
function fnSetAccCurrSym(accID, targetObj) {
	var dhxAccCurrUrl = fnAjaxURLAppendCmpInfo('/GmOrderAjaxServlet?opt=GETACCTCURR&acc='
			+ accID + '&ramdomId=' + Math.random());
	dhtmlxAjax.get(dhxAccCurrUrl, function(loader) {
		var accCurrDtl;
		var response = loader.xmlDoc.responseText;
		if ((response != null || response != '')) {
			accCurrDtl = response.split("|");
			// currencyFmt = accCurrDtl[1];
			if (targetObj != undefined) {
				targetObj.value = accCurrDtl[0];
			}
		}
	});
}

// Description : Load the account name information
function fnLoadAccAjax(loader) {
	var errFlag = false;
	var xmlDoc;
	var pnum;
	var acname = '';

	var response = loader.xmlDoc.responseXML;
	if (response != null) {
		pnum = response.getElementsByTagName("acc");
		for (var x = 0; x < pnum.length; x++) {
			acname = parseXmlNode(pnum[x].childNodes[9].firstChild);
		}

		if (acname != null && acname != '') {
			document.frmAccount.searchCbo_AccId.value = acname;
		} else {
			document.frmAccount.searchCbo_AccId.value = '';
			document.frmAccount.Cbo_AccId.value = '';
			fnResetAccCurrSymb();
			Error_Details(message_operations[702]);
		}
	}

	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}

}