var ctrlId;
function fnAddRow(val, pnum, usageId, ddtNum, itemOrderId, orderInfoId, usedLotOrderId,prodMat) {
	count++;

	var hcnt = parseInt(eval("document.all.hTextboxCnt").value);
	hcnt = hcnt + count;

	var hCnumFl = eval("document.all.hCnumFl"+val).value;	
	var NewRemoveHtml = "<span border=1 id='IconRemove"
			+ hcnt
			+ "' align='top'><BR>&nbsp;&nbsp; <input type='hidden' name='hPnum"
			+ val
			+ "' value='"
			+ pnum
			+ "'  id='hPnum"
			+ hcnt
			+ "'/> <input type='hidden' name='hPartNum"
			+ hcnt
			+ "' id='hPartNum"
			+ hcnt
			+ "' value='"
			+ pnum
			+ "' /><a href=javascript:fnDelete("
			+ hcnt
			+ ");><img src=/images/btn_remove.gif border=0 height=13 width=13 align='right' valign='top' ></a> </span>";
	var QtyObj11 = eval("document.all.CellRemove" + val);
	QtyObj11.insertAdjacentHTML("beforeEnd", NewRemoveHtml);

	var currQty = eval("document.all.hQty" + val);
	var currQtyVal = currQty.value;
	var NewQtyHtml = "<span border=1 id='RmQty"
			+ hcnt
			+ "'><BR>&nbsp;<input type='text' maxlength='2' size='2' value='1' name='qtyTxt"
			+ hcnt
			+ "' id='qtyTxt"
			+ hcnt
			+ "' class='InputArea' onFocus=changeBgColor(this,'#AACCE8'); onBlur=changeBgColor(this,'#ffffff'); ></span>";
	var QtyObj = eval("document.all.Qty" + val);
	QtyObj.insertAdjacentHTML("beforeEnd", NewQtyHtml);

	var NewControlNumHtml = "<span border=1 id='RmControl" + hcnt
			+ "'><BR>&nbsp;<input type='text' maxlength=\'40\' onFocus=fnRemoveImage("+hcnt+",this);changeBgColor(this,'#AACCE8'); onBlur=fnAjaxValidateCtrlNum("+hcnt+",this);changeBgColor(this,'#ffffff') name='controlNumTxt"
			+ hcnt + "' value='' size='25' id='controlNumId" + hcnt
			+ "'> <input type='hidden' name='hCnumFl"
			+ hcnt
			+ "' id='hCnumFl"
			+ hcnt
			+ "' value='"
			+ hCnumFl
			+ "' /> "
			+" <input type='hidden' name='hUsageDetailsId"
				+ hcnt
				+ "' id='hUsageDetailsId"
				+ hcnt
				+ "' value='' />" 
				+" <input type='hidden' name='hDDT"
				+ hcnt
				+ "' id='hDDT"
				+ hcnt
				+ "' value='"
				+ ddtNum
				+ "' />" 
				+" <input type='hidden' name='hItemOrderId"
				+ hcnt
				+ "' id='hItemOrderId"
				+ hcnt
				+ "' value='"
				+ itemOrderId
				+ "' /> <input type='hidden' name='hOrderInfoId"
				+ hcnt
				+ "' id='hOrderInfoId"
				+ hcnt
				+ "' value='"
				+ orderInfoId
				+ "' /> <input type='hidden' name='hUsedLotOrderId"
				+ hcnt
				+ "' id='hUsedLotOrderId"
				+ hcnt
				+ "' value='"
				+ usedLotOrderId
				+ "' /> <input type='hidden' name='hProdMat"
				+ hcnt
				+ "' id='hProdMat"
				+ hcnt
				+ "' value='"
				+ prodMat
				+ "' /> </span><span id='DivShowCnumAvl"+hcnt+"' style='vertical-align:middle; display: none;'>  <img height=16 width=19 title=Control number available src=/images/success.gif></img> </span>"
			+"<span id='DivShowCnumNotExists"+hcnt+"' style=' vertical-align:middle; display: none;'>  <img height=12 width=12 title=Control number does not exists src=/images/delete.gif></img></img>&nbsp;<a href=javascript:fnViewLotReport("+hcnt+")><img height=15 border=0 title=\'Lot Code Report\' src=/images/location.png></img></a> </span>";
	var ControlNumObj = eval("document.all.controlNum" + val);
	ControlNumObj.insertAdjacentHTML("beforeEnd", NewControlNumHtml);

}

function fnDelete(val) {
	obj4 = eval("document.all.qtyTxt" + val);
	obj4.value = '0';

	obj1 = eval("document.all.controlNumId" + val);
	obj1.value = '';
}

function fnDecresQty(val) {
	var objCurrQty = eval("document.all.qtyTxt" + val);
	var currQty = objCurrQty.value;
	if (currQty > 0) {
		eval("document.all.qtyTxt" + val).value = eval(currQty) - 1;
	}
}

function fnIncrQty(val, maxQty) {
	var objCurrQty = eval("document.all.qtyTxt" + val);
	var currQty = objCurrQty.value;
	if (currQty < maxQty) {
		eval("document.all.qtyTxt" + val).value = eval(currQty) + 1;
	}
}

// When submit to check control number is valid for sterile parts. If it is
// valide then submit, otherwise rule engine fire.
function fnValidateCtrlNum(screen) {
	checkItemQty();

	if (document.all.Btn_Save)
		document.all.Btn_Save.disabled = true;

	var TRcnt = parseInt(eval("document.all.hTextboxCnt").value);
	TRcnt = TRcnt + count;
	var contrlNumStr = '';
	var contrlNumQtyStr = '';
	var pnum = '';
	var emptyCnumStr = '';
	var moreQtyStr = '';
	var prodMat = ''; //ProdMat for Tissue Lot number validation used in PC-4659
	for (i = 1; i <= TRcnt; i++) {
		var partCnum = '';
		var objControlNum = eval("document.all.controlNumTxt" + i);
		var objPnum = eval("document.all.hPartNum" + i);
		var objQty = eval("document.all.qtyTxt" + i);
		var objCnumFl =eval("document.all.hCnumFl" + i);
		var objhCnumFl =eval("document.all.hcontrolnum" + i);
		var hCnum = (objhCnumFl != undefined) ? objhCnumFl.value : '';
		var objQtyFl =eval("document.all.qtyTxt" + i);
		var pnum = (objPnum != undefined) ? objPnum.value : '';
		var cntNum = (objControlNum != undefined) ? trimString(objControlNum.value)
				: '';
		var qty = (objQtyFl != undefined) ? objQtyFl.value: '';
		prodMat = eval("document.all.hProdMat" + i).value;
	   
		//Validating the More qty only for the Tissue parts
		if (objQty.value > 1 && objControlNum.value != '' && objCnumFl.value == 'Y') {
			partCnum = trimString(objPnum.value) + '@'
					+ objControlNum.value.toUpperCase();
			moreQtyStr = moreQtyStr + partCnum + ',';
		}

		if (objCnumFl.value == 'Y' && objControlNum.value == '' && objQty.value > 0) {
			emptyCnumStr = emptyCnumStr + objPnum.value + ',';
	
		} else {
			if (cntNum != '' && pnum != '')
				contrlNumStr = contrlNumStr + pnum + ',' + cntNum + '|';
				if(cntNum != hCnum){
					contrlNumQtyStr = contrlNumQtyStr + pnum + ',' + cntNum +','+qty+ '|';
				}
		}
	}
	if (screen == 'EditControl' || screen == 'EditOrder') {

		if (emptyCnumStr.length > 1 && skpUsdLotValid != 'YES') {
	
			Error_Details(Error_Details_Trans(message[5612],emptyCnumStr.substring(0, emptyCnumStr.length - 1)));
		}

              /* Validation for Handling the Tissue parts with more than one qty */
              if (moreQtyStr.length > 1) {
                     moreQtyStr = moreQtyStr.substring(0, moreQtyStr.length - 1);
                     var partCnumArray = moreQtyStr.split(",");
                     var lotNums = '';
                     for (var j = 0; j < partCnumArray.length; j++) {
                           var partArray = partCnumArray[j].split("@");
                           lotNums = lotNums + partArray[1] + ',';
                     }
                     var indexs = lotNums.lastIndexOf(",");
                     lotNums = lotNums.substring(0, indexs)
                                  + lotNums.substring(indexs + 1);
                   //ProdMat for Tissue Lot number validation used in PC-4659
                     if(prodMat=='100845'){
                       Error_Details(Error_Details_Trans(message[5613],lotNums));
                     }
              }
       }
       if (ErrorCount > 0) {
    	   if(screen == 'EditControl'){
    	   	  // Should not enable the save button for switch user
              if(document.frmDOSummaryEdit.Btn_Save && SwitchUserFl !='Y'){
                     document.frmDOSummaryEdit.Btn_Save.disabled = false;
              }
    	   }
              Error_Show();
              Error_Clear();
              return false;
       }

	fnCallAjax('validateControlNumber', contrlNumStr, contrlNumQtyStr);
}
// Return error message if not valid control number.
function fnCallAjax(val, cntrlStr, contrlNumQtyStr) {
	
	var accid = '';
	var repid = '';
	var orderId = (document.all.hOrdId) ? "" : document.all.hOrdId.value;
	var url = fnAjaxURLAppendCmpInfo("/GmOrderAjaxServlet?orderType=2521&submitFl=Y&opt=" + encodeURIComponent(val)
			+ "&PartNums=" + encodeURIComponent(cntrlStr)+"&PartNumwithQty=" + encodeURIComponent(contrlNumQtyStr))+"&doid="+orderId;
	if (typeof XMLHttpRequest != "undefined") {
		req = new XMLHttpRequest();
	} else if (window.ActiveXObject) {
		req = new ActiveXObject("Microsoft.XMLHTTP");
	}
	req.open("GET", url, true);
	req.onreadystatechange = callCtrlNum;

	req.send(null);
}

function callCtrlNum() {
	if (req.readyState == 4) {
		if (req.status == 200) {
			var xmlDoc = req.responseText;
			if (trimString(xmlDoc) != '') {
				Error_Details(xmlDoc);
				Error_Show();
				Error_Clear();
				if (document.all.Btn_Save)
					document.all.Btn_Save.disabled = false;
				return false;
			} else {
				var inputStr = createInputString();
				if (document.all.Btn_Save)
					fnSave(inputStr);
				if (document.all.Btn_Submit)
					fnSubmit(inputStr);
			}
		}
	}
}

function checkItemQty() {
	// Item Qty Variables
	var TRcnt = parseInt(document.all.hTextboxCnt.value);
	TRcnt = TRcnt + count;

	var pnum = '';
	var iqty = 0;

	// throw error if item qty greater than attribute qty.
	var totqty = 0;
	var errMsg = false;
	var strPartNums = '';
	var strNtvQtyPrts = '';
	var greaterQtyPartStr = "";

	for (j = 0; j < orderSize; j++) {
		arr = eval("ItemQtyArr" + j);
		arrobj = arr.split(",");
		pnum = arrobj[0];
		iqty = arrobj[1];
		totqty = 0;

		for (k = 1; k <= TRcnt; k++) {
			var objPnum = eval("document.all.hPartNum" + k);
			var objQty = eval("document.all.qtyTxt" + k);
			if(showUsageLotFl ='Y'){
				// to declare the local variable.
				var UsageId = eval("document.all.hUsageDetailsId" + k);
				var tmpDDTval = eval("document.all.hDDT" + k);
				var tmpQtyval = eval("document.all.hLotQty" + (j+1));
				var tmpUsageId = eval("document.all.hTmpUsageDetailsId" + (j+1));
			}

			if (objQty.value != '' && objQty.value < 0) {
				if (strNtvQtyPrts.indexOf(objPnum.value) == -1)
					strNtvQtyPrts = strNtvQtyPrts + objPnum.value;
				continue;
			}
			if ((objPnum.value != '' && objQty.value != '')
					&& (pnum == objPnum.value)) {
				totqty = totqty + parseInt(objQty.value);
			}
			// validate only usage lot enabled company
			if(showUsageLotFl ='Y'){
				// to validate the entered greater then - current Qty
				if((pnum == objPnum.value) && (UsageId.value == tmpUsageId.value && tmpDDTval.value !='' && tmpQtyval.value < objQty.value) ){
					if(greaterQtyPartStr.indexOf(objPnum.value) == -1){
						greaterQtyPartStr = greaterQtyPartStr + objPnum.value +', ';
					}
				}
			} // end if showUsageLotFl
		}
		if ((totqty != iqty) && (strPartNums.indexOf(pnum) == -1)) {
			errMsg = true;
			strPartNums = strPartNums + "<br> <b>Part Number:</b> " + pnum
					+ ", <b>Order Qty:</b> " + iqty + ", <b>Entered Qty:</b> "
					+ totqty;
		}
	}
	if (strNtvQtyPrts != '') {
		Error_Details(Error_Details_Trans(message[5614],strNtvQtyPrts));
	}
	if (errMsg) {
		Error_Details(Error_Details_Trans(message[5615],strPartNums));
	}
	
	if(greaterQtyPartStr !=''){
		greaterQtyPartStr = greaterQtyPartStr.substring(0,(greaterQtyPartStr.length)-2);
		Error_Details(Error_Details_Trans(message_operations[253],greaterQtyPartStr));
	}
}
function createInputString() {
	var TRcnt = parseInt(document.all.hTextboxCnt.value);
	TRcnt = TRcnt + count;
	var inputStr = '';

	for (i = 1; i <= TRcnt; i++) {
		var voidfl = 'N';
		var pkey = '';
		var objPkey = eval("document.all.hPkey" + i);
		var objQty = eval("document.all.qtyTxt" + i);
		var objControlNum = eval("document.all.controlNumTxt" + i);
		var objPnum = eval("document.all.hPartNum" + i);
		var objVoidFl = eval("document.all.chkVoidOrder" + i);
		var objUsageId = eval("document.all.hUsageDetailsId" + i);
		var objDDT = eval("document.all.hDDT" + i);
		var objItemOrderId = eval("document.all.hItemOrderId" + i);
		var objOrderInfoId = eval("document.all.hOrderInfoId" + i);
		var objUsedLotOrderId = eval("document.all.hUsedLotOrderId" + i);
		
		var v_attr_type = '101723';// The Attribute Type is Added for MDO-109.
		// The control number attribute type need to
		// be saved in Order item Attribute Table
		// when Order raised from portal.
		var qty = (objQty != undefined) ? trimString(objQty.value) : '';
		var pnum = (objPnum != undefined) ? trimString(objPnum.value) : '';
		var cntNum = (objControlNum != undefined) ? trimString(objControlNum.value)
				: '';
		cntNum = cntNum.toUpperCase();
		if (objPkey != undefined && objPkey.value != '') {
			pkey = objPkey.value;
		}

		if (objVoidFl != undefined && objVoidFl.checked) {
			voidfl = 'Y';
		}

		if (qty != '' && qty > 0) {
			/*
			 * The Existing I/P String is Changed for MDO-109.Additional
			 * Parameter is added for Control Number attribute Type. inputStr +=
			 * voidfl+'^'+pkey+'^'+pnum+'^'+ qty+'^'+cntNum+'|';
			 */
			inputStr += objUsageId.value + '^' + pnum + '^' + qty + '^'
					+ cntNum + '^' + objDDT.value +'^'+ objUsedLotOrderId.value + '^'+ objOrderInfoId.value+ '|';
		} else if (qty == '') {
			Error_Details(Error_Details_Trans(message[5616],pnum));
		}
	}
	return inputStr;
}

// MNTTASK-8623 - When create order enter part number with space, getting
// exception, Use trim function.
function trimString(obj) {
	if (obj != undefined && obj != '') {
		return obj.replace(/^\s*|\s*$/g, "");
	}
	return obj;
}
function fnShowFilters(val) {
	var obj = eval("document.all." + val);
	var obj1 = eval("document.all." + val + "img");
	if (obj.style.display == 'none') {
		obj.style.display = 'block';
		if (obj1) {
			obj1.src = '/images/minus.gif';
		}
	} else {
		obj.style.display = 'none';
		if (obj1) {
			obj1.src = '/images/plus.gif';
		}
	}
}

//When tab out in control number textbox, call rule engine fire to show control number valid/invalid icon.
function fnAjaxValidateCtrlNum(trcnt,obj){	
	var pnumobj = eval("document.all.hPartNum"+trcnt);
	var cnumobj = eval("document.all.controlNumTxt"+trcnt);
	if( trimString(cnumobj.value) != '')
	fnValidateCtrlNumber('validateControlNumber',trimString(pnumobj.value),trimString(encodeURIComponent(cnumobj.value)),trcnt);
}
//Ajax call to get the control number details and to show the valid\invalid icon
function fnValidateCtrlNumber(val,pnum,cnum,trcnt)
{

	ctrlId = trcnt;
	
	var submitFl = 'N';
	var hcontrolnumobj = eval("document.all.hcontrolnum"+trcnt);
	var hcontrolnum = (hcontrolnumobj != undefined) ? hcontrolnumobj.value :'';
	if(hcontrolnum != cnum){
		submitFl= 'Y';	
	}

	var url = fnAjaxURLAppendCmpInfo("/GmOrderAjaxServlet?cnum="+encodeURIComponent(cnum)+"&pnum="+encodeURIComponent(pnum)+"&Editdoid="+ordid+"&RE_AJAX_WRAPPER=AJAXWRAPPER&opt=" + encodeURIComponent(val)+"&submitFl="+submitFl);
	
	dhtmlxAjax.get(url,function(loader){
		
		var response = loader.xmlDoc.responseText; 
		
		if((trimString(response) != '') ){
			var txtObj = document.getElementById("DivShowCnumNotExists"+ctrlId);			
			txtObj.style.display='inline-block';
		}else{
		    var txtObj = document.getElementById("DivShowCnumAvl"+ctrlId);			
		    txtObj.style.display='inline-block';
		}
	 });

}

function fnRemoveImage(index)
{
		var imgObj = document.getElementById("DivShowCnumAvl"+index);
		imgObj.style.display='none';

		var imgObj1 = document.getElementById("DivShowCnumNotExists"+index);			
		imgObj1.style.display='none';			
}
//Function to open the Lot Code Report Screen by click on the Lot Number
function fnViewLotReport(cnt){
	var lotNum = eval("document.all.controlNumId" + cnt).value;
	windowOpener(" /gmLotCodeRptAction.do?method=loadLotCodeProdInfo&hscreen=popup&lotNum="+lotNum,"LotCOdeReport","resizable=yes,scrollbars=yes,top=150,left=150,width=1020,height=600");
}