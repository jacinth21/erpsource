function fnGo()
{
	
		var varTstType = document.frmTransfer.Cbo_TransferType.value;
		var varTsfFrom = document.frmTransfer.hTransferFrom.value;
		var varTsfTo = document.frmTransfer.Cbo_TransferTo.value;
		var varSet = document.frmTransfer.Cbo_Set.value;

		if (varTstType == '0')
		{
			Error_Details(message[715]);
		}
		
		if (varTsfFrom == "undefined" || varTsfFrom == '')
		{
			Error_Details(message[716]);
		}
		
		 if (varTsfTo == '0')
		{
			Error_Details(message[717]);
		}
		
		if (varSet =='0' || varSet == '')
		{
			Error_Details(message[724]);
		}
		
		if (varTsfFrom == varTsfTo)
		{
				Error_Details(message[726]);
		}
		
		if (ErrorCount > 0)
	 {
					Error_Show();
					Error_Clear();
					return false;
		}
		
	else {			
		document.frmTransfer.hAction.value = "loadSetTransferInfo";
		document.frmTransfer.hOpt.value = "ViewSetTransfer";
		fnStartProgress();
		document.frmTransfer.submit();	
		}
}

// function to view details of part
function fnViewPartActual(val)
{
	var distId = document.frmTransfer.hTransferFrom.value;
	
	document.frmTransfer.hPartID.value = val;
	document.frmTransfer.Cbo_DistId.value = distId;
	document.frmTransfer.hOpt.value = 'BYPARTDETAIL';
	
	document.frmTransfer.action ="<%=strServletPath%>/GmSalesConsignSearchServlet";
	document.frmTransfer.hAction.value = "Reload";
	document.frmTransfer.submit();
	
}

function fnInitiateSetTransfer()
{
// Verify if the Tsftype, from and To are the same
	var varTstType = document.frmTransfer.Cbo_TransferType.value;	
	var varTsfFrom = document.frmTransfer.hTransferFrom.value;
	var varTsfTo = document.frmTransfer.Cbo_TransferTo.value;
	var varSetId = document.frmTransfer.Cbo_Set.value;
	var varReason = document.frmTransfer.Cbo_TransferReason.value;
	
	var varTsfTypeLoad = document.frmTransfer.hTransferType.value;
	var varTsfFromLoad = document.frmTransfer.hTransferFromLoad.value;
	var varTsfToLoad = document.frmTransfer.hTransferTo.value;
	var varSetIdLoad = document.frmTransfer.hSetId.value;
	
	var varcomments = document.frmTransfer.Txt_LogReason.value;
	
		if (varTstType != varTsfTypeLoad)
	{
		 Error_Details(message[711]);
	}
	
		if (varTsfFrom != varTsfFromLoad)
	{
		 Error_Details(message[712]);
	}
	
		if (varTsfTo != varTsfToLoad)
	{
		 Error_Details(message[713]);
	}
	
	if (varSetId != varSetIdLoad)
	{
		 Error_Details(message[718]);
	}
	
		if (varcomments == '')
	{
				Error_Details(message[714]);
	}
	
		if (varReason == '0')
	{
				Error_Details(message[723]);
	}
	
		if (varTsfFrom == varTsfTo)
	{
				Error_Details(message[726]);
	}
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	
	
	else {	
		CommonDateValidation(document.frmTransfer.Txt_Date,dateFmt, message[611]);	 

	var str ='';
	var theinputs=document.getElementsByTagName('input');
	var len = theinputs.length;
	var checkAll = document.frmTransfer.Chk_SelectAll;
	var negTransPnum = '';
	var errFl = false;
	var nonNumQty = '';

			for(var n=0;n<len;n++)
			{
					objpart = eval("document.frmTransfer.rad"+n); // The checkboxes would be like rad0, rad1 and so on
					
					if(objpart)
					{			
						objtsf = eval("document.frmTransfer.Txt_TsfQty"+n); // The checkboxes would be like rad0, rad1 and so on
						part = objpart.value; // value would the CN or RA #
						qtyonhand = parseInt(objpart.id);
						tsfqty = objtsf.value;
						tsfqtyint = parseInt(objtsf.value);
							if (objpart.checked)
							{
								// check the any speacial character.
								nonNumQty = tsfqty.replace(/-?[0-9]+/g,''); // Transfer Qty text box
								if (varTstType != 90300 && varTstType != 26240616){ // Skip the execess qty validation for the Distributor to Distributor and Missing to Distributor type. 
										if (qtyonhand <  tsfqtyint)	{
												 errFl = true; 
												 Error_Details(message[710]);
												 break;
										}
										
										if (qtyonhand == 0)
										 {
											 errFl = true;
											 Error_Details(message[720]);
										 } 
								} 
								if (tsfqty == 0)
								{
									 errFl = true;	
									 Error_Details(message[721]);
								 }else if(tsfqtyint <0 || nonNumQty.length >0){
									 errFl = true;
									 negTransPnum += part +', ';
								 }
								 
								else { 
										str = str +  part +'^'+tsfqty+ '|'; 
								}
							}
						}
					}
						if (!errFl &&(str == '' || str == null))
					 {
					 		Error_Details(message[719]);
					 }

			 	if (str.length >= 3900)
					 {
					 		Error_Details(message[722]);
					 }
				 if(negTransPnum !=''){
					 negTransPnum = negTransPnum.substring(0,(negTransPnum.length)-2);
					 Error_Details(Error_Details_Trans(message[10003],negTransPnum));
					 }
				document.frmTransfer.hAction.value = "saveTransfer";
				document.frmTransfer.hInputStr.value = str;
					fnSubmit();	
		       	}
}

function fnSubmit()
{
if (ErrorCount > 0)
	{
			document.frmTransfer.hInputStr.value = '';
			Error_Show();
			Error_Clear();
			return false;
	}
	
			confirmed = window.confirm(message[10001]);
			if (confirmed)
			{
					fnStartProgress();
					document.frmTransfer.submit();	
			} 
}

function fnOnCheckRad()
{
	objId = eval("document.frmTransfer.Chk_SelectAll");
	objId.checked = false;
}

function fnLoad()
{
	   	if (val != '')
		{
			var obj = eval("document.all.div"+val);
			document.all.idFrom.innerHTML = obj.innerHTML;
		}
		
		else 
		{
				document.frmTransfer.Cbo_TransferType.value = "90300";
				fnSetFromValues("90300");
		}
}

function fnSelectAll(obj)
{
	var theinputs=document.getElementsByTagName('input');
	var len = theinputs.length;

	for(var n=0;n<len;n++)
	{
			objId = eval("document.frmTransfer.rad"+n); // The checkboxes would be like rad0, rad1 and so on
			if(objId)
			{			
				if(obj.checked)
				{
					objId.checked = true;
				}else
				{
					objId.checked = false;
				}
			}
	 }		
}