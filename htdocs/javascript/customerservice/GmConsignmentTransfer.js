
var strMsg = '';
var strFlag = '';
var partno_row_id='';
var partnoVal='';
var Qty_rowId='';
var qty_val='';
var transfer_qty_rowID = '';
var cnum_rowId='';
var cnum_val='';
//Onpage load Disable approve and grid 
function fnOnPageLoad() {
	document.frmConsignmentTransfer.transferType.value='26240577';
		gridObj = initGridData('dataGridDiv', objGridData);
		gridObj.attachHeader("<input type='checkbox' value='no'  name='selectAll' onClick='javascript:fnChangedFilter(this);'/>"
				+',#text_filter,#text_filter,#text_filter,#numeric_filter,,');	
		gridObj.attachEvent("onCheckbox", doOnCheck);
		check_rowId = gridObj.getColIndexById("check");
		part_rowID = gridObj.getColIndexById("pnum");
		Qty_rowId = gridObj.getColIndexById("qty");
		cnum_rowId = gridObj.getColIndexById("lot_num");
		transfer_qty_rowID = gridObj.getColIndexById("transfer_qty");
		gridObj.checkAll(true);
}

//This function is called while initiating the grid
function initGridData(divRef,gridData)
{
	var gObj = new dhtmlXGridObject(divRef);
		gObj.setSkin("dhx_skyblue");
		gObj.setImagePath("/extweb/dhtmlx/dhtmlxGrid/imgs/");
		gObj.enableMultiline(false);	
		gObj.init();
		gObj.loadXMLString(gridData);
	return gObj;	
}
// Function is used to Check Box Check
function doOnCheck(rowId, cellInd, stage) {
	rowID = rowId;
	if (cellInd == check_rowId) {
		fnSelectAllToggle('selectAll');
	}
	return true;
}

// Description : This function to check the select all check box
function fnSelectAllToggle(varControl) {
	var objControl = eval("document.frmConsignmentTransfer." + varControl);

	var all_row = gridObj.getAllRowIds(",");
	var all_rowLen = all_row.length; // get the rows id length
	var checked_row = gridObj.getCheckedRows(check_rowId);
	var finalval;

	finalval = eval(checked_row.length);
	if (all_rowLen == finalval) {
		objControl.checked = true;// select all check box to be checked
	} else {
		objControl.checked = false;// select all check box un checked.
	}
}

//this function used to select all option (check box)
function fnChangedFilter(obj) {
	
	if (obj.checked) {
		gridObj.forEachRow(function(rowId) {
			var check_disable_fl = gridObj.cellById(rowId, check_rowId).isDisabled();
			if (!check_disable_fl) {
				gridObj.cellById(rowId, check_rowId).setChecked(true);
			}
		});
	} else {
		gridObj.forEachRow(function(rowId) {
			check_id = gridObj.cellById(rowId, check_rowId).getValue();
			
			if (check_id == 1) {
				gridObj.cellById(rowId, check_rowId).setChecked(false);
			}

		});
	}
}



//This function used to press the enter key then submit the page.
function fnEnter() {
	if (event.keyCode == 13) {	
		fnLoad();
	 }		
}
//Function used to validate consignment
function fnValidateConsignment(strConsignmentId,form){
	if(strConsignmentId!=''){
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmConsignmentTransfer.do?method=validateConsignment&consignmentId='+encodeURIComponent(strConsignmentId)+'&ramdomId=' + Math.random());
		dhtmlxAjax.get(ajaxUrl,function(loader)
				{
					var response = loader.xmlDoc.responseText;
					var b = response;
					 var temp = new Array();
					 temp = b.split('##');
					    strFlag = temp[0];
						strMsg = temp[1];
					if(strFlag=='Y'){
						document.all.setSuccimg.style.display='none';
						document.getElementById("dataConsingmentInfo").style.display = 'none';
						if (document.getElementById("setERRimg"))
							document.all.setERRimg.style.display='none';
         					document.all.setErrimg.style.display='inline';
         					Error_Details(strMsg);
         					if (ErrorCount > 0) {
         						Error_Show();
         						Error_Clear();
         						return false;
         					}
					}else{
						document.all.setSuccimg.style.display='inline';
						if (document.getElementById("setERRimg"))
						document.all.setERRimg.style.display='none';
						document.all.setErrimg.style.display='none';
						fnGetConsignmentDtls(strMsg);
					}
				});
		} else{
			strMsg='';
			document.getElementById("setErrimg").style.display='none';
			document.getElementById("setSuccimg").style.display='none';
		}
}
//Get Consignment Details
function fnGetConsignmentDtls(strMsg){
	var accountName = '';
	var created_by = '';
	var created_date = '';
	var request_id = '';
	var shipped_date = '';
	var strMsgJson = '[' +strMsg+ ']';
	if(strMsgJson != ""){
		var rows = [], i=0;
		var JSON_Array_obj = "";
		 JSON_Array_obj = JSON.parse(strMsgJson);
		 $.each(JSON_Array_obj, function(index,jsonObject){
			 accountName = jsonObject.accountName;
			 document.frmConsignmentTransfer.transferFrom.value = jsonObject.account_id;
			 created_by = jsonObject.createdby;
			 created_date = jsonObject.created_date;
			 request_id = jsonObject.requestid;
			 shipped_date = jsonObject.shipped_date; 
			 //default array
			 rows[i] = {};
			 rows[i].id=i;
			 rows[i].data=[];
			 rows[i].data[0]=accountName;
			 rows[i].data[1]=request_id;
			 rows[i].data[2]=shipped_date;
			 rows[i].data[3]=created_by;
			 rows[i].data[4]=created_date;
			 i++;
			 
		 });
			var setInitWidths = "*,100,100,100,100";
		    var setColAlign = "left,left,left,left,left";
		    var setColTypes = "ro,ro,ro,ro,ro";
		    var setColSorting = ",str,str,str,str,str";
		    var setHeader = [lblAccNm , lblReqId, lblShippedDt, lblCreatedBy, lblCreatedDate];
		    var setFilter = "";
		    var setColIds = "";
		    var enableTooltips = "true,true,true,true,true";
		    var footerArry = [];
		    var gridHeight = "";
		    var footerStyles = [];
		    var footerExportFL = true;
		    var dataHost = {};
		    var pagination ="";
		    //dataHost.head = setHeader;
		    dataHost.rows = rows;
		   // console.log ('data host len '+ dataHost.rows.length);
			document.getElementById("dataConsingmentInfo").style.display = 'block';
			gridObj = loadDHTMLXGrid('dataConsingmentInfo', gridHeight, dataHost, setHeader, setInitWidths, setColAlign, setColTypes, setColSorting, setColIds, enableTooltips, setFilter, footerArry, footerStyles, footerExportFL,pagination);
			}else{
				document.getElementById("dataConsingmentInfo").style.display = 'none';
			}
		}
//This Function used to click Load Button	
function fnLoad(){

	var account_type = document.frmConsignmentTransfer.transferType.value;
	var consignment_id = document.frmConsignmentTransfer.consignmentId.value;
	var account_id = document.frmConsignmentTransfer.transferTo.value;
	var reason_id = document.frmConsignmentTransfer.transferReason.value;
	var obj_transfer_date = document.frmConsignmentTransfer.date;
	var transfer_date = obj_transfer_date.value;
	var transfer_from = document.frmConsignmentTransfer.transferFrom.value;

	if(strFlag == 'Y'){
		Error_Details(strMsg);
	}

   fnValidateTxtFld('consignmentId',lblFrom);
   fnValidateTxtFld('transferTo',lblTo);
   fnValidateDropDn('transferReason',lblReason);
   
   if(transfer_date == ''){
	   Error_Details(Error_Details_Trans(message[5681],lblDate));

	}
   CommonDateValidation(obj_transfer_date, format, Error_Details_Trans(message[10002],format));
   if((account_id == transfer_from)&& account_id!=''){
		Error_Details(message[10602]);
	}
   
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	fnStartProgress('Y');
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmConsignmentTransfer.do?method=loadConsignmentDtls&consignmentId='+consignment_id+'&ramdomId='+new Date().getTime());
	dhtmlxAjax.get(ajaxUrl, fillConsignmentDetails);

	document.frmConsignmentTransfer.hTransferType.value = account_type;
	document.frmConsignmentTransfer.hTransferFromLoad.value = consignment_id;
	document.frmConsignmentTransfer.hTransferTo.value = account_id;
	document.frmConsignmentTransfer.hTransferFrom.value = transfer_from;
}

function fillConsignmentDetails(loader){
	var response = loader.xmlDoc.responseText;
	if(response!= null)
	 {   
		      document.getElementById("trDiv").style.display = "table-row";
		      fnStopProgress();
		      gridObj = initGridData('dataGridDiv',response); 
		      gridObj.attachHeader("<input type='checkbox' value='no'  name='selectAll' onClick='javascript:fnChangedFilter(this);'/>"
						+',#text_filter,#text_filter,#text_filter,#numeric_filter,,');
			   gridObj.attachEvent("onCheckbox", doOnCheck);
				check_rowId = gridObj.getColIndexById("check");
				part_rowID = gridObj.getColIndexById("pnum");
				Qty_rowId = gridObj.getColIndexById("qty");
				cnum_rowId = gridObj.getColIndexById("lot_num");
				transfer_qty_rowID = gridObj.getColIndexById("transfer_qty");
				gridObj.checkAll(true);
		      document.getElementById("trComments").style.display = "table-row";
		      document.getElementById("Initiate").style.display = "table-row";
		      
	 }
}
//This Function used to click Initiate Button
function fnConsignmentInitiateTransfer(){
	// Verify if the Tsftype, from and To are the same
	var varTstType = document.frmConsignmentTransfer.transferType.value;
	var varTsfFrom = document.frmConsignmentTransfer.consignmentId.value;
	var varTsfTo = document.frmConsignmentTransfer.transferTo.value;
	var varReason = document.frmConsignmentTransfer.transferReason.value;
	var obj_transfer_date = document.frmConsignmentTransfer.date;
	var transfer_date = obj_transfer_date.value;	

	var varTsfTypeLoad = document.frmConsignmentTransfer.hTransferType.value;
	var varTsfFromLoad = document.frmConsignmentTransfer.hTransferFromLoad.value;
	var varTsfToLoad = document.frmConsignmentTransfer.hTransferTo.value;
	var varcomments = document.frmConsignmentTransfer.txt_LogReason.value;
		
	if(strFlag == 'Y'){
		Error_Details(strMsg);
	}
	
	if (varTstType != varTsfTypeLoad)
	{
		 Error_Details(message[711]);
	}
	
		if (varTsfFrom != varTsfFromLoad)
	{
		 Error_Details(message[10603]);
	}
	
		if (varTsfTo != varTsfToLoad)
	{
		 Error_Details(message[713]);
	}
	
    	
	fnValidateDropDn('transferReason',lblReason);
	
		if (varTsfFrom == varTsfTo)
	{
				Error_Details(message[726]);
	}
    if(transfer_date == ''){
	 Error_Details(Error_Details_Trans(message[5681],lblDate));
	 }
	CommonDateValidation(obj_transfer_date, format, Error_Details_Trans(message[10002],format));	
	
//Loop Grid and form string	
var inputStr='';
var nonNumQty = '';
var negativeQty='';
var greaterQty='';
//close opened editor and return value from editor to the cell
gridObj.editStop();

var gridChrows = gridObj.getChangedRows(',');

//get list of changed rows included added rows
var checked_row = gridObj.getCheckedRows(check_rowId);
if(checked_row == '')
{ 
  Error_Details(message[10604]); 
} 

if(checked_row !=''){
	var chkVal = checked_row.split(",");
			for (var i=0;i<chkVal.length;i++){
				//rowId = rowsarr[rowid];
				transfer_qty_val = gridObj.cellById(chkVal[i], transfer_qty_rowID).getValue();
				partNum = gridObj.cellById(chkVal[i], part_rowID).getValue();
				qty_val = gridObj.cellById(chkVal[i], Qty_rowId).getValue();
				cnum_val = gridObj.cellById(chkVal[i], cnum_rowId).getValue();
				strQty = transfer_qty_val.replace(/-?[0-9]+/g, ''); // Qty text boxs
				if(isNaN(transfer_qty_val) || strQty.length > 0){
					if(nonNumQty.indexOf(partNum) == -1){
						nonNumQty = nonNumQty + partNum + ', ';
					}
			
				}else{
					if(parseInt(qty_val) < parseInt(transfer_qty_val) || parseInt(transfer_qty_val) <= 0){
						greaterQty = greaterQty + partNum + ', ';
					}		
				}
				
				inputStr=inputStr+partNum+'^'+cnum_val+'^'+transfer_qty_val+'|';
				
			}
	}



    if(nonNumQty != ''){
    	nonNumQty = nonNumQty.substring(0, nonNumQty.length - 2);
		Error_Details(Error_Details_Trans(message[10605],nonNumQty));
    }
	
	if(greaterQty!=''){
       	nonNumQty = nonNumQty.substring(0, nonNumQty.length - 2);
       	Error_Details(message[10606]);
     }
    //
	// validate the Comments
	fnValidateTxtFld('txt_LogReason',message[5120]);
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	
	if (confirm(message[10001])) {	
	document.frmConsignmentTransfer.transferType.disabled = false;
	document.frmConsignmentTransfer.inputStr.value = inputStr;
	fnStartProgress('Y');
	document.frmConsignmentTransfer.action = "/gmConsignmentTransfer.do?method=saveConsignmentTransfer";
	document.frmConsignmentTransfer.submit();
	}
}