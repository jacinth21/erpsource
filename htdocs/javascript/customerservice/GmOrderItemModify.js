function fnSubmit()
{
	var emptyRtnQty = 0;
	
	
	if (document.frmVendor.hAction.value == 'EditPrice')
	{		
		NumberValidation(document.frmVendor.Txt_SCost.value, message[5632], 2);
		for (var i=0;i<intSize;i++)
		{	
	 		validateDecimals(document.getElementById("hPrice"+i).value,message[5633], 2, "-0123456789,");
	 		if (document.getElementById("hPrice"+i).value ==""){
	 			document.getElementById("hPrice"+i).value="0.00";
	 		}
		}
		fnValidateTxtFld('Txt_LogReason',message[5550]);
		if (ErrorCount > 0)
		{
				Error_Show();
				Error_Clear();
				return false;
		}
		document.frmVendor.hAction.value = 'SavePrice';
		document.frmVendor.submit();
	}
	else if (document.frmVendor.hAction.value == 'ReturnPart')
	{
		var str = '';
		var strLoaner = '';
		var strConsignment = '';
		var vPNum = 0;
		var vQty = '';
		var vCNum = " ";
		var vItemType = '';
		var cnt = document.frmVendor.hCnt.value;
		var reason = document.frmVendor.Cbo_Reason.value;
		var invFl = document.frmVendor.hInvFl.value;
		var retcnt = document.frmVendor.hReturnCnt.value;
		var previousPart="";
		var previousQty="";
		var partString = new Array();
		var j=0;
		var usedLotStr = '';
		var dupPartChk =0;
		var dupPartStr = '';
		var totalPartCnt=0;
		keyMapArray = [];
		valMapArray = [];
		
		if (invFl == '')
		{
			Error_Details(message[5006]);
		}
		
		if (reason == 3311 || reason == 3313 || reason == 3314)
		{
			Error_Details(message[5007]);
		}
		
		if (reason == 3312 && retcnt != '0')
		{
			Error_Details(message[5008]);
		}
				
		for (var i=0;i<cnt;i++)
		{
			var obj = eval("document.frmVendor.hRetQty"+i);
			var objCon = eval("document.frmVendor.hContNum"+i);
			var objItemType = eval("document.frmVendor.hItemType"+i);
			// get the Part Qty.
			var objPartQty = eval("document.frmVendor.hQty"+i);
			var objPartNum = eval("document.frmVendor.hPNum"+i);
			var objPartNumVal = objPartNum.value;
			var objReturnedQty = eval("document.frmVendor.hReturnedQty"+i);
			
			var objQtyVal = TRIM(obj.value);
			
			var totalReturned = '';
			var objPartPrice = eval("document.frmVendor.hDOPrice"+i);
			var v_itemPrice = objPartPrice.value;
			var objUnitPrice = eval("document.frmVendor.hUnitPrice"+i);
			var v_unitPrice = objUnitPrice.value;
			var objUnitPriceAdj = eval("document.frmVendor.hUnitPriceAdj"+i);
			var v_unitPriceAdj = objUnitPriceAdj.value;
			var objAdjCode = eval("document.frmVendor.hAdjCode"+i);
			var v_adjCode = objAdjCode.value;
			if (objQtyVal != '')
			{
				vItemType = objItemType.value;
				if ((reason == 3316 && vItemType == 50301) || (reason == 3317 && vItemType == 50300))
				{
					Error_Details(message[5009]);
				}
				
				
				//Validate the Qty. equal to zero.
				if (objQtyVal == 0){
					Error_Details(Error_Details_Trans(message[5010],objPartNumVal)+" - "+ message[728]); // Return Qty cannot be zero
				}
				//Validate the Non Numeric char
				//Below code are removed for PC-4237: Error displayed for Sales orders with decimal in Order quantity
				/*var strQty = objQtyVal.replace(/[0-9]+/g,''); // Qty text box
				if(strQty.length >0){
					Error_Details(Error_Details_Trans(message[5011],objPartNumVal));
				}*/
				totalReturned = parseFloat(objReturnedQty.value) + parseFloat(objQtyVal);
				//Validate the return Qty.
				if (objPartQty.value < totalReturned ){
					Error_Details(Error_Details_Trans(message[5011],objPartNumVal));
				}
				vQty = obj.value;
				vCNum = objCon.value;
				/* we added vOrgCNum because in procedure we have 6 values in input string but here we have 5 only.
				 * so we added one more to update the Item Type properly.*/
				var vOrgCNum = vCNum;
				val = eval("document.frmVendor.hPNum"+i);
				vPNum = val.value;
				// to split the Loaner and Consignment inputstring
				if(vItemType == '50300'){
					strConsignment = strConsignment + vPNum+","+vQty+","+vOrgCNum+","+vCNum+","+vItemType+","+v_itemPrice+","+v_unitPrice.replace(',', '')+","+v_unitPriceAdj+","+v_adjCode+"|";
				}
				if(vItemType == '50301'){
					strLoaner = strLoaner + vPNum+","+vQty+","+vOrgCNum+","+vCNum+","+vItemType+","+v_itemPrice+","+v_unitPrice.replace(',', '')+","+v_unitPriceAdj+","+v_adjCode+"|";	
				}
				// to comment the code - Lot validation handle (PMT-44679)
				/*
				if(usedLotFl == 'Y' && objQtyVal >0 ){
					
					
						if(i==0){
							dupPartChk=1;
							 partString[j]=objPartNumVal+'^'+parseInt(objQtyVal);
							 previousQty=parseInt(objQtyVal);
							 previousPart=objPartNumVal;
							}else if(objPartNumVal==previousPart){
								previousQty=previousQty+parseInt(objQtyVal);
								partString[j]=objPartNumVal+'^'+previousQty;
								// if same part repeated , increment the duppartchk
								dupPartChk++;
							}else{
								j++;
							if(partString[0] == undefined){
								j--;
							}
							if(dupPartChk>1){
							// adding repeated parts with repeated count
								putMapValue(previousPart,dupPartChk);
							}
								 partString[j]=objPartNumVal+'^'+parseInt(objQtyVal);
									previousPart=objPartNumVal;
									previousQty=parseInt(objQtyVal);
							// end of every part make duppartchk as zero
									dupPartChk=1;
									
							}
							
							// checks last part number is duplicated
						if( cnt == i+1){
							if(dupPartChk>1){
								putMapValue(previousPart,dupPartChk);
							}
						}
						
					}
					*/
				totalPartCnt++;
				
			} 
			if(objQtyVal == ''){
				emptyRtnQty = parseInt(emptyRtnQty)+ 1;				
			}
		}

		// to comment the code - Lot validation handle (PMT-44679)
		/*
		for (var k = 0; k < partString.length; k++) {
			
		    var QtyPartStr = partString[k];
		    var qtyPartArray = QtyPartStr.split('^');
   
		  usedLotStr += fnCreateUsedLotStr (qtyPartArray[0], qtyPartArray[1]);
	 
		  if(getMapValue(qtyPartArray[0]) !='' ){
		  dupPartStr = dupPartStr+qtyPartArray[0]+',';
		  }
		  
		    //Do something
		}
		*/

		str = strConsignment +"~"+ strLoaner;
		// Standard Validations	
		fnValidateDropDn('Cbo_Reason',message[5634]);
		fnValidateTxtFld('Txt_ExpDate',message[5635]);
		fnValidateTxtFld('Txt_Comments',message[5550]);
		document.frmVendor.hInputStr.value = str;
		document.frmVendor.hUsedLotStr.value = usedLotStr;
		var fromCurrDiff = dateDiff(currentdate,document.frmVendor.Txt_ExpDate.value, dtFormat);
		if(fromCurrDiff < 0){
			Error_Details(message[5012]);
		}		
		
		if (document.frmVendor.hInputStr.value == '')
		{
			Error_Details(message[5013]);
		}
		
		/*
		if(usedLotStr!='' &&  usedLotStr.split('|').length <= totalPartCnt){
			Error_Details(Error_Details_Trans(message[5294],dupPartStr.substr(0,dupPartStr.length-1)));
		}
		*/
		// used lot recored then validate the qty
		if(usedLotFl =='Y' && usageLotDataFl == 'Y'){
			fnValidateUsedLot ();
		}
		
		// If Duplicate Order is selected user should select the 
		// Parent Order Information
		//Remove the parent order id text field as mandatory if return reason is � Duplicate Order�,
		/*if (document.frmVendor.Cbo_Reason.value == '3312')
		{
			if (document.frmVendor.Txt_ParOrderID.value == '')
			{
				Error_Details(message[52]);
			}	
		}*/
	
		// Error message Epmty Qty to Return..
		if(emptyRtnQty == intSize){
			Error_Details(message[5014]);
		}
		
		if(reason == '3253' || reason == '26240385') {
			if(!(orderType == '' || orderTypevalue == 'Y')) {
				Error_Details(message[5308]);
			}	
		}
	
		
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	else
	{
		// start the progress (PMT-45387)
		fnStartProgress("Y");
		document.frmVendor.hAction.value = 'SaveSalesReturns';
		document.frmVendor.action = "GmReturnOrderServlet";	
	    document.frmVendor.submit();
	}
	}
}

function fnReasonChange()
{   var vQty = '';
	var cnt = '';
	
	// If the User selected the duplicate value 
	// Automatically assign the return value 
	cnt = document.frmVendor.hCnt.value;
	for (var i=0;i<cnt;i++)
	{
		val = eval("document.frmVendor.hRetQty"+i);
		if (document.frmVendor.Cbo_Reason.value != '3312')
		{
			
			val.disabled = false;
		}
		else
		{	var obj = eval("document.frmVendor.hQty"+i);
			if (obj.value != '')
			{	vQty = obj.value;
				val.value = vQty;
				val.disabled = true;
			}
		}
	}
	// If the User selected the duplicate value 
	// Automatically assign the Qty value 
	
	if(usedLotFl=='Y' && usageLotDataFl == 'Y'){
		
		if (document.frmVendor.Cbo_Reason.value == '3312')
		{
			// loop the grid and update the Qty
			gridObj.forEachRow(function(rowId) {
				var usageCurrQty_val = gridObj.cellById(rowId, usageQty_cellId).getValue(); // Curr. Qty
				// to set the qty 
				gridObj.cellById(rowId, returnQty_cellId).setValue(usageCurrQty_val);
			 });
		}
	}
	
}

function fnViewReturns(val)
{
	windowOpener('/GmPrintCreditServlet?hAction=VIEW&hRAID='+val,"Returns","resizable=yes,scrollbars=yes,top=150,left=200,width=660,height=400");
}

function fnOnLoad(){
	today.setDate(1); // setting the date as 1st of the month. this way we can chk if month is passed	
	if (deptId == '2020' && today > orderdt){//	A/R
		for (var i=0;i<intSize;i++)
		{			
	 		document.getElementById("hPrice"+i).readOnly = true;	
		}
	}
	

	var hActionVal = document.frmVendor.hAction.value;
	// used lot recored Flag and Initiate Returns then only show the Used lot section
	
	if (hActionVal == 'ReturnPart' && usedLotFl == 'Y') {
		// to fetch the used lot number details
		// parameter -  order id, part number, invoice id
		fnFetchOrderUsageDtls(parent_ord_id, '', '');
	}
}

/* Function to set the Resulted Net Unit Price for the Selected Adjustment Code Value.
 * Getting the Ajax Response.
 * Setting the Resulted values for the selected Row id of Net Unit Price Column.
 * Author: HReddi
 * */
function fnGetNetUnitPrice(rowId,obj,partNUmber,unitPrice,unitPriceAdj,itemID){
	var accountId =  document.getElementById("hAccountId").value;
	var orderId = document.all.hOrderId.value;
	var adjCodeId = obj.value;	
	partNUmber = partNUmber + "@"+ rowId;
	if(adjCodeId != '0'){
		dhtmlxAjax.get('/GmCartAjaxServlet?hAction=fetchNetUnitPrice&hAcctID='+accountId+'&hAdjCOdeID='+adjCodeId+'&hPartNumber='+encodeURIComponent(partNUmber)+'&hOrderId='+orderId+'&hItemID='+itemID+'&ramdomId='+Math.random(),fnSetTotalPrice);
	}else{
		document.getElementById("hPrice"+rowId).value = formatNumber(unitPrice-unitPriceAdj);
	}
}

/* Function to set the Resulted Net Unit Price for the Selected Adjustment Code Value.
 * Getting the Ajax Response.
 * Setting the Resulted values for the selected Row id of Net Unit Price Column.
 * Author: HReddi
 * */
function fnSetTotalPrice(loader){
	var responseAll   = loader.xmlDoc.responseXML;	
	var response = responseAll.getElementsByTagName("data")[0];	
	var netUnitPrice = parseXmlNode(responseAll.getElementsByTagName("unitPrice")[0].firstChild);	
	var strArray = netUnitPrice.split('@');
	document.getElementById("hPrice"+strArray[1]).value = formatNumber(strArray[0]);	
}

var dhxWins,w4;
function fnCallPrice(orderid){
	dhxWins = new dhtmlXWindows();
			if(orderid !=''){
				  w4 =  dhxWins.createWindow("w1", 20, 30, 600, 400);
				  var ajaxUrl = fnAjaxURLAppendCmpInfo("/GmOrderItemServlet?hAction=HELDORDERPRICEREQ&ramdomId="+Math.random()+fnAppendCompanyInfo()+'&hORDID='+orderid);
				  w4.attachURL(ajaxUrl,"Held Order Price Request");	
				  w4.setText(orderid);
		}
	}


//this function used to validate the returns qty Vs used Lot qty.
function fnValidateUsedLot (){
	var validateUsedLotStr = '';
	var bulkUsageLotStr = '';
	var objHiddenOrdQty = '';
	var orderQtyVal = '';
	var validDataFl = true;
	//empty the input string values
	document.frmVendor.hUsedLotStr.value = '';
	// reset the map values
	usedLotMap = new Map;
	returnsQtyMap = new Map;
	
	// to put all the parts to map (Returns details)
	fnPutReturnsQtyMap ();
	// to put all the parts to map (Used lot details)
	fnPutUsedLotQtyMap ();

	// compare the hash map qty
	validDataFl = fnCompareUsedLotQty ();
	
	if(validDataFl){
		// to form the input string
		fnCreateUsedLotInputStr ();
	}
	
	console.log ('Final string ' + document.getElementById('hUsedLotStr').value);

}

// this function used to put all the Returns qty.
function fnPutReturnsQtyMap (){
	// to get the order  cart size
	var cnt = document.frmVendor.hCnt.value;
	var orderId = document.frmVendor.hOrderId.value;
	var mapReturns_val = 0;
	
	for ( var i = 0; i < cnt; i++) {
		objPartNum = eval("document.frmVendor.hPNum" + i);
		objReturnQty = eval("document.frmVendor.hRetQty" + i);
		//
		partNum = objPartNum.value;
		returnQtyVal = TRIM(objReturnQty.value);
		
		// adding the qty
		if(returnQtyVal !=''){
			// to get the values from hashmap
			mapReturns_val = returnsQtyMap.get (partNum);
			
			if(mapReturns_val == undefined){
				mapReturns_val = parseInt(0);
			}
			// to add the returns qty values.
			mapReturns_val = mapReturns_val + parseInt(returnQtyVal);
			
			// set the values to map
			returnsQtyMap.set(partNum, mapReturns_val);
		}
	}
}
