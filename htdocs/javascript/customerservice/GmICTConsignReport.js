/**
 * 
 */
// this function used to on page load to show the grid data
function fnOnLoad() {
	if (objGridData != '') {
		gridObj = initGridData('ConsignIctRpt', objGridData);
		gridObj.enableTooltips("true,false,true,true,true,true,true");
		if (strOption == 'Sets') {
			gridObj.attachHeader("#select_filter,#text_filter,#text_filter,#select_filter,#select_filter,#select_filter,#text_filter");
		} else {
			gridObj.attachHeader("#select_filter,#text_filter,#select_filter,#select_filter");
		}
	}
}
// this function used to on page load to show the grid data
function initGridData(divRef, gridData) {
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.init();
	gObj.loadXMLString(gridData);
	return gObj;
}
// this function used to reload the data
function fnLoad() {
	// validate the Date format
	CommonDateValidation(document.frmAccount.Txt_FromDate,dateFmt,message[10574]+ message[611]);
	CommonDateValidation(document.frmAccount.Txt_ToDate,dateFmt,message[10575]+ message[611]);
	fnValidateTxtFld('Txt_FromDate', lblFrmDt);
	fnValidateTxtFld('Txt_ToDate', lblToDt);
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	
	var actionVal = 'Reload';
	if (strOption == 'Sets') {
		actionVal = 'Load'
	}
	document.frmAccount.hAction.value = actionVal;
	fnStartProgress();
	document.frmAccount.submit();
}

// this function used to print the ack form
function fnAgree(cnid) {
	windowOpener("/GmConsignSetServlet?hId=" + cnid + "&hAction=Ack&", "Ack",
			"resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}
// this function is used to show the print version
function fnPrintVer(cnid) {
	windowOpener("/GmConsignSetServlet?hAction=PrintVersion&hId=" + cnid,
			"Con",
			"resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}
// this function used to show the RFS
function fnReleaseForSale(cnid) {
	windowOpener('/GmProdReportServlet?hAction=Part&hId=0&hConsignID=' + cnid,
			"RFS",
			"resizable=yes,scrollbars=yes,top=250,left=100,width=1170,height=510");
}
// this function used to show the pack slip
function fnPackslip(cnid) {
	windowOpener("/GmConsignSetServlet?hAction=PackVersion&hId=" + cnid, "Pack",
			"resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}
//This function will call the press the enter button.
function fnEnter(){
	if (event.keyCode == 13){ 
		fnLoad();
	}
}
