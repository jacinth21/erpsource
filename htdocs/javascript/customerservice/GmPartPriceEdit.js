function fnSubmit()
{
	if (Flag == 'Acc'){
	validateDecimals(document.Main.Txt_Price.value,message[5188],2,'-0123456789');
	validateDecimals(document.Main.Txt_Discount.value,message[518],2,'0123456789');
	}
	if (ErrorCount > 0)
		 {
			Error_Show();
			Error_Clear();
			return false;
		}
	fnStartProgress();
	document.Main.submit();
}

// To reset the fields
function fnReset(form)
{
	if(Flag == 'Part'){
		form.Txt_LiPrice.value = '';
		form.Txt_LPrice.value = '';
		form.Txt_CPrice.value = '';
		form.Txt_EPrice.value = '';
	}else if(Flag == 'Acc'){
		form.Txt_Price.value = '';
		form.Txt_Discount.value = '';
	}
}