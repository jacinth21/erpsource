function fnOnPageLoad() {	
	if(gridObjData!="")
	{
		gridObj = initGrid('dataGridDiv',gridObjData);
		gridObj.setColumnHidden(2,true);
		gridObj.setColumnHidden(3,true);
	   	gridObj.enableTooltips("true,true,false,true,true,false");
		gridObj.enableMultiline(false);	
		gridObj.enableBlockSelection(true); 
		gridObj.attachEvent("onKeyPress", keyPressed);			
	}
}

function fnLoadHistory(colId,orderId)
{	
	windowOpener("/gmAuditTrail.do?auditId="+ colId +"&txnId="+ orderId,"","resizable=yes,scrollbars=yes,top=250,left=300,width=785,height=300,status=1");
}

function keyPressed(code, ctrl, shift) {
    if (code == 67 && ctrl) {      
        gridObj.setCSVDelimiter("\t");
        gridObj.copyBlockToClipboard();
    }
    
    if (code == 86 && ctrl) {
    	if(gridObj._selectionArea!=null){
			var colIndex = gridObj._selectionArea.LeftTopCol;
			var cb_columnName = '';
			
			if(colIndex!=undefined && colIndex==0){
				alert(message[5185]);
			}else{
				gridObj.pasteBlockFromClipboard();
			}
			gridObj._HideSelection();
		}else{
			alert(message[5186]);
		}
    }
	// This code is to block delete the content from the grid.
	if(code==46&&!ctrl&&!shift){
		
		if(gridObj._selectionArea!=null){
			var area=gridObj._selectionArea
			var leftTopCol=area.LeftTopCol;
			var leftTopRow=area.LeftTopRow;
			var rightBottomCol=area.RightBottomCol;
			var rightBottomRow=area.RightBottomRow;
			
			for (var i=leftTopRow; i<=rightBottomRow; i++){
				
				setRowAsModified(gridObj.getRowId(i),true);

				for (var j=leftTopCol; j<=rightBottomCol; j++){
					gridObj.cellByIndex(i,j).setValue("");
				}
			}
			gridObj._HideSelection();
		}
	}
    return true;
}