// This function used to submit the DDT details
function fnSubmit() {
	var emptyRtnQty = 0;
	var inputStr = "";
	var errorPart = "";
	var maxQtyPart = "";
	var minQtyPart = "";
	var greaterQtyPartStr = "";
	var addQty = 0;
	
	var pnum = '';
	var iqty = 0;

	// throw error if item qty greater than attribute qty.
	var totqty = 0;
	var errMsg = false;
	var strPartNums = '';
	var strNtvQtyPrts = '';
    var partNumStr='';

	fnValidateTxtFld('txt_LogReason',message_operations[468]);
	
	for (i = 0; i < actualSize; i++) {
		arr = eval("ItemQtyArr" + i);
		arrobj = arr.split(",");
		pnum = arrobj[0];
		iqty = arrobj[1];
		totqty = 0;

	/*for ( var i = 0; i < actualSize; i++) {
		var hUsageId = document.getElementById("hTmpItemUsageID" + i).value;
		var totlQty = document.getElementById("hQty" + i).value;
		var Partval = document.getElementById("hPnum" + i).value;*/
		addQty = 0;
		for ( var j = 0; j < intSize; j++) {
			if(document.getElementById("hPnum" + j) != undefined){
				var tmpPartval = document.getElementById("hPnum" + j).value;
				var tmpUsageId = document.getElementById("hTmpItemUsageID" + j).value;
				var DDTvalObj = (document.getElementById("ddtId" + j));
				var DDTval= DDTvalObj.options.value;
                //PC-836-DDT_Improvemnts
				var hDDTval = (document.getElementById("txt_DDT" + j)).value;
				var tmpQtyval = document.getElementById("hQty" + i).value;
				var tmpUsageLotval = document.getElementById("hUsageLot" + j).value;
				var usageIDVal = document.getElementById("hItemUsageID" + i).value;
				
				var txtQty = document.getElementById("txt_Qty" + j).value;
				txtQty = TRIM(txtQty);
				
				//Validate the location Qty 
				if(DDTval != '0'){
				var DDTText= DDTvalObj.options[DDTvalObj.selectedIndex].text;
				var indexsize=DDTText.lastIndexOf( ')' )-1;
				var indexFirst=DDTText.lastIndexOf( '(' )+1;
				var locQty = parseInt(DDTText.substr(indexFirst,indexsize));
				//PC-836-DDT_Improvemnts-check hidden ddt and test ddt not equal
                //if it is not equal throw validation
				if((pnum == tmpPartval) && parseInt(txtQty) > locQty && DDTval != hDDTval){
					if(partNumStr.indexOf(tmpPartval) == -1){
						partNumStr = partNumStr + tmpPartval+',';
					}
					
				}
				}
				// Validate the negative Qty
				if (txtQty != '' && txtQty < 0) {
					if (strNtvQtyPrts.indexOf(tmpPartval) == -1)
						strNtvQtyPrts = strNtvQtyPrts + tmpPartval;
					continue;
				}
				// added the total qty 
				if ((tmpPartval != '' && txtQty != '')
						&& (pnum == tmpPartval)) {
					totqty = totqty + parseInt(txtQty);
				}
				
				// to validate the entered greater then - current Qty
					if((pnum == tmpPartval) && (usageIDVal == tmpUsageId  && tmpUsageLotval !='' && tmpQtyval < txtQty) ){
						if(greaterQtyPartStr.indexOf(tmpPartval) == -1){
							greaterQtyPartStr = greaterQtyPartStr + tmpPartval +', ';
						}
					}
			}
		}
		// 2 max
		if ((totqty != iqty) && (strPartNums.indexOf(pnum) == -1)) {
			errMsg = true;
			strPartNums = strPartNums + message_operations[469] + pnum
					+ message_operations[470]+ iqty + message_operations[471]
					+ totqty;
		}
	}
	if (errorPart != '') {
		errorPart = errorPart.substring(0,(errorPart.length)-2);
		Error_Details(Error_Details_Trans(message_operations[444], errorPart));
	}

	if (strNtvQtyPrts != '') {
		Error_Details(Error_Details_Trans(message_operations[445],strNtvQtyPrts));
	}
	if (errMsg) {
		Error_Details(message_operations[446]+strPartNums);
	}
	
	if(greaterQtyPartStr !=''){
		greaterQtyPartStr = greaterQtyPartStr.substring(0,(greaterQtyPartStr.length)-2);
		Error_Details(Error_Details_Trans(message_operations[447],greaterQtyPartStr));
	}
	
	if(partNumStr != ''){
		partNumStr = partNumStr.substring(0,(partNumStr.length)-1);
		Error_Details(Error_Details_Trans(message_operations[428],partNumStr));
	}
	
	
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	// to form the input string
	var orderId = document.frmOrderDDTForm.orderId.value;
	for ( var i = 0; i < intSize; i++) {
		if(document.getElementById("hPnum" + i) != undefined){
			var hItemUsageId = document.getElementById("hItemUsageID" + i).value;
			var DDTvalObj = (document.getElementById("ddtId" + i));
			var DDTval= DDTvalObj.options.value;
			var hPartval = document.getElementById("hPnum" + i).value;
			var hQtyVal = TRIM(document.getElementById("txt_Qty" + i).value);
			var hUsageLotNum = document.getElementById("hUsageLot" + i).value;
			var hItemId = document.getElementById("hItemId" + i).value;
			var hInforIdNew = document.getElementById("hInfoId" + i).value;
			//
			
			inputStr = inputStr + hItemUsageId + '^' + hPartval + '^' + hQtyVal
					+ '^' + hUsageLotNum + '^' + DDTval +'^'+ ddtOrderId + '^' + hInforIdNew + '|';
		}
	}
	document.frmOrderDDTForm.action = '/gmOrderDDTAction.do?method=saveDDTNumber&inputStr='
			+ encodeURIComponent(inputStr);
	document.frmOrderDDTForm.inputStr.value = inputStr;
	fnStartProgress("Y");
	document.frmOrderDDTForm.submit();
}

// this function used to create the new rows
function fnAddRow(val, pnum, usedLot, itemId, usageId, infoId) {
	var hcnt = document.frmOrderDDTForm.hCnt.value;
	// to setting the tab index
	var indexId= 1;
	var NewHtml = "<span id='SpQty"
			+ hcnt
			+ "'><BR>&nbsp;<input type='text' size='3' value='' id='txt_Qty"
			+ hcnt
			+ "' name='txt_Qty"
			+ hcnt
			+ "' class='InputArea' onFocus=changeBgColor(this,'#AACCE8'); onBlur=\"changeBgColor(this,'#ffffff');\" tabindex='"+indexId+"'> <input type='hidden' id='hItemId"
			+ hcnt + "' name='hItemId" + hcnt + "' value='" + itemId
			+ "'><input type='hidden' id='hUsageLot" + hcnt
			+ "' name='hUsageLot" + hcnt + "' value='" + usedLot
			+ "'><input type='hidden' id='hItemUsageID" + hcnt
			+ "' name='hItemUsageID" + hcnt
			+ "' value=''><input type='hidden' id='hPnum" + hcnt
			+ "' name='hPnum" + hcnt + "' value='" + pnum
			+ "'><input type='hidden' id='hTmpItemUsageID" + hcnt
			+ "' name='hTmpItemUsageID" + hcnt + "' value='" + usageId
			+ "'><input type='hidden' id='hInfoId" +hcnt+ "' name='hInfoId" + hcnt + "' value='" + infoId
			+ "'>" +
					"<input type='hidden' id='hPnum" + hcnt
			+ "' name='hPnum" + hcnt + "' value='" + pnum
			+ "'>" +
					"<input type='hidden' id='hPnum" + hcnt
			+ "' name='hPnum" + hcnt + "' value='" + pnum
			+ "'></span>";
	var newObj = eval("document.all.Cell" + val);
	newObj.insertAdjacentHTML("beforeEnd", NewHtml);
	
	//PC-1496-DDT-Screen-Changes	
	var NewDDTHtml = "<span id='SpDDT"
			+ hcnt
			+ "'><BR>&nbsp;&nbsp;&nbsp;<input type='hidden' maxlength='20' value='' id='txt_DDT"
			+ hcnt
			+ "' name='txt_DDT"
			+ hcnt
			+ "' class='InputArea' onFocus=changeBgColor(this,'#AACCE8'); onBlur=\"changeBgColor(this,'#ffffff');\" tabindex='"+indexId+"'>&nbsp;</span>";
	var newDDTObj = eval("document.all.DDTCell" + val);

	newDDTObj.insertAdjacentHTML("beforeEnd", NewDDTHtml);
	
	
//PMT-55577 - add drop down  
	var NewDDTHtml = "<span border=1 id='SpDDT"+ hcnt +"'><BR>"+ getComboBox("ddtId"+hcnt,"ddtId"+val,'150px',"") +
			"<a href=javascript:fnDelete("+ hcnt + ");><img src=/images/btn_remove.gif border=0 tabindex=6'></a></span>";
	var newDDTObj  = eval("document.all.DDTCell" + val);
	newDDTObj.insertAdjacentHTML("beforeEnd",NewDDTHtml);


/*	//
	var NewDDTHtml = "<span id='SpDDT"
			+ hcnt
			+ "'><BR>&nbsp;&nbsp;&nbsp;<input type='text' maxlength='20' value='' id='txt_DDT"
			+ hcnt
			+ "' name='txt_DDT"
			+ hcnt
			+ "' class='InputArea' onFocus=changeBgColor(this,'#AACCE8'); onBlur=\"changeBgColor(this,'#ffffff');\" tabindex='"+indexId+"'>&nbsp;<a href=javascript:fnDelete("
			+ hcnt + ");><img src=/images/btn_remove.gif border=0></a></span>";
	var newDDTObj = eval("document.all.DDTCell" + val);

	newDDTObj.insertAdjacentHTML("beforeEnd", NewDDTHtml);*/
	//
	count++;
	hcnt = parseInt(hcnt) + 1;
	intSize = hcnt;
	document.frmOrderDDTForm.hCnt.value = hcnt;
}
// this function used to delete the rows
function fnDelete(val) {
	
	var ObjQty = eval("document.all.SpQty" + val);
	var ObjDDT = eval("document.all.SpDDT" + val);
	
	ObjQty.innerHTML = "";
	ObjDDT.innerHTML = "";
	
}
//this function for add DDT Drop down while click '+' icon
function getComboBox(id, cboid, width, event)
{
	var styl = "class=RightText";
	var onChangestr ="";

	if ( width != undefined && width != '')
	{
		styl = "style='width:"+width+"'";
	}
	
	//alert(styl);
	if ( event != undefined && event != '' )
	{
		 onChangestr=event;
	}
	
	var returnstr;
	returnstr = "<select id='"+id+"'"+styl+" " + onChangestr + " >";

	   var list = document.getElementById(cboid);

       for(var i = 0; i < list.options.length; ++i)
       {
    	   returnstr = returnstr +  "<option value='"+list.options[i].value+"'>"+list.options[i].text+"</option>";
       }
       returnstr = returnstr +  "</select>";
     
       return  returnstr;       
}
//This function used to load the DDT drop down down values based on part number , lot num and order id PMT-55577
document.onreadystatechange = function() {
	if (document.readyState == "complete") {

		var orderId = document.frmOrderDDTForm.orderId.value;
        var hDDT='';
		for (var i = 0; i < intSize; i++) {

			var hPartval = document.getElementById("hPnum" + i).value;
			var hUsageLotNum = document.getElementById("hUsageLot" + i).value;
			var ddtIdObj = document.getElementById("ddtId" + i);
			hDDT =document.getElementById("txt_DDT" + i).value;
		
			var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmOrderDDTAction.do?method=loadLocLotTxnList&orderId='
					+ ddtOrderId
					+ '&partNum='
					+ hPartval
					+ '&controlNum='
					+ hUsageLotNum + '&ramdomId=' + Math.random());
			var loader = dhtmlxAjax.getSync(ajaxUrl);
			 fnCallBack(loader,ddtIdObj,hDDT); 
		}
	}
};
//This function used to call back function of ajax respone and set the DDT id to drop  down 
function fnCallBack(loader,ddtIdObj,hDDT){
	var response = loader.xmlDoc.responseText;
	var obj;
	 ddtIdObj.options[0] = new Option("[Choose One]"," ");
	if (response != '') {
		 obj = JSON.parse(response);
		
			for(var j =0; j<obj.length;j++){
				id = obj[j].txnid;
				value = obj[j].txnid;
				name = obj[j].name;
				
				ddtIdObj.options[j+1] = new Option(name,id,value);
			if(hDDT == id){
				ddtIdObj.options[j+1].selected = true;
				}else if (hDDT == '0'){
					ddtIdObj.options[0].selected = true;
				}
			}
	}
}