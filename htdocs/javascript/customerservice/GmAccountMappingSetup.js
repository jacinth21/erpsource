function fnOnPageLoad(){
	document.frmAccountMapping.Btn_Submit.disabled = false;
}
function fnSubmit(){
	var obj='';
	var inputString='';
	
	for(i=0;i<intMapAccParamsSize;i++){
		obj = eval("document.frmAccountMapping.hAttributeId"+i);
		var obj1 = eval("document.frmAccountMapping.AccNm"+i);
		if(obj.value == '101183'){ //EXCLUDE VAT FIELD CODEID VALUE.BASED ON THIS VALUE(Y) VAT EXCLUDED FOR THE ACCOUNT. 
			var oldval = obj1.value;
			obj1.value = TRIM(obj1.value).toUpperCase();
			if(obj1.value != 'Y' && obj1.value != ''){			
			    Error_Details(message_operations[194]);
				Error_Show();
				Error_Clear();
				obj1.value = oldval;
				return false;
			}
		}
		inputString =inputString + obj.value+'^'+TRIM(obj1.value)+'|';
	}
	strAccountId = document.frmAccountMapping.hAccountId.value; 
	document.frmAccountMapping.Btn_Submit.disabled = true;
	fnStartProgress();
    document.frmAccountMapping.action = "\GmAccountServlet?hAccountId="+strAccountId+"&hInputstr="+inputString+"&hAction=SaveMapAccParams";
	document.frmAccountMapping.submit();
}

function fnClose(){
	window.close();
}