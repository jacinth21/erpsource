/*
            SIMS Links
*/
var TREE_ITEMS = [
            ['Setup', null,
                        ['Surgeon Setup', 'gmPartySetup.do?partyType=7000\',\'']
            ],
            ['Reports', null,
                        ['Surgeon Search', 'gmPartyCommonSearch.do?partyType=7000\',\'mainpage'],
						['Surgeon Rep Search', 'gmSurgeonSearchReport.do\',\''],
						['User Lock Report', 'gmUserLockReport.do\',\'']
            ],
            ['Staffing', null,
                        ['Projects Report', 'gmProjectsReport.do\',\'load'],
						['Project Staffing Report', 'gmProjectStaffingReport.do\',\''],
						['Projects Dashboard', 'gmProjectsDashboard.do\',\'']
            ]
];
