<!--
// Based on a Watermark script by Paul Anderson, CNET Builder.com. 
// NS 6 functionality added by N. Parker March 2001
markW = 260; // pixels wide
markH = 140; // pixels high
markX = 50; // percent right
markY = 50; // percent down
markRefresh = 20; // milliseconds before a refresh
barW = 0; // scrollbar compensation for PC Nav
barH = 0;
function setVals() {
	if (document.all) {	// IE5
		wMark = document.all.waterMark.style;
		wMark.zIndex=-1;
	}
	else if (document.getElementById) { //NS6
		wMark = document.getElementById("waterMark").style;
		wMark.zIndex=-1;
	}
	else if (document.layers) {	// NS4
		wMark = document.layers["waterMark"]
	}	
	wMark.width = markW;
	wMark.height = markH; 
	if (document.all) {
	innerWidth = document.body.clientWidth;
   	innerHeight = document.body.clientHeight;
	}
	else {
	innerWidth = window.innerWidth;
   	innerHeight = window.innerHeight;
	if (document.layers) {
		if (document.height > innerHeight) barW = 20;
    	if (document.width > innerWidth) barH = 20;
		}
	}
	posX = ((innerWidth - markW)-barW) * (markX/100);
	posY = ((innerHeight - markH)-barH) * (markY/100);
}

function wRefresh() {
	if (document.all) {  //IE5
		wMark.left = posX + (document.body.scrollLeft);
		wMark.top = posY + (document.body.scrollTop);
   	} else {  
	wMark.left = posX + pageXOffset;
    wMark.top = posY + pageYOffset;
}

}
function markMe() {
   setVals();
   window.onresize=setVals;
   markID = setInterval ("wRefresh()",markRefresh);
   }
   
window.onload=markMe; // safety for Mac IE4.5
//-->
