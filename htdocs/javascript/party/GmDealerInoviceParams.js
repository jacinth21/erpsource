//Submiting values of Dealer Invoice parameter values
function fnDealerSubmit(form){

	//fnValidateDropDn('invoiceClosingDate',lblClosingDate);
	fnValidateDropDn('invoicecustomertype',message[5268]);
	fnValidateDropDn('paymentTerms', message[5272]);
	fnValidateDropDn('invoiceClosingDate', message[5273]);
	var emailFl = form.electronicEmail.checked;
	if(emailFl==true){
		fnValidateDropDn('elecronicFileFormat', message[5269]);
		fnValidateTxtFld('receipientsEmail', message[5270]);
		var receipientsEmail = form.receipientsEmail.value;
		receipientsEmail = receipientsEmail.replace(/;/g, ',');
		receipientsEmail = receipientsEmail.replace(/\s/g, '');
		if (fnValidateEmail(receipientsEmail)) {
			Error_Details(Error_Details_Trans(message[5123],message[5270])); 
		}
		
		fnValidateDropDn('invoiceLayout', message[5271]);
	}
	fnValidateTxtFld('txt_LogReason', message[5550]);
    var varcomments = form.txt_LogReason.value;
    var included = form.includedPage.value;//document.getElementById("includedPage").value;
	var params = new Array("strOpt");
	form.action = "/gmDealerSetup.do";
	var requestStr = fnGenerateRequestString(form, params);
	requestStr += '&strOpt=save&method=saveDealerSetup';
	requestStr = fnAjaxURLAppendCmpInfo(requestStr);
    if (ErrorCount > 0) {
	    Error_Show();
		Error_Clear();
        return false;
    }
	if(included == 'true') {
		fnStartProgress();
		loadajaxpage(requestStr, 'ajaxdivcontentarea');
		fnStopProgress();
	} else {
		
		form.action = requestStr;
		fnStartProgress();
	 	form.submit();
	}
}
//to load the Dealer Account Report 
function fnOnPageLoadRpt(){
	  
		if(objGridData.value !=''){ 
			fnOnChange();
			gridObj = initGridData('dataGridDiv',objGridData);
			}
  }
	
function initGridData(divRef,objGridData){
	
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.attachHeader(",#text_filter,#text_filter,#text_filter,#select_filter,#select_filter,#select_filter,#text_filter,#text_filter,#select_filter,#text_filter");
	gObj.init();
	gObj.loadXMLString(objGridData);	
	return gObj;
	
	}


function fnOnChange(){
	var dealerDropdownVal = document.frmDealerSetup.dealerName.value;
	if(dealerDropdownVal!=''&&dealerDropdownVal!=0){
	      document.getElementById('dealerId').value = document.frmDealerSetup.dealerName.value;
	}else{
		  document.getElementById('dealerId').value='';
	}
	 
}



function fnAccountMappingRpt(dealerId){
		windowOpener("/gmDealerSetup.do?method=fetchDealerAccounMappingReport&dealerID="+dealerId+"&strOpt=AccountMappingPopup","DealerAccountDetails","resizable=yes,scrollbars=yes,top=250,left=300,width=950,height=190");	
	}  
// this function is used to export excel	 
function fnExport(){
	gridObj.toExcel('/phpapp/excel/generate.php');
}

function fnLoadRpt(){

	
	
	var dealerName= document.frmDealerSetup.dealerName.value;
	var statusfl=document.frmDealerSetup.statusFl.value;
	var partyType = document.frmDealerSetup.partyType.value;
	document.frmDealerSetup.strOpt.value="Reload";
	document.frmDealerSetup.action = "/gmDealerSetup.do?method=fetchDealerAccounMappingReport";
	
	
	fnStartProgress();
	document.frmDealerSetup.submit();
	
}
function fnAccountDetails(dealerID){
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmDealerSetup.do?method=loadDealerMappedAccounts&dealerID='+dealerID+'&randomId='+Math.random());


	
	dhtmlxAjax.get(ajaxUrl,function(loader){
		
		var resJSONObj = $.parseJSON(loader.xmlDoc.responseText);
		
		var tableOpen = '<table border="0" width="100% >';
		var tableClose = '</table>';
		var trOpen = '<tr height="25">';
		var trClose ='</tr>';
		var tdLabelCols1 = '<td class="RightTableCaption" align="Right" colspan ="1">';
		var tdLabelCols2 = '<td class="RightTableCaption" align="Right" colspan ="2">';
		var tdLabelCols3 = '<td class="RightTableCaption" align="Right" colspan ="3">';
		var tdLabel2 =	'</td>';
		var tdLabelleftCols2 = '<td class="RightTableCaption" align="left" colspan ="2">';
		var tdDataOpen = '<td  class="RightText">';
		var tdDataOpenCols1 = '<td  class="RightText" colspan = "1">';
		var tdDataOpenCols2 = '<td   class="RightText" colspan = "2">';
		var tdDataOpenCols3 = '<td  class="RightText" colspan = "3">';
		var tdDataClose = '</td>';
		var trNew = '<tr height="15">'
		var tdNew = '<td>'
		var space ='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
		var parentDiv= document.getElementById("parentDiv");
		var itemJSON;
		var htmlStrAcctNm;
		var tablerow ='';
		var dealerName;
		var parentHTML;
		var msgRow='';
		var htmlStrDistId;
		var htmlStrRepNm;
	
		var lineRow ='<tr><td colspan="6" class="LLine"></td></tr>';
		
		
	
		if(resJSONObj.length==undefined)
			resJSONObj = (new Array()).push(resJSONObj);
		
		if (resJSONObj.length == 0){
			
			msgRow = trOpen+tdDataOpen+message_sales[400]+tdDataClose+trClose;
			parentDiv.innerHTML =tableOpen+msgRow+lineRow+tableClose;
		}else{
			
			for(i= 0; i<resJSONObj.length;i++){
				itemJSON = resJSONObj[i];
			

				 dealerName= itemJSON["DEALERNAME"];
				var accountName= itemJSON["ACCOUNTNAME"];
				var actID= itemJSON["ACCOUNTID"];
				var dName= itemJSON["DNAME"];
				var repName= itemJSON["RPNAME"];
				
				
				htmlStrAcctNm = trOpen+tdDataOpenCols2+space+accountName+'('+actID+')'+tdDataClose;
				htmlStrDistId =tdLabelCols1+message_sales[402]+tdLabel2+tdDataOpenCols1+dName+tdDataClose;
				htmlStrRepNm = tdLabelCols1+message_sales[403]+tdLabel2+tdDataOpenCols1+repName+tdDataClose+trClose;
				
				tablerow = tablerow+ htmlStrAcctNm+htmlStrDistId+htmlStrRepNm;
			}
			
			parentHTML = trOpen+tdDataOpen+message_sales[401]+" : "+ dealerName+tdDataClose+trClose ;
			
					
			parentDiv.innerHTML = tableOpen+trOpen+tdNew+tableOpen+parentHTML+tableClose+tdDataClose+trClose+tablerow+msgRow+lineRow+tableClose;
		}
		
	});
}
function fnValidateEmail(inputvalue) {
	var pattern = /(^[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$)/;
	var error = false;
	var email = inputvalue.split(',');
	for ( var i = 0; i < email.length; i++) {
		if (!pattern.test(email[i])) {
			error = true;
			break;
		}
	}
	return error;
}
function fnChangeDealer(obj){
	 document.frmDealerSetup.dealerName.value=obj.value;
}
