/***********************************************************************************
 * Party Setup Container
 ***********************************************************************************/

function fnUpdateTabIndex(link) {
	var linkObj = link.getElementsByTagName('a')[0];
	document.all.hcurrentTab.value = linkObj.name;
}

function fnPartyHelp(wikiPath){
	var currentTab = document.all.hcurrentTab.value;
	var wikiTitle = "";
	if(currentTab == "Basic Info") {
		wikiTitle = "Party_Setup";
	} else if(currentTab != "Party_Setup" && 
			  currentTab != "Internal Comments" && 
			  currentTab != "Upload Section") 
	{
		wikiTitle = currentTab + "_Setup";
	} else { 
		wikiTitle = currentTab;
	}
	if(varpartyType == '7003'){
		wikiTitle = "GROUP_PRICE_BOOK_SETUP"
	}
	//alert("wikiTitle = " + wikiTitle);
	var wikiPage = wikiPath+"/"+wikiTitle;
	windowOpener(wikiPage,"Prntwiki","resizable=yes,scrollbars=yes,top=250,left=50,width=950,height=650");	
}


/***********************************************************************************
 * Party Setup
 ***********************************************************************************/
function fnPartySubmit(form) {
	var savedPartyID = document.getElementById("partyId").value;
	if(form.partyType.value != '7008'){// 7008: Group
		if(form.firstName!= null && form.firstName.value == '') {
			Error_Details(message[5199]);
		}
		if(form.lastName!= null && form.lastName.value == '') {
			Error_Details(message[5200]);
		}
	}
	if(form.partyName!= null && form.partyName.value == '') {	
		if (inclPartyIdFlag == 'Y'){
		    			Error_Details(message[10624]);
 		}	
	}
	// MNTTASK-6074:Account Affiliation added mandatory field
	if(form.partyName!= null && form.partyName.value == '') {		
		Error_Details(message[5201]);
	}
	if(partyType == '26230725'){ //Dealer party type = 26230725
	if(form.partyNameEn!= null && form.partyNameEn.value == '') {
		Error_Details(message[5275]);
	}
	if(form.inactiveFlag.checked){
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmDealerSetup.do?method=loadDealerMappedAccounts&dealerID='+savedPartyID+'&randomId='+Math.random());
		var ajaxUrlVal = dhtmlxAjax.postSync(ajaxUrl);
		var resJSONObj =$.parseJSON(ajaxUrlVal.xmlDoc.responseText);//loader.xmlDoc.responseText);
			if(resJSONObj.length==undefined)
				resJSONObj = (new Array()).push(resJSONObj);
			
			if (resJSONObj.length>=1){
				Error_Details(message[5274]);
			}
	}		
}
    if (ErrorCount > 0) {
	    Error_Show();
		Error_Clear();
        return false;
    }
    
    form.action = "/gmPartySetup.do?strOpt=save&partyId="+savedPartyID
	form.Btn_Submit.disabled = true;// To disable submit button
	fnStartProgress();
    form.submit();
}

function fnPartyFetch(form) {	
	var partyTypeVal = document.getElementById("partyType").value;
	form.action = "/gmPartySetup.do?strOpt=fetchcontainer&partyType="+partyTypeVal;
	form.submit();
}

function fnPartyReset(form) {
	form.strOpt.value = 'fetchcontainer';
	document.getElementById("partyId").value = 0;
	form.submit();
}

function fnPartyVoid(form) {
	
	var partyIdObj = document.getElementById("partyId");		
    if(partyIdObj.value == 0) {
    	if(form.partyType.value == '7008'){//7008: Group
    		Error_Details(message[5202]);
    	}else if (form.partyType.value == '4000641'){//Shipping Account 
    		Error_Details(message[5203]);
    	} else if (partyType == '26230725'){//Dealer party type=26230725
    		Error_Details(message[5276]);
    	}else if (inclPartyIdFlag == 'Y'){
    		Error_Details(message[10624]);
   		}
    	else{
    		Error_Details(message[5204]);
	}
    }
    if (ErrorCount > 0) {
		Error_Show();
        Error_Clear();
        return false;
    }
    if(partyType == '26230725'){
    	var cancelType = 'VDDLR';
    	form.hTxnId.value = partyIdObj.value;
    	form.hTxnName.value = 'Void Dealer: ' + partyIdObj.options[partyIdObj.selectedIndex].text; 
    	form.action="/GmCommonCancelServlet?&hCancelType="+cancelType;
    	form.submit();
    }else{
    form.action ="/GmCommonCancelServlet";
    form.hTxnId.value = partyIdObj.value;

  if (inclPartyIdFlag == 'Y'){
	 form.hTxnName.value = partyIdObj.options[partyIdObj.selectedIndex].text; 
   }else{
    form.hTxnName.value = 'Void Party: ' + partyIdObj.options[partyIdObj.selectedIndex].text; 
    }
    form.submit();
    }
}

/***********************************************************************************
 * Globus Association Setup
 ***********************************************************************************/

function fnGlobusReset(form) {
//	/var form = document.getElementsByName('frmGlobusAssociationSetup')[0];
	form.action = "/gmGlobusAssociationSetup.do";
	var params = new Array("strOpt", "personnelType", "primaryFlag");
	var requestStr = fnGenerateRequestString(form, params);
	requestStr += '&primaryFlag=off&personnelType=0&strOpt=&method=loadGlobusAssociationSetup';
	//alert('requestStr = ' + requestStr);
	loadajaxpage(requestStr, 'ajaxdivcontentarea');
}

function fnGlobusLoad(form) {
	//var form = document.getElementsByName('frmGlobusAssociationSetup')[0];
	form.action = "/gmGlobusAssociationSetup.do";
	var params = new Array("strOpt");
	var requestStr = fnGenerateRequestString(form, params);
	requestStr += '&strOpt=load&method=loadGlobusAssociationSetup';
	//alert('requestStr = ' + requestStr);
	loadajaxpage(requestStr, 'ajaxdivcontentarea');
}

function fnGlobusEditId(val) {	
	var form = document.getElementsByName('frmPartySetup')[0];
	form.action = "/gmGlobusAssociationSetup.do";
	var params = new Array("hglobusAssociationId", "strOpt");
	var requestStr = fnGenerateRequestString(form, params);
	requestStr += '&strOpt=edit&method=loadGlobusAssociationSetup&hglobusAssociationId=' + val;
	//alert('requestStr = ' + requestStr);
	loadajaxpage(requestStr, 'ajaxdivcontentarea');
}

function fnGlobusSubmit(form) {
	//var form = document.getElementsByName('frmGlobusAssociationSetup')[0];
	if(form.personnelType.value == 0) {
		Error_Details(message[5205]);
	}
	if(form.personnelName.value == 0) {
		Error_Details(message[5206]);
	}
    if (ErrorCount > 0) {
		Error_Show();
        Error_Clear();
        return false;
    }
	var params = new Array("strOpt");
	form.action = "/gmGlobusAssociationSetup.do";
	var requestStr = fnGenerateRequestString(form, params);
	requestStr += '&strOpt=save&method=loadGlobusAssociationSetup';
	//alert('requestStr = ' + requestStr);
	loadajaxpage(requestStr, 'ajaxdivcontentarea');
}

function fnGlobusVoid(form) {
	//var form = document.getElementsByName('frmGlobusAssociationSetup')[0];
	if(form.hglobusAssociationId.value == '') {
		Error_Details(message[5207]);
	}
    if (ErrorCount > 0) {
		Error_Show();
        Error_Clear();
        return false;
    }
	form.action ="/GmCommonCancelServlet";
	form.hTxnId.value = form.hglobusAssociationId.value;
	form.hTxnName.value = 'Void Globus Association - ' + form.personnelName.options[form.personnelName.selectedIndex].text; 
	form.submit();
}

/***********************************************************************************
 * Contact Setup
 ***********************************************************************************/


function fnContactEditId(val) {
	var form = document.getElementsByName('frmPartySetup')[0];
	// When call the - Contact setup screen as separate form name should be below.
	if(form == undefined || form == null){
		form = document.getElementsByName('frmContactSetup')[0];
	}
	var params = new Array("strOpt", "hcontactId");
	form.action = "/gmContactSetup.do";
	var requestStr = fnGenerateRequestString(form, params);
	requestStr += '&strOpt=edit&method=loadContactSetup&hcontactId=' + val;
	//alert('requestStr = ' + requestStr);
	var included = form.includedPage.value;
	if(included == 'false') {
		//alert('Its a standalone page!');
		form.action = requestStr;
		form.submit();
	} else {
		//alert('Its not a standalone page!');
		loadajaxpage(requestStr, 'ajaxdivcontentarea');
	}
}

function fnContactSubmit(form) {
	//var form = document.getElementsByName('frmContactSetup')[0];
	
	fnValidateDropDown(form, 'contactMode', message[5208])
	fnValidateTextField(form, 'contactType', message[5209]);
	fnValidateTextField(form, 'contactValue',message[5210]);
	if (form.primaryFlag != undefined) {
		NumberValidation(form.preference.value, message[5211], 0);
	}
	if (ErrorCount > 0) {
		Error_Show();
        Error_Clear();
        return false;
    }
//  PC-5567 Email ID Validation in Sales Rep contact setup
	var contactMode = document.frmContactSetup.contactMode.value;
	if(contactMode != '90450' && contactMode != '90451' && contactMode != '90454'){ // (Phone=90450, Fax=90454 Pager=90451) condition pass only Email Typs 
		var contactValues = document.frmContactSetup.contactValue.value;
		var myarray = contactValues.split(',');
		var updatedcontactValue="";
		for(var i=0; i<myarray.length;i++){
			if(myarray[i]!=''){
				updatedcontactValue=updatedcontactValue+myarray[i]+",";				
			}
		}
		updatedcontactValue=updatedcontactValue.replace(/,(?=[^,]*$)/, '')
		document.frmContactSetup.contactValue.value = updatedcontactValue;
	}
	var included = form.includedPage.value;
	var params = new Array("strOpt");
	form.action = "/gmContactSetup.do";
	var requestStr = fnGenerateRequestString(form, params);
	requestStr += '&strOpt=save&method=loadContactSetup&'+fnAppendCompanyInfo();
	if(included == 'false') {
		form.action = requestStr;
		form.submit();
	} else {
		loadajaxpage(requestStr, 'ajaxdivcontentarea');
	}
}

function fnContactReset(form) {
	//var form = document.getElementsByName('frmContactSetup')[0];
	form.hcontactId.value = '';
	form.contactType.value = '';
	form.contactValue.value = '';
	form.contactMode.value = 0;
	if(form.primaryFlag != undefined)
	{
		form.primaryFlag.checked = false;
		form.primaryFlag.disabled = false;
		form.preference.value = '';
		form.inactiveFlag.checked = false;
		form.inactiveFlag.disabled = false;
	}
}

function fnContactVoid(form) {
	//var form = document.getElementsByName('frmContactSetup')[0];

	if(form.hcontactId.value == '') {
		Error_Details(message[5212]);
	}
    if (ErrorCount > 0) {
		Error_Show();
        Error_Clear();
        return false;
    }

	form.action ="/GmCommonCancelServlet";
	form.hTxnId.value = form.hcontactId.value;
	form.hTxnName.value = 'Void ' + form.contactType.value + ' - ' + form.contactValue.value; 
	form.submit();
}

/***********************************************************************************
 * Address Setup
 ***********************************************************************************/

function fnAddressSubmit(form) {
	//var form = document.getElementsByName('frmAddressSetup')[0];
	var varcomments = form.txt_LogReason.value;
	var hActiveFlag = form.hActiveFlag.value;// To get the hidden active flag, to know whether the address is already inactivated 
	var hcount = form.hcount.value;// to get the count of transactions associated to the address
	var addrID = form.haddressID.value;
	if(partyType!='26230725'){//Dealer party type=26230725
		fnValidateDropDown(form, 'addressType',message[5213]);
		fnValidateDropDown(form, 'carrier',message[5219]);
	}else{
		fnValidateTextField(form, 'billname',message[5143]);
	}
	
	fnValidateTextField(form, 'billAdd1',message[5214]);
	fnValidateTextField(form, 'billCity',message[5215]);
	fnValidateDropDown(form, 'state',message[5216]);
	fnValidateDropDown(form, 'country',message[5217]);
	fnValidateTextField(form, 'zipCode',message[5218]);
	
	if (form.primaryFlag != undefined && form.preference != undefined) {
			NumberValidation(form.preference.value,message[5220], 0);
		}
	
	if (varcomments != '' && TRIM(varcomments).length >= '4000')
	{
	         Error_Details(message[5223]);
	}
	if (ErrorCount > 0) {
		Error_Show();
        Error_Clear();
             return false;
    }
	fnStartProgress();
	var included = form.includedPage.value;
	var params = new Array("strOpt");
	form.action = "/gmAddressSetup.do";
	var requestStr = fnGenerateRequestString(form, params);
	requestStr += '&strOpt=save&method=loadAddressSetup&'+fnAppendCompanyInfo();
	if(included == 'false') {
		var primaryAddObj = document.getElementById('primaryAddID');
		var inactiveFl = false;
		if(form.activeFlag){
			inactiveFl = form.activeFlag.checked;
		}		
		var addID = document.frmAddressSetup.haddressID.value;
		if(primaryAddObj!= null && primaryAddObj.value != addID && form.primaryFlag != undefined && form.primaryFlag.disabled != true && form.primaryFlag.checked == true){
			var primaryConfirm = confirm(message[5221]);
			if(!primaryConfirm)
				return false;
		}
		
		//No need to ask for confirmation, if the address is already inactivated
		if(inactiveFl && hcount!=null && hcount>0 && hActiveFlag == 'off'){
		//If any transaction is associated with the address, need to get confirmation from user to inactivate the address
		 	if(!confirm(message[5222])){
			 	return false;
		 	}
	// 	windowOpener("/gmAddressSetup.do?method=loadAssoAddrTxn&haddressID="+addrID,"","resizable=yes,scrollbars=yes,top=250,left=300,width=1010,height=600");	
	  
		}
		form.action = requestStr;
	 	form.submit();
	} else {
	 	loadajaxpage(requestStr, 'ajaxdivcontentarea');
	 	
	}		 	
     setTimeout(function(){ fnStopProgress(); }, 12000);
     
}

function fnAddressEditId(val) {
	var form = document.getElementsByName('frmPartySetup')[0];
	// When call the - address setup screen as separate form name should be below. 
	if(form == undefined || form == null){
		form = document.getElementsByName('frmAddressSetup')[0];
	}
	var included = document.getElementById("includedPage").value;
	var params = new Array("haddressID", "strOpt");
	form.action = "/gmAddressSetup.do";
	var requestStr = fnGenerateRequestString(form, params);
	requestStr += '&strOpt=edit&method=loadAddressSetup&haddressID=' + val;
	fnStartProgress();
    	if(included == 'false') {
		//alert('Its a standalone page!');
		form.action = requestStr;
		form.submit();
	} else {
		loadajaxpage(requestStr, 'ajaxdivcontentarea');
	}
	setTimeout(function(){ fnStopProgress(); }, 12000);
}

function fnAddressReset(form) {
	//var form = document.getElementsByName('frmAddressSetup')[0];
	form.haddressID.value = '';
	form.addressType.value = 0;
	form.billAdd1.value = '';
	form.billAdd2.value = '';
	form.billCity.value = '';
	form.state.value = 0;
	form.zipCode.value = '';
	form.country.value = 0;
	if (form.primaryFlag != undefined  && form.preference != undefined) {
		form.preference.value = '';
		form.activeFlag.checked = false;	
		form.primaryFlag.checked = false;
		form.primaryFlag.disabled = false;	
		form.activeFlag.disabled = false;
	} 
}

function fnAddressVoid(form) {
	//var form = document.getElementsByName('frmAddressSetup')[0];

	if(form.haddressID.value == '') {
		Error_Details(message[5224]);
	}
    if (ErrorCount > 0) {
		Error_Show();
        Error_Clear();
        return false;
    }
    var paryId = form.partyId.value;
	var fromStropt=form.fromStrOpt.value;
	var addID=form.addIdReturned.value;
	var oldAddID=form.oldAddID.value;
	var repID=form.repID.value;
	var refID=form.refID.value;
    form.hRedirectURL.value =  "/GmPageControllerServlet?strPgToLoad=gmAddressSetup.do&method=loadAddressSetup&includedPage=false&fromStrOpt="+fromStropt+"&transactPage="+form.transactPage.value+"&partyId="+paryId+"&repID="+repID+"&addID="+addID+"&refID="+refID+"&oldAddID="+oldAddID;
    form.hDisplayNm.value = "Back to Previous Screen";
	form.action ="/GmCommonCancelServlet";
	form.hTxnId.value = form.haddressID.value;
	form.hTxnName.value = 'Void ' + form.addressType.value + ' - ' + form.billAdd1.value + ' ' + form.billAdd2.value; 
	form.submit();
}


function refeshParentPage(form){
	//var form = document.getElementsByName('frmAddressSetup')[0];
	if(form == undefined || form == null){
		form = document.getElementsByName('frmAddressSetup')[0];
	}
	var stropt=form.strOpt.value;
	if (form.transactPage.value == 'true')
	{				
		if (form.addIdReturned.value != '')
		{
			 window.opener.document.all.addIdReturned.value = form.addIdReturned.value;
			 window.opener.fnGetAddressList(window.opener.document.getElementById('names'));	
			 window.close();
		}
	}else if (form.transactPage.value == 'CASE' && stropt=='save')
	{				
		if (form.addIdReturned.value != '')
		{
			parent.fnGetAddressList(parent.document.getElementById('names'),form.addIdReturned.value);	
			parent.dhxWins.window("w1").close();
		}
	}	
	else if (form.transactPage.value == 'areaset' && stropt=='save')
	{
		var fromStropt=form.fromStrOpt.value;
		var addID=form.addIdReturned.value;
		var oldAddID=form.oldAddID.value;
		var repID=form.repID.value;
		var refID=form.refID.value;
		var shipMode=form.shipMode.value;	
		var caseDistID = form.caseDistID.value;
		var caseRepID = form.caseRepID.value;
		var caseAccID = form.caseAccID.value;
		
		if(addID==null||addID=='')
		{
			addID=oldAddID;
		}
		if(addID == '0'){
			primaryAddObj = document.getElementById('primaryAddID');
			addID = (primaryAddObj != null)?primaryAddObj.value:addID;
		}
		
		form.action = "/gmAreaSetShipping.do?method=EditAddress&repID="+repID+"&addID="+addID+"&refID="+refID+"&strOpt="+fromStropt+"&shipType=4121&oldAddID="+oldAddID+"&shipMode="+shipMode+"&caseDistID="+caseDistID+"&caseRepID="+caseRepID+"&caseAccID="+caseAccID;
		form.submit();
	}
	
}

function fnBackToAreaset(form){
	//var form = document.getElementsByName('frmAddressSetup')[0];
	form.action = "/gmAddressSetup.do";
	form.strOpt.value='save';
	refeshParentPage(form);
}

function fnClose(){
	   if(parent.dhxWins!= undefined){
		   CloseDhtmlxWindow();
		   return false;
	   }else{
		   window.close();  
	   }  
}

/***********************************************************************************
 * Education Setup
 ***********************************************************************************/
function fnEducationFetch(form) {
	//var form = document.getElementsByName('frmEducationSetup')[0];
	var params = new Array("strOpt");
	form.action = "/gmEducationSetup.do";
	var requestStr = fnGenerateRequestString(form, params);
	requestStr += '&strOpt=load&method=loadEducationSetup';
	//alert('requestStr = ' + requestStr);
	loadajaxpage(requestStr, 'profiledivcontentarea');
}

function fnEducationSubmit(form) {
	//var form = document.getElementsByName('frmEducationSetup')[0];
	fnValidateDropDown(form, 'educationLevel',message[5225]);
	fnValidateDropDown(form, 'designation',message[5226]);
	fnValidateTextField(form, 'instituteName',message[5227]);
	fnValidateTextField(form, 'course',message[5228]);
	fnValidateYear(form.classYear.value, message[5229]);
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	var params = new Array("strOpt");
	form.action = "/gmEducationSetup.do";
	var requestStr = fnGenerateRequestString(form, params);
	requestStr += '&strOpt=save&method=loadEducationSetup';
	//alert('requestStr = ' + requestStr);
	loadajaxpage(requestStr, 'profiledivcontentarea');
}

function fnEducationEditId(val) {
	var form = document.getElementsByName('frmPartySetup')[0];
	form.action = "/gmEducationSetup.do";
	var params = new Array("heducationId", "strOpt");
	var requestStr = fnGenerateRequestString(form, params);
	requestStr += '&strOpt=edit&method=loadEducationSetup&heducationId=' + val;
	//alert('requestStr = ' + requestStr);
	loadajaxpage(requestStr, 'profiledivcontentarea');
}

function fnEducationReset(form) {
	//var form = document.getElementsByName('frmEducationSetup')[0];
	form.strOpt.value='';
	form.heducationId.value='';
	form.educationLevel.value='0';
	form.instituteName.value='';
	form.classYear.value='';
	form.course.value='';
	form.designation.value='0';
}

function fnEducationVoid(form) {
	//var form = document.getElementsByName('frmEducationSetup')[0];
	if(form.heducationId.value == '') {
		Error_Details(message[5230]);	
	}
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	form.action ="/GmCommonCancelServlet";
	form.hTxnId.value = form.heducationId.value;
	var levelDropDown = form.educationLevel;
	var instituteNmTxt = form.instituteName.value;
	form.hTxnName.value = 'Party Education - ' + form.educationLevel.options[form.educationLevel.selectedIndex].text + ' - ' + instituteNmTxt;
	form.submit();
}

/***********************************************************************************
 * Publication Setup
 ***********************************************************************************/

function fnPublicationSubmit(form) {
	//var form = document.getElementsByName('frmPublicationSetup')[0];
	
	fnValidateDropDown(form, 'typeID',message[5231]);
	fnValidateTextField(form, 'title',message[5232]);
	fnValidateTextField(form, 'author', message[5233]); 
	fnValidateYearAllowBlank(form.pyear.value, message[5234]);

	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}

	var params = new Array("strOpt");
	var requestStr = fnGenerateRequestString(form, params);
	requestStr += '&strOpt=save&method=loadPublicationSetup';
	loadajaxpage(requestStr, 'profiledivcontentarea');
}

function fnPublicationEditId(val) {
	var form = document.getElementsByName('frmPartySetup')[0];
	form.action = "/gmPublicationSetup.do";
	var params = new Array("hpublicationID", "strOpt");
	var requestStr = fnGenerateRequestString(form, params);
	requestStr += '&strOpt=edit&method=loadPublicationSetup&hpublicationID=' + val;
	loadajaxpage(requestStr, 'profiledivcontentarea');
}



function fnPublicationReset(form) {
	//var form = document.getElementsByName('frmPublicationSetup')[0];

	form.typeID.value = '0';
	form.title.value = '';
	form.reference.value = '';
	form.pabstract.value = '';
	form.pyear.value = '';
	form.author.value = '';
	form.refURL.value = '';
	form.hpublicationID.value = '';	
}

function fnPublicationVoid(form) {
	//var form = document.getElementsByName('frmPublicationSetup')[0];
	if(form.hpublicationID.value == '') {
		Error_Details(message[5235]);	
	}
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	form.action ="/GmCommonCancelServlet";
	form.hTxnId.value = form.hpublicationID.value;
	form.hTxnName.value = form.title.value;
	form.submit();
}

/***********************************************************************************
 * Career Setup
 ***********************************************************************************/


function fnCareerEditId(val) {
	var form = document.getElementsByName('frmPartySetup')[0];
	var params = new Array("strOpt", "hcareerId");
	form.action = "/gmCareerSetup.do";
	var requestStr = fnGenerateRequestString(form, params);
	requestStr += '&strOpt=edit&method=loadCareerSetup&hcareerId=' + val;
	loadajaxpage(requestStr, 'profiledivcontentarea');
}

function fnCareerSubmit(form) {
	//var form = document.getElementsByName('frmCareerSetup')[0];
	
	fnValidateTextField(form, 'title', message[5232]);
	fnValidateTextField(form, 'companyName', message[5236]);

	fnValidateYearAllowBlank(form.startYear.value, message[5237]);
	fnValidateYearAllowBlank(form.endYear.value, message[5238]);

	fnValidateTextField(form, 'groupDivision', message[5239]);
	fnValidateDropDown(form, 'country', message[5217]);
	fnValidateDropDown(form, 'state',message[5216]);
	if (ErrorCount > 0) {
		Error_Show();
        Error_Clear();
        return false;
    }

	var params = new Array("strOpt");
	var requestStr = fnGenerateRequestString(form, params);
	requestStr += '&strOpt=save&method=loadCareerSetup';
	loadajaxpage(requestStr, 'profiledivcontentarea');
}

function fnCareerReset(form) {
	//var form = document.getElementsByName('frmCareerSetup')[0];

    form.title.value="";
    form.companyName.value="";
    form.companyDesc.value="";
    form.startMonth.value="0";
    form.startYear.value="";
    form.endMonth.value="0";
    form.endYear.value="";
    form.groupDivision.value="";
    form.country.value="0";
    form.state.value="0";
    form.city.value="";
    form.industry.value="";
    form.hcareerId.value="";
}

function fnCareerVoid(form) {
	//var form = document.getElementsByName('frmCareerSetup')[0];

	if(form.hcareerId.value == '') {
		Error_Details(message[5240]);
	}
    if (ErrorCount > 0) {
		Error_Show();
        Error_Clear();
        return false;
    }

	form.action ="/GmCommonCancelServlet";
	form.hTxnId.value = form.hcareerId.value;
	form.hTxnName.value = 'Void ' + form.title.value + ' - ' + form.companyName.value; 
	form.submit();
}

/***********************************************************************************
 * Personal Info Setup
 ***********************************************************************************/

function fnPersonalInfoSubmit(form) {
	//var form = document.getElementsByName('frmPersonalInfoSetup')[0];
	var params = new Array("strOpt");
	var requestStr = fnGenerateRequestString(form, params);
	requestStr += '&strOpt=save';
	//alert('requestStr = ' +requestStr);
	loadajaxpage(requestStr, 'profiledivcontentarea');
}

function fnRelationshipStatus(form) {
	//var form = document.getElementsByName('frmPersonalInfoSetup')[0];
	var status = form.relationshipStatus.value;
	if(status == 92009 || status == 0) {		// [Choose One] or Single
		form.spouseDetails.disabled=true;
		form.spouseDetails.value="";
	} else {
		form.spouseDetails.disabled=false;
	}
}

function fnChildrenCount(form) {
//	/var form = document.getElementsByName('frmPersonalInfoSetup')[0];
	var childCnt = form.children.value;
	if(childCnt == 92011 || childCnt == 0) {	// [Choose One] or None
		form.childrenDetails.disabled=true;
		form.childrenDetails.value="";
	} else {

		form.childrenDetails.disabled=false;
	}
}

/***********************************************************************************
 * Affiliation Setup
 ***********************************************************************************/

function fnAffiliationEditId( val){
	
	var form = document.getElementsByName('frmPartySetup')[0];
	form.action = "/gmAffiliationSetup.do";
	var params = new Array('strOpt', 'affId');
	var requestStr = fnGenerateRequestString (form, params);
    requestStr += '&method=loadAffiliationSetup&strOpt=edit&affId=' + val;
	loadajaxpage(requestStr, 'profiledivcontentarea');
}

function fnAffiliationSubmit(form){
	//var form = document.getElementsByName('frmAffiliationSetup')[0];
	fnValidateTxtFld('organization',message[5241]);
	fnValidateDropDn('affType',message[5231]);
	
	fnValidateYearAllowBlank(form.startYear.value, message[5237]);
	fnValidateYearAllowBlank(form.endYear.value, message[5238]);
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	var params = new Array('strOpt');
	var requestStr = fnGenerateRequestString(form, params);
    requestStr += '&strOpt=save&method=loadAffiliationSetup';
	//alert('requestStr = ' + requestStr);
	loadajaxpage(requestStr, 'profiledivcontentarea');
}

function fnAffiliationReset(form)
{ 
	//var form = document.getElementsByName('frmAffiliationSetup')[0];
	form.affId.value = '';
	form.organization.value = '';
	form.affType.value = '0';
	form.committeeName.value = '';
	form.role.value = '';
	form.startYear.value = '';
	form.endYear.value = '';
	form.stillInPosition.checked = false;
}

function fnAffiliationVoid(form)
{
	//var form = document.getElementsByName('frmAffiliationSetup')[0];
	if(form.affId.value == '') {
		Error_Details(message[5242]);
	}
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	form.action ="/GmCommonCancelServlet";  
	form.hTxnId.value = form.affId.value;
	form.hTxnName.value = form.organization.value;
	form.submit();
}

/***********************************************************************************
 * License Setup
 ***********************************************************************************/

function fnLicenseSubmit(form){
//	/var form = document.getElementsByName('frmLicenseSetup')[0];
	fnValidateTxtFld('title',message[5243]);
	fnValidateYearAllowBlank(form.issueYear.value, message[5244]);
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	var params = new Array('strOpt');  
	var requestStr = fnGenerateRequestString (form, params);
    requestStr += '&strOpt=save&method=loadLicenseSetup';
	loadajaxpage(requestStr, 'profiledivcontentarea');
 
}

function fnLicenseEditId(val){
	var form = document.getElementsByName('frmPartySetup')[0];
	form.action = "/gmLicenseSetup.do";
	var params = new Array('strOpt', 'licenseId'); 
	var requestStr = fnGenerateRequestString (form, params);
	requestStr += '&method=loadLicenseSetup&strOpt=edit&licenseId=' + val;
	loadajaxpage(requestStr, 'profiledivcontentarea');
}

function fnLicenseVoid(form) {
//	/var form = document.getElementsByName('frmLicenseSetup')[0];
	if(form.licenseId.value == '') {
		Error_Details(message[5245]);
	}
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}	
	form.action ="/GmCommonCancelServlet";
	form.hTxnId.value = form.licenseId.value;
	form.hTxnName.value = form.title.value + ' - ' + form.state.options[form.state.selectedIndex].text;
	form.submit();
}

function fnLicenseReset(form) { 
	//var form = document.getElementsByName('frmLicenseSetup')[0];
	form.licenseId.value = '';
	form.title.value = ""; 
	form.description.value = ""; 
	form.issueYear.value = "";
	form.issueMonth.value = "0";
	form.country.value = "0";
	form.state.value = "0";	
}

/***********************************************************************************
 * Internal Comments
 ***********************************************************************************/

function fnInternalCommentsSubmit(form){
	form.action = "/gmInternalCommentsSetup.do";
	var params = new Array('haction');  
	var requestStr = fnGenerateRequestString (form, params);		
	requestStr += '&haction=save';
	loadajaxpage(requestStr, 'ajaxdivcontentarea');
}

/***********************************************************************************
 * Achievement Setup
 ***********************************************************************************/

function fnAchieveVoid(form){
	//var form = document.getElementsByName('frmAchievementSetup')[0];
	if(form.hachievementID.value == '') {
		Error_Details(message[5246]);
	}
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	form.action ="/GmCommonCancelServlet";
	form.hTxnId.value =	form.hachievementID.value;
	form.hTxnName.value = form.type.options[form.type.selectedIndex].text + ' - ' + form.title.value;
	form.submit();
}

function fnAchieveReset(form){
	 //var form = document.getElementsByName('frmAchievementSetup')[0];
	 form.hachievementID.value = '';
	 form.type.value = "0"; 
	 form.title.value = ""; 
	 form.description.value = ""; 
	 form.year.value = "";
	 form.month.value = "0";
	 form.coInvestigators.value = ""; 
}

function fnAchieveSubmit(form) {
	//var form = document.getElementsByName('frmAchievementSetup')[0];
	fnValidateDropDn('type',message[5231]);
	fnValidateTxtFld('title',message[5232]);
	fnValidateYearAllowBlank(form.year.value, message[5247]);

	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	var params = new Array('strOpt');  
	form.action = "/gmAchievementSetup.do";
	var requestStr = fnGenerateRequestString(form, params);		
    requestStr += '&strOpt=save&method=loadAchievementSetup';
	//alert('requestStr = ' + requestStr);
	loadajaxpage(requestStr, 'profiledivcontentarea');
}

function fnAchieveEditId(val) {	
	var form = document.getElementsByName('frmPartySetup')[0];
	form.action = "\gmAchievementSetup.do";
	var params = new Array('strOpt', 'hachievementID');
	var requestStr = fnGenerateRequestString(form, params);
	requestStr += '&strOpt=edit&method=loadAchievementSetup&hachievementID=' + val;
	//alert('requestStr = ' + requestStr);
	loadajaxpage(requestStr, 'profiledivcontentarea');
}


/***************************************************************************************
 * Summary Screen
 ****************************************************************************************/

function fnOpenFile(Id)
{
	windowOpener("/GmCommonFileOpenServlet?sId="+Id+"&uploadString=PARTYUPLOADHOME","OpenDoc","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");	
}
function fnPrint()
{
	window.print();
}
var tdinnner = "";
function hidePrint()
{
	var btnObject = document.all.btnPrint;
	tdinnner = btnObject.innerHTML;
	btnObject.style.display="none";
	
	var linkObjects=document.getElementsByTagName("a");
	for (i=0; i<linkObjects.length; i++) {
		if(linkObjects[i].innerHTML == "Edit" || linkObjects[i].innerHTML == "More") {
			linkObjects[i].style.display="none";
		}
	}
	
}

function showPrint() {
	window.location.reload();
}

/***************************************************************************************
 * Party Common Search Page
 ****************************************************************************************/

function fnHideDivTags() {
	var tabs = document.getElementsByTagName('div');
	for(var i=0; i<tabs.length; i++) {
		var ids = tabs[i].id;
		if(ids.indexOf('contentarea') != -1) {
			tabs[i].style.display = 'none';
		}
	}
	
	var trs = document.getElementsByTagName('tr');
	for(var i=0; i<trs.length; i++) {
		if(trs[i].id != '') {
			trs[i].style.background = 'gray';
		}
	}
	
}

function toggleDisplay(Id) {
	var obj = eval('document.all.' + Id);
	var trObj = eval('document.all.tr' + Id);
	if(obj.style.display == 'none') {
		obj.style.display = '';
		trObj.style.background = '#FFFFFF';
	} else {
		obj.style.display = 'none';
		trObj.style.background = 'gray';
	}
}


function fnSearch() {
	var filtertext = document.getElementsByName('filtertext');
	var filter = "";
	for(var i=0; i<filtertext.length; i++) {
		if(filtertext[i].value != "") {
			filter = filter + filtertext[i].id + ";LIKE^" + filtertext[i].value + "|";
		}
	}

	var filterdd = document.getElementsByName('filterdropdown');
	for(var i=0; i<filterdd.length; i++) {
		if(filterdd[i].value != "" && filterdd[i].value != "0") {
			filter = filter + filterdd[i].value + ";=^" + filterdd[i].options[filterdd[i].selectedIndex].text + "|";
		}
	}	
	var form = document.getElementsByName('frmPartyCommonSearch')[0];
	form.action = "/gmSearchParty.do?searchStringWithId="+filter+"&partyType="+form.partyType.value;
	fnStartProgress();
	form.submit();
}

/***************************************************************************************
 * COMMON FUNCTIONS
 ****************************************************************************************/
function fnValidateTextField(form, val, name) {
	var obj = eval("document." + form.name + "." + val);
	var objVal = TRIM(obj.value);
	if(obj.disabled != true && objVal == '') {
		Error_Details(Error_Details_Trans(message[5248],name));
	}
}

function fnValidateDropDown(form, val, name) {
	var obj = eval("document." + form.name + "." + val);
	var objVal = TRIM(obj.value);
	if(obj.disabled != true && objVal == '0') {
		Error_Details(Error_Details_Trans(message[5249],name));
	}
}

function fnValidateYear(val, field) {
	if(val == '') {
		Error_Details(Error_Details_Trans(message[5250],field));
		return false;
	}
	if(NumberValidation(val, Error_Details_Trans(message[5251],field), 0)) {
		if(val.length != 4 || val < 1800) {
			Error_Details(Error_Details_Trans(message[5252],field));
			return false;
		} else {
			return true;
		}
	} else {
		return false;
	}
}

function fnValidateYearAllowBlank(val, field) {
	if(val != '') {
		if(NumberValidation(val, Error_Details_Trans(message[5251],field), 0)) {
			if(val.length != 4 || val < 1800) {
				Error_Details(Error_Details_Trans(message[5252],field));
				return false;
			} else {
				return true;
			}
		} else {
			return false;
		}
	} else {
		return true;
	}
}

function fnSearchParty(lookupId, searchString) 
{
    var temp = "";
    var searchStringArr = searchString.split("~");
    for (var i=0; i < searchStringArr.length; i++)
     {
       temp += searchStringArr[i] + " ";
     } 
 	var strurl = "/gmSearchParty.do?lookupId="+lookupId+"&searchString="+temp;		
	windowOpener(strurl,"Party","resizable=yes,scrollbars=yes,top=300,left=200,width=1000,height=600");		
}

/* 
 * Common function to generate the request string
 * @param form - the form object
 * @param params - an array of parameter names which should not be included in the request string
 */
function fnGenerateRequestString(form, params) {
	var requestStr = new String(form.action + '?');
	for (var i=0;i<form.length;i++) {
		var obj = form.elements[i];
		var objName = obj.name;
		var objType = obj.type;
		var objValue = obj.value;
		//alert(form.elements[i].name + ' = ' + form.elements[i].type);
		//alert(objName + ' present = ' + fnCheckExists(objName, params));
		if(fnCheckExists(objName, params)) {
			continue;
		}
		
		if(objType == 'checkbox')
		{
			if(obj.checked == true) { objValue = 'on' } else { objValue = 'off' }
			//alert(objName + ' = ' + objValue);
		}
		
		if(objValue != '' && objType != 'button'){
                requestStr += '&'+ objName + '=' + encodeURIComponent(objValue); // PC-351-Changed encodeURI to encodeURIComponent-Getting Error When Update Rep Address
		}
	}
	requestStr = requestStr.replace("?&","?"); // replace the URL
	return requestStr;
}

function fnCheckExists(value, params) {
	if(params != null) {
		for(var k=0;k<params.length;k++) {
			if(value == params[k]) {
				return true;
			}
		}
		return false;
	} else {
		return false;
	}
}

/*** Do not use below function. Instead, use fnGenerateRequestString() ***

function fnCreateRequestString(form, params) {
	var requestStr = new String(form.action + '?');
	for(var k=0;k<params.length;k++) {

		var paramName = params[k];
		for (var i=0;i<form.length;i++) {
			var objName = form.elements[i].name;
			if(paramName == objName) {
				if(k != 0) {
					requestStr += '&';
				}
				requestStr += form.elements[i].name + '=' + form.elements[i].value;
				break;
			}
		}
	}
	return requestStr;
}*/

function fnGlobusLoadSite() {
	var form = document.getElementsByName('frmSiteMapingForm')[0];
	var params = new Array("strOpt");
	var requestStr = fnGenerateRequestString(form, params);
	requestStr += '&strOpt=load&method=loadSiteMapingSetup';
	//alert('requestStr = ' + requestStr);
	loadajaxpage(requestStr, 'ajaxdivcontentarea');
}


function fnSiteMappingSubmit() {
	var form = document.getElementsByName('frmSiteMapingForm')[0];
	
	fnValidateDropDown(form, 'studyListId', message[5253])
	fnValidateDropDown(form, 'siteListId', message[5254]);
	if (ErrorCount > 0) {
		Error_Show();
        Error_Clear();
        return false;
    }

	var included = form.includedPage.value;
	var params = new Array("strOpt");
	var requestStr = fnGenerateRequestString(form, params);
	requestStr += '&strOpt=save&method=loadSiteMapingSetup';
	if(included == 'false') {
		form.action = requestStr;
		form.submit();
	} else {
		loadajaxpage(requestStr, 'ajaxdivcontentarea');
	}
}

function fnSiteMappingReset() {
	var form = document.getElementsByName('frmSiteMapingForm')[0];
	form.studyListId.value = 0;
	form.siteListId.value = 0;
	if (form.primaryFlag != undefined)
	{
		form.primaryFlag.checked = false;
	}
}

function fnSiteMappingVoid() {
	var form = document.getElementsByName('frmSiteMapingForm')[0];

	if(form.sitePartyId.value == 0 & form.siteListId.value == 0 & form.studyListId.value == 0) {
		Error_Details(message[5255]);
	}
    if (ErrorCount > 0) {
		Error_Show();
        Error_Clear();
        return false;
    }

	form.action ="/GmCommonCancelServlet";
	form.hTxnId.value = form.sitePartyId.value;
	form.hTxnName.value = 'Void ' + form.studyListId.options[form.studyListId.selectedIndex].text+ ' - ' + form.siteListId.options[form.siteListId.selectedIndex].text+ ' - ' + form.partyName.value; 
	form.submit();
}

function fnSiteMappingEditId(val,partyName) {
	var form = document.getElementsByName('frmSiteMapingForm')[0];
	
	var params = new Array("strOpt", "partyId","sitePartyId","partyName");
	var requestStr = fnGenerateRequestString(form, params);
	requestStr += '&strOpt=edit&method=loadSiteMapingSetup&sitePartyId=' + val+'&partyId='+form.partyId.value+'&partyName='+partyName;
	//alert('requestStr = ' + requestStr);
	var included = form.includedPage.value;
	if(included == 'false') {
		//alert('Its a standalone page!');
		form.action = requestStr;
		form.submit();
	} else {
		//alert('Its not a standalone page!');
		loadajaxpage(requestStr, 'ajaxdivcontentarea');
	}
}

function fnPageLoad(){
	var form = document.getElementsByName('frmPartySetup')[0];
	var type = form.partyType.value;
	//Below code is to set the current tab, 7008:Group
	if(type == '7008')
		document.all.hcurrentTab.value = "Account Group";
	else
		document.all.hcurrentTab.value = "Party_Setup";
		
}

//Function will call on page load. It will append companyInfo in each tab href and initiate ajaxtab.
function fnAjaxTabLoad(){
	fnAjaxTabAppendCmpInfo("#maintab");
	fnMainTab();
}

//initiate ajaxtab
function fnMainTab(){
		    
	var maintab=new ddajaxtabs("maintab", "ajaxdivcontentarea");				
	maintab.setpersist(false);								
	maintab.init();					    
	
	maintab.onajaxpageload=function(pageurl){					    
		if (pageurl.indexOf("gmProfileSetup.do")!=-1){	
			fnAjaxTabAppendCmpInfo("#profiletab");
			var profiletab=new ddajaxtabs("profiletab", "profiledivcontentarea");
			profiletab.setpersist(false);	
			profiletab.setselectedClassTarget("link") //"link" or "linkparent"
			profiletab.init();
		}
		if (pageurl.indexOf("gmPartySetup.do")!=-1){
			fnAddCompanyParams();
			fnPageLoad();// to set the value of hcurrentTab
		}
		if (pageurl.indexOf("gmPartyShipParamsSetup.do")!=-1){
			if(document.getElementById("ajaxdivcontentarea") != undefined){
	  			document.getElementById("ajaxdivcontentarea").style.height = "auto";
			}	
			fnPageLoad1();
		}
	}											    

}	



