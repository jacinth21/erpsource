/*******************************************************************************************************
 * File						: Error.js 
 * Desc						: This is for Displaying  Error Message from message file 
 * Version					: V1.0  
 ********************************************************************************************************/
/*
	Form Level Variable
*/

document.write('<script language="JavaScript" src="javascript/GmGrid.js"></script>');
document.write("<script language='JavaScript' src='javascript/GmDhtmlxWindowPopup.js'></script>");


var WinError;
var ErrorDetail = new Array();		 /* Holds column length */
var ErrorCount = 0;

var ErrorContent = "";
var device = (navigator.userAgent.indexOf("iPad")!=-1)?'IPAD':'PC';
function Error_Clear() {
	ErrorCount = 0;
	//ErrorDetail = null;
	//alert("inside");
}

function Error_Details(Err_Description, RowNum ) {
	ErrorDetail[ErrorCount] = new Array(2);

	//if (RowNum == null)	{
	//	RowNum = "**";
	//}
	ErrorDetail[ErrorCount][0] = Err_Description;
	//ErrorDetail[ErrorCount][1] = RowNum ;

	ErrorCount = ErrorCount + 1;
}
//Function used to form the dynamic validation message 
function Error_Details_Trans(message,array){
	
	if(array.constructor === Array ) {
		for (var i=0; i<array.length; i++) {
			var index = "["+i+"]";
			message = message.replace(index,array[i]);
		}
	}else{
		message = message.replace("[0]",array);
	}
	return message;	
}

/*
	Will show the error in a different popup
*/
function Error_Show() {
	

	
	if(device=='IPAD')
	{
	ErrorContent="";
	w1 = createDhtmlxWindow();
	w1.setText("Alert(s)"); 

	Generate_HTML_Initial_Setting_Ipad();
	Generate_Error();
	Generate_HTML_End_Setting_Ipad();

	w1.attachHTMLString(ErrorContent);
	}else
		{
		if (WinError != null)
			 // window.close();
			  	WinError.close();
				WinError = window.open("", "Alert", "toolbars=no,top= 300,left=400,height=170,innerHeight=250,width=450,innerWidth=450,scrollbars=yes,resizeable=no,status=");
			
			this.gWinError = WinError;
			//WinError.opener = self;

				// Customize your Calendar here..
			Generate_HTML_Initial_Setting();
			Generate_Error();
			Generate_HTML_End_Setting();

			WinError.show;
		}
	return false;
}


function Error_Show_Netscape() {
	
	//if (WinError != null)
	 //WinError.close();
	 // self.close();
	  //window.close();
	WinError = window.open("", "Alert", "toolbars=no,top= 100,left=200,height=450,innerHeight=450,width=650,innerWidth=650,scrollbars=yes,resizeable=no,status=");
	this.gWinError = WinError;
	

		// Customize your Calendar here..
	Generate_HTML_Initial_Setting();
	Generate_Error();
	
	Generate_HTML_End_Setting_Nescape();
		  
	WinError.show;
	ErrorCount = 0;
}



/*
	Will send the screen to the printer
*/
function Error_print() {
	WinError.print();
}

function Generate_HTML_Initial_Setting() {
	wwrite("<html>");
	wwrite("<head><title>GlobusOne Enterprise Portal-Alert</title>");
	wwrite("</head>");
	
	wwrite("<link rel='stylesheet' href='../styles/Globus.css'>");
	// Header Message
	wwrite("<body topmargin=10 leftmargin=10 rightmargin=10><form name=frmError onSubmit=window.close();>");
	wwrite("<CENTER><b><FONT color='Red' size=2 face='verdana'>Alert(s)</FONT></b><Br></CENTER>");
	
	// Table Header Details
	wwrite("<TABLE width='100%'>");
	wwrite("<tr>");
	wwrite("<td colspan='3' height='1' class='Line'></td>");
	wwrite("</tr>");
	wwrite("<TR class=MenuItem>");
	wwrite("<TD height='20' class=MenuItemText>Description</TD>");
	//wwrite("<TD width='20%' class=RightText>Row Number</TD>");
	wwrite("</TR>");
	//wwrite("<tr>");
	//wwrite("<td colspan='3' height='1' class='Line'></td>");
	//wwrite("</tr>");
	

}

function Generate_HTML_Initial_Setting_Ipad() {
	swrite("<html>");
	swrite("<head><title>GlobusOne Enterprise Portal-Alert</title>");
	swrite("</head>");
	
	swrite("<link rel='stylesheet' href='../styles/Globus.css'>");
	// Header Message
	swrite("<body topmargin=10 leftmargin=10 rightmargin=10><form name=frmError onSubmit=CloseParentDhtmlxWindow();>");
	swrite("<CENTER><b><FONT color='Red' size=2 face='verdana'>Alert(s)</FONT></b><Br></CENTER>");
	
	// Table Header Details
	swrite("<TABLE width='100%'>");
	swrite("<tr>");
	swrite("<td colspan='3' height='1' class='Line'></td>");
	swrite("</tr>");
	swrite("<TR class=MenuItem>");
	swrite("<TD height='20' class=MenuItemText>Description</TD>");
	//swrite("<TD width='20%' class=RightText>Row Number</TD>");
	swrite("</TR>");
	//swrite("<tr>");
	//swrite("<td colspan='3' height='1' class='Line'></td>");
	//swrite("</tr>");
	

}

function Generate_HTML_End_Setting() {
	wwrite("</table>");
	wwrite("<div align='center'><br>");
	wwrite("<input type='submit' name='Btn_Close' value='Close' onClick = 'javascript:window.close(); return false;' class='Button' style='WIDTH: 60px; HEIGHT: 20px'>")
	wwrite("</div>");
	wwrite("</form></body>");
	wwrite("</html>");
}

function Generate_HTML_End_Setting_Ipad() {
	swrite("</table>");
	swrite("<div align='center'><br>");
	swrite("<input type='Button' name='Btn_Close' value='Close' onClick = 'javascript:CloseParentDhtmlxWindow();' class='Button' style='WIDTH: 60px; HEIGHT: 20px'>")
	swrite("</div>");
	swrite("</form></body>");
	swrite("</html>");
}

function Generate_HTML_End_Setting_Nescape() {
	wwrite("</table>");
	wwrite("<input type='Button' name='Btn_Close' value='Close' onClick = 'javascript:window.close();' class='Button' style='WIDTH: 60px; HEIGHT: 20px'>")
	wwrite("</form></body>");
	wwrite("</html>");
}

/* 
	To write the error into the screen
*/
function Generate_Error() {
	var vCode = "";
	var vFlag = 0 ;
		
	for(var i = 0 ; i <= (ErrorCount-1); i++) {
	
		if (vFlag == 0 ) 
		{
			vCode = vCode + "<TR>"; 
			vFlag = 1;
		}
		else
		{
			vCode = vCode + "<TR>";
			vFlag = 0;
		}
		
		vCode = vCode + "<TD align=left width='20%' class='RightText'>" + ErrorDetail[i][0] + "</TD>" ;
		//vCode = vCode + "<TD align=left width='*%' class=RightText>" + ErrorDetail[i][1] + "</TD>";
		vCode = vCode + "</TR>";
		vCode = vCode + "<tr>";
		vCode = vCode + "<td colspan='3' height='1' bgcolor=#cccccc></td>";
		vCode = vCode + "</tr>";

	}
	if(device=='IPAD')
	{
		swrite(vCode);
	}else
	{
		wwrite(vCode);
	}

}

/*
	Will write the same into our html
*/
function wwrite (string) {
	this.gWinError.document.writeln(string);
}

/*
Will write the same into our html
*/
function swrite (string) {
ErrorContent=ErrorContent+string;

}
