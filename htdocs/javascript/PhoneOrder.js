var partnums = "";
var vmode="";
var status = true; 
var ctrlId;
var accountType = '';
function Reload (partnum, flag, ordertype ) {
	
	partnum = trim(partnum); 
	
if(partnum!='')
{
	var valobj = partnums.split(","); 
	var arrlen = valobj.length;
	
	if (arrlen > 0)
	{
		for (var j=0;j< arrlen;j++ )
		{ 
				if ( valobj[j] == partnum)
				{
					status = false;
					break;
				} 
		}
	}
	else
	{
		 status = true;
	}
	 
	 if(flag=='-1')
	 {	
		 partnums = partnums.replace(partnum,"");
	  }
	 else
	 {	 if(status==true )
		 {	 
			 partnums = partnums  + partnum + ",";
		 } 
	 }
	 
	if(ordertype =='2521')	
	{
		ordertype = 'Bill_Ship';
	}
	else 
	{
		ordertype = '';
	}
	
	var f = document.getElementById('iframe1'); 
	   
 	f.src  = document.frmPhOrder.RE_SRC.value + "&orderType=" + ordertype + "&PartNums=" + partnums + "&shipTo=" +document.all.shipTo.value + "&names=" +  document.all.names.value + "&addressid=" +  document.all.addressid.value +"&RefName=RFS" ;
 	 
 	var divfr = document.getElementById('divf'); 
	if(partnum!='')
	{ 
 		divfr.style.display="block"; 
	} 
 	 
 	f.src = f.src;  
	}
	
}
 
function fnValidateCtrlNumber(pnum,cnum,trcnt){
	fnCallAjaxCtrlNum('validateControlNumber',pnum,cnum,trcnt);
}
var varPartNum;
var varCount;
var objPartCell;
var distid;
var accid;
var repid;

function fnOpenAddress()
{
	accid = document.frmPhOrder.Txt_AccId.value;
	repid = document.frmPhOrder.hRepId.value;
	//Cbo_BillTotb
	val = document.frmPhOrder.shipTo.value;
//	id = document.frmPhOrder.Cbo_Rep.value;
	id = document.frmPhOrder.names.value;
	
	if (accid == '0')
	{
		 Error_Details(message[61]);
	}
	
	if (val == '0')
	{
		 Error_Details(message[62]);
	}
	if (val == '4121' && id =='0')
	{
		 Error_Details(message[63]);
	}
	
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}

	windowOpener("/GmSearchServlet?hAction=getAddress&hAccId="+accid+"&hRepId="+repid+"&hShip="+val+"&hShipId="+id,"Address","resizable=yes,scrollbars=yes,top=340,left=420,width=300,height=135");
}

function fnCallShip()
{
	obj = document.frmPhOrder.shipTo;
//	var obj2 = document.frmPhOrder.Cbo_Rep;
	var obj2 = document.frmPhOrder.names;
	obj2.disabled = false;
	if (obj.options[obj.selectedIndex].text == 'Sales Rep')
	{
		var repid = document.frmPhOrder.Cbo_BillTo[document.frmPhOrder.Cbo_BillTo.selectedIndex].id;
		for (var j=0;j<obj2.length;j++)
		{
			if (obj2.options[j].value == repid)
			{
				obj2.options[j].selected = true;
				break;
			}
		}
		obj2.disabled = false;
		obj2.focus();
	}
	else
	{
		obj2.disabled = true;
	}
}


function fnPlaceOrder()
{
	var varPartNums;
	var varRowCnt = document.frmPhOrder.hRowCnt.value;
	var varAccId = document.frmPhOrder.Txt_AccId;
	var varAccIdVal = varAccId.value;
	var varTotalPrice =  document.frmPhOrder.hTotal.value;
	
	varRowCnt ++;
	var k;
	var varPartVal = "";
	for (k = 1; k < varRowCnt; k ++)
	{
		objPartCell = eval("document.frmPhOrder.Txt_PartNum"+k);

		if (objPartCell == "" || objPartCell == null)
		{
				comma = "";
		}
		else if (objPartCell.value == "")
			{
				comma = "";
			}
		else
		{
		varPartNums = varPartNums + objPartCell.value
		}
	}
	
	var varParsePartVal = varPartVal.substring(0,varPartVal.length -1);
	
	if (varAccIdVal == "")
	{
		 Error_Details(message[66]);
	}
	else if (varPartNums == "" || varPartNums == null)
	{
		 Error_Details(message[67]);
	}
	else if (varTotalPrice == "0.0")
	{
		 Error_Details(message[691]);
	}
	else
	{
		document.frmPhOrder.hAction.value = "PlaceOrder";
		document.frmPhOrder.hOpt.value = "PhoneOrder";
	}

	


	fnSubmit();
}

var vFilterShowFl = false;
function fnShowFilters(val)
{
	var obj = eval("document.all."+val);
	var obj1 = eval("document.all."+val+"img");
	if (obj.style.display == 'none')
	{
		obj.style.display = 'block';
		if(obj1){
			obj1.src = '/images/minus.gif';
		}
	}
	else
	{
		obj.style.display = 'none';	
		if(obj1){
			obj1.src = '/images/plus.gif';
		}
	}
}

function fnToggleCart()
{
	fnShowFilters('PartnPricing');
	fnShowFilters('PartnPricingHeader');
	fnShowFilters('PartnPricingDiv');
	fnShowFilters('PartnPricingFooter');
}

function fnSubmit()
{	 
	fnStartProgress('Y');
	var stropt = (document.frmPhOrder.hOpt != undefined)?document.frmPhOrder.hOpt.value:'';
	var str = window.frames.fmCart.fnCreateOrderString();
	if(str == 'ApplyAcc_Err'){
 		Error_Details("Please click <b>Apply Discount</b> button to calculate the discount on this order");
 	}
 	if(str == 'Discount_Err')
 		Error_Details("Discount % and Discounted Price don't match");
 	var strAllParts = window.frames.fmCart.fnCreateAllParts();

	//MNTTASK - 8623 - Create input string for sterile parts and save item t502a item order attribute 
 	var ctrlStr = window.frames.fmCart.fnCreateCtrlNumString();
 	document.frmPhOrder.hCtrlNumberStr.value = ctrlStr;
 	
 	// validate the customer PO - space
 	var text_PO = TRIM(document.frmPhOrder.Txt_PO.value);
	document.frmPhOrder.Txt_PO.value = text_PO;
	
	var custPoLen = text_PO.length;

	if(custPoLen > 20){
		Error_Details("<b>Customer PO</b> cannot be more than 20 characters.");
	}
	 
	if(stropt!= 'QUOTE'){// For Quote, no need to validate white space in Customer PO
		fnValidatePOSpace (text_PO);
	}
 	var orderID = "";
 	var parentOrdID ="";
 	if (document.frmPhOrder.Txt_OrdId){
 		orderID = document.frmPhOrder.Txt_OrdId.value;
 	}
 	if(document.frmPhOrder){
	 	if (document.frmPhOrder.Txt_ParentOrdId){
	 		parentOrdID = document.frmPhOrder.Txt_ParentOrdId.value;
	 	}
 	}
 	if (document.frmPhOrder.hQuoteCnt)
 	{
 	var strQuoteStr = fnCreateQuoteString();
 	document.frmPhOrder.hQuoteStr.value = strQuoteStr;
 	}
	var pNumErrorMsg='';
	document.frmPhOrder.hInputStr.value = str; 
	document.frmPhOrder.hTotal.value = window.frames.fmCart.document.all.hTotal.value;
	if (window.frames.fmCart.document.all.Cbo_Construct)
	{
		document.frmPhOrder.hConCode.value = window.frames.fmCart.document.all.Cbo_Construct.value;	
	}
	//fnValidateDropDn('Cbo_Rep',' Sales Rep - Billing Info '); Sales Rep Changes
	var obj = document.frmPhOrder.Cbo_BillTo;
	if (obj.value =='')
	{
		Error_Details(message[61]);
	}

	var obj = document.frmPhOrder.hInputStr;
	if (obj.value =='')
	{
		Error_Details('Empty cart cannot be saved.');
	}

	var doformat = fnCheckDOFormat();
	var opt = document.frmPhOrder.hOpt.value;
	if (opt == 'PHONE')
	{
		if (doformat)
		{
			Error_Details('Incorrect DO Format.');
		}
		if(parentOrdID != '' && parentOrdID == orderID){
			Error_Details('DO ID and Parent DO ID should not be same.');
		}
	}
	if(orderID.indexOf(' ')>= 0)
	{
		Error_Details('Remove the space between DO ID');	 
	}
	var strArray = str.split('|');
	for (k=0;k<strArray.length-1 ;k++ )
	{
		if (strArray[k].indexOf('Not Valid') >= 0)
		{			
			var elementArray = strArray[k].split(',');
			pNumErrorMsg = pNumErrorMsg + elementArray[0]+',';
		}
	}
	if (pNumErrorMsg != '')
	{
		Error_Details('Tag ID/Etch ID entered for the given parts are not valid - <b>'+ pNumErrorMsg.substring(0,(pNumErrorMsg.length-1))+'</b>' );
	}
	
	/*if (document.all.Txt_LogReason.value == '' ){
		Error_Details(message[911]);
	}
	*/
	document.frmPhOrder.names.disabled = false;
	document.frmPhOrder.addressid.disabled = false;
	var shipToValue = document.frmPhOrder.shipTo.value;
	var strNames = document.frmPhOrder.names.value;
	if(shipToValue == 4120 || shipToValue == 4122 || shipToValue == 4123){
		fnValidateDropDn('names',' Name ');
	}else if(shipToValue == 4121){
		fnValidateDropDn('addressid',' Address ');
		fnValidateDropDn('names',' Name ');
	}
	if(shipToValue == 4120){
		document.frmPhOrder.names.disabled = true;
		document.frmPhOrder.addressid.disabled = true;
	}
	if (ErrorCount > 0)
	{
			fnStopProgress();
			Error_Show();
			Error_Clear();
			return false;
	}
	if(document.getElementById("comments") != undefined){
		var retval = checkMaxLength(document.getElementById("comments").value,4000, "Comments");
		if(retval == false){
			fnStopProgress();
			return false;
		}
	}
	var consigncnt = 0;
	// enabling Order Type dropdowns
	var pobj = '';	
	//var cnt = document.frmPhOrder.hCnt.value;
	var cnt = document.frames.fmCart.frmCart.hRowCnt.value;

	for (i=0;i<cnt;i++)
	{
		var obj = eval("document.frames.fmCart.frmCart.Cbo_OrdPartType"+i);
		pobj = eval("document.frames.fmCart.frmCart.Txt_PNum"+i);

		if (obj)
		{
			obj.disabled = false;
			if (obj.value == '50300' && pobj.value != '')
			{
				consigncnt++;
			}
		}
		obj = eval("document.frames.fmCart.frmCart.Chk_BO"+i);
		if (obj)
		{
			obj.disabled = false;
		}
	}

	document.frmPhOrder.shipCarrier.disabled = false;
	document.frmPhOrder.shipMode.disabled = false;
	document.frmPhOrder.shipTo.disabled = false;
//	document.frmPhOrder.Cbo_Rep.disabled = false;
	document.frmPhOrder.names.disabled = false;

	if (consigncnt == 0 && document.frmPhOrder.hAction.value == "PlaceOrder")
	{
		// setting order to 'Bill Only-Loaner' if no parts are from consignments
		document.frmPhOrder.Cbo_OrdType.value = 2530; 
		document.frmPhOrder.shipCarrier.value = '5040';
		document.frmPhOrder.shipMode.value = '5031';
		document.frmPhOrder.shipTo.value = '4124';
	//	document.frmPhOrder.Cbo_Rep.value = '0';
		document.frmPhOrder.names.value = '0';
	}
	  if(document.frmPhOrder.Btn_PlaceOrd)
			document.frmPhOrder.Btn_PlaceOrd.disabled = true;
	  
	  // When click place order function will call and show progress image.
	  //fnClearDivProgress();
	  
	//alert(consigncnt);
	/*var orderType = document.frmPhOrder.Cbo_OrdType.value;
	if(!orderType == '2521') // 2521 BIll & SHIP
	{
		 
		document.frmPhOrder.action = "/gmRuleEngine.do?RefName=RFS&txnStatus=VERIFY"+"&PartNums="+strAllParts; 
	}
	else
	{	 
		document.frmPhOrder.action = "/gmRuleEngine.do?RefName=RFS&txnStatus=PROCESS"+"&PartNums="+strAllParts; 
	}*/
 	 
 	document.frmPhOrder.submit();
}
function fnPrintPack()
{
	var val = document.frmPhOrder.Txt_OrdId.value;
	if (val != '')
	{
		windowOpener('/GmOrderItemServlet?hAction=PICSlip&hId='+val,"Pack","resizable=yes,scrollbars=yes,top=150,left=150,width=760,height=610");	
	}
}

function fnLoanerInfo(partnum,repid)
{
	windowOpener('/GmLoanerPartRepServlet?hAction=Load&hPartNum='+encodeURIComponent(partnum)+'&hRepId='+repid,"Pack","resizable=yes,scrollbars=yes,top=150,left=150,width=850,height=610");	
}
 
function fnSetShipSel(val)
{
	var val = document.frmPhOrder.Cbo_OrdType.value;
	var cnt = document.frames.fmCart.frmCart.hRowCnt.value;
	//setting default value for OUS country
	var sCarrier = vshipCarrier;
	var sMode = vshipMode;
	var sShipTo = '4122'; //4122-Hospital
	var varAccId = '';
	var sNames = '';
  	var parts =    window.frames.fmCart.fnCreateAllParts();
 	 
    Reload(parts, '1',val); 
	
	var boobj = '';
	var pobj = '';
	var qobj = '';
	var lobj = '';
	//cnt++;
	if (val == 2530) //2530-Bill Only - Loaner
	{
		//setting default value for US
		if(vCountryCode == 'en'){
			sCarrier = '5040';   //5040 - N/A
			sMode = '5031';      //5031 - N/A
		}
		document.frmPhOrder.shipCarrier.value = sCarrier;
		document.frmPhOrder.shipMode.value = sMode;
		document.frmPhOrder.shipTo.value = '4124';  //4124-N/A
	//	document.frmPhOrder.Cbo_Rep.value = '0';
		document.frmPhOrder.names.value = '0';
		document.frmPhOrder.shipCarrier.disabled = true;
		document.frmPhOrder.shipMode.disabled = true;
		document.frmPhOrder.shipTo.disabled = true;
	//	document.frmPhOrder.Cbo_Rep.disabled = true;
		document.frmPhOrder.names.disabled = true;
		for (i=0;i<cnt;i++)
		{
			var obj = eval("document.frames.fmCart.frmCart.Cbo_OrdPartType"+i);
			pobj = eval("document.frames.fmCart.frmCart.Txt_PNum"+i);
			qobj = eval("document.frames.fmCart.frmCart.Txt_Qty"+i);

			if (pobj && qobj)
			{
				if (pobj.value != '' && qobj.value != '')
				{
					lobj = eval("document.frames.fmCart.frmCart.hLoanCogs"+i);
					if (lobj.value == '')
					{
						Error_Details('The cart contains a part that is not a Loaner. Order Type changed to Bill & Ship');
						document.frmPhOrder.Cbo_OrdType.value = 2521;
						Error_Show();
						Error_Clear();
						
						document.frmPhOrder.shipCarrier.disabled = false;
						document.frmPhOrder.shipMode.disabled = false;
						document.frmPhOrder.shipTo.disabled = false;
					//	document.frmPhOrder.Cbo_Rep.disabled = false;
						document.frmPhOrder.names.disabled = false;
						//setting default value for US
						if(vCountryCode == 'en'){
							sCarrier = '5001';  //5001 - FedEx
							sMode = '5004';     //5004 - Priority Overnight
						}
						document.frmPhOrder.shipCarrier.value = sCarrier;
						document.frmPhOrder.shipMode.value = sMode;
						document.frmPhOrder.shipTo.value = '4121';  //4121 -Sales Rep
						varRepId = document.frmPhOrder.hRepId.value;
					//	document.frmPhOrder.Cbo_Rep.value = varRepId;
						document.frmPhOrder.names.value = varRepId;
						return false ;
					}
				}
			}

			if (obj)
			{
				obj.value = '50301';
				obj.disabled = true;
			}
			boobj = eval("document.frames.fmCart.frmCart.Chk_BO"+i);
			if (boobj)
			{
				boobj.checked = false;
				boobj.disabled = true;
			}
			
		}
	
	}
	else
	{
		document.frmPhOrder.shipCarrier.disabled = false;
		document.frmPhOrder.shipMode.disabled = false;
		document.frmPhOrder.shipTo.disabled = false;
	//	document.frmPhOrder.Cbo_Rep.disabled = false;
		document.frmPhOrder.names.disabled = false;
		
		varRepId = document.frmPhOrder.hRepId.value;
		varAccId = document.frmPhOrder.Txt_AccId.value;
		sNames = varAccId;
		//setting default value for US
		if(vCountryCode == 'en'){
			sCarrier = '5001';  //5001 - FedEx
			sMode = '5004';     //5004 - Priority Overnight
			sShipTo = '4121'    //4121 - Sales Rep
			sNames = varRepId;
		}		
		document.frmPhOrder.shipCarrier.value = sCarrier;
		document.frmPhOrder.shipMode.value = sMode;
		document.frmPhOrder.shipTo.value = sShipTo;		
	//	document.frmPhOrder.Cbo_Rep.value = varRepId;
		document.frmPhOrder.names.value = sNames;		
		for (i=0;i<cnt;i++)
		{
			obj = eval("document.frames.fmCart.frmCart.Cbo_OrdPartType"+i);
			boobj = eval("document.frames.fmCart.frmCart.Chk_BO"+i);
			pobj = eval("document.frames.fmCart.frmCart.Txt_PNum"+i);
			obj.disabled = false;

			if (pobj.value == '')
			{
				if (boobj)
				{
					boobj.disabled = false;
				}
				obj.value = '50300';
			}
			else
			{
				if (obj.value == '50301')
				{
					if (boobj)
					{
						boobj.disabled = true;
					}
				}
				else
				{
					obj.value = '50300';
					if (boobj)
					{
						boobj.disabled = false;
					}
				}
			}

		}
	}
}

function fnChangeStatus(val,cnt)
{
	obj = eval("document.frames.fmCart.frmCart.Chk_BO"+cnt);
	if (obj)
	{
		if (val == 50301)
		{
			obj.checked = false;
			obj.disabled = true;
		}
		else
		{
			obj.disabled = false;
		}
	}
}

function fnCheckConstructAccount(conobj)
{
	/*
	var obj = document.frmPhOrder.Cbo_BillTo;
	var conlbl = obj[obj.selectedIndex].label;
	
	if (obj.value == 0)
	{
		conobj.selectedIndex = 0;
	}else
	{
		if (conlbl == 0)
		{
			 Error_Details('This Account does not have Capitated Pricing.');
		}
	}

	if (ErrorCount > 0)
	{
			Error_Show();
			conobj.selectedIndex = 0;
			Error_Clear();
			return false;
	}
	*/

}


/************************************************************************************************************************/
function fnCallAjax(val)
{
	if(val =='GETACC'){
		accountType = '';
	}	
	var allParts = '';
	var accid = document.all.Txt_AccId.value;
	if(document.all.Txt_OrdId){
		var doid = document.all.Txt_OrdId.value;
	}
	// When submit in place order it will get the all parts from the below function and pass in ajax.
	if(window.frames.fmCart)
		allParts = window.frames.fmCart.fnCreateAllPartsWithCtrlNums();

	// The PhoneOrder.js file calling in many places. Need to be check undefined.
	var orderType = (document.frmPhOrder) ? document.frmPhOrder.Cbo_OrdType.value:'';
	var strShipTo = (document.all.shipTo) ? document.all.shipTo.value:'';
	var strName = (document.all.names) ? document.all.names.value:'';
	var strAddress = (document.all.addressid) ? document.all.addressid.value:'';
	
	 
	if (document.frmPhOrder)
	{
		var repid = document.all.hRepId.value;
	}
	
    var url = fnAjaxURLAppendCmpInfo("/GmOrderAjaxServlet?orderType="+encodeURIComponent(orderType)+"&opt=" + encodeURIComponent(val)+ "&acc=" + encodeURIComponent(accid)+ "&rep="+encodeURIComponent(repid)+"&doid="+trim(doid)+ "&PartNums="+encodeURIComponent(allParts)+"&shipTo=" +strShipTo + "&names=" +  strName + "&addressid=" + strAddress);
	if (typeof XMLHttpRequest != "undefined") 
	{
	   req = new XMLHttpRequest();
	} 
	else if (window.ActiveXObject) 
	{
	   req = new ActiveXObject("Microsoft.XMLHTTP");
	}
	req.open("GET", url, true);
	if (val=='CHECKDOID'){             //MNTTASK-7099 -- For displaying the avail/exists images.
		req.onreadystatechange = showimage;
	}else if(val == 'validateControlNumber'){
		req.onreadystatechange = callCtrlNum;
	}else{
		req.onreadystatechange = callback;
	}
	req.send(null);
}
 
function callback() {
    if (req.readyState == 4) {
        if (req.status == 200) {
	        var xmlDoc = req.responseXML;
			parseMessage(xmlDoc);
        }
        else
        {
	        setErrMessage('<span class=RightTextBlue>Account does not exist</span>');
        }
    }
}

function callCtrlNum() {
    if (req.readyState == 4) {
        if (req.status == 200) {
	        var xmlDoc = req.responseText;
	       if(trim(xmlDoc) != '' ){
	    		Error_Details(xmlDoc);
	    		Error_Show();	
	    		Error_Clear();
	    		return false;
	       }else{
	    	   fnCheckHoldFl ();
	    	   fnSubmit();
	       }
         }
    }
}

function parseMessage(xmlDoc) 
{
	

	var pnum = xmlDoc.getElementsByTagName("acc");
	var cons = xmlDoc.getElementsByTagName("const");
	for (var x=0; x<pnum.length; x++) 
	{
		var rgname = parseXmlNode(pnum[x].childNodes[0].firstChild);
		var adname = parseXmlNode(pnum[x].childNodes[1].firstChild);
		var dname = parseXmlNode(pnum[x].childNodes[2].firstChild);
		var trname = parseXmlNode(pnum[x].childNodes[3].firstChild);
		var rpname = parseXmlNode(pnum[x].childNodes[4].firstChild);
		repid = parseXmlNode(pnum[x].childNodes[5].firstChild);
		var prname = parseXmlNode(pnum[x].childNodes[6].firstChild);
		var prid = parseXmlNode(pnum[x].childNodes[7].firstChild);
		var gpocatid = parseXmlNode(pnum[x].childNodes[8].firstChild);
		var acname = parseXmlNode(pnum[x].childNodes[9].firstChild);
		accid = parseXmlNode(pnum[x].childNodes[10].firstChild);
		distid = parseXmlNode(pnum[x].childNodes[11].firstChild);
		vmode =  parseXmlNode(pnum[x].childNodes[12].firstChild);		
		var accCurrency =  '';
		var rpcNm = '';
		var hlthNm = '';
		if(pnum[x].childNodes[17]!=undefined && pnum[x].childNodes[17]!=null)
		{
			accCurrency = parseXmlNode(pnum[x].childNodes[17].firstChild); // Account Currency symbol will displayed in the screen.
		}
		rpname = rpname + ' ('+repid+')';
		//Account Affiliation
		var gpraff = parseXmlNode(pnum[x].childNodes[13].firstChild);
		var hlcaff =  parseXmlNode(pnum[x].childNodes[14].firstChild);
		// get the account type
		accountType = parseXmlNode(pnum[x].childNodes[15].firstChild);
		var accComments = '';
		if(pnum[x].childNodes[16]!=undefined && pnum[x].childNodes[16]!=null)
		{
		   accComments =  parseXmlNode(pnum[x].childNodes[16].firstChild);
		}
		var contType =  '';
		
		if(pnum[x].childNodes[18]!=undefined && pnum[x].childNodes[18]!=null)
		{
			contType = parseXmlNode(pnum[x].childNodes[18].firstChild); // Account Currency symbol will displayed in the screen.
		}
		//pc-2376 Add new fields in Account Pricing Report
		if(pnum[x].childNodes[24]!=undefined && pnum[x].childNodes[24]!=null)
		{
			 rpcNm = parseXmlNode(pnum[x].childNodes[24].firstChild);
		}
		if(pnum[x].childNodes[25]!=undefined && pnum[x].childNodes[25]!=null)
		{
			 hlthNm = parseXmlNode(pnum[x].childNodes[25].firstChild);
		}
	}
	
	if (accid == '')
	{
		Error_Details('Invalid Account with the ID entered.');
		Error_Show();	
		Error_Clear();
		document.all.Txt_AccId.value = '';
		return false;

	}else
	{
		if (document.frmPhOrder)
		{
			document.all.Cbo_BillTotb.value = acname;
			document.all.Cbo_BillTo.value = accid;
			document.all.hRepId.value = repid;
			/*
			objRep = eval(document.all.Cbo_Rep); Sales Rep Changes
			document.all.Cbo_Rep.value = 0;
			document.all.names.value = 0;
			
			for(k=0; k<objRep.length;k++)
			{
				if(objRep[k].value == repid)
				{
					document.all.Cbo_Rep.value = repid;
					document.all.names.value = repid;
					break;
				}
			}
			*/
			//document.all.names.value = repid;
			fnGetNames(document.all.shipTo);
			//document.all.names.fireEvent('onChange');
		}

		if (document.frmAccount)
		{
			document.all.Cbo_AccId.value = accid;
		}
		
		//accCurrency is added to Pass Currency symbol to the function setMessage
		//pc-2376 Add new fields in Account Pricing Report- passing rpcNm,hlthNm inside setMessage function
		setMessage(rgname,adname,dname,trname,rpname,prname,prid,gpocatid,acname,gpraff,hlcaff,accComments,accCurrency,contType,rpcNm,hlthNm);
		if (document.frmPhOrder)
		{
			if (cons.length > 0)
			{
				fnSetConstructs(cons);
			}

			if (repid.length == 2)
			{
				repid = '0'+ repid;
			}
			else if (repid.length == 1)
			{
				repid = '00'+ repid;
			}

			var sysdt = document.frmPhOrder.hSysDate.value;
			var opt = document.frmPhOrder.hOpt.value;
			//alert(opt);
			
			if (opt == 'PHONE')
			{
				document.frmPhOrder.Txt_OrdId.value = repid + '-'+sysdt+'-';;
			}
			else
			{
				document.frmPhOrder.Txt_OrdId.value = accid + '-'+sysdt+'-';;
			}
		}
		if (vmode != '0') {
			document.frmPhOrder.shipMode.value = vmode;
		} 
		else{
			if(document.all.shipMode)
			document.frmPhOrder.shipMode.value = '5004';
		}
			
		if(accountType == '70114'){ // ICS Account
	     	Error_Details("Cannot proceed, since the account type is <b>'Inter-Company Sale'</b>. Please select a valid account.");	     	
			Error_Show();	
			Error_Clear();
			return false;
	     }
	}

}
//pc-2376 Add new fields in Account Pricing Report- passing rpcNm,hlthNm inside setMessage function
function setMessage(rgname,adname,dname,trname,rpname,prname,prid,gpocatid,acname,gpraff,hlcaff,accComments,accCurrency,contType,rpcNm,hlthNm)
{//accCurrency is to get the currency based on account
	var obj = eval("document.all.Lbl_Regn");
	obj.innerHTML = "&nbsp;" + rgname;

    obj = eval("document.all.Lbl_AdName");
    obj.innerHTML = "&nbsp;" + adname;

    obj = eval("document.all.Lbl_DName");
    obj.innerHTML = "&nbsp;" + dname;

    obj = eval("document.all.Lbl_TName");
    obj.innerHTML = "&nbsp;" + trname;

    obj = eval("document.all.Lbl_RName");
    obj.innerHTML = "&nbsp;" + rpname;

    obj = eval("document.all.Lbl_PName");
    obj.innerHTML = "&nbsp;" + prname;
    
    obj = eval("document.all.Lbl_AccInfo");
	if(obj!=undefined && obj!=null)
    obj.innerHTML = "&nbsp;" + accComments;

    // Account Affiliation
    obj = eval("document.all.Lbl_GPRAffName");
    if(obj!=undefined && obj!=null)
    	obj.innerHTML = "&nbsp;" + gpraff;

    obj = eval("document.all.Lbl_HlthCrAffName");
    if(obj!=undefined && obj!=null)
    	obj.innerHTML = "&nbsp;" + hlcaff;
    
    obj = eval("document.all.Lbl_ContractType");
    if(obj!=undefined && obj!=null)
    	obj.innerHTML = "&nbsp;" + contType;
    	
   //PC-2376 Add new fields in Account Pricing Report
    obj = eval("document.all.Lbl_RpcName");
    if(obj!=undefined && obj!=null)
    	obj.innerHTML = "&nbsp;" + rpcNm;   
    
    obj = eval("document.all.Lbl_HealthName");
    if(obj!=undefined && obj!=null)
    	obj.innerHTML = "&nbsp;" + hlthNm;

    if (document.frmPhOrder)
	{
    	document.frmPhOrder.hAccCurrency.value = (accCurrency == '')?strAccSessCurrency:accCurrency;
		obj = eval("document.all.hGpoId");
		obj.value = prid;
		if (prid != '')
		{
			setConstructCodes(prid);
			document.frames.fmCart.frmCart.hGpoId.value = prid;
		}
	}
	//obj = eval("document.all.Lbl_GpoCat");
    //obj.innerHTML = "&nbsp;" + gpocatid;

	obj = document.all.tabAcSum;
	obj.style.display = 'block';

}

function setConstructCodes(id)
{
	if (id == '')
	{
		document.frames.fmCart.frmCart.Cbo_Construct.disabled = true;
		document.frames.fmCart.frmCart.Btn_UpdatePrice.disabled = true;
	}
	else
	{
		document.frames.fmCart.frmCart.Cbo_Construct.disabled = false;
		document.frames.fmCart.frmCart.Btn_UpdatePrice.disabled = false;
	}
}


function fnSetConstructs(obj)
{
	var xml = obj[0].childNodes;
	var len = xml.length;
	var obj = document.frames.fmCart.frmCart.Cbo_Construct;
	for (var x=0; x<len; x++) 
	{
		var elOptNew = document.createElement('option');
		elOptNew.value = parseXmlNode(xml[x].childNodes[0].firstChild);
		elOptNew.id = parseXmlNode(xml[x].childNodes[1].firstChild);
		elOptNew.text = parseXmlNode(xml[x].childNodes[2].firstChild) + ' - '+ parseXmlNode(xml[x].childNodes[3].firstChild) + ' - $' + formatNumber(parseXmlNode(xml[x].childNodes[1].firstChild));
		obj.add(elOptNew, x+1); 
	}
}

function fnReset()
{
	var opt = (document.frmPhOrder.hOpt != undefined)?document.frmPhOrder.hOpt.value:'';
	if(opt == 'QUOTE'){
		location.href = '/GmPageControllerServlet?strPgToLoad=gmIncShipDetails.do?screenType=Order&RE_FORWARD=gmOrderItemServlet&strOpt=QUOTE&strHideControlNum=no';
	}else{
		//location.href = '/GmOrderItemServlet?hAction=Load&hOpt=PHONE';
		location.href = '/GmPageControllerServlet?strPgToLoad=gmIncShipDetails.do?screenType=Order&RE_FORWARD=gmOrderItemServlet&strOpt=PHONE&strHideControlNum=no';
	}
}


function fnAcIdBlur(obj)
{
	changeBgColor(obj,'#ffffff');
	if (obj.value != '')
	{
	
		fnCallAjax('GETACC');
		
		if (document.all.Txt_OrdId)
		{
			document.all.Txt_OrdId.focus();
		}
		
		if(document.frmPhOrder){
			if (document.frmPhOrder.Txt_ParentOrdId)
				{
					document.frmPhOrder.Txt_ParentOrdId.value = "";
				}
			document.frmPhOrder.Txt_PO.value = "";
		}
		
		if(document.frames.fmCart){
			var varRowCnt = document.frames.fmCart.frmCart.hRowCnt.value;
			for (val=0; val < varRowCnt; val ++)
			{
				obj = eval("document.frames.fmCart.frmCart.Txt_Qty"+val);
				obj.value = '';
				obj = eval("document.frames.fmCart.frmCart.Txt_PNum"+val);
				obj.value = '';
				obj = eval("document.frames.fmCart.frmCart.Txt_Price"+val);
				obj.value = '';
				obj = "Lbl_Desc".concat(val);
				obj = window.frames["fmCart"].document.getElementById(obj);
				obj.innerHTML = "";
				obj = "Lbl_Stock".concat(val);
				obj = window.frames["fmCart"].document.getElementById(obj);
				obj.innerHTML = "";
				obj = "Lbl_Amount".concat(val);
				obj = window.frames["fmCart"].document.getElementById(obj);
				obj.innerHTML = "";
			}
			document.frames.fmCart.fnCalculateTotal();
		}
	}
	
}

function fnAcBlur(obj)
{		
	if (document.frmPhOrder)
	{
		var objid = obj.id;
		objid = objid.substr(0,objid.length-2);
		var obj = eval("document.frmPhOrder."+objid);
		var val = obj.value;		
		obj = document.frmPhOrder.Txt_AccId;
	}
	if (document.frmAccount)
	{
		var objid = obj.id;
		var val = obj.value;
		document.frmAccount.hAccId.value = val;
	}
	document.all.Txt_AccId.value = val;
	fnAcIdBlur(obj);
}

function fnCheckDOFormat()
{
	var bl = false;
	var obj = document.frmPhOrder.Txt_OrdId;
	var dtformat = document.frmPhOrder.dateFormat.value;
	var opt = (document.frmPhOrder.hOpt != undefined)?document.frmPhOrder.hOpt.value:'';
	if(opt != 'QUOTE'){ // This DO format is only form New Order, not for Quote
		var val = obj.value;
		var month;
		var strdate;
		var day;
		var count =0;
		if (val.length != 0)
		{
			var strobj =  val.split('-');
			strdate = strobj[1];
			
			if (strobj.length != 3)
			{
				bl = true;
			}
	        if(strobj.length == 3)
	        {
	        	
				if (strobj[1].length != 6 || strobj[2].length == 0)
				{
					bl = true;
				}
				var dtarr = dtformat.split('/');
				if(dtarr.length <3){
					dtarr = dtformat.split('.');
				}
				if(dtarr[0]  == 'MM' || dtarr[0]  == 'mm'){
					 month = strdate.substring(2,4);
				}
				if (dtarr[1]  == 'dd' || dtarr[1]== 'DD'){
					 day = strdate.substring(4,6);
				}
				if (dtarr[0]  == 'dd' || dtarr[0]== 'DD'){
					 day = strdate.substring(4,6);
				}
				if (dtarr[1]  == 'MM' || dtarr[1]  == 'mm'){
					 month = strdate.substring(2,4);
				}
				if (month > 12 || day > 31)
				{
					bl = true;
				}
	        }
			for(i=0 ; i< strobj.length ;i++){
				if(isNaN(strobj[i]) || strobj[i] == 0)
				{
					count++;
				}
			}
			if(count > 0){
				bl = true;	
			}
		}
	}
	return bl;

}


//Trims the leading and trailing spaces
function trim(str)
{
	str = new String(str);
	var intLen = str.length;
	var intBegin = 0;
	var intEnd = intLen - 1;
	while ((str.charCodeAt(intBegin) == 13 || str.charCodeAt(intBegin) == 10 || str.charAt(intBegin) == " ") && intBegin < intLen)
	{
		if (str.charCodeAt(intBegin) == 13 || str.charCodeAt(intBegin) == 10 )
			intBegin+=2;
		else
        		intBegin++;
	}
	while ((str.charCodeAt(intEnd) == 13 || str.charCodeAt(intBegin) == 10 || str.charAt(intEnd) == " ") && intBegin < intEnd)
	{
		if (str.charCodeAt(intEnd) == 13 || str.charCodeAt(intBegin) == 10)
			intEnd-=2;
		else
			intEnd--;
	}
	return str.substring(intBegin, intEnd+1);
}
function fnCreateQuoteString()
{
	var cnt = '';
	if(document.frmPhOrder.hQuoteCnt!= undefined){
		cnt = document.frmPhOrder.hQuoteCnt.value;
	}
	var strDiscountIndex = '';
	if(document.frmPhOrder.hDiscountIndex != undefined){
		strDiscountIndex =document.frmPhOrder.hDiscountIndex.value;
	}
	var str;
	var strquote = '';
	var accDiscnt = 0;
	var strSetIdFl = '';
	var strOpt = document.frmPhOrder.hOpt.value;
	var accDiscnt = parseFloat(document.frmPhOrder.hAccDiscnt.value);
	accDiscnt = trim(accDiscnt!='')?parseFloat(accDiscnt):0;
	// MNTTASK-4744 - Discount Calculation : Addition Acc Dis and Order Dis value when submit. 
	for (i=0;i<cnt;i++)
	{
		var obj = eval("document.frmPhOrder.Txt_Quote"+i);
		id = obj.id;
		val = obj.value;
		if (val != '' || accDiscnt > 0)
		{
			if(i==strDiscountIndex){
				var discountVal = val.replace(/%/g,'');
				discountVal = parseFloat(formatNumber(discountVal)) + parseFloat(formatNumber(accDiscnt));
				str = '^' + id + '^' + formatNumber(discountVal) + '|';
			}else{ 
				// When create order for OUS country that time will give set id.
				if(id == '4000098' && val != ''){ // Set ID 
					strSetIdFl = '^' + '4000099' + '^' + 'Y' + '|'; // Set ID Flag
					var atpos =  val.indexOf(".");
					var atDotArr =  val.split(".");
					var atposComma =  val.indexOf(",");
					var array =  val.split(",");
					var length = array.length;
					var atDotArrlen = atDotArr.length;
					var Arrval = array[1];
					var dotArrVal = atDotArr[2];

					// Do not enter multiple set id in for one order id.
					if(Arrval != '' && Arrval != undefined ){
						Error_Details('Please don\'t enter more than one <b>Set ID</b>.');
					}
					// Do not enter more that one ./,/ 
					if(atpos == -1 || atposComma != -1 || length == 3 || atDotArrlen == 3 || (dotArrVal != '' && dotArrVal != undefined)){
						Error_Details('Please enter valid <b>Set ID</b>.');
					}
				}
				str = '^' + id + '^' + val + '|' + strSetIdFl;
			}
			strquote = strquote + str;
		}
	}
	return strquote;
}
//MNTTASK-7099 THIS IS USED TO CHECK THE AVAIABLITY OF DO-ID IN AJAX.
function fnCheckOrderId(doid,labelid){	
	fnClearDiv();		
	var doidobj = (doid.value).toString().split("-"); 
	var arrlen = doidobj.length;
	var accId = document.all.Txt_AccId.value;
	var opt = (document.frmPhOrder.hOpt != undefined)?document.frmPhOrder.hOpt.value:'';
	if (arrlen == 3 && doidobj[2] != '' && doidobj[1] !='' && doidobj[0] !='' && accId != '' ){
		if(!fnCheckDOFormat()){
			doid=(doid.value).replace(/-/g,'');
			if(!isNaN(doid)){
				fnCallAjax('CHECKDOID');
			}else{
				Error_Details('Incorrect <b>'+labelid+'</b> Format.');
			}
		}else{
			if(opt != undefined && opt != 'QUOTE')
				Error_Details('Incorrect <b>'+labelid+'</b> Format.');		
		}
	}else if( arrlen != 3 && arrlen != 1){
		Error_Details('Incorrect <b>'+labelid+'</b> Format.');		
	}
	if(ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}
}
function showimage() {
    if (req.readyState == 4) {
        if (req.status == 200) {
	        var xmlDoc = req.responseText;	             
	        if(xmlDoc.indexOf("DUPLICATE")!= -1){
	            document.getElementById("DivShowDOIDExists").style.display='';
	        }else if(xmlDoc.indexOf("AVAILABLE")!= -1){  
	            document.getElementById("DivShowDOIDAvail").style.display='';	             
	        }
        }
    }
}
function fnClearDiv(){
	  document.getElementById("DivShowDOIDExists").style.display='none';
      document.getElementById("DivShowDOIDAvail").style.display='none';	
}

function fnPlaceOrderSubmit(){
	var confirmMsg = true;
	
    if (document.frmPhOrder.Txt_OrdId){
 		orderID = document.frmPhOrder.Txt_OrdId.value;
 	}
   
    var strCtlnNumNeedStr = window.frames.fmCart.fnCreateCtrlNumNeededStr();
    if(strCtlnNumNeedStr.length > 0){
    	strCtlnNumNeedStr = strCtlnNumNeedStr.substring(0,strCtlnNumNeedStr.length-1);	
       	Error_Details("Please remove the control numbers entered for the following parts <b>"+strCtlnNumNeedStr+"</b>");
     }
    
    if (ErrorCount > 0){
			Error_Show();
			Error_Clear();
			return false;
	}
    
    var strWithoutCtrlNum = window.frames.fmCart.fnCreateWithoutCtrlNumStr();
    if(strWithoutCtrlNum.length > 1){
    	strWithoutCtrlNum = strWithoutCtrlNum.substring(0,strWithoutCtrlNum.length-1);
    	confirmMsg = confirm("Parts "+strWithoutCtrlNum+" entered without control number(s), do you still want to proceed?."); 
    }
    
    if(!confirmMsg){
    	return false
    }
    if(confirmMsg)
    fnCallAjax('validateControlNumber');
}

// MNTTASK- 8623 When tab out in Customer PO text box after , it will focus in Part Number text box.
function fnPartNumTxtFocus(){
	pobj = eval("document.frames.fmCart.frmCart.Txt_PNum"+0);
	pobj.focus(); // Part number text box first row.
}



function fnCallAjaxCtrlNum(val,pnum,cnum,trcnt)
{
	var allParts = '';
	var ajax = '';
	ctrlId = trcnt;
	var accid = document.all.Txt_AccId.value;
	if(document.all.Txt_OrdId){
		var doid = document.all.Txt_OrdId.value;
	}
	// The PhoneOrder.js file calling in many places. Need to be check undefined.
	var orderType = (document.frmPhOrder) ? document.frmPhOrder.Cbo_OrdType.value:'';
	var strShipTo = (document.all.shipTo) ? document.all.shipTo.value:'';
	var strName = (document.all.names) ? document.all.names.value:'';
	var strAddress = (document.all.addressid) ? document.all.addressid.value:'';
	
	if (document.frmPhOrder)
	{
		var repid = document.all.hRepId.value;
	}
    var url = fnAjaxURLAppendCmpInfo("/GmOrderAjaxServlet?cnum="+cnum+"&pnum="+encodeURIComponent(pnum)+"&RE_AJAX_WRAPPER=AJAXWRAPPER&orderType="+encodeURIComponent(orderType)+"&opt=" + encodeURIComponent(val)+ "&acc=" + encodeURIComponent(accid)+ "&rep="+encodeURIComponent(repid)+"&doid="+trim(doid)+ "&PartNums="+encodeURIComponent(allParts)+"&shipTo=" +strShipTo + "&names=" +  strName + "&addressid=" + strAddress);
	if (typeof XMLHttpRequest != "undefined") 
	{
	   req = new XMLHttpRequest();
	} 
	else if (window.ActiveXObject) 
	{
	   req = new ActiveXObject("Microsoft.XMLHTTP");
	}
	req.open("GET", url, true);
	if(val == 'validateControlNumber'){
		req.onreadystatechange = callCtrlNumInValid;
		}
	req.send(null);
}

function callCtrlNumInValid() {
    if (req.readyState == 4) {
        if (req.status == 200) {
	        var xmlDoc = req.responseText;
	       if(trim(xmlDoc) != '' ){
	    	var iframe = document.getElementById('fmCart');
			var innerDoc = iframe.contentDocument || iframe.contentWindow.document;
			var txtObj = innerDoc.getElementById("DivShowCnumNotExists"+ctrlId);			
			txtObj.style.display='';
	       }
	       else
	       {
	    	    var iframe = document.getElementById('fmCart');
				var innerDoc = iframe.contentDocument || iframe.contentWindow.document;
				var txtObj = innerDoc.getElementById("DivShowCnumAvl"+ctrlId);			
				txtObj.style.display='';
	       }
         }
    }
}

//validate the space - Customer PO
function fnValidatePOSpace(CustPO) {
	var regexp = /\s/g;
	if (regexp.test(CustPO)) {
		Error_Details("Please remove space(s) from the entered Customer PO.");
	}
}

function fnCheckHoldFl(){
	var holdFl = false;
	var holdSts = document.frmPhOrder.hHoldSts.value;
	var varRowCnt = document.frames.fmCart.frmCart.hRowCnt.value;
	var objPartAmt = '';
	var objPriceCell = '';
	var partAmtVal = '';
	var partPriceVal = '';
	for (k=0; k <= varRowCnt; k ++)
	{
		objPartAmt = eval("document.frames.fmCart.frmCart.hPartPrice"+k);
		objPriceCell = eval("document.frames.fmCart.frmCart.Txt_Price"+k);
		
		if(objPartAmt!=null && objPartAmt!=undefined){
			partAmtVal = objPartAmt.value;
			partPriceVal = objPriceCell.value;
			if((partAmtVal !='' && partPriceVal !='' && partAmtVal != partPriceVal) || (partAmtVal == '0.00')){
				holdFl = true;
			}
		}
	}
	if(holdFl && holdSts=='YES'){
		document.frmPhOrder.hHoldFl.value = 'Y';
	}else{
		document.frmPhOrder.hHoldFl.value = '';
	}
}

function fnSetHardCopyPO()
{
	var modeval = document.frmPhOrder.Cbo_Mode.value;
	
	if (modeval == '5017')
	{
		document.frmPhOrder.Chk_HardPO.disabled = true;
	}else{
		document.frmPhOrder.Chk_HardPO.disabled = false;
	}
}

//adding method for US and OUS code migration
function fnEnableDiscount(obj)
{
	var opt = (document.all.hOpt != undefined)?document.all.hOpt.value:'';
	if (opt == 'PHONE'){// On tabout from Discount column, need to replace the value of Price EA with the base price of the part
		window.frames.fmCart.fnResetBasePrice();
	}
	var btn_calc_discount = eval("document.frames.fmCart.frmCart.Btn_Calculate");
	var accDiscnt = 0;
	accDiscnt = (document.frmPhOrder.hAccDiscnt != undefined)?document.frmPhOrder.hAccDiscnt.value:0;
	var strValue = (obj != undefined)?eval(obj.value):0;
	
	if (btn_calc_discount != undefined)
	{	if((strValue=="" || strValue == "0" || strValue == undefined) && accDiscnt == 0){
			btn_calc_discount.disabled = true;
		}else{
			btn_calc_discount.disabled = false;
		}
	}
}

//adding method for US and OUS code migration
function fnNumbersOnly(e,obj){
	var unicode=e.charCode? e.charCode : e.keyCode
	var DiscountVal = obj.value;
	if (unicode!=8){ //if the key isn't the backspace key (which we should allow)
		if ((unicode<48||unicode>57) && (unicode != 46)){ // If not a number or dot
			Error_Details("Please enter only numeric values or numeric values with Decimal");
			if (ErrorCount > 0){
				Error_Show();
				Error_Clear();
				return false;
			}
		}
	}
}

//To clear the images image.
function fnPOClearDiv(){
	if(vcountryCode != 'en'){// Only for OUS
		document.getElementById("DivShowPOIDExists").style.display='none';
	    document.getElementById("DivShowPOIDAvail").style.display='none';
	}
}

//To check whther the customer PO is available or not
function fnCheckPO(val){
	if(vcountryCode != 'en'){// Only for OUS
		var custPoId = document.frmPhOrder.Txt_PO.value;
		if(TRIM(custPoId) == ''){
			CustPOValidFl = false;
			POMessage = '';
			return false;
		}
		POMessage = '';
		CustPOValidFl = false;
	    var url = fnAjaxURLAppendCmpInfo("/GmOrderAjaxServlet?&opt="+ encodeURIComponent(val)+"&Txt_PO="+custPoId);
		if (typeof XMLHttpRequest != "undefined") 
		{
		   req = new XMLHttpRequest();
		} 
		else if (window.ActiveXObject) 
		{
		   req = new ActiveXObject("Microsoft.XMLHTTP");
		}
		req.open("GET", url, true);
		if (val == 'CHECKPO'){             
			req.onreadystatechange = fnShowPOImage;
		}
		req.send(null);
	}
}

//To open the order details, when click on "L" icon
function fnModifyOrder(){
	var strCustPo = document.frmPhOrder.Txt_PO.value;
	windowOpener('/GmOrderVoidServlet?hMode=Reload&strOpt=CUSTPO&Txt_CustPO='+strCustPo,'OrderDetails',"resizable=yes,scrollbars=yes,top=475,left=420,width=1200,height=300");
}

//To show the images
function fnShowPOImage() {
    if (req.readyState == 4) {
        if (req.status == 200) {
	        var xmlDoc = req.responseText;	             
	        if(xmlDoc.indexOf("DUPLICATE")!= -1){
	        	document.getElementById("DivShowPOIDAvail").style.display = 'none';
	        	document.getElementById("DivShowPOIDExists").style.display = 'inline';
	            POMessage = 'DUPLICATE';
	        }else if(xmlDoc.indexOf("AVAILABLE")!= -1){  
	        	document.getElementById("DivShowPOIDExists").style.display = 'none';
	        	document.getElementById("DivShowPOIDAvail").style.display = 'inline';
	            POMessage = 'AVAILABLE';
	        }
        }
        CustPOValidFl = true;
    }
}
