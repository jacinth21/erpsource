/*--------------------------------------------------------------------------------------
		Will set the value in the html page
-----------------------------------------------------------------------------------------*/
function Load_Array(foListArray,obj)
{
	var len = foListArray.length;	<!-- To get the array length -->
	for(var i = 0 ; i <= (len-1); i++)
	{
		var optionValue = new Array();
		optionValue[0] = new Array(TotalColumn);
		string = "";

		for(var j = 0 ; j <= (TotalColumn); j++)
		{
			optionValue[0][j] = foListArray[i][j];
		}
		AddItem(obj,optionValue);
	}
}
/*---------------------------------------------------------------------------------------
	This code is used to Add the record to the listbox
------------------------------------------------------------------------------------------>*/
function AddOption(listbox,foListArray)
{
	var optionValue = new Array();
	var ArrayIndex ;
	var i = "";
	var arSize = foElements.length;
	
	var objValue = "";
	
	optionValue[0] = new Array(TotalColumn);
	
	setValuesToArray(optionValue);

	var pkFlag = TotalColumn;
	var opFlag = pkFlag-1;
	
	optionValue[0][pkFlag] = "0";	
	optionValue[0][opFlag] = "I";

	AddItem(listbox,optionValue);
	
	ArrayIndex = listbox.options.length -1;
	foListArray[ArrayIndex] = new Array(TotalColumn);
	for(var i = 0 ; i <= (TotalColumn); i++)
	{
		foListArray[ArrayIndex][i] = optionValue[0][i];
	}
	
	clearElements();
	return foListArray;
}

/*--------------------------------------------------------------------------------------
	Called when the user selects the value from the listbox
-----------------------------------------------------------------------------------------*/
function Selected(container)
{
	var index = container.selectedIndex;
	var arSize = foElements.length;
	
	for (i=0;i<arSize;i++ )
	{
		obj = foElements[i];

		if ( obj.type == "text" || obj.type == "hidden" || obj.type == "textarea")
		{
			obj.value = foListArray[index][i];
		}
		else if ( obj.type == "select" || obj.type == "select-one")
		{
			obj.selectedIndex = -1;
			var arVal = foListArray[index][i];

			for(var j=0;j<obj.length;j++)
			{
				var Event = obj.options[j].text;
				if(arVal == Event)
				{
					obj.options[j].selected = true;
				}
			}
		}
		else if ( obj.type == "multiple" || obj.type == "select-multiple")
		{
			obj.selectedIndex = -1;
			var arVal = foListArray[index][i];
			var a = arVal.split(",");
			for(var i =0;i<a.length;i++)
			{
				var b = a[i];
				for(var j=0;j<obj.length;j++)
				{
					var Event = obj.options[j].text;
					if(b == Event)
					{
						obj.options[j].selected = true;
					}
				}			
			}
		
		}
		else if ( obj.type == "checkbox")
		{
			if (foListArray[index][i] == "Y")
			{
				obj.checked = true;
			}
			else
			{
				obj.checked = false;
			}
		}
	}

}

/*----------------------------------------------------------------------------------
	Called when the user deletes the value from the listbox
------------------------------------------------------------------------------------*/
function DeleteOption(listbox,foListArray,foListDeletedArray)
{
	var optionValue = new Array();

	var index = listbox.selectedIndex;

	var deleteindex = foListDeletedArray.length;
	var pkFlag = TotalColumn;
	var opFlag = pkFlag-1;
	
	optionValue[0] = new Array(TotalColumn);

	if ( foListArray[index][pkFlag] != 0)
	{
		foListArray[index][opFlag] = "D";
		foListDeletedArray[deleteindex] = foListArray[index];
	}

	for (var i=index;i<foListArray.length;i++)
	{
		foListArray[i] = foListArray[i+1];
	}

	foListArray = foListArray.slice(0,foListArray.length-1);

	RemoveItem(listbox);

	clearElements();
	
	foArray = new Array(foListArray,foListDeletedArray);
	return foArray;
}


/*----------------------------------------------------------------------------------
	Used to Edit the Listbox
------------------------------------------------------------------------------------*/
function UpdateOption(listbox,foListArray)
{
	var index = listbox.selectedIndex;
	var len = listbox.options.length;
	var ArrayIndex;
	var objValue = "";

	var optionValue = new Array();	
	optionValue[0] = new Array(TotalColumn);
	
	setValuesToArray(optionValue);
	
	var pkFlag = TotalColumn;
	var opFlag = pkFlag-1;

	if ( foListArray[index][pkFlag] != '0')
	{
		optionValue[0][opFlag] = 'U';
	}
	else
	{
		optionValue[0][opFlag] = foListArray[index][opFlag];
	}

	optionValue[0][pkFlag] = foListArray[index][pkFlag];

	ArrayIndex = UpdateItem(listbox , optionValue);

	if (ArrayIndex != -1)
	{
		for(var i = 0 ; i <= (TotalColumn); i++)
		{
			foListArray[ArrayIndex][i] = optionValue[0][i];
		}
	}

	clearElements();
	return foListArray;
}

function clearElements()
{
	var arSize = foElements.length;
	for (i=0;i<arSize;i++ )
	{
		obj = foElements[i];
		if ( obj.type == "text" || obj.type == "select" || obj.type == "hidden" || obj.type == "textarea")
		{
			obj.value = "";
		}
		else if ( obj.type == "checkbox")
		{
			obj.checked = false;
		}
	}

}

function setValuesToArray(optionValue)
{
	var arSize = foElements.length;
	
	for (i=0;i<arSize;i++ )
	{
		var objValue = "";
		obj = foElements[i];
		if ( obj.type == "text" || obj.type == "hidden" || obj.type == "textarea")
		{
			objValue = obj.value;
		}
		else if ( obj.type == "select" || obj.type == "multiple" || obj.type == "select-one" || obj.type == "select-multiple")
		{
			for(var k=0;k<obj.length;k++)
			{
				if ( obj.options[k].selected == true)
				{
					objValue = objValue + obj.options[k].text + ',';
				}
			}
			objValue = objValue.substring(0,objValue.length-1);
		}		
		else if ( obj.type == "checkbox")
		{
			if (obj.checked == true)
			{
				objValue = "Y";
			}
			else
			{
				objValue = "N";
			}
		}

		optionValue[0][i] = objValue;
	}
}


function fnParseColumns(arrayOfArrays)
{
   var newArray = new Array();
   
   var n = arrayOfArrays.length;
   
   for(i=0;i<n;i++)
   {
   	var rowArray = arrayOfArrays[i];
   	var m = rowArray.length;
   	var stringValue = "";
   	for(j=0;j<m;j++)
   	{
   	   stringValue = stringValue + rowArray[j];
   	   if(j < (m-1) ) stringValue += "^";
   	}
   	
   	newArray.push(stringValue);
   }
   
   return newArray;
}
 
function fnParseRows(arrayOfRowStrings)
{
  var str = "";
  var n = arrayOfRowStrings.length;
  
  for(i=0;i<n;i++)
  {
  	str = str + arrayOfRowStrings[i];
  	if(i < (n-1) ) str += "|";
  }
  return str;
}

function fnCreateArray(obj)
{
	var zero = 0;
	var one = 1;
	var arLev = new Array();
	
	for ( i = 1, j =0 ;i<obj.length;i++,j++)
	{
		arLev[j] = new Array(2);
		arLev[j][zero] = obj.options[i].text;
		arLev[j][one] = obj.options[i].value;
	}
	return arLev;
}

function fnReplaceArray(arLev,foListArray,num)
{
	for ( j =0; j<arLev.length;j++)
	{
		for ( i =0;i<foListArray.length;i++)
		{
			if ( foListArray[i][num] == arLev[j][0])
			{
				foListArray[i][num] = arLev[j][1];
			}
		}
	}
	return foListArray;
}