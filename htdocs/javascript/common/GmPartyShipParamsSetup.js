var validForm ="";
function fnPageLoad1(){
	
	validForm = document.getElementsByName('frmPartyShipParamsSetup')[1];
	
	if(validForm == undefined){
		validForm = document.getElementsByName('frmPartyShipParamsSetup')[0];

	}
	var form = validForm;
	
	//var form = document.getElementsByName('frmRuleParamsSetup')[0];// To get the form valuealert(form);
		if(parent.window.document.getElementById("ajaxdivcontentarea") != undefined){
				parent.window.document.getElementById("ajaxdivcontentarea").style.height = "400";
		}
	var shipCarrierObj = document.getElementById("shipCarrier");
	
	fnGetShipModes(shipCarrierObj,form);
	var stdpayee = form.stdShipPayee.value;
	var stdChrgType = form.stdShipChargeType.value;		
	var rushPayee = form.rushShipPayee.value;
	var rushChrgType = form.rushShipChargeType.value;
	
	// Validation for Standard Type Ship Parameters
	if(stdpayee == '105040'){ // Globus
		form.stdShipChargeType.disabled = true;
		form.stdShipValue.disabled = true;
	}else if((stdpayee == '105041') && stdChrgType == '105051'){ // Customer && Actual
		form.stdShipValue.disabled = true;
	}else{
		form.stdShipValue.disabled = false;
		form.stdShipChargeType.disabled = false;
	}	
	// Validation for Rush Type Ship Parameters
	if(rushPayee == '105040'){ // Globus
		form.rushShipChargeType.disabled = true;
		form.rushShipValue.disabled = true;
	}else if((rushPayee == '105041') && rushChrgType == '105051'){ // Customer && Actual
		form.rushShipValue.disabled = true;
	}else{
		form.rushShipValue.disabled = false;
		form.rushShipChargeType.disabled = false;
	}
}

function fnDisableChrgVal(form){	
	var stdpayee = form.stdShipPayee.value;
	var stdChrgType = form.stdShipChargeType.value;	
	var rushPayee = form.rushShipPayee.value;
	var rushChrgType = form.rushShipChargeType.value;
	
	// Validation for Standard Type Ship Parameters
	if(stdpayee == '105040'){ // Globus
		form.stdShipChargeType.disabled = true;
		form.stdShipChargeType.value= 0;
		form.stdShipValue.disabled = true;
		form.stdShipValue.value = '';
	}else if((stdpayee == '105041') && stdChrgType == '105051'){ // Customer && Actual
		form.stdShipValue.disabled = true;
		form.stdShipValue.value = ''; 
	}else{
		form.stdShipValue.disabled = false;
		form.stdShipChargeType.disabled = false;
	}
	// Validation for Rush Type Ship Parameters
	if(rushPayee == '105040'){ // Globus
		form.rushShipChargeType.disabled = true;
		form.rushShipValue.disabled = true;
		form.rushShipChargeType.value= 0;
		form.rushShipValue.value = '';
	}else if((rushPayee == '105041') && rushChrgType == '105051'){ // Customer && Actual
		form.rushShipValue.disabled = true;
		form.rushShipValue.value = '';
	}else{
		form.rushShipValue.disabled = false;
		form.rushShipChargeType.disabled = false;
	}
}

function fnValidateShipValue()
{
	validForm = document.getElementsByName('frmPartyShipParamsSetup')[1];
	
	if(validForm == undefined){
		validForm = document.getElementsByName('frmPartyShipParamsSetup')[0];

	}
	var form = validForm;
	// Validation for Shipment Vale entered.
	var strstdShipValue = document.all.stdShipValue.value;
	var strrushShipValue = document.all.rushShipValue.value;
	var strstdShipChargeType = document.all.stdShipChargeType.value;
	var strrushShipChargeType = document.all.rushShipChargeType.value;
	
	if (form.rushShipValue.disabled == false || form.stdShipValue.value == false)
	{
		if(strstdShipChargeType != 0)
		{
			fnValidateTxtFld('stdShipValue',message[5118]);	
		}
		if(strrushShipChargeType != 0)
		{
			fnValidateTxtFld('rushShipValue',message[5119]);	
		}
	}

	if (isNaN(strstdShipValue) || isNaN(strrushShipValue))
	{
		Error_Details(message[5108]);
    } 
	
	if ( (strstdShipChargeType == 105053 &&  parseInt(strstdShipValue) < -100)|| ( strrushShipChargeType == 105053 &&  parseInt(strrushShipValue) < -100))
	{
		Error_Details(message[5109]);
    } 
	 

	if ( (strstdShipChargeType == 105050 &&  parseInt(strstdShipValue) < 0)|| ( strrushShipChargeType == 105050 &&  parseInt(strrushShipValue) < 0))
	{
		Error_Details(message[5110]);
    } 
	
}


function fnShipSubmit(form,pagetype){	
	//var txtAccount = document.getElementById("txtAccount"); 
	 
	var strshipCarrier = document.all.shipCarrier.value;
	var strshipMode = document.all.shipMode.value;
	var strShipmentEmail = '' ;
	
	if(strshipCarrier == 0 && strshipMode != 0)
	{
		Error_Details(message[5110]);
	}
	
	fnValidateTxtFld('txt_LogReason',message[5120]);
	fnValidateShipValue();
	
	// Validation for if we have FedEx Third Party Account id for the Selected Account or Party,
	// system should not allowed Standard Shipment and Rush Shipment values other than [Choose One].
	
	var strthirdParty = document.all.thirdParty.value;
	var strstdShipPayee = document.all.stdShipPayee.value;
	var strrushShipPayee = document.all.rushShipPayee.value;
	var strstdShipChargeType = document.all.stdShipChargeType.value;
	var strrushShipChargeType = document.all.rushShipChargeType.value;
	var strstdShipValue = document.all.stdShipValue.value;
	var strrushShipValue = document.all.rushShipValue.value;	
	
	if(strthirdParty != ''){		
		if(strstdShipPayee != '0' || strrushShipPayee != '0'){
			Error_Details(message[5112]);
		}
	}
	// Validation for if we have Standard Shipment and Rush Shipment values are [Choose One],
	// System should not allow to save values of Charge Type and Value(% or $) values.
	if((strstdShipPayee == '0' && (strstdShipChargeType != '0' || strstdShipValue != '')) || (strrushShipPayee == '0' && (strrushShipChargeType != '0' || strrushShipValue != ''))){
		Error_Details(message[5113]);
	}
	
	var accid =  document.getElementById("accountId");
	var partyId =  document.getElementById("partyID");
	
	if((accid.value == '0' || accid.value == '') && (partyId.value == '0' || partyId.value == '')){
		Error_Details(message[5114]);
	}
	
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}  
	else{ 
			if(pagetype=='group')
				{
					var accid1 =  document.getElementById("accountId");
					var partyId1 =  document.getElementById("partyID");
					var comments = document.all.txt_LogReason.value;	
					var strthirdParty = document.all.thirdParty.value;
					var strshipCarrier = document.all.shipCarrier.value; 
					var strshipMode = document.all.shipMode.value; 
					var strstdShipPayee = document.all.stdShipPayee.value;
					var strrushShipPayee = document.all.rushShipPayee.value;
					var strstdShipChargeType = document.all.stdShipChargeType.value;
					var strrushShipChargeType = document.all.rushShipChargeType.value;
					var strstdShipValue = document.all.stdShipValue.value;
					var strrushShipValue = document.all.rushShipValue.value;
					var ShipEmailArray = '';
					var err_email = '';
                    var atPosition = '';
					if(document.all.shipmentEmail != undefined ){                 // PMT-40167 - field Shipment email as non-mandatory - shipment email field is used only for BBA company
						strShipmentEmail = document.all.shipmentEmail.value;
						if(strShipmentEmail != '' && strShipmentEmail != null){   // PMT-40167 - field Shipment email as non-mandatory - shipment email field is used only for BBA company
						ShipEmailArray = strShipmentEmail.split(",");
						
						for(i=0; i< ShipEmailArray.length;i++){								
							atPosition = ShipEmailArray[i].indexOf("@");
							if (atPosition < 1) {
			                       err_email = err_email +"<br>"+ShipEmailArray[i];
			                   }
						 }	
					 }
					  if (err_email != ''){
						Error_Details(Error_Details_Trans(message[5115],err_email));						
				      }
						 
					}
					
					if(strShipmentEmail.length > 200){
						Error_Details(message[5116]);
					}
					if (ErrorCount > 0){
						Error_Show();
						Error_Clear();
						return false;
					}				
					
					var requestStr = '/gmPartyShipParamsSetup.do?method=setPartyShipParam&strOpt=save&screenType=group&accountId='+accid1.value+'&partyID='
							+ partyId1.value  
							+ '&txt_LogReason='
							+ comments
							+ '&thirdParty='
							+ strthirdParty 
							+ '&shipCarrier='
							+ strshipCarrier
							+ '&shipMode='
							+ strshipMode
							+ '&stdShipPayee='
							+ strstdShipPayee
							+ '&rushShipPayee='
							+ strrushShipPayee 	
							+ '&stdShipChargeType='
							+ strstdShipChargeType	
							+ '&rushShipChargeType='
							+ strrushShipChargeType	
							+ '&stdShipValue='
							+ strstdShipValue
							+ '&rushShipValue='
							+ strrushShipValue 
							+ '&shipmentEmail='
							+ strShipmentEmail ;
					//	loadajaxpage(requestStr, 'ajaxdivcontentarea');
					form.strOpt.value = 'save';
					form.screenType.value = "group"; 
					form.partyID.value = partyId1.value;
					fnStartProgress();
					form.Btn_Submit.disabled = true;
					
					form.submit();  
						
					
				}
			else
			{	
				var strGPOfl = document.all.gpofl.value;
				if(strGPOfl=='Y')
					{
						if(!window.confirm(message[5117]))
				         {
				             return false;
				         }
					
					}
				
				form.strOpt.value = 'save';
				form.accountId.value = accid;
				form.partyID.value = partyId;
				form.Btn_Submit.disabled = true;
				form.submit();  
			 }
		}
}

function fnReset(form){
	form.action = "/gmPartyShipParamsSetup.do?method=setPartyShipParam&strOpt=fetchParams&screenType=button&accountId="+accid;
	form.submit();
}

//Function to display the Ship Mode values based on the Ship Carriers.
function fnGetShipModes(obj,form){
	var carrier = obj.value;
	if(carrier != '')
		dhtmlxAjax.get(fnAjaxURLAppendCmpInfo('/GmCartAjaxServlet?hAction=fetchCarrierModes&hCarrier='+carrier+'&ramdomId='+Math.random()),fnSetShipMode);	
}

function fnSetShipMode(loader){
	
	validForm = document.getElementsByName('frmPartyShipParamsSetup')[1];
	
	if(validForm == undefined){
		validForm = document.getElementsByName('frmPartyShipParamsSetup')[0];

	}
	var form = validForm;

	var responseAll   = loader.xmlDoc.responseXML;	
	var response = responseAll.getElementsByTagName("data")[0];			
	var defaultMode = parseXmlNode(responseAll.getElementsByTagName("mode")[0].firstChild);
	var shiModeObj = document.getElementById("shipMode");	
	var codelist   = response.getElementsByTagName("pnum");	
	if(codelist.length > 0){		
		shiModeObj.options.length = 0;
		shiModeObj.options[0] = new Option("[Choose One]","0");
		for (var x=0; x<codelist.length; x++){
		   	strCodeID = parseXmlNode(codelist[x].childNodes[0].firstChild);
		   	strCodeNm = parseXmlNode(codelist[x].childNodes[1].firstChild);		   
		   	shiModeObj.options[x+1] = new Option(strCodeNm,strCodeID);		   	
		}
		if(defaultMode == ''){
			shiModeObj.value = '0';	
		}else{
			shiModeObj.value = defaultMode;	
		}
	}else{
		for (var i=0;i<shipModeLength;i++){
			arr = eval('AllModeArr'+i);
			arrobj = arr.split(",");
			id = arrobj[0];
			name = arrobj[1];
			shiModeObj.options[i+1] = new Option(name,id);
		}
		shiModeObj.value = '0';
	}
	var carrier = form.shipCarrier.value;	
	// Setting back the saved Ship Mode value for that particuler transaction on change of Saved Ship carrier	
	var previousMode = shiModeObj.value;	
	if(carrier == shipCarrier && shipmode != ''){
		shiModeObj.value = shipmode;
		var count = 0;
		for(var x=0; x<codelist.length; x++){
		   	strCodeID = parseXmlNode(codelist[x].childNodes[0].firstChild);		   
		   	if(strCodeID != shipmode) count++;		   	
		}		
		// Setting Choose One if the transaction having SHip Mode which is not available in corresponding Ship carrier List.
		if(count == codelist.length && shiModeObj.value == '')
			shiModeObj.value = '0';
	}
}

function fnLoadAccount(obj,form){
	var txtAccount = document.getElementById("txtAccount");	
	txtAccount.value = obj.value;
	var accountID = obj.value;
	form.action = "/gmPartyShipParamsSetup.do?method=setPartyShipParam&strOpt=fetchParams&screenType=button&accountId="+accountID;
	form.submit();
}

//To assign the tab id to hidden field, to know which tag is currently open
function fnUpdateTabIndex(link) {
	var linkObj = link.getElementsByTagName('a')[0];
	document.all.hcurrentTab.value = linkObj.id;	
}
