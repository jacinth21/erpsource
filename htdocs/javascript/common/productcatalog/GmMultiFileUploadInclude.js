var fileCount = 1;


function fnAddElementMoreFiles(id) 
{
	var TBODY = document.getElementById(id).getElementsByTagName("TBODY")[0];
    
	for(var j=1; j<5;j++ ){
		
		var row = document.createElement("TR")

	    var td0 = document.createElement("TD")
	    td0.align = "right";
		td0.className = "RightTableCaption";
	    td0.innerHTML = '<font color="red">*</font>&nbsp;'+lblArtifactType+':&nbsp;';
		
		var td1 = document.createElement("TD")
	    td1.innerHTML = '<select name="fileTypeId'+fileCount+'" id="fileTypeId'+fileCount+'" class=RightText  onChange="fnArtifactChange('+fileCount+');">'+optionString+'</select>';
	   
		var td2 = document.createElement("TD")
	    td2.align = "right";
		td2.className = "RightTableCaption";
	    td2.innerHTML = '<font color="red">*</font>&nbsp;'+lblSubType+':&nbsp;';
		
	    var td3 = document.createElement("TD")
	    td3.innerHTML = '<select name="subType'+fileCount+'" id="subType'+fileCount+'" style="width:140px" onChange="fnSubTypeChange('+fileCount+');" ><option value="0">[Choose On]</option></select>';		
		
		var td4 = document.createElement("TD")
	    td4.innerHTML = '<font color="red">*</font>&nbsp;'+lblTitle+':&nbsp;';
		td4.align = "right";
		td4.className = "RightTableCaption";
		
		var td5 = document.createElement("TD")
		var ctrlString ='<input type=text style="width:200px" name="fileTitle'+fileCount+'" id="fileTitle'+fileCount+'" class=InputArea  onFocus=changeBgColor(this,\'#AACCE8\'); value=\'\'>'
		                 + '<input type="hidden" name="titleType'+fileCount+'" id="titleType'+fileCount+'" value=""/>';
	    td5.innerHTML ='<span id="fileTitleSpan'+fileCount+'">'+ctrlString+'</span>';
	    
		
	    var td6 = document.createElement("TD")
	    td6.innerHTML = '&nbsp;'+lblPartNum+':&nbsp;&nbsp;';
	    td6.align = "right";
		td6.className = "RightTableCaption";
		td6.id='partspan'+fileCount;
		  
		var td7 = document.createElement("TD")
		var ctrlString = '<input type="hidden" name="partNum'+fileCount+'" id="partNum'+fileCount+'">';
	    td7.innerHTML = '<span style="width:160px;" id=parttxtval'+fileCount+'></span>'+ctrlString;
	    td7.id='partNumId'+fileCount;
	    
		var td8 = document.createElement("TD")
	    td8.innerHTML = '<input type="file" name="theFile['+fileCount+']" id="theFile['+fileCount+']" value="">';

		row.height = "30";
		row.appendChild(td0);
		row.appendChild(td1);
		row.appendChild(td2);
		row.appendChild(td3);
		row.appendChild(td4);
		row.appendChild(td5);
		row.appendChild(td6);
		row.appendChild(td7);
		row.appendChild(td8);

		TBODY.appendChild(row);
		fileCount++;
	}

	 document.getElementById('attachMoreFile').innerHTML='';
} 

function fnFileUpload(form)
{
	var strFileName = "";
	var strFileType = "";
	var strFileTitle = "";
	var strSubType = "";
	var strTitleType = "";	
	var fileTypeInputstr = "";
	var fileTitleInputstr = "";
	var fileErrorInputstr = "";
	var strFille = "";
	var strFileId = "";
	var strPartNumber = "";
	
	var fileArr = new Array();
	
	fnValidateFilename();
	
	var stropt =  document.all.strOpt.value;
	if (objGridData.indexOf("cell") !='-1' && stropt != 'EditUploadFiles') {
		var fileList = fnGetAllFiles();
		fnValidateGridData(fileList);
	}
	
	for (i=0; i < fileCount; i ++)
	{
		objFile = document.getElementById('theFile['+i+']');
		objType = document.getElementById('fileTypeId'+i);
		objTitle = document.getElementById('fileTitle'+i);
		objSubType = document.getElementById('subType'+i);
		objTitleType = document.getElementById('titleType'+i);	
		objPartNum = document.getElementById('partNum'+i);		
		strFileName = objFile.value;
		strFileType = objType.value;
		strFileTitle = objTitle.value;
		strSubType = objSubType.value;
		strTitleType = objTitleType.value;
		
		if(objPartNum != undefined){
			strPartNumber = objPartNum.value.trim();
		}
		
		strFileId = (i==0)?document.all.hfileId.value:"";
		
		strFille = strFileName.substring(strFileName.lastIndexOf("\\")+1 );
			if(strFileType != '0' || strFileName != '' || ( strTitleType == "" && strFileTitle !='') && (strTitleType == '103216' && strFileTitle !='0') || strSubType !='0'){
				fnValidateDropDn('fileTypeId'+i,message[5177]);
				fnValidateDropDn('subType'+i,message[5183]);
	
				if(strTitleType == '103216'){
					fnValidateDropDn('fileTitle'+i,message[5178]);				
				}else{
					fnValidateTxtFld('fileTitle'+i, message[5178]);				
				}
				
				
				if(strFileName == '' || strFileName == undefined)
				{
					Error_Details(message[5179]);
				}
			}
			if (ErrorCount > 0)
			{
				Error_Show();
				Error_Clear();
				return false;
			}
	  	
  	
		if(strFileType != '0' && strFileName != '' && strFileTitle !='' && strFileTitle !='0' && strSubType !='0')
		{
			var strobj;
			var count = 0;
			var strFileExt = strFileName.substring(strFileName.lastIndexOf("."));
	  		//validate file extension with accept format (rule format)
			strobj =  eval("fileFmt"+strSubType);
			if(strobj.indexOf(",")!=-1){//This will execute if more than one accept formats.
				var arrayFmt =  strobj.split(',');
				for(j=0; j < arrayFmt.length; j ++)
				{
					var fileExtSplit = arrayFmt[j];
			
					if(fileExtSplit==strFileExt){
						count++;
					}
				}
			}else{
				if(strobj==strFileExt){
					count++;
				}
			}

			if(count<=0){
				strFileTypeNm = objSubType.options[objSubType.selectedIndex].text;
				var arrayMsg = [strFileTypeNm,strobj]
				Error_Details(Error_Details_Trans(message[5180],arrayMsg));	 
				if (ErrorCount > 0)
				{
					Error_Show();
					Error_Clear();
					return false;	
				}
			}
			fileTypeInputstr = fileTypeInputstr + strFileType + '^'+strFileTitle + '^'+ strFille + '^'+strSubType + '^'+ strTitleType+'^'+strFileId+'^'+ strPartNumber+'|';
		}
	}
	
	var hide_filename = document.all.hfilename.value;
	
	if (hide_filename != ''){
		var editFileFullPath = document.getElementById('theFile[0]').value;
		var editFileName= editFileFullPath.substring(editFileFullPath.lastIndexOf("\\")+1 );
		if(hide_filename != editFileName){
			Error_Details(Error_Details_Trans(message[5181],hide_filename));
		}
		if (ErrorCount > 0)
		{
			Error_Show();
			Error_Clear();
			return false;	
		}
	}
	
/*	else{*/    // Hide the code PMT-37770
	form.action = "/gmPDFileUpload.do?";
	form.strOpt.value = 'UploadFiles';
	form.fileTypeList.value = fileTypeInputstr;
	fnStartProgress('Y');
	form.submit();
/*	}*/
}

function fnValidateFilename()
{
	filecount = 0;
	var strNewFille = '';
	for (i=0; i < fileCount; i++)
	{
		objType = document.getElementById('fileTypeId'+i);
		strFileType = objType.value;
		objOldFile = document.getElementById('theFile['+i+']');
		strOldFileName = objOldFile.value;
		if(strOldFileName != ''){
			strOldFille = strOldFileName.substring(strOldFileName.lastIndexOf("\\")+1 );
		
			for(j=0; j < fileCount; j++){
				objNewFile = document.getElementById('theFile['+j+']');
				strNewFileName = objNewFile.value;
				strNewFille = strNewFileName.substring(strNewFileName.lastIndexOf("\\")+1 );

				if(strNewFille != ''){					
					if(strNewFille == strOldFille && i != j){
						filecount++;
					}
				}
			}
		}
	}
	if (filecount>1){
		Error_Details(message[5182]);
	}
	
}

function fnGetAllFiles()
{
	gridfilecount = 0;
	var filleArr = new Array;
	for (i=0; i < fileCount; i++)
	{
		objType = eval(document.getElementById('fileTypeId'+i));
		strFileType = objType.value;
		objOldFile = document.getElementById('theFile['+i+']');
		strFileName = objOldFile.value;
		if(strFileName != ''){
			strFilleNm = strFileName.substring(strFileName.lastIndexOf("\\")+1 );
			filleArr.push(strFilleNm);
		}
	}
	return filleArr;
}

//This function will execute on subtype change
function fnSubTypeChange(index,selectedVal) {
	
	var typeObj = document.getElementById("fileTypeId"+index);
	var subTypeObj = document.getElementById("subType"+index);
	var uploadTypeVal = document.frmPDFileUpload.uploadTypeId.value;

	if (typeObj.value == "103114" && uploadTypeVal == "103092") { // 103114-DCOed Documents,103092 -System
		//Ajax will call only for Dcoed doc in system files.
		var subTypeVal = subTypeObj.value;
		var ctrlString = '<select style="width:200px" onchange="javascript:fnGetPartNumVal(this,'+index+')" name="fileTitle'+index+'" id="fileTitle'+index+'"></select>'
				+ '<input type="hidden" name="titleType'+index+'"  id="titleType'+index+'" value="103216"/>';
		document.getElementById("fileTitleSpan"+index).innerHTML = ctrlString;
		dhtmlxAjax.get("/GmPartNumAjaxServlet?strOpt=PARTSBYATTR&attrType=4000444&attrValue="+ subTypeVal + "&ramdomId=" + Math.random(),
				function(loader){
					var responseText = loader.xmlDoc.responseXML;
					if (responseText != null) {
						parseMessage(responseText,index,selectedVal);
					}
				});
	} else {
		var hiddenTitle = "";
		var strReadOnly = "";
		
		if(uploadTypeVal == "103092" ){//getting System Desc from dropdown
			var sysListObj =document.frmPDFileUpload.sysListId;
			hiddenTitle = sysListObj.options[sysListObj.selectedIndex].text;
		}else if(uploadTypeVal == "103094"){//getting Group Desc from dropdown
			var grpListObj =document.frmPDFileUpload.grpListId;
			hiddenTitle = grpListObj.options[grpListObj.selectedIndex].text;
		}else{
			if(uploadTypeVal == "103090" && typeObj.value == "103114"){// Part Number and DCOed doc - readonly title
				strReadOnly ='readonly="readonly"';
			}
			hiddenTitle = document.frmPDFileUpload.hTitle.value;
		}
	
		var ctrlString = '<input type="text" name="fileTitle'+index+'" id="fileTitle'+index+'" '+strReadOnly+' value="'+hiddenTitle+'" style="width:200px" onfocus="changeBgColor(this,\'#AACCE8\');" class="InputArea" onblur="changeBgColor(this,\'#ffffff\');"/>'
				+ '<input type="hidden" name="titleType'+index+'" id="titleType'+index+'"  value=""/>';
		document.getElementById("fileTitleSpan"+index).innerHTML = ctrlString;

	}
}

//This function will populate title dropdown from ajax response
function parseMessage(xmlDoc,index,selectedVal) {
	
	var titleObj = document.getElementById("fileTitle"+index);
	titleObj.options.length = 0;
	titleObj.options[0] = new Option("[Choose One]", "0");

	var pnum = xmlDoc.getElementsByTagName("pnum");
	var pnumlen = pnum.length;
	for ( var i = 0; i < pnumlen; i++) {
		var id = pnum[i].childNodes[0].childNodes[0].nodeValue;
		var name = pnum[i].childNodes[1].childNodes[0].nodeValue;
		titleObj.options[i + 1] = new Option(name, id);

	}
	
	if(selectedVal == null){
		titleObj.value = "0";
	}else{
		titleObj.value = selectedVal;
	}
	

}
//This function will execute on Artifact change
function fnArtifactChange(index){
	var typeObj = document.getElementById("fileTypeId"+index);
	var subTypeObj = document.getElementById("subType"+index);	
	subTypeObj.options.length = 0;
	subTypeObj.options[0] = new Option("[Choose One]","0");
	var k = 0;
	for (var i=0;i<subTypeArrLen;i++)
	{
		arr = eval("subTypeArr"+i);
		arrobj = arr.split(",");
		id = arrobj[0];
		name = arrobj[1];
		altname = arrobj[2];
		if(typeObj.value == altname)
		{
			subTypeObj.options[k+1] = new Option(name,id);
			k++;
		}
	}
	 var strFileType = typeObj.value;
	if(strFileType == '103114' || strFileType == '0'){
		document.getElementById('partspan'+index).style.visibility = 'visible';
	}else{
		document.getElementById("parttxtval"+index).innerHTML="";
		document.getElementById('partspan'+index).style.visibility = 'hidden';
	}

}

// This function used to fetch title based part number show in screen
function fnGetPartNumVal(formValue,index){
	var partval=document.getElementById("fileTitle"+index).value;
	
	var typeObj = document.getElementById("fileTypeId"+index);
	var typeVal=typeObj.value;
	
	if(partval!='0' && typeVal == '103114'){
		document.getElementById("parttxtval"+index).innerHTML=partval;
		document.getElementById("partNum"+index).value=partval;
	}else{
		document.getElementById("parttxtval"+index).innerHTML='';
		document.getElementById("partNum"+index).value='';
	}
}
