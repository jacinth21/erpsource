
// fnCommentLogAjax() - This function is used to call Ajax to get Account Credit Hold comment log JSON
function fnCommentLogAjax(accountId) {
	// to append the company information
	var ajaxURL = fnAjaxURLAppendCmpInfo("/gmCreditHold.do?method=fetchCommentLog&accountId=" + accountId+"&ramdomId=" + Math.random());
	dhtmlxAjax.get(ajaxURL, function(loader) {
		if (loader.xmlDoc.responseText != null) {
			var response = $.parseJSON(loader.xmlDoc.responseText);
		fnCommentTable(response);
		}
	});
}

// fnCommentTable() - This function is used to generate comment log table row's html string from JSON
function fnCommentTable(commentJSONStr){
	var resJSONObj = commentJSONStr;//$.parseJSON(commentJSONStr);
	var objDivCmt =document.getElementById("divCommentTable");
	var strTRHtml= "";
	var commentLen = resJSONObj.length;
	document.getElementById("divShowHide").innerHTML ="";
	for(i= 0; i<commentLen;i++){
		var itemJSON = resJSONObj[i];
		strTRHtml+='<tr><td class="RightText" height="20" width="150">'+itemJSON["DT"]
				      +'</td><td class="RightText" width="150">'+itemJSON["UNAME"]
				      +'</td><td width="500">'+itemJSON["COMMENTS"]+'</td></tr>';
	}
	var strTableHtml = '<table id="tableComment">'+strTRHtml+'</table>';
	objDivCmt.innerHTML = strTableHtml;
	if(commentLen>2){
		fnShowLess();
	}
}

// fnShowLess() - This function is used to hide comment log rows if rows are more than 3. 
function fnShowLess(){
	//alert("less");
	var strMoreHtml = '&nbsp;<a href="javascript:fnShowMore()"><img alt="Click to view all comments" border="0" src="images/plus.gif"/>&nbsp;Show More</a>';
	$('#tableComment').find('tr:gt(2)').hide();
	document.getElementById("divShowHide").innerHTML = strMoreHtml;
}

// fnShowMore() - This function is used to show comment all comment log rows.
function fnShowMore(){
	//alert("more");
	var strLessHtml = '&nbsp;<a href="javascript:fnShowLess()"><img alt="Click to hide comments" border="0" src="images/minus.gif"/>&nbsp;Show Less</a>';
	$('#tableComment').find('tr:gt(2)').toggle();
	document.getElementById("divShowHide").innerHTML = strLessHtml;
}
