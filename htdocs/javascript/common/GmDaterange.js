/**
* @author: Parthiban 
  Description: Common js file to convert date format to company date format
*/

	function fnDaterange (frmdt,todt,Id) {
		$("#fromDt").attr("type","hidden");
		$("#toDt").attr("type","hidden");
		$("#fromDt + img").css("display", "none");
		$("#toDt + img").css("display", "none");
   		var datepicker_visible = false;
		var count=0;
		$(Id).daterangepicker({			
		    startDate: frmdt,
		    endDate: todt,
		    locale: {
	              format: gCmpDateFmt
			}}, function(start, end, label) {
				frmdt = start.format(gCmpDateFmt);
				todt = end.format(gCmpDateFmt);
				$("#fromDt").val(frmdt);
				$("#toDt").val(todt);
			});
						
		$(Id).on('show.daterangepicker', function(ev, picker) {
			datepicker_visible = true;
		});
		$(Id).on('hide.daterangepicker', function(ev, picker) {
			datepicker_visible = false;
		});
		$(Id).on('click', function() {
			if(count>0){
				if(datepicker_visible) {
					$(".daterangepicker").css("display", "none"); 
					$(Id).trigger('hide.daterangepicker', this);
				}
				else{
					$(".daterangepicker").css("display", "block"); 
					$(Id).trigger('show.daterangepicker', this);
				}		
			}
			else{
					$(".daterangepicker").css("display", "block"); 
					$(Id).trigger('show.daterangepicker', this);
			}
			count++;
		});
		$(Id).change(function() {
			$(".daterangepicker").css("display", "block"); 
			$(Id).trigger('show.daterangepicker', this);
			count = 0;
			});		
		 $(document).mouseup(function() {
			if(datepicker_visible){
			$(".daterangepicker").css("display", "block"); 
			$(Id).trigger('show.daterangepicker', this); 
			}
			else{
				count = 0;	
			}	
		});
	} 