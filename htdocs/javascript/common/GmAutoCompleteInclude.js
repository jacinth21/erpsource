/**
 * 
 */


// This function used to call the Auto complete search
function fnAutoCompleteCall() {
	if (localkey == '') {
		fnAutoSearch();
	}
}

// 
function fnAutoSearch() {
var localObjLen = 0;
	$(autoControlId).autocomplete({
		minLength : mLen,
		source : function(request, response) {
			// IE 5 not support the object keys - checking the undefined
			if(Object.keys != undefined){
				localObjLen = Object.keys(localJSON).length;
			}
			if (localObjLen <= 25) {
				$.ajax({
					url : urlStr,
					type : "POST",
					data : {
						term : request.term
					},

					dataType : "json",
					success : function(data) {
						// to validate the data and show the message
						if (!data.length) {
							var result = [ {
								label : 'No data found',
								value : response.term
							} ];
							response(result);
						} else {
							localkey = request.term;
							localJSON = data;
							response($.map(data, function(el) {
								var val = el.NAME;
								Id = el.ID;
								return {
									label : val,
									value : val,
									id : Id
								};
							}));
						}
					}
				});
			} else {
				response($.map(localJSON, function(el) {
					var val = el.NAME;
					Id = el.ID;
					return {
						label : val,
						value : val,
						id : Id
					};
				}));
			}// end if
		}
	});
}