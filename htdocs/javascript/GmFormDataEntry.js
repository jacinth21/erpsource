// value used to store unfilled question information 
var strQuestionError  = '';
var strPrevious_Q_No = '';
var strAlpha = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefgnijklmnopqrstuvwxwz';
var strNum = '0123456789';
var strAlphaNum = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
var strStperId = '';
var strFlag = '';
var strIniNotReqFl = '';
function fnLoad()
{
	document.frmOrder.hAction.value = "Reload";
	document.frmOrder.submit(); 
}

function fnLoadFormsNoEvent(obj)
{
	if(document.frmOrder.cbo_EventNo != undefined)
	{
		document.frmOrder.cbo_EventNo.options.length = 0;
	}
	fnLoadForms(obj);
}

function fnLoadForms(obj){
	
	val = eval(obj.value);
	formObj = document.frmOrder.Cbo_Form;
	formVal = formObj.options[formObj.selectedIndex].value;

	j=0;
	strStperId = '';
	strFlag = '';
	strIniNotReqFl = '';
	document.frmOrder.Cbo_Form.options.length = 0;
	document.frmOrder.Cbo_Form.options[0] = new Option("[Choose One]","0");
	for (i=0;i< FormListLen;i++)
	{
		arr = eval("FormArr"+i);
		arrobj = arr.split(",");
		id = arrobj[0];
		name = arrobj[1];
		inifl = arrobj[2];
		fl = arrobj[3];
		period = arrobj[4];
		stperid = arrobj[5];

		if(period == val )
		{
			if(id==formVal){
				document.frmOrder.Cbo_Form.options[j+1] = new Option(name,id);
				document.frmOrder.Cbo_Form.options[j+1].selected=true;
			} else {			
				document.frmOrder.Cbo_Form.options[j+1] = new Option(name,id); 
			}
			strStperId = strStperId + id + ','+ stperid+'^';
			strFlag = strFlag + id + ',' + fl +'^';
			strIniNotReqFl = strIniNotReqFl + id + ',' + inifl +'^';			
			j++;
        }

	}
	fnDisableQuestions();
}

function fnDisableQuestions(){
	arrDisbQNoS = DisbQNoS.split(",");
	for (var i=0;i<arrDisbQNoS.length ;i++ )
	{
		obj = eval("document.frmOrder.hAnsGrpId"+arrDisbQNoS[i]);	
		if (obj)
		{
			obj.disabled = true;
		}
	}
}

function fnGetIniNotReqFl(stIniNotReqFl)
{
	strIniNotReqFl = strIniNotReqFl.substring(0,strIniNotReqFl.length-1);
	arrStNotReqFlobj = strIniNotReqFl.split("^");
	for (var k=0;k<arrStNotReqFlobj.length ;k++ )
	{
		arrStr = arrStNotReqFlobj[k].split(",");
		if(arrStr[0] == stIniNotReqFl)
		{
			return arrStr[1];
		}
	}
}

function fnGetSteprId(stPerIdVal)
{
	strStperId = strStperId.substring(0,strStperId.length-1);
	arrStPerobj = strStperId.split("^");
	for (var k=0;k<arrStPerobj.length ;k++ )
	{
		arrStr = arrStPerobj[k].split(","); 
		if(arrStr[0] == stPerIdVal)
		{
			return arrStr[1];
		}
	}
}

function fnGetFlagVal(stFlagVal)
{
	strFlag = strFlag.substring(0,strFlag.length-1);
	arrFlagobj = strFlag.split("^");
	for (var k=0;k<arrFlagobj.length ;k++ )
	{
		arrStr = arrFlagobj[k].split(","); 
		if(arrStr[0] == stFlagVal)
		{
			return arrStr[1];
		}
	}
}

function fnLoadSelForm()
{
	periodId = document.frmOrder.hPeriodId;
	var formId = document.frmOrder.hFormId.value;
	if(periodId.value != '')
	{
		fnLoadForms(periodId);
		var allForm = document.frmOrder.Cbo_Form;
		for(var m=0; m<allForm.length;m++)
		{
			if(allForm[m].value == formId )
			{
				document.frmOrder.Cbo_Form.options[m].selected = true;
			}
		}
		
	}
}
function fnLoadQuestions()
{
	Error_Clear();
	fnValidateDropDn('Cbo_Form',' Form Value ');
	document.frmOrder.hAction.value = "LoadQues";
	var stPerId = fnGetSteprId(document.frmOrder.Cbo_Form[document.frmOrder.Cbo_Form.selectedIndex].value);
	document.frmOrder.hStPerId.value = stPerId;
	if(document.frmOrder.Cbo_Period.value != '6309' && document.frmOrder.Txt_FormDate != null) {
	document.frmOrder.Txt_FormDate.value=""; }
	
	var voidedFl = document.frmOrder.hVoidFl.value;
	var dbFormid = document.frmOrder.hFormId.value; 
	var dbPeriodid = document.frmOrder.hPeriodId.value;
	
	//When the user is coming from "Voided Form" report, and when he click on load again, we need to display a new form for 
	// them to enter answers. So we do not need voided record in this case.
	
	if(dbFormid == document.frmOrder.Cbo_Form.value
	   && dbPeriodid ==document.frmOrder.Cbo_Period.value
	   && voidedFl=='Y'){
		document.frmOrder.hIncludeVoidFrm.value = "N";
	}
	document.frmOrder.hVoidFl.value ="";
	
	if (ErrorCount > 0)
		 {
			Error_Show();
			Error_Clear();
			return false;
		}
// value 73 & 75 removed for Event dropdown in form.
	if(document.frmOrder.Cbo_Form.value == '15'||document.frmOrder.Cbo_Form.value == '53' ||document.frmOrder.Cbo_Form.value == '120')
	{
		if(document.frmOrder.cbo_EventNo != undefined)
		{
			document.frmOrder.cbo_EventNo.options.length = 0;
			
		}	
		if(document.frmOrder.Txt_FormDate != undefined)
		{
			document.frmOrder.Txt_FormDate.value = '';
		}
	}
	fnStartProgress('Y');
	document.frmOrder.submit();
}

function fnClearEventNo(){
	if(document.frmOrder.cbo_EventNo != undefined)
	{
		document.frmOrder.cbo_EventNo.options.length = 0;		
	}
}

function fnSubmit()
{
	var inputstr = '';
	var qcnt = document.frmOrder.hQuestCnt.value;
	var qid = '';
	var qanscnt = 0;
	var andid = '';
	var anslistid = '';
	var anslistds = '';

	// Basic validation
	Error_Clear();
	strQuestionError = '';
	strPrevious_Q_No = '';
		
	var examDate = '';
	var objRef='';

	if(document.frmOrder.Txt_FormDate!=null){
		examDate = document.frmOrder.Txt_FormDate.value;
		objRef = document.frmOrder.Txt_FormDate;
	}else if(document.frmOrder.hFormDate!=null){
		examDate =document.frmOrder.hFormDate.value;
		objRef = document.frmOrder.hFormDate;
	}
	// Validate the Exam Date

	if (examDate == '')
	{
		Error_Details(message[600]);
	}else{
		DateValidate(objRef, Messages(600))
	}
	diff = dateDiff(todaysDate,objRef.value);
	if (diff > 0)
	{
		Error_Details("<b>The exam date cannot be a future date.</b>");	
		Error_Show();
		Error_Clear();
		return false;
	}	
	if (document.frmOrder.hFormLocked.value == 'locked')
	{
		Error_Details("<b>The form is locked for update.</b>");	
		Error_Show();
		Error_Clear();
		return false;
	}

	// Validate the Exam Date
	// below code commented to fix issue 9712
	//document.frmOrder.hinitialNotReq.value = fnGetIniNotReqFl(document.frmOrder.Cbo_Form[document.frmOrder.Cbo_Form.selectedIndex].value);
	//if (document.frmOrder.hinitialNotReq.value != 'Y' && document.frmOrder.Txt_Initials.value == '')
	//{
	//	Error_Details(message[601]);
	//}else{
	if(document.frmOrder.Txt_Initials.enabled)
		{
	fnCheckString(document.frmOrder.Txt_Initials.value, Messages(1014), strAlphaNum);
		}
	//}

	// PC-1804 Reflect study - SRS score formula changes
	// reflect study to calculate sum and avg.
	var studyIdObj = document.frmOrder.hStudyId;
	var formDtls = document.frmOrder.hFormId.value;
	
	if(studyIdObj.value == 'GPR010' && formDtls == 276){
		ValidateAnswer(eval("document.frmOrder.hAnsGrpId27608") ,'text',  27608 );
		ValidateAnswer(eval("document.frmOrder.hAnsGrpId27616") ,'text',  27616 );
		ValidateAnswer(eval("document.frmOrder.hAnsGrpId27624") ,'text',  27624 );
		ValidateAnswer(eval("document.frmOrder.hAnsGrpId27632") ,'text',  27632 );
		ValidateAnswer(eval("document.frmOrder.hAnsGrpId27635") ,'text',  27635 );
		ValidateAnswer(eval("document.frmOrder.hAnsGrpId27640") ,'text',  27640 );
		ValidateAnswer(eval("document.frmOrder.hAnsGrpId27643") ,'text',  27643 );
	}
	
 for (i=0;i<qcnt;i++)
	{
		qid = '';
		ansid = '';
		anslistid = '';
		anslistds = '';
			
		qobj = eval("document.frmOrder.hQuesId"+i);
		qidarr = qobj.value;
		var arr = qidarr.split(',');

		for(var j=0;j<arr.length;j++)
		{
			qid = arr[0];
			qanscnt = arr[1];
		}

		ansid = qobj.id;
		
    	if (qanscnt == '1')
		{
			obj = eval("document.frmOrder.hAnsGrpId"+ansid);
			if ( obj.type == "select" || obj.type == "select-one")
			{

				anslistid = obj[obj.selectedIndex].value;
				ValidateAnswer(obj[obj.selectedIndex], obj.type,  ansid );
				objOther = eval("document.frmOrder.hAnsComboOther"+ansid);
				if (objOther.value == "true") {
					objOther = eval("document.frmOrder.txtOther"+ansid);
					anslistds = objOther.value;
				}				
			}
			else if ( obj.type == "checkbox")
			{
				anslistid = ''; 
				if (!obj.checked)
				{
					anslistds = '';
				}else
				{
					anslistds = 'checked';
				}
			}
			else if (obj.type == "text" || obj.type == "textarea")
			{
				ValidateAnswer(obj ,obj.type,  ansid );
				anslistds = obj.value;
			}

			if (document.frmOrder.hAction.value == 'LoadQues')
			{
				inputstr = inputstr + qid +"^"+ ansid + "^"+ anslistid + "^"+ anslistds + "|";
			}
			else
			{
				obj = eval("document.frmOrder.hAnsListId"+ansid);
				patanslistid = obj.value;
				inputstr = inputstr + patanslistid +"^"+ anslistid + "^"+ anslistds+ "^" + qid +"^"+ ansid +"|";
			}
		}
		else
		{
			ansidarr = ansid.split(',');
			for(var k=0;k<ansidarr.length;k++)
			{
				anslistid = '';
				anslistds = '';
				ansid = ansidarr[k];
				obj = eval("document.frmOrder.hAnsGrpId"+ansid);
				if ( obj.type == "select" || obj.type == "select-one")
				{
					anslistid = obj[obj.selectedIndex].value;
					ValidateAnswer(obj[obj.selectedIndex], obj.type,  ansid );
					objOther = eval("document.frmOrder.hAnsComboOther"+ansid);
					if (objOther.value == "true") {
						objOther = eval("document.frmOrder.txtOther"+ansid);
						anslistds = objOther.value;
					}					
				}
				else if ( obj.type == "checkbox")
				{
					anslistid = ''; 
					if (!obj.checked)
					{
						anslistds = '';
					}else
					{
						anslistds = 'checked';
					}
				}
				else if (obj.type == "text" || obj.type == "textarea")
				{
					anslistds = obj.value;
					ValidateAnswer(obj ,obj.type,  ansid );
				}
				if (document.frmOrder.hAction.value == 'LoadQues')
				{
					inputstr = inputstr + qid +"^"+ ansid + "^"+ anslistid + "^"+ anslistds + "|";
				}
				else
				{
					
					obj = eval("document.frmOrder.hAnsListId"+ansid);
					patanslistid = obj.value;
					
					inputstr = inputstr + patanslistid +"^"+ anslistid + "^"+ anslistds + "^" + qid +"^"+ ansid +"|";
				}
			}
		}
	}

	
	if (strQuestionError != '')
	{
		Error_Details(strQuestionError);
	}

	// Validate the Exam Date
	if (document.frmOrder.Cbo_VerifyType && document.frmOrder.Cbo_VerifyType.value == '0')
	{
		Error_Details(message[602]);
	}
	fnValidateAnsGroup();
	if (ErrorCount > 0 && document.frmOrder.Chk_MissingFollowUp.checked == false)//To show validation for select reason BUG-4690
	{
		Error_Show();
		return false;
	}else{
		document.frmOrder.hInputStr.value = inputstr;
		if(document.frmOrder.Chk_MissingFollowUp.checked == true)
		{
		 document.frmOrder.Chk_MissingFollowUp.value='Y';
		}
		document.frmOrder.hStPerId.value = fnGetSteprId(document.frmOrder.Cbo_Form[document.frmOrder.Cbo_Form.selectedIndex].value);

		document.frmOrder.hFlag.value = fnGetFlagVal(document.frmOrder.Cbo_Form[document.frmOrder.Cbo_Form.selectedIndex].value);
		if(fnConfirm())
		 {
			// Code modified for resolving Invaid Number while submitting the form after cancel the Confirmation alert[PMT-243].
			// Validate the Exam Date
			if (document.frmOrder.hAction.value == 'LoadQues')
			{
				document.frmOrder.hAction.value = "SaveAns";
			}
			else
			{	
				document.frmOrder.hAction.value = "EditAns";
			}
			document.frmOrder.Btn_Submit.disabled = true;
			document.frmOrder.Cbo_VerifyType.disabled = false;
			fnStartProgress('Y');
			document.frmOrder.submit();
		 }
	}

}

function fnConfirm()
 {
   return confirm("Are you sure want to save ?");   
 }
 
// Validate for the Answer Section 
function ValidateAnswer(value, type, ansid )
{
	objfrmval = eval("document.frmOrder.hFormVal"+ansid);
	if (objfrmval.value != '')
	{
		objSeq = eval("document.frmOrder.hAnsSeq"+ansid);
		fnFormValidate(objfrmval.value,value,objfrmval, objSeq);
		
	}
	
	if ( type == "select" || type == "select-one")
	{
		if (value.value == "0") 
		{
			objSeq = eval("document.frmOrder.hAnsSeq"+ansid);
			if (objSeq.id == 'false')
			{
				FrameAnswerInfo(objSeq.value);
			}
		}
	}
	else
	{
		strValue  = TRIM(value.value);
		
		if (strValue == "")
		{
			objSeq = eval("document.frmOrder.hAnsSeq"+ansid);
			if (objSeq.id == 'false')
			{
				FrameAnswerInfo(objSeq.value);
			}
			
		}	
	}
	
}


function FrameAnswerInfo(strQuesNo)
{
	
	if ( strPrevious_Q_No != strQuesNo) 
	{ 
		if (strQuestionError == '')
		{
			strQuestionError  = "Answers missing, pl validate the Question No <b> " + strQuesNo + "</b> ";
		}
		else
		{
			strQuestionError  = strQuestionError + ", <b> " + strQuesNo + "</b> "
		}
	}	
	strPrevious_Q_No = strQuesNo;
}
// To view if the value us high 
function ShowHideInputField (selectedValue, checkValue, fldObj) {
	if(selectedValue!='0'){
		if(document.getElementById("hCmtRequiredFlg"+selectedValue).value == 'Y' && document.getElementById("hCmtRequiredFlg"+selectedValue).value!=undefined){
		     fldObj.value = "";
		     fldObj.style.display = 'block';
	    }else{
		     fldObj.style.display = "none";
	         fldObj.value = "";
	    }
	}else{
         fldObj.style.display = 'none';
         fldObj.value = "";
    }
}

function fnCallHistory(val)
{
	windowOpener("/GmFormDataEntryServlet?hAction=ViewHis&hPatListId="+val,"AnsHistory","resizable=yes,scrollbars=yes,top=150,left=100,width=900,height=400");
}

function fnLoadPage()
{
	var hFormDt = "";
	var examDt = "";
	
		if(document.frmOrder.hPeriodId.value == '6309')
		 {
		  if(document.frmOrder.Chk_MissingFollowUp != null)
		   {
		   	document.frmOrder.Chk_MissingFollowUp.disabled = true;
		   	}
		   if(document.frmOrder.BtnGo != null)
		    {	
			   document.frmOrder.BtnGo.disabled = false;
			   if(document.frmOrder.hFormId.value == 15)
				{
					document.frmOrder.BtnGo.disabled = true;
				}
			}   
		 }
		 else
		 {
		
		   if(document.frmOrder.Chk_MissingFollowUp != null)
		   {
		    document.frmOrder.Chk_MissingFollowUp.disabled = false;
		   }
		   if(document.frmOrder.BtnGo != null)
		    {	
			   document.frmOrder.BtnGo.disabled = true;
			}   
		 }
		 fnLoadSelForm();
		 if(document.frmOrder.Txt_Initials != null)
		  {
		  document.frmOrder.hinitialNotReq.value = fnGetIniNotReqFl(document.frmOrder.Cbo_Form[document.frmOrder.Cbo_Form.selectedIndex].value);	
		  if(document.frmOrder.hinitialNotReq.value == 'Y')
			{
				 document.frmOrder.Txt_Initials.disabled = true;
			}
		  }
		  /*
		  if (document.frmOrder.hAction.value == 'Reload')
		  {
			document.frmOrder.Cbo_Period.value = 0;
		  }
		  */
		  fnLoadSelForm();
	// Setting verify Type value based on exam date	  
	hFormId = document.frmOrder.hFormId.value;
	if(hFormId != ""){
		hFormDt = document.frmOrder.hFormDt.value;
		
		examDt = "";
		if(document.frmOrder.Txt_FormDate != undefined){
			examDt  = document.frmOrder.Txt_FormDate.value;
		}
		
		if((!(hFormDt == "")) || examDt != ""){
			var verifyTypeList = document.getElementById("VerifyType");
			for (var i=0;i<verifyTypeList.length;  i++) {
	   			if (verifyTypeList.options[i].value=='6190') {
	     			verifyTypeList.remove(i);
	   				}
			}
		document.frmOrder.Cbo_VerifyType.value = '0';
		document.frmOrder.Btn_Submit.disabled = false;		
		}else{
			document.frmOrder.Cbo_VerifyType.value = '6190';
			document.frmOrder.Cbo_VerifyType.disabled = true;
		}
	}		
}


function fnFormValidate(val,data,obj, objSeq)
{
	var arr = val.split(',');
	
	for(var j=0;j<arr.length;j++)
	{
		fnid = arr[j];
		
		fnCallValMaster(fnid,data,obj, objSeq);
	}
	
}

function fnCallValMaster(id,data,obj, objSeq)
{	
	switch (id)
	{
		case '1': 
		fnValidateTxtFld(obj,'test');
		break;
		case '2': 
		DateValidateMMDDYY(data.value, Messages(1001)+" in Question No  <b>"+objSeq.value+"</b>" );
		break;
		case '3': 
		DateValidateMMYYYY(data.value, Messages(1002)+" in Question No  <b>"+objSeq.value+"</b>" );
		break;
		case '4': 
		BirthDateValidateInRange(data, Messages(1003)+" in Question No  <b>"+objSeq.value+"</b>" , 18, 60 );
		break;
		case '5': 
		BirthDateValidate(data, Messages(1004)+" in Question No  <b>"+objSeq.value+"</b>" , 50 );
		break;
		case '6': 
		NumberValidation(data.value, Messages(1005)+" in Question No  <b>"+objSeq.value+"</b>" , 1 );
		break;
		case '7': 
		AlphaNumWithDashValidation(data.value, Messages(1006)+" in Question No  <b>"+objSeq.value+"</b>" , strAlphaNum,'-',2 );
		break;
		case '8': 
		Number2DigitValidation(data.value, Messages(1007)+" in Question No  <b>"+objSeq.value+"</b>" , 2, strNum );
		break;
		case '9': 
		validateDecimals(data.value, Messages(1008)+" in Question No  <b>"+objSeq.value+"</b>" , 2, strNum );
		break;
		case '10': 
		partValidation(data.value, Messages(1009)+" in Question No  <b>"+objSeq.value+"</b>" , strNum , 3, 7, ".");
		break;
		case '11': 
		validateDecimals(data.value, Messages(1010)+" in Question No  <b>"+objSeq.value+"</b>" , 2, strNum );
		break;
		case '12': 
		fnCheckString(data.value, Messages(1011)+" in Question No  <b>"+objSeq.value+"</b>" , strAlpha );
		break;
		case '13': 
		fnCheckString(data.value, Messages(1012)+" in Question No  <b>"+objSeq.value+"</b>" , strAlphaNum );
		break;
		case '14': 
		DateValidate(data, Messages(1013)+" in Question No  <b>"+objSeq.value+"</b>"  );
		break;
		case '15': 
		validateDecimals(data.value, Messages(1017)+" in Question No  <b>"+objSeq.value+"</b>" , 1, strNum );
		break;
		case '16': 
		partValidation(data.value, Messages(1018)+" in Question No  <b>"+objSeq.value+"</b>" , strNum , 1, 8, "-");
		break;
		case '17':
			
		// PMT-37363: Aminos study - Formula changes	
		var studyIdObj = document.frmOrder.hStudyId;
		
		// to getting the study id
		if(studyIdObj != undefined){
			if(studyIdObj.value == 'GPR009'){ 
				// Study is Amnios then separte formula
				
				fnCalculateAmniosScore(data);
				
			// PC-1804 Reflect study - SRS score formula changes
			} else if (studyIdObj.value == 'GPR010'){ 
				// Study is Amnios then separte formula
				
				fnCalculateReflectScore (data);
				
			}else{
				// existing formula
				
				calAvg(data);
			}
		}
		
		break;
	    default : 
		return;
	}
	
}

function fnLoadHistory(colId,patId)
{
	windowOpener("/gmAuditTrail.do?auditId="+ colId +"&txnId="+ patId,"","resizable=yes,scrollbars=yes,top=250,left=300,width=785,height=300,status=1");
}

function fnQueryDetails (patientAnsListId, patientListId, queryLevelId)
{

	windowOpener("/gmQuery.do?method=fetchQueryContainer&queryLevelId="+queryLevelId+"&patientAnsListId="+patientAnsListId+"&patientListId="+patientListId,"","scrollbars=yes,top=150,left=100,width=950,height=490");
}

function fnEcDetails (patientId, status, ansGrpId, patientListId )
{
	windowOpener("/gmEditCheckReportAction.do?method=fetchEditCheckIncidentReport&formDataScr=true&patientId="+patientId+"&status="+status+"&ansGrpId="+ansGrpId+"&patientAnsListId="+patientListId+"&strOpt=loadReport","","scrollbars=yes,top=150,left=100,width=770,height=700");
}


function fnPatientLog()
{
  var timePeriod = document.frmOrder.hPeriodId.value;
  var hPatientId =  document.frmOrder.hPatientId.value;
  var frmId =  document.frmOrder.hFormId.value;
 var hPatientListId = document.frmOrder.hPatListId.value;
 var strOpt= "";

// alert(timePeriod + " " +hPatientId + " " + frmId + " " + hPatientListId );

 if(hPatientListId != "")
	{
	   strOpt = "edit";
	}

  windowOpener("/gmClinicalPatientLog.do?strOpt="+strOpt+"&studyPeriod="+timePeriod+"&studyForm="+frmId+"&patientId="+hPatientId+"&patientLId="+hPatientListId,"","scrollbars=no,top=200,left=150,width=760,height=260");
}

function fnVoid()
{
	if(document.frmOrder.hPatListId.value=='')
	{
	Error_Details("<b>Please select a transaction to void</b>");	
	Error_Show();
	Error_Clear();
	return false;
	}
	document.frmOrder.action ="/GmCommonCancelServlet";
	document.frmOrder.hTxnId.value = document.frmOrder.hPatListId.value;
	document.frmOrder.hTxnName.value = document.frmOrder.hFromName.value;
	document.frmOrder.hCancelType.value = 'VDFORM';
	document.frmOrder.hAction.value = 'Load';
	document.frmOrder.submit(); 
}
// New Function added for Un-Voiding the Forms as part of MNTTASK-3766
function fnUnvoid(){
	document.frmOrder.hAction.value = 'Unvoid';
	document.frmOrder.submit(); 
	
}
//PMT-243 function to validate form answer group
function fnValidateAnsGroup(){
	errCnt=0;
	if(FormVldArrLen > 0)
		for(i=0 ;i<FormVldArrLen;i++){
			FormVldArr = eval("FormVldArr"+i);
			FormVldArrObj = FormVldArr.split("|");
			ansGrpArr = FormVldArrObj[0];
			vldMsg = FormVldArrObj[1];
			ansGrpArrObj = ansGrpArr.split(","); 
			if(ansGrpArrObj.length > 1){
				for(j=0;j<ansGrpArrObj.length;j++){
					ansGrpVal=eval("document.frmOrder.hAnsGrpId"+ansGrpArrObj[j]).value;
					if(ansGrpVal == '0' || ansGrpVal == '')
						errCnt++;	
				}
				if(errCnt == ansGrpArrObj.length){
					Error_Details(vldMsg);
				}
				
			}else if(ansGrpArrObj.length == 1){
				ansGrpVal=eval("document.frmOrder.hAnsGrpId"+ansGrpArrObj[0]).value;
				if(ansGrpVal == '0' || ansGrpVal == '')
					Error_Details(vldMsg);
			}
		}
}

// PMT-7174 function to change verify type
function fnChangeType(){
	
	var missingFlg;
	var verifyTypeList = document.getElementById("VerifyType");
	var verifyFl = document.frmOrder.hVerifyFl.value;
	if(!(document.frmOrder.hPatListId.value=='')){
		
		document.frmOrder.txtaVerifyDesc.value = "";
		missingFlg = document.frmOrder.Chk_MissingFollowUp.checked;
		
		if(missingFlg == true){
			
			document.frmOrder.Cbo_VerifyType.value = '6192';
			document.frmOrder.Cbo_VerifyType.disabled = true;
			
		}else{
			document.frmOrder.Cbo_VerifyType.disabled = false;
			document.frmOrder.Cbo_VerifyType.value = '0';
			document.frmOrder.Btn_Submit.disabled = false;
	
			for (var i=0;i<verifyTypeList.length;  i++) {
   				if (verifyTypeList.options[i].value=='6190') {
     				verifyTypeList.remove(i);
   				}
			}		
			
		}
		
	}
	
	
}

// PC-1804 Reflect study - SRS score formula changes
function fnCalculateReflectScore (dataObj){
	
	if (dataObj == undefined) {
		return false;
	}

	if (dataObj.name == "")
		return false;

	var startVal = 0;
	var endVal = 0;
	var count = 0;
	var divCount = 0;
	var selectedCount = 0;
	var scoreVal = 0;
	var additionValueFl = false;
	//
	var numberEnteredVal = 0;
	//
	var sumOfIdObj = '';
	var divOfIdObj= '';
	var totalOfId = '';

	var id = dataObj.name;

	switch (id) {
	// Function
	case 'hAnsGrpId27608':
		startVal = 27601;
		endVal = 27605;
		additionValueFl = true;
		sumOfIdObj = eval("document.frmOrder.hAnsGrpId27606");
		divOfIdObj = eval("document.frmOrder.hAnsGrpId27607");
		// divCount = 28;
		break;
		//Pain
	case 'hAnsGrpId27616':
		startVal = 27609;
		endVal = 27613;
		additionValueFl = true;
		sumOfIdObj = eval("document.frmOrder.hAnsGrpId27614");
		divOfIdObj = eval("document.frmOrder.hAnsGrpId27615");
		// divCount = 28;
		break;
		// Self image
	case 'hAnsGrpId27624':
		startVal = 27617;
		endVal = 27621;
		additionValueFl = true;
		sumOfIdObj = eval("document.frmOrder.hAnsGrpId27622");
		divOfIdObj = eval("document.frmOrder.hAnsGrpId27623");
		// divCount = 44;
		break;
		//Mental Health
	case 'hAnsGrpId27632':
		startVal = 27625;
		endVal = 27629;
		additionValueFl = true;
		sumOfIdObj = eval("document.frmOrder.hAnsGrpId27630");
		divOfIdObj = eval("document.frmOrder.hAnsGrpId27631");
		// divCount = 12;
		break;
		// subtotal
	case 'hAnsGrpId27635':
		startVal = 0;
		endVal = -1;
		//
		sumOfIdObj = eval("document.frmOrder.hAnsGrpId27633");
		divOfIdObj = eval("document.frmOrder.hAnsGrpId27634");
		//
		var sumFunctionVal = eval("document.frmOrder.hAnsGrpId27606").value == '' ? 0 : eval("document.frmOrder.hAnsGrpId27606").value;
		var sumPainVal = eval("document.frmOrder.hAnsGrpId27614").value == '' ? 0 : eval("document.frmOrder.hAnsGrpId27614").value;
		var sumSelfImaVal = eval("document.frmOrder.hAnsGrpId27622").value == '' ? 0 : eval("document.frmOrder.hAnsGrpId27622").value;
		var sumMentalVal = eval("document.frmOrder.hAnsGrpId27630").value == '' ? 0 : eval("document.frmOrder.hAnsGrpId27630").value;
		//
		var divFunctionVal = eval("document.frmOrder.hAnsGrpId27607").value == '' ? 0 : eval("document.frmOrder.hAnsGrpId27607").value;
		var divPainVal = eval("document.frmOrder.hAnsGrpId27615").value == '' ? 0 : eval("document.frmOrder.hAnsGrpId27615").value;
		var divSelfImaVal = eval("document.frmOrder.hAnsGrpId27623").value == '' ? 0 : eval("document.frmOrder.hAnsGrpId27623").value;
		var divMetalVal = eval("document.frmOrder.hAnsGrpId27631").value == '' ? 0 : eval("document.frmOrder.hAnsGrpId27631").value;
		//
		count = parseInt (sumFunctionVal) + parseInt (sumPainVal) + parseInt (sumSelfImaVal) + parseInt (sumMentalVal);
		//
		numberEnteredVal = parseInt (divFunctionVal) + parseInt (divPainVal) + parseInt (divSelfImaVal) + parseInt (divMetalVal);
		
		//additionValueFl = true;
		// divCount = 24;
		break;
		// management
	case 'hAnsGrpId27640':
		startVal = 27636;
		endVal = 27637;
		//
		sumOfIdObj = eval("document.frmOrder.hAnsGrpId27638");
		divOfIdObj = eval("document.frmOrder.hAnsGrpId27639");
		//
		additionValueFl = true;
		// divCount = 5;
		break;
	case 'hAnsGrpId27643':
		startVal = 0;
		endVal = -1;
		//
		sumOfIdObj = eval("document.frmOrder.hAnsGrpId27641");
		divOfIdObj = eval("document.frmOrder.hAnsGrpId27642");
		//
		var sumSubTotalVal = eval("document.frmOrder.hAnsGrpId27633").value == '' ? 0 : eval("document.frmOrder.hAnsGrpId27633").value;
		var sumManagementVal = eval("document.frmOrder.hAnsGrpId27638").value == '' ? 0 : eval("document.frmOrder.hAnsGrpId27638").value;
		//
		var divSubTotalVal = eval("document.frmOrder.hAnsGrpId27634").value == '' ? 0 : eval("document.frmOrder.hAnsGrpId27634").value;
		var divManagementVal = eval("document.frmOrder.hAnsGrpId27639").value == '' ? 0 : eval("document.frmOrder.hAnsGrpId27639").value;
		//
		count = parseInt (sumSubTotalVal) + parseInt (sumManagementVal);
		//
		numberEnteredVal = parseInt (divSubTotalVal) + parseInt (divManagementVal);		
		// divCount = 5;
		break;	
	default:
		return false;
	}

	// loop the values and calculate the score
	if (additionValueFl) {
		for (k = startVal; k <= endVal; k++) {
			objSelect = eval("document.frmOrder.hAnsGrpId" + k);
			objSelectVal = objSelect.value;

			if (objSelectVal != 0 && objSelectVal != '') {
				
					// text field calculate
					objSelectTxt = objSelectVal;
					count = count + parseFloat(objSelectTxt);
					//
					numberEnteredVal = numberEnteredVal + 1;

			} // end of object check
		} // end for loop

	}
	
	// Based on flag to calculate the values.
	if (count != 0) {
			sumOfIdObj.value = count;
			divOfIdObj.value = numberEnteredVal;
			scoreVal = (count / numberEnteredVal);
			// PMT-57326: Round decimal digit updated from 2 to 1 digit
			dataObj.value = fnRoundNumber(scoreVal, 1);

	} else {
		dataObj.value = "";
	} // end of count check

}
