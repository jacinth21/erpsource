function incJavascript(jsname) {
var th = document.getElementsByTagName('head')[0];
var s = document.createElement('script');
s.setAttribute('type','text/javascript');
s.setAttribute('src',jsname);
th.appendChild(s);
} 

function incCss(cssName) {
var th = document.getElementsByTagName('head')[0];
var s = document.createElement('link');
s.setAttribute('type','text/css');
s.setAttribute('rel','stylesheet');
s.setAttribute('href',cssName);
th.appendChild(s);
} 

incCss('/extweb/dhtmlx/dhtmlxCombo/dhtmlxcombo.css');
incJavascript("/extweb/dhtmlx/dhtmlxcommon.js");
incJavascript("/extweb/dhtmlx/dhtmlxCombo/dhtmlxcombo.js");
window.dhx_globalImgPath = "/javascript/dhtmlx/dhtmlxGrid/imgs/";

function loadDXCombo(divref,combo_nm, comboxml,event_blur, event_change, combo_width,tabIndx){
	var combo = new dhtmlXCombo(divref, combo_nm,combo_width);
combo.DOMelem_input.tabIndex=tabIndx; 
combo.enableFilteringMode(true);
combo.loadXMLString(comboxml);
combo.attachEvent("onOpen",function(){
	   window.setTimeout(function(){
	           var text = combo.getComboText();
	           combo.setComboText("");
	           combo.filterSelf();
	           combo.setComboText(text);
	             combo.DOMelem_input.select();
	            //combo.DOMelem_input.focus();
	       },1);
	});
if(event_blur != undefined&& event_blur.length >0){
	combo.attachEvent("onBlur",event_blur);
}

if(event_change != undefined&& event_change.length >0){
	combo.attachEvent("onChange",event_change);
}
  return combo;
} 
