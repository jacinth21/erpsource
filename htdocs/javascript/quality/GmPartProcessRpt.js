function initGridData(divRef,gridData)
{
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);
	gObj.enableTooltips("true,true,true,true,true,true,true");
	gObj.attachHeader('#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#rspan');
	gObj.init();		
	gObj.enablePaging(true, 20, 10, "pagingArea", true, "infoArea");
	gObj.setPagingSkin("bricks");
	gObj.loadXMLString(gridData);	
	return gObj;	
}
function fnLoad() {	
	var project = document.frmPartProcessRpt.projectID.value;
	var system  = document.frmPartProcessRpt.systemID.value;
	var partNum = document.frmPartProcessRpt.partNum.value;
	var vendor  = document.frmPartProcessRpt.vendorID.value;
	
	if(project == '0' && system == '0' && vendor == '0' && partNum == ''){
		Error_Details(message_prodmgmnt[94]);
	}
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	document.frmPartProcessRpt.btn_Load.disabled = true; //load time to disabled the button
	document.frmPartProcessRpt.action="/gmPartProcessRpt.do?method=partProcessRptData";
	fnStartProgress('Y');
	document.frmPartProcessRpt.submit();	
	return false;
}
function fnOnPageLoad(){	
	if (objGridData != '') {
		gridObj = initGridData('Div_ValidPO',objGridData);
	}
}
function fnOpenHistory(processID)
{		
		windowOpener("/GmPageControllerServlet?strPgToLoad=/gmAuditTrail.do&auditId=1050&txnId="+ processID, "","resizable=yes,scrollbars=yes,top=250,left=300,width=785,height=300,status=1");
}
function fnDownloadXLS()
{
		gridObj.toExcel('/phpapp/excel/generate.php');
	
}
	

