function  fnLoad(){		
	objStartDt = document.frmCOMRSRReport.frmDate;
	objEndDt = document.frmCOMRSRReport.toDate;
	var elapsedDys = document.frmCOMRSRReport.elapsedDays.value;
	var elapsedDysVal = document.frmCOMRSRReport.elapsedVal.value;
	var dateDiffVal = dateDiff(todaysDate,objStartDt.value,format);
	
	document.frmCOMRSRReport.hCOMRSRID.value = '';
	document.frmCOMRSRReport.hStatusId.value = '';
	document.frmCOMRSRReport.hIssueTyp.value = '';
	
	if(objStartDt.value != ""){
		CommonDateValidation(objStartDt,format,Error_Details_Trans(message_prodmgmnt[197],format));
	}
	if(objEndDt.value != ""){		
		CommonDateValidation(objEndDt,format,Error_Details_Trans(message_prodmgmnt[198],format));
	}
	if(objStartDt.value != "" && objEndDt.value == ""){
		Error_Details(message_prodmgmnt[199]);
	}
	if(objEndDt.value != "" && objStartDt.value == ""){
		Error_Details(message_prodmgmnt[200]);	
	}	
	
	var fromToDiff = dateDiff(objStartDt.value, objEndDt.value, format);
	var fromCurrDiff = dateDiff(objStartDt.value, todaysDate, format);
	var toCurrDiff  = dateDiff(objEndDt.value, todaysDate, format);
	
	
	var dtFrom = document.frmCOMRSRReport.frmDate.value;
    var dtTo = document.frmCOMRSRReport.toDate.value;

	if(fromToDiff < 0){
   	 Error_Details(message_prodmgmnt[201]);
	 }
	 if(fromCurrDiff < 0){
		 Error_Details(message_prodmgmnt[202]);
	 }
	 if(toCurrDiff < 0){
		 Error_Details(message_prodmgmnt[203]);
	 }
	 
	 
	if(elapsedDys == '0' && elapsedDysVal != ''){
		Error_Details(message_prodmgmnt[204]);
	}else if(elapsedDys != '0' && elapsedDysVal == ''){
		Error_Details(message_prodmgmnt[205]);
	}else if(elapsedDysVal != '' && elapsedDys != '0'){
		fnValidateNumber('elapsedVal',lblelapseddays);
	}
	
	if (ErrorCount > 0)
		{
			Error_Show();
			Error_Clear();
			return false;
		}
	
	fnStartProgress();
	document.frmCOMRSRReport.strOpt.value = "Report";
	document.frmCOMRSRReport.submit();
	document.frmCOMRSRReport.Btn_Load.disabled = true;
}

function fnSubmit(){
	var frm = document.frmCOMRSRReport;
	var COMRSRid = frm.hCOMRSRID.value;
	var statusId = frm.hStatusId.value;
	var chooseAction = frm.chooseAction.value;
	var issue    = frm.hIssueTyp.value;
	
	var logType;
	if(issue == '102809'){
		logType = 4000319;
	}else if(issue == '102810'){
		logType = 4000320;
	}
	if (COMRSRid == '' && chooseAction != '0') {
		Error_Details(message_prodmgmnt[206]);
	}
	if ((COMRSRid != '') && (chooseAction == '0')){
		Error_Details(message_prodmgmnt[207]);
	}
	if ((COMRSRid != '') && (chooseAction != '0')){ 
		if(chooseAction == '102842'){ // Navigate to COM/RSR Incident Information screen
			document.frmCOMRSRReport.action = "/gmCOMRSRSetupAction.do?method=editIncidentInfo&strOpt=Edit&comRsrID="+COMRSRid;
		}
		if(chooseAction == '102843'){ // Navigate to QA Evaluation Screen
			document.frmCOMRSRReport.action = "/gmCOMRSRSetup.do?method=editQAEvaluation&strOpt=Edit&COMRSRId="+COMRSRid;
		}
		if(chooseAction == '102844'){ // Navigate to Engineering Evaluations screen
			document.frmCOMRSRReport.action = "/gmEnggEvaluation.do?method=editEnggEvaluation&strOpt=Edit&incidentId="+COMRSRid;
		}
		if(chooseAction == '102845'){ // Navigate to Close COM/RSR Screen
			document.frmCOMRSRReport.action = '/gmCOMRSRProcess.do?method=closeCOMRSR&strOpt=Fetch&closeCOMRSRID='+ COMRSRid;
		}
		if(chooseAction == '102846'){ // Navigate to Upload screen
			document.frmCOMRSRReport.action = "/gmUploadAction.do?strOpt=Fetch&refType=102790&logType="+logType+"&strRefID="+COMRSRid;
		}
		if(chooseAction == '102847'){ // Navigate to Void Screen
			if(issue == '102809'){
				document.frmCOMRSRReport.action = "/GmCommonCancelServlet?hTxnName="+COMRSRid+"&hCancelType=VDCOMP&hTxnId="+COMRSRid+"&hAction=Load";
				document.frmCOMRSRReport.hDisplayNm.value = 'Modify COM/RSR';
				document.frmCOMRSRReport.hRedirectURL.value = '/gmCOMRSRReportAction.do?method=listCOMRSRDetails&strOpt=load&accessFl=true';
			}else{
				document.frmCOMRSRReport.action = "/GmCommonCancelServlet?hTxnName="+COMRSRid+"&hCancelType=VDRSR&hTxnId="+COMRSRid+"&hAction=Load";
				document.frmCOMRSRReport.hDisplayNm.value = 'Modify COM/RSR';
				document.frmCOMRSRReport.hRedirectURL.value = '/gmCOMRSRReportAction.do?method=listCOMRSRDetails&strOpt=load&accessFl=true';
			}

		}
		if(chooseAction == '102848'){ // Navigate to Re-Open screen
			if(statusId == '102841'){
				if(issue == '102809'){
					document.frmCOMRSRReport.action = "/GmCommonCancelServlet?hTxnName="+COMRSRid+"&hCancelType=ROPCOM&hTxnId="+COMRSRid+"&hAction=Load";
					document.frmCOMRSRReport.hDisplayNm.value = 'Modify COM/RSR';
					document.frmCOMRSRReport.hRedirectURL.value = '/gmCOMRSRReportAction.do?method=listCOMRSRDetails&accessFl=true&strOpt=Report&comRSR='+COMRSRid;
				} else{
					document.frmCOMRSRReport.action = "/GmCommonCancelServlet?hTxnName="+COMRSRid+"&hCancelType=ROPRSR&hTxnId="+COMRSRid+"&hAction=Load";
					document.frmCOMRSRReport.hDisplayNm.value = 'Modify COM/RSR';
					document.frmCOMRSRReport.hRedirectURL.value = '/gmCOMRSRReportAction.do?method=listCOMRSRDetails&accessFl=true&strOpt=Report&comRSR='+COMRSRid;
				}
			}else{
				Error_Details(message_prodmgmnt[192]);
			}
			
		}
		
	}else if((COMRSRid == '') && (chooseAction == '0')){
		Error_Details(message_prodmgmnt[208]);
		Error_Details(message_prodmgmnt[209]);
	}

	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	document.frmCOMRSRReport.submit();
	document.frmCOMRSRReport.Btn_Submit.disabled = true;
	fnStartProgress();
}

//This function is used to initiate grid
var gridObj;
function fnOnPageLoad()
{ 
	if(objGridData.value != ''){ 
		gridObj = initGrid('dataGridDiv',objGridData);
	}	
}

function initGridData(divRef,gridData){
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);
	gObj.enableTooltips("true,true,true,true,true,true,true");
	gObj.init();
	gObj.loadXMLString(gridData);	
	return gObj;	
}

//This function is used to activate Load button while pressing the enter key
function fnEnter() {
	if(event.keyCode == 13){
		fnLoad();
	}
}

//To hide first two columns of grid while exporting to excel
function fnExportExcel(){
	if(accessFl == "true"){
		gridObj.setColumnHidden(0,true);
		gridObj.setColumnHidden(1,true);
		gridObj.toExcel('/phpapp/excel/generate.php');
		gridObj.setColumnHidden(0,false);
		gridObj.setColumnHidden(1,false);
	}else{
		gridObj.setColumnHidden(0,true);
		gridObj.toExcel('/phpapp/excel/generate.php');
		gridObj.setColumnHidden(0,false);
	}
 
}

//To set the Selected COMRSRId from grid to jsp
function fnSetCOMRSRId(obj, statusId,issueTyp){ 
	document.frmCOMRSRReport.hCOMRSRID.value = obj;
	document.frmCOMRSRReport.hStatusId.value = statusId;
	document.frmCOMRSRReport.hIssueTyp.value = issueTyp;
	
}

//To open window to add/view comments
function fnOpenOrdLog(strCOMRSRId, issueTyp){
	var logType;
	if(issueTyp == 102809){ // COM
		logType = 4000319;
	}else if(issueTyp == 102810){ // RSR
		logType = 4000320;
	}
	windowOpener("/GmCommonLogServlet?hType="+logType+"&hID=" +strCOMRSRId,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
}

//To open the summary screen corresponding to the COM/RSR Id
function fnOpenSummaryScreen(COMRSRId){
	windowOpener("/gmCOMRSRProcess.do?method=comrsrSummary&strOpt=Summary&refType=102790&comRsrID="+COMRSRId,"","resizable=yes,scrollbars=yes,top=180,left=300,width=1010,height=800");
}

//To open the RA details
function fnOpenRADetails(val){
	windowOpener('/GmPrintCreditServlet?hAction=PRINT&hRAID='+val,"Returns","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=500");	
}
function fnComplaintNames(){
	
	var complaint = document.frmCOMRSRReport.complaint.value;	
	var strOpt = document.frmCOMRSRReport.strOpt.value;
	if(complaint == '0'){
		document.frmCOMRSRReport.complaintname.disabled = true;
	}
	if(complaint != '0' && complaint != '' ){
		document.frmCOMRSRReport.complaintname.disabled = false;
		if (complaint == '102812'){
			document.frmCOMRSRReport.complaintname.options.length = 0;
			document.frmCOMRSRReport.complaintname.options[0] = new Option("[Choose One]","0");
			for (var i=0;i<RepLen;i++){
				arr = eval("alRepListArr"+i);
				arrobj = arr.split(",");
				id = arrobj[0];
				name = arrobj[1];
				document.frmCOMRSRReport.complaintname.options[i+1] = new Option(name,id);
			}
		}else{
			document.frmCOMRSRReport.complaintname.options.length = 0;
			document.frmCOMRSRReport.complaintname.options[0] = new Option("[Choose One]","0");
			for (var i=0;i<EmpLen;i++){
				arr = eval("alEmpListArr"+i);
				arrobj = arr.split(",");
				id = arrobj[0];
				name = arrobj[1];
				document.frmCOMRSRReport.complaintname.options[i+1] = new Option(name,id);
			}
				
		}	
	
}
}
