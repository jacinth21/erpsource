
var address = '';
var phoneNumber = '';
var areaDirect = '';
var company = '';
var partFl=true;
//Enter key functionality.
function fnEnter(){	
	if(event.keyCode == 13)
	{	 
		var hIncidentId = document.frmIncidentInfo.hIncidentID.value;
		var incidentID  = document.frmIncidentInfo.comRsrID.value;
		if (hIncidentId != incidentID){
			var rtVal = fnLoad();
		}else{
			var rtVal = true;
		}
		if(rtVal == false){
			if(!e) var e = window.event;

	        e.cancelBubble = true;
	        e.returnValue = false;	

	        if (e.stopPropagation) {
	                e.stopPropagation();
	                e.preventDefault();
	        }
		}
	}
}

function fnShowFilters(val){
	var obj = eval("document.all."+val);
	var obj1 = eval("document.all."+val+"img");
	if (obj.style.display == 'none'){
		obj.style.display = 'block';
		if(obj1){
			obj1.src = '/images/minus.gif';
		}
	}else{
		obj.style.display = 'none';	
		if(obj1){
			obj1.src = '/images/plus.gif';
		}
	}
}

function fnSetDefaultValues(){
	var complaint = document.frmIncidentInfo.complaint.value;
	var strOpt = document.frmIncidentInfo.strOpt.value;
	var raid = document.frmIncidentInfo.raID.value;
	var complaintNm = document.frmIncidentInfo.complaintname.value;
	var hPartNum = document.frmIncidentInfo.partNum.value;
	if(strOpt != 'Load' && strOpt != 'RALoad')	{
		document.frmIncidentInfo.issueType.disabled=true;
	}else{
		document.frmIncidentInfo.issueType.disabled=false;
	}
	if(mdrValue == 'Yes' || medValue == 'Yes'){
		document.frmIncidentInfo.mdrReportDt.disabled = false;
		document.frmIncidentInfo.Img_Date[2].disabled = false;
		document.frmIncidentInfo.mdrID.disabled = false;		
	}else{	
		document.frmIncidentInfo.mdrReportDt.disabled = true;
		document.frmIncidentInfo.Img_Date[2].disabled = true;
		document.frmIncidentInfo.mdrID.disabled = true;
	}
	
	if(medValue == 'Yes'){
		document.frmIncidentInfo.meddevReportDt.disabled = false;
		document.frmIncidentInfo.Img_Date[3].disabled = false;
		document.frmIncidentInfo.meddevID.disabled = false;		
	}else{	
		document.frmIncidentInfo.meddevReportDt.disabled = true;
		document.frmIncidentInfo.Img_Date[3].disabled = true;
		document.frmIncidentInfo.meddevID.disabled = true;	
	}
	if(raid != '' && strOpt == 'RALoad'){
		fnRADetails(raid,hPartNum);
	}
	if(raid != ''){
		document.frmIncidentInfo.partNum.disabled = true;
	}
	
	if(strOpt != 'Edit'){
		if(complaintNm != '' && complaintNm != '0'){
			var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmCOMRSRSetupAction.do?method=addIncidentInfo&strOpt=ajax&complaintname='+ complaintNm + '&ramdomId=' + Math.random());
			dhtmlxAjax.get(ajaxUrl, fnSetRepDetails);
		}
	}
}
function fnComplaintNames(){
	var complaint = document.frmIncidentInfo.complaint.value;	
	var strOpt = document.frmIncidentInfo.strOpt.value;
	if(complaint == '0'){
		document.frmIncidentInfo("complaintname").value = '0';	
		document.frmIncidentInfo.complaintname.disabled = true;
	}
	if(complaint != '0' && complaint != '' ){
		document.frmIncidentInfo.complaintname.disabled = false;
		if (complaint == '102812'){
			document.frmIncidentInfo.complaintname.options.length = 0;
			document.frmIncidentInfo.complaintname.options[0] = new Option("[Choose One]","0");
			for (var i=0;i<RepLen;i++){
				arr = eval("alRepListArr"+i);
				arrobj = arr.split(",");
				id = arrobj[0];
				name = arrobj[1];
				document.frmIncidentInfo.complaintname.options[i+1] = new Option(name,id);
			}
			
		}else{
			document.frmIncidentInfo.complaintname.options.length = 0;
			document.frmIncidentInfo.complaintname.options[0] = new Option("[Choose One]","0");
			for (var i=0;i<EmpLen;i++){
				arr = eval("alEmpListArr"+i);
				arrobj = arr.split(",");
				id = arrobj[0];
				name = arrobj[1];
				document.frmIncidentInfo.complaintname.options[i+1] = new Option(name,id);
			}
				
		}	
		if(complaint == '102811'){
				document.frmIncidentInfo.company.value = "Globus";
				document.frmIncidentInfo.address.value = "";
				document.frmIncidentInfo.phoneNumber.value = "";
				document.frmIncidentInfo.areaDirector.value = "0";	
			
		}else if(complaint == '102812'){
				document.frmIncidentInfo.company.value = "";
				document.frmIncidentInfo.address.value = "";
				document.frmIncidentInfo.phoneNumber.value = "";
				document.frmIncidentInfo.areaDirector.value = "0";
		}
	}else{
		
			document.frmIncidentInfo.company.value = "";
			document.frmIncidentInfo.address.value = "";
			document.frmIncidentInfo.phoneNumber.value = "";
			document.frmIncidentInfo.areaDirector.value = "0";	
		
	}
}

function fnRADetails(raID,hPartNum) {
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmCOMRSRSetupAction.do?method=addIncidentInfo&strOpt=ajax&raID='+ raID +'&partNum='+encodeURIComponent(hPartNum)+ '&ramdomId=' + Math.random());
	dhtmlxAjax.get(ajaxUrl, fnSetRADetails);		
}
function fnPartDetails(obj) {
	var partNum = obj.value;
	
	if(partNum!=''){
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmCOMRSRSetupAction.do?method=addIncidentInfo&strOpt=ajax&partNum='+ encodeURIComponent(partNum) + '&ramdomId=' + Math.random());
		dhtmlxAjax.get(ajaxUrl, fnSetPartDetails);
	}else{
		document.frmIncidentInfo.partNum.value = "";
		document.frmIncidentInfo.partDesc.value = "";
		document.frmIncidentInfo.project.value = "0";
		document.frmIncidentInfo.type.value = "0";
		document.frmIncidentInfo.quantity.value = "";
		document.frmIncidentInfo.lotNum.value = "";
		var validText = document.frmIncidentInfo("validateText");
		validText.innerHTML ="";
		var validName = eval("document.all.validatePart");
		validName.style.display = 'none';
	}
}

function fnRepDetails(obj) {
	var complaint = document.frmIncidentInfo.complaint.value;	
	var repname = obj.value;	
	if(complaint == '102812'){
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmCOMRSRSetupAction.do?method=addIncidentInfo&strOpt=ajax&complaintname='+ repname + '&ramdomId=' + Math.random());
		dhtmlxAjax.get(ajaxUrl, fnSetRepDetails);
	}
}

function fnSetRepDetails(loader) {
	var response = loader.xmlDoc.responseText;
	var strArray = response.split('|');
	address = strArray[0];
	phoneNumber = strArray[1];
	areaDirect = strArray[2];
	company = strArray[3];
	var strOpt = document.frmIncidentInfo.strOpt.value;
		document.frmIncidentInfo.company.value = company;
		document.frmIncidentInfo.address.value = address;
		document.frmIncidentInfo.phoneNumber.value = phoneNumber;
		document.frmIncidentInfo.areaDirector.value = areaDirect;
}

function fnSetPartDetails(loader) {
	var response = loader.xmlDoc.responseText;
	var strArray = response.split('|');
	part = strArray[0];
	partDesc = strArray[1];
	partProject = strArray[2];
	partFamily = strArray[3];	
	var validName = eval("document.all.validatePart");
	var validText = document.frmIncidentInfo("validateText");
	if(response != null){
		document.frmIncidentInfo.partNum.value = part;
		document.frmIncidentInfo.partDesc.value = partDesc;
		document.frmIncidentInfo.project.value = partProject;
		document.frmIncidentInfo.type.value = partFamily;
		if(partDesc != ''){
			validName.style.display = 'block';
			validName.src = '/images/success.gif';
			validText.innerHTML ="Part is Available";
			partFl = true;
		}else{
			validName.style.display = 'block';
			validName.src = '/images/error.gif';
			validText.innerHTML ="Part is not Available";
			document.frmIncidentInfo.partNum.value = part;
			document.frmIncidentInfo.partDesc.value = "";
			document.frmIncidentInfo.project.value = "0";
			document.frmIncidentInfo.type.value = "0";
			document.frmIncidentInfo.quantity.value = "";
			document.frmIncidentInfo.lotNum.value = "";
			partFl = false;
		}
	}
}

function fnSetRADetails(loader) {
	var response = loader.xmlDoc.responseText;
	var strArray = response.split('|');	
	raID = strArray[0];
	RepID = strArray[1];
	partNum = strArray[2];
	partDesc = strArray[3];	
	partProject = strArray[4];
	partFamily = strArray[5];
	partQty = strArray[6];
	lotNum = strArray[7];	
	document.frmIncidentInfo.complaintname.value = RepID;
	document.frmIncidentInfo.partNum.value = partNum;
	document.frmIncidentInfo.partDesc.value = partDesc;
	document.frmIncidentInfo.project.value = partProject;
	document.frmIncidentInfo.type.value = partFamily;
	document.frmIncidentInfo.quantity.value = partQty;
	document.frmIncidentInfo.lotNum.value = lotNum;
	
}

function fnEnableMDR(obj){
	if(obj.value == 'Yes'){
		document.frmIncidentInfo.mdrID.disabled = false;
		document.frmIncidentInfo.mdrReportDt.disabled = false;
		document.frmIncidentInfo.Img_Date[2].disabled = false;
	}else{
		document.frmIncidentInfo.mdrID.value = "";
		document.frmIncidentInfo.mdrID.disabled = true;
		document.frmIncidentInfo.mdrReportDt.value = "";
		document.frmIncidentInfo.mdrReportDt.disabled = true;
		document.frmIncidentInfo.Img_Date[2].disabled = true;
	}	
}
function fnEnableMEDDEV(obj){
	if(obj.value == 'Yes'){
		document.frmIncidentInfo.meddevID.disabled = false;
		document.frmIncidentInfo.meddevReportDt.disabled = false;
		document.frmIncidentInfo.Img_Date[3].disabled = false;
	}else{
		document.frmIncidentInfo.meddevID.value = "";
		document.frmIncidentInfo.meddevID.disabled = true;
		document.frmIncidentInfo.meddevReportDt.value = "";
		document.frmIncidentInfo.meddevReportDt.disabled = true;
		document.frmIncidentInfo.Img_Date[3].disabled = true;
	}	
}

function fnLoad(){
	var comRsrid = document.frmIncidentInfo.comRsrID.value;
	if(comRsrid == ''){
		Error_Details(message_prodmgmnt[221]);
	}
	if (ErrorCount > 0){
		Error_Show();
	    Error_Clear();
		return false;
	}
	fnStartProgress('Y');
	document.frmIncidentInfo.action = '/gmCOMRSRSetupAction.do?method=editIncidentInfo&strOpt=Edit';
	document.frmIncidentInfo.submit();
	document.frmIncidentInfo.Btn_Load.disabled = true;
}

function fnSubmit(){	
	
	var issueType = document.frmIncidentInfo.issueType.value;	
	if(issueType == '0' || issueType == ''){
		Error_Details(message_prodmgmnt[222]);
	}
	if(!partFl){
		Error_Details(message_prodmgmnt[223]);
	}
	var hIncidentId = document.frmIncidentInfo.hIncidentID.value;
	var comRsrID = document.frmIncidentInfo.comRsrID.value;
	if (hIncidentId != '' && hIncidentId != comRsrID){
	Error_Details(message_prodmgmnt[77]);
	}
	CommonDateValidation(document.frmIncidentInfo.dtIncDate,format,Error_Details_Trans(message_prodmgmnt[224],format));
	CommonDateValidation(document.frmIncidentInfo.complRecvDt,format,Error_Details_Trans(message_prodmgmnt[225],format));
	CommonDateValidation(document.frmIncidentInfo.mdrReportDt,format,Error_Details_Trans(message_prodmgmnt[226],format));
	CommonDateValidation(document.frmIncidentInfo.meddevReportDt,format,Error_Details_Trans(message_prodmgmnt[227],format));
	CommonDateValidation(document.frmIncidentInfo.dateOfSurgery,format,Error_Details_Trans(message_prodmgmnt[228],format));
	CommonDateValidation(document.frmIncidentInfo.postOpeDate,format,Error_Details_Trans(message_prodmgmnt[229],format));
	
	fnLenValidation(document.frmIncidentInfo.ifYesExplain,message_prodmgmnt[230],"250");
	fnLenValidation(document.frmIncidentInfo.detailDescEvent,message_prodmgmnt[231],"4000");
	fnLenValidation(document.frmIncidentInfo.otherDetails,message_prodmgmnt[232],"4000");
		
	if (ErrorCount > 0){		
		Error_Show();
	    Error_Clear();
		return false;
	}
	
	// Get Json input string PC-3183
	
	var comRsrID=document.frmIncidentInfo.comRsrID.value;	
	var raID=document.frmIncidentInfo.raID.value;
	var issueType=document.frmIncidentInfo.issueType.value;
	var complaint=document.frmIncidentInfo.complaint.value;
	var complaintname=document.frmIncidentInfo.complaintname.value;
	var status=document.frmIncidentInfo.status.value;
	var address=document.frmIncidentInfo.address.value;
	var phoneNumber=document.frmIncidentInfo.phoneNumber.value;
	var company=document.frmIncidentInfo.company.value;
	var areaDirector=document.frmIncidentInfo.areaDirector.value;
	var dtIncDate=document.frmIncidentInfo.dtIncDate.value;
	var incidentCat=document.frmIncidentInfo.incidentCat.value;
	var complRecvdBy=document.frmIncidentInfo.complRecvdBy.value;
	var complRecvDt=document.frmIncidentInfo.complRecvDt.value;
	var complRecvVia=document.frmIncidentInfo.complRecvVia.value;
	var originator=document.frmIncidentInfo.originator.value;
	var mdrReport=document.frmIncidentInfo.mdrReport.value;
	var meddevReport=document.frmIncidentInfo.meddevReport.value;
	var mdrID= document.frmIncidentInfo.mdrID.value;
	var meddevID=document.frmIncidentInfo.meddevID.value;
	var mdrReportDt=document.frmIncidentInfo.mdrReportDt.value;
	var meddevReportDt=document.frmIncidentInfo.meddevReportDt.value;
	var detailDescEvent= document.frmIncidentInfo.detailDescEvent.value;
	//var screenType=document.frmIncidentInfo.screenType.value;
	//var statusNm=document.frmIncidentInfo.statusNm.value;	
	var partNum=document.frmIncidentInfo.partNum.value;
	var partDesc=document.frmIncidentInfo.partDesc.value;
	var project=document.frmIncidentInfo.project.value;
	var gem=document.frmIncidentInfo.gem.value;
	var type=document.frmIncidentInfo.type.value;
	var quantity=document.frmIncidentInfo.quantity.value;
	var lonorConsign=document.frmIncidentInfo.lonorConsign.value;
	//var projDesc=document.frmIncidentInfo.projDesc.value;
	var lotNum=document.frmIncidentInfo.lotNum.value;
	var normalWearFl=document.frmIncidentInfo.normalWearFl.value;
	//var eventSurgeyFl=document.frmIncidentInfo.eventSurgeyFl.value;
	var ifYesExplain=document.frmIncidentInfo.ifYesExplain.value;
	var typeOfSurgery=document.frmIncidentInfo.typeOfSurgery.value;
	var reviSurgery=document.frmIncidentInfo.reviSurgery.value;
	var surgeoonName=document.frmIncidentInfo.surgeoonName.value;
	var adverseEffect=document.frmIncidentInfo.adverseEffect.value;
	var hospitalName=document.frmIncidentInfo.hospitalName.value;
	var dateOfSurgery=document.frmIncidentInfo.dateOfSurgery.value;
	var postOpeFlag=document.frmIncidentInfo.postOpeFlag.value;
	var postOpeDate=document.frmIncidentInfo.postOpeDate.value;
	var eventDemoFl=document.frmIncidentInfo.eventDemoFl.value;
	var replacementFl=document.frmIncidentInfo.replacementFl.value;
	var replacePartFl=document.frmIncidentInfo.replacePartFl.value;
	var partAvailEvalFl=document.frmIncidentInfo.partAvailEvalFl.value;
	var deconFl=document.frmIncidentInfo.deconFl.value;
	var shippedFl=document.frmIncidentInfo.shippedFl.value;
	var otherDetails=document.frmIncidentInfo.otherDetails.value;
	//var chooseAction=document.frmIncidentInfo.chooseAction.value;
	var trackingNumber=document.frmIncidentInfo.trackingNumber.value;
	var dateOfShipped=document.frmIncidentInfo.dateOfShipped.value;
	var carrier=document.frmIncidentInfo.carrier.value;
	var reqFieldService=document.frmIncidentInfo.reqFieldService.value;
	var didThisEventHappenDuring=document.frmIncidentInfo.didThisEventHappenDuring.value;
	var procedureOutcome=document.frmIncidentInfo.procedureOutcome.value;
	var company=document.frmIncidentInfo.company.value;
	
	
	
	var inputJsonString = {			
			
			//var comRsrID=document.frmIncidentInfo("comRsrID").value;
			"comRsrID" :comRsrID,	
			"raID":	raID,
			"issueType":issueType,
			"complaint":complaint,
			"complaintname":complaintname,
			"status":status,
			"address":address,
			"phoneNumber":phoneNumber,
			"company":company,
			"areaDirector":areaDirector,
			"dtIncDate":dtIncDate,
			"incidentCat":incidentCat,
			"complRecvdBy":complRecvdBy,
			"complRecvDt":complRecvDt,
			"complRecvVia":complRecvVia,
			"originator":originator,
			"mdrReport":mdrReport,
			"meddevReport":meddevReport,
			"mdrID": mdrID,
			"meddevID":meddevID,
			"mdrReportDt":mdrReportDt,
			"meddevReportDt":meddevReportDt,
			"detailDescEvent": detailDescEvent,
			//"screenType":screenType,
			//"statusNm":statusNm,
			//"partRecvDt":document.frmIncidentInfo("partRecvDt");
			"partNum":partNum,
			"partDesc":partDesc,
			"project":project,
			"gem":gem,
			"type":type,
			"quantity":quantity,
			"lonorConsign":lonorConsign,
			//"projDesc":projDesc,
			"lotNum":lotNum,
			"normalWearFl":normalWearFl,
			//"eventSurgeyFl":eventSurgeyFl,
			"ifYesExplain":ifYesExplain,
			"typeOfSurgery":typeOfSurgery,
			"reviSurgery":reviSurgery,
			"surgeoonName":surgeoonName,
			"adverseEffect":adverseEffect,
			"hospitalName":hospitalName,
			"dateOfSurgery":dateOfSurgery,
			"postOpeFlag":postOpeFlag,
			"postOpeDate":postOpeDate,
			"eventDemoFl":eventDemoFl,
			"replacementFl":replacementFl,
			"replacePartFl":replacePartFl,
			"partAvailEvalFl":partAvailEvalFl,
			"deconFl":deconFl,
			"shippedFl":shippedFl,
			"otherDetails":otherDetails,
			//"chooseAction":chooseAction,
			"trackingNumber":trackingNumber,
			"dateOfShipped":dateOfShipped,
			"carrier":carrier,
			"reqFieldService":reqFieldService,
			"didThisEventHappenDuring":didThisEventHappenDuring,
			"procedureOutcome":procedureOutcome,
			"company":company
	};
	
	var  inputJsonValues = JSON.stringify(inputJsonString);
	confirmMsg = fnConfirmMessage();
	if(confirmMsg){
		fnStartProgress('Y');
		document.frmIncidentInfo.issueType.disabled=false;
		document.frmIncidentInfo.partNum.disabled = false;
		document.frmIncidentInfo.action = '/gmCOMRSRSetupAction.do?method=addIncidentInfo&strOpt=Save&strJsonString='+encodeURIComponent(inputJsonValues);
		//document.frmIncidentInfo.action = '/gmCOMRSRSetupAction.do?method=addIncidentInfo&strOpt=Save';
		document.frmIncidentInfo.submit();
		document.frmIncidentInfo.Btn_Submit.disabled = true;}	
	
}
function fnReset(){
	var strOpt = document.frmIncidentInfo.strOpt.value;
	var strIncidentID = document.frmIncidentInfo.comRsrID.value;
	if((strOpt == 'Load') || (strOpt == 'RALoad')){
		confirmMsg = fnConfirmMessage();
		if(confirmMsg) {
			if((strOpt == 'RALoad')){
				var raID = document.frmIncidentInfo.raID.value;
				document.frmIncidentInfo.action = '/gmCOMRSRSetupAction.do?method=addIncidentInfo&strOpt=RALoad&raID='+raID+"&complaintname="+compname+"&complaint=102812&issueType=102809";
				document.frmIncidentInfo.partNum.disabled = false;
				document.frmIncidentInfo.submit();
				document.frmIncidentInfo.submit();
			}
			else{
				document.frmIncidentInfo.action = '/gmCOMRSRSetupAction.do?method=addIncidentInfo&strOpt=Load';
				document.frmIncidentInfo.submit();
			}
		}
	}else if(strOpt == 'Edit'){
		confirmMsg = fnConfirmMessage();
		if(confirmMsg){
			if(strIncidentID != hIncidentId){
				document.frmIncidentInfo.comRsrID.value = hIncidentId;
			}
			if (hIncidentId != ''){
				fnLoad();
			}else{
				document.frmIncidentInfo.action = '/gmCOMRSRSetupAction.do?method=addIncidentInfo&strOpt=Load';
				document.frmIncidentInfo.submit();
			}
		}
	}
}
function fnConfirmMessage(){
	confirmMsg = confirm(message_prodmgmnt[233]);
	if(confirmMsg)	return true;
	else 			return false;
} 
function fnGo(){
	var chooseAction = document.frmIncidentInfo.chooseAction.value;
	var comrsrID = document.frmIncidentInfo.comRsrID.value;
	var issueType = document.frmIncidentInfo.issueType.value;
	var logType;
	if(issueType == '102809'){
		logType = 4000319;
	}else if(issueType == '102810'){
		logType = 4000320;
	}
	if(chooseAction == '0' || chooseAction == ''){
		Error_Details(message_prodmgmnt[234]);
		Error_Show();
	    Error_Clear();
		return false;
	}else{
		if(chooseAction == '102843'){ //QA Evaluation
			document.frmIncidentInfo.action = "/gmCOMRSRSetup.do?method=editQAEvaluation&strOpt=Edit&COMRSRId="+comrsrID;
		}else if(chooseAction == '102844'){ // Engg Evaluation
			document.frmIncidentInfo.action = "/gmEnggEvaluation.do?method=editEnggEvaluation&strOpt=Edit&incidentId="+comrsrID;
		}else if(chooseAction == '4000325'){ // Modify COMSRS Report
			document.frmIncidentInfo.action = "/gmCOMRSRReportAction.do?method=listCOMRSRDetails&accessFl=true&strOpt=Report&comRSR="+comrsrID;
		}else if(chooseAction == '102846'){ // Upload Documnets
			document.frmIncidentInfo.action = "/gmUploadAction.do?strOpt=Fetch&refType=102790&logType="+logType+"&strRefID="+comrsrID;
		}else if(chooseAction == '102845'){ // Closure COMORSR
			document.frmIncidentInfo.action = '/gmCOMRSRProcess.do?method=closeCOMRSR&strOpt=Fetch&closeCOMRSRID='+ comrsrID;
		}
		document.frmIncidentInfo.partNum.disabled = false;
		document.frmIncidentInfo.submit();
	}
}

//To limit the no. of characters entering into the text area
function fnLenValidation(obj,name,len){ 
	if(obj.value.length > len){
		var arrmsg = [len,name];
		Error_Details(Error_Details_Trans(message_prodmgmnt[235],arrmsg));
	}
}

