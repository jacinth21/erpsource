
//When enter comments this function will call.
function fnUploadSubmit(){
	fnStartProgress('Y');
	document.frmUploadDetail.action = '/gmUploadAction.do?strOpt=Save';  
	document.frmUploadDetail.submit();
}

//When click load this function will be called and get data and display in the report 
function fnUploadLoad(){
	var strRefId = document.frmUploadDetail.strRefID.value;
	if(TRIM(strRefId) == '')
		Error_Details(message_prodmgmnt[179]);
    if (ErrorCount > 0){
 		Error_Show();
 	    Error_Clear();
 		return false;
 	}
    fnStartProgress('Y');
	document.frmUploadDetail.action = '/gmUploadAction.do?strOpt=Fetch';
	document.frmUploadDetail.submit();	 
}

function fnUploadOnPageLoad(){	  
	if (objGridData != ''){
		gridObj = initGridData('grpData',objGridData);
		gridObj.enableTooltips("false,true,true,true,true");
	}
	if(document.getElementById("Submit") != null && SwitchUserFl != 'Y' && accessFl=='Y')
		document.getElementById("Submit").disabled=false;
}

//When click confirm message Y , it will be overwrite the file in that folder. 
function fnUpload(){
	fnStartProgress('Y');
	 var strRefId = document.frmUploadDetail.strRefID.value;
	 var strRefType = document.frmUploadDetail.refType.value;
	 document.frmUploadDetail.action = '/gmUploadAction.do?strOpt=Upload';
	 document.frmUploadDetail.hDisplayNm.value = 'Back to Upload Screen';
	 document.frmUploadDetail.hRedirectURL.value = '/gmUploadAction.do?strOpt=Fetch&strRefID='+strRefId+"&refType="+strRefType;
	 document.frmUploadDetail.submit();	 
}

//When click upload button this function will be called and check file having DB. If it is there give flag value Y.
function fnValidateUpload(){
	var uploadFile = document.frmUploadDetail.file.value;

	var strRefId = document.frmUploadDetail.strRefID.value;
	var strRefType = document.frmUploadDetail.refType.value;
	if(uploadFile!=''){
		fnStartProgress('Y');
		document.getElementById("Submit").disabled = true;
		dhtmlxAjax.get('/gmUploadAction.do?fileName='+encodeURIComponent(uploadFile)+'&refType='+strRefType+'&strRefID='+strRefId+'&strOpt=Validate&ramdomId='+Math.random(),fnValideFile);
	}else{ 
	      Error_Details(message_prodmgmnt[180]);
	     if (ErrorCount > 0){
	  		Error_Show();
	  	    Error_Clear();
	  		return false;
	  	}
	}
}
//Once ajax called following function will be called.
function fnValideFile(loader){
	var confirmMsg = '';  //initialize to avoid undefined error.
	var response = loader.xmlDoc.responseText;
	if(response == 'Y')	confirmMsg = fnUploadConfirmMessage();
	else	fnUpload();
	
	if(confirmMsg){
		fnUpload();
	}else {
		fnStopProgress();
	}
	document.getElementById("Submit").disabled = false;
}

function initGridData(divRef,gridData)
{
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);
	gObj.enableTooltips("true,true,true,true,true");
	gObj.init();	
	gObj.loadXMLString(gridData);	
	return gObj;	
}

// When click icon in the report , it will delete the row. 
function fnDelete(strRefId){
	var confirmMsg = false;
	if(SwitchUserFl == 'Y'){
		return false;
	}
	 confirmMsg = confirm(message_prodmgmnt[181]);
	 document.frmUploadDetail.action = '/gmUploadAction.do?strOpt=Delete&strID='+strRefId;
	 if(confirmMsg){
		 document.frmUploadDetail.submit();
	 }
}

//It will ask message when upload the file if we have file in the folder.
function fnUploadConfirmMessage(){
	confirmMsg = confirm(message_prodmgmnt[182]);
	if(confirmMsg)	return true;
	else 			return false;
}

// To Download when click icon in the report.
function fnDownloadFile(id, refId){
	document.frmUploadDetail.action = '/gmUploadAction.do?strOpt=Download&strID='+id+"&strRefID="+refId;
	document.frmUploadDetail.submit();	 
}

function fnShowFilters(val){
	var obj = eval("document.all."+val);
	var obj1 = eval("document.all."+val+"img");
	
	if (obj.style.display == 'none'){
		obj.style.display = 'block';
		if(obj1){
			obj1.src = '/images/minus.gif';
		}
	}
	else{
		obj.style.display = 'none';	
		if(obj1){
			obj1.src = '/images/plus.gif';
		}
	}
}

function fnUploadClose(){ // To close the Summary screen Popup
	window.close();
}