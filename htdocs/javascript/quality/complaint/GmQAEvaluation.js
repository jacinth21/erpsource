// To show/hide the details in a block
function fnQAShowFilters(val)
{ 
	var obj = eval("document.all."+val);
	var obj1 = eval("document.all."+val+"img");
	if (obj.style.display == 'none')
	{
		obj.style.display = 'block';
		if(obj1){
			obj1.src = '/images/minus.gif';
		}
	}
	else
	{
		obj.style.display = 'none';	
		if(obj1){
			obj1.src = '/images/plus.gif';
		}
	}
}

// Click on submit button
function fnQASubmit(){ 
	
	var COMRSRId = document.frmQAEvaluation.COMRSRId.value;
	var strOpt = document.frmQAEvaluation.strOpt.value;
	if(COMRSRId == ""){
		Error_Details(message_prodmgmnt[236]);
	}
	
	if(strOpt == "load" && COMRSRId != ''){
		Error_Details(message_prodmgmnt[77]);
	}
	
	if(strOpt != "load"){
		if(COMRSRId != hCOMRSRId)
			Error_Details(message_prodmgmnt[77]);
	}
    
	CommonDateValidation(document.frmQAEvaluation.dtRevComp,format,Error_Details_Trans(message_prodmgmnt[410],format));
	CommonDateValidation(document.frmQAEvaluation.dtPartRec,format,Error_Details_Trans(message_prodmgmnt[411],format));
	CommonDateValidation(document.frmQAEvaluation.dtEvalSend,format,Error_Details_Trans(message_prodmgmnt[412],format));
	CommonDateValidation(document.frmQAEvaluation.dtCompleted,format,Error_Details_Trans(message_prodmgmnt[413],format));
	
	fnLenValidation(document.frmQAEvaluation.DHRNCMRRev,message_prodmgmnt[415],"4000");
	fnLenValidation(document.frmQAEvaluation.CAPASCARRev,message_prodmgmnt[416],"4000");
	fnLenValidation(document.frmQAEvaluation.compRev,message_prodmgmnt[417],"4000");
	fnLenValidation(document.frmQAEvaluation.NCMRSCARId,message_prodmgmnt[418],"255");
	fnLenValidation(document.frmQAEvaluation.CAPAId,message_prodmgmnt[419],"255");
	
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	document.frmQAEvaluation.Btn_Submit.disabled = true;
	
	confirmMsg =  fnConfirmMessage(); // to call the function to show confirm message
	if(confirmMsg){	// Press "OK" to submit the form, else return false
		document.frmQAEvaluation.action = "/gmCOMRSRSetup.do?method=addQAEvaluation";
		document.frmQAEvaluation.strOpt.value = "save";
		fnStartProgress('Y');
		document.frmQAEvaluation.submit();
	}else{
		fnStopProgress();
		document.frmQAEvaluation.Btn_Submit.disabled = false;
		return false;
	}		
		
}

// Click on load button
function fnLoadQAEval(){ 
	var COMRSRId = document.frmQAEvaluation.COMRSRId.value;
	
	if(COMRSRId == ""){
		Error_Details(message_prodmgmnt[236]);
	}
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}else{
		document.frmQAEvaluation.Btn_Load.disabled = true;
		fnStartProgress();
		document.frmQAEvaluation.action = "/gmCOMRSRSetup.do?method=editQAEvaluation&strRefID"+COMRSRId;
		document.frmQAEvaluation.strOpt.value = "Edit"; 
		fnStartProgress('Y');
		document.frmQAEvaluation.submit();
	}
}

// To show the confirmation message when click on Submit/Reset button
function fnConfirmMessage(){
	confirmMsg = confirm(message_prodmgmnt[243]);
	if(confirmMsg)	
		return true;
	else 			
		return false;
}

// To reset the form
function fnQAReset(){
	var COMRSRId = document.frmQAEvaluation.COMRSRId.value;
	var strOpt = document.frmQAEvaluation.strOpt.value;
	if(strOpt == "Edit"){ // If strOpt=Edit, it will load the data from DB corresponding to COM/RSR Id
		confirmMsg =  fnConfirmMessage();
		if(confirmMsg){
				var hCOMRSRId = document.frmQAEvaluation.hCOMRSRId.value;
				if(COMRSRId != hCOMRSRId){
					document.frmQAEvaluation.COMRSRId.value = hCOMRSRId;
				}
				if (hCOMRSRId != ''){
					fnLoadQAEval();
				}else{
					document.frmQAEvaluation.action = "/gmCOMRSRSetup.do?method=addQAEvaluation&strOpt=load";
					document.frmQAEvaluation.submit();
				}
			
		}
	}else if(strOpt == "load"){ // If strOpt=load, it will remove all data from form
		confirmMsg =  fnConfirmMessage();
		if(confirmMsg){
			document.frmQAEvaluation.action = "/gmCOMRSRSetup.do?method=addQAEvaluation&strOpt=load";
			document.frmQAEvaluation.submit();
		}
	}
}

// To change the values inside the field to uppercase
function fnUpperCase(obj){
	obj.value = obj.value.toUpperCase();
}

// Click on GO button
function fnQAGo() {
	var frm = document.frmQAEvaluation;
	var COMRSRid = frm.COMRSRId.value;
	var chooseAction = frm.chooseAction.value;
	var logType;
	if(strRefType == '102790'){
		logType = 4000319;
	}else if(strRefType == '102791'){
		logType = 4000320;
	}
	
	if (COMRSRid == '') {
		Error_Details(message_prodmgmnt[244]);
	}
	if ((COMRSRid != '') && (chooseAction == '0')) {
		Error_Details(message_prodmgmnt[245]);
	}
	
	if(COMRSRid != '' && chooseAction != '0'){
		if(chooseAction == '102842'){ // Navigate to COM/RSR Incident Information screen
			frm.action = "/gmCOMRSRSetupAction.do?method=editIncidentInfo&strOpt=Edit&comRsrID="+COMRSRid;
		}
		if(chooseAction == '102844'){ // Navigate to Engineering Evaluations screen
			frm.action = "/gmEnggEvaluation.do?method=editEnggEvaluation&strOpt=Edit&incidentId="+COMRSRid;
		}
		if(chooseAction == '102845'){ // Navigate to Close COM/RSR Screen
			frm.action = "/gmCOMRSRProcess.do?method=closeCOMRSR&strOpt=Fetch&closeCOMRSRID="+COMRSRid;
		}
		if(chooseAction == '102846'){ // Navigate to Upload screen
			frm.action = "/gmUploadAction.do?strOpt=Fetch&refType=102790&logType="+logType+"&strRefID="+COMRSRid;
		}
		if(chooseAction == '4000325'){ // Navigate to Modify/Report on COM/RSR screen
			frm.action = "/gmCOMRSRReportAction.do?method=listCOMRSRDetails&accessFl=true&strOpt=Report&comRSR="+COMRSRid;
		}
		
	}
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}else{
		fnStartProgress('Y');
		frm.submit();
	}
}

function fnQAPrint(){
	var comrsrID = document.frmQAEvaluation.COMRSRId.value;	
	if(comrsrID == ''){
		Error_Details(message_prodmgmnt[246]);
		Error_Show();
		Error_Clear();
		return false;
	}else{
		document.frmQAEvaluation.action = "/gmCOMRSRSetupAction.do?method=printCOMRSR&comRsrID="+comrsrID;
		document.frmQAEvaluation.submit();
	}
}

//To limit the no. of characters entering into the text area
function fnLenValidation(obj,name,len){ 

	if(obj.value.length > len){
		var array = [len,name];
		Error_Details(Error_Details_Trans(message_prodmgmnt[414],array));
	}
}