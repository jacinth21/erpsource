var gridObj="";
var checkedValue = "";
function initGridData(divRef,gridData){
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);
	gObj.enableTooltips("true,true,true,true,true,true,true,true,,true,true");
	gObj.attachHeader('#rspan,#text_filter,#select_filter,#select_filter,#text_filter,#select_filter,#numeric_filter,#select_filter,#select_filter,#select_filter,'+message_prodmgmnt[433]+',#master_checkbox,#text_filter');
	gObj.init();		
	gObj.loadXMLString(gridData);
	return gObj;	
}
function fnOnPageLoad() {
	if (objGridData != '') {
		gridObj = initGridData('pendcomrsrreport',objGridData);
	}
}

function fnDownloadXLS(){
        gridObj.setColumnHidden(11,true);
		gridObj.toExcel('/phpapp/excel/generate.php');
        gridObj.setColumnHidden(11,false);
}

function fnOpenLog(id,type){
	windowOpener("/GmCommonLogServlet?hType="+type+"&hID="+id,"ConsignLog","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=300");
}
function fnOpenRADetails(val)
{
	windowOpener('/GmPrintCreditServlet?hAction=PRINT&hRAID='+val,"Returns","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=500");
}
function fnLoadIncident(raID,repid,partnum){
	document.frmCOMRSRReport.action = '/gmCOMRSRSetupAction.do?method=addIncidentInfo&strOpt=RALoad&raID='+raID+"&partNum="+encodeURIComponent(partnum)+"&complaintname="+repid+"&complaint=102812&issueType=102809";
	document.frmCOMRSRReport.submit();
}
function fnIgnore(){
    var raString = '';
    var partIdString = '';
    var linkIdString = '';
    var ignore_col_id = gridObj.getColIndexById("Ignore");
    var ra_id = gridObj.getColIndexById("ra");
    var part_id = gridObj.getColIndexById("partid");
    var link_id = gridObj.getColIndexById("linkid");
    var gridrows = gridObj.getAllRowIds();
	var rowsarr = gridrows.toString().split(",");
    for ( var rowid = 0; rowid < rowsarr.length; rowid++) {
        var rowcurrentid = rowsarr[rowid];
        checkedValue = gridObj.cellById(rowcurrentid, ignore_col_id).getValue();
        if(checkedValue == "1"){
            raString += gridObj.cellById(rowcurrentid, ra_id).getValue() + '~' + gridObj.cellById(rowcurrentid, part_id).getValue() + ',';
            partIdString += gridObj.cellById(rowcurrentid, part_id).getValue() + ',';
            linkIdString += gridObj.cellById(rowcurrentid, link_id).getValue() + ',';
        }
    }
    if(raString == ''){
        Error_Details("Please select at least one checkbox");
		Error_Show();
		Error_Clear();
		return false;
    }
    raString = raString.substr(0,raString.length-1);
    partIdString = partIdString.substr(0,partIdString.length-1);
    linkIdString = linkIdString.substr(0,linkIdString.length-1);
	if(raString != ''){
        document.frmCOMRSRReport.action = '/GmCommonCancelServlet?hTxnName='+raString+'&hCancelType=IGRCMP&hTxnId='+linkIdString+'&hAction=Load';
        document.frmCOMRSRReport.hDisplayNm.value = 'Pending COM/RSR Dashboard';
        document.frmCOMRSRReport.hRedirectURL.value = '/gmCOMRSRReportAction.do?method=pendingCOMRSRDash&strOpt=report';
        document.frmCOMRSRReport.submit();
	}	
}

