
function fnReset(){
	var refid = document.frmCOMRSRProcess.refID.value;
	var refType = document.frmCOMRSRProcess.refType.value;
	confirmMsg = fnConfirmMessage();
	if(confirmMsg){
		document.frmCOMRSRProcess.toEmail.value = "";
		document.frmCOMRSRProcess.action = '/gmCOMRSRProcess.do?method=closureEmail&strOpt=Load&refID='+refid+"&refType="+refType;
		document.frmCOMRSRProcess.submit();		
	}
}

function fnPreview(){
	var transID = document.frmCOMRSRProcess.refID.value;	
	var refType = document.frmCOMRSRProcess.refType.value;	
	var complaintname = document.frmCOMRSRProcess.issueBy.value;
	var toEmail = document.frmCOMRSRProcess.toEmail.value;
	var emailContent = document.frmCOMRSRProcess.emailCommnets.value;
	var contentlength = emailContent.length;
	if(contentlength > 4000){
		Error_Details(message_prodmgmnt[177]);
		Error_Show();
		Error_Clear();
		return false;
	}else{
		confirmMsg = fnConfirmMessage();
		if(confirmMsg){
			document.frmCOMRSRProcess.action = '/gmCOMRSRProcess.do?method=closureEmail&strOpt=preview&refID='+transID+"&refType="+refType + "&issueBy="+complaintname + "&toEmail="+toEmail;
		    document.frmCOMRSRProcess.submit();
		}
	}	
}

function fnCancel(){
	window.close();
}

function fnSend(){
	var transID = document.frmCOMRSRProcess.refID.value;	
	var refType = document.frmCOMRSRProcess.refType.value;	
	confirmMsg = fnConfirmMessage();
	if(confirmMsg) {
		document.frmCOMRSRProcess.action = '/gmCOMRSRProcess.do?method=closureEmail&strOpt=email&refID='+transID+"&refType="+refType;		
		document.frmCOMRSRProcess.submit();	
	}
}

function fnConfirmMessage(){
	confirmMsg = confirm(message_prodmgmnt[178]);
	if(confirmMsg)	return true;
	else 			return false;
}

function fnClose(){
	confirmMsg = fnConfirmMessage();
	if(confirmMsg) {
		window.close();
	}
	else{
		return false;
	}
}
