
function fnOnPageLoad(){	  
	if (objGridData != ''){
		gridObj = initGridData('grpData',objGridData);
		gridObj.enableTooltips("false,true,true,true,true");
	}
}

function initGridData(divRef,gridData)
{
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);
	gObj.enableTooltips("true,true,true,true,true");
	gObj.init();	
	gObj.loadXMLString(gridData);	
	return gObj;	
}

// To download the file
function fnDownloadFile(id, refId){
	document.frmCOMRSRProcess.action = '/gmUploadAction.do?strOpt=Download&strID='+id+"&strRefID="+refId;
	document.frmCOMRSRProcess.submit();
}

function fnClose(){ // To close the Summary screen Popup
	window.close();
}