function fnValidation(obj,name,len){
	if(obj.length > 4000){
		var arrmsg = [len,name];
		Error_Details(Error_Details_Trans(message_prodmgmnt[183],arrmsg));
	}
}

//When enter comments this function will call.
function fnonSubmit(){
	var Com_Rsr_Id = document.frmCOMRSRProcess.closeCOMRSRID.value;
	var strOpt = document.frmCOMRSRProcess.strOpt.value;
	if(strOpt == "load" && Com_Rsr_Id != ''){
		Error_Details(message_prodmgmnt[184]);
	}
	if(strOpt != "load"){
		if(Com_Rsr_Id != hCOMRSRID)
			Error_Details(message_prodmgmnt[185]);
	}
	if(Com_Rsr_Id  != ''){
		var prevCom       = document.frmCOMRSRProcess.prevComments.value;
		var closureCom    = document.frmCOMRSRProcess.closeCmts.value;
		var dtComplaint = document.frmCOMRSRProcess.closedDt.value;
		var fromCurrDiff = dateDiff(dtComplaint, currentDate, format);
		if(dtComplaint != ''){
			if(fromCurrDiff < 0){
				Error_Details(message_prodmgmnt[186]);
			}	
		}
		CommonDateValidation(document.frmCOMRSRProcess.closedDt,format,Error_Details_Trans(message_prodmgmnt[187],format));
		fnValidation(prevCom,message_prodmgmnt[400],'4000');
		fnValidation(closureCom,message_prodmgmnt[401],'4000');
		document.frmCOMRSRProcess.action = '/gmCOMRSRProcess.do?method=closeCOMRSR&strOpt=save';
	}else{
		Error_Details(message_prodmgmnt[189]);	
	}
	if (ErrorCount > 0){
		Error_Show();
	    Error_Clear();
		return false;
	}
	confirmMsg =  fnonConfirmMessage();
	if(confirmMsg){
		document.frmCOMRSRProcess.strOpt.value = "save";
		document.frmCOMRSRProcess.action = '/gmCOMRSRProcess.do?method=closeCOMRSR'; 
		fnStartProgress('Y');
		document.frmCOMRSRProcess.submit();
		return false;
	}
		else	{
			fnStopProgress();
			return false;
		}
	
	
}

function fnonConfirmMessage(){
	confirmMsg = confirm(message_prodmgmnt[190]);
	if(confirmMsg)	return true;
	else 			return false;
} 

//When click load this function will be called and get data and display in the report 
function fnLoadCOMRSR(){
	
	var Com_Rsr_Id = document.frmCOMRSRProcess.closeCOMRSRID.value;
	
	if(Com_Rsr_Id  != ''){
		
		document.frmCOMRSRProcess.action = '/gmCOMRSRProcess.do?method=closeCOMRSR&strOpt=Fetch'; 
	}else{
		  Error_Details(message_prodmgmnt[191]);	
	}
	
	if (ErrorCount > 0){
		Error_Show();
	    Error_Clear();
		return false;
	}
	document.frmCOMRSRProcess.Btn_Load.disabled=true;
	fnStartProgress('Y');
	document.frmCOMRSRProcess.submit();
}
function fnShowFilters(val){
	var obj = eval("document.all."+val);
	var obj1 = eval("document.all."+val+"img");
	
	if (obj.style.display == 'none'){
		obj.style.display = 'block';
		if(obj1){
			obj1.src = '/images/minus.gif';
		}
	}
	else{
		obj.style.display = 'none';	
		if(obj1){
			obj1.src = '/images/plus.gif';
		}
	}
}

function fnClose(){ // To close the Summary screen Popup
	window.close();
}
function fnonReset(){
	var strOpt   = document.frmCOMRSRProcess.strOpt.value;
	var COMRSRID = document.frmCOMRSRProcess.closeCOMRSRID.value;
	
	if(strOpt == "Fetch"){
		
		confirmMsg =  fnonConfirmMessage();
		if(confirmMsg){
				 if(COMRSRID !=  hCOMRSRID){
					document.frmCOMRSRProcess.closeCOMRSRID.value = hCOMRSRID;
				} 
			
			if (hCOMRSRID != ''){
				fnLoadCOMRSR();
			}else{
				document.frmCOMRSRProcess.action ="/gmCOMRSRProcess.do?method=closeCOMRSR&strOpt=load";
				document.frmCOMRSRProcess.submit();
			}
		}
	} 
	else if(strOpt == "load"){
		
		confirmMsg =  fnonConfirmMessage();
		if(confirmMsg){
			
			document.frmCOMRSRProcess.action = "/gmCOMRSRProcess.do?method=closeCOMRSR&strOpt=load";
			document.frmCOMRSRProcess.submit();
			}
	}
}
function fnEmail(){
	var comrsrID = document.frmCOMRSRProcess.closeCOMRSRID.value;
	var refType = "";
	var strArray = comrsrID.split('-');	
	if(strArray[0] == 'COM'){
		refType = "102790";
	}else if(strArray[0] == 'RSR'){
		refType = "102791";
	}
	windowOpener('/gmCOMRSRProcess.do?method=closureEmail&strOpt=Load&refID='+comrsrID+"&refType="+refType,"ClosureEmail","resizable=yes,scrollbars=yes,top=150,left=200,width=920,height=550");
}
function fnGo(){
	var ChooseActionId = document.frmCOMRSRProcess.chooseAction.value;
	var COMRSRID = document.frmCOMRSRProcess.closeCOMRSRID.value;
	var logType;
	if(issue == '102809'){
		logType = 4000319;
	}else if(issue == '102810'){
		logType = 4000320;
	}
	if (COMRSRID != '' && ChooseActionId != 0) {
		if (ChooseActionId == '102842') { // Navigate to Incident Information screen
			document.frmCOMRSRProcess.action = '/gmCOMRSRSetupAction.do?method=editIncidentInfo&strOpt=Edit&comRsrID='+ COMRSRID;
		}
		if (ChooseActionId == '102844') { // Navigate to Engineering Evaluation screen
			document.frmCOMRSRProcess.action = '/gmEnggEvaluation.do?method=editEnggEvaluation&strOpt=Edit&incidentId='+ COMRSRID;
		}
		if (ChooseActionId == '4000325') { // Navigate to Modify Report screen
			document.frmCOMRSRProcess.action = '/gmCOMRSRReportAction.do?method=listCOMRSRDetails&accessFl=true&strOpt=Report&comRSR='+ COMRSRID;
		}
		if (ChooseActionId == '102848') { // Navigate to Reopen screen
			if(statusId == '102841'){
				if(issue =='102809'){
					document.frmCOMRSRProcess.action = "/GmCommonCancelServlet?hTxnName="+COMRSRID+"&hCancelType=ROPCOM&hTxnId="+COMRSRID+"&hAction=Load";
					document.frmCOMRSRProcess.hDisplayNm.value = 'Close COM/RSR';
					document.frmCOMRSRProcess.hRedirectURL.value = '/gmCOMRSRProcess.do?method=closeCOMRSR&strOpt=Fetch&closeCOMRSRID='+ COMRSRID;
				}else{
					document.frmCOMRSRProcess.action = "/GmCommonCancelServlet?hTxnName="+COMRSRID+"&hCancelType=ROPCOM&hTxnId="+COMRSRID+"&hAction=Load";
					document.frmCOMRSRProcess.hDisplayNm.value = 'Close COM/RSR';
					document.frmCOMRSRProcess.hRedirectURL.value = '/gmCOMRSRProcess.do?method=closeCOMRSR&strOpt=Fetch&closeCOMRSRID='+ COMRSRID;
					}
				}
			else{
				Error_Details(message_prodmgmnt[192]);
			}
		}
		if (ChooseActionId == '102843') { // Navigate to QA Evaluation screen
			document.frmCOMRSRProcess.action = '/gmCOMRSRSetup.do?method=editQAEvaluation&strOpt=Edit&COMRSRId='+ COMRSRID;
		}
		if(ChooseActionId == '102846'){ // Navigate to Upload screen
			document.frmCOMRSRProcess.action = "/gmUploadAction.do?strOpt=Fetch&refType=102790&logType="+logType+"&strRefID="+COMRSRID;
		}
	}
	else if (COMRSRID != '' && ChooseActionId == 0) {
		Error_Details(message_prodmgmnt[193]);
	}
	else if (COMRSRID == '' && ChooseActionId != 0) {
		Error_Details(message_prodmgmnt[194]);
	}
	else if(COMRSRID == '' && ChooseActionId == 0){	
		Error_Details(message_prodmgmnt[195]);
		Error_Details(message_prodmgmnt[196]);
	}	
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	fnStartProgress('Y');
	document.frmCOMRSRProcess.submit();
}