
function fnConfirmMessage(){
	confirmMsg = confirm(message_prodmgmnt[210]);
	if(confirmMsg)	return true;
	else 			return false;
} 
function fnShowFilters(val)
{
	var obj = eval("document.all."+val);
	var obj1 = eval("document.all."+val+"img");
	
	if (obj.style.display == 'none')
	{
		obj.style.display = 'block';
		if(obj1){
			obj1.src = '/images/minus.gif';
		}
	}
	else
	{
		obj.style.display = 'none';	
		if(obj1){
			obj1.src = '/images/plus.gif';
		}
	}
}
function fnLenValidation(obj,name,len){
	if(obj.length > 4000)
		Error_Details(message_prodmgmnt[211]);
}
function fnonSubmit(){
	
	var Com_Rsr_Id = document.frmEnggEvaluation.incidentId.value;
	var strOpt = document.frmEnggEvaluation.strOpt.value;
	if(strOpt == "load" && Com_Rsr_Id != ''){
		Error_Details(message_prodmgmnt[212]);
	}
	
	if(strOpt != "load"){
		if(Com_Rsr_Id != hCOMRSRID)
			Error_Details(message_prodmgmnt[213]);
	}
	if(Com_Rsr_Id  != ''){
		CommonDateValidation(document.frmEnggEvaluation.evalRecDt,format,Error_Details_Trans(message_prodmgmnt[214],format));
		fnLenValidation(document.frmEnggEvaluation.evalResult.value,message_prodmgmnt[408],'4000');
		fnLenValidation(document.frmEnggEvaluation.capaComments.value,message_prodmgmnt[409],'4000');
		document.frmEnggEvaluation.action = '/gmEnggEvaluation.do?method=addEnggEvaluation&strOpt=Save';
	}
	else{
		Error_Details(message_prodmgmnt[215]);	
	}
	if (ErrorCount > 0){
		Error_Show();
	    Error_Clear();
		return false;
	}
	confirmMsg =  fnConfirmMessage();// to call the function to show confirm message
	if(confirmMsg){
		document.frmEnggEvaluation.action = "/gmEnggEvaluation.do?method=addEnggEvaluation";
		document.frmEnggEvaluation.strOpt.value = "Save";
		fnStartProgress();
		document.frmEnggEvaluation.submit();
	}
	else{
		fnStopProgress();
		return false;
	}
}

function fnonLoad(){
	var Com_Rsr_Id = document.frmEnggEvaluation.incidentId.value;
	if(Com_Rsr_Id != ''){
		document.frmEnggEvaluation.action = '/gmEnggEvaluation.do?method=editEnggEvaluation';
		document.frmEnggEvaluation.strOpt.value = "Edit"; 
		document.frmEnggEvaluation.submit();	
	}
	else{
		Error_Details(message_prodmgmnt[216]);
	}
	if (ErrorCount > 0){
		Error_Show();
	    Error_Clear();
		return false;
	}
	fnStartProgress('Y');
	document.frmEnggEvaluation.Btn_Load.disabled=true;	
	
}
function fnReset(){
	var strOpt   = document.frmEnggEvaluation.strOpt.value;
	var COMRSRID = document.frmEnggEvaluation.incidentId.value;
	if(strOpt == "Edit"){
		confirmMsg =  fnConfirmMessage();
		if(confirmMsg){
			if(COMRSRID !=  hCOMRSRID){
					document.frmEnggEvaluation.incidentId.value = hCOMRSRID;
				} 
			if (hCOMRSRID != ''){
				fnonLoad();
			}else{
				document.frmEnggEvaluation.action = "/gmCOMRSRSetup.do?method=addEnggEvaluation&strOpt=load";
				document.frmEnggEvaluation.submit();
			}
		}
	} 
	else if(strOpt == "load"){
		confirmMsg =  fnConfirmMessage();
		if(confirmMsg){
			document.frmEnggEvaluation.action = "/gmEnggEvaluation.do?method=addEnggEvaluation&strOpt=load";
			document.frmEnggEvaluation.submit();
			}
	}
}
function fnGo(){
	var ChooseActionId = document.frmEnggEvaluation.chooseAction.value;
	var COMRSRID = document.frmEnggEvaluation.incidentId.value;
	var logType;
	if(strRefType == '102790'){
		logType = 4000319;
	}else if(strRefType == '102791'){
		logType = 4000320;
	}
	if (COMRSRID != '' && ChooseActionId != 0) {
		if (ChooseActionId == '102842') { // Navigate to Incident Information screen
			document.frmEnggEvaluation.action = '/gmCOMRSRSetupAction.do?method=editIncidentInfo&strOpt=Edit&comRsrID='+ COMRSRID;
		}
		if (ChooseActionId == '4000325') { // Navigate to Modify Report screen
			document.frmEnggEvaluation.action = '/gmCOMRSRReportAction.do?method=listCOMRSRDetails&accessFl=true&strOpt=Report&comRSR='+ COMRSRID;
		}
		if (ChooseActionId == '102843') { // Navigate to QA Evaluation screen
			document.frmEnggEvaluation.action = '/gmCOMRSRSetup.do?method=editQAEvaluation&strOpt=Edit&COMRSRId='+ COMRSRID;
		}
		if(ChooseActionId == '102845'){ // Navigate to Close Complaint screen
			document.frmEnggEvaluation.action = '/gmCOMRSRProcess.do?method=closeCOMRSR&strOpt=Fetch&closeCOMRSRID='+ COMRSRID;
		}
		if(ChooseActionId == '102846'){ // Navigate to Upload screen
			document.frmEnggEvaluation.action = "/gmUploadAction.do?strOpt=Fetch&refType=102790&logType="+logType+"&strRefID="+COMRSRID;
			
		}
	}
	else if (COMRSRID != '' && ChooseActionId == 0) {
		Error_Details(message_prodmgmnt[217]);
	}
	else if (COMRSRID == '' && ChooseActionId != 0) {
		Error_Details(message_prodmgmnt[218]);
	}
	else if(COMRSRID == '' && ChooseActionId == 0){	
		Error_Details(message_prodmgmnt[219]);
		Error_Details(message_prodmgmnt[220]);
	}	
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	fnStartProgress('Y');
	document.frmEnggEvaluation.submit();
}
