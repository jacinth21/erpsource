/**
 * 
 */

var part_rowId = "";
var qty_rowId = "";
var critical_rowId = "";
var invalidPart_rowId = "";
var invalidCriti_rowId = "";
var gridObj;
var strHeaderJson;
var strDtlsJson;

// this function will fire on page load time. Used to fetch set upload bulk
// details
function fnOnPageLoad() {

	document.frmSetMappingUpload.uploadAction.value = '108741';

	if (document.frmSetMappingUpload.haction.value == 'Reload') {
		document.frmSetMappingUpload.haction.value = '';
		fnLoadSetUploadDtls();
	}
}

// this function used to validate the set id using existing AJAX call
function fnValidateSetId(setIdObj) {
	if (setIdObj != '') {
		var strType = document.frmSetMappingUpload.strOpt.value;
		fnStartProgress('Y');
		
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/GmSetMapServlet?hAction=validateSet&setId='
				+ setIdObj.value + '&strType=SETMAP');
		var loader = dhtmlxAjax.getSync(ajaxUrl);
		fnValidateCallback(loader);
	}
}

// this function used to display/populate set details.
function fnValidateCallback(loader) {
	response = loader.xmlDoc.responseText;
	
	if (response == "") {
		document.frmSetMappingUpload.searchsetId.value = '';
		Error_Details("Set ID does not exist, please enter a valid Set ID");
	} else {
		document.frmSetMappingUpload.searchsetId.value = response;
		document.frmSetMappingUpload.setId.value = document.frmSetMappingUpload.setIdVal.value;
		fnLoadSetUploadDtls();
	}

	fnStopProgress();
	
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}

}

// this function will fire after enter the set id/name. To fetch set mapping
// bulk upload details (AJAX call)

function fnLoadSetUploadDtls() {
	var setIdVal = document.frmSetMappingUpload.setId.value;
	document.frmSetMappingUpload.setIdVal.value = setIdVal;

	// Once valid set id then only show fetch the upload details.
	if(setIdVal != ''){
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmSetMappingUpload.do?method=fetchSetMappingUpload&setId='
				+ encodeURIComponent(setIdVal) + '&ramdomId=' + Math.random());
		var loader = dhtmlxAjax.getSync(ajaxUrl);
		fnLoadCallBack(loader);
	}
	
}

// this function used to split the JSON string and populate grid values
function fnLoadCallBack(loader) {
	response = loader.xmlDoc.responseText;

	var setInitWidths = "750,100,130,0,0";
	var setColAlign = "left,right,left,right,left,left";
	var setColTypes = "ed,ed,coro,ro,ro,ro,ro";
	var setColSorting = "str,int,str,int,int,str,str";
	var setHeader = [ "Part #", "Qty", "Critical Flag", "Primary Key",
			"Status", "Invalid Part", "Invalid Critical" ];
	var setFilter = [ "#text_filter", "#numeric_filter", "#select_filter", ,
			"#text_filter", "#text_filter" ];
	var setColIds = "partNum,setQty,criticalFl,setMappingId,uploadAction,invalidPart,invalidCriticalFl";
	var enableTooltips = "true,true,true,true,true";

	var gridHeight = "";
	var footerStyles = [];
	var footerExportFL = true;
	pagination = "";
	format = ""
	var formatDecimal = "";
	var formatColumnIndex = "";

	// to display Set mapping information.
	document.getElementById("trLastUpd").style.display = "table-row";
	document.getElementById("trLastStatus").style.display = "table-row";
	document.getElementById("trImages").style.display = "table-row";
	document.getElementById("trDiv").style.display = "table-row";
	document.getElementById("trAction").style.display = "table-row";
	document.getElementById("trComments").style.display = "table-row";

	// split the functions to avoid multiple parameters passing in single
	// function
	/*gridObj = initGridObject('dataGridDiv');
	gridObj = fnCustomTypes(gridObj, setHeader, setColIds, setInitWidths,
			setColAlign, setColTypes, setColSorting, enableTooltips, setFilter,
			400, pagination);*/
	
	// split as array
	var b = response;
	var temp = new Array();
	temp = b.split('^^');
	strHeaderJson = temp[0];
	strDtlsJson = temp[1];
	fnPopulateHeaderDtls(strHeaderJson);

	//gridObj = loadDhtmlxGridByJsonData(gridObj, strDtlsJson);

gridObj = new dhtmlXGridObject('dataGridDiv');
		gridObj.setImagePath("/extweb/suite/5.2.0/codebase/imgs/");
		gridObj.setHeader("Part #,Qty,Critical Flag, Primary Key,Status, Invalid Part, Invalid Critical");
		gridObj.setInitWidths("750,100,130,0,0,0,0");
		gridObj.setColAlign("left,right,left,right,left,left,left");
		gridObj.setColTypes("ed,ed,coro,ro,ro,ro,ro");
		gridObj.setColSorting("str,int,str,int,int,str,str");
		gridObj.setColumnIds("partNum,setQty,criticalFl,setMappingId,uploadAction,invalidPart,invalidCriticalFl");
			
        gridObj.init();
		gridObj.enableAlterCss("even","uneven");
		gridObj.parse(strDtlsJson,"js");
	    gridObj.attachEvent("onKeyPress", onKeyPressed);
		gridObj.attachEvent("onEditCell",doOnCellEdit);
	    gridObj.enableMultiselect(true);
		gridObj.enableBlockSelection(true);
		
	gridObj.copyBlockToClipboard();
	gridObj.enableMultiline(false);
	// For Single click
	// gridObj.enableEditEvents(true,true,true);
	
	// For enable the tab order
	gridObj.enableEditTabOnly(true);

	// enabling and disabling buttons
	// fnEnableDisableBtn();

	gridObj.attachEvent("onKeyPress", onKeyPressed);
	gridObj.attachEvent("onEditCell", doOnCellEdit);
	gridObj.enableMultiselect(true);
	gridObj.enableBlockSelection(true);

	part_rowId = gridObj.getColIndexById("partNum");
	qty_rowId = gridObj.getColIndexById("setQty");
	critical_rowId = gridObj.getColIndexById("criticalFl");
	invalidPart_rowId = gridObj.getColIndexById("invalidPart");
	invalidCriti_rowId = gridObj.getColIndexById("invalidCriticalFl");

	var comboCritical = gridObj.getCombo(critical_rowId);
	comboCritical.clear();

	comboCritical.put("Y", "Yes");
	comboCritical.put("N", "No");
	comboCritical.save();

	if (strDtlsJson == "") {
		fnAddRows();
		fnAddRows();
		fnAddRows();
	} else {
		gridObj.forEachRow(function(rowId) {
			invalid_part = gridObj.cellById(rowId, invalidPart_rowId)
					.getValue();
			invalid_critical = gridObj.cellById(rowId, invalidCriti_rowId)
					.getValue();

			if (invalid_part == "Y")
				gridObj.setCellTextStyle(rowId, part_rowId,
						"color:red;border:1px solid red;");

			if (invalid_critical == "Y"){
				gridObj.setCellTextStyle(rowId, critical_rowId,
						"color:red;border:1px solid red;");
				gridObj.cellById(rowId, critical_rowId).setValue("Y");
			}
			
		});

	}

	// Hide column
	gridObj.setColumnHidden(3, true);
	gridObj.setColumnHidden(4, true);
	gridObj.setColumnHidden(5, true);
	gridObj.setColumnHidden(6, true);
}

// this function used to populate set mapping bulk - header details
function fnPopulateHeaderDtls(headerObj) {

	document.getElementById("lbl_last_updated_by").innerHTML = '';
	document.getElementById("lbl_last_updated_date").innerHTML = '';
	document.getElementById("lbl_last_updated_status").innerHTML = '';
	
	if (headerObj != '') {
		var setUploadobj = "";
		setUploadobj = JSON.parse(headerObj);

		//
		document.getElementById("lbl_last_updated_by").innerHTML = setUploadobj.setUploadUpdateBy;
		document.getElementById("lbl_last_updated_date").innerHTML = setUploadobj.setUploadUpdatedDate;
		document.getElementById("lbl_last_updated_status").innerHTML = setUploadobj.setUploadStatus;
	} 
}

// To add rows into grid.
function fnAddRows() {
	gridObj.filterBy(0, "");// unfilters the grid while adding rows
	gridObj._f_rowsBuffer = null; // clears the cache
	var newRowId = gridObj.getUID();
	gridObj.addRow(newRowId, '');
}


// This Function Used to set custom types conditions in Grid
function fnCustomTypes(gObj, setHeader, setColIds, setInitWidths, setColAlign,
		setColTypes, setColSorting, enableTooltips, setFilter, gridHeight,
		pagination) {

	if (setHeader != undefined && setHeader.length > 0) {
		gObj.setHeader(setHeader);
		var colCount = setHeader.length;
	}

	if (setInitWidths != "")
		gObj.setInitWidths(setInitWidths);

	if (setColAlign != "")
		gObj.setColAlign(setColAlign);

	if (setColTypes != "")
		gObj.setColTypes(setColTypes);

	if (setColSorting != "")
		gObj.setColSorting(setColSorting);

	if (enableTooltips != "")
		gObj.enableTooltips(enableTooltips);

	if (setFilter != "")
		gObj.attachHeader(setFilter);

	if (setColIds != "") {
		gObj.setColumnIds(setColIds);
	}

	if (gridHeight != "")
		gObj.enableAutoHeight(true, gridHeight, true);
	else
		gObj.enableAutoHeight(true, 400, true);

	if (pagination == "Y") {
		gObj.enablePaging(true, 100, 10, "pagingArea", true);
		gObj.setPagingSkin("bricks");
	}

	return gObj;
}


// this function used to copy the grid data
function onKeyPressed(code, ctrl) {
	if (code == 67 && ctrl) {
		if (!gridObj._selectionArea)
			return false;
		gridObj.setCSVDelimiter("\t");
		gridObj.copyBlockToClipboard();
		gridObj._HideSelection();
	}
	if (code == 86 && ctrl) {
		gridObj.setCSVDelimiter("\t")
		gridObj.pasteBlockFromClipboard();
		//gridObj._HideSelection();
	}
	gridObj.refreshFilters();
	return true;
}


// this function is used to perform action when a cell is editing
function doOnCellEdit(stage, rowId, cellInd, nValue, oValue) {
	// Highlight data when edit cell
	if (stage == 1 && (cellInd == part_rowId || cellInd == qty_rowId)) {
		var c = this.editor.obj;
		if (c != null)
			c.select();
	}
	
	//when Part& Flag updated then reset the color				  
	if (stage == 2 && nValue != oValue && (cellInd == part_rowId || cellInd == critical_rowId) && nValue != '') {
		gridObj.setCellTextStyle(rowId, cellInd,
		"color:none;border:0px");
	}

	return true;
}

// this function used to remove the empty rows
function fnRemoveEmptyRow() {
	gridObj.forEachRow(function(row_id) {
		part_val = gridObj.cellById(row_id, part_rowId).getValue();
		if (part_val == '') {
			gridObj.deleteRow(row_id);
		}
	});
}


// this function used to paste data from excel
function fnAddRowFromClipboard() {
	var cbData = gridObj.fromClipBoard();
	if (cbData == null) {
		Error_Details("Please copy valid data from excel");
	}
	fnRemoveEmptyRow();
	var rowData = cbData.split("\n");
	var rowcolumnData = '';
	var arrLen = (rowData.length) - 1;

	if (ErrorCount > 0) {
		fnStopProgress();
		Error_Show();
		Error_Clear();
		return;
	}
	
	fnStartProgress('Y');
	var newRowData = '';
	setTimeout(function() {
		gridObj.startFastOperations();
		gridObj.filterBy(0, "");// unfilters the grid
		gridObj._f_rowsBuffer = null; // clears the cache
		for (var i = 0; i < arrLen; i++) {

			newRowData = rowData[i];
			newRowData = newRowData.replace(/\n|\r/g, "");
			var rowcolumnData = '';
			var row_id = gridObj.getUID();
			gridObj.setCSVDelimiter("\t");
			gridObj.setDelimiter("\t");
			gridObj.addRow(row_id, newRowData);
			gridObj.setRowAttribute(row_id, "row_added", "Y");
			gridObj.setRowAttribute(row_id, "row_modified", "Y");
			gridObj.setDelimiter(",");
		}
		
		gridObj.stopFastOperations();
		fnStopProgress();
		gridObj.filterByAll();
	}, 100);
}

// this function used to validate required data and submit the form.
function fnSubmit() {
	fnValidateDropDn('uploadAction', ' Action');
	fnValidateDropDn('txt_LogReason', ' Comments');
	
	// validate the duplicate part #
	if (ErrorCount == 0)
		fnValidateAndCreateInputStr();
	// form the input string

	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return;
	} else if (confirm('Are you Sure, you want to Submit the data?')) {
		fnStartProgress('Y');
		
		document.frmSetMappingUpload.action = '/gmSetMappingUpload.do?method=saveSetMappingUpload';
		document.frmSetMappingUpload.submit();

		/*
		 * var loader =
		 * dhtmlxAjax.postSync('/gmSetMappingUpload.do?method=saveSetMappingUpload','&setId='+
		 * document.frmSetMappingUpload.setId.value +'&inputStr='+
		 * document.frmSetMappingUpload.inputStr.value +'&txt_LogReason='
		 * +document.frmSetMappingUpload.txt_LogReason.value +'&uploadAction='
		 * +document.frmSetMappingUpload.uploadAction.value
		 * +'&'+fnAppendCompanyInfo()+'&ramdomId=' + Math.random());
		 * fnSubmitCallBack(loader);
		 */
	}

}


// Function used for save call back function
function fnSubmitCallBack(loader) {

	fnStopProgress();
	var response = loader.xmlDoc.responseText;
	var partDuplicateErrFl = false;
	var criticalDuplicateErrFl = false;

	if (response.indexOf(part_val + "^^") != -1) {

		// split as array
		var b = response;
		var temp = new Array();
		temp = b.split('^^');

		strHeaderJson = temp[0];
		strDtlsJson = temp[1];
		fnPopulateHeaderDtls(strHeaderJson);

		if (strDtlsJson != "") {
			var rows = [], i = 0;
			var JSON_Array_obj = "";
			console.log('strDtlsJson ' + strDtlsJson);
			JSON_Array_obj = JSON.parse(strDtlsJson);

			$.each(JSON_Array_obj, function(index, jsonObject) {

				partNumberVal = jsonObject.partNum;
				partValidateFl = jsonObject.invalidPart;
				criticalValidateFl = jsonObject.invalidCriticalFl;

				gridObj.forEachRow(function(rowId) {
					// check row id set priority equal if both equal and also
					// setvalidate and countryvalidateflag value is N
					// highlight the cell throw validation
					part_val = gridObj.cellById(rowId, part_rowId).getValue();
					if (part_val == partNumberVal) {
						if (partValidateFl == 'Y') {
							gridObj.setCellTextStyle(rowId, part_rowId,
									"color:red;border:1px solid red;");
							partDuplicateErrFl = true;
						}
						if (criticalValidateFl == 'Y') {
							gridObj.setCellTextStyle(rowId, critical_rowId,
									"color:red;border:1px solid red;");
							criticalDuplicateErrFl = true;
						}

					}

				});//       
			});

			// if flag value true set validations
			if (partDuplicateErrFl) {
				Error_Details('The highlighted <b>Part # (s)</b> do not exist');
			}

			if (criticalDuplicateErrFl) {
				Error_Details('The highlighted <b>Critical Flag</b> duplicated');
			}

			if (ErrorCount > 0) {
				Error_Show();
				Error_Clear();
				return false;
			}

		}
	}

}

// this function used to validate duplicate entry (part) and set the input
// string values.
function fnValidateAndCreateInputStr() {

	var inputStr = "";
	var hPartInputStr = "";
	var dupPartids = '';
	var empty_qty = '';
	var error_qtyStr = '';
	var duplicateCritical = '';
	var uploadActionVal = document.frmSetMappingUpload.uploadAction.value;
	var cntDuplicateCriticalFl = 0;
	var partNumberBlankFl = false;

	gridObj.forEachRow(function(row_id) {
		//
		part_val = gridObj.cellById(row_id, part_rowId).getValue();
		qty_val = gridObj.cellById(row_id, qty_rowId).getValue();
		critical_val = gridObj.cellById(row_id, critical_rowId).getValue();
		//
		critical_flag_val = 'N';
		inset_flag_val = 'Y';
		critical_qty = '';
		//
		if (part_val != '') {

			if (hPartInputStr.indexOf(part_val + ",") == -1) {
				hPartInputStr += (part_val + ",");

				if (critical_val == "Y") {
					critical_flag_val = 'Y';
					critical_qty = 1;
					cntDuplicateCriticalFl++;
				}

				if (qty_val == 0) {
					inset_flag_val = 'N';
				}

				if (uploadActionVal == 108741) { // Add or Modify
					// Qty validation
					if (qty_val == '') {
						empty_qty = empty_qty + "<br>" + part_val;
						gridObj.setCellTextStyle(row_id, qty_rowId,
								"color:red;border:1px solid red;");
						gridObj.setRowColor(row_id, "ORANGE");
					} else {
						var validation = NumberValidation(qty_val, '', 0);

						if (validation == false || qty_val < 0) {
							error_qtyStr = error_qtyStr + "<br>" + part_val
									+ " - " + qty_val + "";
							gridObj.setCellTextStyle(row_id, qty_rowId,
									"color:red;border:1px solid red;");
						}
					}

					if (cntDuplicateCriticalFl > 1) {
						duplicateCritical = duplicateCritical + "<br>"
								+ part_val;
						gridObj.setCellTextStyle(row_id, critical_rowId,
								"color:red;border:1px solid red;");
					}
					
				}

				// duplicate part #

				// qty check
				// Part ^ Qty ^ Inset Fl ^ Critical Fl ^ Critical Qty ^ Critical
				// Type ^ Critical Tag^ Unit Type
				// addition type ^ fl ^ map id

				inputStr = inputStr + part_val + '^' + qty_val + '^'
						+ inset_flag_val + '^' + critical_flag_val + '^'
						+ critical_qty + '^^^|';
			} else {

				if (dupPartids.indexOf(part_val + ",") == -1) {
					dupPartids += (part_val + ",");

				}

				gridObj.setCellTextStyle(row_id, part_rowId,
						"color:red;border:1px solid red;");
			}
		} // end of part_val check
		else{
			
			if( qty_val != '' || critical_val != ''){
				gridObj.setCellTextStyle(row_id, part_rowId,
				"color:red;border:1px solid red;");
				gridObj.setRowColor(row_id, "ORANGE");
				partNumberBlankFl = true;
			}
		}
	});

	if (empty_qty != '') {
		Error_Details(Error_Details_Trans(
				"Quantity cannot be empty. Please valid quantity for the Part Number(s) -[0]", empty_qty));
	}

	// Error message for invalid Part Cost..
	if (error_qtyStr != '') {
		Error_Details(Error_Details_Trans(
				"Please enter valid and Positive numbers in Qty for Part# -[0]",
				error_qtyStr));
	}

	// Error message for Duplicate Part Entries..
	if (dupPartids != '' && TRIM(dupPartids).length > 1) {
		dupPartids = dupPartids.substr(0, (TRIM(dupPartids).length - 1));
		error_msg = Error_Details_Trans(
				"Duplicate Part Number(s) Found - [0]", dupPartids);
		Error_Details(error_msg);
	}

	if (duplicateCritical != '') {
		Error_Details(Error_Details_Trans(
				"Critical Flag cannot be Yes for following Part Number(s) -[0]",
				duplicateCritical));
	}

	if(partNumberBlankFl){
		Error_Details("Part Numbers cannot be blank");
	}
	
	if (inputStr == "") {
		Error_Details("Please provide the Part # details ");
	} else {
		document.frmSetMappingUpload.inputStr.value = inputStr;
	}

}


//Use the below function to remove the rows from the grid.
function fnRemoveSetUploadRow() {

	var gridrows = gridObj.getSelectedRowId();
	//check selected status is null or undefined or null 
	if (gridrows == '' || gridrows == undefined || gridrows == null) {
		Error_Details(message_prodmgmnt[37]);
		if (ErrorCount > 0) {
			Error_Show();
			Error_Clear();
			return false;
		}
	} else {

		var gridrowsarr = gridrows.toString().split(",");
		
		for (var rowid = 0; rowid < gridrowsarr.length; rowid++) {
			gridrowid = gridrowsarr[rowid];
			gridObj.deleteRow(gridrowid);
		}

	}

}