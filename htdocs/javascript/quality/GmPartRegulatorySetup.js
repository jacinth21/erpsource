//
var fdaSubSeqErrStr = '';
var FDASUBSeqErrStr = '';
var PMASupSeqErrStr ='';
var pmaSupSeqErrStr = '';
var fdaSubReqFiledStr = '';
var pmaSupReqFiledStr = '';
var fdaProdReqFieldStr = '';
var fdaProdCharStr = '';
var fdaSubCharStr = '';
var fdaNoStr = '';
var fdaValidateStr = '';
var pmaSupCharStr = '';
var fdaSubNumStr = '';
var pmaSupNumStr = '';
var appSubnoErrStr = '';
var NTFReportStr = '';
var USNTFReportStr = '';
var CETechFileNumberStr = '';

function fnSaveRegulatoryInfo(form){
	form.btn_Reg_Submit.disabled = true;
	
	fnValidateTxtFld('txt_Brand_Nm',lblBrandName);
	fnValidateDropDn('cbo_Us_Reg_Class',lblUsRegulatoryClass);
	fnValidateDropDn('cbo_Us_Reg_Pathway',lblUSRegulatoryPathWay);
	fnValidateDropDn('cbo_Eu_Reg_Pathway',lblEuRegPathWay);
	fnValidateTxtFld('comments',message[5550]);
	
	fdaSubSeqErrStr = '';
	FDASUBSeqErrStr = '';
	PMASupSeqErrStr = '';
	pmaSupSeqErrStr = '';
	fdaSubReqFiledStr = '';
	pmaSupReqFiledStr = '';
	fdaProdReqFieldStr = '';
	fdaProdCharStr = '';
	fdaSubCharStr = '';
	fdaNoStr = '';
	fdaValidateStr = '';
	pmaSupCharStr = '';
	fdaSubNumStr = '';
	pmaSupNumStr = '';
	appSubnoErrStr = '';
	NTFReportStr ='';
	USNTFReportStr = '';
	CETechFileNumberStr ='';
	fnValidateFdaSubNum(form);
	fnValidateFdaField(form);
	fnValidatePmaSupNum(form);
	fnValidateFdaProdCode(form);
	fnValidateApprovalDate(form);
	fnMandatoryApprovalDate(form);
	fnMandFDASubmNo(form);
	fnValidateTechFileRev(form);

	var strBrandNm = form.txt_Brand_Nm.value;
	var strUsRegClass = fnCorrectValDropdown(form.cbo_Us_Reg_Class.value);
	var strUsRegPathway = fnCorrectValDropdown(form.cbo_Us_Reg_Pathway.value);
	var strFdaSubNo1 = form.txt_Fda_sub_no1.value;
	var strFdaSubNo2 = form.txt_Fda_sub_no2.value;
	var strFdaSubNo3 = form.txt_Fda_sub_no3.value;
	var strFdaSubNo4 = form.txt_Fda_sub_no4.value;
	var strFdaSubNo5 = form.txt_Fda_sub_no5.value;
	var strFdaSubNo6 = form.txt_Fda_sub_no6.value;
	var strFdaSubNo7 = form.txt_Fda_sub_no7.value;
	var strFdaSubNo8 = form.txt_Fda_sub_no8.value;
	var strFdaSubNo9 = form.txt_Fda_sub_no9.value;
	var strFdaSubNo10 = form.txt_Fda_sub_no10.value;
	var strNtfReport = form.txt_Ntf_Reported.value;
	var strFdaListing = form.txt_Fda_Listing.value;
	var strPmaSupNo1 = form.txt_Pma_Sup_no1.value;
	var strPmaSupNo2 = form.txt_Pma_Sup_no2.value;
	var strPmaSupNo3 = form.txt_Pma_Sup_no3.value;
	var strPmaSupNo4 = form.txt_Pma_Sup_no4.value;
	var strPmaSupNo5 = form.txt_Pma_Sup_no5.value;
	var strPmaSupNo6 = form.txt_Pma_Sup_no6.value;
	var strPmaSupNo7 = form.txt_Pma_Sup_no7.value;
	var strPmaSupNo8 = form.txt_Pma_Sup_no8.value;
	var strPmaSupNo9 = form.txt_Pma_Sup_no9.value;
	var strPmaSupNo10 = form.txt_Pma_Sup_no10.value;
	var strFdaProdCode1 = form.txt_Fda_Prod_Code1.value;
	var strFdaProdCode2 = form.txt_Fda_Prod_Code2.value;
	var strFdaProdCode3 = form.txt_Fda_Prod_Code3.value;
	var strFdaProdCode4 = form.txt_Fda_Prod_Code4.value;
	var strFdaProdCode5 = form.txt_Fda_Prod_Code5.value;
	var strFdaProdCode6 = form.txt_Fda_Prod_Code6.value;
	var strFdaProdCode7 = form.txt_Fda_Prod_Code7.value;
	var strFdaProdCode8 = form.txt_Fda_Prod_Code8.value;
	var strFdaProdCode9 = form.txt_Fda_Prod_Code9.value;
	var strFdaProdCode10 = form.txt_Fda_Prod_Code10.value;
	var strEuRegPathway = fnCorrectValDropdown(form.cbo_Eu_Reg_Pathway.value);
	var strGmdnCode = form.txt_Gmdn_Code.value;
	var strCeTechFileNum = form.txt_Ce_Tech_File_Num.value;
	var strCeTechFileRev = form.txt_Ce_Tech_File_Rev.value;
	var strNotes = form.txt_Notes.value;
	var strApprovalDt = form.txt_Approval_Dt;
	
	var FDANumFlag1 = form.hFDASubmission1.value;
	var FDANumFlag2 = form.hFDASubmission2.value;
	var FDANumFlag3 = form.hFDASubmission3.value;
	var FDANumFlag4 = form.hFDASubmission4.value;
	var FDANumFlag5 = form.hFDASubmission5.value;
	var FDANumFlag6 = form.hFDASubmission6.value;
	var FDANumFlag7 = form.hFDASubmission7.value;
	var FDANumFlag8 = form.hFDASubmission8.value;
	var FDANumFlag9 = form.hFDASubmission9.value;
	var FDANumFlag10 = form.hFDASubmission10.value;
	
	var PMASupFlag1 = form.hPMASup1.value;
	var PMASupFlag2 = form.hPMASup2.value;
	var PMASupFlag3 = form.hPMASup3.value;
	var PMASupFlag4 = form.hPMASup4.value;
	var PMASupFlag5 = form.hPMASup5.value;
	var PMASupFlag6 = form.hPMASup6.value;
	var PMASupFlag7 = form.hPMASup7.value;
	var PMASupFlag8 = form.hPMASup8.value;
	var PMASupFlag9 = form.hPMASup9.value;
	var PMASupFlag10 = form.hPMASup10.value;
	var strExemptSubVal = '';
	var ApprCurrDiff= dateDiff(currdate, strApprovalDt.value, format);
	if(strApprovalDt.value != ""){ 
		CommonDateValidation(strApprovalDt, format, Error_Details_Trans(message[5763],format));
	}
	if(strApprovalDt.value != "" && ApprCurrDiff > 0 ){
		Error_Details(message[5764]);
	}
	
	//validate NTF during submit
	fnValidaeNTF(form, strNtfReport,lblNtfReported);
	
	//validate FDA Listing field during submit
	fnValidateFDAListing(strFdaListing,message[5766]);
	
	//Validating during submit for GMDNCode field
	if(strGmdnCode != ''){
		if(strGmdnCode.length <=3 || isNaN(strGmdnCode)){
			Error_Details(message[5767]);
			}
	}
	
	//Validating during submit for Tech File Number field
	fnValidateTechFileNumber(strCeTechFileNum,message[5768]);
	
	//validate FDASequence during submit
	var hBrandName = form.hBrandName.value;
	if(hBrandName != ''){
		//validate FDASub seq after submit
		if((FDANumFlag1 !='Y' && strFdaSubNo1 =='')   && (FDANumFlag2 !='Y' && strFdaSubNo2 !='')){
			fnValidateFDASeq('FDA Sub. no 1');
		}if((FDANumFlag2 !='Y' && strFdaSubNo2 =='')  && (FDANumFlag3 !='Y' && strFdaSubNo3 !='')){
			fnValidateFDASeq('FDA Sub. no 2');
		}if((FDANumFlag3 !='Y' && strFdaSubNo3 =='')  && (FDANumFlag4 !='Y' && strFdaSubNo4 !='')){
			fnValidateFDASeq('FDA Sub. no 3');
		}if((FDANumFlag4 !='Y' && strFdaSubNo4 =='')  && (FDANumFlag5 !='Y' && strFdaSubNo5 !='')){
			fnValidateFDASeq('FDA Sub. no 4');
		}if((FDANumFlag5 !='Y'&& strFdaSubNo5 =='')   && (FDANumFlag6 !='Y' && strFdaSubNo6 !='')){
			fnValidateFDASeq('FDA Sub. no 5');
		}if((FDANumFlag6 !='Y' && strFdaSubNo6 =='')  && (FDANumFlag7 !='Y' && strFdaSubNo7 !='')){
			fnValidateFDASeq('FDA Sub. no 6');
		}if((FDANumFlag7 !='Y' && strFdaSubNo7 =='')  && (FDANumFlag8 !='Y' && strFdaSubNo8 !='')){
			fnValidateFDASeq('FDA Sub. no 7');
		}if((FDANumFlag8 !='Y' && strFdaSubNo8 =='')  && (FDANumFlag9 !='Y' && strFdaSubNo9 !='')){
			fnValidateFDASeq('FDA Sub. no 8');
		}if((FDANumFlag9 !='Y' && strFdaSubNo9 =='')  && (FDANumFlag10  !='Y' && strFdaSubNo10 !='')){
			fnValidateFDASeq('FDA Sub. no 9');
		}
		//validate PMAsup seq after submit
		if((PMASupFlag1 !='Y' && strPmaSupNo1 =='')   && (PMASupFlag2 !='Y' && strPmaSupNo2 !='')){
			fnValidatePMASeq('PMA Sup. no 1');
		}if((PMASupFlag2 !='Y' && strPmaSupNo2 =='')  && (PMASupFlag3 !='Y' && strPmaSupNo3 !='')){
			fnValidatePMASeq('PMA Sup. no 2');
		}if((PMASupFlag3 !='Y' && strPmaSupNo3 =='')  && (PMASupFlag4 !='Y' && strPmaSupNo4 !='')){
			fnValidatePMASeq('PMA Sup. no 3');
		}if((PMASupFlag4 !='Y' && strPmaSupNo4 =='')  && (PMASupFlag5 !='Y' && strPmaSupNo5 !='')){
			fnValidatePMASeq('PMA Sup. no 4');
		}if((PMASupFlag5 !='Y'&& strPmaSupNo5 =='')   && (PMASupFlag6 !='Y' && strPmaSupNo6 !='')){
			fnValidatePMASeq('PMA Sup. no 5');
		}if((PMASupFlag6 !='Y' && strPmaSupNo6 =='')  && (PMASupFlag7 !='Y' && strPmaSupNo7 !='')){
			fnValidatePMASeq('PMA Sup. no 6');
		}if((PMASupFlag7 !='Y' && strPmaSupNo7 =='')  && (PMASupFlag8 !='Y' && strPmaSupNo8 !='')){
			fnValidatePMASeq('PMA Sup. no 7');
		}if((PMASupFlag8 !='Y' && strPmaSupNo8 =='')  && (PMASupFlag9 !='Y' && strPmaSupNo9 !='')){
			fnValidatePMASeq('PMA Sup. no 8');
		}if((PMASupFlag9 !='Y' && strPmaSupNo9 =='')  && (PMASupFlag10  !='Y' && strPmaSupNo10 !='')){
			fnValidatePMASeq('PMA Sup. no 9');
		}
	}
	
	fnAddErrorDetails ("Please enter the correct Sequence data for ", fdaSubSeqErrStr);
	fnAddErrorDetails ("Please enter the correct Sequence data for ", FDASUBSeqErrStr);
	fnAddErrorDetails ("Please enter the correct Sequence data for ", PMASupSeqErrStr);
	fnAddErrorDetails ("Please enter the correct Sequence data for ", pmaSupSeqErrStr);
	fnAddErrorDetails ("Invalid FDA Submission Number ", fdaSubReqFiledStr);
	fnAddErrorDetails ("Invalid PMA Supplement Number ", pmaSupReqFiledStr);
	fnAddErrorDetails ("Invalid FDA Product Code ", fdaProdReqFieldStr);
	fnAddErrorDetails ("Please enter first char as K or G or P value for ", fdaSubCharStr );
	fnAddErrorDetails ("NTF parts require a 510(k) FDA Submission Number ", fdaNoStr );
	fnAddErrorDetails ("FDA Submission Number Should start with 'P' for ", fdaValidateStr );
	fnAddErrorDetails ("Invalid US ", NTFReportStr ); 
	fnAddErrorDetails ("Invalid FDA Product Code ", fdaProdCharStr);
	fnAddErrorDetails ("Please enter last 6 numeric digit value for ", fdaSubNumStr); 
	fnAddErrorDetails ("Supplement Number should contain only 3 digits for ", pmaSupNumStr);
	fnAddErrorDetails ("US Approval Date should not occur before FDA approval ", appSubnoErrStr);
	fnAddErrorDetails ("Invalid ", CETechFileNumberStr);
	
	if (ErrorCount > 0) {
		form.btn_Reg_Submit.disabled = false;
		Error_Show();
		Error_Clear();
		return false;
	}
	
	if(strUsRegClass == '104681' || strUsRegClass == '104682' || strUsRegClass == '104685'){ 
		//104681 Exempt
		//104682 Class I 
		//104685 Tissue Product (Exempt)
		strExemptSubVal = '80130';//Yes
	}else{
		strExemptSubVal = '80131';//No
	}
	
	var partNum = form.partNumber.value;
	var userComments = form.txt_LogReason.value;
	var regPartInputStr = '104640^' +strBrandNm
	+'|104641^' +strUsRegClass
	+'|104642^' +strUsRegPathway
	+'|104643^' +strFdaSubNo1
	+'|104644^' +strFdaSubNo2
	+'|104645^' +strFdaSubNo3
	+'|104646^' +strFdaSubNo4
	+'|104647^' +strFdaSubNo5
	+'|104648^' +strFdaSubNo6
	+'|104649^' +strFdaSubNo7
	+'|104650^' +strFdaSubNo8
	+'|104651^' +strFdaSubNo9
	+'|104652^' +strFdaSubNo10
	+'|104653^' +strApprovalDt.value
	+'|104654^' +strNtfReport
	+'|104655^' +strFdaListing
	+'|104656^' +strPmaSupNo1
	+'|104657^' +strPmaSupNo2
	+'|104658^' +strPmaSupNo3
	+'|104659^' +strPmaSupNo4
	+'|104660^' +strPmaSupNo5
	+'|104661^' +strPmaSupNo6
	+'|104662^' +strPmaSupNo7
	+'|104663^' +strPmaSupNo8
	+'|104664^' +strPmaSupNo9
	+'|104665^' +strPmaSupNo10 
	+'|104666^' +strFdaProdCode1
	+'|104667^' +strFdaProdCode2
	+'|104668^' +strFdaProdCode3
	+'|104669^' +strFdaProdCode4
	+'|104670^' +strFdaProdCode5
	+'|104671^' +strFdaProdCode6
	+'|104672^' +strFdaProdCode7
	+'|104673^' +strFdaProdCode8
	+'|104674^' +strFdaProdCode9
	+'|104675^' +strFdaProdCode10 
	+'|104676^' +strEuRegPathway 
	+'|104677^' +strGmdnCode 
	+'|104678^' +strCeTechFileNum 
	+'|104679^' +strCeTechFileRev 
	+'|104680^' +strNotes
	+'|104701^' +strExemptSubVal +'|';	

	form.strOpt.value = 'Save';
	form.inputStr.value = regPartInputStr;
	form.submit();
}

//validating sequence of FDA Submission Number
function fnValidateFDASeq(fieldnm){
	//alert("fieldnm-->"+fieldnm);
		FDASUBSeqErrStr = FDASUBSeqErrStr +'<b>'+fieldnm +'</b>, ';
}

//validating sequence of PMA Supplement Number
function fnValidatePMASeq(fieldnm){
	PMASupSeqErrStr = PMASupSeqErrStr +'<b>'+fieldnm +'</b>, ';
}


//Validating during submit for Tech File Rev field
function fnValidateTechFileRev(frm){
	var TechFileNum = frm.txt_Ce_Tech_File_Num.value;
	var TechFileRev = frm.txt_Ce_Tech_File_Rev.value; 
	var pattern = /(^[a-zA-Z]$)/;
	if(TechFileNum != ''){
		if((TechFileRev != 0) && !pattern.test(TechFileRev) || TechFileRev == ''){
			Error_Details(message[5769]);
	    }
    }
}

//this function used to show the history details.
function fnShowHistory(pnum, auditId){
	windowOpener("/gmAuditTrail.do?auditId="+auditId+"&txnId="+ encodeURIComponent(pnum), "","resizable=yes,scrollbars=yes,top=250,left=300,width=785,height=300,status=1");
}


//To change the values inside the field to uppercase
function fnUpperCase(obj){

	obj.value = obj.value.toUpperCase();
}


/*function getNumber(num, pos){
	  var sNum = num + "";
	  if(pos > sNum.length || pos <= 0)  
	  {
		  return "";
	  }
	  return sNum.charAt(pos); 
}*/

//This function used to to set the USRegPathWay.
function fnGetUSPathWay(form,typeObj, UsRegPathWayNm){

	var USRegClass = typeObj.value;
	var USRegPathObj = eval('document.all.'+UsRegPathWayNm);
	if(USRegClass == 0){
		USRegPathObj.options.length = 0;
		USRegPathObj.options[0] = new Option("[Choose One]","0");
		return true;
	}
	//Exempt or ClassI or Tissue Product
	if (USRegClass == 104681 || USRegClass == 104682 || USRegClass == 104685) {
		fnSetUSRegPathValues(arrayExempt, USRegPathObj);
	}else 
	//Class II
		if (USRegClass == 104683) {
		fnSetUSRegPathValues(arrayClassII, USRegPathObj);
	}else 
	//Class III
		if (USRegClass == 104684) {
		fnSetUSRegPathValues(arrayClassIII, USRegPathObj);
	}
	
	
}

function fnSetUSRegPathValues (arrObject, USRegPathObject){
	USRegPathObject.options.length = 0;
	USRegPathObject.options[0] = new Option("[Choose One]","0");
	for ( var i = 0; i < arrObject.length; i++) {
		arrayObjCol = arrObject[i];
		arrayObjColArr = arrayObjCol.split(',');
		arrayCodeId = arrayObjColArr[0];
		arrayCodeNm = arrayObjColArr[1];
		USRegPathObject.options[i+1] = new Option(arrayCodeNm,arrayCodeId);
	}
}

//Disabling the CE Tech File Rev based on the CE Tech File Number
function fnSetCETechFileRev(form, Obj){
	var CETechFileNo = Obj.value;
	if(CETechFileNo != ''){
		form.txt_Ce_Tech_File_Rev.disabled = false;
	}else{
		form.txt_Ce_Tech_File_Rev.value = '';
		form.txt_Ce_Tech_File_Rev.disabled = true;
	}
	
}

//Enabling the Approval date field while editing the field FDA Submission No
function fnSetAprrovalDate(form, Obj){
	var val = Obj.value;
	var strPmaSupNo1 = TRIM(form.txt_Pma_Sup_no1.value);
	var strPmaSupNo2 = TRIM(form.txt_Pma_Sup_no2.value);
	var strPmaSupNo3 = TRIM(form.txt_Pma_Sup_no3.value);
	var strPmaSupNo4 = TRIM(form.txt_Pma_Sup_no4.value);
	var strPmaSupNo5 = TRIM(form.txt_Pma_Sup_no5.value);
	var strPmaSupNo6 = TRIM(form.txt_Pma_Sup_no6.value);
	var strPmaSupNo7 = TRIM(form.txt_Pma_Sup_no7.value);
	var strPmaSupNo8 = TRIM(form.txt_Pma_Sup_no8.value);
	var strPmaSupNo9 = TRIM(form.txt_Pma_Sup_no9.value);
	var strPmaSupNo10 = TRIM(form.txt_Pma_Sup_no10.value);
	var strUSRegPathWay = document.all.cbo_Us_Reg_Pathway.value;
	if(val != '' && strUSRegPathWay == '104690' ){
		if(strPmaSupNo1 =='' && strPmaSupNo2 =='' && strPmaSupNo3 =='' && strPmaSupNo4 =='' && strPmaSupNo5 =='' && 
				strPmaSupNo6 =='' && strPmaSupNo7 =='' && strPmaSupNo8 =='' && strPmaSupNo9 =='' && strPmaSupNo10 ==''){
			
			form.txt_Approval_Dt.value = '';
			form.txt_Approval_Dt.disabled = true;
			document.getElementById("Img_Date").disabled = true;
		}else{
			form.txt_Approval_Dt.disabled = false;
			document.getElementById("Img_Date").disabled = false;
		}
	}else if(val != '' && strUSRegPathWay != '104690'  ){
		form.txt_Approval_Dt.disabled = false;
		document.getElementById("Img_Date").disabled = false;
		}
	else{
		fnDisableAprrovalDate(form);
	}
}

//Disabling the approval date field during on load
function fnDisableAprrovalDate(form){
	var strFdaSubNo1 = form.txt_Fda_sub_no1.value;
	var strFdaSubNo2 = form.txt_Fda_sub_no2.value;
	var strFdaSubNo3 = form.txt_Fda_sub_no3.value;
	var strFdaSubNo4 = form.txt_Fda_sub_no4.value;
	var strFdaSubNo5 = form.txt_Fda_sub_no5.value;
	var strFdaSubNo6 = form.txt_Fda_sub_no6.value;
	var strFdaSubNo7 = form.txt_Fda_sub_no7.value;
	var strFdaSubNo8 = form.txt_Fda_sub_no8.value;
	var strFdaSubNo9 = form.txt_Fda_sub_no9.value;
	var strFdaSubNo10 = form.txt_Fda_sub_no10.value;
	var strPmaSupNo1 = TRIM(form.txt_Pma_Sup_no1.value);
	var strPmaSupNo2 = TRIM(form.txt_Pma_Sup_no2.value);
	var strPmaSupNo3 = TRIM(form.txt_Pma_Sup_no3.value);
	var strPmaSupNo4 = TRIM(form.txt_Pma_Sup_no4.value);
	var strPmaSupNo5 = TRIM(form.txt_Pma_Sup_no5.value);
	var strPmaSupNo6 = TRIM(form.txt_Pma_Sup_no6.value);
	var strPmaSupNo7 = TRIM(form.txt_Pma_Sup_no7.value);
	var strPmaSupNo8 = TRIM(form.txt_Pma_Sup_no8.value);
	var strPmaSupNo9 = TRIM(form.txt_Pma_Sup_no9.value);
	var strPmaSupNo10 = TRIM(form.txt_Pma_Sup_no10.value);
	var strUSRegPathWay = document.all.cbo_Us_Reg_Pathway.value;
	
	if(strUSRegPathWay == '104690'){
		if(strPmaSupNo1 =='' && strPmaSupNo2 =='' && strPmaSupNo3 =='' && strPmaSupNo4 =='' && strPmaSupNo5 =='' && 
				strPmaSupNo6 =='' && strPmaSupNo7 =='' && strPmaSupNo8 =='' && strPmaSupNo9 =='' && strPmaSupNo10 ==''){
			
			form.txt_Approval_Dt.value = '';
			form.txt_Approval_Dt.disabled = true;
			document.getElementById("Img_Date").disabled = true;
	   }
	}		
	if(strFdaSubNo1 =='' && strFdaSubNo2 =='' && strFdaSubNo3 =='' && strFdaSubNo4 =='' && strFdaSubNo5 =='' && 
			strFdaSubNo6 =='' && strFdaSubNo7 =='' && strFdaSubNo8 =='' && strFdaSubNo9 =='' && strFdaSubNo10 ==''){
		form.txt_Approval_Dt.value = '';
		form.txt_Approval_Dt.disabled = true;
		document.getElementById("Img_Date").disabled = true;
	}
	

}
function fnSetNTFReport(form, Obj){
	
	var USRegPath = Obj.value;
	var strFdaSubNo1 = form.txt_Fda_sub_no1.value;
	var strFdaSubNo2 = form.txt_Fda_sub_no2.value;
	var strFdaSubNo3 = form.txt_Fda_sub_no3.value;
	var strFdaSubNo4 = form.txt_Fda_sub_no4.value;
	var strFdaSubNo5 = form.txt_Fda_sub_no5.value;
	var strFdaSubNo6 = form.txt_Fda_sub_no6.value;
	var strFdaSubNo7 = form.txt_Fda_sub_no7.value;
	var strFdaSubNo8 = form.txt_Fda_sub_no8.value;
	var strFdaSubNo9 = form.txt_Fda_sub_no9.value;
	var strFdaSubNo10 = form.txt_Fda_sub_no10.value;
	var strPmaSupNo1 = TRIM(form.txt_Pma_Sup_no1.value);
	var strPmaSupNo2 = TRIM(form.txt_Pma_Sup_no2.value);
	var strPmaSupNo3 = TRIM(form.txt_Pma_Sup_no3.value);
	var strPmaSupNo4 = TRIM(form.txt_Pma_Sup_no4.value);
	var strPmaSupNo5 = TRIM(form.txt_Pma_Sup_no5.value);
	var strPmaSupNo6 = TRIM(form.txt_Pma_Sup_no6.value);
	var strPmaSupNo7 = TRIM(form.txt_Pma_Sup_no7.value);
	var strPmaSupNo8 = TRIM(form.txt_Pma_Sup_no8.value);
	var strPmaSupNo9 = TRIM(form.txt_Pma_Sup_no9.value);
	var strPmaSupNo10 = TRIM(form.txt_Pma_Sup_no10.value);

	if(USRegPath != '104687'){
		form.txt_Ntf_Reported.value = '';
		form.txt_Ntf_Reported.disabled = true;
	}else{
		form.txt_Ntf_Reported.disabled = false;
	}
	if(USRegPath != '104690'){
		form.txt_Pma_Sup_no1.disabled = true;
		form.txt_Pma_Sup_no2.disabled = true;
		form.txt_Pma_Sup_no3.disabled = true;
		form.txt_Pma_Sup_no4.disabled = true;
		form.txt_Pma_Sup_no5.disabled = true;
		form.txt_Pma_Sup_no6.disabled = true;
		form.txt_Pma_Sup_no7.disabled = true;
		form.txt_Pma_Sup_no8.disabled = true;
		form.txt_Pma_Sup_no9.disabled = true;
		form.txt_Pma_Sup_no10.disabled = true;
		form.txt_Pma_Sup_no1.value = '';
		form.txt_Pma_Sup_no2.value = '';
		form.txt_Pma_Sup_no3.value = '';
		form.txt_Pma_Sup_no4.value = '';
		form.txt_Pma_Sup_no5.value = '';
		form.txt_Pma_Sup_no6.value = '';
		form.txt_Pma_Sup_no7.value = '';
		form.txt_Pma_Sup_no8.value = '';
		form.txt_Pma_Sup_no9.value = '';
		form.txt_Pma_Sup_no10.value = '';
		if(strFdaSubNo1 =='' && strFdaSubNo2 =='' && strFdaSubNo3 =='' && strFdaSubNo4 =='' && strFdaSubNo5 =='' && 
				strFdaSubNo6 =='' && strFdaSubNo7 =='' && strFdaSubNo8 =='' && strFdaSubNo9 =='' && strFdaSubNo10 ==''){
			form.txt_Approval_Dt.value = '';
			form.txt_Approval_Dt.disabled = true;
			document.getElementById("Img_Date").disabled = true;
		}else{
			form.txt_Approval_Dt.disabled = false;
			document.getElementById("Img_Date").disabled = false;
		}
		
	}else{
		form.txt_Pma_Sup_no1.disabled = false;
		form.txt_Pma_Sup_no2.disabled = false;
		form.txt_Pma_Sup_no3.disabled = false;
		form.txt_Pma_Sup_no4.disabled = false;
		form.txt_Pma_Sup_no5.disabled = false;
		form.txt_Pma_Sup_no6.disabled = false;
		form.txt_Pma_Sup_no7.disabled = false;
		form.txt_Pma_Sup_no8.disabled = false;
		form.txt_Pma_Sup_no9.disabled = false;
		form.txt_Pma_Sup_no10.disabled = false;
		if(strPmaSupNo1 =='' && strPmaSupNo2 =='' && strPmaSupNo3 =='' && strPmaSupNo4 =='' && strPmaSupNo5 =='' && 
				strPmaSupNo6 =='' && strPmaSupNo7 =='' && strPmaSupNo8 =='' && strPmaSupNo9 =='' && strPmaSupNo10 ==''){
			
			form.txt_Approval_Dt.value = '';
			form.txt_Approval_Dt.disabled = true;
			document.getElementById("Img_Date").disabled = true;
		}else{
			form.txt_Approval_Dt.disabled = false;
			document.getElementById("Img_Date").disabled = false;
		}
	}
}
 

//Validating NTF Reported field
function fnValidaeNTF(frm, val, fieldNm){
	var appErrorFlag;
	var NTFReportFlag = true;
	var firstChar ="";
	var numberVal ="";
	var FDANo ="";
	var falsecnt=0;
	var strFdaSubNo1 = frm.txt_Fda_sub_no1.value;
	var strFdaSubNo2 = frm.txt_Fda_sub_no2.value;
	var strFdaSubNo3 = frm.txt_Fda_sub_no3.value;
	var strFdaSubNo4 = frm.txt_Fda_sub_no4.value;
	var strFdaSubNo5 = frm.txt_Fda_sub_no5.value;
	var strFdaSubNo6 = frm.txt_Fda_sub_no6.value;
	var strFdaSubNo7 = frm.txt_Fda_sub_no7.value;
	var strFdaSubNo8 = frm.txt_Fda_sub_no8.value;
	var strFdaSubNo9 = frm.txt_Fda_sub_no9.value;
	var strFdaSubNo10 = frm.txt_Fda_sub_no10.value;
	var NTFReport = val.split(',');
	if(NTFReport != ""){
	for ( var i = 0; i < NTFReport.length; i++) {
		NTFReportCol = NTFReport[i];
		 
		firstChar = NTFReportCol.substring(0, 1);
		numberVal = NTFReportCol.substring(1, NTFReport[i].length);
		
		if (firstChar != 'K') {
			NTFReportStr = '<b>'+fieldNm +'</b>,';
			NTFReportFlag = false;
		}
		if (numberVal != ''){
			if ((isNaN(numberVal)) || (numberVal.length !=6) ) {
				NTFReportStr = '<b>'+fieldNm +'</b>,';
				NTFReportFlag = false;
			}
		}
	
		//NTF Reported value validating for greater than FDA Submission No
		if(NTFReportFlag == true){
		if(strFdaSubNo1 !=''){
			appErrorFlag = fncheckNTF(numberVal, strFdaSubNo1,  appErrorFlag);
			
		} if(strFdaSubNo2 !='' && appErrorFlag == false){
			appErrorFlag = fncheckNTF(numberVal, strFdaSubNo2,  appErrorFlag);
		
		} if(strFdaSubNo3 !='' && appErrorFlag == false){
			appErrorFlag = fncheckNTF(numberVal, strFdaSubNo3,  appErrorFlag);
			
		} if(strFdaSubNo4 !='' && appErrorFlag == false){
			appErrorFlag = fncheckNTF(numberVal, strFdaSubNo4,  appErrorFlag);
			
		} if(strFdaSubNo5 !='' && appErrorFlag == false){
			appErrorFlag = fncheckNTF(numberVal, strFdaSubNo5,  appErrorFlag);
			
		} if(strFdaSubNo6 !='' && appErrorFlag == false){
			appErrorFlag = fncheckNTF(numberVal, strFdaSubNo6,  appErrorFlag);
			
		} if(strFdaSubNo7 !='' && appErrorFlag == false){
			appErrorFlag = fncheckNTF(numberVal, strFdaSubNo7,  appErrorFlag);
			
		} if(strFdaSubNo8 !='' && appErrorFlag == false){
			appErrorFlag = fncheckNTF(numberVal, strFdaSubNo8,  appErrorFlag);
			
		} if(strFdaSubNo9 !='' && appErrorFlag == false){
			appErrorFlag = fncheckNTF(numberVal, strFdaSubNo9,  appErrorFlag);
			
		} if(strFdaSubNo10 !='' && appErrorFlag == false){
			appErrorFlag = fncheckNTF(numberVal, strFdaSubNo10, appErrorFlag);
		}
		if(appErrorFlag == false){
			falsecnt++;
		}
	  }
	}
  }
	if(falsecnt>0){
		Error_Details(message[5770]);
	}
}	

function fncheckNTF(numval, FDANo, errorStr){
	var FDANo = FDANo.substring(1, 7);

	if(FDANo < numval){
		errorStr = true;
	}else{
		errorStr = false;
	}
	return errorStr;
}
	
//Validating FDAProCode Number Fields
function fnFDAProCode(e){
	var unicode=e.charCode? e.charCode : e.keyCode
	if (unicode!=8){
		if((unicode<65 || unicode>90)  && (unicode<97 || unicode>122)){
			Error_Details(message[5771]);
		}
	}
	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}
}

//Validating during submit for Tech File Number field
function fnValidateTechFileNumber(fieldVal, fieldNm){

	if(fieldVal != ''){
		if(fieldVal.length !=8 ){
			CETechFileNumberStr = CETechFileNumberStr +'<b>'+fieldNm +'</b>, ';
		}else {
			firstChar  = fieldVal.substring(0, 1);
			secondChar = fieldVal.substring(1, 2);
			thirdChar  = fieldVal.substring(2, 3);
			fourthChar = fieldVal.substring(3, 4);
			fifthChar  = fieldVal.substring(4, 5);
			numberVal  = fieldVal.substring(5, fieldVal.length);
			if (firstChar != 'G' || secondChar != 'M' || thirdChar != 'C' || fourthChar != 'E' || fifthChar != '-') {
				CETechFileNumberStr = CETechFileNumberStr +'<b>'+fieldNm +'</b>, ';
			}else if (numberVal != '') {
				if (isNaN(numberVal)) {
					CETechFileNumberStr = CETechFileNumberStr +'<b>'+fieldNm +'</b>, ';
				}
			}
		}
	}
}

//function validate during submit for field FDA Listing Number
function fnValidateFDAListing(fieldVal, fieldNm){
	var pattern = /(^[a-zA-Z]$)/;
	if(fieldVal != ''){
		if(fieldVal.length !=7 ){
			Error_Details(message[5772]);
		}else{
			firstChar  = fieldVal.substring(0, 1);
			numberVal  = fieldVal.substring(1, fieldVal.length);
			if (!pattern.test(firstChar) || (isNaN(numberVal))) {
				Error_Details(message[5773]);
			}
		}
	}
}

//Validating TechFile Number Fields
/*function fnTechFileNo(e,obj){
	
	var unicode=e.charCode? e.charCode : e.keyCode
	var val = obj.value;
	
	if(val.length < 8){
		if(val.length < 5){
		if (unicode!=8){ //if the key isn't the backspace key (which we should allow)\
		
			if ((val.length == 0 &&  unicode != 71 && unicode != 103) || (val.length == 1 && unicode != 77 && unicode != 109) 
					|| (val.length == 2 && unicode != 67 && unicode != 99) || (val.length == 3 && unicode != 69 && unicode != 101)
					|| (val.length == 4 && unicode != 45)){ 
					Error_Details("Value should start with <b>GMCE-</b");
				}
			}
		}else{
				if(unicode<48||unicode>57){
					//if not a number
					  Error_Details("Please enter only numeric values");
				 }
			}
	}
	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}
}*/

//Validating Tech File Rev Field
function fnTechFileRev(e,Obj){
	var unicode=e.charCode? e.charCode : e.keyCode
	var TchFileRev = document.all.txt_Ce_Tech_File_Num.value;
	var val = Obj.value;
	
	if(TchFileRev !='' && val.length <1){
		if (unicode!=8){
			if((unicode !=48) && (unicode<65 || unicode>90) && (unicode<97 || unicode>122)){
				Error_Details(message[5774]);
			}
	    }
    }
		if (ErrorCount > 0){
			Error_Show();
			Error_Clear();
			return false;
		}
	
}

//Validating FDA Listing Number Field
/*function fnFDAListingNo(e,obj){
	var unicode=e.charCode? e.charCode : e.keyCode
	var val = obj.value;
	if(val.length < 7){
		if(val ==""){
		if (unicode!=8){ //if the key isn't the backspace key (which we should allow)\
			  if ((unicode != 68) && (unicode != 100)){ 
					  Error_Details("Value should start with <B>D</B>");
				}
			}
		}else{
			  if(unicode<48||unicode>57){//if not a number
				  Error_Details("Please enter only numeric values");
			 }
		}
	}
	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}

}*/

//Validating GMDN Code Field
function fnGMDNCode(e,obj){
	var unicode = e.charCode? e.charCode : e.keyCode
	var val=obj.value;
	if(val.length<6){
		if (unicode!=8){ 
			if (unicode<48 || unicode>57){
			    Error_Details(message[5775]);
			}
		}
	}
	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}		
}

//Validating PMT Supplement Number fields
/*function fnPMASupNo(e,obj){
	
	var unicode=e.charCode? e.charCode : e.keyCode
	var strUSRegPathWay = document.all.cbo_Us_Reg_Pathway.value;
	var val = obj.value;
	
	if(val == '' ){
	if(strUSRegPathWay == '104690'){
		if (unicode!=8){ //if the key isn't the backspace key (which we should allow)\
			if ((unicode != 83) && (unicode != 115)){ 
				Error_Details("Value should start with <B>S</B>");
			}
		}
	 }
   }else{
	  if(unicode<48||unicode>57){//if not a number
		  Error_Details("Please enter only numeric values");
	  }
   }
    if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}
}*/

//Validating FDA Submission Number fields
/*function fnNumbersOnly(e,obj){
	
	var firstChar ='';
	var unicode=e.charCode? e.charCode : e.keyCode
	var val = obj.value;
	alert("val-->"+val);
	var strUSRegPathWay = document.all.cbo_Us_Reg_Pathway.value;
	if(val == ''){
	if(strUSRegPathWay == '104687'){
		if (unicode!=8){ //if the key isn't the backspace key (which we should allow)\
			if ((unicode != 75) && (unicode != 107)){ 
				Error_Details("Value should start with <B>K</B>");
			}
		}
	}else if (unicode!=8){ //if the key isn't the backspace key (which we should allow)\
		if ((unicode != 75) && (unicode != 80) && (unicode != 71) && (unicode != 107)
				&& (unicode != 103) && (unicode != 112)){ 
			Error_Details("Value should start with <B>K or G or P</B>");
		}
	}
   }else{
	  if((unicode<48||unicode>57) && val.length<7 ){//if not a number
		  Error_Details("Please enter only numeric values");
	}
   }
    if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}
}*/

//Mandatory FDA Submission Number field when type as  No 510(K) required (NTF)
function fnMandFDASubmNo(frm){
	var strFdaSubNo1 = TRIM(frm.txt_Fda_sub_no1.value);
	var strFdaSubNo2 = TRIM(frm.txt_Fda_sub_no2.value);
	var strFdaSubNo3 = TRIM(frm.txt_Fda_sub_no3.value);
	var strFdaSubNo4 = TRIM(frm.txt_Fda_sub_no4.value);
	var strFdaSubNo5 = TRIM(frm.txt_Fda_sub_no5.value);
	var strFdaSubNo6 = TRIM(frm.txt_Fda_sub_no6.value);
	var strFdaSubNo7 = TRIM(frm.txt_Fda_sub_no7.value);
	var strFdaSubNo8 = TRIM(frm.txt_Fda_sub_no8.value);
	var strFdaSubNo9 = TRIM(frm.txt_Fda_sub_no9.value);
	var strFdaSubNo10 = TRIM(frm.txt_Fda_sub_no10.value);
	var strUSRegPathWay = document.all.cbo_Us_Reg_Pathway.value;
	if(strUSRegPathWay == '104687'){
		if(strFdaSubNo1 =='' && strFdaSubNo2 =='' && strFdaSubNo3 =='' && strFdaSubNo4 =='' && strFdaSubNo5 =='' && 
				strFdaSubNo6 =='' && strFdaSubNo7 =='' && strFdaSubNo8 =='' && strFdaSubNo9 =='' && strFdaSubNo10 ==''){
			Error_Details(message[5776]);
		}	
	}
}

//Mandatory appproval date when FDA Submission field starts with K or P
function fnMandatoryApprovalDate(frm){
	var firstChar ='';
	var mandatorydateflag = true;
	var strApprovalDt = frm.txt_Approval_Dt.value;
	var strFdaSubNo1 = TRIM(frm.txt_Fda_sub_no1.value);
	var strFdaSubNo2 = TRIM(frm.txt_Fda_sub_no2.value);
	var strFdaSubNo3 = TRIM(frm.txt_Fda_sub_no3.value);
	var strFdaSubNo4 = TRIM(frm.txt_Fda_sub_no4.value);
	var strFdaSubNo5 = TRIM(frm.txt_Fda_sub_no5.value);
	var strFdaSubNo6 = TRIM(frm.txt_Fda_sub_no6.value);
	var strFdaSubNo7 = TRIM(frm.txt_Fda_sub_no7.value);
	var strFdaSubNo8 = TRIM(frm.txt_Fda_sub_no8.value);
	var strFdaSubNo9 = TRIM(frm.txt_Fda_sub_no9.value);
	var strFdaSubNo10 = TRIM(frm.txt_Fda_sub_no10.value);
	var strPmaSupNo1 = TRIM(frm.txt_Pma_Sup_no1.value);
	var strPmaSupNo2 = TRIM(frm.txt_Pma_Sup_no2.value);
	var strPmaSupNo3 = TRIM(frm.txt_Pma_Sup_no3.value);
	var strPmaSupNo4 = TRIM(frm.txt_Pma_Sup_no4.value);
	var strPmaSupNo5 = TRIM(frm.txt_Pma_Sup_no5.value);
	var strPmaSupNo6 = TRIM(frm.txt_Pma_Sup_no6.value);
	var strPmaSupNo7 = TRIM(frm.txt_Pma_Sup_no7.value);
	var strPmaSupNo8 = TRIM(frm.txt_Pma_Sup_no8.value);
	var strPmaSupNo9 = TRIM(frm.txt_Pma_Sup_no9.value);
	var strPmaSupNo10 = TRIM(frm.txt_Pma_Sup_no10.value);
	var strUSRegPathWay = document.all.cbo_Us_Reg_Pathway.value;
	var PMASupFlag = false;
	if(strUSRegPathWay == '104690' && strPmaSupNo1 =='' && strPmaSupNo2 =='' && strPmaSupNo3 =='' && strPmaSupNo4 =='' && strPmaSupNo5 =='' && 
			strPmaSupNo6 =='' && strPmaSupNo7 =='' && strPmaSupNo8 =='' && strPmaSupNo9 =='' && strPmaSupNo10 ==''){
		PMASupFlag = true;
	}
	if(PMASupFlag == false){
	if(strFdaSubNo1 !=''){
		firstChar = strFdaSubNo1.substring(0, 1);
		if ((firstChar == 'K' || firstChar == 'P' || firstChar == 'G') && strApprovalDt =='') {
			mandatorydateflag = fnValidateText(document.all.txt_Approval_Dt, mandatorydateflag);
		}
	}if(strFdaSubNo2 !=''){
		firstChar = strFdaSubNo2.substring(0, 1);
		if ((firstChar == 'K' || firstChar == 'P' || firstChar == 'G') && strApprovalDt =='') {
			mandatorydateflag = fnValidateText(document.all.txt_Approval_Dt, mandatorydateflag);
		}
	}if(strFdaSubNo3 !=''){
		firstChar = strFdaSubNo3.substring(0, 1);
		if ((firstChar == 'K' || firstChar == 'P' || firstChar == 'G') && strApprovalDt =='') {
			mandatorydateflag = fnValidateText(document.all.txt_Approval_Dt, mandatorydateflag);
		}
	}if(strFdaSubNo4 !=''){
		firstChar = strFdaSubNo4.substring(0, 1);
		if ((firstChar == 'K' || firstChar == 'P' || firstChar == 'G') && strApprovalDt =='') {
			mandatorydateflag = fnValidateText(document.all.txt_Approval_Dt, mandatorydateflag);
		}
	}if(strFdaSubNo5 !=''){
		firstChar = strFdaSubNo5.substring(0, 1);
		if ((firstChar == 'K' || firstChar == 'P' || firstChar == 'G') && strApprovalDt =='') {
			mandatorydateflag = fnValidateText(document.all.txt_Approval_Dt, mandatorydateflag);
		}
	}if(strFdaSubNo6 !=''){
		firstChar = strFdaSubNo6.substring(0, 1);
		if ((firstChar == 'K' || firstChar == 'P' || firstChar == 'G') && strApprovalDt =='') {
			mandatorydateflag = fnValidateText(document.all.txt_Approval_Dt, mandatorydateflag);
		}
	}if(strFdaSubNo7 !=''){
		firstChar = strFdaSubNo7.substring(0, 1);
		if ((firstChar == 'K' || firstChar == 'P' || firstChar == 'G') && strApprovalDt =='') {
			mandatorydateflag = fnValidateText(document.all.txt_Approval_Dt, mandatorydateflag);
		}
	}if(strFdaSubNo8 !=''){
		firstChar = strFdaSubNo8.substring(0, 1);
		if ((firstChar == 'K' || firstChar == 'P' || firstChar == 'G') && strApprovalDt =='') {
			mandatorydateflag = fnValidateText(document.all.txt_Approval_Dt, mandatorydateflag);
		}
	}if(strFdaSubNo9 !=''){
		firstChar = strFdaSubNo9.substring(0, 1);
		if ((firstChar == 'K' || firstChar == 'P' || firstChar == 'G') && strApprovalDt =='') {
			mandatorydateflag = fnValidateText(document.all.txt_Approval_Dt, mandatorydateflag);
		}
	}if(strFdaSubNo10 !=''){
		firstChar = strFdaSubNo10.substring(0, 1);
		if ((firstChar == 'K' || firstChar == 'P' || firstChar == 'G') && strApprovalDt =='') {
			mandatorydateflag = fnValidateText(document.all.txt_Approval_Dt, mandatorydateflag);
		}
	}
  }
	if(mandatorydateflag == false){
		Error_Details(message[5777]);
	}
	
}

//To validate the text field
function fnValidateText(form, mandatorydateflag){
	if(form.value == ''){
		mandatorydateflag = false;
	}
	return mandatorydateflag;
}

//validate approval date
function fnValidateApprovalDate(frm){
	var appErrorFlag;
	var strFdaSubNo1 = TRIM(frm.txt_Fda_sub_no1.value);
	var strFdaSubNo2 = TRIM(frm.txt_Fda_sub_no2.value);
	var strFdaSubNo3 = TRIM(frm.txt_Fda_sub_no3.value);
	var strFdaSubNo4 = TRIM(frm.txt_Fda_sub_no4.value);
	var strFdaSubNo5 = TRIM(frm.txt_Fda_sub_no5.value);
	var strFdaSubNo6 = TRIM(frm.txt_Fda_sub_no6.value);
	var strFdaSubNo7 = TRIM(frm.txt_Fda_sub_no7.value);
	var strFdaSubNo8 = TRIM(frm.txt_Fda_sub_no8.value);
	var strFdaSubNo9 = TRIM(frm.txt_Fda_sub_no9.value);
	var strFdaSubNo10 = TRIM(frm.txt_Fda_sub_no10.value);
	var strApprovalDt = frm.txt_Approval_Dt;
	if(strApprovalDt.value !=""){
		if(strFdaSubNo1 !=''){
			appErrorFlag = fncheckFDASubno(strApprovalDt, strFdaSubNo1, 'FDA Sub. no 1', appErrorFlag);
		} if(strFdaSubNo2 !=''){
			appErrorFlag = fncheckFDASubno(strApprovalDt, strFdaSubNo2, 'FDA Sub. no 2', appErrorFlag);
		} if(strFdaSubNo3 !=''){
			appErrorFlag = fncheckFDASubno(strApprovalDt, strFdaSubNo3, 'FDA Sub. no 3', appErrorFlag);
		} if(strFdaSubNo4 !=''){
			appErrorFlag = fncheckFDASubno(strApprovalDt, strFdaSubNo4, 'FDA Sub. no 4', appErrorFlag);
		} if(strFdaSubNo5 !=''){
			appErrorFlag = fncheckFDASubno(strApprovalDt, strFdaSubNo5, 'FDA Sub. no 5', appErrorFlag);
		} if(strFdaSubNo6 !=''){
			appErrorFlag = fncheckFDASubno(strApprovalDt, strFdaSubNo6, 'FDA Sub. no 6', appErrorFlag);
		} if(strFdaSubNo7 !=''){
			appErrorFlag = fncheckFDASubno(strApprovalDt, strFdaSubNo7, 'FDA Sub. no 7', appErrorFlag);
		} if(strFdaSubNo8 !=''){
			appErrorFlag = fncheckFDASubno(strApprovalDt, strFdaSubNo8, 'FDA Sub. no 8', appErrorFlag);
		} if(strFdaSubNo9 !=''){
			appErrorFlag = fncheckFDASubno(strApprovalDt, strFdaSubNo9, 'FDA Sub. no 9', appErrorFlag);
		} if(strFdaSubNo10 !=''){
			appErrorFlag = fncheckFDASubno(strApprovalDt, strFdaSubNo10, 'FDA Sub. no 10', appErrorFlag);
		}
	}
	if(appErrorFlag == false){
		Error_Details(message[5778]);
	}
}

//validating for all the fda field with approval date
function fncheckFDASubno(date, FDAno, fieldNm, errorStr){
	var apprvdate = date.value;
	apprvdate = apprvdate.substring(8, 10);
	var FDASubNo = FDAno.substring(1, 3);

	if(FDASubNo > apprvdate){
		if(errorStr){
			errorStr = true;
		}else{
			errorStr = false;
		}
		
	}else{
		if(FDASubNo <= apprvdate){
			errorStr = true;
		}
	}
	return errorStr;
	
}
/*function fncheckFDASubno(date, FDAno, fieldNm, errorStr){

	var tempfl = true;
	var date = new Date(date.value);
	var year = date.getFullYear();
	var Ypos2 = getNumber(year,'2');
	var Ypos3 = getNumber(year,'3');
	var Yearpos = Ypos2+""+Ypos3;
	if(FDAno == ''){
		tempfl = false;
	}
	var fdapos1 = getNumber(FDAno,'1');
	var fdapos2 = getNumber(FDAno,'2');
	var FDANo = fdapos1+""+fdapos2;
	if(parseInt(Yearpos) >= parseInt(FDANo)){
		tempfl = false;
	}
	if(tempfl){
		errorStr = errorStr + '<b>'+fieldNm +'</b>, ';
	}
	return errorStr;
}*/
//FDA Submission Field mandatory when USRegpathway as NO 510(K) Required (NTF)
function fnValidateFdaField(frm){
	var strFdaSubNo1 = TRIM(frm.txt_Fda_sub_no1.value);
	var strFdaSubNo2 = TRIM(frm.txt_Fda_sub_no2.value);
	var strFdaSubNo3 = TRIM(frm.txt_Fda_sub_no3.value);
	var strFdaSubNo4 = TRIM(frm.txt_Fda_sub_no4.value);
	var strFdaSubNo5 = TRIM(frm.txt_Fda_sub_no5.value);
	var strFdaSubNo6 = TRIM(frm.txt_Fda_sub_no6.value);
	var strFdaSubNo7 = TRIM(frm.txt_Fda_sub_no7.value);
	var strFdaSubNo8 = TRIM(frm.txt_Fda_sub_no8.value);
	var strFdaSubNo9 = TRIM(frm.txt_Fda_sub_no9.value);
	var strFdaSubNo10 = TRIM(frm.txt_Fda_sub_no10.value);
	var strUSRegPathWay = document.all.cbo_Us_Reg_Pathway.value;
	var strApprovalDt = frm.txt_Approval_Dt.value;
	if(strUSRegPathWay == '104687'){
		var hBrandName = frm.hBrandName.value;
		if(hBrandName ==''){
			fdaSubSeqErrStr = fnValidateSeq (strFdaSubNo1, strFdaSubNo2, 'FDA Sub. no 1', fdaSubSeqErrStr );
			fdaSubSeqErrStr = fnValidateSeq (strFdaSubNo2, strFdaSubNo3, 'FDA Sub. no 2', fdaSubSeqErrStr );
			fdaSubSeqErrStr = fnValidateSeq (strFdaSubNo3, strFdaSubNo4, 'FDA Sub. no 3',fdaSubSeqErrStr);
			fdaSubSeqErrStr = fnValidateSeq (strFdaSubNo4, strFdaSubNo5, 'FDA Sub. no 4',fdaSubSeqErrStr);
			fdaSubSeqErrStr = fnValidateSeq (strFdaSubNo5, strFdaSubNo6, 'FDA Sub. no 5',fdaSubSeqErrStr);
			fdaSubSeqErrStr = fnValidateSeq (strFdaSubNo6, strFdaSubNo7, 'FDA Sub. no 6',fdaSubSeqErrStr);
			fdaSubSeqErrStr = fnValidateSeq (strFdaSubNo7, strFdaSubNo8, 'FDA Sub. no 7',fdaSubSeqErrStr);
			fdaSubSeqErrStr = fnValidateSeq (strFdaSubNo8, strFdaSubNo9, 'FDA Sub. no 8',fdaSubSeqErrStr);
			fdaSubSeqErrStr = fnValidateSeq (strFdaSubNo9, strFdaSubNo10, 'FDA Sub. no 9',fdaSubSeqErrStr);
		}
	fnCheckFDAField(strFdaSubNo1, 'FDA Sub. no 1');
	fnCheckFDAField(strFdaSubNo2,'FDA Sub. no 2');
	fnCheckFDAField(strFdaSubNo3,'FDA Sub. no 3');
	fnCheckFDAField(strFdaSubNo4,'FDA Sub. no 4');
	fnCheckFDAField(strFdaSubNo5,'FDA Sub. no 5');
	fnCheckFDAField(strFdaSubNo6,'FDA Sub. no 6');
	fnCheckFDAField(strFdaSubNo7,'FDA Sub. no 7');
	fnCheckFDAField(strFdaSubNo8,'FDA Sub. no 8');
	fnCheckFDAField(strFdaSubNo9,'FDA Sub. no 9');
	fnCheckFDAField(strFdaSubNo10,'FDA Sub. no 10');
	}
	//Checking for pathway as PMA Required & Approval Date value is not empty
	if(strUSRegPathWay == '104689' && strApprovalDt != ''){
		var hBrandName = frm.hBrandName.value;
		if(hBrandName ==''){
			fdaSubSeqErrStr = fnValidateSeq (strFdaSubNo1, strFdaSubNo2, 'FDA Sub. no 1', fdaSubSeqErrStr );
			fdaSubSeqErrStr = fnValidateSeq (strFdaSubNo2, strFdaSubNo3, 'FDA Sub. no 2', fdaSubSeqErrStr );
			fdaSubSeqErrStr = fnValidateSeq (strFdaSubNo3, strFdaSubNo4, 'FDA Sub. no 3',fdaSubSeqErrStr);
			fdaSubSeqErrStr = fnValidateSeq (strFdaSubNo4, strFdaSubNo5, 'FDA Sub. no 4',fdaSubSeqErrStr);
			fdaSubSeqErrStr = fnValidateSeq (strFdaSubNo5, strFdaSubNo6, 'FDA Sub. no 5',fdaSubSeqErrStr);
			fdaSubSeqErrStr = fnValidateSeq (strFdaSubNo6, strFdaSubNo7, 'FDA Sub. no 6',fdaSubSeqErrStr);
			fdaSubSeqErrStr = fnValidateSeq (strFdaSubNo7, strFdaSubNo8, 'FDA Sub. no 7',fdaSubSeqErrStr);
			fdaSubSeqErrStr = fnValidateSeq (strFdaSubNo8, strFdaSubNo9, 'FDA Sub. no 8',fdaSubSeqErrStr);
			fdaSubSeqErrStr = fnValidateSeq (strFdaSubNo9, strFdaSubNo10, 'FDA Sub. no 9',fdaSubSeqErrStr);
		}
	fnValidateFDAField(strFdaSubNo1, 'FDA Sub. no 1');
	fnValidateFDAField(strFdaSubNo2,'FDA Sub. no 2');
	fnValidateFDAField(strFdaSubNo3,'FDA Sub. no 3');
	fnValidateFDAField(strFdaSubNo4,'FDA Sub. no 4');
	fnValidateFDAField(strFdaSubNo5,'FDA Sub. no 5');
	fnValidateFDAField(strFdaSubNo6,'FDA Sub. no 6');
	fnValidateFDAField(strFdaSubNo7,'FDA Sub. no 7');
	fnValidateFDAField(strFdaSubNo8,'FDA Sub. no 8');
	fnValidateFDAField(strFdaSubNo9,'FDA Sub. no 9');
	fnValidateFDAField(strFdaSubNo10,'FDA Sub. no 10');
	}
}

function fnCheckFDAField(fieldVal, fieldNm){
	if (fieldVal != '') {
		var firstChar = '';
		var numberVal = '';
		
		// validate the FDA filed.
		if (fieldVal.length != 7) {
				fdaSubReqFiledStr = fdaSubReqFiledStr +'<b>'+fieldNm +'</b>, ';
			}else{
				firstChar = fieldVal.substring(0, 1);
				numberVal = fieldVal.substring(1, fieldVal.length);
				if (firstChar != 'K') {
					fdaNoStr = fdaNoStr +'<b>'+fieldNm +'</b>, ';
				}
				if (numberVal != '') {
					if (isNaN(numberVal)) {
						fdaSubNumStr = fdaSubNumStr +'<b>'+fieldNm +'</b>, ';
					}
				}
			}
		
	}
	
}
//Checking for pathway as PMA Required
function fnValidateFDAField(fieldVal, fieldNm){
	if (fieldVal != '') {
		var firstChar = '';
		var numberVal = '';
		
		// validate the FDA filed.
		if (fieldVal.length != 7) {
				fdaSubReqFiledStr = fdaSubReqFiledStr +'<b>'+fieldNm +'</b>, ';
			}else{
				firstChar = fieldVal.substring(0, 1);
				numberVal = fieldVal.substring(1, fieldVal.length);
				if (firstChar != 'P') {
					fdaValidateStr = fdaValidateStr +'<b>'+fieldNm +'</b>, ';
				}
				if (numberVal != '') {
					if (isNaN(numberVal)) {
						fdaSubNumStr = fdaSubNumStr +'<b>'+fieldNm +'</b>, ';
					}
				}
			}
		
	}
	
}
function fnValidateFdaSubNum(frm){
	var strFdaSubNo1 = TRIM(frm.txt_Fda_sub_no1.value);
	var strFdaSubNo2 = TRIM(frm.txt_Fda_sub_no2.value);
	var strFdaSubNo3 = TRIM(frm.txt_Fda_sub_no3.value);
	var strFdaSubNo4 = TRIM(frm.txt_Fda_sub_no4.value);
	var strFdaSubNo5 = TRIM(frm.txt_Fda_sub_no5.value);
	var strFdaSubNo6 = TRIM(frm.txt_Fda_sub_no6.value);
	var strFdaSubNo7 = TRIM(frm.txt_Fda_sub_no7.value);
	var strFdaSubNo8 = TRIM(frm.txt_Fda_sub_no8.value);
	var strFdaSubNo9 = TRIM(frm.txt_Fda_sub_no9.value);
	var strFdaSubNo10 = TRIM(frm.txt_Fda_sub_no10.value);
	var strUSRegPathWay = document.all.cbo_Us_Reg_Pathway.value;
	if(strUSRegPathWay == '104686' || strUSRegPathWay == '104688' || strUSRegPathWay =='104690' ){
	var hBrandName = frm.hBrandName.value;
	if(hBrandName ==''){
		fdaSubSeqErrStr = fnValidateSeq (strFdaSubNo1, strFdaSubNo2, 'FDA Sub. no 1', fdaSubSeqErrStr );
		fdaSubSeqErrStr = fnValidateSeq (strFdaSubNo2, strFdaSubNo3, 'FDA Sub. no 2', fdaSubSeqErrStr );
		fdaSubSeqErrStr = fnValidateSeq (strFdaSubNo3, strFdaSubNo4, 'FDA Sub. no 3',fdaSubSeqErrStr);
		fdaSubSeqErrStr = fnValidateSeq (strFdaSubNo4, strFdaSubNo5, 'FDA Sub. no 4',fdaSubSeqErrStr);
		fdaSubSeqErrStr = fnValidateSeq (strFdaSubNo5, strFdaSubNo6, 'FDA Sub. no 5',fdaSubSeqErrStr);
		fdaSubSeqErrStr = fnValidateSeq (strFdaSubNo6, strFdaSubNo7, 'FDA Sub. no 6',fdaSubSeqErrStr);
		fdaSubSeqErrStr = fnValidateSeq (strFdaSubNo7, strFdaSubNo8, 'FDA Sub. no 7',fdaSubSeqErrStr);
		fdaSubSeqErrStr = fnValidateSeq (strFdaSubNo8, strFdaSubNo9, 'FDA Sub. no 8',fdaSubSeqErrStr);
		fdaSubSeqErrStr = fnValidateSeq (strFdaSubNo9, strFdaSubNo10, 'FDA Sub. no 9',fdaSubSeqErrStr);
	}
	fnCheckCharNumField(strFdaSubNo1, 'FDA Sub. no 1', 'FDA');
	fnCheckCharNumField(strFdaSubNo2,'FDA Sub. no 2', 'FDA');
	fnCheckCharNumField(strFdaSubNo3,'FDA Sub. no 3', 'FDA');
	fnCheckCharNumField(strFdaSubNo4,'FDA Sub. no 4', 'FDA');
	fnCheckCharNumField(strFdaSubNo5,'FDA Sub. no 5', 'FDA');
	fnCheckCharNumField(strFdaSubNo6,'FDA Sub. no 6', 'FDA');
	fnCheckCharNumField(strFdaSubNo7,'FDA Sub. no 7', 'FDA');
	fnCheckCharNumField(strFdaSubNo8,'FDA Sub. no 8', 'FDA');
	fnCheckCharNumField(strFdaSubNo9,'FDA Sub. no 9', 'FDA');
	fnCheckCharNumField(strFdaSubNo10,'FDA Sub. no 10', 'FDA');
  }
}

function fnValidatePmaSupNum(frm){
	var strPmaSupNo1 = TRIM(frm.txt_Pma_Sup_no1.value);
	var strPmaSupNo2 = TRIM(frm.txt_Pma_Sup_no2.value);
	var strPmaSupNo3 = TRIM(frm.txt_Pma_Sup_no3.value);
	var strPmaSupNo4 = TRIM(frm.txt_Pma_Sup_no4.value);
	var strPmaSupNo5 = TRIM(frm.txt_Pma_Sup_no5.value);
	var strPmaSupNo6 = TRIM(frm.txt_Pma_Sup_no6.value);
	var strPmaSupNo7 = TRIM(frm.txt_Pma_Sup_no7.value);
	var strPmaSupNo8 = TRIM(frm.txt_Pma_Sup_no8.value);
	var strPmaSupNo9 = TRIM(frm.txt_Pma_Sup_no9.value);
	var strPmaSupNo10 = TRIM(frm.txt_Pma_Sup_no10.value);
	
	var hBrandName = frm.hBrandName.value;
	if(hBrandName ==''){
		pmaSupSeqErrStr = fnValidateSeq (strPmaSupNo1, strPmaSupNo2, 'PMA Sup. no 1',pmaSupSeqErrStr);
		pmaSupSeqErrStr = fnValidateSeq (strPmaSupNo2, strPmaSupNo3, 'PMA Sup. no 2',pmaSupSeqErrStr);
		pmaSupSeqErrStr = fnValidateSeq (strPmaSupNo3, strPmaSupNo4, 'PMA Sup. no 3',pmaSupSeqErrStr);
		pmaSupSeqErrStr = fnValidateSeq (strPmaSupNo4, strPmaSupNo5, 'PMA Sup. no 4',pmaSupSeqErrStr);
		pmaSupSeqErrStr = fnValidateSeq (strPmaSupNo5, strPmaSupNo6, 'PMA Sup. no 5',pmaSupSeqErrStr);
		pmaSupSeqErrStr = fnValidateSeq (strPmaSupNo6, strPmaSupNo7, 'PMA Sup. no 6',pmaSupSeqErrStr);
		pmaSupSeqErrStr = fnValidateSeq (strPmaSupNo7, strPmaSupNo8, 'PMA Sup. no 7',pmaSupSeqErrStr);
		pmaSupSeqErrStr = fnValidateSeq (strPmaSupNo8, strPmaSupNo9, 'PMA Sup. no 8',pmaSupSeqErrStr);
		pmaSupSeqErrStr = fnValidateSeq (strPmaSupNo9, strPmaSupNo10, 'PMA Sup. no 9',pmaSupSeqErrStr);
	}
	fnCheckCharNumField(strPmaSupNo1, 'PMA Sup. no 1', 'PMA');
	fnCheckCharNumField(strPmaSupNo2,'PMA Sup. no 2', 'PMA');
	fnCheckCharNumField(strPmaSupNo3,'PMA Sup. no 3', 'PMA');
	fnCheckCharNumField(strPmaSupNo4,'PMA Sup. no 4', 'PMA');
	fnCheckCharNumField(strPmaSupNo5,'PMA Sup. no 5', 'PMA');
	fnCheckCharNumField(strPmaSupNo6,'PMA Sup. no 6', 'PMA');
	fnCheckCharNumField(strPmaSupNo7,'PMA Sup. no 7', 'PMA');
	fnCheckCharNumField(strPmaSupNo8,'PMA Sup. no 8', 'PMA');
	fnCheckCharNumField(strPmaSupNo9,'PMA Sup. no 9', 'PMA');
	fnCheckCharNumField(strPmaSupNo10,'PMA Sup. no 10', 'PMA');
}

function fnValidateFdaProdCode (form){
	var strFdaProdCode1 = TRIM(form.txt_Fda_Prod_Code1.value);
	var strFdaProdCode2 = TRIM(form.txt_Fda_Prod_Code2.value);
	var strFdaProdCode3 = TRIM(form.txt_Fda_Prod_Code3.value);
	var strFdaProdCode4 = TRIM(form.txt_Fda_Prod_Code4.value);
	var strFdaProdCode5 = TRIM(form.txt_Fda_Prod_Code5.value);
	var strFdaProdCode6 = TRIM(form.txt_Fda_Prod_Code6.value);
	var strFdaProdCode7 = TRIM(form.txt_Fda_Prod_Code7.value);
	var strFdaProdCode8 = TRIM(form.txt_Fda_Prod_Code8.value);
	var strFdaProdCode9 = TRIM(form.txt_Fda_Prod_Code9.value);
	var strFdaProdCode10 = TRIM(form.txt_Fda_Prod_Code10.value);
	//
	fnCheckCharNumField(strFdaProdCode1, 'FDA Prod. Code 1', 'FDACODE');
	fnCheckCharNumField(strFdaProdCode2,'FDA Prod. Code 2', 'FDACODE');
	fnCheckCharNumField(strFdaProdCode3,'FDA Prod. Code 3', 'FDACODE');
	fnCheckCharNumField(strFdaProdCode4,'FDA Prod. Code 4', 'FDACODE');
	fnCheckCharNumField(strFdaProdCode5,'FDA Prod. Code 5', 'FDACODE');
	fnCheckCharNumField(strFdaProdCode6,'FDA Prod. Code 6', 'FDACODE');
	fnCheckCharNumField(strFdaProdCode7,'FDA Prod. Code 7', 'FDACODE');
	fnCheckCharNumField(strFdaProdCode8,'FDA Prod. Code 8', 'FDACODE');
	fnCheckCharNumField(strFdaProdCode9,'FDA Prod. Code 9', 'FDACODE');
	fnCheckCharNumField(strFdaProdCode10,'FDA Prod. Code 10', 'FDACODE');
}
function fnValidateSeq(firstFieldNo, secondFieldNo, fieldNm, errorStr){
	if(firstFieldNo =='' && secondFieldNo !=''){
		errorStr = errorStr + '<b>'+fieldNm +'</b>, ';
	}
	return errorStr;
}

function fnCheckCharNumField(fieldVal, fieldNm, type) {
	if (fieldVal != '') {
		var firstChar = '';
		var numberVal = '';
		var strUSRegPathWay = document.all.cbo_Us_Reg_Pathway.value;
		// validate the FDA filed.
		if (type == 'FDA') {
			if (fieldVal.length != 7) {
				fdaSubReqFiledStr = fdaSubReqFiledStr +'<b>'+fieldNm +'</b>, ';
			} else {
				if(strUSRegPathWay == '104686' || strUSRegPathWay == '104688' || strUSRegPathWay == '104690'){
					firstChar = fieldVal.substring(0, 1);
					numberVal = fieldVal.substring(1, fieldVal.length);
					if (firstChar != 'K' && firstChar != 'G' && firstChar != 'P') {
						fdaSubCharStr = fdaSubCharStr +'<b>'+fieldNm +'</b>, ';
					}
					if (numberVal != '') {
						if (isNaN(numberVal)) {
							fdaSubNumStr = fdaSubNumStr +'<b>'+fieldNm +'</b>, ';
						}
					}
				}
			}
		} else if (type == 'PMA') {
			if (fieldVal.length != 3) {
				pmaSupReqFiledStr = pmaSupReqFiledStr +'<b>'+fieldNm +'</b>, ';
			} else {
				firstChar = fieldVal.substring(0, 1);
				numberVal = fieldVal;
				if (numberVal != '') {
					if (isNaN(numberVal)) {
						pmaSupNumStr = pmaSupNumStr +'<b>'+fieldNm +'</b>, ';
					}
				}
			}
		}else if(type =='FDACODE'){
			if (fieldVal.length != 3) {
				fdaProdReqFieldStr = fdaProdReqFieldStr +'<b>'+fieldNm +'</b>, ';
			} else {
				var fdaCodeVal = fieldVal.replace(/^[a-zA-Z]*$/,''); 
				if(fdaCodeVal.length >0){
					fdaProdCharStr = fdaProdCharStr +'<b>'+fieldNm +'</b>, ';
				}
			}
		}
	}
}
function fnAddErrorDetails(errorMessage, errorPartStr) {
	if (errorPartStr != '') {
		errorPartStr = errorPartStr.substring(0, (errorPartStr.length) - 2);
		Error_Details(errorMessage + errorPartStr + '</b>');
	}
}
