
function fnReload(){
	var frm = document.frmCapaListReport;
	var fromDate = frm.submitdDtFrom.value;
	var toDate = frm.submitdDtTo.value;
	var elapsedDys = frm.elapsedDys.value;
	var elapsedDysVal = frm.elapsedDysVal.value;
	var dateDiffs = dateDiff( toDate,fromDate, format);
	document.frmCapaListReport.hCapaID.value = '';
	// To check whether the date is in valid format 
	CommonDateValidation(document.frmCapaListReport.submitdDtFrom,format,Error_Details_Trans(message_prodmgmnt[148],format));
	CommonDateValidation(document.frmCapaListReport.submitdDtTo,format,Error_Details_Trans(message_prodmgmnt[149],format));
	
	
	if(elapsedDys == '0' && elapsedDysVal != ''){
		Error_Details(message_prodmgmnt[150]);
	}else if(elapsedDys != '0' && elapsedDysVal == ''){
		Error_Details(message_prodmgmnt[151]);
	}else if(elapsedDysVal != '' && elapsedDys != '0'){
		fnValidateNumber('elapsedDysVal',lblelapseddays);
	}
	
	if(fromDate != '' && toDate == ''){
		Error_Details(message_prodmgmnt[152]);
	}else if(fromDate != '' && toDate == '' ){
		Error_Details(message_prodmgmnt[153]);
	}else if (fromDate != '' && toDate != '' && dateDiffs>0){
		Error_Details(message_prodmgmnt[154]);
	}
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	document.frmCapaListReport.strOpt.value = "Report";
	frm.Btn_Load.disabled=true; // To disable the Load button
	fnStartProgress();
	document.frmCapaListReport.submit();

}

// This function is used to initiate grid
var gridObj;
function fnOnPageLoad()
{ 
	if(objGridData.value != ''){ 
		gridObj = initGrid('dataGridDiv',objGridData);
	}	
}

//This function is used to activate Load button while pressing the enter key
function fnEnter() {
	if(event.keyCode == 13){
		fnReload();
	}
}

// To set the Selected capaId from grid to jsp
function fnSetCapaId(obj, statusId){ 
	document.frmCapaListReport.hCapaID.value = obj;
	document.frmCapaListReport.hStatusId.value = statusId;
}

// To open window to add/view comments
function fnOpenOrdLog(strCapaId){
	windowOpener("/GmCommonLogServlet?hType=4000317&hID=" +strCapaId,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
}

// Function to navigate to other pages while choosing an option from drop down
function fnChooseAction(){
	var frm = document.frmCapaListReport;
	var strChooseActionId = eval(frm.chooseAction.value);
	var capaId = frm.hCapaID.value;
	var statusId = frm.hStatusId.value;
	
	if (capaId == '' && strChooseActionId != '0') {
		Error_Details(message_prodmgmnt[155]);
	}
	if ((capaId != '') && (strChooseActionId == '0')) {
		Error_Details(message_prodmgmnt[156]);
	}
	
	if(capaId != '' && strChooseActionId != '0'){
				
		if(strChooseActionId == '102765'){ // Navigate to CAPA initiation screen
			document.frmCapaListReport.action = "/gmCapaSetup.do?method=editCapa&strOpt=Fetch&strCapaId="+capaId;
		}
		if(strChooseActionId == '102767'){ // Navigate to Upload Screen
			document.frmCapaListReport.action = "/gmUploadAction.do?strOpt=Fetch&refType=102788&logType=4000317&strRefID="+capaId;
		}
		if(strChooseActionId == '102768'){ // Navigate to Common Cancel Screen
			if(statusId == '102769'){// if the status is open
				document.frmCapaListReport.action = "/GmCommonCancelServlet?hTxnName="+capaId+"&hCancelType=VDCAPA&hTxnId="+capaId+"&hAction=Load";
				document.frmCapaListReport.hDisplayNm.value = 'Modify CAPA';
				document.frmCapaListReport.hRedirectURL.value = '/gmCapaListReport.do?method=listCapaDetails&strOpt=Report&accessFl=true&capaID='+capaId;
			}
			else{
				Error_Details(message_prodmgmnt[157]);
			}
		}
	}else if(capaId == '' && strChooseActionId == 0){
		Error_Details(message_prodmgmnt[158]);
		Error_Details(message_prodmgmnt[159]);
	}
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	frm.Btn_Submit.disabled=true; // To disable the Submit button
	fnStartProgress();
	document.frmCapaListReport.submit();
}

// To hide first two columns of grid while exporting to excel
function fnExportExcel(){
	if(accessFl == "true"){
		gridObj.setColumnHidden(0,true);
		gridObj.setColumnHidden(1,true);
		gridObj.toExcel('/phpapp/excel/generate.php');
		gridObj.setColumnHidden(0,false);
		gridObj.setColumnHidden(1,false);
	}else{
		gridObj.setColumnHidden(0,true);
		gridObj.toExcel('/phpapp/excel/generate.php');
		gridObj.setColumnHidden(0,false);
	}
}

function initGridData(divRef,gridData){
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);
	gObj.enableTooltips("true,true,true,true,true,true,true");
	gObj.init();
	gObj.loadXMLString(gridData);	
	return gObj;	
}

// To open the summary screen correspoinding to the CAPA Id
function fnOpenSummaryScreen(capaId){
	windowOpener("/gmUploadAction.do?strOpt=Summary&refType=102788&strRefID="+capaId,"","resizable=yes,scrollbars=yes,top=250,left=300,width=1010,height=600");
}