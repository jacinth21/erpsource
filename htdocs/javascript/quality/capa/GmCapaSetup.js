function fnAssignedToNames() {
	var values = [];
	var combo = document.getElementById("AssignedToCombo");
	var items = document.getElementById("AssingedNameCombo");
	
	if(items.value == ''){
		Error_Details(message_prodmgmnt[160]);
	}
	if (ErrorCount > 0){
		Error_Show();
	    Error_Clear();
		return false;
	}
	if(items.options.length >= 0){
		for (var i = 0; i < items.options.length; i++) {
			if (items.options[i].selected) {
				addOption(combo,items.options[i].value,items.options[i].text,"AssignedToCombo");
			}
		}
	}
	
} 

function fnAssignedToNamesToDB() {
	var values = '';
	var items = document.getElementById("AssignedToCombo");
	if(items.options.length > 0){
		for (var i = 0; i < items.options.length; i++) {
			values = values + items.options[i].value+"|";
		}
	}
	return values;
} 

function fnRemoveNames(){
	var strAssignedNames = document.getElementById("AssignedToCombo");
	if(strAssignedNames.value == ''){
		Error_Details(message_prodmgmnt[317]);
	}
	if (ErrorCount > 0){
		Error_Show();
	    Error_Clear();
		return false;
	}
	if(strAssignedNames.options.length > 0){
		for (var i = strAssignedNames.options.length; i >=0; i--) {
			strAssignedNames.remove(strAssignedNames.selectedIndex);
		}
	}
}

//Add the name and values in drop down when selected names.
function addOption(selectbox, value, text,strComboNm){
	var optn = document.createElement("OPTION");
	optn.text = text;
	optn.value = value;
	if(selectbox.options.length > 0 && strComboNm == 'AssignedToCombo'){
		for (var i = 0; i < selectbox.options.length; i++) {
			if(selectbox.options[i].value == optn.value){
				alert("\""+TRIM(optn.text)+"\" is already added.");
				selectbox.remove(selectbox.selectedIndex);
			}
		}
	}
    selectbox.options.add(optn);
}

//Product Development team names will display the left drop down when page load.
function fnAddAssignedName(){
	var combo = document.getElementById("AssingedNameCombo");
	if(FormVldArrLen > 0)
		for(i=0 ;i<FormVldArrLen;i++){
			FormVldArr = eval("FormVldArr"+i);
			FormVldArrObj = FormVldArr.split("|");
			addOption(combo,FormVldArrObj[0],FormVldArrObj[1]);
 		}
}

//Which we have saved name in DB and will display the right drop down when page load.
function fnAddAssignedToName(){
	var combo = document.getElementById("AssignedToCombo");
	if(FormAssignedToLen > 0)
		for(i=0 ;i<FormAssignedToLen;i++){
			FormVldArr = eval("FormAssignedTo"+i);
			FormVldArrObj = FormVldArr.split("|");
			addOption(combo,FormVldArrObj[0],FormVldArrObj[1]);
 		}
}


function fnSubmit(){ 	
	var dtSubmitted   = document.frmCapaSetup.strCapaSubDt.value;
	var dtResponce    = document.frmCapaSetup.strRespDueDt.value; 	
	var dtClose       = document.frmCapaSetup.strClosedDt.value;
	var strOpt        = document.frmCapaSetup.strOpt.value;
	var strCapaId     = document.frmCapaSetup.strCapaId.value;		
	
	document.frmCapaSetup.strAssignedTo.value = fnAssignedToNamesToDB();	
	
	if(strOpt == "load" && strCapaId != ''){
		Error_Details(message_prodmgmnt[77]);
	}
	
	if(strOpt != "load"){
		if(strCapaId != hCAPAID)
			Error_Details(message_prodmgmnt[77]);
	}
	CommonDateValidation(document.frmCapaSetup.strCapaSubDt,format,Error_Details_Trans(message_prodmgmnt[423],format));
	CommonDateValidation(document.frmCapaSetup.strRespDueDt,format,Error_Details_Trans(message_prodmgmnt[424],format));
	CommonDateValidation(document.frmCapaSetup.strClosedDt,format,Error_Details_Trans(message_prodmgmnt[425],format));
	
	var submitResponceDiff = dateDiff(dtSubmitted,dtResponce , format);
	var submitClosedDiff = dateDiff(dtSubmitted,dtClose , format);
	var submitCurrDiff = dateDiff(dtSubmitted, currdate, format);
	var closedCurrDiff = dateDiff(dtClose, currdate, format);
	
	
	if(dtSubmitted != ''){		
		if(submitCurrDiff < 0){	
			Error_Details(message_prodmgmnt[403]);
		}
	
	}
	if(dtClose != ''){		
		if(closedCurrDiff < 0){			
			Error_Details(message_prodmgmnt[402]);
		}
		
		var varCategory   = document.frmCapaSetup.strCategory.value;
		
		if(varCategory == '' || varCategory == ""){
			
			Error_Details(message_prodmgmnt[485]);
			document.frmCapaSetup.strCategory.focus();		
		}
	}
	if(dtSubmitted != '' && dtResponce != ''){		
		if(submitResponceDiff < 0){			
			Error_Details(message_prodmgmnt[404]);
		}
	}
	
	if(dtSubmitted != '' && dtClose != ''){	
		if(submitClosedDiff < 0){
			Error_Details(message_prodmgmnt[405]);			
		}
	}
	
	fnLenValidation(document.frmCapaSetup.strComplaint,message_prodmgmnt[426],"4000");
	fnLenValidation(document.frmCapaSetup.strRefID,lblReference,"4000");
	fnLenValidation(document.frmCapaSetup.strLotNums,message_prodmgmnt[427],"4000");
	fnLenValidation(document.frmCapaSetup.strProjectID,message_prodmgmnt[428],"4000");
	fnLenValidation(document.frmCapaSetup.strCapaDesc,message_prodmgmnt[429],"4000");
	fnLenValidation(document.frmCapaSetup.strPrevCapas,message_prodmgmnt[430],"4000");
	fnLenValidation(document.frmCapaSetup.strCapaResp,message_prodmgmnt[431],"4000");
	
	if (ErrorCount > 0){
		Error_Show();
	    Error_Clear();
		return false;
	}
	
	confirmMsg =  fnConfirmMessage();
	if(confirmMsg)	{
		document.frmCapaSetup.Btn_Load.disabled = true; // to disable the submit button
		fnStartProgress();
		document.frmCapaSetup.strOpt.value = "save";
		document.frmCapaSetup.action = '/gmCapaSetup.do?method=addCapa'; 
		document.frmCapaSetup.submit();
	}
	else		
		return false;
 }

// To limit the no. of characters entering into the text area
function fnLenValidation(obj,name,len){ 
	
	if(obj.value.length > len){
		var array =[len,name];
		Error_Details(Error_Details_Trans(message_prodmgmnt[414],array));
	}
}

function fnLoad(){
	var capaId = document.frmCapaSetup.strCapaId.value;	
	if(capaId != ''){
		 document.frmCapaSetup.Btn_Load.disabled = true; // to disable the load button
		 fnStartProgress();
		 document.frmCapaSetup.strOpt.value = "Fetch";
		 document.frmCapaSetup.action = '/gmCapaSetup.do?method=editCapa';
		 document.frmCapaSetup.submit();
	}else{
		Error_Details(message_prodmgmnt[171]);
	}	 
	if (ErrorCount > 0){
		Error_Show();
	    Error_Clear();
		return false;
	}
}


function fnReset(){
	
	var strOpt = document.frmCapaSetup.strOpt.value;
	var CapaID = document.frmCapaSetup.strCapaId.value;	
	if(strOpt == "Fetch"){
		confirmMsg =  fnConfirmMessage();
		if(confirmMsg){
				if(CapaID != hCAPAID ){ 
					document.frmCapaSetup.strCapaId.value = hCAPAID;
				}
			if (hCAPAID != ''){
				fnLoad();
			}else{
				document.frmCapaSetup.action = "/gmCapaSetup.do?method=addCapa&strOpt=load";
				document.frmCapaSetup.submit();
			}
		}
	}
	if(strOpt == "load"){
		
		confirmMsg =  fnConfirmMessage();
		if(confirmMsg){
			document.frmCapaSetup.action = "/gmCapaSetup.do?method=addCapa&strOpt=load";
			document.frmCapaSetup.submit();
			}
	}
	
	//alert(document.getElementById("strPrtyLvl").selectedIndex);
	
	document.frmCapaSetup.strCategory.value='';
	document.getElementById("strPrtyLvl").value="0";
	
}

function fnConfirmMessage(){
	confirmMsg = confirm(message_prodmgmnt[172]);
	if(confirmMsg)	return true;
	else 			return false;
} 



function fnShowFilters(val){
	var obj = eval("document.all."+val);
	var obj1 = eval("document.all."+val+"img");
	
	if (obj.style.display == 'none'){
		obj.style.display = 'block';
		if(obj1){
			obj1.src = '/images/minus.gif';
		}
	}
	else{
		obj.style.display = 'none';	
		if(obj1){
			obj1.src = '/images/plus.gif';
		}
	}
}

//Function to navigate to other pages while choosing an option from drop down
function fnChooseAction(){
	var frm = document.frmCapaSetup;
	var strChooseActionId = eval(frm.strCboSelected.value);
	var capaId = frm.strCapaId.value;
	
	if (capaId == '') {
		Error_Details(message_prodmgmnt[407]);
	}
	if ((capaId != '') && (strChooseActionId == '0')) {
		Error_Details(message_prodmgmnt[406]);
	}	
	
	if(capaId != '' && strChooseActionId != '0'){
				
		if(strChooseActionId == '102766'){ // Navigate to CAPA initiation screen
			document.frmCapaSetup.action = "/gmCapaListReport.do?method=listCapaDetails&strOpt=Report&capaID="+capaId+"&accessFl=true";
		}
		if(strChooseActionId == '102767'){ // Navigate to Upload Screen
			document.frmCapaSetup.action = "/gmUploadAction.do?strOpt=Fetch&refType=102788&logType=4000317&strRefID="+capaId;
		}
		if(strChooseActionId == '102768'){// Navigate to Common Cancel Screen		
			
			if(status == "102769"){
				document.frmCapaSetup.action = "/GmCommonCancelServlet?hTxnName="+capaId+"&hCancelType=VDCAPA&hTxnId="+capaId+"&hAction=Load";
				document.frmCapaSetup.hDisplayNm.value = 'CAPA Initiation';
				document.frmCapaSetup.hRedirectURL.value = "/gmCapaSetup.do?method=editCapa&strOpt=Fetch&strCapaId="+capaId;
			}
			else{
					Error_Details(message_prodmgmnt[72]);
				}
		}
		
	}
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	if(frm.Submit) {
		frm.Submit.disabled=true;
	}
	fnStartProgress();
	document.frmCapaSetup.submit();
}

function fnEmail(){
	var capaID = document.frmCapaSetup.strCapaId.value;
	windowOpener('/gmCOMRSRProcess.do?method=closureEmail&strOpt=Load&refID='+capaID+"&refType=102788","ClosureEmail","resizable=yes,scrollbars=yes,top=150,left=200,width=880,height=500");
}

// To set submitted date as system date
function fnOnPageLoad(){
	var strOpt = document.frmCapaSetup.strOpt.value;
	if(strOpt == "load"){
		document.frmCapaSetup.strCapaSubDt.value = currdate;
	}
	
	//When page load, if it is [Choose one] in priority level drop down then default value B.
	if(document.frmCapaSetup.strPrtyLvl.value == "0"){
		document.frmCapaSetup.strPrtyLvl.value = "102773"; //B	
	
	}
	
	if(status == "102770"){
		document.frmCapaSetup.strPrtyLvl.value = "0";
	}
	
	// When page load all the product development team employees will be displayed in the Assigned To drop down.  
	fnAddAssignedName();
	// When submit and it will be loaded assigned names which we have submitted.
	fnAddAssignedToName();
}

// When click print it will download RFT format from jasper.
function fnPrint(){
	var capaId = document.frmCapaSetup.strCapaId.value;
	if(capaId != ''){
		 document.frmCapaSetup.action = '/gmCapaListReport.do?method=printCapaDetails&strOpt=Print&capaID='+capaId;
		 document.frmCapaSetup.submit();
	}else{
		Error_Details(message_prodmgmnt[171]);
	}	 
	if (ErrorCount > 0){
		Error_Show();
	    Error_Clear();
		return false;
	}
}
