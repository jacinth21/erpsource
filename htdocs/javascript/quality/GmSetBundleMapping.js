//Ajax call to get the Status of the SET.
var rowcntval = 0;
var validSetSuc ='';
var validSetErr = '';
function fnGetSetId(setVal,rowcntval){	
	if(setVal!=''){
		dhtmlxAjax.get('/gmSetBundle.do?strOpt=getSetStatus&setId='+setVal +'&ramdomId=' + Math.random(),function(loader){		
			if (loader.xmlDoc.responseText != null) { 
				var response = loader.xmlDoc.responseText;
				fnLoadLoc(response,5,rowcntval);
			}
		});
	}	
}
function fnGetSetIdVal(setVal,rowcntval,setSucess){	
	if(setVal!=''){
		dhtmlxAjax.get('/gmSetBundle.do?strOpt=getSetStatus&setId='+setVal +'&ramdomId=' + Math.random(),function(loader){		
			if (loader.xmlDoc.responseText != null) { 
				var response = loader.xmlDoc.responseText;
				fnLoadLoc(response,5,rowcntval);				
				document.getElementById("validSetSuc0").style.display = "none";
			}
		});
	}	
}

function fnLoadLoc(response,cellInd,rowcntval){	
	   cnt = document.all.hRowCnt.value;
	   var mySplitResult = response.split("^");
	   validSetSuc = eval("document.all.validSetSuc"+rowcntval);	
  	   validSetErr = eval("document.all.validSetErr"+rowcntval);
	   		if(response!=null && response!=''){ 
	   			setId = mySplitResult[0]; 
	   			setDesc = mySplitResult[1];
	   			setType = mySplitResult[2];
	   			seType = mySplitResult[3];
	   			setStatus = mySplitResult[4];
			   
		   		if(setStatus == 'Approved'){
		   			validSetErr.style.display = 'none';
		   			validSetSuc.style.display = 'inline';
		   			document.getElementById('setDesc'+rowcntval).innerHTML =setDesc;
		   			document.getElementById('setType'+rowcntval).innerHTML =setType;
		   			document.getElementById('seType'+rowcntval).innerHTML =seType;
		   			document.getElementById('setStatus'+rowcntval).innerHTML =setStatus;
		   			document.getElementsByName('setId')[0].value =setId;
		   		}else if(setStatus != 'Approved' || setStatus ==''){  
		   			validSetErr.style.display = 'inline';
		   			validSetSuc.style.display = 'none';
		   		}
	   		} else{
	   			var setid = eval("document.all.setId"+rowcntval);
	   			Error_Details("<b>"+setid.value+message_prodmgmnt[342]);
				document.getElementsByName('setId').value = '';
				var obj = eval("document.all.setId"+rowcntval); 
			    obj.value = '';	
				validSetErr.style.display = 'none';
				validSetSuc.style.display = 'none';		
	   		}
	   		if(ErrorCount > 0){
				Error_Show();
				Error_Clear();
				return false;
		}
   		
}


//Baseline validation function calling from Set Type, BaseLine DropDown. 
function fnBaselineValidate(rowId){
	var baseLineObj = eval("document.all.setBaseLine"+rowId); 
	var baseLineValue = baseLineObj.value ;
	if(baseLineValue=='Y'){
		var  setTypeobj = eval("document.all.setType"+rowId); 
		var  setTypeVal = setTypeobj.value ;
		if(setTypeVal!='0'){
			if(setTypeVal!='103148' && setTypeVal!='103147'){
					Error_Details(message_prodmgmnt[106]);
					baseLineObj.value = 0;
					Error_Show();
					Error_Clear();
					return false;
			}
		}
	}
}


var cnt=0;

//To add more rows.
function fnAddRow(form){
	if(form){
		cnt = document.all.hRowCnt.value;
		
	};
	cnt++;

    var tbody = document.getElementById("SystemMapping").getElementsByTagName("TBODY")[0];
    var row = document.createElement("TR")
 
	var td0 = document.createElement("TD")
    td0.innerHTML = '<a href=javascript:fnRemoveItem('+cnt+'); tabindex=\'-1\'><img border=0  Alt=Remove  title=Remove  valign=left src=/images/btn_remove.gif height=10 width=9></a>&nbsp;';
	td0.align = "right";
	
    var td1 = document.createElement("TD")
    td1.innerHTML = '<input type=text size=10 name=setId'+cnt+' class=InputArea onBlur= javascript:fnGetSetId(this.value,'+cnt+'); > <input type=hidden name=hStatus'+cnt+' value="">';
	td1.align = "left";
	
	var td2 = document.createElement("TD")
    td2.innerHTML = '<span id=validSetSuc'+cnt+' style=\'vertical-align:top; display: none;\'> <img tabindex=\'-1\' height=16 width=19  align=left title=\'Set available to Map\' src=/images/success.gif></img></span><span id=validSetErr'+cnt+' style=\'vertical-align:top; display: none;\'><img tabindex=\'-1\' height=13 title=\'Set doesnot available to Map\' width=13 src=/images/error.gif></img></span>';
		
	var td3 = document.createElement("TD")
    td3.innerHTML = '<span id=setDesc'+cnt+' javascript:fnGetSetId(this.value,'+cnt+');> <input type=hidden name=hStatus'+cnt+' value="">';
	
	var td4 = document.createElement("TD")
    td4.innerHTML =  '<span id=setType'+cnt+' javascript:fnGetSetId(this.value,'+cnt+');> <input type=hidden name=hStatus'+cnt+' value="">';

	var td5 = document.createElement("TD")
    td5.innerHTML =  '<span id=seType'+cnt+' javascript:fnGetSetId(this.value,'+cnt+');> <input type=hidden name=hStatus'+cnt+' value="">';
	
	var td6 = document.createElement("TD")
    td6.innerHTML =  '<span id=setStatus'+cnt+' javascript:fnGetSetId(this.value,'+cnt+');><input type=hidden name=hStatus'+cnt+' value="">';
	row.appendChild(td0);
	row.appendChild(td1);
	row.appendChild(td2);
	row.appendChild(td3);
    row.appendChild(td4);
    row.appendChild(td5);
    row.appendChild(td6);
    tbody.appendChild(row);

	document.all.hRowCnt.value = cnt;
}

//onload function to load the Grid data.
function fnOnloadSystemMapping(){	

	var divElement = document.getElementById('setMapDtlsData');
	var frm = '';
	var mygrid = '';
	if(document.frmSetBundleMappingForm){
		frm = document.frmSetBundleMappingForm;
	}else{
		frm = document.all;
	}
	var gridObjData = '';
	if(frm.gridData){
		gridObjData = frm.gridData.value;
	}
	var intSize = 0;
	if(frm.gridData){
		intSize = frm.setMapDtlssize.value;
	}

	if (intSize==0)	{
		divElement.align = 'center';
		divElement.innerHTML='<font color=blue>'+message_prodmgmnt[343]+'</font>';
	}
	else if(gridObjData!=''){
		mygrid = initGridData('setMapDtlsData',gridObjData);
	}
}

function initGridData(divRef,gridData){
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);
	gObj.enableMultiline(true);
	gObj.attachHeader('#rspan,#text_filter,#text_filter,#text_filter,#select_filter,#select_filter,#select_filter');
	gObj.init();	
	gObj.loadXMLString(gridData);	
	return gObj;	
}

//When edit icon is clicked, below function is called and this will assign the values to first row.
//And remove all other rows, Add rows Hyperlink also.
function fnEditSystem(setId){

	var rowCnt = document.all.hRowCnt.value;
	var obj = eval("document.all.setId0"); 
	obj.value = setId;
	obj.disabled=true;
	fnGetSetIdVal(setId,0,1);
	var divElement='';
	divElement = document.getElementById('btn_void');
	if(divElement!='' && divElement!=null){
		divElement.disabled=false;
	}
	divElement = document.getElementById('addrow');
	divElement.style.display='none';

	var table = document.getElementById("SystemMapping");
	for ( var i = 4, row; row = table.rows[i]; i++) { 
		row.style.display='none';
	}
	document.all.hRowCnt.value ='1';	
	document.all.haction.value ='edit';
	//To clear all the values form the cart, while clicking on Edit icon, except the first row
	for(var i=1;i<=rowCnt; i++){
		fnRemoveItem(i);	
	}
}

//Fuction to reset the form
function fnResetMapping(form){
	var rowCnt = form.hRowCnt.value;
	for(var i=0;i<=rowCnt; i++){
		fnRemoveItem(i);	
	}
}

//When we click on Edit icon that time only we are displaying Void button.So during VOID, always take the first row set id from the screen.
function fnVoidMapping(form){	
		var setId ='';
		if(eval("form.setId0"))	{
			setId = eval("form.setId0").value ;
		}
		var systemId= form.systemId.value;
		var txnId = systemId+','+setId;
		if(setId!=''){
			form.action = "/GmCommonCancelServlet?hTxnName="+setId+"&hCancelType=VSBMAP&hCancelReturnVal=true&hTxnId="+txnId;
			form.submit();
		}else{
			Error_Details(message_prodmgmnt[473]);
		}
		if (ErrorCount > 0)
	    {
	             Error_Show();
	             Error_Clear();
	             return false;
	    }
}
//Submit function. form the four input string based on SET TYPE selected.
function fnSubmitMapping(form){
		var rowCnt = form.hRowCnt.value;
		var mappedSetSize = form.setMapDtlssize.value;		
		var actionFrom = form.haction.value ;
		var setidVal ='';
		var allSetids='';
		var setidArr='';
		var statusErr='';
		var statusError='';
		for(var i=0;i<=rowCnt; i++){				
			var obj = eval("document.all.setId"+i);
			setidVal = TRIM(obj.value) ;
			if(setidVal!=''||setidVal!=allSetids){	
				allSetids = allSetids+setidVal+'|';		
		}
			var status = document.getElementById('setStatus'+i).innerHTML;
			if(status != 'Approved' && setidVal != ''){
				statusErr = statusErr + setidVal+', ';	
			}
		}	
		setidArr  = allSetids.split('|');	
		fnValidateTxtFld('comments',message_prodmgmnt[146]);
		if(setidArr =="" || setidArr == null){
			Error_Details(message_prodmgmnt[472]);
		}
		if(statusErr != '' ){
			Error_Details(Error_Details_Trans(message_prodmgmnt[110],statusErr));
		}
		if (ErrorCount > 0)
		    {
		             Error_Show();
		             Error_Clear();
		             return false;
		    }
		
		
		form.action = "/gmSetBundle.do?strOpt=save&setId="+setidArr; 
		form.submit();		
}

//This function is to validate the presence of any duplicate sets.
function checkUniqueSets(myArray){
  isUnique=true;
	for (var i = 0; i < myArray.length; i++){
		for (var j = 0; j < myArray.length; j++){
			if (i != j){
				if (myArray[i] == myArray[j]){
					isUnique=false;
				}
			}
		}
	}
	return isUnique;
	
}

//This function is to validate the presence of any duplicate sets.
function checkUniqueSets(myArray){
  isUnique=true;
	for (var i = 0; i < myArray.length; i++){
		for (var j = 0; j < myArray.length; j++){
			if (i != j){
				if (myArray[i] == myArray[j]){
					isUnique=false;
				}
			}
		}
	}
	return isUnique;
}
//To remove passed the line items values
function fnRemoveItem(val){
	var obj = eval("document.all.setId"+val); 
    obj.value = '';	
	if(obj){
	    obj.value = '';
	}
	var divElement = document.getElementById('validSetSuc'+val);
	divElement.style.display='none';
	var divElement = document.getElementById('validSetErr'+val);
	divElement.style.display='none';
	var divElement = document.getElementById('setDesc'+val);
	divElement.innerHTML='';
    var divElement = document.getElementById('setType'+val);
    divElement.innerHTML='';
	var divElement = document.getElementById('seType'+val);
	divElement.innerHTML='';
	var divElement = document.getElementById('setStatus'+val);
	divElement.innerHTML='';
		
}
//Function will call on page load. It will append companyInfo in each tab href and initiate ajaxtab.
function fnAjaxTabLoad(){
	
	fnAjaxTabAppendCmpInfo("#maintab");
	fnMainTab();
}

//initiate ajaxtab
function fnMainTab(){
	var maintab=new ddajaxtabs("maintab", "ajaxdivcontentarea");				
	maintab.setpersist(false);								
	maintab.init();
	
	maintab.onajaxpageload=function(pageurl){
		if (pageurl.indexOf("gmSetBundleMapping.do")!=-1){
			if(document.getElementById("ajaxdivcontentarea") != undefined){
	  			document.getElementById("ajaxdivcontentarea").style.height = "auto";
			}
		}
		if (pageurl.indexOf("gmSetBundle.do")!=-1){
			fnOnloadSystemMapping();
			if(document.getElementById("ajaxdivcontentarea") != undefined){
	  			document.getElementById("ajaxdivcontentarea").style.height = "auto";
			}	
		}
		
		fnHtmlEditor();
	}			
}	

function fnLoadDashDetails(){ 
		document.frmSetBundleRpt.action = "/gmSetBundleMappingReport.do?method=setBundleRpt&hStrOpt=Load";
		fnStartProgress();
		document.frmSetBundleRpt.submit();
}
