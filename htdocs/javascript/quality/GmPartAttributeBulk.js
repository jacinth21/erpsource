var gridObj;

// this function is used to save the Part Attribute Details
function fnSubmitAttribute(){
	
	
	fnValidateDropDn('attributeType',lblAttributeType);
	
	objval = document.getElementById('attributeValue').value;
	
	

	// Validate that entry format is Part Number , Attribute Value (comma separated)
	var strTemp =TRIM(objval);
	
	if(strTemp == '')
	{
		Error_Details(message_prodmgmnt[86]);	
	}	

	//This javascript code removes all 3 types of line breaks
    strTemp = strTemp.replace(/(\r\n|\n|\r)/gm, "|");
	// Get each line and validate
	var arrLines = '';
	arrLines = strTemp != ''? strTemp.split('|'): '';
	// List of valid characters
    var regularexp = /^[A-Za-z.0-9/+,\t\- ]+$/i;

	var error_cnt = 0;
    var comma_cnt = 0;
    var strTestLine = "";
    var negPrice = "";
    var arrPartDet = '';
    var attVal = '';
    var pnum = '';

	for (var i = 0; i < arrLines.length; i++)
	{
	    strTestLine = TRIM(arrLines[i]);
	    arrPartDet = strTestLine.split(','); // Split the part number and attribute value to the array
	    pnum = arrPartDet[0];
	    attVal = arrPartDet[1]; // get Attribute value from the row
        comma_cnt = 0;
        // Check characters that are allowed
		if (regularexp.exec(strTestLine) == null) // Special character found
		{
			// Only include the message once
            if (error_cnt == 0) {
      			Error_Details(message_prodmgmnt[87]);
			}
			error_cnt++;
			// Include the error line
            Error_Details(strTestLine);
			

		}

		// Check required number of commas per line (must have 1 and only 1)
        comma_cnt = (strTestLine.split(",").length - 1);
		if (comma_cnt != 1 || attVal == '' || pnum == '')
		{
			// Only include the message once
            if (error_cnt == 0) {
      			Error_Details(message_prodmgmnt[88]);
			}
			error_cnt++;
			// Include the error line
			Error_Details(strTestLine);
		}
	}
	if(arrLines.length>10000)
		{
		Error_Details(message_prodmgmnt[90]);
		}
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	
	
	document.frmPartAttributeBulkSetup.strOpt.value = "save";
	document.frmPartAttributeBulkSetup.screenType.value = "Setup";
	document.frmPartAttributeBulkSetup.action = "/gmpartAttrBlkSetup.do?method=savePartAttribute";
	
	fnStartProgress();
	document.frmPartAttributeBulkSetup.submit();

}


// this function is used to load the Part Attribute Details in report screen
function fnReload(){
	
	
	var attTypeval=document.frmPartAttributeBulkSetup.attributeType.value;
	var setIdval= document.frmPartAttributeBulkSetup.setId.value;
	var PartValue=document.frmPartAttributeBulkSetup.partNums.value;
	
	if((attTypeval==0) && (setIdval==0) && (PartValue == '' || PartValue==undefined))
		{
		
		Error_Details(message_prodmgmnt[91]);
		
		}

	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
		
	document.frmPartAttributeBulkSetup.strOpt.value="Reload";
	document.frmPartAttributeBulkSetup.screenType.value="Report";
	document.frmPartAttributeBulkSetup.action = "/gmpartAttrBlkSetup.do?method=loadPartAttribute";
	
	
	fnStartProgress();
	document.frmPartAttributeBulkSetup.submit();
}

//this function used to on page load to show the grid in setup screen	
function fnOnPageLoadSetup(){
		
		var partAttrType = document.getElementById('attributeType').value;
		var allCompanyFl = JSON.parse(jsonCompFl);
		
		
		if(allCompanyFl[partAttrType] == 'Y'){
			
			
			document.getElementById('appforAllCmp').style.display='inline-block';	
			document.frmPartAttributeBulkSetup.chkAllcompany.value = 'on';
		}else{
			
				document.getElementById('appforAllCmp').style.display='none';
				document.frmPartAttributeBulkSetup.chkAllcompany.value = ' ';
			
		}
		
		
	if(objGridData.value !=''){ 
	gridObj = initGridData('dataGridDiv',objGridData);
	}	
	
}

//this function used to on page load to show the grid in report screen
function fnOnPageLoadRpt(){
		
	
if(objGridData.value !=''){ 
gridObj = initGridData('dataGridDiv',objGridData);
}	

}

//this function is used to initiate grid
function initGridData(divRef,objGridData)
	{
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.attachHeader("#text_filter,#text_filter,#text_filter,#select_filter,#select_filter,#select_filter");
	gObj.init();
	gObj.loadXMLString(objGridData);	
	return gObj;
	}


//this function is used to reset the screen.
function fnReset(){
	document.frmPartAttributeBulkSetup.attributeType.value=0;
	document.getElementById('attributeValue').value="";
	document.frmPartAttributeBulkSetup.chkAllcompany.checked=false;
}

//this function is used to export excel
function fnExport()
{	
		var system_id = gridObj.getColIndexById("system");
		gridObj.setColumnHidden(system_id,false);
		gridObj.toExcel('/phpapp/excel/generate.php');
		gridObj.setColumnHidden(system_id,true);

}

//this function is used to show Applicable for all companies label based on attribute type
function fnSetCompanyFlagSetup(){
	
	var partAttrType = document.getElementById('attributeType').value;
	var allCompanyFl = JSON.parse(jsonCompFl);
	
	if(allCompanyFl[partAttrType] == 'Y'){
		
		document.getElementById('appforAllCmp').style.display='inline-block';	
		document.frmPartAttributeBulkSetup.chkAllcompany.value = 'on';
	}else{
		
		document.getElementById('appforAllCmp').style.display='none';
		document.frmPartAttributeBulkSetup.chkAllcompany.value = '';
	}
	

}
//this function is used to check or uncheck the Applicable for all companies checkbox based on attribute type
function fnSetCompanyFlagRpt(){
	
	var partAttrType = document.getElementById('attributeType').value;
	var allCompanyFl = JSON.parse(jsonCompFl);
	
	if(allCompanyFl[partAttrType] == 'Y'){
		
		document.frmPartAttributeBulkSetup.chkAllcompany.checked = true;

	}else{
		
		document.frmPartAttributeBulkSetup.chkAllcompany.checked = false;
	}
	

}

//this function is used to show the number of entered lines in Enter Data field
function fnOnBlurEnterData(){
	
	objval = document.getElementById('attributeValue').value;
	var strTemp =TRIM(objval);
	
    strTemp = strTemp.replace(/(\r\n|\n|\r)/gm, "|");
	
	var arrLines = '';
	arrLines = strTemp != ''? strTemp.split('|'): '';
	document.getElementById('NoOfcnt').style.display='inline-block';
	document.getElementById('NoOfcnt').innerHTML = 'No.of.lines : '+ arrLines.length;
}

function fnVoid(){
	
	
	fnValidateDropDn('attributeType',message_prodmgmnt[421]);
	var attributeType = document.frmPartAttributeBulkSetup.attributeType.value;
	objval = document.getElementById('attributeValue').value;
	
	var strTemp =TRIM(objval);
	
	if(strTemp == '')
	{
		Error_Details(message_prodmgmnt[93]);	
	}	
	var partsToVoid = replaceAll(strTemp,"\n", ",");
	var partsNum = replaceAll(strTemp,"\n", ", ");
	if(strTemp.length > 2000)
		{
		Error_Details(message_prodmgmnt[92]);	
		}
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	var cancelType = 'VDPTAT';
	partsToVoid = strTemp+'|'+attributeType;
	document.frmPartAttributeBulkSetup.action="/GmCommonCancelServlet?hTxnName="+partsNum+"&hCancelType="+cancelType+"&hTxnId="+partsToVoid;
	document.frmPartAttributeBulkSetup.submit();
}




