/**
 * File Name: GmIssuingAgencyConfig.js
 * 
 */
// this function used to on page load to set the configure short name
function fnOnLoad() {
	var strOptVal = document.frmIssuingAgencyConfig.strOpt.value;
	var configNameVal = document.frmIssuingAgencyConfig.configName.value;
	var issuingVal = document.frmIssuingAgencyConfig.issuingAgency.value;
	if (strOptVal != '' && configNameVal != 0) {
		count = savedIdentLen;
		document.frmIssuingAgencyConfig.issuingAgency.disabled = true;
		fnFormDropDownVal(issuingVal);
		var issueAgnecyShortNm = get(issuingVal + 'UDI');
		document.getElementById("shortName").innerHTML = "&nbsp;"+ issueAgnecyShortNm + "&nbsp;";
		document.getElementById("shortName").style.display = 'inline';
	}
	// if configure name voided then set values are empty
	if(configNameVal ==0){
		document.frmIssuingAgencyConfig.issuingAgency.value = 0;
		document.frmIssuingAgencyConfig.txt_configName.value = '';
	}
}
// this function used to reload the page when change the configuration drop down
function fnChangeConfig() {
	var configName = document.frmIssuingAgencyConfig.configName.value;
	var actionVal = "Reload";
	if (configName == 0) {
		document.frmIssuingAgencyConfig.txt_configName.value = '';
		actionVal = "";
	}
	document.frmIssuingAgencyConfig.strOpt.value = actionVal;
	fnStartProgress("Y");
	document.frmIssuingAgencyConfig.submit();
}
// this function used to update the identifier drop down values
function fnChangeIssuAgency(objIssuingAgency) {
	var issuingVal = objIssuingAgency.value;
	// to set the global value as empty
	firstIdenDiNumber = '';
	// document.frmIssuingAgencyConfig.submit();
	var issueAgnecyShortNm = get(issuingVal + 'UDI');
	document.getElementById("shortName").innerHTML = "&nbsp;"+ issueAgnecyShortNm + "&nbsp;";
	document.getElementById("shortName").style.display = 'inline';
	fnFormDropDownVal(issuingVal);
	fnUpdateDropDownVal(issuingVal);
}
// this function used to form the drop down filed (identifiers)
function fnFormDropDownVal(issuingVal) {
	dropDownVal = '<option value=0 id=0 >[Choose One]</option>';
	var identifierId = '';
	for ( var i = 0; i < identifierLen; i++) {
		// Array Search
		arr = get(i);
		if (arr != '') {
			arrobj = arr.split("^^");
			if (arrobj.length > 1) {
				issueAgId = arrobj[0];
				issueConfigId = arrobj[1];
				issueConfigNm = arrobj[2];
				identifierId = arrobj[3];
			}
		}
		if (issueAgId == issuingVal) {
			dropDownVal = dropDownVal + '<option  ' + preSelect + ' value='
					+ issueConfigId + '>' + issueConfigNm + '</option>';
			if(identifierId == '103940'){ // 103940 - DI Number
				firstIdenDiNumber = issueConfigId;
			}
		}
		preSelect = '';
	}
}
// this function used to update the all drop down values based on the agency
function fnUpdateDropDownVal(issuingVal) {
	var identifierId = '';
	for ( var j = 0; j < count; j++) {
		var cntVal = 0;
		var identifierObj = eval("document.all.identifier" + j);
		identifierObj.options.length = 0;
		identifierObj.options[0] = new Option("[Choose One]", "0");
		for ( var i = 0; i < identifierLen; i++) {
			// Array Search
			arr = get(i);
			if (arr != '') {
				arrobj = arr.split("^^");
				if (arrobj.length > 1) {
					issueAgId = arrobj[0];
					issueConfigId = arrobj[1];
					issueConfigNm = arrobj[2];
					identifierId = arrobj[3];
				}
			}
			if (issueAgId == issuingVal) {
				if(identifierId == '103940'){ // 103940 - DI Number
					firstIdenDiNumber = issueConfigId;
				}
				identifierObj.options[cntVal + 1] = new Option(issueConfigNm, issueConfigId);
				cntVal++;
			}
			preSelect = '';
		}
	}
}
// this function used to add the new row in configuration setup screen
function fnAddRow() {

	var rowno = count;
	var showRowno = count + 1;
	var TBODY = document.getElementById('tabIssuingAgency')
			.getElementsByTagName("TBODY")[0];
	var row = document.createElement("TR");

	var td0 = document.createElement("TD");
	td0.innerHTML = showRowno + "<input type='hidden' name='hConfigDtlId"+ rowno + "' value=''/>";

	var td7 = document.createElement("TD");
	td7.innerHTML = '<select  name=identifier'
			+ rowno
			+ ' id=identifier'
			+ rowno
			+ ' class=RightText  onFocus=changeBgColor(this,\'#AACCE8\'); onBlur=changeBgColor(this,\'#ffffff\');>'
			+ dropDownVal + '</select>';

	row.height = "30";
	row.appendChild(td0);
	row.appendChild(td7);

	TBODY.appendChild(row);
	count++;
}
// this function used to save/update the configuration setup 
function fnSubmit() {
	fnValidateDropDn('issuingAgency', ' Issue Agency');
	fnValidateTxtFld('txt_configName', ' Configuration Name');

	var checkVal = '';
	var sequenceVal = '';
	var identifierIdVal = '';
	var inputstring = '';
	var identifierStr = ',';
	var duplicateIdent = '';
	var confirmFl = true;
	var orginalStr = '';
	var configNameVal = document.frmIssuingAgencyConfig.configName.value;
	// form the input string
	var seqNo = 1;
	var firstIdenDropDownVal = '';
	for ( var j = 0; j < count; j++) {
		var identifierObj = eval("document.all.identifier" + j);
		var configDtlsObj = eval("document.all.hConfigDtlId" + j);
		if ((identifierObj.value != 0) || (identifierObj.value == 0 && configDtlsObj.value !='')) {
			// validate the duplicate entry for the selected identifier
			if ((identifierObj.value != 0) && identifierStr.indexOf(","+ identifierObj.value + ",") != '-1') {
				duplicateIdent = duplicateIdent + (j + 1) + ", ";
			}
			inputstring = inputstring + configDtlsObj.value + '^'+ identifierObj.value + '^' + seqNo + '|';
			identifierStr = identifierStr + identifierObj.value + ",";
			seqNo++;
			// to validate the full void check
			if(identifierObj.value != 0){
				orginalStr = orginalStr + identifierObj.value  +'|';
				if(firstIdenDropDownVal ==''){
					firstIdenDropDownVal = identifierObj.value;
				}
			}
		}
	}
	if (inputstring == '') {
		Error_Details(message_prodmgmnt[83]);
	}
	if (orginalStr =='' && configNameVal !=0){
		Error_Details(message_prodmgmnt[83]);
	}
	if (duplicateIdent != '') {
		duplicateIdent = duplicateIdent.substring(0,(duplicateIdent.length) - 2);
		Error_Details(Error_Details_Trans(message_prodmgmnt[84],duplicateIdent));
	}
	// to validate the first field as DI number.
	if(firstIdenDropDownVal != firstIdenDiNumber){
		Error_Details(message_prodmgmnt[85]);
	}
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	if (inputstring != '') {
		document.frmIssuingAgencyConfig.inputStr.value = inputstring;
	}
	// when creating the new configuration - alert the message to user
	if (configNameVal == 0) {
		confirmFl = confirm('Please inform tac@globusmedical.com regarding the creation of new UDI configuration');
	}
	if (confirmFl) {
		document.frmIssuingAgencyConfig.strOpt.value = 'Save';
		document.frmIssuingAgencyConfig.issuingAgency.disabled = false;
		fnStartProgress("Y");
		document.frmIssuingAgencyConfig.submit();
	}
}

this.keyArray = new Array();
this.valArray = new Array();
this.put = put;
this.get = get;
this.findIt = findIt;

function put(key, val) {
	var elementIndex = this.findIt(key);
	if (elementIndex == (-1)) {
		this.keyArray.push(key);
		this.valArray.push(val);
	} else {
		this.valArray[elementIndex] = val;
	}
}

function get(key) {
	var result = null;
	var elementIndex = this.findIt(key);
	if (elementIndex != (-1)) {
		result = this.valArray[elementIndex];
	} else {
		result = "";
	}
	return result;
}

function findIt(key) {
	var result = (-1);

	for ( var i = 0; i < this.keyArray.length; i++) {
		if (this.keyArray[i] == key) {
			result = i;
			break;
		}
	}
	return result;
}
