
var gridObj;
//Initiating grid when on page loading 
function fnOnPageLoad(){
	
if(parent.window.document.getElementById("ajaxdivcontentarea") != undefined){
  	parent.window.document.getElementById("ajaxdivcontentarea").style.height = "690";
}
if(objGridData.value != ''){ 
		gridObj = initGridData('dataGridDiv',objGridData);
}
if (objGridData.indexOf("cell", 0) != -1) {
	document.getElementById("dataGridDiv").style.height = "500px";
}else{
	document.getElementById("dataGridDiv").style.height = "10";
	document.getElementById("ShowDetails").style.color = "red";
	document.getElementById("ShowDetails").style.height = "15";
	document.getElementById("ShowDetails").innerHTML = "Nothing found to display.";
}

}
//Initiating the grid
function initGridData(divRef,gridData){
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.attachHeader("#select_filter,#text_filter,#select_filter,#select_filter,#select_filter");
	gObj.init();
	gObj.loadXMLString(gridData);	
	return gObj;	
}

//Function to export excel
function fnExport()
{	
	gridObj.toExcel('/phpapp/excel/generate.php');
}

function fnPublishChk(obj){
	if(obj.value == "103086" && document.getElementById("103086").checked && !document.getElementById("103085").checked){//103086-Marketting Collateral
	 Error_Details(message_prodmgmnt[115]);
	 obj.checked = false;
    }
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}	
}

function fnUpdateTabIndex(link) {
	var linkObj = link.getElementsByTagName('a')[0];
	document.all.hcurrentTab.value = linkObj.name;
	
}

//function to load the SYSTEM details when a SYSTEM is selected in the SYSTEM LIST.
function fnfetchSystemDet(form) {	
	form.action = "/gmSetBundleMapping.do?strOpt=fetchcontainer";
	fnStartProgress('Y');
	form.submit();
}

//Function to reset the form.
function fnSystemReset(form) {
	document.getElementById("strBundleId").value = '';
	form.strOpt.value = 'fetchcontainer';
	//form.setId.value = '';
	form.submit();
}

//Function to void the sytem.
function fnVoidSystem(form){
	var set_id=form.setId.value;
	form.action = "/GmCommonCancelServlet?hTxnName="+set_id+"&hCancelType=VDSYS&hTxnId="+set_id;
	form.submit();		
}

//function to submit the form.This will save the form details.
function fnSetSubmit(form)

{
	fnValidateTxtFld('txtSetName',message_prodmgmnt[471]);
	fnValidateDropDn('divName',message_prodmgmnt[320]);
    fnValidateDropDn('stsName',message_prodmgmnt[441]);
    fnValidateTxtFld('comments',message_prodmgmnt[146]);
    if (ErrorCount > 0)
    {
             Error_Show();
             Error_Clear();
             return false;
    }
	form.strOpt.value = 'save';
	fnStartProgress('Y');
	form.submit();	
}
//Thie below function is used to form the input stirng for the Check box groups.
function fnFormAttributeString(obj, AttrId){
	var inputStr='';
	//when there is only one value in the check box Group, we should not loop to form the input String since it is considering as an object.
	if(obj.length == undefined && obj.checked == true){
		inputStr = inputStr + AttrId + '^' + obj.value + '|';
	}else{
		for (var i=0;i<obj.length;i++){
			if (obj[i].checked == true){
				inputStr = inputStr + AttrId + '^' + obj[i].value + '|';
			}
		}
	}
	return inputStr;
}

/*Thie below function is called  when we submit if we publish it in product catalog 
it will  prompt a screen where message will be displayed.*/
function fnConfirmPublish(form){
	var obj=form.Chk_GrpPublish
	var publishedIds = form.hPublishInIds.value;//we are getting product catalog and marketing collateral Id's.
	var aryPublish = publishedIds.split(",");
    var strMsg = "";
	for (var i=0;i<obj.length;i++){
		vAction = "";
		vLabel = "";
			var blOldFl = false;
			if(publishedIds != ""){
				for(j=0;j<aryPublish.length;j++){	
					if (obj[i].value == aryPublish[j]){
						blOldFl =true;
						break;
					}
				}
			}
			if(obj[i].value == "103085"){//103085-->product catalog
				vLabel =  "Product Catalog"; 
			}else{
				vLabel =  "Marketing Collateral";
			}
			if(blOldFl){//it will come here if we unpublish a system 

				if (obj[i].checked == false){
					
				    	vAction = "unpublish from";
				}
			}else{
				if (obj[i].checked == true){
					
				    	vAction = "publish to";
				}
			}
			if(vAction != ""){
				strMsg+=vAction+" "+vLabel+" & ";
			}
			
		}
	var blConfirmFl = true;
	if(strMsg !=""){
		blConfirmFl = confirm(message_prodmgmnt[118]+strMsg.substring(0, strMsg.length-3));
	}
	return blConfirmFl;
}
//Function to toggle the headers.
function fnShowFilters(val){
	var obj = eval("document.all."+val);
	var obj1 = eval("document.all."+val+"img");
	
	if (obj.style.display == 'none'){
		obj.style.display = 'block';
		if(obj1){
			obj1.src = '/images/minus.gif';
		}
	}
	else{
		obj.style.display = 'none';	
		if(obj1){
			obj1.src = '/images/plus.gif';
		}
	}
}

//function to check the set atrributes while loading the system details.
function fnCheckSetAttributes()
{
	var form = document.getElementsByName('frmSystemDetails')[0];
	var setid = document.getElementById("setId").value;
	var objgrp = '';
	if (setid != "" && setid != "0"){
		if(form){			
			var groupids = document.getElementById("hGroupIds").value;
			var segmentids = document.getElementById("hSegmentIds").value;
			var techniqueids = document.getElementById("hTechniqueIds").value;
			var hPublishInIds = document.getElementById("hPublishInIds").value;
			if (groupids != '')	{
		 		objgrp = document.getElementsByName("Chk_GrpGroup");
		 		fnCheckSelections(groupids,objgrp);
		 	}
			if (segmentids != '')	{
		 		objgrp = document.getElementsByName("Chk_GrpSegment");
		 		fnCheckSelections(segmentids,objgrp);
		 	}
			if (techniqueids != '')	{
		 		objgrp = document.getElementsByName("Chk_GrpTechnique");
		 		fnCheckSelections(techniqueids,objgrp);
		 	}
			if (hPublishInIds != ''){
		 		objgrp = document.getElementsByName("Chk_GrpPublish");
		 		fnCheckSelections(hPublishInIds,objgrp);
		 	}			
		}
	}
}


function fnCheckSelections(val,objgrp)
{
	var valobj = val.split(",");
	var arrlen = objgrp.length;
			
	if (arrlen > 0)	{
		for (i=0;i<valobj.length ;i++ )	{
			for (j=0;j<arrlen ;j++ ){
				if (valobj[i] == objgrp[j].value){
						objgrp[j].checked = true;
				}
			}
		}
	}
	//when there is only one value in the check box Group, during onload the values are not getting checked by default.
	if(arrlen == undefined && objgrp.checked == false && valobj == objgrp.value){
		objgrp.checked = true;
	}
}

//To limit the no. of characters entering into the text area
function fnLenValidation(obj,name,len){ 
	if(obj.value.length > len){
		var arrmsg = [len,name];
		Error_Details(Error_Details_Trans(message_prodmgmnt[119],arrmsg));
	}
}

//Description : This function used to validate the System name whether it is already exist or not
function fnvalidateSystemName(setName) {
	setName = TRIM(setName);
	var validName = eval("document.all.validateName");				
	var validText = document.getElementById("validateText");
	if(setName!=''){
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmSystemSetup.do?strOpt=validateSystem&setNm='+ encodeURIComponent(setName)+ '&ramdomId=' + Math.random());
		dhtmlxAjax.get(ajaxUrl, function(loader){
			var response = loader.xmlDoc.responseText;
			if (response != '' && response != 'undefined') {
					if (response == 'AVAILABLE') {	
							validName.style.display = 'block';
							validName.src = '/images/success.gif';
							validText.innerHTML ="Available";
						}else{
							validName.style.display = 'block';
							validName.src = '/images/error.gif';
							validText.innerHTML ="Not Available";
						}
					}		
		});
	}else{
			if(validName.style.display == 'block'){
				validName.style.display = 'none';
			}
			if(validText.innerHTML){
				validText.innerHTML ='';
			}
	}
}

//Function to preview the system.
function fnPreviewSystem(form){
	var setid = form.setId.value;
	var setName = form.setNm.value;	
	windowOpener('/gmSystemPreview.do?strOpt=Load&systemId='+setid+'&systemNm='+setName,"SystemDetails","resizable=yes,scrollbars=yes,top=150,left=150,width=865,height=800");
}

function fnTagHistory(form)
{ 
	var strBundleid=document.getElementById('strBundleId').value;
	windowOpener("/gmSetBundleMapping.do?strOpt=History&strBundleId="+strBundleid,"TagHistory","resizable=yes,scrollbars=yes,top=250,left=300,width=800,height=600");
}