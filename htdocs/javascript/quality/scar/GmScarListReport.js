function fnLoad(){
	objStartDt = document.frmScarListReport.assignedFromDt;
	objEndDt   = document.frmScarListReport.assignedToDt;
	var dateDiffVal = dateDiff(todaysDate,objStartDt.value,format);
	document.frmScarListReport.hscarId.value='';
	if(objStartDt.value != "" && objEndDt.value == ""){
		Error_Details(message_prodmgmnt[68]);
	}
	if(objEndDt.value != "" && objStartDt.value == ""){
		Error_Details(message_prodmgmnt[260]);	
	}	
	if(objStartDt.value != ""){
		CommonDateValidation(objStartDt,format,Error_Details_Trans(message_prodmgmnt[261],format));
	}
	if(objEndDt.value != ""){		
		CommonDateValidation(objEndDt,format,Error_Details_Trans(message_prodmgmnt[262],format));
	}
	var dtFrom = document.frmScarListReport.assignedFromDt.value;
	var dtTo   = document.frmScarListReport.assignedToDt.value;
	if(dtFrom != '' || dtTo != ''){	
		if(!(new Date(dtFrom).valueOf() <= new Date(dtTo).valueOf())){
			Error_Details(message_prodmgmnt[69]);
		}			
	}
	var days = document.frmScarListReport.elpasedDys.value;
	var val  = document.frmScarListReport.elpasedDysVal.value;
	if(days == '0' && val != ''){
		Error_Details(message_prodmgmnt[70]);
	}else if(days != '0' && val == ''){
		Error_Details(message_prodmgmnt[71]);
	}else if(val != '' && days != '0'){
		fnValidateNumber('elpasedDysVal',message_prodmgmnt[420]);
	}
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	 fnStartProgress('Y');
	 document.frmScarListReport.strOpt.value = "Report";
	 document.frmScarListReport.action = '/gmScarListReport.do?method=listScarDetails';  
	 document.frmScarListReport.submit();
	 document.frmScarListReport.Btn_Load.disabled=true;
	 
}
function fnExportExcel(){
	if(accessFl == "true"){
		gridObj.setColumnHidden(0,true);
		gridObj.setColumnHidden(1,true);
		gridObj.toExcel('/phpapp/excel/generate.php');
		gridObj.setColumnHidden(0,false);
		gridObj.setColumnHidden(1,false);
	}else{
		gridObj.setColumnHidden(0,true);
		gridObj.toExcel('/phpapp/excel/generate.php');
		gridObj.setColumnHidden(0,false);
	}
}
//This function is used to initiate grid
function initGridData(divRef,gridData){
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);
	gObj.enableTooltips("true,true,true,true,true,true,true");
	gObj.init();
	gObj.loadXMLString(gridData);	
	return gObj;	
}
//This function is used to activate Load button while pressing the enter key
function fnEnter() {
	
	if(event.keyCode == 13){
		fnLoad();
	}
}
//This function is used to initiate grid
function fnOnLoad(){ 
	if (objGridData.value != '') {
		gridObj = initGridData('SCARDATA',objGridData);
	}
}
//To set the Selected scar Id from grid to jsp
function fnsetScarId(obj,statusid){
	document.frmScarListReport.hscarId.value=obj.value;
	document.frmScarListReport.hstatus.value=statusid;
} 
//To open window to add/view comments
function fnOpenOrdLog(strScarId,type){
	windowOpener("/GmCommonLogServlet?hType="+type+"&hID="+strScarId,"PrntInv", "resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
}
//Function to navigate to other pages while choosing an option from drop down
function fnSubmit() {
	var ChooseActionId = document.frmScarListReport.chooseAction.value;
	var ScarId = document.frmScarListReport.hscarId.value;
	var statusId = document.frmScarListReport.hstatus.value;
	
	if (ScarId != '' && ChooseActionId != 0) {// Navigate to Scar initiation screen
		if (ChooseActionId == '102775') {
				document.frmScarListReport.action = '/gmScarInitiation.do?method=editScar&strOpt=Fetch&scarId='+ ScarId;
		}
		else if (ChooseActionId == '102777') {// Navigate to Upload Screen
			document.frmScarListReport.action = "/gmUploadAction.do?strOpt=Fetch&refType=102789&logType=4000318&strRefID="+ScarId;
		}
		else if (ChooseActionId == '102778') {// Navigate to Common Cancel Screen
			if(statusId == '102779'){ // if the status is open
				document.frmScarListReport.action = "/GmCommonCancelServlet?hTxnName="+ScarId+"&hCancelType=VDSCAR&hTxnId="+ScarId+"&hAction=Load";
				document.frmScarListReport.hDisplayNm.value = 'Modify SCAR';
				document.frmScarListReport.hRedirectURL.value = '/gmScarListReport.do?method=listScarDetails&strOpt=Report&accessFl=true&scarId='+ScarId;
			}
			else{ 
				Error_Details(message_prodmgmnt[263]);
			}
		}else if(ChooseActionId == '4000323') {// Navigate to Common Cancel Screen
			if(statusId == '102781'){ // if the status is closed
				document.frmScarListReport.action = "/GmCommonCancelServlet?hTxnName="+ScarId+"&hCancelType=RPSCAR&hTxnId="+ScarId+"&hAction=Load";
				document.frmScarListReport.hDisplayNm.value = 'Modify SCAR';
				document.frmScarListReport.hRedirectURL.value = '/gmScarListReport.do?method=listScarDetails&strOpt=Report&accessFl=true&scarId='+ScarId;
			}
			else{
				Error_Details(message_prodmgmnt[72]);
			}
		}
	}
 
	else if (ScarId != '' && ChooseActionId == 0) {
		Error_Details(message_prodmgmnt[73]);
	}
	else if (ScarId == '' && ChooseActionId != 0) {
		Error_Details(message_prodmgmnt[74]);
	}
	else if(ScarId == '' && ChooseActionId == 0){	
		Error_Details(message_prodmgmnt[74]);
		Error_Details(message_prodmgmnt[73]);
	}	
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
		document.frmScarListReport.submit();
		fnStartProgress('Y');
}

function fnOpenSummaryScreen(scarId){ // To open the Summary Screen
	windowOpener("/gmUploadAction.do?strOpt=Summary&refType=102789&strRefID="+scarId,"","resizable=yes,scrollbars=yes,top=250,left=300,width=1010,height=600");
}