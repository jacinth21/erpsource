function fnLoad(){//This function is used to activate Load button 	 
	var scarId = document.frmScarInitiation.scarId.value;
	
	if(scarId != ''){
		 document.frmScarInitiation.strOpt.value = "Fetch";
		 document.frmScarInitiation.action = '/gmScarInitiation.do?method=editScar';  
		 document.frmScarInitiation.submit();	 
	}else{
		Error_Details(message_prodmgmnt[75]);
	}	 
	if (ErrorCount > 0){
		Error_Show();
	    Error_Clear();
		return false;
	}
	fnStartProgress('Y');
	document.frmScarInitiation.Btn_Load.disabled=true;
}

function fnLenValidation(obj,name,len){
	if(obj.value.length > 4000){
		var array=[len,name];
		Error_Details(Error_Details_Trans(message_prodmgmnt[76],array));
	}
}
function fnSubmit(){
	
	var dtAssign    = document.frmScarInitiation.assignedDt.value;
	var dtResponse  = document.frmScarInitiation.respDueDt.value;
	var dtCLose     = document.frmScarInitiation.closedDt.value;
	var strOpt      = document.frmScarInitiation.strOpt.value;
	var scarId   	= document.frmScarInitiation.scarId.value;
	var currentDate = document.frmScarInitiation.currentdate;
	var estimetdFlUpDt  = document.frmScarInitiation.estimetdFlUpDt.value;
	var followUpFlag = document.frmScarInitiation.followUpFlag.value;
	
	if(strOpt == "load" && scarId != ''){
		Error_Details(message_prodmgmnt[77]);
	}
	
	if(strOpt != "load"){
		if(scarId != hSCARID)
			Error_Details(message_prodmgmnt[77]);
	}
	
	fnLenValidation(document.frmScarInitiation.relaventInfo,message_prodmgmnt[274],'4000');
	fnLenValidation(document.frmScarInitiation.complaint,message_prodmgmnt[275],'4000');
	fnLenValidation(document.frmScarInitiation.prevScar,message_prodmgmnt[276],'4000');
	fnLenValidation(document.frmScarInitiation.scarDesc,message_prodmgmnt[277],'4000');
		
	CommonDateValidation(document.frmScarInitiation.assignedDt,format,Error_Details_Trans(message_prodmgmnt[268],format));
	CommonDateValidation(document.frmScarInitiation.respDueDt,format,Error_Details_Trans(message_prodmgmnt[269],format));
	CommonDateValidation(document.frmScarInitiation.closedDt,format,Error_Details_Trans(message_prodmgmnt[270],format));
	CommonDateValidation(document.frmScarInitiation.estimetdFlUpDt,format,Error_Details_Trans(message_prodmgmnt[271],format));
	
	var assignCurrDiff = dateDiff(dtAssign,currentdate,format);
	var closedCurrDiff = dateDiff(dtCLose,currentdate,format);
	var assignResponceDiff = dateDiff(dtAssign,dtResponse,format);
	var assignClosedDiff = dateDiff(dtAssign,dtCLose,format);
	var assignEstimedDiff = dateDiff(dtAssign,estimetdFlUpDt,format);

	
	if(dtAssign != ''){
		if(assignCurrDiff < 0){			
			Error_Details(message_prodmgmnt[78]);
		}
	}
	
	if(dtCLose != ''){		
		if(closedCurrDiff < 0){			
			Error_Details(message_prodmgmnt[264]);
		}	
	}
	
	if(dtAssign != '' && dtResponse != ''){//comparing assigned date and response date		  
		if(assignResponceDiff < 0){
			Error_Details(message_prodmgmnt[265]);
		}	
	}
	
	if(dtAssign != '' && dtCLose != ''){		  
		if(assignClosedDiff < 0){//comparing assigned date date and closed date
			Error_Details(message_prodmgmnt[266]);
		}	
	}
	
	if(dtAssign != '' && estimetdFlUpDt != '' && followUpFlag == '80130'){		  
		if(assignEstimedDiff < 0){//comparing assigned date date and closed date
			Error_Details(message_prodmgmnt[267]);
		}	
	}
	
	if (ErrorCount > 0){
		Error_Show();
	    Error_Clear();
		return false;
	}
	
	confirmMsg =  fnConfirmMessage();
	if(confirmMsg)	{
		document.frmScarInitiation.strOpt.value = "Add";
		document.frmScarInitiation.action = '/gmScarInitiation.do?method=addScar'; 	
		document.frmScarInitiation.submit();
	}
	else			return false;
	fnStartProgress('Y');
	
 }

function fnReset(){
	var strOpt = document.frmScarInitiation.strOpt.value;
	var ScarID = document.frmScarInitiation.scarId.value;
	if(strOpt == "Fetch"){
		confirmMsg =  fnConfirmMessage();
		if(confirmMsg){
				if(ScarID != hSCARID){
					document.frmScarInitiation.scarId.value = hSCARID;
				}
				if(hSCARID != ''){
					fnLoad();
				}else{
					document.frmScarInitiation.action = "/gmScarInitiation.do?method=addScar&strOpt=load";
					document.frmScarInitiation.submit();
				}
			
		}
	}
	else if(strOpt == "load"){
		
		confirmMsg =  fnConfirmMessage();
		if(confirmMsg){
			document.frmScarInitiation.action = "/gmScarInitiation.do?method=addScar&strOpt=load";
			document.frmScarInitiation.submit();
			}
	}
}

function fnConfirmMessage(){
	confirmMsg = confirm(message_prodmgmnt[432]);
	if(confirmMsg)	return true;
	else 			return false;
} 



function fnShowFilters(val){
	var obj = eval("document.all."+val);
	var obj1 = eval("document.all."+val+"img");
	if (obj.style.display == 'none'){
		obj.style.display = 'block';
		if(obj1){
			obj1.src = '/images/minus.gif';
		}
	}else{
		obj.style.display = 'none';	
		if(obj1){
			obj1.src = '/images/plus.gif';
		}
	}
}


// When click follow up Yes then should disable follow up date field.
function fnSetEsimetedDt(){
	var followUpFlag = document.frmScarInitiation.followUpFlag.value;
	if(followUpFlag == '80130'){
		// Follow Up flag is Y should not disable follow up date control
		document.frmScarInitiation.estimetdFlUpDt.disabled = false;
		document.frmScarInitiation.Img_Date[3].disabled = false;
	}else{
		// Follow Up flag is N should disable follow up date control
		document.frmScarInitiation.estimetdFlUpDt.disabled = true;
		document.frmScarInitiation.Img_Date[3].disabled = true;
	}
}
//Function to navigate to other pages while choosing an option from drop down
function fnChooseAction() {
	var frm = document.frmScarInitiation;
	var ChooseActionId = document.frmScarInitiation.chooseAction.value;
	var ScarId = document.frmScarInitiation.scarId.value;
	
	if (ScarId != '' && ChooseActionId != 0) {
		if (ChooseActionId == '102776') { // Navigate to Scar Report screen
				document.frmScarInitiation.action = '/gmScarListReport.do?method=listScarDetails&accessFl=true&strOpt=Report&scarId='+ ScarId;
		}
		else if (ChooseActionId == '102777') { // Navigate to Upload Screen
			document.frmScarInitiation.action = '/gmUploadAction.do?strOpt=Fetch&logType=4000318&strRefID='+ ScarId+"&refType=102789";
		}
		else if (ChooseActionId == '102778') {// Navigate to Common Cancel Screen
			if(status == '102779'){ // if the status is open
			document.frmScarInitiation.action = "/GmCommonCancelServlet?hTxnName="+ScarId+"&hCancelType=VDSCAR&hTxnId="+ScarId+"&hAction=Load";
			document.frmScarInitiation.hDisplayNm.value = 'SCAR Initiation';
			document.frmScarInitiation.hRedirectURL.value = '/gmScarInitiation.do?method=editScar&strOpt=Fetch&scarId='+ScarId;
		}
			else{
				Error_Details(message_prodmgmnt[72]);
			}
		}
		else if(ChooseActionId == '4000323') {// Navigate to Common Cancel Screen
			if(status == '102781'){ // if the status is closed
				document.frmScarInitiation.action = "/GmCommonCancelServlet?hTxnName="+ScarId+"&hCancelType=RPSCAR&hTxnId="+ScarId+"&hAction=Load";
				document.frmScarInitiation.hDisplayNm.value = ' SCAR Initiation';
				document.frmScarInitiation.hRedirectURL.value = '/gmScarInitiation.do?method=editScar&strOpt=Fetch&scarId='+ScarId;
			}
			else{
				Error_Details(message_prodmgmnt[72]);
			}
		}
	}
	
	else if (ScarId != '' && ChooseActionId == 0) {
		Error_Details(message_prodmgmnt[79]);
	}
	else if (ScarId == '' && ChooseActionId != 0) {
		Error_Details(message_prodmgmnt[80]);
	}
	else if(ScarId == '' && ChooseActionId == 0){	
		Error_Details(message_prodmgmnt[81]);
		Error_Details(message_prodmgmnt[82]);
	}	
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
		if(frm.Submit)frm.Submit.disabled=true;
		fnStartProgress('Y');
		document.frmScarInitiation.submit();
		
		
}

function fnEmail(){
	var scarID = document.frmScarInitiation.scarId.value;
	windowOpener('/gmCOMRSRProcess.do?method=closureEmail&strOpt=Load&refID='+scarID+"&refType=102789","ClosureEmail","resizable=yes,scrollbars=yes,top=150,left=200,width=880,height=500");
}


//When click print it will download RFT format from jasper.
function fnPrint(){
	//var scarID = document.frmScarInitiation.scarId.value;
	if(hSCARID != ''){
		 document.frmScarInitiation.action = '/gmScarListReport.do?method=printScarDetails&strOpt=Print&scarId='+hSCARID;
		 document.frmScarInitiation.submit();
	}else{
		Error_Details(message_prodmgmnt[75]);
	}	 
	if (ErrorCount > 0){
		Error_Show();
	    Error_Clear();
		return false;
	}
}

//To set submitted date as system date
function fnOnPageLoad(){
	var strOpt = document.frmScarInitiation.strOpt.value;
	if(strOpt == "load"){
		document.frmScarInitiation.assignedDt.value = currentdate;
	}
	fnSetEsimetedDt();
	
}