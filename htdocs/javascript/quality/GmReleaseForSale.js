var mygrid;
var gridrowid;
var count =0 ;
var ErrorCount = 0;
var status_desc='';
var status = '';
var statusid='';
var part='';
var partDesc='';
var statusdesc='';
function fnOnPageLoad()
{
	var dataHost = {};
if(strOpt == 'save'){
	
	var rows = [], i = 0;
	  var JSON_Array_obj = "";
	JSON_Array_obj = JSON.parse(objGridData);
	$.each(JSON_Array_obj, function(index, jsonObject) {
		part = jsonObject.PART_NUM;
		partDesc = jsonObject.PART_NUM_DESC;
		statusdesc = jsonObject.STATUS_DESC;
		statusid = jsonObject.STATUS;

		rows[i] = {};
		rows[i].id = part;
		rows[i].data = [];
		
		rows[i].data[0] = part;
		rows[i].data[1] = partDesc;
		rows[i].data[2] = statusdesc;
		rows[i].data[3] = statusid;
	
		i++;
	});
   dataHost.rows = rows;
}
mygrid = new dhtmlXGridObject('mygrid_container');
mygrid.setImagePath("/extweb/suite/5.2.0/codebase/imgs/");
mygrid.setHeader("Part #,Description,Status,Status Id");
mygrid.setInitWidths("140,*,140,*");
mygrid.setColAlign("left,left,left,left");
mygrid.setColTypes("ed,ro,ro,ro");
mygrid.setColSorting("str,str,str,str");
mygrid.setColumnIds("part,partDesc,statusdesc,statusid");
mygrid.attachEvent("onKeyPress",onKeyPressed);
mygrid.init();
mygrid.enableAlterCss("even","uneven");

	if (strOpt !="save"){
       add_fast();
mygrid.parse(objGridData,"js");
	}else{
		mygrid.parse(dataHost,"json");
	}
	
 mygrid.parse(objGridData,"js");
mygrid.enableBlockSelection(); 
var status_desc_colId = mygrid.getColIndexById("statusdesc");
var status_colId = mygrid.getColIndexById("statusid");
mygrid.setColumnHidden(status_colId, true);

	var gridrows   = mygrid.getAllRowIds();
	var gridrowsarr = gridrows.split(",");
	if (strOpt == 'save')
	{
		mygrid.setColTypes("ro,ro,ro,ro");
		document.frmReleaseForSale.Btn_Submit.disabled=true;
		mygrid.forEachRow(function(row_id) {
		var statusVal = mygrid.cellById(row_id, status_colId).getValue();
		if(statusVal != '20367'){
			mygrid.setCellTextStyle(row_id,status_desc_colId,"font-weight:bold;color:Red;"); 
		}
	 });
	}
document.body.style.cursor = "default";
}
function onKeyPressed(code,ctrl,shift){

	if(code==67&&ctrl){
		if (!mygrid._selectionArea) return alert("You need to select a block area in grid first");
			mygrid.setCSVDelimiter("\t");
			mygrid.copyBlockToClipboard()
		}
		if(code==86&&ctrl){
			mygrid.setCSVDelimiter("\t");
			mygrid.pasteBlockFromClipboard()
			//mygrid._HideSelection();
		}
	return true;
}
function fnSubmit()
{
	fnStartProgress('Y');
	var partnos='';
	var invalPartnos='';
	var dupCount=0;
	var countryError = 0;
	var arChk = document.frmReleaseForSale.Chk_GrpCOUNTRY ;
	var str = ''; 
	var len = arChk.length;
	// for country checkbox. vut string using | symbol & assign it to str variable & pass that value back to form.
	for (i=0;i< len;i++ )
	{
		if (document.frmReleaseForSale.Chk_GrpCOUNTRY[i].checked)
		{
			str = str + document.frmReleaseForSale.Chk_GrpCOUNTRY[i].value + '|';
			countryError++;
		}
	}
	document.frmReleaseForSale.hChkRFSCountry.value = str;
	// show the error msg if none of the country is selected.
	if(countryError == 0)
	{
		error_msg=message_prodmgmnt[272];
		Error_Details(error_msg);
	}
	// check validation for RFS status dropdown.
	fnValidateDropDn('rfsStatus',message_prodmgmnt[422]);
	
	mygrid.editStop();// retun value from editor(if presents) to cell and closes editor

	//var gridrows =mygrid.getChangedRows(",");
	var gridrows =mygrid.getAllRowIds(",");
	// get the part details form the grid.
	if(gridrows.length>0)
	{
		fnDisableButton('frmReleaseForSale','Btn_Submit');
		var gridrowid;
		var gridrowsarr = gridrows.toString().split(",");
		var columnCount = mygrid.getColumnsNum();
		
		var validationError=0;
				
		var error_msg='';
		if (gridrowsarr.length > 2500){
			Error_Details(message_prodmgmnt[95]);
		}
		for (var rowid = 0; rowid < gridrowsarr.length; rowid++) 
		{
			gridrowid = gridrowsarr[rowid];
			var Part = mygrid.cellById(gridrowid, 0).getValue();
				
			if(Part != null && Part != '')
			{
				if(partnos.indexOf(Part+ ",")==-1)
				{
					if(TRIM(Part).length >= 21)
					{
						invalPartnos += (Part + ",");
					}
					else
					{
						partnos += (Part + ",");
					}
				}
			}
									
			if(validationError == 0 && TRIM(Part).length>0){
				validationError++;
			}
					
		}
		//commenting this because we eliminating duplicate rows and submitting
		if(invalPartnos!='' && TRIM(invalPartnos).length>1){
			invalPartnos=invalPartnos.substr(0,(TRIM(invalPartnos).length-1));
			error_msg=Error_Details_Trans(message_prodmgmnt[278],invalPartnos);	
			Error_Details(error_msg);
		}
		
		if(validationError==0)
		{
			error_msg=message_prodmgmnt[273];	
			Error_Details(error_msg);
		}
		if(ErrorCount>0)
		{
		fnEnableButton('frmReleaseForSale','Btn_Submit');
		fnStopProgress();
		Error_Show();
		Error_Clear();
		return;
		}
		document.frmReleaseForSale.hPartNos.value = partnos;
		document.frmReleaseForSale.strOpt.value = 'save';
		document.frmReleaseForSale.action='/gmReleaseForSale.do?method=saveReleaseForSale';
		document.frmReleaseForSale.submit();
			
	}
	
}
// To add rows into grid.
function addRow()
{
      mygrid.addRow(mygrid.getUID(),'');
}

// Use the below function to copy data inside the screen.
function docopy()
{
      mygrid.setCSVDelimiter("\t");
      mygrid.copyBlockToClipboard()
      //mygrid._HideSelection();
}
function pasteToGrid(){
	
	if(mygrid._selectionArea!=null){
		
			mygrid.pasteBlockFromClipboard()
			//mygrid._HideSelection();
		
	}else{
		alert(message_prodmgmnt[34]);
	}
}


/*//Use the below function to past data inside the screen as well as from excel to screen.
function addRowFromClipboard()
{
	//var cbData = mygrid.fromClipBoard(); 
	//fnStartProgress('Y');
	mygrid.clearAll(); // clear grid
    mygrid.gridFromClipboard();
   // fnStopProgress();
    var gridrows =mygrid.getAllRowIds(",");
    var gridrowsarr = gridrows.toString().split(",");
	
	if (gridrowsarr.length > 2500){
		Error_Details(message_prodmgmnt[95]);
	}
	if(ErrorCount>0 )
	{
		Error_Show();
		Error_Clear();
		return;
	}
}*/

// Use the below function to remove the rows from the grid.
function removeSelectedRow()
{
      var gridrows=mygrid.getSelectedRowId();   
      if(gridrows!=undefined)
      {
            var NameCellValue ;
            var gridrowsarr = gridrows.toString().split(",");
            var added_row;
            for (var rowid = 0; rowid < gridrowsarr.length; rowid++)
            {
                  gridrowid = gridrowsarr[rowid];                 
                  mygrid.deleteRow(gridrowid);                                
            }
      }else{
            Error_Clear();
            Error_Details(message_prodmgmnt[37]);                       
            Error_Show();
            Error_Clear();
      }
}

// Use the below function to check duplicate value inside the grid.
function checkForDuplicateRow(row_id){
      if(mygrid.doesRowExist(row_id)){
            return true;
      }
      return false;
}

// The below function avoid the duplicate data inside grid.
function doOnCellEdit(stage, rowId, cellInd, nValue, oValue) {
	if(rowId!=null && nValue!=null)
	{
		if(stage=='2' && nValue!=null)
		{
			if(checkForDuplicateRow(nValue)== true)
			{	
				alert("Part# already exists.");
				return false;
			}
			
		}
		mygrid.changeRowId(rowId,nValue);
	}
	return nValue;
}

// Use the below function to reset the screen.
function fnReset()
{
	document.frmReleaseForSale.comments.value='';
	if (strOpt != 'save')
	{
	document.frmReleaseForSale.rfsStatus.value='';
	}
	document.frmReleaseForSale.action='/gmReleaseForSale.do?method=loadReleaseForSale&strOpt=load';
	document.frmReleaseForSale.submit();
}
//this function used add 500 rows fast
function add_fast(){
 mygrid.startFastOperations();
	for(var i=1 ;i<500 ;i++){
	   addRow();
	}
 mygrid.stopFastOperations();
 }