function fnPageLoad()
{
	document.getElementById('setBundleName').focus();
}

function fnLoadDashDetails(){ 
	var setBundleName = document.frmSetBundleRpt.setBundleName.value;
	if(setBundleName == '0' || setBundleName == ''){
		Error_Details(message[5315]);
	}
	
	if (ErrorCount > 0)  
	{
			Error_Show();
			Error_Clear();
			return false;
	}
		document.frmSetBundleRpt.action = "/gmSetBundleMappingReport.do?method=setBundleRpt&hStrOpt=Load";
		fnStartProgress();
		document.frmSetBundleRpt.submit();
}

function Toggle(val){	
	var obj = eval("document.all.div"+val);	
	var trobj = eval("document.all.tr"+val);
	var tabobj = eval("document.all.tab"+val);	
	
	if (obj.style.display == 'none')	{
		obj.style.display = '';
		trobj.className= "ShadeRightTableCaptionBlue";
		tabobj.style.background="#ecf6fe";
	}else{
		obj.style.display = 'none';
		trobj.className= "";
		tabobj.style.background="#ffffff";
	}
}


var gridObj;
function fnOnPageLoad() {
	if (objGridData.value != '') {
		gridObj = initGridData('SetBundleRpt', objGridData);
	}
}
function initGridData(divRef, gridData) {
	var gObj = new dhtmlXGridObject(divRef);
	gObj.attachHeader(',#text_filter,#text_filter,#text_filter,#select_filter,#select_filter,#select_filter,#text_filter'); 
	gObj.setSkin("dhx_skyblue");
	//Before init() need to call attachHeader - changes done for PC-3662-Edge Browser fixes for Quality Module
	gObj.attachHeader(',#text_filter,#text_filter,#text_filter,#select_filter,#select_filter,#select_filter,#text_filter');
	gObj.init();
	gObj.loadXMLString(gridData);
	gObj.enableTooltips("false,false,false,false,false,false,false");
    return gObj;
}

//To download excel;
function fnExcel() { 
	gridObj.toExcel('/phpapp/excel/generate.php');
}