//Declaration Variables
var part_rowId = "";
var part_desc_rowId = "";
var qty_rowId = "";
var type_rowId = "";
var flag_rowId = "";
var map_id = "";
var revNumber_rowId = "";
var combo;

//sub component type drop down variables
var subCompPartTypeCol = '';
var subCompPartTypeColArr = '';
var subCompPartTypeId = '';
var subCompPartTypeNm = '';
var subCompPartTypeVal = '';

function fnOnPageLoad(){
	fnLoadCallBack();
	fnChkAction();
	var strPartNumber = TRIM(document.frmPartNumberAssembly.partNumber.value);
	if(strPartNumber !=''){
		fnLoadPartAssemblyDtls();
	}
}

// this function used to action based BOM flag checkbox enable or disable
function fnChkAction(){
	var vSubComponent = document.frmPartNumberAssembly.subComponent.value;
	// type - sub component
	if(vSubComponent == 109741){
		document.getElementById("bomFlag").disabled = true;
		document.frmPartNumberAssembly.bomFlag.checked = false;
		
	}else if(vSubComponent=109740){
		document.getElementById("bomFlag").disabled = false;
		document.frmPartNumberAssembly.bomFlag.checked = false;
		
	}
}

var gridObj = '';
var part ="";
var partDesc  ="";
var qty  ="";
var ptype  ="";
var revNumber  ="";
var voidfl  ="";
var mapId  ="";
			
function fnLoadCallBack(loader){
	var dataHost = {};
	// to clear the grid data
	if(gridObj !=''){
		gridObj.clearAll();
	}
	
	if(loader == undefined || loader == null){
		strDtlsJson ='';
	}else{
	    strDtlsJson = loader.xmlDoc.responseText;
if(strDtlsJson !='' && strDtlsJson != null && strDtlsJson != undefined){//PC-5338
      var rows = [], i = 0;
	  var JSON_Array_obj = "";
      var strDtlsJsonData = "["+strDtlsJson+"]";
	JSON_Array_obj = JSON.parse(strDtlsJson);
	$.each(JSON_Array_obj, function(index, jsonObject) {
		part = jsonObject.PNUM;
		partDesc = jsonObject.PDESC;
		qty = jsonObject.QTY;
		ptype = jsonObject.PTYPE;
		revNumber = jsonObject.REV_NUMBER;
		voidfl = jsonObject.VOIDFLG;
		mapId = jsonObject.PMAPID;
		rows[i] = {};
		rows[i].id = mapId;
		rows[i].data = [];
		
		rows[i].data[0] = part;
		rows[i].data[1] = partDesc;
		rows[i].data[2] = qty;
		rows[i].data[3] = ptype;
		rows[i].data[4] = revNumber;
		rows[i].data[5] = voidfl;
		rows[i].data[6] = mapId;
		i++;
	});
   dataHost.rows = rows;
}
}
		//var setColIds = "PNUM,PDESC,QTY,PTYPE,REV_NUMBER,VOIDFLG,PMAPID";
	
		document.getElementById("trDiv").style.display = "table-row";
		document.getElementById("trImageShow").style.display = "table-row";
		document.getElementById("buttonshow").style.display = "table-row";
		
		gridObj = new dhtmlXGridObject('dataGridDiv');
		gridObj.setImagePath("/extweb/suite/5.2.0/codebase/imgs/");
		gridObj.setHeader("Part#,Part Description,Qty, Type,Rev Number,Remove Flag,");
		gridObj.setInitWidths("150,200,100,200,*,100,0");
		gridObj.setColAlign("left,left,right,left,left,center,left");
		gridObj.setColTypes("ed,ro,ed,coro,ro,ch,ro");
		gridObj.setColSorting("str,str,int,str,str,str");
		gridObj.setColumnIds("part,partDesc,qty,ptype,revNumber,voidfl,mapId");
		
		//set drop down values for grid body to show pttype
	    combo = gridObj.getCombo(3);
		combo.clear();
		for(var i=0;i<PTMAPArr.length;i++){
		combo.put(PTMAPArr[i][0],PTMAPArr[i][1]);
		}
		combo.save();

		gridObj.init();
		gridObj.enableAlterCss("even","uneven");
		if (strDtlsJson == "") {
		gridObj.parse(strDtlsJson,"js");
		}else{gridObj.parse(dataHost,"json"); }
		
		gridObj.attachEvent("onKeyPress", onKeyPressed);
		gridObj.attachEvent("onEditCell",doOnCellEdit);
	    gridObj.enableMultiselect(true);
		gridObj.enableBlockSelection(true);
	
		part_rowId = gridObj.getColIndexById("part");
		qty_rowId = gridObj.getColIndexById("qty");
		flag_rowId = gridObj.getColIndexById("voidfl");
		type_rowId = gridObj.getColIndexById("ptype");
		map_id = gridObj.getColIndexById("mapId");
		revNumber_rowId = gridObj.getColIndexById("revNumber");
		
		//Add Five rows when Onpagelaod
		if (strDtlsJson == "") {
			fnAddRows();
			fnAddRows();
			fnAddRows();
			fnAddRows();
			fnAddRows();
			// PC-4192: Default rows added.
			fnAddRows();
			fnAddRows();
			fnAddRows();
			fnAddRows();
			fnAddRows();
			fnAddRows();
			
		} 
		
		gridObj.setColumnHidden(map_id,true);
		
		// to disable the part # field.
		
		gridObj.forEachRow(function(row_id) {   
			partMapping_val = gridObj.cellById(row_id, map_id).getValue();
			
			if (partMapping_val != '') {
				gridObj.cellById(row_id, part_rowId).setDisabled(true);
			}
			
		});
		
}

//this function used to copy the grid data
function onKeyPressed(code, ctrl) {
	if (code == 67 && ctrl) {
		if (!gridObj._selectionArea)
			return false;
		gridObj.setCSVDelimiter("\t");
		gridObj.copyBlockToClipboard();
		//gridObj._HideSelection();
	}
	if (code == 86 && ctrl) {
		gridObj.setCSVDelimiter("\t")
		gridObj.pasteBlockFromClipboard();
		//gridObj._HideSelection();
	}
	gridObj.refreshFilters();
	return true;
}

//To add rows into grid.
function fnAddRows() {
	gridObj.filterBy(0, "");// unfilters the grid while adding rows
	gridObj._f_rowsBuffer = null; // clears the cache
	var newRowId = gridObj.getUID();
	gridObj.addRow(newRowId, '');
	// to set the default values
	gridObj.cellById(newRowId, type_rowId ).setValue(30031);
	
	// PC-4192: to set the default qty as 1
	gridObj.cellById(newRowId, qty_rowId).setValue(1);
}

//this function is used to perform action when a cell is editing
function doOnCellEdit(stage, rowId, cellInd, nValue, oValue) {
	// Highlight data when edit cell
	if (stage == 1 && (cellInd == part_rowId || cellInd == qty_rowId)) {
		var c = this.editor.obj;
		if (c != null)
			c.select();
	}
	
	//when Part& Flag updated then reset the color				  
	if (stage == 2 && nValue != oValue) {
		//console.log (" new Value "+ nValue +" Old value "+oValue +" cellInd " + cellInd );
		// PC-4192: Based on Part # to set the Type as Modified Supply (if MS parts)
        if (cellInd == part_rowId && (nValue.toUpperCase()).indexOf('MS.') !== -1){
			//console.log ('Inside the MS parts ');
			gridObj.cellById(rowId, type_rowId).setValue(30032);
		}
		gridObj.setRowAttribute(rowId, "row_modified", "Y");
	}

	return true;
}

//This Function Used to set custom types conditions in Grid
function fnCustomTypes(gObj, setHeader, setColIds, setInitWidths, setColAlign,
		setColTypes, setColSorting, enableTooltips, setFilter, gridHeight,
		pagination) {
	
	//set drop down values for grid body to show pttype
    combo = gObj.getCombo(3);
	combo.clear();
	for(var i=0;i<PTMAPArr.length;i++){
	combo.put(PTMAPArr[i][0],PTMAPArr[i][1]);
	}
	combo.save();

	if (setHeader != undefined && setHeader.length > 0) {
		gObj.setHeader(setHeader);
		var colCount = setHeader.length;
	}

	if (setInitWidths != "")
		gObj.setInitWidths(setInitWidths);

	if (setColAlign != "")
		gObj.setColAlign(setColAlign);

	if (setColTypes != "")
		gObj.setColTypes(setColTypes);

	if (setColSorting != "")
		gObj.setColSorting(setColSorting);

	if (enableTooltips != "")
		gObj.enableTooltips(enableTooltips);

	if (setFilter != "")
		gObj.attachHeader(setFilter);

	if (setColIds != "") {
		gObj.setColumnIds(setColIds);
	}

	if (gridHeight != "")
		gObj.enableAutoHeight(true, gridHeight, true);
	else
		gObj.enableAutoHeight(true, 400, true);

	if (pagination == "Y") {
		gObj.enablePaging(true, 100, 10, "pagingArea", true);
		gObj.setPagingSkin("bricks");
	}

	return gObj;
}


//This function used to upload part ,qty and remove flag details.
function fnSubmit() {
	var partNumberVal = TRIM(document.frmPartNumberAssembly.partNumber.value);
	var actionTypeVal = TRIM(document.frmPartNumberAssembly.subComponent.value);
	var vcomments = document.frmPartNumberAssembly.txt_LogReason.value;
	var vBomId = document.frmPartNumberAssembly.bomId.value;
	var vBomFlag = '';   // bomflag value
    if(document.frmPartNumberAssembly.bomFlag.checked != undefined) {
    	vBomFlag = document.frmPartNumberAssembly.bomFlag.checked;
    	 if(vBomFlag == true){
    	    	document.frmPartNumberAssembly.bomFlag.value = "Y";
    	    }else{
    	    	document.frmPartNumberAssembly.bomFlag.value = "N";
    	    }	
       }
    	fnValidateTxtFld('partNumber',lblPartNumber);
        fnValidateTxtFld('comments',message[5550]);	
        
	// validation
	if (ErrorCount == 0){
	    fnValidateAndCreateInputStr();
	  }

	// PC-4192: to skip the validation - if only selected Add/Modify checkbox
	if(vBomId =="" && vBomFlag == true) {
		Error_Clear();
	}
	
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return;
	} 
	 if (confirm('Are you Sure, you want to Submit the data?')) {
		 var vInputString = document.frmPartNumberAssembly.inputString.value;
		 var vVoidStr = document.frmPartNumberAssembly.voidinputstring.value;
    	 fnStartProgress('Y');
	     var loader = dhtmlxAjax.postSync('/gmPartNumberAssembly.do?method=savePartNumberAssemblyUploadDtls','&partNumber='+encodeURIComponent(partNumberVal)
						     +'&inputString='+document.frmPartNumberAssembly.inputString.value+'&strPartNumbers='+document.frmPartNumberAssembly.strPartNumbers.value+'&bomId='+vBomId
							 +'&voidinputstring='+document.frmPartNumberAssembly.voidinputstring.value+'&subComponent='+document.frmPartNumberAssembly.subComponent.value+'&bomFlag='+document.frmPartNumberAssembly.bomFlag.value
							 +'&txt_LogReason='+vcomments+'&'+fnAppendCompanyInfo()+'&ramdomId=' + Math.random());
							  fnSaveCallBack(loader);
	
	}
}
//this function used to validate duplicate entry (part) and set the input
//string values,get changed grid data 

function fnValidateAndCreateInputStr(){  
	gridObj.editStop();// retun value from editor(if presents) to cell and closes editor
	
	var hPartInputStr = "";
	var dupPartids = '';
	var empty_qty = '';
	var error_qtyStr = '';
	var partNumberBlankFl = false;
	var gridrowid;
	var void_flag;
	var inputStr='';
	var voidStr='';
	var partStr = '';
	var part_val = '';
	var qty_val = '';
	var typ = '';
	var voidfl = '';
	var voidMapId ='';
	var invalidQty = '';
	var hdupPartInputStr ='';
	var emptyPartCheck = 0;
	// part type drop down validate
	var errorpartTypeFl = false;
	
	var partNumberBlankFl = false;
	
	gridObj.forEachRow(function(row_id) {   // duplicate part_value validation each records check 
		part_val = gridObj.cellById(row_id, part_rowId).getValue();
		qty_val = gridObj.cellById(row_id, qty_rowId).getValue();
		if (part_val != '') {
			if (hdupPartInputStr.indexOf(part_val + ",") == -1) {
				hdupPartInputStr += (part_val + ",");
			 } else {
				if (dupPartids.indexOf(part_val + ",") == -1) {
					dupPartids += (part_val + ", ");
				}
				gridObj.setCellTextStyle(row_id, part_rowId,
						"color:red;border:1px solid red;");
			}
		} 
	});
	
	
	
	var gridChrows = gridObj.getChangedRows(',');
	
	if(gridChrows.length==0){
		inputStr = "";
	}
	else
	{
		var rowsarr = gridChrows.toString().split(",");
		if (rowsarr.length > 0) {
			for (var rowid = 0; rowid < rowsarr.length; rowid++) {
								
				rowId = rowsarr[rowid];
								
				part_val = TRIM(gridObj.cellById(rowId, part_rowId).getValue());
				part_val = part_val.toUpperCase();
				
				qty_val = gridObj.cellById(rowId, qty_rowId).getValue();
				
				voidfl = gridObj.cellById(rowId, flag_rowId).getValue();
			 
				if (part_val != '') {  //PartNum  validation
					if (hPartInputStr.indexOf(part_val + ",") == -1) {
						hPartInputStr += (part_val + ",");
							// Qty validation
							if (qty_val == '') {
								empty_qty += part_val+", ";
								gridObj.setCellTextStyle(rowId, qty_rowId,
										"color:red;border:1px solid red;");
								gridObj.setRowColor(rowId, "ORANGE");
							} else {
								 	invalidQty = qty_val.replace(/-?[.0-9]+/g,''); // qty text box
								 	if(invalidQty.length > 0 || qty_val == 0){  // decimal value and '0'- qty validation 
								     error_qtyStr += part_val+", ";
 								  	 gridObj.setCellTextStyle(rowId, qty_rowId,
											"color:red;border:1px solid red;");
								     } 
								 	if (/^(\-(\d*))$/.test(qty_val)){      // negative qty value validation
								 		 error_qtyStr += part_val+", ";
	 								  	 gridObj.setCellTextStyle(rowId, qty_rowId,
												"color:red;border:1px solid red;");
								 	}
							}	
					} 
				} // end of part_val check
				
				typ_val = gridObj.cellById(rowId, type_rowId).getValue();
				
			    if(typ_val !=''){
			    	// based on text to get the sub component type id
			    	typ = fnGetSubComPartTypeId (typ_val);
			    	
			    	// if incorrect drop down selected - high light rows.
			    	if (typ ==''){
			    		errorpartTypeFl = true;
			    		gridObj.setCellTextStyle(rowId, type_rowId,
						"color:red;border:1px solid red;");
			    	}
			    		
			    	
			    }else{
			    	errorpartTypeFl = true;
			    }
							
			    voidMapId = gridObj.cellById(rowId, map_id).getValue();
				
				if (voidfl == '0' && part_val!='' && qty_val!='' && typ!=''){
					inputStr = inputStr + part_val + '^' + qty_val + '^'+ typ + '|';
				}
				if(voidfl =='1'){  // validation for allow Y character or Empty value.
					voidStr = voidStr + voidMapId +',';
				}
				
				if(part_val !=''){
     				partStr = partStr+ part_val+',';
				}
				

			}
			
		}
		
		if (empty_qty != '') {
			empty_qty = empty_qty.substring(0,(empty_qty.length)-2);

			Error_Details(Error_Details_Trans(
					"Quantity cannot be empty. Please enter valid quantity for the Part Number(s) - [0]", empty_qty));
		}

		// Error message for invalid Part Cost..
		if (error_qtyStr != '') {
			error_qtyStr = error_qtyStr.substring(0,(error_qtyStr.length)-2);
			Error_Details(Error_Details_Trans(
					"Please enter Qty greater than zero for Part# - [0] ",error_qtyStr));
		}

		// Error message for Duplicate Part Entries..
		if (dupPartids != '' && TRIM(dupPartids).length > 1) {
			dupPartids = dupPartids.substr(0, (TRIM(dupPartids).length - 1));
			error_msg = Error_Details_Trans(
					"Duplicate Part Number(s) Found - [0] ", dupPartids);
			Error_Details(error_msg);
		}
	}
	
	// to check and show the drop down validate
	if(errorpartTypeFl)
		Error_Details("Please select the Part's Type");
	
	if (inputStr == "" && voidStr == "" && voidMapId== "") {   
		if(ErrorCount == 0){
		     Error_Details("Please provide valid input details ");
		}
	} else {
		document.frmPartNumberAssembly.inputString.value =  inputStr;
		document.frmPartNumberAssembly.strPartNumbers.value = partStr;
		document.frmPartNumberAssembly.voidinputstring.value = voidStr;
	}
	
}
//function used to call back after submit access
function fnSaveCallBack(loader){
	fnStopProgress();
	var strpartValidateFl = '';
	var strInvaliPart = '';
	var strPartNumber = '';
	var partDuplicateErrFl = false;
	var response = loader.xmlDoc.responseText;
	
	//split as array
	var b = response;
	var temp = new Array();
	temp = b.split('##');
	strpartValidateFl = temp[0];
	strInvaliPart = temp[1];
	
	if(strpartValidateFl == "N" && strInvaliPart != ""){
				//
				var tempPartNumber = new Array();
				tempPartNumber = strInvaliPart.split(',');
				for(var i=0;i<=tempPartNumber.length;i++){
				gridObj.forEachRow(function(rowId) {
					// check row id set priority equal if both equal and also
					// partValidateFl  value is N
					// highlight the cell throw validation
					part_val = gridObj.cellById(rowId, part_rowId).getValue();
					if (part_val != '' && (part_val == tempPartNumber[i]) && strpartValidateFl == 'N' ) {
							gridObj.setCellTextStyle(rowId, part_rowId,
									"color:red;border:1px solid red;");
							partDuplicateErrFl = true;
					}

				});// 
				}
			
			// if flag value true set validations
			if (partDuplicateErrFl) {
				Error_Details('The highlighted <b>Part # (s)</b> do not exist');
			}
			if (ErrorCount > 0) {
				Error_Show();
				Error_Clear();
				return false;
			}
	} else if(strpartValidateFl == "Y"){
		document.getElementById("progress").style.display="block";
		document.getElementById("msg").style.color="green";
		document.getElementById("msg").style.display="inline";		
		document.getElementById("msg").innerHTML= "Data Saved Successfully";
		document.getElementById('comments').value = "";
		fnLoadPartAssemblyDtls();
	}else if (strpartValidateFl != ''){  // common error screen 
		document.write(strpartValidateFl);
	}
}

function fnCheckPartNum(pnum){
	fnClearDiv();
	// to set the part # upper case
	var partVal = TRIM(pnum).toUpperCase();
	document.frmPartNumberAssembly.partNumber.value = partVal;
	
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/GmPartNumAjaxServlet?strOpt=CHECKPARTNUM&partNum='+ encodeURIComponent(partVal) + '&ramdomId=' + Math.random());
		var loader = dhtmlxAjax.getSync(ajaxUrl);
		fnShowImage(loader,pnum);
	document.getElementById("progress").style.display="none";
	document.getElementById("msg").style.display="none";	
}

//To Hide the image
function fnClearDiv(){
	document.getElementById("DivShowPartExists").style.display = 'none';
	document.getElementById("DivShowPartAvail").style.display = 'none';
	
	// to clear the grid data
	if(gridObj !=''){
		gridObj.clearAll();
	}
	
	document.getElementById("partDesc").innerHTML='';
	document.getElementById("bomid").innerHTML= '';
	document.getElementById("prodFamily").innerHTML='';
	document.getElementById("prodClass").innerHTML='';

}

function fnShowImage(loader,pnum) {
	var response = loader.xmlDoc.responseText;
	if(response != null){
		if(response == '0'){
			document.getElementById("DivShowPartExists").style.display = 'inline-block';
			document.getElementById("DivShowPartAvail").style.display = 'none';
			Error_Details('Please enter valid <b>Part Number </b>');
			if (ErrorCount > 0) {
				Error_Show();
				Error_Clear();
				return false;
			}
		}
		else{
			document.getElementById("DivShowPartAvail").style.display = 'inline-block';
			document.getElementById("DivShowPartExists").style.display = 'none';
		}
		fnReload ();
	}
}

// to reload the page
function fnReload (){
	var strPartNumber = TRIM(document.frmPartNumberAssembly.partNumber.value);
	var vSubComponent = document.frmPartNumberAssembly.subComponent.value;
	// progress
	fnStartProgress('Y');
	// add encodeURIComponent for this PC-2368 BOM mapping - Remove void access
	document.frmPartNumberAssembly.action = '/gmPartNumberAssembly.do?method=reLoadPartNumberAssembly&partNumber='+encodeURIComponent(strPartNumber) +"&subComponent="+vSubComponent;
	document.frmPartNumberAssembly.submit ();
}

function fnPartDetail(pnum){
	// PC-4192: To reset the default values
	document.frmPartNumberAssembly.bomId.value = '';
	document.frmPartNumberAssembly.inputString.value = '';
	document.frmPartNumberAssembly.voidinputstring.value = '';
	
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmPartNumberAssembly.do?method=fetchPartHeaderDtls&partNumber='+ encodeURIComponent(pnum) + '&ramdomId=' + Math.random());
	dhtmlxAjax.get(ajaxUrl, fnPartDetailResponse);
}

// function used to show the header details 
function fnPartDetailResponse(loader){
	var response = JSON.parse(loader.xmlDoc.responseText);
	//PC-2368 BOM mapping - Remove void access
	var vSubComponent = document.frmPartNumberAssembly.subComponent.value;
//	 JSON.parse(loader.xmlDoc.responseText
	if(response[0].partDesc == '' || response[0].partDesc == null){
	   document.getElementById("partDesc").innerHTML='';
	}else{
	   document.getElementById("partDesc").innerHTML=response[0].partDesc;
	}
	if(response[0].setid == '' || response[0].setid == null){
	  document.getElementById("bomid").innerHTML= '';
	}else{
		document.getElementById("bomid").innerHTML= response[0].setid;
		document.frmPartNumberAssembly.bomId.value=response[0].setid;
	}
	
	if(response[0].family == '' || response[0].family == null){
	   document.getElementById("prodFamily").innerHTML='';
	}else{
	   document.getElementById("prodFamily").innerHTML = response[0].family;
	}
	if(response[0].prodclass =='' || response[0].prodclass == null){
	    document.getElementById("prodClass").innerHTML='';
	}else{
		document.getElementById("prodClass").innerHTML = response[0].prodclass;
	}
	
	//PC-2368 BOM mapping - Remove void access
	var vClass = response[0].bomcheckfl; 
	
	//PC-2368 BOM mapping - Remove void access
	//Enable checked box particular family parts and Sub-Component Action (109740 - Part Number) only.
	if(vClass == 'Y' && vSubComponent == 109740){
		document.getElementById("bomFlag").disabled = false;
	}else{
		document.getElementById("bomFlag").disabled = true;
		document.frmPartNumberAssembly.bomFlag.checked = false;
	}
	
	var vBomId = response[0].setid;
	if(vBomId != '' && vBomId !=null){
		document.getElementById("bomFlag").disabled = true;
		document.frmPartNumberAssembly.bomFlag.checked = true;
	}	
	
	
}
 
//function used load button 
function fnLoadPartAssemblyDtls(){
	  var partNumberVal = TRIM(document.frmPartNumberAssembly.partNumber.value);
	  var actionTypeVal = TRIM(document.frmPartNumberAssembly.subComponent.value);
	
	  // load here
	  fnPartDetail(partNumberVal);
	  
	if(actionTypeVal!=0){
	 var loader = '';
		if(partNumberVal != ''){
			fnStartProgress();
			var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmPartNumberAssembly.do?method=fetchPartNumberAssemblyDtls&partNumber='+encodeURIComponent(partNumberVal)+'&subComponent='+encodeURIComponent(actionTypeVal)+'&ramdomId='+Math.random());
			loader = dhtmlxAjax.getSync(ajaxUrl);
			fnLoadCallBack(loader);
			fnStopProgress();
		}else{
			 Error_Details("Please enter the <b>Part Number</b>");
				if (ErrorCount > 0) {
				    Error_Show();
				    Error_Clear();
				    return false;
				}
		}
	}else{
		 Error_Details("Please Choose a valid <b>Sub component action</b>");
			if (ErrorCount > 0) {
			    Error_Show();
			    Error_Clear();
			    return false;
			}
	}
}

//function used addRow
function addRow(){
	var uid = gridObj.getUID();
	gridObj.addRow(uid,'');
	gridObj.setRowAttribute(uid,"row_added","Y");
	// to set the default values
	gridObj.cellById(uid, type_rowId).setValue(30031);
	
	// PC-4192: to set the default qty as 1
	gridObj.cellById(uid, qty_rowId).setValue(1);
}

//function used removeSelectedRow
function removeSelectedRow(){
	var gridrows=gridObj.getSelectedRowId();
	var isExistRow=false;
	
	if(gridrows!=undefined){
		var gridrowsarr = gridrows.toString().split(",");
		var added_row;
		for (var rowid = 0; rowid < gridrowsarr.length; rowid++) {
			gridrowid = gridrowsarr[rowid];
			added_row = gridObj.getRowAttribute(gridrowid,"row_added");
			
			if(added_row!=undefined && added_row=="Y"){
				
				gridObj.deleteRow(gridrowid);
			}else{
				isExistRow = true;
			}
		}
		if(isExistRow){
			Error_Clear();
			Error_Details(message[501]);				
			Error_Show();
			Error_Clear();
			
		}
		gridObj.clearSelection();
	}else{
		Error_Clear();
		Error_Details(message[502]);				
		Error_Show();
		Error_Clear();
	}
}

//this function used to paste data from excel
function fnAddRowFromClipboard() {
	var cbData = gridObj.fromClipBoard();
	if (cbData == null) {
		Error_Details("Please copy valid data from excel");
	}
	fnRemoveEmptyRow();
	var rowData = cbData.split("\n");
	var rowcolumnData = '';
	var arrLen = (rowData.length) - 1;
	
	if (ErrorCount > 0) {
		fnStopProgress();
		Error_Show();
		Error_Clear();
		return;
	}
	
	fnStartProgress('Y');
	var newRowData = '';
	setTimeout(function() {
		gridObj.startFastOperations();
		gridObj.filterBy(0, "");// unfilters the grid
		gridObj._f_rowsBuffer = null; // clears the cache
		for (var i = 0; i < arrLen; i++) {

			newRowData = rowData[i];
			newRowData = newRowData.replace(/\n|\r/g, "");
			var rowcolumnData = '';
			var row_id = gridObj.getUID();
			gridObj.setCSVDelimiter("\t");
			gridObj.setDelimiter("\t");
			gridObj.addRow(row_id, newRowData);
			gridObj.setRowAttribute(row_id, "row_added", "Y");
			gridObj.setRowAttribute(row_id, "row_modified", "Y");
			gridObj.setDelimiter(",");
			
			// PC-4192: To get the type values and MS parts then default to Modified Supply
			var partNumVal =  gridObj.cellById(row_id, part_rowId).getValue();
			//console.log (partNumVal);
			partNumVal = partNumVal.toUpperCase();
			if(partNumVal.indexOf('MS.') !== -1){
				// Modified Supply
				gridObj.cellById(row_id, type_rowId).setValue(30032);
			}
			// to get the Type values and empty then, set default values
			if(gridObj.cellById(row_id, type_rowId).getValue() == '')
				gridObj.cellById(row_id, type_rowId).setValue(30031);
		}
		
		gridObj.stopFastOperations();
		fnStopProgress();
		gridObj.filterByAll();
	}, 100);
}

//this function used to remove the empty rows
function fnRemoveEmptyRow() {
	gridObj.forEachRow(function(row_id) {
		part_val = gridObj.cellById(row_id, part_rowId).getValue();
		if (part_val == '') {
			gridObj.deleteRow(row_id);
		}
	});
}

// This function used to change dropdown - to reload the page.
function fnChangeAction (){
	document.frmPartNumberAssembly.partNumber.value = '';
	fnReload ();
}


//this function used to return the part subcompoent id based on the code name.
function fnGetSubComPartTypeId (part_drop_down_Val) {
	var subCompType_Id = '';
	var partTypeVal = '';

	if (part_drop_down_Val != '' && part_drop_down_Val != undefined) {
		partTypeVal = part_drop_down_Val;
		
		if(isNaN(partTypeVal)){
			partTypeVal = TRIM(part_drop_down_Val).toUpperCase();
		}
		
		for ( var i = 0; i < arrayPartType.length; i++) {
			subCompPartTypeCol = arrayPartType[i];
			subCompPartTypeArr = subCompPartTypeCol.split(',');
			subCompPartTypeId = subCompPartTypeArr[0];
			subCompPartTypeNm = subCompPartTypeArr[1];
			if (partTypeVal == (subCompPartTypeNm).toUpperCase() || partTypeVal == subCompPartTypeId) {
				subCompType_Id = subCompPartTypeId;
				break;
			}
		}
	}

	return subCompType_Id;
}