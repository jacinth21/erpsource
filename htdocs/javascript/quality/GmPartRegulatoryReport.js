//This function is used to initiate grid
var gridObj;
function fnOnPageLoad(){
	var sel = document.all.hSearch.value;
	if(sel != ''){
		document.all.Cbo_Search.value = sel;
	}
	if(objGridData.value != ''){ 
		gridObj = initGridData('dataGridDiv',objGridData);
	}
}

function initGridData(divRef,gridData){
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);	
	gObj.init();
	gObj.loadXMLString(gridData);	
	return gObj;	
}
// Function to load the values 
function fnLoad(){
	var frm = document.frmPartParameterDtls;
	var partNum = TRIM(frm.regulatoryPartNum.value);
	if(document.all.Cbo_Search){
		var cboSearch = document.all.Cbo_Search.value;
		if(partNum == '' && cboSearch != '0'){
			Error_Details(message_prodmgmnt[14]);
		}
	}	
	if (ErrorCount > 0){
				Error_Show();
				Error_Clear();
				return false;
	}
	
	frm.strOpt.value = 'Load';
	frm.action = "/gmPartParameterDtls.do?method=udiRegulatoryReports";
	fnStartProgress();
	frm.submit();
		
}

//Function to export excel
function fnExport()
{	
	gridObj.toExcel('/phpapp/excel/generate.php');
}

//This function is used to activate Load button while pressing the enter key
function fnEnter() {
	if(event.keyCode == 13){
		fnLoad();
	}
}