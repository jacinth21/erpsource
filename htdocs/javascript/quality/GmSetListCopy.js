/**********************************************************************************
 * File:        /htdocs/javascript/quality/GmSetListCopy.js
 * Description: SEt List Copy functionalities goes here
 * Version:     1.0
 * Author:     	jgurunathan
 **********************************************************************************/

//function to submit the form.This will save the form details.
function fnSubmit(form) {
    var strFromSetId = TRIM(document.frmSetListCopy.txtFromSetID.value);
    var strToSetId = TRIM(document.frmSetListCopy.txtToSetID.value);
    strFromSetId = strFromSetId.toUpperCase();
    strToSetId = strToSetId.toUpperCase();

    fnValidateTxtFld('txtFromSetID', "From Set ");
    fnValidateTxtFld('txtToSetID', "To Set ");
    fnValidateTxtFld('txt_LogReason', "Comments ");

    if (strFromSetId != "" && strToSetId != "" && strFromSetId == strToSetId) {
        Error_Details("From Set and To set shoud not be the same Set");
    }
    if (ErrorCount > 0) {
        Error_Show();
        Error_Clear();
        return false;
    }

    document.frmSetListCopy.txtFromSetID.value = strFromSetId;
    document.frmSetListCopy.txtToSetID.value = strToSetId;
    if(confirm('Are you Sure, you want to Submit the data?')) {
	    fnStartProgress('Y');
	    document.frmSetListCopy.action = '/gmSetListCopy.do?method=saveSetListCopy';
	    document.frmSetListCopy.submit(); 
    }

}

//function used to set the FROM SET value to the form.
function fnUpdateFromSet() {
    document.frmSetListCopy.txtFromSetID.value = document.frmSetListCopy.fromSetId.value;
}

//function used to set the TO SET value to the form.
function fnUpdateToSet() {
    document.frmSetListCopy.txtToSetID.value = document.frmSetListCopy.toSetId.value;
}

//function used to validate the SET.
function fnValidateSet(setObj, action) {
    var setIdVal = TRIM(setObj.value);
    var ajaxUrl = fnAjaxURLAppendCmpInfo('/GmSetMapServlet?hAction=validateSet&setId=' + setIdVal + '&strType=SETMAP');
    var loader = dhtmlxAjax.getSync(ajaxUrl);
    fnCallbackValidateSet(loader, action);

}

//function used to callback from AJAX validate the SET values.
function fnCallbackValidateSet(loader, action) {
    response = loader.xmlDoc.responseText;

    if (response != "") {
        if (action == 'From') {
            document.frmSetListCopy.fromSetId.value = document.frmSetListCopy.txtFromSetID.value;
            document.frmSetListCopy.searchfromSetId.value = response;
        } else if (action == 'To') {
            document.frmSetListCopy.toSetId.value = document.frmSetListCopy.txtToSetID.value;
            document.frmSetListCopy.searchtoSetId.value = response;
        }

    } else {

        if (action == 'From') {
            document.frmSetListCopy.fromSetId.value = '';
            document.frmSetListCopy.searchfromSetId.value = '';
        } else if (action == 'To') {
            document.frmSetListCopy.toSetId.value = '';
            document.frmSetListCopy.searchtoSetId.value = '';
        }

        Error_Details("<b>" + action + "</b>" + " Set ID does not exist, please enter a valid Set ID");

    }

    if (ErrorCount > 0) {
        Error_Show();
        Error_Clear();
        return false;
    }

}