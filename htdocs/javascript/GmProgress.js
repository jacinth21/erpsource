
/*this function is used to disable the screen with color.*/
    fnDisableScreen = function()
    {
        $("BODY").append('<div id="processing_overlay"></div>');
		$("#processing_overlay").height( $(document).height() );
		$("#processing_overlay").width( $(document).width() );
    }
    
/*this function is used to enable the screen.*/
    
    fnEnableScreen = function()
    {
        $("#processing_overlay").remove();
    }

/*this function is used to show the progress bar window.*/
    
    fnShowProgressBar = function()
    {
        fnHideProgressBar();
        parent.parent.LeftFrame.fnDisableScreen();
            $("BODY").append(
		        '<div id="processing_container">' +
		        '<div id="processing_content">' +
    		            '<img src="/images/progress.gif" style="width:215px;height:18px;" alt=""/>' +
    		            '&nbsp;<br/>Processing....' +
    		          '</div>' +
		      '</div>');
		//Progress Bar alignment issue Due To jquery.js updating .so ,for this problem changed window to document.
		var left = (($(document).width() / 2) - ($("#processing_container").outerWidth() / 2)) + 0; 
		if( left < 0 ) left = 0;
		var top=7; // to show progress bar at top frame
		$("#processing_container").css({
			top: top + 'px',
			left: left + 'px'
		});
    }

/*this function is used to hide the progress bar window.*/
    
    fnHideProgressBar = function()
    {
    	window.parent.LeftFrame.fnEnableScreen();
        $("#processing_container").remove();
    }
    
/*this function is used to show the progress bar window , disable the left link screen and current screen.*/
    
    function fnStartProgress(disabFl){
    	if(parent.parent.TopFrame)
    	parent.parent.TopFrame.fnShowProgressBar();
        if(disabFl!= 'N')
    	fnDisableScreen();
    }
    
/*this function is used to hide the progress bar window , enable the left link screen and current screen.*/
    function fnStopProgress(){
    	if(parent.parent.TopFrame)
    	parent.parent.TopFrame.fnHideProgressBar();
    	fnEnableScreen();
    }
    
 /* this function used to disable the button */
    
    function fnDisableButton(frmName,btnName){
    	eval("document."+frmName+"."+btnName).disabled=true;
    }
 
/* this function used to enable the button */
    
    function fnEnableButton(frmName,btnName){
    	eval("document."+frmName+"."+btnName).disabled=false;
    }
    