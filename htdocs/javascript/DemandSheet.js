function fnApprove(form)
{
    var appcomments =  form.appcomments.value;
	var demandSheetMonthId = form.demandSheetMonthId.value;
	loadajaxpage('/gmDSSummary.do?strOpt=approve&appcomments='+appcomments+'&demandSheetMonthId='+demandSheetMonthId,'ajaxdivtabarea');
    fnApproveReload();
}

function fnApproveReload()
{
	var parts = '';
	var groups ='';
	var overridefl ='';
	var demandSheetMonthId = document.all.demandSheetMonthId.value;
   	var yearId = document.all.hYearId.value;
   	var demandMasterID = document.all.hDemandMasterID.value;
   	var monthId = document.all.hMonthID.value;
   	
	loadajaxpage('/gmDSSummary.do?demandSheetMonthId='+demandSheetMonthId+'&strOpt=report&yearId='+yearId+'&partNumbers='+encodeURIComponent(parts)+'&checkedGroupsString='+groups+'&demandSheetId='+demandMasterID+'&monthId='+monthId+'&overrideFlag='+overridefl,'ajaxreportarea');
}

function fnCallTPRReport (pnum)
{
	var invid = document.frmTTPMonthlySummary.inventoryId.value;
    
    windowOpener("/gmTPRReport.do?method=fetchTPRReport&pnum="+encodeURIComponent(pnum)+"&inventoryId="+invid,"INVPOP","resizable=yes,scrollbars=yes,top=40,left=20,width=900,height=400");

	//windowOpener("/gmTPRReport.do?method=fetchTPRReport&pnum="+pnum+"&monYear="+monYear+"&inventoryId="+inventoryId,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=900,height=400");			

}

function fnFetch(demandSheetId,demandMasterId,yearId,monthId)
{	
	var unitprice = '';
	var overridefl = 'N';
	var parts = document.all.partNumbers.value ;
	var forecastmonths = document.all.forecastMonths.value ;
	groups = getCheckedItems(); 
	document.all.checkedGroupId.value ;
	overrideflstatus = ''//document.all.overrideFlag.status ;
	unitpriceobj = document.all.unitrprice;
	
	if (overrideflstatus)
	{
		overridefl = 'Y';
	}
	
	for(var n=0;n<unitpriceobj.length;n++)
	{
		if (unitpriceobj[n].checked)
		{
				unitprice = unitpriceobj[n].value;
				break;
		}
	}	
	loadajaxpage('/gmDSSummary.do?demandSheetMonthId='+demandSheetId+'&strOpt=report&yearId='+yearId+'&partNumbers='+encodeURIComponent(parts)+'&checkedGroupsString='+groups+'&demandSheetId='+demandMasterId+'&monthId='+monthId+'&unitrprice='+unitprice+'&forecastMonths='+forecastmonths+'&overrideFlag='+overridefl,'ajaxreportarea');	
}	

function getCheckedItems()
{
	var objProject = document.all.checkedGroupId;
	var setlen = objProject.length;
	var setstr = '';
	
	var temp = '';
	var items='';
		
	for (var i=0;i<setlen;i++)
	{
		if (objProject[i].checked == true)
		{
			setstr = setstr + objProject[i].value + ',';
		}
		items = setstr.substr(0,setstr.length-1);
	}
	
	return items;
}
   
function fnCallAD(id, type) {					   
 windowOpener("/GmSetReportServlet?hAction=Drilldown&hOpt=SETRPT&hSetId="+id,"PrintInv","resizable=yes,scrollbars=yes,top=250,left=300,width=900,height=400");			
}

function fnCallAD(id, type, name) {					   
windowOpener("/GmSetReportServlet?hAction=Drilldown&hOpt=SETRPT&hSetId="+id+"&hSetNm="+name,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=900,height=400");			
}

 function fnCallSetNumber(val)
{
	alert(val);
}

function fnCallGroupMap(val, type)
{
	windowOpener("/gmGroupPartMap.do","PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=900,height=400");
}

function fnCallDisp(id, type)
{	
    var temp = new Array();
    temp = id.split('^');
	var partNum = temp[0];  
	windowOpener("/GmPartNumServlet?hAction=LoadPartDetail&partNumber="+encodeURIComponent(partNum),"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=760,height=600");
}

function fnSubComponentDrillDown(id)
{
	var forecastmonths = document.all.forecastMonths.value ;
	var dsheetID = document.all.demandSheetMonthId.value;
	var yearId = document.all.hYearId.value; 
   	var monthId = document.all.hMonthID.value;
	var temp = new Array();
	 	    temp = id.split('^');
		    var partNum = temp[0]; 
			var refid =  temp[1];
  // alert("/gmDSSummary.do?strOpt=allpartdrilldown&demandSheetMonthId="+dsheetID+"&partNumbers="+partNum+"&yearId="+yearId+"&monthId="+monthId+"&refId="+refid+"&forecastMonths="+forecastmonths);
 	windowOpener("/gmDSSummary.do?strOpt=allpartdrilldown&demandSheetMonthId="+dsheetID+"&partNumbers="+encodeURIComponent(partNum)+"&yearId="+yearId+"&monthId="+monthId+"&refId="+refid+"&forecastMonths="+forecastmonths,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=900,height=400");
}

function fnCallGroupMap(val, type)
{
	windowOpener("/gmGroupPartMap.do?strOpt=40045&groupId="+val+"&haction=edit","PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=900,height=400");	
}		

function fnCallEditPAR(id, dsid, dmid)
		{
	  	var temp = new Array();
	 	    temp = id.split('^');
		    var partNum = temp[0];      
		windowOpener("/gmDSParSetup.do?partNumbers="+encodeURIComponent(partNum)+"&demandMasterId="+dmid+"&demandSheetId="+dsid+"&strOpt=edit&popType=pop","PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=925,height=300");			
		}

function fnCallConsD(id, type, dsheetID)
		{
		
			var formattedid = id.replace('^','-');
			windowOpener("/GmCommonLogServlet?hType=1228&hID="+dsheetID+ formattedid ,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
		}

function fnViewComments(partnumber, link, sheetid, type)
{
	var hHideComment = '';
	partnumber = partnumber.substring(0,7);

	var dsheetID = sheetid + partnumber   ;
	windowOpener("/GmCommonLogServlet?hType=1228&hID="+dsheetID,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
}

function fnColumnDrillDown(row,col,val)
{
	var b = row;
	var temp = new Array();
	temp = b.split('^');
	var sheetid = document.all.demandSheetId.value;
	var month = document.all.monthId.value;
	var monthid = document.all.demandSheetMonthId.value;
	
	var typeid = document.all.demandTypeId.value;

	var year = col;
	var overrideTypeValue = 20396;
	var currentValue = val;
	var refType = 0;
	
	if(typeid == 40020)
	 {
	  refType = 20295;
	 }
	 else
	 {
	  refType = 20296;
	 }
//	 alert("/GmPageControllerServlet?strPgToLoad=gmOverride.do&references="+year+"&overrideTypeId="+overrideTypeValue+"&refValue="+temp[0]+"&currentValue="+currentValue+"&refId="+sheetid+"&refIdForLogReason="+monthid+"&demandSheetMonthId="+monthid+"&refType="+refType+"&partType="+temp[1]);
   	windowOpener("/GmPageControllerServlet?strPgToLoad=gmOverride.do&references="+year+"&overrideTypeId="+overrideTypeValue+"&refValue="+temp[0]+"&currentValue="+currentValue+"&refId="+sheetid+"&refIdForLogReason="+monthid+"&demandSheetMonthId="+monthid+"&refType="+refType+"&partType="+temp[1],"Override","resizable=yes,scrollbars=yes,top=250,left=300,width=750,height=375");
  
}

function fnCallMnGrowth(row,col,val)
{
	
	var monthid = document.all.demandSheetMonthId.value;
	var year = col;	
	var typeid = document.all.demandTypeId.value;
	var sheetid = document.all.hDemandMasterID.value;
	var month = document.all.hMonthID.value;
	
	var overrideTypeValue = '';
	if (typeid == 40020)
	{
		overrideTypeValue = 20398;
	}else{
		overrideTypeValue = 20399;
	}
	var currentValue = val;
	var refType = 0;
	
	if(typeid == 40020)
	 {
	  refType = 20295;
	 }
	 else
	 {
	  refType = 20296;
	  }
	if(TRIM(currentValue) == '-')
    {
    	Error_Details(message[5159]);
    }
    if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
	}else{
		windowOpener("/GmPageControllerServlet?strPgToLoad=gmOverride.do&references="+year+"&overrideTypeId="+overrideTypeValue+"&refValue="+row+"&currentValue="+currentValue+"&refId="+row+"&demandSheetMonthId="+monthid+"&refType="+refType+"&demandMasterId="+sheetid,"Override","resizable=yes,scrollbars=yes,top=250,left=300,width=750,height=375");
	}
}

function fnFetchGrowthInfo()
{   
	var parts = '';
	var groups ='';
	var overridefl ='';
	var demandSheetMonthId = document.all.demandSheetMonthId.value;
   	var yearId = document.all.hYearId.value;
   	var demandMasterID = document.all.hDemandMasterID.value;
   	var monthId = document.all.hMonthID.value;
   	
	loadajaxpage('/gmDSSummary.do?demandSheetMonthId='+demandSheetMonthId+'&strOpt=report&yearId='+yearId+'&partNumbers='+encodeURIComponent(parts)+'&checkedGroupsString='+groups+'&demandSheetId='+demandMasterID+'&monthId='+monthId+'&overrideFlag='+overridefl,'ajaxreportarea');
	
	var demandtab=new ddajaxtabs("demandtab", "ajaxdivtabarea");				
	demandtab.setpersist(false);								
	demandtab.init();
	demandtab.expandit('growthinfo');					   				    	 
}

function fnCallActuals(id, type)
 {
	var b = id;
    var temp = new Array();
    temp = b.split('^');
	var dsheetID = document.all.demandSheetMonthId.value;// <bean:write name="frmDemandSheetSummary" property="demandSheetMonthId"/>;
	var partNum = temp[0];
	var groupId = temp[1];

	windowOpener("/gmDSSummary.do?strOpt=partdrilldown&demandSheetMonthId="+dsheetID+"&partNumbers="+encodeURIComponent(partNum)+"&groupInfo="+groupId,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=900,height=400");
 }

function fnMediateReqEdit(requestId, requestFor, setId){
	if (setId == '')
	{
		fnReqItemEdit(requestId);
	}
	else {
		fnReqEdit(requestId, requestFor);
	}
}

function fnReqItemEdit(requestId){
			windowOpener("/gmRequestDetail.do?requestId="+requestId ,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=900,height=600");			
}

function fnReqEdit(requestId, requestFor)
{
		windowOpener("/gmRequestEdit.do?FORWARD=gmIncShipDetails&RE_FORWARD=gmRequestHeader&RE_RE_FORWARD=gmConsignSetServlet&hAction=EditLoad&hMode=CON&source=50184&screenType=Consign&FORMNAME=frmRequestHeader&hRequestView=headerView&requestId="+requestId+"&requestFor="+ requestFor +"&hRequestId="+requestId,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=780,height=600");			
	 	   
}

function fnRequestDetails(strReqId,strConId)
    {
		windowOpener("/gmRequestMaster.do?strOpt=print&requestId="+strReqId+"&consignID="+strConId,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=920,height=600");
    }
    

function fnConsignmentVer(strConId)
	{
		windowOpener("/GmConsignSetServlet?hAction=PrintVersion&hId="+strConId,"Con","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
	}

function fnCallPPP(id, pstatus, dsid, dmid)
		{
		var temp = new Array();
		    temp = id.split('^');
		    var partNum = temp[0];
		    var setID = temp[1];
		windowOpener("/gmTPRReport.do?method=fetchTPRReport&pnum="+encodeURIComponent(partNum)+"&pstatus="+pstatus+"&demandSheetMonthId="+dsid+"&demandSheetId="+dmid+"&setId="+setID+"&popType=pop","PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=925,height=300");		
		}

function fnCallMultiSheet(id, dmid)
		{
		var temp = new Array();
		    temp = id.split('^');
		    var partNum = temp[0];		
		windowOpener("/gmOprMultiSheetDetails.do?partDetails="+encodeURIComponent(partNum)+"&demandSheetMonthId="+dmid+"&forecastMonths=4&strOpt=multisheetdetails","PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=925,height=300");		
		}

function fnCallMultiSheetTTP(id)
		{
		var dmid = '';
		var temp = new Array();
		    temp = id.split('^');
		    var partNum = temp[0];		
		windowOpener("/gmOprMultiSheetDetails.do?partDetails="+encodeURIComponent(partNum)+"&demandSheetMonthId="+dmid+"&forecastMonths=4&strOpt=multisheetdetails","PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=925,height=300");		
		}
function fnPullOpenSheets(form)
{
	objMultiSheetArr = form.multiSheet;
	objMultiSheetArrLen = objMultiSheetArr.length;
	dmidString = "";	
	var sheetSelected = false;
	var demandSheetMonthId = document.all.demandSheetMonthId.value;
	if(objMultiSheetArr.id != undefined)
	{		
		if(objMultiSheetArr.checked)
			{
				 dmidString = objMultiSheetArr.id;
				 sheetSelected = true;	
			}	
	}
	else
	{
		for(i = 0; i < objMultiSheetArrLen; i ++) 
		{	
			if(objMultiSheetArr[i].checked)
			{
			  dmidString = dmidString + "," + objMultiSheetArr[i].id;
			  sheetSelected = true;
			}				 
		}
    }

    if(!sheetSelected)
	{
		 alert(message[5160]);
		 return false;
	}

	form.pull.disabled = true;
	loadajaxpage('/gmOprPullOpenSheets.do?strOpt=pullopensheets&demandSheetString='+dmidString+"&demandSheetMonthId="+demandSheetMonthId,'ajaxdivtabarea');
	fnReloadOpenSheets();
}

function fnReloadOpenSheets()
{
	var parts = '';
	var groups ='';
	var overridefl ='';
	var demandSheetMonthId = document.all.demandSheetMonthId.value;
   	var yearId = document.all.hYearId.value;
   	var demandMasterID = document.all.hDemandMasterID.value;
   	var monthId = document.all.hMonthID.value;
	var unitprice = '';
	var overridefl = 'N';

	loadajaxpage('/gmDSSummary.do?demandSheetMonthId='+demandSheetMonthId+'&strOpt=report&yearId='+yearId+'&partNumbers='+encodeURIComponent(parts)+'&checkedGroupsString='+groups+'&demandSheetId='+demandMasterID+'&monthId='+monthId+'&unitrprice='+unitprice+'&forecastMonths=4&overrideFlag='+overridefl,'ajaxreportarea');		
}

function fnOpenOrdLogInv(id)
{
	windowOpener("/GmCommonLogServlet?hType=1228&hID="+id,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
}

function fnOpenReqLog(id)
{
	windowOpener("/GmCommonLogServlet?hType=1239&hID="+id,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
}


function fndemdisp(id,val,name)
{
	//windowOpener("/gmRequestMaster.do?strOpt=print&setID="+id+"&setID="+val+"&NAME="+name,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=920,height=600");
}


function replaceAll(OldString,FindString,ReplaceString) 
{
	var SearchIndex = 0;
  	var NewString = ""; 
  	while (OldString.indexOf(FindString,SearchIndex) != -1)    
	{
    	NewString += OldString.substring(SearchIndex,OldString.indexOf(FindString,SearchIndex));
    	NewString += ReplaceString;
    	SearchIndex = (OldString.indexOf(FindString,SearchIndex) + FindString.length);         
	}
  	NewString += OldString.substring(SearchIndex,OldString.length);
  	return NewString;
}

function fnRequestGrowthDrillDown(firstDayOfMonth,lastDayOfMonth,refId,refType){
	var demandMasterID = document.all.hDemandMasterID.value;
	if (refType == 20296)
	{
		windowOpener("/gmRequestMaster.do?requiredDate="+firstDayOfMonth+"&requiredDateTo="+lastDayOfMonth+"&setID="+refId+"&requestStatus=0&strOpt=reload&requestStatusoper=0&requestFor=0&requestSource=0&requestTo=0&requestTxnID="+demandMasterID,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=50,width=900,height=600");
	}
	else{
		windowOpener("/gmRequestMaster.do?requiredDate="+firstDayOfMonth+"&requiredDateTo="+lastDayOfMonth+"&partNum="+encodeURIComponent(refId)+"&setOrPart=50637&requestStatus=0&strOpt=reload&requestStatusoper=0&requestFor=0&requestSource=0&requestTo=0&requestTxnID="+demandMasterID,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=50,width=900,height=600");
	}

}
function fnCallPPQtyDetails(pnum, ppqty)
{
	var invid = document.frmTTPMonthlySummary.inventoryId.value;
	//alert("pnum: "+ pnum+ "--ppqty:" +ppqty )
    windowOpener("/gmPPQtyDrillDownAction.do?partNumber="+encodeURIComponent(pnum)+"&parentPartQty="+ppqty+"&strOpt=PPQDrillDown"+"&inventoryLockId="+invid,"PPQTYPOP","resizable=yes,scrollbars=yes,top=40,left=20,width=900,height=400");
}