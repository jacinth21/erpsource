function fnOnLoad() {

	var strOpt = document.frmCommonCurrConvForm.strOpt.value;
	var convType = document.frmCommonCurrConvForm.convType.value;
    var strRateFlag =  document.frmCommonCurrConvForm.hRateFlag.value;

	// If loaded from Currency Conversion Report Edit icon, disable all fields except Currency Value
	if (strOpt == 'edit')
	{
		// If the Conversion Type is "Constant" enable Currency Value and From/To Dates
		if (convType == 101103)// Constant
		{
			// Disable editing fields except Currency Value and From/To Dates
		    fnEnableAll('N');
			document.frmCommonCurrConvForm.currValue.disabled = false;
		    document.frmCommonCurrConvForm.dtCurrFromDT.disabled = false;
			document.frmCommonCurrConvForm.dtCurrToDT.disabled = false;
			document.frmCommonCurrConvForm.Img_Date[0].disabled = false;
            document.frmCommonCurrConvForm.Img_Date[1].disabled = false;
        } else {
			// Disable editing all fields except Currency Value
			fnEnableAll('N');
			document.frmCommonCurrConvForm.currValue.disabled = false;
		}
    } else {
		// If the Conversion Type is "Constant" enable From/To Date
		if (convType == 101103)// Constant
		{
			document.frmCommonCurrConvForm.dtCurrFromDT.disabled = false;
			document.frmCommonCurrConvForm.dtCurrToDT.disabled = false;
			document.frmCommonCurrConvForm.Img_Date[0].disabled = false;
            document.frmCommonCurrConvForm.Img_Date[1].disabled = false;
        }

		if (convType == 101104 || convType == 101101 || convType == 101102) // Purchase, Average, Spot
		{
			if (strOpt == 'save')
			{
				// Disable editing all fields except Currency Value
				fnEnableAll('N');
				document.frmCommonCurrConvForm.currValue.disabled = false;
			} else {
				document.frmCommonCurrConvForm.dtCurrFromDT.disabled = true;
				document.frmCommonCurrConvForm.dtCurrToDT.disabled = true;
				document.frmCommonCurrConvForm.Img_Date[0].disabled = true;
				document.frmCommonCurrConvForm.Img_Date[1].disabled = true;
			}
		}
	}
}

function fnConvTypeChange() {
	var strOpt = document.frmCommonCurrConvForm.strOpt.value;
	var currFrm = document.frmCommonCurrConvForm.currFrom.value;
	var currTo = document.frmCommonCurrConvForm.currTo.value;
	var convTyp = document.frmCommonCurrConvForm.convType.value;

	if (currFrm != 0 && currTo != 0 && convTyp != 0 && strOpt != 'edit' && currConvTransFl != 'YES')
	{
		document.frmCommonCurrConvForm.strOpt.value='new';
		document.frmCommonCurrConvForm.submit();
	}
}

function fnSubmit()
{
	// Disable Submit button on click
    document.frmCommonCurrConvForm.Btn_Submit.disabled = true;

	// Enable to validate fields (no validation otherwise)
	document.frmCommonCurrConvForm.currFrom.disabled = false;
	document.frmCommonCurrConvForm.currTo.disabled = false;
	document.frmCommonCurrConvForm.convType.disabled = false;
	fnValidateDropDn('currFrom',message[5174]);
	fnValidateDropDn('currTo',message[5175]);
	fnValidateDropDn('convType',message[5176]);
	objCurrFrom = document.frmCommonCurrConvForm.currFrom.value;
	objCurrTo = document.frmCommonCurrConvForm.currTo.value;
	if(objCurrFrom == objCurrTo && objCurrFrom!=0)
	{
		Error_Details(message[5133]);
	}
	// Enable to validate fields (no validation otherwise)
	document.frmCommonCurrConvForm.dtCurrFromDT.disabled = false;
	document.frmCommonCurrConvForm.dtCurrToDT.disabled = false;
	fnValidateTxtFld('dtCurrFromDT',message[5141]);
	fnValidateTxtFld('dtCurrToDT',message[5142]);
	objStartDt = document.frmCommonCurrConvForm.dtCurrFromDT;
	objEndDt = document.frmCommonCurrConvForm.dtCurrToDT;
	//DateValidateMMDDYYYY(objStartDt, '<b>From Date - </b>'+ message[1]);
	//DateValidateMMDDYYYY(objEndDt, '<b>To Date - </b>'+ message[1]);
	
	var objCurrValue = document.frmCommonCurrConvForm.currValue;
	var objRegExp = /^(?:\d*\.?\d{1,4}?$|\.\[0-9]{1,4}?$)$/;
	/*the regular expression is used to check
	 * ^  start of string
	 * d* must have one or more numbers before decimal point
	 * \. decimal point should be escaped.
	 * [0-9] must have number
	 * {1,4} should have only 4 number after decimal.
	 */ 
	if(objCurrValue.value.length==0 || TRIM(objCurrValue.value) == ''){
		Error_Details(message[5134]);
	}
	else{
		if(isNaN(objCurrValue.value)==true){
			Error_Details(message[5135]);
		}
		if(!objRegExp.test(objCurrValue.value)){//need to validate if there is more than four number in decimal places.
			Error_Details(message[5136]);
		}
		
	}

	// Validate From/To Dates in proper order
	if (dateDiff(objStartDt.value, objEndDt.value,format) < 0)
	{
		Error_Details(message[5137]);
	}
	
	if(document.frmCommonCurrConvForm.transIds!=undefined){
		
		var transIds = document.frmCommonCurrConvForm.transIds.value;
		fnValidateTransactionId(transIds);
	}

	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
	    // Enable Submit button on error
        document.frmCommonCurrConvForm.Btn_Submit.disabled = false;
		fnOnLoad();
		return false;
	}

	// Enable fields to save (nulls the values otherwise)
	fnEnableAll('Y');

	document.frmCommonCurrConvForm.strOpt.value='save';
	fnStartProgress();
	document.frmCommonCurrConvForm.submit();
}

function fnReset()
{
	fnEnableAll('Y');
	document.frmCommonCurrConvForm.currFrom.value=0;
	document.frmCommonCurrConvForm.currTo.value=0;
	document.frmCommonCurrConvForm.convType.value=0;
	document.frmCommonCurrConvForm.dtCurrFromDT.value='';
	document.frmCommonCurrConvForm.dtCurrToDT.value='';
	document.frmCommonCurrConvForm.currValue.value='';
	document.frmCommonCurrConvForm.currId.value='';
	document.frmCommonCurrConvForm.strOpt.value='new';
	
	if(document.frmCommonCurrConvForm.transIds!=undefined){
		document.frmCommonCurrConvForm.transIds.value = '';
	}

	fnOnLoad();
}

function fnEditCurrId(val){
	document.frmCommonCurrConvForm.strOpt.value='edit';
	document.frmCommonCurrConvForm.currId.value=val;
	document.frmCommonCurrConvForm.submit();
}

function fnRptReload()
{
	
	//Form source string
	
	var strSource = fnCheckSelections(document.frmCommonCurrConvForm.checkSelectedSource);
	
	// Disable load button on click
    document.frmCommonCurrConvForm.Btn_Submit.disabled = true;
	objStartDt = document.frmCommonCurrConvForm.dtCurrFromDT.value;
	objEndDt = document.frmCommonCurrConvForm.dtCurrToDT.value;
	//DateValidateMMDDYYYY(objStartDt, '<b>From Date - </b>'+ message[1]);
	//DateValidateMMDDYYYY(objEndDt, '<b>To Date - </b>'+ message[1]);
	objCurrFrom = document.frmCommonCurrConvForm.currFrom.value;
	objCurrTo = document.frmCommonCurrConvForm.currTo.value;
	if(objCurrFrom!=0 && (objCurrFrom == objCurrTo))
	{
		Error_Details(message[5138]);
	}
	// Validate From/To Dates in proper order
	if (dateDiff(objStartDt, objEndDt,format) < 0)
	{
		Error_Details(message[5139]);
	}

	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		// Enable load button on error
		document.frmCommonCurrConvForm.Btn_Submit.disabled = false;
		return false;
	}

	document.frmCommonCurrConvForm.sourceInputStr.value=strSource;
	document.frmCommonCurrConvForm.strOpt.value='report';
	fnStartProgress();
	document.frmCommonCurrConvForm.submit();
}

function fnEnter()
{
	if (event.keyCode == 13)
	{ 
		fnRptReload();
	}
}

function fnVoid()
{
	var currFrm = document.frmCommonCurrConvForm.currFrom.value;
	var currTo = document.frmCommonCurrConvForm.currTo.value;
	var convTyp = document.frmCommonCurrConvForm.convType.value;
	var currId = document.frmCommonCurrConvForm.currId.value;

	if (currId == '')
	{
		Error_Details(message[5140]);
	}
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}

	document.frmCommonCurrConvForm.action ="/GmCommonCancelServlet";
	document.frmCommonCurrConvForm.hTxnId.value = document.frmCommonCurrConvForm.currId.value;
	//document.frmCommonCurrConvForm.hTxnName.value = document.frmCommonCurrConvForm.currFrom.value;
	document.frmCommonCurrConvForm.hRedirectURL.value =  "/gmCommonCurrConv.do?strOpt=view";
	document.frmCommonCurrConvForm.hDisplayNm.value = "Currency Conversion Screen";
	document.frmCommonCurrConvForm.submit();
}

function fnCommentCurrId(currId)
{
	windowOpener("/GmCommonLogServlet?hType=1253&hID="+currId,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
}

function fnEnableAll(enable)
{
	var blEnable = (enable != 'Y');// to enable set disable = false

	// Enable/Disable All Fields
	document.frmCommonCurrConvForm.currFrom.disabled = blEnable;
	document.frmCommonCurrConvForm.currTo.disabled = blEnable;
	document.frmCommonCurrConvForm.convType.disabled = blEnable;
	document.frmCommonCurrConvForm.dtCurrFromDT.disabled = blEnable;
	document.frmCommonCurrConvForm.dtCurrToDT.disabled = blEnable;
	document.frmCommonCurrConvForm.Img_Date[0].disabled = blEnable;
	document.frmCommonCurrConvForm.Img_Date[1].disabled = blEnable;
	document.frmCommonCurrConvForm.currValue.disabled = blEnable;
	
	if(document.frmCommonCurrConvForm.transIds!=undefined){
		document.frmCommonCurrConvForm.transIds.disabled = blEnable;
	}
}

//Popup window to show currency transaction details
function fnOpenTransDtls(currId){
	
	windowOpener("/gmCommonCurrConv.do?currId="+currId+"&strOpt=TransRpt","Currency Conversion Transaction Details","resizable=yes,scrollbars=yes,top=250,left=300,width=600,height=500");
}

//To validate the transaction id field

function fnValidateTransactionId(transIds){
	
	var errTrans = '';
	var arrTransId = new Array();
	
	if(transIds != ''){
		//validate regular expression
		var transRegExp = new RegExp(/^[a-zA-Z0-9,-]*$/);
		if((transRegExp.test(transIds))){
		
			arrTransId = transIds.split(',');
			//Check for the duplicate records found
			for(var i=0;i<arrTransId.length;i++){
				var count = 0;
				for(var j=i+1;j<arrTransId.length;j++){
					if(arrTransId[i]==arrTransId[j]){
						count++;
					}
				}
				if(count>0){
					errTrans += arrTransId[i]+',';
				}
			}
		
			if(errTrans!=''){// If transErr is not empty, throws duplicate transaction validation
				Error_Details(message[5295]+errTrans.substr(0,errTrans.length-1));
			}
		}else{
			// Transaction Id should be comma seperated
			Error_Details(message[5296]);
		}
		
	}else{
		//Transaction ID Mandatory
		Error_Details(message[5297]);
	}
	
}

//this function is used to form the string for the Source based on checked and unchecked value
function fnCheckSelections(objVal)
{
	 var chkdvals=""; 

	 var inputString="";

	var objCheckSourceArr = objVal;

	if(objCheckSourceArr) 
	{
		var objCheckSourceArrLen = objCheckSourceArr.length;

		for(i = 0; i < objCheckSourceArrLen; i ++) 
		{
				if(objCheckSourceArr[i].checked)
			    {	
					chkdvals += objCheckSourceArr[i].value+',';			
					inputString= chkdvals + ",";
				
					if(chkdvals !='')
					inputString=inputString.substring(0,inputString.length-2);
			    }			
		 } 
	}
	return inputString;
}

var gridObj;

//this function used to on page load to show the grid 
function fnOnPageLoadRpt(){
		
	if(objGridData.value !=''){ 
		gridObj = initGridData('dataGridDiv',objGridData);
	}	

}

//this function is used to initiate grid
function initGridData(divRef,objGridData)
	{
	var gObj = new dhtmlXGridObject(divRef);
	var obj = document.frmCommonCurrConvForm.checkSelectedCurrency;
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	if(obj == undefined){//only Exchange Rate Average Report Screen
		gObj.attachHeader("#select_filter,#select_filter,#select_filter,#numeric_filter");
	}
	
	gObj.init();
	gObj.loadXMLString(objGridData);	
	return gObj;
	}

function fnLoadAvgReport(){
	var selectedcurr = fnCheckSelections(document.frmCommonCurrConvForm.checkSelectedCurrency);
	document.frmCommonCurrConvForm.selectedCurrency.value=selectedcurr;
	document.frmCommonCurrConvForm.action = "/gmCommonCurrConv.do?strOpt=AvgRptLoad";
	fnStartProgress();
	document.frmCommonCurrConvForm.submit();
}


//this function is used to export excel
function fnExport()
{	
		gridObj.toExcel('/phpapp/excel/generate.php');

}


function fnSelectAll(varCmd)
{				
				objSelAll = document.all.selectAll;
				if (objSelAll ) {
				objCheckCurrArr = document.all.checkSelectedCurrency;
					
				if ((objSelAll.checked || varCmd == 'selectAll') && objCheckCurrArr)
				{
					objSelAll.checked = true;
					objCheckCurrArrLen = objCheckCurrArr.length;
					
					objCheckCurrArr1 = document.frmCommonCurrConvForm.checkSelectedCurrency;
					
					if (objCheckCurrArr1.type == 'checkbox')
						{
							objCheckCurrArr1.checked = true;
						}
													
					for(i = 0; i < objCheckCurrArrLen; i ++) 
					{	
						objCheckCurrArr[i].checked = true;
						
					}	 
				}
				else if (!objSelAll.checked  && objCheckCurrArr ){
					objCheckCurrArrLen = objCheckCurrArr.length;
					
					objCheckCurrArr1 = document.frmCommonCurrConvForm.checkSelectedCurrency;
					if (objCheckCurrArr1.type == 'checkbox')
						{
							objCheckCurrArr1.checked = false;
						}
					
					for(i = 0; i < objCheckCurrArrLen; i ++) 
					{	
						objCheckCurrArr[i].checked = false;
					}
			
					}
				}
	}
