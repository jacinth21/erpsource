/*******************************************************************************************
 * File					: Message_Accounts.js
 * Desc					: This has the list of Error / Informative Messages for Accounts
 * Version				: V1.0
 *******************************************************************************************/

var message_accounts = new Array(3000);

message_accounts[1]  = "<b>Batch Initiated From - </b>";
message_accounts[2]  = "<b>Batch Initiated To - </b>";
message_accounts[3]  = "Please enter valid data for <B>Batch #</B>";
message_accounts[4]  = "Batch Initiated Date cannot exceed more than 30 days.";
message_accounts[5]  = "Please Select at least one Account to add batch.";
message_accounts[6]  = "Cannot add POs to a batch since the status of the batch is not In Progress.";
message_accounts[7]  = "Please select at least one PO to submit this batch.";
message_accounts[8]  = "Batch #: [0] were already voided.";
message_accounts[9]  = "Cannot void this batch since the status of the batch processing In Progress.";
message_accounts[10] = "Please select all the POs in order to void this batch.";
message_accounts[11] = "Are you sure you want to void the entire Batch?";
message_accounts[12] = "Please select at least one PO to unlink.";
message_accounts[13] = "Please select all the PO in order to process this batch.";
message_accounts[14] = "Cannot process this batch, selected POs are already in Progress.";
message_accounts[15] = "Grand Total";
message_accounts[16] = "Please select at least one Tag ID to Approve.";
message_accounts[17] = "Are you sure you want to Approve the selected Tag(s) ?";
message_accounts[18] = "Please select at least one Tag ID to Reconcile.";
message_accounts[19] = "Are you sure you want to Reconcile the selected Tag(s) ?";
message_accounts[20] = "Update Locked Set";
message_accounts[21] = "Counted Date can not be future date.";
message_accounts[22] = "Cannot save the selected flag type.";
message_accounts[23] = "Please select the flag details.";
message_accounts[24] = "Please select the flag type.";
message_accounts[25] = "Cannot flag the tag: [0],since the status of the tag is matching.";
message_accounts[26] = "Flag Deviation";
message_accounts[27] = "Please select at least one Flag to save.";
message_accounts[28] = "Are you sure you want to Retag ?";
message_accounts[29] = "Please select the value for the Reason.";
message_accounts[30] = "Enter valid value for <B>Missing Charges</B>";
message_accounts[31] = "Please select the Post Option.";
message_accounts[32] = "Please enter the Comment.";
message_accounts[33] = "Please flag the Tag before selecting the Post Option.";
message_accounts[34] = "Flagged can't be done,when the status is on Hold.";
message_accounts[35] = "Please select the Flag Date.";
message_accounts[36] = "This flag : <b>[0]</b> has no Retag option.";
message_accounts[37] = "<b>RA # </b>should not be duplicate for type <b>Reason</b>. <br>Please choose or enter another one.";
message_accounts[38] = "Please enter Batch Upload Data.";
message_accounts[39] = "Please Enter valid Data -[0]";
message_accounts[40] = "Cost should not be Empty for -[0]";
message_accounts[41] = "Invalid cost Entries Found for-[0]";
message_accounts[42] = "Duplicate Set Id Entrie(s) Found - [0]";
message_accounts[43] = "Are you sure you want to submit the Cost?";
message_accounts[44] = "There are no Field Sales available for this Region.";
message_accounts[45] = "Available";
message_accounts[46] = "Not Available";
message_accounts[47] = "<b>Audit Start Date - </b>";
message_accounts[48] = "Please Select at least one Field Sales for Locking.";
message_accounts[49] = "Are you sure you want to lock the selected Field Sales ?";
message_accounts[50] = "Selected Field Sales are already Saved.";
message_accounts[51] = "Selected Field Sales are already Voided.";
message_accounts[52] = "Selected Field Sales are not Saved.";
message_accounts[53] = "Selected Field Sales are already Locked.";
message_accounts[54] = "Please unselect the Field Sales with Tag Qty as 0.";
message_accounts[55] = "Data too large, please narrow down your Set Ids!";
message_accounts[56] = "Number of Field Sales selected exceeds the maximum limit, <br> Please narrow down your selection.";
message_accounts[57] = "Cannot add Field Sales after the status is Data uploaded.";
message_accounts[58] = "Please Select a row to Delete.";
message_accounts[59] = "Cannot delete the selected row. Please perform Void.";
message_accounts[60] = "Cannot delete the selected row.";
message_accounts[61] = "Please Select at least one Field Sales for Voiding.";
message_accounts[62] = "You cannot void partially reconciled status records.";
message_accounts[63] = "You cannot void the Field Sales which are not saved.";
message_accounts[64] = "Are you sure you want to void the selected Field Sales ?";
message_accounts[65] = "<b>Locked From Date - </b> Please enter a valid date in [0] format";
message_accounts[66] = "<b>Locked To Date - </b> Please enter a valid date in [0] format";
message_accounts[67] = "Enter valid value for <B>Deviation Qty</B>";
message_accounts[68] = "Select at least one <b> RA# </b> - to apply .";
message_accounts[69] = "Please enter the valid Qty for the following part # [0]";
message_accounts[70] = "Please fetch cost for parts, since one or more rows are modified";
message_accounts[71] = "Part Number does not exist";
message_accounts[72] = "You need to select a block area in grid first";
message_accounts[73] = "Are you sure you want to submit ?";
message_accounts[74] = "Please specify a <B>Part # </B>in the following rows <B> [0] </B>";
message_accounts[75] = "Please specify <B>Qty </B>in the following rows <B> [0] </B>";
message_accounts[76] = "Cost EA can not be zero for Part Number :<B> [0] </B> Please contact Accounting department for loading cost.";
message_accounts[77] = "Please specify a part # / qty before proceeding";
message_accounts[78] = "Please Select Cell(s) to Paste Data.";
message_accounts[79] = "Please Select Type First";
message_accounts[80] = "Please select a Tag Range From: and To:";
message_accounts[81] = "Please select the filter condition";
message_accounts[82] = "Click here to view history";
message_accounts[83] = "Please select a Type & corresponding Dist/Rep Value";
message_accounts[84] = "Please select a Type & corresponding Dist/Rep Value";
message_accounts[85] = "Are you sure you want to submit ?";
message_accounts[86] = "Tag in Release status should have the following fields as blank.\n <B>Part Number, Set ID, Control #, Ref ID, Location Type, Consigned To </B>";
message_accounts[87] = "Tag in Inactive status. Cannot change status to Released or Active ";
message_accounts[88] = "Tag in Available status. Cannot change status to Released or Active";
message_accounts[89] = "Tag in Released or Active status. Cannot change status to Available";
message_accounts[90] = "Tag in Released or Active status. Cannot change status to Inactive ";
message_accounts[91] = "Please Enter the Correct <b>Tag id</b>";
message_accounts[92] = "Are you sure you want to print statement for multiple accounts?";
message_accounts[93] = "<b>From - </b>";
message_accounts[94]= "<b>To - </b>";
message_accounts[95] = "The Activity Report can only be viewed for 12 months.";
message_accounts[96] = "Please select the date range.";
message_accounts[97] = "Total Bill:";
message_accounts[98] = "<b>Interval > 90(days)</b> value must be equal to 30, 90, 180, or 360";
message_accounts[99] = "<b>Through (days)</b> value must be greater than 90";
message_accounts[100] = "<b>Total:</b>";
message_accounts[101] = "Entered <b>Invoice ID</b> is duplicated.";
message_accounts[102] = "Invalid <b>Invoice ID</b>.";
message_accounts[103] = "Payment already received for the Invoice ID: <b>[0]</b>";
message_accounts[104] = "You may have selected an incorrect Currency.";
message_accounts[105] = "<b>Payment Date - </b>";
message_accounts[106] = "Please enter the amount to post for the following Invoice IDs: <B>[0]</B>";
message_accounts[107] = "The <b>Amount to Post</b> should be less than or equal to the <b>Total Outstanding Amount</b> for the following Invoice IDs: <B>[0]</B>";
message_accounts[108] = "Please wait <b>Invoice ID</b> validation is inprogress.";
message_accounts[109] = "Please enter atleast one Invoice detail.";
message_accounts[110] = "Data too large, please narrow down your Invoice Ids!";
message_accounts[111] = "<b>Error From - </b>";
message_accounts[112] = "<b>Error To - </b>";
message_accounts[113] = "Please Select the <b>Status</b> before proceeding.";
message_accounts[114] = "Status cannot be updated when <b>From Costing Type </b> is blank.";
message_accounts[115] = "Type Should not be empty to perform this Action";
message_accounts[116] = "Account Credit Hold Type";
message_accounts[117] = "Please enter valid data for <B>Credit Limit</B>";
message_accounts[118] = "Please Select AR Summary Type";
message_accounts[119] = "Please enter From and to Dates";
message_accounts[120] = "Date range should be within 3 months";
message_accounts[121] = "Please choose Invoice Type";
message_accounts[122] = "Please enter/select atleast one filter for running the report.";
message_accounts[123] = "<b>Invoice Date - </b> Please enter a valid date in [0] format";
message_accounts[124] = "Balance is not 0. Do you want to save this invoice?";
message_accounts[125] = "calling after page load";
message_accounts[126] = "Please enter Payment date range or Choose a Vendor";
message_accounts[127] = "Date range should be within 6 months";
message_accounts[128] = "This Adjustment Type should have the following fields as blank. <br> <b> Part # </b>";
message_accounts[129] = "This Adjustment Type should have the following fields as blank. <br> <b> Set ID </b>";
message_accounts[130] = "This Adjustment Type should have the following fields as blank. <br> <b> Set ID ,Part #, Tag ID </b>";
message_accounts[131] = "Please enter <b> Comments </b>.";
message_accounts[132] = "Please Enter the QTY as <B>Number</B>";
message_accounts[133] = "Enter valid value for <B>Deviation Qty</B>";
message_accounts[134] = "Please select or enter the value for the reason";
message_accounts[135] = "Please select the value for the reason";
message_accounts[136] = "Please enter the value for the reason";
message_accounts[137] = "Please flag before selecting the post Option";
message_accounts[138] = "Please select the reason before approve";
message_accounts[139] = "Please enter the reason before approve";
message_accounts[140] = "Approval can't be done. when the status is on hold";
message_accounts[141] = "<b>Invoice From Date is not a Valid Date Format</b>";
message_accounts[142] = "<b>Invoice To Date is not a Valid Date Format</b>";
message_accounts[143] = "<b>Invoice From Date</b> cannot be after <b>Invoice To Date</b>";
message_accounts[144] = "<b>Download From Date is not a Valid Date Format</b>";
message_accounts[145] = "<b>Download To Date is not a Valid Date Format</b>";
message_accounts[146] = "<b>Download From Date</b> cannot be after <b>Download To Date</b>";
message_accounts[147] = "Please enter/select atleast one filter for running the report.";
message_accounts[148] = "The <b>Invoice date</b> cannot be left blank.";
message_accounts[149] = "<b>Invoice Date - </b>";
message_accounts[150] = "Cannot Generate an Invoice for Future Date. Please Correct the Date.";
message_accounts[151] = "<b>Inovice Date</b> cannot be less than the Order date";
message_accounts[152] = "Enter valid qty for the parts [0]";
message_accounts[153] = "Enter valid Price EA for the parts [0]";
message_accounts[154] = "Please enter at least one Part Qty and valid Price.";
message_accounts[155] = "<b>Credit Qty</b> should be less than or equals to <b>Qty</b> for part(s) [0]";
message_accounts[156] = "Please select Invoices for which payments have to be posted";
message_accounts[157] = "Amount should be a Numeric field for row <b> [0] </b>";
message_accounts[158] = "All fields in the payment details are mandatory for row [0]";
message_accounts[159] = "<b>Row [0] </b>,";
message_accounts[160] = "Please choose an Account";
message_accounts[161] = "Cannot search on <b>Open invoices</b> with date range";
message_accounts[162] = "Select a file to upload";
message_accounts[163] = "Duplicate Part Number for the Invoice ID: <B> [0] </B>";
message_accounts[164] = "Invalid Invoice ID/Part Number.";
message_accounts[165] = "You may have selected an incorrect Currency.";
message_accounts[166] = "Cannot Issue Credit/Debit to more than one account.";
message_accounts[167] = "Please enter a numeric price for the following Part Numbers: <b>[0]</b>";
message_accounts[168] = "Please enter a valid price for the following Part Numbers: <b>[0]</b>";
message_accounts[169] = "Please enter a valid QTY for the following Part Numbers: <b>[0]</b>";
message_accounts[170] = "Please enter a numeric QTY for the following Part Numbers: <b>[0]</b>";
message_accounts[171] = "Please enter Price for the following Part Numbers: <b>[0]</b>";
message_accounts[172] = "Please enter Qty for the following Part Numbers: <b>[0]</b>";
message_accounts[173] = "Please enter Qty and price for the following Part Numbers: <b>[0]</b>";
message_accounts[174] = "Please enter Part number for the following Invoice IDs: <b>[0]</b>";
message_accounts[175] = "Please enter data for atleast one Invoice.";
message_accounts[176] = "Please Enter Valid Account ID";
message_accounts[177] = "Date range cannot exceed more than 31 days.";
message_accounts[178] = "<B>Invalid Process!!!</B>";
message_accounts[179] = "Please select at least one Invoice ID";
message_accounts[180] = "<b>Total</b>";
message_accounts[181] = "Please choose at least one Filter.";
message_accounts[182] = "<b>Order From Date</b> should not be Empty.";
message_accounts[183] = "<b>Order To Date</b> should not be Empty.";
message_accounts[184] = "<b>Status Updated From Date</b> should not be Empty.";
message_accounts[185] = "<b>Status Updated To Date</b> should not be Empty.";
message_accounts[186] = "<b>Order From Date - </b> Please enter a valid date in [0] format";
message_accounts[187] = "<b>Order To Date - </b> Please enter a valid date in [0] format";
message_accounts[188] = "<b>Status Updated From Date - </b> Please enter a valid date in [0] format";
message_accounts[189] = "<b>Status Updated To Date - </b> Please enter a valid date in [0] format";
message_accounts[190] = "<b>Order From Date</b> Cannot be Greater than <b>Order To Date </b>.";
message_accounts[191] = "<b>Status Updated From Date</b> Cannot be Greater than <b>Status Updated To Date </b>.";
message_accounts[192] = "Please select atleast one <b>order id</b>.";
message_accounts[193] = "Enter valid <b>Rep Account Id</b> .";
message_accounts[194] = "<b>From Date - </b>";
message_accounts[195] = "<b>To Date - </b>";
message_accounts[196] = "Audit Name";
message_accounts[197] = "Primary Auditor";
message_accounts[198] = "Audit Start Date";
message_accounts[199] = "Audit List";
message_accounts[200] = "Distributor";
message_accounts[201] = "Set List";
message_accounts[202] = "Credit Hold";
message_accounts[203] = "Comments";
message_accounts[204] = "Choose Action";
message_accounts[205] = "Invoice Date"
message_accounts[206] = "Choose Invoice Layout";
message_accounts[207] = "Region";
message_accounts[208] = "The Amount to Post does not equal the Total Outstanding Amount for the following Invoice IDs: ";
message_accounts[209] = "The <b>Comment</b> should be less than 500 character for the following Invoice IDs: <B>[0]</B>";
message_accounts[210] = ". Do you want to proceed?";

message_accounts[250] = "Non Numeric";
message_accounts[251] ="Qty entered cannot be more than Qty Available for the following Parts: [0]";
message_accounts[252] ="You do not have sufficient permissions to verify Sets.";
message_accounts[253] ="Please select atleast one DO.";
//GmReleaseInvoicePayment.jsp
message_accounts[254] ='<b>Initiated From Date - </b> Please enter a valid date in [0] format';
message_accounts[255] ='<b>Initiated To Date - </b> Please enter a valid date in [0] format';
message_accounts[256] ="<b>Initiated From Date</b> cannot be after <b>Initiated To Date</b>";
//GmPaymentList.jsp
message_accounts[257] =' From Date ';
message_accounts[258] =' To Date ';
//GmLockedCogsReport.jsp
message_accounts[259] =" Please select at least one of Part Number / Project List";
message_accounts[260] =" Please select Inventory List Id";
//GmInvPosting.jsp
message_accounts[261] ='Please select appropriate date range based on archived date.';
message_accounts[262] ='Please select appropriate date range based on archived date.';
message_accounts[263] ='<b>From - </b>';
message_accounts[264] ='<b>To - </b>';
//GmIncludeAccountType.jsp
message_accounts[265] ='This Account ID <b>[0]</b> does not exist; Please enter an existing Account ID.';
//GmEditAuditTag.jsp
message_accounts[266] ='<b>Counted By - </b>';
//GmDownloadInvoiceDtls.jsp
message_accounts[267] ='<b>Invoice From Date - </b> Please enter a valid date in [0] format';
message_accounts[268] ='<b>Invoice To Date - </b> Please enter a valid date in [0] format';
message_accounts[269] ="<b>Invoice From Date</b> cannot be after <b>Invoice To Date</b>";
//GmCustomerPOEdit.jsp
message_accounts[270] ="<b>Customer PO</b> cannot be more than 20 characters.";
message_accounts[271] ="Please remove space(s) from the entered Customer PO.";
//GmBatchDownloadReport.jsp
message_accounts[272] ='<b>Batch Download From Date is not a Valid Date Format</b>';
message_accounts[273] ='<b>Batch Download To Date is not a Valid Date Format</b>';
message_accounts[274] ="<b>Batch Download From Date</b> cannot be after <b>Batch Download To Date</b>";
//GmAuditUpload.jsp	
message_accounts[275] ="Select a file to upload ";
message_accounts[276] ="Select values in the list Box";
//GmAPPostingsReport.jsp	
message_accounts[277] ='<b>From Date is not a Valid Date Format</b>';
message_accounts[278] ='<b>To Date is not a Valid Date Format</b>';
message_accounts[279] ="<b>From Date</b> cannot be after <b>To Date</b>";
message_accounts[280] =' Batch Date To ';
message_accounts[281] =' Batch Date From ';
//GmAccountRollForwardReport.jsp
message_accounts[282] ='Please select appropriate date range based on archived date.';
message_accounts[283] ='Please select appropriate date range based on archived date.';
//GmTag.jsp
message_accounts[284]="<b>Tag ID</b> cannot be left blank. Please enter valid data";

message_accounts[285]=' physicalAuditID ';
message_accounts[286]="Total :";
//GmPATagControl.jsp dropdown values
message_accounts[287]=' tagRangeFrom ';
message_accounts[288]=' tagRangeTo ';
message_accounts[289]=' auditUserID ';
//GmPATagDetail.jsp drop down values
message_accounts[290]="Please Select <b> Counted By </b> ";
message_accounts[291]="Please Select <b> Warehouse </b> ";
message_accounts[292]="Are you sure you want to enter qty as 0?";
//GmPALockTag.jsp
message_accounts[293]=' Warehouse ';
//macart.js
message_accounts[294]='Price cant be negative';
message_accounts[295]='Please choose a Construct Code before performing this action';
message_accounts[296]='Qty cant be negative';
message_accounts[297]="After you create MA, you can't revert it, are you sure you wish to continue?";
//GmTagStatus.js
message_accounts[298]=" Tag RangeFrom Can not exceed Tag RangeTo";
message_accounts[299]="<b>Tag Range:</b> Can't be negative";
//GmInvoicePayment.js
message_accounts[300]='The <B>Amount to Post</B> should be less than or equal to the <B>Invoice Amount </B> for invoice:<B>[0]</B>';
message_accounts[301]="Please select Invoices for which payments have to be posted";
message_accounts[302]="Please enter Payment Amount ";
message_accounts[303]="Please enter Payment Date ";
message_accounts[304]="\'Payment Amount\' should be equal to the \'Total Amount to Post\'";
message_accounts[305]="Please enter a Valid Discount Price";
message_accounts[306]="Please enter From and To Date";
message_accounts[307]='<b>From Date is not a Valid Date</b>';
message_accounts[308]='<b>To Date is not a Valid Date</b>';
message_accounts[309]="Do not enter Future Date in From Date. Please correct the Date.";
message_accounts[310]=" Please select Account/Group Name";
message_accounts[311]="Do not enter Future Date in To Date. Please correct the Date.";
message_accounts[312]=" Please choose an Action ";
message_accounts[313]=" There are no Overdue Invoices for this Account ";
message_accounts[314]="Credit Price EA should be less than or equals to Unit Price/Invoice Price EA for part(s):<b> [0] </b>";
message_accounts[315]="Enter Qty should not exceed for the following parts :<b> [0] </b>";
message_accounts[316]="Credit Price EA can't be negative for part(s):<b> [0] </b>";

message_accounts[317]  = "Please select at least one Transaction to submit this batch.";


message_accounts[500] = "Please select anyone of the filter condition.";
message_accounts[501] = " Please select choose action";
message_accounts[502] = " Please Select <b>Warehouse</b>.";
message_accounts[503] = " Please Enter the <b>Split size</b> value.";
message_accounts[504] = " <b>Split size</b> value should not be less than 500.";
message_accounts[505] = "Please choose a <b> Supervisor </b> before creating MA ";
message_accounts[506] = "In Tag Range, From can't be greater than To";
message_accounts[507] = "Tag Count can't be empty for the selected transactions";
message_accounts[508] = "Please Select atleast one Transaction ID to Generate Tags.";
message_accounts[509] =  "Please Enter the <b>Warehouse</b> less then or equal to[0]";
message_accounts[510] = "Number of parts selected exceeds the maximum limit, <br> Please narrow down your search";
message_accounts[511] = "Audit List is Mandatory";
message_accounts[512] = " One of the following parameter is Mandatory <br> 1. Warehouse <br> 2. Generate Blank Tags ";
message_accounts[513] = "Please Select / Enter atleast one of the following to proceed. <br> 1. Ref ID <br> 2. Project List <br> 3. Part Numbers <br> 4. Untagged Transactions <br> 5. Generate Blank Tags";
message_accounts[514] = "Please Select / Enter atleast one of the following to proceed. <br> 1. Warehouse  <br> 2. Untagged Transactions";
message_accounts[515] = "Please enter a valid Ref ID.";
message_accounts[516] = "Number of parts in the Textarea exceeds the maximum limit, <br> Please narrow down your search ";
message_accounts[517] = "Please select a valid Warehouse.";
message_accounts[518] = "Too many be selected , please narrow down you search! ";
message_accounts[519] = "Please select transactions having non-zero deviation quantities.";
message_accounts[520] = " Please select at least one of the filter: tagRange / Warehouse";
message_accounts[521] = " Please enter Tag Range From and To";
message_accounts[522] = " Please enter valid number in Tag Range From and To";
message_accounts[523] = " Tag Range From should not greater than Tag Range To";
message_accounts[524] = " Please select any tag ids to void the records.";
message_accounts[525] = "Data too large, narrow down your search.";
message_accounts[526] = "Entered Warehouse does not exist, please enter a valid Warehouse ";
message_accounts[527] = "Entered Counted By does not exist, please enter a valid Counted By";
message_accounts[528] = "For Consignments Quantity Can't be greater than 1";
message_accounts[529] = "Please make sure that the quantity entered is correct.";
message_accounts[530] = "Cannot perform this action on a set in <b>Transferred</b> status";
message_accounts[531] = " Please select a Tag Range From: and To:";
message_accounts[532] =  " Please enter the Payment Mode for the following invoice <br>IDs: <B>[0]</B>" ;
message_accounts[533] = "Please choose Payment Mode ";
message_accounts[534] = "Invoices for the following POs <b>";
message_accounts[535] = "</b> have been voided";
message_accounts[536] = "Please select valid <b>Transaction Type</b>";
message_accounts[537] = "This Dealer/Account ID ([0]) does not exist, please enter an existing Dealer/Account ID";
message_accounts[538] = "The unchecked transaction will not be included on this batch. Do you want to proceed?";
message_accounts[539] = "Please select the <B>Owner Company </B>for following part # <B> [0] </B>";
message_accounts[540] = "Please enter the <B>Local cost </B>for following part # <B> [0] </B>";
message_accounts[541] = "<b>Debit Qty</b> should be less than or equals to <b>Qty</b> for part(s) [0]";
message_accounts[542] = " Please select Dealer Name";
message_accounts[543] = "Enter only numeric value for the Vat Rate";
message_accounts[544] = "Enter only numeric value for the <b>Credit Amount</b>";
message_accounts[545] = "Please Select <b> Year</b>";
// Cycle count Accuracy Report
message_accounts[546] = "Variance By";
message_accounts[547] = "Negative Amount cannot be entered for <b>Credit Amount</b>"; 
message_accounts[548] = "<b>Credit Amount</b> cannot be blank"; 
message_accounts[549] = "<b>Part Number </b> cannot be blank";
message_accounts[550] = "Only Consignment Sets can be marked as <b>Missing</b>";
message_accounts[551] = "Cannot mark a returned Tag as <b>Missing</b>";
message_accounts[552] = "Tag is not associated to a Set, do you want to proceed?";
message_accounts[553] = "<b>Missing Since</b> cannot be blank when 'Missing' is selected in <b>Status</b> field";
message_accounts[554] = "Missing Since cannot be a future date. Please select a valid date.";
message_accounts[555] = "For the Following Part(s) the Repost and Cancel checkbox is selected. Please uncheck one option. <List of all Parts>";
message_accounts[556] = "Enter Price should not exceed for the following parts :<b> [0] </b>";
message_accounts[557] = "Payment cannot be processed for prior month or future date";

function Messages(errno)
{
	return message_accounts[errno];
}
