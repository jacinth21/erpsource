
//This Function is called to Open the PMR page in the Popup
function fnOpenPMR(strServlet,strCustId)
{
	var strParam =	strServlet+"/SamPageControllerServlet?Txt_CustId=P"+strCustId+"&strPgToLoad=SamCommonPMRServlet";
	window.open(strParam,'PMR','menubar=no,resizable=yes,scrollbars=yes,top=150,left=150,width=750,height=500');

}
//This Function is called to Open the Risk Rating page in the Popup
function fnOpenRiskrating(strServlet,strCustId)
{
	var strParam = strServlet+"/SamPageControllerServlet?Txt_CustId=P"+strCustId+"&strPgToLoad=SamFinancialRatingsServlet";
	window.open(strParam,'Risk','menubar=no,resizable=yes,scrollbars=yes,top=150,left=150,width=750,height=500');

}
//This Function is called to Open the CapitalStructure page in the Popup
function fnOpenCapStructure(strServlet,strCustId)
{
	var strParam = strServlet+"/SamPageControllerServlet?Txt_CustId=P"+strCustId+"&strPgToLoad=SamCapitalStructureViewServlet";
	window.open(strParam,'Capital','menubar=no,resizable=yes,scrollbars=yes,top=150,left=150,width=750,height=500');
	
}
//This Function is called to Open the Availability Summary page in the Popup
function fnOpenAvailSummary(strServlet,strCustId)
{

	var strParam = strServlet+"/SamPageControllerServlet?Txt_CustId=P"+strCustId+"&strPgToLoad=SamAvailSumServlet";
	window.open(strParam,'AvailSumm','menubar=no,resizable=yes,scrollbars=yes,top=150,left=150,width=750,height=500');
	
}
//This Function is called to Open the Exit Strategy page in the Popup
function fnOpenExitStrategy(strServlet,strCustId)
{

	var strParam = strServlet+"/SamPageControllerServlet?Txt_CustId=P"+strCustId+"&strPgToLoad=SamDataExitStrategyServlet";
	window.open(strParam,'Exitstrat','menubar=no,resizable=yes,scrollbars=yes,top=150,left=150,width=750,height=500');
}
//This Function is called to Open the Accounts Home page in the Popup
function fnOpenAccountsHome(strServlet,strCustId)
{

	var strParam = strServlet+"/SamAccountsParentServlet";
	window.open(strParam,'AccHome','menubar=no,resizable=yes,scrollbars=yes,top=150,left=150,width=750,height=500');
}
//This Function is called to Open the BuySellHold page in the Popup
function fnOpenBuySellHold(strServlet,strCustId)
{
	var strParam = strServlet+"/SamPageControllerServlet?Txt_CustId=P"+strCustId+"&strPgToLoad=SamFinancialBuySellServlet";
	window.open(strParam,'Buy','menubar=no,resizable=yes,scrollbars=yes,top=150,left=150,width=750,height=500');
}
//This Function is called to Open the Financial Performance Summary page in the Popup
function fnOpenTrendCards(strServlet,strCustId)
{
	var strParam = strServlet+"/SamPageControllerServlet?Txt_CustId=P"+strCustId+"&strPgToLoad=SamFinancialsServlet";
	window.open(strParam,'Trend','menubar=no,resizable=yes,scrollbars=yes,top=150,left=150,width=750,height=500');
}