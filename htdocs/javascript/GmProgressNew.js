

/*this function is used to show the progress bar window.*/
    
    fnShowProgressBarNew = function()
    {
        fnHideProgressBarNew();
            $("BODY").append(
		        '<div id="processing_container">' +
		        '<div id="processing_content">' +
    		            '<img src="/images/progress.gif" style="width:215px;height:18px;" alt=""/>' +
    		            '&nbsp;<br/>Processing....' +
    		          '</div>' +
		      '</div>');
		//Progress Bar alignment issue Due To jquery.js updating .so ,for this problem changed window to document.
		var left = (($(document).width() / 2) - ($("#processing_container").outerWidth() / 2)) + 0; 
		if( left < 0 ) left = 0;
		var top=100; // to show progress bar at top frame
		$("#processing_container").css({
			top: top + 'px',
			left: left + 'px'
		});
    }

/*this function is used to hide the progress bar window.*/
    
    fnHideProgressBarNew = function()
    {
         $("#processing_container").remove();
    }
    
/*this function is used to show the progress bar window , disable the left link screen and current screen.*/
    
    function fnStartProgressNew(disabFl){
    	if(parent.parent.RightFrame)
		{
    	parent.parent.RightFrame.fnShowProgressBarNew();
		}
    	
    }
    
/*this function is used to hide the progress bar window , enable the left link screen and current screen.*/
    function fnStopProgressNew(){
    	if(parent.parent.RightFrame)
		{
    	parent.parent.RightFrame.fnHideProgressBarNew();
		}
    	//fnEnableScreenNew();
    }
    
 
    