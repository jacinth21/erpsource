var conid = '';
function fnSubmit()
{
	document.frmVendor.hAction.value = 'Save';
  	Error_Clear();
	var selectedAct = document.all.hSelectedAction.value;
	var type = document.all.Cbo_Type.value;
	if (selectedAct != 'PROCLOANER'){
		fnValidateDropDn('Cbo_Type',message[5568]);
		if(selectedAct!= 'BSTR')
		fnValidateDropDn('Cbo_Purpose',message[5543]);
		fnValidateDropDn('Cbo_BillTo',message[5567]);
		fnValidateDropDn('shipTo',message[5563]); 
		fnValidateDropDn('names',message[5564]);
	
		var bill = document.all.Cbo_BillTo.value;
		if(selectedAct!= 'BSTR')
		var purpose = document.all.Cbo_Purpose.value;
	
		// Check if the Consignment Type is InHouse and Billto ID is GlobusMed InHouse
		if (type == 4112 && bill == 01)
		{
			document.frmVendor.Cbo_Type.value = '4112';
			document.frmVendor.Cbo_BillTo.value = '01';
		}
		 
		// Check if the Consignment Type is Consignment and Billto ID is Foreign distributor and if purpose is ICT
		if(selectedAct!= 'BSTR'){
		if (type == 4110 && bill !=373 && bill !=456 && bill !=121 && bill !=480 && bill !=506 && bill !=375 && bill !=569 && bill !=676  && purpose ==50061)
		{
			Error_Details(message[5047]); 
		} 
		}
		else
		{
			if (type == 4110 && bill !=373 && bill !=456 && bill !=121 && bill !=480 && bill !=506 && bill !=375 && bill !=569 && bill !=676  )
			{
				Error_Details(message[5047]); 
			} 
		}
	
	
		var shipto = document.all.shipTo.value;
		var shiptoidid = document.all.addressid.value;
		
		if (shipto == '4121' && shiptoidid == '')
		{
			Error_Details(message[32]);
		}

	      // If set contain tissue parts for regular consignment then validation shipping address is hospital 
			if (type == 4110 && purpose == 50060 && vtissuefl != undefined && vtissuefl != null && vtissuefl=='Y')
			{
				if (shipto != '4122'){ // not hospital
					if (shipto == '4121' && shiptoidid != null){ // sales rep address is hospital
					 var address =	document.all.addressid.options[document.all.addressid.selectedIndex].text;
					 var addressArr = address.split('-');
					  if(addressArr.length>0 && addressArr[0] != 'Hospital'){
						  Error_Details(message[10615]);
					  }			     
					}else{
						Error_Details(message[10615]);
					}
				}
			}
	} // end selectedAct

	if (ErrorCount > 0)
	{
		Error_Show();
		return false;
	}
	var requestId=document.frmVendor.hRequestId.value;
	if(selectedAct == 'PROCLOANER'){
		if(type != '4127')
			{
			Error_Details(message[5048]); 
			if (ErrorCount > 0)
			{
				Error_Show();
				Error_Clear();
				return false;
			}
			}
	}
	if(fnConfirm()){
	var strType = document.frmVendor.Cbo_Type.value;
	if (selectedAct != 'PROCLOANER'){
		document.all.names.disabled = false;
	}
	if(strType ==102930){
		document.all.Cbo_Purpose.disabled = false;
	}
	if(document.all.shipTo!=undefined){
	document.all.shipTo.disabled = false;
	}	
    //document.frmVendor.RE_FORWARD.value = "gmConsignSetServlet";
    document.frmVendor.RE_FORWARD.value = "gmIncShipDetails"; //gmRequestHeader";
	document.frmVendor.RE_RE_FORWARD.value = "gmConsignSetServlet";
	document.frmVendor.RE_TXN.value = getRuleTransaction(strType);
	document.frmVendor.txnStatus.value = "VERIFY";
	//PC-5648: Enable returns of expired parts from entities to owner company for Built set to Returns
	document.frmVendor.action = "/gmRuleEngine.do?refId="+requestId+"&source=50184&strOpt=fetch&hSelectedAction="+selectedAct+"&screenType=Consign&requestId="+requestId+"&FORMNAME=frmRequestHeader&hRequestView=headerView";
	fnStartProgress('Y'); 
	document.frmVendor.submit();
	}
}

function getRuleTransaction(strType)
{
   if(strType = "4119") // in House Loaner
	{
	   return "50921"; // Rule transaction Id for In House Loaner
    }
   else if (strType = "4127") //Product Loaner
    {
	   return "50920"; // Rule transaction Id for Product Loaner
	}
   else if (strType = "40050") // Hospital Consignment
   {
	   return "50922"; // Rule Transaction ID for Hospital Consignment check the code group RLTXN
   } 
}

function fnConfirm()
 {
	 var type = document.all.Cbo_Type.value;
	if (type==4112 || type==4119 || type==4127 || type==4113)
	{
		 return confirm(message[5569]);  
	}
   
	else 
		return true;
 }

function fnInv()
{
	document.frmVendor.hAction.value = 'Rollback';
  	document.frmVendor.submit();
}

function fnPrintVer()
{
	conid = document.frmVendor.hConsignId.value;
	windowOpener(vServletPath+"/GmConsignSetServlet?hAction=PrintVersion&hId="+conid,"Con","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnAgree()
{
	conid = document.frmVendor.hConsignId.value;
	windowOpener(vServletPath+"/GmConsignSetServlet?hAction=Ack&hId="+conid,"Ack","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");

}

function fnPicSlip()
{
	conid = document.frmVendor.hConsignId.value;
	windowOpener(vServletPath+"/GmConsignLoanerServlet?hAction=PicSlip&hConsignId="+conid,"SetPic","resizable=yes,scrollbars=yes,top=150,left=150,width=750,height=500");
}


function fnCallType(object)
{ 
	 val = eval(object.value);
  
	  if (val == 4110 || val==102930) //consignment and ICT
	{
		document.all.Cbo_Purpose.options.length = 0;
		document.all.Cbo_Purpose.options[0] = new Option("[Choose One]","0");
		for (var i=0;i<consignLen;i++)
		{
			arr = eval("ConArr"+i);
			arrobj = arr.split(",");
			id = arrobj[0];
			name = arrobj[1];
			document.all.Cbo_Purpose.options[i+1] = new Option(name,id);
		}		
		 
	}

	if (val == 4112) //In-House Consignment
	{
		document.all.Cbo_Purpose.options.length = 0;
		document.all.Cbo_Purpose.options[0] = new Option("[Choose One]","0");	
		for (var i=0;i<inhouseLen;i++)
		{
			arr = eval("InhouseArr"+i);
			arrobj = arr.split(",");
			id = arrobj[0];
			name = arrobj[1];
			document.all.Cbo_Purpose.options[i+1] = new Option(name,id);
		}		
		 
	}

	var obj = document.frmVendor.Cbo_Type;
	var obj1 = document.frmVendor.Cbo_Purpose;
	var obj3 = document.frmVendor.Cbo_BillTo;
	var obj4 = document.all.shipTo;
	var obj5 = document.all.names;
	var obj7 = document.all.shipCarrier;
	var obj8 = document.all.shipMode;
	var obj6 = document.frmVendor.Chk_ShipFlag;

	if (obj.options[obj.selectedIndex].text == 'In-House Consignment')
	{
		obj1.disabled = false;
		obj3.disabled = false;
		obj4.disabled = false;
		obj5.disabled = false;
		obj7.disabled = false;
		obj8.disabled = false;

		obj3.value = '01';
		obj4.value = '4123';

		//fnCallConsignShip();
		fnGetNames(obj4)
		obj1.focus();
	}
	else if (obj.options[obj.selectedIndex].text == 'Hospital Consignment' || obj.options[obj.selectedIndex].text == 'Quarantine')
	{
		obj1.selectedIndex = 0;
		obj3.selectedIndex = 0;
		obj4.selectedIndex = 0;
		obj5.selectedIndex = 0;
		obj3.disabled = true;
		obj4.disabled = true;
		obj1.disabled = true;
		obj5.disabled = true;
		obj7.disabled = true;
		obj8.disabled = true;
	}
	else if(val == 4110){ // consignment
		obj3.disabled = false;
		obj4.disabled = false;
		obj1.selectedIndex = 0;
		obj3.selectedIndex = 0;
		obj4.selectedIndex = 0;
		obj5.options.length = 0;
		obj5.options[0] = new Option("[Choose One]","0");
		//obj1.disabled = true;
		obj1.disabled = false;
		obj5.disabled = true;
		obj7.disabled = false;
		obj8.disabled = false;
	}else if(val == 102930){ // Inter Company Transfer
		obj1.value = 50060;
		obj3.selectedIndex = 0;
		obj4.selectedIndex = 0;
		obj1.disabled = true;
		obj3.disabled = false;
		obj4.disabled = false;
		obj5.disabled = true;
		obj7.disabled = false;
		obj8.disabled = false;
	}
}

function fnCallConsignShip()
{
	obj = document.frmVendor.Cbo_ShipTo;
	var obj2 = document.frmVendor.Cbo_Values;
	if (obj.options[obj.selectedIndex].text == 'Sales Rep')
	{
		obj2.disabled = false;
		obj2.options.length = 0;
		obj2.options[0] = new Option("[Choose One]","0");
		for (var i=0;i<ConRepLen;i++)
		{
			arr = eval("ConRepArr"+i);
			arrobj = arr.split(",");
			id = arrobj[0];
			name = arrobj[1];
			obj2.options[i+1] = new Option(name,id);
		}
		obj2.focus();
	}
	else if (obj.options[obj.selectedIndex].text == 'Hospital')
	{
		obj2.disabled = false;
		obj2.options.length = 0;
		obj2.options[0] = new Option("[Choose One]","0");
		for (var i=0;i<ConAccLen;i++)
		{
			arr = eval("ConAccArr"+i);
			arrobj = arr.split(",");
			id = arrobj[0];
			name = arrobj[1];
			obj2.options[i+1] = new Option(name,id);
		}
		obj2.focus();
	}
	else if (obj.options[obj.selectedIndex].text == 'Employee')
	{
		obj2.disabled = false;
		obj2.options.length = 0;
		obj2.options[0] = new Option("[Choose One]","0");
		for (var i=0;i<ConEmpLen;i++)
		{
			arr = eval("ConEmpArr"+i);
			arrobj = arr.split(",");
			id = arrobj[0];
			name = arrobj[1];
			obj2.options[i+1] = new Option(name,id);
		}
		obj2.focus();
	}
	else
	{
		obj2.options.length = 0;
		obj2.options[0] = new Option("[Choose One]","0");
		obj2.disabled = true;
	}
}

function fnValidateReqId(obj)
{
	if(obj.value != "")
	{
		dhtmlxAjax.get('/gmLoanerReqValidate.do?strOpt=InvPartQty&requestId='+obj.value+'&ramdomId='+Math.random(),function(loader) {
			var response = loader.xmlDoc.responseText;
			if(response=="Fail"){
				document.frmVendor.Txt_LoanerReqId.value = '';
				Error_Details(message[5049]);
				if (ErrorCount > 0)
				{
					Error_Show();
					Error_Clear();
					return false;
				}
			}
		});
	}
}
