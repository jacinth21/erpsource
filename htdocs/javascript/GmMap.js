var map = null;
var geocoder = null;
var mgr = null;
var pane;
function LoadMap()
{
	if (GBrowserIsCompatible())
	{
		map = new GMap2(document.getElementById("map"));
		map.addControl(new GLargeMapControl3D());
		map.setUIToDefault();
		map.addControl(new GMapTypeControl());
		map.setCenter(new GLatLng(40.48719,-74.81410), 7);
		
		pane = map.getPane(G_MAP_FLOAT_SHADOW_PANE);
		pane.style.display = "none";
		
		pane = map.getPane(G_MAP_MARKER_SHADOW_PANE);
		pane.style.display = "none";
		
		if(strXmlData != ''){
			addMarkersFromXML(strXmlData);
			
		}
	}
}

function addMarkersFromXML(text)
{
	var batch = [];
	mgr = new MarkerManager(map, {trackMarkers : true });
	
	xmlDoc=loadXMLString(text);
    var xmlRows=xmlDoc.documentElement.childNodes;
    var j=0;
    for (i=0;i<xmlRows.length;i++)
    {
	    var xmlRow = xmlRows[i];

	    var xmlcellLongitude =  xmlRow.getElementsByTagName("Longitude")[0].childNodes[0].nodeValue;
	  	var xmlcellLatitude =  xmlRow.getElementsByTagName("Latitude")[0].childNodes[0].nodeValue;
	  	var point = new GLatLng(xmlcellLatitude,xmlcellLongitude);	
	  	
	  	var htmlString =  xmlRow.getElementsByTagName("InputString")[0].childNodes[0].nodeValue;
	  	var xmllabel =  xmlRow.getElementsByTagName("Label")[0].childNodes[0].nodeValue;
	  	
	  	var marker = createMarker(point,htmlString,xmllabel);
	  	batch.push(marker);
	  
    }
    var xmlcellCaseRep_Lng =  xmlRow.getElementsByTagName("CaseRep_Lng")[0].childNodes[0].nodeValue;
   	var xmlcellCaseRep_Lat =  xmlRow.getElementsByTagName("CaseRep_Lat")[0].childNodes[0].nodeValue;
   	
   	var point1 = new GLatLng(xmlcellCaseRep_Lat,xmlcellCaseRep_Lng);
    var marker1 = createMarker1(point1);
    batch.push(marker1);
    
    mgr.addMarkers(batch,2);
    mgr.refresh();
}
 	 
function loadXMLString(txt)
{
	if (window.DOMParser)
	{
		parser=new DOMParser();
		xmlDoc=parser.parseFromString(txt,"text/xml");
	}
	else 
	{
		xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
		xmlDoc.async="false";
		xmlDoc.loadXML(txt);
	}
	return xmlDoc;
}

function createMarker(point,htmlString,label) 
{
	var bounds = new GLatLngBounds();
	var Markericon = new GIcon(); 
	Markericon.image = '/images/marker_icon.png';
	Markericon.shadow = '';
	Markericon.iconSize = new GSize(32, 32);
	Markericon.shadowSize = new GSize(22, 20);
	Markericon.iconAnchor = new GPoint(16, 16);
	Markericon.infoWindowAnchor = new GPoint(12, 10);
    //var markerOptions = {icon:iconBlue};
	var marker = new LabeledMarker(point,{icon:Markericon,labelText: label ,labelOffset: new GSize(-5, -8)});
	if(i<1)
	{
		map.setCenter(point,7);
	}
	GEvent.addListener(marker, "click", function() {
		marker.openInfoWindowHtml(htmlString);
	});
	// ==== Each time a point is found, extent the bounds ato include it =====
	//bounds.extend(point);
	// ===== determine the zoom level from the bounds =====
    //map.setZoom(map.getBoundsZoomLevel(bounds));

    // ===== determine the centre from the bounds ======
    //map.setCenter(bounds.getCenter());
	//var center = map.getCenter();
	//map.addOverlay(marker);	
	return marker;
} 

function createMarker1(point) 
{	
	var repIcon = new GIcon();
	repIcon.image = '/images/rep_icon.png';
	//repIcon.shadow = 'markers/santa_shadow.png';
	repIcon.iconSize = new GSize(21,30);
	repIcon.shadowSize = new GSize(36,30);
	repIcon.iconAnchor = new GPoint(11,30);
	repIcon.infoWindowAnchor = new GPoint(11,0);
	var markerOptions = {icon:repIcon, draggable: false ,title:"My Location"};
	
	var marker = new GMarker(point,markerOptions);
	//map.setCenter(point,3);
	/*
	GEvent.addListener(marker, "click", function() {
		marker.openInfoWindowHtml(htmlString);
	});
	
	GEvent.addListener(marker, "dragstart", function() {
		map.closeInfoWindow();
	});
	
	marker.openInfoWindowHtml(htmlString);
	*/
	
	map.addOverlay(marker);	
	return marker;
} 



