var foSize = new Array();		 /* Holds column length */
var foListArray = new Array(1);	/*!-- Holds the user entered value in a 2 dimension array -->*/
var foListDeletedArray = new Array();	/*!-- Holds the deleted value in a 2 dimension array -->*/
var foArray = new Array();
var TotalColumn = 4;				/*!-- Holds the Total column -->*/
var strDeleted = "";
var foElements = "";

function Initial_Setting()
{
	TotalColumn = 4;		/* Column are counted from 0 so 7 represents 8 columns */
	var obj = document.Main;
	foElements = new Array(obj.Cbo_PartNum,obj.Txt_PayDesc,obj.Txt_Qty);
	/* Used to set the column width */
	foSize[0] = 11;
	foSize[1] = 36;
	foSize[2] = 10;
	foSize[3] = 8;
	foSize[4] = 0;
}

function Initial_Load()
{
	Initial_Setting();
	for(var i = 0 ; i < TotalRows; i++)
	{
		foListArray[i] = new Array(8);		/* Setting the List Array */
		foListArray[i][0] = foDBArray[i][0];
		foListArray[i][1] = foDBArray[i][1];
		foListArray[i][2] = foDBArray[i][2];
		foListArray[i][3] = foDBArray[i][3];
		foListArray[i][4] = foDBArray[i][4];
	}

}

function Screen_Load()
{
	Initial_Load();		/* Used to retrieve the value */
	Load_Array(foListArray,document.Main.Lst_Pay);			/* Used to Load the value in Listbox & Screen */
}

function fnAddOption(listbox)
{
	foListArray = AddOption(listbox,foListArray);
}

function fnUpdateOption(listbox)
{
	var index = listbox.selectedIndex;
	if ( index == "-1")
	{
		alert("Please choose an entry to edit");
		return;
	}
   	
	foListArray = UpdateOption(listbox,foListArray);
}

function fnDeleteOption(listbox)
{
	var index = listbox.selectedIndex;
	if ( index == "-1")
	{
		alert("Please choose an entry to delete");
		return;
	}
	foArray = DeleteOption(listbox,foListArray,foListDeletedArray);
	foListArray = foArray[0];
	foListDeletedArray = foArray[1];
}

function fnSubmit()
{
	var index = foListArray.length;
	var delIndex = foListDeletedArray.length;
	/*
	strString = PrepareSpecificString(foListArray,index-1);
	strDeletedString = PrepareSpecificString(foListDeletedArray,delIndex-1);
	strString = strString + "|" +strDeletedString;
	if (delIndex >0)
	{
		strString = strString +"|";
	}

	//alert(strString);
	document.Main.inputString.value = strString;
	*/
	
	var newFoListArray = fnParseColumns(foListArray);
	var newStrString = fnParseRows(newFoListArray);
	var newFoListDeletedArray = fnParseColumns(foListDeletedArray);
	var newStrDeletedString = fnParseRows(newFoListDeletedArray);
	
	newStrString += "|" + newStrDeletedString;
	var newDelIndex = newStrDeletedString.length;
	
	if(newDelIndex > 0)
	{
	    	newStrString += "|";
	}
	
	document.Main.inputString.value = newStrString;
	alert(newStrString);


	//document.Main.submit();
}

// Trims the leading and trailing spaces
function trim(str)
{
	str = new String(str);
	var intLen = str.length;
	var intBegin = 0;
	var intEnd = intLen - 1;
	while ((str.charCodeAt(intBegin) == 13 || str.charCodeAt(intBegin) == 10 || str.charAt(intBegin) == " ") && intBegin < intLen)
	{
		if (str.charCodeAt(intBegin) == 13 || str.charCodeAt(intBegin) == 10 )
			intBegin+=2;
		else
        		intBegin++;
	}
	while ((str.charCodeAt(intEnd) == 13 || str.charCodeAt(intBegin) == 10 || str.charAt(intEnd) == " ") && intBegin < intEnd)
	{
		if (str.charCodeAt(intEnd) == 13 || str.charCodeAt(intBegin) == 10)
			intEnd-=2;
		else
			intEnd--;
	}
	return str.substring(intBegin, intEnd+1);
}


