//	tablehighlight.js

function fnstripedbyid(varId, varStyle)
{
	var table = document.getElementById(varId);
	if (! table) { return; }
	var tbodies = table.getElementsByTagName("tbody");
	var trs = tbodies[0].getElementsByTagName("tr");
	var len = trs.length;
	for (var i = 0; i < len; i++) 
	{
		trs[i].onmouseover=function(){
			this.className += varStyle; return false
		}
		trs[i].onmouseout=function(){
			this.className = this.className.replace(varStyle, ""); return false
		}
	}
}
