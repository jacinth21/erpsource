function fnOnPageLoad()
{
      var sel = document.frmVendor.hSearch.value;
      if (sel != '')
      {
            document.frmVendor.Cbo_Search.value = sel;
      }
}

document.onkeypress = function(){
	if(event.keyCode == 13){
		fnLoad();
		return false;
	}
	
}

function fnLoad()
{
      
      var pnum = document.frmVendor.Txt_PartNum.value;
      
      var projId = document.frmVendor.hProjId.value;
      var search = document.frmVendor.Cbo_Search.value;
      if((search != '' && search != '0') && (pnum == null || pnum == '') && projId != '0')
      {
            Error_Details(message_operations[579]);
      }
      if(pnum == '' && projId == '0')
      {
            Error_Details(message_operations[580]);
      }
      if (ErrorCount > 0)
      {
                  Error_Show();
                  Error_Clear();
                  return false;
      }
      pnum = pnum.toUpperCase();
      document.frmVendor.Txt_PartNum.value = pnum;
      document.frmVendor.hAction.value = 'PriceLoad';
      document.frmVendor.Btn_Load.disabled=true;
      fnStartProgress('Y');
      document.frmVendor.submit();
}

function fnPartDetails(pnum,pagetype)
{     
      windowOpener("/GmPartInvReportServlet?hAction=Go&hOpt=MultipleParts&hPageType="+pagetype+"&hPartNum="+encodeURIComponent(pnum),"INVPOP","resizable=yes,scrollbars=yes,top=300,left=300,width=1010,height=300");
      return false;
}

function fnSubmit()
{
      document.frmVendor.Btn_Submit.disabled=true;
      var len = document.frmVendor.hTextboxCnt.value;
      var vVend = document.frmVendor.hVendId[document.frmVendor.hVendId.selectedIndex].value;
      var vPart = '';
      var vQty = 0;
      var vCost = 0;
      var str = '';
      var temp = 0;
      var vRev = '';
      var vFl = '';
      var vDhr = 'N';
      var vFAR = 'N';
      var vVALIDATEFL = 'N';
      var vLotCount  = '0';
      var vPriceId = '1';
      var qtyInQuaran;
      var varQtyInQuaranErrorParts = '';
      var varQuaranErrorParts = '';
      var errorFlag = false;
      var qtyFlag = false;
      var verifyFlag = 0;
      
      for (var i=0;i<len;i++)
      {
            val = eval("document.frmVendor.hPartNum"+i);
            vPart = val.value;
            val = eval("document.frmVendor.Txt_Cost"+i);
            vCost = val.value;
            val = eval("document.frmVendor.Txt_Qty"+i);
            vQty = parseInt(val.value,10);
            val = eval("document.frmVendor.hRev"+i);
            vRev = val.value;
            val = eval("document.frmVendor.Chk_Flag"+i);
            vFl = val.checked;
            val = eval("document.frmVendor.Chk_Flag"+i);
            vFl = val.checked;
            val = eval("document.frmVendor.hQtyQuaran"+i);
            qtyInQuaran = parseInt(val.value,10);
            
            if (vQty > qtyInQuaran)
            {
                  errorFlag = true;
                  varQtyInQuaranErrorParts = vPart + " , " +varQtyInQuaranErrorParts;
            }
            if (vQty <= 0 || (isFinite(vQty) ==false && vCost != '') || (vQty <= qtyInQuaran && vCost ==''))
            {
                  qtyFlag = true;
                  varQuaranErrorParts = vPart + " , " +varQuaranErrorParts;
            }
            if (vFl)
            {
                  vFl = 'Y';
            }
            else
            {
                  vFl = 'N';
            }
           
            if (vQty != '' && isNaN(vQty)!= true && verifyFlag == 0)
            {
                  verifyFlag = 1;
            }
            if (vCost != '' && vQty !='' && vRev != '')
            {
                  temp = temp + vCost*vQty;
                  str = str + vPart+"^"+vRev+"^"+vVend+"^"+vQty+"^"+vCost+"^"+vFl+"^"+vVALIDATEFL+"^"+vLotCount+"^"+vDhr+"^"+vFAR+"^"+vPriceId+"|";
                  //str = str + vPart+"^"+vRev+"^"+vVend+"^"+vQty+"^"+vCost+"^"+vFl+"^"+vDhr+"|";
            }
      }
      if (vVend == '0')
      {
            Error_Details(message_operations[581]);
      }
      if (errorFlag)
      {
                  varQtyInQuaranErrorParts = varQtyInQuaranErrorParts.substring(0,varQtyInQuaranErrorParts.lastIndexOf(','));
                  Error_Details(message_operations[582]+varQtyInQuaranErrorParts);
      }
      if (qtyFlag)
      {
                  varQuaranErrorParts = varQuaranErrorParts.substring(0,varQuaranErrorParts.lastIndexOf(','));
                  Error_Details(message_operations[583]+varQuaranErrorParts);
      }
      if (verifyFlag == 0)
      {
            Error_Details(message_operations[584]);
      }
      if (ErrorCount > 0)
      {
                  Error_Show();
                  Error_Clear();
                  document.frmVendor.Btn_Submit.disabled=false;
                  return false;
      }

      document.frmVendor.hPOString.value = vVend +',' +temp+'|';
      document.frmVendor.hWOString.value = str.substring(0,str.length-1);
      document.frmVendor.hAction.value = "Initiate";
      document.frmVendor.action = vServletPath + "/GmOrderPlanServlet";
      fnStartProgress('Y');
      document.frmVendor.submit();
}
