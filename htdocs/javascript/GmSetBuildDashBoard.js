function fnGo()
 {
	var objDStatus = document.frmDashBoard.Chk_GrpDStatus;
 	var statuslen = objDStatus.length;
 	var dStatus = "";
 	fnStartProgress ('Y'); 
 /* fnValidateTxtFld('setID',' Set ID ');
  if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
 	*/
 for(var i=0;i<statuslen;i++)
	{
		if (objDStatus[i].checked == true)
		{
			dStatus = dStatus + objDStatus[i].value + ',';
		}
	}
  document.frmDashBoard.staInputs.value = dStatus;	
  document.frmDashBoard.hAction.value="LoadDashBoard";
  document.frmDashBoard.submit();
 }
 
function fnOpenLog(id, type)
{
	windowOpener("/GmCommonLogServlet?hType="+type+"&hID="+id,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
}

 function fnLoad()
{

	var dStatus = document.frmDashBoard.staInputs.value;	
	var objgrp = '';
	
	if (dStatus != '')
	{
		objgrp = document.frmDashBoard.Chk_GrpDStatus;
		fnCheckSelections(dStatus,objgrp);
	}
	
	 		
}


 // Marks the checkboxes as 'checked'
function fnCheckSelections(val,objgrp)
{
	var valobj = val.split(",");
	var sval = '';
	var arrlen = objgrp.length;
	
	if (arrlen >= 0)
	{
		for (var j=0;j< valobj.length;j++ )
		{
			sval = valobj[j];
			for (var i=0;i< arrlen;i++ )
			{
				if (objgrp[i].value == sval)
				{
					objgrp[i].checked = true;
					break;
				}
			}
		}
	}
	else
	{
		objgrp.checked = true;
	}
}

function Toggle(val)
{
	
	var obj = document.getElementById("div"+val);
	var trobj = document.getElementById("tr"+val);
	var tabobj = document.getElementById("tab"+val);

	if (obj.style.display == 'none')
	{
		obj.style.display = '';
		trobj.className="ShadeRightTableCaptionBlue";
		//tabobj.style.background="#c6e6fe";
		//tabobj.style.background="#d7ecfd";
		tabobj.style.background="#ecf6fe";
	}
	else
	{
		obj.style.display = 'none';
		trobj.className="";
		tabobj.style.background="#ffffff";
	}
} 


function fnSubmit()
{
	var varRows = document.frmDashBoard.varRows.value;
  
	var inputString = ''; 
	var flag = ''; 
	var varCons = ''; 
	fnStartProgress ('Y'); 	 
	for (var i =0; i < varRows; i++) 
	{
		objConsID = eval("document.frmDashBoard.Chk_ConsignId"+i);
		objComp = eval("document.frmDashBoard.Chk_Comp"+i);
		objVeri = eval("document.frmDashBoard.Chk_Verify"+i);
		
		 
			
			if(objComp!=null && objComp.checked == true) 
			{
			varCons = objConsID.value;
			flag = 1;
			inputString = inputString + varCons +'^'+ flag +  '|';
			}
	  		if(objVeri!=null && objVeri.checked == true)
	  		{
	  		varCons = objConsID.value;
	  		flag = 2;
	  	 	inputString = inputString + varCons +'^'+ flag +  '|';
	  	 	}
		
		 
	}
	
	var objDStatus = document.frmDashBoard.Chk_GrpDStatus;
 	var statuslen = objDStatus.length;
 	var dStatus = "";
 	
 	for(var i=0;i<statuslen;i++)
	{
		if (objDStatus[i].checked == true)
		{
			dStatus = dStatus + objDStatus[i].value + ',';
		}
	}
	
	document.frmDashBoard.RE_FORWARD.value = "gmOperDashBoardServlet";
	document.frmDashBoard.RE_TXN.value = "SETVERIFYWRAPPER";
	document.frmDashBoard.txnStatus.value = "VERIFY";
    document.frmDashBoard.action = "/gmRuleEngine.do";

  	document.frmDashBoard.staInputs.value = dStatus;	
//	alert("object is " + inputString); 	 
 	document.frmDashBoard.hAction.value =  'save'; //'save';
	document.frmDashBoard.hinputStr.value = inputString;
	document.frmDashBoard.submit();	
	
}

function fnFetch(strConId)
 {
    
    //  windowOpener("/gmSetBuildProcess.do?consignID="+strConId,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=920,height=600");
     document.frmDashBoard.action = "/gmSetBuildProcess.do?consignID="+strConId;
  	document.frmDashBoard.submit();	
    }  
    
function fnViewDetails(strReqId)
 {
    
      windowOpener("/gmRequestMaster.do?strOpt=print&requestId="+strReqId,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=920,height=600");
    }
   
function fnPrintVer(strConId)
{
windowOpener("/GmConsignSetServlet?hAction=PrintVersion&hId="+strConId,"Con","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function chkBoxCount(chkboxObj)
{
	Error_Clear();
	var chkboxCnt = document.all.hChkCnt.value;
	if(chkboxCnt == '')
		chkboxCnt = 0;
	var chkboxSts= chkboxObj.checked; 
	if(chkboxSts == true)
	{
		chkboxCnt++;
	}
	else
	{
		chkboxCnt--;
	}
	if(chkboxCnt > 10)
	{
		chkboxObj.checked = false;
		chkboxCnt--;
		Error_Details("Please process less than or equal to <b>10 Consignments </b>");
	}
	document.all.hChkCnt.value = chkboxCnt; 
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	
} 