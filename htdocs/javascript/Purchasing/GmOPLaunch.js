
function fnSubmit()
{

    var varDMId =  document.frmOPDemandSheetSetup.demandSheetId.value; 
	  
	var inputString = ''; 
	
	
	//var arrPar = varInput.split('^');
	if(varDMId!=null)
	{
		for (var i =0; i < size; i++) 
		{
			obj = eval("document.frmOPDemandSheetSetup.txtId"+i);
			varId = obj.value;
			obj1 = eval("document.frmOPDemandSheetSetup.cboLaunchDate"+i);
			varDate1 = obj1.value;
			obj2 = eval("document.frmOPDemandSheetSetup.cboNewDate"+i);
			varDate2 = obj2.value;
			if (varDate2 == '' && obj1.disabled == false )
			{
				varDate2 = varDate1;
			}
			if (varDate2 != '')
			{
				fnLaunchDateValidation(varDate2);// Here call the function fnLaunchDateValidation();
				inputString = inputString +varId+'^'+ varDate2 + '|';
			}			
			   
		}
//		 alert("inputstr is " + inputString);
	}
	 if (ErrorCount > 0)
		{
			Error_Show();
			Error_Clear();
			return false;
		}

	
	document.frmOPDemandSheetSetup.strOpt.value = 'save';
	document.frmOPDemandSheetSetup.hinputStr.value = inputString;	 
	fnStartProgress('Y');
	 document.frmOPDemandSheetSetup.submit();
}
 
function fnOnLoad(){

 for (var i=0;i<size ;i++ ){
	obj1 = eval("document.frmOPDemandSheetSetup.cboLaunchDate"+i);		
	obj2 = eval("document.frmOPDemandSheetSetup.cboNewDate"+i);	
	obj3 = eval("document.frmOPDemandSheetSetup.txtShipFl"+i);		

	if (obj1.selectedIndex==0 )
	{		
		//alert(obj1.selectedIndex)
		obj2.disabled = true;
	}
	else{
		obj1.disabled = true;
	}

	if (obj3.value =='Y')
	{
		obj1.disabled = true;
		obj2.disabled = true;
	}

 }
}
function fnApplyAllLaunchDTs(value)
{
 for (var i=0;i<size ;i++ )
	{		 
		obj = eval("document.frmOPDemandSheetSetup.cboLaunchDate"+i);		
		if (obj.disabled == false)
		{
			obj.options[value].selected = true;		 
		}

	} 
}

function fnApplyAllNewDTs(value)
{
 for (var i=0;i<size ;i++ )
	{		 
		obj = eval("document.frmOPDemandSheetSetup.cboNewDate"+i);		
		if (obj.disabled == false)
		{
			obj.options[value].selected = true;		 
		} 
	}
}

function fnLaunchDateHistory(val)
{
	windowOpener("/GmPageControllerServlet?strPgToLoad=/gmAuditTrail.do&auditId=1025&txnId="+val,"","resizable=yes,scrollbars=yes,top=250,left=300,width=785,height=300,status=1");
}

function fnClose(){
	window.close();
}

//Date validation function

function fnLaunchDateValidation(launchDate) {
	var launchMonth  = '';
	var launchYear   = '';
	var currentYear = '';
	var currentMonth = '';
	
  // Get the today date.
	
	currentMonth = todayDate.split("/")[0];
	currentYear  = todayDate.split("/")[2];
	 
  // split the Launch date 
	 
	 launchMonth  =  launchDate.split("/")[0];
	 launchYear   =  launchDate.split("/")[2];

 // Here check the year and month are equal or future
	 
	if(currentYear == launchYear && currentMonth<=launchMonth){
			return true;
		} else if(currentYear < launchYear){   // Check the future year 
			return true;  
                                   
		}

	else{
			Error_Clear();
			Error_Details("<b>Launch Date</b> can either be current month or future month. ");
		}
}

