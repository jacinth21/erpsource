var crossover_fl = '';
//This Function used load the TTP Summary page
function fnTTPSummaryPageLoad() {
	if(objGridData.value !=''){ 
	var footer=new Array();	
	footer[0]="";
	footer[1]="Total";
	footer[2]="";
	footer[3]="";
	footer[4]="<b>{#stat_total}<b>";
	footer[5]="";
	footer[6]="<b>{#stat_total}<b>";	
	footer[7]="<b>{#stat_total}<b>";	
	footer[8]="<b>{#stat_total}<b>";	
	footer[9]="<b>{#stat_total}<b>";	
	footer[10]="<b>{#stat_total}<b>";	
	footer[11]="<b>{#stat_total}<b>";	
	footer[12]="<b>{#stat_total}<b>";	
	footer[13]="<b>{#stat_total}<b>";	
	footer[14]="<b>{#stat_total}<b>";	
	footer[15]="<b>{#stat_total}<b>";	
	footer[16]="<b>{#stat_total}<b>";	
	footer[17]="<b>{#stat_total}<b>";
	footer[18]="<b>{#stat_total}<b>";	
	footer[19]="<b>{#stat_total}<b>";	
	footer[20]="<b>{#stat_total}<b>";	
	footer[21]="<b>{#stat_total}<b>";	
	footer[22]="<b>{#stat_total}<b>";	
	footer[23]="<b>{#stat_total}<b>";	
	footer[24]="<b>{#stat_total}<b>";	
	footer[25]="<b>{#stat_total}<b>";	
	footer[26]="<b>{#stat_total}<b>";	
	footer[27]="<b>{#stat_total}<b>";	
	footer[28]="";
	footer[29]="";	
	footer[30]="";	
	footer[31]="<b>{#stat_total}<b>";	
	footer[32]="<b>{#stat_total}<b>";	
	footer[33]="<b>{#stat_total}<b>";	
	footer[34]="<b>{#stat_total}<b>";	
	footer[35]="<b>{#stat_total}<b>";
	footer[36]="<b>{#stat_total}<b>";
	footer[37]="<b>{#stat_total}<b>";
	footer[38]="<b>{#stat_total}<b>";
	footer[39]="";
	
	gridObj = initGridWithDistributedParsing('dataGridDiv', objGridData,footer);
	gridObj.attachHeader(",#text_filter,#text_filter,#text_filter,#numeric_filter,#select_filter," +//6
			            "#numeric_filter,#numeric_filter,#numeric_filter,#numeric_filter,#numeric_filter," +//5
			            "#numeric_filter,#numeric_filter,#numeric_filter,#numeric_filter,#numeric_filter," +//5
			            "#numeric_filter,#numeric_filter,#numeric_filter,#numeric_filter,#numeric_filter," +//5
			            "#numeric_filter,#numeric_filter,#numeric_filter,#numeric_filter,#numeric_filter," +//5
			            "#numeric_filter,#text_filter,#text_filter,#text_filter,#numeric_filter," +//5
			            "#numeric_filter,#numeric_filter,#numeric_filter,#numeric_filter,#numeric_filter,#numeric_filter," +//6
			            "#numeric_filter,#numeric_filter,,");//3
	
	var showHeaderMenuCol = "false,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,false";

	// based on level to hide the column.
	var levelValue = parent.window.document.getElementById("levelId").value;
	
	// World wide level then only show the RW and Parent need information. otherwise to hide the values.
	//PMT-51506 - to add trauma level value (26240179) to below condition for showing parent need information
	//PC-3687: To show all column TTP Summary Report for Arthroplasty(26240569)
	//if(levelValue !=0 && levelValue != '100800' && levelValue != '100801' && levelValue !='26240179' && levelValue !='26240569'){ //Globus Medical Inc
	if(levelValue !=0 && levelFl != 'Y'){
		part_rowID = gridObj.getColIndexById("pnum");
		rw_inv_rowID = gridObj.getColIndexById("rw_inv");
		parent_Need_rowID = gridObj.getColIndexById("parent_need");
		ont_rowID = gridObj.getColIndexById("otn_val");
		ssqty_rowID = gridObj.getColIndexById("ssqty_val");
		// to set the auto with - Part number row
		gridObj.setColWidth(part_rowID,"*");
		// to hide the column
		gridObj.setColumnHidden(rw_inv_rowID,true);
		gridObj.setColumnHidden(parent_Need_rowID,true);
		gridObj.setColumnHidden(ont_rowID,true);
		gridObj.setColumnHidden(ssqty_rowID,true);
		
		showHeaderMenuCol = "true,true,true,true,true,false,true,true,true,true,true,true,true,true,true,false,true,true,true,false,false,false,false,false,false,false,false,false,true,false,false,false,false,false,false,true,true,true,true,false";

		
	}
	
	// based on level to show/hide the header menu column
	gridObj.enableHeaderMenu(showHeaderMenuCol);
	
	}
}

//This function will call the excel download
function fnExport()
{
	gridObj.setColumnHidden(0,true);
	//gridObj.setColumnHidden(37,true);
	//gridObj.setColumnHidden(38,true);
	gridObj.toExcel('/phpapp/excel/generate.php');
	gridObj.setColumnHidden(0,false);
	//gridObj.setColumnHidden(37,false);
	//gridObj.setColumnHidden(38,false);
}
//This function used to Click A icon in Grid
function fnCallActual(partNum)
{
	var groupId = '-9999';
	//Editable - 102623   Region - 102584
	if (access != '102623'&& levelID != '102584'){
		windowOpener("/gmOPDSSummary.do?strOpt=partdrilldown&drillDownType=TTP&demandSheetMonthId="+demandSheetIds+"&partNumbers="+encodeURIComponent(partNum)+"&groupInfo="+groupId+"&accessType="+access+"&levelID="+levelID+"&forecastMonths="+forecastmonth,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=1100,height=600");
	}else{
		windowOpener("/gmOPTTPDrillDownAction.do?strOpt=&demandSheetIds="+demandsheetIds+"&partNumber="+encodeURIComponent(partNum),"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=1100,height=600");
	}
}
//This function used to Click P icon in Grid				
function fnCallPart(id, type)
{			
	windowOpener("/GmPartNumServlet?hAction=LoadPartDetail&partNumber="+encodeURIComponent(id),"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=760,height=600");
}
//This function used to Click TPR Link in Grid
function fnCallTPRReport (pnum)
{
    windowOpener("/gmOPTPRReport.do?method=fetchTPRReport&pnum="+encodeURIComponent(pnum)+"&inventoryId="+invIDVal,"INVPOP","resizable=yes,scrollbars=yes,top=40,left=20,width=900,height=400");
}