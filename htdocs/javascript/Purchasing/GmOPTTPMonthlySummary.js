function fnReport()
{
	var companyId = document.frmOPTTPMonthlySummary.hCompanyID.value;
	var ttpId= document.frmOPTTPMonthlySummary.ttpId.value;
	//validation for Filters.
	//fnValidateDropDn('ttpId', 'TTP Name');
	fnValidateTxtFld('ttpId','TTP Name');
	
	if( companyId != ''){
		fnValidateDropDn('levelId','Level');
	}
	
    document.frmOPTTPMonthlySummary.demandSheetIds.value = "";
    fnUpdateInventoryId();//call UpdateInventoryId function    
	document.frmOPTTPMonthlySummary.strOpt.value = "report";
	var ttpnm = document.frmOPTTPMonthlySummary.searchttpId.value; 
	document.frmOPTTPMonthlySummary.ttpName.value = ttpnm;
	
	if(ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}
	document.frmOPTTPMonthlySummary.loadReport.disabled = true;
	fnStartProgress('Y');
	document.frmOPTTPMonthlySummary.submit();
}

function fnLock()
{	
	document.frmOPTTPMonthlySummary.lock.disabled = true;
    document.body.style.cursor = 'wait';
    document.frmOPTTPMonthlySummary.strOpt.value = "lock";
    fnStartProgress('Y');
	document.frmOPTTPMonthlySummary.submit();
}

function fnColumnDrillDown(row,col)
{
	var ttpid = document.frmOPTTPMonthlySummary.ttpId.value;
	var month = document.frmOPTTPMonthlySummary.monthId.value;
	var year = document.frmOPTTPMonthlySummary.yearId.value;
	var overrideTypeValue = 20397;
	var currentValue = '';

	windowOpener("/GmPageControllerServlet?strPgToLoad=gmOverride.do&references="+month+'/'+year+"&overrideTypeId="+overrideTypeValue+"&refValue="+row+"&currentValue="+currentValue+"&refId="+ttpid,"Override","resizable=yes,scrollbars=yes,top=250,left=300,width=750,height=375");
}

function fnCallActual(id, type)
{
	var demandsheetIds = document.frmOPTTPMonthlySummary.demandSheetIds.value;		
	var b = id;
    var temp = new Array();
    temp = b.split('^');
	var partNum = temp[0];
	var levelID = temp[2];
	var access = temp[3];
	var forecastmonth = temp[4];
	var groupId = '-9999';
	//Editable - 102623   Region - 102584
	if (access != '102623'&& levelID != '102584'){
		windowOpener("/gmOPDSSummary.do?strOpt=partdrilldown&drillDownType=TTP&demandSheetMonthId="+demandsheetIds+"&partNumbers="+encodeURIComponent(partNum)+"&groupInfo="+groupId+"&accessType="+access+"&levelID="+levelID+"&forecastMonths="+forecastmonth,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=1100,height=600");
	}else{
		windowOpener("/gmOPTTPDrillDownAction.do?strOpt=&demandSheetIds="+demandsheetIds+"&partNumber="+encodeURIComponent(partNum),"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=1100,height=600");
	}
}
				
function fnCallPart(id, type)
{			
    //   alert("Under Construction");
	windowOpener("/GmPartNumServlet?hAction=LoadPartDetail&partNumber="+encodeURIComponent(id),"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=760,height=600");
}

function fnSubmit()
{
	var str = "";
	var partnum;
	if(fnConfirm())
		 {
			var chk_length =  document.frmOPTTPMonthlySummary.partnumber.length;
			for(var n=0;n<chk_length-1;n++)
			{
				partnum = document.frmOPTTPMonthlySummary.partnumber[n].value;
				if(partnum.indexOf(".") >= 0 )
					str = str + document.frmOPTTPMonthlySummary.partnumber[n].value+'^'+document.frmOPTTPMonthlySummary.ordqty[n].value+'|';						
			}
			document.frmOPTTPMonthlySummary.strPnumOrd.value = str;
			document.frmOPTTPMonthlySummary.strOpt.value = 'save';
			fnStartProgress('Y');
			document.frmOPTTPMonthlySummary.submit();
		 }	  
}

function fnConfirm()
 {
   return confirm("Are you sure you want to submit ?");   
 }
//While changing the TTP List DrpDn, fetching the Level DrpDn values.
function fetchLevel()
{
	var ttpId=document.frmOPTTPMonthlySummary.ttpId.value;
	var ttpnm = document.frmOPTTPMonthlySummary.searchttpId.value; 
	document.frmOPTTPMonthlySummary.ttpName.value = ttpnm;
	 var companyId = '';
	 //Set the company ID in hidden field. 
	 for(var i=0;i<TTPListLen;i++){  
		 var TTPId = arrTTPMapping[i][0];
		 if(ttpId == TTPId){
			 companyId = arrTTPMapping[i][1];
		 }
	 }
	 document.frmOPTTPMonthlySummary.hCompanyID.value = companyId;	 
	 //if company ID is not null, submitting the form to fetch level drpDn values, 
	 //if company id null, then making the level dropdown as null.
	 if(companyId != ''){
		if(document.frmOPTTPMonthlySummary.levelId){ 
	       document.frmOPTTPMonthlySummary.levelId.value='';
	    }
	       
	    document.frmOPTTPMonthlySummary.loadReport.disabled = true;
	    document.frmOPTTPMonthlySummary.strOpt.value = "reportLevel";
		document.frmOPTTPMonthlySummary.submit();
	 }else{
		var objDrpDn = document.getElementById('lvldrpdn');
		document.frmOPTTPMonthlySummary.hLvlHierarchy.value='';
		
		if(ttpId != ''){
			objDrpDn.innerHTML = '';	
			var objDiv = document.getElementById('lblInventoryNm');
			objDiv.innerHTML = '';
			objDiv = document.getElementById('labelInventory');
			objDiv.innerHTML = '';
		}else{
			objDrpDn.innerHTML = '<font color=red>*&nbsp;</font>Level:&nbsp;<select name=levelId class=RightText tabindex=0   onChange=setInventory(this.value)><option value=0 id=0>[Choose One]</select>';
		}
	 }
	 
	   
}
function fnSetLevel()
{
	var companyId = document.frmOPTTPMonthlySummary.hCompanyID.value;
	
	var objDrpDn = document.getElementById('lvldrpdn');
	if(companyId == '') //multipart
    {
		objDrpDn.innerHTML = '';	
		objDiv = document.getElementById('labelInventory');
		objDiv.innerHTML = '';
    	objDiv = document.getElementById('lblInventoryNm');
	    objDiv.innerHTML = '';
    }
	
	var objDrpDn = document.getElementById('levelId');
	    objDiv = document.getElementById('labelInventory');
	if(objDrpDn != null && objDrpDn.value == '0'){
		objDiv = document.getElementById('lblInventoryNm');
		objDiv.innerHTML = '';
	}
	//Function will call on page load. It will append companyInfo in each tab href and initiate ajaxtab.
	fnAjaxTabAppendCmpInfo("#maintab");
	fnMainTab();
	//Inventory type is not displayed in header-BUG-12236(PMT-42441)
	objDiv = document.getElementById('lblInventoryNm');
	objInvLbl = document.getElementById('labelInventory');
	if(objInvLbl != undefined && objDiv !=undefined && objDiv.innerHTML == ''){
        setInventory(objDrpDn.value);
    }
}

//while changing Level DrpDn, To set Inventory Label Name.
function setInventory(lvlId)
{
	var invName = "";
	var lvl_hierarchy = "";
	
	var objDiv="";
	for(var i=0;i<TTPLevelLen;i++){  
		var lvlID = arrLVLMapping[i][0];
		if(lvlID == lvlId){
			 invName = arrLVLMapping[i][1];
			 lvl_hierarchy = arrHCHYMapping[i][1];
			 break;
		}
	 }
	//setting the Inv Nm.
	objDiv = document.getElementById('lblInventoryNm');
	if(objDiv != null ){
		objDiv.innerHTML = invName;
	}
	document.frmOPTTPMonthlySummary.lblInventory.value=invName;
	document.frmOPTTPMonthlySummary.hLvlHierarchy.value=lvl_hierarchy;
	//hide the Filter Data section. 
	objDiv = document.getElementById('lblFilterdata');
	if(objDiv != null ){
		objDiv.innerHTML = '';
	}

}
//Below function's fnPageLoad, parseMessage is copied from GmOPDemandSheetSummaryTab.jsp to avoid JS error while
//loading the screen from TTP finalizing screen. If changes done here, need to consider that place also.
function fnPageLoad(url){	
	dhtmlxAjax.get(url+'&ramdomId='+Math.random(),function(loader){
		if (loader.xmlDoc.responseText != null){
			var datagridXML = loader.xmlDoc.responseText;
			parseMessage(datagridXML);					
		}
		});	
	var divReference=eval(document.getElementById('ajaxdivtabarea'));
	divReference.style.overflow="";
}

function parseMessage(objGridData){
	gridObj = initGridWithDistributedParsing('ajaxdivtabarea',objGridData);
	document.getElementById("growthRequest").setAttribute("class", "selected");
}
function fnShowFilters(val){
	var obj = eval("document.all."+val);
	var obj1 = eval("document.all."+val+"img");
	if (obj.style.display == 'none'){
		obj.style.display = 'block';
		if(obj1){
			obj1.src = '/images/minus.gif';
		}
	}else{
		obj.style.display = 'none';	
		if(obj1){
			obj1.src = '/images/plus.gif';
			
		}
	}
}

//Trims the leading and trailing spaces
function trim(str)
{
	str = new String(str);
	var intLen = str.length;
	var intBegin = 0;
	var intEnd = intLen - 1;
	while ((str.charCodeAt(intBegin) == 13 || str.charCodeAt(intBegin) == 10 || str.charAt(intBegin) == " ") && intBegin < intLen)
	{
		if (str.charCodeAt(intBegin) == 13 || str.charCodeAt(intBegin) == 10 )
			intBegin+=2;
		else
        		intBegin++;
	}
	while ((str.charCodeAt(intEnd) == 13 || str.charCodeAt(intBegin) == 10 || str.charAt(intEnd) == " ") && intBegin < intEnd)
	{
		if (str.charCodeAt(intEnd) == 13 || str.charCodeAt(intBegin) == 10 )
			intEnd-=2;
		else
			intEnd--;
	}
	return str.substring(intBegin, intEnd+1);
}

//initiate ajaxtab
function fnMainTab(){
	
		var stropt = document.frmOPTTPMonthlySummary.strOpt.value;
		if(stropt != ""){
				fnStartProgress('Y');
				var maintab=new ddajaxtabs("maintab", "ajaxdivcontentarea");				
				maintab.setpersist(false);								
				maintab.init();					    
				maintab.onajaxpageload=function(pageurl){					    
				if (pageurl.indexOf("gmOPDSSummary.do")!=-1){					    
					var demandtab=new ddajaxtabs("demandtab", "ajaxdivtabarea");
					demandtab.setpersist(true);	
					demandtab.setselectedClassTarget("link") //"link" or "linkparent"
					demandtab.init();
				}
				else if(pageurl.indexOf("gmOPTTPMonthlySummary.do")!=-1) {					
					var summarytab=new ddajaxtabs("summarytab", "ajaxdivtabarea");				
					summarytab.setpersist(true);		
					summarytab.setselectedClassTarget("link") //"link" or "linkparent"						
					summarytab.init();			
				}
		     fnStopProgress();
		}
		    
		}
}

//To create the New Function fnUpdateInventoryId
//To get the Month and Year values from dropdown.
//Compare the values to hidden field (month/year)
//If any values changes then, reset the inventory id values.
function fnUpdateInventoryId(){
    var invid = document.getElementById('inventoryId');
    var monthVal = document.frmOPTTPMonthlySummary.monthId.value;
    var yearVal = document.frmOPTTPMonthlySummary.yearId.value;
   if((invid != null && invid != undefined) &&( (yearVal != yearIDVal) || (monthVal != monthIDVal))){
	   document.getElementById('inventoryId').value = "";
   }
}
//Increase the page height and width in TTP finalizing and summary screen 
function fnGridStyle(){
var obj = document.getElementById("tbcontid");
obj.classList.remove("DtTableWidth90Percent");
obj.classList.add("DtTable1300");
document.getElementById("ajaxdivcontentarea").style.height = "auto";
}
//Increase the page height and width in TTP finalizing and summary screen 
function fnDisplayTagStyle(){
	var obj = document.getElementById("tbcontid");
	obj.classList.remove("DtTable1300");
	obj.classList.add("DtTableWidth90Percent");
	document.getElementById("ajaxdivcontentarea").style.height = "80vh";

}
