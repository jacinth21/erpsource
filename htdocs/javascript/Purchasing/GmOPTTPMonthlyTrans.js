
function fnReset()
{
	
}

//while changing Level DrpDn, To set Inventory Label Name.
function setInventory(lvlId)
{
	var invName = "";
	var lvl_hierarchy = "";
	
	var objDiv="";
	for(var i=0;i<TTPLevelLen;i++){  
		var lvlID = arrLVLMapping[i][0];
		if(lvlID == lvlId){
			 invName = arrLVLMapping[i][1];
			 lvl_hierarchy = arrHCHYMapping[i][1];
			 break;
		}
	 }
	//setting the Inv Nm.
	objDiv = document.getElementById('lblInventoryNm');
	if(objDiv != null ){
		objDiv.innerHTML = invName;
	}
	document.frmOPTTPMonthly.lblInventory.value=invName;
	document.frmOPTTPMonthly.hLvlHierarchy.value=lvl_hierarchy;
	
	//hide the Filter Data section. 
	objDiv = document.getElementById('lblFilterdata');
	if(objDiv != null ){
		objDiv.innerHTML = '';
	}
	//while changing the level DrpDn, form should submit & fetch the filter data's by default.
	fnFetch();
}

//Function call during Onload.
function fnPageOnLoad()
{
	var obj = '';
	var ttpId =  document.frmOPTTPMonthly.ttpId.value;
	var companyId =  document.frmOPTTPMonthly.hCompanyID.value;
	var strOpt = document.frmOPTTPMonthly.strOpt.value;
	var inventoryID = document.frmOPTTPMonthly.hInventoryId.value;
	
	//Hiding the data section when strOpt != edit. 
	if(strOpt != 'edit'){
		var objDiv = document.getElementById('lblFilterdata');
		if(objDiv != null )
			objDiv.innerHTML = '';
	}
	//Fetching the company ID
	for(var i=0;i<TTPListLen;i++){  
		 var TTPId = arrTTPMapping[i][0];
		 if(ttpId == TTPId){
			 companyId = arrTTPMapping[i][1];
		 }
	}
	//Hiding the Level DrpDn.
	if(companyId == '' && ttpId != ''){
		 obj = document.getElementById('lvldrpdn');
		 obj.innerHTML = '';	
	}else{
		//setting TTP Finalizing "Level" default to worldwide or algeia
		if(companyId != '' && ttpId != '' && strOpt == 'fetchLevel'){
			document.frmOPTTPMonthly.levelId.value = companyId;			
			setInventory(companyId);
		}
	}
	//Setting the Inventory ID to pre-select the Inventory Dropdown based on the Month,Year selected.
	if(inventoryID!='')
		document.frmOPTTPMonthly.inventoryId.value = inventoryID;
	
	//Default selecting of select all.
	if(strOpt == 'edit'){
		// 'Select All' box should  be checked if the template is mapped to a TTP and the View Summary button can be enable
		if(TTPSheetCount > 0){
			document.all.selectAll.checked = true;
			fnSelectAll('toggle');
		}
	}
	//For Multi part TTP, setting the default Inv Name as US. 
	if(companyId == '' && strOpt == 'edit'){
		objDiv = document.getElementById('lblInventoryNm');
		if(objDiv != null )
			objDiv.innerHTML = 'US';		
	}
}

//While changing the TTP List DrpDn, fetching the Level DrpDn values.
function fetchLevel(ttpId)
{
	ttpId=document.frmOPTTPMonthly.ttpId.value;
	 var companyId = '';
	 //Set the company ID in hidden field. 
	 for(var i=0;i<TTPListLen;i++){  
		 var TTPId = arrTTPMapping[i][0];
		 if(ttpId == TTPId){
			 companyId = arrTTPMapping[i][1];
		 }
	 }
	 document.frmOPTTPMonthly.hCompanyID.value = companyId;	 
	 //if company ID is not null, submitting the form to fetch level drpDn values, 
	 //if company id null, then making the level dropdown as null.
	 if(companyId != ''){
		if(document.frmOPTTPMonthly.levelId){ 
	       document.frmOPTTPMonthly.levelId.value='';
	    }
	    document.frmOPTTPMonthly.strOpt.value = "fetchLevel";
	    document.frmOPTTPMonthly.submit();
	 }else{
		var objDrpDn = document.getElementById('lvldrpdn');
		var objDiv = document.getElementById('lblFilterdata');
		if(objDiv != null )
			objDiv.innerHTML = '';
		
		if(ttpId != ''){
			objDrpDn.innerHTML = '';			
		}else{
			objDrpDn.innerHTML = '<font color=red>*&nbsp;</font>Level:&nbsp;<select name=levelId class=RightText tabindex=0   onChange=setInventory(this.value)><option value=0 id=0>[Choose One]</select>';
		}
	 }
}
//Enter key functionality.
function fnEnter(){	
	if(event.keyCode == 13)
	{	 
		var rtVal = fnFetch();
		if(rtVal == false){
			if(!e) var e = window.event;

	        e.cancelBubble = true;
	        e.returnValue = false;

	        if (e.stopPropagation) {
	                e.stopPropagation();
	                e.preventDefault();
	        }
		}
	}	
}
//While Clicking the GO button,
function fnFetch()
{	
	//Making the Filter data section as null.
	var objDiv = document.getElementById('lblFilterdata');
	if(objDiv != null ){
		objDiv.innerHTML = '';
	}
	var companyId = document.frmOPTTPMonthly.hCompanyID.value;
	var ttpId= document.frmOPTTPMonthly.ttpId.value;
	//validation for Filters.
	//fnValidateDropDn('ttpId', 'TTP Name');
	fnValidateTxtFld('ttpId','TTP Name');
	if( companyId != ''){
		fnValidateDropDn('levelId','Level');
	}
	fnValidateDropDn('yearId', 'Year ');
	fnValidateDropDn('monthId', 'Month');
	if(ErrorCount > 0){
			Error_Show();
			Error_Clear();
			return false;
	}
	//Submitting the Form.
	document.frmOPTTPMonthly.strOpt.value = "edit";
	if(document.all.checkSelectedSheet != null)	
	{
		document.all.selectAll.checked = false; 	
	}
	fnStartProgress('Y');
	document.frmOPTTPMonthly.submit();	
}
function fnViewSummary()
{
	document.frmOPTTPMonthly.ViewSummary.disabled = true;
	document.frmOPTTPMonthly.action = "/gmOPTTPMonthlySummary.do";
	document.frmOPTTPMonthly.strOpt.value = "report";
	var ttpnm = document.frmOPTTPMonthly.searchttpId.value; 
	document.frmOPTTPMonthly.ttpName.value = ttpnm;
	fnStartProgress('Y');
	document.frmOPTTPMonthly.submit();
}

function fnCheckSelections()
{
	objCheckGSArrLen = 0;
	objCheckRIArrLen = 0;
	
	objCheckGSArr = document.frmOPTTPMonthly.checkSelectedGS;
	if(objCheckGSArr) {
	if (objCheckGSArr.type == 'checkbox')
	{
		objCheckGSArr.checked = true;
	}
	else {
		objCheckGSArrLen = objCheckGSArr.length;
		for(i = 0; i < objCheckGSArrLen; i ++) 
			{	
				objCheckGSArr[i].checked = true;
			}
		}
	}
	
	objCheckRIArr = document.frmOPTTPMonthly.checkSelectedRI;
	if (objCheckRIArr) {
		// check if there is just one element in checked. if so mark it as selected
	if (objCheckRIArr.type == 'checkbox')
	{
		objCheckRIArr.checked = true;
	}
	else {
		objCheckRIArrLen = objCheckRIArr.length;
		for(i = 0; i < objCheckRIArrLen; i ++) 
			{	
				objCheckRIArr[i].checked = true;
			}
		}
	}
}
function fnClose()
{
window.opener.fnFetch();
window.close();
}



function fnSelectAll(varCmd)
{
				objSelAll = document.all.selectAll;
				if (objSelAll ) {
				objCheckSheetArr = document.all.checkSelectedSheet;
					
				if ((objSelAll.checked || varCmd == 'selectAll') && objCheckSheetArr)
				{
					objSelAll.checked = true;
					objCheckSheetArrLen = objCheckSheetArr.length;
					
					objCheckSheetArr1 = document.frmOPTTPMonthly.checkSelectedSheet;
					if (objCheckSheetArr1.type == 'checkbox')
						{
							objCheckSheetArr1.checked = true;
						}
													
					for(i = 0; i < objCheckSheetArrLen; i ++) 
					{	
						objCheckSheetArr[i].checked = true;
					}
				}
				else if (!objSelAll.checked  && objCheckSheetArr ){
					objCheckSheetArrLen = objCheckSheetArr.length;
					
					objCheckSheetArr1 = document.frmOPTTPMonthly.checkSelectedSheet;
					if (objCheckSheetArr1.type == 'checkbox')
						{
							objCheckSheetArr1.checked = false;
						}
					
					for(i = 0; i < objCheckSheetArrLen; i ++) 
					{	
						objCheckSheetArr[i].checked = false;
					}
				 	//document.frmOPTTPMonthly.Submit.disabled = true;
					}
				}				 
	}