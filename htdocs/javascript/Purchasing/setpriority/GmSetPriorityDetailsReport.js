var gridObj;
var setpriorityStausObj ='';
var setpriorityStaus ='';
var response='';
function fnOnpageLoad(){
	if(strPriorityId !=''|| strPriorityId != null){
		fnLoadHeaderDtls(strPriorityId);
		fnLoadSetPriorityDtls(strPriorityId);
	}
	
}
//This function used to Fetch the SP details Value.
function fnLoadSetPriorityDtls(strPriorityId) {
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmSetPriorityReport.do?method=fetchSetPriorityDetailsReport&priorityId='
			+ encodeURIComponent(strPriorityId) + '&ramdomId=' + Math.random());
	var loader = dhtmlxAjax.getSync(ajaxUrl);
	fnSetPriorityDtlsCallback(loader);
}
//This function used to load the SP details to grid.
function fnSetPriorityDtlsCallback(loader) {
	response = loader.xmlDoc.responseText;
	if (response !=''){
		
		var setInitWidths = "75,75,100,75,100,100,100,75,75,75,75,50,50,*";
		var setColAlign = "left,left,left,right,left,left,left,left,center,center,left,left,left,left";
		var setColTypes = "ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro";
		var setColSorting = "str,str,str,str,str,str,str,str,str,str,str,str,str,str";
		var setHeader = [ "Country", "Set ID", "Set Name", "Priority", "RQ ID",
				"CN ID", "Sheet Name", "Request For", "Request To",
				"Request Date", "Required Date", "Source", "RQ Status",
				"CN Status" ];
		var setFilter = [ "#select_filter", "#text_filter", "#text_filter",
				"#text_filter", "#text_filter", "#text_filter", "#text_filter",
				"#text_filter", "#text_filter", "#text_filter", "#text_filter",
				"#select_filter", "#select_filter", "#select_filter" ];
		var setColIds = "countrynm,setid,setNm,requestPriority,reqid,cnid,sheetnm,reqfor,reqto,reqdate,requireddt,reqsrc,rqstatus,cnstatus";
		var enableTooltips = "true,true,true,true,true,true,true,true,true,true,true,true,true";
		var gridHeight = "";
		var format = ""
		var dataHost = {};

		document.getElementById("DivNothingMessage").style.display = 'none';

		// split the functions to avoid multiple parameters passing in single
		// function
		gridObj = initGridObject('dataGridDiv');
		gridObj = fnCustomTypes(gridObj, setHeader, setColIds, setInitWidths,
				setColAlign, setColTypes, setColSorting, enableTooltips,
				setFilter, gridHeight, format);
		gridObj = loadDhtmlxGridByJsonData(gridObj, response);
		
		fnEnableDisableBtn();

	} else {
		document.getElementById("dataGridDiv").style.display = 'none';
		document.getElementById("DivNothingMessage").style.display = 'block';
		document.getElementById("DivExportExcel").style.display = 'none';
		document.getElementById("trBtn").style.display = 'none';
	}
}
//Enable and Disable the Submit and Edit button

function fnEnableDisableBtn(){
	var status = document.frmSetPriorityRpt.spStatusId.value;		
	if(status == "108304"){
		if(document.frmSetPriorityRpt.BTN_SUBMIT != undefined && buttonAccessFl == "Y"){
		document.frmSetPriorityRpt.BTN_SUBMIT.disabled=false;
		}
		document.frmSetPriorityRpt.BTN_EDIT.disabled=false;

	}else{
		if(document.frmSetPriorityRpt.BTN_SUBMIT != undefined && buttonAccessFl == "Y"){
			document.frmSetPriorityRpt.BTN_SUBMIT.disabled=true;
		}
		document.frmSetPriorityRpt.BTN_EDIT.disabled=true;
	}
}
//This Function Used to set custom types conditions in Grid

function fnCustomTypes(gObj, setHeader, setColIds, setInitWidths, setColAlign,
		setColTypes, setColSorting, enableTooltips, setFilter, gridHeight,
		format) {

		gObj.setHeader(setHeader);
		var colCount = setHeader.length;
	
		gObj.setInitWidths(setInitWidths);
		gObj.setColAlign(setColAlign);
		gObj.setColTypes(setColTypes);
		gObj.setColSorting(setColSorting);
		gObj.enableTooltips(enableTooltips);
		gObj.attachHeader(setFilter);
		gObj.setColumnIds(setColIds);
     
		
		gObj.enableAutoHeight(true, gridHeight, true);

	return gObj;
}

//This function used to download the SP details to excel file
function fnExportSetPriorityDtls() {
	gridObj.toExcel('/phpapp/excel/generate.php');
}
//This function used to load the Set Priority upload details
function fnEditSetPriorityDtls() {
	
	document.frmSetPriorityRpt.action = "/gmSetPriorityUpload.do?method=loadSetPriorityUpload&priorityId="
			+ strPriorityId;
	fnStartProgress();
	document.frmSetPriorityRpt.submit();
}
//this function used to confirm the SP details
function fnSubmit() {
	
	var status = document.frmSetPriorityRpt.spStatusId.value;	
	if(status == "108304"){
		fnStartProgress();
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmSetPriorityReport.do?method=confirmSetPriority&priorityId='
			+ encodeURIComponent(strPriorityId)
			+ '&spStatusId='
			+ status + '&ramdomId=' + Math.random());
	var loader = dhtmlxAjax.getSync(ajaxUrl);
	fnSubmitCallback(loader);
	}else{
		Error_Details("The Set priority Transaction is not in Allocated Status.");
	}
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}

}//This function used to load the - SP header details After Submit. 
function fnSubmitCallback(loader) {

	response = loader.xmlDoc.responseText;
	fnStopProgress();

	if(response !=''){
		Error_Details(response);
	}else{
			fnLoadHeaderDtls(strPriorityId);
			fnEnableDisableBtn();
	}
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}

}
