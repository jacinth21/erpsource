var gridObj;



// To Load Set Priority Details
function fnLoad() {

	var setPriorityID = document.frmSetPriorityRpt.priorityId.value;
	var setStatus = document.frmSetPriorityRpt.spStatusId.value;
	var setType = document.frmSetPriorityRpt.spTypeId.value;
	var setQuarter = document.frmSetPriorityRpt.spQuarter.value;
	var setYear = document.frmSetPriorityRpt.spYear.value;

	if (setQuarter != "0" && setYear == "0") {
		Error_Details("Please Select Year when selecting a <b>Quarter</b>");
	}

	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}

	// Using Ajax call for load the report

	var response;
	fnStartProgress();
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmSetPriorityReport.do?method=fetchSetPriorityReports&priorityId='
			+ setPriorityID
			+ '&spStatusId='
			+ setStatus
			+ '&spTypeId='
			+ setType
			+ '&spQuarter='
			+ setQuarter
			+ '&spYear='
			+ setYear
			+ '&ramdomId=' + Math.random());
	dhtmlxAjax.get(ajaxUrl, fnCallBack);

}

function fnCallBack(loader) {
	var status = loader.xmlDoc.status;
	if (status == 200) {
		fnPopulateSetPriorityData(loader);
		
	} else {
		fnStopProgress();
		Error_Details("Reponse has been Failed.Please Try again Later");
		if (ErrorCount > 0) {
			Error_Show();
			Error_Clear();
			return false;
		}
	}
}

// Populate Set Priority data
function fnPopulateSetPriorityData(loader) {

	response = loader.xmlDoc.responseText;
	fnStopProgress();

	if (response != "") {

		var setHeader = [ "", "", "Set Priority ID", "Set Priority Name",
				"Quarter", "Year", "Type", "Status", "Updated By",
				"Updated Date","Status ID" ];
		var setColIds = "priorityId,detailId,priorityId,priorityName,spQuarter,spyear,spTypeName,spStatusName,spUpdatedBy,spUpdatedDate,spStatusId";
		var setColTypes = "myedit,mydetail,ro,ro,ro,ro,ro,ro,ro,ro,ro";
		var setColAlign = "center,center,left,left,left,right,left,left,left,left,left";
		var setColSorting = ",str,str,str,str,str,str,str,str,str,int";
		var enableTooltips = "true,true,true,true,true,true,true,true,true,true";
		var setFilter = [ "", "", "#text_filter", "#text_filter",
				"#select_filter", "#select_filter", "#select_filter",
				"#select_filter", "#text_filter", "#text_filter","#numeric_filter" ];
		var setInitWidths = "50,30,120,150,80,80,120,*,150,140";
		var gridHeight = "";
		var dataHost = {};
		format = "Y"
		

		document.getElementById("dataGridDiv").style.display = 'block';
		document.getElementById("DivExportExcel").style.display = 'block';
		document.getElementById("DivNothingFoundColumn").height = '';
		document.getElementById("DivNothingFound").style.display = 'none';

		// split the functions to avoid multiple parameters passing in single
		// function
		gridObj = initGridObject('dataGridDiv');
		gridObj = fnCustomTypes(gridObj, setHeader, setColIds, setInitWidths,
				setColAlign, setColTypes, setColSorting, enableTooltips,
				setFilter, 400, format);
		gridObj = loadDhtmlxGridByJsonData(gridObj, response);
		gridObj.setColumnHidden(10, true);
		gridObj.attachEvent("onRowSelect", doOnRowSelect);


	} else {

		document.getElementById("dataGridDiv").style.display = 'none';
		document.getElementById("DivExportExcel").style.display = 'none';
		document.getElementById("DivNothingFoundColumn").height = '24';
		document.getElementById("DivNothingFound").style.display = 'block';

	}
}



//This Function Used to load loadSetPriorityDetailsReport screen based on validation 

function doOnRowSelect(rowId,cellInd)
{

	if(cellInd == 1)
		{
		
		var setPriorityID = gridObj.cellById(rowId,2).getValue();
		var spStatusId = gridObj.getColIndexById("spStatusId");
    	var currentStatusId = gridObj.cellById(rowId,spStatusId).getValue();
    	if(currentStatusId != 108304 && currentStatusId != 108305 && currentStatusId != 108306){
    		
    		Error_Details("This Set Priority transaction does not have Request details associated as it is not in <b>'Allocated'</b>, <b>'Confirmation in Progress'</b> or  <b>'Confirmed'</b> status. Please allocate Request IDs before viewing Set Priority Details Report.");
    		if (ErrorCount > 0) {
    			Error_Show();
    			Error_Clear();
    			return false;
    		}	
    	}
    	else
    	{
    		
    		document.frmSetPriorityRpt.action = "/gmSetPriorityReport.do?method=loadSetPriorityDetailsReport&priorityId="
				+ setPriorityID;
		fnStartProgress();
		document.frmSetPriorityRpt.submit();
    		
    		
    	}
		
		}

	
}


// This Function Used to set custom types conditions in Grid

function fnCustomTypes(gObj, setHeader, setColIds, setInitWidths, setColAlign,
		setColTypes, setColSorting, enableTooltips, setFilter, gridHeight,
		format) {

		gObj.setHeader(setHeader);
		var colCount = setHeader.length;
	
		gObj.setInitWidths(setInitWidths);
		gObj.setColAlign(setColAlign);
		gObj.setColTypes(setColTypes);
		gObj.setColSorting(setColSorting);
		gObj.enableTooltips(enableTooltips);
		gObj.attachHeader(setFilter);
		gObj.setColumnIds(setColIds);
		gObj.enableAutoHeight(true, gridHeight, true);

	return gObj;
}

// This function used to create user defined column types in Grid
function eXcell_myedit(cell) { // the eXcell name is defined here
	if (cell) { // the default pattern, just copy it
		this.cell = cell;
		this.grid = this.cell.parentNode.grid;
	}
	this.setValue = function(val) {
		this
				.setCValue(
						"<a href=\"#\" onClick=fnViewSetPriorityUpload('"
								+ val
								+ "')><img alt=\"Set Priority Upload\" src=\"/images/edit.jpg\" border=\"0\" disabled=\"true\"></a>",
						val);

	}
	this.getValue = function() {
		return this.cell.childNodes[0].innerHTML; // gets the value
	}
}
eXcell_myedit.prototype = new eXcell;


//This function used to create user defined column types in Grid
function eXcell_mydetail(cell) { // the eXcell name is defined here
	if (cell) { // the default pattern, just copy it
		this.cell = cell;
		this.grid = this.cell.parentNode.grid;
	}
	this.setValue = function(val) {
		this
				.setCValue(
						"<a href=\"#\"><img alt=\"Set Priority Detail\" src=\"/images/d.gif\" border=\"0\"></a>",
						val);

	}
	this.getValue = function() {
		return this.cell.childNodes[0].innerHTML; // gets the value
	}
}
eXcell_mydetail.prototype = new eXcell;



// This Function Used to download Excel Sheet
function fnExportSetPriority() {

	gridObj.setColumnHidden(0, true);
	gridObj.setColumnHidden(1, true);
	gridObj.toExcel('/phpapp/excel/generate.php');
	gridObj.setColumnHidden(0, false);
	gridObj.setColumnHidden(1, false);

}

// This function used to load the set priority upload screen
function fnViewSetPriorityUpload(setPriorityID) {

	document.frmSetPriorityRpt.action = "/gmSetPriorityUpload.do?method=loadSetPriorityUpload&priorityId="
			+ setPriorityID;
	fnStartProgress();
	document.frmSetPriorityRpt.submit();
	

}




