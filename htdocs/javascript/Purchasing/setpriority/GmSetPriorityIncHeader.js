function fnLoadHeaderDtls(strPriorityId){
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmSetPriorityReport.do?method=fetchSetPriorityMasterDtls&priorityId='+encodeURIComponent(strPriorityId)+'&ramdomId=' + Math.random());
	var loader = dhtmlxAjax.getSync(ajaxUrl);
	fnHeaderCallBack(loader);
}
//call back Function
function fnHeaderCallBack(loader){
	var response = loader.xmlDoc.responseText;
	var arr_response = '[' +response+ ']';
	var setPriorityName='';
	var predefinedQty='';
	var processSequence='';
	var quarter='';
	var status='';
	var setPriorityYear='';
	var region='';
	var setPriorityId='';
	var setUpdatedDate='';
	var setPriorityUpdateBy='';
	var setPriorityStatus_id ='';
	if(arr_response!=""){
		var rows = [], i=0;
		var JSON_Array_obj = "";
		 JSON_Array_obj = JSON.parse(arr_response);
		 $.each(JSON_Array_obj, function(index,jsonObject){
			 setPriorityName = jsonObject.setPriorityNm;
			 predefinedQty = jsonObject.predefinedQty;
			 processSequence = jsonObject.setPrioritySeq;
			 quarter = jsonObject.quarter;
			 status = jsonObject.setPriorityStatus;	
			 setPriorityYear = jsonObject.setPriorityYear;
			 region = jsonObject.region;	
			 setPriorityId = jsonObject.setPriorityId;
			 setPriorityUpdateBy = jsonObject.setPriorityUpdateBy;
			 setUpdatedDate=jsonObject.setUpdatedDate;
			 setPriorityStatus_id = jsonObject.setPriorityStatusid;
		 });
		
		  setSetPriorityHeaderLabel(setPriorityId,setPriorityName,predefinedQty,processSequence,quarter,status,setPriorityYear,region,setPriorityUpdateBy,setUpdatedDate,setPriorityShow);
		  document.all.spPredefinedQty.value = predefinedQty;
          document.all.spStatusId.value = setPriorityStatus_id;
	}
}
//set message to respective labels
function  setSetPriorityHeaderLabel(setPriorityId,setPriorityName,predefinedQty,processSequence,quarter,status,setPriorityYear,region,setPriorityUpdateBy,setUpdatedDate,setPriorityShow){
//if flag value is Y set value to setpriority id
	if(setPriorityShow == "Y"){
	var obj = eval("document.all.lbl_static_set_priorityid");
	obj.innerHTML = setPriorityId;
	}
	var obj = eval("document.all.lbl_set_priority_nm");
	obj.innerHTML = setPriorityName;
	
	var obj = eval("document.all.lbl_set_priority_qty");
	obj.innerHTML = "&nbsp;" + predefinedQty;
	
	var obj = eval("document.all.lbl_set_priority_seq");
	obj.innerHTML = "&nbsp;" + processSequence;
	
	var obj = eval("document.all.lbl_set_priority_quarter");
	obj.innerHTML = "&nbsp;" + quarter;
	
	var obj = eval("document.all.lbl_set_priority_status");
	obj.innerHTML = "&nbsp;" + status;
	
	var obj = eval("document.all.lbl_set_priority_year");
	obj.innerHTML = "&nbsp;" + setPriorityYear;
	
	var obj = eval("document.all.lbl_set_priority_region");
	obj.innerHTML = "&nbsp;" + region;
	
	var obj = eval("document.all.lbl_updated_by");
	obj.innerHTML = "&nbsp;" + setPriorityUpdateBy;
	
	var obj = eval("document.all.lbl_updated_date");
	obj.innerHTML = "&nbsp;" + setUpdatedDate ;
	//obj.style.display = 'block';
}