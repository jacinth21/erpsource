//Declaration Variables
var arrValuefromClipboard = new Array();
var countryNm;
var setId;
var setNm;
var requiredQty;

var requestQty;
var setPriorityStatus; 
var strPriorityId;
var inputStr;
var set_priority_map_rowID;

var set_rowID;
var required_qty_rowID;
var request_qtyrowID;
var set_priority_map_val;
var country_val;

var set_val;
var required_qty_val;
var seqno;
var request_qty_val;
var greaterQty;

var gridrowid;
var nonNumQty;
var country_nm_rowID;
var gridObj;
var spRowId;

var setValidateFl;
var countryValidateFl;
var countryValidateFl_row_id;
var  setValidateFl_row_id;

var countryValidateErr;
var setValidateErr;	

var strFlag;
var modifyFl = '';
var priorityStatus_row_id ='';
var priorityStatus='';
//This function used when 'D' icon click from Set priority report
function fnOnPageLoad(){		

	if(strPriorityId!=''){
		fnLoadHeaderDtls(strPriorityId);
		fnLoadUploadDtls(strPriorityId);
	}
}
//This function used to load details when tab out
function fnload(strPriorityId){
	var strPriorityId = strPriorityId.toUpperCase();
	if(strPriorityId!=''){
		document.getElementById("progress").style.display="none";
		document.getElementById("msg").style.display="none";
		document.getElementById("erromsg").style.display="none";
		fnLoadHeaderDtls(strPriorityId);
		fnLoadUploadDtls(strPriorityId);
	}else{
		document.getElementById("trDiv").style.display = "none";
		document.getElementById("trImageShow").style.display = "none";
		document.getElementById("exportOptions").style.display = "none";
		document.getElementById("buttonshow").style.display = "none";
		setSetPriorityHeaderLabel('','','','','','','','','','','');
	}
}
//This function used to load setpriority mapping details in Grid
function fnLoadUploadDtls(strPriorityId)
{
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmSetPriorityUpload.do?method=fetchSetPriorityUpload&priorityId='+encodeURIComponent(strPriorityId)+'&ramdomId='+Math.random());
	var loader = dhtmlxAjax.getSync(ajaxUrl);
	fnLoadCallBack(loader);

}
//call back function
function fnLoadCallBack(loader){
	    set_priority_map_rowID='';
	    set_rowID = '';
	    required_qty_rowID = '';
	    request_qtyrowID = '';
	    country_nm_rowID = ''; 
	    countryValidateFl_row_id = ''; 
	    setValidateFl_row_id = ''; 
	    countryValidateFl = '';
		setValidateFl = '';
		
		var response = loader.xmlDoc.responseText;
		fnStopProgress();
		
		var setInitWidths = "90,100,415,90,85,150,50,50,50";
		var setColAlign = "left,left,left,right,right,left,left,left,left";
		var setColTypes = "ed,ed,ro,ed,ro,ro,ro,ro,ro";
		var setColSorting = "str,str,str,int,int,str,str,str,str";
		var setHeader = ["Country", "Set ID","Set Name", "Required Qty", "Request Qty", "Status","mapid","setfl","countryFl"];
		var setFilter = ["#text_filter", "#text_filter", "#text_filter","#numeric_filter", "#numeric_filter", "#select_filter","#text_filter","#text_filter","#text_filter"];
		var setColIds = "countryNm,setId,setNm,requiredQty,requestQty,setPriorityStatus,setPriorityMapId,countryValidateFl,setValidateFl";
		var enableTooltips = "true,true,true,true,true,true,true,true,true";
		//Get required Qty total
		var footerArry=new Array();	
		footerArry[0]="";
		footerArry[1]="";
		footerArry[2]="<b>Total<b>";
		footerArry[3]="<b>{#stat_total}<b>";
		footerArry[4]="";
		footerArry[5]="";
		var gridHeight = "";
		var footerStyles = [];
		var footerExportFL = true;
			pagination = "";
			 format = ""
		var formatDecimal="";
		var formatColumnIndex="";
		
		document.getElementById("trDiv").style.display = "table-row";
		document.getElementById("trImageShow").style.display = "table-row";
		document.getElementById("exportOptions").style.display = "table-row";
		document.getElementById("buttonshow").style.display = "table-row";
		
		//split the functions to avoid multiple parameters passing in single function
		gridObj = initGridObject('dataGridDiv');
		gridObj = fnCustomTypes(gridObj,setHeader, setColIds, setInitWidths, setColAlign, setColTypes,setColSorting, enableTooltips, setFilter,400,pagination,footerArry,footerStyles,format,formatDecimal,formatColumnIndex);
		//Hide column    
		gridObj.setColumnHidden(6,true);
		gridObj.setColumnHidden(7,true);
		gridObj.setColumnHidden(8,true);	
		  
		gridObj.copyBlockToClipboard();
	    gridObj.enableMultiline(false);	
		gridObj.enableEditEvents(false, true, true);
		    
		gridObj = loadDhtmlxGridByJsonData(gridObj,response);
	    //enabling and disabling buttons
		fnEnableDisableBtn();
		
		gridObj.attachEvent("onKeyPress", onKeyPressed);
		gridObj.attachEvent("onEditCell",doOnCellEdit);
	    gridObj.enableMultiselect(true);
		gridObj.enableBlockSelection(true);
		
		
		set_priority_map_rowID = gridObj.getColIndexById("setPriorityMapId");
		set_rowID = gridObj.getColIndexById("setId");
		required_qty_rowID = gridObj.getColIndexById("requiredQty");
		request_qtyrowID = gridObj.getColIndexById("requestQty");
		country_nm_rowID = gridObj.getColIndexById("countryNm"); 
		countryValidateFl_row_id = gridObj.getColIndexById("countryValidateFl"); 
		setValidateFl_row_id = gridObj.getColIndexById("setValidateFl"); 
		priorityStatus_row_id = gridObj.getColIndexById("setPriorityStatus");
		//High light the allocation Failed set 
		gridObj.forEachRow(function(rowId) {
		   priorityStatus = gridObj.cellById(rowId, priorityStatus_row_id).getValue();
		  if(priorityStatus =='Failed'){
			gridObj.setRowTextStyle(rowId, "background-color:#ffcccc;color:red;");
		   }
		});
}

//this function used to copy the grid data 
function onKeyPressed(code, ctrl) {
	if (code == 67 && ctrl) {
		if (!gridObj._selectionArea)
			return false;
		gridObj.setCSVDelimiter("\t");
		gridObj.copyBlockToClipboard();
		gridObj._HideSelection();
	}
	if(code==86&&ctrl){
		gridObj.setCSVDelimiter("\t")
		gridObj.pasteBlockFromClipboard();
		gridObj._HideSelection();
	}
	gridObj.refreshFilters();
	return true;
}

//this function is used to perform action when a cell is editing
function doOnCellEdit(stage, rowId, cellInd, nValue, oValue) {
	//if cell is modified change the color
	if(nValue != oValue && (cellInd == country_nm_rowID || cellInd == set_rowID || cellInd == required_qty_rowID)){
		if(stage == 2 ){
			gridObj.setCellTextStyle(rowId,cellInd,"background-color:#D8D8D8;");
			modifyFl = true;
		}
	}
	//Highlight data when edit cell
	if (stage == 1 && (cellInd == country_nm_rowID || cellInd == set_rowID || cellInd == required_qty_rowID) ){
		var c=this.editor.obj;			
		if(c!=null) c.select();
	}
		
	return true;
}


function fnEnableDisableBtn (){
	var status = document.frmSetPriorityUpload.spStatusId.value;
//	108300	Open
//	108301	Saved
//	108302	Allocation In Progress
//	108303	Allocation Failed
//	108304	Allocated
//	108305	Confirmation In Progress
//	108306	Confirmed	
/*
 * when status is allocation in progress ,confirmation in progress and confirmed no need to 
 * show grid edit icons 
 */	
if (buttonAccessFl == "Y"){
	if(status == "108300"){
		document.frmSetPriorityUpload.LBL_VOID.disabled = true;
		document.frmSetPriorityUpload.LBL_SAVE.disabled = false;
		document.frmSetPriorityUpload.LBL_ALLOCATE.disabled = true;
	}else if(status == "108301"){
		document.frmSetPriorityUpload.LBL_VOID.disabled = false;
		document.frmSetPriorityUpload.LBL_SAVE.disabled = false;
		document.frmSetPriorityUpload.LBL_ALLOCATE.disabled = false;
	}else if(status == "108302"){
		document.getElementById("progress").style.display="none";
		document.getElementById("msg").style.display="none";
		document.getElementById("erromsg").style.display="none";
		document.getElementById("trImageShow").style.display = "none";
		document.getElementById("trLabelShow").style.display = "none";
		document.frmSetPriorityUpload.LBL_VOID.disabled = true;
		document.frmSetPriorityUpload.LBL_SAVE.disabled = true;
		document.frmSetPriorityUpload.LBL_ALLOCATE.disabled = true;
	}else if(status == "108303"){
		document.frmSetPriorityUpload.LBL_VOID.disabled = false;
		document.frmSetPriorityUpload.LBL_SAVE.disabled = false;
		document.frmSetPriorityUpload.LBL_ALLOCATE.disabled = true;
	}else if(status == "108304"){
		document.frmSetPriorityUpload.LBL_VOID.disabled = false;
		document.frmSetPriorityUpload.LBL_SAVE.disabled = false;
		document.frmSetPriorityUpload.LBL_ALLOCATE.disabled = true;
	}else if(status == "108305"){
		document.getElementById("progress").style.display="none";
		document.getElementById("msg").style.display="none";
		document.getElementById("erromsg").style.display="none";
		document.getElementById("trImageShow").style.display = "none";
		document.getElementById("trLabelShow").style.display = "none";
		document.frmSetPriorityUpload.LBL_VOID.disabled = true;
		document.frmSetPriorityUpload.LBL_SAVE.disabled = true;
		document.frmSetPriorityUpload.LBL_ALLOCATE.disabled = true;
	}else{
		document.getElementById("progress").style.display="none";
		document.getElementById("msg").style.display="none";
		document.getElementById("erromsg").style.display="none";
		document.getElementById("trImageShow").style.display = "none";
		document.getElementById("trLabelShow").style.display = "none";
		document.frmSetPriorityUpload.LBL_VOID.disabled = true;
		document.frmSetPriorityUpload.LBL_SAVE.disabled = true;
		document.frmSetPriorityUpload.LBL_ALLOCATE.disabled = true;
	}
 }
}



//Use the below function to copy data inside the screen.
function docopy() {
	gridObj.setCSVDelimiter("\t");
	gridObj.copyBlockToClipboard();
	arrValuefromClipboard = new Array();
	if (gridObj._selectionArea != null) {
		var area = gridObj._selectionArea
		var leftTopCol = area.LeftTopCol;
		var leftTopRow = area.LeftTopRow;
		var rightBottomCol = area.RightBottomCol;
		var rightBottomRow = area.RightBottomRow;
		for ( var i = leftTopRow; i <= rightBottomRow; i++) {
			for ( var j = leftTopCol; j <= rightBottomCol; j++) {
				setValuefromClipboard = gridObj.cellByIndex(i, j).getValue();
				arrValuefromClipboard.push(setValuefromClipboard);
			}
		}
	}
	gridObj._HideSelection();
}

//to copy the clip board data to grid
function pasteToGrid() {
	var countryNmInd = gridObj.getColIndexById("countryNm");
	var setIdInd = gridObj.getColIndexById("setId");
	var setNmInd = gridObj.getColIndexById("setNm");
	var requiredQtyInd = gridObj.getColIndexById("requiredQty");
	var setPriorityStatusInd = gridObj.getColIndexById("setPriorityStatus");
	if (gridObj._selectionArea != null) {
		var colIndex = gridObj._selectionArea.LeftTopCol;	

		if(colIndex!=undefined && colIndex != countryNmInd && colIndex != setIdInd && colIndex != setNmInd && colIndex != requiredQtyInd && 
				colIndex != setPriorityStatusInd){
			alert(message_operations[48]);
		}else{
			var area = gridObj._selectionArea
			var leftTopCol = area.LeftTopCol;
			var leftTopRow = area.LeftTopRow;
			var rightBottomCol = area.RightBottomCol;
			var rightBottomRow = area.RightBottomRow;
			for ( var i = leftTopRow; i <= rightBottomRow; i++) {
				var m = 0;			
				for ( var j = leftTopCol; j <= rightBottomCol; j++) {
					gridObj.cellByIndex(i, j).setValue(arrValuefromClipboard[m]);
					m++;
				}
			 }
		}
		gridObj._HideSelection();
		gridObj.refreshFilters();
	} else {
		alert(message_operations[49]);
	}
}

//To add rows into grid.
function fnAddRow() {
	gridObj.filterBy(0,"");//unfilters the grid while adding rows
	gridObj._f_rowsBuffer = null; //clears the cache
	var newRowId = gridObj.getUID();
	gridObj.addRow(newRowId, '');
}


//Use the below function to remove the rows from the grid.
function fnRemoveSetPriorityRow() {
	
	inputStr='';
	var priorityId = document.frmSetPriorityUpload.priorityId.value;
	priorityId = priorityId.toUpperCase();
	
	var gridrows = gridObj.getSelectedRowId();
//check selected status is null or undefined or null 
	if(gridrows == ''||gridrows == undefined || gridrows == null)
	{ 
		Error_Details(message_prodmgmnt[37]);
		if (ErrorCount > 0) {
			Error_Show();
			Error_Clear();
			return false;
		}
	}else{
		if (confirm(message_operations[807])) {
			var gridrowsarr = gridrows.toString().split(",");
			for ( var rowid = 0; rowid < gridrowsarr.length; rowid++) {
				gridrowid = gridrowsarr[rowid];
				 set_priority_map_val = gridObj.cellById(gridrowsarr[rowid], set_priority_map_rowID).getValue();
				    //form inputstring as inputstring^priority_map_value|
				    inputStr=inputStr+set_priority_map_val+',';
				    gridObj.deleteRow(gridrowid);
			     }														
				document.getElementById("progress").style.display="none";
				document.getElementById("msg").style.display="none";
				document.getElementById("erromsg").style.display="none";
				gridObj.refreshFilters();
				
				if(set_priority_map_val!=''){
					var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmSetPriorityUpload.do?method=deleteSetPriorityUpload&priorityId='+encodeURIComponent(priorityId)+'&inputString='+inputStr+'&ramdomId='+Math.random());
					dhtmlxAjax.getSync(ajaxUrl);
					var loader = dhtmlxAjax.getSync(ajaxUrl);
					fnRemoveAjaxResponse(loader);
					}
				
				}
		}
}
//Remove response call back
function fnRemoveAjaxResponse(loader){
	var statusnm = loader.xmlDoc.responseText;
	//set staus as open when remove entire row in grid
	if(statusnm!=''){	
	var obj = eval("document.all.lbl_set_priority_status");
	obj.innerHTML = "&nbsp;" + statusnm;
	document.frmSetPriorityUpload.LBL_VOID.disabled = true;
	document.frmSetPriorityUpload.LBL_SAVE.disabled = false;
	document.frmSetPriorityUpload.LBL_ALLOCATE.disabled = true;
	}
}
//this function used to paste data from excel
function addRowFromClipboard() {
	var cbData = gridObj.fromClipBoard();
    if (cbData == null) {
          Error_Details(message_operations[50]);
    }
    var rowData = cbData.split("\n");
    var rowcolumnData = '';
    var arrLen = (rowData.length) - 1;
    if (arrLen > maxUploadData) {
		Error_Details(Error_Details_Trans(message_operations[808],maxUploadData));
			}
	if (ErrorCount > 0) {
		fnStopProgress();
		Error_Show();
		Error_Clear();
		return;
	}
	fnStartProgress('Y');
    var newRowData = '';
    setTimeout(function() {	
    gridObj.startFastOperations();
	gridObj.filterBy(0,"");//unfilters the grid
  	gridObj._f_rowsBuffer = null; //clears the cache
    for ( var i = 0; i < arrLen; i++) {
    	
          newRowData = rowData[i];
          newRowData = newRowData.replace(/\n|\r/g,"");
          var rowcolumnData = '';
          var row_id = gridObj.getUID();
          gridObj.setCSVDelimiter("\t");
          gridObj.setDelimiter("\t");
          gridObj.addRow(row_id, newRowData);
          gridObj.setRowAttribute(row_id, "row_added", "Y");
          gridObj.setRowAttribute(row_id,"row_modified","Y");
          gridObj.setDelimiter(",");
    }
    gridObj.stopFastOperations();
	fnStopProgress();
	 gridObj.filterByAll();    
	},100);
}

//to download excel
function fnExport() {
	gridObj.toExcel('/phpapp/excel/generate.php');
	gridObj.setColumnHidden(6,true);
	gridObj.setColumnHidden(7,true);
	gridObj.setColumnHidden(8,true);
}
//This function used to void the set priority mapping details
function fnVoidUploadDtls(){
	var priorityId = document.frmSetPriorityUpload.priorityId.value;
	    priorityId = priorityId.toUpperCase();
	    
	   if (confirm(message_operations[809])) {
			fnStartProgress('Y');
			document.frmSetPriorityUpload.action = "/GmCommonCancelServlet?hCancelType=VSPUPL&hTxnId="+priorityId;
			document.frmSetPriorityUpload.submit();
			}	
}


//This function used to save the set priority mapping details
function fnSave() {
	nonNumQty = '';
	emptyCheck = '';
	countryErr = '';
	setErr = '';
	requiredQtyErr = '';
	countryValidateErr = '';
	setValidateErr = '';
	inputStr = '';
	predefinedQty = '';
	requiredQtyTotal = '';

	request_qty_val = '';
	set_priority_map_val = '';
	country_val = '';
	set_val = '';
	required_qty_val = '';
	seqno = '';

	countryValidateFl = '';
	setValidateFl = '';
	var requiredQtyErrChk = '';
	var invalidCountryStr = '';
	var invalidSetStr = '';
	var count = 0;

	var priorityId = document.frmSetPriorityUpload.priorityId.value;
	priorityId = priorityId.toUpperCase();
	var status = document.frmSetPriorityUpload.spStatusId.value;
	// Get fooeter label requiredQtyTotal
	predefinedQty = document.frmSetPriorityUpload.spPredefinedQty.value;
	requiredQtyTotal = gridObj.getFooterLabel(3);
	// check if there is no modification
	var gridChrows = gridObj.getChangedRows(',');
	if (gridChrows.length == 0 && status !='108303') {
		Error_Details(message_operations[810]);
	}
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	} else {
		gridObj.refreshFilters();
		gridObj.forEachRow(function(rowId) {

			country_val = gridObj.cellById(rowId, country_nm_rowID).getValue();
			set_val = gridObj.cellById(rowId, set_rowID).getValue();
			request_qty_val = gridObj.cellById(rowId, request_qtyrowID).getValue();
			required_qty_val = gridObj.cellById(rowId, required_qty_rowID).getValue();
			set_priority_map_val = gridObj.cellById(rowId,set_priority_map_rowID).getValue();

			seqno = count + 1;
			count++;

			// check validation if empty value is passing in grid
			if (country_val == '' && set_val == '' && required_qty_val == ''&& emptyCheck == '') {
				emptyCheck = message_operations[811];
			} else if (emptyCheck == '') {
				if (country_val == '' && countryErr == '') {
					countryErr = message_operations[812];
				}
				if (set_val == '' && setErr == '') {
					setErr = message_operations[813];
				}
				if (required_qty_val == '' && requiredQtyErr == '') {
					requiredQtyErr = message_operations[814];
				}
			}
			// check it is nummeric or less than or equal to zero
			if (isNaN(required_qty_val) || parseInt(required_qty_val) <= 0) {
				gridObj.setCellTextStyle(rowId, required_qty_rowID,"color:red;border:1px solid red;");
				requiredQtyErrChk = message_operations[815];
			}
			// Form input string if and only if countryname,setid and
			// requiredqty value is not equal to empty

			// form input string as
			// rowid^setprioritymapid^countrynm^setid^requiredaty^sequence no
			if (country_val != '' && set_val != '' && required_qty_val != '') {
				inputStr = inputStr + rowId + '^' + set_priority_map_val + '^'+ country_val + '^' + set_val + '^' + required_qty_val+ '^' + seqno + '|';
			}
		});
		// check if required Qty is greater than predefined Qty.If means throw
		// validation
		if (parseInt(requiredQtyTotal) > parseInt(predefinedQty)) {
			Error_Details(Error_Details_Trans(message_operations[816],predefinedQty));
		}
		// throw validation if set id ,country name ,required qty as blank
		if (emptyCheck != '') {
			Error_Details(emptyCheck);
		}
		// throw validation if country name as blank
		if (countryErr != '') {
			Error_Details(countryErr);
		}
		// throw validation if set name as blank
		if (setErr != '') {
			Error_Details(setErr);
		}
		// throw validation if required qty as blank
		if (requiredQtyErr != '') {
			Error_Details(requiredQtyErr);
		}
		// throw validation if required qty is nonnumeric,less than or equal to
		// zero
		if (requiredQtyErrChk != '') {
			Error_Details(requiredQtyErrChk);
		}
		if (ErrorCount > 0) {
			Error_Show();
			Error_Clear();
			return false;
		}

		if (confirm(message_operations[817])) {
			fnStartProgress('Y');		
			var loader = dhtmlxAjax.postSync('/gmSetPriorityUpload.do?method=saveSetPriorityUpload','&priorityId='+ encodeURIComponent(priorityId)
					+ '&inputString='+inputStr+'&'+fnAppendCompanyInfo()+'&ramdomId=' + Math.random());
			fnSaveCallBack(loader);
		}
	}
}


// Function used for save call back function
function fnSaveCallBack(loader){
	strFlag = '';
	spRowId = '';
	setValidateFl = '';
    countryValidateFl = '';
    var strJson = '';
    var countryDuplicateErrFl= false;
    var setDuplicateErrFl= false;
   
    
	var priorityId = document.frmSetPriorityUpload.priorityId.value;
	var response = loader.xmlDoc.responseText;
	
	fnStopProgress();
	
	//split as array
	var b = response;
	var temp = new Array();
	temp = b.split('##');

	strFlag = temp[0];
	strJson = temp[1];
	if(strJson != ""){
		var rows = [], i=0;
		var JSON_Array_obj = "";
		
		JSON_Array_obj = JSON.parse(strJson);
		 
		 $.each(JSON_Array_obj, function(index,jsonObject){
			 
			 spRowId = jsonObject.spRowId;
			 setValidateFl = jsonObject.setValidateFl;
			 countryValidateFl = jsonObject.countryValidateFl;

			
				set_rowID = gridObj.getColIndexById("setId");
			 	country_nm_rowID = gridObj.getColIndexById("countryNm"); 
			 	
			 	gridObj.forEachRow(function(rowId) {
			 		//check row id  set priority equal if both equal and also setvalidate and countryvalidateflag value is N 
			 		//highlight the cell throw validation
			 				if(spRowId == rowId ){	
			 						if(countryValidateFl == 'N' ){
			 							gridObj.setCellTextStyle(rowId, country_nm_rowID,"color:red;border:1px solid red;");
			 							countryDuplicateErrFl  = true;
			 							}
			 						  if(setValidateFl == 'N'){
			 							gridObj.setCellTextStyle(rowId, set_rowID,"color:red;border:1px solid red;");
			 							setDuplicateErrFl = true;
			 						}
			 						}
			 					});//       
		 		});
		 //if flag value true set validations
			if(countryDuplicateErrFl){
		    	Error_Details(message_operations[818]);
			}
		 
			if(setDuplicateErrFl){
		    	Error_Details(message_operations[819]);
			}
			if (ErrorCount > 0) {
				Error_Show();
				Error_Clear();
				return false;
			}
		 
		 //flag value is Y set success meesage and redirect to fetch header details and load details
		 if(strFlag == "Y"){
		    document.getElementById("progress").style.display="block";
			document.getElementById("msg").style.color="green";
			document.getElementById("msg").style.display="inline";		
			document.getElementById("msg").innerHTML= "Saved "+priorityId+" Successfully";
			modifyFl = false;
		 }
	}
	fnLoadHeaderDtls(priorityId);
	fnLoadUploadDtls(priorityId);	
}

//This function used to allocate when click allocate button
function fnAllocateRequest(){
	
	var priorityId = document.frmSetPriorityUpload.priorityId.value;
	priorityId = priorityId.toUpperCase();
	var status = document.frmSetPriorityUpload.spStatusId.value;	
	//if data is modifed throw validation
	if(modifyFl == true){
			Error_Details(message_operations[820]);
			if (ErrorCount > 0 )
			{
				Error_Show();
				Error_Clear();
				return false;
			}	
		}else{
			if(status == "108301")//check status is saved
			fnStartProgress('Y');
			var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmSetPriorityUpload.do?method=saveAllocateRequest&priorityId='+encodeURIComponent(priorityId)+'&spStatusId='+status+'&ramdomId='+ Math.random());
			var loader = dhtmlxAjax.getSync(ajaxUrl);
			fnAllocateCallBack(loader);
			}
	}
//call back function
function fnAllocateCallBack(loader){
	var priorityId = document.frmSetPriorityUpload.priorityId.value;
	var response = loader.xmlDoc.responseText;

	if(response != ''){
		document.getElementById("erromsg").style.color="red";
		document.getElementById("erromsg").style.display="inline";		
		document.getElementById("erromsg").innerHTML = response
	}else{
		fnLoadHeaderDtls(priorityId);
		fnLoadUploadDtls(priorityId);
		var statusId = document.frmSetPriorityUpload.spStatusId.value;
		if(statusId == '108304'){
			document.getElementById("progress").style.display="block";
			document.getElementById("msg").style.color="green";
			document.getElementById("msg").style.display="inline";		
			document.getElementById("msg").innerHTML= "Allocated "+priorityId+" Successfully";
		}else if(statusId == '108303'){
			document.getElementById("progress").style.display="none";
			document.getElementById("msg").style.color="red";
			document.getElementById("msg").style.display="inline";		
			document.getElementById("msg").innerHTML= "Allocation Failed for "+priorityId;
		}
		
	}
}

//This Function Used to set custom types conditions in Grid
function fnCustomTypes(gObj,setHeader, setColIds, setInitWidths, setColAlign, setColTypes,setColSorting, enableTooltips, setFilter,gridHeight,pagination,footerArry,footerStyles,format,formatDecimal,formatColumnIndex){

if (setHeader != undefined && setHeader.length > 0) {
 gObj.setHeader(setHeader);
 var colCount = setHeader.length;
 }    

if (setInitWidths != "")
gObj.setInitWidths(setInitWidths);

if (setColAlign != "")
gObj.setColAlign(setColAlign);

if (setColTypes != "")
gObj.setColTypes(setColTypes);

if (setColSorting != "")
gObj.setColSorting(setColSorting);

if (enableTooltips != "")
gObj.enableTooltips(enableTooltips);

if (setFilter != "")
gObj.attachHeader(setFilter);

if (footerArry != undefined && footerArry.length > 0) {
var footstr = eval(footerArry).join(",");
if (footerStyles != undefined && footerStyles.length > 0)
gObj.attachFooter(footstr, footerStyles);
else
gObj.attachFooter(footstr);
}  

if(setColIds != ""){
 gObj.setColumnIds(setColIds);
}

if (gridHeight != "")
gObj.enableAutoHeight(true, gridHeight, true);
else
gObj.enableAutoHeight(true, 400, true);

if(pagination == "Y"){
    gObj.enablePaging(true,100,10,"pagingArea",true);
    gObj.setPagingSkin("bricks");
}	

if (format != ""){
gObj.setNumberFormat(formatDecimal,formatColumnIndex);
}
return gObj;
}







