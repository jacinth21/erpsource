//Function is used to close the button
function fnClose() {
	window.close();
}
//On page load
function fnOnPageLoad(){

	if(objGridData.value !=''){ 
		gridObj = initGridData('dataGridDiv', objGridData);
		gridObj.groupBy(0,["#title","","","",""]);
		}
}
//This function is called while initiating the grid
function initGridData(divRef,gridData)
{
	var gObj = new dhtmlXGridObject(divRef);
		gObj.setSkin("dhx_skyblue");
		gObj.setImagePath("/extweb/dhtmlx/dhtmlxGrid/imgs/");
		gObj.enableMultiline(false);	
		gObj.enableDistributedParsing(true);
		gObj.init();
		gObj.loadXMLString(gridData);
	
	return gObj;	
}
//Function is used to show when request qty is greater than Growth qty
function fnRequestGrowthDrillDown(firstDayOfMonth,lastDayOfMonth,refId,refType, dm_mas_id, popup_drillown){ 
		if (popup_drillown =='N')
		{	
			dm_mas_id = '0';
		}	

		if (refType == 20296)
		{
			windowOpener("/gmRequestMaster.do?teamId=0&requiredDate="+firstDayOfMonth+"&requiredDateTo="+lastDayOfMonth+"&setID="+refId+"&requestStatus=0&strOpt=reload&requestStatusoper=0&requestFor=0&requestSource=0&requestTo=0&requestTxnID="+dm_mas_id,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=50,width=900,height=600");
		}
		else{
			windowOpener("/gmRequestMaster.do?teamId=0&requiredDate="+firstDayOfMonth+"&requiredDateTo="+lastDayOfMonth+"&partNum="+encodeURIComponent(refId)+"&setOrPart=50637&requestStatus=0&strOpt=reload&requestStatusoper=0&requestFor=0&requestSource=0&requestTo=0&requestTxnID="+dm_mas_id,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=50,width=900,height=600");
		}

}
//Function is used to show growth drill down information
function fnGrowthDrillDown(dmdshtid,drilldown,setid, month){ 
	windowOpener("/gmOPDSGrowthRequestDetail.do?strOpt=growthDrillDown&demandSheetMonthId="+dmdshtid+"&growthDrillDown="+drilldown+"&setID="+setid+"&growthDrillDownMonth="+month,"GrowthDrillDown","resizable=yes,scrollbars=yes,top=40,left=20,width=865,height=400");
}