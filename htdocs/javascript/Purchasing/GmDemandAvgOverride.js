
function fnOverrideAvg(){	
	var overrideVal = TRIM(document.frmDemandAvgOverride.txtWeigthedAvg.value);	
	var override = overrideVal.replace(/[0-9]+/g,'');      // text box should allow only Zero,Positive values.
	if( isNaN(override) || override.length > 0 || overrideVal == ''){
		Error_Details('Please enter valid value in <b>Override Weighted  Avg.</b>');
		Error_Show();
		Error_Clear();
		return false;
	}else{		
		document.frmDemandAvgOverride.strOpt.value = "save";
		fnStartProgress('Y');
		document.frmDemandAvgOverride.submit();
	}
}

function fnRemoveOverride(){
	document.frmDemandAvgOverride.txtWeigthedAvg.value="";  // text box should contain only Null values, since removing the override.
	document.frmDemandAvgOverride.strOpt.value = "save";
	fnStartProgress('Y');
	document.frmDemandAvgOverride.submit();
}

function fnOverrideHistory(){
	var txnid = document.frmDemandAvgOverride.demandOverrideId.value;
	windowOpener("/gmAuditTrail.do?auditId=1054&txnId="+ txnid+"&hJNDIConnection="+JNDI_CONNECTION,"","resizable=yes,scrollbars=yes,top=250,left=300,width=785,height=300,status=1");
}

function fnClose(){
	window.close();
}