var gridObj;
var rowID = '';
var check_rowId = '';
var status_Id='';
var inputString='';
var statusVal='';
var load_date_Id='';
var load_date_Val='';
var ttp_detail_id='';
var ttp_detail_str='';
var ttp_name_Id = '';
var hdttp_name='';
//Load button using Ajax call
function fnLoad(){
	
	//Intialize
	var ttpId = document.frmTTPApproval.ttpId.value;   
	var status = document.frmTTPApproval.status.value;   
	var category = document.frmTTPApproval.category.value;   
	var ttpMonth = document.frmTTPApproval.ttpMonth.value;   
	var ttpYear = document.frmTTPApproval.ttpYear.value; 
	var searchttpId = document.frmTTPApproval.searchttpId.value;   
    // Using Ajax call for load the report
	fnValidateDropDn('ttpYear', 'Year ');
	fnValidateDropDn('ttpMonth', 'Month');
	//fnDateValidation(ttpMonth,ttpYear);
	if(ErrorCount > 0){
			Error_Show();
			Error_Clear();
			return false;
	}	
	    document.frmTTPApproval.action = "/gmTTPSummaryApprove.do?method=fetchTTPApproval";
		fnStartProgress();
		document.frmTTPApproval.submit();
}


//Onpage load Disable approve and grid 
function fnOnPageLoad() {
	var ttpMonth = document.frmTTPApproval.ttpMonth.value;   
	var ttpYear = document.frmTTPApproval.ttpYear.value; 
		if(objGridData.value !=''){ 
		//document.getElementById("DivApprove").style.display = 'block';
		gridObj = initGridData('dataGridDiv', objGridData);
		gridObj.attachHeader("<input type='checkbox' value='no'  name='selectAll' onClick='javascript:fnChangedFilter(this);'/>"
				+',#text_filter,#select_filter,#numeric_filter,#numeric_filter,#numeric_filter,#numeric_filter,#text_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter');
		gridObj.attachEvent("onCheckbox", doOnCheck);
		check_rowId = gridObj.getColIndexById("check");
		status_Id = gridObj.getColIndexById("status_id");
		ttp_detail_id = gridObj.getColIndexById("ttp_detail_id");
		ttp_name_Id = gridObj.getColIndexById("ttp_name");
		hdttp_name = gridObj.getColIndexById("hd_ttp_nm");
		 gridObj.forEachRow(function(rowId) {
			 statusVal = gridObj.cellById(rowId, status_Id).getValue();
			 // 26240574 - Locked
			 if(statusVal=="26240574"){
			   gridObj.cellById(rowId, check_rowId).setDisabled(true);
			 }
			 });
		 
		 // based on the date selected to enable/disable the transactions button.
		 
		 fnEnableDisableBtn();
		}
}

//This function is called while initiating the grid
function initGridData(divRef,gridData)
{
	var gObj = new dhtmlXGridObject(divRef);
		gObj.setSkin("dhx_skyblue");
		gObj.setImagePath("/extweb/dhtmlx/dhtmlxGrid/imgs/");
		gObj.enableMultiline(false);	
		// footer set
		var footer=new Array();
		footer[0]="";
		footer[1]="<b>Total</b>";
		footer[2]="";
		footer[3]="<b>{#stat_total}<b>";  //PC-3703: To Show total for Order Qty,3M-Avg Amt,3M-Avg Qty in TTP Summary Approval
		footer[4]="<b>{#stat_total}<b>";
		footer[5]="<b>{#stat_total}<b>";
		footer[6]="<b>{#stat_total}<b>";	
		footer[7]="";	
		footer[8]="";	
		footer[9]="";	
		footer[10]="";	
		footer[11]="";	
		footer[12]="";	
		gObj.attachFooter(footer);
		gObj.init();
		gObj.loadXMLString(gridData);
	return gObj;	
}
// Function is used to Check Box Check
function doOnCheck(rowId, cellInd, stage) {
	rowID = rowId;
	if (cellInd == check_rowId) {
		fnSelectAllToggle('selectAll');
	}
	return true;
}

// Description : This function to check the select all check box
function fnSelectAllToggle(varControl) {
	var objControl = eval("document.frmTTPApproval." + varControl);

	var all_row = gridObj.getAllRowIds(",");
	var all_rowLen = all_row.length; // get the rows id length
	var checked_row = gridObj.getCheckedRows(check_rowId);
	var finalval;

	finalval = eval(checked_row.length);
	if (all_rowLen == finalval) {
		objControl.checked = true;// select all check box to be checked
	} else {
		objControl.checked = false;// select all check box un checked.
	}
}

//this function used to select all option (check box)
function fnChangedFilter(obj) {
	
	if (obj.checked) {
		gridObj.forEachRow(function(rowId) {
			var check_disable_fl = gridObj.cellById(rowId, check_rowId).isDisabled();
			if (!check_disable_fl) {
				gridObj.cellById(rowId, check_rowId).setChecked(true);
			}
		});
	} else {
		gridObj.forEachRow(function(rowId) {
			check_id = gridObj.cellById(rowId, check_rowId).getValue();
			
			if (check_id == 1) {
				gridObj.cellById(rowId, check_rowId).setChecked(false);
			}

		});
	}
}
//Function used to call the Approve function
function fnApprove(){
	//Initialize
	var approveStr = '';
	var ttpId = document.frmTTPApproval.ttpId.value;   
	var status = document.frmTTPApproval.status.value;   
	var category = document.frmTTPApproval.category.value;   
	var ttpMonth = document.frmTTPApproval.ttpMonth.value;   
	var ttpYear = document.frmTTPApproval.ttpYear.value; 
	var searchttpId = document.frmTTPApproval.searchttpId.value;   
	document.frmTTPApproval.inputString.value = '';   
	var checked_row = gridObj.getCheckedRows(check_rowId);
	
	var errorStr = '';

    if(checked_row == '')
    { 
      Error_Details("Please select one record  to proceed"); 
    } 
    
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
    
	if(checked_row !=''){
		var chkVal = checked_row.split(",");
		for (var i=0;i<chkVal.length;i++){
			check_id = gridObj.cellById(chkVal[i], check_rowId).getValue();
			statusVal = gridObj.cellById(chkVal[i], status_Id).getValue();
			ttpNameVal = gridObj.cellById(chkVal[i], ttp_name_Id).getValue();
			// 50580 - Open, 50583 - Approve Failed
			if (check_id == 1) {
				if(statusVal != "50580" && statusVal != "50583")
					errorStr = errorStr + ttpNameVal + '<br>';
				else
					approveStr = approveStr + chkVal[i] +',';	
			}
		}
		
		if(errorStr !=''){
			Error_Details("Below TTP name(s) are not in Open/Approve Failed status :<b><br>"+ errorStr +"</b>");
		}
		
		if (ErrorCount > 0) {
		 		Error_Show();
		 		Error_Clear();
		 		return false;
		 	}
	}
	
	if (confirm("Are you sure you want to Approve ?")) {
		  // Using Ajax call for load the report
		document.frmTTPApproval.action = "/gmTTPSummaryApprove.do?method=saveTTPApproval&strOpt=Approve";
		document.frmTTPApproval.inputString.value = approveStr;
		fnStartProgress();
		document.frmTTPApproval.submit();
	
	}else{
		return false;
	}
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	

}

//This function used to press the enter key then submit the page.
function fnEnter() {
	if (event.keyCode == 13) {	
		fnLoad();
	 }		
}

//Function used to showfaileddtls
function fnFailedDtls(ttp_detail_id){
	windowOpener("/gmTTPSummaryApprove.do?&method=loadFailedTTPInformation&ttpDetailId="+ttp_detail_id, "Pack","resizable=yes,scrollbars=yes,top=150,left=150,width=850,height=500");
}
//For Past Month disable the approve button
function fnEnableDisableBtn (){
	var ttpMonth = document.frmTTPApproval.ttpMonth.value;   
	var ttpYear = document.frmTTPApproval.ttpYear.value; 
	var date=new Date();
	
	if ((date.getMonth()+1!=ttpMonth)||(date.getFullYear()!=ttpYear)||(SwitchUserFl == 'Y')){
		
		if(document.frmTTPApproval.BTN_APPROVE != undefined)
			document.frmTTPApproval.BTN_APPROVE.disabled = true;
		
		if(document.frmTTPApproval.LBL_LOCK_AND_GENERATE != undefined && lockAccFl =='Y')
			document.frmTTPApproval.LBL_LOCK_AND_GENERATE.disabled = true;
     }else {
    
    	 if(document.frmTTPApproval.BTN_APPROVE != undefined)
    		 document.frmTTPApproval.BTN_APPROVE.disabled = false;
    	 
    	 if(document.frmTTPApproval.LBL_LOCK_AND_GENERATE != undefined && lockAccFl =='Y')
    		 document.frmTTPApproval.LBL_LOCK_AND_GENERATE.disabled = false;
    	 
	}
}


//Function lock and generate
function fnTTPLock(){
		var ttpLockStr = '';
		var invalidTTPName = '';
		
		gridObj.forEachRow(function(rowId) {
			chkVal = gridObj.cellById(rowId, check_rowId).getValue();
			statusVal = gridObj.cellById(rowId, status_Id).getValue();
			ttpNameVal = gridObj.cellById(rowId, ttp_name_Id).getValue();
			
			// 50582 - Approved, 26240575 - Lock Failed
			
			if (chkVal =='1'){
				if(statusVal !='50582' && statusVal !='26240575'){
					invalidTTPName = invalidTTPName + ttpNameVal +'<br>';
				}else
				{
					ttpLockStr = ttpLockStr + rowId +',';
				}
			}
			
		
		}); 
		
	    if(ttpLockStr == '')
	    { 
	      Error_Details("Please select one record  to proceed"); 
	    } 
	    
	    if(invalidTTPName !='')
	    {
	    	Error_Details("Below TTP name(s) are not in Approved/Lock Failed status :<b><br>"+invalidTTPName +"</b>");
	    }
	    
	    if (ErrorCount > 0) {
			Error_Show();
			Error_Clear();
			return false;
		}			 	
	    
	    if (confirm("Are you sure you want to Lock ?")) {
	    	document.frmTTPApproval.action = "/gmTTPSummaryApprove.do?method=saveTTPApproval&strOpt=Lock";
			document.frmTTPApproval.inputString.value = ttpLockStr;
			fnStartProgress();
			document.frmTTPApproval.submit();
	    }
		
}



//This function used to export the data to excel file.
function fnExport()
{
	gridObj.setColumnHidden(0,true);
	gridObj.toExcel('/phpapp/excel/generate.php');
	gridObj.setColumnHidden(0,false);
}

//This Function is used to redirect the TTP Finalizing Screen as Popup
function fnTTPFinalize(ttpid,loaddate,levelval){
	var ttpname = gridObj.cellById(ttpid, hdttp_name).getValue();
	var spiltloaddate= loaddate.split("/");
	windowOpener("/gmOPTTPMonthlySummary.do?&strOpt=report&ttpName="+ttpname+"&levelId="+levelval+"&ttpId="+ttpid+"&monthId="+spiltloaddate[0]+"&yearId="+spiltloaddate[2]+"&hCompanyID="+levelval, "Pack","resizable=yes,scrollbars=yes,top=150,left=150,width=850,height=500");
}
