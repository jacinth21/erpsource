//on selection of Project List for the dropdown
function fnOnChenge(){
	fnReload();
}

// on hitting the Enter Key to load the Report
function fnOnHitEnter(){
	if (event.keyCode == 13){
		var returnVal = fnReload();
		if(returnVal == false){
			if(!e) var e = window.event;
	        e.cancelBubble = true;
	        e.returnValue = false;
	        if (e.stopPropagation){
	        	e.stopPropagation();
	        	e.preventDefault();
	        }
		}
	}
}

function fnReload()
{    
	if(document.frmPartLeadTimeSetup.pnum.value == "" && document.frmPartLeadTimeSetup.projectListID.value == 0)
	{
		Error_Details('Please choose a value from the <b>Project List</b> dropdown or enter a valid part # in <b>Part Numbers</b> textbox to proceed.' );
	}
	if (ErrorCount > 0)  
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	document.frmPartLeadTimeSetup.strOpt.value = "reload"; 
	fnStartProgress('Y');
	document.frmPartLeadTimeSetup.submit();   
} 

function  fnLeadtimeHistory(txnid,auditid)
{
	windowOpener("/GmPageControllerServlet?strPgToLoad=/gmAuditTrail.do&auditId="+auditid+"&txnId="+txnid,"","resizable=yes,scrollbars=yes,top=250,left=300,width=785,height=300,status=1");
}

function fnLog(type,id)
{
	var varPLId = document.frmPartLeadTimeSetup.projectListID.value;
	windowOpener("/GmCommonLogServlet?hType="+type+"&hID="+id+"&hJNDIConnection="+JNDI_CONNECTION,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
}

function fnLeadTimeValidate(obj){
	var leadValue = TRIM(obj.value);
	if((leadValue != '' && (leadValue < 4  || leadValue > 52))  || (isNaN(leadValue))){	
		document.frmPartLeadTimeSetup.Btn_Submit.disabled = true;
		Error_Details('<b>Quantity</b> entered should be between 4 and 52.' );
	}else{
		if(SwitchUserFl == 'Y'){
			document.frmPartLeadTimeSetup.Btn_Submit.disabled = true;
		}else{
			document.frmPartLeadTimeSetup.Btn_Submit.disabled = false;
		}
	}	
	if (ErrorCount > 0){
			Error_Show();
			Error_Clear();
			return false;
	}
}

function fnSubmit()
{ 	
	
    var varProjId =  document.frmPartLeadTimeSetup.projectListID.value;
    var varPnum =  document.frmPartLeadTimeSetup.pnum.value;
    var varleadtimeval = '';
    var varpnum = '';
	var inputString = '';
	var invalidleadtime = '';
	var varleadtimeOld = '';
	var chg_fl = "N";
	 
	
	if(varProjId!=null || varPnum != null)
	{
		for (var i =0; i < varRows; i++) 
		{
			objpnum = eval("document.frmPartLeadTimeSetup.pnum"+i);
			objleadtime = eval("document.frmPartLeadTimeSetup.txt_leadtimeval"+i);
			objleadtimeOld = eval("document.frmPartLeadTimeSetup.hLeadTimeVal"+i);
			
			if(objpnum != null || objpnum != undefined)
			{
				varpnum = objpnum.value;
			}
						
			if(objleadtime != null || objleadtime != undefined)
			{
				varleadtimeval = TRIM(objleadtime.value);
			}
			
			varleadtimeOld = TRIM(objleadtimeOld.value);
			var objRegExp = /(^-?\d*$)/;
			if((varleadtimeval != '' && (varleadtimeval < 4  || varleadtimeval > 52)) || !objRegExp.test(varleadtimeval)||varleadtimeOld!='' && varleadtimeval=='')
			{	
				invalidleadtime = invalidleadtime + "<br>" + varpnum;
			}else
			{
				inputString = inputString + varpnum + '^' + varleadtimeval + '|';
			}
			if (varleadtimeOld != varleadtimeval)
			{
				chg_fl = "Y";
			}
		} 
	 
	}
	
	if(chg_fl == "N"){
		Error_Details("Please Enter value for atleast one part number.");
	}
	if(invalidleadtime != ""){
		
		Error_Details("<b>Quantity</b> entered should be between 4 and 52 for <b>Part #: "+invalidleadtime+"</b>");
		 
	}
	
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	fnStartProgress('Y');
 	document.frmPartLeadTimeSetup.strOpt.value = 'save';
 	document.frmPartLeadTimeSetup.hinputStr.value = inputString;
 	document.frmPartLeadTimeSetup.Btn_Submit.disabled = true;
 	fnStartProgress('Y');
	document.frmPartLeadTimeSetup.submit();
	 
}

function fnLoad(){	
	var sel = document.frmPartLeadTimeSetup.hSearch.value;
	if (sel != ''){
		document.frmPartLeadTimeSetup.cbo_search.value = sel;
	}
}
