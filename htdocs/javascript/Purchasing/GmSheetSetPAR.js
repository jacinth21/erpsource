	
	function fnOnpageLoad(){
		var setID 	= document.frmSheetSetPAR.demandSetId.value;
		var setName	= document.frmSheetSetPAR.demandSetNm.value;
		var demandsheetNm 	= document.frmSheetSetPAR.demandSheetName.value;
		if((setID != '' || setName != '') && demandsheetNm == 0  && varRows == 0){
			Error_Details("Set Par can be set only for sets mapped to a worldwide consignment sheet");
			Error_Show();
			Error_Clear();
			return false;
		}
	}
	function fnLoad(){
		var setID 		    = document.frmSheetSetPAR.demandSetId.value;
		var setName 	    = document.frmSheetSetPAR.demandSetNm.value;
		var demandsheetNm 	= document.frmSheetSetPAR.demandSheetName.value;
		if((demandsheetNm == '0') && (setID == '') && (setName == '')){
			Error_Details("Please fill atleast one filter to proceed.");
		}
		if (ErrorCount > 0){
			Error_Show();
			Error_Clear();
			return false;
		}
	 	fnStartProgress('Y');
		document.frmSheetSetPAR.strOpt.value = "reload";
		document.frmSheetSetPAR.submit();
	}
	
	function fnPARValidate(obj){
		var parValue = TRIM(obj.value);
		var objRegExp = /(^-?\d*$)/;
		if((parValue < 0 || parValue >= 99) && (parValue != '') || isNaN(parValue)&& (parValue != '')){			
			document.frmSheetSetPAR.Btn_Submit.disabled = true;
			Error_Details("<B>PAR Value</B> entered should be greater than or equal to 0 and less than 99");
		}else{
			if(accessfl == 'Y' && SwitchUserFl != 'Y'){
				document.frmSheetSetPAR.Btn_Submit.disabled = false;
			}else{
				document.frmSheetSetPAR.Btn_Submit.disabled = true;
			}
		}	
		if (ErrorCount > 0){
			Error_Show();
			Error_Clear();
			return false;
		}				
	}
	
	function fnSubmit(){
		var parValue 		= '';
		var varSet  		= '';
		var inputString 	= ''; 
		var demandSheetId  	= document.frmSheetSetPAR.demandSheetName.value;		
		var varSetID 	   	= document.frmSheetSetPAR.demandSetId.value;
		var errSet = '';
		var oldParval 	= ''; 
		var chg_fl = "N";
		var setName 	    = document.frmSheetSetPAR.demandSetNm.value; 
		if((demandSheetId == '0') && (varSetID == '') && (setName == '')){
			Error_Details("Please fill atleast one filter to proceed.");
		}
		if (ErrorCount > 0){
			Error_Show();
			Error_Clear();
			return false;
		}
		
			if(demandSheetId != null || varSetID != null){
				if(demandSheetId == '0'){
					demandSheetId = '';
				}
				for (var i =0; i < varRows; i++){
					objSetID = eval("document.frmSheetSetPAR.setid"+i);
					objParValue = eval("document.frmSheetSetPAR.parvalue"+i);	
					objOldParVal = eval("document.frmSheetSetPAR.hPARVal"+i);
					if(objSetID != null || objSetID != undefined){
						varSet = objSetID.value;
					}								
					if(objParValue != null || objParValue != undefined){
						parValue = TRIM(objParValue.value);
					}		

					oldParval = TRIM(objOldParVal.value); 
					if( parValue != oldParval ){
						if(parValue >= 0 && parValue < 99 || parValue == '' ){	
							inputString = inputString + demandSheetId + '^' + varSet + '^' + parValue + '|';
						}else{
							errSet = errSet + "<br>" + varSet;
						}
						chg_fl = "Y";
			        }
					 
				}			 
			}	
			if(chg_fl == "N"){
				Error_Details("Please Enter Par value for atleast one set.");
				Error_Show();
				Error_Clear();
				return false;
			}
			
			if(errSet != ''){
				Error_Details("<B>PAR Value</B> entered should be greater than or equal to 0 and less than 99 for the following set(s)<B>"+errSet+ "</B>");
				Error_Show();
				Error_Clear();
				return false;
			}else{
			 	fnStartProgress('Y');
				document.frmSheetSetPAR.strOpt.value    = 'save';
				document.frmSheetSetPAR.hinputStr.value = inputString;
				document.frmSheetSetPAR.Btn_Submit.disabled = true;
				document.frmSheetSetPAR.submit();
			}
	}
	
	function fnParHistory(dtlid,auditid){
		windowOpener("/GmPageControllerServlet?strPgToLoad=/gmAuditTrail.do&auditId="+auditid+"&txnId="+ dtlid+"&hJNDIConnection="+JNDI_CONNECTION, "","resizable=yes,scrollbars=yes,top=250,left=300,width=785,height=300,status=1");
	}
	
	function fnLog(type,setid){
		windowOpener("/GmCommonLogServlet?hType="+type+"&hID="+setid+"&hJNDIConnection="+JNDI_CONNECTION,"StePARLog","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=300");
	}
	
//Enter key functionality.
function fnEnter(){	
	if(event.keyCode == 13)	{	 
		fnLoad();		
	}	
}