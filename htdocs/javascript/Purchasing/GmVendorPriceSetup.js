
function fnCallProject(val)
{
	document.frmVendor.hAction.value = 'Part';
  	document.frmVendor.submit();
}

function fnGo()
{
	fnValidateDropDn('Cbo_Proj',lblProjectName);
	fnValidateDropDn('Cbo_Part',lblPartNum);
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	document.frmVendor.hAction.value = 'Go';
	fnStartProgress('Y');
	document.frmVendor.submit();
}
var cnt = 0;
function fnAddRow(id)
{
	
	var vend = document.all.Vend.innerHTML;
	vend = vend.replace('rowid',cnt);
	var uom = document.all.UOM.innerHTML;
    var tbody = document.getElementById(id).getElementsByTagName("TBODY")[0];
    var row = document.createElement("TR")
    var td1 = document.createElement("TD")
    td1.innerHTML = vend;
    var td2 = document.createElement("TD")
    td2.innerHTML = fnCreateCell('TXT','Cost',5,16);
    td2.align = "Right";
    var td3 = document.createElement("TD")
    td3.innerHTML = fnCreateCell('TXT','QtyQ',3,'');
    td3.align = "Center";
    var td4 = document.createElement("TD")
    td4.innerHTML = fnCreateCell('TXT','Delv',15,72);
	var td5 = document.createElement("TD")
    td5.innerHTML = fnCreateCell('TXT','QiD',7,20);
    var td6 = document.createElement("TD")
    td6.innerHTML = uom;
    var td7 = document.createElement("TD")
    td7.innerHTML = fnCreateCell('TXT','UOMQ',3,6);
    td7.align = "Center";
    var td8 = document.createElement("TD")
    td8.innerHTML = fnCreateCell('CHK','Lock',3);
    td8.align = "Center";
    var td9 = document.createElement("TD")
    td9.innerHTML = fnCreateCell('CHK','Active',3);
    td9.align = "Center";
    //adding 2 new div for  validated and verified flag
    var td10 = document.createElement("TD")
    td10.innerHTML = "<div id='Validate_Fl_"+cnt+"'><td>N</td></div>";
    td10.align = "Center";
    var td11 = document.createElement("TD")
    td11.innerHTML = "<div id='Verified_Fl_"+cnt+"'><td>N</td></div>";
    td11.align = "Center";
        
    row.appendChild(td1);
    row.appendChild(td2);
    row.appendChild(td3);
    row.appendChild(td4);
    row.appendChild(td5);
    row.appendChild(td6);
    row.appendChild(td7);
    row.appendChild(td8);
    row.appendChild(td9);
    row.appendChild(td10);
    row.appendChild(td11);
    tbody.appendChild(row);
	cnt++;
}

function fnCreateCell(type,val,size,maxlength)
{
	val = val + cnt;
	var html = '';
	if (type == 'TXT')
	{
		html = '<input type=text  size='+size+' name=Txt_'+val+' maxlength='+maxlength+' class=InputArea>';
	}
	else if (type == 'CHK')
	{
		html = '<input type=checkbox name=Chk_'+val+'>';
	}
	return html;
}


function fnSubmit()
{
	var pnum = document.frmVendor.Cbo_Part.value;
	if (pnum != document.frmVendor.hPartNum.value)
	{
		Error_Details(message_operations[408]);
	}
	var obj = '';
	var i=0;
	var k=0;
	var hcnt = document.frmVendor.hCnt.value;
	var lockstr = '';
	
	var arr = '';
	var len = 0;
	var inputstr = '';
	var str = '';
	var pid = '';
	var vfl = '';
	var afl = '';
	var activestr = '';
	var lockobj = '';
	var vendorFl   = false;
	var emptyCost  = false;
	var wrongCost  = false;
	var UOMtype    = false;
	var UOMQty     = false;
	var UOMZeroQty = false;
	var exceedCost = false;
	var exceedUOM  = false;
	
	arr = document.all.Cbo_Vendor;
	var uomarr = document.all.Cbo_UOM;

	for (j=0;j<cnt;j++)
	{
		str = '';
		lockobj = eval("document.frmVendor.Chk_Lock"+j);
		if (lockobj)
		{
			obj = eval("document.frmVendor.hId"+j);
			if (obj)
			{
				pid = obj.value;
				vfl = 'U';
			}else{
				pid = '0';
				vfl = 'I';			
			}
			if (lockobj.value == 'Y')
			{
				obj = eval("document.frmVendor.hVendor"+j);
				str = str + obj.value+'^';			
			}
			else
			{
				obj = arr[k];
				len = obj.length;
				for (i=0;i<len;i++)
				{
					if (obj[obj.selectedIndex].value == '0'){
						vendorFl = true;
					}else{
						str = obj[obj.selectedIndex].value+'^';
					}
					break;
				}
			}
			
			obj = eval("document.frmVendor.Txt_Cost"+j);
			// Validation added for Cost Field for not allowing the special characters otherthan ZERO.
			var objval    = TRIM(obj.value);
			var cost	  = obj.value.replace(".","");
			var objRegExp = /(^-?\d\d*$)/;
			if (objval == ''){
				emptyCost = true;
			}
			if(!objval == '' && !objRegExp.test(cost)){
				wrongCost = true;
			}else{
				var costArray = obj.value.split(".");
				if(costArray[0].length > 13){
					exceedCost = true;
				}else{
				str = str + obj.value+'^';
				}				
			}
			obj = eval("document.frmVendor.Txt_QtyQ"+j);		
			str = str + obj.value+'^';
			obj = eval("document.frmVendor.Txt_Delv"+j);
			str = str + obj.value+'^';
			obj = eval("document.frmVendor.Txt_QiD"+j);
			str = str + obj.value+'^';
			
			if (lockobj.value == 'Y'){
				obj = eval("document.frmVendor.hUOM"+j);
				str = str + obj.value+'^';
			}else{	
				obj = uomarr[k];
				len = obj.length;
				for (i=0;i<len;i++)
				{
					if (obj[obj.selectedIndex].value == '0'){
						UOMtype = true;
					}else{
						str = str + obj[obj.selectedIndex].value+'^';
					}
					break;
				}
				k++;
			}

			obj = eval("document.frmVendor.Txt_UOMQ"+j);
			if (obj.value == ''){
				UOMQty = true;
			}else if(Math.round(obj.value) <= 0){
				UOMZeroQty = true;
			}else{	
				var uomQty = obj.value;
				if(uomQty.length > 6 ){
					exceedUOM = true;
				}else{
					str = str + obj.value+'^';
				}
			}
			obj = eval("document.frmVendor.Chk_Lock"+j);
			if (lockobj.value == 'Y'){			
				lfl = true;
			}else{
				lfl = obj.checked;
			}
			obj = eval("document.frmVendor.Chk_Active"+j);
			afl = obj.checked;

			if (lfl == true){
				lfl = 'Y';
			}else{
				lfl = 'N';
			}
			if (afl == true){
				afl = 'Y';
			}else{
				afl = 'N';
			}
						
			str = str + lfl + '^' + afl + '^'+ vfl +'^'+ pid;

			inputstr = inputstr + str + '|';
		}

	}// End of MAIN FOR

	fnValidationErrorMessages(vendorFl,emptyCost,wrongCost,exceedCost,UOMtype,UOMQty,exceedUOM,UOMZeroQty);
	
	if (ErrorCount > 0)
	{
			//inputstr = '';
			//document.frmVendor.inputString.value = '';
			Error_Show();
			Error_Clear();
			return false;
	}
	else
	{
		//alert(inputstr);
		document.frmVendor.inputString.value = inputstr;
		document.frmVendor.hAction.value = 'Save';
		fnStartProgress('Y');
		document.frmVendor.submit();
	}
}

function fnSetSelections()
{
	if (document.frmVendor.hAction.value == 'Go')
	{
		cnt = document.frmVendor.hCnt.value;
		var sel = document.frmVendor.strSelectedVendIds.value;
		if (sel != '')
		{
			sel = sel.substr(0,sel.length-1);
			var arr = sel.split(',');
			var len = arr.length;
			var vid = '';
			var vendarr = document.all.Cbo_Vendor;
			var vendlen = vendarr.length-1;
			var obj = '';
			var objlen = 0;
			for(var i=0;i<len;i++)
			{
				vid = arr[i];
				obj = vendarr[i];
				objlen = obj.length;
				for (var j=0;j<objlen;j++)
				{
					val = obj.options[j].value;
					if (val == vid)
					{
						obj.options[j].selected = true;
						break;
					}
				}
			}
		}
		
		var sel = document.frmVendor.hSelectedUOMIds.value;
		if (sel != '')
		{
			sel = sel.substr(0,sel.length-1);
			var arr = sel.split(',');
			var len = arr.length;
			var vid = '';
			var uomarr = document.all.Cbo_UOM;
			var uomlen = uomarr.length-1;
			var obj = '';
			var objlen = 0;
			for(var i=0;i<len;i++)
			{
				vid = arr[i];
				obj = uomarr[i];
				objlen = obj.length;
				for (var j=0;j<objlen;j++)
				{
					val = obj.options[j].text;
					if (val == vid)
					{
						obj.options[j].selected = true;
						break;
					}
				}
			}
		}	
	}	
}

// this ajax call will load the validated and verified flag
function fnPartVendorStatus(vendorid,rowid)
{
	var pnum = document.frmVendor.Cbo_Part.value; 
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/GmVendorPricingServlet?hAction=status&Cbo_Part='+encodeURIComponent(pnum)+'&hvendorid='+vendorid+'&hrowid='+rowid+'&ramdomId='+Math.random());
	dhtmlxAjax.get(ajaxUrl, fnStatus);
	return false;
	
}
// to load the validated and verified flag on screen 
function fnStatus(loader) {
	var response = loader.xmlDoc.responseText;
	var arr;
	if (response != null) {
		arr=response.split(",");
		if(arr.length > 0){
		var validText = document.getElementById("Validate_Fl_"+arr[2]);
		validText.innerHTML =arr[0];
		var verifyText = document.getElementById("Verified_Fl_"+arr[2]);
		verifyText.innerHTML =arr[1];
		}
	}
}
function fnValidationErrorMessages(vendorFl,emptyCost,wrongCost,exceedCost,UOMtype,UOMQty,exceedUOM,UOMZeroQty){
	if(vendorFl == true){
		Error_Details(message_operations[409]);
	}
	if(emptyCost == true){
		Error_Details(message_operations[410]);
	}
	if(wrongCost == true){
		Error_Details(message_operations[411]);
	}
	if(exceedCost == true){
		Error_Details(message_operations[412]);
	}
	if(UOMtype == true){
		Error_Details(message_operations[413]);
	}
	if(UOMQty == true){
		Error_Details(message_operations[414]);
	}
	if(exceedUOM == true){
		Error_Details(message_operations[415]);
	}
	if(UOMZeroQty == true){
		Error_Details(message_operations[417]);
	}
}