function fnSubmit()
{
	var poAction=document.frmOrder.Cbo_PO_Action.value;
	fnValidateDropDn('Cbo_PO_Action', message[5507]);
	
	    if(poAction== "106423"){   // Print PO
			fnViewPrint();
			return;
			}else if(poAction== "106421"){       //Print All WO	fnAllWOPrint(docRev,docActiveFl);
				fnAllWOPrint(docRev,docActiveFl);
				return;
		}else if(poAction== "106422"){    // Print FAX Cover		fnViewFaxPrint();
			if(editFl=='false'){    //access
				Error_Details(message_operations[597]);
			}else{
				fnViewFaxPrint();
				return;
			}
			if (ErrorCount > 0)
			{
					Error_Show();
					Error_Clear();
					return false;
			}
		} else if(poAction== "106427"){ // VOID WO
			var  rowCnt = document.frmOrder.hTxtCount.value;
			var cancelType = 'VDWO';
			var vcnt =0;
			var str = '';
			var DHRFl='';
			if(editFl=='false'){   //Security access (PMT-53276)
				Error_Details(message_operations[597]);
			}else{
				for (var i=0;i<rowCnt;i++)
				{
					val = eval("document.frmOrder.Chk_PartUp"+i);
					vPartFl = val.checked;
					val = eval("document.frmOrder.hDHRExistFL"+i);
					DHRFl = val.value;
					if(vPartFl == true){
						val = eval("document.frmOrder.hWOId"+i);
						vWO = val.value;
						str = str + vWO+",";
						vcnt ++;
					}
				}
				if(DHRFl == 'Y'){
					Error_Details(Error_Details_Trans(message_operations[750],vWO));
				}
			if ( vcnt == rowCnt)
			{
				Error_Details(message_operations[423]);
			}
			if(vcnt== "0"){
				Error_Details(message_operations[473]);
			}
			}
			if (ErrorCount > 0)
			{
					Error_Show();
					Error_Clear();
					return false;
			}
			
		       document.frmOrder.action="/GmCommonCancelServlet?hTxnName="+str+"&hCancelType="+cancelType+"&hTxnId="+str;
		       document.frmOrder.submit();

			
		} else if(poAction== "106425"){ // Update Part # Attribute
			var str = '';
			var vWO = '';
			var vPartNum = '';
			var vPartFl = '';
			var vcnt = 0;
			var vPoTotal = 0;
			var rowCnt = document.frmOrder.hTxtCount.value;
			if(editFl=='false'){ 
				Error_Details(message_operations[597]);
			}else{
				for (var i=0;i<rowCnt;i++)
				{
					val = eval("document.frmOrder.Chk_PartUp"+i);
					vPartFl = val.checked;
					if(vPartFl == true){
						val = eval("document.frmOrder.hWOId"+i);
						vWO = val.value;
						val = eval("document.frmOrder.hPartNum"+i);
						vPartNum = val.value;
						str = str + vWO+"^"+vPartNum+"|";
						vcnt ++;
					}
				}
				if(vcnt== "0"){
					Error_Details(message_operations[472]);
				}
				
				fnValidateTxtFld('Txt_LogReason',message_operations[151]);
				}
				if (ErrorCount > 0)
				{
						Error_Show();
						Error_Clear();
						return false;
				}
				document.frmOrder.hWOString.value = str;
				document.frmOrder.hPOTotal.value = vPoTotal;
				document.frmOrder.hAction.value = 'UpdatePO';
				document.frmOrder.action = "/GmPOServlet";
				fnStartProgress('Y');
			  	document.frmOrder.submit();
		}else{          			 // Update WO
			var vWO = '';
			var vQty = 0;
			var vCost = 0;
			var vFl = '';
			var vPoTotal = 0;
			var vCritical = '';
			var vValidateFl = '';
			var vFAR = '';
			var vRev = '';
			var vValidFl = '';
			var vCurrRev = '';
			var vRevAscii = '';
			var vCurrRevAscii = '';
			var vOrigQty = '';
			var vPendQty = '';
			var vAction = '';
			var vPoType = '';
			var vChangedQty = '';
			var vPartFl = '';
		
			var cnt = 0;
			var partCnt =0;
		
			var amt = 0;
			var temp = 0;
			var str = '';
			var rowCnt = document.frmOrder.hTxtCount.value;
			vPoType = document.frmOrder.hPOType.value;
			if(editFl=='false'){ 
				Error_Details(message_operations[597]);
			}else{
				for (var i=0;i<rowCnt;i++)
				{
					val = eval("document.frmOrder.Chk_PartUp"+i);
					vPartFl = val.checked;
					val = eval("document.frmOrder.hWOId"+i);
					vWO = val.value;
					val = eval("document.frmOrder.Txt_Qty"+i);
					if(val.disabled)
					{
						continue;
					}
					vQty = parseInt(val.value);
					val = eval("document.frmOrder.hOrigQty"+i);
					vOrigQty = parseInt(val.value);
					val = eval("document.frmOrder.hPendQty"+i);
					vPendQty = parseInt(val.value);
					
					if (vQty < 0)
					{
						Error_Details(Error_Details_Trans(message_operations[417],vWO));
					}
				
					vQty = (vQty - vPendQty) + vOrigQty;
			
					val = eval("document.frmOrder.Txt_Rate"+i);
					vCost = RemoveComma(val.value);
					
					val = eval("document.frmOrder.hValidateFlag"+i);
					vValidateFl = val.value;
					val = eval("document.frmOrder.Chk_Flag"+i);
					vCritical = val.checked;
					
					val = eval("document.frmOrder.Chk_FAR"+i);
					vFAR = val.checked;
					
					/* As this is giving Wrong Total PO Total Amount, this is calculated from Back end through new Function GET_PO_TOTAL_AMOUNT.fnc*/
					/*if (!vFl)
					{	
						vPoTotal = vPoTotal + vCost*vQty;
					}*/
					// For Delete Flag
			
			//Remove void flag value and send as N
			
					vFl = 'N';
					
					// For Critical Flag
					if (vCritical)
					{
						vCritical = 'Y';
					}else{
						vCritical = 'N';
					}
			
					// For FAR Flag
					if (vFAR)
					{
						vFAR = 'Y';
					}else{
						vFAR = 'N';
					}
					// For Validate Flag
					if (vValidateFl)
					{
						vValidFl = 'Y';
					}else{
						vValidFl = '';
					}		
					
					val = eval("document.frmOrder.Txt_Rev"+i);
					vRev = val.value;
					val = eval("document.frmOrder.hCurrRev"+i);
					vCurrRev = val.value;
					val = eval("document.frmOrder.Cbo_Action"+i);
					vAction = val.value;
			
			
					vChangedQty = vOrigQty - vQty;
					
					if (vPoType == 3101 && vChangedQty != 0)
					{
						if ((vQty != vPendQty) && vAction == 0 )
						{
							Error_Details(Error_Details_Trans(message_operations[418],vWO));
						}
						
						if ((vChangedQty > 0) && vAction == 'R' )
						{
							Error_Details(Error_Details_Trans(message_operations[419],vWO));
						}			
						if ((vChangedQty < 0) && (vAction == 'Q' ||vAction == 'S') )
						{
							Error_Details(Error_Details_Trans(message_operations[420],vWO));
						}			
						if (vFl == 'Y' && vAction == 'R' || vFl == 'Y' && vAction == 0)
						{
							Error_Details(Error_Details_Trans(message_operations[421],vWO));
						}
					}
			
			
					vRevAscii = parseInt(showKeyCode(vRev));
					vCurrRevAscii = parseInt(showKeyCode(vCurrRev));
					
					if (vRevAscii > vCurrRevAscii)
					{
						Error_Details(Error_Details_Trans(message_operations[422],vWO));
					}
					
					str = str + vWO+"^"+vQty+"^"+vCost+"^"+vFl+"^"+vCritical+"^"+vFAR+"^"+vRev+"^"+vValidFl+"^"+vAction+"|";
					partCnt ++;
				}
				fnValidateTxtFld('Txt_LogReason',message_operations[151]);
			}
			if (ErrorCount > 0)
			{
					Error_Show();
					Error_Clear();
					return false;
			}
	document.frmOrder.hPOTotal.value = vPoTotal;
	document.frmOrder.hWOString.value = str;
	
	document.frmOrder.hAction.value = 'UpdatePO';
	document.frmOrder.action = "/GmPOServlet";
	fnStartProgress('Y');
  	document.frmOrder.submit();
}
}

function fnSelectAll(intSize){
	var totalRows = document.frmOrder.hTxtCount.value;
		for (var i=0;i<totalRows;i++ )
			{
			if(document.frmOrder.Chk_selectAll.checked){
				obj = eval("document.frmOrder.Chk_PartUp"+i);
				if (obj.disabled == true){
					obj.unchecked = document.frmOrder.Chk_selectAll.checked;
				}else{
				obj.checked = document.frmOrder.Chk_selectAll.checked;
				}
			}
			else{
				obj = eval("document.frmOrder.Chk_PartUp"+i);	
				obj.checked=false;
				}	 
		}
}

function fnSelectCheck(){
	var cnt=0;
	var totalRows = document.frmOrder.hTxtCount.value;
	for (var i=0;i<totalRows;i++ )
		{
			obj = eval("document.frmOrder.Chk_PartUp"+i);
			if (obj.checked == false){
				document.frmOrder.Chk_selectAll.checked=false;
			}
		}
}

function fnCheckVal(i,woRev,critFl,FARFl,penQty,price){
	var val='';
	var woRevEdit='';
	var critFlEdit='';
	var FARFlEdit='';
	var penQtyEdit='';
	var priceEdit='';
	if(critFl=='' || critFl==false){
		critFl=false;
	}else{
		critFl=true;
	}
	if(FARFl='' || FARFl==false){
		FARFl=false;
	}else{
		FARFl=true;
	}
	
	val = eval("document.frmOrder.Txt_Rev"+i);
	woRevEdit=val.value;
	val = eval("document.frmOrder.Chk_Flag"+i);
	critFlEdit=val.checked;
	val = eval("document.frmOrder.Chk_FAR"+i);
	FARFlEdit=val.checked;
	val = eval("document.frmOrder.Txt_Qty"+i);
	penQtyEdit=val.value;
	val = eval("document.frmOrder.Txt_Rate"+i);
	priceEdit=val.value;
	val = eval("document.frmOrder.Chk_PartUp"+i);
	if(woRev!=woRevEdit || critFl!=critFlEdit || FARFl!=FARFlEdit ||  penQty!=penQtyEdit || price!=priceEdit){
		if (val.disabled == true){
			val.checked = false;
		}else{
		val.checked = true;
		document.frmOrder.Cbo_PO_Action.value='106426';//--update
		}
	}else{
		val.checked = false;
	}
	
}

function fnViewPrint()
{
	vid = 'All';
	windowOpener("/GmPOServlet?hAction=PrintPO&hPOId="+poid+"&hVenId="+vid,"PO1","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnAllWOPrint(rev,fl)
{
	windowOpener("/GmPOServlet?hAction=PrintAllWO&hPOId="+poid+"&hDocRev="+rev+"&hDocFl="+fl,"PO2","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnViewFaxPrint()
{
	windowOpener("/GmPOServlet?hAction=PrintFax&hPOId="+poid,"PO3","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}


function fnCallWO(wo,rev,fl)
{
	windowOpener("/GmWOServlet?hAction=PrintWO&hWOId="+wo+"&hDocRev="+rev+"&hDocFl="+fl,"WO1","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=640");
}


function showKeyCode(vRev)
{
	var character = vRev;
	var code = vRev.charCodeAt(0);
	return code;
}

function fnOpenLog(id)
{
	windowOpener("/GmCommonLogServlet?hType=1232&hID="+id,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
}

function fnReOpenPO(woId, poId)
{
	document.frmOrder.hAction.value = "Load";
	document.frmOrder.action ="/GmCommonCancelServlet";
	document.frmOrder.hTxnId.value = woId;
	document.frmOrder.hTxnName.value = poId;
	document.frmOrder.submit();
}

function fnQtyHistory(val){
	windowOpener("/GmPageControllerServlet?strPgToLoad=/gmAuditTrail.do&auditId=1027&txnId="+val,"","resizable=yes,scrollbars=yes,top=250,left=300,width=785,height=300,status=1");
}

function fnPriceHistory(val){
	windowOpener("/GmPageControllerServlet?strPgToLoad=/gmAuditTrail.do&auditId=1026&txnId="+val,"","resizable=yes,scrollbars=yes,top=250,left=300,width=785,height=300,status=1");
}
