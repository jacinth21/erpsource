
function fnReset()
{
	document.frmOPDemandSheetSetup.demandSheetId.value = '0';
	document.frmOPDemandSheetSetup.demandSheetType.value = '';
	document.frmOPDemandSheetSetup.demandSheetName.value = '';
	document.frmOPDemandSheetSetup.demandPeriod.value = '';
	document.frmOPDemandSheetSetup.forecastPeriod.value = '';
	document.frmOPDemandSheetSetup.hierarchyId.value = '0';	
	document.frmOPDemandSheetSetup.includeCurrMonth.checked = false;
	document.frmOPDemandSheetSetup.activeflag.checked = false; 
	document.frmOPDemandSheetSetup.submit();
}	

function fnFetch()
{	
	document.frmOPDemandSheetSetup.strOpt.value = "edit";
	document.frmOPDemandSheetSetup.submit();
}

function fnSubmit()
{
	//fnValidateTxtFld('demandSheetName',' Demand Sheet Name ');
	//fnValidateTxtFld('demandPeriod',' Demand Period ');
	//fnValidateTxtFld('forecastPeriod',' Forecast Period ');
	//fnValidateDropDn('hierarchyId',' Team ');
	
	//When the demand sheet status is changed(Active <--> InActive), comments is mandatory. 
	var frmObj=document.frmOPDemandSheetSetup;	
	var chkActiveFl = (frmObj.activeflag.checked)?"on":"off";
	var hiddenActiveFl = (frmObj.hActiveFl.value == "")?"off":frmObj.hActiveFl.value;
	if(chkActiveFl != hiddenActiveFl){
		fnValidateTxtFld("txt_LogReason", "Comments");
	}
	if (ErrorCount > 0)
		 {
			Error_Show();
			Error_Clear();
			return false;
		}
	if(document.frmOPDemandSheetSetup.activeflag.checked == true){
		document.frmOPDemandSheetSetup.strOpt.value = 'inactivate';
	}else{
		document.frmOPDemandSheetSetup.strOpt.value = 'save';
	}
	document.frmOPDemandSheetSetup.demandSheetOwner.disabled = false;
	fnStartProgress('Y');
	document.frmOPDemandSheetSetup.submit();
}

function fnJobSetup()
{
	fnValidateDropDn('demandSheetId',' Demand Sheet Name ');
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	varRefId = document.frmOPDemandSheetSetup.demandSheetId.value;
	varRefTypeText = 'Order Planning Load';
	varRefType = 90720;
	varJobName = document.frmOPDemandSheetSetup.demandSheetName.value;
	varStrOpt = "edit";
	windowOpener("/GmPageControllerServlet?strPgToLoad=jobSetup.do&refId="+varRefId+"&refTypeText="+varRefTypeText+"&jobName="+varJobName+"&refType="+varRefType+"&strOpt="+varStrOpt,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=750,height=480");
}

function fnMapping()
{
	fnValidateDropDn('demandSheetId',' Demand Sheet Name ');
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	varSheetId = document.frmOPDemandSheetSetup.demandSheetId.value;
	varSheetName = document.frmOPDemandSheetSetup.demandSheetName.value;
	varRefType = document.frmOPDemandSheetSetup.demandSheetType.value;
	varRefTypeName = document.frmOPDemandSheetSetup.demandSheetTypeName.value;
	
	//varRefTypeName = document.frmOPDemandSheetSetup.demandSheetType.options[document.frmOPDemandSheetSetup.demandSheetType.selectedIndex].text;
	
	windowOpener("/GmPageControllerServlet?strPgToLoad=gmOPDemandGroupMap.do&demandSheetId="+varSheetId+"&demandSheetName="+varSheetName+"&demandSheetTypeId="+varRefType,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=785,height=550,status=1");

//	windowOpener("/GmPageControllerServlet?strPgToLoad=gmDemandGroupMap.do","PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
}

function fnSetMapping()
{
	fnValidateDropDn('demandSheetId',' Demand Sheet Name ');
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	varSheetId = document.frmOPDemandSheetSetup.demandSheetId.value;
	  
	windowOpener("/GmPageControllerServlet?strPgToLoad=gmOPSetMapping.do&demandSheetId="+varSheetId,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=785,height=550,status=1");
  }

function fnTeamSetup()
{
	Error_Details(" Coming soon..... ");
	
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
}

function fnVoid()
{
		fnValidateDropDn('demandSheetId',' Demand Sheet Name ');
		if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
		document.frmOPDemandSheetSetup.action ="/GmCommonCancelServlet";
		document.frmOPDemandSheetSetup.hTxnId.value = document.frmOPDemandSheetSetup.demandSheetId.value;
		document.frmOPDemandSheetSetup.hTxnName.value = document.frmOPDemandSheetSetup.demandSheetName.value;
		document.frmOPDemandSheetSetup.submit();
}
function fnOpenLaunch(){
	fnValidateDropDn('demandSheetId',' Demand Sheet Name ');
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	varSheetId = document.frmOPDemandSheetSetup.demandSheetId.value;	 
	windowOpener("/gmOPLaunch.do?demandSheetId="+varSheetId,"Launch","resizable=yes,scrollbars=yes, top=350,left=450,width=780,height=550");
}
 

