
 

function fnReset()
{
	document.frmOPGroupPartMapping.setID.value = '0';
	document.frmOPGroupPartMapping.groupName.value = '';
	document.frmOPGroupPartMapping.groupType.value = '0';
	document.frmOPGroupPartMapping.activeflag.checked = false;
} 	

function fnFetch()
{	
	fnReset();
	document.frmOPGroupPartMapping.haction.value = 'edit';
	document.frmOPGroupPartMapping.submit();
}



function fnEditId(val)
{	
	document.frmOPGroupPartMapping.haction.value = 'edit';
	document.frmOPGroupPartMapping.hvendCatMapId.value = val;
	document.frmOPGroupPartMapping.submit();
}

function fnSubmit(){


	if(!document.frmOPGroupPartMapping.activeflag.checked) 
	document.frmOPGroupPartMapping.hactiveflag.value =  'true';
	else document.frmOPGroupPartMapping.hactiveflag.value ="false";

	var actionval = document.frmOPGroupPartMapping.haction.value;
	var groupName = document.frmOPGroupPartMapping.groupName.value;
	var groupid = document.frmOPGroupPartMapping.groupId.value;
//	var deptid = document.frmOPGroupPartMapping.deptId.value;
	  
	/*if(actionval == '')
	{
		Error_Details(" If you are mapping a new group, please specify the <B>Group Name </B>and then <B>Load</B> the parts. Please Submit the parts after the Load ");	
	}*/ 
	
 
		
	rowcount = window.frames[0].fnCheckPartRows();
	// alert("rowcount   is "+ rowcount);  
 	var primaryPartChecks = ''; 
 	if(rowcount == 0)
 	{	 
		alert(" Please Load parts for the Group");
		return false;
			}
			
	if(rowcount >1)
	{	  
	    objCheckPartArr=window.frames[0].fnCheckPartSelect(); 
	     
		if(objCheckPartArr)
		{ 
			primaryPartChecks =  window.frames[0].fnCheckPrimaryPartSelect();
		 
  		  }
    }
    else  {	 
    
    	primaryPartChecks =  window.frames[0].fnCheckPrimaryPartSelect();
    		
    		}

	fnValidateTxtFld('groupName',' Group Name '); 
	fnValidateDropDn('setID',' System '); 
	fnValidateDropDn('groupType',' Group Type '); 
	fnValidateDropDn('pricedType',' Priced Type '); 
 	

	if(primaryPartChecks.length >= 4000 )
	{
	   Error_Details("Number of parts selected exceeds the maximum limit, <br> Please narrow down your selection ");
	}

	if (ErrorCount > 0)
	 {
		Error_Show();
		Error_Clear();
		return false;
	}
	
	//MNTTASK-6106. When should save, enable GroupName and Priced Type. 
	document.frmOPGroupPartMapping.groupName.disabled = false;
	document.frmOPGroupPartMapping.pricedType.disabled = false;
	
	document.frmOPGroupPartMapping.haction.value =  'save'; //'save';
	document.frmOPGroupPartMapping.hinputPrimaryParts.value = primaryPartChecks;
	objCheckedParts = window.frames[0].fnCheckPartSelect();
	var inputPartChecks='';
	var objCheckedPartsArrLen =0;
	
	 
	 if(rowcount > 1)
 	{
 		objCheckedPartsArrLen = objCheckedParts.length;
		for (var k=0; k < objCheckedPartsArrLen; k++) 
			{ // alert("checked value    "+objCheckedParts[k].checked);
				if( objCheckedParts[k].checked == true)
				 	{ 
				 		if(k < objCheckedPartsArrLen -1)
				 		{
					    	inputPartChecks = inputPartChecks + objCheckedParts[k].value +',';
					    	}
					    else 
					    {
					    	inputPartChecks = inputPartChecks + objCheckedParts[k].value;
					    	}			    		 
				 	}
			}
		}
	else{  
			if(objCheckedParts.checked)
				{
				inputPartChecks = objCheckedParts.value;
			 }
		}
		
	//alert("inputPartChecks    "+inputPartChecks);
	document.frmOPGroupPartMapping.inputString.value = inputPartChecks;
	fnStartProgress('Y');
	document.frmOPGroupPartMapping.submit();

}




function fnVoid()
{
		fnValidateDropDn('groupId',' GroupList ');
		returnval = window.frames[0].fnVoidValidateCheck();
		if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}

		if(returnval== true)
	{
		document.frmOPGroupPartMapping.action ="/GmCommonCancelServlet";
		document.frmOPGroupPartMapping.hTxnId.value = document.frmOPGroupPartMapping.groupId.value;
		document.frmOPGroupPartMapping.hTxnName.value = document.frmOPGroupPartMapping.groupName.value;
		document.frmOPGroupPartMapping.submit();
	}
}	

 
