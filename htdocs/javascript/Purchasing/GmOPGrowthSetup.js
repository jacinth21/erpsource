
function fnSubmit()
{
var growthstr = '';
var obj;
var obj1;
var id;

	for (var i=1;i<13 ;i++ )
	{
		obj = eval("document.frmOPGrowthSetup.month"+i);		
		val = RemoveComma(obj.value);
		id = obj.id;
		obj1 = eval("document.frmOPGrowthSetup.refMonth"+i);
		val1 = obj1.value;
		
		growthstr = growthstr + id + '^' + val + '^'+ val1 + '|';
	}
	document.frmOPGrowthSetup.inputString.value = growthstr;
	
	fnValidateDropDn('refType',' Ref Type ');
	fnValidateDropDn('refId',' Ref Values ');
	fnChkPrjSt();
 
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	document.frmOPGrowthSetup.strOpt.value = 'save';
	fnStartProgress('Y');
	document.frmOPGrowthSetup.submit();
}

function fnCheckYear()
{
	reftype = document.frmOPGrowthSetup.refType.value;
	var selectedYear = document.frmOPGrowthSetup.year.options[document.frmOPGrowthSetup.year.selectedIndex].text;
	 //alert('selected yr '+selectedYear);
	// alert('year '+year);	
	// alert('month ' +month);
	for (var i=1;i<13 ;i++ )
	{
		 
		obj = eval("document.frmOPGrowthSetup.month"+i);	
		obj1 = 	eval("document.frmOPGrowthSetup.refMonth"+i);	
	 	obj2 = 	eval("document.frmOPGrowthSetup.controlValue"+i);	
		val = RemoveComma(obj.value);
		id = obj.id;
	//	alert('chking month '+parseInt(id,10));
	//	alert('controlval '+obj2.value);

	 	if((selectedYear < year ))//||((obj2.value=='D')&&(selectedYear <= year)))		 
		{ 		
		//	alert('1. disabling '+obj.id);
		   obj.disabled = true;
		   obj1.disabled = true;
		}	
		else if (selectedYear == year && parseInt(id,10) < month )
		{
		//	alert('2. disabling '+obj.id);
			obj.disabled = true;
		   obj1.disabled = true;
		}
		else if(selectedYear > year) 
		{
		   obj.disabled = false;
		   obj1.disabled = false;
		}		
	}	
}

function fnTemplateFilterSheet()
{
	var templateId = document.frmOPGrowthSetup.templateId.value;
	if(templateId != "0")
	{ 
		document.frmOPGrowthSetup.action="/gmOPGrowthSetup.do";	
	}  
	document.frmOPGrowthSetup.submit();
}

function fnTypeFilterSheet()
{
	var sheetTypeId = document.frmOPGrowthSetup.sheetTypeId.value;
	if(sheetTypeId != "0")
	{ 
		document.frmOPGrowthSetup.action="/gmOPGrowthSetup.do";	
	}  
	document.frmOPGrowthSetup.submit();
}

function fnRegionFilterSheet()
{
	var regionId = document.frmOPGrowthSetup.regionId.value;
	if(regionId != "0")
	{ 
		document.frmOPGrowthSetup.action="/gmOPGrowthSetup.do";	
	}  
	document.frmOPGrowthSetup.submit();
}

function fnReload()
{

  //document.frmOPGrowthSetup.Voiding.disabled=false;

	if(validateDate(document.frmOPGrowthSetup.fromDate.value,'From Date')
	  &&
	   validateDate(document.frmOPGrowthSetup.toDate.value,'To Date')){

	  var refType = document.frmOPGrowthSetup.refType.value;
	  if( refType == "20295" || refType == "20298" ){
										disabledenabled = "enabled";
									}
	   
		
	  if(refType != '0')//if(fnLoadValidate()) //changed this condition for BUG-2638.
	  { 
		var voidflag = document.frmOPGrowthSetup.refType.value;
		document.frmOPGrowthSetup.strOpt.value = 'reload';
		document.frmOPGrowthSetup.submit();
	  }
	}
}


function fnOpenValidate()
{
	var varRefId = document.frmOPGrowthSetup.demandSheetId.value;	
	var varRefType = document.frmOPGrowthSetup.refType.value;	
	windowOpener("/GmOPGrowthReportServlet?demandSheetId="+varRefId+"&hAction=LoadReport"+"&refInputs="+varRefType,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=1100,height=480");	
}

function fnReloadPnum()
{
	var selectedRef = document.frmOPGrowthSetup.pnum.value;
	var flag =''; 
	// Commented for Growth Screen Enhancement as Ref ID drop down is not valid anymore.
	/*
    obj = document.all.refId;
	for (i=0;i<obj.length;i++)
	{
		if (obj.options[i].value == selectedRef)
		{
			obj.options[i].selected = true;
		 	flag = '1';
			break;
		}
	}
    if(flag!="1") 
    {
	 	Error_Details(message[207]);
	}*/
  		fnReloadRef(); 		 
}

function fnReloadReferences()
{
var refType = document.frmOPGrowthSetup.refType.value;
if(document.frmOPGrowthSetup.applyAll != null)
  	{		 
		document.frmOPGrowthSetup.applyAll.value = '';		 
	}
    var demandId = document.frmOPGrowthSetup.demandSheetId.value;
	if(demandId != "0" ){
	  if(refType != "0" && fnLoadValidate())
	  { 
	 	document.frmOPGrowthSetup.strOpt.value = 'reload';	
	  }  
	}
  document.frmOPGrowthSetup.submit();
}

function fnLoadValidate()
{
 var refType = document.frmOPGrowthSetup.refType.value;
 var demandId =  document.frmOPGrowthSetup.demandSheetId.value;
 
 if(refType == "0" || demandId == "0")
  {
    Error_Details(message[202]);
  }
  
  if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}	

  return true;
}

function fnOverride(month,monthwords)
{

var selectedYear = document.frmOPGrowthSetup.year.options[document.frmOPGrowthSetup.year.selectedIndex].text;
var growthId = document.frmOPGrowthSetup.growthId.value;
var refValue = document.frmOPGrowthSetup.refId.value;
var refType = document.frmOPGrowthSetup.refType.value;
var selectedMonthGrowth = eval("document.frmOPGrowthSetup.month"+parseInt(month,10));
var selectedRefMonth = eval("document.frmOPGrowthSetup.refMonth"+parseInt(month,10));
var selectedRefMonthDisabled = "false";
var pdisable = ""
var selectedYearChar = selectedYear.substring(2);
var demandsheetmonthid =  document.frmOPGrowthSetup.demandSheetId.value;

if(refType == 20296)
 {
    selectedRefMonthDisabled = "true";
 }


var overrideTypeValue = 20395;

if(refValue == 0)
 {
  	refValue = document.frmOPGrowthSetup.pnum.value;
 }

if(selectedYear == '[Choose One]')
{
	 Error_Details(message[203]);
}
else if(growthId == '')
{
	 Error_Details(message[204]);
}
else if(selectedYear < year || (selectedYear == year && parseInt(month,10) < month ))		 
{
	pdisable='disabled';
}
 
		 
	 	obj2 = 	eval("document.frmOPGrowthSetup.controlValue"+ parseInt(month,10));	
	 
		 
	 	if(obj2.value=='D')	
	 
		{ 		 
		  pdisable='disabled';
		}	
		 	
	 
if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			//return false;
	}	
else
	{	
	 	windowOpener("/GmPageControllerServlet?strPgToLoad=gmOverride.do&demandMasterId="+demandsheetmonthid+"&references="+monthwords+'/'+selectedYearChar+"&overrideTypeId="+overrideTypeValue+"&refValue="+refValue+"&currentValue="+selectedMonthGrowth.value+"&refId="+growthId+"&refMonth="+selectedRefMonth.value+"&refType="+refType+"&pdisablevalue="+pdisable,"Override","resizable=yes,scrollbars=yes,top=250,left=300,width=750,height=375");
	}
}


function fnLoad()
{
// fnCheckYear();
// fnChkPrjSt();
	launchCalendar();
 if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();			
	}	
}

function fnLog(id)
{
	windowOpener("/GmCommonLogServlet?hType=1225&hID="+id,"Override Comments","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
}

function fnReloadRef()
{
	// Commented for Growth Screen Enhancement.
	/*obj = document.all.year;
	for (i=0;i<obj.length;i++)
	{
		if (obj.options[i].text == year)
		{
			obj.options[i].selected = true;
			break;
		}
	}*/
	if(validateDate(document.frmOPGrowthSetup.fromDate.value,'From Date')
	  && validateDate(document.frmOPGrowthSetup.toDate.value, 'To Date')){

		var pNum = document.frmOPGrowthSetup.pnum.value;

		/*	if(pNum !='')
			{
				document.frmOPGrowthSetup.pnum.value = '';
				}
		*/		
			if(document.frmOPGrowthSetup.applyAll != null)
		  {	
			 
				document.frmOPGrowthSetup.applyAll.value = '';
				 
			}
			if(fnLoadValidate()){   //Added this condition for BUG-2638.
				fnReload();
			}
	}
	//document.all.year.text = year;
}
function fnApplyAll()
{
 if(document.frmOPGrowthSetup.applyAll != null)
 {
 var applyAllValue = document.frmOPGrowthSetup.applyAll.value;
 
 var selectedApplyAll = document.frmOPGrowthSetup.applyAllMonth.selectedIndex;
   
 for (var i=1;i<13 ;i++ )
	{
		obj = eval("document.frmOPGrowthSetup.month"+i);	
		obj1 = eval("document.frmOPGrowthSetup.refMonth"+i);			
		if(obj.disabled != true)
		 {
		   obj.value = applyAllValue;
		   obj1.options[selectedApplyAll].selected = true;
		 }
	}
}
}
function fnVoid()
{
	document.frmOPGrowthSetup.hTxnId.value =  document.frmOPGrowthSetup.demandSheetId.value;
	document.frmOPGrowthSetup.hCancelType.value = 'VDDSG';
	document.frmOPGrowthSetup.hAction.value = 'Load';
	document.frmOPGrowthSetup.action ="/GmCommonCancelServlet";
	document.frmOPGrowthSetup.submit();
}

function fnChkPrjSt(){
	var selectedRefId = document.frmOPGrowthSetup.refId.value; 
	alert(' In chkPrjSt function'+selectedRefId +' prjSt '+prjSt+'  launchdt '+launchdt);
	if (selectedRefId != 0 )
	{		
		if (prjSt == "NEW" && launchdt == "")
		{
			Error_Details("You need to specify the launch date before defining growth");
		}
	}

}


// JS Functions added for Growth Screen Enhancements.


function fnSubmit(){

	if(validateDate(document.frmOPGrowthSetup.fromDate.value, 'From Date')
	  && validateDate(document.frmOPGrowthSetup.toDate.value, 'To Date')
	  && fnLoadValidate()){
		
		mygrid.editStop();// retun value from editor(if presents) to cell and closes editor

		var gridrows =mygrid.getChangedRows(",");
		//var gridrows =mygrid.getAllRowIds(",");
		if(gridrows.length==0){
			Error_Clear();	
			Error_Details(" No Data saved, as there is no change done to the Growth Data.");
			Error_Show();
			Error_Clear();
		}

		if(gridrows.length>0){
			document.frmOPGrowthSetup.submitbtn.disabled=true;
			var gridrowid;
			var gridrowsarr = gridrows.toString().split(",");
			var columnCount = mygrid.getColumnsNum();

			var finalData='';
			var growthValue;
			var growthType;
			var validationError=0;
			var refType = document.frmOPGrowthSetup.refType.options[document.frmOPGrowthSetup.refType.selectedIndex].value;

			var refTypeExp ;
			var regExp=new RegExp("^\\d+["+refTypeExp+"]?$");

			var void_flag;
			var error_msg='';
			for (var rowid = 0; rowid < gridrowsarr.length; rowid++) {
				gridrowid = gridrowsarr[rowid];
				
				void_flag = mygrid.getRowAttribute(gridrowid,"row_deleted");	
				
				if(void_flag ==  undefined){
					void_flag='';
				}
				var gridrowidval=mygrid.cellById(gridrowid,(0)).getValue();
				finalData = finalData+gridrowidval+'^'+void_flag+'#@#';
				for(var col=0;col<columnCount;col++){
				if(!mygrid.isColumnHidden(col)){
				
					//alert(' Cell value'+mygrid.cellById(gridrowid, col).getValue() +' Column Name'+mygrid.getColumnLabel(col));
				if(mygrid.getColumnLabel(col)== 'Name'){
					
					//Adding trim condition in the growth setup screen while copying from the clipboard.
					//PMT-14418 Author:gpalani Oct 2017
					var Name = TRIM(mygrid.cellById(gridrowid, col).getValue());
					
					if(TRIM(Name).length==0){
						error_msg="There are one or more rows without a name.  Please add a name or delete the row.";			
						validationError++;
						mygrid.setRowColor(gridrowid,"purple");
					}
				}		
				if(mygrid.getColumnLabel(col)!= 'ID'&& mygrid.getColumnLabel(col)!= 'Name'&& mygrid.getColumnLabel(col)!= 'Description'){	
					
					//Adding trim condition in the growth setup screen while copying from the clipboard.
					//PMT-14418 Author:gpalani Oct 2017	

					growthValue = TRIM(mygrid.cellById(gridrowid, col).getValue());
					mygrid.setCellTextStyle(gridrowid,col,"");
					growthType = growthValue.substring(growthValue.length-1);

					if(growthValue.length==0)
					growthValue = '';
					
					if(refType =="20297"){ // For Group-id
						refTypeExp="u|U||a|A";
					}else if(refType =="20296"){ // For Set (cosignment , In-house sheets)
						refTypeExp="a|A|u|U|s|S";
					}else if(refType =="20295"){ // For Part#-Group
						refTypeExp="u|U|a|A";
					}else if(refType =="20298"){ // For Part#-Consignment
						refTypeExp="a|A|u|U";
					}
					
					regExp=new RegExp("^\\d+["+refTypeExp+"]?$");
					// Regular expression will look for one/more digits OR zero/one of A/U/P.
					
					if(growthValue.length>0 && !regExp.test(growthValue)){
						mygrid.setCellTextStyle(gridrowid,col,"color:red;border:2px solid gray;");
					validationError++;
					}
					
					if(growthType.toUpperCase() == "A"){
						growthType='20382';
						growthValue = growthValue.substring(0,growthValue.length-1);
						}
/*					else if(growthType.toUpperCase() == "P"){
						growthType='20380';
						growthValue = growthValue.substring(0,growthValue.length-1);
						}*/
					else if(growthType.toUpperCase() == "U"){
						growthType='20381';
						growthValue = growthValue.substring(0,growthValue.length-1);
						}
					else if(growthType.toUpperCase() == "S"){
						growthType='4000315';
						growthValue = growthValue.substring(0,growthValue.length-1);
						}
					else{
							if(refType =="20297" && growthType != null ){ // For Group-id
								growthType='20381';
							}else if(refType =="20296"){ // For Set
								growthType='20381';
							}else if(refType =="20295"){ // For Part#-Group
								growthType='20381';
							}else if(refType =="20298"){ // For Part#-Consignment
								growthType='20381';
							}
						}
						
						var monthyear = mygrid.getColumnLabel(col).split(" ");
						finalData+=TRIM(monthyear[0])+'^'+monthyear[1]+'^'+growthType+'^'+growthValue+'|';
					}
				}
				
			}
				finalData+="$";
			}
			document.forms[0].inputString.value=finalData;
			document.frmOPGrowthSetup.strOpt.value = 'save';
			mygrid.parentFormOnSubmit();


			if(validationError==0){
				//alert('finalData'+finalData);
				document.forms[0].submit();
			}
			else{
				
				if(error_msg.length==0)
					error_msg=" There is incorrect data entered in the growth section. Please correct the data in the highlighted area(s).";
				
				document.frmOPGrowthSetup.submitbtn.disabled=false;
				Error_Clear();	
				Error_Details(error_msg);
				Error_Show();
				Error_Clear();
				error_msg="";
				validationError=0;
			}
		}
	}
}

function addRow(){
	mygrid.addRow(mygrid.getUID(),'');
	//Below code is not to allowing the user to edit the past month Growth values.
	setColumnType(mygrid);
	var divHeight=document.getElementById('mygrid_container').offsetHeight;
	document.getElementById('mygrid_container').style.height = (divHeight+30)+'px';
	document.getElementById('mygrid_container').style.overflow = 'auto';	
}
//function to set the column type as Read Only.
function setColumnType(mygrid){
	var colLabel ='';
	var colsLength = mygrid.getColumnsNum();
	for (var j=0;j < colsLength; j++ ){
		colLabel = mygrid.getColumnLabel(j);
		var colLabelDt = dateFormat(colLabel);
		if(colLabelDt != ''){
			diff = compareDates( todayDate,colLabelDt);
			if(diff < 0){
				mygrid.setColumnExcellType(j,"ro");  //cell type as read only.
			}
		}
	}
}	
//Function to convert the column name into date. eg.Aug 13 --> 08/01/2013.
function dateFormat(colLabel) {
	var month = colLabel.substring(0,colLabel.indexOf(' ')).toUpperCase();
	var year = colLabel.substring(colLabel.indexOf(' ')+1,colLabel.length);
	var mm='';
	switch (month){
    case "JAN": mm = '01';break;
    case "FEB": mm = '02';break;
    case "MAR": mm = '03';break;
    case "APR": mm = '04';break;
    case "MAY": mm = '05';break;
    case "JUN": mm = '06';break;
    case "JUL": mm = '07';break;
    case "AUG": mm = '08';break;
    case "SEP": mm = '09';break;
    case "OCT": mm = '10';break;
    case "NOV": mm = '11';break;
    case "DEC": mm = '12';break;
    default: return ''; break;
    }
    var date = mm+"/01/20"+year;
	return date;
}

function removeSelectedRow(){

	var gridrows=mygrid.getSelectedRowId();
	if(gridrows!=undefined){
		var NameCellValue ;
		var gridrowsarr = gridrows.toString().split(",");
		var added_row;
		for (var rowid = 0; rowid < gridrowsarr.length; rowid++) {
			gridrowid = gridrowsarr[rowid];
			
			NameCellValue = mygrid.cellById(gridrowid, 0).getValue();
			added_row = mygrid.getRowAttribute(gridrowid,"row_added");
			
			if(TRIM(NameCellValue).length==0 || (added_row!=undefined &&added_row=="Y")){
				mygrid.deleteRow(gridrowid);
			}else{
				mygrid.lockRow(gridrowid,true);
				mygrid.setRowColor(gridrowid,"pink");
				mygrid.setRowAttribute(gridrowid,"row_deleted","Y");
			}
			setRowAsModified(gridrowid,true);
		}
	}else{
		Error_Clear();
		Error_Details(" Please Select a row to Delete.");				
		Error_Show();
		Error_Clear();
	}
}

function enableSelectedRow(){
	var gridrows=mygrid.getSelectedRowId();

	if(gridrows!=undefined){
		
		var gridrowsarr = gridrows.toString().split(",");
		
		for (var rowid = 0; rowid < gridrowsarr.length; rowid++) {
			gridrowid = gridrowsarr[rowid];
			mygrid.lockRow(gridrowid,false);
			mygrid.setRowColor(gridrowid,"");
			mygrid.setRowAttribute(gridrowid,"row_deleted","");
			
			setRowAsModified(gridrowid,true);
		}
	}else{
		Error_Clear();
		Error_Details(" Please Select a row to Undo Delete.");				
		Error_Show();
		Error_Clear();
	}
}

function keyPressed(code, ctrl, shift) {
       if (code == 67 && ctrl) {
        /*if (!mygrid._selectionArea)
            return alert("You need to select a block area in grid first");
            */
        mygrid.setCSVDelimiter("\t");
        mygrid.copyBlockToClipboard()
    }
    
    if (code == 86 && ctrl) {
    	mygrid.setCSVDelimiter("\t");
		mygrid.pasteBlockFromClipboard()
		//mygrid._HideSelection();
    }
	// This code is to block delete the content from the grid.
	if(code==46&&!ctrl&&!shift){
		
		if(mygrid._selectionArea!=null){
			var area=mygrid._selectionArea
			var leftTopCol=area.LeftTopCol;
			var leftTopRow=area.LeftTopRow;
			var rightBottomCol=area.RightBottomCol;
			var rightBottomRow=area.RightBottomRow;
			
			for (var i=leftTopRow; i<=rightBottomRow; i++){
				
				setRowAsModified(mygrid.getRowId(i),true);

				for (var j=leftTopCol; j<=rightBottomCol; j++){
					mygrid.cellByIndex(i,j).setValue("");
				}
			}
			mygrid._HideSelection();
		}
	}
    return true;
}

function setRowAsModified(rowid,modified){
	mygrid.forEachCell(rowid,function(obj){
       obj.cell.wasChanged = modified;
		});
}
function pasteToGrid(){
	
	if(mygrid._selectionArea!=null){
		var colIndex = mygrid._selectionArea.LeftTopCol; 
		if(colIndex!=undefined && colIndex !=0){
			mygrid.pasteBlockFromClipboard();
			//mygrid._HideSelection();
		}else{
			alert(' Please Select a valid value from Part Number Drop Down');
			// If the user is trying to paste values to Part# column,call addRowFromClipboard.
			/*   var cbData = mygrid.fromClipBoard();   		
				var rowCount = cbData.split("\n"); 
				var row_id;		
				for (var i=0; i<rowCount.length; i++){
					var rowData = rowCount[i].split("\t");
				
					for(var j=0;j<rowData.length;j++){
						cb_columnName=rowData[0];
						break;
					}
					row_id=checkBeforeAddRow(cb_columnName);
					
					if(row_id.length>0){
						mygrid.setCSVDelimiter("\t");
						mygrid.setDelimiter("\t");
						//Changing the row id with the newly selected value from name drop down.
					
						mygrid.changeRowId(mygrid.getSelectedRowId(),row_id);
			
						mygrid.setRowAttribute(row_id,"row_added","Y");
						mygrid.setDelimiter(",");
					}
				}*/
		}
	}else{
		alert(' Please BlockSelect Cell(s) to Paste Data.');
	}
}

function addRowFromClipboard(){
	
   var cbData = mygrid.fromClipBoard();   		
	var rowCount = cbData.split("\n"); 
	var ignoredRows ='<b>The Following Part/Group are not added as they are not Mapped to the Sheet. </b><br>';
	var ignoredRowCount = 0;
	
	for (var i=0; i<rowCount.length; i++){
		var rowData = rowCount[i].split("\t");
		
		for(var j=0;j<rowData.length;j++){
			cb_columnName=rowData[0];
			break;
		}
		var dropDownVal =varNameDropDownValues.split("#@#");
		if((varNameDropDownValues.toUpperCase()).indexOf(TRIM(cb_columnName.toUpperCase()))== -1){
			ignoredRows = ignoredRows+cb_columnName+'<br>';
			ignoredRowCount++;
		}

		var db_colName;
		for (var dd=0; dd<dropDownVal.length; dd++){
		
			db_colName = dropDownVal[dd];
			var row_id= TRIM(db_colName.substring(db_colName.indexOf("#")+1,db_colName.length));
			
			db_colName = db_colName.substring(0,db_colName.indexOf("#"));
			
			if(row_id.length>0 && !checkForDuplicateRow(row_id) && TRIM(cb_columnName.toUpperCase()) == TRIM(db_colName.toUpperCase())){
				mygrid.setCSVDelimiter("\t");
				mygrid.setDelimiter("\t");
				mygrid.addRow(row_id,rowCount[i]);
				mygrid.setRowAttribute(row_id,"row_added","Y");
				mygrid.setDelimiter(",");
			}
		}
	}

	if(ignoredRowCount>0){
		Error_Clear();
		Error_Details(ignoredRows);				
		Error_Show();
		Error_Clear();
	}
} 

function checkBeforeAddRow(partName){
	alert('partName'+partName);
	var dropDownVal =varNameDropDownValues.split("#@#");
	var db_colName;
	//dropDownVal.length
	for (var dd=0; dd<dropDownVal.length; dd++){

		db_colName = dropDownVal[dd];
		var row_id= TRIM(db_colName.substring(db_colName.indexOf("#")+1,db_colName.length));
		
		db_colName = db_colName.substring(0,db_colName.indexOf("#"));
		if(row_id.length>0 && !checkForDuplicateRow(row_id) && TRIM(partName) == TRIM(db_colName)){
			alert(row_id);
			return row_id;
		}
	}	
}
	
	function showComments(menuitemId,type){
	
		var refType;
		var url;
		var name="";
		var args="";
		var gridref = mygrid.contextID.split("_"); //rowId_colInd
		
		var growthDetailsID = mygrid.cellById(gridref[0], gridref[1]).getAttribute("growth_det_id");
		
		if(menuitemId == 'comment'){
			refType='1224';
			url = "/GmCommonLogServlet?hType="+refType+"&hID="+growthDetailsID+"&hJNDIConnection="+JNDI_CONNECTION;
			name = "ViewComments";
			args = "resizable=yes,scrollbars=yes,top=250,left=300,width=735,height=600";
		}else if(menuitemId == 'view_override'){
			refType='1002';
			url = "GmPageControllerServlet?strPgToLoad=/gmAuditTrail.do&auditId="+refType+"&txnId="+growthDetailsID+"&hJNDIConnection="+JNDI_CONNECTION;
			name = "ViewOverride";
			args = "resizable=yes,scrollbars=yes,top=250,left=300,width=735,height=600";			
		}
		windowOpener(url,name,args);
	
	}
	
	function checkForDuplicate(stage,rId,cInd,nValue,oValue){
	if(stage==2 && nValue!=oValue){
	
		// This piece of code will be executed only when the modified column is Name drop down.
		// 0 Column Index corresponds to the Name dropdown.
		if(cInd == 0){
			if(checkForDuplicateRow(nValue)){
				Error_Clear();
				Error_Details(" The Group/Part Number already exists in the Demand Sheet. Duplicates are not allowed.");				
				Error_Show();
				Error_Clear();
				return false;
			}
			//Changing the row id with the newly selected value from name drop down.
			mygrid.changeRowId(rId,nValue);
			
			//Adding attribute to a row, so when any row is deleted,based on this flag it will be permanently deleted.
			//Otherwise we will highlight the deleted row with different color.
			mygrid.setRowAttribute(nValue,"row_added","Y");
			
			//Reset the rowcolor which was highlighted in case of row without any value in Name column.
			mygrid.setRowColor(nValue, '');			
		}
	}
	return true;
}

function checkForDuplicateRow(row_id){
	if(mygrid.doesRowExist(row_id)){
		return true;
	}
}

function initializeGrid(){
		menu = new dhtmlXMenuObject();
		menu.setIconsPath("/images/dhtmlxGrid/imgs/");
		menu.renderAsContextMenu();
		menu.attachEvent("onClick",showComments);
		menu.loadXML("growth_grid_comments.xml");
		//mygrid = initGrid('mygrid_container',gridData);
	    //mygrid = initMyGrid('mygrid_container',gridData);
		mygrid = new dhtmlXGridObject('mygrid_container');
		mygrid.setSkin("dhx_skyblue");
		mygrid.enableBlockSelection();
		mygrid.copyBlockToClipboard();	
		mygrid.enableEditEvents(false, true, true);
		mygrid.attachEvent("onKeyPress", keyPressed);
		mygrid.attachEvent("onEditCell", checkForDuplicate);		
		mygrid.setImagePath("/images/dhtmlxGrid/imgs/");
		mygrid.attachEvent("onRowPaste",function(id){
             mygrid.forEachCell(id,function(obj){
                   obj.cell.wasChanged = true;
                   });
			});
		mygrid.enableContextMenu(menu);
		mygrid.enableUndoRedo();
		mygrid.enableMultiselect(true);
		mygrid.setAwaitedRowHeight(200);
		mygrid.init();
		//mygrid.parse(gridData);
		mygrid.enableAlterCss("even","uneven");
		mygrid.setStyle("", "","", "background-color:#eee;color:black;");
		mygrid.loadXMLString(gridData);  
		mygrid.submitOnlyChanged(true);
		mygrid.submitOnlyRowID(false);
		mygrid.enableAutoHeight(true,300,true);
			}

function initMyGrid(divRef,gridData){
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.init();
	gObj.loadXMLString(gridData);
	return gObj;
}

function applyToMarkedCell(){
	var markedArray = mygrid.getMarked();
	
	var refType = document.frmOPGrowthSetup.refType.options[document.frmOPGrowthSetup.refType.selectedIndex].value;
	var growthValue = TRIM(document.forms[0].applyToMarked.value);
	var refTypeExp = '';

	if(refType =="20297"){ // For Group-id
		refTypeExp="u|U|a|A";
	}else if(refType =="20296"){ // For Set  (cosignment , In-house sheets)
		refTypeExp="a|A|u|U|s|S";
	}else if(refType =="20295"){ // For Part#-Group
		refTypeExp="u|U|a|A";
	}else if(refType =="20298"){ // For Part#-Consignment
		refTypeExp="a|A|u|U";
	}
	regExp=new RegExp("^\\d+["+refTypeExp+"]?$");
	
	if(growthValue.length>0){
		if(regExp.test(growthValue)){
			if(markedArray.length==0){
			alert(' Please Mark a cell to Apply value');
			}
			for (var i = 0; i < markedArray.length; i++) {
				var cellMark = markedArray[i].toString().split(",");
				var cellObj = mygrid.cellById(cellMark[0], cellMark[1]);
				
				if(cellObj.cell._cellType != 'ro'){
					cellObj.setValue(growthValue);
				}
				 setRowAsModified(cellMark[0],true);
			}	
			mygrid.unmarkAll();
			mygrid.enableMarkedCells(false);
			document.forms[0].applyToMarked.value="";
		}else {
			alert(' The Data is not applied to Marked Cell(s). \n The Data is not applicable for the selected Demand Sheet Type. ');
		}
	}
}

function enableMarker(){
	mygrid.enableMarkedCells();
}
function disableMarker(){
	mygrid.enableMarkedCells(false);
}

function doundo(){
	mygrid.doUndo();
}
function doredo(){
	mygrid.doRedo();
}

function docopy(){
    mygrid.setCSVDelimiter("\t");
	mygrid.copyBlockToClipboard();
	//mygrid._HideSelection();
}

function validateDate(input, control){

	fnValidateTxtFld('fromDate',"From Date");
	fnValidateTxtFld('toDate',"To Date");

	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}else{
		input = input.toUpperCase();
		regExp=new RegExp("^(JAN|FEB|MAR|MAY|APR|JUL|JUN|AUG|OCT|SEP|NOV|DEC)/\\d{2}$");
		if(input.length>0 && input.search(regExp)<0){
			Error_Clear();
			Error_Details("Incorrect Date Format in <b>"+control+"</b>. Please Enter Date in Mon/YY Format.");				
			Error_Show();
			Error_Clear();
			return false;
		}else{
			return true;
		}
	}
}
var mDCal;
//PMT-2885 - This code commented for DHTMLX new version 3.5. This code will not work in new version 
// and has written new function in bottom. 
/*function launchCalendar(){


mDCal = new dhtmlxDblCalendarObject('dhtmlxDblCalendar', false, {
        isMonthEditable: true,
        isYearEditable: true
    });
    mDCal.setYearsRange(1980, 2020);
    mDCal.setDateFormat("%b/%y");
	mDCal.setOnClickHandler(function(date,obj,type){
	  if(type=="left") document.forms[0].fromDate.value = obj.getFormatedDate("%b/%y",date);
	  else  document.forms[0].toDate.value = obj.getFormatedDate("%b/%y",date);
	});

	mDCal.draw();
    document.getElementById("dhtmlxDblCalendar").style.display="none";
}

function showHideCalendar(fromDate,toDate){
	var obj=document.getElementById("dhtmlxDblCalendar");
	mDCal.setDate(fromDate,toDate);
	mDCal.draw();
	
	if(obj.style.display=="none"){
		obj.style.display="";
	}
	else{ 
		obj.style.display="none";	
	}
	
}*/


function launchCalendar(){
		mDCal = new dhtmlXDoubleCalendar("dhtmlxDblCalendar");
		//Set date format like Ex: Jan/14
		mDCal.setDateFormat("%M/%y");
		mDCal.show();
		document.getElementById("dhtmlxDblCalendar").style.display = "none";
	}

function showHideCalendar(fromDate,toDate){
		var obj = document.getElementById("dhtmlxDblCalendar");
		
		//This line for today date will display in BOLD and this method will be in dhtmlxcalendar.js
		//mDCal.enableTodayHighlight(true); // or false
		
		mDCal.show();
		
		// We are setting date taking from textbox in set in date picker.
		mDCal.setDates(document.getElementById("fromDate").value,document.getElementById("toDate").value);

		//When click any days and will call bellow code. 
		mDCal.attachEvent("onClick", function(side, date){
			if(side=="left")
				document.getElementById("fromDate").value = mDCal.getFormatedDate("%M/%y",date);
			else document.getElementById("toDate").value = mDCal.getFormatedDate("%M/%y",date);
		});
		// If click calendar icon will show, Once selected date again click calendar icon and will close.
		if(obj.style.display == "none"){
			obj.style.display = "block";
			mDCal.show();
		}else if(obj.style.display == "block"){
			obj.style.display = "none";
			mDCal.hide();
		}
}
//For new button Reload Sheet success message
function fnScheduleSheet()
{
	var demandSheetId=document.frmOPGrowthSetup.demandSheetId.value;
	var demandSheetNm=  document.frmOPGrowthSetup.demandSheetId.options[document.frmOPGrowthSetup.demandSheetId.selectedIndex].text;
	dhxWins = new dhtmlXWindows();
    dhxWins.enableAutoViewport(false);
    dhxWins.attachViewportTo(document.body);
    w1 = dhxWins.createWindow("w1", 300, 150, 750, 350);
    w1.attachURL('gmOPGrowthSetup.do?strOpt=reload_sheet&demandSheetId='+demandSheetId+"&demandSheetNm="+demandSheetNm,"SucessMessage","resizable=yes,scrollbars=yes,top=100,left=300,width=1000,height=400");
    w1.setText('Reload Demand Sheet Status');
    w1.setModal(true);
    setTimeout("closepopup();",6000);
}
function closepopup(){
	w1.close();
}



