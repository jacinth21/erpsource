function initGridData(divRef,gridData){
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);
	gObj.enableTooltips("true,true,true,true,true");
	gObj.init();		
	gObj.loadXMLString(gridData);	
	return gObj;	
}

function fnOnPageLoad(){
	gridObj = initGridData('dataGrid',objGridData);
}

function fnLoad(){
	document.frmGlobalMappingReport.strOpt.value = "load";
	fnStartProgress('Y');
	document.frmGlobalMappingReport.submit();
}

function fnExcelExport(){
	gridObj.toExcel('/phpapp/excel/generate.php');
}