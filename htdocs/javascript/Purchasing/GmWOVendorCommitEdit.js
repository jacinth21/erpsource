var workOrdIdIndex = '';
var workOrdQtyIndex = '';
var commitDateIndex = '';
var commitQtyIndex = '';
var pnumIndex = '';
var vendorCommitIdIndex ='';
var hWorkOrderIndex='';

function fnOnPageLoad() {
		gridObj = initGridData('dataGridDiv', objGridData);
		// to set the index values
		workOrdIdIndex = gridObj.getColIndexById("work_ord_id");
		workOrdQtyIndex = gridObj.getColIndexById("work_ord_qty");
		commitDateIndex = gridObj.getColIndexById("commitDate");
		commitQtyIndex = gridObj.getColIndexById("commitQty");
		vendorCommitIdIndex = gridObj.getColIndexById("vendor_commit_id");
		hWorkOrderIndex = gridObj.getColIndexById("hwork_order_id");
}


function initGridData(divRef, gridData) {
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);
	// checking account output obj is undefined, to add select all check box
		gObj.enableTooltips("true,true,true,true,true,true");

	gObj.init();
	gObj.loadXMLString(gridData);
	gObj.enableEditTabOnly(true);
	gObj.enableEditEvents(true, false, true);
	return gObj;
}



function fnSubmit() {
//close opened editor and return value from editor to the cell
gridObj.editStop();
//get list of changed rows included added rows
fnValidateVendorCommit ();

if (ErrorCount > 0) {
	Error_Show();
	Error_Clear();
	return false;
}

var gridChrows = gridObj.getChangedRows(',');
var commitDate = '';
var commitQty = '';
var workOrder = '';
var inputStr = '';

// to handle the Spilt and merge
gridObj.forEachRow(function(rowId) {
	workOrderIdVal = gridObj.cellById(rowId, hWorkOrderIndex).getValue();
	workOrderQtyVal = gridObj.cellById(rowId, workOrdQtyIndex).getValue();
	commitDateVal = gridObj.cellById(rowId, commitDateIndex).getValue();
	commitQtyVal = gridObj.cellById(rowId, commitQtyIndex).getValue();
	vendorCommitVal = gridObj.cellById(rowId, vendorCommitIdIndex).getValue();
	
	if (commitDateVal != '' && commitQtyVal != ''){
		// to form the input string
		inputStr = inputStr + vendorCommitVal + '^' +workOrderIdVal + '^' + commitDateVal + '^' + commitQtyVal + '|';
	}


	
});


	document.frmVendorCommit.inputString.value = inputStr;
	document.frmVendorCommit.vendorId.value = POId;
	fnStartProgress('Y');
	document.frmVendorCommit.action = '/gmVendorCommit.do?method=saveVendorCommit';
	document.frmVendorCommit.submit();

}


function fnValidateVendorCommit (){
	
	// 1. to validate Date field and Qty (only numeric values)
	// 2. validate if any one filed enter and another filed no values (for ex: commit date no values but Qty values)
	// 3. to compare WO qty and Commit Qty
	
	// loop the grid
	var commitDateVal = '';
	var commitQtyVal = '';
	var workOrderQtyVal = '';
	var workOrderIdVal = '';
	
	var noDataFl = true;
	
	// validation var
	var dateError_Str = '';
	var commitQtyError_str = '';
	var qtyNotMatchError_str = '';
	var commitDtError_str='';
	var gridChrows = gridObj.getChangedRows(',');
	var commitDtError_str ='';
	
	if (gridChrows.length != 0) {
		var rowsarr = gridChrows.toString().split(",");
		var toDtDiff= '';
		for (var rowid = 0; rowid < rowsarr.length; rowid++) {
			rowId = rowsarr[rowid];
		workOrderQtyVal = gridObj.cellById(rowId, workOrdQtyIndex).getValue();
		commitDateVal = gridObj.cellById(rowId, commitDateIndex).getValue();
		commitQtyVal = gridObj.cellById(rowId, commitQtyIndex).getValue();
		commitDatefmt = gridObj.cellById(rowId, commitDateIndex);
		workOrdId = gridObj.cellById(rowId, hWorkOrderIndex).getValue();
		// validate any data entered 
		if (commitDateVal != '' && commitQtyVal !='' && noDataFl){
			noDataFl = false;
		}
				
		if(commitDateVal != ''){
			CommonDateValidation(commitDatefmt,format,'Please Enter Valid Date Format');
		}
		
		var toDtDiff = dateDiff(today_dt,commitDateVal,format);

		if (parseInt(toDtDiff) < 1 || toDtDiff == '0'){
			if(commitDtError_str.indexOf(workOrdId) == -1){
			commitDtError_str += workOrdId +', ';
			}
		}
		// validate qty field
		if(isNaN(commitQtyVal)){
			commitQtyError_str = commitQtyVal;
		}
				
		}	
		
	}
	// PMT_30146: to remove the validation (Qty compare) 
	/*
	
	//to validate commit qty and Wo qty is not equal
	gridObj.forEachRow(function(rowId) {
		workOrdId = gridObj.cellById(rowId, workOrdIdIndex).getValue();
		workOrderQtyVal = gridObj.cellById(rowId, workOrdQtyIndex).getValue();

		// to validate the commit Qty
		if (workOrdId != ''){
			tmpCommitQty = 0;
			
			gridObj.forEachRow(function(rowId) {	
				hWorkOrdId = gridObj.cellById(rowId, hWorkOrderIndex).getValue();
				commitQtyVal = gridObj.cellById(rowId, commitQtyIndex).getValue();
				commitDateVal = gridObj.cellById(rowId, commitDateIndex).getValue();
				
				if (workOrdId == hWorkOrdId && commitQtyVal != ''){
					
					
					tmpCommitQty += Number(commitQtyVal);
				}
				
			}); 
			if (workOrderQtyVal != tmpCommitQty && tmpCommitQty != 0){
				qtyNotMatchError_str += workOrdId +', ';
			}
		}
	});
	
	 */
	
    if (commitQtyError_str != '') {
		
		Error_Details('Enter only positive values to enter in Commit Qty ');
	}
    commitDtError_str = commitDtError_str.substr(0,commitDtError_str.length-2);
    if (commitDtError_str != '') {
		Error_Details('Commit Date should be Future Date for '+commitDtError_str);
	}
    
   /* 
    qtyNotMatchError_str = qtyNotMatchError_str.substr(0,qtyNotMatchError_str.length-2);
	if(qtyNotMatchError_str != ''){
		Error_Details('Sum of the Commit Qty should be equal to the WO Qty for '+ qtyNotMatchError_str);
	}
	
	*/
	
	if(noDataFl){
		Error_Details('Please enter the valid data to submit ');
	}
	
}


function fnQtyHistory (commitQty){
	windowOpener("/GmPageControllerServlet?strPgToLoad=/gmAuditTrail.do&auditId=308&txnId="+ commitQty, "","resizable=yes,scrollbars=yes,top=250,left=300,width=785,height=300,status=1");
}

function fnDtHistory (commitDt){
	windowOpener("/GmPageControllerServlet?strPgToLoad=/gmAuditTrail.do&auditId=307&txnId="+ commitDt, "","resizable=yes,scrollbars=yes,top=250,left=300,width=785,height=300,status=1");
}


//Description : This function used to add a row(grid)
function fnSplitRow(id) {
	// new row id
	var uid = gridObj.getUID();

	// get the current row index id
	var current_row_index = gridObj.getRowIndex(id);
	current_row_index = current_row_index + 1;
	
	workOrderQtyVal = gridObj.cellById(id, hWorkOrderIndex).getValue();

	gridObj.addRow(uid, '', current_row_index);
    gridObj.cellById(uid, hWorkOrderIndex).setValue(workOrderQtyVal);
	gridObj.setCellTextStyle(uid, commitDateIndex, "background-color:white;  border-width: 1px;border-style: inset;"); // Commit Date
	gridObj.setCellTextStyle(uid, commitQtyIndex, "background-color:white;  border-width: 1px;border-style: inset;"); // Commit Qty

}