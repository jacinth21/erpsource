//while click on GO button to fetch the results
function fnGo()
 {
  //document.frmVendorInfoReport.activeFl.value = document.frmVendorInfoReport.activeFl.checked;   
  document.frmVendorInfoReport.go.disabled = true;		
  document.body.style.cursor = 'wait';
  document.frmVendorInfoReport.strOpt.value = "Load";
  fnStartProgress('Y');
  document.frmVendorInfoReport.submit();
 }

//This Function used to show the grid values while loading the page
function load() {
	if(objGridData.value !=''){ 
		gridObj = initGridData('dataGridDiv', objGridData);
	}
}

//This Function used to Initiate the grid
function initGridData(divRef,gridData)
{
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/extweb/dhtmlx/dhtmlxGrid/imgs/");
	gObj.attachHeader('#select_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter');
	gObj.enableMultiline(false);	
	gObj.init();
	gObj.loadXMLString(gridData);
	return gObj;
}

//This function used to download the grid data
function fnDownloadXLS() {
	gridObj.toExcel('/phpapp/excel/generate.php');

}