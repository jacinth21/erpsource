var gridObj;
var rowID = '';
var check_rowId = '';
var status_Id='';
var inputString='';
var statusVal='';
var load_date_Id='';
var load_date_Val='';
var ttp_detail_id='';
var ttp_detail_str='';
var ttp_name_Id = '';
var response = '';
var sheetstatus_Id='';
var sheetstatusVal='';
var openvendorcnt_Id ='';
var openvendorVal='';
var ttp_row_id='';
var ttp_name_row_id='';
var gridttpdtlId = '';
var ttp_dtl_row_id = '';
//Load button using Ajax call
function fnLoad(){
	
	//Intialize
	var ttpId = document.frmTTPViewByVendor.ttpId.value;   
	var status = document.frmTTPViewByVendor.status.value;   
	var ttpMonth = document.frmTTPViewByVendor.ttpMonth.value;   
	var ttpYear = document.frmTTPViewByVendor.ttpYear.value; 
	var searchttpId = document.frmTTPViewByVendor.searchttpId.value;   
    // Using Ajax call for load the report
	fnValidateDropDn('ttpYear', 'Year ');
	fnValidateDropDn('ttpMonth', 'Month');
	//fnDateValidation(ttpMonth,ttpYear);
	if(ErrorCount > 0){
			Error_Show();
			Error_Clear();
			return false;
	}	
	fnStartProgress();
	response = '';
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmTTPbyVendorDashboard.do?method=fetchTTPVendorByDashboard&ttpId='
			+ ttpId+'&status='+status+'&ttpMonth='+ttpMonth+'&ttpYear='+ttpYear+'&searchttpId='+searchttpId + '&ramdomId=' + Math.random());
	var loader = dhtmlxAjax.getSync(ajaxUrl);
	fnTTPByVendorCallback(loader);  
}

function fnTTPByVendorCallback(loader){
	response = loader.xmlDoc.responseText;
	fnStopProgress();
	if(response !=''){
		var setInitWidths = "50,*,100,75,75,90,120,125,10,10,10,10";
		var setColAlign = "center,left,left,right,right,right,center,center,left,left,left,left";
		var setColTypes = "ch,ttpnmFl,ro,countFl,ron,ron,ro,ro,ro,ro,ro,ro";
		var setColSorting = "na,str,str,int,int,int,str,str,str";
		var setHeader = ["Select All", "TTP Name","Status","Open Vendor", "Order Qty", "Order Amt","Locked by","Locked Date","","","",""];
		var setFilter = ["<input type='checkbox' value='no'  name='selectAll' onClick='javascript:fnChangedFilter(this);'/>", "#text_filter", "#select_filter","#numeric_filter","#numeric_filter", "#numeric_filter","#select_filter","#select_filter"];
		var setColIds = "chk_box,ttpname,statusnm,openvendor,orderqty,orderamt,lockedby,lockeddate,ttpid,ttpdetailid,statusid,sheetstatus";
		var enableTooltips = "true,true,true,true,,true,true,true,true";
		
		// footer set
		var footerArry=new Array();
		footerArry[0]="";
		footerArry[1]="";
		footerArry[2]="";
		footerArry[3]="<b>Total</b>";
		footerArry[4]="<b>{#stat_total}<b>";
		footerArry[5]="<b>{#stat_total}<b>";
		footerArry[6]="";	
		footerArry[7]="";	
		footerArry[8]="";	
		footerArry[9]="";	
		footerArry[10]="";	
		footerArry[11]="";	
		footerArry[12]="";	
		
		var gridHeight = "";
		var footerStyles = [];
		var footerExportFL = true;
		var pagination = "";
		var	format = "Y"
	
		document.getElementById("DivNothingMessage").style.display = 'none';
		document.getElementById("dataGridDiv").style.display = "block";
		document.getElementById("DivExportExcel").style.display = "table-row";
		document.getElementById("DivButton").style.display = "table-row";
		// split the functions to avoid multiple parameters passing in single
		// function
		gridObj = initGridObject('dataGridDiv');
		gridObj = fnCustomTypes(gridObj,setHeader, setColIds, setInitWidths, setColAlign, setColTypes,setColSorting, enableTooltips, setFilter,400,pagination,footerArry,footerStyles,format);
		gridObj = loadDhtmlxGridByJsonData(gridObj, response);
		//
		gridObj.attachEvent("onCheckbox", doOnCheck);
		check_rowId = gridObj.getColIndexById("chk_box");
		status_Id = gridObj.getColIndexById("statusid");
		sheetstatus_Id = gridObj.getColIndexById("sheetstatus");
		openvendorcnt_Id = gridObj.getColIndexById("openvendor");
		ttp_row_id = gridObj.getColIndexById("ttpid");
		ttp_dtl_row_id = gridObj.getColIndexById("ttpdetailid");
		ttp_name_row_id = gridObj.getColIndexById("ttpname");
		//
		 gridObj.forEachRow(function(rowId) {
			 statusVal = gridObj.cellById(rowId, status_Id).getValue();
			 sheetstatusVal = gridObj.cellById(rowId, sheetstatus_Id).getValue();
			 openvendorVal = gridObj.cellById(rowId, openvendorcnt_Id).getValue();
			 console.log("statusVal"+statusVal);
			 // 26240574 - TTP sheet status Locked//ttp_vendor_status = 108864
			 if((statusVal!="108865") && (sheetstatusVal=="26240574") && (openvendorVal=="0")&& (statusVal !="108863")){
			   gridObj.cellById(rowId, check_rowId).setDisabled(false);
			 }else{
				 gridObj.cellById(rowId, check_rowId).setDisabled(true); 
			 }
			 });
		gridObj.attachEvent("onRowSelect",doOnRowSelect);
		gridObj.attachEvent("onRowSelect",doOnCountSelect);
		 // based on the date selected to enable/disable the transactions button.
		 fnEnableDisableBtn();
		gridObj.setColumnHidden(8, true);
		gridObj.setColumnHidden(9, true);
		gridObj.setColumnHidden(10, true);
		gridObj.setColumnHidden(11, true);
	}else
	{
		document.getElementById("dataGridDiv").style.display = 'none';
		document.getElementById("DivNothingMessage").style.display = 'block';
		document.getElementById("DivExportExcel").style.display = 'none';
		document.getElementById("DivButton").style.display = 'none';
	}
}

//This Function Used to set custom types conditions in Grid
function fnCustomTypes(gObj,setHeader, setColIds, setInitWidths, setColAlign, setColTypes,setColSorting, enableTooltips,setFilter,gridHeight,pagination,footerArry,footerStyles,format){

if (setHeader != undefined && setHeader.length > 0) {
 gObj.setHeader(setHeader);
 var colCount = setHeader.length;
 }    

if (setInitWidths != "")
gObj.setInitWidths(setInitWidths);

if (setColAlign != "")
gObj.setColAlign(setColAlign);

if (setColTypes != "")
gObj.setColTypes(setColTypes);

if (setColSorting != "")
gObj.setColSorting(setColSorting);

if (enableTooltips != "")
gObj.enableTooltips(enableTooltips);

if (setFilter != "")
gObj.attachHeader(setFilter);

if (footerArry != undefined && footerArry.length > 0) {
var footstr = eval(footerArry).join(",");
if (footerStyles != undefined && footerStyles.length > 0)
gObj.attachFooter(footstr, footerStyles);
else
gObj.attachFooter(footstr);
}  

if(setColIds != ""){
 gObj.setColumnIds(setColIds);
}

if (gridHeight != "")
gObj.enableAutoHeight(true, gridHeight, true);
else
gObj.enableAutoHeight(true, 400, true);

if(pagination == "Y"){
    gObj.enablePaging(true,100,10,"pagingArea",true);
    gObj.setPagingSkin("bricks");
}	
if (format != ""){
gObj.setNumberFormat("000,000",4);
gObj.setNumberFormat("000,000.00",5);
}
return gObj;
}


//Function is used to Check Box Check
function doOnCheck(rowId, cellInd, stage) {
	rowID = rowId;
	if (cellInd == check_rowId) {
		fnSelectAllToggle('selectAll');
	}
	return true;
}


//Description : This function to check the select all check box
function fnSelectAllToggle(varControl) {
	var objControl = eval("document.frmTTPViewByVendor." + varControl);

	var all_row = gridObj.getAllRowIds(",");
	var all_rowLen = all_row.length; // get the rows id length
	var checked_row = gridObj.getCheckedRows(check_rowId);
	var finalval;

	finalval = eval(checked_row.length);
	if (all_rowLen == finalval) {
		objControl.checked = true;// select all check box to be checked
	} else {
		objControl.checked = false;// select all check box un checked.
	}
}


//this function used to select all option (check box)
function fnChangedFilter(obj) {
	
	if (obj.checked) {
		gridObj.forEachRow(function(rowId) {
			var check_disable_fl = gridObj.cellById(rowId, check_rowId).isDisabled();
			if (!check_disable_fl) {
				gridObj.cellById(rowId, check_rowId).setChecked(true);
			}
		});
	} else {
		gridObj.forEachRow(function(rowId) {
			check_id = gridObj.cellById(rowId, check_rowId).getValue();
			
			if (check_id == 1) {
				gridObj.cellById(rowId, check_rowId).setChecked(false);
			}

		});
	}
}

//This function used to press the enter key then submit the page.
function fnEnter() {
	if (event.keyCode == 13) {	
		fnLoad();
	 }		
}

//For Past Month disable the approve button
function fnEnableDisableBtn (){
	var ttpMonth = document.frmTTPViewByVendor.ttpMonth.value;   
	var ttpYear = document.frmTTPViewByVendor.ttpYear.value; 
	var date=new Date();
	
	if ((date.getMonth()+1!=ttpMonth)||(date.getFullYear()!=ttpYear)){	
		if(document.frmTTPViewByVendor.LBL_LOCK_AND_GENERATE != undefined && lockAccFl =='Y')
			document.frmTTPViewByVendor.LBL_LOCK_AND_GENERATE.disabled = true;
     }else {   	 
    	 if(document.frmTTPViewByVendor.LBL_LOCK_AND_GENERATE != undefined && lockAccFl =='Y')
    		 document.frmTTPViewByVendor.LBL_LOCK_AND_GENERATE.disabled = false;
    	 
	}
}

//This Function Used to download Excel Sheet
function fnExport() {
	gridObj.setColumnHidden(0,true);
	gridObj.toExcel('/phpapp/excel/generate.php');
	gridObj.setColumnHidden(0,false);
}

//This function used to create user defined column types in Grid
//This Function for hyperlink ttp name
function eXcell_ttpnmFl(cell) {
	// the eXcell name is defined here
	if (cell) { // the default pattern, just copy it
		this.cell = cell;
		this.grid = this.cell.parentNode.grid;
	}
	
	this.setValue = function(val) {
		this.setCValue("<a href=\"#\">"+val+"",val);		
	}

	this.getValue = function() {
		return this.cell.childNodes[0].innerHTML; // gets the value
	}
}
eXcell_ttpnmFl.prototype = new eXcell;


//This function used to create user defined column types in Grid
//This Function for hyperlink ttp name
function eXcell_countFl(cell) {
	// the eXcell name is defined here
	if (cell) { // the default pattern, just copy it
		this.cell = cell;
		this.grid = this.cell.parentNode.grid;
	}
	
	this.setValue = function(val) {
		this.setCValue("<a href=\"#\">"+val+"",val);		
	}

	this.getValue = function() {
		return this.cell.childNodes[0].innerHTML; // gets the value
	}
}
eXcell_countFl.prototype = new eXcell;

//This function used to view the summary detail report when click ttp name hyperlink
function doOnRowSelect(rowId, cellInd, stage){
	if(cellInd == 1){
		var ttpId= gridObj.cellById(rowId, 8).getValue();
		var ttpName= gridObj.cellById(rowId, 1).getValue();
		fnTTPbySummaryDetailReport(ttpId,ttpName);
	}
}
//This function used to view the summary detail report
function fnTTPbySummaryDetailReport(ttpId,ttpName){
	var ttpMonth = document.frmTTPViewByVendor.ttpMonth.value;   
	var ttpYear = document.frmTTPViewByVendor.ttpYear.value; 
	var vendorId= "";
	windowOpener("/gmTTPByVendorSummary.do?method=loadTTPbyVendorSummary&ttpId="+ttpId+"&ttpMonth="+ttpMonth+"&ttpYear="+ttpYear+"&vendorId="+vendorId+"&ttpName="+ttpName,"Search","resizable=yes,scrollbars=yes,top=150,left=150,width=1550,height=700");
}

//This function used to view the vendor report when click Approval count link 
function doOnCountSelect(rowId, cellInd, stage){
	if(cellInd == 3){
		var ttpId= gridObj.cellById(rowId, 8).getValue();
		var ttpName= gridObj.cellById(rowId, 1).getValue();
		fnTTPbyVendorReport(ttpId,ttpName);
	}
	//This function used to view the vendor report.	
function fnTTPbyVendorReport(ttpId,ttpName){
		var ttpMonth = document.frmTTPViewByVendor.ttpMonth.value;   
		var ttpYear = document.frmTTPViewByVendor.ttpYear.value; 
		var vendorId= "";
		windowOpener("/gmTTPbyVendorReport.do?method=loadTTPbyVendorSummary&ttpId="+ttpId+"&monthId="+ttpMonth+"&yearId="+ttpYear+"&vendorId="+vendorId+"&ttpName="+ttpName,"Search","resizable=yes,scrollbars=yes,top=150,left=150,width=1400,height=600");
}	
	
}

//This function used to lock the TTP by vendor Details 
function fnTTPbyVendorLock(){
	var ttpDtlIdInputStr ='';
	var ttpIdInputStr='';
	var ttpnameList ='';
	//get list of changed rows included added rows
	 checked_row = gridObj.getCheckedRows(check_rowId);
	
	 var ttpMonth = document.frmTTPViewByVendor.ttpMonth.value;   
	 var ttpYear = document.frmTTPViewByVendor.ttpYear.value; 
	if (checked_row == '') {
		Error_Details(message_operations[845]);
	}
	if (checked_row != '') {
		 chkVal = checked_row.split(",");
	for (var i = 0; i < chkVal.length; i++) {
		checkVal = gridObj.cellById(chkVal[i], check_rowId).getValue();
		 gridttpId= gridObj.cellById(chkVal[i], ttp_row_id).getValue();
		 gridttpdtlId= gridObj.cellById(chkVal[i], ttp_dtl_row_id).getValue();
		 ttpStatusId= gridObj.cellById(chkVal[i], status_Id).getValue();
		 ttpName= gridObj.cellById(chkVal[i], ttp_name_row_id).getValue();
		if (checkVal == 1) {
			if(ttpStatusId != '108865'){
				ttpIdInputStr =ttpIdInputStr+  gridttpId +',';
				//form ttp detail id string for Purchase order id
				ttpDtlIdInputStr = ttpDtlIdInputStr+gridttpdtlId+',';
		    }else{
			   ttpnameList += ttpName +'<br>';
		    }	
	    }
	}
}
if(ttpnameList !=''){
	Error_Details(message_operations[846] +"<br> <b>" + ttpnameList + "</b>");
  }

	if(ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
  }	
	fnStartProgress();
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmTTPbyVendorDashboard.do?method=lockTTPbyVendor&ttpId='+ttpIdInputStr+'&strTTPDetailIds='+ttpDtlIdInputStr+'&status='+ttpStatusId+
			'&lockPeriod='+ttpMonth+'/'+ttpYear+'&ramdomId=' + Math.random());
	var loader = dhtmlxAjax.getSync(ajaxUrl);
	fnLockCallBack(loader);
}
//This function  used to call back method of TTP vendor Lock 
function fnLockCallBack(loader){
	var ttpNames='';
	 response = loader.xmlDoc.responseText;
	  fnStopProgress();
	  if(response == ''){
		  var ttpId = document.frmTTPViewByVendor.ttpId.value;
		  var searchttpId = document.frmTTPViewByVendor.searchttpId.value; 
		  var ttpMonth = document.frmTTPViewByVendor.ttpMonth.value;   
		  var ttpYear = document.frmTTPViewByVendor.ttpYear.value; 
		  var status = document.frmTTPViewByVendor.status.value;  
		  var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmTTPbyVendorDashboard.do?method=fetchTTPVendorByDashboard&ttpId='
					+ ttpId+'&status='+status+'&ttpMonth='+ttpMonth+'&ttpYear='+ttpYear+'&searchttpId='+searchttpId + '&ramdomId=' + Math.random());
			var loader = dhtmlxAjax.getSync(ajaxUrl);
			fnTTPByVendorCallback(loader);  
	  }else{
		  response = response.split(",");
	    for (var j=0;j< response.length;j++ ){	
		  gridObj.forEachRow(function(rowId) {
				checkVal = gridObj.cellById(rowId, check_rowId).getValue();
				 gridttpId= gridObj.cellById(rowId, ttp_row_id).getValue();
				 ttpStatusId= gridObj.cellById(rowId, status_Id).getValue();
				 ttpName= gridObj.cellById(rowId, ttp_name_row_id).getValue();
				 if(gridttpId == response[j]){
					 ttpNames += ttpName + '<br>';
		
				 }
		  });
		  
	 }
	if(ttpNames !=''){
		Error_Details(message_operations[846] + "<br> <b>" + ttpNames + "</b>");
	}
		 
	  }
	  if(ErrorCount > 0){
			Error_Show();
			Error_Clear();
			return false;
	}	
}