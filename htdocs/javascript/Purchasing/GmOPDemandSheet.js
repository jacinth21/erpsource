function fnApprove(form)
{
    var appcomments =  form.appcomments.value;
	var demandSheetMonthId = form.demandSheetMonthId.value;
	// to get the ttp Id
	var parentForm = document.getElementsByName('frmOPTTPMonthlySummary')[0];
	var ttpIdVal = parentForm.ttpId.value;
	
	loadajaxpage('/gmOPDSSummary.do?strOpt=approve&appcomments='+appcomments+'&demandSheetMonthId='+demandSheetMonthId+'&ttpId='+ttpIdVal,'ajaxdivtabarea');
    fnApproveReload();
}
function fnOverrideAverage(strAvg, sheetId, MasterID, refId, refType){
	//changes for PC-2339 Weighted Avg Override, open windowopener for stelkast parts
	windowOpener("/GmPageControllerServlet?strPgToLoad=/gmDemandAvgOverride.do&strOpt=load&demandSheetId="+sheetId+"&demandMasterId="+MasterID+"&systemWeigthedAvg="+strAvg+"&refID="+encodeURIComponent(refId)+"&refType="+refType,"Override","resizable=yes,scrollbars=yes,top=250,left=300,width=785,height=400");
}
function fnApproveReload()
{
	var parts = '';
	var groups ='';
	var overridefl ='';
	var demandSheetMonthId = document.all.demandSheetMonthId.value;
   	var yearId = document.all.hYearId.value;
   	var demandMasterID = document.all.hDemandMasterID.value;
   	var monthId = document.all.hMonthID.value;
   	
	loadajaxpage('/gmOPDSSummary.do?demandSheetMonthId='+demandSheetMonthId+'&strOpt=report&yearId='+yearId+'&partNumbers='+encodeURIComponent(parts)+'&checkedGroupsString='+groups+'&demandSheetId='+demandMasterID+'&monthId='+monthId+'&overrideFlag='+overridefl,'ajaxreportarea');
}

function fnCallTPRReport (pnum)
{
	var invid = document.all.inventoryId.value;
    
    windowOpener("/gmOPTPRReport.do?method=fetchTPRReport&pnum="+encodeURIComponent(pnum)+"&inventoryId="+invid,"INVPOP","resizable=yes,scrollbars=yes,top=40,left=20,width=900,height=400");

	//windowOpener("/gmTPRReport.do?method=fetchTPRReport&pnum="+pnum+"&monYear="+monYear+"&inventoryId="+inventoryId,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=900,height=400");			

}

function fnFetch(demandSheetId,demandMasterId,yearId,monthId,arrySize)
{
	var unitprice = '';
	var overridefl = 'N';
	var setDtlfl = '';
	var descfl = '';
	var groups = '';
	var parts = document.all.partNumbers.value ;
	var chkset = (document.all.checkSetDtl)?document.all.checkSetDtl.checked:false;
	var chkDesc = document.all.checkDesc.checked;
	var forecastmonths = document.all.forecastMonths.value ;
	if(arrySize != 0){
		groups = getCheckedItems();
		document.all.checkedGroupId.value ;
	}	
	overrideflstatus = ''//document.all.overrideFlag.status ;
	unitpriceobj = document.all.unitrprice;
	
	if (overrideflstatus)
	{
		overridefl = 'Y';
	}
	if (chkset)
	{
		setDtlfl = 'Y';
	}
	if (chkDesc)
	{
		descfl = 'Y';
	}
	for(var n=0;n<unitpriceobj.length;n++)
	{
		if (unitpriceobj[n].checked)
		{
				unitprice = unitpriceobj[n].value;
				break;
		}
	}
	fnStartProgress('Y');
	var url='/gmOPDSSummary.do?demandSheetMonthId='+demandSheetId+'&strOpt=report&yearId='+yearId+'&partNumbers='+encodeURIComponent(parts)+'&checkedGroupsString='+groups+'&demandSheetId='+demandMasterId+'&monthId='+monthId+'&unitrprice='+unitprice+'&forecastMonths='+forecastmonths+'&overrideFlag='+overridefl+'&checkSetDtl='+setDtlfl+'&checkDesc='+descfl;
	dhtmlxAjax.get(url+'&ramdomId='+Math.random(),function(loader){
		if (loader.xmlDoc.responseText != null)
		{
			var data = loader.xmlDoc.responseText;
			document.getElementById("ajaxreportarea").innerHTML = data;
			fnStopProgress();
		}
		});
}	

function getCheckedItems()
{
	var objProject = document.all.checkedGroupId;
	var setlen = objProject.length;
	var setstr = '';
	
	var temp = '';
	var items='';
		
	for (var i=0;i<setlen;i++)
	{
		if (objProject[i].checked == true)
		{
			setstr = setstr + objProject[i].value + ',';
		}
		items = setstr.substr(0,setstr.length-1);
	}
	
	return items;
}
   
function fnCallAD(id, type) {					   
 windowOpener("/GmSetReportServlet?hAction=Drilldown&hOpt=SETRPT&hSetId="+id,"PrintInv","resizable=yes,scrollbars=yes,top=250,left=300,width=900,height=400");			
}

function fnCallAD(id, type, name) {					   
windowOpener("/GmSetReportServlet?hAction=Drilldown&hOpt=SETRPT&hSetId="+id+"&hSetNm="+name,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=900,height=400");			
}

 function fnCallSetNumber(val)
{
	alert(val);
}

function fnCallGroupMap(val, type)
{
	windowOpener("/gmOPGroupPartMap.do","PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=900,height=400");
}

function fnCallDisp(id, type)
{	
    var temp = new Array();
    temp = id.split('^');
	var partNum = temp[0];  
	windowOpener("/GmPartNumServlet?hAction=LoadPartDetail&partNumber="+encodeURIComponent(partNum),"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=760,height=600");
}

function fnSubComponentDrillDown(id)
{
	var forecastmonths = document.all.forecastMonths.value ;
	var dsheetID = document.all.demandSheetMonthId.value;
	var yearId = document.all.hYearId.value; 
   	var monthId = document.all.hMonthID.value;
	var temp = new Array();
	 	    temp = id.split('^');
		    var partNum = temp[0]; 
			var refid =  temp[1];
  // alert("/gmOPDSSummary.do?strOpt=allpartdrilldown&demandSheetMonthId="+dsheetID+"&partNumbers="+partNum+"&yearId="+yearId+"&monthId="+monthId+"&refId="+refid+"&forecastMonths="+forecastmonths);
 	windowOpener("/gmOPDSSummary.do?strOpt=allpartdrilldown&demandSheetMonthId="+dsheetID+"&partNumbers="+encodeURIComponent(partNum)+"&yearId="+yearId+"&monthId="+monthId+"&refId="+refid+"&forecastMonths="+forecastmonths,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=900,height=400");
}

function fnCallGroupMap(val, type)
{
	windowOpener("/gmOPGroupPartMap.do?strOpt=40045&groupId="+val+"&haction=edit","PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=900,height=400");	
}		

function fnCallEditPAR(id, dsid, dmid)
		{
	  	var temp = new Array();
	 	    temp = id.split('^');
		    var partNum = temp[0];      
		windowOpener("/gmOPDSParSetup.do?partNumbers="+encodeURIComponent(partNum)+"&demandMasterId="+dmid+"&demandSheetId="+dsid+"&strOpt=edit&popType=pop","PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=925,height=300");			
		}

function fnCallConsD(id, type, dsheetID)
		{
		
			var formattedid = id.replace('^','-');
			windowOpener("/GmCommonLogServlet?hType=1228&hID="+dsheetID+ formattedid ,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
		}

function fnViewGrowthComments(partnumber, sheetid , masterid, monthyear )
{
	var hHideComment = '';
	var strArray = partnumber.split('^');	
	if(strArray[0] != ''){
		partnumber = strArray[0];
	}else{
		partnumber = strArray[1];
	}
	windowOpener("/gmOPDSSummary.do?strOpt=viewGrowthComments&demandSheetId="+sheetid+"&demandSheetMonthId="+masterid+"&monthYear="+monthyear+"&partNumbers="+encodeURIComponent(partnumber),"Comments","resizable=yes,scrollbars=yes,top=250,left=300,width=920,height=350");
}

function fnColumnDrillDown(row,col,val)
{
	var b = row;
	var temp = new Array();
	temp = b.split('^');
	var sheetid = document.all.demandSheetId.value;
	var month = document.all.monthId.value;
	var monthid = document.all.demandSheetMonthId.value;
	
	var typeid = document.all.demandTypeId.value;

	var year = col;
	var overrideTypeValue = 20396;
	var currentValue = val;
	var refType = 0;
	
	if(typeid == 40020)
	 {
	  refType = 20295;
	 }
	 else
	 {
	  refType = 20296;
	 }
//	 alert("/GmPageControllerServlet?strPgToLoad=gmOverride.do&references="+year+"&overrideTypeId="+overrideTypeValue+"&refValue="+temp[0]+"&currentValue="+currentValue+"&refId="+sheetid+"&refIdForLogReason="+monthid+"&demandSheetMonthId="+monthid+"&refType="+refType+"&partType="+temp[1]);
   	windowOpener("/GmPageControllerServlet?strPgToLoad=gmOverride.do&references="+year+"&overrideTypeId="+overrideTypeValue+"&refValue="+temp[0]+"&currentValue="+currentValue+"&refId="+sheetid+"&refIdForLogReason="+monthid+"&demandSheetMonthId="+monthid+"&refType="+refType+"&partType="+temp[1],"Override","resizable=yes,scrollbars=yes,top=250,left=300,width=750,height=375");
  
}

function fnCallMnGrowth(row,col,val)
{
	
	var monthid = document.all.demandSheetMonthId.value;
	var year = col;	
	var typeid = document.all.demandTypeId.value;
	var sheetid = document.all.hDemandMasterID.value;
	var month = document.all.hMonthID.value;
	
	var overrideTypeValue = '';
	if (typeid == 40020)
	{
		overrideTypeValue = 20398;
	}else{
		overrideTypeValue = 20399;
	}
	var currentValue = val;
	var refType = 0;
	
	if(typeid == 40020)
	 {
	  refType = 20295;
	 }
	 else
	 {
	  refType = 20296;
	  }
	if(TRIM(currentValue) == '-')
    {
    	Error_Details("<b>Growth has not been entered for this set, please enter growth for the set in the Group Part Growth Screen</b>");
    }
    if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
	}else{
		windowOpener("/GmPageControllerServlet?strPgToLoad=gmOverride.do&references="+year+"&overrideTypeId="+overrideTypeValue+"&refValue="+row+"&currentValue="+currentValue+"&refId="+row+"&demandSheetMonthId="+monthid+"&refType="+refType+"&demandMasterId="+sheetid,"Override","resizable=yes,scrollbars=yes,top=250,left=300,width=750,height=375");
	}
}

function fnFetchGrowthInfo()
{   
	var parts = '';
	var groups ='';
	var overridefl ='';
	var demandSheetMonthId = document.all.demandSheetMonthId.value;
   	var yearId = document.all.hYearId.value;
   	var demandMasterID = document.all.hDemandMasterID.value;
   	var monthId = document.all.hMonthID.value;
   	
	loadajaxpage('/gmOPDSSummary.do?demandSheetMonthId='+demandSheetMonthId+'&strOpt=report&yearId='+yearId+'&partNumbers='+encodeURIComponent(parts)+'&checkedGroupsString='+groups+'&demandSheetId='+demandMasterID+'&monthId='+monthId+'&overrideFlag='+overridefl,'ajaxreportarea');
	
	var demandtab=new ddajaxtabs("demandtab", "ajaxdivtabarea");				
	demandtab.setpersist(false);								
	demandtab.init();
	demandtab.expandit('growthinfo');					   				    	 
}

function fnCallActuals(id, type, fcmonths)
 {
	var b = id;
    var temp = new Array();
    temp = b.split('^');
	var dsheetID = document.all.demandSheetMonthId.value;// <bean:write name="frmDemandSheetSummary" property="demandSheetMonthId"/>;
	var access = document.all.hAccessType.value;
	var levelID = document.all.hLevelID.value;
	var partNum = temp[0];
	var groupId = temp[1];	  
	var groupInfo = groupId;
	
 	if (TRIM(partNum)!='' && groupId.indexOf(".") == -1 && access!='102623' )
	{
		groupInfo = '-9999'; 		
	}
	 
	windowOpener("/gmOPDSSummary.do?strOpt=partdrilldown&demandSheetMonthId="+dsheetID+"&partNumbers="+encodeURIComponent(partNum)+"&groupInfo="+groupInfo+"&groupID="+groupId+"&accessType="+access+"&levelID="+levelID+"&forecastMonths="+fcmonths,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=1100,height=600");
 }

function fnMediateReqEdit(requestId, requestFor, setId,demandTypeId){
	if (setId == '')
	{
		fnReqItemEdit(requestId);
	}
	else {
		fnReqEdit(requestId, requestFor,demandTypeId);
	}
}

function fnReqItemEdit(requestId){
			windowOpener("/gmRequestDetail.do?requestId="+requestId ,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=900,height=600");			
}

function fnReqEdit(requestId, requestFor,DemandTypeId)
{
	
		var addtnlParam='';
		if(DemandTypeId == '50632') // Open Request
		{
		  addtnlParam="&initiatedFrom=OP&demandSheetMonthID="+document.all.demandSheetMonthId.value; // This is to denote the request is called from Order Planning.And this will be set only for "New Request".
		}
		windowOpener("/gmRequestEdit.do?FORWARD=gmIncShipDetails&RE_FORWARD=gmRequestHeader&RE_RE_FORWARD=gmConsignSetServlet&hAction=EditLoad&hMode=CON&source=50184&screenType=Consign&FORMNAME=frmRequestHeader&hRequestView=headerView&requestId="+requestId+"&requestFor="+ requestFor +"&hRequestId="+requestId+addtnlParam,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=780,height=600,status=1");			
	 	   
}

function fnRequestDetails(strReqId,strConId)
    {
		windowOpener("/gmRequestMaster.do?strOpt=print&requestId="+strReqId+"&consignID="+strConId,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=920,height=600");
    }
    

function fnConsignmentVer(strConId)
	{
		windowOpener("/GmConsignSetServlet?hAction=PrintVersion&hId="+strConId,"Con","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
	}

function fnCallPPP(id, pstatus, dsid, dmid)
		{
		var temp = new Array();
		    temp = id.split('^');
		    var partNum = temp[0];
		    var setID = temp[1];
		windowOpener("/gmOPTPRReport.do?method=fetchTPRReport&pnum="+encodeURIComponent(partNum)+"&pstatus="+pstatus+"&demandSheetMonthId="+dsid+"&demandSheetId="+dmid+"&setId="+setID+"&popType=pop","PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=925,height=300");		
		}

function fnCallMultiSheet(id, dmid)
		{
		var temp = new Array();
		    temp = id.split('^');
		    var partNum = temp[0];		
		windowOpener("/gmOPOprMultiSheetDetails.do?partDetails="+encodeURIComponent(partNum)+"&demandSheetMonthId="+dmid+"&forecastMonths=4&strOpt=multisheetdetails","PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=925,height=300");		
		}

function fnCallMultiSheetTTP(id)
		{
		var dmid = '';
		var temp = new Array();
		    temp = id.split('^');
		    var partNum = temp[0];		
		windowOpener("/gmOPOprMultiSheetDetails.do?partDetails="+encodeURIComponent(partNum)+"&demandSheetMonthId="+dmid+"&forecastMonths=4&strOpt=multisheetdetails&yearId="+yearIDVal+"&monthId="+monthIDVal+"&inventoryId="+invIDVal,"PrntInv","resizable=yes,scrollbars=yes,top=50,left=300,width=1000,height=900");		
		}

function fnPullSheetsSelectAll(TotalRows)
{
	 for (var i=0;i<TotalRows ;i++ )
		{	
			if(document.all.Chk_selectAll.checked){
				document.all.multiSheet[i].checked=true;
			}else{
				document.all.multiSheet[i].checked=false;
			}	 
		}
}

function fnPullOpenSheets(form)
{
	objMultiSheetArr = form.multiSheet;
	objMultiSheetArrLen = objMultiSheetArr.length;
	dmidString = "";	
	var sheetSelected = false;
	var demandSheetMonthId = document.all.demandSheetMonthId.value;
	if(objMultiSheetArr.id != undefined)
	{		
		var multisheetname = '';
		if(eval('document.all.hmultiSheetname0')){
			multisheetname = eval('document.all.hmultiSheetname0').value;
		}
		if(objMultiSheetArr.checked)
			{
				 dmidString = objMultiSheetArr.id + ","+ multisheetname + "|";				
				 sheetSelected = true;	
			}	
	}
	else
	{
		for(i = 0; i < objMultiSheetArrLen; i ++) 
		{	
			var multisheetname = '';
			if(eval('document.all.hmultiSheetname'+i)){
				multisheetname = eval('document.all.hmultiSheetname'+i).value;
			}
			if(objMultiSheetArr[i].checked)
			{
			  dmidString = dmidString  + objMultiSheetArr[i].id + ","+ multisheetname + "|";			 
			  sheetSelected = true;
			}				 
		}
    }

var truncateAt = 1000;
	var delimiter = '|';
	var data = '';
	var demandidarray = convertStringToArray(dmidString,truncateAt,delimiter);

    if(!sheetSelected)
	{
		 //alert("Please select atleast one sheet to Pull");
		 Error_Details("Please select atleast one sheet to Pull");
		 Error_Show();
		 Error_Clear();
		 return false;
	}
	if(confirm('This action will schedule the Multipart Sheet to be pulled. Email notification will be sent out upon successful data pull. Do you want to proceed?')){
		form.pull.disabled = true;
		for (var rowid = 0; rowid < demandidarray.length; rowid++) {
				dmidString = demandidarray[rowid];
		loadajaxpage('/gmOPOprPullOpenSheets.do?strOpt=pullopensheets&demandSheetString='+dmidString+"&demandSheetMonthId="+demandSheetMonthId,'ajaxdivtabarea');
		}
		//fnReloadOpenSheets();   //as per the design i'm commenting
	}else{
		return false;
	}
}


function convertStringToArray(strInput, truncateAt , delimiter){
		
		var strTemp = "";
		var arr = [];
		if(truncateAt==0){
			outList.add(strInput);
			return outList;
		}		
		
		if(strInput!=null && strInput.length>truncateAt){
			
			while(strInput.length>truncateAt){
				if(strInput.indexOf(delimiter)!=-1){
				
					strTemp = strInput.substring(0,truncateAt);
					strInput = strInput.substring((truncateAt));
					strInput = strTemp.substring(strTemp.lastIndexOf(delimiter)+1)+strInput;
					strTemp = strTemp.substring(0,strTemp.lastIndexOf(delimiter)+1);
				//	alert(strTemp);
					arr.push(strTemp);					
				}
			}
			if(strInput!=''){// add remaining input string
				arr.push(strInput);
			} 			
		}
		else {
			arr.push(strInput);
		}	
		return arr;
	}


function fnReloadOpenSheets()
{
	var parts = '';
	var groups ='';
	var overridefl ='';
	var demandSheetMonthId = document.all.demandSheetMonthId.value;
   	var yearId = document.all.hYearId.value;
   	var demandMasterID = document.all.hDemandMasterID.value;
   	var monthId = document.all.hMonthID.value;
	var unitprice = '';
	var overridefl = 'N';

	loadajaxpage('/gmOPDSSummary.do?demandSheetMonthId='+demandSheetMonthId+'&strOpt=report&yearId='+yearId+'&partNumbers='+encodeURIComponent(parts)+'&checkedGroupsString='+groups+'&demandSheetId='+demandMasterID+'&monthId='+monthId+'&unitrprice='+unitprice+'&forecastMonths=4&overrideFlag='+overridefl,'ajaxreportarea');		
}

function fnOpenOrdLogInv(id)
{
	windowOpener("/GmCommonLogServlet?hType=1228&hID="+id,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
}

function fnOpenReqLog(jndi_connect,id)
{
	windowOpener("/GmCommonLogServlet?hType=1239&hID="+id+"&hJNDIConnection="+jndi_connect,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
}


function fndemdisp(id,val,name)
{
	//windowOpener("/gmRequestMaster.do?strOpt=print&setID="+id+"&setID="+val+"&NAME="+name,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=920,height=600");
}


function replaceAll(OldString,FindString,ReplaceString) 
{
	var SearchIndex = 0;
  	var NewString = ""; 
  	while (OldString.indexOf(FindString,SearchIndex) != -1)    
	{
    	NewString += OldString.substring(SearchIndex,OldString.indexOf(FindString,SearchIndex));
    	NewString += ReplaceString;
    	SearchIndex = (OldString.indexOf(FindString,SearchIndex) + FindString.length);         
	}
  	NewString += OldString.substring(SearchIndex,OldString.length);
  	return NewString;
}

function fnRequestGrowthDrillDown(firstDayOfMonth,lastDayOfMonth,refId,refType, dm_mas_id, popup_drillown){ 

	if (popup_drillown =='N')
	{	
		dm_mas_id = '0';
	}	

	if (refType == 20296)
	{
		windowOpener("/gmRequestMaster.do?teamId=0&requiredDate="+firstDayOfMonth+"&requiredDateTo="+lastDayOfMonth+"&setID="+refId+"&requestStatus=0&strOpt=reload&requestStatusoper=0&requestFor=0&requestSource=0&requestTo=0&requestTxnID="+dm_mas_id,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=50,width=900,height=600");
	}
	else{
		windowOpener("/gmRequestMaster.do?teamId=0&requiredDate="+firstDayOfMonth+"&requiredDateTo="+lastDayOfMonth+"&partNum="+encodeURIComponent(refId)+"&setOrPart=50637&requestStatus=0&strOpt=reload&requestStatusoper=0&requestFor=0&requestSource=0&requestTo=0&requestTxnID="+dm_mas_id,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=50,width=900,height=600");
	}

}
function fnCallPPQtyDetails(pnum, ppqty)
{
	var invid = document.frmTTPMonthlySummary.inventoryId.value;
	//alert("pnum: "+ pnum+ "--ppqty:" +ppqty )
    windowOpener("/gmOPPPQtyDrillDownAction.do?partNumber="+encodeURIComponent(pnum)+"&parentPartQty="+ppqty+"&strOpt=PPQDrillDown"+"&inventoryLockId="+invid,"PPQTYPOP","resizable=yes,scrollbars=yes,top=40,left=20,width=900,height=400");
}
function fnGrowthDrillDown(dmdshtid,drilldown,setid, month){ 
	windowOpener("/gmOPDSGrowthRequestDetail.do?strOpt=growthDrillDown&demandSheetMonthId="+dmdshtid+"&growthDrillDown="+drilldown+"&setID="+setid+"&growthDrillDownMonth="+month,"GrowthDrillDown","resizable=yes,scrollbars=yes,top=40,left=20,width=865,height=400");
}