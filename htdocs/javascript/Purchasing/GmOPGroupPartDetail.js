function fnLoad()
{

	document.frmOPGroupPartMapping.haction.value = 'loadParts';
	var enableSubComp='';
	if(parent.document.frmOPGroupPartMapping.enableSubComponent.checked)
	{
		enableSubComp = 'on';	
	}
	document.frmOPGroupPartMapping.action="/gmGroupPartMap.do?strPartListFlag=Y&groupId="+groupid+'&enableSubComponent='+enableSubComp;
	fnStartProgress('Y');
	document.frmOPGroupPartMapping.submit();
}

function fnAllPartHistory() 
{ 	
	windowOpener("/gmGroupPartHistory.do?groupId="+groupid,"","resizable=yes,scrollbars=yes,top=250,left=300,width=1050,height=400");
} 

 

function fnCheckPartRows()
{
	  
	 return rowsize;
}

function fnCheckPartSelect()
{
	 
	objCheckPartArr = document.frmOPGroupPartMapping.checkPartNumbers;
	 
	 return objCheckPartArr;
}

function fnCheckPrimaryPartSelect()
{ 
	var primaryPartChecks = '';
 	if(rowsize>1)
 	{
		 objCheckPartArr = document.frmOPGroupPartMapping.checkPartNumbers;
		 
		for (var i=0; i < rowsize; i++) 
					{ 

						objPrimary = eval("document.frmOPGroupPartMapping.Chk_BO"+i);
						objPrimaryLock = eval("document.frmOPGroupPartMapping.Chk_PL"+i);						
					 	 
					 	if( objCheckPartArr[i].checked == true)
					 	{
						    primaryPartChecks = primaryPartChecks + fnPrimarySecondaryParts(objPrimary,objPrimaryLock);
					 	}
					} 
	}	
	else {
					objPrimary = eval("document.frmOPGroupPartMapping.Chk_BO0");
					objPrimaryLock = eval("document.frmOPGroupPartMapping.Chk_PL0");	
		    		if( document.frmOPGroupPartMapping.checkPartNumbers.checked == true)
				 	{	 		    			
					    primaryPartChecks = primaryPartChecks + fnPrimarySecondaryParts(objPrimary,objPrimaryLock);						
		    		}		
			}	 
 	return  primaryPartChecks;
}

 function fnPrimarySecondaryParts(objPS,objPL)
 {
 	var psParts = '';
	var plValue ='N';	
	if(objPL.value != ''){
		if(objPL.checked == true){
			plValue='Y';
		}
	 }

 	if(objPS.value != '')	//if it's shared part or not
	{ 
	 	if(objPS.checked == true  )
	   		{ 
	   			psParts = psParts + groupid + '^'+ objPS.value + '^'+ '52080'+'^'+ plValue +'|'; 
   			}
	   		else{
	   		 	psParts = psParts + groupid + '^'+ objPS.value + '^'+ '52081'+'^' + plValue +'|'; 
	   		}					    		 
	}
	return psParts;
 }


 function fnCheckSelections()
{	 
	if(rowsize ==1)
	{	
		document.frmOPGroupPartMapping.checkPartNumbers.checked = true;
		}
		objCheckPartArr = document.frmOPGroupPartMapping.checkPartNumbers;
		 
		 	if (objCheckPartArr) {
			objCheckPartArrLen = objCheckPartArr.length;
			objSelAll = document.frmOPGroupPartMapping.selectAll;
			var varselcount = 0;
		
			for(i = 0; i < objCheckPartArrLen; i ++) 
				{	
					if (!objCheckPartArr[i].checked)
					{
						objSelAll.checked = false;
						break	
					}
					else {
						objSelAll.checked = true;
						varselcount ++;
					}
				}
		
			if (varselcount == 0)
			{
				for(i = 0; i < objCheckPartArrLen; i ++) 
				{	
					objCheckPartArr[i].checked = true;
				}
				objSelAll.checked = true;
			}		
}	
	 	

}
 
function fnSelectAll()
{
				objSelAll = document.frmOPGroupPartMapping.selectAll;
				if (objSelAll) {
				objCheckPartArr = document.frmOPGroupPartMapping.checkPartNumbers;
				objCheckPartArrLen = objCheckPartArr.length;
				
				if (objSelAll.checked)
				{
					for(i = 0; i < objCheckPartArrLen; i ++) 
					{	
						objCheckPartArr[i].checked = true;
					}
				}
				else {
					for(i = 0; i < objCheckPartArrLen; i ++) 
					{	
						objCheckPartArr[i].checked = false;
					}
				}
				}
	}
	
 

function fnViewPartGrp(pnum)
{
	document.frmOPGroupPartMapping.hpnum.value = pnum;
	windowOpener("/gmMultiPartGroup.do?hpnum="+encodeURIComponent(pnum),"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=800,height=220");
}


	

function fnPartHistory(pnum)
{ 
	windowOpener("/gmGroupPartHistory.do?hpnum="+encodeURIComponent(pnum),"","resizable=yes,scrollbars=yes,top=250,left=300,width=1050,height=400");
} 


function fnChangeFocus(frmElement)
 {
    
	if(event.keyCode == 13)
	 {	 
		 
			  if( frmElement == 'submit')
		    	{
		      		fnLoad();
		    	}
		    	 
			 		
	 }
 }


function fnVoidValidateCheck()
{ 
	 
 	if(rowsize>1)
 	{ 
		for (var i=0; i < rowsize; i++) 
					{ 
						objPrimary = eval("document.frmOPGroupPartMapping.Chk_BO"+i);
						objPriceType = eval("document.frmOPGroupPartMapping.Chk_PartPriceType"+i);						

					 	if(objPrimary!=undefined && objPrimary.value != '')	//if it's shared part or not
						{ 

							if(objPrimary.checked == true || objPriceType.value == '52080'){

									Error_Details("A primary part cannot be voided without marking it as primary in another group"); 
									break;

							}
						}

					} 
	}	
	else {
			objPrimary = eval("document.frmOPGroupPartMapping.Chk_BO0");
			objPriceType = eval("document.frmOPGroupPartMapping.Chk_PartPriceType0");						
		    		if(objPrimary!=undefined && objPrimary.value != '')	//if it's shared part or not
						{ 
 							   if(objPrimary.checked == true || objPriceType.value == '52080')
								{ 
									 Error_Details("A primary part cannot be voided without marking it as primary in another group");									
							 
								}
																					 
						}	
			}	  
										
		if (ErrorCount > 0)
				 {
					Error_Show();
					Error_Clear();
					return false;
				}
				else {
					return true;
				}	  
}

function fnClose(){
	   CloseDhtmlxWindow();
	   return false;
}
