//This function will call the press the enter button.
function fnEnter(){
	if (event.keyCode == 13){ 
		fnLoad();
	}
}

function fnTemplateFilterSheet()
{
	var templateId = document.frmPartOTNSetup.templateId.value;
	
	if(templateId != "0")
	{ 
		document.frmPartOTNSetup.action="/gmPartOTNSetupAction.do";	
	}
	document.frmPartOTNSetup.demandMasterId.value = '0';
	document.frmPartOTNSetup.submit();
}

function fnTypeFilterSheet()
{
	var sheetTypeId = document.frmPartOTNSetup.sheetTypeId.value;
	//
	if(sheetTypeId != "0")
	{ 
		document.frmPartOTNSetup.action="/gmPartOTNSetupAction.do";	
	}
	document.frmPartOTNSetup.demandMasterId.value = '0';
	document.frmPartOTNSetup.submit();
}

function fnRegionFilterSheet()
{
	var regionId = document.frmPartOTNSetup.regionId.value;
	//
	if(regionId != "0")
	{ 
		document.frmPartOTNSetup.action="/gmPartOTNSetupAction.do";	
	}
	document.frmPartOTNSetup.demandMasterId.value = '0';
	document.frmPartOTNSetup.submit();
}

function fnReload(){
		fnLoad();
}

function fnLoad()
{	
	var dmid = document.frmPartOTNSetup.demandMasterId.value;
	var pnum   = document.frmPartOTNSetup.partNumbers.value;
	
	fnValidateDropDn('demandMasterId',' Demand Sheet Name ');
	/*
	if(dmid == "0" && pnum == ""){
		Error_Details("Please choose a value from the <b>Demand Sheet Name</b> dropdown or enter a valid part # in <b>Part Number</b> textbox to proceed.");
	}
	*/
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	fnStartProgress('Y');
	document.frmPartOTNSetup.strOpt.value = 'edit';	
	document.frmPartOTNSetup.submit();	
}

function fnOTNValidate(val)
{
	var otnval = TRIM(val);
	otnval = otnval.replace(" ","");
	var objRegExp = /(^-?\d*$)/;
	if(otnval < 0 || !objRegExp.test(otnval))
	{
		document.frmPartOTNSetup.Btn_Submit.disabled = true;
		Error_Details('Please Enter a Valid Data for <b>One Time Need</b>' );
	}	
	else
	{	
		if(SwitchUserFl == 'Y'){
			document.frmPartOTNSetup.Btn_Submit.disabled = true;
		}else{
			document.frmPartOTNSetup.Btn_Submit.disabled = false;
		}
	}	
	if (ErrorCount > 0)  
	{
			Error_Show();
			Error_Clear();
			return false;
	}
}
 
function fnSubmit()
{
	fnValidateDropDn('demandMasterId',' Demand Sheet Name ');
	var varDMId =  document.frmPartOTNSetup.demandMasterId.value;
	var varPnum =  document.frmPartOTNSetup.partNumbers.value;
	var inputString = ''; 
	var invalidOTN = "";
	var otnval = "";
	var oldotnval = "";
	var chg_fl = "N";
	if(varDMId!=null || varPnum != null)
	{
		for (var i =0; i < varRows; i++) 
		{
			obj = eval("document.frmPartOTNSetup.otnvalue"+i);
			otnval = TRIM(obj.value);
			otnval = otnval.replace(" ","");
			obj = eval("document.frmPartOTNSetup.pnum"+i);
			varPnum = obj.value;
			obj = eval("document.frmPartOTNSetup.hOTNVal"+i);
			oldotnval = TRIM(obj.value);
			if(!isNaN(otnval)){ 
				if(otnval < 0){
					invalidOTN = invalidOTN + "<br>" + varPnum;
				}else{
			 		if (oldotnval != otnval)
			 		{
						inputString = inputString + varPnum + '^'+ varDMId +'^'+ otnval +  '|';
			 		}
				}
			}else{
				invalidOTN = invalidOTN + "<br>" + varPnum ;
			}
			
			if (oldotnval != otnval)
			{
				chg_fl = "Y";
			}
		}
	}
	if(chg_fl == "N"){
		Error_Details("Please Enter value for atleast one part number.");
	}
	
	if(invalidOTN != ""){
		Error_Details("Please Enter a Valid Data for <b>One Time Need</b> for <b>Part Numbers: "+invalidOTN+"</b>");
	}
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	fnStartProgress('Y');
	document.frmPartOTNSetup.strOpt.value =  'save'; //'save';
	document.frmPartOTNSetup.hinputStr.value = inputString;
	document.frmPartOTNSetup.Btn_Submit.disabled = true;
	document.frmPartOTNSetup.submit();	
	
	
}

 		
function fnLog(type,id)
{
	var varDSId = document.frmPartOTNSetup.demandMasterId.value;
	if (varDSId != '0'){
		windowOpener("/GmCommonLogServlet?hType="+type+"&hID="+id+"-"+varDSId+"&hJNDIConnection="+JNDI_CONNECTION,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
	}
	else{
		Error_Details("Please choose a valid option for the field: <b>Demand Sheet Name</b>");
		Error_Show();
		Error_Clear();
		return false;
	}
	
}
 

function fnOTNHistory(auditid, txnid)
{
	var varDSId = document.frmPartOTNSetup.demandMasterId.value;
	if (varDSId != '0'){
		windowOpener("/gmAuditTrail.do?auditId="+ auditid +"&txnId="+ txnid+"&hJNDIConnection="+JNDI_CONNECTION,"","resizable=yes,scrollbars=yes,top=250,left=300,width=785,height=300,status=1");
	}
	else{
		Error_Details("Please choose a valid option for the field: <b>Demand Sheet Name</b>");
		Error_Show();
		Error_Clear();
		return false;
	}
		
}
