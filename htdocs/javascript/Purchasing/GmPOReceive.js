var vprodclass='';
var errCnt=0;
var errVendorMsg = '';
function fnCallProject(val)
{
	document.frmVendor.hAction.value = 'Part';
  	document.frmVendor.submit();
}


function fnCalDate(val)
{
	var year = '';
	var month = '';
	var NormalYr = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
	var LeapYr = new Array(31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
	var cal = 0;
	var rem = 0;
	var cal1 = 0;
	var date = '';
	var num = val.substring(3,6);
	var days = '';
	// variable is added for avoiding the date format conflecting.
	// previously we are using format, but by splitting the part, it is converting the date from MM/dd/yyyy to mm/dd/yyyy.
	// while submitting the PO, getting date formation error.
	var MfgDateFmt = '';
	MfgDateFmt = format.toLowerCase();//Converting to lowercase
	var searchMe;
	
	var opera1 = MfgDateFmt.split('/');// To get the separator from the format
	var opera2 = MfgDateFmt.split('-');// To get the separator from the format
	var opera3 = MfgDateFmt.split('.');// To get the separator from the format
	
	var lopera1 = opera1.length; //length will be >1 if the separator is '/'
	var lopera2 = opera2.length; //length will be >1 if the separator is '-'
	var lopera3 = opera3.length; //length will be >1 if the separator is '.'
	
	if (lopera1>1){  
		searchMe = '/'; 
	}else if (lopera2>1){  
		searchMe = '-';   
	}else if (lopera3>1){
		searchMe = '.';
	} 
	
	var firstIndex = MfgDateFmt.indexOf(searchMe);// To get the first index of the separator
	var firstIndexfmt = new String(MfgDateFmt.substring(0,firstIndex));// To get whether 'mm' or 'dd' is coming first in the format

	//Hardcoded till Year 2008
	if (val.charAt(2)=='D') // For 2003
	{
		year = '2003';
		days = NormalYr;
	}
	else if (val.charAt(2)=='E') // For 2004
	{
		year = '2004';
		days = LeapYr;
	}
	else if (val.charAt(2)=='F') // For 2005
	{
		year = '2005';
		days = NormalYr;
	}
	else if (val.charAt(2)=='G') // For 2006
	{
		year = '2006';
		days = NormalYr;
	}
	else if (val.charAt(2)=='H') // For 2007
	{
		year = '2007';
		days = NormalYr;
	}
	else if (val.charAt(2)=='J') // For 2008
	{
		year = '2008';
		days = LeapYr;
	}
	else if (val.charAt(2)=='K') // For 2009
	{
		year = '2009';
		days = NormalYr;
	}
	else if (val.charAt(2)=='L') // For 2010
	{
		year = '2010';
		days = NormalYr;
	}
	for (var i=0;i<days.length;i++ )
	{
		cal = cal + days[i];
		if (cal>=num)
		{
			cal1=cal-days[i];
			rem = num-cal1;
			if (rem<10)
			{
				rem = "0"+rem;
			}
			month = i+1;
			if (month<10)
			{
				month="0"+month;
			}
			//date = month+"/"+rem+"/"+year;
			if(firstIndexfmt == 'mm'){// converting to date based on the session format
				date = month+searchMe+rem+searchMe+year;
			}else{
				date = rem+searchMe+month+searchMe+year;
			}
			
			break;
		}
	}
	return date;
}



function fnLoadPO()
{
	fnValidateTxtFld('Txt_PO',' PO Number');
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	var strPOId = "";
	if(document.frmVendor.Txt_PO != undefined && document.frmVendor.Txt_PO != null){
	strPOId = TRIM(document.frmVendor.Txt_PO.value.toUpperCase());
	}
	document.frmVendor.Txt_PO.value = strPOId;
	document.frmVendor.hAction.value = 'LoadPO';
	fnStartProgress('Y');
	document.frmVendor.submit();
}


function fnOpenLog(id)
{
	windowOpener("/GmCommonLogServlet?hType=1232&hID="+id,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
}


function isAlpha(elm) {
    if (elm == "") {
        return false;
    }
    for (var i = 0; i < elm.length; i++) {
        if ((elm.charAt(i) < "a" || elm.charAt(i) > "z") &&
           (elm.charAt(i) < "A" || elm.charAt(i) > "Z")) {
            return false; 
        }
    }
    return true;
}



function fnValidateControl(vPnum,lot,skipFl,vCntNumFmt)
{
	var minLen = 0;
	if (vCntNumFmt == 'XXXXXX%' || vCntNumFmt == 'XXXXXX') {
    	minLen = 6;
    }
	errCnt =0;
	LotCode = document.frmVendor.hLotCode.value;
	var PoType = document.frmVendor.hPoType.value;
	lot = TRIM(lot.toUpperCase());
	len = lot.length;
	var SkipLot=document.frmVendor.hSkipLot.value;
	var regexp = /\s/g;
	var SkipVendorLot = document.frmVendor.hSkipVendorLot.value;  //added for PMT-31248
	if(SkipLot!="YES" && lot!=''){

				if(lot == "NOC#"){
					if(PoType == '3101' && vprodclass == '4031' ){
						return true;
					}else{
						errCnt++;						
					}
					//The Control Number NOC# entered for the part "+vPnum+" is not valid
                }else if(regexp.test(lot)){
                	errCnt++;
                	//Control Number cannot have an entered space.
				}else if ( PoType == '3103' ||  PoType == '3105' || skipFl == "Y")
				{
					return true;
				}
				else if ( len == 8 && vCntNumFmt == '')
				{
					var lcode = lot.substring(0,2);
					if (lcode != LotCode && PoType != '3101')
					{
						//Incorrect Vendor Code
						errVendorMsg=errVendorMsg+vPnum+":"+lot+"<br>";
					}
			
					var yr = lot.substring(2,3);
					if (!isAlpha(yr))
					{
						//Incorrect Year Format
						errCnt++;
					}
			
					var date = parseInt(lot.substring(3,6));
					
					if(fnJulianLotValidation(date,julianRule) && (SkipVendorLot!='Y' && SkipVendorLot!=undefined)){     // SkipVendorLot added for PMT-31248
						errCnt++;
					}
			
					var size = lot.substring(6,7);
					if (!isAlpha(size) && size != '#')
					{
						//Incorrect Batch Format
						errCnt++;
					}
			
					var rev = lot.substring(7,8);
					if (!isAlpha(rev) && rev != '#')
					{
						//Incorrect Rev Number Format
						errCnt++;
					}
				}else if(len != 8 && vCntNumFmt == '')
				{
					//Control Number length entered for the part "+vPnum+" should be equal to 8 characters
					errCnt++;
				}
				else if ( len >= minLen && len <= 40&& vCntNumFmt != ''){
					return true;
				}
				else{
					//Control Number length entered for the part "+vPnum+" should  be at least 6 characters and maximum 40 characters
					errCnt++;
				}
				if (errCnt == 0)
				{
					return true;
				}else{
					Error_Details("Incorrect Control Number Format for "+vPnum +" : "+lot);
				}
	}else{
			return true;
	}

}

//Two new parameters need to add for PART UDI NUMBER and SHOE UDI TEXT BOX value
//Using these two values we are splitting part number with out any alignment issues.
function fnSplit(val,Wo,pnum,skipfl,cnumFmt,dupCnum,sterileFl,partUDI,udiTextBox,rowColor,prodType,diNum,penQty,strSerialNumFl)//add pending qty as hidden for unable to submit when split(pmt-35878 bug NO:BUG-11642)
{
	cnt++;
	var UDcnt = 0;
	var hcnt = parseInt(document.frmVendor.hTextboxCnt.value);
	var NewQtyHtml = "";
	var NewCnumHtml = "";
	var NewExpDtHtml = "";
	var NewUDIHtml = "";
	var NewUDILabelHtml = "";
	hcnt = hcnt+cnt;	
 
	if(udiTextBox == 'N'){ UDcnt = 1;}else{UDcnt = 2;}
	// Check Product Class (Sterile/Non-Sterile)
		if (sterileFl != '4030'){
			if(udiTextBox == 'N'){	
			// Expiry Date Disabled (text box and calendar icon used for consistency on the screen)
				
				if(partUDI != ''){
					NewQtyHtml = "<span id='Sp"+cnt+"'><BR>&nbsp;&nbsp;&nbsp;<input type='hidden' name='hDINum"+hcnt+"' value='"+diNum+"'>" +
								"<input type='hidden' name='hUDINum"+hcnt+"' value='"+partUDI+"'><input type='hidden' name='hprodType"+hcnt+"' value='"+prodType+"'><" +
								"input type='hidden' name='Lbl_UDI"+hcnt+"' value='"+partUDI+"'><input type='hidden' name='hWO"+hcnt+"' value='"+Wo+"'>" +
								"<input type='hidden' name='hPNum"+hcnt+"' value='"+pnum+"'>" +
								"<input type='hidden' name='hSkipFl"+hcnt+"' value='"+skipfl+"'>" +
								"<input type='hidden' name='hCnumFmt"+hcnt+"' value='"+cnumFmt+"'>" +
								"<input type='hidden' name='hDupCnum"+hcnt+"' value='"+dupCnum+"'>" +
								"<input type='hidden' name='hProductClass"+hcnt+"' value='"+sterileFl+"'>" +
								"<input type='hidden' name='hQty"+hcnt+"' value='"+penQty+"'>" +                //add pending qty as hidden for unable to submit when split(pmt-35878 bug NO:BUG-11642)
								"<input type='hidden' name='hSerialNumNeedFl"+hcnt+"' value='"+strSerialNumFl+"'>" + //add strSerialNumFl as hidden for unable to submit when split(pmt-35878 bug NO:BUG-11642)
								"<input type='text' size='3' value='' name='Txt_QtyRec"+hcnt+"' class='InputArea' onFocus=changeBgColor(this,'#EEEEEE'); onBlur=changeBgColor(this,'"+rowColor+"'); tabindex=4>&nbsp;&nbsp;";
					NewQtyHtml += "<input type='text' size='8' value='' name='Txt_CNum"+hcnt+"' style=\"text-transform:uppercase;\" class='InputArea' onFocus=changeBgColor(this,'#EEEEEE'); onBlur=changeBgColor(this,'"+rowColor+"'),fnAddUDIDetails("+hcnt+"); tabindex=4>&nbsp;&nbsp;&nbsp;";
					NewQtyHtml += "<input type='hidden' value="+val+" name='hPnumRow"+hcnt+"'>";
					NewQtyHtml += "<input type='text' size='8' value='' name='Txt_DoNum"+hcnt+"' class='InputArea' onFocus=changeBgColor(this,'#EEEEEE'); onBlur=changeBgColor(this,'"+rowColor+"'),fnAddUDIDetails("+hcnt+"); tabindex=4>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
					NewQtyHtml += "<input type=text name='Txt_ExpDate"+hcnt+"' id='Txt_ExpDate"+hcnt+"' size='9' value='' class='' tabindex='' onBlur=changeBgColor(this,'"+rowColor+"'),fnAddUDIDetails("+hcnt+");  onFocus=\"javascript:changeBgColor(this,'#EEEEEE');\" maxlength='10' disabled='true' tabindex=4 align='absmiddle'>&nbsp;<img src=/images/nav_calendar.gif border=0 align='absmiddle' height=18 width=19></img>&nbsp;&nbsp;&nbsp;";
					NewQtyHtml += "<input type=text name='Txt_Manufacturedt"+hcnt+"' id='Txt_Manufacturedt"+hcnt+"' size='9' value='' class='InputArea' tabindex=4 onBlur=changeBgColor(this,'"+rowColor+"'),fnAddUDIDetails("+hcnt+");  onFocus=\"javascript:changeBgColor(this,'#EEEEEE');\" maxlength='10'>&nbsp;<img id='Img_Date' style='cursor: hand' onclick=\"javascript:showSingleCalendar('divTxt_Manufacturedt"+hcnt+"','Txt_Manufacturedt"+hcnt+"') \" title='Click to open Calendar' src='/images/nav_calendar.gif' border=0 align=absmiddle height=18 width=19/> <div id='divTxt_Manufacturedt"+hcnt+"' style='position: absolute; z-index: 10;'></div></span>";
					
					NewUDILabelHtml = "<span id='SpUDILbl"+cnt+"'><br><input type='text' style=\"border:none;background:"+rowColor+";\" name='lblHtml"+hcnt+"'  readonly size='52' value='"+partUDI+"'>&nbsp;<a href='javascript:fnDelete("+cnt+","+UDcnt+")';><img src='/images/btn_remove.gif' border=0 width=14 height=14></a></span>";
				}else{
					NewQtyHtml = "<span id='Sp"+cnt+"'><BR>&nbsp;&nbsp;&nbsp;<input type='hidden' name='hUDINum"+hcnt+"' value='"+partUDI+"'><input type='hidden' name='Lbl_UDI"+hcnt+"' value='"+partUDI+"'><input type='hidden' name='hprodType"+hcnt+"' value='"+prodType+"'><input type='hidden' name='hWO"+hcnt+"' value='"+Wo+"'><input type='hidden' name='hPNum"+hcnt+"' value='"+pnum+"'><input type='hidden' name='hSkipFl"+hcnt+"' value='"+skipfl+"'><input type='hidden' name='hCnumFmt"+hcnt+"' value='"+cnumFmt+"'><input type='hidden' name='hDupCnum"+hcnt+"' value='"+dupCnum+"'><input type='hidden' name='hProductClass"+hcnt+"' value='"+sterileFl+"'><input type='text' size='3' value='' name='Txt_QtyRec"+hcnt+"' class='InputArea' onFocus=changeBgColor(this,'#EEEEEE'); onBlur=changeBgColor(this,'"+rowColor+"'); tabindex=4>&nbsp;&nbsp;<input type='text' size='8' value='' name='Txt_CNum"+hcnt+"' style=\"text-transform:uppercase;\" class='InputArea' onFocus=changeBgColor(this,'#EEEEEE'); onBlur=changeBgColor(this,'"+rowColor+"'),fnAddUDIDetails("+hcnt+"); tabindex=4>&nbsp;&nbsp;<input type='hidden' value="+val+" name='hPnumRow"+hcnt+"'> <input type='text' size='8' value='' name='Txt_DoNum"+hcnt+"' class='InputArea' onFocus=changeBgColor(this,'#EEEEEE'); onBlur=changeBgColor(this,'"+rowColor+"'),fnAddUDIDetails("+hcnt+");  tabindex=4>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type=text name='Txt_ExpDate"+hcnt+"' id='Txt_ExpDate"+hcnt+"' size='9' value='' class='' tabindex=4 onBlur=\"javascript:changeBgColor(this,'"+rowColor+"');\"  onFocus=\"javascript:changeBgColor(this,'#EEEEEE');\" maxlength='10' disabled='true' align='absmiddle'>&nbsp;<img src=/images/nav_calendar.gif border=0 align='absmiddle' height=18 width=19></img>&nbsp;&nbsp;&nbsp;<input type=text name='Txt_Manufacturedt"+hcnt+"' id='Txt_Manufacturedt"+hcnt+"' size='9' value='' class='InputArea'  tabindex=4 onBlur=\"javascript:changeBgColor(this,'"+rowColor+"'); \"  onFocus=\"javascript:changeBgColor(this,'#EEEEEE');\" maxlength='10'>&nbsp;<img id='Img_Date' style='cursor: hand' onclick=\"javascript:showSingleCalendar('divTxt_Manufacturedt"+hcnt+"','Txt_Manufacturedt"+hcnt+"') \" title='Click to open Calendar' src='/images/nav_calendar.gif' border=0 align=absmiddle height=18 width=19/> <div id='divTxt_Manufacturedt"+hcnt+"' style='position: absolute; z-index: 10;'></div></span>";
					NewUDILabelHtml = "<span id='SpUDILbl"+cnt+"'><br>&nbsp;<input type='text' style=\"border:none;background:"+rowColor+";\" name='nonLblHtml"+hcnt+"'  readonly size='45' value=''><a href='javascript:fnDelete("+cnt+","+UDcnt+")';><img src='/images/btn_remove.gif' border=0 width=14 height=14></a></span>";
				}
			}else{
				if(partUDI != ''){
					NewQtyHtml = "<span id='Sp"+cnt+"'><BR>&nbsp;&nbsp;&nbsp;<input type='hidden' name='hDINum"+hcnt+"' value='"+diNum+"'><input type='hidden' name='Lbl_UDI"+hcnt+"' value='"+partUDI+"'><input type='hidden' name='hprodType"+hcnt+"' value='"+prodType+"'><input type='hidden' name='hWO"+hcnt+"' value='"+Wo+"'><input type='hidden' name='hshowText"+hcnt+"' value='"+udiTextBox+"'><input type='hidden' name='hPNum"+hcnt+"' value='"+pnum+"'><input type='hidden' name='hSkipFl"+hcnt+"' value='"+skipfl+"'><input type='hidden' name='hCnumFmt"+hcnt+"' value='"+cnumFmt+"'><input type='hidden' name='hDupCnum"+hcnt+"' value='"+dupCnum+"'><input type='hidden' name='hProductClass"+hcnt+"' value='"+sterileFl+"'><input type='text' size='3' value='' name='Txt_QtyRec"+hcnt+"' class='InputArea' onFocus=changeBgColor(this,'#EEEEEE'); onBlur=changeBgColor(this,'"+rowColor+"'); tabindex=4>&nbsp;&nbsp;<input type='text' size='8' value='' name='Txt_CNum"+hcnt+"' style=\"text-transform:uppercase;\" class='InputArea' onFocus=changeBgColor(this,'#EEEEEE'); onBlur=changeBgColor(this,'"+rowColor+"'),fnAddUDIDetails("+hcnt+"); tabindex=4>&nbsp;&nbsp;<input type='hidden' value="+val+" name='hPnumRow"+hcnt+"'>  <input type='text' size='8' value='' name='Txt_DoNum"+hcnt+"' class='InputArea' onFocus=changeBgColor(this,'#EEEEEE'); onBlur=changeBgColor(this,'"+rowColor+"'),fnAddUDIDetails("+hcnt+");  tabindex=4>&nbsp;&nbsp;&nbsp;&nbsp;  <input type=text name='Txt_ExpDate"+hcnt+"' id='Txt_ExpDate"+hcnt+"' size='9' value='' class=''  tabindex=4 onBlur=changeBgColor(this,'"+rowColor+"'),fnAddUDIDetails("+hcnt+");  onFocus=\"javascript:changeBgColor(this,'#EEEEEE');\" maxlength='10' disabled='true' align='absmiddle'>&nbsp;<img src=/images/nav_calendar.gif border=0 align='absmiddle' height=18 width=19></img>&nbsp;&nbsp;&nbsp;<input type=text name='Txt_Manufacturedt"+hcnt+"' id='Txt_Manufacturedt"+hcnt+"' size='9' value='' class='InputArea'  tabindex=4 onBlur=changeBgColor(this,'"+rowColor+"'),fnAddUDIDetails("+hcnt+");  onFocus=\"javascript:changeBgColor(this,'#EEEEEE');\" maxlength='10'>&nbsp;<img id='Img_Date' style='cursor: hand' onclick=\"javascript:showSingleCalendar('divTxt_Manufacturedt"+hcnt+"','Txt_Manufacturedt"+hcnt+"') \" title='Click to open Calendar' src='/images/nav_calendar.gif' border=0 align=absmiddle height=18 width=19/> <div id='divTxt_Manufacturedt"+hcnt+"' style='position: absolute; z-index: 10;'></div></span>";
				}else{
					NewQtyHtml = "<span id='Sp"+cnt+"'><BR>&nbsp;&nbsp;&nbsp;<input type='hidden' name='hDINum"+hcnt+"' value='"+diNum+"'><input type='hidden' name='Lbl_UDI"+hcnt+"' value='"+partUDI+"'><input type='hidden' name='hprodType"+hcnt+"' value='"+prodType+"'><input type='hidden' name='hWO"+hcnt+"' value='"+Wo+"'><input type='hidden' name='hshowText"+hcnt+"' value='"+udiTextBox+"'><input type='hidden' name='hPNum"+hcnt+"' value='"+pnum+"'><input type='hidden' name='hSkipFl"+hcnt+"' value='"+skipfl+"'><input type='hidden' name='hCnumFmt"+hcnt+"' value='"+cnumFmt+"'><input type='hidden' name='hDupCnum"+hcnt+"' value='"+dupCnum+"'><input type='hidden' name='hProductClass"+hcnt+"' value='"+sterileFl+"'><input type='text' size='3' value='' name='Txt_QtyRec"+hcnt+"' class='InputArea' onFocus=changeBgColor(this,'#EEEEEE'); onBlur=changeBgColor(this,'"+rowColor+"'); tabindex=4>&nbsp;&nbsp;<input type='text' size='8' value='' name='Txt_CNum"+hcnt+"' style=\"text-transform:uppercase;\" class='InputArea' onFocus=changeBgColor(this,'#EEEEEE'); onBlur=changeBgColor(this,'"+rowColor+"'),fnAddUDIDetails("+hcnt+"); tabindex=4>&nbsp;&nbsp;<input type='hidden' value="+val+" name='hPnumRow"+hcnt+"'> <input type='text' size='8' value='' name='Txt_DoNum"+hcnt+"' class='InputArea' onFocus=changeBgColor(this,'#EEEEEE'); onBlur=changeBgColor(this,'"+rowColor+"'),fnAddUDIDetails("+hcnt+");  tabindex=4>&nbsp;&nbsp;&nbsp;&nbsp;   <input type=text name='Txt_ExpDate"+hcnt+"' id='Txt_ExpDate"+hcnt+"' size='9' value='' class=''  tabindex=4 onBlur=\"javascript:changeBgColor(this,'"+rowColor+"');\"  onFocus=\"javascript:changeBgColor(this,'#EEEEEE');\" maxlength='10' disabled='true' align='absmiddle'>&nbsp;<img src=/images/nav_calendar.gif border=0 align='absmiddle' height=18 width=19></img>&nbsp;&nbsp;&nbsp;<input type=text name='Txt_Manufacturedt"+hcnt+"' id='Txt_Manufacturedt"+hcnt+"' size='9' value='' class='InputArea'  tabindex=4 onBlur=\"javascript:changeBgColor(this,'"+rowColor+"'); \"  onFocus=\"javascript:changeBgColor(this,'#EEEEEE');\" maxlength='10'>&nbsp;<img id='Img_Date' style='cursor: hand' onclick=\"javascript:showSingleCalendar('divTxt_Manufacturedt"+hcnt+"','Txt_Manufacturedt"+hcnt+"') \" title='Click to open Calendar' src='/images/nav_calendar.gif' border=0 align=absmiddle height=18 width=19/> <div id='divTxt_Manufacturedt"+hcnt+"' style='position: absolute; z-index: 10;'></div></span>";
				}		
				NewUDIHtml = "<span id='SpUDI"+cnt+"'><input type='text' size='45' value='' name='Txt_UDI"+hcnt+"' class='InputArea' onFocus=changeBgColor(this,'#EEEEEE'); onBlur=changeBgColor(this,'#ffffff'); tabindex=4>&nbsp;<a href='javascript:fnDelete("+cnt+","+UDcnt+")';><img src='/images/btn_remove.gif' border=0 width=14 height=14></a></span>";
			}
		} else {	
		// Expiry Date Enabled
		if(udiTextBox == 'N'){
			if(partUDI != ''){
				NewQtyHtml = "<span id='Sp"+cnt+"'><BR>&nbsp;&nbsp;&nbsp;<input type='hidden' name='hDINum"+hcnt+"' value='"+diNum+"'><input type='hidden' name='hUDINum"+hcnt+"' value='"+partUDI+"'><input type='hidden' name='hprodType"+hcnt+"' value='"+prodType+"'><input type='hidden' name='hWO"+hcnt+"' value='"+Wo+"'><input type='hidden' name='hPNum"+hcnt+"' value='"+pnum+"'><input type='hidden' name='hSkipFl"+hcnt+"' value='"+skipfl+"'><input type='hidden' name='hCnumFmt"+hcnt+"' value='"+cnumFmt+"'><input type='hidden' name='hDupCnum"+hcnt+"' value='"+dupCnum+"'><input type='hidden' name='hProductClass"+hcnt+"' value='"+sterileFl+"'><input type='text' size='3' value='' name='Txt_QtyRec"+hcnt+"' class='InputArea' onFocus=changeBgColor(this,'#EEEEEE'); onBlur=changeBgColor(this,'"+rowColor+"'); tabindex=4>&nbsp;&nbsp;<input type='text' size='8' value='' name='Txt_CNum"+hcnt+"' style=\"text-transform:uppercase;\" class='InputArea' onFocus=changeBgColor(this,'#EEEEEE'); onBlur=changeBgColor(this,'"+rowColor+"'),fnAddUDIDetails("+hcnt+"); tabindex=4>&nbsp;&nbsp;&nbsp;<input type='hidden' value="+val+" name='hPnumRow"+hcnt+"'><input type='text' size='8' value='' name='Txt_DoNum"+hcnt+"' class='InputArea' onFocus=changeBgColor(this,'#EEEEEE'); onBlur=changeBgColor(this,'"+rowColor+"'),fnAddUDIDetails("+hcnt+");  tabindex=4>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type=text name='Txt_ExpDate"+hcnt+"' id='Txt_ExpDate"+hcnt+"' size='9' value='' class='InputArea'  tabindex=4 onBlur=changeBgColor(this,'"+rowColor+"'),fnAddUDIDetails("+hcnt+");  onFocus=\"javascript:changeBgColor(this,'#EEEEEE');\" maxlength='10'>&nbsp;<img id='Img_Date' style='cursor: hand' onclick=\"javascript:showSingleCalendar('divTxt_ExpDate"+hcnt+"','Txt_ExpDate"+hcnt+"'),fnAddUDIDetails("+hcnt+"); \" title='Click to open Calendar' src='/images/nav_calendar.gif' border=0 align=absmiddle height=18 width=19/> <div id='divTxt_ExpDate"+hcnt+"' style='position: absolute; z-index: 10;'></div>&nbsp;&nbsp;<input type=text name='Txt_Manufacturedt"+hcnt+"' id='Txt_Manufacturedt"+hcnt+"' size='9' value='' class='InputArea'  tabindex=4 onBlur=changeBgColor(this,'"+rowColor+"'),fnAddUDIDetails("+hcnt+");  onFocus=\"javascript:changeBgColor(this,'#EEEEEE');\" maxlength='10'>&nbsp;<img id='Img_Date' style='cursor: hand' onclick=\"javascript:showSingleCalendar('divTxt_Manufacturedt"+hcnt+"','Txt_Manufacturedt"+hcnt+"') \" title='Click to open Calendar' src='/images/nav_calendar.gif' border=0 align=absmiddle height=18 width=19/> <div id='divTxt_Manufacturedt"+hcnt+"' style='position: absolute; z-index: 10;'></div></span>";
				NewUDILabelHtml = "<span id='SpUDILbl"+cnt+"'><br><input type='text' style=\"border:none;background:"+rowColor+";\" name='lblHtml"+hcnt+"'  readonly size='52' value='"+partUDI+"'>&nbsp;<a href='javascript:fnDelete("+cnt+","+UDcnt+")';><img src='/images/btn_remove.gif' border=0 width=14 height=14></a></span>";
			}else{
				NewQtyHtml = "<span id='Sp"+cnt+"'><BR>&nbsp;&nbsp;&nbsp;<input type='hidden' name='hDINum"+hcnt+"' value='"+diNum+"'><input type='hidden' name='hUDINum"+hcnt+"' value='"+partUDI+"'><input type='hidden' name='hprodType"+hcnt+"' value='"+prodType+"'><input type='hidden' name='hWO"+hcnt+"' value='"+Wo+"'><input type='hidden' name='hPNum"+hcnt+"' value='"+pnum+"'><input type='hidden' name='hSkipFl"+hcnt+"' value='"+skipfl+"'><input type='hidden' name='hCnumFmt"+hcnt+"' value='"+cnumFmt+"'><input type='hidden' name='hDupCnum"+hcnt+"' value='"+dupCnum+"'><input type='hidden' name='hProductClass"+hcnt+"' value='"+sterileFl+"'>";
				NewQtyHtml += "<input type='text' size='3' value='' name='Txt_QtyRec"+hcnt+"' class='InputArea' onFocus=changeBgColor(this,'#EEEEEE'); onBlur=changeBgColor(this,'"+rowColor+"'); tabindex=4>&nbsp;&nbsp;";
				NewQtyHtml += "<input type='text' size='8' value='' name='Txt_CNum"+hcnt+"' style=\"text-transform:uppercase;\" class='InputArea' onFocus=changeBgColor(this,'#EEEEEE'); onBlur=changeBgColor(this,'"+rowColor+"'),fnAddUDIDetails("+hcnt+"); tabindex=4>&nbsp;&nbsp;&nbsp;<input type='hidden' value="+val+" name='hPnumRow"+hcnt+"'>";
				NewQtyHtml += "<input type='text' size='8' value='' name='Txt_DoNum"+hcnt+"' class='InputArea' onFocus=changeBgColor(this,'#EEEEEE'); onBlur=changeBgColor(this,'"+rowColor+"'),fnAddUDIDetails("+hcnt+"); tabindex=4>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
				NewQtyHtml += "<input type=text name='Txt_ExpDate"+hcnt+"' id='Txt_ExpDate"+hcnt+"' size='9' value='' class='InputArea'  tabindex=4 onBlur=\"javascript:changeBgColor(this,'"+rowColor+"');,fnAddUDIDetails("+hcnt+"); \"  onFocus=\"javascript:changeBgColor(this,'#EEEEEE');\" maxlength='10'>&nbsp;<img id='Img_Date' style='cursor: hand' onclick=\"javascript:showSingleCalendar('divTxt_ExpDate"+hcnt+"','Txt_ExpDate"+hcnt+"'),fnAddUDIDetails("+hcnt+"); \" title='Click to open Calendar' src='/images/nav_calendar.gif' border=0 align=absmiddle height=18 width=19/> <div id='divTxt_ExpDate"+hcnt+"' style='position: absolute; z-index: 10;'></div>&nbsp;&nbsp;";
				NewQtyHtml += "<input type=text name='Txt_Manufacturedt"+hcnt+"' id='Txt_Manufacturedt"+hcnt+"' size='9' value='' class='InputArea'  tabindex=4 onBlur=\"javascript:changeBgColor(this,'"+rowColor+"'); \"  onFocus=\"javascript:changeBgColor(this,'#EEEEEE');\" maxlength='10'>&nbsp;<img id='Img_Date' style='cursor: hand' onclick=\"javascript:showSingleCalendar('divTxt_Manufacturedt"+hcnt+"','Txt_Manufacturedt"+hcnt+"') \" title='Click to open Calendar' src='/images/nav_calendar.gif' border=0 align=absmiddle height=18 width=19/> <div id='divTxt_Manufacturedt"+hcnt+"' style='position: absolute; z-index: 10;'></div></span>";
					
				NewUDILabelHtml = "<span id='SpUDILbl"+cnt+"'><br>&nbsp;<input type='text' style=\"border:none;background:"+rowColor+";\" name='nonLblHtml"+hcnt+"' readonly size='45' value=''><a href='javascript:fnDelete("+cnt+","+UDcnt+")';><img src='/images/btn_remove.gif' border=0 width=14 height=14></a></span>";
			}
		}else{
			if(partUDI != ''){
				
				NewQtyHtml = "<span id='Sp"+cnt+"'><BR>&nbsp;&nbsp;&nbsp;<input type='hidden' name='Lbl_UDI"+hcnt+"' value='"+partUDI+"'><input type='hidden' name='hDINum"+hcnt+"' value='"+diNum+"'><input type='hidden' name='hprodType"+hcnt+"' value='"+prodType+"'><input type='hidden' name='hWO"+hcnt+"' value='"+Wo+"'><input type='hidden' name='hshowText"+hcnt+"' value='"+udiTextBox+"'><input type='hidden' name='hPNum"+hcnt+"' value='"+pnum+"'><input type='hidden' name='hSkipFl"+hcnt+"' value='"+skipfl+"'><input type='hidden' name='hCnumFmt"+hcnt+"' value='"+cnumFmt+"'><input type='hidden' name='hDupCnum"+hcnt+"' value='"+dupCnum+"'><input type='hidden' name='hProductClass"+hcnt+"' value='"+sterileFl+"'><input type='text' size='3' value='' name='Txt_QtyRec"+hcnt+"' class='InputArea' onFocus=changeBgColor(this,'#EEEEEE'); onBlur=changeBgColor(this,'"+rowColor+"'); tabindex=4>&nbsp;&nbsp;<input type='text' size='8' value='' name='Txt_CNum"+hcnt+"' style=\"text-transform:uppercase;\" class='InputArea' onFocus=changeBgColor(this,'#EEEEEE'); onBlur=changeBgColor(this,'"+rowColor+"'),fnAddUDIDetails("+hcnt+"); tabindex=4>&nbsp;&nbsp;<input type='hidden' value="+val+" name='hPnumRow"+hcnt+"'><input type='text' size='8' value='' name='Txt_DoNum"+hcnt+"' class='InputArea' onFocus=changeBgColor(this,'#EEEEEE'); onBlur=changeBgColor(this,'"+rowColor+"'); tabindex=4>&nbsp;&nbsp;&nbsp;&nbsp;<input type=text name='Txt_ExpDate"+hcnt+"' id='Txt_ExpDate"+hcnt+"' size='9' value='' class='InputArea'  tabindex=4 onBlur=\"javascript:changeBgColor(this,'"+rowColor+"');\"  onFocus=\"javascript:changeBgColor(this,'#EEEEEE');\" maxlength='10' align='absmiddle'>&nbsp;<img id='Img_Date' style='cursor: hand' onclick=\"javascript:showSingleCalendar('divTxt_ExpDate"+hcnt+"','Txt_ExpDate"+hcnt+"'),fnAddUDIDetails("+hcnt+"); \" title='Click to open Calendar' src='/images/nav_calendar.gif' border=0 align=absmiddle height=18 width=19/> <div id='divTxt_ExpDate"+hcnt+"' style='position: absolute; z-index: 10;'></div> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type=text name='Txt_Manufacturedt"+hcnt+"' id='Txt_Manufacturedt"+hcnt+"' size='9' value='' class='InputArea'  tabindex=4 onBlur=\"javascript:changeBgColor(this,'"+rowColor+"'); \"  onFocus=\"javascript:changeBgColor(this,'#EEEEEE');\" maxlength='10'>&nbsp;<img id='Img_Date' style='cursor: hand' onclick=\"javascript:showSingleCalendar('divTxt_Manufacturedt"+hcnt+"','Txt_Manufacturedt"+hcnt+"') \" title='Click to open Calendar' src='/images/nav_calendar.gif' border=0 align=absmiddle height=18 width=19/> <div id='divTxt_Manufacturedt"+hcnt+"' style='position: absolute; z-index: 10;'></div></span>";
			}else{
				
				NewQtyHtml = "<span id='Sp"+cnt+"'><BR>&nbsp;&nbsp;&nbsp;<input type='hidden' name='Lbl_UDI"+hcnt+"' value='"+partUDI+"'><input type='hidden' name='hDINum"+hcnt+"' value='"+diNum+"'><input type='hidden' name='hprodType"+hcnt+"' value='"+prodType+"'><input type='hidden' name='hWO"+hcnt+"' value='"+Wo+"'><input type='hidden' name='hshowText"+hcnt+"' value='"+udiTextBox+"'><input type='hidden' name='hPNum"+hcnt+"' value='"+pnum+"'><input type='hidden' name='hSkipFl"+hcnt+"' value='"+skipfl+"'><input type='hidden' name='hCnumFmt"+hcnt+"' value='"+cnumFmt+"'><input type='hidden' name='hDupCnum"+hcnt+"' value='"+dupCnum+"'><input type='hidden' name='hProductClass"+hcnt+"' value='"+sterileFl+"'><input type='text' size='3' value='' name='Txt_QtyRec"+hcnt+"' class='InputArea' onFocus=changeBgColor(this,'#EEEEEE'); onBlur=changeBgColor(this,'"+rowColor+"'); tabindex=4>&nbsp;&nbsp;<input type='text' size='8' value='' name='Txt_CNum"+hcnt+"' style=\"text-transform:uppercase;\" class='InputArea' onFocus=changeBgColor(this,'#EEEEEE'); onBlur=changeBgColor(this,'"+rowColor+"'),fnAddUDIDetails("+hcnt+"); tabindex=4>&nbsp;&nbsp;<input type='hidden' value="+val+" name='hPnumRow"+hcnt+"'><input type='text' size='8' value='' name='Txt_DoNum"+hcnt+"' class='InputArea' onFocus=changeBgColor(this,'#EEEEEE'); onBlur=changeBgColor(this,'"+rowColor+"'); tabindex=4>&nbsp;&nbsp;&nbsp;&nbsp;<input type=text name='Txt_ExpDate"+hcnt+"' id='Txt_ExpDate"+hcnt+"' size='9' value='' class='InputArea'  tabindex=4 onBlur=\"javascript:changeBgColor(this,'"+rowColor+"');\"  onFocus=\"javascript:changeBgColor(this,'#EEEEEE');\" maxlength='10' align='absmiddle'>&nbsp;<img id='Img_Date' style='cursor: hand' onclick=\"javascript:showSingleCalendar('divTxt_ExpDate"+hcnt+"','Txt_ExpDate"+hcnt+"'),fnAddUDIDetails("+hcnt+"); \" title='Click to open Calendar' src='/images/nav_calendar.gif' border=0 align=absmiddle height=18 width=19/> <div id='divTxt_ExpDate"+hcnt+"' style='position: absolute; z-index: 10;'></div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type=text name='Txt_Manufacturedt"+hcnt+"' id='Txt_Manufacturedt"+hcnt+"' size='9' value='' class='InputArea'  tabindex=4 onBlur=\"javascript:changeBgColor(this,'"+rowColor+"'); \"  onFocus=\"javascript:changeBgColor(this,'#EEEEEE');\" maxlength='10'>&nbsp;<img id='Img_Date' style='cursor: hand' onclick=\"javascript:showSingleCalendar('divTxt_Manufacturedt"+hcnt+"','Txt_Manufacturedt"+hcnt+"') \" title='Click to open Calendar' src='/images/nav_calendar.gif' border=0 align=absmiddle height=18 width=19/> <div id='divTxt_Manufacturedt"+hcnt+"' style='position: absolute; z-index: 10;'></div> &nbsp;  <a href='javascript:fnDelete("+cnt+")';><img src='/images/btn_remove.gif' border=0 width=14 height=14></a></span>";
			}		
			NewUDIHtml = "<span id='SpUDI"+cnt+"'><input type='text' size='45' value='' name='Txt_UDI"+hcnt+"' class='InputArea' onFocus=changeBgColor(this,'#EEEEEE'); onBlur=changeBgColor(this,'#ffffff'); tabindex=4>&nbsp;<a href='javascript:fnDelete("+cnt+","+UDcnt+")';><img src='/images/btn_remove.gif' border=0 width=14 height=14></a></span>";
		}		
	}		
	
	if(udiTextBox == 'N'){
		var QtyObj = eval("document.all.Cell"+val);
		QtyObj.insertAdjacentHTML("beforeEnd",NewQtyHtml);
		if(partUDI != ''){
			var UdiLblObj = eval("document.all.Lbl_UDI"+val);
			UdiLblObj.insertAdjacentHTML("beforeEnd",NewUDILabelHtml);
		}else{
			var UdiLblObj = eval("document.all.NonLbl_UDI"+val);
			UdiLblObj.insertAdjacentHTML("beforeEnd",NewUDILabelHtml);
		}		
	}else{
		var QtyObj = eval("document.all.Cell"+val);
		QtyObj.insertAdjacentHTML("beforeEnd",NewQtyHtml);
		var UDIObj = eval("document.all.UDICell"+val);
		UDIObj.insertAdjacentHTML("beforeEnd",NewUDIHtml);		
	}
	
	//To enable/disable the text box of control number/ donor number based on product type
	if(prodType == '100845'){
		eval("document.frmVendor.Txt_CNum"+hcnt).disabled = true;
	}else{
		eval("document.frmVendor.Txt_DoNum"+hcnt).disabled = true;
	}
	
}



function fnDelete(val,udval){
	if(udval == 1){
		obj = eval("document.all.Sp"+val);
		obj.innerHTML = "";
		Lblobj = eval("document.all.SpUDILbl"+val);
		Lblobj.innerHTML = "";		
	}else{
		obj = eval("document.all.Sp"+val);
		obj.innerHTML = "";		
		UDIobj = eval("document.all.SpUDI"+val);
		UDIobj.innerHTML = "";
	}
}

function hidestatus()
{
	window.status=statusmsg
	return true
}

function fnSubmit()
{
	var vWO = '';
	var vQty = 0;
	var vCntNum = 0; 
    var vpendqty = '';
	var errRecQty='';
	var str = '';
	var obj = '';
	var vPNum = '';
	var vManfDate = '';
	var vExpDate = '';
	var blFlag;
	var PoType = document.frmVendor.hPoType.value;
	var vSkipFl = '';
	var errMgs='';
	var key = '';
	var value = '';
    errVendorMsg =''
    
	// Used for Expiry Date validation
    var hValidation = new Object();
    var testDate;
	var thisDate;
    var errCtrlNm='';
    var errExpDt='';
    var errPstExpDt='';
    var hUDINum = '';
    var vDonorNum = '';
    var vprodtype = '';
    var vSerialNumNeedFl = '';
    var vManufactureDt = '';
    var errDonorNm  = '';
    var errPstManfDt = "";
	hcnt = parseInt(document.frmVendor.hTextboxCnt.value);
	hcnt = hcnt+cnt;	
	document.frmVendor.hTextboxCnt.value = hcnt;
	var textboxlen = document.frmVendor.hTextboxCnt.value;
    cnt = 0;

    var myCollection = new Collection();
	for (var i=0;i<=hcnt;i++)
	{
		var vUdiNum = '';
		val = eval("document.frmVendor.Txt_QtyRec"+i);
		if ( val)
		{
			vQty = val.value;
			if (vQty != '')
			{
				cnt++;
				val = eval("document.frmVendor.hPNum"+i);
				vPNum = val.value;
				val = eval("document.frmVendor.hWO"+i);
				vWO = val.value;
				val = eval("document.frmVendor.hQty"+i);
				vpendqty = val.value;
				if(vpendqty != '')
					{
					vpendqty=parseFloat(vpendqty); 
					}
				//fnValidateTxtFld('Txt_CNum'+i,'Control #');
				val = eval("document.frmVendor.Txt_CNum"+i);
				vCntNum = TRIM(val.value);
				vCntNum = vCntNum.toUpperCase();
				val = eval("document.frmVendor.hSkipFl"+i);
				vSkipFl = val.value;
				val = eval("document.frmVendor.hCnumFmt"+i);
				vCntNumFmt = val.value;
				val = eval("document.frmVendor.hDupCnum"+i);
				vDupCnt = val.value;
				val = eval("document.frmVendor.hProductClass"+i);
				vprodclass = val.value;
				val = eval("document.frmVendor.Txt_ExpDate"+i);
                vExpDate = val.value == null ? "" : val.value.trim();  
                val = eval("document.frmVendor.hprodType"+i);
				vprodtype = val.value;
				val = eval("document.frmVendor.hSerialNumNeedFl"+i);
				vSerialNumNeedFl = val.value;
                
                val = eval("document.frmVendor.Txt_DoNum"+i);
                vDonorNum = TRIM(val.value); 
                /*   Qty Checking*/
                if((PoType == '3101') && (vQty > vpendqty)){
                	errRecQty =errRecQty+vPNum+",";
                   }
                // Not need to throw control number validation if the part number is serialized
                if(vCntNum == '' && errCtrlNm.indexOf(vPNum)==-1 && vprodtype != '100845' && vSerialNumNeedFl != 'Y'){//control number validation should not show for tissue parts; 100845: tissue
					errCtrlNm = errCtrlNm+vPNum+",";
				}	
                
				if(vDonorNum == '' && errDonorNm.indexOf(vPNum)==-1 && vprodtype == '100845'){// donor number validation should show only for tissue parts; 100845: tissue
					errDonorNm = errDonorNm+vPNum+",";
				}
                val = eval("document.frmVendor.Txt_Manufacturedt"+i);
                vManfDate = val.value == null ? "" : val.value.trim(); 
                 
                // Getting the UDI SHOW TEXT BOX vaue and UDI TEXT BOX VALUE                 
                var showText = (validateObject(eval("document.frmVendor.hshowText"+i)))?eval("document.frmVendor.hshowText"+i).value:'';
                
                // IF UDI having in TEXT BOX box only need to save in database.
                // If it is in Label format, no need to appaend to Input string. will format it from Procedure.
                if(showText == 'Y' && eval("document.frmVendor.Txt_UDI"+i) != undefined){
                	val = eval("document.frmVendor.Txt_UDI"+i);                
                	vUdiNum = val.value.toUpperCase();
                }
                //If it is in Label format,  need to appaend to Input string since it format from front side
                else if (eval("document.frmVendor.lblHtml"+i) != undefined)
               	{
                	val = eval("document.frmVendor.lblHtml"+i);
                	vUdiNum = val.value.toUpperCase();
               	}
                              
                // Validate Expiry date for Sterile Parts
				if (document.getElementById('Txt_ExpDate'+i).disabled != true)
				{
					//fnValidateTxtFld('Txt_ExpDate'+i,'<b>Expiry Date</b>');
					if(vExpDate == '' && errExpDt.indexOf(vPNum)==-1){
						errExpDt = errExpDt+vPNum+",";
					}
					// Validate Date in session format
					CommonDateValidation(document.getElementById('Txt_ExpDate'+i),format,' <b>Expiry Date</b> '+ vExpDate.trim() +' is not a valid date in the format'+format);

                    // Validate Part#/Control# pairs have matching Expiry dates
                    // Build Key
                    key = vPNum.trim() + "/" + vCntNum.trim();
					key = key.toUpperCase();
					value = vExpDate.trim();

					if (hValidation.hasOwnProperty(key))
                    {
				    	// Check if dates match (Format MM/DD/YYYY)
                        testDate = hValidation[key];
						thisDate = vExpDate.trim();
				    	if (testDate != thisDate)
				    	{
                            Error_Details("<b>Expiry Date</b> for " + vPNum + " and " + vCntNum + " must match, please correct.");
				    	}

                    } else {
	    				// add key/value
						hValidation[key] = value;
			    	}

					// Expiry Date cannot be a past date (Today or Future Date)
					var currDate = document.frmVendor.hManfDt.value;
					if(dateDiff(currDate,document.getElementById('Txt_ExpDate'+i).value,format)<0){
						//Expiry Date can not be a past date.
						if(errPstExpDt.indexOf(vPNum)==-1){
							errPstExpDt = errPstExpDt+vPNum+",";
						}
						
					}

            	}

				if (fnValidateControl(vPNum,vCntNum,vSkipFl,vCntNumFmt))
				{
					var todayDate = document.frmVendor.hManfDt.value;
					if(vManfDate == ''){
						if ( vCntNum == 'NOC#' || PoType == '3103' )
						{
							vManfDate = document.frmVendor.hManfDt.value;
							//vCntNum = 'NOC#';
						}
						else 
						{
							vManfDate = fnCalDate(vCntNum);
						}
					}
					else
						{	// Manufacture Date should only be past or current date if not empty (cannot be a Future Date)					
							if(dateDiff(todayDate,vManfDate,format)>0){
								 
								if(errPstManfDt.indexOf(vPNum)==-1){
									errPstManfDt = errPstManfDt+vPNum+",";
								}
								
							}
						
						}
					
					if(vCntNum!='' && vCntNum!="NOC#" && !myCollection.add(vCntNum, vPNum) && errMgs.indexOf(vPNum+":"+vCntNum)==-1 && vDupCnt != '80131')
					{
						errMgs=errMgs+vPNum+":"+vCntNum+"<br>";
					}else
					{
						str = str + vPNum +"^"+ vWO+"^"+vQty+"^"+vCntNum+"^"+vDonorNum+"^"+vManfDate+"^"+vExpDate+"^"+vUdiNum+"^"+vprodtype+"|";
					}
				}
			}
		}
	}	 
	if(errCtrlNm != ''){
		Error_Details("<b>Control #</b> cannot be left blank for part(s) "+errCtrlNm.substring(0,errCtrlNm.length-1));
	}
	if(errRecQty != ''){
		Error_Details("<b>Qty to Rec</b> cannot be more than <b>Qty Pend</b> on a <b>Rework PO</b> for the following Part #s:"+ errRecQty.substring(0,errRecQty.length-1));
	}
	if(errDonorNm != ''){
		Error_Details("<b>Donor Number</b> cannot be left blank for part(s) "+errDonorNm.substring(0,errDonorNm.length-1));
	}
	if(errExpDt != ''){
		Error_Details("<b>Expiry Date</b> cannot be left blank for part(s) "+errExpDt.substring(0,errExpDt.length-1));
	}
	if(errPstExpDt!=''){
		Error_Details("<b>Expiry Date</b> can not be a past date for part(s) "+errPstExpDt.substring(0,errPstExpDt.length-1));
	}
	
	if(errPstManfDt!=''){
		Error_Details("<b>Manufacture Date</b> cannot be future date for part(s) "+errPstManfDt.substring(0,errPstManfDt.length-1));
	}
	if(errMgs.length>0)
	{
		Error_Details("The following Control numbers are duplicated <br>"+errMgs);
	}
	if(errVendorMsg != ''){
		Error_Details("Vendor code entered does not match with the Vendor for the following <br>"+errVendorMsg);
	}
	var pslip = document.frmVendor.Txt_Pack.value;
	if (pslip == '')
	{
		Error_Details("Packing Slip value is mandatory. Please enter correct value.");
	}

	if (str == '' && ErrorCount == 0)
	{
		Error_Details("Please enter proper values to submit the form.");
	}
	// Display error messages if exist
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	if(document.frmVendor.Txt_Pack != undefined && document.frmVendor.Txt_Pack != null){
		document.frmVendor.Txt_Pack.value =  pslip.toUpperCase();
	}
	//Disable the submit button when the user clicks the Submit
	if(document.frmVendor.Btn_PlacePO){
		document.frmVendor.Btn_PlacePO.disabled = true;
	}
	//document.frmVendor.hInputString.value = str.substring(0,str.length-1);
	document.frmVendor.action = '/gmRuleEngine.do';
	document.frmVendor.RE_FORWARD.value = "gmPOReceiveServlet";
	document.frmVendor.RE_TXN.value = "50900";
	document.frmVendor.txnStatus.value = "VERIFY";
	document.frmVendor.hInputString.value = str;
	document.frmVendor.hAction.value = "Initiate";
	fnStartProgress('Y');
	document.frmVendor.submit();
	
}





	function Collection() {
	var collection = {};
	var order = [];

	this.add = function(property, value) {
	if (!this.exists(property)) {
	collection[property] = value;
	order.push(property);
	return true;
	}else
	{
		if(collection[property]==value)
			return true;
		else
			return false;
	}
	}
		
	this.exists = function(property) {
	return collection[property] != null;
	}
	
	}

function fnOnPageLoad(){	
	if (document.frmVendor.Txt_PO.value != "")
		{
		document.frmVendor.Txt_Pack.focus();
		}
	else
		{
		document.frmVendor.Txt_PO.focus();
		}
	//Enable the submit button on page load
	if(document.frmVendor.Btn_PlacePO){
		document.frmVendor.Btn_PlacePO.disabled = false;
	}
	
}
function fnCheckKey()
{
	if(event.keyCode == 13)
		 {	
		   //alert("Coming In");
		 	var rtVal = fnLoadPO();
			if(rtVal == false){
				if(!e) var e = window.event;

		        e.cancelBubble = true;
		        e.returnValue = false;

		        if (e.stopPropagation) {
		                e.stopPropagation();
		                e.preventDefault();
		        }
			}
		 }
}

// This FUnction is adding here due to Dynamic Function call change.
// Onclick of the calender Icon , need to call fnAddUDIDetails() function to set Selected date to the UDI format.
function initCal(calendardiv,textboxref,dateformat){	
	var dIvlength = calendardiv.length-1;	
	var count = calendardiv.substr(dIvlength, calendardiv.length) ;
		if((navigator.userAgent.match(/iPhone/i))!=null || (navigator.userAgent.match(/iPad/i))!=null){
			//This commented code for New version 3.5. The old dhtmlx version we can't editable month and year in IPAD. Now we can edit.
			//mCal = new dhtmlxCalendarObject(calendardiv, false, {isMonthEditable: false,isYearEditable: false });
			mCal = new dhtmlXCalendarObject(calendardiv);
		}else{
			//mCal = new dhtmlxCalendarObject(calendardiv, false, {isMonthEditable: true,isYearEditable: true});
			mCal = new dhtmlXCalendarObject(calendardiv);
		}
	//mCal.setYearsRange(1970, 2500);
	// This range only able to select date. 
	mCal.setSensitiveRange("1970-01-01", "2500-31-12"); 
	// Set date format based on login user.
	mCal.setDateFormat(dateformat);	
	// Hide the time in new dhtmlx version 3.5
	mCal.hideTime();	
	// When click calendar icon and set the date in textbox.
	mCal.attachEvent("onClick", function(d) {
		document.getElementById(textboxref).value =  mCal.getFormatedDate(dateformat, d);
		calendars[calendardiv].hide();
		fnAddUDIDetails(count);
		return true;
	 });	
	mCal.hide();
	return mCal;
}

// Function to set the value for UDI column in PO Receive Screen.
// This format is like (01)UDI NUMBER(17)EXPIRY DATE(10)CONTROL NUMBER
function fnAddUDIDetails(cnt){
	i = cnt;	 
		valObj = (validateObject(eval("document.all.Lbl_UDI"+i)))?eval("document.all.Lbl_UDI"+i):'';//validating the object
		var part_cnt = (validateObject(eval("document.all.hPnumRow"+i)))?eval("document.all.hPnumRow"+i).value:'';
		var udiVal = (validateObject(eval("document.all.hUDINum"+i)))?eval("document.all.hUDINum"+i).value:'';		
		var cnvlaue = (validateObject(eval("document.frmVendor.Txt_CNum"+i)))?eval("document.frmVendor.Txt_CNum"+i).value:'';
		cnvlaue = cnvlaue.toUpperCase();
		var expDate = (validateObject(eval("document.all.Txt_ExpDate"+i)))?eval("document.all.Txt_ExpDate"+i).value:'';
		var DonorNumvalue = (validateObject(eval("document.frmVendor.Txt_DoNum"+i)))?eval("document.frmVendor.Txt_DoNum"+i).value:'';
		var manfDate = (validateObject(eval("document.frmVendor.Txt_Manufacturedt"+i)))?eval("document.frmVendor.Txt_Manufacturedt"+i).value:''; 
		var udiFormat = (validateObject(eval("document.frmVendor.hUDIFormat"+part_cnt)))?eval("document.frmVendor.hUDIFormat"+part_cnt).value:'';	
		var diVal = (validateObject(eval("document.all.hDINum"+i)))?eval("document.all.hDINum"+i).value:'';
		
		 
		if(udiVal != ''){
		//comment following code since we will get UDI value by UDI format.
		/*	 if(cnvlaue != '' || expDate != ''){
				if(cnvlaue != '' && expDate == ''){ // Need to append Control Number only
					eval("document.frmVendor.lblHtml"+i).value = udiVal +cnUdiId + cnvlaue;						
				}else if(cnvlaue == '' && expDate != ''){ // Need to append Expiry Date only
					eval("document.frmVendor.lblHtml"+i).value = udiVal + edUdiId + fnChgDateFormat(expDate);						
				}else if(cnvlaue != '' && expDate != ''){ // Need to append Both Control Number and Expiry Date values
					eval("document.frmVendor.lblHtml"+i).value = udiVal + edUdiId + fnChgDateFormat(expDate) + cnUdiId + cnvlaue;
				}						
			}else{
				eval("document.frmVendor.lblHtml"+i).value = udiVal; // Need to set only Part UDI number
			}	*/ 
			 
			 if(udiFormat != '')
			 {
				eval("document.frmVendor.lblHtml"+i).value = fnUDINumber(udiFormat, diVal, cnvlaue, expDate, DonorNumvalue, manfDate );
			  }
			  else
			  {
				eval("document.frmVendor.lblHtml"+i).value = udiVal;
			  }
			 
		}			
}

// Function used to Return the passed date format[MM/dd/yyyy] into yymmdd format.
function fnChgDateFormat(expDate){
	var str = '';	
	var year='';    
	for(i=0;i<expDate.length;i++){
	   var char = expDate.charAt(i);	
	   if(!isNaN(char))
	   str = str+char;	
    }
	year =	str.substr(6,8);	
	str =	year + str.substr(0,4);
	return str;
}

 var str = '';	 
 var strMMYYExpDT = '';
 var strYYMMDDExpDT = ''; 
 var yearYYYYExpDT='';
 var yearYYExpDT='';
 var yearYYYExpDT='';
 var monthExpDT='';    
 var dayExpDT='';    
 var strExpDT = '';
 var strManfDT = '';
 var strYYYJJJExpDt = '';
 var strYYYJJJManufDT = '';

 var yearYYManfDT='';
 var yearYYYManfDT ='';
 var monthManfDT='';    
 var dayManfDT='';

 var strMMDDYYExpDT = ''; 
 var strYYJJJExpDT = ''; 
 var strYYYJJJExpDT = '';
 var jilianJJJExpDT = '';
 var jiliandayExpDT = '';
 var jilianJJJManfDT = '';
 var jiliandayManfDT = '';
// this function used to split the expiry date with different format for year , month and day
function fnExpdate(expDate)
{
	for(i=0;i<expDate.length;i++){
	   var char = expDate.charAt(i);	
	   if(!isNaN(char))
	   strExpDT = strExpDT+char;	
	  }
	yearYYExpDT =  strExpDT.substr(6,2);
	yearYYYExpDT = strExpDT.substr(5,3);
	monthExpDT = strExpDT.substr(0,2);
	dayExpDT = strExpDT.substr(2,2);

	yearYYYYExpDT = strExpDT.substr(4,4); 
	strMMYYExpDT	= monthExpDT + yearYYExpDT ;
	strMMDDYYExpDT = monthExpDT + dayExpDT + yearYYExpDT; 
	strYYMMDDExpDT  = yearYYExpDT + monthExpDT + dayExpDT; 
	strYYYJJJExpDt = yearYYYExpDT; 
}

//this function used to split the manufacturing date with different format for year , month and day
function fnManfdate(manfDate)
{
	for(i=0;i<manfDate.length;i++){
	   var char = manfDate.charAt(i);	
	   if(!isNaN(char))
	   strManfDT = strManfDT+char;	
	  } 
 
	yearYYManfDT =  strManfDT.substr(6,2);	 
	yearYYYManfDT =  strManfDT.substr(5,3);
	monthManfDT = strManfDT.substr(0,2);
	dayManfDT=  strManfDT.substr(2,2);
	yearYYYYmanfDT = strManfDT.substr(4,4); 
	strYYMMDDManfDT  = yearYYManfDT  + monthManfDT  + dayManfDT;  
	strYYYYMMDDManfDT  = yearYYYYmanfDT  + monthManfDT  + dayManfDT;
	strYYYManfDT = yearYYYManfDT;
}

//Function to convert the UDI based on UDI fornmat.  
function fnUDINumber(format, diVal, cnvlaue, expDate, DonorNumvalue, manfDate) { 

	  strMMYYExpDT = '';
	  strYYMMDDExpDT = ''; 
	  yearYYYYExpDT='';
	  yearYYExpDT='';
	  yearYYYExpDT = '';
	  monthExpDT='';    
	  dayExpDT='';    
	  strExpDT = '';
	  strManfDT = '';

	  yearYYManfDT='';
	  yearYYYManfDT='';
	  monthManfDT='';    
	  dayManfDT='';

	  strMMDDYYExpDT = ''; 
	  strYYJJJExpDT = ''; 
	  strYYYJJJExpDT = '';
	  strYYYJJJManufDT = '';
	  jilianJJJExpDT = '';
	  jiliandayExpDT = '';
	  jilianJJJManfDT = '';
	  jiliandayManfDT = '';
	  str = '';
	  var dateObjec = new Date();
	  var currentHour = dateObjec.getHours();
	fnExpdate(expDate);     

	fnManfdate(manfDate);  
	
	// if expiry date is not empty , convert it to Julian date format YYJJJ
	if (expDate != '')
	{
	   jilianJJJExpDT = ConvertToJulian(parseInt(yearYYYYExpDT), parseInt(monthExpDT), parseInt(dayExpDT)) ;	 
	 
	   jiliandayExpDT  = jilianJJJExpDT.toString().substr(4,3); 
	
	   strYYJJJExpDT = yearYYExpDT + jiliandayExpDT; 
	   strYYYJJJExpDT = yearYYYExpDT + jiliandayExpDT;
	}
    // if Manf. date is not empty, convert it to Julian date format
	if(manfDate !=''){
		jilianJJJManfDT = ConvertToJulian(parseInt(yearYYYYmanfDT), parseInt(monthManfDT), parseInt(dayManfDT)) ;
		
		jiliandayManfDT  = jilianJJJManfDT.toString().substr(4,3); 
		strYYYJJJManufDT = yearYYYManfDT + jiliandayManfDT;
	}
	//always replaceAll '|' and '^' symbol as empty string, and DI - 103940 as actual DI value and Donor numner with actual DOnor num value
	//for GS 1 identifier
	//103940	DI			(01)
	//103941	MFG date	(11)
	//103942	EXP date	(17)
	//103943	LOT	(10)
	//103944	Serial #	(21)
	//103945	Donor Id	
	//4000688	Supplemental serial #


	format =  replaceAll(format,'|', '');
	format = replaceAll(format,'^', '');
	format = replaceAll(format,'103940', diVal);
	format = replaceAll(format,'(21)103944', '');
	format = replaceAll(format,'=,103944', '');
	format = replaceAll(format,'/S4000688', '');
	if(DonorNumvalue !=''){
		str = replaceAll(format,'103945', DonorNumvalue);
	}else{
		str = replaceAll(format,'=103945', '');
	}
	
	 
	// if expiry date is not empty, replaceAll it with actual value by format, otherwise replaceAll as empty string with prefix.
	if (strYYMMDDExpDT != '')
	{ 	
		// exp date with contorl #
		str = replaceAll(str,'103942[YYMMDD]103943', strYYMMDDExpDT +cnvlaue); 
		str = replaceAll(str,'103942[MMYY]103943', strMMYYExpDT +cnvlaue); 
		str = replaceAll(str,'103942[MMDDYY]103943', strMMDDYYExpDT +cnvlaue);
		str = replaceAll(str,'103942[YYMMDDHH]103943', strYYMMDDExpDT+currentHour +cnvlaue); 
		str = replaceAll(str,'103942[YYJJJ]103943', strYYJJJExpDT +cnvlaue); 
		str = replaceAll(str,'103942[YYJJJHH]103943', strYYJJJExpDT+currentHour +cnvlaue);
		// Exp date with Serial #
		str = replaceAll(str,'103942[YYMMDD]103944', strYYMMDDExpDT); 
		str = replaceAll(str,'103942[MMYY]103944', strMMYYExpDT); 
		str = replaceAll(str,'103942[MMDDYY]103944', strMMDDYYExpDT);
		str = replaceAll(str,'103942[YYMMDDHH]103944', strYYMMDDExpDT+currentHour); 
		str = replaceAll(str,'103942[YYJJJ]103944', strYYJJJExpDT); 
		str = replaceAll(str,'103942[YYJJJHH]103944', strYYJJJExpDT+currentHour);
		// Only Exp date
		str = replaceAll(str,'103942[YYMMDD]', strYYMMDDExpDT);
		str = replaceAll(str,'103942[YYYJJJ]', strYYYJJJExpDT);
	}	
	else 
	{
		  str = replaceAll(str,'(17)103942[YYMMDD]', ''); 
		  str = replaceAll(str,'/$$103942[MMYY]', '/$$'); 
		  str = replaceAll(str,'/$$2103942[MMDDYY]', '/$$2'); 
		  str = replaceAll(str,'/$$3103942[YYMMDD]', '/$$3'); 
		  str = replaceAll(str,'/$$4103942[YYMMDDHH]', '/$$4'); 
		  str = replaceAll(str,'/$$5103942[YYJJJ]', '/$$5'); 
		  str = replaceAll(str,'/$$6103942[YYJJJHH]', '/$$6'); 
		  str = replaceAll(str,'/$$+103942[MMYY]', ''); 
		  str = replaceAll(str,'/$$+2103942[MMDDYY]', ''); 
		  str = replaceAll(str,'/$$+3103942[YYMMDD]', ''); 
		  str = replaceAll(str,'/$$+4103942[YYMMDDHH]', ''); 
		  str = replaceAll(str,'/$$+5103942[YYJJJ]', ''); 
		  str = replaceAll(str,'/$$+6103942[YYJJJHH]', '');
		  str = replaceAll(str,'=>103942[YYYJJJ]', '');
	}
	
	// if control num is not empty , replaceAll 103943 with actual control num value, otherwise replaceAll (10)103943 with empty string 
	if (cnvlaue != '')
	{
	   str = replaceAll(str,'103943', cnvlaue);
	   }
	else
	{
	   str = replaceAll(str,'(10)103943', '');
	   str = replaceAll(str,'/$103943', '');
	   str = replaceAll(str,'103943', '');
	   }
	//// if manufacturing date is not empty, replaceAll it with actual value by format, otherwise replaceAll as empty string with prefix.
	if (strYYMMDDManfDT != '')
	{ 	str = replaceAll(str,'103941[YYMMDD]', strYYMMDDManfDT); 
		str = replaceAll(str,'103941[YYYYMMDD]', strYYYYMMDDManfDT);
		str = replaceAll(str,'103941[YYYJJJ]', strYYYJJJManufDT);
	}	
	else 
	{
		str = replaceAll(str,'(11)103941[YYMMDD]', ''); 
		str = replaceAll(str,'/16D103941[YYYYMMDD]', '');
		str = replaceAll(str,'=}103941[YYYJJJ]', '');
	}

	str = replaceAll(str,'/$+103944', '');
	str = replaceAll(str,'S103944', '');
	str = replaceAll(str,'103944', '');
	
	return str;
}

function replaceAll(OldString,FindString,ReplaceString) 
{
	var SearchIndex = 0;
  	var NewString = ""; 
  	while (OldString.indexOf(FindString,SearchIndex) != -1)    
	{
    	NewString += OldString.substring(SearchIndex,OldString.indexOf(FindString,SearchIndex));
    	NewString += ReplaceString;
    	SearchIndex = (OldString.indexOf(FindString,SearchIndex) + FindString.length);         
	}
  	NewString += OldString.substring(SearchIndex,OldString.length);
  	return NewString;
}


// this function will convert calendar date to Julian date
function ConvertToJulian(Y,Mo,D ) {
	var a=Math.floor((14-Mo)/12);
	var y=Y+4800-a;
	var m=Mo+(12*a)-3;
	var julian ='';
 
	julian = D + Math.floor((153*m+2)/5) + (365*y) + Math.floor(y/4) - Math.floor(y/100) + Math.floor(y/400) - 32045   ;
 
	return Math.round(julian);
	
}

/*
 * Function for AX Integration to SpineIT:
 * fnLoadPartDetails(): This function will call the AX webservice and fetch the part details 
 * for the Packing slip id Webservice link is decalred in AX_REC_PO_WS_URL key in constant.properties File
 * Author: karthiks
*/
function fnLoadPartDetails() 
{	
	var PartNo="";
	var PartDesc="";
	var Qty="";
	var LotNum="";	
	var POId="";
	var PONumberAX = "";
	var packingslipID = "";
	var strPOId = "";
	var str = "";
	var strPONumber = "";
	var strpartNo = "";
	var strVendorID = "";
	if(document.frmVendor.Txt_Pack != undefined){
	 packingslipID = TRIM(document.frmVendor.Txt_Pack.value.toUpperCase());
	}
	if(document.frmVendor.Txt_PO != undefined){
	 strPOId = TRIM(document.frmVendor.Txt_PO.value);
	}
	if(document.frmVendor.hVendorId != undefined){
	 strVendorID = document.frmVendor.hVendorId.value;
	}
	str = strPOId;
	strPONumber=str.replace( /\D+/g, '');
	var companyid = AXPOCompId;
	var POWebserviceURL = AXPOURL;
	
	//hide the below condition code - not need for PC-3193-branch-dhr-creation---data-popul
	
	/*if(packingslipID != '' && strVendorID == '136') //vendor id 136 := BRANCH MEDICAL GROUP
	{
	var xmlstring =  '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:test="http://test.org/"><soapenv:Header/><soapenv:Body>'+"<test:showPackingSlipDetails><test:packingslipId>"+packingslipID+"</test:packingslipId><test:comp>"+companyid+"</test:comp></test:showPackingSlipDetails></soapenv:Body></soapenv:Envelope>"
	fnStartProgress();
	
	 Use Ajax call for webservice and pass the webservice url,type,data and data type to the AX webservice
	 * if the result [XMLData] is returned in the webservice call ,then serialize the XML data to String
	 * parse the result to parsexml() to make it to valid XML Document
	 * check the <TABLE> tag is availabe in the resulted data from webservice and if it's available then loop and get the part details
	 * if the PO id from Receive shipment screen and PO id from the webservice results are equal then set the quantity and lotnumber for the respective parts in the cart
	 * if we load the invalid packingslipid then throw the soft validation next to the packslip id textbox
	 * 
	 $.ajax({
        url: POWebserviceURL,
		crossDomain: true,
        type: "GET",
        dataType: "xml", 
        data: xmlstring,
		contentType: "text/xml; charset=utf-8",		
        processData: false,      
        success: function (result) {
        	var xmlSerializer = new XMLSerializer();
    		result =  xmlSerializer.serializeToString(result); 
    		console.log(result); 
		}
	 });
	 
	$.ajax({
        url: POWebserviceURL,
		crossDomain: true,
        type: "POST",
        dataType: "xml", 
        data: xmlstring,
		contentType: "text/xml; charset=utf-8",		
        processData: false,      
        success: function (result) {
        	var xmlSerializer = new XMLSerializer();
    		result =  xmlSerializer.serializeToString(result); 
    		console.log(result); 
			xmlDoc = $.parseXML(result);			
    		$xml = $(xmlDoc);
    		  if ($xml.find("Table").length > 0) {
    				 $xml.find("Table").each(function(){
    					PartNo   = $(this).find("PartNo").text();
	        			PartDesc = $(this).find("PartDesc").text();
	        			Qty      = $(this).find("Qty").text();
	        			LotNum   = $(this).find("LotNum").text();
	        			POId     = $(this).find("PO").text();
	        			PONumberAX = POId.replace( /\D+/g, '');
	        			if(strPOId == POId || strPONumber == PONumberAX)
	        				{
			        			for (i=0 ; i<itemdetailsize; i++)
			        			{
			        				if(eval("document.frmVendor.hPNum" + i) != undefined){
			        					 strpartNo = eval("document.frmVendor.hPNum" + i).value;
			        					}
				        			if(strpartNo == PartNo)
				        			{
					        			eval("document.frmVendor.Txt_QtyRec" + i).value= Qty;
					        			eval("document.frmVendor.Txt_CNum" + i).value= LotNum.toUpperCase();
					        			//while scanning the Packingslipt id,Focusing to control number textbox to load the UDI details with control number
					        			eval("document.frmVendor.Txt_CNum" + i).focus();
					        			eval("document.frmVendor.Txt_QtyRec" + i).focus();
					        			break;
				        			}
			        			}
			        			fnClearDiv();  //PMT-38611
	        				}else{
	        					document.getElementById("DivShowPackSlipIDExists").style.display='';
	        					return false;
	        				}
    				 });
    				   
    		 }else{
    			 document.getElementById("DivShowPackSlipIDExists").style.display='';
    		 }
    		  fnStopProgress();
          }
    });
  }*/
}

function fnClearDiv(){
	  document.getElementById("DivShowPackSlipIDExists").style.display='none';
}

function fnEnter()
{
	if(event.keyCode == 13)
		 {	
		 	var rtVal = fnLoadPartDetails();
			if(rtVal == false){
				if(!e) var e = window.event;
		        e.cancelBubble = true;
		        e.returnValue = false;
		        if (e.stopPropagation) {
		                e.stopPropagation();
		                e.preventDefault();
		        }
			}
		 }
}