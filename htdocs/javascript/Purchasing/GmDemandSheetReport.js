//Enter key functionality.
function fnOnHitEnter(){	
	if(event.keyCode == 13)	{	 
		fnLoad();		
	}		
}

function fnLoad(){	
	var setID 		= document.frmDemandSheetReport.templateSetId.value;
	var setGrpNm 	= document.frmDemandSheetReport.templateSetGrpNm.value;
	var templateNm 	= document.frmDemandSheetReport.templateName.value;
	if((templateNm == '0') && (setID == '') && (setGrpNm == '')){
		Error_Details("Please choose a value from <B>Template Name</B> dropdown to proceed.");
	}
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	document.frmDemandSheetReport.strOpt.value = "TemplateReport";
	document.frmDemandSheetReport.submit();
}