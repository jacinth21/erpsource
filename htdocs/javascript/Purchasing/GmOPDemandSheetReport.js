//Enter key functionality.
function fnOnHitEnter(){	
	if(event.keyCode == 13)	{	 
		fnLoad();		
	}		
}

function fnLoad(){	
	var setID 		= document.frmOPDemandSheetReport.templateSetId.value;
	var setGrpNm 	= document.frmOPDemandSheetReport.templateSetGrpNm.value;
	var templateNm 	= document.frmOPDemandSheetReport.templateName.value;
	if((templateNm == '0') && (setID == '') && (setGrpNm == '')){
		Error_Details("Please choose a value from <B>Template Name</B> dropdown to proceed.");
	}
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	document.frmOPDemandSheetReport.strOpt.value = "TemplateReport";
	fnStartProgress('Y');
	document.frmOPDemandSheetReport.submit();
}