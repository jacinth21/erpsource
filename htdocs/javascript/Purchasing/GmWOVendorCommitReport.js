//this function used to on page load to show the grid in report screen
function fnOnPageLoad(){			
if(objGridData.value !=''){ 
gridObj = initGridData('dataGridDiv',objGridData);
}	

}

//this function is used to initiate grid
function initGridData(divRef,objGridData)
	{
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/extweb/dhtmlx/dhtmlxGrid/imgs/");
	gObj.attachHeader("#select_filter,#text_filter,#text_filter,#select_filter,#select_filter,#select_filter"
					 +",#select_filter,#select_filter,#numeric_filter,#select_filter,,#numeric_filter,,#numeric_filter,,");
	gObj.init();
	gObj.loadXMLString(objGridData);	
	return gObj;
	}
//To click Load Button
function fnLoad(){
	var objFromDt='';
	var objToDt='';
	var partNums='';
	var vendorName='';
	var projectName='';
	var showClosedWOFl='';

	objFromDt=document.frmVendorCommit.fromDate;
	objToDt=document.frmVendorCommit.toDate;
	partNums=document.frmVendorCommit.partNums.value;
	vendorName=document.frmVendorCommit.searchvendorId.value;
	projectName=document.frmVendorCommit.searchprojectID.value;
	noOfDays=document.frmVendorCommit.noOfDays.value;
	fromDt=objFromDt.value;
	toDate = objToDt.value;
	if((partNums == '' ) && (vendorName == '' )&&(projectName == '' )&& (fromDt == '' )&&(toDate == '' )&& (noOfDays == '' )){
		Error_Details("Please choose any one of the following: Vendor, Project, Part Number,Commit Due Days,Commit Date");
	}
	if((fromDt == "") && (toDate != "")){
		fnValidateTxtFld('fromDate',message[10527]);
	}else if((fromDt != "") && (toDate == "")){
		fnValidateTxtFld('toDate',message[10528]);
	}
	CommonDateValidation(objFromDt,format,message[10507],format);
	CommonDateValidation(objToDt,format,message[10508],format);
	if (dateDiff(fromDt, toDate, format) < 0)
	{
		Error_Details(message[10579]);
	}
	if (ErrorCount > 0){
		Error_Show();
	    Error_Clear();
		return false;
	}
	 document.frmVendorCommit.strOpt.value = "Load";
    document.frmVendorCommit.action = "/gmVendorCommit.do?method=loadVendorCommitRpt";
	fnStartProgress();
	document.frmVendorCommit.submit();
}
//To View comments log information
function fnViewLog(vendorCommitId)
{
	
	windowOpener("/GmCommonLogServlet?hType=1500&hID="+vendorCommitId,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
}
//This function used to download the grid data
function fnDownloadXLS() {
	gridObj.toExcel('/phpapp/excel/generate.php');

}
//To allow only numberic value
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
function fnQtyHistory (commitQty){
	windowOpener("/GmPageControllerServlet?strPgToLoad=/gmAuditTrail.do&auditId=308&txnId="+ commitQty, "","resizable=yes,scrollbars=yes,top=250,left=300,width=785,height=300,status=1");
}

function fnDtHistory (commitDt){
	windowOpener("/GmPageControllerServlet?strPgToLoad=/gmAuditTrail.do&auditId=307&txnId="+ commitDt, "","resizable=yes,scrollbars=yes,top=250,left=300,width=785,height=300,status=1");
}
