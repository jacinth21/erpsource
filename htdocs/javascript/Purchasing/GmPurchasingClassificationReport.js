
var  part = '';
var	partDesc = '';
var	project = '';
var	divisionNm = '';
var	sales_qty= '';
var	sales_percent = '';
var	sales_per_overall = '';
var	rank = '';
var	last_mon_rank = '';
var	pre_last_mon_rank = '';
var pagination ="";
var format = "";
//This function used to press the enter key then submit the page.
function fnEnter() {
	if (event.keyCode == 13) {	
		fnLoad();
	 }		
}

//Click Load Button
function fnLoad()
{

	var projectId = document.frmPurchaseClassification.projectId.value;
	var partNum = document.frmPurchaseClassification.pnum.value;
	var pnumSuffix = document.frmPurchaseClassification.pnumSuffix.value;
	var divisionId = document.frmPurchaseClassification.divisionId.value;
	var rankId = document.frmPurchaseClassification.rankId.value;
	var abcMonth = document.frmPurchaseClassification.abcMonth.value;   
	var abcYear = document.frmPurchaseClassification.abcYear.value; 
	fnValidateDropDn('abcYear', 'Year');
	fnValidateDropDn('abcMonth', 'Month');
	if(ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
    }	
	fnStartProgress();
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmPurchaseClassificationRpt.do?method=fetchPartClassificationRpt&projectId='+ projectId+
			'&pnum='+encodeURIComponent(partNum)+ '&pnumSuffix='+ pnumSuffix+ '&divisionId='+ divisionId+'&rankId='+ rankId+
			'&abcMonth='+abcMonth+'&abcYear='+abcYear+'&ramdomId=' + new Date().getTime());
	//dhtmlxAjax.get(ajaxUrl, fnPopulateClassificationData)
	dhtmlxAjax.get(ajaxUrl, fnCallBack)
}


function fnCallBack(loader){
	var status = loader.xmlDoc.status;
	if(status == 300){
		Error_Details("Reponse has been Failed.Please Try again Later");
		if (ErrorCount > 0) {
			Error_Show();
			Error_Clear();
			return false;
		}
	}else if (status == 200){
		fnPopulateClassificationData(loader);
	}
}

//Populate classification data 
function fnPopulateClassificationData(loader){
	response = loader.xmlDoc.responseText;
    var purchaseResponse = JSON.parse(loader.xmlDoc.responseText);
	for(var i = 0; i < purchaseResponse.length; i++){
		if(purchaseResponse[i].previouslastmonrank == null){
		    purchaseResponse[i].previouslastmonrank = "";
		}
		if(purchaseResponse[i].rank == null){
		    purchaseResponse[i].rank = "";
		}
		if(purchaseResponse[i].lastmonrank == null){
		    purchaseResponse[i].lastmonrank = "";
		}
	}
	fnStopProgress();
	if (response != ""){
		 // to load purchasing classification details
		if($(document).height() !=undefined || $(document).height() !=null){
			gridHeight = $(document).height() - 150;
		}
		gridObj = new dhtmlXGridObject('dataGridDiv');
		gridObj.setHeader("Part #, Description,Project, Division,Unit Qty, % of Sales, % Running Total,Current Month Rank, Previous Month Rank, Last Previous Month Rank");
		gridObj.attachHeader("#text_filter,#text_filter,#select_filter,#select_filter,#text_filter,#numeric_filter,#numeric_filter,#select_filter,#select_filter,#select_filter");
		gridObj.setInitWidths("75,200,*,150,80,80,80,90,80,80");
		gridObj.setColAlign("left,left,left,left,right,right,right,center,center,center");
		gridObj.setColTypes("ro,ro,ro,ro,ro,ron,ro,ro,ro,ro");
		gridObj.setColSorting("str,str,str,str,int,int,int,str,str,str");
		gridObj.setColumnIds("pnum,pdesc,projnm,divnm,salesqty,salespercent,salesperoverall,rank,lastmonrank,previouslastmonrank");
		gridObj.enableAutoHeight(true, gridHeight, true);
		var footerArry=new Array();	
			footerArry[0]="";
			footerArry[1]="";
			footerArry[2]="";
			footerArry[3]="<b>Total<b>";
			footerArry[4]="<b>{#stat_total}<b>";
			footerArry[5]="";
			footerArry[6]="";	
			footerArry[7]="";	
			footerArry[8]="";	
			footerArry[9]="";	
			var footerStyles = [];
			var footerExportFL = true;
			var dataHost = {};
			pagination = "";
			 	format = "Y"
			var formatDecimal="0.00";
			var formatColumnIndex="5";
			//dataHost.rows = rows;
			
			if (footerArry != undefined && footerArry.length > 0) {
			var footstr = eval(footerArry).join(",");
			if (footerStyles != undefined && footerStyles.length > 0)
			gridObj.attachFooter(footstr, footerStyles);
			else
			gridObj.attachFooter(footstr);
			}
			
			if(pagination == "Y"){
				gridObj.enablePaging(true,100,10,"pagingArea",true);
				gridObj.setPagingSkin("bricks");
			}	
			
			if (format != ""){
				gridObj.setNumberFormat(formatDecimal,formatColumnIndex);
			}
			
			document.getElementById("trDiv").style.display = "table-row";
			document.getElementById("NothingMsg").style.display = 'none';
			document.getElementById("exportOptions").style.display = "table-row";
			gridObj.init();
			gridObj.enableAlterCss("even","uneven");
		
		    gridObj.enableSmartRendering(true);
		    gridObj.parse(JSON.stringify(purchaseResponse),"js");
		    
			}else{
		         document.getElementById("trDiv").style.display = 'none';	
		         document.getElementById("NothingMsg").style.display = 'table-row';
		 		 document.getElementById("exportOptions").style.display = 'none';
	          }
	
}

//to download excel
function fnDownloadXLS() {
	gridObj.toExcel('/phpapp/excel/generate.php');
}


//This Function Used to set custom types conditions in Grid
function fnCustomTypes(gObj,setHeader, setColIds, setInitWidths, setColAlign, setColTypes,setColSorting, enableTooltips, setFilter,gridHeight,pagination,footerArry,footerStyles,format,formatDecimal,formatColumnIndex){

if (setHeader != undefined && setHeader.length > 0) {
 gObj.setHeader(setHeader);
 var colCount = setHeader.length;
 }    

if (setInitWidths != "")
gObj.setInitWidths(setInitWidths);

if (setColAlign != "")
gObj.setColAlign(setColAlign);

if (setColTypes != "")
gObj.setColTypes(setColTypes);

if (setColSorting != "")
gObj.setColSorting(setColSorting);

if (enableTooltips != "")
gObj.enableTooltips(enableTooltips);

if (setFilter != "")
gObj.attachHeader(setFilter);

if (footerArry != undefined && footerArry.length > 0) {
var footstr = eval(footerArry).join(",");
if (footerStyles != undefined && footerStyles.length > 0)
gObj.attachFooter(footstr, footerStyles);
else
gObj.attachFooter(footstr);
}  

if(setColIds != ""){
 gObj.setColumnIds(setColIds);
}

if (gridHeight != "")
gObj.enableAutoHeight(true, gridHeight, true);
else
gObj.enableAutoHeight(true, 400, true);

if(pagination == "Y"){
    gObj.enablePaging(true,100,10,"pagingArea",true);
    gObj.setPagingSkin("bricks");
}	

if (format != ""){
gObj.setNumberFormat(formatDecimal,formatColumnIndex);
}

return gObj;
}