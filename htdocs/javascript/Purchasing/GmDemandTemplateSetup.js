var frmObj;
/* GmDemandTemplateSetup.jsp functions */
function fnFetch() {
	frmObj.demandTempltId.value = frmObj.demandTempltdrpdwn.value;
	if (frmObj.demandTempltId.value == "0"){
		fnReset();
	}
	frmObj.strOpt.value = 'edit';
	frmObj.submit();
}

function fnOnLoad() {
	var objDiv = document.getElementById('spnBtn');
	if(objDiv != null ){
		objDiv.disabled=false;	
	}
	objDiv = document.getElementById('loadSumryBtn');
	if(objDiv != null ){
		objDiv.disabled=true;	
	}
	frmObj = document.frmGmDemandTemplate;
	if (TRIM(frmObj.demandTempltId.value) == "") {
		frmObj.inactiveFlag.disabled = true;
		document.getElementById("spnBtn").innerHTML = '<input type="button" value="&nbsp;Reset&nbsp;" style="width: 7em" class="button" onclick="fnReset();"/>';
	} else {
		if (TRIM(frmObj.sheetStatusId.value) == "102641") {
			frmObj.companyId.disabled = true;
		}
		document.getElementById("spnBtn").innerHTML = '<input type="button" value="&nbsp;Next&nbsp;" style="width: 7em" class="button" onclick="fnLoadTempMapping();"/>';
	}
}

function fnReset() {
	frmObj.demandTempltId.value = "";
	frmObj.demandTempltdrpdwn.value = "0";
	frmObj.demandTempltNm.value = "";
	frmObj.demandPeriod.value = "";
	frmObj.forecastPeriod.value = "";
	frmObj.setBuild.value = "";
	frmObj.companyId.value = "0";
	frmObj.demandSheetOwner.value = "0";
	frmObj.txt_LogReason.value = "";
}

function fnSubmit() {
	var vStrOpt = '';
	if (frmObj.inactiveFlag.checked == true) {
		//Before displaying the confirmation, need to validate the comments Field.
		fnValidateTxtFld("txt_LogReason", "Comments");
		if (ErrorCount > 0) {
			Error_Show();
			Error_Clear();
			return false;
		}
		var vConfirm = confirm("Inactiving a template will release all mapped Sets and Sales group. Do you want to proceed?")
		if (vConfirm == false)
			return false;
		vStrOpt = 'inactive';
	} else {
		fnValidateTxtFld("demandTempltNm", "Template Name");
		fnValidateTxtFld("demandPeriod", "Demand Period");
		fnValidateTxtFld("forecastPeriod", "Forecast Period");
		fnValidateTxtFld("setBuild", "Set Build Forecast");
		fnValidateDropDn("companyId", " Company");
		fnValidateDropDn("demandSheetOwner", " Sheet Owner");
		var chkActiveFl = (frmObj.inactiveFlag.checked)?"on":"off";
		var hiddenActiveFl = (frmObj.hInActiveFl.value == "")?"off":frmObj.hInActiveFl.value;
		if(chkActiveFl != hiddenActiveFl)
			fnValidateTxtFld("txt_LogReason", "Comments");
		vStrOpt = 'save';
	}
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	var objDiv = document.getElementById('spnBtn');
	if(objDiv != null ){
		objDiv.disabled=true;	
	}
	fnStartProgress('Y');
	frmObj.strOpt.value = vStrOpt;
	frmObj.companyId.disabled = false;
	frmObj.btnTemptSub.disabled = true;
	fnStartProgress('Y');
	frmObj.submit();
}

function fnSetDefault() {
	if ((TRIM(frmObj.demandTempltNm.value) != "")
			&& (TRIM(frmObj.demandTempltId.value) == "")) {
		frmObj.demandPeriod.value = frmObj.demandRuleVal.value;
		frmObj.forecastPeriod.value = frmObj.forecastRuleVal.value;
		frmObj.setBuild.value = frmObj.setBuildRuleVal.value;
	}
}

function fnCheckSelections(chkBox) {
	if (chkBox != undefined) {
		document.getElementById("loadSumryBtn").style.visibility = 'visible';
		var arr = chkBox;
		var arrlen = arr.length;
		if (arr.type == 'checkbox') {
			arr.checked = true;

		} else {
			for ( var i = 0; i < arrlen; i++) {
				arr[i].checked = true;
			}
		}
	}
}

function fnGetSelectedVal(chkBox) {
	var selectedVal = ""
	if (chkBox != undefined) {
		var arr = chkBox;
		var arrlen = arr.length;
		if (arr.type == 'checkbox') {
			selectedVal = (arr.checked == true) ? arr.value : "";

		} else {
			for ( var i = 0; i < arrlen; i++) {
				if (arr[i].checked == true) {
					selectedVal += "," + arr[i].value;
				}
			}
			selectedVal = selectedVal.substring(1, selectedVal.length);
		}
	}
	return selectedVal;
}

function fnLoadDemandTemplate() {
	fnStartProgress('Y');
	frmObj.strOpt.value = "edit";
	fnStartProgress('Y');
	frmObj.submit();
}

/* GmDemandTemplateMapping.jsp functions */
function fnPageLoadTempMapping() {
	frmObj = document.frmGmDemandTemplate;
	fnCheckSelections(frmObj.checkGroupAssoc);
	fnCheckSelections(frmObj.checkConSetAssoc);
	fnCheckSelections(frmObj.checkInhSetAssoc);
}

function fnSubmitTempMapping() {
	var returnVal = "";
	var inputStr = "";

	var groupStr = fnGetSelectedVal(frmObj.checkGroupAvail);
	returnVal = fnGetSelectedVal(frmObj.checkGroupAssoc);
	groupStr = (returnVal != "") ? (groupStr != "")?groupStr + "," + returnVal:returnVal: groupStr;

	var conSetStr = fnGetSelectedVal(frmObj.checkConSetAvail);
	returnVal = fnGetSelectedVal(frmObj.checkConSetAssoc);
	conSetStr = (returnVal != "") ? (conSetStr != "")?conSetStr + "," + returnVal:returnVal : conSetStr;

	var inhSetStr = fnGetSelectedVal(frmObj.checkInhSetAvail);
	returnVal = fnGetSelectedVal(frmObj.checkInhSetAssoc);
	inhSetStr = (returnVal != "") ? (inhSetStr != "")?inhSetStr + "," + returnVal:returnVal : inhSetStr;

	if (groupStr != "") {
		inputStr += "40030^" + groupStr + ",|";
	}
	if (conSetStr != "") {
		inputStr += "40031^" + conSetStr + ",|";
	}
	if (inhSetStr != "") {
		inputStr += "4000109^" + inhSetStr + ",|";
	}
	if(inputStr == ''){
		Error_Details("Please select at least one value from <B>Groups/Consignments Sets/InHouse Sets</B> sections to proceed");
		Error_Show();
		Error_Clear();
		return false;
	}	
	fnStartProgress('Y');
	frmObj.inputString.value = inputStr;
	frmObj.strOpt.value = 'save_template_map';
	frmObj.btnMappingSub.disabled = true;
	var objDiv = document.getElementById('loadSumryBtn');
	if(objDiv != null ){
		objDiv.disabled=true;	
	}
	fnStartProgress('Y');
	frmObj.submit();
}
function fnLoadTempSummary() {
	var groupStr = fnGetSelectedVal(frmObj.checkGroupAvail);
	returnVal = fnGetSelectedVal(frmObj.checkGroupAssoc);
	//groupStr = (returnVal != "") ? (groupStr != "")?groupStr + "," + returnVal:returnVal: groupStr;

	var conSetStr = fnGetSelectedVal(frmObj.checkConSetAvail);
	returnVal += fnGetSelectedVal(frmObj.checkConSetAssoc);
	///conSetStr = (returnVal != "") ? (conSetStr != "")?conSetStr + "," + returnVal:returnVal : conSetStr;

	var inhSetStr = fnGetSelectedVal(frmObj.checkInhSetAvail);
	returnVal += fnGetSelectedVal(frmObj.checkInhSetAssoc);
	//inhSetStr = (returnVal != "") ? (inhSetStr != "")?inhSetStr + "," + returnVal:returnVal : inhSetStr;
		
	if((groupStr != "") || (conSetStr != "") || (inhSetStr != "")){
		if(!confirm("Selected Groups/Sets are not mapped. Please click on Submit to Map or do you want to proceed without mapping?")){
			return false;
		}
	}
	if(returnVal == ""){
		Error_Details("Please click the Submit button to unmap <B>Groups/Sets</B>.");
		Error_Show();
		Error_Clear();
		return false;
	}	
	frmObj.strOpt.value = "load_template_summary";
	frmObj.btnMappingSub.disabled = true;
	var objDiv = document.getElementById('btnMappingBack');
	if(objDiv != null ){
		objDiv.disabled=true;	
	}
	var objDiv = document.getElementById('btnMappingSub');
	if(objDiv != null ){
		objDiv.disabled=true;	
	}
	var objDiv = document.getElementById('loadSumryBtn');
	if(objDiv != null ){
		objDiv.disabled=true;	
	}
	fnStartProgress('Y');
	frmObj.submit();
}

function fnChkChange(chkObj, cntObjNm) {

	if (cntObjNm == "group_avail_cnt") {
		v_group_avail_cnt +=(chkObj.checked)?1:-1;
	} else if (cntObjNm == "group_assoc_cnt") {
		v_group_assoc_cnt+=(chkObj.checked)?1:-1;
	} else if (cntObjNm == "conset_avail_cnt") {
		v_conset_avail_cnt+=(chkObj.checked)?1:-1;
	} else if (cntObjNm == "conset_assoc_cnt") {
		v_conset_assoc_cnt+=(chkObj.checked)?1:-1;
	} else if (cntObjNm == "inhouse_avail_cnt") {
		v_inhouse_avail_cnt+=(chkObj.checked)?1:-1;
	} else if (cntObjNm == "inhouse_assoc_cnt") {
		v_inhouse_assoc_cnt+=(chkObj.checked)?1:-1;
	}

	
	if (   v_group_avail_cnt == frmObj.group_avail_cnt.value
		&& v_group_assoc_cnt == frmObj.group_assoc_cnt.value
		&& v_conset_avail_cnt == frmObj.conset_avail_cnt.value
		&& v_conset_assoc_cnt == frmObj.conset_assoc_cnt.value
		&& v_inhouse_avail_cnt == frmObj.inhouse_avail_cnt.value
		&& v_inhouse_assoc_cnt == frmObj.inhouse_assoc_cnt.value) {
		fnSetObjVisibility("btnMappingBack",false);
		fnSetObjVisibility("btnMappingNext",false);
	} else {
		fnSetObjVisibility("btnMappingBack",true);
		fnSetObjVisibility("btnMappingNext",true);
	}
}

function fnSetObjVisibility(objId,state){
	var obj= document.getElementById(objId);
	if(obj != null ){
		obj.disabled=state;	
	}
}

/* GmDemandTemplateSummary.jsp functions */
function fnLoadTempMapping() {
	var objDiv = document.getElementById('spnBtn');
	if(objDiv != null ){
		objDiv.disabled=true;	
	}
	objDiv = eval(frmObj.btnTemptSub); 
	if(objDiv != null ){
		objDiv.disabled=true;	
	}
	objDiv = eval(frmObj.btnSumryEditTempl); 
	if(objDiv != null ){
		objDiv.disabled=true;	
	}
	fnStartProgress('Y');
	frmObj.strOpt.value = "load_template_map";
	frmObj.submit();
}
function fnSaveDemandSheets() {
	fnStartProgress('Y');
	frmObj.strOpt.value = "save_template_summary";
	frmObj.btnSumrySub.disabled = true;
	fnStartProgress('Y');
	frmObj.submit();
}