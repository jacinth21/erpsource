var TotalValue = 0;
//This function used to press the enter key then submit the page.
function fnEnter() {
	if (event.keyCode == 13) {
		fnGo();
	}
}

function fnGo(){	
   	var month = document.frmMonthlySheetReport.Cbo_Month.value;   
	if( month != '0' && document.frmMonthlySheetReport.Cbo_Year.value == '0' ){
		Error_Details("Please select year for the load period when month is chosen.(It is not necessary to select month)");
		Error_Show();
		Error_Clear();
		return false;
	}
	//fnStartProgress('Y');
	document.frmMonthlySheetReport.hAction.value = "Reload";
	fnStartProgress('Y');
	document.frmMonthlySheetReport.submit();
}

function fnReLoadDSSummary(sheetId,monthId,yearId,demandSheetId){
	//fnStartProgress('Y');
	document.frmMonthlySheetReport.action ="/gmOPDSSummary.do?strOpt=fetch&demandSheetMonthId="+sheetId+"&monthId="+monthId+"&yearId="+yearId+"&demandSheetId="+demandSheetId;
	document.frmMonthlySheetReport.submit();
}

function fnReLoadSheet(sheetId,sheetName,sheetStatus){
	if (sheetStatus != '50550')	{
		Error_Details("Sorry. You can only reload OPEN sheets ");
		Error_Show();
		Error_Clear();
	}else {
		document.frmMonthlySheetReport.action ="/GmCommonCancelServlet";
		document.frmMonthlySheetReport.hTxnId.value = sheetId;
		document.frmMonthlySheetReport.hTxnName.value = sheetName;
		document.frmMonthlySheetReport.hAction.value = "";		
		document.frmMonthlySheetReport.submit();
	}		
}
//for changing the status from Approve to Open
function fnChangeStatus(sheetId,sheetName,sheetStatus){	
	if (sheetStatus != '50551')	{
		Error_Details(message[209]);
		Error_Show();
		Error_Clear();
	}else{
		document.frmMonthlySheetReport.action ="/GmCommonCancelServlet";
		document.frmMonthlySheetReport.hTxnId.value = sheetId;
		document.frmMonthlySheetReport.hTxnName.value = sheetName;
		document.frmMonthlySheetReport.hCancelType.value = "RDSST";
		document.frmMonthlySheetReport.hAction.value = "";		
		document.frmMonthlySheetReport.submit();
	}
}