function fnOnPageLoad(){
	document.all.trDiv.style.display="none";
	document.all.ExcelExport.style.display="none";
	document.all.NothingFound.style.display="none";
}

function fnLoad(){
	var ackFromDt=document.frmVendorAck.fromDate;
	var ackToDt=document.frmVendorAck.toDate;
	var partNums=document.frmVendorAck.partNums.value;
	var strPartSearch = document.frmVendorAck.strPartSearch.value;
	var vendorId=document.frmVendorAck.vendorId.value;
	var projectId=document.frmVendorAck.projectID.value;
	var agentName = document.frmVendorAck.strPurAgentName.value;
	var fromDt=ackFromDt.value;
	var toDate = ackToDt.value;

	if(vendorId == '' && projectId == '' && fromDt == '' && toDate == '' && agentName == 0){
		Error_Details(message_operations[890]);
	}
	if((fromDt == "") && (toDate != "")){
		fnValidateTxtFld('fromDate',message[10527]);
	}else if((fromDt != "") && (toDate == "")){
		fnValidateTxtFld('toDate',message[10528]);
	}
	CommonDateValidation(ackFromDt, format, Error_Details_Trans(message_operations[300],format));
	CommonDateValidation(ackToDt, format, Error_Details_Trans(message_operations[301],format));
	if (dateDiff(fromDt, toDate, format) < 0)
	{
		Error_Details(message[10579]);
	}
	if (ErrorCount > 0){
		Error_Show();
	    Error_Clear();
		return false;
	}
	fnStartProgress();
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmVendorAckReport.do?method=fetchVendorWOAckDtls&vendorId='+
	    vendorId +'&projectID='+projectId+'&partNums='+partNums+'&likeSearch='+strPartSearch+'&strPurAgentName='+agentName
	    +'&fromDate='+fromDt+'&toDate='+toDate+'&ramdomId=' + new Date().getTime());
	dhtmlxAjax.get(ajaxUrl, fnReportResponse)
}
function fnReportResponse(loader){
	response = loader.xmlDoc.responseText;
	fnStopProgress();
	if (response != ""){
			var setInitWidths = "180,80,170,90,90,150,90,87,70,70,80,80,80,80,80,80,80,40,60";
			var setColAlign = "left,left,left,left,left,left,left,left,right,left,right,center,left,left,center,right,left,right,right";
			var setColTypes = "ro,ro,ro,viewPO,viewWO,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro";
			var setColSorting = "str,str,str,str,str,str,str,str,int,str,int,str,str,int,int,int,str,int,int";
			var setHeader = ["Project", "Part Number","Description", "PO ID", "WO ID", "Vendor", "PO Date","WO Due Date", "Total WO Qty", "WO Ack Due","WO Ack Qty", "WO Ack Due Days","Purchase Agent","Lead Time Date","WO Ack Lead Time Late","WO Ack Short Qty","WO Ack Status","Qty Received","WO Pending Qty"];
			var setFilter = ["#select_filter", "#text_filter", "#text_filter","#select_filter", "#select_filter", "#select_filter","#select_filter", "#select_filter", "#numeric_filter","#select_filter", "#numeric_filter", "#numeric_filter","#select_filter", "#numeric_filter", "#numeric_filter", "#numeric_filter","#select_filter", "#numeric_filter", "#numeric_filter"];
			var setColIds = "PROJECTNM,PNUM,PDESC,POID,WOID,VENNM,PODATE,WODUEDT,WO_QTY,WOACKDT,ACKQTY,ACKDUEDAY,PURCHASEAGENTNAME,LEADTIMEDATE,WOACKLEADTIMELATE,WOACKSHORTQTY,WOACKSTATUS,QTYRECEIVED,WOPENDQTY";
			var enableTooltips = "true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true";
			var gridHeight ="600";
			// to set auto height
			if($(document).height() !=undefined || $(document).height() !=null){
				gridHeight = $(document).height() - 200 ;
			}

			document.getElementById("trDiv").style.display = "table-row";
			document.getElementById("NothingFound").style.display = 'none';
			document.getElementById("ExcelExport").style.display = "table-row";
			gridObj = initGridObject('dataGridDiv');
		    gridObj = fnCustomTypes(gridObj,setHeader, setColIds, setInitWidths, setColAlign, setColTypes,setColSorting, enableTooltips, setFilter,gridHeight);
		    gridObj = loadDhtmlxGridByJsonData(gridObj,response);
		    gridObj.enableHeaderMenu();
		    gridObj.setColumnHidden(12, true);
		    gridObj.setColumnHidden(13, true);
		    gridObj.setColumnHidden(17, true);
		    gridObj.setColumnHidden(18, true);
		    ackduedays_row_id = gridObj.getColIndexById("ACKDUEDAY");
		    woackleadtimelate_row_id = gridObj.getColIndexById("WOACKLEADTIMELATE");
		    woackshortqty_row_id = gridObj.getColIndexById("WOACKSHORTQTY");
		    woid_row_id = gridObj.getColIndexById("WOID");
		    ackqty_row_id = gridObj.getColIndexById("ACKQTY");
		    qtyreceived_row_id = gridObj.getColIndexById("QTYRECEIVED");
		    woackstatus_row_id = gridObj.getColIndexById("WOACKSTATUS"); 
	   		var woid_prev = '';
	   		var ackqty_prev = 0;
		    gridObj.forEachRow(function(rowId) {
		   		ackduedays = gridObj.cellById(rowId, ackduedays_row_id).getValue();
		   		woackleadtime = gridObj.cellById(rowId, woackleadtimelate_row_id).getValue();
		   		woackshortqty = gridObj.cellById(rowId, woackshortqty_row_id).getValue();
		   		woid = gridObj.cellById(rowId, woid_row_id).getValue();
		   		ackqty = parseInt(gridObj.cellById(rowId, ackqty_row_id).getValue());
		   		qtyreceived= parseInt(gridObj.cellById(rowId, qtyreceived_row_id).getValue())
		  	if(ackduedays < 0){			
				gridObj.cellById(rowId, ackduedays_row_id).setValue("("+(ackduedays*-1)+")");
				gridObj.setCellTextStyle(rowId,ackduedays_row_id,"color:red;");
		   	}
		  	if(woackleadtime < 0){			
				gridObj.cellById(rowId, woackleadtimelate_row_id).setValue("("+(woackleadtime*-1)+")");
				gridObj.setCellTextStyle(rowId,woackleadtimelate_row_id,"color:red;");
		   	}
		  	if(woackshortqty < 0){			
				gridObj.cellById(rowId, woackshortqty_row_id).setValue("("+(woackshortqty*-1)+")");
				gridObj.setCellTextStyle(rowId,woackshortqty_row_id,"color:red;");
		   	}
		  	if(woid_prev == woid){
		  		ackqty_prev = ackqty_prev + ackqty;
		  		woid_prev = woid;
		  		if(ackqty_prev <= qtyreceived){
		  			gridObj.cellById(rowId, woackstatus_row_id).setValue("Closed");
		  		}
		  		else{
		  			gridObj.cellById(rowId, woackstatus_row_id).setValue("Open");
		  		}
		  	}else{
		  		ackqty_prev=0;
		  		ackqty_prev = ackqty_prev + ackqty;
		  		woid_prev = woid;
		  		if(ackqty_prev <= qtyreceived){
		  			gridObj.cellById(rowId, woackstatus_row_id).setValue("Closed");
		  		}
		  		else{
		  			gridObj.cellById(rowId, woackstatus_row_id).setValue("Open");
		  		}
		  	}
		  	
		});
	}else{
	        document.getElementById("trDiv").style.display = 'none';	
	        document.getElementById("NothingFound").style.display = 'table-row';
	        document.getElementById("ExcelExport").style.display = 'none';
    }
}

//This function used to create user defined column types in Grid
//This Function for viewPO
function eXcell_viewPO(cell) {
	// the eXcell name is defined here
	if (cell) { // the default pattern, just copy it
		this.cell = cell;
		this.grid = this.cell.parentNode.grid;
	}
	this.setValue = function(val) {
		this.setCValue("<a href=\"#\" onClick=fnLoadPO('"+ val +"')>"+ val +"</a>", val);
	}

	this.getValue = function() {
		return this.cell.childNodes[0].innerHTML; // gets the value
	}
}
eXcell_viewPO.prototype = new eXcell;

function fnLoadPO(poId){
	windowOpener("/GmPOServlet?hAction=ViewPO&hCloseFlag=Y&hPOId="+poId,"PO","resizable=yes,scrollbars=yes,top=150,left=50,width=770,height=600");
}
//This function used to create user defined column types in Grid
//This Function for view WO
function eXcell_viewWO(cell) {
	// the eXcell name is defined here
	if (cell) { // the default pattern, just copy it
		this.cell = cell;
		this.grid = this.cell.parentNode.grid;
	}
	this.setValue = function(val) {
			this.setCValue("<a href=\"#\" onClick=fnLoadWO('"+ val +"')>"+ val +"</a>", val);
	}

	this.getValue = function() {
		return this.cell.childNodes[0].innerHTML; // gets the value
	}
}
eXcell_viewWO.prototype = new eXcell;

function fnLoadWO(woId){
	windowOpener("/GmWOServlet?hAction=PrintWO&hWOId="+woId+"&hDocFl=Y","WO1","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=640");
}
// Function to validate number and allow decimal and negative values
function isNumberKey(val)
{
	return /^-?(0|[1-9]\d*)(\.\d+)?$/.test(val);
}

//to download excel
function fnExport() {
	gridObj.toExcel('/phpapp/excel/generate.php');
}

//This Function Used to set custom types conditions in Grid
function fnCustomTypes(gObj,setHeader, setColIds, setInitWidths, setColAlign, setColTypes,setColSorting, enableTooltips, setFilter,gridHeight){

	if (setHeader != undefined && setHeader.length > 0) {
		gObj.setHeader(setHeader);
 		var colCount = setHeader.length;	
	}    

	if (setInitWidths != "")
		gObj.setInitWidths(setInitWidths);

	if (setColAlign != "")
		gObj.setColAlign(setColAlign);

	if (setColTypes != "")
		gObj.setColTypes(setColTypes);

	if (setColSorting != "")
		gObj.setColSorting(setColSorting);

	if (enableTooltips != "")
		gObj.enableTooltips(enableTooltips);

	if (setFilter != "")
		gObj.attachHeader(setFilter);

	if(setColIds != "")
		gObj.setColumnIds(setColIds);

	if (gridHeight != "")
		gObj.enableAutoHeight(true, gridHeight, true);

	return gObj;
}