var count = 0;
var columns = new Array();
var operators = new Array();

function fnRemoveRow(id) {
	var trObj = eval("document.all.tr"+id);
	var trLineObj = eval("document.all.trline" + id);
	var selectList = trObj.getElementsByTagName("select");
	var txt = trObj.getElementsByTagName("input")[0];
	selectList[0].value="0";
	selectList[1].value="0";
	txt.value = "";
}

function fnAddRow(id) {
	var idx = parseInt(id) + 1;
	document.all.index.value = idx;

	var tbody = document.getElementById("filter").getElementsByTagName("tbody")[0];
	
	var linerow = document.createElement("TR");
	var attrLineRowId = document.createAttribute("id");
	attrLineRowId.nodeValue = "trline" + idx;
	linerow.setAttributeNode(attrLineRowId);
	
	var linetd = document.createElement("TD");

	var attrCol = document.createAttribute("colspan");
	attrCol.nodeValue = "5";
	linetd.setAttributeNode(attrCol);
	
	var attrClass = document.createAttribute("class");
	attrClass.nodeValue = "LLine";
	linetd.setAttributeNode(attrClass);
	linerow.appendChild(linetd);
	
	var row = document.createElement("TR");
	var attrRowId = document.createAttribute("id");
	attrRowId.nodeValue = "tr" + idx;
	row.setAttributeNode(attrRowId);
	
    var td1 = document.createElement("TD");
    td1.innerHTML = document.all.index.value;

    var td2 = document.createElement("TD");
    td2.innerHTML = '<a href=javascript:fnRemoveRow(' + idx + ');><img border="0" valign="center" src="/images/btn_remove.gif" height="10" width="9"/></a>';
    
    var td3 = document.createElement("TD")
    td3.innerHTML = fnGetDropdown(idx, columns);
    
    var td4 = document.createElement("TD")
    td4.innerHTML = fnGetDropdown(idx, operators);
    
    var td5 = document.createElement("TD")
    td5.innerHTML = '<input type="text" name=txt'+idx+' size="10" class="InputArea"/>';
    
    row.appendChild(td1);
	row.appendChild(td2);
	row.appendChild(td3);
	row.appendChild(td4);
	row.appendChild(td5);
	
	tbody.appendChild(linerow);
	tbody.appendChild(row);

}

function fnGetDropdown(idx, list) {
	var arrayList = list;
	var innerhtml = '<select name=cbo'+idx+'><option value="0">[Choose One]</option>';
	var len = list.length;
	for(var i=0; i<len; i++) {
		var opt = arrayList[i];
		var arrObj = opt.split(',');
		var id = arrObj[0];
		var name = arrObj[1];
		innerhtml = innerhtml + '<option id=' + id + ' value=' + id + '>' + name + '</option>';
	}
	innerhtml = innerhtml + '</select>';
	return innerhtml;
}

function fnCrossTabSubmit() {

	var filter = document.getElementById('filter');
	if(filter != undefined) {
		var selectList = filter.getElementsByTagName('select');
		var txtList = filter.getElementsByTagName('input');
		var mslist = new Array();
		var oplist = new Array();

		var len = selectList.length;
		//alert('len = ' + len);
		var mscnt = 0;
		var opcnt = 0;
		for(var i=1;i<=len;i++) {
			if(i%2 == 0) {
				oplist[opcnt] = selectList[i-1];
				opcnt++;
			} else {
				mslist[mscnt] = selectList[i-1];
				mscnt++;
			}
		}

		var filterStr = '';
		for(var i=0;i<mslist.length;i++) {
			var milestone = mslist[i];
			var operator = oplist[i];
			filterStr = filterStr + milestone.options[milestone.selectedIndex].text + ',' + oplist[i].value + ',' + txtList[i].value + ';';
		}
		//alert('filterStr = ' + filterStr);
		document.all.hfilter.value = filterStr;
	} else {
		//alert('filter is not yet defined');
	}
}

function fnToggleFilter() {
	var showHide = document.getElementById('showhide');
	//alert(showHide.innerHTML);
	var tbody = document.getElementById('filter').getElementsByTagName('tbody')[0];

	if(tbody.style.display == 'none') {
		tbody.style.display = '';
		showHide.innerHTML = '<a href=javascript:fnToggleFilter();><img border=\'0\' valign=\'center\' src=\'/images/minus.gif\' height=\'10\' width=\'9\'/></a>';
	} else {
		tbody.style.display = 'none';
		showHide.innerHTML = '<a href=javascript:fnToggleFilter();><img border=\'0\' valign=\'center\' src=\'/images/plus.gif\' height=\'10\' width=\'9\'/></a>';
	}
}