
var tempPnum = '';
var blDiscFlag = false;
function validate(cnt,obj) 
{
	var pnum = ''; 
	var qty = '';
	var accid = parent.document.all.Txt_AccId.value;
    var repid = parent.document.all.hRepId.value;
	var gpoid = document.frmCart.hGpoId.value;
	if (gpoid == '')
	{
		gpoid = '0';
	}

    if (accid == 0)
    {
    	alert(message[5171]);
    }
    else
    {
	    trcnt = cnt;
		var pnumobj = eval("document.frmCart.Txt_PNum"+trcnt);
		var qtyobj = eval("document.frmCart.Txt_Qty"+trcnt);
		var priceobj = eval("document.frmCart.Txt_Price"+trcnt);
	    changeBgColor(obj,'#ffffff');

		if (pnumobj.value != '' && qtyobj.value != '')
		{

		   if (priceobj.value != '')
		   {
				fnCalExtPrice(priceobj,trcnt,'Y');
		   }
		   else
		   {
			   pnum = pnumobj.value.toUpperCase();
			   qty = qtyobj.value;
			   tempPnum = pnum;
			
			   var url = fnAjaxURLAppendCmpInfo("/GmCartAjaxServlet?pnum=" + encodeURIComponent(pnum)+ "&qty=" + encodeURIComponent(qty)+ "&acc="+encodeURIComponent(accid)+ "&rep="+encodeURIComponent(repid)+ "&gpoid="+encodeURIComponent(gpoid)+ "&regexp=NO"+"&"+fnAppendCompanyInfo());
			  // var url = "ValidateServlet";
			   if (typeof XMLHttpRequest != "undefined") {
				   req = new XMLHttpRequest();
			   } else if (window.ActiveXObject) {
				   req = new ActiveXObject("Microsoft.XMLHTTP");
			   }
			   //req.open("POST", url, true);
			   req.open("GET", url, true);
			   req.onreadystatechange = callback;
			
			   //req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			   //req.send("id=" + encodeURIComponent(obj.value));
			
			   req.send(null); 
			  
		   } 
		} // end of 'if partnum and qty != 0'
	}// end of 'if account id is null'
}
 
function callback() 
{
    if (req.readyState == 4) {
        if (req.status == 200) {
	        var xmlDoc = req.responseXML;
	        parseMessage(xmlDoc);
	        var priceobj = eval("document.frmCart.Txt_Price"+trcnt);
	        fnCalExtPrice(priceobj,trcnt);
	        if((showDiscount == 'YES') && blDiscFlag != false){
	        	fnCalculateDiscount();
	        }
        }
        else
        {
	        setErrMessage('<span class=RightTextBlue>Part Number does not exist</span>');
        }
    }
}

function parseMessage(xmlDoc) 
{
	var strarr = 'pdesc,stock,price,loanerstatus,lprice,loancogs,rfl,ctrl_needed_fl';
	var pnum = xmlDoc.getElementsByTagName("pnum");
	var opt = (parent.document.all.hOpt != undefined)?parent.document.all.hOpt.value:'';
	
	var ordertype = parent.document.all.Cbo_OrdType.value;

	if (pnum.length == 0)
	{
		setErrMessage('<span class=RightTextBlue>Part Number does not exist</span>');
		return;
	}

	var strobj =  strarr.split(',');

	var strlen = strobj.length;
	var strresult = '';

	for (var i=0;i<strlen ;i++ )
	{
		var str = strobj[i];
		for (var x=0; x<pnum[0].childNodes.length; x++) 
		{
			xmlstr = pnum[0].childNodes[x].tagName;
			if (xmlstr == str)
			{
				if(pnum[0].childNodes[x].firstChild){
					strresult = strresult + '^' + parseXmlNode(pnum[0].childNodes[x].firstChild);
					break;
				}else{
					strresult = strresult + '^' ;
				}
			}
		}
	}

	strresult = strresult.substr(1,strresult.length);
	var strobj =  strresult.split('^');

	var pdesc = strobj[0];
	var stock = strobj[1];
	var price = strobj[2];
	var loaner = strobj[3];
	var list = strobj[4];
	var lcogs = strobj[5];
	var releasefl = strobj[6];
	var ContNumNeedfl = strobj[7];
	obj = eval("document.frmCart.Txt_Qty"+trcnt);
	qty = parseInt(obj.value);
	var amount;
	
	//For quote, we are using list price instead of account price
	if(opt =='PHONE' || opt =='PROFORMA')
		amount = qty*price;
	else
		amount = qty*list;

	if (pdesc == null)
	{
		setErrMessage('<span class=RightTextBlue>Part Number does not exist</span>');
		return;
	}
	else if (releasefl == 'N')
	{
		setErrMessage('<span class=RightTextBlue>This part has not yet been released for sale</span>');
		return;
	}
 	else if (list == 0)
	{
		setErrMessage('<span class=RightTextBlue>Price is not set for this Part</span>');
		return;
	}
/*	else if (price == '')
	{
		setErrMessage('<span class=RightTextBlue>Price is not set for this Part for this Account</span>');
		return;
	}*/
	else
	{
		obj = eval("document.frmCart.hLoanCogs"+trcnt);
		obj.value = lcogs;

		obj = eval("document.frmCart.Cbo_OrdPartType"+trcnt);

		if (lcogs == '')
		{
			obj.value = 50300;
			obj.disabled = true;
			ordtype = parent.document.all.Cbo_OrdType.value;
			//alert(ordtype);
			if (ordtype == '2530')
			{
				Error_Details('The cart contains a part that is not a Loaner. Order Type changed to Bill & Ship');
				parent.document.all.Cbo_OrdType.value = 2521;
				Error_Show();
				Error_Clear();
				fnResetLoanerFields(trcnt);
			}
		}
		else
		{
			obj.disabled = false;
		}
		setMessage(pdesc,stock,price,amount,loaner,list);
	}

	// to skip the control number validation
	if (ordertype == 26240232 || ordertype == 26240233){
		ContNumNeedfl = '';
	}
	
	window.parent.Reload(tempPnum, '1', ordertype);
	if(ContNumNeedfl != undefined && ContNumNeedfl !='' && ("document.all.hCntrlNumberNeededFl"+trcnt) != undefined){
		obj = eval("document.all.hCntrlNumberNeededFl"+trcnt);
		if(obj!= undefined && obj != '')
			obj.value = ContNumNeedfl;
	}
}
 
function setMessage(pdesc,stock,price,amount,loaner,list) 
{
	var opt = parent.document.all.hOpt.value;
	
	var obj = eval("document.all.Lbl_Desc"+trcnt);
	obj.innerHTML = "&nbsp;" + pdesc;

    obj = eval("document.all.Lbl_Stock"+trcnt);
    obj.innerHTML = stock + "&nbsp;&nbsp;";

    obj = eval("document.all.Txt_BasePrice"+trcnt);
    if(obj!=undefined && obj!=null){
    	obj.value =  (opt == 'QUOTE')?formatNumber(list ):formatNumber(price ); //list price is needed for QUOTE
    }
    
    obj = eval("document.all.hBasePrice"+trcnt);
    if (opt == 'QUOTE'){ 
	    obj.value = formatNumber(list );
    }else{
    	obj.value = formatNumber(price );
    }
    
    obj = eval("document.frmCart.Txt_Price"+trcnt);
    if (opt == 'QUOTE') // Quote module. Price is taken from T205 rather than from T507
    {
    	obj.value = formatNumber(list);
    }
    else 
    {
    	obj.value = formatNumber(price);
    }
    
    obj = eval("document.all.Lbl_Amount"+trcnt); 
    obj.innerHTML = formatNumber(amount) + "&nbsp;";

    obj = eval("document.all.hPartPrice"+trcnt);
    obj.value = formatNumber(price);
    
	fnCalculateTotal();
}

function setErrMessage(msg) 
{
    var obj = eval("document.all.Lbl_Desc"+trcnt);
    obj.innerHTML = msg;
    
    obj = eval("document.all.Txt_Qty"+trcnt);
    obj.value = '';

    obj = eval("document.all.Txt_PNum"+trcnt);
    obj.value = '';
    obj.focus(); 
}

var varPartNum;
var varCount;
var objPartCell;

function fnAddRow(id)
{
	var opt = (parent.document.all.hOpt != undefined)?parent.document.all.hOpt.value:'';
	
	var tbody = document.getElementById(id).getElementsByTagName("TBODY")[0];
    var row = document.createElement("TR")

    var td0 = document.createElement("TD")
    td0.innerHTML = cnt+1;

    var td1 = document.createElement("TD")
    td1.innerHTML = fnCreateCell('TXT','PNum',10);
   
    var td2 = document.createElement("TD")
    td2.innerHTML = fnCreateCell('TXT','Qty',3);
    td2.align = "center";
    
    var td3 = document.createElement("TD")
    td3.innerHTML = fnCreateCell('TXT','Price',8);
    td3.innerHTML = td3.innerHTML + '<input type="hidden" name="hBasePrice' + cnt + '" value="">';
    td3.align = "Right";
    
    var td4 = document.createElement("TD")
	td4.id = "Lbl_Stock"+cnt;
    td4.align = "Right";
    
    var td5 = document.createElement("TD")
	td5.id = "Lbl_Amount"+cnt;
    td5.align = "Right";
    
    var td6 = document.createElement("TD")
	td6.id = "Lbl_Desc"+cnt;
    td6.align = "Left";
    if(opt == 'QUOTE'){
    	var tdhid = document.createElement("TD")
    	tdhid.colSpan='3';    	
    	tdhid.innerHTML = '<input type=hidden name="Chk_BO' + cnt + '" tabindex="-1">';
    	tdhid.innerHTML = tdhid.innerHTML + '<input type="hidden" name="hLoanCogs' + cnt + '" value="">';
    	tdhid.innerHTML = tdhid.innerHTML + '<input type="hidden" name="Cbo_OrdPartType' + cnt + '" value="50300">';
    	tdhid.innerHTML = tdhid.innerHTML + '<input type="hidden" name="Txt_RefId' + cnt + '" value="">';
    	tdhid.innerHTML = tdhid.innerHTML + '<input type="hidden" name="Chk_Cap' + cnt + '" value="">';
    //}
    
    //if(opt == 'QUOTE'){//Base Price is not needed for US
		var td14 = document.createElement("TD")
	   	td14.innerHTML = fnCreateCell ('TXT','BasePrice', 8);
		td14.align = "right";
    }
    if(opt != 'QUOTE'){
	    var td7 = document.createElement("TD")
	    td7.innerHTML = fnCreateCell('CHK','BO',1);
	    td7.align = "center";
	
	    var td8 = document.createElement("TD")
	    td8.innerHTML = fnCreateCell('Cbo','OrdPartType',1);
	    td8.align = "center";
	
	    var td9 = document.createElement("TD")
	    td9.innerHTML = fnCreateCell('CHK','Cap',1);
	    td9.align = "Center";
	    var td11 = document.createElement("TD")
		td11.innerHTML = fnCreateCell('TXT','RefId',7);
		// MNTTASK-8623 - Added Control Number Column
		var td12 = document.createElement("TD")
	   	td12.innerHTML = fnCreateCell ('TXT','Cnum', 7);
    }
    
    var td10 = document.createElement("TD")
    td10.innerHTML = fnCreateCell('IMG','',1);
    
    row.appendChild(td0); //TD
	row.appendChild(td10); //IMG
    row.appendChild(td1); //Part num
    row.appendChild(td2); //Quantity
    row.appendChild(td4); //Stock
    if(opt!='QUOTE'){
    	row.appendChild(td7); //BO
	}
    row.appendChild(td6); //Part Desc
    if(opt!='QUOTE'){
	    row.appendChild(td8); //Type
		row.appendChild(td11); //Ref id
		row.appendChild(td9); //Cap
    }else{
    	row.appendChild(tdhid);
    }
    if(opt == 'QUOTE')
    	row.appendChild(td14); //Base Price
    
    row.appendChild(td3); //Price EA
    row.appendChild(td5); //Ext Price
    if( (opt=='PHONE')){
    	row.appendChild(td12); //Cnum
    }
    tbody.appendChild(row);
    var row1 = document.createElement("TR");
    var td20 = document.createElement("TD"); 
    td20.colSpan =16;
    td20.className="LLine";
    td20.height="1";
    row1.appendChild(td20);
    tbody.appendChild(row1);
	cnt++;
	document.frmCart.hRowCnt.value = cnt;
}

function fnCreateCell(type,val,size)
{
	param = val;
	val = val + cnt;
	var html = '';
	var opt = (parent.document.all.hOpt != undefined)?parent.document.all.hOpt.value:'';
	if (type == 'TXT')
	{
		if (param == 'PNum') {
				html = '<input type=text size='+size+' name=Txt_'+val+' class=InputArea onBlur=fnSetPartSearch('+cnt+',this); onFocus=changeBgColor(this,\'#AACCE8\'); value=\'\'>';
						}
		else if (param == 'Qty') {
				html = '&nbsp;<input type=text size='+size+' name=Txt_'+val+' class=InputArea onblur=validate('+cnt+',this); onFocus=changeBgColor(this,\'#AACCE8\'); value=\'\'>';
						}
		else if (param == 'Price') {
				html = '<input type=text size='+size+' name=Txt_'+val+' class=InputArea style=\'text-align: right;\' onBlur=fnCalExtPrice(this,'+cnt+',\'Y\'); onFocus=changeBgColor(this,\'#AACCE8\'); tabindex=\'-1\' value=\'\'><input type=hidden name=hPartPrice'+cnt+'>';
						}
		else if (param == 'RefId') {
				html = '&nbsp;<input type=text size='+size+' name=Txt_'+val+' class=InputArea onBlur=fnChkValidFieldVal(this,'+cnt+'); tabindex=\'-1\' onFocus=changeBgColor(this,\'#AACCE8\'); value=\'\'>';
						}
		else if (param == 'Cnum') {
				html = '&nbsp;<input type=text size='+size+' name=Txt_'+val+' class=InputArea onBlur=\'fnValidateCtrlNum('+cnt+',this)\' tabindex=\'-1\' onFocus=\'fnRemoveImage('+cnt+',this)\' value=\'\'> <input type=hidden name=hCntrlNumberNeededFl'+cnt+'> <span id=DivShowCnumAvl'+cnt+' style=\'vertical-align:middle; display: none;\'> <img tabindex=\'-1\' height=16 width=19 title=\'Control number available\' src=/images/success.gif></img></span> <span id=DivShowCnumNotExists'+cnt+' style=\'vertical-align:middle; display: none;\'> <img height=12 width=12 title=\'Control number does not exists\' src=/images/delete.gif></img></span>'; 
					}
		else if(param == 'BasePrice'){
				html = '&nbsp;<input type=text size='+size+' name=Txt_'+val+' class=InputArea onBlur=fnCalExtPrice(this,'+cnt+',\'Y\'); onFocus=changeBgColor(this,\'#AACCE8\'); tabindex=\'-1\' value=\'\'>';
					}
	}
	
	else if (type == 'CHK')
	{
		html = '<input type=checkbox name=Chk_'+val+' tabindex="-1">';
		if (param == 'BO')
		{
			html = html + ' <input type=hidden name=hLoanCogs'+cnt+' value="">';
		}
	}
	
	else if (type == 'Lbl')
	{
		html = '&nbsp;';
	}
	
	else if (type == 'Btn')
	{
		var fn = size;
		html = '<input type=button onclick='+fn+' value = PartLookup name=Btn_'+val+' class=Button>';
	}
	
	else if (type == 'Cbo')
	{
		var fn = size;
		var obj =  parent.document.all.Cbo_OrdType.value;
		if(obj==2530)
		{
			html = '<select name=Cbo_'+val+' class=RightText tabindex="-1" onchange="fnChangeStatus(this.value,'+val+');"><option value="50300">C</option><option value="50301" selected>L</option></select>';
		}
		else
		{
			html = '<select name=Cbo_'+val+' class=RightText tabindex="-1" onchange="fnChangeStatus(this.value,'+val+');"><option value="50300">C</option><option value="50301">L</option></select>';
		}
	}

	else if (type == 'IMG')
	{
		var fn = size;
		html = '<a href=javascript:fnRemoveItem('+val+') tabindex=\'-1\'; ><img border=0  Alt=Remove from cart valign=left src=/images/btn_remove.gif height=10 width=9></a>';
	}
	
	return html;
}

function fnOpenPart()
{
	var varAccId = parent.document.all.Txt_AccId.value;
	if (objPartCell == null)
	{
		varPartNum = "";
	}
	else
	{
		varPartNum = objPartCell.value; 
		windowOpener("/GmSearchServlet?hAction=SearchPart&Txt_PartNum="+encodeURIComponent(varPartNum)+"&hCount="+varCount+"&Txt_AccId="+varAccId,"Search","resizable=yes,scrollbars=yes,top=340,left=420,width=600,height=435");
	}
}

function fnSetPartSearch(count,obj)
{
	objPartCell = eval("document.frmCart.Txt_PNum"+count);
	eval("document.frmCart.Txt_PNum"+count).value = objPartCell.value.toUpperCase();
	objPartQty = eval("document.frmCart.Txt_Qty"+count);
	objRefId = eval("document.frmCart.Txt_RefId"+count);
	objRefId.value = '';
	changeBgColor(obj,'#FFFFFF');
	if (objPartQty.value != '')
	{
		var priceobj = eval("document.frmCart.Txt_Price"+count);
		priceobj.value = '';
		validate(count,obj);
	}
}

function fnClearVal(count)
{
	varCount = count;
	objPartCntCell = eval("document.frmPhOrder.hPrice"+varCount);
	if (objPartCntCell == "" || objPartCntCell == null) 
	{
	}

	objPartCntCell.value = 0;
}

function fnReturnStatus(status)
{
	if (status)
	{
		return 'Y';
	}
	else
	{
		return '';
	}
}

function fnCreateOrderString()
{
	
	if (showDiscount == 'YES') {
		// MNTTASK - 3603 SA - Ability to calculate discount for Quotes - Amount Validation
		var  strDiscountIndex = parent.document.all.hDiscountIndex.value;
		var objDiscountVal = eval("parent.document.frmPhOrder.Txt_Quote"+strDiscountIndex).value ;
		var cnt = document.frmCart.hRowCnt.value;
		var strOpt = parent.document.all.hOpt.value;
		var listPrice = 0.00;
		var DisEAPRice = 0.00;
		objDiscountVal =(objDiscountVal == '' || objDiscountVal == undefined)?0:objDiscountVal;
		var AccDiscount = (parent.document.all.hAccDiscnt != undefined)?parent.document.all.hAccDiscnt.value:0;
		objDiscountVal = parseFloat(formatNumber(objDiscountVal)) + parseFloat(formatNumber(AccDiscount));
		objDiscountPercentage = trim(objDiscountVal)!=''?(parseFloat(objDiscountVal)/100):parseFloat(0);
		var lbl_Total = formatNumber(document.frmCart.hTotal.value);
		if(objDiscountVal == '')
		{
			var btn_calc_discount = eval("document.frmCart.Btn_Calculate");
			btn_calc_discount.disabled = true;
			objDiscountVal.value=0;
			//fnCalculateDiscount();
		}
		for (i=0;i<cnt;i++)
		{
			pobj = eval("document.frmCart.Txt_PNum"+i);
			if(pobj.value != '')
			{
				if(strOpt != 'QUOTE'){// For US, we are using hBasePrice to calculate the discount, doesn't have text field
					// MNTTASK-4744 - Discount Calculation : Added Price EA column in the UI. Always we have to get txtR_price value to DB.
					priceEAObj = eval("document.frmCart.hBasePrice"+i);// Base price to calculate the discount
				}else{
					priceEAObj = eval("document.frmCart.Txt_BasePrice"+i);
				}
				listPrice = eval("document.all.Txt_Price"+i).value; //Getting the Price EA value to compare with the new discount calculated while submitting
				TotalExtPrice = eval("document.all.Lbl_Amount"+i).innerHTML;
				qty = eval("document.frmCart.Txt_Qty"+i).value;

				DisEAPRice =parseFloat(RemoveComma(priceEAObj.value)) - (parseFloat(RemoveComma(priceEAObj.value) * objDiscountPercentage));

				TotalExtPrice = TotalExtPrice.substring(0,TotalExtPrice.indexOf('&nbsp;'));
				
				// Once submit and the input string error value it's goto Phoneorder.js. 
				if(!blDiscFlag && objDiscountVal != 0){
					inputstr = "ApplyAcc_Err";
					if(lbl_Total != '0.00'){
						return inputstr;
					}
				}
				if(objDiscountPercentage!=0)
				{
					if(formatNumber(DisEAPRice) != formatNumber(listPrice))
					{
						inputstr = "Discount_Err";
						if(lbl_Total != '0.00'){
							return inputstr;
						}
					}
				}else{
					fnCalExtPrice(priceEAObj,i);
				}
			}
		}
		// MNTTASK - 3603 SA - Ability to calculate discount for Quotes - Amount Validation
		}
	
	var varRowCnt = document.frmCart.hRowCnt.value;
	var k;
	var obj;
	var str;
	var token = ',';
	var cnum = '';
	var status = true;
	var inputstr = '';

	for (k=0; k < varRowCnt; k ++)
	{
		obj = eval("document.frmCart.Txt_PNum"+k);
		if (trimString(obj.value) != '')
		{
			str = trimString(obj.value) + token;
			obj = eval("document.frmCart.Txt_Qty"+k);
			str = str + trimString(obj.value) + token;
			str = str + cnum + token;
			obj = eval("document.frmCart.Cbo_OrdPartType"+k);
			str = str + trimString(obj.value) + token;
			obj = eval("document.frmCart.Txt_RefId"+k);
			str = str + trimString(obj.value) + token;
			obj = eval("document.frmCart.Chk_Cap"+k);
			str = str + trimString(fnReturnStatus(obj.checked)) + token;
			obj = eval("document.frmCart.Txt_Price"+k);
			str = str + RemoveComma(trimString(obj.value)) + token;
			obj = eval("document.frmCart.Chk_BO"+k);
			str = str + trimString(fnReturnStatus(obj.checked)) + '|';

			inputstr = inputstr + str;
		}
	}

	return inputstr;
}

function fnCalculateTotal()
{
	var varRowCnt = document.frmCart.hRowCnt.value;
	var k;
	var obj;
	var qty = 0;
	var price = 0.0;
	var total = 0.0;

	var objDiscountPercentage = fnDiscountPercentage();
	for (k=0; k <= varRowCnt; k ++)
	{
		obj = eval("document.frmCart.Txt_PNum"+k);
		if (obj)
		{
			if (obj.value != '')
			{
				obj = eval("document.frmCart.Txt_Qty"+k);
				qty = parseInt(obj.value);
				obj = eval("document.frmCart.Txt_Price"+k);
				price = parseFloat(RemoveComma(obj.value));
				total = total + (price * qty);
			}
		}
	}
	var strAccCurrency = (parent.document.all.hAccCurrency != undefined)?parent.document.all.hAccCurrency.value:'';
	document.frmCart.hTotal.value = total;
	document.all.Lbl_Total.innerHTML = '<b>'+strAccCurrency+' '+formatNumber(total)+'</b>&nbsp;';
}

function fnUpdatePrice()
{
	//alert( document.frmCart.Cbo_Construct[document.frmCart.Cbo_Construct.selectedIndex].value);
	if (document.frmCart.Cbo_Construct.value == '0')
	{
		alert(message[5172]);
		return;
	}
	var varPartNums = '';;
	var blQty;
	var blPrice;
	var varAccId = parent.document.all.Txt_AccId.value;
	var varAccIdVal = varAccId.value;
	var comma = ',';
	var varRowCnt = document.frmCart.hRowCnt.value;
	var k;
	var varPartVal = "";

	for (k=0; k < varRowCnt; k ++)
	{
		objPartCell = eval("document.frmCart.Txt_PNum"+k);
		objQtyCell = eval("document.frmCart.Txt_Qty"+k);
		objPriceCell = eval("document.frmCart.Txt_Price"+k);
		
		if (objPartCell == "" || objPartCell == null )
		{
				comma = "";
		}
		else if (objPartCell.value == "")
		{
				comma = "";
		}
		else
		{
			comma = ",";
			varPartNums = varPartNums + objPartCell.value + comma;
			varPartVal = varPartVal + objPartCell.value + comma;
		}

		if (objQtyCell == "" || objQtyCell == null  || objQtyCell == "undefined" || objPriceCell == "" || objPriceCell == null  || objPriceCell == "undefined" )
		{
		}
		else if (objQtyCell.value =="" | objQtyCell.value ==null)
		{
		
		}
		else if (!objQtyCell.value.match(/^\d+$/))
		{
			blQty = "1";
			break;
		}
		else if(!RemoveComma(objPriceCell.value).match(/^[\d\.]+$/))
		{
			blPrice = "1";
			break;
		}
	}


	var varParsePartVal = varPartVal.substring(0,varPartVal.length -1);
	//parent.document.all.Txt_PartNum.value = varParsePartVal;
	
	if (varAccIdVal == "")
	{
		 Error_Details(message[66]);
	}
	else if (varPartNums == "" || varPartNums == null)
	{
		Error_Details(message[67]);
	}
	else if (blQty == "1")
	{
	 	Error_Details(message[68]);
	}
	else if (blPrice == "1")
	{
		 Error_Details(message[69]);
	}

	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	//fnSubmit();	
	var str = fnCreateOrderString();
	
	document.frmCart.hAction.value = 'UpdatePrice';
	document.frmCart.hInputStr.value = str;
	varPartNums = varPartNums.substr(0,varPartNums.length-1);
	document.frmCart.hPartNumStr.value = varPartNums;
	document.frmCart.hConsValue.value = document.frmCart.Cbo_Construct[document.frmCart.Cbo_Construct.selectedIndex].id;
	document.frmCart.submit();
}


function fnCalExtPrice(obj,cnt,onSubmitFl)
{
	var qty = 0;
	var price = 0.0;
	var DisEAPRice = 0.0;
	var pobj = eval("document.frmCart.Txt_Qty"+cnt);
	qty = parseInt(pobj.value);
	var objDiscountPercentage = 0;

	if (!isNaN(qty)) 
	{
		price = parseFloat(RemoveComma(obj.value));
		var objDiscountPercentage = fnDiscountPercentage();
		DisEAPRice = (parseFloat(price) - (parseFloat(price) * objDiscountPercentage));
		
		if(blDiscFlag && onSubmitFl=='Y'){
			DisEAPRice = price;
		}
		
		var eprice = DisEAPRice * qty;
		var pobj = eval("document.all.Lbl_Amount"+cnt);
		pobj.innerHTML = formatNumber(eprice)+"&nbsp;";
		obj.value = formatNumber(price);
		
		var obj = eval("document.all.Txt_Price"+cnt);
		obj.value = formatNumber(DisEAPRice);
		fnCalculateTotal();
		
		var partAmtObj = eval("document.all.hPartPrice"+cnt);
		var partAmtVal = parseFloat(partAmtObj.value);	
	}
	changeBgColor(obj,'#ffffff');
}

var vFilterShowFl = false;
function fnShowFilters(val)
{
	var obj = eval("document.all."+val);
	var obj1 = eval("document.all."+val+"img");
	if (obj.style.display == 'none')
	{
		obj.style.display = 'block';
		if(obj1){
			obj1.src = '/images/minus.gif';
		}
	}
	else
	{
		obj.style.display = 'none';	
		if(obj1){
			obj1.src = '/images/plus.gif';
		}
	}
}

function fnToggleCart()
{
	fnShowFilters('PartnPricing');
	fnShowFilters('PartnPricingHeader');
	fnShowFilters('PartnPricingDiv');
	fnShowFilters('PartnPricingFooter');
}

function fnSubmit()
{
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	var consigncnt = 0;
	// enabling Order Type dropdowns
	var pobj = '';	
	var cnt = document.frmPhOrder.hCnt.value;
	for (i=0;i<cnt;i++)
	{
		var obj = eval("document.frmPhOrder.Cbo_OrdPartType"+i);
		pobj = eval("document.frmPhOrder.Txt_PartNum"+i);

		if (obj)
		{
			obj.disabled = false;
			if (obj.value == '50300' && pobj.value != '')
			{
				consigncnt++;
			}
		}
		obj = eval("document.frmPhOrder.Chk_BO"+i);
		if (obj)
		{
			obj.disabled = false;
		}
	}

	document.frmPhOrder.Cbo_ShipCarr.disabled = false;
	document.frmPhOrder.Cbo_ShipMode.disabled = false;
	document.frmPhOrder.Cbo_ShipTo.disabled = false;
	document.frmPhOrder.Cbo_Rep.disabled = false;

	if (consigncnt == 0 && document.frmPhOrder.hAction.value == "PlaceOrder")
	{
		// setting order to 'Bill Only-Loaner' if no parts are from consignments
		document.frmPhOrder.Cbo_OrdType.value = 2530; 
		document.frmPhOrder.Cbo_ShipCarr.value = '5040';
		document.frmPhOrder.Cbo_ShipMode.value = '5031';
		document.frmPhOrder.Cbo_ShipTo.value = '4124';
		document.frmPhOrder.Cbo_Rep.value = '0';
	}

	document.frmPhOrder.submit();
}

function fnLoanerInfo(partnum,repid)
{
	windowOpener('/GmLoanerPartRepServlet?hAction=Load&hPartNum='+encodeURIComponent(partnum)+'&hRepId='+repid,"Pack","resizable=yes,scrollbars=yes,top=150,left=150,width=850,height=610");	
}


function fnChangeStatus(val,cnt)
{
	obj = eval("document.frmCart.Chk_BO"+cnt);
	if (obj)
	{
		if (val == 50301)
		{
			obj.checked = false;
			obj.disabled = true;
		}
		else
		{
			obj.disabled = false;
		}
	}
}

function fnCheckConstructAccount(conobj)
{
	/*
	var obj = document.frmPhOrder.Txt_AccId;
	var conlbl = obj[obj.selectedIndex].label;
	
	if (obj.value == 0)
	{
		conobj.selectedIndex = 0;
	}else
	{
		if (conlbl == 0)
		{
			 Error_Details('This Account does not have Capitated Pricing.');
		}
	}

	if (ErrorCount > 0)
	{
			Error_Show();
			conobj.selectedIndex = 0;
			Error_Clear();
			return false;
	}
	*/

}

function fnRemoveItem(val)
{
	var opt = (parent.document.all.hOpt != undefined)?parent.document.all.hOpt.value:'';
	
	obj = eval("document.frmCart.Txt_PNum"+val);
	window.parent.Reload(obj.value, '-1', '' );
	obj.value = '';
	obj = eval("document.frmCart.Txt_Qty"+val);
	obj.value = '';	
	obj = eval("document.frmCart.Txt_Price"+val);
	obj.value = '';
	obj = eval("document.frmCart.hPartPrice"+val);
	obj.value = '';
    obj = eval("document.all.Lbl_Desc"+val);
    obj.innerHTML = "";
    obj = eval("document.all.Lbl_Stock"+val);
    obj.innerHTML = "";  
    
    obj = eval("document.all.Lbl_Amount"+val);
    obj.innerHTML = "";
	obj = eval("document.all.Txt_Cnum"+val);
	if(obj!=undefined && obj!=null)
		obj.value = '';
	obj = eval("document.all.Txt_BasePrice"+val);
	if(obj!=undefined && obj!=null)
		obj.value = '';
	obj = eval("document.all.hBasePrice"+val);
	if(obj!=undefined && obj!=null)
		obj.value = '';
	
    obj = eval("document.all.hCntrlNumberNeededFl"+val);
    if(obj!=undefined && obj!=null)
    	obj.value = '';
    obj = eval("document.all.Lbl_PriceEA"+val);
   	if(obj!=undefined && obj!=null)
		obj.value = '';
    
    if(opt == 'PHONE'){
    	fnRemoveImage(val);
    }
	fnCalculateTotal();
}

function fnClearCart()
{
	var opt = (parent.document.all.hOpt != undefined)?parent.document.all.hOpt.value:'';
	var varRowCnt = document.frmCart.hRowCnt.value;
	
	for (val=0; val < varRowCnt; val ++)
	{
		obj = eval("document.frmCart.Txt_Qty"+val);
		obj.value = '';
		obj = eval("document.frmCart.Txt_PNum"+val);
		obj.value = '';
		obj = eval("document.frmCart.Txt_Price"+val);
		obj.value = '';
		obj = eval("document.frmCart.hPartPrice"+val);
		obj.value = '';
		obj = eval("document.all.Lbl_Desc"+val);
		obj.innerHTML = "";
		obj = eval("document.all.Lbl_Stock"+val);
		obj.innerHTML = "";  
		obj = eval("document.all.Lbl_Amount"+val);
		obj.innerHTML = "";
		obj = eval("document.all.Txt_Cnum"+val);
		if(obj!=undefined && obj!=null)
			obj.value = '';
		obj = eval("document.all.Txt_BasePrice"+val);
		if(obj!=undefined && obj!=null)
			obj.value = '';
		obj = eval("document.all.hBasePrice"+val);
		if(obj!=undefined && obj!=null)
			obj.value = '';
		obj = eval("document.all.hCntrlNumberNeededFl"+val);
		if(obj!=undefined && obj!=null)
			obj.value = '';
		if(opt == 'PHONE'){
	    	fnRemoveImage(val);
	    }
	}
	fnCalculateTotal();
}

function fnResetLoanerFields(cnt)
{
	parent.document.all.Cbo_ShipCarr.disabled = false;
	parent.document.all.Cbo_ShipMode.disabled = false;
	parent.document.all.Cbo_ShipTo.disabled = false;
	parent.document.all.Cbo_Rep.disabled = false;
	parent.document.all.Cbo_ShipCarr.value = '5001';
	parent.document.all.Cbo_ShipMode.value = '5004';
	parent.document.all.Cbo_ShipTo.value = '4121';
	varRepId = parent.document.all.hRepId.value;
	parent.document.all.Cbo_Rep.value = varRepId;

	boobj = eval("document.frmCart.Chk_BO"+cnt);
	boobj.disabled = false;
	var rowcnt = document.frmCart.hRowCnt.value;
	cnt++;
	for (i=cnt;i<rowcnt;i++)
	{
		obj = eval("document.frmCart.Cbo_OrdPartType"+i);
		boobj = eval("document.frmCart.Chk_BO"+i);
		obj.disabled = false;
		boobj.disabled = false;
		obj.value = '50300';
	}
}

function fnLoad(ordtype)
{
	if (ordtype == '2530')
    {
    	var cnt = document.frmCart.hRowCnt.value;
    	for (i=0;i<cnt;i++)
		{
			var obj = eval("document.frmCart.Cbo_OrdPartType"+i);
			pobj = eval("document.frmCart.Txt_PNum"+i);
			qobj = eval("document.frmCart.Txt_Qty"+i);

			if (pobj && qobj)
			{
				if (pobj.value != '' && qobj.value != '')
				{
					lobj = eval("document.frmCart.hLoanCogs"+i);
					if (lobj.value == '')
					{
						Error_Details('The cart contains a part that is not a Loaner. Order Type changed to Bill & Ship');
						parent.document.all.Cbo_OrdType.value = 2521;
						Error_Show();
						Error_Clear();
						
						document.frmPhOrder.Cbo_ShipCarr.disabled = false;
						document.frmPhOrder.Cbo_ShipMode.disabled = false;
						document.frmPhOrder.Cbo_ShipTo.disabled = false;
						document.frmPhOrder.Cbo_Rep.disabled = false;
						document.frmPhOrder.Cbo_ShipCarr.value = '5001';
						document.frmPhOrder.Cbo_ShipMode.value = '5004';
						document.frmPhOrder.Cbo_ShipTo.value = '4121';
						varRepId = document.frmPhOrder.hRepId.value;
						document.frmPhOrder.Cbo_Rep.value = varRepId;
						return false ;
					}
				}
			}

			if (obj)
			{
				obj.value = '50301';
				obj.disabled = true;
			}
			boobj = eval("document.frmCart.Chk_BO"+i);
			if (boobj)
			{
				boobj.checked = false;
				boobj.disabled = true;
			}
			
		}
    }

}

var refIdCnt = 0;
function fnChkValidFieldVal(obj,cnt)
{
	var pnumObj = eval("document.frmCart.Txt_PNum"+cnt);
	var typeObj = eval("document.frmCart.Cbo_OrdPartType"+cnt);
	var refId = obj.value;
	refIdCnt = cnt;
	if (refId.length > 0)
	{	
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/GmCartAjaxServlet?refId='+refId+'&ordType='+typeObj.value+'&pnum='+encodeURIComponent(pnumObj.value)+'&ramdomId='+Math.random());
		dhtmlxAjax.get(ajaxUrl,fnGetValidData);
	}
}

function fnGetValidData(loader)
{
	var response = TRIM(loader.xmlDoc.responseText);
	if (response == null || response.length == 0)
	{
		var refIdObj = eval("document.frmCart.Txt_RefId"+refIdCnt);
		refIdObj.value = 'Not Valid';		
	}
}

function fnCreateAllParts()
{
	var varRowCnt = document.frmCart.hRowCnt.value; 
	var obj;
	var str = '';;
	var token = ','; 
	var inputstr = '';

	for (k=0; k < varRowCnt; k ++)
	{
		obj = eval("document.frmCart.Txt_PNum"+k);
		if (trimString(obj.value) != '')
		{
			str = trimString(obj.value)  + token; 
	 	 
			inputstr = inputstr + str;
		}
	}
	 
	return inputstr;
}


// Trims the leading and trailing spaces
function trim(str)
{
	str = new String(str);
	var intLen = str.length;
	var intBegin = 0;
	var intEnd = intLen - 1;
	while ((str.charCodeAt(intBegin) == 13 || str.charCodeAt(intBegin) == 10 || str.charAt(intBegin) == " ") && intBegin < intLen)
	{
		if (str.charCodeAt(intBegin) == 13 || str.charCodeAt(intBegin) == 10 )
			intBegin+=2;
		else
        		intBegin++;
	}
	while ((str.charCodeAt(intEnd) == 13 || str.charCodeAt(intBegin) == 10 || str.charAt(intEnd) == " ") && intBegin < intEnd)
	{
		if (str.charCodeAt(intEnd) == 13 || str.charCodeAt(intBegin) == 10)
			intEnd-=2;
		else
			intEnd--;
	}
	return str.substring(intBegin, intEnd+1);
}

//MNTTASK-8623 - I have added function for Add new col Conrol Number and getting data to DB.
function fnCreateCtrlNumString(){
		var varRowCnt = document.frmCart.hRowCnt.value;
		var k;
		var obj;
		var str;
		var token = '^';
		var cnum = '';
		var inputstr = '';
		var objCnumNeedFl = '';
		var v_attr_type = '101723';//The Attribute Type is Added for MDO-109. The control number attribute type need to be saved in Order item Attribute Table when Order raised from portal.

		for (k=0; k < varRowCnt; k ++){
			cnum = eval("document.frmCart.Txt_Cnum"+k);
			obj = eval("document.frmCart.Txt_PNum"+k);
			objCnumNeedFl = eval("document.frmCart.hCntrlNumberNeededFl"+k);
			if(obj != undefined && cnum != undefined && objCnumNeedFl != undefined){
				if (trimString(cnum.value) !='' && trimString(obj.value) != '' && objCnumNeedFl.value == 'Y'){
					str = token + token + trimString(obj.value) + token;
					obj = eval("document.frmCart.Txt_Qty"+k);
					str = str + trimString(obj.value) + token;
					//The Existing I/P String is Changed for MDO-109.Additional Parameter is added for  attribute Type.
					str = str + trimString(cnum.value).toUpperCase() +token+v_attr_type+ '|';
					inputstr = inputstr + str;
				}
			}
		}
		return inputstr;
}
// When click place order, if it entered invalid control number , throw validation.
function fnCreateAllPartsWithCtrlNums()
{
	var varRowCnt = document.frmCart.hRowCnt.value; 
	var obj;
	var str = '';
	var token = ','; 
	var inputstr = '';
	var ObjCnum;
	for (k=0; k < varRowCnt; k ++)
	{
		obj = eval("document.frmCart.Txt_PNum"+k);
		ObjCnum = eval("document.frmCart.Txt_Cnum"+k);
		if(obj != undefined && ObjCnum != undefined){
			if (trimString(obj.value) != '') //&& ObjCnum.value != '' 
			{
				str = trimString(obj.value)  + token + trimString(ObjCnum.value.toUpperCase()) + '|'; 
				inputstr = inputstr + str;
			}
		}
	}
	return inputstr;
}
// When tab out in control number textbox, controlnumber needed flag is 'Y', then call rule engine fire in new order screen in bottom.
function fnValidateCtrlNum(trcnt,obj){	
	var pnumobj = eval("document.frmCart.Txt_PNum"+trcnt);
	var cnumobj = eval("document.frmCart.Txt_Cnum"+trcnt);
	var objCnumNeedFl = eval("document.frmCart.hCntrlNumberNeededFl"+trcnt);
	var CntrlNumNeededFl = objCnumNeedFl.value;
	if(CntrlNumNeededFl == 'Y' && trimString(cnumobj.value) != '')
		window.parent.fnValidateCtrlNumber(trimString(pnumobj.value),trimString(cnumobj.value),trcnt);
}
// If Control number needed flag is 'N', should not throw rule When enter control number in new order screen.
function fnCreateCtrlNumNeededStr(){
	var varRowCnt = document.frmCart.hRowCnt.value; 
	var obj;
	var inputstr = '';
	var ObjCnum;
	for (k=0; k < varRowCnt; k ++){
		obj = eval("document.frmCart.Txt_PNum"+k);
		ObjCnum = eval("document.frmCart.Txt_Cnum"+k);
		var objCnumNeedFl = eval("document.frmCart.hCntrlNumberNeededFl"+k);
		if(obj != undefined && ObjCnum != undefined && objCnumNeedFl != undefined){
			var CntrlNumNeededFl = objCnumNeedFl.value;
			if (trimString(obj.value) != '' && trimString(ObjCnum.value) !='' && CntrlNumNeededFl == 'N'){
					inputstr = inputstr + trimString(obj.value) + ',';  
			}
		}
	}
	return inputstr;
}
// When no entor control number for sterile parts, then create input string and throw confirm message.
function fnCreateWithoutCtrlNumStr(){
	var varRowCnt = document.frmCart.hRowCnt.value; 
	var obj;
	var str = '';;
	var inputstr = '';
	var ObjCnum;
	var objCnumNeedFl = '';
	for (k=0; k < varRowCnt; k ++)
	{
		obj = eval("document.frmCart.Txt_PNum"+k);
		ObjCnum = eval("document.frmCart.Txt_Cnum"+k);
		objCnumNeedFl = eval("document.frmCart.hCntrlNumberNeededFl"+k);
		var CntrlNumNeededFl = (objCnumNeedFl!= undefined)?objCnumNeedFl.value:'N';
		if (trimString(obj.value) != ''){
			if(trimString((ObjCnum!= undefined)?ObjCnum.value:'') == '' && trimString((obj!= undefined)?obj.value:'') != '' && CntrlNumNeededFl == 'Y')
				inputstr = inputstr + trimString(obj.value) + ',';  
		}
	}
	return inputstr;
}


function fnCreateAllPartsWithCtrlNumsValid()
{
	var varRowCnt = document.frmCart.hRowCnt.value; 
	var obj;
	var str = '';
	var token = ','; 
	var inputstr = '';
	var ObjCnum;
	for (k=0; k < varRowCnt; k ++)
	{
		obj = eval("document.frmCart.Txt_PNum"+k);
		ObjCnum = eval("document.frmCart.Txt_Cnum"+k);
		var objCnumNeedFl = eval("document.frmCart.hCntrlNumberNeededFl"+k);
		var CntrlNumNeededFl = objCnumNeedFl.value;
		
		if (trimString(obj.value) != '' && CntrlNumNeededFl == 'Y') 
		{
			ObjCnumVal = trimString(ObjCnum.value.toUpperCase());
			str = trimString(obj.value)  + token + ObjCnumVal + '|'; 
			inputstr = inputstr + str;
		}
	}
	return inputstr;
}

// MNTTASK- 8623 When Enter invalid or valid control number, show image.  
function fnRemoveImage(index)
{
		var imgObj = document.getElementById("DivShowCnumAvl"+index);
		imgObj.style.display='none';

		var imgObj1 = document.getElementById("DivShowCnumNotExists"+index);			
		imgObj1.style.display='none';			
}

//MNTTASK-8623 - When create order enter part number with space, getting exception, Use trim function.
function trimString(obj) 
{ 
if(obj != undefined && obj != '') 
{ 
	return trim(obj); 
} 
return obj; 
} 
//adding methods for US and OUS code migration - this will used for apply discount 
function fnCalculateDiscount()
{
	blDiscFlag=true;
	var objDiscountPercentage = fnDiscountPercentage();
	var cnt = document.frmCart.hRowCnt.value;
	var strOpt = parent.document.all.hOpt.value;
	var listPrice = 0;
	for (i=0;i<cnt;i++)
	{
		pobj = eval("document.frmCart.Txt_PNum"+i);
		//var eaPrice = parseFloat(RemoveComma(priceObj.value));
		if(pobj.value != '')
		{
			
			// MNTTASK-4744 - Discount Calculation : Added Price EA column in the UI. Always we have to get txt_price value to DB.
			if(strOpt != 'QUOTE'){
				priceEAObj = eval("document.frmCart.hBasePrice"+i);
				listPrice= RemoveComma(eval("document.frmCart.hBasePrice"+i).value);
			}else{
				priceEAObj = eval("document.frmCart.Txt_BasePrice"+i);
				listPrice= RemoveComma(eval("document.frmCart.Txt_BasePrice"+i).value);
			}
			var DisEAPRice = (parseFloat(listPrice) - (parseFloat(listPrice) * objDiscountPercentage));
			var obj = eval("document.all.Txt_Price"+i);
			obj.value = formatNumber(DisEAPRice);
			var hprice = eval("document.all.Lbl_PriceEA"+i)
			if(hprice != undefined && hprice != '') 
				hprice.value = formatNumber(DisEAPRice);
			fnCalExtPrice(priceEAObj,i);
		}
	}
	
}

function fnDiscountPercentage(){
	var objDiscountVal = 0;
	var objDiscountPercentage = 0.00;
	if(blDiscFlag){
		var  strDiscountIndex = parent.document.all.hDiscountIndex.value;
		objDiscountVal = eval("parent.document.frmPhOrder.Txt_Quote"+strDiscountIndex).value ;
		objDiscountVal =(objDiscountVal == '')?0:objDiscountVal;
		var AccDiscount = (parent.document.all.hAccDiscnt != undefined)?parent.document.all.hAccDiscnt.value:0;
		objDiscountVal = parseFloat(formatNumber(objDiscountVal)) + parseFloat(formatNumber(AccDiscount));
		objDiscountPercentage = trim(objDiscountVal)!=''?(parseFloat(objDiscountVal)/100):parseFloat(0);
	}
	return objDiscountPercentage;
}

// This function is to reset the Price EA column in New Order screen to the base price on blur of Discount
function fnResetBasePrice(){
	var basePrice;
	var cnt = document.frmCart.hRowCnt.value;
	for (i=0;i<cnt;i++)
	{
		pobj = eval("document.frmCart.Txt_PNum"+i);
		
		if(pobj.value != '')
		{
				
			obj = eval("document.all.hBasePrice"+i);
			basePrice = obj.value; 
			
		    obj = eval("document.frmCart.Txt_Price"+i);
		   	obj.value = basePrice;
		}
	}
}