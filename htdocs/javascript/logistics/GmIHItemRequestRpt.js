var checkInValidTag=false;
function initGridData(divRef, gridData) {
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);
	gObj.enableTooltips("true,true,true,true,true,true");
	gObj.init();
	gObj.loadXMLString(gridData);
	return gObj;
}

// Description: This function used for initializing the filter values gridData.
function fnOnPageLoad() {
	var statsids = document.frmIHItemRequest.hStatus.value;
	var reqType = document.frmIHItemRequest.reqtype.value;
	var purpose = document.frmIHItemRequest.purpose.value;
	fnChange();
	if (statsids != '') {
		var objgrp = document.frmIHItemRequest.Chk_Grpstatus;
		fnCheckSelections(statsids, objgrp);
	}
	gridObj = initGridData('GridData', objGridData);
}

function fnEnter() {
	if (event.keyCode == 13) {
		fnLoad();
	}
}
// Description: This function will call onclick load button. check the
// validation & pass control to action.
function fnLoad() {

	objRequestType = document.frmIHItemRequest.reqtype.value;
	objPurpose = document.frmIHItemRequest.purpose.value;
	objFromDate = document.frmIHItemRequest.fromDt;
	objToDate = document.frmIHItemRequest.toDt;
	//objStatus = document.frmIHItemRequest.status.value;
		var objStatus = document.frmIHItemRequest.Chk_Grpstatus;
		var str = '';
		var len = 0;
		if (objStatus) {
			len = objStatus.length;
		}
		if (objStatus) {
			for ( var i = 0; i < len; i++) {
				if (objStatus[i].checked == true) {
					str += objStatus[i].value + ',';
				}
			}
			if(str != '')
				document.frmIHItemRequest.hStatus.value = str.substr(0, str.length - 1);
			else
				document.frmIHItemRequest.hStatus.value = "";
		}
	if (objFromDate.value != "" && objToDate.value == "") {
		Error_Details(message_operations[537]);
	}
	if (objToDate.value != "" && objFromDate.value == "") {
		Error_Details(message_operations[538]);
	}
	if (objFromDate.value != "") {
		CommonDateValidation(objFromDate,format, Error_Details_Trans(message_operations[539],format));
	}
	if (objToDate.value != "") {
		CommonDateValidation(objToDate,format, Error_Details_Trans(message_operations[540],format));
	}
	if (dateDiff(objFromDate.value,objToDate.value,format) < 0) {
		Error_Details(message_operations[541]);
	}
	if (objRequestType == '0' && objPurpose == '0' && objFromDate.value == ""
			&& objToDate.value == "" && str == "") {
		Error_Details(message_operations[542]);
	}
	if(checkInValidTag){
		document.frmIHItemRequest.strTagID.value ='';
	}

	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	
	document.frmIHItemRequest.strOpt.value = "Load"
	document.frmIHItemRequest.action = "/gmIHItemRequest.do";
	fnStartProgress('Y');
	document.frmIHItemRequest.submit();
}

function fnSetReqID(obj, reqid) {
	document.frmIHItemRequest.requestID.value = obj.value;
	document.frmIHItemRequest.requestTypeID.value = reqid;
}

function fnSubmit() {
	var Errorcount = 0;
	var reqId = document.frmIHItemRequest.requestID.value;
	var reqTypeId = document.frmIHItemRequest.requestTypeID.value;
	var val = eval(document.frmIHItemRequest.chooseAction.value);
	if (reqId == '') {
		Error_Details(message_operations[543]);
	}
	if ((reqId != '') && (val == '0')) {
		Error_Details(message_operations[544]);
	}
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	document.frmIHItemRequest.requestID.value = "";
	if (val == '100451' && reqId != '') { // Approved Request
		document.frmIHItemRequest.action = '/gmIHItemRequest.do?strOpt=ReconfigReq&requestID='
				+ reqId + '&requestTypeID=' + reqTypeId;
		document.frmIHItemRequest.submit();
	} else if (val == '100452' && reqId != '') { // Void Request
		if (reqTypeId == '1006483' || reqTypeId == '1006572') {
			vCancelType = 'RBIHLN';
		}
		document.frmIHItemRequest.action = "/GmCommonCancelServlet?hTxnName="
				+ reqId + "&hCancelType=" + vCancelType + "&hTxnId=" + reqId;
		fnStartProgress('Y');
		document.frmIHItemRequest.submit();
	}
}

// Description: This function will used to generate excel.
function fnDownloadXLS() {
	gridObj.toExcel('/phpapp/excel/generate.php');
}

function fnOpenCmtLog(id) {
	windowOpener("/GmCommonLogServlet?hType=1288&hID=" + id, "PrntInv",
			"resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
}
// Marks the checkboxes as 'checked'
function fnCheckSelections(val, objgrp) {
	var valobj = val.split(",");
	var sval = '';
	var arrlen = objgrp.length;

	if (arrlen > 0) {
		for ( var j = 0; j < valobj.length; j++) {
			sval = valobj[j];
			for ( var i = 0; i < arrlen; i++) {
				if (objgrp[i].value == sval) {
					objgrp[i].checked = true;
				}
			}
		}
	} else {
		objgrp.checked = true;
	}
}
//this function is used to validate the tag id
function fnValidateTag(tagId){
	tagId=tagId.replace(/(^\s+|\s+$)/g, '');
	if(tagId!=''){
	var ajaxUrl = fnAjaxURLAppendCmpInfo("/gmValdiateTagId.do?tagId="+tagId);
	dhtmlxAjax.get(ajaxUrl,function(loader)
			{
				var response = loader.xmlDoc.responseText;
				if(response == '1'){
					document.getElementById('DivShowTagAvl').style.display='inline-block';
					document.getElementById('DivShowTagNotExists').style.display='none';

				}else{
					document.getElementById('DivShowTagNotExists').style.display='inline-block';
					document.getElementById('DivShowTagAvl').style.display='none';
					checkInValidTag=true;

				}
			});
	}
}

//this function is used  to open tag report in new window

function fnTagLookup (){
	
	windowOpener("/GmPageControllerServlet?strPgToLoad=gmTagReport.do&strOpt=Load","TAGRPT","resizable=yes,scrollbars=yes,top=300,left=300,width=1100,height=400");
	
}

//this function is used  to hide validation on click

function fnToggleTag (){
	document.getElementById('DivShowTagAvl').style.display='none';
	document.getElementById('DivShowTagNotExists').style.display='none';
}

function fnChange(){
	var reqType = document.frmIHItemRequest.reqtype.value;
	var purpose = document.frmIHItemRequest.purpose.value;

	if(reqType == '1006483' && purpose =='102787'){
		document.getElementById("hideContId").style.display='block';
		document.getElementById("hideTagContId").style.display='block';
		document.getElementById("hideLoadBtn").innerHTML='';
	}
	else
		{
		document.getElementById("hideContId").style.display='none';
		document.getElementById("hideTagContId").style.display='none';
		document.getElementById("hideLoadBtn").innerHTML='<INPUT onclick=" fnLoad();" onmousedown="" class=Button accessKey="" type=button value=&nbsp;Load&nbsp; name=Btn_Submit>';
		document.frmIHItemRequest.strTagID.value='';
		document.getElementById('DivShowTagNotExists').style.display='none';
		document.getElementById('DivShowTagAvl').style.display='none';
		}
}
