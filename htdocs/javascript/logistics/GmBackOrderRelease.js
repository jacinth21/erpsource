var gridObj;
function fnOnLoad(){
	switchUserFl = document.frmBackOrderRelease.switchUserFl.value;
	if(switchUserFl == 'Y')
			{
			document.all.btnReleaseBO.disabled = true;
			document.all.btnUpdatetoBillOnly.disabled = true;
			}
	var initialCheckFull = fullQtyCheck;
	if(initialCheckFull == "yes"){
		document.frmBackOrderRelease.chkFullQty.checked=true;
	}
	if (objGridData.value!= '') {
		gridObj = setDefalutGridProperties('dataGridDiv');
		gridObj.enableMultiline(false);	
		//gridObj.enableDistributedParsing(true);
		gridObj.setSkin("dhx_skyblue");
		gridObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
        gridObj.attachHeader('#rspan,#rspan,#text_filter,#text_filter,#rspan,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#rspan,#rspan,#text_filter');
		gridObj = initializeDefaultGrid(gridObj,objGridData);
		gridObj.groupBy(2,["","","#title","","","","","","#stat_total","#stat_max","","","","","","","",""]);
		ChkboxRefId = gridObj.getColIndexById("ChkBoxRefId");
		partNos = gridObj.getColIndexById("partNo");
		Order_id = gridObj.getColIndexById("OrderId");
		Qty_to_release = gridObj.getColIndexById("qtyToRelease"); 
		bopriority = gridObj.getColIndexById("BoPriority");
		boQty = gridObj.getColIndexById("boQty");
		fgQty = gridObj.getColIndexById("fgQty"); 
		qtyToReleaseFull = gridObj.getColIndexById("qtyToReleaseFull");
		showImg = gridObj.getColIndexById("showImg"); 
		QtyReleaseTxtBox = gridObj.getColIndexById("QtyReleaseTxtBox");
		
		//gridObj.attachEvent("onEditCell", doOnCellEdit);
		gridObj.attachEvent("onCheck", doOnChecks);
		fnSetBOPriorityColor();
	} 
}


function doOnChecks(rowId, cellInd, state) {
	var checkedRowID = '';
	checkedRowID = gridObj.getCheckedRows(ChkboxRefId);
	if(checkedRowID == '' && checkedRowID != undefined){
		document.all.btnReleaseBO.disabled = false;
	}
	 gridObj.forEachRow(function(rowId) {
		 check_id = gridObj.cellById(rowId, ChkboxRefId).getValue(); 
		 if(check_id == 1) {
				BO_Prioirty = gridObj.cellById(rowId, bopriority).getValue();
				if(BO_Prioirty == '3'){
					document.all.btnReleaseBO.disabled = true;
				}
				else{
					document.all.btnReleaseBO.disabled = false;
		       }
			}
		 });
	 switchUserFl = document.frmBackOrderRelease.switchUserFl.value;
		if(switchUserFl == 'Y')
				{
				document.all.btnReleaseBO.disabled = true;
				document.all.btnUpdatetoBillOnly.disabled = true;
				}
}

function fnSetBOPriorityColor(){
	gridObj.forEachRow(function(rowId) {
		BO_Prioirty = gridObj.cellById(rowId, bopriority).getValue();
		if(BO_Prioirty=='1'){
			gridObj.setRowTextStyle(rowId, "background-color: rgb(198,239,206);");
			gridObj.setCellTextStyle(rowId,9,"color:rgb(198,239,206);background-color: rgb(198,239,206);"); //to make the FG qty not appear on line items
		}
		if(BO_Prioirty=='2'){
			gridObj.setRowTextStyle(rowId, "background-color: rgb(255,235,156);");
			gridObj.setCellTextStyle(rowId,9,"color:rgb(255,235,156);background-color: rgb(255,235,156);"); //to make the FG qty not appear on line items
		}
		if(BO_Prioirty=='3'){
			gridObj.setRowTextStyle(rowId, "background-color: rgb(255,199,206);");
			gridObj.setCellTextStyle(rowId,9,"color:rgb(255,199,206);background-color: rgb(255,199,206);");//to make the FG qty not appear on line items
		}
	});
}

function doOnCellEdit(stage,rowId,cellInd,newVal,oldVal)
{
	if(stage==0){
		return true;
		}
		if (stage == 1) {
		return true;
		}
	if (stage == 2) {
		return true;
	}
}


function fnLoad()
{    
	 if(document.frmBackOrderRelease.chkFullQty.checked == false && document.frmBackOrderRelease.chkPartialQty.checked == false && document.frmBackOrderRelease.chkFGQty.checked == false){
		Error_Details(message_operations[775]); 
	 }
	 if (ErrorCount > 0){
			Error_Show();
			Error_Clear();
			return false;
		}
	fnStartProgress('Y');
	document.frmBackOrderRelease.submit();
}

function fnChangedFilter(obj){
	if(obj.checked){		
		gridObj.forEachRow(function(rowId) {
			BO_Prioirty = gridObj.cellById(rowId, bopriority).getValue();//new
			if(BO_Prioirty == '1'){
				check_id = gridObj.cellById(rowId, 0).getValue();
				gridObj.cellById(rowId, 0).setChecked(true);
			}
	   });
	}else{
		document.all.btnReleaseBO.disabled = false;
		gridObj.forEachRow(function(rowId) {
			check_id = gridObj.cellById(rowId, 0).getValue();
			if(check_id == 1){
				gridObj.cellById(rowId, 0).setChecked(false);
			}
	   });			
	}
	 switchUserFl = document.frmBackOrderRelease.switchUserFl.value;
		if(switchUserFl == 'Y')
				{
				document.all.btnReleaseBO.disabled = true;
				document.all.btnUpdatetoBillOnly.disabled = true;
				}
}

function fnSetQtyValue(obj,refid,pnum){
	gridObj.forEachRow(function(rowId) {
				Ref_id = gridObj.cellById(rowId, Order_id).getValue();
				part_num = gridObj.cellById(rowId, partNos).getValue();
				if(Ref_id == refid && pnum == part_num){
					gridObj.cellById(rowId, Qty_to_release).setValue(obj);
				}
	});
}


/*fnReleaseBackOrder(): function to release backorder
 */
function fnReleaseBackOrder() {	
	var checkedRowID = '';
	checkedRowID = gridObj.getCheckedRows(ChkboxRefId);
	if(checkedRowID == '' && checkedRowID != undefined){
		Error_Details(message_operations[249]);
	}
	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}

	gridObj.forEachRow(function(rowId) {
		check_id = gridObj.cellById(rowId, ChkboxRefId).getValue();
		var releaseToQty = '';
		if(check_id == 1) {
			Ref_id = gridObj.cellById(rowId, Order_id).getValue();
			Release_qty = gridObj.cellById(rowId, Qty_to_release).getValue(); 
			Release_full_qty = gridObj.cellById(rowId, qtyToReleaseFull).getValue(); 
			pnum = gridObj.cellById(rowId, partNos).getValue(); 
				if(Release_qty.indexOf("span") !='-1'){
					Release_qty = Release_full_qty;
				}
			releaseToQty = Release_qty
			var ajaxUrl = fnAjaxURLAppendCmpInfo('gmBackOrderReleaseAction.do?method=releaseBackOrderDetails&hOrderId=' + encodeURIComponent(Ref_id) 
			 + '&strSessUserId=' + encodeURIComponent(sessionUserId) + '&pnum=' + encodeURIComponent(pnum) +'&releaseQty=' + encodeURIComponent(Release_qty) +'&ramdomId=' + Math.random() +"&"+ fnAppendCompanyInfo());
			fnStartProgress('Y');

			dhtmlxAjax.get(ajaxUrl, function(loader){
			     response = loader.xmlDoc.responseText;
			    if (response == '' ){
			    	gridObj.cellById(rowId,showImg).setValue("<img  title='Success' height='24' width='25' src='/images/success.gif'/ border='0'>");
			    	gridObj.cellById(rowId,QtyReleaseTxtBox).setValue(releaseToQty);
			    	gridObj.cellById(rowId,0).setDisabled(true); //To make the checkbox disabled after Releasing an order
			    	gridObj.setRowTextStyle(rowId, "background-color:rgb(182, 175, 175);"); //To highlight the entire saved Row
			    	gridObj.setCellTextStyle(rowId,9,"color:rgb(182, 175, 175);background-color: rgb(182, 175, 175);"); //to make the FG qty not appear on line items after the entire row is highlighted(PBUG-2989)
			    	fnStopProgress('Y');
			    	fnMouseOver(gridObj);
			    	return true;
			    }
			    else{
			    	gridObj.cellById(rowId,showImg).setValue("<img id='fail"+rowId+"' title='"+response+"' alt='Failure' src='/images/delete.gif'/ border='0'>");
			    	fnMouseOver(gridObj);
			    	fnStopProgress('Y');
			    }
		    });
				gridObj.cellById(rowId, 0).setChecked(false);
		} 
    });
}

function fnMouseOver(gridObj){
	gridObj.attachEvent("onMouseOver",function()
		{
			return false;
		});
}


function fnViewOrder(val)
{
windowOpener('/GmEditOrderServlet?hMode=PrintPack&hOrdId='+val,"Pack","resizable=yes,scrollbars=yes,top=150,left=150,width=760,height=610");
	
}

function fnOpenOrdLog(id)
{
	windowOpener("/GmCommonLogServlet?hType=1233&hID="+id,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
}


function fnOpenOrdLogInv(id)
{
	windowOpener("/GmCommonLogServlet?hType=1234&hID="+id,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
}


function fnOverallLookup(){
	var checkedRowID = '';
	var pnums = '';
	checkedRowID = gridObj.getCheckedRows(ChkboxRefId);
	if(checkedRowID == '' && checkedRowID != undefined){
		Error_Details(message_operations[251]);
	}
	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}
	var str = ''
	gridObj.forEachRow(function(rowId) {
		check_id = gridObj.cellById(rowId, ChkboxRefId).getValue();
		if(check_id == 1) {
			partnum = gridObj.cellById(rowId, 2).getValue();
			str = str + partnum + ",";
		 }
	 });
	if(str != undefined && str !=''){
		 pnums = fnRemoveDuplicateValues(str);	
	}
	
   windowOpener('/GmPartInvReportServlet?hAction=Go&hOpt=MultipleParts&hScreenType=BACKORDER&hPartNum='+encodeURIComponent(pnums),"Pack","resizable=yes,scrollbars=yes,top=250,left=250,width=960,height=510");
}

function fnLookup(partNm)
{
	var strPartNm ='';
	if(partNm != undefined && partNm !=''){
		strPartNm = partNm+",";
	}
	if(strPartNm == ''){
		Error_Details(message_operations[251]);
	}
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	windowOpener('/GmPartInvReportServlet?hAction=Go&hOpt=MultipleParts&hScreenType=BACKORDER&hPartNum='+encodeURIComponent(strPartNm),"Pack","resizable=yes,scrollbars=yes,top=250,left=250,width=960,height=510");
}

function fnloadPartFulfillReport()
{
	fnStartProgress('Y');
	var val= sales;
	var str = hpnum;
	str = str.substr(0,str.length-1);
	windowOpener("/gmLogisticsReport.do?method=loadPartFulfillReport&BackOrderType="+val+"&hPartNum="+encodeURIComponent(str),"Pack","resizable=yes,scrollbars=yes,top=250,left=250,width=1020,height=600");
    fnStopProgress();
}

function fnDownloadXLS()
{  
    gridObj.setColumnHidden(ChkboxRefId,true);//hiding checkbox
	gridObj.setColumnHidden(1,true);//hiding lookup icon
	gridObj.setColumnHidden(16,true);//hiding success.gif
    gridObj.toExcel('/phpapp/excel/generate.php');
	gridObj.setColumnHidden(ChkboxRefId,false);
	gridObj.setColumnHidden(1,false);
	gridObj.setColumnHidden(16,false);
	}

// This function used to dropdown value choosed then submit value to action
function fnSubmit(){
	var vBillAction = document.frmBackOrderRelease.strUpdateBillAction.value;
	if(vBillAction != ''){
		if(vBillAction =='billonly'){   //When choose on 109742 - 'Update to Bill Only' value
			var checkedRowID = '';
			var order_id = '';
			var check_id ='';
			checkedRowID = gridObj.getCheckedRows(ChkboxRefId);
			if(checkedRowID == '' && checkedRowID != undefined){
				Error_Details(message_operations[252]);
			}
			checkedRowID = gridObj.getCheckedRows(ChkboxRefId);
			var arr=checkedRowID.split(",");
			var checkedRows=arr.length; //number of selected rows
			if(checkedRows >1){
			Error_Details(message_operations[776]);
			}
			if (ErrorCount > 0){
				Error_Show();
				Error_Clear();
				return false;
			}
			   gridObj.forEachRow(function(rowId) {
					check_id = gridObj.cellById(rowId, ChkboxRefId).getValue();
					if(check_id == 1) {
						Ref_id = gridObj.cellById(rowId, Order_id).getValue();
					}
				});
				    fnStartProgress();
				    
		            document.frmBackOrderRelease.action = "/GmOrderItemServlet?hAction=UpdateToBillOnly&hOrderId="+Ref_id;
		            document.frmBackOrderRelease.submit();
			
		}else if(vBillAction =='billonlyrtn'){     //When choose on 109743 -'Update to Bill Only Return' value
			var checkedRowID = '';
			var order_id = '';
			var check_id ='';
			checkedRowID = gridObj.getCheckedRows(ChkboxRefId);
			if(checkedRowID == '' && checkedRowID != undefined){
				Error_Details(message_operations[252]);
			}
			checkedRowID = gridObj.getCheckedRows(ChkboxRefId);
			var arr=checkedRowID.split(",");
			var checkedRows=arr.length; //number of selected rows
			if(checkedRows >1){
			Error_Details(message_operations[776]);
			}
			if (ErrorCount > 0){
				Error_Show();
				Error_Clear();
				return false;
			}
			   gridObj.forEachRow(function(rowId) {
					check_id = gridObj.cellById(rowId, ChkboxRefId).getValue();
					if(check_id == 1) {
						Ref_id = gridObj.cellById(rowId, Order_id).getValue();
					}
				});
				    fnStartProgress();
				   document.frmBackOrderRelease.action = '/GmCommonCancelServlet?hTxnId='+Ref_id+'&hCancelType=UPBLR&hAction=Load&hCancelReturnVal=true';
				   document.frmBackOrderRelease.submit();
			
		}else{
			Error_Details("Please choose a <b>Update Bill Action</b> ");
			if (ErrorCount > 0) {
				Error_Show();
				Error_Clear();
				return;
			}
		}
		
	}
}

function fnRemoveDuplicateValues(inValue) {
	var items = inValue.split(",");
	var arrlen = items.length - 1;
	var i = 0;
	var outValue = "";
	var prevItem;
	while (i <= arrlen) {
		if (outValue.length > 0) {
			if (prevItem != items[i].trim()) {
				var str = items[i].trim();
				outValue = outValue.replace(prevItem + ",", "");
				outValue += "," + str;
			}
		} else {
			outValue = items[i].trim();
		}
		prevItem = items[i].trim();
		i++;
	}
	return outValue;
}

