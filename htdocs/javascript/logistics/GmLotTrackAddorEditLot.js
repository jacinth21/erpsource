function fnOnload(){
	if(strAccess != "Y"){
		document.frmLotTrackNonTissueRpt.button_submit.disabled = true;
	}
	var consignedTo = document.frmLotTrackNonTissueRpt.warehouseType.value;
	fnChangeConsignee();
}

//to change the Consignee Name dropdown values based on the Warehouse type
function fnChangeConsignee(){
	var consignedTo = document.frmLotTrackNonTissueRpt.warehouseType.value;
	if(consignedTo == '4000339'){// 4000339: Field sales Qty[warehouse dropdown]
		var objConsigneeBlk = eval('document.all.consignee_blk.style');
  		objConsigneeBlk.display = 'block';
  		var objAccountBlk = eval('document.all.account_blk.style');
  		objAccountBlk.display = 'none';
  		var objChooseOneBlk = eval('document.all.chooseOne_blk.style');
  		objChooseOneBlk.display = 'none';  		
	}else if(consignedTo == '56002'){// 56002: Account Qty [warehouse dropdown]
		var objConsigneeBlk = eval('document.all.consignee_blk.style');
  		objConsigneeBlk.display = 'none';
  		var objAccountBlk = eval('document.all.account_blk.style');
  		objAccountBlk.display = 'block';
  		var objChooseOneBlk = eval('document.all.chooseOne_blk.style');
  		objChooseOneBlk.display = 'none';
	}else{
		var objConsigneeBlk = eval('document.all.consignee_blk.style');
  		objConsigneeBlk.display = 'none';
  		var objAccountBlk = eval('document.all.account_blk.style');
  		objAccountBlk.display = 'none';
  		var objChooseOneBlk = eval('document.all.chooseOne_blk.style');
  		objChooseOneBlk.display = 'block'; 		
	}
	fnResetFields(consignedTo); // To reset the dropdown
}

// to reset the fields other than selected fields
function fnResetFields(consignedTo){
		if(consignedTo != '4000339'){
  			document.frmLotTrackNonTissueRpt.fieldSalesNm.value = '0';
  		}
  		if(consignedTo != '56002'){
  			document.frmLotTrackNonTissueRpt.accountNm.value = '0';
  		}
}

//This function used to get Plant Lists
function fnGetPlantList(obj) {
	var compnyId = obj.value;
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmLotTrackNonTissueRpt.do?method=addOrEditLotTrackDetails&strOpt=fetchCompanyPlants&companyNm=' + TRIM(compnyId) +'&ramdomId=' + Math.random());
	dhtmlxAjax.get(ajaxUrl, fnSetPlantDetails);
}

//Description : This function used to load the Plant Drop down details based on the COmpany ID Selection
function fnSetPlantDetails(loader) {
	var response = loader.xmlDoc.responseXML;	
	var plantDetails = response.getElementsByTagName("pnum");
	var defaultMode = response.getElementsByTagName("defplant");
	document.frmLotTrackNonTissueRpt.plantNm.options.length = 0;
	document.frmLotTrackNonTissueRpt.plantNm.options[0] = new Option("[Choose One]","0");
	if(plantDetails.length == 0){
		for (var i=0;i<PlantLength;i++){
			arr = eval('AllPlantArr'+i);
			arrobj = arr.split(",");
			id = arrobj[0];
			name = arrobj[1];
			document.frmLotTrackNonTissueRpt.plantNm.options[i+1] = new Option(name,id);
		}
		document.frmLotTrackNonTissueRpt.plantNm.value = '0';
	}else{
		for (var x=0; x<plantDetails.length; x++){
		   	strPlantID = parseXmlNode(plantDetails[x].childNodes[0].firstChild);
		   	strPlantName = parseXmlNode(plantDetails[x].childNodes[1].firstChild);
		   	document.frmLotTrackNonTissueRpt.plantNm.options[x+1] = new Option(strPlantName,strPlantID);	   	
		}	
		var defaultMode = parseXmlNode(response.getElementsByTagName("defplant")[0].firstChild);
		document.frmLotTrackNonTissueRpt.plantNm.value = defaultMode;	
	}	
}

function fnSave(){
	var strPartNumber = document.frmLotTrackNonTissueRpt.partNum.value;
	var strLotNumber = document.frmLotTrackNonTissueRpt.lotNum.value;
	var quantity = document.frmLotTrackNonTissueRpt.qty.value;
	var companyId = document.frmLotTrackNonTissueRpt.companyNm.value;
	var plantId = document.frmLotTrackNonTissueRpt.plantNm.value;
	var warehouseType =  document.frmLotTrackNonTissueRpt.warehouseType.value;
	var strCntrlNumInvId = document.frmLotTrackNonTissueRpt.strCntrlNumInvId.value;
	var consignedTo = document.frmLotTrackNonTissueRpt.warehouseType.value;
	if(consignedTo == '4000339'){
		var strLocationId = document.frmLotTrackNonTissueRpt.fieldSalesNm.value;
	}
	else if(consignedTo == '56002'){
		var strLocationId = document.frmLotTrackNonTissueRpt.accountNm.value;
	}
	else {
		var strLocationId = document.frmLotTrackNonTissueRpt.location.value;
	}
		fnValidate();
		fnValidateCompany();
		fnValidateLocation();
		if (ErrorCount > 0) {
	   		Error_Show();
	   		Error_Clear();
	   		return false;
	   		}
		fnStartProgress();
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmLotTrackNonTissueRpt.do?method=addOrEditLotTrackDetails&strOpt=save&strPartNumber='
			+ encodeURIComponent(strPartNumber)
			+ '&strLotNumber='
			+ encodeURIComponent(strLotNumber)
			+ '&quantity='
			+ quantity
			+ '&companyId='
			+ companyId
			+ '&plantId='
			+ plantId
			+ '&warehouseType='
			+ warehouseType
			+ '&strLocationId='
			+ strLocationId
			+ '&strCntrlNumInvId='
			+ strCntrlNumInvId
			+ '&ramdomId='
			+ new Date().getTime());
	dhtmlxAjax.get(ajaxUrl, fnSaveLot);
}

function fnSaveLot(loader) {
	var response = loader.xmlDoc.responseText;
	if(response != ""){
		document.getElementById("successCont").innerHTML = "<font color='green' style='font-weight:bold;'><center>Records Saved Successfully</center></font>";
	}
	fnStopProgress();
}
//Function used to reset the values entered
function fnReset(){
	document.frmLotTrackNonTissueRpt.partNum.value = '';
	document.frmLotTrackNonTissueRpt.lotNum.value = '';
	document.frmLotTrackNonTissueRpt.qty.value = '';
	document.frmLotTrackNonTissueRpt.companyNm.value = '0';
	document.frmLotTrackNonTissueRpt.plantNm.value = '0';
	document.frmLotTrackNonTissueRpt.warehouseType.value = '0';
	document.frmLotTrackNonTissueRpt.fieldSalesNm.value = '0';
	document.frmLotTrackNonTissueRpt.accountNm.value = '0';
	document.frmLotTrackNonTissueRpt.location.value = '';
	document.getElementById("successCont").style.display = 'none';
	document.getElementById("partDesc").innerHTML = '';
	document.getElementById("partDesc").innerHTML = ":";
}

//function used to validate the warehouse and location
function fnValidateLocation(){
	var warehouseType =  document.frmLotTrackNonTissueRpt.warehouseType.value;
	var consignedTo = document.frmLotTrackNonTissueRpt.warehouseType.value;
	var strLocationId = document.frmLotTrackNonTissueRpt.location.value;
	if(warehouseType == '0'){
		Error_Details('Please choose warehouseType to proceed');
	}
	else if(warehouseType != '0'){
		if(consignedTo == '4000339' || consignedTo == '56002'){
			if(strLocationId == '0'){
				Error_Details('Please choose a Location to proceed');
			}
		}
		else if(consignedTo != '4000339' || consignedTo != '56002'){
			if(strLocationId == ""){
			Error_Details('Please enter a valid Location to proceed');
			}
		}
	}
}

//function used to validate part#, qty and lot#
function fnValidate(){
	var strPartNumber = document.frmLotTrackNonTissueRpt.partNum.value;
	var strLotNumber = document.frmLotTrackNonTissueRpt.lotNum.value;
	var quantity = document.frmLotTrackNonTissueRpt.qty.value;
	if(strPartNumber == ""){
		Error_Details('Please Enter Valid Part Number');
	}
	if(strLotNumber == ""){
		Error_Details('Please Enter Valid Lot Number');
	}
	if(quantity == "") {
		Error_Details('Please Enter Valid Quantity');
	}
	if(quantity != ""){
		if(isNaN(quantity) == true) {
			Error_Details('Quantity should be a Number');
		}
	}
}

//function used to validate the company and plant
function fnValidateCompany(){
	var companyId = document.frmLotTrackNonTissueRpt.companyNm.value;
	var plantId = document.frmLotTrackNonTissueRpt.plantNm.value;
	if(companyId == '0'){
		Error_Details('Please choose Company to proceed');
	}
	if(plantId == '0'){
		Error_Details('Please choose Plant to proceed');
	}
}

//This function used to get Part Description
function fnGetPartDesc(obj) 
{
	var pnum = ''; 	
		var pnumobj = eval("document.frmLotTrackNonTissueRpt.Txt_PNum");		
	    changeBgColor(obj,'#ffffff');

		if (pnumobj.value != '')
		{
		  
			   pnum = pnumobj.value.toUpperCase();			 
			   var partdescurl = fnAjaxURLAppendCmpInfo("/GmCartAjaxServlet?hAction=fetchPartDesc&pnum=" + encodeURIComponent(pnum)+"&"+fnAppendCompanyInfo());
			  
			   if (typeof XMLHttpRequest != "undefined") {
				   reqDesc = new XMLHttpRequest();
			   } else if (window.ActiveXObject) {
				   reqDesc = new ActiveXObject("Microsoft.XMLHTTP");
			   }
			   reqDesc.open("GET", partdescurl, false);
			   reqDesc.onreadystatechange = partdesccallback;	
			   reqDesc.send(null);
		}
}

function partdesccallback() 
{
	 if (reqDesc.readyState == 4) {
        if (reqDesc.status == 200) {
	        var pdesc = reqDesc.responseText;
	       var obj = eval("document.all.partDesc");
	        if (pdesc != null && pdesc != '' && pdesc.length > 2)
	    	{
		    	obj.innerHTML = ":" + "&nbsp;" + pdesc;
	        	
	    	}else{	    	
	    		setPartDescErrMessage('<span class=RightTextBlue>'+message[10566]+'</span>');
	    		return;	    	
	    	}
        }
        else
        {
        	setPartDescErrMessage('<span class=RightTextBlue>'+message[10566]+'</span>');
        }
        
    }
}

function setPartDescErrMessage(msg) 
{
    var obj = eval("document.all.partDesc");
    obj.innerHTML = "&nbsp;" + msg;
    obj = eval("document.all.Txt_PNum");
    obj.value = '';
    obj.focus(); 
}