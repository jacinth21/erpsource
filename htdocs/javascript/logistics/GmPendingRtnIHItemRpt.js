function initGridData(divRef,gridData)
{
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);
	gObj.enableTooltips("true,true,true,true,true,true,true,true,true,true");
	gObj.init();		
	gObj.loadXMLString(gridData);	
	return gObj;	
}
function fnOnPageLoad() {
	fnSetValues();
	if (objGridData != '') {
		gridObj = initGridData('pendshipitemdata',objGridData);
	}
}
function fnLoad(){	
	objEventName = document.frmIHLoanerItem.eventName.value;
	objPupEvent = document.frmIHLoanerItem.eventPurpose.value;
	objLoanTo = document.frmIHLoanerItem.loanToName.value;
	objLoanToName = document.frmIHLoanerItem.eventLoanToName.value;
	objStartDt = document.frmIHLoanerItem.eventStartDate;
	objEndDt = document.frmIHLoanerItem.eventEndDate;
	var dttype = document.frmIHLoanerItem.dtType;
	objType = document.frmIHLoanerItem.loanType.value;
	
	if(objStartDt.value != "" && objEndDt.value == ""){
		Error_Details(message_operations[89]);
	}
	if(objEndDt.value != "" && objStartDt.value == ""){
		Error_Details(message_operations[538]);
	}
	if(objStartDt.value != ""){
		CommonDateValidation(objStartDt, format, Error_Details_Trans(message_operations[563],format));
	}
	if(objEndDt.value != ""){
		CommonDateValidation(objEndDt, format, Error_Details_Trans(message_operations[564],format));
	}
	if(objStartDt.value != "" && objEndDt.value != ""){
		if(dateDiff(objStartDt.value,objEndDt.value,format) < 0){
			Error_Details(message_operations[565]);
		}	
	}
	
	if(objEventName == '0' && objPupEvent == '0' && objLoanToName == '0' && objStartDt.value == "" && objEndDt.value == "" && objType == '0' ){
		Error_Details(message_operations[566]);
	}
	
	if((objLoanToName != '0') && (objLoanTo == '0')){
		Error_Details(message_operations[567]);
	}
	if((dttype.value != '0') && (objStartDt.value == "") && (objEndDt.value == "")){
		Error_Details(message_operations[568]);
	}	
	if (ErrorCount > 0)	{
		Error_Show();
		Error_Clear();
		return false;
	}	
    document.frmIHLoanerItem.strOpt.value = "Reload"
	document.frmIHLoanerItem.action = "/gmIHLoanerItem.do?method=loadPendingRtnIHReport";
    fnStartProgress();
	document.frmIHLoanerItem.submit();
}

function fnViewDetails(val){	
	windowOpener("/GmConsignItemServlet?hAction=PicSlip&hConsignId="+val,"SetPic","resizable=yes,scrollbars=yes,top=150,left=150,width=750,height=500");	
}

function fnDownloadXLS(){
	gridObj.toExcel('/phpapp/excel/generate.php');
	
}
function fnLoanTo(obj){	
	var loanto = obj.value;	
	if(loanto == '0'){
		document.getElementById("loanToName").value = '0';	
		document.frmIHLoanerItem.loanToName.disabled = true;
	}
	if(loanto != '0' && loanto != '' ){
		document.frmIHLoanerItem.loanToName.disabled = false;
		if (loanto == '19518') 
		{
			document.frmIHLoanerItem.loanToName.options.length = 0;
			document.frmIHLoanerItem.loanToName.options[0] = new Option("[Choose One]","0");
			for (var i=0;i<RepLen;i++)
			{
				arr = eval("alRepListArr"+i);
				arrobj = arr.split(",");
				id = arrobj[0];
				name = arrobj[1];
				document.frmIHLoanerItem.loanToName.options[i+1] = new Option(name,id);
			}
		
		}
		else if(loanto == '19523')
		{
			document.frmIHLoanerItem.loanToName.options.length = 0;
			document.frmIHLoanerItem.loanToName.options[0] = new Option("[Choose One]","0");
			for (var i=0;i<RepLen;i++)
			{
				arr = eval("alOUSListArr"+i);
				arrobj = arr.split(",");
				id = arrobj[0];
				name = arrobj[1];
				document.frmIHLoanerItem.loanToName.options[i+1] = new Option(name,id);
			}
		
		}else if(loanto == '106400'){
			document.frmIHLoanerItem.loanToName.options.length = 0;
			document.frmIHLoanerItem.loanToName.options[0] = new Option("[Choose One]","0");
			for (var i=0;i<EmpLen;i++)
			{
				arr = eval("alEmpListArr"+i);
				arrobj = arr.split(",");
				id = arrobj[0];
				name = arrobj[1];
				document.frmIHLoanerItem.loanToName.options[i+1] = new Option(name,id);
			}
			//document.getElementById("eventLoanToName").value = '0';		
		}
	}
}
function fnSetValues(){
	document.frmIHLoanerItem.dtType.value=strdtType;
}
