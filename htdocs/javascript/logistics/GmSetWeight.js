//Enter key functionality.
function fnEnter(){	
	if(event.keyCode == 13)	{	 
		fnLoad();		
	}	
}

function fnLoad()
{
	var setID		= document.frmSetWeight.setId.value;
	var setName		= document.frmSetWeight.setNm.value;
	var weightValue = '';
	var count		= 0;
	
	if(setID == '' && setName == '')
	{
		Error_Details(message_operations[569]);
	}
	
	if((setID != '' || setName != '') && varRows != 0){
		for (var i = 0; i < varRows; i++){
			objhWeightVal = eval("document.frmSetWeight.hWeightVal"+i);
			
			hWeightVal = TRIM(objhWeightVal.value); 
			if(hWeightVal != '' ){
				count++;
			}
		}
	}
	if (count<0){
		document.frmSetWeight.Btn_Submit.disabled = true;
	}
	
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	//fnStartProgress('Y');
	document.frmSetWeight.strOpt.value = "reload";
	fnStartProgress('Y');
	document.frmSetWeight.submit();
}

function fnWeightValidate(obj){
	
	var weightVal = TRIM(obj.value);
	var objRegExp = /(^-?\d*$)/;
	
	if((weightVal <= 0) && (weightVal != '')){			
		document.frmSetWeight.Btn_Submit.disabled = true;
		Error_Details(message_operations[570]);
	}else if(isNaN(weightVal) && (weightVal != '')){
		Error_Details(message_operations[571]);
		document.frmSetWeight.Btn_Submit.disabled = true;	
	}else{
		document.frmSetWeight.Btn_Submit.disabled = false;
	}	
	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}	
	if(SwitchUserFl == 'Y'){
		document.frmSetWeight.Btn_Submit.disabled = true;
	}
}

function fnSubmit(){
	var weightValue  = '';
	var varSet  	 = '';
	var inputString  = ''; 
	var varSetID 	 = document.frmSetWeight.setId.value;
	var setName 	 = document.frmSetWeight.setNm.value;
	var errSet		 = '';
	var oldWeightVal = ''; 
	var chg_fl 		 = "N";
	 
	if((varSetID == '') && (setName == '')){
		Error_Details(message_operations[572]);
	}
	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}
	
	if(varSetID != null){
		for (var i = 0; i < varRows; i++){
			
			objSetID = eval("document.frmSetWeight.setid"+i);
			objWeightValue = eval("document.frmSetWeight.weightvalue"+i);	
			objoldWeightVal = eval("document.frmSetWeight.hWeightVal"+i);
			
			if(objSetID != null || objSetID != undefined){
				varSet = objSetID.value;
			}								
			if(objWeightValue != null || objWeightValue != undefined){
				weightValue = TRIM(objWeightValue.value);
			}		

			oldWeightVal = TRIM(objoldWeightVal.value);
			
			if( weightValue != oldWeightVal )
			{
				if(weightValue > 0 || weightValue == '' ){	
					inputString = inputString + varSet + '^' + weightValue + '|';
				}else{
					errSet = errSet + "<br>" + varSet;	
				}
				chg_fl = "Y";
			}				 
		}			 
	}
	if(chg_fl == "N"){
		Error_Details(message_operations[573]);
		Error_Show();
		Error_Clear();
		return false;
	}
	if(errSet != ''){
		Error_Details(Error_Details_Trans(message_operations[574],errSet));
		Error_Show();
		Error_Clear();
		return false;
	}else{
	 	//fnStartProgress('Y');
		document.frmSetWeight.strOpt.value    = 'save';
		document.frmSetWeight.hinputStr.value = inputString;
		document.frmSetWeight.Btn_Submit.disabled = true;
		fnStartProgress('Y');
		document.frmSetWeight.submit();
	}
}

