function fnFetchKitDetails(kitName,form){
	form.action = 'gmkitMapping.do?method=loadKitMapping&strOpt=fetchContainer&kitName='+kitName;
	fnStartProgress('Y');
	form.submit();	
}

function fnSearchKitNm(strKitNm,form){
	if(strKitNm!=''){
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmkitMapping.do?method=loadKitMapping&strOpt=SearchKitName&KitNm='+encodeURIComponent(strKitNm) +'&ramdomId=' + Math.random());
		dhtmlxAjax.get(ajaxUrl,function(loader)
				{
					var response = loader.xmlDoc.responseText;
					if(response=='N'){
						document.all.setSuccimg.style.display='inline';
						if (document.getElementById("setERRimg"))
						document.all.setERRimg.style.display='none';
						document.all.setErrimg.style.display='none';
					}else{
						document.all.setSuccimg.style.display='none';
						if (document.getElementById("setERRimg"))
						document.all.setERRimg.style.display='none';
						document.all.setErrimg.style.display='inline';
					}
				});
}  }
function fnAdd(setid,cnt){
	 	var tbody = document.getElementById("KitMapping").getElementsByTagName("TBODY")[0];
	    var row = document.createElement("TR")
	    row.setAttribute("id", "TR"+cnt);
	 
		var td0 = document.createElement("TD")
	    td0.innerHTML = '<a href=javascript:fnRmeItem('+cnt+'); tabindex=\'-1\'><img border=0  Alt=Remove  title=Remove  valign=left src=/images/red_btn_minus.gif height=10 width=9></a>';
		td0.align = "right";
		
	    var td1 = document.createElement("TD")
	    td1.innerHTML = '<span id=setId'+cnt+'>';
		td1.align = "left";
	
		var td2 = document.createElement("TD")
	    td2.innerHTML = '<span id=setNm'+cnt+' javascript:fnGetSetId(this.value,'+cnt+');> <input type=hidden name=hStatus'+cnt+' value="">';
		
		var td3 = document.createElement("TD") 
	    td3.innerHTML =  '<span id=Cell'+cnt+'><a href=javascript:fnAddTagRows('+cnt+'); tabindex=\'-1\'><img border=0  Alt=Remove  title=Remove  valign=left src=/images/plus.gif height=10 width=9></a>&nbsp;'+
	    '<input type=text size=10 id=tagId'+cnt+' class=InputArea onblur= javascript:fnTagSearch(this.value,'+cnt+');>'+
	    '<span id=validSetSuc'+cnt+' style=\'vertical-align:top; display: none;\'>&nbsp;&nbsp;<img  tabindex=\'-1\'  title=\'Tag number available\' height=16 width=19 src=/images/success.gif></img></span>'+
	    '<span id=validSetErr'+cnt+' style=\'vertical-align:top; display: none;\'>&nbsp;&nbsp;&nbsp;<img id=ASSO'+cnt+' tabindex=\'-1\' height=13 title=\'Invalid data entered\' width=13 src=/images/error.gif></img></span></span>';

		row.appendChild(td0);
		row.appendChild(td1);
		row.appendChild(td2);
		row.appendChild(td3);
	    tbody.appendChild(row);
	    cnt++;
		document.all.hcnt.value = cnt;		
}
var strSetId = '';
function fnGetSetDetails(setID){
	 cnt = document.all.hcnt.value;
	 var strSetID= setID.substring(0,setID.indexOf('-'));
	
	 
	  if(strSetID!=0){
	   var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmkitMapping.do?method=loadKitMapping&strOpt=searchSetDetails&setId='+strSetID +'&ramdomId=' + Math.random());

		dhtmlxAjax.get(ajaxUrl,function(loader)
		{
			var response = loader.xmlDoc.responseText;
			 var mySplitResult = response.split("^");
			 if(response!=null && response!=''){ 
		   			setId = mySplitResult[0]; 
		   			setName = mySplitResult[1];
		   			setStatus = mySplitResult[2];
			 }
			 if(setStatus=="Approved"){
				 fnAdd(strSetID,cnt);
					document.getElementById("setId"+cnt).innerHTML = setId;
					document.getElementById("setNm"+cnt).innerHTML = setName; 
					document.getElementById("searchsetId").value = "";
					document.all.validSetError.style.display = "none";
			 }else{
				 document.all.validSetError.style.display = "inline";
			 }
		
			
		});
	  }
}

function fnRmeItem(cnt){
	 var row = document.getElementById("TR"+cnt);
	 row.parentNode.removeChild(row);
}
function fnRemoveItem(cnt){
var setID=document.getElementById('setId'+cnt).innerHTML;
var tagID=document.getElementById('tagId'+cnt).value;
var kitID=document.getElementById('kitId').value;
var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmkitMapping.do?method=loadKitMapping&strOpt=RemoveSet&setId='+setID+'&kitId='+kitID+'&tagId='+tagID+'&ramdomId=' + Math.random());

dhtmlxAjax.get(ajaxUrl,function(loader)
{
	var response = loader.xmlDoc.responseText;
	if(response != '1'){
	 var row = document.getElementById("TR"+cnt);
	 row.parentNode.removeChild(row);
	} else{
		Error_Details(message_operations[777]);
		Error_Show();
        Error_Clear();
        return false;
	}
});
}

function fnAddTagRows(val){

	cnt = parseInt(document.getElementById("hNewCnt").value);	
	cnt++;
	document.getElementById("hNewCnt").value = cnt;
	var hcnt = parseInt(document.getElementById("hcnt").value);
	hcnt = hcnt+cnt;
	var setid = document.getElementById("setId"+val).innerHTML;

	var NewQtyHtml = "<span id='Sp"+cnt+"'><BR><span style='display:none' id='setId"+hcnt+"'>"+setid+"</span><a href=javascript:fnDelete('"+setid+"','"+cnt+"');><img img width='9' height='10' src=/images/btn_remove.gif border=0></a>&nbsp;"+
	"<input  tabindex='' type='text' size='10' value='' id='tagId"+hcnt+"' onBlur=javascript:fnAddTagSearch(this.value,'"+setid+"','"+hcnt+"','"+val+"');>&nbsp;"+
	"<span id='validSetSuc"+hcnt+"' style=\'vertical-align:top; display: none;\'> "+
	"<img tabindex=\'-1\' height=16 width=19  title=\'Set available to Map\' src=/images/success.gif></img></span>" +
	"<span id='validSetErr"+hcnt+"' style=\'vertical-align:top; display: none;\'>&nbsp;&nbsp;<img id=\'ASSO"+hcnt+"\' tabindex=\'-1\' height=13 title=\'Invalid data entered\' width=13 src=/images/error.gif></img></span></span>";
	var QtyObj = eval("document.all.Cell"+val);
	QtyObj.insertAdjacentHTML("beforeEnd",NewQtyHtml);
}

function fnAddTagRow(val,setid){

	cnt = parseInt(document.getElementById("hNewCnt").value);	
	cnt++;
	document.getElementById("hNewCnt").value = cnt;
	var hcnt = parseInt(document.getElementById("hcnt").value);
	hcnt = hcnt+cnt;

var NewQtyHtml = "<span id='Sp"+cnt+"'><BR><span style='display:none' id='setId"+hcnt+"'>"+setid+"</span><a href=javascript:fnDelete('"+setid+"','"+cnt+"');><img img width='9' height='10' src=/images/btn_remove.gif border=0></a>&nbsp;"+
"<input  tabindex='' type='text' size='10' value='' id='tagId"+hcnt+"' onBlur=javascript:fnAddTagSearch(this.value,'"+setid+"','"+hcnt+"','"+val+"');>&nbsp;"+
"<span id='validSetSuc"+hcnt+"' style=\'vertical-align:top; display: none;\'> "+
"<img tabindex=\'-1\' height=16 width=19 title=\'Set available to Map\' src=/images/success.gif></img></span>" +
"<span id='validSetErr"+hcnt+"' style=\'vertical-align:top; display: none;\'>&nbsp;&nbsp;<img id=\'ASSO"+hcnt+"\' tabindex=\'-1\' height=13 title=\'Invalid data entered\' width=13 src=/images/error.gif></img></span></span>";

var QtyObj = eval("document.all.Cell"+val);
QtyObj.insertAdjacentHTML("beforeEnd",NewQtyHtml);
}

function fnAddTagSearch(tagID,setID,rowCnt,mcnt){

	validSetSuc = eval("document.all.validSetSuc"+rowCnt);	
	validSetErr = eval("document.all.validSetErr"+rowCnt);
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmkitMapping.do?method=loadKitMapping&strOpt=searchTagNum&TagId='+tagID +'&setId='+setID+'&ramdomId=' + Math.random());

	dhtmlxAjax.get(ajaxUrl,function(loader)
			{
				var response = loader.xmlDoc.responseText;
				if(response=='N'){
					validSetErr.style.display = 'inline';
		   			validSetSuc.style.display = 'none';
		   			document.getElementById('ASSO'+rowCnt).title = "Invalid data entered";
				}else if(response=='NN'){
					validSetErr.style.display = 'inline';
		   			validSetSuc.style.display = 'none';
		   			document.getElementById('ASSO'+rowCnt).title = "Tag associated to another Kit";
				}else if(response=='Y'){
					validSetErr.style.display = 'none';
		   			validSetSuc.style.display = 'inline';
				}else{
					validSetErr.style.display = 'inline';
		   			validSetSuc.style.display = 'none';
				}
			});
}
function fnTagSearch(tagID,rowCnt){
	var kitId = document.frmKitMapping.kitId.value;
	var setID=document.getElementById('setId'+rowCnt).innerHTML;
	validSetSuc = eval("document.all.validSetSuc"+rowCnt);	
	validSetErr = eval("document.all.validSetErr"+rowCnt);

	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmkitMapping.do?method=loadKitMapping&strOpt=searchTagNum&TagId='+tagID +'&setId='+setID+'&kitId='+kitId+'&ramdomId=' + Math.random());

	dhtmlxAjax.get(ajaxUrl,function(loader)
			{
				var response = loader.xmlDoc.responseText;
				if(response=='N'){
					validSetErr.style.display = 'inline';
		   			validSetSuc.style.display = 'none';
		   			document.getElementById('ASSO'+rowCnt).title = "Invalid data entered";
				}else if(response=='NN'){
					validSetErr.style.display = 'inline';
		   			validSetSuc.style.display = 'none';
		   			document.getElementById('ASSO'+rowCnt).title = "Tag associated to another Kit";
				}
				else if(response=='Y'){
					validSetErr.style.display = 'none';
		   			validSetSuc.style.display = 'inline';
				}else{
					validSetErr.style.display = 'inline';
		   			validSetSuc.style.display = 'none';
				}
			});
}

function fnDelete(setid,cnt){
	var Obj=eval("document.all.Sp"+cnt);
	Obj.innerHTML = "";	 
}
function fnRemoveTagRow(cnt){
	var table = document.getElementById("KitMapping").getElementsByTagName("TBODY")[0];
    var row = table.deleteRow(cnt);
}


function fnSubmit(frm){
	var alltagid='';
	var allSetid='';
	var allSetids='';
	var allTagIds='';
	var setidArr='';
	var setidVal ='';
	var tagnumVal ='';
	var tagError = '';
	var activefl = '';
	var tagAssocheck = '';
	var tagAssokitcheck = '';
	var tagsetIDcheck = '';
	var response = '';
	var errQty = '';
	activefl = document.frmKitMapping.activeFl.title;
	/*if(activefl=='on'){*/
	cnt = document.all.hcnt.value;
	hcnt = document.getElementById("hNewCnt").value;
	hcnt=parseInt(hcnt)+parseInt(cnt);
		for(var i=0;i<=hcnt; i++){
			if(document.getElementById("setId"+i) != undefined){	
				setidVal = eval("document.all.setId"+i).innerHTML;
				allSetids = allSetids+setidVal+'|';	
			}if(document.getElementById("tagId"+i) != undefined){
				tagnumVal = eval("document.all.tagId"+i).value;
				if(tagnumVal==''){
					tagError = tagError + setidVal + ',';
				}
				allTagIds = allTagIds+tagnumVal+'|';
			}	
			
		}

		var arr=tagError.split(',');
		for(var i=0;i<=arr.length;i++){
			qid = arr[i];
			for(var j=0;j<=hcnt;j++) {
				if(document.getElementById("setId"+i) != undefined){
				setidVal = eval("document.all.setId"+i).innerHTML;
				}
				if(document.getElementById("tagId"+i) != undefined){
					tagnumVal = eval("document.all.tagId"+i).value;
				}
				if(tagnumVal==''){
				errQty += (errQty.indexOf(setidVal) != -1)? "":", "+setidVal;	
				}
			}
		}
		if(errQty!=''){
			Error_Details("<b>Tag Number</b> cannot be blank. Please enter a valid data for <b>Set ID: </b>"+errQty.substr(1,errQty.length));
		}
		if(setidVal==''){
			Error_Details(message_operations[772]);
		}
		fnValidateTxtFld('kitNm',message_operations[773]);
		fnValidateTxtFld('comments',message_operations[151]);
		
	if(activefl!='on'){
		var kitid = document.frmKitMapping.searchkitId.value;
	
		var name = document.frmKitMapping.kitNm.value;
		var setselect = document.frmKitMapping.searchsetId.value;
		var comments = document.frmKitMapping.comments.value;
		if(comments!=''){
			if(kitid=='' && name=='' && setselect==''){
				Error_Details(message_operations[774]);
			}
		}
	}

	if (ErrorCount > 0)
    {
             Error_Show();
             Error_Clear();
             return false;
    }
	var KITID = document.frmKitMapping.kitId.value;
	var ajaxUrl = fnAjaxURLAppendCmpInfo('gmkitMapping.do?method=loadKitMapping&strOpt=searchTagNumber&TagId='+allTagIds +'&setId='+allSetids+'&KitId='+KITID+'&ramdomId=' + Math.random());	
	dhtmlxAjax.get(ajaxUrl,function(loader){
			response = loader.xmlDoc.responseText;
			var mySplitResult = response.split("^");
			if(response!=''){
				tagAssoSetVal = mySplitResult[0];
				tagsetID = mySplitResult[1];
				tagAssoKitVal = mySplitResult[2];
			}
			var tagAssoSetValue = tagAssoSetVal.split("/");
			tagAssoSet = tagAssoSetValue[0];			
			setidvalues = tagAssoSetValue[1];
			
			var tagAssoKitValue = tagAssoKitVal.split("/");
			tagAssoKit = tagAssoKitValue[0];			
			kitidvalues = tagAssoKitValue[1];
			
			tagAssocheck =  tagAssoSet.substring(tagAssoSet.indexOf(',')+1,tagAssoSet.length);
			tagsetIDcheck =  tagsetID.substring(tagsetID.indexOf(',')+1,tagsetID.length);
			tagAssokitcheck = tagAssoKit.substring(tagAssoKit.indexOf(',')+1,tagAssoKit.length);

			if(tagsetIDcheck!=''){
				Error_Details("<b>Tag Number</b> must be associated to a Consignment set. Please enter valid data for <b>Set ID:</b> "+tagsetIDcheck);
			}
			
			if(tagAssokitcheck!=''){
				Error_Details("Tag ID <b>" +tagAssokitcheck+ "</b> is associated to another Kit <b>" +kitidvalues.substring(kitidvalues.indexOf(',')+1,kitidvalues.length)+"</b>");
			}
			if(tagAssocheck!=''){
				Error_Details("Tag ID <b>" +tagAssocheck+ "</b> is associated to <b>" +setidvalues.substring(setidvalues.indexOf(',')+1,setidvalues.length)+ "</b> per Tag Report");
			}
			if (ErrorCount > 0)
		    {
		             Error_Show();
		             Error_Clear();
		             return false;
		    }
			frm.action = 'gmkitMapping.do?method=loadKitMapping&strOpt=save&setId='+allSetids+'&tagId='+allTagIds;
			fnStartProgress('Y');
			frm.submit();
		});

}

function fnReset(frm){
		document.all.kitNm.value = '';
		document.all.kitId.value = '';
		frm.strOpt.value = '';
		frm.action = "/gmkitMapping.do?method=loadKitMapping";
		frm.submit();	
}

function fnLoad(){
	if( stractiveFl == "Y"){
		document.frmKitMapping.activeFl.checked = true;		
	}else if(stractiveFl == ""){
		document.frmKitMapping.activeFl.checked = true;	
	}else{
		document.frmKitMapping.activeFl.checked = false;	
	} 
} 

function uncheck(){
	if(document.frmKitMapping.activeFl.value=="on"){
		document.frmKitMapping.activeFl.title="off";
	}
}

//Report
function selectedValue(){  
    if(selectedValues !=null) {
        document.all.kitStatus.selectedIndex=selectedValues ;          
    }    
}
function fnLoadKit()
{
	document.frmGmKitMapping.strOpt.value = 'load';  	
	document.frmGmKitMapping.action = '/gmKitMappingReport.do?method=loadKitMapReports'; 		        	
  	document.frmGmKitMapping.submit();
} 

function fnOnPageLoad(){
	
if (objGridData.value!= '') {
	gridObj = initGridData('kitMappingRpt', objGridData);
} 
}

function initGridData(divRef, gridData) { 
	mygrid = setDefalutGridProperties(divRef);	
	//mygrid.setNumberFormat("$0,000.00",4,".",",");
	mygrid.enableMultiline(false);	
	mygrid.enableDistributedParsing(true);
	mygrid = initializeDefaultGrid(mygrid,gridData);
	mygrid.setColumnHidden(1,true);
	mygrid.attachHeader('#text_filter,#text_filter,#text_filter,#text_filter,#select_filter,#text_filter,#text_filter,#text_filter,#select_filter,#text_filter');
	mygrid.enableTooltips("false,false,false,false,false,false,false,false,false,false");
	mygrid.enableBlockSelection(true); 
return mygrid;
}
//To download excel;
function fnExcel(){ 
	mygrid.setColumnHidden(1,false);
	mygrid.setColumnHidden(0,true);
	mygrid.toExcel('/phpapp/excel/generate.php');
	mygrid.setColumnHidden(1,true);
	mygrid.setColumnHidden(0,false);
}

function fnViewCaseDetails(caseId) {
	 w1 = createDhtmlxWindow(750,200,true);
	 w1.setText("Case Info");
	 w1.attachURL('/gmCaseSchedular.do?paramCaseId='+caseId+'&strOpt=kitMapCaseInfo&ramdomId='+ Math.random());
}

function fnViewCaseDetails(caseId) {
	 w1 = createDhtmlxWindow(750,220,true);
	 w1.setText("Case Info");
	 w1.attachURL('/gmCaseSchedular.do?paramCaseId='+caseId+'&strOpt=kitMapCaseInfo&ramdomId='+ Math.random());
}

function fnLoadCaseInfo(){
	if(strLongTerm == "Y"){
		document.all.longTerm.checked = true;
	}else{
		document.all.longTerm.checked = false;	
	}
}
