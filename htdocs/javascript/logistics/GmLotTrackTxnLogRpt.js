
// This function used to On page load 
function fnOnload(){
	document.getElementById("dataGridDiv").style.display = 'none';
	document.getElementById("DivExportExcel").style.display = 'none';
	document.getElementById("DivNothingMessage").style.display = 'none';
	
//	var partNumber = document.frmLotTrackTxnLogRpt.strPartNumber.value;
//	var lotNumber = document.frmLotTrackTxnLogRpt.strLotNumber.value;
	var warehouseType = document.frmLotTrackTxnLogRpt.warehouseType.value;
	var controlInvId = document.frmLotTrackTxnLogRpt.strControlInvId.value;

	if(controlInvId != '' && controlInvId != undefined){
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmLotTrackTxnLogRpt.do?method=fetchLotTrackTxnLogDetails&strOpt=Reload&warehouseType='+warehouseType+'&strControlInvId='+controlInvId+'&ramdomId='+new Date().getTime());
		dhtmlxAjax.get(ajaxUrl,fnLoadLotAjax);
		fnStartProgress();
	}
}

// This  function used to Load button
function fnLoad(){
	var partNumber = document.frmLotTrackTxnLogRpt.strPartNumber.value;
	var lotNumber = document.frmLotTrackTxnLogRpt.strLotNumber.value;
	var expiryFromDate = document.frmLotTrackTxnLogRpt.expiryDateFrom.value;
	var expiryToDate = document.frmLotTrackTxnLogRpt.expiryDateTo.value;
	var warehouseType = document.frmLotTrackTxnLogRpt.warehouseType.value;
	var strSearch = document.frmLotTrackTxnLogRpt.strPartLiteral.value;
	//PC-2786 - 
	var strLocation = document.frmLotTrackTxnLogRpt.strLocation.value;  
	var chkds = $("input[name='strExpired']:checkbox");
	 if (chkds.is(":checked"))  {
		 document.frmLotTrackTxnLogRpt.strExpired.value='Y'; 
	 }
	  else {
		 document.frmLotTrackTxnLogRpt.strExpired.value=''; 
	  }
	var strExpired = document.frmLotTrackTxnLogRpt.strExpired.value;

	if(((partNumber !='' && partNumber != undefined) || (strLocation !='' && strLocation != undefined))  || ((lotNumber !='' && lotNumber != undefined) || (strLocation !='' && strLocation != undefined))){
		
		if (partNumber.length < 3 && (partNumber!='' && partNumber != undefined)){
			Error_Details (message_operations[349]);
			Error_Show();
			Error_Clear();
			return false;		 
		}
		
		if(expiryFromDate != '' || expiryToDate != ''){
			CommonDateValidation(document.frmLotTrackTxnLogRpt.expiryDateFrom,gCmpDateFmt,message[5500]);
			CommonDateValidation(document.frmLotTrackTxnLogRpt.expiryDateTo,gCmpDateFmt,message[5512]);
			var dateRange = dateDiff(expiryFromDate,expiryToDate,gCmpDateFmt);
			fnValidateTxtFld('expiryDateFrom',message_operations[144]);
			fnValidateTxtFld('expiryDateTo',message_operations[145]);	
			if(!isNaN(dateRange) && dateRange < 0){
				 Error_Details(message_operations[117]);
			  }
			if (ErrorCount > 0)
			{
				Error_Show();
				Error_Clear();
				return false;
			}
		}
		fnStartProgress();
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmLotTrackTxnLogRpt.do?method=fetchLotTrackTxnLogDetails&strOpt=Reload&strPartNumber='+encodeURIComponent(partNumber)+'&strLotNumber='+encodeURIComponent(lotNumber)+
			'&warehouseType='+warehouseType+'&expiryDateFrom='+expiryFromDate+'&expiryDateTo='+expiryToDate+'&strPartLiteral='+strSearch+'&strLocation='+encodeURIComponent(strLocation)+'&strExpired='+strExpired+'&ramdomId='+new Date().getTime());
	dhtmlxAjax.get(ajaxUrl,fnLoadLotAjax);
	}else{
		 Error_Details(message_operations[853]);
	    }
		if (ErrorCount > 0)
		{
			Error_Show();
			Error_Clear();
			return false;
		}
}

//This function used to generate the grid
function fnLoadLotAjax (loader){
	var partNum = '';
	var lotNum = '';
	var transId = '';
	var transType ='';
	var createDate = '';
	var origQty = '';
	var txnQty = '';
	var newQty = '';
	var expDate = '';
	var locationId = '';
	var warehouseType = '';
	var companyId = '';
	var plantId = '';
	
	var response = loader.xmlDoc.responseText;
	 if(response != "") {
		 
		  //to form the grid string 
			var rows = [], i=0;
			var JSON_Array_obj = "";
			 JSON_Array_obj = JSON.parse(response);
			
			$.each(JSON_Array_obj, function(index,jsonObject){
				partNum = jsonObject.PARTNO;
				lotNum = jsonObject.CNTROLNO;   
				transId = jsonObject.TRANSID;
				transType = jsonObject.TRANSTYPE;
				createDate = jsonObject.CREATEDATE;
				origQty = jsonObject.ORIGQTY;
				txnQty = jsonObject.TXNQTY;
				newQty = jsonObject.NEWQTY;
				expDate = jsonObject.EXPIRYDATE;
				locationId = jsonObject.LOCATIONID;
				warehouseType = jsonObject.WAREHOUSETYPE;
				companyId = jsonObject.COMPANYID;
				plantId = jsonObject.PLANTID;
				
				// default array
		        rows[i] = {};
		        rows[i].id = i;
		        rows[i].data = [];
		       
		        rows[i].data[0] = partNum;
		        rows[i].data[1] = lotNum;   
		        rows[i].data[2] = transId;    
		        rows[i].data[3] = transType;   
		        rows[i].data[4] = createDate;   
		        rows[i].data[5] = origQty;   
		        rows[i].data[6] = txnQty;   
		        rows[i].data[7] = newQty;
		        rows[i].data[8] = expDate;
		        rows[i].data[9] = locationId;   
		        rows[i].data[10] = warehouseType;   
		        rows[i].data[11] = companyId;   
		        rows[i].data[12] = plantId;   
		       	
		        i++;
			});
			
			var setInitWidths = "90,100,100,100,80,100,50,50,70,100,120,170,*";
			
		    var setColAlign = "left,left,left,left,left,right,right,right,left,left,left,left,left" ;
		    var setColTypes = "ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro" ;		    		          
		    var setColSorting = "str,str,str,str,str,int,int,int,str,str,str,str,str";		   
		    var setHeader = ["Part #","Lot #","Trans.ID","Trans.Type","Trans.Date","Org Qty","Tran Qty","New Qty","Expiry Date","Location","Warehouse","Company","Plant"];		  
		    var setFilter = ["#text_filter","#text_filter","#text_filter","#text_filter","#text_filter","#numeric_filter","#numeric_filter","#numeric_filter","#text_filter",
		                     "#text_filter","#select_filter","#select_filter","#select_filter"];
		    var setColIds = "";		 
		    var enableTooltips = "true,true,true,true,true,true,true,true,true,true,true,true,true";
		    var footerArry = [];
		    var gridHeight = "";
		    var footerStyles = [];
		    var footerExportFL = true;
		    var dataHost = {};
		    dataHost.rows = rows;
			document.getElementById("dataGridDiv").style.display = 'block';
			document.getElementById("DivNothingMessage").style.display = 'none';
			document.getElementById("DivExportExcel").style.display = 'block';
		    gridObj = loadDHTMLXGrid('dataGridDiv', gridHeight, dataHost, setHeader, setInitWidths, setColAlign, setColTypes, setColSorting, setColIds, enableTooltips, setFilter, footerArry, footerStyles, footerExportFL);
		  
		    }else{
			document.getElementById("dataGridDiv").style.display = 'none';
			document.getElementById("DivExportExcel").style.display = 'none';
			document.getElementById("DivNothingMessage").style.display = '';
		    }	
	  fnStopProgress();
}

//This function used to download excel
function fnDownloadXLS() {
	gridObj.toExcel('/phpapp/excel/generate.php');
}