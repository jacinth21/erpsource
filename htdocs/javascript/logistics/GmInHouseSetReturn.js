function fnLoad(){	
	if(document.frmVendor.Btn_submit && document.frmVendor.Cbo_CameraId.value == '0' && transType != '4119'){   // In-House Loaner Set
		document.frmVendor.Btn_submit.disabled = true;
	}
}
function fnSubmit()
{	
	fnValidateTxtFld('Txt_RetDate',message_operations[545]);
	fnValidateDropDn('Cbo_CameraId',message_operations[546]);
	fnValidateTxtFld('Txt_LogReason',message_operations[547]);
	//PMT-40075 Able to select future Return date in Accept Return Screen
	//get the Return date, current date(todaysdate)and dateFmt 
	var objTxtRetDate = document.frmVendor.Txt_RetDate;
	var returnDate;
	if (objTxtRetDate != undefined || objTxtRetDate != null) {
		returnDate = objTxtRetDate.value;
		// get the difference value using dateDiff function
		diff = dateDiff(returnDate, todaysdate, dateFmt);
		// if the diff value is less than zero show below validation
		if (diff < 0) {
			Error_Details(message_operations[824]);
		}
	}
	if (ErrorCount > 0)
	{		
		Error_Show();
		Error_Clear();
		return false;//PMT-40075
	}else{
		document.frmVendor.hAction.value = 'ReturnSet';			
		fnStartProgress('Y');
  		document.frmVendor.submit();
  		if(parent.window.document.getElementById("refId") != undefined){
			parent.window.document.getElementById("refId").value = "";
			parent.window.document.getElementById("refId").focus();
  		}
  	}
}
// this function is used to change the log comments based on the selected Camera.
function fnChangeCamera(Obj) {
	var cameraValue = Obj.value;
	if (cameraValue != '0') {
		// to get the values for selected camera name
		var cameraName = document.frmVendor.Cbo_CameraId.options[document.frmVendor.Cbo_CameraId.selectedIndex].text;
		document.all.Txt_LogReason.value = cameraName + " " + default_comment + "\n";   //Append camera name and Default comment for PBUG-2684
		if(document.frmVendor.Btn_submit){
			document.frmVendor.Btn_submit.disabled = false;
		}		
	} else {
		// Camera select Choose one the comments to be empty.
		// In servlet getting the cookies values and set to log comments.
		document.all.Txt_LogReason.value = '';
	}
}

function fnPrintVer(hopt){
	windowOpener("/GmInHouseSetServlet?hAction=ViewPrint&hConsignId="+val+"&hType="+type+"&hOpt="+hopt,"LoanPrint","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}