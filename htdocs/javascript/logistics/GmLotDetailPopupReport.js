function fnOnLoadPage(){
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmLotDetailRpt.do?method=fetchLotTransHistory&strPartNumber='+encodeURIComponent(strPartNumber)+'&strLotNumber='+strLotNumber+
				'&strPartLiteral='+strPartLiteral+'&strInvType='+strInvType+'&companyId='+strCompanyId+
				'&fieldSalesNm='+strFieldSalesNm+'&strSetId='+strSetId+'&ramdomId='+new Date().getTime());
		dhtmlxAjax.get(ajaxUrl,fnPopulateTxnHistory);
}
//this function is used to populate transaction history details when click on field sales, Consignment and loaner for lot track detail report
function fnPopulateTxnHistory (loader){
	var transId = '';
	var partNum = '';
	var qty = '';
	var transDate = '';
	var transType = '';
	var invType = '';
	var location = '';
	var companyName = '';
	var plantName = '';
	var tagId = '';
	var setId = '';
	var setName = '';
	
	var response = loader.xmlDoc.responseText;

	 if(response != "") {
		  //to form the grid string 
			var rows = [], i=0;
			var JSON_Array_obj = "";
			 JSON_Array_obj = JSON.parse(response);
			 
			 if(strInvType == '4000339')   {    //Fiels Sales
				 $.each(JSON_Array_obj, function(index,jsonObject){
				
					transId = jsonObject.TRANSID;
					qty = jsonObject.QTY;
					location = jsonObject.LOCATIONNAME;
					companyName = jsonObject.COMPANYNAME;
					partNum = jsonObject.PARTNUM;
					transDate = jsonObject.CREATEDDATE;
					transType = jsonObject.TRANSTYPE;
					invType = jsonObject.WAREHOSETYPE;
				// default array
					rows[i] = {};
					rows[i].id = i;
					rows[i].data = [];
		       
					rows[i].data[0] = transId;
					rows[i].data[1] = partNum;
					rows[i].data[2] = qty;   
					rows[i].data[3] = transDate;
					rows[i].data[4] = transType;
					rows[i].data[5] = invType;
					rows[i].data[6] = location;   
					rows[i].data[7] = companyName;
					i++;
				});
					var setInitWidths = "100,100,50,100,150,100,100,*";			
					var setColAlign = "left,left,right,right,left,left,left,left";	    		          
					var setColSorting = "str,str,int,str,str,str,str,str";		   
					var setHeader = ["Trans. ID","Part #","Qty","Trans. Date","Trans. Type","Inv. Type","Location","Company Name"];	 					
				}else{
					$.each(JSON_Array_obj, function(index,jsonObject){
					transId = jsonObject.TRANSID;
					qty = jsonObject.QTY;
					location = jsonObject.LOCATIONNAME;
					companyName = jsonObject.COMPANYNAME;
					tagId = jsonObject.TAGID;
					setId = jsonObject.SETID;
					setName = jsonObject.SETNAME;
					plantName = jsonObject.PLANTNAME;
				// default array
					rows[i] = {};
					rows[i].id = i;
					rows[i].data = [];
		       
					rows[i].data[0] = transId;
					rows[i].data[1] = tagId;
					rows[i].data[2] = qty;   
					rows[i].data[3] = setId;
					rows[i].data[4] = setName;
					rows[i].data[5] = location;
					rows[i].data[6] = companyName;   
					rows[i].data[7] = plantName;
					i++;
				});
					var setInitWidths = "100,100,50,100,150,100,100,*";			
					var setColAlign = "left,left,right,right,left,left,left,center";	    		          
					var setColSorting = "str,str,int,str,str,str,str,str";		   
					var setHeader = ["Consignment#","Tag #","Qty","Set ID","Set Name","Location","Company Name","Plant Name"];	 
				}
		    var setColIds = "";
		    var footerArry = [];
		    var gridHeight = "";
		    var footerStyles = [];
		    var footerExportFL ='';
		    var dataHost = {};
		    var enableTooltips = "true,true,true,true,true,true,true,true";
			var setColTypes = "ro,ro,ro,ro,ro,ro,ro,ro" ;	
		    dataHost.rows = rows;
			document.getElementById("dataGridTxnHisInfo").style.display = 'block';
			gridObj = loadDHTMLXGrid('dataGridTxnHisInfo', gridHeight, dataHost, setHeader, setInitWidths, setColAlign, setColTypes, setColSorting, setColIds, enableTooltips, footerArry, footerStyles, footerExportFL);		  
		    }else{
			document.getElementById("dataGridTxnHisInfo").style.display = 'none';
		    }	
}