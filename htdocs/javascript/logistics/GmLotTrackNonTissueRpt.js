
// This function used to On page load 
function fnOnload(){
	document.getElementById("dataGridDiv").style.display = 'none';
	document.getElementById("DivExportExcel").style.display = 'none';
	document.getElementById("DivNothingMessage").style.display = 'none';
	
	var partNumber = document.frmLotTrackNonTissueRpt.strPartNumber.value;
	var lotNumber = document.frmLotTrackNonTissueRpt.strLotNumber.value;
	var expiryFromDate = document.frmLotTrackNonTissueRpt.expiryDateFrom.value;
	var expiryToDate = document.frmLotTrackNonTissueRpt.expiryDateTo.value;
	var warehouseType = document.frmLotTrackNonTissueRpt.warehouseType.value;
	var strSearch = document.frmLotTrackNonTissueRpt.strPartLiteral.value;
	if((partNumber!='' && partNumber != undefined)  || (lotNumber!='' && lotNumber != undefined)){
		fnLoad();
	}
}

// This  function used to Load button
function fnLoad(){
	var partNumber = document.frmLotTrackNonTissueRpt.strPartNumber.value;
	var lotNumber = document.frmLotTrackNonTissueRpt.strLotNumber.value;
	var expiryFromDate = document.frmLotTrackNonTissueRpt.expiryDateFrom.value;
	var expiryToDate = document.frmLotTrackNonTissueRpt.expiryDateTo.value;
	var warehouseType = document.frmLotTrackNonTissueRpt.warehouseType.value;
	var strSearch = document.frmLotTrackNonTissueRpt.strPartLiteral.value;
	var strCompanyId = document.frmLotTrackNonTissueRpt.companyId.value;
	var strPlantId = document.frmLotTrackNonTissueRpt.plantId.value;
	var strDateRange = document.frmLotTrackNonTissueRpt.expDateRange.value;
	var strFieldSales = document.frmLotTrackNonTissueRpt.strFieldSales.value;
	var strSetId = document.frmLotTrackNonTissueRpt.strSetId.value;
	//strLocation ,strExpired added for PC-2786 - Add Field Sales Filter in Lot Inventory Report 
	var strLocation = document.frmLotTrackNonTissueRpt.strLocation.value;  
	var chkds = $("input[name='strExpired']:checkbox");
	 if (chkds.is(":checked"))  {
		 document.frmLotTrackNonTissueRpt.strExpired.value='Y'; 
	 }
	  else {
		 document.frmLotTrackNonTissueRpt.strExpired.value=''; 
	  }
	var strExpired = document.frmLotTrackNonTissueRpt.strExpired.value;
	
	if(((partNumber !='' && partNumber != undefined) || (strLocation !='' && strLocation != undefined))  || ((lotNumber !='' && lotNumber != undefined) || (strLocation !='' && strLocation != undefined))){
		
		if (partNumber.length < 3 && (partNumber!='' && partNumber != undefined)){
			Error_Details (message_operations[349]);
			Error_Show();
			Error_Clear();
			return false;		 
		}
		
		if(expiryFromDate != '' || expiryToDate != ''){
			CommonDateValidation(document.frmLotTrackNonTissueRpt.expiryDateFrom,gCmpDateFmt,message[5500]);
			CommonDateValidation(document.frmLotTrackNonTissueRpt.expiryDateTo,gCmpDateFmt,message[5512]);
			var dateRange = dateDiff(expiryFromDate,expiryToDate,gCmpDateFmt);
			fnValidateTxtFld('expiryDateFrom',message_operations[144]);
			fnValidateTxtFld('expiryDateTo',message_operations[145]);			
			
			if(!isNaN(dateRange) && dateRange < 0){
				Error_Details(message_operations[117]);
			}

			if (ErrorCount > 0)
			{
				Error_Show();
				Error_Clear();
				return false;
			}
		}
		fnStartProgress();
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmLotTrackNonTissueRpt.do?method=fetchLotTrackNonTissueDetails&strOpt=Reload&strPartNumber='+encodeURIComponent(partNumber)+'&strLotNumber='+encodeURIComponent(lotNumber)+
			'&warehouseType='+warehouseType+'&expiryDateFrom='+expiryFromDate+'&expiryDateTo='+expiryToDate+'&expDateRange='+strDateRange+
			'&companyId='+strCompanyId+'&plantId='+strPlantId+'&strPartLiteral='+strSearch+'&strFieldSales='+strFieldSales+'&strSetId='+strSetId+'&strLocation='+encodeURIComponent(strLocation)+'&strExpired='+strExpired+'&ramdomId='+new Date().getTime());
	dhtmlxAjax.get(ajaxUrl,fnLoadLotAjax);
	}else{
		 Error_Details(message_operations[853]);
	    }
		if (ErrorCount > 0)
		{
			Error_Show();
			Error_Clear();
			return false;
		}
}

//This function used to generate the grid
function fnLoadLotAjax (loader){
	var partNum = '';
	var lotNum = '';
	var transId = '';
	var transType ='';
	var createDate = '';
	var origQty = '';
	var txnQty = '';
	var newQty = '';
	var locationId = '';
	var warehouseType = '';
	var companyId = '';
	var plantId = '';
	var cntlinvid = '';
	var companyName = '';
	var plantName = '';
	var response = loader.xmlDoc.responseText;
	 if(response != "") {
		 
		  //to form the grid string 
			var rows = [], i=0;
			var JSON_Array_obj = "";
			 JSON_Array_obj = JSON.parse(response);
			
			$.each(JSON_Array_obj, function(index,jsonObject){
				partNum = jsonObject.PARTNO;
				partNumDesc = jsonObject.PARTDESC;   
				lotNum = jsonObject.CNTROLNO;
				qty = jsonObject.QTY;
				expiryDate = jsonObject.EXPIRYDATE;
				locationId = jsonObject.LOCATION;
				warehouseType = jsonObject.WAREHOUSETYPE;
				warehouseTypeId = jsonObject.WAREHOUSETYPEID;
				companyName = jsonObject.COMPANYNM;
				plantName = jsonObject.PLANTNM;
				cntlinvid = jsonObject.CNTLINVID;
				companyId = jsonObject.CMPID;
				plantId = jsonObject.PLTID;
				
				// default array
		        rows[i] = {};
		        rows[i].id = i;
		        rows[i].data = [];
		        if(strAccess == "Y"){
		        	rows[i].data[0] = "<a href=\"#\" title= 'Add/Edit Lot Track Details' onClick=fnAddorEditLotTrackDetails('"+cntlinvid+"')><img alt='Edit ID' src='/images/edit.jpg' border='0'></a>";
		        }
		        else{
		        	rows[i].data[0] = "<a href=\"#\" title= 'Add/Edit Lot Track Details' onClick=fnValidateAccess()><img alt='Edit ID' src='/images/edit.jpg' border='0'></a>";
		        }
		        rows[i].data[1] = "<a href=\"#\" title= 'Lot Track Transaction Log Report' onClick=fnLotTrackTransLogReport('"+warehouseTypeId+"','"+cntlinvid+"','"+companyId+"','"+plantId+"')>LL</a>";
		        rows[i].data[2] = "<a href=\"#\" title= 'Lot Detail Report' onClick=fnLotTrackDetailReport('"+partNum+"','"+lotNum+"')>LD</a>";		    
		        rows[i].data[3] = partNum;
		        rows[i].data[4] = partNumDesc;   
		        rows[i].data[5] = lotNum;    
		        rows[i].data[6] = qty;   
		        rows[i].data[7] = expiryDate;   
		        rows[i].data[8] = locationId;   
		        rows[i].data[9] = warehouseType;   
		        rows[i].data[10] = companyName;   
		        rows[i].data[11] = plantName;   		       	
		        i++;
			});
			
			var setInitWidths = "30,30,30,100,200,100,80,100,120,100,192,*";
			
		    var setColAlign = "left,center,center,left,left,left,right,left,left,left,left,left" ;
		    var setColTypes = "ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro" ;		    		          
		    var setColSorting = "str,str,str,str,str,str,int,str,str,str,str,str";		   
		    var setHeader = [" "," "," ","Part#","Part Desc","Lot#","Qty","Expiry Date","Location","Warehouse","Company","Plant"];		  
		    var setFilter = ["","","","#text_filter","#text_filter","#text_filter","#numeric_filter","#text_filter","#text_filter","#select_filter","#select_filter","#select_filter"];
		    var setColIds = "";		 
		    var enableTooltips = "true,true,true,true,true,true,true,true,true,true,true,true";
		    var footerArry = [];
		    var gridHeight = "";
		    var footerStyles = [];
		    var footerExportFL = true;
		    var dataHost = {};
		    dataHost.rows = rows;
			document.getElementById("dataGridDiv").style.display = 'block';
			document.getElementById("DivNothingMessage").style.display = 'none';
			document.getElementById("DivExportExcel").style.display = 'block';
		    gridObj = loadDHTMLXGrid('dataGridDiv', gridHeight, dataHost, setHeader, setInitWidths, setColAlign, setColTypes, setColSorting, setColIds, enableTooltips, setFilter, footerArry, footerStyles, footerExportFL);
		  
		    }else{
			document.getElementById("dataGridDiv").style.display = 'none';
			document.getElementById("DivExportExcel").style.display = 'none';
			document.getElementById("DivNothingMessage").style.display = '';
		    }	
	  fnStopProgress();
}

//This function used to download excel
function fnDownloadXLS() {
	gridObj.setColumnHidden(0,true);
	gridObj.setColumnHidden(1,true);
	gridObj.toExcel('/phpapp/excel/generate.php');
	gridObj.setColumnHidden(0,false);
	gridObj.setColumnHidden(1,false);
}

//This function is used to load the lot track transaction log report screen when LL clicked
function fnLotTrackTransLogReport(warehousetype,strInvId,strCompanyId,strPlantId){
	windowOpener("/gmLotTrackTxnLogRpt.do?method=loadLotTrackTxnLogDetails&strOpt=Reload&warehouseType="+warehousetype+'&strControlInvId='+strInvId+
			'&companyId='+strCompanyId+'&plantId='+strPlantId,
			"Search","resizable=yes,scrollbars=yes,top=150,left=150,width=1250,height=600");
}

//This function is used to load the lot track detail report screen when LD clicked
function fnLotTrackDetailReport(partnum,lotnum){
	windowOpener("/gmLotDetailRpt.do?method=loadLotDetailsReport&strPartNumber="+encodeURIComponent(partnum)+'&strLotNumber='+encodeURIComponent(lotnum),
			"Search","resizable=yes,scrollbars=yes,top=150,left=150,width=830,height=800");
}

//This function is used to Load Add/Edit the lot track detail screen when Edit Icon clicked - PMT#54256
function fnAddorEditLotTrackDetails(cntlinvid){
	var actionURL = "/gmLotTrackNonTissueRpt.do?method=addOrEditLotTrackDetails&strDisable=disabled&strOpt=fetch&strCntrlNumInvId="+cntlinvid;
    document.frmLotTrackNonTissueRpt.action= actionURL;
    document.frmLotTrackNonTissueRpt.submit();
}

//function used to throw validation if user doesn't have access
function fnValidateAccess(){
	Error_Details(message[5062]);
	if (ErrorCount > 0) {
   		Error_Show();
   		Error_Clear();
   		return false;
   		}
}