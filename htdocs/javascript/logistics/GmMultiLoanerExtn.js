var next_dt ='';
var Sur_date = '';    
var Cur_date = ''; 
var dateCur_date='';
var Current_date='';
var next_date='';
// This function used to initial page load called functions
function fnOnPageLoad(){
	fnLoadMultiRequestAjax();
	fnLoadSelectedGrid();
}

//This function used to Load button functionality
function fnLoadLoanerDtl(){
	var vSetId = document.frmMultiLoanerExtn.strSetId.value;
	var vSetName = document.frmMultiLoanerExtn.strSetName.value;
	var vConsignId = document.frmMultiLoanerExtn.strConsignmentId.value;
	var vSalesRep = document.frmMultiLoanerExtn.Cbo_RepId.value;
	var vDistributor = document.frmMultiLoanerExtn.Cbo_DistId.value;
	
	if((vSetName != '' && vSetName != undefined) || (vSetId!='' && vSetId != undefined) || (vConsignId!='' && vConsignId != undefined) || (vSalesRep != '' && vSalesRep != undefined) || (vDistributor != '' && vDistributor != undefined) ){
		
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmMultiLoanerExtn.do?method=fetchLoanerList&strSetId='+vSetId+ '&strSetName='+ vSetName+'&strConsignmentId='+vConsignId+'&strSalesRep='+vSalesRep+'&strDistributor='+vDistributor);
	    fnStartProgress();
		dhtmlxAjax.get(ajaxUrl,fnLoadMultiRequestAjax);
	}else{
		Error_Details ('Please enter either Set Name/ Set ID/ Consignment ID/ Sales Rep/ Distributor.');
		Error_Show();
		Error_Clear();
		return false;		 
	}
}

//Declare the grid field's ids for variables 
var showImg_id ='';
var consignId = '';
var setId = '';
var setName ='';
var salesRep ='';
var distributor ='';
var loanDate = '';
var expReturnDate = '';
//added loaner # in grid data view used loanerNm in PC-4334 -PC-4334-Add Loaner Numbers to Multiple Set Extension Screen
var loanerNm = '';   

var gridObj;
var response='';
//This function used to generate the grid
function fnLoadMultiRequestAjax (loader){
	
	var strDtlsJson ='';
	var strStatus = '';
	
	if(loader == undefined || loader == null){
		strDtlsJson ='';
		
	}else{
	    strDtlsJson = loader.xmlDoc.responseText;
	    fnStopProgress();
	   if(strDtlsJson ==''){
		    Error_Details ('No data found.');  
			Error_Show();
			Error_Clear();
	   }
	}
	
	var setInitWidths = "30,120,100,230,200,200,100,100,100,*";
	var setColAlign = "center,left,left,left,left,left,left,left,left,left,left";
	var setColTypes = "ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro";
	var setColSorting = "na,str,str,str,str,str,str,str,str,str,,str,na,na";
	
	var setHeader = [" ","Consign ID","Set ID", "Set Name","Sales Rep","Distributor","Loaner #","Loan Date","Exp Return Date","  "];
	var setFilter = [" ","#text_filter","#text_filter","#text_filter","#select_filter","#select_filter","#select_filter","#text_filter","#text_filter",""];
	var setColIds = "showImg,CONSID,SETID,NAME,REPNAME,DNAME,ETCHID,LDATE,EDATE,";

	var enableTooltips = "true,true,true,true,true,true,true,true,true,true,true,true,true";
	    pagination = "";
		
		//split the functions to avoid multiple parameters passing in single function
		gridObj = initGridObject('loanerGridDiv');
		
		gridObj = fnCustomTypes(gridObj, setHeader, setColIds, setInitWidths,
				setColAlign, setColTypes, setColSorting, enableTooltips, setFilter,
				800, pagination);
		if(strDtlsJson !=''){
		gridObj = loadDhtmlxGridByJsonData(gridObj,strDtlsJson);
	   
		}
		
		showImg_id = gridObj.getColIndexById("showImg");
		consignId = gridObj.getColIndexById("CONSID");
		setId = gridObj.getColIndexById("SETID");
		setName =  gridObj.getColIndexById("NAME");
		salesRep = gridObj.getColIndexById("REPNAME"); 
		distributor = gridObj.getColIndexById("DNAME"); 
		loanerNm = gridObj.getColIndexById("ETCHID"); 
		loanDate = gridObj.getColIndexById("LDATE"); 
		expReturnDate = gridObj.getColIndexById("EDATE"); 
		
		gridObj.attachEvent("onRowSelect", doOnRowSelect);
		
	   if(strDtlsJson !=''){
		   
		   var gridLength = 10 - gridObj.getRowsNum();
		   if(gridLength > 0) {
     			   for(var i=1;i<=gridLength;i++){
	     			   fnAddGridRows();
			          }
			   }
		   gridObj.forEachRow(function(rowId) {
			   consignValue = gridObj.cellById(rowId, consignId).getValue();
			   setIdValue = gridObj.cellById(rowId, setId).getValue();
			   
			   if(consignValue !='' || setIdValue !=''){
		             gridObj.cellById(rowId,0).setValue('<img alt=\"Add\" src=\"/images/add.gif\" title=\"Add\" border=\"0\">');
			   }
		   });
		   
	      }else{
			  gridObj = loadDhtmlxGridByJsonData(gridObj,strDtlsJson);
			  showImg_id = gridObj.getColIndexById("showImg");
				for(var i=1;i<=10;i++){
					fnAddGridRows();
				}
		  }
}


//Declare the grid fields variables for get values 
var consignValue = '';
var setIdValue ='';
var setNameValue = '';
var salesRepValue = '';
var distributorValue = '';
var loanDateValue = '';
var expReturnDateValue = '';
//added loaner # in grid data view used loanerNmValue in PC-4334 -PC-4334-Add Loaner Numbers to Multiple Set Extension Screen
var loanerNmValue = '';

var strJsonValue = [];
var temJson = {};
//This function used to grid selected row when add button click 
function doOnRowSelect(rowId,cellInd){
	// strJsonValue.length check is selected set grid only 10 records show
	if(cellInd == 0 && strJsonValue.length <=9){   
		consignValue = gridObj.cellById(rowId, consignId).getValue();
	  // reqValue is empty check is empty record not allow (add) to selected set grid records
	  if(consignValue !=''){	                
	  var gridrow = gridObj.getSelectedRowId();  // Row index value / which row selected
		if(gridrow!=undefined ){
				consignValue = gridObj.cellById(rowId, consignId).getValue();
				setIdValue = gridObj.cellById(rowId, setId).getValue();
				setNameValue = gridObj.cellById(rowId, setName).getValue();
				salesRepValue = gridObj.cellById(rowId, salesRep).getValue();
				distributorValue = gridObj.cellById(rowId, distributor).getValue();
				loanerNmValue = gridObj.cellById(rowId, loanerNm).getValue();
				loanDateValue = gridObj.cellById(rowId, loanDate).getValue();
				expReturnDateValue = gridObj.cellById(rowId, expReturnDate).getValue();
				
			    gridObj.setRowTextStyle(rowId, "background-color:rgb(182, 175, 175);");  
			    gridObj.cells(rowId,0).setValue('');
			    
				temJson =  {"CONSID":consignValue,"SETID":setIdValue, "NAME":setNameValue,"REPNAME":salesRepValue,"DNAME":distributorValue,"ETCHID":loanerNmValue, "LDATE":loanDateValue,
							 "EDATE":expReturnDateValue};
				var result = true;
				strJsonValue.forEach(function(item){
				      if(item.CONSID == temJson.CONSID){
				    	  result =false;
				      }
			    });
			
			if(result){
			 strJsonValue.push(temJson);
		    }
		    fnLoadSelectedGrid(JSON.stringify(strJsonValue));
	    }
	  }
	  
	}
}

    
//To add rows into grid.
function fnAddGridRows() {
	gridObj.filterBy(0, "");// unfilters the grid while adding rows
	gridObj._f_rowsBuffer = null; // clears the cache
	var newRowId = gridObj.getUID();
	gridObj.addRow(newRowId, '');
          
}

//This Function Used to set custom types conditions in Grid
function fnCustomTypes(gObj, setHeader, setColIds, setInitWidths, setColAlign,
		setColTypes, setColSorting, enableTooltips, setFilter, gridHeight,
		pagination) {
	
	
	//set drop down values for grid body to show pttype
    combo = gObj.getCombo(3);
	combo.clear();
	combo.save();

	if (setHeader != undefined && setHeader.length > 0) {
		gObj.setHeader(setHeader);
		var colCount = setHeader.length;
	}

	if (setInitWidths != "")
		gObj.setInitWidths(setInitWidths);

	if (setColAlign != "")
		gObj.setColAlign(setColAlign);

	if (setColTypes != "")
		gObj.setColTypes(setColTypes);

	if (setColSorting != "")
		gObj.setColSorting(setColSorting);

	if (enableTooltips != "")
		gObj.enableTooltips(enableTooltips);

	if (setFilter != "")
		gObj.attachHeader(setFilter);

	if (setColIds != "") {
		gObj.setColumnIds(setColIds);
	}
	if (gridHeight != ""){
		gObj.enableAutoHeight(true, gridHeight, true);
	}
	else{
		gObj.enableAutoHeight(true, 800, true);
	}

	if (pagination == "Y") {
		gObj.enablePaging(true, 100, 10, "pagingArea", true);
		gObj.setPagingSkin("bricks");
	}
	return gObj;
}

//function used addRow for gridObj
function addRow(){
	var uid = gridObj.getUID();
	gridObj.addRow(uid,'');
}

// Selected grid data view
var gridObjSelectlnext;
function fnLoadSelectedGrid(loader){

	if(loader == undefined || loader == null){
		 strDtlsJson ='';
	}else{
		 strDtlsJson = 	loader;
		 fnStopProgress();
	}

	var setInitWidths = "30,100,80,200,120,120,100,100,100,100,100,48,*";
	var setColAlign = "center,left,left,left,left,left,left,center,center,center,center,center,center";
	var setColTypes = "ro,ro,ro,ro,ro,ro,ro,ro,ro,dhxCalendarA,dhxCalendarA,ro,ro";
	var setColSorting = "na,str,str,str,str,str,str,str,str,str,str,na,na";
	var setHeader = ["","Consign ID","Set ID", "Set Name","Sales Rep","Distributor","Loaner #","Loan Date","Exp Return Date","Surgery Date","Extn Date of Return","Status",""];
	var setFilter = [" ", "#text_filter","#text_filter","#text_filter","#select_filter","#select_filter","#select_filter","#text_filter","#text_filter","#text_filter","#text_filter","",""];
	
	var setColIds = "showImg,CONSID,SETID,NAME,REPNAME,DNAME,ETCHID,LDATE,EDATE,SDATE,ETRDATE,STATUS,hide";
	
	var enableTooltips = "true,true,true,true,true,true,true,true,true,true,true,true,false";
	
		var gridHeight = "";
		var footerStyles = [];
		var footerExportFL = true;
		pagination = "";
		format = ""
		var formatDecimal = "";
		var formatColumnIndex = "";
		
		//split the functions to avoid multiple parameters passing in single function
		gridObjSelectlnext = initGridObject('selectGridDiv');
		gridObjSelectlnext = fnCustomTypes(gridObjSelectlnext, setHeader, setColIds, setInitWidths,
				setColAlign, setColTypes, setColSorting, enableTooltips, setFilter,
				800, pagination);
		 
		  gridObjSelectlnext.attachEvent("onRowSelect", doOnRowSelectRemove);
		  
		  if(currDateFmt == 'MM/dd/yyyy'){   
		  gridObjSelectlnext.setDateFormat("%m/%d/%Y");
		  }
		  else if(currDateFmt == 'dd/MM/yyyy'){
		   gridObjSelectlnext.setDateFormat("%d/%m/%Y");
		  }
		  else if(currDateFmt == 'dd.MM.yyyy'){
		   gridObjSelectlnext.setDateFormat("%d.%m.%Y"); 
		  }
		  
		  gridObjSelectlnext.enableEditTabOnly(true);
		  gridObjSelectlnext.enableEditEvents(true, false, true);
		  gridObjSelectlnext.attachEvent("onEditCell",doOnCellEdit); 
		  
		  var hideId = gridObjSelectlnext.getColIndexById("hide");
		
		  gridObjSelectlnext.setColumnHidden(12, true);
	  
		if(strDtlsJson !=''){
		
			   gridObjSelectlnext = loadDhtmlxGridByJsonData(gridObjSelectlnext,strDtlsJson);
			   var i= 0;
				// while add selected sets showing remove button -- start
				   gridObjSelectlnext.forEachRow(function(rowId) {
						
						var saveFlag = JSON.parse(strDtlsJson)[i].hide;
					  
					    if(saveFlag !='true'){
					      gridObjSelectlnext.cellById(rowId,0).setValue('<img alt=\"Remove\" src=\"/images/red_btn_minus.gif\" border=\"0\"  title=\"Remove\" height=\"15px\" width=\"15px\">');
				         }else{
				        	 gridObjSelectlnext.setRowTextStyle(rowId, "background-color:rgb(182, 175, 175);");  
				 			 gridObjSelectlnext.setCellExcellType(rowId,9,"ro");
				 			 gridObjSelectlnext.setCellExcellType(rowId,10,"ro");
				         }
					    i++;
				   });
				   fnMouseOver(gridObjSelectlnext);   // this is used success tick icon mouse overview for show message
			  //showing remove button -- end
			   var gridLength = 10 - gridObjSelectlnext.getRowsNum();
			   
			   if(gridLength > 0) {
	     			   for(var i=1;i<=gridLength;i++){
		     			   fnAddRowsGridSelect();
				          }
				   }
		   }else{
			 gridObjSelectlnext = loadDhtmlxGridByJsonData(gridObjSelectlnext,strDtlsJson);
			 
			 showImg_id = gridObjSelectlnext.getColIndexById("showImg");
			
			for(var i=1;i<=10;i++){
				fnAddRowsGridSelect();
			}
		} 
}

//To add rows into grid.
function fnAddRowsGridSelect() {
	gridObjSelectlnext.filterBy(0, "");// unfilters the grid while adding rows
	gridObjSelectlnext._f_rowsBuffer = null; // clears the cache
	var newRowId = gridObjSelectlnext.getUID();
	gridObjSelectlnext.addRow(newRowId, '');

}


//function used addRow for remove records
function addRow(){
	var uid = gridObjSelectlnext.getUID();
	gridObjSelectlnext.addRow(uid,'');

}


var surgeryDate = '';
var surgDtObj;
//This function used to remove the selected records
function doOnRowSelectRemove(rowId,cellInd){
	
	if(cellInd == 0){
		 fnStartProgress();
		var hide = gridObjSelectlnext.cellById(rowId,11).getValue();
	    if(hide == 'true'){
	    	return false;
	    }
		
		consignValue = gridObjSelectlnext.cellById(rowId, consignId).getValue();
		
		strJsonValue = strJsonValue.filter(function( obj ){
			  return obj.CONSID != consignValue;
			});
		
		gridObjSelectlnext.deleteRow(rowId);
		   
	    gridObj.forEachRow(function(rowId) {
		   
		   var consignFGridValue = gridObj.cellById(rowId, consignId).getValue();
		 
		   if(consignValue == consignFGridValue){
			   
			     gridObj.cellById(rowId,showImg_id).setValue("<img alt=\"Add\" src=\"/images/add.gif\" title=\"Add\" border=\"0\">");
			     gridObj.setRowTextStyle(rowId,"background-color:none;");
			   }
		   });
		
		var gridLength = 10 - gridObjSelectlnext.getRowsNum();
	
		   if(gridLength > 0) {
  			   for(var i=1;i<=gridLength;i++){
	     			   fnAddRowsGridSelect();
			          }
			   }
		   fnStopProgress();
	   }
	}

//Pads number with zeros until it's length is the same as given length.
function padNumber(number) {
    var string  = '' + number;
    string      = string.length < 2 ? '0' + string : string;
    return string;
}

//function to set Repname in SalesRep filter
function fnSetRepName(setName){
	document.all.hSalesRepName.value = setName;
}

//function to set Distributor name in Distributor filter
function fnDistName(distName){
	document.all.hDistName.value = distName;
}

//this method get the date format in MM/dd/yyyy
function getDateFormatMMDDYYYY(today){
	 
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!

    var yyyy = today.getFullYear();
    if (dd < 10) {
      dd = '0' + dd;
    } 
    if (mm < 10) {
      mm = '0' + mm;
    } 
 return   today = mm + '/' + dd + '/' + yyyy;
}
//this method get the date format in dd/MM/yyyy
function getDateFormatDDMMYYYY(today){
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!

    var yyyy = today.getFullYear();
    if (dd < 10) {
      dd = '0' + dd;
    } 
    if (mm < 10) {
      mm = '0' + mm;
    } 
 return   today = dd + '/' + mm + '/' + yyyy;
}
//this method get the date format in dd.MM.yyyy
function getDotDateFmtDDMMYYYY(today){
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!

    var yyyy = today.getFullYear();
    if (dd < 10) {
      dd = '0' + dd;
    } 
    if (mm < 10) {
      mm = '0' + mm;
    } 
 return   today = dd + '.' + mm + '.' + yyyy;
}
//this function used to split the date and add + 2 days based on comp date format
function fnAddTwoWorkingDays(dateObj){
	var dateParts='';
	
	var surgerydt = dateObj.getValue();
	 date      = new Date(surgerydt);
     var today = new Date();
     day       = date.getDay();
     
     if(currDateFmt == 'MM/dd/yyyy'){
	    Sur_date = getDateFormatMMDDYYYY(date);    
        Cur_date = getDateFormatMMDDYYYY(today); 
        Current_date = new Date(); 
		next_dt = fnAddDate(Cur_date,Sur_date,day);//this function for adding next two working day 
	 	formatted = padNumber(next_dt.getMonth() +1) + '/' + padNumber(next_dt.getDate()) + '/' + padNumber(next_dt.getFullYear())
        
     }else if(currDateFmt == 'dd/MM/yyyy'){
    	 dateParts = surgerydt.split("/");
    	 date = new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0]);
		 day    = date.getDay();
		 Sur_date = getDateFormatDDMMYYYY(date);    
	     Cur_date = getDateFormatDDMMYYYY(today); 
	     dateCur_date = Cur_date.split("/");
	     Current_date = new Date(+dateCur_date[2], dateCur_date[1] - 1, +dateCur_date[0]); 
	     next_dt = fnAddDate(Cur_date,Sur_date,day);//this function for adding next two working day 
	     formatted = padNumber(next_dt.getDate()) + '/' + padNumber(next_dt.getMonth() + 1) + '/' + padNumber(next_dt.getFullYear())
     }else if(currDateFmt == 'dd.MM.yyyy'){
    	 dateParts = surgerydt.split(".");
    	 date = new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0]);
    	 Sur_date = getDotDateFmtDDMMYYYY(date);    
	     Cur_date = getDotDateFmtDDMMYYYY(today);
	     dateCur_date = Cur_date.split(".");
         Current_date = new Date(+dateCur_date[2], dateCur_date[1] - 1, +dateCur_date[0]); 
    	 next_dt = fnAddDate(Cur_date,Sur_date,day); //this function for adding next two working day 
         formatted = padNumber(next_dt.getDate()) + '.' + padNumber(next_dt.getMonth() + 1) + '.' + padNumber(next_dt.getFullYear())
     }
     return formatted;
}


//this function for adding next two working day 
function fnAddDate(Cur_date,Sur_date,day){
	 var today = new Date();
     var curent_day = today.getDay();
	if(Cur_date == Sur_date && day != 5 && day != 4){
	 	 next_date = new Date(date.setDate(date.getDate() + 2));
	  }else if(day==1 || day==2 || day==3 || day== 0){
	 	 next_date = new Date(date.setDate(date.getDate() + 2));
	  }else if(day==4){
	 	 next_date = new Date(date.setDate(date.getDate() + 4));
	  }else if(day == 6){
	 	 next_date = new Date(date.setDate(date.getDate() + 3));
	  }else if((Cur_date == Sur_date && day == 4) || day==5 || (Cur_date == Sur_date && day == 5)){
	 	 next_date = new Date(date.setDate(date.getDate() + 4));
	  }
return next_date;
}

//This function used to submit functionality - save data
function fnSubmit(){
	    var message = '';
	    gridObjSelectlnext.forEachRow(function(rowId) {
	    	var consignIdValue =  gridObjSelectlnext.cellById(rowId, 1).getValue();
			if(consignIdValue !=''){
				var surgeryDateValue = gridObjSelectlnext.cellById(rowId, 9).getValue();
				var extRtnDateValue = gridObjSelectlnext.cellById(rowId, 10).getValue();
				var strReasonTyp ='50321';
				var strComment = 'Multi Loaner Extension';
				var strReqextfl = '';
				var strApprovalFl = '';
				
				   surgDtObj = gridObjSelectlnext.cellById(rowId, 9);
				   
				   if(surgeryDateValue != '' ){
				     var res = editDateValue(surgDtObj,rowId);
				     if(res == true)
					 return true;
				   }
				CommonDateValidation(gridObjSelectlnext.cellById(rowId, 10), currDateFmt,Error_Details_Trans(message_operations[878],currDateFmt));
				if(dateDiff(currDate,gridObjSelectlnext.cellById(rowId, 10).getValue(),currDateFmt )<=0){
					message = message_operations[877];
				}
				// surgery date validation 
				gridObjSelectlnext.cellById(rowId, 9).setValue(surgeryDateValue);
				surgDtObj = gridObjSelectlnext.cellById(rowId,9);
				var loanDT = gridObjSelectlnext.cellById(rowId, 7).getValue();
				if(surgDtObj != undefined){
					var surgDT = gridObjSelectlnext.cellById(rowId, 9).getValue();
					if(surgDT!=''){
					  CommonDateValidation(surgDtObj, currDateFmt,Error_Details_Trans(message_operations[876],currDateFmt));
					  if((dateDiff(currDate,surgDT,currDateFmt)<0) || (dateDiff(loanDT,surgDT,currDateFmt)<0)){
						  message = message_operations[873];
					   }
					  var objExtnDt = gridObjSelectlnext.cellById(rowId, 10).getValue();
					  if (dateDiff(objExtnDt, surgDT, currDateFmt) > 0){
						  message = message_operations[874];
					   }
					}else{
						  message = message_operations[875];  
						 }
				}
				
				if (message!=''){
					gridObjSelectlnext.cellById(rowId,11).setValue("<img id='fail"+rowId+"' title='"+message+"' alt='failure' src='/images/delete.gif'/ border='0' height='18' width='18'>");
			    	fnMouseOver(gridObjSelectlnext);
			    	message = '';
			    	return true;
				}else{
					if((surgeryDateValue !='' && surgeryDateValue != undefined) && (extRtnDateValue !='' && extRtnDateValue != undefined)){
					
					    var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmMultiLoanerExtn.do?method=saveLoanerExtDtl&strConsignmentId='+consignIdValue+'&strExtOfDateRtn='+extRtnDateValue+'&strReasonType='+strReasonTyp
					    		 +'&strSurgeryDt='+surgeryDateValue+'&strComments='+strComment+'&strReqextfl='+strReqextfl+'&strApprovalFl='+strApprovalFl);
					 
					    fnStartProgress('Y');
					    setTimeout(function(){
					    	var loader = dhtmlxAjax.postSync(ajaxUrl);
					    	fnCallBack(loader,rowId);
					    }, 0);
					}else{
						  message = 'Please enter '+message_operations[549];
						 }
				}
				if(message !=''){
					gridObjSelectlnext.cellById(rowId,11).setValue("<img id='fail"+rowId+"' title='"+message+"' alt='failure' src='/images/delete.gif'/ border='0' height='18' width='18'>");
			    	fnMouseOver(gridObjSelectlnext);
			    	message = '';
			    	return true;
				}
		  }
	   });
  }

function fnCallBack(loader,rowId){

	var resMsg = loader.xmlDoc.responseText;
	var status = loader.xmlDoc.status;
	if(status == 200){
		if(resMsg == ''){ 
			gridObjSelectlnext.cellById(rowId,11).setValue("<img id='success"+rowId+"' title='Loaner extension submitted for approval.' alt='success' src='/images/success.gif'/  border='0' height='24' width='25'>");  
		 }else{
			 gridObjSelectlnext.cellById(rowId,11).setValue("<img id='success"+rowId+"' title='"+resMsg+"' alt='success' src='/images/success.gif'/  border='0' height='24' width='25'>");   
		 }
	
		gridObjSelectlnext.cellById(rowId,12).setValue('true');
		gridObjSelectlnext.setRowTextStyle(rowId, "background-color:rgb(182, 175, 175);");  
		gridObjSelectlnext.setCellExcellType(rowId,9,"ro");
		gridObjSelectlnext.setCellExcellType(rowId,10,"ro");
		
		fnStopProgress('Y');
    	fnMouseOver(gridObjSelectlnext);
		
		// hiding the remove button
		gridObjSelectlnext.cells(rowId,0).setValue('');
		var consignIdVal = gridObjSelectlnext.cellById(rowId,consignId).getValue();
		if(consignIdVal !=''){
		var result = false;
			strJsonValue.forEach(function(item){
			      if(item.CONSID == consignIdVal){
			    	  item["STATUS"] = "<img id='success"+rowId+"' title='"+resMsg+"' alt='\success'\ src='/images/success.gif'/  border='0' height='24' width='25'>";
			    	  item["SDATE"] = gridObjSelectlnext.cellById(rowId, 9).getValue();
			    	  item["ETRDATE"] = gridObjSelectlnext.cellById(rowId, 10).getValue();
			    	  item["showImg"] = '';
			    	  item["hide"] = 'true';
			      }
		    });
			
		if(result){
		 strJsonValue.push(temJson);
		  }
		}
		
	}else{
		gridObjSelectlnext.cellById(rowId,11).setValue("<img id='fail"+rowId+"' title='"+resMsg+"' alt='failure' src='/images/delete.gif'/ border='0' height='18' width='18'>");
    	fnMouseOver(gridObjSelectlnext);
    	return true;
	}
	
}


function fnMouseOver(gridObjSelectlnext){
	gridObjSelectlnext.attachEvent("onMouseOver",function()
		{
			return false;
		});
}

// edit the grid calendar grid column fields
var extDateRtn ='';
var surgDtObj;
var strReasonTyp = '';
var strEeqextfl = '';
function doOnCellEdit(stage, rowId, cellInd, nValue, oValue) {
	 if(nValue!=''){
		if (stage == 2) {
			var consignmentid =  gridObjSelectlnext.cellById(rowId, 1).getValue();
	    	var surgeryDateValue = gridObjSelectlnext.cellById(rowId, 9).getValue();
			var extRtnDateValue = gridObjSelectlnext.cellById(rowId, 10).getValue();
			if(consignmentid !=''){
			if(cellInd == 9){ //change the surgery date --start
				gridObjSelectlnext.cellById(rowId, 9).setValue(surgeryDateValue);
				surgDtObj = gridObjSelectlnext.cellById(rowId, 9);
			
				if(gridObjSelectlnext.cellById(rowId, 9).getValue() != ""){
					surgDtObj.value = gridObjSelectlnext.cellById(rowId, 9).getValue();
				}
			     editDateValue(surgDtObj,rowId);
			 }  //change the surgery date --end
			 if(cellInd == 10){
				 gridObjSelectlnext.cellById(rowId, 10).setValue(extRtnDateValue);
			  }
			
		    }else{
		    		gridObjSelectlnext.cellById(rowId, 9).setValue('');
		    		gridObjSelectlnext.cellById(rowId, 10).setValue('');
		    	}
			}
		return true;
	 }
}

//This function used to surgery date country based date format validation
function editDateValue(surgDtObj,rowId){
	
	if(surgDtObj != undefined && surgDtObj.value != ''){
		if(currDateFmt=='MM/dd/yyyy'){
		 LoanerextDateValidation(surgDtObj, currDateFmt,  message[1]);
		}
		if(currDateFmt=='dd/MM/yyyy'){
		 LoanerextDateValidation(surgDtObj, currDateFmt,  message[5316]);	
		}
		if(currDateFmt=='dd.MM.yyyy'){
		  LoanerextDateValidation(surgDtObj, currDateFmt,  message[5316]);
		}
		if (ErrorCount > 0){
			if(currDateFmt=='MM/dd/yyyy'){
				gridObjSelectlnext.cellById(rowId,11).setValue("<img id='fail"+rowId+"' title='"+message[1]+"' alt='failure' src='/images/delete.gif'/ border='0' height='18' width='18'>");
				ErrorCount = 0;
				fnStopProgress('Y');
		    	fnMouseOver(gridObjSelectlnext);
		    	return true;
			}
			if(currDateFmt=='dd/MM/yyyy'){
				gridObjSelectlnext.cellById(rowId,11).setValue("<img id='fail"+rowId+"' title='"+message[5316]+"' alt='failure' src='/images/delete.gif'/ border='0' height='18' width='18'>");
				ErrorCount = 0;
				fnStopProgress('Y');
		    	fnMouseOver(gridObjSelectlnext);
		    	return true;
			}
			if(currDateFmt=='dd.MM.yyyy'){
				var errMsg = 'Please enter a valid date in DD.MM.YYYY format.'; 
				gridObjSelectlnext.cellById(rowId,11).setValue("<img id='fail"+rowId+"' title='"+errMsg+"' alt='failure' src='/images/delete.gif'/ border='0' height='18' width='18'>");
				ErrorCount = 0;
				fnStopProgress('Y');
		    	fnMouseOver(gridObjSelectlnext);
		    	return true;
			}
			return false;
		}else{
			//this function used to split the date and add + 2 days  based on comp date format
			formattedDate = fnAddTwoWorkingDays(surgDtObj);
			var extDate = gridObjSelectlnext.cellById(rowId, 10).getValue();
			if(extDate ==''){
				gridObjSelectlnext.cellById(rowId, 10).setValue(formattedDate);
				extDateRtn = formattedDate;
			}
			
		}
	 }
}

//This function used to code for Loaner ext date validation
function LoanerextDateValidation(dateObj,format,msg)
{
	var returnval=false;
	var validformat='';
	var splitby='';
	var mon=0;
	var day=0;
	var year=0;
	var dhtmlxFl = 'N';
	var dateVal=dateObj.value;
	
	//To get date value from DHTMLX by using getValue() 
	if(dateVal == undefined){
		dateVal = dateObj.getValue();
		dhtmlxFl = 'Y';
	}
	if(dateVal == "" || dateVal==undefined)
		return true;
	
	switch(format){
	case 'MM/dd/yyyy': 
		validformat=/^\d{2}\/\d{2}\/\d{4}$/; 
		splitby="/";
		mon=0;
		day=1;
		year=2;
		break;
	case 'dd.MM.yyyy': 
		validformat=/^\d{2}\.\d{2}\.\d{4}$/; 
		splitby=".";
		mon=1;
		day=0;
		year=2;
		break;
	case 'dd/MM/yyyy': 
		validformat=/^\d{2}\/\d{2}\/\d{4}$/; 
		splitby="/";
		mon=1;
		day=0;
		year=2;
		break;
	case 'MM.dd.yyyy': 
		validformat=/^\d{2}\.\d{2}\.\d{4}$/; 
		splitby=".";
		mon=0;
		day=1;
		year=2;
		break;	
	case 'yyyy/MM/dd': 
		validformat=/^\d{4}\/\d{2}\/\d{2}$/;
		splitby="/";
		mon=1;
		day=2;
		year=0;
		break;
	}
	if (!validformat.test(dateVal))
	{
		Error_Details(msg);
	}
	else
	{
		var monthfield=dateVal.split(splitby)[mon];
		var dayfield=dateVal.split(splitby)[day];
		var yearfield=dateVal.split(splitby)[year];
		var intyearfield = parseInt(yearfield);
		if(intyearfield < 1900)
			Error_Details(msg);
		
		var dayobj = new Date(yearfield, monthfield-1, dayfield);
		if ((dayobj.getMonth()+1!=monthfield)||(dayobj.getDate()!=dayfield)||(dayobj.getFullYear()!=yearfield))
			Error_Details(msg);
		else
			returnval=true;
	}
}

// This is used to Reset functionality
function fnReset(){
	 fnStartProgress();
	 document.frmMultiLoanerExtn.strSetName.value='';
	 document.frmMultiLoanerExtn.strSetId.value='';
	 document.frmMultiLoanerExtn.strConsignmentId.value='';
	 document.frmMultiLoanerExtn.strConsignmentId.value='';
	 document.frmMultiLoanerExtn.searchCbo_RepId.value='';
	 document.frmMultiLoanerExtn.searchCbo_DistId.value='';
	 
	 strJsonValue = [];
	 temJson = {};
	 fnLoadMultiRequestAjax();
	 fnLoadSelectedGrid();
	 fnStopProgress();
}

