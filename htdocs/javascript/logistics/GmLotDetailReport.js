function fnOnload(){
	document.getElementById("dataGrid").style.display = 'none';
	document.getElementById("dataGridRec").style.display = 'none';
	document.getElementById("dataGridInv").style.display = 'none';
	document.getElementById("dataGridInhouse").style.display = 'none';
	document.getElementById("dataGridFieldSales").style.display = 'none';
	document.getElementById("dataGridConsignment").style.display = 'none';
	document.getElementById("dataGridLoaner").style.display = 'none';
	document.getElementById("DivNothingMessage").style.display = 'none';
	document.getElementById("DivRecNothingMessage").style.display = 'none';
	document.getElementById("DivInhouseNothingMessage").style.display = 'none';
	document.getElementById("DivFieldSalesNothingMessage").style.display = 'none';
	document.getElementById("DivConsignmentNothingMessage").style.display = 'none';
	document.getElementById("DivLoanerNothingMessage").style.display = 'none';
	
	var partNumber = document.frmLotDetailReport.strPartNumber.value;
	var lotNumber = document.frmLotDetailReport.strLotNumber.value;
	var strSearch = document.frmLotDetailReport.strPartLiteral.value;
//	var strInvType = document.frmLotDetailReport.strInvType.value;
	
	if((partNumber!='' && partNumber != undefined)  || (lotNumber!='' && lotNumber != undefined)){
		fnLoad();
	}
}

function fnLoad(){
	var partNumber = document.frmLotDetailReport.strPartNumber.value;
	var lotNumber = document.frmLotDetailReport.strLotNumber.value;
	var strSearch = document.frmLotDetailReport.strPartLiteral.value;
	fnStartProgress();
	if((partNumber!='' && partNumber != undefined)  && (lotNumber!='' && lotNumber != undefined)){		
	fnProdInfo(partNumber,lotNumber,strSearch);
	fnReceivingInfo(partNumber,lotNumber,strSearch);
	fnInvInfo(partNumber,lotNumber,strSearch);
	fnFieldSalesInfo(partNumber,lotNumber,strSearch);
	fnConsignmentInfo(partNumber,lotNumber,strSearch);
	fnLoanerInfo(partNumber,lotNumber,strSearch);
	$('.lineHide').show();	
	}else{
	 Error_Details(message_operations[855]);
	}
	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		fnStopProgress();
		return false;
	}
}
//this function is used to get the product information based on part number or lot number
function fnProdInfo(partNumber,lotNumber,strSearch){
	//fnStartProgress();
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmLotDetailRpt.do?method=fetchLotDetailProdInfo&strPartNumber='+encodeURIComponent(partNumber)+'&strLotNumber='+encodeURIComponent(lotNumber)+
			'&strPartLiteral='+strSearch+'&ramdomId='+new Date().getTime());
	dhtmlxAjax.get(ajaxUrl,fnPopulateProdInfo);
}

//This function used to generate and populate the grid values for product info on Lot track deatil report
function fnPopulateProdInfo (loader){
	var partNum = '';
	var partDesc = '';
	var lotNum = '';
	var expiryDate = '';
	var vendor = '';
	
	var response = loader.xmlDoc.responseText;

//	fnStartProgress();
	 if(response != "") {
		 
		  //to form the grid string 
			var rows = [], i=0;
			var JSON_Array_obj = "";
			 JSON_Array_obj = JSON.parse(response);
			
			$.each(JSON_Array_obj, function(index,jsonObject){
				partNum = jsonObject.PARTNO;
				partDesc = jsonObject.PARTDESC;
				lotNum = jsonObject.CNTROLNO; 
				expiryDate = jsonObject.EXPIRYDATE;
				vendor = jsonObject.VENDOR;
				
				// default array
		        rows[i] = {};
		        rows[i].id = i;
		        rows[i].data = [];
		       
		        rows[i].data[0] = partNum;
		        rows[i].data[1] = partDesc;   
		        rows[i].data[2] = lotNum;    
		        rows[i].data[3] = expiryDate;   
		        rows[i].data[4] = vendor; 
		        i++;
			});
			
			var setInitWidths = "100,200,100,100,*";
			
		    var setColAlign = "center,left,left,right,left" ;
		    var setColTypes = "ro,ro,ro,ro,ro" ;		    		          
		    var setColSorting = "str,str,str,str,str";		   
		    var setHeader = ["Part Number","Part Desc","Lot #","Expiry Date","Vendor"];		  
		    var setColIds = "";		 
		    var enableTooltips = "true,true,true,true,true";
		    var footerArry = [];
		    var gridHeight = "100";
		    var footerStyles = [];
		    var footerExportFL = true;
		    var dataHost = {};
		    dataHost.rows = rows;
			document.getElementById("dataGridDivforProductInfo").style.display = 'block';
			document.getElementById("dataGrid").style.display = '';
			document.getElementById("DivNothingMessage").style.display = 'none';
		    gridObj = loadDHTMLXGrid('dataGridDivforProductInfo', gridHeight, dataHost, setHeader, setInitWidths, setColAlign, setColTypes, setColSorting, setColIds, enableTooltips, footerArry, footerStyles, footerExportFL);
		  
		    }else{
			document.getElementById("dataGridDivforProductInfo").style.display = 'none';
			document.getElementById("DivNothingMessage").style.display = '';
			document.getElementById("dataGrid").style.display = '';
		    }	
	 // fnStopProgress();
}
//this function is used to get the Receiving information based on part number or lot number
function fnReceivingInfo(partNumber,lotNumber,strSearch){	
	//fnStartProgress();
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmLotDetailRpt.do?method=fetchLotDetailReceivingInfo&strPartNumber='+encodeURIComponent(partNumber)+'&strLotNumber='+encodeURIComponent(lotNumber)+
			'&strPartLiteral='+strSearch+'&ramdomId='+new Date().getTime());
	dhtmlxAjax.get(ajaxUrl,fnPopulateReceivingInfo);
}

//This function used to generate and populate the grid values for Receiving info on Lot track detail report
function fnPopulateReceivingInfo (loader){
	var dhrId = '';
	var qty = '';
	var packSlip = '';
	var vendor = '';
	var receivedDate = '';
	var releasedDate = '';
	var status = '';
//	fnStartProgress();
	var response = loader.xmlDoc.responseText;
	 if(response != "") {
		  //to form the grid string 
			var rows = [], i=0;
			var JSON_Array_obj = "";
			 JSON_Array_obj = JSON.parse(response);
			
			$.each(JSON_Array_obj, function(index,jsonObject){
				dhrId = jsonObject.DHRID;
				qty = jsonObject.QTY;
				packSlip = jsonObject.PACKSLIP; 
				vendor = jsonObject.VENDOR;
				receivedDate = jsonObject.RECEIVEDDATE;
				releasedDate = jsonObject.RELEASEDDATE;
				status = jsonObject.STATUS;
				
				// default array
		        rows[i] = {};
		        rows[i].id = i;
		        rows[i].data = [];
		       
		        rows[i].data[0] = dhrId;
		        rows[i].data[1] = qty;   
		        rows[i].data[2] = packSlip;    
		        rows[i].data[3] = vendor;   
		        rows[i].data[4] = receivedDate; 
		        rows[i].data[5] = releasedDate; 
		        rows[i].data[6] = status; 
		        i++;
			});
			
			var setInitWidths = "100,100,100,100,100,100*";
			
		    var setColAlign = "left,right,right,left,right,right,left" ;
		    var setColTypes = "ro,ro,ro,ro,ro,ro,ro" ;		    		          
		    var setColSorting = "str,int,str,str,str,str,str";		   
		    var setHeader = ["DHR ID","Qty","Packing Slip","Vendor","Received Date","Released Date","Status"];		  
		    var setColIds = "";		 
		    var enableTooltips = "true,true,true,true,true,true,true";
		    var footerArry = [];
		    var gridHeight = "100";
		    var footerStyles = [];
		    var footerExportFL = true;
		    var dataHost = {};
		    dataHost.rows = rows;
			document.getElementById("dataGridDivforRecInfo").style.display = 'block';
			document.getElementById("dataGridRec").style.display = '';
			document.getElementById("DivRecNothingMessage").style.display = 'none';
		    gridObj = loadDHTMLXGrid('dataGridDivforRecInfo', gridHeight, dataHost, setHeader, setInitWidths, setColAlign, setColTypes, setColSorting, setColIds, enableTooltips, footerArry, footerStyles, footerExportFL);		  
		    }else{
			document.getElementById("dataGridDivforRecInfo").style.display = 'none';
			document.getElementById("DivRecNothingMessage").style.display = '';
			document.getElementById("dataGridRec").style.display = '';
		    }	
	 // fnStopProgress();
}
//this function is used to get the Inventory information based on part number or lot number
function fnInvInfo(partNumber,lotNumber,strSearch){
	//fnStartProgress();
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmLotDetailRpt.do?method=fetchLotDetailInventoryInfo&strPartNumber='+encodeURIComponent(partNumber)+'&strLotNumber='+encodeURIComponent(lotNumber)+
			'&strPartLiteral='+strSearch+'&ramdomId='+new Date().getTime());
	dhtmlxAjax.get(ajaxUrl,fnPopulateInvInfo);
}
//This function used to generate and populate the grid values for Inventory information on Lot track detail report
function fnPopulateInvInfo (loader){
	var warehouseId = '';
	var warehouseName = '';
	var qty = '';
	var companyName = '';
	var plantName = '';
	var companyid = '';
	var plantid = '';
	var partNumber = document.frmLotDetailReport.strPartNumber.value;
	var lotNumber = document.frmLotDetailReport.strLotNumber.value;
	var strSearch = document.frmLotDetailReport.strPartLiteral.value;
//	fnStartProgress();
	var response = loader.xmlDoc.responseText;
	 if(response != "") {
		  //to form the grid string 
			var rows = [], i=0;
			var JSON_Array_obj = "";
			 JSON_Array_obj = JSON.parse(response);
			
			$.each(JSON_Array_obj, function(index,jsonObject){
				warehouseId = jsonObject.WAREHOUSEID;
				warehouseName = jsonObject.WAREHOUSETYPE;
				qty = jsonObject.QTY;
				companyName = jsonObject.COMPANYNAME; 
				plantName = jsonObject.PLANTNAME;
				companyid = jsonObject.COMPANYID; 
				plantid = jsonObject.PLANTID; 
				// default array
		        rows[i] = {};
		        rows[i].id = i;
		        rows[i].data = [];
		       
		        rows[i].data[0] = warehouseName;
		        rows[i].data[1] = "<a href=\"#\" onClick=fnLotTrackLog('"+partNumber+"','"+lotNumber+"','"+strSearch+"','"+warehouseId+"','"+companyid+"','"+plantid+"')><u>"+qty+"</u></a>";   
		        rows[i].data[2] = companyName;    
		        rows[i].data[3] = plantName;
		        i++;
			});
			var setInitWidths = "200,100,350,*";
			
		    var setColAlign = "left,right,left,left" ;
		    var setColTypes = "ro,ro,ro,ro" ;		    		          
		    var setColSorting = "str,int,str,str";		   
		    var setHeader = ["Warehouse Name","Qty","Company Name","Plant Name"];	
		    var setFilter = [ "#select_filter", "#numeric_filter", "#select_filter","#select_filter"];
		    var setColIds = "";		 
		    var enableTooltips = "true,true,true,true";
		    var footerArry = [];
		    var gridHeight = "250";
		    var footerStyles = [];
		    var footerExportFL = true;
		    var dataHost = {};
		    dataHost.rows = rows;
			document.getElementById("dataGridDivforInhouseInfo").style.display = 'block';
			document.getElementById("dataGridInv").style.display = '';
			document.getElementById("dataGridInhouse").style.display = '';
			document.getElementById("DivInhouseNothingMessage").style.display = 'none';
			gridObj = loadDHTMLXGrid('dataGridDivforInhouseInfo', gridHeight, dataHost, setHeader, setInitWidths, setColAlign, setColTypes, setColSorting, setColIds, enableTooltips, setFilter, footerArry, footerStyles, footerExportFL);		  
		    }else{
			document.getElementById("dataGridDivforInhouseInfo").style.display = 'none';
			document.getElementById("DivInhouseNothingMessage").style.display = '';
			document.getElementById("dataGridInv").style.display = '';
			document.getElementById("dataGridInhouse").style.display = '';
		    }	
	// fnStopProgress();
}
//this function is used to get the Fiels Sales information based on part number or lot number
function fnFieldSalesInfo(partNumber,lotNumber,strSearch){
	//fnStartProgress();
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmLotDetailRpt.do?method=fetchLotDetailFieldSalesInfo&strPartNumber='+encodeURIComponent(partNumber)+'&strLotNumber='+encodeURIComponent(lotNumber)+
			'&strPartLiteral='+strSearch+'&ramdomId='+new Date().getTime());
	dhtmlxAjax.get(ajaxUrl,fnPopulateFieldSalesInfo);
}
//This function used to generate and populate the grid values for Field Sales information on Lot track detail report
function fnPopulateFieldSalesInfo (loader){
	var name = '';
	var qty = '';
	var companyName = '';
	var companyid = '';
	var partNumber = document.frmLotDetailReport.strPartNumber.value;
	var lotNumber = document.frmLotDetailReport.strLotNumber.value;
	var strSearch = document.frmLotDetailReport.strPartLiteral.value;
//	fnStartProgress();	
	var response = loader.xmlDoc.responseText;
	 if(response != "") {
		  //to form the grid string 
			var rows = [], i=0;
			var JSON_Array_obj = "";
			 JSON_Array_obj = JSON.parse(response);
			
			$.each(JSON_Array_obj, function(index,jsonObject){
				name = jsonObject.NAME;
				qty = jsonObject.QTY;
				companyName = jsonObject.COMPANYNAME;
				companyid = jsonObject.COMPANYID;
				// default array
		        rows[i] = {};
		        rows[i].id = i;
		        rows[i].data = [];
		       
		        rows[i].data[0] = name;
		        rows[i].data[1] = "<a href=\"#\" onClick=fnLotTransHistory('"+partNumber+"','"+lotNumber+"','"+strSearch+"','4000339','"+companyid+"','"+encodeURIComponent(name)+"','')><u>"+qty+"</u></a>";   
		        rows[i].data[2] = companyName;   
		        i++;
			});
			var setInitWidths = "400,100,*";
			
		    var setColAlign = "left,center,left" ;
		    var setColTypes = "ro,ro,ro" ;		    		          
		    var setColSorting = "str,int,str";		   
		    var setHeader = ["Name","Qty","Company Name"];	
		    var setFilter = [ "#text_filter", "#numeric_filter", "#select_filter"];
		    var setColIds = "";		 
		    var enableTooltips = "true,true,true";
		    var footerArry = [];
		    var gridHeight = "250";
		    var footerStyles = [];
		    var footerExportFL = true;
		    var dataHost = {};
		    dataHost.rows = rows;
			document.getElementById("dataGridDivforFieldSalesInfo").style.display = 'block';
			document.getElementById("DivFieldSalesNothingMessage").style.display = 'none';
			document.getElementById("dataGridFieldSales").style.display = '';
			gridObj = loadDHTMLXGrid('dataGridDivforFieldSalesInfo', gridHeight, dataHost, setHeader, setInitWidths, setColAlign, setColTypes, setColSorting, setColIds, enableTooltips, setFilter, footerArry, footerStyles, footerExportFL);		  
		    }else{
			document.getElementById("dataGridDivforFieldSalesInfo").style.display = 'none';
			document.getElementById("DivFieldSalesNothingMessage").style.display = '';
			document.getElementById("dataGridFieldSales").style.display = '';
		    }	
	// fnStopProgress();
}

//This function is used to get Consignment information
function fnConsignmentInfo(partNumber,lotNumber,strSearch){
	//fnStartProgress();
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmLotDetailRpt.do?method=fetchLotDetailConsignmentInfo&strPartNumber='+encodeURIComponent(partNumber)+'&strLotNumber='+encodeURIComponent(lotNumber)+
			'&strPartLiteral='+strSearch+'&ramdomId='+new Date().getTime());
	dhtmlxAjax.get(ajaxUrl,fnPopulateConsignmentInfo);
}
//This function used to generate and populate the grid values for Consignment information on Lot track detail report
function fnPopulateConsignmentInfo (loader){
	var setId = '';
	var setName = '';
	var qty = '';
	var companyName = '';
	var plantName = '';
	var companyId = '';
	var plantid = '';
	var partNumber = document.frmLotDetailReport.strPartNumber.value;
	var lotNumber = document.frmLotDetailReport.strLotNumber.value;
	var strSearch = document.frmLotDetailReport.strPartLiteral.value;
//	fnStartProgress();	
	var response = loader.xmlDoc.responseText;
	 if(response != "") {
		  //to form the grid string 
			var rows = [], i=0;
			var JSON_Array_obj = "";
			 JSON_Array_obj = JSON.parse(response);
			
			$.each(JSON_Array_obj, function(index,jsonObject){
				setId = jsonObject.SETID;
				setName = jsonObject.SETNAME;
				qty = jsonObject.QTY;
				companyName = jsonObject.COMPANYNAME;
				plantName = jsonObject.PLANTNAME; 
				companyid = jsonObject.COMPANYID; 
				plantid = jsonObject.PLANTID; 
				// default array
		        rows[i] = {};
		        rows[i].id = i;
		        rows[i].data = [];
		       
		        rows[i].data[0] = setId;
		        rows[i].data[1] = setName;   
		        rows[i].data[2] = "<a href=\"#\" onClick=fnLotTransHistory('"+partNumber+"','"+lotNumber+"','"+strSearch+"','4311','"+companyid+"','"+plantid+"','"+setId+"')><u>"+qty+"</u></a>";
		        rows[i].data[3] = companyName;   
		        rows[i].data[4] = plantName;   
		        i++;
			});
			var setInitWidths = "150,200,100,200,*";
			
		    var setColAlign = "center,left,center,left,center" ;
		    var setColTypes = "ro,ro,ro,ro,ro" ;		    		          
		    var setColSorting = "str,str,int,str,str";		   
		    var setHeader = ["Set ID","Set Name","Qty","Company Name","Plant Name"];
		    var setFilter = [ "#text_filter", "#text_filter", "#numeric_filter","#select_filter", "#select_filter"];
		    var setColIds = "";		 
		    var enableTooltips = "true,true,true,true,true";
		    var footerArry = [];
		    var gridHeight = "250";
		    var footerStyles = [];
		    var footerExportFL = true;
		    var dataHost = {};
		    dataHost.rows = rows;
			document.getElementById("dataGridDivforConsignmentInfo").style.display = 'block';
			document.getElementById("DivConsignmentNothingMessage").style.display = 'none';
			document.getElementById("dataGridConsignment").style.display = '';
			gridObj = loadDHTMLXGrid('dataGridDivforConsignmentInfo', gridHeight, dataHost, setHeader, setInitWidths, setColAlign, setColTypes, setColSorting, setColIds, enableTooltips, setFilter, footerArry, footerStyles, footerExportFL);		  
		    }else{
			document.getElementById("dataGridDivforConsignmentInfo").style.display = 'none';
			document.getElementById("DivConsignmentNothingMessage").style.display = '';
			document.getElementById("dataGridConsignment").style.display = '';
		    }	
	// fnStopProgress();
}
//This function is used to get loaner information based on part or lot number
function fnLoanerInfo(partNumber,lotNumber,strSearch){
	//fnStartProgress();
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmLotDetailRpt.do?method=fetchLotDetailLoanerInfo&strPartNumber='+encodeURIComponent(partNumber)+'&strLotNumber='+encodeURIComponent(lotNumber)+
			'&strPartLiteral='+strSearch+'&ramdomId='+new Date().getTime());
	dhtmlxAjax.get(ajaxUrl,fnPopulateLoanerInfo);
	fnStopProgress();
}
//This function used to generate and populate the grid values for Loaner information on Lot track detail report
function fnPopulateLoanerInfo (loader){
	var setId = '';
	var setName = '';
	var qty = '';
	var companyName = '';
	var plantName = '';
	var companyId = '';
	var plantId = '';
	var partNumber = document.frmLotDetailReport.strPartNumber.value;
	var lotNumber = document.frmLotDetailReport.strLotNumber.value;
	var strSearch = document.frmLotDetailReport.strPartLiteral.value;
//	fnStartProgress();	
	var response = loader.xmlDoc.responseText;
	 if(response != "") {
		  //to form the grid string 
			var rows = [], i=0;
			var JSON_Array_obj = "";
			 JSON_Array_obj = JSON.parse(response);
			
			$.each(JSON_Array_obj, function(index,jsonObject){
				setId = jsonObject.SETID;
				setName = jsonObject.SETNAME;
				qty = jsonObject.QTY;
				companyName = jsonObject.COMPANYNAME;
				plantName = jsonObject.PLANTNAME; 
				companyId = jsonObject.COMPANYID;
				plantId = jsonObject.PLANTID;
				// default array
		        rows[i] = {};
		        rows[i].id = i;
		        rows[i].data = [];
		       
		        rows[i].data[0] = setId;
		        rows[i].data[1] = setName;   
		        rows[i].data[2] = "<a href=\"#\" onClick=fnLotTransHistory('"+partNumber+"','"+lotNumber+"','"+strSearch+"','106221','"+companyId+"','"+plantId+"','"+setId+"')><u>"+qty+"</u></a>";  
		        rows[i].data[3] = companyName;   
		        rows[i].data[4] = plantName;   
		        i++;
			});
			var setInitWidths = "150,200,100,200,*";
			
		    var setColAlign = "center,left,center,left,center" ;
		    var setColTypes = "ro,ro,ro,ro,ro" ;		    		          
		    var setColSorting = "str,str,int,str,str";		   
		    var setHeader = ["Set ID","Set Name","Qty","Company Name","Plant Name"];	
		    var setFilter = [ "#text_filter", "#text_filter", "#numeric_filter","#text_filter", "#select_filter"];
		    var setColIds = "";		 
		    var enableTooltips = "true,true,true,true,true";
		    var footerArry = [];
		    var gridHeight = "250";
		    var footerStyles = [];
		    var footerExportFL = true;
		    var dataHost = {};
		    dataHost.rows = rows;
			document.getElementById("dataGridDivforLoanerInfo").style.display = 'block';
			document.getElementById("DivLoanerNothingMessage").style.display = 'none';
			document.getElementById("dataGridLoaner").style.display = '';
			gridObj = loadDHTMLXGrid('dataGridDivforLoanerInfo', gridHeight, dataHost, setHeader, setInitWidths, setColAlign, setColTypes, setColSorting, setColIds, enableTooltips, setFilter, footerArry, footerStyles, footerExportFL);		  
		    }else{
			document.getElementById("dataGridDivforLoanerInfo").style.display = 'none';
			document.getElementById("DivLoanerNothingMessage").style.display = '';
			document.getElementById("dataGridLoaner").style.display = '';
		    }	
	// fnStopProgress();
}
//This function is used to show the Lot track Non Tissue report when Warehouse qty clicked on lot track detail report
function fnLotTrackLog(partnum,lotnum,strSearch,strWarehouseId,strCompanyId,strPlantId){
	windowOpener("/gmLotTrackNonTissueRpt.do?method=loadLotTrackNonTissueDetails&strPartNumber="+encodeURIComponent(partnum)+'&strLotNumber='+encodeURIComponent(lotnum)
			+'&strPartLiteral='+strSearch+'&warehouseType='+strWarehouseId+'&companyId='+strCompanyId+'&plantId='+strPlantId+'&strOpt=ReLoad',
			"Search","resizable=yes,scrollbars=yes,top=150,left=150,width=1230,height=600");
}
//This function is used to show the Transaction History report when Filed sales,Consignment and Loaner qty clicked on lot track detail report
function fnLotTransHistory(partnum,lotnum,strSearch,strInvType,strCompanyId,name,strSetId){
	windowOpener("/gmLotDetailRpt.do?method=loadLotTransHistory&strPartNumber="+encodeURIComponent(partnum)+'&strLotNumber='+encodeURIComponent(lotnum)
			+'&strPartLiteral='+strSearch+'&strInvType='+strInvType+'&companyId='+strCompanyId+'&fieldSalesNm='+encodeURIComponent(name)+
			'&strSetId='+strSetId,
			"Search","resizable=yes,scrollbars=yes,top=150,left=150,width=830,height=600");	
}
