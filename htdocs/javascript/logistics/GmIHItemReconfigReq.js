//members 
this.valArray = new Array(); // Location Values 
         
// methods 
this.put = put; 
this.get = get; 


function put(val){ 
   this.valArray.push( val ); 
} 
 
function get() { 
    var result = null;  
    for( var i = 0; i < this.valArray.length; i++ ) { 
        result = this.valArray[ i ]; 
    } 
    return result; 
}

function initGridData(divRef,gridData){
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);
	gObj.enableTooltips("true,true,true,true,true,true");
	gObj.init();		
	gObj.loadXMLString(gridData);	
	return gObj;	
}

//Description: This function used for initializing the filter values gridData.
function fnOnPageLoad(){
	gridObj = initGridData('GridData',objGridData);	
}

function fnGetPartQty(i,obj,pnum){
	var replenishID = obj.value;
	var reqtype = document.frmIHItemRequest.requestTypeID.value;
	var partnumber = pnum;
	if(replenishID == '102754' || replenishID == '0'){
		document.getElementById("partNumQty"+i).innerHTML = "";	
	}else{
		if(replenishID != ''){
			var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmIHItemRequest.do?strOpt=InvPartQty&replenish='+replenishID+'&requestTypeID='+reqtype+'&partNum='+encodeURIComponent(partnumber)+'&ramdomId='+Math.random());
			dhtmlxAjax.get(ajaxUrl,function(loader)
					{
						var response = loader.xmlDoc.responseText;
						document.getElementById("partNumQty"+i).innerHTML = response;	
					});
		}
	}
	
}

function fnSubmit(){
	var errPnums = "";
	var errQty   = "";
	var errInvQty   = "";
	var errReplenishQty   = "";
	var errRepFrmQty   = "";
	var hcnt = parseInt(document.getElementById("hCnt").value);
	cnt  = parseInt(document.getElementById("hNewCnt").value);	
	hcnt = hcnt+cnt;
	var shelfinputString   = "";	
	var ihshelfinputString = "";
	var bulkinputString    = "";
	var voidinputString    = "";
	var inputString        = "";
	var replenishQty = 0;
	
	fnResetReplenishQty();
		
	for(var i = 0; i <= hcnt; i++ ){		
		var pnumobj     = document.getElementById("hPartNum"+i);
		var txtqtyobj   = document.getElementById("Txt_Qty"+i); 
		var dropdownobj = document.getElementById("replenish"+i);
		var pnum    = "";
		var qty     = "";	
		var dropdn  = "";
		var replenishFrom = (dropdownobj)?dropdownobj.value:'0';
		if((replenishFrom == '102754') ) {
			dropdn = "V";
		}else{
			dropdn = "R";
		}		
		if(pnumobj&&txtqtyobj){
			pnum = pnumobj.value;
		 	qty  = parseInt(txtqtyobj.value);
			if (qty > 0 && qty != ""){
				var invQty = TRIM(document.getElementById("partNumQty"+i).innerHTML);
				var pnumstr = pnum.replace(/\./g,'');
				var objPendQty      = document.getElementById("hQty"+pnumstr);
				var objReplenishQty      = document.getElementById("hReplenishQty"+pnumstr);
				var pendQty = parseInt(objPendQty.value);
				var replenishQty = parseInt(objReplenishQty.value);
				if(eval(invQty)<qty &&  replenishFrom != '102754'){/*validate process qty with inventory qty. */
					errInvQty += (errInvQty.indexOf(pnum) != -1)? "":", "+pnum;
				}
				
				if(replenishFrom == '0'){ /*dropdown validation*/
					errRepFrmQty += (errRepFrmQty.indexOf(pnum) != -1)? "":", "+pnum;
				}
				
				replenishQty = replenishQty + qty;
				if(pendQty < replenishQty){
					errReplenishQty += (errReplenishQty.indexOf(pnum) != -1)? "":", "+pnum;
				}
				var requestTypeID=document.frmIHItemRequest.requestTypeID.value;
				if (requestTypeID == '1006483'){//IHRQ
			 		if(replenishFrom == '102751'){//SHELF
			 			shelfinputString = shelfinputString +pnum+"^"+qty+"^9106|";
			 		}else if(replenishFrom == '102752'){//IH Shelf
			 			ihshelfinputString = ihshelfinputString +pnum+"^"+qty+"^9116|";
			 		}else if(replenishFrom == '102753'){//Bulk
			 			bulkinputString = bulkinputString +pnum+"^"+qty+"^9104|";
			 		}else if(replenishFrom == '102754'){//Void
			 			voidinputString = voidinputString +pnum+"^"+qty+"^9117|";
			 		}
				}
				if (requestTypeID == '1006572'){ //ISBO
			 		if(replenishFrom == '102751'){//SHELF
			 			shelfinputString = shelfinputString +pnum+"^"+qty+"^50150|";
			 		}else if(replenishFrom == '102752'){//IH Shelf
			 			ihshelfinputString = ihshelfinputString +pnum+"^"+qty+"^1006573|";
			 		}else if(replenishFrom == '102753'){//Bulk
			 			bulkinputString = bulkinputString +pnum+"^"+qty+"^1006570|";
			 		}else if(replenishFrom == '102754'){//Void
			 			Error_Details("Cannot perform this Action");
			 		}
				}
		 		objReplenishQty.value = replenishQty;
		 	}else if(TRIM(txtqtyobj.value)!= '' || replenishFrom != '0'){
				errQty += (errQty.indexOf(pnum) != -1)? "":", "+pnum;							
			}		 	
		}			
	}	
	
	if(errQty != ''){
		Error_Details(Error_Details_Trans(message_operations[531],errQty.substr(1,errQty.length)));
	}else if(errRepFrmQty != ''){
		Error_Details(Error_Details_Trans(message_operations[532],errRepFrmQty.substr(1,errRepFrmQty.length)));
	}else if(errInvQty != ''){
		Error_Details(Error_Details_Trans(message_operations[533],errInvQty.substr(1,errInvQty.length)));
	}else if(errReplenishQty != ''){
		Error_Details(Error_Details_Trans(message_operations[534],errReplenishQty.substr(1,errReplenishQty.length)));
	}else if(shelfinputString == '' && ihshelfinputString == '' && bulkinputString == '' && voidinputString == ''){
		Error_Details(message_operations[535]);
	}
	
	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}
	document.frmIHItemRequest.strOpt.value = 'Save';
	document.frmIHItemRequest.strFGString.value = shelfinputString;
	document.frmIHItemRequest.strBLString.value = bulkinputString;
	document.frmIHItemRequest.strIHString.value = ihshelfinputString;
	document.frmIHItemRequest.strVDString.value = voidinputString;
	document.frmIHItemRequest.btnSubmit.disabled = true;
	fnStartProgress('Y');
	document.frmIHItemRequest.submit();
	
}

function fnResetReplenishQty(){
		var allPnums = document.all.hAllPnums.value;
		var errArray = allPnums.substr(1,allPnums.length).split(",");
		for(var i=0;i<errArray.length;i++){
			var pnumstr = TRIM(errArray[i]);
			if(pnumstr != null || pnumstr == ''){
				var objReplenishQty      = document.getElementById("hReplenishQty"+pnumstr);
				objReplenishQty.value = '0';
			}
		}
}

function fnSplit(val,pnum,pendqty){
	 var pnumstr = pnum.replace(/\./g,'');
     var objSplitQty = document.getElementById("hSplitQty"+pnumstr);
     var splitQty = eval(objSplitQty.value);
	if(splitQty <= 0){
		Error_Details(Error_Details_Trans(message_operations[536],pnum));
		Error_Show();
		Error_Clear();
	}else{	
		var strSelect = "";
		cnt = parseInt(document.getElementById("hNewCnt").value);
		cnt++;
		document.getElementById("hNewCnt").value = cnt;
		var hcnt = parseInt(document.getElementById("hCnt").value);
		hcnt = hcnt+cnt;	
		
		var partNum		=  document.getElementById("hPartNum"+val);
		var dropdownObj = eval("document.all.Dropdown"+val);
		var partQty     = eval("document.all.partqty"+val); 
		var textQty     = eval("document.all.Qty"+val);
		var NewDropDnHtml  = "";
		var NewPartQtyHtml = "";
		var NewTextQtyHtml = "";	
		var part = partNum.value;
		
		NewDropDnHtml = "<span id='Dropdown"+hcnt+"'><BR><select style='width:105px'  name='replenish"+hcnt+"' class=RightText tabindex='-1' onChange=fnGetPartQty("+hcnt+",this,'"+part+"');>"+get()+"</select></span>";
		dropdownObj.insertAdjacentHTML("beforeEnd",NewDropDnHtml+"</span>");
		NewPartQtyHtml = "<span id='partqty"+hcnt+"'><BR><BR><span id ='partNumQty"+hcnt+"'></span>";	
		partQty.insertAdjacentHTML("beforeEnd",NewPartQtyHtml+"</span>");
		NewTextQtyHtml = "<span id='Qty"+hcnt+"'><BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='hidden' name='hPartNum"+hcnt+"' value='"+pnum+"'><input type='text' size='2' value='' name='Txt_Qty"+hcnt+"' class='InputArea' onFocus=changeBgColor(this,'#AACCE8'); onBlur=\"changeBgColor(this,'#ffffff');\" tabindex=3>";
		NewTextQtyHtml = NewTextQtyHtml +"&nbsp;<a href=javascript:fnDelete("+hcnt+"); ><img src=/images/btn_remove.gif border=0></a>";
		textQty.insertAdjacentHTML("beforeEnd",NewTextQtyHtml+"</span>");
		
		objSplitQty.value = splitQty -1;
	}
}

function fnDelete(val){	
	var ObjDropdn = eval("document.all.Dropdown"+val);
	var ObjPart = eval("document.all.partqty"+val);
	var ObjDes = eval("document.all.Qty"+val);	
	var ObjPnum = eval("document.all.hPartNum"+val);
	var pnum = ObjPnum.value;
	var pnumstr = pnum.replace(/\./g,'');
	var objSplitQty = document.getElementById("hSplitQty"+pnumstr);
	var splitQty = eval(objSplitQty.value);
	if(ObjPart){
		ObjDropdn.innerHTML = "";
		ObjPart.innerHTML = "";
		ObjDes.innerHTML = "";
		objSplitQty.value = splitQty + 1;
	}
}

function fnPicSlip(val){
	windowOpener("/GmConsignInHouseServlet?hAction=PicSlip&hConsignId="+val,"SetPic","resizable=yes,scrollbars=yes,top=150,left=150,width=750,height=500");
}



function fnShowHide(val){
	var obj = eval("document.all."+val);
	var obj1 = eval("document.all."+val+"img");
	if (obj.style.display == 'none'){
		obj.style.display = 'block';
		if(obj1){
			obj1.src = '/images/minus.gif';
		}
	}
	else{
		obj.style.display = 'none';	
		if(obj1){
			obj1.src = '/images/plus.gif';
		}
	}
}
function fnReview(){
	document.frmIHItemRequest.strOpt.value = 'Review';
	document.frmIHItemRequest.submit();
}