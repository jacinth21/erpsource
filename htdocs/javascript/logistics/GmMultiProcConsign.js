// This function used to initial page load called functions
function fnOnPageLoad(){
	fnLoadMultiRequestAjax();
	fnLoadSelectedGrid();
	fnEnableShip();
	clearMsg();
}

//This function used to Load button functionality
function fnLoad(){
	clearMsg();
	var vSetName = document.frmMultiProcConsign.strSetName.value;

	var vSetId = document.frmMultiProcConsign.strSetId.value;

	var vConsignId = document.frmMultiProcConsign.strConsignmentId.value;
	
	var vStatus = document.frmMultiProcConsign.strStatus.value;
	
	var vRequestId = document.frmMultiProcConsign.strReqId.value;
	
	if((vSetName != '' && vSetName != undefined) || (vSetId!='' && vSetId != undefined) || (vConsignId!='' && vConsignId != undefined) || (vRequestId != '' && vRequestId != undefined)){
		
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmMultiProcConsign.do?method=loadMultiSetDetails&strSetId='+vSetId+ '&strSetName='+ vSetName+'&strStatus='+vStatus+'&strConsignmentId='+vConsignId+'&strReqId='+vRequestId);
	fnStartProgress();
    var loader = dhtmlxAjax.getSync(ajaxUrl);
		fnLoadMultiRequestAjax(loader);
		
	}else{
		Error_Details ('Please enter either Set ID/ Set Name/ Consignment ID/ Request ID.');
		Error_Show();
		Error_Clear();
		return false;		 
	}
}

//Declare the grid field's ids for variables 
var showImg_id ='';
var reqId = '';
var consignId = '';
var setId = '';
var setName ='';
var requestFor ='';
var requestDate ='';
var requiredDate = '';
var requestTo = '';
var source = '';
var RQStatus = '';
var CNStatus = '';
var sourceId = '';

var gridObj;
var response='';
//This function used to generate the grid
function fnLoadMultiRequestAjax (loader){
	
	var strDtlsJson ='';
	
	if(loader == undefined || loader == null){
		strDtlsJson ='';
		
	}else{
	    strDtlsJson = loader.xmlDoc.responseText;
	    fnStopProgress();
	   if(strDtlsJson ==''){
		    Error_Details ('Please enter the valid data.');
			Error_Show();
			Error_Clear();
	   }
	}
	
	var setInitWidths = "100,100,70,200,100,100,80,100,100,100,100,*";
	var setColAlign = "left,left,right,left,left,left,left,left,left,left,left,center";
	var setColTypes = "ro,ro,ed,ro,ro,ro,ro,ro,ro,ro,ro,ro";
	var setColSorting = "na,na,na,na,na,na,na,na,na,na,na,na";
	var setHeader = ["Req ID","Consign ID","Set ID", "Set Name","Request for","Request Date","Required Date","Request To","Source","RQ Status","CN Status"," "];
	var setFilter = ["#text_filter", "#text_filter","#text_filter","#text_filter","#text_filter","#text_filter","#text_filter","#text_filter","#text_filter","#text_filter","#text_filter",""];
	var setColIds = "REQID,CONID,SETID,NAME,REQFOR,REQDT,REQUDT,REQTO,REQSRC,RQSTATUS,CNSTATUS,showImg";

	var enableTooltips = "true,true,true,true,true,true,true,true,true,true,true,true";
	    pagination = "";
		
		//split the functions to avoid multiple parameters passing in single function
		gridObj = initGridObject('dataGridDiv');
		gridObj = fnCustomTypes(gridObj, setHeader, setColIds, setInitWidths,
				setColAlign, setColTypes, setColSorting, enableTooltips, setFilter,
				400, pagination);
		showImg_id = gridObj.getColIndexById("showImg");
		reqId = gridObj.getColIndexById("REQID");
		consignId = gridObj.getColIndexById("CONID");
		setId = gridObj.getColIndexById("SETID");
		setName =  gridObj.getColIndexById("NAME");
		requestFor = gridObj.getColIndexById("REQFOR"); 
		requestDate = gridObj.getColIndexById("REQDT"); 
		requiredDate = gridObj.getColIndexById("REQUDT"); 
		requestTo = gridObj.getColIndexById("REQTO"); 
		source = gridObj.getColIndexById("REQSRC"); 
		RQStatus = gridObj.getColIndexById("RQSTATUS"); 
		CNStatus = gridObj.getColIndexById("CNSTATUS"); 
		
		gridObj.attachEvent("onRowSelect", doOnRowSelect);
		
	   if(strDtlsJson !=''){
		   
		   gridObj = loadDhtmlxGridByJsonData(gridObj,strDtlsJson);
		   var gridLength = 10 - gridObj.getRowsNum();
		   if(gridLength > 0) {
     			   for(var i=1;i<=gridLength;i++){
	     			   fnAddGridRows();
			          }
			   }
		   gridObj.forEachRow(function(rowId) {
			   reqValue = gridObj.cellById(rowId, reqId).getValue();
				consignValue = gridObj.cellById(rowId, consignId).getValue();
			   
			   if(reqValue !='' || consignValue !=''){
		             gridObj.cellById(rowId,11).setValue('<img alt=\"add\" src=\"/images/add.gif\" border=\"0\">');
			   }
		   });
		   
	      }else{
			  gridObj = loadDhtmlxGridByJsonData(gridObj,strDtlsJson);
			  showImg_id = gridObj.getColIndexById("showImg");
				for(var i=1;i<=10;i++){
					fnAddGridRows();
				}
		  }
}
	   


//Declare the grid fields variables for get values 
var reqValue = '';
var consignValue = '';
var setIdValue ='';
var setNameValue = '';
var requestForValue = '';
var requestDateValue = '';
var requiredDateValue = '';
var requestToValue = '';
var sourceValue = '';
var RQStatusValue = '';
var CNStatusValue = '';
var sourceIdValue = '';

var strJsonValue = [];

var temJson = {};
//This function used to grid selected row when add button click 
function doOnRowSelect(rowId,cellInd){
	// strJsonValue.length check is selected set grid only 10 records show
	if(cellInd == 11 && strJsonValue.length <=9){   
	  reqValue = gridObj.cellById(rowId, reqId).getValue();
	  // reqValue is empty check is empty record not allow (add) to selected set grid records
	  if(reqValue !=''){	                
	  var gridrow = gridObj.getSelectedRowId();  // Row index value / which row selected
		if(gridrow!=undefined ){
				reqValue = gridObj.cellById(rowId, reqId).getValue();
				consignValue = gridObj.cellById(rowId, consignId).getValue();
				setIdValue = gridObj.cellById(rowId, setId).getValue();
				setNameValue = gridObj.cellById(rowId, setName).getValue();
				requestForValue = gridObj.cellById(rowId, requestFor).getValue();
				requestDateValue = gridObj.cellById(rowId, requestDate).getValue();
				requiredDateValue = gridObj.cellById(rowId, requiredDate).getValue();
				requestToValue = gridObj.cellById(rowId, requestTo).getValue();
				sourceValue = gridObj.cellById(rowId, source).getValue();
				RQStatusValue = gridObj.cellById(rowId, RQStatus).getValue();
				CNStatusValue = gridObj.cellById(rowId, CNStatus).getValue();
				
			    gridObj.setRowTextStyle(rowId, "background-color:rgb(182, 175, 175);");  
			  
			    gridObj.cells(rowId,11).setValue('');
			   
				temJson =  {"REQID":reqValue,"REQDT":requestDateValue, "SETID":setIdValue, "NAME":setNameValue,"CONID":consignValue, "REQUDT":requiredDateValue,"REQSRC":sourceValue, "REQFOR":requestForValue,
							 "RQSTATUS":RQStatusValue,"CNSTFL":CNStatusValue, "CNSTATUS":CNStatusValue,"REQTO":requestToValue,"STFLG":""};
				var result = true;
				strJsonValue.forEach(function(item){
				      if(item.REQID == temJson.REQID){
				    	  result =false;
				      }
			    });
			
			if(result){
			 strJsonValue.push(temJson);
		    }
		    fnLoadSelectedGrid(JSON.stringify(strJsonValue));
	    }
	  }
	}
}

    
//To add rows into grid.
function fnAddGridRows() {
	gridObj.filterBy(0, "");// unfilters the grid while adding rows
	gridObj._f_rowsBuffer = null; // clears the cache
	var newRowId = gridObj.getUID();
	gridObj.addRow(newRowId, '');
	
	gridObj.cellById(newRowId,showImg_id).setValue("<img alt=\"Add\" src=\"/images/add.gif\" border=\"0\">");
          
}

//This Function Used to set custom types conditions in Grid
function fnCustomTypes(gObj, setHeader, setColIds, setInitWidths, setColAlign,
		setColTypes, setColSorting, enableTooltips, setFilter, gridHeight,
		pagination) {
	
	//set drop down values for grid body to show pttype
    combo = gObj.getCombo(3);
	combo.clear();
	combo.save();

	if (setHeader != undefined && setHeader.length > 0) {
		gObj.setHeader(setHeader);
		var colCount = setHeader.length;
	}

	if (setInitWidths != "")
		gObj.setInitWidths(setInitWidths);

	if (setColAlign != "")
		gObj.setColAlign(setColAlign);

	if (setColTypes != "")
		gObj.setColTypes(setColTypes);

	if (setColSorting != "")
		gObj.setColSorting(setColSorting);

	if (enableTooltips != "")
		gObj.enableTooltips(enableTooltips);

	if (setFilter != "")
		gObj.attachHeader(setFilter);

	if (setColIds != "") {
		gObj.setColumnIds(setColIds);
	}

	if (gridHeight != "")
		gObj.enableAutoHeight(true, gridHeight, true);
	else
		gObj.enableAutoHeight(true, 400, true);

	if (pagination == "Y") {
		gObj.enablePaging(true, 100, 10, "pagingArea", true);
		gObj.setPagingSkin("bricks");
	}
	return gObj;
}

//function used addRow for gridObj
function addRow(){
	var uid = gridObj.getUID();
	gridObj.addRow(uid,'');
}

// Selected grid data view
var gridObjSelection;
function fnLoadSelectedGrid(loader){

	if(loader == undefined || loader == null){
		 strDtlsJson ='';
	}else{
		 strDtlsJson = 	loader;
	}

	var setInitWidths = "100,100,70,200,100,100,80,100,100,100,100,*";
	
	var setColAlign = "left,left,right,left,left,left,left,left,left,left,left,center";
	var setColTypes = "ro,ro,ed,ro,ro,ro,ro,ro,ro,ro,ro,Remove";
	var setColSorting = "na,na,na,na,na,na,na,na,na,na,na,na";
	var setHeader = ["Req ID","Consign ID","Set ID", "Set Name","Request for","Request Date","Required Date","Request To","Source","RQ Status","CN Status"," "];
	var setFilter = ["#text_filter", "#text_filter","#text_filter","#text_filter","#text_filter","#text_filter","#text_filter","#text_filter","#text_filter","#text_filter","#text_filter",""];
	
	
	var setColIds = "REQID,CONID,SETID,NAME,REQFOR,REQDT,REQUDT,REQTO,REQSRC,RQSTATUS,CNSTATUS,showImg";
	
	var enableTooltips = "true,true,true,true,true,true,true,true,true,true,true,true";
	    
		var gridHeight = "";
		var footerStyles = [];
		var footerExportFL = true;
		pagination = "";
		format = ""
		var formatDecimal = "";
		var formatColumnIndex = "";
		
		
		//split the functions to avoid multiple parameters passing in single function
		gridObjSelection = initGridObject('dataGridDiv2');
		gridObjSelection = fnCustomTypes(gridObjSelection, setHeader, setColIds, setInitWidths,
				setColAlign, setColTypes, setColSorting, enableTooltips, setFilter,
				400, pagination);
		
		 
		  gridObjSelection.attachEvent("onRowSelect", doOnRowSelectRemove);
		  gridObjSelection.attachEvent("onRowSelect", doOnRowSelectDetails);
	  
		if(strDtlsJson !=''){
			   gridObjSelection = loadDhtmlxGridByJsonData(gridObjSelection,strDtlsJson);
			   var gridLength = 10 - gridObjSelection.getRowsNum();
			   
			   if(gridLength > 0) {
	     			   for(var i=1;i<=gridLength;i++){
		     			   fnAddRowsGridSelect();
				          }
				   }
		   }else{
			 gridObjSelection = loadDhtmlxGridByJsonData(gridObjSelection,strDtlsJson);
			 
			 showImg_id = gridObjSelection.getColIndexById("showImg");
			
			for(var i=1;i<=10;i++){
				fnAddRowsGridSelect();
			}
		} 
}

//To add rows into grid.
function fnAddRowsGridSelect() {
	gridObjSelection.filterBy(0, "");// unfilters the grid while adding rows
	gridObjSelection._f_rowsBuffer = null; // clears the cache
	var newRowId = gridObjSelection.getUID();
	gridObjSelection.addRow(newRowId, '');
	
	gridObjSelection.cellById(newRowId,showImg_id).setValue("<img alt=\"Remove\" src=\"/images/red_btn_minus.gif\" border=\"0\"  title=\"Remove\" height=\"15px\" width=\"15px\">");
}


//function used addRow for remove records
function addRow(){
	var uid = gridObjSelection.getUID();
	gridObjSelection.addRow(uid,'');

}

//this function is used display the File icon in ready for screen
function eXcell_Remove(cell) {
    // the eXcell name is defined here
    if (cell) { // the default pattern, just copy it
        this.cell = cell;
        this.grid = this.cell.parentNode.grid;
    }
    this.setValue = function (val) {
    	
    	this
		.setCValue("<img alt=\"Remove\" src=\"/images/red_btn_minus.gif\" border=\"0\" >",
                val);
    }
    this.getValue = function () {
        return this.cell.childNodes[0].innerHTML; // gets the value
    }
};
eXcell_Remove.prototype = new eXcell;

//This function used to remove the selected records
function doOnRowSelectRemove(rowId,cellInd){
	
	if(cellInd == 11){
		
        reqValue = gridObjSelection.cellById(rowId, reqId).getValue();
		
		strJsonValue = strJsonValue.filter(function( obj ){
			  return obj.REQID != reqValue;
			});
		
		gridObjSelection.deleteRow(rowId);
		   
	    gridObj.forEachRow(function(rowId) {
		   
		   var reqFGDValue = gridObj.cellById(rowId, reqId).getValue();
		 
		   if(reqValue == reqFGDValue){
			   
			     gridObj.cellById(rowId,showImg_id).setValue("<img alt=\"Add\" src=\"/images/add.gif\" border=\"0\">");
			     gridObj.setRowTextStyle(rowId,"background-color:none;");
			   }
		   });
		
		var gridLength = 10 - gridObjSelection.getRowsNum();
	
		   if(gridLength > 0) {
  			   for(var i=1;i<=gridLength;i++){
	     			   fnAddRowsGridSelect();
			          }
			   }
		
	   }
	}

//This function used to fetch the request details used Ajax call
function doOnRowSelectDetails(rowId,cellInd){   // Second grid select record details
	if(cellInd == 0){
		  var gridrow = gridObj.getSelectedRowId();  // Row index value / which row selected
			if(gridrow!=undefined){
				    
					reqValue = gridObjSelection.cellById(rowId, reqId).getValue();
					consignValue = gridObjSelection.cellById(rowId, consignId).getValue();
				    if(consignValue !=''){
					var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmMultiProcConsign.do?method=fetchRequestDetails&strReqTransIds='+reqValue+'&strConsignmentId='+consignValue);
				    fnStartProgress();	
//					dhtmlxAjax.get(ajaxUrl, fnLoadRequestAjax);
					var loader = dhtmlxAjax.getSync(ajaxUrl);
					fnLoadRequestAjax(loader);
				    }
			}
	}
  }

// This function used to fetch the values modify request details and Shipping details
function fnLoadRequestAjax(loader){
	//Enable the shipping details
	fnEnableShip();
	var response = loader.xmlDoc.responseText;
	fnStopProgress();
	var responseSplit = response.split("^");
	var headerDtls = responseSplit[0];
	var JSON_Array_obj = JSON.parse(headerDtls);
	
	   var obj = eval("document.all.lbl_request_date");
	   var requestdateValue = JSON_Array_obj[0].requestdate;
		   if(requestdateValue != undefined && requestdateValue != null){
			   obj.innerHTML = requestdateValue;
		   }else{
			   obj.innerHTML = '';
		   }

	   var obj = eval("document.all.lbl_source");
	   var requestsourceValue = JSON_Array_obj[0].requestsource;
	   if((requestsourceValue != undefined) && (requestsourceValue != null)){
			   obj.innerHTML = requestsourceValue;
		   }else{
			   obj.innerHTML = '';
		   }
	     

        var obj = eval("document.all.lbl_req_trans_id");
        var requesttxnidVal = JSON_Array_obj[0].requesttxnid;
       
		   if((requesttxnidVal != 0) && (requesttxnidVal != null)){
		       obj.innerHTML = requesttxnidVal;
			}else {
			   obj.innerHTML = '';
			}

		var obj = eval("document.all.lbl_set_id");
		var setIDValue = JSON_Array_obj[0].setid;     
		if((setIDValue != null) && (setIDValue != undefined)){
		   obj.innerHTML = setIDValue;
		 }else{
		   obj.innerHTML = '';
		   }  
		
		var obj = eval("document.all.lbl_set_name");
		var setnameValue = JSON_Array_obj[0].setname;
		if((setnameValue != null) && (setnameValue != undefined)){
		   obj.innerHTML = setnameValue;
          }else{
	       obj.innerHTML = '';
	     }	
		
		var obj = eval("document.all.lbl_req_type");
		var requesttypeVal = JSON_Array_obj[0].requesttype;
		if((requesttypeVal != null) && (requesttypeVal != undefined)){
		   obj.innerHTML = requesttypeVal;
          }else{
	       obj.innerHTML = '';
	     }

		var obj = eval("document.all.strReqTo");
		var requesttoTextVal = JSON_Array_obj[0].requestto;
		var reqtoVal = JSON_Array_obj[0].reqto;
		
		if(requesttoTextVal !='' && requesttoTextVal != undefined){
			obj.options[obj.selectedIndex].text = requesttoTextVal;
		}else{
			obj.options[obj.selectedIndex].text = '';
		}
		if(reqtoVal !='' && reqtoVal != undefined){
			document.all.strReqTo.value = reqtoVal;
		}else{
			document.all.strReqTo.value = 0;
		}
		
		var obj = eval("document.all.lbl_req_status");
		var requeststatusVal = JSON_Array_obj[0].requeststatus;
		if((requeststatusVal != null) && (requeststatusVal != undefined)){
			obj.innerHTML = requeststatusVal;
		}else{
		obj.innerHTML ='';
		}
		
		var obj = eval("document.all.lbl_req_for");
		var requestforVal = JSON_Array_obj[0].requestfor;
		if((requestforVal != null) && (requestforVal != undefined)){
			obj.innerHTML = requestforVal;
		}else{
			obj.innerHTML ='';
		}
		
		var obj = eval("document.all.strRequirDate");  
		var requireddateVal = JSON_Array_obj[0].requireddate;
		if((requireddateVal != null) && (requireddateVal != undefined)){
		 obj.value = requireddateVal;
		}else{
		 obj.value = '';
		}
		
		var obj = eval("document.all.strPlanedShipDt");
		var planShipDateVal = JSON_Array_obj[0].planShipDate;
		if((planShipDateVal != null) && (planShipDateVal !=undefined)){
		  obj.value = planShipDateVal;
		}else{
		  obj.value = '';
		}
	// Ship details
		  var obj = eval("document.all.shipTo");  
		  if(JSON_Array_obj[0].requestshipto !='' && JSON_Array_obj[0].requestshipto != undefined){
			  document.all.shipTo.value = JSON_Array_obj[0].requestshipto;
		  }else{
			  document.all.shipTo.value = 0;
		  }

		  var obj = eval("document.all.shipCarrier");  
		  if(JSON_Array_obj[0].deliverycarrierid !='' && JSON_Array_obj[0].deliverycarrierid != undefined ){
			  document.all.shipCarrier.value= JSON_Array_obj[0].deliverycarrierid;
		  }else{
			  document.all.shipCarrier.value = 0;
		  }
		  
		  var obj = eval("document.all.shipMode");  
		  
		  if(JSON_Array_obj[0].deliverymodeid != '' && JSON_Array_obj[0].deliverymodeid !=undefined){
			  document.all.shipMode.value = JSON_Array_obj[0].deliverymodeid;
		  }else{
			  document.all.shipMode.value = 0;
		  }
		  
		  var obj = eval("document.all.shipInstruction");  
		  var shipinstrVal = JSON_Array_obj[0].shipinstr;
		  if((shipinstrVal !=null) && (shipinstrVal !=undefined)){
			  obj.innerHTML = shipinstrVal;
		  }else{
			  obj.innerHTML = '';
		  }
		 
		  var obj = eval("document.all.overrideAttnTo");  
		  var overrideattnVal = JSON_Array_obj[0].overrideattn;
		  if((overrideattnVal !=null) && (overrideattnVal !=undefined)){
		      obj.value = overrideattnVal;
		  }else{
			  obj.value = '';
		  }
		  
		  var obj = eval("document.all.names");  
		  var name = JSON_Array_obj[0].requestshiptoid;
		  fnGetAssocRepNames(document.all.shipTo,'ship');
		  if(name != 0 && name != null){
			  document.all.names.value = name;
		  }else{
			  document.all.names.value = 0;
		  }
		  
		  var obj = eval("document.all.addressid");  
		  obj.value = JSON_Array_obj[0].addressid;
}

var strValidReq ='';
var strInvalidReq='';

//This function used to submit functionality - save data
function fnSubmit(){
	
	document.getElementById("msg").innerHTML = "";
	document.getElementById("erromsg").innerHTML= "";
	
	var planedShipDt = document.all.strPlanedShipDt.value;
	var requirDate = document.all.strRequirDate.value;

	
	var req_dateObj = document.frmMultiProcConsign.strRequirDate;
	var plan_dateObj = document.frmMultiProcConsign.strPlanedShipDt;
	
	// validate the Date format
	CommonDateValidation(req_dateObj,date_format,message[5784]+ message[611]);
	CommonDateValidation(plan_dateObj,date_format,message[5785]+ message[611]);

	// to get the date Different () Required Date
	var date_diff = dateDiff(today_Dt, req_dateObj.value, date_format);

	if(date_diff <0){
		Error_Details(message[5054]);
		}
	// to get the date Different () Planned Ship Date
	date_diff = dateDiff(today_Dt, plan_dateObj.value, date_format);
	if(date_diff <0){
		Error_Details(message[5055]);
		}
	// to get the date Different () Requried Date and Planned Ship Date
	if(req_dateObj.value !=''){
		date_diff = dateDiff(req_dateObj.value, plan_dateObj.value, date_format);
		if(date_diff >0){
			Error_Details(message[5056]);
			}
		}
	
	
	
	var requestTo = document.all.strReqTo.value;
	
	//ship details fields
	var vShipto = document.all.shipTo.value;
	var vAttn = document.all.overrideAttnTo.value;
	var vShipadd = document.all.addressid.value;
	var vShipName = document.all.names.value;
	var vShipCarrier = document.all.shipCarrier.value;
	var vShipMode =  document.all.shipMode.value;
	var vShipIns = document.all.shipInstruction.value;
	var vshipId = document.all.shipId.value;
	var vType = '';
	var vPurpose = '';
	var vShipfl = '';
	var ajaxUrl = "";
	
       if(planedShipDt ==''){
		  Error_Details ('Please enter valid <b>Planned Ship Date</b>');
	    }
       if(requirDate ==''){
    	   Error_Details ('Please enter valid <b>Required Date</b>');
       }

	   if(requestTo == '0' || requestTo == 0){
		Error_Details ('Please select valid <b>Request To</b>');
	    }
	
	    fnValidateShippingDetails();
	   
	    gridObjSelection.forEachRow(function(rowId) {
	    	
	    	reqValue = gridObjSelection.cellById(rowId, reqId).getValue();
			consignValue = gridObjSelection.cellById(rowId, consignId).getValue();
			
			
			if((reqValue !='' && reqValue != undefined) && (consignValue !='' && consignValue != undefined)){
			
			    var vInputStr = reqValue+'^'+consignValue+'|';
			   
			    ajaxUrl = fnAjaxURLAppendCmpInfo('/gmMultiProcConsign.do?method=processMultiConsignSets&strInputString='+encodeURIComponent(vInputStr)+'&strPlanedShipDt='+planedShipDt+'&strRequirDate='  
						  +requirDate+'&shipTo='+vShipto+'&names='+vShipName+'&shipCarrier='+vShipCarrier+'&addressid='+vShipadd+'&overrideAttnTo='
						   +vAttn+'&shipInstruction='+vShipIns+'&strType='+vType+'&strPurpose='+vPurpose+'&strReqTo='+requestTo+'&shipFlag='+vShipfl+'&shipMode='+vShipMode);
			   
			    fnStartProgress();
			    var loader = dhtmlxAjax.postSync(ajaxUrl);
			    fnLoadMultiSetPrceessAjax(loader);
			}
			
	   });
	     
	    if(strValidReq == '' && strInvalidReq == ''){
	    	
	    	Error_Details ('Please add the Selected Sets Details');
	    }
	    
	    if(ErrorCount > 0) {
			Error_Show();
			Error_Clear();
			return false;
		}
	    
	    
	    if(strValidReq!='')  {
			    strValidReq = strValidReq.substring(0,(strValidReq.length)-2);
			    document.getElementById("msg").style.color="green";
			    document.getElementById("msg").style.display="block";		
				document.getElementById("msg").innerHTML= "The following request(s) "+strValidReq+ " are updated successfully";
				strValidReq = '';
		  }
	    if(strInvalidReq !=''){
			    strInvalidReq = strInvalidReq.substring(0,(strInvalidReq.length)-2);
				document.getElementById("erromsg").style.color="red";
				document.getElementById("erromsg").style.display="block";		
				document.getElementById("erromsg").innerHTML= "The following request(s) "+strInvalidReq +" are available in Invalid state to progress";
				strInvalidReq ='';
		 }
}

var strValid = '';
var strInValid = '';
//This function used to Multi sets saved for show result message
function fnLoadMultiSetPrceessAjax(loader){
	var response = loader.xmlDoc.responseText;
	    fnStopProgress();
	var msg = response;
	
	var temp = new Array();
		temp = msg.split('#');
		strValid = temp[0];
		strInValid = temp[1];
	
		if(strValid !='' && strValid != undefined){
				if (strValidReq.indexOf(strValid + ", ") == -1) {
			        strValidReq += (strValid+", ");
				}
		}
		
		if(strInValid !='' && strInValid != undefined){
			if (strInvalidReq.indexOf(strInValid + ", ") == -1) {
				strInvalidReq += strInValid+", ";
			}
		}
}

// fnReset - function used to mandatory field values clear
function fnReset(){
	
	 document.frmMultiProcConsign.strSetName.value='';
	 document.frmMultiProcConsign.strSetId.value='';
	 document.frmMultiProcConsign.strConsignmentId.value='';
	 document.frmMultiProcConsign.strReqId.value='';
	 document.frmMultiProcConsign.strStatus.value='50646';
	
	 document.all.lbl_request_date.innerHTML = '';
	 document.all.lbl_req_trans_id.innerHTML = '';
	 document.all.lbl_source.innerHTML = '';
	 document.all.lbl_set_id.innerHTML = '';
	 document.all.lbl_set_name.innerHTML = '';
	 document.all.lbl_req_for.innerHTML = '';
	 document.all.lbl_req_status.innerHTML = '';
	 document.all.lbl_req_type.innerHTML = '';
	 
	 document.all.strReqTo.value = '';
	 document.all.strPlanedShipDt.value = '';	
	 document.all.strRequirDate.value = '';
	 document.all.shipTo.value = 0;
	 document.all.shipCarrier.value=0;
	 document.all.shipInstruction.value='';
	 document.all.shipMode.value=0;
	 document.all.overrideAttnTo.value='';
	 document.all.names.value=0;
	 document.all.addressid.value = 0;
	
	 strJsonValue = [];
	 temJson = {};
	 fnLoadMultiRequestAjax();
	 fnLoadSelectedGrid();
	 clearMsg();
}

function clearMsg(){
	document.getElementById("msg").innerHTML= "";
	document.getElementById("erromsg").innerHTML= "";
}

function fnValidateShippingDetails(){
	var billTo = document.all.strReqTo.value;
	if (billTo != ''&& billTo != '0')
	{			
			fnValidateDropDn('shipTo',message[5620]);
			fnValidateDropDn('names',message[5564]);
			
			var shipto = document.all.shipTo.value;
			var shiptoidid = document.all.addressid.value;
			if (shipto == '4121' && shiptoidid == '')
			{
				Error_Details(message[32]);
			}
	}
	if (billTo == ''|| billTo == '0')
	{
		fnDisableShip();
	}
}

function fnDisableShip(){
	document.all.shipTo.value = 0;
	document.all.names.value= 0;
	document.all.shipCarrier.value= 0;
	document.all.shipMode.value = 0;
	document.all.shipTo.disabled = true;
	document.all.names.disabled = true;
	document.all.shipCarrier.disabled = true;
	document.all.shipMode.disabled = true;
}

function fnEnableShip(){
	document.all.shipTo.disabled = false;
	document.all.names.disabled = false;
	document.all.shipCarrier.disabled = false;
	document.all.shipMode.disabled = false;
}

