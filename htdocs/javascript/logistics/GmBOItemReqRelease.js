var gridObj;
var check_rowId = '';
var loc_img_rowId = '';
var part_num_rowId = '';
var Req_id_row_id = '';
var Qty_to_release_row_id = '';
var qtyToReleaseFull_row_id = '';
var showImgRowId = '';
var bopriority = '';
var Master_req_row_id = '';
var hidden_bo_req_id = '';
function fnOnLoad(){
	switchUserFl = document.frmBOItemReqRelease.switchUserFl.value;
	if(switchUserFl == 'Y')
			{
			document.all.btnReleaseBO.disabled = true;
			document.all.btnUpdatetoBillOnly.disabled = true;
			}
	var initialCheckFull = fullQtyCheck;
	if(initialCheckFull == "yes"){
		document.frmBOItemReqRelease.chkFullQty.checked=true;
	}
	document.getElementById("buttonDiv").style.display = "none";
    document.getElementById("exportDiv").style.display = "none";
    document.getElementById("noDataDiv").style.display = "none";
	fnLoad();
}

//This function used to load setpriority mapping details in Grid
function fnLoad()
{
    	//Intialize
	var strAccountName = document.frmBOItemReqRelease.strAccountName.value;   
	var strSalesRep = document.frmBOItemReqRelease.strSalesRep.value;   
	var strPartNos = document.frmBOItemReqRelease.strPartNos.value;   
	var strReqId = document.frmBOItemReqRelease.strReqId.value;   
	var chkFullQty = document.frmBOItemReqRelease.chkFullQty.value; 
	var chkPartialQty = document.frmBOItemReqRelease.chkPartialQty.value;
    var chkFGQty = document.frmBOItemReqRelease.chkFGQty.value;
    if(document.frmBOItemReqRelease.chkFullQty.checked == false && document.frmBOItemReqRelease.chkPartialQty.checked == false && document.frmBOItemReqRelease.chkFGQty.checked == false){
		Error_Details(message_operations[775]); 
	 }
	 if (ErrorCount > 0){
			Error_Show();
			Error_Clear();
			return false;
		}
    
     if(document.frmBOItemReqRelease.chkFullQty.checked){
        chkFullQty = 'yes';
    }else{
         chkFullQty = '';
    }
    
    if(document.frmBOItemReqRelease.chkPartialQty.checked){
        chkPartialQty = 'yes';
    }else{
         chkPartialQty = '';
    }
    
    if(document.frmBOItemReqRelease.chkFGQty.checked){
        chkFGQty = 'yes';
    }else{
         chkFGQty = '';
    }
	fnStartProgress('Y');
	var ajaxUrl = fnAjaxURLAppendCmpInfo("/gmBOItemReqReleaseAction.do?method=fetchBOItemReqDetails&strAccountName="+strAccountName+"&strSalesRep="+strSalesRep+"&strPartNos="+strPartNos+"&strReqId="+strReqId+"&chkFullQty="+chkFullQty+"&chkPartialQty="+chkPartialQty+"&chkFGQty="+chkFGQty+"&initialFullchk="+chkFullQty+"&ramdomId=" + Math.random()+"&"+ fnAppendCompanyInfo());
	var loader = dhtmlxAjax.getSync(ajaxUrl);
	fnLoadCallBack(loader);

}

//call back function
function fnLoadCallBack(loader){
       var check_rowId = '';
       document.getElementById("buttonDiv").style.display = "table-row";
       document.getElementById("exportDiv").style.display = "table-row";
       document.getElementById("dataGridDiv").style.display = "table-row";
       document.getElementById("noDataDiv").style.display = "none"; 
	    var response = loader.xmlDoc.responseText;
        fnStopProgress('Y');
        if(response != ''){
		var setInitWidths = "50,40,100,100,80,140,140,340,70,70,70,76,0,0";
		var setColAlign = "center,center,left,left,left,left,left,left,right,right,right,center,center,center";
		var setColTypes = "ch,ro,ro,ro,ro,ro,ro,ro,ro,ro,ed,ron,ro,ro";
		var setColSorting = "na,str,str,str,str,str,str,str,str,str,str,int,int,str";
		var setHeader = ["","","Part Number", "BO Req ID","Parent Req ID","Requested Date","Request Type - Account/Field Sales","Requestor Name","BO Qty", "FG Qty","Qty to Release","","",""];
		var setFilter = ["<input type='checkbox' value='no'  name='selectAll' onClick='javascript:fnChangedFilter(this);'/>","#rspan","#text_filter", "#text_filter", "#text_filter","#text_filter", "#text_filter", "#text_filter","#text_filter","#text_filter","#rspan","#rspan","#rspan",""];
		var setColIds = "chk_box,locationimg,partnumber,boreqid,parentreqid,requesteddt,reqtype,requestornm,qty,fgqty,qtytorelease,showImg,bopriority,hboreqid";
		var enableTooltips = "true,true,true,true,true,true,true,true,true,true,true,true,true,true";
	
		var gridHeight = "";
		var footerStyles = [];
		var footerExportFL = true;
			pagination = "";
			 format = ""
		
		//split the functions to avoid multiple parameters passing in single function
		gridObj = initGridObject('dataGridDiv');
		gridObj = fnCustomTypes(gridObj,setHeader, setColIds, setInitWidths, setColAlign, setColTypes,setColSorting, enableTooltips, setFilter,500,pagination); 
		gridObj = loadDhtmlxGridByJsonData(gridObj,response);
    
		//gridObj.attachEvent("onEditCell",doOnCellEdit);
        gridObj.attachEvent("onCheckbox", doOnCheck);
        gridObj.attachEvent("onEditCell", doOnCellEdit);
        check_rowId = gridObj.getColIndexById("chk_box");
        loc_img_rowId = gridObj.getColIndexById("locationimg");
        part_num_rowId = gridObj.getColIndexById("partnumber");
		Req_id_row_id = gridObj.getColIndexById("boreqid");
        Master_req_row_id = gridObj.getColIndexById("parentreqid");
		Qty_to_release_row_id = gridObj.getColIndexById("qtytorelease"); 
		qtyToReleaseFull_row_id = gridObj.getColIndexById("fgqty");
        showImgRowId = gridObj.getColIndexById("showImg");
        bopriority = gridObj.getColIndexById("bopriority");
        hidden_bo_req_id = gridObj.getColIndexById("hboreqid");
        gridObj.setColumnHidden(12,true);
        gridObj.setColumnHidden(13,true);
        gridObj.groupBy(2,["","","#title","","","","","","#stat_total","#stat_max","","",""]);
        gridObj.forEachRow(function(rowId) {
            var partnum  = gridObj.cellById(rowId, part_num_rowId).getValue();
            var reqId = gridObj.cellById(rowId, Req_id_row_id).getValue();
            var parenReqId = gridObj.cellById(rowId, Master_req_row_id).getValue();
            gridObj.cellById(rowId, loc_img_rowId).setValue('<a href="javascript:void(0);" onClick="javascript:fnLookup(\''+partnum+'\');"><img alt="Inventory Lookup" src="/images/location.png"  border="0"></a>');
            gridObj.cellById(rowId, Req_id_row_id).setValue('<a href=\"#\" onClick="fnViewOrder(\''+reqId+'\')">'+reqId+'</a>');
            gridObj.cellById(rowId, Master_req_row_id).setValue('<a href=\"#\" onClick="fnViewOrder(\''+parenReqId+'\')">'+parenReqId+'</a>');
            
            
        });
       fnSetBOPriorityColor();
        }else{
           document.getElementById("buttonDiv").style.display = "none";
         document.getElementById("exportDiv").style.display = "none";
         document.getElementById("dataGridDiv").style.display = "none";
          document.getElementById("noDataDiv").style.display = "table-row"; 
        }
}

function fnSetBOPriorityColor(){
	gridObj.forEachRow(function(rowId) {
		BO_Prioirty = gridObj.cellById(rowId, bopriority).getValue();
		if(BO_Prioirty=='1'){
			gridObj.setRowTextStyle(rowId, "background-color: rgb(198,239,206);");
			gridObj.setCellTextStyle(rowId,9,"color:rgb(198,239,206);background-color: rgb(198,239,206);"); //to make the FG qty not appear on line items
			gridObj.setCellTextStyle(rowId,Qty_to_release_row_id,"background-color: rgb(255,255,255);"); //to make unique the Qty release column 
		}
		if(BO_Prioirty=='2'){
			gridObj.setRowTextStyle(rowId, "background-color: rgb(255,235,156);");
			gridObj.setCellTextStyle(rowId,9,"color:rgb(255,235,156);background-color: rgb(255,235,156);"); //to make the FG qty not appear on line items
			gridObj.setCellTextStyle(rowId,Qty_to_release_row_id,"background-color: rgb(255,255,255);"); //to make unique the Qty release column 
			gridObj.cellById(rowId, Qty_to_release_row_id).setValue('');
		}
		if(BO_Prioirty=='3'){
			gridObj.setRowTextStyle(rowId, "background-color: rgb(255,199,206);");
			gridObj.setCellTextStyle(rowId,9,"color:rgb(255,199,206);background-color: rgb(255,199,206);");//to make the FG qty not appear on line items
			gridObj.setCellTextStyle(rowId,Qty_to_release_row_id,"background-color: rgb(255,255,255);"); //to make unique the Qty release column 
			gridObj.cellById(rowId, Qty_to_release_row_id).setValue('');
		}
	});
}

//this function is called when click select all option by header checkbox
function fnChangedFilter(obj) {
    check_rowId = gridObj.getColIndexById("chk_box");
	if (obj.checked) {
		gridObj.forEachRow(function(rowId) {  
        var BO_Prioirty = gridObj.cellById(rowId, bopriority).getValue();//new
        if(BO_Prioirty == '1'){
				gridObj.cellById(rowId, check_rowId).setChecked(true);
        }
		});
	} else {
		gridObj.forEachRow(function(rowId) {
			check_id = gridObj.cellById(rowId, check_rowId).getValue();
			if (check_id == 1) {
				gridObj.cellById(rowId, check_rowId).setChecked(false);
			}

		});
	}
    var switchUserFl = document.frmBOItemReqRelease.switchUserFl.value;
    if(switchUserFl == 'Y')
				{
				document.all.btnReleaseBO.disabled = true;
				document.all.btnUpdatetoBillOnly.disabled = true;
				}
}

// Function is used to Check Box Check
function doOnCheck(rowId, cellInd, stage) {
    var check_rowId = gridObj.getColIndexById("chk_box");
    var checkedRowID = '';
	checkedRowID = gridObj.getCheckedRows(check_rowId);
    
    if(checkedRowID == '' && checkedRowID != undefined){
		document.all.btnReleaseBO.disabled = false;
	}
    
    gridObj.forEachRow(function(rowId) {
        var check_id = gridObj.cellById(rowId, check_rowId).getValue(); 
         if(check_id == 1) {
           var BO_Prioirty = gridObj.cellById(rowId, bopriority).getValue();//new  
             if(BO_Prioirty == '3'){
					document.all.btnReleaseBO.disabled = true;
				}else{
                    document.all.btnReleaseBO.disabled = false;
                }
         }
    });
    switchUserFl = document.frmBOItemReqRelease.switchUserFl.value;
    if(switchUserFl == 'Y')
				{
				document.all.btnReleaseBO.disabled = true;
				document.all.btnUpdatetoBillOnly.disabled = true;
				}
}

function fnSelectAllToggle(varControl) {
	var objControl = eval("document.frmBOItemReqRelease." + varControl);

	var all_row = gridObj.getAllRowIds(",");
	var all_rowLen = all_row.length; // get the rows id length
	var checked_row = gridObj.getCheckedRows(check_rowId);
	var finalval;

	finalval = eval(checked_row.length);
	if (all_rowLen == finalval) {
		objControl.checked = true;// select all check box to be checked
	} else {
		objControl.checked = false;// select all check box un checked.
	}
}


function doOnCellEdit(stage,rowId,cellInd,newVal,oldVal)
{
	if(stage==0){
		return true;
		}
		if (stage == 1) {
		return true;
		}
	if (stage == 2) {
		return true;
	}
}
//This Function Used to set custom types conditions in Grid
function fnCustomTypes(gObj,setHeader, setColIds, setInitWidths, setColAlign, setColTypes,setColSorting, enableTooltips, setFilter,gridHeight,pagination){

if (setHeader != undefined && setHeader.length > 0) {
 gObj.setHeader(setHeader);
 var colCount = setHeader.length;
 }    

if (setInitWidths != "")
gObj.setInitWidths(setInitWidths);

if (setColAlign != "")
gObj.setColAlign(setColAlign);

if (setColTypes != "")
gObj.setColTypes(setColTypes);

if (setColSorting != "")
gObj.setColSorting(setColSorting);

if (enableTooltips != "")
gObj.enableTooltips(enableTooltips);

if (setFilter != "")
gObj.attachHeader(setFilter);

  

if(setColIds != ""){
 gObj.setColumnIds(setColIds);
}

if (gridHeight != "")
gObj.enableAutoHeight(true, gridHeight, true);
else
gObj.enableAutoHeight(true, 400, true);

if(pagination == "Y"){
    gObj.enablePaging(true,100,10,"pagingArea",true);
    gObj.setPagingSkin("bricks");
}	

return gObj;
}
/*fnReleaseBackOrder(): function to release backorder
 */
function fnReleaseBackOrder() {	
	var checkedRowID = '';
    var check_rowId = gridObj.getColIndexById("chk_box");
    var showImg = gridObj.getColIndexById("showImg");
	checkedRowID = gridObj.getCheckedRows(check_rowId);
	if(checkedRowID == '' && checkedRowID != undefined){
		Error_Details(message_operations[249]);
	}
	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}

	gridObj.forEachRow(function(rowId) {
		check_id = gridObj.cellById(rowId, check_rowId).getValue();
		var releaseToQty = '';
		if(check_id == 1) {
			Ref_id = gridObj.cellById(rowId, hidden_bo_req_id).getValue();  
			Release_qty = gridObj.cellById(rowId, Qty_to_release_row_id).getValue(); 
			Release_full_qty = gridObj.cellById(rowId, qtyToReleaseFull_row_id).getValue(); 
			pnum = gridObj.cellById(rowId, part_num_rowId).getValue(); 
				if(Release_qty.indexOf("span") !='-1'){
					Release_qty = Release_full_qty;
				}
			releaseToQty = Release_qty;
		
		if (isNaN(releaseToQty) || parseInt(releaseToQty) <= 0) {
			gridObj.cellById(rowId,showImg).setValue("<span class=\"close-sm\" id=\"closebtn\"><a href=\"#\"><span class=\"glyphicon glyphicon-remove\" title='Qty to Release should be numeric' id='fail"+rowId+"'  alt='Failure' style='color:red' width='15' height='15'></span></a></span>");
			    	fnMouseOver(gridObj);
			    	fnStopProgress('Y');
			
		}else 
		{
			
			var ajaxUrl = fnAjaxURLAppendCmpInfo('gmBOItemReqReleaseAction.do?method=saveBOItemReqDetails&hReqId=' + encodeURIComponent(Ref_id) 
			 + '&strSessUserId=' + encodeURIComponent(sessionUserId) + '&pnum=' + encodeURIComponent(pnum) +'&releaseQty=' + encodeURIComponent(Release_qty) +'&ramdomId=' + Math.random() +"&"+ fnAppendCompanyInfo());
			fnStartProgress('Y');

			dhtmlxAjax.get(ajaxUrl, function(loader){
			     response = loader.xmlDoc.responseText;
                var res = JSON.parse(response);
                console.log("Reponse of after save Request"+res);
			   if (res.error != '' && res.error != undefined){
			    	gridObj.cellById(rowId,showImg).setValue("<span class=\"close-sm\" id=\"closebtn\"><a href=\"#\"><span class=\"glyphicon glyphicon-remove\" title='"+res.error.replace(/\'/g, "")+"' id='fail"+rowId+"'  alt='Failure' style='color:red' width='15' height='15'></span></a></span>");
			    	fnMouseOver(gridObj);
			    	fnStopProgress('Y');
			    }else if(res.data != '' && res.data != undefined){
			    	gridObj.cellById(rowId,showImg).setValue("<span class=\"tick-sm\" id=\"tickmark\"><a href=\"#\"><span class=\"glyphicon glyphicon-ok\" title='Success' style='color:green'   width='15' height='15'></span></a></span>");
			    	//gridObj.cellById(rowId,QtyReleaseTxtBox).setValue(releaseToQty);
			    	gridObj.cellById(rowId,0).setDisabled(true); //To make the checkbox disabled after Releasing an order
			    	gridObj.setRowTextStyle(rowId, "background-color:rgb(182, 175, 175);"); //To highlight the entire saved Row
			    	gridObj.setCellTextStyle(rowId,10,"color:rgb(182, 175, 175);background-color: rgb(182, 175, 175);");
			    	fnStopProgress('Y');
			    	fnMouseOver(gridObj);
			    	return true;
                }
		    });
				gridObj.cellById(rowId, 0).setChecked(false);
			}
		} 
    });
}

function fnMouseOver(gridObj){
	gridObj.attachEvent("onMouseOver",function()
		{
			return false;
		});
}



function fnOverallLookup(){
	var checkedRowID = '';
	var pnums = '';
    var check_rowId = gridObj.getColIndexById("chk_box");
	checkedRowID = gridObj.getCheckedRows(check_rowId);
	if(checkedRowID == '' && checkedRowID != undefined){
		Error_Details(message_operations[251]);
	}
	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}
	var str = ''
	gridObj.forEachRow(function(rowId) {
		check_id = gridObj.cellById(rowId, check_rowId).getValue();
		if(check_id == 1) {
			partnum = gridObj.cellById(rowId, 2).getValue();
			str = str + partnum + ",";
		 }
	 });
	if(str != undefined && str !=''){
		 pnums = fnRemoveDuplicateValues(str);	
	}
	
   windowOpener('/GmPartInvReportServlet?hAction=Go&hOpt=MultipleParts&hScreenType=BACKORDER&hPartNum='+encodeURIComponent(pnums),"Pack","resizable=yes,scrollbars=yes,top=250,left=250,width=960,height=510");
}

function fnLookup(partNm)
{
	var strPartNm ='';
	if(partNm != undefined && partNm !=''){
		strPartNm = partNm+",";
	}
	if(strPartNm == ''){
		Error_Details(message_operations[251]);
	}
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	windowOpener('/GmPartInvReportServlet?hAction=Go&hOpt=MultipleParts&hScreenType=BACKORDER&hPartNum='+encodeURIComponent(strPartNm),"Pack","resizable=yes,scrollbars=yes,top=250,left=250,width=960,height=510");
}

function fnloadPartFulfillReport()
{
	fnStartProgress('Y');
	var val= items;
    var partnum = '';
    var hpnum = '';
     var part_num_rowId = gridObj.getColIndexById("partnumber");
    gridObj.forEachRow(function(rowId) {
			partnum = gridObj.cellById(rowId, part_num_rowId).getValue();
			hpnum = hpnum + partnum + ",";
	 });
	var str = hpnum;
	str = str.substr(0,str.length-1);
	windowOpener("/gmLogisticsReport.do?method=loadPartFulfillReport&BackOrderType="+val+"&hPartNum="+encodeURIComponent(str),"Pack","resizable=yes,scrollbars=yes,top=250,left=250,width=1020,height=600");
    fnStopProgress();
}

function fnDownloadXLS()
{  var check_rowId = gridObj.getColIndexById("chk_box");
    gridObj.setColumnHidden(check_rowId,true);//hiding checkbox
	gridObj.setColumnHidden(1,true);//hiding lookup icon
	gridObj.setColumnHidden(11,true);//hiding success.gif
    gridObj.setColumnHidden(12,true);//hiding bopriority column
    gridObj.setColumnHidden(13,true);//hiding bopriority column
    gridObj.toExcel('/phpapp/excel/generate.php');
	gridObj.setColumnHidden(check_rowId,false);
	gridObj.setColumnHidden(1,false);
	gridObj.setColumnHidden(11,false);
   gridObj.setColumnHidden(12,false);
	
}

function fnRemoveDuplicateValues(inValue) {
	var items = inValue.split(",");
	var arrlen = items.length - 1;
	var i = 0;
	var outValue = "";
	var prevItem;
	while (i <= arrlen) {
		if (outValue.length > 0) {
			if (prevItem != items[i].trim()) {
				var str = items[i].trim();
				outValue = outValue.replace(prevItem + ",", "");
				outValue += "," + str;
			}
		} else {
			outValue = items[i].trim();
		}
		prevItem = items[i].trim();
		i++;
	}
	return outValue;
}

function fnViewOrder(val,varType)
{
	intType = 0;
	
	if(varType!='') 
	{
		intType = parseInt(varType);
	}
		
	if (intType > 4116){
	windowOpener("/GmReprocessMaterialServlet?hAction=ViewInHouse&hId="+val,"Con1","resizable=yes,scrollbars=yes,top=250,left=200,width=800,height=700");
  }else{
	 // temp = val.substring(0,5);
temp = val.substring(val.lastIndexOf('-')-2,val.lastIndexOf('-'));
	  
	  if(temp=='RQ')
	  { 
	 // if(temp=='GM-RQ')
	  //{
	  windowOpener("/gmRequestMaster.do?strOpt=print&requestId="+val,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=920,height=600");
	  }
	  else
	  {
	  windowOpener('/GmEditOrderServlet?hMode=PrintPack&hOrdId='+val,"Pack","resizable=yes,scrollbars=yes,top=150,left=150,width=760,height=610");
	  }

	 
	}
}

