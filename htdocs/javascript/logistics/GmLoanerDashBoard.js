function fnReload(ObjTitle) {
	if(ObjTitle == 'Open' || ObjTitle == 'Allocated' || ObjTitle == 'ToPick' || ObjTitle == 'ToPutAway'|| ObjTitle == 'PendingShipping' || ObjTitle == 'SameDay') {
		dhtmlxAjax.get('/gmLoanerDash.do?method=loadLoanerDash&ramdomId='+Math.random());
		}
	}
//This function is used to render the chart on screen in same day div
function fnChart(opensameday,topicksameday,toshipsameday) {
	var sameopen = opensameday.value;
	var samepick = topicksameday.value;
	var sameship = toshipsameday.value;
if (sameopen == '0') {
	sameopen = "";
}
if (samepick == '0') {
	samepick = "";
}
if (sameship == '0') {
	sameship = "";
}
FusionCharts.ready(function() {
	  var chartObj = new FusionCharts({
	    type: 'stackedbar2d',
	    renderAt: 'chart-container',
	    width: '70%',
	    height: '15%',
	    dataFormat: 'json',
	    dataSource: {
	      "chart": {
	        "theme": "fusion",
	        "numberPrefix": "",
	        "theme": "fusion",
	        "showBorder": "0",
		    "showLegend": "1",
			"baseFont": "Verdana",
			"baseFontSize": "15",
			"baseFontColor": "#0066cc",
	        "showLegendBorder": "0",
	        "legendBorderColor": "#ffffff",
	        "canvasBorderAlpha" : "0",
			"usePlotGradientColor" : "0",
			"showAlternateVGridColor" : "0",
			"showXAxisLine" : "0",
			"xAxisLineColor" : "#ffffff",
			"divlineColor" : "#ffffff",
			"chartBottomMargin": "0",
	        "chartTopMargin": "0",
	        "chartRightMargin": "0",
	        "chartLeftMargin": "0",
	        "showTrendlineLabels" : "1",
	        "legendShadow": false,
	        "enableSmartLabels": "1",
	        "showXAxisValues" : "0",
	        "showYAxisValues" : "0",
	        "xAxisNameFontColor": "#ffffff",
	        "labelDisplay": "none",
	        "showLabels": "0",
	        "use3DLighting":"0",
	 	    "drawcustomlegendicon": "1",
	 	    "bgColor": "#ffffff",
	 		"drawcustomlegendicon": "1",
	 		"legendIconAlpha": "25",
	      },
	      "categories": [{
	        "category": [{
	            "label": ""
	          }
	        ]
	      }],
	      "dataset": [{
	          "seriesname": "Open",
	          "data": [{
	              "value": sameopen
	            }
	          ]
	        },
	        {
	          "seriesname": "ToPick",
	          "data": [{
	              "value": samepick
	            }
	          ]
	        },
	        {
		          "seriesname": "ToShip",
		          "data": [{
		              "value": sameship
		            }
		          ]
		        }
	      ]
	    },
	  }).render();
	    chartObj.addEventListener("dataPlotClick", function (e, v) {
        var selectedStatus = v.datasetName;
        fnOpenLoanerReqSameDay(selectedStatus);
      });
	});	
}


//This function is used to load the details in popup
function fnOpenLoanerRequest(inputStatus) {
	if(inputStatus =='Open') {
		windowOpener("/GmLoanerPartRepServlet?hAction=Reload&hOpt=Requests&hStatus=10&inputParam="+inputStatus,"Loaner",
		"resizable=yes,scrollbars=no,top=150,left=150,width=1250,height=800");		
	}
	if(inputStatus =='Allocated') {
		windowOpener("/GmLoanerPartRepServlet?hAction=Reload&hOpt=Requests&hStatus=20&inputParam="+inputStatus,"Loaner", 
				"resizable=yes,scrollbars=no,top=150,left=150,width=1250,height=800");
	}
	if(inputStatus == 'ToPick'){
		windowOpener("/GmLoanerPartRepServlet?hAction=Reload&hOpt=Requests&hStatus=&inputParam="+inputStatus,"Loaner",
				"resizable=yes,scrollbars=no,top=150,left=150,width=1250,height=800");
	}
	if(inputStatus == 'ToPutAway'){
		windowOpener("/GmLoanerPartRepServlet?hAction=Reload&hOpt=Requests&hStatus=&inputParam="+inputStatus,"Loaner", 
				"resizable=yes,scrollbars=no,top=150,left=150,width=1250,height=800");
	}
	if(inputStatus == 'PendingShipping'){
		windowOpener("/GmLoanerPartRepServlet?hAction=Reload&hOpt=Requests&hStatus=&inputParam="+inputStatus,"Loaner", 
				"resizable=yes,scrollbars=no,top=150,left=150,width=1250,height=800");
	}
	
}

//This function is used to load the details in popup which is in same day mode 
function fnOpenLoanerReqSameDay(selectedStatus) {
	if(selectedStatus == 'Open') {
		windowOpener("/GmLoanerPartRepServlet?hAction=Reload&hOpt=Requests&hStatus=10&selectedStatus="+selectedStatus+"&inputParam=sameday","Loaner",
				"resizable=yes,scrollbars=no,top=150,left=150,width=1250,height=800");
	}
	if(selectedStatus == 'ToPick') {
		windowOpener("/GmLoanerPartRepServlet?hAction=Reload&hOpt=Requests&hStatus=&selectedStatus="+selectedStatus+"&inputParam=sameday","Loaner",
			    "resizable=yes,scrollbars=no,top=150,left=150,width=1250,height=800");
	}
	if(selectedStatus == 'ToShip') {
		windowOpener("/GmLoanerPartRepServlet?hAction=Reload&hOpt=Requests&hStatus=&selectedStatus="+selectedStatus+"&inputParam=sameday","Loaner",
        	    "resizable=yes,scrollbars=no,top=150,left=150,width=1250,height=800");
	}
}
