function validate(cnt,obj) 
{
	var pnum = '';
	var qty = '';
	    trcnt = cnt;
		var varfromcosttype = document.frmManualAdjTxn.hmaFromType.value;
	
		var pnumobj = eval("document.frmCart.Txt_PNum"+trcnt);
		var qtyobj = eval("document.frmCart.Txt_Qty"+trcnt);
		var priceobj = eval("document.frmCart.Txt_Price"+trcnt);
		var obj = eval("document.all.Lbl_Desc"+trcnt);

		if (TRIM(qtyobj.value) == ''  && obj.innerHTML != '<SPAN class=RightTextBlue>'+message_accounts[71]+'</SPAN>')	{
			fnRemoveItem(cnt);
		}
	    changeBgColor(obj,'#ffffff');
		
		varPartQty =  qtyobj.value;
		if(varPartQty.charAt(0) == '-')
		{	
			setErrMessage('<span class=RightTextRed>'+message_accounts[296]+'</span>');
		}
		//if (pnumobj.value != '' && qtyobj.value != '')
		if (varPartQty == '')
		{
			varPartQty = 1;
		}
		if (pnumobj.value != '')
		{

		   if (priceobj.value != '')
		   {
			   fnCalExtPrice(priceobj,trcnt);
		   }
		   else
		   {
			   pnum = pnumobj.value;
			   qty = qtyobj.value;
		
			   var url = fnAjaxURLAppendCmpInfo("/GmCartAjaxServlet?pnum=" + encodeURIComponent(pnum)+ "&qty=" + encodeURIComponent(qty)+ "&FROMPG=MA" + "&HMAFROMTYPE=" +varfromcosttype);
			  // var url = "ValidateServlet";
			   if (typeof XMLHttpRequest != "undefined") {
				   req = new XMLHttpRequest();
			   } else if (window.ActiveXObject) {
				   req = new ActiveXObject("Microsoft.XMLHTTP");
			   }
			   //req.open("POST", url, true);
			   req.open("GET", url, true);
			   req.onreadystatechange = callback;
			
			   //req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			   //req.send("id=" + encodeURIComponent(obj.value));
			   req.send(null);
		   } 
		}
}
 
function callback() 
{
    if (req.readyState == 4) {
        if (req.status == 200) {
	        var xmlDoc = req.responseXML;
	        parseMessage(xmlDoc);
        }
        else
        {
	        setErrMessage('<span class=RightTextBlue>'+message_accounts[71]+'</span>');
        }
    }
}

function parseMessage(xmlDoc) 
{
	var strarr = 'qoh,pdesc,price,oprqoh,oprqohop,ownercd,ownerccy,localccy,localprice,ownerid';
	var pnum = xmlDoc.getElementsByTagName("pnum");
	var data = xmlDoc.getElementsByTagName("data");
	var datalength = data[0].childNodes.length;

	if(datalength == 0){
		 setErrMessage('<span class=RightTextBlue>'+message_accounts[71]+'</span>');
		 return;
	}
	var strobj =  strarr.split(',');

	var strlen = strobj.length;
	var strresult = '';

	for (var i=0;i<strlen ;i++ )
	{
		var str = strobj[i];
		for (var x=0; x<pnum[0].childNodes.length; x++) 
		{
			xmlstr = pnum[0].childNodes[x].tagName;
			if (xmlstr == str) 
			{
				if(pnum[0].childNodes[x].firstChild){
					strresult = strresult + '^' + parseXmlNode(pnum[0].childNodes[x].firstChild);
					break;
				}else{
					strresult = strresult + '^';
				}
			}
		}
	}

strresult = strresult.substr(1,strresult.length);
var strobj =  strresult.split('^');

var qoh = strobj[0];
var pdesc = strobj[1];
var price = strobj[2];
// to get the owner information
var owner_cd = strobj[5];
var owner_ccy = strobj[6];
var local_ccy = strobj[7];
var local_price = strobj[8];
var ownerID = strobj[9];

var oprqoh = '';
var deptid=document.frmCart.hDeptID.value;

if(deptid=='2022')
	oprqoh=strobj[3];
else
	oprqoh=strobj[4];

if(qoh == -9999)
	qoh = '-';

if(oprqoh == -9999)
	oprqoh = '-';

if(ownerID == '' || owner_ccy == ''){
	ownerID = 0;
	owner_ccy = 0;
}

obj = eval("document.frmCart.Txt_Qty"+trcnt);
qty = parseFloat(obj.value);

var amount = qty*price;
var localCost = parseFloat(local_price);
var localExt = parseFloat(qty*localCost);

	if (pdesc == null)
	{
		setErrMessage('<span class=RightTextBlue>'+message_accounts[71]+'</span>');
		return;
	}
	/*else if (price == '')
	{
		setErrMessage('<span class=RightTextBlue>Price is not set for this Part for this Account</span>');
		return;
	}*/
	else
	{
		setMessage(qoh,pdesc,price,amount,oprqoh,owner_cd,owner_ccy,local_ccy,localCost, localExt, ownerID);
	}
}
 
function setMessage(qoh,pdesc,price,amount,oprqoh,owner_cd,owner_ccy,local_ccy,localCost, localExt, ownerID) 
{
	qohmin = qoh;

	var obj = eval("document.all.Lbl_Desc"+trcnt);
	obj.innerHTML = "&nbsp;" + pdesc;

    obj = eval("document.all.Lbl_Stock"+trcnt);
    obj.innerHTML = qoh + "&nbsp;&nbsp;";

    obj = eval("document.all.Lbl_OprStock"+trcnt);
    obj.innerHTML = oprqoh + "&nbsp;&nbsp;";
//	alert(' qoh ' + qoh + ' oprqoh ' + oprqoh);
	if(oprqoh != '-' &&  oprqoh <= qoh){
		qohmin = oprqoh;
	}
//	alert(' qohmin ' + qohmin);
	obj = eval("document.all.hStock"+trcnt);
    obj.value = formatNumber(qohmin);

    obj = eval("document.frmCart.Txt_Price"+trcnt);
	obj.value = formatNumber(price);
    //obj.value = price ;
    
    obj = eval("document.all.Lbl_Amount"+trcnt); 
    obj.innerHTML = formatNumber(amount) + "&nbsp;";
    // international posting changes
    obj = eval("document.frmCart.cbo_owner_company"+trcnt);
	obj.value = ownerID;
	
	obj = eval("document.frmCart.cbo_owner_currency"+trcnt);
	obj.value = owner_ccy;
	
	obj = eval("document.frmCart.Txt_Local_Cost"+trcnt);
	obj.value = formatNumber(localCost);
	
	 obj = eval("document.all.Lbl_local_ext_cost"+trcnt); 
	  obj.innerHTML = formatNumber(localExt) + "&nbsp;";
	
	obj = eval("document.all.hLocalCurrency"+trcnt);
    obj.value = local_ccy;
	
    obj = eval("document.all.hOwnerId"+trcnt);
    obj.value = ownerID;
    // PC-1002 - Auto Populate Company and Currency Info on Manual Adjusment
	// before entering part header dropdown select after enter paer and qty header dropdown will change choose one
	document.getElementById('cbo_owner_company').value= 0;

	fnCalculateTotal();
}

function setErrMessage(msg) 
{
    var obj = eval("document.all.Lbl_Desc"+trcnt);
    obj.innerHTML = msg;
    
    obj = eval("document.all.Txt_Qty"+trcnt);
    obj.value = '';

	obj = eval("document.all.Lbl_Stock"+trcnt);
    obj.innerHTML = '';

	obj = eval("document.all.Lbl_OprStock"+trcnt);
    obj.innerHTML = '';

	obj = eval("document.all.Txt_Price"+trcnt);
    obj.value = '';

	obj = eval("document.all.Lbl_Amount"+trcnt);
    obj.innerHTML = '';

    obj = eval("document.all.Txt_PNum"+trcnt);
    obj.value = '';
    obj.focus(); 
}

var varPartNum;
var varCount;
var objPartCell;

function fnAddRow(id)
{
	var tbody = document.getElementById(id).getElementsByTagName("TBODY")[0];
    var row = document.createElement("TR")

    var td0 = document.createElement("TD")
    td0.innerHTML = cnt+1;

    var td1 = document.createElement("TD")
    td1.innerHTML = fnCreateCell('TXT','PNum',7);
   
    var td2 = document.createElement("TD")
    td2.innerHTML = fnCreateCell('TXT','Qty',3);
    td2.align = "center";
    
    var td3 = document.createElement("TD")
    td3.innerHTML = fnCreateCell('TXT','Price',8);
    
    var td4 = document.createElement("TD")
	td4.id = "Lbl_Stock"+cnt;
    td4.align = "Right";

	var td12 = document.createElement("TD")
	td12.id = "Lbl_OprStock"+cnt;
    td12.align = "Right";
    
    var td5 = document.createElement("TD")
	td5.id = "Lbl_Amount"+cnt;
    td5.align = "Right";
    
    var td6 = document.createElement("TD")
	td6.id = "Lbl_Desc"+cnt;
    td6.align = "Left";

    var td7 = document.createElement("TD")
    td7.innerHTML = fnCreateCell('CHK','BO',1);
    td7.align = "center";

    var td8 = document.createElement("TD")
    td8.innerHTML = fnCreateCell('Cbo','OrdPartType',1);
    td8.align = "Left";

    var td9 = document.createElement("TD")
    td9.innerHTML = fnCreateCell('CHK','Cap',1);
    td9.align = "Center";

    var td10 = document.createElement("TD")
    td10.innerHTML = fnCreateCell('IMG','',1);

	var td11 = document.createElement("TD")
    td11.innerHTML = fnCreateCell('IMG','P',1);
	
	// international posting changes
	var td13 = document.createElement("TD")
	td13.id = "Lbl_owner_comp"+cnt;
    td13.align = "Left";
    
    var td14 = document.createElement("TD")
	td14.id = "Lbl_owner_currency"+cnt;
    td14.align = "Left";
    
    var td15 = document.createElement("TD")
	td15.id = "Lbl_local_cost"+cnt;
    td15.align = "Left";
    
    var td16 = document.createElement("TD")
	td16.id = "Lbl_local_ext_cost"+cnt;
    td16.align = "Left";
    

    row.appendChild(td0);
	row.appendChild(td11);
	row.appendChild(td10);
    row.appendChild(td1);
    row.appendChild(td2);
    row.appendChild(td4);
    row.appendChild(td12);
    row.appendChild(td6);
    row.appendChild(td13);
    row.appendChild(td14);
    row.appendChild(td3);
    row.appendChild(td5);
    row.appendChild(td15);
    row.appendChild(td16);

    tbody.appendChild(row);
	cnt++;
	document.frmCart.hRowCnt.value = cnt;
	// PC-1002 - Auto Populate Company and Currency Info on Manual Adjusment
	// before add row header dropdown select after add row means header dropdown will change choose one
	document.getElementById('cbo_owner_company').value= 0;
}

function fnCreateCell(type,val,size)
{
	param = val;
	val = val + cnt;
	var html = '';
	
	if (type == 'TXT')
	{
		if (param == 'PNum') {
				html = '<input type=text size='+size+' name=Txt_'+val+' class=InputArea onBlur=fnSetPartSearch('+cnt+',this); onFocus=changeBgColor(this,\'#AACCE8\'); value=\'\'>';
						}
		else if (param == 'Qty') {
				html = '<input type=text size='+size+' name=Txt_'+val+' class=InputArea onblur=validate('+cnt+',this); onFocus=changeBgColor(this,\'#AACCE8\'); value=\'\'>';
						}
		else if (param == 'Price') {
				html = '<input type=text size='+size+' name=Txt_'+val+' class=InputArea onBlur=fnCalExtPrice(this,'+cnt+'); onFocus=changeBgColor(this,\'#AACCE8\'); tabindex=\'-1\' value=\'\'>';
						}
	}
	
	else if (type == 'CHK')
	{
		html = '<input type=checkbox name=Chk_'+val+' tabindex="-1">';
	}
	
	else if (type == 'Lbl')
	{
		html = '&nbsp;';
	}
	
	else if (type == 'Btn')
	{
		var fn = size;
		html = '<input type=button onclick='+fn+' value = PartLookup name=Btn_'+val+' class=Button>';
	}
	
	else if (type == 'Cbo')
	{
		var fn = size;
		html = '<select name=Cbo_'+val+' class=RightText tabindex="-1" onchange="fnChangeStatus(this.value,'+val+');"><option value="50300">C</option><option value="50301">L</option></select>';
	}

	else if (type == 'IMG')
	{
		var fn = size;
		if (param == 'P'){
			html = '<a href=javascript:fnInventoryDetails('+val+');><img border=0 Alt=Part Inventory Details valign=left src=/images/product_icon.gif height=10 width=9></a>';
		}else {
			html = '<a href=javascript:fnRemoveItem('+val+');><img border=0 Alt=Remove from cart valign=left src=/images/btn_remove.gif height=10 width=9></a>';
		}
	}
	
	return html;
}

function fnSetPartSearch(count,obj)
{
	objPartCell = eval("document.frmCart.Txt_PNum"+count);
	objPartQty = eval("document.frmCart.Txt_Qty"+count);
	changeBgColor(obj,'#FFFFFF');

	var priceobj = eval("document.frmCart.Txt_Price"+count);
	priceobj.value = '';
	//validate(count,obj);
}


function fnClearVal(count)
{
	varCount = count;
	objPartCntCell = eval("document.frmPhOrder.hPrice"+varCount);
	if (objPartCntCell == "" || objPartCntCell == null) 
	{
	}

	objPartCntCell.value = 0;
}

function fnReturnStatus(status)
{
	if (status)
	{
		return 'Y';
	}
	else
	{
		return '';
	}
}

function fnCreateOrderString()
{
	var varMAType = document.frmManualAdjTxn.maTypeId.value;
	var varfromcosttype = document.frmManualAdjTxn.hmaFromType.value;
	var varRowCnt = document.frmCart.hRowCnt.value;
	var k;
	var j;
	var i;
	var obj;
	var str;
	var token = '^';
	var cnum = '';
	var status = true;
	var inputstr = '';
	var stockqty = 0;
	var qty = 0;
	var objstock;
	var pnum = '';
	var parterrmsg = '';
	var qtyerrmsg = '';
	var objcostea;
	var costea=0;
	var costmsg ='';
	var ownerCompMsg = '';
	var localCostMsg = '';
	
	for (k=0; k < varRowCnt; k ++)
	{
		obj = eval("document.frmCart.Txt_PNum"+k);
		pnum = TRIM(obj.value);
		objqty = eval("document.frmCart.Txt_Qty"+k);
		qty = parseFloat(objqty.value);
		objcostea = eval("document.frmCart.Txt_Price"+k);
		costea = parseFloat(RemoveComma(objcostea.value));
		if (pnum != ''){
			if(isNaN(qty)){
				j = k + 1;
				qtyerrmsg =  qtyerrmsg + ' '+ j ;
			}
			str = pnum + token;
			objstock = eval("document.frmCart.hStock"+k);
			if(objstock){
				stockqty = parseFloat(RemoveComma(objstock.value));
				/*if(varfromcosttype != '' && qty > stockqty && varMAType != '48056') {
					Error_Details(" Qty adjusted is greater than inventory qty for " + pnum);
				}*/
			}
			
			// international changes
			ownerID =  eval("document.frmCart.cbo_owner_company"+k).value;
			localCost =  eval("document.frmCart.Txt_Local_Cost"+k).value;
			localCost = parseFloat(RemoveComma(localCost));

			
			str = str + TRIM(objqty.value) + token;
			obj = eval("document.frmCart.Txt_Price"+k);
			str = str + TRIM(RemoveComma(obj.value))+ token ;
			// international 
			str = str + ownerID + token;
			str = str + localCost + '|';
			inputstr = inputstr + str;
			
			if(costea == 0.00){
				costmsg = costmsg + ' ' + pnum +' ';
			}
			// International posting validation
			if(ownerID ==0){
				ownerCompMsg = ownerCompMsg + pnum  +' ';
			}
			if( localCost == 0.00 || localCost == ''){
				localCostMsg = localCostMsg + pnum  +' ';
			}
		} 
		else if (!isNaN(qty))	{
				j = k + 1;
				parterrmsg =  parterrmsg + ' '+ j ;
			}
	}

	if(parterrmsg != ''){
		Error_Details(Error_Details_Trans(message_accounts[74],parterrmsg));
	}

	if(qtyerrmsg != ''){
		Error_Details(Error_Details_Trans(message_accounts[75],qtyerrmsg));
	}
	if(costmsg != ''){
		Error_Details(Error_Details_Trans(message_accounts[76],costmsg));
	}

	else if(inputstr == '' ){
		Error_Details(message_accounts[77]);
	}
	if(ownerCompMsg !=''){
		Error_Details(Error_Details_Trans(message_accounts[539],ownerCompMsg));
	}
	if(localCostMsg !=''){
		Error_Details(Error_Details_Trans(message_accounts[540],localCostMsg));
	}
	
	return inputstr;
}

function fnCalculateTotal()
{
	var varRowCnt = document.frmCart.hRowCnt.value;
	var k;
	var obj;
	var qty = 0;
	var price = 0.0;
	var total = 0.0;
	var pnum = '';

	for (k=0; k <= varRowCnt; k ++)
	{
		obj = eval("document.frmCart.Txt_PNum"+k);
		if (obj)
		{
			pnum = obj.value;
			if (pnum != '')
			{	
				obj = eval("document.frmCart.Txt_Qty"+k);
				qty = parseFloat(obj.value);
				obj = eval("document.frmCart.Txt_Local_Cost"+k);
				price = parseFloat(RemoveComma(obj.value));
				total = total + (price * qty);
			}
		}
	}

	document.frmCart.hTotal.value = total;
	document.all.Lbl_Total.innerHTML = '<b>'+ formatNumber(total)+'</b>&nbsp;';
}

function fnUpdatePrice()
{
	//alert( document.frmCart.Cbo_Construct[document.frmCart.Cbo_Construct.selectedIndex].value);
	if (document.frmCart.Cbo_Construct.value == '0')
	{
		alert(message_accounts[295]);
		return;
	}
	var varPartNums = '';;
	var blQty;
	var blPrice;
	var varAccId = parent.document.all.Txt_AccId.value;
	var varAccIdVal = varAccId.value;
	var comma = ',';
	var varRowCnt = document.frmCart.hRowCnt.value;
	var k;
	var varPartVal = "";

	for (k=0; k < varRowCnt; k ++)
	{
		objPartCell = eval("document.frmCart.Txt_PNum"+k);
		objQtyCell = eval("document.frmCart.Txt_Qty"+k);
		objPriceCell = eval("document.frmCart.Txt_Price"+k);
		
		if (objPartCell == "" || objPartCell == null )
		{
				comma = "";
		}
		else if (objPartCell.value == "")
		{
				comma = "";
		}
		else
		{
			comma = ",";
			varPartNums = varPartNums + objPartCell.value + comma;
			varPartVal = varPartVal + objPartCell.value + comma;
		}

		if (objQtyCell == "" || objQtyCell == null  || objQtyCell == "undefined" || objPriceCell == "" || objPriceCell == null  || objPriceCell == "undefined" )
		{
		}
		else if (objQtyCell.value =="" | objQtyCell.value ==null)
		{
		
		}
		else if (!objQtyCell.value.match(/^\d+$/))
		{
			blQty = "1";
			break;
		}
		else if(!RemoveComma(objPriceCell.value).match(/^[\d\.]+$/))
		{
			blPrice = "1";
			break;
		}
	}


	var varParsePartVal = varPartVal.substring(0,varPartVal.length -1);
	//parent.document.all.Txt_PartNum.value = varParsePartVal;
	
	if (varAccIdVal == "")
	{
		 Error_Details(message[66]);
	}
	else if (varPartNums == "" || varPartNums == null)
	{
		Error_Details(message[67]);
	}
	else if (blQty == "1")
	{
	 	Error_Details(message[68]);
	}
	else if (blPrice == "1")
	{
		 Error_Details(message[69]);
	}

	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	//fnSubmit();	
	var str = fnCreateOrderString();
	
	document.frmCart.hAction.value = 'UpdatePrice';
	document.frmCart.hInputStr.value = str;
	varPartNums = varPartNums.substr(0,varPartNums.length-1);
	document.frmCart.hPartNumStr.value = varPartNums;
	document.frmCart.hConsValue.value = document.frmCart.Cbo_Construct[document.frmCart.Cbo_Construct.selectedIndex].id;
	document.frmCart.submit();
}


function fnCalExtPrice(obj,cnt)
{
	var qty = 0;
	var price = 0.0;
	var pobj = eval("document.frmCart.Txt_Qty"+cnt);
	var pcost = eval("document.frmCart.Txt_Price"+cnt);
	var pcostval = pcost.value;
	qty = parseFloat(pobj.value);

	
    
	if(pcostval.charAt(0) == '-')
	{	
		setErrMessage('<span class=RightTextRed>'+message_accounts[294]+'</span>');
	}

	else if (!isNaN(qty)) 
	{
		price = parseFloat(RemoveComma(obj.value));
		var eprice = price * qty;
		var pobj = eval("document.all.Lbl_Amount"+cnt);
		pobj.innerHTML = formatNumber(eprice)+"&nbsp;";
		// to calculate the local cost
		var localCostObj = eval("document.frmCart.Txt_Local_Cost"+cnt);
		fnChangeLocalCost (localCostObj, cnt);
	}
	changeBgColor(obj,'#ffffff');
}

function fnChangeLocalCost (obj, cntVal){
	var qty = 0;
	var price = 0.0;
	
	var qObj = eval("document.frmCart.Txt_Qty"+cntVal);
	var costObj = eval("document.frmCart.Txt_Price"+cntVal);
	var localCostVal = obj.value;
	qty = parseFloat(qObj.value);
	var localCurrencyObj = eval("document.all.hLocalCurrency" + cntVal);
	var ownerCurrencyObj = eval("document.frmCart.cbo_owner_currency"+ cntVal);
	
	if(localCurrencyObj.value == ownerCurrencyObj.value){
		localCostVal = costObj.value;
		eval("document.frmCart.Txt_Local_Cost"+cntVal).value = localCostVal;
	}
	
	
	if(localCostVal.charAt(0) == '-')
	{	
		setErrMessage('<span class=RightTextRed>'+message_accounts[294]+'</span>');
	}else if (!isNaN(qty)) 
	{
		price = parseFloat(RemoveComma(localCostVal));
		var eprice = price * qty;
		var pobj = eval("document.all.Lbl_local_ext_cost"+cntVal);
		pobj.innerHTML = formatNumber(eprice)+"&nbsp;";
		fnCalculateTotal();
	}
	
}

var vFilterShowFl = false;
function fnShowFilters(val)
{
	var obj = eval("document.all."+val);
	var obj1 = eval("document.all."+val+"img");
	if (obj.style.display == 'none')
	{
		obj.style.display = 'block';
		if(obj1){
			obj1.src = '/images/minus.gif';
		}
	}
	else
	{
		obj.style.display = 'none';	
		if(obj1){
			obj1.src = '/images/plus.gif';
		}
	}
}

function fnToggleCart()
{
	fnShowFilters('PartnPricing');
	fnShowFilters('PartnPricingHeader');
	fnShowFilters('PartnPricingDiv');
	fnShowFilters('PartnPricingFooter');
}



function fnLoanerInfo(partnum,repid)
{
	windowOpener('/GmLoanerPartRepServlet?hAction=Load&hPartNum='+encodeURIComponent(partnum)+'&hRepId='+repid,"Pack","resizable=yes,scrollbars=yes,top=150,left=150,width=850,height=610");	
}


function fnChangeStatus(val,cnt)
{
	obj = eval("document.frmCart.Chk_BO"+cnt);
	if (obj)
	{
		if (val == 50301)
		{
			obj.checked = false;
			obj.disabled = true;
		}
		else
		{
			obj.disabled = false;
		}
	}
}

function fnCheckConstructAccount(conobj)
{
	/*
	var obj = document.frmPhOrder.Txt_AccId;
	var conlbl = obj[obj.selectedIndex].label;
	
	if (obj.value == 0)
	{
		conobj.selectedIndex = 0;
	}else
	{
		if (conlbl == 0)
		{
			 Error_Details('This Account does not have Capitated Pricing.');
		}
	}

	if (ErrorCount > 0)
	{
			Error_Show();
			conobj.selectedIndex = 0;
			Error_Clear();
			return false;
	}
	*/

}

function fnRemoveItem(val)
{
	obj = eval("document.frmCart.Txt_Qty"+val);
	obj.value = '';
	obj = eval("document.frmCart.Txt_PNum"+val);
	obj.value = '';
	obj = eval("document.frmCart.Txt_Price"+val);
	obj.value = '';
    obj = eval("document.all.Lbl_Desc"+val);
    obj.innerHTML = "";
    obj = eval("document.all.Lbl_Stock"+val);
    obj.innerHTML = "";  
	 obj = eval("document.all.Lbl_OprStock"+val);
    obj.innerHTML = "";  
    obj = eval("document.all.Lbl_Amount"+val); 
    obj.innerHTML = "";
    // international posting changes
    obj = eval("document.frmCart.cbo_owner_company"+val); 
    obj.value = "0";
    obj = eval("document.frmCart.cbo_owner_currency"+val); 
    obj.value = "0";
    obj = eval("document.frmCart.Txt_Local_Cost"+val); 
    obj.value = "";
    obj = eval("document.all.Lbl_local_ext_cost"+val); 
    obj.innerHTML = "";
    obj = eval("document.all.hLocalEaCost"+val); 
    obj.innerHTML = "";
    obj = eval("document.all.hOwnerId"+val); 
    obj.innerHTML = "";

	fnCalculateTotal();
}

function fnClearCart()
{
	var varRowCnt = document.frmCart.hRowCnt.value;
	for (val=0; val < varRowCnt; val ++)
	{
		obj = eval("document.frmCart.Txt_Qty"+val);
		obj.value = '';
		obj = eval("document.frmCart.Txt_PNum"+val);
		obj.value = '';
		obj = eval("document.frmCart.Txt_Price"+val);
		obj.value = '';
		obj = eval("document.all.Lbl_Desc"+val);
		obj.innerHTML = "";
		obj = eval("document.all.Lbl_Stock"+val);
		obj.innerHTML = "";  
		obj = eval("document.all.Lbl_OprStock"+val);
		obj.innerHTML = "";  
		obj = eval("document.all.Lbl_Amount"+val); 
		obj.innerHTML = "";
	}
	fnCalculateTotal();
}
// PC-1002 - Auto Populate Company and Currency Info on Manual Adjusment
// when comapny selected based on company currency will selected in currency select box
function fnChangeOwnerComp(companyIdName, i){
	var selectedCurrencyId = 0 ;
	var selectedCompanyId =document.getElementById(companyIdName+i).value;
	if(selectedCompanyId !=='0'){
        selectedCurrencyId = Number(companyIdObject[selectedCompanyId]);
	}
	document.getElementById('cbo_owner_currency'+i).value= selectedCurrencyId;
	document.getElementById('cbo_owner_company').value= 0;
}
// PC-1002 - Auto Populate Company and Currency Info on Manual Adjusment
// when header comapany ddropdown selected based on all companya and currency dropdown will select
function fnChangeOwnerCompSelectAll(){
	var selectedCurrencyId = 0 ;
	var selectedCompanyId =document.getElementById('cbo_owner_company').value;
	if(selectedCompanyId !=='0'){
        selectedCurrencyId = Number(companyIdObject[selectedCompanyId]);
	}
	var tbodyLength = document.getElementById('PartnPricing').tBodies[0].rows.length;
	for(let i = 0; i<tbodyLength; i++){
		document.getElementById('cbo_owner_company'+i).value= selectedCompanyId;
		document.getElementById('cbo_owner_currency'+i).value= selectedCurrencyId;
	}
	
	
}