function fnSortList(){
    var list = $("ul"),
        origOrder = list.children();
    
    //list.on("click", ":checkbox", function() {
        var i, checked = document.createDocumentFragment(),
            unchecked = document.createDocumentFragment();
        for (i = 0; i < origOrder.length; i++) {
            if (origOrder[i].getElementsByTagName("input")[0].checked) {
                checked.appendChild(origOrder[i]);
            } else {
                unchecked.appendChild(origOrder[i]);
            }
        }
        list.append(checked).append(unchecked);
//    });
}
function fnSortUlList(){
    var list = $("ol"),
    origOrder = list.children();

//list.on("click", ":checkbox", function() {
    var i, checked = document.createDocumentFragment(),
        unchecked = document.createDocumentFragment();
    for (i = 0; i < origOrder.length; i++) {
        if (origOrder[i].getElementsByTagName("input")[0].checked) {
            checked.appendChild(origOrder[i]);
        } else {
            unchecked.appendChild(origOrder[i]);
        }
    }
    list.append(checked).append(unchecked);
//});
}
function fnSelectDefault(obj){
	strSelVal = document.frmMultiUserGroup.strOpt.value;
	if (strSelVal== "SECUSR"){
	var obj = document.frmMultiUserGroup.secGrp;
	}
	var companyId = document.frmMultiUserGroup.companyId.value;
	
	if(obj.value!='0' && obj.value!= undefined && companyId!=undefined && companyId!='0'){

		var url = '\gmMultiUserGroupAction.do?strOpt=' + strSelVal + '&optVal=' + obj.value + '&ramdomId='+Math.random()+'&companyId='+companyId;
		
		if (typeof XMLHttpRequest != "undefined") 
		{
		   req = new XMLHttpRequest();
		} 
		else if (window.ActiveXObject) 
		{
		   req = new ActiveXObject("Microsoft.XMLHTTP");
		}
		req.open("GET", url, true);
		req.onreadystatechange = callback;
		req.send(null);
		document.frmMultiUserGroup.btn_history.disabled = false; 
   }else{
	   	document.frmMultiUserGroup.btn_history.disabled = true; 
   }
}
function callback() {
    if (req.readyState == 4) {
        if (req.status == 200) {
	       var strRetVal = req.responseText;
	       var strOpt_ = document.frmMultiUserGroup.strOpt.value;
	       if(strOpt_ =='SECUSR'){
	    	   setMessageSecUser(strRetVal);
	       }else
	    	   setMessageUserSec(strRetVal);
        }        
    }
}
function onLoad(strOpt){
 
	if(strOpt =='SECUSR'){
		fnSelectDefault(document.frmMultiUserGroup.secGrp);
	}
	else{
		fnSelectDefault(document.frmMultiUserGroup.userList);	
	}
}
function fnLoadUser(){
	document.frmMultiUserGroup.haction.value = "Reload";
	document.frmMultiUserGroup.action = '\gmMultiUserGroupAction.do';
	document.frmMultiUserGroup.submit();
}
function fnSubmit(){
	var strOpt = document.frmMultiUserGroup.strOpt.value;
	var obj;
	var drpObj;
	var varLab = "";
	var str = "";
	var strDefGrp = "";
	var strDrp = "";
	var companyId = document.frmMultiUserGroup.companyId.value;
	fnValidateDropDn('companyId',' Company ');
	if(strOpt == 'SECUSR'){
		obj = document.frmMultiUserGroup.Chk_Grpuser;
		drpObj = 'secGrp';
		strDrp = "Security Group";
		varLab = "User List";
	}else{
		obj = document.frmMultiUserGroup.Chk_Grpsec;
		drpObj = 'userList';
		strDrp = "User";
		varLab = "Security Group";
		/*
		fnValidation(document.frmMultiUserGroup.Opt_Grp,'Default Security Group');		
		for(var j=0;j<document.frmMultiUserGroup.Opt_Grp.length;j++){
			if(document.frmMultiUserGroup.Opt_Grp[j].checked){
				strDefGrp = document.frmMultiUserGroup.Opt_Grp[j].value; 
			}
		}*/
		//fnValidateDefGroup(obj,strDefGrp);//commetned this one for OP-430
	}
	fnValidation(obj,varLab);
	fnValidateDropDn(drpObj,strDrp);
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	for(var j=0;j<obj.length;j++){
		if(obj[j].checked){
			str += obj[j].value + ","; 
		}
	}
	document.frmMultiUserGroup.hinputstring.value = str;
	document.frmMultiUserGroup.defaultgrp.value = strDefGrp;
	document.frmMultiUserGroup.haction.value = "save";
	document.frmMultiUserGroup.action = '\gmMultiUserGroupAction.do';
    fnStartProgress('Y');
	document.frmMultiUserGroup.submit();
}
function fnValidation(obj,strVal)
{
	var objCheckCategoriesArr = obj;
	var categoryCheck = '0';
   for( var j=0; j< objCheckCategoriesArr.length; j++)
	{ 
		if(objCheckCategoriesArr[j].checked)	
		{
			categoryCheck = '1';	
		}
	}
   if( categoryCheck =='0')
	{
		 Error_Details('Please select <b>' + strVal + '</b>.');
	} 
}
function fnValidateDefGroup(obj,strVal){
	var objCheckCategoriesArr = obj;
	var categoryCheck = '0';
   for( var j=0; j< objCheckCategoriesArr.length; j++)
	{ 
		if(objCheckCategoriesArr[j].checked)	
		{
			if(objCheckCategoriesArr[j].value == strVal){
				categoryCheck = '1';
				break;
			}
		}
	}
   if( categoryCheck =='0')
	{
		 Error_Details('<b>Default Security Group</b> is not matched with the <b>Security Groups</b>.');
	}
}
function fnSelectAll(obj){
	var frmObj = document.frmMultiUserGroup.Chk_Grpuser;
	var strOpt = document.frmMultiUserGroup.strOpt.value;
	var srcObj = obj;
	for( var j=0; j< frmObj.length; j++)
	{ 
		if(srcObj.checked){
			frmObj[j].checked = true;
		}
		else{
			frmObj[j].checked = false;
		}
	}
	if(srcObj.checked== false){
		if(strOpt =='SECUSR')
			fnSelectDefault(document.frmMultiUserGroup.secGrp);
		else
			fnSelectDefault(document.frmMultiUserGroup.userList);
	}
}

function setMessageSecUser(strVal)
{
	var arrVal = strVal.split(',');
	var arrObjChkList;
	var arrObjOptList;
	var blFlg = false;
	var strOpt = document.frmMultiUserGroup.strOpt.value;
	if(strOpt == 'SECUSR'){
		arrObjChkList = document.frmMultiUserGroup.Chk_Grpuser;
	}else{
		arrObjChkList = document.frmMultiUserGroup.Chk_Grpsec;
		arrObjOptList = document.frmMultiUserGroup.Opt_Grp;
	}
	for(var k=0;k<arrObjChkList.length;k++){
			arrObjChkList[k].checked = false;
			if(arrObjOptList)
				arrObjOptList[k].checked = false;
	}
	for(var i=0;i<arrVal.length;i++){
		for(var j=0;j<arrObjChkList.length;j++){
			//alert(arrVal[i].replace('\n','') + ' - ' + arrObjChkList[j].value);
			var defSplit = arrVal[i].split('^');	//		
			if(defSplit[0]==arrObjChkList[j].value){
				arrObjChkList[j].checked = true;				
				if(defSplit[1] == 'Y' && blFlg == false){
					for(var k=0;k<arrObjOptList.length;k++){
						if(arrObjOptList[k].value == defSplit[0])
							arrObjOptList[k].checked= true;
					}
					blFlg = true;
				}
			}
		}
	}
		fnSortList();
		fnSortUlList();
} 

function setMessageUserSec(strVal){
	
	var arrVal = strVal.split('@');
	var arrSecurityGrp =arrVal[0].split(',');
	var arrDefaultGrp = arrVal[1].split(',');
	var arrObjChkList;
	var arrObjOptList;
	var blFlg = false;
	var strOpt = document.frmMultiUserGroup.strOpt.value;
	
	
	arrObjChkList = document.frmMultiUserGroup.Chk_Grpsec;
	arrObjOptList = document.frmMultiUserGroup.Opt_Grp;
	
	for(var k=0;k<arrObjChkList.length;k++){
			arrObjChkList[k].checked = false;
			if(arrObjOptList)
				arrObjOptList[k].checked = false;
	}
	
	
	for(var i=0;i<arrSecurityGrp.length;i++){
		for(var j=0;j<arrObjChkList.length;j++){
			var defSplit = arrSecurityGrp[i].split('^');
			if(defSplit[0]==arrObjChkList[j].value){
				arrObjChkList[j].checked = true;
			}
		}
	}
	/*
	for(var i=0;i<arrDefaultGrp.length;i++){
		for(var j=0;j<arrObjOptList.length;j++){
			var defSplit = arrDefaultGrp[i].split('^');
			if(defSplit[0]==arrObjOptList[j].value){
				arrObjOptList[j].checked = true;
			}
		}
	}*/	
	fnSortList();
		//fnSortUlList();
} 
function fnSwap(){
	var strOpt = document.frmMultiUserGroup.strOpt.value;
	if(strOpt =='SECUSR'){
		document.frmMultiUserGroup.strOpt.value = "USRSEC";
	}else{
		document.frmMultiUserGroup.strOpt.value = "SECUSR";
	}
	document.frmMultiUserGroup.action = '\gmMultiUserGroupAction.do';
	document.frmMultiUserGroup.submit();
}
function fnload(){
	var strOpt = document.frmMultiUserGroup.strOpt.value;
	var companyId = document.frmMultiUserGroup.companyId.value;
	
	if(strOpt =='SECUSR') {
			var secGrpId = document.frmMultiUserGroup.secGrp.value;
			fnValidateDropDn('secGrp',' Group ');
	}
	else {
		var userListId = document.frmMultiUserGroup.userList.value;
		fnValidateDropDn('userList',' User ');
	}	
	fnValidateDropDn('companyId',' Company ');
	
	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}
	
	document.frmMultiUserGroup.action = '\gmMultiUserGroupAction.do';
	fnStartProgress('Y');
	document.frmMultiUserGroup.submit();
	
}


/*Add History button to view information*/
function fnHistory(){
	var strOpt = document.frmMultiUserGroup.strOpt.value;
	var companyId = document.frmMultiUserGroup.companyId.value;
	
	if(strOpt =='SECUSR') {
			var secGrpId = document.frmMultiUserGroup.secGrp.value;
			var secGrpName=document.frmMultiUserGroup.secGrp.options[document.frmMultiUserGroup.secGrp.selectedIndex].text
			fnValidateDropDn('secGrp',' Group ');
	}
	else {
		var userListId = document.frmMultiUserGroup.userList.value;
		var userName=document.frmMultiUserGroup.userList.options[document.frmMultiUserGroup.userList.selectedIndex].text
		fnValidateDropDn('userList',' User ');
	}	
	fnValidateDropDn('companyId',' Company ');
	
	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}
	if(userListId==undefined){
		userListId="";	
	}
	if(secGrpId==undefined){
		secGrpId="";
		}
	windowOpener("\gmMultiUserGroupAction.do?haction=history&secGrp="+secGrpId+"&userList="+userListId+"&companyId="+companyId+
			"&strOpt="+strOpt+"&secGrpName="+secGrpName+"&userName="+userName,"HistoryDetails","resizable=yes,scrollbars=yes,top=150,left=300,width=850,height=550");	
}