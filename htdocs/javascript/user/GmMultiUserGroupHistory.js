/*Load History information using dhtmlx */
var gridObj;
function fnOnPageLoad(){
	if(objGridData.value != ''){ 
		gridObj = initGridData('dataGridDiv',objGridData);   
	}	
}
/*Initialize Grid*/
function initGridData(divRef,gridData){
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);
	gObj.enableTooltips("true,true,true,true,true");
	gObj.init();
	gObj.loadXMLString(gridData);
	return gObj;	
}


function fnExportExcel(){
	gridObj.toExcel('/phpapp/excel/generate.php');

}