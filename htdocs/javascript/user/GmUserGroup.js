gridObj ="";
function fnLoad(){
	var frm=document.frmUserGroup;
	frm.strOpt.value="load";
	frm.submit();
	
}
function  fnPageLoad(){
	if (objGridData != '')
	{
		gridObj = initGridData('grpData',objGridData);
		gridObj.enableTooltips("false,true,true,true,true,true");
		gridObj.attachHeader("#rspan,#select_filter,#select_filter,#text_filter,#select_filter,#text_filter");
		//gridObj.groupBy(1);
		gridObj.customGroupFormat=function(name,count){
            return "<font size=1>"+name+" ( " +count+ " ) </font>";
        }
		gridObj.setColumnHidden(2,true);
		//gridObj.collapseAllGroups(); 	        
	}	
}
function fnMSSubmit(){
	fnValidateDropDn('groupName',' Security Group Name ');
	fnValidateDropDn('moduleName',' Module Name ');
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	var frm=document.frmUserGroup;
	frm.strOpt.value="SCMD";
	frm.haction.value="save";
	fnStartProgress('Y');
	frm.submit();
}
function fnSubmit(){
	var frm=document.frmUserGroup;
	
	fnValidate();

	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	frm.strOpt.value="save";
	fnStartProgress('Y');
	frm.submit();
}

function fnValidate()
{
	var frm = document.frmUserGroup; 
	//fnValidateDropDn('userDept',' User Department ');
	fnValidateDropDn('userNm',' User ');
	fnValidateDropDn('groupName',' Group Name ');
}
function fnVoidGroupMapping(){
	var frm=document.frmUserGroup;
	var group_mapping_id=document.getElementById("moduleName").value;
	frm.action = "/GmCommonCancelServlet?hTxnName="+group_mapping_id+"&hCancelType=VGPMAP&hTxnId="+group_mapping_id;
	frm.submit();
		
}
function fnMapNewUser(id){
	var frm=document.frmUserGroup;
	frm.strOpt.value="SCGRP";
	frm.grpMappingID.value = id;
	frm.submit();
}
// function for report
function fnOnPageLoad()
{	
	if (objGridData != '')
	{
		gridObj = initGridData('grpData',objGridData);
		gridObj.enableTooltips("false,true,true,true,true,true");
		gridObj.attachHeader("#rspan,#select_filter,#select_filter,#select_filter,#select_filter");
		//gridObj.groupBy(1);
		gridObj.customGroupFormat=function(name,count){
            return "<font size=1>"+name+" ( " +count+ " ) </font>";
        }
		gridObj.setColumnHidden(2,true);
		//gridObj.collapseAllGroups();	        
	}	
}
function fnEditGrpMap(id, compid){
	var strOpt="edit";
		// Below if condition is added when edit icon is clicked from Module-Sec Group map report is used.
	if (compid == undefined || compid == ''){
		compid = 0;
		strOpt = "SECUSR";
	}
	var frm=document.frmUserGroup;
	frm.action = '\gmMultiUserGroupAction.do?grpmapId=' + id+'&companyId='+compid+'&strOpt='+strOpt+'&strAccessFl='+strAccess;
	frm.submit();

}
function fnUserLoad(){
	var frm=document.frmUserGroup;
	frm.strOpt.value="report";
	frm.submit();	
}

function fnReset(){
	var frm=document.frmUserGroup;	
	frm.grpMappingID.value="";
	frm.userNm.value ='';
	frm.groupName.value ='';
	document.getElementById("grpDesc").innerHTML="";
	frm.strOpt.value="load";
	frm.submit();
}

function fnLoadReport(){
	var frm=document.frmUserGroup;
	frm.strOpt.value="ReloadReport";
	fnStartProgress('Y');
	frm.submit();
	
}
function fnCallReport(){	
	if(event.keyCode == 13)
	{	 
		fnLoadReport();
	}	
}
function fnCallAjax(obj) 
{	
	if(obj.value!='0' || obj.value!=undefined){
		var frm=document.frmUserGroup;
		var url = '\gmUserGroup.do?groupId='+obj.value+'&strOpt=UGAJAX';
	   
		if (typeof XMLHttpRequest != "undefined") 
		{
		   req = new XMLHttpRequest();
		} 
		else if (window.ActiveXObject) 
		{
		   req = new ActiveXObject("Microsoft.XMLHTTP");
		}
		req.open("GET", url, true);
		req.onreadystatechange = callback;
		req.send(null);
   }
}
function fnMDCallAjax(obj) 
{	
	if(obj.value!='0' || obj.value!=undefined){
		var frm=document.frmUserGroup;
		var url = '\gmUserGroup.do?groupId='+obj.value+'&strOpt=UGAJAX';
	   
		if (typeof XMLHttpRequest != "undefined") 
		{
		   req = new XMLHttpRequest();
		} 
		else if (window.ActiveXObject) 
		{
		   req = new ActiveXObject("Microsoft.XMLHTTP");
		}
		req.open("GET", url, true);
		req.onreadystatechange = mdCallBack;
		req.send(null);
   }
}

function callback() {
    if (req.readyState == 4) {
        if (req.status == 200) {
	       var strGroupDesc = req.responseText;
	       document.getElementById("grpDesc").innerHTML=strGroupDesc;
        }        
    }
}
function mdCallBack() {
    if (req.readyState == 4) {
        if (req.status == 200) {
	       var strGroupDesc = req.responseText;
	       document.getElementById("mdDesc").innerHTML=strGroupDesc;
        }        
    }
}

function fnOnLoad(){
	fnCallAjax(document.frmUserGroup.groupName);
	setTimeout("fnMDCallAjax(document.frmUserGroup.moduleName)",500);
}

function fnMSReset(){
	var frm=document.frmUserGroup;
	frm.moduleName.value='0';
	frm.groupName.value='0';
	document.getElementById("grpDesc").innerHTML="";
	document.getElementById("mdDesc").innerHTML="";
	frm.strOpt.value="load";
}
function initGridData(divRef,gridData)
{
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.init();
	gObj.loadXMLString(gridData);
	return gObj;
}


function fnExportExcel() {
//Commenting these lines for the PMT-36095, to change dhtmlx excel export to server side export
//since not able to export larger data	
	
	//var edit_rowId = gridObj.getColIndexById("editIcon");
	//gridObj.setColumnHidden(edit_rowId,true);
	//gridObj.toExcel('/phpapp/excel/generate.php');
	//gridObj.setColumnHidden(edit_rowId,false);
	
	var grpNm = document.frmUserGroup.groupName.value;
	var userNm = document.frmUserGroup.userNm.value;
	var compId = document.frmUserGroup.companyId.value;

	windowOpener("/gmUserGroup.do?exportExcelFl=Y&strOpt=ReloadReport&groupName="+grpNm+"&userNm="+userNm+"&companyId="+compId,"INVPRINT","resizable=yes,scrollbars=yes,top=300,left=200,width=800,height=490");
}
//Exporting the report to PDF file .
function fnPDFDownload() {
	var frm=document.frmUserGroup;
	frm.strOpt.value="PDFDownload";
	frm.submit();
}
//fnOnPageLoadRpt - this function is used to load dhtmlx grid on page load for the jsp(GmUserGroupReport.jsp)
//Copied from the function fnOnPageLoad()
//for the PMT-36095, adding pagination for the dhtmlx grid report
function fnOnPageLoadRpt(){
	if (objGridData != '')
	{
		//since we are using pagination so need to enable distributed parsing
		//but it is not available in initGridData(), so changing to this function call initGridWithDistributedParsing()
		gridObj = initGridWithDistributedParsing('grpData',objGridData,'');
		gridObj.enableTooltips("false,true,true,true,true,true");
		gridObj.attachHeader("#rspan,#select_filter,#select_filter,#select_filter,#select_filter");
		gridObj.enablePaging(true,100,5,"pagingArea",true);
		gridObj.setPagingSkin("bricks");
		gridObj.customGroupFormat=function(name,count){
            return "<font size=1>"+name+" ( " +count+ " ) </font>";
        }
		gridObj.setColumnHidden(2,true);
		       
	}	
}
