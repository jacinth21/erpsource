
function fnOnPageLoad() {
	if(gridObjData!="")
	{
			var frm = document.frmUserList;
		   	mygrid = initGrid('dataGridDiv',gridObjData,'','');
		   	var headerStr = '';
		   	var editFl = frm.editUserAccess.value;
		   	var switchFl = frm.switchUserAccess.value;
		   	if(editFl =='Y'){
		   		headerStr = '#rspan,';
		   	}
		   	if(switchFl =='Y'){
		   		headerStr = headerStr +'#rspan,';
		   	}
		   	headerStr += '#text_filter,#text_filter,#text_filter,#select_filter_strict,#text_filter,#select_filter_strict,#select_filter_strict,#select_filter_strict,#select_filter_strict,#select_filter_strict,#select_filter_strict,#select_filter_strict,#select_filter_strict';
		   	mygrid.attachHeader(headerStr);
		   	mygrid.setSizes();
	}
}
function fnChangeUserTitle(){
	var frm = document.frmUserList;
	var strUserTypeIdx = frm.userType.selectedIndex;
	var strUserTypeVal = frm.userType.options[strUserTypeIdx].value;
	if (strUserTypeVal == '300'){
		frm.title.value = 0;
		frm.title.disabled = true;
	}else{
		frm.title.disabled = false;
	}
}
function fnLoad(){
	document.getElementById("strOpt").value='Reload';
	fnStartProgress();
	document.frmUserList.submit();
}

function fnEdit(userid){
	document.getElementById("strOpt").value = 'USERPROFILE';
	document.forms[0].action="/gmUserListAction.do?huserid="+userid;
	document.frmUserList.submit();
}

function fnSubmit(){
	var frm = document.frmUserList;
	
	var strUserTypeIdx = frm.userType.selectedIndex;
	var strUserTypeVal = frm.userType.options[strUserTypeIdx].value;
	
	var strUserDeptIdx = frm.departmentid.selectedIndex;
	var strUserDeptVal = frm.departmentid.options[strUserDeptIdx].value;
	
	var strUserTitleIdx = frm.title.selectedIndex;
	var strUserTitleVal = frm.title.options[strUserTitleIdx].value;

	if (strUserTypeVal!= '300' && strUserDeptVal!='2005' && strUserDeptVal!=0){
		Error_Details("Department selected should be <b>Sales</b>");
	}
	fnValidateDropDn('securityGrp',' Security Group');	
	var strAcclvl = TRIM(frm.accesslevel.value);
	fnValidateTxtFld('accesslevel',' AccessLevel');
	if(strAcclvl.length > 0){
		fnValidateNumber('accesslevel',' AccessLevel');
	}
	//MNTTASK-5884 - Add validation for 7115:Associate Spine Specialist 
	if (strUserTitleVal == '70115' && strAcclvl != 7 && strAcclvl != ''){ 
		Error_Details("For <b>Associate Spine Specialist</b> access level needs to be <b>7</b>");
	}
	fnValidateTxtFld('emailid',' Email ID');
	fnValidateDropDn('userType',' User Type');
	fnValidatekEmail(frm.emailid.value);
	var userStatus =frm.userStatus.value;
	if(userStatus!='New'){
	fnValidateTxtFld('txt_LogReason','Comments');
	}
	fnValidateCheckForm();
	if (ErrorCount > 0){
			Error_Show();
			Error_Clear();
			return false;
	}else{
		if(frm.hpassword.value==''){
			frm.hpassword.value='Globus!234';
		}
		frm.emailid.disabled = false;
		document.getElementById("strOpt").value= 'save';
		fnStartProgress('Y');
		frm.submit();
	}
}

function fnValidateCheckForm(){
	fnValidateDropDn('departmentid',' Department ID ');
}

function fnReset(){
	var frm = document.frmUserList;
	frm.userLoginName.value = "";
	frm.shortName.value = "";
	frm.emailid.value = "";
	frm.departmentid.value = 0;
	frm.accesslevel.value = "";
	frm.workPhone.value = "";
	frm.title.value = "";
	frm.externalAccFl.checked = false;
	frm.fname.value = "";
	frm.lname.value = "";
	frm.userLoginName.value = "";
	frm.userType.value = 0;
	frm.securityGrp.value=0;
	document.getElementById("divFName").innerHTML = '';	
	document.getElementById("divLName").innerHTML = '';	
	document.getElementById("divStatus").innerHTML = '';
	if(document.getElementById("Btn_Inact")){
		document.getElementById("Btn_Inact").disabled = true;
	}
	if(document.getElementById("Btn_Rollbck")){
		document.getElementById("Btn_Rollbck").disabled = true;
	}
	if(document.getElementById("Btn_Submit")){
		document.getElementById("Btn_Submit").disabled = true;
	}
	frm.strOpt.value = "New";
	frm.action = "\gmUserListAction.do";
	frm.submit();
}

function fnExportExcel(){
	var frm = document.frmUserList;
	var editFl = frm.editUserAccess.value;
   	var switchFl = frm.switchUserAccess.value;
   	var edit_rowId = mygrid.getColIndexById("editIcon");
   	var switch_rowId = mygrid.getColIndexById("switchIcon");
   	// hidden the row (Edit and Switch access)
   	if(editFl =='Y'){
   		mygrid.setColumnHidden(edit_rowId,true);
   	}
	if(switchFl =='Y'){
		mygrid.setColumnHidden(switch_rowId,true);
	}
	mygrid.toExcel('/phpapp/excel/generate.php');
	// show the hidden filed.
	if(editFl =='Y'){
   		mygrid.setColumnHidden(edit_rowId,false);
   	}
	if(switchFl =='Y'){
		mygrid.setColumnHidden(switch_rowId,false);
	}
}

function fnValidatekEmail(inputvalue){	
	var pattern=/^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/;
    if(pattern.test(inputvalue)== false){         
    	Error_Details("Please enter valid <b> Email ID </b>");
    }
}

function fnGo(){

	var frm = document.frmUserList;
	var strUserLoginName = frm.userLoginName.value;
	fnValidateTxtFld('userLoginName',' User Login Name ');
	
	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}else{
		frm.huserid.value='';
		frm.strOpt.value = 'edit';
		frm.action="/gmUserListAction.do?userloginname="+strUserLoginName;
		fnStartProgress('Y');
		frm.submit();
	}
}

function fnRollBack(){
	var userid = document.frmUserList.huserid.value;
	var userName = document.frmUserList.userLoginName.value;
	document.frmUserList.action="/GmCommonCancelServlet?hTxnName="+userName+"&hCancelType=USRRLB&hTxnId="+userid;
	document.frmUserList.submit();
}
function fnInActive(){
	var userid = document.frmUserList.huserid.value;
	var userName = document.frmUserList.userLoginName.value;
	document.frmUserList.action="/GmCommonCancelServlet?hTxnName="+userName+"&hCancelType=USRTRM&hTxnId="+userid;
	document.frmUserList.submit();
}
function fnUserTitle(){
	var strUserTitleIdx = document.frmUserList.title.selectedIndex;
	var strUserTitleVal = document.frmUserList.title.options[strUserTitleIdx].value;
	if (strUserTitleVal == '70119'){ //70119 is Area Director
		document.frmUserList.accesslevel.value='3';
	}
	if (strUserTitleVal == '70116'){ // 70116 is Spine Specialist
		document.frmUserList.accesslevel.value='2';
	}
	if (strUserTitleVal == '70115'){ //70115 is Associate Spine Specialist
		document.frmUserList.accesslevel.value='7';
	}
}

function fnchkUsertype(){

	var strUserTypeIdx = document.frmUserList.userType.selectedIndex;
	var strUserTypeVal = document.frmUserList.userType.options[strUserTypeIdx].value;
	
	var strUserDeptIdx = document.frmUserList.departmentid.selectedIndex;
	var strUserDeptVal = document.frmUserList.departmentid.options[strUserDeptIdx].value;
	if (strUserTypeVal == '303'){
		document.frmUserList.accesslevel.value='3';
	}
	if (strUserTypeVal == '300' || !strUserDeptVal=='2005'){
		document.frmUserList.title.value = 0;
		document.frmUserList.title.disabled = true;
	}else{
		document.frmUserList.title.disabled = false;
	}
}
function fnLoginEnter(e){
	var key;
	key = e.keyCode;
	if(key == 13){
		fnGo();
	}
}
function fnSwitchUser(strUserNm) 
{	
    OpenUserInNewSession(strUserNm);
}

function OpenUserInNewSession(strUsernm) {
	var strUrl= "http:\\\\" + document.domain + "\\GmLogonServlet?strOpt=SwitchUser&hAction=Login&Txt_Username=" + strUsernm ;
	window.open(strUrl, "_blank");
	//var WshShell = new ActiveXObject("WScript.Shell");
    //WshShell.Run("iexplore.exe -nomerge " + strUrl);
  } 