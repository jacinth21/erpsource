//var frm=document.frmAccessControlForm;
function fnCallAjax(obj) 
{	
	var frm=document.frmAccessControlForm;
	if(frm.groupId.value=='0'){
		fnReset();
		return false;
	}
	frm.strOpt.value="editload";
	frm.submit();
}

function fnSubmit()
{
	var frm=document.frmAccessControlForm;	
	fnValidate();

	fnValidateLength();

	var groupDes = TRIM(frm.groupDesc.value);
	if(groupDes.match(/\n/) != null){
		Error_Details("Enter key is not allowed in the Group description field.");
	}
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	//frm.groupId.disabled=false;
	if(frm.groupId.value == 0){
		frm.strOpt.value = 'save';
	}else{
		frm.strOpt.value = 'edit';
	}
	frm.groupDesc.value = groupDes;
	fnStartProgress('Y');
	frm.submit();
}
function fndisable(){
	var frm=document.frmAccessControlForm;
	var str=frm.strOpt.value;
	if(str=='edit'){
		//frm.groupId.disabled=true;
	}
}

function fnReset()
{
	var frm=document.frmAccessControlForm;
	var groupType = frm.groupType.value;
	var strOpt = "";
	if(groupType== '92264')
		strOpt = "SCGRP";
	else
		strOpt = "MDGRP";
	frm.groupId.value ="";
	frm.groupId.disabled=false;
	frm.groupName.value ="";
	frm.groupDesc.value ="";
	frm.groupId.value='0';
	frm.strOpt.value=strOpt;
	
	frm.action = "\gmAccessControlGroup.do?strOpt=" + strOpt + "&haction=" + strOpt + "&randomId=" + Math.random();
	frm.submit();
}

function fnValidateLength(){
	var frm=document.frmAccessControlForm;
	//var groupId=frm.groupId.value;
	var groupName=frm.groupName.value;
	var groupDesc=frm.groupDesc.value;


	/*if(groupId.length>20){
		Error_Details(message[5000]);
	}*/
	if(groupName.length>100){
		Error_Details("Please enter less than 100 characters in <b>Group Name</b> field.");
	}
	if(groupDesc.length>225){
		Error_Details("Please enter less than 255 characters in <b>Group Description</b> field.");
	}
}

function fnValidate()
{
	
	//fnValidateTxtFld('groupId',' Group ID '); 
	fnValidateTxtFld('groupName',' Group Name ');
	fnValidateTxtFld('groupDesc',' Group Description ');
	//fnValidateDropDn('groupType','Group Type');
}
function fnOnPageLoad()
{	  
	if (objGridData != '')
	{
		gridObj = initGrid('grpData',objGridData);
		gridObj.enableTooltips("false,true,true,true,true");
		if(document.frmAccessControlForm.groupType.value=='92264')
			gridObj.attachHeader("#rspan,#rspan,#rspan,#rspan,#rspan,#select_filter,#text_filter");
		else
			gridObj.attachHeader("#rspan,#rspan,#rspan,#select_filter,#text_filter");
	}
}

function fnEditGrp(id,type){
	var frm=document.frmAccessControlForm;
	frm.groupId.value =id;
	frm.groupType.value =type;
	frm.strOpt.value = 'editload';
	frm.action = "/gmAccessControlGroup.do";
	frm.submit();

}
function fnVoidGroup(){
	var frm=document.frmAccessControlForm;
	var group_id=frm.groupId.value;
	frm.action = "/GmCommonCancelServlet?hTxnName="+group_id+"&hCancelType=VGROUP&hTxnId="+group_id;
	frm.submit();
		
}
function fnEditDetails(id,type)
{
	if(type == "92264"){
		strType = "ReloadReport";
	}else if(type == "92265"){
		strType ="SMREPORT";
	}
	windowOpener("/gmUserGroup.do?haction=hide&strOpt="+strType+"&groupName="+id,"Details","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}
function fnlookup(grpID,rowid,groupnm)
{
	if(	grpID==null || grpID==undefined)
	{
		grpID='';
	}
	//document.frmUserGroupMap.rowId.value=rowid;
	windowOpener("/gmUserNavigation.do?strOpt=SEQLOOKUP&userGroup="+grpID+"&userGroupName="+groupnm,"Lookup","resizable=yes,scrollbars=yes,top=150,left=150,width=350,height=600");
	
}
function fnModSecGrpMap(id,type){
	strType ="MSREPORT";
	windowOpener("/gmUserGroup.do?haction=hide&strOpt="+strType+"&groupName="+id,"Details","resizable=yes,scrollbars=yes,top=150,left=200,width=750,height=600");
}

function fnTaskLookup(id,type){
		windowOpener("/gmUserGroupMap.do?method=fchFunctDetails&strOpt=SGMAP&grpId="+id + "&grpType=" + type,"UserGroupMap","resizable=yes,scrollbars=yes,top=150,left=150,width=970,height=600");
}

function fnGrpSecMap(id,type){
	windowOpener("/gmUserGroupMap.do?method=loadFunList&strOpt=SESG&haction=SECGRP_SECEVN&grpId="+id + "&grpType=" + type,"UserGroupMap","resizable=yes,scrollbars=yes,top=150,left=150,width=1020,height=600");
}
