//load button in user profile screen
function fnGo(){

	var frm = document.frmUserList;
	var strUserLoginName = frm.userLoginName.value;
	var ldapIdVal = frm.ldapID.value;
	//alert('user name'+strUserLoginName);
	fnValidateTxtFld('userLoginName',' User Login Name ');
	fnValidateDropDn('ldapID',' LDAP');	
	
	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}else{

		frm.strOpt.value = 'USERPROFILE';
		frm.action="/gmUserListAction.do?huserLoginName="+strUserLoginName+"&ldapID="+ldapIdVal;
		fnStartProgress('Y');
		frm.submit();
	}
}


//this function is used to get the elements in left side and Right side div.
function fnOnPageLoadPlant(){

	// When page load all the product development team employees will be displayed in the Assigned To drop down.  
	fnAddAssignedName();
	// When submit and it will be loaded assigned names which we have submitted.
	fnAddAssignedToName();
}


function fnAddAssignedToName(){
	var combo = document.getElementById("AssignedToCombo");
	if(FormAssignedToLen > 0)
		for(i=0 ;i<FormAssignedToLen;i++){
			FormVldArr = eval("FormAssignedTo"+i);
			FormVldArrObj = FormVldArr.split("|");
			addOption(combo,FormVldArrObj[0],FormVldArrObj[1]);
 		}
}

function fnAddAssignedName(){
	var combo = document.getElementById("AssingedNameCombo");
	
	if(FormVldArrLen > 0)
		for(i=0 ;i<FormVldArrLen;i++){			
			FormVldArr = eval("FormVldArray"+i);			
			FormVldArrObj = FormVldArr.split("|");
			addOption(combo,FormVldArrObj[0],FormVldArrObj[1]);
 		}
}

//this function is used to move the values from left to right
function fnAssignedToNames() {
	var values = [];
	var tmpString = "";
	var combo = document.getElementById("AssignedToCombo");
	var items = document.getElementById("AssingedNameCombo");
	var availLen = items.options.length;
	var removeStr='';
	for (var i = 0; i < availLen; i++) {
		if (items.options[i].selected) {			
			addOption(combo,items.options[i].value,items.options[i].text,"AssignedToCombo");	
			//items.remove(i);
		}		
	}
	fnRemoveSelected(items);
	  
} 

	//this function is used to move the values from right to left
	function fnRemoveNames(){
        var tmpString="";
		var values = [];
		var combo = document.getElementById("AssignedToCombo");
		var items = document.getElementById("AssingedNameCombo");
		var availLen = combo.options.length;
		for (var i = 0; i < availLen; i++) {
			if (combo.options[i].selected) {
				addOption(items,combo.options[i].value,combo.options[i].text,"AssingedNameCombo");
                //combo.remove(i);
			}
		}
		fnRemoveSelected(combo);
	}
	
function fnRemoveSelected(elSelect){
	var i;
	var availLen = elSelect.length;
	for (i = availLen - 1; i>=0; i--) {
		if (elSelect.options[i].selected) {
			elSelect.remove(i);
		}
	}
}

//This function is used to move the selected option
function addOption(selectbox, value, text,strComboNm){
	var optn = document.createElement("OPTION");
	optn.text = text;
	optn.value = value;
	//alert('In add option...'+optn.text+'...'+optn.value);
	selectbox.options.add(optn);	
}
function sortSelect(selElem) {
    var tmpAry = new Array();
    for (var i=0;i<selElem.options.length;i++) {
        tmpAry[i] = new Array();
        tmpAry[i][0] = selElem.options[i].text;
        tmpAry[i][1] = selElem.options[i].value;
    }
   
    tmpAry.sort();
    while (selElem.options.length > 0) {
        selElem.options[0] = null;
    }
   
    for (var i=0;i<tmpAry.length;i++) {
           var op = new Option(tmpAry[i][0]);
           selElem.options[i] = op;
       }
   
    return;
}
//This function is for submit button
function fnSubmit(){
	var frm = document.frmUserList;
	
	var strUserTypeIdx = frm.userType.selectedIndex;
	var strUserTypeVal = frm.userType.options[strUserTypeIdx].value;
	
	var strUserDeptIdx = frm.departmentid.selectedIndex;
	var strUserDeptVal = frm.departmentid.options[strUserDeptIdx].value;
	var strUserLoginName = frm.userLoginName.value;
	var strUserTitleIdx = frm.title.selectedIndex;
	var strUserTitleVal = frm.title.options[strUserTitleIdx].value;
	var companyId = frm.companyId.value;
	var ldapObj = parent.document.all.ldapID;
	fnValidateDropDn('companyId',' Company ');
	//To prevent the validation “Department selected should be Sales” for the department Id “Vendor" - PC-387
	if (strUserTypeVal!= '300' && strUserDeptVal!='2005' && strUserDeptVal!=0 && strUserDeptVal != 109301){
		Error_Details("Department selected should be <b>Sales</b>");
	}
	fnValidateDropDn('securityGrp',' Security Group');	
	var strAcclvl = TRIM(frm.accesslevel.value);
	fnValidateTxtFld('accesslevel',' AccessLevel');
	if(strAcclvl.length > 0){
		fnValidateNumber('accesslevel',' AccessLevel');
	}
	//MNTTASK-5884 - Add validation for 7115:Associate Spine Specialist 
	if (strUserTitleVal == '70115' && strAcclvl != 7 && strAcclvl != ''){ 
		Error_Details("For <b>Associate Spine Specialist</b> access level needs to be <b>7</b>");
	}
	fnValidateTxtFld('emailid',' Email ID');
	fnValidateDropDn('userType',' User Type');
	fnValidatekEmail(frm.emailid.value);
	var userStatus =frm.userStatus.value;
	fnValidateTxtFld('fname',' First Name');
	fnValidateTxtFld('lname',' Last Name');
	if(userStatus!='New'){
	fnValidateTxtFld('txt_LogReason','Comments');
	}
	fnValidateCheckForm();
	//Validating username 
	if(document.getElementById('DivShowUsrNotAvl').style.display!='none' || TRIM(strUserLoginName)==''){
		Error_Details("Enter a valid <b>User Name</b>");
	}
	
	if (ErrorCount > 0){
			Error_Show();
			Error_Clear();
			return false;
	}else{
		if(frm.hpassword.value==''){
			frm.hpassword.value='Globus!234';
		}
		var hCopyUserName = document.frmUserList.hCopyUserName.value;
		var CopyUserName = document.frmUserList.copyUserName.value;
	
		if (CopyUserName == hCopyUserName){
			document.frmUserList.copyUserName.value = '';
		}
		
		frm.emailid.disabled = false;
		frm.action="/gmUserListAction.do?strOpt=save&ldapID="+ldapObj.value;
		fnStartProgress('Y');
		frm.submit();
	}
}
function fnAssignedToNamesToDB() {
	var values = '';
	var combo = document.getElementById("AssignedToCombo");
	//var items = document.getElementById("AssingedNameCombo");
	for (var i = 0; i < combo.options.length; i++) {
		values = values + combo.options[i].value+",";
	}	
	return values;
} 
//This function is used to submit associate companys in the company mapping
function fnSubmitAss(){
	fnValidateTxtFld('txt_LogReason','Comments');
	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}
	var frm = document.frmUserList;
	var huserNm = parent.document.all.userLoginName.value;
	frm.strAssCompany.value = fnAssignedToNamesToDB();
	frm.strOpt.value="save";
	frm.action="/gmPartyCompanyMapAction.do?strOpt=save";
	fnStartProgress('Y');
	frm.submit();
}
function fnValidateCheckForm(){
	fnValidateDropDn('departmentid',' Department ID ');
}
//Thsi function is used to reset  in use profile screen
function fnReset(){
	var frm = document.frmUserList;
	//frm.userLoginName.value = "";
	frm.shortName.value = "";
	frm.emailid.value = "";
	frm.departmentid.value = 0;
	frm.accesslevel.value = "";
	frm.workPhone.value = "";
	frm.title.value = "";
	frm.externalAccFl.checked = false;
	frm.fname.value = "";
	frm.lname.value = "";
	frm.companyId.value = "";
	parent.document.all.userLoginName.value="";
	//frm.userLoginName.value = "";
	frm.userType.value = 0;
	frm.securityGrp.value=0;
	document.getElementById("divFName").innerHTML = '';	
	document.getElementById("divLName").innerHTML = '';	
	document.getElementById("divStatus").innerHTML = '';
	if(document.getElementById("Btn_Inact")){
		document.getElementById("Btn_Inact").disabled = true;
	}
	if(document.getElementById("Btn_Rollbck")){
		document.getElementById("Btn_Rollbck").disabled = true;
	}
	if(document.getElementById("Btn_Submit")){
		document.getElementById("Btn_Submit").disabled = true;
	}
	frm.strOpt.value = "New";
	frm.action = "\gmUserListAction.do";
	frm.submit();
}
function fnValidatekEmail(inputvalue){	
	var pattern=/^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/;
    if(pattern.test(inputvalue)== false){         
    	Error_Details("Please enter valid <b> Email ID </b>");
    }
}
function fnRollBack(){
	document.frmUserList.action="/gmUserListAction.do?strOpt=ROLLBACK";
	document.frmUserList.submit();
}
function fnInActive(){
	var userid = document.frmUserList.huserid.value;
	var userName = parent.document.all.userLoginName.value;
	document.frmUserList.action="/GmCommonCancelServlet?hTxnName="+userName+"&hCancelType=USRTRM&hTxnId="+userid;
	document.frmUserList.submit();
}

//This function used to press the enter key then submit the page.
function fnEnter() {
	if (event.keyCode == 13) {
		fnGo();
	}
}
//This function used to Get the access key while changing the title.
function fnUserTitle(){
	var strUserTitleIdx = document.frmUserList.title.selectedIndex;
	var strUserTitleVal = document.frmUserList.title.options[strUserTitleIdx].value;
	if (strUserTitleVal == '70119'){ //70119 is Area Director
		document.frmUserList.accesslevel.value='3';
	}
	if (strUserTitleVal == '70116'){ // 70116 is Spine Specialist
		document.frmUserList.accesslevel.value='2';
	}
	if (strUserTitleVal == '70115'){ //70115 is Associate Spine Specialist
		document.frmUserList.accesslevel.value='7';
	}
}
function fnchkUsertype(){

	var strUserTypeIdx = document.frmUserList.userType.selectedIndex;
	var strUserTypeVal = document.frmUserList.userType.options[strUserTypeIdx].value;
	
	var strUserDeptIdx = document.frmUserList.departmentid.selectedIndex;
	var strUserDeptVal = document.frmUserList.departmentid.options[strUserDeptIdx].value;
	if (strUserTypeVal == '303'){
		document.frmUserList.accesslevel.value='3';
	}
	if (strUserTypeVal == '300' || !strUserDeptVal=='2005'){
		document.frmUserList.title.value = 0;
		document.frmUserList.title.disabled = true;
	}else{
		document.frmUserList.title.disabled = false;
	}
}
//This function used to hidden the Authentication drop down filed.
function fnUserSetupPageLoad(){
	var userName = document.frmUserList.huserLoginName.value;
	var ldapObj = parent.document.all.ldapID;
	if(userName !=''){
		ldapObj.disabled = true;
	}
}

// function to validate username already exists
function fnValidateUserName(username){
	var hUserName= document.frmUserList.huserLoginName.value;
	username = TRIM(username); 
	if(hUserName != username && username!=''){
		 var tempDate = new Date();
		 ajaxUrl = fnAjaxURLAppendCmpInfo("/gmUserListAction.do?strOpt=VALIDATEUSERLOGIN&userLoginName="+username+"&encode="+tempDate.getTime());
		 dhtmlxAjax.get(ajaxUrl,fnCallbackValidateUser);
	}else{
		document.getElementById('DivShowUsrNotAvl').style.display='none';
 	 	document.getElementById('DivShowUsrAvl').style.display='none';
	}
}

//function to get ajax response
function fnCallbackValidateUser(loader){
		var response = loader.xmlDoc.responseText;
	   if(response == '0'){
	 	document.getElementById('DivShowUsrAvl').style.display='inline-block';
	 	document.getElementById('DivShowUsrNotAvl').style.display='none';
	 	}else{
	 	document.getElementById('DivShowUsrNotAvl').style.display='inline-block';
	 	document.getElementById('DivShowUsrAvl').style.display='none';
	 	}
}

// function to hide validate img while click on username textbox
function fnUserToggle(){
	document.getElementById('DivShowUsrNotAvl').style.display='none';
	 	document.getElementById('DivShowUsrAvl').style.display='none';
}

/*   PMT-51134--Copy Security Group access
 *   1) This function used to show the correct user information.
	 2) Get the count - from response string
	 3) Based on count to decide - user
	 4) If invalid - throw an error message and reset the field to empty
	    otherwise - show green color tick mark
 **/
function fnValidateCopyAccessUser(copyUsername){
	var userName= document.frmUserList.huserLoginName.value;
	var hCopyUserName = document.frmUserList.hCopyUserName.value;
	var copyUsername = TRIM(copyUsername).toLowerCase();
	
	if(userName != copyUsername && copyUsername!='' && copyUsername!=hCopyUserName){
		var tempDate = new Date();
		 ajaxUrl = fnAjaxURLAppendCmpInfo("/gmUserListAction.do?strOpt=VALIDATEUSERLOGIN&userLoginName="+copyUsername+"&encode="+tempDate.getTime());
		 dhtmlxAjax.get(ajaxUrl,fnCallbackValidateCopyAccessCallback);
	}else{
		document.getElementById('DivCopyUsrNotAvl').style.display='none';
	 	document.getElementById('DivCopyShowUsrAvl').style.display='none';
	}
	}

//Callback
function fnCallbackValidateCopyAccessCallback(loader){
	var response = loader.xmlDoc.responseText;
	
    if(response == '1'){
 	document.getElementById('DivCopyShowUsrAvl').style.display='inline-block';
 	document.getElementById('DivCopyUsrNotAvl').style.display='none';
 	}else{
 	document.getElementById('DivCopyUsrNotAvl').style.display='inline-block';
 	document.getElementById('DivCopyShowUsrAvl').style.display='none';
 	}
}