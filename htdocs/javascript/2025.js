// JS File For 2025 Department ( International Customer Service)
var TREE_ITEMS = [
	['Setup', null,
		['Group-Part Growth', 'gmGrowthSetup.do\',\''],
	],		
	['Reports', null,
		['Monthly Sheet Summary', 'GmTTPMonthlySheetServlet\',\''],
		['Demand Sheet Summary', 'gmDSSummary.do\',\''],	
		['Part Number Search', 'GmPartSearchServlet\',\''],
		['Set List Report', 'GmSetReportServlet\',\'SETRPT'],
	],
	['Documents', null,
		['Forms', 'GmSalesForms.html'],
		['Product Info', 'GmProdInfo.html'],
		['Product Inserts', 'GmProdInsertsOUS.html'],
		['Product Demo', 'GmProdDemoOUS.html']
	],

];