function fnCallGo(){	
		if(event.keyCode == 13)
		 {	 
			 refsrc =  document.frmShipDetails.refId.value				
			 arrRefsrc = refsrc.split("$");
			 document.frmShipDetails.refId.value =arrRefsrc[0] ;
			 document.all.source.value = arrRefsrc[1];
			if (document.all.source.value != 0 ){
				fnGo();
			}
		}
}

function fnGo(){
	fnValidateTxtFld('refId',message[5617])
	fnValidateDropDn('source',message[5618]);

	 if (ErrorCount > 0)
		{
			Error_Show();
			 Error_Clear();
			return false;
		}
	//if(document.frmShipDetails.refId != '' && document.frmShipDetails.source != ''){ // also chk for source	
	document.all.strOpt.value="modify";
	document.all.shipId.value="";
	document.frmShipDetails.Btn_Go.disabled = true;
	fnStartProgress();
		document.frmShipDetails.submit();
	
}

function fnSubmit(){

var status = document.all.status.value;

if (document.all.status.value == ""){
	Error_Details(message[33]);
	if (ErrorCount > 0)
		{
			Error_Show();
			 Error_Clear();
			return false;
		}
}

	fnValidateTxtFld('refId',message[5619])
	fnValidateDropDn('source',message[5618]);
	fnValidateDropDn('shipTo',message[5620]);
	fnValidateDropDn('names',message[5564]);
	fnValidateDropDn('shipCarrier',message[5621]);
	fnValidateDropDn('shipMode',message[5622]);
//	fnValidateTxtFld('trackingNo','Tracking Number')
//	fnValidateTxtFld('freightAmt','Freight Amt')
	//NumberValidation(document.all.freightAmt.value, 'Enter valid value for <B>FreightAmt</B>', 1);
/*
if (document.all.source.value == "50180")
{
	fnValidateTxtFld('freightAmt','Freight Amt')
}*/

if (document.all.hrefId.value != document.all.refId.value) 
{
	Error_Details(message[34]);
	
}
	var shipto = document.all.shipTo.value;
	var hshipTo = document.all.hshipTo.value;
	var nameId = document.all.names.value;
	var hnameid = document.all.hnames.value;
	var shipToAdddId = document.all.addressid.value;
	var haddressid = document.all.haddressid.value;
	var shipCarrierId = document.all.shipCarrier.value;
	var hshipCarrierId = document.all.hshipCarrier.value;
	var shipModeId = document.all.shipMode.value;
	var hshipModeId = document.all.hshipMode.value;
	//var freightAmtVal = document.all.freightAmt.value;
	//var hfreightAmtVal = document.all.hfreightAmt.value;
	var overrideAttnToVal = document.all.overrideAttnTo.value;
	var hoverrideAttnToVal = document.all.hoverrideAttnTo.value;
	var trackingno= document.all.trackingNo.value;
	var ruleStatus = 'PROCESS';

	
	//PMT#40555-tissue-part-shipping-validation
	if(tissueflag == 'Y')
		{
	var shiptoidid = document.all.addressid.value;
	if (shipto != '4122') { // not hospital
		if(shipto == '4121' && shiptoidid != null){// sales rep address is hospital
			var address =	document.all.addressid.options[document.all.addressid.selectedIndex].text;
			var addressarr = address.split('-');
			if (addressarr.length > 0 && addressarr[0] != 'Hospital') {
				Error_Details(message[10615]);
			}
		} else {
			Error_Details(message[10615]);
		}
		}
	}
	
	
	if (shipto == '4121' && shipToAdddId == '')
	{
		Error_Details(message[32]);
	}
	
	

	
	if (document.frmShipDetails.updAction)
	{
		if (document.frmShipDetails.updAction.value == 0)
		{	if (!validateHiddenField('shipTo')) 
			{
				if (!validateHiddenField('names'))
				{
					if (!validateHiddenField('addressid'))
					{
						if (!validateHiddenField('shipCarrier'))
						{
							if (!validateHiddenField('shipMode'))
							{
								if (!validateHiddenField('trackingNo'))
								{
								}
							}
						}
					}
				}
			}			
		}
	}
	if(strPSFGHoldFl == 'Y' && trackingno != '' ){
		 Error_Details(Error_Details_Trans(message[5317],asscTxnID));
	}
	if (status == '33' || status == '36')	// 33-Packing In Progress, 36-Ready For Pickup
	{
		if (shipto != hshipTo){
			Error_Details(message[5093]);
		}
		if(nameId != hnameid){
			Error_Details(message[5094]);
		}
		if(shipToAdddId != haddressid && shipToAdddId != ''){
			Error_Details(message[5095]);
		}
		if(overrideAttnToVal != hoverrideAttnToVal){
			Error_Details(message[5096]);
		}
		if(shipCarrierId != hshipCarrierId){
			Error_Details(message[5097]);
		}
		if(shipModeId != hshipModeId){
			Error_Details(message[5098]);
		}
		/*
		if(freightAmtVal != hfreightAmtVal){
					Error_Details("You cannot change field<b> Freight Amt </b> at this time.");
				}*/
		
	}


    if (ErrorCount > 0)
		{
			Error_Show();
			 Error_Clear();
			return false;
		}
		// to enable the fileds just b4 submit
		enabledisableFields(false);	
		fnStartProgress('Y');
		if(document.frmShipDetails.custPO != undefined)
			document.frmShipDetails.custPO.disabled = false;
		document.all.strOpt.value="ShipOut";
		document.frmShipDetails.action = "/gmRuleEngine.do";
		var addrId = document.all.addressid.value;
		dhtmlxAjax.post('/GmCartAjaxServlet?hAction=validateRepAdd&hAddrId='+addrId+'&ramdomId='+Math.random(),'',fnValidateAddress);
		//adding changes for PMT-3011 Address Change Issue
		if(trackingno != undefined && trackingno != ''){
			ruleStatus = 'VERIFY';
		}
		document.frmShipDetails.txnStatus.value = ruleStatus;
		//document.frmShipDetails.Btn_Submit.disabled=true;
		//document.frmShipDetails.submit();
}

function validateHiddenField(objname){
	var hiddenval = eval('document.frmShipDetails.h'+objname+'.value');
	var currentval = eval('document.frmShipDetails.'+objname+'.value');	
		if (hiddenval == '' || currentval == '')
		{
			hiddenval = currentval = 0;
		}
		if (hiddenval != currentval)
		{
			Error_Details(message[5623]);
			return true;
		}
}

function fnPicSlip(){
	var source = document.all.source.value;
	var addressId = document.all.addressid.value;
	fnValidateTxtFld('refId',message[5617])
	fnValidateDropDn('source',message[5618]);
	 if (ErrorCount > 0)
		{
			Error_Show();
			 Error_Clear();
			return false;
		}
	
	val = document.all.refId.value;
     
		if (document.all.source.value == '50183'){//Open window for Loaner Extn
           windowOpener("/GmConsignInHouseServlet?hAction=PicSlip&hConsignId="+val+"&ruleSource="+source+"&addressid="+addressId,"SetPic","resizable=yes,scrollbars=yes,top=150,left=150,width=750,height=500");
        }
		if (document.all.source.value == '50182'){//Open window for Loaner 
         windowOpener("/GmInHouseSetServlet?hAction=ViewPrint&hConsignId="+val+"&ruleSource="+source+"&addressid="+addressId+"&hType="+4127+"&loanerReqId="+loanerReqID,"LoanPrint","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
        }
		if (document.all.source.value == '50180'){//Open window for order 
           windowOpener('/GmOrderItemServlet?hAction=PICSlip&hId='+val,"Pack","resizable=yes,scrollbars=yes, top=150,left=150,width=760,height=610");
        }
		if((document.all.source.value  == '50181')||(document.all.source.value  == '50186')||(document.all.source.value == '4000518') || (document.all.source.value == '26240435')){//Open window for Consignments , In-House Loaners or Returns
		    windowOpener("/GmConsignItemServlet?hAction=PicSlip&hConsignId="+val+"&ruleSource="+source+"&addressid="+addressId,"SetPic","resizable=yes,scrollbars=yes,top=150,left=150,width=760,height=600");            
        }
}


function fnRollback()
{
	fnValidateTxtFld('refId',message[5617])
	fnValidateDropDn('source',message[5618]);
		
	var refid = '';
	var cancelType = '';
	var status = document.frmShipDetails.status.value;
	var loanerCheck = document.frmShipDetails.chk_loaner;
	var txnsource = document.frmShipDetails.source.value;
		
	refid = document.frmShipDetails.refId.value;
		
	if(loanerCheck != null && loanerCheck.checked){
		Error_Details(message[5099]);
	}
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	refid = (status == '33' || status == '36')? refid + '|': refid;
	
	cancelType = fnGetRBDtsl(status,txnsource);
	
	if(cancelType == "disabled"){
		return;
	}
	document.frmShipDetails.hCancelType.value = cancelType;
	if(status == '36'){
		fnRollbackShipping();
		return;
	}
	
	document.frmShipDetails.refId.value = refid;
	document.frmShipDetails.hTxnId.value = refid;
	document.frmShipDetails.hTxnName.value = refid;
	document.frmShipDetails.action ="/GmCommonCancelServlet";
	document.frmShipDetails.hAction.value = "Load";             
	document.frmShipDetails.Btn_Rollback.disabled=true;
	document.frmShipDetails.submit();
}

function fnGetRBDtsl(status,txnsource){
	var cancelType;
	var disableCnt = 0;
	switch (txnsource){
		case "50180" :
			cancelType = (status == '33' || status == '36') 
								? 'RBORDN' 
								: (status == '30') 
										? 'RBTXN' 
										:  disableCnt++;
			break;
		case "50181" :
			cancelType = (status == '33' || status == '36') 
								? 'RBCN' 
								: (status == '30') 
										? 'RBTXN' 
										:  disableCnt++;
		   break;
		case "50182" :
			cancelType = (status == '33' || status == '36') 
								? 'RBLNT' 
								: 'VDLNT';
			break;
		case "50183" :
			cancelType = (status == '33' || status == '36') 
								? 'RBFGLE' 
								: disableCnt++;
		   break;
		case "50186" :
			cancelType = (status == '33' || status == '36') 
								? 'RBLNIT' 
								: 'RBIHIT';
		   break;
		case "26240435" :
			cancelType = (status == '30') 
										? 'RBTXN' 
										:  disableCnt++;
		   break;
		default : disableCnt++;
		
	}
	if(disableCnt > 0){
		document.frmShipDetails.Rollback.disabled = true;
		cancelType = "disabled";
	}
	return cancelType;
}

function fnRollbackShipping ()
{
	var shipStatusVal = '';
	var refid = '';
	var objDivStyle = eval('document.all.rollbackStatus.style');
	objDivStyle.display = 'block';
	document.frmShipDetails.Rollback.disabled = true;
	objShipStauts = document.all.shipStatus;
	objShipStautsLen = objShipStauts.length;
	if (objShipStautsLen > 0)
	{
		for (i=0 ; i<objShipStautsLen ; i++ )
		{
			if (objShipStauts[i].checked == true)
			{
				document.frmShipDetails.Rollback.disabled = false;
				shipStatusVal = objShipStauts[i].value;
				refid = document.frmShipDetails.refId.value;
				document.frmShipDetails.hTxnName.value = refid;
				document.frmShipDetails.hTxnId.value = refid + '|' + shipStatusVal;;
				document.frmShipDetails.action ="/GmCommonCancelServlet";
				document.frmShipDetails.hAction.value = "Load";
				document.frmShipDetails.Btn_Rollback.disabled=true;
				document.frmShipDetails.submit();
			}
		}
	}
}

function fnEnableRollback()
{
	var txnsource = document.frmShipDetails.source.value;
	var status = document.frmShipDetails.status.value;
	var ruleTxns = rbTxns;
	var ruleTxnTypes = ruleTxns.replace(/,/g, '');
	if(status == '30' && ruleTxnTypes.search(strSource) == -1){
		return;
	}
	document.frmShipDetails.Rollback.disabled = false;
}

function disableRollback(){
	var strHoldFl=document.frmShipDetails.hholdFl.value; 
	var status = document.frmShipDetails.status.value;
	var strSource = document.frmShipDetails.source.value;
	var ruleTxns = rbTxns;
	var ruleTxnTypes = ruleTxns.replace(/,/g, '');
	if (strSource == 50183 || strSource == 50180 || strSource == 50181  || strSource == 50182  || strSource == 4000518  || strHoldFl=='Y') // loaners extn & ORDERS & Consign
	{
	  	 if (status != '33' && status != '36' && status != '30'){
	  		 document.frmShipDetails.Rollback.disabled = true;
		 }else if(status == '30' && ruleTxnTypes.search(strSource) == -1){
			 document.frmShipDetails.Rollback.disabled = true;
		 }
	}
	else {
		 document.frmShipDetails.Rollback.disabled = false;
	}
}

function fnStatusDetails(){
	val = document.all.refId.value;
	if (val != '')
	{
		windowOpener('/gmstatuslog.do?strOpt=DETAIL&haction=reload&refID='+val,"StatusDetails","resizable=yes,scrollbars=no, top=150,left=150,width=880,height=610");
	}
}

function fnFormLoad(){
	
	var strOption=document.frmShipDetails.strOpt.value;
	var strHoldFl=document.frmShipDetails.hholdFl.value;
	var strShipMode = document.frmShipDetails.shipMode.value;
	var dtype = document.frmShipDetails.hdtype.value;
	var shipTo = document.frmShipDetails.hshipTo.value;
	var status = document.frmShipDetails.status.value;
	var strSource = document.frmShipDetails.source.value;
	var type = document.frmShipDetails.ctype.value;

	//disable address for plant(26240419)
	if(shipTo == '26240419'){
		document.frmShipDetails.addressid.disabled = true;
	}
	
	if(strSource=="50180"){//50180 - Order
		//check account is on credit hold. if yes show warning msg and hide place order button
		fnValidateCreditHold(accid,["Btn_Submit"],"frmShipDetails");
	}
	
	if(strHoldFl=='Y')
	{		
		document.getElementById('Btn_Submit').disabled = true;
	}
	if (strOption =='modify')
	{
		document.frmShipDetails.trackingNo.focus() ;
	}else{
		document.frmShipDetails.refId.focus() ;
	}
	
	if ((document.frmShipDetails.shipMode.value == 5030 || document.frmShipDetails.shipMode.value == 10304738) && document.frmShipDetails.trackingNo.value=='') //hand delivered
	{
		document.frmShipDetails.trackingNo.value = 'Hand Delivered';
	}
	disableRollback();
	
	if (document.frmShipDetails.tracknos)
	{
		if (document.frmShipDetails.trackingNo.value == document.frmShipDetails.tracknos.value)
		{
			enabledisableFields(true);
		}
		if (document.frmShipDetails.trackingNo.value =='')
		{
			document.frmShipDetails.tracknos.options[document.frmShipDetails.tracknos.selectedIndex].value= 0;
		
		}
	}	
	sourceID  = document.all.source.value;
	if(shipFlg == 'true' && sourceID == 50181 && 
	(dtype != '70103' &&
	 dtype != '70104' &&
	 dtype != '70105' &&
	 dtype != '70106' &&
	 dtype != '70107' &&
	 dtype != '26240143'
			) ){
		fnPrintVersionLetter('autoprint');
	}
	if(shipFlg == 'true' && ((sourceID == 50181 && dtype == '26240143')||(sourceID == 4000518))){
		fnPrintForReturnType('autoprint');
	}
	if(shipFlg == 'true' && sourceID == 50180){
		fnPackSlip('autoprint');
	}
	// When create item initiate with purpose is OUS Sales Replenishment then, should not edit Cust Po field in Shipping screen.
	if(document.frmShipDetails.custPO != undefined)
		if(strPurpose == 'Y') // getting value from DB for OUS sales Replenishment.
			document.frmShipDetails.custPO.disabled = true;
	
	if(sourceID == '50186' && status == '40'){
		document.getElementById('Btn_Submit').disabled = true;
		document.getElementById('Btn_Rollback').disabled=true;
	}
	if(sourceID == '4000518' || (sourceID == '50181' && type == '26240144')){
		document.getElementById('Btn_Disable').disabled = true;
		document.getElementById('Btn_Rollback').disabled=true;
	}
}

function fnPackSlip(printtyp){
	sourceID  = document.all.source.value;
	val = document.all.refId.value;
		if(sourceID == '50180'){//Open window for Orders
			
			if(strPrintCountFl == '3' && printtyp=='manually'){// get the value from the property
				
		        fnPackSlipChallan("ORG_RECIPIENT",printtyp,sourceID,val);
                fnPackSlipChallan("DUP_TRANSPORTER",printtyp,sourceID,val);
                fnPackSlipChallan("TRIP_SUPPLIER",printtyp,sourceID,val);

		   }
		  else{
              windowOpener('/GmEditOrderServlet?hMode=PrintPack&hSource=shipout&hOrdId='+val+"&hOpt="+printtyp,"Pack","resizable=yes,scrollbars=yes,top=150,left=150,width=760,height=610");
		  }
		}
        else if((sourceID == '50181')||(sourceID == '50186')||(sourceID == '4000518')) {
        	//Open window for Consignments-50181 , In-House Loaner Items-50186 or Returns-4000518
        	//Three pop up window for PackSlip
        	if ((strPrintCountFl =='3')&&( sourceID == '50181')){
     			fnPackSlipChallan("ORG_RECIPIENT",printtyp,sourceID,val);
                fnPackSlipChallan("DUP_TRANSPORTER",printtyp,sourceID,val);
                fnPackSlipChallan("TRIP_SUPPLIER",printtyp,sourceID,val);

		     }
        	else{
		    windowOpener("/GmConsignSetServlet?hAction=PackVersion&hId="+val,"Con","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");            
        }   
       }   
        else if(sourceID == '50183'){//Open window for Loaner Extn
            windowOpener("/GmConsignInHouseServlet?hAction=PicSlip&hConsignId="+val,"SetPack","resizable=yes,scrollbars=yes,top=150,left=150,width=750,height=500");
        }else if( sourceID == '26240435') { // Stock Transfer Invoice
        	
        	if(strPrintCountFl == 3){
     		   for(i=1;i<=strPrintCountFl;i++){
     			 var recipientLbl = ((i==1)?"ORG_RECIPIENT":(i==2)?"DUP_TRANSPORTER":"TRIP_SUPPLIER");
     			windowOpener('/gmStockTransferAction.do?method=stockTransferInvoicePrint&fulfillTrnasctionId='+val+'&invPrintLbl='+recipientLbl,recipientLbl,"resizable=yes,scrollbars=yes,top=150,left=200,width=790,height=700");
     		   }
        	}
        }
		
		
		if(sourceID == '50180' && printtyp == 'autoprint'){
			 setTimeout("popupWin.close();",5100); 
		 }
}
// function is used to create PackSlip three times based on the value
function fnPackSlipChallan(ose,printtyp,sourceID,refId){
		if(sourceID == '50181'){//Open window for Consignments			 
			windowOpener("/GmConsignSetServlet?hAction=PackVersion&hId="+refId+"&ose="+ose,ose,"resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
        }
	
		else if(sourceID == '50180'){//Open window for Orders
			
            windowOpener("/GmEditOrderServlet?hMode=PrintPack&hSource=shipout&hOrdId="+refId+"&hOpt="+printtyp+"&ose="+ose,ose,"resizable=yes,scrollbars=yes,top=150,left=150,width=760,height=610");
        } 
}

function fnPrintLetter()
{
	val = document.all.refId.value;
	sourceID  = document.all.source.value;
	if (sourceID = 50182) //loaner
	{
		type=document.frmShipDetails.ctype.value;
	}
	windowOpener("/GmInHouseSetServlet?hAction=ViewLetter&hConsignId="+val+"&hType="+type,"LoanPrint","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnPrintVersion(){
	val = document.all.refId.value;
    windowOpener("/GmInHouseSetServlet?hAction=ViewPrint&hConsignId="+val+"&barCodeFl=YES&hType="+document.frmShipDetails.ctype.value+"&hOpt=manually&status="+status+"&loanerReqId="+loanerReqID,"LoanPrint","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnPrintVersionLetter(printtype){
	val = document.all.refId.value;
	var shipTo=document.all.shipTo.value;
	sourceID  = document.all.source.value;
  if (sourceID == 50181){
	  windowOpener("/GmConsignSetServlet?hAction=PrintVersionLetter&hId="+val+"&hShipTo="+shipTo+"&hopt="+printtype+"&status="+status,"Con","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
  }
  else if (sourceID == 50182)
	{
		windowOpener("/GmInHouseSetServlet?hAction=ViewLetterVer&hConsignId="+val+"&hType="+document.frmShipDetails.ctype.value+"&status="+status+"&loanerReqId="+loanerReqID,"LoanPrintVerLetter","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
	}
  else if (sourceID == 50186)
	{
	    var raId = document.all.returnID.value;
		windowOpener("/GmInHouseSetServlet?hAction=ViewLetterVer&hConsignId="+val+"&hReturnId="+raId+"&hType="+document.frmShipDetails.ctype.value+"&status="+status,"LoanPrintVerLetter","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
	}
 if(sourceID == 50181 && printtype == 'autoprint'){
	 setTimeout("popupWin.close();",7100); 
 }
}

function fnPrintForReturnType(printtype){
	val = document.all.refId.value;
	var type = document.all.ctype.value;
	sourceID  = document.all.source.value;			//4000518 --Returns
													//50181 --Consignments
													//26240144 --Return to Owner Company
	if(sourceID == 4000518 || (sourceID == 50181 && type == 26240144)){  
		  windowOpener("/gmICTReturn.do?method=fetchInhouseTransItems&requestId="+val+"&sourceType="+sourceID+"&strOpt="+printtype,"ReturnPrintVersion","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
	  }
}


function validateTrackingNum(){
	if(event.keyCode == 13){		
	var trkNum ='';
	var validTrkNum= '';
	var shipCarrier = '';
	var shipMode = '';
	shipCarrier=  document.frmShipDetails.shipCarrier.value; //(5001  - FedEx)
	shipMode= document.frmShipDetails.shipMode.value;  // (5008  = ground;  others-express)
	trkNum = document.frmShipDetails.trackingNo.value;
	  
	
	if (shipCarrier=='5001') // Fedex
	{
		if (shipMode != '5008')// 5008 - Ground
		{ 	 
			var strTrkNum = 'FEDEX_PRIORITY_' + trkNum.length ;
			var strTrkNumTrim = 'FEDEX_PRIORITY_' + trkNum.length + '_TRIM';
			
			validTrkNum = checkTrkNum(strTrkNum, strTrkNumTrim, trkNum); 
			 
		}
		else {  // Ground
			var strTrkNum = 'FEDEX_GROUND' ;
			var strTrkNumTrim = 'FEDEX_GROUND_TRIM';
			
			validTrkNum = checkTrkNum(strTrkNum, strTrkNumTrim, trkNum);
		}
	}
	else {                     
			validTrkNum = trkNum;
	}
	
	document.frmShipDetails.trackingNo.value = validTrkNum;     
	if(window.event && window.event.keyCode == 13)
	{
	window.event.keyCode = 8;
	}
	document.frmShipDetails.txt_LogReason.focus();
 }
}

function checkTrkNum(strTrkNum, strTrkNumTrim, trkNum)
{ 
	var strFedex ='';
	var strFedexTrim ='';
	var validTrkNum= '';
	
	for (var i=0;i<len;i++)
	{
		arr = eval("FexArr"+i);
		arrobj = arr.split(",");
		key = arrobj[0];
		value = arrobj[1]; 
		 
		if(key == strTrkNum)
		{
			strFedex = value; 
		}
		if(key == strTrkNumTrim)
		{
			strFedexTrim = value; 
		}
	}	 
	 
	if(Number(strFedexTrim)!=0)
	{
		validTrkNum = trkNum.slice(Number(strFedex), Number(strFedexTrim)); 
	}
	else
	{
		validTrkNum = trkNum.slice(Number(strFedex)); 
	}
	return validTrkNum;
}

function fnConsignDetails(){
	val = document.all.refId.value;
	windowOpener("/GmConsignSetServlet?hAction=LoadConsign&hId="+val,"Con1","resizable=yes,scrollbars=yes,top=150,left=200,width=900,height=600");
}

function cpyToTxtBx(value){
	if (value != 0 )
	{
		fnValidateDropDn('shipTo',message[5620]);
		fnValidateDropDn('names',message[5564]);
		fnValidateDropDn('shipCarrier',message[5621]);
		fnValidateDropDn('shipMode',message[5622]);			
		if (document.all.shipTo.value == '4121' && document.all.addressid.value == '')
		{
			Error_Details(message[32]);
		}
		 if (ErrorCount > 0) {
			Error_Show();
			Error_Clear();						
			document.frmShipDetails.tracknos.value =0;			
			return false;			
		 }
		document.frmShipDetails.trackingNo.value = value;
		enabledisableFields(true);
	}
	if (value == 0)
	{
		document.frmShipDetails.trackingNo.value = "";
		enabledisableFields(false);
	}
}

function enabledisableFields(value){
	/*if (value == true)
	{		
		if (document.frmShipDetails.shipTo.value != document.frmShipDetails.hshipTo.value)
		{
			document.frmShipDetails.shipTo.value = document.frmShipDetails.hshipTo.value;
		}
		if (document.frmShipDetails.names.value != document.frmShipDetails.hnames.value)
		{
			fnGetNames(document.frmShipDetails.shipTo);
			document.frmShipDetails.names.value = document.frmShipDetails.hnames.value;
		}		
		if (document.frmShipDetails.haddressid.value != '')
		{
			fnGetAddressList(document.all.names,document.frmShipDetails.haddressid.value);
		}	
	}
*/
		
		if (document.frmShipDetails.shipTo.value == 4121) // sales rep
		{	
			document.frmShipDetails.addressid.disabled = value;
		}	
		document.frmShipDetails.shipTo.disabled = value		
		document.frmShipDetails.names.disabled = value	
		document.frmShipDetails.shipCarrier.disabled = value		
		document.frmShipDetails.shipMode.disabled = value	

} 

function fnDisableEmail()
{
	document.frmShipDetails.hTxnId.value = document.frmShipDetails.hshipId.value;
		document.frmShipDetails.hTxnName.value = document.frmShipDetails.refId.value;
	document.frmShipDetails.action ="/GmCommonCancelServlet";
	document.frmShipDetails.hAction.value = "Load";			
	document.frmShipDetails.hCancelType.value = 'DSHEM';
	document.frmShipDetails.submit(); 
}

function fnValidateAddress(loader){
	responseText =loader.xmlDoc.responseText;
	if(TRIM(responseText) == "Y"){
		Error_Details(message[5100]);
	}else{
		document.frmShipDetails.Btn_Submit.disabled=true;
		document.frmShipDetails.submit();
	}
	if (ErrorCount > 0)
	{
		Error_Show();
		 Error_Clear();
		return false;
	}
}
function fnPrintRAInfo(val)
{
	windowOpener("/GmPrintCreditServlet?hAction=PRINT&hRAID="+val,"Con1","resizable=yes,scrollbars=yes,top=250,left=200,width=800,height=300");  
}


function fnAsscTxn(val,type,refid)
{ 
	windowOpener("/gmItemControl.do?haction=PicSlip&hConsignId="+val+"&txntype="+type+"&ruleSource="+type+"&refId="+refid,"SetPic","resizable=yes,scrollbars=yes,top=150,left=150,width=750,height=500");	
  
}

// this function used to open the HQ company commerical invoice details.
function fnCommercialInv() {
	var ordId = document.frmShipDetails.refId.value;
	windowOpener("/gmProformaInvoice.do?orderid=" + ordId
			+ "&strOpt=COMMERCIAL_INV", "Print",
			"resizable=yes,scrollbars=yes,top=150,left=150,width=800,height=750");
}


//For StockTransfer Print
function fnPrintSlip(){
	val = document.all.refId.value;
	//Three pop up window for Stock Transfer Slip
	if ((strPrintCountFl =='3')){
    for(i=1;i<=strPrintCountFl;i++){
		recipientLbl = ((i==1)?"ORG_RECIPIENT":(i==2)?"DUP_TRANSPORTER":"TRIP_SUPPLIER");
		fnStocktransferChallan("recipientLbl",val);
	}
	}else{
		windowOpener("/GmConsignSetServlet?hAction=PackVersion&hId="+val,"Con","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600"); 
	}
}
function fnStocktransferChallan(printPurpose,refId){
		windowOpener("/GmConsignSetServlet?hAction=PackVersion&hId="+refId+"&printPurpose="+recipientLbl,recipientLbl,"resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}





