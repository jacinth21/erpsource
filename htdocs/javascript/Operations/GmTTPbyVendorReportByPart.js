function fnOnpageLoad(){
	document.getElementById("dataGridDiv").style.display = 'none';
	document.getElementById("DivExportExcel").style.display = 'none';
	
}
function fnPartDtdLoad(){
	
	var ttpId = document.frmTTPByVendorParDet.ttpId.value;    
	var ttpMonth = document.frmTTPByVendorParDet.monthId.value;  
	var ttpYear = document.frmTTPByVendorParDet.yearId.value; 
	var vendorId = document.frmTTPByVendorParDet.vendorId.value; 
	if(ttpId == '' && vendorId == ''){
		Error_Details(message_operations[825]);
	}
	fnValidateDropDn('monthId','Month');
	fnValidateDropDn('yearId','Year');
	
	if (ErrorCount > 0)
	{
	Error_Show();
	Error_Clear();
	return false;
	}
	//fnStartProgress();
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmTTPByVendorPartDetails.do?method=fetchTTPbyVendorPartDetail&ttpId='+ ttpId+
			'&monthId='+ ttpMonth+ '&yearId='+ ttpYear+ '&vendorId='+ vendorId+'&ramdomId=' + new Date().getTime());
	dhtmlxAjax.get(ajaxUrl,fnTTPByVendorPartResponse)
	fnStartProgress();
}
function fnTTPByVendorPartResponse(loader){
	
	response = loader.xmlDoc.responseText;
	//alert(response);
	fnStopProgress();
	if(response !=''){	
		
		var setHeader = ["TTP Name","Status","Vendor","Part #","Description","Period","Recomm.Qty","Raise PO Qty","Order Amt","","","",""]; //,"","","","" 
		var setColIds = "ttpname,status,vendorname,partnum,partnumdesc,period,recommandqty,raisePOqty,orderamt,ttpdid,vendordid,ttpdmonth,ttpdyear"; //,ttpdid,vendordid,ttpdmonth,ttpdyear

		  		var setColTypes = "ttpNm,ro,vendorNm,ro,ro,ro,ron,ron,ron,ro,ro,ro,ro"; 
		  		var setColAlign = "left,left,left,left,left,left,right,right,right"; 
		  		var setColSorting = "str,str,str,str,str,str,int,int,int";
		  		var enableTooltips = "true,true,true,true,true,true,true,true,true,true,true,true,true";
		  		var setFilter = [ "#select_filter", "#select_filter", "#select_filter", "#text_filter","#text_filter", "#select_filter", "#numeric_filter", "#numeric_filter", "#numeric_filter"];
		  		//for full data table width
		  		var setInitWidths = "200,100,200,80,220,100,90,90,103,5,4,4,4";
		  		//var setInitWidths = "200,100,200,80,220,90,90,90,100";
		  		var gridHeight = "";
		  		var dataHost = {};
		  		format = "Y"		  			
		  		var footerArry=new Array();	
				footerArry[0]="";
				footerArry[1]="";
				footerArry[2]="";
				footerArry[3]="";
				footerArry[4]="";
				footerArry[5]="<b>Total<b>";
				footerArry[6]="<b>{#stat_total}<b>";	
				footerArry[7]="<b>{#stat_total}<b>";	
				footerArry[8]="<b>{#stat_total}<b>";		
				var footerStyles = [];
				var footerExportFL = true;
				 		
				document.getElementById("trDiv").style.display = "table-row";
		  		document.getElementById("dataGridDiv").style.display = 'block';
		  		document.getElementById("DivNothingMessage").style.display = 'none';
		  		document.getElementById("DivExportExcel").style.display = 'block';
		  		// split the functions to avoid multiple parameters passing in single
		  		
		  		// function   
		  		gridObj = initGridObject('dataGridDiv');
		  		gridObj = fnCustomTypes(gridObj, setHeader, setColIds, setInitWidths,
		  				setColAlign, setColTypes, setColSorting, enableTooltips,
		  				setFilter, 400, format,footerArry,footerStyles);
		  		
		  		if (format != ""){
		            var formatDecimal = "0,000.00";
		            gridObj.setNumberFormat("0,000",6);
		            gridObj.setNumberFormat("0,000",7);
		            gridObj.setNumberFormat(formatDecimal,8,".",",");
		        }
		  		gridObj = loadDhtmlxGridByJsonData(gridObj, response);
		  		gridObj.attachEvent("onRowSelect",function(rowId,cellIndex){
		  			if(cellIndex == 0 ){
		                var ttpdid= gridObj.cellById(rowId, 9).getValue();
		                var vendordid= '';
		                var ttpdmonth= gridObj.cellById(rowId, 11).getValue();
		                var ttpdyear= gridObj.cellById(rowId, 12).getValue();
		                var ttpname= gridObj.cellById(rowId, 0).getValue();
		                var vendorname= '';
		                		                
		                fnViewTTPSumryDetByTTPID(ttpdid,vendordid,ttpdmonth,ttpdyear,ttpname,vendorname);
		            } 
		  			if(cellIndex == 2){
		                var ttpdid= '';
		                var vendordid= gridObj.cellById(rowId, 10).getValue();
		                var ttpdmonth= gridObj.cellById(rowId, 11).getValue();
		                var ttpdyear= gridObj.cellById(rowId, 12).getValue();
		                var ttpname = '';
		                var vendorname= gridObj.cellById(rowId, 2).getValue();
		                
		                fnViewTTPSumryDetByVendorID(ttpdid,vendordid,ttpdmonth,ttpdyear,ttpname,vendorname);
		            }
		  			
					});
		  		
		  		gridObj.setColumnHidden(9, true);
				gridObj.setColumnHidden(10, true);
		  		gridObj.setColumnHidden(11, true);
		  		gridObj.setColumnHidden(12, true); 	
		  		
		}else{
		  		document.getElementById("dataGridDiv").style.display = 'none';
		  		document.getElementById("DivNothingMessage").style.display = 'block';
		  		document.getElementById("DivExportExcel").style.display = 'none';
		  	 }	  		
}

//This Function Used to set custom types conditions in Grid
function fnCustomTypes(gObj, setHeader, setColIds, setInitWidths, setColAlign,
		 setColTypes, setColSorting, enableTooltips, setFilter, gridHeight,format,footerArry,footerStyles) {

		gObj.setHeader(setHeader);
		var colCount = setHeader.length;
		gObj.setInitWidths(setInitWidths);
		gObj.setColAlign(setColAlign);
		gObj.setColTypes(setColTypes);
		gObj.setColSorting(setColSorting);
		gObj.enableTooltips(enableTooltips);
		gObj.attachHeader(setFilter);
		gObj.setColumnIds(setColIds);  
		
		gObj.enableAutoHeight(true, gridHeight, true);
		
		if (footerArry != undefined && footerArry.length > 0) {
			var footstr = eval(footerArry).join(",");
				if (footerStyles != undefined && footerStyles.length > 0)
					gObj.attachFooter(footstr, footerStyles);
				else
					gObj.attachFooter(footstr);
		}  
			gObj.enableAutoHeight(true, gridHeight, true);	
			return gObj;
}
//to download excel
function fnExport() {

	gridObj.toExcel('/phpapp/excel/generate.php');

}

//This function used to create user defined column types in Grid
function eXcell_ttpNm(cell) {
	//alert("ttpNm");// the eXcell name is defined here
	if (cell) { // the default pattern, just copy it
		this.cell = cell;
		this.grid = this.cell.parentNode.grid;
	}
	
	this.setValue = function(val) {
		this.setCValue("<a href=\"#\">"+val+"",val);	
	}
	this.getValue = function() {
		return this.cell.childNodes[0].innerHTML; // gets the value
	}
}
eXcell_ttpNm.prototype = new eXcell;

function eXcell_vendorNm(cell) {
	//alert("vendorNm");// the eXcell name is defined here
	if (cell) { // the default pattern, just copy it
		this.cell = cell;
		this.grid = this.cell.parentNode.grid;
	}
	
	this.setValue = function(val) {
				this.setCValue("<a href=\"#\">"+val+"",val);	
	}
	this.getValue = function() {
		return this.cell.childNodes[0].innerHTML; // gets the value
	}
}
eXcell_vendorNm.prototype = new eXcell;

function fnViewTTPSumryDetByTTPID(ttpdid,vendordid,ttpdmonth,ttpdyear,ttpname,vendorname){
	windowOpener("/gmTTPByVendorSummary.do?method=loadTTPbyVendorSummary&ttpId="+ttpdid+"&ttpMonth="+ttpdmonth+"&ttpYear="+ttpdyear+"&vendorId="+vendordid+"&ttpName="+ttpname+"&vendorName="+vendorname,"Search","resizable=yes,scrollbars=yes,top=250,left=300,width=1550,height=700");
}

function fnViewTTPSumryDetByVendorID(ttpdid,vendordid,ttpdmonth,ttpdyear,ttpname,vendorname){
	windowOpener("/gmTTPByVendorSummary.do?method=loadTTPbyVendorSummary&ttpId="+ttpdid+"&ttpMonth="+ttpdmonth+"&ttpYear="+ttpdyear+"&vendorId="+vendordid+"&ttpName="+ttpname+"&vendorName="+vendorname,"Search","resizable=yes,scrollbars=yes,top=250,left=300,width=1550,height=700");
}
