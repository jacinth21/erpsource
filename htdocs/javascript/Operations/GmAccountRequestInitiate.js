var lonerReqIdValidFl = true;
var refids = "";
var refNm = "";
var status = true; 
var  response ="";
function Reload (refid, refname, flag ) { 

	refid = trim(refid);
	
	var valobj = refids.split(","); 
	var arrlen = valobj.length;  
	if (arrlen > 0)
	{
		for (var j=0;j< arrlen;j++ )
		{ 	 
				if ( valobj[j] == refid)
				{
					status = false;
					break ; 
				} 
		}
	}
	else
	{
		 status = true; 
	}
	  
	if(flag=='-1' )
	 {	 
		refids = refids.replace(refid,"");
		 
	  }
	 else
	 {	 
		 if(status==true )
		 {	 
			 refids = refids  + refid + ",";
		 }
	 }
	
	refNm =  refname;
	var f = document.getElementById('iframe1'); 
	   
 	f.src  = document.frmAccountRequestInitiate.RE_SRC.value + "&RefIDs=" + refids + "&shipTo=" +document.all.shipTo.value + "&names=" +  document.all.names.value + "&RefName=" + refname;

 	var divfr = document.getElementById('divf'); 
	if(refid!='')
	{ 
 		divfr.style.display="block"; 
	}   
 	f.src = f.src;   
	}

function addJavascript(jsname) {
var th = document.getElementsByTagName('head')[0];
var s = document.createElement('script');
s.setAttribute('type','text/javascript');
s.setAttribute('src',jsname);
th.appendChild(s);
}


function fnSubmit(){
	addJavascript('/javascript/Message.js');
	addJavascript('/javascript/Error.js'); 
	var setid = document.getElementById("frmCart").contentWindow.fnGetSetID(); //To support Edge browser PC-3659 changes 
	var obj = document.all.consignmentType;
	var seltext = obj.options[obj.selectedIndex].text;
 	var bill = document.all.distributorId.value;

 	var purpose = document.frmAccountRequestInitiate.inHousePurpose.value; 
	 flg = 'true';

	 if(purpose == 4000097){ //OUS Sales Replenishments	 
	 	// validate the Customer PO field. (here only enter the PO # - In Modify Shipping Details screen we can't edit the PO #)
		 var custPO = document.frmAccountRequestInitiate.Txt_1006420;
		 if(custPO != undefined){
			 fnValidateTxtFld('Txt_1006420', message[5541]);
		 }
	 }
	 
	 if (purpose == 50061){ // ICT sameple
		var DistlistArr = Distlist.split(",");		
		for (var j=0;j< DistlistArr.length;j++ )
			{			
				if (bill != DistlistArr[j]){
					flg = 'false';
				}
				else{
					flg = 'true';
					break;
				}
			}
	 }
	if (flg == 'false'){		
		Error_Details(message[5600]);
	}
		
	var str = document.getElementById("frmCart").contentWindow.fnCreateOrderString(); //To support Edge browser PC-3659 changes

	document.frmAccountRequestInitiate.hinputString.value = str;
	// Appending the customer po attribute type and attributeVal to the input string.
	var attType = '';
	var attVal = '';
	str='';
	for(var i=0;i<attCnt;i++){
		var attribType= eval("document.frmAccountRequestInitiate.h"+i);
		attType = attribType.value;
		var attribvalue = eval("document.frmAccountRequestInitiate.Txt_"+attType);
		attVal = attribvalue.value;
		if(attVal != '')
			str += attType + '^' + attVal + '|';

	}
	document.frmAccountRequestInitiate.hinputstr.value = str;
	fnValidate();
	fnValidateShipping();	
	if(strErrorWhType != ''){
		Error_Details(Error_Details_Trans(message[5601],strErrorWhType.substring(1, strErrorWhType.length)));
	}
	if(strErrorRwQty != ''){
		Error_Details(Error_Details_Trans(message[5602],strErrorRwQty.substring(1, strErrorRwQty.length)));
	}
	
	if(strErrorFG !=''){
		Error_Details(Error_Details_Trans(message[5603],strErrorFG.substring(1, strErrorRwQty.length)));
	}
	if(strErrorRW !=''){
		Error_Details(Error_Details_Trans(message[5604],strErrorRW.substring(1, strErrorRwQty.length)));
	}
	
	 var loanerReqIdObj  = document.frmAccountRequestInitiate.loanerReqID;
	  if(loanerReqIdObj != undefined){
		   var loanerReqId = loanerReqIdObj.value;
		   if(!lonerReqIdValidFl && loanerReqId !=''){
				Error_Details(message[10607]);
			}
	   }
	
	if(response != "" ) {
 		Error_Details(response);
 	}
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	if (document.all.names.value != 0 ){
		document.all.names.disabled = false;
	} 
	
	if(document.all.consignmentType.value == 102930){
		document.all.inHousePurpose.disabled = false;
	} 
	var Repobj = '';
	if(document.all.Cbo_LoanToASSORep){
		Repobj = document.all.Cbo_LoanToASSORep;
	}
	
	var confMsg = false;
	var submitFl = false;
	/*
	 * when the Loaner requested by Rep under his name we Need to display Confirmation Box , saying that Assoc Reps not selected.
	 * In other Scenarios need not display the confirmation box.
	 */
	if(Repobj != undefined && Repobj.value != ''){
		if(Repobj.value == '0' ){	
		   if(document.all.assocRep.style.display == 'block'){
			 if(confirm(message[5605])){	
					submitFl = true;
			 }		
		   }else{
			   submitFl = true;
		   }
		}else{
			submitFl = true;
		}	
	}else{
		    submitFl = true;
	}
	if(submitFl){
		if (seltext== 'Product Loaner' ){
			fnStartProgress('Y');
		}
		if(Repobj != undefined && Repobj.value == '0'){
			Repobj.value = '';
		}
		
		document.frmAccountRequestInitiate.action = "/gmRuleEngine.do?RefName="+refNm+"&RefIDs="+refids; 
	 	document.frmAccountRequestInitiate.hrefName.value = refNm; 
		document.frmAccountRequestInitiate.strOpt.value = 'save';
		document.frmAccountRequestInitiate.Btn_Submit.disabled = true;
		fnStartProgress();
		document.frmAccountRequestInitiate.submit();
		
	}
}


function fnValidate(){
	var requestTypeVal = '';
	var requiredDate = '';
	var billTo = '';
	var shipTo = '';
	var inputstr = '';
    var inHousePurposeVal='';
	fnValidateDropDn('consignmentType',message[5542]);
	//fnValidateTxtFld('requiredDate',' Required Date ');
	fnValidateDropDn('inHousePurpose',message[5543]);

	inputstr = document.frmAccountRequestInitiate.hinputString.value;	
	objPlannedDt = document.all.plannedDate;
	var palnnedDateDiffVal = dateDiff(todaysDate,objPlannedDt.value,format);
	requestTypeVal = document.frmAccountRequestInitiate.consignmentType.value;
	inHousePurposeVal = document.frmAccountRequestInitiate.inHousePurpose.value;
	var reqTypeName = document.frmAccountRequestInitiate.consignmentType.options[document.frmAccountRequestInitiate.consignmentType.selectedIndex].text;
	
	 if(requestTypeVal != '40025'){
 		 Error_Details(message[5079]); 		
 	 }
	 if(requestTypeVal == '40025' && inHousePurposeVal != '50060'){
 		 Error_Details(message[5080]); 		
 	 }	 
	if (requestTypeVal == 4127)
	{
		fnValidateTxtFld('requiredDate',message[5544]);
	}
	else
	{
		fnValidateTxtFld('requiredDate',message[5545]);
	}
	 
	fnValidateTxtFld('plannedDate',message[5606]);
	if(inputstr == ''){
			Error_Details(message[5081]);
		}
	
	if(objPlannedDt.value != ""){
		CommonDateValidation(objPlannedDt,format,Error_Details_Trans(message[5546],format));
	}
	if(objPlannedDt.value != ''){
		if(palnnedDateDiffVal < 0){
			Error_Details(message[5082]);
		}
	}
	
	
	if(requestTypeVal == '40021' || requestTypeVal == '102930'){
		 
		billTo = document.frmAccountRequestInitiate.distributorId.value;
		if(billTo == '01'){
			Error_Details(Error_Details_Trans(message[5083],reqTypeName));
		}
	}

	if(requestTypeVal == '40022'){
		var objInHousePurpose = document.frmAccountRequestInitiate.inHousePurpose;

		if (objInHousePurpose.disabled == true){
			Error_Details(message[5084]);	
		}
	
			fnValidateDropDn('inHousePurpose',message[5543]);
		//	fnValidateDropDn('employeeId',' Value ');
		
		billTo = document.frmAccountRequestInitiate.distributorId.value;
		shipTo = document.all.shipTo.value;

		if(billTo != '01'){
			Error_Details(message[5085]);
		}
		
		if(shipTo != '4123'){
			Error_Details(message[5086]);
		}
	}
	
	
	// validation added for GIMS
	var negQtyStr = '';
	var nonNumStr = '';
	var maxQtyStr = '';
	var empQtyStr = '';
	var stoQtyStr = '';
	var decimalCheck = '';
	for (i = 0;i<window.frames.frmCart.cnt ; i++){		
	
		var reqQtyObj = eval("window.frames.frmCart.document.all.Txt_Qty"+i);
		var stockQtyObj = eval("window.frames.frmCart.document.all.hStock"+i);
		var objpnum = eval("window.frames.frmCart.document.all.Lbl_Part"+i);

		var reqQtyVal = '';
		var stockQtyVal = '';	

		if ( reqQtyObj != undefined )
			reqQtyVal = TRIM(reqQtyObj.value);

		if (stockQtyObj != undefined )
			stockQtyVal = parseInt(stockQtyObj.value);

		var errFl = true;

		if (objpnum.innerHTML != '&nbsp;') {
			if(reqQtyVal <= 0){
				negQtyStr += objpnum.innerHTML +", ";
			}
			decimalCheck = reqQtyVal.replace(/[0-9]+/g,''); // Qty text box
			if (isNaN(reqQtyVal) || decimalCheck.length >0){   
				errFl = false;
				nonNumStr += objpnum.innerHTML +", ";		    	   	
		    }

			if(reqQtyVal ==''){
				empQtyStr += objpnum.innerHTML +", ";				
			}
			
			if(requestTypeVal == '102930'){ // Inter Company Transfer
				if(stockQtyVal <='0' || stockQtyVal ==''){
					stoQtyStr += objpnum.innerHTML +", ";					
				}
				if(stockQtyVal != '0' && stockQtyVal < reqQtyVal){
					if(errFl){
						maxQtyStr += objpnum.innerHTML +", ";
					}
					
				}
			}			
		}		
		}
	
	if (negQtyStr !=''){			
		Error_Details(Error_Details_Trans(message[5607],negQtyStr.substr(0,negQtyStr.length-2)));
	}
	if (nonNumStr !=''){			
		Error_Details(Error_Details_Trans(message[5608],nonNumStr.substr(0,nonNumStr.length-2)));
	}
	if (maxQtyStr !=''){			
		Error_Details(Error_Details_Trans(message[5609],maxQtyStr.substr(0,maxQtyStr.length-2)));
	}
	if (empQtyStr !=''){			
		Error_Details(Error_Details_Trans(message[5610],empQtyStr.substr(0,empQtyStr.length-2)));
	}
	if (stoQtyStr !=''){			
		Error_Details(Error_Details_Trans(message[5610],stoQtyStr.substr(0,stoQtyStr.length-2)));
	}
}


//Trims the leading and trailing spaces
function trim(str)
{
	str = new String(str);
	var intLen = str.length;
	var intBegin = 0;
	var intEnd = intLen - 1;
	while ((str.charCodeAt(intBegin) == 13 || str.charCodeAt(intBegin) == 10 || str.charAt(intBegin) == " ") && intBegin < intLen)
	{
		if (str.charCodeAt(intBegin) == 13 || str.charCodeAt(intBegin) == 10)
			intBegin+=2;
		else
        		intBegin++;
	}
	while ((str.charCodeAt(intEnd) == 13 || str.charCodeAt(intBegin) == 10 ||  str.charAt(intEnd) == " ") && intBegin < intEnd)
	{
		if (str.charCodeAt(intEnd) == 13 || str.charCodeAt(intBegin) == 10)
			intEnd-=2;
		else
			intEnd--;
	}
	return str.substring(intBegin, intEnd+1);
}
/*
 * The below function is used to fetch the Assoc Reps based on the Selected Sales Rep.
 * 
 */
function fnSetAssocReps (RepId){
	j = 0;
	var Repobj = "";
	if(document.all.Cbo_LoanToASSORep){
		Repobj = document.all.Cbo_LoanToASSORep;
		Repobj.disabled = false;
		Repobj.options.length = 0;
		Repobj.options[0] = new Option("[Choose One]","0");
	}
	
	for (var i=0;i<AssociateSalesRepLen;i++)
	{
		/*
		 * First we are getting Assoc Rep Array ,it consists of Assoc Rep id,Assoc Rep Name,Rep Id and Rep Name .
		 */
		document.all.assocRep.style.display = "none";
		arr = eval("AssoRepArr"+i);
	
		arrobj = arr.split(",");
		associd = arrobj[1];//Assoc Rep id		
		assocname = arrobj[0];//Assoc Rep Name
		repid = arrobj[2];//Rep Id
		repname = arrobj[3];//Rep Name
		/*
		 * We are in need to display Sales Rep based on the Distributor ,here we are filtering the sales Rep.
		 */
			if(repid == RepId ){
				j++;
				Repobj.options[j] = new Option(assocname,associd);
				if(j == 1){
					aid = associd;
					aName = assocname;
				}
			}
	}
	/*
	 * We are in need to set Assoc Rep Name to default if there is only one Assoc Rep for the corresponding sales Rep.
	 */
	if((j == 1)){
		Repobj.options[j] = new Option(aName,aid);
		Repobj.value=aid;
	}
	//We need to display the Assoc Sales Rep Dropdown only if there is values in that dropdown.
	if(Repobj.options.length >= 2){
		document.all.assocRep.style.display = "block";
	}
	
	
}

function fnLoadList(ConsignType){
	if(ConsignType != '0' && ConsignType != '' ){
		if (ConsignType == '40025'){
			document.frmAccountRequestInitiate.distributorId.options.length = 0;
			document.frmAccountRequestInitiate.distributorId.options[0] = new Option("[Choose One]","0");
			for(var i=0;i<hospiLen;i++){
				arr = eval("AccountArr"+i);
				arrobj = arr.split(",");
				id = arrobj[0];
				name = arrobj[1];
				document.frmAccountRequestInitiate.distributorId.options[i+1] = new Option(name,id);			
			}
		}else{
			document.frmAccountRequestInitiate.distributorId.options.length = 0;
			document.frmAccountRequestInitiate.distributorId.options[0] = new Option("[Choose One]","0");
			for(var i=0;i<distLen;i++){
				arr = eval("DistArr"+i);
				arrobj = arr.split(",");
				id = arrobj[0];
				name = arrobj[1];
				document.frmAccountRequestInitiate.distributorId.options[i+1] = new Option(name,id);
			}
		}
	}	
}
//PMT-32450 - validate Loaner Request Id 
function fnvalidateLoanerRefID(){ 
	 var loanerReqIdObj = document.frmAccountRequestInitiate.loanerReqID;
	if(loanerReqIdObj != undefined){
	 var loanerReqId = loanerReqIdObj.value;
	 var accId = document.frmAccountRequestInitiate.distributorId.value;
	 lonerReqIdValidFl = false;
	 if(loanerReqId ==''){
		 response='';
	 }else{
	 var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmAccountRequestInitiate.do?strOpt=fetchLoanerRefDetails&loanerReqID='+loanerReqId +'&accountId='+accId+'&randomId='+ Math.random());
	 dhtmlxAjax.get(ajaxUrl,function(loader){
	       response = loader.xmlDoc.responseText;
	       if(response != "") {
		 		Error_Details(response);
		 	}
	       if (ErrorCount > 0)
		 	{
 			Error_Show();
 			Error_Clear();
 			return false;
		 	}
	       lonerReqIdValidFl = true;
	 });
	 }
	} 
}
