var gridObj ='';
var copyFromExcelFL;
var hdupWoInputStr ='';

function fnOnPageLoad(){
	fnLoadCallBack();
}

//This function is used to get response and set to screen
function fnLoadCallBack(response){
	fnStartProgress();
	// to clear the grid data
	if(gridObj !=''){
		gridObj.clearAll();
	}
	
	if(response == undefined || response == null){
		strDtlsJson ='';
	}else{
	    strDtlsJson = response;
	}
     
	// to set auto height
	if($(document).height() !=undefined || $(document).height() !=null){
		gridHeight = $(document).height() - 130;
	}
		    
	    gridObj = new dhtmlXGridObject('dataGridDiv');
		gridObj.setHeader("Part Number,Qty,Primary TTP Name");
		gridObj.attachHeader("#text_filter,#numeric_filter,#text_filter");
		gridObj.setInitWidths("200,150,*");
		gridObj.setColAlign("left,center,left");
		gridObj.setColTypes("ed,ed,ro");
		gridObj.setColSorting("str,int,str");
		gridObj.setColumnIds("partnum,qty,ttpnm");
		gridObj.enableAutoHeight(true, gridHeight, true);
	
		gridObj.init();
		gridObj.enableAlterCss("even","uneven");
		gridObj.parse(strDtlsJson,"js");
		gridObj.attachEvent("onKeyPress", onKeyPressed);
		gridObj.attachEvent("onEditCell",doOnCellEdit);
		gridObj.attachEvent("onTab",doOnCellEdit);
	    gridObj.enableMultiselect(true);
		gridObj.enableBlockSelection(true);
		
		partnum_rowId = gridObj.getColIndexById("partnum");
		qty_rowId = gridObj.getColIndexById("qty");
		ttpnm_rowId = gridObj.getColIndexById("ttpnm");
	
		if (strDtlsJson == "") {
			add_fast();
		} 
		fnStopProgress();
}
function fnAddRows() {
	gridObj.filterBy(0, "");// unfilters the grid while adding rows
	gridObj._f_rowsBuffer = null; // clears the cache
	var newRowId = gridObj.getUID();
	gridObj.addRow(newRowId, '');
}
	
//This method is used to add 500 grid rows
function add_fast(){
		gridObj.clearAll();
		gridObj.startFastOperations();
		for (var i = 500; i > 0; i--){
			gridObj.addRow(i,'');
		}
		gridObj.stopFastOperations();
}

//This function is used to validate and fetch primary ttp name for the part
function fnSave(){
    fnRemoveEmptyRow();
	fnValidate();
}

//This function is used to validate input from grid
function fnValidate(){
var hdupPartInputStr = '';
var hdupPartInputString = '';
var dupPartids='';
	var partnumvalidation = '';
	var qtyvalidation ='';
	var qtynevalidation='';
	
	gridObj.forEachRow(function(row_id) { 
		partnumId = TRIM(gridObj.cellById(row_id,partnum_rowId).getValue());
		qty_val = TRIM(gridObj.cellById(row_id, qty_rowId).getValue());
		if(partnumId != '' || qty_val != ''){
		fnStartProgress();
		if(partnumId == '' && qty_val != ''){
				gridObj.setCellTextStyle(row_id,partnum_rowId,"color:red;");
				gridObj.setRowAttribute(row_id,"row_error","Y");
				partnumvalidation = 'Y';
		}
		
		if(qty_val < 0 && qty_val != ''){
				gridObj.setCellTextStyle(row_id,partnum_rowId,"color:red;");
				gridObj.setCellTextStyle(row_id,qty_rowId,"color:red;");
				gridObj.setRowAttribute(row_id,"row_error","Y");
				qtynevalidation = 'Y';
		}else{
				if(partnumvalidation != 'Y' ){
					gridObj.setCellTextStyle(row_id,partnum_rowId,"color:block;");
					gridObj.setCellTextStyle(row_id,qty_rowId,"color:block;");
					gridObj.setRowAttribute(row_id,"row_error","N");
				}
		}
		if(qty_val == '' && qty_val < 0){
				gridObj.setCellTextStyle(row_id,partnum_rowId,"color:red;");
				gridObj.setCellTextStyle(row_id,qty_rowId,"color:red;");
				gridObj.setRowAttribute(row_id,"row_error","Y");
				qtyvalidation = 'Y';
		}else{
				if(partnumvalidation != 'Y' ){
					gridObj.setCellTextStyle(row_id,partnum_rowId,"color:block;");
					gridObj.setCellTextStyle(row_id,qty_rowId,"color:block;");
					gridObj.setRowAttribute(row_id,"row_error","N");
				}
		}		
		if (partnumId != '' && qty_val != '') {
				if (hdupPartInputStr.indexOf(partnumId + ",") == -1) {
					hdupPartInputStr += (partnumId + ",");
					hdupPartInputString += (partnumId + '^' + qty_val + '|');
				} else {
					if (dupPartids.indexOf(partnumId + ",") == -1) {
						dupPartids += (partnumId + ", ");
					}
					gridObj.setRowAttribute(row_id,"row_error","Y");
					gridObj.setCellTextStyle(row_id, partnum_rowId,"color:red;");
				}
			} 
		}
	});
	if (partnumvalidation == 'Y'){
		Error_Details(message_operations[892]);
	}
	if(qtyvalidation == 'Y'){
		Error_Details(message_operations[893]);
	}
	if(qtynevalidation == 'Y'){
		Error_Details(message_operations[894]);
	}
	// Error message for Duplicate Part number Entries..
		if (dupPartids != '' && TRIM(dupPartids).length > 1) {
			dupPartids = dupPartids.substr(0, (TRIM(dupPartids).length - 1));
			error_msg = Error_Details_Trans("The highlighted <b>Part Number(s)</b> are duplicate - [0] ", dupPartids);
			Error_Details(error_msg);
		}
	if (ErrorCount > 0){
	fnStopProgress();
		Error_Show();
		Error_Clear();
		return false;
	}
	fnCreateInputStr();
}
	var partStr = '';
	var inputStr = '';
//This function is used to form inputstring
function fnCreateInputStr(){
	var worowerror = '';
fnStartProgress();
	var gridChrow = gridObj.getChangedRows();
	var gridChrows = gridObj.getChangedRows(',');
	var rowsarr = gridChrows.toString().split(",");
		if (rowsarr.length > 0) {
			for (var rowid = 0; rowid < rowsarr.length; rowid++) {
								
				rowId = rowsarr[rowid];
				qty_id = TRIM(gridObj.cellById(rowId,qty_rowId).getValue());
				partnum_id = TRIM(gridObj.cellById(rowId,partnum_rowId).getValue());
			    worowerror = gridObj.getRowAttribute(rowId,"row_error");
				worowmofied = gridObj.getRowAttribute(rowId,"row_modified");
				
				if(partnum_id != '' && worowerror != 'Y' && qty_id !='' && worowmofied == 'Y'){
					partStr = partStr + partnum_id +',';
					inputStr = inputStr + partnum_id + '^' + qty_id + '|';
				}
			}		
		}
	var loader = dhtmlxAjax.postSync('/gmSafetyStockUpload.do?method=validateandFetchTTPName','&partStr='+
	    partStr +'&inputStr='+inputStr+'&companyInfo='+strCompanyInfo+'&ramdomId=' + Math.random());
		  fnSaveResponseCallback(loader);
}

//fuction is used to set response to grid
function fnSaveResponseCallback(loader){
	response = loader.xmlDoc.responseText;
	var responseSpilt = response;
	
	if (responseSpilt != '') {
	fnStartProgress();
		resInvalidPart = responseSpilt.split('^');
		if(resInvalidPart[0] !='' && resInvalidPart[0] != undefined){
			var partnumArrs = resInvalidPart[0].split(',');
			var partnotexiststr = '';
			for(j=0;j<partnumArrs.length;j++){
				gridObj.forEachRow(function(row_id){
				part_num_val = TRIM(gridObj.cellById(row_id,partnum_rowId).getValue());
			if(part_num_val == partnumArrs[j]){
				gridObj.setCellTextStyle(row_id, partnum_rowId,"color:red;");
				partnotexiststr = 'Y';
			}
			});
	  		}
		if (partnotexiststr == 'Y'){
			Error_Details(message_operations[895]);
		}
		}
		
		
	if (ErrorCount > 0  || partnotexiststr == 'Y' ){
		Error_Show();
		Error_Clear();
		return false;
	}else{
		
	var rows = [], i = 0;
	var JSON_Array_obj = "";
	var ttpnamevalidation = '';
	if(resInvalidPart[1] != '' && resInvalidPart[1] != undefined){
	JSON_Array_obj = JSON.parse(resInvalidPart[1]);
	$.each(JSON_Array_obj, function(index, jsonObject) {
		ttpNm = jsonObject.ttpnm;
		partNum = jsonObject.partnum;	
		// default array
		rows[i] = {};
		rows[i].id = partNum;
		rows[i].data = [];
		gridObj.forEachRow(function(row_id) {
			part_num_val = TRIM(gridObj.cellById(row_id,partnum_rowId).getValue());
			worowerror = gridObj.getRowAttribute(row_id,"row_error");
			if(part_num_val == partNum && worowerror != 'Y'){
				gridObj.cellById(row_id, ttpnm_rowId).setValue(ttpNm);			
				gridObj.setRowAttribute(row_id,"row_modified","N");
				gridObj.setCellTextStyle(row_id,partnum_rowId,"color:block;");
				gridObj.setCellTextStyle(row_id,qty_rowId,"color:block;");
			}else{
			setTimeout(function() {	
			ttp_name_val = TRIM(gridObj.cellById(row_id,ttpnm_rowId).getValue());
			part_num_val = TRIM(gridObj.cellById(row_id,partnum_rowId).getValue());
			qty_val = TRIM(gridObj.cellById(row_id,qty_rowId).getValue());
		
			if (ttp_name_val == '' && part_num_val != '' && qty_val != ''){
				gridObj.setCellTextStyle(row_id,partnum_rowId,"color:red;");
				gridObj.setCellTextStyle(row_id,qty_rowId,"color:red;");
				gridObj.setRowAttribute(row_id,"row_error","N");
				ttpnamevalidation = 'Y';
			}
			},1000);
			}
		});	
		i++;	
	});
	
	}else{
		gridObj.forEachRow(function(row_id) {
			gridObj.setCellTextStyle(row_id,partnum_rowId,"color:red;");
			gridObj.setCellTextStyle(row_id,qty_rowId,"color:red;");
			gridObj.setRowAttribute(row_id,"row_error","N");
		});
		ttpnamevalidation = 'Y';
	}
	setTimeout(function() {
	if (ttpnamevalidation == 'Y' ){
		Error_Details("The highlighted part is not mapped to any primary TTP");
		Error_Show();
		Error_Clear();
		return false;
		
	}else{
	fnStartProgress();
	var loader = dhtmlxAjax.post('/gmSafetyStockUpload.do?method=saveStockUpload','&partStr='+
	    partStr +'&inputStr='+inputStr+'&companyInfo='+strCompanyInfo+'&ramdomId=' + Math.random());
		//  fnSaveResponseCallback(loader);
	document.all.datasuc.style.display = "inline";
	fnStopProgress();
	}
	},1050);
	}}
}
//Funciton will be called while editing the columns in the grid
function doOnCellEdit(stage, rowId, cellInd, nValue, oValue){
	
	if(cellInd == partnum_rowId && (oValue != '' || nValue != '') && stage == 2){ //once the value in part number column
		gridObj.setRowAttribute(rowId,"row_modified","Y");
 	}else if(cellInd == qty_rowId && (oValue != '' || nValue != '') && stage == 2){//once the value in qty column
 		gridObj.setRowAttribute(rowId,"row_modified","Y");
 	}
 	//if(cellInd == qty_rowId && stage == "2"){
	//	fnValidatePart();
	//}
	return true;
}

//Use the below function to copy data inside the screen.
function docopy(){
      gridObj.setCSVDelimiter("\t");
      gridObj.copyBlockToClipboard();
      gridObj._HideSelection();
}

//this function used to copy and paste the grid data
function onKeyPressed(code,ctrl,shift){
			if(code==67&&ctrl){
				if (!gridObj._selectionArea) return alert("You need to select a block area in grid first");
					gridObj.setCSVDelimiter("\t");
					gridObj.copyBlockToClipboard();
				}
				if(code==86&&ctrl){
					fnStartProgress();
					gridObj.setCSVDelimiter("\t");
					gridObj.pasteBlockFromClipboard();
					setTimeout(function() {	
					fnRemoveEmptyRow();
					fnAddModified();
					},1000);
				}
				fnStopProgress();
			return true;
		}
function fnAddModified() {
	gridObj.forEachRow(function(rowId) {
		qty_id = TRIM(gridObj.cellById(rowId,qty_rowId).getValue());
		partnum_id = TRIM(gridObj.cellById(rowId,partnum_rowId).getValue());
	   // worowerror = gridObj.getRowAttribute(rowId,"row_error");
		if(qty_id!='' && partnum_id !='') {
			gridObj.setRowAttribute(rowId,"row_modified","Y");
		}
	});
	
}
//paste when ctrl + V
function addRowFromPaste(){

	var cbData = gridObj.fromClipBoard();
    if (cbData == null) {
          Error_Details(message_prodmgmnt[121]);
    }  
    var rowData = cbData.split("\n");
    var arrLen = (rowData.length) - 1;    
    if(rowData.length - 1 == 0){
		Error_Details(message_prodmgmnt[122]);
		Error_Show();
        Error_Clear();
        return false;	
    }
    
    if(rowData.length-1 > 1000){
		fnStopProgress();
		Error_Details(message_prodmgmnt[123]);		
		Error_Show();
        Error_Clear();
        return false;	
	}

    var newRowData = '';
    var str='';
    setTimeout(function() {	
    gridObj.startFastOperations();
    gridObj.filterBy(0,"");//unfilters the grid
    gridObj._f_rowsBuffer = null; //clears the cache
    for ( var i = 0; i < arrLen; i++) {    	
          newRowData = rowData[i];          
          rowcolumnData = rowData[i].split("\t");
          var row_id = gridObj.getUID();
          gridObj.setCSVDelimiter("\t");
          gridObj.setDelimiter("\t");
          gridObj.addRow(row_id, '');
          gridObj.cellById(row_id, partnum_rowId).setValue(rowcolumnData[0]);
		  gridObj.cellById(row_id, qty_rowId).setValue(rowcolumnData[1]);
		  str = str  +row_id+'^' +rowcolumnData[0] + '^' + rowcolumnData[1] +  '|';
          gridObj.setRowAttribute(row_id, "row_added", "Y");
          gridObj.setRowAttribute(row_id,"row_modified","Y");
          gridObj.setDelimiter(",");
    }

    gridObj.stopFastOperations();
	fnStopProgress();	
	},1000);
	document.all.datasuc.style.display = "none";
}
//Use the below function to remove the rows from the grid.
function removeSelectedRow(){
      var gridrows=gridObj.getSelectedRowId();   
      if(gridrows!=undefined)
      {
            var NameCellValue ;
            var gridrowsarr = gridrows.toString().split(",");
            var added_row;
            var dtlId;
            for (var rowid = 0; rowid < gridrowsarr.length; rowid++)
            {
                  gridrowid = gridrowsarr[rowid];                  
                  gridObj.setRowAttribute(gridrowid,"row_modified","N");
                  gridObj.deleteRow(gridrowid);                                
            }
      }else{
            Error_Clear();
            Error_Details(message_prodmgmnt[120]);                       
            Error_Show();
            Error_Clear();
      }
}

var removeEmptyFl = false;
var confirmMsg = true;

//this function used to remove the empty rows
function fnRemoveEmptyRow() {
	gridObj.forEachRow(function(row_id) {
		partnum_val = gridObj.cellById(row_id, partnum_rowId).getValue();
        qty_val = gridObj.cellById(row_id, qty_rowId).getValue();
        ttpnm_val = gridObj.cellById(row_id, ttpnm_rowId).getValue();
		 if (partnum_val == '' && qty_val == '' && ttpnm_val == '') {
            gridObj.deleteRow(row_id);
        }
	});
}

//To add datas from clipboard.
function addRowFromClipboard() {
	copyFromExcelFL = 'Y';
	var cbData = gridObj.fromClipBoard();
    if (cbData == null) {
          Error_Details(message_prodmgmnt[121]);
    }  
    var rowData = cbData.split("\n");
    var arrLen = (rowData.length) - 1;    
    if(rowData.length - 1 == 0){
		Error_Details(message_prodmgmnt[122]);
		Error_Show();
        Error_Clear();
        return false;	
    }
    
    if(rowData.length-1 > 1000){
		fnStopProgress();
		Error_Details(message_prodmgmnt[123]);		
		Error_Show();
        Error_Clear();
        return false;	
	}

    fnRemoveEmptyRow();
	fnStartProgress('Y');
  
    var newRowData = '';
    var str='';
    setTimeout(function() {	
    gridObj.startFastOperations();
    gridObj.filterBy(0,"");//unfilters the grid
    gridObj._f_rowsBuffer = null; //clears the cache
    for ( var i = 0; i < arrLen; i++) {    	
          newRowData = rowData[i];          
          rowcolumnData = rowData[i].split("\t");
          var row_id = gridObj.getUID();
          gridObj.setCSVDelimiter("\t");
          gridObj.setDelimiter("\t");
          gridObj.addRow(row_id, '');
          gridObj.cellById(row_id, partnum_rowId).setValue(rowcolumnData[0]);
		  gridObj.cellById(row_id, qty_rowId).setValue(rowcolumnData[1]);
		  str = str  +row_id+'^' +rowcolumnData[0] + '^' + rowcolumnData[1] + '|';
          gridObj.setRowAttribute(row_id, "row_added", "Y");
          gridObj.setRowAttribute(row_id,"row_modified","Y");
          gridObj.setDelimiter(",");
    }

    gridObj.stopFastOperations();
	fnStopProgress();	
	},1000);
	document.all.datasuc.style.display = "none";
}

//To paste the data to the grid
function pasteToGrid(){
	copyFromExcelFL = 'Y';
	if(gridObj._selectionArea!=null){
		var enterCnt = 0;
		var woid, woackdt;
		var topRowCnt, rowID;
		var cbData = gridObj.fromClipBoard();
		if (cbData == null) {
			fnStopProgress();
			Error_Details(message_prodmgmnt[89]);
			Error_Show();
        	Error_Clear();
        	return;
		}
		var emptyRowCnt = 0;
		var partExist,added_row;
		var rowData = cbData.split("\n");
		rowcolumnData = rowData[0].split("\t");
		
		if(rowcolumnData.length > 1){
			for (var i=0; i< rowData.length-1; i++){
				rowcolumnData = rowData[i].split("\t");
				if(i == 0){
					topRowCnt = gridObj.getSelectedBlock().LeftTopRow;// Get the top index of selected row
				}else{
					topRowCnt++;
				}
				rowID=gridObj.getRowId(topRowCnt); // Get the row id to paste the data
				
				if(enterCnt == 0){// check whether there are enough rows to paste the record or not
					var gridrowsarr = '';
					var gridrows=gridObj.getAllRowIds();
					if(gridrows!=undefined) gridrowsarr = gridrows.toString().split(",");
					// Get the empty rows count to paste the record
					for(var j=topRowCnt; j< gridrowsarr.length; j++){
						emptyRowCnt++;
					}

					if(emptyRowCnt < rowData.length-1){
						fnStopProgress();
						Error_Details(message_prodmgmnt[124]);
						Error_Show();
			        	Error_Clear();
			        	return;
					}
					enterCnt++;
				}
							
				woid = rowcolumnData[0];
				woackdt = rowcolumnData[1];	
							
				gridObj.cellById(rowID, partnum_rowId).setValue(woid);
				gridObj.cellById(rowID, qty_rowId).setValue(woackdt);
				
				gridObj.setRowAttribute(rowID,"row_modified","Y");
			
			    gridObj._HideSelection();
			}
		}else{
			gridObj.pasteBlockFromClipboard();
		    gridObj._HideSelection();
		}
		 document.all.datasuc.style.display = "none";	
	}else{
		Error_Details(message_prodmgmnt[125]);
		Error_Show();
        Error_Clear();
	}	
}