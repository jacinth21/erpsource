function fnSetList()
{
	windowOpener("/GmPageControllerServlet?strPgToLoad=GmSetReportServlet&strOpt=SETRPT","Con","resizable=yes,scrollbars=yes,top=150,left=200,width=680,height=600");
}

function fnReload(){
	
	CommonDateValidation(document.frmSetLogReport.frmDate, format, Error_Details_Trans(message_operations[300],format));
	CommonDateValidation(document.frmSetLogReport.toDate, format, Error_Details_Trans(message_operations[301],format));
	
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	document.frmSetLogReport.haction.value="Reload"
        fnStartProgress('Y');
	document.frmSetLogReport.submit();
}