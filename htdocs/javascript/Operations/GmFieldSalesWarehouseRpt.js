//This Function used to show the grid values while loading the page
function fnOnPageLoad() {
	var warehouseid='3';
	if (objGridData.indexOf("cell") !='-1') {
		gridObj = initGridData('acctivityRpt', objGridData);
		gridObj.attachEvent("onFilterStart",fnCustomFilter);
	}
	var locType = document.frmfieldSalesWarehouse.locationType.value;
	
	if(locType == '0'){
		if(document.getElementById("fieldSales")!=undefined){
			document.getElementById("fieldSales").style.display = 'none';
		}
		document.getElementById("accounts").style.display = 'none';
	}else if(locType == '4000338'){     // Field Sales
		document.getElementById("accounts").style.display = 'none';
	}else if(locType == '26230710'){     // Hospital dealer
		if(document.getElementById("fieldSales")!=undefined){
			document.getElementById("fieldSales").style.display = 'none';
		}
	}else if(locType == '70110'){     // Hospital Company
		if(document.getElementById("fieldSales")!=undefined){
		document.getElementById("fieldSales").style.display = 'none';
		warehouseid = '5';       
		}

	}else if(locType == '56005'){    // Account
		document.getElementById("fieldSales").style.display = 'none';
		warehouseid = '5';       
	}
	document.frmfieldSalesWarehouse.warehouseId.value = warehouseid;	
	
	// checking account output obj is undefined, to call system id selected
	var obj = document.frmfieldSalesWarehouse.accOutputType;
	if(obj != undefined){
		fnChngeOutputType(obj);
		fnCheckSelections();
	}
	
}

//This Function used to Initiate the grid
function initGridData(divRef, gridData) {
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);
	var obj = document.frmfieldSalesWarehouse.accOutputType;
	// checking account output obj is undefined, to add select all check box
	if(obj != undefined){
		gObj.enableTooltips("true,true,true,true,true,true,true,true,true,true,true,true,true,true,true");
		gObj.attachHeader("<input type='checkbox' value='no' name='selectAll' onClick='javascript:fnChangedFilter(this);'/>"+',#text_filter,#select_filter,#select_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter');
	}else{
		gObj.enableTooltips("true,true,true,true,true,true,true,true,true,true,true,true,true,true");
		gObj.attachHeader('#text_filter,#select_filter,#select_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter');
	}

	gObj.attachEvent("onCheckbox", fnSelectAllToggle);
	
	gObj.init();
	gObj.enablePaging(true, 100, 10, "pagingArea", true);
	gObj.setPagingSkin("bricks");
	gObj.loadXMLString(gridData);
	return gObj;
}

function fnRptLoad(){
	var name = '0';
	var locType = document.frmfieldSalesWarehouse.locationType.value;
	
	
	var pNum =  document.frmfieldSalesWarehouse.pNum.value;
	var setstr = '';
	
	var obj = document.frmfieldSalesWarehouse.accOutputType;
	//Checking account output obj is undefined, to form selected systems to string
	if(obj != undefined){
		var objProject = document.frmfieldSalesWarehouse.Chk_GrpChk_System;
		var setlen = objProject.length;
		for (var i=0;i<setlen;i++)
		{
			if (objProject[i].checked == true)
			{
				setstr = setstr + objProject[i].value + ',';
			}
		}
		document.frmfieldSalesWarehouse.sysIds.value = setstr.substr(0,setstr.length-1);
	}
	
	
	
		var fsQtyOpr   = document.frmfieldSalesWarehouse.fsQtyVal.value;
		var fsQuantity = document.frmfieldSalesWarehouse.fsQuantity.value;
	
		if(fsQtyOpr != '0' &&  fsQuantity == ''){
			Error_Details(message[10502]);
		}
		if(locType == '0'){
			Error_Details(message[5283]);
		}
		
		if (document.frmfieldSalesWarehouse.fsQuantity){
			NumberValidation(fsQuantity,message[10504],0);
		}else{
			document.frmfieldSalesWarehouse.warehouseId.value = '3';
			name = document.frmfieldSalesWarehouse.fieldSales.value;
		}
	
	 if(locType == '26230710' ||locType=="70110"){ //Hospital delear 
			document.frmfieldSalesWarehouse.warehouseId.value = '5';
			name = document.frmfieldSalesWarehouse.account.value;
	 }
	// added account name empty validation
	if(obj != undefined){
	if(obj.value == '106930'){
		if((name == '0' || name=='') && pNum == '')	{
			Error_Details(message[10505]);
		}
	}
	if(obj.value == '106931'){
		if((name == '0' || name=='') && setstr =='') {
			Error_Details(message[5312]);
		}
	}
	}
	if (ErrorCount > 0)  
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	fnStartProgress("Y");	
	document.frmfieldSalesWarehouse.locationType.value = locType;
	document.frmfieldSalesWarehouse.strOpt.value = 'ReloadRptByLot';
	document.frmfieldSalesWarehouse.submit();	
}
function fnLoad(){
	
	var name = '0';
	var locType = document.frmfieldSalesWarehouse.locationType.value;
	
	var fsQtyOpr   = document.frmfieldSalesWarehouse.fsQtyVal.value;
	var fsQuantity = document.frmfieldSalesWarehouse.fsQuantity.value;
	var pNum =  document.frmfieldSalesWarehouse.pNum.value;

	if(fsQtyOpr != '0' &&  fsQuantity == ''){
		Error_Details(message[10502]);
	}
	if(locType == '0'){
		Error_Details(message[10503]);
	}
	
	if (document.frmfieldSalesWarehouse.fsQuantity){
		NumberValidation(fsQuantity,message[10504],0);
	}
	if(locType == '56005' || locType == '26230710' || locType == '70110'){ 
		if(document.getElementById("fieldSales")!=undefined){
		document.getElementById("fieldSales").style.display = 'none';
		}
		document.getElementById("accounts").style.display = 'block';
		document.frmfieldSalesWarehouse.warehouseId.value = '5';
		name = document.frmfieldSalesWarehouse.account.value;
	}
	
	else if(locType == '26230710'|| locType == '70110') {
		
		document.frmfieldSalesWarehouse.warehouseId.value = '5';
		name = document.frmfieldSalesWarehouse.account.value;
	}
		
	
	
	
	else{
		document.frmfieldSalesWarehouse.warehouseId.value = '3';
		name = document.frmfieldSalesWarehouse.fieldSales.value;
	}
	if(name == '0' && pNum == ''){
		Error_Details(message[10505]);
	}
	
	if (ErrorCount > 0)  
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	fnStartProgress("Y");	
	document.frmfieldSalesWarehouse.locationType.value = locType;
	document.frmfieldSalesWarehouse.strOpt.value = 'ReloadRpt';
	document.frmfieldSalesWarehouse.submit();
}
//This function will call the press the enter button.
function fnEnter(){
	if (event.keyCode == 13){ 
		fnLoad();
	}
}
function fnLogEnter(){
	if (event.keyCode == 13){ 
		fnRptLoad();
	}
}
//This function will call the excel download
function fnExport()
{	
	gridObj.setColumnHidden(0,true);
	gridObj.toExcel('/phpapp/excel/generate.php');
	gridObj.setColumnHidden(0,false);
}

//This function will call Log quantity report 
function fnLoadLogQty(location,partnum){
	var locType = document.frmfieldSalesWarehouse.locationType.value;
	windowOpener("/gmFieldSalesWarehouseRpt.do?strOpt=QtyLog&locationId="+location+"&partNumber="+encodeURIComponent(partnum)+"&locationType="+locType, "FieldSalesLog",
			"resizable=yes,scrollbars=yes,top=250,left=250,width=1020,height=520");
}


function fnLoadLogQtyByAcct(location,partnum,lot){
	var locType = document.frmfieldSalesWarehouse.locationType.value;
	if(lot=='NOC#'){
		lot = 'NOLOT';
	}
	windowOpener("/gmFieldSalesWarehouseRpt.do?strOpt=QtyLog&locationId="+location+"&partNumber="+encodeURIComponent(partnum)+"&lotNum="+lot+"&locationType="+locType+"&warehouse=5", "FieldSalesLog",
			"resizable=yes,scrollbars=yes,top=250,left=250,width=1020,height=520");
}

// Customer Filter added.
function fnCustomFilter(ord,data){
	/* This custom filter to avoid hyper link content column filter issue. 
	 * Because default filter consider all text with in the tag. 
	 * Ex. Let us assume xml as <cell><a ..>text</a></cell>.  If user try to file 'a' text then all rows will show. */
		
		for(i=0;i<data.length;i++){
			if(data[i]!="" && data[i] != undefined && (""+data[i]).indexOf("anonymous")==-1){
				var original=data[i];
				original = original.toLowerCase();
				data[i]=function(value){
							if (value.toString().replace(/<[^>]*>/g,"").toLowerCase().indexOf(original)!=-1)
								return true;
							return false;
							};
			}
				
		}
		gridObj.filterBy(ord,data);
		return false;
	}

function fnLoadNames(obj){
	var warehouseid='3';
	if(obj.value == '0'){
		if(document.getElementById("fieldSales")!=undefined){
			document.getElementById("fieldSales").style.display = 'none';
		}
		document.getElementById("accounts").style.display = 'none';
	}else if(obj.value == '4000338'){     // Field Sales
		document.getElementById("accounts").style.display = 'none';
		document.getElementById("fieldSales").style.display = 'block';
	}else if(obj.value == '26230710' || obj.value == '70110'){     // Hospital dealer
		document.getElementById("accounts").style.display = 'block';
		if(document.getElementById("fieldSales")!=undefined){
			document.getElementById("fieldSales").style.display = 'none';
		}
		warehouseid = '5';
	}else if(obj.value == '56005'){    // Account
		document.getElementById("fieldSales").style.display = 'none';
		document.getElementById("accounts").style.display = 'block';
		warehouseid = '5';
	}
	document.frmfieldSalesWarehouse.warehouseId.value = warehouseid;
	fnLoadAjax();
}

function fnLoadAjax(){
	
	var locType = document.frmfieldSalesWarehouse.locationType.value;
	var ajaxUrl = fnAjaxURLAppendCmpInfo("/gmFieldSalesWarehouseRpt.do?strOpt=locType&locationId="+locType);
	dhtmlxAjax.get(ajaxUrl, function(loader){	
	var resJSONObj = $.parseJSON(loader.xmlDoc.responseText);
	var obj=document.getElementById("account");
	fnPopulateOptions(obj, resJSONObj, "ID", "NAME", "");
	});
}

// Function to display part/system based on output type
function fnChngeOutputType(obj){
	var accOutType = obj.value; 
	if(accOutType == "106931"){ //By System
			document.frmfieldSalesWarehouse.pNum.style.display = 'none';
			document.getElementById("accOutType").style.display = 'none';
			document.getElementById("system").style.display = 'block';
			document.getElementById("systemVal").style.display = 'block';
			document.frmfieldSalesWarehouse.Chk_selectAll.checked = false;
			fnSelectAll();
			document.frmfieldSalesWarehouse.pNum.value = '';
	}else if(accOutType == "106930"){ // By part
			document.getElementById("system").style.display = 'none';
			document.getElementById("systemVal").style.display = 'none';
			document.frmfieldSalesWarehouse.pNum.style.display = 'block';
			document.getElementById("accOutType").style.display = 'block';
	}

}

// Function to select all systems
function fnSelectAll(){
	var objProject = document.frmfieldSalesWarehouse.Chk_GrpChk_System;
	var setlen = objProject.length;
	
	var selectAll = document.frmfieldSalesWarehouse.Chk_selectAll;
	var objSelectAll = eval("document.frmfieldSalesWarehouse.Chk_selectAll");	
	 for (var i=0;i<setlen;i++)
		{
			if (objSelectAll.checked == true)
			{
				objProject[i].checked = true;
			}else{
				objProject[i].checked = false;
			}
		}
}

// Function to check the selected systems after loading the report
function fnCheckSelections()
{
	var valSysIds = document.frmfieldSalesWarehouse.sysIds.value;
	
	var objProject = document.frmfieldSalesWarehouse.Chk_GrpChk_System;
	var setlen = objProject.length;
	var sval ='';
	if (valSysIds.indexOf(",") >  0)
	{
		var valobj = valSysIds.split(",");
		for (var j=0;j< valobj.length;j++ )
		{
			sval = valobj[j];
			for (var i=0;i< setlen;i++ )
			{
				if (objProject[i].value == sval)
				{
					objProject[i].checked = true;
					break;
				}
			}
		}
		
		if(valobj.length == setlen){
			document.frmfieldSalesWarehouse.Chk_selectAll.checked = true;
		}
		
	}else if(valSysIds.indexOf(",") <=  0 && valSysIds!=''){
		for (var i=0;i< setlen;i++ )
		{
			if (objProject[i].value == valSysIds)
			{
				objProject[i].checked = true;
				break;
			}
		}
	}
}

//Function to select all the line items in report
function fnChangedFilter(obj) {
	var check_rowId = gridObj.getColIndexById("check");
	if (obj.checked) {
		gridObj.forEachRow(function(rowId) {
			var check_disable_fl = gridObj.cellById(rowId, check_rowId).isDisabled();
			if (!check_disable_fl) {
				gridObj.cellById(rowId, check_rowId).setChecked(true);
			}
		});
	} else {
		gridObj.forEachRow(function(rowId) {
			var 	check_id = gridObj.cellById(rowId, check_rowId).getValue();

			if (check_id == 1) {
				gridObj.cellById(rowId, check_rowId).setChecked(false);
			}

		});
	}
}

// Function to export report in PDF,Excel and RTF 
function fnDownloadRpt(){
	var exportType = document.frmfieldSalesWarehouse.downloadType.value;
	
	var strString = '';
	var downloadType = document.frmfieldSalesWarehouse.downloadType.value;
	if(downloadType == 0){
		Error_Details(message[4000]);
	}

			gridObj.forEachRow(function(rowId) {
				var 	check_id =gridObj.cellById(rowId,0).getValue();
				if (check_id == 1) {
					strString = strString + gridObj.cellById(rowId,1).getValue() +'#@#'
								+gridObj.cellById(rowId,6).getValue() +'#@#'
								+gridObj.cellById(rowId,7).getValue() +'#@#'
								+gridObj.cellById(rowId,8).getValue() +'#@#'
								+gridObj.cellById(rowId,10	).getValue() +'#@#'
								+gridObj.cellById(rowId,2).getValue()+'#@#'
								+gridObj.cellById(rowId,3).getValue()+'#@#'
								+gridObj.cellById(rowId,4).getValue()+'#@#'
								+gridObj.cellById(rowId,5).getValue()+'\\|'
								
								
								
				}
			});
			
			if(strString == ""){
				Error_Details(message[5310]);
			}
			if (ErrorCount > 0)  
			{
					Error_Show();
					Error_Clear();
					return false;
			}	


	document.frmfieldSalesWarehouse.strInput.value = strString;
	document.frmfieldSalesWarehouse.strOpt.value = 'exportReport';
	document.frmfieldSalesWarehouse.submit();
		
}

// Function to clear the account values in redis text field
function fnChangeAccSource(accType, blVal) {

    // on change the Type drop down
	if (blVal == true) {
		document.frmfieldSalesWarehouse.searchaccount.value = '';
		document.frmfieldSalesWarehouse.account.value = '';
	}
	if(accType == '0'){
			document.getElementById("searchaccount").style.display = 'none';
	}else{
		document.getElementById("searchaccount").style.display = 'inline-block';
	}
}

//Description : This function to check the select all check box
function fnSelectAllToggle() {
	var objControl = eval("document.frmfieldSalesWarehouse." + "selectAll");
	var count =0;

	gridObj.forEachRow(function(rowId) {
		var check_id =gridObj.cellById(rowId,0).getValue();
			if (check_id == 1) {
			}else{
				count++;
			}
	});
		if(count != 0){
			objControl.checked = false;
		}else{
			objControl.checked = true;
		}
}