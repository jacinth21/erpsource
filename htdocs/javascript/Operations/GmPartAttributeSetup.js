this.keyArray = new Array(); 
this.valArray = new Array();
this.put = put; 
this.get = get; 
this.findIt = findIt;


function put(key,val) 
{ 
    var elementIndex = this.findIt(key); 
    if( elementIndex == (-1) ) 
    { 
        this.keyArray.push( key ); 
        this.valArray.push( val ); 
    } 
    else 
    { 
        this.valArray[ elementIndex ] = val; 
    } 
} 
 
function get(key) 
{ 
    var result = null; 
    var elementIndex = this.findIt( key ); 
    if( elementIndex != (-1) ) 
    {    
        result = this.valArray[ elementIndex ];
    } 
    else {
    	result = "";
    }
    return result; 
}
function findIt(key) 
{ 
    var result = (-1); 
 
    for( var i = 0; i < this.keyArray.length; i++ ) 
    { 
        if( this.keyArray[ i ] == key ) 
        { 
            result = i; 
            break; 
        } 
    } 
    return result; 
} 

function fnReset(){
	document.frmPartAttributeSetup.partnumber.value="";
	document.frmPartAttributeSetup.attributetype.value="101262";
	document.frmPartAttributeSetup.attributevalue.value="";
	fnAttributeVal('101262');
}

function fnSubmit(){
	fnValidateTxtFld('partnumber',message[5256]);
	fnValidateDropDn('attributetype',message[5257]);
	fnValidateDropDn('attributevalue',message[5258]);
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	document.frmPartAttributeSetup.strOpt.value="save";
	fnStartProgress('Y');
	document.frmPartAttributeSetup.submit();
}

function fnAttributeVal(val){
	//var val = TRIM(obj.value);
	document.all.attributevalue.options.length = 0;
	var len = get(val);
	for (var i=0;i<len;i++)
	{
		arr = get(val+i);
		arrobj = arr.split(",");
		codeid = arrobj[0];
		codenm = arrobj[1];
		document.all.attributevalue.options[i] = new Option(codenm,codeid);
		if(selectID == codeid){
			document.all.attributevalue.options[i].selected = true;
		}
	}
}

function fnPageLoad(){
	var attrType = document.frmPartAttributeSetup.attributetype.value;
	if(attrType != 0){
		fnAttributeVal(attrType);
	}
}
function fnClearMessage(){
	var message = document.getElementById("msg_text").innerHTML;
	if(message != undefined){
		document.getElementById("msg_text").innerHTML ="";
	}
}