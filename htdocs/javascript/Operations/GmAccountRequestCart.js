var req;
var trcnt = 0;
var cnt = 0;
var tempPnum = '';

function fnGetSetID()
{
	var setid = '';
	if(document.frmCart.Cbo_Set){
		var setobj = document.frmCart.Cbo_Set;
		setid = setobj.value;
	}	
	return setid;

}

function fnAddToCart()
{ 
	var setid = '';
	if(document.frmCart.Cbo_Set){
		var setobj = document.frmCart.Cbo_Set;
		setid = setobj.value;
	}
	var setindex;
	var reqdate = parent.document.all.requiredDate.value;
	 
	if (setid != 0)
	{

		var url = fnAjaxURLAppendCmpInfo("/GmCartAjaxServlet?hAction=Set&setid=" + encodeURIComponent(setid)); //+ "&qty=" + encodeURIComponent(qty)+ "&acc="+encodeURIComponent(accid)+ "&rep="+encodeURIComponent(repid)+ "&gpoid="+encodeURIComponent(gpoid);
		if (typeof XMLHttpRequest != "undefined") {
		 req = new XMLHttpRequest();
		} else if (window.ActiveXObject) {
		 req = new ActiveXObject("Microsoft.XMLHTTP");
		}
		req.open("GET", url, true);
		req.onreadystatechange = callback;
		
		req.send(null);
		window.parent.Reload(setid, "Loaner", "1");
		//document.frmCart.Txt_PartNum.value = '';

		var setnm = setobj[setobj.selectedIndex].text;
		setindex = setnm.indexOf("-");
		setnm = setnm.substr(setindex+1);

		for (i=0;i<cnt;i++)
		{
			pnumobj = eval("document.all.Lbl_Part"+i);
			
			if (cnt == i+1)
			{
				fnAddRow('PartnPricing');
			}

			if (pnumobj.innerHTML == '&nbsp;')
			{
				pnumobj.innerHTML = setid;
				pdesc = eval("document.all.Lbl_Desc"+i);
				pdesc.innerHTML = setnm;
				obj = eval("document.all.Txt_Qty"+i);
				obj.value = '1';
				reqdt = eval("document.all.Txt_ReqDate"+i);
				reqdt.value = reqdate;
				break;
			}
		}
		
	}
else
	{
		validate();
	}

}

function fnRemoveItem(val)
{	
	var setid = '';
	if(document.frmCart.Cbo_Set){
		setid = document.frmCart.Cbo_Set.value; 
	}	 
	obj = eval("document.frmCart.Txt_Qty"+val);
	obj.value = '';
	
    obj = eval("document.all.Lbl_Desc"+val);
    obj.innerHTML = "&nbsp;";
	obj = eval("document.all.Lbl_Stock"+val);
	obj.innerHTML = "&nbsp;";
	obj = eval("document.all.Lbl_RwStock"+val);
	obj.innerHTML = "&nbsp;";
	obj = eval("document.all.hStock"+val);
    obj.value = '';
	obj = eval("document.all.Txt_ReqDate"+val);
	obj.value = '';
	if(setid!=0)
	{
		window.parent.Reload(setid, "Loaner", "-1");
	}
	
	obj = eval("document.all.Lbl_Part"+val);
	pnum = obj.innerHTML;
	obj.innerHTML = "&nbsp;";
	if(pnum!='')
	{
		window.parent.Reload(pnum, "item", "-1");
	}
	
}

function fnClearCart()
{
	var varRowCnt = document.frmCart.hRowCnt.value;
	for (val=0; val < varRowCnt; val ++)
	{
		obj = eval("document.frmCart.Txt_Qty"+val);
		obj.value = '';
		obj = eval("document.all.Lbl_Part"+val);
		obj.innerHTML = "&nbsp;";
		obj = eval("document.all.Lbl_Desc"+val);
		obj.innerHTML = "&nbsp;";
		obj = eval("document.all.Lbl_Stock"+val);
		obj.innerHTML = "&nbsp;";
		obj = eval("document.all.hStock"+val);
	     obj.value = '';
		obj = eval("document.all.Txt_ReqDate"+val);
		obj.value = '';
	}
}

function validate() 
{
	pnum = document.frmCart.Txt_PartNum.value;
	var conType = parent.document.all.consignmentType.value;
	var setId  = '';
	if(document.frmCart.Cbo_Set){
		setId  = document.frmCart.Cbo_Set.value;
	}	
	if (pnum == '' && conType != '4127')
	{
		Error_Details(message[5087]);
	}
	if(conType == '4127' && setId == 0)
	{
		Error_Details(message[5088]);
	}
	if (ErrorCount > 0)
	 {
		Error_Show();
		Error_Clear();
		return false;
	}
	tempPnum = pnum;
	qty = 1;
	accid = 0;
	repid = 0;
	gpoid = 0;
		
	var url = fnAjaxURLAppendCmpInfo("/GmCartAjaxServlet?pnum=" + encodeURIComponent(pnum)+ "&qty=" + encodeURIComponent(qty)+ "&acc="+encodeURIComponent(accid)+ "&rep="+encodeURIComponent(repid)+ "&gpoid="+encodeURIComponent(gpoid));
	
	if (typeof XMLHttpRequest != "undefined") {
	 req = new XMLHttpRequest();
	} else if (window.ActiveXObject) {
	 req = new ActiveXObject("Microsoft.XMLHTTP");
	}
	req.open("GET", url, true);
	req.onreadystatechange = callback;

	req.send(null);
 	document.frmCart.Txt_PartNum.value = '';  
}
 
function callback() {
    if (req.readyState == 4) {
        if (req.status == 200) {
	        var xmlDoc = req.responseXML;
	        parseMessage(xmlDoc);
        }
    }
}

function parseMessage(xmlDoc) 
{
	
	var pnum = xmlDoc.getElementsByTagName("pnum");
	var data = xmlDoc.getElementsByTagName("data");
	var datalength = data[0].childNodes.length;
	var setid = '';
	if(document.frmCart.Cbo_Set){
		var setobj = document.frmCart.Cbo_Set;
		setid = setobj.value;
	}	
	if(datalength == 0){		
		 setErrMessage(message[5639]); 
		 return;
	}

	var pnumlen = pnum.length;
	var rem = 0;

	for (i=0;i<cnt;i++)
	{
		pnumobj = eval("document.all.Lbl_Part"+i);
		if (pnumobj.innerHTML == '&nbsp;')
		{
			rem++;
		}
	}
	rem = pnumlen - rem;

	for (i=0;i<rem;i++)
	{
		fnAddRow('PartnPricing');
	}
		
	var strresult = '';
	var partindex = '';
	var pdescindex = '';
	var stockindex = '';
	var currdateindex = '';
	var saleFlagIndex='';
	var conspriceindex=-1;
	var rwstockindex = '';

	// This loop is to just get the index from the first part tag (pnum[0])
	
	for (var x=0; x<pnum[0].childNodes.length; x++) 
		{
			xmlstr = pnum[0].childNodes[x].tagName;
	
			if (xmlstr == 'partnum' || xmlstr == 'setid')
			{
				partindex = x;
			} 
			else if (xmlstr == 'pdesc' )
			{
				pdescindex = x;
				
			}
			else if (xmlstr == 'stock' || xmlstr == 'qty')
			{
				stockindex = x;
			}
			else if (xmlstr == 'rwqty')
			{
				rwstockindex = x;
			}
			else if (xmlstr == 'currdate')
			{
				currdateindex = x;
				
			}
			else if (xmlstr == 'rfl')//Release for sale
			{
				saleFlagIndex=x; 
			}
			else if (xmlstr == 'consprice')//part price (Consignment price) 
			{
				conspriceindex = x; 
			}
		}
	//alert("saleFlagIndex "+ saleFlagIndex);
	// This loop is to set all the values as per the index got from the loop
	for (var x=0; x<pnumlen; x++) 
	{
		var pdesc = parseXmlNode(pnum[x].childNodes[pdescindex].firstChild);
		var part = parseXmlNode(pnum[x].childNodes[partindex].firstChild);
		var stock = parseXmlNode(pnum[x].childNodes[stockindex].firstChild);
		var rwstock = '';
		if(pnum[x].childNodes[rwstockindex])
			rwstock = parseXmlNode(pnum[x].childNodes[rwstockindex].firstChild);
		var reqtype = parent.document.all.consignmentType.value;
		var releaseForSale='';
		var Txtobj = eval("document.all.Txt_Qty"+x);
		if(saleFlagIndex>0)
		{
			releaseForSale = parseXmlNode(pnum[x].childNodes[saleFlagIndex].firstChild);
		}
		var currdate='';
		if (currdateindex > 0)
		{
			 currdate = parseXmlNode(pnum[x].childNodes[currdateindex].firstChild);
		}
		var reqtype = parent.document.all.consignmentType.value;
		var releaseForSale='';
		if(setid !='0') 
		{
			if(saleFlagIndex>0)
			 {
				releaseForSale = parseXmlNode(pnum[x].childNodes[saleFlagIndex].firstChild);
			 }
		}
		else
		{
			releaseForSale = parseXmlNode(pnum[x].childNodes[saleFlagIndex].firstChild);
		}
		if (reqtype == 40021 || reqtype == 40025 || reqtype == 102930)// only for the Consignment and ICT , need to check release_for_sale Flag
		{
			if(releaseForSale == 'N'){		
				setErrMessage(message[5640]);
				Txtobj = '';
				return;
		}
	}
		var partprice =0;
		if(conspriceindex >= 0)
	 	{
	 		partprice = parseXmlNode(pnum[x].childNodes[conspriceindex].firstChild);
	 	}else{
	 		partprice = -1;
	 	}
		setMessage(part,pdesc,stock,rwstock,currdate,partprice);
	}
 	if(tempPnum!='')
 	{
 		window.parent.Reload(tempPnum, "item", "1");
 	}
}
 
function setMessage(part,pdesc,stock,rwstock,currdate,partprice) 
{
	var reqtype = parent.document.all.consignmentType.value;
	var reqdate = parent.document.all.requiredDate.value;
	var reqQty = '1';


	var blCon = false;
	if (reqtype == 40021 || reqtype == 40025 || reqtype == 102930)
	{
		blCon = true;
	}

	for (i=0;i<cnt;i++)
	{
		pnumobj = eval("document.all.Lbl_Part"+i);
		obj = eval("document.all.Txt_Qty"+i);
 	    var hStockObj = '';
 	    
		if (pnumobj.innerHTML == '&nbsp;')
		{
			pnumobj.innerHTML = part;
			//pnumobj.value = part;
			pdescobj = eval("document.all.Lbl_Desc"+i);
			stockobj = eval("document.all.Lbl_Stock"+i);
			hStockObj = eval("document.all.hStock"+i);
			rwstockobj = eval("document.all.Lbl_RwStock"+i);
			dateObj = eval("document.all.Txt_ReqDate"+i);
			Lbl_Cal = eval("document.all.Lbl_Cal"+i);
			if (reqtype == 40021 || reqtype == 40025 || reqtype == 102930){
				Lbl_Cal.style.display = 'none';
			}else{
				Lbl_Cal.style.display = '';
			}
			obj.disabled = false;
			obj.style.display = '';
			
			if(partprice==''){
				pdesc = message[5641];
				stock = '';
				rwstock = '';
				obj.disabled = true;
				// If no consignment price then Qty column should be hided, MNTTASK -  2158
				obj.style.display = 'none';
				reqQty = '';
				// If no consignment price then Calendar column should be hided, MNTTASK -  2158
				Lbl_Cal.style.display = 'none';
			}
			
			pdescobj.innerHTML = "&nbsp;" + pdesc;
			stockobj.innerHTML = "&nbsp;" + stock;
 		    hStockObj.value = stock; 
			rwstockobj.innerHTML = rwstock;
			
			obj.value = reqQty;
			
			if (reqtype == 40021 || reqtype == 40025 || reqtype == '102930')
			{
				dateObj.value = '';
			}			
			else if (reqtype == 4127)
			{
				dateObj.value = '';
			}
			else
			{
				dateObj.value = currdate;
			}
			break;

		}
	}
}

function setErrMessage(msg) 
{	
	for (i=0;i<cnt;i++){		
		pnumobj = eval("document.all.Lbl_Part"+i);
		if (pnumobj.innerHTML == '&nbsp;')
		{
			var obj = eval("document.all.Lbl_Desc"+i);
			obj.innerHTML = msg;
			
			obj = eval("document.all.Lbl_Part"+i);
			obj.innerHTML = '&nbsp;';
			obj.focus(); 

			obj = eval("document.all.Lbl_Stock"+i);
			obj.innerHTML = '';
			obj = eval("document.all.hStock"+i);
			obj.value = '';  
			obj = eval("document.all.Lbl_RwStock"+i);
			obj.innerHTML = '';
			break;
		}
	}

}
// To hide the Date Req fields when change request type consignment

function fnRemoveDateReq(){
	 for (val=0; val < cnt; val++){
		 	obj = eval("document.all.Txt_ReqDate"+val);
			obj.value = '';
			obj = eval("document.all.Lbl_Cal"+val);
			obj.style.display = 'none';
	
	 }
	 obj = document.getElementById("date2");
	 obj.style.display = 'none';
	 obj = document.getElementById("tdDateReq"); 
	 obj.style.display = 'none';		
	 document.getElementById("tdReqQty").align = "right"; // when change the request type should be aligned req qty.
}

//To display the Date Req fields  when change request type In-house Consignment or Product Loaner
function fnShowDateReq(){
	 for (val=0; val < cnt; val++) {
			obj = eval("document.all.Lbl_Cal"+val);
			obj.style.display = 'block';
	
	 }
	 obj = document.getElementById("tdDateReq");
	 obj.style.display = 'block';		
	 obj = document.getElementById("date2"); // Date Req Lable
	 obj.style.display = 'block';
	 document.getElementById("tdReqQty").align = "center"; // when change the request type should be aligned req qty. 
}



function fnAddRow(id)
{
	var reqtype = parent.document.all.consignmentType.value;
	var tbody = document.getElementById(id).getElementsByTagName("TBODY")[0];
    var row = document.createElement("TR");

    var td0 = document.createElement("TD");
    td0.innerHTML = Number(cnt)+1;

    var td1 = document.createElement("TD");
    td1.innerHTML = fnCreateCell('IMG','',1);
    
    var td2 = document.createElement("TD");
	td2.id = "Lbl_Part"+cnt;
	td2.innerHTML = "&nbsp;";
   
    var td3 = document.createElement("TD");
	td3.id = "Lbl_Desc"+cnt;
	td3.innerHTML = "&nbsp;";
    td3.align = "Left";

	var td4 = document.createElement("TD");
	td4.id = "Lbl_Stock"+cnt;
	td4.innerHTML = "&nbsp;";
    td4.align = "right";
            
    var td5 = document.createElement("TD");
    td5.innerHTML = fnCreateCell('TXT','Qty',3);
    td5.align = "center";
    
    var td6 = document.createElement("TD");
	td6.innerHTML = fnCreateCell('TXT','ReqDate',9);
	td6.id	= "Lbl_Cal"+cnt;

	var td7 = document.createElement("TD");
	td7.id = "Lbl_RwStock"+cnt;
	td7.innerHTML = "&nbsp;";
    td7.align = "right";
    if(parent.document.all.hDisplayRW.value == 'N'){
    	td7.style.display = "none";
    }else{
    	td7.style.display = "block";
    }

    var drpWareHouse = document.createElement("select");

    
    var oldWareOpt = "warehouseType" + (cnt-1);    
    var selectobject;

    if(document.getElementById(oldWareOpt)){
    	selectobject = document.getElementById(oldWareOpt);
    	drpWareHouse.className  = selectobject.className ;
    }

    for (var k=0; k<selectobject.length; k++){
        var optWareHouse = document.createElement("option");
    	optWareHouse.value = selectobject.options[k].value;
    	optWareHouse.text = selectobject.options[k].text;
        drpWareHouse.add(optWareHouse);
    }
    drpWareHouse.id = "warehouseType" + cnt;
    drpWareHouse.name = "warehouseType" + cnt;
    
	var td8 = document.createElement("TD");
	td8.id = "Lbl_WareHouse"+cnt;
	td8.align = "right";
    if(parent.document.all.hDisplayRW.value == 'Y' && reqtype=='40022'){
    	td8.style.display = "block";
    }else{
    	td8.style.display = "none";
    }
    td8.appendChild(drpWareHouse);
    
    
    row.appendChild(td0);
	row.appendChild(td1);
    row.appendChild(td2);
    row.appendChild(td3);
    row.appendChild(td4);
//    if(parent.document.all.hDisplayRW.value != 'N'){
    	row.appendChild(td7);
    	row.appendChild(td8);
  //  }
    row.appendChild(td5);
    row.appendChild(td6);

    tbody.appendChild(row);
	cnt++;
	document.frmCart.hRowCnt.value = cnt;
	if (reqtype== 40021 || reqtype == 40025)//If the type is consigment,call this function. 
	{
		fnRemoveDateReq();

    }
}

function fnCreateCell(type,val,size)
{
	param = val;
	val = val + cnt;
	var html = '';
	var formatJS = document.frmCart.hformatJS.value;
	if (type == 'TXT')
	{
		if (param == 'Qty') {
				html = '&nbsp;<input type=text size='+size+' name=Txt_'+val+' class=InputArea onFocus=changeBgColor(this,\'#AACCE8\'); onBlur=changeBgColor(this,'+cnt+'); value=\'\'>';
				html = html+ '<input type=hidden name=hStock'+cnt+' value=\'\'>';
						}
		else if (param == 'ReqDate') {
				
				html = '&nbsp;<input type=text size='+size+' name=Txt_'+val+' id=Txt_'+val+' class=InputArea onFocus=changeBgColor(this,\'#AACCE8\'); onBlur=changeBgColor(this,'+cnt+'); value=\'\'>';
				html = html+ ' <img style=cursor:hand onclick=javascript:showSingleCalendar("divTxt_'+val+'","Txt_'+val+'","'+formatJS+'"); title="Click to open Calendar"  src="images/nav_calendar.gif" border=0 align=absmiddle height=18 width=19 maxlength=10/><div id="divTxt_'+val+'" style="position: absolute; z-index: 10;"></div>';
		}
	}
	else if (type == 'IMG')
	{
		var fn = size;
		html = '<a href=javascript:fnRemoveItem('+val+') tabindex=\'-1\';><img border=0 Alt=Remove from cart valign=left src=/images/btn_remove.gif height=10 width=9></a>';
	}
	
	return html;
}


function fnCreateOrderString(){
//	alert(cnt);
	var objpnum = '';
	var pnum = '';	
	var objreqqty = '';
	var reqqty = '';
	var objdate = '';
	var date = '';
	var token = '^';
	var inputstring = '';
	var str = '';
	var objRW = '';
	var objFG = '';
	var rwQty = '';
	var fgQty = '';
	var reqdate = parent.document.all.requiredDate.value;
	var objWareHouse = '';
	var whType = '';
	var reqtype = parent.document.all.consignmentType.value;
	var blCon = false;
	if(parent.strErrorWhType != undefined && parent.strErrorWhType != null ){
		parent.strErrorWhType = '';
	}
	
	if(parent.strErrorRwQty != undefined && parent.strErrorRwQty != null){
		parent.strErrorRwQty = '';
	}
	
	if(parent.strErrorFG != undefined && parent.strErrorFG != null ){
		parent.strErrorFG = '';
	}
	if(parent.strErrorRW != undefined && parent.strErrorRW != null){
		parent.strErrorRW = '';
	}
	if (reqtype == 40021 || reqtype == 40025 || reqtype == 102930)
	{
		blCon = true;
	}

	for (val = 0; val < cnt; val++ )
	{
			whType = '';
			rwQty = '';
			var intFG = 0;
			var intRW = 0;
			objpnum = eval("document.all.Lbl_Part"+val);
			pnum = objpnum.innerHTML;
			objreqqty = eval("document.all.Txt_Qty"+val);			
			reqqty = objreqqty.value;
			objWareHouse = eval("document.all.warehouseType"+val);
			objRW = eval("document.all.Lbl_RwStock"+val);
			objFG = eval("document.all.Lbl_Stock"+val);
			objdate = eval("document.all.Txt_ReqDate"+val);
			
			fgQty = objFG.value;
			
			if (blCon)
			{
				date = reqdate;
			}
			else
			{
				date = objdate.value;
			}
			
			if(reqqty != ''){
				if (reqtype =='4127')
				{ 
					for (i=0;i<reqqty ;i++ )
					{
						str = pnum + token + '1' + token + date +  '|';
						inputstring = inputstring + str;
					}

				}
				else{
					if(reqtype =='40022'){
						if(objWareHouse){
							whType = (objWareHouse.value == '0')?'0':(objWareHouse.value == '2')?'56001':'90800'; 
						}

						if(objRW){ 
							rwQty  = parseInt(objRW.innerHTML);
						}
							
						
						if(whType == '0'){
							if(parent.strErrorWhType != undefined && parent.strErrorWhType != null )
								parent.strErrorWhType += ", " + pnum;
						}

						if(whType == '56001' && (reqqty > rwQty)){
							if(parent.strErrorRwQty != undefined && parent.strErrorRwQty != null) 
								parent.strErrorRwQty += ", " + pnum;
						}
						
						for(var k=0;k<cnt;k++){
							var PnumObj = eval("document.all.Lbl_Part"+k);
							var whObj = eval("document.all.warehouseType"+k);
							var whtypes ='90800';
							if(whObj){
								whtypes = (whObj.value == '0')?'0':(whObj.value == '2')?'56001':'90800';
							}
							var strPnum = PnumObj.value; 
							if(pnum == strPnum){
								if(whtypes == '90800')
									intFG += reqqty;
								if(whtypes == '56001')
									intRW += reqqty;
							}
						}
						
						if(intFG > fgQty ){
							if(parent.strErrorFG != undefined && parent.strErrorFG != null ){
								var _pnum = "," + pnum + "," ;
								if(parent.strErrorFG.indexOf(_pnum)==-1){
									parent.strErrorFG += "," + pnum + ",";
								}
							}
						}
						
						if(intRW > rwQty){
							if(parent.strErrorRW != undefined && parent.strErrorRW != null){
								var _pnum = "," + pnum + "," ;
								if(parent.strErrorRW.indexOf(_pnum)==-1){
									parent.strErrorRW += "," + pnum + ",";
								}
							}
						}
						
					}
					str = pnum + token + reqqty + token + date + '^' + whType +  '|';
					inputstring = inputstring + str;
				}				
			}			
	}
	return inputstring;
}

function fnOpenPart()
{
	pnum = document.frmCart.Txt_PartNum.value;
	//windowOpener("/GmSearchServlet?hAction=SearchPart&Txt_PartNum="+pnum+"&hCount="+varCount+"&Txt_AccId="+varAccId,"Search","resizable=yes,scrollbars=yes,top=340,left=420,width=600,height=435");
	windowOpener("/GmSearchServlet?hAction=SearchPart&Txt_PartNum="+encodeURIComponent(pnum),"Search","resizable=yes,scrollbars=yes,top=340,left=420,width=600,height=435");
}

function copyDate(){
	chkbox = document.all.datechk;
	date = document.all.Txt_ReqDate0.value;	
		for (val = 1; val < cnt; val++ )
		{
			objdate = eval("document.all.Txt_ReqDate"+val);		
			pnumobj = eval("document.all.Lbl_Part"+val);			
				if (chkbox.checked == true){
					if (date!='' && pnumobj.innerHTML != '&nbsp;' )
					objdate.value = date;			
				}
				else				
					objdate.value = '';
				
		}

}
//code merge from OUS side
function fnGetAddInfo(){
	
	return document.frmCart.Txt_AddInfo.value;
}