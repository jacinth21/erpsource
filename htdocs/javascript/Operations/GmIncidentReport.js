function fnLoadIncidentVal(){
	document.frmOprIncident.strOpt.value = "loadIncidentVal";
	document.frmOprIncident.submit();
}
function fnSubmit(){
	// Checking for Empty date Fields
	fnValidateTxtFld('fromDate',message_operations[144]);
	fnValidateTxtFld('toDate',message_operations[145]);
	
	// Checking for valid date formations like month:1-12 day:1-31
	CommonDateValidation(document.frmOprIncident.fromDate,format,Error_Details_Trans(message[10523],format));
	CommonDateValidation(document.frmOprIncident.toDate,format,Error_Details_Trans(message[10524],format));
	
	// Checking for Valid date range. means To date always greater than or equals to From date.
	var fromToDiff = dateDiff(document.frmOprIncident.fromDate.value, document.frmOprIncident.toDate.value,format);	
	if(fromToDiff < 0){
		Error_Details(message[10525]);
	}
	
	if(ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}
	document.frmOprIncident.strOpt.value = "load";
	fnStartProgress();
	document.frmOprIncident.submit();
	
}

function fnSelect(id){
	alert('InProgress');
}

function fnSelectAll(varCmd){
	objSelAll = document.all.selectAll;
	if(objSelAll){
		objCheckIncidents = document.all.selectedIncidents;
      //objCheckSiteArrLen = objCheckSiteArr.length;
		if((objSelAll.checked || varCmd == 'selectAll') && objCheckIncidents){
			objSelAll.checked = true;
			objCheckIncArrLen = objCheckIncidents.length;
				for(i = 0; i < objCheckIncArrLen; i ++){	
					objCheckIncidents[i].checked = true;
				}
		}else if(!objSelAll.checked  && objCheckIncidents ){
			objCheckIncArrLen = objCheckIncidents.length;
			for(i = 0; i < objCheckIncArrLen; i ++){	
				objCheckIncidents[i].checked = false;
			}
		}
	}
}
