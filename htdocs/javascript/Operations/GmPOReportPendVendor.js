var gObj;
var projId, projIdExl, logId, daysIdExl, daysId, partInv;

function fnLoad()
{
	//fnOnPageLoad();
	document.frmAccount.Cbo_Type.value = strType;
	if (objGridData.value != '') {
		gridObj = initGridData('PoPendVenRpt', objGridData);		
		gridObj.enableHeaderMenu();
		
		gridObj.attachHeader("#rspan,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#select_filter"
				 +",#select_filter,#text_filter,#text_filter,#text_filter,#text_filter,#numeric_filter,#numeric_filter,#numeric_filter,#numeric_filter,#text_filter"
				 +",#text_filter,#text_filter,#select_filter,#rspan");
		//gridObj.enableAutoHeight(true);
		
		// to set auto height
        if($(document).height() !=undefined || $(document).height() !=null){
            gridHeight = $(document).height() - 275;
        }
		
        gridObj.enableAutoHeight(true, gridHeight, true);
		gridObj.objBox.style.overflowX = "hidden";
		
	}
}

//This function used to download the grid data
function fnDownloadXLS() {
	projIdExl = gridObj.getColIndexById("projIdExl");
	logId = gridObj.getColIndexById("logId");
	daysIdExl = gridObj.getColIndexById("daysIdExl");
	daysId = gridObj.getColIndexById("daysId");
	partInv = gridObj.getColIndexById("partInv");
	
	gridObj.detachHeader(1);
	gridObj.setColumnHidden(partInv,true);
	gridObj.setColumnHidden(logId,true);
	gridObj.setColumnHidden(daysIdExl,false);
	gridObj.setColumnHidden(daysId,true);
	gridObj.toExcel('/phpapp/excel/generate.php');
	gridObj.attachHeader("#rspan,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#select_filter"
			 +",#select_filter,#text_filter,#text_filter,#text_filter,#text_filter,#numeric_filter,#numeric_filter,#numeric_filter,#numeric_filter,#text_filter"
			 +",#text_filter,#text_filter,#select_filter,#rspan");
	gridObj.setColumnHidden(partInv,false);
	gridObj.setColumnHidden(logId,false);
	gridObj.setColumnHidden(daysIdExl,true);
	gridObj.setColumnHidden(daysId,false);

}

function initGridData(divRef,gridData)
{
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/extweb/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableMultiline(false);	
	//gObj.enableDistributedParsing(true); The screen should load completely.
	gObj.init();
	gObj.loadXMLString(gridData);
	return gObj;
}


function fnOpenLog(id)
{
	windowOpener("/GmCommonLogServlet?hType=1232&hID="+id+"&companyInfo="+companyInfoObj,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
}

function fnOpenMainInvRpt(val){ 
	windowOpener('/GmPartInvReportServlet?hAction=Go&subComponentFl=Y&hPartNum='+encodeURIComponent(val),"Pack","resizable=yes,scrollbars=yes,top=250,left=250,width=1000,height=510");
}

function fnGo()
{
		
	document.frmAccount.hAction.value = 'Go';
	document.frmAccount.hOpt.value = 'PendVend';	
	document.frmAccount.action = strServletPath+'/GmPOReportServlet';
	fnStartProgress();
	document.frmAccount.submit();
}
function fnCallOpenPO(po)
{
	document.frmAccount.hAction.value = 'LoadPO';
	document.frmAccount.Txt_PO.value = po;
	document.frmAccount.hType.value = 'DUMMY';
	document.frmAccount.action = strServletPath+'/GmPOReceiveServlet?companyInfo='+companyInfoObj;
	document.frmAccount.submit();
}
//PMT-32321 Expand All/ Collapse all functionality in Pending - By Vendor screen
/*function fnExpColl()
{
	var imgObj = eval("document.all.PendPOImg");
	fnStartProgress('Y');
	setTimeout(function(){
	// Change the image style
	if(expandNum%2==0){
		if(imgObj){
			imgObj.src = '/images/minus.gif';
			gridObj.expandAllGroups();
		}
	}else{
		if(imgObj){
			imgObj.src = '/images/plus.gif';
			gridObj.collapseAllGroups();
		}
	}
	fnStopProgress();
	expandNum++;
	},25);
}*/
