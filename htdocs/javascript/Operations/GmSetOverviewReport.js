function fnGo()
 { 
	var inputString="";
	inputString=fnCheckSelections();
    
  if (ErrorCount > 0)
		 {
			Error_Show();
			Error_Clear();
			return false;
		}
  
  document.frmLogisticsReport.hAction.value = "GO";
  document.frmLogisticsReport.hTxnId.value = inputString;
  document.frmLogisticsReport.action = "/gmLogisticsReport.do?method=loadSetOverview"; 
  document.frmLogisticsReport.strOpt.value="load";
  document.frmLogisticsReport.hExcel.value="";
  fnStartProgress('Y');  
  document.frmLogisticsReport.submit();
 }
 
function fnCheckSelections()
{
	 var chkdvals=""; ;
	 var unchkdvals ="";
	 var chk=0;	
	 var notchk=0;
	 var inputString="";

	objCheckArrLen = 0;
	 
	flag = '';
	 
	objCheckArr = document.frmLogisticsReport.checkSelectedSets;
	if(objCheckArr) {
	 
		objCheckArrLen = objCheckArr.length;
		for(i = 0; i < objCheckArrLen; i ++) 
			{	
			    if(objCheckArr[i].checked==true)
			    {
					flag = '1';
					chkdvals += objCheckArr[i].value+',';			
					chk ++;			
					
			    }else
			    {
			    	unchkdvals += objCheckArr[i].value+','	;
					notchk ++;
			    }

			   
			}
		 if(chk<=0)
		 {
			 Error_Details(message_operations[307]);
		 }

		 

		 
		 if(parseInt(objCheckArrLen)==parseInt(chk))
		 {
			 inputString="ALL|";
		 }else
		 {
			 if(parseInt(notchk)<parseInt(chk))
			 {
				 inputString="UNCHECKED|"+unchkdvals;
				 inputString=inputString.substring(0,inputString.length-1);
				 
			 }else
			 {
				 inputString="CHECKED|"+chkdvals;
				 inputString=inputString.substring(0,inputString.length-1);
			 }
		 }
		 

		 if(inputString.length>4000)
		 {
			 Error_Details(message_operations[308]);	
		 }
		 
	}
	
	 
	if(flag=='')
	{
	Error_Details(message_operations[309]);
	}
	return inputString;
}	

 

 function fnSelectAll(varCmd)
{
				objSelAll = document.all.selectAll;
				if (objSelAll ) {
				objCheckSheetArr = document.all.checkSelectedSets;
//				objCheckSiteArrLen = objCheckSiteArr.length;
				
					
				if ((objSelAll.checked || varCmd == 'selectAll') && objCheckSheetArr)
				{
					objSelAll.checked = true;
					objCheckSheetArrLen = objCheckSheetArr.length;
					
					objCheckSheetArr1 = document.frmLogisticsReport.checkSelectedSets;
					if (objCheckSheetArr1.type == 'checkbox')
						{
							objCheckSheetArr1.checked = true;
						}
													
					for(i = 0; i < objCheckSheetArrLen; i ++) 
					{	
						objCheckSheetArr[i].checked = true;
					}
				 
				 
				}
				else if (!objSelAll.checked  && objCheckSheetArr ){
					objCheckSheetArrLen = objCheckSheetArr.length;
					
					objCheckSheetArr1 = document.frmLogisticsReport.checkSelectedSets;
					if (objCheckSheetArr1.type == 'checkbox')
						{
							objCheckSheetArr1.checked = false;
						}
					
					for(i = 0; i < objCheckSheetArrLen; i ++) 
					{	
						objCheckSheetArr[i].checked = false;
					}
			
					}
				}
			 	 
	}
	



		function fnHouseLoaner(id)
		{
			windowOpener("/GmInHouseSetServlet?Chk_GrpSet="+id+"&hAction=Reload&hOpt=4119&hSetIds="+id,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=925,height=300");		
		}
		
		function fnProductLoaner(id)
		{
			windowOpener("/GmInHouseSetServlet?Chk_GrpSet="+id+"&hAction=Reload&hOpt=4127&hSetIds="+id,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=925,height=300");		
		}
		
		function fnAvgNet(id)
		{ 
 			windowOpener("/GmConsignSearchServlet?Chk_GrpSet="+id+"&Cbo_Category=20210&Cbo_Status=20230&Cbo_Type=20200&hAction=Reload&hOpt=BYSET&hSetIds="+id+"&Txt_FrmDt="+fromDayDate+"&Txt_ToDt="+todayDate,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=925,height=500");		 
		}
		
		function fnSetStatus(id,reqstatus,constatus)
		{
			windowOpener("/gmRequestMaster.do?setID="+id+"&setOrPart=50636&mainOrChild=50638&teamId=0&strOpt=reload&hCancelType=VDREQ&requestFor=0&requestSource=0&requestStatus="+reqstatus+"&conStatusFlag="+constatus+"&requestTo=0&requestTxnID=0","PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=1160,height=500");		
		}
		
		function fnSetStatusShipped(id,reqstatus,constatus)
		{
			windowOpener("/gmRequestMaster.do?setID="+id+"&setOrPart=50636&mainOrChild=50638&strOpt=reload&hCancelType=VDREQ&requestFor=0&requestSource=0&requestStatus="+reqstatus+"&conStatusFlag="+constatus+"&requestTo=0&requestTxnID=0&hshipFromDate="+fromDaysDate+"&hshipToDate="+todaysDate,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=1160,height=500");		
		}
		
		function fnForecastMon(refid,month)
		{
			windowOpener("/gmDSSummary.do?strOpt=setsheetreport&refId="+refid+"&month="+month,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=960,height=300");		
		}
		function fnDownloadVer()
		{
			var inputString="";
			inputString=fnCheckSelections();
		    
			  if (ErrorCount > 0)
					 {
						Error_Show();
						Error_Clear();
						return false;
					}
			  
			 
			document.frmLogisticsReport.hTxnId.value = inputString;
			document.frmLogisticsReport.hExcel.value='Excel';
			document.frmLogisticsReport.strOpt.value="load";
			document.frmLogisticsReport.action = "/gmLogisticsReport.do?method=loadSetOverview"; 
 			document.frmLogisticsReport.submit();
 		
}