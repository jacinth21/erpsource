	var cnt = 0;	
	function fnLoad(){
		var distName 		= document.frmGmMissingChargesDtl.distName.value;
		var repName  		= document.frmGmMissingChargesDtl.repName.value;
		var requestID 		= document.frmGmMissingChargesDtl.requestID.value;
		var reconCommnets 	= document.frmGmMissingChargesDtl.reconCommnets.value;
		
		objStartDt = document.frmGmMissingChargesDtl.fromDate;
		objEndDt = document.frmGmMissingChargesDtl.toDate;
		var dateDiffVal = dateDiff(todaysDate,objStartDt.value,format);
		if(objStartDt.value != "" && objEndDt.value == ""){
			Error_Details(message[10035]);
		}
		if(objEndDt.value != "" && objStartDt.value == ""){
			Error_Details(message[10035]);	
		}	
		if(objStartDt.value != ""){
			//DateValidateMMDDYYYY(objStartDt, '<b>From Date - </b>'+ message[1]);
			CommonDateValidation(objStartDt,format, Error_Details_Trans(message[10002],format));
		}
		if(objEndDt.value != ""){
			
			//DateValidateMMDDYYYY(objEndDt, '<b>To Date - </b>'+ message[1]);
			CommonDateValidation(objEndDt,format,Error_Details_Trans(message[10002],format));
		}
		var dtFrom = document.frmGmMissingChargesDtl.fromDate.value;
	    var dtTo = document.frmGmMissingChargesDtl.toDate.value;
	    
		if(dtFrom != '' || dtTo != ''){	
			if(dateDiff(dtFrom, dtTo, format) < 0){
				Error_Details(message[10041]);
			}			
		}
		if((distName == '0') && (repName == '0') && (requestID == '') && (reconCommnets == '0') && (objStartDt.value == '') && (objEndDt.value == '')){
			Error_Details(message[10042]);
		}
		if (ErrorCount > 0)
			{
				Error_Show();
				Error_Clear();
				return false;
			}

		document.frmGmMissingChargesDtl.strOpt.value = "LoadReport";	
		document.frmGmMissingChargesDtl.action = '/gmMissingChargesDtlAction.do?companyInfo='+strCompanyInfo;
		document.frmGmMissingChargesDtl.Btn_Load.disabled = true;
		fnStartProgress();
		document.frmGmMissingChargesDtl.submit();
	}

	function fnCopyDate(){
		if(vCountryCode == 'en')
			showCalendar('divDate','%m/%d/%Y');
		else
			showCalendar('divDate','%d/%m/%Y');
	}
	
    function showCalendar(calendardiv,dateformat){
    	// This method using for set style. Because some JSP using RightTableCaption class and there bold,right align used.
    	// It should not display with right alignment and bold. So we called this method to date-pciker.js
    	onSetStyleToCalendar(calendardiv);
    	
		if (dateformat == null){
			dateformat='%m/%d/%Y';
		}	
		if(!calendars[calendardiv])
			calendars[calendardiv]= initCalendar(calendardiv,dateformat);

		//This line for today date will display in BOLD and this method will be in dhtmlxcalendar.js
		calendars[calendardiv].enableTodayHighlight(true); // or false
		
		if(calendars[calendardiv].isVisible())
			calendars[calendardiv].hide();
		else 
			calendars[calendardiv].show();
	}
	
 // The initCalendar method written for DHTMLX 3.5 new version 
	function initCalendar(calendardiv,dateformat){	
		//Below code used for dhtmlx 3.5 new version. In old dhtmlx version we can't edit month and year in IPAD. Now we can edit.
		mCal = new dhtmlXCalendarObject(calendardiv);
		
		// This range only able to select date. 
		mCal.setSensitiveRange("1970-01-01", "2500-31-12"); 

		// Set date format based on login user.
		mCal.setDateFormat(dateformat);

		// Hide the time in new dhtmlx version 3.5
		mCal.hideTime(); 
	
		// Changed the below code for new version DHTMLX. When click calendar icon and set the date in textbox.
		mCal.attachEvent("onClick", function(date) {
			calendars[calendardiv].hide();
			var val = 0;
			var objdate = '';
			var cnt = document.getElementsByName("deductiondate").length;		
			for (val = 0; val <cnt; val++ ){
				objdate = eval("document.all.deductiondt"+val);
						if ((mCal.getFormatedDate(dateformat, date)!='')){
							objdate.value = mCal.getFormatedDate(dateformat, date);
						}
			}
			
			return true;
		 });
		mCal.hide();
		return mCal;
	}


/* The below code commented PMT-2885 DHTMLX 3.5 new version changes from date picker. Here changes for
	Missing changes Details only and using above method. */
/*
 function initCalendar(calendardiv,dateformat){	
		if((navigator.userAgent.match(/iPhone/i))!=null || (navigator.userAgent.match(/iPad/i))!=null){
			mCal = new dhtmlxCalendarObject(calendardiv, false, {
			isMonthEditable: false,
			isYearEditable: false });
		}else{
			mCal = new dhtmlxCalendarObject(calendardiv, false, {
			isMonthEditable: true,
			isYearEditable: true});
		}
   
	mCal.setYearsRange(1970, 2500);
	mCal.draw();
	mCal.disableIESelectFix(true);
	mCal.setDateFormat(dateformat);
	mCal.setOnClickHandler(function(date,obj,type){
		this.hide();
		var val = 0;
		var cnt = document.getElementsByName("deductiondate").length;		
		for (val = 0; val <=cnt; val++ ){
			objdate = eval("document.all.deductiondt"+val);			
					if ((this.getFormatedDate(dateformat, date)!='')){
						objdate.value = this.getFormatedDate(dateformat, date);			
					}
		}		
		return true;
	});
	mCal.hide();
	return mCal;
}*/
	
	function fnTicket(ticketurl){
		windowOpener(ticketurl,"jiraticket","resizable=yes,scrollbars=yes,top=250,left=300,width=840,height=700");
	}
	
	function fnPrintDetails(requestid){
		windowOpener("/gmOprLoanerReqEdit.do?requestId="+requestid+"&strOpt=print&txnType=4127","LoanerRequest","resizable=yes,scrollbars=yes,top=250,left=300,width=840,height=700");
	}
	
	function fnSetEditCharge(rowid,charge){
		var chkststus = eval("document.all.chk_rowwise"+rowid).checked;
		var chargevalue = eval("document.all.editCharge"+rowid).value;	
		var chargeQty = eval("document.all.chargeQty"+rowid).value;
		var preeditcharge = eval("document.all.preeditcharge"+rowid).value;
		charge = charge * chargeQty;
		chargevalue = preeditcharge;
		var selectAllStst = document.all.selectall.checked;		
			if(chkststus){
				if(chargevalue == 'null'){
					eval("document.all.editCharge"+rowid).value = charge;	
				}else{				
					eval("document.all.editCharge"+rowid).value = chargevalue;					
				}
				cnt++;
			}else{				
				eval("document.all.editCharge"+rowid).value = '';				
				cnt--;
				document.all.selectall.checked = false;			
			}				
		if(cnt == arraySize){
			document.all.selectall.checked = true;
		}
		
	}	
	
	function fnOpenLog(requestid,type){
		windowOpener("/GmCommonLogServlet?hType="+type+"&hID="+requestid,"ReconCommnetLog","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=300");
	}
	
	function fnSubmit(strOpt){
		console.log("strOpt======="+strOpt);
		var inputString = "";
		var chargeId = "";
		var transaid = "";
		var setID     = "";
		var requestID = "";
		var deductDt   = "";
		var reconcommnets = "";
		var editcharge = "";
		var distID = "";
		var repID = "";
		var missQty = "";
		var chargeQty = "";
		var unitcost = "";
		var partnum = "";
		var charge = "";
		var invalidCharge = "";
		var ecxcessCharge = "";
		var assocrepid = "";
		for (var i=0; i<arraySize; i++){
			var chkststus = eval("document.all.chk_rowwise"+i).checked;
			if(chkststus){
				if(eval("document.all.chargeid"+i)){
					 chargeId = eval("document.all.chargeid"+i).value;
				}
				if(eval("document.all.transaid"+i)){
					transaid = eval("document.all.transaid"+i).value;
				}
				if(eval("document.all.setid"+i)){
					setID    = eval("document.all.setid"+i).value;
				}
				if(eval("document.all.requestID"+i)){
					requestID = eval("document.all.requestID"+i).value;
				}
				if(eval("document.all.deductiondt"+i)){
					deductDt   = eval("document.all.deductiondt"+i).value;
				}
				if(eval("document.all.cbo_reconcommnets"+i)){
					reconcommnets = eval("document.all.cbo_reconcommnets"+i).value;
				}
				if(eval("document.all.editCharge"+i)){
					editcharge = eval("document.all.editCharge"+i).value;
				}
				if(eval("document.all.distid"+i)){
					distID = eval("document.all.distid"+i).value;
				}
				if(eval("document.all.repid"+i)){
					repID = eval("document.all.repid"+i).value;
				}
				if(eval("document.all.assocrepid"+i)){
					assocrepid = eval("document.all.assocrepid"+i).value;
				}
				if(eval("document.all.missQty"+i)){
					missQty = eval("document.all.missQty"+i).value;
				}
				if(eval("document.all.chargeQty"+i)){
					chargeQty = eval("document.all.chargeQty"+i).value;
				}
				if(eval("document.all.listprice"+i)){
					unitcost = eval("document.all.listprice"+i).value;
				}
				if(eval("document.all.partnum"+i)){
					partnum = eval("document.all.partnum"+i).value;
				}
				if(eval("document.all.rowcharge"+i)){
					charge = eval("document.all.rowcharge"+i).value;
				}		
				charge = chargeQty * charge;
				
				if(editcharge > charge){					
					ecxcessCharge = ecxcessCharge +"<br>"+ partnum;	
				}
				var objRegExp = /(^-?\d*$)/;
				console.log("invalidCharge===b4=if==="+invalidCharge);
				if((editcharge < 0 ) || (!objRegExp.test(editcharge))){
					invalidCharge = invalidCharge +"<br>"+ partnum;	
					console.log("invalidCharge===inside=if==="+invalidCharge);
				}
				var status = "";
				var creditEmount = "";
				inputString = inputString + chargeId + "^" + transaid + "^" + setID + "^" + requestID + "^" + deductDt + "^" + reconcommnets + "^" + editcharge + "^" + distID + "^" + repID + "^" + missQty + "^" + chargeQty + "^" + unitcost + "^" + partnum+"^"+ status +"^"+assocrepid+"^"+ creditEmount +"|";
			}
		}
		if(inputString == ''){
			Error_Details(message[10043]);
			Error_Show();
			Error_Clear();
			return false;
		}
		if (ecxcessCharge != ''){
			Error_Details(Error_Details_Trans(message[10044],ecxcessCharge));
			Error_Show();
			Error_Clear();
			return false;
		}
		console.log("invalidCharge===validation==="+invalidCharge);
		if (invalidCharge != ''){
			Error_Details(Error_Details_Trans(message[10045],invalidCharge));
			Error_Show();
			Error_Clear();
			return false;
		}
		document.frmGmMissingChargesDtl.inputString.value =  inputString;
		document.frmGmMissingChargesDtl.strOpt.value = strOpt;
		document.frmGmMissingChargesDtl.action = url+'?companyInfo='+strCompanyInfo;
		fnStartProgress('Y');
		document.frmGmMissingChargesDtl.submit();			
		eval("document.frmGmMissingChargesDtl.Btn_"+strOpt).disabled = true;	
		
	}	
	
	function fnSelectAll(obj){
	    var objId;
		for(var n=0;n<arraySize;n++){
			objId = eval("document.all.chk_rowwise"+n); // The checkboxes would be like rad0, rad1 and so on
			var charge = eval("document.all.rowcharge"+n);
				if(objId){			
					if(obj.checked){
						objId.checked = true;
						fnSetEditCharge(n,charge.value);
					}else{
						objId.checked = false;
						fnSetEditCharge(n,charge.value);
					}
				}
		 }		
	}
	//This function is used to send a mail to sales rep 
	function fnprocessLateFeeEmail(){
		var inputString = "";
		var chargeId = "";
		for (var i=0; i<arraySize; i++){
			var chkststus = eval("document.all.chk_rowwise"+i).checked;
			
				if(chkststus){
					if(eval("document.all.chargeid"+i)){
						chargeId = eval("document.all.chargeid"+i).value;
						console.log("chargeId===mail==="+chargeId);
					}
					// form the below input string only if chargeId is not null or empty
				inputString = inputString + chargeId + "," ;
				console.log("inputString==1=mail==="+inputString);
				}
		}
		inputString = inputString.substring(0,inputString.length-1);
		console.log("inputString=2==mail==="+inputString);
		if(inputString == ''){
			Error_Details(message[10043]);
			Error_Show();
			Error_Clear();
			return false;
		} else if(inputString != '0'){
			fnStartProgress('Y');			
			var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmMissingChargesDtlAction.do?strOpt=sendLoanerLateFeeEmail&strInputString='+inputString+'&ramdomId='+Math.random());
			dhtmlxAjax.get(ajaxUrl,function(loader){
				var strJsonString =loader.xmlDoc.responseText;	
				fnStopProgress();			
			});	
		}else{
		Error_Details(message[10626]);
			Error_Show();
			Error_Clear();
			return false;
		
		}	  
	}

