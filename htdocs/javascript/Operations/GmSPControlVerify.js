var gridObj = '';
var errparts ;
var tissueControls = '';
function fnSubmit(){
	tissueControls = '';
	hcnt = parseInt(document.frmVendor.hCnt.value);
	hcnt = hcnt+cnt;
	var errCtrlNum = "";
	document.frmVendor.hCnt.value = hcnt;
	var type = document.frmVendor.hType.value;
	Error_Clear();
	var strErrorMore = '';
	var strErrorLess = '';
	var strErrorNumeric = '';
	var errorTissueParts = "";
	document.frmVendor.hAction.value = 'SaveControl';
	if (document.frmVendor.hAction.value == 'SaveControl' ){
		var theinputs = document.getElementsByTagName("input");
	    var arr = document.frmVendor.pnums.value.split(',');
	    var blk = '';
	    var cnum = '';
	    var pnum = "";
	    var pnumMaterial = "";
	    var qtycnt = 0;
	    hcnt++;
	    for(var i=0;i<arr.length;i++){
  	       qid = arr[i];
  	     // For remove special characters in Part Numbers	
  	     pnum1 = replacePartSpecialchars(qid);   
	        blk = eval("document.frmVendor.hQty"+pnum1);
			   for(var j=0;j<hcnt;j++) {
			       pnumobj = eval("document.frmVendor.hPartNum"+j);
				if (pnumobj){
						if (pnumobj.value == qid){
							 qtyobj = eval("document.frmVendor.Txt_Qty"+j);
							 cnumobj = eval("document.frmVendor.Txt_CNum"+j);
							 cnum = TRIM(cnumobj.value.toUpperCase());	 
							 document.getElementById("Txt_CNum"+j).value = cnum;// to set converted uppercase control number back to screen	
							 pnum = TRIM(pnumobj.value);						
							 if(cnum == "" && document.getElementById("Chk_ShipFl").checked){
								 if(qtyobj.value !=0)
									 errCtrlNum += (errCtrlNum.indexOf(pnum) != -1)? "":", "+pnum;
							  }
							 
							// Validation for not allowing Scanning same COntrol Number Twice for Tissue Parts
							 if(document.getElementById("hPartMaterialType"+i) != undefined){
									var pnumMaterialObj = document.getElementById("hPartMaterialType"+i);
									pnumMaterial = pnumMaterialObj.value;
									}			
								var errparts = fnValidateAllControlNo(pnum,cnum,qtyobj.value,pnumMaterial);		
								if(errparts != ''){
									//errorTissueParts +=  ", <BR>"+errparts;
									errorTissueParts = errorTissueParts + "<br>" +errparts;
								}
							  							
							if(!isNaN(qtyobj.value)){
								qty = parseInt(qtyobj.value);
								cnum = TRIM(cnumobj.value.toUpperCase());
								if (qty > 0){
									qtycnt = qtycnt + qty;
								}
							}
							else{
								strErrorNumeric = strErrorNumeric + ","+ qid;
								Error_Details(Error_Details_Trans(message[10085],strErrorNumeric.substring(1, strErrorNumeric.length)));
							}
						}
					} // IF Valid object
					
			
				} // END of Inner FOR loop
		 	 
			if (qtycnt > parseInt(blk.value) ){
				strErrorMore  = strErrorMore + ","+ qid;
			}
			if (qtycnt < parseInt(blk.value) ){
				strErrorLess  = strErrorLess + ","+ qid;
			}
			qtycnt = 0;
		}	
	    if (document.frmVendor.hStatusFl.value == '2' && document.frmVendor.Chk_ShipFl.checked == true){
			document.frmVendor.Chk_ShipFl.value = '3'; // submit for verification
			document.frmVendor.hAction.value = 'ReleaseControl';
		}
        else if (document.frmVendor.hStatusFl.value == '3' && document.frmVendor.Chk_ShipFl.checked == true){
			document.frmVendor.Chk_ShipFl.value = '4'; // submit to close
		}
		else{
			document.frmVendor.Chk_ShipFl.value = '2'; // Still entering Control #'s
			errCtrlNum = "";
		}
	    if(errCtrlNum != ""){          
			Error_Details(Error_Details_Trans(message[10086],errCtrlNum.substring(1, errCtrlNum.length)));
		}
	    if(errorTissueParts != ""){
			Error_Details(Error_Details_Trans(message[10005],errorTissueParts));
		}
		if (strErrorMore != ''){
			Error_Details(Error_Details_Trans(message[10087],strErrorMore.substr(1,strErrorMore.length)));
		}
		if (strErrorLess != ''){
			Error_Details(Error_Details_Trans(message[10088],strErrorLess.substr(1,strErrorLess.length)));
		}
		if (ErrorCount > 0){
			Error_Show();
			Error_Clear();
			return false;
		}
	 }	
	 
 		//if(type == '50157' || type == '50158'|| type == '50155'|| type == '50160' || type == '50161' || type == '50162'||type == '4112'||type == '40053')
 		//{
			if(document.frmVendor.Chk_ShipFl.checked == true)
			{
				document.frmVendor.txnStatus.value = 'VERIFY';
			}
			//if(vIncludeRuleEng != 'NO'){
				//document.frmVendor.action = '/gmRuleEngine.do'; 
			//}
		//}
		document.frmVendor.chkRule.value = 'Yes';
	 	fnStartProgress('Y');
	 	document.frmVendor.submit();
}


function fnDelete(pnum,val,scanned)
{
	objTxtQty = eval("document.all.Sp"+val);
	objCnum = eval("document.all.SpC"+val);
	objTxtQty.innerHTML = "";
	objCnum.innerHTML = "";
	fnDeleteScanned(pnum,scanned); 
	
}

function fnInitiate()
{
	document.frmVendor.hAction.value = 'Initiate';
  	document.frmVendor.submit();
}

function fnVerify()
{
 	fnStartProgress('Y');

	document.frmVendor.hAction.value = 'Verify';
	var type = document.frmVendor.hType.value;
	//if(type == '50157' || type == '50158' || type == '50155' || type == '50160' ||type == '50161' || type == '50162'|| type == '4112'||type == '40053'||type == '40051'||type == '40052')
 		//{	
	if(vIncludeRuleEng != 'NO'){
			document.frmVendor.txnStatus.value = 'VERIFY';
			document.frmVendor.action = '/gmRuleEngine.do';
	}
		//}
	if(document.frmVendor.Chk_CreateShipFl){
		if(document.frmVendor.Chk_CreateShipFl.checked==true)
		{
			document.frmVendor.Chk_CreateShipFl.value="Y";
		}
	}
	//removed condition (50152) for PC-4273 change
	if (type == '50161' || type == '50157')
	{
		var newQty=0;
		var updatedQty = 0;
		var pnum='';
		var pnumTemp = '';
		var cnum = '';
		var partQty = 0;
		var pnumStr = '';
		var lcnId ='';
		var pnumLcnStr ='';
		var vmaxqty = 0;
		var newLcn = '';
		var markedQtyStr = '';
		var dispstr = '';
		var flg = false;
		var gridrows =gridObj.getAllRowIds(",");
		if(gridrows.length>0)
		{
			var gridrowid;
			var gridrowsarr = gridrows.toString().split(",");
			var columnCount = gridObj.getColumnsNum();
			
			var i = 0;
			
			 for (var rowid = 0; rowid < gridrowsarr.length; rowid++) 
			 {
				gridrowid = gridrowsarr[rowid];
				pnum = gridObj.cellById(gridrowid, 3).getValue();
				i++;
				if (pnumStr.indexOf(pnum) != -1)
				{
					continue;
				}
				pnumStr = pnumStr + pnum + ',';
				partQty = 0;
				updatedQty = 0;
				markedQtyStr = '';
				for (var rowtempid = i-1; rowtempid < gridrowsarr.length; rowtempid++) 
				{
					pnumTemp = gridObj.cellById(rowtempid, 3).getValue();
					if (pnum == pnumTemp)
					{
						cnum = 	gridObj.cellById(rowtempid, 12).getValue();					
						if(cnum != '-')
						{					
							partQty = parseInt(partQty) + parseInt(gridObj.cellById(rowtempid, 9).getValue());
						}
						else
						{
							flg = true;
							newQty =  parseInt(gridObj.cellById(rowtempid, 9).getValue());
							updatedQty = parseInt(updatedQty) + newQty;
							
							lcnId = gridObj.cellById(rowtempid, 4).getValue();
							vmaxqty = parseInt(gridObj.cellById(rowtempid, 8).getValue());
							markedQtyStr = markedQtyStr + pnumTemp + ' : ' + lcnId + ' , ';		
							if (parseInt(updatedQty) > parseInt(vmaxqty))
							{
								newLcn = newLcn + pnumTemp + ' : ' + lcnId + ' , ';							
							}
							if(newQty > 0)
							{
								pnumLcnStr = 	pnumLcnStr + pnumTemp + ','+ newQty +','+ lcnId +'|';
							}
						}										
					}
					else
					{					
						break;
					}				
				}
				if(parseInt(updatedQty) != parseInt(partQty) && flg )
				{
					dispstr = dispstr + markedQtyStr; 			
				} 
			 }
		}	
		if (dispstr != '')
		{
			Error_Details(Error_Details_Trans(message[10084],dispstr));
		}
		if (ErrorCount > 0)
		{
				fnStopProgress();
				Error_Show();
				Error_Clear();
				return false;
		}
		if (newLcn != '')
		{
			var answer = confirm(Error_Details_Trans(message_operations[306],newLcn))
			if (!answer){
				return false;
			}
		}
		document.frmVendor.hpnumLcnStr.value = pnumLcnStr;
	}
	
  	document.frmVendor.submit();
}

function fnBlankNOC(obj)
{
	if(obj.value == 'NOC#' ||obj.value == 'TBE')
	{
		obj.value = '';	
	} 			
	
}
function chgTRBgColor(val,BgColor) {
	   var obj1 = eval("document.all.tr"+prevtr);
	   obj1.style.background = "";
	   var obj = eval("document.all.tr"+val);
	   prevtr = val;
	   obj.style.background = BgColor;
	}

	function fnBlankLine(varCounter,value)
	{
		if(value == 'TBE' || value == 'NOC#'){
			obj = eval("document.frmVendor.Txt_CNum"+varCounter);
			obj.value = '';
		}
		chgTRBgColor(varCounter,'#AACCE8');
	}


function fnOnPageLoad()
{
	fnExistScanPartCount();
	if(objGridData.indexOf("cell", 0)==-1){
		  return true;
	}
	gridObj = initItemGrid('dataGridDiv',objGridData);
	gridObj.enableTooltips("false,false,false,false,false,false,false,false,false,false,false");
	gridObj.groupBy(1);
	gridObj.attachEvent("onEditCell",doOnCellEdit);
	if(vStatus == 'Verified')
	{
		gridObj.setColumnHidden(4,true);
		gridObj.setColumnHidden(5,true);
		gridObj.setColumnHidden(6,true);
		gridObj.setColumnHidden(7,true);
		gridObj.setColumnHidden(8,true);
	}
}
function initItemGrid(divRef,gridData){
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);
	gObj.init();	
	
	gObj.loadXMLString(gridData);
	
	return gObj;
}

function doOnCellEdit(stage, rowId, cellInd, nValue, oValue) {
	var cnum;
	cnum = gridObj.cellById(rowId, 12).getValue();
	if(cnum != '-')
	{
		return false;
	}
	if(cellInd == 9 && stage == 1)
	{
			var c= this.editor.obj;
			if(c!=null) c.select();						
	}	
	if(gridObj.cellById(rowId, 9).getValue() == '' || gridObj.cellById(rowId, 9).getValue() < 0)
	{
		gridObj.cellById(rowId, 9).setValue(0);
	}
	return true;
}

function fnGenerate(pnum,controlno){
	var cnum = '';
	var val='';
	var control = new Object();
	var cnumstatus = false;
	var qtyobj = new Object();
	var qty = 0;
	var qtycnt = 0;
	var orgqty = 0;
	var vPnumProdMetType = '';
	var pnum1 = pnum.replace(/\./g,'');
	if(pnum1.indexOf("-") != -1){// To remove the '-' while using the part number as id
		pnum1 = replaceAll(pnum1,"-","");
	}
	var blk = document.getElementById("hQty"+pnum1);
	if(blk != null){
		orgqty = parseInt(blk.value);
	}
	var pnumlistobj = document.getElementById(pnum); 
	if(pnumlistobj != null){
		var pnumlist = pnumlistobj.value;
		var pnumlist = pnumlist.substring(0,pnumlist.length-1);
		var cnumarr= pnumlist.split(",");
		var cnumsize = cnumarr.length;
		//couting all sum of qty for passing part
			if(cnumsize > 0){
				for(i=0; i<cnumsize; i++){
					 qtyobj = document.getElementById("Txt_Qty"+cnumarr[i]);	
					 var cnumobj = document.getElementById('Txt_CNum'+cnumarr[i]);
					 if(qtyobj != null){
						 qty = parseInt(qtyobj.value);
						 if(cnumobj != null){
							 cnum = cnumobj.value;
						 }						
						 if (qty > 0 && qty != "" && cnum != null && cnum != '' && cnum != 'TBE'){
							 qtycnt = qtycnt + qty;
						 }
					 }
				}					
				if(qtycnt >= orgqty){
					Error_Details(Error_Details_Trans(message[5020],pnum));
					 return false;
				}else{
					// updating control number and qty in existing row
					for(i=0; i<cnumsize; i++){
						val = cnumarr[0];
						var cnumobj = document.getElementById('Txt_CNum'+cnumarr[i]); 					
						if(cnumobj != null){
							qtyobj = document.getElementById("Txt_Qty"+cnumarr[i]);
							qty = parseInt(qtyobj.value);
							cnum = cnumobj.value;
							 if (cnum == controlno){
									 if(qty+1 > orgqty){
										 Error_Details(Error_Details_Trans(message[5020],pnum));
										 return false;
									 }else{
										 if(validateObject(document.getElementById("partMaterialType"))){// Getting Part num Product Material type
											   vPnumProdMetType = document.getElementById("partMaterialType").value;	
										 }
										 if(vPnumProdMetType == '100845'){ // Tissue Part
											 var array=[pnum,cnum];
											  Error_Details(Error_Details_Trans(message[5021],array));											 
											  return false;
										 }else{
											 qtyobj.value = qty + 1;
										 }
									 }
								 cnumstatus = false;
								 break;	
							 }else if(cnum == null || cnum == '' || cnum == 'TBE'){
								 cnumobj.value = controlno;
								 qtyobj.value = 1;
								 cnumstatus = false;
								 break;
							 }else{
								 cnumstatus = true;
							 }
						}
					}
					
				}
			}
	}else{
		Error_Details(Error_Details_Trans(message[10010],pnum));
		return false;
	}
	//updating control number and qty in newly added row
	if(cnumstatus){
		fnSplit(val,pnum,pickRule,'','Y');
		var count = parseInt(document.getElementById("hNewCnt").value);	
		var hcount = parseInt(document.getElementById("hCnt").value);
		hcount = hcount+count;
		var cnumobj = document.getElementById('Txt_CNum'+hcount); 
		cnumobj.value = controlno;				 		
		qtyobj = document.getElementById("Txt_Qty"+hcount);
		qtyobj.value = 1;
	}
	
	if (ErrorCount > 0 ){
		Error_Show();
	    Error_Clear();
	}else{
		fnScannedPartCount(pnum);
		//caling rule validation
		var strAction = document.getElementById("hAction").value;
		if(strAction != 'EditVerify' && vIncludeRuleEng != 'NO'){
			var ruleTransType =document.getElementById("RE_TXN").value;
			var TransTypeID =document.getElementById("TXN_TYPE_ID").value; 
			control.value = controlno;
			fnFetchMessages(control,pnum,'PROCESS',ruleTransType,TransTypeID);
		}
	}
	}

function fnSplit(val,pnum,pickRule,vqty,scanned){
	cnt = parseInt(document.getElementById("hNewCnt").value);
	cnt++;
	document.getElementById("hNewCnt").value = cnt;
	var hcnt = parseInt(document.frmVendor.hCnt.value);
	hcnt = hcnt+cnt;
	var pnumobj = document.getElementById(pnum); 
	if(pnumobj != null){	
		pnumobj.value=pnumobj.value+hcnt+",";
	}
	var priceobj = eval("document.frmVendor.hPrice"+val);
	var pnumProdMaterialObj = document.getElementById("hPartMaterialType"+val);
	var pnumProdMaterialVal =  pnumProdMaterialObj.value;	
	var pnumProdMaterialStr = "";
	var price = priceobj.value;
	var vFetchMsg = (vIncludeRuleEng != 'NO')? "fnFetchMessages(this,"+pnum+",'PROCESS',"+ruleTransType+","+transTypeID+");":"";
	var NewQtyHtml = "<span id='Sp"+cnt+"'><BR><input type='hidden' name='hPartNum"+hcnt+"' id='hPartNum"+hcnt+"' value='"+pnum+"'><input type='hidden' name='hPrice"+hcnt+"'  id='hPrice"+hcnt+"'  value='"+price+"'><input type='hidden' name='hPartMaterialType"+hcnt+"' id='hPartMaterialType"+hcnt+"' value='"+pnumProdMaterialVal+"'><input type='text' size='3' value='' name='Txt_Qty"+hcnt+"' id='Txt_Qty"+hcnt+"' class='InputArea Controlinput' onFocus=changeBgColor(this,'#ffffff'); onBlur=changeBgColor(this,'#ffffff'); tabindex=1></span>";
	var NewCnumHtml = "<span id='SpC"+cnt+"'><BR><input  type='text' size='22' value='' name='Txt_CNum"+hcnt+"' id='Txt_CNum"+hcnt+"' class='InputArea Controlinput' onFocus=fnBlankNOC(this);fnBlankLine("+val+",this.value);changeBgColor(this,'#ffffff'); onBlur=changeBgColor(this,'#ffffff'); onChange="+vFetchMsg+"; tabindex=1>&nbsp;<a href=\"javascript:fnDelete('"+pnum+"',"+cnt +",'"+scanned+"')\"; ><img src=/images/btn_remove.gif border=0></a></span>";
	var QtyObj = eval("document.all.Cell"+val);
	var CnumObj = eval("document.all.Cnum"+val);
	QtyObj.insertAdjacentHTML("beforeEnd",NewQtyHtml);
	CnumObj.insertAdjacentHTML("beforeEnd",NewCnumHtml);
}
function fnPicSlip(){
	val = document.frmVendor.hConsignId.value;	
	windowOpener(servletpath+"/GmSetBuildServlet?hAction=PicSlip&hConsignId="+val,"SetPic","resizable=yes,scrollbars=yes,top=150,left=150,width=670,height=500");
}

function fnPrintVer(){
	windowOpener("/GmConsignSetServlet?hAction=PrintVersion&hId=<%=strConsignId%>","Con","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnAgree(){
	windowOpener("/GmConsignSetServlet?hId=<%=strConsignId%>&hAction=Ack&","Ack","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnPackslip(){
	windowOpener("/GmConsignSetServlet?hAction=PackVersion&hId=<%=strConsignId%>","Con","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnVRule(){
	 document.frmVendor.hAction.value ="Load";
	 document.frmVendor.action =servletpath + "/GmCommonCancelServlet";
	 document.frmVendor.submit(); 
}

function fnValidateAllControlNo(pnum,ctrlNum,qty,pnumMaterial){	
	var errparts = "";
	if(pnumMaterial == '100845' && qty > 1){ // 100845[Tissue Part]
		if(ctrlNum == ''){
			errparts += pnum + "  " + ctrlNum;
		}else{
			errparts += pnum + " --- " + ctrlNum;
		}
	}
	// For validating the Duplicate COntrol Number on Submit
	if(pnumMaterial == '100845' && qty == 1 && ctrlNum != ''){ // 100845[Tissue Part]
		if(tissueControls.indexOf("|" + pnum +ctrlNum + "|") == -1){
			tissueControls = tissueControls + "|" + pnum +ctrlNum + "|";
		}else{
			errparts += pnum + " --- " + ctrlNum;
		}
	}	
	return errparts;
}
