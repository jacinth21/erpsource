//To change the values inside the field to uppercase
function fnUpperCase(obj){
	obj.value = obj.value.toUpperCase();
}

//To verify control no, when click on "Add" button
function fnAddControlNo(obj) {
	
	var items = document.getElementById("controlNo");
	if(event.keyCode == 13 || obj.name == 'Btn_Cno_Add'){ //It should be called while scanning or pressing the button(event.keyCode == 13, represents scanning)
		fnValidateControlNo(items.value);
	}
	
} 

//Get the Txn type for the control screen
function fnGetTxnType() {

	var disp = '';
	var txnType = '';
	
	// Return Accept screen has the Txn type in a hidden variable but rest are inside a Form element
	if(window.location.href.indexOf('GmReturnReceiveServlet')>0){
		txnType = document.getElementById("hRetType").value;
	}
	else if (window.location.href.indexOf('GmConsignItemServlet')>0)
	{
		txnType = document.getElementById("hType").value;
	}
	else if (window.location.href.indexOf('GmSetAddRemPartsServlet')>0)
	{
		txnType = document.getElementById("hType").value;
	}
	else if(window.location.href.indexOf('gmItemControl')>0){
		txnType = document.getElementById("txntype").value;
	}
	//alert(txnType);
	return txnType;
	
} 

//To validate control no
function fnValidateControlNo(partcno){
	var errStatus = "ERROR";
	var indexof = '0';
	var cntlindexof = '0';
	var partnum = '';
	var controlNo = '';
	var msg = 'Test';
	var pnumProdMaterial = '';
	var responseVal = '';
	ErrorCount = 0;
	var isError = false; // To display the error message
	
	
	indexof = partcno.indexOf("^"); // get the index of '~'
	cntlindexof = partcno.indexOf("(10)"); // get the index of '(10)'
	
	if(indexof != -1){// While scanning only format will be "partno^controlno"
		var cnumarr= partcno.split("^");
		partnum = cnumarr[0]; // get the characters before '^'
		controlNo = cnumarr[1]; //Getting the remaining character, after "^"
	}else if(cntlindexof != -1){// While scanning only format will be "(01)udi number(17)exp date(10)controlno"		
		var cnumArray = partcno.split("(10)");
		controlNo = cnumArray[1];	// //Getting the remaining character, after "(10)"		
	}else{
		controlNo = partcno;
	}
	controlNo = controlNo.toUpperCase();
	
	fnStartCommonProgress();
	//partnum = '557.000';
	// Get the Txn Type
	var txnType = fnGetTxnType();
	
	// While scanning part no with control no it will validate below
	if(controlNo == ''){ 
		Error_Details(Error_Details_Trans(message[10017],controlNo));
		isError = true;
	}else{    // PC-3459- Load control number from UDI in set build
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmLoanerLotReprocess.do?method=controlNoValidation&ctrlNum='+controlNo+'&partNum='+encodeURIComponent(partnum)+'&log='+msg+'&ramdomId=' + Math.random());
		var loader;
		var response;
		
		loader = dhtmlxAjax.getSync(ajaxUrl);
		response = loader.xmlDoc.status;
		if(response == 200){  //On success status parsing the response values.
			var strJsonString = loader.xmlDoc.responseText;
			strJsonString = "["+strJsonString+"]";
			JSON_Array_obj = JSON.parse(strJsonString);
				$.each(JSON_Array_obj, function(index,jsonObject) {
					controlNo = jsonObject.CONTROLNUM;
					msg = jsonObject.ERRORMSG;
					partnum = jsonObject.PARTNUM;
				});
		}
		if(msg == ''|| msg == null){
			ErrorCount = 0;
			errStatus = "SUCCESS";
		}else{
			Error_Details(msg);
			ErrorCount = 1;
		}
		if (ErrorCount > 0 ){							
			fngenerateDivError(errStatus);						   
		}else{
			fnGenerate(partnum,controlNo);
			if (ErrorCount == 0 ){
				errStatus = "SUCCESS";
				Error_Details("");								
			}
			fngenerateDivError(errStatus);					
		}
	document.getElementById("controlNo").value = "";
	document.getElementById("controlNo").focus();			
	
	// used below codes to stop reloading the page after scanning the id
	if(!e) var e = window.event;

    e.cancelBubble = true;
    e.returnValue = false;	

    if (e.stopPropagation) {
            e.stopPropagation();
            e.preventDefault();
    }
 }
	 fnStopCommonProgress();   // change added in PC-4644 
		if(isError){
			if (ErrorCount > 0 ){
				fngenerateDivError(errStatus);	
			}
		}		
}

//To call the Start Progress method based on the Parent and child form
function fnStartCommonProgress(){
	if(window.parent.document.getElementById("refId")){
		window.parent.fnStartParentProgress();
	}else{
		fnStartProgress();
	}
}

//To call the Stop Progress method based on the Parent and child form
function fnStopCommonProgress(){
	if(window.parent.document.getElementById("refId")){
		window.parent.fnStopParentProgress();
	}else{
		fnStopProgress();
	}
}

function fngenerateDivError(errStatus){
	var messgaeColor = "red";
	if(errStatus == 'SUCCESS'){
		messgaeColor = "green";
	}
	fnStopCommonProgress();
	var vCode = "";
	for(var i = 0 ; i <= (ErrorCount-1); i++){			
		if(errStatus == 'SUCCESS'){
			vCode = vCode + "<TR><TD align=left width='20%'> <font color = '"+messgaeColor+"'><B>"+ ErrorDetail[i][0] + "</B></font></TD>" ;
		}else{
			fnPlayErrorSound('beep');
			vCode = vCode + "<TR><TD align=left width='20%'> <font color = '"+messgaeColor+"'>"+ ErrorDetail[i][0] + "</font></TD>" ;
		}		
		vCode = vCode + "</TR>";
		vCode = vCode + "<tr>";
		vCode = vCode + "</tr>";
	}
	objError = document.all.errormessage;
	objError.innerHTML = vCode;
	Error_Clear();
	return false;
}

function fnPlayErrorSound(soundObj) {
	try{ 
		  var sound = document.getElementById(soundObj);
		   
		   if (sound)
				 sound.Play();
		 }
	catch (err)
		{
		 return null;
		} 

}