var objGridData;

function initGridData(divRef,gridData){
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");	
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.init();	
	gObj.loadXMLString(gridData);
	return gObj;	
}

function fnOnPageLoad(){  
  var footer = new Array();
  gridObj = initGridData('dataGridDiv',objGridData);
  gridObj.setColTypes("ed,coro,ro,ro,ed,coro,ro,ro");
  var gridrows =gridObj.getAllRowIds(",");
  if(gridrows.length==0){
	  addRow();
	  addRow();
	  addRow();
	  addRow();
	  addRow();
  }	
  //gridObj.enableTooltips("false,false,false,false,false,false,false,false");
  gridObj.attachEvent("onEditCell",doOnCellEdit);
  var divfr0 = document.getElementById('dropdown');
  var divfr1 = document.getElementById('gridData'); 
  var divfr2 = document.getElementById('addTbl');
  var strOpt = document.getElementById('strOpt');
  var obj    = eval(document.getElementById('dataGridDiv')); 
  	if(strOpt.value == "save"){
  		obj.style.visibility = 'hidden';
		obj.style.display = 'none';
			  
		divfr2.style.visibility = 'hidden';
		divfr2.style.display = 'none';

		divfr0.style.visibility = 'hidden';
		divfr0.style.display = 'none';
	}else{
		obj.style.visibility = 'visible';
		obj.style.display = 'block';

		divfr2.style.visibility = 'visible';
		divfr2.style.display = 'block';

		divfr0.style.visibility = 'visible';
		divfr0.style.display = 'block';
	}
}


function fnSubmit()
{
	var gridrows =gridObj.getAllRowIds(",");
	var finalData='';
	var strInputString = "";	
	var v_string ="";
	var errString = "";
	var errLocString = "";
	var errPartnum = "";
	var errEmptyString = ""; 
	var errReqQty = "";
	var errEmptyLocation = "";
		
	var warehouseid = document.frmInitTransfer.warehouseid.value;
	
	if(gridrows.length>0){
		var gridrowid;
		var gridrowsarr = gridrows.toString().split(",");
		var columnCount = gridObj.getColumnsNum();
		
		 for (var rowid = 0; rowid < gridrowsarr.length; rowid++) 
		 {
				gridrowid = gridrowsarr[rowid];
				for(var col=0;col<columnCount;col++){
					
					var strpartNum = gridObj.cellById(gridrowid, 0).getValue();
					//var strToLocTemp = gridObj.cellById(gridrowid, 1).getValue();
					//var strFromLocTemp = gridObj.cellById(gridrowid, 5).getValue();
					
					//Changing & retrieving the column values because of From loc,To loc column's are changed.
					var strFromLoc = gridObj.cellById(gridrowid, 1).getValue();
					var strToLoc = gridObj.cellById(gridrowid, 5).getValue();
					strToLoc = strToLoc.substring(strToLoc.indexOf('_')+1,strToLoc.length); 					
					strFromLoc = strFromLoc.substring(strFromLoc.indexOf('_')+1,strFromLoc.length);
					var strreqQty = gridObj.cellById(gridrowid, 4).getValue();
					var strbulkQty = gridObj.cellById(gridrowid, 3).getValue();
					v_string = strpartNum+","+strToLoc+","+strFromLoc+","+strreqQty+"|";
				}
				if(strreqQty <= "0" && strpartNum != "")
				{
					errReqQty  = errReqQty + "," + strpartNum ;
				}
				if(strpartNum != '') 
				{
					if(strpartNum == "Invalid Part#")
					{
						Error_Details("Please enter a valid part number.");
						continue;
					}
					 
					if(strToLoc == strFromLoc && strToLoc != "" && strFromLoc!="")
					{
						errLocString = errLocString + ","+ strpartNum;
					}
					
					if(parseInt(strreqQty) > parseInt(strbulkQty))   
					{
						errString = errString +","+strpartNum;
					}
					
					if(strToLoc == '' || strFromLoc == '')
					{
						errEmptyLocation = errEmptyLocation + "," +  strpartNum;
					}
					
				}
				
					
				
				if(strpartNum != ''  ||   strToLoc!='' || strFromLoc!='' || strreqQty != '') 
				{
					strInputString = strInputString + v_string;
				}
				
		 }
		 
		 if(errReqQty != "")
		 {
			Error_Details("Sugg. qty should not be less than or equal to 0 for the following parts:"+errReqQty.substr(1, errReqQty.length));				
		 }
		 
		 if(errString != "")
		 {
			Error_Details("To location qty should be less or equal to the from location qty for the following parts:"+errString.substr(1, errString.length));				
		 }
		 if(errLocString != "") 
		 {
			 Error_Details("From and To Location should not be same for the following parts:"+errLocString.substr(1, errLocString.length));				
		 }
		 
		 if( errPartnum != "") 
		 {
			Error_Details("Part Number should not be empty for the following parts:"+errPartnum.substr(1, errPartnum.length));				
		 }
		 
		 if( errEmptyLocation != "") 
		 {
			Error_Details("From and To Location should not be empty for the following parts:"+errEmptyLocation.substr(1, errEmptyLocation.length));				
		 }
         if(strInputString == ""){
        	 Error_Details("No Records Found.Please Enter Valid data To proceed.");
         }		 
		 if (ErrorCount > 0)
			{
				Error_Show();
				Error_Clear();
				return false;
			}
		 
		 	for(var j=0;j<arrValue.length;j++){
	 			var arrVal = arrValue[j].split(",");
		 		if(warehouseid == arrVal[0]){
		 			warehouseid = arrVal[1];
		 			break;
		 		}
		 	}
		 	document.frmInitTransfer.hWareHouseType.value = warehouseid;
		 	document.frmInitTransfer.v_str.value = strInputString;
			document.frmInitTransfer.action = "/gmInitiateTransfer.do?method=saveReplenishment";
			document.frmInitTransfer.submit();
}
}


function addRow(){
	gridObj.addRow(gridObj.getUID(),'');
}


function removeSelectedRow(){

	var gridrows=gridObj.getSelectedRowId();	
	if(gridrows!=undefined){
		var NameCellValue ;
		var gridrowsarr = gridrows.toString().split(",");
		var added_row;
		for (var rowid = 0; rowid < gridrowsarr.length; rowid++) {
			gridrowid = gridrowsarr[rowid];			
			gridObj.deleteRow(gridrowid);						
		}
	}else{
		Error_Clear();
		Error_Details(" Please Select a row to Delete.");				
		Error_Show();
		Error_Clear();
	}
}


function doOnCellEdit(stage, rowId, cellInd, nValue, oValue) {
	//alert("stage ::"+stage+":::cellInd::"+cellInd);
	var warehouseid = document.frmInitTransfer.warehouseid.value;
	fnValidateDropDn('warehouseid',' Warehouse');
	
	 if (ErrorCount > 0)
		{
			Error_Show();
			Error_Clear();
			return false;
		}
	rowID = rowId;
	if(cellInd == 0)
	{ 
	
		if(nValue!=oValue) {
			gridObj.cellById(rowID,1).setValue("");
			gridObj.cellById(rowID,2).setValue("");
			gridObj.cellById(rowID,3).setValue("");
			gridObj.cellById(rowID,4).setValue("");
			gridObj.cellById(rowID,5).setValue("");
			gridObj.cellById(rowID,6).setValue("");
			gridObj.cellById(rowID,7).setValue("");
			}

		
		if (stage==2 && nValue != ''){
                     dhtmlxAjax.get('/gmInitiateTransfer.do?method=fetchPartDetails&partNum='+ encodeURIComponent(nValue)+'&warehouseid=' +  warehouseid + '&ramdomId='+Math.random(),fnAddPartLoc);
                     gridObj.cellById(rowId,1).setValue('');
                   }
		if(nValue == ''){
	    	gridObj.cellById(rowId, 0).setValue("");	
	    	gridObj.getCustomCombo(rowID,1).clear();
	    	gridObj.getCustomCombo(rowID,5).clear();
	    }
		
		
    }
   if(cellInd == 1){
    	if (stage==2 && nValue != ''){
        	var locationID = nValue;
        	locationID = locationID.substring(locationID.indexOf('_')+1,locationID.length);
        	var partNum = gridObj.cellById(rowId, 0).getValue();
			dhtmlxAjax.get('/gmInitiateTransfer.do?method=fetchInvLocationByID&locationId='+locationID+'&warehouseid=' +  warehouseid + '&partNum='+encodeURIComponent(partNum)+'&ramdomId='+Math.random(),fnLoadFromLoc);
			
    	}
    	else if(stage == 1 && oValue == ''){
    		return;
    	}
    }
   if(cellInd == 5){
   	if (stage==2 && nValue != ''){
   		var locationID = nValue;
   		locationID = locationID.substring(locationID.indexOf('_')+1,locationID.length);
   		var partNum = gridObj.cellById(rowId, 0).getValue();
   		dhtmlxAjax.get('/gmInitiateTransfer.do?method=fetchInvLocationByID&locationId='+locationID+'&warehouseid=' +  warehouseid + '&partNum='+encodeURIComponent(partNum)+'&ramdomId='+Math.random(),fnLoadToLoc);
   		
   	}
   }

   if(cellInd == 4){
	   	if (stage==2 && nValue != ''){
	   		var strlocationQty = gridObj.cellById(rowId, 1).getValue();

	   		//strlocationQty = strlocationQty.substring(strlocationQty.indexOf("(")+1,strlocationQty.indexOf(")"));
	   		strCurrQty = gridObj.cellById(rowId, 3).getValue();
	   		var strreqQty = gridObj.cellById(rowId, 4).getValue();
		}
	   }
   if((cellInd == 0 || cellInd == 1 || cellInd == 5)&&(oValue!=undefined)){
	   var gridrows =gridObj.getAllRowIds(",");
	   var gridrowsarr = gridrows.toString().split(","); 
	   var strPartNum = gridObj.cellById(rowId, 0).getValue();
	   var strToLoc = gridObj.cellById(rowId, 5).getValue();
	   var strFromLoc = gridObj.cellById(rowId, 1).getValue(); 
	   for (var rowsid = 0; rowsid < gridrowsarr.length; rowsid++) 
		 {   	 
	         var gridrowsid = gridrowsarr[rowsid];
			 var PartNum = gridObj.cellById(gridrowsid, 0).getValue();
			 var ToLoc = gridObj.cellById(gridrowsid, 5).getValue();
			 var FromLoc = gridObj.cellById(gridrowsid, 1).getValue();
			 if(rowId != gridrowsid){ 
				if(strPartNum == PartNum && strToLoc == ToLoc && strFromLoc == FromLoc && strPartNum != '' && strToLoc !='' && strFromLoc!=''){
					Error_Details("From and To Location combination are already exsits for the Part Number :<b>"+strPartNum+"</b>.");				
					Error_Show();						
					Error_Clear();
					return false;
				}
			 }
		 }
}
    
return true;
}

function fnAddPartLoc(loader) {	
	
    var response =loader.xmlDoc.responseText;
    var LocationIDArr = new Array();
    
   if(response == "")
   {
	   gridObj.cellById(rowID, 0).setValue("Invalid Part#");	
	   gridObj.getCustomCombo(rowID,1).clear();
	   gridObj.cellById(rowID, 2).setValue('');
	   gridObj.cellById(rowID, 3).setValue('');
	   gridObj.cellById(rowID, 4).setValue(''); 
	   gridObj.getCustomCombo(rowID,5).clear();	   
	   gridObj.cellById(rowID, 6).setValue('');
	   gridObj.cellById(rowID, 7).setValue('');
   }
   
   else if(response != null && response.length>0)
   {	
      //Changing because of From loc,To loc column's are changed in vm & loading the from,to loc combo values. 
	   var comboToLocation = gridObj.getCustomCombo(rowID,5);
	   var comboFromLocation = gridObj.getCustomCombo(rowID,1);
		comboToLocation.clear();
		comboFromLocation.clear();
	   var gridrowsarr = response.split("|");
	   var idarray ="";	
	   	   for (i=0; i<gridrowsarr.length; i++) {
			id = gridrowsarr[i];
 			LocationIDArr[i]=new Array(2);
			LocationIDArr[i][0]=id.substring(id.indexOf(':')+1,id.length); // the combo id is "loccode_locid"
			LocationIDArr[i][1]=id.substring(0,id.indexOf('_'));           // the combo option value is "warehousenm:loccode"
		}  
		for(i=0;i<LocationIDArr.length;i++)	{
			comboToLocation.put(LocationIDArr[i][0],LocationIDArr[i][1]);
			comboFromLocation.put(LocationIDArr[i][0],LocationIDArr[i][1]);
		}
		comboToLocation.save();				 	
		comboFromLocation.save();
   }
}

function parseNull(val){
	if(val=='null')
		return val="";
	else
		return val; 
}


function fnLoadFromLoc(loader)
{
	var response = loader.xmlDoc.responseText;
	 fnLoadLoc(response,1);
}
function fnLoadToLoc(loader)
{
	 var response = loader.xmlDoc.responseText;
	 fnLoadLoc(response,5);
}
function fnLoadLoc(response,cellInd){

	   var mySplitResult = response.split("^");
	   if(response!=null){ 
	   	strCurrQty = parseNull(mySplitResult[0]); 
	   	strMaxQty = parseNull(mySplitResult[1]);
	   	strMinQty = parseNull(mySplitResult[2]);
	   	strLocType = parseNull(mySplitResult[3]);
	   	strSugQty = parseNull(mySplitResult[4]);
	   } 
	 
	// var cellInd=1;
	   if(cellInd == 1)
	   {
		   
	   gridObj.cellById(rowID,2).setValue(strLocType);
	   gridObj.cellById(rowID,3).setValue(strCurrQty);	
	   //gridObj.cellById(rowID,4).setValue(strSugQty);
	   }

	   if(cellInd ==5){
		   
		   gridObj.cellById(rowID,6).setValue(strLocType);
		   gridObj.cellById(rowID,7).setValue(strCurrQty);
	   } 
	   	
}
