//members 
this.keyArray = new Array(); // Location Keys 
this.valArray = new Array(); // Location Values 
         
// methods 
this.put = put; 
this.get = get; 
this.findIt = findIt;
var tempCntlNum;
var errparts ;
var tissueControls = '';
var checkVeriFl = "";
function put(key,val) 
{ 
    var elementIndex = this.findIt(key); 
     
    if( elementIndex == (-1) ) 
    { 
        this.keyArray.push( key ); 
        this.valArray.push( val ); 
    } 
    else 
    { 
        this.valArray[ elementIndex ] = val; 
    } 
} 

function get(key) 
{ 
    var result = null; 
    var elementIndex = this.findIt( key ); 
 
    if( elementIndex != (-1) ) 
    {    
        result = this.valArray[ elementIndex ]; 
    } 
    else {
    	result = "<option value= >"+message_operations[424]+"</option>";
    }
    return result; 
}
function findIt(key) 
{ 
    var result = (-1); 
 
    for( var i = 0; i < this.keyArray.length; i++ ) 
    { 
        if( this.keyArray[ i ] == key ) 
        { 
            result = i; 
            break; 
        } 
    } 
    return result; 
} 

function fnShowLocDrpDwn(pnum,count,mapLocId){
	var obj="locDiv"+count;
	var drpdnStr = get(pnum);
	var valStr = "value=\""+mapLocId + "\"";
	drpdnStr = (drpdnStr.indexOf(mapLocId)>0)?drpdnStr.replace(valStr,valStr+" selected"):drpdnStr;
	document.getElementById(obj).innerHTML='<select  name="locDrpDn'+count+'" id="locDrpDn'+count+'" class=RightText tabindex="-1" onChange="">'+drpdnStr+'</select>';
	
}

//When click on Generate button to generate control numbers
function fnGenerate(pnum,controlno){
	var cnum = '';
	var val='';
	var control = new Object();
	var cnumstatus = false;
	var qtyobj = new Object();
	var qty = 0;
	var qtycnt = 0;
	var orgqty = 0;
	var vPnumProdMetType = '';
    var pnum1 = pnum.replace(/\./g,'');
    var blk = document.getElementById("hQty"+pnum1);
    if(blk != null){
    	orgqty = parseInt(blk.value);
    }
	var pnumlistobj = document.getElementById(pnum); 
	if(pnumlistobj != null){
	var pnumlist = pnumlistobj.value;
	var pnumlist = pnumlist.substring(0,pnumlist.length-1);
	var cnumarr= pnumlist.split(",");
	var cnumsize = cnumarr.length;
	//couting all sum of qty for passing part
		if(cnumsize > 0){
			for(i=0; i<cnumsize; i++){
				 qtyobj = document.getElementById("Txt_Qty"+cnumarr[i]);	
				 var cnumobj = document.getElementById('Txt_CNum'+cnumarr[i]);
				 if(qtyobj != null){
					 qty = parseInt(qtyobj.value);
					 if(cnumobj != null){
						 cnum = cnumobj.value;
					 }
					 if (qty > 0 && qty != "" && cnum != null && cnum != '' && cnum != 'TBE'){
						 qtycnt = qtycnt + qty;
					 }
				 }
			}
			if(qtycnt >= orgqty){
				Error_Details(message_operations[33]+pnum);
				return false;
			}else{
				// updating control number and qty in existing row
				for(i=0; i<cnumsize; i++){
					val = cnumarr[0];
					var cnumobj = document.getElementById('Txt_CNum'+cnumarr[i]); 					
					if(cnumobj != null){
						qtyobj = document.getElementById("Txt_Qty"+cnumarr[i]);
						qty = parseInt(qtyobj.value);
						cnum = cnumobj.value;
						 if (cnum == controlno){
								 if(qty+1 > orgqty){
									 Error_Details(Error_Details_Trans(message[5020],pnum));		
									 return false;
								 }else{
									 if(validateObject(document.getElementById("partMaterialType"))){// Getting Part num Product Material type
										   vPnumProdMetType = document.getElementById("partMaterialType").value;	
									 }
									 if(vPnumProdMetType == '100845'){ // Tissue Part
										 var array=[pnum,cnum];
										  Error_Details(Error_Details_Trans(message[5021],array));											 
										  return false;
									 }else{
										 qtyobj.value = qty + 1;
									 }
								 }							 
							 cnumstatus = false;
							 break;	
						 }else if(cnum == null || cnum == '' || cnum == 'TBE'){
							 cnumobj.value = controlno;
							 qtyobj.value = 1;
							 cnumstatus = false;						
							 break;						
						 }else{
							 cnumstatus = true;							
						 }		
					}
				}
				
			}
		}
	}else{
		Error_Details(Error_Details_Trans(message[5022],pnum));
		return false;
	}
	//updating control number and qty in newly added row
	if(cnumstatus){
		fnSplit(val,pnum,pickRule,'','Y');
		var count = parseInt(document.getElementById("hNewCnt").value);	
		var hcount = parseInt(document.getElementById("hCnt").value);
		hcount = hcount+count;
		var cnumobj = document.getElementById('Txt_CNum'+hcount); 
		cnumobj.value = controlno;				 		
		qtyobj = document.getElementById("Txt_Qty"+hcount);
		qtyobj.value = 1;
	}
	
	if (ErrorCount > 0 ){
		Error_Show();
	    Error_Clear();		
	}else{
		fnScannedPartCount(pnum);
		//caling rule validation
		var strAction = document.getElementById("haction").value;
		if(strAction != 'EditVerify' && vIncludeRuleEng != 'NO'){
			var ruleTransType =document.getElementById("RE_TXN").value;
			var TransTypeID =document.getElementById("TXN_TYPE_ID").value; 
			control.value = controlno;
			fnFetchMessages(control,pnum,'PROCESS',ruleTransType,TransTypeID);
		}
	}	
}
function fnSplit(val,pnum,pickLocRule,vqty,scanned)
{
	var partDesigNum = pnum;
	var strSelect = "";
	cnt = parseInt(document.getElementById("hNewCnt").value);	
	cnt++;
	document.getElementById("hNewCnt").value = cnt;
	var hcnt = parseInt(document.getElementById("hCnt").value);
	hcnt = hcnt+cnt;
	var pnumobj = document.getElementById(pnum); 
	if(pnumobj != null){
	var pnum = pnumobj.value;
	pnumobj.value=pnumobj.value+hcnt+",";
	}
	var pnumobj = document.getElementById("hPartNum"+val); 
	var priceObj = document.getElementById("hPrice"+val); 
	var warehouseObj = document.getElementById("hWareHouse"+val);
	var pnumProdMaterialObj = document.getElementById("hPartMaterialType"+val);
	var lottrackflobj = document.getElementById("hlottrackfl"+val); 
	if ( pnumProdMaterialObj != null && pnumProdMaterialObj != undefined ){
	var pnumProdMaterialVal =  pnumProdMaterialObj.value;	
	}
	var pnum = pnumobj.value;
	var price = priceObj.value;
	if ( lottrackflobj != null && lottrackflobj != undefined ){
	var lottrackfl = lottrackflobj.value;
	}
	var warehouse = (warehouseObj)?warehouseObj.value:'90800';
	var cnumstr = "";
	var strAction = document.getElementById("haction").value;
	if(strAction != 'EditVerify' ){
		var ruleTransType =document.getElementById("RE_TXN").value;
		var TransTypeID =document.getElementById("TXN_TYPE_ID").value; 
		var inludeRuleFn = (vIncludeRuleEng != 'NO')?"fnCntrNumValidation('"+hcnt+"',this,'"+pnum+"','"+lottrackfl+"');":"";
		cnumstr = "<input type='text' size='22' value='' id='Txt_CNum"+hcnt+"' name='Txt_CNum"+hcnt+"' class='InputArea Controlinput'  maxlength=40  onFocus=changeBgColor(this,'#AACCE8');  onBlur=\"changeBgColor(this,'#ffffff'); "+inludeRuleFn+"\"  tabindex=3>";
	}else {
		var cnumObj =document.getElementById("Txt_CNum"+val); 
		var cnum = cnumObj.value;
		cnumstr = "<input type='hidden' id='Txt_CNum"+hcnt+"' name='Txt_CNum"+hcnt+"' value='"+cnum+"'>"+"&nbsp;&nbsp;&nbsp;&nbsp;"+cnum;
	}
	var NewHtml ="<span id='Sp"+hcnt+"'><BR><input type='hidden' id='hPartNum"+hcnt+"' name='hPartNum"+hcnt+"' value='"+pnum+"'><input type='hidden' id='hPNumReDesig"+hcnt+"' name='hPNumReDesig"+hcnt+"' value='"+partDesigNum+"'><input type='hidden' id='hPrice"+hcnt+"' name='hPrice"+hcnt+"' value='"+price+"'><input type='hidden' name='hPartMaterialType"+hcnt+"' value='"+pnumProdMaterialVal+"'><input type='hidden' id='hWareHouse"+hcnt+"' name='hWareHouse"+hcnt+"' value='"+warehouse+"'><input type='text' size='2' value='' id='Txt_Qty"+hcnt+"' name='Txt_Qty"+hcnt+"' class='InputArea Controlinput' onFocus=changeBgColor(this,'#AACCE8'); onBlur=\"changeBgColor(this,'#ffffff');\" tabindex=3>&nbsp;&nbsp;"+cnumstr+"&nbsp;<span style='color:white;' id='SpQty" + hcnt + "'>" + vqty + "</span>";//<span id='locDiv"+hcnt+"' ></span>&nbsp<a href=javascript:fnDelete("+cnt+"); ><img src=/images/btn_remove.gif border=0></a></span>";
	var newObj = eval("document.all.Cell"+val);

	if(pickLocRule == "YES"){

		var NewLocDnHtml = "";
			NewLocDnHtml = "<span id='locSp"+hcnt+"'><BR><select id='locDrpDn"+hcnt+"' name='locDrpDn"+hcnt+"'  class=RightText tabindex='-1' onChange=''>"+get(pnum)+"</select>&nbsp<a style='display:inline-block' href=\"javascript:fnDelete('"+pnum+"',"+hcnt+",'"+pickLocRule+"','"+scanned+"')\"; ><img src=/images/btn_remove.gif border=0></a></span>";
		var lcDnObj = eval("document.all.locCell"+val);
		lcDnObj.insertAdjacentHTML("beforeEnd",NewLocDnHtml);
	}else{
		NewHtml = NewHtml + "<a href=\"javascript:fnDelete('"+pnum+"',"+hcnt+",'"+pickLocRule+"','"+scanned+"')\"; ><img src=/images/btn_remove.gif border=0></a>";
	}
	newObj.insertAdjacentHTML("beforeEnd",NewHtml+"</span>");
}

function fnDelete(pnum,val,pickLocRule,scanned)
{
	var Obj = eval("document.all.Sp"+val);
	var ObjPart = eval("document.all.SpPart"+val);
	var ObjDes = eval("document.all.SpDes"+val);
	var ObjQty = eval("document.all.SpQty"+val);
	
	Obj.innerHTML = "";
	
	if(ObjPart){
		ObjPart.innerHTML = "";
		ObjDes.innerHTML = "";
		ObjQty.innerHTML = "";
	}
	if(pickLocRule == "YES"){
		var newLocObj = eval("document.all.locSp"+val);
		newLocObj.innerHTML = "";
	}
	fnDeleteScanned(pnum,scanned);
}

function fnSubmit(pickLocRule){
	tissueControls = '';
	var errPnums = "";
	var errLocPnums = "";
	var errCtrlNum = "";
	var errInvLocType = "";
	var errFGLocType = "";
	var errQty = "";	
	var errorTissueParts = "";
	var pnumMaterial = "";
	var hcnt = parseInt(document.getElementById("hCnt").value);
	cnt = parseInt(document.getElementById("hNewCnt").value);
	var strtxntype = document.frmItemControl.txntype.value;
	hcnt = hcnt+cnt;
	var inputString = "";
	
	var val;
	var TxnType ='';
	var fgBinString = '';
	var vReDesignTrans = '';

	if(FGBinFl == "YES" && strtxntype != '4112' && strtxntype != '111800' && strtxntype != '111801' && strtxntype != '111802' && strtxntype != '111803' && strtxntype != '111804' && strtxntype != '111805'){ //If FG Bin section is showing in the screen, we need to pass the string to save
		//111800 - Quarantine - Inhouse ,111801 - Bulk - Inhouse, 111802 - Finished Goods - Inhouse, 111803 -Repackaging - Inhouse , 111804 - IH Inventory - Inhouse, 111805- Restricted Warehouse - Inhouse
		var items = document.getElementById("AssignedToCombo");
		if(items.options.length > 0){// IF FG Bin id is there in combo box
			for (var i = 0; i < items.options.length; i++) {
				val = items.options[i].value; //Getting FGBin value to a variable
				fgBinString = fgBinString + val+"|";
				
			}
		}/*else{//FG Bin id is mandatory For Orders, Item Consignment and FGLE
			Error_Details("Please scan/enter atleast one FG Bin id");
		}*/
	}
	if(document.getElementById("TXN_TYPE_ID"))
		TxnType = document.getElementById("TXN_TYPE_ID").value;
	
	var txnid =  document.getElementById("txnid").value;
	var txn = txnid.split('-');
	var txnPrefix = txn[0];
	
	for(var i = 0; i <= hcnt; i++ ){
		var pnumobj = document.getElementById("hPartNum"+i);
		var priceObj = document.getElementById("hPrice"+i); 
		var qtyobj = document.getElementById("Txt_Qty"+i); 
		var ctrlNumObj = document.getElementById("Txt_CNum"+i); 
		var warehouseObj = document.getElementById("hWareHouse"+i);
	 	var lottrackfl = document.getElementById("hlottrackfl"+i);
		if(document.getElementById("hControl"+i) != undefined){
			var tempCntlNumObj = document.getElementById("hControl"+i);
			tempCntlNum = tempCntlNumObj.value;
		}
		
 		var pnum = "";
 		var pnumredesig = "";
		var price = "";
		var qty = "";
		var ctrlNum = "";
		
		var warehouse = "";
		
		if(pnumobj&&qtyobj){
			pnum = pnumobj.value;
		 	price = priceObj.value;
		 	qty = qtyobj.value;
		 	ctrlNum = ctrlNumObj.value.toUpperCase();
		 	/* if the lot is 'NOC#' or the lottrack is not enabled for the flag,
		 	 * then set the hEnableNOCfl flag to 'N' 
		 	 * */
		 	if(document.getElementById("hEnableNOCfl") != undefined){
		 		if (ctrlNum != 'NOC#' || lottrackfl != 'Y'){
		 			document.getElementById("hEnableNOCfl").value = 'N'; 
		 		}
		 	}
		 	if(document.getElementById("hPNumReDesig"+i) != undefined){
				var pnumredesigobj = document.getElementById("hPNumReDesig"+i);
				pnumredesig = pnumredesigobj.value;
				}
		 	if(warehouseObj)
		 		warehouse = warehouseObj.value;
		 	else{
		 		warehouse = '90800';
		 	}
		 	if(ctrlNum == "TBE"){
		 		ctrlNum = "";
		 	}
		 	if(ctrlNum == "" && document.getElementById("relverfl").checked){
		 		errCtrlNum += (errCtrlNum.indexOf(pnum) != -1)? "":", "+pnum;
		 	}
		    // Validation for not allowing Scanning same COntrol Number Twice for Tissue Parts
		 	if(document.getElementById("hPartMaterialType"+i) != undefined){
				var pnumMaterialObj = document.getElementById("hPartMaterialType"+i);
				pnumMaterial = pnumMaterialObj.value;
			}
		 	/* Code reverd to previus version from 68239 to 68115 due to need to validate the Tissue Parts in all the Cases.
			 * but here condition states that, need to validate only, when the Release for Verification Check box selection time.
			 * so code modified accordinlgy. 	
			*/
		 	if(validateObject(document.getElementById("relverfl"))){
		 		checkVeriFl = document.getElementById("relverfl").checked;
		 	}
		 	var errparts = fnValidateAllControlNo(pnum,ctrlNum,qty,pnumMaterial,checkVeriFl);
			if(errparts != ''){
				//errorTissueParts +=  ", <BR>"+errparts;
				errorTissueParts = errorTissueParts + "<br>" +errparts;
			}
		 	var LocId = "";
		 	
		 	if(pickLocRule == "YES"){
			 	var LocIdObj = document.getElementById("locDrpDn"+i); 
			 	var locDrpDn_len = LocIdObj.length ; 
			 	LocId = LocIdObj.options[LocIdObj.selectedIndex].value;
			 	var locTxt = LocIdObj.options[LocIdObj.selectedIndex].text;
			 	var locType = "";   //added for PMT-37780
			 	if(locTxt.indexOf('FG:') != -1){    //added for PMT-37780
			 		locType = 'FG'; 
			 	}
			 	if(locTxt.indexOf('RW:') != -1){    //added for PMT-37780
			 		locType = 'RW';
			 	}
			 /*	var locType = locTxt.substring(0,2);*/   //commented for PMT-37780
			 	
			 	if(warehouse == '56001'){
			 		//LocId != '' is added for the literature parts. No need of locations for such parts.
			 		if(locType != 'RW' && LocId != '0' && LocId != ''){
			 			errInvLocType += (errLocPnums.indexOf(pnum) != -1)? "":", "+pnum;
			 		}
			 	}
			 	if(TxnType == '4112'){
			 		if(warehouse == '90800'){
			 			//LocId != '' is added for the literature parts. No need of locations for such parts.
			 			if(locType != 'FG' && (LocId != '0' && LocId != '')){
				 			errFGLocType += (errLocPnums.indexOf(pnum) != -1)? "":", "+pnum;
				 		}
				 	}
			 	}
			 	
			 	LocId = (LocId=="0")?"":LocId;
			 	
				if((get(pnum)).length<40 && locDrpDn_len>1){//check part has location , if it's no location(only choose one), should pass
						errPnums += (errPnums.indexOf(pnum) != -1)? "":", "+pnum; 
				}else if(LocId == 0 && qty> 0 && locDrpDn_len>1){
					errLocPnums += (errLocPnums.indexOf(pnum) != -1)? "":", "+pnum;
				}
			}
		 	if (qty > 0){
		 		if(validateObject(document.getElementById("reDesigTrans"))){
		 			vReDesignTrans = document.getElementById("reDesigTrans").value;				 	
			 	}
		 		if (vReDesignTrans == 'Y'){
		 			pnum = pnumredesig; // Here original (From) part is appened in input string for PTRD Verification.
		 		}
		 		inputString = inputString+pnum+","+qty+","+ctrlNum+","+LocId+ "," + tempCntlNum + "," + warehouse+ "|";
		 	}
		 	
		 	document.getElementById("Txt_CNum"+i).value = ctrlNum;// to set converted uppercase	control number back to screen
		}
	}
	
	/* if the Release for Shipment is checked and any one of the Insert check box is not checked for the Transaction,
	 * then throw the below validation 
	 * */
 	if (checkVeriFl == true && InsertFl == 'Y' && TransInsertFl == 'Y' ) {
	 	var checkboxTotalLen 	= document.getElementsByName("Chk_insert");
	 	if (checkboxTotalLen != null && checkboxTotalLen != undefined){
	 		checkboxTotalLen = document.getElementsByName("Chk_insert").length;
	 	}
	 	var checkedCheckBoxLen 	= document.querySelectorAll('input[name="Chk_insert"]:checked');
	 	if (checkedCheckBoxLen != null && checkedCheckBoxLen != undefined){
	 		checkedCheckBoxLen 	= document.querySelectorAll('input[name="Chk_insert"]:checked').length;
	 	}
 		 	if (checkboxTotalLen != checkedCheckBoxLen) {
		 		Error_Details(message_operations[765]);
		 	}
 	}
	if(errCtrlNum != "")
	{
		Error_Details(message_operations[34]+errCtrlNum.substring(1, errCtrlNum.length));
	}
	if(errorTissueParts != ""){
		Error_Details(Error_Details_Trans(message_operations[35],errorTissueParts));
	}
	if(errPnums != "")
	{
		Error_Details(message_operations[36]+errPnums.substring(1, errPnums.length));
	}else if(errLocPnums != ""){
		
		Error_Details(message_operations[37]+errLocPnums.substring(1, errLocPnums.length));
	}
	if(errFGLocType != '' && TxnType == '4112'){
		Error_Details(message_operations[38]+errFGLocType.substring(1, errFGLocType.length));
	}
	
	if(errInvLocType != '' && TxnType == '4112'){
		Error_Details(message_operations[39]+errInvLocType.substring(1, errInvLocType.length));
	}
	
	/*Qty validation*/
	var strAction = document.getElementById("haction").value;
		var strErrorMore = '';
		var strErrorLess = '';
		var strErrorQty = '';
		var strErrorLocQty = '';
		var strLowRWQty = '';
		var strErrRWQty = '';
	    var strErrorFGQty = '';
		var strMsg = '';
	    var arr = document.getElementById("strpnums").value.split(',');
	    var blk = '';
	    var qtycnt = 0;
	    var strPnum = '';
	    //hcnt++;
	    for(var i=0;i<arr.length-1;i++)
	    {
	       qid = arr[i];
	       pnum1 = qid.replace(/\./g,'');
	       blk = document.getElementById("hQty"+pnum1);
		    for(var j=0;j<=hcnt;j++)
		    {
				pnumobj = document.getElementById("hPartNum"+j);
				qtyobj = document.getElementById("Txt_Qty"+j);
				if (pnumobj&&qtyobj)
				{
					if (pnumobj.value == qid)
					{
						//if(!isNaN(qtyobj.value))
						//{
						
							qty = parseInt(qtyobj.value);
							if (qty > 0 && qty != "")
							{
								if(pickLocRule == "YES" && strAction == "EditControl"){
								 	var LocIdObj = document.getElementById("locDrpDn"+j); 
								 	var locDrpDn_len = LocIdObj.length ; 
								 	var LocId = LocIdObj.options[LocIdObj.selectedIndex].value;
								 	var locTxt = LocIdObj.options[LocIdObj.selectedIndex].text;
								 	var locCD = locTxt.substring(eval(locTxt.indexOf(":"))+1,locTxt.indexOf(" "));
								 	var locQty = locTxt.substring(eval(locTxt.indexOf("("))+1,locTxt.indexOf(")"));
								 	if(qty > eval(locQty)  && locDrpDn_len>1){//check location qty vs picked qty
										strErrorLocQty = strErrorLocQty + ","+locCD;
									}									
								}
								qtycnt = qtycnt + qty;
							}
						//}
						else
						{
							errQty += (errQty.indexOf(pnumobj.value) != -1)? "":", "+pnumobj.value;							
						}
					}
				} // IF Valid object
			} // END of Inner FOR loop  
		    if(strAction == "EditVerify")
		    {
				if (qtycnt > parseInt(blk.value))
				{
					strErrorMore  = strErrorMore + ","+ qid; //+"/"+cnumobj.value;
					
				}
				if (qtycnt < parseInt(blk.value) )
				{
					strErrorLess  = strErrorLess + ","+ qid; //+"/"+cnumobj.value;
				}	
			 }	
			qtycnt = 0;
		} // End of outer FOR loop	
	    // Start of Validation Logic that fires when we have enough RW qty and user selects FG location
	    if(pickLocRule == "YES" && strAction == "EditControl" ){
	    	if(TxnType == '4110'){
	    		var strFGPart = '';
	    		var strRWPart = '';
			    for(var k=0;k<=hcnt;k++){
		    		var totQty = 0;
			    	pnumobj = document.getElementById("hPartNum"+k);
			    	var whtypeObj = document.getElementById("hWareHouse"+k);
			    	var whtype = '';
			    	// to check the object exist
			    	if(whtypeObj !=null){
			    		whtype = whtypeObj.value;
			    	}
			    	
		    		qtyobj = document.getElementById("SpQty"+k);
		    		strPnum = (pnumobj)? pnumobj.value :'';		    		
		    		qtycnt = (qtyobj)? parseInt(qtyobj.innerHTML):0;
		    		var LocIdObj = document.getElementById("locDrpDn"+k); 
				 	var locDrpDn_len = 0;
				 	var locSelectedTxt = '';
				 	var locSelected ='';
				 	if(LocIdObj){
				 		locDrpDn_len = LocIdObj.length ;				 	
					 	locSelectedTxt = LocIdObj.options[LocIdObj.selectedIndex].text;
					 	locSelected = locSelectedTxt.substring(0,2);
					 	for(var s=0;s<locDrpDn_len;s++){
					 		var locTxt = LocIdObj.options[s].text;
					 		var whnm = locTxt.substring(0,2);
					 		var locQty = locTxt.substring(eval(locTxt.indexOf("("))+1,locTxt.indexOf(")"));
						 	if(whnm =='RW'){
						 		totQty += parseInt(locQty);
						 	}
					 	}
				 	}
				 	/*
				 	 * This code is disabled due the the validation happening if location qty > 0 without checking it that qty is allocated or not.
				 	if(totQty >= qtycnt && locSelected == 'FG'){
				 		if(strErrRWQty.indexOf(strPnum) == -1)
				 			strErrRWQty += "," + strPnum;
				 	}
				 	*/
				 	/*
				 	if(totQty > 0 && totQty< qtycnt && locSelected == 'FG'){
				 		strFGPart += "," + strPnum;
				 	}
				 	
				 	if(totQty > 0 && totQty< qtycnt && locSelected == 'RW'){
				 		strRWPart += strPnum + "|";
				 	}*/
				 	if(whtype == 56001 && locSelected == 'FG')
				 	{
				 		strErrRWQty += "," + strPnum;
				 	}
				 	
			    }
			    /*
			    strFGPart = strFGPart.substr(1,strFGPart.length);
			    //strRWPart = strRWPart.substr(1,strRWPart.length);
			    
			    var arrPnums = strFGPart.split(",");
			    for(var p=0;p<arrPnums.length;p++){
			    	 var strValu = "|" + arrPnums[p] + "|";
			    	 if(arrPnums[p] != '' && strRWPart.indexOf(strValu)==-1){
			    		 strErrorFGQty += "," + arrPnums[p];
			    	 }
			    }*/
	    	}
	    }
	    //End Validation Logic
	    /*
	     * This code is disabled due the the validation happening if location qty > 0 without checking it that qty is allocated or not.
	    if(strErrorFGQty!=''){
	    	Error_Details("Following part(s) " + strErrorFGQty.substr(1,strErrorFGQty.length) + " having few qty in <b>RW locations</b>. Please choose valid RW location and Split the rest to FG Locations");
	    }
	    if ( (errInvLocType != '' || strErrRWQty != '') && TxnType != '4112')
		{
			Error_Details("Following part(s) " + strErrRWQty.substr(1,strErrRWQty.length) + " having enough qty in <b>RW locations</b>. Please choose valid RW location");
		}
	    */
	    if (strErrRWQty != '')
	    {
	    	Error_Details(Error_Details_Trans(message_operations[40],strErrRWQty.substr(1,strErrRWQty.length)));
	    }
	    if (strErrorLocQty != '')
		{
			Error_Details(message_operations[41]+strErrorLocQty.substr(1,strErrorLocQty.length));
		}
	    if (errQty != '')
		{
			Error_Details(message_operations[42]+errQty.substr(1,errQty.length));
		}
	    
	    if (strErrorMore != '')
		{
	    	Error_Details(message_operations[43]+strErrorMore.substr(1,strErrorMore.length));
		}
	    // Checking all FG transcations  
		if (strErrorLess != '' && TransactionCode.indexOf("|"+txnPrefix+"|")!=-1)
		{
			Error_Details(message_operations[751]+strErrorLess.substr(1,strErrorLess.length));
		}
		
		if (ErrorCount > 0)
		{
			Error_Show();
			Error_Clear();
			return false;
		}

		document.getElementById("hCnt").value = hcnt;

		document.getElementById("inputString").value = inputString;
		if(fgBinString != ''){
			document.getElementById("fgBinString").value = fgBinString;
		}
		
		var htype = document.getElementById("txntype").value;
		var txnStatus = document.getElementById("txnStatus").value;
		var enableNOCFl = "";
		if(document.getElementById("hEnableNOCfl") != undefined){
	 		enableNOCFl = document.getElementById("hEnableNOCfl").value;
	 	}
	var txnId = '';
	if (document.getElementById("txnid") != null)
	{
		txnId = document.getElementById("txnid").value;
	}
	if(strAction == 'EditControl' || strAction == 'SaveControl'){		
			//document.getElementById("frmItemControl").action = "/gmItemControl.do";
			document.getElementById("loctype").value="93343"; //pick transacted
			document.getElementById("haction").value = "SaveControl";
			document.getElementById("chkRule").value = "Yes";
			if(vIncludeRuleEng != 'NO'){
				txnStatus = (document.getElementById("relverfl").checked)?"VERIFY":"PROCESS";
				//document.getElementById("frmItemControl").action = '/gmRuleEngine.do?hAction=SaveControl&hType='+htype+'&txnStatus='+txnStatus+'&hCnt='+parseInt(document.getElementById("hCnt").value);
				document.getElementById("frmItemControl").action = '/gmItemControl.do?hAction=SaveControl&txnID='+txnId+'&hType='+htype+'&txnStatus='+txnStatus+'&hCnt='+parseInt(document.getElementById("hCnt").value)+'&hEnableNOCfl='+enableNOCFl;
			}else{
				document.getElementById("frmItemControl").action = "/gmItemControl.do";
			}
	}else if(strAction == 'EditVerify'){
			//document.getElementById("frmItemControl").action = "/gmItemVerify.do";
			document.getElementById("loctype").value = "93345";  //put transacted
			document.getElementById("haction").value = "Verify"; 
			if(vIncludeRuleEng != 'NO'){
			txnStatus = "VERIFY";
				document.getElementById("frmItemControl").action = '/gmRuleEngine.do?hAction=Verify&txnID='+txnId+'&hType='+htype+'&txnStatus='+txnStatus+'&hCnt='+parseInt(document.getElementById("hCnt").value)+'&hEnableNOCfl='+enableNOCFl;
			}else{
				document.getElementById("frmItemControl").action = "/gmItemVerify.do";
			}
	}
	fnStartProgress('Y');
 	document.getElementById("frmItemControl").submit();
}

//PMT-48516
function fnVoid(){
	var txnId = document.getElementById("txnid").value;
	cancelType = (cancelType == '')?'VFGTL':cancelType;
	var txnType = document.getElementById("txntype");
	var voidtxnType;
	if(txnType!= null){
		voidtxnType =txnType.value;
	}
	
	if(voidtxnType == '100406'){
		if (confirm(message_operations[849]))
		{
			document.getElementById("frmItemControl").action="/GmCommonCancelServlet?hTxnName="+txnId+"&hCancelType="+cancelType+"&hTxnId="+txnId;
			document.getElementById("frmItemControl").submit();
	    }
	
	    }else{//PMT-50422 Unable to void inhouse transactions
		    document.getElementById("frmItemControl").action="/GmCommonCancelServlet?hTxnName="+txnId+"&hCancelType="+cancelType+"&hTxnId="+txnId;
		    document.getElementById("frmItemControl").submit();
	    }
}
function fnOpenTag(strPnum,strConId)
{
	windowOpener("/gmTag.do?strOpt=reload&pnum="+encodeURIComponent(strPnum)+"&refID="+strConId+"&refType=51000","PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=920,height=600");
}
function fnPicSlip(val)
{
	windowOpener("/GmConsignItemServlet?hAction=PicSlip&hConsignId="+val,"SetPic","resizable=yes,scrollbars=yes,top=150,left=150,width=750,height=500");
}
function fnRuleMessage(){
	var strAction = document.getElementById("haction").value;
	if(strAction == 'EditControl' || strAction == 'SaveControl'){
		var txnType = document.getElementById("txntype").value;
		var txnId = document.getElementById("txnid").value;
		if(txnType == "400063"){
			fnFetchRuleMessage("PROCESS","50932", txnId);
		}
	}
}

function fnPaperWork(){
	val = document.frmItemControl.txnid.value;
	type = document.frmItemControl.txntype.value;
	refid = document.frmItemControl.refid.value;
	contype = document.frmItemControl.contype.value;
	if(contype == '' || type == '93341'){ // 93341[FGRP]
		contype = 'T';
	}
	fnPicSlip(val,type,refid,contype); //function in GmDashboard.js
}

// Function to call on page load
function fnPageLoad(){
	var haction = document.all.haction.value;
	var strtxntype = document.frmItemControl.txntype.value;
	
	fnExistScanPartCount();
	
	//Need to check the "Release for Shipping/Verification" check box, once we control the transaction; 50261:Order(Only for order, status flag is 2 once we control the transaction)
	if((statusFl == 3 && haction == 'EditControl') || (statusFl == 4 && haction == 'EditVerify') || (statusFl == 2 && strtxntype == '50261') ){
		document.all.relverfl.checked = true;
		if(FGBinFl == "YES" && strtxntype != '4112' && strtxntype != '111800' && strtxntype != '111801' && strtxntype != '111802' && strtxntype != '111803' && strtxntype != '111804' && strtxntype != '111805'){ 
			//111800 - Quarantine - Inhouse ,111801 - Bulk - Inhouse, 111802 - Finished Goods - Inhouse, 111803 -Repackaging - Inhouse , 111804 - IH Inventory - Inhouse, 111805- Restricted Warehouse - Inhouse
			//Not able to add or remove FG Bin id, once the transaction is controlled
			document.all.Btn_Add.disabled = true;
			document.all.Btn_Remove.disabled = true;
			
		}
	}
	if(FGBinFl == "YES" && strtxntype != '4112' && strtxntype != '111800' && strtxntype != '111801' && strtxntype != '111802' && strtxntype != '111803' && strtxntype != '111804' && strtxntype != '111805'){ 	
		//111800 - Quarantine - Inhouse ,111801 - Bulk - Inhouse, 111802 - Finished Goods - Inhouse, 111803 -Repackaging - Inhouse , 111804 - IH Inventory - Inhouse, 111805- Restricted Warehouse - Inhouse
		fnSetFGBinId();//Call the function to set the FG Bin id list to the box, if FG Bin ids are already saved
	}
}

function fnValidateAllControlNo(pnum,ctrlNum,qty,pnumMaterial,checkVeriFl){
	var errparts = "";
	if(pnumMaterial == '100845' && qty > 1){ // 100845[Tissue Part]
		if(ctrlNum == ''){
			if(checkVeriFl)
				errparts += pnum + "  " + ctrlNum;
		}else{
			errparts += pnum + " --- " + ctrlNum;
		}		
	}
	// For validating the Duplicate COntrol Number on Submit
	if(pnumMaterial == '100845' && qty == 1 && ctrlNum != ''){ // 100845[Tissue Part]
		if(tissueControls.indexOf("|" + pnum +ctrlNum + "|") == -1){
			tissueControls = tissueControls + "|" + pnum +ctrlNum + "|";
		}else{
			errparts += pnum + " --- " + ctrlNum;
		}
	}
	return errparts;
}

/*fnCntrNumValidation()-calling while tabout from control number textbox in process transaction screen
 * */
function fnCntrNumValidation(varcount,cnum,pnum,lottrackfl){
 	var controlNUM = cnum.value.toUpperCase();
 	var ruleTransType = document.getElementById("RE_TXN").value;
	var TransTypeID = document.getElementById("TXN_TYPE_ID").value; 
	var strtxntype = document.frmItemControl.txntype.value;
	var v_fetch_message = false;
	/* change the LOT to uppercase if we tabouot from Control number screen
	 */
	controlNUM = TRIM(controlNUM);
	   if(controlNUM != ''){
			obj = eval("document.frmItemControl.Txt_CNum"+varcount);
			obj.value = controlNUM.toUpperCase();
		}
	/*if NOC# is not available then dont need to show the validation dispaly message
	 * */
	var v_noc_count = 0;
	var prevLoT = document.getElementById("hPreviousLot").value;
	if(controlNUM =='NOC#'){
		 v_noc_count++;
	}
	else if (prevLoT=='NOC#' && controlNUM != 'NOC#'){
		v_noc_count--;
	}
	/*if NOC# is not in the list for the lot track enable parts then hide the message section
	 *  */
	if (v_noc_count >0){
		if (v_enable_noc_txn.indexOf("|" + strtxntype + "|") != -1  
				&& controlNUM == 'NOC#' && lottrackfl == 'Y'){
			document.getElementById("displayMsg").style.display = 'block';
		}
	}
	if(v_noc_count==0 && prevLoT!= 'NOC#'){
		document.getElementById("displayMsg").style.display = 'none';
	}
		/*
		Rule message should fire in below conditions
		- if transaction is not IAFG (means not in v_enable_noc_txn)
		- if transaction is IAFG and lot number is not NOC#
		- if transaction is IAFG and lot number is NOC# and lot track is not enabled 
		*/ 
	
	if (vIncludeRuleEng != 'NO')
	{
		if(v_enable_noc_txn.indexOf("|" + strtxntype + "|") == -1 ){
			v_fetch_message = true;
		}
		else if (v_enable_noc_txn.indexOf("|" + strtxntype + "|") != -1 && controlNUM !='NOC#'){
			v_fetch_message = true;
		}
		else if (v_enable_noc_txn.indexOf("|" + strtxntype + "|") != -1 && controlNUM =='NOC#'  &&  lottrackfl != 'Y'){
			v_fetch_message = true;
		}
	}	
	if (v_fetch_message == true){
		fnFetchMessages(cnum,pnum,'PROCESS',ruleTransType,TransTypeID);
	}
	
	document.getElementById("hPreviousLot").value = controlNUM;
}

function fnUnAssignTxn(){
	var msg = confirm(message[10621]);
	var txnId = '';
	if (document.getElementById("txnid") != null)
	{
		txnId = document.getElementById("txnid").value;
	}
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmUnAssignItemControl.do?method=unAssignTransactionDetails&txnid='
			+ encodeURIComponent(txnId) +'&ramdomId='+Math.random());
	dhtmlxAjax.get(ajaxUrl, fnCallBack);
}

	function fnCallBack(loader) {
		var status = loader.xmlDoc.status;
		if (status == 200) {
			alert(msg);		
		} else {
			fnStopProgress();
		}
}

