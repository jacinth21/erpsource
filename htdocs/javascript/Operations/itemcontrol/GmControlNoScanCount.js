 

var newScannedQty = 0;
var newSingleScannedQty = 0;
var previousPart = ''; 
  
//this function used to get total scanned count and single part scanned count when after scanning
function fnScannedPartCount(pnum)
{	  
		var lblPartScanVal = document.getElementById("lblPartScan"+ pnum).innerHTML;
		lblPartScanVal = isNaN(parseInt(lblPartScanVal))?0:lblPartScanVal;
		document.getElementById("lblTotalS").value = parseInt(document.getElementById("lblTotalS").value) + 1;
		document.getElementById("lblPartScan"+ pnum).innerHTML = parseInt(lblPartScanVal) + 1; 
  
} 

//this function used to get total scanned count when valid control number scanned or page load
 function fnTotalScannedParts(scannedParts, newScannedQty){
 
	var count = 0;
	  for ( i = 0 ; i <= scannedParts; i++ )
	  {
		  controlnum = document.getElementById("hControl"+i).value; 
		  if(controlnum != '' && controlnum !='NOC#'&& controlnum !='TBE'){ 
				count = count + parseInt(document.getElementById("Txt_Qty"+i).value);	 
		  }
	  }
	document.getElementById("lblTotalS").value = count+newScannedQty;
}

//this function used to get single part scanned count when valid control number scanned
function fnSingleScannedPart(scannedParts, pnum, newSingleScannedQty){
 
	var count = 0;	
	  for ( i = 0 ; i <= scannedParts; i++ )
	  {
		  controlnum = document.getElementById("hControl"+i).value; 
		  partnum = document.getElementById("hPartNum"+i).value;

		  if(controlnum != ''&& controlnum !='NOC#'&& controlnum !='TBE'&& pnum == partnum){ 
				count= count + parseInt(document.getElementById("Txt_Qty"+i).value);	 
		  }
	  }  
	previousPart = pnum;
	document.getElementById("lblPartScan"+ pnum).innerHTML = count+newSingleScannedQty;
}

//this function used to get total scanned count and single part scanned count when delete scanned control number
function fnDeleteScanned(pnum,scanned)
{	
	if(scanned=='Y')
 	{	newScannedQty--;
		newSingleScannedQty--;  
		document.getElementById("lblTotalS").value = document.getElementById("lblTotalS").value - 1;
		document.getElementById("lblPartScan"+ pnum).innerHTML = parseInt(document.getElementById("lblPartScan"+ pnum).innerHTML) - 1; 
 	}
}

//this function used to get single part scanned count when page load
function fnExistSingleScannedPart(scannedParts){
 
	  var count = 0;
 	  var prePart = '';
	  var scanqty = 0;
	  for ( i = 0 ; i <= scannedParts; i++ )
	  {
		  controlnum = document.getElementById("hControl"+i).value; 
		  partnum = document.getElementById("hPartNum"+i).value;
		  scanqty = document.getElementById("Txt_Qty"+i).value;

		  if (prePart != partnum)
		  {	 
		  	count = 0;
		  }	
		  
		  if(controlnum != '' && controlnum !='NOC#'  && controlnum !='TBE' && scanqty =='1' ){ 
				  	count ++;
					document.getElementById("lblPartScan"+ partnum).innerHTML = count;
				 
		  }	 
	 
	 	  prePart = partnum;
	  } 	
}
 
//this function used to get total scanned count and single part scanned count when page load
function fnExistScanPartCount(){	 
	var scannedParts = document.getElementById("hCnt").value;
 	fnTotalScannedParts(scannedParts, 0);
 	fnExistSingleScannedPart(scannedParts);
	document.getElementById("lblTotalP").value = document.getElementById("hTotalParts").value;
}

