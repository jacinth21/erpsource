
//members 
this.InkeyArray = new Array(); // Location Keys 
this.InvalArray = new Array(); // Location Values 
       
//methods 
this.Insertput = Insertput; 
this.Insertget = Insertget; 
this.InsertfindIt = InsertfindIt;
function Insertput(key,val) 
{ 
  var elementIndex = this.InsertfindIt(key); 
   
  if( elementIndex == (-1) ) 
  { 
      this.InkeyArray.push( key ); 
      this.InvalArray.push( val ); 
  } 
  else 
  { 
      this.InvalArray[ elementIndex ] = val; 
  } 
} 

function Insertget(key) 
{ 
  var result = null; 
  var elementIndex = this.InsertfindIt( key ); 

  if( elementIndex != (-1) ) 
  {    
      result = this.InvalArray[ elementIndex ]; 
  } 
  else {
  	result = "<option value= >"+message_operations[424]+"</option>";
  }
  return result; 
}
function InsertfindIt(key) 
{ 
  var result = (-1); 

  for( var i = 0; i < this.InkeyArray.length; i++ ) 
  { 
      if( this.InkeyArray[ i ] == key ) 
      { 
          result = i; 
          break; 
      } 
  } 
  return result; 
} 

/*fnShowInsertLocDrpDwn(): Function to create the location dropdown
 */
function fnShowInsertLocDrpDwn(pnum,count,mapLocId){
	var obj="InlocDiv"+count;
	var drpdnStr = Insertget(pnum);
	var valStr = "value=\""+mapLocId + "\"";
	drpdnStr = (drpdnStr.indexOf(mapLocId)>0)?drpdnStr.replace(valStr,valStr+" selected"):drpdnStr;
	document.getElementById(obj).innerHTML='<select  insertid ='+pnum+' name="InlocDrpDn'+count+'" id="InlocDrpDn'+count+'" class=RightText tabindex="-1" onChange="fnRemovePickedInsert(this);">'+drpdnStr+'</select>';
}

/*fnsaveInsertDetails(): In Process Transaction screen, Insert section will be save by using the below Ajax call while checking the Checkbox
 */
function fnsaveInsertDetails(e,cnt,insertid ,revnum,location) {
	 var txnid =  document.getElementById("txnid").value;
	 var strtxntype = document.frmItemControl.txntype.value;
	 var checkFl  = e.checked;
	 location = document.getElementById("InlocDrpDn"+cnt); 
	 var LocId = location.options[location.selectedIndex].value;
	 LocId = (LocId=="0")?"":LocId;
	 
	 var ajaxUrl = fnAjaxURLAppendCmpInfo('gmItemControl.do?haction=saveInsertTrasactionDetails&insertid=' + encodeURIComponent(insertid) 
			 + '&revnum=' + encodeURIComponent(revnum) + '&location=' + encodeURIComponent(LocId)
			 + '&txnid='  + encodeURIComponent(txnid)+ '&txntype=' +strtxntype
			 + '&checkFl=' +checkFl +'&ramdomId=' + Math.random());
	 
	 document.getElementsByClassName(insertid+"img")[0].style.display="inline-block";
     dhtmlxAjax.get(ajaxUrl, function(loader){
     response = loader.xmlDoc.responseText;
     document.getElementsByClassName(insertid+"img")[0].style.display="none";
     });
}

/*
 * fnRemovePickedInsert(): If we change the location, then need to pick the Insert again,
 * so that unchecking the checkbox field in Insert screen
 * */
function fnRemovePickedInsert(obj)
{
	var insertid = obj.getAttribute('insertid');
	document.getElementsByClassName(insertid)[0].checked = false;
}