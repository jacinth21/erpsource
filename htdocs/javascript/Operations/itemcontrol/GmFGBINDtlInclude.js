
//Calling on page load to set the saved FG Bin ids to the combo box
function fnSetFGBinId() {
	document.getElementById("scanId").value = "";//To Set the value of "FG Bin Id" Text Box as empty
	document.getElementById("scanId").focus();//To Set the focus in of "FG Bin Id" Text Box

	var combo = document.getElementById("AssignedToCombo");
	var indexof =0;
	var fgbinId;
	var len = FGBinIdList.length;//Saved FG Bin list from the data base
	var tempFgBinList;
	tempFgBinList = FGBinIdList.split("|");//Used to change the '|' to ',' and make it as an array
	var tempLen=1;

	if(FGBinIdList != ''){//If not null, we need to set the values to the combo box
		//Loop through the FG Bin id and adding values to the combo box
		for(var i=0; i<tempFgBinList.length; i++){//Since from DB we are not getting '|' at last, no need to use length-1
			var opt = document.createElement("option");
			combo.options.add(opt);        
			opt.text = tempFgBinList[i];
			opt.value = tempFgBinList[i];
		}
	}
	
}

//To change the values inside the field to uppercase
function fnUpperCase(obj){
	obj.value = obj.value.toUpperCase();
}

//To add FG Bin id from text box to combo box, when click on "Add" button
function fnAddFGBinId(obj) {
	var combo = document.getElementById("AssignedToCombo");
	var items = document.getElementById("scanId");
	if(event.keyCode == 13 || obj.name == 'Btn_Add'){ //It should be called while scanning or pressing the button(event.keyCode == 13, represents scanning)
		fnValidateScanId(obj,items.value,combo);
	 
	}
	
} 



//To remove the FG Bin id, when click on "Remove" button
function fnRemoveFGBinId(obj){
	if(obj.value == 'Remove' ){// Should Process only If click on "Remove" button 
		var strAssignedID = document.getElementById("AssignedToCombo");
		var txnid = document.getElementById("txnid").value;
		var fgBinId = '';
		var fgBinIdStr = '';
		
		if(strAssignedID.value == ''){
			Error_Details(message[5023]);
		}
		if (ErrorCount > 0){
			Error_Show();
		    Error_Clear();
			return false;
		}
		if(strAssignedID.options.length > 0){
			for (var i = strAssignedID.options.length; i >=0; i--) {// Remove all the selected ids
				strAssignedID.remove(strAssignedID.selectedIndex);
			}
			
			
		}
		
	}
}

//Add the FG Bin ids from the text box to combo box
function addOption(selectbox, value, strComboNm){
	var opt = document.createElement("option");
	
	if(selectbox.options.length > 0 && strComboNm == 'AssignedToCombo'){
		for (var i = 0; i < selectbox.options.length; i++) {
			
			if(selectbox.options[i].text == value){
				Error_Details("\""+value+"\""+message_operations[23]);
				
			}
		}
	}
	if (ErrorCount > 0){
		document.getElementById("scanId").value = "";
		document.getElementById("scanId").focus();
		fnStopCommonProgress();
		Error_Show();
	    Error_Clear();
		return false;
	}
	selectbox.options.add(opt);        
	opt.text = value;
	opt.value = value;
	document.getElementById("scanId").value = "";
	document.getElementById("scanId").focus();
	fnStopCommonProgress();
	
}


//To validate FG Bin id
function fnValidateScanId(obj,fgbinid,combo){
	var indexof = '0';
	var type;
	
	if(obj.name != 'Btn_Add'){// While scanning only format will be "100~"
		indexof = fgbinid.indexOf("~"); // get the index of '~'
		type = fgbinid.substring(0, indexof); // get the characters before '~'
		indexof++;
	}
	First_char = fgbinid.substring(indexof,indexof+1); //First character should be T
	remain_char = fgbinid.substring(indexof+1,fgbinid.length); //Getting the remaining character, after "T"
	var scanid = fgbinid.substring(indexof,fgbinid.length);
	var msg = '';
	ErrorCount = 0;
	var isError = false; // To display the error message
	
	fnStartCommonProgress();
	
	// While scanning the FG Bin id will be something like '100~T23456', using '100' we can validate it. It should start with "100~" followed by 'T' and some numbers
			if((type != '100' && obj.name != 'Btn_Add') || First_char != 'T' || fgbinid == "" || remain_char == "" || isNaN(remain_char)){ 
				Error_Details(message[5024]);	
				isError = true;
			}else{//Need to check in the DB, whether FG Bin id is used by other transactions or not
				var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmItemControl.do?haction=ValidateFGBIN&Txt_FgBin='+scanid+ '&ramdomId=' + Math.random());
					dhtmlxAjax.get(ajaxUrl,function(loader){
						var response = loader.xmlDoc.responseText;
						var indexof;
						var errcode;
						var msg = ''; 
						if((response != null || response != '') ){
							indexof = response.indexOf(":");
							//errcode = response.substring(0, indexof);
							msg = (indexof != -1)?response.substring(indexof+1,response.length):'';//Getting the validation message from DB
							if(msg != ''){
								Error_Details(msg);
							}
						}
						if (ErrorCount > 0 ){
							document.getElementById("scanId").value = "";
							document.getElementById("scanId").focus();
							fnStopCommonProgress();
							Error_Show();
						    Error_Clear();
							return false;
							
						}else{
							
							addOption(combo,scanid,"AssignedToCombo");
						}
					});//End of DhtmlxAjax call function
				
					document.getElementById("scanId").value = scanid;
				
			}
			
			// used below codes to stop reloading the page after scanning the id
			if(!e) var e = window.event;

	        e.cancelBubble = true;
	        e.returnValue = false;	

	        if (e.stopPropagation) {
	                e.stopPropagation();
	                e.preventDefault();
	        }
	
	
	if(isError){
		if (ErrorCount > 0 ){
			document.getElementById("scanId").value = "";
			document.getElementById("scanId").focus();
			fnStopCommonProgress();
			Error_Show();
		    Error_Clear();
			return false;			
		}
	}
	
	
}

//To call the Start Progress method based on the Parent and child form
function fnStartCommonProgress(){
	if(window.parent.document.getElementById("refId")){
		window.parent.fnStartParentProgress();
	}else{
		fnStartProgress();
	}
}

//To call the Stop Progress method based on the Parent and child form
function fnStopCommonProgress(){
	if(window.parent.document.getElementById("refId")){
		window.parent.fnStopParentProgress();
	}else{
		fnStopProgress();
	}
}