var cnt = 0;
function fnRollback(){
	var conId=document.frmSetBuildProcess.consignID.value;
	var cnStatusId=document.frmSetBuildProcess.statusflag.value;
	var txnId = conId + '|' + cnStatusId;
	document.frmSetBuildProcess.action="/GmCommonCancelServlet?hTxnName=" + conId + "&hCancelType=RLBCK&hTxnId=" + txnId ;
	document.frmSetBuildProcess.submit();
}
function fnSplit(val,pnum, qty)
{
	var pnumwithoutdot = "bulkQty"+pnum.replace(/\./g,"");
	cnt++;
	var hcnt = parseInt(document.frmSetBuildProcess.hcounter.value) - 1;
	hcnt = hcnt+cnt;
	document.getElementById("hNewCnt").value = cnt;
	
	var NewQtyHtml = "<span border=1 id='Sp"+cnt+"'><BR>&nbsp;&nbsp;<input type='hidden' id="+pnum+" name="+pnum+" value="+hcnt+",/><input type='hidden' id='hControl"+hcnt+"' name='hControl"+hcnt+"' value=''>";
	NewQtyHtml += "<input type='hidden' name='"+pnumwithoutdot+"' id='"+pnumwithoutdot+"' value='"+qty+"'/>";
	NewQtyHtml += "<input type='hidden' name='bulkQty"+hcnt+"' value='"+0+"'><input type='hidden' name='partNum"+hcnt+"' value='"+pnum+"'><input type='hidden'  name='hPartNum"+hcnt+"' id='hPartNum"+hcnt+"' value='"+pnum+"' />";
	NewQtyHtml += "<input type='text' size='3' value='' name='Txt_Qty"+hcnt+"' id='Txt_Qty"+hcnt+"' alt='qty"+pnum+hcnt+"' class='InputArea' onFocus=changeBgColor(this,'#AACCE8'); onBlur=changeBgColor(this,'#ffffff'); >&nbsp;&nbsp;&nbsp;";
	NewQtyHtml += "<input type='text' size='10' value='' name='controlNum"+hcnt+"' id='controlNum"+hcnt+"' class='InputArea' onFocus=changeBgColor(this,'#AACCE8'); onBlur=changeBgColor(this,'#ffffff');>&nbsp;<a href=javascript:fnDelete("+cnt+");><img src=/images/btn_remove.gif border=0></a></span>";
	var QtyObj = eval("document.all.Cell"+val);
	QtyObj.insertAdjacentHTML("beforeEnd",NewQtyHtml);
}

function fnDelete(val)
{
	obj = eval("document.all.Sp"+val);
	obj.innerHTML = "";
}

function fnSubmit()
{
	Error_Clear();
	fnVerifyQty();
	if(document.frmSetBuildProcess.cbo_Type){
		var typeVal = document.frmSetBuildProcess.cbo_Type.value;
		if (typeVal == undefined  || typeVal  == null || typeVal  == ""  || typeVal==0){
			Error_Details(message_operations[797]);
		}
}
	fnCheckControlNumber();
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
		var inputstring = fnCreateInputString();
		document.frmSetBuildProcess.hinputstring.value = inputstring;
		document.frmSetBuildProcess.strOpt.value = 'save';
		objCompleteFlag = document.frmSetBuildProcess.completeFlag;
		if(objCompleteFlag)
		{
			if(document.frmSetBuildProcess.completeFlag.checked)
			{
				document.frmSetBuildProcess.txnStatus.value = 'VERIFY';				
			}
			else
			{
				document.frmSetBuildProcess.txnStatus.value = 'PROCESS';				
			}
			document.frmSetBuildProcess.action = '/gmRuleEngine.do';
		}
		fnStartProgress('Y');
		document.frmSetBuildProcess.submit();
	}

function fnCheckControlNumber(){
	var Flag = false;
	var varSetBuildAction = '';
	objCompleteFlag = document.frmSetBuildProcess.completeFlag;
	objVerifyFlag = document.frmSetBuildProcess.verifyFlag;
	if (objCompleteFlag){
		Flag = document.frmSetBuildProcess.completeFlag.checked;
		varSetBuildAction = 'controlling';
		}
	else if(objVerifyFlag){
		Flag = document.frmSetBuildProcess.verifyFlag.checked;
		varSetBuildAction = 'verifying';
		}	
	if(Flag){	
			var controlNum = '';
			varRowCnt = parseInt(document.frmSetBuildProcess.hcounter.value);
			varRowCnt = varRowCnt + cnt;
			var blCnumFlag = false;
			
			for (k=0; k < varRowCnt; k ++)
			{
				objControlNum = eval("document.frmSetBuildProcess.controlNum"+k);
				if(objControlNum != undefined && objControlNum != null){
					controlNum = objControlNum.value;
					if((controlNum == 'TBE') || (controlNum == '')){
						blCnumFlag = true;
						break;
					}
				} // end if undefined check
			}
			
			if (blCnumFlag){
				Error_Details(Error_Details_Trans(message[5050],varSetBuildAction));
			}
		}
	}

function fnCreateInputString(){
	var token = '^';
	var pnum = '';
	var qty = '';
	var controlnum = '';
	var inputstr = '';
	var str = '';
	var typeVal = '';
	var varRowCnt = parseInt(document.frmSetBuildProcess.hcounter.value);
	varRowCnt = varRowCnt + cnt;

	for (k=0; k < varRowCnt; k ++)
	{
	//alert(' k ' + k );
		obj = eval("document.frmSetBuildProcess.partNum"+k);
	//	alert(' pnum ' + obj.value);
	if(obj != undefined && obj != null ){
		pnum = obj.value;
		objqty = eval("document.frmSetBuildProcess.Txt_Qty"+k);
		qty = parseFloat(objqty.value);
		objControlNum = eval("document.frmSetBuildProcess.controlNum"+k);
		if(document.frmSetBuildProcess.cbo_Type){
			 typeVal = document.frmSetBuildProcess.cbo_Type.value;
		}	
		controlnum = TRIM(objControlNum.value);   // remove extra space in control number used to PC-3860 
		
		if(!isNaN(qty) && qty > 0)
		{
			str = pnum + token + qty + token + controlnum + token + typeVal +  '|';
			inputstr = inputstr + str;
		}// end if qty (NaN check)
	} // end if (undefined check)
	}
//	alert(inputstr);
	return inputstr;
}

var prevtr = 0;

function chgTRBgColor(val,BgColor) {
   var obj1 = eval("document.all.tr"+prevtr);
   obj1.style.background = "";
   var obj = eval("document.all.tr"+val);
   prevtr = val;
   obj.style.background = BgColor;
}	

function fnVoidSetCsg()
{
	document.frmSetBuildProcess.action =serveletPath+"/GmCommonCancelServlet";
	document.frmSetBuildProcess.hAction.value = "Load";
	document.frmSetBuildProcess.submit();
}

function fnVoid()
{

		document.frmSetBuildProcess.action =serveletPath+"/GmCommonCancelServlet";
		document.frmSetBuildProcess.hTxnId.value = document.frmSetBuildProcess.requestId.value;
		document.frmSetBuildProcess.hCancelType.value = "VDREQ";
		document.frmSetBuildProcess.hAction.value = 'Load';
		document.frmSetBuildProcess.submit();
}	

function fnPicSlip()
{
	var val = document.frmSetBuildProcess.consignID.value;
	windowOpener(serveletPath+"/GmSetBuildServlet?hAction=PicSlip&hConsignId="+val,"SetPic","resizable=yes,scrollbars=yes,top=150,left=150,width=670,height=500");
}

function fnLoad(){
	document.frmSetBuildProcess.strOpt.value = "";
	fnStartProgress('Y');
	document.frmSetBuildProcess.submit();
}

function fnBlankLine(varCounter,value)
{
	if(value == 'TBE'){
		obj = eval("document.frmSetBuildProcess.controlNum"+varCounter);
		obj.value = '';
	}
	chgTRBgColor(varCounter,'#AACCE8');
}

function fnBlankTBE(varCounter,value){
		if(value == ''){
				obj = eval("document.frmSetBuildProcess.controlNum"+varCounter);
				obj.value = 'TBE';
			}
			
			chgTRBgColor(varCounter,'#AACCE8');
	}

function fnVerifyQty(){
	var arr = document.frmSetBuildProcess.hPartList.value.split(',');
	var arrlen = arr.length - 1;
    var blk = '';
    var cnum = '';
    var qtycnt = 0;
	var strErrorMore = '';
	var pnum = '';
	var pnumwithoutdot = '';
	var editErrFl = false;
	var maxSetQtyFl = false;
	var qtyEmptyFl = false;
	var qtyEmptyStr = '';
	var nonNumericStr = '';
	var qtyEditStr = '';
	var varRowCnt = parseInt(document.frmSetBuildProcess.hcounter.value);
	varRowCnt = varRowCnt + cnt;
    varRowCnt++;

    for(var i=0;i<arrlen;i++)
    {
       var duplicatePartFl = false;
       pnum = arr[i];
       //  For remove special characters in Part Numbers
       pnumwithoutdot = replacePartSpecialchars(pnum);
       blk = eval("document.frmSetBuildProcess.setQty"+pnumwithoutdot);
       var bulkQtyVal = 0;
		    for(var j=0;j<varRowCnt;j++)
		    {
				pnumobj = eval("document.frmSetBuildProcess.partNum"+j);
				if (pnumobj)
				{
					if (pnumobj.value == pnum)
					{
						qtyobj = eval("document.frmSetBuildProcess.Txt_Qty"+j);
						var bulkQtyObj = eval("document.frmSetBuildProcess.bulkQty"+j);
						cnumobj = eval("document.frmSetBuildProcess.controlNum"+j);
						var qtyObjVal = TRIM(qtyobj.value);
						if(!duplicatePartFl && (qtyObjVal =='' || qtyObjVal == 0)){
							qtyEmptyFl = true;
							duplicatePartFl = true;
							qtyEmptyStr += pnum +', ';
						}
						//Validate the Non Numeric char
						var strQty = qtyObjVal.replace(/[0-9]+/g,''); // Qty text box
 						if(strQty.length >0){
							nonNumericStr += pnum +', ';
						}
						// validate the Qty changes.
						bulkQtyVal = parseInt(bulkQtyVal) + parseInt(bulkQtyObj.value);
						if(!isNaN(qtyObjVal))
						{
							qty = parseInt(qtyobj.value);
							cnum = cnumobj.value;
							if (qty >= 0)
							{
								qtycnt = qtycnt + qty;
							}
						}
					}
				}
			}
		if (qtycnt > parseInt(blk.value) ||  (qtycnt != 0 && qtycnt != bulkQtyVal))
		{
			maxSetQtyFl = true;
			strErrorMore  = strErrorMore + pnum +', ';
		}
		qtycnt = 0;
	}
	 
	if (maxSetQtyFl || editErrFl){
			strErrorMore = strErrorMore.substring(0,(strErrorMore.length)-2);
			Error_Details(Error_Details_Trans(message[5051],strErrorMore));
	}	
	if(nonNumericStr !=''){
		nonNumericStr =	nonNumericStr.substring(0,(nonNumericStr.length)-2);
		Error_Details(Error_Details_Trans(message[5052],nonNumericStr));
	}
	if(qtyEmptyFl){
		qtyEmptyStr = qtyEmptyStr.substring(0,(qtyEmptyStr.length)-2);
		Error_Details(Error_Details_Trans(message[5053],qtyEmptyStr)); 
	}
}

function fnCallTransactionSearch()
{
windowOpener('/GmConsignSearchServlet?hOpt=4110',"Returns","resizable=yes,scrollbars=yes,top=150,left=200,width=660,height=400");
}
var vsetId = '<%=strSetId%>';


function fnOpenTag(strPnum,strConId, refType)
{
	windowOpener("/gmTag.do?strOpt=reload&pnum="+encodeURIComponent(strPnum)+"&refID="+strConId+"&refType="+refType,"OpenTag","resizable=yes,scrollbars=yes,top=250,left=300,width=920,height=600");
}
