function fnSubmit(){
	document.frmAccount.hAction.value = "LoadNCMRDetails";
	fnStartProgress();
	document.frmAccount.submit();
}

function fnSort(val){
	document.frmAccount.hSort.value = val;
	if (val == strSortCName){
		if (document.frmAccount.hSortAscDesc.value == '1'){
			document.frmAccount.hSortAscDesc.value = '2'
		}else{
			document.frmAccount.hSortAscDesc.value = '1'
		}
	}else{
		document.frmAccount.hSortAscDesc.value = '1';
	}	
	document.frmAccount.hAction.value = "LoadNCMRDetails";
	fnStartProgress('Y');
	document.frmAccount.submit();
}

var prevtr = 0;
function changeTRBgColor(val,BgColor) {
   var obj1 = eval("document.all.tr"+prevtr);
   obj1.style.background = "";
   var obj = eval("document.all.tr"+val);
   prevtr = val;
   obj.style.background = BgColor;
}
function fnViewDetails(hNCMRID){
	//windowOpener("/GmARSummaryServlet?hAction=LoadARDetail&hAccountID="+hAccountId+"&hReportType="+hReport,"AR Details","resizable=yes,scrollbars=yes,top=300,left=300,width=670,height=200");
	windowOpener("/GmNCMRServlet?hAction=P&hNCMRId="+hNCMRID,"INVPOP","resizable=yes,scrollbars=yes,top=200,left=150,width=800,height=535");
}
function fnLoad(){
	document.frmAccount.Cbo_Type.value = Cbo_Type;
	document.frmAccount.Cbo_TypeNew.value = Cbo_TypeNew;
}


function fnPrintDownVer(action){	
	var partNums = document.frmAccount.Txt_PartNum.value;
	var PartName = document.frmAccount.Txt_PartName.value;
	var vendorNm = document.frmAccount.Txt_VendorName.value;
	var nonConDesc = document.frmAccount.Txt_NonConf.value;
	var ncmrID = document.frmAccount.Txt_NCMRNO.value;
	var evalID = document.frmAccount.Txt_EVALID.value;
	var dhrID = document.frmAccount.Txt_DHRNO.value;
	var frmDate = document.frmAccount.Txt_FromDate.value;
	var toDate = document.frmAccount.Txt_ToDate.value;
	var status = document.frmAccount.Cbo_Type.value;
	var type = document.frmAccount.Cbo_TypeNew.value;	
	vendorNm = vendorNm.replace('&','@');
	fnStartProgress('Y');
	windowOpener("/GmNCMRReportServlet?hAction="+action+"&Txt_FromDate="+frmDate+"&Txt_ToDate="+toDate+"&Txt_PartNum="+encodeURIComponent(partNums)+"&Txt_PartName="+PartName+"&Txt_VendorName="+vendorNm+"&Cbo_Type="+status+"&Txt_NCMRNO="+ncmrID+"&Txt_NonConf="+nonConDesc+"&Txt_DHRNO="+dhrID+"&Txt_EVALID="+evalID+"&Cbo_TypeNew="+type,"NCMRPRINT","resizable=yes,scrollbars=yes,top=300,left=200,width=1200,height=490");
	fnStopProgress();
}

var gridObj;
function fnOnPageLoad() {
	fnLoad();
	if (objGridData.value != '') {
		gridObj = initGridData('NCMRRpt', objGridData);
	}
}
//To initiate the dhtmlx grid
function initGridData(divRef, gridData) {
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/extweb/dhtmlx/dhtmlxGrid/imgs/");
	gObj.init();
	gObj.loadXMLString(gridData);
    gObj.attachHeader('#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#select_filter,#select_filter,#select_filter,#select_filter,#text_filter,#text_filter,#text_filter');
    return gObj;
}



