//This function is used fetch and display lead time report data when load button clicked
function fnReportLoad(){
		var vendorId = document.frmVendorWOLeadTimeRpt.vendorId.value;
		var partNum = TRIM(document.frmVendorWOLeadTimeRpt.partNums.value);
		var purchaseAgent = document.frmVendorWOLeadTimeRpt.strPurAgent.value;
		var division = document.frmVendorWOLeadTimeRpt.strDivision.value;
		var projectId = document.frmVendorWOLeadTimeRpt.projectID.value;
		var strPartSearch = document.frmVendorWOLeadTimeRpt.strPartSearch.value;
		
		if(vendorId == '' && partNum == '' && purchaseAgent == '0' && division == '0' && projectId == ''){
			Error_Details(message_operations[882]);
		}
		if (ErrorCount > 0){
		Error_Show();
	    Error_Clear();
		return false;
		}
		
		fnStartProgress();
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmVendorWOLaedTimeRpt.do?method=fetchVendorWOLeadTimeRpt&projectID='
			+ projectId
			+ '&partNums='
			+ partNum
			+ '&strPurAgent='
			+ purchaseAgent
			+ '&strDivision='
			+ division
			+ '&vendorId='
			+ vendorId
			+ '&likeSearch='
			+ strPartSearch
			+ '&ramdomId=' + new Date().getTime());
	dhtmlxAjax.get(ajaxUrl, fnReportResponse)
}

//Populate leadtime report data 
function fnReportResponse(loader){
	response = loader.xmlDoc.responseText;
	fnStopProgress();
	if (response != ""){
			var setInitWidths = "200,150,100,250,*,100,120,150,120";
			var setColAlign = "left,left,left,left,left,left,right,left,left";
			var setColTypes = "ro,ro,ro,ro,ro,ro,ro,ro,ro";
			var setColSorting = "str,str,str,str,str,str,int,str,str";
			var setHeader = ["Vendor Name", "Purchase Agent Name","Part Number", "Part Description", "Project Name", "Division", "Lead Time (Weeks)","Updated By", "Updated Date"];
			var setFilter = ["#text_filter", "#select_filter", "#text_filter","#text_filter", "#text_filter", "#select_filter","#numeric_filter", "#text_filter", "#rspan"];
			var setColIds = "vendornm,purchaseAgentnm,partnum,pdesc,projnm,divnm,leadtime,updatedby,updateddate";
			var enableTooltips = "true,true,true,true,true,true,true,true,true,true";
			var gridHeight ="600";
			// to set auto height
			if($(document).height() !=undefined || $(document).height() !=null){
				gridHeight = $(document).height() - 200 ;
			}

			document.getElementById("trDiv").style.display = "table-row";
			document.getElementById("NothingFound").style.display = 'none';
			document.getElementById("ExcelExport").style.display = "table-row";
			//split the functions to avoid multiple parameters passing in single function
			gridObj = initGridObject('dataGridDiv');
		    gridObj = fnCustomTypes(gridObj,setHeader, setColIds, setInitWidths, setColAlign, setColTypes,setColSorting, enableTooltips, setFilter,gridHeight);
		   // gridObj.enableDistributedParsing(true);
		    gridObj = loadDhtmlxGridByJsonData(gridObj,response);
	}else{
	        document.getElementById("trDiv").style.display = 'none';	
	        document.getElementById("NothingFound").style.display = 'table-row';
	        document.getElementById("ExcelExport").style.display = 'none';
   }
}

//to download excel
function fnExport() {
	gridObj.toExcel('/phpapp/excel/generate.php');
}

//This Function Used to set custom types conditions in Grid
function fnCustomTypes(gObj,setHeader, setColIds, setInitWidths, setColAlign, setColTypes,setColSorting, enableTooltips, setFilter,gridHeight){

	if (setHeader != undefined && setHeader.length > 0) {
		gObj.setHeader(setHeader);
 		var colCount = setHeader.length;	
	}    

	if (setInitWidths != "")
		gObj.setInitWidths(setInitWidths);

	if (setColAlign != "")
		gObj.setColAlign(setColAlign);

	if (setColTypes != "")
		gObj.setColTypes(setColTypes);

	if (setColSorting != "")
		gObj.setColSorting(setColSorting);

	if (enableTooltips != "")
		gObj.enableTooltips(enableTooltips);

	if (setFilter != "")
		gObj.attachHeader(setFilter);

	if(setColIds != "")
		gObj.setColumnIds(setColIds);

	if (gridHeight != "")
		gObj.enableAutoHeight(true, gridHeight, true);

	return gObj;
}