function fnReset()
{
	document.frmTTPGroupMap.ttpId.value = "";
	document.frmTTPGroupMap.ttpName.value = "";
	document.frmTTPGroupMap.ttpOwner.value = "0";
	document.frmTTPGroupMap.submit();	
}	

function fnFetch()
{	
	if(document.frmTTPGroupMap.ttpId.value=="")
	{
		document.frmTTPGroupMap.ttpName.value = "";
		document.frmTTPGroupMap.ttpOwner.value = "0";
	}
	fnStartProgress('Y');
	document.frmTTPGroupMap.strOpt.value = "edit";
	document.frmTTPGroupMap.submit();
}

function fnSubmit()
{
	fnValidateTxtFld('ttpName',' TTP Name ');
	fnValidateDropDn('ttpOwner',' TTP Owner ');
	
	
	varcheckcnt = 0;
	objCheckRIArr = document.frmTTPGroupMap.checkSelectedDS;
	objCheckRIArr1 = document.frmTTPGroupMap.checkUnSelectedDS
	if(objCheckRIArr) {	 
		 
		 	objCheckRIArrLen = objCheckRIArr.length;
			for(i = 0; i < objCheckRIArrLen; i ++){	
					if(objCheckRIArr[i].checked){
						varcheckcnt ++;
						break;
					}
				}
				if (objCheckRIArr.type == 'checkbox' && objCheckRIArr.checked)
				{
		 			varcheckcnt ++;
				}
		 }
		 
	if(objCheckRIArr1) {	 
		 
		 	objCheckRIArrLen = objCheckRIArr1.length;
			for(i = 0; i < objCheckRIArrLen; i ++){	
					if(objCheckRIArr1[i].checked){
						varcheckcnt ++;
						break;
					}
				}
				if (objCheckRIArr1.type == 'checkbox' && objCheckRIArr1.checked)
				{
		 			varcheckcnt ++;
				}
		 }
		 
				if (varcheckcnt == 0){
				 
					Error_Details("Please check at least one available sheet or associated sheet.");
				}
				
		if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	document.frmTTPGroupMap.strOpt.value = 'save';
	fnStartProgress('Y');
	document.frmTTPGroupMap.submit();
}

function fnVoid()
{
		fnValidateDropDn('ttpId',' TTP List ');
		if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
		document.frmTTPGroupMap.action =servletpath+"/GmCommonCancelServlet";
		document.frmTTPGroupMap.hTxnId.value = document.frmTTPGroupMap.ttpId.value;
		document.frmTTPGroupMap.hTxnName.value = document.frmTTPGroupMap.ttpName.value;
		document.frmTTPGroupMap.submit();
}

function fnCheckSelections()
{
	objCheckDSArrLen = 0;
	
	objCheckDSArr = document.frmTTPGroupMap.checkSelectedDS;
	if(objCheckDSArr) {
	if (objCheckDSArr.type == 'checkbox')
	{
		objCheckDSArr.checked = true;
	}
	else {
		objCheckDSArrLen = objCheckDSArr.length;
		for(i = 0; i < objCheckDSArrLen; i ++) 
			{	
				objCheckDSArr[i].checked = true;
			}
		}
	}
	
}	
