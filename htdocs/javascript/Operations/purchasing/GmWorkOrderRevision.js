var gridObj;
var check_rowId = '';
var rowID = '';
var woid_rowId = '';
var inputStr = '';
var checkVal = '';
var woId = '';
var response ='';
var part = "";
var partDesc = "";
var project = "";
var poID = "";
var woID = "";
var woRev = "";
var newRev = "";
var action = "";
var ignoreReason = "";
var ignoreCmnts = "";
var performedBy = "";
var performedDt = "";
var woQty = "";
var dhrQty = "";
var revisionBy = "";
var revisionDt = "";
var varStatus = "";
var woRevId="";

//on page Load 
function fnOnPageLoad() {

	document.getElementById("dataGridDiv").style.display = 'none';
	document.getElementById("DivExportExcel").style.display = 'none';
	document.getElementById("DivNothingMessage").style.display = 'none';
	if (status == "108242") {
		document.getElementById("buttonDiv").style.display = 'none';
	}	
	
}
// when click load - load the revision uodated details 
function fnLoad() {
	var projectId = document.frmWORevision.projectId.value;
	var partNum = document.frmWORevision.pnum.value;
	var pnumSuffix = document.frmWORevision.pnumSuffix.value;
	var poId = document.frmWORevision.poId.value;
	// reset the response values
	response = '';
	
	if(status == '108242'){
		fnStartProgress();
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmWoRevision.do?method=reloadWORevisionRpt&projectId='+ projectId+
				'&pnum='+ partNum+ '&pnumSuffix='+ pnumSuffix+ '&poId='+ poId+'&status='+ status+'&ramdomId=' + new Date().getTime());
		dhtmlxAjax.get(ajaxUrl, fnLoadWORevisionDtls)
	
	}else{
	var actionId = document.frmWORevision.woActionId.value;
	var fromDtObj = document.frmWORevision.dtWOFromDate;
	var toDtObj = document.frmWORevision.dtWOToDate;
	var fromDt = fromDtObj.value;
	var toDt = toDtObj.value;
	
	if(fromDtObj.value != ""){
		CommonDateValidation(fromDtObj,format,Error_Details_Trans(message[5535],format));
	}
	if(toDtObj.value != ""){
		CommonDateValidation(toDtObj,format,Error_Details_Trans(message[5536],format));
	}	
	if (dateDiff(fromDt, toDt, format) < 0) {
		Error_Details(message_operations[504]);
	}
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	fnStartProgress();
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmWoRevision.do?method=reloadWORevisionRpt&projectId='
			+ projectId
			+ '&pnum='
			+ partNum
			+ '&pnumSuffix='
			+ pnumSuffix
			+ '&poId='
			+ poId
			+ '&woActionId='
			+ actionId
			+ '&dtWOFromDate='
			+ fromDt
			+ '&dtWOToDate='
			+ toDt
			+ '&status='
			+ status
			+ '&ramdomId=' + new Date().getTime());
	dhtmlxAjax.get(ajaxUrl, fnLoadWORevisionDtls)
	}
}
//this Function Used to generate the grid 
function fnLoadWORevisionDtls(loader) {
	
 response = loader.xmlDoc.responseText;
	fnStopProgress();
	if (response != ""){
		if (status == "108242") {
			fnLoadWODashboard();
		  }else{
			  fnLoadWOReport();
		  }
	}else{
		document.getElementById("dataGridDiv").style.display = 'none';
		document.getElementById("DivNothingMessage").style.display = 'block';
		document.getElementById("DivExportExcel").style.display = 'none';
		if (status == "108242") {
		 document.getElementById("buttonDiv").style.display = 'none';
		}
	}
}

function fnLoadWODashboard(){

	
	var strJsonString = '[' + response + ']';
	//to form the grid string 
	var rows = [], i = 0;
	var JSON_Array_obj = "";
	JSON_Array_obj = JSON.parse(strJsonString);
	$.each(JSON_Array_obj, function(index, jsonObject) {
		part = jsonObject.PART_NUM;
		partDesc = jsonObject.PART_DESC;
		project = jsonObject.PROJECTNAME;
		poID = jsonObject.PO_ID;
		woID = jsonObject.WO_ID;
		woRev = jsonObject.REV_NUM;
		newRev = jsonObject.NEW_REV;
		revisionBy = jsonObject.REVUPDBY;
		revisionDt = jsonObject.REVUPDDT;
		woQty = jsonObject.WOQTY;
		dhrQty = jsonObject.DHRQTY;
		varStatus = jsonObject.STATUS;
		woRevId=jsonObject.WOREVID;
		vendorNm=jsonObject.VENDOR;
		agentNm=jsonObject.AGENT;		
		// default array
		rows[i] = {};
		rows[i].id = woRevId;
		rows[i].data = [];
		
		// PC-1753: Add vendor and Agent name
		
			rows[i].data[0] = '';
			rows[i].data[1] = vendorNm;
			rows[i].data[2] = agentNm;
			rows[i].data[3] = part;
			rows[i].data[4] = partDesc;
			rows[i].data[5] = project;
			rows[i].data[6] = "<a href='#' onClick=fnCallPO('" + poID + "')><u>" + poID + "</u></a>";
			rows[i].data[7] = woID;
			rows[i].data[8] = woRev;
			rows[i].data[9] = newRev;
			rows[i].data[10] = revisionBy;
			rows[i].data[11] = revisionDt;
			rows[i].data[12] = woQty;
			rows[i].data[13] = dhrQty;
		
		i++;

	});

	 // to load open status details for work order revision dashboard
		var setInitWidths = "49,100,100,75,150,150,100,100,55,50,85,85,50,*";
		var setColAlign = "center,left,left,left,left,left,left,left,center,center,left,center,right,right";
		var setColTypes = "ch,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ron,ron";
		var setColSorting = "na,str,str,str,str,str,str,str,str,str,str,str,int,int";
		var setHeader = [ "Select All", "Vendor","Purchasing Agent","Part #", "Part Description",
				"Project", "PO ID", "WO ID", "WO Rev", "New Rev",
				"Revision Updated By", "Revision Updated Date", "WO Qty",
				"DHR Qty" ];
		var setFilter = [
				"<input type='checkbox' value='no'  name='selectAll' onClick='javascript:fnChangedFilter(this);'/>",
				"#text_filter", "#text_filter","#text_filter", "#text_filter", "#select_filter",
				"#text_filter", "#text_filter", "#text_filter",
				"#text_filter", "#select_filter", "#select_filter",
				"#numeric_filter", "#numeric_filter" ];
		var setColIds = "chk_box,vendorNm,agentNm,partNum,partDesc,project,poId,woId,woRev,newREv,performedBy,performedDt,woQty,dhrQty";
		var enableTooltips = "true,true,true,true,true,true,true,true,true,true,true,true,true,true";

		var footerArry = [];
		var gridHeight = "";
		var footerStyles = [];
		var footerExportFL = true;
		var dataHost = {};
		dataHost.rows = rows;
		document.getElementById("buttonDiv").style.display = 'block';
		document.getElementById("dataGridDiv").style.display = 'block';
		document.getElementById("DivNothingMessage").style.display = 'none';
		document.getElementById("DivExportExcel").style.display = 'block';
		gridObj = loadDHTMLXGrid('dataGridDiv', gridHeight, dataHost,
				setHeader, setInitWidths, setColAlign, setColTypes,
				setColSorting, setColIds, enableTooltips, setFilter,
				footerArry, footerStyles, footerExportFL);
		gridObj.attachEvent("onCheckbox", doOnCheck);
		check_rowId = gridObj.getColIndexById("chk_box");
		woid_rowId = gridObj.getColIndexById("woId");
 

}

function fnLoadWOReport(){

	var strJsonString = '[' + response + ']';
	//to form the grid string 
	var rows = [], i = 0;
	var JSON_Array_obj = "";
	JSON_Array_obj = JSON.parse(strJsonString);
	$.each(JSON_Array_obj, function(index, jsonObject) {
		part = jsonObject.PART_NUM;
		partDesc = jsonObject.PART_DESC;
		project = jsonObject.PROJECTNAME;
		poID = jsonObject.PO_ID;
		woID = jsonObject.WO_ID;
		woRev = jsonObject.REV_NUM;
		newRev = jsonObject.NEW_REV;
		action = jsonObject.ACTION;
		ignoreReason = jsonObject.REASON;
		ignoreCmnts = jsonObject.COMMENTS;
		performedBy = jsonObject.PERFORMED_BY;
		performedDt = jsonObject.performed_date;
		varStatus = jsonObject.STATUS;
		woRevId=jsonObject.WOREVID;
		vendorNm=jsonObject.VENDOR;
		agentNm=jsonObject.AGENT;
		// default array
		rows[i] = {};
		rows[i].id = woRevId;
		rows[i].data = [];
		
		// PC-1753: Add vendor and Agent name
		
			rows[i].data[0] = vendorNm;
			rows[i].data[1] = agentNm;
			rows[i].data[2]= part;
			rows[i].data[3] = partDesc;
			rows[i].data[4] = project;
			rows[i].data[5] = "<a href='#' onClick=fnCallPO('" + poID + "')><u>" + poID + "</u></a>";
			rows[i].data[6] = woID;
			rows[i].data[7] = woRev;
			rows[i].data[8] = newRev;
			rows[i].data[9] = action;
			rows[i].data[10] = varStatus;
			rows[i].data[11] = ignoreReason;
			rows[i].data[12] = ignoreCmnts;
			rows[i].data[13] = performedBy;
			rows[i].data[14] = performedDt;

		i++;

	});

// to load other than open status details in work order revision update report
		
		var setInitWidths = "100,100,75,115,100,100,100,50,50,50,60,60,75,75,*";
		var setColAlign = "left,left,left,left,left,left,left,center,center,center,center,left,left,left,center";
		var setColTypes = "ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro";
		var setColSorting = "str,str,str,str,str,str,str,str,str,str,str,str,str,str,date";
		var setHeader = ["Vendor","Purchasing Agent", "Part #", "Part Description", "Project", "PO ID",
				"WO ID", "WO Rev", "New Rev", "Action", "Status",
				"Ignore Reason", "Ignore Comments", "Performed By",
				"Performed Date" ];
		var setFilter = [ "#text_filter", "#text_filter","#text_filter", "#text_filter", "#select_filter",
				"#text_filter", "#text_filter", "#text_filter",
				"#text_filter", "#select_filter", "#select_filter",
				"#select_filter", "#text_filter", "#select_filter",
				"#select_filter" ];
		var setColIds = "";
		var enableTooltips = "true,true,true,true,true,true,true,true,true,true,true,true,true,true";
		var footerArry = [];
		var gridHeight = "";
		var footerStyles = [];
		var footerExportFL = true;
		var pagination = "";
		var dataHost = {};
	
		dataHost.rows = rows;
		document.getElementById("dataGridDiv").style.display = 'block';
		document.getElementById("DivNothingMessage").style.display = 'none';
		document.getElementById("DivExportExcel").style.display = 'block';
		gridObj = loadDHTMLXGrid('dataGridDiv', gridHeight, dataHost,
				setHeader, setInitWidths, setColAlign, setColTypes,
				setColSorting, setColIds, enableTooltips, setFilter,
				footerArry, footerStyles, footerExportFL, pagination);
}

// when click Update button - This function for to update the Revision status
function fnRevisionUpdate() {
	// to reset the input string values
	inputStr = '';
	
	var projectId = document.frmWORevision.projectId.value;
	var partNum = document.frmWORevision.pnum.value;
	var pnumSuffix = document.frmWORevision.pnumSuffix.value;
	var poId = document.frmWORevision.poId.value;
	//get list of changed rows included added rows
	var checked_row = gridObj.getCheckedRows(check_rowId);
	if (checked_row == '') {
		Error_Details(message_operations[796]);
	}
	if (checked_row != '') {
		var chkVal = checked_row.split(",");
		for (var i = 0; i < chkVal.length; i++) {
			checkVal = gridObj.cellById(chkVal[i], check_rowId).getValue();
			woId = gridObj.cellById(chkVal[i], woid_rowId).getValue();
			if (checkVal == 1) {
				inputStr += woId + ',';
			}
		}
	}

	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	fnStartProgress();
	
	// This is save parts - change the AJAX type to post method 
	var ajaxUrl = '/gmWoRevision.do?method=saveWORevision';
	var paramString = '&workOrderId='+ inputStr+'&projectId=' + projectId+ '&pnum='	+ partNum + '&pnumSuffix='	+ pnumSuffix + '&poId='	+ poId	+ '&status=' + status+ '&'+fnAppendCompanyInfo()+ '&ramdomId=' + new Date().getTime();
	
	// reset the response values
	response = '';

	dhtmlxAjax.post(ajaxUrl, paramString, fnLoadWORevisionDtls);

}

//when click Update button - This function for to ignored the Revision update
function fnRevisionIgnore() {
	// reset the string
	inputStr = '';
	
	var checked_row = gridObj.getCheckedRows(check_rowId);
	if (checked_row == '') {
		Error_Details(message_operations[796]);
	}
	if (checked_row != '') {
		var chkVal = checked_row.split(",");
		for (var i = 0; i < chkVal.length; i++) {
			checkVal = gridObj.cellById(chkVal[i], check_rowId).getValue();
			woId = gridObj.cellById(chkVal[i], woid_rowId).getValue();
			if(checkVal == 1){
				inputStr += woId + ',';
			}
			
		}
	}

	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	
	document.frmWORevision.action = "/GmCommonCancelServlet?hCancelType=IGWORE&hTxnId="+ inputStr;
	document.frmWORevision.submit();
}
// to download excel
function fnDownloadXLS() {
	if (status == '108242') { // hidden first column for dashboard report 
		gridObj.setColumnHidden(0, true);
	}
	gridObj.toExcel('/phpapp/excel/generate.php');
	gridObj.setColumnHidden(0, false);

}

//this function for to populate Purchase order view screen
function fnCallPO(poId) {
	windowOpener("/GmPOServlet?hPOId=" + poId + "&hAction=ViewPO",
			"View PO Details",
			"resizable=yes,scrollbars=yes,top=150,left=200,width=900,height=600");

}
//Function is used to Check Box Check
function doOnCheck(rowId, cellInd, stage) {
	rowID = rowId;
	if (cellInd == check_rowId) {
		fnSelectAllToggle('selectAll');
	}
	return true;
}

// Description : This function to check the select all check box
function fnSelectAllToggle(varControl) {
	var objControl = eval("document.frmWORevision." + varControl);

	var all_row = gridObj.getAllRowIds(",");
	var all_rowLen = all_row.length; // get the rows id length
	var checkRowID = check_rowId;
	var checked_row = gridObj.getCheckedRows(checkRowID);
	var finalval;
	finalval = eval(checked_row.length);
	if (all_rowLen == finalval) {
		objControl.checked = true;// select all check box to be checked
	} else {
		objControl.checked = false;// select all check box un checked.
	}
}

//this function used to select all option (check box)
function fnChangedFilter(obj) {
	if (obj.checked) {
		gridObj.forEachRow(function(rowId) {
		   var check_disable_fl = gridObj.cellById(rowId, check_rowId).isDisabled();
		    if (!check_disable_fl) {
				gridObj.cellById(rowId, check_rowId).setChecked(true);
			}
		});
	}else{
		gridObj.forEachRow(function(rowId) {
			check_id = gridObj.cellById(rowId, check_rowId).getValue();
			if (check_id == 1) {
				gridObj.cellById(rowId, check_rowId).setChecked(false);
			}

		});
	}
}


//This function used to press the enter key then submit the page.
function fnEnter() {
	if (event.keyCode == 13) {	
		fnLoad();
	 }		
}