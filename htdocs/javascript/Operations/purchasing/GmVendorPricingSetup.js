var arrValuefromClipboard = new Array();
var chk_Active_ColId='';
var gridObj ='';
var all_rowLen ='';
var orginal_row_cnt='';
var finalPdId='';
var existFl = false;
var changeFl = false;
var filterMode = false;
//This function is used to activate Load button while pressing the enter key
function fnEnter() {
	if (event.keyCode == 13) {
		//fnLoad();  //commented to fix BUG-5402
	}
}

// this function is used to fetch vendor pricing details
function fnLoad(){
	
	var projectId = document.frmVendorPricingSetup.projectId.value;
	var partNum = document.frmVendorPricingSetup.partNumber.value;
	// if partnumber field is empty setting dropdown value as '0'-[ChooseOne] //to fix BUG-5405
	if(TRIM(partNum) == ''){
		document.frmVendorPricingSetup.search.value = '0';
	}
	
	if(projectId == '0'){
		Error_Details(message_operations[44]);
	}
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	
	document.frmVendorPricingSetup.haction.value= 'load';
	document.frmVendorPricingSetup.strOpt.value= 'fetch';
	fnStartProgress('Y');
	document.frmVendorPricingSetup.submit();
}

//this function is used to fetch vendor pricing details block by block
function doOnDistributedEnd(){
	document.getElementById("processimg").style.display="none";
    document.getElementById("Btn_Submit").disabled=false;
	gridObj.refreshFilters();
}

// fnOnPageLoad function
function fnOnPageLoad(){ 
	
	if(msgFl=='block'){
		document.getElementById("progress").style.display="block";
		document.getElementById("msg").style.color="green";
		document.getElementById("msg").style.display="inline";		
		document.getElementById("msg").innerHTML= message_operations[45];
	}
	
	if(document.getElementById("divGrid") != undefined)
	{
//		gridObj = setDefalutGridProperties('divGrid');
        gridObj = new dhtmlXGridObject('divGrid');
		gridObj.setImagePath("/images/dhtmlxGrid/imgs/");
		gridObj.enableTooltips("true,true,true,true,false,false,true,false,true,false,false,false,false,flase");
		gridObj.attachHeader('#text_filter,#text_filter,#select_filter,#text_filter,#numeric_filter,#numeric_filter,#text_filter,#text_filter,#select_filter,#numeric_filter,'
				+ ","
				+ "&nbsp;<input  type='checkbox'  value='no' name='selectAllActiveFiles' onClick='javascript:fnCheckActiveFlag(this);'/>,"
				+ ',,,');
		gridObj.init();
		gridObj.enableAlterCss("even","uneven");		
//		gridObj.copyBlockToClipboard();
		gridObj.enableMultiline(false);	
		gridObj.enableEditEvents(false, true, true);
		gridObj.enableDistributedParsing(true,100,0);
//		gridObj.loadXMLString(objGridData);
        gridObj.parse(objGridData, "xml");
        
		document.getElementById("Btn_Submit").disabled=false;
		gridObj.attachEvent("onDistributedEnd", doOnDistributedEnd);
		gridObj.attachEvent("onKeyPress", onKeyPressed);
		gridObj.attachEvent("onEditCell",doOnCellEdit);
		gridObj.setImagePath("/images/dhtmlxGrid/imgs/");  
		gridObj.attachEvent("onRowPaste",function(id){
             gridObj.forEachCell(id,function(obj){
                   obj.cell.wasChanged = true;
                   });
			});
		gridObj.attachEvent("onCheckbox", doOnCheck);		
		chk_Active_ColId = gridObj.getColIndexById("active");
		
		gridObj.enableMultiselect(true);
		gridObj.setAwaitedRowHeight(200);
		//gridObj.submitOnlyChanged(true);
		gridObj.submitOnlyRowID(false);
		gridObj.enableAutoHeight(true,300,false);		
		existFl = true; // on page load we are not filtering, so this flag should be true
		gridObj.enableBlockSelection(true);
	}
	
	fnShowInactVend(document.frmVendorPricingSetup.showInactiveVendor);
	     
	//search value set as dynamic
	if(selSearch != ''){
		document.frmVendorPricingSetup.search.value = selSearch;
	}
	//locked value set as dynamic
	if(selLocked != ''){		
		document.frmVendorPricingSetup.locked.value = selLocked;
	}	
	//active value set as dynamic
	if(selActive != ''){		
		document.frmVendorPricingSetup.active.value = selActive;
	}
	document.frmVendorPricingSetup.msgFl.value = "none";
    
    fnADDRows();
}
// when we click on Active check box below function is caling
function doOnCheck(rowId, cellInd, stage) {
	
	var gridrows =gridObj.getAllRowIds(",");
	var gridrowsarr = gridrows.split(",");
	all_rowLen = (gridrows != "")?gridrowsarr.length:0;
	orginal_row_cnt = all_rowLen;
	setRowAsModified(rowId,true);
	if(cellInd == chk_Active_ColId){
		fnActiveSelectAll('selectAllActiveFiles');
	}
	return true;
}

function fnADDRows(){
   
        gridObj.startFastOperations();
        // maxCount Using For Default Row Size
        for (var i = 0; i < maxCount; i++){
            addRow();
        }
        gridObj.stopFastOperations();
}

//This function to check the select all active check box
function fnActiveSelectAll(name) {
	var objControl = eval("document.frmVendorPricingSetup." + name);
	var checked_row = gridObj.getCheckedRows(chk_Active_ColId);
	var finalval;
	var ary_checked_row =checked_row.split(",");
	finalval = eval(ary_checked_row.length);

	if (orginal_row_cnt == finalval) {
		gridObj.setRowAttribute(ary_checked_row[0],"row_modified","Y");
		objControl.checked = true;// select all check box to be checked
		setRowAsModified(ary_checked_row[0],true);
	} else {
		objControl.checked = false;// select all check box un checked.
	}
}

//This function used to Check all the check boxs for ShareReqFlag.
function fnCheckActiveFlag(obj) {
	var checkColumnID = chk_Active_ColId;
	gridObj.forEachRow(function(rowId) {
			temppartid = TRIM(gridObj.cellById(rowId, gridObj.getColIndexById("part_no")).getValue());
			if (temppartid !== '') {
				gridObj.cellById(rowId, checkColumnID).setChecked(obj.checked);
				gridObj.setRowAttribute(rowId,"row_modified","Y");
				setRowAsModified(rowId,true);
			}
		});

}
// This function is used to add row in grid
function addRow(){
	gridObj.filterBy(0,"");//unfilters the grid while adding rows
	gridObj._f_rowsBuffer = null; //clears the cache
	gridObj.addRow(gridObj.getUID(),'');
	var gridrows = gridObj.getAllRowIds();
	var rowsarr = gridrows.toString().split(",");
	
	var selVendorId = document.frmVendorPricingSetup.vendorId;
	var selVendorTxt = "";
	 
	if(selVendorId.value != '0'){
		selVendorTxt = selVendorId.options[selVendorId.selectedIndex].text;
	}
	
	//10- Lock, 11-Active
	if (rowsarr.length > 0) {
		for ( var rowid = 0; rowid < rowsarr.length; rowid++) {
			var rowcurrentid = rowsarr[rowid];
			if(gridObj.cells(rowcurrentid, gridObj.getColIndexById("part_no")).getValue()==''){
					var cel_lock=gridObj.cells(rowcurrentid,10);
			        var cell_act=gridObj.cells(rowcurrentid,11);
			        cel_lock.setValue('N'); 
		            if (cell_act.isCheckbox()) cell_act.setValue(1);
		            gridObj.setRowAttribute(rowcurrentid,"row_added","Y");
		            gridObj.setRowAttribute(rowcurrentid,"row_modified","Y");
					//When a new row is added, Defaulting the dropdwon values as [choose one].
		        	gridObj.cellById(rowcurrentid, gridObj.getColIndexById("part_no")).setValue('0');
		        	gridObj.cellById(rowcurrentid, gridObj.getColIndexById("vendor")).setValue('0');
		        	gridObj.cellById(rowcurrentid, gridObj.getColIndexById("uom")).setValue('0');
		        	gridObj.cellById(rowcurrentid, gridObj.getColIndexById("h_filter_by")).setValue('1');// hidden column value setting to '1' to filter grid
		        	// if grid doesn't have vendor nmae and highlevel vendor filter selected, setting same in the grid
		       	   if(selVendorId.value !='0'){
		       		  gridObj.cellById(rowcurrentid, gridObj.getColIndexById("vendor")).setValue(selVendorTxt);
		       	   }
			}
		}
		/*Below code is to filter the grid when user click on addrow button, there is no defalut
		functionality to filter, when the grid is in filter mode changes adding/deleteing are not consider.
		So to overcome this we are calling below function manually to apply previous filter	
		*/
		gridObj.filterByAll();// filters the grid back
		if(filterMode){	// if the grid is in filter mode then only this function will call
			filterMode = false; // to re apply the filter to grid we are setting false
			filterData();// setting grid filter
		}
	}
	
}
//this function is used to perform action when a cell is editing
function doOnCellEdit(stage, rowId, cellInd, nValue, oValue) {	
	//0-Part Number,2-vendor,4-cost,5-Qty quoted,6-Delivery Time Frame,7-Quote ID,8-UOM,9-UOM Qty				  
	if (stage == 2 && nValue != oValue && (cellInd == '0' || cellInd == '2') && nValue != ''){
		gridObj.cellById(rowId, gridObj.getColIndexById("validate")).setValue('');
		gridObj.cellById(rowId, gridObj.getColIndexById("verify")).setValue('');
		gridObj.setRowAttribute(rowId,"row_modified","Y");
	} 	
	
	if(cellInd == '0' || cellInd == '2' || cellInd == '4' || cellInd == '5' || cellInd == '6' || cellInd == '7' || cellInd == '8' || cellInd == '9' ){
		var lockFl =  gridObj.cells(rowId, gridObj.getColIndexById("lock")).getValue();
		var validFl =  gridObj.cells(rowId, gridObj.getColIndexById("validate")).getValue();
		if((stage == '0' && lockFl=='Y') || (stage == '0' && (cellInd == '0' || cellInd == '2') && validFl != '')){
			return false;
		}
		if( stage == 2 ){
			gridObj.setCellTextStyle(rowId,cellInd,"background-color:#D8D8D8;");
			//gridObj.setCellTextStyle(rowId,cellInd,"");
		}
	}
	//stage-2 : after edit
	if( stage == 2 ){
		gridObj.clearSelection();
	}
	/*Below condition is to filter the grid once data is modified, since there is no defalut
	functionality to filter, when the grid is in filter mode changes adding/deleteing are not consider.
	So to overcome this we are using hidden column value as either '0' or '1'.
    Here '0' for existing rows if there is no modified, and '1' is for newly added rowswith existing modified rows.
	*/
	if( stage == 2 &&  nValue != oValue ){
		gridObj.cellById(rowId, gridObj.getColIndexById("h_filter_by")).setValue('1');
	}
	if (stage == 1 && (cellInd == '4' || cellInd == '5' || cellInd == '6' || cellInd == '7' ||  cellInd == '9') ){
		var c=this.editor.obj;			
		if(c!=null) c.select();
	}
		
	return true;
}
//this function used to copy the grid data 
function onKeyPressed(code, ctrl) {
	if (code == 67 && ctrl) {
		if (!gridObj._selectionArea)
			return false;
		gridObj.setCSVDelimiter("\t");
		gridObj.copyBlockToClipboard();
//		gridObj._HideSelection();
	}
	if(code==86&&ctrl){
		gridObj.setCSVDelimiter("\t")
		gridObj.pasteBlockFromClipboard();
//		gridObj._HideSelection();
	}
	gridObj.refreshFilters();
	return true;
}
//delete the selected row from Grid
function removeSelectedRow(){
	var deleteFl = false;
	var lockFlVal = 'N';
	var gridrows=gridObj.getSelectedRowId();
	if(gridrows!=undefined){
		var NameCellValue ;
		var gridrowsarr = gridrows.toString().split(",");
		var added_row;
		
		for (var rowid = 0; rowid < gridrowsarr.length; rowid++) {
			gridrowid = gridrowsarr[rowid];
			added_row = gridObj.getRowAttribute(gridrowid,"row_added");
			lockFlVal =  gridObj.cells(gridrowid, gridObj.getColIndexById("lock")).getValue();
			
			if(added_row!=undefined && added_row=="Y"){
				gridObj.setRowAttribute(gridrowid,"row_modified","N");
				gridObj.deleteRow(gridrowid);
			}else if(lockFlVal!="Y"){	//instead of deleting existing rows from grid, we are setting delete flag as Y along with color
				gridObj.lockRow(gridrowid,true);
				gridObj.setRowColor(gridrowid,"pink");
				gridObj.setRowAttribute(gridrowid,"row_deleted","Y");
				setRowAsModified(gridrowid,true);	
			}			
		}	
		gridObj.clearSelection();
	}
	else{		
		Error_Details(message_operations[46]);	
	}
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
}
//this function is used to enable selected row
function enableSelectedRow(){
	var gridrows	= gridObj.getSelectedRowId();
	if(gridrows!=undefined){
		
		var gridrowsarr = gridrows.toString().split(",");
		
		for (var rowid = 0; rowid < gridrowsarr.length; rowid++) {
			gridrowid = gridrowsarr[rowid];
			gridObj.lockRow(gridrowid,false);
			gridObj.setRowColor(gridrowid,"");
			gridObj.setRowAttribute(gridrowid,"row_deleted","");
			
			setRowAsModified(gridrowid,false);
		}
	}else{
		Error_Clear();
		Error_Details(message_operations[47]);				
		Error_Show();
		Error_Clear();
	}
}
//Use the below function to copy data inside the screen.
function docopy() {
	gridObj.setCSVDelimiter("\t");
	gridObj.copyBlockToClipboard();
	arrValuefromClipboard = new Array();
	if (gridObj._selectionArea != null) {
		var area = gridObj._selectionArea
		var leftTopCol = area.LeftTopCol;
		var leftTopRow = area.LeftTopRow;
		var rightBottomCol = area.RightBottomCol;
		var rightBottomRow = area.RightBottomRow;
		for ( var i = leftTopRow; i <= rightBottomRow; i++) {
			for ( var j = leftTopCol; j <= rightBottomCol; j++) {
				setValuefromClipboard = gridObj.cellByIndex(i, j).getValue();
				arrValuefromClipboard.push(setValuefromClipboard);
			}
		}
	}
	gridObj._HideSelection();
}
//to copy the clip board data to grid
function pasteToGrid() {
	var partInd = gridObj.getColIndexById("part_no");
	var vendInd = gridObj.getColIndexById("vendor");
	var costInd = gridObj.getColIndexById("cost");
	var qtyInd = gridObj.getColIndexById("qty_quoted");
	var dtFrmInd = gridObj.getColIndexById("dt_frame");
	var qIDInd = gridObj.getColIndexById("quote_id");                
	var uomInd = gridObj.getColIndexById("uom");
	var uomQtyInd = gridObj.getColIndexById("uom_qty");
	
	if (gridObj._selectionArea != null) {
		var colIndex = gridObj._selectionArea.LeftTopCol;	
		if(colIndex!=undefined && colIndex != partInd && colIndex != vendInd && colIndex != costInd && colIndex != qtyInd && 
				colIndex != dtFrmInd && colIndex != qIDInd && colIndex != uomInd && colIndex != uomQtyInd){
			alert(message_operations[48]);
		}else{
			var area = gridObj._selectionArea
			var leftTopCol = area.LeftTopCol;
			var leftTopRow = area.LeftTopRow;
			var rightBottomCol = area.RightBottomCol;
			var rightBottomRow = area.RightBottomRow;
			for ( var i = leftTopRow; i <= rightBottomRow; i++) {
				var m = 0;			
				for ( var j = leftTopCol; j <= rightBottomCol; j++) {
					gridObj.cellByIndex(i, j).setValue(arrValuefromClipboard[m]);
					m++;
				}
			 }
			//When a cell value is copied, need to set the cell is changed attribute.
			 for ( var k = leftTopRow; k <= rightBottomRow; k++) {
					setRowAsModified(gridObj.getRowId(k),true);										
			}
		}
		gridObj._HideSelection();
		gridObj.refreshFilters();
	} else {
		alert(message_operations[49]);
	}
}

function setRowAsModified(rowid,modified) {
	var filetrValue = modified?'1':'0';	
	gridObj.forEachCell(rowid,function(obj){
		// hidden column value setting to '1' to filter grid
	   gridObj.cellById(rowid, gridObj.getColIndexById("h_filter_by")).setValue(filetrValue);
       obj.cell.wasChanged = modified;
		});
}

//addRowFromClipboard() is used to copies multiple data from Excel Sheet
function addRowFromClipboard(){
	
	var cbData = gridObj.fromClipBoard();
    if (cbData == null) {
          Error_Details(message_operations[50]);
    }
    var selVendorId = document.frmVendorPricingSetup.vendorId;
	var selVendorTxt = "";
	 
	if(selVendorId.value != '0'){
		selVendorTxt = selVendorId.options[selVendorId.selectedIndex].text;
	}
    var rowData = cbData.split("\n");
    var rowcolumnData = '';
    var arrLen = (rowData.length) - 1;
    
    if (ErrorCount > 0) {
          Error_Show();
          Error_Clear();
          return;
    }

	fnStartProgress('Y');
    document.getElementById("processimg").style.display="block";
    document.getElementById("Btn_Submit").disabled=true;
    var newRowData = '';
    setTimeout(function() {	
    gridObj.startFastOperations();
	gridObj.filterBy(0,"");//unfilters the grid
  	gridObj._f_rowsBuffer = null; //clears the cache
    for ( var i = 0; i < arrLen; i++) {
    	
          newRowData = rowData[i];
          newRowData = newRowData.replace(/\n|\r/g,"");
          var rowcolumnData = '';
          newRowData = newRowData +'	N'+'	1';
         
          var row_id = gridObj.getUID();
          gridObj.setCSVDelimiter("\t");
          gridObj.setDelimiter("\t");
          gridObj.addRow(row_id, newRowData);
          // hidden column value setting to '1' to filter grid
          gridObj.cellById(row_id, gridObj.getColIndexById("h_filter_by")).setValue('1');
          var gridVendVal =  gridObj.cells(row_id, gridObj.getColIndexById("vendor")).getValue();
          // if grid doesn't have vendor nmae and highlevel vendor filter selected, setting same in the grid
     	  if(selVendorId.value !='0' && (gridVendVal =='')){
     		 gridObj.cellById(row_id, gridObj.getColIndexById("vendor")).setValue(selVendorTxt);
     	  }
          gridObj.setRowAttribute(row_id, "row_added", "Y");
          gridObj.setRowAttribute(row_id,"row_modified","Y");
          gridObj.setDelimiter(",");
    }
    gridObj.stopFastOperations();
	fnStopProgress();
	/*Below code is to filter the grid once data is copied from excel, there is no defalut
	functionality to filter, when the grid is in filter mode changes adding/deleteing are not consider.
	So to overcome this we are calling below function manually to apply previous filter	
	*/
	 gridObj.filterByAll();
	    if(filterMode){	// if the grid is in filter mode before adding data then only this function will call
			filterMode = false; // to re-apply previous filter to grid we are setting false
			filterData();
		}
	    
	},100);
    document.getElementById("Btn_Submit").disabled=false;
    document.getElementById("processimg").style.display="none";
}
//save function
function fnSave(){
	
	var projectId = document.frmVendorPricingSetup.projectId.value;
	if(projectId == '0'){
		Error_Details(message_operations[44]);
	}	
	//close opened editor and return value from editor to the cell
	gridObj.editStop();
	
	var vendorIDdrpVal = document.frmVendorPricingSetup.vendorId;
	var vendorTextVal = "";
	if(vendorIDdrpVal.value != '0'){
		vendorTextVal = vendorIDdrpVal.options[vendorIDdrpVal.selectedIndex].text;
	}
	
	var str = '';
	var cost = '';
	var pId = '';
	var pnum = '';
	var qty_quote = '';
	var dt_frm = '';
	var quote_id = '';
	var uom = '';
	var uom_qty = '';
	var lockFl = '';
	var activeFl = '';	
	var vend = '';
	var woDuplicateErrFl= false;
	var priceDuplicateErrFl= false;
	var validateErrFl= false;
	var vendEmpFl = false;
	var voidConfirmFl = false;
	var submitFl = false;
	//get list of changed rows included added rows
	var gridChrows =gridObj.getChangedRows(',');	
	if(gridChrows.length==0){
		Error_Details(message_operations[51]);
	}
	if (ErrorCount > 0 )
	{
		Error_Show();
		Error_Clear();
		return false;
	}else{	
		
		document.getElementById("progress").style.display="none";
		document.getElementById("msg").innerHTML="";
		document.getElementById("Btn_Submit").disabled=true;
		
		var gridChrows =gridObj.getChangedRows(true);	
		var rowsarr = gridChrows.toString().split(",");
			
		if (rowsarr.length > 0) {
		
			for ( var rowid = 0; rowid < rowsarr.length; rowid++) {
				var row_voided ='N';
				var rowcurrentid = rowsarr[rowid];
				// getting delete fl and sending this along with inputstring 
				row_voided = gridObj.getRowAttribute(rowcurrentid,"row_deleted");
				if(row_voided=='Y'){
					voidConfirmFl = true;
				}else{
					row_voided ='N';
				}
				pnum = gridObj.cells(rowcurrentid, gridObj.getColIndexById("part_no")).getValue();
				if(pnum != '' && pnum != 0){					 
					/*vend = (vendorIDdrpVal != '0')?vendorTextVal:vend;
					gridObj.cells(rowcurrentid, gridObj.getColIndexById("vendor")).setValue(vend);*/
					vend =  gridObj.cells(rowcurrentid, gridObj.getColIndexById("vendor")).getValue();		
					
					if(vend == '' && vendorTextVal == ''){
						vendEmpFl = true;
					}
					if(vend == ''){
						vendEmpFl = true;
						gridObj.setCellTextStyle(rowcurrentid, gridObj.getColIndexById("vendor") ,"color:red;border:1px solid red;");
						gridObj.setRowColor(rowcurrentid,"#F8E0E0");
					}
					/*The below code is commented to fix - BUG-5372
					if(vendorIDdrpVal.value != '0'  && vendorTextVal != ''){
						//when top level vendor is selected, pre selecting the individual pricing records with the same vendor.
						gridObj.cells(rowcurrentid, gridObj.getColIndexById("vendor")).setValue(vendorTextVal);
						vend = vendorTextVal;						
					}*/
					curr =  gridObj.cells(rowcurrentid, gridObj.getColIndexById("currency")).getValue();
				
					cost =  gridObj.cells(rowcurrentid, gridObj.getColIndexById("cost")).getValue();
					qty_quote =  gridObj.cells(rowcurrentid, gridObj.getColIndexById("qty_quoted")).getValue();       
					dt_frm = gridObj.cells(rowcurrentid, gridObj.getColIndexById("dt_frame")).getValue();
					quote_id =  gridObj.cells(rowcurrentid, gridObj.getColIndexById("quote_id")).getValue();
					uom =  gridObj.cells(rowcurrentid, gridObj.getColIndexById("uom")).getValue();
					uom_qty =  gridObj.cells(rowcurrentid, gridObj.getColIndexById("uom_qty")).getValue();
					lockFl =  gridObj.cells(rowcurrentid, gridObj.getColIndexById("lock")).getValue();
					activeFl =  gridObj.cells(rowcurrentid, gridObj.getColIndexById("active")).getValue(); 
					pId	= gridObj.cells(rowcurrentid, gridObj.getColIndexById("pid")).getValue(); 				
					activeFl = (activeFl == '0')?'N':'Y';			
					
					str = str+pnum+'^'+vend+'^'+cost+'^'+qty_quote+'^'+dt_frm +'^'+quote_id+'^'+uom+'^'+uom_qty+'^'+lockFl+'^'+activeFl+'^'+pId+'^'+rowcurrentid+'^'+row_voided+'|';
				
					finalPdId = finalPdId +rowcurrentid+',';
				}
			}
		  }
		
		if(voidConfirmFl){
			submitFl = confirm(message_operations[52]);
			if(!submitFl){
				document.getElementById("Btn_Submit").disabled=false;
				return false;
			}
		}
		if(str != '' && !vendEmpFl){
			var loader = dhtmlxAjax.postSync('/gmVendorPricingSetup.do','haction=save&projectId='+projectId+'&vendorId='+vendorIDdrpVal.value+'&inputString='+encodeURIComponent(str)+'&randomid='+Math.random()+'&'+fnAppendCompanyInfo());
			var status = loader.xmlDoc.status;
			var responseTextData = loader.xmlDoc.responseText;	
			if (status == 200) {
				var SavedDet = loader.xmlDoc.responseText;
				gridObj.clearChangedState();
				document.frmVendorPricingSetup.msgFl.value = "block";
				fnLoad();
			}else if(status == 300){  ///AJAX reponse Error status that is explicitly set as 300 in action.
				var errorDet = loader.xmlDoc.responseText;
				var errorPart = errorDet.split('|');
				var errSize = errorPart.length; 
				for(i=0;i<errSize-1;i++){
					if(errorPart[i] !=''){
						currRow = errorPart[i].split('^');
						currlen = currRow.length;
						if(currlen>1){
							for(j=1;j<currlen;j++){
								if(currRow[j] != ''){
									if (currRow[j] =='DUP_PRC_ERR' ){
										gridObj.setRowColor(currRow[0],"F5A9A9");
										priceDuplicateErrFl  = true;
									}else if ( currRow[j] =='WO_ERR'){
										gridObj.setRowColor(currRow[0],"F5A9A9");
										woDuplicateErrFl = true;
									}else{
										gridObj.setCellTextStyle(currRow[0],gridObj.getColIndexById(currRow[j].toLowerCase())
												,"color:red;border:1px solid red;");
										validateErrFl=true;
									}
								} 
							}
							gridObj.setRowColor(currRow[0],"#F8E0E0");
						}
					}
				}
			}
		}
		if(str == ''){
			Error_Details(message_operations[51]);
		}
		if(woDuplicateErrFl){
	    	Error_Details(message_operations[53]);
		}
		if(priceDuplicateErrFl){
	    	Error_Details(message_operations[54]);
		}		
		if(validateErrFl){
	    	Error_Details(message_operations[55]);
		}
		if(vendEmpFl){
			Error_Details(message_operations[56]);
		}
		if (ErrorCount > 0) {
			document.getElementById("Btn_Submit").disabled=false;
			Error_Show();
			Error_Clear();
			return false;
		}
		
	}
}

//This function is used to show vendors based on the vendor checkbox 
function fnShowInactVend(obj){
	var actFl1 = obj.checked;
	var selVendorId = document.frmVendorPricingSetup.vendorId.value;
	
	if(actFl1){
			
			document.frmVendorPricingSetup.vendorId.options.length = 0;
			document.frmVendorPricingSetup.vendorId.options[0] = new Option("[Choose One]","0");
			var j="0";
			
			for (var i=0;i<actVendLen;i++)
			{
				arr = eval("allVendArr"+i);   
				arrobj = arr.split("^");
				value = arrobj[0];
				name = arrobj[1];
				document.frmVendorPricingSetup.vendorId.options[i+1] = new Option(name,value);
				j++;
			}
			document.frmVendorPricingSetup.vendorId.value=selVendorId;
		}else{
			
			document.frmVendorPricingSetup.vendorId.options.length = 0;
			document.frmVendorPricingSetup.vendorId.options[0] = new Option("[Choose One]","0");
			
			for (var i=0;i<actVendArrLen;i++)
			{
				arr = eval("actVendArr"+i);   
				arrobj = arr.split("^");
				value = arrobj[0];
				name = arrobj[1];
				document.frmVendorPricingSetup.vendorId.options[i+1] = new Option(name,value);		
			}
			document.frmVendorPricingSetup.vendorId.value=selVendorId;
		}
}
//This function used to down load the excel file
function fnDownloadXLS() {
	var actInd = gridObj.getColIndexById("active");
	var hActInd = gridObj.getColIndexById("h_act");
	var partNo = gridObj.getColIndexById("part_no");
	/* below code is added when any thing changed as part of active column, setting respective value to hidden
	column, since we are using hidden column to export excel */
	gridObj.forEachRow(function(rowId){
		var actColVal =  gridObj.cells(rowId, actInd).getValue() == '1'?'Y':'N';		
	    gridObj.cellById(rowId, hActInd).setValue(actColVal); 
	    var partColVal =  gridObj.cells(rowId, partNo).getValue(); 
	    if(partColVal=='0')
	    {
	     gridObj.setRowHidden(rowId,true);
	    }
		});
	gridObj.setColumnHidden(actInd,true);
	gridObj.setColumnHidden(hActInd,false);
	//var defaultRowCount=gridObj.getRowsNum();
	//Excel export comes without blank records
	//for(var i=defaultRowCount;i>=defaultRowCount-50;i--){
	   // gridObj.setRowHidden(gridObj.getRowId(i),true);
	  //  }		
	var vendorNmColInd = gridObj.getColIndexById("vendor");
	var combooptions = gridObj.getCombo(vendorNmColInd);	
	combooptions.save();       // saves the current state of vendor combo
	combooptions.clear();      // removes all records from the collection before exporting into excel
	gridObj.toExcel('/phpapp/excel/generate.php');
	combooptions.restore();    // restores the previously saved state of vendor combo
	gridObj.setColumnHidden(actInd,false);	
	gridObj.setColumnHidden(hActInd,true);
	gridObj.forEachRow(function(rowId){
	    var partColVal =  gridObj.cells(rowId, partNo).getValue(); 
	    if(partColVal=='0')
	    {
	     gridObj.setRowHidden(rowId,false);
	    }
		});
	//Excel export comes without blank records
	//for(var i=defaultRowCount;i>=defaultRowCount-50;i--){
	   // gridObj.setRowHidden(gridObj.getRowId(i),false);
	   // }

}
//this function is used to filter the grid data.
function filterData(){
	var divValue =  document.getElementById("filter_Lbl").innerHTML;
	
	if(!filterMode){
		if(divValue == message_operations[392]){
			existFl = true;
		}else if(divValue == message_operations[393]){
			changeFl = true;
		}
	}
	//to only show existing rows
	if(existFl){
		gridObj.filterBy(gridObj.getColIndexById("h_filter_by"), "1");		
		document.getElementById("filter_Lbl").innerHTML=message_operations[392];
		existFl = false;changeFl = true;filterMode = true;
	}else if(changeFl){//to show only changed rows
		gridObj.filterBy(gridObj.getColIndexById("h_filter_by"), "0");	
		document.getElementById("filter_Lbl").innerHTML=message_operations[393];
		existFl = true;changeFl = false;filterMode = true;
	}
	
}
