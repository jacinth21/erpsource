var TotalValue = 0;
function fnGo()
{

   var month = document.frmMonthlySheetReport.Cbo_Month.value;
   
    //alert(month);
	if( month != '0' && document.frmMonthlySheetReport.Cbo_Year.value == '0' )
	{
	Error_Details("Please select year for the load period when month is chosen.(It is not necessary to select month)");
	Error_Show();
	Error_Clear();
	return false;
	}

	document.frmMonthlySheetReport.hAction.value = "Reload";
	fnStartProgress('Y');
	document.frmMonthlySheetReport.submit();
}

function fnReLoadSheet(sheetId,sheetName,sheetStatus){

/*alert(" Temporarily disabled. Please contact IT ");*/
		if (sheetStatus != '50550')	{
			Error_Details("Sorry. You can only reload OPEN sheets ");
			Error_Show();
			Error_Clear();
		}
		else {
		document.frmMonthlySheetReport.action ="/GmCommonCancelServlet";
		document.frmMonthlySheetReport.hTxnId.value = sheetId;
		document.frmMonthlySheetReport.hTxnName.value = sheetName;
		document.frmMonthlySheetReport.hAction.value = "";		
		document.frmMonthlySheetReport.submit();
		}
		
}

//for changing the status from Approve to Open
function fnChangeStatus(sheetId,sheetName,sheetStatus){
	
		if (sheetStatus != '50551')	{
			Error_Details(message[209]);
			Error_Show();
			Error_Clear();
		}
		else
		{
			document.frmMonthlySheetReport.action ="/GmCommonCancelServlet";
			document.frmMonthlySheetReport.hTxnId.value = sheetId;
			document.frmMonthlySheetReport.hTxnName.value = sheetName;
			document.frmMonthlySheetReport.hCancelType.value = "RDSST";
			document.frmMonthlySheetReport.hAction.value = "";		
			document.frmMonthlySheetReport.submit();
		}
}
