var gridObj ='';
var copyFromExcelFL;
var hdupWoInputStr ='';

function fnOnPageLoad(){
	fnLoadCallBack();
}

//This function is used to get response and set to screen
function fnLoadCallBack(response){
	
	// to clear the grid data
	if(gridObj !=''){
		gridObj.clearAll();
	}
	
	if(response == undefined || response == null){
		strDtlsJson ='';
	}else{
	    strDtlsJson = response;
	}
      	
		// to set auto height
		if($(document).height() !=undefined || $(document).height() !=null){
			gridHeight = $(document).height() - 130;
		}
	
	    gridObj = new dhtmlXGridObject('dataGridDiv');
		gridObj.setHeader("Work Order,ACK Date,ACK Qty");
		gridObj.setInitWidths("200,200,*");
		gridObj.setColAlign("left,left,left");
		gridObj.setColTypes("ed,ed,ed");
		gridObj.setColSorting("str,str,str");
		gridObj.setColumnIds("woid,woackdt,qty");
		gridObj.enableAutoHeight(true, gridHeight, true);
		
		gridObj.init();
		gridObj.enableAlterCss("even","uneven");
		gridObj.parse(strDtlsJson,"js");
		gridObj.attachEvent("onKeyPress", onKeyPressed);
		gridObj.attachEvent("onEditCell",doOnCellEdit);
		gridObj.attachEvent("onTab",doOnCellEdit);
		
	    gridObj.enableMultiselect(true);
		gridObj.enableBlockSelection(true);
		
		wo_rowId = gridObj.getColIndexById("woid");
		woackdt_rowId = gridObj.getColIndexById("woackdt");
		qty_rowId = gridObj.getColIndexById("qty");
		
		if (strDtlsJson == "") {
			fnAddFast500Rows();
		} 
}

function fnAddFast500Rows(){
			gridObj.clearAll();
			gridObj.startFastOperations();
			for (var i = 500; i > 0; i--){
				gridObj.addRow(i,'');
			}
			gridObj.stopFastOperations();
}

//Below function is used to add new rows whwn on page load
function fnAddRows() {
	gridObj.filterBy(0, "");           //unfilters the grid while adding rows
	gridObj._f_rowsBuffer = null;      // clears the cache
	var newRowId = gridObj.getUID();
	gridObj.addRow(newRowId, '');
	document.all.datasuc.style.display = "none";
}

//This function is used to save and fetch work order details when copy paste data
function fnSave(){
    fnRemoveEmptyRow();
	fnValidate();
}

//This function is used to validate input from grid
function fnValidate(){
	var pastdate = '';
	var wovalidation = '';
	var woackdtvalidation = '';
	var dateval = '';
	var dateVal = '';
	var currentDate = new Date();
	var woStr = '';
	var qtyvalidation ='';
	
	gridObj.forEachRow(function(row_id) { 
		workOrderId = TRIM(gridObj.cellById(row_id,wo_rowId).getValue());
		qty_id = TRIM(gridObj.cellById(row_id, qty_rowId).getValue());
		workOrderAckDt = gridObj.cellById(row_id,woackdt_rowId);
		woackdate = new Date(workOrderAckDt.getValue());
	
		currentDate = getDateFormat(currentDate,gCmpDateFmt);
	    woackdate = new Date(workOrderAckDt.getValue());
		woackdate = getDateFormat(woackdate,gCmpDateFmt);
		woackdatevalue = woackdate;
		currentDate = new Date(currentDate);
		woackdate = new Date(woackdate);
		
		if(workOrderAckDt.getValue() != ''){
			dateVal = validateDt(TRIM(workOrderAckDt.getValue()),gCmpDateFmt);
		}
		if(dateVal == 'Y' && workOrderAckDt.getValue() != '' && workOrderAckDt.getValue() != undefined && workOrderId != ''){
				gridObj.setCellTextStyle(row_id,wo_rowId,"color:red;");
				gridObj.setCellTextStyle(row_id,woackdt_rowId,"color:red;");
				gridObj.setRowAttribute(row_id,"row_error","Y");
				dateval = 'Y';
		}else{
				gridObj.setCellTextStyle(row_id,wo_rowId,"color:block;");
				gridObj.setCellTextStyle(row_id,woackdt_rowId,"color:block;");
				gridObj.setRowAttribute(row_id,"row_error","N");
		}
		
		if (currentDate > woackdate && dateVal != 'Y') {
				gridObj.setCellTextStyle(row_id,wo_rowId,"color:red;");
				gridObj.setCellTextStyle(row_id,woackdt_rowId,"color:red;");
				gridObj.setRowAttribute(row_id,"row_error","Y");
				pastdate = 'Y';
		}else{
				if(dateval != 'Y'){
					gridObj.setCellTextStyle(row_id,wo_rowId,"color:block;");
					gridObj.setCellTextStyle(row_id,woackdt_rowId,"color:block;");
					gridObj.setRowAttribute(row_id,"row_error","N");
				}
		}
		
		if(workOrderId == '' && workOrderAckDt.getValue() != ''){
				gridObj.setCellTextStyle(row_id,wo_rowId,"color:red;");
				gridObj.setRowAttribute(row_id,"row_error","Y");
				wovalidation = 'Y';
		}else{
				if(dateval != 'Y' && pastdate != 'Y' && wovalidation != 'Y'){
					gridObj.setCellTextStyle(row_id,wo_rowId,"color:block;");
					gridObj.setRowAttribute(row_id,"row_error","N");
				}
		}
		
		if(workOrderAckDt.getValue() == '' && workOrderId != ''){
				gridObj.setCellTextStyle(row_id,wo_rowId,"color:red;");
				gridObj.setRowAttribute(row_id,"row_error","Y");
				woackdtvalidation = 'Y';
		}else{
				if(dateval != 'Y' && pastdate != 'Y' && wovalidation != 'Y' && woackdtvalidation != 'Y'){
					gridObj.setCellTextStyle(row_id,wo_rowId,"color:block;");
					gridObj.setRowAttribute(row_id,"row_error","N");
				}
		}
		
		if(qty_id <= 0){
				gridObj.setCellTextStyle(row_id,wo_rowId,"color:red;");
				gridObj.setCellTextStyle(row_id,qty_rowId,"color:red;");
				gridObj.setRowAttribute(row_id,"row_error","Y");
				qtyvalidation = 'Y';
		}else{
				if(dateval != 'Y' && pastdate != 'Y' && wovalidation != 'Y' && woackdtvalidation != 'Y'){
					gridObj.setCellTextStyle(row_id,wo_rowId,"color:block;");
					gridObj.setCellTextStyle(row_id,qty_rowId,"color:block;");
					gridObj.setRowAttribute(row_id,"row_error","N");
				}
		}
			
	});
	
	if (pastdate == 'Y'){
		Error_Details(message_operations[889]);
	}
	if (wovalidation == 'Y'){
		Error_Details(message_operations[883]);
	}
	if (dateval == 'Y'){
		Error_Details(message[611]);
	}
	if (woackdtvalidation != '' && wovalidation != 'Y'){
		Error_Details(message_operations[886]);
	}	
	if(qtyvalidation == 'Y'){
		Error_Details(message_operations[888]);
	}
	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}
	fnCreateInputStr();
}

//This function is used to form inputstring
function fnCreateInputStr(){
	var woStr = '';
	var inputStr = '';
	var worowerror = '';

	var gridChrow = gridObj.getChangedRows();
	
	var gridChrows = gridObj.getChangedRows(',');
		var rowsarr = gridChrows.toString().split(",");
		if (rowsarr.length > 0) {
			for (var rowid = 0; rowid < rowsarr.length; rowid++) {
								
				rowId = rowsarr[rowid];
				qty_id = TRIM(gridObj.cellById(rowId,qty_rowId).getValue());
				work_order_id = TRIM(gridObj.cellById(rowId,wo_rowId).getValue());		
				wo_ackdt_val = gridObj.cellById(rowId, woackdt_rowId).getValue();
			    worowerror = gridObj.getRowAttribute(rowId,"row_error");
				worowmofied = gridObj.getRowAttribute(rowId,"row_modified");
				
				if(work_order_id != '' && worowerror != 'Y' && worowmofied == 'Y'){
					woStr = woStr + work_order_id +',';
					inputStr = inputStr + work_order_id + '^' + wo_ackdt_val + '^' + qty_id +'|';
				}
			}		
		}
	var loader = dhtmlxAjax.postSync('/gmWOAckDateBulkUpload.do?method=updateWOAckDateUpload','&woStr='+
	    woStr +'&inputStr='+inputStr+'&companyInfo='+strCompanyInfo+'&ramdomId=' + Math.random());
		  fnSaveResponseCallback(loader);
}

//fuction is used to set response to grid
function fnSaveResponseCallback(loader){
	response = loader.xmlDoc.responseText;
	var responseSpilt = response;
	if (responseSpilt != '') {
		resInvalidWO = responseSpilt.split('~');
		if(resInvalidWO[0] !='' && resInvalidWO[0] != undefined){
			responseInvalidWo = resInvalidWO[0].substr(1);
			var woidArrs = responseInvalidWo.split(',');
			var wonotexiststr = '';
			for(j=0;j<woidArrs.length;j++){
				gridObj.forEachRow(function(row_id){
				wo_id_val = TRIM(gridObj.cellById(row_id,wo_rowId).getValue());
			if(wo_id_val == woidArrs[j]){
				gridObj.setCellTextStyle(row_id, wo_rowId,"color:red;");
				wonotexiststr = 'Y';
			}
			});
	  		}
		if (wonotexiststr == 'Y'){
			Error_Details(message_operations[887]);
		}
		}
		
		if(resInvalidWO[1] !='' && resInvalidWO[1] != undefined){
			var resErrMsg = resInvalidWO[1].substr(1);
			var woidArr = resErrMsg.split(',');
			var woclosestr = '';
			for(i=0;i<woidArr.length;i++){
				gridObj.forEachRow(function(row_id){
				wo_id_val = TRIM(gridObj.cellById(row_id,wo_rowId).getValue());
				if(wo_id_val == woidArr[i]){
					gridObj.setCellTextStyle(row_id, wo_rowId,"color:red;");
					woclosestr = 'Y';
				}
				});
	  		}
		if (woclosestr == 'Y'){
			Error_Details(message_operations[885]);
		}
		}
//		PC-4387 - ACK Qty validation for threshold WO quantity
		if(resInvalidWO[2] !='' && resInvalidWO[2] != undefined){
			var resExcessQty = resInvalidWO[2].substr(1);
			var woidArr = resExcessQty.split(',');
			var woexcessqty = '';
			for(i=0;i<woidArr.length;i++){
			gridObj.forEachRow(function(row_id){
			wo_id_val = TRIM(gridObj.cellById(row_id,wo_rowId).getValue());
			if(wo_id_val == woidArr[i]){
			gridObj.setCellTextStyle(row_id, wo_rowId,"color:red;");
			woexcessqty = 'Y';
			}
			});
			}
			if (woexcessqty == 'Y'){
				Error_Details(message_operations[896]);
				}
		}
	}
	if (ErrorCount > 0 || woclosestr == 'Y' || wonotexiststr == 'Y' || woexcessqty == 'Y' ){
		Error_Show();
		Error_Clear();
		return false;
	}else{
		gridObj.forEachRow(function(row_id) {
			gridObj.setRowAttribute(row_id,"row_modified","N");
		});
		document.all.datasuc.style.display = "inline";
	}
}
//This Function Used to set custom types conditions in Grid
function fnCustomTypes(gObj, setHeader, setColIds, setInitWidths, setColAlign,
		setColTypes, setColSorting, enableTooltips, gridHeight,
		pagination) {
	
	if (setHeader != undefined && setHeader.length > 0) {
		gObj.setHeader(setHeader);
		var colCount = setHeader.length;
	}

	if (setInitWidths != "")
		gObj.setInitWidths(setInitWidths);

	if (setColAlign != "")
		gObj.setColAlign(setColAlign);

	if (setColTypes != "")
		gObj.setColTypes(setColTypes);

	if (setColSorting != "")
		gObj.setColSorting(setColSorting);

	if (enableTooltips != "")
		gObj.enableTooltips(enableTooltips);

	if (setColIds != "") {
		gObj.setColumnIds(setColIds);
	}

	if (gridHeight != "")
		gObj.enableAutoHeight(true, gridHeight, true);

	if (pagination == "Y") {
		gObj.enablePaging(true, 100, 10, "pagingArea", true);
		gObj.setPagingSkin("bricks");
	}

	return gObj;
}

//Funciton will be called while editing the columns in the grid
function doOnCellEdit(stage, rowId, cellInd, nValue, oValue){
	
	if(cellInd == wo_rowId && (oValue != '' || nValue != '') && stage == 2){ //once the value in part number column
		gridObj.setRowAttribute(rowId,"row_modified","Y");
 	}else if(cellInd == woackdt_rowId && (oValue != '' || nValue != '') && stage == 2){//once the value in Adjustment code column
 		gridObj.setRowAttribute(rowId,"row_modified","Y");
 	}else if(cellInd == qty_rowId && (oValue != '' || nValue != '') && stage == 2){//once the value in Adjustment code column
 		gridObj.setRowAttribute(rowId,"row_modified","Y");
 	}
 	
	return true;
}

//Use the below function to copy data inside the screen.
function docopy(){
      gridObj.setCSVDelimiter("\t");
      gridObj.copyBlockToClipboard();
      gridObj._HideSelection();
}

//this function used to copy the grid data
function onKeyPressed(code, ctrl) {
	if (code == 67 && ctrl) {
		if (!gridObj._selectionArea)
			return false;
		gridObj.setCSVDelimiter("\t");
		gridObj.copyBlockToClipboard();
		//gridObj._HideSelection();
	}
	if (code == 86 && ctrl) {
		//fnStartProgress();
		gridObj.setCSVDelimiter("\t");		
		//fnRemoveEmptyRow();
		//addRowFromPaste();
	    //gridObj._HideSelection();
		gridObj.pasteBlockFromClipboard();
		 setTimeout(function() {	
					fnAddModified();
					fnRemoveEmptyRow();
					},1000);
	}
	return true;
}
function fnAddModified() {
	gridObj.forEachRow(function(rowId) {
		wo_id = TRIM(gridObj.cellById(rowId,wo_rowId).getValue());
		woackdt_id = TRIM(gridObj.cellById(rowId,woackdt_rowId).getValue());		
		qty_id = TRIM(gridObj.cellById(rowId,qty_rowId).getValue());
	   // worowerror = gridObj.getRowAttribute(rowId,"row_error");
		if(wo_id!='' || woackdt_id !='' || qty_id !='') {
			gridObj.setRowAttribute(rowId,"row_modified","Y");
		}
	});
	//fnSave();
}
//paste when ctrl + V
function addRowFromPaste(){
	var cbData = gridObj.fromClipBoard();
	var subPartNum, subPartDesc, lpprCode , partPrice , partTVA;
    
    var rowData = cbData.split("\n");
    var arrLen = (rowData.length) - 1;   

    var newRowData = '';
    var str='';
    setTimeout(function() {	
    gridObj.startFastOperations();
    gridObj.filterBy(0,"");//unfilters the grid
    gridObj._f_rowsBuffer = null; //clears the cache
    for ( var i = 0; i < arrLen; i++) {    	
          newRowData = rowData[i];          
          rowcolumnData = rowData[i].split("\t");
          var row_id = gridObj.getUID();
          gridObj.setCSVDelimiter("\t");
          gridObj.setDelimiter("\t");
          gridObj.addRow(row_id, '');
          gridObj.cellById(row_id, wo_rowId).setValue(rowcolumnData[0]);
		  gridObj.cellById(row_id, woackdt_rowId).setValue(rowcolumnData[1]);
		  gridObj.cellById(row_id, qty_rowId).setValue(rowcolumnData[2]);
		  str = str  +row_id+'^' +rowcolumnData[0] + '^' + rowcolumnData[1] + '^' + rowcolumnData[2] + '|';
          gridObj.setRowAttribute(row_id, "row_added", "Y");
          gridObj.setRowAttribute(row_id,"row_modified","Y");
          gridObj.setDelimiter(",");
    }

    gridObj.stopFastOperations();
	fnStopProgress();	
	},1000);
	document.all.datasuc.style.display = "none";
}
//Use the below function to remove the rows from the grid.
function removeSelectedRow(){
      var gridrows=gridObj.getSelectedRowId();   
      if(gridrows!=undefined)
      {
            var NameCellValue ;
            var gridrowsarr = gridrows.toString().split(",");
            var added_row;
            var dtlId;
            for (var rowid = 0; rowid < gridrowsarr.length; rowid++)
            {
                  gridrowid = gridrowsarr[rowid];                  
                  gridObj.setRowAttribute(gridrowid,"row_modified","N");
                  gridObj.deleteRow(gridrowid);                                
            }
      }else{
            Error_Clear();
            Error_Details(message_prodmgmnt[120]);                       
            Error_Show();
            Error_Clear();
      }
}

var removeEmptyFl = false;
var confirmMsg = true;

//this function used to remove the empty rows
function fnRemoveEmptyRow() {
	gridObj.forEachRow(function(row_id) {
		wo_val = gridObj.cellById(row_id, wo_rowId).getValue();
		wo_ackdtval = gridObj.cellById(row_id, woackdt_rowId).getValue();
        wo_qtyval = gridObj.cellById(row_id, qty_rowId).getValue();
		 if (wo_val == '' && wo_ackdtval == '' && wo_qtyval == '') {
            gridObj.deleteRow(row_id);
        }
	});
}

//To add datas from clipboard.
function addRowFromClipboard() {
	copyFromExcelFL = 'Y';
	var cbData = gridObj.fromClipBoard();
	var subPartNum, subPartDesc, lpprCode , partPrice , partTVA;
    if (cbData == null) {
          Error_Details(message_prodmgmnt[121]);
    }  
    var rowData = cbData.split("\n");
    var arrLen = (rowData.length) - 1;    
    if(rowData.length - 1 == 0){
		Error_Details(message_prodmgmnt[122]);
		Error_Show();
        Error_Clear();
        return false;	
    }
    
    if(rowData.length-1 > 1000){
		fnStopProgress();
		Error_Details(message_prodmgmnt[123]);		
		Error_Show();
        Error_Clear();
        return false;	
	}

    fnRemoveEmptyRow();
	fnStartProgress('Y');
  
    var newRowData = '';
    var str='';
    setTimeout(function() {	
    gridObj.startFastOperations();
    gridObj.filterBy(0,"");//unfilters the grid
    gridObj._f_rowsBuffer = null; //clears the cache
    for ( var i = 0; i < arrLen; i++) {    	
          newRowData = rowData[i];          
          rowcolumnData = rowData[i].split("\t");
          var row_id = gridObj.getUID();
          gridObj.setCSVDelimiter("\t");
          gridObj.setDelimiter("\t");
          gridObj.addRow(row_id, '');
          gridObj.cellById(row_id, wo_rowId).setValue(rowcolumnData[0]);
		  gridObj.cellById(row_id, woackdt_rowId).setValue(rowcolumnData[1]);
		  gridObj.cellById(row_id, qty_rowId).setValue(rowcolumnData[2]);
		  str = str  +row_id+'^' +rowcolumnData[0] + '^' + rowcolumnData[1] + '^' + rowcolumnData[2] + '|';
          gridObj.setRowAttribute(row_id, "row_added", "Y");
          gridObj.setRowAttribute(row_id,"row_modified","Y");
          gridObj.setDelimiter(",");
    }

    gridObj.stopFastOperations();
	fnStopProgress();	
	},1000);
	document.all.datasuc.style.display = "none";
}

//To paste the data to the grid
function pasteToGrid(){
	copyFromExcelFL = 'Y';
	if(gridObj._selectionArea!=null){
		var enterCnt = 0;
		var woid, woackdt;
		var topRowCnt, rowID;
		var cbData = gridObj.fromClipBoard();
		if (cbData == null) {
			fnStopProgress();
			Error_Details(message_prodmgmnt[89]);
			Error_Show();
        	Error_Clear();
        	return;
		}
		var emptyRowCnt = 0;
		var partExist,added_row;
		var rowData = cbData.split("\n");
		rowcolumnData = rowData[0].split("\t");
		
		if(rowcolumnData.length > 1){
			for (var i=0; i< rowData.length-1; i++){
				rowcolumnData = rowData[i].split("\t");
				if(i == 0){
					topRowCnt = gridObj.getSelectedBlock().LeftTopRow;// Get the top index of selected row
				}else{
					topRowCnt++;
				}
				rowID=gridObj.getRowId(topRowCnt); // Get the row id to paste the data
				
				if(enterCnt == 0){// check whether there are enough rows to paste the record or not
					var gridrowsarr = '';
					var gridrows=gridObj.getAllRowIds();
					if(gridrows!=undefined) gridrowsarr = gridrows.toString().split(",");
					// Get the empty rows count to paste the record
					for(var j=topRowCnt; j< gridrowsarr.length; j++){
						emptyRowCnt++;
					}

					if(emptyRowCnt < rowData.length-1){
						fnStopProgress();
						Error_Details(message_prodmgmnt[124]);
						Error_Show();
			        	Error_Clear();
			        	return;
					}
					enterCnt++;
				}
							
				woid = rowcolumnData[0];
				woackdt = rowcolumnData[1];
				qty = rowcolumnData[2];		
							
				gridObj.cellById(rowID, wo_rowId).setValue(woid);
				gridObj.cellById(rowID, woackdt_rowId).setValue(woackdt);
				gridObj.cellById(rowID, qty_rowId).setValue(qty);
				
				gridObj.setRowAttribute(rowID,"row_modified","Y");
			
			    gridObj._HideSelection();
			}
		}else{
			gridObj.pasteBlockFromClipboard();
		    gridObj._HideSelection();
		}
		 document.all.datasuc.style.display = "none";	
	}else{
		Error_Details(message_prodmgmnt[125]);
		Error_Show();
        Error_Clear();
	}	
}
//This function is used to format the date 
function getDateFormat(date,format){
	var dd = date.getDate();
    var mm = date.getMonth() + 1; //January is 0!

    var yyyy = date.getFullYear();
    if (dd < 10) {
      dd = '0' + dd;
    } 
    if (mm < 10) {
      mm = '0' + mm;
    } 
    
	if(format == 'MM/dd/yyyy')
		return   date = mm + '/' + dd + '/' + yyyy;
	else if (format == 'dd/MM/yyyy')
		return   date = dd + '/' + mm + '/' + yyyy;
	else if (format == 'dd.MM.yyyy')
		return   date = dd + '.' + mm + '.' + yyyy;
	else if (format == 'MM.dd.yyyy')
		return   date = mm + '.' + dd + '.' + yyyy;
	else (format == 'yyyy/MM/dd')
		return   date = yyyy + '/' + mm + '/' + dd;
}
//This function is used to validate the date format
function validateDt(dateVal,format){
		switch(format){
	case 'MM/dd/yyyy': 
		validformat=/^\d{2}\/\d{2}\/\d{4}$/; 
		splitby="/";
		mon=0;
		day=1;
		year=2;
		break;
	case 'dd.MM.yyyy': 
		validformat=/^\d{2}\.\d{2}\.\d{4}$/; 
		splitby=".";
		mon=1;
		day=0;
		year=2;
		break;
	case 'dd/MM/yyyy': 
		validformat=/^\d{2}\/\d{2}\/\d{4}$/; 
		splitby="/";
		mon=1;
		day=0;
		year=2;
		break;
	case 'MM.dd.yyyy': 
		validformat=/^\d{2}\.\d{2}\.\d{4}$/; 
		splitby=".";
		mon=0;
		day=1;
		year=2;
		break;	
	case 'yyyy/MM/dd': 
		validformat=/^\d{4}\/\d{2}\/\d{2}$/;
		splitby="/";
		mon=1;
		day=2;
		year=0;
		break;
	}
	
	var mm=dateVal.split(splitby)[mon];
	var dd=dateVal.split(splitby)[day];
	var yyyy=dateVal.split(splitby)[year];
		
	if (dd.length < 2) {
      dd = '0' + dd;
    } 

    if (mm.length < 2) {
      mm = '0' + mm;
    }

    if(format == 'MM/dd/yyyy')
		dateVal = mm + '/' + dd + '/' + yyyy;
	else if (format == 'dd/MM/yyyy')
		dateVal = dd + '/' + mm + '/' + yyyy;
	else if (format == 'dd.MM.yyyy')
		dateVal = dd + '.' + mm + '.' + yyyy;
	else if (format == 'MM.dd.yyyy')
		dateVal = mm + '.' + dd + '.' + yyyy;
	else if(format == 'yyyy/MM/dd')
		dateVal = yyyy + '/' + mm + '/' + dd;
		
	if (!validformat.test(dateVal))
	{
		return dateVal = 'Y';
	}else{
		
	var monthfield=dateVal.split(splitby)[mon];
		var dayfield=dateVal.split(splitby)[day];
		var yearfield=dateVal.split(splitby)[year];
		var intyearfield = parseInt(yearfield);
		if(intyearfield < 1900)
			Error_Details(msg);
		var dayobj = new Date(yearfield, monthfield-1, dayfield);
		if ((dayobj.getMonth()+1!=monthfield)||(dayobj.getDate()!=dayfield)||(dayobj.getFullYear()!=yearfield))
			return dateVal = 'Y';
		else
			return dateVal = 'N';
	}
}