var gridObj ='';
var copyFromExcelFL;
var hdupWoInputStr ='';

//This function is used to get reponse and set to screen
function fnLoadCallBack(response){
	
	// to clear the grid data
	if(gridObj !=''){
		gridObj.clearAll();
	}
	
	if(response == undefined || response == null){
		strDtlsJson ='';
	}else{
	    strDtlsJson = response;
	}
      	
		// to set auto height

		if($(document).height() !=undefined || $(document).height() !=null){
			gridHeight = $(document).height() - 130;
		}
		
	    gridObj = new dhtmlXGridObject('dataGridDiv');
		gridObj.setHeader("Work Order Id,WO Due Date,Purchase Order Id, Vendor Name");
		gridObj.attachHeader("#rspan,#rspan,#select_filter,#select_filter");
		gridObj.setInitWidths("200,200,200,*");
		gridObj.setColAlign("left,left,right");
		gridObj.setColTypes("ed,ed,ro,ro");
		gridObj.setColSorting("str,str,str,str");
		gridObj.setColumnIds("woid,woduedt,purchaseid,vendornm");
		gridObj.enableAutoHeight(true, gridHeight, true);
		
		gridObj.init();
		gridObj.enableAlterCss("even","uneven");
		gridObj.parse(strDtlsJson,"js");
		gridObj.attachEvent("onKeyPress", onKeyPressed);
		gridObj.attachEvent("onEditCell",doOnCellEdit);
		gridObj.attachEvent("onTab",doOnCellEdit);
		gridObj.attachEvent("onFilterEnd",doOnFilter);
		
	    gridObj.enableMultiselect(true);
		gridObj.enableBlockSelection(true);
		
		wo_rowId = gridObj.getColIndexById("woid");
		woduedt_rowId = gridObj.getColIndexById("woduedt");
		po_rowId = gridObj.getColIndexById("purchaseid");
		vendor_rowId = gridObj.getColIndexById("vendornm");
		
		if (strDtlsJson == "") {
			fnAddFast500Rows();
		} 
}

function fnAddFast500Rows(){
			gridObj.clearAll();
			gridObj.startFastOperations();
			for (var i = 500; i > 0; i--){
				gridObj.addRow(i,'');
			}
			gridObj.stopFastOperations();
}

function fnSave(){
	fnRemoveEmptyRow();
	fnCreateInputStr();
}

function fnCreateInputStr(){
	var dupWoids = '';
	var hdupWoInputStr = '';
	var pastdate = '';
	var wovalidation = '';
	var woduedtvalidation = '';
	var dateval = '';
	var dateVal = '';
	var currentDate = new Date();
	var woStr = '';
	var inputStr = '';
	var hdupWoInputString = '';
	var worowerror = '';
	Error_Clear();
	gridObj.forEachRow(function(row_id) { 
		workOrderId = TRIM(gridObj.cellById(row_id,wo_rowId).getValue());
		po_id = TRIM(gridObj.cellById(row_id, po_rowId).getValue());
		workOrderDueDt = gridObj.cellById(row_id,woduedt_rowId);
		woduedate = new Date(workOrderDueDt.getValue());
	
		currentDate = getDateFormat(currentDate,gCmpDateFmt);
	    woduedate = new Date(workOrderDueDt.getValue());
		woduedate = getDateFormat(woduedate,gCmpDateFmt);
		currentDate = new Date(currentDate);
		woduedate = new Date(woduedate);
		woduedatevalue = gridObj.cellById(row_id, woduedt_rowId).getValue();
		if(workOrderId != '' || woduedatevalue != ''){
		if(workOrderDueDt.getValue() != ''){
			dateVal = validateDt(TRIM(workOrderDueDt.getValue()),gCmpDateFmt);
		}
			if(dateVal == 'Y' && workOrderDueDt.getValue() != '' && workOrderDueDt.getValue() != undefined && workOrderId != ''){
				gridObj.setCellTextStyle(row_id,wo_rowId,"color:red;");
				gridObj.setCellTextStyle(row_id,woduedt_rowId,"color:red;");
				gridObj.setRowAttribute(row_id,"row_error","Y");
				dateval = 'Y';
			}else{
				gridObj.setCellTextStyle(row_id,wo_rowId,"color:block;");
				gridObj.setCellTextStyle(row_id,woduedt_rowId,"color:block;");
				gridObj.setRowAttribute(row_id,"row_error","N");
			}
			if (currentDate > woduedate && dateVal != 'Y') {
				gridObj.setCellTextStyle(row_id,wo_rowId,"color:red;");
				gridObj.setCellTextStyle(row_id,woduedt_rowId,"color:red;");
				gridObj.setRowAttribute(row_id,"row_error","Y");
				pastdate = 'Y';
			}else{
				if(dateval != 'Y'){
					gridObj.setCellTextStyle(row_id,wo_rowId,"color:block;");
					gridObj.setCellTextStyle(row_id,woduedt_rowId,"color:block;");
					gridObj.setRowAttribute(row_id,"row_error","N");
				}
			}
			if(workOrderId == '' && workOrderDueDt.getValue() != ''){
				gridObj.setCellTextStyle(row_id,wo_rowId,"color:red;");
				gridObj.setRowAttribute(row_id,"row_error","Y");
				wovalidation = 'Y';
			}else{
				if(dateval != 'Y' && pastdate != 'Y' && wovalidation != 'Y'){
				gridObj.setCellTextStyle(row_id,wo_rowId,"color:block;");
					gridObj.setRowAttribute(row_id,"row_error","N");
				}
			}
			if(workOrderDueDt.getValue() == '' && workOrderId != ''){
				gridObj.setCellTextStyle(row_id,wo_rowId,"color:red;");
				gridObj.setRowAttribute(row_id,"row_error","Y");
				woduedtvalidation = 'Y';
			}else{
				if(dateval != 'Y' && pastdate != 'Y' && wovalidation != 'Y' && woduedtvalidation != 'Y'){
				gridObj.setCellTextStyle(row_id,wo_rowId,"color:block;");
					gridObj.setRowAttribute(row_id,"row_error","N");
				}
			}
			if (workOrderId != '' && workOrderDueDt.getValue() != '') {
				if (hdupWoInputStr.indexOf(workOrderId + ",") == -1) {
					hdupWoInputStr += (workOrderId + ",");
					hdupWoInputString += (workOrderId + '^' + woduedatevalue + '|');
				} else {
					if (dupWoids.indexOf(workOrderId + ",") == -1) {
						dupWoids += (workOrderId + ", ");
					}
					/*if(dateval != 'Y' && pastdate != 'Y' && wovalidation != 'Y' && woduedtvalidation != 'Y'){
						gridObj.setRowAttribute(row_id,"row_error","N");
					}else{
						gridObj.setRowAttribute(row_id,"row_error","Y");
					}*/
					gridObj.setRowAttribute(row_id,"row_error","Y");
					gridObj.setCellTextStyle(row_id, wo_rowId,"color:red;");
				}
			} 
		}
		
	});
	if (pastdate == 'Y'){
		Error_Details(message_operations[884]);
	}
	if (wovalidation == 'Y'){
		Error_Details(message_operations[883]);
	}
	if (dateval == 'Y'){
		Error_Details(message[611]);
	}
	if (woduedtvalidation != '' && wovalidation != 'Y'){
		Error_Details(message_operations[886]);
	}
	
	// Error message for Duplicate WO id Entries..
		if (dupWoids != '' && TRIM(dupWoids).length > 1) {
			dupWoids = dupWoids.substr(0, (TRIM(dupWoids).length - 1));
			error_msg = Error_Details_Trans("Duplicate Work Order(s) Found - [0] ", dupWoids);
			Error_Details(error_msg);
		}
	
	var gridChrow = gridObj.getChangedRows();
	//if(gridChrow.length==0){
	var gridChrows = gridObj.getChangedRows(',');
		var rowsarr = gridChrows.toString().split(",");
		if (rowsarr.length > 0) {
			for (var rowid = 0; rowid < rowsarr.length; rowid++) {
								
				rowId = rowsarr[rowid];
				purchase_order_id = TRIM(gridObj.cellById(rowId,po_rowId).getValue());
				work_order_id = TRIM(gridObj.cellById(rowId,wo_rowId).getValue());
				//work_order_id = work_order_id.toUpperCase();			
				wo_duedt_val = gridObj.cellById(rowId, woduedt_rowId).getValue();
			    worowerror = gridObj.getRowAttribute(rowId,"row_error");
				worowmofied = gridObj.getRowAttribute(rowId,"row_modified");
				if(work_order_id != '' && worowerror != 'Y' && worowmofied == 'Y'){
					woStr = woStr + work_order_id +',';
					inputStr = inputStr + work_order_id + '^' + wo_duedt_val + '|';
				}
			}		
		}
	/*}else{
		var rowsarr = gridChrow.toString().split(",");
		if (rowsarr.length > 0) {
			for (var rowid = 0; rowid < rowsarr.length; rowid++) {
								
				rowId = rowsarr[rowid];
				purchase_order_id = TRIM(gridObj.cellById(rowId,po_rowId).getValue());
				work_order_id = TRIM(gridObj.cellById(rowId,wo_rowId).getValue());
				//work_order_id = work_order_id.toUpperCase();			
				wo_duedt_val = gridObj.cellById(rowId, woduedt_rowId).getValue();
			    worowerror = gridObj.getRowAttribute(rowId,"row_error");
				worowmofied = gridObj.getRowAttribute(rowId,"row_modified");
				if(work_order_id != '' && worowerror != 'Y' && worowmofied == 'Y'){
					woStr = woStr + work_order_id +',';
					inputStr = inputStr + work_order_id + '^' + wo_duedt_val + '|';
				}
			}		
		}
	}*/
	var loader = dhtmlxAjax.postSync('/gmWODueDateBulkUpload.do?method=updateWODueDateUpload','&woStr='+
	    woStr +'&inputStr='+inputStr+'&companyInfo='+strCompanyInfo+'&ramdomId=' + Math.random());
		  fnSaveResponseCallback(loader);
}
//fuction is used to set response to grid
function fnSaveResponseCallback(loader){
	response = loader.xmlDoc.responseText;
	var responseSpilt = response.split('^');
	if (responseSpilt[0] != '') {
		resInvalidWO = responseSpilt[0].split('~');
		if(resInvalidWO[0] !='' && resInvalidWO[0] != undefined){
			responseInvalidWo = resInvalidWO[0].substr(1);
			var woidArrs = responseInvalidWo.split(',');
			var wonotexiststr = '';
			for(j=0;j<woidArrs.length;j++){
				gridObj.forEachRow(function(row_id){
				wo_id_val = TRIM(gridObj.cellById(row_id,wo_rowId).getValue());
			if(wo_id_val == woidArrs[j]){
				gridObj.setCellTextStyle(row_id, wo_rowId,"color:red;");
				wonotexiststr = 'Y';
				gridObj.cellById(row_id, po_rowId).setValue("");
				gridObj.cellById(row_id, vendor_rowId).setValue("");
			}
			});
	  		}
		if (wonotexiststr == 'Y'){
			Error_Details(message_operations[887]);
			//Error_Show();	
		}
		}
		
		if(resInvalidWO[1] !='' && resInvalidWO[1] != undefined){
			var resErrMsg = resInvalidWO[1].substr(1);
			var woidArr = resErrMsg.split(',');
			var woclosestr = '';
			for(i=0;i<woidArr.length;i++){
				gridObj.forEachRow(function(row_id){
				wo_id_val = TRIM(gridObj.cellById(row_id,wo_rowId).getValue());
				if(wo_id_val == woidArr[i]){
					gridObj.setCellTextStyle(row_id, wo_rowId,"color:red;");
					woclosestr = 'Y';
					gridObj.cellById(row_id, po_rowId).setValue("");
					gridObj.cellById(row_id, vendor_rowId).setValue("");
				}
				});
	  		}
		if (woclosestr == 'Y'){
			Error_Details(message_operations[885]);
			//Error_Show();	
		}
		}
	}
	if (ErrorCount > 0 || woclosestr == 'Y' || wonotexiststr == 'Y' ){
		Error_Show();
		/*Error_Clear();
		return false;*/
	}
fnStopProgress();
	var rows = [], i = 0;
	var JSON_Array_obj = "";
	if(responseSpilt[1] != '' && responseSpilt[1] != undefined){
	JSON_Array_obj = JSON.parse(responseSpilt[1]);
	$.each(JSON_Array_obj, function(index, jsonObject) {
		poID = jsonObject.purchaseid;
		vendorNm = jsonObject.vendornm;
		woID = jsonObject.woid;	
		// default array
		rows[i] = {};
		rows[i].id = woID;
		rows[i].data = [];
		gridObj.forEachRow(function(row_id) {
			wo_id_val = TRIM(gridObj.cellById(row_id,wo_rowId).getValue());
			worowerror = gridObj.getRowAttribute(row_id,"row_error");
			if(wo_id_val == woID && worowerror != 'Y'){
				gridObj.cellById(row_id, po_rowId).setValue(poID);
				gridObj.cellById(row_id, vendor_rowId).setValue(vendorNm);				
				gridObj.setRowAttribute(row_id,"row_modified","N");
				gridObj.setCellTextStyle(row_id,wo_rowId,"color:block;");
				gridObj.setCellTextStyle(row_id,woduedt_rowId,"color:block;");
			}
		});	
		i++;
	
	});
	fnStopProgress();
	gridObj.refreshFilters();
	}
}

function fnOnPageLoad(){
	fnLoadCallBack();
}

function fnAddRows() {
	gridObj.filterBy(0, "");// unfilters the grid while adding rows
	gridObj._f_rowsBuffer = null; // clears the cache
	var newRowId = gridObj.getUID();
	gridObj.addRow(newRowId, '');
}

//Funciton will be called while editing the columns in the grid
function doOnCellEdit(stage, rowId, cellInd, nValue, oValue){
	
	if(cellInd == wo_rowId && (oValue != '' || nValue != '') && stage == 2){//once the value in part number column
		gridObj.setRowAttribute(rowId,"row_modified","Y");
 	}else if(cellInd == woduedt_rowId && (oValue != '' || nValue != '') && stage == 2){//once the value in Adjustment code column
 		gridObj.setRowAttribute(rowId,"row_modified","Y");
 	}
	
	if(cellInd == woduedt_rowId && stage == "2"){
		fnSave();
	}
 	
	return true;
}
//to show print button based on onFilterEnd events
function doOnFilter(elements){
	if(elements[0][0].value != '' || elements[1][0].value !=''){
		document.getElementById("BTN_PRINT").disabled = false;
	}else{
		document.getElementById("BTN_PRINT").disabled = true;
	}
}

//function is used to show the print details for WO
function fnPrint(){
	var woArr = '';
	gridObj.forEachRowA(function(row_id){
		woid = gridObj.cellById(row_id,wo_rowId).getValue();
		woArr = woArr + woid + ',';
	});
	windowOpener("/gmWODueDateBulkUpload.do?method=fetchWOPrintDetails&woStr="+woArr,"PO2","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}
//Use the below function to copy data inside the screen.
function docopy(){
      gridObj.setCSVDelimiter("\t");
      gridObj.copyBlockToClipboard();
      gridObj._HideSelection();
}

//this function used to copy the grid data
function onKeyPressed(code,ctrl,shift){
			if(code==67&&ctrl){
				if (!gridObj._selectionArea) return alert("You need to select a block area in grid first");
					gridObj.setCSVDelimiter("\t");
					gridObj.copyBlockToClipboard();
				}
				if(code==86&&ctrl){
					fnStartProgress();
					gridObj.setCSVDelimiter("\t");
					gridObj.pasteBlockFromClipboard();
					setTimeout(function() {	
					fnAddModified();
					fnRemoveEmptyRow();
					},1000);
				}
				//fnStopProgress();
				gridObj.refreshFilters();
			return true;
		}
function fnAddModified() {
	gridObj.forEachRow(function(rowId) {
		wo_id = TRIM(gridObj.cellById(rowId,wo_rowId).getValue());
		woduedt_id = TRIM(gridObj.cellById(rowId,woduedt_rowId).getValue());
		if(wo_id!='' || woduedt_id !='') {
			gridObj.setRowAttribute(rowId,"row_modified","Y");
		}
	});
	fnSave();
}
//paste when ctrl + V
function addRowFromPaste(){
	var cbData = gridObj.fromClipBoard();
	var subPartNum, subPartDesc, lpprCode , partPrice , partTVA;
    
    var rowData = cbData.split("\n");
    var arrLen = (rowData.length) - 1;   

    var newRowData = '';
    var str='';
    setTimeout(function() {	
    gridObj.startFastOperations();
    gridObj.filterBy(0,"");//unfilters the grid
    gridObj._f_rowsBuffer = null; //clears the cache
    for ( var i = 0; i < arrLen; i++) {    	
          newRowData = rowData[i];          
          rowcolumnData = rowData[i].split("\t");
          var row_id = gridObj.getUID();
          gridObj.setCSVDelimiter("\t");
          gridObj.setDelimiter("\t");
          gridObj.addRow(row_id, '');
          gridObj.cellById(row_id, wo_rowId).setValue(rowcolumnData[0]);
		  gridObj.cellById(row_id, woduedt_rowId).setValue(rowcolumnData[1]);
		  str = str  +row_id+'^' +rowcolumnData[0] + '^' + rowcolumnData[1] + '|';
          gridObj.setRowAttribute(row_id, "row_added", "Y");
          gridObj.setRowAttribute(row_id,"row_modified","Y");
          gridObj.setDelimiter(",");
    }

    gridObj.stopFastOperations();
	fnStopProgress();	 
	fnSave();
	},1000);
}
//Use the below function to remove the rows from the grid.
function removeSelectedRow(){
      var gridrows=gridObj.getSelectedRowId();   
      if(gridrows!=undefined)
      {
            var NameCellValue ;
            var gridrowsarr = gridrows.toString().split(",");
            var added_row;
            var dtlId;
            for (var rowid = 0; rowid < gridrowsarr.length; rowid++)
            {
                  gridrowid = gridrowsarr[rowid];                  
                  gridObj.setRowAttribute(gridrowid,"row_modified","N");
                  gridObj.deleteRow(gridrowid);                                
            }
      }else{
            Error_Clear();
            Error_Details(message_prodmgmnt[120]);                       
            Error_Show();
            Error_Clear();
      }
	fnSave();
	gridObj.refreshFilters();
}

var removeEmptyFl = false;
var confirmMsg = true;

//this function used to remove the empty rows
function fnRemoveEmptyRow() {
	gridObj.forEachRow(function(row_id) {
		wo_val = gridObj.cellById(row_id, wo_rowId).getValue();
		if (wo_val == '') {
			gridObj.deleteRow(row_id);
		}
	});
}

//To add datas from clipboard.
function addRowFromClipboard() {
	copyFromExcelFL = 'Y';
	var cbData = gridObj.fromClipBoard();
	var subPartNum, subPartDesc, lpprCode , partPrice , partTVA;
    if (cbData == null) {
          Error_Details(message_prodmgmnt[121]);
    }  
    var rowData = cbData.split("\n");
    var arrLen = (rowData.length) - 1;    
    if(rowData.length - 1 == 0){
		Error_Details(message_prodmgmnt[122]);
		Error_Show();
        Error_Clear();
        return false;	
    }
    
    if(rowData.length-1 > 1000){
		fnStopProgress();
		Error_Details(message_prodmgmnt[123]);		
		Error_Show();
        Error_Clear();
        return false;	
	}

    fnRemoveEmptyRow();
	fnStartProgress('Y');
  
    var newRowData = '';
    var str='';
    setTimeout(function() {	
    gridObj.startFastOperations();
    gridObj.filterBy(0,"");//unfilters the grid
    gridObj._f_rowsBuffer = null; //clears the cache
    for ( var i = 0; i < arrLen; i++) {    	
          newRowData = rowData[i];          
          rowcolumnData = rowData[i].split("\t");
          var row_id = gridObj.getUID();
          gridObj.setCSVDelimiter("\t");
          gridObj.setDelimiter("\t");
          gridObj.addRow(row_id, '');
          gridObj.cellById(row_id, wo_rowId).setValue(rowcolumnData[0]);
		  gridObj.cellById(row_id, woduedt_rowId).setValue(rowcolumnData[1]);
		  str = str  +row_id+'^' +rowcolumnData[0] + '^' + rowcolumnData[1] + '|';
          gridObj.setRowAttribute(row_id, "row_added", "Y");
          gridObj.setRowAttribute(row_id,"row_modified","Y");
          gridObj.setDelimiter(",");
    }

    gridObj.stopFastOperations();
	fnStopProgress();	 
	fnSave();
	},1000);
	
}

//To paste the data to the grid
function pasteToGrid(){
	copyFromExcelFL = 'Y';
	if(gridObj._selectionArea!=null){
		var enterCnt = 0;
		var woid, woduedt;
		var topRowCnt, rowID;
		var cbData = gridObj.fromClipBoard();
		if (cbData == null) {
			fnStopProgress();
			Error_Details(message_prodmgmnt[89]);
			Error_Show();
        	Error_Clear();
        	return;
		}
		var emptyRowCnt = 0;
		var partExist,added_row;
		var rowData = cbData.split("\n");
		rowcolumnData = rowData[0].split("\t");
		
		if(rowcolumnData.length > 1){
			for (var i=0; i< rowData.length-1; i++){
				rowcolumnData = rowData[i].split("\t");
				if(i == 0){
					topRowCnt = gridObj.getSelectedBlock().LeftTopRow;// Get the top index of selected row
				}else{
					topRowCnt++;
				}
				rowID=gridObj.getRowId(topRowCnt); // Get the row id to paste the data
				
				if(enterCnt == 0){// check whether there are enough rows to paste the record or not
					var gridrowsarr = '';
					var gridrows=gridObj.getAllRowIds();
					if(gridrows!=undefined) gridrowsarr = gridrows.toString().split(",");
					// Get the empty rows count to paste the record
					for(var j=topRowCnt; j< gridrowsarr.length; j++){
						emptyRowCnt++;
					}

					if(emptyRowCnt < rowData.length-1){
						fnStopProgress();
						Error_Details(message_prodmgmnt[124]);
						Error_Show();
			        	Error_Clear();
			        	return;
					}
					enterCnt++;
				}
							
				woid = rowcolumnData[0];
				woduedt = rowcolumnData[1];
									
				gridObj.cellById(rowID, wo_rowId).setValue(woid);
				gridObj.cellById(rowID, woduedt_rowId).setValue(woduedt);
				
				gridObj.setRowAttribute(rowID,"row_modified","Y");
			
			    gridObj._HideSelection();
			    //document.all.Btn_Save.disabled = false;
			}
			fnSave();
		}else{
			gridObj.pasteBlockFromClipboard();
		    gridObj._HideSelection();
		}
			
	}else{
		Error_Details(message_prodmgmnt[125]);
		Error_Show();
        Error_Clear();
	}	
}
//This function is used to format the date 
function getDateFormat(date,format){
	var dd = date.getDate();
    var mm = date.getMonth() + 1; //January is 0!

    var yyyy = date.getFullYear();
    if (dd < 10) {
      dd = '0' + dd;
    } 
    if (mm < 10) {
      mm = '0' + mm;
    } 
	
	if(format == 'MM/dd/yyyy')
		return   date = mm + '/' + dd + '/' + yyyy;
	else if (format == 'dd/MM/yyyy')
		return   date = dd + '/' + mm + '/' + yyyy;
	else if (format == 'dd.MM.yyyy')
		return   date = dd + '.' + mm + '.' + yyyy;
	else if (format == 'MM.dd.yyyy')
		return   date = mm + '.' + dd + '.' + yyyy;
	else (format == 'yyyy/MM/dd')
		return   date = yyyy + '/' + mm + '/' + dd;
}
//This function is used to validate the date format
function validateDt(dateVal,format){
		switch(format){
	case 'MM/dd/yyyy': 
		validformat=/^\d{2}\/\d{2}\/\d{4}$/; 
		splitby="/";
		mon=0;
		day=1;
		year=2;
		break;
	case 'dd.MM.yyyy': 
		validformat=/^\d{2}\.\d{2}\.\d{4}$/; 
		splitby=".";
		mon=1;
		day=0;
		year=2;
		break;
	case 'dd/MM/yyyy': 
		validformat=/^\d{2}\/\d{2}\/\d{4}$/; 
		splitby="/";
		mon=1;
		day=0;
		year=2;
		break;
	case 'MM.dd.yyyy': 
		validformat=/^\d{2}\.\d{2}\.\d{4}$/; 
		splitby=".";
		mon=0;
		day=1;
		year=2;
		break;	
	case 'yyyy/MM/dd': 
		validformat=/^\d{4}\/\d{2}\/\d{2}$/;
		splitby="/";
		mon=1;
		day=2;
		year=0;
		break;
	}
	
	var mm=dateVal.split(splitby)[mon];
	var dd=dateVal.split(splitby)[day];
	var yyyy=dateVal.split(splitby)[year];
		
	if (dd.length < 2) {
      dd = '0' + dd;
    } 
    if (mm.length < 2) {
      mm = '0' + mm;
    }

    if(format == 'MM/dd/yyyy')
		dateVal = mm + '/' + dd + '/' + yyyy;
	else if (format == 'dd/MM/yyyy')
		dateVal = dd + '/' + mm + '/' + yyyy;
	else if (format == 'dd.MM.yyyy')
		dateVal = dd + '.' + mm + '.' + yyyy;
	else if (format == 'MM.dd.yyyy')
		dateVal = mm + '.' + dd + '.' + yyyy;
	else if(format == 'yyyy/MM/dd')
		dateVal = yyyy + '/' + mm + '/' + dd;
		
	if (!validformat.test(dateVal))
	{
		return dateVal = 'Y';
	}else{
		
	var monthfield=dateVal.split(splitby)[mon];
		var dayfield=dateVal.split(splitby)[day];
		var yearfield=dateVal.split(splitby)[year];
		var intyearfield = parseInt(yearfield);
		if(intyearfield < 1900)
			Error_Details(msg);
		var dayobj = new Date(yearfield, monthfield-1, dayfield);
		if ((dayobj.getMonth()+1!=monthfield)||(dayobj.getDate()!=dayfield)||(dayobj.getFullYear()!=yearfield))
			return dateVal = 'Y';
		else
			return dateVal = 'N';
	}
}