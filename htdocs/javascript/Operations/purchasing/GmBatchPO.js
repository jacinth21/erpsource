function fnReload(){
	document.frmVendor.hAction.value = 'LoadVendor';
	document.frmVendor.hOpt.value = 'ReLoad';
  	document.frmVendor.submit();
	
}
	function fnBatchUpdate()
	{
		Error_Clear();
		var obj = "";
	document.frmVendor.hAction.value = "BatchPO";
	var obj = document.getElementById("Txt_Batch").value;
	var objval = TRIM(document.frmVendor.Txt_Batch.value);
	document.frmVendor.Txt_Batch.value = objval;
	var objlen = objval.length;
    var vendorname = document.frmVendor.Cbo_VendId.options[document.frmVendor.Cbo_VendId.selectedIndex].text ;
    var vendorid = document.frmVendor.Cbo_VendId.value;
    var poType = document.getElementById("Cbo_Po_Type").value; 
	if(vendorid==0)
	{
		Error_Details('Please choose a valid option for the field: <b>Vendor List</b>');
	}
	if(poType==0)
	{
		Error_Details('Please choose a valid option for the field: <b>Po Type</b>');
	}
	if (objlen == 0)
	{
		Error_Details('No data to submit. Please enter data');
	}
	if (objlen > 35000)
	{
		Error_Details('Exceeded limit of 35,000 characters. Please remove some records');
	}
    // If either of the first two validations fail stop here
	if (ErrorCount > 0)
	{
			Error_Show();
			return false;
	}

	// Validate that entry format is Part Number , Price (comma separated)
	var strTemp = objval;
	//This javascript code removes all 3 types of line breaks
    strTemp = strTemp.replace(/(\r\n|\n|\r)/gm, "|");   

	// Get each line and validate
	var arrLines = strTemp.split('|');	 
	// List of valid characters
   // var regularexp = /^[A-Za-z.0-9,\t\-]+$/i;
	//var regularexp = /^[A-Za-z.0-9, \t\-]+$/i;
	var regularexp = /^[A-Za-z.0-9, \t\-+ ]+$/i;
    var regexquantity = /^[0-9\b]+$/;
                            
	var error_cnt = 0;
    var comma_cnt = 0;
    var strTestLine = "";
    var invalide_qnty ='';
    var strInputStr = '';
    var far_error_str = '';
    var far_flag = '';
    var rework_msg_cnt = '';

	for (var i = 0; i < arrLines.length; i++)
	{
	
	    strTestLine = TRIM(arrLines[i]);
        comma_cnt = 0;    

        // Check characters that are allowed
		if (regularexp.exec(strTestLine) == null) // Special character found
		{
			// Only include the message once
            if (error_cnt == 0) {
      			Error_Details('The format is incorrect for the following part(s). Please correct it to be Part# , Quantity, FAR Flag');
			}
			error_cnt++ ;
			// Include the error line
            Error_Details(strTestLine);

		}
		// Check required number of commas per line (must have 1 and only 1)
        comma_cnt = (strTestLine.split(",").length - 2);  

		if (comma_cnt != 1 )
		{
			// Only include the message once
            if (error_cnt == 0)
             {  	
      			Error_Details('The format is incorrect for the following part(s).  Please use format: <b>Part# , Quantity, FAR Flag</b>');
	         }
			error_cnt++;
			// Include the error line
			Error_Details(strTestLine);
			continue;
		}
		
	 var qnty_check = TRIM((strTestLine.split(",")[1]));
     var part_check = TRIM((strTestLine.split(",")[0])); 
     var part_ext_check = part_check.length ; 
    
		if(part_ext_check==0)
			 {
				if (error_cnt == 0) {
					Error_Details('The format is incorrect for the following part(s).  Please use format: <b>Part# , Quantity, FAR Flag</b> ');
				}
				error_cnt++;
				Error_Details(strTestLine);
				}
		if(qnty_check==0 || !regexquantity.test(qnty_check) ) {
 	              invalide_qnty +=(part_check+", ");
 	       }
		// to validate first article value
		 far_flag = TRIM((strTestLine.split(",")[2]));
		if (far_flag !='Y' && far_flag !='N')
		{
		far_error_str = far_error_str + part_check+', ';
		}
		if (poType == '3101' && far_flag == 'Y') // 3101 is value for rework
			 {
               rework_msg_cnt++;
             }
	
		if(far_error_str!=''){
			far_error_str=far_error_str.substr(0,(TRIM(far_error_str).length-1));
		if (error_cnt == 0) {
		    Error_Details("The format is incorrect for the following part(s).  Please use format: <b>Part# , Quantity, FAR Flag:</b> ");
		}
			error_cnt++;
			Error_Details(strTestLine);
			far_error_str='';
		}
		if(far_flag =='Y' && qnty_check > 1){
            // to form the string (Part #, Qty, FAR Flag)
            strInputStr = strInputStr + part_check +',1,Y|';
            // Reduce one Qty
            qnty_check = qnty_check - 1;
            // reset the first article flag 
            far_flag = 'N';
             }  
		// to form the string (Part #, Qty, FAR Flag)
				strInputStr = strInputStr + part_check +',' + qnty_check +','+ far_flag + '|';
						}
	if(rework_msg_cnt >0)
	{
        Error_Details('Please select a Type Other than Re-Work');
    }
	if(invalide_qnty!=''){
		invalide_qnty=invalide_qnty.substr(0,(TRIM(invalide_qnty).length-1));
		Error_Details("Please enter valid and Positive numbers in <b>Qty </b> for Part # "+invalide_qnty);
	}
		
	if (ErrorCount > 0)
	{
			Error_Show();
			return false;
	}
	else
	{
		if (confirm("Are you sure you want to submit PO for : "+vendorname+" ?"))
		{
			fnStartProgress();
			document.frmVendor.hInputStr.value = strInputStr;
			document.frmVendor.submit();
		}
	}
	}

	function fnReset(){
		document.frmVendor.hAction.value = 'LoadVendor';
		document.frmVendor.hOpt.value = '';
		document.frmVendor.submit();
	}

	