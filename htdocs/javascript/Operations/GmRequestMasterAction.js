/**
 * 
 */

// NSP-504 Changes
function fnSubmit()
{
	  requestId = document.frmRequestMaster.hId.value ;
	  reqStatus = document.frmRequestMaster.reqStatus.value ; 
	var action =    document.frmRequestMaster.Cbo_Action.value ;
    if(action == '0')
    { 
      Error_Details(message[406]); 
    }     
        
    if(requestId == '')
    { 
      Error_Details(message[407]); 
    }   
    else
    {
    	if(reqStatus == '60'){ // Transferred.
       	 Error_Details(message[5043]);
        }
    if( action == 'RCON' )
    {    	
  	  var PurposeType = fnGetPurpose(requestId);
  	  var CNStatus = fnGetCNStatus(requestId);
	  if (CNStatus == '1.20' || CNStatus == '2.20' || CNStatus == '3.30' || CNStatus == '3.60'){
		  Error_Details(message[409]); 
	  }
	  else
	  {		  	  
	  document.frmRequestMaster.strRequestPurpose.value = PurposeType;
      document.frmRequestMaster.hAction.value = "ReloadReconfig";
      document.frmRequestMaster.action = "/GmSetBuildServlet";
	  } 
    }
	
	
	else if( action == 'SBDP' )
    {
	  var CNStatus = fnGetCNStatus(requestId);
	  if (CNStatus == '2.20' || CNStatus == '3.30' || CNStatus == '3.60'){
		  Error_Details(message[409]); 
	  }
	  else
	  {	
      document.frmRequestMaster.strOpt.value = "";
 	  var PurposeType = fnGetPurpose(requestId);
  	  document.frmRequestMaster.strRequestPurpose.value = PurposeType;
      document.frmRequestMaster.action = "/gmSetBuildProcess.do?requestId="+requestId;
	  }
    }  
    
    else if( action == 'PROCCSG' || action =='PROCLOANER'|| action=='BSTR' )
    {
    	var CNStatus = fnGetCNStatus(requestId);
    	if(action=='BSTR' && CNStatus != '2')
    		{
    		Error_Details(message[410]); 
    		}
		if (CNStatus == '1.20' || CNStatus == '2.20' || CNStatus == '3.30' || CNStatus == '3.60'){
			Error_Details(message[409]); 
		}
		else
		{	
		document.frmRequestMaster.hMode.value = 'CON';
		document.frmRequestMaster.hCboAction.value = action;
    	var PurposeType = fnGetPurpose(requestId);
		document.frmRequestMaster.strRequestPurpose.value = PurposeType; 
		document.frmRequestMaster.hAction.value = "EditLoad";
		document.frmRequestMaster.hRequestId.value = requestId;
		document.frmRequestMaster.RE_FORWARD.value = "gmRequestHeader";
		document.frmRequestMaster.RE_RE_FORWARD.value = "gmConsignSetServlet";
		document.frmRequestMaster.FORWARD.value = "gmIncShipDetails";
		//document.frmRequestMaster.action = "/gmRequestHeader.do?requestId="+requestId+"&FORMNAME=frmRequestHeader&hRequestView=headerView";
		document.frmRequestMaster.action = "/gmRequestEdit.do?refId="+requestId+"&source=50184&strOpt=fetch&screenType=Consign&requestId="+requestId+"&FORMNAME=frmRequestHeader&hRequestView=headerView";
		//document.frmRequestMaster.action ="/GmConsignSetServlet";
		//document.frmRequestMaster.submit();    
		}
	}  
   
   
   if( action == 'RREQ' )
    {
		var CNStatus = fnGetCNStatus(requestId);
		if (CNStatus == '1.20' || CNStatus == '2.20' || CNStatus == '3.30' || CNStatus == '3.60'){
			Error_Details(message[409]); 
		}
		else
		{
      //document.frmRequestMaster.hAction.value = "ReloadReconfig";
      document.frmRequestMaster.action = "gmRequestDetail.do?requestId="+requestId;
		}
    }
   
   	
   	if( action == 'RQED' )
    {
      //document.frmRequestMaster.hAction.value = "ReloadReconfig";
      document.frmRequestMaster.action = "gmRequestEdit.do?requestId="+requestId;
    }
   	
    if (action == 'RSTAT' )  
    {  
    	var CNStatus = fnGetCNStatus(requestId);
		if (CNStatus == '1.20' || CNStatus == '2.20' || CNStatus == '3.30' || CNStatus == '3.60'){
			Error_Details(message[409]); 
		}
		else
		{
    	strStatus = document.frmRequestMaster.reqStatus.value;
		var PurposeType = fnGetPurpose(requestId);
    	document.frmRequestMaster.strRequestPurpose.value = PurposeType;
    	if (strStatus == 30){	    	
	    	document.frmRequestMaster.hTxnId.value = requestId;
			document.frmRequestMaster.action ="/GmCommonCancelServlet";
			document.frmRequestMaster.hAction.value = "Load";		
			document.frmRequestMaster.hCancelType.value = 'STSRB'
			document.frmRequestMaster.submit();
		}
		else{
		 	Error_Details(message[27]); 
		}
      //Error_Details(" Under Construction... ");
		}
       
    }
    
    if  (action == 'RVOD' ) 
    {
    	var CNStatus = fnGetCNStatus(requestId);
		if (CNStatus == '1.20' || CNStatus == '2.20' || CNStatus == '3.30' || CNStatus == '3.60'){
			Error_Details(message[409]); 
		}
		else
		{	
    	var reqSoucre = fnGetSourceId(requestId);   //To get the source Id for selected req ID.
		if(reqSoucre == '4000122'){       //For Set Par Source, cannot allow to void.
			Error_Details(message[5566]); 
		}else{
	     	var PurposeType = fnGetPurpose(requestId);
	    	document.frmRequestMaster.strRequestPurpose.value = PurposeType; 
	     	document.frmRequestMaster.action ="/GmCommonCancelServlet";
			document.frmRequestMaster.hTxnId.value =  requestId;
			
			document.frmRequestMaster.hAction.value = 'Load';
		}
		}       
    }
    }
	if (ErrorCount > 0)
		 {
			Error_Show();
			Error_Clear();
			return false;
		}

	fnStartProgress();
	document.frmRequestMaster.submit();
}

//To get source id from the wrapper class hidden field.
function fnGetSourceId(reqId){
	var sourceId = '';
	reqId = reqId.replace(/\-/g,'_');
	var objSoucre = eval("document.frmRequestMaster.hreqsourceid"+reqId);
	if(objSoucre != undefined) sourceId = objSoucre.value;
	
	return sourceId;
}
function fnReload()
{

	
	var varMoC =  document.frmRequestMaster.mainOrChild.value ;
	var varMreq =  document.frmRequestMaster.masterReqID.value ;
	 
	if(varMoC == '50638' && varMreq !='')
	{	 
		 Error_Details(message[408]); 
		 }
	if (ErrorCount > 0)
		 {
			Error_Show();
			Error_Clear();
			return false;
		}
	document.frmRequestMaster.strOpt.value = 'reload';
		
	if(document.frmRequestMaster.requestStatusoper.value != "0")
	{	
 	document.frmRequestMaster.requestStatusval.value=frmRequestMaster.requestStatusoper.options[frmRequestMaster.requestStatusoper.selectedIndex].text;
 	}
	// to set the values to auto complete variable
	if(document.frmRequestMaster.requestTxnID.value !=''){
		document.frmRequestMaster.requestTxnName.value = document.frmRequestMaster.searchrequestTxnID.value;
	}else{
		document.frmRequestMaster.requestTxnName.value = '';
	}
	
	
 	fnStartProgress('Y');
	document.frmRequestMaster.submit();
}

function fnSelectPart(val1,val2)
{
  //  requestId = obj.value ; 
  document.frmRequestMaster.hId.value = val1 ;
  strStatus = val2;  
  document.frmRequestMaster.reqStatus.value = val2 ;
}

function fnFetch()
{	
	document.frmRequestMaster.requestTxnID.value = "";
	fnStartProgress('Y');
	document.frmRequestMaster.submit();
}


 function fnViewDetails(strReqId,strConId)
 {
    
      windowOpener("/gmRequestMaster.do?strOpt=print&requestId="+strReqId+"&consignID="+strConId,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=920,height=600");
    }
    
function fnInitiate()
{
	 document.frmRequestMaster.action = "/gmSetInitiate.do";
	 fnStartProgress('Y');
	 document.frmRequestMaster.submit();
}
  
function fnInitiateItem()
{
	 document.frmRequestMaster.action = "/GmPageControllerServlet?strPgToLoad=gmIncShipDetails.do?screenType=Initiate&RE_FORWARD=gmRequestInitiate&strOpt=";
	 fnStartProgress('Y');
	 document.frmRequestMaster.submit();
}  
      
function fnPrintVer(strConId)
{
windowOpener("/GmConsignSetServlet?hAction=PrintVersion&hId="+strConId,"Con","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnSetList()
{
windowOpener("/GmPageControllerServlet?strPgToLoad=GmSetReportServlet&strOpt=SETRPT","Con","resizable=yes,scrollbars=yes,top=150,left=200,width=680,height=600");
}
function fnOpenOrdLogInv(id)
{
	
	windowOpener("/GmCommonLogServlet?hType=1235&hID="+id,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
}
function fnOpenTag(strCNId)
{
 var strPnum="";
	windowOpener("/gmTag.do?strOpt=reload&pnum="+encodeURIComponent(strPnum)+"&refID="+strCNId+"&refType=51000","PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=1070,height=600");
} 
//When get request id from the wrapper class we cannt get the value.
function fnGetPurpose(strInHousePurpId){
	var  PurposeType = '';
	strInHousePurpId = strInHousePurpId.replace(/\-/g,'_')
	var objPurpose = eval("document.frmRequestMaster.InHousePurose"+strInHousePurpId);
	if(objPurpose != undefined) PurposeType = objPurpose.value;
	return PurposeType;
}

//When get request id from the wrapper class we cannt get the value.
function fnGetCNStatus(requestId){
	var  cnstatus = '';
	requestId = requestId.replace(/\-/g,'_');
	var objCNStatus = eval("document.frmRequestMaster.hCNStatusfl"+requestId);
	if(objCNStatus != undefined) cnstatus = objCNStatus.value;
	return cnstatus;
}