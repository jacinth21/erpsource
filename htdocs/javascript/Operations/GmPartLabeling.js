var gridObj;
var scannedLotCnt = 0;

function fnPageLoad(){
	if(objGridData != ''){ 
		gridObj = initGridData('dataGridDiv',objGridData);
	}
}

//This function is used to initiate grid
function initGridData(divRef,gridData){
	var optionstr;
	var gObj = new dhtmlXGridObject(divRef);
	var action = document.frmPartLabel.action.value;
	
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");

	if(action == '1'){// 1: Pending Inspected; once inspected, then no need to show the dropdown
		gObj._in_header_custom_drpdown=function(tag,index,data){   // this function is to add the dropdown in grid header
			optionstr = '<option value="0">'+message_operations[394]+'</option>';
	         optionstr = optionstr + "<option value=Approve>"+message_operations[395]+"</option>";
	         optionstr = optionstr + "<option value=Reject>"+message_operations[396]+"</option>";
			tag.innerHTML='<select name=ApprRej class=RightText tabindex="-1" onChange="fnPopulateValue(this);">' + optionstr +'</select>';
	     }
		gObj.attachHeader('#rspan,#rspan,#rspan,#rspan,#custom_drpdown');
	}
	gObj.enableAutoHeight(true);
	gObj.init();
	gObj.loadXMLString(gridData);	
	return gObj;	
}

//To populate the value in dropdown based on the value selected in header dropdown
function fnPopulateValue(obj){
	var value = obj.value;
	//var childform = document.getElementsByName('frmPartLabel')[1];// To get parent form name
	var txnTypeColId = gridObj.getColIndexById("txnType");
	
	if(value == 'Approve'){
		gridObj.forEachRow(function(rowId){
			// set all the value to approve; 1400: Approve
			apprRej = gridObj.cellById(rowId, txnTypeColId).setValue('1400');
		});
	}else if(value == 'Reject'){
		gridObj.forEachRow(function(rowId){
			// set all the value to reject; 1401: Reject
			apprRej = gridObj.cellById(rowId, txnTypeColId).setValue('1401');
		});
	}else{
		gridObj.forEachRow(function(rowId){
			// reset the values to choose one
			apprRej = gridObj.cellById(rowId, txnTypeColId).setValue('0');
		});
	}
}

// To submit the Pending Inspection screen
function fnSubmit(){
	var approveStr = '';
	var rejectStr = '';
	var v_method = '';
	var errPart = '';
	var price = '';
	
	var pnum, ctrlNum, qty, price, apprRej,eqPrice;
	
	gridObj.forEachRow(function(rowId){
		// get the selected value from the grid
		pnum	= gridObj.cellById(rowId, gridObj.getColIndexById("pnum")).getValue();
		ctrlNum = gridObj.cellById(rowId, gridObj.getColIndexById("control")).getValue();
		qty = gridObj.cellById(rowId, gridObj.getColIndexById("pqty")).getValue();
		eqPrice = gridObj.cellById(rowId, gridObj.getColIndexById("eqprice")).getValue();
		apprRej = gridObj.cellById(rowId, gridObj.getColIndexById("txnType")).getValue();
		if(apprRej == 1400){
			v_method = 'savePartApproveDetails';
	 		approveStr = approveStr + pnum +"," + qty +"," + ctrlNum + "," + price+ "|";
		}else if(apprRej == 1401){
	 		v_method = 'savePartRejectDetails';
	 		rejectStr = rejectStr + pnum +"," + qty +"," + ctrlNum + "," + eqPrice+ "|";
	 	}else if(apprRej == 0){
	 		errPart += errPart+ pnum + ",";
	 	}
	});
	
	if(approveStr != '' && rejectStr != ''){
		v_method = 'savePartApprRejDetails';
	}
	
	if(errPart != ''){
		Error_Details(message_operations[760]);
	}
	
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	fnStartProgress();
	document.frmPartLabel.haction.value = v_method;
	document.frmPartLabel.hApprString.value = approveStr;
	document.frmPartLabel.hRejString.value = rejectStr;
	document.frmPartLabel.action = '/gmPartLabel.do?companyInfo='+companyInfoObj+'&method=savePartApprRejDetails';		 
	document.frmPartLabel.submit();
}

// To void the transaction
function fnVoid(){
	var txnId = document.getElementById("txnid").value;
	var cancelType = 'VFGTL';	
	document.getElementById("frmPartLabel").action="/GmCommonCancelServlet?hTxnName="+txnId+"&hCancelType="+cancelType+"&hTxnId="+txnId;
	document.getElementById("frmPartLabel").submit();
}


//function to save attribute value when check/uncheked
function fnCheck(obj){
	var attribValue = '';
	if(obj=='PartLabelChk'){
	        if(document.getElementById("chkprintlable").checked == true){
		               attribValue = 'Y';
		            	               
	         }else{
	                   attribValue = 'N';
	                               
	         }
	         var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmPartLabel.do?method=saveChkBox&Txt_txn_id='+txnid+'&Txt_AttribValue='+attribValue+'&Txt_AttribType='+104521+'&ramdomId=' + Math.random()); //104521---Labels Printed
	         dhtmlxAjax.get(ajaxUrl, function(loader){
		     response = loader.xmlDoc.responseText;
             });
	 }
    if(obj=='QCLabelChk'){  
	        if(document.getElementById("chkqcverification").checked == true){
	                   attribValue = 'Y';
	                   
	         }else{
	                   attribValue = 'N';
	                  
	         }		 
	         var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmPartLabel.do?method=saveChkBox&Txt_txn_id='+txnid+'&Txt_AttribValue='+attribValue+'&Txt_AttribType='+104522+'&ramdomId=' + Math.random());//104522-- QC Verification of Labels
		     dhtmlxAjax.get(ajaxUrl, function(loader){
	         response = loader.xmlDoc.responseText;
	         });
     }
    if(obj=='PreStagingChk'){  
        if(document.getElementById("preStaging").checked == true){
                   attribValue = 'Y';
                   
         }else{
                   attribValue = 'N';
                  
         }		 
         var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmPartLabel.do?method=saveChkBox&Txt_txn_id='+txnid+'&Txt_AttribValue='+attribValue+'&Txt_AttribType='+104523+'&ramdomId=' + Math.random());//104523-- Pre-Staging
	     dhtmlxAjax.get(ajaxUrl, function(loader){
         response = loader.xmlDoc.responseText;
         });
 }
    if(obj=='StagingChk'){  
        if(document.getElementById("staging").checked == true){
                   attribValue = 'Y';
                   
         }else{
                   attribValue = 'N';
                  
         }		 
         var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmPartLabel.do?method=saveChkBox&Txt_txn_id='+txnid+'&Txt_AttribValue='+attribValue+'&Txt_AttribType='+104524+'&ramdomId=' + Math.random());//104524-- Staging
	     dhtmlxAjax.get(ajaxUrl, function(loader){
         response = loader.xmlDoc.responseText;
         });
 }
	
}

function fnSaveComments()
{
	fnStartProgress("Y");
	document.frmPartLabel.txnid.value = txnid;
	document.frmPartLabel.action = '/gmPartLabel.do?method=saveComments';
	document.frmPartLabel.submit();
}

function fnSave()
{
	fnStartProgress("Y");
	document.frmPartLabel.action = '/gmPartLabel.do?method=savPrintlabelProcess&Txt_txn_id='+txnid+'&statusflg='+statusfl;
	document.frmPartLabel.submit();
}

// To expand the summary of Transaction
function fnExpandSummary(comments){
	window.open("/operations/GmPartLabelingSummary.jsp?comments="+comments,null,"resizable=yes,scrollbars=yes,height=150,width=350,top=300,left=500");
}

//while Enter the lot code text box in Release shipment screen at verification status,need to call fnValidateLotCode() to validate the Lot.
function fnVerifyLotCode(obj){
	if(event.keyCode == 13){
		fnValidateCode(obj,gridObj);
		
		if(!e) var e = window.event;
        
		e.cancelBubble = true;
		e.returnValue = false;
      
		if (e.stopPropagation)
		{
			e.stopPropagation();
			e.preventDefault();
		}
	}
}

function fnValidateCode(obj,gridObj){
	var scanedval = TRIM(obj.value);
	var varLotColInd = gridObj.getColIndexById("control");
	var varImgInd = gridObj.getColIndexById("ChkBoxRefId");
	var varPartInd = gridObj.getColIndexById("pnum");
	var lotTotalcount = gridObj.getRowsNum();
	var lotcode;
	var lotExistFl = false;
	var indexof = '0';
	var cntlindexof = '0';
	var scanedPart = '';
	var partNum = '';
	
	indexof = scanedval.indexOf("^"); // get the index of '~'
	cntlindexof = scanedval.indexOf("(10)"); // get the index of '(10)'
	
	if(indexof != -1){// While scanning only format will be "partno^controlno"
		var cnumarr= scanedval.split("^");
		scanedPart = cnumarr[0]; // get the characters before '^'
		scanedval = cnumarr[1]; //Getting the remaining character, after "^"
	}else if(cntlindexof != -1){// While scanning only format will be "(01)udi number(17)exp date(10)controlno"		
		var cnumArray = scanedval.split("(10)");
		scanedval = cnumArray[1];	// //Getting the remaining character, after "(10)"		
	}
	scanedval = scanedval.toUpperCase();
	
	/* Description for below validation: Error thorows if LotCode text box is empty and ReleaseShipment button is disable 
	 */
	if(scanedval == ''){
		Error_Details(message_operations[107]);
		fngenerateDivError();
        return false;
	}
	
	if(scanedval != ''){
		gridObj.forEachRow(function(rowId){
			lotcode = gridObj.cellById(rowId, varLotColInd).getValue();
			partNum = gridObj.cellById(rowId, varPartInd).getValue();
			if(scanedval == lotcode){
				/* Description for below validation: Error throws if Scanned part and parts in grid are not matched
				 */
				lotExistFl = true;
				if(scanedPart != '' && scanedPart != partNum){
					var array = [scanedPart,scanedval]
					Error_Details(Error_Details_Trans(message_operations[108],array));
					fngenerateDivError()
			        return false;
				}
				/* Description for below validation: Error throws if the check image is already scanned
				 */
				if(gridObj.cellById(rowId, varImgInd).getValue() != ''){
					Error_Details(Error_Details_Trans(message_operations[109],scanedval));
					fngenerateDivError()
			        return false;
				}
				document.all.errormessage.innerHTML = '';
				gridObj.cellById(rowId, varImgInd).setValue('<img src="/images/success.gif" height="17" width="18" alt="Success" border="0">');
				scannedLotCnt++;
				
				if(scannedLotCnt == lotTotalcount){
					fnEnableRelButton('true');
				}
			}
			/* Description for below validation: Error throws if the Scanned Lot is not available in Grid
			 */
			if(!lotExistFl){
				Error_Details(Error_Details_Trans(message_operations[110],scanedval));
				fngenerateDivError()
		        return false;
			}
		});
	}
	document.getElementById("scanLotCode").value = '';
	document.getElementById("scanLotCode").focus();

}

function fnVerify(){
	var strInput = '';
	var pnum, qty, ctrlNum, locat, apprRej;
	var varImgInd = gridObj.getColIndexById("ChkBoxRefId");
	gridObj.forEachRow(function(rowId){		
		// get the selected value from the grid
		pnum	= gridObj.cellById(rowId, gridObj.getColIndexById("pnum")).getValue();
		qty = gridObj.cellById(rowId, gridObj.getColIndexById("pqty")).getValue();
		ctrlNum = gridObj.cellById(rowId, gridObj.getColIndexById("control")).getValue();
		locat = '90800'; //Put Transacted 
		strInput = strInput + pnum +","+qty + ","+ ctrlNum + ","+""+","+ ctrlNum + ","+ locat +"|";	
	});
	
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	//PC-5928 Encode URL issue for paramters which are passed in the request without encoding.
	strInput=encodeURIComponent(strInput);
	fnStartProgress("Y");
	document.frmPartLabel.action = '/gmPartLabel.do?method=verifyPrintLableProcess&Txt_txn_id='+txnid+'&txntypenm='+txntypenm+'&strInput='+strInput+'&statusflg='+statusfl;
	document.frmPartLabel.submit();
}
// To enabel the button
function fnEnableRelButton(obj){
	var checkedFl = (obj.value)? obj.checked: obj;
	if(checkedFl && verifyBtnAccess == 'Y'){
		document.all.Btn_submit.disabled = false;
	}else{
		document.all.Btn_submit.disabled = true;
	}
}

//Function to mark all the lot number as checked and to enable release shipment button
function fnEnableButton1(obj){
	var checkedFl = obj.checked;
	if(checkedFl){
		fnShowCheckMark();
	}
	else{
		fnHideCheckMark();
		scannedLotCnt = 0;
	}
	
	fnEnableRelButton(obj);
}

//To check all the check marks when we check the Master checkbox
function fnShowCheckMark(){
	gridObj.forEachRow(function(rowId){
		 gridObj.cellById(rowId, 0).setValue('<img src="/images/success.gif" height="17" width="18" alt="Success" border="0">');
	});
}

//To hide all the check marks when we uncheck the Master checkbox
function fnHideCheckMark(){
	gridObj.forEachRow(function(rowId){
		 gridObj.cellById(rowId, 0).setValue('');
	});
}

//Function to show the error details in the Scan Lot Code section
function fngenerateDivError(errStatus){
	var messgaeColor = "red";
	var vCode = "";
	for(var i = 0 ; i <= (ErrorCount-1); i++){		
		fnPlayErrorSound('beep');
		vCode = vCode + "<TR> <TD align=left> <font color = '"+messgaeColor+"'>"+ ErrorDetail[i][0] + "</font></TD>" ;
		vCode = vCode + "</TR>";
		vCode = vCode + "<tr>";
		vCode = vCode + "</tr>";
	}
	objError = document.all.errormessage;
	objError.innerHTML = vCode;
	document.getElementById("scanLotCode").value = '';
	document.getElementById("scanLotCode").focus();
	Error_Clear();
	return false;
}

function fnPlayErrorSound(soundObj) {
	try{ 
		  var sound = document.getElementById(soundObj);
		   
		   if (sound)
				 sound.Play();
		 }
	catch (err)
		{
		 return null;
		} 

}