function fnCallLoanerDistDetails(setid,value)
{
	setid = setid.replace("#","");	
	var lnTyp = document.frmLoaner.Cbo_lnType.value;
	windowOpener('/GmLoanerPartRepServlet?hAction=LoanerStatusByDist&hOpt=null&hScreenType=LoanerStRpt&hSetId='+setid+'&hLnTyp='+lnTyp,"LoanerSet","resizable=yes,scrollbars=yes,top=150,left=200,width=900,height=400");
}

function fnSort(varColumnName, varSortOrder)
{
	document.frmLoaner.hSortColumn.value = varColumnName;
	document.frmLoaner.hSortOrder.value = varSortOrder;
	document.frmLoaner.submit();
}
function fnPrint()
{	
	//document.getElementById("tablediv").style.overflow ="visible";
	//var pagePrint = document.getElementById("myHighlightTable").innerHTML;	
	document.getElementById("Btn_Print").style.visibility='hidden';
	window.print();  
	//document.getElementById("tablediv").style.overflow ="auto";
	document.getElementById("Btn_Print").style.visibility='visible';
} 

function fnGo(){
	fnValidateDropDn('Cbo_lnType',lblType);// to validate the dropdown
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}else{
		document.frmLoaner.Btn_Go.disabled=true; // To disable the Load button
		fnStartProgress();
		document.frmLoaner.submit();
	}
}




function fnOnPageLoad(){
	

   if (objGridData != '') {
    	 // to form the grid string

	   var rows = [], i = 0;
       var JSON_Array_obj = "";
       var Set_ID = "";
       var Set_Name = "";
       var Pnd_Rtn = "";
       var Avail = "";
       var OpenReq = "";
       var Pnd_Apr_Req = "";
       var Alloc = "";
       var Pnd_Ship = "";
       var PIP = "";
       var RFP = "";
       var Deployed = "";
       var Missed = "";
       var Pnd_Chk = "";
       var WIP = "";
       var Pnd_Vrf = "";
       var Pnd_Prc = "";
       var Pnd_Pic = "";
       var Pen_Pick = "";
       var Pnd_FG = "";
       var Pnd_Accept = "";
       var setHeader = "";
       var setFilter = "";
       var setColIds = "";
       var enableTooltips = "";
       var gridHeight = "";
       var footerStyles = "";
       var footerExportFL ="";
       var Total="0";
       var dataHost = "";
   // to set background colour for each cell
		var gridrows = ""
		var gridrowsarr = "";
		var arrLen = "";
		var footer=new Array();
		footer[0]="";
		footer[1]="<center> <b> Total  </b> </center>";
		footer[2]="<b><center>{#stat_total}<b> </center>";
		footer[3]="<b><center>{#stat_total}<b> </center>";
		footer[4]="<b><center>{#stat_total}<b> </center>";
		footer[5]="<b><center>{#stat_total}<b> </center>";
		footer[6]="<b><center>{#stat_total}<b> </center>";	
		footer[7]="<b><center>{#stat_total}<b> </center>";	
		footer[8]="<b><center>{#stat_total}<b> </center>";	
		footer[9]="<b><center>{#stat_total}<b> </center>";	
		footer[10]="<b><center>{#stat_total}<b> </center>";	
		footer[11]="<b><center>{#stat_total}<b> </center>";	
		footer[12]="<b><center>{#stat_total}<b> </center>";	
		footer[13]="<b><center>{#stat_total}<b> </center>";	
		footer[14]="<b><center>{#stat_total}<b> </center>";	
		footer[15]="<b><center>{#stat_total}<b> </center>";	
		footer[16]="<b><center>{#stat_total}<b> </center>";	
		footer[17]="<b><center>{#stat_total}<b> </center>";
		footer[18]="<b><center>{#stat_total}<b> </center>";	
		footer[19]="<b><center>{#stat_total}<b> </center>";	
		footer[20]="<b><center>{#stat_total}<b> </center>";   ///Sub total for Disputed count
		footer[21]="<b><center>{#stat_total}<b> </center>";	

		
		
       JSON_Array_obj = JSON.parse(objGridData);
       $.each(JSON_Array_obj, function(index, jsonObject) {

    	Set_ID = jsonObject.Set_ID;
    	
    	Set_Name = jsonObject.Set_Name;
    	Pnd_Rtn = jsonObject.Pnd_Rtn;
    	Avail = jsonObject.Avail;
    	OpenReq=jsonObject.OpenReq;
    	Pen_Pick=jsonObject.Pen_Pick;
    	Pnd_Apr_Req=jsonObject.Pnd_Apr_Req;
    	Alloc=jsonObject.Alloc;
    	Pnd_Ship = jsonObject.Pnd_Ship
    	PIP = jsonObject.PIP;
    	RFP = jsonObject.RFP;
    	Pnd_Accept=jsonObject.Pen_Accept;
    	Missed = jsonObject.Missed;
    	Pnd_Chk= jsonObject.Pnd_Chk;
    	WIP = jsonObject.WIP;
    	Pnd_Vrf = jsonObject.Pnd_Vrf;
    	Pnd_Prc = jsonObject.Pnd_Prc;
    	Pnd_Pic = jsonObject.Pnd_Pic;
    	Pnd_FG = jsonObject.Pnd_Fg;
    	Deployed= jsonObject.Deployed;
    	Disputed= jsonObject.Disputed;   
    	Total= jsonObject.Total;


    	 // default array 
        rows[i] = {};
        rows[i].data = [];
        rows[i].id = Set_ID;
        rows[i].data[0]= 
        	
        	"<a href=\"javascript:fnCallLoanerDistDetails(&#39#"+Set_ID+"&#39);\" >" +
			"<img  id=\"imgEdit\" style=\"cursor:hand\" border=\"0\" src=\"/images/icon-letter.png\" width=\"14\" height=\"14\" align=\"left\"></a>"+Set_ID+"  ";
        	
         
        rows[i].data[1] =  Set_Name; 
        rows[i].data[2] =  Pnd_Apr_Req;
        rows[i].data[3] =  OpenReq;
        rows[i].data[4] =  Avail;
        rows[i].data[5] =  Pen_Pick;
        rows[i].data[6] =  Alloc;
        rows[i].data[7] =  Pnd_Ship;
        rows[i].data[8] =  PIP;
        rows[i].data[9] =  RFP;
        rows[i].data[10] =  Pnd_Rtn;
        rows[i].data[11] = Pnd_Accept;
        rows[i].data[12] = Pnd_Chk;
        rows[i].data[13] = WIP;
        rows[i].data[14] = Pnd_Vrf;
        rows[i].data[15] = Pnd_Pic;
        rows[i].data[16] = Pnd_Prc;
        rows[i].data[17] = Pnd_FG;
        rows[i].data[18] = Disputed;
        rows[i].data[19] = Missed;
        rows[i].data[20] = Deployed;
        rows[i].data[21] = rows[i].data[4]+rows[i].data[5]+rows[i].data[6]+rows[i].data[7]
        +rows[i].data[8]+rows[i].data[9]+rows[i].data[10]+rows[i].data[11]+rows[i].data[12]+rows[i].data[13]+rows[i].data[14]	
        +rows[i].data[15]+rows[i].data[16]+rows[i].data[17]+rows[i].data[18]+rows[i].data[19]+rows[i].data[20];


        i++;
       });
       
       setInitWidths = "90,350,60,60,50,50,50,50,50,40,50,50,50,50,50,50,50,50,60,65,64,50";
       setColAlign = "left,left,center,center,center,center,center,center,center,center,center,center,center,center,center,center,center,center,center,center,center,center";
       setColTypes = "ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro";
       setColSorting = "str,str,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int";
       setHeader = [ "Set ID", " Set Name", " Pnd Apr Req", "Open Req", "Avail", "Pnd Pick"," Alloc","Pnd Shp","PIP", "RFP","Pnd Rtn","Pnd Accept", "Pnd Chk","WIP","Pnd Vrf","Pnd Prc","Pnd Pic","Pnd FG","Disputed","Missed","Deployed","Total"];
       setFilter = [ "#text_filter","#text_filter","#numeric_filter", "#numeric_filter","#numeric_filter","#numeric_filter","#numeric_filter","#numeric_filter", "#numeric_filter","#numeric_filter","#numeric_filter","#numeric_filter","#numeric_filter","#numeric_filter"
                     ,"#numeric_filter","#numeric_filter","#numeric_filter","#numeric_filter","#numeric_filter","#numeric_filter","#numeric_filter","#numeric_filter"];
       setColIds = "Set_ID,Set_Name,Pnd_Apr_Req,OpenReq,Avail,Pnd_Pick,Alloc,Pnd_Ship,PIP,RFP,Pnd_Rtn,Pnd_Accept,Pnd_Chk,WIP,Pnd_Vrf,Pnd_Prc,Pnd_Pic,Pnd_FG,Disputed,Missed,Deployed,Total";
       enableTooltips = "true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true";
       document.getElementById('dataGridDiv').setAttribute("style","width:1490px");
       setFooterAlign="center"
       
       footerArry = [];
       gridHeight = "680";	

       footerStyles = [];
       footerExportFL = true;	
       dataHost = {};
       dataHost.rows = rows;
   	
       
     //  gridObj = initGridWithDistributedParsing('dataGridDiv', objGridData,footer);

       
       gridObj = loadDHTMLXGrid('dataGridDiv', gridHeight, dataHost, setHeader,
               setInitWidths, setColAlign, setColTypes, setColSorting,
               setColIds, enableTooltips, setFilter, footer, footerStyles,
               footerExportFL);

       // to set background colour for each cell
	    Set_ID = gridObj.getColIndexById("Set_ID");

	    gridrows = gridObj.getAllRowIds(",");
	    gridrowsarr = gridrows.toString().split(",");
	    arrLen = gridrowsarr.length;
	    
	    Set_ID = gridObj.getColIndexById("Set_ID");
	    Set_Name = gridObj.getColIndexById("Set_Name");
	    Pnd_Apr_Req = gridObj.getColIndexById("Pnd_Apr_Req");
	    OpenReq = gridObj.getColIndexById("OpenReq");
	    Avail = gridObj.getColIndexById("Avail");
	    Pnd_Pick = gridObj.getColIndexById("Pnd_Pick");
	    Alloc = gridObj.getColIndexById("Alloc");
	    Pnd_Ship = gridObj.getColIndexById("Pnd_Ship");
	    PIP = gridObj.getColIndexById("PIP");
	    RFP = gridObj.getColIndexById("RFP");
	    Pnd_Rtn = gridObj.getColIndexById("Pnd_Rtn");
	    Pnd_Accept = gridObj.getColIndexById("Pnd_Accept");
	    Pnd_Chk = gridObj.getColIndexById("Pnd_Chk");
	    WIP = gridObj.getColIndexById("WIP");
	    Pnd_Vrf = gridObj.getColIndexById("Pnd_Vrf");
	    Pnd_Prc = gridObj.getColIndexById("Pnd_Prc");
	    Pnd_Pic = gridObj.getColIndexById("Pnd_Pic");
	    Pnd_FG = gridObj.getColIndexById("Pnd_FG");
	    Missed = gridObj.getColIndexById("Missed"); 
	    Deployed =gridObj.getColIndexById("Deployed");
	    Disputed =gridObj.getColIndexById("Disputed");  
	    Total =gridObj.getColIndexById("Total");


	    

	    for (var i = 0; i < arrLen; i++) {
			rowId = gridrowsarr[i];
		
			
	    }
	    
	    document.getElementById("dataGridDiv").style.display = 'block';
        // document.getElementById("divNoExcel").style.display = 'block';
         document.getElementById("DivNoMsg").style.display = 'none';
	} else {
       // document.getElementById("divNoExcel").style.display = 'none';
       // document.getElementById("dataGridDiv").style.display = 'none';
       // document.getElementById("DivNoMsg").style.display = 'block';
}


    }
	
		

function fnExport(str){
	
	ExcellgridObj=gridObj;
	ExcellgridObj.forEachRow(function(id){ // function that gets id of the row as an incoming argument
	    // here id - id of the row
		for (var i=0; i<ExcellgridObj.getColumnCount(); i++){
	        // i - index of the column
	        var val = ExcellgridObj.cells(id,i).getValue();
	        if (val == "-"){                
	        	ExcellgridObj.cells(id,i).setValue('--');
	        }
		}
	    
	})

		ExcellgridObj.toExcel('/phpapp/excel/generate.php');	
	}