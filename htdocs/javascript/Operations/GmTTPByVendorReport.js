function fnOnpageLoad() {
	document.getElementById("dataGridDiv").style.display = 'none';
	document.getElementById("DivNothingMessage").style.display = 'none';
	document.getElementById("DivExportExcel").style.display = 'none';
	
	var ttpId = document.frmTTPbyVendorReport.ttpId.value;
	var ttpMonth = document.frmTTPbyVendorReport.monthId.value;
	var ttpYear = document.frmTTPbyVendorReport.yearId.value;
	var vendorId = document.frmTTPbyVendorReport.vendorId.value;
	
	if(ttpId != '' || vendorId != ''){
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmTTPbyVendorReport.do?method=fetchTTPbyVendorSummary&ttpId='
				+ ttpId
				+ '&monthId='
				+ ttpMonth
				+ '&yearId='
				+ ttpYear
				+ '&vendorId=' + vendorId + '&ramdomId=' + new Date().getTime());
			dhtmlxAjax.get(ajaxUrl, fnVendorReportResponse)
			fnStartProgress();
	}
	
}

function fnVendorReportLoad(){
	var ttpId = document.frmTTPbyVendorReport.ttpId.value;
	var ttpMonth = document.frmTTPbyVendorReport.monthId.value;
	var ttpYear = document.frmTTPbyVendorReport.yearId.value;
	var vendorId = document.frmTTPbyVendorReport.vendorId.value;
	
	if (ttpId == '' && vendorId == '') {
			Error_Details(message_operations[825]);
	}
	fnValidateDropDn('monthId','Month');
	fnValidateDropDn('yearId','Year');
	
	
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmTTPbyVendorReport.do?method=fetchTTPbyVendorSummary&ttpId='
					+ ttpId
					+ '&monthId='
					+ ttpMonth
					+ '&yearId='
					+ ttpYear
					+ '&vendorId=' + vendorId + '&ramdomId=' + new Date().getTime());
				dhtmlxAjax.get(ajaxUrl, fnVendorReportResponse)
				fnStartProgress();
}

//this  call back function used to generate the DHTMLX Grid in JS
function fnVendorReportResponse(loader) {
	response = loader.xmlDoc.responseText;
	fnStopProgress();
	if(response != ''){
		setColIds = "ttpname,status,vendorname,period,recommandqty,raisepoqty,ordercost,ttpid,vendorid,month,year";
		var setColTypes = "ttpnmFl,ro,ro,ro,ro,ro,ron,ro,ro,ro,ro";
		var setColAlign = "left,left,left,left,right,right,right";
		var setColSorting = "str,str,str,str,int,int,int,str,str,str,str";
		var enableTooltips = "true,true,true,true,true,true,true,true,true,true,true";
		var setHeader = [ "TTP Name","Status","Vendor","Period","Recomm. Qty","Raise PO Qty","Order Amt","","","","" ];
		var setFilter = [ "#select_filter", "#select_filter", "#select_filter","#select_filter", "#numeric_filter", "#numeric_filter", "#numeric_filter"];
		var setInitWidths = "290,150,290,150,100,100,193";
		var gridHeight = "";
		var dataHost = {};	
		format = "Y"
		var footerArry=new Array();	
		footerArry[0]="";
		footerArry[1]="";
		footerArry[2]="";
		footerArry[3]="<b>Total<b>";
		footerArry[4]="<b>{#stat_total}<b>";
		footerArry[5]="<b>{#stat_total}<b>";
		footerArry[6]="<b>{#stat_total}<b>";
		var footerStyles = [];
		var footerExportFL = true;
		
		document.getElementById("dataGridDiv").style.display = 'block';
		document.getElementById("DivNothingMessage").style.display = 'none';
		document.getElementById("DivExportExcel").style.display = 'block';
		
		// split the functions to avoid multiple parameters passing in single function
		gridObj = initGridObject('dataGridDiv');
		gridObj = fnCustomTypes(gridObj, setHeader, setColIds, setInitWidths,setColAlign, setColTypes, setColSorting, enableTooltips,
								                 setFilter, 400, footerArry,footerStyles);
		if (format != ""){
			var formatDecimal = "0,000.00";
			gridObj.setNumberFormat(formatDecimal,6,".",",");
		}
		gridObj = loadDhtmlxGridByJsonData(gridObj, response);
		gridObj.attachEvent("onRowSelect",function(rowId,cellIndex){
			if(cellIndex == 0){
				var ttpId= gridObj.cellById(rowId, 7).getValue();
				var vendorId= "";
				var monthId= gridObj.cellById(rowId, 9).getValue();
				var yearId= gridObj.cellById(rowId, 10).getValue();
				var ttpName= gridObj.cellById(rowId, 0).getValue();
				fnTTPbyVendorReport(ttpId,vendorId,yearId,monthId,ttpName);
			}  
		});
		gridObj.setColumnHidden(7, true);
		gridObj.setColumnHidden(8, true);
		gridObj.setColumnHidden(9, true);
		gridObj.setColumnHidden(10, true);
	}else {
		document.getElementById("dataGridDiv").style.display = 'none';
		document.getElementById("DivNothingMessage").style.display = 'block';
		document.getElementById("DivExportExcel").style.display = 'none';		
	}
}
//This function used to create user defined column types in Grid
//This Function for hyperlink ttp name
function eXcell_ttpnmFl(cell) {
	// the eXcell name is defined here
	if (cell) { // the default pattern, just copy it
		this.cell = cell;
		this.grid = this.cell.parentNode.grid;
	}
	
	this.setValue = function(val) {
		this.setCValue("<a href=\"#\">"+val+"",val);		
	}

	this.getValue = function() {
		return this.cell.childNodes[0].innerHTML; // gets the value
	}
}
eXcell_ttpnmFl.prototype = new eXcell;
//This Function Used to set custom types conditions in Grid
function fnCustomTypes(gObj, setHeader, setColIds, setInitWidths, setColAlign,
			setColTypes, setColSorting, enableTooltips, setFilter, gridHeight,footerArry,footerStyles) {
	gObj.setHeader(setHeader);
	var colCount = setHeader.length;
	gObj.setInitWidths(setInitWidths);
	gObj.setColAlign(setColAlign);
	gObj.setColTypes(setColTypes);
	gObj.setColSorting(setColSorting);
	gObj.enableTooltips(enableTooltips);
	gObj.attachHeader(setFilter);
	gObj.setColumnIds(setColIds);
	if (footerArry != undefined && footerArry.length > 0) {
		var footstr = eval(footerArry).join(",");
			if (footerStyles != undefined && footerStyles.length > 0)
				gObj.attachFooter(footstr, footerStyles);
			else
				gObj.attachFooter(footstr);
	}  
		gObj.enableAutoHeight(true, gridHeight, true);	
		return gObj;
}

function fnExport() {
	gridObj.setColumnHidden(7, true);
	gridObj.setColumnHidden(8, true);
	gridObj.setColumnHidden(9, true);
	gridObj.setColumnHidden(10, true);
	gridObj.toExcel('/phpapp/excel/generate.php');
	gridObj.setColumnHidden(7, false);
	gridObj.setColumnHidden(8, false);
	gridObj.setColumnHidden(9, false);
	gridObj.setColumnHidden(10, false);	
}

function fnTTPbyVendorReport(ttpId,vendorId,yearId,monthId,ttpName){
	windowOpener("/gmTTPByVendorSummary.do?method=loadTTPbyVendorSummary&ttpId="+ttpId+"&ttpMonth="+monthId+"&ttpYear="+yearId+"&vendorId="+vendorId+"&ttpName="+ttpName,"Search","resizable=yes,scrollbars=yes,top=150,left=150,width=1550,height=700");
}