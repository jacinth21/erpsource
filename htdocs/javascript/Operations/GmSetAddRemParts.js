function fnSubmit()
{
	document.frmSetAddRemParts.hAction.value = 'Save';
	fnStartProgress('Y');
  	document.frmSetAddRemParts.submit();
}

function fnInitiate()
{
	document.frmSetAddRemParts.hAction.value = 'EditLoad';
	fnValidateDropDn('Cbo_OprType', lblType);
	fnValidateDropDn('Cbo_Reason', lblreason);
	if (ErrorCount > 0) {
        Error_Show();
        Error_Clear();
        return false;
    }

	fnStartProgress('Y');
  	document.frmSetAddRemParts.submit();
}

function fnAddToCart()
{
	document.frmSetAddRemParts.hAction.value = "GoCart";
	fnStartProgress('Y');
	document.frmSetAddRemParts.submit();
}

function fnRemoveItem(val)
{
	document.frmSetAddRemParts.hDelPartNum.value = val;
	document.frmSetAddRemParts.hAction.value = "RemoveCart";
	document.frmSetAddRemParts.submit();	
}

function fnUpdateCart()
{
	document.frmSetAddRemParts.hAction.value = "UpdateCart";
	fnValidateDropDn('Cbo_OprType', lblType);
	fnValidateDropDn('Cbo_Reason', lblreason);
	if (ErrorCount > 0) {
        Error_Show();
        Error_Clear();
        return false;
    }

	document.frmSetAddRemParts.submit();	
}

function fnPlaceOrder()
{
	var arr = document.frmSetAddRemParts.hPartNums.value.split(',');
	var oprtype = document.frmSetAddRemParts.Cbo_OprType.value;
	var pnum = '';
	var stock = '';
	var qty = '';
	var obj = '';
	var cnum = '';
	Error_Clear();
	var strErrorMore = '';
	var strErrorSetCsgQty = '';
	var j = 0;
	fnValidateDropDn('Cbo_OprType', lblType);
	fnValidateDropDn('Cbo_Reason', lblreason);
	
    for(var i=0;i<arr.length;i++)
    {
        qid = arr[i];
        j++;
		obj = eval("document.frmSetAddRemParts.hStock"+j);
		stock = parseInt(obj.value);
		obj = eval("document.frmSetAddRemParts.Txt_Qty"+j);
		qty = parseInt(obj.value);
		obj = eval("document.frmSetAddRemParts.hQtyInCsgSet"+j);
		qtyInCsgSet = parseInt(obj.value);

		if (oprtype == "50151" || oprtype == "50152" || oprtype == "50153" || oprtype == "50156" || oprtype == "50157" || oprtype == "50158")
		{
			if (qty > qtyInCsgSet)
			{
				strErrorSetCsgQty  = strErrorSetCsgQty + ","+ qid;
			}
		}
		else
		{
			if (qty > stock)
			{
				strErrorMore  = strErrorMore + ","+ qid;
			}
		}		
		
	}

	if (strErrorMore != '')
	{
		Error_Details(Error_Details_Trans(message_operations[310],strErrorMore.substr(1,strErrorMore.length)));
	}
	
	if (strErrorSetCsgQty != '')
	{
		Error_Details(Error_Details_Trans(message_operations[311],strErrorSetCsgQty.substr(1,strErrorSetCsgQty.length)));
	}

		if (ErrorCount > 0)
		{
			Error_Show();
			return false;
		}else{
			document.frmSetAddRemParts.hAction.value = 'PlaceOrder';
			fnStartProgress('Y');
		  	document.frmSetAddRemParts.submit();
	  	}
}


function fnSetReason()
{
	var objType = document.frmSetAddRemParts.Cbo_OprType;
	var obj = document.frmSetAddRemParts.Cbo_Reason;
	var oper = objType.value;
	var j = 0;
	obj.options.length = 0;

	obj.options[0] = new Option("[Choose One]","0");

	for (i=0;i<len ;i++ )
	{
		arr = eval("arr"+i);
		arrobj = arr.split(",");
		reasonid = arrobj[2];
		if (reasonid == oper)
		{
			j++;
			reasid = arrobj[0];
			reasname = arrobj[1];
			obj.options[j] = new Option(reasname,reasid);
			if (document.frmSetAddRemParts.hReasonId.value == reasid)
			{
				obj.options[j].selected = true;
			}
		}
	}
	
}

function fnPrintVer(val)
{
	windowOpener("/GmInHouseSetServlet?hAction=ViewSet&hConsignId="+val,"Con1","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}