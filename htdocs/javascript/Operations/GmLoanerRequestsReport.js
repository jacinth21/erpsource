var mygrid;
var esclFlColInd;
var esclFlHidden;
var esclFamEsclLogInd;
//click load button the report will fetch loaner details
function fnReload(inputParam,selectedstatus)
{  
	var fromDate = document.frmLoanerReqRpt.fromDt.value;
	var toDate = document.frmLoanerReqRpt.toDt.value;
	if (fromDate != '' && toDate != '')
	{ 
		var diff = dateDiff(toDate,fromDate,format); 
		
		if(diff > 0)
		{
			Error_Details(message[5061]);				
		}
		
	}	 
	
	var objStatus = document.frmLoanerReqRpt.Chk_Grpstatus;
	var str = '';
	if (objStatus)
		{
			var len = objStatus.length;			
		}
	if (objStatus)
		{	
			for (var i=0;i<len;i++)
			{
				if (objStatus[i].checked == true)
				{				
					str += objStatus[i].value + ',';					
				}
			}
			document.frmLoanerReqRpt.hStatus.value = str.substr(0,str.length-1);
		}
	document.frmLoanerReqRpt.action='/gmOprLoanerReqRpt.do?method=loadLoanerRequest&strOpt=Reload';
	if(inputParam != undefined && inputParam != null && inputParam != ''){
	document.frmLoanerReqRpt.hinputparam.value = inputParam;
	}
	if(selectedstatus != undefined && selectedstatus != null && selectedstatus != ''){
	document.all.hselectedstatus.value = selectedstatus;
	}
		
	if (ErrorCount > 0)
	 {
		Error_Show();
		Error_Clear();
		return false;
	}
	fnStartProgress('Y');
	document.frmLoanerReqRpt.submit();
	return false;
}

//Marks the checkboxes as 'checked'
function fnCheckSelections(val,objgrp)
{	
	var valobj = val.split(",");
	var sval = '';
	var arrlen = objgrp.length;
	if (arrlen > 0)
	{
		for (var j=0;j< valobj.length;j++ )
		{
			sval = valobj[j];
			for (var i=0;i< arrlen;i++ )
			{
				if (objgrp[i].value == sval)
				{
					objgrp[i].checked = true;
					break;
				}
			}
		}
	}
	else
	{
		objgrp.checked = true;
	}
}



function fnGo()
{
	document.frmLoanerReqRpt.submit();
}
function fnSelectPart(val,prdreqid,status)
{
  document.frmLoanerReqRpt.hId.value = val ;
  document.frmLoanerReqRpt.hPrdReqId.value = prdreqid ;
  document.frmLoanerReqRpt.hStatus.value = status;
}

function fnSubmit()
{
	 var action = document.frmLoanerReqRpt.reqAction.value;
	 var parentReqId =document.frmLoanerReqRpt.hPrdReqId.value;
	 var requestId=document.frmLoanerReqRpt.hId.value; 
	 var accessFl = document.frmLoanerReqRpt.hAccessFl.value;
     
      if(action == '0')
    { 
      Error_Details(message[406]); 
    }     
        
     if(requestId == '')
    { 
      Error_Details(message[407]); 
    } else
    {
    	
   	 	var statusid =	 document.frmLoanerReqRpt.hStatus.value ;
    		if (statusid == '30' || statusid == '40')
    	    {
    			 Error_Details(message[2004]); 
    	    }
    }
    
    if  (action == '108700' ) 
    {  
     	document.frmLoanerReqRpt.action ="/GmCommonCancelServlet?hCancelType=VDIHLN";
		document.frmLoanerReqRpt.hTxnId.value =  requestId;		
		document.frmLoanerReqRpt.hAction.value = 'Load';       
    }
	if (action == '108701')
	{
			document.frmLoanerReqRpt.action ="/gmOprLoanerReqEdit.do?strOpt=load&requestId="+parentReqId+"&txnType=4127";	
	}
    
	if (ErrorCount > 0)
		 {
			Error_Show();
			Error_Clear();
			return false;
		}
		
	document.frmLoanerReqRpt.Btn_Submit.disabled=true;
	fnStartProgress('Y');
	document.frmLoanerReqRpt.submit();
	return false;
}


function fnSetValues()
{
	document.frmLoanerReqRpt.dtType.value=strdtType;
}

function fnOnLoad()
{

	fnSetValues();
	var statsids = document.frmLoanerReqRpt.hStatus.value;
	if (statsids != '')
	{
		objgrp = document.frmLoanerReqRpt.Chk_Grpstatus;
		fnCheckSelections(statsids,objgrp);
	}
	mygrid = initGridData('dataGridDiv',gridXmlData);  
	esclFlColInd = mygrid.getColIndexById("EsclFl");
	esclFamEsclLogInd = mygrid.getColIndexById("FamEsclLog");
	esclFlHidden = mygrid.getColIndexById("EsclFlHidden");
	mygrid.enableTooltips("false,true,false,false,false,false,false,false,false,false,false,false,false,false");
	mygrid.attachEvent("onMouseOver",function(id,ind)
	{
		if (ind==1){
			mygrid.cells(id,ind).setAttribute("title","Log Details");
			mygrid.setCellTextStyle(id,ind,"cursor:hand;"); 
		}
		else
		{
			return false;
		}
		return true;
	});

}

function initGridData(divRef,gridData)
{
	var gObj;
	gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);
	//gObj.enableTooltips("true,true,true,true,true,true,true,true,true,true,true,true,true,true,true");
	gObj.attachHeader('#rspan,,,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter');
	gObj.enableMultiline(true);
	gObj.init();	
	gObj.loadXMLString(gridData);
	//gObj.attachEvent("onRowSelect", doOnRowSelect);
	return gObj;	
}

function fnDownloadXLS(){
	mygrid.setColumnHidden(1, true);
	mygrid.setColumnHidden(2, true);
	mygrid.setColumnHidden(3, false);
	mygrid.toExcel('/phpapp/excel/generate.php');
	mygrid.setColumnHidden(1, false);
	mygrid.setColumnHidden(2, false);
	mygrid.setColumnHidden(3, true);
	
}
//escalation flaged by C/S
function fnUpdateEscalation(reqId,id,name,status){
	if(strEsclFl == 'Y' && SwitchUserFl != 'Y'){ //PMT-44477
		var comments = 'Flagged for Escalation';
		var logIcon = mygrid.cellById(id,esclFlColInd).getValue();
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmOprLoanerReqRpt.do?method=updateLoanerEscalation&hType=26240693&requestId='+reqId+'&hEscFl=Y&comments='+comments+'&reqStatus='+status+'&randomId='+Math.random());
		dhtmlxAjax.get(ajaxUrl,function(loader){
				var escFl =loader.xmlDoc.responseText;
			});
			mygrid.cellById(id, esclFlColInd).setValue('<a href="#" onClick=fnRemoveEscalation('+reqId+','+id+',"update","Open")><img alt="Escalation Flag" src="/img/escalation_flag_red.jpg" height="15" width="15" border="0"></a>');
			//PMT-45519
			mygrid.cellById(id, esclFlHidden).setValue('Y');
			mygrid.cellById(id, esclFamEsclLogInd).setValue('<a href="#" onClick=fnOpenReqLog('+reqId+')><img alt="Comments Log" src="/images/phone-icon_ans.gif" height="15" width="15" border="0">');
	}
	
}
//remove escalation flag by FAM
function fnRemoveEscalation(reqId,id,name,status){
	if(strRemEscFl == 'Y' && SwitchUserFl != 'Y')  { //PMT-44477
		var comments = 'Escalation unflagged';
		var logIcon = mygrid.cellById(id,esclFlColInd).getValue();
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmOprLoanerReqRpt.do?method=updateLoanerEscalation&hType=26240693&requestId='+reqId+'&hEscFl=N&comments='+comments+'&randomId='+Math.random());
		 dhtmlxAjax.get(ajaxUrl,function(loader){
				var escFl =loader.xmlDoc.responseText;
			});
			mygrid.cellById(id, esclFlColInd).setValue('<a href="#" onClick=fnUpdateEscalation('+reqId+','+id+',"remove","Open")><img alt="Escalation Flag" src="/img/escalation_flag_grey.jpg" height="15" width="15" border="0"></a>');
			//PMT-45519
			mygrid.cellById(id, esclFlHidden).setValue('N');
			mygrid.cellById(id, esclFamEsclLogInd).setValue('<a href="#" onClick=fnOpenReqLog('+reqId+')><img alt="Comments Log" src="/images/phone-icon_ans.gif" height="15" width="15" border="0">');
	}
}
//fetch log details
function fnOpenReqLog(reqId) {
	windowOpener("/GmCommonLogServlet?hType=26240693&hID="           
            +reqId+"&beanKeyId=LOANESCL&hHideComment=Y&hAlignSubmit=L","RequestLog","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
}

