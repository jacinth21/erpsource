
function fnSubmit()
{

	Error_Clear();
	var strError = '';
	var strErrorLess = '';
	var hcnt = parseInt(document.frmVendor.hCnt.value);
	document.frmVendor.hAction.value = 'Save';
	document.frmVendor.hCnt.value = hcnt;
	
	var retType = document.frmVendor.hRetType.value;
	
	var CNStr = '';
	var QNStr = '';
	var QNSterileStr='';
	var QNNonSterileStr='';
    var PNStr = '';
    var IAStr = '';
    var IHStr = '';
    var FGStr = '';
    
    var CNobj;
    var QNobj;
    var PNobj;
    var IAobj;
    var IHobj;
    var FGobj;
    
    var CNval = '';
    var QNval = '';
    var PNval = '';
    var IAval = '';
    var IHval = '';
    var FGval = '';
    

	var strNumErrMsg = '';
    
    var cnt = 0;         
    for(var j=0;j<hcnt;j++)
    {
   
		pnumobj = eval("document.frmVendor.hPartNum"+j);
		cnumobj = eval("document.frmVendor.hCNum"+j);
		qtyobj = eval("document.frmVendor.hQty"+j);
		itype = eval("document.frmVendor.hIType"+j);
		sterileflObj=eval("document.frmVendor.hISterileFlag"+j);
		 if(sterileflObj != undefined && sterileflObj != null){
		   sterileflVal=sterileflObj.value;
		 }
		if (itype.value == '50301')
		{
			break;
		}

		cnt = 0;
		if (pnumobj)
		{
			pnumval = pnumobj.value;
			cnumval = cnumobj.value;

	        setQtyObj = eval("document.frmVendor.hSetQty"+j);
	        if(retType == '3308'){
	
				IAobj = eval("document.frmVendor.Txt_IA"+j);
				QNobj = eval("document.frmVendor.Txt_QN"+j);
				IHobj = eval("document.frmVendor.Txt_IH"+j);
				
		        IHval = IHobj.value;
		        QNval = QNobj.value;
		        IAval = IAobj.value;
		        
	        }else if(retType == '3309'){
	 
				QNobj = eval("document.frmVendor.Txt_QN"+j);
				PNobj = eval("document.frmVendor.Txt_PN"+j);
				FGobj = eval("document.frmVendor.Txt_FG"+j);
				
		        QNval = QNobj.value;
		        PNval = PNobj.value;
                FGval = FGobj.value;
	        }else{

				CNobj = eval("document.frmVendor.Txt_CN"+j);
				QNobj = eval("document.frmVendor.Txt_QN"+j);
				PNobj = eval("document.frmVendor.Txt_PN"+j);
				FGobj = eval("document.frmVendor.Txt_FG"+j);
				
		        CNval = CNobj.value;
		        QNval = QNobj.value;
		        PNval = PNobj.value;
                FGval = FGobj.value;
	        }


            qtyDiff = 0; 
	         		
			if(CNval != '' && CNval!='0')
			{
      
				CNqty = parseFloat(CNval);
				if(isNaN(CNqty)){
					strNumErrMsg += "," + pnumval + "-(RCN)"  ;
				}
				setQty = parseInt(setQtyObj.value);
				if (CNqty > setQty)
				 {
				   qtyDiff = CNqty - setQty;
				   CNqty  = CNqty - qtyDiff;
				 }
				CNStr = CNStr + pnumval +'^'+cnumval+'^'+CNqty+'|';
				cnt = cnt + CNqty;
			}
		if(QNval != '' && QNval != '0')
			{
		
				QNqty = parseFloat(QNval);
				if(isNaN(QNqty)){
					strNumErrMsg += "," + pnumval  + "-(RHQN)"  ;
				}
			/*Split by two parts sterile and non sterile*/
			   if(splitfl=='Y'){
					if(sterileflVal=='Y'){
						QNSterileStr = QNSterileStr + pnumval +'^'+cnumval+'^'+QNqty+'|';
					}
					else{
						QNNonSterileStr = QNNonSterileStr + pnumval +'^'+cnumval+'^'+QNqty+'|';
					}
			  }
		 
			  else{

				QNNonSterileStr = QNNonSterileStr + pnumval +'^'+cnumval+'^'+QNqty+'|';
			  }
			    cnt = cnt + QNqty;
			}
			
			
			if(PNval != '' && PNval!= '0')
			{
				
				PNqty = parseFloat(PNval);
				if(isNaN(PNqty)){
					strNumErrMsg += "," + pnumval  + "-(RHPN)"  ;
				}
				PNqty = PNqty + qtyDiff; 
				PNStr = PNStr + pnumval +'^'+cnumval+'^'+PNqty+'|';
				cnt = cnt + PNqty;
			}
			if(IAval != '' && IAval!= '0')
			{
		
				IAqty = parseFloat(IAval);
				if(isNaN(IAqty)){
					strNumErrMsg += "," + pnumval + "-(RHIA)"  ;
				}
				IAqty = IAqty + qtyDiff; 
				IAStr = IAStr + pnumval +'^'+cnumval+'^'+ IAqty+'|';
				cnt = cnt + IAqty;
			}
			if(IHval != '' && IHval!= '0')
			{
		
				IHqty = parseFloat(IHval);
				if(isNaN(IHqty)){
					strNumErrMsg += "," + pnumval +  "-(RHIH)"  ;
				}
				IHqty = IHqty + qtyDiff; 
				IHStr = IHStr + pnumval +'^'+cnumval+'^'+IHqty+'|';
				cnt = cnt + IHqty;
			}
			//PC-4788-New Inhouse Transaction - Returns to Finished Goods
			if(FGval != '' && FGval!= '0')
			{
				
				FGqty = parseInt(FGval);
				if(isNaN(FGqty)){
					strNumErrMsg += "," + pnumval  + "-(RHFG)"  ;
				}
				FGqty = FGqty + qtyDiff; 
				FGStr = FGStr + pnumval +'^'+cnumval+'^'+FGqty+'|';
				cnt = cnt + FGqty;
			}

		} // End of IF
			if (cnt > qtyobj.value)
			{
				strError  = strError + ","+ pnumval+"/"+cnumval;
			}
			else if (cnt < qtyobj.value)
			{
				strErrorLess = strErrorLess + ","+ pnumval+"/"+cnumval;
			}
	}// End of FOR
	
	var request_id = fnGetRequestID();
	
	if(strNumErrMsg != '')
	{		
		Error_Details(Error_Details_Trans(message_operations[317],strNumErrMsg.substr(1,strNumErrMsg.length)));
	}
	if(request_id == "")
	{
		Error_Details(message_operations[318]);
	}
	else if (request_id == "New")
	{
	   request_id = "";
	}
	
	if (strError != '')
	{
		Error_Details(Error_Details_Trans(message_operations[319],strError));
	}
	
	
	
	if (strErrorLess != '')
	{
		Error_Details(Error_Details_Trans(message_operations[320],strErrorLess));
	}
			
	if (ErrorCount > 0)
	{
		Error_Show();
		return false;
	}else{

		document.frmVendor.hRequestID.value = request_id;

		document.frmVendor.hCNStr.value = CNStr;
		document.frmVendor.hQNStr.value = QNNonSterileStr;
		//set to form for sterile
		document.frmVendor.hQNSterileStr.value = QNSterileStr;
		document.frmVendor.hPNStr.value = PNStr;
		document.frmVendor.hIAStr.value = IAStr;
		document.frmVendor.hIHStr.value = IHStr;
		document.frmVendor.hFGStr.value = FGStr; //set to form for FG string - PC-4788
		//document.frmVendor.hIHStr.value = IHStr; QN
		document.frmVendor.hAction.value = 'Save';
	 	fnStartProgress('Y');
	 	 
		// if CN, PN, IH has value then need expiry date validation , otherwise need skip validation 
		if(CNStr == '') {
			document.frmVendor.CNRTNTYPE.value = 'RETURN';  
		}else{
			document.frmVendor.CNRTNTYPE.value = '';
		}
		if(PNStr == '') {
			document.frmVendor.PNRTNTYPE.value = 'RETURN';  
		}else{
			document.frmVendor.PNRTNTYPE.value = '';
		}
		if(FGStr == '') { //PC-4788-New Inhouse Transaction - Returns to Finished Goods
			document.frmVendor.PNRTNTYPE.value = 'RETURN';  
		}else{
			document.frmVendor.PNRTNTYPE.value = '';
		}
		
	  	document.frmVendor.submit();
  	}
}

function fnReload()
{
  document.frmVendor.hAction.value = "Reload";
  document.frmVendor.submit();
}

function fnAdd()
{
  var hraids =   document.frmVendor.Txt_Returns.value;
  hraids = hraids + ","+ document.frmVendor.hId.value;
  document.frmVendor.hRAIDs.value = hraids;   
  document.frmVendor.hAction.value = "AddRA";
  document.frmVendor.submit();
}

function fnRollback()
{
		document.frmVendor.hTxnId.value = document.frmVendor.hRAId.value;
		document.frmVendor.action ="/GmCommonCancelServlet";
		document.frmVendor.hAction.value = "Load";		
		document.frmVendor.hCancelType.value = 'CRBTN'
		document.frmVendor.submit();
}

function fnDisplayLog()
{
	var hcnt = parseInt(document.frmVendor.hCnt.value);
	objLog = document.getElementById('log');
	objLog.style.display = "none";
	objLog.style.visibility = "hidden";
	for(var j=0;j<hcnt;j++)
    {
		QNobj = eval("document.frmVendor.Txt_QN"+j);
		QNval = QNobj.value;
		if(QNval > 0)
		{
			objLog.style.display = "block";
			objLog.style.visibility = "visible";
		}
	}
}

function fnOpenTag(strPnum,strRaId)
{
	windowOpener("/gmTag.do?strOpt=reload&pnum="+encodeURIComponent(strPnum)+"&refID="+strRaId+"&refType=51001","PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=920,height=600");
}
