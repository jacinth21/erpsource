 var partNos='';
 
function fnSubmit()
{
	if (document.frmTerritory.Chk_ActiveFl.checked)
	{
		document.frmTerritory.Chk_ActiveFl.value = 'Y';
	}
	document.frmTerritory.submit();
}

function fnLoadList(val)
{
if (val == '3301')
	{
		// document.all.list.innerHTML = document.all.Dist.innerHTML;
		document.all.cont.innerHTML = document.all.Set.innerHTML;
		var obj = document.frmTerritory.Cbo_Reason;
		for (var i=0;i<obj.length ;i++ )
		{
			val = obj.options[i].value;
			if (val == "3313")
			{
				obj.options[i].selected = true;
				break;
			}
		}
	}
	else if (val == '3302')
	{
//		document.all.list.innerHTML = document.all.Dist.innerHTML;
		if (document.frmTerritory.hAction.value != 'PopOrd')
		{
			document.all.cont.innerHTML = document.all.PartNums.innerHTML;
		}
	}

}

function fnPrint(val)
{
	windowOpener("/GmPrintCreditServlet?hAction=PRINT&hRAID="+val,"Con1","resizable=yes,scrollbars=yes,top=250,left=200,width=800,height=300");  
}

function fnInitiate()
{
	var objType = document.frmTerritory.Cbo_Type;
	 var objReason = document.frmTerritory.Cbo_Reason;
 	//objType.value: 3301 - Consignment - Sets ; 3302- Consignment - Items  and objReason.value:3313-Consignment Return : 3253 -- QA Evaluation
 	
 	if ((objType.value == '3301'|| objType.value == '3302')&&(objReason.value !='3313' || objReason.value !='3311') || objReason.value !='3253') ){
	Error_Details(message[99]);
	}

	if(fnValidateInitiateInput())
	 {	   
		fnMakeLoadString();
		if(fnMakeInitiateString())
		  {		
		      if(partNos != '')
	      	{
		        if(!window.confirm(Error_Details_Trans(message_operations[338],partNos)))
		           {
		               return false;
		           }
	    	 }   
		  			
			document.frmTerritory.hAction.value = "Initiate";
			document.frmTerritory.submit();
		}
	 }	
}

function fnReload()
{
	if(fnValidateLoadInput())
	 {
	    fnMakeLoadString();	
        document.frmTerritory.hAction.value = "Reload";
        fnStartProgress('Y');
		document.frmTerritory.submit();
	 }	
}

function fnValidateInitiateInput()
{	
	partNos='';
	 var len =   document.frmTerritory.length.value;
	 var setNoLen =   document.frmTerritory.hRowCnt.value;
     var setNumTxtFeild;
     var setNum;
     var setCnt = 0;
     
for(var count = 0; count < setNoLen ; count++)
   {
    setNumTxtFeild = eval("document.frmTerritory.Txt_SetID"+count);

    if(setNumTxtFeild.value != '')
     {
       setCnt++;
       setNum = setNumTxtFeild.value;
           //   //alert( setNum +" :  " + setCnt);
     }
   }
	 
	 if(setCnt == 1)
	  {
	     document.frmTerritory.hSetNo.value = setNum;
	     ////alert(document.frmTerritory.hSetNo.value);
	   }

	// //alert(len);
	 var objDist = document.frmTerritory.Cbo_DistId;
	 
	 var pendingQty;
     var rtnQty;
     var partNo;
	 var check;
	 
	 if(objDist.value != "01")

	 // Removed code that validated excess returns from Inhouse. This is not needed as IH posting is done to a new acct PD Expense.
	// PD Expense is like scrap acct.
	 {
		 for(var count = 0; count < len ; count++)
		  {
		    pendingQty = eval("document.frmTerritory.hPendingQty"+count);
            rtnQty = eval("document.frmTerritory.Txt_RetQty"+count);
            partNo = eval("document.frmTerritory.hPartNo"+count);
            check = eval("document.frmTerritory.rad"+count)
            
           
            
            if(parseInt(rtnQty.value) > parseInt(pendingQty.value) && check.checked == true)
             {              
               partNos =  partNos+partNo.value+", ";
            //   //alert(rtnQty.value + " , " + pendingQty.value + partNos);
             }
         }   	   
	 }
	 if(objDist.value != '<%=strDistID%>')
	  {
	     Error_Details(message[96]);
	  }
fnValidateDropDn('Cbo_Reason',' Reason ');

   if (ErrorCount > 0)
	{
//		//alert("here");
			Error_Show();
			Error_Clear();
			return false;
	}	

  // //alert(" else ");

  return true;	
}

function fnValidateLoadInput()
 {
 var len =   document.frmTerritory.hRowCnt.value;
// //alert("length : "+len); 
 	 var setNumTxtFeild;
	 var setQtyTxtFeild;
 	 var setNumTxtFeildOuter;
	 var setNumTxtFeildInner;
	 var duplicateFlg = false;
 
 
 var objType = document.frmTerritory.Cbo_Type;
 var objReason = document.frmTerritory.Cbo_Reason;
 
	if (objType.value == '0')
	{
		Error_Details(message[90]);
	}

 var objDist = document.frmTerritory.Cbo_DistId;
	if (objDist.value == '0')
	{
		Error_Details(message[92]);
	}	
	
 var objEmp = document.frmTerritory.cbo_EmpName;
	if (objDist.value == '01' && objEmp.value == '0')
	{
		Error_Details(message[93]);
	}
	
 var objPartNums  = document.frmTerritory.Txt_PartNums;
  if(objType.value == 3302 && objPartNums.value == '')	
   {
     Error_Details(message[94]);
   }
 
 var chkNull = true;
 
  if(objType.value == 3301)	
   {
	if(objPartNums.value != '')
 	{
 		chkNull = false;
 	}
 	
 	if(chkNull)
 	{	
	 	for(var count = 0; count < len ; count++)
	   	{
	    setNumTxtFeild = eval("document.frmTerritory.Txt_SetID"+count);
	    ////alert(" setNumTxtFeild "+setNumTxtFeild+", Txt_SetID"+count); 
	    if(setNumTxtFeild.value != '')
	     {
	        chkNull = false;
	     }	   
	    } 
 
     if(chkNull)
     {
         Error_Details(message[95]);
     }	   	   
   }
 }
 
 
 for(var count = 0; count < len ; count++)
   {
    setNumTxtFeild = eval("document.frmTerritory.Txt_SetID"+count);
    setQtyTxtFeild = eval("document.frmTerritory.Txt_Qty"+count);
    
   // //alert(setNumTxtFeild + ", "+setQtyTxtFeild);
    
    if(setNumTxtFeild.value != '' && setQtyTxtFeild.value == '')
     {
       setQtyTxtFeild.value = "1";
     }
   }

   for(var count = 0; count < len ; count++)
   {
   var countSet = 0;
	    setNumTxtFeildOuter = eval("document.frmTerritory.Txt_SetID"+count);
	    	  
	    if(setNumTxtFeildOuter.value != '')
	     {
	    //  //alert(setNumTxtFeildOuter.value);
	       for(var countInner = 0; countInner < len ; countInner++)
	  	 	{  
	          setNumTxtFeildInner = eval("document.frmTerritory.Txt_SetID"+countInner);
	          
		          if(setNumTxtFeildOuter.value ==  setNumTxtFeildInner.value)	            
		          {
		         //  //alert(setNumTxtFeildOuter.value + ", " + setNumTxtFeildInner.value);
		            countSet++;
		            if(countSet == 2)
		              {
			            duplicateFlg = true;
			            break;  
			          }  
		          }
	        }       
	     }     
	     
	     if(duplicateFlg)
	      {
	        Error_Details(message[97]);
	        break;
	      }
    }
   
 if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}    
 return true;	 
 }
 
function fnAddToCart()
{
	document.frmTerritory.hAction.value = "GoCart";
	document.frmTerritory.submit();
}

function fnRemoveItem(val)
{
	document.frmTerritory.hDelPartNum.value = val;
	document.frmTerritory.hAction.value = "RemoveCart";
	document.frmTerritory.submit();	
}

function fnUpdateCart()
{
	document.frmTerritory.hAction.value = "UpdateCart";
	document.frmTerritory.submit();	
}

function fnLoad()
{
	document.frmTerritory.hRowCnt.value = '<%=defaultSetCount%>';
	// //alert("Row Count : "+document.frmTerritory.hRowCnt.value);
	if (document.frmTerritory.hAction.value == 'PopOrd')
	{
		fnLoadList('<%=strType%>');
	}
	document.frmTerritory.Txt_ExpDate.value = '<%=strEdate%>';
	fnCallEmp(document.frmTerritory.hId);
	fnParseFields();
	fnChangeDisplay();
}

function fnPlaceOrder()
{
	var objType = document.frmTerritory.Cbo_Type;
	if (objType.value == '0')
	{
		Error_Details(message[90]);
	}

	var objReason = document.frmTerritory.Cbo_Reason;
	if (objReason.value == '0')
	{
		Error_Details(message[91]);
	}
	
	var objDist = document.frmTerritory.Cbo_Id;
	if (objDist.value == '0')
	{
		Error_Details(message[92]);
	}

	var objEmp = document.frmTerritory.cbo_EmpName;
	if (objDist.value == '01' && objEmp.value == '0')
	{
		Error_Details(message[93]);
	}
	
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}

	document.frmTerritory.hAction.value = "PlaceOrder";
	document.frmTerritory.submit();	
}

function fnSelectAll(obj)
{
	var len = document.frmTerritory.length.value;
    var objId;
	for(var n=0;n<len;n++)
	{
		objId = eval("document.frmTerritory.rad"+n); // The checkboxes would be like rad0, rad1 and so on
			if(objId)
			{			
				if(obj.checked)
				{
					objId.checked = true;
				}else
				{
					objId.checked = false;
				}
			}
	 }		
}


function fnOnCheckRad(count)
{
    var objId;
    var objTxtRetQty;
	objId = eval("document.frmTerritory.Chk_SelectAll");
	objId.checked = false;
	objTxtRetQty = eval("document.frmTerritory.Txt_RetQty"+count);
	objTxtRetQty.value="0";
}

function fnParseFields()
{
 var setNums = '<%=strSetNums%>';
 var setQty = '<%=strSetQty%>';


 
 if(setNums != '')
 {
 var arrSetNums = setNums.split(",");
 var arrSetQty = setQty.split(",");
 
 var setNumTxtFeild;
 var setQtyTxtFeild;
 
 //document.frmTerritory.hRowCnt.value = arrSetNums.length;
 
 for(var i=0; i< arrSetNums.length -1;i++)
  {
    setNumTxtFeild = eval("document.frmTerritory.Txt_SetID"+i);
    setQtyTxtFeild = eval("document.frmTerritory.Txt_Qty"+i);
    setNumTxtFeild.value = arrSetNums[i];
	setQtyTxtFeild.value = arrSetQty[i];   
	   
	////alert(setNumTxtFeild.value + ", " + setQtyTxtFeild.value);
  } 
 }
} 

function fnChangeDisplay()
 {
  
  var type = document.frmTerritory.Cbo_Type.value;
  var rowCnt = document.frmTerritory.hRowCnt.value; 
  var setNumTxtFeild;
  var setQtyTxtFeild;
 
 if(type == "3302")
  { 
	 for(var i=0; i< rowCnt;i++)
	  {
	    setNumTxtFeild = eval("document.frmTerritory.Txt_SetID"+i);
	    setQtyTxtFeild = eval("document.frmTerritory.Txt_Qty"+i);
	   
//	    //alert(setNumTxtFeild +" , "+ setQtyTxtFeild);
	   // if(setNumTxtFeild != undefined)
	     {  
		    setNumTxtFeild.disabled = true;
		    setQtyTxtFeild.disabled = true;        
		 }   
	  }
	  document.frmTerritory.Btn_AddRow.disabled = true;
  }
   else if(type == "3301")
   {   
     for(var i=0; i< rowCnt;i++)
	  {
	    setNumTxtFeild = eval("document.frmTerritory.Txt_SetID"+i);
	    setQtyTxtFeild = eval("document.frmTerritory.Txt_Qty"+i);
	   
	   // //alert(setNumTxtFeild +" , "+ setQtyTxtFeild);
	    //   if(setNumTxtFeild != undefined)
	     	{  	
		    	setNumTxtFeild.disabled = false;
		    	setQtyTxtFeild.disabled = false;           
		    }
     }
     document.frmTerritory.Btn_AddRow.disabled = false;
  }
  
 }
 
function fnCallEmp()
{
	var objEmp = document.frmTerritory.cbo_EmpName;
	var obj = document.frmTerritory.Cbo_DistId;
	if (obj.value == '01')
	{
		objEmp.disabled = false;
		objEmp.focus();
	}else
	{
		objEmp.disabled = true;
		objEmp.options[0].selected = true;
	}
}

function fnViewPartActual(val)
{	
	document.frmTerritory.Cbo_DistId.value = '<%=strDistID%>';	
	if(document.frmTerritory.Cbo_DistId.value != '01') {
			windowOpener("/GmSalesConsignSearchServlet?hAction=Reload&hOpt=BYPARTDETAIL&Cbo_DistId='<%=strDistID%>'&hPartID="+val,"Search","resizable=yes,scrollbars=yes,top=340,left=420,width=600,height=435");
	}
}

function fnOpenPart()
{
	var varAccId = '<%=strAccId%>';
	var varPartNum = document.frmTerritory.Txt_PartNums.value;
	var varCount=0;
	windowOpener("/GmSearchServlet?hAction=SearchPart&Txt_PartNum="+encodeURIComponent(varPartNum)+"&hCount="+varCount+"&Txt_AccId="+varAccId,"Search","resizable=yes,scrollbars=yes,top=340,left=420,width=600,height=435");
}

function fnSetLookup()
{
   windowOpener("/GmSetReportServlet?hOpt=SETRPT","Search","resizable=yes,scrollbars=yes,top=340,left=420,width=600,height=435");
}

