//This Function used to show the grid values while loading the page
function fnOnPageLoad() {

	if (objGridData != '') {
		gridObj = initGridData('acctivityRpt', objGridData);
	}
}

//This Function used to Initiate the grid
function initGridData(divRef, gridData) {
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);
	gObj.enableTooltips("true,true,true,true,true,true,true,true,true");
	gObj.attachHeader('#select_filter,#select_filter,#text_filter,#select_filter,#text_filter,#text_filter,#text_filter,#text_filter,#select_filter');
	gObj.init();
	gObj.enablePaging(true, 100, 10, "pagingArea", true);
	gObj.setPagingSkin("bricks");
	gObj.loadXMLString(gridData);
	return gObj;
}
function fnLoad(){
	// check the date filed. if more than 2 years then auto check the History flag.
	CommonDateValidation(document.frmfieldSalesWarehouse.fromDt,date_format,'<b>From - </b>'+ message[611]);
	CommonDateValidation(document.frmfieldSalesWarehouse.toDt,date_format,'<b>To - </b>'+ message[611]);
	var frmDate = document.frmfieldSalesWarehouse.fromDt.value;
	var toDate = document.frmfieldSalesWarehouse.toDt.value;
	var selectedHistoryFl = document.frmfieldSalesWarehouse.historicData.checked; 

	var max_Date = dateDiff(frmDate, toDate, date_format);
	
	if (frmDate == '' && toDate != '') {
		Error_Details(message[10576]);
	}
	
	if (frmDate != '' && toDate == '') {
		Error_Details(message[10577]);
	}
	
	if(selectedHistoryFl){
		if (max_Date >= 366) { // 1 year 366 days.
			Error_Details(message[10578]);
		}
	}	
	if(frmDate != '' && toDate != ''){
		if(max_Date < 0){
			Error_Details(message[10579]);
		}
	}
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	} 
	fnStartProgress("Y");
	document.frmfieldSalesWarehouse.strOpt.value = 'QtyLog';
	document.frmfieldSalesWarehouse.submit();
}
//This function will call the press the enter button.
function fnEnter(){
	if (event.keyCode == 13){ 
		fnLoad();
	}
}

//This function will call the excel download
function fnExport()
{	
	gridObj.toExcel('/phpapp/excel/generate.php');
}
// This function used to close the popup window.
function fnFsLogClose(){
	window.close();
}
// This function used to open the Transaction reports details.
function fnOpenTxnDetails(txnId, txnType, type) {
	if (txnType == '102900' && (type == '102907' || type == '102909')) {
		// 102900/102907 - Consignment, 102909 - Transfer
		fnOpenConsignDtls(txnId);
	} else if (txnType == '102901' && (type == '102908' || type == '102909')) {
		// 102901/102908 - Return, 102909 - Transfer
		fnOpenReturnDtls(txnId);
	} else if (type == '4000337') {
		// Bill only Consignments
		fnOpenOrderDtls(txnId);
	}
}
// This function used to open the Consignment details.
function fnOpenConsignDtls(txnId) {
	windowOpener("/GmConsignSetServlet?hAction=PrintVersion&hId=" + txnId,
			"Consignment",
			"resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=700");
}
// This function used to open the Returns details.
function fnOpenReturnDtls(txnId) {
	windowOpener("/GmPrintCreditServlet?hAction=PRINT&hRAID=" + txnId,
			"Returns",
			"resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=700");
}
// This function used to open the Order details.
function fnOpenOrderDtls(txnId) {
	windowOpener("/GmEditOrderServlet?hMode=PrintPrice&hOrdId=" + txnId,
			"Orders",
			"resizable=yes,scrollbars=yes,top=250,left=250,width=820,height=700");
}