function fnReload(inputParam,selectedstatus)
{  
	var fromDate = document.frmLoaner.fromDt.value;
	var toDate = document.frmLoaner.toDt.value;
	
	if (fromDate != '' && toDate != '')
	{ 
		var diff = dateDiff(toDate,fromDate,format); 
		
		if(diff > 0)
		{
			Error_Details(message[5061]);				
		}
		
	}	 
	
	var objStatus = document.frmLoaner.Chk_GrpStatus;
	var str = '';
	if (objStatus)
		{
			var len = objStatus.length;			
		}
	if (objStatus)
		{	
			for (var i=0;i<len;i++)
			{
				if (objStatus[i].checked == true)
				{				
					str += objStatus[i].value + ',';					
				}
			}
			document.frmLoaner.hStatus.value = str.substr(0,str.length-1);
		}
	document.frmLoaner.action='/GmLoanerPartRepServlet';
	document.frmLoaner.hAction.value = "Reload";
	document.frmLoaner.hOpt.value = "Requests";
	if(inputParam != undefined && inputParam != null && inputParam != ''){
	document.frmLoaner.hinputparam.value = inputParam;
	}
	if(selectedstatus != undefined && selectedstatus != null && selectedstatus != ''){
	document.all.hselectedstatus.value = selectedstatus;
	}
	if(strInHouseType == "InhouseLoanerRequests" ){
		document.frmLoaner.hOpt.value = "InhouseLoanerRequests";
	}
	
	if (ErrorCount > 0)
	 {
		Error_Show();
		Error_Clear();
		return false;
	}
	
	fnStartProgress('Y');
	document.frmLoaner.submit();
	return false;
}

function fnGo()
{
	//document.frmLoaner.hAction.value = "LoanerByDist";
	document.frmLoaner.submit();
}
function fnSelectPart(val,prdreqid,status)
{
  //  requestId = obj.value ;
  document.frmLoaner.hId.value = val ;
  document.frmLoaner.hPrdReqId.value = prdreqid ;
  document.frmLoaner.hStatus.value = status;
}

function fnSubmit()
{
	 var action = document.frmLoaner.Cbo_Action.value;
	 var parentReqId =document.frmLoaner.hPrdReqId.value;
	 var requestId=document.frmLoaner.hId.value; 
	 var accessFl = document.frmLoaner.hAccessFl.value;
     
      if(action == '0')
    { 
      Error_Details(message[406]); 
    }     
        
     if(requestId == '')
    { 
      Error_Details(message[407]); 
    } else
    {
    	//var status = eval("document.frmLoaner.hstatus_"+requestId).value;
    	
   	 	var statusid =	 document.frmLoaner.hStatus.value ;
    		if (statusid == '30' || statusid == '40')
    	    {
    			 Error_Details(message[2004]); 
    	    }
    }
    
    if (accessFl != 'Y' && action != '0' && strInHouseType=='InhouseLoanerRequests')
	{
			Error_Details(message[5062]);
	}
    
    if  (action == 'RVOD' ) 
    {  
     	document.frmLoaner.action ="/GmCommonCancelServlet?hCancelType=VDIHLN";
		document.frmLoaner.hTxnId.value =  requestId;		
		document.frmLoaner.hAction.value = 'Load';       
    }
	if (action == 'EDIT')
	{
		if(strInHouseType=='InhouseLoanerRequests'){
		    document.frmLoaner.action ="/gmOprLoanerReqEdit.do?strOpt=load&requestId="+parentReqId+"&txnType=4119";
		}else{
			document.frmLoaner.action ="/gmOprLoanerReqEdit.do?strOpt=load&requestId="+parentReqId+"&txnType=4127";	
		}
	}
    
	if (ErrorCount > 0)
		 {
			Error_Show();
			Error_Clear();
			return false;
		}
		
	document.frmLoaner.Btn_Submit.disabled=true;
	fnStartProgress('Y');
	document.frmLoaner.submit();
	return false;
}


function fnSetValues()
{
	document.frmLoaner.dtType.value=strdtType;
}

// Marks the checkboxes as 'checked'
function fnCheckSelections(val,objgrp)
{	
	var valobj = val.split(",");
	var sval = '';
	var arrlen = objgrp.length;
	
	if (arrlen > 0)
	{
		for (var j=0;j< valobj.length;j++ )
		{
			sval = valobj[j];
			for (var i=0;i< arrlen;i++ )
			{
				if (objgrp[i].value == sval)
				{
					objgrp[i].checked = true;
					break;
				}
			}
		}
	}
	else
	{
		objgrp.checked = true;
	}
}

function fnOnLoad()
{
	var inputParam = document.frmLoaner.hinputparam.value;
	if(inputParam != ''){
		fnSetValues();
		var statusParam = document.frmLoaner.hstatusparam.value;
		document.frmLoaner.hStatus.value = statusParam;
		var status = document.frmLoaner.hStatus.value;
		var selectedstatus = document.frmLoaner.hselectedstatus.value;
		if (inputParam == 'ToPick' || inputParam == 'PendingShipping' || inputParam == 'ToPutAway' || selectedstatus == 'ToPick' || selectedstatus == 'ToShip') {
			var status = '20';
		}
		if (status != '')
		{
			objgrp = document.frmLoaner.Chk_GrpStatus;
			fnCheckSelections(status,objgrp);
		}
		fnReload(inputParam,selectedstatus);
	}
	else{
	fnSetValues();
	var statsids = document.frmLoaner.hStatus.value;
	if (statsids != '')
	{
		objgrp = document.frmLoaner.Chk_GrpStatus;
		fnCheckSelections(statsids,objgrp);
	}
	mygrid = initGridData('dataGridDiv',gridObjData);  
	}

}

function initGridData(divRef,gridData)
{
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);
	gObj.enableTooltips("true,true,true,true,true,true,true,true");
	gObj.attachHeader('#rspan,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter');
	gObj.enableMultiline(true);
	gObj.init();	
	gObj.loadXMLString(gridData);	
	return gObj;	
}

function fnDownloadXLS(){
	mygrid.toExcel('/phpapp/excel/generate.php');
	
}
