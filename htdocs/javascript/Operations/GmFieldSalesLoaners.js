
function fnLoad(){ 
	if (document.frmFieldSalesLoaners.reqId.value != ""){ 
		fnValidateNumber('reqId',message_operations[404]);
	}
	if (document.frmFieldSalesLoaners.returnIn.value != ""){ 
		fnValidateNumber('returnIn',message_operations[405]);
	}
	if (ErrorCount > 0)
	 {
		Error_Show();
		Error_Clear();
		return false;
	}else{ 
		fnStartProgress('Y');
		fnReloadSales(null,'/gmFieldSalesLoaners.do?strOpt=load');
	//	document.frmFieldSalesLoaners.strOpt.value ="load";
	//	document.frmFieldSalesLoaners.submit();
	}
}

function enterPressed() {		
	if (window.event && window.event.keyCode == 13) {		
		fnLoad();	
	}
}
	document.onkeypress = enterPressed;

function fnReload (){
	document.frmFieldSalesLoaners.setName.value="";
	document.frmFieldSalesLoaners.reqId.value="";
	document.frmFieldSalesLoaners.etchId.value="";
	document.frmFieldSalesLoaners.cnId.value="";
	document.frmFieldSalesLoaners.daysOverDue.value="";
	fnDeSelectAll();
	document.all.selectedStatus[2].checked = true;
	document.frmFieldSalesLoaners.returnIn.value="";
	document.frmFieldSalesLoaners.strOpt.value ="reload";
	document.frmFieldSalesLoaners.submit();
}

function fnDeSelectAll()
{	
	objCheckStatus = document.all.selectedStatus;
	objCheckSets = document.all.selectedSets;
	objCheckDists = document.all.selectedDists;

	objStatArrLen = objCheckStatus.length;
	objSetArrLen = objCheckSets.length;
	objDistArrLen = objCheckDists.length;
	
	if (objDistArrLen == undefined ){
		objCheckDists.checked = false;
	}
	
	for(i = 0; i < objStatArrLen; i ++) 
	{	
		objCheckStatus[i].checked = false;
	}
	for(i = 0; i < objSetArrLen; i ++) 
	{	
		objCheckSets[i].checked = false;
	}
	for(i = 0; i < objDistArrLen; i ++) 
	{			
		objCheckDists[i].checked = false;
	}
}


function keyPressed(code, ctrl, shift) {
    if (code == 67 && ctrl) {      
        gridObj.setCSVDelimiter("\t");
        gridObj.copyBlockToClipboard();
    }
    
    if (code == 86 && ctrl) {
    	if(gridObj._selectionArea!=null){
			var colIndex = gridObj._selectionArea.LeftTopCol;
			var cb_columnName = '';
			
			if(colIndex!=undefined && colIndex==0){
				alert(message_operations[143]);
			}else{
				gridObj.pasteBlockFromClipboard();
			}
			gridObj._HideSelection();
		}else{
			alert(message_operations[49]);
		}
    }
	// This code is to block delete the content from the grid.
	if(code==46&&!ctrl&&!shift){
		
		if(gridObj._selectionArea!=null){
			var area=gridObj._selectionArea
			var leftTopCol=area.LeftTopCol;
			var leftTopRow=area.LeftTopRow;
			var rightBottomCol=area.RightBottomCol;
			var rightBottomRow=area.RightBottomRow;
			
			for (var i=leftTopRow; i<=rightBottomRow; i++){
				
				setRowAsModified(gridObj.getRowId(i),true);

				for (var j=leftTopCol; j<=rightBottomCol; j++){
					gridObj.cellByIndex(i,j).setValue("");
				}
			}
			gridObj._HideSelection();
		}
	}
    return true;
}