	var cnt = 0;	
function fnGo(){
	
	var fromDateObj = document.frmChargesApproval.fromDate;
	var toDateObj = document.frmChargesApproval.toDate;
	var fromDate = fromDateObj.value;
	var toDate = toDateObj.value;
	var dateDiffs = dateDiff( toDate,fromDate, format);
	
	// To check whether the date is in valid format
	if(fromDate != ""){
		CommonDateValidation(fromDateObj,format,Error_Details_Trans(message[10002],format));
	}
	if(toDate != ""){
		CommonDateValidation(toDateObj,format,Error_Details_Trans(message[10002],format));
	}
	
	if(fromDate != '' && toDate == ''){
		Error_Details(message[10034]);
	}else if(fromDate == '' && toDate != '' ){
		Error_Details(message[10035]);
	}else if (fromDate != '' && toDate != '' && dateDiffs>0){
		Error_Details(message[10036]);
	}
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	
	for (var i = 0; i < totalRecs; i++){
		obj1 = eval("document.frmChargesApproval.ID"+i);
		obj2 = eval("document.frmChargesApproval.cbo_row_stat"+i);	
		if(obj1!= undefined)
		obj1.removeNode(true);
		if(obj2!= undefined)
		obj2.removeNode(true);
	}
	
		fnStartProgress('Y');
		document.frmChargesApproval.strOpt.value="load";
		fnStartProgress('Y');
		document.frmChargesApproval.submit();
	
}

function fnSubmit(){
	var str = fnCreateInputString();
	if (str !=''){
		document.frmChargesApproval.updateString.value = str ;	
		document.frmChargesApproval.strOpt.value="update";
		fnStartProgress('Y');
		document.frmChargesApproval.submit();
	}
}

function fnLoadStatus(sel) {
	for ( var i = 0; i < totalRecs; i++) {
		obj = eval("document.frmChargesApproval.cbo_row_stat" + i);
		if(obj != undefined){	
			obj.value = sel;
			if (obj.value == "25") {
				eval("document.all.ccharge" + i).disabled = false;
				if (eval("document.all.crediteddt" + i) && eval("document.all.hCrdDate" + i)) {
					eval("document.all.crediteddt" + i).value = eval("document.all.hCrdDate" + i).value;
				}
			}else{
				eval("document.all.ccharge" + i).value = "";
				eval("document.all.ccharge" + i).disabled = true;
				if (eval("document.all.crediteddt" + i)) {
					eval("document.all.crediteddt" + i).value = "";
				}
			}
		}
	}
}

function fnCreateInputString(){
	var str ='';
	var creditDate='';
	for (var i = 0; i < totalRecs; i++){
		
		obj1 = eval("document.frmChargesApproval.ID"+i);
		obj2 = eval("document.frmChargesApproval.cbo_row_stat"+i);
		obj3 = eval("document.all.ccharge"+i);
		if (obj3 != undefined ){
			obj3.disabled = false;
		}
		obj4 = eval("document.all.crediteddt"+i);
		if (obj4 != undefined ){
			creditDate = obj4.value;
		}		
		if(obj3.value < 0){
			Error_Details(message[10037]);
			Error_Show();
			Error_Clear();
			str = '';
			return false;
		}
		/*fnValidateDropDn(obj2.name,' Status ');
		 if (ErrorCount > 0)
		{
			Error_Show();
			Error_Clear();
			str ='';
			return false;

		}
		*/
		if (obj2!= undefined && obj2.value !=0 )	{
			str +=obj1.value+","+obj2.value+","+obj3.value+","+	creditDate;		
			str +="|";
		}
	} 
	return str;
}
function fnRefillHistory(consignid){
	document.frmChargesApproval.action = '/GmInHouseSetServlet?hAction=ReloadHist&hConsignId='+consignid;
	document.frmChargesApproval.submit();
}
function fnLoadIncludeFilters(chargeType){	
	document.frmChargesApproval.chargesFor.value = chargeType;
	 
	if(chargeType == '50891'){
		document.getElementById("missingone").style.display = "block";
		document.getElementById("missingtwo").style.display = "block";
		document.getElementById("missingthree").style.display = "block";
		document.getElementById("missingfour").style.display = "block";		
		document.getElementById("normalload").style.display = "none";
		document.getElementById("missingfive").disabled = true;
		document.getElementById("missingsix").disabled = true;	
	}else{
		document.getElementById("missingone").style.display = "none";
		document.getElementById("missingtwo").style.display = "none";
		document.getElementById("missingthree").style.display = "none";
		document.getElementById("missingfour").style.display = "none";		
		document.getElementById("normalload").style.display = "block";
		document.getElementById("missingfive").disabled = false;
		document.getElementById("missingsix").disabled = false;	
	}	
}
function fnhideDiv(){
	if(document.getElementById("otherdata")){
		document.getElementById("otherdata").style.display = "none";
		document.getElementById("messagedata").style.visibility = 'visible'; 
	}
	if(document.getElementById("missingdata")){
		document.getElementById("missingdata").style.display = "none";
		document.getElementById("messagedata").style.visibility = 'visible'; 
	}
}

function fnMissingDetails(){
	var chargeType = document.frmChargesApproval.chargesFor.value;
	var distID = document.frmChargesApproval.dist.value ;
	var repId = document.frmChargesApproval.rep.value ;
	var fromDate = document.frmChargesApproval.fromDate.value ;
	var toDate = document.frmChargesApproval.toDate.value ;	
	var requestID = document.frmChargesApproval.requestID.value ;	
	var shwDist = document.frmChargesApproval.shwDist.checked ;
	var shwRep = document.frmChargesApproval.shwDirRep.checked ;
	
	if (!shwDist && !shwRep)
	{
		document.frmChargesApproval.shwDist.checked = true;
		document.frmChargesApproval.shwDirRep.checked = true;
		shwDist = document.frmChargesApproval.shwDist.checked ;
		shwRep = document.frmChargesApproval.shwDirRep.checked ;
	}
	document.frmChargesApproval.distName.value = distID;
	document.frmChargesApproval.repName.value = repId;
	
	fnStartProgress('Y');
	document.frmChargesApproval.screenType.value = "LoanerCharges";
	document.frmChargesApproval.strOpt.value = "LoadReport";
	document.frmChargesApproval.action = "/gmChargesApproval.do?FORMNAME=frmChargesApproval&shwDist=" + shwDist +"&shwDirRep="+shwRep;
	fnStartProgress('Y');
	document.frmChargesApproval.submit();	
}
 
function fnLoadCreditAmount(rowid,obj,date){
	if(date == null){ 
		date = "";
	}	
	if(obj.value == "50864"){
		eval("document.all.crediCharge"+rowid).disabled = false;
		if(eval("document.all.crediteddt"+rowid)){			
			eval("document.all.crediteddt"+rowid).value = date;
		}
	}else{
		eval("document.all.crediCharge"+rowid).value = "";
		eval("document.all.crediCharge"+rowid).disabled = true;
		if(eval("document.all.crediteddt"+rowid)){			
			eval("document.all.crediteddt"+rowid).value = "";
		}
	}
}

function fnLoadCreditAmounts(rowid, obj, date) {
	if (obj.value == "25") {
		eval("document.all.ccharge" + rowid).disabled = false;
		if (eval("document.all.crediteddt" + rowid)) {
			eval("document.all.crediteddt" + rowid).value = date;
		}
	} else {
		eval("document.all.ccharge" + rowid).value = "";
		eval("document.all.ccharge" + rowid).disabled = true;
		if (eval("document.all.crediteddt" + rowid)) {
			eval("document.all.crediteddt" + rowid).value = "";
		}
	}
}
function fnCredit(){
	var inputString = "";
	var arraySize = document.all.totalcount.value;
	arraySize++;
	var chargeId = "";
	var chargeId = "";
	var setID    = "";
	var requestID = "";
	var creditDate = "";
	var status = "";
	var creditEmount = "";
	var distID = "";
	var repID = "";
	var missQty = "";
	var chargeQty = "";
	var unitcost = "";
	var partnum = "";
	var editCharge = "";
	var invalidCharge = "";
	var ecxcessCharge = "";
	var assocrepid = "";
	for (var i=0; i<arraySize; i++){
		var chkststus = eval("document.all.chk_rowwise"+i).checked;
		if(chkststus){			
			if(eval("document.all.chargeid"+i)){
				chargeId = eval("document.all.chargeid"+i).value;
			}
			if(eval("document.all.transaid"+i)){
				transaid = eval("document.all.transaid"+i).value;
			}
			if(eval("document.all.setid"+i)){
				setID    = eval("document.all.setid"+i).value;
			}
			if(eval("document.all.requestID"+i)){
				requestID = eval("document.all.requestID"+i).value;
			}
			if(eval("document.all.crediteddt"+i)){
				creditDate   = eval("document.all.crediteddt"+i).value;
				}
			if(eval("document.all.cbo_status"+i)){
				status = eval("document.all.cbo_status"+i).value;
				}
			if(eval("document.all.reconcilecommnets"+i)){
				reconcommnets = eval("document.all.reconcilecommnets"+i).value;
				if(reconcommnets == "null"){
					reconcommnets = "0";
				}
			}
			if(eval("document.all.rowcharge"+i)){
				editCharge = eval("document.all.rowcharge"+i).value;
				}
			if(eval("document.all.crediCharge"+i)){
				creditEmount = eval("document.all.crediCharge"+i).value;
				}
			if(eval("document.all.distid"+i)){
				distID = eval("document.all.distid"+i).value;
				}
			if(eval("document.all.repid"+i)){
				repID = eval("document.all.repid"+i).value;
				}
			if(eval("document.all.missQty"+i)){
				missQty = eval("document.all.missQty"+i).value;
				}
			if(eval("document.all.chargeQty"+i)){
				chargeQty = eval("document.all.chargeQty"+i).value;
				}
			if(eval("document.all.listprice"+i)){
				unitcost = eval("document.all.listprice"+i).value;
				}
			if(eval("document.all.partnum"+i)){
				 partnum = eval("document.all.partnum"+i).value;
				}
			if(eval("document.all.assocrepid"+i)){
				assocrepid = eval("document.all.assocrepid"+i).value;
			}
			
			editCharge = chargeQty * editCharge;
			if(creditEmount > editCharge){					
				ecxcessCharge = ecxcessCharge +"<br>"+ partnum;	
			}
			var objRegExp = /(^-?\d*$)/;
			if((creditEmount < 0 ) || (!objRegExp.test(creditEmount))){
				invalidCharge = invalidCharge +"<br>"+ partnum;	
			}	
			
			
				inputString = inputString + chargeId + "^" + transaid + "^" + setID + "^" + requestID + "^" + creditDate + "^" + reconcommnets + "^" + editCharge + "^" + distID + "^" + repID + "^" + missQty + "^" + chargeQty + "^" + unitcost + "^" + partnum + "^" + status + "^" + assocrepid + "^" + creditEmount + "|";
				
		}
	}
	if (ecxcessCharge != ''){
		Error_Details(Error_Details_Trans(message[10038],ecxcessCharge));
		Error_Show();
		Error_Clear();
		return false;
	}
	if (invalidCharge != ''){
		Error_Details(Error_Details_Trans(message[10039],invalidCharge));
		Error_Show();
		Error_Clear();
		return false;
	}
	if(inputString == ''){
		Error_Details(message[10040]);
		Error_Show();
		Error_Clear();
		return false;
	}else{
		
		var shwDist = document.frmChargesApproval.shwDist.checked ;
		var shwRep = document.frmChargesApproval.shwDirRep.checked ;
		
		if (!shwDist && !shwRep)
		{
			document.frmChargesApproval.shwDist.checked = true;
			document.frmChargesApproval.shwDirRep.checked = true;
			shwDist = document.frmChargesApproval.shwDist.checked ;
			shwRep = document.frmChargesApproval.shwDirRep.checked ;
		}
		
		
		document.frmChargesApproval.inputString.value = inputString;
		document.frmChargesApproval.strOpt.value = "Credit";
		document.frmChargesApproval.screenType.value = "LoanerCharges";
		document.frmChargesApproval.action = "/gmChargesApproval.do?FORMNAME=frmChargesApproval&shwDist=" + shwDist +"&shwDirRep="+shwRep;
		document.frmChargesApproval.submit();
		document.frmChargesApproval.Btn_Credit.disabled = true;	
	}
}

function fnSetEditCharge(rowid,charge){
	var arraySize = document.all.totalcount.value;
	arraySize++;
	var chkststus = eval("document.all.chk_rowwise"+rowid).checked;
		if(chkststus){			
			cnt++;
		}else{				
			cnt--;
			document.all.selectall.checked = false;
		}				
	if(cnt == arraySize){
		document.all.selectall.checked = true;
	}	
}	
function fnSelectAll(obj){
    var objId;
    var arraySize = document.all.totalcount.value;
    arraySize++;
	for(var n=0;n<arraySize;n++){
		objId = eval("document.all.chk_rowwise"+n); // The checkboxes would be like rad0, rad1 and so on
		var charge = eval("document.all.rowcharge"+n);
			if(objId){			
				if(obj.checked){
					objId.checked = true;
					fnSetEditCharge(n,charge.value);
				}else{
					objId.checked = false;
					fnSetEditCharge(n,charge.value);
				}
			}
	 }		
}	
function fnTicket(ticketurl){
	windowOpener(ticketurl,"jiraticket","resizable=yes,scrollbars=yes,top=250,left=300,width=840,height=700");
}

function fnPrintDetails(requestid){
	windowOpener("/gmOprLoanerReqEdit.do?requestId="+requestid+"&strOpt=print&txnType=4127","LoanerRequest","resizable=yes,scrollbars=yes,top=250,left=300,width=840,height=700");
}
function fnOpenLog(requestid,type){
	windowOpener("/GmCommonLogServlet?hType="+type+"&hID="+requestid,"ReconCommnetLog","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=300");
}