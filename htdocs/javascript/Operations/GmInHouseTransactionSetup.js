var rowID;
var partNum_rowId ='';
var partDesc_rowId ='';
var whName_rowId ='';
var whQty_rowId ='';
var reqQty_rowId ='';
var whType_rowId ='';

function fnOnPageLoad()
{		
		document.all.inHousePurpose.value = '4139';  //4139-Customs
		document.all.consignmentType.value = '40022';  //40022-Inhouse COnsignment
		document.all.consignmentType.disabled = true;
		
		gridObj = initGridWithDistributedParsing('dataGrid',objGridData);
		gridObj.attachEvent("onEditCell",doOnCellEdit);
		gridObj.enableEditEvents(true);    
	    gridObj.enableEditTabOnly(true);

		for(var i=0;i<20 ;i++){
			addRow();
		}
		$("#searchempId").focus();
	
	 partNum_rowId = gridObj.getColIndexById("partNum");
	 partDesc_rowId = gridObj.getColIndexById("partDesc");
	 whName_rowId = gridObj.getColIndexById("wh");
	 whQty_rowId = gridObj.getColIndexById("whQty");
	 reqQty_rowId = gridObj.getColIndexById("reqQty");
	 whType_rowId = gridObj.getColIndexById("whInv");
	 
}

//this function is used to add the gird rows
function addRow(){
	gridObj.addRow(gridObj.getUID(),'');
}

//this function is used to remove the selected rows
function removeSelectedRow(){
var gridrows=gridObj.getSelectedRowId();
	if(gridrows!=undefined){
		var NameCellValue ;
		var gridrowsarr = gridrows.toString().split(",");
		var added_row;
		for (var rowid = 0; rowid < gridrowsarr.length; rowid++) {
			gridrowid = gridrowsarr[rowid];
			gridObj.deleteRow(gridrowid);			
		}
	}else{
		Error_Clear();
		Error_Details(message_operations[8]);				
		Error_Show();
		Error_Clear();
	}		
		
}
//this function used , when edit the part then fetch the part details and qty
function doOnCellEdit(stage, rowId, cellInd, nValue, oValue) {	
var pnum='';
var qty='';
var accid = '';
var repid = '';
var gpoid = '';

	if(cellInd == 0){
		 rowID = rowId;
		if (stage==2){
			
			if(nValue !=''){
				pnum = gridObj.cellById(rowId,partNum_rowId).getValue();
	           tempPnum = pnum;
		      qty = 1;
		      accid = 0;
		      repid = 0;
		      gpoid = 0;
		
		var url = fnAjaxURLAppendCmpInfo("/GmCartAjaxServlet?pnum=" + encodeURIComponent(pnum)+ "&qty=" + encodeURIComponent(qty)+ "&acc="+encodeURIComponent(accid)+ "&rep="+encodeURIComponent(repid)+ "&gpoid="+encodeURIComponent(gpoid)+"&inhouseType="+inhouseType+"&frmGrid=Y"+"&"+fnAppendCompanyInfo());
			var loader = dhtmlxAjax.getSync(url);
			fnAddToCart(loader,rowId);  
			
			}		
			} 
			
				
		}			
	
			
return true;
}

function fnGoCart(){
	var pnum = document.getElementById("Txt_PartNum").value;
	
	var partNumberStr ='';
	var PartStrLength = pnum.length;
	var index = pnum.lastIndexOf(",");
	
	if(PartStrLength == index+1){
	pnum = pnum.substr(0,index);
	}
	gridObj.forEachRow(function(rowId) {
		partNum = gridObj.cellById(rowId, partNum_rowId).getValue();
		if(partNum != ''){
			partNumberStr += ','+ partNum;
		}
		
	});
	
	var qty = 1;
	var accid = 0;
	var repid = 0;
	var gpoid = 0;
	var url ='';
	var loader ='';
	var response ='';
	if(partNumberStr != '' ){
		confirmMsg = confirm("This will replace the current partnumber(s) in the cart, Do you want to proceed?"); 
	if(!confirmMsg){
    	return false
    }
    if(confirmMsg){
	 url = fnAjaxURLAppendCmpInfo("/GmCartAjaxServlet?pnum=" + encodeURIComponent(pnum)+ "&qty=" + encodeURIComponent(qty)+ "&acc="+encodeURIComponent(accid)+ "&rep="+encodeURIComponent(repid)+ "&gpoid="+encodeURIComponent(gpoid)+"&inhouseType="+inhouseType+"&frmGrid=N"+"&"+fnAppendCompanyInfo());
	 loader = dhtmlxAjax.getSync(url);
	}
	}else{
	   url = fnAjaxURLAppendCmpInfo("/GmCartAjaxServlet?pnum=" + encodeURIComponent(pnum)+ "&qty=" + encodeURIComponent(qty)+ "&acc="+encodeURIComponent(accid)+ "&rep="+encodeURIComponent(repid)+ "&gpoid="+encodeURIComponent(gpoid)+"&inhouseType="+inhouseType+"&frmGrid=N"+"&"+fnAppendCompanyInfo());
	   loader = dhtmlxAjax.getSync(url);
	}

	 response = loader.xmlDoc.responseText;
   
   if(response.indexOf("cell") !='-1'){
		gridObj = initGridWithDistributedParsing('dataGrid',response);
		gridObj.attachEvent("onEditCell",doOnCellEdit);
		gridObj.enableEditEvents(true);    
		gridObj.enableEditTabOnly(true);
	}else{
		Error_Details(message[10078]);
	}
	
	if (ErrorCount > 0)
	 {
		Error_Show();
		Error_Clear();
		return false;
	}
	
	for(var i=0;i<20 ;i++){
			addRow();
		}
	 
}
//This function used to load the part details in grid
function fnAddToCart(loader,rowId){
	//var response =loader.xmlDoc.responseText;
	  var xmlDoc = loader.xmlDoc.responseXML;
      rowID = rowId;
	  parseMessage(xmlDoc,rowID);
}
//This method used to parse the response and set the value 
function parseMessage(xmlDoc,rowID) 
{
	var pnum = xmlDoc.getElementsByTagName("pnum");
	var data = xmlDoc.getElementsByTagName("data");
	var tissueParts = xmlDoc.getElementsByTagName("tissueparts");
	var partsList = '';	
	var tissueSet = xmlDoc.getElementsByTagName("tissueSets");
	var tissueSetList= '';
	var datalength = data[0].childNodes.length;
	
	if(datalength == 0){		
		 setErrMessage(message[10078],rowID); 
		 return;
	}

	var pnumlen = pnum.length;
	var rem = 0;
	
	var strresult = '';
	var partindex = '';
	var pdescindex = '';
	var stockindex = '';
	var currdateindex = '';
	var saleFlagIndex='';
	var conspriceindex=-1;
	var rwstockindex = '';
    var tissueindex='';
    var whInvindex = '';
     var qtyindex ='';
	// This loop is to just get the index from the first part tag (pnum[0])
	
	for (var x=0; x<pnum[0].childNodes.length; x++) 
		{
			xmlstr = pnum[0].childNodes[x].tagName;
			if (xmlstr == 'partnum' || xmlstr == 'setid')
			{
				partindex = x;
			} 
			else if (xmlstr == 'pdesc' )
			{
				pdescindex = x;
				
			}
			else if (xmlstr == 'qty')
			{
				qtyindex = x;
			}
			else if (xmlstr == 'whtype')
			{
				whtypeindex = x;
			}
			else if (xmlstr == 'currdate')
			{
				currdateindex = x;
				
			}
			else if (xmlstr == 'rfl')//Release for sale
			{
				saleFlagIndex=x; 
			}
			else if (xmlstr == 'consprice')//part price (Consignment price) 
			{
				conspriceindex = x; 
				
			}
			else if (xmlstr == 'tissueParts')//part price (Consignment price) 
			{
				tissueindex = x; 
				
			}else if (xmlstr == 'whinv')//part price (Consignment price) 
			{
				whInvindex = x; 
				
			}

			
			
		}
	//alert("saleFlagIndex "+ saleFlagIndex);
	// This loop is to set all the values as per the index got from the loop
	for (var x=0; x<pnumlen; x++) 
	{
		//alert("pnum[x]=="+pnum[x]);
		var pdesc = parseXmlNode(pnum[x].childNodes[pdescindex].firstChild);
		var part = parseXmlNode(pnum[x].childNodes[partindex].firstChild);
		var stock = parseXmlNode(pnum[x].childNodes[qtyindex].firstChild);
		var whtype = parseXmlNode(pnum[x].childNodes[whtypeindex].firstChild);
		var whInv = parseXmlNode(pnum[x].childNodes[whInvindex].firstChild);
		var rwstock = '';
		
		//var reqtype = parent.document.all.consignmentType.value;
		var releaseForSale='';
		///var Txtobj = eval("document.all.Txt_Qty"+x);
		var tissueFlag='';

		
		if(tissueFlag!= null && tissueFlag!='') 
			{
		 tissueFlag = parseXmlNode(pnum[x].childNodes[tissueindex].firstChild);
			}
		if(saleFlagIndex>0)
		{
			releaseForSale = parseXmlNode(pnum[x].childNodes[saleFlagIndex].firstChild);
		}  
		var currdate='';
		if (currdateindex > 0)
		{
			 currdate = parseXmlNode(pnum[x].childNodes[currdateindex].firstChild);
		}
		//var reqtype = parent.document.all.consignmentType.value;
		var releaseForSale='';
	   releaseForSale = parseXmlNode(pnum[x].childNodes[saleFlagIndex].firstChild);
		//}
		/*if (reqtype == 40021 || reqtype == 102930)// only for the Consignment and ICT , need to check release_for_sale Flag
		{
			if(releaseForSale == 'N'){		
				setErrMessage('<span class=RightTextBlue>Part Number does not released for sale</span>',rowID);
				Txtobj = '';
				return;
		}
	}*/
		var partprice =0;
		if(conspriceindex >= 0)
	 	{
	 		partprice = parseXmlNode(pnum[x].childNodes[conspriceindex].firstChild);
	 	}else{
	 		partprice = -1;
	 	}
		setMessage(part,pdesc,stock,whtype,currdate,partprice,whInv);
	}

}
 //This function used to set the value in grid
function setMessage(part,pdesc,stock,whtype,currdate,partprice,whInv){
	
	var reqQty = '1';
	if(partprice==''){
	pdesc = '<span class=RightTextBlue>The Price is not set for this part</span>';
		gridObj.cellById(rowID, partDesc_rowId).setValue(pdesc);		
		gridObj.cellById(rowID, partNum_rowId).setValue("");	
	}else{
	gridObj.cellById(rowID, partDesc_rowId).setValue(pdesc);	
	gridObj.cellById(rowID, whName_rowId).setValue(whtype);	
	gridObj.cellById(rowID, whQty_rowId).setValue(stock);	
	gridObj.cellById(rowID, whType_rowId).setValue(whInv);	
	gridObj.cellById(rowID, reqQty_rowId).setValue(reqQty);	
	
	}
}
//This function used to set the validation msg in grid
function setErrMessage(msg,rowID) 
{		
	gridObj.cellById(rowID, partNum_rowId).setValue('');
	gridObj.cellById(rowID, partDesc_rowId).setValue(msg);		
}

var refNm='';
var refids='';
//This function used to create the inpit string and pass the value for save 
function fncreateIHTransaction(){
	//var setid = document.getElementById("frmCart").contentWindow.fnGetSetID();
	var consignType = document.frmInhouseTransaction.consignmentType.value;
	//var seltext = obj.options[obj.selectedIndex].text;
 	var bill = document.frmInhouseTransaction.empId.value;
    var partNum='';
    var reqQty ='';
    var whType ='';
    var whInv ='';
    var qty ='';
   var strErrorRwQty ='';
   var strReqQty ='';
  
 	var purpose = document.frmInhouseTransaction.inHousePurpose.value; 
var inputString ='';
var comments = document.frmInhouseTransaction.Txt_LogReason.value;

	gridObj.forEachRow(function(rowId) {
				//gridrowid = gridrowsarr[rowid];
				partNum = gridObj.cellById(rowId, partNum_rowId).getValue();
				whType = gridObj.cellById(rowId, whName_rowId).getValue();
				qty = gridObj.cellById(rowId, whQty_rowId).getValue();
				reqQty = gridObj.cellById(rowId, reqQty_rowId).getValue();
				whInv = gridObj.cellById(rowId, whType_rowId).getValue();
				
				if(partNum !=''){
				if((parseInt(reqQty) > parseInt(qty))){
					strErrorRwQty += ", " + partNum;
				}
		        if(partNum !='' && reqQty ==''){
			          strReqQty +="," +partNum;
		          }
				if(partNum !='' && whInv !=''){
					inputString += 	partNum+'^'+reqQty+'^'+whInv+'|';
				}
			}
				
			});


if(strErrorRwQty != ''){
		Error_Details(Error_Details_Trans("Requested Qty cannot be greater than warehouse available quantity for the following part(s) [0] ",strErrorRwQty.substring(1, strErrorRwQty.length)));
}

if(strReqQty != ''){
	Error_Details(Error_Details_Trans("Requested Qty cannot be Empty for the following part(s) [0] ",strReqQty.substring(1, strReqQty.length)));
}

document.frmInhouseTransaction.hinputString.value = inputString;
	fnValidate();
	if (ErrorCount > 0)
	 {
		Error_Show();
		Error_Clear();
		return false;
	}else{			
	
	    document.frmInhouseTransaction.action = "/gmRuleEngine.do?RefName="+refNm+"&RefIDs="+refids;  
		document.frmInhouseTransaction.consignmentType.value = consignType;
		document.frmInhouseTransaction.hinputString.value = inputString;
	 	document.frmInhouseTransaction.IHTransType.value =inhouseType;
	    document.frmInhouseTransaction.inHousePurpose.value =purpose;
		document.frmInhouseTransaction.empId.value = bill;
		document.frmInhouseTransaction.strOpt.value = 'save';
		//document.frmInhouseTransaction.Btn_Submit.disabled = true;
		document.frmInhouseTransaction.txt_LogReason.value = comments;
		
		fnStartProgress();
		document.frmInhouseTransaction.submit();
		}
}
//This function used to validate the field
function fnValidate(){
	var requestTypeVal = '';
	var requiredDate = '';
	var billTo = '';
	var shipTo = '';
	var inputstr = '';

	fnValidateDropDn('consignmentType',message[5542]);
	fnValidateDropDn('inHousePurpose',message[5543]);

	inputstr = document.frmInhouseTransaction.hinputString.value;	
	requestTypeVal = document.frmInhouseTransaction.consignmentType.value;
	var reqTypeName = document.frmInhouseTransaction.consignmentType.options[document.frmInhouseTransaction.consignmentType.selectedIndex].text;
   	var bill = document.frmInhouseTransaction.empId.value;

	if(inputstr == ''){
			Error_Details(message[5081]);
		}
		if(bill == ''){
			Error_Details("Please Enter the Bill To");
		}
	
	if(requestTypeVal == '40022'){
		var objInHousePurpose = document.frmInhouseTransaction.inHousePurpose;

		if (objInHousePurpose.disabled == true){
			Error_Details(message[5038]);	
		}

	}

	// validation added for GIMS
	var negQtyStr = '';
	var nonNumStr = '';
	var maxQtyStr = '';
	var empQtyStr = '';
	var stoQtyStr = '';
	var decimalCheck = '';
	var stockQtyVal='';
	var errFl = true;
	var partNum='';
	
	var gridrows =gridObj.getChangedRows(",");
	var gridrowsarr = gridrows.toString().split(",");
	var columnCount = gridObj.getColumnsNum();

		gridObj.forEachRow(function(rowId) {
				//gridrowid = gridrowsarr[rowid];
				partNum = gridObj.cellById(rowId, partNum_rowId).getValue();
				whType = gridObj.cellById(rowId, whName_rowId).getValue();
				reqQtyVal = gridObj.cellById(rowId, reqQty_rowId).getValue();
				whInv = gridObj.cellById(rowId, whType_rowId).getValue();
				stockQtyVal =gridObj.cellById(rowId, whQty_rowId).getValue();
			
				if(partNum !=''){
					//if (objpnum.innerHTML != '&nbsp;') {
					if(reqQtyVal <= 0){
						negQtyStr += partNum +", ";
					}
					decimalCheck = reqQtyVal.replace(/[0-9]+/g,''); // Qty text box
					if (isNaN(reqQtyVal) || decimalCheck.length >0){   
						errFl = false;
						nonNumStr +=partNum +", ";		    	   	
				    }
		
					if(reqQtyVal ==''){
						empQtyStr += partNum +", ";				
					}
					
				//}		
				}
				
			});
	
	if (negQtyStr !=''){			
		Error_Details(Error_Details_Trans(message[5553],negQtyStr.substr(0,negQtyStr.length-2)));
	}
	if (nonNumStr !=''){			
		Error_Details(Error_Details_Trans(message[5554],nonNumStr.substr(0,nonNumStr.length-2)));
	}
	if (maxQtyStr !=''){			
		Error_Details(Error_Details_Trans(message[5555],maxQtyStr.substr(0,maxQtyStr.length-2)));
	}
	if (empQtyStr !=''){			
		Error_Details(Error_Details_Trans(message[5556],empQtyStr.substr(0,empQtyStr.length-2)));
	}
	if (stoQtyStr !=''){			
		Error_Details(Error_Details_Trans(message[5557],stoQtyStr.substr(0,stoQtyStr.length-2)));
	}
}
//This function used to open the part lookup sceen
function fnOpenPart()
{
	var pnum = document.getElementById("Txt_PartNum").value;
	windowOpener("/GmSearchServlet?hAction=SearchPart&Txt_PartNum="+encodeURIComponent(pnum),"Search","resizable=yes,scrollbars=yes,top=340,left=420,width=600,height=435");
}