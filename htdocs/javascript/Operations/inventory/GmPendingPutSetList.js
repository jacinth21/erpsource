// this fuction is used to load the grid data.
var chk_rowId = '';
var tag_rowId = '';
var set_rowId = '';
var typeNm_rowId = '';
var location_rowId = '';
var nextSt_rowId = '';
var refId_rowId = '';
var type_rowId = ''; 
var locID_rowId = '';
var tagIdval = "";
var msg = "";
var msg1 = message_operations[14];
var msg2 = "";
var msg3 = "";
var msg4 = "";
var rowID = "";
var scanTagIdfl=false;
var statusfl = false;
var onBlurSubmitFl = false;
var onEnterSubmitFl = false;


function fnOnPageLoad() {
	var nextStatus = '';
	if (objFl == 'Y') {
		gridObj = initGridWithDistributedParsing('dataGrid', objGridData);
		gridObj.attachEvent("onRowSelect", doOnRowSelected);
		gridObj.attachEvent("onEditCell", doOnCellEdit);
		// to get each column IDs
		chk_rowId = gridObj.getColIndexById("put_ch");
		tag_rowId = gridObj.getColIndexById("tagId");
		set_rowId = gridObj.getColIndexById("setId");
		typeNm_rowId = gridObj.getColIndexById("setTypeNm");
		location_rowId = gridObj.getColIndexById("locations");
		nextSt_rowId = gridObj.getColIndexById("nextStatus");
		refId_rowId = gridObj.getColIndexById("refIds");
		type_rowId = gridObj.getColIndexById("cnType"); 
		locID_rowId = gridObj.getColIndexById("locID");
		
		// For Single click 
		gridObj.enableEditEvents(true,false,true);
		// For enable the tab order
		gridObj.enableEditTabOnly(true)
		
		gridObj.forEachRow(function(rowId) {
			nextStatus = gridObj.cellById(rowId, nextSt_rowId).getValue();

			if (nextStatus == 'Pending Shipping') {
				gridObj.cellById(rowId, location_rowId).setDisabled(true);
			}
		}); 
		document.all.Message.innerHTML = msg1;
		document.frmPendingPutSet.tagID.focus();
	}
	
}

// function used when select the row.
function doOnRowSelected(rowId) {
    nextSt_rowId = gridObj.getColIndexById("nextStatus");
    nextStatus = gridObj.cellById(rowId, nextSt_rowId).getValue();
	if (nextStatus == 'Pending Shipping') {
		gridObj.cellById(rowId, location_rowId).setDisabled(true);
		gridObj.editStop(true);
	}
}

// this function used , when edit the location id then validate the location id
function doOnCellEdit(stage, rowId, cellInd, nValue, oValue) {

	if (cellInd == location_rowId) {
		rowID = rowId;
		if (stage == 2) {
			var location_Id = gridObj.cellById(rowID, location_rowId).getValue();
			var wh_Id = document.frmPendingPutSet.wareHouseID.value;   
			if (location_Id != '')
				var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmPendingPutSet.do?method=validateLocation&locationID='+ nValue +'&wareHouseID='+ wh_Id + '&ramdomId=' + Math.random());
				dhtmlxAjax.get(ajaxUrl,fnValidateLocationId);
		}
	}
	return true;
}

// this function used to show the part/set description
function fnValidateLocationId(loader) {
	var response = loader.xmlDoc.responseText;
	var scanLocId = document.frmPendingPutSet.hScanLocID.value;
	var locID = "";
	if (scanLocId != ''){
		rowID = document.frmPendingPutSet.hGridRowId.value;
	}
	
	if (response == "^") {
		Error_Details(message_operations[15]);
	} else if (response != null && response.length > 0) { 
		var locTypeId = response.substring(0, response.indexOf("^"));
		locID = response.substring(response.indexOf("^")+ 1);
		 
		var location_type = gridObj.cellById(rowID, type_rowId).getValue(); // Consignment /Product Loaner/ In house Loaner 
		if (location_type == '4110') {
			if (locTypeId != '93501') { // Consignment
				Error_Details(message_operations[16]);
			}
		} else if (location_type == '4127') { // Loaner
			if (locTypeId != '93502') {
				Error_Details(message_operations[17]);
			}
		}else if (location_type == '4119') { // In House Loaner
			if (locTypeId != '93503') {
				Error_Details(message_operations[18]);
			}
		}
	}
	if (ErrorCount > 0) {
		gridObj.cellById(rowID, location_rowId).setValue("");
		Error_Show();
		Error_Clear();
		return false;
	}else{	 
		gridObj.cellById(rowID, chk_rowId).setChecked(true);
		gridObj.cellById(rowID, locID_rowId).setValue(locID);
		if (scanLocId != ''){
			gridObj.cellById(rowID, location_rowId).setValue(scanLocId);
		}
		if(onEnterSubmitFl){
			fnSubmit();
			rtVal = true;
		}
	}

}

function parseNull(val) {
	if (val == 'null')
		return val = "";
	else
		return val;
}

function fnSubmit() {
	var gridrowid;
	var inputString = '';
	var gridrows = gridObj.getCheckedRows(0);
	var tempval = '';
	var temploc = '';
	var tempSet = '';
	var temptag = '';
	var tempCn = '';
	var tempType = '';
	var tempNextStat = ''; 
	
	if (gridrows.length > 0) {
		var gridrowsarr = gridrows.toString().split(",");

		for ( var rowid = 0; rowid < gridrowsarr.length; rowid++) {
			gridrowid = gridrowsarr[rowid];
			tempval = gridObj.cellById(gridrowid, chk_rowId).getValue();
			temploc = gridObj.cellById(gridrowid, locID_rowId).getValue();
			tempSet = gridObj.cellById(gridrowid, set_rowId).getValue();
			temptag = gridObj.cellById(gridrowid, tag_rowId).getValue();
			tempCn = gridObj.cellById(gridrowid, refId_rowId).getValue();
			tempType = gridObj.cellById(gridrowid, type_rowId).getValue();
			tempNextStat = gridObj.cellById(gridrowid, nextSt_rowId).getValue(); 
			
			if (tempval == 1) {
				if (temploc == ''&& tempNextStat !='Pending Shipping') {
					Error_Clear();
					Error_Details(message_operations[19]);
				}
				inputString += tempCn + '^' +  temploc + '^'+ temptag  +'|';
			}
		}
	}

	if (inputString == '') {
		Error_Details(message_operations[13]);
	}

	if (ErrorCount > 0) {
		inputString = '';
		Error_Show();
		Error_Clear();
		return false;
	}
	document.frmPendingPutSet.strInputString.value = inputString;
	document.frmPendingPutSet.strOpt.value = 'Save';
 	document.frmPendingPutSet.action = '/gmPendingPutSet.do?method=savePendingPutSets';
 	fnStartProgress('Y');
 	document.frmPendingPutSet.submit();
}

// this function used to print location ()
function fnPrint() {

	gridObj.setColumnHidden(chk_rowId, true);
	// gridObj.printView();
	parent.window.print();
	gridObj.setColumnHidden(chk_rowId, false);

	// document.printInvLocation.submit();
}
function hidePrint() {
	if (accFl == 'Y') {
		buttonTdObj = document.getElementById("buttonTR");
		buttonTdObj.style.display = 'none';
	}
}

function showPrint() {
	if (accFl == 'Y') {
		buttonTdObj = document.getElementById("buttonTR");
		buttonTdObj.style.display = 'block';
	}

}

function fnScanIdOnBlur()
{
	var tagIdval = document.frmPendingPutSet.tagID.value;
	var htagID = document.frmPendingPutSet.hTagID.value;
	msg = "";
	msg2 = "<font color='red'>(" + message_operations[20] + " )</font>";
	msg3 = "( " +message_operations[21]+tagIdval+" )";
	msg4 = "<font color='red'>( " +message_operations[22]+htagID+" )</font>";
		
	if (tagIdval != '')
	{
		var objRegExp = tagIdval.match(/101~/g);
		
		if (!scanTagIdfl && tagIdval.indexOf("-") != -1)
		{
			document.all.Message.innerHTML = msg2;
			document.frmPendingPutSet.tagID.value = '';
			document.frmPendingPutSet.tagID.focus();
		}else if (!scanTagIdfl && tagIdval.indexOf("-") == -1)
		{
			onBlurSubmitFl = false;
			fnScanTagId(tagIdval);
		}else if (scanTagIdfl && tagIdval.indexOf("-") == -1)
		{
			document.all.Message.innerHTML = msg4;
			document.frmPendingPutSet.tagID.value = '';
			document.frmPendingPutSet.tagID.focus();
		}else if (scanTagIdfl && tagIdval.indexOf("-") != -1)
		{
			if (objRegExp == null){
				document.all.Message.innerHTML = msg4;
				document.frmPendingPutSet.tagID.value = '';
				document.frmPendingPutSet.tagID.focus();
			}else{
				var vstring = tagIdval.substring(0,tagIdval.indexOf('^'));
				tagIdval = tagIdval.substring((vstring.length)+1,tagIdval.length);
				fnScanLocationId(tagIdval);
			}
		}
	}
	
}

function fnScanTagId(tagIdval)
{
	gridObj.forEachRow(function(rowId) {
		
		tag_rowId_Val = gridObj.cellById(rowId, tag_rowId).getValue();
		
		nextStatus = gridObj.cellById(rowId, nextSt_rowId).getValue();

		if (nextStatus == 'Pending Shipping') {
			gridObj.cellById(rowId, location_rowId).setDisabled(true);
			
		}
		
		if (tag_rowId_Val == tagIdval)
		{
			document.frmPendingPutSet.hGridRowId.value = rowId;
			document.frmPendingPutSet.hTagID.value = tagIdval;
			if (nextStatus == 'Pending Shipping'){
				gridObj.cellById(rowId, chk_rowId).setChecked(true);
				scanTagIdfl = false;
				statusfl = true
				if(onBlurSubmitFl){
					fnSubmit();
				}
				document.all.Message.innerHTML = msg1;
			}else{
				document.all.Message.innerHTML = msg3;
				scanTagIdfl = true;
			}
		}
	});
	if(!scanTagIdfl && !statusfl && tag_rowId_Val != tagIdval)
	{
		document.all.Message.innerHTML = msg2;
	}else if (!scanTagIdfl && statusfl){
		document.all.Message.innerHTML = msg1;
		statusfl = false;
	}
	document.frmPendingPutSet.tagID.value = '';
	document.frmPendingPutSet.tagID.focus();
}

function fnScanLocationId(tagIdval)
{
	document.frmPendingPutSet.hScanLocID.value = tagIdval;
	var wh_Id = document.frmPendingPutSet.wareHouseID.value;   
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmPendingPutSet.do?method=validateLocation&locationID='+ tagIdval+'&wareHouseID='+ wh_Id + '&ramdomId=' + Math.random());
	dhtmlxAjax.get(ajaxUrl,fnValidateLocationId);

	document.frmPendingPutSet.tagID.value = '';
	document.frmPendingPutSet.tagID.focus();
	scanTagIdfl = false;
	document.all.Message.innerHTML = msg1;
}

function fnScanIdOnEnter()
{
	if(event.keyCode == 13)
	{
		var tagIdval = document.frmPendingPutSet.tagID.value;
		var htagID = document.frmPendingPutSet.hTagID.value;
		var objRegExp = "";
		var rtVal = false;
		msg = "";
		msg2 = "<font color='red'>( "+message_operations[20]+" )</font>";
		msg3 = "( "+message_operations[21]+tagIdval+" )";
		msg4 = "<font color='red'>( "+message_operations[22]+htagID+" )</font>";
			
		if (tagIdval != '')
		{
			objRegExp = tagIdval.match(/101~/g);
			
			if (!scanTagIdfl && tagIdval.indexOf("-") != -1)
			{
				document.all.Message.innerHTML = msg2;
				document.frmPendingPutSet.tagID.value = '';
				document.frmPendingPutSet.tagID.focus();
			}else if (!scanTagIdfl && tagIdval.indexOf("-") == -1)
			{
				onBlurSubmitFl = true;
				fnScanTagId(tagIdval);
			}else if (scanTagIdfl && tagIdval.indexOf("-") == -1)
			{
				document.all.Message.innerHTML = msg4;
				document.frmPendingPutSet.tagID.value = '';
				document.frmPendingPutSet.tagID.focus();
			}else if (scanTagIdfl && tagIdval.indexOf("-") != -1)
			{
				if (objRegExp == null){
					document.all.Message.innerHTML = msg4;
					document.frmPendingPutSet.tagID.value = '';
					document.frmPendingPutSet.tagID.focus();
				}else{
					var vstring = tagIdval.substring(0,tagIdval.indexOf('^'));
					tagIdval = tagIdval.substring((vstring.length)+1,tagIdval.length);
					onEnterSubmitFl = true;
					fnScanLocationId(tagIdval);
				}
			}
		}
		if(rtVal == false)
		{
			if(!e) var e = window.event;

	        e.cancelBubble = true;
	        e.returnValue = false;	

	        if (e.stopPropagation) {
	                e.stopPropagation();
	                e.preventDefault();
	        }
		}
	}
}