
// this function used to load the grid data
function fnOnPageLoad()
{
	var elem = document.frmLocation.locationID;
	var locationMap = document.frmLocation.locMap.value;
	var showFilters = document.frmLocation.hshowFilters.value; 
	elem.focus();    
	if(objGridData.indexOf("cell", 0)!=-1){ 
		gridObj = initGridWithDistributedParsing('dataGrid',objGridData);
		if(locationMap =='SetMap'){ // Set Report Load
			gridObj.enableTooltips("false,false,false,true,true,true,true,true");
		}else{ // Part Load
			gridObj.enableTooltips("false,false,false,true,true,true,true,true,true,true");
			
			gridObj.attachHeader(",,,,,,,,,#numeric_filter,,,,"); 
		}
			gridObj.enableBlockSelection();
			gridObj.copyBlockToClipboard();
	}
	
fnHideRow();
//Need to display the DHTMLX Dropdowns , when we come from Main Inventory report.
/*	if(showFilters =='yes'){
		fnRptReload();
	}*/
}
// this function is used to hide the column based on the stropt
function fnHideRow()
{
	var val = document.frmLocation.strOpt.value; 
	var showFilters = document.frmLocation.hshowFilters.value; 
	if(val == 'view')
	{
		if(showFilters !='yes'){
			document.getElementById("filRow1").style.display = 'none';
			document.getElementById("filRow2").style.display = 'none';
			document.getElementById("saveBtnRow").style.display = 'none';
	    }		
	}else{
		if(showFilters !='yes'){
			document.getElementById("closeBtnRow").style.display = 'none';
		}
	}
}
// to show the error msg
function fnErrorMsg(msg){
	Error_Details(msg);
	Error_Show();
	Error_Clear();
	return false;
}
//this function is used to click on check box to reload the rpt
function fnRptReload()
{
	document.frmLocation.haction.value = "rptReload";
	document.frmLocation.strOpt.value = 'load';
	fnStartProgress('Y');
	document.frmLocation.submit();
}
//this function used to redirect to edit location screen.
function fnEditLocation(id,wid,bid)
{
	//document.frmLocation.locationID = id;
	document.frmLocation.action = "/gmlocation.do?method=fetchInvLocationByID&locationId="+id + "&wid=" + wid + "&bid=" + bid;
 	document.frmLocation.submit();
		
}
//this function used to print location (barcode)
function fnPrint()
{
		document.frmLocation.strOpt.value='Print';
		document.frmLocation.action = "/gmlocation.do?method=printInvLocation";
		document.frmLocation.submit();
	 	//windowOpener("/gmlocation.do?method=fetchInvLocationPartMapping&strOpt=Print","","scrollbars=no,top=150,left=100,width=720,height=500");
}
//this function used to export the excel report
function fnExcelExport(){
	gridObj.editStop();
	gridObj.detachHeader(1);
	gridObj.toExcel('/phpapp/excel/generate.php');
}
//this function used load the rpt when enter key press
function fnEnterPress(evt){
	evt = (evt) ? evt : window.event
	if(evt.keyCode==13){
		fnRptReload();
	}
}

function fnUpper(ustr) {
	var str = ustr.value;
	ustr.value = str.toUpperCase();
	
}
/*
function fnUpperEvt(evt){
	var e=window.event || evt;
	
	if(e.keyCode >=65 && e.keyCode<=90 ){
		e.keyCode += 5;
	}
	
}*/