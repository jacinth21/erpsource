// This function used to split the transaction for PMT-36676
function fnSubmit(){
    var transId = document.frmRerunTxnSplit.strTxnId.value;
    var transType = document.frmRerunTxnSplit.strTxnType.value;
   
    if(transId == "" && transType == '0'){
		document.getElementById("successCont").innerHTML = '';
        Error_Details(message_operations[804]);
  	       if (ErrorCount > 0) {
  	   		Error_Show();
  	   		Error_Clear();
  	   		return false;
  	   		}
      }
    
    if(transId == ""){
	  document.getElementById("successCont").innerHTML = '';
      Error_Details(message_operations[805]);
	       if (ErrorCount > 0) {
	   		Error_Show();
	   		Error_Clear();
	   		return false;
	   		}
    }
    
    if(transType == '0') {
	  document.getElementById("successCont").innerHTML = '';
      Error_Details(message_operations[806]);
	       if (ErrorCount > 0) {
	   		Error_Show();
	   		Error_Clear();
	   		return false;
	   		}
	 }
    
    var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmRerunTxnSplitAction.do?method=processSplitTransactionByLocation&strTxnId='
			+ encodeURIComponent(transId)
			+ '&strTxnType='
			+ transType
			+ '&ramdomId='
			+ Math.random());
	dhtmlxAjax.get(ajaxUrl, fnLoadTxnSplitMessageAjax);   
    
}



//To display the error and success message for PMT-36676
function fnLoadTxnSplitMessageAjax(loader) {
	var response = loader.xmlDoc.responseText;
	if(response != ''){
		if( response.indexOf('Successfully') >= 0){ //to display the success message in green color
			document.getElementById("successCont").style.color = 'green';
			document.getElementById("successCont").style.fontWeight = 'bold';
			}
		document.getElementById("successCont").innerHTML = response;
	}
}
