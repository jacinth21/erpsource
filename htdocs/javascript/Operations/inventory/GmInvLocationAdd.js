
var gridObj;
// this function used to calculate the Shelf length
function fnminLength()
{
	var shelfLen = document.getElementById('shelf').value;
	if(shelfLen.length<3 || shelfLen.length>3 ||!isNumber(shelfLen))
	{	
		Error_Details(message_operations[1]);  
	} 
	else {
		return true;
	}
	 
}

//this function used to load the Grid	
function fnGrid()
{
	 var cellValue=new Array();
	 var rowValue;
	 var colCount = document.frmLocation.columnNum.value;
	 var rowCount = document.frmLocation.rowNum.value;
	 var aisleValue = document.frmLocation.aisleNum.value;
  	 var zoneValue  = document.frmLocation.zoneNum.options[document.frmLocation.zoneNum.selectedIndex].text; 
	 var shelfValue =	document.frmLocation.shelf.value;
	 var bFlag = document.frmLocation.bldFlag.value;
	 var newzone = document.frmLocation.newZone.value;
	 var chkds = $("input[name='activeFl']:checkbox");
	 if (chkds.is(":checked"))  {
		 document.frmLocation.activeFl.value='Y'; 
	 }
	  else {
		  document.frmLocation.activeFl.value=''; 
	  }
	fnminLength();
	fnAisleCheck(aisleValue);
	fnCheck(colCount,lblColumns);
	fnCheck(rowCount, lblRows);
	fnCheckType();
	fnValidateZoneDrop(zoneValue,newzone);
	fnValidateZone();
	
	
	//added for PMT-33507 - Add or Edit Building Locations
	if(bFlag == 'Y') {
		fnValidateDropDn('buildingID',lblBuilding); //PMT-33507
	}
	fnValidateDropDn('warehouseid',lblWarehouse);
	fnValidateTxtFld('aisleNum',lblAisle);
	fnValidateTxtFld('shelf',lblShelf); 
	fnValidateTxtFld('rowNum',lblRow); 
	fnValidateTxtFld('columnNum',lblColumn); 
	fnValidateDropDn('typeNum',lblType);  

	if (ErrorCount > 0)
	{ 
			Error_Show();
			Error_Clear();
			return false;
	}
	 var i=0;
	  
	 
		  document.getElementById("tableData").style.display ="block";	
	
		if(50+colCount*120 < 700)
		{
		   document.getElementById("dataGrid").style.width =50+colCount*100;
		}
		else
		{
		   document.getElementById("dataGrid").style.width =500+200;
		}
		
		if(document.frmLocation.rowNum.value*40 <= 400){
			if(document.frmLocation.rowNum.value == 1)
				document.getElementById('dataGrid').style.height = 60+'px';
			else
				document.getElementById('dataGrid').style.height = document.frmLocation.rowNum.value*40+10+'px';
		}
		else
			document.getElementById('dataGrid').style.height = 400+'px';
		
	 
	  
	gridObj = new dhtmlXGridObject('dataGrid');		 
	gridObj.init();
	gridObj.setStyle("background:'#E3EFFF'; font-weight:bold;", "","color:red;", "");
	var dynaWidth = 640/colCount;
	
	var xmlString = "<rows><head><column align='center' type='ro' width='50'> </column>";

	
	for(var colValue=0; colValue<document.frmLocation.columnNum.value; colValue++) 
	{
		xmlString = xmlString + "<column align='center' type='ro' width='"+dynaWidth+"'>Col"+(colValue+1) +"</column>";
	}
	//xmlString = xmlString + "<column align='center'  type='ro' width='80' class='RightDashBoardHeader'>Col"+(colValue+1) +"</column>";
	xmlString = xmlString + "</head></rows>";
	gridObj.loadXMLString(xmlString);	
	
 
	 
	 for(i=0;i<document.frmLocation.rowNum.value;i++) 
	{
		rowValue = String.fromCharCode(i+65);
	     cellValue[0]="Row"+(i+1);
	    
		for(var colValue=1; colValue<=document.frmLocation.columnNum.value; colValue++) 
		{
		
			 if(zoneValue=='[Created New Zone]'){
				    cellValue[colValue]=newzone+"-"+(document.frmLocation.aisleNum.value).toUpperCase()+"-"+document.frmLocation.shelf.value+"-"+rowValue+"-"+colValue+"";                         
			 }
			 else{
				    cellValue[colValue]=zoneValue+"-"+(document.frmLocation.aisleNum.value).toUpperCase()+"-"+document.frmLocation.shelf.value+"-"+rowValue+"-"+colValue+"";                         
			 }
		}
		gridObj.addRow(i+1,cellValue);
    }
   
  //document.getElementById('dataGrid').style.height = document.frmLocation.rowNum.value*30+'px';
  
	//alert(document.getElementById('dataGrid').style.width);
  	//alert(document.getElementById('dataGrid').style.height);
   
}
//this function used to validate the Type and Zone
// This Function code is commenting since SPP Project is not moving currently.
 //* For BUG-2304 Resolution under GOP Project
/*
function fnCheckType(){
	var zone = document.frmLocation.zoneNum.value;
	var type = document.frmLocation.typeNum.value;
    var msgDisp = 'Location type does not match the location, please choose correct type.';
	if(type == 93501 || type == 93502 || type == 93503){ // Consignment or Product Loaner or In-House Loaner
		if(zone == 100145 && type != 93501){ //Consignment
			Error_Details(msgDisp);
			}else if(zone == 100144 && type != 93502){ //Product Loaner
				Error_Details(msgDisp);
		    }else if(zone == 100300 && type != 93503){ //Product Loaner
		    	Error_Details(msgDisp);
			    }else if(zone != 100145 && zone != 100300 && zone !=100144){
			    	Error_Details(msgDisp);
				    }
	}
		if(type == 93320 || type == 93336){
			if(zone == 100145 ||zone == 100144||zone == 100300){
			Error_Details(msgDisp);
			}
	   }
}
*/

function fnCheckType(){
	var warehouseId = document.frmLocation.warehouseid.value;
	var type = document.frmLocation.typeNum.value;
    var msgDisp = message_operations[2];
	
    if(warehouseId == 4 && type != 93503){ // In-House Loaner
		Error_Details(msgDisp);
	}
}

//this function used to form the String and save
function fnSubmit()
{
		gridObj.editStop();// retun value from editor(if presents) to cell and closes editor
		var gridrowid;
		var void_flag;
		var inputString='';
		var gridrows =gridObj.getChangedRows(",");
		
		fnCheckType();
		if(gridrows.length==0){
			inputString = "";
		}
		else
		{
			var gridrowsarr = gridrows.toString().split(",");
			var columnCount = gridObj.getColumnsNum();
		
			
			for (var rowid = 0; rowid < gridrowsarr.length; rowid++) {
				gridrowid = gridrowsarr[rowid];
			
					void_flag = gridObj.getRowAttribute(gridrowid,"row_deleted");
					if(void_flag ==  undefined){
						void_flag='';
					}
					
					for(var col=1;col<columnCount;col++){
						
						inputString+=gridObj.cellById(gridrowid, col).getValue()+'^';
						
					}
					
					inputString+='^';
			}
			inputString+="$";
		
		
	}	

		if (ErrorCount > 0)
		{ 
				Error_Show();
				Error_Clear();
			//	document.getElementById("tableData").style.display ="none";
				return false;
		}
		
		document.frmLocation.strOpt.value = 'save';
		document.frmLocation.strInputString.value = inputString;
		fnStartProgress('Y');
		document.frmLocation.submit();
	 
}
//this function validate the number.
function isNumber (val)
{
  return ! isNaN (val-0);
}
//this function validate the rows and column value.
function fnCheck(val, txt)
{ 
	if(val.length > 3)
	{
		Error_Details(Error_Details_Trans(message_operations[3],txt)); 
	}
	if(!isNumber(val))
	{
		Error_Details(Error_Details_Trans(message_operations[4],txt)); 
	}

 
}
//this function used to Check the Aisel
function fnAisleCheck(val)
{
	 var regularexp = /^[a-z\xC0-\xFF\u0430-\u045F]+$/i
	if(val.length > 1||!regularexp.exec(val))
	{
		Error_Details(message_operations[5]);   
	}
	document.frmLocation.aisleNum.value = (document.frmLocation.aisleNum.value).toUpperCase();
	  
}
// This function used, if press enter key then reload the report.
function fnEnterPress(evt){
	evt = (evt) ? evt : window.event
	if(evt.keyCode==13){
		fnGrid();
	}
}

function fnDisabledNewZone(val){
	if(val>0){
		document.getElementById("newZone").value="";
		document.getElementById("newZone").disabled = true;
	}
	else{
		document.getElementById("newZone").disabled = false;
	}
}

function fnValidateZone()
{
	var newZone = document.frmLocation.newZone.value;
	 var zonLis = document.getElementById('zoneNum');
	for(var i=1;i<zonLis.length;i++){
		if(newZone==zonLis[i].textContent){
			Error_Details("This Zone is already Exist");
		}
	}
}
function fnValidateZoneDrop(zonevalue,newZone){
	var newZone = document.frmLocation.newZone.value;
	if(newZone==''){
		fnValidateDropDn('zoneNum',lblZone);
	}
	else{
		fnNewZoneLength(newZone);
	}
}
function fnNewZoneLength(newZone)
{
	if(newZone.length<3 || newZone.length>3 ||!isNumber(newZone))
	{	
		Error_Details(message_operations[850]);  
	} 
	else {
		return true;
	}
	 
}