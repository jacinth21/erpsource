// This function is used to load the grid data
var check_rowId = '';
var location_rowId = '';
var tag_rowId = '';
var set_rowId = '';
var cn_rowId = '';
var locType = "";

function fnOnPageLoad(rptAction) {
	if (objFl == 'Y') {
		gridObj = initGridWithDistributedParsing('dataGrid', objGridData);
		if(rptAction =='PICK_DASH'){ // Pending Pick Dashboard
			gridObj.enableTooltips("true,true,true");
			gridObj.attachHeader('#select_filter,#text_filter,#text_filter');
		}
		
		check_rowId = gridObj.getColIndexById("picked");
		location_rowId = gridObj.getColIndexById("location");
		tag_rowId = gridObj.getColIndexById("tagid");
		set_rowId = gridObj.getColIndexById("setid");
		cn_rowId = gridObj.getColIndexById("hcnid");
		locType = document.frmPendingPickSet.locationType.value;
		if (locType != ''){
			document.frmPendingPickSet.tagId.focus();
		}
	}
}
// this function is used to save the data.
function fnSubmit() {
	// gridObj.editStop();// retun value from editor(if presents) to cell and
	// closes editor
	var gridrowid;
	var inputString = '';
	var gridrows = gridObj.getCheckedRows(0);
	var tempval = '';
	var temploc = '';
	var tempSet = '';
	var temptag = '';
	var tempCn = '';

	var locationTypeVal = document.frmPendingPickSet.locationType.value;
	if (gridrows.length > 0) {
		var gridrowsarr = gridrows.toString().split(",");

		for ( var rowid = 0; rowid < gridrowsarr.length; rowid++) {
			gridrowid = gridrowsarr[rowid];
			tempval = gridObj.cellById(gridrowid, check_rowId).getValue();

			if (tempval == 1) {
				temploc = gridObj.cellById(gridrowid, location_rowId).getValue();
				tempSet = gridObj.cellById(gridrowid, set_rowId).getValue();
				temptag = gridObj.cellById(gridrowid, tag_rowId).getValue();
				tempCn = gridObj.cellById(gridrowid, cn_rowId).getValue();
				
				inputString += tempCn +'^'+ temploc + '^' + temptag + '|';
			}
		}
	}

	if (inputString == '') {
		Error_Details(message_operations[13]);
	}

	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	document.frmPendingPickSet.strOpt.value = 'Save';
	document.frmPendingPickSet.strInputString.value = inputString;
	document.frmPendingPickSet.action = '/gmPendingPickSet.do?method=savePendingPickSetStatus';
	fnStartProgress('Y');
	document.frmPendingPickSet.submit();
}
// This function is used to open the Picked details
function fnOpenPickDetails(action, count, refId) {
	var fs_rowId = gridObj.getColIndexById("h_field_sales");
	var enterFl = false;
	var field_name = '';
	var actionURL = '';

	if (action == '4127') { // Loaner
		enterFl = true;
		actionURL = "/gmPendingPickSet.do?method=loadLoanerSetsDtls&locationType="
				+ action
				+ "&pickLocationName="
				+ refId
				+ "&pickLocationID="
				+ refId;

	}else if(action =='4119'){ // In house Loaner
		enterFl = true;
		actionURL = "/gmPendingPickSet.do?method=loadInhouseLoanerSetsDtls&locationType="
				+ action
				+ "&pickLocationName="
				+ refId
				+ "&pickLocationID="
				+ refId;
	}else if (action == '4110' || action == '4112'|| action == '26240144') { // Consignment, inhouse consignment, Return Set
		
		enterFl = true;
		gridObj.forEachRow(function(rowId) {
			if (rowId == count) {
				field_name = gridObj.cellById(rowId, fs_rowId).getValue();
			}
		});
		actionURL = "/gmPendingPickSet.do?method=loadConsignedSetsDtls&locationType="
				+ action
				+ "&pickLocationName="
				+ field_name
				+ "&pickLocationID=" + refId;
	}
	if (enterFl) {
		document.frmPendingPickSet.action = actionURL;
		document.frmPendingPickSet.submit();
	}
}
// This function is used to redirect to previous screen (Pending Pick Dashboard) 
function fnClose(){
	document.frmPendingPickSet.action = "/gmPendingPickSet.do?method=loadPendingPickSets";
	document.frmPendingPickSet.submit();
}

function fnScanIdOnEnter()
{
	if(event.keyCode == 13)
	{ 
		var tagIdval = document.frmPendingPickSet.tagId.value;
		
		var msg1 = document.getElementById("message_1");
		var msg2 = document.getElementById("message_2");
		var verifyFl = false;

		gridObj.forEachRow(function(rowId) {
			tag_rowId_Val = gridObj.cellById(rowId, tag_rowId).getValue();
			if (tag_rowId_Val == tagIdval)
			{
				gridObj.cellById(rowId, check_rowId).setChecked(true);
				verifyFl = true;
			}
			if(!e) var e = window.event;
                   
			e.cancelBubble = true;
			e.returnValue = false;
          
			if (e.stopPropagation)
			{
				e.stopPropagation();
				e.preventDefault();
			}
		});
             
		document.frmPendingPickSet.tagId.value = '';
		document.frmPendingPickSet.tagId.focus();
              
		if(verifyFl){ 
			msg1.style.display='block';
			msg2.style.display='none';
		}else{
			msg1.style.display='none';
			msg2.style.display='block';
		}
	}
}

function fnScanIdOnBlur()
{
	var tagIdval = document.frmPendingPickSet.tagId.value;
	var msg1 = document.getElementById("message_1");
	var msg2 = document.getElementById("message_2");
	var verifyFl = false;
	
	if (tagIdval != '')
	{
		gridObj.forEachRow(function(rowId) {
			tag_rowId_Val = gridObj.cellById(rowId, tag_rowId).getValue();
			if (tag_rowId_Val == tagIdval)
			{
				gridObj.cellById(rowId, check_rowId).setChecked(true);
				verifyFl = true;  
			}
		});
              
		if(verifyFl){ 
			msg1.style.display='block';
			msg2.style.display='none';
		}else{
			msg1.style.display='none';
			msg2.style.display='block';
		}
	}
        
	document.frmPendingPickSet.tagId.value = '';
	document.frmPendingPickSet.tagId.focus();
}
