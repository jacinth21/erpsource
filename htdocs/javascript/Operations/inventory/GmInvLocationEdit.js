//this function used to load the grid
function fnOnPageLoad()
{
	gridObj = initGridWithDistributedParsing('dataGrid',objGridData);
	gridObj.attachEvent("onEditCell",doOnCellEdit);

	fnSetCursorPosition('locationID'); //to set default focus of location id (Common Script)
	
	var warehouseId = document.frmLocation.warehouseid.value;
	var locationId = document.frmLocation.locationID.value;
	var typeval = document.frmLocation.typeNum.value;
	var statusval = document.frmLocation.statusNum.value;
	var tabelObj = document.getElementById("tab_Disable");
	var savebtnObj = document.getElementById("savebtn_Disable");
	
	
	if( (typeval == 93320 || typeval == 93336) && objGridData.indexOf("cell") != '-1'){ // Pick Face - Bulk
		tabelObj.style.display='block';
	}else if(warehouseId == '0' && locationId == '' && typeval == '0' && statusval == '0'){
		tabelObj.style.display='block';
	}else if(warehouseId != '0' && locationId != '' && typeval == '0'){
		tabelObj.style.display='none';
		savebtnObj.style.display='none';
	}else if(typeval == 93320 || typeval == 93336 || typeval == 0){ // Pick Face - Bulk
		tabelObj.style.display='block';
	}else{
		tabelObj.style.display='none';
	}
}

//this function used , when edit the part then fetch the part details and qty
function doOnCellEdit(stage, rowId, cellInd, nValue, oValue) {	

	if(cellInd == 2)
	{ rowID = rowId;
		if (stage==2){
			var gridrows = gridObj.getAllRowIds();
			var gridrowsarr = gridrows.toString().split(",");
			for (var rowid = 0; rowid < gridrowsarr.length; rowid++){
				gridrowid = gridrowsarr[rowid];
				if(gridrowid != rowID){
					var pnum = gridObj.cellById(gridrowid,2).getValue();
					if(nValue == pnum){
						gridObj.cellById(rowID, 3).setValue(message_operations[6]);
						return true;		
					}
				}
			}
			var locId = document.frmLocation.locationID.value;
			var ajaxUrl = fnAjaxURLAppendCmpInfo('gmlocation.do?method=getPartNumDesc&partNum='+ encodeURIComponent(nValue)+'&ramdomId='+Math.random());
            dhtmlxAjax.get(ajaxUrl,fnAddPartSetDesc);
        }					
    }	
return true;
}
// this function used to show the part/set description
function fnAddPartSetDesc (loader) {	
var response =loader.xmlDoc.responseText;

   if(response=="Fail")
   {
   	//	Error_Clear();
	//	Error_Details(" There is no Part Number");				
	//	Error_Show();
	//	Error_Clear();
   	//	return;
   		gridObj.cellById(rowID, 3).setValue(message_operations[7]);	
   }
   else if(response != null && response.length>0)
   {
         	gridObj.cellById(rowID, 3).setValue(response);				 	
   }
}

//this function is used click on check box, then load
function fnRptReload()
{
	document.frmLocation.haction.value = "rptReload";
	document.frmLocation.submit();
}
//this function is used to add the gird rows
function addRow(){
	gridObj.addRow(gridObj.getUID(),'');
}
//this function is used to remove the selected rows
function removeSelectedRow(){
	var gridrows=gridObj.getSelectedRowId();
	if(gridrows!=undefined){
		var NameCellValue ;
		var gridrowsarr = gridrows.toString().split(",");
		var added_row;
		for (var rowid = 0; rowid < gridrowsarr.length; rowid++) {
			gridrowid = gridrowsarr[rowid];
			
			NameCellValue = gridObj.cellById(gridrowid, 0).getValue();
			added_row = gridObj.getRowAttribute(gridrowid,"row_added");
			gridObj.deleteRow(gridrowid);			
			voidRowIdStr = voidRowIdStr + gridrowid + ',';
		}
	}else{
		Error_Clear();
		Error_Details(message_operations[8]);				
		Error_Show();
		Error_Clear();
	}
}
//this function is used to form the input string and submit the form.
function fnSubmit()
{
		gridObj.editStop();// retun value from editor(if presents) to cell and closes editor
		var gridrowid;
		var void_flag;
		var inputString='';
		var gridrows =gridObj.getChangedRows(",");
		var tempval = '';

		fnValidateTxtFld('locationID',lblLocationID);
		fnValidateDropDn('typeNum',lblType);
		fnValidateDropDn('statusNum',lblStatus);
		
		var typeval = document.frmLocation.typeNum.value;
		var statusval = document.frmLocation.statusNum.value;		
		
		if(gridrows.length==0){
			inputString = "";
			
		}
		else
		{
			var gridrowsarr = gridrows.toString().split(",");
			var columnCount = gridObj.getColumnsNum();
			
			for (var rowid = 0; rowid < gridrowsarr.length; rowid++) {
				gridrowid = gridrowsarr[rowid];
				tempval = gridObj.cellById(gridrowid, 3).getValue();
				if(tempval == lblNoPartNum)
				{
					Error_Details(message_operations[9]);					
				}
				if(tempval == lblPartNumDuplicated)
				{
					Error_Details(message_operations[10]);					
				}
				
					void_flag = gridObj.getRowAttribute(gridrowid,"row_deleted");
					if(void_flag ==  undefined){
						void_flag='';
					}
					
					for(var col=0;col<columnCount;col++){
						if(col != 3 && col != 5 && col != 6){
						inputString+=gridObj.cellById(gridrowid, col).getValue()+'^';
						}
					}
					inputString+='|';
			}
			inputString+="$";
		
		
	 }
		var ruleTypeVal = document.frmLocation.ruleType.value;
		
		if(locMap != 'SetMap' && locMap != '')
		{
			if(typeval != 93320 && typeval != 93336 && typeval != '0') // Pick Face - Bulk
			{
				if(ruleTypeVal !='' && typeval != ruleTypeVal){
						Error_Details(message_operations[11]);
					}
			}
		}
			
		if (ErrorCount > 0)
		{
			Error_Show();
			Error_Clear();
			return false;
		}
		document.frmLocation.strOpt.value = 'save';
		document.frmLocation.strInputString.value = inputString;
		document.frmLocation.strVoidRowIds.value = voidRowIdStr;
		fnStartProgress('Y');
		document.frmLocation.submit();
}

// this function is used to load the reports.
function fnLoad()
{	
	var bFlag = document.frmLocation.bldFlag.value;
	if(bFlag == 'Y') {
		fnValidateDropDn('buildingID',lblBuilding); // added for PMT-33507 - validate building dropdown
	}
	fnValidateDropDn('warehouseid',lblWareHouse);
	fnValidateTxtFld('locationID',lblLocationID);

	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	//document.frmLocation.Btn_Submit.disabled = false;
	document.frmLocation.strOpt.value = '';
	var tempval = document.frmLocation.locationID.value;
	var wid = document.frmLocation.warehouseid.value;
	document.frmLocation.action="/gmlocation.do?method=fetchInvLocationByID&locationId ="+tempval + "&wid=" + wid;
	fnStartProgress();
	document.frmLocation.submit();
}
//this function is used to validate the types
function fnTypeChange(){
	var typeval = document.frmLocation.typeNum.value;
	if (locMap != 'SetMap' && locMap != '')
	{
		if(typeval != 93320 && typeval != 93336 && typeval !=0 && typeval !=hTypeVal ) // Pick Face - Bulk
		{
			Error_Details(message_operations[12]);
		}
	}
	if (locMap == 'SetMap' && locMap != '')
	{
		if(hTypeVal == 93501 || hTypeVal == 93502) // Consignment - Product Loaner
		{
			if(typeval != 93501 && typeval != 93502 && typeval !=0 && typeval !=hTypeVal )
			{
				Error_Details(message_operations[12]);
			}
		}
		else if(hTypeVal == 93320 || hTypeVal == 93336) // Pick Face - Bulk
		{
			if(typeval != 93501 && typeval != 93502 && typeval !=0 && typeval !=hTypeVal )
			{
				Error_Details(message_operations[12]);
			}
		}
		else if(hTypeVal == 93503 && typeval !=0 && typeval !=hTypeVal ) // Consignment - Product Loaner
		{
			Error_Details(message_operations[12]);
		}
	}
	
	if (ErrorCount > 0)
	{
		document.frmLocation.typeNum.value = hType;
		Error_Show();
		Error_Clear();
		return false;
	}
		
		//hType = hTypeVal;
}

// this function is used to validate the warehouse
function fnTypeWarehouse()
{
	if (locMap == 'SetMap' || locMap == '')
	{
		
		
		var warehouseId = document.frmLocation.warehouseid.value;
		if (warehouseId != hwarehouseid || SwitchUserFl == 'Y')
		{
			document.frmLocation.Btn_Submit.disabled = true;
		}else{
			document.frmLocation.Btn_Submit.disabled = false;
		}
	}
}

//this function is used to validate the LocationId
function fnValidateLocationId()
{
	if (locMap == 'SetMap' || locMap == '')
	{
		
		var locationId = document.frmLocation.locationID.value;
		if (locationId != locationid || SwitchUserFl == 'Y')
		{
			document.frmLocation.Btn_Submit.disabled = true;
		}else{
			document.frmLocation.Btn_Submit.disabled = false;
		}
	}
}