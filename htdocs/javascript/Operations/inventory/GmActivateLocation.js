//This function is used to save Activate/Inactivate Location
function fnSubmit() {
	fnValidateDropDn('strWareHouseId', lblWarehouse);
	fnValidateTxtFld('strLocationId', lblLocId);
	fnValidateDropDn('strStatusType', lblStatus);
	var strLocationIds = document.frmActionLocation.strLocationId.value;
	var strLocids = strLocationIds.split(',');
	var errcnt = 0;
	for(var i=0;i<strLocids.length;i++){
		if(strLocids[i] == ''){
			errcnt = errcnt+1;
		}
		var strChar = strLocids[i].charAt(0);
		if(strChar == ' '){
			errcnt = errcnt+1;
		}
	}
	if(errcnt > 0){
		Error_Details(message_operations[897]);
	}
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}

	fnStartProgress('Y');
	document.frmActionLocation.submit();

}
