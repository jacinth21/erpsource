var objGridData;
var msg;
//used to initialize grid
function initGridData(divRef, gridData) {
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.init();
	gObj.loadXMLString(gridData);
	return gObj;
}
// used to add empty row on page load
function fnOnPageLoad() {
	var footer = new Array();
	
		var setInitWidths = "48,200,200,200,48";
		var setColAlign = "center,left,left,left,center";
		var setColTypes = "ro,ed,coro,coro,ro";
		var setColSorting = "str,str,str,str,str";
		var setHeader = ["", "Part Number","Old Location", "New Location", ""];
		var setFilter = ["", "","", "",""];
		var setColIds = ",PNUM,OLOC,NLOC,";
		var enableTooltips = "true,true,true,true,true";
		var gridHeight ="600";
		// to set auto height
		if($(document).height() !=undefined || $(document).height() !=null){
			gridHeight = $(document).height() - 200 ;
		}

		var gridHeight = "";
		var footerStyles = [];
		var footerExportFL = true;
		pagination = "";
		format = ""
		var formatDecimal = "";
		var formatColumnIndex = "";
		
		document.getElementById("trDiv").style.display = "table-row";
		document.getElementById("trImageShow").style.display = "table-row";
		document.getElementById("buttonshow").style.display = "table-row";
		
		//split the functions to avoid multiple parameters passing in single function
		gridObj = initGridObject('dataGridDiv');
		gridObj = fnCustomTypes(gridObj, setHeader, setColIds, setInitWidths,
				setColAlign, setColTypes, setColSorting, enableTooltips, setFilter,
				400, pagination);
	  
		gridObj.copyBlockToClipboard();
	    gridObj.enableMultiline(false);	
		gridObj.enableEditEvents(false, true, true);

		var strDtlsJson = "";
		gridObj = loadDhtmlxGridByJsonData(gridObj,strDtlsJson);
	  
		//gridObj.attachEvent("onKeyPress", onKeyPressed);
		gridObj.attachEvent("onEditCell",doOnCellEdit);
	    gridObj.enableMultiselect(true);
		gridObj.enableBlockSelection(true);
		
		part_rowId = gridObj.getColIndexById("pnum");
		price_rowId = gridObj.getColIndexById("price");
		
		//Add Five rows when Onpagelaod
		if (strDtlsJson == "") {
			addRow();
			addRow();
			addRow();
		} 
	

}


// used to refresh the grid
function fnRefreshGrid() {
	fnOnPageLoad();

}
//used to submit bulk locations for save

function fnSubmit() {
	var gridrows = gridObj.getAllRowIds(",");
	var finalData = '';
	var strInputString = "";
	var v_string = "";
	var errString = "";
	var errLocString = "";
	var errPartnum = "";
	var errEmptyString = "";
	var errReqQty = "";
	var errEmptyLocation = "";

	var warehouseid = document.frmLocationTransfer.warehouseid.value;
	if (gridrows.length > 0) {
		var gridrowid;
		var gridrowsarr = gridrows.toString().split(",");
		var columnCount = gridObj.getColumnsNum();

		for (var rowid = 0; rowid < gridrowsarr.length; rowid++) {
			gridrowid = gridrowsarr[rowid];
			for (var col = 0; col < columnCount; col++) {

				var strpartNum = gridObj.cellById(gridrowid, 1).getValue();
				//Changing & retrieving the column values because of From loc,To loc column's are changed.
				var strFromLoc = gridObj.cellById(gridrowid, 2).getValue();
				var strToLoc = gridObj.cellById(gridrowid, 3).getValue();
				strFromLoc = strFromLoc.split('_');
				strFromLoc = strFromLoc[0];
				strToLoc = strToLoc.split('_');
				strToLoc = strToLoc[0];
				v_string = strpartNum + "^" + strFromLoc + "^" + strToLoc + "|";
			}

			if (strpartNum != '') {
				if (strpartNum == "Invalid Part#") {
					Error_Details(message_operations[383]);
					continue;
				}

				if (strToLoc == strFromLoc && strToLoc != ""
						&& strFromLoc != "") {
					errLocString = errLocString + "," + strpartNum;
				}

				if (strToLoc == '' || strFromLoc == '') {
					errEmptyLocation = errEmptyLocation + "," + strpartNum;
				}

			}

			if (strpartNum != '' || strToLoc != '' || strFromLoc != '') {
				strInputString = strInputString + v_string;
			}

		}

		if (errString != "") {
			Error_Details(Error_Details_Trans(message_operations[385],
					errString.substr(1, errString.length)));
		}
		if (errLocString != "") {
			Error_Details(Error_Details_Trans(message_operations[386],
					errLocString.substr(1, errLocString.length)));
		}

		if (errPartnum != "") {
			Error_Details(Error_Details_Trans(message_operations[387],
					errPartnum.substr(1, errPartnum.length)));
		}

		if (errEmptyLocation != "") {
			Error_Details(Error_Details_Trans(message_operations[388],
					errEmptyLocation.substr(1, errEmptyLocation.length)));
		}
		if (strInputString == "") {
			Error_Details(message_operations[389]);
		}
		if (ErrorCount > 0) {
			Error_Show();
			Error_Clear();
			return false;
		}

		
		document.frmLocationTransfer.v_str.value = strInputString;
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmLocationTransfer.do?method=saveReplenishment&v_str='
				+ encodeURIComponent(strInputString)
				+ '&warehouseid='
				+ warehouseid + '&ramdomId=' + Math.random());
		dhtmlxAjax.get(ajaxUrl, fnAddResult, gridrowsarr);

	}
}

function addRow() {
	gridObj.filterBy(0, "");// unfilters the grid while adding rows
	gridObj._f_rowsBuffer = null; // clears the cache
	var newRowId = gridObj.getUID();
	gridObj.addRow(newRowId, '');
}
// used to remove selected row
function removeSelectedRow() {

	var gridrows = gridObj.getSelectedRowId();
	if (gridrows != undefined) {
		var NameCellValue;
		var gridrowsarr = gridrows.toString().split(",");
		var added_row;
		for (var rowid = 0; rowid < gridrowsarr.length; rowid++) {
			gridrowid = gridrowsarr[rowid];
			gridObj.deleteRow(gridrowid);
		}
	} else {
		Error_Clear();
		Error_Details(message_operations[390]);
		Error_Show();
		Error_Clear();
	}
}
//used to call function while cell edit

function doOnCellEdit(stage, rowId, cellInd, nValue, oValue) {
	var warehouseid = document.frmLocationTransfer.warehouseid.value;
	fnValidateDropDn('warehouseid', lblWarehouse);

	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	rowID = rowId;
	if (cellInd == 1) {

		if (nValue != oValue) {
			gridObj.cellById(rowId, 1).setValue("");
			gridObj.cellById(rowId, 2).setValue("");
			gridObj.cellById(rowId, 3).setValue("");
			gridObj.cellById(rowId, 4).setValue("");

		}

		if (stage == 2 && nValue != '') {
			var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmLocationTransfer.do?method=fetchPartDetails&partNum='
					+ encodeURIComponent(nValue)
					+ '&warehouseid='
					+ warehouseid + '&ramdomId=' + Math.random());
			dhtmlxAjax.get(ajaxUrl, fnAddPartLoc);
			gridObj.cellById(rowId, 1).setValue(nValue);
			gridObj
					.cellById(rowId, 0)
					.setValue(
							"<img alt='"
									+ nValue
									+ "' onClick='fnFetchPartDetail("
									+ nValue
									+ ")' src='/images/location.png' width='15px' border='0'/>");
			fnMouseOver(gridObj);

		}

	}

	if ((cellInd == 1) && (oValue != undefined)) {
		var gridrows = gridObj.getAllRowIds(",");
		var gridrowsarr = gridrows.toString().split(",");
		var strPartNum = gridObj.cellById(rowId, 1).getValue();
		var strToLoc = gridObj.cellById(rowId, 3).getValue();
		var strFromLoc = gridObj.cellById(rowId, 2).getValue();
		for (var rowsid = 0; rowsid < gridrowsarr.length; rowsid++) {
			var gridrowsid = gridrowsarr[rowsid];
			var PartNum = gridObj.cellById(gridrowsid, 1).getValue();
			var ToLoc = gridObj.cellById(gridrowsid, 3).getValue();
			var FromLoc = gridObj.cellById(gridrowsid, 2).getValue();
			if (rowId != gridrowsid) {
				if (strPartNum == PartNum && strToLoc == ToLoc
						&& strFromLoc == FromLoc && strPartNum != ''
						&& strToLoc != '' && strFromLoc != '') {
					Error_Details(Error_Details_Trans(message_operations[391],
							strPartNum));
					Error_Show();
					Error_Clear();
					return false;
				}
			}
		}
	}

	return true;
}
//used to add available location for parts

function fnAddPartLoc(loader) {

	var response = loader.xmlDoc.responseText;
	var LocationIDArr = new Array();

	if (response == "") {
		gridObj.cellById(rowID, 0).setValue('');
		gridObj.cellById(rowID, 1).setValue("Invalid Part#");
		gridObj.getCustomCombo(rowID, 2).clear();
		gridObj.cellById(rowID, 3).setValue('');

	}

	else if (response != null && response.length > 0) {
		//Changing because of From loc,To loc column's are changed in vm & loading the from,to loc combo values. 

		var comboFromLocation = gridObj.getCustomCombo(rowID, 2);
		var comboToLocation = gridObj.getCustomCombo(rowID, 3);
		comboToLocation.clear();
		comboFromLocation.clear();
		var gridrowsarr = response.split("|");
		var idarray = "";
		for (i = 0; i < gridrowsarr.length; i++) {
			id = gridrowsarr[i];
			LocationIDArr[i] = new Array(2);
			LocationIDArr[i][0] = id.substring(id.indexOf(':') + 1, id.length); // the combo id is "loccode_locid"
			LocationIDArr[i][1] = id.substring(0, id.indexOf('_')); // the combo option value is "warehousenm:loccode"
		}
		for (i = 0; i < LocationIDArr.length; i++) {
			comboToLocation.put(LocationIDArr[i][0], LocationIDArr[i][1]);
			comboFromLocation.put(LocationIDArr[i][0], LocationIDArr[i][1]);
		}
		comboToLocation.save();
		comboFromLocation.save();
	}
}
//used to add result details after saving

function fnAddResult(loader) {
	var gridrows = gridObj.getAllRowIds(",");
	var response = loader.xmlDoc.responseText;
	
	if (response != null && response.length > 0) {
		var resultarr = response.split("|");

		var resimg = resultarr[1].split(",");
		var resmsg = resultarr[0].split(",");
		var LocationIDArr = new Array();

		if (gridrows.length > 0) {
			var gridrowid;
			var gridrowsarr = gridrows.toString().split(",");
			var columnCount = gridObj.getColumnsNum();

			for (var rowid = 0; rowid < gridrowsarr.length; rowid++) {
				gridrowid = gridrowsarr[rowid];
				if (resimg[rowid] == 'Y') {
					gridObj
							.cellById(gridrowid, 4)
							.setValue(
									"<img  title='"
											+ resmsg[rowid]
											+ "' width='15px' src='/images/success.gif'/ border='0'>");
					fnMouseOver(gridObj);
					
				} else if (resimg[rowid] == 'N') {
					gridObj
							.cellById(gridrowid, 4)
							.setValue(
									"<img  title='"
											+ resmsg[rowid]
											+ "' width='15px' src='/images/dhtmlxGrid/delete.gif'/ border='0'>");
					fnMouseOver(gridObj);
					
				}

			}
		}
	}

}

function parseNull(val) {
	if (val == 'null')
		return val = "";
	else
		return val;
}

//used to relocate location part 

function fnFetchPartDetail(id) {
	    fnStartProgress('Y');
		windowOpener("/gmlocation.do?method=fetchInvLocationPartMapping&strOpt=view&hshowFilters=yes&partNum="+encodeURIComponent(id),"ViewLocation","resizable=yes,scrollbars=yes,top=250,left=300,width=1070,height=600");
		fnStopProgress();

}

function fnMouseOver(gridObj) {
	gridObj.attachEvent("onMouseOver", function() {
		return false;
	});
}

//This Function Used to set custom types conditions in Grid
function fnCustomTypes(gObj, setHeader, setColIds, setInitWidths, setColAlign,
		setColTypes, setColSorting, enableTooltips, setFilter, gridHeight,
		pagination) {

	if (setHeader != undefined && setHeader.length > 0) {
		gObj.setHeader(setHeader);
		var colCount = setHeader.length;
	}

	if (setInitWidths != "")
		gObj.setInitWidths(setInitWidths);

	if (setColAlign != "")
		gObj.setColAlign(setColAlign);

	if (setColTypes != "")
		gObj.setColTypes(setColTypes);

	if (setColSorting != "")
		gObj.setColSorting(setColSorting);

	if (enableTooltips != "")
		gObj.enableTooltips(enableTooltips);

	if (setFilter != "")
		gObj.attachHeader(setFilter);

	if (setColIds != "") {
		gObj.setColumnIds(setColIds);
	}

	if (gridHeight != "")
		gObj.enableAutoHeight(true, gridHeight, true);
	else
		gObj.enableAutoHeight(true, 400, true);

	if (pagination == "Y") {
		gObj.enablePaging(true, 100, 10, "pagingArea", true);
		gObj.setPagingSkin("bricks");
	}

	return gObj;
}

