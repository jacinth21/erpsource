//This is function Load to Building, UserList showed based on Groups and Company
function fnload(){
	var strOpt = document.frmUserBuildingMapping.strOpt.value;
	
	var strGroupId = document.frmUserBuildingMapping.secGrp.value;
	fnValidateDropDn('secGrp',' Group ');
	
	var strCompanyId = document.frmUserBuildingMapping.companyId.value;
	fnValidateDropDn('companyId',' Company ');
	
	var strBuildingId = document.frmUserBuildingMapping.buildingId.value;
	fnValidateDropDn('buildingId',' Building ');
	
	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}
	document.frmUserBuildingMapping.action = '\gmUserBuildingMappingAction.do';
	fnStartProgress('Y');
	document.frmUserBuildingMapping.submit();
}

// This function used to save the users to Building, company and group 
function fnSubmit(){
    var strOpt = document.frmUserBuildingMapping.strOpt.value;
	var obj;
	var drpObj;
	var varLab = "";
	var str = "";
	var strDrp = "";
	var companyId = document.frmUserBuildingMapping.companyId.value;
	fnValidateDropDn('companyId',' Company ');
	if(strOpt == 'SECUSR'){
		obj = document.frmUserBuildingMapping.Chk_Grpuser;
	
		drpObj = 'secGrp';
		strDrp = "Security Group";
		varLab = "User List";
	}else{
		obj = document.frmUserBuildingMapping.Chk_Grpsec;
		drpObj = 'userList';
		strDrp = "User";
		varLab = "Security Group";
	}
	
	var buildingId = document.frmUserBuildingMapping.buildingId.value;
	fnValidateDropDn('buildingId',' Building ');
	
	fnValidation(obj,varLab);
	fnValidateDropDn(drpObj,strDrp);
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	for(var j=0;j<obj.length;j++){
		if(obj[j].checked){
			str += obj[j].value + ","; 
		}
	}
	document.frmUserBuildingMapping.hinputstring.value = str;
	document.frmUserBuildingMapping.haction.value = "save";
	document.frmUserBuildingMapping.action = '\gmUserBuildingMappingAction.do';
    fnStartProgress('Y');
	document.frmUserBuildingMapping.submit();
}


function fnSortList(){
    var list = $("ul"),
        origOrder = list.children();
        var i, checked = document.createDocumentFragment(),
            unchecked = document.createDocumentFragment();
        for (i = 0; i < origOrder.length; i++) {
            if (origOrder[i].getElementsByTagName("input")[0].checked) {
                checked.appendChild(origOrder[i]);
            } else {
                unchecked.appendChild(origOrder[i]);
            }
        }
        list.append(checked).append(unchecked);
}
function fnSortUlList(){
    var list = $("ol"),
    origOrder = list.children();
    var i, checked = document.createDocumentFragment(),
        unchecked = document.createDocumentFragment();
    for (i = 0; i < origOrder.length; i++) {
        if (origOrder[i].getElementsByTagName("input")[0].checked) {
            checked.appendChild(origOrder[i]);
        } else {
            unchecked.appendChild(origOrder[i]);
        }
    }
    list.append(checked).append(unchecked);
}

function fnSelectDefault(obj){
	strSelVal = document.frmUserBuildingMapping.strOpt.value;
	var buildId = document.frmUserBuildingMapping.buildingId.value;
	
	if (strSelVal== "SECUSR"){
	var obj = document.frmUserBuildingMapping.secGrp;
	}
	var companyId = document.frmUserBuildingMapping.companyId.value;
	
	if(obj.value!='0' && obj.value!= undefined && companyId!=undefined && companyId!='0'){

		var url = '\gmUserBuildingMappingAction.do?strOpt=' + strSelVal + '&optVal=' + obj.value +'&buildingId=' + buildId +'&companyId='+companyId+'&ramdomId='+Math.random();
		
		if (typeof XMLHttpRequest != "undefined") 
		{
		   req = new XMLHttpRequest();
		} 
		else if (window.ActiveXObject) 
		{
		   req = new ActiveXObject("Microsoft.XMLHTTP");
		}
		req.open("GET", url, true);
		req.onreadystatechange = callback;
		req.send(null);
   }
}
function callback() {
    if (req.readyState == 4) {
        if (req.status == 200) {
	       var strRetVal = req.responseText;
	       var strOpt_ = document.frmUserBuildingMapping.strOpt.value;
	       if(strOpt_ =='SECUSR'){
	    	   setMessageSecUser(strRetVal);
	       }else
	    	   setMessageUserSec(strRetVal);
        }        
    }
}

function onLoad(strOpt){
	if(strOpt =='SECUSR'){
		fnSelectDefault(document.frmUserBuildingMapping.secGrp);
	}
	else{
		fnSelectDefault(document.frmUserBuildingMapping.userList);	
	}
}

function fnValidation(obj,strVal)
{
	var objCheckCategoriesArr = obj;
	var categoryCheck = '0';
   for( var j=0; j< objCheckCategoriesArr.length; j++)
	{ 
		if(objCheckCategoriesArr[j].checked)	
		{
			categoryCheck = '1';	
		}
	}
   if( categoryCheck =='0')
	{
		 Error_Details('Please select <b>' + strVal + '</b>.');
	} 
}
function fnValidateDefGroup(obj,strVal){
	var objCheckCategoriesArr = obj;
	var categoryCheck = '0';
   for( var j=0; j< objCheckCategoriesArr.length; j++)
	{ 
		if(objCheckCategoriesArr[j].checked)	
		{
			if(objCheckCategoriesArr[j].value == strVal){
				categoryCheck = '1';
				break;
			}
		}
	}
   if( categoryCheck =='0')
	{
		 Error_Details('<b>Default Security Group</b> is not matched with the <b>Security Groups</b>.');
	}
}
function fnSelectAll(obj){
	var frmObj = document.frmUserBuildingMapping.Chk_Grpuser;
	var strOpt = document.frmUserBuildingMapping.strOpt.value;
	var srcObj = obj;
	for( var j=0; j< frmObj.length; j++)
	{ 
		if(srcObj.checked){
			frmObj[j].checked = true;
		}
		else{
			frmObj[j].checked = false;
		}
	}
	if(srcObj.checked== false){
		if(strOpt =='SECUSR')
			fnSelectDefault(document.frmUserBuildingMapping.secGrp);
		else
			fnSelectDefault(document.frmUserBuildingMapping.userList);
	}
}

function setMessageSecUser(strVal)
{
	var arrVal = strVal.split(',');
	var arrObjChkList;
	var arrObjOptList;
	var blFlg = false;
	var strOpt = document.frmUserBuildingMapping.strOpt.value;
	if(strOpt == 'SECUSR'){
		arrObjChkList = document.frmUserBuildingMapping.Chk_Grpuser;
	}else{
		arrObjChkList = document.frmUserBuildingMapping.Chk_Grpsec;
		arrObjOptList = document.frmUserBuildingMapping.Opt_Grp;
	}
	for(var k=0;k<arrObjChkList.length;k++){
			arrObjChkList[k].checked = false;
			if(arrObjOptList)
				arrObjOptList[k].checked = false;
	}
	for(var i=0;i<arrVal.length;i++){
		for(var j=0;j<arrObjChkList.length;j++){
			var defSplit = arrVal[i].split('^');	//		
			if(defSplit[0]==arrObjChkList[j].value){
				arrObjChkList[j].checked = true;				
				if(defSplit[1] == 'Y' && blFlg == false){
					for(var k=0;k<arrObjOptList.length;k++){
						if(arrObjOptList[k].value == defSplit[0])
							arrObjOptList[k].checked= true;
					}
					blFlg = true;
				}
			}
		}
	}
		fnSortList();
		fnSortUlList();
} 

function setMessageUserSec(strVal){
	
	var arrVal = strVal.split('@');
	var arrSecurityGrp =arrVal[0].split(',');
	var arrDefaultGrp = arrVal[1].split(',');
	var arrObjChkList;
	var arrObjOptList;
	var blFlg = false;
	var strOpt = document.frmMultiUserGroup.strOpt.value;
	
	
	arrObjChkList = document.frmMultiUserGroup.Chk_Grpsec;
	arrObjOptList = document.frmMultiUserGroup.Opt_Grp;
	
	for(var k=0;k<arrObjChkList.length;k++){
			arrObjChkList[k].checked = false;
			if(arrObjOptList)
				arrObjOptList[k].checked = false;
	}
	
	
	for(var i=0;i<arrSecurityGrp.length;i++){
		for(var j=0;j<arrObjChkList.length;j++){
			var defSplit = arrSecurityGrp[i].split('^');
			if(defSplit[0]==arrObjChkList[j].value){
				arrObjChkList[j].checked = true;
			}
		}
	}
	fnSortList();
    fnSortUlList();
} 
