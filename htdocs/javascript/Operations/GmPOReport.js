var gridObj;
function fnOnPageLoad()
{  
	var footer=new Array();
	footer[0]="";
	footer[1]="";
	footer[2]="";
	footer[3]="";
	footer[4]=message_operations[339];
	footer[5]="";
	footer[6]="<b>{#stat_total}<b>";
	footer[7]="";
	footer[8]="";
	footer[9]="";
	footer[10]="";
	gridObj = initGridWithDistributedParsing('dataGridDiv',objGridData,footer);
	gridObj.setColTypes("ro,ro,ro,ro,ro,ro,ron,ro,ro,ro");
	gridObj.enableTooltips("false,false,false,false,true,false,false,false,true,false");
	gridObj.attachHeader('#text_filter,,#select_filter,#text_filter,,,,#text_filter');
	gridObj.attachEvent("onFilterStart",fnCustomFilter);
}

function fnCallProject(val)
{
	document.frmVendor.hAction.value = 'PriceLoad';
  	document.frmVendor.submit();
}

function fnGo()
{
	if(document.frmVendor.hPOType.value == '0'){
		document.frmVendor.hPOType.value = '';
	}
	var objFromDt = document.frmVendor.Txt_FromDate;
	var objToDt = document.frmVendor.Txt_ToDate;
	var dateRange = dateDiff(objFromDt.value,objToDt.value,gCmpDateFmt);
	var fromDt = TRIM(document.frmVendor.Txt_FromDate.value);
	var poId = TRIM(document.frmVendor.Txt_POId.value);
	var toDt = TRIM(document.frmVendor.Txt_ToDate.value);
	if(!isNaN(dateRange) && dateRange < 0){
		 Error_Details(message_operations[304]);
	}
	//This validation is added when both po id and from date are empty (PC-3255)
	if (poId == '' && fromDt == '' ){
			Error_Details(message_operations[880]);
	}
	//This validation is added when both po id and To date are empty (PC-3255)
	if (poId == '' && toDt == '' )
	{
		Error_Details(message_operations[881]);
	}
	
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	document.frmVendor.hAction.value = 'Go';
	fnStartProgress('Y');
	document.frmVendor.submit();
}

function fnCallPO(po,vid)
{
	document.frmVendor.action = gStrServletPath+ "/GmPOServlet";	
	document.frmVendor.hAction.value = 'ViewPO';
	document.frmVendor.hPOId.value = po;
	document.frmVendor.hVenId.value = vid;
	document.frmVendor.submit();
	//windowOpener("<%=strServletPath%>/GmPOServlet?hAction=ViewPO&hPOId="+po+"&hVenId="+vid,"PO","resizable=yes,scrollbar=yes,top=150,left=50,width=750,height=600");
}

function fnCallEdit()
{
	
	var obj = document.frmVendor.Cbo_Action;
	var len = document.frmVendor.hLen.value;
	var cnt = 0;
	
	if ( len == 1)
	{
		
		document.frmVendor.hPOId.value = document.frmVendor.rad.value;
	}
	else if ( len >1)
	{
		
		var arr = document.frmVendor.rad;
		for (var i=0;i<arr.length ;i++ )
		{
			if (arr[i].checked == true)
			{
				document.frmVendor.hPOId.value = arr[i].value;
				cnt++;
				break;
			}

		}
		if (cnt == 0)
		{
			
			alert(message_operations[340]);
			return;
		}
	}
	
	if (obj.value == '107404')
	{	
		
		document.frmVendor.action =gStrServletPath+ "/GmPOServlet";	
		document.frmVendor.hAction.value = "EditPO";
	}
	else if (obj.value == '107405')
	{
		
		document.frmVendor.hTxnId.value = document.frmVendor.hPOId.value
		document.frmVendor.action = gStrServletPath+ "/GmCommonCancelServlet";
		document.frmVendor.hAction.value = "Load";
	}
	else if (obj.value == '107406')
	{
		var poID = document.frmVendor.hPOId.value;
		document.frmVendor.action = "/gmVendorCommit.do?method=loadVendorCommitEdit&strVendorId="+poID;
	}
	fnStartProgress('Y');
	document.frmVendor.submit();
}

function fnOpenLog(id)
{
	windowOpener("/GmCommonLogServlet?hType=1203&hID="+id,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
}
function fnCustomFilter(ord,data){
	
	/* This custom filter to avoid & content column filter issue. 
	 * Because the actual text for & is '&amp;'
	 * Ex. Let us assume xml as <cell><a ..>B&amp;G Manufacturing Co ,Inc</a></cell>.  If user try to filter 'B&G' text then it wont show . B&;G Manufacturing Co ,Inc*/
			if(data[2]!= ''){
				var original=data[2];
				original = original.toLowerCase();
				data[2]=function(value){
							if (value.toString().replace(/&amp;/g,"&").toLowerCase().indexOf(original)!=-1)
								return true;
							return false;
							};
			}
				
		gridObj.filterBy(ord,data);
		return false;
	}
	
//This function is used to remove from date when PO ID is entered
function fnRemoveFromDt(val){
	if(TRIM(val) != ''){
		document.frmVendor.Txt_FromDate.value = '';
	}
}
