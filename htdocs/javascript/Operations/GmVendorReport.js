function fnCallEdit(val)
{
	document.frmVendor.hId.value = val;
	document.frmVendor.action = strServletPath +"/GmVendorServlet?hAction=EditLoad";
	document.frmVendor.submit();
}

function fnReload()
{
	var objType = document.frmVendor.Chk_GrpType;
	var objCat = document.frmVendor.Chk_GrpCat;
	var objStat = document.frmVendor.Chk_GrpStat;

	var catlen = objCat.length;
	var typelen = objType.length;
	var statlen = objStat.length;

	var catstr = '';
	var typestr = '';
	var statstr = '';
	
	var temp = '';
	for (var i=0;i<catlen;i++)
	{
		if (objCat[i].checked == true)
		{
			catstr = catstr + objCat[i].value + ',';
		}
	}
	
	for (var i=0;i<typelen;i++)
	{
		if (objType[i].checked == true)
		{
			typestr = typestr + objType[i].value + ',';
		}
	}
	
	for (var i=0;i<statlen;i++)
	{
		if (objStat[i].checked == true)
		{
			statstr = statstr + objStat[i].value + ',';
		}
	}
	
	if(typestr != '')
	{
		document.frmVendor.hTypeIds.value = typestr.substr(0,typestr.length-1);
	}
	if(catstr != '')
	{
		document.frmVendor.hCatIds.value = catstr.substr(0,catstr.length-1);
	}
	if(statstr != '')
	{	
		document.frmVendor.hStatIds.value = statstr.substr(0,statstr.length-1);
	}
	fnStartProgress('Y');
	document.frmVendor.submit();
}

//This function used to download the grid data
function fnDownloadXLS() {
	gridObj.toExcel('/phpapp/excel/generate.php');
}
function fnLoad()
{
	var objgrp = '';
	
	if (typeids != '')
	{
		objgrp = document.frmVendor.Chk_GrpType;
		fnCheckSelections(typeids,objgrp);
	}
	
	if (catids != '')
	{
		objgrp = document.frmVendor.Chk_GrpCat;
		fnCheckSelections(catids,objgrp);
	}	
	
	if (status != '')
	{
		objgrp = document.frmVendor.Chk_GrpStat;
		fnCheckSelections(status,objgrp);
	}	
	if(objGridData.value != ''){ 
		gridObj = initGridData('dataGridDiv',objGridData);
	}	
}

// Marks the checkboxes as 'checked'
function fnCheckSelections(val,objgrp)
{
	var valobj = val.split(",");
	var sval = '';
	var arrlen = objgrp.length;
	
	if (arrlen > 0)
	{
		for (var j=0;j< valobj.length;j++ )
		{
			sval = valobj[j];
			for (var i=0;i< arrlen;i++ )
			{
				if (objgrp[i].value == sval)
				{
					objgrp[i].checked = true;
					break;
				}
			}
		}
	}
	else
	{
		objgrp.checked = true;
	}
}

  function initGridData(divRef,gridData)
  {
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);
	gObj.enableTooltips("true,true,true,true,true,true,true");
	gObj.attachHeader("#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#select_filter"
			 +",#select_filter,#text_filter,#text_filter,#select_filter,#select_filter,#rspan,#select_filter,#select_filter,#select_filter");//PMT-51211 Display Purchasing agent in the Vendor Report
	gObj.init();
	gObj.loadXMLString(gridData);	
	return gObj;	
}
   
function fnHistoryLoad (vendorId){
	windowOpener("/GmPageControllerServlet?strPgToLoad=/gmAuditTrail.do&auditId=4000765&txnId="+ vendorId, "","resizable=yes,scrollbars=yes,top=250,left=300,width=785,height=300,status=1");
}