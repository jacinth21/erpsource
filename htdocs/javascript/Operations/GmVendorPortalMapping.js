var gridObj;
//This Function used remove the vendor Portal Map
function fnVoid(partylinkid){
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmVendorPortalMapping.do?method=voidVendorPortalMap&partylinkid='+partylinkid+'&ramdomId='+new Date().getTime());
	dhtmlxAjax.get(ajaxUrl,fnLoadLotAjax);
}

//This Function used to save the vendor Portal Map
function fnSave(){
	var frompartyid=document.frmVendorPortalMap.fromVendorId.value;
	var val = document.frmVendorPortalMap.toPartyID.value;
    var tmp='';
    for (var i=0;i<partyAlLen;i++)
	{
    	arr = eval("RepArr"+i);
    	var did = arr;	//
    	if (did == val)
		{
    		tmp ='Y';
		}
	}
    if(tmp == 'Y'){
    	Error_Details("Already User is Exist");
	    	if (ErrorCount > 0)
	    	{
	    			Error_Show();
	    			Error_Clear();
	    			return false;
	    	}
    	} else{
    		
    	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmVendorPortalMapping.do?method=saveVendorPortalMap&toPartyID='+val+'&fromVendorId='+frompartyid+'&ramdomId='+new Date().getTime());
    	dhtmlxAjax.get(ajaxUrl,fnLoadLotAjax);
    }
}

//This Function used close the popup window
function fnClose() {
	window.close();
}

//This function used reload page after save and remove vendor details
function fnLoadLotAjax (loader){
	var response = loader.xmlDoc.responseText;
	 window.location.reload();
}
//This function is used for on Page Load
function fnOnPageLoad(){
	gridObj = initGridData('dataGridDiv',objGridData);
}

//this function is used to initiate grid data	
function initGridData(divRef,objGridData){
	
		var gObj = new dhtmlXGridObject(divRef);
		gObj.setSkin("dhx_skyblue");
		gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
		gObj.attachHeader('#rspan,#select_filter,#select_filter,#select_filter,#text_filter,#select_filter');
		gObj.init();
		gObj.loadXMLString(objGridData);	
		return gObj;
		

	}

//Export excel
function fnExportExcel() {
	gridObj.setColumnHidden(0,true);
	gridObj.toExcel('/phpapp/excel/generate.php');
	gridObj.setColumnHidden(0,false);
}
