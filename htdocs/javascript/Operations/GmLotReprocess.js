var ajax = new Array();
var index; 

function fnInHousePicSlip(val)
{
	windowOpener("/GmConsignInHouseServlet?hAction=PicSlip&hConsignId="+val,"SetPic","resizable=yes,scrollbars=yes,top=150,left=150,width=750,height=500");
}
function fnPicSlip(val,type,refid){
	windowOpener("/GmConsignInHouseServlet?hAction=PicSlip&hConsignId="+val+"&ruleSource="+type+"&refId="+refid,"SetPic","resizable=yes,scrollbars=yes,top=150,left=150,width=750,height=500");
}







function fnPrintVer(val)
{
	windowOpener("/GmInHouseSetServlet?hAction=ViewPrint&hConsignId="+val+"&hType="+type,"LoanPrint","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}



function fnPrint(printOpt){
	document.getElementById("successMsg").innerHTML ="";
	if(cbo_type =="LNRECONFIG" || cbo_type == "PROCESSRETURN"){
     var ajaxUrl = fnAjaxURLAppendCmpInfo("/gmReconfigLoaner.do?method=loadReconfgPrintDtl&strConsignId="+val+"&strSetId="+setid+"&ramdomId="+Math.random());
	 dhtmlxAjax.get(ajaxUrl, function(loader){	
	var response = loader.xmlDoc.responseText;
});
 
}

if(printOpt == 'manually'){
	fnPrintPPW(printOpt);
		fnPrintVersion(printOpt);
		fnOpenDoc(printOpt);
	}
	
if(reconfigLoanFl != 'Y'){
	var defaultprint = executeCommands();
	if(defaultprint == ''){
		Error_Details(message_operations[174]);
	}
	if (ErrorCount > 0){		
		Error_Show();
		Error_Clear();
		return false;
	}
	}
	if(printOpt != 'manually'){
		if(reconfigLoanFl == 'Y'){
		fnPDFPrintPPW(printOpt);
		fnPDFPrintVersion(printOpt);
		fnPDFOpenDoc(printOpt);
		document.getElementById("successMsg").innerHTML ="Print paper work is scheduled.";
	}else{
	fnPrintPPW(printOpt);
		setTimeout("popupWin.close();",3000);
		setTimeout("fnPrintVersion("+printOpt+");",3100);
		setTimeout("popupWin.close();",6000);
		setTimeout("fnOpenDoc("+printOpt+");",6100);
		}
		
	}/*else{
		fnPrintPPW(printOpt);
		fnPrintVersion(printOpt);
		fnOpenDoc(printOpt);
	}*/
}

function fnPrintPPW(hopt){
	var comments = document.all.Txt_LogReason.value;
	windowOpener("/GmInHouseSetServlet?hAction=PrintLoanerPPW&hConsignId="+val+"&hOpt="+hopt+"&Txt_LogReason="+comments,"PrintLoaner","resizable=yes,scrollbars=yes,top=10,left=150,width=800,height=600");		
}
function fnPrintVersion(hopt){
	windowOpener("/GmInHouseSetServlet?hAction=ViewPrint&hConsignId="+val+"&hType="+type+"&hOpt="+hopt,"LoanPrint","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnOpenDoc(hopt){
	var setid = document.getElementById("hSetId").value;
		windowOpener("/GmInHouseSetServlet?hAction=InsViewer&hSetId="+setid+"&hType="+type+"&hOpt="+hopt,"OpenDocument","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}
function executeCommands(){
	var oShell = new ActiveXObject("WScript.Shell");
	sRegVal = 'HKEY_CURRENT_USER\\Software\\Microsoft\\Windows NT\\CurrentVersion\\Windows\\Device';
	sDefault = oShell.RegRead(sRegVal);
	return sDefault;
}

//--below function is used to fetch control number in loaner process check for japan company 
function fnPartLotAjax(rowId,partnum){
	var ajaxUrl = fnAjaxURLAppendCmpInfo("/gmSetLotMaster.do?method=loadSetPartLotDtl&txnId="+val+"&partNum="+encodeURIComponent(partnum)+"&ramdomId="+Math.random());
	dhtmlxAjax.get(ajaxUrl, function(loader){	
		var resJSONObj = $.parseJSON(loader.xmlDoc.responseText);
		var obj=document.getElementById("Cbo_UseLot"+rowId);
		fnPopulateOptions(obj, resJSONObj, "CNUM", "CNUMQTY", "CQTY");
	});
}

//Open Back order report from Process Transaction from Process Check screen
function fnOpenLoanerItemsBORpt(refId)
{
	var cnType = '';
	
	//4127 - Loaner ,50266 - Loaner Items Back Order,
	//4119 - InHouse Loaner ,50269 - In House Loaner items
	if(type == 4127) {
		cnType = '50266';
	}else if(type == 4119) {
		cnType = '50269';
	}
	
	location.href  ="/GmOrderItemServlet?Cbo_Type="+cnType+"&hOpt=ProcBO&hFrom=PROCESS_TXN_SCN&REFID="+refId+"&"+fnAppendCompanyInfo();
}
//Ajax call to validate the Lot numner in New Lot# field
function fnValidateLot(hcnt,pnum){

	var cnumObj = eval("document.frmVendor.Txt_New_Lot"+hcnt); 
	var cnum = cnumObj.value;
	
	cnum=cnum.replace(/(^\s+|\s+$)/g, '');

	if(pnum != '' && cnum != ''){
		
		var ajaxUrl = fnAjaxURLAppendCmpInfo("/gmSetLotMaster.do?method=validateLotNumber&ctrlNum="+encodeURIComponent(cnum)+"&partNum="+encodeURIComponent(pnum)+"&ramdomId="+Math.random());
		dhtmlxAjax.get(ajaxUrl,function(loader)
				{
					var response = loader.xmlDoc.responseText;
					if(response == ''){
						document.getElementById('DivShowTagAvl'+hcnt).style.display='inline-block';
						document.getElementById('DivShowTagNotExists'+hcnt).style.display='none';
						document.getElementById('hInvalidLotFl'+hcnt).value = '';

					}else{
						document.getElementById('DivShowTagNotExists'+hcnt).style.display='inline-block';
						document.getElementById('DivShowTagAvl'+hcnt).style.display='none';
						document.getElementById('hInvalidLotFl'+hcnt).value = 'Y';
											}
				});
		
	}
}
//To remove the image
function fnToggleLot(hcnt){
	document.getElementById('DivShowTagAvl'+hcnt).style.display='none';
	document.getElementById('DivShowTagNotExists'+hcnt).style.display='none';
}

var strLocVal = {};	
var gridRecLotsObj='';
var misQty = 1;
var partDesc = '';
var expiryDt = '';
var strMsgJson = '';
var partnum = '';
var request_id = '';
var shipped_date = '';
var strMsg = '';
var strMsgJson = '';
var strValJson = '';
var newArr = '';
var arr = '';
//var val = '';
var strVal = '';
var h_part_num = '';
var map = new Map();
var arrValue = new Array();
var valOfArr = new Array();
var addRowId = 1;
var misDupQty = 1;
var error ='';
var parttype_col;
var cntlNoTitle='';
var expiryQty = 0;
var removeFl = 'N';
var expiryFl = '';
var valMsgFl = '';
var excessPart = 'Additional Parts:';
var reccount=0;
var expiryDate = '';
/* var selRowID=''; */

var removeImg = '&nbsp;<a href="javascript:fnRemove()"><img alt="Click to view all comments" border="0" src="/images/dhtmlxGrid/delete.gif"/></a>';
	function fnUpperCase(obj) {
		obj.value = obj.value.toUpperCase();
	}
 
	function fnAddRow(obj){	
		// When do scan in bar code that time will call this function.	
			if(event.keyCode == 13 || obj.name == 'Btn_Cno_Add')
			{	 
				var rtVal;
				var validMsgFl;
				validMsgFl = fnValidateControl();
				if(validMsgFl){
					rtVal = fnAddControlNo();
				}
				
				// used below codes to stop reloading the page after scanning the id
				if(!e) var e = window.event;
			
			     e.cancelBubble = true;
			     e.returnValue = false;	
			
			     if (e.stopPropagation) {
			             e.stopPropagation();
			             e.preventDefault();
			     }
			}
	}
	
	function fnValidateControl(){
		var partcno = TRIM(document.getElementById("controlNo").value);
		// If the text box 'Scan/Enter control' empty, show validation
		if(partcno == ""){
			Error_Details(message_operations[827]);
			Error_Show();
		    Error_Clear();
		    return false;
		}
		var msg = '';
		var pnum_col = '';
		var pnumProdMaterial = '';
		var txnType = '';
		var partNumber = '';
		var invPartNo = '';
		var count = 0;
		var pnum_col = '';			
		var qty_col = '';
		var cnum_col = '';
		var recPartNum = '';
		var qty = '';
		var controlNum = '';		
		var receivedLotError = 0;
		if( expiryFl != undefined ){
		 expiryFl = '';
		}
		indexof = partcno.indexOf("^");
		cntlindexof = partcno.indexOf("(10)"); // get the index of '(10)'
			
		if(indexof != -1){// While scanning only format will be "partno^controlno"
			var cnumarr= partcno.split("^");
			partnum = cnumarr[0]; // get the characters before '^'
			
			if(cntlindexof != -1){
				var cnumArray = partcno.split("(10)");
				controlNo = cnumArray[1];	// //Getting the remaining character, after "(10)"		
			}else{
				controlNo = cnumarr[1];
			}
		}else if(cntlindexof != -1){
				var cnumArray = partcno.split("(10)");
				controlNo = cnumArray[1];	// //Getting the remaining character, after "(10)"	
				partnum = '';
		}else{
			controlNo = partcno;
			partnum = '';
		}
		controlNo = controlNo.toUpperCase();
	       // While scanning part no with control no it will validate below
		if(controlNo == ''){ 
			Error_Details(Error_Details_Trans(message[10017],controlNo));
			isError = true;
		}else{		
			
			
			var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmLoanerLotReprocess.do?method=controlNoValidation&ctrlNum='+controlNo+'&partNum='+encodeURIComponent(partnum)+'&log='+msg+'&ramdomId=' + Math.random());
			var loader;
			var response;
			var strControlNoVal;
			loader = dhtmlxAjax.getSync(ajaxUrl);
			response = loader.xmlDoc.status;
			if(response == 200){  //On success status parsing the response values.
				var strJsonString = loader.xmlDoc.responseText;
				strJsonString = "["+strJsonString+"]";
				JSON_Array_obj = JSON.parse(strJsonString);
					$.each(JSON_Array_obj, function(index,jsonObject) {
						controlNo = jsonObject.CONTROLNUM;
						msg = jsonObject.ERRORMSG;
						partDesc = jsonObject.PARTDESC;
						expiryDt = jsonObject.EXPIRYDT;
						partNum = jsonObject.PARTNUM;
					});
			}else{
						Error_Details(Error_Details_Trans(message_operations[842],controlNo));
						beep();
						Error_Show();
						Error_Clear();
						return false;
			}			
			if(msg != null && msg != ''){
				Error_Details(Error_Details_Trans(message_operations[842],controlNo));
				beep();
				Error_Show();
			    Error_Clear();
			    return false;
			}else{
				strControlNoVal = partNum+'^'+controlNo+'^'+expiryDt+'^'+partDesc;
				document.getElementById("controlNo").value = strControlNoVal;
				partnum = partNum;
			}
			if(partnum != ''){
			//If the part number is not available in the set, throw validation
			gridrows = gridObj.getAllRowIds(",");
		    gridrowsarr = gridrows.toString().split(",");
		    arrLen = gridrowsarr.length;
			for (var i = 0; i < arrLen; i++) {
				partNumber = gridrowsarr[i];
				if(partNumber == partnum){
					count++;
				}
			}
			if(count == 0){
				Error_Details(Error_Details_Trans(message_operations[830],partnum));
				beep();
				Error_Show();
			    Error_Clear();		
			    return false
			}
			
			// should not allow to scan non sterile part
			parttype_col = gridObj.getColIndexById("parttype");
			var partType = gridObj.cellById(partnum, parttype_col).getValue();
			if(partType == '4031'){   //non-sterile part should not scan in control no field
				Error_Details(Error_Details_Trans(message_operations[839],partnum));
				beep();
				Error_Show();
			    Error_Clear();
			    return false;
			}
			
			/*
		   	// validate duplicate part number in received lots
			if(gridRecLotsObj != null && gridRecLotsObj != undefined && gridRecLotsObj != ''){
				pnum_col = gridRecLotsObj.getColIndexById("partnum");
										
			    gridrows = gridRecLotsObj.getAllRowIds(",");
			    gridrowsarr = gridrows.toString().split(",");
			    arrLen = gridrowsarr.length;
				for (var i = 0; i < arrLen; i++) {
					rowId = gridrowsarr[i];
					recPartNum = gridRecLotsObj.cellById(rowId, pnum_col).getValue();	
					if (recPartNum == partnum){
						error = 1;
					}else{
						error = 0;
					}					
				}
							
				if(error == 1){
					Error_Details(Error_Details_Trans(message_operations[833],partnum));
				}
				if (ErrorCount > 0 ){
					Error_Show();
				    Error_Clear();
				   // return false;
				}
			}*/
						 

			//Should not scan the part number if the missing qty is 0
			var missqty_col = gridObj.getColIndexById("missQty");
			var missQty = gridObj.cellById(partnum, missqty_col).getValue();
			if(missQty != ''){
				if(missQty <= 0){
					beep();
					if (confirm(Error_Details_Trans(message_operations[831],partnum))) {
				    fnAddExcessParts(partNum,controlNo);
				    }
					fnClearDiv();
				    return false;
				}
			}	
			//control number expiry date should above 30 and 14days 
			var expiryQty_col = gridObj.getColIndexById("expiryQty");
			expiryQty = gridObj.cellById(partnum, expiryQty_col).getValue();
			if(expiryDt != ''){
				diff = dateDiff(todaysDate, expiryDt, format);
				if(diff >=15 && diff <= 30){
					expiryFl = 'N';
					gridObj.setRowTextStyle(partnum, "background-color:#ffcccc;color:red;border:1px solid gray;");
				}
				if(diff <= 14){					
					expiryQty = Number(expiryQty)+1;
					gridObj.cells(partnum,expiryQty_col).setValue(expiryQty);
					var valid_col = gridObj.getColIndexById("validation");
					gridObj.setRowTextStyle(partnum, "background-color:#ffcccc;color:red;border:1px solid gray;");
					expiryFl = 'Y';
					fnValidateTrans(partnum); 
				}
				if(diff > 30 && (expiryQty == '' || expiryQty == null)){
					expiryFl = '';
				}
			}
			}
		
//			Error_Details(message_operations[840]);
		if (ErrorCount > 0 ){
			beep();
			Error_Show();
		    Error_Clear();
		    return false;
		    
		}
		return true;
	}
}
	function fnAddControlNo() {
		
		if(gridRecLotsObj != null && gridRecLotsObj != undefined && gridRecLotsObj != ''){
			addRow();
		}else{		
		var partcno = TRIM(document.getElementById("controlNo").value);	
		indexof = partcno.indexOf("^");
		cntlindexof = partcno.indexOf("(10)"); // get the index of '(10)'

		if(indexof != -1){// While scanning only format will be "partno^controlno"
			var cnumarr= partcno.split("^");
			partnum = cnumarr[0]; // get the characters before '^'
			expiryDt = cnumarr[2];
			partDesc = cnumarr[3];
			
			if(cntlindexof != -1){
				var cnumArray = partcno.split("(10)");
				controlNo = cnumArray[1];	// //Getting the remaining character, after "(10)"		
			}else{
				controlNo = cnumarr[1];
			}
		}else{
			controlNo = partcno;
		}
		controlNo = controlNo.toUpperCase();
			strLocVal.partnum = partnum;
			strLocVal.controlNo = controlNo;
			strLocVal.partDesc = partDesc;
			strLocVal.misQty = misQty;
			strLocVal.expiryDt = expiryDt;
			strMsgJson = JSON.stringify(strLocVal);
			strMsgJson = '[' + strMsgJson + ']';
			fnClearDiv();
			if (strMsgJson != "") {
				var rows = [], i = 0;
				var JSON_Array_obj = "";
				JSON_Array_obj = JSON.parse(strMsgJson);
				$.each(JSON_Array_obj, function(index, jsonObject) {
					partnum = jsonObject.partnum;
					controlNo = jsonObject.controlNo;
					partDesc = jsonObject.partDesc;
					misQty = jsonObject.misQty;
					expiryDt = jsonObject.expiryDt;
					//default array
					rows[i] = {};
					rows[i].id = i;
					rows[i].data = [];
					rows[i].data[0] = removeImg;
					rows[i].data[1] = partnum;
					rows[i].data[2] = controlNo;
					rows[i].data[3] = partDesc;
					rows[i].data[4] = misQty;
					rows[i].data[5] = expiryDt;
					i++;

				});
				var setInitWidths = "35,100,100,200,80,80";
				var setColAlign = "center,left,left,left,left,left";
				var setColTypes = "ro,ro,ro,ro,ro,ro";
				var setColSorting = "str,str,str,str,str,date";
				var removeAll = '&nbsp;<a href="javascript:fnRemoveAll()"><img alt="Click to view all comments" border="0" src="/images/dhtmlxGrid/delete.gif"/></a>';
				var setHeader = [ removeAll,"Part Number", "Control Number", "Part Desc", "Received Qty", "Expiry Dt" ];
				var setColIds ="remove,partnum,contlNum,desc,qty,expiryDate";
				var setFilter = "";
				var enableTooltips = "true,true,true,true,true,true";
				var footerArry = [];
				var gridHeight = "";
				var footerStyles = [];
				var footerExportFL = true;
				var dataHost = {};
				var pagination = "";
				dataHost.rows = rows;
				document.getElementById("dataInfo").style.display = 'block';
				gridRecLotsObj = loadDHTMLXGrid('dataInfo', gridHeight, dataHost,
						setHeader, setInitWidths, setColAlign, setColTypes,
						setColSorting, setColIds, enableTooltips, setFilter,
						footerArry, footerStyles, footerExportFL, pagination);
				h_part_num = gridRecLotsObj.getColIndexById("partnum");
				//gridRecLotsObj.setColumnHidden(5, true);
				fnUpdateMissingQty(partnum,misQty);
				return false;
			} else {
				document.getElementById("dataInfo").style.display = 'none';
				return false;
			}
	}
		return false;
		
	}
	
function addRow(){
	   
		var partcno = TRIM(document.getElementById("controlNo").value);
		indexof = partcno.indexOf("^");
		cntlindexof = partcno.indexOf("(10)"); // get the index of '(10)'

		if(indexof != -1){// While scanning only format will be "partno^controlno"
			var cnumarr= partcno.split("^");
			partnum = cnumarr[0]; // get the characters before '^'
			expiryDt = cnumarr[2];
			partDesc = cnumarr[3];
			
			if(cntlindexof != -1){
				var cnumArray = partcno.split("(10)");
				controlNo = cnumArray[1];	// //Getting the remaining character, after "(10)"		
			}else{
				controlNo = cnumarr[1];
			}
			
		}else{
			controlNo = partcno;
		}
		controlNo = controlNo.toUpperCase();
			strLocVal.partnum = partnum;
			strLocVal.controlNo = controlNo;
			strLocVal.restext = partDesc;
			strLocVal.misQty = misQty;
			strLocVal.expiryDt = expiryDt;
			strMsgJson = JSON.stringify(strLocVal);
			strMsgJson = '[' + strMsgJson + ']';
			fnClearDiv();
			
			if (strMsgJson != "") {
				var rows = [], i = 0;
				var JSON_Array_obj = "";
				JSON_Array_obj = JSON.parse(strMsgJson);
				$.each(JSON_Array_obj, function(index, jsonObject) {
					partnum = jsonObject.partnum;
					controlNo = jsonObject.controlNo;
					restext = jsonObject.restext;
					misQty = jsonObject.misQty;
					expiryDt = jsonObject.expiryDt;
			});		
			}
			var rowId=gridRecLotsObj.getUID();                  //generates an unique id
		    var pos = gridRecLotsObj.getRowsNum();  //gets the number of rows in grid
	        gridRecLotsObj.addRow(rowId,[removeImg,partnum,controlNo,restext,misQty,expiryDt],pos);  //adds a new row   
			fnUpdateMissingQty(partnum,misQty);
			return false;
	}
	
    //this function is used to perform action when a cell is editing
    function fnValidateTrans(rowId){
    	
	var pnum_col = gridObj.getColIndexById("partnum");
	var desc_col = gridObj.getColIndexById("desc");
	var qty_col = gridObj.getColIndexById("qty");
	var missqty_col = gridObj.getColIndexById("missQty");
	var setlistqty_col = gridObj.getColIndexById("setListQty");
	var self_col = gridObj.getColIndexById("self");
	var selfAvail_col = gridObj.getColIndexById("shelfAvail");
	var bulk_col = gridObj.getColIndexById("bulk");
	var bulkAvail_col = gridObj.getColIndexById("bulkAvail");
	var backOrder_col = gridObj.getColIndexById("backOrder");
	var shelf_col = gridObj.getColIndexById("shelf");
	var scrap_col = gridObj.getColIndexById("scrap");
	var quarantine_col = gridObj.getColIndexById("quarantine");
	var valid_col = gridObj.getColIndexById("validation");
	var parttype_col = gridObj.getColIndexById("parttype");
	var expiryQty_col = gridObj.getColIndexById("expiryQty");
	
	var qtySet = gridObj.cellById(rowId, qty_col).getValue();
	var missQty = gridObj.cellById(rowId, missqty_col).getValue();
	var setListQty = gridObj.cellById(rowId, setlistqty_col).getValue();
	var selfQty = gridObj.cellById(rowId, self_col).getValue();
	var bulkQty = gridObj.cellById(rowId, bulk_col).getValue();
	var backOrderQty = gridObj.cellById(rowId, backOrder_col).getValue();
	var moveToShelfQty = gridObj.cellById(rowId, shelf_col).getValue();
	var moveToScrapQty = gridObj.cellById(rowId, scrap_col).getValue();
	var moveToQnQty = gridObj.cellById(rowId, quarantine_col).getValue();
	var validationIcon = gridObj.cellById(rowId, valid_col).getValue();
	var shelfAvailQty = gridObj.cellById(rowId, selfAvail_col).getValue();
	var bulkAvailQty = gridObj.cellById(rowId, bulkAvail_col).getValue();
	var expiryQty = gridObj.cellById(rowId, expiryQty_col).getValue();

	var count = 0;
	var editTitle = '';
	
	var setMoveToQty = Number(missQty) + Number(moveToShelfQty) + Number(moveToScrapQty) + Number(moveToQnQty);
	var setFromQty = Number(selfQty) + Number(bulkQty) + Number(backOrderQty);
	var moveToQty = Number(moveToShelfQty) + Number(moveToScrapQty) + Number(moveToQnQty);
	var setMaxFromQty = (Number(qtySet) - Number(setListQty)) + Number(selfQty) + Number(bulkQty) + Number(backOrderQty);
	//Expiry date validation message also need to show in into mark image
	if(cntlNoTitle != ''){
		editTitle = cntlNoTitle;
		count++;
	}
	//Qty in set should not less than set list qty
	if((selfQty != '' || bulkQty != '' || backOrderQty != '') && (Number(selfQty) != 0 || Number(bulkQty) != 0 || Number(backOrderQty) != 0)){
		if(Number(setFromQty)+ Number(qtySet) < setListQty) {
			editTitle = editTitle +'\n'+ message_operations[852];
		}
			
	}
	//while edit value on move to shelf/scrap/qn the row color need to change
	if(expiryFl == 'Y'){ //expire 14days
		gridObj.setRowTextStyle(rowId, "background-color:#ffcccc;color:red;border:1px solid gray;");
		if(( moveToScrapQty != '' || moveToQnQty !='') && ( moveToScrapQty != 0 || moveToQnQty !=0)){ 
			if (expiryQty < Number(moveToScrapQty) + Number(moveToQnQty)){
			editTitle = editTitle +'\n'+ message_operations[841];
			valMsgFl = 'Y';
			count++;
			}
		}else{
			editTitle = editTitle +'\n'+ message_operations[841];
			valMsgFl = 'Y';
			count++;
		}
		
	}else if(expiryFl == 'N' && valMsgFl == 'Y'){ //expire 14days & 30days
		gridObj.setRowTextStyle(rowId, "background-color:#ffcccc;color:red;border:1px solid gray;");
		if(( moveToScrapQty != '' || moveToQnQty !='') && ( moveToScrapQty != 0 || moveToQnQty !=0)){ 
			if (expiryQty < Number(moveToScrapQty) + Number(moveToQnQty)){
				editTitle = editTitle +'\n'+ message_operations[841];
				count++;
			}
		}else{
			    editTitle = editTitle +'\n'+ message_operations[841];
				count++;
		}
		
	}else if(expiryFl == 'N'){ //expire 30days
		gridObj.setRowTextStyle(rowId, "background-color:#ffcccc;color:red;border:1px solid gray;");
	}else if(expiryFl == ''){ //no expire control number
		fnRowColor(rowId);
	}
	//Qty in Set needs to be equal to Set List Qty
	if((qtySet-moveToQty) > setListQty){
		editTitle = editTitle +'\n'+ message_operations[837];
		count++;
	}
	//Qty to replenish cannot be more than Qty in Shelf
	if(((selfQty != 0) && (Number(shelfAvailQty) == 0)) || (Number(selfQty) > (Number(shelfAvailQty)))){
		editTitle =  editTitle +'\n'+ message_operations[834];
		count++;
	}
	//Qty to replenish cannot be more than Qty in bulk
	if(((bulkQty != 0) && (Number(bulkAvailQty) == 0)) ||  (Number(bulkQty) > (Number(bulkAvailQty)))){
		editTitle =  editTitle +'\n'+ message_operations[835];
		count++;
	}
	//Move to qty cannot be more than Qty in Set
	if(setMoveToQty != '' || setMoveToQty != 0){
		if((Number(qtySet) + Number(setFromQty)) < setMoveToQty){
			editTitle =  editTitle +'\n'+ message_operations[836];
			count++;
		}
	}
	//Qty to Replenish needs to be equal to Sum of 'Missing Qty' & 'Move To Qty'
	if((Number(qtySet) <= setListQty) && (setMoveToQty != setFromQty)){
		editTitle =  editTitle +'\n'+ message_operations[828];
		count++;
	}
	if((Number(qtySet) > setListQty) && (setMoveToQty != setMaxFromQty)){
		editTitle =  editTitle +'\n'+ message_operations[828];
		count++;
	}
	if(count == 0){
		gridObj.cells(rowId,valid_col).setValue('<img  title="Success" height="20" width="20" src="/images/success.gif"/ border="0">'); 
		fnMouseOver(gridObj);
	}else{
		gridObj.cells(rowId,valid_col).setValue('<img title="'+TRIM(editTitle)+'" height="14" width="15" src="/images/dhtmlxGrid/delete.gif"/ border="0">'); 
		fnMouseOver(gridObj);
	}
	
}
	function fnUpdateMissingQty(partnum,qty){
	// Validate the expiry qty and set the flag for showing error message
	var expiryQty_col = gridObj.getColIndexById("expiryQty");
	expiryQty = gridObj.cellById(partnum, expiryQty_col).getValue();
	if(expiryQty == '' || expiryQty == 0){
		if(expiryFl != 'N'){
			expiryFl = '';
			valMsgFl = ''; 
		}
	}else{
			expiryFl = 'Y';
	}	
	var missqty_col = gridObj.getColIndexById("missQty");
	var missQty = gridObj.cellById(partnum, missqty_col).getValue();
	missQty=missQty-qty;
	gridObj.cells(partnum,missqty_col).setValue(missQty);	
	fnValidateTrans(partnum);
	var rowIndex=gridObj.getRowIndex(partnum);	
	gridObj.selectRow(rowIndex,true,true,true);
	}
	
	//To clear the field
	function fnClearDiv() {
		document.getElementById("controlNo").value = '';
	}
	function fnRemove(){
	    var selId = gridRecLotsObj.getSelectedId();       //gets the Id of the selected row				
		var recqty_col = gridRecLotsObj.getColIndexById("qty");
	    var recQty = gridRecLotsObj.cellById(selId, recqty_col).getValue();
		var recPartNum_col = gridRecLotsObj.getColIndexById("partnum");
	    var partnum = gridRecLotsObj.cellById(selId, recPartNum_col).getValue();
        // to increase missing qty
		var missqty_col = gridObj.getColIndexById("missQty");
	    var missQty = gridObj.cellById(partnum, missqty_col).getValue();
	    missQty=parseInt(missQty)+parseInt(recQty);
	    gridObj.cells(partnum,missqty_col).setValue(missQty);
	    gridRecLotsObj.deleteRow(selId);            //deletes the row with the specified id
//	    var expiryqty_col = gridObj.getColIndexById("expiryQty");
//	    expiryQty = gridObj.cellById(partnum, expiryqty_col).getValue();
//	    alert('before--> '+expiryQty);
//	    expiryQty=Number(expiryQty)-1;
//	    gridObj.cells(partnum,expiryqty_col).setValue(expiryQty);
//	    alert('remove--> '+expiryQty);
	    removeExpiryDtValidation(partnum);
	    fnValidateTrans(partnum);
//	    fnRowColor(partnum);
	    
	}
	function fnRemoveAll(){
		if (confirm(message_operations[832])) {
			// to increase missing qty
			var missqty_col = gridObj.getColIndexById("missQty");
			var setlistqty_col = gridObj.getColIndexById("setListQty");
		    var setListQty = '';
	        var partType = '';
			gridrows = gridObj.getAllRowIds(",");
			gridrowsarr = gridrows.toString().split(",");
			arrLen = gridrowsarr.length;
				for (var i = 0; i < arrLen; i++) {
					rowId = gridrowsarr[i];
					setListQty = gridObj.cellById(rowId, setlistqty_col).getValue();					
					partType = gridObj.cellById(rowId, parttype_col).getValue();					
					//to revert sterile parts missing qty in received parts	popup				
					if( partType != undefined && partType != ''	&& partType == '4030'){	
						gridObj.cells(rowId,missqty_col).setValue(setListQty);
					}	
					var expiryqty_col = gridObj.getColIndexById("expiryQty");
					gridObj.cells(rowId,expiryqty_col).setValue('0');
					expiryFl = '';
					fnValidateTrans(rowId);
				}
			//delete all rows from the grid
			gridRecLotsObj.clearAll();
			removeFl = 'Y';
			//delete all rows from the grid, clear header
			//gridRecLotsObj.clearAll(true);
		}
	}
	
	
	
	
	function fnOnLoad(){
		  fnMsgLoad(); //This function used to get priority based on Consignment id
		 fnLoadLoanerHeader();
		 fnLoadConsignDtl();
		 fnLoadLoanerOpenTrans();
		 fnLoadBioHazardDropdown();
		 fnLoadDamageDropdown();
	}
	//header section
	function fnLoadLoanerHeader(){
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmLoanerLotReprocess.do?method=loadLoanerHeader&setid='+setid+'&consignmentId='+consignId+'&randomId='+Math.random());
		 dhtmlxAjax.get(ajaxUrl,function(loader){
				var strJsonString =loader.xmlDoc.responseText;
				var rows = [], i = 0;
				var JSON_Array_obj = "";
				var setname='';
				var settype='';
				var etchid='';
				var loansfl='';
				var billnm='';
				var ldate='';
				var edate='';
				var returndt='';
				var rollbackfl='';
			if(strJsonString!=""){
				JSON_Array_obj = JSON.parse(strJsonString);
				$.each(JSON_Array_obj, function(index,jsonObject) {
					setname = jsonObject.setname;
					settype = jsonObject.settype;
					etchid = jsonObject.etchid;
					loansfl = jsonObject.loansfl;
					billnm = jsonObject.billnm;
					ldate = jsonObject.ldate;
					edate = jsonObject.edate;
					returndt = jsonObject.returndt;
					loansflcd = jsonObject.loansflcd;
					rollbackfl = jsonObject.rollbackfl;
					
					if(rollbackfl == 'Y' && removeFl != 'Y'){
						document.getElementById("rollbackFl").value = rollbackfl;
					}
					
					// default array
					rows[i] = {};
					rows[i].id = consignId;
					rows[i].data = [];
					var obj = eval("document.all.setName");
					obj.innerHTML = setname;
					var obj = eval("document.all.settype");
					obj.innerHTML = settype;
					if(etchid != null){
						var obj = eval("document.all.Txt_EtchId");
						obj.innerHTML = etchid;
					}
					var obj = eval("document.all.loansfl");
					obj.innerHTML = loansfl;
					var obj = eval("document.all.billnm");
					obj.innerHTML = billnm;
					if(ldate != null){
						var obj = eval("document.all.ldate");
						obj.innerHTML = ldate;
					}
					if(edate != null){
						var obj = eval("document.all.edate");
						obj.innerHTML = edate;
					}
					if(loansflcd == '25' && returndt != null){
						var obj = eval("document.all.returndt");
						obj.innerHTML = returndt;
					}
				});
				
				
				
			}	
			});

	}

function fnLoadConsignDtl(){
	document.all.controlNo.focus();
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmLoanerLotReprocess.do?method=getSetConsignDetails&consignmentId='+consignId+'&randomId='+Math.random());
	 dhtmlxAjax.get(ajaxUrl,function(loader){
			var strJsonString =loader.xmlDoc.responseText;
			var rows = [], i = 0;
			var JSON_Array_obj = "";
			var pnum='';
			var pdesc='';
			var qty='';
			var missQty='';
			var setType='';
			var setListQty='';
			var self='';
			var bulk='';
			var price='';
			var backOrder='';
			var gridrows = ""
			var gridrowsarr = "";
			var arrLen = "";
			
			var pnum_col = '';
			var desc_col = '';
			var qty_col = '';
			var missqty_col = '';
			var setlistqty_col = '';
			var self_col = '';
			var selfAvail_col = '';
			var bulk_col= '';
			var bulkAvail_col = '';
			var backOrder_col = '';
			var shelf_col = '';
			var scrap_col = '';
			var quarantine_col = '';
			
			var partNum = '';
			var qty = '';
			var missQty = '';
			var setListQty = '';
			var selfQty = '';
			var bulkQty = '';
			var backOrderQty = '';
			var shelfAvailQty = '';
			var bulkAvailQty = '';
			var moveToShelfQty = '';
			var moveToScrapQty = '';
			var moveToQnQty = '';
			var validationIcon = '';
			var qtySet = '';
			
			if(strJsonString!=""){
				JSON_Array_obj = JSON.parse(strJsonString);
				$.each(JSON_Array_obj, function(index,jsonObject) {
					pnum = jsonObject.PNUM;
					pdesc = jsonObject.PDESC;
					qty = jsonObject.QTY;
					missQty = jsonObject.MISSQTY;
					setListQty = jsonObject.SETLISTQTY;
					self = jsonObject.SHELF;
					bulk = jsonObject.BULK;
					backOrder = jsonObject.BACKORDER;
					setType = jsonObject.SETTYPE;
					price = jsonObject.PRICE;
					
					rows[i] = {};
					rows[i].id = i;
					rows[i].data = [];
					rows[i].data[0] = pnum;
					rows[i].data[1] = pdesc;
					rows[i].data[2] = qty;
					//rows[i].data[3] = missQty;
					rows[i].data[3] = missQty-backOrder;
					rows[i].data[4] = setListQty;
					rows[i].data[5] = '';
					rows[i].data[6] = self;
					rows[i].data[7] = '';
					rows[i].data[8] = bulk;
					rows[i].data[9] = '';
					rows[i].data[10] = '';
					rows[i].data[11] = '';
					rows[i].data[12] = '';
					rows[i].data[13] = '';
					rows[i].data[14] = price;
					rows[i].data[15] = setType;
					i++;

				});
				var setInitWidths = "80,*,50,55,50,50,50,50,50,50,50,50,50,30,30,10,10";
				var setColAlign = "left,left,center,center,center,right,center,right,center,center,right,right,right,center,right,right,right";
				var setColTypes = "ro,ro,ro,ro,ro,ed,ro,ed,ro,ed,ed,ed,ed,ro,ro,ro,ro";				
				var setColSorting = "str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str";
				var setHeader = [ "Part#","Part Description", "Qty in Set", "Missing Qty", "Set List Qty", "Rep from Shelf", "Shelf Avail", "Rep from Bulk", "Bulk Avail", "Rep from Back Order", "Move to Shelf", "Move to Scrap", "Move to QN", "","Price","Part Type","Expiry Qty" ];
				var setColIds ="partnum,desc,qty,missQty,setListQty,self,shelfAvail,bulk,bulkAvail,backOrder,shelf,scrap,quarantine,validation,price,parttype,expiryQty";
				var setFilter = [ "#text_filter", "#text_filter", "#text_filter",
				  				"#text_filter", "#text_filter", "#text_filter",
				  				"#text_filter", "#text_filter", "#text_filter",
				  				"#text_filter", "#text_filter", "#text_filter",
				  				"#text_filter"];
				var enableTooltips = "true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true";
				var footerArry = [];
				var gridHeight = "";
				var footerStyles = [];
				var footerExportFL = true;
				var dataHost = {};
				var pagination = "";
				dataHost.rows = rows;
				document.getElementById("setConInfo").style.display = 'block';
				gridObj = loadDHTMLXGrid('setConInfo', gridHeight, dataHost,
						setHeader, setInitWidths, setColAlign, setColTypes,
						setColSorting, setColIds, enableTooltips, setFilter,
						footerArry, footerStyles, footerExportFL, pagination);

				gridObj.setColumnHidden(14, true);
				gridObj.setColumnHidden(15, true);
				gridObj.setColumnHidden(16, true);
				gridObj.attachEvent("onEditCell",doOnCellEdit);
				gridObj.enableEditEvents(true,false,true);
				pnum_col = gridObj.getColIndexById("partnum");
				desc_col = gridObj.getColIndexById("desc");
				qty_col = gridObj.getColIndexById("qty");
				missqty_col = gridObj.getColIndexById("missQty");
				setlistqty_col = gridObj.getColIndexById("setListQty");
				self_col = gridObj.getColIndexById("self");
				selfAvail_col = gridObj.getColIndexById("shelfAvail");
				bulk_col = gridObj.getColIndexById("bulk");
				bulkAvail_col = gridObj.getColIndexById("bulkAvail");
				backOrder_col = gridObj.getColIndexById("backOrder");
				shelf_col = gridObj.getColIndexById("shelf");
				scrap_col = gridObj.getColIndexById("scrap");
				quarantine_col = gridObj.getColIndexById("quarantine");
				valid_col = gridObj.getColIndexById("validation");
				parttype_col = gridObj.getColIndexById("parttype");
				
				gridrows = gridObj.getAllRowIds(",");
			    gridrowsarr = gridrows.toString().split(",");
			    arrLen = gridrowsarr.length;
				for (var i = 0; i < arrLen; i++) {
					rowId = gridrowsarr[i];		
					gridObj.setCellTextStyle(rowId, pnum_col,
							"background-color:#FFFFFF;border:1px solid gray;");
					gridObj.setCellTextStyle(rowId, desc_col,
							"background-color:#FFFFFF;border:1px solid gray;");
					gridObj.setCellTextStyle(rowId, qty_col,
					"background-color:#D1DFF1;border:1px solid gray;");
					
					gridObj.setCellTextStyle(rowId, missqty_col,
					"background-color:#D1DFF1;border:1px solid gray;");
					//to make it editable
					var partType = gridObj.cellById(rowId, parttype_col).getValue();
					if( partType != undefined && partType != ''	&& partType != '4030'){					 	
					gridObj.setCellExcellType(rowId, missqty_col,"ed");
					gridObj.cells(rowId,missqty_col).setValue("");
					}					
					gridObj.setCellTextStyle(rowId, setlistqty_col,
					"background-color:#D1DFF1;border:1px solid gray;");
					gridObj.setCellTextStyle(rowId, self_col,
					"background-color:#ffecb9;border:1px solid gray;");
					gridObj.setCellTextStyle(rowId, bulk_col,
					"background-color:#ffecb9;border:1px solid gray;");
					gridObj.setCellTextStyle(rowId, backOrder_col,
					"background-color:#ffecb9;border:1px solid gray;");
					gridObj.setCellTextStyle(rowId, selfAvail_col,
					"background-color:#ccdd99;border:1px solid gray;");
					gridObj.setCellTextStyle(rowId, bulkAvail_col,
					"background-color:#ccdd99;border:1px solid gray;");
					gridObj.setCellTextStyle(rowId, shelf_col,
					"background-color:#f2dcdb;border:1px solid gray;");
					gridObj.setCellTextStyle(rowId, scrap_col,
					"background-color:#f2dcdb;border:1px solid gray;");
					gridObj.setCellTextStyle(rowId, quarantine_col,
					"background-color:#f2dcdb;border:1px solid gray;");
					gridObj.setCellTextStyle(rowId, valid_col,
					"background-color:#ecf6fe;border:1px solid gray;");
					
					partNum = gridObj.cellById(rowId, pnum_col).getValue();				
					qty = gridObj.cellById(rowId, qty_col).getValue();
					missQty = gridObj.cellById(rowId, missqty_col).getValue();
					setListQty = gridObj.cellById(rowId, setlistqty_col).getValue();
					selfQty = gridObj.cellById(rowId, self_col).getValue();
					bulkQty = gridObj.cellById(rowId, bulk_col).getValue();
					backOrderQty = gridObj.cellById(rowId, backOrder_col).getValue();
					shelfAvailQty = gridObj.cellById(rowId, selfAvail_col).getValue();
					bulkAvailQty = gridObj.cellById(rowId, bulkAvail_col).getValue();
					moveToShelfQty = gridObj.cellById(rowId, shelf_col).getValue();
					moveToScrapQty = gridObj.cellById(rowId, scrap_col).getValue();
					moveToQnQty = gridObj.cellById(rowId, quarantine_col).getValue();
					validationIcon = gridObj.cellById(rowId, valid_col).getValue();
					
					var setMoveToQty = missQty + moveToShelfQty + moveToScrapQty + moveToQnQty;
					var setFromQty = selfQty + bulkQty + backOrderQty;
					var count = 0;
					var loadTitle = '';
					
					if(qty < setListQty){
						loadTitle = loadTitle +'\n'+ message_operations[837];
						count++;
					}
					if(setMoveToQty != setFromQty){
						loadTitle = loadTitle +'\n'+ message_operations[828];
						count++;
					}
					if(Number(setFromQty)+ Number(qty) < setListQty) {
						loadTitle = loadTitle +'\n'+ message_operations[852];
					}
					if(count == 0){
						gridObj.cells(rowId,valid_col).setValue('<img  title="Success" height="20" width="20" src="/images/success.gif"/ border="0">'); 
						fnMouseOver(gridObj);
					}else{
						gridObj.cells(rowId,valid_col).setValue('<img title="'+TRIM(loadTitle)+'" height="14" width="15" src="/images/dhtmlxGrid/delete.gif"/ border="0">'); 
						fnMouseOver(gridObj);
					}
					gridObj.setRowId(rowId,partNum);	
				}
				
				//if (document.getElementById("rollbackFl").value == 'Y'){
						fnLoadExistingLot();
				//}
			}else{
				document.getElementById("setConInfo").style.display = 'none';
			}
	 });
	 
	 
}

function fnMouseOver(gridObj){
gridObj.attachEvent("onMouseOver",function()
	{
		return false;
	});
}

//change column grid color 
function fnRowColor(rowId){
	pnum_col = gridObj.getColIndexById("partnum");
	desc_col = gridObj.getColIndexById("desc");
	qty_col = gridObj.getColIndexById("qty");
	missqty_col = gridObj.getColIndexById("missQty");
	setlistqty_col = gridObj.getColIndexById("setListQty");
	self_col = gridObj.getColIndexById("self");
	selfAvail_col = gridObj.getColIndexById("shelfAvail");
	bulk_col = gridObj.getColIndexById("bulk");
	bulkAvail_col = gridObj.getColIndexById("bulkAvail");
	backOrder_col = gridObj.getColIndexById("backOrder");
	shelf_col = gridObj.getColIndexById("shelf");
	scrap_col = gridObj.getColIndexById("scrap");
	quarantine_col = gridObj.getColIndexById("quarantine");
	valid_col = gridObj.getColIndexById("validation");
	parttype_col = gridObj.getColIndexById("parttype");
	
	gridObj.setCellTextStyle(rowId, pnum_col,
	"background-color:#FFFFFF;border:1px solid gray;");
	gridObj.setCellTextStyle(rowId, desc_col,
			"background-color:#FFFFFF;border:1px solid gray;");
	gridObj.setCellTextStyle(rowId, qty_col,
	"background-color:#D1DFF1;border:1px solid gray;");
	gridObj.setCellTextStyle(rowId, missqty_col,
	"background-color:#D1DFF1;border:1px solid gray;");
	gridObj.setCellTextStyle(rowId, setlistqty_col,
	"background-color:#D1DFF1;border:1px solid gray;");
	gridObj.setCellTextStyle(rowId, self_col,
	"background-color:#ffecb9;border:1px solid gray;");
	gridObj.setCellTextStyle(rowId, bulk_col,
	"background-color:#ffecb9;border:1px solid gray;");
	gridObj.setCellTextStyle(rowId, backOrder_col,
	"background-color:#ffecb9;border:1px solid gray;");
	gridObj.setCellTextStyle(rowId, selfAvail_col,
	"background-color:#ccdd99;border:1px solid gray;");
	gridObj.setCellTextStyle(rowId, bulkAvail_col,
	"background-color:#ccdd99;border:1px solid gray;");
	gridObj.setCellTextStyle(rowId, shelf_col,
	"background-color:#f2dcdb;border:1px solid gray;");
	gridObj.setCellTextStyle(rowId, scrap_col,
	"background-color:#f2dcdb;border:1px solid gray;");
	gridObj.setCellTextStyle(rowId, quarantine_col,
	"background-color:#f2dcdb;border:1px solid gray;");
	gridObj.setCellTextStyle(rowId, valid_col,
	"background-color:#ecf6fe;border:1px solid gray;");
}

//this function is used to perform action when a cell is editing
function doOnCellEdit(stage, rowId, cellInd, nValue, oValue) {
	var self_col = gridObj.getColIndexById("self");
	var bulk_col = gridObj.getColIndexById("bulk");
	var shelf_col = gridObj.getColIndexById("shelf");
	var scrap_col = gridObj.getColIndexById("scrap");
	var quarantine_col = gridObj.getColIndexById("quarantine");
	//Highlight data when edit cell
	if (stage == 1 && (cellInd == self_col || cellInd == bulk_col || cellInd == shelf_col || cellInd == scrap_col || cellInd == quarantine_col )){
		var c=this.editor.obj;			
		if(c!=null) c.select();
	}
		var expiryQty_col = gridObj.getColIndexById("expiryQty");
		expiryQty = gridObj.cellById(rowId, expiryQty_col).getValue();
		if(expiryQty == '' || expiryQty == 0){
			expiryFl = '';
			valMsgFl = ''; 
		}else{
			expiryFl = 'Y';
		}
	fnValidateTrans(rowId);
	return true;
}


function fnLoadLoanerOpenTrans(){
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmLoanerLotReprocess.do?method=loadLoanerOpenTransactions&consignmentId='+consignId+'&randomId='+Math.random());
	 dhtmlxAjax.get(ajaxUrl,function(loader){
			var strJsonString =loader.xmlDoc.responseText;
			var rows = [], i = 0;
			var JSON_Array_obj = "";
			var pnum='';
			var pdesc='';
			var qty='';
			var transId='';
			var masterId='';
			var createdDate='';
			var status='';
			var type='';
			var gridrows = ""
			var gridrowsarr = "";
			var arrLen = "";
			var borptfl = "";
			
			
			if(strJsonString!=""){
				JSON_Array_obj = JSON.parse(strJsonString);
				$.each(JSON_Array_obj, function(index,jsonObject) {
					pnum = jsonObject.PNUM;
					pdesc = jsonObject.PDESC;
					transId = jsonObject.ID;
					masterId = jsonObject.MASTER_ID;
					qty = jsonObject.QTY;
					createdDate = jsonObject.CRDATE;
					status = jsonObject.STATUS;
					type = jsonObject.TYPE;
					borptfl = jsonObject.BORPTFL;
					
					
					rows[i] = {};
					rows[i].id = i;
					rows[i].data = [];
					rows[i].data[0] = pnum;
					rows[i].data[1] = pdesc;
					rows[i].data[2] = "<a href=\"#\" onclick=\"fnPicSlip('" + transId + "','50156','" + masterId + "');\"><img height='12' width='12' src=/images/packslip.gif border=0 ></img></a>&nbsp;"+transId;
					rows[i].data[3] = qty;
					rows[i].data[4] = createdDate;
					rows[i].data[5] = status;
					rows[i].data[6] = type;
					if((type =="Loaner Back Order")&&(borptfl =="Y")){
					   rows[i].data[7] = "<a href=\"#\" onclick=\"fnOpenLoanerItemsBORpt('" + masterId + "');\"><u>"+borptfl+"</u></a>";
					}else{
					   rows[i].data[7] ='';
					}
				
					
					i++;

				});
				var setInitWidths = "100,*,120,80,100,100,200,50";
				var setColAlign = "left,left,left,right,left,left,left,center";
				var setColTypes = "ro,ro,ro,ro,ro,ro,ro,ro";
				var setColSorting = "str,str,str,str,str,str,str,str";
				var setHeader = [ "Part #","Description", "Transaction ID", "BO Qty", "Date", "Status", "Type","" ];
				var setColIds ="partnum,desc,id,boQty,date,status,type,borptfl";
				var setFilter = "";
				var enableTooltips = "true,true,true,true,true,true,true,true";
				var footerArry = [];
				var gridHeight = "";
				var footerStyles = [];
				var footerExportFL = true;
				var dataHost = {};
				var pagination = "";
				dataHost.rows = rows;
				document.getElementById("opentransInfo").style.display = 'block';
				document.getElementById("DivNothingMessage").style.display = 'none';
				gridObj = loadDHTMLXGrid('opentransInfo', gridHeight, dataHost,
						setHeader, setInitWidths, setColAlign, setColTypes,
						setColSorting, setColIds, enableTooltips, setFilter,
						footerArry, footerStyles, footerExportFL, pagination);
	 }else{
		    document.getElementById("DivNothingMessage").style.display = 'block';
			document.getElementById("opentransInfo").style.display = 'none';
		}

});
}

function fnLoadBioHazardDropdown(){
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmLoanerLotReprocess.do?method=loadBioHazardDropdown&consignmentId='+consignId+'&randomId='+Math.random());
	 dhtmlxAjax.get(ajaxUrl,function(loader){
			var resJSONObj = $.parseJSON(loader.xmlDoc.responseText);
			var obj=document.getElementById("biohazard");
			fnPopulateOptions(obj, resJSONObj, "CODEID", "CODENM", "");
			});
}

function fnLoadDamageDropdown(){
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmLoanerLotReprocess.do?method=loadDamageDropdown&consignmentId='+consignId+'&randomId='+Math.random());
	 dhtmlxAjax.get(ajaxUrl,function(loader){
			var resJSONObj = $.parseJSON(loader.xmlDoc.responseText);
			var obj=document.getElementById("damage");
			fnPopulateOptions(obj, resJSONObj, "CODEID", "CODENM", "");
			});
}



function fnLoadRules()
{
	var cnt = document.frmVendor.hCnt.value; 
	var inputStr = '';
	for (var i=0;i<cnt;i++)
	{
		pnumobj = eval("document.frmVendor.hPartNum"+i);		
		inputStr = inputStr+pnumobj.value + ",";
	}
	if(inputStr.length > 1)
	{
		inputStr = inputStr.substr(0,inputStr.length-1);
		inputStr = "1^50607^"+inputStr + "^|";
	}
	document.frmVendor.action = "/gmRuleReport.do?loadRule=true&inputStr="+inputStr+"&strOpt=reload&txnId=0&consequenceId=0&status=0&initiatedBy=0";
	document.frmVendor.submit();
}


function fnSaveCharges(){
	
	//92070;values code group BHGRP|92071;values code group DMGRP|92074;Values - Text Area|
			var damage = '';
			var biohazard = '';
			var enterExcess = '';
			var chargesStr = '';
			if(document.getElementById("damage") != undefined){
	  				damage = document.getElementById("damage").value ;
					if (damage != '0'){
						chargesStr = '92070;'+damage+'|';
					}
	  				
			}
			if(document.getElementById("biohazard") != undefined){
	  				biohazard = document.getElementById("biohazard").value ;
					if (biohazard != '0'){
						chargesStr = chargesStr + '92071;'+biohazard+'|';
					}
	  				
			}
			if(document.getElementById("enterExcess") != undefined){
	  				enterExcess = document.getElementById("enterExcess").value ;
					if (enterExcess != ''){
						chargesStr = chargesStr + '92074;'+enterExcess+'|';
					}
	  				
			}
			
			if (chargesStr != ''){
				var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmLoanerLotReprocess.do?method=saveLoanerCharges&transType=4127&inputString='+encodeURIComponent(chargesStr)+'&consignmentId='+consignId+'&randomId='+Math.random());
				dhtmlxAjax.get(ajaxUrl,function(loader){	  
				 var resText = loader.xmlDoc.responseText;			
				});
			}

	
}

function fnSaveReceivedLots(){
             var pnum_col = '';			
			 var qty_col = '';
			 var cnum_col = '';
			 var partNum = '';
			 var qty = '';
			 var controlNum = '';
			 
			 if(gridRecLotsObj != null && gridRecLotsObj != undefined && gridRecLotsObj != ''){
				//"remove,partnum,contlNum,desc,qty";
				pnum_col = gridRecLotsObj.getColIndexById("partnum");
 				qty_col = gridRecLotsObj.getColIndexById("qty");
				cnum_col = gridRecLotsObj.getColIndexById("contlNum");
								
		        gridrows = gridRecLotsObj.getAllRowIds(",");
			    gridrowsarr = gridrows.toString().split(",");
			    arrLen = gridrowsarr.length;
				for (var i = 0; i < arrLen; i++) {
					rowId = gridrowsarr[i];
					partNum = gridRecLotsObj.cellById(rowId, pnum_col).getValue();				
					qty = gridRecLotsObj.cellById(rowId, qty_col).getValue();
					controlNum = gridRecLotsObj.cellById(rowId, cnum_col).getValue();
					//alert('qty '+qty+'partNum '+partNum+'controlNum '+controlNum);	
					
					if (partNum != '' && controlNum!= ''  && qty!= ''){
						fnSaveRecLots(partNum,controlNum,qty);
					}					
					
				}
			 }
}
function fnSaveRecLots(partNum,controlNum,qty){
		reccount++;
		updateStatus='N';
		if(reccount == 1){
			updateStatus='Y';
		}
	        var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmLoanerLotReprocess.do?method=saveReceivedLots&partNum='+partNum+'&ctrlNum='+controlNum+'&qty='+qty+'&consignmentId='+consignId+'&strStatusFl='+updateStatus+'&randomId='+Math.random());
			dhtmlxAjax.get(ajaxUrl,function(loader){	  
			 var resText = loader.xmlDoc.responseText;	
			 if(resText == 'Y'){
				 document.getElementById("successCont").style.display = 'block';
				 document.getElementById("successCont").innerHTML = "<font color='green' style='font-weight:bold;'><center style='left: 350px; position: relative;' >Records Saved Successfully</center></font>";
			 }
			});
		
}

function fnSaveTransactions(){
	
		    var pnum_col = '';			
			var qty_col = '';
			var missqty_col = '';
			var setlistqty_col = '';
			var self_col = '';
			var selfAvail_col = '';
			var bulk_col= '';
			var bulkAvail_col = '';
			var backOrder_col = '';
			var shelf_col = '';
			var scrap_col = '';
			var quarantine_col = '';
			var valid_col = '';
			var price_col = '';
			var parttype_col = '';
			
			var partNum = '';
			var qty = '';
			var missQty = '';
			var setListQty = '';
			var selfQty = '';
			var bulkQty = '';
			var backOrderQty = '';
			var shelfAvailQty = '';
			var bulkAvailQty = '';
			var moveToShelfQty = '';
			var moveToScrapQty = '';
			var moveToQnQty = '';
			var validation = '';
			var price = ''; 
			var partType = '';
			var nsPartQty = '';
			var missingStr = '';
			var replenishShelfStr = '';
			var replenishBulkStr = '';
			var replenishBOStr = '';
			var movetoShelfStr = '';
			var movetoScrapStr = '';
			var movetoQNStr = '';
			var rowIndex = '';
			var pos = '';
			
			var transIds = '';
			var log= '';
			if(document.getElementById("comments") != undefined){
	  				log = document.getElementById("comments").value ;
	  				
			}
			
			//var ajaxUrl = '/gmLoanerLotReprocess.do?method=saveLoanerReturnTrans&transType=4127&log=test&consignmentId='+consignId+'&randomId='+Math.random();
			var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmLoanerLotReprocess.do?method=saveLoanerReturnTrans&transType=4127&log='+log+'&consignmentId='+consignId+'&hSetId='+setid+'&randomId='+Math.random());
			var transUrl= '';
			document.all.SubmitButton.disabled = true;
			
					
			//"partnum,desc,qty,missQty,setListQty,self,shelfAvail,bulk,bulkAvail,backOrder,shelf,scrap,quarantine,validation,price,parttype";
		        pnum_col = gridObj.getColIndexById("partnum");
 				qty_col = gridObj.getColIndexById("qty");
				missqty_col = gridObj.getColIndexById("missQty");
				setlistqty_col = gridObj.getColIndexById("setListQty");
				self_col = gridObj.getColIndexById("self");
				selfAvail_col = gridObj.getColIndexById("shelfAvail");
				bulk_col = gridObj.getColIndexById("bulk");
				bulkAvail_col = gridObj.getColIndexById("bulkAvail");
				backOrder_col = gridObj.getColIndexById("backOrder");
				shelf_col = gridObj.getColIndexById("shelf");
				scrap_col = gridObj.getColIndexById("scrap");
				quarantine_col = gridObj.getColIndexById("quarantine");
				valid_col = gridObj.getColIndexById("validation");
				price_col = gridObj.getColIndexById("price");
				parttype_col = gridObj.getColIndexById("parttype");
		        gridrows = gridObj.getAllRowIds(",");
			    gridrowsarr = gridrows.toString().split(",");
			    arrLen = gridrowsarr.length;
				for (var i = 0; i < arrLen; i++) {
					rowId = gridrowsarr[i];
					partNum = gridObj.cellById(rowId, pnum_col).getValue();				
					qty = gridObj.cellById(rowId, qty_col).getValue();
					missQty = gridObj.cellById(rowId, missqty_col).getValue();
					setListQty = gridObj.cellById(rowId, setlistqty_col).getValue();
					selfQty = gridObj.cellById(rowId, self_col).getValue();
					bulkQty = gridObj.cellById(rowId, bulk_col).getValue();
					backOrderQty = gridObj.cellById(rowId, backOrder_col).getValue();
					shelfAvailQty = gridObj.cellById(rowId, selfAvail_col).getValue();
					bulkAvailQty = gridObj.cellById(rowId, bulkAvail_col).getValue();
					moveToShelfQty = gridObj.cellById(rowId, shelf_col).getValue();
					moveToScrapQty = gridObj.cellById(rowId, scrap_col).getValue();
					moveToQnQty = gridObj.cellById(rowId, quarantine_col).getValue();
					validation = gridObj.cellById(rowId, valid_col).getValue();
					price = gridObj.cellById(rowId, price_col).getValue();
					partType = gridObj.cellById(rowId, parttype_col).getValue();
					//alert('qty'+qty+'missQty '+missQty+'setListQty '+setListQty+'selfQty'+selfQty+'bulkQty'+bulkQty+'backOrderQty'+backOrderQty+'shelfAvailQty'+shelfAvailQty+'bulkAvailQty'+bulkAvailQty+'moveToShelfQty'+moveToShelfQty+'moveToScrapQty'+moveToScrapQty+'moveToQnQty'+moveToQnQty+'validation'+validation+'price'+price);	
					
					if(missQty == undefined || missQty == ''){
						missQty = 0 ;
					}
					//missing
					if(missQty > 0){
					missingStr = missingStr + partNum + ','+ missQty + ',,'+price+'|';}
					//replenish
					if(selfQty > 0){
					replenishShelfStr = replenishShelfStr +  partNum + ','+ selfQty + ',,'+price+'|';}
					 if(bulkQty > 0){
					 replenishBulkStr = replenishBulkStr +  partNum + ','+ bulkQty + ',,'+price+'|';}
					 if(backOrderQty > 0){
					 replenishBOStr = replenishBOStr +  partNum + ','+ backOrderQty + ',,'+price+'|';}
					//moveto
					if(moveToShelfQty > 0){
					movetoShelfStr = movetoShelfStr +  partNum + ','+ moveToShelfQty + ',,'+price+'|';}
					if(moveToScrapQty > 0){
					movetoScrapStr = movetoScrapStr +  partNum + ','+ moveToScrapQty + ',,'+price+'|';}
					if(moveToQnQty > 0){
					movetoQNStr = movetoQNStr +  partNum + ','+ moveToQnQty + ',,'+price+'|';}
					
					
					//to save non sterile parts in received parts					
					if( partType != undefined && partType != ''	&& partType != '4030'){	
					// check the part number already available then delete it
					/*rowIndex=gridRecLotsObj.getRowIndex(partNum);
						if(rowIndex != undefined && rowIndex != ''){
						 gridRecLotsObj.deleteRow(partNum);    
						}
					pos = gridRecLotsObj.getRowsNum();  //gets the number of rows in grid
					nsPartQty = parseInt(qty)-parseInt(missQty);
						if(pos != undefined && pos != ''){
						 gridRecLotsObj.addRow(partNum,['',partNum,'NOC#','',nsPartQty],pos);  //adds a new row  
						}*/
						nsPartQty = parseInt(qty)-parseInt(missQty);
						fnSaveRecLots(partNum,'NOC#',nsPartQty)
					}					
					
					
				}
					//Missing qty
					if(missingStr != null && missingStr != ''){					 
					 transUrl = transUrl + '&MISSSTR='+missingStr;					 					  
					}
					//alert('missingParam'+missingParam);
					// repln from Shelf
					if(replenishShelfStr != null && replenishShelfStr != ''){
					 transUrl = transUrl +  '&REPFROM100040='+replenishShelfStr;
					}
					//alert('repShelfParam'+repShelfParam);
					// repln from Bulk
					if(replenishBulkStr != null && replenishBulkStr != ''){
					 transUrl = transUrl +  '&REPFROM100041='+replenishBulkStr;
					}
					//alert('repBulkParam'+repBulkParam);
					// repln from BO
					if(replenishBOStr != null && replenishBOStr != ''){
					 transUrl = transUrl +  '&REPFROM100061='+replenishBOStr;
					}
					//alert('repBOParam'+repBOParam);
					// move to shelf
					if(movetoShelfStr != null && movetoShelfStr != ''){
					 transUrl = transUrl +  '&MOVETO100045='+movetoShelfStr;	
					}
					//alert('moveShelfParam'+moveShelfParam);
					// move to Scrap
					if(movetoScrapStr != null && movetoScrapStr != ''){
					 transUrl = transUrl +  '&MOVETO100046='+movetoScrapStr;	
					}
					//alert('moveScrapParam'+moveScrapParam);
					// move to Quarantine
					if(movetoQNStr != null && movetoQNStr != ''){
					 transUrl = transUrl +  '&MOVETO100047='+movetoQNStr;	
					}
					//alert('moveQnParam'+moveQnParam);				
										
				
				ajaxUrl = ajaxUrl+encodeURIComponent(transUrl);
				//alert('ajaxUrl1111111111'+ajaxUrl);
				/*dhtmlxAjax.get(ajaxUrl,function(loader){
				transId =loader.xmlDoc.responseText;
				});*/
				//transId =fnGenerateTransaction(ajaxUrl);
				fnStartProgress();
				dhtmlxAjax.post(ajaxUrl,'',fnShowTransactions);
				//alert('transIds'+transId);
				
}

function fnShowTransactions(loader){	
	responseText =loader.xmlDoc.responseText;	
	fnStopProgress();
	if(TRIM(responseText) != ''){
		//alert('set into div');
		var inhouseTransDiv = document.getElementById('inhouseTrans');
		if(inhouseTransDiv != undefined){
		inhouseTransDiv.innerHTML = '<B> Created Transactions: '+responseText+'</B>';
		}
	}
	fnSaveReceivedLots();
	fnSaveCharges();
	fnInitiateJMS();
	   

}


function fnGenerateTransaction(ajaxUrl){ 
 var transId = '';
 dhtmlxAjax.get(ajaxUrl,function(loader){	 
	transId =loader.xmlDoc.responseText;
});
 return transId;
}

function fnInitiateJMS(){
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmLoanerLotReprocess.do?method=initiateLoanerLotJMS&consignmentId='+consignId+'&randomId='+Math.random());
				dhtmlxAjax.get(ajaxUrl,function(loader){	  
				var resText = loader.xmlDoc.responseText;			
				});
}
	   
function fnLoanerRcfSubmit()//Include JSP's contains different JS files which is having same fnSubmit functions.
{
	var gridrows = ""
	var gridrowsarr = "";
	var arrLen = "";
	var errorCount = "";
	gridrows = gridObj.getAllRowIds(",");
	gridrowsarr = gridrows.toString().split(",");
	arrLen = gridrowsarr.length;
		for (var i = 0; i < arrLen; i++) {
			rowId = gridrowsarr[i];
			var validIcon=gridObj.cells(rowId,valid_col).getValue();
			if(validIcon.indexOf('delete.gif')!= -1){
				errorCount = 1;
			}
		}
		if (errorCount == 1){
			Error_Details(message_operations[838]);
		}else{
			 fnSaveTransactions();
		}
		 
	if (ErrorCount > 0 ){
		Error_Show();
	    Error_Clear();	
	    return false;
	}
}
//sound for the errors
function beep() {
	var x = document.getElementById('myAudio');
	  x.play(); 
}

//Excess parts
function fnAddExcessParts(partNum,controlNo) {
	excessPart += '\n'+ partNum +'-'+controlNo+'-'+1;
	document.getElementById('enterExcess').value = excessPart;
}

function fnClose(){
	window.close();
	document.getElementById("successCont").style.display = 'none';
	/*if(removeFl == 'Y'){
		fnLoadConsignDtl();
	}*/
}

function fnLoadExistingLot(){
	document.getElementById("successCont").style.display = 'none';
	var rows = [], i = 0;
	var JSON_Array_obj = "";

	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmLoanerLotReprocess.do?method=getExistingReceivedDetails&consignmentId='+consignId+'&randomId='+Math.random());
	dhtmlxAjax.get(ajaxUrl,function(loader){
	var strJsonString =loader.xmlDoc.responseText;
	if(strJsonString!=""){
	JSON_Array_obj = JSON.parse(strJsonString);
	$.each(JSON_Array_obj, function(index,jsonObject) {

	//default array
	    if(jsonObject.PARTTYPE == '4030'){
		rows[i] = {};
		rows[i].id = i;
		rows[i].data = [];
         //only for sterile
		rows[i].data[0] = removeImg;
		rows[i].data[1] = jsonObject.PNUM;
		rows[i].data[2] = jsonObject.CNUM;
		rows[i].data[3] = jsonObject.PDESC;
		rows[i].data[4] = jsonObject.QTY;
		rows[i].data[5] = jsonObject.EXPIRYDT;
		i++;
		fnUpdateMissingQty(jsonObject.PNUM, jsonObject.QTY);
        }
	});
	if (i>0){
		var setInitWidths = "35,100,100,200,80,80";
		var setColAlign = "center,left,left,left,left,left";
		var setColTypes = "ro,ro,ro,ro,ro,ro";
		var setColSorting = "str,str,str,str,str,date";
		var removeAll = '&nbsp;<a href="javascript:fnRemoveAll()"><img alt="Click to view all comments" border="0" src="/images/dhtmlxGrid/delete.gif"/></a>';
		var setHeader = [ removeAll,"Part Number", "Control Number", "Part Desc", "Received Qty", "Expiry Dt" ];
		var setColIds ="remove,partnum,contlNum,desc,qty,expiryDate";
		var setFilter = "";
		var enableTooltips = "true,true,true,true,true,true";
		var footerArry = [];
		var gridHeight = "";
		var footerStyles = [];
		var footerExportFL = true;
		var dataHost = {};
		var pagination = "";
		dataHost.rows = rows;
		document.getElementById("dataInfo").style.display = 'block';
		gridRecLotsObj = loadDHTMLXGrid('dataInfo', gridHeight, dataHost,
				setHeader, setInitWidths, setColAlign, setColTypes,
				setColSorting, setColIds, enableTooltips, setFilter,
				footerArry, footerStyles, footerExportFL, pagination);
		//gridRecLotsObj.setColumnHidden(5, true);
		}
		
	}
});
	document.getElementById("successCont").style.display = 'none';
}
//while click on remove in received lots, validate the main grid expiry date
function removeExpiryDtValidation(partnum){
	var count = 0;
	var expiryDt_col = '';
	
	if(gridRecLotsObj != null && gridRecLotsObj != undefined && gridRecLotsObj != ''){
		pnum_col = gridRecLotsObj.getColIndexById("partnum");
		expiryDt_col = gridRecLotsObj.getColIndexById("expiryDate");
		gridrows = gridRecLotsObj.getAllRowIds(",");
	    gridrowsarr = gridrows.toString().split(",");
	    arrLen = gridrowsarr.length;
		var expiryqty_col = gridObj.getColIndexById("expiryQty");
		gridObj.cells(partnum,expiryqty_col).setValue('0');
		for (var i = 0; i < arrLen; i++) {
			rowId = gridrowsarr[i];
			if(rowId != ''){
				recPartNum = gridRecLotsObj.cellById(rowId, pnum_col).getValue();	
				if (recPartNum == partnum){
					expiryDt = gridRecLotsObj.cellById(rowId, expiryDt_col).getValue();
					if(expiryDt != ''){
						diff = dateDiff(todaysDate, expiryDt, format);
						if(diff <= 30){
							expiryFl = 'N';
							valMsgFl = 'N'
						}
						if(diff <= 14){
							expiryFl = 'Y';							
							var expiryQty = gridObj.cellById(partnum, expiryqty_col).getValue();
							expiryQty = Number(expiryQty)+1;
							gridObj.cells(partnum,expiryQty_col).setValue(expiryQty);
						}
					}
					
				}else{
					expiryFl = '';
				}
			}else{
				expiryFl = '';
			}
		}
	}
}
//This function used to save the lot Number. to call exiting function fnSaveReceivedLots
function fnSave() { //PMT-52000 - Changes
	reccount =0;
	fnStartProgress();
	fnSaveReceivedLots();
	fnStopProgress();
	fnUpdateRollbackFlag();
}
//this function used to update the roll back flag 
function fnUpdateRollbackFlag() { //PMT-52000 - Changes

	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmLoanerLotReprocess.do?method=updateRollbackFlag&consignmentId='
			+ consignId + '&randomId=' + Math.random());
	dhtmlxAjax.get(ajaxUrl,function(loader){	  
		 var response = loader.xmlDoc.responseText;			
   });
	
}
//PMT-52000 - This function used to get priority  based on Consignment id
function fnMsgLoad(){
	
	var priority = '';
	var priorityId = '';
	var priorityStr = '';
	var requestid = '';
	var prArray = new Array();
	//103802: Critical; 103803: High; 103804: Medium; 103805: Low
	prArray[103802] = "#CC3399";
	prArray[103803] = "Green";
	prArray[103804] = "Orange";
	prArray[103805] = "Black";
	prArray[103807] = "Black";
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmSetPrioritization.do?method=fetchSetPriority&consignmentId='+consignId+ '&ramdomId=' + Math.random());
	dhtmlxAjax.get(ajaxUrl,function(loader){
		var response = loader.xmlDoc.responseText;
		if(response != ''){
			priorityId = response.substring(0,response.indexOf('|'));
			priorityStr = response.substring(response.indexOf('|')+1,response.length);
			priority = priorityStr.substring(0,priorityStr.indexOf('|'));
			priority = priority.fontcolor(prArray[priorityId]);
			requestid = priorityStr.substring(priorityStr.indexOf('|')+1,priorityStr.length);
			document.getElementById("priMsg").innerHTML = "<font size=3/><b>"+priority+"</b></font>";
			document.getElementById("reqId").innerHTML = requestid;
		}
	});
}
//This function used to print PPW paperwork
function fnPDFPrintPPW(printOpt){
var ajaxUrl = fnAjaxURLAppendCmpInfo("/gmReconfigLoaner.do?method=printReconfgPrintDtl&strPrintOpt=PrintPPW&strConsignId="+val+"&ramdomId="+Math.random());
	dhtmlxAjax.get(ajaxUrl, function(loader){	
	var response = loader.xmlDoc.responseText;
	});
}
//This function used to print loaner Paper work 
function fnPDFPrintVersion(printOpt){
var ajaxUrl = fnAjaxURLAppendCmpInfo("/gmReconfigLoaner.do?method=printReconfgPrintDtl&strPrintOpt=ViewPrint&strConsignId="+val+"&ramdomId="+Math.random());
	dhtmlxAjax.get(ajaxUrl, function(loader){	
	var response = loader.xmlDoc.responseText;
	});
}
//This function used to print inspection sheet 
function fnPDFOpenDoc(printOpt){
var ajaxUrl = fnAjaxURLAppendCmpInfo("/gmReconfigLoaner.do?method=printReconfgPrintDtl&strPrintOpt=InsViewer&strConsignId="+val+"&strSetId="+setid+"&ramdomId="+Math.random());
	dhtmlxAjax.get(ajaxUrl, function(loader){	
	var response = loader.xmlDoc.responseText;
	});
}