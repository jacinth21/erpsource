function fnReload()
{
  document.frmCLRValidate.hAction.value= 'VALIDATE';
  fnStartProgress('Y');
  document.frmCLRValidate.submit();
}

function fnInitiate()
{
  document.frmCLRValidate.initiate.disabled = true;
  document.frmCLRValidate.hAction.value= 'INITIATE';
  fnStartProgress('Y');
  document.frmCLRValidate.submit();
}

function fnPrint(val)
{
	windowOpener("/GmPrintCreditServlet?hAction=PRINT&hRAID="+val,"Con1","resizable=yes,scrollbars=yes,top=250,left=200,width=800,height=300");  
}

function fnViewPartActual(val)
{
	document.frmCLRValidate.hPartID.value = val;
	document.frmCLRValidate.Cbo_DistId.value = distId;
	document.frmCLRValidate.hOpt.value = 'BYPARTDETAIL';
	
	document.frmCLRValidate.action =servletPath+"/GmSalesConsignSearchServlet";
	document.frmCLRValidate.hAction.value = "Reload";
	document.frmCLRValidate.submit();	
}

function fnLoad(){
	 if(document.frmCLRValidate.hAction.value != 'VALIDATE'){
		 document.frmCLRValidate.Txt_EDate.value = edate;
	 }else{
		 document.frmCLRValidate.Txt_EDate.value = todayDate;
	 }
 }