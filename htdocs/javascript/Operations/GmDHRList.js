function fnCallDHR(id,vid)
{
	document.frmVendor.hAction.value = 'ViewDHR';
	document.frmVendor.hDHRId.value = id;
	document.frmVendor.hVenId.value = vid;
  	document.frmVendor.submit();
}



function fnGoDHRList()
{
    var strDHRID = "";
    if(document.frmVendor.Txt_DHRID != undefined && document.frmVendor.Txt_DHRID != null){	
    	strDHRID = document.frmVendor.Txt_DHRID.value.toUpperCase();
    	document.frmVendor.Txt_DHRID.value = strDHRID;
    }
	if((document.frmVendor.hVendorId.value == '' )&& document.frmVendor.Txt_DHRID.value == ''){
		Error_Details(message[2001]);
	}
	var objFromDt = document.frmVendor.Txt_FromDate;
	var objToDt = document.frmVendor.Txt_ToDate;
	CommonDateValidation(objFromDt, gCmpDateFmt,Error_Details_Trans(message_operations[115],gCmpDateFmt));
	CommonDateValidation(objToDt, gCmpDateFmt,Error_Details_Trans(message_operations[116],gCmpDateFmt));
	var dateRange = dateDiff(objFromDt.value,objToDt.value,gCmpDateFmt);
	if(!isNaN(dateRange) && dateRange < 0){
		 Error_Details(message_operations[117]);
	}
	if (ErrorCount > 0)    
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	document.frmVendor.action = gStrServletPath + "/GmDHRReportServlet?companyInfo="+companyInfoObj;
	document.frmVendor.hAction.value = 'Go';
	document.frmVendor.hOpt.value = gStrOpt;
	fnStartProgress('Y');
	document.frmVendor.submit();
}

function fnCallEdit()
{
	Error_Clear();
	var len = document.frmVendor.hLen.value;
	var cnt = 0;
	var sfl = "";
	var obj = "";
	var objOrder = "";
	var orderType = "";
	var dhrid = '';
	var ncmrid = '';
	var mwoid='';
	if ( len == 1)
	{
		document.frmVendor.hDHRId.value = document.frmVendor.dhr.value;
		mwoid=document.frmVendor.dhr.value;
	}
	else if (len >1)
	{
		var arr = document.frmVendor.dhr;
		for (var i=0;i<arr.length ;i++ )
		{
			if (arr[i].checked == true)
			{				
				document.frmVendor.hDHRId.value = arr[i].value;
				sfl = arr[i].id;
				dhrid = arr[i].value;
				mwoid=arr[i].value;;
				obj = eval("document.frmVendor.hNcmrId"+i);
				ncmrid = obj.value;
				cnt++;
				break;
			}
		}
		if (cnt == 0)
		{
			alert(message_operations[118]);
			return;
		}
	}
	
	if(mwoid.indexOf("MWO")>0 && document.frmVendor.Cbo_Action.value!="Rec"){
		Error_Details(message[86]);
	}
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	// Called when Edit Receiving is selected
	if (document.frmVendor.Cbo_Action.value == "Rec")
	{
		if (sfl > 1)
		{
			alert(message_operations[119]);
			return;
		}
		else
		{	
			document.frmVendor.hAction.value = document.frmVendor.Cbo_Action.value;
			document.frmVendor.action = "GmDHRModifyServlet?companyInfo="+companyInfoObj;
			fnStartProgress('Y');
			document.frmVendor.submit();
		}
	}
	
	if (document.frmVendor.Cbo_Action.value == "Rollback")
	{
		if(rollBackAccess != 'Y' && rollBackAccess != 'y'){
			Error_Details(message_operations[120]);		
		}
		if (ncmrid != '')
		{
				Error_Details(message[74]);
		}
		else if (sfl > 3)
		{
				Error_Details(message[75]);	
		}
		else
		{
			document.frmVendor.hAction.value = document.frmVendor.Cbo_Action.value;
			document.frmVendor.action = "GmDHRModifyServlet?companyInfo="+companyInfoObj;
		}		
		fnSubmit();
	}

    if (document.frmVendor.Cbo_Action.value == "voidDHR")
    {
    if (ncmrid != '')
		{
				Error_Details(message[76]);		
		}
		else if (sfl > 3)
		{
				Error_Details(message[77]);
		}
		else
		{
			document.frmVendor.hAction.value = document.frmVendor.Cbo_Action.value;
			document.frmVendor.action = "GmDHRModifyServlet?companyInfo="+companyInfoObj;
		}		
    fnSubmit();
    }

}
    
function fnSubmit()
{
if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	document.frmVendor.submit();
}

// function to open popup when click on "D" icon
function fnCallDonor(dhrId, mode, vendorId, status){
	// DHR status  3: Pending Verification, 4: Verified
	if(status > 2)
	{
		fnAssignLotNum(dhrId, vendorId);		
	}
	else
	{
		windowOpener("/gmDonorInfo.do?companyInfo="+companyInfoObj+"&method=fetchDonorInfo&strOpt=load&hDhrId="+dhrId+"&hMode="+mode+"&hVendorId="+vendorId,"PrntInv","resizable=yes,scrollbars=yes,top=200,left=300,width=585,height=700");
	}
}

//To open the popup of donor details when click on "Assign Lot#"
function fnAssignLotNum(dhrId, vendorId){
	windowOpener("/gmDonorInfo.do?companyInfo="+companyInfoObj+"&method=fetchDHRdetail&strOpt=load&hDhrId="+dhrId+"&hMode=Report&hVendorId="+vendorId,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
}
 