function fnSubmit(){ 
	if(document.frmPicture.Chb_AckRback[0].checked)
	{
		fnValidateTxtFld('Txt_LogReason',message_operations[151]);
		if (ErrorCount > 0)
		{
			Error_Show();
			Error_Clear();
			return false;
		}
		document.frmPicture.hAction.value="Process_Picture";
		document.frmPicture.action=sServletPath+'/GmInHouseSetServlet';
		fnStartProgress('Y');
		document.frmPicture.submit();
	}else if(document.frmPicture.Chb_AckRback[1].checked){
		fnRollback();
	}
	if(parent.window.document.getElementById("refId") != undefined){
			parent.window.document.getElementById("refId").value = "";
			parent.window.document.getElementById("refId").focus();
	}
}

function fnRollback(){
	
	var hconsignId = document.frmPicture.hConsignId.value; 
	document.frmPicture.hAction.value = "Load"; 
	document.frmPicture.action = sServletPath+'/GmCommonCancelServlet?hTxnId='+hconsignId+'&hCancelType=RBPRS' ;
 
	document.frmPicture.submit();
}
function fnEnter()
{
		if (event.keyCode == 13)
		{ 
			fnSubmit();
		}
}
function fnOnLoad()
{
	if(document.frmPicture.Txt_LogReason!=null)
		document.frmPicture.Txt_LogReason.value='CAM 3';	 //Added outbound for PBUG-2684
}

//this function is used to change the log comments based on the selected Camera.
function fnChangeCamera(Obj) {
	var cameraValue = Obj.value;
	if (cameraValue != '0') {
		// to get the values for selected camera name
		var cameraName = document.frmPicture.Cbo_CameraId.options[document.frmPicture.Cbo_CameraId.selectedIndex].text;
		document.all.Txt_LogReason.value = cameraName + " " + default_comment + "\n"; //Append camera name and default_comment for PBUG-2684
	} else {
		// Camera select Choose one the comments to be empty.
		// In servlet getting the cookies values and set to log comments.
		document.all.Txt_LogReason.value = '';
	}
}