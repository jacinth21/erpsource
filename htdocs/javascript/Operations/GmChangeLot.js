//Function used to save the lot for the transaction
function fnSave() {
	var transactionId = document.frmChangeLot.transactionId.value;
	var partNumber = document.frmChangeLot.partNumber.value;
	var quantity = document.frmChangeLot.quantity.value;
	var oldCntrlNumber = document.frmChangeLot.oldCntrlNumber.value;
	var newCntrlNumber = document.frmChangeLot.newCntrlNumber.value;
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmChangeLotNumber.do?method=saveChangeLotNumber&partNumber='
			+ encodeURIComponent(partNumber)
			+ '&transactionId='
			+ transactionId
			+ '&quantity='
			+ quantity
			+ '&oldCntrlNumber='
			+ encodeURIComponent(oldCntrlNumber)
			+ '&newCntrlNumber='
			+ encodeURIComponent(newCntrlNumber)
			+ '&ramdomId='
			+ new Date().getTime());
	dhtmlxAjax.get(ajaxUrl, fnChangeLotAjax);
}

//Function used to show the response
function fnChangeLotAjax(loader) {
	var response = loader.xmlDoc.responseText;
	if(response != ''){
		if( response.indexOf('Control Number Updated') >= 0){
			document.getElementById("successCont").style.color = 'green';
			document.getElementById("successCont").style.fontWeight = 'bold';
		}
		else {
			document.getElementById("successCont").style.color = 'red';
			document.getElementById("successCont").style.fontWeight = 'bold';
		}
		document.getElementById("successCont").innerHTML = response;
	}
}
//Function used to reset the values entered
function fnReset(){
	document.frmChangeLot.transactionId.value = '';
	document.frmChangeLot.partNumber.value = '';
	document.frmChangeLot.quantity.value = '';
	document.frmChangeLot.oldCntrlNumber.value = '';
	document.frmChangeLot.newCntrlNumber.value = '';
	document.getElementById("successCont").style.display = 'none';
}