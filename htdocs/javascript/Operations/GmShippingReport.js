var objGridData;
var gridObj;
function initGrid(divRef,gridData)
{
	gridObj = new dhtmlXGridObject(divRef);
	gridObj.setSkin("dhx_skyblue");
	gridObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	if(deptid == 'S'){
		gridObj.enableTooltips("false,true,true,true,true,true,true");
	}else{
		gridObj.enableTooltips("false,false,false,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true");
	}	
	gridObj.init();	
	gridObj.loadXMLString(gridData);	
	gridObj.setColumnHidden(8,true);
	gridObj.enableHeaderMenu();
	return gridObj;	
}

function fnOnPageLoad(){	  
	
	var statusids = document.frmAccount.hStatus.value;
	if (statusids != '')
	{
		objgrp = document.frmAccount.Chk_GrpStatus;
		fnCheckSelections(statusids,objgrp);
	}
	
	if (objGridData != ''){
		gridObj = initGrid('grpData',objGridData);
		gridObj.attachEvent("onCheckbox", doOnCheck);
		ChkboxRefId = gridObj.getColIndexById("ChkBoxRefId");
		
		fnCheckOptionalCol();
	}
}


//Marks the status checkboxes as 'checked' on PageLoad
function fnCheckSelections(val,objgrp)
{
	var valobj = val.split(",");
	var sval = '';
	var arrlen = objgrp.length;
	
	if (arrlen > 0)
	{
		for (var j=0;j< valobj.length;j++ )
		{
			sval = valobj[j];
			for (var i=0;i< arrlen;i++ )
			{
				if (objgrp[i].value == sval)
				{
					objgrp[i].checked = true;
					break;
				}
			}
		}
	}
	else
	{
		objgrp.checked = true;
	}
}

// Mark the Optional-column check boxes as 'checked' on PageLoad
function fnCheckOptionalCol()
{
	var val = document.all.hchkOptCol.value;
	var chkOptColArr = val.split(",");
	var chkOptColArrLen = chkOptColArr.length;
	var chkOptCol = document.all.chkOptCol;
	
	for (i=0;i<chkOptColArrLen ;i++ )
	{
		for (j=0;j<chkOptCol.length ;j++ )
		{
			if (chkOptColArr[i] == chkOptCol[j].value)
			{
				document.all.chkOptCol[j].checked = true;
			}
		}
	}
	fnShowHideColumn();
}

// Show or Hide columns based on selected Optional-column checkbox on PageLoad
function fnShowHideColumn()
{
	objChkOptColArr = document.all.chkOptCol;
	objChkOptColArrLen = objChkOptColArr.length;
	if (objChkOptColArrLen > 0){
		for(i = 0; i < objChkOptColArrLen; i ++) 
		{	
			if(objChkOptColArr[i].checked == true){
				colIndex = gridObj.getColIndexById(objChkOptColArr[i].value);
				gridObj.setColumnHidden(colIndex,false);
			}else{
				colIndex = gridObj.getColIndexById(objChkOptColArr[i].value);
				gridObj.setColumnHidden(colIndex,true);
			}
					
		}
	}	
}

function fnReload(obj) 
{
	var fromDtDiff = '';
	var toDtDiff = '';
	
	document.frmAccount.hAction.value = "Reload";
	// When click load the value will be empty for Choose Action
	document.frmAccount.strConsignIds.value = '';
	if(document.frmAccount.Cbo_Shippment_Type != undefined)
		document.frmAccount.Cbo_Shippment_Type.value = 0;
	var haction = document.frmAccount.hAction.value;
	var source = '';
	var status = '';
	if(document.frmAccount.Cbo_ShippingSource  != undefined)
		source = document.frmAccount.Cbo_ShippingSource.value;
	if(document.frmAccount.Chk_GrpStatus  != undefined)
		status = document.frmAccount.hStatus.value;
	if (source == 50182 && status == 15  )//loaners and pend contrl #
	{
		Error_Details(message[2005]);
	}
//	if (source == 0 && status == 50803)//all
//	{
//		Error_Details(message[2006]);
//	}
	//if (source == 0 && status == 50802  ) //source choose one and status completed
	//{
	//	Error_Details(message[2007]);
	//}
	frmDt= document.all.Txt_FromDate.value 
	toDt = document.all.Txt_ToDate.value
	if (frmDt !='' && toDt != '')
	{
		fnValidateDropDn('Cbo_Date',message[5511]);
	}
	CommonDateValidation(document.all.Txt_FromDate,dateFmt,message[5500]);
	CommonDateValidation(document.all.Txt_ToDate,dateFmt,message[5512]);
	fromDtDiff = dateDiff(todaysdate,frmDt,dateFmt);
	toDtDiff = dateDiff(todaysdate,toDt,dateFmt);
	if (fromDtDiff > toDtDiff){
		Error_Details(message[5501]);
	}
	
	fnCheckSelectStatus();
	fnCheckSelectOptCol();
	
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	//fnStartProgress();
	fnReloadSales("","");
	//document.frmAccount.submit();
}

function fnCheckSelectStatus()
{
	var objChkStatus = document.frmAccount.Chk_GrpStatus;
	var chkstr = '';
	
	if (objChkStatus)
		{
			var statuslen = objChkStatus.length;
		}
	if (objChkStatus)
		{	
			for (var i=0 ; i < statuslen ; i++)
			{
				if (objChkStatus[i].checked == true)
				{				
					chkstr += objChkStatus[i].value + ',';
				}
			}
			document.all.hStatus.value = chkstr.substr(0,chkstr.length-1);
		}
}

function fnCheckSelectOptCol()
{
	var str = '';
	
	objChkOptColArr = document.all.chkOptCol;
	objChkOptColArrLen = objChkOptColArr.length;
	if (objChkOptColArrLen > 0){
		for(i = 0; i < objChkOptColArrLen; i ++) 
		{	
			if (objChkOptColArr[i].checked)
			{
				str = str + objChkOptColArr[i].value + ',';
			}
			
					
		}
		objChkOptColArrLen = str.length;
		str = str.substring(0,objChkOptColArrLen-1);
		document.all.hchkOptCol.value = str;
	}
}

<!--Added as part of Shipping Dashboard modification by bvidyasankar on 12/11/2008-->
function fnOpenTrack(val, shippingCarrier){
	var replaceStr = ' ';
	val = val.replace(/~/g,replaceStr);
    if(shippingCarrier == '5001'){ //fedex
         windowOpener('https://www.fedex.com/fedextrack?ascend_header=1&clienttype=dotcom&cntry_code=us&language=english&tracknumbers='+val,"Track","resizable=yes,scrollbars=yes,top=150,left=150,width=760,height=610");
    }      
    else if(shippingCarrier == '5000'){}
}

<!--Added as part of Shipping Dashboard modification by bvidyasankar on 12/11/2008-->
function fnPrintSlip(refID, sourceID, status,loanerReqID){                  //add parameter loanerReqId for PMT-42950
	if(status != '40'){ //pending shipping, pending Control, PIP, RFP
        fnPrintPicSlip(refID, sourceID);
    }
    else if(status == '40'){ //completed
		fnPrintPackSlip(refID, sourceID,status,loanerReqID);
    }
}

<!--Added as part of Shipping Dashboard modification by bvidyasankar on 12/11/2008-->
function fnPrintPicSlip(val, sourceID){
	var source = sourceID;
		if(sourceID == '50180'){//Open window for Orders
            windowOpener('/GmOrderItemServlet?hAction=PICSlip&hId='+val,"Pack","resizable=yes,scrollbars=yes, top=150,left=150,width=760,height=610");
        }
        else if((sourceID == '50181')||(sourceID == '50186')||(sourceID == '4000518')||(sourceID == '26240435')) {//Open window for Consignments , In-House Items or Returns
		    windowOpener("/GmConsignItemServlet?hAction=PicSlip&hConsignId="+val+"&ruleSource="+source,"SetPic","resizable=yes,scrollbars=yes,top=150,left=150,width=760,height=600");            
        }
        else if(sourceID == '50182'){//Open window for Loaners
            windowOpener("/GmInHouseSetServlet?hAction=ViewPrint&hConsignId="+val+"&hType="+4127+"&ruleSource="+source,"LoanPrint","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
        }
        else if(sourceID == '50183'){//Open window for Loaner Extn
           windowOpener("/GmConsignInHouseServlet?hAction=PicSlip&hConsignId="+val+"&ruleSource="+source,"SetPic","resizable=yes,scrollbars=yes,top=150,left=150,width=750,height=500");
        }
}

<!--Added as part of Shipping Dashboard modification by bvidyasankar on 12/11/2008-->
function fnPrintPackSlip(val, sourceID,status,loanerReqID){   //add parameter loanerReqId for PMT-42950
        if(sourceID == '50180'){//Open window for Orders
            windowOpener('/GmEditOrderServlet?hMode=PrintPack&hOrdId='+val,"Pack","resizable=yes,scrollbars=yes,top=150,left=150,width=760,height=610");
        }
        else if((sourceID == '50181')||(sourceID == '50186')||(sourceID == '4000518')||(sourceID == '26240435')){//Open window for Consignments , In-House Loaner Items or Returns 
		    windowOpener("/GmConsignSetServlet?hAction=PackVersion&hId="+val,"Con","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");            
        }
        else if(sourceID == '50182'){//Open window for Loaners
            windowOpener("/GmInHouseSetServlet?hAction=ViewPrint&hConsignId="+val+"&hType="+4127+"&status="+status+"&loanerReqId="+loanerReqID,"LoanPrint","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
        }
        else if(sourceID == '50183'){//Open window for Loaner Extn
            windowOpener("/GmConsignInHouseServlet?hAction=PicSlip&hConsignId="+val,"SetPack","resizable=yes,scrollbars=yes,top=150,left=150,width=750,height=500");
        }
}
function setDate(obj){
 var status = obj.value;
 var objChkStatus = document.frmAccount.Chk_GrpStatus;
 
 if(obj.checked == true && status == '40'){ //  completed	
	 document.frmAccount.Cbo_Date.value=51056;
	 document.all.Txt_FromDate.value =todaysdate;
	 document.all.Txt_ToDate.value =todaysdate;
 }
 else if (((status == '15' || status == '30' || status == '33' || status == '36') && (obj.checked != false && status == '40')) || (obj.checked == false && status == '40'))//pending Control pending status/Pending Shipping, PIP,RFP
 {
	 document.all.Txt_FromDate.value ='';
	 document.all.Txt_ToDate.value ='';
	 document.frmAccount.Cbo_Date.value=0;
 }/*else if(status == '30'){
	 	document.frmAccount.Cbo_Date.value = 4000096;
 		document.all.Txt_FromDate.value = todaysdate;
		document.all.Txt_ToDate.value = todaysdate;
 }*/
}

function fnPrintAll(){
	var count = document.frmAccount.hCntR.value;
	var refId='';
	var IHLNId='';
	var Chk_sub;
	var cnt=0;
	var type = '4127';
	var IHLNType = '';
	//document.frmAccount.hAction.value = "Reload";
	var requestId=document.frmAccount.Txt_ReqId.value
	if(requestId == ''){
		fnValidateTxtFld('Txt_ReqId',message[5502]);
	}
	for (var i =0; i < count; i++) 
	{
		obj = eval("document.frmAccount.Chk_sub"+i);
		objSource = eval("document.frmAccount.hSource"+i);
		hSource = '';
		if(obj!=null){
			varStatus = obj.value;
			if(obj.checked)
			 {
				if(objSource != null){
					hSource = objSource.value;
				}
				
/*				if(i==(count-1)){
					if(hSource== '50186'){
						IHLNId = IHLNId + varStatus;
					}else{
						refId = refId + varStatus;
					}
				}else{*/
					if(hSource== '50186'){
						IHLNId = IHLNId + varStatus + ',';
					}else{
						refId = refId + varStatus + ',';
					}
				//}
					 cnt++;
				
			  }
		}
	}
	// Validate if atleast one box checked
	if (cnt == 0) {
		Error_Details(message[5503]);
	}
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	IHLNId = IHLNId.substr(0,IHLNId.length-1);
	refId = refId.substr(0,refId.length-1);
	if(IHLNId != ''){
		type = '4119';
		IHLNType = '9110';
	}
	
//document.frmAccount.submit();
windowOpener("/GmInHouseSetServlet?hAction=PrintAll&strConsignId="+ refId +"&hType="+type+"&strIHLNId="+IHLNId+'&IHLNType='+IHLNType+'&requestId='+requestId ,"LoanPrintAll","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");

}

function fnEnter()
{
		if (event.keyCode == 13)
		{ 
			fnReload();
		}
}
// Code Added as part of PMT-637 [Loaner Printable Tag]
function fnCallGo(){	
	if(event.keyCode == 13)	{	 
		var rtVal = fnGo();
		if(rtVal == false){
			if(!e) var e = window.event;
	        e.cancelBubble = true;
	        e.returnValue = false;
	        if (e.stopPropagation) {
	                e.stopPropagation();
	                e.preventDefault();
	        }
		}
	}	
}

function fnGo(){
	fnValidateTxtFld('Txt_RefId',message[5504]);
	if (ErrorCount > 0)	{
		Error_Show();
		Error_Clear();
		return false;
	}
	var refsrc =  document.frmAccount.Txt_RefId.value;	
	if(refsrc.indexOf("$")>0){
		var arrRefsrc = refsrc.split("$");
		document.frmAccount.Txt_RefId.value = arrRefsrc[0];
		document.frmAccount.Cbo_ShippingSource.value = arrRefsrc[1];
	}	
	fnStartProgress('Y');
	document.all.submit();	
}
// When select master check box, it will be select all check box.
function fnChangedFilter(obj){
	if(obj.checked){		
		gridObj.forEachRow(function(rowId) {
				check_id = gridObj.cellById(rowId, 0).getValue();
				gridObj.cellById(rowId, 0).setChecked(true);
	   });
	}else{
		gridObj.forEachRow(function(rowId) {
			check_id = gridObj.cellById(rowId, 0).getValue();
			if(check_id == 1){
				gridObj.cellById(rowId, 0).setChecked(false);
			}
	   });			
	}
}
//Description : This function to check the select all (master check box)
function doOnCheck(rowId, cellInd, stage) {
	rowID = rowId;
	if(cellInd == 0) {
		fnSelectAllToggle('selectFSAll');
	}
	return true;
}
//Description : This function to check the select all check box
function fnSelectAllToggle( varControl){
	var objControl = eval("document.frmAccount."+varControl);
	var CheckFlag = false;
	gridObj.forEachRow(function(rowId) {
		check_id = gridObj.cellById(rowId, ChkboxRefId).getValue();
		if(check_id == 0){ // Voided
			CheckFlag = true;
		}
   });			
	if(CheckFlag){
		objControl.checked = false;// select all check box un checked.
	}else{
		objControl.checked = true;// select all check box to be checked
		
	}
}

function fnSubmit(){
	var strConsignIds = '';
	var strOtherIds = '';
	var checkedRowID = '';
	var CountryId = '';
	var ContryTemp = '';
	var ShipmentDropDownFl = false;
	var StatusDropDownFl = false;
	var PlannedShipDtDDFl = false;
	var DiffShippCntryFl = false;
	var i = 0;
	var CboShipDet = document.frmAccount.Cbo_Shippment_Type.value;
	var status = document.frmAccount.hStatus.value;
	var strCbo_Date = document.frmAccount.Cbo_Date.value;
	var strInvIds = "";

	gridObj.forEachRow(function(rowId) {
		SourceId 		= gridObj.getUserData(rowId,"SOURCE");
		CompleteStatus  = gridObj.getUserData(rowId,"STATUS");
		ContryTemp  		= gridObj.getUserData(rowId,"COUNTRYID");
		
		check_id = gridObj.cellById(rowId, ChkboxRefId).getValue();
		Ref_id = gridObj.cellById(rowId, 3).getValue();		
		if(check_id == 1){
			i++;
			ShipmentDropDownFl = true;
			//including order ids for shipment value calculation
			 if(SourceId == 50181 || (SourceId == 50180 && CboShipDet == 102540)){ // 50181:Source ID
				 strConsignIds = strConsignIds + "," + rowId;
			 }else if(CboShipDet == 102540 || CboShipDet == 102541){ // 102540:Shipment Value
				 if(Ref_id != '' && Ref_id != undefined)
					 strOtherIds = strOtherIds + "," + "<BR>" + Ref_id ;
			}
			// If any consigment we can select before/after shipment. So i commented above condition
			/*if(CompleteStatus != 40 && CboShipDet == 102540){ // 102540:Shipment Value
				StatusDropDownFl = true;
			}*/ 
			if(CboShipDet == 102541){ // 102541:Change Planned Ship Date
				if(CompleteStatus == 40)
					PlannedShipDtDDFl = true;	 
			}
			if(i == 1)	CountryId = ContryTemp;
				
			if(CboShipDet == 102540 && 	CountryId != ContryTemp){ // 102540:Shipment Value
				DiffShippCntryFl = true;
			}
			CountryId = ContryTemp;
			if(CompleteStatus == 33 || CompleteStatus == 36){	//	33-Packing In Progress, 36-Ready For Pickup
				if(Ref_id != '' && Ref_id != undefined)
					 strInvIds = strInvIds + "," + "<BR>" + Ref_id ;
			}
		}
   });	
	checkedRowID = gridObj.getCheckedRows(ChkboxRefId);
	if(checkedRowID == '' && checkedRowID != undefined)
		Error_Details(message[5505]);
	
	if(strOtherIds !=''){
		strOtherIds = strOtherIds.substring(1,strOtherIds.length);
		Error_Details(Error_Details_Trans(message[(CboShipDet == 102540?5800:5506)],strOtherIds));
	}
	if(ShipmentDropDownFl)
		fnValidateDropDn('Cbo_Shippment_Type',message[5507]);
	
	/*if(StatusDropDownFl)
		Error_Details("Please select Shipped out Consignment IDs");*/
	
	if(PlannedShipDtDDFl)
		Error_Details(message[5508]);
	
	if(DiffShippCntryFl)
		Error_Details(message[5509]);
	
	if(strInvIds != ''){
		strInvIds = strInvIds.substring(1,strInvIds.length);
		Error_Details(Error_Details_Trans(message[5510],strInvIds));
	}
	frmDt= document.all.Txt_FromDate.value 
	toDt = document.all.Txt_ToDate.value 
	if (frmDt !='' && toDt != ''){
		fnValidateDropDn('Cbo_Date',message[5511]);
	}
	
	if (ErrorCount > 0){
			Error_Show();
			Error_Clear();
			return false;
	}
	strAction = "Reload";
	document.frmAccount.strConsignIds.value = strConsignIds;
	
	w2 =  createDhtmlxWindow(600,250,true);
	if(CboShipDet == 102540)
		w2.setText(lblShipValue);
	if(CboShipDet == 102541)
		w2.setText(lblPlanShipDt);
	fnDisableScreen();
	w2.button("close").hide();
	var dhtmlxUrl = fnAjaxURLAppendCmpInfo('/GmShippingReportServlet?hStatus='+status+'&Cbo_Shippment_Type='+CboShipDet+'&Cbo_Date='+strCbo_Date+'&Txt_FromDate='+frmDt+'&Txt_ToDate='+toDt+'&hAction='+strAction+'&strConsignIds='+strConsignIds);
	w2.attachURL(dhtmlxUrl,"PRICE");
	//return false;
}

// When click choose one in date field will be empty data range 
function fnSetDate(cboDate){	
	if(cboDate == 0){ // Choose one	
		document.all.Txt_FromDate.value = '';
		document.all.Txt_ToDate.value = '';
 	}else if(cboDate == 4000096){
		document.all.Txt_FromDate.value = todaysdate;
		document.all.Txt_ToDate.value = todaysdate;
	}
}
// When getexcel export first three column should not be displayed in excel.
function fnExportExcel(){
	gridObj.setColumnHidden(0,true); // Check Box
	gridObj.setColumnHidden(1,true); // Image
	gridObj.setColumnHidden(2,true);
	gridObj.toExcel('/phpapp/excel/generate.php');
	gridObj.setColumnHidden(0,false); // Check Box
	gridObj.setColumnHidden(1,false); // Image
	gridObj.setColumnHidden(2,false);
}
//When select shipment value after close popup. Need to give choose one.
function fnClearShipmentType(){
	if(document.frmAccount.Cbo_Shippment_Type != undefined)
		document.frmAccount.Cbo_Shippment_Type.value = 0;
}

