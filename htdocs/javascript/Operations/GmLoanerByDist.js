
function fnGo()
{
      //document.frmLoaner.hAction.value = "LoanerByDist";
      var type="";
      var fromMon = document.all.Cbo_FromMonth.value;
      var fromYr = document.all.Cbo_FromYear.value;
      var toMon = document.all.Cbo_ToMonth.value;
      var toYr = document.all.Cbo_ToYear.value;
      
       if((toYr-fromYr) < 0 || ((toYr-fromYr == 0) && ((toMon-fromMon) < 0)))
            Error_Details(message_operations[153]);
      
      if(document.frmLoaner.Cbo_lnType)
            type = document.frmLoaner.Cbo_lnType.value;
      
      if(type == '0'){
            Error_Details(message_operations[154]);
      }
      if (ErrorCount > 0)
      {
                  Error_Show();
                  Error_Clear();
                  return false;
      }else {
    	  fnStartProgress('Y');
            document.frmLoaner.submit();
      }
}

function fnCallDetail(val)
{
	document.frmLoaner.DistributorID.value = val;
	document.frmLoaner.hAction.value = "LoadAccount";
	document.frmLoaner.submit();
}

function fnCallLoanerSetDetails(distid, value)
{
	document.frmLoaner.hDistId.value = distid;
	document.frmLoaner.hAction.value = "LoanerByDistBySet";
	document.frmLoaner.hOpt.value = "";
	document.frmLoaner.submit();
}

function fnCallLoanerDistDetails(setid,value)
{
	document.frmLoaner.hSetId.value = setid;
	document.frmLoaner.hAction.value = "LoanerByDist";
	document.frmLoaner.hOpt.value = "";
	document.frmLoaner.submit();
}

function fnShowLoanerUsageDetails(id, month) {
	var type = document.frmLoaner.hAction.value;
	var lnType = ""; 
	var distId = "";
	var setId = "";
	if(document.frmLoaner.Cbo_lnType)
		lnType = document.frmLoaner.Cbo_lnType.value;
	if(lnType == "")
		lnType = document.all.hLnTyp.value;
	if(type == 'LoanerByDist') {
		setId = document.frmLoaner.hSetId.value;
		windowOpener('/GmInHouseSetServlet?hAction=ViewUsageHistory&month='+month+'&distributorId='+id+'&setId='+setId+'&lnType='+lnType, 'UsageDetails', 'resizable=yes,scrollbars=yes,top=40,left=50,width=900,height=600,align=center');
	} 
	else if(type == 'LoanerByDistBySet') {
		distId = document.frmLoaner.hDistId.value;
		windowOpener('/GmInHouseSetServlet?hAction=ViewUsageHistory&month='+month+'&setId='+id+'&lnType='+lnType+'&distributorId='+distId, 'UsageDetails', 'resizable=yes,scrollbars=yes,top=40,left=50,width=900,height=600,align=center');	
	}
	else {
		windowOpener('/GmInHouseSetServlet?hAction=ViewUsageHistory&month='+month+'&consignId='+id, 'UsageDetails', 'resizable=yes,scrollbars=yes,top=40,left=50,width=900,height=600,align=center');
	}
}
