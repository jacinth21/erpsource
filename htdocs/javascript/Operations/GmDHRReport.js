//global variables are defined in GmDHRReport.jsp
function fnCallDHR(id,vid)
{
	document.frmVendor.hAction.value = 'ViewDHR';
	document.frmVendor.hDHRId.value = id;
	document.frmVendor.hVenId.value = vid;
  	document.frmVendor.submit();
}

function fnPartNumberGo()
{
	Error_Clear();
	if(document.frmVendor.Cbo_Project.value == "0" && document.frmVendor.Txt_PartNum.value == ''){
		Error_Details(message[802]);
	}
	//PC-4543 Upper Case Conversion Issue
	if(document.frmVendor.Txt_PartNum != undefined){
    	if(document.frmVendor.Txt_PartNum.value != undefined && document.frmVendor.Txt_PartNum.value != ""){
    	document.frmVendor.Txt_PartNum.value = TRIM(document.frmVendor.Txt_PartNum.value.toUpperCase());
    	}
    }
	var objFromDt = document.frmVendor.Txt_FromDate;
	var objToDt = document.frmVendor.Txt_ToDate;
	CommonDateValidation(objFromDt, gCmpDateFmt,Error_Details_Trans(message_operations[115],gCmpDateFmt));
	CommonDateValidation(objToDt, gCmpDateFmt,Error_Details_Trans(message_operations[116],gCmpDateFmt));
	var dateRange = dateDiff(objFromDt.value,objToDt.value,gCmpDateFmt);
	if(!isNaN(dateRange) && dateRange < 0){
		 Error_Details(message_operations[117]);
	}
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	document.frmVendor.action = gStrServletPath + "/GmDHRReportServlet?companyInfo="+companyInfoObj;
	document.frmVendor.hAction.value = 'Go';
	document.frmVendor.hOpt.value = gStrOpt;
	fnStartProgress('Y');
	document.frmVendor.submit();
}

function Toggle(val)
{
	
	var obj = document.getElementById("div"+val);
	var trobj = document.getElementById("tr"+val);
	var tabobj = document.getElementById("tab"+val);

	if (obj.style.display == 'none')
	{
		obj.style.display = '';
		trobj.className= "ShadeRightTableCaptionBlue";
		tabobj.style.background="#ecf6fe";
	}
	else
	{
		obj.style.display = 'none';
		trobj.className= "";
		tabobj.style.background="#ffffff";
	}
}

//To open the popup when click on "D" icon
function fnCallDonor(dhrId, vendorId){
	windowOpener("/gmDonorInfo.do?companyInfo="+companyInfoObj+"&method=fetchDonorInfo&strOpt=load&hDhrId="+dhrId+"&hMode=Report&hVendorId="+vendorId,"PrntInv","resizable=yes,scrollbars=yes,top=200,left=300,width=585,height=700");
}