function fnOnPageLoad()
{  

  var footer = new Array();
  footer[0]=message_operations[335];
  footer[1]="#cspan";
  footer[2]="#cspan";
  footer[3]="#cspan";
  footer[4]="#cspan";
  footer[5]="#cspan"
  footer[6]="<b>{#stat_total}<b>";
  footer[7]=""
  footer[8]=""
  footer[9]=""
  
  gridObj = initGridWithDistributedParsing('dataGridDiv',objGridData,footer);
  gridObj.setColTypes("ro,ro,ro,ro,ro,ron,ron,ro,ro,ro");
  gridObj.enableTooltips("false,false,false,false,false,false,false,false,false,true");
  gridObj.callEvent("onGridReconstructed",[]);
  gridObj.groupBy(0);
  gridObj.attachHeader("#select_filter,#text_filter,#text_filter,#select_filter,#text_filter,#numeric_filter");   
}
var typeArr = ['40053', '40054','4117','4118','50150','50151','50152','50153','50155','50156','50157','50158','50159','50160','50161','50162','50154','100406'];

//This prototype is provided by the Mozilla foundation and
//is distributed under the MIT license.
//http://www.ibiblio.org/pub/Linux/LICENSES/mit.license

if (!Array.prototype.indexOf)
{
Array.prototype.indexOf = function(elt /*, from*/)
{
  var len = this.length;

  var from = Number(arguments[1]) || 0;
  from = (from < 0)
       ? Math.ceil(from)
       : Math.floor(from);
  if (from < 0)
    from += len;

  for (; from < len; from++)
  {
    if (from in this &&
        this[from] === elt)
      return from;
  }
  return -1;
};
}

function fnPicSlip(val,type,refid,conType)
{
	if (conType == 'C')
	{
		windowOpener("/GmConsignItemServlet?hAction=PicSlip&hConsignId="+val+"&ruleSource="+type,"SetPic","resizable=yes,scrollbars=yes,top=150,left=150,width=760,height=600");
	}
	else if (conType == 'I')
	{
		windowOpener("/GmConsignInHouseServlet?hAction=PicSlip&hConsignId="+val+"&ruleSource="+type+"&refId="+refid,"SetPic","resizable=yes,scrollbars=yes,top=150,left=150,width=750,height=500");	
	}	
	else if (conType == 'T')
	{
		windowOpener("/gmItemControl.do?haction=PicSlip&hConsignId="+val+"&txntype="+type+"&ruleSource="+type+"&refId="+refid,"SetPic","resizable=yes,scrollbars=yes,top=150,left=150,width=750,height=500");	
	}else{
		// For Consigned Items Dashboard, the query will not have 'C' or 'I' so adding this to call the GmConsignItemServlet
		
		windowOpener("/GmConsignItemServlet?hAction=PicSlip&hConsignId="+val+"&ruleSource="+type,"SetPic","resizable=yes,scrollbars=yes,top=150,left=150,width=760,height=600");
	}
	 
}
function fnPicSlip(val,type,refid){
	windowOpener("/GmConsignInHouseServlet?hAction=PicSlip&hConsignId="+val+"&ruleSource="+type+"&refId="+refid,"SetPic","resizable=yes,scrollbars=yes,top=150,left=150,width=750,height=500");
}
function fnOpenOrdLogcsg(val)
{
	windowOpener("/GmCommonLogServlet?hType=1236&hID="+val,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
}

function fnCallEditItems(val,type,conType)
{
	var parentLcn;
	var urlStr;
	var targetUrlStr;

	document.frmOperDashBoardDispatch.hId.value = val;
	document.frmOperDashBoardDispatch.hMode.value = "CONTROL";
	document.frmOperDashBoardDispatch.hFrom.value = "ItemDashboard";
	document.frmOperDashBoardDispatch.hAction.value = "EditControl";
	document.frmOperDashBoardDispatch.hConsignId.value = val;
	document.frmOperDashBoardDispatch.hType.value = type;
	urlStr = "&hId="+val+"&hMode=CONTROL&hFrom=ItemDashboard&hAction=EditControl&hConsignId="+val+"&hType="+type;
	if (conType == 'I')
	{
		document.frmOperDashBoardDispatch.action = "GmSetAddRemPartsServlet?";
		
	}else if(conType == 'T'){
		urlStr = "&txnid="+val+"&mode=CONTROL&haction=EditControl&hConsignId="+val+"&txntype="+type+"&loctype=93343";
		document.frmOperDashBoardDispatch.action = "/gmItemControl.do?";
	}
	else
	{
		document.frmOperDashBoardDispatch.action = "GmConsignItemServlet?";	
	}
	// to append the company info
	urlStr = urlStr + "&" + fnAppendCompanyInfo();
	targetUrlStr = document.frmOperDashBoardDispatch.action+urlStr;
	parentLcn = parent.location.href;
	if(parentLcn.indexOf("GmEnterprisePortal") != -1)
	{
		location.href = targetUrlStr;
	}
	else
	{
		location.href = targetUrlStr;
	}
}

function fnCallItemsShip(val,type,conType)
{	 
	var parentLcn;
	document.frmOperDashBoardDispatch.hId.value = val;
	document.frmOperDashBoardDispatch.hFrom.value = "LoadItemShip";
	document.frmOperDashBoardDispatch.hConsignId.value = val;
	document.frmOperDashBoardDispatch.hAction.value = "LoadItemShip";
	document.frmOperDashBoardDispatch.hMode.value = "VERIFY";
	document.frmOperDashBoardDispatch.hType.value = type;
	document.frmOperDashBoardDispatch.hOpt.value = type;
	urlStr = "&hId="+val+"&hFrom=LoadItemShip&hAction=LoadItemShip&hConsignId="+val+"&hType="+type+"&hOpt="+type;
	
	// If type is inhouse consignment, need to go to shipping screen, this list of transactions for this one might increase, once all the transactions are identified
	// remove the last else condition
	
	if (type == "4112")
	{
		document.frmOperDashBoardDispatch.action = "/gmOPEditShipDetails.do?refId="+val+"&strOpt=modify&source=50181";
	}
	else if (conType == 'C')
	{
		document.frmOperDashBoardDispatch.action = "GmReprocessMaterialServlet?";
	}
	else if (conType == 'I')
	{
		document.frmOperDashBoardDispatch.action = "GmSetAddRemPartsServlet?";
		
	}else if (conType == 'T'){
		urlStr = "&txnid="+val+"&mode=VERIFY&haction=EditVerify&hConsignId="+val+"&txntype="+type+"&loctype=93345";
		document.frmOperDashBoardDispatch.action = "/gmItemVerify.do?";
	}
	else
   {                              
       document.frmOperDashBoardDispatch.action = "/gmOPEditShipDetails.do?refId="+val+"&strOpt=modify&source=50181";                
   }
	// to append the company info
	urlStr = urlStr + "&" + fnAppendCompanyInfo(); 
	targetUrlStr = document.frmOperDashBoardDispatch.action+urlStr;
	parentLcn = parent.location.href;
	
	if(parentLcn.indexOf("GmEnterprisePortal") != -1)
	{
		location.href = targetUrlStr;
	}
	else
	{
		location.href = targetUrlStr;
	}
	
}
function fnCallControl(val, type)
{	 
	var parentLcn;
	var urlStr;
	var targetUrlStr;

	document.frmCustDashBoardDispatch.hId.value = val;
	document.frmCustDashBoardDispatch.hMode.value = "CONTROL"; 
	document.frmCustDashBoardDispatch.hAction.value = "EditControl";
	document.frmCustDashBoardDispatch.hConsignId.value = val; 

	urlStr = "&txnid="+val+"&mode=CONTROL&haction=EditControl&hConsignId="+val+"&txntype="+type+"&loctype=93343";
	document.frmCustDashBoardDispatch.action = "/gmItemControl.do?";
	 
	targetUrlStr = document.frmCustDashBoardDispatch.action+urlStr;
	parentLcn = parent.location.href;
	if(parentLcn.indexOf("GmEnterprisePortal") != -1)
	{
		location.href = targetUrlStr;
	}
	else
	{
		location.href = targetUrlStr;
	}
}

function fnCallShip(val)
{
	document.frmCustDashBoardDispatch.hOrdId.value = val;
	document.frmCustDashBoardDispatch.hAction.value = "EditShip";
	document.frmCustDashBoardDispatch.action = "/GmOrderItemServlet";
	document.frmCustDashBoardDispatch.submit();
}
function fnPrintPick(val)
{

	windowOpener('/GmOrderItemServlet?hAction=PICSlip&hId='+val,"Pack","resizable=yes,scrollbars=yes,top=150,left=150,width=760,height=610");	
}

function fnViewOrder(val)
{
	windowOpener('/GmEditOrderServlet?hMode=PrintPrice&hOrdId='+val,"Summ","resizable=yes,scrollbars=yes,top=250,left=250,width=760,height=410");	
}


function fnCallSetShip(val)
{
	document.frmCustDashBoardDispatch.hId.value = val;
	document.frmCustDashBoardDispatch.hAction.value = "LoadSetShip";
	document.frmCustDashBoardDispatch.action = "/GmConsignSetServlet";
	document.frmCustDashBoardDispatch.submit();	
}
function fnCallRecon(val)
{	
	 parent.location = "/GmLoanerReconServlet?hId="+val+"&hAction=LoanRecon";
}

function fnInHousePicSlip(val)
{
	windowOpener("/GmConsignInHouseServlet?hAction=PicSlip&hConsignId="+val,"SetPic","resizable=yes,scrollbars=yes,top=150,left=150,width=750,height=500");
}

function fnCallEditTransfer(val,tsftype)
{
	document.frmCustDashBoardDispatch.hTransferId.value = val;
	document.frmCustDashBoardDispatch.hAction.value="Load";
	document.frmCustDashBoardDispatch.hTransferType.value = tsftype;
	document.frmCustDashBoardDispatch.action = "/GmAcceptTransferServlet";	
	document.frmCustDashBoardDispatch.submit();	
}
function fnCallEditDummyConsignment(val)
{
	document.frmCustDashBoardDispatch.hId.value = val;
	document.frmCustDashBoardDispatch.hAction.value="LoadSetDummy";
	document.frmCustDashBoardDispatch.action = "/GmConsignSetServlet";	
	document.frmCustDashBoardDispatch.submit();	
}
function fnShowNCMR(val)
{
windowOpener("/GmNCMRServlet?hAction=PrintNCMR&hId="+val,"NCMR","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=640");
}
function fnShowEVAL(val)
{
windowOpener("/GmNCMRServlet?hAction=PrintEVAL&hId="+val,"NCMR","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=640");
}

function fnUpdateNCMR(id,val)
{
	document.frmOperDashBoardDispatch.hNCMRId.value = id;
	document.frmOperDashBoardDispatch.hMode.value = val;
	document.frmOperDashBoardDispatch.hAction.value = "UpdateNCMR";
	document.frmOperDashBoardDispatch.action = "GmNCMRServlet";
	document.frmOperDashBoardDispatch.submit();
}

function fnUpdateDHR(id,val)
{
	parent.location = "/GmPOReceiveServlet?hAction=UpdateDHR&hFrom=DashboardDHR&hMode="+val+"&hId="+id;
}


function fnOpenForm(varPatientId, varEventId)
{
	//alert(valFormID + " = " + valStudy  + " = " + valStudyPeriodKey + " = " + valPatientID );
	document.frmAdverseEvent.Cbo_Form.value = 14;
	document.frmAdverseEvent.Cbo_Study.value = document.frmAdverseEvent.studyListId.value;
//	document.frmAdverseEvent.hStPerId.value = valStudyPeriodKey;
	document.frmAdverseEvent.hStPerId.value = 97;
	document.frmAdverseEvent.Cbo_Period.value = 6309; // 6309 is N/A
	document.frmAdverseEvent.Cbo_Patient.value = varPatientId;
	document.frmAdverseEvent.hAction.value = "LoadQues";
	document.frmAdverseEvent.cbo_EventNo.value = varEventId;
	document.frmAdverseEvent.action = "/GmFormDataEntryServlet";
	document.frmAdverseEvent.submit();

}
function fnCallInv(po,id)
{
	document.frmClinicalDashBoardDispatch.hId.value = id;
	document.frmClinicalDashBoardDispatch.hPO.value = po;
	document.frmClinicalDashBoardDispatch.hMode.value = "INV";
	document.frmClinicalDashBoardDispatch.hPgToLoad.value = "GmInvoiceServlet";
	document.frmClinicalDashBoardDispatch.submit();
}
function Toggle(val)
{

	var obj = eval("document.all.div"+val);
	var trobj = eval("document.all.tr"+val);
	var tabobj = eval("document.all.tab"+val);

	if (obj.style.display == 'none')
	{
		obj.style.display = '';
		//trobj.className="ShadeRightTableCaptionBlue";
		//tabobj.style.background="#c6e6fe";
		//tabobj.style.background="#d7ecfd";
		//tabobj.style.background="#ecf6fe";
	}
	else
	{
		obj.style.display = 'none';
		//trobj.className="";
		//tabobj.style.background="#ffffff";
	}
}

function fnFormDetails(valFormID, valStudy, valStudyPeriodKey, valCboPeriod, valPatientID)
{
	document.frmClinicalDashBoardDispatch.action = "/GmFormDataEntryServlet";
	document.frmClinicalDashBoardDispatch.hAction.value="LoadQues";
	document.frmClinicalDashBoardDispatch.Cbo_Form.value = valFormID;
	document.frmClinicalDashBoardDispatch.Cbo_Study.value = valStudy;
	document.frmClinicalDashBoardDispatch.hStPerId.value = valStudyPeriodKey;
	document.frmClinicalDashBoardDispatch.Cbo_Period.value = valCboPeriod;
	document.frmClinicalDashBoardDispatch.Cbo_Patient.value = valPatientID;
	document.frmClinicalDashBoardDispatch.submit();
}
function fnSubmit1()
{

	
	//document.frmClinicalDashBoardDispatch.action = "/gmClinicalDashBoardDispatch.do?method=pendingForm";
	document.frmClinicalDashBoardDispatch.hAction.value="Load";

	 loadajaxpage('/gmClinicalDashBoardDispatch.do?method=pendingForms&studyListId='+document.frmClinicalDashBoardDispatch.studyListId.value+'&craId='+document.frmClinicalDashBoardDispatch.craId.value,'ajaxdivcontentarea');
}
function fnSubmit2()
{


	//document.frmClinicalDashBoardDispatch.action = "/gmClinicalDashBoardDispatch.do?method=pendingForm";
	document.frmClinicalDashBoardDispatch.hAction.value="Load";

	 loadajaxpage('/gmClinicalDashBoardDispatch.do?method=outOfWindow&studyListId='+document.frmClinicalDashBoardDispatch.studyListId.value+'&craId='+document.frmClinicalDashBoardDispatch.craId.value,'ajaxdivcontentarea');
}
function fnSubmit3()
{


	//document.frmClinicalDashBoardDispatch.action = "/gmClinicalDashBoardDispatch.do?method=pendingForm";
	document.frmClinicalDashBoardDispatch.hAction.value="Load";

	 loadajaxpage('/gmClinicalDashBoardDispatch.do?method=pendingVerification&studyListId='+document.frmClinicalDashBoardDispatch.studyListId.value+'&craId='+document.frmClinicalDashBoardDispatch.craId.value,'ajaxdivcontentarea');
}

function fnViewDetails(strReqId,strConId)
{    
    windowOpener("/gmRequestMaster.do?strOpt=print&requestId="+strReqId+"&consignID="+strConId,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=920,height=600");
  }
function fnUpdateNCMREval(id,val)
{
	document.frmOperDashBoardDispatch.hNCMRId.value = id;
	document.frmOperDashBoardDispatch.hMode.value = val;
	document.frmOperDashBoardDispatch.hAction.value = "UpdateNCMREVAL";
	document.frmOperDashBoardDispatch.action = "GmNCMRServlet";
	document.frmOperDashBoardDispatch.submit();
}

function fnCallEditReturn(val)
{
	/*document.frmCustDashBoardDispatch.action = "GmReturnProcessServlet";
	document.frmCustDashBoardDispatch.hAction.value = 'Reload';
	document.frmCustDashBoardDispatch.hId.value = val;
	document.frmCustDashBoardDispatch.submit();
	*/
	parent.location.href  ="/GmReturnProcessServlet?hAction=Reload&hId="+val;
}
function fnCallReturn(val)
{
/*	document.frmCustDashBoardDispatch.hRAId.value = val;
	document.frmCustDashBoardDispatch.hAction.value = "LoadCredit";
	document.frmCustDashBoardDispatch.action = "/GmReturnCreditServlet";
	document.frmCustDashBoardDispatch.submit();
	*/
	parent.location.href  ="/GmReturnCreditServlet?hAction=LoadCredit&hRAId="+val;;
}
function fnViewReturns(val)
{
	windowOpener('/GmPrintCreditServlet?hAction=VIEW&hRAID='+val,"Returns","resizable=yes,scrollbars=yes,top=150,left=200,width=660,height=400");
}
//Open Back order report from Process Transaction from WIP-Loaners status
function fnOpenLoanerItemsBORpt(refId)
{
	var type = '';
	
	//4127 - Loaner ,50266 - Loaner Items Back Order,
	//4119 - InHouse Loaner ,50269 - In House Loaner items
	if(cnType == 4127) {
		type = '50266';
	}else if(cnType == 4119) {
		type = '50269';
	}
	
	location.href  ="/GmOrderItemServlet?Cbo_Type="+type+"&hOpt=ProcBO&hFrom=PROCESS_TXN_SCN&REFID="+refId+"&"+fnAppendCompanyInfo();
}
//To move loaner set from WIP-Loaners to Pending Process
function fnMoveSetToPendProcess(consignID){
	
	hredirectURL = "/gmModifyControlNumber.do?REFID="+consignID+"&"+fnAppendCompanyInfo();
	location.href  ="GmCommonCancelServlet?hTxnName="+consignID+"&hRedirectURL="+hredirectURL+"&hDisplayNm=Process Transaction&hScreenFrom=PROCESS_TXN_SCN&hAction=Load&hCancelType=MVPROC&hTxnId="+consignID+"&"+fnAppendCompanyInfo();
}
