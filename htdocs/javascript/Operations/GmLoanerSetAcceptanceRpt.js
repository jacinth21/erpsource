var gridObj;
// To load report 
function fnOnPageLoad() {

	if (objGridData.value != '') {
		gridObj = initGridData('LoanerSetAcceptanceReport', objGridData);
		gridObj.attachHeader('#text_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#text_filter,#text_filter,#text_filter');
	}
}
//To initiate the dhtmlx grid
function initGridData(divRef, gridData) {
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.init();
    gObj.loadXMLString(gridData);
	return gObj;
}

//To load the report
function fnLoad(){
	var objFromDt = "";
	var objToDt="";
	var fromDt = "";
	var toDate="";
	var errDate ="";

	
	objFromDt = document.frmLoanerSetAcceptanceReport.startDtFrom;
	objToDt = document.frmLoanerSetAcceptanceReport.startDtTo;
	
	fromDt=objFromDt.value;
	toDate = objToDt.value;
	
	if((fromDt == "") && (toDate == "")){
		Error_Details("Please Enter the Start and To Date");
	}
	if((fromDt == "") && (toDate != "")){
		//fnValidateTxtFld('fromDate',message[10527]);
		Error_Details(message[10535]);
	}else if((fromDt != "") && (toDate == "")){
		//fnValidateTxtFld('toDate',message[10528]);
		Error_Details(message[10536]);
	}
	
	
	CommonDateValidation(objFromDt,date_format,message[10600]);
	CommonDateValidation(objToDt,date_format,message[10601]);
	//From date should not be greater than To Date
	if (dateDiff(fromDt, toDate,date_format) < 0){
	    Error_Details(message[10579]);
	}
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false; 
	}
	
    fnStartProgress("Y");
    document.frmLoanerSetAcceptanceReport.strOpt.value= "reload";
    document.frmLoanerSetAcceptanceReport.submit();
    
}

//To download excel;
function fnExcel(){
	if(gridObj!=undefined && gridObj!=''){
		gridObj.toExcel('/phpapp/excel/generate.php');
	}
	
}