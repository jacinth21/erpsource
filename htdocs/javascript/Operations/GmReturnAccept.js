var strErrMsg = "";
var excessPartQtyStr = ',';
var errparts ;
var tissueControls = "";
function fnSubmit()
{
	tissueControls = '';
	var val = document.frmVendor.hAction.value;
	var strType = document.frmVendor.hRetType.value;
	var strCntrlMsg = "";
	var strZeroRetMsg = "";
	var strNumErrMsg = "";
	var strInvCnumMsg = "";
	var blComplete = false;
	//var blCompleteType = false;
	var retDate = document.frmVendor.Txt_RetDate;
	var curDate = new Date();
	var curFmtDate = compCurrDt;
	var inputStr = '';
	var partNumArr = document.getElementById("hPartNumArr").value;
	var inputStrTemp = "";
	var totalRejQty = 0;
	var partNoTempArr;
	var validQtyMsg = "";
	var blLnItemType = false;
	var blConsItemType = false;
	var blReconfig = false;
	var errorTissueParts = "";
	
	// To get the date format based on company
/* 
	if (format=='MM/dd/yyyy')
		{
			curFmtDate =  (curDate.getMonth()+1) + "/" +  curDate.getDate() + "/" + curDate.getFullYear();
				
		}
	else
		{
			curFmtDate = curDate.getDate() + "/" + (curDate.getMonth()+1) + "/" + curDate.getFullYear();	
		}*/
	 
	var dateDiffs = dateDiff( curFmtDate,retDate.value, format); 
	if(document.frmVendor.Chk_RAComplete.checked == true)
    {
     document.frmVendor.Chk_RAComplete.value = 1;
     blComplete = true;
    }
    else
    {
     document.frmVendor.Chk_RAComplete.value = 0;
    }
    // Code added for allow submitting the screen for partially...
	/*
	if(strType == '3308' || strType == '3309' || strType == '3302'|| strType == '3304'){
			blCompleteType = true;
		}*/
	
	if(strType == '3308' || strType == '3309' ){//3308: IH Loaner - Items; 3309: Product Loaner - Items;
		blLnItemType = true;
	}
	
	//3302: Consignment - Items; 3301: Consignment - Sets; 3300: Sales - Items ; 3307 -- ICT Returns ; 3306 - Consignment - Distributor Closure; 3304: Account Consignment
	if(strType == '3302' || strType == '3301' || strType == '3300' || strType == '3307' || strType == '3306'|| strType == '3304'){
		blConsItemType = true;
	}
	
	if(strAction == 'ReloadReconf'){// For Reconfiguration(Partial return) screen
		blReconfig = true;
	}
	
	hcnt = parseInt(document.getElementById("hCnt").value);
	hcnt = hcnt+cnt;
	//document.frmVendor.hCnt.value = hcnt;  
	
	if(blLnItemType || blConsItemType){
		// Commenting below code, because if there is only one record, part number is displaying two times
		// Below code is added for the single row processing.
		//if(hcnt == 0 ){
		//	hcnt = 1;
		//	document.frmVendor.hCnt.value = hcnt;  
		//}

		for(var i=0;i<=hcnt;i++){
			if(eval("document.frmVendor.hPartNum"+i) != undefined){ // No need to include the deleted row
				if(eval("document.frmVendor.hPartNumTotalQty"+i)){
					var loanedObj = eval("document.frmVendor.hPartNumTotalQty"+i);
				}
				if(eval("document.frmVendor.Txt_Qty"+i)){
					var retObj = eval("document.frmVendor.Txt_Qty"+i);
				}
				
				if(eval("document.frmVendor.hPartNum"+i)){
					var partnum = eval("document.frmVendor.hPartNum"+i).value;
				}
				if(eval("document.frmVendor.hControl"+i)){
					var orgCnum = eval("document.frmVendor.hControl"+i).value;
				}
				if(eval("document.frmVendor.Txt_CNum"+i)){
					var cnum = eval("document.frmVendor.Txt_CNum"+i).value.toUpperCase();
				}
				if(eval("document.frmVendor.hItemType"+i)){
					var itemType = eval("document.frmVendor.hItemType"+i).value;
				}
				if(eval("document.frmVendor.hItemPrice"+i)){
					var item_price = eval("document.frmVendor.hItemPrice"+i).value;
				}
				if(eval("document.frmVendor.hUnitPrice"+i)){
					var unit_price = eval("document.frmVendor.hUnitPrice"+i).value;
				}
				if(eval("document.frmVendor.hUnitPriceAdj"+i)){
					var unit_price_adj = eval("document.frmVendor.hUnitPriceAdj"+i).value;
				}
				if(eval("document.frmVendor.hAdjCode"+i)){
					var adj_code = eval("document.frmVendor.hAdjCode"+i).value;
				}				
				var l_qty = 0;
				var ret_qty = 0;
				if(loanedObj)
					l_qty = parseInt(loanedObj.value);
				if(retObj)
					ret_qty = parseInt(retObj.value);
				
				if(ret_qty < 0){// To check the negative qty
					validQtyMsg += partnum + ',';
				}
		
				if(isNaN(ret_qty) && cnum != '' && cnum != 'TBE' && strNumErrMsg.indexOf(partnum) == -1){
					strNumErrMsg += "," + partnum  ;
				}/*else if(l_qty < ret_qty   && strErrMsg.indexOf(partnum) == -1){
					strErrMsg += "," + partnum  ;
				}*/
	
				if(blComplete == false && ret_qty == 0 && strZeroRetMsg.indexOf(partnum) == -1){
					strZeroRetMsg += "," + partnum;
				}
				// The NOC# is removed because of some part have control number as NOC# like 685.005.
				if( !isNaN(ret_qty) && (cnum == '' || cnum == 'TBE' ) && blComplete == false && blLnItemType == true && strCntrlMsg.indexOf(partnum) == -1){
					strCntrlMsg += "," + partnum;
				}
				if(blComplete == true && ret_qty > 0 &&  (cnum == '' || cnum == 'TBE' ) && strCntrlMsg.indexOf(partnum) == -1){
					strCntrlMsg += "," + partnum;
				}
	
				if((cnum != '' && cnum != 'TBE' ) && (orgCnum != cnum) && blLnItemType  && strInvCnumMsg.indexOf(partnum) == -1){
					strInvCnumMsg += "," + partnum;
				}
				// Validation for not allowing Scanning same COntrol Number Twice for Tissue Parts
				if(document.getElementById("hPartMaterialType"+i) != undefined){
					var pnumMaterialObj = document.getElementById("hPartMaterialType"+i);
					pnumMaterial = pnumMaterialObj.value;
				}			
				var errparts = fnValidateAllControlNo(partnum,cnum,ret_qty,pnumMaterial,blComplete);		
				if(errparts != ''){
					errorTissueParts = errorTissueParts + "<br>" +errparts;
				}
				if(cnum != 'TBE' && !isNaN(ret_qty) && ret_qty != ''){
					inputStr = inputStr +  partnum + ',' + ret_qty + ',' + cnum + ','+ orgCnum + ','+ itemType + ','+item_price+','+unit_price+ ','+unit_price_adj+ ','+ adj_code+ '|';
				}
			}
		}
	}
	
	// To fix the issue of showing incorrect validation for "Return Quantity exceeds the Initiated Quantity"
	inputStrTemp = inputStr
	partNoTempArr = partNumArr;
	strErrMsg = "";
    excessPartQtyStr = ',';
	fnCalTotalQty(inputStrTemp, partNoTempArr, blReconfig, strTypeid);
	
	if(blLnItemType){
		if(strNumErrMsg != ''){
			Error_Details(Error_Details_Trans(message_operations[321],strNumErrMsg.substr(1,strNumErrMsg.length)));
		}
		if(strErrMsg !=''){
			Error_Details(Error_Details_Trans(message_operations[322],strErrMsg.substr(1,strErrMsg.length)));
		}
		if(strZeroRetMsg != ''){
			Error_Details(Error_Details_Trans(message_operations[323],strZeroRetMsg.substr(1,strZeroRetMsg.length)));
		}
		if(strInvCnumMsg !=''){
			Error_Details(Error_Details_Trans(message_operations[324],strInvCnumMsg.substr(1,strInvCnumMsg.length)));
		}
	}
	if(strTypeid == '3304' ){//3304:Account Consignment
		if(strErrMsg !='' && !blReconfig){
			Error_Details(Error_Details_Trans(message_operations[325],strErrMsg.substr(1,strErrMsg.length)));
		}
		excessPartQtyStr = excessPartQtyStr.substr(1,excessPartQtyStr.length-2);
		if(excessPartQtyStr != '' && blReconfig){// For Account Consignment
	    	Error_Details(Error_Details_Trans(message_operations[326],excessPartQtyStr));
     	}
	}
	if(errorTissueParts != ""){
		Error_Details(Error_Details_Trans(message_operations[327],errorTissueParts));
	}
	
	if(validQtyMsg != ''){// Should not allow to save -ve qty for returns
		Error_Details(Error_Details_Trans(message_operations[328],validQtyMsg.substr(0,validQtyMsg.length-1)));
	}
		
	if(strCntrlMsg !=''){ //PMT-3799
		Error_Details(Error_Details_Trans(message_operations[329],strCntrlMsg.substr(1,strCntrlMsg.length)));
	}
		
	CommonDateValidation(retDate,format,message_operations[330]+ message[611]);
	
	if(dateDiffs>0){
		Error_Details(message_operations[331]);
	}
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
  if(val == "ReloadReconf" || val == "SaveReconf")
    {
     document.frmVendor.hAction.value = 'SaveReconf';
     document.frmVendor.action="/GmReturnReconfRecServlet";
    }
  else
   {      
		document.frmVendor.hAction.value = 'Save';
		if(document.frmVendor.Chk_RAComplete.checked == true)
		{
			document.frmVendor.txnStatus.value = 'VERIFY';
		}
		if(document.frmVendor.chkRule)
			document.frmVendor.chkRule.value = 'Yes';
		//document.frmVendor.action = '/gmRuleEngine.do';
  	}
  
  /*document.frmVendor.hInputString.value = inputStr;
  document.frmVendor.hReturnType.value = strType;*/
  document.getElementById("hCnt").value = hcnt;
  fnStartProgress('Y');
  document.frmVendor.submit();
  	
}

//To calculate the total qty for a part if the qty is splitting to multiple rows
function fnCalTotalQty(inputStrTemp, partNoTempArr, blReconfig, strTypeid){
	var partNo, partNoTemp, inputStrTemp2, partNoQty, remainingStr, itemTyp, OrgPartQty, ConsQty;
	//Loop through the part number array to get the orginal part details
	while(partNoTempArr.indexOf('|') != '-1'){
		partNo = partNoTempArr.substring(0,partNoTempArr.indexOf(','));
		
		partNoTempArr =  partNoTempArr.substring(partNo.length+1,partNoTempArr.lastIndexOf('|')+1);
		OrgPartQty = partNoTempArr.substring(0,partNoTempArr.indexOf(','));
		partNoTempArr =  partNoTempArr.substring(OrgPartQty.length+1,partNoTempArr.lastIndexOf('|')+1);
		ConsQty = partNoTempArr.substring(0,partNoTempArr.indexOf('|'));
		partNoTempArr =  partNoTempArr.substring(ConsQty.length+1,partNoTempArr.lastIndexOf('|')+1);
		inputStrTemp2 = inputStrTemp;
		totalRejQty = 0;
		
		// Looping through each row to get the total Ret. qty corresponding to the part
		while(inputStrTemp2.indexOf('|') != '-1'){
			partNoTemp = inputStrTemp2.substring(0,inputStrTemp2.indexOf(','));
			inputStrTemp2 = inputStrTemp2.substring(partNoTemp.length+1,inputStrTemp2.lastIndexOf('|')+1);
			partNoQty = inputStrTemp2.substring(0,inputStrTemp2.indexOf(','));
			inputStrTemp2 = inputStrTemp2.substring(partNoQty.length+1,inputStrTemp2.lastIndexOf('|')+1);
			remainingStr = inputStrTemp2.substring(0,inputStrTemp2.indexOf('|'));
			inputStrTemp2 = inputStrTemp2.substring(remainingStr.length+1,inputStrTemp2.lastIndexOf('|')+1);
			
			if(partNo == partNoTemp){// Getting the total qty for a particular part number
				totalRejQty = totalRejQty+parseInt(partNoQty);
			}
		}
		if(OrgPartQty < totalRejQty   && strErrMsg.indexOf(partNo) == -1 && !blReconfig){
			strErrMsg += "," + partNo  ;
		}
		//Only for account consignment (3304)
		if(ConsQty < totalRejQty   && excessPartQtyStr.indexOf(','+partNo+',') == -1 && strTypeid == '3304' && blReconfig){//3304: Account consignment item
			excessPartQtyStr += partNo + ',';
		}
		
	}
}

function fnSetCheck(val)
{
	if (val.checked == true)
	{
		document.frmVendor.hStatusFl.value = "1";
	}
	else
	{
		document.frmVendor.hStatusFl.value = "";
	}
}

function fnCallSplit(val)
{
	var QtyObj = eval("document.all.Qty"+val);
	var CNumObj = eval("document.all.CNum"+val);
	
	var NewQtyHtml = "<input type='text' size='3' value='' name='Txt_Qty' class='InputArea' onFocus='changeBgColor(this,'#AACCE8');' onBlur='changeBgColor(this,'#ffffff');' tabindex=1>";

	var NewCNumHtml = "<input type='text' size='3' value='' name='Txt_CNum' class='InputArea' onFocus='changeBgColor(this,'#AACCE8');' onBlur='changeBgColor(this,'#ffffff');' tabindex=1>";

	//alert(val);
	var ob = document.all.myTable.rows;
	//alert(ob);
	for (var i=0;i<ob.length;i++)
	{
		rownum = ob[i].id;
		//alert(rownum);
		if (rownum == val)
		{
			num = ob[i].rowIndex;
			//alert("ROWINDEX IS:"+num);
	//document.all.myTable.deleteRow(val);
			break;
		}
	}
	document.all.myTable.deleteRow(num);

}

//When click on Generate button to generate control numbers
function fnGenerate(pnum,controlno){
	var cnum = '';
	var val= '';
	var control = new Object();
	var cnumstatus = false;
	var qtyobj = new Object();
	var qty = 0;
	var qtycnt = 0;
	var orgqty = 0;
	var vPnumProdMetType = '';
    var pnum1 = pnum.replace(/\./g,'');
    var blk = document.getElementById("hQty"+pnum1);
    if(blk != null){
    	orgqty = parseInt(blk.value);
    }
	var pnumlistobj = document.getElementById(pnum); 
	if(pnumlistobj != null){
	var pnumlist = pnumlistobj.value;
	var pnumlist = pnumlist.substring(0,pnumlist.length-1);
	var cnumarr= pnumlist.split(",");
	var cnumsize = cnumarr.length;
	//couting all sum of qty for passing part
		if(cnumsize > 0){
			for(i=0; i<cnumsize; i++){
				 qtyobj = document.getElementById("Txt_Qty"+cnumarr[i]);
				 var cnumobj = document.getElementById('Txt_CNum'+cnumarr[i]);
				 if(qtyobj != null){
					 qty = parseInt(qtyobj.value);
					 if(cnumobj != null){
						 cnum = cnumobj.value;
					 }
					 if (qty > 0 && qty != "" && cnum != null && cnum != '' && cnum != 'TBE'){
						qtycnt = qtycnt + qty;
					}
				 }
			}
			if(qtycnt >= orgqty && strAction != 'ReloadReconf'){
				Error_Details(Error_Details_Trans(message_operations[332],pnum));
				return false;
			}else{
				// updating control number and qty in existing row
				for(i=0; i<cnumsize; i++){
					val = cnumarr[0];
					var cnumobj = document.getElementById('Txt_CNum'+cnumarr[i]); 					
					if(cnumobj != null){
						qtyobj = document.getElementById("Txt_Qty"+cnumarr[i]);
						qty = parseInt(qtyobj.value);
						cnum = cnumobj.value;
						 if (cnum == controlno){							 
								 if(qty+1 > orgqty && strAction != 'ReloadReconf'){									 
									 Error_Details(Error_Details_Trans(message_operations[332],pnum));
									 return false;
								 }else{
									 if(validateObject(document.getElementById("partMaterialType"))){// Getting Part num Product Material type
										  vPnumProdMetType = document.getElementById("partMaterialType").value;	
									 }
									 if(vPnumProdMetType == '100845'){ // Tissue Part
										 var array=[pnum,cnum];
										  Error_Details(Error_Details_Trans(message_operations[333],array));
										  return false;
									 }else{
										 qtyobj.value = qty + 1;
									 }
								 }							 
							 cnumstatus = false;
							 break;	
						 }else if(cnum == null || cnum == '' || cnum == 'TBE'){
							 cnumobj.value = controlno;
							 qtyobj.value = 1;
							 cnumstatus = false;						
							 break;						
						 }else{
							 cnumstatus = true;							
						 }		
					}
				}
				
			}
		}
	}else{
		Error_Details(Error_Details_Trans(message_operations[334],pnum));
		return false;
	}
	//updating control number and qty in newly added row
	if(cnumstatus){
		fnSplit(val,pnum,pickRule,'','','','','','Y');
		var count = parseInt(document.getElementById("hNewCnt").value);	
		var hcount = parseInt(document.getElementById("hCnt").value);
		hcount = hcount+count;
		var cnumobj = document.getElementById('Txt_CNum'+hcount); 
		cnumobj.value = controlno;				 		
		qtyobj = document.getElementById("Txt_Qty"+hcount);
		qtyobj.value = 1;
	}
	
	if (ErrorCount > 0 ){
		Error_Show();
	    Error_Clear();		
	}else{
		fnScannedPartCount(pnum);
		//caling rule validation
		if(vIncludeRuleEng != 'NO'){
			var ruleTransType = "";
			var TransTypeID = ""; 
			control.value = controlno;
			fnFetchMessages(control,pnum,'PROCESS',ruleTransType,TransTypeID);
		}
	}	
}

function fnSplit(val,pnum,porgCont,strTagFl,itemPrice,unitPrice,unitPriceAdj,adjCode,scanned){
	cnt = parseInt(document.getElementById("hNewCnt").value);
	cnt++;
	document.getElementById("hNewCnt").value = cnt;
	var hcnt = parseInt(document.getElementById("hCnt").value);
	hcnt = hcnt+cnt;	
	var itemobj = eval("document.frmVendor.hItemType"+val);
	var itype = itemobj.value;
	var NewQtyHtml = "";
	var pnumobj = document.getElementById(pnum); 
	var pnum = pnumobj.value;
	pnumobj.value=pnumobj.value+hcnt+",";
	var pnumobj = document.getElementById("hPartNum"+val); 
	var pnum = pnumobj.value;
	var pnumProdMaterialObj = document.getElementById("hPartMaterialType"+val);
	var pnumProdMaterialVal =  pnumProdMaterialObj.value;	
	var pnumProdMaterialStr = "";
	if (typeId == '3308'){
		NewQtyHtml = "<span id='Sp"+cnt+"'><BR><input type='hidden' name='hPartNum"+hcnt +"' value='"+pnum + 
					 "'><input type='hidden' name='strTagFl"+hcnt+"' value='"+strTagFl+"'><input type='hidden' name='hItemType"+hcnt+"' value='"+itype+"'><input type='hidden' name='hItemPrice"+hcnt+"' value='"+itemPrice+"'><input type='hidden' name='hUnitPrice"+hcnt+"' value='"+unitPrice+"'><input type='hidden' name='hUnitPriceAdj"+hcnt+"' value='"+unitPriceAdj+"'><input type='hidden' name='hAdjCode"+hcnt+"' value='"+adjCode+"'><input type='hidden' name='hOrgCNum" + hcnt + "' value='" + porgCont + 
					 "'><input type='hidden' name='hPartMaterialType"+hcnt+"' value='"+pnumProdMaterialVal+"'><input type='text' size='3' value='' name='Txt_Qty"+hcnt+"' id='Txt_Qty"+hcnt+"' class='InputArea' onFocus=changeTRBgColor("+val+"," + 
					 "'#AACCE8'); onBlur=changeTRBgColor("+val+",'#ffffff'); tabindex=1>&nbsp;&nbsp;&nbsp;" + 
					 "<input type='text' size='20' value='" + porgCont + "' name='Txt_CNum"+hcnt+"' id='Txt_CNum"+hcnt+"' class='InputArea' onFocus=changeTRBgColor("+val+"," + 
					 "'#AACCE8'); onBlur=changeTRBgColor("+val+",'#ffffff'); tabindex=1>&nbsp;<a href=\"javascript:fnDelete('"+pnum+"',"+cnt +",'"+scanned+"')\"; ><img src=/images/btn_remove.gif border=0></a></span>" + 
					 "<input type='hidden' name='hControl" + hcnt + "' value='"+ porgCont+ "'/>";
	}else{
		NewQtyHtml = "<span id='Sp"+cnt+"'><BR><input type='hidden' name='hPartNum"+hcnt +"' value='"+pnum + 
		 "'><input type='hidden' name='strTagFl"+hcnt+"' value='"+strTagFl+"'><input type='hidden' name='hItemType"+hcnt+"' value='"+itype+"'><input type='hidden' name='hItemPrice"+hcnt+"' value='"+itemPrice+"'><input type='hidden' name='hUnitPrice"+hcnt+"' value='"+unitPrice+"'><input type='hidden' name='hUnitPriceAdj"+hcnt+"' value='"+unitPriceAdj+"'><input type='hidden' name='hAdjCode"+hcnt+"' value='"+adjCode+"'><input type='hidden' name='hOrgCNum" + hcnt + "' value='" + porgCont + 
		 "'><input type='hidden' name='hPartMaterialType"+hcnt+"' value='"+pnumProdMaterialVal+"'><input type='text' size='3' value='' name='Txt_Qty"+hcnt+"' id='Txt_Qty"+hcnt+"' class='InputArea' onFocus=changeTRBgColor("+val+"," + 
		 "'#AACCE8'); onBlur=changeTRBgColor("+val+",'#ffffff'); tabindex=1>&nbsp;&nbsp;&nbsp;" + 
		 "<input type='text' size='20' value='' name='Txt_CNum"+hcnt+"' id='Txt_CNum"+hcnt+"' class='InputArea' onFocus=changeTRBgColor("+val+"," + 
		 "'#AACCE8'); onBlur=changeTRBgColor("+val+",'#ffffff'); tabindex=1>&nbsp;<a href=\"javascript:fnDelete('"+pnum+"',"+cnt +",'"+scanned+"')\"; ><img src=/images/btn_remove.gif border=0></a></span>";
	}
	var QtyObj = eval("document.all.Cell"+val);
	QtyObj.insertAdjacentHTML("beforeEnd",NewQtyHtml);
}


function fnDelete(pnum,val,scanned){
	obj = eval("document.all.Sp"+val);
	obj.innerHTML = "";
	fnDeleteScanned(pnum,scanned); 
}

function fnVoidReturn()
{
		document.frmVendor.hTxnId.value = document.frmVendor.hRAId.value;
		document.frmVendor.action ="/GmCommonCancelServlet";
		document.frmVendor.hAction.value = "Load";
		document.frmVendor.submit();
}

function fnReload()
{
	var addPartNum = document.all.Txt_PartNums.value;
	var partNumLen = addPartNum.length;
	var addPartNumTemp = addPartNum+',';
	var addPartNumSubTemp;
	var partNo;
	var invalidPartArr = '';
	var val = document.frmVendor.hAction.value;
  	if(val == "LoadReconf" || val == "ReloadReconf")
    {
      document.frmVendor.hAction.value = "ReloadReconf";
	  document.frmVendor.action="/GmReturnReconfRecServlet";    
	  
	  while(partNumLen > 0){
			partNo = addPartNumTemp.substring(0,addPartNumTemp.indexOf(','));
			addPartNumTemp = addPartNumTemp.substring(partNo.length+1,addPartNumTemp.lastIndexOf(',')+1);
			partNumLen = addPartNumTemp.length;
			
			if(raPartNums.indexOf(partNo) == -1 && strTypeid == '3304'){// if the part number is not consigned to an account, should show validation
					invalidPartArr += partNo + ',';
			}
		}
	  /* The following Code is commented as part of TKT-19151 as we are going to fix the issue in gm_pkg_op_return.get_returned_parts():
	   * This will be enable once the TSK-6166 is done.
	   
		if(invalidPartArr != ''){
			Error_Details("Cannot initiate returns for part(s) not consignment to the Account: "+invalidPartArr.substr(0,invalidPartArr.length-1));
			Error_Show();
			Error_Clear();
			return;
		}
	  */
    }
  else
   { 
		document.frmVendor.hAction.value = "Reload";
   }
  fnStartProgress('Y');
    document.frmVendor.submit();  		
}

function fnLoad()
{
   fnExistScanPartCount();
   document.frmVendor.Chk_RAComplete.value = RaComplete;
   
   if(document.frmVendor.Chk_RAComplete.value == "1" && errCnt == 0)
    {
     document.frmVendor.Chk_RAComplete.checked = "true";
     document.frmVendor.Btn_Void.disabled="true";
     document.frmVendor.Btn_Submit.disabled="true";
    }
    
}

function fnCallTransactionSearch()
{
windowOpener('/GmConsignSearchServlet?hOpt=4110',"Returns","resizable=yes,scrollbars=yes,top=150,left=200,width=660,height=400");
}

function fnOpenTag(strPnum,strRaId)
{
	windowOpener("/gmTag.do?strOpt=reload&pnum="+encodeURIComponent(strPnum)+"&refID="+strRaId+"&refType=51001","PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=920,height=600");
}

function fnChangeToCaps(index)
 {
  var obj = eval("document.frmVendor.Txt_CNum"+index);    
  obj.value = obj.value.toUpperCase();
 }
function fnBlankLine(varCounter,value)
{	
	if(value.substr(0,3) == 'TBE' || value.substr(0,4) == 'NOC#'){
		obj = eval("document.frmVendor.Txt_CNum"+varCounter);
		obj.value = '';
	}	
}
function fnPicSlip(val)
{
	windowOpener("/GmConsignInHouseServlet?hAction=PicSlip&hConsignId="+val,"SetPic","resizable=yes,scrollbars=yes,top=150,left=150,width=750,height=500");
}

function fnValidateAllControlNo(pnum,ctrlNum,qty,pnumMaterial,blComplete){	
	var errparts = "";
	if(pnumMaterial == '100845' && qty > 1){ // 100845[Tissue Part]
		if(ctrlNum == ''){
			if(blComplete)
				errparts += pnum + "  " + ctrlNum;
		}else{
			errparts += pnum + " --- " + ctrlNum;
		}		
	}
	// For validating the Duplicate COntrol Number on Submit
	if(pnumMaterial == '100845' && qty == 1 && ctrlNum != ''){ // 100845[Tissue Part]
		if(tissueControls.indexOf("|" + pnum +ctrlNum + "|") == -1){
			tissueControls = tissueControls + "|" + pnum +ctrlNum + "|";
		}else{
			errparts += pnum + " --- " + ctrlNum;
		}
	}
	return errparts;
}