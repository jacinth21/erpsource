
function fnLoadReq()
{
	fnValidateTxtFld("requestId",message[5502]);
	/* Code added for Validating the Entered Request ID [It Should be the Number Always]*/
	var requestID = TRIM(document.frmLoanerReqEdit.requestId.value);
	var objRegExp = /(^-?\d\d*$)/;
	if(!requestID == '' && !objRegExp.test(requestID)){
		Error_Details(message[5583]);
	}
	if (ErrorCount > 0)
		 {
			Error_Show();
			Error_Clear();
			return false;
		}
	document.frmLoanerReqEdit.strOpt.value =  'load';	
	document.frmLoanerReqEdit.action ="/gmOprLoanerReqEdit.do";
	fnStartProgress('Y');
	document.frmLoanerReqEdit.Btn_Load.disabled = true;
	document.frmLoanerReqEdit.submit();
}

function fnSubmit()
{
	var valVoidFl = "N";
	var valVoidFlArray = new Array();
	var reason = "";
	var k = 0;
	var str ="";
	
	var displaytable = document.getElementById('displaytable');
	var voidarr = displaytable.getElementsByTagName('select');
	var dateDiffVal = "";
	var txnType = document.frmLoanerReqEdit.requestTxnType.value;
	var pnumId = document.frmLoanerReqEdit.shippedDate.value;
	var errPnumIds = "";
	var invalReqid = "";
	var ihlnShipDtChange = "";
	var invalidPIPReqid = "";
	var invalidRFPReqid = "";
	var blsurgDtError = false;
	var blsurg7DtError = false;
	var blvoidError = false;
	fnValidateTxtFld('requestId',message[5502]);
	
	if(txnType=='4127'){
		dateDiffVal = dateDiff(todaysDate,document.all.surgeryDt.value,format);
		fnValidateTxtFld('surgeryDt',message[5544]);
		//fnValidateTxtFld('txt_LogReason',' Comments ');
		//DateValidate(document.frmLoanerReqEdit.surgeryDt,message[1]);
		CommonDateValidation(document.frmLoanerReqEdit.surgeryDt,format,message[1]);
			
		if (dateDiffVal > reqday || dateDiffVal < 0)
		{
			Error_Details(message[2003]);
		}
		if (ErrorCount > 0)
		{
			Error_Show();
			Error_Clear();
			return false;
		}
	}
	var chk_length =  document.frmLoanerReqEdit.shippedDate.length;
	var inputString = "";
	var etchidInputStr = "";
	var voidInputStr = "";
	if(chk_length ==undefined)
	{
		if(document.frmLoanerReqEdit.Chk_voidfl.checked)
		{
			for(var n=0;n<voidarr.length;n++)
			{
			valVoidFl = "Y";
			obj = voidarr[n];
			var myname = obj.getAttribute("name");
			 if(myname=='Cbo_VOIDRSN')
			 {
				len = obj.length;
				for (i=0;i<len;i++)
					{
						if(obj.selectedIndex != -1){
							if (obj[obj.selectedIndex].value == '0')
							{
								blvoidError = true;
							}
							else
							{
								reason = obj[obj.selectedIndex].value;
							}
						}
						break;
					}
			 }
			}
		}
		var val = 1;
		objDate = eval("document.frmLoanerReqEdit.shipDt"+val);
		etchId = eval("document.frmLoanerReqEdit.etchId"+val);
		etchId_val = (etchId != undefined)? etchId.value:"";
		var status = document.frmLoanerReqEdit.shipStatus.value;
		
		shipdateDiff = dateDiff(todaysDate,objDate.value,format);
		if( txnType =='4127'){
			    shipsurgerydateDiff = dateDiff(objDate.value,document.all.surgeryDt.value,format);
			    
			    if ((shipdateDiff > dateDiffVal || shipdateDiff < 0) && (status !='30' && status!='40') && valVoidFl!='Y')
				{
			    	 blsurgDtError = true;
					
				}
				if ((shipsurgerydateDiff > shipday) && (status !='30' && status!='40') && valVoidFl!='Y')
				{
					blsurg7DtError = true;					
				}
		}else if(txnType =='4119'){			
			 var dtEventStrtDt        = document.frmLoanerReqEdit.eventStartDate.value;   
			    var startShippDtDiff 		 = dateDiff(dtEventStrtDt,objDate.value,format); 
			    var oldShippedDate = eval("document.frmLoanerReqEdit.oldShippedDate"+val).value;
				var shippedUpdate = dateDiff(oldShippedDate,objDate.value,format);
			    if(dtEventStrtDt != ""){
			    	if(startShippDtDiff > 0 || (shipdateDiff < 0 && shippedUpdate != 0)){
			    		errPnumIds += (errPnumIds.indexOf(pnumId) != -1)? "":", "+pnumId;
			    		Error_Details(Error_Details_Trans(message[5584],errPnumIds.substr(1,errPnumIds.length)));
			    	}
			    }
		}
		
		if (status !='30' && status!='40')
		{
			inputString = inputString + document.frmLoanerReqEdit.shippedDate.value+"^"+valVoidFl+"^"+reason+"^"+objDate.value+"^"+etchId_val+"|";			
			    if(etchId_val != '')
				{
					etchidInputStr = etchidInputStr+document.frmLoanerReqEdit.shippedDate.value+",";
				}
			    if(valVoidFlArray[n] == 'Y')
			    {
			    	voidInputStr = voidInputStr+document.frmLoanerReqEdit.shippedDate.value+",";
			    }
				
		}		
	}
	for(var n=0;n<voidarr.length;n++)
	{
		var errCnt=0;
		obj = voidarr[k];
		if(obj != undefined)
		{
			var myname = obj.getAttribute("name");
			if(myname=='Cbo_VOIDRSN')
			{
			len = obj.length;
			for (i=0;i<len;i++)
			{
				if(obj.selectedIndex != -1){
				if (obj[obj.selectedIndex].value == '0')
				{
				    blvoidError = true;
					errCnt = 1;
				}
				else
				{
					str = str + obj[obj.selectedIndex].value+'|';
				}
				}
				break;
			}
			if(errCnt ==1)
			{
				break;
			}
			}
			k++;
		}
		
		else
			break;
	}

	for(var n=0;n<chk_length;n++)
	{
		var val = n+1;
		var status = document.frmLoanerReqEdit.shipStatus[n].value;
		var objIHLN = eval("document.frmLoanerReqEdit.hIHLN"+val);
		var ihlnStr  = objIHLN.value;
		
		if(document.frmLoanerReqEdit.Chk_voidfl[n].checked)
		{
			 if (status == '13'){	// 13-PIP
				 Error_Details(Error_Details_Trans(message[5585],ihlnStr));
			 }
			 if (status == '16'){	// 16-RFP
				 Error_Details(Error_Details_Trans(message[5586],ihlnStr));
			 }
			valVoidFlArray[n] = "Y";
			reason = str.substring (0, str.indexOf('|'));
			str	= str.substring (str.indexOf('|')+1);
		}
		else
		{
			valVoidFlArray[n] = "N";
		}
		
		objDate = eval("document.frmLoanerReqEdit.shipDt"+val);
		var reqId = document.frmLoanerReqEdit.shippedDate[n].value;
		var errReqId = "";
		etchId=eval("document.frmLoanerReqEdit.etchId"+val);
		etchId_val = (etchId != undefined)? etchId.value:"";
		shipdateDiff = dateDiff(todaysDate,objDate.value,format);	
		var oldShippedDate = eval("document.frmLoanerReqEdit.oldShippedDate"+val).value;
		var shippedUpdate = dateDiff(oldShippedDate,objDate.value,format); 
	    var objPnum = eval("document.frmLoanerReqEdit.partnum"+val);
	    var pnumVal = objPnum.value;
	   
		if( txnType =='4127'){
		    shipsurgerydateDiff = dateDiff(objDate.value,document.all.surgeryDt.value,format);
		    
		    if ((shipdateDiff > dateDiffVal || shipdateDiff < 0) && (status !='30' && status!='40') && valVoidFl!='Y')
			{
		    	blsurgDtError = true;			
			}
			if ((shipsurgerydateDiff > shipday) && (status !='30' && status!='40') && valVoidFl!='Y')
			{
				blsurg7DtError = true;		
			}
			if (shippedUpdate > 0 && status == '13')
			{
				if(TRIM(reqId).length >= 0){
    				invalidPIPReqid += (reqId + ",");
    			}
			}
			if (shippedUpdate > 0 && status == '16')
			{
				if(TRIM(reqId).length >= 0){
    				invalidRFPReqid += (reqId + ",");
    			}
			}
	}else if(txnType =='4119'){			
		    var dtEventStrtDt        = document.frmLoanerReqEdit.eventStartDate.value;   
		    var startShippDtDiff 		 = dateDiff(dtEventStrtDt,objDate.value,format); 
		    if(dtEventStrtDt != ""  && status!='30'){
		    	if(startShippDtDiff > 0 || (shipdateDiff < 0 && shippedUpdate != 0) ){
		    		if(errReqId.indexOf(reqId+ ",")==-1){
		    			if(TRIM(reqId).length >= 0){
		    				invalReqid += (reqId + ",");
		    			}
		    		}
		    	}
		    	
		    }
			
		    /*if part request is allocated with IHLN and user updating the ship date for the request.*/
		    if(pnumVal != '' && ihlnStr != '' &&  shippedUpdate != 0){
		    	var ihlnID = ihlnStr.substring (0, ihlnStr.indexOf('('));
		    	/* warning msg will come only for IHLN txns */
		    	if(ihlnID.indexOf("IHLN") != -1 ){ 
		    		ihlnShipDtChange += (ihlnShipDtChange.indexOf(ihlnID)!= -1)?"":", "+ihlnID;
		    	}
		    }
		    
	}
		if (status !='30' && status!='40')
		{
			inputString = inputString + reqId+"^"+valVoidFlArray[n]+"^"+reason+"^"+objDate.value+"^"+etchId_val+"|";
			if(etchId_val != '')
			{
				etchidInputStr = etchidInputStr+reqId+",";
			}
			if(valVoidFlArray[n] == 'Y')
		    {
		    	voidInputStr = voidInputStr+reqId+",";
		    }
		}
	}	
	if(blsurgDtError ){
		Error_Details(message[5587]);	
	}
	if(blsurg7DtError ){
		Error_Details(message[5588]);
	}
	if(blvoidError){
		Error_Details(message[5589]);	
	}
	if (invalReqid != ''){
		Error_Details(message[5590]);
	}
	if (invalidPIPReqid != ''){
		Error_Details(Error_Details_Trans(message[5591],ihlnStr));
	}
	if (invalidRFPReqid != ''){
		Error_Details(Error_Details_Trans(message[5592],ihlnStr));
	}
	if (ErrorCount > 0)
		 {
			Error_Show();
			Error_Clear();
			return false;
		}
	/*This warning message will appear if part request is allocated with IHLN and user updating the ship date for the request.*/
	if(ihlnShipDtChange != ""){
		var confirmVal = confirm(Error_Details_Trans(message[5593],ihlnShipDtChange.substring(1, ihlnShipDtChange.length)));
        if (confirmVal==false){
            return false;
        }
        

	}
	document.frmLoanerReqEdit.hinputString.value = inputString;
	document.frmLoanerReqEdit.etchidInputStr.value = etchidInputStr;
	document.frmLoanerReqEdit.voidInputStr.value = voidInputStr;
	str = '';
	document.frmLoanerReqEdit.strOpt.value =  'save'; //'save';
	document.frmLoanerReqEdit.Btn_Submit.disabled = true;
	fnStartProgress('Y');
	document.frmLoanerReqEdit.submit();
}

function fnVoidAll()
{
	var hiddenInnner = "";
	var voidRsn = "";
	var id = "";
	var voidStatus = "";
	objVoidAll = document.frmLoanerReqEdit.voidAll;
	
	strHiddenObject = eval("document.all.VOIDRSNHIDDEN");
	hiddenInnner = strHiddenObject.innerHTML;
	if (objVoidAll) 
	{
		var objChk_voidfl = document.frmLoanerReqEdit.Chk_voidfl;
		var chk_length =  objChk_voidfl.length; 		
		if (objVoidAll.checked)
		{
			if(chk_length == undefined)
			{
				voidStatus = document.frmLoanerReqEdit.shipStatus.value;
				if (voidStatus !='30' && voidStatus!='40')
				{
					id = objChk_voidfl.id;
					voidRsn = eval("document.all.voidrsn"+id);
					voidRsn.style.display='block';
					voidRsn.innerHTML = hiddenInnner;
					objChk_voidfl.checked = true;
				}				
			}
			for(i = 0; i < chk_length; i ++) 
			{	
				voidStatus = document.frmLoanerReqEdit.shipStatus[i].value;
				if (voidStatus !='30' && voidStatus!='40')
				{
					id = objChk_voidfl[i].id;
					voidRsn = eval("document.all.voidrsn"+id);
					voidRsn.style.display='block';
					voidRsn.innerHTML = hiddenInnner;
					objChk_voidfl[i].checked = true;
				}
			}
		}
		else 
		{
			if(chk_length == undefined)
			{
				voidStatus = document.frmLoanerReqEdit.shipStatus.value;
				if (voidStatus !='30' && voidStatus!='40')
				{
					id = objChk_voidfl.id;
					voidRsn = eval("document.all.voidrsn"+id);
					voidRsn.style.display='none';
					voidRsn.innerHTML = '';
					objChk_voidfl.checked = false;
				}				
			}
			for(i = 0; i < chk_length; i ++) 
			{	
				id = objChk_voidfl[i].id;
				voidRsn = eval("document.all.voidrsn"+id);
				voidRsn.style.display='none';	
				voidRsn.innerHTML = '';
				objChk_voidfl[i].checked = false;
			}
		}
	}
}
function fnEnableDiv(obj, id)
{
	
	var hiddenInnner = "";
	strHiddenObject = eval("document.all.VOIDRSNHIDDEN");
	hiddenInnner = strHiddenObject.innerHTML;
	strObject = eval("document.all.voidrsn"+id);
	if(obj.checked)
	{
		strObject.style.display='block';
		strObject.innerHTML = hiddenInnner;	
		
	}
	else
	{
	strObject.style.display='none';	
	strObject.innerHTML = '';
	}	
}

function fnOpenLog(id, type)
{
	windowOpener("/GmCommonLogServlet?hType="+type+"&hID="+id,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
}

function fnEdtId(id,source)
{
	var strAction = 'modify';
	var shipArraySizeFl;
	if(document.frmLoanerReqEdit.recordCount != null || document.frmLoanerReqEdit.recordCount == undefined){
		var recordFlagvalue = document.frmLoanerReqEdit.recordCount.value;
	}	
	if(recordFlagvalue == 'Y'){
		shipArraySizeFl = 'Y';
	}else{		
		shipArraySizeFl = 'N';
	}	
	source = (source == null || source == '')?"50182":source;
	document.frmLoanerReqEdit.action ="/gmOPEditShipDetails.do?refId="+id+"&strOpt="+strAction+"&source="+source;	
	document.frmLoanerReqEdit.submit();
}

function fnInitLoaner()
{
	document.frmLoanerReqEdit.action ="/gmRequestInitiate.do?consignmentType=4127";
	document.frmLoanerReqEdit.strOpt.value =  ''; 
	document.frmLoanerReqEdit.submit();
}
function fnPageLoad(){
	
	hactionVal =document.all.haction.value;
	var reqObj = document.frmLoanerReqEdit.requestId;
	// code added for disabling the Print Button for PMT-254
	if(document.frmLoanerReqEdit.recordCount != undefined){
		var reqCountFl = document.frmLoanerReqEdit.recordCount;
		if(reqCountFl.value == 'N'){
			document.getElementById('Btn_Print').disabled = true;
		}
	}
	reqObj.focus();
	if(TRIM(hactionVal) == 'hidden'){
	 document.getElementById("Btn_Over").value = 'Dont Override';
	 document.frmLoanerReqEdit.Btn_Over.style.visibility="visible";
	 var rowCount = document.getElementById("currentRowObject").rows.length;
		for(var k=1;k<rowCount;k++){
			var strEcth = "etchId" + k;
			var strpnum = "partnum" +k;
			var strStatus = "statusId" + k;
		    var pnum = document.getElementById(strpnum).value; // code added for not to enable the etch id for parts.
		    var status = document.getElementById(strStatus).value; 
		    
			if(document.getElementById(strEcth) != undefined && pnum == '' && status != null && status == 10 )
				document.getElementById(strEcth).style.visibility="visible";
		} 
	}else if(eval(document.all.hOverride)!= undefined && eval(document.all.hOverride).value == 'Y' && hactionVal == 'visible'){
		document.frmLoanerReqEdit.Btn_Over.style.visibility="visible";
	}else if(eval(document.all.hOverride)!= undefined && eval(document.all.hOverride).value == 'Y'){
		document.frmLoanerReqEdit.Btn_Over.style.visibility="visible";
	}
	/*Disable print button if the request does not have a set associated. */
	if(eval(document.all.hPrintFl)!= undefined && eval(document.all.hPrintFl).value == 'Y'){
		document.frmLoanerReqEdit.Btn_Print.disabled=false;
	}
}
function fnOverride(){	
	var hideObj = document.frmLoanerReqEdit.haction;
	var rowCount = document.getElementById("currentRowObject").rows.length;
	
	if(hideObj.value == 'visible'){
	for(var k=1;k<rowCount;k++){		
		var eatchIDObj = eval("document.all.etchId"+k);
		var strpnum = "partnum" +k;
		var strStatus = "statusId" + k;
	    var pnum = document.getElementById(strpnum).value; // code added for not to enable the etch id for parts.
	    var status = document.getElementById(strStatus).value; 
	    if(eatchIDObj != undefined && pnum == '' && status != null && status == 10 ){
	    	eatchIDObj.style.visibility = 'visible';  
	    }			
	} 
	document.frmLoanerReqEdit.haction.value='hidden';
	document.getElementById("Btn_Over").value = "Dont Override";
	}else{
	for(var k=1;k<rowCount;k++){
		var eatchIDObj = eval("document.all.etchId"+k);
		if(eatchIDObj != undefined){
			eatchIDObj.value ='';
			eatchIDObj.style.visibility="hidden";  
	} 
	document.frmLoanerReqEdit.haction.value='visible';
	document.getElementById("Btn_Over").value="Override";
	}
	}
}
function fnPendingApproval(){
	var txnType=document.frmLoanerReqEdit.txnType.value;
	var reqId=document.frmLoanerReqEdit.requestId.value;
	document.frmLoanerReqEdit.action ="/gmLoanerRequestApproval.do?strOpt=load&strTxnType="+txnType+"&requestId="+reqId;
	document.frmLoanerReqEdit.submit();
}
function fnLoadRequset(){
	if(event.keyCode == 13)
	{	 
		var HiddenReqId = document.frmLoanerReqEdit.hiddenReqID.value;
		var ReqID       = document.frmLoanerReqEdit.requestId.value;
		if (HiddenReqId != ReqID){
			var rtVal = fnLoadReq();
		}else{
			var rtVal = true;
		}
		if(rtVal == false){
			if(!e) var e = window.event;

	        e.cancelBubble = true;
	        e.returnValue = false;

	        if (e.stopPropagation) {
	                e.stopPropagation();
	                e.preventDefault();
	        }
		}
	}
}
// Changes for MNTTASK-8116
function fnPrint(){
	var reqID = document.frmLoanerReqEdit.requestId.value;
	var txnType = document.frmLoanerReqEdit.requestTxnType.value;
	if(txnType =='4127'){
	    windowOpener("/gmOprLoanerReqEdit.do?requestId="+reqID+"&strOpt=print&txnType=4127","LoanerRequest","resizable=yes,scrollbars=yes,top=250,left=300,width=840,height=700");
	}else if(txnType =='4119'){
		windowOpener("/gmOprLoanerReqEdit.do?requestId="+reqID+"&strOpt=InhousePrint&txnType=4119","LoanerRequest","resizable=yes,scrollbars=yes,top=250,left=300,width=840,height=700");
	}
}
function fnCopyDate(){	
	var topObj = getObjTop();
	var jsDateformat = topObj.gCompJSDateFmt;
	showCalendar('divDate',jsDateformat);	
}
function showCalendar(calendardiv,dateformat){
	// Overite style for calendar only from class RightTableCaption. This method call to date-pciker.js.
	onSetStyleToCalendar(calendardiv);
	
	if (dateformat == null){
		dateformat='%m/%d/%Y';
	}	
	if(!calendars[calendardiv])
		calendars[calendardiv]= initCalendar(calendardiv,dateformat);

	//This line for today date will display in BOLD and this method will be in dhtmlxcalendar.js
	calendars[calendardiv].enableTodayHighlight(true); // or false
		
	if(calendars[calendardiv].isVisible())
		calendars[calendardiv].hide();
	else 
		calendars[calendardiv].show();
}

//Below codes are modified for new DHTMLX version
function initCalendar(calendardiv,dateformat){	
		/*if((navigator.userAgent.match(/iPhone/i))!=null || (navigator.userAgent.match(/iPad/i))!=null){
			mCal = new dhtmlxCalendarObject(calendardiv, false, {
			isMonthEditable: false,
			isYearEditable: false });
		}else{
			mCal = new dhtmlxCalendarObject(calendardiv, false, {
			isMonthEditable: true,
			isYearEditable: true});
		}*/
	
	//Above commented code for New version 3.5. In old dhtmlx version we can't edit month and year in IPAD. Now we can edit.
	mCal = new dhtmlXCalendarObject(calendardiv);
	//mCal.setYearsRange(1970, 2500);
	
	// This range only able to select date. 
	mCal.setSensitiveRange("1970-01-01", "2500-31-12"); 
	
	//mCal.draw();
	//mCal.disableIESelectFix(true);
	mCal.setDateFormat(dateformat);
	mCal.hideTime(); // Hide the time in new dhtmlx version 3.5
	/*mCal.setOnClickHandler(function(date,obj,type){
		this.hide();
		var val = 0;
		var cnt = document.getElementsByName("shippedDate").length;		
		for (val = 1; val <=cnt; val++ ){
			objdate = eval("document.all.shipDt"+val);			
			objStatus = eval("document.frmLoanerReqEdit.shipStatusId"+val);			
					if ((this.getFormatedDate(dateformat, date)!='') && (objStatus.value != '40' && objStatus.value != '30')){
						objdate.value = this.getFormatedDate(dateformat, date);			
					}
		}		
		return true;
	});*/
	
	// Changed the above code for new version DHTMLX. When click calendar icon and set the date in textbox.
	mCal.attachEvent("onClick", function(date) {
		calendars[calendardiv].hide();
		var val = 0;
		var cnt = document.getElementsByName("shippedDate").length;		
		for (val = 1; val <=cnt; val++ ){
			objdate = eval("document.all.shipDt"+val);			
			objStatus = eval("document.frmLoanerReqEdit.shipStatusId"+val);			
					if ((this.getFormatedDate(dateformat, date)!='') && (objStatus.value != '40' && objStatus.value != '30')){
						objdate.value = this.getFormatedDate(dateformat, date);			
					}
		}
		
		return true;
	 });
	mCal.hide();
	return mCal;
}

function fnShowFilters(val){
	var obj = eval("document.all."+val);
	var obj1 = eval("document.all."+val+"img");
	if (obj.style.display == 'none'){
		obj.style.display = 'table-row';
		if(obj1){
			obj1.src = '/images/minus.gif';
		}
	}else{
		obj.style.display = 'none';	
		if(obj1){
			obj1.src = '/images/plus.gif';
		}
	}
}

function fnValidateShipDate(val1,val2){
	alert("welcome");
}


//code merge from OUS side

function fnLoadReport(){
	val = document.frmLoanerReqEdit.Cbo_Report.value;
	if (val == 'DELSLIP' )
	{
		fnDeliverySlip();
	}else if (val == 'COLSLIP' )
	{
		fnCollectionSlip();
	} 
}




function fnDeliverySlip()
{
	var reqId = document.frmLoanerReqEdit.requestId.value ;
	windowOpener("/gmOprLoanerReqEdit.do?strOpt=PicSlip&strMode=Del&requestId="+reqId,"frmLoanerReqEdit","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnCollectionSlip()
{
	var reqId = document.frmLoanerReqEdit.requestId.value ;
	windowOpener("/gmOprLoanerReqEdit.do?strOpt=PicSlip&strMode=Coll&requestId="+reqId,"frmLoanerReqEdit","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

//PC-137 Called when the loaner flag is updated
function fnUpdateEscalation(strReqId){	
	if(strEscFl == 'Y' && SwitchUserFl != 'Y'){
		var comments = 'Flagged for Escalation';
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmOprLoanerReqRpt.do?method=updateLoanerEscalation&hType=26240693&requestId='+strReqId+'&hEscFl=Y&comments='+comments+'&reqStatus='+strEscFl+'&randomId='+Math.random());
            dhtmlxAjax.get(ajaxUrl,function(loader){
		var escFl  =loader.xmlDoc.responseText;
		document.getElementById(strReqId).setAttribute("src","/img/escalation_flag_red.jpg");
		document.getElementById(strReqId).style.display = "inline";
		fnLoadReq();
		document.getElementById(strReqId).setAttribute("onclick", "fnRemoveEscalation("+strReqId+");"); 
	  });  
	}
}

//PC-137 Called when the loaner flag is removed						 
function fnRemoveEscalation(strReqId){
	if(strEscFl == 'Y' && SwitchUserFl != 'Y'){
		var comments = 'Escalation unflagged';
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmOprLoanerReqRpt.do?method=updateLoanerEscalation&hType=26240693&requestId='+strReqId+'&hEscFl=N&comments='+comments+'&reqStatus='+strEscFl+'&randomId='+Math.random());
		   dhtmlxAjax.get(ajaxUrl,function(loader){
		var escFl =loader.xmlDoc.responseText;
		document.getElementById(strReqId).src = "/img/escalation_flag_grey.jpg";
		document.getElementById(strReqId).setAttribute("onclick", "fnUpdateEscalation("+strReqId+");"); 
		document.getElementById(strReqId).style.display = "inline";
			});	
}
}

//PC-137 Called when the call log button is clicked in edit loaner screen
function fnReqOpenLog(strReqId) { 
	windowOpener("/GmCommonLogServlet?hType=26240693&hID="
			+strReqId+"&beanKeyId=LOANESCL&hHideComment=Y&hAlignSubmit=L","RequestLog","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
}
