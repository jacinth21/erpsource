var ajax = new Array();
var index; 

function fnInHousePicSlip(val)
{
	windowOpener("/GmConsignInHouseServlet?hAction=PicSlip&hConsignId="+val,"SetPic","resizable=yes,scrollbars=yes,top=150,left=150,width=750,height=500");
}
function fnPicSlip(val,type,refid){
	windowOpener("/GmConsignInHouseServlet?hAction=PicSlip&hConsignId="+val+"&ruleSource="+type+"&refId="+refid,"SetPic","resizable=yes,scrollbars=yes,top=150,left=150,width=750,height=500");
}



function  doValidation(pnumobj, setqty, missqty, stockqty, setlistqty, repqty, retqty, priceobj,repType, retType)
{
	 var varRowCnt = parseInt(document.frmVendor.hcounter.value) -1;
	 var strErrorTotal="";
	 var strErrorStock="";
	 var strErrorQty="";
	 var strErrorMore="";
	 var strErrorMoreRet="";
	 var strErrorMoreRep="";
	 var reptotal="";
	 var retstr ="";
	 var strReplneishFinalCheck ="";
	 var strMoveToFinalCheck ="";
	 var strMissingFinalCheck ="";
	 var strMissingErrorQty = "";

	 var ZeroErrorMissqty = "";
	 
	 
	 if ( missqty == 0 )
	 {
		 ZeroErrorMissqty = ZeroErrorMissqty + ","+ pnumobj.value;
	 }
		
	 if ( isNaN(missqty) || !isNaN(missqty) < 0)
	 {
		 missqty =0;
	 }
	 if(lotFlag == "YES" && loansflcd!=0 && loansflcd!=5 && loansflcd!=10){
		 var missQtyTemp =0;
	     for (ind=0; ind <= varRowCnt; ind ++)
	     {
	          pnumobjTemp = eval("document.frmVendor.hPartNum"+ind);
	          cntnumobjtemp = eval("document.frmVendor.Cbo_UseLot"+ind);
	          
	          
	          if (pnumobj == undefined)
	          {
	       	   continue;
	          }   
	          if(pnumobj.value == pnumobjTemp.value){
	        	  missobj = eval("document.frmVendor.Txt_Miss"+ind);
	       	  
	            missQtyTemp += (missobj.value == '')? 0: parseInt(missobj.value);
	     	  } 
	     }
	    
	      missqty = missQtyTemp;
	}
	 //
	 if ( isNaN(setqty) || !isNaN(setqty) < 0)
	 {
		 setqty =0;
	 }
	 
	 if ( isNaN(stockqty) || !isNaN(stockqty) < 0)
	 {
		 stockqty =0;
	 }
	 
	 if ( isNaN(setlistqty) || !isNaN(setlistqty) < 0)
	 {
		 setlistqty =0;
	 }
	 
	 if ( isNaN(repqty) || !isNaN(repqty) < 0)
	 {
		 repqty =0;
	 }
	 
	 if ( isNaN(retqty) || !isNaN(retqty) < 0)
	 {
		 retqty =0;
	 }
	 
if(!isNaN(repqty) && repqty > 0) //either 0 or more
{
	if ( !isNaN(missqty) && !isNaN(retqty))
	{
		if ( setqty > 0 && retqty > 0 && (repqty != (setlistqty - (setqty - (missqty + retqty)))))
		{
			strErrorQty  = strErrorQty + ","+ pnumobj.value;
//			alert(3);
			}
		
	}
	else if (!isNaN(setqty) && setqty == 0)
	{
		strErrorQty  = strErrorQty;
//		alert(6);
	}
	else if (!isNaN(missqty))
	{
		if(lotFlag =="YES" && loansflcd!=0 && loansflcd!=5 && loansflcd!=10){
			var varRowCnt = parseInt(document.frmVendor.hcounter.value) -1;
			var repQtyTemp = 0;
			var repobjtemp;
			for (ind = 0; ind <= varRowCnt; ind++) {
				pnumobjTemp = eval("document.frmVendor.hPartNum"+ ind);

				if (pnumobj == undefined) {
					continue;
				}
				if (pnumobj.value == pnumobjTemp.value) {
					repobjtemp = eval("document.frmVendor.Txt_Rep"+ ind);

					repobjtemp += (repobjtemp == undefined || repobjtemp.value == '') ? 0: parseInt(repobjtemp.value);
				}
			}
			
			if (missqty != repQtyTemp.value) {
				strErrorQty = strErrorQty + "," + pnumobj.value;
			}
		}else{
			if (missqty != repqty)
			{
				strErrorQty  = strErrorQty + ","+ pnumobj.value;
//				alert(4);
				}
		}
		
	}
	
	
	else if (!isNaN(retqty))
	{
		if(lotFlag !="YES" && loansflcd!=0 && loansflcd!=5 && loansflcd!=10){
		
			if (retqty != repqty)
			{
				strErrorQty  = strErrorQty + ","+ pnumobj.value;
//				alert(5);
				}
		}
		
	}
	else
	{
		strErrorQty  = strErrorQty + ","+ pnumobj.value;
//		alert(7);
	}
}		

	if(!isNaN(repqty) && repqty > 0 )
	{
		if ( retqty == 0  && repqty > (setlistqty - (setqty - missqty) )){
			strErrorMoreRep  = strErrorMoreRep + ","+ pnumobj.value;
		}
	}
	
	if (!isNaN(missqty) && missqty > 0)
	{
	//	//alert("missqty: "+missqty+"setqty: "+ setqty);
		
		if (missqty > setqty)
		{
			strErrorMore  = strErrorMore + ","+ pnumobj.value;
		}else{
		reptotal = parseInt(reptotal) + missqty;
		}
	}

	if(!isNaN(missqty) && missqty > 0 )  //either 0 or more
	{
			if ( setqty > 0  && (repqty != (setlistqty - (setqty - (missqty + retqty)))))
			{
				strMissingErrorQty  = strMissingErrorQty + ","+ pnumobj.value;
				//alert(3);
				}
			
		}


	if (!isNaN(retqty) && retqty > 0)
	{
		if (retqty > setqty)
		{
			strErrorMoreRet  = strErrorMoreRet + ","+ pnumobj.value;
		}else{
			reptotal = parseInt(reptotal) + retqty;
		if (reptotal > setqty)
		{
				strErrorTotal  = strErrorTotal + ","+ pnumobj.value;
			reptotal = parseInt(reptotal) - retqty;
		}else{
			retstr = retstr + pnumobj.value + ','+ retqty + ',,'+priceobj.value+'|';
		}
		
		}
	}

	if (!isNaN(repqty) && repqty > 0)
	{
		if (repqty > stockqty &&  setqty == setlistqty)
		{
			strErrorStock  = strErrorStock + ","+ pnumobj.value;
		}
	}
	
	//alert(" msgs :"+ strErrorTotal+":"+strErrorStock+":"+strErrorQty+":"+ strErrorMore+":"+strErrorMoreRet+":"+strErrorMoreRep+";");
	
	if (strErrorTotal == '' && strErrorStock == '' 
		&& strErrorQty == '' && strErrorMore == '' 
			&& strErrorMoreRet == '' && strErrorMoreRep == '' && repType != 0)
	{
		if ( !isNaN(missqty) && !isNaN(retqty)&& retqty > 0 )
		{	
			if ( repqty > (setlistqty - (setqty - (missqty + retqty))))
			 { 
				 strReplneishFinalCheck = strReplneishFinalCheck + "," + pnumobj.value;
			 }
		}
	}


if (strErrorTotal == '' && strErrorStock == '' 
		&& strErrorQty == '' && strErrorMore == '' 
			&& strErrorMoreRet == '' && strErrorMoreRep == '' && retType != 0 && strReplneishFinalCheck=='')
	{
		if( retqty > ((setqty + repqty) - missqty - setlistqty))
		{
			strMoveToFinalCheck = strMoveToFinalCheck + "," + pnumobj.value;
		}
    }

if (strErrorTotal == '' && strErrorStock == '' 
		&& strErrorQty == '' && strErrorMore == '' 
			&& strErrorMoreRet == '' && strErrorMoreRep == '' && missqty > 0 && strMoveToFinalCheck =='' && lnSts == '25' && retqty > 0)
	{
	 if ( missqty > ((setqty + repqty) - retqty - setlistqty))
	
		{
			strMissingFinalCheck = strMissingFinalCheck + "," + pnumobj.value;
		}
	} 

if (ZeroErrorMissqty != '' && lnSts == '25')
{
	Error_Details(message_operations[155]+ZeroErrorMissqty.substr(1,ZeroErrorMissqty.length));
}

if (strMissingErrorQty != '')
{
Error_Details(message_operations[156]+strMissingErrorQty.substr(1,strMissingErrorQty.length));
}

if (strErrorTotal != '')
{
Error_Details(message_operations[157]+strErrorTotal.substr(1,strErrorTotal.length));
}

if (strErrorStock != '')
{
Error_Details(message_operations[158]+strErrorStock.substr(1,strErrorStock.length));
}

if (strErrorQty != '')
{
Error_Details(message_operations[159]+strErrorQty.substr(1,strErrorQty.length));
}

if (strErrorMore != '')
{
Error_Details(message_operations[160]+strErrorMore.substr(1,strErrorMore.length));
}

if (strErrorMoreRet != '')
{
Error_Details(message_operations[161]+strErrorMoreRet.substr(1,strErrorMoreRet.length));
}

if (strErrorMoreRep != '')
{
Error_Details(message_operations[162]+strErrorMoreRep.substr(1,strErrorMoreRep.length));
}

if (strReplneishFinalCheck != '')
{
Error_Details(message_operations[163]+strReplneishFinalCheck.substr(1,strReplneishFinalCheck.length));
}

if (strMoveToFinalCheck != '')
{
Error_Details(message_operations[164]+strMoveToFinalCheck.substr(1,strMoveToFinalCheck.length));
}

if (strMissingFinalCheck != '')
{
Error_Details(message_operations[165]+strMissingFinalCheck.substr(1,strMissingFinalCheck.length));
}
}

function fnCheckRepType(val, pnum)
{
	var id = document.getElementsByName("hpnum"+val);
    var repfrom;
    var repstring = "";
    var repfrmval = "";
    var stock = "";
    
    for ( var index =0 ; index < id.length; index++)
    {
    	repfrom = eval("document.frmVendor.Cbo_Repfrom"+id[index].id); 
    	stock = eval("document.frmVendor.Txt_Stock"+id[index].id); 
    	if (repfrom!= undefined)
    	{
    		//alert(repfrom.options[repfrom.selectedIndex].value);
    		repfrmval  = repfrom.options[repfrom.selectedIndex].value;
	    		if (repfrmval != "0")
	    		{
		    		 if ( repstring.indexOf(repfrmval) == -1 ) 
		    		 {
		    		 repstring = repstring + repfrmval + "^";
		    		 }
		    		 else
		    		 {
		    			 
		    			 Error_Details(message_operations[166] + pnum );
		    			 repfrom.value ="0";
		    			 repfrom.focus();
		    			 /*stock.removeAttribute('readOnly'); 
		    			 stock.value="";
		    			 stock.setAttribute('readOnly','readonly');
		    			 */
		    			
		    		 }
	    		}	 
    	}
    }
    if (ErrorCount > 0)
	 {		
	 	Error_Show();
		Error_Clear();
	 	return false;
	 }
	 else{
		return true;
	 }
}

//--- below function is uesd to check same lot number for parts. 
function fnCheckLotNum(val, pnum)
{
	var cnumobj=eval("document.frmVendor.Cbo_UseLot"+val); 
	var cnum = cnumobj.value;
	var lcnt = parseInt(document.frmVendor.hcounter.value) -1;	 
    var useLot;
    var useLotval = "";
    
    for ( var index =0 ; index < lcnt; index++)
    {
    	useLot = eval("document.frmVendor.Cbo_UseLot"+index); 
    	partnumobj = eval("document.frmVendor.hPartNum"+index);
    	
    	if(val != index){
    	if(pnum == partnumobj.value){
    		
    		useLotval  += useLot.value+',';
    		
    	 }
    	}
    }
   
    if ( useLotval.indexOf(cnum) != -1 ) {
    	Error_Details(message_operations[731] + pnum );
    	cnumobj.value = "0";
    }
    
    
    if (ErrorCount > 0)
	 {		
	 	Error_Show();
		Error_Clear();
		
	 	return false;
	 }
	 else{
		return true;
	 }
}


function fnCheckMoveTo(val, pnum)
{
	var id = document.getElementsByName("hpnum"+val);
    var moveto;
    var movetostring = "";
    var movetoval = "";
    var stock = "";
    
    for ( var index =0 ; index < id.length; index++)
    {
    	moveto = eval("document.frmVendor.Cbo_MoveTo"+id[index].id); 
    	
    	if (moveto!= undefined)
    	{
    		//alert(moveto.options[moveto.selectedIndex].value);
    		movetoval  = moveto.options[moveto.selectedIndex].value;
	    		if (movetoval != "0")
	    		{
		    		 if ( movetostring.indexOf(movetoval) == -1 ) 
		    		 {
		    			 movetostring = movetostring + movetoval + "^";
		    		 }
		    		 else
		    		 {
		    			 Error_Details(message_operations[167] + pnum );
		    			 moveto.value ="0";
		    			 moveto.focus();
		    		 }
	    		}	 
    	}
    }
    if (ErrorCount > 0)
	 {		
	 	Error_Show();
		Error_Clear();
	 	return false;
	 }
	 else{
		return true;
	 }
}

function validate(){
	
	hcnt = parseInt(document.frmVendor.hcounter.value) -1;	 
	var num = "1234567890";
	
	var missobjqty;
	var repfromqty;
	var movetoqty;
	var qtyobjqty;
	
	 for (k=0; k <= hcnt; k ++)
     {
		pobj = eval("document.frmVendor.hPartNum"+k);
		
		if(pobj == undefined)
		{
			continue;
		}	
		
		if (lnSts == '25'){ 
			missobj = eval("document.frmVendor.Txt_Miss"+k);
			
			if(missobj != undefined)
			{
				missobjqty = missobj.value;
			}
			fnCheckString(missobjqty, message_operations[168]+pobj.value, num);
		}
		
		repfrom = eval("document.frmVendor.Txt_Rep"+k);		
		
		if(repfrom != undefined)
		{
			repfromqty = repfrom.value;
		}
			
		fnCheckString(repfromqty, message_operations[169]+pobj.value, num);	
		
		moveto = eval("document.frmVendor.Txt_Remove"+k);	
		
		if(moveto != undefined)
		{
			movetoqty = moveto.value; 	
		}
		
		fnCheckString(movetoqty, message_operations[170]+pobj.value, num);
		
		qtyobj = eval("document.frmVendor.hSetQty"+k);
		
		if(qtyobj != undefined)
		{
			qtyobjqty = qtyobj.value; 
		}
		
		
		 
		repfromdd = eval("document.frmVendor.Cbo_Repfrom"+k);
	
		if(repfromdd != undefined)
		{
			repfromselval = repfromdd.options[repfromdd.selectedIndex].value;
		}
		
		if (repfromselval != undefined && repfromselval != 0 && (repfromqty <= 0 || repfromqty == "")){
			Error_Details(message_operations[171]+pobj.value);
		}
		
		if ( repfromqty > 0 && repfromselval != undefined && repfromselval == 0){
			Error_Details(message_operations[172]+pobj.value);
		}
		
		movetodd = eval("document.frmVendor.Cbo_MoveTo"+k);
		
		if(movetodd != undefined)
		{
			movetoselval = movetodd.options[movetodd.selectedIndex].value;
		}
		
		if (movetoselval != undefined && movetoselval != 0 && (movetoqty <= 0 || movetoqty == "")){
			Error_Details(message_operations[170]+pobj.value);
		}
		if (movetoselval != undefined && movetoqty > 0 && movetoselval == 0){
			Error_Details(message_operations[173]+pobj.value);
		}

     }
	 
	 if (ErrorCount > 0)
	 {		
	 	Error_Show();
		Error_Clear();
	 	return false;
	 }
	 else{
		return true;
	 }
}

function fnCreateInputString()
{     
	 // var varRowCnt = nxtrowid;
	  //if (varRowCnt == 0){
		  varRowCnt = parseInt(document.frmVendor.hcounter.value) -1;
		  ////alert("varRowCnt "+varRowCnt );
	  //}      
      //varRowCnt = varRowCnt + cnt - 1;
	  var listRepFrom = document.getElementById("Cbo_Repfrom0");
      var listMoveTo = document.getElementById("Cbo_MoveTo0");
      var obj;
      var pnumobj;
      var missobj="";
      var qtyobj;
      var priceobj;
      var setlistqtyobj;
      var repfrom;
      var moveto;
      var repfromtotal;
      var movetototal;
      var inputstrtype;
      var str;
      var setqty = 0;
      var missqty = 0;
      var stockqty = 0;
      var repqty = 0;
      var retqty = 0;
      var setlistqty = 0;
      var priceobjqty =0;
      var repfromqty =0;
      var movetoqty =0;
      var repfromtotalqty = 0;
      var movetototalqty = 0;
      var previousPnum;
      var strPartNum = "";
      var qtyMissChk = "";
      var useLotChk = "";
    //alert(listRepFrom.options[2].value);
    str = eval("document.frmVendor.hInputStr");
    str.value = "";
      
  	for(var i = 0; i < listRepFrom.options.length; ++i)
    {
		if (listRepFrom.options[i].value != '0' )
		{
			inputstrtype = eval("document.frmVendor.strRplnFrm_"+ listRepFrom.options[i].value);
			inputstrtype.value = "";
		}
    }


	  for(var i = 0; i < listMoveTo.options.length; ++i)
     {
		 if (listMoveTo.options[i].value != '0' )
		{
		  inputstrtype = eval("document.frmVendor.strMoveTo_"+ listMoveTo.options[i].value);  
		  inputstrtype.value = "";
		}
	  }                   
      
      for (k=0; k <= varRowCnt; k ++)
      {
    	   priceobjqty = 0;
           obj = eval("document.frmVendor.partNum"+k);
           reptotal = 0;
           pnumobj = eval("document.frmVendor.hPartNum"+k);
           priceobj = eval("document.frmVendor.hPrice"+k);
           priceobjqty = priceobj.value; 
           if (pnumobj == undefined)
           {
        	   continue;
           }   
           
           if (lnSts == '25')
           { 
        	   missobj = eval("document.frmVendor.Txt_Miss"+k);
	           if (missobj != undefined) {
	        	   missqty = parseInt(missobj.value);
	           }
	           
	           var cnumObj = eval("document.frmVendor.Cbo_UseLot"+k);
        	   var cnum = 	 (cnumObj != undefined)?  cnumObj.value:"";
	          
	           if(isNaN(missqty) && cnum != 0  && lotFlag =="YES" && loansflcd!=0 && loansflcd!=5 && loansflcd!=10){
   		   		   useLotChk += pnumobj.value+",";
   		   	   }
	           
	           if(!isNaN(missqty) && missqty > 0)
	           {
	        	   //alert(cnumObj.options[cnumObj.selectedIndex].id);
	        	   
	        	   if(lotFlag =="YES" && loansflcd!=0 && loansflcd!=5 && loansflcd!=10){
	        		   
	        		   if(missqty!=0 && (cnum == 0 || cnum =='')){
	        			   qtyMissChk += pnumobj.value+',';
	        		   }
	        		   
	     
	        		   if(missqty!=0 && missqty > cnumObj.options[cnumObj.selectedIndex].id && cnumObj.options[cnumObj.selectedIndex].id!=''){
	        			   Error_Details(message_operations[728]+cnum);
	        		   }
	        	   
	        	   }
	   		    str.value = str.value + pnumobj.value + ','+ missqty + ','+cnum+','+priceobjqty+'|';
   		   	   }
	           
	          
	
           }
           
           qtyobj = eval("document.frmVendor.hSetQty"+k);
           setlistqtyobj = eval("document.frmVendor.hSetListQty"+k);
           priceobj = eval("document.frmVendor.hPrice"+k);
           stockobj = eval("document.frmVendor.Txt_Stock"+k);
           repfrom = eval("document.frmVendor.Txt_Rep"+k);
           moveto = eval("document.frmVendor.Txt_Remove"+k);
           
           if (qtyobj != undefined) {
        	   setqty = parseInt(qtyobj.value);
           }
           
           if (stockobj != undefined) {
        	   stockqty = parseInt(stockobj.value);
           }
           
           if (setlistqtyobj != undefined) {
        	   setlistqty = parseInt(setlistqtyobj.value);
           }
           
           if (priceobj != undefined) {
        	  // priceobjqty = parseInt(priceobj.value);
        	   	 priceobjqty = parseFloat(priceobj.value); 
           }
           
           if (repfrom != undefined){
        	   repfromqty = parseInt(repfrom.value);
        	   repqty = repqty + parseInt(repfromqty);
        	  // alert("repqty : " + repqty);
           }
           
           if (moveto != undefined){
        	   movetoqty = parseInt(moveto.value);
        	   retqty = retqty +  parseInt(movetoqty);
        	 //  alert("retqty : " + retqty);
           }
   		    		          
           BOQtyobj = eval("document.frmVendor.hBOQty"+k);
   		   if(BOQtyobj != undefined){
   			   backOrderQty = parseInt(BOQtyobj.value);
   			   setqty = setqty+backOrderQty;
   		   }
   		 var id = document.getElementsByName("hpnum"+k);
         
   		 for ( var index =0 ; index < id.length; index++)
         { 
			var repfromtotalTemp = 0;
			var  movetototalTemp = 0;

         	repfromtotal = eval("document.frmVendor.Txt_Rep"+id[index].id); 
         	if (repfromtotal!= undefined )
         	{
				   if (repfromtotal.value != '')
				   {
						repfromtotalTemp =  parseInt(repfromtotal.value);
				   }
         			repfromtotalqty = repfromtotalqty + repfromtotalTemp; 
				stockqty = stockqty + repfromtotalTemp;  //if back order adding the replinish qty with shelf qty
         	}

         	 movetototal = eval("document.frmVendor.Txt_Remove"+id[index].id);
         	 if (movetototal!= undefined)
	            	{
						 if (movetototal.value != '')
						   {
							 movetototalTemp = parseInt(movetototal.value)					 
							 }
         			     movetototalqty = movetototalqty + movetototalTemp; 
	            	}
         	strPartNum = strPartNum + id[index].name+"^";
         }

   		 // Make the stock qty equal to repqty to skip the validation for back order type
   		 var repfromBackOrder = eval("document.frmVendor.Cbo_Repfrom"+k);
   		 var repfromBackOrderValue =  repfromBackOrder.options[repfromBackOrder.selectedIndex].value;
   		 
   		 var retfromBackOrder = eval("document.frmVendor.Cbo_MoveTo"+k);
   		 var retfromBackOrderValue =  retfromBackOrder.options[retfromBackOrder.selectedIndex].value;
   		
   		 if ( repfromBackOrderValue == '100061' )
   		{
   			stockqty = repfromtotalqty;
   		}
   		 
         if (strPartNum.indexOf("hpnum"+k) != -1)
         {
        	 doValidation(pnumobj, setqty, missqty, stockqty, setlistqty, repfromtotalqty, movetototalqty, priceobj, repfromBackOrderValue, retfromBackOrderValue);
			  repfromtotalqty = 0;
			  movetototalqty = 0;  
         }
   		   	
          for(var i = 0; i < listRepFrom.options.length; ++i)
             {
        	//  alert("+k "+k);
        	  var currentReplFrom = eval("document.frmVendor.Cbo_Repfrom"+k);
    //alert(currentReplFrom.selectedIndex +" "+i);
        	  if( currentReplFrom.options[currentReplFrom.selectedIndex].value == listRepFrom.options[i].value && listRepFrom.options[i].value != 0)
               {
                        inputstrtype = eval("document.frmVendor.strRplnFrm_"+ listRepFrom.options[i].value);
                        inputstrtype.value = inputstrtype.value +  pnumobj.value + ','+ repfromqty + ',,'+priceobjqty+'|';
                        	
               }                                                            
             }
          
          for(var i = 0; i < listMoveTo.options.length; ++i)
          {
        	  var currentReplTo = eval("document.frmVendor.Cbo_MoveTo"+k);
        	  
            if( currentReplTo.options[currentReplTo.selectedIndex].value == listMoveTo.options[i].value && listMoveTo.options[i].value != 0)
            {
                     inputstrtype = eval("document.frmVendor.strMoveTo_"+ listMoveTo.options[i].value);                    
                     inputstrtype.value = inputstrtype.value +  pnumobj.value + ','+ movetoqty + ',,'+priceobjqty+'|';
                    	 
            }                                                            
          }
          
          previousPnum = pnumobj;  
      }
      
      if(qtyMissChk!=""){
    	  Error_Details(message_operations[729]+qtyMissChk.substr(0,qtyMissChk.length-1));
      }
      
      if(useLotChk != ''){
    	  Error_Details(message_operations[730]+useLotChk.substr(0,useLotChk.length-1));
      }
      if (ErrorCount > 0)
      {
      	Error_Show();
      	Error_Clear();
      	return false;
      }
      else 
      {
    	   return true;
      }
}

function fnLoanerRcfSubmit()//Include JSP's contains different JS files which is having same fnSubmit functions.
{
	

	    	 if(cbo_type == "LOTRECONFIG"){
	    		fnValidateTxtFld('Txt_LogReason','Comments ');
	    		//PMT-53016 Process Check Screen's comment session shouldnt be mandatory
	 			if(fnValidateReconfigLot()){
	 				document.frmVendor.hAction.value = 'LOTRECONFIG';
	 				fnStartProgress('Y');
	 				document.frmVendor.submit();	
	 			}
	 						
	 	}else{
	 		if (validate())
	 		{
	 			if (fnCreateInputString())
	 			{
	 				var inputString = getIncidentsString();
	 				document.frmVendor.hInputChgStr.value = inputString;
	 				document.frmVendor.hAction.value = 'ProcessReturnSet';
	 				fnStartProgress('Y');
	 				document.frmVendor.submit();
	 				if(parent.window.document.getElementById("refId") != undefined){
	 	  				parent.window.document.getElementById("refId").value = "";
	 	  				parent.window.document.getElementById("refId").focus();
	 				}
	 			}
	 		}
	 	}
     
	
	
	
   
	
	
	
}
//Validating the inputs and forming the string for Reconfigure by Lot screen
function fnValidateReconfigLot() {
	
	var hCount;
	var partNumObj;
	var setQtyObj;
	var qtySetObj;
	var reconQtyObj;
	var removeLotObj;
	var newLotObj;
	var invalidlotObj;
	
	var reconfigQty=0;
	var setListQty=0;
	var qtyInSetQty=0;
	var removeLot =0;
	var newLot = "";
	var invalidLotFl="";
	var errorReconQty = "";
	var moreReconQty = "";
	var removeLotError = "";
	var newLotError = "";
	var invalidLotError="";
	var inputReconStr = "";
	var lotValidateFl = true;
	var rmvLotQty;
	var remLotNegQty;
	var newLtTxt;
	
	hCount = parseInt(document.frmVendor.hcounter.value)-1;
	
	for (k=0; k <= hCount; k ++)
    {
		partNumObj = eval("document.frmVendor.hPartNum"+k);
		
		if(partNumObj == undefined)
		{
			continue;
		}	

		reconQtyObj = eval("document.frmVendor.Txt_Recon_Qty"+k);
		reconfigQty = (reconQtyObj != undefined)?  reconQtyObj.value:"";

		qtySetObj = eval("document.frmVendor.hSetQty"+k);
		qtyInSetQty = (qtySetObj != undefined)?  qtySetObj.value:"";
		
		
		setQtyObj = eval("document.frmVendor.hSetListQty"+k);
		setListQty = (setQtyObj != undefined)?  setQtyObj.value:"";
		
		removeLotObj = eval("document.frmVendor.Cbo_UseLot"+k);
 	    removeLot = (removeLotObj != undefined)?  removeLotObj.value:"";
		
 	    newLotObj = eval("document.frmVendor.Txt_New_Lot"+k);
 	    newLtTxt = newLotObj.value.trim();
 	    newLot = (newLotObj != undefined)?  newLtTxt:"";
 	    
 	   	invalidlotObj = eval("document.frmVendor.hInvalidLotFl"+k);
 	    invalidLotFl = (invalidlotObj != undefined)?  invalidlotObj.value:"";
 	    if(invalidLotFl == 'Y'){
 	    	lotValidateFl = false;
 	    }
 	    
 	   if(reconfigQty !=0 && reconfigQty != ''){
			reconfigQty = reconfigQty.replace(/^0+/, '');
		}
		//validating input for reconfigure lot column, will support > 0
    	var reconFl = fnValidateInput(reconfigQty);
		if(reconFl == false){
			errorReconQty = errorReconQty + partNumObj.value + ",";
		}

		//PMT-42614 Change Lot Numbers of Loaner Set
		if ( removeLot != 0){
			
             rmvLotQty = removeLot.lastIndexOf("(");
             remLotNegQty = rmvLotQty.toString().substring(0,1);
           }

		
		//Check reconfigure qty is greater than set list qty 
		//and also added new lot !='' condition to  remove Qty if qty in set is lesser than lot qty
		if((parseInt(reconfigQty) > parseInt(qtyInSetQty)) && newLot != ''){ 
			moreReconQty = moreReconQty + partNumObj.value + ",";
		}else if(reconFl == true && reconfigQty != '' && removeLot != 0 && newLot == '' && remLotNegQty== '-' ){ //for remove lot
			inputReconStr +=  partNumObj.value+'^'+reconfigQty+'^'+removeLot+'^|';
			document.frmVendor.hInputReconLotStr.value = inputReconStr;
		}else if(reconFl == true && reconfigQty != ''&& removeLot == 0&& newLot != '' && invalidLotFl == 'Y'){//add lot
			var array=[newLot,partNumObj.value];
			Error_Details(Error_Details_Trans(message_operations[741],array));
		}else if(reconFl == true && reconfigQty != ''&& removeLot != 0 && newLot != '' && invalidLotFl == 'Y'){//add lot
			var array=[newLot,partNumObj.value];
			Error_Details(Error_Details_Trans(message_operations[741],array));
		}
		else if(reconFl == true && reconfigQty != ''&& removeLot == 0&& newLot != '' && invalidLotFl != 'Y'){//add lot
			inputReconStr +=  partNumObj.value+'^'+reconfigQty+'^'+'0'+'^'+newLot+'^|'; 
			document.frmVendor.hInputReconLotStr.value = inputReconStr;
		}else if(reconFl == true && reconfigQty != '' && removeLot != 0 && newLot != '' && invalidLotFl == ''){ // form input string
			inputReconStr +=  partNumObj.value+'^'+reconfigQty+'^'+removeLot+'^'+newLot+'^|';
			document.frmVendor.hInputReconLotStr.value = inputReconStr;
		}
		
    }
	
	if(errorReconQty != ""){
		
		Error_Details(message_operations[737]+errorReconQty.substr(0,errorReconQty.length-1));
	}
	
	if(moreReconQty != ""){
		Error_Details(message_operations[738]+moreReconQty.substr(0,moreReconQty.length-1));
	}
	
//	if(removeLotError != ""){
//		Error_Details(message_operations[739]+removeLotError.substr(0,removeLotError.length-1));
//	}
	
//	if(newLotError != ""){
//		Error_Details(message_operations[740]+newLotError.substr(0,newLotError.length-1));
//	}

	if(errorReconQty == '' && moreReconQty == '' && removeLotError == '' && newLotError == '' && lotValidateFl == true && inputReconStr == ''){
		Error_Details(message_operations[742]);
	}
	 
	 if (ErrorCount > 0)
	 {		
	 	Error_Show();
		Error_Clear();
	 	return false;
	 }
	 else{
		 
		return true;
	 }
	
	
	
}
//PMT-42614 Change Lot Numbers of Loaner Set
//to disable the new lot text box if the quantity is negative 
function fnChangeTxtDisable(val,pnum){
	var cnumobj=eval("document.frmVendor.Cbo_UseLot"+val); 
	var cnum = cnumobj.options[cnumobj.selectedIndex].id;
    var newLoTtxt = eval("document.frmVendor.Txt_New_Lot"+val);

    //if user changes from positive qty to negative , the value must be null and disabled
	if(cnum.indexOf('-')!= -1){
		newLoTtxt.value = "";
		newLoTtxt.disabled = true;
		document.getElementById('DivShowTagAvl'+val).style.display='none';
		document.getElementById('DivShowTagNotExists'+val).style.display='none';
		}
	else{
		newLoTtxt.disabled = false;
		}
	
}
//to validate the field support the non decimal and 0
function fnValidateInput(val)
{
	
	var objval = TRIM(val);

	 var objRegExp =  /^[1-9][0-9]*$/;
	
	if(objval == '' || objRegExp.test(objval)){
		return true;
	}
	else{
		return false;
	}
	
}

function getIncidentsString()
{
	var inputString = '';

	var form = document.frmVendor;
	for(var i=0; i<form.elements.length; i++)
	{
		if(form.elements[i].name.indexOf('charge') != -1 && form.elements[i].name.indexOf('hcharge') == -1)
		{
			var nm = 'h' + form.elements[i].name;
			var objId = document.getElementById(nm).value;
			var objVal = form.elements[i].value;
			
			if(objVal == '' || objVal == '0')
			{
				continue;
			}		
			inputString += objId + ';' + objVal + '|';
		}
	}
	return inputString;
}

function changeTROBgColor(object,val,BgColor) {
   var obj1 = eval("document.all.tr"+prevtr);
   obj1.style.background = "";
   var obj = eval("document.all.tr"+val);
   prevtr = val;
   obj.style.background = BgColor;
   object.style.background = BgColor;
}

function fnPrintVer(val)
{
	windowOpener("/GmInHouseSetServlet?hAction=ViewPrint&hConsignId="+val+"&hType="+type,"LoanPrint","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnCallAjax(partnum,val,ind) 
{	
	index = ind;

	var frmvalue =eval("document.all.Cbo_Repfrom"+index);
	
	if (frmvalue.value == 0){
		var QtyObj = eval("document.all.Txt_Stock"+ind);
		QtyObj.value = "";	
		return;
	}

	var url = fnAjaxURLAppendCmpInfo("/GmLoanerAjaxServlet?opt=" + encodeURIComponent(frmvalue.value)+ "&partNum=" + encodeURIComponent(partnum));
	
	if (typeof XMLHttpRequest != "undefined") 
	{
	   req = new XMLHttpRequest();
	} 
	else if (window.ActiveXObject) 
	{
	   req = new ActiveXObject("Microsoft.XMLHTTP");
	}
	req.open("GET", url, true);
	req.onreadystatechange = callback;
	req.send(null);
}
 
function callback() {
    if (req.readyState == 4) {
        if (req.status == 200) {
	        var xmlDoc = req.responseXML;
			parseMessage(xmlDoc);
        }        
    }
}

function parseMessage(xmlDoc) 
{
	var pnum = xmlDoc.getElementsByTagName("pnum");
	for (var x=0; x<pnum.length; x++) 
	{
		var qty = parseXmlNode(pnum[x].childNodes[0].firstChild);
	}
	var frmvalue =eval("document.all.Cbo_Repfrom"+index);
	
	var QtyObj = eval("document.all.Txt_Stock"+index);
	
  if(frmvalue.value == '0')
  {
	  QtyObj.value = "";
  }
  else
  {
	  QtyObj.value = qty  ;
  }
}

var cnt = 0;

function getComboBox(id, cboid, width, event)
{
	var styl = "class=RightText";
	var onChangestr ="";

	if ( width != undefined && width != '')
	{
		styl = "style='width:"+width+"'";
	}
	
	//alert(styl);
	if ( event != undefined && event != '' )
	{
		 onChangestr=event;
	}
	
	var returnstr;
	returnstr = "<select id='"+id+"'"+styl+" " + onChangestr + " >";

	   var list = document.getElementById(cboid);

       for(var i = 0; i < list.options.length; ++i)
       {
    	   returnstr = returnstr +  "<option value='"+list.options[i].value+"'>"+list.options[i].text+"</option>";
       }
       returnstr = returnstr +  "</select>";
     
       return  returnstr;       
}

function fnSplit(val,pnum)
{		
	var missQtyInputType = "hidden";
	var missonBlurEvent = "";
	cnt++;
	var hcnt = parseInt(document.frmVendor.hcounter.value) - 1;	 //2
	hcnt = hcnt+1; //3
	var NewQtyHtml = "";
	var QtyObj;
	var expandCnt = eval("document.all.hExpandCnt"+val).value;
	expandCnt = expandCnt == '' ? 1 : expandCnt;
	if(cbo_type == "LOTRECONFIG"){//For Reconfigure by Lot screen
		
		missQtyInputType = "input";
		missonBlurEvent ="onBlur=fnPartLotAjax("+hcnt+",'"+pnum+"');";
		validLotEvent = "onClick=javascript:fnToggleLot("+hcnt+") onBlur=javascript:fnValidateLot("+hcnt+",'"+pnum+"')";
		
		var setQtyobj = eval("document.all.hSetQty"+val);
		if(expandCnt == setQtyobj.value && setQtyobj.value == 0){
			document.getElementById("splitId"+val).style.pointerEvents = 'none';
			document.getElementById("splitId"+val).style.cssText = 'pointer-events: none;';
			eval("document.all.hExpandCnt"+val).value = 1;
			
		}else{

			NewQtyHtml = "<span border=1 id='Sp"+cnt +"' align='right' valign='center'><BR>&nbsp;&nbsp; <a href=javascript:fnToggleLot("+hcnt+");javascript:fnEnableExpand("+val+");javascript:fnDelete("+cnt+");><img src=/images/btn_remove.gif border=0 height=13 width=13 ></a> </span>";
			QtyObj = eval("document.all.COLUMN5"+val);
			QtyObj.insertAdjacentHTML("beforeEnd",NewQtyHtml);
			
			obj = eval("document.all.hSetListQty"+val);
			cnt++;
			NewQtyHtml = "<span border=1 id='Sp"+cnt +"'> <input type=hidden name=hSetListQty"+hcnt+" value ="+obj.value+" id="+hcnt+" > </span>";
			QtyObj = eval("document.all.COLUMN5"+val);
			QtyObj.insertAdjacentHTML("beforeEnd",NewQtyHtml);
			
			obj = eval("document.all.hSetListQty"+val);
			
			
			cnt++;	
			NewQtyHtml = "<span border=1 id='Sp"+cnt +"'> <input type=hidden name=hSetQty"+hcnt+" value ="+setQtyobj.value+" id="+hcnt+" > </span>";
			QtyObj = eval("document.all.COLUMN3"+val);
			QtyObj.insertAdjacentHTML("beforeEnd",NewQtyHtml);
			
			expandCnt++;
			eval("document.all.hExpandCnt"+val).value = expandCnt;
			if(expandCnt == setQtyobj.value){
				console.log(document.getElementById("splitId"+val).style.cssText);
				document.getElementById("splitId"+val).style.pointerEvents = 'none';
				document.getElementById("splitId"+val).style.cssText = 'pointer-events: none;';
				console.log(document.getElementById("splitId"+val).style.cssText);
				eval("document.all.hExpandCnt"+val).value = expandCnt;
			}
			
			obj = eval("document.all.Txt_Recon_Qty"+val);
			cnt++;
			NewQtyHtml = "<span border=1 id='Sp"+cnt +"'> <input type='text' size='3' name=Txt_Recon_Qty"+hcnt+" id="+hcnt+" value='' "+missonBlurEvent+" > </span>";  
			QtyObj = eval("document.all.COLUMN4"+val);
			QtyObj.insertAdjacentHTML("beforeEnd",NewQtyHtml);
			
			cnt++;
			NewQtyHtml = "<span border=1 id='Sp"+cnt +"'><BR>&nbsp;"+getComboBox("Cbo_UseLot"+hcnt,"Cbo_UseLot"+val,'150px',"onChange=javascript:fnChangeTxtDisable('"+hcnt+"','"+pnum+"');") + "</span>";
			QtyObj = eval("document.all.COLUMN111"+val);
			QtyObj.insertAdjacentHTML("beforeEnd",NewQtyHtml);
			
			cnt++;
			NewQtyHtml = "<span border=1 id='Sp"+cnt+"'><BR><input type='text' name=Txt_New_Lot"+(hcnt)+" size='20' class='RightText' id="+hcnt+" "+validLotEvent+" /> <input type='hidden' name=hInvalidLotFl"+hcnt+" value='' id=hInvalidLotFl"+hcnt+" />  <input type='hidden' name=hPartNum"+hcnt+" value='"+pnum+"'  id='"+pnum+"' /> <input type='hidden' name=hpnum"+val+" value='"+pnum+"'  id='"+hcnt+"' /> </span> " +
						 "<span id='DivShowTagAvl"+(hcnt)+"' style='vertical-align:middle; display:none;'> <img height=16 width=19 src=/images/success.gif></img></span>" +
						 "<span id='DivShowTagNotExists"+(hcnt)+"' style='vertical-align:middle;display: none;'> <img  height=12 width=12 src=/images/delete.gif></img></span> ";
			QtyObj = eval("document.all.COLUMN8"+val);
		    QtyObj.insertAdjacentHTML("beforeEnd",NewQtyHtml);
		} 
		
	}else{
		
		if(lotFlag =="YES" && loansflcd!=0 && loansflcd!=5 && loansflcd!=10){
			 missQtyInputType = "input";
			 missonBlurEvent ="onBlur=fnPartLotAjax("+hcnt+",'"+pnum+"');";
			NewQtyHtml =  "<span border=1 id='Sp"+cnt +"' align='right' valign='center'><BR>&nbsp;&nbsp; <a href=javascript:fnDelete("+cnt+");><img src=/images/btn_remove.gif border=0 height=13 width=13 ></a> </span>";
			QtyObj = eval("document.all.COLUMN3"+val);
			QtyObj.insertAdjacentHTML("beforeEnd",NewQtyHtml);
			
			cnt++;
			NewQtyHtml = "<span border=1 id='Sp"+cnt +"'><BR>&nbsp;"+getComboBox("Cbo_UseLot"+hcnt,"Cbo_UseLot"+val,'150px',"onchange=javascript:fnCheckLotNum("+hcnt+","+pnum+")") + "</span>";
			QtyObj = eval("document.all.COLUMN111"+val);
			QtyObj.insertAdjacentHTML("beforeEnd",NewQtyHtml);
		}else{
			NewQtyHtml = "<span border=1 id='Sp"+cnt +"' align='right' valign='center'><BR>&nbsp;&nbsp; <a href=javascript:fnDelete("+cnt+");><img src=/images/btn_remove.gif border=0 height=13 width=13 ></a> </span>";
			QtyObj = eval("document.all.COLUMN5"+val);
			QtyObj.insertAdjacentHTML("beforeEnd",NewQtyHtml);
		}
		
		var obj = eval("document.all.hSetQty"+val);
		cnt++;	
		NewQtyHtml = "<span border=1 id='Sp"+cnt +"'> <input type=hidden name=hSetQty"+hcnt+" value ="+obj.value+" id="+hcnt+" > </span>";
		QtyObj = eval("document.all.COLUMN3"+val);
		QtyObj.insertAdjacentHTML("beforeEnd",NewQtyHtml);
	
		
		if (lnSts == '25'){ 
			 obj = eval("document.all.Txt_Miss"+val);
			cnt++;
			NewQtyHtml = "<span border=1 id='Sp"+cnt +"'> <input type="+missQtyInputType+" size='3' name=Txt_Miss"+hcnt+" id="+hcnt+" value='' "+missonBlurEvent+" > </span>"; //PMT-794 Assigning empty value 
			QtyObj = eval("document.all.COLUMN4"+val);
			QtyObj.insertAdjacentHTML("beforeEnd",NewQtyHtml);
		}
		
		obj = eval("document.all.hSetListQty"+val);
		cnt++;
		NewQtyHtml = "<span border=1 id='Sp"+cnt +"'> <input type=hidden name=hSetListQty"+hcnt+" value ="+obj.value+" id="+hcnt+" > </span>";
		QtyObj = eval("document.all.COLUMN5"+val);
		QtyObj.insertAdjacentHTML("beforeEnd",NewQtyHtml);
		
		obj = eval("document.all.hPrice"+val);
		cnt++;
		NewQtyHtml = "<span border=1 id='Sp"+cnt +"'> <input type=hidden name=hPrice"+hcnt+" value ="+obj.value+" id="+hcnt+" > </span>";
		QtyObj = eval("document.all.COLUMN5"+val);
		QtyObj.insertAdjacentHTML("beforeEnd",NewQtyHtml);
		
		cnt++;
		NewQtyHtml = "<span border=1 id='Sp"+cnt +"'><BR>&nbsp;&nbsp;"+getComboBox("Cbo_Repfrom"+hcnt,"Cbo_Repfrom0",'',"onChange=javascript:fnCallAjax('"+pnum+"','',"+hcnt+");javascript:fnCheckRepType('"+val+"','"+pnum+"');") + "</span>";
		QtyObj = eval("document.all.COLUMN6"+val);
		QtyObj.insertAdjacentHTML("beforeEnd",NewQtyHtml);
		
		cnt++;
		NewQtyHtml = "<span border=1 id='Sp"+cnt +"'> <input type=text name=Txt_Stock"+hcnt+" size=5 class=hide id="+hcnt+" > </span>";
		QtyObj = eval("document.all.COLUMN7"+val);
		QtyObj.insertAdjacentHTML("beforeEnd",NewQtyHtml);
		
		cnt++;
		NewQtyHtml = "<span border=1 id='Sp"+cnt+"'><BR><input type='text' name=Txt_Rep"+(hcnt)+" size='3' class='RightText' id="+hcnt+" onBlur=javascript:fnCheckRepType('"+val+"','"+pnum+"'); /></span>";
		QtyObj = eval("document.all.COLUMN8"+val);
	    QtyObj.insertAdjacentHTML("beforeEnd",NewQtyHtml);
	    
	    cnt++;
		NewQtyHtml = "<span border=1 id='Sp"+cnt+"'><BR>"+ getComboBox("Cbo_MoveTo"+hcnt,"Cbo_MoveTo0",'',"onChange=javascript:fnCheckMoveTo('"+val+"','"+pnum+"');") +"</span>";
		QtyObj = eval("document.all.COLUMN9"+val);
		QtyObj.insertAdjacentHTML("beforeEnd",NewQtyHtml);
		
		cnt++;
		NewQtyHtml = "<span border=1 id='Sp"+cnt+"'><BR><input type='text' name='Txt_Remove"+hcnt+"' size='3' class='RightText' id="+hcnt+" onBlur=javascript:fnCheckMoveTo('"+val+"','"+pnum+"'); /> <input type='hidden' name=hPartNum"+hcnt+" value='"+pnum+"'  id='"+pnum+"' /> <input type='hidden' name=hpnum"+val+" value='"+pnum+"'  id='"+hcnt+"' /> </span>";
		//alert(NewQtyHtml);
		QtyObj = eval("document.all.COLUMN10"+val);
		QtyObj.insertAdjacentHTML("beforeEnd",NewQtyHtml);
	}
	document.frmVendor.hcounter.value =hcnt+1;
}

function fnEnableExpand(val) {
	var hExpandCnt = parseInt(eval("document.all.hExpandCnt"+val).value);
	eval("document.all.hExpandCnt"+val).value = hExpandCnt -1;
	document.getElementById("splitId"+val).style.pointerEvents = '';
	document.getElementById("splitId"+val).style.cssText = '';
}


function fnDelete(val)
{
	var indexSize= (lotFlag =="YES" && loansflcd!=0 && loansflcd!=5 && loansflcd!=10)?11:10;
	
	for (var i =0; i< indexSize ; i++){
		if (i == 2 && lnSts == '25' ){
			obj = eval("document.all.Sp"+val);
			obj.innerHTML = "";
			val++;
		}
		else {
			obj = eval("document.all.Sp"+val);
			obj.innerHTML = "";
			val++; 	
		}
	}
	var hcnt = parseInt(document.frmVendor.hcounter.value) ;
	document.frmVendor.hcounter.value =hcnt-1;
	////alert(document.frmVendor.hcounter.value);
}

function fnProcessCheckRowHighlight(tableObj,rowId) {
    for (var index = 0; index < tableObj.childNodes.length; index++) {
        var row = tableObj.childNodes[index];
       if (row.style) {
            row.style.backgroundColor ="#81b4d7";
            if(index == 7){
            	var partids = document.getElementsByName("hpnum"+rowId);
            	for ( var stockindex =0 ; stockindex < partids.length; stockindex++)
                {
            	stockObj = eval("document.frmVendor.Txt_Stock"+partids[stockindex].id);
            	if(stockObj != undefined){
            		stockObj.style.backgroundColor = "#81b4d7";
            		}
            	}
            		
            }
        }
    }
}

function fnProcessCheckRestoreBgColor(tableObj,rowId) {
	 
    for (var index = 0; index < tableObj.childNodes.length; index++) {
        var row = tableObj.children[index];
        var column4 = document.getElementById("COLUMN4"+rowId);
       if(lotFlag =="YES" && loansflcd!=0 && loansflcd!=5 && loansflcd!=10){
	       if(index == 2|| (index == 4 && lotFlag =="YES") || (index == 3 && column4!=undefined)){
	        	row.style.backgroundColor = "#ffecb9";
	        }else if (index == 5 && column4 == undefined){
	        	row.style.backgroundColor = "#e4e6f2";
	        }else if(index == 6 || index == 7 || index == 8 ){
	             if(index == 8){
	                 	var partids = document.getElementsByName("hpnum"+rowId);
	                 	for ( var stockindex =0 ; stockindex < partids.length; stockindex++)
	                     {
	                 	stockObj = eval("document.frmVendor.Txt_Stock"+partids[stockindex].id);
	                 	if(stockObj != undefined){
	                 		stockObj.style.backgroundColor = "#e4e6f2";
	                 		}
	                 	}
	                 		
	               
	             }
	        	row.style.backgroundColor = "#e4e6f2";
	        }else if(index == 9 || index == 10){
	        	row.style.backgroundColor = "#e1f3a6";
	        }else {
	            row.style.backgroundColor = "#FFFFFF";
	        }
       }else{
    	   if(index == 2 || (index == 3 && column4!=undefined)){
           	row.style.backgroundColor = "#ffecb9";
           }else if (index == 4 && column4 == undefined){
           	row.style.backgroundColor = "#e4e6f2";
           }else if(index == 5 || index == 6 || index == 7 ){
                if(index == 7){
                    	var partids = document.getElementsByName("hpnum"+rowId);
                    	for ( var stockindex =0 ; stockindex < partids.length; stockindex++)
                        {
                    	stockObj = eval("document.frmVendor.Txt_Stock"+partids[stockindex].id);
                    	if(stockObj != undefined){
                    		stockObj.style.backgroundColor = "#e4e6f2";
                    		}
                    	}
                    		
                  
                }
           	row.style.backgroundColor = "#e4e6f2";
           }else if(index == 8 || index == 9){
           	row.style.backgroundColor = "#e1f3a6";
           }else {
            row.style.backgroundColor = "#FFFFFF";
           }
       }
    }
}

function fnPrint(printOpt){
document.getElementById("successMsg").innerHTML ="";
if(cbo_type =="LNRECONFIG" || cbo_type == "PROCESSRETURN"){
     var ajaxUrl = fnAjaxURLAppendCmpInfo("/gmReconfigLoaner.do?method=loadReconfgPrintDtl&strConsignId="+val+"&strSetId="+setid+"&ramdomId="+Math.random());
	 dhtmlxAjax.get(ajaxUrl, function(loader){	
	var response = loader.xmlDoc.responseText;
});
 
}
if(printOpt == 'manually'){
	fnPrintPPW(printOpt);
	fnPrintVersion(printOpt);
	fnOpenDoc(printOpt);
}

if(reconfigLoanFl != 'Y'){
	var defaultprint = executeCommands();
	if(defaultprint == ''){
		Error_Details(message_operations[174]);
	}
	if (ErrorCount > 0){		
		Error_Show();
		Error_Clear();
		return false;
	}
	}
	if(printOpt != 'manually'){
		if(reconfigLoanFl == 'Y'){
		fnPDFPrintPPW(printOpt);
		fnPDFPrintVersion(printOpt);
		fnPDFOpenDoc(printOpt);
		document.getElementById("successMsg").innerHTML ="Print paper work is scheduled.";
	}else{
	fnPrintPPW(printOpt);
		setTimeout("popupWin.close();",3000);
		setTimeout("fnPrintVersion("+printOpt+");",3100);
		setTimeout("popupWin.close();",6000);
		setTimeout("fnOpenDoc("+printOpt+");",6100);
		}
		
	}/*else{
		fnPrintPPW(printOpt);
		fnPrintVersion(printOpt);
		fnOpenDoc(printOpt);
	}*/
}

function fnPrintPPW(hopt){
	var comments = document.frmVendor.Txt_LogReason.value;
	windowOpener("/GmInHouseSetServlet?hAction=PrintLoanerPPW&hConsignId="+val+"&hOpt="+hopt+"&Txt_LogReason="+comments,"PrintLoaner","resizable=yes,scrollbars=yes,top=10,left=150,width=800,height=600");		
}
function fnPrintVersion(hopt){
	windowOpener("/GmInHouseSetServlet?hAction=ViewPrint&hConsignId="+val+"&hType="+type+"&hOpt="+hopt,"LoanPrint","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}
function fnOpenDoc(hopt){
	windowOpener("/GmInHouseSetServlet?hAction=InsViewer&hSetId="+setid+"&hType="+type+"&hOpt="+hopt,"OpenDocument","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}
function executeCommands(){
	var oShell = new ActiveXObject("WScript.Shell");
	sRegVal = 'HKEY_CURRENT_USER\\Software\\Microsoft\\Windows NT\\CurrentVersion\\Windows\\Device';
	sDefault = oShell.RegRead(sRegVal);
	return sDefault;
}

//--below function is used to fetch control number in loaner process check for japan company 
function fnPartLotAjax(rowId,partnum){
	var ajaxUrl = fnAjaxURLAppendCmpInfo("/gmSetLotMaster.do?method=loadSetPartLotDtl&txnId="+val+"&partNum="+encodeURIComponent(partnum)+"&ramdomId="+Math.random());
	dhtmlxAjax.get(ajaxUrl, function(loader){	
		var resJSONObj = $.parseJSON(loader.xmlDoc.responseText);
		var obj=document.getElementById("Cbo_UseLot"+rowId);
		fnPopulateOptions(obj, resJSONObj, "CNUM", "CNUMQTY", "CQTY");
	});
}

//Open Back order report from Process Transaction from Process Check screen
function fnOpenLoanerItemsBORpt(refId)
{
	var type = '';
	
	//4127 - Loaner ,50266 - Loaner Items Back Order,
	//4119 - InHouse Loaner ,50269 - In House Loaner items
	if(cnType == 4127) {
		type = '50266';
	}else if(cnType == 4119) {
		type = '50269';
	}
	
	location.href  ="/GmOrderItemServlet?Cbo_Type="+type+"&hOpt=ProcBO&hFrom=PROCESS_TXN_SCN&REFID="+refId+"&"+fnAppendCompanyInfo();
}
//Ajax call to validate the Lot numner in New Lot# field
function fnValidateLot(hcnt,pnum){

	var cnumObj = eval("document.frmVendor.Txt_New_Lot"+hcnt); 
	var cnum = cnumObj.value;
	
	cnum=cnum.replace(/(^\s+|\s+$)/g, '');

	if(pnum != '' && cnum != ''){
		
		var ajaxUrl = fnAjaxURLAppendCmpInfo("/gmSetLotMaster.do?method=validateLotNumber&ctrlNum="+encodeURIComponent(cnum)+"&partNum="+encodeURIComponent(pnum)+"&ramdomId="+Math.random());
		dhtmlxAjax.get(ajaxUrl,function(loader)
				{
					var response = loader.xmlDoc.responseText;
					if(response == ''){
						document.getElementById('DivShowTagAvl'+hcnt).style.display='inline-block';
						document.getElementById('DivShowTagNotExists'+hcnt).style.display='none';
						document.getElementById('hInvalidLotFl'+hcnt).value = '';

					}else{
						document.getElementById('DivShowTagNotExists'+hcnt).style.display='inline-block';
						document.getElementById('DivShowTagAvl'+hcnt).style.display='none';
						document.getElementById('hInvalidLotFl'+hcnt).value = 'Y';
											}
				});
		
	}
}
//To remove the image
function fnToggleLot(hcnt){
	document.getElementById('DivShowTagAvl'+hcnt).style.display='none';
	document.getElementById('DivShowTagNotExists'+hcnt).style.display='none';
}

//This function used to print PPW paperwork
function fnPDFPrintPPW(printOpt){
var ajaxUrl = fnAjaxURLAppendCmpInfo("/gmReconfigLoaner.do?method=printReconfgPrintDtl&strPrintOpt=PrintPPW&strConsignId="+val+"&strSetId="+setid+"&ramdomId="+Math.random());
	dhtmlxAjax.get(ajaxUrl, function(loader){	
	var response = loader.xmlDoc.responseText;
	});
}
//This function used to print loaner Paper work 
function fnPDFPrintVersion(printOpt){
var ajaxUrl = fnAjaxURLAppendCmpInfo("/gmReconfigLoaner.do?method=printReconfgPrintDtl&strPrintOpt=ViewPrint&strConsignId="+val+"&strSetId="+setid+"&ramdomId="+Math.random());
	dhtmlxAjax.get(ajaxUrl, function(loader){	
	var response = loader.xmlDoc.responseText;
	});
}
//This function used to print inspection sheet 
function fnPDFOpenDoc(printOpt){
var ajaxUrl = fnAjaxURLAppendCmpInfo("/gmReconfigLoaner.do?method=printReconfgPrintDtl&strPrintOpt=InsViewer&strConsignId="+val+"&strSetId="+setid+"&ramdomId="+Math.random());
	dhtmlxAjax.get(ajaxUrl, function(loader){	
	var response = loader.xmlDoc.responseText;
	});
}