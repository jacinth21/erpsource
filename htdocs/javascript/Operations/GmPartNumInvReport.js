/**
 * 
 */

function fnGo()
{
	var emptyInv = '';
	var pnum = document.frmAccount.hPartNum.value;
	var viewall = document.frmAccount.hViewAll.value;
	var setId = document.frmAccount.hSetId.value;
	var setIdObj= setId.substring(0,setId.indexOf(','));
   if(setIdObj != ""){
	   document.frmAccount.hSetId.value = setIdObj;
	   var rmvSetId = setId.substring(setId.indexOf(',')+1,setId.length);
	   if(rmvSetId != ""){ 
		   document.frmAccount.hSetId.value = setId;
		Error_Details(message_operations[771]);
		Error_Show();
		Error_Clear();
		return false;
	   }
	}else{
		 document.frmAccount.hSetId.value = setId;
	}
	 	//PC-4543 Upper Case Conversion Issue
	if(document.frmAccount.hSetId != undefined){
    	if(document.frmAccount.hSetId.value != undefined && document.frmAccount.hSetId.value != ""){
    	document.frmAccount.hSetId.value = TRIM(document.frmAccount.hSetId.value.toUpperCase());
    	}
    }
	if (viewall != 'true')
	{
     pnum = pnum.replace(/\s/g, '');  // to remove space from the part number
 	if(setId == ""){	
 	if ((pnum == '' && document.frmAccount.Cbo_ProjId.value == 0) || (pnum.indexOf("..") !=  -1))
		{
			Error_Details(message_operations[348]);
			Error_Show();
			Error_Clear();
			return false;			
		}
 
	if (pnum.length < 3 && document.frmAccount.Cbo_ProjId.value == 0) 
	 {
	 Error_Details (message_operations[349]);
	 Error_Show();
	 Error_Clear();
	 return false;		 
	 }
 	}
	}
	if (pnum != ''){		
		document.frmAccount.hOpt.value = "MultipleParts";
		document.frmAccount.Cbo_ProjId.selectedIndex = 0;
	}	
	arr = document.frmAccount.Chk_GrpInventory;
	for(var i=0;i<arr.length;i++){
		if(arr[i].checked == false){
			emptyInv++;
		}
	}	
	var includedInv = '';
	for(var i=0;i<arr.length;i++){
		if(arr[i].checked){		
			includedInv = includedInv + ',' +arr[i].id;
		}			
	}	
	if(emptyInv == arrayLength ){			
		Error_Details(message_operations[350]);
		Error_Show();
		Error_Clear();
		return false;		
	}else{		
	document.frmAccount.hIncludedColumns.value = includedInv;
	document.frmAccount.hExcludedColumns.value = excludedInvs;	
	document.frmAccount.hAction.value = "Go";
	fnStartProgress('Y');
	document.frmAccount.submit();
	}
}

function changeTRBgColor(val,BgColor) {
   var obj1 = eval("document.all.tr"+prevtr);
   obj1.style.background = "";
   var obj = eval("document.all.tr"+val);
   prevtr = val;
   obj.style.background = BgColor;
}

function fnViewDetails(pnum,act)
{
	fnStartProgress('Y');
	windowOpener("/GmPartInvReportServlet?hAction=DrillDown&hOpt="+act+"&hPartNum="+encodeURIComponent(pnum),"INVPOP","resizable=yes,scrollbars=yes,top=300,left=300,width=710,height=200");
	fnStopProgress();
}

function fnPrintDownVer(type)
{
	var projid = document.frmAccount.hId.value;
	var defaultCols = document.frmAccount.hIncludedColumns.value;
	var pnum = document.frmAccount.hPartNum.value;
	var setId = document.frmAccount.hSetId.value;
	var hopt = '';
	var hsearch = '';
	var subComponentFl ='';
	var enableObsolete = '';
	if (pnum != ''){
		hopt = "MultipleParts";
	}
	if(document.frmAccount.subComponentFl.checked){
	  subComponentFl = 'on';
	}
	if(document.frmAccount.enableObsolete.checked){
	  enableObsolete = 'on';
	}
	hsearch = document.frmAccount.Cbo_Search.value;
	fnStartProgress('Y');
	windowOpener("/GmPartInvReportServlet?hAction="+type+"&Cbo_Search="+hsearch+"&hOpt="+hopt+"&hPartNum="+encodeURIComponent(pnum)+"&Cbo_ProjId="+projid+"&subComponentFl="+subComponentFl+"&enableObsolete="+enableObsolete+"&hIncludedColumns="+defaultCols+"&hSetId="+setId,"INVPRINT","resizable=yes,scrollbars=yes,top=300,left=200,width=800,height=490");
	fnStopProgress();
}

function fnLoad()
{
	if(document.frmAccount.hPartNum != undefined && document.frmAccount.hPartNum != null){
	   document.frmAccount.hPartNum.focus();
	}
	var sel = document.frmAccount.hSearch.value;
	if (sel != '')
	{
		document.frmAccount.Cbo_Search.value = sel;
	}
	arr = document.frmAccount.Chk_GrpInventory;
	fnCheckSelections(document.frmAccount.hIncludedColumns.value,arr,document.frmAccount.selectAllInventories);
}

function fnCheckSelections(val,arr,selAllObj)
{
	var valobj = val.split(",");
	var sval = '';
	var arrlen = arr.length;
	var selItemCount = 0;
	if (arrlen > 0)
	{
		for (var j=0;j< valobj.length;j++ )
		{
			sval = valobj[j];
			for (var i=0;i< arrlen;i++ )
			{
				if (arr[i].id == sval)
				{
					arr[i].checked = true;
					selItemCount++;
					break;
				}
			}
		}		
		if(selItemCount==arrlen)
			selAllObj.checked = true;		
	}
	else
	{
		arr.checked = true;
		selAllObj.checked = true;
	}
}
function fnSelectInv(){
	var k = 0;
	var objCheckInvArr = document.frmAccount.Chk_GrpInventory;
	var len = objCheckInvArr.length;
	for( var j=0; j<len; j++)
	{
		if(objCheckInvArr[j].checked){
			k++;
		}else{
			 document.frmAccount.selectAllInventories.checked = false;
			 return;
		}	
	}
	if(k == len){
		document.frmAccount.selectAllInventories.checked = true;
	}
}
function fnCallDisp(id, type)
{	
    fnStartProgress('Y');
	windowOpener("/GmPartNumServlet?hAction=LoadPartDetail&partNumber="+encodeURIComponent(id),"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=760,height=600");
	fnStopProgress();
}

function fnViewLocations(id)
{	
     //id = id.substring(0,7); --Not working for S parts.
    fnStartProgress('Y');
	windowOpener("/gmlocation.do?method=fetchInvLocationPartMapping&strOpt=view&hshowFilters=yes&partNum="+encodeURIComponent(id),"ViewLocation","resizable=yes,scrollbars=yes,top=250,left=300,width=1070,height=600");
	fnStopProgress();
}

function fnSelectAllInventories(varControl,varControlToSelect,varCmd){
	objSelAll = varControl;
	if (objSelAll ) {
	var objCheckSiteArr = eval("document.frmAccount."+varControlToSelect);

	if ((objSelAll.checked || varCmd == 'selectAll') && objCheckSiteArr)
	{
		objSelAll.checked = true;
		objCheckSiteArr.checked = true;
		objCheckSiteArrLen = objCheckSiteArr.length;
		for(i = 0; i < objCheckSiteArrLen; i ++) 
		{	
			objCheckSiteArr[i].checked = true;
		}
	}
	else if (!objSelAll.checked  && objCheckSiteArr ){
		objCheckSiteArrLen = objCheckSiteArr.length;
		objCheckSiteArr.checked = false;
		for(i = 0; i < objCheckSiteArrLen; i ++){	
			objCheckSiteArr[i].checked = false;
		}
	}
	}				

	if (varControl.name == 'selectAllInventories'){
		document.frmAccount.Chk_GrpInventory.checked = document.frmAccount.Chk_GrpInventory.checked;
		fnSelectAll(document.frmAccount.Chk_GrpInventory,'Chk_GrpInventory','toggle');
	}
}

function fnShowMoreFilters(){	
	var tabobj = document.all.InvFilter;
	objStatus(tabobj);
}

function objStatus(obj)
{
	if(obj != null){	
		if (obj.style.display == 'none')
		{
			obj.style.display = 'block';
		}
		else
		{
			obj.style.display = 'none';	
		}
	}		
}