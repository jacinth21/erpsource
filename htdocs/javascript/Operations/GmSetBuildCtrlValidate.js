function fnPageLoad(){
	fnExistScanPartCount();
}


//When click on Generate button to generate control numbers
function fnGenerate(pnum,controlno){
	var cnum = '';
	var val='';
	var control = new Object();
	var cnumstatus = false;
	var qtyobj = new Object();
	var qty = 0;
	var qtycnt = 0;
	var orgqty = 0;
	var vPnumProdMetType = '';
 // var pnum1 = pnum.replace(/\./g,'');
  
	 var pnum1 = pnum.replace(/[^a-zA-Z0-9]/g,''); //  PC-3459- Load control number from UDI in set build
  
  // Get the part number quantity
  var blk = document.getElementById("bulkQty"+pnum1);
  if(blk != null){
  	orgqty = parseInt(blk.value);
  }
	var pnumlistobj = document.getElementById(pnum); // part number index
	if(pnumlistobj != null){
		var pnumlist = pnumlistobj.value;
		var pnumlist = pnumlist.substring(0,pnumlist.length-1);
		var cnumarr= pnumlist.split(",");
		var cnumsize = cnumarr.length;
		//couting all sum of qty for passing part
		if(cnumsize > 0){
			for(i=0; i<cnumsize; i++){
				 qtyobj = document.getElementById("Txt_Qty"+cnumarr[i]);	
				 var cnumobj = document.getElementById('controlNum'+cnumarr[i]);
				 if(qtyobj != null){
					 qty = parseInt(qtyobj.value);
					 //alert('qty '+qty);
					 if(cnumobj != null){
						 cnum = cnumobj.value;
					 }
					 if (qty > 0 && qty != "" && cnum != null && cnum != '' && cnum != 'TBE'){
						 qtycnt = qtycnt + qty;
					 }
				 }
			}
			if(qtycnt >= orgqty){
				Error_Details(message_operations[33]+pnum);
				return false;
			}else{
				// updating control number and qty in existing row
				for(i=0; i<cnumsize; i++){
					val = cnumarr[0];
					var cnumobj = document.getElementById('controlNum'+cnumarr[i]); 					
					if(cnumobj != null){
						qtyobj = document.getElementById("Txt_Qty"+cnumarr[i]);
						qty = parseInt(qtyobj.value);
						cnum = cnumobj.value;
						 if (cnum == controlno){
								 if(qty+1 > orgqty){
									 Error_Details(Error_Details_Trans(message[5020],pnum));		
									 return false;
								 }else{
									 if(validateObject(document.getElementById("partMaterialType"))){// Getting Part num Product Material type
										   vPnumProdMetType = document.getElementById("partMaterialType").value;	
									 }
									 if(vPnumProdMetType == '100845'){ // Tissue Part
										 var array=[pnum,cnum];
										  Error_Details(Error_Details_Trans(message[5021],array));											 
										  return false;
									 }else{
										 qtyobj.value = qty + 1;
									 }
								 }							 
							 cnumstatus = false;
							 break;	
						 }else if(cnum == null || cnum == '' || cnum == 'TBE'){
							 cnumobj.value = controlno;
							 qtyobj.value = 1;
							 cnumstatus = false;						
							 break;						
						 }else{
							 cnumstatus = true;							
						 }		
					}
				}
				
			}
		}
	}else{
		Error_Details(Error_Details_Trans(message[5022],pnum));
		return false;
	}
	//updating control number and qty in newly added row
	if(cnumstatus){
		fnSplit(val,pnum,'');
		var count = parseInt(document.getElementById("hNewCnt").value);
		var hcount = parseInt(document.getElementById("hCnt").value);
		hcount = hcount+count;
		var cnumobj = document.getElementById('controlNum'+hcount); 
		cnumobj.value = controlno;
		qtyobj = document.getElementById("Txt_Qty"+hcount);
		qtyobj.value = 1;
	}
	
	if (ErrorCount > 0 ){
		Error_Show();
	    Error_Clear();		
	}else{
		fnScannedPartCount(pnum);
	}	
}
