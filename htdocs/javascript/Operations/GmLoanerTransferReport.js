function fnLoad(){
	var frm = document.frmLoanerTransfer;
	
	objDtfrom = frm.transferDtFrom;
	objDtTo = frm.transferDtTo;
	if(objDtfrom.value != ""){
		CommonDateValidation(objDtfrom, format, Error_Details_Trans(message[10531],format));
	}	
	if(objDtTo.value != ""){
		CommonDateValidation(objDtTo, format, Error_Details_Trans(message[10532],format));
	}
	if (dateDiff(objDtfrom.value, objDtTo.value,format) < 0)
	{
		Error_Details(message[10533]);
	}
	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}
	frm.strOpt.value='load';
	fnStartProgress();
	frm.submit();	
}
function fnOnPageLoad()
{	
	if (objGridData != '')
	{
		gridObj = initGrid('grpData',objGridData);		
	}
}
function fnPrintVer(val)
{
	windowOpener("/GmInHouseSetServlet?hAction=ViewSet&hConsignId="+val,"Con1","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}
function fnEnter()
{
	if (event.keyCode == 13)
	{ 
		fnLoad();
	}
}
function fnRollback(){
	var gridrowid;
	var usrdata='';
	var gridrows =gridObj.getChangedRows(",");
	if(gridrows.length > 0){
		var gridrowsarr = gridrows.toString().split(",");
		for (var rowid = 0; rowid < gridrowsarr.length; rowid++) {
			gridrowid = gridrowsarr[rowid];
			var raSelected=gridObj.cellById(gridrowid, 0).getValue();
			if(raSelected=="1"){ 
					usrdata+=gridObj.getUserData(gridrowid,"hConsignId") + ',';
			}
		}
		usrdata=usrdata.substr(0,usrdata.length-1);
	//	alert(usrdata);
document.frmLoanerTransfer.action= "/GmCommonCancelServlet?hTxnName="+ usrdata +"&hCancelType=RBLTR&hTxnId="+usrdata;
document.frmLoanerTransfer.submit();
	}
}
function fnSubmit(){
	var gridrowid;
	var val;
	var inputString="";
	var gridrows =gridObj.getChangedRows(",");
	if(gridrows.length > 0){		
		var gridrowsarr = gridrows.toString().split(",");
		for (var rowid = 0; rowid < gridrowsarr.length; rowid++) {
			gridrowid = gridrowsarr[rowid];			
            val = gridObj.cellById(gridrowid, 7).getValue();
            if (val == '')
            {
            	Error_Details(message[5286] +Error_Details_Trans(message[5287],format));
            }else{
            	CommonDateValidation(gridObj.cellById(gridrowid, 7),format,Error_Details_Trans(message[10588],val) + Error_Details_Trans(message[5287],format));
            	validateDT(val,'');
            }
            
			inputString+=gridrowid+'^'+val+'|';
		}		
	}
	else
		return;
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	//alert(inputString);
	if (inputString != ""){
		document.frmLoanerTransfer.inputString.value = inputString;
		document.frmLoanerTransfer.strOpt.value='edit';
		fnStartProgress('Y');
		document.frmLoanerTransfer.submit();
	}
}

//code for date validation
function validateDT(value, msg){
//hide for PMT-16743-using CommonDateValidation 
//	validateDTFormatMMDDYYYY(value, msg);
	var diff = dateDiff(strTodaysDate,value,format);
	if (diff > 0){
		Error_Details(message[10534]);
	}	
}

function validateDTFormatMMDDYYYY(value, msg)
{	
	var validformat=/^\d{2}\/\d{2}\/\d{4}$/; //Basic check for format validity
	var returnval=false;
	if (!validformat.test(value))
	{
		Error_Details(msg);
	}
	else
	{
		if(vCountryCode == 'en'){
			var monthfield=value.split("/")[0];
			var dayfield=value.split("/")[1];
		}else{
			var dayfield=value.split("/")[0];
			var monthfield=value.split("/")[1];
		}
		
		var yearfield=value.split("/")[2];
		var intyearfield = parseInt(yearfield);
		if(intyearfield < 1900)
			Error_Details(msg);
		
		var dayobj = new Date(yearfield, monthfield-1, dayfield);
		if ((dayobj.getMonth()+1!=monthfield)||(dayobj.getDate()!=dayfield)||(dayobj.getFullYear()!=yearfield))
			Error_Details(msg);
		else
			returnval=true;
	}	
}

function fnExport()
{	
	//gridObj.setColumnHidden(1,false);
	//alert(gridObj);
	gridObj.toExcel('/phpapp/excel/generate.php');
}