var gridObj;
var response = '';
//This function used to check SaveFl to identify whether request comes from Re-direct or Left link
function fnOnPageLoad(){
	
	if(strSaveFl == 'Y')
		{
		fnLoad();
		}
}
//Load button using Ajax call to fetch Print Location for WIP sets in response and render with grid
function fnLoad(){
	//Intialize 
	strSaveFl = '';
	//Validate input values
	var warehouseID = document.frmConsignmentLocationReport.warehouseID; 
	if(warehouseID!=undefined && warehouseID!=null)
		{
		warehouseID = warehouseID.value;
		}
	var locationID = document.frmConsignmentLocationReport.locationID;
	if(locationID!=undefined && locationID!=null)
	{
		locationID = locationID.value;
	}
	var cnID = document.frmConsignmentLocationReport.cnID;
	if(cnID!=undefined && cnID!=null)
	{
		cnID = cnID.value;
	}
	var zoneID = document.frmConsignmentLocationReport.zoneID;
	if(zoneID!=undefined && zoneID!=null)
	{
		zoneID = zoneID.value;
	}
	var invTypeID = document.frmConsignmentLocationReport.invTypeID;
	if(invTypeID!=undefined && invTypeID!=null)
	{
		invTypeID = invTypeID.value;
	}
	var statusID = document.frmConsignmentLocationReport.statusID;
	if(statusID!=undefined && statusID!=null)
	{
		statusID = statusID.value;
	}
	var buildingID = document.frmConsignmentLocationReport.buildingID;
	if(buildingID!=undefined && buildingID!=null)
	{
		buildingID = buildingID.value;
	}
	var strAisleNum = document.frmConsignmentLocationReport.aisleNum;
	if(strAisleNum!=undefined && strAisleNum!=null)
	{
		strAisleNum = strAisleNum.value;
	}
	var strShelf = document.frmConsignmentLocationReport.shelf;
	if(strShelf!=undefined && strShelf!=null)
	{
		strShelf = strShelf.value;
	}
	
	fnStartProgress();
	response = '';
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmConsignmentLocationRpt.do?method=fetchConsignmentLocMapDetails&warehouseID='+ warehouseID +'&locationID=' + locationID +'&cnID='+ cnID + 
											'&zoneID=' + zoneID +'&invTypeID='+ invTypeID +'&statusID='+ statusID +'&buildingID='+ buildingID +'&aisleNum='+ strAisleNum +'&shelf='+ strShelf +'&ramdomId=' + new Date().getTime());
	var loader = dhtmlxAjax.getSync(ajaxUrl);
	fnLoadConsignmentLocUsingAjax(loader); 
}

//This function used to check AJAX response and render grid report for WIP set Print location
function fnLoadConsignmentLocUsingAjax(loader){
	response = loader.xmlDoc.responseText;
	fnStopProgress();
	if(response !=''){
		var setInitWidths = "180,150,150,180,150,*";
		var setColAlign = "left,left,left,left,left,left";
		var setColTypes = "ro,ro,ro,ro,ro,ro";
		var setColSorting = "str,str,str,str,str,str";
		var setHeader = ["Location", "Warehouse","Type","Status", "CN ID", "Building"];
		var setFilter = ["#text_filter","#text_filter","#text_filter","#text_filter","#text_filter","#text_filter"];
		var setColIds = "LOCATION_CD,WAREHOUSENM,INVTYPE,STATUS,CONSIGNMENT_ID,BNM";
		var enableTooltips = "true,true,true,true,true,true";
		
		var gridHeight = "";
		var dataHost = {};
		var pagination = "";
		format = "Y"

		document.getElementById("DivNothingMessage").style.display = 'none';
		document.getElementById("dataGridDiv").style.display = "block"; 
		document.getElementById("DivExportExcel").style.display = "table-row";
		document.getElementById("DivData").height = '';
		document.getElementById("DivButton").style.display = "table-row";
		document.getElementById("DivDataRow").style.display = 'none';
		

		// split the functions to avoid multiple parameters passing in single function
		gridObj = initGridObject('dataGridDiv');
		gridObj = fnCustomTypes(gridObj,setHeader, setColIds, setInitWidths, setColAlign, setColTypes,setColSorting, enableTooltips, setFilter,400,pagination,format);
		gridObj = loadDhtmlxGridByJsonData(gridObj, response);
		//
	}else
	{
		document.getElementById("dataGridDiv").style.display = 'none';
		document.getElementById("DivNothingMessage").style.display = 'block';
		document.getElementById("DivData").height = '30';
		document.getElementById("DivExportExcel").style.display = 'none';
		document.getElementById("DivButton").style.display = 'none';
		document.getElementById("DivDataRow").style.display = 'block';
	}
	
}


//This Function Used to set custom types conditions in Grid
function fnCustomTypes(gObj,setHeader, setColIds, setInitWidths, setColAlign, setColTypes,setColSorting, enableTooltips,setFilter,gridHeight,pagination,format){

	 	
	if (setHeader != undefined && setHeader.length > 0) {
	 gObj.setHeader(setHeader);
	 var colCount = setHeader.length;
	 }    

	if (setInitWidths != "")
	gObj.setInitWidths(setInitWidths);

	if (setColAlign != "")
	gObj.setColAlign(setColAlign);

	if (setColTypes != "")
	gObj.setColTypes(setColTypes);

	if (setColSorting != "")
	gObj.setColSorting(setColSorting);

	if (enableTooltips != "")
	gObj.enableTooltips(enableTooltips);

	if (setFilter != "")
	gObj.attachHeader(setFilter);
 

	if(setColIds != ""){
	 gObj.setColumnIds(setColIds);
	}

	if (gridHeight != "")
	gObj.enableAutoHeight(true, gridHeight, true);
	else
	gObj.enableAutoHeight(true, 400, true);

	if(pagination == "Y"){
	    gObj.enablePaging(true,100,10,"pagingArea",true);
	    gObj.setPagingSkin("bricks");
	}	
	
return gObj;
}


//this function used to print location (barcode)
function fnPrint()
{
		fnStartProgress();
		document.frmConsignmentLocationReport.action = "/gmConsignmentLocationRpt.do?method=printInvLocation";
		document.frmConsignmentLocationReport.submit();
}

//This Function Used to download Excel Sheet
function fnExport() {

	gridObj.toExcel('/phpapp/excel/generate.php');

}