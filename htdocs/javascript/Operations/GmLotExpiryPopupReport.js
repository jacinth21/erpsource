function fnLoadPopup(){
//	fnStartProgress();
	fnLoadInvLotDetails();
	fnLoadConsignmentLotDetails();
	fnLoadFieldSalesLotDetails();
	fnLoadLoanerLotDetails();
}
//This function is used to get the inventory details based on partnum, project id and loadfl like expired or 30Dyas.,
function fnLoadInvLotDetails(){
	fnStartProgress();
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmLotExpiryReport.do?method=fetchInventoryLotDetails&strType=Inventory&loadFlg='+strLoadFl+'&partNum='+encodeURIComponent(strPartNumber)+
			'&projectId='+strProjectId+'&strOpt='+strOpt+'&chk_ActiveFl='+strExcQuar+'&ramdomId='+new Date().getTime());
	dhtmlxAjax.get(ajaxUrl,fnOpenLotTrackRpt);
}
//This function is used to get the Filed Sales details based on partnum, project id and loadfl like expired or 30Dyas.,
function fnLoadFieldSalesLotDetails(){
	fnStartProgress();
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmLotExpiryReport.do?method=fetchFieldSalesLotDetails&strType=FieldSales&loadFlg='+strLoadFl+'&partNum='+encodeURIComponent(strPartNumber)+
			'&projectId='+strProjectId+'&strOpt='+strOpt+'&chk_ActiveFl='+strExcQuar+'&ramdomId='+new Date().getTime());
	dhtmlxAjax.get(ajaxUrl,fnOpenLotTrackRpt);
}
//This function is used to get the Consignment set details based on partnum, project id and loadfl like expired or 30Dyas.,
function fnLoadConsignmentLotDetails(){
	fnStartProgress();
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmLotExpiryReport.do?method=fetchConsignmentSetLotDetails&strType=Consignment&loadFlg='+strLoadFl+'&partNum='+encodeURIComponent(strPartNumber)+
			'&projectId='+strProjectId+'&strOpt='+strOpt+'&chk_ActiveFl='+strExcQuar+'&ramdomId='+new Date().getTime());
	dhtmlxAjax.get(ajaxUrl,fnOpenLotTrackRpt);
}
//This function is used to get the loaner set details based on partnum, project id and loadfl like expired or 30Dyas.,
function fnLoadLoanerLotDetails(){
	fnStartProgress();
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmLotExpiryReport.do?method=fetchLoanerSetLotDetails&strType=Loaner&loadFlg='+strLoadFl+'&partNum='+encodeURIComponent(strPartNumber)+
			'&projectId='+strProjectId+'&strOpt='+strOpt+'&chk_ActiveFl='+strExcQuar+'&ramdomId='+new Date().getTime());
	dhtmlxAjax.get(ajaxUrl,fnOpenLotTrackRpt);
}
//This function is used to populate the grid data based on response
function fnOpenLotTrackRpt (loader){
	var response = loader.xmlDoc.responseText;
	var responseSpilt = response.split('^');
	var partNum = '';
	var partDesc = '';
	var plant = '';
	var warehouse = '';
	var qty = '';
	var warehouseId = '';
	var companyid = '';
	var plantid = '';
	var strloadfl = '';
	var fieldsales = '';
	var setid = '';
	if(responseSpilt[0] == '' || responseSpilt[0] == null){
	 var loadfl = responseSpilt[1];
	 	if(loadfl == 'Inventory') {
	 		document.getElementById("loadInventory").innerHTML = '<center>No data to display.</center>';
	 		document.getElementById("excelToInventory").style.display = 'none';
	 	}
	    if(loadfl == 'Consignment') {
	    	document.getElementById("loadConsignmentSet").innerHTML = '<center>No data to display.</center>';
	 		document.getElementById("excelToConsignmentSet").style.display = 'none';
	    }
	    if(loadfl == 'FieldSales') {
	    	document.getElementById("loadFieldSales").innerHTML = '<center>No data to display.</center>';
	 		document.getElementById("excelToFieldSales").style.display = 'none';
	   }
	    if(loadfl == 'Loaner'){
	    	document.getElementById("loadLoanerSet").innerHTML = '<center>No data to display.</center>';
	 		document.getElementById("excelToLoanerSet").style.display = 'none';
	    }  		
	}else{
  //to form the grid string 
	var rows = [], i=0;
	var JSON_Array_obj = "";
	var partcount = 0;

	JSON_Array_obj = JSON.parse(responseSpilt[0]);	
	$.each(JSON_Array_obj, function(index,jsonObject){
		partNum = jsonObject.PARTNUM;
		partDesc = jsonObject.PARTDESC; 
		plant = jsonObject.PLANTNAME;
		warehouseId = jsonObject.WAREHOUSEID;
		warehouse = jsonObject.WAREHOUSETYPE;
		qty = jsonObject.QTY;
		type = jsonObject.TYPE;
		plantid = jsonObject.PLANTID;
		companyid = jsonObject.COMPANYID;
		strloadfl = jsonObject.LOADFL;
		fieldsales = jsonObject.FIELDSALES;
		setid = jsonObject.SETID;
		// default array
        rows[i] = {};
        rows[i].id = i;
        rows[i].data = [];
        rows[i].data[0] = partNum;
        rows[i].data[1] = partDesc;   
        rows[i].data[2] = plant; 
        rows[i].data[3] = warehouse;  
        if(strOpt == 'loadSterile'){
        	 rows[i].data[4] = "<a href=javascript:fnLoadLotTrackReportDtlSterile('"+encodeURIComponent(partNum)+"','"+warehouseId+"','"+strOpt+"','"+plantid+"','"+companyid+"','"+strloadfl+"','"+encodeURIComponent(fieldsales)+"','"+setid+"');>"+qty+"</a>";
        }else{
        	rows[i].data[4] = "<a href=javascript:fnLoadLotTrackReportDtlTissue('"+encodeURIComponent(partNum)+"','"+warehouseId+"','"+strOpt+"','"+plantid+"','"+companyid+"','"+strloadfl+"');>"+qty+"</a>";
        }        	
        i++;
        partcount = partcount+qty;
	});
	var setInitWidths = "";
    var setColAlign = "left,left,left,left,right";
    var setColTypes = "ro,ro,ro,ro,ro";
    var setColSorting = "str,str,str,str,str"; 
    var setHeader = '';
    var setFilter = '';
    if(type == 'Inventory') {
    setHeader = [ "Part#","Part Desc","Plant","Warehouse","Qty"]; 
    setFilter = [ "#text_filter", "#text_filter","#select_filter","#select_filter","#text_filter"]; 
    setInitWidths = "100,200,130,130,*";	
    } if(type == 'FieldSales'){
    setHeader = [ "Part#","Part Desc","Company","Field Sales / Account","Qty"]; 
    setFilter = [ "#text_filter", "#text_filter","#select_filter","#select_filter","#text_filter"]; 
    setInitWidths = "100,200,130,130,*";
    }if(type == 'Consignment' || type == 'Loaner'){
    setHeader = [ "Part#","Set#","Set Name","Plant","Qty"]; 
    setFilter = [ "#text_filter", "#text_filter","#text_filter","#select_filter","#text_filter"]; 
    setInitWidths = "100,130,200,130,*";
    }
    var setColIds = "";
    var enableTooltips = "true,true,true,true,true";  
    var gridHeight = "250px;";
    var footerStyles = [];
    var footerExportFL = true;
    var dataHost = {};
    var footerArry=new Array();	
	footerArry[0]="";
	footerArry[1]="";
	footerArry[2]="";
	footerArry[3]="<b>Total<b>";
	footerArry[4]="<div align='center'><b>"+partcount+"</b></div>";
    dataHost.rows = rows;
    document.getElementById("loadInventory").style.display = 'block';
    document.getElementById("loadConsignmentSet").style.display = 'block';
	document.getElementById("loadFieldSales").style.display = 'block';
	document.getElementById("loadLoanerSet").style.display = 'block';
    if(type == 'Inventory') {
    	gridObjInventory = loadDHTMLXGrid('loadInventory', gridHeight, dataHost, setHeader, setInitWidths, setColAlign, setColTypes, setColSorting, setColIds, enableTooltips, setFilter,footerArry, footerStyles, footerExportFL);
    }
    if(type == 'Consignment') {
    	gridObjConsignment = loadDHTMLXGrid('loadConsignmentSet', gridHeight, dataHost, setHeader, setInitWidths, setColAlign, setColTypes, setColSorting, setColIds, enableTooltips,setFilter, footerArry, footerStyles, footerExportFL);
    }
    if(type == 'FieldSales') {
    	gridObjFieldSales = loadDHTMLXGrid('loadFieldSales', gridHeight, dataHost, setHeader, setInitWidths, setColAlign, setColTypes, setColSorting, setColIds, enableTooltips,setFilter, footerArry, footerStyles, footerExportFL);
    }
    if(type == 'Loaner'){
    	gridObjLoaner = loadDHTMLXGrid('loadLoanerSet', gridHeight, dataHost, setHeader, setInitWidths, setColAlign, setColTypes, setColSorting, setColIds, enableTooltips,setFilter, footerArry, footerStyles, footerExportFL);	
    }  
    }
  fnStopProgress();
}
//this function is used to show lot track report (sterile) screen
function fnLoadLotTrackReportDtlSterile(strPartNumber,strWarehouseId,strOpt,plantid,companyid,strloadfl,strFieldSales,strSetId){

		windowOpener("/gmLotTrackNonTissueRpt.do?method=loadLotTrackNonTissueDetails&strPartNumber="+encodeURIComponent(strPartNumber)+'&warehouseType='+strWarehouseId+'&companyId='+companyid+
				'&plantId='+plantid+'&expDateRange='+strloadfl+'&strFieldSales='+encodeURIComponent(strFieldSales)+'&strSetId='+strSetId,
					"Search","resizable=yes,scrollbars=yes,top=150,left=150,width=1230,height=600");
}
//this function is used to show lot track report (sterile) screen
function fnLoadLotTrackReportDtlTissue(strPartNumber,strWarehouseId,strOpt,plantid,companyid,strloadfl){

		windowOpener("/gmLotTrackReport.do?method=fetchLotTrackingDetails&strOpt=ReLoad&strFl=Y&partNum="+encodeURIComponent(strPartNumber)+'&warehouse='+strWarehouseId+'&companyid='+companyid+
								'&plantid='+plantid+'&expDateRange='+strloadfl,
				"Search","resizable=yes,scrollbars=yes,top=150,left=150,width=1450,height=700");
}
//This function is Export excel
function fnExportExcel(type){
	if(type == 'Inventory'){
		gridObjInventory.toExcel('/phpapp/excel/generate.php');
	}
	if(type == 'Consignment'){
		gridObjConsignment.toExcel('/phpapp/excel/generate.php');
	}
	if(type == 'FieldSales'){
		gridObjFieldSales.toExcel('/phpapp/excel/generate.php');
	}
	if(type == 'Loaner'){
		gridObjLoaner.toExcel('/phpapp/excel/generate.php');
	}
}
