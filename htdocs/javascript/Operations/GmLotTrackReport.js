//This function is used to initiate grid
var gridObj;
var errorVal = false;
function fnOnPageLoad(){
	var sel = document.all.hSearch.value;
	var consignedTo = document.frmLotTrackReport.warehouse.value;
	var strOpt = document.frmLotTrackReport.strOpt.value;
	if (sel != ''){
		document.all.Cbo_Search.value = sel;
	}
	if((consignedTo == 0 || consignedTo == '') && strOpt == ''){// By default show "Inventory Qty(90800)" value in warehouse dropdown
		document.frmLotTrackReport.warehouse.value = 90800;
	}
	fnChangeConsignee();// To get the consignee dropdown on page load
	if(objGridData.value != ''){
		gridObj = initGridData('dataGridDiv',objGridData);
		gridObj.enableBlockSelection(true);
		gridObj.attachEvent("onKeyPress", keyPressed);
	}	
	
}

function initGridData(divRef,gridData){
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.attachHeader("#rspan,#text_filter,#text_filter,#text_filter,#text_filter,#select_filter_strict,#numeric_filter,#select_filter,#text_filter,#text_filter,#numeric_filter,#numeric_filter,#text_filter,#rspan,#text_filter,#select_filter,#select_filter,#text_filter,#text_filter");
	gObj.init();
	gObj.enablePaging(true, 100, 10, "pagingArea", true, "infoArea");
	gObj.setPagingSkin("bricks");
	gObj.enableSmartRendering(true);// To enable rendering
	gObj.loadXMLString(gridData);
	gObj.enableHeaderMenu(); //To enable the header menu while right click inside the Grid
	return gObj;	
}

//When click on Load button, below function 
function fnLoad(){
	var frm = document.frmLotTrackReport;
	// Below function is used to validate the form
	fnValidation(frm);
	
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	
	frm.strOpt.value = 'ReLoad';
	frm.action = "/gmLotTrackReport.do?method=fetchLotTrackingDetails";
	fnStartProgress();
	frm.submit();
		
}

// Function to validate the date field
function fnValidation(frm){
	var partNum = TRIM(frm.partNum.value);
	var ctrlNum = TRIM(frm.ctrlNum.value);
	var donorNum = TRIM(frm.donorNum.value);
	var warehouse = TRIM(frm.warehouse.value);
	var expDateObj = frm.expDate;
	var expDate = TRIM(frm.expDate.value);
	var expDateRange = TRIM(frm.expDateRange.value);
	var status = TRIM(frm.lotType.value);
	if(document.all.Cbo_Search){
		var cboSearch = document.all.Cbo_Search.value;
		if(partNum == '' && cboSearch != '0'){
			Error_Details(message_operations[351]);
			errorVal = true;
		}else{
			errorVal = false;
		}
	}
	if(partNum == '' && ctrlNum == '' && donorNum == '' && warehouse == '0' && expDate == '' && expDateRange == '0' && status == '0' && (frm.internationalUse.checked == false) && errorVal == false){ // Added filter conditions
		Error_Details(message_operations[352]);
	}
	if(expDateRange == '0' && expDate != ''){
		Error_Details(message_operations[353]);
	}else if(expDateRange != '0' && expDate == ''){
		Error_Details(message_operations[354]);
	}else if(expDate != ''){
		CommonDateValidation(expDateObj,format,Error_Details_Trans(message_operations[355],format));
	}
	
	CommonDateValidation(frm.shippedFromDate,format,message[5500]);
	CommonDateValidation(frm.shippedToDate,format,message[5512]);
	var shippedFromDate = TRIM(frm.shippedFromDate.value);
	var shippedToDate = TRIM(frm.shippedToDate.value);
	fromDtDiff = dateDiff(todaysdate,shippedFromDate,format);
	toDtDiff = dateDiff(todaysdate,shippedToDate,format);
	if (fromDtDiff > toDtDiff){
		Error_Details(message[5501]);
	}
}

//Function to export excel
function fnExport()
{	
	gridObj.toExcel('/phpapp/excel/generate.php');
}

//This function is used to activate Load button while pressing the enter key
function fnEnter() {
	if(event.keyCode == 13){
		fnLoad();
	}
}

//to change the Consignee Name dropdown values based on the Warehouse type
function fnChangeConsignee(){
	var consignedTo = document.frmLotTrackReport.warehouse.value;
	
	if(consignedTo == '4000339'){// 4000339: Field sales Qty[warehouse dropdown]
		var objConsigneeBlk = eval('document.all.consignee_blk.style');
  		objConsigneeBlk.display = 'block';
  		var objAccountBlk = eval('document.all.account_blk.style');
  		objAccountBlk.display = 'none';
  		var objChooseOneBlk = eval('document.all.chooseOne_blk.style');
  		objChooseOneBlk.display = 'none';  		
  		var objShippintTr = eval('document.all.shipping_tr.style');
  		objShippintTr.display = 'block';
	}else if(consignedTo == '56002'){// 56002: Account Qty [warehouse dropdown]
		var objConsigneeBlk = eval('document.all.consignee_blk.style');
  		objConsigneeBlk.display = 'none';
  		var objAccountBlk = eval('document.all.account_blk.style');
  		objAccountBlk.display = 'block';
  		var objChooseOneBlk = eval('document.all.chooseOne_blk.style');
  		objChooseOneBlk.display = 'none';
  		var objShippintTr = eval('document.all.shipping_tr.style');
  		objShippintTr.display = 'block';
	}else{
		var objConsigneeBlk = eval('document.all.consignee_blk.style');
  		objConsigneeBlk.display = 'none';
  		var objAccountBlk = eval('document.all.account_blk.style');
  		objAccountBlk.display = 'none';
  		var objChooseOneBlk = eval('document.all.chooseOne_blk.style');
  		objChooseOneBlk.display = 'block';
  		var objShippintTr = eval('document.all.shipping_tr.style');
  		objShippintTr.display = 'none';
  		document.frmLotTrackReport.shippedFromDate.value='';
  		document.frmLotTrackReport.shippedToDate.value='';  		
	}
	fnResetFields(consignedTo); // To reset the dropdown
}

// to reset the fields other than selected fields
function fnResetFields(consignedTo){
		document.frmLotTrackReport.consigneeNm.value = '0';
		if(consignedTo != '4000339'){
  			document.frmLotTrackReport.fieldSalesNm.value = '0';
  		}
  		if(consignedTo != '56002'){
  			document.frmLotTrackReport.accountNm.value = '0';
  		}
}

//Function to forward the control to Lot Number Override Setup Screen
function fnLoadLotOverride(partNum,ctrlNum){
	windowOpener("/gmPartControlSetup.do?companyInfo="+companyInfoObj+"&method=loadPartControlInfo&disableText=Y&hAccess=Y&hScreenType=popup&partNM="+encodeURIComponent(partNum)+'&controlNM='+ctrlNum,"PrntInv","resizable=yes,scrollbars=yes,top=150,left=200,width=750,height=500");
	
}

function fnViewLotReportfrmLotTrack(lotNum){
	windowOpener(" /gmLotCodeRptAction.do?method=loadLotCodeProdInfo&lotNum="+lotNum,"LotCOdeReport","resizable=yes,scrollbars=yes,top=150,left=150,width=1020,height=600");
} 


function keyPressed(code, ctrl) {
    if (code == 67 && ctrl) {       
    	gridObj.setCSVDelimiter("\t");
    	gridObj.copyBlockToClipboard();
    } 
    return true;
}