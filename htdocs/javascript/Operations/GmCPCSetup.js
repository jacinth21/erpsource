//to save/edit the cpc details
function fnSubmit()
{
    fnValidateTxtFld('cpcName',lblCPCName);	
	fnValidateDropDn('activeFl',lblActiveFlag);
	fnValidateTxtFld('txt_LogReason',lblComments);
	
	var vCPCName = document.frmCpcSetup.cpcName.value;
	var vCPCId = document.frmCpcSetup.cpcId.value;
	
	if (vCPCName.indexOf(" ") != -1) {
		Error_Details(message_operations[113]);
	}

	 //var vCPCList = document.frmCPCSetup.alCpcList.value;
		for (var i=0;i<CPCLen;i++)
		{
			arr = eval("alCpcArr"+i);
			arrobj = arr.split(",");
			codeid = arrobj[0];
			codenm = arrobj[1];
			
			if(vCPCName == codenm && (vCPCId == '0' || vCPCId == ''))
			{
				Error_Details(Error_Details_Trans(message_operations[114],vCPCName));
			}				
		}
			
	
	if (ErrorCount > 0)
		{
			Error_Show();
			Error_Clear();
			return false;
		}

		if(document.frmCpcSetup.cpcId.value == '0'){
			document.frmCpcSetup.strOpt.value = "add";
		}
		else{
			document.frmCpcSetup.strOpt.value = "save";
		}
		fnStartProgress('Y');
		document.frmCpcSetup.submit();

}
// to void the CPC 
function fnVoid()
{

	fnValidateDropDn('cpcId',lblCFCNameList);
		if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
		var cpcId = document.frmCpcSetup.cpcId.value;
	  document.frmCpcSetup.action ="/GmCommonCancelServlet?hAction=Load&hCancelType=VDCPC&hTxnId="+cpcId+"&hTxnName="+document.frmCpcSetup.cpcName.value;
	  document.frmCpcSetup.submit();


}
// To load the CPC
function fnLoadCPC()
{	
    if(document.frmCpcSetup.cpcId.value == '0'){
			document.frmCpcSetup.strOpt.value = "edit";
			document.frmCpcSetup.cpcId.value = "0";
			document.frmCpcSetup.cpcName.value = "";
			document.frmCpcSetup.cpcAddr.value = "";
			document.frmCpcSetup.cpcBarcode.value = "";
			document.frmCpcSetup.cpcFinal.value  = "";
			//document.frmCpcSetup.TXT_LOGREASON.value = "";
			document.frmCpcSetup.activeFl.value = "0";
			document.frmCpcSetup.submit();

		} 
    else{
			document.frmCpcSetup.strOpt.value = "edit";
			document.frmCpcSetup.submit();
			
		}
		//document.frmCPCSetup.strOpt.value = "edit";
	   // document.frmCPCSetup.submit();
	
	
}

