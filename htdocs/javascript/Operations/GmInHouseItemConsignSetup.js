function fnSubmit()
{
	document.frmVendor.hAction.value = 'Save';
	fnStartProgress('Y');
  	document.frmVendor.submit();
}

function fnInitiate()
{
	Error_Clear();
	fnValidateDropDn('Cbo_Purpose',message_operations[589]);
	fnValidateDropDn('Cbo_Values',message_operations[590]);
	
	if (ErrorCount > 0)
	{
		Error_Show();
		return false;
	}else{
		document.frmVendor.hAction.value = 'EditLoad';
		fnStartProgress('Y');
	  	document.frmVendor.submit();
  	}
  	
}

function fnAddToCart()
{
	var addCart=document.frmVendor.Cbo_Add_Cart.value;
	Error_Clear();
	if(addCart == '106458'){//106458-by transaction
		var copyTransErr='';
		var Cbo_Type=document.frmVendor.Cbo_Type.value;
		var transactionId = document.frmVendor.Txt_PartNum.value;
		var tempDate = new Date();
		var arrTransId=new Array();
		var arrNewTransId= new Array();
		var hidTransIds=document.frmVendor.hPartNums.value;
		var transErr='';
		
		arrTransId=transactionId.split(',');
		// check duplicate transaction entered in txtbox
		for(var i=0;i<arrTransId.length;i++){
			var cnt=0;
			for(var j=i+1;j<arrTransId.length;j++){
				if(arrTransId[i]==arrTransId[j]){
					cnt++;
				}
			}
				if(cnt>0){// If count greater than zero, duplicate transactions are entered
				transErr+=arrTransId[i]+',';
			}
		}
		
			if(transErr!=''){// If transErr is not empty, throws duplicate transaction validation
				Error_Details(message_operations[743]+transErr.substr(0,transErr.length-1));
			}
		
		//check transaction is already added to cart
		if(hidTransIds!=''){
			arrTransId=transactionId.split(',');
			arrNewTransId=hidTransIds.split(',');
			for(var i=0;i<arrTransId.length;i++){
				for(var k=0;k<arrNewTransId.length;k++){
					if(arrTransId[i]==arrNewTransId[k]){
						copyTransErr+=arrTransId[i]+',';
						break;
					}
				}
			}
		
	}
		
		if(copyTransErr!=''){// copyTransErr is not null, already transaction added to cart validation
			 Error_Details(copyTransErr.substr(0,copyTransErr.length-1)+message_operations[744]);
		} 
		
		if(transactionId.substr(transactionId.length-1,transactionId.length-1) != ','){
			transactionId=transactionId+',';
		}
		 
		//ajax call to check validation 
		var ajaxUrl = fnAjaxURLAppendCmpInfo("/GmConsignItemServlet?hAction=ByTransaction&Src_Trans_Id="+transactionId+"&Trans_Type="+Cbo_Type+"&encode="+tempDate.getTime());
				dhtmlxAjax.get(ajaxUrl,function(loader)
				{
			var response = loader.xmlDoc.responseText;
				if(response == ''){// if response is empty, all transactios are valid
					if (ErrorCount > 0)
					{
						Error_Show();
						return false;
					}
					document.frmVendor.hAddCartType.value=addCart;
					document.frmVendor.hAction.value = "GoCart";
					fnStartProgress('Y');
					document.frmVendor.submit();
				}else{
					var arr=response.split('|');
					if(arr[0]!=''){
						Error_Details(message_operations[745]+arr[0].substr(0,arr[0].length-1));//--- to show invalid transaction validation
					}
					if(arr[1]!=''){
						Error_Details(arr[1].substr(0,arr[1].length-1)+message_operations[746]);//-- to show voided transaction validation 
					}
					if(arr[2]!=''){
						Error_Details(message_operations[747]+arr[2].substr(0,arr[2].length-1));//-- to show not verified transaction validation
					}
					if(arr[3]!=''){
						Error_Details(arr[3].substr(0,arr[3].length-1)+message_operations[748]);//-- to show already transaction copied validation
					}
					
					if (ErrorCount > 0)
					{
						Error_Show();
						return false;
					}
				}
				});			
	}else{
	document.frmVendor.hAddCartType.value=addCart;
	document.frmVendor.hAction.value = "GoCart";
	fnStartProgress('Y');
	document.frmVendor.submit();
	}
}

function fnRemoveItem(val)
{
	document.frmVendor.hDelPartNum.value = val;
	document.frmVendor.hAction.value = "RemoveCart";
	document.frmVendor.submit();	
}

function fnUpdateCart()
{
	document.frmVendor.hAction.value = "UpdateCart";
	fnStartProgress('Y');
	document.frmVendor.submit();	
}

function fnPlaceOrder(val)
{	
	Error_Clear();
	fnValidateDropDn('Cbo_Purpose',message_operations[589]);
	fnValidateDropDn('Cbo_Values',message_operations[590]);

	var txnType = '<%=strTypeRule%>';
	var arr =new Array(); 
	var addCart=document.frmVendor.Cbo_Add_Cart.value;

	if(addCart == '106458'){//106458 - by transaction,to split transaction partnumber keys
		arr=	document.frmVendor.hTransPartNums.value.split(',');
	}else{
		arr=	document.frmVendor.hPartNums.value.split(',');
	}

	var pnum = '';
	var stock = '';
	var qty = '';
	var obj = '';
	var cnum = '';
	var strErrorMore = '';
	var strErrorNegitive = '';
	var strErrorQty = '';
	var j = 0;
    for(var i=0;i<arr.length;i++)
    {
        qid = arr[i];
        j++;
        if(addCart == '106458'){//106458 - by transaction,to split transaction partnumber keys
        	obj = eval("document.frmVendor.hPNum"+j);
        	qid = obj.value;
    	}
		obj = eval("document.frmVendor.Txt_Qty"+j);		
		qty = parseInt(obj.value);
		qty = isNaN(qty)?0:qty;
		var qtyVal = obj.value;
		if(stockFl == 'Y'){// if stock qty is available
			obj = eval("document.frmVendor.hStock"+j);
			stock = parseInt(obj.value);
			
			if (qty > stock)
			{
				strErrorMore  = strErrorMore + ","+ qid;
			}
		}

		if(qty < 0 || isNaN(qtyVal))
		{
			strErrorNegitive = strErrorNegitive +","+ qid; 
		}else if(qty == 0 || qty == ''){
			strErrorQty = strErrorQty +","+ qid; 
		}
	}

	if (strErrorNegitive != '')
	{
		Error_Details(message_operations[147]+strErrorNegitive.substr(1,strErrorNegitive.length));
	}
    if(strErrorQty != ''){
    	Error_Details(message_operations[148]+strErrorQty.substr(1,strErrorQty.length));
    }    
	if (strErrorMore != '' && txnType!='400068' && txnType!='400080' && txnType!='400081' && txnType!='400082' && strSkipVal == 'N')	// Skip validation for TXN -- Inv Adj to Inv,Inv Adj to In-House
	{
		Error_Details(Error_Details_Trans(message_operations[149],val)+strErrorMore.substr(1,strErrorMore.length));
	}
	//to validate number of rows in cart
	if(j>parseInt(rowCnt)){
		Error_Details(message_operations[749]);
	}
	if (ErrorCount > 0)
	{
		Error_Show();
		return false;
	}else{
		document.frmVendor.hAddCartType.value=addCart;
		document.frmVendor.hAction.value = 'PlaceOrder';
		fnStartProgress('Y');
	  	document.frmVendor.submit();
  	}
}

function fnPrintVer()
{
windowOpener("/GmConsignSetServlet?hAction=PrintVersion&hId=<%=strConsignId%>","Con","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnAgree()
{
windowOpener("/GmConsignSetServlet?hId=<%=strConsignId%>&hAction=Ack&","Ack","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");

}

function fnPicSlip()
{
	val = document.frmVendor.hConsignId.value;		
	windowOpener("/GmConsignInHouseServlet?hAction=PicSlip&hConsignId="+val,"SetPic","resizable=yes,scrollbars=yes,top=150,left=150,width=750,height=500");
}

// function calls onchange in add to cart dropdown, to show related labels
function fnAddCartTxt(obj){
	if(obj.value=='106458'){//106458-By Transaction
		document.getElementById("byTransTxtId").style.display="block";
		document.getElementById("byPartTxtId").style.display="none";
	}else{
		document.getElementById("byTransTxtId").style.display="none";
		document.getElementById("byPartTxtId").style.display="block";
	}
}

//-- to prevent auto form submit by scanning barcode and press enter button 
function fnPreventAutoSubmit(){
	if(window.event.keyCode== 13){
		fnAddToCart();
	}
	return !(window.event && window.event.keyCode == 13); 
}