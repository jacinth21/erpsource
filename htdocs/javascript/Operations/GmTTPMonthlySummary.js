function fnReport()
{	
	document.frmTTPMonthlySummary.loadReport.disabled = true;
    if(document.frmTTPMonthlySummary.httpId.value != document.frmTTPMonthlySummary.ttpId.value)
    {
      document.frmTTPMonthlySummary.demandSheetIds.value = "";
    }        
	document.frmTTPMonthlySummary.strOpt.value = "report";
	var ttpnm = document.frmTTPMonthlySummary.searchttpId.value; 
	document.frmTTPMonthlySummary.ttpName.value = ttpnm;
	fnStartProgress('Y');
	document.frmTTPMonthlySummary.submit();
	fnStopProgress();
}

function fnLock()
{	
	document.frmTTPMonthlySummary.lock.disabled = true;
    document.body.style.cursor = 'wait';
    document.frmTTPMonthlySummary.strOpt.value = "lock";
	document.frmTTPMonthlySummary.submit();
}

function fnColumnDrillDown(row,col)
{
	var ttpid = document.frmTTPMonthlySummary.ttpId.value;
	var month = document.frmTTPMonthlySummary.monthId.value;
	var year = document.frmTTPMonthlySummary.yearId.value;
	var overrideTypeValue = 20397;
	var currentValue = '';

	windowOpener("/GmPageControllerServlet?strPgToLoad=gmOverride.do&references="+month+'/'+year+"&overrideTypeId="+overrideTypeValue+"&refValue="+row+"&currentValue="+currentValue+"&refId="+ttpid,"Override","resizable=yes,scrollbars=yes,top=250,left=300,width=750,height=375");
}

function fnCallActual(id, type)
{
	var demandsheetIds = document.frmTTPMonthlySummary.demandSheetIds.value;		
	windowOpener("/gmTTPDrillDownAction.do?demandSheetIds="+demandsheetIds+"&partNumber="+encodeURIComponent(id),"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=910,height=250");
}
				
function fnCallPart(id, type)
{			
    //   alert("Under Construction");
	windowOpener("/GmPartNumServlet?hAction=LoadPartDetail&partNumber="+encodeURIComponent(id),"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=760,height=600");
}

function fnSubmit()
{
	var str = "";
	var partnum;
	if(fnConfirm())
		 {
			var chk_length =  document.frmTTPMonthlySummary.partnumber.length;
			for(var n=0;n<chk_length-1;n++)
			{
				partnum = document.frmTTPMonthlySummary.partnumber[n].value;
				if(partnum.indexOf(".") >= 0 )
					str = str + document.frmTTPMonthlySummary.partnumber[n].value+'^'+document.frmTTPMonthlySummary.ordqty[n].value+'|';						
			}
			document.frmTTPMonthlySummary.strPnumOrd.value = str;
			document.frmTTPMonthlySummary.strOpt.value = 'save';
			document.frmTTPMonthlySummary.submit();
		 }	  
}

function fnConfirm()
 {
   return confirm("Are you sure you want to submit ?");   
 }
