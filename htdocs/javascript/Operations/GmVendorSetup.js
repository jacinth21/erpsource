function fnSubmit()
{
	
	if (document.frmVendor.Chk_ActiveFl.checked)
	{
		document.frmVendor.Chk_ActiveFl.value = 'N';
	}	

	if (document.frmVendor.Chk_Sec199Fl.checked)
	{
		document.frmVendor.Chk_Sec199Fl.value = 'Y';
		fnValidateTxtFld('Txt_percentage',lblSplitPercent);
		var varSplit = document.frmVendor.Txt_percentage.value;
		if (isNaN(varSplit)== true)	
		{	
			Error_Details(message_operations[371]);
		}
		if ( varSplit < 0 || varSplit > 100)	
		{	
			Error_Details(message_operations[372]);
		}
		
	}	
	if (document.frmVendor.Chk_RMinvoiceFl.checked)
	{
		document.frmVendor.Chk_RMinvoiceFl.value = 'Y';
	}
	if (document.frmVendor.Chk_VPAccessFl.checked)
	{
		document.frmVendor.Chk_VPAccessFl.value = 'Y';
	}
	if (document.frmVendor.Chk_AutoPublishFl.checked)
	{
		document.frmVendor.Chk_AutoPublishFl.value = 'Y';
	}
	fnValidateTxtFld('Txt_VendNm',lblVendorName);
	fnValidateTxtFld('Txt_VendShNm',lblShortName);
	fnValidateTxtFld('Txt_LotCode',lblLotCode);
	fnValidateTxtFld('Txt_ContPer',lblContactPerson);
	fnValidateTxtFld('Txt_Email',lblEmail);
	
	fnValidateTxtFld('Txt_Phn',lblPhone);
	fnValidateTxtFld('Txt_Fax',lblFax);
	
	
	fnValidateDropDn('vendor_Type',lblCostType);
	fnValidateDropDn('Cbo_Curr',lblCurrency);
	fnValidateDropDn('Cbo_Status',lblStatus);
	fnValidateTxtFld('Txt_LogReason',lblComments);
	fnValidateDropDn('Cbo_Purchasing_agent',lblPurchasingAgent);/*PMT-50795-->Capture Purchasing Agent - Vendor Setup screen*/
	
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	fnStartProgress();
 	document.frmVendor.vendor_Type.disabled = false; 
	document.frmVendor.submit();
}

function fnReload(val)
{
	if(eval(val)==0){
		document.frmVendor.hAction.value = 'Load';
		document.frmVendor.submit();
	}else{
	document.frmVendor.hId.value = val;
	document.frmVendor.hAction.value = 'EditLoad';
	fnStartProgress('Y');
	document.frmVendor.submit();
	}
}

function fnOpenVendorMap()
{	var flag='';
	val = document.frmVendor.hId.value;
	flag=document.frmVendor.hstrSetupAccessflag.value;
	windowOpener('/GmPageControllerServlet?strPgToLoad=vendorMapping.do&strOpt=load&vendorId='+val+'&strAccessfl='+flag,"PO2","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=400");
}

function fnOpenAddress()
{
	val = document.frmVendor.hId.value;
	windowOpener("/GmPageControllerServlet?strPgToLoad=gmAddressInfo.do&strOpt=7002&partyId="+val,"PO2","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=650");
}
function fnEnableSEC99(){

	var frm = document.frmVendor;
	var val = document.frmVendor.Chk_Sec199Fl.checked;
	var haction=document.frmVendor.hAction.value;
	var dropdown=document.frmVendor.vendor_Type.value; 
	if(val == false){
		frm.Chk_RMinvoiceFl.disabled = true;
		
		frm.Txt_percentage.disabled=true;	
	}else{
		frm.Chk_RMinvoiceFl.disabled = false;
		frm.Txt_percentage.disabled=false;
	}

}

function fnOpenVendorPortalMap(){
	var fromVendorId=document.frmVendor.strSelectedVendorId.value;
	windowOpener('/gmVendorPortalMapping.do?method=fetchVendorPortalMap&fromVendorId='+fromVendorId,"Search","resizable=yes,scrollbars=yes,top=150,left=200,width=735,height=550");
}

//function to enable autopublish checkbox 
function fnAutoPublish(){

	var frm = document.frmVendor;
	var val = document.frmVendor.Chk_VPAccessFl.checked;
	if(val == false){
		frm.Chk_AutoPublishFl.disabled = true;
		document.frmVendor.Chk_AutoPublishFl.checked = false;
	}else{
		frm.Chk_AutoPublishFl.disabled = false	;
	}

}