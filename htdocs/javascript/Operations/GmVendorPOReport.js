var gridObj;
var check_rowId='';
var po_rowId='';
var status_rowId='';
var pub_rowId='';
function fnOnPageLoad()
{  
	var strOpt=document.frmVendorPOReport.strOpt.value;
	if(strOpt=='Load'){
	var footer=new Array();
	footer[0]="";
	footer[1]="";
	footer[2]="";
	footer[3]="";
	footer[4]="";
	footer[5]="";
	footer[6]=message_operations[339];
	footer[7]="";
	footer[8]="<b>{#stat_total}<b>";
	footer[9]="";
	footer[10]="";
	footer[11]="";
	footer[12]="";
	footer[13]="";
	footer[14]="";
	gridObj = initGridWithDistributedParsing('dataGridDiv',objGridData,footer);
	gridObj.setColTypes("ch,ro,ro,ro,ro,ro,ro,ro,ron,ro,ro,ro,ro,ro,ro");
	gridObj.enableTooltips("false,false,true,false,false,false,true,false,false,false,true,false,false,false,false");
	gridObj.attachHeader("<input type='checkbox' value='no'  name='selectAll' style='border:0;' onClick='javascript:fnChangedFilter(this);'/>"
			+',#text_filter,#select_filter,,#select_filter,#text_filter,,,,#text_filter,,,,,');
	gridObj.attachEvent("onFilterStart",fnCustomFilter);
	gridObj.attachEvent("onCheckbox", doOnCheck);
	check_rowId = gridObj.getColIndexById("check");
	po_rowId = gridObj.getColIndexById("po_id");
	status_rowId = gridObj.getColIndexById("sts_fl");
	pub_rowId = gridObj.getColIndexById("pub_fl");
	}
}

//Function is used to Check Box Check
function doOnCheck(rowId, cellInd, stage) {
	rowID = rowId;
	if (cellInd == check_rowId) {
		fnSelectAllToggle('selectAll');
	}
	return true;
}


//Description : This function to check the select all check box
function fnSelectAllToggle(varControl) {
	var objControl = eval("document.frmVendorPOReport." + varControl);

	var all_row = gridObj.getAllRowIds(",");
	var all_rowLen = all_row.length; // get the rows id length
	var checked_row = gridObj.getCheckedRows(check_rowId);
	var finalval;

	finalval = eval(checked_row.length);
	if (all_rowLen == finalval) {
		objControl.checked = true;// select all check box to be checked
	} else {
		objControl.checked = false;// select all check box un checked.
	}
}

//this function used to select all option (check box)
function fnChangedFilter(obj) {
	
	if (obj.checked) {
		gridObj.forEachRow(function(rowId) {
			var check_disable_fl = gridObj.cellById(rowId, check_rowId).isDisabled();
			if (!check_disable_fl) {
				gridObj.cellById(rowId, check_rowId).setChecked(true);
			}
		});
	} else {
		gridObj.forEachRow(function(rowId) {
			check_id = gridObj.cellById(rowId, check_rowId).getValue();
			
			if (check_id == 1) {
				gridObj.cellById(rowId, check_rowId).setChecked(false);
			}

		});
	}
}

function fnGo()
{
	if(document.frmVendorPOReport.potype.value == '0'){
		document.frmVendorPOReport.potype.value = '';
	}
	var objFromDt = document.frmVendorPOReport.frmdt;
	var objToDt = document.frmVendorPOReport.todt;
	var dateRange = dateDiff(objFromDt.value,objToDt.value,gCmpDateFmt);
	if(!isNaN(dateRange) && dateRange < 0){
		 Error_Details(message_operations[304]);
	}
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	document.frmVendorPOReport.action = gStrServletPath+ "/gmVendorPOReport.do?method=fetchVendorPOReport";
	document.frmVendorPOReport.strOpt.value = 'Load';
	fnStartProgress('Y');
	document.frmVendorPOReport.submit();
}

function fnCallPO(po,vid)
{
	document.frmVendorPOReport.action = gStrServletPath+ "/GmPOServlet";	
	document.frmVendorPOReport.hAction.value = 'ViewPO';
	document.frmVendorPOReport.hPOId.value = po;
	document.frmVendorPOReport.hVenId.value = vid;
	fnStartProgress('Y');
	document.frmVendorPOReport.submit();
	//windowOpener("<%=strServletPath%>/GmPOServlet?haction=ViewPO&hpoid="+po+"&hVenId="+vid,"PO","resizable=yes,scrollbar=yes,top=150,left=50,width=750,height=600");
}

function fnCallEdit()
{	
	var obj = document.frmVendorPOReport.cbo_Action;
	var cnt = 0, pubcnt = 0, holdcnt = 0, unholdcnt = 0;
	var pubPO="", holdPO="", unholdPO="", strAlrtMsg="", chk="";
	var checked_row = gridObj.getCheckedRows(check_rowId);
	var inputStr='';
	
	if(checked_row == '')
    { 
		showError(message_operations[340]);
		return;
    }	
	else{
		var chkVal = checked_row.split(",");
		for (var i=0;i<chkVal.length;i++){
			check_id = gridObj.cellById(chkVal[i], check_rowId).getValue();
			poID = gridObj.cellById(chkVal[i], po_rowId).getValue();
			statusVal = gridObj.cellById(chkVal[i], status_rowId).getValue();
			pubFL = gridObj.cellById(chkVal[i], pub_rowId).getValue();
			if (check_id ==1 && pubFL=="Y"){
				if(pubcnt==0)
					pubPO=poID;
				else
					pubPO=pubPO+", "+poID;
				pubcnt++;
			}
			if (check_id ==1 && statusVal=="108364"){
				if(holdcnt==0)
					holdPO=poID;
				else
					holdPO=holdPO+", "+poID;
				holdcnt++;
			}
			if (check_id ==1 && statusVal!="108364"){
				if(unholdcnt==0)
					unholdPO=poID;
				else
					unholdPO=unholdPO+", "+poID;
				unholdcnt++;
			}
			if(check_id ==1){
				if(inputStr=='')
					inputStr = poID;
				else
					inputStr = inputStr + ',' + poID;	
			}
		}
			document.frmVendorPOReport.hpostring.value = inputStr;
	}
	if (obj.value == '0'){
		strAlrtMsg="Please select an action";
		showError(strAlrtMsg);
		return;
	}
	else if (obj.value == '108560')
	{
	if(pubcnt > 0){
		strAlrtMsg=pubPO+" are already Published";
	}
	if(holdcnt>0){
		if(strAlrtMsg !="")
			strAlrtMsg=strAlrtMsg+", "+holdPO+" is in Hold Status";
		else
			strAlrtMsg=holdPO+" is in Hold Status";			
	}
	if(pubcnt > 0 || holdcnt>0){
		showError(strAlrtMsg);
		return;
	}
	document.frmVendorPOReport.hAction.value = 'Publish';
	document.frmVendorPOReport.strOpt.value = 'Load';
	document.frmVendorPOReport.action = gStrServletPath+ "/gmVendorPOReport.do?method=publishMultiplePO";
	}
else if (obj.value == '108561')
{
	if(document.frmVendorPOReport.strHoldFL.value == ""){
		strAlrtMsg="You do not have permission to perform this action";
		showError(strAlrtMsg);
		return;
	}
	else if(holdcnt>0){			
			strAlrtMsg=holdPO+" is already in Hold Status";
			showError(strAlrtMsg);
			return;
	}
	document.frmVendorPOReport.hTxnId.value = document.frmVendorPOReport.hpostring.value
	document.frmVendorPOReport.hCancelType.value = "VDMVHS";
	document.frmVendorPOReport.action = gStrServletPath+ "/GmCommonCancelServlet";
	document.frmVendorPOReport.hAction.value = "Load";
	document.frmVendorPOReport.hOpt.value = "Hold";
}
else if (obj.value == '108562')
{
	if(document.frmVendorPOReport.strHoldFL.value == ""){
		strAlrtMsg="You do not have permission to perform this action";
		showError(strAlrtMsg);
		return;
	}
	else if(unholdcnt>0){			
		strAlrtMsg=unholdPO+" is not currently in Hold status";
		showError(strAlrtMsg);
		return;
	}
	document.frmVendorPOReport.hTxnId.value = document.frmVendorPOReport.hpostring.value
	document.frmVendorPOReport.hCancelType.value = "VDRFHS";
	document.frmVendorPOReport.action = gStrServletPath+ "/GmCommonCancelServlet";
	document.frmVendorPOReport.hAction.value = "Load";
	document.frmVendorPOReport.hOpt.value = "UnHold";
}
fnStartProgress('Y');
document.frmVendorPOReport.submit();
}

function showError(errmsg){
	Error_Details(errmsg);	
	Error_Show();
	Error_Clear();
}

function fnOpenLog(id)
{
	windowOpener("/GmCommonLogServlet?hType=1203&hID="+id,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
}
function fnCustomFilter(ord,data){
	
	/* This custom filter to avoid & content column filter issue. 
	 * Because the actual text for & is '&amp;'
	 * Ex. Let us assume xml as <cell><a ..>B&amp;G Manufacturing Co ,Inc</a></cell>.  If user try to filter 'B&G' text then it wont show . B&;G Manufacturing Co ,Inc*/
			if(data[2]!= ''){
				var original=data[2];
				original = original.toLowerCase();
				data[2]=function(value){
							if (value.toString().replace(/&amp;/g,"&").toLowerCase().indexOf(original)!=-1)
								return true;
							return false;
							};
			}
				
		gridObj.filterBy(ord,data);
		return false;
	}
