var gridObj,gObj;
var cnumid_rowId,ctrl_rowId,repackqty_rowId,fromPart_rowId,toPart_rowId,chk_rowId;
var chk_Control_Number_Id ='';
var ctrlId = '';
var scannedLotStr = '';
var partNumStr = '';
var donorNumStr = '';
var pnumClosebtns = document.getElementsByClassName("part-close-sm");
var donorClosebtns = document.getElementsByClassName("donor-close-sm");
var lotClosebtns = document.getElementsByClassName("lot-close-sm");

function fnOnPageLoad(){
	var frmpart_val;
	// To display the part, lot and donor string back to screen
	fnDisplayScannedVal();
	if(objGridData.value != ''){ 
		gridObj = initGridData('dataGridDiv',objGridData);
	}
	gridObj.attachEvent("onEditCell",doOnCellEdit);
	gridObj.attachEvent("onCheckbox", doOnCheck);
	chk_Control_Number_Id = gridObj.getColIndexById("check");
	toPart_colId = gridObj.getColIndexById("toPartId");
	frmpart_rowId = gridObj.getColIndexById("frompartNumber");
	fromPart_rowId = gridObj.getColIndexById("fromPartId");	
	frmpartNum_rowId = gridObj.getColIndexById("fromPartId");
	var gridrows =gridObj.getAllRowIds(",");
	var gridrowsarr = gridrows.split(",");
	var all_rowLen = (gridrows != "")?gridrowsarr.length:0;
	orginal_row_cnt = all_rowLen;
	ctrlId = gridObj.getColIndexById("ctrlId");
}

function initGridData(divRef,gridData){
	gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.init();
	
	gObj.attachHeader("<input  type='checkbox'  value='no' name='selectAllFiles' onClick='javascript:fnCheckControlNumberFlag(this);'/>,#text_filter,#text_filter,#numeric_filter,#text_filter,#numeric_filter,#text_filter,#text_filter,#text_filter,#text_filter,#numeric_filter,#numeric_filter,#rspan,#rspan");
	gObj.loadXMLString(gridData);
	return gObj;	
}

//Click on load button
function fnLoad(obj){
	var frm = document.frmPartRedesignation;	
	var partNum = frm.partId.value;
	var donorNum = frm.donorId.value;
	var scannedLotNum = '';
	var scannedLotFinStr = (scannedLotStr.lastIndexOf(',') == scannedLotStr.length-1 )
								? scannedLotStr.substring(0,scannedLotStr.length-1)
										: scannedLotStr;
	
	if(frm.controlId != undefined && frm.controlId != null){
		scannedLotNum = TRIM(frm.controlId.value);
		if(scannedLotNum != ''){
			scannedLotNum = getScannedValue(scannedLotNum);
			scannedLotFinStr = scannedLotFinStr == ''
				? scannedLotFinStr + scannedLotNum
					: scannedLotFinStr + ',' +scannedLotNum;
		}
		frm.hScannedLotStr.value = scannedLotFinStr;
	}
	
	if(partNumStr.indexOf(',') == 0){
		frm.hScannedPartStr.value = partNumStr.substring(1,partNumStr.length);
	}else{
		frm.hScannedPartStr.value = partNumStr.substring(0,partNumStr.length);
	}
	if(donorNumStr.indexOf(',') == 0){
		frm.hScannedDonorStr.value = donorNumStr.substring(1,donorNumStr.length);
	}else{
		frm.hScannedDonorStr.value = donorNumStr.substring(0,donorNumStr.length);
	}
	
	if(partNumStr == '' && donorNumStr == '' && scannedLotFinStr == ''){
		Error_Details(message_operations[341]);
	}
	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}
	
	fnStartProgress();	
	frm.strOpt.value = 'Load';
	frm.submit();	
}

// when click on the common check box at the header
function onClickSelectAll(val){
	var checkInd = gObj.getColIndexById("check");
	if(val.checked == true && checkInd == 0 ){
		 gObj.forEachRow( function(id){
			 gObj.cellById(id,checkInd).setChecked(true);			 
			 selectedReqIds += ','+id;
			 selectedRowCnt++;
		 });
	}else{ 
		gObj.forEachRow( function(id){
		 gObj.cellById(id,checkInd).setChecked(false);
		 selectedReqIds = '';
		 selectedRowCnt ='';
	 });
	}
}

// on changing the dropdown values
function doOnCellEdit(stage, rowId, cellInd, nValue, oValue){
	var trnsType = gridObj.cellById(rowId,5).getValue();
	var type = gridObj.getColIndexById("type");
	var fgqty = gridObj.getColIndexById("fgqty");
	/*if(cellInd == '5' && stage == 2){//once the value is selected
		fnApplyType(rowId);
	}
	if(cellInd == '12' && stage == 2){//once the value is selected
		fnApplyAll(rowId);
	}*/
	
	return true;
}

// to reset the form
function fnReset(obj){
	location.href = '/GmPageControllerServlet?strPgToLoad=gmPartredesignation.do?method=fetchPartRedesignation&companyInfo='+companyInfoObj;
}

// when click on submit button
function fnSubmit(obj){	
	var frm = document.frmPartRedesignation;
	//var fromPart = frm.partId.value;
	var cNumId,ctrlNum, qty, toPart, gridrowid;
	var gridrows = gridObj.getCheckedRows(0);
	
	var tempval = '';
	var fromPart = '';
	var inputString = '';
	var ctlNumInvString = '';
	var repackQty = '';
	var err_Qty = '';
    var err_Zero_Qty = '';
    var err_less_repack = '';
    var err_toPart = '';
    var donor_counter='0';
    var donor_row_id='';
    var donor_num_first_checked=''; 
    var donor_num_next_checked='';
    var err_multiple_donors='';
    var stgDtObj = document.frmPartRedesignation.stgDt;
	var stgDate =stgDtObj.value;
	
	fnValidateTxtFld('stgDt',message_operations[802]); //Stage Date cannot be left blank
	
	if (stgDtObj.value!= '' && stgDtObj.value < currdate){ //Stage Date cannot be past date
	     Error_Details(message_operations[803]);
		}
	
	cnumid_rowId = gridObj.getColIndexById("cnumid");
	ctrl_rowId = gridObj.getColIndexById("ctrlId");
	repackqty_rowId = gridObj.getColIndexById("repackqtyId");
	toPart_rowId = gridObj.getColIndexById("toPartId");
	fromPart_rowId = gridObj.getColIndexById("fromPartId");	
	chk_rowId = gridObj.getColIndexById("check");
	var gridrowsarr = gridrows.toString().split(",");
	if(gridrowsarr == ''){
		Error_Details(message_operations[342]);
	}else{
	// Loop through the rows and get the string for selected rows 
		
		gridObj.forEachRow(function(rowId){
		gridrowid = gridrowsarr[rowId];
		if(gridrowid){
			tempval = gridObj.cellById(gridrowid, chk_rowId).getValue(); // Get the checked row
			
			if(tempval == 1){
			
			/*Adding donor counter variable  to avoid selecting multiple donor number per transaction
			PMT-25109 Date: Feb 2019
			Author: gpalani	
			Increment counter variable to just to save the first donor selected in donor_num_first_checked variable.
			In the next iteration the if loop will compare the donor_num_first_checked with donor_num_next_checked variable.
			If there is a difference assign the respective error message.
     		*/	
				donor_counter=donor_counter+1;
				if(donor_counter==1) 
				    { 
					donor_row_id=gridObj.getColIndexById("donorId");
			
					donor_num_first_checked = parseInt(gridObj.cellById(gridrowid, donor_row_id).getValue());	
				    }	
				    donor_row_id=gridObj.getColIndexById("donorId");

				    donor_num_next_checked = parseInt(gridObj.cellById(gridrowid, donor_row_id).getValue());	

					if(donor_num_first_checked!=donor_num_next_checked)
					{
				    	err_multiple_donors ='Multiple Donor Selected'; 
				    }
				
			
				// get the selected value from the grid
				cNumId = gridObj.cellById(gridrowid, cnumid_rowId).getValue();
				ctrlNum = gridObj.cellById(gridrowid, ctrl_rowId).getValue();
				qty = eval("document.frmPartRedesignation.qty" + gridrowid).value;
				repackQty = parseInt(gridObj.cellById(gridrowid, repackqty_rowId).getValue());				
				fromPart = gridObj.cellById(gridrowid, fromPart_rowId).getValue();				
				//toPart = gridObj.cellById(gridrowid, toPart_rowId).getValue();
				toPart = eval("document.frmPartRedesignation.topart" + gridrowid).value;
				var objRegExp = /(^-?\d\d*$)/;
				if(isNaN(qty) || (!qty == '' && !objRegExp.test(qty))){
                    err_Qty = err_Qty +"<br><B>" +ctrlNum+ "</B>"; 
				}
				if (qty <= 0) {
                    err_Zero_Qty = err_Zero_Qty +"<br><B>" +ctrlNum+ "</B>"; 
				}
				if (repackQty < qty) {
                    err_less_repack = err_less_repack +"<br><B>" +ctrlNum+ "</B>"; 
				}
				if(toPart == ''){// If the toPart is empty, pass from part as the to part
					toPart = fromPart;
				}
				/*if(toPart == ''){// User should select To Part
            	   err_toPart = err_toPart +"<br><B>" +ctrlNum+ "</B>"; 
				}else{*/
					inputString = inputString + fromPart + ',' + qty + ','+ ctrlNum + ',' + toPart + ',' + '' + '|';
					ctlNumInvString = ctlNumInvString + cNumId + ',' + qty + '|';
				//}
			}
		}
	});
	}
	
	if(err_Qty != ''){
        Error_Details(Error_Details_Trans(message_operations[343],err_Qty));        
   }
   if(err_Zero_Qty != ''){
        Error_Details(Error_Details_Trans(message_operations[344],err_Zero_Qty));        
   }
   if(err_less_repack != ''){
        Error_Details(Error_Details_Trans(message_operations[345],err_less_repack));        
   }
   /*if(err_toPart != ''){
        Error_Details(Error_Details_Trans(message_operations[346],err_toPart));        
   }*/
   
   if(err_multiple_donors != ''){
	   Error_Details(Error_Details_Trans(message_operations[778],''));
  }
   
   
   if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}else{
		frm.hInputString.value = inputString;
		frm.hctlNumInvString.value = ctlNumInvString;
		frm.strOpt.value = 'save';
		frm.action = '/gmPartredesignation.do?method=savePartRedesignation&companyInfo='+companyInfoObj;
		fnStartProgress();
		frm.Btn_Submit.disabled = true;
		frm.submit();
	}
	
}

function doOnCheck(rowId, cellInd, stage) {
	if(cellInd == chk_Control_Number_Id) {
		fnSelectAll('selectAllFiles');
	}
	return true;
}
// This function to check the select all check box
function fnSelectAll(name){	
	var objControl = eval("document.frmPartRedesignation." + name);
	var checked_row = gridObj.getCheckedRows(chk_Control_Number_Id);
	toPart_rowId = gridObj.getColIndexById("toPartId");
	var finalval;
	var ary_checked_row = checked_row.split(",");
	finalval = eval(ary_checked_row.length);	
	if(orginal_row_cnt == finalval){
		objControl.checked = true;// select all check box to be checked		
	}else {
		objControl.checked = false;// select all check box un checked.
	}
}

//This function used to Check all the check boxs for ShareReqFlag.
function fnCheckControlNumberFlag(obj){
	var shareReqFlVal = '';
	var checkVal = '';
	var checkColumnID = chk_Control_Number_Id;
	gridObj.forEachRow(function(rowId){		
		tempControlNumberid = TRIM(gridObj.cellById(rowId, ctrlId).getValue());
		// If the check box is disabled, do not check it while clicking master check box
		if(tempControlNumberid !== '' && !gridObj.cellById(rowId, checkColumnID).isDisabled()){
			gridObj.cellById(rowId, checkColumnID).setChecked(obj.checked);
		}			
	});
}

// Function to Apply th e Selected To Part for all selected Control Numbers
function fnSetPartNumber(obj){
	var chk = gridObj.getCheckedRows(0).split(",");
	var frmpart_val = "";
	toPart_rowId = gridObj.getColIndexById("toPartId");
	frmpart_rowId = gridObj.getColIndexById("frompartNumber");
	frmpartNum_rowId = gridObj.getColIndexById("fromPartId");
	var objToPart = eval("document.frmPartRedesignation.ToPart"); 
	var objControl = eval("document.frmPartRedesignation.selectAllFiles");
	var tempPartNum  = '';
	var invalidPart = '';
	
	for (var i=0;i<chk.length;i++){
		tempPartNum  = gridObj.cellById(chk[i], toPart_rowId).getValue();	
		if(tempPartNum != '') break;
	}
	if(tempPartNum == '' && obj.checked){
		Error_Details(message_operations[347]);
		objToPart.checked = false;
	}	
	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}else{
		gridObj.forEachRow(function(rowId){
			var checkColumnID = gridObj.cellById(rowId, 0).getValue();	
			if(obj.checked){
				if(checkColumnID == 1){
					//fnCheckApplyPart(rowId,tempPartNum);
					gridObj.cellById(rowId, toPart_rowId).setValue(tempPartNum);
				}
			}else{
				gridObj.cellById(rowId, toPart_rowId).setValue("");
			}
		});
	}
}

// Function to view the Pic Slip of newly initiated PTRD -Transaction
function fnViewPicSlip(){
	windowOpener("/gmPartredesignation.do?method=generatePicSlip&strTxnIds="+transID+"&txntype=103932&ruleSource=103932&refId="+transID+"&companyInfo="+companyInfoObj,"SetPic","resizable=yes,scrollbars=yes,top=150,left=150,width=770,height=500");
}

// Function to apply the selected part for all rows ny editing the ToPart Cell
function fnApplyAll(rowId){
	var chk = gridObj.getCheckedRows(0).split(",");	
	toPart_rowId = gridObj.getColIndexById("toPartId");
	var objToPart = eval("document.frmPartRedesignation.ToPart"); 
	var tempPartNum  = '';	
	tempPartNum  = gridObj.cellById(rowId, toPart_rowId).getValue();	
	if(objToPart.checked){
		gridObj.forEachRow(function(rowId){
			var checkColumnID = gridObj.cellById(rowId, 0).getValue();
			if(objToPart.checked){
				if(checkColumnID == 1){
					gridObj.cellById(rowId, toPart_rowId).setValue(tempPartNum);
				}
			}else{
				gridObj.cellById(rowId, toPart_rowId).setValue("");
			}
		});
	}
}

function fnCheckApplyPart(rowId,PartNum){
	toPart_colId = gridObj.getColIndexById("toPartId");
	var combos = gridObj.getCombo(toPart_colId);	
}



// To validate the to part number on tab out from text box
function fnValidateToPart(obj, rowId){
	var varImgInd = gridObj.getColIndexById("tickRefId");
	var frompartColInd = gridObj.getColIndexById("fromPartId");
	var fromPart = gridObj.cellById(rowId, frompartColInd).getValue();
	var toPart = obj.value;
	var chkid = gridObj.getColIndexById("check");
	if(toPart == ''){
		gridObj.cellById(rowId, chkid).setDisabled(false);
		gridObj.cellById(rowId, varImgInd).setValue('');
		return;
	}
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmPartredesignation.do?method=validateToPart&fromPart='+fromPart+'&toPart='+obj.value+'&ramdomId=' + Math.random());
    dhtmlxAjax.get(ajaxUrl, function(loader){
    	response = loader.xmlDoc.responseText;
    	if(response == 1){
    		gridObj.cellById(rowId, varImgInd).setValue('<img src="/images/success.gif" height="17" width="18" alt="Valid" border="0">');
    		gridObj.cellById(rowId, chkid).setDisabled(false);
    	}else{
    		gridObj.cellById(rowId, chkid).setChecked(false);
    		gridObj.cellById(rowId, varImgInd).setValue('<img src="/images/delete.gif" height="13" width="13" alt="Invalid" border="0">');
    		gridObj.cellById(rowId, chkid).setDisabled(true);
    	}
    });
}

// To add the lot code below the filter on scanning
function fnAddLotCode(obj){
	if(event.keyCode == 13){
		var scanedval = TRIM(obj.value);
		var dataAppend = '';
		var remVal;
		if(scanedval == ''){
			return;
		}
		scanedval = getScannedValue(scanedval);
		
		scannedLotStr = (scannedLotStr.length > 0 && (scannedLotStr.lastIndexOf(',')+1 != scannedLotStr.length) && scannedLotStr.lastIndexOf(',') != -1)
							? scannedLotStr + ','+ scanedval + ','
									: scannedLotStr + scanedval + ',';
		document.getElementById("scannedLotTr").style.display = "block";
		dataAppend = "<li id='currDtls' style=\"list-style-type: none;\"><div>";
		dataAppend = dataAppend + scanedval;
		dataAppend = dataAppend + "&nbsp;&nbsp;&nbsp;<span class='close-sm lot-close-sm' id='closebtn'><a href=\"#\"><span class=\"glyphicon glyphicon-remove\" title='Remove' width='15' height='15'></span></a></span>&nbsp;&nbsp;";
		dataAppend = dataAppend + " </div></li>";
		$("#scannedLotDiv ul").append(dataAppend);
		document.frmPartRedesignation.controlId.value = '';
		document.frmPartRedesignation.controlId.focus();
		
		for (i = 0; i < lotClosebtns.length; i++) {
			lotClosebtns[i].onclick = function(rownum) {
	   			remVal = $(this).parent().parent().text().trim();
	   			$(this).parent().parent().remove();
	   			if(scannedLotStr != ''){
	   				scannedLotStr = (scannedLotStr.indexOf(remVal+',') != -1) 
						? scannedLotStr.replace(remVal+',','')
								: scannedLotStr.replace(remVal,'');
	   			}
	   			
	   			if(scannedLotStr == '' || scannedLotStr.length == 1){
	   				document.getElementById("scannedLotTr").style.display = "none";
	   			}
	   		}
	   	}
		
		if(!e) var e = window.event;
        
		e.cancelBubble = true;
		e.returnValue = false;
      
		if (e.stopPropagation)
		{
			e.stopPropagation();
			e.preventDefault();
		}
	}
}

// To get only the control number from the scanned format
function getScannedValue(scanedval){
	var indexof = '0';
	var cntlindexof = '0';
	
	indexof = scanedval.indexOf("^"); // get the index of '~'
	cntlindexof = scanedval.indexOf("(10)"); // get the index of '(10)'
	
	if(indexof != -1){// Only while scanning, format will be "partno^controlno"
		var cnumarr= scanedval.split("^");
		scanedPart = cnumarr[0]; // get the characters before '^'
		scanedval = cnumarr[1]; //Getting the remaining character, after "^"
	}else if(cntlindexof != -1){// Only while scanning, format will be "(01)udi number(17)exp date(10)controlno"		
		var cnumArray = scanedval.split("(10)");
		scanedval = cnumArray[1];	//Getting the remaining character, after "(10)"		
	}
	scanedval = scanedval.toUpperCase();
	return scanedval;
}

function fnDisplayScannedVal(){
	var lotStr = document.frmPartRedesignation.hScannedLotStr.value;
	var partStr = document.frmPartRedesignation.hScannedPartStr.value;
	var donorStr = document.frmPartRedesignation.hScannedDonorStr.value;
	var lotTempStr = lotStr;
	var lotStrLength = lotStr.length;
	var partTempStr = partStr;
	var donorTempStr = donorStr
	var partStrLength = partStr.length;
	var donorStrLength = donorStr.length;
	var lotId, partId, donorId;
	var dataAppend = '';
	
	if(lotStrLength == 0 && partStrLength == 0 && donorStrLength == 0){
		return;
	}
	// To populate the lot string after Load
	if(lotStrLength != 0){
		document.getElementById("scannedLotTr").style.display = "block";
		while(lotTempStr.indexOf(',') != -1){
			lotId = lotTempStr.substring(0,lotTempStr.indexOf(','));
			lotTempStr = lotTempStr.substring(lotTempStr.indexOf(',')+1,lotTempStr.length);
			
			dataAppend = "<li id='currDtls' style=\"list-style-type: none;\"><div>";
			dataAppend = dataAppend + lotId;
			dataAppend = dataAppend + "&nbsp;&nbsp;&nbsp;<span class='close-sm lot-close-sm' id='closebtn'><a href=\"#\"><span class=\"glyphicon glyphicon-remove\" title='Remove' width='15' height='15'></span></a></span>&nbsp;&nbsp;";
			dataAppend = dataAppend + " </div></li>";
			$("#scannedLotDiv ul").append(dataAppend);
		}
		dataAppend = '';
		dataAppend = dataAppend + "<li id='currDtls' style=\"list-style-type: none;\"><div>";
		
		dataAppend = dataAppend + lotTempStr.substring(0,lotTempStr.length);
		
		dataAppend = dataAppend + "&nbsp;&nbsp;&nbsp;<span class='close-sm lot-close-sm' id='closebtn'><a href=\"#\"><span class=\"glyphicon glyphicon-remove\" title='Remove' width='15' height='15'></span></a></span>&nbsp;&nbsp;";
		dataAppend = dataAppend + " </div></li>";
		$("#scannedLotDiv ul").append(dataAppend);
		for (i = 0; i < lotClosebtns.length; i++) {
			lotClosebtns[i].onclick = function(rownum) {
	   			remVal = $(this).parent().parent().text().trim();
	   			$(this).parent().parent().remove();
	   			if(scannedLotStr != ''){
	   				scannedLotStr = (scannedLotStr.indexOf(remVal+',') != -1) 
						? scannedLotStr.replace(remVal+',','')
								: scannedLotStr.replace(remVal,'');
	   			}
	   			if(scannedLotStr == ''){
	   				document.getElementById("scannedLotTr").style.display = "none";
	   			}
	   		}
	   	}
		scannedLotStr = lotStr;
	}

	// To populate the part string after Load
	if(partStrLength != 0){
		document.getElementById("partTr").style.display = "block";
		while(partTempStr.indexOf(',') != -1){
			partId = partTempStr.substring(0,partTempStr.indexOf(','));
			partTempStr = partTempStr.substring(partTempStr.indexOf(',')+1,partTempStr.length);
			
			dataAppend = "<li id='currPartDtls' style=\"list-style-type: none;\"><div>";
			dataAppend = dataAppend + partId;
			dataAppend = dataAppend + "&nbsp;&nbsp;&nbsp;<span class='close-sm part-close-sm' id='closebtn'><a href=\"#\"><span class=\"glyphicon glyphicon-remove\" title='Remove' width='15' height='15'></span></a></span>&nbsp;&nbsp;";
			dataAppend = dataAppend + " </div></li>";
			$("#partDiv ul").append(dataAppend);
		}
		dataAppend = '';
		dataAppend = dataAppend + "<li id='currPartDtls' style=\"list-style-type: none;\"><div>";
		
		dataAppend = dataAppend + partTempStr.substring(0,partTempStr.length);
		
		dataAppend = dataAppend + "&nbsp;&nbsp;&nbsp;<span class='close-sm part-close-sm' id='closebtn'><a href=\"#\"><span class=\"glyphicon glyphicon-remove\" title='Remove' width='15' height='15'></span></a></span>&nbsp;&nbsp;";
		dataAppend = dataAppend + " </div></li>";
		$("#partDiv ul").append(dataAppend);
		for (i = 0; i < pnumClosebtns.length; i++) {
			pnumClosebtns[i].onclick = function(rownum) {
	   			remVal = $(this).parent().parent().text().trim();
	   			$(this).parent().parent().remove();
	   			if(partNumStr != ''){
	   				partNumStr = (partNumStr.indexOf(','+remVal) != -1) 
	   								? partNumStr.replace(','+remVal,'')
	   										: partNumStr.replace(remVal,'');
	   			}
	   			if(partNumStr == ''){
	   				document.getElementById("partTr").style.display = "none";
	   			}
	   		}
	   	}
		partNumStr = partStr;
	}

	// To populate the donor string after Load
	if(donorStrLength != 0){
		document.getElementById("donorTr").style.display = "block";
		while(donorTempStr.indexOf(',') != -1){
			donorId = donorTempStr.substring(0,donorTempStr.indexOf(','));
			donorTempStr = donorTempStr.substring(donorTempStr.indexOf(',')+1,donorTempStr.length);
			
			dataAppend = "<li id='currDonorDtls' style=\"list-style-type: none;\"><div>";
			dataAppend = dataAppend + donorId;
			dataAppend = dataAppend + "&nbsp;&nbsp;&nbsp;<span class='close-sm donor-close-sm' id='closebtn'><a href=\"#\"><span class=\"glyphicon glyphicon-remove\" title='Remove' width='15' height='15'></span></a></span>&nbsp;&nbsp;";
			dataAppend = dataAppend + " </div></li>";
			$("#donorDiv ul").append(dataAppend);
		}
		dataAppend = '';
		dataAppend = dataAppend + "<li id='currDonorDtls' style=\"list-style-type: none;\"><div>";
		
		dataAppend = dataAppend + donorTempStr.substring(0,donorTempStr.length);
		
		dataAppend = dataAppend + "&nbsp;&nbsp;&nbsp;<span class='close-sm donor-close-sm' id='closebtn'><a href=\"#\"><span class=\"glyphicon glyphicon-remove\" title='Remove' width='15' height='15'></span></a></span>&nbsp;&nbsp;";
		dataAppend = dataAppend + " </div></li>";
		$("#donorDiv ul").append(dataAppend);
		for (i = 0; i < donorClosebtns.length; i++) {
			donorClosebtns[i].onclick = function(rownum) {
	   			remVal = $(this).parent().parent().text().trim();
	   			$(this).parent().parent().remove();
	   			if(donorNumStr != ''){
	   				donorNumStr = (donorNumStr.indexOf(','+remVal) != -1) 
	   						? donorNumStr.replace(','+remVal,'')
	   								: donorNumStr.replace(remVal,'');
	   			}
	   			if(donorNumStr == ''){
	   				document.getElementById("donorTr").style.display = "none";
	   			}
	   		}
	   	}
		donorNumStr = donorStr;
	}
}

// To generate the Part number string
function fnGeneratePartStr(obj){
	var partnum = TRIM(obj.value);
	if(partnum == '') return;
	partNumStr = partNumStr + ',' + partnum;	
	
	document.getElementById("partTr").style.display = "block";
	dataAppend = "<li id='currPartDtls' style=\"list-style-type: none;\"><div>";
	dataAppend = dataAppend + partnum;
	dataAppend = dataAppend + "&nbsp;&nbsp;&nbsp;<span class='close-sm part-close-sm' id='closebtn'><a href=\"#\"><span class=\"glyphicon glyphicon-remove\" title='Remove' width='15' height='15'></span></a></span>&nbsp;&nbsp;";
	dataAppend = dataAppend + " </div></li>";
	$("#partDiv ul").append(dataAppend);
	
	document.frmPartRedesignation.partId.value = '';
	document.frmPartRedesignation.partId.focus();
	
	for (i = 0; i < pnumClosebtns.length; i++) {
		pnumClosebtns[i].onclick = function(rownum) {
   			remVal = $(this).parent().parent().text().trim();
   			$(this).parent().parent().remove();
   			if(partNumStr != ''){
   				partNumStr = (partNumStr.indexOf(','+remVal) != -1) 
								? partNumStr.replace(','+remVal,'')
										: partNumStr.replace(remVal,'');
   			}
   			if(partNumStr == ''){
   				document.getElementById("partTr").style.display = "none";
   			}
   		}
   	}
}

// To generate the Donor number string
function fnGenerateDonorStr(obj){
	var donorNum = TRIM(obj.value);
	if(donorNum == '') return;
	donorNumStr = donorNumStr + ',' + donorNum;
	
	document.getElementById("donorTr").style.display = "block";
	dataAppend = "<li id='currDonorDtls' style=\"list-style-type: none;\"><div>";
	dataAppend = dataAppend + donorNum;
	dataAppend = dataAppend + "&nbsp;&nbsp;&nbsp;<span class='close-sm donor-close-sm' id='closebtn'><a href=\"#\"><span class=\"glyphicon glyphicon-remove\" title='Remove' width='15' height='15'></span></a></span>&nbsp;&nbsp;";
	dataAppend = dataAppend + " </div></li>";
	$("#donorDiv ul").append(dataAppend);
	
	document.frmPartRedesignation.donorId.value = '';
	document.frmPartRedesignation.donorId.focus();
	
	for (i = 0; i < donorClosebtns.length; i++) {
		donorClosebtns[i].onclick = function(rownum) {
   			remVal = $(this).parent().parent().text().trim();
   			$(this).parent().parent().remove();
   			if(donorNumStr != ''){
   				donorNumStr = (donorNumStr.indexOf(','+remVal) != -1) 
   								? donorNumStr.replace(','+remVal,'')
   										: donorNumStr.replace(remVal,'');
   			}
   			if(donorNumStr == ''){
   				document.getElementById("donorTr").style.display = "none";
   			}
   		}
   	}
}