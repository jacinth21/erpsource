/*
 * This function used to not need click a load button
 */
function fnCallGo() {
	if (event.keyCode == 13) {
		var rtVal = fnLoad();
		if (rtVal == false) {
			if (!e)
				var e = window.event;

			e.cancelBubble = true;
			e.returnValue = false;

			if (e.stopPropagation) {
				e.stopPropagation();
				e.preventDefault();
			}
		}
	}
}
/*
 * This function used to load the Consignment Id Details and Priority Details
 */
function fnLoad() {

	var consignmentId = document.frmSetPrioritization.consignmentId.value;	
	if (consignmentId == '') //The Consignment id is empty  throwing valiadtion
	{
		Error_Details(message_operations[761]);
	}
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	if (consignmentId.indexOf("$")>0) //This condition used for when scanining the consignment id showing some special character
	{
		consignmentId = consignmentId.substring(0,consignmentId.indexOf('$'));
	} 
	document.frmSetPrioritization.consignmentId.value = consignmentId;
	document.frmSetPrioritization.strOpt.value = "Reload";
	document.frmSetPrioritization.action = '/gmSetPrioritization.do?method=fetchChangePriority';
	fnStartProgress();
	document.frmSetPrioritization.submit();	
}
/*
 * This function used to update the Consignment Id Details and Priority Details
 */
function fnSubmit() {
    
	var currentPriorityid = document.frmSetPrioritization.currentPriorityid.value;
	var choosePriority = document.frmSetPrioritization.changepriority.value;
	var setId = document.frmSetPrioritization.setid.value;
	if (setId == '') //The Set Id empty that time showing validation
	{
		Error_Details(message_operations[764]);
	}
	if (currentPriorityid == choosePriority) //The current priority same as choose priority that a showing validation
	{

		Error_Details(message_operations[762]);

	}
	if ((choosePriority == '0' ) || ((choosePriority == '103806')&&(currentPriorityid == '')))  //The current priority value choosing choose one or no priority that a showing validation
		{
		
		Error_Details(message_operations[763]);

	}
	fnValidateTxtFld('txt_LogReason', message[5550]);
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	document.frmSetPrioritization.action = '/gmSetPrioritization.do?method=saveChangePriority';
	fnStartProgress();
	document.frmSetPrioritization.submit();
}
/*
 * This function used to focus on the consignment id field
 */
function fnPageLoad() {
	
	document.frmSetPrioritization.consignmentId.focus();
	var strOpt;
	var setId;
	strOpt = document.frmSetPrioritization.strOpt.value;
	setId = document.frmSetPrioritization.setid.value;
	if (strOpt == 'Reload' && setId == '')
		{
		Error_Details(message_operations[764]);
           }

    if (ErrorCount > 0) {
	Error_Show();
	Error_Clear();
	return false;
  }
}
