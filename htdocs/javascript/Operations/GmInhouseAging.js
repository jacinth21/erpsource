function fnOnPageLoad()
{     
  if(objGridData.indexOf("cell", 0)==-1){
	      return true;
    }
 
    var footer = new Array();
	footer[0]=message_operations[146];
	footer[1]="<b>{#stat_total}<b>";
	footer[2]="<b>{#stat_total}<b>";
	footer[3]="<b>{#stat_total}<b>";
	footer[4]="<b>{#stat_total}<b>";
	footer[5]="<b>{#stat_total}<b>";
	gridObj = initGrid('dataGridDiv',objGridData,'',footer);
	gridObj.setColTypes("ro,link,link,link,link,ron");
	gridObj.enableTooltips("false,false,false,false,false,false");
	gridObj.callEvent("onGridReconstructed",[]);  
}

function fnCallItemsShip(strTxnType,strAgingVal)
{
	var strUrl = '/gmOperDashBoardDispatch.do?method=consignedItems&txnType='+strTxnType+'&agingVal='+strAgingVal;
	document.frmInhouseAgingReport.action=strUrl;
	document.frmInhouseAgingReport.submit();
}