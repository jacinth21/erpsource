 var gridObj; // Grid for Donor Details screen
 var gridObj1; //Grid for DHR Split screen
 var ctrl_rowId,txnid_rowId;
 var errorMsgFl = false;
 var scanRowId = 0;
 var serialNum = 1;
 var deleteAdj = '';
 var delrows = 0;
 var totaladdCount = 0;
 
 function onPageLoad(){
	 var mode = document.getElementById("hMode").value;
	 
	 //Should be able to edit the control number till Pending Processing status, once DHR is processed, should not be able to edit the control number
	 if(dhrStatus < 3){ // 3 : Pending Verification;
		 fnPopulateLotNumGrid();
	 }else{
	     if(mode == 'Report'|| mode == 'DHRSplitReport'){  
	    	 document.getElementById("tableData").style.display ="block"; 
	    	
	    	 if(objGridData.indexOf("cell") !='-1'){	 
	    			gridObj1 = initGridData('dataGridDiv', objGridData);
	    			gridObj1.attachEvent("onEditCell",doOnCellEdit);
	    	 }
	    	 
	    	 fnPopulateGrid(); // if data is saved, need to show the details in grid 
	    	 
	     }
	 }
    	 
 	//document.frmDonorInfo.Btn_Submit.disabled = true;
 }
 
//on changing the dropdown values
 function doOnCellEdit(stage, rowId, cellInd, nValue, oValue){
 	if(cellInd == '3' && stage == 2){//once the value is selected
 		fnApplyTxnId(rowId);
 		//document.frmDonorInfo.Btn_Submit.disabled = false;
 	}
 	return true;
 }
 
//To set the values for type and FG qty
 function fnApplyTxnId(rowId){
 	var trnsType = gridObj1.cellById(rowId,3).getValue();
 	for (i=0;i< txntypeLen;i++)
 	{
 		arr = eval("TypeArr"+i);
 		arrobj = arr.split(",");
 		txntypeid = arrobj[0];
 		txnid =  arrobj[1];
 		txnQty =  arrobj[2];
 		if(trnsType == txntypeid){
 			gridObj1.cellById(rowId, 4).setValue(txnid);
 		}else if(trnsType == '0'){
 			gridObj1.cellById(rowId, 4).setValue('');
 		}
 	}
 	
 } 

 //When click on Generate button to generate control numbers
 function fnGenerate(obj){
	 if(event.keyCode == 13 || obj.value == 'Generate'){//Need to work while clicking the button as well as while scanning
		   var preFixObj = document.getElementById("prefix");      
		   var preFix = preFixObj.value;
		   var serialNumObj = document.getElementById("serialNum");       
	       var serialNum = serialNumObj.value;
	       var suffixObj = document.getElementById("suffix");      
	       var suffix = suffixObj.value;
	       var scanCodeObj = document.getElementById("scan");         
	       var scanCode = scanCodeObj.value;
	       // To validate the form
	       var validFl = fnValidateSerialNum(preFix,serialNum,suffix,scanCode);
	       
	       //If there any validation, then no need to process further
	       if(validFl){
	    	   var finalSerialNum = [];
	    	   
	    	   // The Process of generating control number is different if scanning the code
	    	   if(scanCode != ''){
				   var scanCodearr = scanCode.split("^");
				   	 
					if(scanCodearr.length ==2 || scanCodearr.length ==3){
			            		scanCode = scanCodearr[1];
					}
				
	    		   if(fnValidateScan(scanCode)){
						
						var dhrId = document.getElementById("hDhrId").value;
						//the following adjax call used to check if the control number which will be generated already be used for other DHRs
						var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmDonorInfo.do?method=validateCtrlNum&scanCode='+scanCode+'&hDHRId='+dhrId+ '&ramdomId=' + Math.random());
					   dhtmlxAjax.get(ajaxUrl,function(loader){
						var response = loader.xmlDoc.responseText;	 
						if((response != null && response != '') ){
							Error_Details(message_operations[129]+response);
							Error_Show();
							Error_Clear();
							return false;
							}
							else
							{	
								 fnGenerateFromScan(scanCode); // To generate the grid based on the generated serial number[Using Generate button]
							}
						});

	    			  
	    		   }else{
	    			   Error_Details(message_operations[130]);
	    			   document.getElementById("scan").value = '';
	    			   Error_Show();
			           Error_Clear();
			           return false;
	    		   }			 
	    	   }else{
	    		   var serialNumarr = serialNum.split(",");
			       
			       var prefixZero = '';
			       var rangeval1 = '';
			       var rangeval2 = '';  
			       var finalStr = '';
			       var prefix_zerolegth1 = 0;
			       var prefix_zerolegth2 = 0;
			       var rangeValInt1 = '';
			       var rangeValInt2 = '';
			
			       var rangelength = 0;
			       
			       for (var i = 0; i < serialNumarr.length; i++) {
			              
			    	   prefixZero = '';
			           prefix_zerolegth1 = 0;
			           prefix_zerolegth2 = 0;
			
			           rangeArr = serialNumarr[i].split("-");
			           
			           // if there's two '-' in one range, then throw error, ex 80-83-90  
			           if (rangeArr.length > 2){
			        	   Error_Details(message_operations[131]);
			        	   errorMsgFl = true;
			               break; 
			            }
			            else{                             
			            	if(rangeArr.length ==1){
			            		rangeval1 = rangeArr[0];
			                    rangeval2 = rangeArr[0];
			                }
			                else{
			                    rangeval1 = rangeArr[0];
			                    rangeval2 = rangeArr[1];
			                }
			            	
			                // if there's one empty string in the range  , then throw error, ex 80- 
			                if (TRIM(rangeval1) =='' || TRIM(rangeval2) ==''){
			                	Error_Details(message_operations[131]);
			                	errorMsgFl = true;
			                    break; 
			                }
			                                                
			                if (isNaN(rangeval1) || isNaN(rangeval2)){
			                     Error_Details(message_operations[131]);
			                     errorMsgFl = true;
			                     break; 
			                 }
			                                                
			                 rangeValInt1  = parseInt(rangeval1,10);
			                 rangeValInt2  = parseInt(rangeval2,10);
			                 rangelength = rangeValInt2 -rangeValInt1 +1;
			                
			                 // if there's no ',', no '-' in the serial number, then range length default to received qty
			                 if(serialNum.indexOf(",")==-1 && serialNum.indexOf("-")==-1)
			                 {
			                	 rangelength = totlalQty;
			                 }
			
			                 if (rangeval1.length > rangeValInt1.toString().length)
			                 {
			                	 prefix_zerolegth1 = rangeval1.length -  rangeValInt1.toString().length    
			                 }
			
			                 if (rangeval2.length > rangeValInt2.toString().length)
			                 {
			                	 prefix_zerolegth2 = rangeval2.length -  rangeValInt2.toString().length    
			                 }
			
			                 // these two values should be same format , otherwise throw error,  ex, 0072-075
			                 if(rangeValInt1.toString().length > rangeValInt2.toString().length || prefix_zerolegth1 != prefix_zerolegth2) 
			                 {
			                 	Error_Details(message_operations[131]);
			                 	errorMsgFl = true;
			                    break; 
			                 }
			                                         
			                 if (prefix_zerolegth1 != 0)
			                 {
			                	 for (var x = 0; x < prefix_zerolegth1; x++){
			                 		prefixZero = prefixZero + '0';
			                	 } 
			                                                 
			                 }                                               
			              
			                 for (var j = finalSerialNum.length, k = 0; j < finalSerialNum.length + rangelength, k < rangelength; j++, k++) 
			                 {
			                	 finalSerialNum[j] = rangeValInt1 + k ;                        
			                     finalSerialNum[j] = prefixZero + finalSerialNum[j];       
			                 }
			                                                                                        
			            }//end first else
			                     
			       }// end for loop
			
					//if scanned code more than received qty , pop error.
			       if(finalSerialNum.length != totlalQty  && !errorMsgFl)
			       {
			              Error_Details(message_operations[132]);
			       }
			       
			       if (ErrorCount > 0)
			       {
			    	   errorMsgFl = false;
			           Error_Show();
			           Error_Clear();
			           return false;
			       }
					 var dhrId = document.getElementById("hDhrId").value;
					 var  ctrlNumString = '';
						 for(i=0; i<finalSerialNum.length; i++){ 
							 ctrlNumString = ctrlNumString + preFix +finalSerialNum[i] + suffix + ','; // for generating the control number 
						 }	
						//the following adjax call used to check if the control number which will be generated already be used for other DHRs
						dhtmlxAjax.get('/gmDonorInfo.do?method=validateCtrlNum&scanCode='+ctrlNumString+'&hDHRId='+dhrId+ '&ramdomId=' + Math.random(),function(loader){
						var response = loader.xmlDoc.responseText;	 
						if((response != null && response != '') ){
							Error_Details(message_operations[129]+response);
							Error_Show();
							Error_Clear();
							return false;
						}
						else
						{	
							fnGenerateEditGrid(finalSerialNum, preFix, suffix,scanCode); // To generate the grid based on the generated serial number[Using Generate button]
						}
						});

		    	}
		    } 
			
	       if(ErrorCount > 0){
			   Error_Show();
	           Error_Clear();
	           return false;
		   }
	   	   	
	 }  
	 
	 //document.frmDonorInfo.Btn_Submit.disabled = false;
	 
}

 //To validate the input for generating control number
function fnValidateSerialNum(preFix,serialNum,suffix,scanCode){
	
	// We should use either "Assign a patter" field or "Scan here" field to generate control number
	if(((preFix != '' || serialNum !='') && scanCode!='') || ((preFix == '' && serialNum =='' && suffix == '') && scanCode =='')){
		 Error_Details(message_operations[133]);
		 return false;
	 }else if(preFix == '' && serialNum != ''){
		 Error_Details(message_operations[134]);
		 return false;
	 }else if(preFix != '' && serialNum == ''){
		 Error_Details(message_operations[135]);
		 return false;
	 }else if(preFix == '' && serialNum =='' && suffix != ''){
		 Error_Details(message_operations[136]);
		 return false;
	 }
	 return true;
}

//validate if there's duplicate input or scanned
function fnValidateScan(scanCode){
	var ctrlNum;
	var flag = true; 
	
	// Loop through the rows and get the control number to check whether the scanned code is already entered or not
	if(validateObject(gridObj)){
		gridObj.forEachRow(function(rowId){
			// get the control number from the grid
			ctrlNum = gridObj.cellById(rowId, 2).getValue();
			if(scanCode == ctrlNum){
				flag = false;
			}		
		});
	}
	
	return flag;
}
	

//To generate control number if we are using scan id
function fnGenerateFromScan(scanCode){
		var cellValue=new Array();
		var tempNxtSerialNum;
		var tempPrevSerialNum;
		var serialNumDiff;
		var slNum = 0;

		document.getElementById("tableData").style.display ="block"; //To show the grid section
		// Header section should show only the first time we generate grid
		if(scanRowId == '0'){
		     gridObj = new dhtmlXGridObject('dataGrid');            
		     gridObj.init();
		     gridObj.setStyle("background:'#E3EFFF'; font-weight:bold;", "","color:green;", ""); 
		     //hide the delete icon since will use delete button on the screen 
		     var xmlString = "<rows><head><column align='center' type='ro' width='0'> - </column><column align='center' type='ro' width='60'>S#</column>";
		
		     xmlString = xmlString + "<column align='center' type='ro' width='250'>Lot/ Serial Code</column>";
		     xmlString = xmlString + "<column align='left' type='ro' width='188'>Size</column>";
		     xmlString = xmlString + "</head></rows>";
		     gridObj.loadXMLString(xmlString);
	    }
		
	    if(scanRowId+1-delrows >totlalQty){// Should not add the code to grid if the Qty is more than received qty
	    	Error_Details(message_operations[403]);
	    	document.getElementById("scan").value = '';
	    	document.getElementById("scan").focus();
		   	Error_Show();
		    Error_Clear();
	    	return false;
	    }
	    if(scanCode != '' ){// Add row to the grid
	    	cellValue[0]='<a href="#"> <img src="/images/dhtmlxGrid/delete.gif" alt="Remove Row" style="border: none;" onClick="javascript:removeSelectedRow()" height="14"></a>';
			cellValue[1] = scanRowId+1;
			cellValue[2] = scanCode;
			cellValue[3]= '&nbsp;&nbsp;&nbsp;&nbsp;<input type=text name="size'+scanRowId+'" id="size'+scanRowId+'" size="15" tabindex="9" onblur=javascript:changeBgColor(this,\'#ffffff\') onfocus=javascript:changeBgColor(this,\'#AACCE8\')>&nbsp;';
			if(scanRowId == 0){
			  	 cellValue[3] = cellValue[3]+ "<input type='checkbox' name='checkSize' onClick='javascript:fnCheckSize(this);'>";
			}
			gridObj.addRow(++scanRowId,cellValue);
			document.getElementById("scan").value = '';
			document.getElementById("scan").focus();
	    }
	    totaladdCount = totaladdCount + 1;
	    //For setting the serial number in order, even after we delete the rows
	    gridObj.forEachRow(function(rowId) {	
			gridObj.cellById(rowId, 1).setValue(++slNum); 
	   });
	    
	     // used below codes to stop reloading the page after scanning the id
		if(!e) var e = window.event;
	
	     e.cancelBubble = true;
	     e.returnValue = false;	
	
	     if (e.stopPropagation) {
	             e.stopPropagation();
	             e.preventDefault();
	     }
	     
}

//For generating the grid from JS if using "Assign a Pattern" for generating the control number
function fnGenerateEditGrid(finalSerialNum, preFix, suffix, scanCode){
     var cellValue=new Array();
     
	 var temp = '';
	 
	 for(i=0; i<finalSerialNum.length; i++){		 
		 temp  = finalSerialNum[i];
		 for(k=0; k<finalSerialNum.length; k++){
			 if( i!= k)
			 {
				 if (temp  == finalSerialNum[k])
				 {
					 Error_Details(message_operations[130]); 
	    			   Error_Show();
			           Error_Clear();
			           return false;
				 }
			 }
		 }
		 
	 }
     var rowValue;
     scanRowId = 0;// after using "Generate button for generating the control number, if we are scanning a code the grid should generate from first
     document.getElementById("tableData").style.display ="block"; //To show the grid section
     
     gridObj = new dhtmlXGridObject('dataGrid');            
     gridObj.init();
     gridObj.setStyle("background:'#E3EFFF'; font-weight:bold;", "","color:green;", ""); 
     
     var xmlString = "<rows><head><column align='center' type='ro' width='0'> - </column><column align='center' type='ro' width='60'>S#</column>";

     xmlString = xmlString + "<column align='center' type='ro' width='250'>Lot/ Serial Code</column>";
     xmlString = xmlString + "<column align='left' type='ro' width='188'>Size</column>";
     xmlString = xmlString + "</head></rows>";
     gridObj.loadXMLString(xmlString); 
     
     for(i=0; i<finalSerialNum.length; i++){ 
    	 cellValue[0]='<a href="#"> <img src="/images/dhtmlxGrid/delete.gif" alt="Remove Row" style="border: none;" onClick="javascript:removeSelectedRow()" height="14"></a>';
    	 cellValue[1]=i+1;// For generating serial number
         cellValue[2]=preFix +finalSerialNum[i] + suffix; // for generating the control number
         cellValue[3]= '&nbsp;&nbsp;&nbsp;&nbsp;<input type=text name="size'+i+'" id="size'+i+'" size="15" tabindex="9" onblur=javascript:changeBgColor(this,\'#ffffff\') onfocus=javascript:changeBgColor(this,\'#AACCE8\')>&nbsp;';
         if(i == 0){
        	 cellValue[3] = cellValue[3]+ "<input type='checkbox' name='checkSize' onClick='javascript:fnCheckSize(this);'>";
         }
         gridObj.addRow(i+1,cellValue);
        
     }
     
}


function fnClose(obj){
	window.close();
}

//Reset the generated serial number
function fnReset(obj){
	 document.getElementById("tableData").style.display ="none"; //To show the grid section
	 scanRowId = 0;
	 delrows = 0;
	 deleteAdj = '';
	 totaladdCount = 0;
	 gridObj = new dhtmlXGridObject('dataGrid');  
}

// If check box is checked, need to copy the value of text box to others
function fnCheckSize(obj){
	
	var rowId;
	var gridrows =gridObj.getChangedRows(",");
	var gridrowsarr = gridrows.toString().split(",");
	var gridrowsarrLen = gridrowsarr.length;
	var firstRow = gridObj.getRowId(0);
	/* totalRow should be having value of scanRowId if we use Scan Here to generate the control number
		otherwise should be the received qty
	*/
	var totalRow = scanRowId == '0'?totlalQty:scanRowId;
	
	if(obj.checked){
		
			for (var i = 0; i < totalRow; i++) {
				rowId = i+1;
					if(validateObject(document.getElementById("size"+rowId))){ // if size column is availabe, need to set the value to the column
						document.getElementById("size"+rowId).value = document.getElementById("size0").value;
					}
			}
				
	}else{ 
		
		for (var i = 0; i < totalRow; i++) {
			rowId = i+1;
			if(validateObject(document.getElementById("size"+rowId))){//If the size column is available, need to reset the column
				document.getElementById("size"+rowId).value = '';
			}
		}
	}
}

function fnSubmit(obj)
{
	if(screenType == 'DHRDetail'){
		fnSubmitDHRDetails(obj); //While submitting the form from Update DHR screen
	}else{
		fnSubmitControlDtls(obj);//While submitting the form from Receive shipment screen
	}
		
}

// For submitting the form
function fnSubmitControlDtls(obj){
	gridObj.editStop();// retun value from editor(if presents) to cell and closes editor
	var gridrowid;
	var inputString='';
	var serialNum;
	var gridrows =gridObj.getChangedRows(",");
	/* totalRow should be having value of scanRowId if we use Scan Here to generate the control number
		otherwise should be the received qty
	 */
	var totalRow = scanRowId == '0'?totlalQty:scanRowId;
	var rowId;
	var firstRow = gridObj.getRowId(0);// To get the fist row
	var ctrlnm,size;
	if(gridrows.length==0){
		inputString = "";
	}
	else
	{
		var gridrowsarr = gridrows.toString().split(",");
		var gridrowsarrLen = gridrowsarr.length;
		//Generated control number should be equal to the received qty
		if(gridrowsarrLen != totlalQty){
			Error_Details(message_operations[132]);
		}
		var columnCount = gridObj.getColumnsNum();
		
		var i=0;
		for (var rowid = 0; rowid < gridrowsarrLen; rowid++) {
			gridrowid = gridrowsarr[rowid];
			while(i<totalRow){
				if(validateObject(document.getElementById("size"+i))){
					ctrlnm = gridObj.cellById(gridrowid,2).getValue();
					size = document.getElementById("size"+i).value;
					i++;
					break;
				}else{
					i++;
				}
				
			}
			
			inputString+= ctrlnm + '^' + size + '|';
			
		}
	
	}
	 if (ErrorCount > 0)
     {
         Error_Show();
         Error_Clear();
         return false;
     }
	document.frmDonorInfo.strOpt.value = 'save';
	document.frmDonorInfo.hInputString.value = inputString;
	document.frmDonorInfo.action = '/gmDonorInfo.do?method=saveDonorInfo';
	if(validateObject(window.opener.document.all.Btn_Regenerate)){// Once click on submit button need to disable the "Regenerate Lot #" button in parent form
		window.opener.document.all.Btn_Regenerate.disabled = true;
	}
	fnStartProgress('Y');
	document.frmDonorInfo.submit();
}

//While submitting the form from Receive shipment screen
function fnSubmitDHRDetails(obj){
	var ctrlNum, txnid;
	var inputString = '';
	
	fnValidateTxnTypeQty(); //For validating the count of transactions
	
	ctrl_rowId = gridObj1.getColIndexById("ctrlId");
	txnid_rowId = gridObj1.getColIndexById("txnId");
	
	// Loop through the rows and get the string for selected rows 
	gridObj1.forEachRow(function(rowId){
		
		// get the selected value from the grid
		ctrlNum = gridObj1.cellById(rowId, ctrl_rowId).getValue();
		txnid = gridObj1.cellById(rowId, txnid_rowId).getValue();
				
		inputString = inputString + ctrlNum + '^' + txnid + '|';
	});
	
	if (ErrorCount > 0)
    {
		ErrCnt = 0;
        Error_Show();
        Error_Clear();
        return false;
    }else{
		document.frmDonorInfo.strOpt.value = 'save';
		document.frmDonorInfo.hInputString.value = inputString;
		document.frmDonorInfo.action = '/gmDonorInfo.do?method=saveDHRSplit';
		/*if(validateObject(window.opener.document.all.Btn_AssignLotNum)){// Once click on submit button need to disable the "Regenerate Lot #" button in parent form
			window.opener.document.all.Btn_AssignLotNum.disabled = true;
		}*/
		fnStartProgress('Y');
		document.frmDonorInfo.submit();
    }
}

//For validating the total count and selected transaction type
function fnValidateTxnTypeQty(){
	
	var trnsType;
	var count;
	var ErrCnt = 0;
	var totalCount=0;
	 	for (i=0;i< txntypeLen;i++)
	 	{
	 		count = 0;
	 		arr = eval("TypeArr"+i);
	 		arrobj = arr.split(",");
	 		txntypeid = arrobj[0];
	 		txnQty =  arrobj[2]; 
	 		gridObj1.forEachRow(function(rowId){
	 			trnsType = gridObj1.cellById(rowId,3).getValue(); 
		 			if(trnsType == txntypeid){
			 			count++;
			 			totalCount++;
			 		}
	 		});
	 		if(count>txnQty){
	 			Error_Details(message_operations[137]);
	 			ErrCnt++
	 			break;
		 	}
	 	}
	 	if( ErrCnt == '0' && totalCount != packQty){
	 		Error_Details(message_operations[138]);
	 	}
}

//Description : This function to load the grid data

function initGridData(divRef,gridData)
{
	var mode = document.getElementById("hMode").value;
	gridObj1 = new dhtmlXGridObject(divRef);
	gridObj1.setSkin("dhx_skyblue");
	gridObj1.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gridObj1.init();
	if((mode == 'Report' || mode == 'DHRSplitReport') && screenType == 'DHRDetail'){// Check box need to show only if the screen is loading from "Update Device History Record" screen
		gridObj1.attachHeader(",,,<input  type='checkbox' value='no' name='selectAllFiles' onClick='javascript:fnSelectAllTxnType(this); '/></span>,");
	}
	gridObj1.loadXMLString(gridData);	
	return 	gridObj1;	
}

 
//When select master check box, it will copy the value of first dropdown to other dropdowns.
function fnSelectAllTxnType(obj){
		trnsType = gridObj1.cellById(1,3).getValue();
		trnsid = gridObj1.cellById(1,4).getValue();
		if(obj.checked){
			gridObj1.forEachRow(function(rowId) {	 
				gridObj1.cellById(rowId, 3).setValue(trnsType); 
				for (i=0;i< txntypeLen;i++)
			 	{ 
			 			gridObj1.cellById(rowId, 4).setValue(trnsid);
			 		 
			 	}
				 
		   });
		}else{
			gridObj1.forEachRow(function(rowId) {	 
				gridObj1.cellById(rowId, 3).setValue(''); 
				for (i=0;i< txntypeLen;i++)
			 	{ 
			 			gridObj1.cellById(rowId, 4).setValue('');
			 		 
			 	}
				 
		   });
		}
	 
} 

// if data is saved, need to show the details in grid 
function fnPopulateGrid(){
	var txntype, txnId;
	for (i=0;i< dhrSplitLen;i++)
 	{
 		arr = eval("SplitArr"+i);
 		arrobj = arr.split(",");
 		txntype = arrobj[0];
 		txnId =  arrobj[1];
 		if(txntype != ''){
 			gridObj1.cellById(i+1,3).setValue(txntype);
 		}
 		gridObj1.cellById(i+1,4).setValue(txnId);
 	}	 
}

// if data is save, need to reload the Saved Control Number section
function fnPopulateLotNumGrid(){
	
	var ctrlNum, partSize;
	var cellValue=new Array();
	
	if(ctrlListLen == 0){//If there is no control number saved, no need to populate the grid
		return;
	}
	
	document.getElementById("tableData").style.display ="block";
	
	 gridObj = new dhtmlXGridObject('dataGrid');            
     gridObj.init();
     gridObj.setStyle("background:'#E3EFFF'; font-weight:bold;", "","color:green;", ""); 
     
     var xmlString = "<rows><head><column align='center' type='ro' width='0'> - </column><column align='center' type='ro' width='60'>S#</column>";

     xmlString = xmlString + "<column align='center' type='ro' width='250'>Lot/ Serial Code</column>";
     xmlString = xmlString + "<column align='left' type='ro' width='188'>Size</column>";
     xmlString = xmlString + "</head></rows>";
     gridObj.loadXMLString(xmlString); 
	
	//Populate the grid with saved values
	for (i=0;i< ctrlListLen;i++)
 	{
 		arr = eval("CtrlArr"+i);
 		arrobj = arr.split(",");
 		ctrlNum = arrobj[0];
 		partSize =  arrobj[1];
 	     
    	 cellValue[0]='<a href="#"> <img src="/images/dhtmlxGrid/delete.gif" alt="Remove Row" style="border: none;" onClick="javascript:removeSelectedRow()" height="14"></a>';
    	 cellValue[1]=i+1;// For generating serial number
         cellValue[2]=ctrlNum; // for generating the control number
         cellValue[3]= '&nbsp;&nbsp;&nbsp;&nbsp;<input type=text name="size'+i+'" id="size'+i+'" size="15" tabindex="9" onblur=javascript:changeBgColor(this,\'#ffffff\') onfocus=javascript:changeBgColor(this,\'#AACCE8\')>&nbsp;';
         
         if(i == 0){
        	 cellValue[3] = cellValue[3]+ "<input type='checkbox' name='checkSize' onClick='javascript:fnCheckSize(this);'>";
         }
         gridObj.addRow(i+1,cellValue);
         document.getElementById("size"+i).value = partSize;
 
 	}	
	
	scanRowId = gridObj.getRowsNum(); //set the no. of rows in grid to scanRowId, for adding more rows/validations
}


//this function is used to remove the selected rows
function removeSelectedRow(){
	
	var gridrow=gridObj.getSelectedRowId();
	var firstRow,secondRow;
	var colStr ;
	var slNum = 0;
	var sizeVal;
	if(gridrow!=undefined){
			firstRow = gridObj.getRowId(0);// To get the fist row
			secondRow = gridObj.getRowId(1);// To get the fist row
			/*if(firstRow != secondRow){
				sizeVal = document.getElementById("size"+(gridrow)).value;
			}*/
			gridObj.deleteRow(gridrow);
			deleteAdj = deleteAdj + gridrow + ','; 
			delrows = delrows +1;
			gridObj.forEachRow(function(rowId) {	 
				gridObj.cellById(rowId, 1).setValue(++slNum); 
		   });
			//if last one row be deleted ,need reset  
			if ((totaladdCount - delrows) == 0)
			{	
				fnReset(this);
			}
			if(gridrow == firstRow){//Need to add checkbox to the next row if the 1st row is deleted
				gridrow++;
				var tem = gridObj.cellById(secondRow, 1).getValue() - 1;			 
				sizeVal = document.getElementById("size"+(tem+delrows)).value;					 
					
				colStr = '&nbsp;&nbsp;&nbsp;&nbsp;<input type=text value="' +sizeVal+ '"name="size'+tem+'" id="size'+tem+'" size="15" tabindex="9" onblur=javascript:changeBgColor(this,\'#ffffff\') onfocus=javascript:changeBgColor(this,\'#AACCE8\')>&nbsp;';
		        colStr = colStr + "<input type='checkbox' name='checkSize' onClick='javascript:fnCheckSize(this);'>"; 
		        gridObj.cellById(secondRow, 3).setValue(colStr);
		        /*if(firstRow != secondRow)
		        	document.getElementById("size"+tem).value = sizeVal;*/
			}	
			
	}else{
		Error_Clear();
		Error_Details(message_operations[8]);				
		Error_Show();
		Error_Clear();
	}
}

function fnParamSubmit(obj){
	document.frmDonorInfo.action = '/gmDonorInfo.do?method=saveDonorParamDtls&FORMNAME=frmDonorInfo';
	fnStartProgress('Y');
	document.frmDonorInfo.submit();
}

var lotCodeGridObj;
function onLotCodePageLoad(){
	if(objGridData.value != ''){
		lotCodeGridObj = initLotCodeGridData('dataGrid',objGridData);
	}
}

function initLotCodeGridData(divRef,gridData){
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.init();
	gObj.loadXMLString(gridData);	
	return gObj;	
}

// To save the status corresponding to the control number
function fnSubmitStatus(obj){
	var apprRejFl = false;
	var inputString = '';
	var rejQty = document.all.rejQty.value;
	var rejCnt = 0;
	fnStartProgress();
	
	// Loop through the rows and get the string for selected rows 
	lotCodeGridObj.forEachRow(function(rowId){
		
		// get the selected value from the grid
		ctrlNum = lotCodeGridObj.cellById(rowId, 1).getValue();
		apprRej = lotCodeGridObj.cellById(rowId, 2).getValue();
		if(apprRej == '0'){ //0: Choose One
			apprRejFl = true;
		}
		if(apprRej == '1401'){// Rejected control number should not be greater than rejected qty; 1401: Reject
			rejCnt++;
		}
				 
		inputString = inputString + ctrlNum + '^' + apprRej  + '|';
	});
	
	if(apprRejFl){
		Error_Details(message_operations[139]);
	}
	
	if(rejCnt > rejQty){
		Error_Details(message_operations[140]);
	}
	
	if (ErrorCount > 0)
	{
		Error_Show();
		fnStopProgress();
		Error_Clear();
		return false;
	}
	
	document.frmDonorInfo.hInputString.value = inputString;
	document.frmDonorInfo.action = '/gmDonorInfo.do?method=saveLotCodeStatus';
	document.frmDonorInfo.submit();
}
