function fnCallDHR(id,vid)
{
	document.frmVendor.hAction.value = 'ViewDHR';
	document.frmVendor.hDHRId.value = id;
	document.frmVendor.hVenId.value = vid;
  	document.frmVendor.submit();
}

function fnGo()
{	 
	objStartDt = document.frmVendor.Txt_FromDate;
	objEndDt = document.frmVendor.Txt_ToDate;
	CommonDateValidation(objStartDt, gCmpDateFmt,Error_Details_Trans(message_operations[115],gCmpDateFmt));
	CommonDateValidation(objEndDt, gCmpDateFmt,Error_Details_Trans(message_operations[116],gCmpDateFmt));
	var dateRange = dateDiff(objStartDt.value,objEndDt.value,gCmpDateFmt);
	if(!isNaN(dateRange) && dateRange < 0){
		 Error_Details(message_operations[117]);
	}
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}	
	else{ 
		document.frmVendor.action = gStrServletPath + "/GmDHRReportServlet?companyInfo="+companyInfoObj;
		
		document.frmVendor.hAction.value = 'Go';
		document.frmVendor.hOpt.value = gStrOpt;
		fnStartProgress('Y');
		document.frmVendor.submit();
	}
}

function Toggle(val)
{
	
	var obj = eval("document.all.div"+val);
	var trobj = eval("document.all.tr"+val);
	var tabobj = eval("document.all.tab"+val);

	if (obj.style.display == 'none')
	{
		obj.style.display = '';
		trobj.className= "ShadeRightTableCaptionBlue";
		tabobj.style.background="#ecf6fe";
	}
	else
	{
		obj.style.display = 'none';
		trobj.className= "";
		tabobj.style.background="#ffffff";
	}
}

//To open the popup when click on "D" icon
function fnCallDonor(dhrId, vendorId){
	windowOpener("/gmDonorInfo.do?companyInfo="+companyInfoObj+"&method=fetchDonorInfo&strOpt=load&hDhrId="+dhrId+"&hMode=Report&hVendorId="+vendorId,"PrntInv","resizable=yes,scrollbars=yes,top=200,left=300,width=585,height=700");
}