//When click on Load button, below function 
function fnLoad(obj){
	if(event.keyCode == 13 || obj.value == 'Load'){
		var frm = document.frmFGBINTrans;
		fnValidateScanId(obj.value); //to validate Scan id
		if (ErrorCount > 0) {
			document.frmFGBINTrans.scanId.focus();
			Error_Show();
			Error_Clear();
			return false;
		}else{ 
			frm.action = "/gmFGBINTrans.do?method=fetchFGBINTransactions";
			fnStartProgress();
			frm.submit();
		}
		
	}
}

//This function is used to initiate grid
var gridObj;
function fnOnPageLoad()
{ 
	document.frmFGBINTrans.scanId.focus();
	if(objGridData.value != ''){ 
		gridObj = initGridData('dataGridDiv',objGridData);
	}
	
}

function initGridData(divRef,gridData){
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.init();
	gObj.loadXMLString(gridData);	
	return gObj;	
}

// To validate Scan id
function fnValidateScanId(obj){
	var fgbinid = TRIM(document.frmFGBINTrans.scanId.value);
	var indexof = fgbinid.indexOf("~"); // get the index of '~'
	var type = fgbinid.substring(0, indexof); // get the characters before '~'
	var scanid = fgbinid.substring(indexof+1,fgbinid.length);
	
	if(fgbinid == ""){
		Error_Details(message_operations[141]);
	}else{
		if(obj != 'Load'){// Only needed while scanning, no need for load button
			document.frmFGBINTrans.scanId.value = scanid;
			if(type != '100'){ // While scanning the FG Bin id will be something like '100~T23456', using '100' we can validate it 
				Error_Details(message_operations[142]);
			}
			
			// used below codes to stop reloading the page after scanning the id
			if(!e) var e = window.event;

	        e.cancelBubble = true;
	        e.returnValue = false;	

	        if (e.stopPropagation) {
	                e.stopPropagation();
	                e.preventDefault();
	        }
		}
	}
	
}