
function initGridData(divRef,gridData){
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);	
	gObj.init();	
	gObj.loadXMLString(gridData);	
	return gObj;	
}
function fnOnPageLoad(){
	if (objGridData != ''){
		gridObj = initGridData('employeedata',objGridData);
		gridObj.enableTooltips("true,true,true,true,true,true");
	}
}

function fnSubmit(){
		var frm = document.getElementsByName('frmSetMapping')[0];
		fnValidateDropDown(frm, 'loanersetid',message_operations[176]);
		fnValidateDropDown(frm, 'consignsetid',message_operations[177]);
		if (ErrorCount > 0) {
			Error_Show();
	        Error_Clear();
	        return false;
	    }
		frm.strOpt.value = 'save';
		fnStartProgress('Y');
		frm.submit();
	}

	function fnReset(){
		
		var frm = document.getElementsByName('frmSetMapping')[0];
		frm.loanersetid.value = 0;
		frm.consignsetid.value = 0;
		frm.strOpt.value = 'load';
		frm.submit();
	}
	
	function fnValidateDropDown(form, val, name) {
		var obj = eval("document." + form.name + "." + val);
		var objVal = TRIM(obj.value);
		if(obj.disabled != true && objVal == '0') {
			Error_Details(Error_Details_Trans(message_operations[178],name));
		}
	}
	
	function fnEditLoanerSetId(loanerSetId,consignSetId){
		var frm = document.frmSetMapping;
		frm.loanersetid.value = loanerSetId;
		frm.consignsetid.value = consignSetId;
		frm.strOpt.value = 'editload';
		frm.submit();
	}



