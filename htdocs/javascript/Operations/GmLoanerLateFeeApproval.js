var gridObj;
var fieldSales = "";   
var salesRep = ""; 
var status = ""; 
var requestID = ""; 
var fromDate = ""; 
var toDate = ""; 
var chargeDetailId = ""; 
var statusDrp = "";
var check_rowId = "";
 
function fnOnPageLoad() {
    document.frmLateFeeCharges.fromDate.value = strFromdaysDate;
	document.frmLateFeeCharges.toDate.value= strTodaysDate;
	arr = document.all.Chk_GrpDist;
	fnLoadDist(arr);
	
	var arrList = document.all.Chk_GrpZone;
    var filterCookie= document.all.hZoneGrp.value;
    fnCheckBoxSelections(filterCookie,arrList);   
  
    arrList = document.all.Chk_GrpRegn;   
    filterCookie=document.all.hRegionGrp.value;
    if(zone !=''){   
    fnLoadRegn(zone.split(","));
    }
    fnCheckBoxSelections(filterCookie,arrList);
}

function fnGo(){
	requestID = document.frmLateFeeCharges.requestID.value;
	fieldSales = document.frmLateFeeCharges.fieldSales.value;   
	salesRep = document.frmLateFeeCharges.salesRep.value; 
	status = document.frmLateFeeCharges.status.value;
	fromDate = document.frmLateFeeCharges.fromDate.value;
	toDate = document.frmLateFeeCharges.toDate.value;
	var fromDateObj =document.frmLateFeeCharges.fromDate;
	var toDateObj = document.frmLateFeeCharges.toDate;
	fromDate = fromDateObj.value;
	toDate = toDateObj.value;
	var result = '';
	fieldSalesNm = document.frmLateFeeCharges.searchfieldSales.value;
	salesRepNm = document.frmLateFeeCharges.searchsalesRep.value;
	
	var strZone = fnGetCheckedValues(document.all.Chk_GrpZone);
	var strRegion = fnGetCheckedValues(document.all.Chk_GrpRegn);
	var strDivision = fnGetCheckedValues(document.all.Chk_GrpDist);
	
	//this condition is used to remove Rep code id if code name not exist
	if(salesRepNm == '' && salesRep != '')
	{	
		salesRep = '';
	}
	//this condition is used to remove Distributor code id if code name not exist
	if(fieldSalesNm == '' && fieldSales != '')
	{	
		fieldSales = '';
	}
	//this condition used to fetch record based on distributor name
	if(fieldSales == '' && fieldSalesNm.length > 0)
	{
		searchFieldSalesNm = fieldSalesNm;

	}
	else
	{
		searchFieldSalesNm = '';
	}

	//this condition used to fetch record based on salesrep name
	if(salesRep == '' && salesRepNm.length > 0)
	{
		searchSalesRepNm = salesRepNm;

	}
	else
	{
		searchSalesRepNm = '';
	}
	
	if(fromDate == '' && toDate == '' && fieldSales =='' && salesRep == '' && status =='0' && requestID =='' && searchSalesRepNm == '' && searchFieldSalesNm == '' && strZone == '' && strRegion == '' && strDivision == ''){
		Error_Details('Input or selection is required in at least one field');
	}else{
		var dateDiffs = dateDiff( toDate,fromDate, format);

		// To check whether the date is in valid format
		if(fromDate != ""){
			CommonDateValidation(fromDateObj,format,Error_Details_Trans(message[10002],format));
		}
		if(toDate != ""){
			CommonDateValidation(toDateObj,format,Error_Details_Trans(message[10002],format));
		}

		if(fromDate != '' && toDate == ''){
			Error_Details(message[10034]);
		}else if(fromDate == '' && toDate != '' ){
			Error_Details(message[10035]);
		}else if (fromDate != '' && toDate != '' && dateDiffs>0){
			Error_Details(message[10036]);
		}
		if (ErrorCount > 0)
		{
			Error_Show();
			Error_Clear();
			return false;
		}
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmLoanerLateFeeAction.do?method=FetchLateFeeCharges&fieldSales='
				+ fieldSales+'&salesRep='+salesRep+'&requestID='+requestID+'&status='+status+'&fromDate='+fromDate+'&toDate='+toDate+'&searchsalesRep='+searchSalesRepNm+ '&searchfieldSales='+searchFieldSalesNm+ '&zone='+strZone+ '&region='+strRegion+ '&division='+strDivision+'&ramdomId=' + Math.random());
		fnStartProgress();
		dhtmlxAjax.get(ajaxUrl,fnShowFlGrid);
		document.getElementById("amterrorMsg").style.display = "none";
		document.getElementById("cmnterrorMsg").style.display = "none";
		document.getElementById("mailsenterror").style.display = "none";
		document.getElementById("mailsentmsg").style.display = "none";
	}
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
}

function fnShowFlGrid(loader){

		fnfetchLateFeeShowDetlCallback(loader);	
}

//If show flag is enable
function fnfetchLateFeeShowDetlCallback(loader){
	var response = loader.xmlDoc.responseText;
	fnStopProgress();
	if(response !=''){
		var setInitWidths = "40,100,100,100,60,60,100,70,70,70,70,70,60,70,70,70,30,100,30,80,80,80,80,1,1,1,1,1,1";
		var setColAlign = "center,left,left,left,left,left,left,left,left,left,left,left,left,left,left,right,center,left,center,left,left,left,left,left,left,left,left,left,left";
		var setColTypes = "ch,ro,ro,ro,ro,ro,ro,ro,ro,ro,ron,ro,ro,ron,ron,edn,ro,ed,ro,coro,coro,ro,ro,ro,ro,ro,ro,ro,ro";
		var setColSorting = "na,str,str,str,str,str,str,date,date,date,int,str,str,date,date,int,str,str,str,na,str,str,str,str,str,str,str,str,str";
		var setHeader = ["","Dist Name", "Rep Name","Assoc. Rep","Req ID","Set ID","Set Name","Loaned Date","Expected Return Date", "Actual Return Date","Days OverDue", "Etch ID/Trans. ID","Tag #","Charge Start Date","Charge End Date","Amount","","Notes","","Status<html><select name='cbo_head_stat' onchange='fnLoadStatus(this.value);'><option value='0'>[Select All]</option><option value='110501'>Accrued</option><option value='110502'>Hold</option><option value='110503'>Submitted</option><option value='110505'>Waived</option></html>","Waive Reason","Credit Date","Credit","","","","","",""];
		var setFilter = ["<input type='checkbox' value='no'  id='selectAll' onClick='fnChangedFilter(this);'/>","#select_filter", "#select_filter", "#select_filter","#text_filter", "#select_filter","#select_filter","#select_filter","#select_filter","#select_filter","#numeric_filter","#text_filter","#text_filter","#text_filter","#text_filter", "#numeric_filter","", "#text_filter", "", "#select_filter","#select_filter", "#select_filter", "", "", "","","","",""];
		var setColIds = "chkbox,distname,salesrep,assocrep,reqid,setid,setdesc,ldate,erdate,rdate,elpdays,etchid,tagid,chargestartdate,chargeenddate,amount,amthistoryfl,notes,historyfl,stflname,waivereason,creditdate,credit,stfl,count,incidentvalue,hwaivereason,hamtcnt,lnstatus";
		var enableTooltips = "true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true";
		fnStopProgress();
		
		var gridHeight = "";
		var footerStyles = [];
		var footerExportFL = true;
		var pagination = "";
			
		document.getElementById("sendMailBtn").style.visibility = 'visible';
		document.getElementById("DivExportExcel").style.display = "table-row";
		document.getElementById("submitBtn").style.visibility = 'visible';
	    document.getElementById("messagedata").style.visibility = 'hidden';
	    document.getElementById("dataGridDiv").style.display = "block";
	    
	 // split the functions to avoid multiple parameters passing in single
		// function
		gridObj = initGridObject('dataGridDiv');
		gridObj = fnCustomTypes(gridObj,setHeader, setColIds, setInitWidths, setColAlign, setColTypes,setColSorting, enableTooltips, setFilter,400,pagination,footerStyles);
		gridObj = loadDhtmlxGridByJsonData(gridObj, response);
		gridObj.attachEvent("onEditCell",doOnCellEdit);
		
		check_rowId = gridObj.getColIndexById("chkbox");
		var stfl_col = gridObj.getColIndexById("stfl");
		var credit_col = gridObj.getColIndexById("credit");
		var amt_col = gridObj.getColIndexById("amount");
		var count_col = gridObj.getColIndexById("count");
		var amtcount_col = gridObj.getColIndexById("hamtcnt");
		var hisfl_col = gridObj.getColIndexById("historyfl");
		var amthisfl_col = gridObj.getColIndexById("amthistoryfl");
		var incidentVal_col = gridObj.getColIndexById("incidentvalue");
		var chargeendDt_col = gridObj.getColIndexById("chargeenddate");
		var waivereason_col = gridObj.getColIndexById("waivereason");
		
		var notes_col = gridObj.getColIndexById("notes");
		var amount_col = gridObj.getColIndexById("amount");
		var lnstatus_col = gridObj.getColIndexById("lnstatus");
		
		var creditAmt ="";
		var statusNm = "";
		gridObj.setColumnHidden(stfl_col, true);
		gridObj.setColumnHidden(count_col, true);
		gridObj.setColumnHidden(incidentVal_col, true);
		gridObj.setColumnHidden(amtcount_col, true);
		gridObj.setColumnHidden(lnstatus_col, true);
		
		gridObj.forEachRow(function(rowId) {
			statusNm = gridObj.cellById(rowId, stfl_col).getValue();
			if(statusNm == "110503"){  // credit button should show only for submitted status 
				gridObj.cells(rowId,credit_col).setValue("<div><button class='btn btn-round color-1 material-design' type='button' onClick='fetchCreditDetails("+rowId+")'>Credit</buttton></div>"); 
			}
			creditAmt = gridObj.cellById(rowId, amt_col).getValue();
			count = gridObj.cellById(rowId, count_col).getValue();
			amtCount = gridObj.cellById(rowId, amtcount_col).getValue();
			lnsta = gridObj.cellById(rowId, lnstatus_col).getValue();
			if (creditAmt < 0) {
				creditAmt = creditAmt.toString();
				creditAmt = creditAmt.substring(1);
                gridObj.cells(rowId, amt_col).setValue(creditAmt);
                gridObj.setCellTextStyle(rowId, amt_col, "color:red;");
            }
			if(count > 1){
				gridObj.cells(rowId, hisfl_col).setValue("<div><img id='imgEdit' align='center' style='cursor:hand' width='15' height='15' onClick='fnCommentHistory("+rowId+")' alt='History Details' src='/images/icon_History.gif'/></div>");
			}
			if(amtCount > 1){
				gridObj.cells(rowId, amthisfl_col).setValue("<div><img id='imgEdit' align='center' style='cursor:hand' width='15' height='15' onClick='fnAmountHistory("+rowId+")' alt='History Details' src='/images/icon_History.gif'/></div>");
			}
			if(statusNm == "110501" || statusNm == "110502"){  //110501 - Accured 110502-Hold
				gridObj.cells(rowId,chargeendDt_col).setValue(""); 
			}
			
			//to set notes column as non editable if user has no access to edit
			if(noteseventaccfl != "Y"){
				gridObj.cells(rowId,notes_col).setDisabled(true); 
			}
			//to set amount column as non editable if user has no access to edit
			if(amounteventaccfl != "Y"){
				gridObj.cellById(rowId, amount_col).setDisabled(true);
			}
			if(statusNm == "110501" || statusNm == "110502" || statusNm == "110505"){
				gridObj.cellById(rowId, check_rowId).setDisabled(true);
			}
			if(statusNm != "110501" || noteseventaccfl != "Y"){  // if status Accured then only waived reason should enable
				gridObj.cells(rowId,waivereason_col).setDisabled(true);
			}
			//If LN Status is disputed the row should show in red color 
			if(lnsta == "21"){ //21 Disputed
				gridObj.setRowTextStyle(rowId, "background-color:#ffcccc;color:red;");
			}
			
		});
		
	}else{
		document.getElementById("messagedata").style.visibility = 'visible';
 		document.getElementById("submitBtn").style.visibility = 'hidden';
 		document.getElementById("dataGridDiv").style.display = 'none';
 		document.getElementById("DivExportExcel").style.display = 'none';
	}
}

//This Function Used to set custom types conditions in Grid
function fnCustomTypes(gObj,setHeader, setColIds, setInitWidths, setColAlign, setColTypes,setColSorting, enableTooltips,setFilter,gridHeight,pagination,footerStyles){
	
	combo = gObj.getCombo(19);
	combo.clear();
	for(var i=0;i<statusArr.length;i++){
	combo.put(statusArr[i][0],statusArr[i][1]);
	}
	combo.save();
	
	comboReason = gObj.getCombo(20);
	comboReason.clear();
	for(var i=0;i<reasonArr.length;i++){
		comboReason.put(reasonArr[i][0],reasonArr[i][1]);
	}
	comboReason.save();
	
if (setHeader != undefined && setHeader.length > 0) {
 gObj.setHeader(setHeader);
 var colCount = setHeader.length;
 }    

if (setInitWidths != "")
gObj.setInitWidths(setInitWidths);

if (setColAlign != "")
gObj.setColAlign(setColAlign);

if (setColTypes != "")
gObj.setColTypes(setColTypes);

if (setColSorting != "")
gObj.setColSorting(setColSorting);

if (enableTooltips != "")
gObj.enableTooltips(enableTooltips);

if (setFilter != "")
gObj.attachHeader(setFilter);

if(setColIds != ""){
 gObj.setColumnIds(setColIds);
}

if (gridHeight != "")
gObj.enableAutoHeight(true, gridHeight, true);
else
gObj.enableAutoHeight(true, 400, true);

if(pagination == "Y"){
    gObj.enablePaging(true,100,10,"pagingArea",true);
    gObj.setPagingSkin("bricks");
}	

var amt_col = gridObj.getColIndexById("amount");
gObj.setNumberFormat("00,000.00",amt_col);

return gObj;
}

//this function is used to perform action when a cell is editing
function doOnCellEdit(stage, rowId, cellInd, nValue, oValue) {
	var amount_col = gridObj.getColIndexById("amount");
	var notes_col = gridObj.getColIndexById("notes");
	var stfl_col = gridObj.getColIndexById("stflname");
	var hstfl_col = gridObj.getColIndexById("stfl");
	var waivereason_col = gridObj.getColIndexById("waivereason");

	var amount ='';
	var notes = '';
	var count = 0;
	//Highlight data when edit cell
	if(oValue != nValue){
		if (stage == 2 && (cellInd == stfl_col)){
			stfl = gridObj.cellById(rowId, hstfl_col).getValue();
			if(switchUserFlag !="Y" && statuseventaccfl == "Y"){
				//user must be able to update the status ,only when old status is either accrued(10) or hold (15)
				if((stfl == "110501" || stfl == "110502") && (nValue == "110503")){ //if accurred-110501 or hold-110502 , can change to submitted-110503
					gridObj.cells(rowId,stfl_col).setValue(nValue);
				}else if((stfl == "110501" || stfl == "110503" || stfl == "110502") && (nValue == "110505")){ //if accurred-110501 or submitted-110503 or hold-110502 can change to 110505-Waived
					gridObj.cells(rowId,stfl_col).setValue(nValue);
				}else if(holdeventaccfl == "Y" && stfl == "110501" && nValue == "110502"){ //if 110501-accurred can change to 110502-hold
					gridObj.cells(rowId,stfl_col).setValue(nValue);
				}else if(holdeventaccfl == "Y" && stfl == "110502" && nValue == "110501" || nValue == "110503"){ //if Hold-110502, can change to 110501-accurred,submitted-110503
					gridObj.cells(rowId,stfl_col).setValue(nValue);
				}
				else if(stfl == nValue)
				{
					gridObj.cells(rowId,stfl_col).setValue(nValue);  ////move to saved status 
				}

				else if(stfl != "110501" && nValue == "110502"){
					Error_Details('Only Late Fee in <b>Accrued</b> status can be placed on <b>Hold</b>');
				}else if(stfl != "110501" && stfl != "110503" && stfl != "110502" && nValue == "110505"){
					Error_Details('Only Late Fee in <b>Accrued</b> and <b>Submitted</b> and <b>Hold</b> status can be <b>Waived</b>');
				}else if(stfl != "110501" && stfl != "110502" && nValue == "110503"){
					Error_Details('Only Late Fee in <b>Accurred</b> and <b>Hold</b> status can be <b>Submitted</b>');
				}else if(stfl != "110502" && nValue == "110501"){ //if Hold-110502, can change to 110501-accurred
					Error_Details('Only Late Fee in <b>Hold</b> status can be <b>Accrued</b>');
				}
				else{
					//if all above condition is not satisfied then throw validation
					Error_Details(message_operations[872]);
				} 
					if(nValue == "110501" && noteseventaccfl == "Y"){  // if status Accured then only waived reason should enable
						gridObj.cells(rowId,waivereason_col).setDisabled(false);
					}else{
						gridObj.cells(rowId,waivereason_col).setDisabled(true);
					}
			}else{
				Error_Details(message_operations[871]);
			}
			if (ErrorCount > 0) {
				Error_Show();
				Error_Clear();
				return false;
			}
		}
		
		if (stage == 2 && (cellInd == amount_col)){
			if(switchUserFlag !="Y" && amounteventaccfl == "Y"){
				if (stage == 2 && (cellInd == amount_col)){
					amount = gridObj.cellById(rowId, amount_col).getValue();
					if(amount != oValue){
						fnSaveAmount(rowId,amount);
					}
				}
			}else{
				Error_Details(message_operations[868]);
			}
		}
		if (stage == 2 && (cellInd == notes_col)){
			if(switchUserFlag !="Y" && noteseventaccfl == "Y"){
				if (stage == 2 && (cellInd == notes_col)){
					notes = gridObj.cellById(rowId, notes_col).getValue();
					if(notes != oValue){
						fnSaveComment(rowId,notes);
					}
				}
			}else{
				Error_Details(message_operations[869]);
			}
		}

		if (ErrorCount > 0) {
			Error_Show();
			Error_Clear();
			return false;
		}
	}
	return true;
}

function fnLoadStatus(statusVal){

	var hstflnm_col = gridObj.getColIndexById("stfl");
	var stflnm_col = gridObj.getColIndexById("stflname");
	var waivereason_col = gridObj.getColIndexById("waivereason");

	if(switchUserFlag !="Y" && statuseventaccfl == "Y"){
		for(var i=0; i<gridObj.getRowsNum();i++)
		{
			var id=gridObj.getRowId(i);
			statusfl = gridObj.cellById(id, hstflnm_col).getValue();

			if((statusfl == "110501" || statusfl == "110502") && (statusVal == "110503")){ //if accurred-110501 or hold-110502 , can change to submitted-110503
				gridObj.cells(id,stflnm_col).setValue(statusVal);
			}else if((statusfl == "110501" || statusfl == "110503" || statusfl == "110502") && (statusVal == "110505")){ //if accurred-110501 or submitted-110503,can change to 110505-Waived
				gridObj.cells(id,stflnm_col).setValue(statusVal);
			}else if(holdeventaccfl == "Y" &&  statusfl == "110501" && statusVal == "110502"){ //if 110501-accurred , can change to 110502-hold
				gridObj.cells(id,stflnm_col).setValue(statusVal);
			}else if(holdeventaccfl == "Y" &&  statusfl == "110502" && statusVal == "110501"){ //if 110501-accurred , can change to 110502-hold
				gridObj.cells(id,stflnm_col).setValue(statusVal);
			}
			else if(statusfl == statusVal)
			{
				gridObj.cells(id,stflnm_col).setValue(statusVal); //move to saved status 
			}
			
			if(statusVal == "110501"){  // if status Accured then only waived reason should enable
				gridObj.cells(id,waivereason_col).setDisabled(false);
			}else{
				gridObj.cells(id,waivereason_col).setDisabled(true);
			}
		}
	}else{
		Error_Details(message_operations[871]);
	}
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}

}

function fetchCreditDetails(rowId) {
	var incident_col = gridObj.getColIndexById("incidentvalue");
	var charge_startDt_col = gridObj.getColIndexById("chargestartdate");
	var charge_endDt_col = gridObj.getColIndexById("chargeenddate");
	var amount_col = gridObj.getColIndexById("amount");
	var stflnm_col = gridObj.getColIndexById("stflname");
	
	amount = gridObj.cellById(rowId, amount_col).getValue();
	incidentval = gridObj.cellById(rowId, incident_col).getValue();
	chargeStartDt = gridObj.cellById(rowId, charge_startDt_col).getValue();
	chargeEndDt = gridObj.cellById(rowId, charge_endDt_col).getValue();
	stflname = gridObj.cellById(rowId, stflnm_col).getValue();
	
	if(switchUserFlag !="Y" && crediteventaccfl == "Y"){
		   windowOpener("/gmLoanerLateFeeAction.do?method=fetchCreditAmount&incidentId="+incidentval+"&chargeFromDate="+chargeStartDt+"&chargeToDate="+chargeEndDt+"&creditAmount="+amount+"&chargeDetailId="+rowId,"creditPopup","resizable=yes,scrollbars=yes,top=200,left=300,width=500,height=700");
	}
	else{
		Error_Details(message_operations[870]);
    }
    if (ErrorCount > 0) {
   		Error_Show();
   		Error_Clear();
   		return false;
	}
}

//This Function used close the popup window
function fnClose() {
	window.close();
}

function fnSaveCredit(){
	 var creditAmt = document.frmLateFeeCharges.creditAmt.value;   
		var creditNotes = document.frmLateFeeCharges.creditNotes.value; 
		var creditDt = document.frmLateFeeCharges.creditDate.value;
		var chargeDetailId = document.frmLateFeeCharges.chargeDetailId.value;
		var chargeFDt = document.frmLateFeeCharges.chargeFromDate.value;
		var chargeToDt = document.frmLateFeeCharges.chargeToDate.value;
		var amount = document.frmLateFeeCharges.creditAmount.value;
		var availamount = parseInt(document.frmLateFeeCharges.strAvailAmount.value);
		var creditDtObj = document.frmLateFeeCharges.creditDate;
		creditAmt = parseInt(creditAmt);
		
		var fromCurrDiff = dateDiff(creditDt, todaysDate, format);
		if(creditAmt == ""){
			Error_Details('Input is required in  <b>Credit Amount</b>');
		}
		if (creditDt == ""){
			Error_Details('<b>Credit Date</b> should not blank');
		}
		if(isNaN(creditAmt)){
			Error_Details('<b>Credit Amount</b> Should allow only numbers');
		}
		if(availamount < creditAmt){
			Error_Details('<b>Credit Amount</b> cannot exceed the Charge Amount');
		}
		if(creditAmt < 0){
			Error_Details('Negative Amount cannot be entered in <b>Credit Amount</b>');
		}
		if (fromCurrDiff > 0) {			
			Error_Details('<b>Credit Date</b> must be current or future date');
		}
		if(creditDt != ""){
			CommonDateValidation(creditDtObj,format,Error_Details_Trans(message[10002],format));
		}
		if (ErrorCount > 0){
			Error_Show();
			Error_Clear();
			return false;
		}
		creditAmt = "-"+creditAmt;
		var ajaxUrl = 
			fnAjaxURLAppendCmpInfo('/gmLoanerLateFeeAction.do?method=saveLoanerLateFeeCreditAmount&chargeDetailId='+chargeDetailId+'&creditAmount='+creditAmt+'&creditDate='+creditDt+'&creditNotes='+creditNotes+'&chargeFDt='+chargeFDt+'&chargeToDt='+chargeToDt+'&ramdomId='+Math.random());
		var loader = dhtmlxAjax.getSync(ajaxUrl);
		var errMsg = loader.xmlDoc.responseText;
		if(errMsg !=''){
			errMsg = errMsg.split('^ ');
			Error_Details(errMsg[1]);
			Error_Show();
			Error_Clear();
		}else{
			fnClose();

		}
}

function fnSaveStatus(){
	var stflnm_col = gridObj.getColIndexById("stflname"); 
	var hstflnm_col = gridObj.getColIndexById("stfl");
	var waivereason_col = gridObj.getColIndexById("waivereason");
	var hwaivereason_col = gridObj.getColIndexById("hwaivereason");
	var statusVal = "";
	var oldstatusVal = "";
	var inpuString = "";
	var strWaiveReason = "";
	var oldWaiveReason = "";
	
	for(var i=0; i<gridObj.getRowsNum();i++)
	{
		var id=gridObj.getRowId(i);
		statusVal = gridObj.cellById(id, stflnm_col).getValue();
		oldstatusVal = gridObj.cellById(id, hstflnm_col).getValue();
		strWaiveReason = gridObj.cellById(id, waivereason_col).getValue();
		oldWaiveReason = gridObj.cellById(id, hwaivereason_col).getValue();
		if(!isNaN(statusVal) || !isNaN(strWaiveReason)){
			if((oldstatusVal != statusVal) || ((oldWaiveReason != strWaiveReason) && statusVal == "110501")) {
				inpuString = inpuString + '{"chargeid":"' + id + '",' + '"status":"' + statusVal + '",' + '"waiveReason":"' + strWaiveReason + '"},';
			}
		}
	}
			
	if(inpuString != ''){
		inpuString = inpuString.substring(0, inpuString.length - 1);
		inpuString = '[' + inpuString + ']';
        //PC-4042 Save Waive reasons for the filtered data on Loaner Late Fee screen-->change getSync to Post Sync
	    var loader = dhtmlxAjax.postSync('/gmLoanerLateFeeAction.do?method=saveLoanerLateFeeStatus&ramdomId='+Math.random()+'&'+fnAppendCompanyInfo(),'inputString='+inpuString);
		fnGo();
	}
}

	/*
	 * This function used to load the Rebate Audit trail information
	 * Parameter Type - Based on Rebate field - Type passed dynamically
	 * 
	 */
function fnCommentHistory(chargeDtlId){

	windowOpener("/GmPageControllerServlet?strPgToLoad=/gmAuditTrail.do&auditId=''&txnId="+chargeDtlId+"&strDynamicAuditTrailFl=Y&strProcedureRuleId=CHARGE_DETAIL&strAuditType="+110500, "",
                 "resizable=yes,scrollbars=yes,top=250,left=300,width=785,height=300,status=1"); 
}

function fnAmountHistory(chargeDtlId){
	windowOpener("/GmPageControllerServlet?strPgToLoad=/gmAuditTrail.do&auditId=''&txnId="+chargeDtlId+"&strDynamicAuditTrailFl=Y&strProcedureRuleId=CHARGE_AMT_DETAIL&strAuditType="+110500, "",
		"resizable=yes,scrollbars=yes,top=250,left=300,width=785,height=300,status=1");
}
	
function fnSaveAmount(rowId,amount){
//The Anount field will be editable only when the status is  "ACCRUED" or "HOLD"
			if(switchUserFlag !="Y" && amounteventaccfl == "Y"){
                //PC-4042 Save Waive reasons for the filtered data on Loaner Late Fee screen-->change getSync to Post Sync
                var loader = dhtmlxAjax.postSync('/gmLoanerLateFeeAction.do?method=saveLoanerLateFeeAmount&chargedtlId='+rowId+ '&amount='
			                + amount+'&ramdomId='+Math.random()+'&'+fnAppendCompanyInfo());
				var response = loader.xmlDoc.responseText;
			    var status = loader.xmlDoc.status;
                if(response =! "" && status == 300){
                     document.getElementById("amterrorMsg").style.display = "block";
                }
            }
            else{
            	Error_Details(message_operations[868]);
            }
            if (ErrorCount > 0) {
		   		Error_Show();
		   		Error_Clear();
		   		return false;
			}
}



//this function is to save the comment entered or updated by the user 
function fnSaveComment(rowId,comment){
			if(switchUserFlag !="Y" && noteseventaccfl == "Y"){
				if(comment.length <= 200){
                    //PC-4042 Save Waive reasons for the filtered data on Loaner Late Fee screen-->change getSync to Post Sync
                     var loader = dhtmlxAjax.postSync('/gmLoanerLateFeeAction.do?method=saveLoanerLateFeeComment&chargedtlId='+rowId+ '&comment='
			                + comment+'&ramdomId='+Math.random()+'&'+fnAppendCompanyInfo());
					var response = loader.xmlDoc.responseText;
					 var status = loader.xmlDoc.status;
	                if(response =! "" && status == 300){
	                     document.getElementById("cmnterrorMsg").style.display = "block";
	                }
				}else{
					Error_Details('<b>Comments</b> cannot exceed more than 200 characters');
				}
			}else{
					Error_Details(message_operations[869]); 
			}
			if (ErrorCount > 0) {
	   		Error_Show();
	   		Error_Clear();
	   		return false;
			}
	}
	
//This Function Used to download Excel Sheet
function fnExport() {
	gridObj.toExcel('/phpapp/excel/generate.php');
}

//Called to load zone list dynamically based on the selected regions in the Regions list
function fnLoadZone(arrChk)
{
	var j = 0;
	var k = 0;
	var arrZone = new Array();
	var arrResult = new Array();
	if (arrChk != null && arrChk.length > 0)
	{
	// does nothing ... but dont remove		
	}
	else if( document.all.Chk_GrpDist != null ) //for VP,AD the ZONE filter is not available  Chk_GrpZone
	{
		var arr = document.all.Chk_GrpDist;
		var arrChk = new Array();
		var arrlen = arr.length;
		if(arrlen == undefined && arr.checked == true)
		{
			arrChk[j] = arr.id;
			j++;
		}
		else
		{
			for (var i=0;i< arrlen;i++ )
			{
				if (arr[i].checked == true)
				{
					arrChk[j] = arr[i].id;
					j++;
				}
			}
		}
		if(j==0 && document.all.Chk_GrpRegn != null){
			var arrayValues = getArrayOfValues(document.all.Chk_GrpRegn);
			if(arrayValues.length > 0){
				fnLoadRegn(arrayValues);
			}
			return;
		}
	}else{
		/*	
		    If the above 'else if' is not executed, the arrChk will throws error. 
			If we declared arrChk variable at the top of the method then 
			the filter methods are calling cyclically it will throws out of memory error 
		*/
		var arrChk = new Array(); 
	}

		document.all.Div_Zone.innerHTML = '';
		var newhtml = '';
		var arrChklen = arrChk.length;
		var arrZonelen = ZAr.length;
		var chkDiv = '';
		if(arrChklen > 0)
		{
			for (var i=0;i<arrChklen ;i++ )
			{
				if(arrChklen != 1){
					chkDiv = arrChk[i];
				}else{
					chkDiv = arrChk;
				}
				
				for (var j=0;j<arrZonelen ;j++ )
				{		
					if (ZAr [j][3] == chkDiv)//Code Id For All
					{
						arrResult[k] = new Array();
						arrResult[k] = ZAr[j];
						arrZone[k] = ZAr[j][0];
						k++;
					}
				}
			}
		}
		else {
			for (var j=0;j<arrZonelen ;j++ )
			{		
					arrResult[k] = new Array();
					arrResult[k] = ZAr[j];
					arrZone[k] = ZAr[j][0];
					k++;
			}
	}
		arrResult.sort(sortFunction);
		for (var l=0;l<k ;l++ )
		{
			if (newhtml.indexOf("id='"+arrResult[l][0]+"'") < 0)
			{
			newhtml = newhtml + fnCreateChkBox('Zone',arrResult[l][0],arrResult[l][1],arrResult[l][2],'fnLoadRegn(this)');
			}
		}
		document.all.Div_Zone.innerHTML = newhtml;
		if (newhtml =='')
		{
			document.all.Div_Zone.innerHTML = '<span class=RightTextRed>No Zone</span>';
			document.all.Div_Regn.innerHTML = '<span class=RightTextRed>No Region</span>';
		}else
		{
			document.all.Div_Zone.innerHTML = newhtml;	
		fnLoadRegn(arrZone);
		} 

}

function fnLoadRegn(arrChk)
{
	var j = 0;
	var k = 0;
	var arrRegn = new Array();
	var arrResult = new Array();
	if (arrChk != null && arrChk.length > 0)
	{
	// does nothing ... but dont remove		
	}
	else if( document.all.Chk_GrpZone != null ) //for VP,AD the ZONE filter is not available
	{
		var arr = document.all.Chk_GrpZone;
		var arrChk = new Array();
		var arrlen = arr.length;
		if(arrlen == undefined && arr.checked == true)
		{
			arrChk[j] = arr.id;
			j++;
		}
		else
		{
			for (var i=0;i< arrlen;i++ )
			{
				if (arr[i].checked == true)
				{
					arrChk[j] = arr[i].id;
					j++;
				}
			}
		}
		if(j==0 && document.all.Chk_GrpDiv != null){
			var arrayValues = getArrayOfValues(document.all.Chk_GrpDiv);
			if(arrayValues.length > 0){
				fnLoadZone(arrayValues);
			}
			return;
		}
	}else{
		/*	
		    If the above 'else if' is not executed, the arrChk will throws error. 
			If we declared arrChk variable at the top of the method then 
			the filter methods are calling cyclically it will throws out of memory error 
		*/
		var arrChk = new Array(); 
	}

		document.all.Div_Regn.innerHTML = '';
		var newhtml = '';
		var arrChklen = arrChk.length;
		var arrregnlen = RAr.length;
		var chkzone = '';

		if(arrChklen > 0)
		{
			for (var i=0;i<arrChklen ;i++ )
			{
				chkzone = arrChk[i];
				for (var j=0;j<arrregnlen ;j++ )
				{		
					if (RAr [j][2] == chkzone)//Code Id For All
					{
						arrResult[k] = new Array();
						arrResult[k] = RAr[j];
						arrRegn[k] = RAr[j][0];
						k++;
					}
				}
			}
		}
		else {
			for (var j=0;j<arrregnlen ;j++ )
			{		
					arrResult[k] = new Array();
					arrResult[k] = RAr[j];
					arrRegn[k] = RAr[j][0];
					k++;
			}
	}
		arrResult.sort(sortFunction);
		for (var l=0;l<k ;l++ )
		{
			if (newhtml.indexOf("id='"+arrResult[l][0]+"'") < 0)
			{
			newhtml = newhtml + fnCreateChkBox('Regn',arrResult[l][0],arrResult[l][1],arrResult[l][2],'fnLoadTerr(this)');
			}
		}
		document.all.Div_Regn.innerHTML = newhtml;
		if (newhtml =='')
		{
			document.all.Div_Regn.innerHTML = '<span class=RightTextRed>No Region</span>';		
		}else
		{
			document.all.Div_Regn.innerHTML = newhtml;	
			fnLoadTerr(arrRegn);
		} 		
}
//Called to load Distributor list dynamically based on the selected regions in the Regions list
function fnLoadDist(arrChk)
{
	var j = 0;
	var k = 0;
	var arrDist = new Array();
	var arrResult = new Array();
	
		document.all.Div_Dist.innerHTML = '';
		var newhtml = '';
		var arrChklen = 0;
		var arrDistlen = DAr.length;
		var chkregn = '';
		var arrDiv = new Array();
		var defDivarr = new Array();
		defDivarr = 100800;
		if(arrDistlen > 0)
		{
			for (var j=0;j<arrDistlen ;j++)
			{		
					if (newhtml.indexOf(DAr[j][0]) < 0)
					{
					newhtml = newhtml + fnCreateChkBox('Dist',DAr[j][0],DAr[j][1],DAr[j][2],'fnLoadZone(this)');
					arrDiv[k] = DAr[j][0];
					k++;
					}
			}
		}
		document.all.Div_Dist.innerHTML = newhtml;
		if (newhtml =='')
		{
			document.all.Div_Dist.innerHTML = '<span class=RightTextRed>No Division</span>';
		}else
		{
			document.all.Div_Dist.innerHTML = newhtml;	
			fnLoadZone(defDivarr);	//Spine setting by default
		} 		

}

function fnLoadTerr(arrChk){
	
}

function fnCreateChkBox(grp,id,nm,pid,jsfunc)
{
	if(id == "100800"){
		var html = "<input class='RightInput' type='checkbox'  checked='true' name='Chk_Grp"+grp+"' id='"+id+"' value='"+pid+"' onClick='javascript:"+jsfunc+";'>&nbsp;<span class='RightTextAS'>"+nm+"</span><BR>";
	}else{
		var html = "<input class='RightInput' type='checkbox' name='Chk_Grp"+grp+"' id='"+id+"' value='"+pid+"' onClick='javascript:"+jsfunc+";'>&nbsp;<span class='RightTextAS'>"+nm+"</span><BR>";
	}
	return html;
}

function sortFunction(a, b) {
	var x = a[1].toLowerCase();
	var y = b[1].toLowerCase();
	return ((x < y) ? -1 : ((x > y) ? 1 : 0));
}

//Function to get the checked values from the object passed
function fnGetCheckedValues(obj)
{
	var str = '';
	var arr = obj;
	 
	if (obj)
	{
		var arrlen = arr.length;
		if (arrlen > 0)
		{
			for (var i=0;i< arrlen;i++ )
			{
				if (arr[i].checked == true)
				{
					str = str + arr[i].id + ',';
				}
			}
		
			if (str != '')
			{
				str = str.substr(0,str.length-1);
			}
		}
		else if(arr.checked == true)
		{
			str = arr.id;
		}
	}
	return str;
}

function fnCheckBoxSelections(val,arr)
{
	var valobj = val.split(",");
	var sval = '';
	var arrlen = arr.length;
	if (arrlen > 0)
	{
		for (var j=0;j< valobj.length;j++ )
		{
			sval = valobj[j];
			for (var i=0;i< arrlen;i++ )
			{
				if (arr[i].id == sval)
				{
					
					arr[i].checked = true;
					break;
				}
			}
		}
	}
	else
	{
		arr.checked = true;
	}
}

function getArrayOfValues(obj){
	var arr = obj;
	var j =0;
	var arrChk = new Array();
	if(obj == null){
		return arrChk;
	}
	var arrlen = arr.length;
	if(arrlen == undefined && arr.checked == true)
	{
		arrChk[j] = arr.id;
		j++;
	}else{
		for (var i=0;i< arrlen;i++ )
		{
			if (arr[i].checked == true)
			{
				arrChk[j] = arr[i].id;
				j++;
			}
		}
	}
	return arrChk;
}


//This function is to send emails of selected records
function fnSendHREmails (){
	var chargeInputString = gridObj.getCheckedRows(0);
	if(switchUserFlag !="Y"){
		if(chargeInputString != ""){
			var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmLoanerLateFeeAction.do?method=sendHREmails&chargeDetailInputString='
	                + chargeInputString +'&ramdomId='+Math.random());

	var loader = dhtmlxAjax.getSync(ajaxUrl);
	var response = loader.xmlDoc.responseText;
	var status = loader.xmlDoc.status;
	document.getElementById("mailsenterror").style.display = "none";
	document.getElementById("mailsentmsg").style.display = "none";
	 if(response =! "" && status == 300){
         document.getElementById("mailsenterror").style.display = "block";
         document.getElementById("mailsentmsg").style.display = "none";
    }else if(response =! "" && status == 200){
    	document.getElementById("mailsentmsg").style.display = "block";
    }else if(status == 200){
    	document.getElementById("mailsentmsg").style.display = "block";
    	document.getElementById("mailsenterror").style.display = "none";
    }
		}else{
			Error_Details(message_operations[873]);
	    	document.getElementById("mailsentmsg").style.display = "none";
	}
	
}else{
		Error_Details(message_operations[872]); 
}
if (ErrorCount > 0) {
	Error_Show();
	Error_Clear();
	return false;
}
}
//This fuction is to check and uncheck master checkbox
function fnChangedFilter(obj) {
    if (obj.checked) {
    	gridObj.forEachRow(function (rowId) {
            var check_disable_fl = gridObj.cellById(rowId, check_rowId).isDisabled();
            if (!check_disable_fl) {
                gridObj.cellById(rowId, check_rowId).setChecked(true);
            }
        });
    } else {
        	gridObj.forEachRow(function (rowId) {
            var check_id = gridObj.cellById(rowId, check_rowId).getValue();
            if (check_id == 1) {
            var check_disable_fl = gridObj.cellById(rowId, check_rowId)
                .isDisabled();
            if (!check_disable_fl) {
               gridObj.cellById(rowId, check_rowId).setChecked(false);

            }
            }
        });
    }
}

function fnClearReqId(){
	requestID = document.frmLateFeeCharges.requestID.value;
	if(requestID != ''){
		
		document.frmLateFeeCharges.fromDate.value = '';
		document.frmLateFeeCharges.toDate.value = '';
		document.frmLateFeeCharges.status.value = 0;
		document.frmLateFeeCharges.searchfieldSales.value = '';
		document.frmLateFeeCharges.searchsalesRep.value = '';
		document.frmLateFeeCharges.fieldSales.value = '';
		document.frmLateFeeCharges.salesRep.value = '';
		fnLoadZone(null);
		fnLoadRegn(null);
		fnLoadDist(null);
	}
}
