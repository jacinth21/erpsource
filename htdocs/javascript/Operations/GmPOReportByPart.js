// This function used to when click the hyper link of PO - Load the PO Details
function fnCallPO(po, vid) {
	document.frmVendor.action = "/GmPOServlet";
	document.frmVendor.hAction.value = 'ViewPO';
	document.frmVendor.hPOId.value = po;
	document.frmVendor.hVenId.value = vid;
	document.frmVendor.submit();
}
// This function used to Reload the screen.
function fnGo() {

 	//PC-4543 Upper Case Conversion Issue
	if(document.frmVendor.Txt_PartNum != undefined){
    	if(document.frmVendor.Txt_PartNum.value != undefined && document.frmVendor.Txt_PartNum.value != ""){
    	document.frmVendor.Txt_PartNum.value = TRIM(document.frmVendor.Txt_PartNum.value.toUpperCase());
    	}
    }

	document.frmVendor.hAction.value = 'Go';
	fnStartProgress('Y');
	document.frmVendor.submit();
}
// This function used to drill down the part details.
function Toggle(val) {

	var obj = document.getElementById("div" + val);
	var trobj = document.getElementById("tr" + val);
	var tabobj = document.getElementById("tab" + val);

	if (obj.style.display == 'none') {
		obj.style.display = '';
		trobj.className = "ShadeRightTableCaptionBlue";
		tabobj.style.background = "#ecf6fe";
	} else {
		obj.style.display = 'none';
		trobj.className = "";
		tabobj.style.background = "#ffffff";
	}
}
// This function used to open the comment log
function fnOpenOrdLogwo(val) {
	windowOpener("/GmCommonLogServlet?hType=1232&hID=" + val, "PrntInv",
			"resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
}