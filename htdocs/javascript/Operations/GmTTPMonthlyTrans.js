function fnReset()
{
	
}	

function fnFetch()
{	
	document.frmTTPMonthly.strOpt.value = "edit";
	if(document.all.checkSelectedSheet != null)	
	{
		document.all.selectAll.checked = false; 	
	}
	fnStartProgress();
	document.frmTTPMonthly.submit();	
}
function fnViewSummary()
{
	document.frmTTPMonthly.ViewSummary.disabled = true;
	document.frmTTPMonthly.action = "/gmTTPMonthlySummary.do";
	document.frmTTPMonthly.strOpt.value = "report";
	var ttpnm = document.frmTTPMonthly.searchttpId.value; 
	document.frmTTPMonthly.ttpName.value = ttpnm;
	fnStartProgress('Y');
	document.frmTTPMonthly.submit();
}

function fnCheckSelections()
{
	objCheckGSArrLen = 0;
	objCheckRIArrLen = 0;
	
	objCheckGSArr = document.frmTTPMonthly.checkSelectedGS;
	if(objCheckGSArr) {
	if (objCheckGSArr.type == 'checkbox')
	{
		objCheckGSArr.checked = true;
	}
	else {
		objCheckGSArrLen = objCheckGSArr.length;
		for(i = 0; i < objCheckGSArrLen; i ++) 
			{	
				objCheckGSArr[i].checked = true;
			}
		}
	}
	
	objCheckRIArr = document.frmTTPMonthly.checkSelectedRI;
	if (objCheckRIArr) {
		// check if there is just one element in checked. if so mark it as selected
	if (objCheckRIArr.type == 'checkbox')
	{
		objCheckRIArr.checked = true;
	}
	else {
		objCheckRIArrLen = objCheckRIArr.length;
		for(i = 0; i < objCheckRIArrLen; i ++) 
			{	
				objCheckRIArr[i].checked = true;
			}
		}
	}
}
function fnClose()
{
window.opener.fnFetch();
window.close();
}



function fnSelectAll(varCmd)
{
				objSelAll = document.all.selectAll;
				if (objSelAll ) {
				objCheckSheetArr = document.all.checkSelectedSheet;
//				objCheckSiteArrLen = objCheckSiteArr.length;
				
					
				if ((objSelAll.checked || varCmd == 'selectAll') && objCheckSheetArr)
				{
					objSelAll.checked = true;
					objCheckSheetArrLen = objCheckSheetArr.length;
					
					objCheckSheetArr1 = document.frmTTPMonthly.checkSelectedSheet;
					if (objCheckSheetArr1.type == 'checkbox')
						{
							objCheckSheetArr1.checked = true;
						}
													
					for(i = 0; i < objCheckSheetArrLen; i ++) 
					{	
						objCheckSheetArr[i].checked = true;
					}
				 
				 
				}
				else if (!objSelAll.checked  && objCheckSheetArr ){
					objCheckSheetArrLen = objCheckSheetArr.length;
					
					objCheckSheetArr1 = document.frmTTPMonthly.checkSelectedSheet;
					if (objCheckSheetArr1.type == 'checkbox')
						{
							objCheckSheetArr1.checked = false;
						}
					
					for(i = 0; i < objCheckSheetArrLen; i ++) 
					{	
						objCheckSheetArr[i].checked = false;
					}
				 	//document.frmTTPMonthly.Submit.disabled = true;
					}
				}				 
	}
//To set Master check box is  working as the flow, the selectAll checkbox changed based on checkSelectedSheet checkbox.
function fnMasterCheckbox()
{
			var	objSel = document.all.checkSelectedSheet;
				if (objSel) {
					document.all.selectAll.checked=true;
				 var  objCheckArrLen = document.all.checkSelectedSheet.length;
				    for (var i = 0; i < objCheckArrLen; i++) {
						if (document.all.checkSelectedSheet[i].checked == false) {
							document.all.selectAll.checked = false;	
							break;
						}
					}
				}
}