function fnLoad(){
    if(document.frmVoidedDHRReport.fromDate.value != ""){
		CommonDateValidation(document.frmVoidedDHRReport.fromDate,dateFormat,Error_Details_Trans(message_operations[300],dateFormat));
	}
	if(document.frmVoidedDHRReport.toDate.value != ""){
	CommonDateValidation(document.frmVoidedDHRReport.toDate,dateFormat,Error_Details_Trans(message_operations[301],dateFormat));
	}
	var fromDateRange = dateDiff(document.frmVoidedDHRReport.fromDate.value,document.frmVoidedDHRReport.currentdate.value,dateFormat);
	if(fromDateRange < 0){
		Error_Details(message_operations[302]);
	}
	var toDateRange = dateDiff(document.frmVoidedDHRReport.toDate.value,document.frmVoidedDHRReport.currentdate.value,dateFormat);
	if(toDateRange < 0){
		Error_Details(message_operations[303]);
	}
	var datediff = dateDiff(document.frmVoidedDHRReport.fromDate.value,document.frmVoidedDHRReport.toDate.value,dateFormat);
	if(datediff < 0){
		Error_Details(message_operations[304]);
	}
	if (ErrorCount > 0){
		Error_Show();
	    Error_Clear();
		return false;
	}

	fnStartProgress('Y');
	document.frmVoidedDHRReport.strOpt.value = "Reload ";
	document.frmVoidedDHRReport.action = '/GmVoidedDHRReport.do?method=editDHR';  
	fnStartProgress('Y');
	document.frmVoidedDHRReport.submit();
}
//This function is used to initiate grid
var gridObj;
function fnOnLoad(){ 
	if (objGridData.value != '') {
		gridObj = initGridData('VOIDEDDHRDATA',objGridData);
		gridObj.attachEvent("onFilterStart",fnCustomFilter);
	}
}
//This function is used to initiate grid
function initGridData(divRef,gridData){
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);
	gObj.attachHeader('#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#numeric_filter,#text_filter,#text_filter,#select_filter,#select_filter,#text_filter,#select_filter,#text_filter');
	gObj.enableTooltips("true,true,true,false,false,false,false,false,false,false,true,false,true");
	gObj.init();
	gObj.loadXMLString(gridData);	
	return gObj;	
}
function fnCustomFilter(ord,data){
	/* This custom filter to avoid & content column filter issue. 
	 * Because the actual text for & is '&amp;'
	 * Ex. Let us assume xml as <cell><a ..>B&amp;G Manufacturing Co ,Inc</a></cell>.  If user try to filter 'B&G' text then it wont show . B&;G Manufacturing Co ,Inc*/
		
			if(data[0]!=""){
				var original=data[0];
				original = original.toLowerCase();
				data[0]=function(value){
							if (value.toString().replace(/&amp;/g,"&").toLowerCase().indexOf(original)!=-1)
								return true;
							return false;
							};
			}
				
		gridObj.filterBy(ord,data);
		return false;
	}

//This function is used to activate Load button while pressing the enter key
function fnEnter() {
	
	if(event.keyCode == 13){
		fnLoad();
	}
}
function fnExportExcel(){
	
	gridObj.toExcel('/phpapp/excel/generate.php');
	
}

