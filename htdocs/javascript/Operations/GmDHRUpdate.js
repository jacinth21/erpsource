var gridObj = '';
var qtyremains = '';

function fnViewPrintAll()
{
var dhrTransIds='';
var gridrows =gridObj.getAllRowIds();
var gridrowsarr = gridrows.toString().split(",");
var arrLen = gridrowsarr.length;
var rowId='';
var gridval='';
for (var i=0;i<arrLen;i++)
{
    rowId=gridrowsarr[i];
    gridval=gridObj.cellById(rowId,2).getValue();
    
    if (gridval != '') 
    {
   			dhrTransIds=dhrTransIds+ gridval;
    		dhrTransIds=dhrTransIds + "$" + rowId + ','; 
    }
}
windowOpener("/GmDHRUpdateServlet?companyInfo="+companyInfoObj+"&hAction=DhrPrintAll&hId="+dhrTransIds,"Con","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}
function fnViewPrint()
{
	var hmode='';
	hmode = document.frmOrder.hAction.value;
	windowOpener("/GmPOReceiveServlet?companyInfo="+companyInfoObj+"&hAction=ViewDHRPrint&hDHRId=" + gStrDHRId +"&hVenId=" + gStrVendorId+"&hMode=" + hmode+"&hWOId=" + gStrWOId,"DHR","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}
function fnViewPrintFAR()
{
	windowOpener("/GmPOReceiveServlet?companyInfo="+companyInfoObj+"&hAction=printFAR&hWOId=" + gStrWOId,"FAR","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}
function fnUpdate()
{
	Error_Clear();	
	var strDHRId = document.frmOrder.Txt_DHRID.value;
	var strhDHRId = document.frmOrder.hDHRId.value;

	if(strhDHRId != strDHRId){
		Error_Details(message[2000]);	
	}
	
	//dont allow update if logistics and status is not pending validation
	if (hDeptId == '2014' && document.frmOrder.hStatusFl.value != '3')
	{
		Error_Details(message[2009]);	
	}
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}

	obj = document.frmOrder.hAction;
	if(validateObject(document.frmOrder.Chk_UDI)){ // Validating the Object
		var checkval = document.frmOrder.Chk_UDI;
	}
	if(checkval.checked == true){
		document.frmOrder.Chk_UDI.value = 'Y';
	}else{
		document.frmOrder.Chk_UDI.value = 'N';
	}
	
	var qtyrec = parseFloat(document.frmOrder.hRecQty.value);
	//Below code will be used to validate the verification process
	var	InspectedBY = gStrInspectedBy;
	var	PackedBY = gStrPackagedBy;
	var	VerifiedBy = gStrUserId;
	var newLcn = '';
	var strTxn = "50902";
	var blFlg = "false";
	if (obj.value == 'C')
	{
		fnSubmit();
	}
	else
	{
		if (obj.value == 'I')
		{
			obj1 = document.frmOrder.Txt_QtyInsp.value;
			if (obj1 == '')
			{
				Error_Details(message[10]);
			}
			obj2 = document.frmOrder.Cbo_InspEmp.value;
			if (obj2 == '0')
			{
				Error_Details(message[11]);
			}
			obj3 = document.frmOrder.Txt_QtyRej.value;
			if (obj3 == '')
			{
				Error_Details(message[12]);
			}
			if (obj1 > qtyrec)
			{
				Error_Details(message[17]);
			}
			if (obj3 > qtyrec)
			{
				Error_Details(message[18]);
			}		
			obj4 = document.frmOrder.Cbo_ReasonType.value;
			if (obj3 > 0 && obj4 == '0')
			{
				Error_Details(message[25]);
			}
		}
		else if (obj.value == 'P')
		{
			/*var strSterFl=document.frmOrder.hSterFl.value;
			var skip =false;
			if(strSterFl!='1')
				skip = document.frmOrder.Chk_Skip.status;
			
			if (!skip)
			{*/
				var qtyrej = parseFloat(document.frmOrder.hRejQty.value);
				
				if(isNaN(qtyrej)){
					qtyrej = 0;
				}
				
				var qtyclose = parseFloat(document.frmOrder.hCloseQty.value);
				if(isNaN(qtyclose)){
					qtyclose = 0;
				}
				var gridrows =gridObj.getAllRowIds();
				var gridrowsarr = gridrows.toString().split(",");
				var arrLen = gridrowsarr.length;
				var gridval;
				var errorGridTransNm = '';
				var gridTransNm;
				var obj1=0;
				var inpStr="";
				var strExcludeCodeId = "56023";
				for (var i=0;i<arrLen;i++)
				{
			        rowId=gridrowsarr[i];
			        gridval=gridObj.cellById(rowId,1).getValue();
			        gridTransNm = gridObj.cellById(rowId,0).getValue();			        
			        if(rowId != strExcludeCodeId){
			        	if(parseInt(gridval)>0){
			        		blFlg = "true";
			        		if(rowId == '100067'){
			        			strTxn = '50931';
			        		}else if(rowId == '100065'){
			        			strTxn = '50934';
			        		}else if(rowId == '100066'){
			        			strTxn = '50935';
			        		}else if(rowId == '100069'){
			        			strTxn = '50936';
			        		}else if(rowId == '100070'){
			        			strTxn = '50937';
			        		}
			        	}
			        }
					if(gridval!='' && gridval!='0' && gridval!=undefined && gridval!=null){						
						if(!IsNumeric(gridval)){						
							errorGridTransNm = errorGridTransNm + "<br>" + gridTransNm;
						}else{
							inpStr=inpStr + rowId;
							inpStr = inpStr +',';
							inpStr=inpStr + gridval;
							inpStr=inpStr + '|';
							obj1+=parseFloat(gridval);
				        }
					}
				}
				if(errorGridTransNm != ''){
					Error_Details(Error_Details_Trans(message_operations[127],errorGridTransNm));
				}
				obj1=Math.round(obj1*Math.pow(10,2))/Math.pow(10,2);
				if (qtyrec + qtyclose != qtyrej)
				{
					if (obj1 == '')
					{
						Error_Details(message[13]);
					}
				
					if (obj1 > qtyrec-qtyrej+qtyclose)
					{
						Error_Details(message[20]);
					}
					
					if (obj1 < qtyrec-qtyrej+qtyclose)
					{
						Error_Details(message[19]);
					}
				}					
			//}
			document.frmOrder.hInpStr.value=inpStr;
		}
		else if (obj.value == 'V')
		{
			var qtyrej = parseInt(document.frmOrder.hRejQty.value);
			
			if(isNaN(qtyrej)){
				qtyrej = 0;
			}
			
			var qtyclose = parseInt(document.frmOrder.hCloseQty.value);
			if(isNaN(qtyclose)){
				qtyclose = 0;
			}
			obj1 = document.frmOrder.Txt_Qty.value;
			if (obj1 == '')
			{
				Error_Details(message[15]);
			}

			if (obj1 > qtyrec-qtyrej+qtyclose)
			{
				Error_Details(message[22]);
			}
			if (obj1 < qtyrec-qtyrej+qtyclose)
			{
				Error_Details(message[23]);
			}
			if ((InspectedBY ==	VerifiedBy) || (PackedBY == VerifiedBy)) 
			{
				Error_Details(message[24]);
			}
		}
	    if (ErrorCount > 0)
		{
				Error_Show();
				Error_Clear();
				return false;
		}

	    document.frmOrder.hAction.value = gStrMode;
	  	if(gStrMode =='I' )
		{
			document.frmOrder.RE_TXN.value = '50901';
		}
		else if(gStrMode =='V')
		{
			document.frmOrder.RE_TXN.value = '50903';
			//document.frmOrder.hpnumLcnStr.value = pnumLcnStr;
			if(newLcn != '')
			{
				var answer = confirm(Error_Details_Trans(message_operations[128],newLcn.substring(2)))
				if (!answer){
					return false;
				}
			}
			document.frmOrder.Txt_Qty.disabled=false;
		}
		else if(gStrMode =='P')
		{
			if(blFlg == "true"){
				document.frmOrder.RE_TXN.value = strTxn;
				if(gstrPartClass == '4031' && gstrPoType== '3101' && gstrControlNum.toUpperCase()=="NOC#"){
					document.frmOrder.txnStatus.value = '';
				}
				else
				{
					document.frmOrder.txnStatus.value = 'VERIFY';
				}
			}else{
				document.frmOrder.RE_TXN.value = strTxn;
			}
			
		}
		document.frmOrder.action= "/gmRuleEngine.do";
		fnStartProgress('Y');
		document.frmOrder.submit();
	 }
}

function fnViewPrintWO()
{
windowOpener("/GmWOServlet?companyInfo="+companyInfoObj+"&hAction=PrintWO&hWOId=" + gStrWOId + "&hDocRev=" + gStrDocRev + "&hDocFl=" + gStrDocActiveFl,"WO1","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=640");
}

function fnViewPrintNCMR()
{
windowOpener("/GmNCMRServlet?companyInfo="+companyInfoObj+"&hAction=PrintNCMR&hId=" + gStrNCMRId,"NCMR","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=640");
}
function fnViewPrintEVAL()
{
windowOpener("/GmNCMRServlet?companyInfo="+companyInfoObj+"&hAction=PrintEVAL&hId=" + gStrNCMRId,"NCMR","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=640");
}
function fnCalDate(val)
{
	var year = '';
	var month = '';
	var NormalYr = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
	var LeapYr = new Array(31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
	var cal = 0;
	var rem = 0;
	var cal1 = 0;
	var date = '';
	var num = val.substring(3,6);

	//Hardcoded till Year 2008
	if (val.charAt(2)=='D') // For 2003
	{
		year = '2003';
		days = NormalYr;
	}
	else if (val.charAt(2)=='E') // For 2004
	{
		year = '2004';
		days = LeapYr;
	}
	else if (val.charAt(2)=='F') // For 2005
	{
		year = '2005';
		days = NormalYr;
	}
	else if (val.charAt(2)=='G') // For 2006
	{
		year = '2006';
		days = NormalYr;
	}
	else if (val.charAt(2)=='H') // For 2007
	{
		year = '2007';
		days = NormalYr;
	}
	else if (val.charAt(2)=='J') // For 2008
	{
		year = '2008';
		days = LeapYr;
	}
	else if (val.charAt(2)=='K') // For 2009
	{
		year = '2009';
		days = NormalYr;
	}
	else if (val.charAt(2)=='L') // For 2010
	{
		year = '2010';
		days = NormalYr;
	}

	for (var i=0;i<days.length;i++ )
	{
		cal = cal + days[i];
		if (cal>=num)
		{
			cal1=cal-days[i];
			rem = num-cal1;
			if (rem<10)
			{
				rem = "0"+rem;
			}
			month = i+1;
			if (month<10)
			{
				month="0"+month;
			}
			date = month+"/"+rem+"/"+year;
			break;
		}
	}
	return date;
}

function fnSubmit()
{
	var vWO = '';
	var vQty = 0;
	var vCntNum = 0;

	var str = '';
	var obj = '';
	var vPNum = '';
	var vManfDate = '';

	hcnt = parseInt(document.frmOrder.hSubCnt.value);

	for (var i=0;i<hcnt;i++)
	{
		val = eval("document.frmOrder.Txt_CNum"+i);
		if (val)
		{
			vCntNum = val.value;
			if (vCntNum != '')
			{
				val = eval("document.frmOrder.hSubComp"+i);
				vPNum = val.value;
				val = eval("document.frmOrder.hSubDHR"+i);
				vSubDhrId = val.value;
				val = eval("document.frmOrder.Txt_CNum"+i);
				vCntNum = val.value;
				
				if ( vCntNum == 'NOC#')
				{
					vManfDate = gStrDate;
				}
				else
				{
					vManfDate = fnCalDate(vCntNum);
				}
				str = str + vPNum +"^"+ vSubDhrId +"^"+ vCntNum +"^"+ vManfDate +"|";
			}
		}
	}
	//document.frmOrder.hInputString.value = str.substring(0,str.length-1);
	document.frmOrder.hInputString.value = str;
	//alert("STRING: "+document.frmOrder.hInputString.value);
	document.frmOrder.action= gStrServletPath + "/GmDHRUpdateServlet?companyInfo="+companyInfoObj;
	fnStartProgress('Y');
	document.frmOrder.submit();
}

function fnFetchDHR()
{
	fnValidateTxtFld('Txt_DHRID',lblDHRId);
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	var dhrId = document.frmOrder.Txt_DHRID.value;
	var validDhrId;
	//dhrId="GM-DHR-GM-DHR-45117+";
	//alert("dhrId.substring(7, 14) is : "+ dhrId.substring(7, 14));
		if(dhrId.substring(7, 14)=='GM-DHR-'){
	//alert("dhrId.substring(7,dhr.length):  " + dhrId.substring(7,dhrId.length));
	validDhrId =dhrId.substring(7,dhrId.length);
	}
	else{
		validDhrId =dhrId;
	}
	document.frmOrder.hId.value = validDhrId.replace("+", "");
	document.frmOrder.hFrom.value = "DashboardDHR";
	document.frmOrder.hAction.value = "UpdateDHR";
	document.frmOrder.action = "GmPOReceiveServlet";
	fnStartProgress('Y');
	document.frmOrder.submit();
}

function fnShowFilters(val)
{
	var obj = eval("document.all."+val);
	var obj1 = eval("document.all."+val+"img");
	if (obj.style.display == 'none')
	{
		obj.style.display = 'block';
		if(obj1){
			obj1.src = '/images/minus.gif';
		}
	}
	else
	{
		obj.style.display = 'none';	
		if(obj1){
			obj1.src = '/images/plus.gif';
		}
	}
}

function fnFocusDHRID()
{
	var elem = document.frmOrder.Txt_DHRID;
	if(elem != null)
	{
        if(elem.createTextRange)
		{
			var range = elem.createTextRange();
            range.move('character', 15);
            range.select();
        }
        else 
		{
            if(elem.selectionStart) 
			{
                elem.focus();
				elem.setSelectionRange(15,15);
            }
            else
                elem.focus();
        }
    }  
    
}

function fnOnPageLoad()
{
	var count = 0;
	var totQty;
	objAction = document.frmOrder.hAction.value;
	document.frmOrder.Txt_DHRID.focus();
	if(objAction == 'V' || objAction == '')
    {
		if(objGridData.indexOf("cell", 0)==-1){
			  return true;
		}
		gridObj = initGrid('dataGridDiv',objGridData);
		//gridObj.enableTooltips("false,false,false,false");
		gridObj.setEditable(false);
		
		//Get the no. of inhouse transactions created
		gridObj.forEachRow(function(rowId){
 			var txnId = gridObj.cellById(rowId,2).getValue();
			if(txnId != ''){
				count++;
			}
 		});
		
		var qtyrec = parseInt(document.frmOrder.hRecQty.value);

		var qtyrej = parseInt(document.frmOrder.hRejQty.value);

		var qtyclose = parseInt(document.frmOrder.hCloseQty.value);
		
		//gridObj.sortRows(0,'str','asc');
		//gridObj.attachEvent("onEditCell",doOnCellEdit);
		if(document.frmOrder.Txt_Qty != undefined)
		{
			document.frmOrder.Txt_Qty.disabled=false;
		}
		totQty = eval(qtyrec)-eval(qtyrej)+eval(qtyclose); // get the qty that needs to be processed
		document.frmOrder.Txt_Qty.value= totQty;

		if(gridObj != '')
    	{
			qtyremains = document.getElementById("qty_remains");
    	}
		var	InspectedBY = gStrInspectedBy;
		var	PackedBY = gStrPackagedBy;
		var	VerifiedBy = gStrUserId;
		if ((InspectedBY ==	VerifiedBy) || (PackedBY == VerifiedBy)) {
			document.frmOrder.Btn_Update.disabled=true;
		}
		
		// if there is only one inhouse transaction created and if received qty generated is same as the qty while processing , no need to show the "Assign Lot#" button
		if(totQty == qtyrec && count == 1){
			document.frmOrder.Btn_AssignLotNum.style.visibility='hidden';
		}
		
    }else if(objAction == 'P' && (gStrNCMRFl != '1' || gStrNCMRFl == '0')){
    	if(objGridData.indexOf("cell", 0)==-1){
			  return true;
		}
    	gridObj = setDefalutGridProperties('dataGridDiv');    	
    	gridObj.enableEditTabOnly(true);    	    	 
    	initializeDefaultGrid(gridObj,objGridData);
    	var focusCell = 0;
    	gridObj.enableEditEvents(false, true, true); 
    	gridObj.enableLightMouseNavigation(true);
    	if(gstrPartClass == '4031' && gstrPoType== '3101' && gstrControlNum.toUpperCase()=="NOC#"){
			if(qtyPackages > 0 ){
				gridObj.cellById(100066, 1).setValue(parseInt(qtyPackages)+parseInt(gStrBQtyPack));
				focusCell = 2;
			}
    	}else{
    		gridObj.cellById(100067, 1).setValue(qtyPackages);	
    		gridObj.cellById(100065, 1).setValue(gStrBQtyPack);
    		gridObj.cellById(100069, 1).setValue(gstrRMQtyPack);
    	}
    	gridObj.selectCell(focusCell,1);
		 gridObj.editCell();
		 gridObj.attachEvent("onEditCell", function(stage, rowId, cellInd) {
	    	    if (stage == 0) {
	    	        //User starting cell editing: row id is" + rowId + ", cell index is " + cellInd
	    	    } else if (stage == 1) {
	    	        //Cell editor opened
	    	    	
	    	    } else if (stage == 2) {
	    	        //Cell editor closed
	    	    	var strCellValue=gridObj.cellById(rowId, cellInd).getValue();
	    	    	/*if(!IsNumeric(strCellValue)){
	    	    		Error_Details(message[46] + '<b>' + gridObj.cellById(rowId, 0).getValue()+'</b>');
	    	    	}*/
	    	    	if (ErrorCount > 0)
	    	    	{
	    	    		gridObj.selectCell(rowId,cellInd);
	    	    		gridObj.editCell();	    	    	
	    	    		gridObj.cellById(rowId, cellInd).setValue('');
	    	    		Error_Show();
	    	    		Error_Clear();
	    	    		return false;
	    	    	}

	    	    }
	    	    
	    	    return true;
	    	});
		
    }else{
    	if(objGridData.indexOf("cell", 0)==-1){
			  return true;
		}
		gridObj = initGrid('dataGridDiv',objGridData);
		//gridObj.enableTooltips("false,false,false,false");
		gridObj.setEditable(false);
    }
}
function IsNumeric(strString) //  check for valid numeric strings	
{
	if(!/\D/.test(strString)) return true;//IF NUMBER
	else if(/^\d+\.\d+$/.test(strString)) return true;//IF A DECIMAL NUMBER HAVING AN INTEGER ON EITHER SIDE OF THE DOT(.)
	else return false;
}

function doOnCellEdit(stage, rowId, cellInd, nValue, oValue) { 
	if(document.frmOrder.Txt_Qty == undefined)
	{
		return false;
	}
	var vrfnQty = document.frmOrder.Txt_Qty.value
	var newQty;
	var updatedQty=0;
	if(cellInd == 2 && stage == 1)
	{
		var c=this.editor.obj;			
		if(c!=null) c.select();
		return true;
	}
	if(stage == 2)
	{
		var gridrows =gridObj.getAllRowIds(",");
		qtyremains = document.getElementById("qty_remains");
		if(gridrows.length>0){
			var gridrowid;
			var gridrowsarr = gridrows.toString().split(",");
			var columnCount = gridObj.getColumnsNum();
	
			 for (var rowid = 0; rowid < gridrowsarr.length; rowid++) {
					gridrowid = gridrowsarr[rowid];
					newQty = gridObj.cellById(gridrowid, 2).getValue();					
					updatedQty = parseInt(updatedQty) + parseInt(newQty);					
			 }
		}
		qty_rem = vrfnQty - updatedQty;
		qtyremains.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;'+ qty_rem;
	}
	if(gridObj.cellById(rowId, 2).getValue() == '' || gridObj.cellById(rowId, 2).getValue() < 0)
	{
		gridObj.cellById(rowId, 2).setValue(0);
	}
	return true;
}

//To open the popup of donor details when click on "Assign Lot#"
function fnAssignLotNum(){
	windowOpener("/gmDonorInfo.do?companyInfo="+companyInfoObj+"&method=fetchDHRdetail&strOpt=load&hDhrId="+gStrDHRId+"&hMode=Report&hVendorId="+gStrVendorId,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
}

//to call the report when click on "D" icon
function fnCallDonor(dhrId, vendorId, status){
	// DHR status  3: Pending Verification, 4: Verified
	if(status > 2)
		{
			fnAssignLotNum();		
		}
	else
		{
			windowOpener("/gmDonorInfo.do?companyInfo="+companyInfoObj+"&method=fetchDonorInfo&strOpt=load&hDhrId="+dhrId+"&hMode=Report&hVendorId="+vendorId,"PrntInv","resizable=yes,scrollbars=yes,top=200,left=300,width=585,height=700");
		}
}