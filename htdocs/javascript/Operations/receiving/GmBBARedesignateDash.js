
function fnOnload(){
	if (objGridData != '') {
		gridObj = initGridData1('recdashRpt', objGridData);
		 
    }
}
function initGridData1(divRef,gridData)
{
	var gObj = new dhtmlXGridObject(divRef);
		gObj.setSkin("dhx_skyblue");
		gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
		gObj.enableTooltips("true,true,true,true,true,true");
		gObj.init();
		gObj.loadXMLString(gridData);
		gObj.attachHeader("#text_filter,#text_filter,#numeric_filter,#text_filter");
	return gObj;	
}

//this function is used to Pending Shipping Verification
function fnCallItemsShip(val, type, conType) {
	var statuflg='';
	document.frmBBARedesignatedash.hId.value = val;
	document.frmBBARedesignatedash.hFrom.value = "LoadItemShip";
	document.frmBBARedesignatedash.hConsignId.value = val;
	document.frmBBARedesignatedash.hAction.value = "LoadItemShip";
	document.frmBBARedesignatedash.hMode.value = "VERIFY";
	document.frmBBARedesignatedash.hType.value = type;
	document.frmBBARedesignatedash.hOpt.value = type;
//if transcation is part re-designation transaction (PTRD)
	if(conType == 1){
		targetUrlStr = "/gmPartLabel.do?method=fetchPartLabelDetail&txnid="+val+"&action="+conType+"&txntype=" + type+"&hAction=PI";
	}else if(conType == 2){ // if transcation status is 2(pending process) 
		targetUrlStr = "/gmPartLabel.do?method=fetchPartLabelDetail&txnid="+val+"&hAction=PP&txntype=" + type+"&statuflg=" + conType;
	}else if(conType == 3){ // if transcation status is 2(pending process) 
		targetUrlStr = "/gmPartLabel.do?method=fetchPartLabelDetail&txnid="+val+"&hAction=PV&txntype=" + type+"&statuflg=" + conType;
	}else if (conType == 'T') {
		urlStr = "&txnid=" + val
				+ "&mode=VERIFY&haction=EditVerify&hConsignId=" + val
				+ "&txntype=" + type + "&loctype=93345";
		document.frmBBARedesignatedash.action = "/gmItemVerify.do?";
		targetUrlStr = document.frmBBARedesignatedash.action + urlStr;
	} else {
		document.frmBBARedesignatedash.action = "GmReprocessMaterialServlet";
		document.frmBBARedesignatedash.submit();
		return false;
	}
	
	parentLcn = parent.location.href;

	if (parentLcn.indexOf("GmEnterprisePortal") != -1) {
		fnLocationHref(targetUrlStr);
	} 

    else if (parentLcn.indexOf("GmGlobusOnePortal") != -1) {
			fnLocationHref(targetUrlStr);
		} 
	
	else {
		fnLocationHref(targetUrlStr, parent);
	}

}
//This function used to down load the excel file
function fnDownloadXLS() {
	gridObj.toExcel('/phpapp/excel/generate.php');
}

function fnPicSlip(val)
{
	windowOpener("/gmPartredesignation.do?&companyInfo="+compInfObj+"&method=generatePicSlip&strTxnIds="+val+"&txntype=103932&ruleSource=103932&refId="+val,"PTRTPicSlip","resizable=yes,scrollbars=yes,top=150,left=150,width=750,height=500");
}

