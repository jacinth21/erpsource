
var scannedLotCnt = 0;
var btnRelFl = false;

function fnOnScanLoad(){
	document.getElementById("scanLotCode").value = '';
	document.getElementById("scanLotCode").focus();
	document.all.Btn_Release.disabled = true;
}

//To change the values inside the field to uppercase
function fnUpperCase(obj){
	obj.value = obj.value.toUpperCase();
}

//To validate the Lot Details
function fnValidateLotCode(obj,gridObj){
	var scanedval = TRIM(obj.value);
	var varLotColInd = gridObj.getColIndexById("LotId");
	var varImgInd = gridObj.getColIndexById("ChkBoxRefId");
	var varPartInd = gridObj.getColIndexById("partNum");
	var lotTotalcount=gridObj.getRowsNum();
	var lotcode;
	var lotExistFl = false;
	var indexof = '0';
	var cntlindexof = '0';
	var scanedPart = '';
	var partNum = '';
	
	indexof = scanedval.indexOf("^"); // get the index of '~'
	cntlindexof = scanedval.indexOf("(10)"); // get the index of '(10)'
	
	if(indexof != -1){// While scanning only format will be "partno^controlno"
		var cnumarr= scanedval.split("^");
		scanedPart = cnumarr[0]; // get the characters before '^'
		scanedval = cnumarr[1]; //Getting the remaining character, after "^"
	}else if(cntlindexof != -1){// While scanning only format will be "(01)udi number(17)exp date(10)controlno"		
		var cnumArray = scanedval.split("(10)");
		scanedval = cnumArray[1];	// //Getting the remaining character, after "(10)"		
	}
	scanedval = scanedval.toUpperCase();
	
	/* Description for below validation: Error thorows if LotCode text box is empty and ReleaseShipment button is disable 
	 */
	if(scanedval == '' && !btnRelFl){
		Error_Details(message_operations[107]);
		fngenerateDivError();
        return false;
	}
	
	if(scanedval != ''){
		gridObj.forEachRow(function(rowId){
			lotcode = gridObj.cellById(rowId, varLotColInd).getValue();
			partNum = gridObj.cellById(rowId, varPartInd).getValue();
			
			if(scanedval == lotcode){
				/* Description for below validation: Error throws if Scanned part and parts in grid are not matched
				 */
				lotExistFl = true;
				if(scanedPart != '' && scanedPart != partNum){
					var array = [scanedPart,scanedval]
					Error_Details(Error_Details_Trans(message_operations[108],array));
					fngenerateDivError()
			        return false;
				}
				/* Description for below validation: Error throws if the check image is already scanned
				 */
				if(gridObj.cellById(rowId, varImgInd).getValue() != ''){
					Error_Details(Error_Details_Trans(message_operations[109],scanedval));
					fngenerateDivError()
			        return false;
				}
				document.all.errormessage.innerHTML = '';
				gridObj.cellById(rowId, varImgInd).setValue('<img src="/images/success.gif" height="17" width="18" alt="Success" border="0">');
				scannedLotCnt++;
				//fnOnScanLoad();
				if(scannedLotCnt == lotTotalcount){
					fnEnableRelButton('true');
				}
			}
			/* Description for below validation: Error throws if the Scanned Lot is not available in Grid
			 */
			if(!lotExistFl){
				Error_Details(Error_Details_Trans(message_operations[110],scanedval));
				fngenerateDivError()
		        return false;
			}
		});
	}
	//Call submit function if the scanned value is empty
	if(scanedval == ''){
		//Call the function to submit the screen
		fnCallSubmit('scanLotCode');
	}
	document.getElementById("scanLotCode").value = '';
	document.getElementById("scanLotCode").focus();
}

//Function to show the error details in the Scan Lot Code section
function fngenerateDivError(errStatus){
	var messgaeColor = "red";
	var vCode = "";
	for(var i = 0 ; i <= (ErrorCount-1); i++){		
		fnPlayErrorSound('beep');
		vCode = vCode + "<TR> <TD align=left> <font color = '"+messgaeColor+"'>"+ ErrorDetail[i][0] + "</font></TD>" ;
		vCode = vCode + "</TR>";
		vCode = vCode + "<tr>";
		vCode = vCode + "</tr>";
	}
	objError = document.all.errormessage;
	objError.innerHTML = vCode;
	document.getElementById("scanLotCode").value = '';
	document.getElementById("scanLotCode").focus();
	Error_Clear();
	return false;
}

//Function to mark all the lot number as checked and to enable release shipment button
function fnEnableButton(obj){
	var checkedFl = obj.checked;
	if(checkedFl){
		fnShowCheckMark();
	}
	else{
		fnHideCheckMark();
	}
	
	fnEnableRelButton(obj);
}

// To show all the check marks when we click on Master checkbox
function fnShowCheckMark(){
	gridObj.forEachRow(function(rowId){
		gridObj.cellById(rowId, 0).setValue('<img src="/images/success.gif" height="17" width="18" alt="Success" border="0">');
	});
}

//To hide all the check marks when we uncheck the Master checkbox
function fnHideCheckMark(){
	gridObj.forEachRow(function(rowId){
		gridObj.cellById(rowId, 0).setValue('');
	});
}

//Function to Enable the Release shipment button only if  all lot's are checked in Process tab and all the parameters should checked in Parameter Tab.
function fnEnableRelButton(obj){
	var checkedFl = (obj.value)? obj.checked: obj;
	
	if (document.frmPOBulkReceive.strAllChecked) {
		allParameterCkd = document.frmPOBulkReceive.strAllChecked.value;
	}
	if(checkedFl && allParameterCkd == 'true' && (SwitchUserFl =='Y' || SwitchUserFl == '')){
		btnRelFl = true;
		document.all.Btn_Release.disabled = false;
	}else{
		btnRelFl = false;
		document.all.Btn_Release.disabled = true;
	}
}

function fnCallSubmit(val){
	if(event.keyCode == 13){
		var actElement = document.activeElement.id;
		/*
		 * Call the submit function on pressing enter key based on below conditions
		 * 1. The active element should be scan lot code field and the function is calling from fnValidateLotCode()
		 * 2. The active element should not be scan lot code field and RS id field and if we press the enter key
		 */
		if((actElement == 'scanLotCode' && val == 'scanLotCode') 
					|| (actElement != 'scanLotCode' && actElement != 'rsNumber' && val == 'enter')){
			
			if (document.frmPOBulkReceive.strAllChecked) {
				allParameterCkd = document.frmPOBulkReceive.strAllChecked.value;
			}
			
			/* Need to submit the form while clicking enter key for the below conditions
			 * 1. The value in the lot code field should be empty
			 * 2. The release for shipment button should be enabled
			 * 3. All the parameters in parameter tab should be checked
			 * 4. The screen is not opened for a switch user
			 * 5. The status should be 3(Pending verification)
			 */
			if(btnRelFl && allParameterCkd == 'true' && (SwitchUserFl =='Y' || SwitchUserFl == '')){
				fnReleaseShipment(document.frmPOBulkReceive);
			}
		}
	}
}

function fnPlayErrorSound(soundObj) {
	try{ 
		  var sound = document.getElementById(soundObj);
		   
		   if (sound)
				 sound.Play();
		 }
	catch (err)
		{
		 return null;
		} 

}
 

