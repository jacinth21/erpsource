var vprodclass='';
var errCnt=0;
var errVendorMsg = '';
var donorFlag='Y';
var validPartNo = true;

function fnOnLoad(){
	if(document.frmPartControlSetup.partNM != undefined && document.frmPartControlSetup.partNM != null){
	   document.frmPartControlSetup.partNM.focus();
	}
	if( prodType == '100845' || prodClass == '4030'){
		document.getElementById('expDateCal').disabled = false; 
		document.frmPartControlSetup.expDate.disabled=false;	
	  }else{
		document.getElementById('expDateCal').disabled = true; 
		document.frmPartControlSetup.expDate.disabled=true;
	}
	if( prodType == '100845'){
		document.getElementById('revLevel').disabled = true;	
	  }else{	
		document.getElementById('revLevel').disabled = false;
	}
	if (disableText == 'Y'){
		document.getElementById('controlNM').disabled = true;
		document.getElementById('revLevel').disabled = true;	
		document.getElementById('donorNM').disabled = true;	
	}
	
}

// function to open popup when click on "D" icon
function fnCallDonor(){
     var donorNum = document.frmPartControlSetup.donorNM.value;
          fnValidateTxtFld('donorNM',donorNum);
          if(ErrorCount>0){
              Error_Show();
             Error_Clear();
             return false;
          }
          var vendorId = document.frmPartControlSetup.vendorId.value;      	
          document.frmPartControlSetup.donorNM.focus();
          var screeType = document.frmPartControlSetup.hScreenType.value;
          
          windowOpener("/gmDonorInfo.do?companyInfo="+companyInfoObj+"&method=fetchDonorParamDtls&strOpt=load&screenType="+screeType+"&FORMNAME=frmDonorInfo&hDonorNum="+donorNum+"&hVendorId="+vendorId,"PrntInv","resizable=yes,scrollbars=yes,top=300,left=550,width=585,height=350");
}

function fnSubmit(){
    
       
    var vCNum = document.frmPartControlSetup.controlNM.value; 
    var vPNum = document.frmPartControlSetup.partNM.value;
    var vManfDate = '';
    var vExpDate = '';
    var LotCode = '';
    var errMgs = '';
    errVendorMsg = '';
    var errCtrlNm = '';
    var errExpDt = '';
    var errPstExpDt = '';
    var vprodtype = document.frmPartControlSetup.prodType.value;
    var errDonorNm  = '';
    var errManfDt = '';
    var errPstManfDt = '';
    var strScreenType = document.frmPartControlSetup.hScreenType.value;
    var ajaxPartType = document.frmPartControlSetup.hPartType.value;

 	//PC-4543 Upper Case Conversion Issue
	if(document.frmPartControlSetup.partNM != undefined){
    	if(document.frmPartControlSetup.partNM.value != undefined && document.frmPartControlSetup.partNM.value != ""){
    	document.frmPartControlSetup.partNM.value = TRIM(document.frmPartControlSetup.partNM.value.toUpperCase());
    	}
    }
    if(vprodtype == '100845' || ajaxPartType == '100845'){
   	 var donarNumber=document.frmPartControlSetup.donorNM.value;
   	 if(donarNumber== ''){
        Error_Details(message_operations[57]);
   	 }
    }
    val = eval(document.frmPartControlSetup.manfDate);
    vManfDate = val.value == null ? "" : val.value.trim(); 
    if(vCNum == '' && errCtrlNm.indexOf(vPNum)==-1 && vprodtype != '100845'){
    //control number validation should not show for tissue parts; 100845: tissue
    errCtrlNm = errCtrlNm+vPNum+",";
    }
    // Validate Expiry date for Sterile Parts   
    if(document.getElementById('expDate').disabled != true){
    	
    val = eval(document.frmPartControlSetup.expDate);
        vExpDate = val.value == null ? "" : val.value.trim();  
        if(vExpDate == '' && errExpDt.indexOf(vPNum)==-1){
          errExpDt = errExpDt+vPNum+",";
        }                                
     }

    // Validate Manufactured date  for Sterile Parts   
    if(document.getElementById('manfDate').disabled != true){
    val = eval(document.frmPartControlSetup.manfDate);
        vmanfDate = val.value == null ? "" : val.value.trim();  
        if(vmanfDate == '' && errManfDt.indexOf(vPNum)==-1){
        	errManfDt = errManfDt+vPNum+",";
        }                                
        // Manufatured date cannot be a Futured date ( Future Date)
        if(dateDiff(currDate,document.getElementById('manfDate').value,format)>0){
          //Manufatured date cannot be a Futured date.
            if(errPstManfDt.indexOf(vPNum)==-1){
            	errPstManfDt = errPstManfDt+vPNum+",";
            }
         }
    }

     if( vprodtype == '100845'){
    	 var donarNumber=document.frmPartControlSetup.donorNM.value;
    	 if(!donorNumExistFl && donorNM != donarNumber){
          Error_Details(message_operations[58]);
    	}
     }
     vPNum
    if(vPNum == ''){
    	Error_Details(message_operations[59]+errCtrlNm.substring(0,errCtrlNm.length-1));
    }
    if(!validPartNo){
    	Error_Details(message_operations[60]+vPNum);
    }
    if(vCNum == 'NOC#'){
    	Error_Details(message_operations[61]);
     }
    if(errCtrlNm != '' || vCNum == ''){
    Error_Details(message_operations[62]+errCtrlNm.substring(0,errCtrlNm.length-1));
    }
    if(errExpDt != ''){
        Error_Details(message_operations[63]+errExpDt.substring(0,errExpDt.length-1));
    }
    if(errPstManfDt!=''){
        Error_Details(message_operations[65]+errPstManfDt.substring(0,errPstManfDt.length-1));
    }    
    fnValidateTxtFld('txt_LogReason',message_operations[552]);
    // Display error messages if exist
     if(ErrorCount > 0){
         Error_Show();
         Error_Clear();
         return false;
     }
     document.frmPartControlSetup.partCNId.value=partCnId;
     document.frmPartControlSetup.udi.disabled=false;
     var udiNumber=document.frmPartControlSetup.udiNo.value;
     var udiNum = document.frmPartControlSetup.udi.value;  
     if(udiNumber != udiNum ){
    	 var UDI_LOG_ENTRY =  document.frmPartControlSetup.udilog.value;
     }
     else if(udiNumber == udiNum)
    	 {
    	 var UDI_LOG_ENTRY = "";
    	 }
     
    document.frmPartControlSetup.udiNo.value = udiNum;
    var actionURL = "/gmPartControlSetup.do?companyInfo="+companyInfoObj+"&method=savePartControlInfo&UDI_LOG_ENTRY="+UDI_LOG_ENTRY;
    document.frmPartControlSetup.action= actionURL;
    fnStartProgress('Y');
    document.frmPartControlSetup.donorNM.disabled=false;
    document.frmPartControlSetup.controlNM.disabled=false;
    document.frmPartControlSetup.revLevel.disabled=false;
    document.frmPartControlSetup.submit();
}
// To Reset all the fields
 function fnReset(){	 
	 document.frmPartControlSetup.controlNM.value = ''; 
	 document.frmPartControlSetup.partNM.value = '';
	 document.frmPartControlSetup.revLevel.value = '';
	 document.frmPartControlSetup.udi.value = '';
	 document.frmPartControlSetup.donorNM.value = '';
	 document.frmPartControlSetup.expDate.value = '';
	 document.frmPartControlSetup.manfDate.value = '';
	 document.frmPartControlSetup.customSize.value = '';
	 document.frmPartControlSetup.TXT_LOGREASON.value = '';
}

//To clear the image
function fnClearDiv(){
    document.getElementById("DivShowDonorIDExists").style.display='none';
    document.getElementById("DivShowDonorIDNotExists").style.display='none';     
}

//To take last digit from the Lot number textbox if the Rev Textbox is Empty
function fnGetRevNumber(){
	var Rev = document.frmPartControlSetup.revLevel.value ;
	var vprodtype = document.frmPartControlSetup.prodType.value;
	  
	if(Rev == '' && vprodtype != '100845'){
	var lotNumber =  document.getElementById("controlNM").value;
	var lotNoLength = lotNumber.length;
	var lastChar=lotNumber.charAt(lotNumber.length-1);
	document.frmPartControlSetup.revLevel.value = lastChar;	
	}
}

//To check whether the Part Number is already exist or not if request is coming from LEFTLINK
function fnPartNumber(){
	 var partNM = document.frmPartControlSetup.partNM.value.toUpperCase(); //PC-4543 Upper Case Conversion Issue
	 fnClearDiv();
	    if(TRIM(partNM) == ''){
	         validMessage = '';
	         return false;
	    }
	    validMessage = '';
	    dhtmlxAjax.get('/gmPartControlSetup.do?companyInfo='+companyInfoObj+'&method=fetchPartNumber&partNM='+encodeURIComponent(partNM)+ '&ramdomId=' + Math.random(),function(loader){
	    	  var response = loader.xmlDoc.responseText;
	    	  parseMessage(response);
	     });		    	  	                     
}
//To assign the values getting from ajax call to the corresponding fields
function parseMessage(xmlData) 
{
	
	if(xmlData == '' || xmlData == undefined){
        
        donorNumExistFl = false;
        validPartNo = false;
        document.getElementById("DivShowPartNoIDExists").style.display = 'none';
        document.getElementById("DivShowPartNoIDNotExists").style.display = 'inline';
        return false;
      }else{
         donorNumExistFl  = true;
         validPartNo = true;
         document.getElementById("DivShowPartNoIDExists").style.display = 'inline';
         document.getElementById("DivShowPartNoIDNotExists").style.display = 'none';
      }
      var xmlDataArr=xmlData.split('~');
      var pnum = xmlDataArr[0];
      if(savedUdi != ''){
    	  document.frmPartControlSetup.udiNo.value = savedUdi;
    	  document.frmPartControlSetup.udi.value = savedUdi;
      }else{
    	  document.frmPartControlSetup.udiNo.value = xmlDataArr[1];
    	  document.frmPartControlSetup.udi.value = xmlDataArr[1];
      }   

      
      var show_txt = xmlDataArr[2];
	   document.frmPartControlSetup.show_Txtbox.value = show_txt;
	  
	   if(show_txt =='N'){
	       	   	document.frmPartControlSetup.udi.disabled=true;
	         }else{
	        	 document.frmPartControlSetup.udi.disabled=false;

	         }
	   
	    var  prodType = xmlDataArr[3];
	    var prodClass= xmlDataArr[4];
	   if( prodType == '100845' || prodClass == '4030'){		
		document.getElementById('expDateCal').disabled = false; 
		document.frmPartControlSetup.expDate.disabled=false;		
      }else{    	
    	document.getElementById('expDateCal').disabled = true; 
    	document.frmPartControlSetup.expDate.disabled=true;    	
    }
      document.frmPartControlSetup.hPartType.value = prodType;
      document.frmPartControlSetup.prodType.value = prodType;
      if(prodType == '100845') {
    	  document.getElementById('revLevel').disabled = true;  
    	  var objTissue = document.getElementById('donorNM');
  		objTissue.disabled = '';
  		document.getElementById('DonorIcon').style.display = 'block';
      }else{
    	  document.getElementById('DonorIcon').style.display = 'none';
    	  document.getElementById('revLevel').disabled = false; 
      }

      
      document.frmPartControlSetup.prodClass.value = xmlDataArr[4];
      document.frmPartControlSetup.udiFormat.value = xmlDataArr[5];
      document.frmPartControlSetup.diNum.value = xmlDataArr[6];
    
}


var validMessage;
var donorNumExistFl = false;

// To check whether the donor number already exist or not
function fnCheckDonorNum(donorObj){  
    var donorNum = donorObj.value;    
     var vendorId = document.frmPartControlSetup.vendorId.value; 
     fnClearDiv();      
    if(TRIM(donorNum) == ''){
         validMessage = '';
         return false;
    }
    validMessage = '';
    dhtmlxAjax.get('/gmPOBulkReceive.do?companyInfo='+companyInfoObj+'&method=validateDonorNum&donorNum='+donorNum+"&vid="+vendorId+ '&ramdomId=' + Math.random(),function(loader){
         var response = loader.xmlDoc.responseText;         
         if(response == '0'){// Count '0' means, donor number is not existing
             donorNumExistFl = false;
               document.getElementById("DivShowDonorIDExists").style.display = 'none';
         document.getElementById("DivShowDonorIDNotExists").style.display = 'inline';
         }else{
             donorNumExistFl  = true;
             document.getElementById("DivShowDonorIDExists").style.display = 'inline';
            document.getElementById("DivShowDonorIDNotExists").style.display = 'none';
         }
    }); 
 }

//Function to set the value for UDI column in PO Receive Screen.
//This format is like (01)UDI NUMBER(17)EXPIRY DATE(10)CONTROL NUMBER
function fnAddUDIDetails(){

	var udiFormat =document.frmPartControlSetup.udiFormat.value;
	var cnvlaue = document.frmPartControlSetup.controlNM.value;
	var diVal = document.frmPartControlSetup.diNum.value;
	var expDate = document.frmPartControlSetup.expDate.value;
	var DonorNumvalue =document.frmPartControlSetup.donorNM.value;
	var manfDate = document.frmPartControlSetup.manfDate.value;
    var udiVal =document.frmPartControlSetup.udiNo.value;
    var udiValue;    
    if(savedUdi != '' && savedControlnum == cnvlaue  ){			
		 
    	 document.frmPartControlSetup.udi.value = savedUdi;
		 document.frmPartControlSetup.udiNo.value = savedUdi;
		 return false;
	}

		if(udiVal != ''){	
			
			 if(udiFormat != '')
			 {
				 udiValue = fnUDINumber(udiFormat, diVal, cnvlaue, expDate, DonorNumvalue, manfDate );				 
			  }
			  else
			  {
				  udiValue = udiVal;				
			  }
			 /*document.frmPartControlSetup.udi.disabled=false;*/
			 document.frmPartControlSetup.udi.value = udiValue;
			 document.frmPartControlSetup.udiNo.value = udiValue;
			 
		}			
}

var str = '';	 
var strMMYYExpDT = '';
var strYYMMDDExpDT = ''; 
var yearYYYYExpDT='';
var yearYYExpDT='';
var yearYYYExpDT='';
var monthExpDT='';    
var dayExpDT='';    
var strExpDT = '';
var strManfDT = '';
var strYYYJJJExpDt = '';
var strYYYJJJManufDT = '';

var yearYYManfDT='';
var yearYYYManfDT ='';
var monthManfDT='';    
var dayManfDT='';

var strMMDDYYExpDT = ''; 
var strYYJJJExpDT = ''; 
var strYYYJJJExpDT = '';
var jilianJJJExpDT = '';
var jiliandayExpDT = '';
var jilianJJJManfDT = '';
var jiliandayManfDT = '';
//this function used to split the expiry date with different format for year , month and day
function fnExpdate(expDate)
{
	for(i=0;i<expDate.length;i++){
	   var char = expDate.charAt(i);	
	   if(!isNaN(char))
	   strExpDT = strExpDT+char;	
	  }
	yearYYExpDT =  strExpDT.substr(6,2);
	yearYYYExpDT = strExpDT.substr(5,3);
	monthExpDT = strExpDT.substr(0,2);
	dayExpDT = strExpDT.substr(2,2);

	yearYYYYExpDT = strExpDT.substr(4,4); 
	strMMYYExpDT	= monthExpDT + yearYYExpDT ;
	strMMDDYYExpDT = monthExpDT + dayExpDT + yearYYExpDT; 
	strYYMMDDExpDT  = yearYYExpDT + monthExpDT + dayExpDT; 
	strYYYJJJExpDt = yearYYYExpDT; 
}

//this function used to split the manufacturing date with different format for year , month and day
function fnManfdate(manfDate)
{
	for(i=0;i<manfDate.length;i++){
	   var char = manfDate.charAt(i);	
	   if(!isNaN(char))
	   strManfDT = strManfDT+char;	
	  } 

	yearYYManfDT =  strManfDT.substr(6,2);	 
	yearYYYManfDT =  strManfDT.substr(5,3);
	monthManfDT = strManfDT.substr(0,2);
	dayManfDT=  strManfDT.substr(2,2);
	yearYYYYmanfDT = strManfDT.substr(4,4); 
	strYYMMDDManfDT  = yearYYManfDT  + monthManfDT  + dayManfDT;  
	strYYYYMMDDManfDT  = yearYYYYmanfDT  + monthManfDT  + dayManfDT;
	strYYYManfDT = yearYYYManfDT;
}
//Function to convert the UDI based on UDI fornmat.  
function fnUDINumber(format, diVal, cnvlaue, expDate, DonorNumvalue, manfDate) { 
      
       strMMYYExpDT = '';
       strYYMMDDExpDT = ''; 
       yearYYYYExpDT='';
       yearYYExpDT='';
       yearYYYExpDT = '';
       monthExpDT='';    
       dayExpDT='';    
       strExpDT = '';
       strManfDT = '';

       yearYYManfDT='';
       yearYYYManfDT='';
       monthManfDT='';    
       dayManfDT='';

       strMMDDYYExpDT = ''; 
       strYYJJJExpDT = ''; 
       strYYYJJJExpDT = '';
       strYYYJJJManufDT = '';
       jilianJJJExpDT = '';
       jiliandayExpDT = '';
       jilianJJJManfDT = '';
       jiliandayManfDT = '';
       str = '';
       var dateObjec = new Date();
       var currentHour = dateObjec.getHours();
     fnExpdate(expDate);     

     fnManfdate(manfDate);  
     
     // if expiry date is not empty , convert it to Julian date format YYJJJ
     if (expDate != '')
     {
        jilianJJJExpDT = ConvertToJulian(parseInt(yearYYYYExpDT), parseInt(monthExpDT), parseInt(dayExpDT)) ;      
      
        jiliandayExpDT  = jilianJJJExpDT.toString().substr(4,3); 
     
        strYYJJJExpDT = yearYYExpDT + jiliandayExpDT; 
        strYYYJJJExpDT = yearYYYExpDT + jiliandayExpDT;
     }
  // if Manf. date is not empty, convert it to Julian date format
     if(manfDate !=''){
            jilianJJJManfDT = ConvertToJulian(parseInt(yearYYYYmanfDT), parseInt(monthManfDT), parseInt(dayManfDT)) ;
            
            jiliandayManfDT  = jilianJJJManfDT.toString().substr(4,3); 
            strYYYJJJManufDT = yearYYYManfDT + jiliandayManfDT;
     }
     //always replaceAll '|' and '^' symbol as empty string, and DI - 103940 as actual DI value and Donor numner with actual DOnor num value
     //for GS 1 identifier
     //103940      DI                   (01)
     //103941      MFG date      (11)
     //103942      EXP date      (17)
     //103943      LOT    (10)
     //103944      Serial #      (21)
     //103945      Donor Id      
     //4000688     Supplemental serial #

     format =  replaceAll(format,'|', '');
     format = replaceAll(format,'^', '');
     format = replaceAll(format,'103940', diVal);
     format = replaceAll(format,'(21)103944', '');
     format = replaceAll(format,'=,103944', '');
     format = replaceAll(format,'/S4000688', '');
     if(DonorNumvalue !=''){
            str = replaceAll(format,'103945', DonorNumvalue);
     }else{
            str = replaceAll(format,'=103945', '');
     }
     
     
     // if expiry date is not empty, replaceAll it with actual value by format, otherwise replaceAll as empty string with prefix.
     if (strYYMMDDExpDT != '')
     {      
            // exp date with contorl #
            str = replaceAll(str,'103942[YYMMDD]103943', strYYMMDDExpDT +cnvlaue); 
            str = replaceAll(str,'103942[MMYY]103943', strMMYYExpDT +cnvlaue); 
            str = replaceAll(str,'103942[MMDDYY]103943', strMMDDYYExpDT +cnvlaue);
            str = replaceAll(str,'103942[YYMMDDHH]103943', strYYMMDDExpDT+currentHour +cnvlaue); 
            str = replaceAll(str,'103942[YYJJJ]103943', strYYJJJExpDT +cnvlaue); 
            str = replaceAll(str,'103942[YYJJJHH]103943', strYYJJJExpDT+currentHour +cnvlaue);
            // Exp date with Serial #
            str = replaceAll(str,'103942[YYMMDD]103944', strYYMMDDExpDT); 
            str = replaceAll(str,'103942[MMYY]103944', strMMYYExpDT); 
            str = replaceAll(str,'103942[MMDDYY]103944', strMMDDYYExpDT);
            str = replaceAll(str,'103942[YYMMDDHH]103944', strYYMMDDExpDT+currentHour); 
            str = replaceAll(str,'103942[YYJJJ]103944', strYYJJJExpDT); 
            str = replaceAll(str,'103942[YYJJJHH]103944', strYYJJJExpDT+currentHour);
            // Only Exp date
            str = replaceAll(str,'103942[YYMMDD]', strYYMMDDExpDT);
            str = replaceAll(str,'103942[YYYJJJ]', strYYYJJJExpDT);
     }      
     else 
     {
              str = replaceAll(str,'(17)103942[YYMMDD]', ''); 
              str = replaceAll(str,'/$$103942[MMYY]', '/$$'); 
              str = replaceAll(str,'/$$2103942[MMDDYY]', '/$$2'); 
              str = replaceAll(str,'/$$3103942[YYMMDD]', '/$$3'); 
              str = replaceAll(str,'/$$4103942[YYMMDDHH]', '/$$4'); 
              str = replaceAll(str,'/$$5103942[YYJJJ]', '/$$5'); 
              str = replaceAll(str,'/$$6103942[YYJJJHH]', '/$$6'); 
              str = replaceAll(str,'/$$+103942[MMYY]', ''); 
              str = replaceAll(str,'/$$+2103942[MMDDYY]', ''); 
              str = replaceAll(str,'/$$+3103942[YYMMDD]', ''); 
              str = replaceAll(str,'/$$+4103942[YYMMDDHH]', ''); 
              str = replaceAll(str,'/$$+5103942[YYJJJ]', ''); 
              str = replaceAll(str,'/$$+6103942[YYJJJHH]', '');
              str = replaceAll(str,'=>103942[YYYJJJ]', '');
     }
     
     // if control num is not empty , replaceAll 103943 with actual control num value, otherwise replaceAll (10)103943 with empty string 
     if (cnvlaue != '')
     {
        str = replaceAll(str,'103943', cnvlaue);
        }
     else
     {
        str = replaceAll(str,'(10)103943', '');
        str = replaceAll(str,'/$103943', '');
        str = replaceAll(str,'103943', '');
        }
     //// if manufacturing date is not empty, replaceAll it with actual value by format, otherwise replaceAll as empty string with prefix.
     if (strYYMMDDManfDT != '')
     {      str = replaceAll(str,'103941[YYMMDD]', strYYMMDDManfDT); 
            str = replaceAll(str,'103941[YYYYMMDD]', strYYYYMMDDManfDT);
            str = replaceAll(str,'103941[YYYJJJ]', strYYYJJJManufDT);
     }      
     else 
     {
            str = replaceAll(str,'(11)103941[YYMMDD]', ''); 
            str = replaceAll(str,'/16D103941[YYYYMMDD]', '');
            str = replaceAll(str,'=}103941[YYYJJJ]', '');
     }

     str = replaceAll(str,'/$+103944', '');
     str = replaceAll(str,'S103944', '');
     str = replaceAll(str,'103944', '');
     
     return str;
}
//this function will convert calendar date to Julian date
function ConvertToJulian(Y,Mo,D ) {
	var a=Math.floor((14-Mo)/12);
	var y=Y+4800-a;
	var m=Mo+(12*a)-3;
	var julian ='';
 
	julian = D + Math.floor((153*m+2)/5) + (365*y) + Math.floor(y/4) - Math.floor(y/100) + Math.floor(y/400) - 32045   ;
 
	return Math.round(julian);
	
}
function replaceAll(OldString,FindString,ReplaceString) 
{
	var SearchIndex = 0;
  	var NewString = ""; 
  	while (OldString.indexOf(FindString,SearchIndex) != -1)    
	{
    	NewString += OldString.substring(SearchIndex,OldString.indexOf(FindString,SearchIndex));
    	NewString += ReplaceString;
    	SearchIndex = (OldString.indexOf(FindString,SearchIndex) + FindString.length);         
	}
  	NewString += OldString.substring(SearchIndex,OldString.length);
  	return NewString;
}

function fnEnablefunction()
{
    var cnvlaue = document.frmPartControlSetup.controlNM.value;	
    if(cnvlaue != "")
    	{
    	document.frmPartControlSetup.udi.disabled=false;
    	}
    
}