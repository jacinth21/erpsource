//This Function is used to fetch the data from DB
function fetchStageDash()
{
	windowopener("/gmBBAStageDash.do?method=fetchStagingDashboard","resizable=yes,scrollbars=yes,top=150,left=150,width=800,height=550");
}
//This function used to down load the excel file
function fnExport()
{	
	gridObj.toExcel('/phpapp/excel/generate.php');
}
// This function is to design and display the grid based on the data getting
// from DB
function fnPageOnload() {
    if (objGridData != '') {
        // to form the grid string
        var rows = [], i = 0;
        var JSON_Array_obj = "";
        // getting rowId
        var form_42_rowId = "";
        var labels_printed_rowId = "";
        var qc_verified_rowId = "";
        var PRE_STAGING_rowId = "";
        var Staging_rowId = "";
        var PI_rowId = "";
        var PP_rowId = "";
        var PV_rowId = "";
        var today = "";
        var date = "";
        var day = "";
        var Diff = "";
        var numDay = "";
        var txn = "";
        var setInitWidths = "";
        var setColAlign = "";
        var setColTypes = "";
        var setColSorting = "";
        var setHeader = "";
        var setFilter = "";
        var setColIds = "";
        var enableTooltips = "";
        var footerArry = "";
        var gridHeight = "";
        var footerStyles = "";
        var footerExportFL ="";
        var dataHost = "";
        var dateEntered_rowId="";
        // to set background colour for each cell
		var gridrows = ""
		var gridrowsarr = "";
		var arrLen = "";
        JSON_Array_obj = JSON.parse(objGridData);
        $.each(JSON_Array_obj, function(index, jsonObject) {
        	
        //comments- phone symbol
    	log_msg = jsonObject.log_msg;
    	txn_id = jsonObject.txn_id;
    	// to get the type and STATUS_FL
    	ctype = jsonObject.CTYPE;
    	con_type = jsonObject.CON_TYPE;
    	status_fl = jsonObject.STATUS_FL
     	donor_number = jsonObject.donor_number;
    	load_number = jsonObject.load_number;
    	part_count = jsonObject.part_count;
    	qty_received= jsonObject.qty_received;
    	due = jsonObject.due;
    	stage_date= jsonObject.STAGE_DATE;
    	form_42 = jsonObject.form_42;
       	labels_printed = jsonObject.labels_printed;
    	qc_verified = jsonObject.qc_verified;
    	pre_staging = jsonObject.PRE_STAGING;
    	Staging = jsonObject.Staging;
        pp = jsonObject.PP;
        pi = jsonObject.PI;
        pv = jsonObject.PV;
        txntype = jsonObject.TXNTYPE;
        date_entered = jsonObject.DATE_ENTERED;


        // default array 
        rows[i] = {};
        rows[i].data = [];
        
        rows[i].id = txn_id;
        if(log_msg == 'N'){
        	
        		rows[i].data[0] = "<a href='#' onclick=fnBulkDHRLog('"+txn_id+"','"+log_msg+"')><img alt='Click to add comments' src='/images/phone_icon.jpg' width='20' height='17'></a>";
        	}else{
    			rows[i].data[0] = "<a href='#' onclick=fnBulkDHRLog('"+txn_id+"','"+log_msg+"')><img alt='Click to add comments' src='/images/phone-icon_ans.gif' width='20' height='17'></a>";
        }
        // 103932
         
		if (txn.indexOf("RS")!=-1 && form_42 != 'Y' &&txntype=='9105')   
   
		     {
        rows[i].data[1]= "<font color='RED'>"+txn_id+"</font>";
        rows[i].data[2] = "<font color='RED'>"+donor_number+"</font>";
        rows[i].data[3] = "<font color='RED'>"+load_number+"</font>";
        rows[i].data[4] =  "<font color='RED'>"+part_count+"</font>";
        rows[i].data[5] =  "<font color='RED'>"+qty_received+"</font>";
        
        }
        
        else
        	
           {
        	
            rows[i].data[1]= txn_id;
            rows[i].data[2] = donor_number;
            rows[i].data[3] = load_number;
            rows[i].data[4] =  part_count;
            rows[i].data[5] = qty_received;
            
            }
        	
        // due column 
        if(stage_date != null){
        today = new Date();
        date =(today.getMonth()+1)+"/"+today.getDate()+"/"+today.getFullYear();
        Diff = dateDiff(date,stage_date);
        numDay= Diff.toString().replace(/-/g, '');
        
       //PMT-45812: Due date calculation bug fix
        
        //If the stage date-current date difference is 0 - Due Date
        	if(Diff == '0'){
	        	 day = "today";
	        	 rows[i].data[6] = "<font color='red'>Due  "+day+"</font>";
        }
            //If the stage date-current date difference is <0 - Past Due (N days)

        	 else if(Diff < 0){
        		 day = "days";
            	 rows[i].data[6] ="<font color='red'>Past  Due("+numDay+")"+day+"</font>"; 
             }
            //If the stage date-current date difference is >0 - Due in N days

        	 else if(Diff >0){
	        	day = "days";
	        	rows[i].data[6] = "Due in "+numDay+" "+day;
        }
        }
        rows[i].data[7] = stage_date; 
        txn = txn_id.substring(0,2);
		// form42 , QC label and label print if Y should be in black and not a link
		if (txn.indexOf("RS")!=-1 && form_42 != 'Y'){
				rows[i].data[8] = "<a href='#'  onClick=fetchProcessTab('"+txn_id+"','"+txntype+"','"+form_42+"')><u><font color='red'>"+form_42+"</font></u></a>";
			}else{
				rows[i].data[8] = form_42;	
		}
		// labels printed
		if(labels_printed == 'Y'){
			rows[i].data[9] =labels_printed;
		    }else if(status_fl=="1" &&(labels_printed == 'N' || qc_verified == 'N')){
			 	rows[i].data[9] = "<a href='#'  onClick=fetchProcessTab('"+txn_id+"','"+txntype+"','"+ctype+"','2')><u><font color='red'>"+labels_printed+"</font></u></a>";	
			}else if(status_fl=="2"){
				rows[i].data[9] = "<a href='#'  onClick=fetchProcessTab('"+txn_id+"','"+txntype+"','"+ctype+"','"+status_fl+"')><u><font color='red'>"+labels_printed+"</font></u></a>";
			} else if(labels_printed == 'N' && (txntype == "0")){
				
				rows[i].data[9]="<a href='#'  onClick=fetchProcessTab('"+txn_id+"','"+txntype+"','"+ctype+"','"+status_fl+"')><u><font color='red'>"+labels_printed+"</font></u></a>";
			}
			else {
	        	 rows[i].data[9]= labels_printed;
			}
		//QC label
		if(qc_verified == 'Y'){
			rows[i].data[10]=qc_verified;
		    }else if(status_fl=="1" &&(labels_printed == 'N' || qc_verified == 'N')){
			rows[i].data[10]= "<a href='#'  onClick=fetchProcessTab('"+txn_id+"','"+txntype+"','"+ctype+"','2')><u><font color='red'>"+qc_verified+"</font></u></a>";	
			}else if(status_fl=="2"){
				rows[i].data[10]="<a href='#'  onClick=fetchProcessTab('"+txn_id+"','"+txntype+"','"+ctype+"','"+status_fl+"')><u><font color='red'>"+qc_verified+"</font></u></a>";
			}else if(qc_verified == 'N' && (txntype == "0")){
				
				rows[i].data[10]="<a href='#'  onClick=fetchProcessTab('"+txn_id+"','"+txntype+"','"+ctype+"','"+status_fl+"')><u><font color='red'>"+qc_verified+"</font></u></a>";
			}
			else {
	        	 rows[i].data[10]= qc_verified;
			}
		
		//Prestaging 
		
		if(pre_staging == 'Y'){
			 rows[i].data[11]=pre_staging;
		    }else if(status_fl=="1" &&(pre_staging == 'N' || Staging == 'N')){
			 rows[i].data[11]= "<a href='#'  onClick=fetchProcessTab('"+txn_id+"','"+txntype+"','"+ctype+"','2')><u><font color='red'>"+pre_staging+"</font></u></a>";	
			}else if(status_fl=="2"){
				 rows[i].data[11]="<a href='#'  onClick=fetchProcessTab('"+txn_id+"','"+txntype+"','"+ctype+"','"+status_fl+"')><u><font color='red'>"+pre_staging+"</font></u></a>";
			}else if(pre_staging == 'N' && (txntype == "0")){
				
				rows[i].data[11]="<a href='#'  onClick=fetchProcessTab('"+txn_id+"','"+txntype+"','"+ctype+"','"+status_fl+"')><u><font color='red'>"+pre_staging+"</font></u></a>";
			}else {
	        	 rows[i].data[11]= pre_staging;
			}
		 // Staging
		if(Staging == 'Y'){
			rows[i].data[12]= Staging;
		    }else if(status_fl=="1" &&(pre_staging == 'N' || Staging == 'N')){
			rows[i].data[12]= "<a href='#'  onClick=fetchProcessTab('"+txn_id+"','"+txntype+"','"+ctype+"','2')><u><font color='red'>"+Staging+"</font></u></a>";	
			}else if(status_fl=="2"){
				rows[i].data[12]="<a href='#'  onClick=fetchProcessTab('"+txn_id+"','"+txntype+"','"+ctype+"','"+status_fl+"')><u><font color='red'>"+Staging+"</font></u></a>";
			}else if(Staging == 'N' && (txntype == "0")){
				
				rows[i].data[12]="<a href='#'  onClick=fetchProcessTab('"+txn_id+"','"+txntype+"','"+ctype+"','"+status_fl+"')><u><font color='red'>"+Staging+"</font></u></a>";
			}else {
	        	 rows[i].data[12]= Staging;
			}
		
		
        //pass CTYPE and STATUS_FL from db for below links
        if(pi == 'y' || pi == 'Y'){
        		rows[i].data[13]= "<a href='#'  onClick=fnUpdateStagingTXN('"+txn_id+"','"+txntype+"','"+ctype+"','"+status_fl+"')><u><font color='red'>"+pi+"</font></u></a>";
        	}else{
        		rows[i].data[13]= "-";	
        }
        if(pp == 'y' || pp == 'Y'){
        		rows[i].data[14]="<a href='#'  onClick=fnUpdateStagingTXN('"+txn_id+"','"+txntype+"','"+ctype+"','"+status_fl+"')> <u><font color='red'>"+pp+"</font></u></a>";
        	}else{
        		rows[i].data[14]= "-" ;
            }
        if(pv == 'y' || pv == 'Y'){
        		rows[i].data[15]= "<a href='#'  onClick=fnUpdateStagingTXN('"+txn_id+"','"+txntype+"','"+ctype+"','"+status_fl+"')><u><font color='red'>"+pv+"</font></u></a>";
        	}else{
        		rows[i].data[15]= "-";
        }
        
        rows[i].data[16] = date_entered; 

        i++;
        });
	   // Updating the Alignment to change the Stage Date field editable PMT-44211
        setInitWidths = "70,80,70,60,60,60,90,75,60,60,60,60,60,60,60,60,75";
        setColAlign = "center,left,right,right,right,right,center,center,center,center,center,center,center,center,center,center,center";
        setColTypes = "ro,ro,ro,ro,ro,ro,ro,dhxCalendar,ro,ro,ro,ro,ro,ro,ro,ro,ro";
        setColSorting = "na,str,int,int,int,int,str,date,str,str,str,str,str,str,str,str,str";
        setHeader = [ "Comments", " Transaction", "Donor", " Load Num", " Part #",
                "Qty","Due", "Staged Date" ,"Form 42","Label Print","QC(Label)","Pre Staging","Staging","PI","PP","PV", "Date Entered"];
        setFilter = [ ,"#text_filter","#text_filter","#text_filter","#numeric_filter", "#numeric_filter","#text_filter", "#text_filter","#text_filter","#text_filter","#text_filter","#text_filter","#text_filter","#text_filter","#text_filter","#text_filter","#text_filter"];
        setColIds = "comments,txn_id,donor_number,load_number,part_count,qty_received,due,STAGE_DATE,form_42,labels_printed,qc_verified,PRE_STAGING,Staging,PI,PP,PV,Date_Entered";
        enableTooltips = "true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true";
        document.getElementById('BBAStageDashBoardRpt').setAttribute("style","width:1140");

        footerArry = [];
        gridHeight = "700";
        gridWidth =  "1000";
        footerStyles = [];
        footerExportFL = true;
        dataHost = {};
        dataHost.rows = rows;
        var pagination = "";
        gridObj = initGridData('BBAStageDashBoardRpt', gridHeight, dataHost, setHeader,
                setInitWidths, setColAlign, setColTypes, setColSorting,
                setColIds, enableTooltips, setFilter, footerArry, footerStyles,
                footerExportFL, pagination);
        gridObj.enableEditTabOnly(true);
		gridObj.enableEditEvents(true, false, true);
        
        loadnum_rowid = gridObj.getColIndexById("txn_id");
        
		// to set background colour for each cell
	    gridrows = gridObj.getAllRowIds(",");
	    gridrowsarr = gridrows.toString().split(",");
	    arrLen = gridrowsarr.length;
	    
	    comments = gridObj.getColIndexById("comments");
	    txn_id = gridObj.getColIndexById("txn_id");
	    donor_number = gridObj.getColIndexById("donor_number");
	    load_number = gridObj.getColIndexById("load_number");
	    part_count = gridObj.getColIndexById("part_count");
	    qty_received = gridObj.getColIndexById("qty_received");
	    due = gridObj.getColIndexById("due");
	    STAGE_DATE = gridObj.getColIndexById("STAGE_DATE");
		form_42_rowId = gridObj.getColIndexById("form_42");
		labels_printed_rowId = gridObj.getColIndexById("labels_printed");
		qc_verified_rowId = gridObj.getColIndexById("qc_verified");
		pre_staging_rowid = gridObj.getColIndexById("PRE_STAGING");
		Staging_rowId = gridObj.getColIndexById("Staging");
		pi_rowid = gridObj.getColIndexById("PI");
		pp_rowid = gridObj.getColIndexById("PP");
		pv_rowid = gridObj.getColIndexById("PV");
		dateEntered_rowId = gridObj.getColIndexById("Date_Entered");

		
        for (var i = 0; i < arrLen; i++) {
		rowId = gridrowsarr[i];
		
		gridObj.setCellTextStyle(rowId, comments,
				"background-color:#e9ebdb");
		gridObj.setCellTextStyle(rowId, txn_id,
				"background-color:#e9ebdb");
		gridObj.setCellTextStyle(rowId, donor_number,
				"background-color:#e9ebdb");
		gridObj.setCellTextStyle(rowId, load_number,
		"background-color:#e9ebdb");
		gridObj.setCellTextStyle(rowId, part_count,
				"background-color:#e9ebdb");
		gridObj.setCellTextStyle(rowId, qty_received,
				"background-color:#e9ebdb");
		gridObj.setCellTextStyle(rowId, due,
				"background-color:#e9ebdb");
		gridObj.setCellTextStyle(rowId, STAGE_DATE,
				"background-color:#e9ebdb");
		gridObj.setCellTextStyle(rowId, form_42_rowId,
				"background-color:#e1f3a6");
		gridObj.setCellTextStyle(rowId, labels_printed_rowId,
				"background-color:#e1f3a6");
		gridObj.setCellTextStyle(rowId, qc_verified_rowId,
				"background-color:#e1f3a6");
		gridObj.setCellTextStyle(rowId, pre_staging_rowid,
				"background-color:#ffecb9");
		gridObj.setCellTextStyle(rowId, Staging_rowId,
				"background-color:#ffecb9");
		gridObj.setCellTextStyle(rowId, pi_rowid,
				"background-color:#ffecb9");
		gridObj.setCellTextStyle(rowId, pp_rowid,
				"background-color:#e1f3a6");
		gridObj.setCellTextStyle(rowId, pv_rowid,
				"background-color:#ffecb9");		
		gridObj.setCellTextStyle(rowId, dateEntered_rowId,
		"background-color:#e9ebdb");
		
		
}
        gridObj.attachEvent("onEditCell", doOnCellEdit);
	        document.getElementById("BBAStageDashBoardRpt").style.display = 'block';
	        document.getElementById("divNoExcel").style.display = 'block';
	        document.getElementById("DivNoMsg").style.display = 'none';
    	} else {
	        document.getElementById("divNoExcel").style.display = 'none';
	        document.getElementById("BBAStageDashBoardRpt").style.display = 'none';
	        document.getElementById("DivNoMsg").style.display = 'block';
}
    
}

// New function to change the Stage date
function doOnCellEdit(stage, rowId, cellInd, nValue, oValue) {

	if(nValue!=''){
		
		txnId = gridObj.cellById(rowId, 1).getValue();
	stage_date = gridObj.cellById(rowId, 7).getValue();
	gridObj.setImagePath("/extweb/dhtmlx/dhtmlxGrid/imgs/");
	gridObj.setDateFormat("%m/%d/%Y","%m/%d/%Y");
	var txnspilt = txnId.split('-');
	var txn_type=txnspilt[0]=='PTRD'? '101260':'9105';
	
     if (stage == 2) {
      stage_dat =nValue;
      
     var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmBBARedesignatedash.do?&method=SavStageDate&strStageDt='
				+ stage_date
				+ '&strTxn='
				+ txnId
				+ '&strTxnDate='
				+ stage_date
				+ '&strTxnType='
				+ txn_type + '&ramdomId=' + Math.random());
 	var loader = dhtmlxAjax.getSync(ajaxUrl);


		//dhtmlxAjax.get(ajaxUrl, fnCallBack);
	
	}
     return true;
	}
}

//This function is called while initiating the grid with customized dropdown
function initGridData(divRef, gridHeight, gridData, setHeader, setInitWidths,
		setColAlign, setColTypes, setColSorting, setColIds, enableTooltips,
		setFilter, footerArry, footerStyles, footerExportFL, pagination) {

	
	var gObj = new dhtmlXGridObject(divRef);
	gObj.enableColSpan(true);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/extweb/dhtmlx/dhtmlxGrid/imgs/");
	gObj.setDateFormat("%m/%d/%Y","%m/%d/%Y");

	if (setHeader != undefined && setHeader.length > 0) {
		gObj.setHeader(setHeader);
		var colCount = setHeader.length;
	}
	// this function is to add the calendar in grid header
	gObj._in_header_custom_calendar = function(tag, index, data) {
		tag.innerHTML = '<input type="text" size="10" name="mfgDate" id="mfgDate" class="InputArea" onFocus="changeBgColor(this,\'#AACCE8\');" onBlur="changeBgColor(this,\'#ffffff\');" tabindex=9><img id="Img_Date" style="cursor:hand" onclick="javascript:showSglCalendar(\'dcalendardiv\',\'mfgDate\');" title="Click to open Calendar"  src="<%=strImagePath%>/nav_calendar.gif" border=0 align="absmiddle" height=18 width=19 />	<div id="dcalendardiv" style="position: absolute; z-index: 10;"></div>';
	}

	if (setInitWidths != "")
		gObj.setInitWidths(setInitWidths);
	if (setColAlign != "")
		gObj.setColAlign(setColAlign);
	if (setColTypes != "")
		gObj.setColTypes(setColTypes);
	if (setColSorting != "")
		gObj.setColSorting(setColSorting);
	if (enableTooltips != "")
		gObj.enableTooltips(enableTooltips);
	if (setFilter != "")
		gObj.attachHeader(setFilter);
	if (footerArry != undefined && footerArry.length > 0) {
		var footstr = eval(footerArry).join(",");
		if (footerStyles != undefined && footerStyles.length > 0)
			gObj.attachFooter(footstr, footerStyles);
		else
			gObj.attachFooter(footstr);
	}

	if (setColIds != "") {
		gObj.setColumnIds(setColIds);
	}

	if (gridHeight != "")
		gObj.enableAutoHeight(true, gridHeight, true);
	else
		gObj.enableAutoHeight(true, 400, true);

	gObj.init();
	if (pagination == "Y") {
		gObj.enablePaging(true, 100, 10, "pagingArea", true);
		gObj.setPagingSkin("bricks");
	}

	gObj.parse(gridData, "json");
	return gObj;
}

function showSglCalendar(calendardiv, dateformat) {
	// Overite style for calendar only from class RightTableCaption. This method
	// call to date-pciker.js.
	onSetStyleToCalendar(calendardiv);

	if (dateformat == null) {
		dateformat = '%m/%d/%Y';
	}
	if (!calendars[calendardiv])
		calendars[calendardiv] = initCalendar(calendardiv, dateformat);

	// This line for today date will display in BOLD and this method will be in
	// dhtmlxcalendar.js
	calendars[calendardiv].enableTodayHighlight(true); // or false

	if (calendars[calendardiv].isVisible())
		calendars[calendardiv].hide();
	else
		calendars[calendardiv].show();
}

// Below codes are modified for new DHTMLX version
function initCalendar(calendardiv, dateformat) {

	// Above commented code for New version 3.5. In old dhtmlx version we can't
	// edit month and year in IPAD. Now we can edit.
	mCal = new dhtmlXCalendarObject(calendardiv);

	// This range only able to select date.
	mCal.setSensitiveRange("1970-01-01", "2500-31-12");

	mCal.setDateFormat(dateformat);
	mCal.hideTime(); // Hide the time in new dhtmlx version 3.5

	// Changed the above code for new version DHTMLX. When click calendar icon
	// and set the date in textbox.
	mCal.attachEvent("onClick", function(date) {
		calendars[calendardiv].hide();
		return true;
	});
	mCal.hide();
	return mCal;
}




//form 42,QClabel,prestaging,staging(hyperlink)
//internally invoking the methods in GmRecieveShipDash.js and GmBBARedesignate.js

function fetchProcessTab(txn_id,txntype,ctype,status_fl){
	    //if the txntype & ctype is 103932 
	    if (txntype ==103932)
		{
	    	// call this method if the function type is PTRD -GmBBARedesignateDash.js
	      fnCallItemsShip(txn_id,ctype,status_fl);
		}
		else if (txntype ==9105){
			// call this method if the function type is RS -GmReceiveShipDash.js
		  fnProcessTab(txn_id);
		}
		}

//pi,pp,pv(hyperlink) calls fnUpdateStagingTXN() if the function type is PTRD
		
function fnUpdateStagingTXN(txn_id,txntype,ctype,status_fl){
	    //if the txntype & ctype is 103932 
		if (txntype ==103932)
		{
		// call this method if the function type is PTRD -GmBBARedesignateDash.js
		   fnCallItemsShip(txn_id,ctype,status_fl);
		}
		else if (txntype ==9105){
		//if the txn type is RS call the function fnUpdateDHR(id,val) -GmReceiveShipDash.js
		   fnUpdateDHR(txn_id);
		}
		}

//this function is used to Pending Shipping Verification
function fnCallItemsShip(txn_id, ctype,con_type) {
		var statuflg='';
		document.frmBBAStagingDashBoard.hId.value = txn_id;
		document.frmBBAStagingDashBoard.hFrom.value = "LoadItemShip";
		document.frmBBAStagingDashBoard.hConsignId.value = txn_id;
		document.frmBBAStagingDashBoard.hAction.value = "LoadItemShip";
		document.frmBBAStagingDashBoard.hMode.value = "VERIFY";
		document.frmBBAStagingDashBoard.hType.value = ctype;
		document.frmBBAStagingDashBoard.hOpt.value = ctype;
		
//if transcation is part re-designation transaction (PTRD)
		if(con_type == 1){
			targetUrlStr = "/gmPartLabel.do?method=fetchPartLabelDetail&txnid="+txn_id+"&action="+con_type+"&txntype="+ctype+"&hAction=PI";
		}else if(con_type == 2){ // if transcation status is 2(pending process) 
			targetUrlStr = "/gmPartLabel.do?method=fetchPartLabelDetail&txnid="+txn_id+"&hAction=PP&txntype="+ctype+"&statuflg="+con_type;
		}else if(con_type == 3){ // if transcation status is 2(pending process) 
			targetUrlStr = "/gmPartLabel.do?method=fetchPartLabelDetail&txnid="+txn_id+"&hAction=PV&txntype="+ctype+"&statuflg="+con_type;
		}else if (con_type == 'T') {
			urlStr = "&txnid=" + txn_id
					+ "&mode=VERIFY&haction=EditVerify&hConsignId=" + txn_id
					+ "&txntype=" + ctype + "&loctype=93345";
			document.frmBBAStagingDashBoard.action = "/gmItemVerify.do?";
			targetUrlStr = document.frmBBAStagingDashBoard.action + urlStr;
		} else {
			document.frmBBAStagingDashBoard.action = "GmReprocessMaterialServlet";
			document.frmBBAStagingDashBoard.submit();
			return false;
		}
		parentLcn = parent.location.href;
		if (parentLcn.indexOf("GmEnterprisePortal") != -1) {
			fnLocationHref(targetUrlStr);
		}
		
		else if (parentLcn.indexOf("GmGlobusOnePortal") != -1) {
			fnLocationHref(targetUrlStr);
		} 
		
		 else {
			fnLocationHref(targetUrlStr, parent);
		}
}