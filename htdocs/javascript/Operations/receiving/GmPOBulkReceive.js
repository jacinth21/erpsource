var gridObj,gObj;
var gridRowId = 0;
var scanRowId = 0;
var delrows = 0;
var dupLotNumFl = false;
var costpricefl =false;

//When click on Load button
function fnLoad(obj){
	if(event.keyCode == 13 || obj.value == 'Load'){//Need to work while clicking the button as well as pressing enter key
		fnValidateTxtFld('poNumber',message_operations[578]);
		if (ErrorCount > 0)
		{
			Error_Show();
			Error_Clear();
			return false;
		}
		var poNum = document.frmPOBulkReceive.poNumber.value;
		document.frmPOBulkReceive.poNumber.value = poNum.toUpperCase();
		document.frmPOBulkReceive.strOpt.value = 'loadPO';
		fnStartProgress('Y');
		document.frmPOBulkReceive.submit();
	}
}

function fnOnPageload(){
	
	document.frmPOBulkReceive.poNumber.focus();
	var strOpt = document.frmPOBulkReceive.strOpt.value;
	
	if(objGridData){
		gridObj = initGridData('dataGrid',objGridData);
		gridObj.attachEvent("onEditCell",doOnCellEdit);
	}
	
	//gridObj.enableAutoHeight(true,500,true);// To enable auto height for grid
	if(strOpt == 'loadPO'){
		//Reset all the values when click on Load button
		document.frmPOBulkReceive.receivedDt.focus();
		document.frmPOBulkReceive.packSlipId.value = '';
		document.frmPOBulkReceive.donorNum.value = '';
	}
	document.frmPOBulkReceive.partNum.value = '';
	fnResetValues();
	
	if(validateObject(document.getElementById("recPO"))){
		document.getElementById("recPO").style.display ="table";
	}
}

// validate lot code on changing the values
function doOnCellEdit(stage, rowId, cellInd, nValue, oValue){
	var gridLotNum;
	var count = 0;
	var mfgCurrDiff, expCurrDiff, expMfgDiff;
	var mfgDate;
	
	if(cellInd == '1' && stage == 2){//once the value is selected
		if(oValue != nValue){
			gridObj.forEachRow(function(grrowid){
				gridLotNum = gridObj.cellById(grrowid,1).getValue();
		 			if(nValue == gridLotNum && grrowid != rowId){
			 			count++;
			 		}
	 		});
		}
				
		fnvalidateLot(nValue);		
		
		if(count > 0){// Should not add duplicate control number to the grid
			document.getElementById("errMsg").innerHTML = "<font color=red><b>"+message_operations[66]+"</b></font>";
			gridObj.cellById(rowId,1).setValue(oValue);
		}else{
			document.getElementById("errMsg").innerHTML = "";
		}
		
	}
	// For Expiry date validation
	if(cellInd == '5' && stage == 2){
		mfgDate = gridObj.cellById(rowId,6).getValue();
		expMfgDiff= dateDiff(mfgDate, nValue, format);
		expCurrDiff= dateDiff(currdate, nValue, format);
		if(nValue == ''){
			Error_Details(message_operations[67]);
		}
		if(expMfgDiff < 0){
			Error_Details(message_operations[68]);
		}
		
		if(expCurrDiff < 0){
			Error_Details(message_operations[69]);
		}
		
		if (ErrorCount > 0)
		{
			Error_Show();
			gridObj.cellById(rowId,5).setValue(oValue);
			Error_Clear();
			return false;
		}
	}
	
	// For Manufacturing date validation
	if(cellInd == '6' && stage == 2){
		mfgCurrDiff= dateDiff(nValue, currdate, format);
		if(nValue == ''){
			Error_Details(message_operations[70]);
		}
		if(mfgCurrDiff < 0){
			Error_Details(message_operations[71]);
		}
		
		if (ErrorCount > 0)
		{
			Error_Show();
			gridObj.cellById(rowId,6).setValue(oValue);
			Error_Clear();
			return false;
		}
	}
	
	return true;
}

function initGridData(divRef,gridData){
	gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.init();
	gObj.loadXMLString(gridData);
	return gObj;	
}

// To fetch the part details
function fnFetchPartdtl(obj){
	var partNumber = obj.value;
	var strCostPrice ='';
	var strpartstatus='';
	
	
	var mfgDate = document.frmPOBulkReceive.mfgDate;
	if(partNumber != ''){//Get the part details
		var loader=dhtmlxAjax.getSync("/gmPOBulkReceive.do?companyInfo="+compInfObj+"&method=loadPartInfo&partNum=" + encodeURIComponent(obj.value)+ "&ramdomId=" + Math.random());
		var status = loader.xmlDoc.status;
			if(status == 200){  //On success stauts parsing the response values.
				var responseText = loader.xmlDoc.responseXML;
				var data = responseText.getElementsByTagName("data");
				var datalength = data[0].childNodes.length;
				if (responseText != null && datalength != 0) {
				        parseMessage(responseText);
				        //fnResetValues(partNumber);//To reset the values if the part number is changed
				}
				
			}
			if(mfgDate.value != ''){// Need to calculate Exp date if the part number is changed
				fnCalculateExpDate(mfgDate);
			}
			
	}else{// Reset the values if part number text box is empty
		fnResetPartDtls();
	}
	// var costprice = loader.xmlDoc.costprice;
}

// Reset the values if part number text box is empty
function fnResetPartDtls(){
	document.getElementById("pdesclabel").innerHTML = '';
	document.getElementById("psizelabel").innerHTML = '';
	document.getElementById("pprodtypelabel").innerHTML = '';
	document.frmPOBulkReceive.hShelfLife.value = '';
	document.frmPOBulkReceive.hProcessClient.value = '';
}

//To assign the values getting from ajax call to the corresponding fields
/*PC-3658 Code populating undefined in Edge Browser.Changes did for support Edge browser
Need to change the code for populating undefined while tabbing out from partnumber field
Existing like strpdesc = parseXmlNode(pdesc[0].childNodes[0].firstChild); instead of this 
changed to var pdesc = pnum[0].getElementsByTagName("pdesc")[0];*/
function parseMessage(xmlDoc) 
{
	var strpdesc ='';
	var strpsize ='';
	var strProdType ='';
	var strShelfLife ='';
	var strContProCInt ='';
	
	var pnum = xmlDoc.getElementsByTagName("pnum");
	var pnumlen = pnum[0].childNodes.length;
	if(pnumlen == 0){
		Error_Details(message_operations[72]);
		document.frmPOBulkReceive.partNum.value = '';
		document.frmPOBulkReceive.partNum.focus();
		fnResetPartDtls();// Reset the values if part number text box is empty
		Error_Show();
	    Error_Clear();
	    return false;
	}
	
	var partnum = pnum[0].getElementsByTagName("partnum")[0];
	var partnumlen = partnum.length;
	var pdesc = pnum[0].getElementsByTagName("pdesc")[0];
	var pdesclength = pdesc.length;
	var lsize = pnum[0].getElementsByTagName("lsize")[0];
	var lsizelength = lsize.length;
	var prodType = pnum[0].getElementsByTagName("prodtype")[0];
	var prodTypelength = prodType.length;
	var shelfLife = pnum[0].getElementsByTagName("shelflife")[0];
	var shelfLifelength = shelfLife.length;
	var contproclnt = pnum[0].getElementsByTagName("cpclient")[0];
	var contproclntlength = contproclnt.length;
	var costprice = pnum[0].getElementsByTagName("costprice")[0];
	var costpricelength = costprice.length;
	var partstatus = pnum[0].getElementsByTagName("partstatus")[0];
	var partstatuslength = partstatus.length;
	//Reset the error messge 
  	document.getElementById("errMsg").innerHTML ='';
	
	if(validateObject(pdesc) && pdesclength!=0){
		var pdescValue = pdesc.textContent != undefined? pdesc.textContent : pdesc.text;
		strpdesc = parseXmlNode(pdescValue);
		document.getElementById("pdesclabel").innerHTML =pdescValue;		
		}
	if(validateObject(lsize) && lsizelength!=0){ 	
		var psizeValue = lsize.textContent != undefined? lsize.textContent : lsize.text;
		strpsize = parseXmlNode(psizeValue) ;
		document.getElementById("psizelabel").innerHTML =psizeValue;	
		document.frmPOBulkReceive.hsize.value =psizeValue;
		}
	if(validateObject(prodType) && prodTypelength!=0){ 
		var prodTypeValue = prodType.textContent != undefined? prodType.textContent : prodType.text;
		strProdType = parseXmlNode(prodTypeValue);
		document.getElementById("pprodtypelabel").innerHTML =prodTypeValue;
		document.frmPOBulkReceive.hProdType.value = prodTypeValue;
		}
	if(validateObject(shelfLife) && shelfLifelength!=0){
		var shelfLifeValue = shelfLife.textContent != undefined? shelfLife.textContent : shelfLife.text;
		strShelfLife =  parseXmlNode(shelfLifeValue) ;
		document.frmPOBulkReceive.hShelfLife.value =shelfLifeValue;
		}
	if(validateObject(contproclnt) && contproclntlength!=0){ 
		var contProCIntValue = contproclnt.textContent != undefined? contproclnt.textContent : contproclnt.text;
		strContProCInt = parseXmlNode(contProCIntValue) ;
		document.frmPOBulkReceive.hProcessClient.value =contProCIntValue;
		}
		var costPriceValue = costprice.textContent != undefined? costprice.textContent : costprice.text;
	  	strCostPrice = TRIM(costPriceValue);
		var partstatusValue = partstatus.textContent != undefined? partstatus.textContent : partstatus.text;
	  	strpartstatus=TRIM(partstatusValue);

      	if(strCostPrice!=undefined && strCostPrice<='0')
      	{
		document.getElementById("errMsg").innerHTML = "<font color=red><b>"+message_operations[857]+"</b></font>";
		
		}	 
		
		else if(strpartstatus!=undefined && strpartstatus!='Y')
		{
				document.getElementById("errMsg").innerHTML = "<font color=red><b>"+message_operations[865]+"</b></font>";
		
		}
		
	
		else
		{
			document.getElementById("errMsg").innerHTML = "";
		}
    
}

//To reset the fields
function fnResetValues(partNumber){
	//var hPartNum = document.frmPOBulkReceive.hPartNum.value;
	//if(hPartNum != partNumber){
		document.frmPOBulkReceive.lotCode.value = '';
		document.frmPOBulkReceive.mfgDate.value = '';
		document.frmPOBulkReceive.expDate.value = '';
		document.frmPOBulkReceive.customSize.value = '';
	//}
	//document.frmPOBulkReceive.hPartNum.value = partNumber;
}

//function to add the row to the grid
function fnAdd(obj){
	if(event.keyCode == 13 || obj.value == 'Add'){//Need to work while clicking the button as well as pressing enter key
		var frm = document.frmPOBulkReceive;
		var lotCode = frm.lotCode.value;
		lotCode = lotCode.toUpperCase()
		var partNum = frm.partNum.value;
		var custSize = frm.customSize.value;
		var size = frm.hsize.value;
		var expDateObj = frm.expDate;
		var expDate = frm.expDate.value;
		var mfgDateObj = frm.mfgDate;
		var mfgDate = frm.mfgDate.value;
		var prodType = document.frmPOBulkReceive.hProdType.value;
		var contProcessClient = frm.hProcessClient.value;
		var cellValue=new Array();
		validateField(partNum, lotCode, mfgDateObj, expDateObj);//Validate the fields before generating the grid
		
		if(ErrorCount>0){
			Error_Show();
	        Error_Clear();
	        return false;
		}else if((!dupLotNumFl) && !(costpricefl)){// Add row to the grid, should not add the control number if it is already there
			if(delrows != 0){//if any row is deleted then need to reassign the scanRowId for the proper serial number
				scanRowId = scanRowId - delrows;
				delrows = 0;
			}
	    	cellValue[0] = ++scanRowId;
			cellValue[1] = lotCode;
			cellValue[2] = partNum;
			cellValue[3] = custSize;
			cellValue[4] = size;
			cellValue[5] = expDate;
			cellValue[6] = mfgDate;
			cellValue[7] = prodType;
			cellValue[8] = contProcessClient;
			gridObj.addRow(scanRowId,cellValue);
			//For setting border for editable fields
			gridObj.setCellTextStyle(scanRowId,1,"border:1px solid gray;");
			gridObj.setCellTextStyle(scanRowId,3,"border:1px solid gray;");
			gridObj.setCellTextStyle(scanRowId,5,"border:1px solid gray;");
			gridObj.setCellTextStyle(scanRowId,6,"border:1px solid gray;");
	    }
		
		document.frmPOBulkReceive.lotCode.focus();
		document.frmPOBulkReceive.lotCode.value = '';
		document.frmPOBulkReceive.customSize.value = '';
		
	}
}

//Function to validate the fields
function validateField(partNum, lotCode, mfgDateObj,expDateObj){
	var ctrlNum;
	var flag = true;
	var dateDiffs;
	var mfgCurrDiff;
	fnValidateTxtFld('partNum',message_operations[187]);
	fnValidateTxtFld('lotCode',message_operations[188]);
	fnValidateTxtFld('expDate',message_operations[189]);
	fnValidateTxtFld('mfgDate',message_operations[190]);
	CommonDateValidation(expDateObj,format,Error_Details_Trans(message_operations[73],format));
	CommonDateValidation(mfgDateObj,format,Error_Details_Trans(message_operations[74],format));
	
	dateDiffs = dateDiff( expDateObj.value,mfgDateObj.value, format);
	mfgCurrDiff = dateDiff(mfgDateObj.value, currdate, format);
	expCurrDiff = dateDiff(currdate, expDateObj.value, format);
	
	if (mfgDateObj.value != '' && mfgCurrDiff < 0){
		Error_Details(message_operations[75]);
	}
	
	if (expDateObj.value != '' && expCurrDiff < 0){
		Error_Details(message_operations[76]);
	}
	
	if (expDateObj.value != '' && mfgDateObj.value != '' && dateDiffs > 0){
		Error_Details(message_operations[77]);
	}
	 
	//To check whether the lot number is already in grid or not
	if(validateObject(gridObj)){
		gridObj.forEachRow(function(rowId){
			// get the control number from the grid
			ctrlNum = gridObj.cellById(rowId, 1).getValue();
			if(lotCode == ctrlNum){
				flag = false;
			}		
		});
	}
	if(!flag){
		document.getElementById("errMsg").innerHTML = "<font color=red><b>"+message_operations[78]+"</b></font>";
		dupLotNumFl = true;
	}else{
		document.getElementById("errMsg").innerHTML = "";
		dupLotNumFl = false;
	}
		
	if (strCostPrice!=undefined && strCostPrice<='0')
      	{
		document.getElementById("errMsg").innerHTML = "<font color=red><b>"+message_operations[857]+"</b></font>";
		costpricefl=true;
		}	
	else if (strpartstatus!=undefined && strpartstatus!='Y')
	
		{
			document.getElementById("errMsg").innerHTML = "<font color=red><b>"+message_operations[865]+"</b></font>";
			costpricefl=true;
	
		}
		
		else
		{
			document.getElementById("errMsg").innerHTML = "";
			costpricefl=false;
		}
    
	
}

//Function to open the window when click on "D" icon
function fnCallDonor(){
	var donorNum = document.frmPOBulkReceive.donorNum.value;
	fnValidateTxtFld('donorNum',message_operations[191]);
	if(ErrorCount>0){
		Error_Show();
        Error_Clear();
        return false;
	}
	var vendorId = document.frmPOBulkReceive.vid.value;
	document.frmPOBulkReceive.donorNum.focus();
	windowOpener("/gmDonorInfo.do?companyInfo="+compInfObj+"&method=fetchDonorParamDtls&strOpt=load&screenType=recShip&FORMNAME=frmDonorInfo&hDonorNum="+donorNum+"&donorNo="+donorNum+"&hVendorId="+vendorId,"PrntInv","resizable=yes,scrollbars=yes,top=300,left=550,width=585,height=350");

}

//Function to calculate the expiry date
function fnCalculateExpDate(obj){
	var mfgDate = obj.value;
	var mfgDateObj = document.frmPOBulkReceive.mfgDate;
	var hmfgDate = document.frmPOBulkReceive.hMfgDate.value;
	var expDate = document.frmPOBulkReceive.expDate.value;
	CommonDateValidation(mfgDateObj,format,Error_Details_Trans(message_operations[74],format));
	if(ErrorCount>0){
		Error_Show();
        Error_Clear();
        return false;
	}
	
	//if(mfgDate != hmfgDate || expDate == ''){//should call ajax, only if the date is changed
		var shelfLife = document.frmPOBulkReceive.hShelfLife.value;
		

		dhtmlxAjax.get('/gmPOBulkReceive.do?companyInfo='+compInfObj+'&method=fetchExpiryDate&mfgDate='+mfgDate+'&hShelfLife='+shelfLife+ '&ramdomId=' + Math.random(),function(loader){
			var response = loader.xmlDoc.responseText;
			if((response != null || response != '') ){
				document.frmPOBulkReceive.expDate.value = response;
			}
		});
		document.frmPOBulkReceive.hMfgDate.value = mfgDate;
	//}
}

//function to set focus 
function fnFocus(obj){
	if(event.keyCode == 13){//Need to work while clicking the button as well as pressing enter key
		var mfgDate = document.frmPOBulkReceive.mfgDate.value;
		var expDate = document.frmPOBulkReceive.expDate.value;
		if(mfgDate == ''){
			document.frmPOBulkReceive.mfgDate.focus();
		}else if(expDate == ''){
			document.frmPOBulkReceive.expDate.focus();
		}else{
			document.frmPOBulkReceive.customSize.focus();
		}
		
		 // used below codes to stop reloading the page after scanning the id
		if(!e) var e = window.event;

	     e.cancelBubble = true;
	     e.returnValue = false;	

	     if (e.stopPropagation) {
	             e.stopPropagation();
	             e.preventDefault();
	     }
	}
}

//this function is used to remove the selected rows
function removeSelectedRow(){
	
	var gridrow=gridObj.getSelectedRowId();
	var colStr ;
	var slNum = 0;
	var sizeVal;
	if(gridrow!=undefined){
			gridObj.deleteRow(gridrow);
			delrows = delrows +1;
			gridObj.forEachRow(function(rowId) {
				gridObj.cellById(rowId, 0).setValue(++slNum); 
		   });

	}else{
		Error_Clear();
		Error_Details(message_operations[8]);				
		Error_Show();
		Error_Clear();
	}
}

// when click on Submit button
function fnSubmitBulkDHR(obj){
	var confirmMsg = true;
	var partNum, ctrlNum, custsize, expdate, manfdate, dateDiffs;
	var inputString = '';	
	var lotString   = '';
	var recCurrDiff;
	var recDateObj = document.frmPOBulkReceive.receivedDt;
	var stgDtObj = document.frmPOBulkReceive.stgDt;
	var stgDate =stgDtObj.value;
	
	fnValidateTxtFld('receivedDt',message_operations[192]);
	fnValidateTxtFld('stgDt',message_operations[802]); //Stage Date cannot be left blank
	
	if (stgDtObj.value!= '' && stgDtObj.value < currdate){ //Stage Date cannot be past date
     Error_Details(message_operations[803]);
	}
	
	fnValidateTxtFld('packSlipId',message_operations[193]);
	fnValidateTxtFld('donorNum',message_operations[191]);
	CommonDateValidation(recDateObj,format,Error_Details_Trans(message_operations[79],format));	
	recCurrDiff = dateDiff(recDateObj.value, currdate, format);
	
	// Received date should not be greater that current date
	if (recDateObj.value != '' && recCurrDiff < 0){
		Error_Details(message_operations[80]);
	}
	
	fnCheckDonorNum(document.frmPOBulkReceive.donorNum);// to check whether the donor information entered or not 
	
	if(!donorNumExistFl){
		Error_Details(message_operations[81]);
	}
	
	if (ErrorCount > 0)
    {
        Error_Show();
        Error_Clear();
        return false;
    }

	// Loop through the rows and get the string for selected rows 
	gridObj.forEachRow(function(rowId){
		
		// get the selected value from the grid
		ctrlNum = gridObj.cellById(rowId, 1).getValue();
		partNum = gridObj.cellById(rowId, 2).getValue();
		custsize = gridObj.cellById(rowId, 3).getValue();
		expdate = gridObj.cellById(rowId, 5).getValue();
		manfdate = gridObj.cellById(rowId, 6).getValue();
		inputString = inputString + partNum + '^'+ ctrlNum.toUpperCase() + '^' + custsize + '^' + expdate + '^' + manfdate + '^'+ '^'+ '^'+ '^'+ '1' +'|';
		lotString = lotString + ctrlNum.toUpperCase() + ',';
	});
	
	if(inputString == ''){
		Error_Details(message_operations[82]);
		fnStopProgress();
		Error_Show();
        Error_Clear();
        return false;
	}else{
		
		dhtmlxAjax.get('/gmPOBulkReceive.do?companyInfo='+compInfObj+'&strOpt=LOTNUMBERS&method=validateLotNumber&hLotString='+lotString+'&ramdomId=' + Math.random(),function(loader){
			var response = loader.xmlDoc.responseText;
			//var responseArray = response.split("@");
			if(response != '' ){
				document.getElementById("errMsg").innerHTML = "<font color=red><B>"+message_operations[83]+"<BR>"+response+"</B></font>";
		        return false;
			}else{
				confirmMsg = confirm(message_operations[84]); 
				if(!confirmMsg){
			    	return false
			    }
				fnStartProgress();
				document.frmPOBulkReceive.haction.value = 'SAVE_BULK_DHR';
				document.frmPOBulkReceive.strOpt.value = 'loadRS';
			 	document.frmPOBulkReceive.hInputString.value = inputString;
			 	document.frmPOBulkReceive.hLotString.value = lotString;
				document.frmPOBulkReceive.action = '/gmPOBulkReceive.do?companyInfo='+compInfObj+'&method=saveBulkDHR';				 
			 	document.frmPOBulkReceive.submit();
			}
		});	
	}
}

var mCal='';
var calendars = {};

function showSglCalendar(calendardiv,textboxref,dateformat){
	
	// Overite style for calendar only from class RightTableCaption.
	onSetStyleToCalendar(calendardiv);
	if (dateformat == null){
		dateformat='%m/%d/%Y';
	}
	
	if(!calendars[calendardiv]) 
		calendars[calendardiv]= initMfgCal(calendardiv,textboxref,dateformat);
	
	//This line for today date will display in BOLD and this method will be in dhtmlxcalendar.js
	calendars[calendardiv].enableTodayHighlight(true); // or false
	
	if(document.getElementById(textboxref).value.length>0){
		calendars[calendardiv].setDate(document.getElementById(textboxref).value);
	}

	if(calendars[calendardiv].isVisible())
		calendars[calendardiv].hide();
	else 
		calendars[calendardiv].show();
}

// The following new changes will applicable to current calendar.  
function initMfgCal(calendardiv,textboxref,dateformat){
		if((navigator.userAgent.match(/iPhone/i))!=null || (navigator.userAgent.match(/iPad/i))!=null)
		{
			//This commented code for New version 3.5. The old dhtmlx version we can't editable month and year in IPAD. Now we can edit.
			//mCal = new dhtmlxCalendarObject(calendardiv, false, {isMonthEditable: false,isYearEditable: false });
			mCal = new dhtmlXCalendarObject(calendardiv);
		}else{
			//mCal = new dhtmlxCalendarObject(calendardiv, false, {isMonthEditable: true,isYearEditable: true});
			mCal = new dhtmlXCalendarObject(calendardiv);
		}

	// This range only able to select date. 
	mCal.setSensitiveRange("1970-01-01", "2500-31-12"); 

	// Set date format based on login user.
	mCal.setDateFormat(dateformat);
	
	// Hide the time in new dhtmlx version 3.5
	mCal.hideTime();
	
	// When click calendar icon and set the date in textbox.
	mCal.attachEvent("onClick", function(d) {
		document.getElementById(textboxref).value =  mCal.getFormatedDate(dateformat, d);
		calendars[calendardiv].hide();
		fnCalculateExpDate(document.getElementById(textboxref));
		return true;
	 });
	
	mCal.hide();
	return mCal;
}
// To clear the image
function fnClearDiv(){
	document.getElementById("DivShowDonorIDExists").style.display='none';
    document.getElementById("DivShowDonorIDNotExists").style.display='none';	
}

var validMessage;
var donorNumExistFl = false;

// To check whether the donor number already exist or not
function fnCheckDonorNum(donorObj){	
	var donorNum = donorObj.value;
	var vendorId = document.frmPOBulkReceive.vid.value;
	fnClearDiv();		
	if(TRIM(donorNum) == ''){
		validMessage = '';
		return false;
	}
	validMessage = '';
	dhtmlxAjax.get('/gmPOBulkReceive.do?companyInfo='+compInfObj+'&method=validateDonorNum&donorNum='+donorNum+"&vid="+vendorId+ '&ramdomId=' + Math.random(),function(loader){
		var response = loader.xmlDoc.responseText;
		if(response == '0'){// Count '0' means, donor number is not existing
			donorNumExistFl = false;
			document.getElementById("DivShowDonorIDExists").style.display = 'none';
        	document.getElementById("DivShowDonorIDNotExists").style.display = 'inline';
		}else{
			donorNumExistFl  = true;
			document.getElementById("DivShowDonorIDExists").style.display = 'inline';
        	document.getElementById("DivShowDonorIDNotExists").style.display = 'none';
		}
	});
	
}

function fnvalidateLot(lotNumber){
	lotNumber = lotNumber.toUpperCase();
	dhtmlxAjax.get('/gmPOBulkReceive.do?companyInfo='+compInfObj+'&strOpt=LOT&method=validateLotNumber&lotCode='+lotNumber+'&ramdomId=' + Math.random(),function(loader){
		var response = loader.xmlDoc.responseText;
		var responseArray = response.split("@");
		if(responseArray[0] > 0 ){
			var array = [lotNumber,responseArray[1]];
			document.getElementById("errMsg").innerHTML = "<font color=red>"+Error_Details_Trans(message_operations[85],array)+"</font>";
			document.frmPOBulkReceive.lotCode.value = '';
	        return false;
		}
	});	
}
