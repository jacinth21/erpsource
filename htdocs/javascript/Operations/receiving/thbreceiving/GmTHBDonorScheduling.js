// to hide Div in on page Load
var loadNum ='';
var checkVal = '';
var loadid = '';
var donornum = '';
var qtyAllocated = '';
var qtyNotAllocated = '';
var stageDt = '';
var rsflag='';
var donorNumString='';
function fnOnPageLoad() {
	if (objScheduleJSONData == "") {
		document.getElementById("donorSchedulidGridDiv").style.display = 'none';
		document.getElementById("thbDash_1").style.display = 'none';
		document.getElementById("DivExportExcel").style.display = 'none';
		document.getElementById("DivNothingMessage").style.display = 'none';
		document.getElementById("thbchoose").style.display = 'none';
	}
}
//function to clear the content of div
function clearcontent(elementID) {
	document.getElementById(elementID).innerHTML = "";
}
//function to initiate the layout for the Refresh screen
function fnLayoutDonorStage()
{
	var loadNumber = document.frmTHBDonorSchedule.strLoadNum.value;
	dhxLayout_1 = new dhtmlXLayoutObject("thbDash_1", "1C");
	dhxLayout_1.cells("a").setText(
					"<a onclick='fnReloadCell()' style=\"cursor:hand;\"><img src=\"/images/icon_btn_refresh.png\" style=\"width: 18px; height: 17px;\" title=\"Refresh\"></a>");
	dhxLayout_1.cells("a").attachURL("/GmTHBStageDateDatagrid.html?objCell=Stage&popupName=StageDetails&loadNum="+loadNumber+"&ramdomId="
							+ Math.random());
}
//function to load the details to the screen
function fnGetDonorSchedDtls() {
	clearcontent('donorSchedulidGridDiv');
	clearcontent('thbDash_1');
	 
	loadNum =  document.frmTHBDonorSchedule.strLoadNum.value;
	if (isNaN(loadNum)) {
		Error_Details("Please enter valid THB LoadNum");
	}
	
	 if (ErrorCount > 0) {
			Error_Show();
			Error_Clear();
			return false;
		}

	
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmTHBDonorScheduling.do?method=fetchTHBDonorSchedulingDtls&strLoadNum='
			+ loadNum + '&ramdomId=' + Math.random());
	var loader = dhtmlxAjax.getSync(ajaxUrl);
	
	fnDisplayGrid(loader);
	fnLayoutDonorStage();
}

var check_rowId = '';
var load_rowId = '';
var donor_rowId = '';
var qtyAllocated_rowId = '';
var qtyNotAllocated_rowId = '';
var rs_flag_row_id="";
var stageDt_rowId = "";


// to generate the grid
function fnDisplayGrid(loader) {
	var response = loader.xmlDoc.responseText;
	if (response == '' || response == null || response == undefined) {
		document.getElementById("donorSchedulidGridDiv").style.display = 'none';
		document.getElementById("thbDash_1").style.display = 'none';
		document.getElementById("DivExportExcel").style.display = 'none';
		document.getElementById("DivNothingMessage").style.display = 'block';
		document.getElementById("thbchoose").style.display = 'none';
	} else {
		var rows = [], i = 0;
		var JSON_Array_obj = JSON.parse(response);		
		$.each(JSON_Array_obj, function(index, jsonObject) {
			loadNum = jsonObject.Load_Num;
			donor = jsonObject.Donor_Num;
			age = jsonObject.Age;
			sex = jsonObject.Sex;
			units = jsonObject.No_Of_Units;
			qtyAllocated = jsonObject.Qty_Allocated;
			qtyNotAllocated = jsonObject.Qty_Not_Allocated;
			stageDt = jsonObject.stgdt;
			rsflag = jsonObject.rsflag;

			// default array
			rows[i] = {};
			rows[i].id = donor;
			rows[i].data = [];
			
			// set the value to the columns
			//rows[i].id = loadNum;
			rows[i].data[0] = '';
			rows[i].data[1] = loadNum;
			rows[i].data[2] = donor;
			rows[i].data[3] = age;
			rows[i].data[4] = sex;
			rows[i].data[5] = units;
			rows[i].data[6] = qtyAllocated;
			rows[i].data[7] = qtyNotAllocated;
			rows[i].data[8] = stageDt;
			rows[i].data[9] = rsflag;

			i++;
		});
		var setInitWidths = "40,75,75,75,75,75,75,100,100";
		var setColAlign = "center,center,center,center,center,center,center,center,center,center";
		var setColTypes = "ch,ro,ro,ro,ro,ro,ro,ro,dhxCalendarA,ro";
		var setColSorting = "na,str,str,str,str,str,str,str,str,date,str";
		var setHeader = ["Select All", "Load#", "Donor#", "Age", "Sex", "# Of Units",
				"Qty Allocated", "Qty Not Allocated", "Stage Date" , "RS FLAG" ];
		var setFilter = [ "<input type='checkbox' value='no'  name='selectAll' onClick='javascript:fnChangedFilter(this);'/>", "#numeric_filter", "#numeric_filter",
				"#numeric_filter", "#text_filter", "#numeric_filter",
				"#numeric_filter", "#numeric_filter", "#text_filter","#text_filter" ];
		var setColIds = "check,load,donorNum,age,sex,units,qtyAllocated,qtyNotAllocated,stageDt,rsflag";
		var enableTooltips = "true,true,true,true,true,true,true,true,true,true";
		var footerArry = [];
		var gridHeight = "";
		var footerStyles = [];
		var footerExportFL = true;
		var dataHost = {};
		var pagination = "";
		dataHost.rows = rows;
		document.getElementById("donorSchedulidGridDiv").style.display = 'block';
		document.getElementById("thbDash_1").style.display = 'block';
		document.getElementById("DivNothingMessage").style.display = 'none';
		document.getElementById("DivExportExcel").style.display = 'block';
		document.getElementById("thbchoose").style.display = 'block';
		gridObj = initGridData('donorSchedulidGridDiv', gridHeight, dataHost,
				setHeader, setInitWidths, setColAlign, setColTypes,
				setColSorting, setColIds, enableTooltips, setFilter,
				footerArry, footerStyles, footerExportFL, pagination);
		gridObj.enableEditTabOnly(true);
		gridObj.enableEditEvents(true, false, true);
		gridObj.attachEvent("onEditCell", doOnCellEdit);
		gridObj.attachEvent("onCheckbox", doOnCheck);
	    gridObj.enableBlockSelection(true);
		gridObj.forceLabelSelection(true);  
		gridObj.attachEvent("onKeyPress", keyPressedEvnt);
	
		setFooterAlign="center"
		document.getElementById('donorSchedulidGridDiv').setAttribute("style","width:1500x");
        footerArry = [];
	    gridHeight = "680";	
		var gridrows = gridObj.getAllRowIds(",");
		var gridrowsarr = gridrows.toString().split(",");
		var arrLen = gridrowsarr.length;
		
		check_rowId = gridObj.getColIndexById("check");
		load_rowId = gridObj.getColIndexById("load");
		donor_rowId = gridObj.getColIndexById("donorNum");
		qtyAllocated_rowId = gridObj.getColIndexById("qtyAllocated");
		qtyNotAllocated_rowId = gridObj.getColIndexById("qtyNotAllocated");		
		stageDt_rowId = gridObj.getColIndexById("stageDt");
		rs_flag_row_id = gridObj.getColIndexById("rsflag");
		document.getElementById("DivExportExcel").style.display = 'block';
		
	}
}
// to download excel
function fnDownloadXLS() {
	gridObj.toExcel('/phpapp/excel/generate.php');
}
//to reload the Refresh screen
function fnReloadCell() {
     loadNum = document.frmTHBDonorSchedule.strLoadNum.value;
	dhxLayout_1.cells("a").attachURL(
			"/GmTHBStageDateDatagrid.html?&loadNum="+loadNum+"&ramdomId="
					+ Math.random());
}

function doOnCheck(rowId, cellInd, stage) {
	rowID = rowId;
	//fnSetValue(rowID);
	if (cellInd == check_rowId) {
		fnSelectAllToggle('selectAll');
	}
	return true;
}
/*function fnSetValue(rowId) {
	var checkVal = '';
	var loadnm = '';	
	checkVal = gridObj.cellById(rowId, check_rowId).getValue();
	loadnm = gridObj.cellById(rowId, load_rowId).getValue();
	donornum = gridObj.cellById(rowId, donor_rowId).getValue();
	qtyAllocated = gridObj.cellById(rowId, qtyAllocated_rowId).getValue();
	qtyNotAllocated = gridObj.cellById(rowId, qtyNotAllocated_rowId).getValue();
	if (checkVal == 1) {
		loadid = loadnm;
	}
	rsflag = gridObj.cellById(rowId, rs_flag_row_id).getValue();
	stageDt=gridObj.cellById(rowId, stageDt_rowId).getValue();

}*/
//function called when choose action dropdown is selected and submitted
function fnChooseAction() {
	var v_action_val = document.frmTHBDonorSchedule.Choose_Action.value;
	if (v_action_val == '0') {
		v_fnAction = eval('fnDoAction')
	} else {
		v_fnAction = eval('fnCreateRS')
	}
	
		v_fnAction(loadid);

}
//function called if the choose one is selected 
function fnDoAction(loadid) {
	Error_Details(message_operations[859]);

	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
}
	
//this function called if the dropdown is selected as Schedule
function fnCreateRS(loadid) {
	var checkVal = '';
	var loadnm = '';	
	var donorNumString='';
	var rsFlagErrString='';
	var qtyAllocatedErrStr='';
	var stgeDtErrStr='';
	
	 checked_row = gridObj.getCheckedRows(check_rowId);
     if (checked_row == '') {
		Error_Details(message_operations[858]);
	}
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	if (checked_row != '') {
		 chkVal = checked_row.split(",");
	   for (var i = 0; i < chkVal.length; i++) {
		checkVal = gridObj.cellById(chkVal[i], check_rowId).getValue();
		if (checkVal == 1) {
		   loadnm = gridObj.cellById(chkVal[i], load_rowId).getValue();
	       donornum = gridObj.cellById(chkVal[i], donor_rowId).getValue();
	       qtyAllocated = gridObj.cellById(chkVal[i], qtyAllocated_rowId).getValue();
	       qtyNotAllocated = gridObj.cellById(chkVal[i], qtyNotAllocated_rowId).getValue();
	       rsflag = gridObj.cellById(chkVal[i], rs_flag_row_id).getValue();
	       stageDt=gridObj.cellById(chkVal[i], stageDt_rowId).getValue();

		loadid = loadnm;
		donorNumString += donornum+',';
		
		if(rsflag=='Y'){
			rsFlagErrString +=donornum +',';
		}
		if(qtyNotAllocated != 0){
			qtyAllocatedErrStr +=donornum+',';
		}
		if(stageDt==null || stageDt=='')
		{
			stgeDtErrStr +=donornum+',';
		}
		
	}
	    }
	}
	
	
	if (loadid == '') {
		Error_Details(message_operations[858]);
	}
	 	
	else if(stgeDtErrStr !='')
		{
		stgeDtErrStr = stgeDtErrStr.substring(0, stgeDtErrStr.length - 1); // to remove last Comma
    	Error_Details(message_operations[862] + stgeDtErrStr);

		}
    else if(qtyAllocatedErrStr != ''){
         qtyAllocatedErrStr = qtyAllocatedErrStr.substring(0, qtyAllocatedErrStr.length - 1); // to remove last Comma
    	Error_Details(message_operations[860] + qtyAllocatedErrStr);
	}
	
    else if (rsFlagErrString !=''){
		    rsFlagErrString = rsFlagErrString.substring(0, rsFlagErrString.length - 1); // to remove last Comma
	    	Error_Details(message_operations[861] + rsFlagErrString);
		}
		
	
	else{
		windowOpener("/gmTHBDonorScheduling.do?method=loadTHBDonorPO&strLoadNum="+loadid+"&strDonorNum="+donorNumString ,
				"THB Donor Schedule", "resizable=yes,scrollbars=yes,top=150,left=150,width=1050,height=600");
	}
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
}
function fnSubmit(){
	var ponumber = document.frmTHBDonorSchedule.strPoNum.value;
	fnStartProgress();
	var ajaxURL = fnAjaxURLAppendCmpInfo('/gmTHBDonorScheduling.do?method=createRS&strLoadNum='+loadnumber+'&strDonorNum='+donornumber+'&strPoNum='+ponumber+'&ramdomId='+Math.random());
	dhtmlxAjax.get(ajaxURL, fnCallBack);
	return true;
	}

function doOnCellEdit(stage, rowId, cellInd, nValue, oValue) {

	if(nValue!=''){
	strdonor = gridObj.cellById(rowId, 2).getValue();
	strload = gridObj.cellById(rowId, 1).getValue();
	
	if (stage == 2) {
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmTHBDonorScheduling.do?&method=SavDonorStageDate&strStageDt='
				+ nValue
				+ '&strDonorNum='
				+ strdonor
				+ '&strLoadNum='
				+ strload + '&ramdomId=' + Math.random());
		dhtmlxAjax.get(ajaxUrl, fnCallBack);
	}
	return true;
  }
	
}

// This function is called front end show Error Message for Stage date is not within the Analysis processing dates
function fnCallBack(loader){
	var status = loader.xmlDoc.status;
	var errorDet = loader.xmlDoc.responseText;
	var errorPart = errorDet.split('^');
	if(status == 200){
		document.getElementById("successCont").innerHTML = "<font><center><b><h4>RS Creation Job is scheduled. Once completed, you will be notified through email.</h4></b></center></font>";
		document.frmTHBDonorSchedule.Btn_Load.disabled = true;
		fnStopProgress();
	}
	else if(status == 300){
		Error_Details(errorPart[1]);
		if (ErrorCount > 0) {
			Error_Show();
			Error_Clear();
			return false;
		}
	}
	return null;
}

// This function is called while initiating the grid with customized dropdown
function initGridData(divRef, gridHeight, gridData, setHeader, setInitWidths,
		setColAlign, setColTypes, setColSorting, setColIds, enableTooltips,
		setFilter, footerArry, footerStyles, footerExportFL, pagination) {

	var gObj = new dhtmlXGridObject(divRef);
	gObj.enableColSpan(true);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/extweb/dhtmlx/dhtmlxGrid/imgs/");
	gObj.setDateFormat("%m/%d/%Y","%m/%d/%Y");

	if (setHeader != undefined && setHeader.length > 0) {
		gObj.setHeader(setHeader);
		var colCount = setHeader.length;
	}
	// this function is to add the calendar in grid header
	gObj._in_header_custom_calendar = function(tag, index, data) {
		tag.innerHTML = '<input type="text" size="10" name="mfgDate" id="mfgDate" class="InputArea" onFocus="changeBgColor(this,\'#AACCE8\');" onBlur="changeBgColor(this,\'#ffffff\');" tabindex=9><img id="Img_Date" style="cursor:hand" onclick="javascript:showSglCalendar(\'dcalendardiv\',\'mfgDate\');" title="Click to open Calendar"  src="<%=strImagePath%>/nav_calendar.gif" border=0 align="absmiddle" height=18 width=19 />	<div id="dcalendardiv" style="position: absolute; z-index: 10;"></div>';
	}

	if (setInitWidths != "")
		gObj.setInitWidths(setInitWidths);
	if (setColAlign != "")
		gObj.setColAlign(setColAlign);
	if (setColTypes != "")
		gObj.setColTypes(setColTypes);
	if (setColSorting != "")
		gObj.setColSorting(setColSorting);
	if (enableTooltips != "")
		gObj.enableTooltips(enableTooltips);
	if (setFilter != "")
		gObj.attachHeader(setFilter);
	if (footerArry != undefined && footerArry.length > 0) {
		var footstr = eval(footerArry).join(",");
		if (footerStyles != undefined && footerStyles.length > 0)
			gObj.attachFooter(footstr, footerStyles);
		else
			gObj.attachFooter(footstr);
	}

	if (setColIds != "") {
		gObj.setColumnIds(setColIds);
	}

	if (gridHeight != "")
		gObj.enableAutoHeight(true, gridHeight, true);
	else
		gObj.enableAutoHeight(true, 400, true);

	gObj.init();
	if (pagination == "Y") {
		gObj.enablePaging(true, 100, 10, "pagingArea", true);
		gObj.setPagingSkin("bricks");
	}

	gObj.parse(gridData, "json");
	return gObj;
}

function showSglCalendar(calendardiv, dateformat) {
	// Overite style for calendar only from class RightTableCaption. This method
	// call to date-pciker.js.
	onSetStyleToCalendar(calendardiv);

	if (dateformat == null) {
		dateformat = '%m/%d/%Y';
	}
	if (!calendars[calendardiv])
		calendars[calendardiv] = initCalendar(calendardiv, dateformat);

	// This line for today date will display in BOLD and this method will be in
	// dhtmlxcalendar.js
	calendars[calendardiv].enableTodayHighlight(true); // or false

	if (calendars[calendardiv].isVisible())
		calendars[calendardiv].hide();
	else
		calendars[calendardiv].show();
}

// Below codes are modified for new DHTMLX version
function initCalendar(calendardiv, dateformat) {

	// Above commented code for New version 3.5. In old dhtmlx version we can't
	// edit month and year in IPAD. Now we can edit.
	mCal = new dhtmlXCalendarObject(calendardiv);

	// This range only able to select date.
	mCal.setSensitiveRange("1970-01-01", "2500-31-12");

	mCal.setDateFormat(dateformat);
	mCal.hideTime(); // Hide the time in new dhtmlx version 3.5

	// Changed the above code for new version DHTMLX. When click calendar icon
	// and set the date in textbox.
	mCal.attachEvent("onClick", function(date) {
		calendars[calendardiv].hide();
		return true;
	});
	mCal.hide();
	return mCal;
}

function fnCallGo(){	
	if(event.keyCode == 13)
	{	 
		var rtVal = fnGetDonorSchedDtls();
		if(rtVal == false || rtVal == undefined){
			if(!e) var e = window.event;

	        e.cancelBubble = true;
	        e.returnValue = false;

	        if (e.stopPropagation) {
	                e.stopPropagation();
	                e.preventDefault();
	        }
		}
	}	
}
// This toggles the "Select All" checkbox. If one of the checkboxes is off, then the "select all" is checked off
function fnSelectAllToggle(varControl) {
	var objControl = eval("document.frmTHBDonorSchedule." + varControl);

	var all_row = gridObj.getAllRowIds(",");
	var all_rowLen = all_row.length; // get the rows id length
	var checked_row = gridObj.getCheckedRows(check_rowId);
	var finalval;

	finalval = eval(checked_row.length);
	
	if (all_rowLen == finalval) {
		objControl.checked = true;// select all check box to be checked
	} else {
		objControl.checked = false;// select all check box un checked.
	}
}
//this function is called when click select all option by header checkbox
function fnChangedFilter(obj) {
	
	if (obj.checked) {
		gridObj.forEachRow(function(rowId) {
			
			var check_disable_fl = gridObj.cellById(rowId, check_rowId).isDisabled();
			if (!check_disable_fl) {
				gridObj.cellById(rowId, check_rowId).setChecked(true);
			}
		});
	} else {
		gridObj.forEachRow(function(rowId) {
			check_id = gridObj.cellById(rowId, check_rowId).getValue();
			
			if (check_id == 1) {
				gridObj.cellById(rowId, check_rowId).setChecked(false);
			}

		});
	}
}
//this function used to copy the selected value
function keyPressedEvnt(code, ctrl) {
	if (code == 67 && ctrl) {
		docopy();
	}
	if(code==86&&ctrl){
		pasteToGrid();
	}
	gridObj.refreshFilters();
	return true;
}
//this function used to copy
function docopy() {
	gridObj.setCSVDelimiter("\t");
	gridObj.copyBlockToClipboard();
	var startPoint = 0;
	arrValuefromClipboard = new Array();
	if (gridObj._selectionArea != null) {
		var area = gridObj._selectionArea;
		var leftTopCol = area.LeftTopCol;
		var leftTopRow = area.LeftTopRow;
		var rightBottomCol = area.RightBottomCol;
		var rightBottomRow = area.RightBottomRow;
		for ( var i = leftTopRow; i <= rightBottomRow; i++) {
			for ( var j = leftTopCol; j <= rightBottomCol; j++) {
				setValuefromClipboard = gridObj.cellByIndex(i, j).getValue();
				arrValuefromClipboard.push(setValuefromClipboard);
				startPoint++;
			}
		}
	}
	gridObj._HideSelection();
}

//this function used to copy
function pasteToGrid() {
		if (gridObj._selectionArea != null) {
			var area = gridObj._selectionArea;
			var leftTopCol = area.LeftTopCol;
			var leftTopRow = area.LeftTopRow;
			var rightBottomCol = area.RightBottomCol;
			var rightBottomRow = area.RightBottomRow;
		
			for ( var i = leftTopRow; i <= rightBottomRow; i++) {
			   var m = 0;
				for ( var j = leftTopCol; j <= rightBottomCol; j++) {
					var newRowId = gridObj.getRowId(i);
				    var e = gridObj.cellByIndex(i, j);
			         gridObj.cellByIndex(i, j).setValue(arrValuefromClipboard[m]);
		             gridObj.setCellTextStyle(newRowId, j,"color:black;font-weight:normal;");
					 gridObj.setRowColor(newRowId,"#9FF781");
					}
				fnSaveStageDt(newRowId,j-1,arrValuefromClipboard[m]);
				m++;
				}
			gridObj._HideSelection();
			gridObj.refreshFilters();
		} else {
			alert(message_prodmgmnt[34]);
		}
	}

//This function called while paste the stage date 
function fnSaveStageDt(rowId, cellInd, nValue) {
	
    if(nValue!=''){
	     strdonor = gridObj.cellById(rowId, 2).getValue();
	     strload = gridObj.cellById(rowId, 1).getValue();
	if(cellInd == '8'){
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmTHBDonorScheduling.do?&method=SavDonorStageDate&strStageDt='
				+ nValue
				+ '&strDonorNum='
				+ strdonor
				+ '&strLoadNum='
				+ strload + '&ramdomId=' + Math.random());
		dhtmlxAjax.get(ajaxUrl, fnCallBack);
	}
	}
}