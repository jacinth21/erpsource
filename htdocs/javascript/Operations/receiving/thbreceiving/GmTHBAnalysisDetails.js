
// to hide Div in on page Load
function fnOnPageLoad() {

	if (objGridJSONData == "") {
		document.getElementById("dataGridDiv").style.display = 'none';
		document.getElementById("DivExportExcel").style.display = 'none';
		document.getElementById("DivNothingMessage").style.display = 'block';
		document.getElementById("completeBtnRow").style.display = 'none';
	
	}else{
	    fnDisplayGrid();
	}
}
// to disable the fourth field of column filter based on the qty value
function fnDisableDropDown(obj) {

	var qty = obj.value;
	if (qty != '') {
		document.frmTHBAnalysisDetails.partType.disabled = true;
	} else {
		document.frmTHBAnalysisDetails.partType.disabled = false;
	}
}
// to disable the third field of column filter based on Fourth Filed Selection
function fnDisableTextField(obj) {
	var partType = document.frmTHBAnalysisDetails.partType.value;
	if (partType != '0') {
		document.frmTHBAnalysisDetails.qty.disabled = true;
	} else {
		document.frmTHBAnalysisDetails.qty.disabled = false;
	}
}

// to fetch the THB analysis details based on the base part , private part ,
// column filter
function fnFetchDetials(obj) {
	var frm = document.frmTHBAnalysisDetails;

	var columnFilterId = document.frmTHBAnalysisDetails.columnFilter.value;
	var compTypeId = document.frmTHBAnalysisDetails.comparType.value;
	var qtyId = document.frmTHBAnalysisDetails.qty.value;
	var partTypeId = document.frmTHBAnalysisDetails.partType.value;
	var basePart = document.frmTHBAnalysisDetails.basePart.value;
	var privatePart = document.frmTHBAnalysisDetails.privatePart.value;

	if (basePart != "") {
		frm.hBasePartStr.value = basePart;
	} 
	if (privatePart != "") {
		frm.hPrivatePartStr.value = privatePart;
	} 
	if(partTypeId =='0'){
		frm.hColumnFilterStr.value = columnFilterId +','+compTypeId +','+qtyId;
	}
	if(qtyId ==''){
		frm.hColumnFilterStr.value = columnFilterId +','+compTypeId +','+partTypeId;
	}
    if(columnFilterId =='0' || (qtyId == '' && partTypeId =='0')){
    	Error_Details(message_operations[785]);	
    }	
    if (qtyId != '') {
		if (isNaN(qtyId)) {
			Error_Details(message_operations[787]);	
			} 	
	}
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}else{
	fnStartProgress();
	frm.submit();
	}
}
// to generate the grid
function fnDisplayGrid() {
	var basePart = "";
	var privatePart = "";
	var partDesc = "";
	var boQty = "";
	var pendingLess2Wks = "";
	var pendingGreater2Wks = "";
	var pastThreeMonthSale = "";
	var shelfQty = "";
	var dhrQty = "";
	var repackQty = "";
	var ptrdQty = "";
	var need = "";
	var radRunQty = "";
	var allocRadRun = "";
	// Getting Row id
	var basePart_rowId = "";
	var privatePart_rowId = "";
	var partDesc_rowId = "";
	var boQty_rowId = "";
	var pendingLess2Wks_rowId = "";
	var pendingGreater2Wks_rowId = "";
	var pastThreeMonthSale_rowId = "";
	var shelfQty_rowId = "";
	var dhrQty_rowId = "";
	var repackQty_rowId = "";
	var ptrdQty_rowId = "";
	var need_rowId = "";
	var radRunQty_rowId = "";
	var allocRadRun_rowId = "";
	var analysisId="";
    
	if (objGridJSONData != '') {
		
		var rows = [] , i = 0;
		var JSON_Array_obj = "";
		JSON_Array_obj = JSON.parse(objGridJSONData);
		
		$.each(JSON_Array_obj,function(index, jsonObject) {
			
	        analysisId= jsonObject.ANALYSISID;
			basePart = jsonObject.BASE_PART;
			privatePart = jsonObject.PRIVATE_PART;
			partDesc = jsonObject.PART_DESCRIPTION;
			boQty = jsonObject.BO_QTY;
			pendingLess2Wks = jsonObject.PEND_2_WEEKS;
			pendingGreater2Wks = jsonObject.PEND_AFTER_2_WEEKS;
			pastThreeMonthSale = jsonObject.PAST_3_MONTH_SALE;
			shelfQty = jsonObject.SHELF_QTY;
			dhrQty = jsonObject.DHR_QTY;
			repackQty = jsonObject.REPACK_QTY;
			ptrdQty = jsonObject.PTRD_QTY;
			need = jsonObject.NEED_QTY;
			radRunQty = jsonObject.RAD_RUN_QTY;
			allocRadRun = jsonObject.ALLOCATED_QTY;
		 
			// default array
			rows[i] = {};
			rows[i].id = i;
			rows[i].data = [];
			rows[i].data[0] = basePart;
			rows[i].data[1] = privatePart;
			rows[i].data[2] = partDesc;
			if(boQty == null || boQty =='0'){
				rows[i].data[3] = '0';
			}else{
			rows[i].data[3] = "<a href='#' onClick=fnOpenInventoryLookUp('" + boQty 
			                    + "','"+basePart+"','"+privatePart+"','BACKORDER','"+analysisId+"')><u>" + boQty + "</u></a>";
			}
			if(pendingLess2Wks == null || pendingLess2Wks == '0'){
			rows[i].data[4] = '0';
			}else{
				rows[i].data[4] = "<a href='#' onClick=fnOpenInventoryLookUp('" + pendingLess2Wks
                + "','"+basePart+"','"+privatePart+"','PEND2WEEKS','"+analysisId+"')><u>" + pendingLess2Wks + "</u></a>";
			}
			if(pendingGreater2Wks == null || pendingGreater2Wks == '0'){
			rows[i].data[5] = '0';
			}else{
				rows[i].data[5] = "<a href='#' onClick=fnOpenInventoryLookUp('" + pendingGreater2Wks
                                   + "','"+basePart+"','"+privatePart+"','PENDAFTER2WEEKS','"+analysisId+"')><u>" + pendingGreater2Wks + "</u></a>";
			}
			rows[i].data[6] = pastThreeMonthSale;
			rows[i].data[7] = shelfQty;
			rows[i].data[8] = dhrQty;
			rows[i].data[9] = repackQty;
			if(ptrdQty== null || ptrdQty == '0'){
			rows[i].data[10] = '0';
			}else{
				rows[i].data[10] = "<a href='#' onClick=fnOpenInventoryLookUp('" + ptrdQty
                + "','"+basePart+"','"+privatePart+"','PTRD','"+analysisId+"')><u>" + ptrdQty + "</u></a>";
			}
			
			rows[i].data[11] = need;
			if(radRunQty == null || radRunQty =='0'){
				rows[i].data[12] = '0';
			}else if(pendingQty == '0'){
				rows[i].data[12] = radRunQty;
		     }else{
					rows[i].data[12] = "<a href='#' onClick=fnOpenDonorAllocation('" + radRunQty
		            + "','"+basePart+"','"+privatePart+"','radRunQty','"+analysisId+"')><u>" + radRunQty + "</u></a>";
			}
			
			rows[i].data[13] = allocRadRun;
			i++;
	});

		var setInitWidths = "100,100,*,70,70,70,70,70,70,70,70,70,70,70";
		var setColAlign = "left,left,left,right,right,right,right,right,right,right,right,right,right,right";
		var setColTypes = "ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro";
		var setColSorting = "str,str,str,str,str,str,str,str,str,str,str,str,str,str";
		var setHeader = [ "Base Part#", "Private Part#", "Part Description",
				"B.O Qty", "Pending < 2 Wks", "Pending > 2 Wks",
				"Past 3 Mo. Sale", "Shelf Qty", "DHR Qty", "Repack Qty",
				"PTRD Qty", "Need", "Rad Run Qty", "Alloc.in Rad Run" ];
		var setFilter = [ "#text_filter", "#text_filter", "#text_filter",
				"#text_filter", "#text_filter", "#text_filter", "#numeric_filter",
				"#numeric_filter", "#numeric_filter", "#numeric_filter", "#text_filter",
				"#text_filter", "#text_filter", "#numeric_filter" ];
		var setColIds = "basePart,privatePart,partDesc,boQty,pendIn2Wks,pendAfter2Wks,past3Months,shelfQty,dhrQty,repackQty,ptrdQty,need,radRunQty,allocRadRun";
		var enableTooltips = "true,true,true,true,true,true,true,true,true,true,true,true,true,true";
		var footerArry = [];
		var gridHeight = "700";
		var footerStyles = [];
		var footerExportFL = true;
		var dataHost = {};
		var pagination = "";
		dataHost.rows = rows;

		document.getElementById("dataGridDiv").style.display = 'block';

		gridObj = loadDHTMLXGrid('dataGridDiv', gridHeight, dataHost,
				setHeader, setInitWidths, setColAlign, setColTypes,
				setColSorting, setColIds, enableTooltips, setFilter,
				footerArry, footerStyles, footerExportFL, pagination);
		gridObj.enableBlockSelection(true);
		gridObj.attachEvent("onKeyPress", keyPressed);
		// to set background colour for each cell
		var gridrows = gridObj.getAllRowIds(",");
		var gridrowsarr = gridrows.toString().split(",");
		var arrLen = gridrowsarr.length;
		basePart_rowId = gridObj.getColIndexById("basePart");
		privatePart_rowId = gridObj.getColIndexById("privatePart");
		partDesc_rowId = gridObj.getColIndexById("partDesc");
		boQty_rowId = gridObj.getColIndexById("boQty");
		pendingLess2Wks_rowId = gridObj.getColIndexById("pendIn2Wks");
		pendingGreater2Wks_rowId = gridObj.getColIndexById("pendAfter2Wks");
		pastThreeMonthSale_rowId = gridObj.getColIndexById("past3Months");
		shelfQty_rowId = gridObj.getColIndexById("shelfQty");
		dhrQty_rowId = gridObj.getColIndexById("dhrQty");
		repackQty_rowId = gridObj.getColIndexById("repackQty");
		ptrdQty_rowId = gridObj.getColIndexById("ptrdQty");
		need_rowId = gridObj.getColIndexById("need");
		radRunQty_rowId = gridObj.getColIndexById("radRunQty");
		allocRadRun_rowId = gridObj.getColIndexById("allocRadRun");

		for (var i = 0; i < arrLen; i++) {
			rowId = gridrowsarr[i];
			gridObj.setCellTextStyle(rowId, basePart_rowId,
					"background-color:#8dd5ef");
			gridObj.setCellTextStyle(rowId, privatePart_rowId,
					"background-color:#8dd5ef");
			gridObj.setCellTextStyle(rowId, partDesc_rowId,
					"background-color:#8dd5ef");
			gridObj.setCellTextStyle(rowId, boQty_rowId,
					"background-color:#bab689");
			gridObj.setCellTextStyle(rowId, pendingLess2Wks_rowId,
					"background-color:#bab689");
			gridObj.setCellTextStyle(rowId, pendingGreater2Wks_rowId,
					"background-color:#bab689");
			gridObj.setCellTextStyle(rowId, pastThreeMonthSale_rowId,
					"background-color:#bab689");
			gridObj.setCellTextStyle(rowId, shelfQty_rowId,
					"background-color:#c9b9cc");
			gridObj.setCellTextStyle(rowId, dhrQty_rowId,
					"background-color:#c9b9cc");
			gridObj.setCellTextStyle(rowId, repackQty_rowId,
					"background-color:#c9b9cc");
			gridObj.setCellTextStyle(rowId, ptrdQty_rowId,
					"background-color:#c9b9cc");
			gridObj.setCellTextStyle(rowId, need_rowId,
					"background-color:#d69d98");
			gridObj.setCellTextStyle(rowId, radRunQty_rowId,
					"background-color:#edc597");
			gridObj.setCellTextStyle(rowId, allocRadRun_rowId,
					"background-color:#6aa35e");
		}
		if(document.frmTHBAnalysisDetails.btn_Complete_Analysis){
			if((status == 'Completed' || completeBtnAccess == 'true' || SwitchUserFl =='Y')){
				document.frmTHBAnalysisDetails.btn_Complete_Analysis.disabled = true;
			}else{
				document.frmTHBAnalysisDetails.btn_Complete_Analysis.disabled = false;
			}
		}
		document.getElementById("DivNothingMessage").style.display = 'none';
		document.getElementById("DivExportExcel").style.display = 'block';
		if(document.getElementById("msgDiv")){
			document.getElementById("msgDiv").style.display = 'none';
		}
		
	} else {
		document.getElementById("dataGridDiv").style.display = 'none';
		document.getElementById("DivExportExcel").style.display = 'none';
		document.getElementById("DivNothingMessage").style.display = 'block';
		document.getElementById("completeBtnRow").style.display = 'none';

	}
}
// to download excel 
function fnDownloadXLS() {
	gridObj.toExcel('/phpapp/excel/generate.php');
}
function fnOpenInventoryLookUp(qty,basePart,privatePart,header,analysisId){
	windowOpener('/gmInventoryLookUpDetails.do?method=fetchInventoryLookUpDetails&strQty='+qty+'&strBase='+basePart+'&strPrivatePart='+privatePart+'&strFromLink='+header+'&stranalysisid='+analysisId,"Inventory Details","resizable=yes,scrollbars=yes,top=150,left=150,width=1050,height=610");
	
}
// this function used to load the THB Donor Allocation Screen
function fnOpenDonorAllocation(qty, basePart, privatePart, header, analysisId) {
	windowOpener("/gmDonorPartInvMap.do?method=fetchDonorAllocationDetails&strFromLink="
		+ header+'&basePart='+basePart +'&privatePart='+privatePart+'&analysisId='+analysisId,"Donor Allocation","resizable=yes,scrollbars=yes,top=100,left=100,width=1050,height=1200");
	
}
//this function used to update analysis status 
function fnCompleteAnalysis(){
	var analysisId=document.frmTHBAnalysisDetails.analysisId.value;
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmTHBAnalysisDetails.do?method=saveAnalysisStatus&analysisId='+analysisId
			+ '&ramdomId='
			+ new Date().getTime());
	dhtmlxAjax.get(ajaxUrl, function(loader){
		document.getElementById("msgDiv").style.display = 'inline-block';
		if(document.frmTHBAnalysisDetails.btn_Complete_Analysis){
			document.frmTHBAnalysisDetails.btn_Complete_Analysis.disabled = true;
		}
	});
	
}
//this function used to copy the selected value
function keyPressed(code, ctrl) {
    if (code == 67 && ctrl) {       
    	gridObj.setCSVDelimiter("\t");
    	gridObj.copyBlockToClipboard();
    } 
    return true;
}