//Depending on the header value, functions will be called
function fnOnPageLoad() {
	if (header == "PTRD") {
		fnPopulatePTRDDetails();
	} else {
		fnPopulateBackOrderDetails();
	}
}

// When header == "PTRD"
function fnPopulatePTRDDetails() {
	if (objGridData != '') {

		var rows = [], i = 0;
		var JSON_Array_obj = "";
		JSON_Array_obj = JSON.parse(objGridData);
		$.each(JSON_Array_obj, function(index, jsonObject) {
			
			load_id = i;
			base = jsonObject.Base_Part;
			private_part = jsonObject.Private_Part;
			part_desc = jsonObject.Part_Description;
			created_by = jsonObject.created_by;
			date = jsonObject.created_date;
			qty = jsonObject.QTY;
			ref_id = jsonObject.Ref_Id;
			ref_type = jsonObject.Ref_type;

			// Array
			rows[i] = {};
			rows[i].id = load_id;
			rows[i].data = [];

			rows[i].data[0] = base;
			rows[i].data[1] = private_part;
			rows[i].data[2] = part_desc;
			rows[i].data[3] = created_by;
			rows[i].data[4] = date;
			rows[i].data[5] = qty;
			rows[i].data[6] = ref_id;
			rows[i].data[7] = ref_type;
			i++;
		});
		// Grid dimensions
		var gridHeight = "";
		var setInitWidths = "70,70,*,120,100,50,70,150";
		var setColAlign = "right,right,left,left,center,right,center,center";
		var setColTypes = "ro,ro,ro,ro,ro,ro,ro,ro";
		var setHeader = [ "Base", "Private Part #", "Part Desc.",
				"Created By", "Created Date", "Qty", "Ref ID", "Ref Type" ];
		var setFilter = [ "#text_filter", "#text_filter", "#text_filter",
				"#text_filter", "#text_filter", "#text_filter", "#text_filter",
				"#text_filter" ];
		var enableTooltips = "true,true,true,true,true,true,true,true";
		var setColIds = "Base,Private_Part_#,Part_Desc,Account_Name,Required_Date,Qty,Ref_ID,Ref_Type";
		var setColSorting = "str,str,str,str,date,str,str,str";
		var footerExportFL = true;
		var footerArry = [];
		var footerStyles = [];
		var pagination = "";
		var dataHost = {};
		dataHost.rows = rows;
		document.getElementById("inventoryLookUpRptDiv").style.display = 'block';
		document.getElementById("nothingToMsgDiv").style.display = 'none';
		document.getElementById("excelDiv").style.display = 'block';

		//Loads the Grid

		gridObj = loadDHTMLXGrid('inventoryLookUpRptDiv', gridHeight, dataHost,
				setHeader, setInitWidths, setColAlign, setColTypes,
				setColSorting, setColIds, enableTooltips, setFilter,
				footerArry, footerStyles, footerExportFL, pagination);
	} else {
		document.getElementById("inventoryLookUpRptDiv").style.display = 'none';
		document.getElementById("nothingToMsgDiv").style.display = 'block';
		document.getElementById("excelDiv").style.display = 'none';
	}
}

//When header == "BACKORDER"
function fnPopulateBackOrderDetails() {
	if (objGridData != '') {
		
		var rows = [], i = 0;
		var JSON_Array_obj = "";
		JSON_Array_obj = JSON.parse(objGridData);
		$.each(JSON_Array_obj, function(index, jsonObject) {
		  
			load_id = i;
			
			base = jsonObject.Base_Part;
			private_part = jsonObject.Private_Part;
			part_desc = jsonObject.Part_Description;
			account_name = jsonObject.Account_Name;
			date = jsonObject.Required_Date;
		    qty = jsonObject.QTY;
			ref_id = jsonObject.Ref_Id;
			ref_type = jsonObject.Ref_type;

			// Array
			rows[i] = {};
			rows[i].id = load_id;
			rows[i].data = [];

			rows[i].data[0] = base;
			rows[i].data[1] = private_part;
			rows[i].data[2] = part_desc;
			rows[i].data[3] = account_name;
			rows[i].data[4] = date;
			rows[i].data[5] = qty;
			rows[i].data[6] = ref_id;
			rows[i].data[7] = ref_type;
			i++;
		});
		// Grid dimensions
		
		var gridHeight = "";
		var setInitWidths = "70,70,*,120,100,50,70,150";
		var setColAlign = "right,right,center,center,center,right,center,left";
		var setColTypes = "ro,ro,ro,ro,ro,ro,ro,ro";
		var setHeader = [ "Base", "Private Part #", "Part Desc.",
				"Account Name", "Required Date", "Qty", "Ref ID", "Ref Type" ];
		var setFilter = [ "#text_filter", "#text_filter", "#text_filter",
				"#text_filter", "#text_filter", "#text_filter", "#text_filter",
				"#text_filter" ];
		var enableTooltips = "true,true,true,true,true,true,true,true";
		var setColIds = "base,private_part_#,part_desc,account_name,required_date,qty,Ref_Id,Ref Type";
		var setColSorting = "str,str,str,str,date,str,str,str";
		var footerExportFL = true;
		var footerArry = [];
		var footerStyles = [];
		var pagination = "";
		var dataHost = {};
		dataHost.rows = rows;
		
		document.getElementById("backOrderRptDiv").style.display = 'block';
		document.getElementById("nothingToMsgDiv").style.display = 'none';
		document.getElementById("excelDiv").style.display = 'block';

		// Loads the Grid

		gridObj = loadDHTMLXGrid('backOrderRptDiv', gridHeight, dataHost,
				setHeader, setInitWidths, setColAlign, setColTypes,
				setColSorting, setColIds, enableTooltips, setFilter,
				footerArry, footerStyles, footerExportFL, pagination);
	} else {
		document.getElementById("backOrderRptDiv").style.display = 'none';
		document.getElementById("nothingToMsgDiv").style.display = 'block';
		document.getElementById("excelDiv").style.display = 'none';
	}
}
// Closes the popup window
function fnClose() {
	window.close();
}
// Excel
function fnDownloadXLS() {
	gridObj.toExcel('/phpapp/excel/generate.php');
}