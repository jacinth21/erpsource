var check_rowId = '';
var rowID = '';
var loadnum_rowid = '';
var loadnumstr = '';
var loadidstr = '';
// This function is called while initiating the grid
function initGridData(divRef, gridData) {
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableMultiline(false);
	gObj.init();
	gObj.loadXMLString(gridData);
	return gObj;
}
// Function is used to Check Box Check
function doOnCheck(rowId, cellInd, stage) {
	rowID = rowId;
	return true;
}

// This function is to design and display the grid based on the data getting
// from DB
function fnOnPageLoad() {
	if (objGridData != '') {
		// to form the grid string
		var rows = [], i = 0;
		var JSON_Array_obj = "";
		JSON_Array_obj = JSON.parse(objGridData);
		$.each(JSON_Array_obj, function(index, jsonObject) {
		//	load_id = jsonObject.LOADID;
			load_num = jsonObject.LOADNUM;
			load_num1 = jsonObject.LOADNUM;
			rdate = jsonObject.RDATE;
			type = jsonObject.type;
			num_of_donors = jsonObject.NUMOFDONORS;
			total_part_qty = jsonObject.TOTALPARTQTY;
			// default array
			rows[i] = {};
			rows[i].data = []; 
			//
			rows[i].id = load_num;
			rows[i].data[0] = '';
			rows[i].data[1] = "<a href='#' onClick=fnLoadDtls("+load_num+")>" + load_num + "</a>";
			rows[i].data[2] = rdate;
			rows[i].data[3] = type;
			rows[i].data[4] = "<a href='#'  onClick=fnLoadDonorLoadDtls("+load_num+")>" + num_of_donors + "</a>";
			rows[i].data[5] = total_part_qty;
			rows[i].data[6] = load_num;
			i++;
		});
		var setInitWidths = "60,120,120,90,90,115";
		var setColAlign = "center,right,center,left,right,right,right";
		var setColTypes = "ch,ro,ro,ro,ro,ro,ro";
		var setColSorting = "na,str,date,str,int,str,int";
		var setHeader = [ "Select", " Load#", "Date", " Type",
				"# of Donors", "Total part Qty", "Load#" ];
		var setFilter = [ "#master_checkbox", "#text_filter", "#text_filter",
				"#text_filter", "#text_filter", "#numeric_filter","#numeric_filter" ];
		var setColIds = "check,load_num,rdate,type,num_of_donors,total_part_qty,load_num1";
		var enableTooltips = "true,true,true,true,true,true,true";
		var footerArry = [];
		var gridHeight = "";
		var footerStyles = [];
		var footerExportFL = true;
		var dataHost = {};
		dataHost.rows = rows;
		gridObj = loadDHTMLXGrid('thbdashRpt', gridHeight, dataHost, setHeader,
				setInitWidths, setColAlign, setColTypes, setColSorting,
				setColIds, enableTooltips, setFilter, footerArry, footerStyles,
				footerExportFL);
		gridObj.attachEvent("onCheckbox", doOnCheck);
		check_rowId = gridObj.getColIndexById("check");
		loadnum_rowid = gridObj.getColIndexById("load_num1");
		gridObj.setColumnHidden(6, true);
		document.getElementById("thbdashRpt").style.display = 'block';
		document.getElementById("divNoExcel").style.display = 'block';
		document.getElementById("thblockbtn").style.display = 'block';
		document.getElementById("DivNoMsg").style.display = 'none';
	} else {
		document.getElementById("divNoExcel").style.display = 'none';
		document.getElementById("thbdashRpt").style.display = 'none';
		document.getElementById("thblockbtn").style.display = 'none';
		document.getElementById("DivNoMsg").style.display = 'block';
	}
	
}
//Used to close the popup window
function fnClose() {
	window.close();
}

// This function is to open the popup of Donor Load Details screen
function fnLoadDtls(loadNum) {
	windowOpener(
			"/gmDonorLoadDtlsRpt.do?method=fetchDonorLoadDtsl&strFromLink=DonorLoadDetails&loadNum="
					+ loadNum, "View Load Details",
	"resizable=yes,scrollbars=yes,top=350,left=840,width=770,height=610");
}

//This function is to open the popup of Donor Details Report screen
function fnLoadDonorLoadDtls(loadNum) {
 	windowOpener(
 			"/gmDonorDetailsRpt.do?method=fetchDonorDtlsReport&strFromLink=DonorDetails&loadNum="+loadNum,
 			"View Donor Details",
 	"resizable=yes,scrollbars=yes,top=350,left=840,width=1030,height=610");
 }

// Function to download the excel report
function fnDownloadXLS() {
	gridObj.setColumnHidden(0,true);
	gridObj.toExcel('/phpapp/excel/generate.php');
	gridObj.setColumnHidden(0,false);
}

// this function is used to generate Load number String 
//and open the Lock and generate popup
function fnLockGenerateData() {
	var inputStr = '';
	var checkVal = '';
	var loadnum = '';
	var inputString = '';
	gridObj.forEachRow(function(rowId) {
		checkVal = gridObj.cellById(rowId, check_rowId).getValue();
		loadnum = gridObj.cellById(rowId, loadnum_rowid).getValue();
		if (checkVal == 1) {
			inputStr += loadnum+'|' ;			
			if(inputString == '') // to append the loadnum with comma inorder to show in the Screen
				{
				inputString = loadnum;
				}
			else
				{
				inputString += ' ,'+loadnum ;
				}
		}
	});
	if (inputStr == '') {
		Error_Details(message_operations[786]);
		if (ErrorCount > 0) {
			Error_Show();
			Error_Clear();
			return false;
		}
	} else {
		//PC-5928 Encode URL issue for parameters which are passed in the request without encoding.
		inputStr=encodeURIComponent(inputStr);
		windowOpener(
				"/gmTHBLoadPendDash.do?method=loadLockAndGenerate&strLoadNumStr="
						+ inputStr+"&hLoadNumStr="+inputString, "Lock and Generate",
		    "resizable=yes,scrollbars=yes,top=350,left=840,width=600,height=300");
	}
}