var itemDetailId='';
var loadNum = "";
var graftId = "";
var processedDt = "";
var poNum = "";
var donorNum = "";
var purpose = "";
var isQualify = "";
var productCode = "";
var measurement = "";
var redesignated = "";
var moveTo = "";
var basePart="";
var gridObj="";
var itemId = '';
var locid,loctype ,optionstr ,redesinateId,combobox;
var moveToInventory = "";
//this function for hide div in On page load
function fnOnLoad() {
	if(objGridData == ''){
		document.getElementById("dataGridDiv").style.display = 'none';
		document.getElementById("DivExportExcel").style.display = 'none';
		document.getElementById("btnRow").style.display = 'none';
		
	}
	
	var stropt = document.frmDonorDtlsRpt.strOpt.value;
	if ((objGridData !='') ||(stropt == "reload")) {
		if(header == "radRunQty"){
			fnRadRunQtyDtls();
		}else{
			fnDonorDtlsAjax();
		}
	}

}
//Used to close the popup window
function fnClose() {
	window.close();
}

// to fetch donor details 
function fnFetchDonorLoadDtls(obj) {
	var frm = document.frmDonorDtlsRpt;
	var loadNum = frm.loadNum.value;
	var donorNum = frm.donorNum.value;
	var processedDtObj = frm.processedDate;
	var processedDt = processedDtObj.value;
	var basePartNum = frm.basePart.value;
	if ((loadNum == '' && donorNum == '' && basePartNum == '' && processedDt == '')) {
		Error_Details(message_operations[791]);
	}
	if(processedDtObj.value != ""){
		CommonDateValidation(processedDtObj,format,Error_Details_Trans(message[10002],format));
	}
	if(isNaN(loadNum))
	{
	Error_Details("Please enter valid THB LoadNum");
	}
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	fnStartProgress();
	frm.action = '/gmDonorDetailsRpt.do?method=fetchDonorDtlsReport&strOpt=reload';
	frm.submit();
	/*var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmDonorDetailsRpt.do?method=fetchDonorDtlsReport&hLoadNumStr='
			+ loadNum
			+ '&hDonorNumStr='
			+ donorNum
			+ '&processedDate='
			+ processedDt
			+ '&hBasePartStr='
			+ basePartNum + '&ramdomId=' + new Date().getTime());
	dhtmlxAjax.get(ajaxUrl, fnDonorDtlsAjax)*/
}
//to generate Grid  for Donor Details screen
function fnDonorDtlsAjax(loader) {

	//var response = loader.xmlDoc.responseText;
	fnStopProgress();

    if (objGridData != '') {
		var rows = [], i = 0;
        var JSON_Array_obj = JSON.parse(objGridData);
		$.each(JSON_Array_obj, function(index, jsonObject) {
			loadNum = jsonObject.Load_Num;
			graftId = jsonObject.Allograft_Num;
			processedDt = jsonObject.Processed_Date;
			poNum = jsonObject.PO_Num;
			donorNum = jsonObject.Donor_Num;
			purpose = jsonObject.Donor_Purpose;
			isQualify = jsonObject.Donor_International_Flag;
			productCode = jsonObject.Part_Num;
			measurement = jsonObject.Measurement;
			redesignated = jsonObject.Re_Designated_As;
			moveToInventory=jsonObject.Move_To_Inventory;

			// default array
			rows[i] = {};
			rows[i].id = i;
			rows[i].data = [];
			//set the value to the columns 
			rows[i].data[0] = loadNum;
			rows[i].data[1] = graftId;
			rows[i].data[2] = processedDt;
			rows[i].data[3] = poNum;
			rows[i].data[4] = donorNum;
			rows[i].data[5] = purpose;
			rows[i].data[6] = isQualify;
			rows[i].data[7] = productCode;
			rows[i].data[8] = measurement;
			rows[i].data[9] = redesignated;
			rows[i].data[10] = moveToInventory;

			i++;
		});
		var setInitWidths = "65,85,85,75,100,90,85,*,120,80";
		var setColAlign = "right,right,center,right,right,Left,Left,right,right,right,left";
		var setColTypes = "ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro";
		var setColSorting = ",str,str,str,str,str,str,str,str,str,str,str";
		var setHeader = [ "Load#", "Graft ID", "Processed Date", "PO Number",
				"Donor #", "Purpose", "Is Qualified International",
				"Product Code", "Measurements", "Redesignated As", "Move To Inventory" ];
		var setFilter = [ "#text_filter", "#text_filter", "#text_filter",
				"#text_filter", "#text_filter", "#text_filter", "#text_filter",
				"#text_filter", "#text_filter", "#text_filter","#text_filter"];
		var setColIds = "";
		var enableTooltips = "true,true,true,true,true,true,true,true,true,true,true";
		var footerArry = [];
		var gridHeight = "";
		var footerStyles = [];
		var footerExportFL = true;
		var dataHost = {};
		var pagination = "";
		dataHost.rows = rows;

		document.getElementById("dataGridDiv").style.display = 'block';
		document.getElementById("DivNothingMessage").style.display = 'none';

		gridObj = loadDHTMLXGrid('dataGridDiv', gridHeight, dataHost,
				setHeader, setInitWidths, setColAlign, setColTypes,
				setColSorting, setColIds, enableTooltips, setFilter,
				footerArry, footerStyles, footerExportFL, pagination);
		gridObj.enableBlockSelection(true);
		gridObj.forceLabelSelection(true);
		gridObj.attachEvent("onKeyPress", keyPressedEvnt);
		gridObj.enableMultiselect(true);
		document.getElementById("DivExportExcel").style.display = 'block';

	} else {
		document.getElementById("dataGridDiv").style.display = 'none';
		document.getElementById("DivExportExcel").style.display = 'none';
		document.getElementById("DivNothingMessage").style.display = 'block';

	}
}
// to download excel 
function fnDownloadXLS() {
	gridObj.toExcel('/phpapp/excel/generate.php');
}

// To generate grid for THB Donor Allocation screen
function fnRadRunQtyDtls(){
	fnStopProgress();
    if (objGridData != '') {
		var rows = [], i = 0;
        var JSON_Array_obj = JSON.parse(objGridData);
		$.each(JSON_Array_obj, function(index, jsonObject) {
			loadNum = jsonObject.LOAD_NUM;
			graftId = jsonObject.ALLOGARFT_NUM;
			processedDt = jsonObject.PROCESSED_DATE;
			donorNum = jsonObject.DONOR_NUM;
			purpose = jsonObject.DONOR_PURPOSE;
			isQualify = jsonObject.DONOR_INTERNATIONAL_FLAG;
			measurement = jsonObject.MEASUREMENT;
			if(jsonObject.REDESIGNATEDAS == null || jsonObject.REDESIGNATEDAS ==''){
				redesignated = '';
			}else{
				redesignated = jsonObject.REDESIGNATEDAS;
			}
			if(jsonObject.MOVETO == null || jsonObject.MOVETO ==''){
				moveTo = '';
			}else{
				moveTo=jsonObject.MOVETO;
			}
            itemDetailId = jsonObject.ITEM_DETAIL_ID;
            basePart = jsonObject.BASEPART;
            
			// default array
			
			//set the value to the columns
				rows[i] = {};
				rows[i].id = itemDetailId;
				rows[i].data = [];
				rows[i].data[0] = loadNum;
				rows[i].data[1] = graftId;
				rows[i].data[2] = processedDt;
				rows[i].data[3] = donorNum;
				rows[i].data[4] = purpose;
				rows[i].data[5] = isQualify;
				rows[i].data[6] = basePart;
				rows[i].data[7] = measurement;
				rows[i].data[8] = redesignated;
				rows[i].data[9] = moveTo;
			
			     itemId = itemId+itemDetailId +',';
			i++;
		});
		
		var setInitWidths = "65,85,85,75,100,90,85,*,150,140";
		var setColAlign = "right,left,center,right,center,center,left,right,center,center";
		var setColTypes = "ro,ro,ro,ro,ro,ro,ro,ro,coro,coro";
		var setColSorting = ",str,str,str,str,str,str,str,str,str,str";
		var setHeader = [ "Load#", "Graft ID", "Processed Date",
			  				"Donor #", "Purpose", "Is Qualified International",
			  				"Base Part#", "Measurements", "Redesignate As","Move To" ];
		
		var setFilter = [ "#text_filter", "#text_filter", "#text_filter",
				"#text_filter", "#text_filter", "#text_filter", "#text_filter",
				"#text_filter", "#custom_drpdown1","#custom_drpdown2"];
	
		var setColIds = "";
		var enableTooltips = "true,true,true,true,true,true,true,true,true";
		var footerArry = [];
		var gridHeight = "";
		var footerStyles = [];
		var footerExportFL = true;
		var dataHost = {};
		var pagination = "";
		dataHost.rows = rows;

		document.getElementById("dataGridDiv").style.display = 'block';
		document.getElementById("DivNothingMessage").style.display = 'none';

		gridObj = initGridData('dataGridDiv', gridHeight, dataHost,
				setHeader, setInitWidths, setColAlign, setColTypes,
				setColSorting, setColIds, enableTooltips, setFilter,
				footerArry, footerStyles, footerExportFL, pagination);
		gridObj.attachEvent("onEditCell",doOnCellEdit);
		gridObj.enableBlockSelection(true);
		gridObj.forceLabelSelection(true);  
		gridObj.attachEvent("onKeyPress", keyPressedEvnt);
		gridObj.enableMultiselect(true);
		document.getElementById("DivExportExcel").style.display = 'block';

	} else {
		document.getElementById("dataGridDiv").style.display = 'none';
		document.getElementById("DivExportExcel").style.display = 'none';
		document.getElementById("btnRow").style.display = 'none';

	}
}
//This function is called while initiating the grid with customized dropdown
function initGridData(divRef, gridHeight, gridData, setHeader, setInitWidths, setColAlign, setColTypes, setColSorting, setColIds, enableTooltips, setFilter, footerArry, footerStyles, footerExportFL,pagination) {

	var gObj = new dhtmlXGridObject(divRef);
    gObj.enableColSpan(true);
    gObj.setSkin("dhx_skyblue");
    gObj.setImagePath("/extweb/dhtmlx/dhtmlxGrid/imgs/");
    if (setHeader != undefined && setHeader.length > 0) {
        gObj.setHeader(setHeader);
        var colCount = setHeader.length;
    }
 // this function is to add the dropdown in grid header
    gObj._in_header_custom_drpdown1=function(tag,index,data){   
		optionstr = '<option value="0">[Choose one]</option>';
		for(i=0;i<redesignateLen;i++){
			
			arr=eval("RedesinateArr"+i);
			arrobj = arr.split(",");
			redesinateId = arrobj[0];
			optionstr = optionstr + "<option value="+redesinateId+">"+redesinateId+"</option>";
			
			//set drop down values for grid body
			 combobox = gObj.getCombo(8);
			 combobox.put("0","[Choose one]");
			 combobox.put(redesinateId,redesinateId);
		}
         
		tag.innerHTML='<select name=ApprRej class=RightText tabindex="-1" onChange="fnSavPartInvMap(this);">' + optionstr +'</select>';
     }
    gObj._in_header_custom_drpdown2=function(tag,index,data){ // this function is to add the dropdown in grid header
    	
		optionstr = '<option value="0">[Choose one]</option>';
		for(i=0;i<locationLen;i++){
			
			arr=eval("TypeArr"+i);
			arrobj = arr.split(",");
			locid = arrobj[0];
			loctype =  arrobj[1];
			optionstr = optionstr + "<option value="+locid+">"+loctype+"</option>";
			 //set drop down values for grid body
			 combobox = gObj.getCombo(9); 
			 combobox.put("0","[Choose one]");
			 combobox.put(locid,loctype);
		}
		 
		tag.innerHTML='<select name=ApprRej id =ApprRej class=RightText tabindex="-1" onChange="fnSavePartInventory(this);">' + optionstr +'</select>';
     }
   
    if (setInitWidths != "")
        gObj.setInitWidths(setInitWidths);
    if (setColAlign != "")
        gObj.setColAlign(setColAlign);
    if (setColTypes != "")
        gObj.setColTypes(setColTypes);
    if (setColSorting != "")
        gObj.setColSorting(setColSorting);
    if (enableTooltips != "")
        gObj.enableTooltips(enableTooltips);
    if (setFilter != "")
        gObj.attachHeader(setFilter);
    if (footerArry != undefined && footerArry.length > 0) {
        var footstr = eval(footerArry).join(",");
        if (footerStyles != undefined && footerStyles.length > 0)
            gObj.attachFooter(footstr, footerStyles);
        else
            gObj.attachFooter(footstr);
    }
    
    if(setColIds != ""){
    	gObj.setColumnIds(setColIds);
    }
    /*if (footerExportFL)
        fnAttachExptFooter(divRef, colCount, gObj);*/
    
    if (gridHeight != "")
        gObj.enableAutoHeight(true, gridHeight, true);
    else
        gObj.enableAutoHeight(true, 400, true);

    gObj.init();
    if(pagination == "Y"){
	    gObj.enablePaging(true,100,10,"pagingArea",true);
	    gObj.setPagingSkin("bricks");
    }
    
    gObj.parse(gridData, "json");
    return gObj;
}

//this function used for  save the redesignated part
function fnSavPartInvMap(obj){
   var val = obj.value;
   var optVal= obj.options[obj.selectedIndex].text
  gridObj.forEachRow(function(rowId){
    ApprRej=gridObj.cellById(rowId, 8).setValue(optVal);
 });
var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmDonorPartInvMap.do?method=saveInvAllocationForPart&itemDetailsId='
		+ itemId
		+'&redesinateAs='
		+val
		+'&dropDownType=RedesignateType'
		+ '&ramdomId='
		+ new Date().getTime());
dhtmlxAjax.get(ajaxUrl, function(loader){});
}
//this function used for save the Inventory location 
function fnSavePartInventory(obj){
	var val = obj.value;
	var optVal= obj.options[obj.selectedIndex].text
	gridObj.forEachRow(function(rowId){
     ApprRej=gridObj.cellById(rowId, 9).setValue(optVal);
		
	});
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmDonorPartInvMap.do?method=saveInvAllocationForPart&itemDetailsId='
			+ itemId
			+'&locationType='
			+val
			+'&dropDownType=InvType'
			+ '&ramdomId='
			+ new Date().getTime());
	dhtmlxAjax.get(ajaxUrl, function(loader){});
}

//this function is used to perform action when a cell is editing
function doOnCellEdit(stage, rowId, cellInd, nValue, oValue) {	
	     if(cellInd == '9'){
			if (stage==2){
				var itemDtlsId = rowId+',';
				var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmDonorPartInvMap.do?method=saveInvAllocationForPart&itemDetailsId='
						+ itemDtlsId
						+'&locationType='
						+nValue
						+'&dropDownType=InvType'
						+ '&ramdomId='
						+ new Date().getTime());
				dhtmlxAjax.get(ajaxUrl, function(loader){});
	        }					
	     }
	     if(cellInd == '8'){
	 		if (stage==2){
	 			var itemDtlsId = rowId+',';
	 			var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmDonorPartInvMap.do?method=saveInvAllocationForPart&itemDetailsId='
	 					+ itemDtlsId
	 					+'&redesinateAs='
	 					+nValue
	 					+'&dropDownType=RedesignateType'
	 					+ '&ramdomId='
	 					+ new Date().getTime());
	 			dhtmlxAjax.get(ajaxUrl, function(loader){});
	         }					
	      }
	return true;
	}

//This function used for save the allocation qty 
function fnSchduling(){
	var moveTo='';
	var basePart='';
	var donorNum='';
	var loadnum='';
	var garftId='';
	var privatePart = '';
	var strGarftId='';
	fnStartProgress('Y');
	gridObj.forEachRow(function(rowId){
	     moveTo =gridObj.cellById(rowId, 9).getValue();
	     basePart = gridObj.cellById(rowId, 6).getValue();
	      donorNum = gridObj.cellById(rowId, 3).getValue();
	      loadnum = gridObj.cellById(rowId, 0).getValue();
	      garftId = gridObj.cellById(rowId, 1).getValue();
	      privatePart =  document.frmDonorDtlsRpt.privatePart.value;
	      if((moveTo =='0' || moveTo =='')||(moveTo =='0'&& moveTo =='')){
	    	  garftId = gridObj.cellById(rowId, 1).getValue();
	    	  if(strGarftId !=''){
	 	    	  strGarftId=strGarftId+','+garftId;
	 	      }else{
	 	    	 strGarftId =garftId;
	 	      }
	    	  
	      }
	 });
	 if((strGarftId != '')){
	 		Error_Details(message_operations[798]+strGarftId);
	 }
	if (ErrorCount > 0) {
		fnStopProgress('Y');
		Error_Show();
		Error_Clear();
		return false;
	}
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmDonorPartInvMap.do?method=saveRadRunAllocation&donorNum='
				+ donorNum
				+'&basePart='
				+basePart
				+'&analysisId='
				+strAnalysisId
				+'&loadNum='
				+loadnum
				+'&privatePart='
				+privatePart
				+ '&ramdomId='
				+ new Date().getTime());
	
		dhtmlxAjax.get(ajaxUrl, fnCallBack);
	
	
}


//This function is called front end show Error Message for Stage date is not within the Analysis processing dates
function fnCallBack(loader){
	var status = loader.xmlDoc.status;
	var errorDet = loader.xmlDoc.responseText;
	var errorPart = errorDet.split('^');
	
	if(status == 200){
		
		document.getElementById("errMsg").innerHTML = "<font color=red><b>"+message_operations[863]+basePart+"</b></font>";

	}		
	fnStopProgress('Y');

	
}
//this function used to copy the selected value
function keyPressedEvnt(code, ctrl) {
	if (code == 67 && ctrl) {
		docopy();
	}
	if(code==86&&ctrl){
		pasteToGrid();
	}
	gridObj.refreshFilters();
	return true;
}
//this function used to copy
function docopy() {
	gridObj.setCSVDelimiter("\t");
	gridObj.copyBlockToClipboard();
	var startPoint = 0;
	arrValuefromClipboard = new Array();
	if (gridObj._selectionArea != null) {
		var area = gridObj._selectionArea;
		var leftTopCol = area.LeftTopCol;
		var leftTopRow = area.LeftTopRow;
		var rightBottomCol = area.RightBottomCol;
		var rightBottomRow = area.RightBottomRow;
		for ( var i = leftTopRow; i <= rightBottomRow; i++) {
			for ( var j = leftTopCol; j <= rightBottomCol; j++) {
				setValuefromClipboard = gridObj.cellByIndex(i, j).getValue();
				var e = gridObj.cellByIndex(i, j);
				if (e.combo) {
					for ( var g = e.combo.values, y = 0; y < g.length; y++){
						if (setValuefromClipboard == e.combo.keys[y]) {
							setValuefromClipboard = e.combo.values[y];
							g = null;
							break;
						}
					}
				}
				arrValuefromClipboard.push(setValuefromClipboard);
				startPoint++;
				
			}
		}
	}
	gridObj._HideSelection();
}
//this function used to copy
function pasteToGrid() {
		if (gridObj._selectionArea != null) {
			var area = gridObj._selectionArea;
			var leftTopCol = area.LeftTopCol;
			var leftTopRow = area.LeftTopRow;
			var rightBottomCol = area.RightBottomCol;
			var rightBottomRow = area.RightBottomRow;
			for ( var i = leftTopRow; i <= rightBottomRow; i++) {
				var m = 0;
				for ( var j = leftTopCol; j <= rightBottomCol; j++) {
					var newRowId = gridObj.getRowId(i);
					var disbleFl = false;//gridObj.cellByIndex(i, j).isDisabled();
					var comboValFl = false;
					if(!disbleFl){
						var e = gridObj.cellByIndex(i, j);
						if (e.combo) {
							for ( var g = e.combo.values, y = 0; y < g.length; y++){
								if (arrValuefromClipboard[m] == e.combo.values[y]) {
									e.setValue(e.combo.keys[y]);
									comboValFl = true;
									g = null;
									break;
								}
							}
							if (!comboValFl){
								e.setValue('');
							}
						}else{
							gridObj.cellByIndex(i, j).setValue(arrValuefromClipboard[m]);
						}
						gridObj.setCellTextStyle(newRowId, j,"color:black;font-weight:normal;");
						gridObj.setRowColor(newRowId,"#9FF781");
					}
					m++;
				}
				fnSaveRedesignateType(newRowId,j-1,e.combo.values[y]);
				fnSaveInvAllocType(newRowId,j-1,e.combo.keys[y]);
			 }
			gridObj._HideSelection();
			gridObj.refreshFilters();
		} else {
			alert(message_prodmgmnt[34]);
		}
	}
function fnSaveRedesignateType(rowId, cellInd, nValue)
	{
	if(cellInd == '8'){
	var itemDtlsId = rowId+',';
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmDonorPartInvMap.do?method=saveInvAllocationForPart&itemDetailsId='
		 					+ itemDtlsId
		 					+'&redesinateAs='
		 					+nValue
		 					+'&dropDownType=RedesignateType'
		 					+ '&ramdomId='
		 					+ new Date().getTime());
		 					dhtmlxAjax.get(ajaxUrl, function(loader){});
	}
}
function fnSaveInvAllocType(rowId, cellInd, nValue)
	{
	if(cellInd == '9'){
	var itemDtlsId = rowId+',';
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmDonorPartInvMap.do?method=saveInvAllocationForPart&itemDetailsId='
						+ itemDtlsId
						+'&locationType='
						+nValue
						+'&dropDownType=InvType'
						+ '&ramdomId='
						+ new Date().getTime());
						dhtmlxAjax.get(ajaxUrl, function(loader){});
	}
}	