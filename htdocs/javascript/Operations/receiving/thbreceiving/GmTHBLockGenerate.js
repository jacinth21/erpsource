// this function is called when cancel button is pressed
function fnCancel() {
	document.frmTHBLoadPendDash.strLoadNumStr.value = '';
	window.close();
}

// this function is used to validate the input and
// lock and generate for the date input
function fnSubmit() {
	var fromDateObj = document.frmTHBLoadPendDash.fromDate;
	var fromDt = fromDateObj.value;
	var toDtObj = document.frmTHBLoadPendDash.toDate;
	var toDt = toDtObj.value;
	var screenType = document.frmTHBLoadPendDash.strScreen.value;

	CommonDateValidation(fromDateObj, format,
			message_operations[784] + format);
	CommonDateValidation(toDtObj, format,
			message_operations[785] + format);

	if (fromDt == '') 
	{
		Error_Details(message_operations[793]);
	}
	if (toDt == '') 
	{
		Error_Details(message_operations[794]);
	}

	if (dateDiff(fromDt, toDt, format) < 0) 
	{
		Error_Details(message_operations[787]);
	}

	if(dateDiff(currDate,fromDt, format) < 0){
		Error_Details(message_operations[792]);
	}

	if (ErrorCount > 0) 
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	fnStartProgress();
	document.frmTHBLoadPendDash.action = "/gmTHBLoadPendDash.do?method=saveLockAndGenerate&companyInfo="+companyInfoObj;
	document.frmTHBLoadPendDash.submit();
	if(screenType != 'List_rpt') // not to refresh if its called from List report 
	{
		fnRefreshParent();
	}

}
// this function is used to refresh the Parent dashboard screen
function fnRefreshParent() {
	if (window.opener != null && !window.opener.closed) {
		window.opener.location.href = window.opener.location.href
				+ "&randomId=" + Math.random();
	}
}

function onload() {
	var loadStatus = document.frmTHBLoadPendDash.strLoadStatus.value;
	if (loadStatus == "108220") {
		document.getElementById("DivLockMsg").style.display = 'block';
	} else {
		document.getElementById("DivLockMsg").style.display = 'none';
	}

}
