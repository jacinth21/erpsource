var loadNum = "";
var donorNum = "";
//this function for hide div 
function fnOnLoad() {
	var linkObj = document.frmDonorLoadDtlsRpt.hFromLink;
	var link = (linkObj) ? linkObj.value: '';
	document.getElementById("dataGridDiv").style.display = 'none';
	document.getElementById("DivExportExcel").style.display = 'none';
	document.getElementById("DivNothingMessage").style.display = 'none';
	document.frmDonorLoadDtlsRpt.loadNum.focus();
	var stropt = document.frmDonorLoadDtlsRpt.strOpt.value;
	if((objGridData !='') && (link == "donorSchedule")) 
	{
		fnFetchDonorScheduleDtls();
	}else if((objGridData !='') && (link == "donorReceivingPro")) 
	{
		fnFetchDonorReceivingDtls();
	}
	else if((objGridData !='') ||(stropt == "reload")) {
			fnLoadDonorDetailsAjax();
		}
}

// to fetch donor details validation
function fnFetchDonorLoadDtls(obj) {
	var frm = document.frmDonorLoadDtlsRpt;
	loadNum = frm.loadNum.value;
	donorNum = frm.donorNum.value;
	var linkObj = document.frmDonorLoadDtlsRpt.hFromLink;
	var link = (linkObj) ? linkObj.value: '';
	if(link == 'donorSchedule' || link == "donorReceivingPro"){
		var stgToDt = document.frmDonorLoadDtlsRpt.stageDateTo.value;
		var stgFrmDt = document.frmDonorLoadDtlsRpt.stageDateFrom.value;
		
		if(loadNum =='' && donorNum =='' && stgToDt =='' && stgFrmDt =='')
		{
			Error_Details(message_operations[799]);	
		}
		if (stgToDt != '' && stgFrmDt == '') 
		{	
				Error_Details(message_operations[800]);
		}
		if (dateDiff(stgFrmDt, stgToDt, format) < 0) 
		{
			Error_Details(message_operations[801]);
		}
	}
	else {
	if(loadNum == '' && donorNum == '') {
			Error_Details(message_operations[790]);
		}
	}
	if(isNaN(loadNum))
	{
	Error_Details("Please enter valid THB LoadNum");
	}
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	fnStartProgress();
	document.frmDonorLoadDtlsRpt.action = '/gmDonorLoadDtlsRpt.do?method=fetchDonorLoadDtsl&strOpt=reload';
	document.frmDonorLoadDtlsRpt.submit();
	/*var ajaxUrl = fnAjaxURLAppendCmpInfo('gmDonorLoadDtlsRpt.do?method=fetchDonorLoadDtsl&hLoadNumStr='
			+ loadNum
			+ '&hDonorNumStr='
			+ donorNum
			+ '&ramdomId='
			+ new Date().getTime());
	dhtmlxAjax.get(ajaxUrl, fnLoadDonorDetailsAjax)*/
}

//fnFetchDonorReceivingDtls process to generate Grid 
function fnFetchDonorReceivingDtls() {
	var age = "";
	var sex = "";
	var units = "";
	var qtyAllocated = "";
	var qtyNotAllocated = "";
	var stageDate = "";

	fnStopProgress();
	if (objGridData != '') {
		var rows = [], i = 0;
		var JSON_Array_obj = JSON.parse(objGridData);
		$.each(JSON_Array_obj, function(index, jsonObject) {
			loadNum = jsonObject.Load_Num;
			donorNum = jsonObject.Donor_Num;
			age = jsonObject.Age;
			sex = jsonObject.Sex;
			units = jsonObject.No_of_Units;
			qtyAllocated = jsonObject.Qty_Allocated;
			qtyNotAllocated = jsonObject.Qty_Not_Allocated;
			stageDate = jsonObject.Stage_Date;
			// default array
			rows[i] = {};
			rows[i].id = i;
			rows[i].data = [];
			// set the value to the columns
			
			rows[i].data[0] = loadNum;
			rows[i].data[1] = donorNum;
			rows[i].data[2] = age;
			rows[i].data[3] = sex;
			rows[i].data[4] = "<a href='#' onClick=fnLoadDonorLoadDtls("+loadNum+")>" + units + "</a>";
			rows[i].data[5] = "<a href='#' onClick=fnLoadDonorLoadDtls("+loadNum+")>" + qtyAllocated + "</a>";
			rows[i].data[6] = "<a href='#' onClick=fnLoadDonorLoadDtls("+loadNum+")>" + qtyNotAllocated + "</a>";
			rows[i].data[7] = stageDate;
			i++;
		});
		var setInitWidths = "50,100,100,100,100,100,100,100,100,100,100,*";
		var setColAlign = "right,right,right,left,right,right,right,center,right,right,right";
		var setColTypes = "ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro";
		var setColSorting = ",str,str,str,str,str,str,str,str,str,str,str";
		var setHeader = [ "Load#", "Donor#", "Age", "Sex", "# Of Units",
				"Qty Allocated", "Qty Not Allocated", "Stage Date" , "Create RS" , "Pre_Staged" , "Staged"];
		var setFilter = [ "#numeric_filter", "#numeric_filter",
				"#numeric_filter", "#text_filter", "#numeric_filter",
				"#numeric_filter", "#numeric_filter", "#text_filter"];
		var setColIds = "";
		var enableTooltips = "true,true,true,true,true,true,true,true";
		var footerArry = [];
		var gridHeight = "650";
		var footerStyles = [];
		var footerExportFL = true;
		var dataHost = {};
		var pagination = "";
		dataHost.rows = rows;
		document.getElementById("dataGridDiv").style.display = 'block';
		document.getElementById("DivNothingMessage").style.display = 'none';
		gridObj = loadDHTMLXGrid('dataGridDiv', gridHeight, dataHost,
				setHeader, setInitWidths, setColAlign, setColTypes,
				setColSorting, setColIds, enableTooltips, setFilter,
				footerArry, footerStyles, footerExportFL, pagination);
		document.getElementById("DivExportExcel").style.display = 'block';
	} else {
		document.getElementById("dataGridDiv").style.display = 'none';
		document.getElementById("DivExportExcel").style.display = 'none';
		document.getElementById("DivNothingMessage").style.display = 'block';
	}
}

//This function is to open the popup of Donor Details Report screen
function fnLoadDonorLoadDtls(loadNum,donorNum) {
     windowOpener(
        "/gmDonorDetailsRpt.do?method=fetchDonorDtlsReport&loadNum="+loadNum+'&donorNum='+donorNum,
        "View Donor Details",
        "resizable=yes,scrollbars=yes,top=150,left=150,width=800,height=550");
}
// fnFetchDonorScheduleDtls to generate Grid 
function fnFetchDonorScheduleDtls() {
	
	var age = "";
	var sex = "";
	var units = "";
	var qtyAllocated = "";
	var qtyNotAllocated = "";
	var stageDate = "";

	fnStopProgress();
	if (objGridData != '') {
		var rows = [], i = 0;
		var JSON_Array_obj = JSON.parse(objGridData);
		$.each(JSON_Array_obj, function(index, jsonObject) {
			loadNum = jsonObject.Load_Num;
			donorNum = jsonObject.Donor_Num;
			age = jsonObject.Age;
			sex = jsonObject.Sex;
			units = jsonObject.No_of_Units;
			qtyAllocated = jsonObject.Qty_Allocated;
			qtyNotAllocated = jsonObject.Qty_Not_Allocated;
			stageDate = jsonObject.Stage_Date;
			// default array
			rows[i] = {};
			rows[i].id = i;
			rows[i].data = [];
			// set the value to the columns
			
			rows[i].data[0] = loadNum;
			rows[i].data[1] = donorNum;
			rows[i].data[2] = age;
			rows[i].data[3] = sex;
			if(units == null || units =='0'){
				rows[i].data[4] = '0';
			}else{
				rows[i].data[4] = "<a href='#' onClick=fnLoadDonorLoadDtls('"+loadNum+"','"+donorNum+"')>" + units + "</a>";
			}
			if(qtyAllocated == null|| qtyAllocated =='0'){
				rows[i].data[5] = '0';
			}else{
				rows[i].data[5] = "<a href='#' onClick=fnLoadDonorLoadDtls('"+loadNum+"','"+donorNum+"')>" + qtyAllocated + "</a>";
			}
			if(qtyNotAllocated == null|| qtyNotAllocated =='0'){
				rows[i].data[6] = '0';
			}else{
				rows[i].data[6] = "<a href='#' onClick=fnLoadDonorLoadDtls('"+loadNum+"','"+donorNum+"')>" + qtyNotAllocated + "</a>";
			}
			rows[i].data[7] = stageDate;
			i++;
		});
		var setInitWidths = "100,100,100,100,100,100,100,*";
		var setColAlign = "right,right,right,left,right,right,right,center";
		var setColTypes = "ro,ro,ro,ro,ro,ro,ro,ro";
		var setColSorting = "str,str,str,str,str,str,str,date";
		var setHeader = [ "Load #", "Donor #", "Age", "Sex", "Units",
				"Qty Allocated", "Qty Not Allocated", "Stage Date" ];
		var setFilter = [ "#numeric_filter", "#numeric_filter",
				"#numeric_filter", "#text_filter", "#numeric_filter",
				"#numeric_filter", "#numeric_filter", "#text_filter" ];
		var setColIds = "";
		var enableTooltips = "true,true,true,true,true,true,true,true";
		var footerArry = [];
		var gridHeight = "650";
		var footerStyles = [];
		var footerExportFL = true;
		var dataHost = {};
		var pagination = "";
		dataHost.rows = rows;
		document.getElementById("dataGridDiv").style.display = 'block';
		document.getElementById("DivNothingMessage").style.display = 'none';
		gridObj = loadDHTMLXGrid('dataGridDiv', gridHeight, dataHost,
				setHeader, setInitWidths, setColAlign, setColTypes,
				setColSorting, setColIds, enableTooltips, setFilter,
				footerArry, footerStyles, footerExportFL, pagination);
		document.getElementById("DivExportExcel").style.display = 'block';
	} else {
		document.getElementById("dataGridDiv").style.display = 'none';
		document.getElementById("DivExportExcel").style.display = 'none';
		document.getElementById("DivNothingMessage").style.display = 'block';
	}
}
// to generate grid data
function fnLoadDonorDetailsAjax() {

	var age = "";
	var sex = "";
	var units = "";
	var qtyAllocated = "";
	var qtyNotAllocated = "";
	fnStopProgress();
	if (objGridData != '') {
		var rows = [], i = 0;
		var JSON_Array_obj = JSON.parse(objGridData);
		$.each(JSON_Array_obj, function(index, jsonObject) {
			loadNum = jsonObject.Load_Num;
			donorNum = jsonObject.Donor_Num;
			age = jsonObject.Age;
			sex = jsonObject.Sex;
			units = jsonObject.No_of_Units;
			qtyAllocated = jsonObject.Qty_Allocated;
			qtyNotAllocated = jsonObject.Qty_Not_Allocated;
			// default array
			rows[i] = {};
			rows[i].id = i;
			rows[i].data = [];
			// set the value to the columns
			rows[i].data[0] = loadNum;
			rows[i].data[1] = donorNum;
			rows[i].data[2] = age;
			rows[i].data[3] = sex;
			rows[i].data[4] = units;
			rows[i].data[5] = qtyAllocated;
			rows[i].data[6] = qtyNotAllocated;
			i++;
		});
		var setInitWidths = "100,100,100,100,100,100,*";
		var setColAlign = "right,right,right,left,right,right,right";
		var setColTypes = "ro,ro,ro,ro,ro,ro,ro";
		var setColSorting = ",str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str";
		var setHeader = [ "Load#", "Donor#", "Age", "Sex", "# Of Units",
				"Qty Allocated", "Qty Not Allocated" ];
		var setFilter = [ "#numeric_filter", "#numeric_filter",
				"#numeric_filter", "#text_filter", "#numeric_filter",
				"#numeric_filter", "#numeric_filter" ];
		var setColIds = "";
		var enableTooltips = "true,true,true,true,true,true,true";
		var footerArry = [];
		var gridHeight = "";
		var footerStyles = [];
		var footerExportFL = true;
		var dataHost = {};
		var pagination = "";
		dataHost.rows = rows;

		document.getElementById("dataGridDiv").style.display = 'block';
		document.getElementById("DivNothingMessage").style.display = 'none';

		gridObj = loadDHTMLXGrid('dataGridDiv', gridHeight, dataHost,
				setHeader, setInitWidths, setColAlign, setColTypes,
				setColSorting, setColIds, enableTooltips, setFilter,
				footerArry, footerStyles, footerExportFL, pagination);
		document.getElementById("DivExportExcel").style.display = 'block';

	} else {
		document.getElementById("dataGridDiv").style.display = 'none';
		document.getElementById("DivExportExcel").style.display = 'none';
		document.getElementById("DivNothingMessage").style.display = 'block';

	}
}
 
//Used to close the popup window
function fnClose() {
	window.close();
}

// to download excel
function fnDownloadXLS() {
	gridObj.toExcel('/phpapp/excel/generate.php');
}

