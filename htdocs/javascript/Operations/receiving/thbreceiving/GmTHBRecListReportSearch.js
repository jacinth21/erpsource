var Chkbox_rowId ='';
var loadId_rowId='';
var loadNum_rowId='';
var boxNum_rowId='';
var statusId_rowId='';
function fnOnPageLoad() {
	if (objGridData.value != '') {
		gObj = initGridData('dataGridDiv', objGridData);
		gObj.attachHeader(",#text_filter,#text_filter,#text_filter,#text_filter,#select_filter");
		// adding check box in last column in report
		Chkbox_rowId = gObj.getColIndexById("chk_box");
		loadId_rowId=  gObj.getColIndexById("loadid");
		loadNum_rowId = gObj.getColIndexById("loadNum");
	    boxNum_rowId = gObj.getColIndexById("boxNum");
        statusId_rowId = gObj.getColIndexById("statusId");
	    gObj.attachEvent("onCheck", doOnChecks);

      gObj.forEachRow(function(rowId) {
	     var statusId = gObj.cellById(rowId, statusId_rowId).getValue();
        if(statusId !='107881'){//if THD load staus is initiated , disable the check box
	         gObj.cellById(rowId, Chkbox_rowId).setDisabled(true);
	
        } 
	  });
	}

}

// This function is called while initiating the grid
function initGridData(divRef, gridData) {
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableMultiline(false);
	gObj.enableDistributedParsing(true);
	gObj.init();
	gObj.loadXMLString(gridData);
	return gObj;
}

// This function is called when the load button is clicked
function fnLoad() {

	var objFromDt = '';
	var objToDt = '';
	var loadNum = '';
	var boxNum = '';
	var loadStatus = '';

	objFromDt = document.frmTHBRecListRpt.strFromDate;
	objToDt = document.frmTHBRecListRpt.strToDate;
	loadNum = document.frmTHBRecListRpt.strThbLoadNum.value;
	boxNum = document.frmTHBRecListRpt.strBoxNum.value;
	loadStatus = document.frmTHBRecListRpt.strLoadStatus.value;

	fromDt = objFromDt.value;
	toDate = objToDt.value;

	if ((loadNum == '') && (boxNum != '')) {
		Error_Details(message_operations[783]);
	}

	if ((fromDt == "") && (toDate != "")) {
		fnValidateTxtFld('strFromDate', message[10527]);
	} else if ((fromDt != "") && (toDate == "")) {
		fnValidateTxtFld('strToDate', message[10528]);
	}

	CommonDateValidation(objFromDt, format, message[10507], format);
	CommonDateValidation(objToDt, format, message[10508], format);
	if (dateDiff(fromDt, toDate, format) < 0) {
		Error_Details(message[10579]);
	}
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	document.frmTHBRecListRpt.strOpt.value = "Load";
	document.frmTHBRecListRpt.action = "/gmTHBRecListRpt.do?method=fetchTHBRecListRpt";
	fnStartProgress();
	document.frmTHBRecListRpt.submit();

}

// This function used to download the grid data
function fnDownloadXLS() {
	gObj.toExcel('/phpapp/excel/generate.php');
}

// This function used to press the enter key then call the Load function.
function fnEnter() {
	if (event.keyCode == 13) {
		fnLoad();
	}
}
// This function used to fetch the hyperlink details of loadnum
function fnCallLoadNumRpt(loadNum) {
	windowOpener(
			"/gmTHBRecListRpt.do?method=fetchTHBListReportDetail&strThbLoadNum="
					+ loadNum, "Pack",
			"resizable=yes,scrollbars=yes,top=150,left=150,width=800,height=550");
}
// This function used to fetch the hyperlink details of Boxnum
function fnCallBoxNumRpt(loadId) {
	windowOpener(
			"/gmTHBRecListRpt.do?method=fetchTHBListReportDetail&strLoadId="
					+ loadId, "Pack",
			"resizable=yes,scrollbars=yes,top=150,left=150,width=800,height=550");
}
//This function fired  whecn click the check box and update the status
function doOnChecks(rowId, cellInd, state) {
	var checkedRowID = '';
	var check_id = '';
	var loadId = '';
	var loadNum = '';
	var boxNum = '';

	check_id = gObj.cellById(rowId, Chkbox_rowId).getValue();
	loadId = gObj.cellById(rowId, loadId_rowId).getValue();
	loadNum = gObj.cellById(rowId, loadNum_rowId).getValue();
	boxNum = gObj.cellById(rowId, boxNum_rowId).getValue();

	if (check_id == 1) {
		if (confirm("Are you sure want to Re-scan ?")) {
	       var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmTHBReceivingLoadTran.do?method=rollbackTHBBox&strLoadId='
		              + encodeURIComponent(loadId) + '&ramdomId=' + Math.random() + "&" + fnAppendCompanyInfo());
		dhtmlxAjax.get(ajaxUrl, function (loader) {});
		fnLoad();
		document.getElementById("SuccessMsg").style.display = "inline";
	    docment.getElementById("SuccessMsg").style.color = "green";
		document.getElementById("SuccessMsg").innerHTML = "Successfully rollback THB Load Box " + loadNum + " and " + boxNum;
		
	}else{
		return false;
	}
		
	}

}