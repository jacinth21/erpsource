// function to load on page Load
function fnDoOnLoad(LoadNumParam)
{	
	document.getElementById("thbDash_").style.width= "250px";
	dhxLayout_ = new dhtmlXLayoutObject("thbDash_", "1C");
	fnLoadLayout(dhxLayout_,"a",objCell,LoadNumParam);
}
// function to load the grid in Layout
function fnLoadLayout(objLayout,objLayoutCell, objCell,LoadNumParam)
{
	objLayout.cells(objLayoutCell).progressOn();
	objLayout.cells(objLayoutCell).hideHeader();
	dhxGrid = objLayout.cells(objLayoutCell).attachGrid();
	dhxGrid.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	fnAjaxGridData(dhxGrid, objCell,objLayout,objLayoutCell,LoadNumParam);

}
// function for Ajax call
function fnAjaxGridData(ojbMygrid, objCell, objLayout,objLayoutCell,LoadNumParam)
{
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmTHBDonorScheduling.do?&method=fchDonorStageDate&strLoadNum='+LoadNumParam+'&ramdomId='+Math.random());
		var loader = dhtmlxAjax.getSync(ajaxUrl);
		fnLoadCallBack(loader,ojbMygrid,objLayout,objLayoutCell,objCell);
}
// loader function to fetch the response
function fnLoadCallBack(loader,ojbMygrid,objLayout,objLayoutCell,objCell)
{
	var response = loader.xmlDoc.responseText;
	if (response != null)
		{
		var datagridXML = loader.xmlDoc.responseText;
		ojbMygrid.loadXMLString(datagridXML);	
		objLayout.cells(objLayoutCell).progressOff();	
		}
	
}
function getParams() {
	var idx = document.URL.indexOf('?');
	if (idx != -1) 
	{
		var tempParams = new Object();
		var pairs = document.URL.substring(idx+1,document.URL.length).split('&');
		for (var i=0; i<pairs.length; i++) 
		{
			nameVal = pairs[i].split('=');
			tempParams[nameVal[0]] = nameVal[1];			
		}
		return tempParams;
	}
}
