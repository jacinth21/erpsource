//This function is used to download the excel 
function fnDownloadXLS() {
	gObj.toExcel('/phpapp/excel/generate.php');
}
//This function is called when the left link is clicked
function fnOnload(){	 
	if (objGridData != '') {
		gObj = initGridData('thbdashRpt', objGridData);
		gObj.attachHeader("#text_filter,#text_filter,#text_filter,#text_filter,#text_filter");
    }
}
function fnCallEdit(loadid){
	document.frmTHBReceivingReport.action ="/gmTHBReceivingLoadTran.do?method=fetchTHBLoadDetailsfrmDash&strFrom=dash&strLoadId="+ loadid;
	document.frmTHBReceivingReport.submit();
}

//This function is called while initiating the grid
function initGridData(divRef,gridData)
{
	var gObj = new dhtmlXGridObject(divRef);
		gObj.setSkin("dhx_skyblue");
		gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
		gObj.enableMultiline(false);	
		gObj.enableDistributedParsing(true);
		gObj.init();
		gObj.loadXMLString(gridData);
	return gObj;	
}