var Lotcodeid;
var ctrlnum;
var varImgInd;
var LotdtlId;
var Status;
var totallo;
var scanallo;
var scanRowId;
// To fetch the lot number details of the selected box number
function fnFetchLoadDtls() {
	var loadid = document.frmTHBLoad.strLoadId.value;
	var ajaxUrl = fnAjaxURLAppendCmpInfo('gmTHBReceivingLoadTran.do?method=fetchTHBLoadDetailsByBoxNum&strLoadId='
			+ loadid + '&ramdomId=' + Math.random());
	dhtmlxAjax.get(ajaxUrl, fnGetTHBLoadDtl);
	document.frmTHBLoad.strCtrlNo.focus();
}
// To display the grid and set other values based on the box number selected
function fnGetTHBLoadDtl(loader) {
	var status = document.frmTHBLoad.strStatus.value;
	var response = loader.xmlDoc.responseText;
	if (response != null && response.length > 0) {
		var resgrid = response.split("||");
		document.getElementById("totAlloId").innerHTML = '&nbsp;' + resgrid[0];
		totallo = resgrid[0];
		document.getElementById("scanAlloId").innerHTML = '&nbsp;' + resgrid[1];
		scanallo = resgrid[1];
		document.frmTHBLoad.strStatus.value = resgrid[2];
		Status = resgrid[2];
		gObj = initGridData('THBReceive', resgrid[3]); //
		gObj.attachHeader("#rspan,#text_filter,#text_filter,#text_filter");
		var loadId = document.getElementById("strLoadId");
		var loadVal = loadId.options[loadId.selectedIndex].value;
		if (loadVal == 0) {
			document.getElementById("scanContId").style.display = "none";
		} else {
			document.getElementById("scanContId").style.display = "block";
		}
		if (document.frmTHBLoad.strStatus.value == '107881') // Status:Scan
		// completed
		{
			document.frmTHBLoad.Btn_Add.disabled = true;
			document.frmTHBLoad.Btn_Scan.disabled = true;
			document.frmTHBLoad.strCtrlNo.disabled = true;
		} else {
			document.frmTHBLoad.Btn_Add.disabled = false;
			document.frmTHBLoad.Btn_Scan.disabled = false;
			document.frmTHBLoad.strCtrlNo.disabled = false;
		}
	}

}
// To initiate the grid
function fnOnPageLoad() {
	gObj = initGridData('THBReceive', objGridData);
	gObj.enableBlockSelection(true);
	gObj.attachEvent("onKeyPress", keyPressed);
	gObj.attachHeader("#rspan,#text_filter,#text_filter,#text_filter");
	var status = document.frmTHBLoad.strStatus.value;
	if (status == '107881') {
		document.frmTHBLoad.Btn_Add.disabled = true;
		document.frmTHBLoad.Btn_Scan.disabled = true;
		document.frmTHBLoad.strCtrlNo.disabled = true;
	} else {
		document.frmTHBLoad.Btn_Add.disabled = false;
		document.frmTHBLoad.Btn_Scan.disabled = false;
		document.frmTHBLoad.strCtrlNo.disabled = false;
	}
}

function initGridData(divRef, gridData) {
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/extweb/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableMultiline(false);
	gObj.enableDistributedParsing(true);
	gObj.init();
	gObj.loadXMLString(gridData);
	return gObj;
}

// To fetch the box number details based on the given load number
function fnFetchBoxDtls() {
	fnValidateTxtFld('strLoadNo', ' Load Number');
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	var LNo = document.frmTHBLoad.strLoadNo.value;
	document.frmTHBLoad.action = "/gmTHBReceivingLoadTran.do?method=fetchTHBLoadDetails&LoadNo="
			+ LNo;
	fnStartProgress('Y');
	document.frmTHBLoad.submit();

}
// While scanning the graft
function fnScanGraft(obj) {
	if (event.keyCode == 13) {
		fnAddScanKey();
	}

}
// To check whether the scanned lot number is available or not and save the
// status
function fnAddScanKey(obj) {
	var loadid;
	var coord;
	var rowId;
	var sarray = new Array();
	var scanedval = document.frmTHBLoad.strCtrlNo.value;
	Lotcodeid = gObj.getColIndexById("Load_id");
	ctrlnum = gObj.getColIndexById("ctrl_no");
	varImgInd = gObj.getColIndexById("imgInd");
	LotdtlId = gObj.getColIndexById("Dtl_id");
	var lotTotalcount = gObj.getRowsNum();
	var imgvalue;
	indexof = scanedval.indexOf("^"); // get the index of '~'
	cntlindexof = scanedval.indexOf("(10)"); // get the index of '(10)'
	if (indexof != -1) {// While scanning only format will be "partno^controlno"
		var cnumarr = scanedval.split("^");
		scanedPart = cnumarr[0]; // get the characters before '^'
		scanedval = cnumarr[1]; // Getting the remaining character, after "^"
	} else if (cntlindexof != -1) {// While scanning only format will be
		// "(01)udi number(17)exp date(10)controlno"
		var cnumArray = scanedval.split("(10)");
		scanedval = cnumArray[1]; // //Getting the remaining character, after
		// "(10)"
	}
	scanedval = scanedval.toUpperCase();
	// set the control number back to the textbox by removing the unwanted
	// values in the scan
	document.frmTHBLoad.strCtrlNo.value = scanedval;
	if (scanedval != '') {
		// Get the coordinated of the cell if the lot number is available
		coord = gObj.findCell(scanedval, ctrlnum, true);
		// If the lot number is not available, and if user confirms need to save
		// the value in backend
		if (coord.length == 0) {
			fnPlayErrorSound('beep');
			if (confirm(message_operations[780])) {
				fnSaveScanGraft(scanedval, 0);
				fnShowValinGrid(scanedval);
				document.frmTHBLoad.strCtrlNo.value = '';
				return false;
			}
		}
		// If lot number already avaialbe, get the corresponding rowid
		if (coord.length != 0) {

			rowId = coord[0][0];

		}
		imgvalue = gObj.cellById(rowId, varImgInd).getValue();
		// if the lot number is already scanned need to display validation
		// message
		if (imgvalue != '') {
			sarray[0] = scanedval;
			fnPlayErrorSound('beep');
			Error_Details(Error_Details_Trans(message_operations[779],
					scanedval));
		}
		gObj
				.cellById(rowId, varImgInd)
				.setValue(
						'<img src="/images/success.gif" height="17" width="18" alt="Success" border="0">');
		if (ErrorCount > 0) {
			Error_Show();
			Error_Clear();
			return false;
		}
	}
	lotDtlval = gObj.cellById(rowId, LotdtlId).getValue();
	// to update the status of the control number as received
	fnSaveScanGraft(scanedval, lotDtlval);
	// to sort the rows based on the tick mark
	gObj.sortRows(varImgInd, "str", "DES");
	document.frmTHBLoad.strCtrlNo.value = '';

}

// function to display the new value in the grid
function fnShowValinGrid(lotnum) {
	scanRowId++;
	var cellValue = new Array();
	cellValue[0] = '<img src="/images/success.gif" height="17" width="18" alt="Success" border="0">';
	cellValue[1] = '';
	cellValue[2] = lotnum;
	cellValue[3] = '';
	gObj.addRow(scanRowId, cellValue);
	gObj.sortRows(varImgInd, "str", "DES");
}
// To update or save the lot number received details
function fnSaveScanGraft(scanedctrlval, lotDtlval) {
	var loadid = document.frmTHBLoad.strLoadId.value;
	var ajaxUrl = fnAjaxURLAppendCmpInfo('gmTHBReceivingLoadTran.do?method=saveTHBScannedGrafts&strLoadDtlId='
			+ lotDtlval
			+ '&strCtrlNo='
			+ scanedctrlval
			+ '&strLoadId='
			+ loadid + '&ramdomId=' + Math.random());
	dhtmlxAjax.get(ajaxUrl, fnGetScanGraft);
}

// function to re-set the count after saving new value
function fnGetScanGraft(loader) {
	var response = loader.xmlDoc.responseText;
	if (response != null && response.length > 0) {
		var resgrid = response.split("||");
		document.getElementById("totAlloId").innerHTML = '&nbsp;' + resgrid[0];
		totallo = resgrid[0];
		document.getElementById("scanAlloId").innerHTML = '&nbsp;' + resgrid[1];
		scanallo = resgrid[1];
	}
}

// while clicking on scan complete button
function fnUpdateLoadDtls() {
	fnStartProgress();
	var loadid = document.frmTHBLoad.strLoadId.value;
	var confirmSubmit;
	if (totallo != scanallo) {
		confirmSubmit = confirm(message_operations[782]);
	} else {
		confirmSubmit = confirm(message_operations[781]);
	}
	if (!confirmSubmit) {
		fnStopProgress();
		return false;
	}
	document.frmTHBLoad.action = "/gmTHBReceivingLoadTran.do?method=updTHBScannedGrafts&strLoadId="
			+ loadid;
	document.frmTHBLoad.submit();
	document.frmTHBLoad.Btn_Add.disabled = true;
	document.frmTHBLoad.Btn_Scan.disabled = true;
	document.frmTHBLoad.strCtrlNo.disabled = true;

}
// while pressing enter key from load number textbox
function fnCheckKey() {
	if (event.keyCode == 13) {
		var rtVal = fnFetchBoxDtls();
		if (rtVal == false) {
			if (!e)
				var e = window.event;

			e.cancelBubble = true;
			e.returnValue = false;

			if (e.stopPropagation) {
				e.stopPropagation();
				e.preventDefault();
			}
		}
	}
}

function keyPressed(code, ctrl) {
	if (code == 67 && ctrl) {
		gridObj.setCSVDelimiter("\t");
		gridObj.copyBlockToClipboard();
	}
	return true;
}

function fnPlayErrorSound(soundObj) {
	try {
		var sound = document.getElementById(soundObj);

		if (sound)
			sound.Play();
	} catch (err) {
		return null;
	}

}