var analyPartStr = '';
var analyPartbtns = document.getElementsByClassName("analyPart-close-sm");
var loadPartStr = '';
var loadPartbtns = document.getElementsByClassName("loadPart-close-sm");
var columStatusStr = '';
var columStatusbtns = document.getElementsByClassName("columStatus-close-sm");
var check_rowId = '';
var hAnalysisIdStr = '';
var fmdate_rowId = '';
var todate_rowid = '';
var fmdateVal = '';
var todateVal = '';
var status_rowid = '';
var statusVal = '';
// Function to download the excel report
function fnDownloadXLS() {
	gridObj.setColumnHidden(0,true);
	gridObj.toExcel('/phpapp/excel/generate.php');
	gridObj.setColumnHidden(0,false);
}

// this function is used to fetch the Report List when load button is clicked
function fnFetchListRptDtls(obj) {
	hAnalysisIdStr = '';
	var fromDt = document.frmTHBAnalyListRpt.procStartDt.value;
	var toDt = document.frmTHBAnalyListRpt.procEndDt.value;
	var analysisId = document.frmTHBAnalyListRpt.analysisId.value;
	var loadId = document.frmTHBAnalyListRpt.loadId.value;
	var status = document.frmTHBAnalyListRpt.status.value;
	/*if(analyPartStr == '')
		{
		analyPartStr = document.frmTHBAnalyListRpt.analysisId.value;
		}
	if(loadPartStr == '')
		{
		loadPartStr = document.frmTHBAnalyListRpt.loadId.value;
		}*/
 	CommonDateValidation(document.frmTHBAnalyListRpt.procStartDt,format,message_operations[783]+format);
 	CommonDateValidation(document.frmTHBAnalyListRpt.procEndDt,format,message_operations[784]+format);
	if (fromDt != "" && toDt == "") {
		Error_Details(message_operations[789]);
	}
	if (fromDt == "" && toDt != "") {
		Error_Details(message_operations[788]);
	}
	if (dateDiff(fromDt, toDt, format) < 0) {
		Error_Details(message_operations[787]);
	}
	if(isNaN(loadId))
		{
		Error_Details("Please enter valid THB LoadNum");
		}
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	fnStartProgress();
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmTHBAnalyListRpt.do?method=fetchListReportDetails&analysisId='
			+ analysisId
			+ '&loadId='
			+ loadId
			+ '&status='
			+ status
			+ '&procStartDt='
			+ fromDt
			+ '&procEndDt='
			+ toDt
			+ '&ramdomId='
			+ Math.random());
	dhtmlxAjax.get(ajaxUrl, fnLoadListRptDtls);
}
// this is the ajax loader function to form Grid 
function fnLoadListRptDtls(loader) {

	var response = loader.xmlDoc.responseText;
	// Pending condition check if response is empty
	if (response == '' || response == null || response == undefined) {
		fnStopProgress();
		document.getElementById("divNoExcel").style.display = 'none';
		document.getElementById("thbListRpt").style.display = 'none';
		document.getElementById("thbchoose").style.display = 'none';
		document.getElementById("DivNoMsg").style.display = 'block';
	} else {
		var rows = [], i = 0;
		var JSON_Array_obj = "";
		JSON_Array_obj = JSON.parse(response);
		$.each(JSON_Array_obj, function(index, jsonObject) {
			Analy_id = jsonObject.ANALYSISID;
			Load_id = jsonObject.LOADID;
			fmdate = jsonObject.PFMDATE;
			todate = jsonObject.PTODATE;
			loadstatus = jsonObject.LOADSTATUS;
			num_of_donors = jsonObject.NUMOFDONORS;
			total_part_qty = jsonObject.TOTALPARTQTY;
			sqty = jsonObject.SQTY;
			// default array
			rows[i] = {};
			rows[i].data = [];
			//
			rows[i].id = Analy_id;
			rows[i].data[0] = '';
			rows[i].data[1] = Analy_id;
			rows[i].data[2] = Load_id;
			// rows[i].data[3] = num_of_donors;
			rows[i].data[3] = "<a href='#'  onClick=fnLoadDonorLoadDtls("
					+ Load_id + ")>" + num_of_donors + "</a>";
			rows[i].data[4] = total_part_qty;
			rows[i].data[5] = sqty;
			rows[i].data[6] = fmdate;
			rows[i].data[7] = todate;
			rows[i].data[8] = loadstatus;
			i++;
		});
		var setInitWidths = "60,120,100,85,85,85,150,150,150";
		var setColAlign = "center,left,right,right,right,right,center,center,center";
		var setColTypes = "ra,ro,ro,ro,ro,ro,ro,ro,ro";
		var setColSorting = "na,str,str,str,int,int,date,date,str";
		var setHeader = [ " ", "Analysis ID", "Load#", "# of Donors",
				" Total Qty", "Scheduled Qty", "Processing Start Date",
				"Processing End Date", "Status" ];
		var setFilter = [ "", "#text_filter", "#text_filter",
				"#text_filter", "#numeric_filter", "#numeric_filter",
				"#text_filter", "#text_filter", "#text_filter" ];
		var setColIds = "check,Analy_id,Load_id,num_of_donors,total_part_qty,sqty,fmdate,todate,status";
		var enableTooltips = "true,true,true,true,true,true,true,true,true";
		var footerArry = [];
		var gridHeight = "";
		var footerStyles = [];
		var footerExportFL = true;
		var dataHost = {};
		dataHost.rows = rows;
		gridObj = loadDHTMLXGrid('thbListRpt', gridHeight, dataHost, setHeader,
				setInitWidths, setColAlign, setColTypes, setColSorting,
				setColIds, enableTooltips, setFilter, footerArry, footerStyles,
				footerExportFL);
		gridObj.attachEvent("onCheck", doOnCheck);
		// to set background colour for each cell
		var gridrows = gridObj.getAllRowIds(",");
		var gridrowsarr = gridrows.toString().split(",");
		var arrLen = gridrowsarr.length;
		var sched_qty; 
		var total_qty;

		for (var i = 0; i < arrLen; i++) {
			rowId = gridrowsarr[i];
			sched_qty = gridObj.cellById(rowId,5).getValue();
			total_qty = gridObj.cellById(rowId,4).getValue();
			if(sched_qty < total_qty){
				gridObj.setCellTextStyle(rowId, 5,"background-color:red");
			}else if(total_qty == sched_qty){
				gridObj.setCellTextStyle(rowId, 5,"background-color:#57f73b");
			}
		}
		
		check_rowId = gridObj.getColIndexById("check");
		analy_rowid = gridObj.getColIndexById("Analy_id");
		fmdate_rowid = gridObj.getColIndexById("fmdate");
		todate_rowid = gridObj.getColIndexById("todate");
		status_rowid = gridObj.getColIndexById("status");
		document.getElementById("thbListRpt").style.display = 'block';
		document.getElementById("thbchoose").style.display = 'block';
		document.getElementById("divNoExcel").style.display = 'block';
		document.getElementById("divNoExcelLine").style.display = 'block';
		document.getElementById("DivNoMsg").style.display = 'none';
		fnStopProgress();
	}
}

function doOnCheck(rowId, cellInd, stage) {
	rowID = rowId;
	fnSetValue(rowID);
	return true;
}

function fnSetValue(rowId) {
	var checkVal = '';
	var analynum = '';
	checkVal = gridObj.cellById(rowId, check_rowId).getValue();
	analynum = gridObj.cellById(rowId, analy_rowid).getValue();
	fmdateVal = gridObj.cellById(rowId, fmdate_rowid).getValue();
	todateVal = gridObj.cellById(rowId, todate_rowid).getValue();
	statusVal = gridObj.cellById(rowId, status_rowid).getValue();
	if (checkVal == 1) {
		hAnalysisIdStr = analynum;
	}

}
// used to load Load Details Report
function fnLoadDonorLoadDtls(Load_id) {
	 windowOpener(
	            "/gmDonorLoadDtlsRpt.do?method=fetchDonorLoadDtsl&loadNum="
	                    + Load_id, "View Load Details",
	            "resizable=yes,scrollbars=yes,top=150,left=150,width=800,height=550");
}
// function called when choose action dropdown is selected and submitted
function fnChooseAction() {
	var v_action_val = document.frmTHBAnalyListRpt.Cbo_Action1.value;
	if (v_action_val == '0') {
		v_fnAction = eval('fnDoAction')
	} else {
		v_fnAction = eval('fnDo' + v_action_val + 'Action')
	}

	v_fnAction(hAnalysisIdStr);

}
// function called if the choose one is selected 
function fnDoAction(hAnalysisIdStr) {
	Error_Details("Please Select DropDown Value");

	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
}
//this function called if the dropdown is selected as Schedule
function fnDo108120Action(hAnalysisIdStr) {
	if (hAnalysisIdStr == '') {
		Error_Details(message_operations[795]);//95
	}else
		{
		fnStartProgress("Y");
		document.frmTHBAnalyListRpt.action = "/gmTHBAnalysisDetails.do?method=fetchTHBAnalysisDetails&analysisId="+hAnalysisIdStr+"&status="+statusVal;
		document.frmTHBAnalyListRpt.submit();
		}
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}

}
//this function called if the dropdown is selected as Edit Processing Dates
function fnDo108121Action(hAnalysisIdStr) {
	if (hAnalysisIdStr == '') {
		Error_Details(message_operations[795]);
	}
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}

	windowOpener(
			"/gmTHBLoadPendDash.do?method=loadLockAndGenerate&strAnalysisId="
					+ hAnalysisIdStr + "&strScreen=List_rpt"+"&fromDate="+fmdateVal+"&toDate="+todateVal, "THB Edit Processing Dates",
			"resizable=yes,scrollbars=yes,top=150,left=150,width=550,height=250");

}
//this function called if the dropdown is selected as Void Analysis
function fnDo108122Action(hAnalysisIdStr) {
	if (hAnalysisIdStr == '') {
		Error_Details(message_operations[795]);
	}
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	
	document.frmTHBAnalyListRpt.hCancelType.value = "LOCKVO";
	document.frmTHBAnalyListRpt.hCancelTypeFromMenu.value = "Cancelling Analysis Lock";	
	document.frmTHBAnalyListRpt.action = gStrServletPath+ "/GmCommonCancelServlet";
    document.frmTHBAnalyListRpt.hTxnId.value = hAnalysisIdStr;
	document.frmTHBAnalyListRpt.hAction.value = "Load";
	fnStartProgress('Y');
	document.frmTHBAnalyListRpt.submit();


}

//This function is called when the left link is clicked
function fnOnPageLoad() {
	document.getElementById("divNoExcel").style.display = 'none';
	document.getElementById("divNoExcelLine").style.display = 'none';
	document.getElementById("thbListRpt").style.display = 'none';
	document.getElementById("thbchoose").style.display = 'none';
	document.getElementById("DivNoMsg").style.display = 'block';

}

// As of now we are commenting the below dynamic filter procedures,it might be added later
/*function fnGenerateAnalysisStr(obj) {
var analyPart = TRIM(obj.value);
if (analyPart == '')
	return;
analyPartStr = analyPartStr + ',' + analyPart;
document.getElementById("AnalysisPartTr").style.display = "block";
document.getElementById("AnalysisPartTrLine").style.display = "block";
dataAppend = "<li id='curranalyPartDtls' style=\"list-style-type: none;\"><div>";
dataAppend = dataAppend + analyPart
dataAppend = dataAppend
		+ "&nbsp;&nbsp;&nbsp;<span class='close-sm analyPart-close-sm' id='closebtn'><a href=\"#\"><span class=\"glyphicon glyphicon-remove\" title='Remove' width='15' height='15'></span></a></span>&nbsp;&nbsp;";
dataAppend = dataAppend + " </div></li>";
$("#AnalysisPartDiv ul").append(dataAppend);

document.frmTHBAnalyListRpt.analysisId.value = '';
document.frmTHBAnalyListRpt.analysisId.focus();

for (i = 0; i < analyPartbtns.length; i++) {
	analyPartbtns[i].onclick = function(rownum) {
		remVal = $(this).parent().parent().text().trim();
		$(this).parent().parent().remove();
		if (analyPartStr != '') {
			analyPartStr = (analyPartStr.indexOf(',' + remVal) != -1) ? analyPartStr
					.replace(',' + remVal, '')
					: analyPartStr.replace(remVal, '');
		}
		if (analyPartStr == '') {
			document.getElementById("AnalysisPartTr").style.display = "none";
			document.getElementById("AnalysisPartTrLine").style.display = "none";
		}
	}
}
}*/

/*function fnGenerateLoadNumStr(obj) {
var loadPart = TRIM(obj.value);
if (loadPart == '')
	return;
loadPartStr = loadPartStr + ',' + loadPart;
document.getElementById("LoadPartTr").style.display = "block";
document.getElementById("LoadPartTrLine").style.display = "block";
dataAppend = "<li id='currloadPartDtls' style=\"list-style-type: none;\"><div>";
dataAppend = dataAppend + loadPart
dataAppend = dataAppend
		+ "&nbsp;&nbsp;&nbsp;<span class='close-sm loadPart-close-sm' id='closebtn'><a href=\"#\"><span class=\"glyphicon glyphicon-remove\" title='Remove' width='15' height='15'></span></a></span>&nbsp;&nbsp;";
dataAppend = dataAppend + " </div></li>";
$("#LoadPartDiv ul").append(dataAppend);

document.frmTHBAnalyListRpt.loadId.value = '';
document.frmTHBAnalyListRpt.loadId.focus();

for (i = 0; i < loadPartbtns.length; i++) {
	loadPartbtns[i].onclick = function(rownum) {
		remVal = $(this).parent().parent().text().trim();
		$(this).parent().parent().remove();
		if (loadPartStr != '') {
			loadPartStr = (loadPartStr.indexOf(',' + remVal) != -1) ? loadPartStr
					.replace(',' + remVal, '')
					: loadPartStr.replace(remVal, '');
		}
		if (loadPartStr == '') {
			document.getElementById("LoadPartTr").style.display = "none";
			document.getElementById("LoadPartTrLine").style.display = "none";
		}
	}
}
}*/

/*
* function fnGenerateStatusStr(obj) { // var partType =''; var columnStatusObj =
* document.frmTHBAnalyListRpt.status; var columnStatus =
* columnStatusObj.options[columnStatusObj.selectedIndex].text; // for getting
* Id var columnStatusId = document.frmTHBAnalyListRpt.status.value; if
* (columnStatusId == '0') return; columStatusStr = columStatusStr + ',' +
* columnStatusId; document.getElementById("columnStatusTr").style.display =
* "block"; dataAppend = "<li id='currStatFilterDtls' style=\"list-style-type: none;\"><div>";
* dataAppend = dataAppend + columnStatus dataAppend = dataAppend +
* "&nbsp;&nbsp;&nbsp;<span class='close-sm columStatus-close-sm'
* id='closebtn'><a href=\"#\"><span class=\"glyphicon glyphicon-remove\"
* title='Remove' width='15' height='15'></span></a></span>&nbsp;&nbsp;";
* dataAppend = dataAppend + " </div></li>"; $("#columnStatusDiv
* ul").append(dataAppend); document.frmTHBAnalyListRpt.status.value = '0';
* document.frmTHBAnalyListRpt.status.focus();
* 
* for (i = 0; i < columStatusbtns.length; i++) { columStatusbtns[i].onclick =
* function(rownum) { remVal = $(this).parent().parent().text().trim();
* $(this).parent().parent().remove(); if (columStatusStr != '') {
* alert((columStatusStr.indexOf(',' + remVal) != -1)); columStatusStr =
* (columStatusStr.indexOf(',' + remVal) != -1) ? columStatusStr .replace(',' +
* remVal, '') : columStatusStr.replace(remVal, ''); } if (columStatusStr == '') {
* document.getElementById("columnStatusTr").style.display = "none"; } } } }
*/