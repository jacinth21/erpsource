function fnShowNCMR(val)
{
windowOpener("/GmNCMRServlet?hAction=PrintNCMR&hId="+val,"NCMR","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=640");
}
function fnShowEVAL(val)
{
windowOpener("/GmNCMRServlet?hAction=PrintEVAL&hId="+val,"NCMR","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=640");
}
//Pending Action, Pending Closure
function fnUpdateNCMR(id,val)
{
	var parentLoc =parent.location.href;
	
	if(parentLoc.indexOf("gmPOBulkReceive.do") != -1){// When loading from Bulk Shipment screen
		parent.location = "/GmNCMRServlet?hAction=UpdateNCMR&hNCMRId="+id+"&hMode="+val+"&"+fnAppendCompanyInfo();
	}else{// From NCMR Dashboard
		document.frmOperDashBoardDispatch.hNCMRId.value = id;
		document.frmOperDashBoardDispatch.hMode.value = val;
		document.frmOperDashBoardDispatch.hAction.value = "UpdateNCMR";
		document.frmOperDashBoardDispatch.action = "GmNCMRServlet";
		document.frmOperDashBoardDispatch.submit();
	}
}
//Pending Eval
function fnUpdateNCMREval(id,val)
{
	var parentLoc =parent.location.href;
	 
	if(parentLoc.indexOf("gmPOBulkReceive.do") != -1){// When loading from Bulk Shipment screen
		parent.location = "/GmNCMRServlet?hAction=UpdateNCMREVAL&hNCMRId="+id+"&hMode="+val+"&"+fnAppendCompanyInfo();
	}else{// From NCMR Dashboard
		
		document.frmOperDashBoardDispatch.hNCMRId.value = id;
		document.frmOperDashBoardDispatch.hMode.value = val;
		document.frmOperDashBoardDispatch.hAction.value = "UpdateNCMREVAL";
		document.frmOperDashBoardDispatch.action = "GmNCMRServlet";
		document.frmOperDashBoardDispatch.submit();
	}
}

function fnOnload(){	
	if (objGridData != '') {
		gridObj = initGridData1('recshipmntRpt', objGridData);
    }
}
function initGridData1(divRef,gridData)
{
	var gObj = new dhtmlXGridObject(divRef);
		gObj.setSkin("dhx_skyblue");
		gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
		gObj.enableTooltips("true,true,true,true,true,true");
		gObj.init();
		gObj.loadXMLString(gridData);
	return gObj;	
}