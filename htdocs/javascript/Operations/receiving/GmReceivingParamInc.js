var gridObj = '';
var qcChkd, RecChkd, labelsPrintChkd, preStageChk, stageChk ;

function fnLoad(){
	var attributevalue = '';

	if (objGridData.value != '') {
		gridObj = initGridData('RECEIVEATTRIBUTEDATA',objGridData);   

		gridObj.forEachRow(function(rowId){
	
			var attrid = gridObj.getColIndexById("ATTRID");
			var chkid = gridObj.getColIndexById("CHKFL");
			attributevalue = TRIM(gridObj.cellById(rowId, attrid).getValue());

			if(attributevalue == '104520' && form42chk == 'N'){
				var chk_rowId = gridObj.getColIndexById("CHKFL");
				gridObj.cellById(rowId, chk_rowId).setDisabled(true);
			}

			if(attributevalue == '104521' && labelprintchk == 'N'){
				var chk_rowId = gridObj.getColIndexById("CHKFL");
				gridObj.cellById(rowId, chk_rowId).setDisabled(true);
			}
			if(attributevalue == '104522' && qcverifiedchk == 'N'){
				var chk_rowId = gridObj.getColIndexById("CHKFL");
				gridObj.cellById(rowId, chk_rowId).setDisabled(true);
			}
		
			//Disable the checkbox if user dont have access or dhr status crossed pending inspection
			if(attributevalue == '104523' && (dhrstatus>=2||preStagingchk == 'N')){
				var chk_rowId = gridObj.getColIndexById("CHKFL");
				gridObj.cellById(rowId, chk_rowId).setDisabled(true);
			}
			//Disable the checkbox if user dont have access or dhr status crossed pending inspection

			if(attributevalue == '104524' && (dhrstatus>=2||stagingchk == 'N')){
				var chk_rowId = gridObj.getColIndexById("CHKFL");
				gridObj.cellById(rowId, chk_rowId).setDisabled(true);
			}
			if(attributevalue == '104523'){//Pre Staging
				
			 preStageChk = gridObj.cellById(rowId, chkid).getValue();
				
			}
			// If the Pre - Staging is not checked disabling the Staging check box
			if(attributevalue == '104524'){//Staging
				stageChk = gridObj.cellById(rowId, chkid).getValue();
				if(preStageChk == 1){
					gridObj.cellById(rowId, chkid).setDisabled(false);
				}
				else
					{
					gridObj.cellById(rowId, chkid).setDisabled(true);
					}
			}
			
			
			if(attributevalue == '104522'){//QC Verified
				qcChkd = gridObj.cellById(rowId, chkid).getValue();
			}
			
			if(attributevalue == '104520'){ //42 Received (QC Verification of Entries)
				RecChkd = gridObj.cellById(rowId, chkid).getValue();
				// If the checkbox is checked, then disable it. Since the RHPN is releasing to verification before completing the shipment
				if(RecChkd == 1){
					gridObj.cellById(rowId, chkid).setDisabled(true);
				}
			}
			
			if(attributevalue == '104521'){ //Labels Printed
				labelsPrintChkd = gridObj.cellById(rowId, chkid).getValue();
			}
			
		});
		 
	}
	if(parent.window.document.getElementById("ajaxdivcontentarea") != undefined){
  		parent.window.document.getElementById("ajaxdivcontentarea").style.height = "170";
  		parent.window.document.getElementById("ajaxdivcontentarea").style.width = "1090";
  	}
}

//This function is used to initiate grid
function initGridData(divRef,gridData){
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);
	gObj.init();
	gObj.loadXMLString(gridData);	
	return gObj;	
}
function fnSaveDHRParamter(form){
	var attr_type = '104560';
	var attr_value = '';
	var inputstr = '';
	var chkflag = '';
	fnStartProgress();
	gridObj.forEachRow(function(rowId){
		var attrid = gridObj.getColIndexById("ATTRID");
		var chkid = gridObj.getColIndexById("CHKFL");
		attr_value = TRIM(gridObj.cellById(rowId, attrid).getValue());
		chkflag = TRIM(gridObj.cellById(rowId, chkid).getValue());
		/*if(chkflag == 1){
		inputstr += attr_value + ',';
		}*/
		
		//Should prepare the string only for those rows whose checkbox value changed
		if(attr_value == '104522' && chkflag != qcChkd){//QC Verified
			inputstr += attr_value + ',' + chkflag + '|';
		}
		
		if(attr_value == '104520'  && chkflag != RecChkd){ //42 Received
			inputstr += attr_value + ',' + chkflag + '|';
		}
		
		if(attr_value == '104521' && chkflag != labelsPrintChkd){ //Labels Printed
			inputstr += attr_value + ',' + chkflag + '|';
		}
		
		if(attr_value == '104523' && chkflag != preStageChk){ //Pre-Staging
			inputstr += attr_value + ',' + chkflag + '|';
		}
		if(attr_value == '104524' && chkflag != stageChk){ //Labels Printed
			inputstr += attr_value + ',' + chkflag + '|';
		}
	});
	
	if(qcChkd == '0' && RecChkd =='0' && labelsPrintChkd == '0' && inputstr == ''){// Validation should show only if all the check boxes where unchecked already
		Error_Details(message_operations[94]);
	}
	if (ErrorCount > 0){
		fnStopProgress();
		Error_Show();
	    Error_Clear();
		return false;
	}
	form.inputstr.value = inputstr;
	form.strOpt.value = 'save';
	form.attrtype.value = attr_type;
	form.action = "/GmReceivingParamInc.do";
	form.submit();
	
}