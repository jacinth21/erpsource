//This function is used to initiate grid
var gridObj;
var errorVal = false;
function fnOnPageLoad(){
	var sel = document.all.hSearch.value;
	if(sel != ''){
		document.all.Cbo_Search.value = sel;
	}
	if(objGridData.value != ''){ 
		gridObj = initGridData('dataGridDiv',objGridData);
	}
	
}

function initGridData(divRef,gridData){
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.attachHeader("#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#select_filter,#numeric_filter,#select_filter,#text_filter,#numeric_filter,#text_filter,#text_filter,#select_filter,#select_filter,#text_filter,#text_filter");
	gObj.init();
	gObj.enablePaging(true, 100, 10, "pagingArea", true, "infoArea");
	gObj.setPagingSkin("bricks");
	gObj.enableSmartRendering(true);// To enable rendering
	gObj.loadXMLString(gridData);	
	return gObj;	
}

//When click on Load button, below function 
function fnLoad(){
	var frm = document.frmLotTrackReport;
	
	// Below function is used to validate the form
	fnValidation(frm);
	
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	
	frm.strOpt.value = 'ReLoad';
	frm.action = "/gmReservedLots.do?method=fetchReservedLotsDetails";
	fnStartProgress();
	frm.submit();
		
}

// Function to validate the date fields
function fnValidation(frm){
	var partNum = TRIM(frm.partNum.value);
	var ctrlNum = TRIM(frm.ctrlNum.value);
	var donorNum = TRIM(frm.donorNum.value);
	var warehouse = TRIM(frm.warehouse.value);
	var expDateObj = frm.expDate;
	var expDate = TRIM(frm.expDate.value);
	var expDateRange = TRIM(frm.expDateRange.value);
	var refId = TRIM(frm.refId.value);
	if(document.all.Cbo_Search){
		var cboSearch = document.all.Cbo_Search.value;
		if(partNum == '' && cboSearch != '0'){
			Error_Details(message_operations[95]);
			errorVal = true;
		}else{
			errorVal = false;
		}
	}
	if(partNum == '' && ctrlNum == '' && donorNum == '' && refId == '' && warehouse == '0' && expDate == '' && expDateRange == '0' && (frm.internationalUse.checked == false) && errorVal == false){
		Error_Details(message_operations[96]);
	}
	if(expDateRange == '0' && expDate != ''){
		Error_Details(message_operations[97]);
	}else if(expDateRange != '0' && expDate == ''){
		Error_Details(message_operations[98]);
	}else if(expDate != ''){
		CommonDateValidation(expDateObj,format,Error_Details_Trans(message_operations[73],format));
	}
}

//Function to export excel
function fnExport()
{	
	gridObj.toExcel('/phpapp/excel/generate.php');
}

//This function is used to activate Load button while pressing the enter key
function fnEnter() {
	if(event.keyCode == 13){
		fnLoad();
	}
}