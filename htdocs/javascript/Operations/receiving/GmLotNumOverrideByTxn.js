//This function is used to hit when submit button clicked
function fnSubmitLotTrans(){

	fnValidateTxtFld('lotOverrideTxnids',message[10622]);
	
	if(ErrorCount > 0){
        Error_Show();
        Error_Clear();
        return false;
    }
	
	var strLotTransId = document.getElementById("lotOverrideTxnids").value;	
	strLotTransId = strLotTransId.toUpperCase();
	strLotTransId = strLotTransId.replace(/ /g,'');  //replace spaces
	strLotTransId = strLotTransId.replace(/(\n)/gm, ','); //replace new line
	fnStartProgress();
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmPartControlSetup.do?method=saveLotOverrideByTxn&lotOverrideTxnids='+strLotTransId+
			'&ramdomId='+new Date().getTime());
	dhtmlxAjax.get(ajaxUrl,fnSubmitLotTransCallback);
	 
}
//This function is used to populate Error message and Success message
function fnSubmitLotTransCallback(loader){
	response = loader.xmlDoc.responseText;
	if(response != ''){
		Error_Details(Error_Details_Trans(message[10623],response));	
		fnStopProgress();
	}else{
		document.getElementById("successCont").style.display='block';
		document.getElementById("successCont").style.color = 'green';
		document.getElementById("successCont").style.fontWeight = 'bold';
		document.getElementById("lotOverrideTxnids").value = '';	
		fnStopProgress();
	}
	if(ErrorCount > 0){
        Error_Show();
        Error_Clear();
        return false;
    }
	
}