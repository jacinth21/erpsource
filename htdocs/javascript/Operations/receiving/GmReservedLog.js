
function fnOnload(){	
	if(inventoryType != ''){
	document.frmReservedLogRpt.inventoryType.value = inventoryType;
	}
	if (objGridData != '') {
		gridObj = initGridData1('reservedLogRpt', objGridData);
    }
}

function initGridData1(divRef,gridData)
{
	var gObj = new dhtmlXGridObject(divRef);
		gObj.setSkin("dhx_skyblue");
		gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
		gObj.enableTooltips("true,true,true,true,true,true");
		gObj.init();
		gObj.loadXMLString(gridData);
	return gObj;	
}

function fnSubmit(){
	var objfromDate = document.frmReservedLogRpt.fromDate;
	var objtoDate = document.frmReservedLogRpt.toDate;
	if(objfromDate.value != ""){ 
		CommonDateValidation(objfromDate, format, message_operations[87]+format);
	}
	if(objtoDate.value != ""){ 
		CommonDateValidation(objtoDate, format, message_operations[88]+format);
	}
	if(objfromDate.value != "" && objtoDate.value == ""){
		Error_Details(message_operations[89]);
	}
	if(objtoDate.value != "" && objfromDate.value == ""){
		Error_Details(message_operations[90]);
	}
	var dateDifference = dateDiff(objfromDate.value,objtoDate.value,format);
	if(dateDifference < 0){		
			Error_Details(message_operations[91]);			
	}
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}else{
		document.frmReservedLogRpt.strOpt.value = "load";
		fnStartProgress('Y');
		document.frmReservedLogRpt.submit();
	}
}

//This function used to down load the excel file
function fnDownloadXLS() {
	gridObj.toExcel('/phpapp/excel/generate.php');
}

// the below code is referred from GmTransactionLog.js file
function fnShowTransSlip(val,type)
{
	var subs = val.substr(0,5);
    if (type == '4314')
	{
		//alert('Print DHR');
		fnPrintDHR(val);
	}  	
	else if (type == '4316' && subs =='GM-MA')
	{
		//alert('Print MA');
		fnPrintMA(val);
	}
	else if (type == '4310' )
	{
		//alert('Print Pack Slip');
		fnPrintPack(val);
	}
	else if (type == '4312')
	{
		//alert('Print RA');
		fnPrintRA(val);
	}
	else if (type == '4119'|| type == '4127' || type =='4322' 
		|| type ==  '4117' || type =='4118'   || type =='40051' 
		|| type == '40052' || type =='40053'  || type == '40054' 
		|| type == '50150' || type == '50151' || type == '50152' 
		|| type == '50153' || type == '400078'|| type == '50155'
		|| type == '50156' || type == '50157' || type == '50158'
		|| type == '50159' || type == '50160' || type == '50161'
		|| type == '50162' || type == '100062'|| type == '100063' 
		|| type == '100406'|| type == '100064'|| type == '120600'
		|| type == '120601'|| type == '120602'|| type == '120603'
		|| type == '120604'|| type == '120605'|| type == '120606'
		|| type == '120607'|| type == '120608'|| type == '120609'
		|| type == '120610'|| type == '120611'|| type == '120612'
		|| type == '400066'|| type == '400069'|| type == '120614'
		|| type == '120615'|| type == '120616'|| type == '120617'
		|| type == '120618'|| type == '120619'|| type == '120620'
		|| type == '120621'|| type == '1006570' || type == '1006470' 
		|| type == '4324'  || type == '400074' || type == '400075'
		|| type == '56010' || type == '56020' || type == '56021' 
		|| type == '56022' || type == '56023' || type == '56024' 
		|| type == '56025' || type == '56026' || type == '56027' 
		|| type == '56028' || type == '56040' || type == '56041' 
		|| type == '56042' || type == '56043' || type == '56044' 
		|| type == '56045' || type == '56046' || type == '56000' 
		|| type == '120628' || type == '120629' || type == '4000645')
		// Removed type=4311 (Consignment) from above condition. Now Consignment type is going to call this fnPickSlip()
	{
		
		fnPrintLN(val, type);
	} 
	else if(type == '4319'){
		fnViewPrintWO(val);
	}
	else
	{
		//alert('Print PIC Slip');
		fnPicSlip(val);
	}	
}

function fnPrintLN(val,type)
{
	windowOpener("/GmConsignInHouseServlet?hAction=PicSlip&hConsignId="+val,"SetPic","resizable=yes,scrollbars=yes,top=150,left=150,width=750,height=500");	
}

function fnPrintPO (val)
{
	vid = 'All';
	windowOpener("/GmPOServlet?hAction=PrintPO&hPOId="+val+"&hVenId="+vid,"PO1","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnPrintPack(val)
{
	windowOpener('/GmEditOrderServlet?hMode=PrintPack&hOrdId='+val,"Pack","resizable=yes,scrollbars=yes,top=150,left=150,width=760,height=610");	
}
function fnViewPrintWO(val)
{
windowOpener("/GmWOServlet?hAction=PrintWO&hWOId=" + val ,"WO1","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=640");
}

function fnPicSlip(val)
{	
	windowOpener("/GmConsignItemServlet?hAction=PicSlip&hConsignId="+val,"SetPic","resizable=yes,scrollbars=yes,top=150,left=150,width=750,height=500");	
}

function fnPrintDHR(val)
{
	windowOpener('/GmPOReceiveServlet?hAction=ViewDHRPrint&hDHRId='+val,"DHR","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnPrintMA(val)
{
	windowOpener('/gmMADetail.do?method=reportMADetail&maID='+val,"MA","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

// To open lot tracking report screen from the Lot number log screen 
function fnOpenLotTrackRpt(lotno)
{		
	windowOpener('/gmLotTrackReport.do?method=fetchLotTrackingDetails&strOpt=ReLoad&partNum=&ctrlNum='+lotno,'lottrackingreport',"resizable=yes,scrollbars=yes,top=475,left=420,width=1380,height=480");
}