
function fnOnload(){	
	if (objGridData != '') {
		gridObj = initGridData1('recdashRpt', objGridData);
    }
}
function initGridData1(divRef,gridData)
{
	var gObj = new dhtmlXGridObject(divRef);
		gObj.setSkin("dhx_skyblue");
		gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
		gObj.enableTooltips("true,true,true,true,true,true");
		gObj.init();
		gObj.loadXMLString(gridData);
		gObj.attachHeader("#text_filter,,#text_filter,#text_filter,#text_filter,#text_filter,#numeric_filter,#text_filter");
	return gObj;	
}

function fnBulkDHRLog(ID){	
	windowOpener("/GmCommonLogServlet?hType=4000697&hID="+ID,"BulkDHRLog","resizable=yes,scrollbars=yes,top=300,left=300,width=690,height=300");				
}					
function fnUpdateDHR(id,val)				
{	
	location.href =  "/GmPageControllerServlet?strPgToLoad=/gmPOBulkReceive.do?companyInfo="+compInfObj+"&method=loadBatchshipment&strOpt=loadRS&strMode=FETCHCONTAINER&rsNumber="+id;
}
function fnProcessTab(id){
	location.href =  "/GmPageControllerServlet?strPgToLoad=/gmPOBulkReceive.do?companyInfo="+compInfObj+"&method=loadBatchshipment&strOpt=loadRS&strMode=FETCHCONTAINER&selectedTab=parameter&rsNumber="+id;
}
//This function used to down load the excel file
function fnDownloadXLS() {
	var piCol = gridObj.getColIndexById("pi");
	var ppCol = gridObj.getColIndexById("pp");
	var pvCol = gridObj.getColIndexById("pv");
	gridObj.forEachRow( function(rowId){ 
	    var piColVal = gridObj.cellById(rowId,piCol).getValue();
	    if(piColVal == '-'){
	    	gridObj.cellById(rowId,piCol).setValue(""); 
	    }
	    var ppColVal = gridObj.cellById(rowId,ppCol).getValue();
	    if(ppColVal == '-'){
	    	gridObj.cellById(rowId,ppCol).setValue(""); 
	    }
	    var pvColVal = gridObj.cellById(rowId,pvCol).getValue();
	    if(pvColVal == '-'){
	    	gridObj.cellById(rowId,pvCol).setValue(""); 
	    }
	});
	gridObj.toExcel('/phpapp/excel/generate.php');
	
}