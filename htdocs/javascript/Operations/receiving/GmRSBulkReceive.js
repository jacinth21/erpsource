 var dhrIdInd,donorIdInd,lotCodeInd, partInd, custSizeInd, sizeInd, expDateInd,	mfgDateInd, productTypeInd, cpcInd, apprRejInd,moveToInd;

//When click on Load button
function fnLoad(obj){
	if(event.keyCode == 13 || obj.value == 'Load'){//Need to work while clicking the button as well as pressing enter key
		fnValidateTxtFld('rsNumber',message_operations[373]);
		if (ErrorCount > 0)
		{
			Error_Show();
			Error_Clear();
			return false;
		}else{
			var parentform = document.getElementsByName('frmPOBulkReceive')[0];// To get parent form name
			var rsNum = document.getElementById("rsNumber").value;
			document.getElementById("rsNumber").value = rsNum.toUpperCase();
			document.getElementById("strOpt").value = 'loadRS';
			parentform.action = '/gmPOBulkReceive.do?method=loadBatchshipment&strMode=FETCHCONTAINER&companyInfo='+companyInfoObj+'&rsNumber='+rsNum.toUpperCase()+'&strOpt=loadRS';
			fnStartProgress('Y');
			parentform.submit();
		}
	}
}

function fnOnPageload(){
	//var parentform = document.getElementsByName('frmPOBulkReceive')[0];// To get parent form name
	document.getElementById("rsNumber").focus();
	/*Make Lot Code text box Empty on page load
	 * -Call fnOnScanLoad() to make it as empty and set focus on Textbox 
	 */
	if(document.getElementById("scanLotCode"))
		fnOnScanLoad();
	var strOpt = document.getElementById("strOpt").value;
	//var childform = document.getElementsByName('frmPOBulkReceive')[1];// To get parent form name
	var statusId = document.getElementById("statusId").value;
	var appRejVal;
	if (document.frmPOBulkReceive.strAllChecked) {
		allParameterCkd = document.frmPOBulkReceive.strAllChecked.value;
	}
	if(strOpt == 'loadRS'){
		
		gridObj = initGridData('dataGrid',objGridData);
		gridObj.attachEvent("onEditCell",doOnCellEdit);
		
	}
	
	if(validateObject(document.getElementById("recRS"))){
		document.getElementById("recRS").style.display ="table";
	}
	
	if(allParameterCkd == 'false'){// if all the parameters are not checked, then it should show the below message
		document.getElementById("errMsg").innerHTML = "<font color=red><b>"+message_operations[99]+"</b></font>";
	}
}

//Function will call on page load. It will append companyInfo in each tab href and initiate ajaxtab.
function fnAjaxTabLoad(){
	fnAjaxTabAppendCmpInfo("#maintab");
	fnMainTab();
}

//initiate ajaxtab
function fnMainTab(){
	var maintab=new ddajaxtabs("maintab", "ajaxdivcontentarea");				
	maintab.setpersist(false);								
	maintab.init();
	// To call the function on page load
	maintab.onajaxpageload=function(pageurl)
    {
		fnOnPageload();
		
    	if (pageurl.indexOf("gmPOBulkReceive.do?method=loadBatchshipment")!=-1){
    		fnAddCompanyParams();
    		if(document.getElementById("ajaxdivcontentarea") != undefined){
	  			document.getElementById("ajaxdivcontentarea").style.height = "492px";
			}
    	}
    	if(pageurl.indexOf("GmReceivingParamInc.do?")!=-1){	
			if(document.getElementById("#iframe") != undefined){
	  			document.getElementById("#iframe").style.height = "550px";
			}
		}
		if(pageurl.indexOf("gmOperDashBoardDispatch.do?method=NCMRDashForShipment")!=-1){
				if(document.getElementById("#iframe") != undefined){
	  			document.getElementById("#iframe").style.height = "550px";
			}
		}
		if(pageurl.indexOf("gmOperDashBoardDispatch.do?method=DHRDashForShipment")!=-1){
			if(document.getElementById("#iframe") != undefined){
  			document.getElementById("#iframe").style.height = "550px";
			}
		}
		if(pageurl.indexOf("gmPOBulkReceive.do?method=loadLog")!=-1){
			if(document.getElementById("ajaxdivcontentarea") != undefined){
	  			document.getElementById("ajaxdivcontentarea").style.height = "550px";
			}
		} 
		fnHtmlEditor();
	}	
}

//validate on changing the lot code values
function doOnCellEdit(stage, rowId, cellInd, nValue, oValue){
	var parentform = document.getElementsByName('frmPOBulkReceive')[0];// To get parent form name
	var childform = document.getElementsByName('frmPOBulkReceive')[1];// To get parent form name
	var statusId = parentform.statusId.value;
	var gridLotNum;
	var count = 0;
	var colId;
	var mfgCurrDiff, expCurrDiff, expMfgDiff;
	var mfgDate;
	var apprRejVal;
	var btnDisableFl = false;
	if(statusId == '1'){
		colId = '2';
	}else{
		colId = '3';
	}

	if(cellInd == colId && stage == 2){//once the value is selected
		if(oValue != nValue){
			gridObj.forEachRow(function(grrowid){
				gridLotNum = gridObj.cellById(grrowid,colId).getValue();
		 			if(nValue == gridLotNum && grrowid != rowId){
			 			count++;
			 		}
	 		});
		}
		// Function call for validating the Enterd Lot is Valid OR Not
		fnvalidateLot(nValue);		
		if(count > 0){
			Error_Details(message_operations[100]);
			Error_Show();
	        Error_Clear();
	        return false;
		}
		
	}
	
	//Expiry Date validation
	if(cellInd == '6' && stage == 2){
		mfgDate = gridObj.cellById(rowId,7).getValue();
		expMfgDiff= dateDiff(mfgDate, nValue, format);
		expCurrDiff= dateDiff(currdate, nValue, format);
		if(nValue == ''){
			Error_Details(message_operations[67]);
		}
		if(expMfgDiff < 0){
			Error_Details(message_operations[101]);
		}
		
		if(expCurrDiff < 0){
			Error_Details(message_operations[76]);
		}
		
		if (ErrorCount > 0)
		{
			Error_Show();
			gridObj.cellById(rowId,6).setValue(oValue);
			Error_Clear();
			return false;
		}
	}
	
	// Manufacturing date validation
	if(cellInd == '7' && stage == 2){
		mfgCurrDiff= dateDiff(nValue, currdate, format);
		if(nValue == ''){
			Error_Details(message_operations[70]);
		}
		if(mfgCurrDiff < 0){
			Error_Details(message_operations[75]);
		}
		
		if (ErrorCount > 0)
		{
			Error_Show();
			gridObj.cellById(rowId,7).setValue(oValue);
			Error_Clear();
			return false;
		}
	}
	var appcnt,rejcnt;
	// Disable submit button if the value is selected
	if(cellInd == '10' && stage == 2){
		
		if(nValue != '0'){
			appcnt = 0;
			rejcnt = 0;
			document.frmPOBulkReceive.Btn_RSSubmit.disabled = true;
			gridObj.forEachRow(function(rowId){
				apprRejVal = gridObj.cellById(rowId, 10).getValue();
				if(apprRejVal == '1400'){
					appcnt++;
				}else if(apprRejVal == '1401'){
					rejcnt++;
				}
			});
		}else{
			appcnt = 0;
			rejcnt = 0;
			gridObj.forEachRow(function(rowId){
				apprRejVal = gridObj.cellById(rowId, 10).getValue();
				if(apprRejVal != '0'){
					btnDisableFl = true;
				}
				if(apprRejVal == '1400'){
					appcnt++;
				}else if(apprRejVal == '1401'){
					rejcnt++;
				}
			});
			if(btnDisableFl){
				document.frmPOBulkReceive.Btn_RSSubmit.disabled = true;
			}else{
				document.frmPOBulkReceive.Btn_RSSubmit.disabled = false;
			}
		}
		// Need to display the Split check box only if user selected both approved and rejected
		if(document.getElementById("chkboxtr") != undefined && document.getElementById("chkboxtr")!= null){
			if(appcnt >0 && rejcnt >0){
				document.getElementById("chkboxtr").style.display="table-row";//To align proper in Edge browser PC-3658
			}else{
				document.getElementById("chkboxtr").style.display="none";
			}
		}
	}
	
	return true;
}

function initGridData(divRef,gridData){
	var optionstr;
	var parentform = document.getElementsByName('frmPOBulkReceive')[0];// To get parent form name
	var statusId = parentform.statusId.value;
	var txntypeid,txntype;
	
	gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
		
	if(statusId == '1' && varApprRejFl == '' && accessFl == 'Y'){// 1: Pending Inspected; once inspected, then no need to show the dropdown
		gObj._in_header_custom_drpdown=function(tag,index,data){   // this function is to add the dropdown in grid header
			optionstr = '<option value="0">'+message_operations[394]+'</option>';
	         optionstr = optionstr + "<option value=Approve>"+message_operations[395]+"</option>";
	         optionstr = optionstr + "<option value=Reject>"+message_operations[396]+"</option>";
			tag.innerHTML='<select name=ApprRej class=RightText tabindex="-1" onChange="fnPopulateValue(this);">' + optionstr +'</select>';
	     }
		gObj.attachHeader('#rspan,#rspan,#rspan,#rspan,#rspan,#rspan,#rspan,#rspan,#rspan,#rspan,#custom_drpdown');
	}
	if(statusId == '2' && accessFl == 'Y' && strRejectFl != 'Y'){//Pending Processing
		gObj._in_header_custom_drpdown=function(tag,index,data){   // this function is to add the Move to dropdown in grid header
			optionstr = '<option value="0">[Choose One]</option>';
			for (i=0;i< locationLen;i++)
		 	{
		 		arr = eval("TypeArr"+i);
		 		arrobj = arr.split(",");
		 		txntypeid = arrobj[0];
		 		txntype =  arrobj[1];
			
		        optionstr = optionstr + "<option value="+txntypeid+">"+txntype+"</option>";
		 	}
			tag.innerHTML='<select name=ApprRej class=RightText tabindex="-1" onChange="fnPopulateLocation(this);">' + optionstr +'</select>';
	    }
		gObj.attachHeader('#rspan,#rspan,#rspan,#rspan,#rspan,#rspan,#rspan,#rspan,#rspan,#rspan,#rspan,#rspan,#custom_drpdown');
	}
	gObj.init();
	gObj.loadXMLString(gridData);
	return gObj;	
}
 
// To populate the value in dropdown based on the value selected in header dropdown
function fnPopulateValue(obj){
	var value = obj.value;
	var childform = document.getElementsByName('frmPOBulkReceive')[1];// To get parent form name
	
	if(value == 'Approve'){
		gridObj.forEachRow(function(rowId){
			// set all the value to approve; 1400: Approve
			apprRej = gridObj.cellById(rowId, 10).setValue('1400');
			document.all.Btn_RSSubmit.disabled = true;
		});
	}else if(value == 'Reject'){
		gridObj.forEachRow(function(rowId){
			// set all the value to reject; 1401: Reject
			apprRej = gridObj.cellById(rowId, 10).setValue('1401');
			document.all.Btn_RSSubmit.disabled = true;
		});
	}else{
		gridObj.forEachRow(function(rowId){
			// reset the values to choose one
			apprRej = gridObj.cellById(rowId, 10).setValue('0');
			document.all.Btn_RSSubmit.disabled = false;
		});
	}
	if(document.getElementById("chkboxtr") != undefined && document.getElementById("chkboxtr")!= null){
		document.getElementById("chkboxtr").style.display="none";
	}

}

//To populate the value in dropdown based on the value selected in header dropdown
function fnPopulateLocation(obj){
	var value = obj.value;
	gridObj.forEachRow(function(rowId){
		// set all the value to approve; 1400: Approve
		apprRej = gridObj.cellById(rowId, 12).setValue(value);
	});
	
}

// when click on "Release for Process" button
function fnReleaseForProcess(form){
	//var parentform = document.getElementsByName('frmPOBulkReceive')[0];// To get parent form name
	//var childform = document.getElementsByName('frmPOBulkReceive')[1];
	var partNum, ctrlNum, apprRej, dhrId, custsize, expdate, manfdate;
	var apprString = ''; 
	var rejString = '';
	var inputString = '';
	var rejType = document.getElementById("rejType").value;
	var rejReason = document.getElementById("rejReason").value;
	var rsNum = document.getElementById("rsNumber").value;
	var apprRejFl = false;
	var rejFlag = false;
	var chkSplitRSObj = document.frmPOBulkReceive.chkSplitRS;
	
	if(document.frmPOBulkReceive.strAllChecked!=undefined) 
	{
		if (document.frmPOBulkReceive.strAllChecked)
		{
		allParameterCkd = document.frmPOBulkReceive.strAllChecked.value;
		}

		if(allParameterCkd == 'false')

			{// if all the parameters are not checked, then it should show the below message
				document.getElementById("errMsg").innerHTML = "<font color=red><b>"+message_operations[864]+"</b></font>";
			}
	}
	fnStartProgress();
	
	// Loop through the rows and get the string for selected rows 
	gridObj.forEachRow(function(rowId){
		
		// get the selected value from the grid
		dhrId	= gridObj.cellById(rowId, 1).getValue();
		ctrlNum = gridObj.cellById(rowId, 2).getValue();
		partNum = gridObj.cellById(rowId, 3).getValue(); 
		custsize = gridObj.cellById(rowId, 4).getValue();
		expdate = gridObj.cellById(rowId, 6).getValue();
		manfdate = gridObj.cellById(rowId, 7).getValue();
		apprRej = gridObj.cellById(rowId, 10).getValue();
		
		if(apprRej == '0'){ //0: Choose One
			apprRejFl = true;
		}
		if(apprRej == '1401'){ //1401: Reject
			rejFlag = true;
		}
		
		if(chkSplitRSObj != undefined && chkSplitRSObj != null && chkSplitRSObj.checked){
			 // Split the approve/reject string based on the check box
			if(apprRej == '1400'){
				apprString = apprString + partNum + '^'+ ctrlNum.toUpperCase() + '^' + custsize + '^' + expdate + '^' + manfdate + '^'+ dhrId +  '^'+ apprRej  + '^'+ '^'+ '1' +'|';
			}else if(apprRej = '1401'){
				rejString = rejString + partNum + '^'+ ctrlNum.toUpperCase() + '^' + custsize + '^' + expdate + '^' + manfdate + '^'+ dhrId +  '^'+ apprRej  + '^'+ '^'+ '1' +'|';
			}
		}else{
			inputString = inputString + partNum + '^'+ ctrlNum.toUpperCase() + '^' + custsize + '^' + expdate + '^' + manfdate + '^'+ dhrId +  '^'+ apprRej  + '^'+ '^'+ '1' +'|';
		}
	});
	
	if(apprRejFl){
		Error_Details(message_operations[102]);
	}
	if(rejFlag){
		if(rejType == '0')
			Error_Details(message_operations[103]);
		if(rejReason == '')
			Error_Details(message_operations[104]);
	}
	
	if (ErrorCount > 0)
	{
		Error_Show();
		fnStopProgress();
		Error_Clear();
		return false;
	}
	if(apprString == '' && rejString != ''){
		inputString = rejString;
	}else if(apprString != '' && rejString == ''){
		inputString = apprString;
	}
	
	 document.frmPOBulkReceive.happrString.value = apprString;
	 document.frmPOBulkReceive.hrejString.value = rejString;
	 document.frmPOBulkReceive.hInputString.value = inputString;
	 document.frmPOBulkReceive.action = '/gmPOBulkReceive.do?companyInfo='+companyInfoObj+'&method=saveBulkDHRInsp&rsNumber='+rsNum;		 
	 document.frmPOBulkReceive.submit();
}

// When click on Submit button
function fnSubmitBulkDHR(form){
	//var parentform = document.getElementsByName('frmPOBulkReceive')[0];// To get parent form name
	var partNum, ctrlNum, custsize, expdate, manfdate, dhrId;
	var inputString = '';
	var lotString   = '';
	var rsNum = document.getElementById("rsNumber").value;	
	// Loop through the rows and get the string for selected rows 
	gridObj.forEachRow(function(rowId){
		
		// get the selected value from the grid
		dhrId	= gridObj.cellById(rowId, 1).getValue();
		ctrlNum = gridObj.cellById(rowId, 2).getValue();
		partNum = gridObj.cellById(rowId, 3).getValue();
		custsize = gridObj.cellById(rowId, 4).getValue();
		expdate = gridObj.cellById(rowId, 6).getValue();
		manfdate = gridObj.cellById(rowId, 7).getValue();
				
		inputString = inputString + partNum + '^'+ ctrlNum.toUpperCase() + '^' + custsize + '^' + expdate + '^' + manfdate + '^'+ dhrId + '^'+ '^'+ '^'+ '1' +'|'; 
		lotString = lotString + ctrlNum.toUpperCase() + ',';
	});
    
	dhtmlxAjax.get('/gmPOBulkReceive.do?companyInfo='+companyInfoObj+'&strOpt=LOTNUMBERS&method=validateLotNumber&hLotString='+lotString+'&rsNumber='+rsNum+'&ramdomId=' + Math.random(),function(loader){
		var response = loader.xmlDoc.responseText;
		if(response != '' ){
			document.getElementById("errMsg").innerHTML = "<font color=red><B>"+message_operations[83]+"<BR>"+response+"</B></font>";
	        return false;
		}else{
			fnStartProgress();
			document.getElementById("haction").value = 'EDIT_BULK_DHR';
			document.frmPOBulkReceive.hInputString.value = inputString; 
			document.frmPOBulkReceive.action = '/gmPOBulkReceive.do?companyInfo='+companyInfoObj+'&method=saveBulkDHR&rsNumber='+rsNum+'&haction=EDIT_BULK_DHR';		 
		 	document.frmPOBulkReceive.submit();
		}
	});
	
}

//To assign the tab id to hidden field, to know which tag is currently open
function fnUpdateTabIndex(link) {	
	var linkObj = link.getElementsByTagName('a')[0];
	document.all.hcurrentTab.value = linkObj.id;	
}

// When click on "Release for Verification" button
function fnReleaseForVerification(form){
	//var parentform = document.getElementsByName('frmPOBulkReceive')[0];// To get parent form name
	//var childform = document.getElementsByName('frmPOBulkReceive')[1];
	var partNum, ctrlNum, moveTo, dhrId, custsize, expdate, manfdate, apprSts;
	var rsNum = document.getElementById("rsNumber").value;
	var inputString = ''; 
	var fgString = '';
	var qnString = '';
	var pnString = '';
	var moveToFl = false;
	var rejMoveToFl = false;
	var rejMoveTypeFl = false;
	var splitCnt = 0;
	
    fnStartProgress();
	
	// Loop through the rows and get the string for selected rows 
	gridObj.forEachRow(function(rowId){
		
		// get the selected value from the grid
		dhrId	= gridObj.cellById(rowId, 1).getValue();
		ctrlNum = gridObj.cellById(rowId, 3).getValue();
		partNum = gridObj.cellById(rowId, 4).getValue();
		custsize = gridObj.cellById(rowId, 5).getValue();
		expdate = gridObj.cellById(rowId, 7).getValue();
		manfdate = gridObj.cellById(rowId, 8).getValue();
		apprSts = gridObj.cellById(rowId, 11).getValue();
		moveTo = gridObj.cellById(rowId, 12).getValue();
		
		
		if(moveTo == '0' && apprSts == 'Approved'){// Move To should not be choose one
			moveToFl = true;
		}
		if(moveTo == '0' && apprSts == 'Rejected'){// Move To should not be choose one
			rejMoveToFl = true;
		}
		if(apprSts == 'Rejected' && (moveTo != '0' && moveTo != '104622')){  // Move to type NOT IN QUARANTINE
			rejMoveTypeFl = true;
		}
		inputString = inputString + partNum + '^'+ ctrlNum + '^' + custsize + '^' + expdate + '^' + manfdate + '^'+ dhrId +  '^'+ '^'+ moveTo + '^'+ '1' +'|';
		
		// Create sperate string based on the inventory to split the DHR
		if(moveTo == '104620'){// DHR to Finished Goods
			fgString = fgString + partNum + '^'+ ctrlNum + '^' + custsize + '^' + expdate + '^' + manfdate + '^'+ dhrId +  '^'+ '^'+ moveTo + '^'+ '1' +'|';
		}else if(moveTo == '104621'){// DHR to Repack
			pnString = pnString + partNum + '^'+ ctrlNum + '^' + custsize + '^' + expdate + '^' + manfdate + '^'+ dhrId +  '^'+ '^'+ moveTo + '^'+ '1' +'|';
		}else if(moveTo == '104622'){//Qty to Quarantine
			qnString = qnString + partNum + '^'+ ctrlNum + '^' + custsize + '^' + expdate + '^' + manfdate + '^'+ dhrId +  '^'+ '^'+ moveTo + '^'+ '1' +'|';
		}
		
	});
	
	// If only one string is available, no need to split the DHR, otherwise need to split
	if(fgString != ''){
		splitCnt++;
	}
	if(pnString != ''){
		splitCnt++;
	}
	if(qnString != ''){
		splitCnt++;
	}
	
	if(moveToFl || rejMoveToFl ){
		Error_Details(message_operations[105]);
	}

	if(rejMoveTypeFl){
		Error_Details(message_operations[106]);
	}
	if (ErrorCount > 0){
		Error_Show();
		fnStopProgress();
		Error_Clear();
		return false;
	}
	document.getElementById("hdhrProcessAction").value = 'P';
	document.frmPOBulkReceive.hInputString.value = inputString; 
	document.frmPOBulkReceive.hFGString.value = fgString;
	document.frmPOBulkReceive.hPNString.value = pnString;
	document.frmPOBulkReceive.hQNString.value = qnString;
	document.frmPOBulkReceive.hInvSplitCnt.value = splitCnt;
	document.frmPOBulkReceive.action = '/gmPOBulkReceive.do?companyInfo='+companyInfoObj+'&method=saveBulkDHRProcessVerify&rsNumber='+rsNum+'&hdhrProcessAction=P';		 
 	document.frmPOBulkReceive.submit();
}

// Click on Release Shipment button
function fnReleaseShipment(form){
	//var parentform = document.getElementsByName('frmPOBulkReceive')[0];// To get parent form name
	//var childform = document.getElementsByName('frmPOBulkReceive')[1];
	
	/* while submitting Call fnInitColumVar() to get the DHR,Lot,PartApprove/Reject,moveTo column values, Instead of hardcoding the column values
	 */
	fnInitColumVar();
	var partNum, ctrlNum, moveTo, dhrId,apprSts;
	var rsNum = document.getElementById("rsNumber").value;
	var inputString = ''; 
	
	fnStartProgress();
	// Loop through the rows and get the string for selected rows 
	gridObj.forEachRow(function(rowId){
	
	// get the selected value from the grid
		dhrId	= gridObj.cellById(rowId, dhrIdInd).getValue();
		ctrlNum = gridObj.cellById(rowId, lotCodeInd).getValue();
		partNum = gridObj.cellById(rowId, partInd).getValue();
		apprSts = gridObj.cellById(rowId, apprRejInd).getValue();
		moveTo = gridObj.cellById(rowId, moveToInd).getValue();
		
		inputString = inputString + partNum + '^'+ ctrlNum + '^' + '^' +  '^' + '^'+ dhrId +  '^'+  '^'+   '^'+ '1' +'|';
	});
	document.getElementById("hdhrProcessAction").value = 'V';
	document.frmPOBulkReceive.hInputString.value = inputString; 
	document.frmPOBulkReceive.action = '/gmPOBulkReceive.do?companyInfo='+companyInfoObj+'&method=saveBulkDHRVerifyRelease&rsNumber='+rsNum+'&hdhrProcessAction=V';		 
 	document.frmPOBulkReceive.submit();
}

//Function to get the column id for DHR,Lot,PartApprove/Reject,moveTo column while submitting
function fnInitColumVar(){
	dhrIdInd = gridObj.getColIndexById("dhrid");
	lotCodeInd = gridObj.getColIndexById("LotId");
	partInd = gridObj.getColIndexById("partNum");
	apprRejInd = gridObj.getColIndexById("apprRej");
	moveToInd = gridObj.getColIndexById("moveto");
}

// When click on inhouse transaction id hyperlink
function fnInhouseTxn(val,type,refid,conType){
	windowOpener("/gmItemControl.do?companyInfo="+companyInfoObj+"&haction=PicSlip&hConsignId="+val+"&txntype="+type+"&ruleSource="+type+"&refId="+refid,"SetPic","resizable=yes,scrollbars=yes,top=150,left=150,width=750,height=500");	
}
//The Below function will be called when we click print button in receive bulk shipment screen.
function fnPrint(){
	windowOpener("/gmPOBulkReceive.do?companyInfo="+companyInfoObj+"&method=printRecShipment&rsNumber="+rsnumber,"ReceivePrint","resizable=yes,scrollbars=yes,top=150,left=200,width=830,height=640");
}

function fnvalidateLot(lotNumber){
	lotNumber = lotNumber.toUpperCase();
	dhtmlxAjax.get('/gmPOBulkReceive.do?companyInfo='+companyInfoObj+'&strOpt=LOT&method=validateLotNumber&lotCode='+lotNumber+'&ramdomId=' + Math.random(),function(loader){
		var response = loader.xmlDoc.responseText;
		var responseArray = response.split("@");
		if(responseArray[0] > 0 ){
			document.getElementById("errMsg").innerHTML = "<font color=red>"+message_operations[397]+" <B>"+lotNumber+ "</B> "+message_operations[398]+" [<B>"+responseArray[1]+"</B>],"+ message_operations[399]+"</font>";			
	        return false;
		}
	});
}

// while Enter the lot code text box in Release shipment screen at verification status,need to call fnValidateLotCode() to validate the Lot.
function fnVerifyLotCode(obj){
	if(event.keyCode == 13){
		fnValidateLotCode(obj,gridObj);
	}
}
