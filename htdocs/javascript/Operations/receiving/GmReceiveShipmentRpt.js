
function fnOnload(){	
	if (objGridData != '') {
		gridObj = initGridData1('recshipRpt', objGridData);
    }
}
function initGridData1(divRef,gridData)
{
	var gObj = new dhtmlXGridObject(divRef);
		gObj.setSkin("dhx_skyblue");
		gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
		gObj.enableTooltips("true,true,true,true,true,true");
		gObj.init();
		gObj.loadXMLString(gridData);
	return gObj;	
}

function fnShowReceiveShip(val)
{
	location.href =  "/GmPageControllerServlet?strPgToLoad=/gmPOBulkReceive.do?companyInfo="+compInfObj+"&method=loadBatchshipment&strOpt=loadRS&strMode=FETCHCONTAINER&rsNumber="+val;
}

function fnLoad(){

	var objfromDate = document.frmReceiveShipRpt.fromDate;
	var objtoDate = document.frmReceiveShipRpt.toDate;
	var fromDtCurrDiff= dateDiff(currdate, objfromDate.value, format);
	var toDtCurrDiff= dateDiff(currdate, objtoDate.value, format);
		
	// Front-end validation added for the Entered Receive Shipment ID 
	var recShipId = TRIM(document.frmReceiveShipRpt.recShipID.value);
	if(objfromDate.value == "" && objtoDate.value == "" && recShipId == ""){
		Error_Details(message_operations[86]);
	}
	if(objfromDate.value != ""){ 
		CommonDateValidation(objfromDate, format, message_operations[87]+format);
	}
	if(objtoDate.value != ""){ 
		CommonDateValidation(objtoDate, format, message_operations[88]+format);
	}
	if(objfromDate.value != "" && objtoDate.value == ""){
		Error_Details(message_operations[89]);
	}
	if(objtoDate.value != "" && objfromDate.value == ""){
		Error_Details(message_operations[90]);
	}
	var dateDifference = dateDiff(objfromDate.value,objtoDate.value,format);
	if(dateDifference < 0){		
			Error_Details(message_operations[91]);			
	}
	
	if(!objfromDate.value == "" && fromDtCurrDiff > 0 ){
		Error_Details(message_operations[92]);
	}
	
	if(!objtoDate.value == "" && toDtCurrDiff < 0 ){
		Error_Details(message_operations[93]);
	}
	
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	if(recShipId !=''){
		document.frmReceiveShipRpt.recShipID.value = recShipId;	
	}
	document.frmReceiveShipRpt.haction.value = "load";
	fnStartProgress('Y');
	document.frmReceiveShipRpt.submit();

}
//This function used to down load the excel file
function fnDownloadXLS() {
	gridObj.toExcel('/phpapp/excel/generate.php');
}