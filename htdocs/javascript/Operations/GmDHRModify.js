function fnViewPrint()
{
windowOpener("/GmPOReceiveServlet?companyInfo="+companyInfoObj+"&hAction=ViewDHRPrint&hDHRId=" + gStrDHRId + "&hVenId=" + gStrVendorId,"DHR","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnUpdate()
{	
	obj = document.frmOrder.hAction;
	
	var qtyrec = parseInt(document.frmOrder.hRecQty.value);
	if (obj.value == 'C')
	{
		fnSubmit();
	}
	else
	{
		if (obj.value == 'I')
		{
			obj1 = document.frmOrder.Txt_QtyInsp.value;
			if (obj1 == '')
			{
				Error_Details(message[10]);
			}
			obj2 = document.frmOrder.Cbo_InspEmp.value;
			if (obj2 == '0')
			{
				Error_Details(message[11]);
			}
			obj3 = document.frmOrder.Txt_QtyRej.value;
			if (obj3 == '')
			{
				Error_Details(message[12]);
			}
			if (obj1 > qtyrec)
			{
				Error_Details(message[17]);
			}
			if (obj3 > qtyrec)
			{
				Error_Details(message[18]);
			}		
		}
		else if (obj.value == 'P')
		{
			var skip = document.frmOrder.Chk_Skip.checked;
			if (!skip)
			{
				var qtyrej = parseInt(document.frmOrder.hRejQty.value);
				var qtyclose = parseInt(document.frmOrder.hCloseQty.value);
				obj1 = document.frmOrder.Txt_Qty.value;
				if (obj1 == '')
				{
					Error_Details(message[13]);
				}
				obj2 = document.frmOrder.Cbo_Emp.value;
				if (obj2 == '0')
				{
					Error_Details(message[14]);
				}
				if (obj1 > qtyrec)
				{
					Error_Details(message[19]);
				}
				if (obj1 > qtyrec-qtyrej+qtyclose)
				{
					Error_Details(message[20]);
				}
			}
		}
		else if (obj.value == 'V')
		{
			var qtyrej = parseInt(document.frmOrder.hRejQty.value);
			var qtyclose = parseInt(document.frmOrder.hCloseQty.value);
			obj1 = document.frmOrder.Txt_Qty.value;
			if (obj1 == '')
			{
				Error_Details(message[15]);
			}
			obj2 = document.frmOrder.Cbo_Emp.value;
			if (obj2 == '0')
			{
				Error_Details(message[16]);
			}
			if (obj1 > qtyrec)
			{
				Error_Details(message[21]);
			}
			if (obj1 > qtyrec-qtyrej+qtyclose)
			{
				Error_Details(message[22]);
			}
			if (obj1 < qtyrec-qtyrej+qtyclose)
			{
				Error_Details(message[23]);
			}
		}

        fnValidateExpDate();

		if (ErrorCount > 0)
		{
				Error_Show();
				Error_Clear();
				return false;
		}
		document.frmOrder.hAction.value = gStrAction;
		fnStartProgress('Y');
	  	document.frmOrder.submit();
	 }
}

function fnViewPrintWO()
{
windowOpener("/GmWOServlet?companyInfo="+companyInfoObj+"&hAction=PrintWO&hWOId=" + gStrWOId + "&hDocRev=" + gStrDocRev + "&hDocFl=" + gStrDocActiveFl,"WO1","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=640");
}

function fnViewPrintNCMR()
{
windowOpener("/GmNCMRServlet?companyInfo="+companyInfoObj+"&hAction=PrintNCMR&hId=" + gStrNCMRId,"NCMR","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=640");
}

function fnCalDate(val)
{
	var year = '';
	var month = '';
	var NormalYr = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
	var LeapYr = new Array(31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
	var cal = 0;
	var rem = 0;
	var cal1 = 0;
	var date = '';
	var num = val.substring(3,6);
	var days = '';
	//Hardcoded till Year 2008
	if (val.charAt(2)=='D') // For 2003
	{
		year = '2003';
		days = NormalYr;
	}
	else if (val.charAt(2)=='E') // For 2004
	{
		year = '2004';
		days = LeapYr;
	}
	else if (val.charAt(2)=='F') // For 2005
	{
		year = '2005';
		days = NormalYr;
	}
	else if (val.charAt(2)=='G') // For 2006
	{
		year = '2006';
		days = NormalYr;
	}
	else if (val.charAt(2)=='H') // For 2007
	{
		year = '2007';
		days = NormalYr;
	}
	else if (val.charAt(2)=='J') // For 2008
	{
		year = '2008';
		days = LeapYr;
	}
	else if (val.charAt(2)=='K') // For 2009
	{
		year = '2009';
		days = NormalYr;
	}
	else if (val.charAt(2)=='L') // For 2010
	{
		year = '2010';
		days = NormalYr;
	}
	for (var i=0;i<days.length;i++ )
	{
		cal = cal + days[i];
		if (cal>=num)
		{
			cal1=cal-days[i];
			rem = num-cal1;
			if (rem<10)
			{
				rem = "0"+rem;
			}
			month = i+1;
			if (month<10)
			{
				month="0"+month;
			}
			date = month+"/"+rem+"/"+year;
			break;
		}
	}
	return date;
}

function fnUpdate()
{
	document.frmOrder.Txt_QtyRec.disabled = false;
	
	fnValidateTxtFld('Txt_CNum',message_operations[400]);
	fnValidateTxtFld('Txt_QtyRec',message_operations[401]);
	vPnum = document.frmOrder.hPartNum.value;
	vCntNum = document.frmOrder.Txt_CNum.value;
	vQtyRec = document.frmOrder.Txt_QtyRec.value;
	var PoType = gStrWOType;
    var vSkipFl = gStrSkipFl;  
    var vCnumFmt = gStrCnumFmt;
    var qtyrec = parseInt(document.frmOrder.hRecQty.value);
    var qtyreject = parseInt(document.frmOrder.hRejQty.value);
    var errRecQty='';
    if(PoType == '3101' && qtyrec < vQtyRec)
        {
        	errRecQty = vPnum+",";
        }
    if(errRecQty != '')
    {
		Error_Details("<b>Qty to Rec</b> cannot be more than <b>Qty Pend</b> on a <b>Rework PO</b> for the following Part #s:"+ errRecQty.substring(0,errRecQty.length-1));
	}
    //Compating the previous quantity and current quantity to disable/enable the Regenerate lot#
    if(qtyrec != vQtyRec){
    	if(qtyrec = null || qtyrec ==''){// if qtyrec is empty(first time loading the screen) need to set current qty as previous qty
    		document.frmOrder.hPrevQty.value = vQtyRec;
    	}else{
    		document.frmOrder.hPrevQty.value = qtyrec;
    	}
    }
    
    if(vQtyRec < qtyreject)
    {
    	Error_Details(message_operations[121]);
    	
    	}

    if (isNaN(vQtyRec))
    {   	
    	Error_Details(message[85]);   	
    }
    
    if (fnValidateControl(vPnum,vCntNum,vSkipFl,vCnumFmt))
	{
		if ( vCntNum == 'NOC#' || PoType == '3103' )
		{ 
				document.frmOrder.hManfDt.value = gStrDate;
		}
		else
		{
				document.frmOrder.hManfDt.value = fnCalDate(vCntNum);
		}
	}
   
    fnValidateExpDate();

	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	
	//alert("STRING: "+document.frmOrder.hManfDt.value);
	document.frmOrder.hAction.value = "UpdateRec";
	document.frmOrder.submit();
}

function fnValidateExpDate() {
	var vExpDate = '';
	val = eval("document.frmOrder.Txt_ExpDate");
    vExpDate = val.value == null ? "" : val.value.trim();

	// Validate Expiry date for Sterile Parts
	if (document.getElementById('Txt_ExpDate').disabled != true) {
		// Sterile Parts Require an expiration date
		fnValidateTxtFld('Txt_ExpDate',message_operations[402]);
		
		// Validate Date in session format
		var array = [vExpDate.trim(),format];
		CommonDateValidation(document.getElementById('Txt_ExpDate'),format,Error_Details_Trans(message_operations[122],array));

        // Expiry Date cannot be a past date (Today or Future Date)
		var currDate = document.frmOrder.hTodaysDate.value;
		if(dateDiff(currDate,document.getElementById('Txt_ExpDate').value,format)<0){
			Error_Details(message_operations[123]);
		}
    }
}

function fnValidateControl(vPnum,lot,skipFl,vCntNumFmt)
{
	if(prodType == '100845'){//No need to give control number for Tissue part
		return;
	}
	var minLen = 0;
	if (vCntNumFmt == 'XXXXXX%' || vCntNumFmt == 'XXXXXX') {
    	minLen = 6;
    }
	
	LotCode = gStrLotCode;
	var PoType = gStrWOType;
	lot = TRIM(lot.toUpperCase());
	len = lot.length; 
	var regexp = /\s/g;
	var prodclass = document.frmOrder.hProdclass.value;
	
	if(lot == "NOC#"){
		if(PoType == '3101' && prodclass == '4031' ){
			return true;
		}else{
			Error_Details(Error_Details_Trans(message_operations[124],vPnum));	
		}
      }
	if(regexp.test(lot)){
		Error_Details(message_operations[125]);
		return true;
	}
	if ( PoType == '3103' ||  PoType == '3105' || skipFl == "Y")
	{
		return true;
	}
	else if (len == 8 && vCntNumFmt == '')
	{	 
		var lcode = lot.substring(0,2);
		if( PoType != '3101')
		{
		if (lcode != LotCode)
		{	 
			Error_Details(message[79]);
		}
		 }

		var yr = lot.substring(2,3);
		if (!isAlpha(yr))
		{
			Error_Details(message[80]);
		}

		var date = parseInt(lot.substring(3,6));
		
		/* Commenting since branch parts receiving lot from 0 to 999
		if(fnJulianLotValidation(date,julianRule)){
			Error_Details(message[81]);  // Entered Incorrect Date Range or not Numeric
		}*/
		var size = lot.substring(6,7);
		if (!isAlpha(size) && size != '#')
		{
			Error_Details(message[82]);
		}

		var rev = lot.substring(7,8);
		if (!isAlpha(rev) && rev != '#')
		{
			Error_Details(message[83]);
		}
	}else if(len != 8 && vCntNumFmt == ''){
		Error_Details(message[84]);
	}
	else if (len >= minLen && len <= 20&& vCntNumFmt != ''){
					return true;
	}else{
					Error_Details(Error_Details_Trans(message_operations[126],vPnum));
	}

	if (ErrorCount == 0)
	{
		return true;
	}
	fnAddUDIDetails();
	 
}

function isAlpha(elm) {
    if (elm == "") {
        return false;
    }
    for (var i = 0; i < elm.length; i++) {
        if ((elm.charAt(i) < "a" || elm.charAt(i) > "z") &&
           (elm.charAt(i) < "A" || elm.charAt(i) > "Z")) {
            return false; 
        }
    }
    return true;
}

//To open the popup when click on "D" icon
function fnCallDonor(){
	windowOpener("/gmDonorInfo.do?companyInfo="+companyInfoObj+"&method=fetchDonorInfo&strOpt=load&hDhrId="+gStrDHRId+"&hMode=Edit&hVendorId="+gStrVendorId,"PrntInv","resizable=yes,scrollbars=yes,top=200,left=300,width=585,height=700");
}

// Function to enable/disable the Regenerate Lot# button based on the whether the qty is changed or not
function fnOnPageLoad(){
	var frm = document.frmOrder;
	
	var vQtyRec = frm.Txt_QtyRec.value;
    var qtyrec = parseInt(frm.hRecQty.value);
    var hPrevQty = frm.hPrevQty.value;
	if(hPrevQty != '' && hPrevQty != vQtyRec){
		qtyflag = true; //if the previous qty is different from current qty, need to disable the button
	}
	
	if(!qtyflag && (frm.Btn_Regenerate)){
		frm.Btn_Regenerate.disabled = true;
	}else if(frm.Btn_Regenerate){
		frm.Btn_Regenerate.disabled = false;
	}
}
//function to update udi values while changing exp date and control number
function fnAddUDIDetails(){
	
		var ud = document.all.hUDINum.value.trim();
		var udlength = ud.length;
        valObj = (validateObject(eval("document.all.Lbl_UDI")))?eval("document.all.Lbl_UDI"):'';//validating the object
        var part_cnt = (validateObject(eval("document.all.hPnumRow")))?eval("document.all.hPnumRow").value:'';
        var udiVal = (validateObject(eval("document.all.hUDINum")))?eval("document.all.hUDINum").value:'';       
        var cnvlaue = (validateObject(eval("document.all.Txt_CNum")))?eval("document.all.Txt_CNum").value:'';        
        var manfDate = '';
        var DonorNumvalue = '';
        var expDate = (validateObject(eval("document.all.Txt_ExpDate")))?eval("document.all.Txt_ExpDate").value:'';
        var udiFormat = (validateObject(eval("document.frmOrder.hUDIFormat")))?eval("document.frmOrder.hUDIFormat").value:''; 
       var diValue = (validateObject(eval("document.all.hDINum")))?eval("document.all.hDINum").value:'';
        var checkVal = document.all.Chk_VDfl.checked;
        if(checkVal == true){
        	var diVal =diValue; 
        }
        else{
        if(udlength == 14){
        	var diVal = ud.substring(0, 14);
        }
        else if(udlength >= 28){
        	var diVal = ud.substring(4, 18);
        }else if(udlength >= 40){
        	var diVal = ud.substring(4, 18);
        }
        else{
        	var diVal = ud;
        }
        }
        
        if(udiVal != ''){
            
            if(udiFormat != '')
            {
                eval("document.frmOrder.Txt_UDI").value = fnUDINumber(udiFormat, diVal, cnvlaue, expDate, DonorNumvalue, manfDate);
             }
             else
             {
                eval("document.frmOrder.Txt_UDI").value = udiVal;
             }
            
        }            
}
//function to change updated udi number while clicking regenerate check box
function fnCheck(check) {
	var ud = document.all.hUDINum.value.trim();
	var udlength = ud.length;
	var DI = document.frmOrder.hDINum.value;
	var di = document.getElementById("udi").value;
	var dilength = di.length;
	
	if(udlength == 14){
    	var diVal = ud.substring(0, 14);
    }
    else if(udlength == 28){
    	var diVal = ud.substring(4, 18);
    }else if(udlength >= 40){
    	var diVal = ud.substring(4, 18);
    }
	
	if(check.checked==true) {
	if(dilength >= 29){
		di = di.substring(0, 4) + DI + di.substring(18,dilength);
		document.frmOrder.udi.value = di;
		}
		else if(dilength >= 40){
			di = di.substring(0, 4) + DI + di.substring(18,dilength);
			document.frmOrder.udi.value = di;
		}
		else{
		document.frmOrder.udi.value = DI;
		}
	}
}

var str = '';	 
var strMMYYExpDT = '';
var strYYMMDDExpDT = ''; 
var yearYYYYExpDT='';
var yearYYExpDT='';
var yearYYYExpDT='';
var monthExpDT='';    
var dayExpDT='';    
var strExpDT = '';
var strManfDT = '';
var strYYYJJJExpDt = '';
var strYYYJJJManufDT = '';

var yearYYManfDT='';
var yearYYYManfDT ='';
var monthManfDT='';    
var dayManfDT='';

var strMMDDYYExpDT = ''; 
var strYYJJJExpDT = ''; 
var strYYYJJJExpDT = '';
var jilianJJJExpDT = '';
var jiliandayExpDT = '';
var jilianJJJManfDT = '';
var jiliandayManfDT = '';


//Function to convert the UDI based on UDI fornmat.  
function fnUDINumber(format, diVal, cnvlaue, expDate, DonorNumvalue, manfDate) { 	

	  strMMYYExpDT = '';
	  strYYMMDDExpDT = ''; 
	  yearYYYYExpDT='';
	  yearYYExpDT='';
	  yearYYYExpDT = '';
	  monthExpDT='';    
	  dayExpDT='';    
	  strExpDT = '';
	  strManfDT = '';

	  yearYYManfDT='';
	  yearYYYManfDT='';
	  monthManfDT='';    
	  dayManfDT='';

	  strMMDDYYExpDT = ''; 
	  strYYJJJExpDT = ''; 
	  strYYYJJJExpDT = '';
	  strYYYJJJManufDT = '';
	  jilianJJJExpDT = '';
	  jiliandayExpDT = '';
	  jilianJJJManfDT = '';
	  jiliandayManfDT = '';
	  str = '';
	  var dateObjec = new Date();
	  var currentHour = dateObjec.getHours();
	fnExpdate(expDate);     

	fnManfdate(manfDate);  
	
	// if expiry date is not empty , convert it to Julian date format YYJJJ
	if (expDate != '')
	{
	   jilianJJJExpDT = ConvertToJulian(parseInt(yearYYYYExpDT), parseInt(monthExpDT), parseInt(dayExpDT)) ;	 
	 
	   jiliandayExpDT  = jilianJJJExpDT.toString().substr(4,3); 
	
	   strYYJJJExpDT = yearYYExpDT + jiliandayExpDT; 
	   strYYYJJJExpDT = yearYYYExpDT + jiliandayExpDT;
	}
   // if Manf. date is not empty, convert it to Julian date format
	if(manfDate !=''){
		jilianJJJManfDT = ConvertToJulian(parseInt(yearYYYYmanfDT), parseInt(monthManfDT), parseInt(dayManfDT)) ;
		
		jiliandayManfDT  = jilianJJJManfDT.toString().substr(4,3); 
		strYYYJJJManufDT = yearYYYManfDT + jiliandayManfDT;
	}
	//always replaceAll '|' and '^' symbol as empty string, and DI - 103940 as actual DI value and Donor numner with actual DOnor num value
	//for GS 1 identifier
	//103940	DI			(01)
	//103941	MFG date	(11)
	//103942	EXP date	(17)
	//103943	LOT	(10)
	//103944	Serial #	(21)
	//103945	Donor Id	
	//4000688	Supplemental serial #


	format =  replaceAll(format,'|', '');
	format = replaceAll(format,'^', '');
	format = replaceAll(format,'103940', diVal);
	format = replaceAll(format,'(21)103944', '');
	format = replaceAll(format,'=,103944', '');
	format = replaceAll(format,'/S4000688', '');
	if(DonorNumvalue !=''){
		str = replaceAll(format,'103945', DonorNumvalue);
	}else{
		str = replaceAll(format,'=103945', '');
	}
	
	 
	// if expiry date is not empty, replaceAll it with actual value by format, otherwise replaceAll as empty string with prefix.
	if (strYYMMDDExpDT != '')
	{ 	
		// exp date with contorl #
		str = replaceAll(str,'103942[YYMMDD]103943', strYYMMDDExpDT +cnvlaue); 
		str = replaceAll(str,'103942[MMYY]103943', strMMYYExpDT +cnvlaue); 
		str = replaceAll(str,'103942[MMDDYY]103943', strMMDDYYExpDT +cnvlaue);
		str = replaceAll(str,'103942[YYMMDDHH]103943', strYYMMDDExpDT+currentHour +cnvlaue); 
		str = replaceAll(str,'103942[YYJJJ]103943', strYYJJJExpDT +cnvlaue); 
		str = replaceAll(str,'103942[YYJJJHH]103943', strYYJJJExpDT+currentHour +cnvlaue);
		// Exp date with Serial #
		str = replaceAll(str,'103942[YYMMDD]103944', strYYMMDDExpDT); 
		str = replaceAll(str,'103942[MMYY]103944', strMMYYExpDT); 
		str = replaceAll(str,'103942[MMDDYY]103944', strMMDDYYExpDT);
		str = replaceAll(str,'103942[YYMMDDHH]103944', strYYMMDDExpDT+currentHour); 
		str = replaceAll(str,'103942[YYJJJ]103944', strYYJJJExpDT); 
		str = replaceAll(str,'103942[YYJJJHH]103944', strYYJJJExpDT+currentHour);
		// Only Exp date
		str = replaceAll(str,'103942[YYMMDD]', strYYMMDDExpDT);
		str = replaceAll(str,'103942[YYYJJJ]', strYYYJJJExpDT);
	}	
	else 
	{
		  str = replaceAll(str,'(17)103942[YYMMDD]', ''); 
		  str = replaceAll(str,'/$$103942[MMYY]', '/$$'); 
		  str = replaceAll(str,'/$$2103942[MMDDYY]', '/$$2'); 
		  str = replaceAll(str,'/$$3103942[YYMMDD]', '/$$3'); 
		  str = replaceAll(str,'/$$4103942[YYMMDDHH]', '/$$4'); 
		  str = replaceAll(str,'/$$5103942[YYJJJ]', '/$$5'); 
		  str = replaceAll(str,'/$$6103942[YYJJJHH]', '/$$6'); 
		  str = replaceAll(str,'/$$+103942[MMYY]', ''); 
		  str = replaceAll(str,'/$$+2103942[MMDDYY]', ''); 
		  str = replaceAll(str,'/$$+3103942[YYMMDD]', ''); 
		  str = replaceAll(str,'/$$+4103942[YYMMDDHH]', ''); 
		  str = replaceAll(str,'/$$+5103942[YYJJJ]', ''); 
		  str = replaceAll(str,'/$$+6103942[YYJJJHH]', '');
		  str = replaceAll(str,'=>103942[YYYJJJ]', '');
	}
	
	// if control num is not empty , replaceAll 103943 with actual control num value, otherwise replaceAll (10)103943 with empty string 
	if (cnvlaue != '')
	{
	   str = replaceAll(str,'103943', cnvlaue);
	   }
	else
	{
	   str = replaceAll(str,'(10)103943', '');
	   str = replaceAll(str,'/$103943', '');
	   str = replaceAll(str,'103943', '');
	   }
	//// if manufacturing date is not empty, replaceAll it with actual value by format, otherwise replaceAll as empty string with prefix.
	if (strYYMMDDManfDT != '')
	{ 	str = replaceAll(str,'103941[YYMMDD]', strYYMMDDManfDT); 
		str = replaceAll(str,'103941[YYYYMMDD]', strYYYYMMDDManfDT);
		str = replaceAll(str,'103941[YYYJJJ]', strYYYJJJManufDT);
	}	
	else 
	{
		str = replaceAll(str,'(11)103941[YYMMDD]', ''); 
		str = replaceAll(str,'/16D103941[YYYYMMDD]', '');
		str = replaceAll(str,'=}103941[YYYJJJ]', '');
	}

	str = replaceAll(str,'/$+103944', '');
	str = replaceAll(str,'S103944', '');
	str = replaceAll(str,'103944', '');
	
	return str;
}

//function will replace oldstring with newstring
function replaceAll(OldString,FindString,ReplaceString) 
{
	var SearchIndex = 0;
 	var NewString = ""; 
 	while (OldString.indexOf(FindString,SearchIndex) != -1)    
	{
   	NewString += OldString.substring(SearchIndex,OldString.indexOf(FindString,SearchIndex));
   	NewString += ReplaceString;
   	SearchIndex = (OldString.indexOf(FindString,SearchIndex) + FindString.length);         
	}
 	NewString += OldString.substring(SearchIndex,OldString.length);
 	return NewString;
}


//this function will convert calendar date to Julian date
function ConvertToJulian(Y,Mo,D ) {
	var a=Math.floor((14-Mo)/12);
	var y=Y+4800-a;
	var m=Mo+(12*a)-3;
	var julian ='';

	julian = D + Math.floor((153*m+2)/5) + (365*y) + Math.floor(y/4) - Math.floor(y/100) + Math.floor(y/400) - 32045   ;

	return Math.round(julian);
	
}


//this function used to split the expiry date with different format for year , month and day
function fnExpdate(expDate)
{
	for(i=0;i<expDate.length;i++){
	   var char = expDate.charAt(i);	
	   if(!isNaN(char))
	   strExpDT = strExpDT+char;	
	  }
	yearYYExpDT =  strExpDT.substr(6,2);
	yearYYYExpDT = strExpDT.substr(5,3);
	monthExpDT = strExpDT.substr(0,2);
	dayExpDT = strExpDT.substr(2,2);

	yearYYYYExpDT = strExpDT.substr(4,4); 
	strMMYYExpDT	= monthExpDT + yearYYExpDT ;
	strMMDDYYExpDT = monthExpDT + dayExpDT + yearYYExpDT; 
	strYYMMDDExpDT  = yearYYExpDT + monthExpDT + dayExpDT; 
	strYYYJJJExpDt = yearYYYExpDT; 
}

//this function used to split the manufacturing date with different format for year , month and day
function fnManfdate(manfDate)
{
	if(manfDate != undefined) {
		for(i=0;i<manfDate.length;i++){
			   var char = manfDate.charAt(i);	
			   if(!isNaN(char))
			   strManfDT = strManfDT+char;	
			  }	
	}
	 

	yearYYManfDT =  strManfDT.substr(6,2);	 
	yearYYYManfDT =  strManfDT.substr(5,3);
	monthManfDT = strManfDT.substr(0,2);
	dayManfDT=  strManfDT.substr(2,2);
	yearYYYYmanfDT = strManfDT.substr(4,4); 
	strYYMMDDManfDT  = yearYYManfDT  + monthManfDT  + dayManfDT;  
	strYYYYMMDDManfDT  = yearYYYYmanfDT  + monthManfDT  + dayManfDT;
	strYYYManfDT = yearYYYManfDT;
}