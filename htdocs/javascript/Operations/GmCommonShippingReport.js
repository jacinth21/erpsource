
// GmGird Methods & Functions.

var split_At = '';
var w1,dhxWins;

function setDefalutGridProperties(divRef)
{
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableMultiline(true);
	return gObj;
}

function initializeDefaultGrid(gObj, gridData)
{	
	gObj.init();
	gObj.loadXMLString(gridData);
	return gObj;
}

function initGrid(divRef,gridData){
	var gObj = setDefalutGridProperties(divRef);
	return initializeDefaultGrid(gObj,gridData);
}

// End GmGrid code.