//Get capaid and Ajaxcall to fetch the failed status data

function fnOnPageLoad() {
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmTTPVendorPOPopUp.do?method=fetchVendorPOFailure&capaDtlsId='+capaDtlsId+'&ramdomId='+Math.random());
	var loader = dhtmlxAjax.getSync(ajaxUrl);
	fnLoadCallBack(loader);	
}
//Get the value from loader
function fnLoadCallBack(loader){
	var vendorName = '';
	var loadDate = '';
	var errorDetails = '';
	var response = loader.xmlDoc.responseText;
	if(response != ""){
		var rows = [], i=0;
		var strErrorDtlsJson =  '['+response+']';
		var JSON_Array_obj = JSON.parse(strErrorDtlsJson);
		
		$.each(JSON_Array_obj, function(index,jsonObject){
			vendorName = jsonObject.vendorname;
			loadDate = jsonObject.loaddate;
			errorDetails = jsonObject.errordetails;			
		});
		setPoPopUpFailureHeaderLabel(vendorName,loadDate,errorDetails);
		
	}
}
//Split by ^ symbol and display the error details
function setPoPopUpFailureHeaderLabel(vendorName,loadDate,errorDetails){
	 
	 var errorDetailsSplit = errorDetails.replace("^", "</br>");
	
	if (vendorName!='' || vendorName == undefined){
	var obj = eval("document.all.lbl_vendor_nm");
	obj.innerHTML = vendorName;
	}
	
	if (loadDate!='' || loadDate == undefined){
	var obj = eval("document.all.lbl_load_date");
	obj.innerHTML = loadDate;
	}
	
	if (errorDetailsSplit!='' || errorDetailsSplit == undefined){
	var obj = eval("document.all.lbl_Failure_Reason");
	obj.innerHTML = errorDetailsSplit;
	}
}

//This Function used close the popup window
function fnClose() {
	window.close();
}