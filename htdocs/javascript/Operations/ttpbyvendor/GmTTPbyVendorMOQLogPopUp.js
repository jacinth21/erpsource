//this function used to load the page and fetch the moq log history details
//this function calling from GmTTPbyVendorMOQLogPopUp.jsp (window opener on page load)
function fnOnPageLoad(){
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmTTPbyVendorMoq.do?method=fetchTTPbyVendorMOQLog&vendorMOQId='
			+ vendorMOQId
			+ '&ramdomId='
			+ Math.random());
	var loader = dhtmlxAjax.getSync(ajaxUrl);
	fnHistoryLogResponse(loader);

}
//this function used to get the response and generate the grid data
function fnHistoryLogResponse(loader){
	response = loader.xmlDoc.responseText;
	if(response !=''){
	var setHeader = [ "Part Number","MOQ", "Updated By", "Updated Date"];
	var setColIds = "partnum,moq,updatedby,updateddate";

	var setColTypes = "ro,ron,ro,ro";
	var setColAlign = "left,right,left,left";
	var setColSorting = "str,int,str,str";
	var enableTooltips = "true,true,true,true";
	var setFilter = ["#text_filter", "#numeric_filter", "#select_filter","#select_filter" ];
	var setInitWidths = "150,100,180,250";

	var gridHeight = "";
	var dataHost = {};
	
	document.getElementById("DivNothingMessage").style.display = 'none';
	document.getElementById("dataGridDiv").style.display = 'block';
	document.getElementById("DivClose").style.display = 'block';
	// split the functions to avoid multiple parameters passing in
	// single function
	gridObj = initGridObject('dataGridDiv');
	
	gridObj = fnCustomTypes(gridObj, setHeader, setColIds, setInitWidths,
			setColAlign, setColTypes, setColSorting, enableTooltips,
			setFilter, 500);

	gridObj = loadDhtmlxGridByJsonData(gridObj, response);
}else{
    document.getElementById("dataGridDiv").style.display = 'none';
	document.getElementById("DivNothingMessage").style.display = 'block';
	document.getElementById("DivClose").style.display = 'none';
}
}
//This function used to close the pop up window
function fnClose(){
	window.close();
}
//This Function Used to set custom types conditions in Grid
function fnCustomTypes(gObj, setHeader, setColIds, setInitWidths, setColAlign,
		setColTypes, setColSorting, enableTooltips, setFilter, gridHeight) {
	if (setHeader != undefined && setHeader.length > 0) {
		gObj.setHeader(setHeader);
		var colCount = setHeader.length;
	}
	if (setInitWidths != "")
		gObj.setInitWidths(setInitWidths);
	if (setColAlign != "")
		gObj.setColAlign(setColAlign);
	if (setColTypes != "")
		gObj.setColTypes(setColTypes);
	if (setColSorting != "")
		gObj.setColSorting(setColSorting);
	if (enableTooltips != "")
		gObj.enableTooltips(enableTooltips);
	if (setFilter != "")
		gObj.attachHeader(setFilter);
	if (setColIds != "") {
		gObj.setColumnIds(setColIds);
	}
	if (gridHeight != "")
		gObj.enableAutoHeight(true, gridHeight, true);
	else
		gObj.enableAutoHeight(true, 400, true);

	return gObj;
}