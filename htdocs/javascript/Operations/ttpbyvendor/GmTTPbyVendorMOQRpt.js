var response = '';
var loader;
var moq_row_id='';
var historyfl_row_id='';
var historyfl_value ='';
var moq_id='';
// This function used to load the vendorMOQ Details
function fnLoad() {
	var vendorId = document.frmVendorMoq.vendorId.value;
	var divisionId = document.frmVendorMoq.divisionId.value;
	var partNo = document.frmVendorMoq.partNum.value;
	var partNumSuffix = document.frmVendorMoq.pnumSuffix.value;
	var projectId = document.frmVendorMoq.projectId.value;
    var purchaseAgent = document.frmVendorMoq.purchaseAgent.value;
	
	fnStartProgress();
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmTTPbyVendorMoq.do?method=fetchTTPbyVendorMOQReport&vendorId='
			+ vendorId
			+ '&divisionId='
			+ divisionId
			+ '&partNum='
			+ partNo
			+ '&projectId='
			+ projectId
			+ '&pnumSuffix='
			+ partNumSuffix
			+'&purchaseAgent='
			+ purchaseAgent
			+ '&ramdomId=' + Math.random());

	loader = dhtmlxAjax.getSync(ajaxUrl);
	fnLoadCallBack(loader);
	

}
// this function used to get the response. if response is not equal to empty
// generate the grid
function fnLoadCallBack(loader) {

	response = loader.xmlDoc.responseText;
	fnStopProgress();

	if (response != '') {
		var setInitWidths = "100,100,200,205,75,75,115,40,125,130,5";
		var setColAlign = "left,left,left,left,left,right,left,center,left,left";
		var setColTypes = "ro,ro,ro,ro,ro,ron,ro,historyFl,ro,ro,ro";
		var setColSorting = "str,str,str,str,str,int,str,str,str,str";
		var setHeader = [ "Vendor Name", "Part Number", "Part Description",
				"Project name", "Division", "MOQ", "Purchase Agent","", "Updated By",
				"Updated Date","" ];
		var setFilter = [ "#select_filter", "#text_filter", "#text_filter",
				"#text_filter", "#select_filter", "#numeric_filter", "#select_filter","",
				"#select_filter", "#select_filter" ];
		var setColIds = "vendornm,partnum,partdesc,projectname,divisionnm,moq,purchaseagentname,historyfl,updatedby,updateddate,moqid";
		var enableTooltips = "true,true,true,true,true,true,true,true,true";

		document.getElementById("dataGridDiv").style.display = 'block';
		document.getElementById("DivExportExcel").style.display = 'block';
		document.getElementById("DivNothingMessage").style.display = 'none';
		// split the functions to avoid multiple parameters passing in single
		// function
		gridObj = initGridObject('dataGridDiv');
		gridObj.enableDistributedParsing(true);
		gridObj = fnCustomTypes(gridObj, setHeader, setColIds, setInitWidths,
				setColAlign, setColTypes, setColSorting, enableTooltips,
				setFilter, 500);
		gridObj = loadDhtmlxGridByJsonData(gridObj, response);
		
		moq_row_id=gridObj.getColIndexById("moqid");
		historyfl_row_id=gridObj.getColIndexById("historyfl");
		
		gridObj.attachEvent("onRowSelect", doOnRowSelect);
		gridObj.setColumnHidden(moq_row_id, true);
		 
	} else {
		document.getElementById("dataGridDiv").style.display = 'none';
		document.getElementById("DivExportExcel").style.display = 'none';
		document.getElementById("DivNothingMessage").style.display = 'block';
	}
}
// This Function Used to set custom types conditions in Grid
function fnCustomTypes(gObj, setHeader, setColIds, setInitWidths, setColAlign,
		setColTypes, setColSorting, enableTooltips, setFilter, gridHeight) {
	if (setHeader != undefined && setHeader.length > 0) {
		gObj.setHeader(setHeader);
		var colCount = setHeader.length;
	}
	if (setInitWidths != "")
		gObj.setInitWidths(setInitWidths);
	if (setColAlign != "")
		gObj.setColAlign(setColAlign);
	if (setColTypes != "")
		gObj.setColTypes(setColTypes);
	if (setColSorting != "")
		gObj.setColSorting(setColSorting);
	if (enableTooltips != "")
		gObj.enableTooltips(enableTooltips);
	if (setFilter != "")
		gObj.attachHeader(setFilter);
	if (setColIds != "") {
		gObj.setColumnIds(setColIds);
	}
	if (gridHeight != "")
		gObj.enableAutoHeight(true, gridHeight, true);
	else
		gObj.enableAutoHeight(true, 400, true);

	return gObj;
}

// to download excel
function fnExport() {
	gridObj.setColumnHidden(historyfl_row_id, true);
	gridObj.toExcel('/phpapp/excel/generate.php');
	gridObj.setColumnHidden(historyfl_row_id, false);
}
//This function used to view  MOQ Log details 
function fnHistroryLog(vendorMOQId){
	windowOpener("/gmTTPbyVendorMoq.do?method=loadTTPbyVendorMOQLog&vendorMOQId="+vendorMOQId,"Search","resizable=yes,scrollbars=yes,top=150,left=150,width=850,height=400");
}
//This function used to create user defined column types in Grid
//This Function for set history icon 
function eXcell_historyFl(cell) {
	// the eXcell name is defined here
	if (cell) { // the default pattern, just copy it
		this.cell = cell;
		this.grid = this.cell.parentNode.grid;
	}
	this.setValue = function(val) {
		if(val =='Y'){
		this.setCValue("<a href=\"#\"><img alt=\"Viewing MOQ Details\" " +
				        "src=\"/images/icon_History.gif\" border=\"0\"></a>",val);
		}
	}
	this.getValue = function() {
		return this.cell.childNodes[0].innerHTML; // gets the value
	}
}
eXcell_historyFl.prototype = new eXcell;
//when click history icon to call the fnHistroryLog function
function doOnRowSelect(rowId,cellInd)
{
	if(cellInd == historyfl_row_id){
		historyfl_value = gridObj.cellById(rowId, historyfl_row_id).getValue();
		 moq_id = gridObj.cellById(rowId, moq_row_id).getValue();
		if(historyfl_value != ''){
			fnHistroryLog(moq_id);
		}
	   
	}  
}
