
var purchaseord_rowid='';
//Get capaid and Ajaxcall to fetch the Success status data
function fnOnPageSuccessLoad() {
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmTTPVendorPOPopUp.do?method=fetchVendorPOSuccess&capaDtlsId='
			+ vendorCapaId
			+ '&ramdomId='
			+ Math.random());
	var loader = dhtmlxAjax.getSync(ajaxUrl);
	fnPOSuccessResponse(loader);
}
// Get the value from loader and split the response
function fnPOSuccessResponse(loader) {
	
	var response = loader.xmlDoc.responseText;
	var responseSplit = response.split("^");
	var headerDtls = responseSplit[0];
	var PODtls = responseSplit[1];
	var vendorName='';
	var loadPeriod='';
	var poCreatedBy='';
	var poCreatedDt='';
	var strHeaderDtlsJson =  '['+headerDtls+']';
	var JSON_Array_obj = JSON.parse(strHeaderDtlsJson);
	
	$.each(JSON_Array_obj, function(index,jsonObject){
		vendorName = jsonObject.vendorname;
		loadPeriod = jsonObject.loadperiod;
		poCreatedBy = jsonObject.pocreatedby;
		poCreatedDt= jsonObject.pocreateddate;
	});
	//this used to show header details in report
	setPoPopUpSuccessHeaderLabel(vendorName,loadPeriod,poCreatedBy,poCreatedDt);
	
     if(PODtls != ''){  // form the grid report
			var setHeader = [ "S.NO ","PO#", "WO Line", "PO Qty", "PO Amount" ];
			var setColIds = ",purchaseordid,powocnt,pototalqty,totalamt";

			var setColTypes = "cntr,poid,ron,ron,ron";
			var setColAlign = "center,left,right,right,right";
			var setColSorting = "na,str,int,int,int";
			var enableTooltips = "true,true,true,true";
			var setFilter = ["", "#text_filter", "#numeric_filter", "#numeric_filter","#numeric_filter" ];
			var setInitWidths = "60,180,100,150,180";

			var gridHeight = "";
			var dataHost = {};
			var footerArry = new Array();
			footerArry[1] = "";
			footerArry[1] = "<b>Total<b>";
			footerArry[2] = "<b>{#stat_total}<b>";
			footerArry[3] = "<b>{#stat_total}<b>";
			footerArry[4] = "<b>{#stat_total}<b>";
			var footerStyles = [];
			var footerExportFL = true;
			var format = "Y"
			var formatDecimal = "000,000.00";
			var formatColumnIndex = 4;
			document.getElementById("DivNothingMessage").style.display = 'none';

			// split the functions to avoid multiple parameters passing in
			// single function
			gridObj = initGridObject('dataGridDiv');
			
			gridObj = fnCustomTypes(gridObj, setHeader, setColIds,
					setInitWidths, setColAlign, setColTypes, setColSorting,
					enableTooltips, setFilter, gridHeight, footerArry,
					footerStyles, format, formatDecimal, formatColumnIndex);
		
			gridObj = loadDhtmlxGridByJsonData(gridObj, PODtls);
			purchaseord_rowid = gridObj.getColIndexById("purchaseordid");
			gridObj.attachEvent("onRowSelect", doOnRowSelect);
     }else{
    	document.getElementById("dataGridDiv").style.display = 'none';
  		document.getElementById("DivNothingMessage").style.display = 'block';
  		document.getElementById("DivExportExcel").style.display = 'none';
  		document.getElementById("DivClose").style.display = 'none';
     }
}
// This Function Used to set custom types conditions in Grid
function fnCustomTypes(gObj, setHeader, setColIds, setInitWidths, setColAlign,
		setColTypes, setColSorting, enableTooltips, setFilter, gridHeight,
		footerArry, footerStyles, format, formatDecimal, formatColumnIndex) {

	if (setHeader != undefined && setHeader.length > 0) {
		gObj.setHeader(setHeader);
		var colCount = setHeader.length;
	}

	if (setInitWidths != "")
		gObj.setInitWidths(setInitWidths);

	if (setColAlign != "")
		gObj.setColAlign(setColAlign);

	if (setColTypes != "")
		gObj.setColTypes(setColTypes);

	if (setColSorting != "")
		gObj.setColSorting(setColSorting);

	if (enableTooltips != "")
		gObj.enableTooltips(enableTooltips);

	if (setFilter != "")
		gObj.attachHeader(setFilter);

	if (footerArry != undefined && footerArry.length > 0) {
		var footstr = eval(footerArry).join(",");
		if (footerStyles != undefined && footerStyles.length > 0)
			gObj.attachFooter(footstr, footerStyles);
		else
			gObj.attachFooter(footstr);
	}

	if (setColIds != "") {
		gObj.setColumnIds(setColIds);
	}
	if (gridHeight != "")
		gObj.enableAutoHeight(true, gridHeight, true);
	else
		gObj.enableAutoHeight(true, 400, true);

	if (format != "") {
		gObj.setNumberFormat(formatDecimal, formatColumnIndex);
		gObj.setNumberFormat("000,000",3);
		gObj.setNumberFormat("000,000",2);
		
	}
	return gObj;
}
//this function used to set header details to report 
function  setPoPopUpSuccessHeaderLabel(vendorName,loadPeriod,poCreatedBy,poCreatedDt){
	var obj = eval("document.all.lbl_vendor_nm");
	obj.innerHTML = vendorName;

	var obj = eval("document.all.lbl_lock_period");
	obj.innerHTML = loadPeriod;

	var obj = eval("document.all.lbl_po_created_by");
	obj.innerHTML = poCreatedBy;

	var obj = eval("document.all.lbl_po_created_date");
	obj.innerHTML =poCreatedDt;
} 

// to download excel
function fnExport() {

	gridObj.toExcel('/phpapp/excel/generate.php');

}
// This Function used close the popup window
function fnClose() {
	window.close();
}
//this function used to  set the hyper link to PO id
function eXcell_poid(cell) {// the eXcell name is defined here
	if (cell) { // the default pattern, just copy it
		this.cell = cell;
		this.grid = this.cell.parentNode.grid;
	}
	
	this.setValue = function(val) {
				this.setCValue("<a href=\"#\">"+val+"",val);	
	}
	this.getValue = function() {
		return this.cell.childNodes[0].innerHTML; // gets the value
	}
}
eXcell_poid.prototype = new eXcell;
//This function used to view the work order
function doOnRowSelect(rowId, cellInd, stage){
	if(cellInd == purchaseord_rowid){
		var poId= gridObj.cellById(rowId, purchaseord_rowid).getValue();

		document.frmTTPVendorPODashboard.action = gStrServletPath+ "/GmPOServlet";	
		document.frmTTPVendorPODashboard.hAction.value = 'ViewPO';
		document.frmTTPVendorPODashboard.hPOId.value = poId;
		document.frmTTPVendorPODashboard.hVenId.value = vendorId;
		document.frmTTPVendorPODashboard.submit();
	}
}