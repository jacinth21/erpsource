function fnOnPageLoad()
{
	if(objGridData != '')
	{
		gridObj = initGrid('grpData',objGridData);
	}
	
}

function  onReload(){	
	objStartDt = document.frmProductReqSummary.fromDt;
	objEndDt = document.frmProductReqSummary.toDt;
	var dateDiffVal = dateDiff(todaysDate,objStartDt.value,format);
	
	/*if(dateDiffVal < 0 && document.frmProductReqSummary.dist.value == 0 && document.frmProductReqSummary.rep.value == 0 ){
		
		Error_Details("Please choose a valid option for the filed: <b>Field Sales / Sales Rep<b>");
	}*/
	
	if(objStartDt.value == "")
	{
		fnValidateTxtFld('fromDt',message[5594]);
	}
	if(objEndDt.value == '')
	{
		fnValidateTxtFld('toDt',message[5595]);
	}
	
	if(objStartDt.value != ""){
		CommonDateValidation(objStartDt, format,Error_Details_Trans(message[5596],format));
	}

	if(objEndDt.value != ""){
		CommonDateValidation(objEndDt, format,Error_Details_Trans(message[5597],format));
	}

	if (ErrorCount > 0)
		{
			Error_Show();
			Error_Clear();
			return false;
		}	

	
	document.frmProductReqSummary.strOpt.value = "load"
	fnStartProgress();
	frmProductReqSummary.submit();
}
function fnLoadRequestDetails(reqId)
{
	windowOpener("/GmLoanerPartRepServlet?hAction=Reload&hStatus=10,20,30,40&hOpt=Requests&PrdReqId="+reqId,"ReqDetails","resizable=yes,scrollbars=yes,top=150,left=200,width=1050,height=600");	
}
function fnLoadEditRequest(reqId)
{
	windowOpener("/gmOprLoanerReqEdit.do?requestId="+reqId,"EditReq","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}
function fnLoadReconciliationDetails(reqId)
{
	windowOpener("/gmLoanerReconciliation.do?strOpt=reLoad&qtType=10059&status=50823&requestId="+reqId,"ReconDetails","resizable=yes,scrollbars=yes,top=150,left=200,width=1100,height=600");	
}
function fnLoanerTicketSearch(url)
{
	windowOpener(url,"TicketSecarch","resizable=yes,scrollbars=yes,top=150,left=200,width=900,height=600");	
}
