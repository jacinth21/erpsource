var gridObj,gObj;

function fnOnPageLoad(){
	document.getElementById("DivGridData").style.display = 'none';
	document.getElementById("DivPrintButton").style.display = 'none';
	document.getElementById("DivExportExcel").style.display = 'none';
	document.getElementById("DivNothingMessage").style.display = 'none';
	document.getElementById("DivNoDataMessage").style.display = 'block';
	
	var donorNum = document.frmDonorLotRpt.donorId.value;
	if(donorNum != ''){
		document.getElementById("DivGridData").style.display = 'block';
		document.getElementById("DivPrintButton").style.display = 'block';
		document.getElementById("DivExportExcel").style.display = 'block';
		document.getElementById("DivNothingMessage").style.display = 'block';
		document.getElementById("DivNoDataMessage").style.display = 'none';
		fnLoad();
	}

}

function initGridData(divRef,gridData){
	gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);
	gObj.enableTooltips("true,true,true,true,true,true,true,true,true,true,true");
	gObj.attachHeader('#numeric_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter');
	gObj.init();
	gObj.loadXMLString(gridData);
	return gObj;	
}

//Click on load button
function fnLoad(){
	var donorNum = document.frmDonorLotRpt.donorId.value;	
	if(donorNum == ''){
		Error_Details(message[10501]);        
	}
	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}
	fnDonorLotInfo(donorNum);
}


//To load the Lot Details based on the input Donor Number
function fnDonorLotInfo(donorNum){
	var loader;
	var response;
	fnStartProgress();
	dhtmlxAjax.get('/gmDonorLotRptAction.do?method=loadDonorLotInfo&strOpt=fetchLots&donorId=' + encodeURIComponent(donorNum)+"&hscreen="+screen+"&companyInfo="+companyInfoObj+'&ramdomId='+Math.random(),function(loader){
		if (loader.xmlDoc.responseText != null){
			response = loader.xmlDoc.responseText;
			gridObj = initGridData('DonorLotData',response);
			document.getElementById("DivGridData").style.display = 'block';
			document.getElementById("DivPrintButton").style.display = 'block';
			document.getElementById("DivExportExcel").style.display = 'block';
			document.getElementById("DivNothingMessage").style.display = 'none';
			document.getElementById("DivNoDataMessage").style.display = 'none';
			gridObj.enableAutoHeight(true,500,true);// To enable auto height for grid
			if(response.indexOf("cell") == -1){
				document.getElementById("DivNothingMessage").style.display = 'block';
				document.getElementById("DivPrintButton").style.display = 'none';
				document.getElementById("DivExportExcel").style.display = 'none';
				document.getElementById("DivNoDataMessage").style.display = 'none';
			}			
		}
		fnStopProgress();
	});
}

// Function to view the Pic Slip by clicking on the corresponding Transaction
/*function fnViewPicSlip(transTypeID,vendor,woid,type,refid){
//	var transArry = transTypeID.split("|");
	var transID = transArry[0];
	var transType = transArry[1];
	if(transType == 'DHR'){
		windowOpener("/GmPOReceiveServlet?hAction=ViewDHRPrint&hDHRId="+transID+"&hVenId="+vendor+"&hWOId="+woid,"DHR","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
	}else if(transType == 'Returns'){
		windowOpener('/GmPrintCreditServlet?hAction=PRINT&hRAID='+transID,"Returns","resizable=yes,scrollbars=yes,top=150,left=200,width=1050,height=850");
	}else if(transType == 'Order' || transType == 'Reserved'){
		windowOpener('/GmOrderItemServlet?hAction=PICSlip&hId='+transID,"Pack","resizable=yes,scrollbars=yes, top=150,left=150,width=760,height=610");
	}else if(transType == 'Consignment'){
		windowOpener("/GmConsignItemServlet?hAction=PicSlip&hConsignId="+transID+"&ruleSource="+type,"SetPic","resizable=yes,scrollbars=yes,top=150,left=150,width=760,height=600");
	}else{
		if(type == '103932'){
			windowOpener("/gmPartredesignation.do?method=generatePicSlip&strTxnIds="+transID+"&txntype=103932&ruleSource=103932&refId="+refid,"PTRTPicSlip","resizable=yes,scrollbars=yes,top=150,left=150,width=750,height=500");
		}else{
			windowOpener("/gmItemControl.do?haction=PicSlip&hConsignId="+transID+"&txntype="+type+"&ruleSource="+type+"&refId="+refid,"SetPic","resizable=yes,scrollbars=yes,top=150,left=150,width=760,height=600");
		}
	}
}
*/

//Function to view the Pic Slip by clicking on the corresponding Transaction
function fnViewPicSlip(transID, type){

	/*	4110	Consignment
		4114	Shelf to Repack
		4115	Quarantine To Scrap
		400058	Returns Hold to Repack 
		400063	Repack to Shelf
		400064	Returns Hold to Quarantine
		4000645  Part Redesignation
		4310	sales
		102908  Returns
		DHR     DHR
	*/
	if(type == 'DHR'){
		windowOpener("/GmViewPopup.do?method=dhrViewPopup&hDHRId="+transID,"DHR","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
	}else if(type == '400064' || type == '400058' || type == '102908' ){
		windowOpener('/GmPrintCreditServlet?hAction=PRINT&hRAID='+transID,"Returns","resizable=yes,scrollbars=yes,top=150,left=200,width=1050,height=850");
	}else if(type  == '4310') {
		windowOpener('/GmOrderItemServlet?hAction=PICSlip&hId='+transID,"Pack","resizable=yes,scrollbars=yes, top=150,left=150,width=780,height=610");
	}else if(type == '4110'|| type == '4115'|| type == '400063'){
		windowOpener("/GmConsignItemServlet?hAction=PicSlip&hConsignId="+transID+"&ruleSource="+type,"SetPic","resizable=yes,scrollbars=yes,top=150,left=150,width=780,height=600");
	}else if(type == '4000645'){
		windowOpener("/gmPartredesignation.do?method=generatePicSlip&strTxnIds="+transID+"&txntype=103932&ruleSource=103932&refId="+transID,"PTRTPicSlip","resizable=yes,scrollbars=yes,top=150,left=150,width=780,height=500");
	}else{
		windowOpener("/gmItemControl.do?haction=PicSlip&hConsignId="+transID+"&txntype="+type+"&ruleSource="+type+"&refId=","SetPic","resizable=yes,scrollbars=yes,top=150,left=150,width=780,height=600");
	} 
}
//Function to download the report to the Excel.
function fnDownloadXLS(){
	gridObj.toExcel('/phpapp/excel/generate.php');	
}
//Function to open the Printable view of the report by clicking on the Print Button
function fnPrintReport(){
	gridObj.printView();
}
// Function to open the Lot Code Report Screen by click on the Lot Number
function fnViewLotReport(lotNum){
	windowOpener(" /gmLotCodeRptAction.do?method=loadLotCodeProdInfo&hscreen=popup&lotNum="+lotNum,"LotCOdeReport","resizable=yes,scrollbars=yes,top=150,left=150,width=1020,height=600");
}
//Function to open the Part Number Setup Screen by click on the Part Number
function fnViewPartReport(partNum){
	windowOpener("/GmPartNumServlet?hOpt=FETCHCONTAINER&hAction=Lookup&Txt_PartNum="+encodeURIComponent(partNum),"PartNumSetup","resizable=yes,scrollbars=yes,top=250,left=300,width=1010,height=750");
}
// To open Receive bulk shipment screen
function fnOpenRSScreen(rsid){ 
	windowOpener("/GmPageControllerServlet?strPgToLoad=/gmPOBulkReceive.do?method=loadBatchshipment&strOpt=loadRS&strMode=FETCHCONTAINER&rsNumber="+rsid, "","resizable=yes,scrollbars=yes,top=250,left=300,width=1050,height=750");
}