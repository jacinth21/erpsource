//This function used to load the grid
function fnOnPageLoad()
{
	document.frmInsertEdit.txnID.focus();
	gridObj = initGridWithDistributedParsing('dataGrid',objGridData);
	gridObj.attachEvent("onEditCell",doOnCellEdit);
}

//This function used to edit the cell in Grid
function doOnCellEdit(stage, rowId, cellInd, nValue, oValue) {
	if(cellInd == 1){ 
		if (stage==2){
			var gridrows = gridObj.getAllRowIds();
			var gridrowsarr = gridrows.toString().split(",");
			//to check the part number is available in Part Number Table
			var ajaxUrl = fnAjaxURLAppendCmpInfo('gmlocation.do?method=getPartNumDesc&partNum='+ encodeURIComponent(nValue)+'&ramdomId='+Math.random());
			var response = dhtmlxAjax.getSync(ajaxUrl);
			if(response.xmlDoc.responseText=="Fail"){
				gridObj.cellById(rowId, 2).setValue(message_operations[767]);
				return true;
			}
			for (var rowid = 0; rowid < gridrowsarr.length; rowid++){
				gridrowid = gridrowsarr[rowid];
				if(gridrowid != rowId){
					var pnum = gridObj.cellById(gridrowid,1).getValue();
					if(nValue == pnum){
						gridObj.cellById(rowId, 2).setValue(message_operations[766]);
						return true;		
					}
				}
			}
        }					
    }	
return true;
}

//This function is used to add the gird rows
function addRow(){
	gridObj.addRow(gridObj.getUID(),'');
}

//This function is used to remove the selected rows
function removeSelectedRow(){
	var gridrows=gridObj.getSelectedRowId();
	if(gridrows!=undefined){
		var NameCellValue ;
		var gridrowsarr = gridrows.toString().split(",");
		var added_row;
		for (var rowid = 0; rowid < gridrowsarr.length; rowid++) {
			gridrowid = gridrowsarr[rowid];
			
			NameCellValue = gridObj.cellById(gridrowid, 0).getValue();
			added_row = gridObj.getRowAttribute(gridrowid,"row_added");
			gridObj.deleteRow(gridrowid);			
			voidRowIdStr = voidRowIdStr + gridrowid + ',';
		}
	}else{
		Error_Clear();
		Error_Details(message_operations[8]);				
		Error_Show();
		Error_Clear();
	}
}

//This function is used to form the input string and submit the form.
function fnSubmit()
{
		gridObj.editStop();// return value from editor(if presents) to cell and closes editor
		var gridrowid;
		var void_flag;
		var inputString='';
		var gridrows =gridObj.getChangedRows(",");
		var tempval = '';
		var insertId= '';

		fnValidateTxtFld('txnID',lblTxnID);
		if(gridrows.length==0){
			inputString = "";
		}
		else
		{
			var gridrowsarr = gridrows.toString().split(",");
			var columnCount = gridObj.getColumnsNum();
			
			for (var rowid = 0; rowid < gridrowsarr.length; rowid++) {
				gridrowid = gridrowsarr[rowid];
				tempval = gridObj.cellById(gridrowid, 2).getValue();
				insertId = gridObj.cellById(gridrowid, 1).getValue();
				if(tempval == lblNoPartNum)
				{
					Error_Details(message_operations[768]);					
				}
				if(tempval == lblPartNumDuplicated)
				{
					Error_Details(message_operations[769]);					
				}
				if(insertId == ''){
					Error_Details(message_operations[770]);
				}
					void_flag = gridObj.getRowAttribute(gridrowid,"row_deleted");
					if(void_flag ==  undefined){
						void_flag='';
					}
					
					for(var col=0;col<columnCount;col++){
						inputString+=gridObj.cellById(gridrowid, col).getValue()+'^';
					}
					inputString+='|';
			}
			inputString+="$";
	 }
		if (ErrorCount > 0)
		{
			Error_Show();
			Error_Clear();
			return false;
		}
		document.frmInsertEdit.strInputString.value = inputString;
		document.frmInsertEdit.strVoidRowIds.value  = voidRowIdStr;
		document.frmInsertEdit.action="/gmInsertEditAction.do?method=saveInsertDetail";
		fnStartProgress('Y');
		document.frmInsertEdit.submit();
}

// This function is used to load the report.
function fnLoad()
{
	fnValidateTxtFld('txnID',lblTxnID);
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	var txnID = TRIM(document.frmInsertEdit.txnID.value);
	document.frmInsertEdit.action="/gmInsertEditAction.do?method=fetchInsertDetail&txnID ="+txnID;
	fnStartProgress();
	document.frmInsertEdit.submit();
}