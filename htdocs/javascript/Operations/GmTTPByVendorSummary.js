var response = '';
var hisMon1_rowId = '';
var hisMon2_rowId = '';
var hisMon3_rowId = '';
var hisMon4_rowId = '';
var hisMon5_rowId = '';
var hisMon6_rowId = '';
var foreCastMon1_rowId = '';
var foreCastMon2_rowId = '';
var foreCastMon3_rowId = '';
var foreCastMon4_rowId = '';
var foreCastMon5_rowId = '';
var foreCastMon6_rowId = '';
var strMsg = '';
var strFlag = '';
var ttpcapa_dtls_rowId='';
var ovrride_split_rowId='';
var raisepoqty_rowId='';
var overridesplitper_rowId='';
var status_rowId='';
var approvedby_rowId = '';
var overridevendornm_rowId = '';
var othervendorsplitper_rowId = '';
var editrowfl_rowId = '';
var combo = '';

function fnOnpageLoad() {
	var ttpId = document.frmTTPByVendorRpt.ttpId.value;
	var ttpMonth = document.frmTTPByVendorRpt.ttpMonth.value;
	var ttpYear = document.frmTTPByVendorRpt.ttpYear.value;
	var vendorId = document.frmTTPByVendorRpt.vendorId.value;
	if(ttpId != '' || vendorId != ''){
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmTTPByVendorSummary.do?method=fetchTTPbyVendorSummary&ttpId='
				+ ttpId
				+ '&strLoadDate='
				+ ttpMonth + '/'+ ttpYear
				+ '&vendorId=' + vendorId + '&ramdomId=' + new Date().getTime());
		dhtmlxAjax.get(ajaxUrl, fnSummaryResponse)
		fnStartProgress();
	}
}
//this function used to fetch the TTP Summary details
function fnSummaryLoad() {
	var ttpId = document.frmTTPByVendorRpt.ttpId.value;
	var ttpMonth = document.frmTTPByVendorRpt.ttpMonth.value;
	var ttpYear = document.frmTTPByVendorRpt.ttpYear.value;
	var vendorId = document.frmTTPByVendorRpt.vendorId.value;
	
	fnValidateDropDn('ttpMonth','Month');
	fnValidateDropDn('ttpYear','Year');
	//if ttp id and vendor id is empty it show error message
	if (ttpId == '' && vendorId == '') {
		Error_Details(message_operations[825]);
	}

	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	fnStartProgress();
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmTTPByVendorSummary.do?method=fetchTTPbyVendorSummary&ttpId='
			+ ttpId
			+ '&strLoadDate='
			+ ttpMonth + '/'+ ttpYear
			+ '&vendorId=' + vendorId + '&ramdomId=' + new Date().getTime());
	dhtmlxAjax.get(ajaxUrl, fnSummaryResponse)

	if (vendorId != '') {
		document.all.BTN_CHART_BY_VENDOR.disabled=false;
	}else{
		document.all.BTN_CHART_BY_VENDOR.disabled=true;
	}
	document.all.chartDivimg.src = '/images/plus.gif';
	document.all.chartHdrDiv.innerHTML = "";
}
//this  call back function used to generate the DHTMLX Grid in JS
function fnSummaryResponse(loader) {
	document.getElementById("succesMsg").style.display = "none";
	document.getElementById("rolledBackMsg").style.display = "none";
	var ttpId = document.frmTTPByVendorRpt.ttpId.value;
	var vendorId = document.frmTTPByVendorRpt.vendorId.value;
	
	response = loader.xmlDoc.responseText;
	
	var responseSpilt = response.split('^'); // Split the response

	fnStopProgress();
	if (responseSpilt[0] != '') {
		JSON_Array_obj = JSON.parse(responseSpilt[0]);
		var setColIds = "";
		var setHeader = responseSpilt[1]; //Splited response responseSpilt[1] set to header details
		var setStatusId = responseSpilt[2];//Pass the status value to fnEnableDisableBtn function as input parameter
		if (document.frmTTPByVendorRpt.historicalData[0].checked) { //if receipt radio button checked show receipt data else show PO qty data
			setColIds = "partnum,partnumdesc,ttpname,vendorname,histrecpmon01,histrecpmon02,histrecpmon03,histrecpmon04,histrecpmon05,histrecpmon06,"
					+ "forecastmon01,forecastmon02,forecastmon03,forecastmon04,forecastmon05,forecastmon06,totalordqty,historysplitper,"
					+ "overridesplitper,overridesplitfl,othervendorname,othervenraisepoqty,overridevendorname,othervendorsplitper,historyavgqty,recommandqty," +
					"raisepoqty,raisepoqtyvar,absraisepoqtyvar,ttpchartbypartdata,ttpvendorcapadtlsid,statusid,approvedby,editrowfl,overridevendorid,vendormoq";
		} else {
			setColIds = "partnum,partnumdesc,ttpname,vendorname,hispomon01,hispomon02,hispomon03,hispomon04,hispomon05,hispomon06,"
					+ "forecastmon01,forecastmon02,forecastmon03,forecastmon04,forecastmon05,forecastmon06,totalordqty,historysplitper,"
					+ "overridesplitper,overridesplitfl,othervendorname,othervenraisepoqty,overridevendorname,othervendorsplitper,historyavgqty,recommandqty," +
					"raisepoqty,raisepoqtyvar,absraisepoqtyvar,ttpchartbypartdata,ttpvendorcapadtlsid,statusid,approvedby,editrowfl,overridevendorid,vendormoq";
		}
	
		// PMT-55992 Forecast Chart value change (to update column align)
		var setColTypes = "ro,ro,ro,ro,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ro,ed,historyFl,ro,ron,coro,ro,ron,ron,ed,ron,ron,charticon,ro,ro,ro,ro,ro,ro";
		var setColAlign = "left,left,left,left,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,center,left,right,left,right,right,right,right,right,center,left,left,left,left,left,left,right";
		var setColSorting = "str,str,str,str,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,,str,int,str,int,int,int,int,int,int,str,str,int,int,str,int,int";
		var enableTooltips = "true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true";
		var setFilter = [ "#text_filter","#text_filter","#text_filter", "#select_filter", "#numeric_filter","#numeric_filter", "#numeric_filter", "#numeric_filter", "#numeric_filter",
				"#numeric_filter", "#numeric_filter", "#numeric_filter", "#numeric_filter","#numeric_filter", "#numeric_filter", "#numeric_filter", "#numeric_filter",
				"#numeric_filter", "","", "#select_filter","#numeric_filter","#select_filter","", "#numeric_filter","#numeric_filter", "#numeric_filter","#numeric_filter","#numeric_filter","","","","","","","#numeric_filter"];
		//var setInitWidths = "65,100,70,*,40,40,40,40,40,40,40,40,40,40,40,40,60,60,55,20,100,50,60,60,60,50,50,50,0,0,0";
   	    var setInitWidths = "70,50,50,120,50,50,50,50,50,50,50,50,50,50,50,50,60,50,60,25,100,30,100,50,65,65,60,60,45,50,45,,,,,50";

		//Get required Qty total
		var footerArry=new Array();	
		footerArry[0]="";
		footerArry[1]="";
		footerArry[2]="";
		// showing footer total for historical month (1 to 6) , forecast month(1 to 6) and total order qty
		footerArry[3]="<b>Total<b>";
		footerArry[4]="<b>{#stat_total}<b>";
		footerArry[5]="<b>{#stat_total}<b>";
		footerArry[6]="<b>{#stat_total}<b>";
		footerArry[7]="<b>{#stat_total}<b>";
		footerArry[8]="<b>{#stat_total}<b>";
		footerArry[9]="<b>{#stat_total}<b>";
		footerArry[10]="<b>{#stat_total}<b>";
		footerArry[11]="<b>{#stat_total}<b>";
		footerArry[12]="<b>{#stat_total}<b>";
		footerArry[13]="<b>{#stat_total}<b>";
		footerArry[14]="<b>{#stat_total}<b>";
		footerArry[15]="<b>{#stat_total}<b>";
		footerArry[16]="<b>{#stat_total}<b>";
		footerArry[17]="";
		footerArry[18]="";
		footerArry[19]="";
		footerArry[20]="";
		footerArry[21]="<b>{#stat_total}<b>";//other vendor raise PO qty
		footerArry[22]="";
		footerArry[23]=""; 
		footerArry[24]="<b>{#stat_total}<b>";//avg Po hist.Qty
		footerArry[25]="<b>{#stat_total}<b>";//Recomm. PO qty
		footerArry[26]="<b>{#stat_total}<b>";//Raise PO qty
		footerArry[27]="";
		footerArry[28]="";
		footerArry[29]="<img alt=\"Viewing Total footer Chart\" src=\"/images/chart.gif\" border=\"0\" onClick=fnFooterChartByFilter()>"; 
		var gridHeight = "";//screen.width * screen.height;
		var footerStyles = [];
		var footerExportFL = true;
		var	pagination = "";
		var	format = "Y"
		var formatDecimal="000,000";
		var formatColumnIndex=25;
		var formatColumnIndexOne=26;
		document.getElementById("dataGridDiv").style.display = 'block';
		document.getElementById("DivExportExcel").style.display = 'block';
		document.getElementById("DivApprove").style.display = 'block';
		document.getElementById("chartHdrDiv").style.display = 'block';
		document.getElementById("Expandchart").style.display = 'block';
		document.getElementById("DivNothingMessage").style.display = 'none';
		// split the functions to avoid multiple parameters passing in single function
		gridObj = initGridObject('dataGridDiv');

		// Addded for PMT-53403 - enable distributedparse
		gridObj.enableDistributedParsing(true);

		gridObj = fnCustomTypes(gridObj,setHeader, setColIds, setInitWidths, 
				setColAlign, setColTypes,setColSorting, enableTooltips, setFilter,500,pagination,footerArry,footerStyles,format,
				formatDecimal,formatColumnIndex,formatColumnIndexOne);
		gridObj = loadDhtmlxGridByJsonData(gridObj, responseSpilt[0]);
		gridObj.attachEvent("onEditCell",  doOnCellEdit);
		//Addded for PC-5526 - sort part number on grid load time
		gridObj.sortRows(0,"str","asc");  
		//gridObj.enableEditTabOnly(true);
		//gridObj.enableEditEvents(true,false,true);
		ttpcapa_dtls_rowId = gridObj.getColIndexById("ttpvendorcapadtlsid");
	    ovrride_split_rowId = gridObj.getColIndexById("overridesplitfl");
	    editrowfl_rowId = gridObj.getColIndexById("editrowfl");
	    othervendorsplitper_rowId = gridObj.getColIndexById("othervendorsplitper");
	    raisepoqty_rowId = gridObj.getColIndexById("raisepoqty");
	    overridesplitper_rowId = gridObj.getColIndexById("overridesplitper");
	    status_rowId = gridObj.getColIndexById("statusid");
	    approvedby_rowId = gridObj.getColIndexById("approvedby");
	    
	    overridevendornm_rowId = gridObj.getColIndexById("overridevendorname");
	    gridObj.attachEvent("onRowSelect", doOnRowSelect);
	    //gridObj.attachEvent("onRowSelect", doOnRowSelect);
	    gridObj.setColumnHidden(1, true);
	    gridObj.setColumnHidden(2, true);
	    gridObj.setColumnHidden(21, true);
	    gridObj.setColumnHidden(27, true);
	    gridObj.setColumnHidden(28, true);
		gridObj.setColumnHidden(30, true);
		gridObj.setColumnHidden(31, true);
		gridObj.setColumnHidden(32, true);
		gridObj.setColumnHidden(33, true);
		gridObj.setColumnHidden(34, true);
		gridObj.setColumnHidden(35, true);
		gridObj.enableHeaderMenu();
		gridObj.forEachRow(function(rowId) {
			var status = gridObj.cellById(rowId, status_rowId).getValue();
			var approvedby = gridObj.cellById(rowId, approvedby_rowId).getValue();
			if((status!='26240574') && (approvedby != '')){
				gridObj.setCellTextStyle(rowId,overridesplitper_rowId,"border-width: 0px;border-style: inset;");
				gridObj.cellById(rowId, overridesplitper_rowId).setDisabled(true);
				gridObj.setCellTextStyle(rowId,raisepoqty_rowId,"border-width: 0px;border-style: inset;");
				gridObj.cellById(rowId, raisepoqty_rowId).setDisabled(true);
			}else if(status!='26240574'){
				gridObj.setCellTextStyle(rowId,raisepoqty_rowId,"border-width: 0px;border-style: inset;");
				gridObj.cellById(rowId, raisepoqty_rowId).setDisabled(true);
				gridObj.setCellTextStyle(rowId,overridesplitper_rowId,"background-color:white;  border-width: 1px;border-style: inset;");
				
			}else if(approvedby != ''){
				gridObj.setCellTextStyle(rowId,overridesplitper_rowId,"border-width: 0px;border-style: inset;");
				gridObj.cellById(rowId, overridesplitper_rowId).setDisabled(true);
				gridObj.setCellTextStyle(rowId,raisepoqty_rowId,"background-color:white;  border-width: 1px;border-style: inset;");
			}else{
				gridObj.setCellTextStyle(rowId,overridesplitper_rowId,"background-color:white;  border-width: 1px;border-style: inset;");
				gridObj.setCellTextStyle(rowId,raisepoqty_rowId,"background-color:white;  border-width: 1px;border-style: inset;");
			}
		});
		
		fnEnableDisableBtn(setStatusId);
	} else {

		document.getElementById("dataGridDiv").style.display = 'none';
		document.getElementById("DivNothingMessage").style.display = 'block';
		document.getElementById("DivExportExcel").style.display = 'none';
		document.getElementById("DivApprove").style.display = 'none';
		document.getElementById("chartHdrDiv").style.display = 'none';
		document.getElementById("Expandchart").style.display = 'none';

	}
}

//this function is used to perform action when a cell is editing
function doOnCellEdit(stage, rowId, cellInd, nValue, oValue) {
	//Highlight data when edit cell
	var editrowfl =  gridObj.cellById(rowId, editrowfl_rowId).getValue();
	if (editrowfl == 'Y') {
		return false;
	}else{
		//alert(stage);
	if (stage == 1 && (cellInd == overridesplitper_rowId || cellInd == raisepoqty_rowId) ){
		var c=this.editor.obj;			
		if(c!=null) c.select();
	}
		
	//Get the other override vendor name based on part num and vendor id
	if(stage == 1 && (cellInd == overridevendornm_rowId)){
		var ttpCAPADtlsId= gridObj.cellById(rowId, 30).getValue();
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmTTPByVendorSummary.do?method=fetchActiveVendorPriceByParts&ttpId='
				+ ttpCAPADtlsId + '&ramdomId=' + new Date().getTime());
		var loader = dhtmlxAjax.getSync(ajaxUrl);
		fnVendorPriceResponse(loader,rowId);
	}
	
	//Calculate other override vendor split pec when override split pec chenged
	if(stage == 2 &&(cellInd == overridesplitper_rowId)){
		if(nValue != ''){
			var overridevenper = 100 - nValue;
			gridObj.cellById(rowId, othervendorsplitper_rowId).setValue(overridevenper);
		}
	}
	
	if(stage == 2 &&(cellInd == overridevendornm_rowId)){
		var overrideven = gridObj.cellById(rowId,overridevendornm_rowId).getValue();
		var overridePec = gridObj.cellById(rowId,othervendorsplitper_rowId).getValue();
		var ttpCAPADtlsId= gridObj.cellById(rowId, 30).getValue();
		if(overrideven != '' && overridePec == ''){
			var overridevenper = gridObj.cellById(rowId,18).getValue();
			if (overridevenper == ''){
				overridevenper = gridObj.cellById(rowId,17).getValue();
				gridObj.cellById(rowId, othervendorsplitper_rowId).setValue(100-overridevenper);				
			}
			else{
				gridObj.cellById(rowId, othervendorsplitper_rowId).setValue(100-overridevenper);	
			}		
		}
	}
	return true;
	}
	
}
//when click history icon to call the fnLoadOverideHistory function
function doOnRowSelect(rowId,cellInd)
{
	if(cellInd == ovrride_split_rowId){
		var ttpCAPADtlsId= gridObj.cellById(rowId, 30).getValue();
		var overridefl = gridObj.cellById(rowId, ovrride_split_rowId).getValue();
		if(overridefl != ''){
			 fnLoadOverideHistory(ttpCAPADtlsId);
		}
	   
	}  
}
//This function is used to populate other override vendor name list based on part num and vendor
function fnVendorPriceResponse(loader,rowId){
	var JSON_Array_obj = "";
	var i = 0;
	response = loader.xmlDoc.responseText;
	if(response == 'N'){
		Error_Details(message_operations[879]);
		Error_Show();
		Error_Clear();
		return false;
	}else{
		JSON_Array_obj = JSON.parse(response);
		combo = gridObj.getCustomCombo(rowId, overridevendornm_rowId);
		combo.clear();
		$.each(JSON_Array_obj, function(index,jsonObject) {
			id = jsonObject.vendorid; 
			name = jsonObject.vendorname;
			combo.put(id,name);
		i++;
		});
		combo.save();
	}
		
}

//This Function Used to set custom types conditions in Grid
function fnCustomTypes(gObj,setHeader, setColIds, setInitWidths, setColAlign, 
		setColTypes,setColSorting, enableTooltips, setFilter,gridHeight,pagination,
		footerArry,footerStyles,format,formatDecimal,formatColumnIndex,formatColumnIndexOne){

if (setHeader != undefined && setHeader.length > 0) {
 gObj.setHeader(setHeader);
 var colCount = setHeader.length;
 }    

if (setInitWidths != "")
gObj.setInitWidths(setInitWidths);

if (setColAlign != "")
gObj.setColAlign(setColAlign);

if (setColTypes != "")
gObj.setColTypes(setColTypes);

if (setColSorting != "")
gObj.setColSorting(setColSorting);

if (enableTooltips != "")
gObj.enableTooltips(enableTooltips);

if (setFilter != "")
gObj.attachHeader(setFilter);

if (footerArry != undefined && footerArry.length > 0) {
var footstr = eval(footerArry).join(",");
if (footerStyles != undefined && footerStyles.length > 0)
gObj.attachFooter(footstr, footerStyles);
else
gObj.attachFooter(footstr);
}  

if(setColIds != ""){
 gObj.setColumnIds(setColIds);
}
gObj.setColumnColor(",,,,#f5c276,#f5c276,#f5c276,#f5c276,#f5c276,#f5c276,#b6f0dc,#b6f0dc,#b6f0dc,#b6f0dc,#b6f0dc,#b6f0dc");
if (gridHeight != "")
gObj.enableAutoHeight(true, gridHeight, true);
else
gObj.enableAutoHeight(true, 400, true);

if(pagination == "Y"){
    gObj.enablePaging(true,100,10,"pagingArea",true);
    gObj.setPagingSkin("bricks");
}	

if (format != ""){
gObj.setNumberFormat(formatDecimal,formatColumnIndex);
gObj.setNumberFormat(formatDecimal,formatColumnIndexOne);
}
return gObj;
}
//Enable and Disable the Save and Approve button
function fnEnableDisableBtn(statusValue){	
	var ttpId = document.frmTTPByVendorRpt.ttpId.value;
	var vendorId = document.frmTTPByVendorRpt.vendorId.value;
	var ttpMonth = document.frmTTPByVendorRpt.ttpMonth.value;
	var ttpYear = document.frmTTPByVendorRpt.ttpYear.value;
	var date=new Date();
    if(document.frmTTPByVendorRpt.BTN_SAVE != undefined && saveBtnAccessFl == "Y" && SwitchUserFl != 'Y' 
    	  && ((date.getMonth()+1 == ttpMonth)&&(date.getFullYear()== ttpYear))){
		document.frmTTPByVendorRpt.BTN_SAVE.disabled=false;
	}else{
			document.frmTTPByVendorRpt.BTN_SAVE.disabled=true;
	}
 
 
	if(document.frmTTPByVendorRpt.BTN_APPROVE != undefined && approveBtnAccessFl == "Y" && SwitchUserFl != 'Y' 
		&& ((date.getMonth()+1 ==ttpMonth)&&(date.getFullYear()==ttpYear))){
		//Also check search only by vendor,then enable the approve button
		if( (ttpId !=''&&vendorId == '') || (ttpId !=''&&vendorId != '') ){
			document.frmTTPByVendorRpt.BTN_APPROVE.disabled=true;
			document.frmTTPByVendorRpt.BTN_ROLL_BACK.disabled=true;
		}else{
			
			//In approve enable code, check status and enable the Approve/Rollback button
			//Status is approve , then enable rollback button and disable rollback button
			//Status is open, then enable approve button and disable rollback button	
			if(statusValue == '108860'){
		    document.frmTTPByVendorRpt.BTN_APPROVE.disabled=false;
		    document.frmTTPByVendorRpt.BTN_ROLL_BACK.disabled=true;
			}else if(statusValue == '108862'){     
			document.frmTTPByVendorRpt.BTN_ROLL_BACK.disabled=false;
			document.frmTTPByVendorRpt.BTN_APPROVE.disabled=true;
			}//Status is Approved, then enable Rollback button and disable approve button 
			//Status is PO Generated(26240822) or PO Generation Failed(26240823), then disable Rollback button
			else if(statusValue == '26240822' || statusValue == '26240823'){
				document.frmTTPByVendorRpt.BTN_ROLL_BACK.disabled=true;
				document.frmTTPByVendorRpt.BTN_APPROVE.disabled=true;
			}
		}
	}else{
		document.frmTTPByVendorRpt.BTN_APPROVE.disabled=true;
		document.frmTTPByVendorRpt.BTN_ROLL_BACK.disabled=true;
	}

}
//to download excel
function fnExport() {
	gridObj.setColumnHidden(19, true);
	gridObj.setColumnHidden(31, true);
	gridObj.toExcel('/phpapp/excel/generate.php');
	gridObj.setColumnHidden(19, false);
	gridObj.setColumnHidden(31, false);
}

//This function used to create user defined column types in Grid  
//This function for show  Chart icon
function eXcell_charticon(cell) {
	// the eXcell name is defined here
	if (cell) { // the default pattern, just copy it
		this.cell = cell;
		this.grid = this.cell.parentNode.grid;
	}

	this.setValue = function(val) {
		this
				.setCValue(
                        "<a href=\"#\" onClick=fnLoadChartByPart('"
                        + val
                        + "')><img alt=\"Viewing Part num Chart\" src=\"/images/chart.gif\" border=\"0\" disabled=\"true\"></a>",
                        val);
	}
	this.getValue = function() {
		return this.cell.childNodes[0].innerHTML; // gets the value
	}
}
eXcell_charticon.prototype = new eXcell;

//This function used to create user defined column types in Grid
//This Function for history icon
function eXcell_historyFl(cell) {
	// the eXcell name is defined here
	if (cell) { // the default pattern, just copy it
		this.cell = cell;
		this.grid = this.cell.parentNode.grid;
	}
	this.setValue = function(val) {
		if(val =='Y'){
		this.setCValue("<a href=\"#\"><img alt=\"Viewing Part num Chart\" src=\"/images/icon_History.gif\" border=\"0\" disabled=\"true\"></a>",
						val);
		}
	}

	this.getValue = function() {
		return this.cell.childNodes[0].innerHTML; // gets the value
	}
}
eXcell_historyFl.prototype = new eXcell;
//
//Function to used to expand the Chart.
function fnExpandChart(val) {
	
	var obj = eval("document.all.chartHdrDiv");
	var obj1 = eval("document.all.chartDivimg");
	
	if (obj.style.display == 'none') {	
		obj1.src = '/images/minus.gif';
		obj.style.display='block';
	} else {
		obj1.src = '/images/plus.gif';
		obj.style.display='none';
	}
}

//this function used to open the history log
function fnLoadOverideHistory(ttpCapaDtlId){
	windowOpener("/gmAuditTrail.do?auditId=315&hJNDIConnection=DMJNDI&txnId="+ttpCapaDtlId,"","resizable=yes,scrollbars=yes,top=250,left=300,width=785,height=300,status=1");
}

//This Function is used to approve the vendor when click approve button
function fnApproval(){
	var ttpId = document.frmTTPByVendorRpt.ttpId.value;
	var ttpMonth = document.frmTTPByVendorRpt.ttpMonth.value;
	var ttpYear = document.frmTTPByVendorRpt.ttpYear.value;
	var vendorId = document.frmTTPByVendorRpt.vendorId.value;
	if (confirm("Are you sure you want to Approve ?")) {
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmTTPByVendorSummary.do?method=approveTTPbyVendorSummary&ttpId='
			+ ttpId
			+ '&ttpMonth='
			+ ttpMonth
			+ '&ttpYear='
			+ ttpYear
			+ '&vendorId=' + vendorId + '&ramdomId=' + new Date().getTime());
	fnStartProgress();
	dhtmlxAjax.get(ajaxUrl, fnApprovalResponse)
	}else{
		return false;
	}
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	
}
//this  call back function used to show error message and success message
function fnApprovalResponse(loader) {
	var response = loader.xmlDoc.responseText;
	document.getElementById("succesMsg").style.display = "none";
	document.getElementById("rolledBackMsg").style.display = "none";
	fnStopProgress();
	//response Format=N##ErroMessage
	//Split as flag and Message
	if (response!=''){ 
	var b = response;
	var temp =  new Array();
	temp = b.split('##');
	strFlag  =  temp[0];
	strMsg =  temp[1];
	if(strFlag=='Y'){
				Error_Details(strMsg);
				if (ErrorCount > 0) {
					Error_Show();
					Error_Clear();
					return false;
				}
		}else{
			//fnSummaryLoad();
			document.getElementById("succesMsg").style.display = "table-row";
			document.frmTTPByVendorRpt.BTN_APPROVE.disabled=true;
			document.frmTTPByVendorRpt.BTN_ROLL_BACK.disabled=false;
		}
}	
}
// This function is used to form the Input for Save Percentage and RaisePOQty
function fnSubmitSummary(){
	var gridChrows = gridObj.getChangedRows(',');
	var gridRowid = gridChrows.split(',');
	var inputStr='';
	for(var i=0;i<gridRowid.length;i++){
		var ttpCAPADtlsId= gridObj.cellById(gridRowid[i], 30).getValue();
		var lockstatus= gridObj.cellById(gridRowid[i], status_rowId).getValue();
		var approvedby = gridObj.cellById(gridRowid[i], approvedby_rowId).getValue();
		var ttpPOQTY= gridObj.cellById(gridRowid[i], raisepoqty_rowId).getValue();
		var ttpOverRidePercentage = gridObj.cellById(gridRowid[i], overridesplitper_rowId).getValue();
		var otherOverrideVendor = gridObj.cellById(gridRowid[i], overridevendornm_rowId).getValue();
		var overrideVendorPer = gridObj.cellById(gridRowid[i], othervendorsplitper_rowId).getValue();
		if(isNaN(otherOverrideVendor)){
			otherOverrideVendor = gridObj.cellById(gridRowid[i], 34).getValue();
		}
		if(otherOverrideVendor != '' && overrideVendorPer == ''){	
			var historicalpec = gridObj.cellById(gridRowid[i], 17).getValue();
			overrideVendorPer = 100 - historicalpec;
		}
		if(isNaN(ttpOverRidePercentage)|| isNaN(ttpPOQTY) ){
			Error_Details("Please Enter the Numeric Value");   	
		 }
		if(ttpPOQTY < 0){
			Error_Details("Please Enter the <b>Raise PO Qty</b> greater than or equal to zero ");   
		}
		if(ttpOverRidePercentage < 0){
			Error_Details("Please enter <b>Override Split %</b> greater than or equal to zero");   
		}
		if(approvedby=='' && lockstatus=='26240574'){
			inputStr = inputStr + ttpCAPADtlsId + '^' + ttpOverRidePercentage+ '^'+ttpPOQTY + '^'+otherOverrideVendor +'^'+overrideVendorPer + '|';
		}
		else if(approvedby=='' && lockstatus!='26240574'){
			inputStr = inputStr + ttpCAPADtlsId + '^' + ttpOverRidePercentage+ '^'+''+ '^'+otherOverrideVendor +'^'+overrideVendorPer +'|';
		}
		else if(approvedby!='' && lockstatus=='26240574'){
			inputStr = inputStr + ttpCAPADtlsId + '^' + '' + '^'+ ttpPOQTY + '^'+''+'^'+''+'|';  
	    }	
		
	}
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
   fnStringFormation(inputStr);
}
// This function is used to save Override Percentage and Raise POQty
function fnStringFormation(inputStr){
		var ttpMonth = document.frmTTPByVendorRpt.ttpMonth.value;
		var ttpYear = document.frmTTPByVendorRpt.ttpYear.value;
		fnStartProgress();
		var loader = dhtmlxAjax.postSync('/gmTTPByVendorSummary.do?method=saveTTPbyVendorSummary&inputString='+inputStr+'&ttpMonth='+ ttpMonth
				+ '&ttpYear='
				+ ttpYear+'&ramdomId=' + Math.random()+'&'+fnAppendCompanyInfo());
		fnSaveCallBack(loader);
	}
//This  Function used for save call back function
function fnSaveCallBack(loader){
		var response = loader.xmlDoc.responseText;
		fnStopProgress();
		if(response == ''){
			fnSummaryLoad();
		}else{
		//split as array
		var ttpidlist = response.split(',');
		var ttpCAPADtlsId ='';
		for(var i=0;i<ttpidlist.length;i++){
			gridObj.forEachRow(function(rowid) {
				 ttpCAPADtlsId = gridObj.cellById(rowid, 30).getValue();
				 if(ttpCAPADtlsId==ttpidlist[i]){
					 gridObj.setRowTextStyle(rowid, "background-color:#ffcccc;color:red;");
				 }
			        });
			}
	}
	}
	
//This function used to press the enter key then submit the page.
function fnEnter() {
	if (event.keyCode == 13) {	
		fnSummaryLoad();
	 }		
}
// declare global variable
var ttpPartNumId = '';

//This function Used to load the chart based on ttp by part 
function fnLoadChartByPart(val) {
	console.log (' val ==> '+ val);
	var valSplit = val.split('~');
	var ttpCAPADtlId = valSplit[0];
	ttpPartNumId = valSplit[1];
	var ttpMonth = document.frmTTPByVendorRpt.ttpMonth.value;
	var ttpYear = document.frmTTPByVendorRpt.ttpYear.value;
	var strChartby = '';
	
	if (document.frmTTPByVendorRpt.historicalData[0].checked){
		strChartby = 'Receipt';
	}else{
		strChartby = 'PO';
	}
	
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmTTPByVendorSummary.do?method=fetchTTPbyVendorChartByPart&ttpCapaId='
			+ ttpCAPADtlId
			+ '&ttpPartNumId='
			+ ttpPartNumId
			+ '&strLoadDate='
			+ ttpMonth + '/'+ ttpYear
			+ '&strChartby='
			+ strChartby
			+ '&ramdomId=' + new Date().getTime());

	dhtmlxAjax.get(ajaxUrl, fnChartByPartResponse)
}

function fnChartByPartResponse(loader){
	response = loader.xmlDoc.responseText;
	var responseParse= JSON.parse(response);
	var result =  responseParse.chartbypartjsondata;
	var responseSplit = result.split(',dataset:');
	
	// PMT-55992 Forecast Chart value change (Chart label added the Part number and date)
	
	var ttpMonth = document.frmTTPByVendorRpt.ttpMonth.value;
	var ttpYear = document.frmTTPByVendorRpt.ttpYear.value;
	
	document.all.chartDivimg.src = '/images/minus.gif';
	document.all.chartHdrDiv.style.display = 'block';
	FusionCharts.ready(function() {
		var cat= new Array();
		var dat= new Array();
		  var myChart = new FusionCharts({
		    type: "stackedcolumn2dline",
		    renderAt: "chartHdrDiv",
		    width: "100%",
		    height: "400px",
		    dataFormat: "json",
		    legendPosition:"RIGHT",
		    dataSource: {
		    	  chart: {
		    		    showvalues: "0",
		    		    caption: "Part chart ("+ttpPartNumId +" - "+ ttpMonth + '/'+ ttpYear+ ")",
		    		    showhovereffect: "1",
		    		    showsum: "1",
		    		    theme: "fusion",
		    		    labelBinSize: "0",
		    		    interactiveLegend:"1",
		    		    paletteColors:"#ff0000,#FFA500,#ffbb90,#ffff00,#66bbbb,#019145,#000000,#000000",
                        usePlotGradientColor: "0"
		    		  },
		    			categories:eval(responseSplit[0]),
		    	    	dataset:eval(responseSplit[1])
		    } 
		  }).render();
		});
}

function fnLoadChartByVendor(vendorId) {
	var vendorId = document.frmTTPByVendorRpt.vendorId.value;
	var ttpMonth = document.frmTTPByVendorRpt.ttpMonth.value;
	var ttpYear = document.frmTTPByVendorRpt.ttpYear.value;
	var strChartby = '';
	
	if (document.frmTTPByVendorRpt.historicalData[0].checked){
		strChartby = 'Receipt';
	}else{
		strChartby = 'PO';
	}
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmTTPByVendorSummary.do?method=fetchTTPbyVendorChart&vendorId='
			+ vendorId 
			+ '&strLoadDate='
			+ ttpMonth + '/'+ ttpYear
			+ '&strChartby='
			+ strChartby
			+ '&ramdomId=' + new Date().getTime());

	dhtmlxAjax.get(ajaxUrl, fnChartByVendorResponse)
}
function fnChartByVendorResponse(loader){
	
	response = loader.xmlDoc.responseText;

	var responseParse= JSON.parse(response);
	var result =  responseParse.chartbyvendorjsondata;
	var responseSplit = result.split(',dataset:');
	
	// PMT-55992 Forecast Chart value change (Chart label added the Vendor name and date)
	var vendorName = $("#searchvendorId").val();
	var ttpMonth = document.frmTTPByVendorRpt.ttpMonth.value;
	var ttpYear = document.frmTTPByVendorRpt.ttpYear.value;
	
	document.all.chartDivimg.src = '/images/minus.gif';
	document.all.chartHdrDiv.style.display = 'block';
	
	FusionCharts.ready(function() {
		  var myChart = new FusionCharts({
		    type: "stackedcolumn2dline",
		    renderAt: "chartHdrDiv",
		    width: "100%",
		    height: "400px",
		    dataFormat: "json",
		    dataSource: {
		    	  chart: {
		    		    showvalues: "0",
		    		    caption: "Vendor chart ("+ vendorName+" - "+ ttpMonth + '/'+ ttpYear+ ")",
		    		    showhovereffect: "1",
		    		    showsum: "1",
		    		    theme: "fusion",
		    		    labelBinSize: "0",
		    		    interactiveLegend:"1",
		    		    paletteColors:"#ff0000,#FFA500,#ffbb90,#ffff00,#66bbbb,#019145,#000000,#000000",
                        usePlotGradientColor: "0"
		    		  },
		    		  categories:eval(responseSplit[0]),
		    	      dataset:eval(responseSplit[1])
		    }
		  }).render();
		});
}
// This function Used to  show total footer chart based on filtered data 
function fnFooterChartByFilter() {
	var inputStr = '';
	var strChartby = '';
	var vendorCapaDtlsId='';
	var vendorCapaDtlId_rowId = gridObj.getColIndexById("ttpvendorcapadtlsid");
	
	gridObj.forEachRowA(function(rowid) { // It allows you to iterate in the filtered grid through the rows passed the filtration only.
		 vendorCapaDtlsId = gridObj.cellById(rowid, vendorCapaDtlId_rowId).getValue();
		 if(vendorCapaDtlsId !=''){
			 inputStr += vendorCapaDtlsId +',';
		 }	 
	
	});
	if (document.frmTTPByVendorRpt.historicalData[0].checked) {
		strChartby = 'Receipt';
		//get index id for getting historical month Label values and set into x axis 
		hisMon1_rowId = gridObj.getColIndexById("histrecpmon01");
		hisMon2_rowId = gridObj.getColIndexById("histrecpmon02");
		hisMon3_rowId = gridObj.getColIndexById("histrecpmon03");
		hisMon4_rowId = gridObj.getColIndexById("histrecpmon04");
		hisMon5_rowId = gridObj.getColIndexById("histrecpmon05");
		hisMon6_rowId = gridObj.getColIndexById("histrecpmon06");
	} else {
		strChartby = 'PO';
		//get index id for getting historical month Label values and set into x axis 
		hisMon1_rowId = gridObj.getColIndexById("hispomon01");
		hisMon2_rowId = gridObj.getColIndexById("hispomon02");
		hisMon3_rowId = gridObj.getColIndexById("hispomon03");
		hisMon4_rowId = gridObj.getColIndexById("hispomon04");
		hisMon5_rowId = gridObj.getColIndexById("hispomon05");
		hisMon6_rowId = gridObj.getColIndexById("hispomon06");
	}

	var loader = dhtmlxAjax.postSync('/gmTTPByVendorSummary.do?method=fetchTTPbyVendorFooterChart&inputString='
					+ inputStr
					+ '&strChartby='
					+ strChartby
					+ '&ramdomId='
					+ new Date().getTime() + '&' + fnAppendCompanyInfo());

	fnFooterChartByFilterResponse(loader);
}
// ThisFunction used for get the response of fnFooterChartByFilter function and
// show the Chart based on total filter
function fnFooterChartByFilterResponse(loader) {

	response = loader.xmlDoc.responseText;
	
	document.all.chartDivimg.src = '/images/minus.gif';
	document.all.chartHdrDiv.style.display = 'block';
	//get index id for getting the forecast month Label values and set into x axis 
	   foreCastMon1_rowId = gridObj.getColIndexById("forecastmon01");
	   foreCastMon2_rowId = gridObj.getColIndexById("forecastmon02");
	   foreCastMon3_rowId = gridObj.getColIndexById("forecastmon03");
	   foreCastMon4_rowId = gridObj.getColIndexById("forecastmon04");
	   foreCastMon5_rowId = gridObj.getColIndexById("forecastmon05");
	   foreCastMon6_rowId = gridObj.getColIndexById("forecastmon06");
	 
	var categoryValue='[{category:[{"label":"'+gridObj.getColumnLabel(hisMon1_rowId)+'"},{"label":"'+gridObj.getColumnLabel(hisMon2_rowId)+'"},'
                                 +'{"label":"'+gridObj.getColumnLabel(hisMon3_rowId)+'"},{"label":"'+gridObj.getColumnLabel(hisMon4_rowId)+'"},'
								 +'{"label":"'+gridObj.getColumnLabel(hisMon5_rowId)+'"},{"label":"'+gridObj.getColumnLabel(hisMon6_rowId)+'"},'
								 +'{"label":"'+gridObj.getColumnLabel(foreCastMon1_rowId)+'"},{"label":"'+gridObj.getColumnLabel(foreCastMon2_rowId)+'"},'
								 +'{"label":"'+gridObj.getColumnLabel(foreCastMon3_rowId)+'"},{"label":"'+gridObj.getColumnLabel(foreCastMon4_rowId)+'"},'
								 +'{"label":"'+gridObj.getColumnLabel(foreCastMon5_rowId)+'"},{"label":"'+gridObj.getColumnLabel(foreCastMon6_rowId)+'"}]}]';

	FusionCharts.ready(function() {
		var cat= new Array();
		var dat= new Array();
		  var myChart = new FusionCharts({
		    type: "stackedcolumn2dline",
		    renderAt: "chartHdrDiv",
		    width: "100%",
		    height: "400px",
		    dataFormat: "json",
		    legendPosition:"RIGHT",
		    dataSource: {
		    	  chart: {
		    		    showvalues: "0",
		    		    caption: "Total Chart",
		    		    showhovereffect: "1",
		    		    showsum: "1",
		    		    theme: "fusion",
		    		    labelBinSize: "0",
		    		    interactiveLegend:"1",
		    		    paletteColors:"#ff0000,#FFA500,#ffbb90,#ffff00,#66bbbb,#019145,#000000,#000000",
                        usePlotGradientColor: "0"
		    		  },
		    		  categories : eval(categoryValue),
					  dataset : eval(response)
		    } 
		  }).render();
		});
}


//This Function is used to approve the vendor when click approve button
function fnRollback(){
	var ttpId = document.frmTTPByVendorRpt.ttpId.value;
	var ttpMonth = document.frmTTPByVendorRpt.ttpMonth.value;
	var ttpYear = document.frmTTPByVendorRpt.ttpYear.value;
	var vendorId = document.frmTTPByVendorRpt.vendorId.value;
	if (confirm("Are you sure you want to Rollback ?")) {
	var loader = dhtmlxAjax.postSync('/gmTTPByVendorSummary.do?method=rollbackTTPbyVendorSummary&ttpId='
			+ ttpId
			+ '&ttpMonth='
			+ ttpMonth
			+ '&ttpYear='
			+ ttpYear
			+ '&vendorId=' + vendorId + '&'+fnAppendCompanyInfo()+'&ramdomId=' + Math.random());
	fnStartProgress();
	fnRollbackResponse(loader);
	}else{
		return false;
	}
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	
}
//this  call back function used to show error message and success message
function fnRollbackResponse(loader) {
	var response = loader.xmlDoc.responseText;
	document.getElementById("succesMsg").style.display = "none";
	fnStopProgress();
	//response Format=N##ErroMessage
	//Split as flag and Message
	if (response!=''){ 
	var b = response;
	var temp =  new Array();
	temp = b.split('##');
	strFlag  =  temp[0];
	strMsg =  temp[1];
	if(strFlag=='Y'){
				Error_Details(strMsg);
				if (ErrorCount > 0) {
					Error_Show();
					Error_Clear();
					return false;
				}
		}else{
			//fnSummaryLoad();
			document.getElementById("rolledBackMsg").style.display = "table-row";
			document.frmTTPByVendorRpt.BTN_APPROVE.disabled=false;
			document.frmTTPByVendorRpt.BTN_ROLL_BACK.disabled=true;
		}
}	
}