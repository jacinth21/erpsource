function fnOnPageLoad(){
	document.getElementById("InfoSection").style.display = 'none';
	var lotNum = document.all.lotNum.value.toUpperCase();	
	if(lotNum != ''){
		document.getElementById("InfoSection").style.display = 'block';
		fnGo('Btn_Go');
	}
}

// To initialize the grid
function initGridData(divRef,gridData){
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.init();
	gObj.loadXMLString(gridData);	
	return gObj;	
}

// When click on Go button
function fnGo(obj){
	if(obj != undefined && obj != ''){
		if(event.keyCode == 13 || obj.name == 'Btn_Go' || obj == 'Btn_Go'){
			fnStartProgress();
			var lotNum = document.all.lotNum.value.toUpperCase();
			var loader;
			var status,responseText, data, datalength;
			document.frmLotCodeRpt.strOpt.value = 'Load';
			document.all.lotNum.value = lotNum;
			if(lotNum != ''){
				document.getElementById("InfoSection").style.display = 'block';
				fnloadDonorInfo(lotNum);// To load the donor information
				fnloadProdInfo(lotNum);// To load the Product information
				fnlotInfo(lotNum);// To load the lot information
				fnlotRecInfo(lotNum); // To load receiving information
				fnOpenTransaction(lotNum);// To load open transactions
				fnInvInformation(lotNum);// To load inventory informations
				fnTxnHistory(lotNum);// To load transaction history
			}else{
				Error_Details(message[10500]);
				Error_Show();
				Error_Clear();
				fnStopProgress();
				return false;
			}
			
			// used below codes to stop reloading the page after scanning the id
			if(!e) var e = window.event;
	
	        e.cancelBubble = true;
	        e.returnValue = false;	
	
	        if (e.stopPropagation) {
	                e.stopPropagation();
	                e.preventDefault();
	        }
		}
	}
	
}
//To Load Lot Error Details
function fnLoadLotErrorDetail() {
	var lotNum = document.frmLotCodeRpt.lotNum.value;
	var strFlag = 'Y';
	windowOpener(
			'/gmLotErrorReport.do?method=loadLotReport&lotNumber=' + encodeURIComponent(lotNum)+'&strFlag='+strFlag+"&companyInfo="+companyInfoObj, "Search",
			"resizable=yes,scrollbars=yes,top=200,left=250,width=1050,height=670");
}
// To load the donor information
function fnloadDonorInfo(lotNum){
	var loader;
	var status,responseText, data, datalength;
	loader=dhtmlxAjax.getSync("/gmLotCodeRptAction.do?method=loadLotCodeDonorInfo&lotNum=" + encodeURIComponent(lotNum)+ "&companyInfo="+companyInfoObj+ "&ramdomId=" + Math.random());
 	status = loader.xmlDoc.status;
 	
	if(status == 200){  //On success status parsing the response values.
		responseText = loader.xmlDoc.responseXML;	
		data = responseText.getElementsByTagName("data");
		datalength = data[0].childNodes.length;
		
		if (responseText != null && datalength != 0) {
			parseDonorInfo(responseText);
		}
	}
}
//To load the Product information
function fnloadProdInfo(lotNum){
	var loader;
	var status,responseText, data, datalength;
	loader=dhtmlxAjax.getSync("/gmLotCodeRptAction.do?method=loadLotCodeProdInfo&strOpt=Load&lotNum=" + encodeURIComponent(lotNum)+ "&companyInfo="+companyInfoObj+ "&ramdomId=" + Math.random());
 	status = loader.xmlDoc.status;
 	
	if(status == 200){  //On success stauts parsing the response values.
		responseText = loader.xmlDoc.responseXML;	
		data = responseText.getElementsByTagName("data");
		datalength = data[0].childNodes.length;
		if (responseText != null && datalength != 0) {
			parsePdtInfo(responseText);
		}
	}
}
//To load the lot information
function fnlotInfo(lotNum){
	var loader;
	var status,responseText, data, datalength;
	loader=dhtmlxAjax.getSync("/gmLotCodeRptAction.do?method=loadLotCodeInfo&lotNum=" + encodeURIComponent(lotNum)+ "&companyInfo="+companyInfoObj+ "&ramdomId=" + Math.random());
	status = loader.xmlDoc.status;
 	
	if(status == 200){  //On success stauts parsing the response values.
		responseText = loader.xmlDoc.responseXML;	
		
		data = responseText.getElementsByTagName("data");
		datalength = data[0].childNodes.length;
		
		if (responseText != null && datalength != 0) {
		       parseLotInfo(responseText);
		}
		
	}
}
//To load inventory informations
function fnlotRecInfo(lotNum){
	var loader;
	var response;
	dhtmlxAjax.get('/gmLotCodeRptAction.do?method=loadLotCodeRecInfo&lotNum=' + encodeURIComponent(lotNum)+ "&hscreen="+screen+ "&companyInfo="+companyInfoObj+ '&ramdomId='+Math.random(),function(loader){
		if (loader.xmlDoc.responseText != null){
			response = loader.xmlDoc.responseText;
			gridObj = initGridData('RecDataGrid',response);
			document.getElementById("DivRecNoDataMessage").style.display = (response.indexOf("<cell>") != '-1') ? 'none':'block';
			gridObj.enableAutoHeight(true,500,true);// To enable auto height for grid
			
		}
	});
 	
}
//To load open transactions
function fnOpenTransaction(lotNum){
	var loader;
	var response;
	dhtmlxAjax.get('/gmLotCodeRptAction.do?method=loadLotCodeOpentxn&lotNum=' + encodeURIComponent(lotNum)+ "&companyInfo="+companyInfoObj+ '&ramdomId='+Math.random(),function(loader){
		if (loader.xmlDoc.responseText != null){
			response = loader.xmlDoc.responseText;
			gridObj = initGridData('TxnDataGrid',response);
			document.getElementById("DivTxnNoDataMessage").style.display = (response.indexOf("<cell>") != '-1') ? 'none':'block';
			gridObj.enableAutoHeight(true,500,true);// To enable auto height for grid
		}
	});	 
}
//To load inventory informations
function fnInvInformation(lotNum){
	var loader;
	var response;
	dhtmlxAjax.get('/gmLotCodeRptAction.do?method=loadLotCodeInvInfo&lotNum=' + encodeURIComponent(lotNum)+ "&companyInfo="+companyInfoObj+ '&ramdomId='+Math.random(),function(loader){
		if (loader.xmlDoc.responseText != null){
			response = loader.xmlDoc.responseText;
			gridObj = initGridData('InvDataGrid',response);
			document.getElementById("DivInvNoDataMessage").style.display = (response.indexOf("<cell>") != '-1') ? 'none':'block';
			gridObj.enableAutoHeight(true,500,true);// To enable auto height for grid
		}
	});		 
 	
}
//To load transaction history
function fnTxnHistory(lotNum){
	var loader;
	var response;
	dhtmlxAjax.get('/gmLotCodeRptAction.do?method=loadLotCodeTxnHistory&lotNum=' + encodeURIComponent(lotNum)+ "&companyInfo="+companyInfoObj+ '&ramdomId='+Math.random(),function(loader){
		if (loader.xmlDoc.responseText != null){
			response = loader.xmlDoc.responseText;
			gridObj = initGridData('HistDataGrid',response);
			document.getElementById("DivHistNoDataMessage").style.display = (response.indexOf("<cell>") != '-1') ? 'none':'block';
			gridObj.enableAutoHeight(true,500,true);// To enable auto height for grid
		}
		fnStopProgress();
	});		
}

//To assign the values getting from ajax call to the corresponding fields
function parseDonorInfo(xmlDoc) 
{
	var strforresearch, strdonorno, strage, strinternaluse, strsex, strdhrid, strvid;
	var pnum = xmlDoc.getElementsByTagName("pnum");
	var pnumlen = pnum[0].childNodes.length;
	
	if(pnumlen != '0'){
		/*To support Edge browser to fetch data PC-3659
		from var forresearch = xmlDoc.getElementsByTagName("forresearch") changes to var forresearch = pnum[0].getElementsByTagName("forresearch")[0]*/ 
		var forresearch = pnum[0].getElementsByTagName("forresearch")[0];
		var forresearchlen = forresearch.length;
		var donorno = pnum[0].getElementsByTagName("donorno")[0];
		var donornolength = donorno.length;
		var age = pnum[0].getElementsByTagName("age")[0];  
		var agelength = age.length;
		var internationaluse = pnum[0].getElementsByTagName("internationaluse")[0];
		var intUselength = internationaluse.length;
		var sex = pnum[0].getElementsByTagName("sex")[0];
		var sexlength = sex.length;
		var dhrid = pnum[0].getElementsByTagName("dhrid")[0];
		var dhridlen = dhrid.length;
		var vid = pnum[0].getElementsByTagName("vid")[0];
		var vidlen = vid.length;
		
		var forresearchVal = forresearch.textContent != undefined? forresearch.textContent : forresearch.text;
		strforresearch = validateObject(forresearch) && forresearchlen!=0? forresearchVal: '';
		document.getElementById("forResearch").innerHTML =strforresearch;
		
		var dhridVal = dhrid.textContent != undefined? dhrid.textContent : dhrid.text;
		strdhrid = validateObject(dhrid) && dhridlen!=0? dhridVal: '';
		
		var vidVal = vid.textContent != undefined? vid.textContent : vid.text;
		strvid = validateObject(vid) && vidlen!=0? vidVal: '';
		
		var donornoVal = donorno.textContent != undefined? donorno.textContent : donorno.text;
		strdonorno = validateObject(donorno) && donornolength!=0? donornoVal: '';
		if(screen != 'popup'){
			document.getElementById("donorNum").innerHTML = document.getElementById("donorNum").innerHTML ='<a href="javascript:fnCallDonor();">'+strdonorno+'</a>';			
		}else{
			document.getElementById("donorNum").innerHTML = document.getElementById("donorNum").innerHTML =strdonorno;	
		}
		if(validateObject(document.all.hdonorNumber)){
			document.all.hdonorNumber.value = strdonorno;
		}
		
		var ageVal = age.textContent != undefined? age.textContent : age.text;
		strage = validateObject(age) && agelength!=0? ageVal: '';
		document.getElementById("donorAge").innerHTML =strage;
		
		var internaluseVal = internationaluse.textContent != undefined? internationaluse.textContent : internationaluse.text;
		strinternaluse = validateObject(internationaluse) && intUselength!=0? internaluseVal: '';
		document.getElementById("intUse").innerHTML =strinternaluse;
	
		var sexVal = sex.textContent != undefined? sex.textContent : sex.text;
		strsex = validateObject(sex) && sexlength!=0? sexVal: '';
		document.getElementById("donorSex").innerHTML =strsex;
	}else{
		fnResetDonorInfo();
	}
}

// To populate Product information
function parsePdtInfo(xmlDoc){
	var strpartNum, strpartdesc, strpsize, strcpclient;
	var pnum = xmlDoc.getElementsByTagName("pnum");
	var pnumlen = pnum[0].childNodes.length;
	if(pnumlen != '0'){
	/*To support Edge browser to fetch data PC-3659
		from var partnum = xmlDoc.getElementsByTagName("partnum"); changes to var partnum = pnum[0].getElementsByTagName("partnum")[0]*/
		var partnum = pnum[0].getElementsByTagName("partnum")[0];
		var partnumlen = partnum.length;
		var pdesc = pnum[0].getElementsByTagName("pdesc")[0];
		var pdesclen = pdesc.length;
		var psize = pnum[0].getElementsByTagName("psize")[0];
		var psizelen = psize.length;
		var cpclient = pnum[0].getElementsByTagName("cpclient")[0];
		var cpclientlen = cpclient.length;
		
		var partNumVal = partnum.textContent != undefined? partnum.textContent : partnum.text;
		strpartNum = validateObject(partnum) && partnumlen!=0? partNumVal: '';
		if(screen != 'popup'){
		document.getElementById("partnum").innerHTML = "<a href=javascript:fnPartNumSetup('"+strpartNum+"');>"+strpartNum+"</a>";
		}else{
		document.getElementById("partnum").innerHTML = strpartNum;	
		}
		var partdescVal = pdesc.textContent != undefined? pdesc.textContent : pdesc.text; 
		strpartdesc = validateObject(pdesc) && pdesclen!=0? partdescVal: '';
		document.getElementById("pdesc").innerHTML =strpartdesc;
		
		var psizeVal = psize.textContent != undefined? psize.textContent : psize.text; 
		strpsize = validateObject(psize) && psizelen!=0? psizeVal: '';
		document.getElementById("psize").innerHTML =strpsize;
		
		var partdescVal = cpclient.textContent != undefined? cpclient.textContent : cpclient.text; 
		strcpclient = validateObject(cpclient) && cpclientlen!=0? partdescVal: '';
		document.getElementById("cpclient").innerHTML =strcpclient;
	}else{
		fnPdtInfo();
	}

}
// To populate Lot information
function parseLotInfo(xmlDoc){
	var strexpdate, strstatus, strerrflag, strcurrplant;
	var pnum = xmlDoc.getElementsByTagName("pnum");
	var pnumlen = pnum[0].childNodes.length;
	if(pnumlen != '0'){
		var expdate = pnum[0].getElementsByTagName("expdate")[0];
		var expdatelen = expdate.length;
		var currstatus = pnum[0].getElementsByTagName("currstatus")[0];
		var statuslen = currstatus.length;
		var errflag = pnum[0].getElementsByTagName("errflag")[0];
		var errflaglen = errflag.length;
		var currplant = pnum[0].getElementsByTagName("currplant")[0];
		var currplantlen = currplant.length;
		
		var expdateVal = expdate.textContent != undefined? expdate.textContent : expdate.text; 
		strexpdate = validateObject(expdate) && expdatelen!=0? expdateVal: '';
		document.getElementById("expDate").innerHTML =strexpdate;
		
		var statusVal = currstatus.textContent != undefined? currstatus.textContent : currstatus.text; 
		strstatus = validateObject(currstatus) && statuslen!=0? statusVal: '';
		document.getElementById("currStatus").innerHTML =strstatus;
		
		var currplantVal = currplant.textContent != undefined? currplant.textContent : currplant.text; 
		strcurrplant = validateObject(currplant) && currplantlen!=0? currplantVal: '';
		document.getElementById("currPlant").innerHTML =strcurrplant;
		
		var errflagVal = errflag.textContent != undefined? errflag.textContent : errflag.text;
		strerrflag = validateObject(errflag) && errflaglen!=0? errflagVal: '';
		if(screen != 'popup'){
			document.getElementById("exceptionStatus").innerHTML = "<a href=javascript:fnLoadLotErrorDetail();>"+strerrflag+"</a>";
			}else{
			document.getElementById("exceptionStatus").innerHTML = strerrflag;	
			}
		
	}
}

//To open the popup when click on donor number icon
function fnCallDonor(){
	var donorid = document.all.hdonorNumber.value;
	windowOpener("/gmDonorLotRptAction.do?method=loadDonorLotInfo&hscreen=popup&donorId="+donorid,"PrntInv","resizable=yes,scrollbars=yes,top=200,left=300,width=1250,height=500");
}

// To open part number setup screen
function fnPartNumSetup(pnum){
	windowOpener("/GmPartNumServlet?hOpt=FETCHCONTAINER&hAction=Lookup&Txt_PartNum="+encodeURIComponent(pnum), "","resizable=yes,scrollbars=yes,top=250,left=300,width=1050,height=750");
}

// To open Receive bulk shipment screen
function fnOpenRSScreen(rsid){ 
	windowOpener("/GmPageControllerServlet?strPgToLoad=/gmPOBulkReceive.do?method=loadBatchshipment&strOpt=loadRS&strMode=FETCHCONTAINER&rsNumber="+rsid, "","resizable=yes,scrollbars=yes,top=250,left=300,width=1050,height=750");
}
// To open DHR view
function fnOpenDHRView(dhrid, vendorid){
	windowOpener("/GmPOReceiveServlet?hAction=ViewDHR&hDHRId="+dhrid+"&hVenId="+vendorid, "","resizable=yes,scrollbars=yes,top=250,left=300,width=800,height=750");
	
}

// To open sales adjustment
function fnOpenPicSlip(transID, txntype, type){

	if(txntype == 'Return'){
        windowOpener('/GmPrintCreditServlet?hAction=PRINT&hRAID='+transID,"Returns","resizable=yes,scrollbars=yes,top=150,left=200,width=750,height=450");
   }else if( type == '2532' || txntype == 'Order' || txntype == 'Reserved' || txntype == 'Sales'){  // 2532: Bill Only Sales Consignment
        windowOpener('/GmOrderItemServlet?hAction=PICSlip&hId='+transID,"Pack","resizable=yes,scrollbars=yes, top=150,left=150,width=760,height=610");
   }else if(txntype == 'Consignment' || type == '4115'){ //4115: Quarantine To Scrap
        windowOpener("/GmConsignItemServlet?hAction=PicSlip&hConsignId="+transID+"&ruleSource="+type,"SetPic","resizable=yes,scrollbars=yes,top=150,left=150,width=760,height=600");
   }else if(type == '103932' || type == '4000645' || type == '120621'){ //103932: Part Redesignation; 4000645: Part Redesignation
	   windowOpener("/gmPartredesignation.do?method=generatePicSlip&strTxnIds="+transID+"&txntype=103932&ruleSource=103932&refId="+transID,"PTRTPicSlip","resizable=yes,scrollbars=yes,top=150,left=150,width=850,height=650");
   }
   // PMT-38026 Bug Fix
   else if(type==102909 && transID.indexOf('TFCN') != -1) { //for PMT-29681
	   windowOpener("/GmConsignSetServlet?hAction=LoadConsign&hId="+transID,"Con1","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600"); 
   }
   else if(type==102909 && transID.indexOf('TFRA') != -1) { //for PMT-29681
	   windowOpener("/GmReportCreditsServlet?hAction=PrintVersion&hId="+transID,"Con1","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600"); 
   }
   else{
		windowOpener("/gmItemControl.do?haction=PicSlip&hConsignId="+transID+"&txntype="+type+"&ruleSource="+type+"&refId=","SetPic","resizable=yes,scrollbars=yes,top=150,left=150,width=760,height=600");
	}
}

function fnPrintScreen(){
	window.print();
}

// To reset the donor information
function fnResetDonorInfo(){
	document.getElementById("forResearch").innerHTML = '';
	document.getElementById("donorNum").innerHTML = '';
	document.getElementById("donorAge").innerHTML = '';
	document.getElementById("intUse").innerHTML = '';
	document.getElementById("donorSex").innerHTML = '';
}

// To reset the product information
function fnPdtInfo(){
	document.getElementById("partnum").innerHTML = '';
	document.getElementById("pdesc").innerHTML = '';
	document.getElementById("psize").innerHTML = '';
	document.getElementById("cpclient").innerHTML = '';
}

// To open the window to display the comments 
function fnOpenComments(txnid){
	windowOpener("/GmCommonLogServlet?hType=4000317&hID=" +txnid+"&hHideComment=Y&hAlignSubmit=L&hScreen=LotCodeRpt","PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=400");
}