var gridObj;
function fnCallEditReturn(val)
{
	if (val == 'txt')
	{
		val = document.frmReturnReport.Txt_RAID.value;
		fnValidateTxtFld('Txt_RAID',message[10185]);
	}
	else
	{
	 document.frmReturnReport.Txt_RAID.value = val;
	}
	
	document.frmReturnReport.action = "/GmReturnReceiveServlet";
	document.frmReturnReport.hRAId.value = val;	
	document.frmReturnReport.hAction.value = "Reload";
	document.frmReturnReport.hMode.value="Process";
	fnSubmit();
}

function fnReceiveReturns(val)
{
	document.frmReturnReport.action = "/GmReturnDetailsServlet";
	document.frmReturnReport.hRAId.value = val;
	document.frmReturnReport.hAction.value = "Report";
	document.frmReturnReport.submit();
}


function fnLog(id)
{
	windowOpener("/GmCommonLogServlet?hType=1216&hID="+id,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
}


function fnGo()
{
	var datetype    = document.frmReturnReport.Cbo_DateType.value;
	var objFromDate = document.frmReturnReport.Txt_FrmDate;
	var objToDate   = document.frmReturnReport.Txt_ToDate;
	var objrefId       = document.frmReturnReport.Txt_RefID;
	
	if((datetype == 0) && ((objFromDate.value != '') || (objToDate.value != ''))){
		Error_Details(message_operations[312]);
	}else if((datetype != 0) && ((objFromDate.value == '') && (objToDate.value == '') || (((objFromDate.value != '') && (objToDate.value == '')) || ((objFromDate.value == '') && (objToDate.value != ''))))){
		Error_Details(message_operations[313]);
	}
	if(datetype != 0 ){
		if(objFromDate.value != ""){
			CommonDateValidation(objFromDate, format, Error_Details_Trans(message_operations[300],format));
		}
		if(objToDate.value != ""){
			CommonDateValidation(objToDate, format, Error_Details_Trans(message_operations[301],format));
		}
		if(objFromDate.value != "" && objToDate.value != ""){
			if(dateDiff(objFromDate.value,objToDate.value,format) < 0){
				Error_Details(message_operations[314]);
			}	
		}
	}	
	if (ErrorCount > 0){
			Error_Show();
			Error_Clear();
			return false;
	}
	objrefId.value=TRIM(objrefId.value);
	fnStartProgress('Y');
	document.frmReturnReport.hAction.value = "Report";
	document.frmReturnReport.submit();
}

function fnSubmit()
{
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	document.frmReturnReport.submit();
}

function fnViewReturns(val)
{
	windowOpener('/GmPrintCreditServlet?hAction=VIEW&hRAID='+val,"Returns","resizable=yes,scrollbars=yes,top=150,left=200,width=660,height=400");
}

function fnReconfigReturn(val)
{   
	if (val == 'txt')
		{
			val = document.frmReturnReport.Txt_RAID.value;
			fnValidateTxtFld('Txt_RAID',message[10185]);
		}		
 	document.frmReturnReport.action = "/GmReturnReconfRecServlet";
	document.frmReturnReport.hParentRAId.value = val;
	document.frmReturnReport.hAction.value = "LoadReconf";
	fnSubmit();
}

function fnPrintRA(val){
	windowOpener('/GmPrintCreditServlet?hAction=PRINT&hRAID='+val,"Returns","resizable=yes,scrollbars=yes,top=150,left=200,width=1050,height=850");
}

/*
function fnHelp()
{
  windowOpener("/GmReturnSummaryHelp.html");	
}*/


function fnSetValues()
{
	document.frmReturnReport.Cbo_DateType.value=stDtTyp;
	fnCallRep();
	document.frmReturnReport.Cbo_RepName.value=stRepID;
	if (document.frmReturnReport.Cbo_RepName.value ==''){
		document.frmReturnReport.Cbo_RepName.value =0;
	}
}

function fnCallRep(){
	var objRep = document.frmReturnReport.Cbo_RepName;
	var objval = document.frmReturnReport.Cbo_DistId.value;
	objRep.options.length = 0;
	objRep.options[0] = new Option("[Choose One]","0");	
	var j = 0;			
	for (var i=0;i<repLen;i++)
	{
		arr = eval("RepArr"+i);
		arrobj = arr.split(",");
		id = arrobj[0];		
		nameVal = arrobj[1];   //  nameVal = arrobj[1] instead of name = arrobj[1] changes in PC-3656 Edge Browser fixes for Logistics Module
		did = arrobj[3];
		if (did == objval)
		{			
			j++;			
			objRep.options[j] = new Option(nameVal,id);
		}			
	}		
}
function fnOnPageLoad(){
	
	var raId = document.frmReturnReport.Txt_RAID.value;
	var parentraId = document.frmReturnReport.Txt_ParentRAID.value;
	var setId = document.frmReturnReport.Txt_SetID.value;
	var partnumId = document.frmReturnReport.Txt_PartNum.value
	var changeReason = document.all.hChgReason.value;
		if (changeReason != '') {
			document.all.Cbo_ChangeReason.value = changeReason;
		}
		//Load default Returns Summary once load the RAID from process transaction screen
		if(strPrcessFl=='YES'){
			fnGo();
		}
	
	if (objGridData != ''){
		gridObj = initGridData('employeedata',objGridData);
		fnLoadDrpDwn(); // To load the distributor/ account drop down based on the type
	}

	if((objGridData.indexOf("cell") == -1) && (refID !='') && (raId.trim()=='')&& (parentraId.trim()=='')&& (setId.trim()=='')&& (partnumId.trim()=='')){
		Error_Details(message_operations[733]);
	}
	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
}
}

function initGridData(divRef,gridData)
{
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableTooltips("false,true,true,true,true,true,true,true,true,true,true,true,true");	
	gObj.init();
	gObj.loadXMLString(gridData);
	return gObj;
}

function fnDownloadXLS(){
	fnStartProgress('Y');
	gridObj.toExcel('/phpapp/excel/generate.php');
	fnStopProgress();
}
// To load the distributor/ account drop down based on the type
function fnLoadDrpDwn(){
	var type = document.all.Cbo_Type.value;
	
	if(type == '3304'){//3304: Account Consignment - Items
		var objDistBlk = eval('document.all.dist_blk.style');
  		objDistBlk.display = 'none';
  		var objAccountBlk = eval('document.all.acct_blk.style');
  		objAccountBlk.display = 'block';
  		document.all.Cbo_DistId.value = '0';
	}else{
		var objDistBlk = eval('document.all.dist_blk.style');
  		objDistBlk.display = 'block';
  		var objAccountBlk = eval('document.all.acct_blk.style');
  		objAccountBlk.display = 'none';
  		document.all.Cbo_AcctId.value = '0';
	}
}

//This function is used to call the GmCommonCancelServlet to change the RA reason to QA Evaluvation
function fnLoadRAReport(){
	
	var changeReason = document.frmReturnReport.Cbo_ChangeReason.value;
	
	var radio_rowId = gridObj.getColIndexById("rad");
	var raId = '';
	var rad = gridObj.getCheckedRows(radio_rowId).split(",");
	if(rad == ''){
		Error_Details(message_operations[315]);
	}else{
		for (var i=0;i<rad.length;i++){
			raId = raId + gridObj.cellById(rad[i], 5).getValue()+ "," ;
		}	
	
		if(changeReason == '105020'){//105020-Convert To QA Evaluvation
			if(raId != ""){
				raId = raId.substring(0,(raId.length-1));
			}
			document.frmReturnReport.hCancelType.value = "RQA";
			document.frmReturnReport.action = "/GmCommonCancelServlet";
	        document.frmReturnReport.hTxnId.value = raId;
			document.frmReturnReport.hAction.value = "Load";	
			fnStartProgress();
			document.frmReturnReport.submit();
		}
		
		else if (changeReason == '26240409'){//26240409-Convert To Field Corrective Action
			if(raId != ""){
				raId = raId.substring(0,(raId.length-1));
			}
			document.frmReturnReport.hCancelType.value = "FCA";
			document.frmReturnReport.action = "/GmCommonCancelServlet";
	        document.frmReturnReport.hTxnId.value = raId;
			document.frmReturnReport.hAction.value = "Load";	
			fnStartProgress();
			document.frmReturnReport.submit();
		}
		
		
		else{
			Error_Details(message_operations[316]);
			
		}
	}
			
	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}
}

