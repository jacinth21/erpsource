function fnReLoad() {
	var partNM = document.frmControlNumberException.partNumber.value.toUpperCase;
	var partDescp = document.frmControlNumberException.partDesc.value; 
	var cntrlNM = document.frmControlNumberException.controlNM.value.toUpperCase;
	if(partNM =='' && partDescp =='' && cntrlNM =='')
	{
		Error_Details(message_operations[111]);	
	}
 	//PC-4543 Upper Case Conversion Issue
	if(document.frmControlNumberException.partNumber != undefined){
    	if(document.frmControlNumberException.partNumber.value != undefined && document.frmControlNumberException.partNumber.value != ""){
    	document.frmControlNumberException.partNumber.value = TRIM(document.frmControlNumberException.partNumber.value.toUpperCase());
    	}
    }
	if(document.frmControlNumberException.controlNM != undefined){
    	if(document.frmControlNumberException.controlNM.value != undefined && document.frmControlNumberException.controlNM.value != ""){
    	document.frmControlNumberException.controlNM.value = TRIM(document.frmControlNumberException.controlNM.value.toUpperCase());
    	}
    }	
	var toDate = document.getElementById('toDt').value;
	var fromDate = document.getElementById('fromDt').value;
	if (toDate != null && fromDate != null ) {
		if(dateDiff(toDate,fromDate,format)>0){
	        //Manufatured date cannot be a Futured date.
			Error_Details(message_operations[112]);	
	       }
	}
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	} 
		//document.form.strOpt.value="reload";
		document.frmControlNumberException.strOpt.value="reload"; //PC-4543 Upper Case Conversion Issue
		fnStartProgress('Y');
		document.getElementById("form").submit();
}
function fnOnPageLoad() {
	if(document.frmControlNumberException.partNumber != undefined && document.frmControlNumberException.partNumber != null){
	   document.frmControlNumberException.partNumber.focus();
	}
	if(gridObjData!="")
	{
		   	mygrid = initGrid('dataGridDiv',gridObjData);
		   	//mygrid.attachHeader('#rspan,#text_filter,#text_filter,#select_filter,#rspan,#select_filter,#rspan');
	}
}

function fnSetupLoad(){
	if (document.frmControlNumberException.leftLink.value == "true"){
		document.getElementById('btn_close').style.display = "none";
	}
}
function fnSubmit(){
	var frm = document.frmControlNumberException;
	var stropt = '';
	fnValidate();
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	else{
		frm.partNM.disabled = false;
		stropt = frm.strOpt.value;
		if(stropt == null || (stropt !='saveExpDtOverride' && stropt !='loadExpDtOverride')){
		frm.strOpt.value='save';
		}		
		fnStartProgress('Y');
		frm.submit();
	}
}

function fnValidate()
{
	fnValidateTxtFld('partNM',message_operations[374]); 
	fnValidateTxtFld('controlNM',message_operations[375]);
	fnValidateTxtFld('txt_LogReason',message_operations[151]);	
}

function fnClose() {
	window.close();
}

function fnOpenLog(id,type)
{
	windowOpener("/GmCommonLogServlet?companyInfo="+companyInfoObj+"&hType="+type+"&hID="+id,"ConsignLog","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=300");
}

//Function to forward the control to Part Control Number Stup Screen
function fnEditLoad(partCNId,cformat){
	var actionURL = "/gmPartControlSetup.do?companyInfo="+companyInfoObj+"&method=loadPartControlInfo&hAccess=Y&hScreenType=lotNumberOverrideRpt&partCNId="+partCNId+'&cnumfmt='+cformat;
	 document.frmControlNumberException.action= actionURL;
     document.frmControlNumberException.submit();
}
//Press Enter key to load control override screen details
function fnLoadControls(){
	if(event.keyCode == 13){
		fnReLoad();
	}
}

//function to open popup when click on "D" icon
function fnCallDonor(donorNM,vendorId){	
          //fnValidateTxtFld('donorNM','Donor #');
          if(ErrorCount>0){
              Error_Show();
             Error_Clear();
             return false;
          }
          windowOpener("/gmDonorInfo.do?companyInfo="+companyInfoObj+"&method=fetchDonorParamDtls&strOpt=load&screenType=recShip&submitAccess=lotNumberOverrideRpt&hAccess=N&FORMNAME=frmDonorInfo&hDonorNum="+donorNM+"&hVendorId="+vendorId,"PrntInv","resizable=yes,scrollbars=yes,top=300,left=550,width=585,height=350");
}