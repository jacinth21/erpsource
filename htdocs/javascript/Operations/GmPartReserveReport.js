//This function is used to initiate grid
var gridObj;
function fnOnPageLoad(){ 
	if(objGridData.value != ''){ 
		gridObj = initGridData('dataGridDiv',objGridData);		
	}	
}
function initGridData(divRef,gridData){
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableTooltips("true,true,true,true,true,true,true,true,true,true,true,true,true,true");
	gObj.attachHeader('#text_filter,#text_filter,#numeric_filter,#text_filter,#numeric_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#numeric_filter,#numeric_filter,#numeric_filter,#numeric_filter');
	gObj.init();
	gObj.enableLightMouseNavigation(true);
	gObj.loadXMLString(gridData);	
	return gObj;	
}
//Function to export excel
function fnExport(){	
	gridObj.toExcel('/phpapp/excel/generate.php');
}

//This function is used to activate Load button while pressing the enter key
function fnEnter(){
	if(event.keyCode == 13){
		fnLoad();
	}
}

function fnSubmit(){
	var transid = document.frmLotTrackReport.transId.value;	
	var inputString = "";
	var gridrows = gridObj.getChangedRows(",");
	var err_RemovedQty = '';
	var err_MoreAddQty = '';
	var err_ZeroAddQty = '';
	var err_MoreRemoveQty = '';
	var err_ZeroRemoveQty = '';
	var err_nanAddQty = '';	
	var totReservQty = 0;	
	var err_NegQty = '';
	var isErrFl = false;
	
	partInv_rowId  = gridObj.getColIndexById("partInvId");
	part_rowId = gridObj.getColIndexById("partNum");
	cntrl_rowId = gridObj.getColIndexById("controlnum");
	avilQty_rowId = gridObj.getColIndexById("availQty");
	reserveQty_rowId = gridObj.getColIndexById("reserveQty");
	addQty_rowId = gridObj.getColIndexById("addQty");
	removeQty_rowId = gridObj.getColIndexById("removeQty");
	
	gridObj.forEachRow(function(rowId) {
		part_inv = gridObj.cellById(rowId, partInv_rowId).getValue();
		part_num = gridObj.cellById(rowId, part_rowId).getValue();		
		cntrl_num = gridObj.cellById(rowId, cntrl_rowId).getValue();
		avail_qty = gridObj.cellById(rowId, avilQty_rowId).getValue();
		reserve_qty = gridObj.cellById(rowId, reserveQty_rowId).getValue();
		add_qty = gridObj.cellById(rowId, addQty_rowId).getValue();
		remove_qty = gridObj.cellById(rowId, removeQty_rowId).getValue();
		var objRegExp = /(^-?\d\d*$)/;
		// validating the ZERO for Add Quantity and Remove Quantity
		if((add_qty == 0 && add_qty != '') || (remove_qty == 0 && remove_qty != '')){
			err_ZeroAddQty = err_ZeroAddQty +"<br><B>"+part_num+" </B>- <B>"+cntrl_num+"</B>";			
		}
		// validating the More Add Quantity than Avail Quantity and Shelf Quantity
		if(parseInt(add_qty) != '' && parseInt(add_qty) > parseInt(avail_qty)){
			err_MoreAddQty = err_MoreAddQty +"<br><B>"+part_num+" </B>- <B>"+cntrl_num+"</B>";			
		}
		// validating other than the number for Add Quantity and Remove Quantity
		if((isNaN(add_qty) || isNaN(remove_qty))|| ((! add_qty== '' && !objRegExp.test(add_qty)) || (! remove_qty== '' && !objRegExp.test(remove_qty)))){
			err_nanAddQty = err_nanAddQty +"<br><B>"+part_num+" </B>- <B>"+cntrl_num+"</B>";			
		}
		// Adding Add Quantity to the Reserved Quantity. If Add Qty 1 and Reserve Qty is 0, then total becomes 1 
		if(add_qty != '' && add_qty != 0){
			reserve_qty = parseInt(reserve_qty) + parseInt(add_qty);
		}
		
		// validating the More Remove Quantity than Reserved Quantity
		if(parseInt(remove_qty) > parseInt(reserve_qty)){
			err_MoreRemoveQty = err_MoreRemoveQty +"<br><B>"+part_num+" </B>- <B>"+cntrl_num+"</B>";
		}
		// subtracting Remove Quantity from the Reserved Quantity. If Reserve Qty 1 and Reserve Qty is 2, then total becomes 1 		
		if(remove_qty != '' && remove_qty != 0){
			reserve_qty = parseInt(reserve_qty) - parseInt(remove_qty);
		}
		// Adding the total Reserved Quantity to the temp total Quantity to validate it should not exceeds the Total Release Qty
		totReservQty = parseInt(totReservQty) + parseInt(reserve_qty);
		// Forming the input string for reserving the parts.
		if((add_qty != '' && add_qty != 0) || (remove_qty != '' && remove_qty != 0)){
			inputString+= part_inv+'^'+part_num+'^'+transid+'^'+reserve_qty+'|';
		}
		if(add_qty < 0 || remove_qty < 0){ //Qty cannot be negative
			err_NegQty = err_NegQty +"<br><B>"+part_num+" </B>- <B>"+cntrl_num+"</B>";;
		}
		
   });
	if(err_nanAddQty != ''){
		isErrFl = true;
		Error_Details(Error_Details_Trans(message[5625],err_nanAddQty));
	}
	if(err_NegQty){
		isErrFl = true;
		Error_Details(Error_Details_Trans(message[5626],err_NegQty));
	}
	
	if(err_ZeroAddQty != ''){
		isErrFl = true;
		Error_Details(Error_Details_Trans(message[5627],err_ZeroAddQty));
	}
	/*if(err_ZeroRemoveQty != ''){
		isErrFl = true;
		Error_Details('Can not perform the Part Reserve action for following combination(s)-'+err_ZeroRemoveQty+' due to <B> Remove Qty</B> is ZERO[0]');
	}*/
	if(err_MoreAddQty != ''){
		isErrFl = true;
		Error_Details(Error_Details_Trans(message[5628],err_MoreAddQty));
	}
	if(err_MoreRemoveQty != ''){
		isErrFl = true;
		Error_Details(Error_Details_Trans(message[5629],err_MoreRemoveQty));
	}
	if(totReservQty > releaseQty){
		isErrFl = true;
		Error_Details(message[5630]);
	}
	if(inputString == '' && !isErrFl){
		Error_Details(message[5631]);
	}
	
	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}else{
		document.frmLotTrackReport.inputString.value = inputString;
		document.frmLotTrackReport.releaseQty.value = releaseQty;
		document.frmLotTrackReport.shelfQty.value = shelfQty;
		document.frmLotTrackReport.action = "/gmLotTrackReport.do?method=fetchPartReserveDetails&strOpt=ReservePart&partNum="+encodeURIComponent(part_num);
		document.frmLotTrackReport.Btn_Submit.disabled = true;
		document.frmLotTrackReport.submit();
	}
}

function fnClose(){	
	window.close();	
	fnRefreshParent();
}

function fnRefreshParent(){
	if(window.opener != null && !window.opener.closed){
		window.opener.location.reload();
	}
}
