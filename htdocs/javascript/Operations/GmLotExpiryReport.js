function fnOnload() {
		/*document.getElementById("loadExpired").style.display = '';
	    document.getElementById("load30Days").style.display = '';
		document.getElementById("load60Days").style.display = '';
		document.getElementById("load90Days").style.display = '';
		document.getElementById("loadOthers").style.display = '';*/
}
//This function is used to load data based on when load button pressed
function fnLoad(){
	
	var strPartNum = document.frmLotExpiryReport.partNum.value;
	var strProjectId = document.frmLotExpiryReport.projectId.value;
	var strSearch = document.frmLotExpiryReport.strPartLiteral.value;
	var strOpt = document.all.strOpt.value;
	if(document.frmLotExpiryReport.Chk_ActiveFl.checked){
		document.frmLotExpiryReport.Chk_ActiveFl.value = 'Y';
	}else{
		document.frmLotExpiryReport.Chk_ActiveFl.value = 'N';
	}
	var strExcQuar = document.frmLotExpiryReport.Chk_ActiveFl.value;
	if(strProjectId != '' || strPartNum != ''){
		if(strProjectId == ''){
			strProjectId = '0';
		}

/*		if (strPartNum.length < 3 && (strPartNum!='' && strPartNum != undefined)){
			Error_Details (message_operations[349]);
			Error_Show();
			Error_Clear();
			return false;		 
		}*/
	//	fnStartProgress();
		fnExpiredLots(strPartNum,strProjectId,strExcQuar,strSearch,strOpt);
		fnLotExpByThirtyDays(strPartNum,strProjectId,strExcQuar,strSearch,strOpt);
		fnLotExpBySixtyDays(strPartNum,strProjectId,strExcQuar,strSearch,strOpt);
		fnLotExpByNinetyDays(strPartNum,strProjectId,strExcQuar,strSearch,strOpt);
		fnLotExpOthers(strPartNum,strProjectId,strExcQuar,strSearch,strOpt);
		fnLotExpChart(strPartNum,strProjectId,strExcQuar,strSearch,strOpt);
	}else{
		Error_Details(message_operations[854]);
	}
	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}
}
//This function is used to get chart data based on tissue parts
function fnLotExpChart(strPartNumber,strProjectId,strExcQuar,strSearch,strOpt){
	fnStartProgress();
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmLotExpiryReport.do?method=fetchLotExpGraph&partNum='+encodeURIComponent(strPartNumber)+'&projectId='+strProjectId+
			'&chk_ActiveFl='+strExcQuar+'&strPartLiteral='+strSearch+'&strOpt='+strOpt+'&ramdomId='+new Date().getTime());
	dhtmlxAjax.get(ajaxUrl, fnPopulateLotExpChart)
}
//This function is used to populate the chart based on response
function fnPopulateLotExpChart(loader) {
    //fnStartProgress();
	var response = loader.xmlDoc.responseText;
	var lotThirty = '';
	var lotSixty = '';
	var lotNinety = '';
	var lotExpired = '';
	var lotOthers = '';

    var JSON_Array_obj = JSON.parse(response);

    $.each(JSON_Array_obj, function(index,jsonObject){
    	lotThirty = jsonObject.THIRTY;
    	lotSixty = jsonObject.SIXTY;   
    	lotNinety = jsonObject.NINTY;
    	lotExpired = jsonObject.EXPIRED;   
    	lotOthers = jsonObject.OTHERS;
    });
    FusionCharts.ready(function() {
	  var chartObj = new FusionCharts({
	    type: 'pie2d',
	    renderAt: 'chart-container',
	    width: "370px;",
        height: "210px;",
	    dataFormat: 'json',
	    dataSource: {
	      "chart": {
	    	  "showBorder": "0",
	    	  "showShadow": "0",
              "bgColor": "#ffffff",
              "captionpadding": "0",
              "decimals": "0",
              "showLabels": "0",
              "legendBorderColor": "#FFFFFF",
              "chartBottomMargin": "5",
              "chartTopMargin": "5",
              "chartLeftMargin": "0",
              "chartLeftMargin": "0",
			  "plottooltext": "<b>$percentValue </b>",
			  "showlegend": "1",
			  "showpercentvalues": "1",
			  "legendposition": "right",
			  "usedataplotcolorforlabels": "1",
			  "showLegendBorder": "0",
			  "theme": "fusion",
		      "paletteColors":"#5094D2,#F09556,#A0A0A0,#4472C4,#FFC000"
	      },
	      "data": [{
	          "label": "0 to 30",
	          "value": lotThirty
	        },
	        {
	          "label": "31 to 60",
	          "value": lotSixty
	        },
	        {
	          "label": "61 to 90",
	          "value": lotNinety
	        },
	        {
	          "label": ">90",
	          "value":lotOthers
	        },
	        {
	           "label": "Expired",
	           "value": lotExpired
	        },
	      ]
	    },
	  }).render();
	});	
fnStopProgress();
}
//this function is used to get data for lot expired >90 days
function fnLotExpOthers(strPartNumber,strProjectId,strExcQuar,strSearch,strOpt) {
	fnStartProgress();
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmLotExpiryReport.do?method=fetchLotExpByRange&loadFlg=others&partNum='+encodeURIComponent(strPartNumber)+'&projectId='+strProjectId+
			'&chk_ActiveFl='+strExcQuar+'&strPartLiteral='+strSearch+'&strOpt='+strOpt+'&ramdomId='+new Date().getTime());
	dhtmlxAjax.get(ajaxUrl,fnPopulateLotExp);
}
//this function is used to get data for lot expired between 0 to 30 days
function fnLotExpByThirtyDays(strPartNumber,strProjectId,strExcQuar,strSearch,strOpt) {
	fnStartProgress();
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmLotExpiryReport.do?method=fetchLotExpByRange&expDays=30&loadFlg=30Days&partNum='+encodeURIComponent(strPartNumber)+'&projectId='+strProjectId+
			'&chk_ActiveFl='+strExcQuar+'&strPartLiteral='+strSearch+'&strOpt='+strOpt+'&ramdomId='+new Date().getTime());
	dhtmlxAjax.get(ajaxUrl,fnPopulateLotExp);
}
//this function is used to get data for lot expired between 31 to 60 days
function fnLotExpBySixtyDays(strPartNumber,strProjectId,strExcQuar,strSearch,strOpt) {
	fnStartProgress();
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmLotExpiryReport.do?method=fetchLotExpByRange&expDays=60&loadFlg=60Days&partNum='+encodeURIComponent(strPartNumber)+'&projectId='+strProjectId+
			'&chk_ActiveFl='+strExcQuar+'&strPartLiteral='+strSearch+'&strOpt='+strOpt+'&ramdomId='+new Date().getTime());
	dhtmlxAjax.get(ajaxUrl,fnPopulateLotExp);
}
//this function is used to get data for lot expired between 61 to 90 days
function fnLotExpByNinetyDays(strPartNumber,strProjectId,strExcQuar,strSearch,strOpt) {
	fnStartProgress();
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmLotExpiryReport.do?method=fetchLotExpByRange&expDays=90&loadFlg=90Days&partNum='+encodeURIComponent(strPartNumber)+'&projectId='+strProjectId+
			'&chk_ActiveFl='+strExcQuar+'&strPartLiteral='+strSearch+'&strOpt='+strOpt+'&ramdomId='+new Date().getTime());
	dhtmlxAjax.get(ajaxUrl,fnPopulateLotExp);
}
//this function is used to get data for lot expired already
function fnExpiredLots(strPartNumber,strProjectId,strExcQuar,strSearch,strOpt) {
	fnStartProgress();
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmLotExpiryReport.do?method=fetchLotExpByRange&loadFlg=expired&partNum='+encodeURIComponent(strPartNumber)+'&projectId='+strProjectId+
			'&chk_ActiveFl='+strExcQuar+'&strPartLiteral='+strSearch+'&strOpt='+strOpt+'&ramdomId='+new Date().getTime());
	dhtmlxAjax.get(ajaxUrl,fnPopulateLotExp);
}
//this function is used to populate the grid data for lot expiry report screen
function fnPopulateLotExp (loader){
	//fnStartProgress();
	var response = loader.xmlDoc.responseText;
	var responseSpilt = response.split('^');
	var partNum = '';
	var partDesc = '';
	var count = '';
	var flag = '';
	var projectId = '';
	var strlotexp = '';
	var strlotexp30 = '';
	var strlotexp60 = '';
	var strlotexp90 = '';
	var strlotexpother = '';
	var strlotexpgraph = '';
	if(responseSpilt[0] == '' || responseSpilt[0] == null){
	var loadfl = responseSpilt[1];	 
	 	if(loadfl == 'others') {
	 		document.getElementById("loadOthers").innerHTML = '<center>No data to display.</center>';
	 		document.getElementById("excelToOthers").style.display = 'none';
	 	}
	    if(loadfl == '30Days') {
	    	document.getElementById("load30Days").innerHTML = '<center>No data to display.</center>';
	    	document.getElementById("excelTo30Days").style.display = 'none';
	    }
	    if(loadfl == '60Days') {
	    	document.getElementById("load60Days").innerHTML = '<center>No data to display.</center>';
	    	document.getElementById("excelTo60Days").style.display = 'none';
	    }
	    if(loadfl == '90Days'){
	    	document.getElementById("load90Days").innerHTML = '<center>No data to display.</center>';
	    	document.getElementById("excelTo90Days").style.display = 'none';
	    }  
	    if(loadfl == 'expired'){
	    	document.getElementById("loadExpired").innerHTML = '<center>No data to display.</center>';
	    	document.getElementById("excelToExpired").style.display = 'none';
	    	
	    }
	    strlotexp = document.getElementById("loadExpired").style.display;
	    strlotexp30 = document.getElementById("load30Days").style.display;
	    strlotexp60 = document.getElementById("load60Days").style.display;
	    strlotexp90 = document.getElementById("load90Days").style.display;
	    strlotexpother = document.getElementById("loadOthers").style.display;
	    
	    if(strlotexp == '' && strlotexp30 == '' && strlotexp60 == '' && strlotexp90 == '' && strlotexpother == ''){
	    	document.getElementById("DivRecNothingMessage").style.display = '';
	    }
	}else{
  //to form the grid string 
	var rows = [], i=0;
	var JSON_Array_obj = "";
	var partcount = 0;

	JSON_Array_obj = JSON.parse(responseSpilt[0]);	
	$.each(JSON_Array_obj, function(index,jsonObject){
		partNum = jsonObject.PARTNUM;
		partDesc = jsonObject.PARTDESC;  
		count = jsonObject.COUNT;
		flag = jsonObject.FLAG;
		projectId = jsonObject.PROJECTID;
    	
		// default array
        rows[i] = {};
        rows[i].id = i;
        rows[i].data = [];
        rows[i].data[0] = partNum;
        rows[i].data[1] = partDesc;   
        rows[i].data[2] = "<a href=javascript:fnLoadLotCountDetails('"+encodeURIComponent(partNum)+"','"+projectId+"','"+flag+"');>"+count+"</a>";  
        	
        i++;
        partcount = partcount+count;
	});
	var setInitWidths = "75,150,*";	
    var setColAlign = "left,left,right";
    var setColTypes = "ro,ro,ro";
    var setColSorting = "str,str,str";  
    var setHeader = [ "Part","Desc","Lot Count"]; 
    var setColIds = "";
    var enableTooltips = "true,true,true";
    var setFilter = "";   
    var gridHeight = "210px;";
    var footerStyles = [];
    var footerExportFL = true;
    var dataHost = {};
    var footerArry=new Array();	
	footerArry[0]="";
	footerArry[1]="<b>Total<b>";
	footerArry[2]="<div align='center'><b>"+partcount+"</b></div>";
    dataHost.rows = rows;
    document.getElementById("loadExpired").style.display = 'block';
    document.getElementById("load30Days").style.display = 'block';
	document.getElementById("load60Days").style.display = 'block';
	document.getElementById("load90Days").style.display = 'block';
	document.getElementById("loadOthers").style.display = 'block';
	$('.hline').show();	

	document.getElementById("DivRecNothingMessage").style.display = 'none';
	
    if(flag == 'others') {
    	document.getElementById("excelToOthers").style.display='block';
    	gridObj = loadDHTMLXGrid('loadOthers', gridHeight, dataHost, setHeader, setInitWidths, setColAlign, setColTypes, setColSorting, setColIds, enableTooltips, setFilter,footerArry, footerStyles, footerExportFL);
    }
    if(flag == '30Days') {
    	document.getElementById("excelTo30Days").style.display = 'block';
    	thirtygridObj = loadDHTMLXGrid('load30Days', gridHeight, dataHost, setHeader, setInitWidths, setColAlign, setColTypes, setColSorting, setColIds, enableTooltips,setFilter, footerArry, footerStyles, footerExportFL);
    }
    if(flag == '60Days') {
    	document.getElementById("excelTo60Days").style.display = 'block';
    	sixtygridObj = loadDHTMLXGrid('load60Days', gridHeight, dataHost, setHeader, setInitWidths, setColAlign, setColTypes, setColSorting, setColIds, enableTooltips,setFilter, footerArry, footerStyles, footerExportFL);
    }
    if(flag == '90Days'){
    	document.getElementById("excelTo90Days").style.display = 'block';
    	ninetygridObj = loadDHTMLXGrid('load90Days', gridHeight, dataHost, setHeader, setInitWidths, setColAlign, setColTypes, setColSorting, setColIds, enableTooltips,setFilter, footerArry, footerStyles, footerExportFL);	
    }  
    if(flag == 'expired'){
    	document.getElementById("excelToExpired").style.display = 'block';
    	expiredgridObj = loadDHTMLXGrid('loadExpired', gridHeight, dataHost, setHeader, setInitWidths, setColAlign, setColTypes, setColSorting, setColIds, enableTooltips, setFilter,footerArry, footerStyles, footerExportFL);
    }
    }
  fnStopProgress();
}
/*This function is used to load popup window when click qty on lot expiry report screen to show details based on inventory, 
  Consignment, Loaner and Field Sales/Account
*/
function fnLoadLotCountDetails(strPartNumber,strProjectId,strLoadFl){
	var strOpt = document.frmLotExpiryReport.strOpt.value;
	var strCheckExcQuar = document.frmLotExpiryReport.Chk_ActiveFl.value;
	windowOpener("/gmLotExpiryReport.do?method=loadExpiryLotDetails&loadFlg="+strLoadFl+'&partNum='+encodeURIComponent(strPartNumber)+'&projectId='+strProjectId+
			'&strOpt='+strOpt+'&chk_ActiveFl='+strCheckExcQuar,"","resizable=yes,scrollbars=yes,top=50,left=50,width=1500,height=600,status=1");
}

//This function is Export excel
function fnExcel(flag){
	if(flag == 'others'){
		gridObj.toExcel('/phpapp/excel/generate.php');
	}
	if(flag == '30Days'){
		thirtygridObj.toExcel('/phpapp/excel/generate.php');
	}
	if(flag == '60Days'){
		sixtygridObj.toExcel('/phpapp/excel/generate.php');
	}
	if(flag == '90Days'){
		ninetygridObj.toExcel('/phpapp/excel/generate.php');
	}
	if(flag == 'expired'){
		expiredgridObj.toExcel('/phpapp/excel/generate.php');
	}
}
