var gridObj = '';
function fnSubmit()
{
	var consType = document.frmVendor.hConsignType.value;
	//9114	Returns Hold TO Inv Adjustment, 400064	Returns Hold to Quarantine, need to skip expiry date validation rule
	if(consType == '9114' || consType == '400064' )
	{
		document.frmVendor.TXN_TYPE_ID.value = 'RETURN';
	}
	if(vIncludeRuleEng != 'NO'){
		document.frmVendor.txnStatus.value = 'VERIFY';
		document.frmVendor.action = '/gmRuleEngine.do';
	}
	//}
	if (consType == '4116' || consType == '4114' || consType == '4118')
	{
		var newQty=0;
		var updatedQty = 0;
		var pnum='';
		var pnumTemp = '';
		var cnum = '';
		var partQty = 0;
		var pnumStr = '';
		var lcnId ='';
		var pnumLcnStr ='';
		var vmaxqty = 0;
		var newLcn = '';
		var markedQtyStr = '';
		var dispstr = '';
		var flg = false;
		var gridrows =gridObj.getAllRowIds(",");
		if(gridrows.length>0)
		{
			var gridrowid;
			var gridrowsarr = gridrows.toString().split(",");
			var columnCount = gridObj.getColumnsNum();
			
			var i = 0;
			
			 for (var rowid = 0; rowid < gridrowsarr.length; rowid++) 
			 {
				gridrowid = gridrowsarr[rowid];
				pnum = gridObj.cellById(gridrowid, 3).getValue();
				i++;
				if (pnumStr.indexOf(pnum) != -1)
				{
					continue;
				}
				pnumStr = pnumStr + pnum + ',';
				partQty = 0;
				updatedQty = 0;
				markedQtyStr = '';
				for (var rowtempid = i-1; rowtempid < gridrowsarr.length; rowtempid++) 
				{
					pnumTemp = gridObj.cellById(rowtempid, 3).getValue();
					if (pnum == pnumTemp)
					{
						cnum = 	gridObj.cellById(rowtempid, 12).getValue();					
						if(cnum != '-')
						{					
							partQty = parseInt(partQty) + parseInt(gridObj.cellById(rowtempid, 9).getValue());
						}
						else
						{
							flg = true;
							newQty =  parseInt(gridObj.cellById(rowtempid, 9).getValue());
							updatedQty = parseInt(updatedQty) + newQty;
							
							lcnId = gridObj.cellById(rowtempid, 4).getValue();
							vmaxqty = parseInt(gridObj.cellById(rowtempid, 8).getValue());
							markedQtyStr = markedQtyStr + pnumTemp + ' : ' + lcnId + ' , ';		
							if (parseInt(updatedQty) > parseInt(vmaxqty))
							{
								newLcn = newLcn + pnumTemp + ' : ' + lcnId + ' , ';							
							}
							if(newQty > 0)
							{
								pnumLcnStr = 	pnumLcnStr + pnumTemp + ','+ newQty +','+ lcnId +'|';
							}
						}										
					}
					else
					{					
						break;
					}				
				}
				if(parseInt(updatedQty) != parseInt(partQty) && flg )
				{
					dispstr = dispstr + markedQtyStr; 			
				} 
			 }
		}	
		if (dispstr != '')
		{
			Error_Details(Error_Details_Trans(message[10084],dispstr));
		}
		if (ErrorCount > 0)
		{
				Error_Show();
				Error_Clear();
				return false;
		}
		if (newLcn != '')
		{
			var answer = confirm(Error_Details_Trans(message_operations[150],newLcn))
			if (!answer){
				return false;
			}
		}
		document.frmVendor.hpnumLcnStr.value = pnumLcnStr;
	}
	//alert(pnumLcnStr);
	fnStartProgress('Y');
  	document.frmVendor.submit();
}

function fnOnPageLoad()
{
	if(objGridData.indexOf("cell", 0)==-1){
		  return true;
	}
	gridObj = initItemGrid('dataGridDiv',objGridData);
	gridObj.enableTooltips("false,false,false,false,false,false,false,false,false,false,false");
	gridObj.groupBy(1);
	
	//gridObj.setColumnHidden(1,true);	
	gridObj.attachEvent("onEditCell",doOnCellEdit);
	if(vStatus == '4')
	{
		gridObj.setColumnHidden(4,true);
		gridObj.setColumnHidden(5,true);
		gridObj.setColumnHidden(6,true);
		gridObj.setColumnHidden(7,true);
		gridObj.setColumnHidden(8,true);
		gridObj.setColumnHidden(10,true);
		gridObj.setColumnHidden(11,true);
	}
}
function initItemGrid(divRef,gridData){
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);
	gObj.init();	
	
	gObj.loadXMLString(gridData);
	
	return gObj;
}

function doOnCellEdit(stage, rowId, cellInd, nValue, oValue) {
	var cnum;
	cnum = gridObj.cellById(rowId, 12).getValue();
	if(cnum != '-')
	{
		return false;
	}
	if(cellInd == 9 && stage == 1)
	{
			var c= this.editor.obj;
			if(c!=null) c.select();						
	}	
	if(gridObj.cellById(rowId, 9).getValue() == '' || gridObj.cellById(rowId, 9).getValue() < 0)
	{
		gridObj.cellById(rowId, 9).setValue(0);
	}
	return true;
}