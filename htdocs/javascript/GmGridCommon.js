/*******************************************************************************************************
 * File						: GmGridCommon.js 
 * Desc						: This is for Displaying  Error Message from message file and copied from Error.js  
 * 							  without Grid and dhtmlx imports for the PC-3971
 ********************************************************************************************************/
var split_At = '';
var w1,dhxWins;
function incJavascript(jsname) {
var th = document.getElementsByTagName('head')[0];
var s = document.createElement('script');
s.setAttribute('type','text/javascript');
s.setAttribute('src',jsname);
th.appendChild(s);
} 

function incCss(cssName) {
var th = document.getElementsByTagName('head')[0];
var s = document.createElement('link');
s.setAttribute('type','text/css');
s.setAttribute('rel','stylesheet');
s.setAttribute('href',cssName);
th.appendChild(s);
} 



incCss('/extweb/dhtmlx/dhtmlxGrid/dhtmlxgrid.css');
incCss('/extweb/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css');
incCss('/extweb/dhtmlx/dhtmlxCombo/dhtmlxcombo.css');
incCss('/extweb/dhtmlx/dhtmlxWindows/dhtmlxwindows.css');
incCss('/extweb/dhtmlx/dhtmlxWindows/skins/dhtmlxwindows_dhx_skyblue.css');
incJavascript("/extweb/dhtmlx/dhtmlxcommon.js");
incJavascript("/extweb/dhtmlx/dhtmlxGrid/dhtmlxgrid.js");
incJavascript("/extweb/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js");
incJavascript("/extweb/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_splt.js");
incJavascript("/extweb/dhtmlx/dhtmlxCombo/dhtmlxcombo.js");
incJavascript("/extweb/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js");
incJavascript("/extweb/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_group.js");
incJavascript("/extweb/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_sub_row.js");
incJavascript("/extweb/dhtmlx/dhtmlxTreeGrid/dhtmlxtreegrid.js");
incJavascript("/extweb/dhtmlx/dhtmlxWindows/dhtmlxwindows.js");
incJavascript("/extweb/dhtmlx/dhtmlxWindows/dhtmlxcontainer.js");





function setDefalutGridProperties(divRef,footerArry)
{
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/extweb/dhtmlx/dhtmlxGrid/imgs/");
	if(footerArry != undefined && footerArry.length >0)
	gObj.attachFooter(footerArry);
	gObj.enableMultiline(true);	
	return gObj;
}

function initializeDefaultGrid(gObj, gridData, split_At)
{	
	gObj.init();
	if(split_At !='' && split_At != undefined)
	{
		gObj.splitAt(split_At);
	}
	gObj.loadXMLString(gridData);	
	return gObj;
}

function initGrid(divRef,gridData,split_At,footerArry){
	var gObj = setDefalutGridProperties(divRef,footerArry);
	return initializeDefaultGrid(gObj,gridData,split_At);
}

function initGridWithDistributedParsing(divRef,gridData,footerArry){
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/extweb/dhtmlx/dhtmlxGrid/imgs/");
	if(footerArry != undefined && footerArry.length >0)
	gObj.attachFooter(footerArry);
	gObj.enableDistributedParsing(true);
	gObj.init();		
	gObj.loadXMLString(gridData);	
	return gObj;
}

function initTreeGrid(divRef,gridData,header,colTypes,alignment){	
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/images/dhtmlxGrid/imgs/icons_greenfolders/");
	gObj.setHeader(header);
	gObj.setColTypes(colTypes);
	gObj.setColAlign(alignment);

	gObj.init();
	gObj.loadXMLString(gridData);
	gObj.expandAll();
	
	return gObj;
}

function initGridFromTable(htmlTableId)
{
    var gridTable =dhtmlXGridFromTable(htmlTableId);
	gridTable.setSkin("dhx_skyblue");
	gridTable.setImagePath("/extweb/dhtmlx/dhtmlxGrid/imgs/");	
	gridTable.enableAutoWidth(true);
	gridTable.enableAutoHeight(true);
	
	return gridTable;
}



function createDhtmlxWindow()
{
dhxWins = new dhtmlXWindows();
dhxWins.enableAutoViewport(true);
dhxWins.setImagePath("extweb/dhtmlx/dhtmlxWindows/imgs");
w1 = dhxWins.createWindow("w1",150,150,500,250);
w1.button("park").hide();
w1.button("minmax1").hide();

return w1;
}


function CloseDhtmlxWindow(){
	parent.dhxWins.window("w1").close();
	   
}


// Load DHTMLX grid with JSON data

function loadDHTMLXGrid(divRef, gridHeight, gridData, setHeader, setInitWidths, setColAlign, setColTypes, setColSorting, setColIds, enableTooltips, setFilter, footerArry, footerStyles, footerExportFL,pagination) {

	var gObj = new dhtmlXGridObject(divRef);
    gObj.enableColSpan(true);
    gObj.setSkin("dhx_skyblue");
    gObj.setImagePath("/extweb/dhtmlx/dhtmlxGrid/imgs/");
    if (setHeader != undefined && setHeader.length > 0) {
        gObj.setHeader(setHeader);
        var colCount = setHeader.length;
    }
    
    if (setInitWidths != "")
        gObj.setInitWidths(setInitWidths);
    if (setColAlign != "")
        gObj.setColAlign(setColAlign);
    if (setColTypes != "")
        gObj.setColTypes(setColTypes);
    if (setColSorting != "")
        gObj.setColSorting(setColSorting);
    if (enableTooltips != "")
        gObj.enableTooltips(enableTooltips);
    if (setFilter != "")
        gObj.attachHeader(setFilter);
    if (footerArry != undefined && footerArry.length > 0) {
        var footstr = eval(footerArry).join(",");
        if (footerStyles != undefined && footerStyles.length > 0)
            gObj.attachFooter(footstr, footerStyles);
        else
            gObj.attachFooter(footstr);
    }
    
    if(setColIds != ""){
    	gObj.setColumnIds(setColIds);
    }
    /*if (footerExportFL)
        fnAttachExptFooter(divRef, colCount, gObj);*/
    
    if (gridHeight != "")
        gObj.enableAutoHeight(true, gridHeight, true);
    else
        gObj.enableAutoHeight(true, 400, true);

    gObj.init();
    if(pagination == "Y"){
	    gObj.enablePaging(true,100,10,"pagingArea",true);
	    gObj.setPagingSkin("bricks");
    }
    gObj.parse(gridData, "json");
    return gObj;
}


//Initialize Json Data 
function initGridObject(divRef) {
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/extweb/dhtmlx/dhtmlxGrid/imgs/");
	return gObj;
}

//Function Used to load Multipe Json Record(20,000) Using Smart Rendering. 
function loadDhtmlxGridByJsonData(gObj,jsonData){
	var dataRespone = "{\"data\":" + jsonData + "}";
	gObj.init();
	if(jsonData!= ''){
	dataRespone = JSON.parse(dataRespone);
	gObj.parse(dataRespone, "js");
	}
	return gObj;
}



