var TREE_ITEMS = [
	['Reports', null,
		['Raw Materials Inventory', 'GmInvListReportServlet\',\'ManfPart'],
		['Main Inventory', 'GmPartInvReportServlet\',\'Main'],
		['Transactions Log', 'GmTranLogReportServlet\',\'LoadNCMR'],
		['BOM Report', 'GmSetReportServlet\',\'BOMRPT'],
		['PO Reports - By Part Number', 'GmPOReportServlet\',\'Part'],
		['Work Order Status', 'GmMFGReconWOServlet\',\'LoadWOStat'],
		['Work Order Summary', 'GmMFGReconWOServlet\',\'LoadWOSum'],
		['Part Conversion Details', 'gmPCDetail.do?method=reportPCDetail\',\'FromMenu'],
		['Part Conversion Report', 'gmPCSummary.do?method=reportPCSummary\',\''],
		['Sub Assembly Part Search', 'GmPartNumServlet\',\'SUBASMSCH'],
		['Part Number Transaction', 'GmPartNumSearchServlet\',\''],
	]
];