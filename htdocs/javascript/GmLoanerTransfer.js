var gridObj = ''; 
function fnLoad()
{
	var frm = document.frmLoanerTransfer;
	var selIdx1 = frm.fromDistributor.selectedIndex;
	fromDistributor = frm.fromDistributor.options[selIdx1].value;
	var selIdxToDist = frm.toDistributor.selectedIndex;
	toDistributor = frm.toDistributor.options[selIdxToDist].value;
	/*if(fromDistributor == toDistributor){  //MNTTASK-2279 Transfer a Loaner Set from one rep to another that are within the same distributor
		Error_Details("<b>From</b> and <b>To</b> should not have the same value");
	}*/
	fnValidateCheckForm();
	if (ErrorCount > 0){
			Error_Show();
			Error_Clear();
			return false;
	}else{
		frm.strOpt.value = 'load';
		fnStartProgress('Y');
		frm.submit();
	}
	
}

function fnOnPageLoad(){	  
	if (objGridData != ''){
		gridObj = initGrid('LoanerData',objGridData);
		//gridObj.enableTooltips("false,true,true,true,true,true");
		gridObj.groupBy(1); //for Type ID
		gridObj.setColumnHidden(1,true); //for Type ID
		gridObj.attachEvent("onEditCell",doOnCellEdit);
	}
}

function doOnCellEdit(stage, rowId, cellInd, nValue, oValue){
		var trnsValue = gridObj.cellById(rowId,7).getValue();
		var reqDtlValue  = gridObj.cellById(rowId,8).getValue();
		if(cellInd == '7' && nValue!=oValue ){
            var comboDistRep = gridObj.getCustomCombo(rowId,8);
            comboDistRep.clear();
            comboDistRep.put(0,'[Choose One]');
            if(trnsValue == '1006701'){
                  for(i=0;i<RepDetArr.length;i++)     {
                        strVal=RepArr[gridObj.getRowIndex(rowId)];
                        if(strVal==RepDetArr[i][0])   {     
                              comboDistRep.put(RepDetArr[i][1],RepDetArr[i][1]);
                        }
                  }
            }else if(trnsValue == '1006702'){
                  for(i=0;i<ReqDtlArr.length;i++)     {
                        strValArr=ReqDtlArr[i].split(",");
                        comboDistRep.put(strValArr[0],strValArr[1]);
                        }
                  }
            gridObj.cellById(rowId, 8).setValue('0');
            gridObj.editor.combo = comboDistRep;
            comboDistRep.save();
      }
	  if(cellInd == '9'){
		      if (Type == '4127' && (reqDtlValue== '1006703' || reqDtlValue== '1006704')){ //product loaner
				//get the request details Type from 8th  cell
		    	var comboRep = gridObj.getCustomCombo(rowId,9);
		    	    comboRep.clear();
		    	    comboRep.put('0','[Choose One]');
		    	  for(i=0;i<RepsArr.length;i++)	{
						strRepsArr=RepsArr[i].split(",");
						comboRep.put(strRepsArr[0],strRepsArr[1]);
						}
		    	  /*Whenever we are changing in Sales Rep Column it needs to fetch Assoc Rep based on Sales Rep.
		    	   *In the below if condition We are getting the new value of Sales Rep Column and passing it to the function  
		    	   *In The function we will allow to show the Assoc Rep for the corresponding Sales Rep.
		    	   */
		    	  if(stage ==2){
		    		  var salesRepId = nValue;
		    		  var comboAssocRep = gridObj.getCustomCombo(rowId,10);
		    		  fnAssocReps(salesRepId,comboAssocRep);
		    	  }
		    	  
		    	  gridObj.editor.combo = comboRep;
		    	  comboRep.save();
		    	
				}
		}else if(cellInd == '11'){   //current account name in account dropdown
			      if (Type == '4127' && (reqDtlValue== '1006703' || reqDtlValue== '1006704')){ //product loaner
						//get the request details Type from 8th  cell
				    	var comboAcc = gridObj.getCustomCombo(rowId,11);
				    	var frmAccid = gridObj.cellById(rowId,12).getValue();
				    	comboAcc.clear();
				    	
				    	comboAcc.put('0','[Choose One]');
				    	  for(i=0;i<AccArr.length;i++)	{
								strAccArr=AccArr[i].split(",");
 								if(frmAccid!=strAccArr[0]){
								comboAcc.put(strAccArr[0],strAccArr[1]);
									}
								}
				    	  gridObj.editor.combo = comboAcc;
				    	  comboAcc.save();
						}
					}

	return true;
}
function fnAssocReps(salesRepId,comboAssocRep){
	
	
	  comboAssocRep.clear();
	  comboAssocRep.put('0','[Choose One]');
	  for (var i=0;i<AssocRepsArr.length;i++)
		{
			arr = AssocRepsArr[i]; 		
			arrobj = arr.split(",");
			repid = arrobj[2];
			repname = arrobj[3];
			associd = arrobj[1];
			assocname = arrobj[0];
				if(repid == salesRepId ){
					comboAssocRep.put(associd,assocname);;
					}
			}
		gridObj.editor.combo = comboAssocRep;
  		comboAssocRep.save();
	  }


function fnPrintVer(val)
{
	windowOpener("/GmInHouseSetServlet?hAction=ViewSet&hConsignId="+val,"Con1","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnOpenLog(id, type)
{

	windowOpener("/GmCommonLogServlet?hType="+type+"&hID="+id,"ConsignLog","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=300");
}

function fnValidateCheckForm(){
	fnValidateDropDn('fromDistributor',message[10072]);
	fnValidateDropDn('toDistributor',message[10073]);
}

function fnSubmit()
{
	fromDistributor = document.frmLoanerTransfer.fromDistributor.value;
	toDistributor = document.frmLoanerTransfer.toDistributor.value;
	var openReqArr = new Array();
	var rowCount = 0;
	var chkBoxflag = false;
	var finalFlag = true;
	var openReqFlag = false;
	var strInputString = "";
	var strConsignId = "";
	var repId ="0";
	var accID ="0";
	var assocRepID = '0';
	count = gridObj.getRowsNum();
	var row = 0;
	var strChecked = false;
	var tdate;
	
	gridObj.forEachRow(function(id){
		strChecked = document.getElementById("ChkReqId"+row).checked;
		if(strChecked == true)
		{
			strConsignId = document.getElementById("ChkReqId"+row).value;
			var tdate = gridObj.cells2(row,6).getValue();
			var trnsValue = gridObj.cells2(row,7).getValue();
			var svalue = gridObj.cells2(row,8).getValue();
			if(Type == '4127'){ // Product Loaner
				repId  = gridObj.cells2(row,9).getValue();
				//accID  = gridObj.cells2(row,10).getValue();	
				
				  accID  = gridObj.cells2(row,11).getValue();
				 assocRepID  = gridObj.cells2(row,10).getValue();	
			}
						
			validateDT(tdate, 'Cannot update transfer date - '+tdate +' ' + message[1]);	 
			if(svalue =="[Choose One]")	
				svalue="0";
			
			if(trnsValue == '0')
				Error_Details(Error_Details_Trans(message[10027],strConsignId.split("^")[0]));
			if(svalue == '0' && trnsValue!='0' )
				Error_Details(Error_Details_Trans(message[10028],strConsignId.split("^")[0]));
			
			if(svalue == '1006703' || svalue == '1006704'){ // create for surgery , create for wrong shipment
				if(repId == '0')
					Error_Details(Error_Details_Trans(message[10029],strConsignId.split("^")[0]));
				if(accID == '0')
					Error_Details(Error_Details_Trans(message[10030],strConsignId.split("^")[0]));
				
			}
			
			if(svalue != '0' && trnsValue !='0'){			
				for(i=0;i<openReqArr.length;i++){
					if(openReqArr[i] == svalue && trnsValue!='1006702')
					{
						Error_Details(Error_Details_Trans(message[10031],svalue));
					}
				}
				if (ErrorCount == 0) 
				{
					openReqArr[rowCount] = svalue;
					rowCount++;
				}
				strInputString = strInputString + strConsignId +'^'+ svalue +'^'+ tdate +'^'+trnsValue+'^'+toDistributor+'^'+repId+'^'+assocRepID+'^'+accID+'|';
			}		
		}				
		row++;
	});
	if(strInputString == '' && ErrorCount == 0)
	{
		Error_Details(message[10032]);
	}
	
	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}else{
		
		var fromDist = document.frmLoanerTransfer.fromDistributor;
		var fromSelIndex = fromDist.selectedIndex;
		var toDist = document.frmLoanerTransfer.toDistributor;
		var toSelIndex = toDist.selectedIndex;
		
		document.frmLoanerTransfer.strInputString.value =  strInputString;
		document.frmLoanerTransfer.fromDistName.value = fromDist.options[fromSelIndex].text;
		document.frmLoanerTransfer.toDistName.value = toDist.options[toSelIndex].text;
		document.frmLoanerTransfer.strOpt.value =  'save';
		fnStartProgress('Y');
		document.frmLoanerTransfer.submit(); 
	}
}
//code for date validation
function validateDT(value, msg){
	//validateDTFormatMMDDYYYY(value, msg); -- Here no need to check the date format because daye is not editable here.
	var diff = dateDiff(strTodaysDate,value,dateFmt);
	if (diff > 0){
		Error_Details(message[10033]);
	}	
}

function validateDTFormatMMDDYYYY(value, msg)
{	
	var validformat=/^\d{2}\/\d{2}\/\d{4}$/; //Basic check for format validity
	var returnval=false;
	if (!validformat.test(value))
	{
		Error_Details(msg);
	}
	else
	{
		var monthfield=value.split("/")[0];
		var dayfield=value.split("/")[1];
		var yearfield=value.split("/")[2];
		var intyearfield = parseInt(yearfield);
		if(intyearfield < 1900)
			Error_Details(msg);
		
		var dayobj = new Date(yearfield, monthfield-1, dayfield);
		if ((dayobj.getMonth()+1!=monthfield)||(dayobj.getDate()!=dayfield)||(dayobj.getFullYear()!=yearfield))
			Error_Details(msg);
		else
			returnval=true;
	}	
}

function getReqDtlValue(dtlValue){
	dtlValue = TRIM(dtlValue);
	if(dtlValue == "Create For Wrong Shipment"){
		dtlValue="1006704";
	}else if(dtlValue == "Create For Surgery"){
		dtlValue="1006703";
	}else if(dtlValue == "[Choose One]"){
		dtlValue="";
	}else	
	{
		dtlValue = dtlValue.split("-")[0];
	}
	return dtlValue;
}
