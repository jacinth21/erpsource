var TREE_ITEMS = [
	['Home', 'GmSaleDashBoardServlet\',\''],
	['Reports', null,
		['Part Number Search', 'GmPartSearchServlet\',\''],
		['Field Sales',null,
			['Field Sales Report', 'gmDistributorReport.do\',\''],
			['Field Sales/Accounts Report', 'GmAccountReportServlet\',\'ALL'],
			['Field Sales/Reps Report', 'GmAccountReportServlet\',\'REPALL'],
			['All Mapping Report', 'GmTerritoryServlet\',\'MapReport'],
		],
			['Accounts Report', 'GmAccountReportServlet\',\''],
			['Pending Orders (PO)', 'GmPendingOrdersPOServlet\',\'RPT'],
			['Detailed Monthly Sales', 'GmDetailedSalesServlet\',\''],
			['Monthly Dashboard', 'GmMonthlyDashboardServlet\',\'LoadStateSales'],
			['Sales By State', 'GmDetailStateSalesServlet\',\'LoadStateSales'],
			['Sales By Project', 'GmSalesByProjectServlet\',\'LoadProjectSales'],
			['Pricing', 'GmSalesAcctPriceReportServlet\',\'CurrentPrice'],
		['Back Orders', null,
			['Sales', 'GmOrderItemServlet\',\'ProcBO'],
			['Items from Shipped Sets', 'GmOrderItemServlet?Cbo_Type=50262\',\'ProcBO'],
			['Items', 'GmOrderItemServlet?Cbo_Type=50263\',\'ProcBO'],
			['Sets', 'GmOrderItemServlet?Cbo_Type=50264\',\'ProcBO'],
			['Loaners', 'GmOrderItemServlet?Cbo_Type=50265\',\'ProcBO'],
			['All Parts', 'GmOrderItemServlet\',\'PartBO'],
		],
		['Historical Sales',null,
			['By Field Sales', 'GmSalesYTDReportServlet\',\'LoadDistributorA'],
			['By Spine Specialist', 'GmSalesYTDReportServlet\',\'LoadRepA'],
			['By Account', 'GmSalesYTDReportServlet\',\'LoadAccountA'],
			['By System', 'GmSetYTDReportServlet\',\'LoadGroupA'],
			['By Part', 'GmSalesYTDReportServlet\',\'LoadPartA'],
		],
		['Quota Performance',null,
			['By VP', 'GmSalesGrowthServlet\',\'VP'],
			['By AD', 'GmSalesGrowthServlet\',\'AD'],
			['By Field Sales', 'GmSalesGrowthServlet\',\'DIST'],
			['By Territory', 'GmSalesGrowthServlet\',\'TERR'],
			['Summary', 'GmQuotaTrackingServlet\',\'QuotaTrack'],		
		],	
		['Account Price Increase', 'GmSalesAcctPriceReportServlet\',\'PriceIncrease'],
		['Field Inventory Report',null,			
			['By VP', 'GmSalesConsignmentServlet\',\'LoadVP'],
			['By AD', 'GmSalesConsignmentServlet\',\'LoadAD'],
			['By System', 'GmSalesConsignmentServlet\',\'LoadGCONS'],
			['By Field Sales', 'GmSalesConsignmentServlet\',\'LoadDCONS'],
			['Details By Part', 'GmSalesConsignDetailsServlet\',\'LoadPart'],
			['Set List Report', 'GmSetReportServlet\',\'SETRPT'],
			['Consignment Net Report (By Distributor)', 'GmSalesVirtualActualServlet\',\'BYCDIST'],
			['Consignment Net Report (By Set)', 'GmSalesVirtualActualServlet\',\'BYCSET'],

		],
	],
	['Documents', null,
		['Forms', 'GmSalesForms.html'],
		['Product Info', 'GmProdInfo.html'],
		['Registrations/Certificates', 'GmRegnCert.html'],
		['Inserts', 'GmProdInserts.html'],
		['How To', 'GmHowTo.html'],
		['510(k)', '510k.html'],
		['Product Demo', 'GmProdDemo.html']
	],
];