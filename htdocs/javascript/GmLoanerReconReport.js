
var gridObj;
function fnReload()
{
	if (document.frmLoaner.Chk_shwDet.checked == true && document.frmLoaner.Cbo_ReconType.value != 0 ){
		 Error_Details(message[2008]);
	}
	
	objStartDt = document.frmLoaner.Txt_FromDate;
	objEndDt = document.frmLoaner.Txt_ToDate;
	if(objStartDt.value != ""){
		CommonDateValidation(objStartDt,dateFmt,Error_Details_Trans(message[10070],dateFmt));
	}

	if(objEndDt.value != ""){
		CommonDateValidation(objEndDt,dateFmt,Error_Details_Trans(message[10071],dateFmt));
	}
	
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}

	document.frmLoaner.hAction.value = "Reload";
	document.frmLoaner.hOpt.value = "Recon";
	fnStartProgress();
	document.frmLoaner.submit();
}

function fnPrintInvoice(val)
{
	document.frmLoaner.hId.value = val;
	document.frmLoaner.hAction.value = "LoanRecon";
	document.frmLoaner.action = "/GmLoanerReconServlet";
	document.frmLoaner.submit();	
}

function fnOpenPartDet(val){
	var format = dateFmt.toLowerCase();//Converting to lowercase
	
	distid = document.frmLoaner.Cbo_DistId.value;
	fromdate = document.frmLoaner.Txt_FromDate.value;
	todate = document.frmLoaner.Txt_ToDate.value;
	
	if (fromdate == '' || todate == '' )
	{
		fromdate =strTodaysDate;
		todate = strTodaysDate;
	}
	
	fromdate = fnGetFromDate(format,fromdate); 
	windowOpener("/gmPartNumOrderDetail.do?partNum="+encodeURIComponent(val)+"&dist="+distid+"&fromDate="+fromdate+"&toDate="+todate+"&ordType=50301","PartNumDetails","resizable=yes,scrollbars=no, top=150,left=150,width=880,height=610");	
}

function enterPressed(evn) {
	if (window.event && window.event.keyCode == 13) {
	  fnReload();
	} else if (evn && evn.keyCode == 13) {
	  fnReload();
	}
}

function fnOnPageLoad()
{	  
	if (objGridData != '')
	{
		gridObj = initGridWithDistributedParsing('LoanerReconReport',objGridData);
		gridObj.enableTooltips("false,true,true,true,true");		
		gridObj.groupBy(1); //for Type ID
		gridObj.setColumnHidden(1,true); //for Type ID
		
	}
}

function fnExport()
{	
	gridObj.expandAllGroups();
	gridObj.setColumnHidden(1,false);
	gridObj.setColumnHidden(0,true);
	gridObj.toExcel('/phpapp/excel/generate.php');
	gridObj.setColumnHidden(1,true);
	gridObj.setColumnHidden(0,false);

}

function fnGetFromDate(format,fromdate){
	var searchMe = format.charAt(2);//'/';
    var firstIndex = fromdate.indexOf(searchMe);
    var lastIndex = fromdate.lastIndexOf(searchMe);
    var yyyy = new String(fromdate.substring(lastIndex+1,fromdate.length));
    var firstchar = new String(format.substring(0,firstIndex));
    
    var dd = (firstchar == 'dd') 
    				?new String(fromdate.substring(0,firstIndex))
    						:new String(fromdate.substring(firstIndex+1,lastIndex));
	var mm = (firstchar == 'dd') 
					?new String(fromdate.substring(firstIndex+1,lastIndex))
							:new String(fromdate.substring(0,firstIndex));
	
	var five_day=1000*60*60*24*5;
	var fromdate = new Date(yyyy, mm-1, dd);
	fromdate = new Date(fromdate.getTime()-five_day);
	var changedMnth = fromdate.getMonth()+1;// Month index is starting from 0
	
	if(firstchar == 'mm'){
		fromdate = changedMnth+'/'+ fromdate.getDate() + '/' + fromdate.getFullYear();
	}else if(firstchar == 'dd'){
		fromdate = fromdate.getDate()+searchMe+ changedMnth + searchMe + fromdate.getFullYear();
	}

    return fromdate;
}

document.onkeypress = enterPressed;