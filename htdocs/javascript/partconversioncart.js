function fnCreateOrderString()
{
	var varRowCnt = document.frmPartConversion.counterValue.value;
	var k;
	var obj;
	var str;
	var token = '^';
	var cnum = '';
	var status = true;
	var inputstr = '';
	var stockqty = 0;
	var qty = 0;
	var price = 0;
	var pnum = '';
	var costerrmsg = '';
	var qtyerrmsg = '';
	var objtotalconvqty = document.frmPartConversion.totalConversionQty.value;
	var objfromtotalqty = document.frmPartConversion.invQOH.value;
	var totalconvqty = parseInt(objtotalconvqty);
	var fromtotalqty = parseInt(objfromtotalqty);
	var invType = '';

	

	for (k=0; k < varRowCnt; k ++)
	{
		obj = eval("document.frmPartConversion.toPartNum"+k);
		pnum = obj.value;
		objqty = eval("document.frmPartConversion.toPartQty"+k);
		qty = parseInt(objqty.value);
		objCost = eval("document.frmPartConversion.toCogsCost"+k);
		price = parseInt(RemoveComma(objCost.value));
		objInvType = eval("document.frmPartConversion.toInvType"+k);
		invType = parseInt(RemoveComma(objInvType.value));

		if(isNaN(qty) && !isNaN(price)){
			j = k + 1;
			qtyerrmsg =  qtyerrmsg + ' '+ j ;
			}
		else if (!isNaN(qty) && isNaN(price)){
			i = k + 1;
			costerrmsg =  costerrmsg + ' '+ i ;
		}
		else if (!isNaN(qty) && !isNaN(price)){
			str = pnum + token + qty + token + price + token + invType + '|';
			inputstr = inputstr + str;
		}
	}

	if(qtyerrmsg != ''){
		Error_Details(" Please specify <B>Qty </B>in the following rows <B>" + qtyerrmsg + " </B>");
	}

	if(costerrmsg != ''){
		Error_Details(" Please specify a <B>Cost </B>in the following rows <B>" + costerrmsg + " </B>");
	}
	if(inputstr == '' ){
		Error_Details(" Please specify Qty and Cost for a part to proceed ");
	}
//	alert(inputstr);
	return inputstr;
}

function fnCalculateTotal()
{
	var varRowCnt = document.frmPartConversion.counterValue.value;
	var k;
	var obj;
	var qty = 0;
	var price = 0.0;
	var total = 0.0;
	var pnum = '';
	var totalqty = 0;

	for (k=0; k < varRowCnt; k ++)
	{
		obj = eval("document.frmPartConversion.toPartNum"+k);
		pnum = obj.value;
		obj = eval("document.frmPartConversion.toPartQty"+k);
		qty = parseInt(obj.value);
		obj = eval("document.frmPartConversion.toCogsCost"+k);
		price = parseFloat(RemoveComma(obj.value));

		if(!isNaN(qty) && !isNaN(price)){
			total = total + (price * qty);
		}

		if(!isNaN(qty)){
			totalqty = totalqty + qty;
		}

	}

//	document.frmPartConversion.hTotal.value = total;
	document.all.Lbl_Total.innerHTML = '<b>$'+ formatNumber(total)+'</b>&nbsp;';
	document.all.Lbl_TotalQty.innerHTML = '<b>'+ totalqty+'</b>&nbsp;';
	document.frmPartConversion.totalPrice.value = total;
	document.frmPartConversion.totalConversionQty.value = totalqty;
}


function fnCalExtPrice(cnt)
{
	var qty = 0;
	var price = 0.0;
	var pqty = eval("document.frmPartConversion.toPartQty"+cnt);
	var pcost = eval("document.frmPartConversion.toCogsCost"+cnt);
	var pcostval = pcost.value;
	qty = parseInt(pqty.value);
	price = parseFloat(pcostval);

	if(pcostval.charAt(0) == '-'){	
		Error_Details(" Please specify Qty and Cost for a part to proceed ");
	}

	else if (!isNaN(qty) && !isNaN(price)){
		var eprice = price * qty;
		var pobj = eval("document.all.Lbl_Amount"+cnt);
		pobj.innerHTML = formatNumber(eprice)+"&nbsp;";
		pcost.value = formatNumber(price);
	}

	if(isNaN(qty) && isNaN(parseInt(pcostval)))	{
		var pobj = eval("document.all.Lbl_Amount"+cnt);
		pobj.innerHTML = "";
	}

	fnCalculateTotal();
	changeBgColor(pqty,'#ffffff');
	changeBgColor(pcost,'#ffffff');

}

