

var delYear='';
var curdate = new Date()
var curmonth = curdate.getMonth()+1;
var curyear=curdate.getFullYear();
var mygrid;
function fnReset()
{
	document.frmTerritory.Cbo_CompanyId.value = "0";
	document.frmTerritory.hAction.value = 'Load';
	document.frmTerritory.submit();
}

function fnSubmit()
{
	fnValidateDropDn('Cbo_CompanyId',' Company Name List ');
	fnValidateDropDn('Cbo_CurrType',' Currency Type ');
	fnValidateDropDn('Cbo_RepId',' Sales Rep List ');
	fnValidateTxtFld('Txt_TerrNm',' Territory Name');
	fnValidateTxtFld('Txt_LogReason',' Comments');
	
	
	
	if (document.frmTerritory.Chk_ActiveFl.checked)
	{
		document.frmTerritory.Chk_ActiveFl.value = 'Y';
	}
	
	var quotastr = '';
	if(mygrid!=null)
	{
		mygrid.editStop();
		var gridrows =mygrid.getChangedRows(true);
		if(gridrows != undefined)
		{
			var gridrowsarr = gridrows.toString().split(",");
			var arrLen = gridrowsarr.length;
			var year='';
			for (var i=0;i<arrLen;i++)
			{
		        rowId=gridrowsarr[i];
		        if (mygrid.getRowIndex(rowId) != -1)
		        {
			        year=mygrid.cellById(rowId,0).getValue();
			        if(parseInt(year)>=parseInt(curyear))
					{
			        	for(var x=1;x<=12;x++)
			        	{
			        		val=TRIM(mygrid.cellById(rowId,x).getValue());
			        		if(x<10)
					        id="0"+x;
					        else
					        id=x;
					        
					        quotastr = quotastr + id + '^' +  year + '^'+val + '|';
			        	}
					}
		        }
			}		
		}
	}
	if(delYear.length>0)
	{
		delYear=delYear.substring(0,delYear.length-1);
	}
	document.frmTerritory.Cbo_Year.value = delYear;
	if (document.frmTerritory.hAction.value == 'Add')
	{
		quotastr = '01^'+currYear+'^|'+'02^'+currYear+'^|'+'03^'+currYear+'^|'+'04^'+currYear+'^|'+'05^'+currYear+'^|'+'06^'+currYear+'^|'+'07^'+currYear+'^|'+'08^'+currYear+'^|'+'09^'+currYear+'^|'+'10^'+currYear+'^|'+'11^'+currYear+'^|'+'12^'+currYear+'^|';
	}
	document.frmTerritory.inputString.value = quotastr;
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();	
	}
	else
	{
		fnStartProgress();
		document.frmTerritory.submit();
	}
	
}

function fnCallTerritory(val)
{
	var quotastr ='';
	document.frmTerritory.hAction.value = (val == '0')?'Load':'Reload';
	quotastr = '01^'+currYear+'^|'+'02^'+currYear+'^|'+'03^'+currYear+'^|'+'04^'+currYear+'^|'+'05^'+currYear+'^|'+'06^'+currYear+'^|'+'07^'+currYear+'^|'+'08^'+currYear+'^|'+'09^'+currYear+'^|'+'10^'+currYear+'^|'+'11^'+currYear+'^|'+'12^'+currYear+'^|';
	document.frmTerritory.inputString.value = quotastr;
	document.frmTerritory.submit();
}

function fnPopulateRep(val)
{
	var quotastr ='';
	document.frmTerritory.hAction.value = (val == '0')?'Load':'LoadRepList';
	quotastr = '01^'+currYear+'^|'+'02^'+currYear+'^|'+'03^'+currYear+'^|'+'04^'+currYear+'^|'+'05^'+currYear+'^|'+'06^'+currYear+'^|'+'07^'+currYear+'^|'+'08^'+currYear+'^|'+'09^'+currYear+'^|'+'10^'+currYear+'^|'+'11^'+currYear+'^|'+'12^'+currYear+'^|';
	document.frmTerritory.inputString.value = quotastr;
	document.frmTerritory.submit();
}

function fnShowCurr(val){
	var quotastr ='';
	document.frmTerritory.hAction.value = (val == '0')?'Load':'Reload';
	quotastr = '01^'+currYear+'^|'+'02^'+currYear+'^|'+'03^'+currYear+'^|'+'04^'+currYear+'^|'+'05^'+currYear+'^|'+'06^'+currYear+'^|'+'07^'+currYear+'^|'+'08^'+currYear+'^|'+'09^'+currYear+'^|'+'10^'+currYear+'^|'+'11^'+currYear+'^|'+'12^'+currYear+'^|';
	document.frmTerritory.inputString.value = quotastr;
	fnSetCurrencySymb();	
	if (document.frmTerritory.Cbo_RepId.value != '0'){
	
		//For Base Currency Type submit button should be disabled.
		if (document.frmTerritory.Cbo_CurrType.value == '105460'){
			fnDisableButton('frmTerritory','btn_submit');
			fnDisableButton('frmTerritory','btn_reset');
		}else{
			fnEnableButton('frmTerritory','btn_submit');
			fnEnableButton('frmTerritory','btn_reset');
		}
		document.frmTerritory.submit();
	}
}

function fnSetCurrencySymb(){
	if(document.frmTerritory.Cbo_CurrType.value=='105461'){
		document.getElementById("currsymbl").innerHTML=document.frmTerritory.hTxnCurr.value;
	}
	else if(document.frmTerritory.Cbo_CurrType.value=='105460')
	{
		document.getElementById("currsymbl").innerHTML=document.frmTerritory.hRptCurr.value;
	}else{
		document.getElementById("currsymbl").innerHTML="";
	}
}
function fnSetQuota(objGridData)
{	

	
	if(document.frmTerritory.Cbo_CurrType.value == 0 &&
	   document.frmTerritory.Cbo_CompanyId.value != 0){
		document.frmTerritory.Cbo_CurrType.value= '105461';		
	} 
	fnSetCurrencySymb();
	
	if (objGridData != '')
	{
		
		menu = new dhtmlXMenuObject();
		menu.setIconsPath("/images/dhtmlxGrid/imgs/");
		menu.renderAsContextMenu();
		menu.loadXML("growth_grid_comments.xml");
		
		
		mygrid = initGridData('grpData',objGridData);
		
		mygrid.clearAll(); 
		mygrid.enableBlockSelection();
		mygrid.copyBlockToClipboard();
		//mygrid.enableEditEvents(false, true, true);
		mygrid.enableEditEvents(true,false,true);
		//mygrid.attachEvent("onKeyPress", keyPressed);
		
		mygrid.attachEvent("onEditCell", checkForDuplicate);		
		mygrid.setImagePath("/images/dhtmlxGrid/imgs/");
		mygrid.attachEvent("onRowPaste",function(id){
             mygrid.forEachCell(id,function(obj){
                   obj.cell.wasChanged = true;
                   });
			});
		mygrid.enableContextMenu(menu);
		mygrid.enableUndoRedo();
		mygrid.enableMultiselect(true);
		mygrid.setHeader("Content-type: text/xml");
		mygrid.setAwaitedRowHeight(200);
		
		
	    mygrid.setCSVDelimiter("\t"); 
	    //mygrid.gridFromClipboard();      
	    //mygrid.updateRowFromClipboard();
	    //mygrid.updateCellFromClipboard(); 

	    
	    
		mygrid.init();		
		mygrid.loadXMLString(objGridData);	  
		mygrid.submitOnlyChanged(true);
		mygrid.submitOnlyRowID(false);
		mygrid.enableAutoHeight(true,300,true);
		
		//For Base Currency Type submit button should be disabled.
		if (document.frmTerritory.Cbo_CurrType.value == '105460'){
			fnDisableButton('frmTerritory','btn_submit');
			fnDisableButton('frmTerritory','btn_reset');
		}else{
			fnEnableButton('frmTerritory','btn_submit');
			fnEnableButton('frmTerritory','btn_reset');
		}
	}
}

function docopy(){
    mygrid.setCSVDelimiter("\t");
	mygrid.copyBlockToClipboard();
	mygrid._HideSelection();
}

function pasteToGrid(){
	
	if(mygrid._selectionArea!=null){
		var colIndex = mygrid._selectionArea.LeftTopCol; 
		if(colIndex!=undefined && colIndex !=0){
			mygrid.pasteBlockFromClipboard();
			mygrid._HideSelection();
		}else{
			alert(' Please Select a valid value from year Drop Down');
				}
	}else{
		alert(' Please BlockSelect Cell(s) to Paste Data.');
	}
}


function addRowFromClipboard(){
	
   var cbData = mygrid.fromClipBoard();   
  
	var rowCount = cbData.split("\n"); 
	
	var ignoredRows ='<b>The Following year are not added as they are not Mapped to the Sheet. </b><br>';
	var ignoredRowCount = 0;
	
	for (var i=0; i<rowCount.length; i++){
		
		var rowData = rowCount[i].split("\t");
		
		for(var j=0;j<rowData.length;j++){
			cb_columnName=rowData[0];
			break;
		}
				
		var dropDownVal=varNameDropDownValues.split("#@#");
		if((varNameDropDownValues.toUpperCase()).indexOf(TRIM(cb_columnName.toUpperCase()))== -1){
			ignoredRows = ignoredRows+cb_columnName+'<br>';
			ignoredRowCount++;
		}

		
		for (var dd=0; dd<dropDownVal.length; dd++){
		
			var row_id= dropDownVal[dd];
						
			if(row_id.length>0 && !checkForDuplicateRow(row_id) && TRIM(row_id) == TRIM(cb_columnName)){
				mygrid.setCSVDelimiter("\t");
				mygrid.setDelimiter("\t");
				mygrid.addRow(row_id,rowCount[i]);
				mygrid.setRowAttribute(row_id,"row_added","Y");
				mygrid.setDelimiter(",");
			}
		}
	}

	if(ignoredRowCount>0){
		Error_Clear();
		Error_Details(ignoredRows);				
		Error_Show();
		Error_Clear();
	}
}

function doredo(){
	mygrid.doRedo();
}

function doundo(){
	mygrid.doUndo();
}

function addRow(){
	mygrid.addRow(mygrid.getUID(),'');
}



function removeSelectedRow()
{
	var gridrowid=mygrid.getSelectedRowId();
	
	if(gridrowid!=undefined){
		var added_row;
		var gridrowsarr = gridrowid.toString().split(",");
						
		for (var rowid = 0; rowid < gridrowsarr.length; rowid++)
        {
			gridrowid = gridrowsarr[rowid];
			var selectedYear = mygrid.cellById(gridrowid,0).getValue();
            if(parseInt(selectedYear)>=parseInt(curyear) || parseInt(selectedYear.length) == 0)
			{
				mygrid.deleteRow(gridrowid);
				delYear=delYear+gridrowid+','
			}else
			{
				Error_Clear();
				Error_Details(" You cannot delete previous years records ");				
				Error_Show();
				Error_Clear();
			}
		}
	}else{
		Error_Clear();
		Error_Details(" Please Select a row to Delete.");				
		Error_Show();
		Error_Clear();
	}
}



function setRowAsModified(rowid,modified){
	mygrid.forEachCell(rowid,function(obj){
       obj.cell.wasChanged = modified;
		});
}

function checkForDuplicateRow(row_id){
	
	if(mygrid.doesRowExist(row_id)){
		return true;
	}
	
}

function checkForDuplicate(stage,rId,cInd,nValue,oValue){
	
	
	var year=mygrid.cellById(rId,0).getValue();
	
	
	var dupRow=false;
	
	//if((stage==0||stage==1) && ((parseInt(cInd) >= parseInt(curmonth)&& parseInt(year) == parseInt(curyear)) || ((parseInt(year) > parseInt(curyear)))))
	if((stage==0||stage==1) && parseInt(year) >= parseInt(curyear))
	{
		return true;
	}else if((stage==0||stage==1) && (parseInt(cInd) ==0 ) && year.length==0)
	{
		return true;
	}else if((stage==2) && (parseInt(cInd) ==0 ) && year.length>0)
	{
		
		if(checkForDuplicateRow(year))
		{
			Error_Clear();
			Error_Details(" Year already exist");				
			Error_Show();
			Error_Clear();
			return false;
		}else if(parseInt(year)<parseInt(curyear))
		{
			Error_Clear();
			Error_Details(" You cannot choose previous years");				
			Error_Show();
			Error_Clear();
			return false;
		}
		
		
		
		return true;
	}
	
	if(stage==2 && nValue!=oValue  ){
		
		
				if(!isNumeric(nValue))
				{
					Error_Clear();
					Error_Details(" Please enter the numeric value");				
					Error_Show();
					Error_Clear();
					return false;
				}
				
				if(parseInt(year) >= parseInt(curyear)){
				//Changing the row id with the newly selected value from name drop down.
				mygrid.changeRowId(rId,nValue);
				
				//Adding attribute to a row, so when any row is deleted,based on this flag it will be permanently deleted.
				//Otherwise we will highlight the deleted row with different color.
				mygrid.setRowAttribute(nValue,"row_added","Y");
				
				//Reset the rowcolor which was highlighted in case of row without any value in Name column.
				mygrid.setRowColor(nValue, '');	
				return true;
				}else
				{
				Error_Clear();
				Error_Details(" The year and month should be greater then this month of year");				
				Error_Show();
				Error_Clear();
				return false;
				}
					
		
	}
	
	return false;
}


function fnSetFocus(obj)
{
	obj.select();
	changeBgColor(obj,'#AACCE8');
}

function fnCalcTotal(obj,num)
{
	changeBgColor(obj,'#ffffff');
	Total = 0.0;
	var j = 0;
	for (var i=0;i<12;i++ )
	{
		j++;
		obj = eval("document.frmTerritory.Txt_"+j);
		if (obj.value != '')
		{
			Total = Total + parseFloat(RemoveComma(obj.value));
			obj.value = formatNumber(obj.value);
		}
	}
	document.all.Lbl_Total.innerHTML = "<b>$"+formatNumber(Total)+"</b>&nbsp;&nbsp;&nbsp;";
}


// For Bug-6099, Commented this function, As isNumeric function is already exist in GlobusCommonScript.js
/*function isNumeric(value) {
	  if (value != null && !value.toString().match(/^[-]?\d*?\d*$/)) return false;
	  return true;
	}*/

function initGridData(divRef,gridData){
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);
	gObj.enableTooltips("true,true,true,true,true,true,true,true,true,true,true,true,true,true");
	gObj.init();	
	gObj.loadXMLString(gridData);	
	return gObj;	
}
