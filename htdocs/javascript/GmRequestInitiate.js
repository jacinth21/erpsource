
var refids = "";
var refNm = "";
var status = true; 
var loanerReqValidMsg = "";//PMT-45058

function Reload (refid, refname, flag ) { 

	refid = trim(refid);
	
	var valobj = refids.split(","); 
	var arrlen = valobj.length;  
	if (arrlen > 0)
	{
		for (var j=0;j< arrlen;j++ )
		{ 	 
				if ( valobj[j] == refid)
				{
					status = false;
					break ; 
				} 
		}
	}
	else
	{
		 status = true; 
	}
	  
	if(flag=='-1' )
	 {	 
		refids = refids.replace(refid,"");
		 
	  }
	 else
	 {	 
		 if(status==true )
		 {	 
			 refids = refids  + refid + ",";
		 }
	 }
	
	refNm =  refname;
	var f = document.getElementById('iframe1'); 
	   
 	f.src  = document.frmRequestInitiate.RE_SRC.value + "&RefIDs=" + refids + "&shipTo=" +document.all.shipTo.value + "&names=" +  document.all.names.value + "&RefName=" + refname;

 	var divfr = document.getElementById('divf'); 
	if(refid!='')
	{ 
 		divfr.style.display="block"; 
	}   
 	f.src = f.src;   
	}

function addJavascript(jsname) {
var th = document.getElementsByTagName('head')[0];
var s = document.createElement('script');
s.setAttribute('type','text/javascript');
s.setAttribute('src',jsname);
th.appendChild(s);
}
//PMT-45058 Ability to link Loaner Request to Item consignment transaction
//this function is used for validating the valid request id 
function fnvalidateLoanerRefID(){ 
	 var loanerReqIdObj = document.frmRequestInitiate.loanerReqID;
	if(loanerReqIdObj != undefined){
		
	 var loanerReqId = loanerReqIdObj.value;
	 var accIdObj = document.frmRequestInitiate.Cbo_LoanToAcc;
	 var consignTypeObj = document.all.consignmentType;
	 var selConsignType = consignTypeObj.options[consignTypeObj.selectedIndex].value;//PBUG-4023
	 
	 
	 var accId = '';
	 
	 if(accIdObj != undefined){
		 accId = accIdObj.value;
	 }
	 if(accId == '0'){
		 accId = '';
	 }
	
	 lonerReqIdValidFl = false;
	 if(loanerReqId ==''){
		 loanerReqValidMsg='';
	 }else{
	 var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmAccountRequestInitiate.do?strOpt=fetchLoanerRefDetails&loanerReqID='+loanerReqId +'&accountId='+accId+'&reqFor='+selConsignType+'&randomId='+ Math.random());
	 dhtmlxAjax.get(ajaxUrl,function(loader){
		 loanerReqValidMsg = loader.xmlDoc.responseText;
	       if(loanerReqValidMsg != "") {
		 		Error_Details(loanerReqValidMsg);
		 	}
	       if (ErrorCount > 0)
		 	{
			Error_Show();
			Error_Clear();
			return false;
		 	}
	       lonerReqIdValidFl = true;
	 });
	 }
	} 
}
//PMT-45058
function fnSubmit(){
	addJavascript('/javascript/Message.js');
	addJavascript('/javascript/Error.js'); 
	var setid = document.getElementById("frmCart").contentWindow.fnGetSetID();
	var obj = document.all.consignmentType;
	var seltext = obj.options[obj.selectedIndex].text;
 	var bill = document.all.distributorId.value;

 	var purpose = document.frmRequestInitiate.inHousePurpose.value; 
	 flg = 'true';

	 if(purpose == 4000097){ //OUS Sales Replenishments	 
	 	// validate the Customer PO field. (here only enter the PO # - In Modify Shipping Details screen we can't edit the PO #)
		 var custPO = document.frmRequestInitiate.Txt_1006420;
		 if(custPO != undefined){
			 fnValidateTxtFld('Txt_1006420',message[5541]);
		 }
	 }
	 
	 if (purpose == 50061){ // ICT sameple
		var DistlistArr = Distlist.split(",");		
		for (var j=0;j< DistlistArr.length;j++ )
			{			
				if (bill != DistlistArr[j]){
					flg = 'false';
				}
				else{
					flg = 'true';
					break;
				}
			}
	 }
	if (flg == 'false'){		
		Error_Details(message[5033]);
	}
		
	var str = document.getElementById("frmCart").contentWindow.fnCreateOrderString();	

	document.frmRequestInitiate.hinputString.value = str;
		var attType = '';
		var attVal = '';
		str='';
		for(var i=0;i<attCnt;i++){
			var attrib= eval("document.frmRequestInitiate.h"+i);
			attType = attrib.value;
			var attrib1 = eval("document.frmRequestInitiate.Txt_"+attType);
			attVal = attrib1.value;
			if(attVal != '')
				str += attType + '^' + attVal + '|';  
		}
		document.frmRequestInitiate.hinputstr.value = str;
	fnValidate();
	fnValidateShipping();
	//fnValidateTissueShipping();
	if(strErrorWhType != ''){
		Error_Details(Error_Details_Trans(message[5558],strErrorWhType.substring(1, strErrorWhType.length)));
	}
	if(strErrorRwQty != ''){
		Error_Details(Error_Details_Trans(message[5559],strErrorRwQty.substring(1, strErrorRwQty.length)));
	}
	
	if(strErrorFG !=''){
		Error_Details(Error_Details_Trans(message[5560],strErrorFG.substring(1, strErrorRwQty.length)));
	}
	if(strErrorRW !=''){
		Error_Details(Error_Details_Trans(message[5561],strErrorRW.substring(1, strErrorRwQty.length)));
	}//PMT-45058 
	if(loanerReqValidMsg != "") {
 		Error_Details(loanerReqValidMsg);
 	}
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	if (document.all.names.value != 0 ){
		document.all.names.disabled = false;
	} 
	
	if(document.all.consignmentType.value == 102930){
		document.all.inHousePurpose.disabled = false;
	} 
	
	var Repobj = document.all.Cbo_LoanToASSORep;
	var confMsg = false;
	var submitFl = false;
	/*
	 * when the Loaner requested by Rep under his name we Need to display Confirmation Box , saying that Assoc Reps not selected.
	 * In other Scenarios need not display the confirmation box.
	 */
	if(Repobj != undefined && Repobj.value != ''){
		if(Repobj.value == '0' ){	
		   if(document.all.assocRep.style.display == 'block'){
			 if(confirm(message[5562])){	
					submitFl = true;
			 }		
		   }else{
			   submitFl = true;
		   }
		}else{
			submitFl = true;
		}	
	}else{
		    submitFl = true;
	}
	if(submitFl){ 
		if (seltext== 'Product Loaner' )
		{
		fnStartProgress('Y');
		}
		if(Repobj != undefined && Repobj.value == '0'){
				Repobj.value = '';
			}
	//	var tissueFlObj = document.getElementById('frmCart').contentWindow.document.all.hpartfamily;
		var tissueFlObj = eval("document.getElementById('frmCart').contentWindow.document.all.hTissueParts");
		var tissueSetFlObj = eval("document.getElementById('frmCart').contentWindow.document.all.hTissueSet");
		var tissueFlag="";
		if(tissueFlObj != undefined){
 			if(tissueFlObj.value!= null) 
 			{
			tissueFlag = TRIM(tissueFlObj.value);
 			}
		}
		
		tissueFlag= TRIM(tissueFlag);
		if(tissueSetFlObj != undefined && (tissueFlag==null || tissueFlag==""))
			{
 			if(tissueSetFlObj.value!= null)
 				{
 				
 				tissueFlag=TRIM(tissueSetFlObj.value);
 				
 				}
			
			}
		document.frmRequestInitiate.action = "/gmRuleEngine.do?RefName="+refNm+"&RefIDs="+refids;  
	 	document.frmRequestInitiate.hrefName.value = refNm; 
	 	document.frmRequestInitiate.haddInfo.value = document.getElementById("frmCart").contentWindow.fnGetAddInfo();
	 	document.frmRequestInitiate.tissueFl.value = tissueFlag; 
		document.frmRequestInitiate.strOpt.value = 'save';
		document.frmRequestInitiate.Btn_Submit.disabled = true;
		fnStartProgress();
		document.frmRequestInitiate.submit();
		
	}
 
}


function fnValidate(){
	var requestTypeVal = '';
	var requiredDate = '';
	var billTo = '';
	var shipTo = '';
	var inputstr = '';

	fnValidateDropDn('consignmentType',message[5542]);
	//fnValidateTxtFld('requiredDate',' Required Date ');
	fnValidateDropDn('inHousePurpose',message[5543]);

	inputstr = document.frmRequestInitiate.hinputString.value;	
	objPlannedDt = document.all.plannedDate;
	var palnnedDateDiffVal = dateDiff(todaysDate,objPlannedDt.value,format);
	requestTypeVal = document.frmRequestInitiate.consignmentType.value;
	var reqTypeName = document.frmRequestInitiate.consignmentType.options[document.frmRequestInitiate.consignmentType.selectedIndex].text;
	if (requestTypeVal == 4127)
	{
		fnValidateTxtFld('requiredDate',message[5544]);
	}
	else
	{
		fnValidateTxtFld('requiredDate',message[5545]);
	}
	 
 
	if(inputstr == ''){
			Error_Details(message[5034]);
		}
	
	if(objPlannedDt.value == ""){
		Error_Details(message[5035]);
	}
	if(objPlannedDt.value != ""){
		CommonDateValidation(objPlannedDt,format,Error_Details_Trans(message[5546],format));
	}
	if(objPlannedDt.value != ''){
		if(palnnedDateDiffVal < 0){
			Error_Details(message[5036]);
		}
	}
	
	
	if(requestTypeVal == '40021' || requestTypeVal == '102930'){
		 
		billTo = document.frmRequestInitiate.distributorId.value;
		if(billTo == '01'){
			Error_Details(Error_Details_Trans(message[5037],reqTypeName));
		}
	}

	if(requestTypeVal == '40022'){
		var objInHousePurpose = document.frmRequestInitiate.inHousePurpose;

		if (objInHousePurpose.disabled == true){
			Error_Details(message[5038]);	
		}
	
			fnValidateDropDn('inHousePurpose',message[5543]);
		//	fnValidateDropDn('employeeId',' Value ');
		
		billTo = document.frmRequestInitiate.distributorId.value;
		shipTo = document.all.shipTo.value;

		if(billTo != '01'){
			Error_Details(message[5039]);
		}
		
		if(shipTo != '4123'){
			Error_Details(message[5040]);
		}
	}
	if(requestTypeVal == '4127'){ // product loaner			
		var reqDate=document.all.requiredDate.value;	
		var dateDiffVal = dateDiff(todaysDate,reqDate,format);
		var strsetids='';
		var strsetid4dt='';
		var strsetid4date='';
		if (dateDiffVal > reqday || dateDiffVal < 0)
		{
			Error_Details(message[2003]);
		}
		
		for (i = 0;i<document.getElementById("frmCart").contentWindow.cnt ; i++)
		{			
			dateObj = eval("document.getElementById('frmCart').contentWindow.document.all.Txt_ReqDate"+i);	
			objpnum = eval("document.getElementById('frmCart').contentWindow.document.all.Lbl_Part"+i);		
			
			if (objpnum.innerHTML != '&nbsp;') {

				if (dateObj.value == '' )
				{
					strsetid4dt += objpnum.innerHTML+", ";
				}
				if (dateObj.value != '' )
				{
					shipdateDiff = dateDiff(todaysDate,dateObj.value,format);
					shipsurgerydateDiff = dateDiff(dateObj.value,reqDate,format);
					if (shipdateDiff > dateDiffVal || shipdateDiff < 0)
					{
						strsetids += objpnum.innerHTML+', ';										
					}
					if(shipsurgerydateDiff > shipday)
					{
						strsetid4date += objpnum.innerHTML+', ';
					}
				}
			}
			
		}
		if (strsetids !='')
		{			
			Error_Details(Error_Details_Trans(message[5547],strsetids.substr(0,strsetids.length-2)));
		}
		if (strsetid4dt !='')
		{			
			Error_Details(Error_Details_Trans(message[5548],strsetid4dt.substr(0,strsetid4dt.length-2)));
		}
		if (strsetid4date !='')
		{
			Error_Details(Error_Details_Trans(message[5549],strsetid4date.substr(0,strsetid4date.length-2)));
		}
		// Validate Account or Comments based on checkbox
		var chk_account = document.frmRequestInitiate.chk_account.checked;
		if (chk_account)
		{
			fnValidateTxtFld('txt_LogReason',message[5550]);
		}
		else
		{
			fnValidateDropDn('Cbo_LoanToAcc',message[5551]);
		}
		//fnValidateDropDn('Cbo_LoanToAcc',' Account ');
			fnValidateDropDn('Cbo_LoanToRep',message[5552]);
	}
	
	// validation added for GIMS
	var negQtyStr = '';
	var nonNumStr = '';
	var maxQtyStr = '';
	var empQtyStr = '';
	var stoQtyStr = '';
	var decimalCheck = '';
	for (i = 0;i<document.getElementById("frmCart").contentWindow.cnt ; i++){		
	
		var reqQtyObj = eval("document.getElementById('frmCart').contentWindow.document.all.Txt_Qty"+i);
		var stockQtyObj = eval("document.getElementById('frmCart').contentWindow.document.all.hStock"+i);
		var objpnum = eval("document.getElementById('frmCart').contentWindow.document.all.Lbl_Part"+i);

		var reqQtyVal = '';
		var stockQtyVal = '';	

		if ( reqQtyObj != undefined )
			reqQtyVal = TRIM(reqQtyObj.value);

		if (stockQtyObj != undefined )
			stockQtyVal = parseInt(stockQtyObj.value);

		var errFl = true;

		if (objpnum.innerHTML != '&nbsp;') {
			if(reqQtyVal <= 0){
				negQtyStr += objpnum.innerHTML +", ";
			}
			decimalCheck = reqQtyVal.replace(/[0-9]+/g,''); // Qty text box
			if (isNaN(reqQtyVal) || decimalCheck.length >0){   
				errFl = false;
				nonNumStr += objpnum.innerHTML +", ";		    	   	
		    }

			if(reqQtyVal ==''){
				empQtyStr += objpnum.innerHTML +", ";				
			}
			
			if(requestTypeVal == '102930'){ // Inter Company Transfer
				if(stockQtyVal <='0' || stockQtyVal ==''){
					stoQtyStr += objpnum.innerHTML +", ";					
				}
				if(stockQtyVal != '0' && stockQtyVal < reqQtyVal){
					if(errFl){
						maxQtyStr += objpnum.innerHTML +", ";
					}
					
				}
			}			
		}		
		}
	
	if (negQtyStr !=''){			
		Error_Details(Error_Details_Trans(message[5553],negQtyStr.substr(0,negQtyStr.length-2)));
	}
	if (nonNumStr !=''){			
		Error_Details(Error_Details_Trans(message[5554],nonNumStr.substr(0,nonNumStr.length-2)));
	}
	if (maxQtyStr !=''){			
		Error_Details(Error_Details_Trans(message[5555],maxQtyStr.substr(0,maxQtyStr.length-2)));
	}
	if (empQtyStr !=''){			
		Error_Details(Error_Details_Trans(message[5556],empQtyStr.substr(0,empQtyStr.length-2)));
	}
	if (stoQtyStr !=''){			
		Error_Details(Error_Details_Trans(message[5557],stoQtyStr.substr(0,stoQtyStr.length-2)));
	}
	//added for tissue parts shipping validation PMT-41499
	var purpose = document.frmRequestInitiate.inHousePurpose.value; 
 //PMT-40240 - Tissue Parts shipping validation in loaner and item consignment
	if((requestTypeVal == '40021' && purpose == '50060')|| requestTypeVal == '4127'){
	var reqTissueFlObj = eval("document.getElementById('frmCart').contentWindow.document.all.hTissueParts");
	var reqTissueSetFlObj = eval("document.getElementById('frmCart').contentWindow.document.all.hTissueSet");
	var tissueParts = reqTissueFlObj.value;	
	var tissueSet = reqTissueSetFlObj.value;
	   if(tissueParts.length > 0 || tissueSet.length >0){
		   fnValidateTissueShipping();
		   var tissueShipto = document.all.tissueShipTo.value;
		   if (tissueShipto != '4122'){ // not hospital
			var tissueShiptoidid = document.all.tissueAddressId.value;
				if (tissueShipto == '4121' && tissueShiptoidid != ''){ // sales rep address is hospital
				 var tissueAddress =	document.all.tissueAddressId.options[document.all.tissueAddressId.selectedIndex].text;
				 var tissueAddressArr = tissueAddress.split('-');
				  if(tissueAddressArr.length>0 && tissueAddressArr[0] != 'Hospital'){
					  Error_Details(message[10615]);
				  }			     
				}else{
					Error_Details(message[10615]);
				}
			}		   
	   }
	}
}


//Trims the leading and trailing spaces
function trim(str)
{
	str = new String(str);
	var intLen = str.length;
	var intBegin = 0;
	var intEnd = intLen - 1;
	while ((str.charCodeAt(intBegin) == 13 || str.charCodeAt(intBegin) == 10 || str.charAt(intBegin) == " ") && intBegin < intLen)
	{
		if (str.charCodeAt(intBegin) == 13 || str.charCodeAt(intBegin) == 10)
			intBegin+=2;
		else
        		intBegin++;
	}
	while ((str.charCodeAt(intEnd) == 13 || str.charCodeAt(intBegin) == 10 || str.charAt(intEnd) == " ") && intBegin < intEnd)
	{
		if (str.charCodeAt(intEnd) == 13 || str.charCodeAt(intBegin) == 10 )
			intEnd-=2;
		else
			intEnd--;
	}
	return str.substring(intBegin, intEnd+1);
}
/*
 * The below function is used to fetch the Assoc Reps based on the Selected Sales Rep.
 * 
 */
function fnSetAssocReps (RepId){
	j = 0;
	var Repobj = "";
	if(document.all.Cbo_LoanToASSORep){
		Repobj = document.all.Cbo_LoanToASSORep;
		Repobj.disabled = false;
		Repobj.options.length = 0;
		Repobj.options[0] = new Option("[Choose One]","0");
	}
	
	for (var i=0;i<AssociateSalesRepLen;i++)
	{
		/*
		 * First we are getting Assoc Rep Array ,it consists of Assoc Rep id,Assoc Rep Name,Rep Id and Rep Name .
		 */
		document.all.assocRep.style.display = "none";
		arr = eval("AssoRepArr"+i);
	
		arrobj = arr.split(",");
		associd = arrobj[1];//Assoc Rep id		
		assocname = arrobj[0];//Assoc Rep Name
		repid = arrobj[2];//Rep Id
		repname = arrobj[3];//Rep Name
		/*
		 * We are in need to display Sales Rep based on the Distributor ,here we are filtering the sales Rep.
		 */
			if(repid == RepId ){
				j++;
				Repobj.options[j] = new Option(assocname,associd);
				if(j == 1){
					aid = associd;
					aName = assocname;
				}
			}
	}
	/*
	 * We are in need to set Assoc Rep Name to default if there is only one Assoc Rep for the corresponding sales Rep.
	 */
	if((j == 1)){
		Repobj.options[j] = new Option(aName,aid);
		Repobj.value=aid;
	}
	//We need to display the Assoc Sales Rep Dropdown only if there is values in that dropdown.
	if(Repobj.options.length >= 2){
		document.all.assocRep.style.display = "block";
	}
	
	
}