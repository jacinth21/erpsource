
var tempPnum = '';
var blDiscFlag = false;
var partCnt=0;
function validate(cnt,obj) 
{
	//getPartDesc(cnt,obj); 
	var pnum = ''; 
	var qty = '';
	var accid = parent.document.all.Txt_AccId.value;
    var repid = parent.document.all.hRepId.value;
	var gpoid = document.frmCart.hGpoId.value;
	var opt = (parent.document.all.hOpt != undefined)?parent.document.all.hOpt.value:'';       //Adding opt is PHONE changes to add new row automatically while entering parts in end of the row of cart in new order in PC-3637
	if (gpoid == '')
	{
		gpoid = '0';
	}

    if (accid == 0)
    {
    	alert(message[10564]);
    }
    else
    {
	    trcnt = cnt;
		var pnumobj = eval("document.frmCart.Txt_PNum"+trcnt);
		var qtyobj = eval("document.frmCart.Txt_Qty"+trcnt);
		var priceobj = eval("document.frmCart.Txt_Price"+trcnt);
	    changeBgColor(obj,'#ffffff');

		if (pnumobj.value != '' && qtyobj.value != '')
		{

		   if (priceobj.value != '')
		   {
				fnCalExtPrice(priceobj,trcnt,'Y');
		   }
		   else
		   {
			   pnum = pnumobj.value.toUpperCase();
			   qty = qtyobj.value;
			   tempPnum = pnum; 
			   //PC-3867- Adding fromScreenPage parameter in below Url 
			   var url = fnAjaxURLAppendCmpInfo("/GmCartAjaxServlet?pnum=" + encodeURIComponent(pnum)+ "&qty=" + encodeURIComponent(qty)+ "&acc="+encodeURIComponent(accid)+ "&rep="+encodeURIComponent(repid)+ "&gpoid="+encodeURIComponent(gpoid)+ "&regexp=NO"+"&fromScreenPage=SALESORDER&"+fnAppendCompanyInfo());
			  // var url = "ValidateServlet";
			   if (typeof XMLHttpRequest != "undefined") {
				   req = new XMLHttpRequest();
			   } else if (window.ActiveXObject) {
				   req = new ActiveXObject("Microsoft.XMLHTTP");
			   }
			  
			   //req.open("POST", url, true);
			   req.open("GET", url, false);
			   req.onreadystatechange = callback;
			
			   //req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			   //req.send("id=" + encodeURIComponent(obj.value));
			  req.send(null); 
			  
		   }
		     //vhNewRowLot added for new row automatically cursor focus on Control number field in Italy and japan PC-4726 - New order - IE Edge changes
			var vhNewRowLot = '';
			 if(parent.document.all.hNewRowLot != undefined)
			{
			  vhNewRowLot = parent.document.all.hNewRowLot.value;
			}
			
		   //Adding below changes to add new row automatically while entering parts in end of the row of cart     
		   if(opt =='PHONE' || opt =='PROFORMA'){		   //Adding opt is PHONE changes to add new row automatically while entering parts in end of the row of cart in new order in PC-3637
		    var hrowcnt = (document.frmCart.hRowCnt != undefined)?document.frmCart.hRowCnt.value:'';  
		    var obj = null;
			   if(trcnt == (hrowcnt-1)){
			    fnAddRow('PartnPricing');		    
				    if(vhNewRowLot == 'YES' ) {     // for Italy and japan 
	  				    if(eval("document.all.Txt_Cnum"+hrowcnt-1) != undefined){
	  					    obj = eval("document.all.Txt_Cnum"+hrowcnt-1);
	  					    obj.value = '';
	  				   }
	  			   }else{
	  				 if(eval("document.all.Txt_PNum"+hrowcnt) != undefined){
	                     obj = eval("document.all.Txt_PNum"+hrowcnt);
	                     obj.value = '';
	                     setTimeout(function() {
	                                   obj.focus();
	                                   }, 0);
	  			        }
	  			 }
			   }
		   }
		} // end of 'if partnum and qty != 0'
	}// end of 'if account id is null'
}

function callback() 
{ 	 
	if (req.readyState == 1) {
	 var obj = eval("document.all.Lbl_Stock"+trcnt);
	    obj.innerHTML = '<img src="/images/progress.gif" style="width:25px;height:10px;" alt=""/>' 

		}
	
    if (req.readyState == 4) {
        if (req.status == 200) {
	        var xmlDoc = req.responseXML;
	        
	        parseMessage(xmlDoc);
	        var priceobj = eval("document.frmCart.Txt_Price"+trcnt);
	        fnCalExtPrice(priceobj,trcnt);
	        if((showDiscount == 'YES') && blDiscFlag != false){
	        	fnCalculateDiscount();
	        }
        }
        else
        {
	        setErrMessage('<span class=RightTextBlue>'+message[10566]+'</span>');
        }
       
    }
}

function getPartDesc(cnt,obj) 
{
	
	var pnum = ''; 	
	var accid = parent.document.all.Txt_AccId.value;   

    if (accid == 0)
    {
    	alert(message[10564]);
    }
    else
    {
    	partCnt = cnt;
		var pnumobj = eval("document.frmCart.Txt_PNum"+partCnt);		
	    changeBgColor(obj,'#ffffff');

		if (pnumobj.value != '')
		{
		  
			   pnum = pnumobj.value.toUpperCase();			 
			   var partdescurl = fnAjaxURLAppendCmpInfo("/GmCartAjaxServlet?hAction=fetchPartDesc&pnum=" + encodeURIComponent(pnum)+"&"+fnAppendCompanyInfo());
			  
			   if (typeof XMLHttpRequest != "undefined") {
				   reqDesc = new XMLHttpRequest();
			   } else if (window.ActiveXObject) {
				   reqDesc = new ActiveXObject("Microsoft.XMLHTTP");
			   }
			   reqDesc.open("GET", partdescurl, false);
			   reqDesc.onreadystatechange = partdesccallback;	
			   reqDesc.send(null); 
			  
		    
		} // end of 'if partnum and qty != 0'
	}// end of 'if account id is null'
}

function partdesccallback() 
{
	 if (reqDesc.readyState == 4) {
        if (reqDesc.status == 200) {
	        var pdesc = reqDesc.responseText;
	       var obj = eval("document.all.Lbl_Desc"+partCnt);
	        if (pdesc != null && pdesc != '' && pdesc.length > 2)
	    	{
	        	
		    	obj.innerHTML = "&nbsp;" + pdesc;
	        	
	    	}else{	    	
	    		setPartDescErrMessage('<span class=RightTextBlue>'+message[10566]+'</span>');
	    		return;	    	
	    	}
        }
        else
        {
        	setPartDescErrMessage('<span class=RightTextBlue>'+message[10566]+'</span>');
        }
        
    }
}

function setPartDescErrMessage(msg) 
{
    var obj = eval("document.all.Lbl_Desc"+partCnt);
    obj.innerHTML = msg;
    
    obj = eval("document.all.Txt_Qty"+partCnt);
    obj.value = '';

    obj = eval("document.all.Txt_PNum"+partCnt);
    obj.value = '';
    obj.focus(); 
    
    if(opt == 'PHONE'){
    	// Remove the image if there are any errors
    	fnRemoveImage(partCnt);
    }
}


function parseMessage(xmlDoc) 
{
	var strarr = 'pdesc,stock,price,loanerstatus,lprice,loancogs,rfl,ctrl_needed_fl,unitprice,unitadj,adjcode,waste,revision,product_material';
	var pnum = xmlDoc.getElementsByTagName("pnum");
	var opt = (parent.document.all.hOpt != undefined)?parent.document.all.hOpt.value:'';
	var pNumValidObj = document.getElementById("hPnumValid"+trcnt);
	var pNumProjValidObj = document.getElementById("hPnumProjValid"+trcnt);
	if(pNumValidObj!=undefined && pNumValidObj!=null)
		pNumValidObj.value = '';
	var txtObj = document.getElementById("DivShowCnumNotExists"+trcnt);
	var ordertype = parent.document.all.Cbo_OrdType.value;
	var validmsg = '';
	
	var plantValid = xmlDoc.getElementsByTagName("plantValid");
	// PMT-24605 To display error if pnum is 0
	if (pnum.length = 0)
	{
		setErrMessage('<span class=RightTextBlue>'+message[10566]+'</span>');
		return;
	}
	// PMT-24605 PlantValid value is passed
	if(plantValid[0])
		validmsg = parseXmlNode(plantValid[0].childNodes[0].firstChild);
	if (plantValid[0] && validmsg.length != 0)
	{
		if(txtObj!=undefined && txtObj!=null && pNumValidObj!=undefined && pNumValidObj!=null){
			txtObj.style.display='';
			txtObj.title= validmsg;
			pNumValidObj.value = validmsg;
		}
	}else{
		if(txtObj!=undefined && txtObj!=null){
			txtObj.style.display='none';
		}
	}
	// pNumProjValidObj value we are setting here
	if(pNumProjValidObj != undefined && pNumProjValidObj != null)
		pNumProjValidObj.value = '';
		if(plantValid[0] && validmsg == 'TISSUEPART'){
			if(pNumProjValidObj != undefined && pNumProjValidObj != null){
				txtObj.style.display='';
				txtObj.title= validmsg;
				//pNumValidObj.value = validmsg;
				pNumProjValidObj.value = validmsg;
	            validmsg = '';
	            //PMT-40657 Tissue parts ship changes in Bill and ship orders
	            //to remove the red cross mark before giving the lot #
	            fnRemoveImage(trcnt);
			}
		}
	var strobj =  strarr.split(',');
	var strlen = strobj.length;
	var strresult = '';

	for (var i=0;i<strlen ;i++ )
	{
		var str = strobj[i];
		for (var x=0; x<pnum[0].childNodes.length; x++) 
		{
			xmlstr = pnum[0].childNodes[x].tagName;
			if (xmlstr == str)
			{
				if(pnum[0].childNodes[x].firstChild){
					strresult = strresult + '^' + parseXmlNode(pnum[0].childNodes[x].firstChild);
					break;
				}else{
					strresult = strresult + '^' ;
				}
			}
		}
	}

	strresult = strresult.substr(1,strresult.length);
	var strobj =  strresult.split('^');

	var pdesc = strobj[0];
	var stock = strobj[1];
	var price = strobj[2];
	var loaner = strobj[3];
	var list = strobj[4];
	var lcogs = strobj[5];
	var releasefl = strobj[6];
	var ContNumNeedfl = strobj[7];
	var unitprice = strobj[8];
	var unitadj = strobj[9];
	var adjcode = strobj[10];
	var waste = strobj[11];
	var revision = strobj[12];
	var productMat = strobj[13];
	obj = eval("document.frmCart.Txt_Qty"+trcnt);
	qty = parseInt(obj.value);
	var amount;
	
	
	//For quote, we are using list price instead of account price
	if(opt =='PHONE' || opt =='PROFORMA')
		amount = qty*price;
	else
		amount = qty*list;

	if (pdesc == null)
	{
		setErrMessage('<span class=RightTextBlue>'+message[10566]+'</span>');
		return;
	}
	else if (releasefl == 'N')
	{
		setErrMessage('<span class=RightTextBlue>'+message[10567]+'</span>');
		return;
	}
 	else if (list == 0)
	{
		setErrMessage('<span class=RightTextBlue>'+message[10568]+'</span>');
		return;
	}
/*	else if (price == '')
	{
		setErrMessage('<span class=RightTextBlue>Price is not set for this Part for this Account</span>');
		return;
	}*/
	else
	{
		obj = eval("document.frmCart.hLoanCogs"+trcnt);
		obj.value = lcogs;

		obj = eval("document.frmCart.Cbo_OrdPartType"+trcnt);

		// IF part present in loaner costing layer then, to make it as Order type as disable
		// for the Order type 26240232:Direct, 26240233:Intercompany and 26240236: Bill and Hold
		if (lcogs == '' || (ordertype == 26240232 || ordertype == 26240233 || ordertype == 26240236))
		{
			obj.value = 50300;
			obj.disabled = true;
			ordtype = parent.document.all.Cbo_OrdType.value;
			if (ordtype == '2530')
			{
				Error_Details(message[10565]);
				parent.document.all.Cbo_OrdType.value = 2521;
				Error_Show();
				Error_Clear();
				fnResetLoanerFields(trcnt);
			}
		}
		else
		{
			if(ordertype == '2530' || ordertype == '2532'){//Bill Only Loaner and Bill Only - From Sales Consignments
				obj.disabled = true;
			}else{
			obj.disabled = false;
			}
		}
		setMessage(pdesc,stock,price,amount,loaner,list,unitprice,unitadj,adjcode,waste,revision);
	}
	window.parent.Reload(tempPnum, '1', ordertype);
	if(ContNumNeedfl != undefined && ContNumNeedfl !='' && ("document.all.hCntrlNumberNeededFl"+trcnt) != undefined){
		obj = eval("document.all.hCntrlNumberNeededFl"+trcnt);
		if(obj!=undefined && obj!=null)
			obj.value = ContNumNeedfl;
	}
	if(productMat != undefined && productMat !='' && ("document.all.hproductMat"+trcnt) != undefined){
		obj = eval("document.all.hproductMat"+trcnt);
		if(obj!=undefined && obj!=null)
			obj.value = productMat;
	}
	
}
 
function setMessage(pdesc,stock,price,amount,loaner,list,unitprice,unitadj,adjcode,waste,revision) 
{
	var opt = parent.document.all.hOpt.value;
	
	var obj = eval("document.all.Lbl_Desc"+trcnt);
	obj.innerHTML = "&nbsp;" + pdesc;

    obj = eval("document.all.Lbl_Stock"+trcnt);
    obj.innerHTML = stock + "&nbsp;&nbsp;";

	obj = eval("document.all.Txt_unit_price"+trcnt);
	if(obj!=undefined && obj!=null){
		obj.value = formatNumber(unitprice) ;
	}
	
	obj = eval("document.all.hUnitPartPrice"+trcnt);
	if(obj!=undefined && obj!=null){
		obj.value = formatNumber(unitprice);
	}

	obj = eval("document.all.Txt_unit_adj"+trcnt);
	if(obj!=undefined && obj!=null){
		obj.value = formatNumber(unitadj) ;
	}

	obj = eval("document.all.Cbo_adj_code"+trcnt);
	if(obj!=undefined && obj!=null){
		obj.value = adjcode;
	}
	
	obj = eval("document.all.hWaste"+trcnt);
	if(obj!=undefined && obj!=null){
		obj.value = waste;
	}
	
	obj = eval("document.all.hRevision"+trcnt);
	if(obj!=undefined && obj!=null){
		obj.value = revision;
	}

    obj = eval("document.all.Txt_BasePrice"+trcnt);
    if(obj!=undefined && obj!=null){
    	obj.value =  (opt == 'QUOTE')?formatNumber(list ):formatNumber(price ); //list price is needed for QUOTE
    }
    
    obj = eval("document.all.hBasePrice"+trcnt);
    if (opt == 'QUOTE'){ 
	    obj.value = formatNumber(list );
    }else{
    	obj.value = formatNumber(price );
    }
    
    obj = eval("document.frmCart.Txt_Price"+trcnt);
    if (opt == 'QUOTE') // Quote module. Price is taken from T205 rather than from T507
    {
    	obj.value = formatNumber(list);
    }
    else 
    {
    	obj.value = formatNumber(price);
    }
    
    obj = eval("document.all.Lbl_Amount"+trcnt); 
    obj.innerHTML = formatNumber(amount) + "&nbsp;";

    obj = eval("document.all.hPartPrice"+trcnt);
    obj.value = formatNumber(price);
    
    obj = eval("document.all.hNetPartPrice"+trcnt);
    obj.value = formatNumber(price);
    
	fnCalculateTotal();
}

function setErrMessage(msg) 
{
    var obj = eval("document.all.Lbl_Desc"+trcnt);
    obj.innerHTML = msg;
    
    obj = eval("document.all.Txt_Qty"+trcnt);
    obj.value = '';

    obj = eval("document.all.Txt_PNum"+trcnt);
    obj.value = '';
    obj.focus(); 
    
    if(opt == 'PHONE'){
    	// Remove the image if there are any errors
    	fnRemoveImage(trcnt);
    }
}

var varPartNum;
var varCount;
var objPartCell;

function fnAddRow(id)
{ 
	var opt = (parent.document.all.hOpt != undefined)?parent.document.all.hOpt.value:'';
	
	var tbody = document.getElementById(id).getElementsByTagName("TBODY")[0];
    var row = document.createElement("TR")

    var td0 = document.createElement("TD")
    td0.innerHTML = cnt+1;

    var td1 = document.createElement("TD"); 
    td1.innerHTML = fnCreateCell('TXT','PNum',10); //PC-2067 New Order/ACK Order screen UI changes
   
    var td2 = document.createElement("TD")
    td2.innerHTML = fnCreateCell('TXT','Qty',2);
    td2.align = "center";
    
    var td3 = document.createElement("TD")
    td3.innerHTML = fnCreateCell('TXT','Price',8);
    td3.innerHTML = td3.innerHTML + '<input type="hidden" name="hBasePrice' + cnt + '" value="">';
    td3.align = "Right";
    
    var td4 = document.createElement("TD")
	td4.id = "Lbl_Stock"+cnt;
    td4.align = "Right";
    
    var td5 = document.createElement("TD")
	td5.id = "Lbl_Amount"+cnt;
    td5.align = "Right";
    
    var td6 = document.createElement("TD")
	td6.id = "Lbl_Desc"+cnt;
    td6.align = "Left";

	var td17 = document.createElement("TD")
	td17.innerHTML = fnCreateCell('TXT','UnitPrice',7);
	td17.align = "Right";

	var td18 = document.createElement("TD")
	td18.innerHTML =  fnCreateCell('TXT','UnitAdj',7);
	td18.align = "Right";

	var td19 = document.createElement("TD")
	td19.innerHTML = fnCreateCell('Cbo','AdjCode',10);
	td19.align = "center";

    if(opt == 'QUOTE'){
    	var tdhid = document.createElement("TD")
    	tdhid.colSpan='3';    	
    	tdhid.innerHTML = '<input type=hidden name="Chk_BO' + cnt + '" tabindex="-1">';
    	tdhid.innerHTML = tdhid.innerHTML + '<input type="hidden" name="hLoanCogs' + cnt + '" value="">';
    	tdhid.innerHTML = tdhid.innerHTML + '<input type="hidden" name="Cbo_OrdPartType' + cnt + '" value="50300">';
    	tdhid.innerHTML = tdhid.innerHTML + '<input type="hidden" name="Txt_RefId' + cnt + '" value="">';
    	tdhid.innerHTML = tdhid.innerHTML + '<input type="hidden" name="Chk_Cap' + cnt + '" value="">';
    }
    
    if(vcountryCode != 'en'){//Base Price is not needed for US
		var td14 = document.createElement("TD")
	   	td14.innerHTML = fnCreateCell ('TXT','BasePrice', 8);
		td14.align = "right";
    }
    if(opt != 'QUOTE'){
	    var td7 = document.createElement("TD")
	    td7.innerHTML = fnCreateCell('CHK','BO',1);
	    td7.align = "center";
	
	    var td8 = document.createElement("TD")
	    td8.innerHTML = fnCreateCell('Cbo','OrdPartType',1);
	    td8.align = "center";
	
	    var td9 = document.createElement("TD")
	    td9.innerHTML = fnCreateCell('CHK','Cap',1);
	    td9.align = "Center";
	    var td11 = document.createElement("TD")
		td11.innerHTML = fnCreateCell('TXT','RefId',7);
		// MNTTASK-8623 - Added Control Number Column
		var td12 = document.createElement("TD")
	   	td12.innerHTML = fnCreateCell ('TXT','Cnum', 28);
    }
    
    var td10 = document.createElement("TD")
    td10.innerHTML = fnCreateCell('IMG','',1);
    
    row.appendChild(td0); //TD
	row.appendChild(td10); //IMG
	row.appendChild(td1); //Part num
    row.appendChild(td2); //Quantity
    row.appendChild(td4); //Stock
    if(opt!='QUOTE'){
    	row.appendChild(td7); //BO
	}
    row.appendChild(td6); //Part Desc
    if(opt!='QUOTE'){
	    row.appendChild(td8); //Type
	//    if (document.getElementById("col9").value =='1') row.appendChild(td11); //Ref id
	//    if (document.getElementById("col10").value =='1') row.appendChild(td9); //Cap
    }else{
    	row.appendChild(tdhid);
    }
    if(vcountryCode != 'en')
    	row.appendChild(td14); //Base Price
    
	row.appendChild(td17); //Unit Price
	row.appendChild(td18); //Unit Price Adj
	//alert(td17);
	row.appendChild(td19); //Adj Code

    row.appendChild(td3); //Price EA
    row.appendChild(td5); //Ext Price
    //alert('  test '+ document.getElementById("col15").value);
    if( (opt=='PHONE')){
    	 row.appendChild(td12); //Cnum
    }

	 if(opt!='QUOTE'){ 
	    if (document.getElementById("col16") != undefined && document.getElementById("col16").value =='1') row.appendChild(td11); //Ref id
	    if (document.getElementById("col17") != undefined && document.getElementById("col17").value =='1') row.appendChild(td9); //Cap
    }

    tbody.appendChild(row);
    
    var row1 = document.createElement("TR");
    var td20 = document.createElement("TD"); 
    td20.colSpan =16;
    td20.className="LLine";
    td20.height="1";
    row1.appendChild(td20);
    tbody.appendChild(row1);
    
	cnt++;
	document.frmCart.hRowCnt.value = cnt;
}

function fnCreateCell(type,val,size)
{
	//alert(+loanCogsChk);
	param = val;
	val = val + cnt;
	var html = '';
	var opt = (parent.document.all.hOpt != undefined)?parent.document.all.hOpt.value:'';
	if (type == 'TXT')
	{
		if (param == 'PNum') {
				html = '<input type=text size='+size+' name=Txt_'+val+' class=InputArea onBlur=fnSetPartSearch('+cnt+',this); onFocus=changeBgColor(this,\'#AACCE8\'); value=\'\'>';
						}
		else if (param == 'Qty') {
				html = '<input type=text size='+size+' name=Txt_'+val+' class=InputArea onkeypress="return fnIsNumeric(event);" onblur=validate('+cnt+',this); onFocus=changeBgColor(this,\'#AACCE8\'); value=\'\'>';
						}
		else if (param == 'Price') {
				html = '<input type=text size='+size+' name=Txt_'+val+' class=InputArea  style=\'text-align: right;\' onBlur=fnCalExtPrice(this,'+cnt+',\'Y\'); onFocus=changeBgColor(this,\'#AACCE8\'); tabindex=\'-1\'  value=\'\'><input type=hidden name=hPartPrice'+cnt+'><input type=hidden name=hNetPartPrice'+cnt+'>';
						}
		else if (param == 'RefId') {
				html = '<input type=text size='+size+' name=Txt_'+val+' class=InputArea onBlur=fnChkValidFieldVal(this,'+cnt+'); tabindex=\'-1\' onFocus=changeBgColor(this,\'#AACCE8\'); value=\'\'>';
						}
		else if (param == 'Cnum') {
			
			var objOrderType =  (parent.document.all.Cbo_OrdType != undefined)?parent.document.all.Cbo_OrdType.value:'';
			
			// Providing tabindex access for control number column based on 'cntrlTabIndexFl' variable
			
			if(cntrlTabIndexFl == 'Y')  // If condition is 'true' providing tab permisson to control number column
				{
				html = '<input type=text size='+size+' name=Txt_'+val+' class=InputArea style="text-transform: uppercase;width: 80%;" onBlur=\"fnValidateCtrlNum('+cnt+',this);changeBgColor(this,\'#ffffff\');\"  onFocus=\"fnRemoveImage('+cnt+',this);changeBgColor(this,\'#AACCE8\');\" value=\'\'>' ;
				}
			
			else{  // If condition is 'false' and order types are Bill & Hold, Direct and Intercompany then control number column will be diabled.
				
					if(objOrderType == 26240236 || objOrderType == 26240232 || objOrderType == 26240233)
					{
						html = '<input type=text size='+size+' disabled name=Txt_'+val+' class=InputArea style="text-transform: uppercase;width: 80%;" onBlur=\'fnValidateCtrlNum('+cnt+',this)\' tabindex=\'-1\' onFocus=\'fnRemoveImage('+cnt+',this)\' value=\'\'>' ; 
						
					}
					
					else{    // If condition is 'false' and order types are other than above then contol number column will be enabled and tab permission will also be restricted. 
						
						html = '<input type=text size='+size+' name=Txt_'+val+' class=InputArea style="text-transform: uppercase;width: 80%;" onBlur=\'fnValidateCtrlNum('+cnt+',this)\' tabindex=\'-1\' onFocus=\'fnRemoveImage('+cnt+',this)\' value=\'\'>' ;
					}
				}
				html = html + '<input type=hidden name=hCntrlNumberNeededFl'+cnt+'> <span id=DivShowCnumAvl'+cnt+' style=\'vertical-align:middle; display: none;\'> <img tabindex=\'-1\' height=16 width=19 src=/images/success.gif></img></span>';
				html = html + '<span id=DivShowCnumNotExists'+cnt+' style=\'vertical-align:middle; display: none;\'> <img height=12 width=12 src=/images/delete.gif></img>';
				if(cmpnyRule == 'YES'){
					html = html + '&nbsp;<a href=javascript:fnViewLotReport('+cnt+')><img height=15 border=0 title=\'Lot Code Report\' src=/images/location.png></img></a>';
				}
				html = html + '</span><input type=hidden name=hPnumValid'+cnt+' id=hPnumValid'+cnt+' value=\'\'>';
				html = html + '</span><input type=hidden name=hPnumProjValid'+cnt+' id=hPnumProjValid'+cnt+' value=\'\'>';
				html = html + '</span><input type=hidden name=hproductMat'+cnt+' id=hproductMat'+cnt+' value=\'\'>';
				}
		else if(param == 'BasePrice'){
				html = '&nbsp;<input type=text size='+size+' name=Txt_'+val+' class=InputArea onBlur=fnCalExtPrice(this,'+cnt+',\'Y\'); onFocus=changeBgColor(this,\'#AACCE8\'); tabindex=\'-1\' value=\'\'>';
					}
		else if(param == 'UnitPrice'){
				html = '<input type=text tabIndex =\'-1\' size= 7 readonly id=Txt_unit_price'+cnt+' name=Txt_unit_price'+cnt+' style=\'border: none;text-align: right\'><input type=hidden name=hUnitPartPrice'+cnt+'>';
		}
		else if(param == 'UnitAdj'){
			html = '<input type=text tabIndex = \'-1\' size= 7 readonly id=Txt_unit_adj'+cnt+' name=Txt_unit_adj'+cnt+' style=\'border: none;text-align: right\'>';
			html = html + '<input type=hidden name=hWaste'+cnt+' >';
			html = html + '<input type=hidden name=hRevision'+cnt+' >';			
		}
	}
	
	else if (type == 'CHK')
	{
		var objOrderType = (parent.document.all.Cbo_OrdType != undefined)?parent.document.all.Cbo_OrdType.value:'';
		
		if(objOrderType == 2530)  // If order type is Bill Only - Loaner[2530], BO Flag column will be disbaled  
			{
			html = '<input type=checkbox disabled name=Chk_'+val+' tabindex="-1">';
			}
		else{
			html = '<input type=checkbox name=Chk_'+val+' tabindex="-1">';
			}
		
	
		if (param == 'BO')
		{
			html = html + ' <input type=hidden name=hLoanCogs'+cnt+' value="">';
		}
	}
	
	else if (type == 'Lbl')
	{
		html = '&nbsp;';
	}
	
	else if (type == 'Btn')
	{
		var fn = size;
		html = '<input type=button onclick='+fn+' value = PartLookup name=Btn_'+val+' class=Button>';
	}
	
	else if (type == 'Cbo')
	{
	var fn = size;
	if (param == 'AdjCode')
		{
		html = '<select name=Cbo_adj_code'+cnt+' class=RightText tabindex="-1" onChange="fnGetUnitPrice(this ,'+cnt+');"><option value="0" selected>N/A</option><option value="107970">Waste</option><option value="107971" >Revision</option></select>';
		}
	else
		{
			var objOrderType = (parent.document.all.Cbo_OrdType != undefined)?parent.document.all.Cbo_OrdType.value:'';
			
			if(objOrderType==2530 )  // If order type is Bill Only - Loaner[2530], Type column will be disbaled and defult select option will be 'L'  
			{
				html = '<select name=Cbo_'+val+' disabled class=RightText tabindex="-1"><option value="50300">C</option><option value="50301" selected>L</option></select>';
				
			}
			// If order type are Bill Only - From sales Consignments[2532], Bill & Hold[26240236], Direct[26240232] and Intercompany[26240233], Type column will be disbaled and defult select option will be 'C' 
			
			else if(objOrderType == 2532 || objOrderType == 26240236 ||  objOrderType == 26240232 || objOrderType == 26240233){
				
				html = '<select name=Cbo_'+val+' disabled class=RightText tabindex="-1"><option value="50301">L</option><option value="50300" selected>C</option></select>';
			}
			
			// If order type are not in above mentioned, Type column will be enabled and select options will be user choice
			else
			{
				html = '<select name=Cbo_'+val+'  class=RightText tabindex="-1" onchange="fnChangeStatus(this.value,'+cnt+');"><option value="50300">C</option><option value="50301">L</option></select>';
			}		
		}
	}

	else if (type == 'IMG')
	{
		var fn = size;
		html = '<a href=javascript:fnRemoveItem('+val+') tabindex=\'-1\'; ><img border=0  Alt=Remove from cart valign=left src=/images/btn_remove.gif height=10 width=9></a>';
	}
	
//	alert(html);
	return html;
}

function fnOpenPart()
{
	var varAccId = parent.document.all.Txt_AccId.value;
	if (objPartCell == null)
	{
		varPartNum = "";
	}
	else
	{
		varPartNum = objPartCell.value; 
		windowOpener("/GmSearchServlet?hAction=SearchPart&Txt_PartNum="+encodeURIComponent(varPartNum)+"&hCount="+varCount+"&Txt_AccId="+varAccId,"Search","resizable=yes,scrollbars=yes,top=340,left=420,width=600,height=435");
	}
}

function fnSetPartSearch(count,obj)
{
	getPartDesc(count,obj);
	objPartCell = eval("document.frmCart.Txt_PNum"+count);
	eval("document.frmCart.Txt_PNum"+count).value = objPartCell.value.toUpperCase();
	objPartQty = eval("document.frmCart.Txt_Qty"+count);
	objRefId = eval("document.frmCart.Txt_RefId"+count);
	if(objRefId != undefined && objRefId != null)
		objRefId.value = '';
	changeBgColor(obj,'#FFFFFF');
	if (objPartQty.value != '')
	{
		var priceobj = eval("document.frmCart.Txt_Price"+count);
		priceobj.value = '';
		validate(count,obj);
	}
	
}

function fnClearVal(count)
{
	varCount = count;
	objPartCntCell = eval("document.frmPhOrder.hPrice"+varCount);
	if (objPartCntCell == "" || objPartCntCell == null) 
	{
	}

	objPartCntCell.value = 0;
}

function fnReturnStatus(status)
{
	if (status)
	{
		return 'Y';
	}
	else
	{
		return '';
	}
}

function fnCreateOrderString()
{
	
	if (showDiscount == 'YES') {
		// MNTTASK - 3603 SA - Ability to calculate discount for Quotes - Amount Validation
		var  strDiscountIndex = parent.document.all.hDiscountIndex.value;
		var objDiscountVal = eval("parent.document.frmPhOrder.Txt_Quote"+strDiscountIndex).value ;
		var cnt = document.frmCart.hRowCnt.value;
		var strOpt = parent.document.all.hOpt.value;
		var listPrice = 0.00;
		var DisEAPRice = 0.00;
		objDiscountVal =(objDiscountVal == '' || objDiscountVal == undefined)?0:objDiscountVal;
		var AccDiscount = (parent.document.all.hAccDiscnt != undefined)?parent.document.all.hAccDiscnt.value:0;
		objDiscountVal = parseFloat(formatNumber(objDiscountVal)) + parseFloat(formatNumber(AccDiscount));
		objDiscountPercentage = trim(objDiscountVal)!=''?(parseFloat(objDiscountVal)/100):parseFloat(0);
		var lbl_Total = formatNumber(document.frmCart.hTotal.value);
		if(objDiscountVal == '')
		{
			var btn_calc_discount = eval("document.frmCart.Btn_Calculate");
			btn_calc_discount.disabled = true;
			objDiscountVal.value=0;
			//fnCalculateDiscount();
		}
		for (i=0;i<cnt;i++)
		{
			pobj = eval("document.frmCart.Txt_PNum"+i);
			if(pobj.value != '')
			{
				if(vcountryCode == 'en'){// For US, we are using hBasePrice to calculate the discount, doesn't have text field
					// MNTTASK-4744 - Discount Calculation : Added Price EA column in the UI. Always we have to get txtR_price value to DB.
					priceEAObj = eval("document.frmCart.Txt_unit_price"+i);// Base price to calculate the discount
				}else{
					priceEAObj = eval("document.frmCart.Txt_BasePrice"+i);
				}
				listPrice = eval("document.all.Txt_Price"+i).value; //Getting the Price EA value to compare with the new discount calculated while submitting
				TotalExtPrice = eval("document.all.Lbl_Amount"+i).innerHTML;
				qty = eval("document.frmCart.Txt_Qty"+i).value;

				DisEAPRice =parseFloat(RemoveComma(priceEAObj.value)) - (parseFloat(RemoveComma(priceEAObj.value) * objDiscountPercentage));

				TotalExtPrice = TotalExtPrice.substring(0,TotalExtPrice.indexOf('&nbsp;'));
				
				// Once submit and the input string error value it's goto Phoneorder.js. 
				if(!blDiscFlag && objDiscountVal != 0){
					inputstr = "ApplyAcc_Err";
					if(lbl_Total != '0.00'){
						return inputstr;
					}
				}
				if(objDiscountPercentage!=0)
				{
					if(formatNumber(DisEAPRice) != formatNumber(listPrice))
					{
						inputstr = "Discount_Err";
						if(lbl_Total != '0.00'){
							return inputstr;
						}
					}
				}else{
					fnCalExtPrice(priceEAObj,i);
				}
			}
		}
		// MNTTASK - 3603 SA - Ability to calculate discount for Quotes - Amount Validation
		}
	
	var inputstr='';
	// function to group order items by partnumber,item price and used lot details
	fnGroupOrderString();
	
	// function to form order string from grouped order items
	 inputstr=fnFormOrderString();
	
	return inputstr;
}

function fnCalculateTotal()
{
	var varRowCnt = document.frmCart.hRowCnt.value;
	var k;
	var obj;
	var unitPriceObj;
	var qty = 0;
	var price = 0.0;
	var unitPrice = 0.0;
	var total = 0.0;
	var totalBeforeAdj = 0.0;
	
	var objDiscountPercentage = fnDiscountPercentage();
	for (k=0; k <= varRowCnt; k ++)
	{
		obj = eval("document.frmCart.Txt_PNum"+k);
		if (obj)
		{
			if (obj.value != '')
			{
				obj = eval("document.frmCart.Txt_Qty"+k);
				qty = parseFloat(obj.value);
				obj = eval("document.frmCart.Txt_Price"+k);
				unitPriceObj = eval("document.frmCart.Txt_unit_price"+k);
				price = parseFloat(RemoveComma(obj.value));
				unitPrice = parseFloat(RemoveComma(unitPriceObj.value));
				total = total + (price * qty);
				totalBeforeAdj = totalBeforeAdj + (unitPrice * qty);
			}
		}
	}
		
	var strAccCurrency = (parent.document.all.hAccCurrency != undefined)?parent.document.all.hAccCurrency.value:'';
	
	document.frmCart.hTotal.value = total;
	document.frmCart.hTotalBeforeAdj.value = totalBeforeAdj;
	
	document.all.Lbl_Total.innerHTML = '<b>'+strAccCurrency+' '+formatNumber(total)+'</b>&nbsp;';
	document.all.Lbl_Total_Before_Adj.innerHTML = '<b>'+strAccCurrency+' '+formatNumber(totalBeforeAdj)+'</b>&nbsp;';
}

function fnUpdatePrice()
{
	//alert( document.frmCart.Cbo_Construct[document.frmCart.Cbo_Construct.selectedIndex].value);
	if (document.frmCart.Cbo_Construct.value == '0')
	{
		alert(message[10587]);
		return;
	}
	var varPartNums = '';;
	var blQty;
	var blPrice;
	var varAccId = parent.document.all.Txt_AccId.value;
	var varAccIdVal = varAccId.value;
	var comma = ',';
	var varRowCnt = document.frmCart.hRowCnt.value;
	var k;
	var varPartVal = "";

	for (k=0; k < varRowCnt; k ++)
	{
		objPartCell = eval("document.frmCart.Txt_PNum"+k);
		objQtyCell = eval("document.frmCart.Txt_Qty"+k);
		objPriceCell = eval("document.frmCart.Txt_Price"+k);
		
		if (objPartCell == "" || objPartCell == null )
		{
				comma = "";
		}
		else if (objPartCell.value == "")
		{
				comma = "";
		}
		else
		{
			comma = ",";
			varPartNums = varPartNums + objPartCell.value + comma;
			varPartVal = varPartVal + objPartCell.value + comma;
		}

		if (objQtyCell == "" || objQtyCell == null  || objQtyCell == "undefined" || objPriceCell == "" || objPriceCell == null  || objPriceCell == "undefined" )
		{
		}
		else if (objQtyCell.value =="" | objQtyCell.value ==null)
		{
		
		}
		else if (!objQtyCell.value.match(/^\d+$/))
		{
			blQty = "1";
			break;
		}
		else if(!RemoveComma(objPriceCell.value).match(/^[\d\.]+$/))
		{
			blPrice = "1";
			break;
		}
	}


	var varParsePartVal = varPartVal.substring(0,varPartVal.length -1);
	//parent.document.all.Txt_PartNum.value = varParsePartVal;
	
	if (varAccIdVal == "")
	{
		 Error_Details(message[66]);
	}
	else if (varPartNums == "" || varPartNums == null)
	{
		Error_Details(message[67]);
	}
	else if (blQty == "1")
	{
	 	Error_Details(message[68]);
	}
	else if (blPrice == "1")
	{
		 Error_Details(message[69]);
	}

	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	//fnSubmit();	
	var str = fnCreateOrderString();
	
	document.frmCart.hAction.value = 'UpdatePrice';
	document.frmCart.hInputStr.value = str;
	varPartNums = varPartNums.substr(0,varPartNums.length-1);
	document.frmCart.hPartNumStr.value = varPartNums;
	document.frmCart.hConsValue.value = document.frmCart.Cbo_Construct[document.frmCart.Cbo_Construct.selectedIndex].id;
	document.frmCart.submit();
}


function fnCalExtPrice(obj,cnt,onSubmitFl)
{
	var qty = 0;
	var price = 0.0;
	var DisEAPRice = 0.0;
	var netUnitPriceObj = 0.0;
	var pobj = eval("document.frmCart.Txt_Qty"+cnt);
	qty = parseFloat(pobj.value);
	var objDiscountPercentage = 0;

	if (!isNaN(qty)) 
	{
		price = parseFloat(RemoveComma(obj.value));
		var objDiscountPercentage = fnDiscountPercentage();
		
		if(objDiscountPercentage > 0 && price != 0){
			DisEAPRice = (parseFloat(price) - (parseFloat(price) * objDiscountPercentage));
			
			if(blDiscFlag && onSubmitFl=='Y'){
				DisEAPRice = price;
			}
		}else{
			netUnitPriceObj = eval("document.all.Txt_Price"+cnt);
			DisEAPRice = parseFloat(RemoveComma(netUnitPriceObj.value));
		}
		
		var eprice = DisEAPRice * qty;
		var pobj = eval("document.all.Lbl_Amount"+cnt);
		pobj.innerHTML = formatNumber(eprice)+"&nbsp;";
		obj.value = formatNumber(price);
		
		var obj = eval("document.all.Txt_Price"+cnt);
		obj.value = formatNumber(DisEAPRice);
		fnCalculateTotal();
		
		var partAmtObj = eval("document.all.hPartPrice"+cnt);
		var partAmtVal = parseFloat(partAmtObj.value);	
	}
	changeBgColor(obj,'#ffffff');
}

var vFilterShowFl = false;
function fnShowFilters(val)
{
	var obj = eval("document.all."+val);
	var obj1 = eval("document.all."+val+"img");
	if (obj.style.display == 'none')
	{
		obj.style.display = 'block';
		if(obj1){
			obj1.src = '/images/minus.gif';
		}
	}
	else
	{
		obj.style.display = 'none';	
		if(obj1){
			obj1.src = '/images/plus.gif';
		}
	}
}

function fnToggleCart()
{
	fnShowFilters('PartnPricing');
	fnShowFilters('PartnPricingHeader');
	fnShowFilters('PartnPricingDiv');
	fnShowFilters('PartnPricingFooter');
}

function fnSubmit()
{
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	var consigncnt = 0;
	// enabling Order Type dropdowns
	var pobj = '';	
	var cnt = document.frmPhOrder.hCnt.value;
	for (i=0;i<cnt;i++)
	{
		var obj = eval("document.frmPhOrder.Cbo_OrdPartType"+i);
		pobj = eval("document.frmPhOrder.Txt_PartNum"+i);

		if (obj)
		{
			obj.disabled = false;
			if (obj.value == '50300' && pobj.value != '')
			{
				consigncnt++;
			}
		}
		obj = eval("document.frmPhOrder.Chk_BO"+i);
		if (obj)
		{
			obj.disabled = false;
		}
	}

	document.frmPhOrder.Cbo_ShipCarr.disabled = false;
	document.frmPhOrder.Cbo_ShipMode.disabled = false;
	document.frmPhOrder.Cbo_ShipTo.disabled = false;
	document.frmPhOrder.Cbo_Rep.disabled = false;

	if (consigncnt == 0 && document.frmPhOrder.hAction.value == "PlaceOrder")
	{
		// setting order to 'Bill Only-Loaner' if no parts are from consignments
		document.frmPhOrder.Cbo_OrdType.value = 2530; 
		document.frmPhOrder.Cbo_ShipCarr.value = '5040';
		document.frmPhOrder.Cbo_ShipMode.value = '5031';
		document.frmPhOrder.Cbo_ShipTo.value = '4124';
		document.frmPhOrder.Cbo_Rep.value = '0';
	}

	document.frmPhOrder.submit();
}

function fnLoanerInfo(partnum,repid)
{
	windowOpener('/GmLoanerPartRepServlet?hAction=Load&hPartNum='+encodeURIComponent(partnum)+'&hRepId='+repid,"Pack","resizable=yes,scrollbars=yes,top=150,left=150,width=850,height=610");	
}


function fnChangeStatus(val,cnt)
{
	obj = eval("document.frmCart.Chk_BO"+cnt);
	if (obj)
	{
		if (val == 50301)
		{
			
			obj.checked = false;
			obj.disabled = true;
		}
		else
		{
			obj.disabled = false;
		}
	}
}

function fnCheckConstructAccount(conobj)
{
	/*
	var obj = document.frmPhOrder.Txt_AccId;
	var conlbl = obj[obj.selectedIndex].label;
	
	if (obj.value == 0)
	{
		conobj.selectedIndex = 0;
	}else
	{
		if (conlbl == 0)
		{
			 Error_Details('This Account does not have Capitated Pricing.');
		}
	}

	if (ErrorCount > 0)
	{
			Error_Show();
			conobj.selectedIndex = 0;
			Error_Clear();
			return false;
	}
	*/

}

function fnRemoveItem(val)
{
	var opt = (parent.document.all.hOpt != undefined)?parent.document.all.hOpt.value:'';
	obj = eval("document.frmCart.Txt_PNum"+val);
	window.parent.Reload(obj.value, '-1', obj.value );
	obj.value = '';
	obj = eval("document.frmCart.Txt_Qty"+val);
	obj.value = '';	
	obj = eval("document.frmCart.Txt_Price"+val);
	obj.value = '';
	obj = eval("document.frmCart.hPartPrice"+val);
	obj.value = '';
	obj = eval("document.frmCart.hNetPartPrice"+val);
	obj.value = '';
	obj = eval("document.frmCart.Txt_unit_price"+val);
	obj.value = '';
	obj = eval("document.frmCart.Txt_unit_adj"+val);
	obj.value = '';
	obj = eval("document.frmCart.Cbo_adj_code"+val);
	obj.value = '0';
	obj = eval("document.frmCart.hWaste"+val);
	obj.value = '';
	obj = eval("document.frmCart.hRevision"+val);
	obj.value = '';
    obj = eval("document.all.Lbl_Desc"+val);
    obj.innerHTML = "";
    obj = eval("document.all.Lbl_Stock"+val);
    obj.innerHTML = "";      
    obj = eval("document.frmCart.Txt_RefId"+val);
    if(obj!=undefined && obj!=null)
    	obj.value = '';
	obj = eval("document.frmCart.Chk_Cap"+val);
	if(obj!=undefined && obj!=null)
		obj.checked = false;
    
    obj = eval("document.all.Lbl_Amount"+val);
    obj.innerHTML = "";
	obj = eval("document.all.Txt_Cnum"+val);
	if(obj!=undefined && obj!=null)
		obj.value = '';
	obj = eval("document.all.Txt_BasePrice"+val);
	if(obj!=undefined && obj!=null)
		obj.value = '';
	obj = eval("document.all.hBasePrice"+val);
	if(obj!=undefined && obj!=null)
		obj.value = '';
	
    obj = eval("document.all.hCntrlNumberNeededFl"+val);
    if(obj!=undefined && obj!=null)
    	obj.value = '';
    obj = eval("document.all.Lbl_PriceEA"+val);
   	if(obj!=undefined && obj!=null)
		obj.value = '';
   	obj = eval("document.frmCart.hPnumProjValid"+val);
   	if(obj!=undefined && obj!=null)
    	obj.value = '';
   	
    if(opt == 'PHONE'){
    	fnRemoveImage(val);
    }
	fnCalculateTotal();
}

function fnClearCart()
{
	var opt = (parent.document.all.hOpt != undefined)?parent.document.all.hOpt.value:'';
	var varRowCnt = document.frmCart.hRowCnt.value;
	
	for (val=0; val < varRowCnt; val ++)
	{
		obj = eval("document.frmCart.Txt_Qty"+val);
		obj.value = '';
		obj = eval("document.frmCart.Txt_PNum"+val);
		obj.value = '';
		obj = eval("document.frmCart.Txt_Price"+val);
		obj.value = '';
		obj = eval("document.frmCart.hPartPrice"+val);
		obj.value = '';
		obj = eval("document.frmCart.hNetPartPrice"+val);
		obj.value = '0';
		obj = eval("document.frmCart.Txt_unit_price"+val);
		obj.value = '';
		obj = eval("document.frmCart.Txt_unit_adj"+val);
		obj.value = '';
		obj = eval("document.frmCart.Cbo_adj_code"+val);
		obj.value = '0';
		obj = eval("document.frmCart.hWaste"+val);
		obj.value = '';
		obj = eval("document.frmCart.hRevision"+val);
		obj.value = '';
		obj = eval("document.all.Lbl_Desc"+val);
		obj.innerHTML = "";
		obj = eval("document.all.Lbl_Stock"+val);
		obj.innerHTML = "";  
		obj = eval("document.all.Lbl_Amount"+val);
		obj.innerHTML = "";
		obj = eval("document.all.Txt_Cnum"+val);
		if(obj!=undefined && obj!=null)
			obj.value = '';
		obj = eval("document.all.Txt_BasePrice"+val);
		if(obj!=undefined && obj!=null)
			obj.value = '';
		obj = eval("document.all.hBasePrice"+val);
		if(obj!=undefined && obj!=null)
			obj.value = '';
		obj = eval("document.all.hCntrlNumberNeededFl"+val);
		if(obj!=undefined && obj!=null)
			obj.value = '';
		if(opt == 'PHONE'){
	    	fnRemoveImage(val);
	    }
	}
	fnCalculateTotal();
}

function fnResetLoanerFields(cnt)
{
		
	
	parent.document.all.shipCarrier.disabled = false;
	parent.document.all.shipMode.disabled = false;
	parent.document.all.shipTo.disabled = false;
	parent.document.all.names.disabled = false;
	parent.document.all.addressid.disabled = false;
	parent.document.all.shipCarrier.value = '5001';
	parent.document.all.shipMode.value = '5004';
	parent.document.all.shipTo.value = '4121';
	varRepId = parent.document.all.hRepId.value;
	parent.document.all.names.value = varRepId;
	
	

//	boobj = eval("document.frmCart.Chk_BO"+cnt);
	var rowcnt = document.frmCart.hRowCnt.value;
	for (i=0;i<rowcnt;i++)
	{
		var orderparttypeobj = eval("document.frmCart.Cbo_OrdPartType"+i);
		var boflagobj = eval("document.frmCart.Chk_BO"+i);
		
		if(i==cnt)
			{

		fnHidePart(orderparttypeobj,boflagobj); // To disable Part type and enable BO flag
		
			}
		
		else{

			fnShowPart(orderparttypeobj,boflagobj); // To disable Part type and enable BO flag
		}
	}
}

function fnHidePart(parttype,boflag) // To disable Part type and enable BO flag
{
	parttype.disabled = true;
	boflag.disabled = false;
	parttype.value = '50300';
}


function fnShowPart(parttype,boflag) // To disable Part type and enable BO flag
{
	parttype.disabled = false;
	boflag.disabled = false;
	parttype.value = '50300';	

}

function fnLoad(ordtype)
{
	
	if (ordtype == '2530')
    {
    	var cnt = document.frmCart.hRowCnt.value;
    	for (i=0;i<cnt;i++)
		{
			
    		var obj = eval("document.frmCart.Cbo_OrdPartType"+i);
			pobj = eval("document.frmCart.Txt_PNum"+i);
			qobj = eval("document.frmCart.Txt_Qty"+i);

			if (pobj && qobj)
			{
				if (pobj.value != '' && qobj.value != '')
				{
					lobj = eval("document.frmCart.hLoanCogs"+i);
					if (lobj.value == '')
					{
						Error_Details(message[10565]);
						parent.document.all.Cbo_OrdType.value = 2521;
						Error_Show();
						Error_Clear();
						
						document.frmPhOrder.Cbo_ShipCarr.disabled = false;
						document.frmPhOrder.Cbo_ShipMode.disabled = false;
						document.frmPhOrder.Cbo_ShipTo.disabled = false;
						document.frmPhOrder.Cbo_Rep.disabled = false;
						document.frmPhOrder.Cbo_ShipCarr.value = '5001';
						document.frmPhOrder.Cbo_ShipMode.value = '5004';
						document.frmPhOrder.Cbo_ShipTo.value = '4121';
						varRepId = document.frmPhOrder.hRepId.value;
						document.frmPhOrder.Cbo_Rep.value = varRepId;
						return false ;
					}
				}
			}

			if (obj)
			{
				obj.value = '50301';
				obj.disabled = true;
			}
			boobj = eval("document.frmCart.Chk_BO"+i);
			if (boobj)
			{
				boobj.checked = false;
				boobj.disabled = true;
			}
			
		}
    }

}

var refIdCnt = 0;
function fnChkValidFieldVal(obj,cnt)
{
	var pnumObj = eval("document.frmCart.Txt_PNum"+cnt);
	var typeObj = eval("document.frmCart.Cbo_OrdPartType"+cnt);
	//alert(' pnumObj '+ pnumObj.value  + '  typeObj.value  '+ typeObj.value + '  refid '+ obj.value);
	var refId = obj.value;
	refIdCnt = cnt;
	if (refId.length > 0)
	{	
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/GmCartAjaxServlet?refId='+refId+'&ordType='+typeObj.value+'&pnum='+pnumObj.value+'&ramdomId='+Math.random());
		dhtmlxAjax.get(ajaxUrl,fnGetValidData);
	}
}

function fnGetValidData(loader)
{
	var response = TRIM(loader.xmlDoc.responseText);
	if (response == null || response.length == 0)
	{
		var refIdObj = eval("document.frmCart.Txt_RefId"+refIdCnt);
		refIdObj.value = 'Not Valid';		
	}
}

function fnCreateAllParts()
{
	var varRowCnt = document.frmCart.hRowCnt.value; 
	var obj;
	var str = '';;
	var token = ','; 
	var inputstr = '';

	for (k=0; k < varRowCnt; k ++)
	{
		obj = eval("document.frmCart.Txt_PNum"+k);
		if (trimString(obj.value) != '')
		{
			str = trimString(obj.value)  + token; 
	 	 
			inputstr = inputstr + str;
		}
	}
	 
	return inputstr;
}


// Trims the leading and trailing spaces
function trim(str)
{
	str = new String(str);
	var intLen = str.length;
	var intBegin = 0;
	var intEnd = intLen - 1;
	while ((str.charCodeAt(intBegin) == 13 || str.charCodeAt(intBegin) == 10 || str.charAt(intBegin) == " ") && intBegin < intLen)
	{
		if (str.charCodeAt(intBegin) == 13 || str.charCodeAt(intBegin) == 10)
			intBegin+=2;
		else
        		intBegin++;
	}
	while ((str.charCodeAt(intEnd) == 13 || str.charCodeAt(intBegin) == 10  || str.charAt(intEnd) == " ") && intBegin < intEnd)
	{
		if (str.charCodeAt(intEnd) == 13 || str.charCodeAt(intBegin) == 10 )
			intEnd-=2;
		else
			intEnd--;
	}
	return str.substring(intBegin, intEnd+1);
}

//MNTTASK-8623 - I have added function for Add new col Conrol Number and getting data to DB.
function fnCreateCtrlNumString(){
	var inputstr='';
	
	// function to group lot string by part number, item price and used lot values
	fnGroupCtrlString();
	
	// function to form used lot   string from key and value pair array
	inputstr=fnFormCtrlString();
	
	return inputstr;
}
// When click place order, if it entered invalid control number , throw validation.
function fnCreateAllPartsWithCtrlNums()
{
	var varRowCnt = document.frmCart.hRowCnt.value; 
	var obj;
	var str = '';
	var token = ','; 
	var inputstr = '';
	var ObjCnum;
	for (k=0; k < varRowCnt; k ++)
	{
		obj = eval("document.frmCart.Txt_PNum"+k);
		ObjCnum = eval("document.frmCart.Txt_Cnum"+k);
		if(obj != undefined && ObjCnum != undefined){
			if (trimString(obj.value) != '') //&& ObjCnum.value != '' 
			{
				str = trimString(obj.value)  + token + trimString(ObjCnum.value.toUpperCase()) + '|'; 
				inputstr = inputstr + str;
			}
		}
	}
	//alert(' inputstr '+ inputstr);
	return inputstr;
}

//When click place order, if it entered invalid control number , throw validation, creating a string with qty
function fnCreateAllPartsWithQty()
{
	var varRowCnt = document.frmCart.hRowCnt.value; 
	var obj;
	var str = '';
	var token = ','; 
	var inputstr = '';
	var ObjCnum;
	for (k=0; k < varRowCnt; k ++)
	{
		obj = eval("document.frmCart.Txt_PNum"+k);
		ObjCnum = eval("document.frmCart.Txt_Cnum"+k);
		ObjQty = eval("document.frmCart.Txt_Qty"+k);
		if(obj != undefined && ObjCnum != undefined){
			if (trimString(obj.value) != '') //&& ObjCnum.value != '' 
			{
				str = trimString(obj.value)  + token + trimString(ObjCnum.value.toUpperCase()) +token+ trimString(ObjQty.value) + '|'; 
				inputstr = inputstr + str;
			}
		}
	}
	//alert(' inputstr '+ inputstr);
	return inputstr;
}
// When tab out in control number textbox, controlnumber needed flag is 'Y', then call rule engine fire in new order screen in bottom.
function fnValidateCtrlNum(trcnt,obj){	
	var pnumobj = eval("document.frmCart.Txt_PNum"+trcnt);
	var cnumobj = eval("document.frmCart.Txt_Cnum"+trcnt);
	var objCnumNeedFl = eval("document.frmCart.hCntrlNumberNeededFl"+trcnt);
	var CntrlNumNeededFl = objCnumNeedFl.value;
	
	//ATEC-161 : Removed the Control number needed flag condtion to validate Control number for both the Tissue and non-Tissue parts
	if( trimString(cnumobj.value) != ''){
		window.parent.fnValidateCtrlNumber(trimString(pnumobj.value),trimString(encodeURIComponent(cnumobj.value)),trcnt);
	}
	else{
		var pNumValidMsg = '';
		pNumValidMsg = eval("document.frmCart.hPnumValid"+trcnt).value;
		if(pNumValidMsg != '' && pNumValidMsg != null){
			var txtNonExistObj = document.getElementById("DivShowCnumNotExists"+trcnt);		
			txtNonExistObj.style.display='';
			txtNonExistObj.title = pNumValidMsg;
		}
	}
}

// If Control number needed flag is 'N', should not throw rule When enter control number in new order screen.
function fnCreateCtrlNumNeededStr(){
	var varRowCnt = document.frmCart.hRowCnt.value; 
	var obj;
	var inputstr = '';
	var ObjCnum;
	for (k=0; k < varRowCnt; k ++){
		obj = eval("document.frmCart.Txt_PNum"+k);
		ObjCnum = eval("document.frmCart.Txt_Cnum"+k);
		var objCnumNeedFl = eval("document.frmCart.hCntrlNumberNeededFl"+k);
		if(obj != undefined && ObjCnum != undefined && objCnumNeedFl != undefined){
			var CntrlNumNeededFl = objCnumNeedFl.value;
			if (trimString(obj.value) != '' && trimString(ObjCnum.value) !='' && CntrlNumNeededFl == 'N'){
					inputstr = inputstr + trimString(obj.value) + ',';  
			}
		}
	}
	return inputstr;
}

// When no entor control number for sterile parts, then create input string and throw confirm message.
function fnCreateWithoutCtrlNumStr(){
	var varRowCnt = document.frmCart.hRowCnt.value; 
	var obj;
	var str = '';;
	var inputstr = '';
	var ObjCnum;
	var objCnumNeedFl = '';
	// to get the order type
	var ordertype = parent.document.all.Cbo_OrdType.value;
	for (k=0; k < varRowCnt; k ++)
	{
		obj = eval("document.frmCart.Txt_PNum"+k);
		ObjCnum = eval("document.frmCart.Txt_Cnum"+k);
		objCnumNeedFl = eval("document.frmCart.hCntrlNumberNeededFl"+k);
		var CntrlNumNeededFl = (objCnumNeedFl!= undefined)?objCnumNeedFl.value:'N';
		
		// to skip the control number validation
		// 26240232 : Direct, 26240233 : Intercompany, 26240236 : Bill & Hold
		if (ordertype == 26240232 || ordertype == 26240233 || ordertype == 26240236){
			CntrlNumNeededFl = 'N';
		}
		
		if (trimString(obj.value) != ''){
			if(trimString((ObjCnum!= undefined)?ObjCnum.value:'') == '' && trimString((obj!= undefined)?obj.value:'') != '' && CntrlNumNeededFl == 'Y')
				inputstr = inputstr + trimString(obj.value) + ',';  
		}
	}
	return inputstr;
}

function fnCreateAllPartsWithCtrlNumsValid()
{
	var varRowCnt = document.frmCart.hRowCnt.value; 
	var obj;
	var str = '';
	var token = ','; 
	var inputstr = '';
	var ObjCnum;
	for (k=0; k < varRowCnt; k ++)
	{
		obj = eval("document.frmCart.Txt_PNum"+k);
		ObjCnum = eval("document.frmCart.Txt_Cnum"+k);
		var objCnumNeedFl = eval("document.frmCart.hCntrlNumberNeededFl"+k);
		var CntrlNumNeededFl = objCnumNeedFl.value;
		
		if (trimString(obj.value) != '' && CntrlNumNeededFl == 'Y') 
		{
			ObjCnumVal = trimString(ObjCnum.value.toUpperCase());
			str = trimString(obj.value)  + token + ObjCnumVal + '|'; 
			inputstr = inputstr + str;
		}
	}
	return inputstr;
}

// MNTTASK- 8623 When Enter invalid or valid control number, show image.  
function fnRemoveImage(index)
{
		var imgObj = document.getElementById("DivShowCnumAvl"+index);
		imgObj.style.display='none';

		var imgObj1 = document.getElementById("DivShowCnumNotExists"+index);			
		imgObj1.style.display='none';			
}

//MNTTASK-8623 - When create order enter part number with space, getting exception, Use trim function.
function trimString(obj) 
{ 
		if(obj != undefined && obj != '') 
		{ 
			return trim(obj); 
		} 
	return obj; 
} 
//adding methods for US and OUS code migration - this will used for apply discount 
function fnCalculateDiscount()
{
	var btnName = ''; 
	if (document.frmCart.Btn_Calculate)
		btnName = document.frmCart.Btn_Calculate.value; 
	// Based on the button name, call the function to calculate the discount/ reset the discount
	if(btnName == 'Reset Discount'){
		fnResetDisc();
	}else{
		fnCalculateDisc();
	}
}

function fnDiscountPercentage(){
	var objDiscountVal = 0;
	var objDiscountPercentage = 0.00;
	if(blDiscFlag){
		var  strDiscountIndex = parent.document.all.hDiscountIndex.value;
		objDiscountVal = eval("parent.document.frmPhOrder.Txt_Quote"+strDiscountIndex).value ;
		objDiscountVal =(objDiscountVal == '')?0:objDiscountVal;
		var AccDiscount = (parent.document.all.hAccDiscnt != undefined)?parent.document.all.hAccDiscnt.value:0;
		objDiscountVal = parseFloat(formatNumber(objDiscountVal)) + parseFloat(formatNumber(AccDiscount));
		objDiscountPercentage = trim(objDiscountVal)!=''?(parseFloat(objDiscountVal)/100):parseFloat(0);
	}
	return objDiscountPercentage;
}

// This function is to reset the Price EA column in New Order screen to the base price on blur of Discount
function fnResetBasePrice(){
	var basePrice;
	var cnt = document.frmCart.hRowCnt.value;
	for (i=0;i<cnt;i++)
	{
		pobj = eval("document.frmCart.Txt_PNum"+i);
		
		if(pobj.value != '')
		{
				
			obj = eval("document.all.hBasePrice"+i);
			basePrice = obj.value; 
			
		    obj = eval("document.frmCart.Txt_Price"+i);
		   	obj.value = basePrice;
		}
	}
}

/* New Function for Creating the Input String for Unit Parice Adjustment Details */
function fnCreateAdjString(){
		var cnt = document.frmCart.hRowCnt.value;
		var strOpt = parent.document.all.hOpt.value;		
		var k;
		var obj;
		var str;
		var token = ',';
		var cnum = '';
		var status = true;
		var inputstr = '';
		var err_totPrice = '';
		var validatePrice;
		var enterPrice;
		var partNum;
		var error_string = '';
		for (k=0; k < cnt; k ++){
			obj = eval("document.frmCart.Txt_PNum"+k);
			partNum = obj.value;
			if (trimString(obj.value) != ''){
				str = trimString(obj.value) + token;
				obj = eval("document.frmCart.Txt_unit_price"+k);
				str = str + RemoveComma(trimString(obj.value)) + token;				
				obj = eval("document.frmCart.Txt_unit_adj"+k);
				str = str + RemoveComma(trimString(obj.value)) + token;
				obj = eval("document.frmCart.Cbo_adj_code"+k);
				str = str + trimString(obj.value) + token;
				obj = eval("document.frmCart.Txt_Price"+k);
				str = str + RemoveComma(trimString(obj.value)) + '|';
				obj = eval("document.frmCart.Txt_Price"+k);
				validatePrice = RemoveComma(trimString(obj.value));
				enterPrice = trimString(obj.value);
	               var count_qty = validatePrice.length;
	               if(count_qty>12){	            	   
		               err_totPrice = err_totPrice +"<br>"+partNum+" - "+enterPrice+"";
	               }
				inputstr = inputstr + str;
			}
		}
		if (err_totPrice != ''){	         
	          error_string = error_string + 'The <b>Net Unit Price</b> is entered for the following part(s) should be less than 12 digits'+err_totPrice;
	          inputstr = inputstr + '@ '+error_string;
		}
		return inputstr;
}

function fnCalculateNetPrice(obj,cnt) {
	var discObj;
	var priceObj =	eval("document.all.Txt_unit_price" + cnt);
	var priceAdjObj = eval("document.all.Txt_unit_adj" + cnt);
	var priceTxtObj =	eval("document.all.Txt_Price" + cnt);
	var calcPrice = "";
	if(priceObj != undefined && priceAdjObj != undefined){
		calcPrice = parseFloat(RemoveComma(priceObj.value)) - parseFloat(RemoveComma(priceAdjObj.value));
	}
	if (obj.value== "107970")
	  {
		discObj = eval("document.all.hWaste"+cnt);
	  }
	else if (obj.value== "107971")
	  {
		discObj = eval("document.all.hRevision"+cnt);
	  }
	else if (obj.value== "0" && priceObj != undefined && priceObj.value != "" && priceObj.value != "0" && !isNaN(RemoveComma(priceObj.value)))
	 {	
		priceTxtObj.value = parseFloat(calcPrice);
		fnCalExtPrice(priceTxtObj,cnt);
	 }
	
	if (discObj != undefined && discObj.value != "" && !isNaN(RemoveComma(discObj.value)))
	  {		
		if (priceObj != undefined && priceAdjObj != undefined && !isNaN(RemoveComma(calcPrice)))
			{
				var price = parseFloat(calcPrice);
			    var discount = parseFloat(RemoveComma(discObj.value));
				var discountedPrice = price - (price * discount / 100);
				priceTxtObj.value = discountedPrice.toFixed(2);
				fnCalExtPrice(priceTxtObj,cnt);
			}
	  }
}

//When click place order, if it Entered COntrol Number is for More than 1 unit Quantity for the same part.
function fnCreateMoreQtyString(){
	var varRowCnt = document.frmCart.hRowCnt.value; 
	var obj;
	var str = '';
	var token = ','; 
	var inputstr = '';
	var ObjCnum;
	var objQty = 0;
	for (k=0; k < varRowCnt; k ++){
		obj = eval("document.frmCart.Txt_PNum"+k);
		ObjCnum = eval("document.frmCart.Txt_Cnum"+k);
		objQty = eval("document.frmCart.Txt_Qty"+k);
		var objCnumNeedFl = eval("document.frmCart.hCntrlNumberNeededFl"+k);
		var partCnum = '';
		if(obj != undefined && ObjCnum != undefined && objCnumNeedFl != undefined){
			var CntrlNumNeededFl = objCnumNeedFl.value;
			if (trimString(obj.value) != '' && trimString(ObjCnum.value) !='' && CntrlNumNeededFl == 'Y' && objQty.value > 1){
				partCnum = trimString(obj.value) + '@' + trimString(ObjCnum.value).toUpperCase();
					inputstr = inputstr + partCnum + ',';  
			}
		}
	}
	return inputstr;
}

function fnGetUnitPrice(obj,cnt){
	var partNUmber = eval("document.frmCart.Txt_PNum" + cnt);
	partNUmber = partNUmber.value + "@"+ cnt;
	var adjType = obj.value;
	var accid = parent.document.all.Txt_AccId.value;
	var priceObj =	eval("document.all.hUnitPartPrice" + cnt);
	if(adjType != 0){
		var ajaxUrl = fnAjaxURLAppendCmpInfo("/GmCartAjaxServlet?hAction=fetchAccPartListPrice&hAcctID="+accid+"&hAdjCOdeID="+adjType+"&hPartNumber="+encodeURIComponent(partNUmber)+"&ramdomId="+Math.random());
		dhtmlxAjax.get(ajaxUrl,function(loader){
			var responseAll   = loader.xmlDoc.responseXML;	
			if (responseAll != null) {
				fnSetUnitPrice(responseAll,obj,cnt);
			}
		});
		
	}else{
		eval("document.all.Txt_unit_price" + cnt).value = priceObj.value;
		fnCalculateNetPrice(obj,cnt);
	}
	
}

function fnSetUnitPrice(responseAll,obj,cnt){
	var response = responseAll.getElementsByTagName("data")[0];	
	var UnitPrice = parseXmlNode(responseAll.getElementsByTagName("unitPrice")[0].firstChild);	
	var strArray = UnitPrice.split('@');
	document.getElementById("Txt_unit_price"+strArray[1]).value = formatNumber(strArray[0]);
	fnCalculateNetPrice(obj,cnt);
}

//Function to open the Lot Code Report Screen by click on the Lot Number
function fnViewLotReport(cnt){
	var lotNum = eval("document.all.Txt_Cnum"+cnt).value;
	windowOpener(" /gmLotCodeRptAction.do?method=loadLotCodeProdInfo&hscreen=popup&lotNum="+lotNum,"LotCOdeReport","resizable=yes,scrollbars=yes,top=150,left=150,width=1020,height=600");
}
//PMT-17225 [to fetch all the parts details from a particular consignment/Tag ID]
function fnLoadParts()
{
	var accid = parent.document.all.Txt_AccId.value;
	var consinId = document.frmCart.Txn_consignmentId.value;
	if (accid == 0)
    {
		Error_Details(message[5288]);
    	Error_Show();
    	Error_Clear();
    	return false;
    	
    }
	 var responseAll="";
	 fnStartProgress('Y'); 
	 var url = fnAjaxURLAppendCmpInfo("/GmCartAjaxServlet?hAction=ConsignLoad&consinId="+consinId);
	     if (typeof XMLHttpRequest != "undefined"){
		     reqLoadParts = new XMLHttpRequest();
	     }else if(window.ActiveXObject){
		     reqLoadParts = new ActiveXObject("Microsoft.XMLHTTP");
	     }
	  reqLoadParts.open("GET", url, true);
	  reqLoadParts.onreadystatechange = callBackLoadParts;
	  reqLoadParts.send(null); 
}
function callBackLoadParts()
{   	
	var xmlstr ='';
	var pnum = '';
	var strresult = '';
	var partNum = '';
	var rowCount = document.frmCart.hRowCnt.value;
    if (reqLoadParts.readyState == 4){
	      if(reqLoadParts.status == 200){
		        var xmlDocm = reqLoadParts.responseXML;
		        pnum = xmlDocm.getElementsByTagName("pnum");
		        if(pnum.length==0)
		  	    {
		        	 fnStopProgress();
		  	         Error_Details(message[5289]);
		  			 Error_Show();
		  			 Error_Clear();
		  			 return false;
		  	    }
		  	    var count = 0;
		  	    //To check whether any parts entered manually.
			    for(var i=0;i<pnum.length;i++) {
				     var partNumer  = parseXmlNode(xmlDocm.getElementsByTagName("partnum")[i].firstChild);
				     var controlNum = parseXmlNode(xmlDocm.getElementsByTagName("cntrlnum")[i].firstChild);
				     var qty        = parseXmlNode(xmlDocm.getElementsByTagName("iqty")[i].firstChild);
				     var partType   = parseXmlNode(xmlDocm.getElementsByTagName("parttype")[i].firstChild);				    
				     if(partType == '4030')
				          {
					     //If same Sterile Part has Qty greater than 1, Adding one qty for each row.
					          for(var k=0;k<qty;k++){
					               for(var j=count;j<rowCount;j++)
					            	 {
							             partNum = eval("document.frmCart.Txt_PNum"+j).value;
							             if(partNum !=0)
							             {
									        count++; 
								         }else{
								        	 break;
								         }		    	 
							          }
					                  if(eval("document.frmCart.Txt_PNum"+count) == undefined)
					                      {
					                        fnAddRow('PartnPricing');
					                      }
					                   eval("document.frmCart.Txt_PNum"+count).value=partNumer;
					                   eval("document.frmCart.Txt_Qty"+count).value='1';
						               eval("document.frmCart.Txt_Cnum"+count).value=controlNum
						               fnSetPartSearch(count,eval("document.frmCart.Txt_PNum"+count)); 
						               count++;	 
						      } 
				          }else{     
				        	   for(var j=count;j<rowCount;j++)
				            	 {
						           partNum = eval("document.frmCart.Txt_PNum"+j).value;
						           if(partNum !=0)
						           {
								        count++; 
							        }else
							        {
							        	 break;
							        }		    	 
						         }
					                if(eval("document.frmCart.Txt_PNum"+count) == undefined)
					                {
					                     fnAddRow('PartnPricing');
					                 }
					                 eval("document.frmCart.Txt_PNum"+count).value=partNumer;
					                 eval("document.frmCart.Txt_Qty"+count).value=qty;
						             eval("document.frmCart.Txt_Cnum"+count).value=controlNum;				
						             fnSetPartSearch(count,eval("document.frmCart.Txt_PNum"+count));
						             count++;	   
				               }
				    
	               } 
			    fnStopProgress();
		   }      
      }   
   
}

//check for valid numeric strings
function fnIsNumeric(evt)  
{
	var iKeyCode = (evt.which) ? evt.which : evt.keyCode
    if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
        return false;
    return true;
}

//Function to froup order item by part number,item price and used lot
function fnGroupOrderString(){
	var varRowCnt = document.frmCart.hRowCnt.value;
	var k;
	var obj;
	var str;
	var cnum = '';
	var status = true;
	var inputAdjstr = '';
	var strAdj = '';
	var tempQtyVal = '';
	
	//empty key and value array before values assign
	keyMapArray = [];
	valMapArray = [];
	
	//input string formation is change to group order item by part number,item price and used lot
	for (k=0; k < varRowCnt; k ++){
		obj = eval("document.frmCart.Txt_PNum"+k);
		if (trimString(obj.value) != '')
		{
		// Order itmes are added in array as key and value
		// key format as partnmber ^ order part type ^ ref id ^ cap check ^ item price ^ Bo check ^ used lot
		// value as item quantity
			str = trimString(obj.value) + '^';
			strAdj = str;
			obj = eval("document.frmCart.Cbo_OrdPartType"+k);
			str = str + trimString(obj.value) + '^';
			obj = eval("document.frmCart.Txt_RefId"+k);
			if(obj!=undefined && obj!=null)
				str = str + trimString(obj.value) + '^';
			else
				str = str + '' + '^';
			obj = eval("document.frmCart.Chk_Cap"+k);
			if(obj!=undefined && obj!=null)
				str = str + trimString(fnReturnStatus(obj.checked)) + '^';
			else
				str = str + '' + '^';
			obj = eval("document.frmCart.Txt_Price"+k);
			str = str + RemoveComma(trimString(obj.value)) + '^';
			obj = eval("document.frmCart.Chk_BO"+k);
			str = str + trimString(fnReturnStatus(obj.checked)) + '^';
			obj = eval("document.frmCart.Txt_Cnum"+k);
			if(obj!=undefined && obj!=null){
				str = str + trimString(obj.value);
			}else{
				str = str + '' + '^';
			}
			
			
			obj = eval("document.frmCart.Txt_Qty"+k);
			// Passing key  and getting value 
			tempQtyVal = getMapValue(str);
			// if key value is null then its added to array 
			if(tempQtyVal==''){
				putMapValue(str,trimString(obj.value));
			}else{
			// if key values is present,then adding previous quantity with current quantity
				tempQtyVal = parseInt(tempQtyVal) + parseInt(trimString(obj.value));
				putMapValue(str,tempQtyVal);
			}
			
		}
	}
}

// function to form order string from key and value array
function fnFormOrderString(){
	var token = ',';
	var inputAdjstr = '';
	var inputstr ='';
	var cnum='';
	var tempArr = new Array();
	//Getting values from  array having key and values
	for(var i=0;i<keyMapArray.length;i++){
		tempArr = keyMapArray[i].split('^');
		inputstr = inputstr + tempArr[0]+ token; // getting part number from key 
		inputstr = inputstr + getMapValue(keyMapArray[i]) +token;// getting quantity from  value array
		inputstr = inputstr +cnum + token;// getting control num from key
		inputstr = inputstr + tempArr[1]+ token;//getting order part type from key
		inputstr = inputstr + tempArr[2]+ token;//getting ref id from key
		inputstr = inputstr + tempArr[3]+ token;//getting cap val from key
		inputstr = inputstr + tempArr[4]+ token;//getting item price from key
		inputstr = inputstr + tempArr[5]+ '|';// getting bo from key
		}
	
//	for (k=0; k < varRowCnt; k ++)
//	{
//		obj = eval("document.frmCart.Txt_PNum"+k);
//		if (trimString(obj.value) != '')
//		{
//			str = trimString(obj.value) + token;
//			strAdj = str;
//			obj = eval("document.frmCart.Txt_Qty"+k);
//			str = str + trimString(obj.value) + token;
//			str = str + cnum + token;
//			obj = eval("document.frmCart.Cbo_OrdPartType"+k);
//			str = str + trimString(obj.value) + token;
//			obj = eval("document.frmCart.Txt_RefId"+k);
//			if(obj!=undefined && obj!=null)
//				str = str + trimString(obj.value) + token;
//			else
//				str = str + '' + token;
//			obj = eval("document.frmCart.Chk_Cap"+k);
//			if(obj!=undefined && obj!=null)
//				str = str + trimString(fnReturnStatus(obj.checked)) + token;
//			else
//				str = str + '' + token;
//			obj = eval("document.frmCart.Txt_Price"+k);
//			str = str + RemoveComma(trimString(obj.value)) + token;
//			obj = eval("document.frmCart.Chk_BO"+k);
//			str = str + trimString(fnReturnStatus(obj.checked)) + '|';
//
//			inputstr = inputstr + str;
//			
//		}
//	}
	
	
	return inputstr;
}

//function to group control string by part number ,item price and used lot details
function fnGroupCtrlString(){
	var varRowCnt = document.frmCart.hRowCnt.value;
	var k;
	var obj;
	var str;
	var token = '^';
	var cnum = '';
	var inputstr = '';
	var objCnumNeedFl = '';
	var itemPrice = '';
	var v_attr_type = '101723';//The Attribute Type is Added for MDO-109. The control number attribute type need to be saved in Order item Attribute Table when Order raised from portal.
	var keyFormat ='';
	var tempQtyVal ='';
	var objItemPrice ='';

//empty key and value array before values assign
	keyMapArray = [];
	valMapArray = [];
//input string formation is change to group order item by part number,item price and used lot
	for (k=0; k < varRowCnt; k ++){
//	used lot values are added in array as key and value
// key format as part number,control number and price
// value as item quantity
		cnum = eval("document.frmCart.Txt_Cnum"+k);
		obj = eval("document.frmCart.Txt_PNum"+k);
		objCnumNeedFl = eval("document.frmCart.hCntrlNumberNeededFl"+k);

		if(obj != undefined && cnum != undefined && objCnumNeedFl != undefined){
		if (trimString(cnum.value) !='' && trimString(obj.value) != '' ){
			var Qtyobj = eval("document.frmCart.Txt_Qty"+k);
			
			objItemPrice = eval("document.frmCart.Txt_Price"+k);
			if(objItemPrice != undefined){
				itemPrice =RemoveComma(objItemPrice.value);
			}else{
				itemPrice = '';
			}
			
			keyFormat = trimString(obj.value) +'^'+trimString(cnum.value)+'^'+parseFloat(itemPrice);
			//getting value from array by key 
			tempQtyVal =  getMapValue(keyFormat);

			//if value is null , adding the key and value to array
			if(tempQtyVal==''){
				putMapValue(keyFormat,trimString(Qtyobj.value));
			}else{
			// if value is present, sum previous quantity and current quantity
				tempQtyVal = parseInt(tempQtyVal) + parseInt(trimString(Qtyobj.value));
				putMapValue(keyFormat,tempQtyVal);
			}
		}
		}		
	}
}

//function to form control string from key , value pair array
function fnFormCtrlString(){
	var tempArr = new Array();
	var inputstr='';
	for(var i=0;i<keyMapArray.length;i++){
		tempArr = keyMapArray[i].split('^');
		// geting value from key and value array in format as partnumber,qty,used lot
		inputstr = inputstr + '' +'^'+ tempArr[0]+ '^'+ getMapValue(keyMapArray[i]) +'^'+ tempArr[1] +'^'+''+'^'+'' +'^'+ ''+'|';
		}
	
//	for (k=0; k < varRowCnt; k ++){
//		
//		cnum = eval("document.frmCart.Txt_Cnum"+k);
//		obj = eval("document.frmCart.Txt_PNum"+k);
//		objCnumNeedFl = eval("document.frmCart.hCntrlNumberNeededFl"+k);
//		
//		//ATEC-161 : preparing input string for both the Tissue and non-Tissue parts which are having control number.
////		if(obj != undefined && cnum != undefined && objCnumNeedFl != undefined){
////			if (trimString(cnum.value) !='' && trimString(obj.value) != '' ){
////				var Qtyobj = eval("document.frmCart.Txt_Qty"+k);
////				//The Existing I/P String is Changed for MDO-109.Additional Parameter is added for  attribute Type.
////				inputstr = inputstr + '' +'^'+ trimString(obj.value)+ '^'+ trimString(Qtyobj.value) +'^'+ trimString(cnum.value) +'^'+''+'^'+'' +'^'+ ''+'|';
////			}
////		}
//	}
	return inputstr;
}