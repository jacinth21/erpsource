//New file is added since PhoneOrder.js is using  in Pricing screen also 

var partnums = "";
var vmode="";
var vcarr="";
var status = true; 
var ctrlId;
var accountType = '';
var CustPOValidFl = true;
var POMessage = false;
var ordertype ="";
var validOrdIdFl = false;
var dealerid = '';
var dealername = '';
var closingdate = '';
var egpsFl = '';   // used egpsFl for  PC-4659--Add EGPS Usage in DO - SpineIT New order

function Reload (partnum, flag, ordertype ) {
	partnum = trim(partnum); 
    
if(partnum!='')
{
	var valobj = partnums.split(","); 
	var arrlen = valobj.length;
	
	if (arrlen > 0)
	{
		for (var j=0;j< arrlen;j++ )
		{ 
				if ( valobj[j] == partnum)
				{
					status = false;
					break;
				} 
		}
	}
	else
	{
		 status = true;
	}
	
	 if(flag=='-1')
	 {	
		 partnums = partnums.replace(partnum,"");
	  }
	 else
	 {	 if(status==true )
		 {	 
			 partnums = partnums  + partnum + ",";
		 } 
	 }
	 
	if(ordertype != undefined && ordertype =='2521')	
	{
		ordertype = 'Bill_Ship';
	}
	else 
	{
		ordertype = '';
	}
	
	var f = document.getElementById('iframe1'); 
	  
	if (f != undefined)
	 {
		f.src  = document.frmPhOrder.RE_SRC.value + "&orderType=" + ordertype + "&PartNums=" + encodeURIComponent(partnums) + "&shipTo=" +document.all.shipTo.value + "&names=" +  document.all.names.value + "&addressid=" +  document.all.addressid.value +"&RefName=RFS" ;
	 } 
 	
 	var divfr = document.getElementById('divf');
 	
	if(divfr!= undefined && partnum!='')
	{ 
 		divfr.style.display="block"; 
	} 
	
 //	f.src = f.src;  
	}
	
}
 
function fnValidateCtrlNumber(pnum,cnum,trcnt){
	fnCallAjaxCtrlNum('validateControlNumber',pnum,cnum,trcnt);
}
var varPartNum;
var varCount;
var objPartCell;
var distid;
var accid;
var repid;

function fnOpenAddress()
{
	accid = document.frmPhOrder.Txt_AccId.value;
	repid = document.frmPhOrder.hRepId.value;
	//Cbo_BillTotb
	val = document.frmPhOrder.shipTo.value;
//	id = document.frmPhOrder.Cbo_Rep.value;
	id = document.frmPhOrder.names.value;
	
	if (accid == '0')
	{
		 Error_Details(message[61]);
	}
	
	if (val == '0')
	{
		 Error_Details(message[62]);
	}
	if (val == '4121' && id =='0')
	{
		 Error_Details(message[63]);
	}
	
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}

	windowOpener("/GmSearchServlet?hAction=getAddress&hAccId="+accid+"&hRepId="+repid+"&hShip="+val+"&hShipId="+id,"Address","resizable=yes,scrollbars=yes,top=340,left=420,width=300,height=135");
}

function fnCallShip()
{
	obj = document.frmPhOrder.shipTo;
//	var obj2 = document.frmPhOrder.Cbo_Rep;
	var obj2 = document.frmPhOrder.names;
	obj2.disabled = false;
	if (obj.options[obj.selectedIndex].text == 'Sales Rep')
	{
		var repid = document.frmPhOrder.Cbo_BillTo[document.frmPhOrder.Cbo_BillTo.selectedIndex].id;
		for (var j=0;j<obj2.length;j++)
		{
			if (obj2.options[j].value == repid)
			{
				obj2.options[j].selected = true;
				break;
			}
		}
		obj2.disabled = false;
		obj2.focus();
	}
	else
	{
		obj2.disabled = true;
	}
}


function fnPlaceOrder()
{
	var varPartNums;
	var varRowCnt = document.frmPhOrder.hRowCnt.value;
	var varAccId = document.frmPhOrder.Txt_AccId;
	var varAccIdVal = varAccId.value;
	var varTotalPrice =  document.frmPhOrder.hTotal.value;
	
	varRowCnt ++;
	var k;
	var varPartVal = "";
	for (k = 1; k < varRowCnt; k ++)
	{
		objPartCell = eval("document.frmPhOrder.Txt_PartNum"+k);

		if (objPartCell == "" || objPartCell == null)
		{
				comma = "";
		}
		else if (objPartCell.value == "")
			{
				comma = "";
			}
		else
		{
		varPartNums = varPartNums + objPartCell.value
		}
	}
	
	var varParsePartVal = varPartVal.substring(0,varPartVal.length -1);
	
	if (varAccIdVal == "")
	{
		 Error_Details(message[66]);
	}
	else if (varPartNums == "" || varPartNums == null)
	{
		 Error_Details(message[67]);
	}
	else if (varTotalPrice == "0.0")
	{
		 Error_Details(message[691]);
	}
	else
	{
		document.frmPhOrder.hAction.value = "PlaceOrder";
		document.frmPhOrder.hOpt.value = "PhoneOrder";
	}

	


	fnSubmit();
}

var vFilterShowFl = false;
function fnShowFilters(val)
{
	var obj = eval("document.all."+val);
	var obj1 = eval("document.all."+val+"img");
	if (obj.style.display == 'none')
	{
		obj.style.display = 'table';
		if(obj1){
			obj1.src = '/images/minus.gif';
		}
	}
	else
	{
		obj.style.display = 'none';	
		if(obj1){
			obj1.src = '/images/plus.gif';
		}
	}
}

function fnToggleCart()
{
	fnShowFilters('PartnPricing');
	fnShowFilters('PartnPricingHeader');
	fnShowFilters('PartnPricingDiv');
	fnShowFilters('PartnPricingFooter');
}

function fnSubmit()
{	
	fnStartProgress('Y');
	//validating the object
	var stropt = (validateObject(document.frmPhOrder.hOpt))?document.frmPhOrder.hOpt.value:'';
	var str = document.getElementById('fmCart').contentWindow.fnCreateOrderString();
	var adjString = document.getElementById('fmCart').contentWindow.fnCreateAdjString();
	var modeOfOrder = document.frmPhOrder.Cbo_Mode.value;
	var dodate = '';
	var confirmSubmit = true;
	var surgery_date ='';
	var surgery_date_obj ='';
	if(str == 'ApplyAcc_Err'){
 		Error_Details(message[10537]);
 	}
	
	if(validateObject(document.frmPhOrder.Txt_ReqDate)){
		CommonDateValidation(document.frmPhOrder.Txt_ReqDate,format,Error_Details_Trans(message[10538],format));
		var reqDT = document.frmPhOrder.Txt_ReqDate.value;
		if(dateDiff( currDate,reqDT, format) < 0){
			Error_Details(message[10539]);
		}
	}
	
 	if(str == 'Discount_Err')
 		Error_Details(message[10540]);
 	var strAllParts = document.getElementById('fmCart').contentWindow.fnCreateAllParts();

	//MNTTASK - 8623 - Create input string for sterile parts and save item t502a item order attribute 
 	var ctrlStr = document.getElementById('fmCart').contentWindow.fnCreateCtrlNumString();
 	if(document.frmPhOrder.hCtrlNumberStr != undefined){
 		document.frmPhOrder.hCtrlNumberStr.value = ctrlStr;
 	}
 	
 	// validate the customer PO - space
 	var text_PO = TRIM(document.frmPhOrder.Txt_PO.value);
	document.frmPhOrder.Txt_PO.value = text_PO;
	
	var custPoLen = text_PO.length;

	if(custPoLen > 50 && stropt!= 'QUOTE'){  //custPoLen increased to 50 for PC-846
		Error_Details(message[10541]);
	}else if(custPoLen > 100){ // For quote, should accept 100 characters
		Error_Details(message[10542]);
	}
	 
	if(stropt!= 'QUOTE'){// For Quote, no need to validate white space in Customer PO
		fnValidatePOSpace (text_PO);
	}
 	var orderID = "";
 	var parentOrdID ="";
 	if (document.frmPhOrder.Txt_OrdId){
 		orderID = document.frmPhOrder.Txt_OrdId.value;
 	}
 	if(document.frmPhOrder){
	 	if (document.frmPhOrder.Txt_ParentOrdId){
	 		parentOrdID = document.frmPhOrder.Txt_ParentOrdId.value;
	 	}
 	}
 	if (document.frmPhOrder.hQuoteCnt)
 	{
 	var strQuoteStr = fnCreateQuoteString(dodate);
 	document.frmPhOrder.hQuoteStr.value = strQuoteStr;
 	}
	var pNumErrorMsg='';
	document.frmPhOrder.hInputStr.value = str; 

	var adjStringArray = adjString.split("@");
	if(adjStringArray[1] != undefined || adjStringArray[1] != null){
		Error_Details(adjStringArray[1]);
	}	
	if (document.frmPhOrder.hAdjInputStr)
		document.frmPhOrder.hAdjInputStr.value = adjStringArray[0];	
	document.frmPhOrder.hTotal.value = document.getElementById('fmCart').contentWindow.document.all.hTotal.value;
	if (document.getElementById('fmCart').contentWindow.document.all.Cbo_Construct)
	{
		document.frmPhOrder.hConCode.value = document.getElementById('fmCart').contentWindow.document.all.Cbo_Construct.value;	
	}
	//fnValidateDropDn('Cbo_Rep',' Sales Rep - Billing Info '); Sales Rep Changes
	var obj = document.frmPhOrder.Cbo_BillTo;
	if (obj.value =='')
	{
		Error_Details(message[61]);
	}
	if((modeOfOrder == 0 || modeOfOrder == null ))
	{
		 Error_Details(message[10593]);	 
    } 
	
	var obj = document.frmPhOrder.hInputStr;
	if (obj.value =='')
	{
		Error_Details(message[10543]);
	}

	var doformat = fnCheckDOFormat();
	var opt = document.frmPhOrder.hOpt.value;
	if (opt == 'PHONE')
	{
		if (doformat)
		{
			Error_Details(message[10544]);
		}
		if(parentOrdID != '' && parentOrdID == orderID){
			Error_Details(message[10545]);
		}
	}
	if(orderID.indexOf(' ')>= 0)
	{
		Error_Details(message[10546]);	 
	}
	
	 surgery_date_obj = document.frmPhOrder.Txt_SurgDate;
	if (surgery_date_obj != undefined && surgery_date_obj != null ){
		surgery_date = surgery_date_obj.value;
	}
	if (surgery_date != '' ){

		CommonDateValidation(document.frmPhOrder.Txt_SurgDate, dateFmt,
				'<b>'+lblSurgeryDt+' - </b>' + message[611]);
		diff = dateDiff(todaysDate, surgery_date, dateFmt);
			if (diff > 0) {
				Error_Details(Error_Details_Trans(message[5280],lblSurgeryDt));
			}
		
	}
	//throw surgery date validation while raising Bill&ship,BOSC and Bill only loaner for japan	
	if (opt == 'PHONE' && surgeryDateInfo != ''){
		var fl = fnSurDtMandFl();
			if(fl == true && surgery_date == ''){
					Error_Details(message[5292]);
			}
	}

	var strArray = str.split('|');
	for (k=0;k<strArray.length-1 ;k++ )
	{
		if (strArray[k].indexOf('Not Valid') >= 0)
		{			
			var elementArray = strArray[k].split(',');
			pNumErrorMsg = pNumErrorMsg + elementArray[0]+',';
		}
	}
	if (pNumErrorMsg != '')
	{
		var pNumErrmsg = pNumErrorMsg.substring(0,(pNumErrorMsg.length-1));
		Error_Details(Error_Details_Trans(message[10547],pNumErrmsg));
	}
	
	/*if (document.all.Txt_LogReason.value == '' ){
		Error_Details(message[911]);
	}
	*/
	document.frmPhOrder.names.disabled = false;
	document.frmPhOrder.addressid.disabled = false;
	var shipToValue = document.frmPhOrder.shipTo.value;
	var strNames = document.frmPhOrder.names.value;
	if(shipToValue == 4120 || shipToValue == 4122 || shipToValue == 4123){
		fnValidateDropDn('names',message[5564]);
	}else if((shipToValue == 4121) || (shipToValue == 4000642)){		// 4000642 Shipping Account
		fnValidateDropDn('names',message[5564]);
		fnValidateDropDn('addressid',message[5624]);
	}
	if(shipToValue == 4120){
		document.frmPhOrder.names.disabled = true;
		document.frmPhOrder.addressid.disabled = true;
	}
	if (ErrorCount > 0)
	{
			fnStopProgress();
			Error_Show();
			Error_Clear();
			return false;
	}
	if(document.getElementById("comments") != undefined){
		var retval = checkMaxLength(document.getElementById("comments").value,4000, "Comments");
		if(retval == false){
			fnStopProgress();
			return false;
		}
	}
	var consigncnt = 0;
	// enabling Order Type dropdowns
	var pobj = '';	
	//var cnt = document.frmPhOrder.hCnt.value;
	var cnt = document.getElementById('fmCart').contentWindow.document.frmCart.hRowCnt.value;

	for (i=0;i<cnt;i++)
	{
		var obj = eval("document.getElementById('fmCart').contentWindow.document.frmCart.Cbo_OrdPartType"+i);
		pobj = eval("document.getElementById('fmCart').contentWindow.document.frmCart.Txt_PNum"+i);

		if (obj)
		{
			obj.disabled = false;
			if (obj.value == '50300' && pobj.value != '')
			{
				consigncnt++;
			}
		}
		obj = eval("document.getElementById('fmCart').contentWindow.document.frmCart.Chk_BO"+i);
		if (obj)
		{
			obj.disabled = false;
		}
	}

	document.frmPhOrder.shipCarrier.disabled = false;
	document.frmPhOrder.shipMode.disabled = false;
	document.frmPhOrder.shipTo.disabled = false;
//	document.frmPhOrder.Cbo_Rep.disabled = false;
	document.frmPhOrder.names.disabled = false;

	var varRepId = document.frmPhOrder.hRepId.value;
	if (consigncnt == 0 && document.frmPhOrder.hAction.value == "PlaceOrder")
	{
		// setting order to 'Bill Only-Loaner' if no parts are from consignments
		document.frmPhOrder.Cbo_OrdType.value = 2530; 
		if(vcountryCode == 'en'){// N/A is using for US
			document.frmPhOrder.shipCarrier.value = '5040';
			document.frmPhOrder.shipMode.value = '5031';
			document.frmPhOrder.shipTo.value = '4124';
		//	document.frmPhOrder.Cbo_Rep.value = '0';
			document.frmPhOrder.names.value = '0';
		}else{//For OUS countries values are getting from DB
			document.frmPhOrder.shipCarrier.value = vshipCarrier;
			document.frmPhOrder.shipMode.value = vshipMode;
			document.frmPhOrder.shipTo.value = vshipTo;
			document.frmPhOrder.names.value = varRepId;
		}
			
	}
	// Setting the Selected Columns to the form Variables.
	var tagColumn = document.getElementById('fmCart').contentWindow.document.getElementById('cols16').checked;
	var capFlColumn = document.getElementById('fmCart').contentWindow.document.getElementById('cols17').checked;
	
	if(document.frmPhOrder.hTagColumn)
	document.frmPhOrder.hTagColumn.value = tagColumn;
	if(document.frmPhOrder.hTagColumn)
	document.frmPhOrder.hCapFlColumn.value = capFlColumn;	
	
	// Get the order type name which is selected in the dropdown
	var ordTypeNm = document.frmPhOrder.Cbo_OrdType.options[document.frmPhOrder.Cbo_OrdType.selectedIndex].text;
	//If all the validations are done, then confirm the order type selected
	if(opt == 'PHONE' && viaCellPlantFl == 'Y'){ 
		confirmSubmit = confirm(Error_Details_Trans(message[10594],ordTypeNm)); 
	}
	if(!confirmSubmit){
		fnStopProgress();
		return false;
	}
	
	if(document.frmPhOrder.Btn_PlaceOrd)
		document.frmPhOrder.Btn_PlaceOrd.disabled = true;

	  // When click place order function will call and show progress image.
	  //fnClearDivProgress();
	  
	//alert(consigncnt);
	/*var orderType = document.frmPhOrder.Cbo_OrdType.value;
	if(!orderType == '2521') // 2521 BIll & SHIP
	{
		 
		document.frmPhOrder.action = "/gmRuleEngine.do?RefName=RFS&txnStatus=VERIFY"+"&PartNums="+strAllParts; 
	}
	else
	{	 
		document.frmPhOrder.action = "/gmRuleEngine.do?RefName=RFS&txnStatus=PROCESS"+"&PartNums="+strAllParts; 
	}*/ 
	  if(vcountryCode == 'au'){// Changes are done only for au
		  
		  
		  if(strQuoteStr != false){
			  document.frmPhOrder.submit();
		  }else{
			  fnStopProgress();
			  document.frmPhOrder.Btn_PlaceOrd.disabled = false;
		  }
	  }else{
		   document.frmPhOrder.submit();
	  }
}
function fnPrintPack()
{
	var val = document.frmPhOrder.Txt_OrdId.value;
	if (val != '')
	{
		windowOpener('/GmOrderItemServlet?hAction=PICSlip&hId='+val,"Pack","resizable=yes,scrollbars=yes,top=150,left=150,width=760,height=610");	
	}
}

function fnLoanerInfo(partnum,repid)
{
	windowOpener('/GmLoanerPartRepServlet?hAction=Load&hPartNum='+encodeURIComponent(partnum)+'&hRepId='+repid,"Pack","resizable=yes,scrollbars=yes,top=150,left=150,width=850,height=610");	
}
 
function fnSetShipSel(val)
{

	var stropt = (validateObject(document.frmPhOrder.hOpt))?document.frmPhOrder.hOpt.value:'';	
	var val = document.frmPhOrder.Cbo_OrdType.value;
	var cnt = document.getElementById('fmCart').contentWindow.document.frmCart.hRowCnt.value;
	//setting default value for OUS country
	var sCarrier = vshipCarrier;
	var sMode = vshipMode;
	var sShipTo = vshipTo; //Getting value from DB
	var varAccId = '';
	var sNames = '';
  	var parts =    document.getElementById('fmCart').contentWindow.fnCreateAllParts();
  	
	if(strShowDOID !='NO'){
    	document.frmPhOrder.Txt_OrdId.disabled = false;
  	}
 	 
    Reload(parts, '1',val); 
	
	var boobj = '';
	var pobj = '';
	var qobj = '';
	var lobj = '';
	//cnt++;
	
	if (val == 2530) //2530-Bill Only - Loaner
	{
		//setting default value for US
		if(vcountryCode == 'en'){
			sCarrier = '5040';   //5040 - N/A
			sMode = '5031';      //5031 - N/A
			sShipTo = '4124';    //4124 - N/A
		}
		document.frmPhOrder.shipCarrier.value = sCarrier;
		document.frmPhOrder.shipMode.value = sMode;
		document.frmPhOrder.shipTo.value = sShipTo;  
	//	document.frmPhOrder.Cbo_Rep.value = '0';
		if(vcountryCode == 'en'){ // No need to disable for OUS
			document.frmPhOrder.names.value = '0';
			document.frmPhOrder.shipCarrier.disabled = true;
			document.frmPhOrder.shipMode.disabled = true;
			document.frmPhOrder.shipTo.disabled = true;
		//	document.frmPhOrder.Cbo_Rep.disabled = true;
			document.frmPhOrder.names.disabled = true;
			document.frmPhOrder.addressid.disabled = true;
		}else{
			varAccId = document.frmPhOrder.Txt_AccId.value;
			document.frmPhOrder.names.value = varAccId;
		}
		for (i=0;i<cnt;i++)
		{
			
			
			var obj = eval("document.getElementById('fmCart').contentWindow.document.frmCart.Cbo_OrdPartType"+i);
			pobj = eval("document.getElementById('fmCart').contentWindow.document.frmCart.Txt_PNum"+i);
			qobj = eval("document.getElementById('fmCart').contentWindow.document.frmCart.Txt_Qty"+i);
			controlObj = eval("document.getElementById('fmCart').contentWindow.document.frmCart.Txt_Cnum"+i);
			if(controlObj != undefined){
				controlObj.disabled = false;
			}
			
			if (pobj && qobj)
			{
				if (pobj.value != '' && qobj.value != '')
				{
					lobj = eval("document.getElementById('fmCart').contentWindow.document.frmCart.hLoanCogs"+i);
					if (lobj.value == '')
					{
						
					
						Error_Details(message[10548]);
						document.frmPhOrder.Cbo_OrdType.value = 2521;
						Error_Show();
						Error_Clear();
				
						fnHideFields(cnt); // For hide Bill & Ship details
						
					
						document.frmPhOrder.shipCarrier.disabled = false;
						document.frmPhOrder.shipMode.disabled = false;
						document.frmPhOrder.shipTo.disabled = false;
					//	document.frmPhOrder.Cbo_Rep.disabled = false;
						document.frmPhOrder.names.disabled = false;
						document.frmPhOrder.addressid.disabled = false;
						//setting default value for US
						//Commented becuse the value is getting from DB, no need to hardcode
						/*if(vcountryCode == 'en'){
							sCarrier = '5001';  //5001 - FedEx
							sMode = '5004';     //5004 - Priority Overnight
						}*/
						document.frmPhOrder.shipCarrier.value = sCarrier;
						document.frmPhOrder.shipMode.value = sMode;
						document.frmPhOrder.shipTo.value = sShipTo;
						varRepId = document.frmPhOrder.hRepId.value;
					//	document.frmPhOrder.Cbo_Rep.value = varRepId;
						document.frmPhOrder.names.value = varRepId;
						return false ;
					}
				}
			}

			if (obj)
			{
				obj.value = '50301';
				obj.disabled = true;
			}
			boobj = eval("document.getElementById('fmCart').contentWindow.document.frmCart.Chk_BO"+i);
			if (boobj)
			{
				boobj.checked = false;
				boobj.disabled = true;
			}
			
		}
	
	}
	else
	{
		document.frmPhOrder.shipCarrier.disabled = false;
		document.frmPhOrder.shipMode.disabled = false;
		document.frmPhOrder.shipTo.disabled = false;
	//	document.frmPhOrder.Cbo_Rep.disabled = false;
		document.frmPhOrder.names.disabled = false;
		document.frmPhOrder.addressid.disabled = false;
		
		varRepId = document.frmPhOrder.hRepId.value;
		varAccId = document.frmPhOrder.Txt_AccId.value;
		sNames = varAccId;
		//setting default value for US
		if(vcountryCode == 'en'){//Value should come from DB
			sCarrier = vshipCarrier;  
			sMode = vshipMode;     
			sShipTo = vshipTo;    
			sNames = varRepId;
		}		
		
		document.frmPhOrder.shipCarrier.value = sCarrier;
		document.frmPhOrder.shipMode.value = sMode;
		document.frmPhOrder.shipTo.value = sShipTo;		
	//	document.frmPhOrder.Cbo_Rep.value = varRepId;
		document.frmPhOrder.names.value = sNames;		
		for (i=0;i<cnt;i++)
		{
		
			obj = eval("document.getElementById('fmCart').contentWindow.document.frmCart.Cbo_OrdPartType"+i);
			boobj = eval("document.getElementById('fmCart').contentWindow.document.frmCart.Chk_BO"+i);
			pobj = eval("document.getElementById('fmCart').contentWindow.document.frmCart.Txt_PNum"+i);
			controlObj = eval("document.getElementById('fmCart').contentWindow.document.frmCart.Txt_Cnum"+i);
			lobj = eval("document.getElementById('fmCart').contentWindow.document.frmCart.hLoanCogs"+i);
			obj.disabled = false;
			if(controlObj != undefined){
				controlObj.disabled = false;
			}
			
			if (pobj.value == '')
			{
				if (boobj)
				{
					boobj.disabled = false;
				}
				obj.value = '50300';
			}
			else
			{
				if (obj.value == '50301')
				{
					if (boobj)
					{
						boobj.disabled = false;
					}
				}
				else
				{
					obj.value = '50300';
					if (boobj)
					{
						boobj.disabled = false;
					}
				}
			}
			
			if(stropt == 'PROFORMA' && val == '101260')
				{
				boobj.disabled = true;
				
				}
			
			
			if (val == 2532) //2530-Bill Only - sales Consignment
			{
				if (obj)
				{
					obj.value = '50300';
					obj.disabled = true;
				}
			}
			
			if(val == 2521) // 2521 - Bill & Ship
				{
				
				if(pobj.value != '' && lobj.value == '') // To Hide Part Type when hLoanCogs is empty and Part number column is filled with value
					{
					
					obj.value = '50300';
					obj.disabled = true;
					}	
				
				
				
				}
			
			// To Enable Part Type with Consignment[50300] when Part Number is filled with value and Part type is selected as Loaner[50301]
			
			if(val == 2521 || val == 2527 || val == 2524 || val == 2522 || val == 101260) // 2521 - Bill & Ship, 2527 - Replacement, 2524 - Price Adjustment, 2522 - Duplicate Order, 101260 - Acknowledgement Order
				{
				
				if(pobj.value != '' && obj.value == '50301')
				{
				
				obj.value = '50300';
				obj.disabled = false;
				
				}
				
				}
			
			
			
			
			

		}
		// 26240232 Direct
		// 26240233 Intercompany
		// 26240236 Bill & Hold
		if (val == 26240232 || val == 26240233 || val == 26240236) 
		{
			fnCartPut ('Cbo_OrdPartType', true);
			fnCartPut ('Txt_Cnum', true);
			fnCartEnableDisableFields ();
		}
	}
}

// Hide fields for Bill & Ship

function fnHideFields(cnt)
{

	for (j=0;j<cnt;j++)
	{
		
		var partnumberobj = eval("document.getElementById('fmCart').contentWindow.document.frmCart.Txt_PNum"+j);
		var quantityobj = eval("document.getElementById('fmCart').contentWindow.document.frmCart.Txt_Qty"+j);
		var loancogsobj = eval("document.getElementById('fmCart').contentWindow.document.frmCart.hLoanCogs"+j);
		var orderparttypeobj = eval("document.getElementById('fmCart').contentWindow.document.frmCart.Cbo_OrdPartType"+j);
		var boflagobj = eval("document.getElementById('fmCart').contentWindow.document.frmCart.Chk_BO"+j);
		var controlnumberObj = eval("document.getElementById('fmCart').contentWindow.document.frmCart.Txt_Cnum"+j);
		if(controlnumberObj != undefined){
			controlnumberObj.disabled = false;
		}
		
		if (partnumberobj.value != '' && quantityobj.value != '')
		{
		
		if(loancogsobj.value == '')
			{
			
			fnHidePart(orderparttypeobj,boflagobj); // To disable Part type and enable BO flag
			
			}
		
		else{
			fnShowPart(orderparttypeobj,boflagobj); // To disable Part type and enable BO flag
			}
		
		}
		
		else
			{
			fnShowPart(orderparttypeobj,boflagobj); //To enable Part type and BO flag
			}
		
	}


}


// To disable Part type and enable BO flag

function fnHidePart(parttype,boflag)
{
	parttype.disabled = true;
	boflag.disabled = false;
	parttype.value = '50300';	

}

//To enable Part type and BO flag

function fnShowPart(parttype,boflag)
{
	parttype.disabled = false;
	boflag.disabled = false;
	parttype.value = '50300';	

}

//show surgery date mandatory symbol while raising Bill&ship,BOSC and Bill only loaner for japan
function fnShowSurgDtMandSymb(){
	var fl = fnSurDtMandFl();
			if(fl == true){
				 document.getElementById("divMandSurgDt").style.display = 'inline';
			}else{
				 document.getElementById("divMandSurgDt").style.display = 'none';
			}
}

function fnSurDtMandFl()
{
	var opt = document.frmPhOrder.hOpt.value;
	var ordType = document.frmPhOrder.Cbo_OrdType.value;
	var fl ='';
	if (opt == 'PHONE' && surgeryDateInfo != ''){
		var strArraySurDtInfo = surgeryDateInfo.split(',');
		for (var h=0;h<strArraySurDtInfo.length;h++)
		{
			if (strArraySurDtInfo[h].indexOf(ordType) != -1){
				fl = true;
				break;
			}else{
				fl = false;
			}
		}
		return fl;
	}
	
}

function fnChangeStatus(val,cnt)
{
	obj = eval("document.getElementById('fmCart').contentWindow.document.frmCart.Chk_BO"+cnt);
	if (obj)
	{
		if (val == 50301)
		{
			obj.checked = false;
			obj.disabled = true;
		}
		else
		{
			obj.disabled = false;
		}
	}
}

function fnCheckConstructAccount(conobj)
{
	/*
	var obj = document.frmPhOrder.Cbo_BillTo;
	var conlbl = obj[obj.selectedIndex].label;
	
	if (obj.value == 0)
	{
		conobj.selectedIndex = 0;
	}else
	{
		if (conlbl == 0)
		{
			 Error_Details('This Account does not have Capitated Pricing.');
		}
	}

	if (ErrorCount > 0)
	{
			Error_Show();
			conobj.selectedIndex = 0;
			Error_Clear();
			return false;
	}
	*/

}


/************************************************************************************************************************/
function fnCallAjax(val)
{
	if(val =='GETACC'){
		accountType = '';
	}	
	var allParts = '';
	var allPartwithQty = '';
	var accid = document.all.Txt_AccId.value;
	if(document.all.Txt_OrdId){
		var doid = document.all.Txt_OrdId.value;
	}
	// When submit in place order it will get the all parts from the below function and pass in ajax.
	if(window.frames.fmCart){
		allParts =  document.getElementById("fmCart").contentWindow.fnCreateAllPartsWithCtrlNums();
		// String with partnumber,cnum,qty
		allPartwithQty =  document.getElementById("fmCart").contentWindow.fnCreateAllPartsWithQty();
	}

	// The PhoneOrder.js file calling in many places. Need to be check undefined.
	var orderType = (document.frmPhOrder) ? document.frmPhOrder.Cbo_OrdType.value:'';
	var strShipTo = (document.all.shipTo) ? document.all.shipTo.value:'';
	var strName = (document.all.names) ? document.all.names.value:'';
	var strAddress = (document.all.addressid) ? document.all.addressid.value:'';
	
	 
	if (document.frmPhOrder)
	{
		var repid = document.all.hRepId.value;
	}
    var url = fnAjaxURLAppendCmpInfo("/GmOrderAjaxServlet?orderType="+encodeURIComponent(orderType)+"&opt=" + encodeURIComponent(val)+ "&acc=" + encodeURIComponent(accid)+ "&rep="+encodeURIComponent(repid)+"&doid="+trim(doid)+ "&PartNums="+encodeURIComponent(allParts)+"&PartNumwithQty="+encodeURIComponent(allPartwithQty)+"&shipTo=" +strShipTo + "&names=" +  strName + "&addressid=" + strAddress+"&submitFl=Y");
	if (typeof XMLHttpRequest != "undefined") 
	{
	   req = new XMLHttpRequest();
	} 
	else if (window.ActiveXObject) 
	{
	   req = new ActiveXObject("Microsoft.XMLHTTP");
	}
	req.open("GET", url, true);
	if (val=='CHECKDOID'){             //MNTTASK-7099 -- For displaying the avail/exists images.
		req.onreadystatechange = showimage;
	}else if(val == 'validateControlNumber'){
		req.onreadystatechange = callCtrlNum;
	}else{
		req.onreadystatechange = callback;
	}
	req.send(null);
}
 
function callback() {
    if (req.readyState == 4) {
        if (req.status == 200) {
	        var xmlDoc = req.responseXML;
			parseMessage(xmlDoc);
        }
        else
        {
	        setErrMessage('<span class=RightTextBlue>'+message[10595]+'</span>');
        }
    }
}

function callCtrlNum() {
    if (req.readyState == 4) {
        if (req.status == 200) {
	        var xmlDoc = req.responseText;
	       if(trim(xmlDoc) != '' ){
	    		Error_Details(xmlDoc);
	    		Error_Show();	
	    		Error_Clear();
	    		return false;
	       }else{
	    	   fnCheckHoldFl ();
	    	   fnSubmit();
	       }
         }
    }
}

function parseMessage(xmlDoc) 
{
	
	var pnum = xmlDoc.getElementsByTagName("acc");
	var cons = xmlDoc.getElementsByTagName("const");
	for (var x=0; x<pnum.length; x++) 
	{
		var rgname = parseXmlNode(pnum[x].childNodes[0].firstChild);
		var adname = parseXmlNode(pnum[x].childNodes[1].firstChild);
		var dname = parseXmlNode(pnum[x].childNodes[2].firstChild);
		var trname = parseXmlNode(pnum[x].childNodes[3].firstChild);
		var rpname = parseXmlNode(pnum[x].childNodes[4].firstChild);
		repid = parseXmlNode(pnum[x].childNodes[5].firstChild);
		var prname = parseXmlNode(pnum[x].childNodes[6].firstChild);
		var prid = parseXmlNode(pnum[x].childNodes[7].firstChild);
		var gpocatid = parseXmlNode(pnum[x].childNodes[8].firstChild);
		var acname = parseXmlNode(pnum[x].childNodes[9].firstChild);
		accid = parseXmlNode(pnum[x].childNodes[10].firstChild);
		distid = parseXmlNode(pnum[x].childNodes[11].firstChild);
		vmode =  parseXmlNode(pnum[x].childNodes[12].firstChild);		
		var accCurrency =  '';
		if(pnum[x].childNodes[17]!=undefined && pnum[x].childNodes[17]!=null)
		{
			accCurrency = parseXmlNode(pnum[x].childNodes[17].firstChild); // Account Currency symbol will displayed in the screen.
		}
		rpname = rpname + ' ('+repid+')';
		//Account Affiliation
		var gpraff = parseXmlNode(pnum[x].childNodes[13].firstChild);
		var hlcaff =  parseXmlNode(pnum[x].childNodes[14].firstChild);
		// get the account type
		accountType = parseXmlNode(pnum[x].childNodes[15].firstChild);
		var accComments = '';
		if(pnum[x].childNodes[16]!=undefined && pnum[x].childNodes[16]!=null)
		{
		   accComments =  parseXmlNode(pnum[x].childNodes[16].firstChild);
		}
		vcarr =  parseXmlNode(pnum[x].childNodes[19].firstChild);
		var cigVal = parseXmlNode(pnum[x].childNodes[20].firstChild);
		
		//NET NUMBER CHANGE
		// Fetching dealer id from GmCustomerBean
		dealerid = parseXmlNode(pnum[x].childNodes[21].firstChild);
		dealername = parseXmlNode(pnum[x].childNodes[22].firstChild);
		closingdate = parseXmlNode(pnum[x].childNodes[23].firstChild);
		
		egpsFl = parseXmlNode(pnum[x].childNodes[26].firstChild);  //  used egpsFl for  PC-4659--Add EGPS Usage in DO - SpineIT New order
	}
	
	if (accid == '')
	{
		Error_Details(message[10549]);
		Error_Show();	
		Error_Clear();
		document.all.Txt_AccId.value = '';
		return false;

	}else
	{
		if (document.frmPhOrder)
		{
			//document.all.Cbo_BillTotb.value = acname;
			document.all.Cbo_BillTo.value = accid;
			if(document.all.searchCbo_BillTo != undefined)
				document.all.searchCbo_BillTo.value = acname;
			document.all.hRepId.value = repid;
			/*
			objRep = eval(document.all.Cbo_Rep); Sales Rep Changes
			document.all.Cbo_Rep.value = 0;
			document.all.names.value = 0;
			
			for(k=0; k<objRep.length;k++)
			{
				if(objRep[k].value == repid)
				{
					document.all.Cbo_Rep.value = repid;
					document.all.names.value = repid;
					break;
				}
			}
			*/
			//document.all.names.value = repid;
			//fnGetNames(document.all.shipTo);
			//document.all.names.fireEvent('onChange');
		}

		if (document.frmAccount)
		{
			document.all.Cbo_AccId.value = accid;
		}
		
		//accCurrency is added to Pass Currency symbol to the function setMessage
		setMessage(rgname,adname,dname,trname,rpname,prname,prid,gpocatid,acname,gpraff,hlcaff,accComments,accCurrency,dealerid,dealername,closingdate);
		// To set the CIG filed values.
        
        if (cigVal != ''){
        	
               if(document.getElementById("400154") != undefined){
                      document.getElementById("400154").value = cigVal;
               }
        }

		if (document.frmPhOrder)
		{
			if (cons.length > 0)
			{
				fnSetConstructs(cons);
			}

		}
		
		if(vcarr != '0' || vcarr != ''){
				document.frmPhOrder.shipCarrier.value = vcarr;
				fnGetShipModes(document.all.shipCarrier,'');
		}
				
		if (vmode != '0') {
			document.frmPhOrder.shipMode.value = vmode;
		} 
		else{
			if(document.all.shipMode)
				document.frmPhOrder.shipMode.value = vshipMode;//Should come from DB, should not hardcode the value
		}
		fnSetCarr(vcarr);
		fnGetNames(document.all.shipTo);
		
		/*
		if(accid != 0){
					fnCalcShipCharge();// To calculate the shipping charge
				}*/
				
		if(accountType == '70114'){ // ICS Account
	     	Error_Details(message[10550]);	     	
			Error_Show();	
			Error_Clear();
			return false;
	     }
	}
	fnResetCarr();
}
 
function setMessage(rgname,adname,dname,trname,rpname,prname,prid,gpocatid,acname,gpraff,hlcaff,accComments,accCurrency,dealerid,dealername,closingdate)
{//accCurrency is to get the currency based on account
	var obj = eval("document.all.Lbl_Regn");
	obj.innerHTML = "&nbsp;" + rgname;

    obj = eval("document.all.Lbl_AdName");
    obj.innerHTML = "&nbsp;" + adname;

    obj = eval("document.all.Lbl_DName");
    obj.innerHTML = "&nbsp;" + dname;

    obj = eval("document.all.Lbl_TName");
    obj.innerHTML = "&nbsp;" + trname;

    obj = eval("document.all.Lbl_RName");
    obj.innerHTML = "&nbsp;" + rpname;

    obj = eval("document.all.Lbl_PName");
    obj.innerHTML = "&nbsp;" + prname;
    
    obj = eval("document.all.Lbl_AccInfo");
	if(obj!=undefined && obj!=null)
    obj.innerHTML = "&nbsp;" + accComments;

    // Account Affiliation
    obj = eval("document.all.Lbl_GPRAffName");
    if(obj!=undefined && obj!=null)
    	obj.innerHTML = "&nbsp;" + gpraff;

    obj = eval("document.all.Lbl_HlthCrAffName");
    if(obj!=undefined && obj!=null)
    	obj.innerHTML = "&nbsp;" + hlcaff;

    obj = eval("document.all.Lbl_DId");
    if(obj!=undefined && obj!=null)
    obj.innerHTML = "&nbsp;" + dealerid;
    
    obj = eval("document.all.Lbl_Dname");
    if(obj!=undefined && obj!=null)
    obj.innerHTML = "&nbsp;" + dealername;
    
    obj = eval("document.all.Lbl_Closing_Date");
    if(obj!=undefined && obj!=null)
    obj.innerHTML = "&nbsp;" + closingdate;
    
    if (document.frmPhOrder)
	{
    	document.frmPhOrder.hAccCurrency.value = (accCurrency == '')?strAccSessCurrency:accCurrency;
		obj = eval("document.all.hGpoId");
		obj.value = prid;
		if (prid != ''){
			setConstructCodes(prid);
			document.getElementById('fmCart').contentWindow.document.frmCart.hGpoId.value = prid;
		}
	}
	//obj = eval("document.all.Lbl_GpoCat");
    //obj.innerHTML = "&nbsp;" + gpocatid;

	obj = document.all.tabAcSum;
	obj.style.display = 'table';

}

function setConstructCodes(id)
{
	if (id == '')
	{
		if(document.getElementById('fmCart').contentWindow.document.frmCart.Cbo_Construct !=undefined ){
			document.getElementById('fmCart').contentWindow.document.frmCart.Cbo_Construct.disabled = true;
		}
		if(document.getElementById('fmCart').contentWindow.document.frmCart.Btn_UpdatePrice !=undefined ){
			document.getElementById('fmCart').contentWindow.document.frmCart.Btn_UpdatePrice.disabled = true;
		}
	}
	else
	{
		if(document.getElementById('fmCart').contentWindow.document.frmCart.Cbo_Construct !=undefined ){
			document.getElementById('fmCart').contentWindow.document.frmCart.Cbo_Construct.disabled = false;
		}
		if(document.getElementById('fmCart').contentWindow.document.frmCart.Btn_UpdatePrice !=undefined ){
			document.getElementById('fmCart').contentWindow.document.frmCart.Btn_UpdatePrice.disabled = false;
		}
	}
}


function fnSetConstructs(obj)
{
	var xml = obj[0].childNodes;
	var len = xml.length;
	var obj = document.getElementById('fmCart').contentWindow.document.frmCart.Cbo_Construct;
	for (var x=0; x<len; x++) 
	{
		var elOptNew = document.createElement('option');
		elOptNew.value = parseXmlNode(xml[x].childNodes[0].firstChild);
		elOptNew.id = parseXmlNode(xml[x].childNodes[1].firstChild);
		elOptNew.text = parseXmlNode(xml[x].childNodes[2].firstChild) + ' - '+ parseXmlNode(xml[x].childNodes[3].firstChild) + ' - $' + formatNumber(parseXmlNode(xml[x].childNodes[1].firstChild));
		obj.add(elOptNew, x+1); 
	}
}

function fnReset()
{
	var opt = (validateObject(document.frmPhOrder.hOpt))?document.frmPhOrder.hOpt.value:'';//validating the object
	if(opt == 'QUOTE'){
		location.href = "/GmPageControllerServlet?strPgToLoad=gmIncShipDetails.do?screenType=Order&RE_FORWARD=gmOrderItemServlet&strOpt=QUOTE&strHideControlNum=no&"+fnAppendCompanyInfo();
	}else if(opt == 'PROFORMA'){
		location.href = "/GmPageControllerServlet?strPgToLoad=gmIncShipDetails.do?screenType=Order&RE_FORWARD=gmOrderItemServlet&strOpt=PROFORMA&strHideControlNum=yes&"+fnAppendCompanyInfo();
	}else{
		//location.href = '/GmOrderItemServlet?hAction=Load&hOpt=PHONE';
		location.href = "/GmPageControllerServlet?strPgToLoad=gmIncShipDetails.do?screenType=Order&RE_FORWARD=gmOrderItemServlet&strOpt=PHONE&strHideControlNum=no&"+fnAppendCompanyInfo();
	}
}


function fnAcIdBlur(obj)
{
	if(document.all.Btn_PlaceOrd){//Enable once the shipping charge is calculated
			document.all.Btn_PlaceOrd.disabled = true;
	}
	changeBgColor(obj,'#ffffff');
	if (obj.value != '')
	{
		//check account is on credit hold. if yes show warning msg and hide place order button
		fnValidateCreditHold(obj.value,["Btn_PlaceOrd"],"frmPhOrder");
		fnCallAjax('GETACC');
		if (validateObject(document.all.Txt_SurgDate) && strShowDOID !='NO')//validating the object
		{
			document.all.Txt_SurgDate.focus();
		}
		
		if(validateObject(document.frmPhOrder)){
			if (validateObject(document.frmPhOrder.Txt_ParentOrdId))
				{
					document.frmPhOrder.Txt_ParentOrdId.value = "";
				}
			document.frmPhOrder.Txt_PO.value = "";
		}
		if(validateObject(window.frames.fmCart)){
			var iFramObj = document.getElementById('fmCart').contentWindow;
			var varRowCnt = iFramObj.document.frmCart.hRowCnt.value;
			for (val=0; val < varRowCnt; val ++)
			{
				obj = eval("document.getElementById('fmCart').contentWindow.document.frmCart.Txt_Qty"+val);
				obj.value = '';
				obj = eval("document.getElementById('fmCart').contentWindow.document.frmCart.Txt_PNum"+val);
				obj.value = '';
				obj = eval("document.getElementById('fmCart').contentWindow.document.frmCart.Txt_Price"+val);
				obj.value = '';
				obj = "Lbl_Desc".concat(val);
				obj = document.getElementById('fmCart').contentWindow.document.getElementById(obj);
				obj.innerHTML = "";
				obj = "Lbl_Stock".concat(val);
				obj = document.getElementById('fmCart').contentWindow.document.getElementById(obj);
				obj.innerHTML = "";
				obj = "Lbl_Amount".concat(val);
				obj = document.getElementById('fmCart').contentWindow.document.getElementById(obj);
				obj.innerHTML = "";
				obj = eval("document.getElementById('fmCart').contentWindow.document.frmCart.Txt_unit_price"+val);
				obj.value = '';
				obj = eval("document.getElementById('fmCart').contentWindow.document.frmCart.Txt_unit_adj"+val);
				obj.value = '';
				obj = eval("document.getElementById('fmCart').contentWindow.document.frmCart.Cbo_adj_code"+val);
				obj.value = '0';
			}
			iFramObj.fnCalculateTotal();
		}
	}
}

function fnAcBlur(obj)
{	
	val=document.all.Cbo_BillTo.value;
	if (document.frmPhOrder)
	{
		//var objid = obj.id;
		//objid = objid.substr(0,objid.length-2);
		//var obj = eval("document.frmPhOrder."+objid);
		//var val = obj.value;		
		obj = document.frmPhOrder.Txt_AccId;
	}
	
	if (document.frmAccount)
	{
		//var objid = obj.id;
		//var val = obj.value;
		document.frmAccount.hAccId.value = val;
	}
	document.all.Txt_AccId.value = val;	
	fnAcIdBlur(obj);
}

function fnCheckDOFormat()
{
	var bl = false;
	var obj = document.frmPhOrder.Txt_OrdId;
	var dtformat = document.frmPhOrder.dateFormat.value;
	var opt = (validateObject(document.frmPhOrder.hOpt))?document.frmPhOrder.hOpt.value:'';//validating the object
	if(opt != 'QUOTE' && opt != 'PROFORMA'){ // This DO format is only form New Order, not for Quote
		var val = obj.value;
		var month;
		var strdate;
		var day;
		var count =0;
		if (val.length != 0)
		{
			var strobj =  val.split('-');
			strdate = strobj[1];
			
			if (strobj.length != 3)
			{
				bl = true;
			}
	        if(strobj.length == 3)
	        {
	        	
				if (strobj[1].length != 6 || strobj[2].length == 0)
				{
					bl = true;
				}
				var dtarr = dtformat.split('/');
				if(dtarr.length <3){
					dtarr = dtformat.split('.');
				}
				if(dtarr[0]  == 'MM' || dtarr[0]  == 'mm'){
					 month = strdate.substring(2,4);
				}
				if (dtarr[1]  == 'dd' || dtarr[1]== 'DD'){
					 day = strdate.substring(4,6);
				}
				if (dtarr[0]  == 'dd' || dtarr[0]== 'DD'){
					 day = strdate.substring(4,6);
				}
				if (dtarr[1]  == 'MM' || dtarr[1]  == 'mm'){
					 month = strdate.substring(2,4);
				}
				if (month > 12 || day > 31)
				{
					bl = true;
				}
	        }
			for(i=0 ; i< strobj.length ;i++){
				if(isNaN(strobj[i]) || strobj[i] == 0)
				{
					count++;
				}
			}
			if(count > 0){
				bl = true;	
			}
		}
	}
	return bl;

}


//Trims the leading and trailing spaces
function trim(str)
{
	str = new String(str);
	var intLen = str.length;
	var intBegin = 0;
	var intEnd = intLen - 1;
	while ((str.charCodeAt(intBegin) == 13  || str.charCodeAt(intBegin) == 10 || str.charAt(intBegin) == " ") && intBegin < intLen)
	{
		if (str.charCodeAt(intBegin) == 13 || str.charCodeAt(intBegin) == 10 )
			intBegin+=2;
		else
        		intBegin++;
	}
	while ((str.charCodeAt(intEnd) == 13  || str.charCodeAt(intBegin) == 10 || str.charAt(intEnd) == " ") && intBegin < intEnd)
	{
		if (str.charCodeAt(intEnd) == 13 || str.charCodeAt(intBegin) == 10 )
			intEnd-=2;
		else
			intEnd--;
	}
	return str.substring(intBegin, intEnd+1);
}
function fnCreateQuoteString(dodate){
	var cnt = '';
	if(document.frmPhOrder.hQuoteCnt!= undefined){
		cnt = document.frmPhOrder.hQuoteCnt.value;
	}
	var strDiscountIndex = '';
	if(document.frmPhOrder.hDiscountIndex != undefined){
		strDiscountIndex =document.frmPhOrder.hDiscountIndex.value;
	}
	var str;
	var strquote = '';
	var accDiscnt = 0;
	var strSetIdFl = '';
	var strOpt = document.frmPhOrder.hOpt.value;
	var accDiscnt = parseFloat(document.frmPhOrder.hAccDiscnt.value);
	accDiscnt = trim(accDiscnt!='')?parseFloat(accDiscnt):0;
	// MNTTASK-4744 - Discount Calculation : Addition Acc Dis and Order Dis value when submit. 
	for (i=0;i<cnt;i++){
		var obj = eval("document.frmPhOrder.Txt_Quote"+i);
		id = obj.id;
		val = obj.value;
		// 10304731: Patient Name code id of Poland
		if((id == '400145' || id == '400108' || id == '10304731')  && val != '' && val.length > 100){//400108 & 400145: Patient Name(code id is different for Order and Quote)
			Error_Details(message[10551]);
		}
		if (val != '' || accDiscnt > 0){
			if(i==strDiscountIndex){
				var discountVal = val.replace(/%/g,'');
				discountVal = parseFloat(formatNumber(discountVal)) + parseFloat(formatNumber(accDiscnt));
				str = '^' + id + '^' + formatNumber(discountVal) + '|';
			}else{ 
				// When create order for OUS country that time will give set id.
				if(id == '4000098' && val != ''){ // Set ID 
					strSetIdFl = '^' + '4000099' + '^' + 'Y' + '|'; // Set ID Flag
					var atpos =  val.indexOf(".");
					var atDotArr =  val.split(".");
					var atposComma =  val.indexOf(",");
					var array =  val.split(",");
					var length = array.length;
					var atDotArrlen = atDotArr.length;
					var Arrval = array[1];
					var dotArrVal = atDotArr[2];

					// Do not enter multiple set id in for one order id.
					if(Arrval != '' && Arrval != undefined ){
						Error_Details(message[10552]);
					}
					// Do not enter more that one ./,/ 
					if(atpos == -1 || atposComma != -1 || length == 3 || atDotArrlen == 3 || (dotArrVal != '' && dotArrVal != undefined)){
						Error_Details(message[10553]);
					}
				}
				if(id == '400151' && val != ''){ // Date of Surgery							
					CommonDateValidation(obj,dateFmt,Error_Details_Trans(message[10554],dateFmt));
				}
				str = '^' + id + '^' + val + '|' + strSetIdFl;
			}
			strquote = strquote + str;
		}
		
		if(vcountryCode == 'au' && (id == '400151' && dodate != '')){			
			eval("document.frmPhOrder.Txt_Quote"+i).value = dodate;
		}				
	}
	/*if (ErrorCount > 0){
		fnStopProgress();
		Error_Show();
		Error_Clear();
		return false;
	}else{*/
		return strquote;
	//}
}
//MNTTASK-7099 THIS IS USED TO CHECK THE AVAIABLITY OF DO-ID IN AJAX.
function fnCheckOrderId(doid,labelid){	
	fnClearDiv();		
	var doidobj = (doid.value).toString().split("-");
	var arrlen = doidobj.length;
	var doido =  doidobj[2];
	var accId = document.all.Txt_AccId.value;
	var opt = (validateObject(document.frmPhOrder.hOpt))?document.frmPhOrder.hOpt.value:'';//validating the object
	if (arrlen == 3 && doidobj[2] != '' && doidobj[1] !='' && doidobj[0] !='' && accId != '' ){
		if(!fnCheckDOFormat()){
			doid=(doid.value).replace(/-/g,'');
			if(!isNaN(doid)){
				fnCallAjax('CHECKDOID');
			}else{
				Error_Details(Error_Details_Trans(message[10555],labelid));
			}
		}else{
			if(opt != undefined && opt != 'QUOTE')
				Error_Details(Error_Details_Trans(message[10555],labelid));		
		}
	}else if( arrlen != 3 && arrlen != 1){
		Error_Details(Error_Details_Trans(message[10555],labelid));		
	}
	if(ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}
}
function showimage() {
	var doId = document.all.Txt_OrdId.value;
	var hdoId = document.frmPhOrder.hOrderId.value;
    if (req.readyState == 4) {
        if (req.status == 200) {
	        var xmlDoc = req.responseText;
	        if(xmlDoc.indexOf("DUPLICATE")!= -1){
	            document.getElementById("DivShowDOIDExists").style.display='';
	            validOrdIdFl = false;
	        }else if(xmlDoc.indexOf("AVAILABLE")!= -1){  
	            document.getElementById("DivShowDOIDAvail").style.display='';
	            document.getElementById("npidtls").style.display = 'inline';
	            validOrdIdFl = true;
	            if(hdoId != '' && doId != hdoId){
	            	//PMT#52854 - To empty npi trans id
                    if(fnValidateObject("hNpiPareTranId")){
                          document.getElementById("hNpiPareTranId").value = '';
                    }
                    fnSaveParentRow('');
	            }
	            document.frmPhOrder.hOrderId.value = doId;
	        }
	        if(vcountryCode == 'au'){
	        	var strArray = xmlDoc.split('-');
	        	fnCreateQuoteString(strArray[1]);
	        }
        }
    }
}
function fnClearDiv(){
	  document.getElementById("DivShowDOIDExists").style.display='none';
      document.getElementById("DivShowDOIDAvail").style.display='none';	
      //fnSetDOID('focus','');
}

/* Function to validate the plant which is selected on the top based on the part number
 Only viacell part can be raised in the plant 'San Antonio Globus'. Otherwise it should throw validation
*/
function fnValidatePartPlant(){
	// If the company is enabled for viacell part, then should validate the part number and the plant
	if(viaCellCompFl == 'Y'){
		var confirmSubmit = true;
		
		// Call the function to validate the part
		fnGetValidMsg();
		if(ErrorCount>0){
			Error_Show();
			Error_Clear();
			return false;
		}
	}
	// If plant and part selected is correct, call the submit function to proceed with submitting the form	
	fnPlaceOrderSubmit();
}

function fnGetValidMsg(){
	var objPartCell;
	var objImgCell;
	var pnum;
	var errMsg = '';
	var errStr = '';
	var errStr1 = '';
	var errMsg1 = '';
	var objProjCell;
	var startToken = '<tr><td>';
	var endToken = '</td></tr>';
	var shipFl = '';
	
	var varRowCnt = eval("document.getElementById('fmCart').contentWindow.document.frmCart.hRowCnt").value;
	// Loop through each row to check whether the Plant and the part number is correct or not
	for (k=0; k < varRowCnt; k ++)
	{
		objPartCell = eval("document.getElementById('fmCart').contentWindow.document.frmCart.Txt_PNum"+k);
		pnum = objPartCell.value;
		var iframe = document.getElementById('fmCart');
		var innerDoc = iframe.contentDocument || iframe.contentWindow.document;
		objImgCell = innerDoc.getElementById("hPnumValid"+k);
		errMsg = objImgCell.value;

		var orderType = document.frmPhOrder.Cbo_OrdType.value;
		var shipToValue = document.frmPhOrder.shipTo.value;
		objProjCell = innerDoc.getElementById("hPnumProjValid"+k);
		errMsg1 = objProjCell.value;
		if(errMsg1 == 'TISSUEPART'){
			shipFl=true;
		}
		// Check whether the error icon is displaying for the part number or not, If it is there, get the message and store in a variable
		if(pnum != '' && pnum != null && errMsg != '' && errMsg != null){
			errStr = errStr + startToken + errMsg + endToken;
		}
	}
	if(shipFl == true) { //2521= bill and ship; 4122 = hospital. If shipF1 true, show the validation message
		//PMT-40657 Tissue parts ship changes in Bill and ship orders
		//Removing the code to remove validation for the tissue parts
		/*if ((orderType == 2521) && (shipToValue != 4122)) {
			Error_Details(message[10615]);
		}*/
		//PMT-49848 To validate the shipping address for tissue parts
		if ((orderType == 2521) && (shipToValue != 4122)) {
				var shiptoidid = document.frmPhOrder.addressid.value;
				if(shipToValue == 4121 && shiptoidid != null){// sales rep address is hospital
					var address =	document.frmPhOrder.addressid.options[document.frmPhOrder.addressid.selectedIndex].text;
					var addressarr = address.split('-');
					if (addressarr.length > 0 && addressarr[0] != 'Hospital') {
						Error_Details(message[10615]);
					}
				}
			}	
	}else if(errStr != ''){ 	//If error message is not empty, show the validation message
		errStr = '<table>' + errStr + '</table>';
		Error_Details(errStr);
	}
}

function fnPlaceOrderSubmit(){
	var lotOverrideFl = document.frmPhOrder.Chk_LotOverride?document.frmPhOrder.Chk_LotOverride.checked:'';
	var confirmMsg = true;
	var confirmPOMsg = true;
	var custPoId = (document.frmPhOrder.Txt_PO != undefined)?document.frmPhOrder.Txt_PO.value:'';
	var ordType = document.frmPhOrder.Cbo_OrdType.value;
	var egpsVal = '';
	if(ordType == 0 || ordType == null ){
		 Error_Details(message[10556]);	 
	 }
    if (document.frmPhOrder.Txt_OrdId){
 		orderID = document.frmPhOrder.Txt_OrdId.value;
 	}
    var strCtlnNumNeedStr = document.getElementById('fmCart').contentWindow.fnCreateCtrlNumNeededStr();
    
    //If the ajax call to validate the PO is not completed while submiting the form
    if(!CustPOValidFl){
		Error_Details(Error_Details_Trans(message[10557],vcustPOLabel));
	}
    //Used egpsFl for  PC-4659--Add EGPS Usage in DO - SpineIT New order
    if(egpsFl != ''){
		if(document.frmPhOrder.Cbo_egpsusage != undefined){
    	 egpsVal = document.frmPhOrder.Cbo_egpsusage.value;
		}
    	if(egpsFl == 'Y' && egpsVal =='0'){
    		Error_Details("Please provide <b>Capital Equp. Used</b> details to proceed further");	
    	}
    }
    
    //code change for PMT-39389
	if(typeof vNPIMandatoryFl !== 'undefined' && orderID != undefined){
		if(vNPIMandatoryFl = 'Y'  && orderID != null && orderID != '' ){
		  if (document.getElementById("searchTxt_Npi") != undefined && document.getElementById("searchTxt_Surgeon") != undefined){
			var vnpiId = document.getElementById("searchTxt_Npi").value;
			var vsurgeonname = document.getElementById("searchTxt_Surgeon").value;
			if(vNPIOrderType.indexOf(ordType+",") !== -1 ){
			   if(vnpiId == '' || vsurgeonname == ''){
				   Error_Details('Please provide <b>NPI</b> and <b>Surgeon Name</b> details to proceed further');
			  }
			}
		  }
		} 
	}
    
    if (ErrorCount > 0){
			Error_Show();
			Error_Clear();
			return false;
	}
    var strWithoutCtrlNum = document.getElementById('fmCart').contentWindow.fnCreateWithoutCtrlNumStr();    

    if((strWithoutCtrlNum.length > 1) && (((lotOverrideAccess == 'Y') && (lotOverrideFl)) || skpUsdLotValid == 'YES')){
    	strWithoutCtrlNum = strWithoutCtrlNum.substring(0,strWithoutCtrlNum.length-1);
    	confirmMsg = confirm(Error_Details_Trans(message[10558],strWithoutCtrlNum)); 
    }else if((lotOverrideAccess == 'N') && (strWithoutCtrlNum.length > 1)){
    	Error_Details(Error_Details_Trans(message[10559],strWithoutCtrlNum));
    }else if((lotOverrideAccess == 'Y') && (strWithoutCtrlNum.length > 1) && (!lotOverrideFl)){
    	Error_Details(Error_Details_Trans(message[10559],strWithoutCtrlNum));
    }
    /* Validation for Handling the Tissue parts with more than one qty*/
    var strMoreQty = document.getElementById('fmCart').contentWindow.fnCreateMoreQtyString();
    if(strMoreQty.length > 1){
    	strMoreQty = strMoreQty.substring(0,strMoreQty.length-1);
    	var partCnumArray = strMoreQty.split(",");
    	var lotNums = '';
    	for (var j=0;j< partCnumArray.length;j++ ){ 
    		var partArray = partCnumArray[j].split("@");
    		lotNums = lotNums + partArray[1] + ',';
		}
    	var index = lotNums.lastIndexOf(",");
    	lotNums = lotNums.substring(0, index) + lotNums.substring(index + 1);  
    	if(document.getElementById('fmCart') != undefined || document.getElementById('fmCart') !=null){  
	    	var iframe = document.getElementById('fmCart');
			var innerDoc = iframe.contentDocument || iframe.contentWindow.document;
			if(innerDoc.getElementById("hproductMat"+ctrlId) != undefined || innerDoc.getElementById("hproductMat"+ctrlId) !=null){
			var productMat = innerDoc.getElementById("hproductMat"+ctrlId);
			productMat = productMat.value;
			    if(productMat == '100845'){ // 100845 - TISSUE
			    	Error_Details(Error_Details_Trans(message[10560],lotNums));
			     }
			}
    	}
    }
    if(!confirmMsg){
    	return false
    }
    
    //To confirm the customer PO
    if(POMessage == 'DUPLICATE' && custPoId != ''){
    	confirmPOMsg = confirm(Error_Details_Trans(message[10561],vcustPOLabel));
    }
    
    
    if(!confirmPOMsg){
    	return false;
    }
    if(confirmMsg)
    fnCallAjax('validateControlNumber');
    
    var txtOrdId = '';
	if(document.frmPhOrder.Txt_OrdId.value!= undefined) {
		txtOrdId = document.frmPhOrder.Txt_OrdId.value;
	}
    fnUpdateTagRecords(txtOrdId); //function to update Y to valid tags PMT-24435
}

//while tab out in Customer PO text box after it will focus in NPI text box
//Moved the Part Number text box focus code to fnAutoFocus() function
function fnPartNumTxtFocus(){
	document.getElementById("searchTxt_Npi").focus();
}



function fnCallAjaxCtrlNum(val,pnum,cnum,trcnt)
{
	var allParts = '';
	var ajax = '';
	ctrlId = trcnt;
	var accid = document.all.Txt_AccId.value;
	if(document.all.Txt_OrdId){
		var doid = document.all.Txt_OrdId.value;
	}
	// The PhoneOrder.js file calling in many places. Need to be check undefined.
	var orderType = (document.frmPhOrder) ? document.frmPhOrder.Cbo_OrdType.value:'';
	var strShipTo = (document.all.shipTo) ? document.all.shipTo.value:'';
	var strName = (document.all.names) ? document.all.names.value:'';
	var strAddress = (document.all.addressid) ? document.all.addressid.value:'';
	
	if (document.frmPhOrder)
	{
		var repid = document.all.hRepId.value;
	}
    var url = fnAjaxURLAppendCmpInfo("/GmOrderAjaxServlet?cnum="+encodeURIComponent(cnum)+"&pnum="+encodeURIComponent(pnum)+"&RE_AJAX_WRAPPER=AJAXWRAPPER&orderType="+encodeURIComponent(orderType)+"&opt=" + encodeURIComponent(val)+ "&acc=" + encodeURIComponent(accid)+ "&rep="+encodeURIComponent(repid)+"&doid="+trim(doid)+ "&PartNums="+encodeURIComponent(allParts)+"&shipTo=" +strShipTo + "&names=" +  strName + "&addressid=" + strAddress+"&submitFl=Y");
	if (typeof XMLHttpRequest != "undefined") 
	{
	   req = new XMLHttpRequest();
	} 
	else if (window.ActiveXObject) 
	{
	   req = new ActiveXObject("Microsoft.XMLHTTP");
	}
	req.open("GET", url, true);
	if(val == 'validateControlNumber'){
		req.onreadystatechange = callCtrlNumInValid;
		}
	req.send(null);
}

function callCtrlNumInValid() {
    if (req.readyState == 4) {
        if (req.status == 200) {
	        var xmlDoc = req.responseText;
	        var existMsg = '';
	        var token = '';
	        var commaIndex = 0;
	        var pNumValidMsg = '';
	       if(trim(xmlDoc) != '' ){
	    	var iframe = document.getElementById('fmCart');
			var innerDoc = iframe.contentDocument || iframe.contentWindow.document;
			var txtObj = innerDoc.getElementById("DivShowCnumNotExists"+ctrlId);	
			pNumValidMsg = innerDoc.getElementById("hPnumValid"+ctrlId);
			txtObj.style.display='';
			// If there is any error message for the part number, then that validation also need to display along with control number validation
			existMsg = pNumValidMsg.value;
			token = existMsg != ''? '\n' : token;
			xmlDoc = xmlDoc.replace('<table><tr><td>','');
			xmlDoc = xmlDoc.replace('</td></tr></table>','');
			// show both the part number validation and control number validation over the 'x' icon
			txtObj.title= existMsg + token + message[10596];
	       }
	       else
	       {
	    	    var iframe = document.getElementById('fmCart');
				var innerDoc = iframe.contentDocument || iframe.contentWindow.document;
				var txtObj = innerDoc.getElementById("DivShowCnumAvl"+ctrlId);
				var txtNonExistObj = innerDoc.getElementById("DivShowCnumNotExists"+ctrlId);
				pNumValidMsg = innerDoc.getElementById("hPnumValid"+ctrlId);
				existMsg = pNumValidMsg.value;
				token = existMsg != ''? '\n' : token;

				// Evn if the control number entered is correct and If part number validation is there, then it should show
				if(existMsg != '' && existMsg != null){
					if(existMsg != 'TISSUEPART'){
						txtNonExistObj.style.display='';
						txtNonExistObj.title = existMsg;
					}
					else{
						txtObj.style.display='';
						txtObj.title = existMsg + token + message[10597];
					}
				}else{
					txtObj.style.display='';
					txtObj.title = message[10597];
				}
				
	       }
         }
    }
}

//validate the space - Customer PO
function fnValidatePOSpace(CustPO) {
	var regexp = /\s/g;
	if (regexp.test(CustPO)) {
		Error_Details(message[10562]);
	}
}

function fnCheckHoldFl(){
	var holdFl = false;
	if(document.frmPhOrder.hHoldSts != undefined){
		var holdSts = document.frmPhOrder.hHoldSts.value;
	}
	var varRowCnt = document.getElementById('fmCart').contentWindow.document.frmCart.hRowCnt.value;
	var objPartAmt = '';
	var objPriceCell = '';
	var partAmtVal = '';
	var partPriceVal = '';
	for (k=0; k <= varRowCnt; k ++)
	{
		objPartAmt = eval("document.getElementById('fmCart').contentWindow.document.frmCart.hPartPrice"+k);
		objPriceCell = eval("document.getElementById('fmCart').contentWindow.document.frmCart.Txt_Price"+k);
		
		if(objPartAmt!=null && objPartAmt!=undefined){
			partAmtVal = objPartAmt.value;
			partPriceVal = objPriceCell.value;
			if((partAmtVal !='' && partPriceVal !='' && partAmtVal != partPriceVal) || (partAmtVal <= '0.00')){
				holdFl = true;
			}
		}
	}
	if(document.frmPhOrder.hHoldFl != undefined){
		if(holdFl && holdSts=='YES'){
			document.frmPhOrder.hHoldFl.value = 'Y';
		}else{
			document.frmPhOrder.hHoldFl.value = '';
		}
	}
}

function fnSetHardCopyPO()
{
	var modeval = document.frmPhOrder.Cbo_Mode.value;
	
	if (modeval == '5017')
	{
		document.frmPhOrder.Chk_HardPO.disabled = true;
	}else{
		document.frmPhOrder.Chk_HardPO.disabled = false;
	}
}

//adding method for US and OUS code migration
function fnEnableDiscount(obj)
{
	var opt = (document.all.hOpt != undefined)?document.all.hOpt.value:'';
	if (vcountryCode == 'en'){// On tabout from Discount column, need to replace the value of Price EA with the base price of the part
		document.getElementById('fmCart').contentWindow.fnResetBasePrice();
	}
	var btn_calc_discount = eval("document.getElementById('fmCart').contentWindow.document.frmCart.Btn_Calculate");
	var accDiscnt = 0;
	accDiscnt = (document.frmPhOrder.hAccDiscnt != undefined)?document.frmPhOrder.hAccDiscnt.value:0;
	var strValue = (obj != undefined)?eval(obj.value):0;
	
	if (btn_calc_discount != undefined)
	{	if((strValue=="" || strValue == "0" || strValue == undefined) && accDiscnt == 0){
			btn_calc_discount.disabled = true;
		}else{
			btn_calc_discount.disabled = false;
		}
	}
}

//adding method for US and OUS code migration
function fnNumbersOnly(e,obj){
	var unicode=e.charCode? e.charCode : e.keyCode
	var DiscountVal = obj.value;
	if (unicode!=8){ //if the key isn't the backspace key (which we should allow)
		if ((unicode<48||unicode>57) && (unicode != 46)){ // If not a number or dot
			Error_Details(message[10563]);
			if (ErrorCount > 0){
				Error_Show();
				Error_Clear();
				return false;
			}
		}
	}
}

//To clear the images image.
function fnPOClearDiv(){
	//Removing the condition to make the functionality available for US also
	/*if(vcountryCode != 'en'){*/// Only for OUS
		document.getElementById("DivShowPOIDExists").style.display='none';
	    document.getElementById("DivShowPOIDAvail").style.display='none';
	/*}*/
}

//To check whther the customer PO is available or not
function fnCheckPO(val){
	/*if(vcountryCode != 'en'){*/// Only for OUS
		var custPoId = document.frmPhOrder.Txt_PO.value;
		if(TRIM(custPoId) == ''){
			POMessage = '';
			return false;
		}
		POMessage = '';
		CustPOValidFl = false;
	    var url = fnAjaxURLAppendCmpInfo("/GmOrderAjaxServlet?&opt="+ encodeURIComponent(val)+"&Txt_PO="+custPoId);
		if (typeof XMLHttpRequest != "undefined") 
		{
		   req = new XMLHttpRequest();
		} 
		else if (window.ActiveXObject) 
		{
		   req = new ActiveXObject("Microsoft.XMLHTTP");
		}
		req.open("GET", url, true);
		if (val == 'CHECKPO'){             
			req.onreadystatechange = fnShowPOImage;
		}
		req.send(null);
	/*}*/
}

//To open the order details, when click on "L" icon
function fnModifyOrder(){
	var strCustPo = document.frmPhOrder.Txt_PO.value;
	windowOpener('/GmOrderVoidServlet?hMode=Reload&strOpt=CUSTPO&Txt_CustPO='+strCustPo,'OrderDetails',"resizable=yes,scrollbars=yes,top=475,left=420,width=1200,height=300");
}

//To show the images
function fnShowPOImage() {
    if (req.readyState == 4) {
        if (req.status == 200) {
	        var xmlDoc = req.responseText;	             
	        if(xmlDoc.indexOf("DUPLICATE")!= -1){
	        	document.getElementById("DivShowPOIDAvail").style.display = 'none';
	        	document.getElementById("DivShowPOIDExists").style.display = 'inline';
	            POMessage = 'DUPLICATE';
	        }else if(xmlDoc.indexOf("AVAILABLE")!= -1){  
	        	document.getElementById("DivShowPOIDExists").style.display = 'none';
	        	document.getElementById("DivShowPOIDAvail").style.display = 'inline';
	            POMessage = 'AVAILABLE';
	        }
        }
        CustPOValidFl = true;
    }
}


function fnNumbersOnlyWithDecimal(e,obj){
	var unicode=e.charCode? e.charCode : e.keyCode
	var DiscountVal = obj.value;
	if (unicode!=8){ //if the key isn't the backspace key (which we should allow)
		if ((unicode<48||unicode>57) && (unicode != 46)){ // If not a number or dot
			Error_Details(message[10563]);
			if (ErrorCount > 0){
				Error_Show();
				Error_Clear();
				return false;
			}
		}
	}
}

// This function used to enable/disable the order cart field
function fnCartEnableDisableFields() {
	var cnt = document.getElementById('fmCart').contentWindow.document.frmCart.hRowCnt.value;

	for (i = 0; i < cnt; i++) {
		// to get the order type value
		objFlag = fnCartGet('Cbo_OrdPartType');
		
		if (objFlag != '') {
			obj = eval("document.getElementById('fmCart').contentWindow.document.frmCart.Cbo_OrdPartType"+ i);
			// to set type dropdown value
			fnCartFieldUpd(obj, 50300, objFlag);
		}
		// to get the control # value
		objFlag = fnCartGet('Txt_Cnum');
		if (objFlag != '') {
			controlObj = eval("document.getElementById('fmCart').contentWindow.document.frmCart.Txt_Cnum"+ i);
			fnCartFieldUpd(controlObj, '', objFlag);
		}
		// to get the BO Flag
		objFlag = fnCartGet('Chk_BO');
		if (objFlag != '') {
			boObj = eval("document.getElementById('fmCart').contentWindow.document.frmCart.Chk_BO"+ i);
			fnCartFieldUpd(boObj, '', objFlag);
		}
		// to get the Adj code
		objFlag = fnCartGet('Cbo_adj_code');
		if (objFlag != '') {
			adjCodeObj = eval("document.getElementById('fmCart').contentWindow.document.frmCart.Cbo_adj_code"+ i);
			fnCartFieldUpd(adjCodeObj, '', objFlag);
		}
		// to get the Tag ID
		objFlag = fnCartGet('Txt_RefId');
		if (objFlag != '') {
			tagObj = eval("document.getElementById('fmCart').contentWindow.document.frmCart.Txt_RefId"+ i);
			fnCartFieldUpd(tagObj, '', objFlag);
		}
		// to get the Cap Flag
		objFlag = fnCartGet('Chk_Cap');
		if (objFlag != '') {
			capObj = eval("document.getElementById('fmCart').contentWindow.document.frmCart.Chk_Cap"+ i);
			fnCartFieldUpd(capObj, '', objFlag);
		}
	}
}
// This function used to update the cart field based on parameters value
function fnCartFieldUpd(obj, val, flag) {
	if (obj != undefined) {
		obj.value = val;
		obj.disabled = flag;
	}
}
// to get hashmap value

this.cartKeyArray = new Array();
this.cartValArray = new Array();
// This function used to put the values into array
function fnCartPut(key, val) {
	var elementIndex = this.fnCartFindIt(key);
	if (elementIndex == (-1)) {
		this.cartKeyArray.push(key);
		this.cartValArray.push(val);
	} else {
		this.cartValArray[elementIndex] = val;
	}
}
// This function used to get the values from array
function fnCartGet(key) {
	var result = null;
	var elementIndex = this.fnCartFindIt(key);
	if (elementIndex != (-1)) {
		result = this.cartValArray[elementIndex];
	} else {
		result = "";
	}
	return result;
}
// this function used to find the values in array and returns
function fnCartFindIt(key) {
	var result = (-1);

	for (var i = 0; i < this.cartKeyArray.length; i++) {
		if (this.cartKeyArray[i] == key) {
			result = i;
			break;
		}
	}
	return result;
}

function fnSetNPINum(obj){
	var surgeonnm = document.getElementById("Txt_Surgeon").value;
	document.getElementById("searchTxt_Npi").value = surgeonnm;
	fnSaveParentRow('');
}

function fnSetSurgeonNm(obj){
	var npiId = document.getElementById("Txt_Npi").value;
	document.getElementById("searchTxt_Surgeon").value = npiId;
	fnSaveParentRow('');
}
// While tabout from NPI #/Surgeon name field
function fnAutoFocus(obj){
	// After Tabout from NPI#, it should focus on Surgeon name
	// Aftet tabout from Surgeon name, the value should be saved
	var npiId = document.getElementById("searchTxt_Npi").value;
	var surgeonnm = document.getElementById("searchTxt_Surgeon").value;
	var acceptNum;
	var errorFl;
	// NPI field should accept only numbers and surgeon field should accept only characters with dot
	if(npiId != ''){
		acceptNum = /[^0-9]/g;
        errorFl = acceptNum.test(npiId);
        if (errorFl) {  /*Accept only Digits in NPI# field*/
        	Error_Details("Please provide NPI# in digits");
        }
	}
	if(surgeonnm != ''){
		acceptNum = /^[a-zA-Z\.\, ]*$/;
        errorFl = acceptNum.test(surgeonnm);
        if (!errorFl) {  /*Accept only Digits in NPI# field*/
        	Error_Details("Please provide surgeon name in letters");
        }
	}
	
	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}
	
	if(obj.id == 'searchTxt_Npi'){
		document.getElementById("searchTxt_Surgeon").focus();
	}else if(validOrdIdFl && (npiId != '' || surgeonnm!= '')){
		fnSaveParentRow('');
		pobj = eval("document.getElementById('fmCart').contentWindow.document.frmCart.Txt_PNum"+0);
		pobj.focus(); // Focusing to Part number text box first row.
	}
}

var mCal='';
var calendars = {};

function showSglCalendar(calendardiv,textboxref,dateformat){
	dateformat = getObjTop().gCompJSDateFmt;
	// Overite style for calendar only from class RightTableCaption.
	onSetStyleToCalendar(calendardiv);
	if (dateformat == null){
		dateformat='%m/%d/%Y';
	}
	
	if(!calendars[calendardiv]) 
		calendars[calendardiv]= initSurgCal(calendardiv,textboxref,dateformat);
	
	//This line for today date will display in BOLD and this method will be in dhtmlxcalendar.js
	calendars[calendardiv].enableTodayHighlight(true); // or false
	
	if(document.getElementById(textboxref).value.length>0){
		calendars[calendardiv].setDate(document.getElementById(textboxref).value);
	}

	if(calendars[calendardiv].isVisible())
		calendars[calendardiv].hide();
	else 
		calendars[calendardiv].show();
}

// The following new changes will applicable to current calendar.  
function initSurgCal(calendardiv,textboxref,dateformat){
	var doDateFmt = document.frmPhOrder.dateFormat.value;
		if((navigator.userAgent.match(/iPhone/i))!=null || (navigator.userAgent.match(/iPad/i))!=null)
		{
			//This commented code for New version 3.5. The old dhtmlx version we can't editable month and year in IPAD. Now we can edit.
			//mCal = new dhtmlxCalendarObject(calendardiv, false, {isMonthEditable: false,isYearEditable: false });
			mCal = new dhtmlXCalendarObject(calendardiv);
		}else{
			//mCal = new dhtmlxCalendarObject(calendardiv, false, {isMonthEditable: true,isYearEditable: true});
			mCal = new dhtmlXCalendarObject(calendardiv);
		}

	// This range only able to select date. 
	mCal.setSensitiveRange("1970-01-01", "2500-31-12"); 

	// Set date format based on login user.
	mCal.setDateFormat(dateformat);
	
	// Hide the time in new dhtmlx version 3.5
	mCal.hideTime();
	var v_date_str;
	var datedo ;
	// When click calendar icon and set the date in textbox.
	mCal.attachEvent("onClick", function(d) {
		v_date_str = mCal.getFormatedDate(dateformat, d);
		document.getElementById(textboxref).value =  v_date_str;
		datedo = formattedDate(d,doDateFmt);
		calendars[calendardiv].hide();
		fnSetDOID('click',datedo);
		return true;
	 });
	
	mCal.hide();
	document.frmPhOrder.Txt_OrdId.focus();
	return mCal;
}

function formattedDate(dt, dateformat) 
{ 
	dateformat = dateformat.toLowerCase();
    try {
        if (dt != "" && dt != null && dt != undefined &&
            dateformat != "" && dateformat != null && dateformat != undefined) 
        {
            var newdate = new Date(dt);
            var resultedDate = $.datepicker.formatDate(dateformat, new Date(dt)).substring(2);
            if (resultedDate != "" || resultedDate != null || resultedDate != undefined || resultedDate != "NaN/NaN/NaN") 
            {
                dt = resultedDate;
            }
        }
    } catch (err) 
    {

    }
    return dt;
}

function fnSetDOID(action,datedo){
	var doDateFmt = document.frmPhOrder.dateFormat.value;
	var surgDate = document.frmPhOrder.Txt_SurgDate.value;
	var repid = document.all.hRepId.value;
	var formtDt;
	if(repid == ''){
		return;
	}
	if (repid.length == 2)
	{
		repid = '0'+ repid;
	}
	else if (repid.length == 1)
	{
		repid = '00'+ repid;
	}

	if(surgDate != null && surgDate != ''){
		// Convert to MM/dd/yyy, since the date is forming always based on this format
		surgDate = formattoMMddYYYY(surgDate,dateFmt);
		datedo = formattedDate(surgDate,doDateFmt);
	}
	var sysdt = document.frmPhOrder.hSysDate.value;
	datedo = (datedo != null && datedo != '') ? datedo: sysdt;
	var opt = document.frmPhOrder.hOpt.value;
	
	if (opt == 'PHONE' && strShowDOID != 'NO')
	{
		//NET NUMBER CHANGE
		if (dealerid!='' & accountType=='26230710')
		{
			 document.frmPhOrder.Txt_OrdId.value = dealerid + '-'+datedo+'-';;
		}
		else{
			document.frmPhOrder.Txt_OrdId.value = repid + '-'+datedo+'-';;
		}
		
	}
	// document.frmPhOrder.Txt_OrdId.focus();
// Commenting the above and created new function to set the cursor focus to the end of DO ID Text Area
//PMT-24162
	 moveCursorToEnd(document.frmPhOrder.Txt_OrdId);
}


function moveCursorToEnd(el) {
    
	var elemLen = el.value.length;

	el.selectionStart = elemLen;
    el.selectionEnd = elemLen;
    el.focus();
    var range = el.createTextRange();
    range.collapse(false);
    range.select();
}



// Function to convert date to MM/dd/yyyy
function formattoMMddYYYY(dt, dateformat) {
	dateformat = dateformat.toLowerCase();
	var firstsplitPos;
	var lastsplitPos;
	var token;
	var strDay, strMonth, strYear;
	var formtDt = '';
	var arrDate;
	var fVal;
	var sVal;
	var tVal;
	
	if(dateformat.indexOf('/') != -1){
		token = '/';
		splitPos = dateformat.indexOf('/');
		lastsplitPos = dateformat.lastIndexOf('/');
	}else if(dateformat.indexOf('.') != -1){
		token = '.';
		splitPos = dateformat.indexOf('.');
		lastsplitPos = dateformat.lastIndexOf('.');
	}else if(dateformat.indexOf('-') != -1){
		token = '-';
		splitPos = dateformat.indexOf('-');
		lastsplitPos = dateformat.lastIndexOf('-');
	}
	
	arrDate = dt.split(token);
	fVal = dateformat.substring(0,splitPos);
	sVal = dateformat.substring(splitPos+1,lastsplitPos);
	tVal = dateformat.substring(lastsplitPos+1,dateformat.length);
	switch(fVal){
		case 'dd': strDay = dt.substring(0,splitPos);
					break;
		case 'mm': strMonth = dt.substring(0,splitPos);
					break;
		case 'yyyy': strYear = dt.substring(0,splitPos);
					break;
	}
	switch(sVal){
		case 'dd': strDay = dt.substring(splitPos+1,lastsplitPos);
					break;
		case 'mm': strMonth = dt.substring(splitPos+1,lastsplitPos);
					break;
		case 'yyyy': strYear = dt.substring(splitPos+1,lastsplitPos);
					break;
	}
	switch(tVal){
		case 'dd': strDay = dt.substring(lastsplitPos+1,dateformat.length);
					break;
		case 'mm': strMonth = dt.substring(lastsplitPos+1,dateformat.length);
					break;
		case 'yyyy': strYear = dt.substring(lastsplitPos+1,dateformat.length);
					break;
	}
	formtDt = strMonth + '/' + strDay + '/' + strYear;
	return formtDt;
}


//function to open Add tag screen in popup  PMT-24435
function fnAddTags(){
	var txtOrdId = '';
	if(document.frmPhOrder.Txt_OrdId.value!= undefined) {
		txtOrdId = document.frmPhOrder.Txt_OrdId.value;
	}
	var txtAccountId = '';
	if(document.frmPhOrder.Txt_AccId.value!= undefined) {
		txtAccountId = document.frmPhOrder.Txt_AccId.value;
	}
	var tempId = '';
	if(document.frmPhOrder.tempId.value!= undefined) {
		tempId = document.frmPhOrder.tempId.value;
	}
	document.frmPhOrder.tempId.value = tempId;
	 if(txtOrdId!=null && txtAccountId!=null){
		 windowOpener('/gmOrderTagRecordAction.do?method=fetchTagRecordInfo&tagRefId='+txtOrdId+'&accountId='+txtAccountId+'&tempId='+tempId+'&'+fnAppendCompanyInfo(),"AddTagRecordInfo","resizable=yes,scrollbars=yes,top=250,left=300,width=830,height=500");
		 
	    } 
}

//function to enable Add tag button  PMT-24435
function fnEnableAddTagBtn(){
	document.frmPhOrder.Btn_AddTags.disabled = false; 
	var txtOrdId = '';
	if(document.frmPhOrder.Txt_OrdId.value!= undefined) {
		txtOrdId = document.frmPhOrder.Txt_OrdId.value;
	}
	if(txtOrdId == '') {
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmOrderTagRecordAction.do?method=getOrderId&ramdomId='+ Math.random());
		dhtmlxAjax.get(ajaxUrl,function(loader) {
			var response = loader.xmlDoc.responseText;
			document.all.tempId.value = response;
		});
	}
}

//function to update Y to valid tags  PMT-24435
function fnUpdateTagRecords(txtOrdId) {
	if(txtOrdId!=null){
		dhtmlxAjax.get('/gmOrderTagRecordAction.do?method=updateTagDetails&tagRefId='+txtOrdId +'&ramdomId='+ Math.random(),function(loader1){});
	    } 
}