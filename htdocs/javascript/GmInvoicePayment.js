function fnGo()
{
	document.frmAccount.hAction.value = "PAYALL";
	document.frmAccount.submit();
	//fnLoad();
}

function fnPostPayment()
{
	Error_Clear();
	var theinputs=document.getElementsByTagName("input");
	var the_textfields=new Array();
	var str = '';
	var payamt = 0;
	var propayamt = 0;
	var cnt = 0;
	var varPayMode = document.frmAccount.Cbo_PayMode.value;
	var Txt_Details = document.frmAccount.Txt_Details.value;
	
	var blFlag = true;
	
	for(var n=0;n<theinputs.length;n++)
	{
		if(theinputs[n].type=="checkbox" && theinputs[n].name !="Chk_GrpChk_Invoice_Type"&&theinputs[n].name !="Chk_ParentFl")
		{
			if (theinputs[n].checked)
			{
				val = theinputs[n].value;
	
				obj = eval("document.frmAccount.Txt_Post"+val);
				amt = parseFloat(RemoveComma(obj.value));
				obj = eval("document.frmAccount.Txt_PO"+val);
				po = obj.value;
				objDiscount = eval("document.frmAccount.Txt_Discount"+val);	
				objRecdAmount = eval("document.frmAccount.Txt_Recd"+val);
				objInvAmount = eval("document.frmAccount.Txt_InvAmount"+val);

				discount = parseFloat(RemoveComma(objDiscount.value));
				recdAmount = parseFloat(RemoveComma(objRecdAmount.value));
				invAmount= Math.round(parseFloat(RemoveComma(objInvAmount.value)));
			
				//otheramt = recdAmount + amt + discount;
				otheramt = Math.round(parseFloat(recdAmount + amt + discount).toFixed(2));
				
				payamtcalc = Math.round(payamtcalc);
				
				if (otheramt <= 0 && (invAmount > otheramt))					
				{
					blFlag = false;
				}
				if (otheramt >= 0 && (invAmount < otheramt))
				{
					blFlag = false;
				}
				if(payamtcalc < 0 && ppupd!='N'){
					Error_Details(message[7002]);
					Error_Show();
					Error_Clear();
    				return false;
				}
				if(document.frmAccount.Txt_PAmt.value < 0 ){
					Error_Details(message[7003]);
					Error_Show();
					Error_Clear();
    				return false;
				}
				if(ppupd=='Y' && document.frmAccount.Txt_PAmt.value > 0){
					Error_Details(message[7001]);
					Error_Show();
					Error_Clear();
    				return false;
				}				
				if(ppupd=='Y' && payamtcalc == 0){
					blFlag = true;
				}
				if((ppupd!='Y' && ppupd!='N') && payamtcalc == 0){
					blFlag = false;
				}
				if(ppupd=='Y' && payamtcalc != 0){
					Error_Details(message[7000]);
					Error_Show();
					Error_Clear();
    				return false;
				}
				if (!blFlag)
				{
					Error_Details(Error_Details_Trans(message_accounts[300],val));
					Error_Show();
					Error_Clear();
    				return false;
				}
				else
				{
					payamt = payamt + parseFloat(amt) //+ parseFloat(discount); - commenting this for issue 2579
					blFlag = true;
				}
				val = val.replace(/_/g, "-");
				// Added paymode and comments in input string. We are  added two sting in Cash Application screen. The input string called same procedure.
				if (discount != '0.00'){
					// 5013 Discount
					str = str + val + '^' + po + '^'+ '5013' + '^' + Txt_Details + '^'+ discount + '^' + '2701' + '|';
				//	amt = parseInt(amt) - parseFloat(discount);
				}
				if (amt != '0.00') {
					if (varPayMode == 5010 || varPayMode == 5011 || varPayMode == 5012 || varPayMode == 5023 || varPayMode == 26240161 || varPayMode == 26240162) {
						// 5010-check , 5011-Bank Transfer , 5012-Credit card , 5023- Wire transfer , 2700 -- Payment
						str = str + val + '^' + po + '^' + varPayMode + '^'
								+ Txt_Details + '^' + amt + '^' + '2700' + '|';
					} else {
						// 2701 -- Discount
						str = str + val + '^' + po + '^' + varPayMode + '^'
								+ Txt_Details + '^' + amt + '^' + '2701' + '|';
					}

				}
				cnt++;
			}
		}
	}

	if (cnt == 0)
	{
		Error_Details(message_accounts[301]);
		Error_Show();
		Error_Clear();
    	return false;
	}

	if(document.frmAccount.Txt_PAmt.value != "")
	{
	document.frmAccount.Txt_PAmt.value = parseFloat(RemoveComma(document.frmAccount.Txt_PAmt.value));	
	}

	propayamt = document.frmAccount.Txt_PAmt.value;
	payamt=Math.round(payamt*100)/100;// to round number into two decimal point 

		
	if (propayamt == '' && varPayMode!= 5013)
	{
		Error_Details(message_accounts[302]);
		document.frmAccount.Txt_PAmt.focus();
		Error_Show();
		Error_Clear();
		return false;
	}
	//PC-4637-payment date validation
	if (document.frmAccount.Txt_PDate.value == '')
	{
		Error_Details(message_accounts[303]);
		document.frmAccount.Txt_PDate.focus();
		Error_Show();
		Error_Clear();
		return false;
	}else{
		var preDiff = dateDiff(strFromdaysDate,document.frmAccount.Txt_PDate.value, format);
		var futDiff = dateDiff(strLastdaysDate,document.frmAccount.Txt_PDate.value, format);
		if(paymentValidationFl =='Y' && (preDiff < 0 || futDiff > 0)) {
		Error_Details(message_accounts[557])
		document.frmAccount.Txt_PDate.focus();
		Error_Show();
		Error_Clear();
		return false;
		}
		}
	if (varPayMode==0)
	{
	Error_Details(message_accounts[533]);
	Error_Show();
	Error_Clear();
	return false ;
	}
	if (payamt != propayamt && varPayMode!= 5013)
	{
		Error_Details(message_accounts[304]);
		Error_Show();
		Error_Clear();
		return false;
    } 
	
	//Changing below lines for BUG-3806
	document.frmAccount.hInputStr.value = str;
	document.frmAccount.hAction.value = "UPDALL";
	document.frmAccount.action = "/GmInvoiceServlet" ;//While giving '#' in comments section, not able to post
	fnStartProgress('Y');
	document.frmAccount.submit();
}

function fnPrintInvoice(varinv)
{
windowOpener('/GmInvoiceInfoServlet?hAction=Print&hInv='+varinv,"PrntInv","resizable=yes,scrollbars=yes,top=150,left=200,width=750,height=500");
}

var TotalValue = 0;
var payamtcalc = 0;

function fnCalculateAmount(CheckValue)
{
	var amt = 0;
	var val = CheckValue.value;
	pendobj = eval("document.frmAccount.Txt_Pend"+val);
	pendamt = RemoveComma(pendobj.value);
	objPost = eval("document.frmAccount.Txt_Post"+val);	
	objDiscount = eval("document.frmAccount.Txt_Discount"+val);	
	amt = pendamt * 1;
	discount = RemoveComma(objDiscount.value);
	if (discount == null || discount == "")
	discount = '0.00';
	//	alert ("Amt Value is " + amt + " pendamt val is "  + pendamt + "Discount val is  " + discount)  ;
		
	if (CheckValue.checked)
	{
		objPost.value = formatNumber(pendobj.value);
		objDiscount.value = discount;
		TotalValue = TotalValue +  parseFloat(amt)  + parseFloat(discount);
		// alert ("payamt  " + payamtcalc + " amt  " + amt + " discount  " + discount);
		payamtcalc = payamtcalc + parseFloat(amt);
	}
	else
	{
		tval = RemoveComma(objPost.value);
		if ( tval != pendamt)
		{
			amt = tval * 1;
		}
		TotalValue = TotalValue -  parseFloat(amt) +   parseFloat(discount);
		payamtcalc = payamtcalc - parseFloat(amt);
		objPost.value = '';
		objDiscount.value = '';
	
	}
	
	
	document.all.IDCalculate.innerHTML = "&nbsp;" +sessCurSymbol+ formatNumber(payamtcalc) + "&nbsp;"; 
}	

function fnCal(tobj)
{
	tval = tobj.value;
	var theinputs=document.getElementsByTagName("input");
	var the_textfields=new Array();
	var str = '';
	var payamt = 0;
	var cnt = 0;
	
	var payamtcalculated = 0;
	for(var n=0;n<theinputs.length;n++)
	{
		if(theinputs[n].type=="checkbox" && theinputs[n].name !="Chk_GrpChk_Invoice_Type"&&theinputs[n].name !="Chk_ParentFl")
		{
			if (theinputs[n].checked)
			{
				val = theinputs[n].value;
			//	objRecdAmount = eval("document.frmAccount.Txt_Recd"+val);
				objPost = eval("document.frmAccount.Txt_Post"+val);
				objDiscount = eval("document.frmAccount.Txt_Discount"+val);
			//	objInvAmount = eval("document.frmAccount.Txt_InvAmount"+val);
				amt = RemoveComma(objPost.value);
				discount = RemoveComma(objDiscount.value);
			//	recdAmount =RemoveComma(objRecdAmount.value);
			//	invAmount=RemoveComma(objInvAmount.value);
				if (!discount.match(/\d.+/))
				{
					Error_Details(message_accounts[305]);
				}
				payamt = payamt + parseFloat(amt) + parseFloat(discount);
				
			//	if (parseFloat(invAmount) < (parseFloat(recdAmount) +  parseFloat(amt) + parseFloat(discount)))
			//	{
			//		alert ("the 'Invoice Amt' should greater than or equals the sum of 'Amount Recd.', 'Amount to Post' and 'Discount'. ");		
			//		return false;
			//	}
				// alert (" Inside fncal payamtcalc  " + payamtcalc  + " invAmount " + invAmount  +" recdAmount " + recdAmount +" amt  " + amt + " discount  " + discount);
				payamtcalculated = payamtcalculated + parseFloat(amt);
			}
		}
	}
	tobj.value = formatNumber(tval);
	TotalValue = payamt;
	payamtcalc = payamtcalculated;
	document.frmAccount.hTotalValue.value = TotalValue;
	document.all.IDCalculate.innerHTML = "&nbsp;" +sessCurSymbol+ formatNumber(payamtcalc) + "&nbsp;";

	if (ErrorCount > 0)
				{
					Error_Show();
					Error_Clear();
					return false;
				}
}


function fnOpenLog(id)
{
	windowOpener("/GmCommonLogServlet?hType=1201&hID="+id,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
}

function fnSet_hTotalAmt(value)
{
	document.getElementById("hTotalAmt").value = value;
	//document.all.hTotalAmt.value = value
	//document.getElementById("hTotalAmt").value = value
}

function fnLoad()
{
	//if(document.all.hTotalAmt.value == 0)
	if((document.getElementById("hTotalAmt").value == 0) ||(document.getElementById("hTotalAmt") == null))
	{
		document.all.Lbl_Total.innerHTML = sessCurSymbol+'0';
		//document.getElementById("Lbl_Total").innerHTML = '$0'
	}
	else
	{
		//document.getElementById("Lbl_Total").innerHTML = '$' + document.getElementById("hTotalAmt").value;
		document.all.Lbl_Total.innerHTML = sessCurSymbol + document.getElementById("hTotalAmt").value;
	}
	var val = document.all.hCheckedInvTypes.value;
	var obj = document.all.Chk_Invoice_Type;
	fnCheckSelections(val,obj);
}

// Marks the checkboxes as 'checked'
function fnCheckSelections(val,objgrp)
{
	var valobj = val.split(",");
	var sval = '';
	var arrlen = objgrp.length;
	
	if (arrlen > 0)
	{
		for (var j=0;j< valobj.length;j++ )
		{
			sval = valobj[j];
			for (var i=0;i< arrlen;i++ )
			{
				if (objgrp[i].value == sval)
				{
					objgrp[i].checked = true;
					break;
					
				}
			}
		}
	}
	else
	{
		objgrp.checked = true;
	}
}

//this function used to export the files
function fnExportFile(fileID){
	var uploadDir="BATCH_INVOICE";
	document.frmAccount.action = "/GmCommonFileOpenServlet?sId="+fileID+"&uploadString="+uploadDir;
	document.frmAccount.submit();
}


function fnLoad1()
{
	if(strType == 'Account'){
	
	if(parentFl == 'false'){
			document.frmAccount.Chk_ParentFl.checked = false;
		}
	}
	var hTypeValue = document.frmAccount.hType.value;
	if( hTypeValue == 'Account')
	{
		fnDispatchLoad();	
	}
	
	if(hlength >0){
		var inOverdueValue = '0.00';
		document.getElementById('Btn_load').disabled = false;
		document.onkeypress = function(){
			if(event.keyCode == 13){
				fnGetValues();
			}
		}

		if (strInvoiceType != '')
		{
			var objgrp = document.frmAccount.Chk_GrpChk_Invoice_Type;
			fnCheckSelections(strInvoiceType,objgrp);
		}
		if (varIdAccount!= null && strType == 'Account')
		{
			document.all.Txt_AccId.value = varIdAccount;
		}
		
		if(objGridData!='')
			{
			gridObj = initGrid('arsummarydata',objGridData);	
			gridObj.forEachRow(function(id){
	  		var invAmtValue = gridObj.getUserData(id,"addinvamt");
			var invAmtPaid = gridObj.getUserData(id,"invamtpaid");
			var invType = gridObj.getUserData(id,"invtypeid");
			var inOverdueValue = gridObj.getUserData(id,"totaloverdue");
			//50200:Regular, 50202:Credit,50203:Debit
			if(invType=='50200' || invType=='50202' || invType=='50203')
			{
				TotAmt = parseFloat(TotAmt) + (parseFloat(invAmtValue) - parseFloat(invAmtPaid));
		 	if(inOverdueValue=='0'){//Calculation of Overdue amt
					TotOver = parseFloat(TotOver) + (parseFloat(invAmtValue) - parseFloat(invAmtPaid));
					}
		 	}
			});
			
			TotOverdue = parseFloat(TotAmt) - parseFloat(TotOver);
		} 
		if(strAction != 'Load'){
			if(TotAmt>=0 && TotOverdue>=0)
				{
				TotAmt = formatNumber(TotAmt);
				TotOverdue = formatNumber(TotOverdue);
				document.all.Lbl_Total.innerHTML = AccountCurrSign+" "+TotAmt;
		
				if(document.all.Lbl_overdue!=undefined){
					document.all.Lbl_overdue.innerHTML = AccountCurrSign+" "+TotOverdue;
				}
				document.all.hTotalAmtVal.value =  TotAmt;
				document.all.hTotalOverdue.value = TotOverdue;
				document.all.Lbl_Total.innerHTML = document.all.hTotalAmt.value;
				}
			else
				{
				TotAmt = formatNumber(TotAmt);
				TotAmt = TotAmt.substring(1,TotAmt.length);
				document.all.Lbl_Total.innerHTML = AccountCurrSign+" "+ "<font color=red>("+TotAmt+")</font>";
				if(document.all.Lbl_overdue!=undefined){
					document.all.Lbl_overdue.innerHTML = AccountCurrSign+" "+"0.00";
				}
				document.all.hTotalAmtVal.value = "<font color=red>("+TotAmt+")</font>";
				document.all.hTotalOverdue.value = "0.00";
		    	}
		}
	}
	
	
		
}

function fnGetValues()
{ 
	var parentFl = '';
	var sourceType= '';
	
	if(document.frmAccount.Cbo_InvSource != undefined){
		sourceType = document.frmAccount.Cbo_InvSource.value;
	}

	var parentck = eval(document.frmAccount.Chk_ParentFl);
	if (parentck !=undefined){
	  parentFl  = document.frmAccount.Chk_ParentFl.checked;
	}
	
	if(strType == 'Account'){
		accId = document.frmAccount.Cbo_AccId.value;
	}else{
		accId = Cbo_AccId.getSelectedValue();
	}
    if( accId == '0' || accId == ''){
    	if(sourceType == '26240213'){
    		Error_Details(message_accounts[542]);
    	}else{
    		Error_Details(message_accounts[310]);
    	}		
	}
 	 		
 	// Future date validation
 	  var fromDt = document.frmAccount.Txt_FromDate.value;
 	 var toDt = document.frmAccount.Txt_ToDate.value;
 	 if(fromDt !="" || toDt !="")
 		 {
 		 	if(fromDt =="" || toDt =="")
 			{
 			Error_Details(message_accounts[306]);
 			}
 		 }
 	 CommonDateValidation(document.frmAccount.Txt_FromDate,dateFmt,message_accounts[307]);
 	 CommonDateValidation(document.frmAccount.Txt_ToDate,dateFmt,message_accounts[308]);
 	 fromDtDiff = dateDiff(todaysDate,fromDt,dateFmt);
 	 toDtDiff = dateDiff(todaysDate,toDt,dateFmt);
 	 if (fromDtDiff > 0){
			Error_Details(message_accounts[309]);
	 }
 	if (toDtDiff > 0){
		Error_Details(message_accounts[311]);
     }
 	if (ErrorCount > 0)
	 {
			Error_Show();
			Error_Clear();
			return false;
	 }
 	else
 		{
 		document.frmAccount.hAccId.value = accId;
 		document.frmAccount.hType.value = strType;
 	 	document.frmAccount.hAction.value = "ACCDET";
 	 	document.getElementById('Btn_load').disabled = true;
 	 	fnStartProgress();
 		document.frmAccount.submit();	
 		}
 	
}

function fnOpenStatement()
{
	var repOpenType = '';
	var accCurrId = document.frmAccount.Cbo_Comp_Curr.value;
	var parentck = eval(document.frmAccount.Chk_ParentFl);
	if (parentck !=undefined){
	var parentFl  = document.frmAccount.Chk_ParentFl.checked;
	}else{
		parentFl = 'STMTBYGRP';//Statement by group report hyperlink
	}
	repOpenType = document.frmAccount.Cbo_Action1.value;
	if(repOpenType=='0'){
		Error_Details(message_accounts[312]);
    	Error_Show();
		Error_Clear();
		return false;
	}	
	else if(repOpenType=='DE'){//for Download to Excel 	
		fnExportExcel();
	}else {
		gridSortValue = gridObj.getSortingState();
		gridIndex= gridSortValue[0];
		gridSortOrder = gridSortValue[1];
		fromDate = document.frmAccount.Txt_FromDate.value;
		toDate = document.frmAccount.Txt_ToDate.value;
		strInvoiceID = document.frmAccount.Txt_InvoiceID.value;
		strCustomerPO = document.frmAccount.Txt_CustomerPO.value;
		if(strType == 'Account'){
			strAccGrpVal = document.frmAccount.Cbo_AccId.value;
		}else{
			strAccGrpVal = Cbo_AccId.getSelectedText();
		}
		strTotalAmtVal = document.frmAccount.hTotalAmtVal.value;
	    strTotalOverdue = document.frmAccount.hTotalOverdue.value; 
	    if(repOpenType=='SO'){
	    if(strTotalOverdue == '0.00')
	    	{
	    	Error_Details(message_accounts[313]);
	    	Error_Show();
			Error_Clear();
			return false;
	    	}
	    }
	   	
 		if(gridIndex == '0')
		{
			orderByField = 'B.C503_INVOICE_ID';
		}
		if(gridIndex == '1')
		{
			orderByField = 'B.C503_INVOICE_DATE';
		}
		if(strType == 'Group')
		{
			if(gridIndex == '4')
			{
				orderByField = 'B.C704_ACCOUNT_ID';
			}	
		}
		if(gridIndex == undefined)
		{
			orderByField = '';
			gridSortOrder = '';
		}
		var type = '';
		if(document.all.Cbo_InvSource){
			type = document.all.Cbo_InvSource.value;
		}
		if (type != 50253 && type != 26240213)
		{
			type = 50255
		}
			
		if(repOpenType == 'ST'){//for Statement Total
		windowOpener('/GmInvoiceServlet?Cbo_InvSource='+type+'&Cbo_Action=OPEN&hTotalAmtVal='+strTotalAmtVal+'&strAccGrpVal='+strAccGrpVal+'&Txt_CustomerPO='+strCustomerPO+'&Txt_InvoiceID='+strInvoiceID+'&Chk_GrpChk_Invoice_Type='+strInvoiceType+'&hAction=STMNT&orderByField='+orderByField+'&gridSortOrder='+gridSortOrder+'&Txt_FromDate='+fromDate+'&Chk_ParentFl='+parentFl+'&Txt_ToDate='+toDate+'&hType='+strType+'&Cbo_AccId='+strAccId+'&strRepOpenType='+repOpenType+'&hStatementType=ST&Cbo_Comp_Curr='+accCurrId,"Statment","resizable=yes,scrollbars=Yes,top=150,left=150,width=900,height=610");
		}
		else{
		windowOpener('/GmInvoiceServlet?Cbo_InvSource='+type+'&Cbo_Action=OPEN&hTotalOverdue='+strTotalOverdue+'&strAccGrpVal='+strAccGrpVal+'&Txt_CustomerPO='+strCustomerPO+'&Txt_InvoiceID='+strInvoiceID+'&Chk_GrpChk_Invoice_Type='+strInvoiceType+'&hAction=STMNT&orderByField='+orderByField+'&gridSortOrder='+gridSortOrder+'&Txt_FromDate='+fromDate+'&Chk_ParentFl='+parentFl+'&Txt_ToDate='+toDate+'&hType='+strType+'&Cbo_AccId='+strAccId+'&strRepOpenType='+repOpenType+'&hStatementType=SO&Cbo_Comp_Curr='+accCurrId,"Statment","resizable=yes,scrollbars=Yes,top=150,left=150,width=900,height=610");
		}
	}
	 
}

//For InvoiceStatementReport Jsp
function fnExportExcel(){	
	var tag_rowId = gridObj.getColIndexById("invAmtExcel");
	if(tag_rowId == 7)
	{
	gridObj.setColumnHidden(6,true); // Hide the negative value format in EXCEL (12.00)
	gridObj.setColumnHidden(8,true);
	gridObj.setColumnHidden(7,false);
	gridObj.setColumnHidden(9,false);
	gridObj.toExcel('/phpapp/excel/generate.php');
	gridObj.setColumnHidden(6,false);
	gridObj.setColumnHidden(8,true);
	gridObj.setColumnHidden(7,true);
	gridObj.setColumnHidden(9,false);
	}
else
	{
	gridObj.setColumnHidden(5,true);
	gridObj.setColumnHidden(7,true);
	gridObj.setColumnHidden(6,false);
	gridObj.setColumnHidden(8,false);
	gridObj.toExcel('/phpapp/excel/generate.php');
	gridObj.setColumnHidden(5,false);
	gridObj.setColumnHidden(7,false);
	gridObj.setColumnHidden(6,true);
	gridObj.setColumnHidden(8,true);
	}

		//Download to Excel functionality
}