var typeArr = ['40053', '40054','4117','4118','50150','50151','50152','50153','50155','50156','50157','50158','50159','50160','50161','50162','50154','100406'];


/* this method is called to include a js inside a js.
keeping this as a reference*/
function incJavascript(jsname) {
var th = document.getElementsByTagName('head')[0];
var s = document.createElement('script');
s.setAttribute('type','text/javascript');
s.setAttribute('src',jsname);
th.appendChild(s);
} 

incJavascript("/javascript/operations/receiving/GmNCMRInfoInclude.js");

//This prototype is provided by the Mozilla foundation and
//is distributed under the MIT license.
//http://www.ibiblio.org/pub/Linux/LICENSES/mit.license

if (!Array.prototype.indexOf)
{
  Array.prototype.indexOf = function(elt /*, from*/)
  {
    var len = this.length;

    var from = Number(arguments[1]) || 0;
    from = (from < 0)
         ? Math.ceil(from)
         : Math.floor(from);
    if (from < 0)
      from += len;

    for (; from < len; from++)
    {
      if (from in this &&
          this[from] === elt)
        return from;
    }
    return -1;
  };
}

function fnPicSlip(val,type,refid,conType)
{
	if (conType == 'C')
	{
		windowOpener("/GmConsignItemServlet?hAction=PicSlip&hConsignId="+val+"&ruleSource="+type,"SetPic","resizable=yes,scrollbars=yes,top=150,left=150,width=760,height=600");
	}
	else if (conType == 'I')
	{
		windowOpener("/GmConsignInHouseServlet?hAction=PicSlip&hConsignId="+val+"&ruleSource="+type+"&refId="+refid,"SetPic","resizable=yes,scrollbars=yes,top=150,left=150,width=750,height=500");	
	}else if (conType == 'T' && type == '103932'){
		windowOpener("/gmPartredesignation.do?&companyInfo="+companyInfoObj+"&method=generatePicSlip&strTxnIds="+val+"&txntype=103932&ruleSource=103932&refId="+val,"PTRTPicSlip","resizable=yes,scrollbars=yes,top=150,left=150,width=750,height=500");
	}else if (conType == 'T')
	{
		windowOpener("/gmItemControl.do?haction=PicSlip&hConsignId="+val+"&txntype="+type+"&ruleSource="+type+"&refId="+refid,"SetPic","resizable=yes,scrollbars=yes,top=150,left=150,width=750,height=500");	
	}else{
		// For Consigned Items Dashboard, the query will not have 'C' or 'I' so adding this to call the GmConsignItemServlet
		
		windowOpener("/GmConsignItemServlet?hAction=PicSlip&hConsignId="+val+"&ruleSource="+type,"SetPic","resizable=yes,scrollbars=yes,top=150,left=150,width=760,height=600");
	}
	 
}
function fnPackSlip(val)
{
	 windowOpener("/GmConsignSetServlet?hAction=PackVersion&hId="+val+"&strIntrasitPackSlip=INTRANSIT_PACK_SLIP","Con","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");            
}

function fnOpenOrdLogcsg(val)
{
	windowOpener("/GmCommonLogServlet?hType=1236&hID="+val,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
}
 
function fnCallEditItems(val,type,conType)
{
	var parentLcn;
	var urlStr;
	var targetUrlStr;

	document.frmOperDashBoardDispatch.hId.value = val;
	document.frmOperDashBoardDispatch.hMode.value = "CONTROL";
	document.frmOperDashBoardDispatch.hFrom.value = "ItemDashboard";
	document.frmOperDashBoardDispatch.hAction.value = "EditControl";
	document.frmOperDashBoardDispatch.hConsignId.value = val;
	document.frmOperDashBoardDispatch.hType.value = type;
	urlStr = "&hId="+val+"&hMode=CONTROL&hFrom=ItemDashboard&hAction=EditControl&hConsignId="+val+"&hType="+type+"&"+fnAppendCompanyInfo();
	if (conType == 'I')
	{
		document.frmOperDashBoardDispatch.action = "GmSetAddRemPartsServlet?";
		
	}else if(conType == 'T'){
		urlStr = "&txnid="+val+"&mode=CONTROL&haction=EditControl&hConsignId="+val+"&txntype="+type+"&loctype=93343"+"&"+fnAppendCompanyInfo();
		document.frmOperDashBoardDispatch.action = "/gmItemControl.do?";
	}
	else
	{
		document.frmOperDashBoardDispatch.action = "GmConsignItemServlet?";	
	}
	targetUrlStr = document.frmOperDashBoardDispatch.action+urlStr;
	parentLcn = parent.location.href;
	if(parentLcn.indexOf("GmEnterprisePortal") != -1)
	{
		location.href = targetUrlStr;
	}
	
	else if(parentLcn.indexOf("GmGlobusOnePortal")!= -1)
	{
		location.href = targetUrlStr;
	}
	
	else
	{
		parent.location.href = targetUrlStr;
	}
}

function fnCallItemsShip(val,type,conType)
{	 
	var parentLcn;
	document.frmOperDashBoardDispatch.hId.value = val;
	document.frmOperDashBoardDispatch.hFrom.value = "LoadItemShip";
	document.frmOperDashBoardDispatch.hConsignId.value = val;
	document.frmOperDashBoardDispatch.hAction.value = "LoadItemShip";
	document.frmOperDashBoardDispatch.hMode.value = "VERIFY";
	document.frmOperDashBoardDispatch.hType.value = type;
	document.frmOperDashBoardDispatch.hOpt.value = type;
	urlStr = "&hId="+val+"&hFrom=LoadItemShip&hAction=LoadItemShip&hConsignId="+val+"&hType="+type+"&hOpt="+type+"&"+fnAppendCompanyInfo();
	
	// If type is inhouse consignment, need to go to shipping screen, this list of transactions for this one might increase, once all the transactions are identified
	// remove the last else condition 
	if (type == "4112")
	{
		document.frmOperDashBoardDispatch.action = "/gmOPEditShipDetails.do?refId="+val+"&strOpt=modify&source=50181";
	}
	//if it's QNSC, need set TXN_TYPE_ID as 4115 to skip expiry date validation in rule
	else if(type == "4115")
	{
		document.frmOperDashBoardDispatch.action = "GmReprocessMaterialServlet?TXN_TYPE_ID=4115";
	}
	else if (conType == 'C')
	{
		document.frmOperDashBoardDispatch.action = "GmReprocessMaterialServlet?";
	}
	else if (conType == 'I')
	{
		document.frmOperDashBoardDispatch.action = "GmSetAddRemPartsServlet?";
		
	}else if (conType == 'T'){
		urlStr = "&txnid="+val+"&mode=VERIFY&haction=EditVerify&hConsignId="+val+"&txntype="+type+"&loctype=93345"+"&"+fnAppendCompanyInfo();
		document.frmOperDashBoardDispatch.action = "/gmItemVerify.do?";
	}
	else
     {                              
         document.frmOperDashBoardDispatch.action = "/gmOPEditShipDetails.do?refId="+val+"&strOpt=modify&source=50181";                
     }

	
	targetUrlStr = document.frmOperDashBoardDispatch.action+urlStr;
	parentLcn = parent.location.href;
	
	if(parentLcn.indexOf("GmEnterprisePortal") != -1)
	{
		location.href = targetUrlStr;
	}
	
	else if(parentLcn.indexOf("GmGlobusOnePortal")!= -1)
	{
		location.href = targetUrlStr;
	}
	
	else
	{
		parent.location.href = targetUrlStr;
	}
	
}
function fnCallControl(val, type)
{	 
	var parentLcn;
	var urlStr;
	var targetUrlStr;

	document.frmCustDashBoardDispatch.hId.value = val;
	document.frmCustDashBoardDispatch.hMode.value = "CONTROL"; 
	document.frmCustDashBoardDispatch.hAction.value = "EditControl";
	document.frmCustDashBoardDispatch.hConsignId.value = val; 
  
	urlStr = "&txnid="+val+"&mode=CONTROL&haction=EditControl&hConsignId="+val+"&txntype="+type+"&loctype=93343"+"&"+fnAppendCompanyInfo();
	document.frmCustDashBoardDispatch.action = "/gmItemControl.do?";
	 
	targetUrlStr = document.frmCustDashBoardDispatch.action+urlStr;
	parentLcn = parent.location.href;
	if(parentLcn.indexOf("GmEnterprisePortal") != -1)
	{
		location.href = targetUrlStr;
	}
	
	else if(parentLcn.indexOf("GmGlobusOnePortal")!= -1)
	{
		location.href = targetUrlStr;
	}
	
	else
	{
		parent.location.href = targetUrlStr;
	}
}

function fnCallShip(val)
{
	document.frmCustDashBoardDispatch.hOrdId.value = val;
	document.frmCustDashBoardDispatch.hAction.value = "EditShip";
	document.frmCustDashBoardDispatch.action = "/GmOrderItemServlet";
	document.frmCustDashBoardDispatch.submit();
}
function fnPrintPick(val)
{

	windowOpener('/GmOrderItemServlet?hAction=PICSlip&hId='+val,"Pack","resizable=yes,scrollbars=yes,top=150,left=150,width=760,height=610");	
}

function fnViewOrder(val)
{
	windowOpener('/GmEditOrderServlet?hMode=PrintPrice&hOrdId='+val,"Summ","resizable=yes,scrollbars=yes,top=250,left=250,width=760,height=410");	
}


function fnCallSetShip(val)
{
	document.frmCustDashBoardDispatch.hId.value = val;
	document.frmCustDashBoardDispatch.hAction.value = "LoadSetShip";
	document.frmCustDashBoardDispatch.action = "/GmConsignSetServlet";
	document.frmCustDashBoardDispatch.submit();	
}
function fnCallRecon(val)
{	
	 parent.location = "/GmLoanerReconServlet?hId="+val+"&hAction=LoanRecon";
}

function fnInHousePicSlip(val)
{
	windowOpener("/GmConsignInHouseServlet?hAction=PicSlip&hConsignId="+val,"SetPic","resizable=yes,scrollbars=yes,top=150,left=150,width=750,height=500");
}

function fnCallEditTransfer(val,tsftype)
{
	document.frmCustDashBoardDispatch.hTransferId.value = val;
	document.frmCustDashBoardDispatch.hAction.value="Load";
	document.frmCustDashBoardDispatch.hTransferType.value = tsftype;
	document.frmCustDashBoardDispatch.action = "/GmAcceptTransferServlet";	
	document.frmCustDashBoardDispatch.submit();	
}
function fnCallEditDummyConsignment(val)
{
	document.frmCustDashBoardDispatch.hId.value = val;
	document.frmCustDashBoardDispatch.hAction.value="LoadSetDummy";
	document.frmCustDashBoardDispatch.action = "/GmConsignSetServlet";	
	document.frmCustDashBoardDispatch.submit();	
}
function fnUpdateDHR(id,val)
{
	parent.location = "/GmPOReceiveServlet?hAction=UpdateDHR&hFrom=DashboardDHR&hMode="+val+"&hId="+id+"&"+fnAppendCompanyInfo();
}


function fnOpenForm(varPatientId, varEventId)
{
	//alert(valFormID + " = " + valStudy  + " = " + valStudyPeriodKey + " = " + valPatientID );
	document.frmAdverseEvent.Cbo_Form.value = 14;
	document.frmAdverseEvent.Cbo_Study.value = document.frmAdverseEvent.studyListId.value;
//	document.frmAdverseEvent.hStPerId.value = valStudyPeriodKey;
	document.frmAdverseEvent.hStPerId.value = 97;
	document.frmAdverseEvent.Cbo_Period.value = 6309; // 6309 is N/A
	document.frmAdverseEvent.Cbo_Patient.value = varPatientId;
	document.frmAdverseEvent.hAction.value = "LoadQues";
	document.frmAdverseEvent.cbo_EventNo.value = varEventId;
	document.frmAdverseEvent.action = "/GmFormDataEntryServlet";
	document.frmAdverseEvent.submit();

}
function fnCallInv(po,id)
{
	document.frmClinicalDashBoardDispatch.hId.value = id;
	document.frmClinicalDashBoardDispatch.hPO.value = po;
	document.frmClinicalDashBoardDispatch.hMode.value = "INV";
	document.frmClinicalDashBoardDispatch.hPgToLoad.value = "GmInvoiceServlet";
	document.frmClinicalDashBoardDispatch.submit();
}
function Toggle(val)
{

	var obj = eval("document.all.div"+val);
	var trobj = eval("document.all.tr"+val);
	var tabobj = eval("document.all.tab"+val);

	if (obj.style.display == 'none')
	{
		obj.style.display = '';
		//trobj.className="ShadeRightTableCaptionBlue";
		//tabobj.style.background="#c6e6fe";
		//tabobj.style.background="#d7ecfd";
		//tabobj.style.background="#ecf6fe";
	}
	else
	{
		obj.style.display = 'none';
		//trobj.className="";
		//tabobj.style.background="#ffffff";
	}
}

function fnFormDetails(valFormID, valStudy, valStudyPeriodKey, valCboPeriod, valPatientID)
{
	document.frmClinicalDashBoardDispatch.action = "/GmFormDataEntryServlet";
	document.frmClinicalDashBoardDispatch.hAction.value="LoadQues";
	document.frmClinicalDashBoardDispatch.Cbo_Form.value = valFormID;
	document.frmClinicalDashBoardDispatch.Cbo_Study.value = valStudy;
	document.frmClinicalDashBoardDispatch.hStPerId.value = valStudyPeriodKey;
	document.frmClinicalDashBoardDispatch.Cbo_Period.value = valCboPeriod;
	document.frmClinicalDashBoardDispatch.Cbo_Patient.value = valPatientID;
	document.frmClinicalDashBoardDispatch.submit();
}
function fnSubmit1()
{

	
	//document.frmClinicalDashBoardDispatch.action = "/gmClinicalDashBoardDispatch.do?method=pendingForm";
	document.frmClinicalDashBoardDispatch.hAction.value="Load";

	 loadajaxpage('/gmClinicalDashBoardDispatch.do?method=pendingForms&studyListId='+document.frmClinicalDashBoardDispatch.studyListId.value+'&craId='+document.frmClinicalDashBoardDispatch.craId.value,'ajaxdivcontentarea');
}
function fnSubmit2()
{


	//document.frmClinicalDashBoardDispatch.action = "/gmClinicalDashBoardDispatch.do?method=pendingForm";
	document.frmClinicalDashBoardDispatch.hAction.value="Load";

	 loadajaxpage('/gmClinicalDashBoardDispatch.do?method=outOfWindow&studyListId='+document.frmClinicalDashBoardDispatch.studyListId.value+'&craId='+document.frmClinicalDashBoardDispatch.craId.value,'ajaxdivcontentarea');
}
function fnSubmit3()
{


	//document.frmClinicalDashBoardDispatch.action = "/gmClinicalDashBoardDispatch.do?method=pendingForm";
	document.frmClinicalDashBoardDispatch.hAction.value="Load";

	 loadajaxpage('/gmClinicalDashBoardDispatch.do?method=pendingVerification&studyListId='+document.frmClinicalDashBoardDispatch.studyListId.value+'&craId='+document.frmClinicalDashBoardDispatch.craId.value,'ajaxdivcontentarea');
}

function fnViewDetails(strReqId,strConId)
 {    
      windowOpener("/gmRequestMaster.do?strOpt=print&requestId="+strReqId+"&consignID="+strConId,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=920,height=600");
    }
function fnCallEditReturn(val)
{
	/*document.frmCustDashBoardDispatch.action = "GmReturnProcessServlet";
	document.frmCustDashBoardDispatch.hAction.value = 'Reload';
	document.frmCustDashBoardDispatch.hId.value = val;
	document.frmCustDashBoardDispatch.submit();
	*/
	//parent.location.href  ="/GmReturnProcessServlet?hAction=Reload&hId="+val;
	fnLocationHref("/GmReturnProcessServlet?hAction=Reload&hId="+val,parent);
}
function fnCallReturn(val)
{
/*	document.frmCustDashBoardDispatch.hRAId.value = val;
	document.frmCustDashBoardDispatch.hAction.value = "LoadCredit";
	document.frmCustDashBoardDispatch.action = "/GmReturnCreditServlet";
	document.frmCustDashBoardDispatch.submit();
	*/
	//parent.location.href  ="/GmReturnCreditServlet?hAction=LoadCredit&hRAId="+val;
	fnLocationHref("/GmReturnCreditServlet?hAction=LoadCredit&hRAId="+val,parent);
}
function fnViewReturns(val)
{
	windowOpener('/GmPrintCreditServlet?hAction=VIEW&hRAID='+val,"Returns","resizable=yes,scrollbars=yes,top=150,left=200,width=660,height=400");
}
function fnOpenTransferLog(ID){
	windowOpener("/GmCommonLogServlet?hType=1205&hID="+ID,"TransLog","resizable=yes,scrollbars=yes,top=300,left=300,width=690,height=300");
}

function fnLoad()
{
	document.frmAdverseEvent.strOpt.value="Dashboard";
	loadajaxpage('/gmRptAdverseEvent.do?method=reportAdverseEvent&strOpt=Dashboard&form='+frmAdverseEvent+'&studyListId='+document.frmAdverseEvent.studyListId.value+'&HIDDENSITELIST=dashboard','ajaxdivcontentarea');
}

//Function will call on page load. It will append companyInfo in each tab href and initiate ajaxtab.
function fnAjaxTabLoad(){
	fnAjaxTabAppendCmpInfo("#maintab");
	fnMainTab();
}

//initiate ajaxtab
function fnMainTab(){
	var maintab=new ddajaxtabs("maintab", "ajaxdivcontentarea");				
    maintab.setpersist(true);		
    maintab.init();		
    maintab.onajaxpageload=function(pageurl)
    {
    	
    	if (pageurl.indexOf("GmOperDashBoardServlet")!=-1)
    	{
    			fnLoad();
    	}
    }	
}
