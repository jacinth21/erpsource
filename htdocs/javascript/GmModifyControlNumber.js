
function fnStartParentProgress(){
	fnStartProgress();
}

function fnStopParentProgress(){
	fnStopProgress();
}

function fnCallGo(){	
	if(event.keyCode == 13)
	{	 
		var rtVal = fnGo();
		if(rtVal == false){
			if(!e) var e = window.event;

	        e.cancelBubble = true;
	        e.returnValue = false;

	        if (e.stopPropagation) {
	                e.stopPropagation();
	                e.preventDefault();
	        }
		}
	}	
}

function fnSetFocus()
{
	
	var txtRefId = document.frmModifyControlNumber.refId;
	
	if(txtRefId.value == '')
	{
		txtRefId.focus();
		txtRefId.select();
	}
	else
	{
		if(validateObject(window.frames.myFrame) && validateObject(window.frames.myFrame.document.all.scanId)){//validating for undefined
			
			document.getElementsByName("myFrame")[0].contentWindow.document.scanId.focus();//To set the focus on FG Bin Id, if the text box is available
		}
		
		// to get the form name.
		var iframeFormName = document.getElementsByName("myFrame")[0].contentWindow.document.getElementsByTagName("form")[0];   
		if(iframeFormName !=undefined && iframeFormName !=null){
			// to set the focus for the iframe (used to fire the enter key event in form).
			document.getElementsByName("myFrame")[0].contentWindow.focus();
			// form name is frmVendor then only get the hShowCam value.
			if(iframeFormName.name =='frmVendor'){
			    var showCam = document.getElementsByName("myFrame")[0].contentWindow.document.getElementById("hShowCam");
			    if (showCam != null && showCam.value)
			    {
					document.getElementsByName("myFrame")[0].contentWindow.document.frmVendor.comments.focus();
					var str = document.getElementsByName("myFrame")[0].contentWindow.document.frmVendor.comments.value;
					document.getElementsByName("myFrame")[0].contentWindow.document.frmVendor.comments.value = str;
					document.getElementsByName("myFrame")[0].contentWindow.document.frmVendor.comments.focus();
				}
			}
		}
	}
}

function fnGo(){
	fnValidateTxtFld('refId',lblRefId);
	var refsrc =  document.frmModifyControlNumber.refId.value.toUpperCase(); //PMT-30594 Converts the case
	document.frmModifyControlNumber.refId.value = refsrc;
	// Validating the part re-designation transcations
	if(refsrc.indexOf("PTRD")!==-1)
		{
		Error_Details(message_operations[759]);
		}
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}	
	if(refsrc.indexOf("$")>0){
		var arrRefsrc = refsrc.split("$");
		document.frmModifyControlNumber.refId.value =arrRefsrc[0];
	}
	fnStartProgress('Y');
	document.frmModifyControlNumber.submit();
	fnSetFocus();
}