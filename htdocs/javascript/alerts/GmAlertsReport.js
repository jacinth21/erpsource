
function fnReload(obj){
	var fromDtObj = document.frmAlerstReport.fromDate;
	var toDtObj = document.frmAlerstReport.toDate;
	var fromDt = fromDtObj.value;
	var toDt = toDtObj.value;
	var dateDiffs = dateDiff(toDt,fromDt,format);// To get From and To date difference
	var fromDtDiff = dateDiff(todaysdate,fromDt,format);// To get From and today's date difference
	var toDtDiff = dateDiff(todaysdate,toDt,format);// To get To and today's date difference
	
	//Check the date format
	CommonDateValidation(fromDtObj,format,'<b>From Date - </b> Please enter a valid date in '+format+' format');
	CommonDateValidation(toDtObj,format,'<b>To Date - </b> Please enter a valid date in '+format+' format');
	
	if(fromDt != '' && toDt == ''){
		Error_Details("Please select the To date range.");
	}else if(fromDt == '' && toDt != '' ){
		Error_Details("Please select the From date range.");
	}else if( fromDt!= '' && toDt != ''){
		if(dateDiffs>0){
			Error_Details("From Date should not be greater than To Date.");
		}
		document.frmAlerstReport.haction.value = ''; // If comes from header section
	}
		 
 	 if (fromDtDiff > 0){
			Error_Details("Do not enter Future Date in From Date. Please correct the Date.");
	 }
 	 if (toDtDiff > 0){
		Error_Details("Do not enter Future Date in To Date. Please correct the Date.");
     }
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	
	document.frmAlerstReport.strOpt.value = "Reload";
	document.frmAlerstReport.Btn_Load.disabled = "true";
	fnStartProgress();
	document.frmAlerstReport.submit();
	return false;
}

function initGridData(divRef,gridData)
{
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);
	gObj.enableTooltips("true,true,true,true,true,true");
	//gObj.attachHeader('#rspan,#rspan,#select_filter,#select_filter,#text_filter,#text_filter,#select_filter');
	var strFilter = "#rspan,#select_filter,#select_filter,#text_filter,#text_filter,#select_filter";
	gObj.attachHeader("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='checkbox' name='selectAll' onClick='javascript:fnChangedFilter(this);'/>,"+strFilter);

	// Row display per page getting value from rule table. If not there in rule table default value will assign.
	if(strRowDisp == undefined || strRowDisp == '')	strRowDisp = 40; 
	if(strPageNation == undefined || strPageNation == '') strPageNation = 5;

	gObj.enablePaging(true, strRowDisp, strPageNation, "pagingArea", true, "infoArea");
	gObj.setPagingSkin("bricks");
	gObj.init();	
	gObj.loadXMLString(gridData);	
	return gObj;	
}

function fnOnPageLoad() {
	var strOpt = document.frmAlerstReport.strOpt.value;
	if( document.frmAlerstReport.haction.value != 'Header'){ // When click unread in header, should not display current date.
		if(document.frmAlerstReport.fromDate.value == '' && strOpt == '')
			document.frmAlerstReport.fromDate.value = todaysdate;
		if(document.frmAlerstReport.toDate.value == '' && strOpt == '')
			document.frmAlerstReport.toDate.value = todaysdate;
	}
	if(gridObjData!=""){
	   	mygrid = initGridData('dataGridDiv',gridObjData);
	   	ChkboxAlertId = 0; // First Index column
	   	mygrid.attachEvent("onCheckbox", doOnCheck);
	}
}

// When click subject , it will be displayed alert details for particular alert in popup window.
function fnAlertDet(alertId){
	  w1 =  createDhtmlxWindow(520,300,true);
	  w1.setText("Alert Details"); 
	//  alert(alertId);
	  var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmAlertsReportAction.do?strOpt=AlertDet&alertID='+alertId);
	  w1.attachURL(ajaxUrl,"PRICE");
	  return false;
	  
}

// When select choose action Flag as Read/Unread and hit submit , it will be called this function and get alert IDs to DB.
function fnSubmit(obj){
	var strInputAlertIds = '';
	var AlertId = '';
	var checkedRowID = '';
	var CboAction = document.frmAlerstReport.readFlag.value;
	mygrid.forEachRow(function(rowId) {
		AlertId = mygrid.getUserData(rowId,"ALERT_ID");
		check_id = mygrid.cellById(rowId, ChkboxAlertId).getValue();
		if(check_id == 1){
			strInputAlertIds = strInputAlertIds + "," + AlertId;
		}
		checkedRowID = mygrid.getCheckedRows(ChkboxAlertId);
		
   });
	// When click submit and without select it will be check this condition.
	if(checkedRowID == '' && checkedRowID != undefined)
		Error_Details("Please select at least one alert to Proceed");
	
	// When select alert and without select drop down it will be check this condition.
	if(checkedRowID != '' && checkedRowID != undefined && CboAction == 0)
		fnValidateDropDn('readFlag',' Choose Action ');
	
	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}
	
    document.frmAlerstReport.strOpt.value = "Update";
	document.frmAlerstReport.strInputAlertIds.value = strInputAlertIds;
	fnStartProgress();
	document.frmAlerstReport.submit();
}

//Description : This function will be set check true in master check box
function fnChangedFilter(obj){
	if(obj.checked){		
		mygrid.forEachRow(function(rowId) {
			mygrid.cellById(rowId, ChkboxAlertId).setChecked(true);
	   });
	}else{
		mygrid.forEachRow(function(rowId) {
			check_id = mygrid.cellById(rowId, 0).getValue();
			if(check_id == 1){
				mygrid.cellById(rowId, 0).setChecked(false);
			}
	   });			
	}
}

//Description : This function to check the select all (master check box)
function doOnCheck(rowId, cellInd, stage) {
	rowID = rowId;
	if(cellInd == 0) {
		fnSelectAllToggle('selectAll');
	}
	return true;
}

//Description : This function to check the select all check box
function fnSelectAllToggle(varControl){
	var objControl = eval("document.frmAlerstReport."+varControl);
	var CheckFlag = false;
	mygrid.forEachRow(function(rowId) {
		check_id = mygrid.cellById(rowId, ChkboxAlertId).getValue();
		if(check_id == 0){
			CheckFlag = true;
		}
   });			
	
	if(CheckFlag){
		objControl.checked = false;// select all check box Un checked.
	}else{
		objControl.checked = true;// select all check box to be checked
	}
}
