//Declaration Variables
var arrValuefromClipboard = new Array();
var vendorId;
var part_rowId = "";
var price_rowId = "";


//Function OnpageLoad call
function fnOnPageLoad(){
	var setInitWidths = "*,175";
	var setColAlign = "left,left";
	var setColTypes = "ed,ed";
	var setColSorting = "str,str";
	var setHeader = ["Part#", "Price"];
	var setFilter = ["#text_filter", "#text_filter"];
	var setColIds = "pnum,price";
	var enableTooltips = "true,true";
	
	var gridHeight = "";
	var footerStyles = [];
	var footerExportFL = true;
	pagination = "";
	format = ""
	var formatDecimal = "";
	var formatColumnIndex = "";
	
	document.getElementById("trDiv").style.display = "table-row";
	document.getElementById("trImageShow").style.display = "table-row";
	document.getElementById("buttonshow").style.display = "table-row";
	
	//split the functions to avoid multiple parameters passing in single function
	gridObj = initGridObject('dataGridDiv');
	gridObj = fnCustomTypes(gridObj, setHeader, setColIds, setInitWidths,
			setColAlign, setColTypes, setColSorting, enableTooltips, setFilter,
			400, pagination);
  
	gridObj.copyBlockToClipboard();
    gridObj.enableMultiline(false);	
	gridObj.enableEditEvents(false, true, true);

	var strDtlsJson = "";
	gridObj = loadDhtmlxGridByJsonData(gridObj,strDtlsJson);
  
	gridObj.attachEvent("onKeyPress", onKeyPressed);
	gridObj.attachEvent("onEditCell",doOnCellEdit);
    gridObj.enableMultiselect(true);
	gridObj.enableBlockSelection(true);
	
	part_rowId = gridObj.getColIndexById("pnum");
	price_rowId = gridObj.getColIndexById("price");
	
	//Add Five rows when Onpagelaod
	if (strDtlsJson == "") {
		fnAddRows();
		fnAddRows();
		fnAddRows();
		fnAddRows();
		fnAddRows();
	} 
	
}

//To add rows into grid.
function fnAddRows() {
	gridObj.filterBy(0, "");// unfilters the grid while adding rows
	gridObj._f_rowsBuffer = null; // clears the cache
	var newRowId = gridObj.getUID();
	gridObj.addRow(newRowId, '');
}

//this function is used to perform action when a cell is editing
function doOnCellEdit(stage, rowId, cellInd, nValue, oValue) {
	// Highlight data when edit cell
	if (stage == 1 && (cellInd == part_rowId || cellInd == price_rowId)) {
		var c = this.editor.obj;
		if (c != null)
			c.select();
	}
	
	//when Part& Flag updated then reset the color				  
	if (stage == 2 && nValue != oValue && (cellInd == part_rowId || cellInd == price_rowId) && nValue != '') {
		gridObj.setCellTextStyle(rowId, cellInd,
		"color:none;border:0px");
	}

	return true;
}


//This Function Used to set custom types conditions in Grid
function fnCustomTypes(gObj, setHeader, setColIds, setInitWidths, setColAlign,
		setColTypes, setColSorting, enableTooltips, setFilter, gridHeight,
		pagination) {

	if (setHeader != undefined && setHeader.length > 0) {
		gObj.setHeader(setHeader);
		var colCount = setHeader.length;
	}

	if (setInitWidths != "")
		gObj.setInitWidths(setInitWidths);

	if (setColAlign != "")
		gObj.setColAlign(setColAlign);

	if (setColTypes != "")
		gObj.setColTypes(setColTypes);

	if (setColSorting != "")
		gObj.setColSorting(setColSorting);

	if (enableTooltips != "")
		gObj.enableTooltips(enableTooltips);

	if (setFilter != "")
		gObj.attachHeader(setFilter);

	if (setColIds != "") {
		gObj.setColumnIds(setColIds);
	}

	if (gridHeight != "")
		gObj.enableAutoHeight(true, gridHeight, true);
	else
		gObj.enableAutoHeight(true, 400, true);

	if (pagination == "Y") {
		gObj.enablePaging(true, 100, 10, "pagingArea", true);
		gObj.setPagingSkin("bricks");
	}

	return gObj;
}


//this function used to copy the grid data
function onKeyPressed(code, ctrl) {
	if (code == 67 && ctrl) {
		if (!gridObj._selectionArea)
			return false;
		gridObj.setCSVDelimiter("\t");
		gridObj.copyBlockToClipboard();
		gridObj._HideSelection();
	}
	if (code == 86 && ctrl) {
		gridObj.setCSVDelimiter("\t")
		gridObj.pasteBlockFromClipboard();
		gridObj._HideSelection();
	}
	gridObj.refreshFilters();
	return true;
}


//this function used to remove the empty rows
function fnRemoveEmptyRow() {
	gridObj.forEachRow(function(row_id) {
		part_val = gridObj.cellById(row_id, part_rowId).getValue();
		if (part_val == '') {
			gridObj.deleteRow(row_id);
		}
	});
}


// this function used to paste data from excel
function fnAddRowFromClipboard() {
	var cbData = gridObj.fromClipBoard();
	if (cbData == null) {
		Error_Details("Please copy valid data from excel");
	}
	fnRemoveEmptyRow();
	var rowData = cbData.split("\n");
	var rowcolumnData = '';
	var arrLen = (rowData.length) - 1;
	if (arrLen > maxUploadData) {
	Error_Details(Error_Details_Trans(message_operations[808],maxUploadData));
	}
	if (ErrorCount > 0) {
		fnStopProgress();
		Error_Show();
		Error_Clear();
		return;
	}
	
	fnStartProgress('Y');
	var newRowData = '';
	setTimeout(function() {
		gridObj.startFastOperations();
		gridObj.filterBy(0, "");// unfilters the grid
		gridObj._f_rowsBuffer = null; // clears the cache
		for (var i = 0; i < arrLen; i++) {

			newRowData = rowData[i];
			newRowData = newRowData.replace(/\n|\r/g, "");
			var rowcolumnData = '';
			var row_id = gridObj.getUID();
			gridObj.setCSVDelimiter("\t");
			gridObj.setDelimiter("\t");
			gridObj.addRow(row_id, newRowData);
			gridObj.setRowAttribute(row_id, "row_added", "Y");
			gridObj.setRowAttribute(row_id, "row_modified", "Y");
			gridObj.setDelimiter(",");
		}
		
		gridObj.stopFastOperations();
		fnStopProgress();
		gridObj.filterByAll();
	}, 100);
}



//to copy the clip board data to grid
function pasteToGrid() {
	var pnumInd = gridObj.getColIndexById("pnum");
	var priceInd = gridObj.getColIndexById("price");
	if (gridObj._selectionArea != null) {
		var colIndex = gridObj._selectionArea.LeftTopCol;	

		if(colIndex!=undefined && colIndex != pnumInd && colIndex != priceInd){
			alert(message_operations[48]);
		}else{
			var area = gridObj._selectionArea
			var leftTopCol = area.LeftTopCol;
			var leftTopRow = area.LeftTopRow;
			var rightBottomCol = area.RightBottomCol;
			var rightBottomRow = area.RightBottomRow;
			for ( var i = leftTopRow; i <= rightBottomRow; i++) {
				var m = 0;			
				for ( var j = leftTopCol; j <= rightBottomCol; j++) {
					gridObj.cellByIndex(i, j).setValue(arrValuefromClipboard[m]);
					m++;
				}
			 }
		}
		gridObj._HideSelection();
		gridObj.refreshFilters();
	} else {
		alert(message_operations[49]);
	}
}

//Use the below function to copy data inside the screen.
function docopy() {
	gridObj.setCSVDelimiter("\t");
	gridObj.copyBlockToClipboard();
	arrValuefromClipboard = new Array();
	if (gridObj._selectionArea != null) {
		var area = gridObj._selectionArea
		var leftTopCol = area.LeftTopCol;
		var leftTopRow = area.LeftTopRow;
		var rightBottomCol = area.RightBottomCol;
		var rightBottomRow = area.RightBottomRow;
		for ( var i = leftTopRow; i <= rightBottomRow; i++) {
			for ( var j = leftTopCol; j <= rightBottomCol; j++) {
				setValuefromClipboard = gridObj.cellByIndex(i, j).getValue();
				arrValuefromClipboard.push(setValuefromClipboard);
			}
		}
	}
	gridObj._HideSelection();
}


//Use the below function to remove the rows from the grid.
function fnRemoveSetUploadRow() {

	var gridrows = gridObj.getSelectedRowId();
	//check selected status is null or undefined or null 
	if (gridrows == '' || gridrows == undefined || gridrows == null) {
		Error_Details(message_prodmgmnt[37]);
		if (ErrorCount > 0) {
			Error_Show();
			Error_Clear();
			return false;
		}
	} else {

		var gridrowsarr = gridrows.toString().split(",");
		
		for (var rowid = 0; rowid < gridrowsarr.length; rowid++) {
			gridrowid = gridrowsarr[rowid];
			gridObj.deleteRow(gridrowid);
		}

	}

}

//This function used to upload part and price number details
function fnSubmit() {
	
	var vendorId = document.frmStandardReport.strVendorId.value;
    if(vendorId == '' || vendorId == '0' || vendorId == undefined){
    	Error_Details("Please select the Vendor Name.");
		if (ErrorCount > 0) {
			Error_Show();
			Error_Clear();
			return false;
		}
    }
	// validation
	if (ErrorCount == 0)
		fnValidateAndCreateInputStr();
	
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return;
	} else if (confirm('Are you Sure, you want to Submit the data?')) {
		fnStartProgress('Y');
		var loader = dhtmlxAjax.postSync('/gmVendorStandardCost.do?method=saveStandardCostBulkUpload','&vendorId='+
	    document.frmStandardReport.strVendorId.value +'&inputString='+document.frmStandardReport.inputString.value+'&strPartNumbers='+document.frmStandardReport.strPartNumbers.value
		  +'&'+fnAppendCompanyInfo()+'&ramdomId=' + Math.random());
		  fnSubmitCallBack(loader);
		
	}
}

//function used to call back after submit access
function fnSubmitCallBack(loader){
	var vendorId = document.frmStandardReport.strVendorId.value;
	var vendorIdName = document.frmStandardReport.strVendorId.options[document.frmStandardReport.strVendorId.selectedIndex].text;
	fnStopProgress();
	
	var strpartValidateFl = '';
	var strInvaliPart = '';
	
	var partDuplicateErrFl = false;
	var response = loader.xmlDoc.responseText;
	
	//split as array
	var b = response;
	var temp = new Array();
	temp = b.split('##');
	strpartValidateFl = temp[0];
	strInvaliPart = temp[1];
	
	
	if(strpartValidateFl == "N" && strInvaliPart != ""){
				//
				var tempPartNumber = new Array();
				tempPartNumber = strInvaliPart.split(',');
				for(var i=0;i<=tempPartNumber.length;i++){
				gridObj.forEachRow(function(rowId) {
					// check row id set priority equal if both equal and also
					// partValidateFl  value is N
					// highlight the cell throw validation
					part_val = gridObj.cellById(rowId, part_rowId).getValue();
					if (part_val != '' && (part_val == tempPartNumber[i]) && strpartValidateFl == 'N' ) {
							gridObj.setCellTextStyle(rowId, part_rowId,
									"color:red;border:1px solid red;");
							partDuplicateErrFl = true;
					}

				});// 
				}
			
			// if flag value true set validations
			if (partDuplicateErrFl) {
				Error_Details('The highlighted <b>Part # (s)</b> does not exist or not mapped to this Vendor');
			}
			if (ErrorCount > 0) {
				Error_Show();
				Error_Clear();
				return false;
			}
	} else if(strpartValidateFl == "Y"){
		document.getElementById("progress").style.display="block";
		document.getElementById("msg").style.color="green";
		document.getElementById("msg").style.display="inline";		
		document.getElementById("msg").innerHTML= "Vendor cost successfully saved for "+vendorIdName;
	}
}


//this function used to validate duplicate entry (part) and set the input
//string values.
function fnValidateAndCreateInputStr() {

	var inputStr = "";
	var hPartInputStr = "";
	var dupPartids = '';
	var empty_qty = '';
	var error_qtyStr = '';
	var partNumberBlankFl = false;
		
	gridObj.forEachRow(function(row_id) {
		//
		part_val = gridObj.cellById(row_id, part_rowId).getValue();
		price_val = gridObj.cellById(row_id, price_rowId).getValue();
		//
		if (part_val != '') {
			if (hPartInputStr.indexOf(part_val + ",") == -1) {
				hPartInputStr += (part_val + ",");
					// Qty validation
					if (price_val == '') {
						empty_qty = empty_qty + "<br>" + part_val;
						gridObj.setCellTextStyle(row_id, price_rowId,
								"color:red;border:1px solid red;");
						gridObj.setRowColor(row_id, "ORANGE");
					} else {
					
						if (isNaN(price_val) || parseInt(price_val) <= 0) {
							error_qtyStr = error_qtyStr + "<br>" + part_val
									+ " - " + price_val + "";
							gridObj.setCellTextStyle(row_id, price_rowId,
									"color:red;border:1px solid red;");
						}
					}	
			

				inputStr = inputStr + part_val + '^' + price_val  + '|';
			} else {

				if (dupPartids.indexOf(part_val + ",") == -1) {
					dupPartids += (part_val + ",");

				}

				gridObj.setCellTextStyle(row_id, part_rowId,
						"color:red;border:1px solid red;");
			}
		} // end of part_val check
		else{
			
			if( price_val != ''){
				gridObj.setCellTextStyle(row_id, part_rowId,
				"color:red;border:1px solid red;");
				gridObj.setRowColor(row_id, "ORANGE");
				partNumberBlankFl = true;
			}
		}
	});
   
	if (empty_qty != '') {
		Error_Details(Error_Details_Trans(
				"Price cannot be empty. Please provide valid Price for the Part Number(s) -[0]", empty_qty));
	}

	// Error message for invalid Part Cost..
	if (error_qtyStr != '') {
		Error_Details(Error_Details_Trans(
				"Please enter valid and Positive numbers in Price for Part# -[0]",
				error_qtyStr));
	}

	// Error message for Duplicate Part Entries..
	if (dupPartids != '' && TRIM(dupPartids).length > 1) {
		dupPartids = dupPartids.substr(0, (TRIM(dupPartids).length - 1));
		error_msg = Error_Details_Trans(
				"Duplicate Part Number(s) Found - [0]", dupPartids);
		Error_Details(error_msg);
	}
   
	if (inputStr == "") {
		Error_Details("Please provide the Part # details ");
	} else {
		document.frmStandardReport.inputString.value =  inputStr;
		document.frmStandardReport.strPartNumbers.value = hPartInputStr;
	}

}

//clear field if vendor name changed
function fnChange(changedVendor){
	document.getElementById("progress").style.display="none";
	document.getElementById("msg").style.display="none";
	gridObj.refreshFilters();
	fnOnPageLoad();
}

