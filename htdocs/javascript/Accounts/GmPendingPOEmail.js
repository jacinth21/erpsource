function fnLoadReport(val)
{
	document.frmAccount.hType.value = val;
	document.frmAccount.submit();
}


function fnCallPO(val,parentOrderId)
{
	document.frmAccount.hOrdId.value = val;
	if(document.frmAccount.hOpt.value == 'PROFORMA'){
		fnStartProgress("Y");
		document.frmAccount.hAction.value = "ACKORD";
 	 	document.frmAccount.action = "/GmOrderItemServlet";
 	 	document.frmAccount.submit();
	}else{
		windowOpener('/GmOrderItemServlet?hAction=EditPO&hOrdId='+val+'&hParentOrdId='+parentOrderId,"POPrint","resizable=yes,scrollbars=yes,top=250,left=250,width=1010,height=610");
	}
}

function fnViewOrder(val, parentOrderID)
{
	windowOpener('/GmEditOrderServlet?hMode=PrintPrice&hOrdId='+val,"Pack","resizable=yes,scrollbars=yes,top=250,left=250,width=960,height=650");	
}

function Toggle(val)
{
	
	var obj = eval("document.all.div"+val);
	var trobj = eval("document.all.tr"+val);
	var tabobj = eval("document.all.tab"+val);

	if (obj.style.display == 'none')
	{
		obj.style.display = '';
		trobj.className="ShadeRightTableCaptionBlue";
		//tabobj.style.background="#c6e6fe";
		//tabobj.style.background="#d7ecfd";
		tabobj.style.background="#ecf6fe";
	}
	else
	{
		obj.style.display = 'none';
		trobj.className="";
		tabobj.style.background="#ffffff";
	}
}

function fnSendMail()
{
	var frmObj = document.frmAccount.Chk_Id;
	var emailstr = '';
	var ordstr = '';
	var len = frmObj.length;
	var prev = '';
	var curr= '';
	var cnt = 0;
	var ord = '';
	var id = '';
	
	if (len>1)
	{
		for (i=0;i<len;i++)
		{
			if (frmObj[i].checked == true)
			{
				ord = frmObj[i].value;
				id = frmObj[i].id;
				emailstr = emailstr + ord + '^';
			}
		}
		document.frmAccount.hInputStr.value = emailstr.substr(0,emailstr.length-1);
	}else{
		document.frmAccount.hInputStr.value = frmObj.value;
	}

	document.frmAccount.hAction.value = 'SendMail';
	document.frmAccount.submit();	
}

function fnCallType(val)
{
	document.frmAccount.hAction.value = 'Reload';
	if (val !='0') {
	document.frmAccount.submit();
	}
}
function fnLoad()
{
	document.frmAccount.Cbo_Type.value = document.frmAccount.hType.value; 
	
// For Email Remainder screen the Parent Account Type is not applicable and if its
// available in the cookie then the value will be set as "By Rep".
	if(bolEmailFl == 'true' &&  document.frmAccount.hReportType.value =='103286'){
		document.frmAccount.hReportType.value = '103284';	
	}
	
	document.frmAccount.Cbo_ReportType.value = document.frmAccount.hReportType.value;
	fnLoadMasterInfo();
	if(masterSelID != '' && document.all.Cbo_Master){
		document.all.Cbo_Master.value = masterSelID;
	}	
//In Email Remainder Screen Parent Account should be removed.
	if( bolEmailFl == 'true'){
		
		var select=document.getElementById('Cbo_ReportType');
	
		for (i=0;i<select.length;  i++) {
		   if (select.options[i].value=='103286') {
		     select.remove(i);
		   }
		}
	}
	
	var mailAction ='0';
	
	if(document.all.Cbo_EmailAction){
		mailAction = document.all.Cbo_EmailAction.value; 
		if( mailAction == '0'){ //Defaulting to Weekly PO reminder email while loading
			document.all.Cbo_EmailAction.value = '103280';
		}
		
		
	}
	
}

function fnPrintPick(val,orderType){
	var vMode = (orderType == '101260') ? "ACKPrintPack" : "PrintPack";
	windowOpener('/GmEditOrderServlet?hMode='+vMode+'&hOrdId='+val,"Pack","resizable=yes,scrollbars=yes,top=150,left=150,width=760,height=610");	
}

function fnViewOrder(val)
{
	windowOpener('/GmEditOrderServlet?hMode=PrintPrice&hOrdId='+val,"Pack","resizable=yes,scrollbars=yes,top=250,left=250,width=960,height=650");	
}

function fnPrintCreditMemo(val)
{
	windowOpener('/GmReportCreditsServlet?hAction=PrintVersion&hOrderId='+val,"Credit","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnPrintCashAdjustMemo(val)
{
	windowOpener('/GmReportCreditsServlet?hAction=PrintCashAdj&hOrderId='+val,"Credit","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

//During page on-load this function will get called 
//For PO email screen this will populate the Group List Dropdown options.
function fnLoadMasterInfo(){
	var grpTyp = document.frmAccount.Cbo_ReportType.value;
	if( bolEmailFl == 'true'){
		if(document.all.Cbo_Master){
			masterobj = document.all.Cbo_Master;
			masterobj.options.length = 0;
			masterobj.options[0] = new Option("[Choose One]","0");
		}
		if(grpTyp == '103283'){  //Account
			for (var i=0;i<accArrLen;i++){
				arr = eval("AccArr"+i);	
				arrobj = arr.split(",");
				assocname = arrobj[1];
				associd = arrobj[0];
				masterobj.options[i+1] = new Option(assocname,associd);
			}
		}else if(grpTyp == '103284'){ //Sales rep
			for (var i=0;i<repArrLen;i++){
				arr = eval("RepArr"+i);	
				arrobj = arr.split(",");
				assocname = arrobj[1];
				associd = arrobj[0];
				masterobj.options[i+1] = new Option(assocname,associd);
			}
		}else if(grpTyp == '103285'){//Dist
			for (var i=0;i<distArrLen;i++){
				arr = eval("DistArr"+i);	
				arrobj = arr.split(",");
				assocname = arrobj[1];
				associd = arrobj[0];
				masterobj.options[i+1] = new Option(assocname,associd);
			}
		}	
	}	
}
function fnSubmitAction(){
	if( bolEmailFl == 'true'){
		var pgAction = document.frmAccount.Cbo_EmailAction.value;
		//var chkOrdType = document.frmAccount.Cbo_OType.value;
		
		var emailAction     = document.getElementById("Cbo_EmailAction");
		var emailActionNm = emailAction.options[emailAction.selectedIndex].text;	
		
		var orderType = document.frmAccount.hordertype.value;
		var chkOrdType     = document.getElementById("Cbo_OType");
		var chkOrdTypeNm = chkOrdType.options[chkOrdType.selectedIndex].text;	
		var orderTypeNm = '';		
		for(var i=0;i<=chkOrdType.options.length;i++){			
			if(chkOrdType.options[i].value == orderType){
				orderTypeNm = chkOrdType.options[i].text;
				break;
			}
		}

		var dueDays = document.frmAccount.Txt_DueDate.value;
		if(dueDays == ''){
			 Error_Details(message[10509]);
		}
		if((dueDays <= '0' && dueDays != '') || dueDays == '00'){
			Error_Details(message[10510]);
		}
		if(isNaN(dueDays)){
			Error_Details(message[10511]);
		}
	
		var inputStr='';
		var errMsg='';
		var arSelGrpArr =  new Array();
		if(pgAction == '0'){
			 Error_Details(message[10512]);
		}	
		if(pgAction != '103280' ){  
			fnValidateDate();			
			errMsg = message[10513];
			fnValidateParentSelection(arParentSelection, errMsg);
		}
		if(pgAction == '4000627' ){ 	//4000627 - Held Order Commission E-mail
			if(chkOrdType.value != '101783'){		//101783 - Held Order
				Error_Details(message[10514]);
			}else if(orderType != chkOrdType.value){
				var array = [orderTypeNm,chkOrdTypeNm]
				Error_Details(Error_Details_Trans(message[10515],array));
			}
		}else{
			if(chkOrdType.value == '101783'){
				Error_Details(Error_Details_Trans(message[10516],emailActionNm));
			}else if(orderType != chkOrdType.value){
				var array = [orderTypeNm,chkOrdTypeNm]
				Error_Details(Error_Details_Trans(message[10517],array));
			}
		}
		if (ErrorCount > 0)
		{
			Error_Show();
			Error_Clear();
			return false;
		}else{
			var accidarr = eval('document.frmAccount.hAccIdVal');
			var accidarrlen = accidarr.length;
			var accid = '';
			var blSelectedGrp = false;
			var blChldOrdFl = false;
			var blSubmitFl = true;
			var childOrdFl='';
			var backOrdStr = '';
			if (accidarrlen>1)
			{					
				for (i=0;i<accidarrlen;i++){
					accid =  accidarr[i].value;
					var frmObj = eval('document.frmAccount.Chk_Id'+accid);
					var acclen = frmObj.length;
					blSelectedGrp = false;
					if (acclen>1){
						for (j=0;j<acclen;j++){
							if(frmObj[j].checked){
								inputStr = inputStr + frmObj[j].value +'^';
								blSelectedGrp = true;
								childOrdFl =  eval('document.all.h' +frmObj[j].value.replace(/-/g,'')+'chldfl');
								if(childOrdFl.value=='Y'){
									blChldOrdFl = true;
									backOrdStr = backOrdStr + frmObj[j].value +',';
								}									
							}
						}					
					}else{
						if(frmObj.checked && frmObj){
							inputStr = inputStr + frmObj.value +'^';
							blSelectedGrp = true;
							childOrdFl =  eval('document.all.h' +frmObj.value.replace(/-/g,'')+'chldfl');
							if(childOrdFl.value=='Y'){
								blChldOrdFl = true;
								backOrdStr = backOrdStr + frmObj.value +',';
							}
						}
					}
					if(blSelectedGrp){
						arSelGrpArr.push(accid);
					}
				}
				inputStr = inputStr.substr(0,inputStr.length-1);
			}else{
				accid =  accidarr.value;
				var frmObj = eval('document.frmAccount.Chk_Id'+accid);
				var acclen = frmObj.length;
				blSelectedGrp = false;
				if (acclen>1){
					for (j=0;j<acclen;j++){
						if(frmObj[j].checked){
							inputStr = inputStr + frmObj[j].value +'^';
							blSelectedGrp = true;
							childOrdFl =  eval('document.all.h' +frmObj[j].value.replace(/-/g,'')+'chldfl');
							if(childOrdFl.value=='Y'){
								blChldOrdFl = true;
								backOrdStr = backOrdStr + frmObj[j].value +',';
							}
						}
					}					
				}else{
					if(frmObj.checked && frmObj){
						inputStr = inputStr + frmObj.value +'^';
						blSelectedGrp = true;
						childOrdFl =  eval('document.all.h' +frmObj.value.replace(/-/g,'')+'chldfl');
						if(childOrdFl.value=='Y'){
							blChldOrdFl = true;
							backOrdStr = backOrdStr + frmObj.value +',';
						}
					}
				}
				if(blSelectedGrp){
					arSelGrpArr.push(accid);
				}
				inputStr = inputStr.substr(0,inputStr.length-1);
			}
			if(pgAction == '103280'){
				document.frmAccount.hAction.value = "SendMail";
			}else{
				errMsg = message[10518];
				fnValidateParentSelection(arSelGrpArr, errMsg);   //This is to avoid Multiple parent selection  
				document.frmAccount.hAction.value = "commsn_email";
			}
			if(inputStr == ''){
				Error_Details(message[10519]);
			}
			if (ErrorCount > 0)
			{
					Error_Show();
					Error_Clear();
					return false;
			}
			backOrdStr = backOrdStr.substr(0,backOrdStr.length-1);
			//Below confirmation should fire if Back orders are selected before sending any kind of mail
			if(blChldOrdFl && backOrdStr ){
				if(confirm(message[10520]+backOrdStr)){
					blSubmitFl = true;
				}else{
					blSubmitFl = false;
				}
			}
			
			if(blSubmitFl){
				fnStartProgress("Y");
				document.frmAccount.hInputStr.value = inputStr;		
				document.frmAccount.submit();
			}
		}	
	}
}
function fnValidateDate(){
	 var toDate = document.frmAccount.Txt_ToDate.value;
	 if(toDate == '' || toDate == 'null'){
		 Error_Details(message[10521]);
	 }
	 var htoDate = document.frmAccount.hToDate.value;
	 if(toDate != '' && htoDate != toDate && ErrorCount == '0'){
		 var array = [htoDate,toDate]
		 Error_Details(Error_Details_Trans(message[10522],array));
	 }
}
function fnValidateParentSelection(arr, msg){
	 var parentSelLength = arr.length;
	 if(parentSelLength > 1){
		 Error_Details(msg);
	 }
}
function fnSelectOrders(accId){
	fnSetParentSelection(accId);
	fnSelectParentDO(accId);
}
//When master check box is selected acc./Rep./dist, adding the id into global array which will be used to validate during submit to avoid multiple parent selection.
function fnSetParentSelection(accId){	
	var parentStatus = eval('document.frmAccount.parentsel'+accId);	
	if(parentStatus.checked){
		arParentSelection.push(accId);
	}else{
		 for(var i=0; i<arParentSelection.length; i++) {
	        if(arParentSelection[i] == accId) {
	        	arParentSelection.splice(i, 1);
	            break;
	        }
		 }
	} 
}
//When master check box is selected acc./Rep./dist, below function is used to select the orders under the acc./Rep./dist.
function fnSelectParentDO(accId){
	var frmObj = eval('document.frmAccount.Chk_Id'+accId);
	var parentStatus = eval('document.frmAccount.parentsel'+accId);
	var len = frmObj.length;	
	var odrID = '';
	var childOrdFl = '';
	if(parentStatus.checked){
		if(frmObj.value){
			childOrdFl =  eval('document.all.h' +frmObj.value.replace(/-/g,'')+'chldfl');
			if(childOrdFl.value=='N'){
				frmObj.checked = true;
			}
		}else if(len>1){
			for (i=0;i<len;i++)	{
				childOrdFl =  eval('document.all.h' +frmObj[i].value.replace(/-/g,'')+'chldfl');
				if(childOrdFl.value=='N'){
					frmObj[i].checked = true;
				}
			}
		}
	}else{
		if(frmObj.value){
			frmObj.checked = false;
		}else if(len>1){
			for (i=0;i<len;i++)	{
				frmObj[i].checked = false;
			}
		}
	}	
}
function fnARcurrency(){
	
	var accId = document.all.Cbo_Master.value;
	if (accId != '0' && accId != null){
		fnSetAccCurrSym(accId,document.frmAccount.Cbo_Comp_Curr);
	}else{
		fnResetAccCurrSymb();
	}
	
}
function fnResetAccCurrSymb(){
	var accCurrObj = document.frmAccount.Cbo_Comp_Curr;
	for ( var i = 0; i < accCurrObj.options.length; i++ ) {
        if ( accCurrObj.options[i].id == compCurrSign ) {
        	accCurrObj.options[i].selected = true;
            return;
        }
    }
}

function fnSetAccCurrSym(accID,targetObj){
	   var dhxAccCurrUrl = fnAjaxURLAppendCmpInfo('/GmOrderAjaxServlet?opt=GETACCTCURR&acc='+accID+'&ramdomId=' + Math.random());
    dhtmlxAjax.get(dhxAccCurrUrl,function(loader){
    	   var accCurrDtl;
    	   var response = loader.xmlDoc.responseText;    
              if((response != null || response != '') ){
            	  accCurrDtl =response.split("|");
            	//  currencyFmt = accCurrDtl[1];
            	  if(targetObj != undefined){
            		  targetObj.value =accCurrDtl[0];
            	  }
              }
       });
}
function fnViewDODocument(strOrdId){
	if(strOrdId !=''){
		  w4 =  createDhtmlxWindow(1000,400,true);
		  w4.setText("Upload DO Document Information");
		  var ajaxUrl = fnAjaxURLAppendCmpInfo("/gmDODocumentUpload.do?method=loadOrderUpload&strOpt=Load&haction=View&uploadTypeId=26240412&strOrderId="+strOrdId);
		  w4.attachURL(ajaxUrl,"Upload Files");	  
		  return false;	
}

}
