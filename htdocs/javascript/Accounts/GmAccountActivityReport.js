var tdinnner = "";

//This Function used load the multiple checkbox
function fnOnPageLoad() {

	var accSelect = document.frmAccountActivity.accSelected.value;
	var typeSelect = document.frmAccountActivity.typeSelected.value;
	if (accSelect != '') {
		var objgrp = document.frmAccountActivity.Chk_GrpChk_Account;
		fnCheckSelections(accSelect, objgrp);
	}
	if (typeSelect != '') {
		var objgrp = document.frmAccountActivity.Chk_GrpChk_Invoice_Type;
		fnCheckSelections(typeSelect, objgrp);
	}
	//Status id set as dynamic
	if(inv_status != ''){
		document.frmAccountActivity.status.value = inv_status;
	}
	if (objGridData != '') {
		gridObj = initGridData('acctivityRpt', objGridData);
		gridObj.enablePaging(true, 100, 10, "pagingArea", true);
		gridObj.setPagingSkin("bricks");
	}
}
// This Function used to Initiate the grid
function initGridData(divRef, gridData) {
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);
	gObj.enableTooltips("true,true,true,true,true,true,true,true,true");
	gObj.attachHeader('#text_filter,#select_filter,#text_filter,#select_filter,#text_filter,#select_filter,#text_filter,#text_filter,#text_filter,#text_filter');
	gObj.init();
	gObj.loadXMLString(gridData);
	return gObj;
}
// This Function used load the reports
function fnLoad() {

	fnDateValidation();
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	} else {
		fnStartProgress('Y');
		document.frmAccountActivity.btn_Load.disabled = true; //load time to disabled the button
		document.frmAccountActivity.strOpt.value = "Reload";
		document.frmAccountActivity.submit();
	}
}
// This Function will call the Print button
function fnPrintRpt() {
	fnDateValidation();
	var accSelect = document.frmAccountActivity.accSelected.value;
	var accountArray = accSelect.split(",");
	var accLength = accountArray.length;
	var lastAccount = accountArray[accLength - 1];
	var typeSelect = document.frmAccountActivity.typeSelected.value;
	var invSource = document.frmAccountActivity.invSource.value;
	var invStatus = document.frmAccountActivity.status.value;
	var fromDt = document.frmAccountActivity.fromDt.value;
	var toDt = document.frmAccountActivity.toDt.value;
	var m =0;

	
	if (accLength > 0) {
		for ( var j = 0; j < accLength; j++) {
			m++;
		}
	}
			
	/*if(invSource == '50253' || invSource == '50256'){ // ICT/ICS
		Error_Details("Cannot print account statement for filter type Inter-Company Transfer (ICT)/ Inter-Company Sales (ICS)?");		
	}*/

	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	} else {
		// Confirmation for multiple account selected 
		if(m>1){
			if (!confirm(message_accounts[92])){
				return false;
			}
		}
		
		windowOpener("/gmAccountActivity.do?strOpt=Print&invSource="
				+ invSource + "&status=" + invStatus + "&accSelected="
				+ accSelect + "&typeSelected=" + typeSelect + "&fromDt="
				+ fromDt + "&toDt=" + toDt, "Print",
				"resizable=yes,scrollbars=yes,top=150,left=150,width=850,height=750");
	}

}
// This Function used check the multiple check box
function fnCheckSelections(val, objgrp) {
	var valobj = val.split(",");
	var sval = '';
	var arrlen = objgrp.length;

	if (arrlen > 0) {
		for ( var j = 0; j < valobj.length; j++) {
			sval = valobj[j];
			for ( var i = 0; i < arrlen; i++) {
				if (objgrp[i].value == sval) {
					objgrp[i].checked = true;
					break;
				}
			}
		}
	} else {
		objgrp.checked = true;
	}
}
// This Function used get the all account details
function fnGetAccountList(val) {
	
	
	if(invsource == '26240213'){  //26240213:Company Dealer
		document.getElementById('td_nm_id').innerHTML='<fmtAccountActivityRpt:message key="LBL_DEALER" var="varAccounts"/>';
	}else{
		document.getElementById('td_nm_id').innerHTML='<fmtAccountActivityRpt:message key="LBL_ACCOUNTS" var="varAccounts"/>';

	}		
	document.frmAccountActivity.accSelected.value = '';
	document.frmAccountActivity.strOpt.value = 'Load';
	document.frmAccountActivity.submit();
}
// This Function used validate the date
function fnDateValidation() {
	var fromDt = document.frmAccountActivity.fromDt.value;
	var toDt = document.frmAccountActivity.toDt.value;

	CommonDateValidation(document.frmAccountActivity.fromDt,date_format,message_accounts[93]+ message[611]);
	CommonDateValidation(document.frmAccountActivity.toDt,date_format,message_accounts[94]+ message[611]);
	
	var max_Date = dateDiff(fromDt, toDt, date_format);

	if (max_Date >= 366) { // 1 year 366 days.
		Error_Details(message_accounts[95]);
	}
	if (fromDt == '' || toDt == '') {
		Error_Details(message_accounts[96]);
	}
}
//This function used to print the reports
function fnPrint(){
	window.print();
}
// This Function used close the popup window
function fnClose() {
	window.close();
}


//This Function will hide the all button
function hidePrint() {
	strObject = eval("document.all.button");
	tdinnner = strObject.innerHTML;
	strObject.innerHTML = "";
}
//This Function will Show the all button after Print
function showPrint() {
	strObject = eval("document.all.button");
	strObject.innerHTML = tdinnner;
}
//This function will call the press the enter button.
function fnEnter(){
	if (event.keyCode == 13){ 
		fnLoad();
	}
}
// This function used to show the excel amount
function fnExport(){
	gridObj.detachHeader(1);
	// hidden the column, because we are show the negative values (9999)
	gridObj.setColumnHidden(6,true);
	gridObj.setColumnHidden(7,true);
	gridObj.setColumnHidden(8,true);
	// enable the column, its a number.
	gridObj.setColumnHidden(9,false);
	gridObj.setColumnHidden(10,false);
	gridObj.setColumnHidden(11,false);
	gridObj.toExcel('/phpapp/excel/generate.php');
	gridObj.attachHeader('#text_filter,#select_filter,#text_filter,#select_filter,#text_filter,#select_filter,#text_filter,#text_filter,#text_filter,#text_filter');
	gridObj.setColumnHidden(6,false);
	gridObj.setColumnHidden(7,false);
	gridObj.setColumnHidden(8,false);
	gridObj.setColumnHidden(9,true);
	gridObj.setColumnHidden(10,true);
	gridObj.setColumnHidden(11,true);
}

