/**
 * File Name: GmBatchReport.js Author:
 */
// This function used to initiate the grid
function fnOnPageLoad() {
	// the default focus - batch id
	document.frmBatchReport.batchID.focus();
	if(objGridData.indexOf("cell") !='-1'){
		gridObj = initGridData('batchReport', objGridData);
		gridObj.attachEvent("onFilterStart",fnCustomFilter);
		gridObj.enableTooltips("true,true,true,true,true,true,true");
		gridObj.attachHeader('#text_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter');
		gridObj.setSizes();
	}
}

// This function load the report
function fnLoad() {
	var batchNum = TRIM(document.frmBatchReport.batchID.value);
	var dtFromObj = document.frmBatchReport.batchFromDt;
	var dtToObj = document.frmBatchReport.batchToDt;
	var batch_Status = document.frmBatchReport.batchStatus.value;
	// date validation added
	CommonDateValidation(dtFromObj,dtFormat,message_accounts[1]+ message[611]);
	CommonDateValidation(dtToObj,dtFormat,message_accounts[2]+ message[611]);
	// validate the batch id
	NumberValidation(batchNum,message_accounts[3], 0);
	
	if(batchNum !=''){
		// remove the all the filter
		document.frmBatchReport.batchStatus.value = '0';
		document.frmBatchReport.batchType.value = '0';
		document.frmBatchReport.batchFromDt.value = '';
	}
	if(batch_Status =='18744' && batchNum ==''){ // Processed
		fnValidateTxtFld('batchFromDt',lblBatchInitFrmDt );
		fnValidateTxtFld('batchToDt', lblBatchInitToDt);
		// validate the date 30 days
		var max_Date = dateDiff(dtFromObj.value, dtToObj.value, dtFormat);

		if (max_Date >= 30) { // 30 days.
			Error_Details(message_accounts[4]);
		}
	}
	
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	} else{ 
		document.frmBatchReport.strOpt.value = 'Load';
		fnStartProgress();
		document.frmBatchReport.submit();
	}
}
// This fuction used to call the Batch based on the type.
function fnCallBatch(batchid, batchType, batchStatusVal) {
	if (batchType == '18751' || batchType == '18756') { // invoice batch
		document.frmBatchReport.action = '/gmInvBatchEdit.do?method=invBatchEditPOs&screenType=EditPOs&invBatchID='
				+ batchid +'&invBatchStatus='+ batchStatusVal+'&refType='+batchType; // 18751 Invoice Batch
	}else if(batchType == '18752') { // DO Summary
			document.frmBatchReport.action = '/gmInvBatchEdit.do?method=invBatchEditPOs&invBatchID='
					+ batchid +'&invBatchStatus='+ batchStatusVal+'&refType=18752'; // 18752 DO Summary Print
	}
	document.frmBatchReport.submit();
}
// This function used to down load the excel file
function fnDownloadXLS() {
	gridObj.toExcel('/phpapp/excel/generate.php');	
}
// This function used to only allow the number (to avoid the char)
function fnNumbersOnly(e){
	var unicode=e.charCode? e.charCode : e.keyCode
	if (unicode!=8){ //if the key isn't the backspace key (which we should allow)
	if (unicode<48||unicode>57) //if not a number
	return false //disable key press
	}
}

//this function used to filter the particular row
function fnCustomFilter(ord, data) {
	/*
	 * This custom filter to avoid hyper link content column filter issue.
	 * Because default filter consider all text with in the tag. Ex. Let us
	 * assume xml as <cell><a ..>text</a></cell>. If user try to file 'a'
	 * text then all rows will show.
	 */
	for (i = 0; i < data.length; i++) {
		if (data[i] != "" && data[i] != undefined
				&& ("" + data[i]).indexOf("anonymous") == -1) {
			var original = data[i];
			original = original.toLowerCase();
			data[i] = function(value) {
				if (value.toString().replace(/<[^>]*>/g, "").toLowerCase().indexOf(original) != -1)
					return true;
				return false;
			};
		}

	}
	gridObj.filterBy(ord, data);
	return false;
}
// this function is used to remove the link code and sort
function alink(a, b, order) {
	/* If hyperlink added then default sorting is not working in dhtmlx grid */
	a = a.replace(/<[^>]*>/g, "");
	b = b.replace(/<[^>]*>/g, "");
	a = parseInt(a);
	b = parseInt(b);
	if (order == "asc"){
		return a > b ? 1 : -1;
	}		
	else{
		return a < b ? 1 : -1;
	}
		
}
//This function used to press the enter key then submit the page.
function fnEnter() {
	if (event.keyCode == 13) {
		fnLoad();
	}
}
//Description : This function to load the grid data

function initGridData(divRef,gridData)
{
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.init();	
	gObj.loadXMLString(gridData);	
	return gObj;	
}
