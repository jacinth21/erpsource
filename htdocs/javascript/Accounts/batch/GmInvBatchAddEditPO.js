/**
 *  GmInvBatchAddEditPO.js
 */
var tempvarCurrSign ='';
var footerEnableFl= true;
var filter_values = '';
var filter_index = '';
var filter_fl = false;
//tax alert
var validShip_rowID = '';
var taxCountryFl_rowID = '';
var taxStartFl_rowID = '';
// this function used to on load initiate the grid
function fnOnPageLoad() {

	gridObj = initGridData('batchEditPO', objGridData);
	var batch_type = document.frmInvBatchEdit.batchType.value;
	if(batch_type == '18751' || batch_type == '18756'){ // Invoice Batch
		gridObj.attachEvent("onCheckbox", doOnCheck);
		// declare global variable and set index value.
		accName_rowID = gridObj.getColIndexById("acc_name");
		chk_rowID = gridObj.getColIndexById("chk_box");
		img_rowID = gridObj.getColIndexById("img_id");
		custPO_rowID = gridObj.getColIndexById("cust_po");
		poDate_rowID = gridObj.getColIndexById("po_date");
		totalInv_rowID = gridObj.getColIndexById("total_inv");
		orderDate_rowID = gridObj.getColIndexById("order_date");
		status_rowID = gridObj.getColIndexById("status");
		dist_rowID = gridObj.getColIndexById("dist");
		chk_emailReq_rowID = gridObj.getColIndexById("chk_email_req");
		electVer_rowID = gridObj.getColIndexById("elect_ver");
		inv_rowID = gridObj.getColIndexById("inv_id");
		accID_rowID = gridObj.getColIndexById("acc_id");
		holdFl_rowID = gridObj.getColIndexById("hold_fl");
		batchDtlID_rowID = gridObj.getColIndexById("batch_detail_id");
		hidden_email_req_rowID = gridObj.getColIndexById("hidden_email_req");
		// tax
		validShip_rowID = gridObj.getColIndexById("hidden_add_valid");
		taxCountryFl_rowID = gridObj.getColIndexById("hidden_tax_country_fl");
		taxStartFl_rowID = gridObj.getColIndexById("hidden_tax_start_fl");
		gridObj.attachEvent("onFilterStart",fnCustomFilter);
		// enable the selection
		gridObj.enableMultiline(false);	
		
		gridObj.forEachRow(function(rowId) {
			var holdFlVal = gridObj.cellById(rowId, holdFl_rowID).getValue();
			if (holdFlVal == 'Y') {
				gridObj.cellById(rowId, chk_rowID).setDisabled(true);
			}
			else{
				
				gridObj.cellById(rowId, chk_rowID).setChecked(true);
			}
			var hemail_reqFl = gridObj.cellById(rowId, hidden_email_req_rowID).getValue();
			if (hemail_reqFl != 'Y') {
				gridObj.cellById(rowId, chk_emailReq_rowID).setDisabled(true);
			}
		});

		gridObj.enableTooltips("true,true,true,true,true,true,true,true,true,true,true,true");
			gridObj.attachHeader('#select_filter,'
					+ "<input type='checkbox' name='selectPOAll' onClick='javascript:fnChangedFilter(this);' checked/>,"
					+ '#rspan,#text_filter,#select_filter,#select_filter,#text_filter,#text_filter,#select_filter,#select_filter,#select_filter,'
					+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='checkbox' align='left' name='selectEmailReqFlagAll'  onClick='javascript:fnCheckEmailReq(this);'/>,"
					+ "#select_filter,#text_filter");
		if (screen_type == 'SavePOs') {
			gridObj.setColumnHidden(chk_rowID, true); // Check box
		}
		//To check the master Check box during on load.
		fnSelectAllToggle('selectEmailReqFlagAll'); 
		
		var batch_Status = document.frmInvBatchEdit.batchStatus.value;
		if (batch_Status >= 18743) { // 18743 - Processing In Progress
			gridObj.setColumnHidden(img_rowID, true); // Images
		}
		if(batch_Status == 18744){
			gridObj.setColumnHidden(chk_rowID, true); // Check box
		}
		if(batch_Status == 18741 || batch_Status == 18742 ){ //18741-In Progress, 18742-Pending Processing
			 fnDisplayFooter();
			if (footerEnableFl){
               gridObj.attachFooter(",,,,,<b>"+message_accounts[15]+":</b>,<div id='divTotal'>0</div>,,,,,,,",["width:200px;background-color:#DCDCDC;border:none;","width:35px;background-color:#DCDCDC;border:none;","width:40px;background-color:#DCDCDC;border:none;","width:100px;background-color:#DCDCDC;border:none;","width:100px;background-color:#DCDCDC;border:none;","width:65px;background-color:#DCDCDC;border:none;","width:100px;background-color:#DCDCDC;border:none;","width:100px;background-color:#DCDCDC;border:none;","width:100px;background-color:#DCDCDC;border:none;","width:72px;background-color:#DCDCDC;border:none;","width:72px;background-color:#DCDCDC;border:none;","width:72px;background-color:#DCDCDC;border:none;","width:72px;background-color:#DCDCDC;border:none;","width:72px;background-color:#DCDCDC;border:none;"]);
				
                fnCalculateTotal();	
			}
		}
	}else if(batch_type == '18752'){ // Do Summary Print
		gridObj.attachHeader('#select_filter,#text_filter,#text_filter,#select_filter,#text_filter,#select_filter,#select_filter,#select_filter,#text_filter');
	}
	
	// Voided POs to display the alert message to user
	if(errorPos !=''){
		Error_Details(message_accounts[534] + errorPos + message_accounts[535]);
		Error_Show();
		Error_Clear();
	}
}
//This function is used to show/hide the Grand total footer section if the account currency are same.
function fnDisplayFooter(){
	
	var curr_colID = gridObj.getColIndexById("acc_curr");
	 for (var i = 0; i < gridObj.getRowsNum(); i++) {
		  var varCurrSign = gridObj.cellById(i,curr_colID).getValue();
		  if ((tempvarCurrSign != varCurrSign)  && (i!=0)){
	  		footerEnableFl =false;
	  	}
	  	tempvarCurrSign = varCurrSign;
	  	
	 }
}
//Below function is used to calculate the Total invoice.
function fnCalculateTotal(){

	var total = 0;
	totalInv_colID = gridObj.getColIndexById("total_inv");
	shipCost_colID = gridObj.getColIndexById("ship_cost");
    for (var i = 0; i < gridObj.getRowsNum(); i++) {
    	var val = gridObj.cellById(i,totalInv_colID).getValue();
    	var shipCostval = gridObj.cellById(i,shipCost_colID).getValue();
        total += parseFloat(val) + parseFloat(shipCostval);
    }
    var divTotal = document.getElementById("divTotal");
    divTotal.innerHTML = "<b>"+formatCurrency(total,tempvarCurrSign)+"</b>";
}
//Below is used to format the currency values.
function formatCurrency(num) {
	
	num = num.toString();
	if(isNaN(num))
		num = "0";
	sign = (num == (num = Math.abs(num)));
	num = Math.floor(num*100+0.50000000001);
	cents = num%100;
	num = Math.floor(num/100).toString();
	if(cents<10)
		cents = "0" + cents;
	for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
		num = num.substring(0,num.length-(4*i+3))+','+num.substring(num.length-(4*i+3));
	if(sign){
		return currSign+'&nbsp;' + num+ '.' + cents;
	}else{
		return '<font color=red>'+currSign+'&nbsp;(' + num+ '.' + cents+')</font>'; //(((sign)?'':'-') +
	}
	
}


//Description : This function to load the grid data

function initGridData(divRef,gridData)
{
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.init();	
	gObj.loadXMLString(gridData);	
	return gObj;	
}

// This function used to open the A/R Dash board
function fnOpenPODetails(accID, custPO) {
	windowOpener("/GmAcctDashBoardServlet", "PODetails",
			"resizable=yes,scrollbars=yes,top=150,left=200,width=970,height=700");
}
// This function used to go back to Create Invoice Batch Screen.
function fnBack() {
	document.frmInvBatchEdit.action = '/gmInvBatchAdd.do?method=loadInvBatchAddAccount';
	document.frmInvBatchEdit.submit();
}
// This function used to Add the selected POs to Batch.
function fnSubmit() {
	var inputStr = '';
	var checked = '';
	var accID = '';
	var custPo = '';
	var emailFl = '';
	var acEmailFl= '';
	var emailInputStr = '';
	
	var accountIds = document.frmInvBatchEdit.strAccountIDs.value;
	var invBatch = document.frmInvBatchEdit.invBatchID.value;
	var batch_type = document.frmInvBatchEdit.batchType.value;
	// batch status get the hidden values from GmBatchInfoInclude.jsp
	var batch_status = document.frmInvBatchEdit.batchStatus.value;

	if(batch_status != 18741){ //18741 - In Progress
		Error_Details(message_accounts[6]);
	}
	gridObj.forEachRow(function(rowId) {
		checked = gridObj.cellById(rowId, chk_rowID).getValue();
		if (checked == '1') { // Checked
			accID = gridObj.cellById(rowId, accID_rowID).getValue();
			custPo = gridObj.cellById(rowId, custPO_rowID).getValue();
			emailFl = gridObj.cellById(rowId, chk_emailReq_rowID).getValue();
			acEmailFl = gridObj.cellById(rowId, hidden_email_req_rowID).getValue();
			emailFl = (emailFl == '1')? 'Y':(acEmailFl == 'Y')?'N':'';
			
			//As Emailed PDF to be added to the end of the batch,we are adding the non-email invoice first and appending email pdf at the end.
			//So When user are looking at the batch all PDF invoice will be listed at the end .
			if(emailFl == '')			
				inputStr += accID + '^' + custPo + '^|';
			else	
				emailInputStr += accID + '^' + custPo + '^' + emailFl +'|';		
		}
	});

	if (inputStr == '' && emailInputStr == '') {
		Error_Details(message_accounts[7]);
	}

	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	
	fnStartProgress("Y");
	document.frmInvBatchEdit.hRedirectURL.value = '/gmInvBatchEdit.do?method=invBatchAddPOs&strOpt=ADDPOS&screenType=AddPOs&inputString='+accountIds+'&invBatchID='+invBatch+'&refType='+batch_type;
	document.frmInvBatchEdit.strOpt.value = 'SaveBatch';
	document.frmInvBatchEdit.inputString.value = inputStr+emailInputStr;
	document.frmInvBatchEdit.submit();
}
// This function used to void the batch
function fnVoidBatch() {
	// batch id get the hidden values from GmBatchInfoInclude.jsp
	var batch = document.frmInvBatchEdit.batchID.value;
	var batch_type = document.frmInvBatchEdit.batchType.value;
	var batch_status = document.frmInvBatchEdit.batchStatus.value;

	if(batch_type =='' && batch_status ==''){
		Error_Details(Error_Details_Trans(message_accounts[8],batch));
	}
	
	if(batch_status == 18743){//Processing In Progress
		Error_Details(message_accounts[9]);
	}
	
	if(screen_type =='EditPOs' || screen_type =='AddPOs'){
		var alCheckFl = true;
		gridObj.forEachRow(function(rowId) {
			checked = gridObj.cellById(rowId, chk_rowID).getValue();
			holdFlVal = gridObj.cellById(rowId, holdFl_rowID).getValue();
			if (checked != '1' && holdFlVal !='Y') { // Checked
				alCheckFl = false;
			}
		});
		
		if(!alCheckFl){
			Error_Details(message_accounts[10]);
		}
	}
	
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	if (confirm(message_accounts[11])){
		document.frmInvBatchEdit.action = "/GmCommonCancelServlet";
		document.frmInvBatchEdit.hTxnId.value = batch;
		document.frmInvBatchEdit.hAction.value = "Load";
		document.frmInvBatchEdit.hCancelType.value = 'VBATCH';
		document.frmInvBatchEdit.hRedirectURL.value = "/gmBatchReport.do?method=loadBatchRpt&batchStatus="+batch_status+"&batchType="+batch_type;
		document.frmInvBatchEdit.hDisplayNm.value = "Process Batch Screen";
		document.frmInvBatchEdit.submit();
	}
}
// This function used to Check all the check box
function fnChangedFilter(obj) {
	var holdFlVal = '';
	var checkVal = '';
	var temp_val = '';
	if (obj.checked) {
		gridObj.forEachRow(function(rowId) {
			holdFlVal = gridObj.cellById(rowId, holdFl_rowID).getValue();
			if (holdFlVal != 'Y') { // Hold Flag
				if(filter_index ==3){
					temp_val = gridObj.cellById(rowId, filter_index).getValue();
					if(temp_val.indexOf(filter_values)!=-1){
						gridObj.cellById(rowId, chk_rowID).setChecked(true);
					}
				}else if(filter_values !=''){
					temp_val = gridObj.cellById(rowId, filter_index).getValue();
					if(temp_val == filter_values){
						gridObj.cellById(rowId, chk_rowID).setChecked(true);
					}
				}else{
					gridObj.cellById(rowId, chk_rowID).setChecked(true);
				}
			}
		});
	} else {
		gridObj.forEachRow(function(rowId) {
			checkVal = gridObj.cellById(rowId, chk_rowID).getValue();

			if (checkVal == 1) {
				gridObj.cellById(rowId, chk_rowID).setChecked(false);
			}

		});
	}
}

//This function used to Check all the check boxs for EmailReqFlag.
function fnCheckEmailReq(obj){
	var emailReqFlVal = '';
	var checkVal = '';
	var temp_val = '';
	var checkColumnID = chk_emailReq_rowID;
	var checkFlagVal = hidden_email_req_rowID;
	if (obj.checked) {
		gridObj.forEachRow(function(rowId) {
			emailReqFlVal = gridObj.cellById(rowId, checkFlagVal).getValue();
			if (emailReqFlVal == 'Y') { 
				if(filter_index ==3){
					temp_val = gridObj.cellById(rowId, filter_index).getValue();
					if(temp_val.indexOf(filter_values)!=-1){
						gridObj.cellById(rowId, checkColumnID).setChecked(true);
					}
				}else if(filter_values !=''){
					temp_val = gridObj.cellById(rowId, filter_index).getValue();
					if(temp_val == filter_values){
						gridObj.cellById(rowId, checkColumnID).setChecked(true);
					}
				}else{
					gridObj.cellById(rowId, checkColumnID).setChecked(true);
				}
			}
		});
	} else {
		gridObj.forEachRow(function(rowId) {
			checkVal = gridObj.cellById(rowId, checkColumnID).getValue();
			if (checkVal == 1) {
				gridObj.cellById(rowId, checkColumnID).setChecked(false);
			}
		});
	}
}
// Description : This function to check the select all (master check box)
function doOnCheck(rowId, cellInd, stage) {
	rowID = rowId;
	if (cellInd == chk_rowID) {
		fnSelectAllToggle('selectPOAll');
	}
	if (cellInd == chk_emailReq_rowID) {	
		chk_val = gridObj.cellById(rowId, cellInd).getValue();
		chk_val = (chk_val==1)?'true':'false';
		custPOVal = gridObj.cellById(rowId, custPO_rowID).getValue();
		accIDVal = gridObj.cellById(rowId, accID_rowID).getValue();
		
		fnSelectAllToggle('selectEmailReqFlagAll',chk_val,custPOVal,accIDVal);
	}
	return true;
}
// Description : This function to check the select all check box
function fnSelectAllToggle(varControl,chk_value,custPOVal,accIDVal) {
	var objControl = eval("document.frmInvBatchEdit." + varControl);

	var all_row = gridObj.getAllRowIds(",");
	var all_rowLen = all_row.length; // get the rows id length
	var checkRowID = chk_rowID;
	if(varControl == 'selectEmailReqFlagAll'){
		checkRowID = chk_emailReq_rowID;
	}
	var checked_row = gridObj.getCheckedRows(checkRowID);
	var holdFl_row = '';
	var holdFlVal = '';
	var finalval;

	if(varControl == 'selectPOAll'){
		gridObj.forEachRow(function(rowId) {
			holdFlVal = gridObj.cellById(rowId, holdFl_rowID).getValue();
			if (holdFlVal == 'Y') { // Hold Fl
				holdFl_row = holdFl_row + rowId + ',';
			}
		});
	}else if(varControl == 'selectEmailReqFlagAll'){		
		gridObj.forEachRow(function(rowId) {
			holdFlVal = gridObj.cellById(rowId, hidden_email_req_rowID).getValue();
			if (holdFlVal != 'Y') { // Hold Fl
				holdFl_row = holdFl_row + rowId + ',';
			}
			custPO = gridObj.cellById(rowId, custPO_rowID).getValue();
			accID = gridObj.cellById(rowId, accID_rowID).getValue();
			if(custPOVal){ 
				if(custPOVal == custPO && accIDVal == accID){
					gridObj.cellById(rowId, checkRowID).setChecked(chk_value);
				}
			}
		});
	}

	finalval = eval(checked_row.length) + eval(holdFl_row.length);
	if (all_rowLen == finalval) {
		objControl.checked = true;// select all check box to be checked
	} else {
		objControl.checked = false;// select all check box un checked.
	}
}
// This function used to show the invoice page
function fnViewInv(accid, poNum) {
	windowOpener("/GmInvoiceServlet?hPO=" + poNum + "&hAccId=" + accid
			+ "&hAction=INV&hOpt=VIEW", "ViewInv",
			"resizable=yes,scrollbars=yes,top=150,left=200,width=900,height=600");
}
// This function used to unlink the selected POs
function fnUnlink() {
	var inputStr = '';
	var checked = '';
	var accID = '';
	var custPo = '';
	// batch id and type get the hidden values from GmBatchInfoInclude.jsp 
	var batch_id = document.frmInvBatchEdit.batchID.value;
	var batch_type = document.frmInvBatchEdit.batchType.value;
	
	gridObj.forEachRow(function(rowId) {
		checked = gridObj.cellById(rowId, chk_rowID).getValue();
		if (checked == '1') { // Checked
			accID = gridObj.cellById(rowId, accID_rowID).getValue();
			custPo = gridObj.cellById(rowId, custPO_rowID).getValue();
			inputStr += batch_id + '^' + accID + '^' + custPo + '^'
					+ batch_type + '|';
		}
	});

	if (inputStr == '') {
		Error_Details(message_accounts[12]);
	}

	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}

	document.frmInvBatchEdit.action = "/GmCommonCancelServlet?hCancelReturnVal=true";
	document.frmInvBatchEdit.hTxnName.value = batch_id;
	document.frmInvBatchEdit.hTxnId.value = inputStr;
	document.frmInvBatchEdit.hAction.value = "Load";
	document.frmInvBatchEdit.hCancelType.value = 'UNLBAT';
	document.frmInvBatchEdit.hRedirectURL.value = "/gmInvBatchEdit.do?method=invBatchEditPOs&screenType=EditPOs&invBatchID="
			+ batch_id+'&refType='+batch_type;
	document.frmInvBatchEdit.hDisplayNm.value = "Invoice Edit Batch Screen";
	document.frmInvBatchEdit.submit();
}
// This function used to call the Process Batch
function fnProcessBatch() {
	// batch id get the hidden values from GmBatchInfoInclude.jsp
	var batch_id = document.frmInvBatchEdit.batchID.value;
	var inputStr = '';
	var alCheckFl = true;
	// tax
	var validShipFl = '';
	var taxCountryFl = '';
	var taxStartFl = '';
	var confirmStr = '';
	var accountName = '';
	gridObj.forEachRow(function(rowId) {
		checked = gridObj.cellById(rowId, chk_rowID).getValue();
		batchDtlID = gridObj.cellById(rowId, batchDtlID_rowID).getValue();
		emailFl = gridObj.cellById(rowId, chk_emailReq_rowID).getValue();
		acEmailFl = gridObj.cellById(rowId, hidden_email_req_rowID).getValue();
		emailFl = (emailFl == '1')? 'Y':(acEmailFl == 'Y')?'N':'';
		holdFlVal = gridObj.cellById(rowId, holdFl_rowID).getValue();
		if (checked != '1' && holdFlVal !='Y') { // Checked
			alCheckFl = false;
		}
		//tax
		accountName = gridObj.cellById(rowId, accName_rowID).getValue();
		validShipFl = gridObj.cellById(rowId, validShip_rowID).getValue();
		taxCountryFl = gridObj.cellById(rowId, taxCountryFl_rowID).getValue();
		taxStartFl = gridObj.cellById(rowId, taxStartFl_rowID).getValue();
		if(validShipFl !='Y' && taxCountryFl =='Y' && taxStartFl =='Y'){
			if(confirmStr.indexOf(accountName) ==-1){
				confirmStr = confirmStr + accountName +'<br>';
			}
		}
		inputStr += batchDtlID+'^'+emailFl+'|';
	});
	// check all the POs are selected.
	if(!alCheckFl){
		Error_Details(message_accounts[13]);
	}
	// batch status get the hidden values from GmBatchInfoInclude.jsp
	var batch_status = document.frmInvBatchEdit.batchStatus.value;
	
	if(batch_status == 18744){//Processing In Progress
		Error_Details(message_accounts[14]);
	}
	
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	fnStartProgress("Y");
	document.frmInvBatchEdit.inputString.value = inputStr;
	document.frmInvBatchEdit.strOpt.value = 'ProcessBatch';
	document.frmInvBatchEdit.action = "/gmInvBatchEdit.do?method=invBatchEditPOs";
	document.frmInvBatchEdit.submit();
}
//this function used to filter the particular row
function fnCustomFilter(ord, data) {
	/*
	 * This custom filter to avoid hyper link content column filter issue.
	 * Because default filter consider all text with in the tag. Ex. Let us
	 * assume xml as <cell><a ..>text</a></cell>. If user try to file 'a'
	 * text then all rows will show.
	 */
	filter_values = '';
	filter_index = '';
	filter_fl = false;
	for (i = 0; i < data.length; i++) {
		if (data[i] != "" && data[i] != undefined
				&& ("" + data[i]).indexOf("anonymous") == -1) {
			var original = data[i];
			filter_values = original;
			filter_index = i;
			filter_fl = true;
			original = original.toLowerCase();
			data[i] = function(value) {
				if (value.toString().replace(/<[^>]*>/g, "").toLowerCase().indexOf(original) != -1)
					return true;
				return false;
			};
		}

	}
	if(filter_index !='' && filter_index !=0){
		filter_index = filter_index +2;
	}
	if(!filter_fl){
		// uncheck the master check box values.
		var objControl = eval("document.frmInvBatchEdit.selectPOAll");
		objControl.checked = false;// select all check box un checked.
		gridObj.forEachRow(function(rowId) {
			checkVal = gridObj.cellById(rowId, chk_rowID).getValue();

			if (checkVal == 1) {
				gridObj.cellById(rowId, chk_rowID).setChecked(false);
			}

		});
	}
	gridObj.filterBy(ord, data);
	return false;
}
// Open the invoice print
function fnPrintInvoice(varinv)
{
windowOpener('/GmInvoiceInfoServlet?hAction=Print&hInv='+varinv,"PrntInv","resizable=yes,scrollbars=yes,top=150,left=200,width=820,height=600");
}