var calendarDivFl = '';
//this function is used for on page load 
function fnOnPageLoadRpt(){
	var invClosingDt = document.frmMonthlyInvoiceBatch.invoiceClosingDate.value 
		if(document.frmMonthlyInvoiceBatch.strOpt.value=="Reload"){
			
			objCheckCompanyArrLen = 0;
			objCheckTransTypeArr = document.frmMonthlyInvoiceBatch.checkSelectedTransType;
			
				if(objCheckTransTypeArr) 
				{
					
					objCheckTransTypeArrLen = objCheckTransTypeArr.length;
					
				
					for(i = 0; i < objCheckTransTypeArrLen; i++) 
					{	
						
						

						if(objCheckTransTypeArr[i].checked)
					    {		
						
							objCheckTransTypeArr[i].checked = true;
							
					    }
						else
						{
							objCheckTransTypeArr[i].checked = false;
						}
					}
						
					 
				}
		}
		else{
			
			 var objCheckTransTypeArr = document.frmMonthlyInvoiceBatch.checkSelectedTransType;
			 var objCheckTransTypeArrLen = objCheckTransTypeArr.length;
		
			  for(i = 0; i < objCheckTransTypeArrLen; i ++) 
				{	
				  if(objCheckTransTypeArr[i].value != '106182'){ //106182 opening balance
				  	objCheckTransTypeArr[i].checked = true;
				  }
				}
		
		}

		if((document.frmMonthlyInvoiceBatch.strOpt.value=="Reload")||(document.frmMonthlyInvoiceBatch.strOpt.value=="save")){ 
			
			gridObj = initGridData('dataGridDiv',objGridData);
			gridObj.attachEvent("onCheckbox", doOnCheck);
	
			// declare global variable and set index value.
			
			chk_rowID = gridObj.getColIndexById("chk_box");
			
			holdFl_rowID = gridObj.getColIndexById("hold_fl");
			transDate_rowID = gridObj.getColIndexById("trans_dt");
			// enable the selection
			gridObj.enableMultiline(false);	
			
			gridObj.forEachRow(function(rowId) {
				
				var holdFlVal = gridObj.cellById(rowId, holdFl_rowID).getValue();
				var surgeryDateVal = gridObj.cellById(rowId, transDate_rowID).getValue();
				// displaying the line item in different colour if the surgery date is less than issue from Date
				if(dateDiff(document.frmMonthlyInvoiceBatch.fromDate.value,surgeryDateVal, dtFormat) < 0){
					
					gridObj.setRowColor(rowId,"#F8E0E0");
				}
		
				if (holdFlVal == 'Y') {
					gridObj.cellById(rowId, chk_rowID).setDisabled(true);
				}
				else{
					gridObj.cellById(rowId, chk_rowID).setChecked(true);
				}
				
			});

			
			}
		
		var nameId=document.frmMonthlyInvoiceBatch.selectedNameList.value;
		if(!(nameId=='0')&&!(nameId=='')){
			
			document.frmMonthlyInvoiceBatch.invoiceClosingDate.disabled=true;
		}
		else{
			document.frmMonthlyInvoiceBatch.invoiceClosingDate.disabled=false;
		}
		if(incldtAcsFlg != 'Y' || invClosingDt == '0' || invClosingDt ==''){
			document.frmMonthlyInvoiceBatch.fromDate.disabled = true;
			document.frmMonthlyInvoiceBatch.Img_Date[0].disabled = true;
			}
			document.frmMonthlyInvoiceBatch.toDate.disabled = true;
			document.frmMonthlyInvoiceBatch.Img_Date[1].disabled = true;
		

 }
//this function is used to initiate grid data	
function initGridData(divRef,objGridData){
	
		var gObj = new dhtmlXGridObject(divRef);
		gObj.setSkin("dhx_skyblue");
		gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
		gObj.attachHeader('#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,'+ "<input type='checkbox' name='selectAllAccount'  onClick='javascript:fnChangedFilter(this);' checked/>,"+ '#rspan,#text_filter,#select_filter,#select_filter,#text_filter,#select_filter');
		gObj.init();
		gObj.loadXMLString(objGridData);	
		return gObj;
		

	}
//this function is used to set the invoice closing based on the dealer/account	
	function fnOnChange(){
		
			 var billingCustomer=document.frmMonthlyInvoiceBatch.billingCustomer.value;
			 var nameId=document.frmMonthlyInvoiceBatch.selectedNameList.value;
			
			 if(!(nameId=='0')&&!(nameId=='')){
				 document.getElementById('selectedNameId').value = document.frmMonthlyInvoiceBatch.selectedNameList.value;
				 var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmMonthlyInvoiceBatch.do?method=fetchInvCloseDate&selectedNameList='+nameId+'&randomId='+Math.random());
				 dhtmlxAjax.get(ajaxUrl,function(loader){
						var resJSONObj = $.parseJSON(loader.xmlDoc.responseText);
						document.frmMonthlyInvoiceBatch.invoiceClosingDate.value=resJSONObj["INVCLOSINGDATE"];
						document.frmMonthlyInvoiceBatch.invoiceClosingDate.disabled=true;
						fnOnClosingDateChange();
						
					});

				 if(billingCustomer == '0' || billingCustomer == ''){

					 var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmMonthlyInvoiceBatch.do?method=fetchBillCustType&selectedNameList='+nameId+'&randomId='+Math.random());
					 dhtmlxAjax.get(ajaxUrl,function(loader){
							var resJSONObj = $.parseJSON(loader.xmlDoc.responseText);
							document.frmMonthlyInvoiceBatch.billingCustomer.value=resJSONObj["INV_CUST_TYPE"];
						});

				 }
			 }
			 else{
				 document.getElementById('selectedNameId').value = "";
				 document.frmMonthlyInvoiceBatch.invoiceClosingDate.disabled=false;
			 }
		
	}
//this function is used to choose the dropdown based on dealer/accounts ID 
 function fntabOut(nameID){
	 var cnt=0;
	 if(nameID.value != ''){
		 
		 for(i= 0; i<=namelistLen;i++){

			 if(document.frmMonthlyInvoiceBatch.selectedNameList.options[i].value==nameID.value){
				 cnt++;
			 }

			}
		
		 if(cnt==0){
			
			 Error_Details(Error_Details_Trans(message_accounts[537],nameID.value));
			 }
		 else{
			 document.frmMonthlyInvoiceBatch.selectedNameList.value = nameID.value;
			 fnOnChange();
		 }
		 if (ErrorCount > 0)
			{
				Error_Show();
				Error_Clear();
				return false;
			}
		 }
	     	
	     
	
	 
	 
 }
//this function is used to submit the values	
 function fnLoad(){
	 
	fnValidateDropDn('invoiceClosingDate',lblClosingDate);
		
	var transType=fnCheckSelections();

	if(transType==''){
		 Error_Details(message_accounts[536]);	
	 }
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	document.frmMonthlyInvoiceBatch.invoiceClosingDate.disabled=false;
	document.frmMonthlyInvoiceBatch.fromDate.disabled = false;
	document.frmMonthlyInvoiceBatch.toDate.disabled = false;
	document.frmMonthlyInvoiceBatch.Img_Date[0].disabled = false;
	document.frmMonthlyInvoiceBatch.Img_Date[1].disabled = false;
	document.frmMonthlyInvoiceBatch.transactionType.value = transType;
 	document.frmMonthlyInvoiceBatch.strOpt.value = "Reload";
    document.frmMonthlyInvoiceBatch.haction = "/gmMonthlyInvoiceBatch.do?method=loadMonthlyInvoiceBatch";
	fnStartProgress();
	document.frmMonthlyInvoiceBatch.submit();

	
} 
//this function is used to select the from and to date based on the invoice closing date

 function fnOnClosingDateChange(selectedDate){
	 //if the date is not selected from the calendar then the below statement will execute to get the current date
	 if(selectedDate == undefined){
		 var date = new Date(), y = date.getFullYear(), m = date.getMonth();
	 }
	// To calculate the "to date" from the selected date based on the invoice closing date
	 else{
		 selectedDate = selectedDate.toString();
		 var pattern = /^(\d{1,2})\/(\d{1,2})\/(\d{4})$/;
		 var arrayDate = selectedDate.match(pattern);
		 var date = new Date(arrayDate[3], arrayDate[2], arrayDate[1]); 
		 var y = date.getFullYear(), m = date.getMonth();
		 
	 }
	
	 var fromdate = '';
	 var todate = ''; 
	 var firstDay = '';
	 var lastDay = '';
    
 	 var invCloseDt = document.frmMonthlyInvoiceBatch.invoiceClosingDate.value;

			if(invCloseDt=='106142'){
			  firstDay = new Date(y, m, 2);
			  lastDay = new Date(y, m + 1, 1);
				
			}else if(invCloseDt=='106140'){
			
			  firstDay = new Date(y, m-1, 21);
			  lastDay = new Date(y, m , 20);	
				
			}else if(invCloseDt=='106141'){
			
			  var currFirstDay = new Date(y, m, 1);
			  var currLastDay = new Date(y, m + 1, 0);
			  
			  var prevFirstDay = new Date(y, m-1, 1);
			  var prevLastDay = new Date(y, (m-1) + 1, 0);	
			
			  var currTimeDiff = Math.abs(currLastDay.getTime() - date.getTime());
			  var currDiffDays = Math.ceil(currTimeDiff / (1000 * 3600 * 24)); 
			 
			  var prevTimeDiff = Math.abs(prevLastDay.getTime() - date.getTime());
			  var prevDiffDays = Math.ceil(prevTimeDiff / (1000 * 3600 * 24)); 
			  
			  /* Logic for Invoice closing Date is 106141 (Month End),
			   * If users are generating the Invoice for 106141 (Month End) and if the last day of the month is weekend(Dec 31 2017,Sunday),
			   * They might generate the Invoice on the last business working day of the month (Dec 29 2017, Friday) then the FirstDay and LastDay 
			   * calculated based on current month (Dec 01 2017 to Dec 31 2017) else generating on Next Month(January 02 2018, Tues Day) then
			   * calculating the  FirstDay and LastDay  based Previous month.
			   * */
			  
			  if(prevDiffDays < currDiffDays)
			  {
				  firstDay = prevFirstDay;
				  lastDay = prevLastDay;	
				  
			  }else{
				  
				  firstDay = currFirstDay;
				  lastDay = currLastDay;	
			  }
			  
			}else {
				firstDay = '';
				lastDay = '';
			}
		
		fromdate = formattedDate(firstDay, dtFormat);
		todate = formattedDate(lastDay, dtFormat);
		calendarDivFl ='Y';
		document.frmMonthlyInvoiceBatch.fromDate.value=fromdate;
		document.frmMonthlyInvoiceBatch.toDate.value=todate;
		if(incldtAcsFlg == 'Y' && invCloseDt != '0'){
		document.frmMonthlyInvoiceBatch.fromDate.disabled = false;
		document.frmMonthlyInvoiceBatch.Img_Date[0].disabled = false;
		}
		else{
			document.frmMonthlyInvoiceBatch.fromDate.disabled = true;
			document.frmMonthlyInvoiceBatch.Img_Date[0].disabled = true;
		}
		document.frmMonthlyInvoiceBatch.toDate.disabled = true;
		document.frmMonthlyInvoiceBatch.Img_Date[1].disabled = true;
		mCal.hide();
	
 }
 
 
 /* fomattedDate() -Common method to get the date format based on the company 
  */
 function formattedDate(dt, dateformat) {
     try {
         if (dt != "" && dt != null && dt != undefined &&
             dateformat != "" && dateformat != null && dateformat != undefined) {
             var newdate = new Date(dt);
             var resultedDate = $.format.date(new Date(dt), dateformat);
             if (resultedDate != "" || resultedDate != null || resultedDate != undefined || resultedDate != "NaN/NaN/NaN") {
                 dt = resultedDate;
             }
         }
     } catch (err) {

     }
     return dt;
 }
 
 // This function used to check the xml node and node present then return the values
 function parseXmlNode(nodeObj){
 	var returnVal = '';
 	if(nodeObj != undefined){
 		returnVal = nodeObj.nodeValue;
 	}
 	if(returnVal == null || returnVal ==''){
 		return returnVal;
 	}else{
 		return returnVal;
 	}
 	  
 }
 
 function fnOnBillingCustomerChange(billingCustomer){

		var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmMonthlyInvoiceBatch.do?method=loadNameList&billingCustomer='+billingCustomer.value+'&randomId='+Math.random());
	
		dhtmlxAjax.get(ajaxUrl,function(loader){
			
			var resJSONObj = $.parseJSON(loader.xmlDoc.responseText);
			
			document.frmMonthlyInvoiceBatch.selectedNameList.options.length = 0;
			document.frmMonthlyInvoiceBatch.selectedNameList.options[0] = new Option("[Choose One]","0");
			namelistLen = resJSONObj.length;
				for(i= 0; i<resJSONObj.length;i++){
					itemJSON = resJSONObj[i];
				
					var id= itemJSON["ID"];
					var name= itemJSON["NAME"];
				
					document.frmMonthlyInvoiceBatch.selectedNameList.options[i+1] = new Option(name,id);
				
					
				}
			
	
		});
		document.frmMonthlyInvoiceBatch.invoiceClosingDate.disabled=false;
		document.getElementById('selectedNameId').value = "";
		
 }
//This function to check the select all (master check box)
 function doOnCheck(rowId, cellInd, stage) {
		rowID = rowId;
		if (cellInd == chk_rowID) {
			fnSelectAllToggle('selectAllAccount');
			
		}
		
		return true;
	}
//This function to check the select all check box
 function fnSelectAllToggle(varControl) {
		
 	var objControl = eval("document.frmMonthlyInvoiceBatch." + varControl);
 	var all_row = gridObj.getAllRowIds(",");
 	var all_rowLen = all_row.length; // get the rows id length
 	var checkRowID = chk_rowID;
 	var checked_row = gridObj.getCheckedRows(checkRowID);
 	var holdFl_row = '';
 	var holdFlVal = '';
 	var finalval;

 	if(varControl == 'selectAllAccount'){
 		gridObj.forEachRow(function(rowId) {
 			holdFlVal = gridObj.cellById(rowId, holdFl_rowID).getValue();
 			if (holdFlVal == 'Y') { // Hold Fl
 				holdFl_row = holdFl_row + rowId + ',';
 			}
 		});
 	}

 	finalval = eval(checked_row.length) + eval(holdFl_row.length);
 	if (all_rowLen == finalval) {
 		objControl.checked = true;// select all check box to be checked
 	} else {
 		objControl.checked = false;// select all check box un checked.
 	}
 }
//This function used to Check all the check box 
 function fnChangedFilter(obj) {
		
		var holdFlVal = '';
		var filter_values='';
		var checkVal = '';
		if (obj.checked) {
			gridObj.forEachRow(function(rowId) {
				holdFlVal = gridObj.cellById(rowId, holdFl_rowID).getValue();
				if (holdFlVal != 'Y') { // Hold Flag
					
						gridObj.cellById(rowId, chk_rowID).setChecked(true);
					}
			});
		} else {
			gridObj.forEachRow(function(rowId) {
				checkVal = gridObj.cellById(rowId, chk_rowID).getValue();

				if (checkVal == 1) {
					gridObj.cellById(rowId, chk_rowID).setChecked(false);
				}

			});
		}
	}
//this function is used to form the string for the transaction type based on checked and unchecked value
 function fnCheckSelections()
 {
	 var chkdvals=""; 

	 var inputString="";

	objCheckCompanyArrLen = 0;


	
	objCheckTransTypeArr = document.frmMonthlyInvoiceBatch.checkSelectedTransType;
	if(objCheckTransTypeArr) 
	{
		objCheckTransTypeArrLen = objCheckTransTypeArr.length;
	
		
		for(i = 0; i < objCheckTransTypeArrLen; i ++) 
		 {	
				if(objCheckTransTypeArr[i].checked)
			    {				    		   
					
					chkdvals += objCheckTransTypeArr[i].value+',';			
				
					inputString= chkdvals + ",";
				
					if(chkdvals !='')
					inputString=inputString.substring(0,inputString.length-2);
					
			    }			
				
		 }
			
		 
	}
	return inputString;
}

 function fnViewOrder(orderId)
 {	
		windowOpener('/GmEditOrderServlet?hMode=PrintPrice&hOrdId='+orderId,"Pack","resizable=yes,scrollbars=yes,top=250,left=250,width=965,height=450");	
 }
//This function used to Add the selected POs to Batch.
 function fnSubmit() {

 	var dealerinputStr = '';
 	var accinputStr = '';
 	var checked = '';
 	var billcust_type = '';
 	var dealerID = '';
 	var accID = '';
 	var custPo = '';
 	var emailFl = '';
 	var dealeremailInputStr = '';
 	var accmailInputStr = '';
 	var type_id = '';
 	var trans_id = '';
 	var parent_trans_id = '';
 	var dealerinputStrPO = '';
 	var accinputStrPO = '';
 	var inputStrPO = '';
 	var inputDealerInvStmt = '';
 	var inputAccountInvStmt = '';
 	var rowcnt=0;
 	var checkedcnt=0;
 	var invTypeCnt =0;
 	
 	
 	

 	var bill_cust_rowId = gridObj.getColIndexById("buil_cust_id");
 	var dealerID_rowID = gridObj.getColIndexById("dealer_id");
 	var accID_rowID = gridObj.getColIndexById("account_id");
 	var chk_rowID =  gridObj.getColIndexById("chk_box");
 	var typeID_rowID = gridObj.getColIndexById("type_id");
 	var emailfl_rowID = gridObj.getColIndexById("email_fl");
 	var transID_rowID = gridObj.getColIndexById("trans_id");
 	var parent_transid_rowID = gridObj.getColIndexById("parent_trans_id");
 	var cust_po_rowID = gridObj.getColIndexById("custpo");

 	
 	
 	
 	gridObj.forEachRow(function(rowId) {
 		rowcnt++;
 	
 	checked = gridObj.cellById(rowId, chk_rowID).getValue();
 
 	if (checked == '1') { // Checked
 		checkedcnt++;
 		
 			billcust_type = gridObj.cellById(rowId, bill_cust_rowId).getValue();
 			accID = gridObj.cellById(rowId, accID_rowID).getValue();
 			dealerID = gridObj.cellById(rowId, dealerID_rowID).getValue();
 			emailFl = gridObj.cellById(rowId, emailfl_rowID).getValue();
 			type_id = gridObj.cellById(rowId, typeID_rowID).getValue();
 			trans_id = gridObj.cellById(rowId, transID_rowID).getValue();
 			parent_trans_id = gridObj.cellById(rowId, parent_transid_rowID).getValue();
 			custPo = gridObj.cellById(rowId, cust_po_rowID).getValue();
 			
 		if(type_id != '2700' && type_id != '106182') { // 106182 open balance
 			
 			//106154 - dealer
 			if(billcust_type == '106154') {
 				
 				if(custPo == ''){
 					custPo = dealerID + '-' + fmtDate;
 				}
 				dealerinputStrPO += trans_id + '^' + custPo + '|';

 				
 				//As Emailed PDF to be added to the end of the batch,we are adding the non-email invoice first and appending email pdf at the end.
 	 			//So When user are looking at the batch all PDF invoice will be listed at the end .
 	 			if(emailFl == ''){
 	 				//remove the duplicate dealer id
 	 				if(dealerinputStr.indexOf(accID + '^' + custPo + '^|') == -1){
 	 					dealerinputStr += accID + '^' + custPo + '^|';
 	 				}
 	 			}else{	
 	 				if(dealeremailInputStr.indexOf(accID + '^' + custPo + '^' + emailFl +'|') == -1){
 	 					dealeremailInputStr += accID + '^' + custPo + '^' + emailFl +'|';		
 	 				}
 	 			}
 			}else if(billcust_type == '106155'){
 				
 				
 				
 				if(custPo == ''){
 					
 					custPo = accID + '-' + fmtDate;
 				}
 				
 				accinputStrPO += trans_id + '^' + custPo + '|';
 				//As Emailed PDF to be added to the end of the batch,we are adding the non-email invoice first and appending email pdf at the end.
 	 			//So When user are looking at the batch all PDF invoice will be listed at the end .
 	 			if(emailFl == ''){	
 	 				 //remove the duplicate account id
 	 				if(accinputStr.indexOf(accID + '^' + custPo + '^|') == -1){
 	 					accinputStr += accID + '^' + custPo + '^|';
 	 				}
 	 			}else{	
 	 				if(accmailInputStr.indexOf(accID + '^' + custPo + '^' + emailFl +'|') == -1){
 	 					accmailInputStr += accID + '^' + custPo + '^' + emailFl +'|';	
 	 				}
 	 			}
 				
 				
 			}
 			
 		}else if(type_id == '2700' || type_id == '106182' ){ // 2700 - Payemnt type
 			invTypeCnt++;
 			if(billcust_type == '106154') { // 106154 - dealer 
 				inputDealerInvStmt = inputDealerInvStmt + dealerID + '|';
 			}else if(billcust_type == '106155'){ // 106155 - direct account
 				inputAccountInvStmt = inputAccountInvStmt + accID + '|';
 			}
 		} 		
 	}
 });
	
 	inputStrPO = dealerinputStrPO + accinputStrPO;
 
 	dealerinputStr = dealerinputStr + dealeremailInputStr;
 	accinputStr = accinputStr + accmailInputStr;


 	if (dealerinputStr == '' && accinputStr == '' && invTypeCnt==0) { 
 		Error_Details(message_accounts[317]);
 	}
 	if (ErrorCount > 0) {
 		Error_Show();
 		Error_Clear();
 		return false;
 	}
 	if (checkedcnt != rowcnt) {
 		if(!confirm(message_accounts[538])){
 			return false;
 		}
 	}
 	 //this call is used generate invoice for opening balance and payments
 	if(dealerinputStr == ''  && accinputStr == '' && invTypeCnt > 0 ){
 		
		var fromDate=document.frmMonthlyInvoiceBatch.fromDate.value;
		var toDate=document.frmMonthlyInvoiceBatch.toDate.value;
		var closeDate= document.frmMonthlyInvoiceBatch.invoiceClosingDate.value;
		fnStartProgress("Y");
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmMonthlyInvoiceBatch.do?method=generateInvoiceStatement&dealerInputStr='+inputDealerInvStmt+'&accInputStr='+inputAccountInvStmt+'&invoiceClosingDate='+closeDate+'&fromDate='+fromDate+'&toDate='+toDate+'&billingCustomer='+billcust_type+'&randomId='+Math.random());
		 dhtmlxAjax.get(ajaxUrl,function(loader){
				var resJSONObj =loader.xmlDoc.responseText;
				document.getElementById('invStmtSuccessId').style.display='block';
				document.getElementById('dataGridDiv').style.display='none';
				document.getElementById('submitButtonId').style.display='none';
				fnStopProgress();
			});
		 
	}else{

		document.frmMonthlyInvoiceBatch.dealerInputStr.value = dealerinputStr;
	 	document.frmMonthlyInvoiceBatch.accInputStr.value = accinputStr;
	 	document.frmMonthlyInvoiceBatch.poInputStr.value = inputStrPO;
	 	document.frmMonthlyInvoiceBatch.invoiceClosingDate.disabled=false;
		document.frmMonthlyInvoiceBatch.fromDate.disabled = false;
		document.frmMonthlyInvoiceBatch.toDate.disabled = false;
		document.frmMonthlyInvoiceBatch.Img_Date[0].disabled = false;
		document.frmMonthlyInvoiceBatch.Img_Date[1].disabled = false;
	 	document.frmMonthlyInvoiceBatch.action = '/gmMonthlyInvoiceBatch.do?method=loadMonthlyInvoiceBatch&strOpt=save';
	 	fnStartProgress("Y");
	 	document.frmMonthlyInvoiceBatch.submit();
	}

 	
 	
 }
 //the below function is used to disable all the days in the calendar except some days 
 var tempDate = "";
 function showSingleCalendar(calendardiv,textboxref,dateformat){
 	dateformat = getObjTop().gCompJSDateFmt;
 	if (dateformat == null){
 		dateformat='%m/%d/%Y';
 	}
	if(calendarDivFl == 'Y'){
 		calendars[calendardiv] = undefined;
 		calendarDivFl = '';
 	}
 	if(!calendars[calendardiv]) 
 		calendars[calendardiv]= initCal(calendardiv,textboxref,dateformat);

 	//This line for today date will display in BOLD and this method will be in dhtmlxcalendar.js
 	calendars[calendardiv].enableTodayHighlight(true); // or false
 	
 	if(document.getElementById(textboxref).value.length>0 || tempDate !=''){
 		calendars[calendardiv].setDate(document.getElementById(textboxref).value);
 		if(tempDate !=''){
 			calendars[calendardiv].setDate(tempDate);
 		}
 	}
 	if(calendars[calendardiv].isVisible())
 		calendars[calendardiv].hide();
 	else 
 		calendars[calendardiv].show();
 }


 // The initCalendar method written for DHTMLX 3.5 new version 
 function initCal(calendardiv,textboxref,dateformat){
 	//Below code used for dhtmlx 3.5 new version. In old dhtmlx version we can't edit month and year. Now we can edit.
 	mCal = new dhtmlXCalendarObject(calendardiv);
 	var toDate = mCal.getDate(true);
 	// This range only able to select date. 
 	mCal.setSensitiveRange("1970-01-01", toDate); 

 	// Set date format based on login user.
 	mCal.setDateFormat(dateformat);

 	// Hide the time in new dhtmlx version 3.5
 	mCal.hideTime();
 	var invCloseDt = document.frmMonthlyInvoiceBatch.invoiceClosingDate.value;
			if(invCloseDt=='106140'){
	//disable all the dates except 20th day of all the months based on the invoice closing date
				mCal.disableDays("month", [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31]);
			 
			}else if(invCloseDt=='106141'){
	//disable all the dates except 1st day of all the months based on the invoice closing date
				mCal.disableDays("month", [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31]);
			}

 	
 	// Changed the below code for new version DHTMLX. When click calendar icon and set the date in textbox.
 	mCal.attachEvent("onClick", function(d) {
 		document.getElementById(textboxref).value =  mCal.getFormatedDate(dateformat, d);
 		tempDate = this.getFormatedDate(dateformat, d);
      //to calculate the “to date” field based on the “from date” field chosen by user
 		fnOnClosingDateChange(tempDate);
 		calendars[calendardiv].hide();
 		return true;
 	 });
 	
 	mCal.hide();
 	return mCal;
 }
//this function is used to uncheck opening balance when sales is checked and uncheck sales when opening balance is checked.
//checkSelectedTransType[0] - sales
//checkSelectedTransType[1] - payments
//checkSelectedTransType[2] - opening balance
 function fnChkTransType(obj){   
	 if(obj.value != '106181'){  // Payments
		 
		 if(obj.value == '106180'){  //Sales orders
			 if(document.frmMonthlyInvoiceBatch.checkSelectedTransType[0].checked == true){
				 document.frmMonthlyInvoiceBatch.checkSelectedTransType[2].checked = false;
			 }else{
				 document.frmMonthlyInvoiceBatch.checkSelectedTransType[2].checked = true;
			 }
			 
		 }else if(obj.value == '106182'){ // opening balance
			 if(document.frmMonthlyInvoiceBatch.checkSelectedTransType[2].checked == true){
				 document.frmMonthlyInvoiceBatch.checkSelectedTransType[0].checked = false;
			 }else{
				 document.frmMonthlyInvoiceBatch.checkSelectedTransType[0].checked = true;
			 }
			 
		 }
	}
 }
 