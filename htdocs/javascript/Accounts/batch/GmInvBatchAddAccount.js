/**
 * File Name: GmInvBatchAddAccount.js
 * Author : 
 */
var chk_rowId = '';
var accName_rowId = '';
var custPO_rowId = '';
var totalAmt_rowId = '';
var accID_rowId = '';
var holdFl_rowId = '';
var poCount_rowId = '';

// This function is used to on load initiate the Grid
function fnOnPageLoad() {
	if(objGridData.indexOf("cell") !='-1'){
		gridObj = initGridData('invBatchAddAccount', objGridData);
		gridObj.attachEvent("onFilterStart",fnCustomFilter);
		gridObj.enableMultiline(false);	
		
		// declare global variable and set index value.
		chk_rowId = gridObj.getColIndexById("chk_id");
		accName_rowId = gridObj.getColIndexById("acc_name");
		custPO_rowId = gridObj.getColIndexById("cust_po");
		totalAmt_rowId = gridObj.getColIndexById("total_inv");
		accID_rowId = gridObj.getColIndexById("acc_id");
		holdFl_rowId = gridObj.getColIndexById("holdFl");
		poCount_rowId = gridObj.getColIndexById("po_count");
        
		// to disable the Hold fl POs
		 gridObj.forEachRow(function(rowId) {
				var holdFlVal =   gridObj.cellById(rowId, holdFl_rowId).getValue();
				var totalPO = gridObj.cellById(rowId, poCount_rowId).getValue();
				if(totalPO == holdFlVal){
					gridObj.cellById(rowId, chk_rowId).setDisabled(true);
				}
			  });
	}
	
}

//Description : This function to load the grid data

function initGridData(divRef,gridData)
{
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
    gObj.enableTooltips("true,true,true,true,true");
    gObj.attachHeader('#rspan,#select_filter,#text_filter,#select_filter,#text_filter');
	gObj.init();	
	gObj.loadXMLString(gridData);	
	return gObj;	
}

// This function is used when click on the Customer PO to Open the AR Dashboard screen
function fnOpenPODetails(accID, custPO, accCurrId) {
	windowOpener("/GmAcctDashBoardServlet?screenType=INV_BATCH&accountID="
			+ accID +'&Cbo_Comp_Curr='+accCurrId, "PODetails",
			"resizable=yes,scrollbars=yes,top=150,left=200,width=970,height=700");
}
// This function is used to Go the next page
function fnNext() {
	var inputStr = '';
	var checkVal = '';
	var accVal = '';
	gridObj.forEachRow(function(rowId) {
		checkVal = gridObj.cellById(rowId, chk_rowId).getValue();
		accVal = gridObj.cellById(rowId, accID_rowId).getValue();

		if (checkVal == 1) {
			inputStr += accVal + ',';
		}
	});

	if (inputStr == '') {
		Error_Details(message_accounts[5]);
	}

	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	//var companyObj = document.frmInvBatch.companyInfo.value;	
	var strURL = fnAjaxURLAppendCmpInfo("/gmInvBatchEdit.do?method=invBatchAddPOs&strOpt=ADDPOS&screenType=AddPOs&inputString="+inputStr+"&refType=18751");
	    //document.frmInvBatch.action = "/gmInvBatchEdit.do?method=invBatchAddPOs&strOpt=ADDPOS&screenType=AddPOs&inputString="+inputStr+"&refType=18751&companyInfo="+companyObj; // 18751 Invoice Batch
	document.frmInvBatch.action = strURL;
	document.frmInvBatch.submit();
}
// this function used to filter the particular row
function fnCustomFilter(ord, data) {
	/*
	 * This custom filter to avoid hyper link content column filter issue.
	 * Because default filter consider all text with in the tag. Ex. Let us
	 * assume xml as <cell><a ..>text</a></cell>. If user try to file 'a'
	 * text then all rows will show.
	 */
	for (i = 0; i < data.length; i++) {
		if (data[i] != "" && data[i] != undefined
				&& ("" + data[i]).indexOf("anonymous") == -1) {
			var original = data[i];
			original = original.toLowerCase();
			data[i] = function(value) {
				if (value.toString().replace(/<[^>]*>/g, "").toLowerCase().indexOf(original) != -1)
					return true;
				return false;
			};
		}

	}
	gridObj.filterBy(ord, data);
	return false;
}
// this function is used to remove the link code and sort
function alink(a, b, order) {
	/* If hyperlink added then default sorting is not working in dhtmlx grid */
	a = a.replace(/<[^>]*>/g, "");
	b = b.replace(/<[^>]*>/g, "");
	a = parseInt(a);
	b = parseInt(b);
	if (order == "asc"){
		return a > b ? 1 : -1;
	}		
	else{
		return a < b ? 1 : -1;
	}
		
}
// this function used to reload the page based on customer currency selected
function fnLoad() {
	fnStartProgress('Y');
	document.frmInvBatch.submit();
}
