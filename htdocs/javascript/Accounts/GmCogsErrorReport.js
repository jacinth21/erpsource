var status_rowId = '';
var part_rowId = '';
var tran_rowId = '';
var fromCost_rowId = '';
var toCost_rowId = '';
var qty_rowId = '';
var company_rowId ='';
var country_rowId ='';
var errorTypeNm_rowId = '';
var errorDt_rowId = '';
var errorBy_rowId = '';
var fixDt_rowId = '';
var fixBy_rowId ='';
var erroTypeId_rowId = '';
var gridObj;
var gObj;
function showSingleCalendar(calendardiv,textboxref,dateformat){
	// Overite style for calendar only from class RightTableCaption.
	onSetStyleToCalendar(calendardiv);
	var topObj = getObjTop();
	dateformat = topObj.gCompJSDateFmt;
	var jsdatefmt=null;
	 try
		{
		jsdatefmt=self.opener.top.jsdatefmt;
		}catch(err) {
			jsdatefmt=null;
		}
	if (dateformat == null){
		if( jsdatefmt != null && jsdatefmt != '' && jsdatefmt != undefined){
			    dateformat=jsdatefmt;
			}else{
				dateformat='%m/%d/%Y';	}	
	}
	
	if(!calendars[calendardiv]) 
		calendars[calendardiv]= initCal(calendardiv,textboxref,dateformat);
	
	//This line for today date will display in BOLD and this method will be in dhtmlxcalendar.js
	//calendars[calendardiv].enableTodayHighlight(true); // or false
	
	if(document.getElementById(textboxref).value.length>0){
		calendars[calendardiv].setDate(document.getElementById(textboxref).value);
	}

	if(calendars[calendardiv].isVisible())
		calendars[calendardiv].hide();
	else 
		calendars[calendardiv].show();
}

function fnOnPageLoad(){
	document.frmCogsErrorForm.inputStr.value = '';
	var status = document.frmCogsErrorForm.status.value;
	if (objGridData != '')
	{
		fnChangeStatus();
		gridObj = myInitGridData('cogsdata',objGridData);
		gridObj.enableBlockSelection(true);
		
		
		//status_rowId = gridObj.getColIndexById("status");
		fromCost_rowId = gridObj.getColIndexById("fromCostId");
		erroTypeId_rowId = gridObj.getColIndexById("errorTypeId");
		partNum_rowId = gridObj.getColIndexById("partNum");
	}
	
}

function keyPressed(code, ctrl, shift) {
	// This code is to copy the selected cell from the grid.
    if (code == 67 && ctrl) {
    	gridObj.setCSVDelimiter("\t");
    	gridObj.copyBlockToClipboard();
    }
    return true;
}

function fnReload()
 {
	CommonDateValidation(document.frmCogsErrorForm.fromDate,date_fmt,message_accounts[111]+ message[611]);
	CommonDateValidation(document.frmCogsErrorForm.toDate,date_fmt,message_accounts[112]+ message[611]);
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	document.frmCogsErrorForm.strOpt.value = 'reload';
	fnStartProgress();
	document.frmCogsErrorForm.submit();
	

 }
function fnSubmit(){
	var inputStr = '';
	fnCreateString();
	inputStr = document.frmCogsErrorForm.inputStr.value;
	if(inputStr ==''){
		Error_Details(message_accounts[113]);
	}
	
	if (ErrorCount > 0)
	{
		document.frmCogsErrorForm.inputStr.value = '';
		Error_Show();
		Error_Clear();
		return false;
	}else{
	document.frmCogsErrorForm.strOpt.value = 'Save';
	fnStartProgress();
	document.frmCogsErrorForm.submit();
	}
}

function fnCreateString(){
	var inputString="";
	var errorType = "";
	var pnum = "";
	var gridrowid;
	
	var errorString = "";
	var inputCancelString = "";
	var inputRepostString = "";
	var Repost="";
	var Cancel = "";
	var UnCancel = "";
	gridObj.forEachRow(function(rowId) {
		
		fromCost = gridObj.cellById(rowId, fromCost_rowId).getValue();
		errorType = gridObj.cellById(rowId, erroTypeId_rowId).getValue();
		pnum = gridObj.cellById(rowId, partNum_rowId).getValue();
		Repost= document.getElementById("post_id"+rowId);
		Cancel = document.getElementById("cancel_id"+rowId);
		UnCancel = document.getElementById("uncancel_id"+rowId);
		// To header filter used, to form the input string (only filter).
		if (Repost != undefined && Cancel != undefined){
			
			if(UnCancel.checked == true && Cancel.checked == true){
				Cancel.checked = false;
			}
			//checking if fromCost is not null , error type not equal to 102941(Non Costing error)
			// also both repost and cancel are checked must throw the below validation
			if(fromCost ==''  && errorType != 102941 && (Repost.checked==true || Cancel.checked==true || UnCancel.checked==true)){ //102941 - Non Costing Error
				inputString = '0';
				Error_Clear();
				Error_Details(message_accounts[114]);
		   //if repost and cancel are checked then form the part numbers in error string
			}else if(Repost.checked==true && Cancel.checked==true){
				
				errorString +=pnum+',';
		   //if cancel is checked form the inputCancelString 
			}else if(Cancel.checked==true){
				inputCancelString+= rowId+','+'53007|';
		   //if repost is checked form the inputRepostString
			}else if(Repost.checked==true){
				inputRepostString+= rowId+','+'53008|';
			}else if(UnCancel.checked==true){
				inputRepostString+= rowId+','+'26241378|';
			}

		} // end of undefined check
		
	});
	
	inputString +=inputCancelString+',|'+inputRepostString+'|';
	document.frmCogsErrorForm.inputStr.value = inputString;
	//if the errorstring is not null , then throw the validation message
	// "For the Following Part(s) the Repost and Cancel checkbox is selected. Please uncheck one option. <List of all Parts>";
	if(errorString != ""){
		Error_Details(message_accounts[555]+errorString);
	 
           }
}
function fnChangeStatus(){
	var status = document.frmCogsErrorForm.status.value;
	if (objGridData != '' && SwitchUserFl != 'Y'){
		
		if(status != 0){
			document.frmCogsErrorForm.Submit_button.disabled = true;
		}else{
			document.frmCogsErrorForm.Submit_button.disabled = false;
		}
	}
}// This function used to press enter key to reload the form
function fnEnter(){
	if (event.keyCode == 13){ 
		fnReload();
	}
}

//This Function used to Initiate the grid
function myInitGridData(divRef, gridData) {
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableTooltips("true,true,true,false,true,true,true,true,true,true,true,true,true");
	gObj.attachHeader("<input type='checkbox' value='no' id='masterrepost' onClick='javascript:onClickSelectAllPost(this);'/>,<input type='checkbox' value='no' id='mastercancel' onClick='javascript:onClickSelectAllCancel(this);'/>,<input type='checkbox' value='no' id='masteruncancel' onClick='javascript:onClickSelectAllUnCancel(this);'/>,#text_filter,#text_filter,#select_filter,#select_filter,#text_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter");
	gObj.attachEvent("onKeyPress", keyPressed);
	gObj.init();
	gObj.enableAlterCss("even","uneven");
	gObj.loadXMLString(gridData);
	return gObj;
}
function fnExport()
{	
	//gridObj.detachHeader(1);
	// for disabling the Repost and Cancel and UnCancel checkbox column when excel download
	gridObj.setColumnHidden(0,true); 
	gridObj.setColumnHidden(1,true);
	gridObj.setColumnHidden(2,true);
	gridObj.toExcel('/phpapp/excel/generate.php');
	gridObj.setColumnHidden(0,false); 
	gridObj.setColumnHidden(1,false);
	gridObj.setColumnHidden(2,false);
	//gridObj.attachHeader('#rspan,#text_filter,#text_filter,#select_filter,#select_filter,#text_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter');
}
//this function is for master checkbox for Cancel column
function onClickSelectAllCancel(){
	var obj = document.getElementById("mastercancel");
	var Cancel = "";
	var status = document.frmCogsErrorForm.status.value;
	//the condition is checked only when the status is open (0)
	if (status == "0"){
	gridObj.forEachRow(function(rowId) {
		Cancel = document.getElementById("cancel_id"+rowId);
		// if header filter applied - to handle the undefined error.
		if(Cancel != undefined){
			
			if (obj.checked == true){
				Cancel.checked=true;
			}else{
				Cancel.checked=false;
			}
		} // end of cancel undefined check
		
	});
	}
}

function onClickSelectAllUnCancel(){
	var obj = document.getElementById("masteruncancel");
	var Cancel = "";
	var status = document.frmCogsErrorForm.status.value;
	//the condition is checked only when the status is cancel (53007)
	if (status == "53007"){ 
	gridObj.forEachRow(function(rowId) {
		Cancel = document.getElementById("uncancel_id"+rowId);
		// if header filter applied - to handle the undefined error.
		if(Cancel != undefined){
			
			if (obj.checked == true){
				Cancel.checked=true;
			}else{
				Cancel.checked=false;
			}
		} // end of cancel undefined check
		
	});
	}
}

//this function is for master checkbox for Repost column
function onClickSelectAllPost(){
	var obj = document.getElementById("masterrepost");
	var Repost = "";
	var status = document.frmCogsErrorForm.status.value;
	//the condition is checked only when the status is open (0)
	if (status == "0"){
	gridObj.forEachRow(function(rowId) {
		 Repost= document.getElementById("post_id"+rowId);
		// if header filter applied - to handle the undefined error.
		 if(Repost != undefined){
			 
			 if (obj.checked == true){
					
					Repost.checked=true;
				}else{
					Repost.checked=false;
				} 
		 } // end of repost undefined check
		
	});
	}
}
