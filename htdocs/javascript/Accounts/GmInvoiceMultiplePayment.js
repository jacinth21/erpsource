function fnGo()
{
	Error_Clear();
	fnValidateDropDn('Cbo_Action',lblChooseRpt);
	var invoiceId = document.frmAccount.Txt_InvoiceID.value;
 	var customerPO = document.frmAccount.Txt_CustomerPO.value;
 	var invoiceAmt = document.frmAccount.Txt_Invoice_Amount.value;
 	var accountID  = document.frmAccount.Txt_AccId.value;
    var accountNm  = document.frmAccount.Cbo_AccId.value;
    var parentFl  = document.frmAccount.Chk_ParentFl.checked;
 //	if ((invoiceId!=null&&!invoiceId.equals(""))||!customerPO.equals("") ||!invoiceAmt.equals("")){
 	
 //  	 fnValidateTxtFld('Cbo_AccId',' Account ');
   	 
 //   }
  
 	if(accountID == '' && (accountNm != '0' || accountNm != ''))
 	{           
 		document.frmAccount.Txt_AccId.value = accountNm;
 	}else{
 		document.frmAccount.Cbo_AccId.value = accountID;
 	}

 	if (ErrorCount > 0)
	{
		Error_Show();
		return false;
	}
 	fnStartProgress('Y');
	document.frmAccount.action="/GmInvoiceServlet";
	document.frmAccount.hAction.value = "PAYALL";
	document.frmAccount.submit();
}
function fnSort(val)
{
	document.frmAccount.hSort.value = val;
	if (val == sortCname)
	{

		if (document.frmAccount.hSortAscDesc.value == '1') 
		{
			document.frmAccount.hSortAscDesc.value = '2'
		}
		else
		{
			document.frmAccount.hSortAscDesc.value = '1'
		}
	}
	else
	{
		document.frmAccount.hSortAscDesc.value = '2';
	}
	document.frmAccount.hAction.value = "PAYALL";
	document.frmAccount.submit(); 
}
function fnLoad1()
{
fnDispatchLoad();

	
	if(document.frmAccount.hAction.value != "Load"){
		document.all.Lbl_Total.innerHTML = document.all.hTotalAmt.value;
	}
	
	if (strInvoiceType != '')
	{
		var objgrp = document.frmAccount.Chk_GrpChk_Invoice_Type;
		fnCheckSelections(strInvoiceType,objgrp);
	}
	
	if (varSource != '')
	{
		document.all.Cbo_InvSource.value = varSource;
	}

	if (varIdAccount!='')
	{
	//	document.all.idAccount.value = varIdAccount;
	}

	var obj = document.frmAccount.Cbo_Action;
	for (var i=0;i<obj.length ;i++ )
	{
		if (document.frmAccount.hMode.value == obj.options[i].value)
		{
			obj.options[i].selected = true;
			break;
		}
	}
	
	if(parAccfl == 'false'){
		document.frmAccount.Chk_ParentFl.checked = false;
	}

	
	$("#tdAccInfoCommon").find("tbody tr:last").each(function(){
		$(this).prepend($("#trSubParentPanel td:first"));
		$(this).append($("#trSubParentPanel td"));
	});
	
}

function fnCheckSelections(val,objgrp)
{
	var valobj = val.split(",");
	var sval = '';
	var arrlen = objgrp.length;
	
	if (arrlen > 0)
	{
		for (var j=0;j< valobj.length;j++ )
		{
			sval = valobj[j];
			for (var i=0;i< arrlen;i++ )
			{
				if (objgrp[i].value == sval)
				{
					objgrp[i].checked = true;
					break;
				}
			}
		}
	}
	else
	{
		objgrp.checked = true;
	}
}

function fnOpenStatement()
{
	var obj = document.all.Cbo_AccId;
	var type = document.all.Cbo_InvSource.value;
	var parentFl  = document.frmAccount.Chk_ParentFl.checked;
	var accCurrId = document.frmAccount.Cbo_Comp_Curr.value;
	
	if (obj.value =='' && (accId == ''|| accId == '0'))
	{
		Error_Details(message_accounts[160]); 
	}
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}else{
		windowOpener('/GmInvoiceServlet?Cbo_InvSource='+type+'&Cbo_Action=OPEN&hAction=STMNT&Chk_ParentFl='+parentFl+'&Cbo_AccId='+accId+'&Cbo_Comp_Curr='+accCurrId,"Statment","resizable=yes,scrollbars=yes,top=150,left=150,width=820,height=610");
	}
}

//this function used to export the RTF files
function fnExportRTFFile(invID, fileID){
	var uploadDir="UPLOAD_INVOICE"; 
	if (fileID !='')
	{
		document.frmAccount.action = "/GmCommonFileOpenServlet?sId="+fileID+"&uploadString="+uploadDir;
	}
	else
	{
		document.frmAccount.action = "/GmInvoiceInfoServlet?hInv="+invID+"&hAction=Print&type=saveRTFfile" ;
	} 
	 document.frmAccount.submit();
}
