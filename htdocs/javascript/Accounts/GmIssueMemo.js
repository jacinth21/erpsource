// This function is used to on page load - to initiate the grid.
function fnOnPageLoad() {
	var footer = new Array();
	footer[0] = message_accounts[100];
	footer[1] = "#cspan";
	footer[2] = "#cspan";
	footer[3] = "#cspan";
	footer[4] = "#cspan";
	if(arDealerFlag == 'YES'){
		footer[5] = "#cspan";
		footer[6] = "<b><div id='tot_Qty'>0</div></b>";
		footer[7] = "<b><div id='tot_Price'>0</div></b>";
		footer[8] = "<b><div id='tot_Amount'>0</div></b>";
		footer[9] = "";
	}
	else{
		footer[5] = "<b><div id='tot_Qty'>0</div></b>";
		footer[6] = "<b><div id='tot_Price'>0</div></b>";
		footer[7] = "<b><div id='tot_Amount'>0</div></b>";
		footer[8] = "";
	}

	var memoTypeVal = document.frmIssueMemo.memoType.value;
	var disPlay = lblCredit;
	if (memoTypeVal == '50203') {
		disPlay = lblDebit;
	}

	gridObj = initGridWithDistributedParsing('issueMemoRpt', objGridData, footer);
	if(arDealerFlag == 'YES'){
		gridObj.enableTooltips("true,true,true,true,true,true,true,true,true");
		 gridObj.attachHeader('#rspan,#rspan,#rspan,#rspan,#rspan,'+lblPriceEA+','+disPlay+ lblQty+','+disPlay +lblPriceEA +','+lblAmt+',#rspan');
	}else{
		gridObj.enableTooltips("true,true,true,true,true,true,true,true");
		 gridObj.attachHeader('#rspan,#rspan,#rspan,#rspan,'+lblPriceEA+','+disPlay+ lblQty+','+disPlay +lblPriceEA +','+lblAmt+',#rspan');
	}

	//gridObj.attachHeader('#rspan,#rspan,#rspan,#rspan,'+lblPriceEA, disPlay + ' Qty,' + disPlay + lblPriceEA+',Amount,#rspan');
	gridObj.attachEvent("onEditCell", doOnCellEdit);

	// declare global variable and set index value.
	inv_rowId = gridObj.getColIndexById("invID");
	part_rowId = gridObj.getColIndexById("part");
	accName_rowId = gridObj.getColIndexById("accName");
	if(arDealerFlag == 'YES'){
		accType_rowId = gridObj.getColIndexById("accType");
	}
	qty_rowId = gridObj.getColIndexById("qty");
	price_rowId = gridObj.getColIndexById("priceEA");
	userQty_rowId = gridObj.getColIndexById("userQty");
	userPrice_rowId = gridObj.getColIndexById("userPrice");
	totalAmt_rowId = gridObj.getColIndexById("totalAmt");
	accID_rowId = gridObj.getColIndexById("accID");
	orderID_rowId = gridObj.getColIndexById("orderID");
	custPO_rowId = gridObj.getColIndexById("custPO");
	itemType_rowId = gridObj.getColIndexById("itemType");
	itemOrdId_rowId = gridObj.getColIndexById("itemOrdId");
	comments_rowId = gridObj.getColIndexById("comments");

	
	if(arDealerFlag == 'YES'){
		// For Single click
		gridObj.enableEditEvents(true, true, false, false, false, false, true, true, false);
	}else{
		// For Single click
		gridObj.enableEditEvents(true, true, false, false, false, true, true, false);
	}

	// For enable the tab order
	gridObj.enableEditTabOnly(true);

	var gridrows = gridObj.getAllRowIds(",");
	// default 10 rows to display
	if (gridrows.length == 0) {
		for (i = 0; i <= 9; i++) {
			addRow();
		}
	}

}

// Description : This function used to add a row(grid)
function addRow() {
	gridObj.addRow(myRowId, '');
	fnViewCellText(myRowId);
	myRowId = myRowId + 1;
}
// Description : This function used to remove a row(grid)
function removeSelectedRow() {
	var gridrows = gridObj.getSelectedRowId();
	if (gridrows != undefined) {
		var gridrowsarr = gridrows.toString().split(",");
		for ( var rowid = 0; rowid < gridrowsarr.length; rowid++) {
			gridrowid = gridrowsarr[rowid];
			gridObj.deleteRow(gridrowid);
			fnSetFooterVal();
		}
	} else {
		Error_Clear();
		Error_Details(message_accounts[58]);
		Error_Show();
		Error_Clear();
	}
}

// Description : This function to fire the cell edit event. To get the Tag qty
function doOnCellEdit(stage, rowId, cellInd, nValue, oValue) {
	if (stage == 2){
		rowID = rowId;
		if (cellInd == part_rowId || cellInd == inv_rowId) { //Part number and Invoice Index
			var invoiceVal = gridObj.cellById(rowId, inv_rowId).getValue();
			var newPartVal = gridObj.cellById(rowId, part_rowId).getValue();
			if (invoiceVal != '' && nValue != oValue && newPartVal !='') {
				var gridrows = gridObj.getAllRowIds();
				var gridrowsarr = gridrows.toString().split(",");
				for ( var rowid = 0; rowid < gridrowsarr.length; rowid++) {
					gridrowid = gridrowsarr[rowid];
					if (gridrowid != rowId) {
						var OldInvId = gridObj.cellById(gridrowid, inv_rowId).getValue();
						var OldPart = gridObj.cellById(gridrowid, part_rowId).getValue();

						if (invoiceVal == OldInvId && newPartVal == OldPart) {
							Error_Details(Error_Details_Trans(message_accounts[163],invoiceVal));
							setTimeout(function(){fnShowError(invoiceVal)},5);  
							return true;		
						}
					}
				}
				fnGetInvoiceDtls(invoiceVal, newPartVal);
			}
			if (nValue == '') {
				fnClearValues();
				fnSetFooterVal();
			}

		} else if (cellInd == userPrice_rowId) { // Price index
			if (nValue != oValue) {
				var invQty = gridObj.cellById(rowId, userQty_rowId).getValue();
				if (invQty != '' && invQty != '0') {
					gridObj.cellById(rowId, userPrice_rowId).setValue(formatNumber(nValue));
					var cal = invQty * nValue;
					gridObj.cellById(rowId, totalAmt_rowId).setValue(formatNumber(cal));
					var totalQtyVal = document.getElementById("tot_Qty");
					totalQtyVal.innerHTML = sumColumn(userQty_rowId);
					var totalPriceVal = document.getElementById("tot_Price");
					totalPriceVal.innerHTML = formatNumber(sumColumn(userPrice_rowId));
					var totalAmtVal = document.getElementById("tot_Amount");
					totalAmtVal.innerHTML = formatNumber(sumColumn(totalAmt_rowId));
				}
			}
		} else if (cellInd == userQty_rowId) { // Qty index
			if (nValue != oValue) {
				var invId = gridObj.cellById(rowId, inv_rowId).getValue();
				var qtyPrice = gridObj.cellById(rowId, userPrice_rowId).getValue();
					gridObj.cellById(rowId, userQty_rowId).setValue(nValue);
					var cal = nValue * qtyPrice;
					gridObj.cellById(rowId, totalAmt_rowId).setValue(formatNumber(cal));
					var totalQtyVal = document.getElementById("tot_Qty");
					totalQtyVal.innerHTML = sumColumn(userQty_rowId);
					var totalPriceVal = document.getElementById("tot_Price");
					totalPriceVal.innerHTML = formatNumber(sumColumn(userPrice_rowId));
					var totalAmtVal = document.getElementById("tot_Amount");
					totalAmtVal.innerHTML = formatNumber(sumColumn(totalAmt_rowId));
			}
		}
	}
	
	return true;
}
// This function is used to show the error message.
// Enable the tab order so, the error message go back, here set the time out at
// the time only this function call
function fnShowError(val) {
	if (ErrorCount > 0) {
		fnClearValues();
		Error_Show();
		Error_Clear();
		return false;
	}
}
// Description : This function used to get the Invoice Details
function fnGetInvoiceDtls(invID, pNum) {
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmIssueMemo.do?method=fetchInvDtls&invoiceID=' + invID
			+ '&partNum=' + encodeURIComponent(pNum) + '&ramdomId=' + Math.random());
	dhtmlxAjax.get(ajaxUrl, fnLoadInvDtls);
}

// Description : This function used to load the invoice details
function fnLoadInvDtls(loader) {
	var response = loader.xmlDoc.responseXML;
	var strAccName = '';
	var strInvID = '';
	var strPnum = '';
	var strQty = '';
	var strPriceEa = '';
	var strAccID = '';
	var strPO = '';
	var strOrderID = '';
	var strItemType = '';
	var strInvStatus = '';
	var strInvType = '';
	var strItemOrdId = '';
	var strAccType = '';
	var row_ID = '';
	var InvDet = response.getElementsByTagName("pnum");
	var arSelectedCurr = document.frmIssueMemo.strCompCurrency.value;
	if (InvDet.length == '0') {
		Error_Details(message_accounts[164]);
	}

	if (!blFirstRec) {
		firstAccRowID = rowID;
		blFirstRec = true;
	}
	row_ID = rowID;
	for ( var x = 0; x < InvDet.length; x++) {
		strInvID = parseXmlNode(InvDet[x].childNodes[0].firstChild);
		strPnum = parseXmlNode(InvDet[x].childNodes[1].firstChild);
		strAccName = parseXmlNode(InvDet[x].childNodes[2].firstChild);
		strQty = parseXmlNode(InvDet[x].childNodes[3].firstChild);
		strPriceEa = parseXmlNode(InvDet[x].childNodes[4].firstChild);
		strAccID = parseXmlNode(InvDet[x].childNodes[5].firstChild);
		strPO = parseXmlNode(InvDet[x].childNodes[6].firstChild);
		strOrderID = parseXmlNode(InvDet[x].childNodes[7].firstChild);
		strItemType = parseXmlNode(InvDet[x].childNodes[8].firstChild);
		strInvStatus = parseXmlNode(InvDet[x].childNodes[9].firstChild);
		strInvType = parseXmlNode(InvDet[x].childNodes[10].firstChild);
		strItemOrdId = parseXmlNode(InvDet[x].childNodes[11].firstChild);
		var strARCurrId = parseXmlNode(InvDet[x].childNodes[12].firstChild);
		var strAccType = parseXmlNode(InvDet[x].childNodes[13].firstChild);
		row_ID = fnGetRowID(x,rowID);

		fnSetValues(row_ID, inv_rowId, strInvID);
		fnSetValues(row_ID, part_rowId, strPnum);
		fnSetValues(row_ID, accName_rowId, strAccName);
		if(arDealerFlag == 'YES'){
			fnSetValues(row_ID, accType_rowId, strAccType);	
		}
		fnSetValues(row_ID, qty_rowId, strQty);
		fnSetValues(row_ID, price_rowId, strPriceEa);
		// set the hidden values
		fnSetValues(row_ID, accID_rowId, strAccID);
		fnSetValues(row_ID, orderID_rowId, strOrderID);
		fnSetValues(row_ID, custPO_rowId, strPO);
		fnSetValues(row_ID, itemType_rowId, strItemType);
		fnSetValues(row_ID, itemOrdId_rowId, strItemOrdId);
		
		if(strInvType =='50200'){ // Regular
		   	gridObj.setRowTextStyle(row_ID, "color:green;");
	   	}else if(strInvType =='50201'){ // OBO
	   		gridObj.setRowTextStyle(row_ID, "color:purple;");
	   	}else if(strInvType =='50202'){ // Credit
	   		gridObj.setRowTextStyle(row_ID, "color:red;");
	   	}else if(strInvType =='50203'){ // Debit
	   		gridObj.setRowTextStyle(row_ID, "color:blue;");
	   	}
		fnViewCellText(row_ID);
	   	
		//Currency symbol validation
		if(strARCurrId!=arSelectedCurr){
			Error_Details(message_accounts[165]);
		}
	}// For Loop

	fnValidateAccount();
	fnSetFooterVal ();
	if (ErrorCount > 0) {
		fnClearValues();
		Error_Show();
		Error_Clear();
		return false;
	}

}
// This function is used to return ""
function parseNull(val) {
	if (val == 'null')
		return val = "";
	else
		return val;
}
// This function is used to form the input string and submit the form.
function fnSubmit() {
	var invoiceVal = '';
	var partNum = '';
	var accountVal = '';
	var custPoVal = '';
	var orderVal = '';
	var userQty = '';
	var userprice = '';
	var itemTypeVal = '';
	var itemOrdIdVal = '';
	var commentsVal = '';

	var inputStr = '';
	var invoiceStr = '';
	var duinvoiceStr = '';
	var blDataFl = false;
	var blTotalPrice = false;
	var blNegativePrice
	var greaterQty = '';
	var notNumPrice = '';
	var notNumQty = '';
	var negPrice = '';
	var negQty = '';
	var emptyPrice = '';
	var emptyQty = '';
	var strQty = '';
	var strAmt = '';
	var emptyQtyPrice = '';
	var emptyPart = '';
	var errTotalAmt = '';
	var errNegativeAmt = '';
	var greaterprice = '';     

	fnValidateDropDn('memoType', lblMemoType);
	fnValidateAccount ();
	if(blOtherAcc){
		Error_Details(message_accounts[166]);
	}
	
	gridObj.forEachRow(function(rowId) {
		invoiceVal = gridObj.cellById(rowId, inv_rowId).getValue();
		partNum = gridObj.cellById(rowId, part_rowId).getValue();
		
		if(invoiceVal != '' && partNum != ''){
			var memoTypeVal = document.frmIssueMemo.memoType.value;
			
			userQty = gridObj.cellById(rowId, userQty_rowId).getValue();
			actualQty = gridObj.cellById(rowId, qty_rowId).getValue();
			
			priceEA = gridObj.cellById(rowId, price_rowId).getValue();
			userprice = RemoveComma(gridObj.cellById(rowId, userPrice_rowId).getValue());
			
			//added to skip if the price has negative unit price 
			if (priceEA != null && priceEA != undefined){
			priceEA = Math.abs(priceEA);
			}
			strQty = userQty.replace(/[0-9]+/g,''); // Qty text box
			strAmt = userprice.replace(/-?[.0-9]+/g,''); // Amt text box
			
			// Price - number validation
			if (isNaN(userprice))
		    {
				notNumPrice = notNumPrice + partNum +',';
		    } else if(strAmt.length >0){
				negPrice = negPrice + partNum +',';
			}
			if (isNaN(userQty) || strQty.length >0){
				negQty = negQty + partNum +',';
			}
			
			if (userQty !='' && ( userprice =='' || (parseFloat(priceEA)!=0 && parseFloat(userprice) ==0)))
		    {   
				emptyPrice = emptyPrice + partNum +',';
		    }
			if (userprice !='' && (userQty =='' || userQty =='0'))
		    {   
				emptyQty = emptyQty + partNum +',';
		    }
			if(userQty =='' && userprice ==''){
				emptyQtyPrice = emptyQtyPrice + partNum +',';
			}
			
			if((userQty !='' && userQty !='0') && ((parseFloat(priceEA)!=0 && parseFloat(userprice) !=0) || (parseFloat(priceEA) ==0 && parseFloat(userprice) ==0))){
				
				blDataFl = true;
			}
			if(parseFloat(userQty) > parseFloat(actualQty)){
				greaterQty = greaterQty + partNum +',';
			}
			if(memoTypeVal == '50202' && (parseFloat(userprice) > parseFloat(priceEA))){     // Used to PMT-39771 -System Validating for A/R Issue Credit quantity and amount
				greaterprice = greaterprice + partNum +',';
			}
			
			
			//Removing the below validation for BUG-8831
			/*if(memoTypeVal == '50202' && (parseFloat(priceEA) < parseFloat(userprice)))
			{
			blTotalPrice = true;
			errTotalAmt = errTotalAmt + partNum +',';
			}*/
			
			if(memoTypeVal == '50202' && (parseFloat(userprice)<0))// Used to PMT-39771 -System Validating for A/R Issue Credit quantity and amount
			{
			blNegativePrice = true;
			errNegativeAmt = errNegativeAmt + partNum +',';
			}
			
		}
		if(invoiceVal != '' && partNum ==''){
			emptyPart = emptyPart + invoiceVal +',';
		}
	});
	
	if (blTotalPrice) {
		errTotalAmt = errTotalAmt.substring(0, errTotalAmt.length - 1);
		Error_Details(Error_Details_Trans(message_accounts[314],errTotalAmt));
	}
	if(greaterQty !=''){
		greaterQty = greaterQty.substring(0,(greaterQty.length)-1);
		Error_Details(Error_Details_Trans(message_accounts[315],greaterQty));
	}
	if (blNegativePrice) {
		errNegativeAmt = errNegativeAmt.substring(0, errNegativeAmt.length - 1);
		Error_Details(Error_Details_Trans(message_accounts[316],errNegativeAmt));
	}
	if(notNumPrice !=''){
		notNumPrice = notNumPrice.substring(0,(notNumPrice.length)-1);
		Error_Details(Error_Details_Trans(message_accounts[167],notNumPrice));
	}
	if(negPrice !=''){
		negPrice = negPrice.substring(0,(negPrice.length)-1);
		Error_Details(Error_Details_Trans(message_accounts[168],negPrice));
	}
	if(negQty !=''){
		negQty = negQty.substring(0,(negQty.length)-1);
		Error_Details(Error_Details_Trans(message_accounts[169],negQty));
	}
	if(notNumQty !=''){
		notNumQty = notNumQty.substring(0,(notNumQty.length)-1);
		Error_Details(Error_Details_Trans(message_accounts[170],notNumQty));
	}
	if(emptyPrice !=''){
		emptyPrice = emptyPrice.substring(0,(emptyPrice.length)-1);
		Error_Details(Error_Details_Trans(message_accounts[171],emptyPrice));
	}
	if(emptyQty !=''){
		emptyQty = emptyQty.substring(0,(emptyQty.length)-1);
		Error_Details(Error_Details_Trans(message_accounts[172],emptyQty));
	}
	if(emptyQtyPrice !=''){
		emptyQtyPrice = emptyQtyPrice.substring(0,(emptyQtyPrice.length)-1);
		Error_Details(Error_Details_Trans(message_accounts[173],emptyQtyPrice));
	}
	if(emptyPart !=''){
		emptyPart = emptyPart.substring(0,(emptyPart.length)-1);
		Error_Details(Error_Details_Trans(message_accounts[174],emptyPart));
	}
	if(greaterprice !=''){                         // Used to PMT-39771 -System Validating for A/R Issue Credit quantity and amount
		greaterprice = greaterprice.substring(0,(greaterprice.length)-1);
		Error_Details(Error_Details_Trans(message_accounts[556],greaterprice));
	}
	
	if(!blDataFl && ErrorCount == 0){
		Error_Details(message_accounts[175]);
	}
	
	if (ErrorCount > 0) {
		document.frmIssueMemo.inputString.value = '';
		Error_Show();
		Error_Clear();
		return false;
	}else{
		// group the invoice row
		gridObj.groupBy(inv_rowId);
		// to get the invoice id Group, and store to invoiceStr String.
		gridObj.forEachRow(function(rowId) {
			invoiceVal = gridObj.cellById(rowId, inv_rowId).getValue();
			partNum = gridObj.cellById(rowId, part_rowId).getValue();
			if (invoiceVal != '' && partNum != '') {
				if (invoiceStr.indexOf(invoiceVal) !== -1) {
					duinvoiceStr += invoiceVal + '^';
				} else {
					invoiceStr += invoiceVal + '^';
				}
			}
		});

		// split the invoiceStr and form the input string.
		var mySplitResult = invoiceStr.split("^");
		var resultLen = mySplitResult.length;
		for (i = 0; i < resultLen - 1; i++) {
			var blInvoicefl = false;
			var invoiceInputStr = '';
			var partInputStr = '';
			var commentsStr = '[';
			gridObj.forEachRowInGroup(mySplitResult[i], function(id) {
				invoiceVal = gridObj.cellById(id, inv_rowId).getValue();
				partNum = gridObj.cellById(id, part_rowId).getValue();
				// only one time to invoice id, account id and po add to String.
			 if(invoiceVal !='' && partNum !=''){
				 
				 if (!blInvoicefl) {
						accountVal = gridObj.cellById(id, accID_rowId).getValue();
						custPoVal = gridObj.cellById(id, custPO_rowId).getValue();
						invoiceInputStr += invoiceVal + '[' + accountVal + '[' + custPoVal + '[';
						blInvoicefl = true;
					}
					// To form the Part input string - this string directly calling to gm_save_credit_sales prc.
					orderVal = gridObj.cellById(id, orderID_rowId).getValue();
					userQty = gridObj.cellById(id, userQty_rowId).getValue();
					userprice = RemoveComma(gridObj.cellById(id, userPrice_rowId).getValue());
					itemTypeVal = gridObj.cellById(id, itemType_rowId).getValue();
					itemOrdIdVal = gridObj.cellById(id, itemOrdId_rowId).getValue();
					commentsVal = TRIM(gridObj.cellById(id, comments_rowId).getValue());
					
					if(commentsVal !=''){
						commentsStr +=  commentsVal+' ';
					}

					if(userQty !='' && userprice !=''){
						partInputStr += orderVal + '^' + partNum + '^' + userQty + '^' + userprice + '^' + itemTypeVal + '^' + itemOrdIdVal +  '|';
					}				
			 }	
			});
			// set the master Input String.
			inputStr += invoiceInputStr + partInputStr + commentsStr +'!!';
		}
	}
	if (ErrorCount > 0) {
		document.frmIssueMemo.inputString.value = '';
		Error_Show();
		Error_Clear();
		return false;
	}
	fnStartProgress("Y");
	document.frmIssueMemo.strOpt.value = 'Save';
	document.frmIssueMemo.inputString.value = inputStr;
	document.frmIssueMemo.submit();
}
// This function is used to when change the Memo type reload the page
function fnChangeMemo(objMemoType) {
	if (objMemoType.value != 0) {
		document.frmIssueMemo.strOpt.value = '';
		document.frmIssueMemo.submit();
	}
}
// This function used to sum the index column
function sumColumn(ind) {
	var out = 0;
	var invId = '';
	var partnumber = '';
	gridObj.forEachRow(function(rowId) {
		invId = gridObj.cellById(rowId, inv_rowId).getValue();
		partnumber = gridObj.cellById(rowId, part_rowId).getValue();
		if (invId != '' && partnumber != '') {
			var total = RemoveComma(gridObj.cellById(rowId, ind).getValue());
			if (total != '') {
				out = out + parseFloat(total);
			}
		}
	});
	return out;
}
// This function used to set the grid values.
function fnSetValues(id, indexid, values) {
	gridObj.cellById(id, indexid).setValue(values);
	return true;
}
// This function used to show the text view in grid. (Invoice,Part,Qty and Price) cell
function fnViewCellText(changeID) {
	gridObj.setCellTextStyle(changeID, inv_rowId, "background-color:white; border-width: 1px;border-style: inset;");
	gridObj.setCellTextStyle(changeID, part_rowId, "background-color:white; border-width: 1px;border-style: inset;");
	gridObj.setCellTextStyle(changeID, userQty_rowId, "background-color:white; border-width: 1px;border-style: inset;");
	gridObj.setCellTextStyle(changeID, userPrice_rowId,"background-color:white; border-width: 1px;border-style: inset;");
	gridObj.setCellTextStyle(changeID, comments_rowId,"background-color:white; border-width: 1px;border-style: inset;");
}
// This function used to compart the first account and all other account (validate)
function fnValidateAccount() {
	blOtherAcc = false;
	var fistAccFlag = true;
	var firstAccountVal = '';
	gridObj.forEachRow(function(rowId) {
		var invoiceVal = gridObj.cellById(rowId, inv_rowId).getValue();
		var partNum = gridObj.cellById(rowId, part_rowId).getValue();
		if (invoiceVal != '' && partNum != '') {
			if(fistAccFlag){
				firstAccountVal = gridObj.cellById(rowId, accID_rowId).getValue();
				fistAccFlag = false;
			}		 
			var currentAccountVal = gridObj.cellById(rowId, accID_rowId).getValue();
			if (firstAccountVal != '' && currentAccountVal !='' && firstAccountVal != currentAccountVal) {
				gridObj.setCellTextStyle(rowId, accName_rowId, "color:red;");
				blOtherAcc = true;
			}
		}
	});
	return true;
}
// This function used to show the Print invoice screen.
function fnPrintInvoice(varinv) {
	windowOpener('/GmInvoiceInfoServlet?hAction=Print&hInv=' + varinv,
			"PrntInv",
			"resizable=yes,scrollbars=yes,top=150,left=200,width=750,height=500");
}
// This function used to return the next row id
function fnGetRowID(id,rowIDs){
	var returnID = '';
	var blEmptyFl = false;
	if(id ==0){
		returnID = rowIDs;
	}else if((rowIDs + id) < (myRowId)){
		gridObj.forEachRow(function(rowId) {
			invId = gridObj.cellById(rowId, inv_rowId).getValue();
			partnumber = gridObj.cellById(rowId, part_rowId).getValue();
			if (invId == '' && partnumber == '' && rowIDs < rowId) {
				if(!blEmptyFl){
					returnID = rowId;
					blEmptyFl = true;
				}				
			}
		});
	}else{
		addRow();
		returnID = rowIDs + id;		
	}
	return returnID;
}
//This function used to set the footer total values.
function fnSetFooterVal (){
	var totalQtyVal = document.getElementById("tot_Qty");
	totalQtyVal.innerHTML = sumColumn(userQty_rowId);
	var totalPriceVal = document.getElementById("tot_Price");
	totalPriceVal.innerHTML = formatNumber(sumColumn(userPrice_rowId));
	var totalAmtVal = document.getElementById("tot_Amount");
	totalAmtVal.innerHTML = formatNumber(sumColumn(totalAmt_rowId));
}
// This function used to press the enter key then submit the page.
function fnEnter() {
	if (event.keyCode == 13) {
		fnSubmit();
	}
}
// This function used to clear the values.
function fnClearValues() {
	gridObj.cellById(rowID, part_rowId).setValue("");
	gridObj.cellById(rowID, accName_rowId).setValue("");
	if(arDealerFlag == 'YES'){
		gridObj.cellById(rowID, accType_rowId).setValue("");
	}
	gridObj.cellById(rowID, qty_rowId).setValue("");
	gridObj.cellById(rowID, price_rowId).setValue("");
	gridObj.cellById(rowID, userQty_rowId).setValue("");
	gridObj.cellById(rowID, userPrice_rowId).setValue("");
	gridObj.cellById(rowID, totalAmt_rowId).setValue("");
	gridObj.cellById(rowID, accID_rowId).setValue("");
	gridObj.cellById(rowID, orderID_rowId).setValue("");
	gridObj.cellById(rowID, custPO_rowId).setValue("");
	gridObj.cellById(rowID, itemType_rowId).setValue("");
	gridObj.cellById(rowID, itemOrdId_rowId).setValue("");
	gridObj.cellById(rowID, comments_rowId).setValue("");
}
