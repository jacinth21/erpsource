function fnOnPageLoad(){
	
	//we implement the auto complete functionality - so, not required to call below function. 
	fnDispatchLoad();
	
	if(parentFl == 'false'){
			document.all.Chk_ParentFl.checked = false;
//As we have two Grid format [ With Parent Account or Rep Account] 
// we will have to load the Grid accordingly. The below function will do loading of Grid.
			fnLoadRepGrid();
	}else{
		fnLoadParentGrid();
	}
}
//Loading the Grid based on the Parent Checkbox selected.
function fnLoadParentGrid()
{
	if (objGridData!=''){
		var rptType = document.all.reportType.value;
		var footerval;
		if (rptType == "ARByInvoiceDt")
		{
			if(strCustCategoryId == "Y"){
				footerval = "16";
			}else{
				footerval = "15";
			}		
		}else{
			if(strCustCategoryId == "Y"){
				footerval = "18";
			}else{
				footerval = "17";
			}
		}
		var footer = new Array();
		if(strCustCategoryId == "Y" && rptType == "ARByDueDt"){
			footer[0]= message_accounts[97];
			footer[1]="#cspan";
			footer[2]="#cspan";
			footer[3]="#cspan";
			footer[4]="#cspan";
			footer[5]="#cspan";
			footer[6]="#cspan";
			footer[7]="#cspan";
			footer[8]=currSymbol+"{#stat_total}";
			footer[9]="#cspan";
			footer[10]=currSymbol + "{#stat_total}";
			footer[11]="#cspan";
			footer[12]=currSymbol + "{#stat_total}";
			footer[13]="#cspan";
			footer[14]=currSymbol + "{#stat_total}";
			footer[15]="#cspan";
		}else if (strCustCategoryId == "Y" && rptType == "ARByInvoiceDt"){
			footer[0]= message_accounts[97];
			footer[1]="#cspan";
			footer[2]="#cspan";			
			footer[3]="#cspan";
			footer[4]="#cspan";
			footer[5]="#cspan";
			footer[6]="#cspan";
			footer[7]="#cspan";
			footer[8]=currSymbol+"{#stat_total}";
			footer[9]="#cspan";
			footer[10]=currSymbol + "{#stat_total}";
			footer[11]="#cspan";
			footer[12]=currSymbol + "{#stat_total}";
			footer[13]="#cspan";
			footer[14]=currSymbol + "{#stat_total}";
			footer[15]="#cspan";		
		}else{
			footer[0]= message_accounts[97];
			footer[1]="#cspan";
			footer[2]="#cspan";			
			footer[3]="#cspan";
			footer[4]="#cspan";
			footer[5]="#cspan";
			footer[6]="#cspan";
			footer[7]=currSymbol+"{#stat_total}";
			footer[8]="#cspan";
			footer[9]=currSymbol + "{#stat_total}";
			footer[10]="#cspan";
			footer[11]=currSymbol + "{#stat_total}";
			footer[12]="#cspan";
			footer[13]=currSymbol + "{#stat_total}";
			footer[14]="#cspan";
		}
		
		if (rptType == "ARByDueDt")
		{
			if(strCustCategoryId == "Y"){
				footer[16]=currSymbol + "{#stat_total}";
				footer[17]="#cspan";
				footer[18]=currSymbol + "{#stat_total}";
			}else{
				footer[15]=currSymbol + "{#stat_total}";
				footer[16]="#cspan";
				footer[17]=currSymbol + "{#stat_total}";
			}

		}
		if (rptType == "ARByInvoiceDt")
		{
			if (columnSize<2){
				if(strCustCategoryId == "Y"){
					footer[16]=currSymbol + "{#stat_total}";
					footer[17]="";
					footer[18]="";
					footer[19]="";
				}else{
					footer[15]=currSymbol + "{#stat_total}";
					footer[16]="";
					footer[17]="";
					footer[18]="";
				}
			}else{
				for(i = 1; i<columnSize; i++){
					if(i>=2){footerval++;}
					footer[footerval]=currSymbol + "{#stat_total}";
					footerval++;
					footer[footerval]="#cspan";
				}
				footer[footerval+1]=currSymbol + "{#stat_total}";
				footer[footerval+2]="";
				footer[footerval+3]="";
				footer[footerval+4]="";
			}
		}else{
			if (columnSize<2){
				if(strCustCategoryId == "Y"){
					footer[19]="";
					footer[20]="";
				}else{
					footer[18]="";
					footer[19]="";
					footer[20]="";
				}
				
				}else{
					for(i = 1; i<columnSize; i++){
						if(i>=2){footerval++;}
						footer[footerval]=currSymbol + "{#stat_total}";
						footerval++;
						footer[footerval]="#cspan";
					}
					footer[footerval+1]=currSymbol + "{#stat_total}";
					footer[footerval+2]="";
					footer[footerval+3]="";
					footer[footerval+4]="";
				}
		}
		  gridObj = initGridWithDistributedParsing('arsummarydata',objGridData,footer);
		  gridObj.enableHeaderMenu();

		  region_Index = gridObj.getColIndexById("region");
		  credit_rating_Index = gridObj.getColIndexById("credit_rating");
		  last_credit_Index = gridObj.getColIndexById("last_credit");		 
	}
}

//Loading the Grid based on the Rep Account selected.
function fnLoadRepGrid(){
	
	if (objGridData!=''){
		var rptType = document.all.reportType.value;
		var footerval;
		if (rptType == "ARByInvoiceDt")
		{
			if(arRptByDealerFlag=='YES'){
					footerval = "17";
			}
			else{
				if(strCustCategoryId == "Y"){
					footerval = "17";
				}else{
					footerval = "16";
				}
			}
			
		}else{
			if(arRptByDealerFlag=='YES'){
					footerval = "17";
			}
			else{
				if(strCustCategoryId == "Y"){
					footerval = "19";
				}else{
					footerval = "18";
				}
			}
		}
		var footer = new Array();
		footer[0]= message_accounts[97];
		footer[1]="#cspan";
		footer[2]="#cspan";
		footer[3]="#cspan";
		footer[4]="#cspan";
		footer[5]="#cspan";
		footer[6]="#cspan";
		footer[7]="#cspan";
		if(arRptByDealerFlag=='YES'){
			footer[8]="#cspan";
			footer[9]=currSymbol+"{#stat_total}";
			footer[10]="#cspan";
			footer[11]=currSymbol + "{#stat_total}";
			footer[12]="#cspan";
			footer[13]=currSymbol + "{#stat_total}";
			footer[14]="#cspan";
			footer[15]=currSymbol + "{#stat_total}";
			footer[16]="#cspan";
		}
		else
			{
			if(strCustCategoryId == "Y"){
				footer[8]="#cspan";
				footer[9]=currSymbol+"{#stat_total}";
				footer[10]="#cspan";
				footer[11]=currSymbol + "{#stat_total}";
				footer[12]="#cspan";
				footer[13]=currSymbol + "{#stat_total}";
				footer[14]="#cspan";
				footer[15]=currSymbol + "{#stat_total}";
				footer[16]="#cspan";
			}else{
				footer[8]=currSymbol+"{#stat_total}";
				footer[9]="#cspan";
				footer[10]=currSymbol + "{#stat_total}";
				footer[11]="#cspan";
				footer[12]=currSymbol + "{#stat_total}";
				footer[13]="#cspan";
				footer[14]=currSymbol + "{#stat_total}";
				footer[15]="#cspan";
			}
		
			}

		
		if (rptType == "ARByDueDt")
		{
			if(arRptByDealerFlag=='YES'){
				footer[17]=currSymbol + "{#stat_total}";
				footer[18]="#cspan";
				footer[19]=currSymbol + "{#stat_total}";
			}
			else
				{
				if(strCustCategoryId == "Y"){
					footer[17]=currSymbol + "{#stat_total}";
					footer[18]="#cspan";
					footer[19]=currSymbol + "{#stat_total}";
				}else{
					footer[16]=currSymbol + "{#stat_total}";
					footer[17]="#cspan";
					footer[18]=currSymbol + "{#stat_total}";
				}

				}

		}
		if (rptType == "ARByInvoiceDt")
		{
			if (columnSize<2){
				if(arRptByDealerFlag=='YES'){
					footer[17]=currSymbol + "{#stat_total}";
					footer[18]="";
					footer[19]="";
					footer[20]="";
				}
				else
					{
					if(strCustCategoryId == "Y"){
						footer[16]="#cspan";
						footer[17]=currSymbol + "{#stat_total}";
						footer[18]="";
						footer[19]="";
					}else{
						footer[16]=currSymbol + "{#stat_total}";
						footer[17]="";
						footer[18]="";
						footer[19]="";
					}

					}

			}else{
				for(i = 1; i<columnSize; i++){
					if(i>=2){footerval++;}
					footer[footerval]=currSymbol + "{#stat_total}";
					footerval++;
					footer[footerval]="#cspan";
				}
				footer[footerval+1]=currSymbol + "{#stat_total}";
				footer[footerval+2]="";
				footer[footerval+3]="";
				footer[footerval+4]="";
			}
		}else{
			if (columnSize<2){
				if(arRptByDealerFlag=='YES'){
					footer[20]="";
					footer[21]="";
					footer[22]="";
				}
				else
					{
					if(strCustCategoryId == "Y"){
						footer[20]="";
						footer[21]="";
					}else{
						footer[19]="";
						footer[20]="";
						footer[21]="";	
					}

					}

				}else{
					for(i = 1; i<columnSize; i++){
						if(i>=2){footerval++;}
						footer[footerval]=currSymbol + "{#stat_total}";
						footerval++;
						footer[footerval]="#cspan";
					}
					footer[footerval+1]=currSymbol + "{#stat_total}";
					footer[footerval+2]="";
					footer[footerval+3]="";
					footer[footerval+4]="";
				}
		}
		  gridObj = initGridWithDistributedParsing('arsummarydata',objGridData,footer);
		  gridObj.enableHeaderMenu();

		  last_payment_Index = gridObj.getColIndexById("last_payment");
		  region_Index = gridObj.getColIndexById("region");
		  credit_rating_Index = gridObj.getColIndexById("credit_rating");
		  last_credit_Index = gridObj.getColIndexById("last_credit");	

	}

}

function fnSubmit()
{
	document.frmAccount.hAction.value = "Go";
	document.frmAccount.submit();
}

var prevtr = 0;

function changeTRBgColor(val,BgColor) {
   var obj1 = eval("document.all.tr"+prevtr);
   obj1.style.background = "";
   var obj = eval("document.all.tr"+val);
   prevtr = val;
   obj.style.background = BgColor;
}

function fnViewDetails(hRefid,hReport,hfromDay,htoDay,hInvSource,hDivId)
{
	if (hInvSource == '' || hInvSource == null){
		hInvSource = document.all.Cbo_InvSource.value;
	}
	var snapShotDate = document.all.Cbo_SnapShot.value;
	var parentFl  = document.all.Chk_ParentFl.checked;
	windowOpener("/GmARSummaryServlet?hAction=LoadARDetail&hRefid="+hRefid+"&hReportType="+hReport+"&hInvSource="+hInvSource+"&Chk_ParentFl="+parentFl+"&hFromDay="+hfromDay+"&hToDay="+htoDay+"&hDivId="+hDivId+"&Cbo_SnapShot="+snapShotDate,"INVPOP","resizable=yes,scrollbars=yes,top=300,left=300,width=670,height=200");
	// To Add StatusBar and Header in the pop-up window
	//	windowOpener("/GmARSummaryServlet?hAction=LoadARDetail&hAccountID="+hAccountId+"&hReportType="+hReport,"INVPOP","toolbar,location,directories,status,menubar,resizable=yes,scrollbars=yes,top=300,left=150,width=850,height=400");
}

function fnGo()
{
	var form = document.frmARSummary;
	form.Btn_Load.disabled=true; // To disable the Load button
	var interval = TRIM(form.Txt_Interval.value);
	var throughval = TRIM(form.Txt_Through.value);
	var re = /(30|90|180|360)/i;
	var found = interval.match(re);
	if (interval == '' && throughval != '')
	{
		Error_Details(message_accounts[98]);
	}
	if (interval != '' && throughval == '')
	{
		Error_Details(message_accounts[99]);
	}
	if (interval != ''&& (found == null || isNaN(interval)))
	{
		Error_Details(message_accounts[98]);
	}
	if ((TRIM(throughval) != '' && isNaN(throughval)) || (throughval>0 && throughval<90))
	{
		Error_Details(message_accounts[99]);
	}
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		form.Btn_Load.disabled=false;
		return false;
	}else{
		fnStartProgress();
		fnReloadSales("ReLoadAR");
	}
}

// Function resets the lists and the selections
function fnReset()
{	
	var form = document.frmARSummary;
	form.Txt_PaySinceDate.value='';
	form.Cbo_CollectorId.value='0';
	form.Cbo_AccId.value = '0';
	form.searchCbo_AccId.value = '';
	form.Txt_AccId.value = '';
	form.Cbo_AccGrp.value = '0';
	form.Cbo_Division.value = '0';
	form.Cbo_Category.value = '0';
	if(arRptByDealerFlag=='YES'){
		form.Cbo_InvSource.value = '26240213';
	}
	else{
		form.Cbo_InvSource.value = '50255';
	}

	form.Txt_Interval.value = '';
	form.Txt_Through.value = '';
	
}

//uncheck all the check box in the group
function uncheckAll(field)
{
	for (i = 0; i < field.length; i++)
	{
		field[i].checked = false ;
	}
}


// Function to get the checked values from the object passed
function fnGetCheckedValues(obj)
{
	var str = '';
	var arr = obj;
	if (obj)
	{
		var arrlen = arr.length;
		if (arrlen > 0)
		{
			for (var i=0;i< arrlen;i++ )
			{
				if (arr[i].checked == true)
				{
					str = str + arr[i].id + ',';
				}
			}
		
			if (str != '')
			{
				str = str.substr(0,str.length-1);
			}
		}
		else
		{
			str = arr.id;
		}
	}

	return str;
}

function fnDownloadXLS(){
	if(parentFl == 'false'){
		fnDownloadRepXLS();
	}
	else{
		fnDownloadParXLS();
	}
}
// As the Grid loaded for Rep account and Parent Account is different, the XLS Download function should also reflect it.
function fnDownloadRepXLS()
{

	gridObj.toExcel('/phpapp/excel/generate.php');

	return false;
}

// As the Grid loaded for Rep account and Parent Account is different, the XLS Download function should also reflect it.
function fnDownloadParXLS()
{

	gridObj.toExcel('/phpapp/excel/generate.php');

	return false;
}