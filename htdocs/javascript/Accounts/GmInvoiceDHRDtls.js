function fnReload()
{     
	//used to throw validation when none of the filters are selected
	fnValidateInput();
	//	alert(document.frmInvoiceDHR.missingSageVenIds.value);
	var objFromDT = document.frmInvoiceDHR.invoiceDateFrom.value;
	var objToDT = document.frmInvoiceDHR.invoiceDateTo.value;
	// check correct month format entered
	CommonDateValidation(document.frmInvoiceDHR.invoiceDateFrom, format, message_accounts[141]);
    CommonDateValidation(document.frmInvoiceDHR.invoiceDateTo, format, message_accounts[142]);
	
    // Validate Order From/To Dates in proper order
	if (dateDiff(objFromDT, objToDT, format) < 0)
	{
		Error_Details(message_accounts[143]);
	}
	
	var DlFromDT = document.frmInvoiceDHR.downloadDateFrom.value;
	var DlToDT = document.frmInvoiceDHR.downloadDateTo.value;
	// check correct month format entered
		
    CommonDateValidation(document.frmInvoiceDHR.downloadDateFrom, format, message_accounts[144]);
    CommonDateValidation(document.frmInvoiceDHR.downloadDateTo, format, message_accounts[145]);
	
	// Validate Order From/To Dates in proper order
	if (dateDiff(DlFromDT, DlToDT, format) < 0)
	{
		Error_Details(message_accounts[146]);
	}
	 if (document.frmInvoiceDHR.invoiceType.value=="-1")
 	{
	 fnValidateTxtFld('invoiceDateFrom',lblInvDtFrm);
	 fnValidateTxtFld('invoiceDateTo',lblInvDtTo);
	}
  	 if (document.frmInvoiceDHR.invoiceType.value=="30" )
	 {
		 fnValidateTxtFld('downloadDateFrom',lblDownldDtFrm);
		 fnValidateTxtFld('downloadDateTo',lblDownldDtTo); 
		}
	if (ErrorCount > 0)  
					{
						Error_Show();
						Error_Clear();
						return false;
									} 
	document.frmInvoiceDHR.strOpt.value = "reload"; 
	if(document.frmInvoiceDHR.invoiceOper.value != "0")
	{	
 	document.frmInvoiceDHR.operatorSign.value=frmInvoiceDHR.invoiceOper.options[frmInvoiceDHR.invoiceOper.selectedIndex].text;
 	}
	//BUG-4241 Choose one, Invoice Entered both invoice type of options, the value is '0'.
	//From left link, the type value is NULL. so it takes default value as Choose one.
	//So while Reloading, the type value is '0' and it selects Invoice Entered status also it filters the report by this status. 
	//To differentiate this two invoice types making the invoiceType value as '' when it is Choose one.
	var option = document.frmInvoiceDHR.invoiceType.options[document.frmInvoiceDHR.invoiceType.selectedIndex];
	if(option.id == '0'){   //For choose one the Option ID value is 0 <option value=0 id=0 >[Choose One]</option>. other options this ID is NULL
		 document.frmInvoiceDHR.invoiceType.value ='';
	}			
	document.frmInvoiceDHR.Load.disabled = true;
	fnStartProgress("Y");
	document.frmInvoiceDHR.submit();   
}

function fnValidateInput(){
	var vendId = document.frmInvoiceDHR.vendorId.value;
	var po = TRIM(document.frmInvoiceDHR.poID.value);
	var invType = document.frmInvoiceDHR.invoiceType.value;
	var invFrmDate = TRIM(document.frmInvoiceDHR.invoiceDateFrom.value);
	var invToDate =  TRIM(document.frmInvoiceDHR.invoiceDateTo.value);
	var dwnFrmDate = TRIM(document.frmInvoiceDHR.downloadDateFrom.value);
	var dwnToDate =  TRIM(document.frmInvoiceDHR.downloadDateTo.value);
    var invTypeOption = document.frmInvoiceDHR.invoiceType.options[document.frmInvoiceDHR.invoiceType.selectedIndex];

	if(invTypeOption.id =='0' && vendId=='0' && po =='' && invFrmDate=='' && invToDate=='' &&  dwnFrmDate=='' && dwnToDate == '')
		{
			Error_Details(message_accounts[147]);
		}

}
 
 function fnInvcDetails(strPaymentID)
 {   
      windowOpener("/gmAPSaveInvoiceDtls.do?strOpt=viewInvc&hpaymentId="+strPaymentID,"viewInvc","resizable=yes,scrollbars=yes,top=250,left=300,width=900,height=500");
 }
 function fnViewPO(val)
 {
 	windowOpener("/GmPOServlet?hAction=ViewPO&hCloseFlag=Y&hPOId="+val,"PO","resizable=yes,scrollbars=yes,top=150,left=50,width=770,height=600");
 }
  
  