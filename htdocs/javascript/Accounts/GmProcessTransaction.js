//When click load button, it will call this function and when press bar code image.

function fnLoad(){	
	fnValidateTxtFld('strRefID',lblRefId);
	fnValidateDropDn('strSource',lblSource);
	if (ErrorCount > 0){
			Error_Show();
			Error_Clear();
			return false;
	}	
	var refsrc =  document.frmProcessTransaction.strRefID.value;
	if(refsrc.indexOf("$")>0){
		var arrRefsrc = refsrc.split("$");
		document.frmProcessTransaction.strRefID.value =arrRefsrc[0];
	}
	document.frmProcessTransaction.strOpt.value = "Reload";
	fnStartProgress();
	document.frmProcessTransaction.submit();
}

// When onload or left link click that time will call default set focus in Ref Id text box.
function fnSetFocus()
{
	var txtRefId = document.frmProcessTransaction.strRefID;
	var strOptVal = document.frmProcessTransaction.strOpt.value;
	if(txtRefId.value == ''){
		txtRefId.focus();
		txtRefId.select();
	}
	// if reload then, auto focus to iframe.
	if(strOptVal =='Reload'){
		var iframeFormName = document.getElementsByName("myFrame")[0].contentWindow.document.getElementsByTagName("form")[0];   
		if(iframeFormName !=undefined && iframeFormName !=null){
			// to set the focus for the iframe (used to fire the enter key event in form).
			document.getElementsByName("myFrame")[0].contentWindow.focus();
			if(iframeFormName.name =='frmInvoice'){
				var batchBtn = document.getElementsByName("myFrame")[0].contentWindow.document.getElementById("Btn_Submit");
			    if (batchBtn != null && batchBtn !=undefined)
			    {
			    	// to focus the batch button.
					document.getElementsByName("myFrame")[0].contentWindow.document.frmInvoice.Btn_Submit.focus();

				}
			}
			
		} //end If Iframe Check
	} //end If strOptVal
}

// When do scan in bar code that time will call this function.
function fnCallGo(){	
	if(event.keyCode == 13)
	{	 
		var rtVal = fnLoad();
		if(rtVal == false){
			if(!e) var e = window.event;

	        e.cancelBubble = true;
	        e.returnValue = false;

	        if (e.stopPropagation) {
	                e.stopPropagation();
	                e.preventDefault();
	        }
		}
	}	
}
