// click on load button - this function will call
function fnGo() {
	
	document.frmAccount.action = "/GmPendingInvoiceServlet";
	var statusType = document.frmAccount.Cbo_Type.value;
	var parentFl  = document.frmAccount.Chk_ParentFl.checked;
	//All and closed invoice validation
	if(statusType == '0' || statusType == '2'){
		if((document.frmAccount.Cbo_AccId.value == '' || document.frmAccount.Cbo_AccId.value == '0') && document.frmAccount.Txt_PO.value == '' 
			&& document.frmAccount.Txt_InvoiceID.value == '' && document.frmAccount.Txt_OrderID.value == ''){
				var dateRange = dateDiff(document.frmAccount.Txt_FromDate.value,document.frmAccount.Txt_ToDate.value,format);
				if(isNaN(dateRange) || dateRange > 30 || dateRange < 0){
					Error_Details(message_accounts[177]);
				}
			}
	}
	
	if (ErrorCount > 0){
			Error_Show();
			Error_Clear();
			//return false;
	}else{
		fnStartProgress("Y");
		document.frmAccount.hAction.value = "GO";
		document.frmAccount.submit();
	}
	
}

function fnCallInv(invid) {
	if ((document.frmAccount.hSource.value != 50255) &&  (document.frmAccount.hSource.value != 50253) && ((document.frmAccount.hSource.value != 26240213)))  {
		Error_Details(message_accounts[178]);
		Error_Show();
		Error_Clear();

	} else {
		document.frmAccount.action = "/GmInvoiceServlet";
		document.frmAccount.hInv.value = invid;
		document.frmAccount.hAction.value = "PAY";
		document.frmAccount.submit();
	}
}

function Toggle(val) {
	var obj = eval("document.all.div" + val);
	var trobj = eval("document.all.tr" + val);
	var tabobj = eval("document.all.tab" + val);

	if (obj.style.display == 'none') {
		obj.style.display = '';
		trobj.className = "ShadeRightTableCaptionBlue";
		// tabobj.style.background="#c6e6fe";
		// tabobj.style.background="#d7ecfd";
		tabobj.style.background = "#ecf6fe";
	} else {
		obj.style.display = 'none';
		trobj.className = "";
		tabobj.style.background = "#ffffff";
	}
}

function fnPrintVer() {
	frm = document.frmAccount;
	var fromdt = frm.Txt_FromDate.value;
	var todt = frm.Txt_ToDate.value;
	var parentFl  = document.frmAccount.Chk_ParentFl.checked;
	var accCurrID = document.frmAccount.Cbo_Comp_Curr.value;
	var dateTypeVal = document.frmAccount.Cbo_DateType.value;
	parentFl = parentFl?"on":"";
	windowOpener(
			'/GmPendingInvoiceServlet?hMode=Excel&hAction=' + actionVal
					+ '&Cbo_InvSource=' + invSourceVal + '&Cbo_AccId=' + accVal
					+ '&Txt_FromDate=' + fromDtVal + '&Txt_ToDate=' + toDtVal
					+ '&Cbo_Type=' + statusTypeVal + '&Txt_PO=' + custPOVal
					+ '&Txt_InvoiceID=' + invoiceVal + '&Txt_OrderID=' + orderVal+'&Chk_ParentFl='+parentFl
					+ '&Cbo_Comp_Curr='+accCurrID + '&Cbo_DateType='+dateTypeVal,
			"Excel",
			"menubar=yes,resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnLoad1() {
	document.frmAccount.Cbo_Type.value = statusTypeVal;
	//we implement the auto complete functionality - so, not required to call below function.
	fnDispatchLoad();
	if(parAccfl == 'false'){
		document.frmAccount.Chk_ParentFl.checked = false;
	}
}
function fnOpenInvLog(id) {
	windowOpener("/GmCommonLogServlet?hType=1201&hID=" + id, "PrntInv",
			"resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
}

// This function used to selected invoices - Send mail
function fnSendEmail() {
	var rowCount = document.frmAccount.hCount.value;
	var inputStr = '';
	var emailVersion = '';

	for ( var i = 0; i < rowCount; i++) {
		obj = eval("document.frmAccount.Chk_Mail" + i);

		if (obj != null && obj.checked == true) {
			emailVersion = eval("document.frmAccount.hEmailVersion" + obj.value);
			inputStr = inputStr + obj.value + ',' + emailVersion.value + '|';
		}
	}
	if (inputStr == '') {
		Error_Details(message_accounts[179]);
	}

	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	fnStartProgress("Y");
	document.frmAccount.action = '/GmPendingInvoiceServlet?hInputStr='
			+ inputStr + '&hMode=SendMail&hAction=' + actionVal
			+ '&Cbo_InvSource=' + invSourceVal + '&Cbo_AccId=' + accVal
			+ '&Txt_FromDate=' + fromDtVal + '&Txt_ToDate=' + toDtVal
			+ '&Cbo_Type=' + statusTypeVal+ '&Txt_PO=' + custPOVal
			+ '&Txt_InvoiceID=' + invoiceVal + '&Txt_OrderID=' + orderVal;
	document.frmAccount.submit();
}
// this function used to export the files
function fnExportFile(fileID) {
	var uploadDir = "BATCH_INVOICE";
	document.frmAccount.action = "/GmCommonFileOpenServlet?sId=" + fileID
			+ "&uploadString=" + uploadDir;
	document.frmAccount.submit();
}

// this function used to export the RTF files
function fnExportRTFFile(invID, fileID) {
	var uploadDir = "UPLOAD_INVOICE";
	if (fileID != '') {
		document.frmAccount.action = "/GmCommonFileOpenServlet?sId=" + fileID
				+ "&uploadString=" + uploadDir;
	} else {
		document.frmAccount.action = "/GmInvoiceInfoServlet?hInv=" + invID
				+ "&hAction=Print&type=saveRTFfile";
	}
	document.frmAccount.submit();
}

function fnPrintInvoice(varinv) {
	windowOpener('/GmInvoiceInfoServlet?hAction=Print&hInv=' + varinv,
			"PrntInv",
			"resizable=yes,scrollbars=yes,top=150,left=200,width=750,height=500");
}