function fnSubmit()
{
	fnValidateTxtFld('shipName','Name');
	fnValidateTxtFld('shipAdd1','Address 1');
	fnValidateTxtFld('shipCity','City');
	fnValidateDropDn('shipState','State');
	fnValidateDropDn('shipCountry','Country');
	fnValidateTxtFld('shipZip','Zip/Postal Code');
	fnValidateDropDn('carrier','Preferred Carrier');
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	document.frmAccountShipSetup.strOpt.value='save';
	fnStartProgress();
	document.frmAccountShipSetup.submit();
}

function fnClose(){
	window.close();
}
