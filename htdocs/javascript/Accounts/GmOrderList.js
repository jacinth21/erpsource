// Click on Load button this function will call
function fnSubmit()
{
	var dateFormat = document.frmAccount.hDateFmt.value;
	var parentFl  = document.frmAccount.Chk_ParentFl.checked;
	CommonDateValidation(document.frmAccount.Txt_FromDate,dateFormat,message_accounts[93]+ message[611]);
	CommonDateValidation(document.frmAccount.Txt_ToDate,dateFormat,message_accounts[94]+ message[611]);
	
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	} else {
		document.frmAccount.hAction.value = "Reload";
		fnStartProgress('Y');
		fnReloadSales(); // Call the Action in the Filters jsp
		//document.frmAccount.submit();
	}
    
}
//Click on Invoice ID this function will call
function fnPrintInvoice(varinv)
{
windowOpener('/GmInvoiceInfoServlet?hAction=Print&hInv='+varinv,"PrntInv","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=750");
}
//Click on Order ID this function will call
function fnPrintPack(val)
{
	windowOpener('/GmEditOrderServlet?hMode=PrintPack&hOrdId='+val,"Pack","resizable=yes,scrollbars=yes,top=150,left=150,width=780,height=610");	
}
//Click on Invoice ID this function will call
function fnPrintCreditMemo(val)
{
	windowOpener('/GmReportCreditsServlet?hAction=PrintVersion&hOrderId='+val,"Credit","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}
//Click on Invoice ID this function will call
function fnPrintCashAdjustMemo(val)
{
	windowOpener('/GmReportCreditsServlet?hAction=PrintCashAdj&hOrderId='+val,"Credit","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}
// enter key then load the reports
function fnEnterPress(evt){
	evt = (evt) ? evt : window.event
	if(evt.keyCode==13){
		fnSubmit();
	}
}
//function is used ,when the text box is changed,account id will come in account dorpdown.
function fnDisplayAccID()
{		
	var accID = document.frmAccount.Cbo_AccId.value;
	if (accID != '0' && accID != null){
		document.frmAccount.Txt_AccId.value = accID ;
		fnSetAccCurrSym(accID,document.frmAccount.Cbo_Comp_Curr);	
	}else{
		document.frmAccount.Txt_AccId.value = '';
		fnResetAccCurrSymb();
	}
}
//function is used ,when the account dorpdown is changed,account id will come in text box.
function fnDisplayAccNm()
{
	var txt_AccId = TRIM(document.frmAccount.Txt_AccId.value);
	if (txt_AccId != ''){

		var ajaxUrl = fnAjaxURLAppendCmpInfo('/GmOrderAjaxServlet?opt=GETACC&acc=' +  encodeURIComponent(txt_AccId) + '&ramdomId=' + Math.random());
		dhtmlxAjax.get(ajaxUrl, fnLoadAccAjax);
		
		fnSetAccCurrSym(txt_AccId,document.frmAccount.Cbo_Comp_Curr);
	}else{
		fnResetAccCurrSymb();
	}
	//Cbo_AccId.DOMelem_input.focus();

	if(txt_AccId = '')
	{
		document.frmAccount.Txt_AccId.value = '';
		document.frmAccount.Cbo_AccId.value = '';
		document.frmAccount.searchCbo_AccId.value = '';
		Error_Details(message_accounts[176]);
		Error_Show();
		Error_Clear();
		return false;
	}
}


function fnResetAccCurrSymb(){
	var accCurrObj = document.frmAccount.Cbo_Comp_Curr;
	for ( var i = 0; i < accCurrObj.options.length; i++ ) {
        if ( accCurrObj.options[i].id == compCurrSign ) {
        	accCurrObj.options[i].selected = true;
            return;
        }
    }
}

function fnSetAccCurrSym(accID,targetObj){
	   var dhxAccCurrUrl = fnAjaxURLAppendCmpInfo('/GmOrderAjaxServlet?opt=GETACCTCURR&acc='+accID+'&ramdomId=' + Math.random());
    dhtmlxAjax.get(dhxAccCurrUrl,function(loader){
    	   var accCurrDtl;
    	   var response = loader.xmlDoc.responseText;    
              if((response != null || response != '') ){
            	  accCurrDtl =response.split("|");
            	//  currencyFmt = accCurrDtl[1];
            	  if(targetObj != undefined){
            		  targetObj.value =accCurrDtl[0];
            	  }
              }
       });
}

function initGridData(divRef,gridData)
{
	var footer=new Array();
	footer[0]="";
	footer[1]="";
	footer[2]="";
	footer[3]="";
	footer[4]="";
	footer[5]="";
	footer[6]="";
	footer[7]="";
	footer[8]="";
	footer[9]= "";
	footer[10]="";
	footer[11]="";
	footer[12]=message_accounts[180];
	footer[13]="<b>{#stat_total}<b>";
	footer[14]="<b>{#stat_total}<b>";
	footer[15]="<b>{#stat_total}<b>";
	footer[16]="";
	if(strOrderType!='103184'){
		footer[17]="";
	}
	if(strOrderType=='103186'){
		footer[18]="";
	}
	
		footer[19]="";
	
	
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);
	if(strOrderType=='103184'){
		gObj.enableTooltips("true,true,true,true,true,true,true,true,true, true,true,true,true,true,true,true,true");
		gObj.attachHeader("#select_filter,#text_filter,#text_filter,#select_filter,#text_filter,#select_filter,#select_filter,#text_filter,#select_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#select_filter");
	}else{
		gObj.enableTooltips("true,true,true,true,true,true,true,true,true, true,true,true,true,true,true,true,true,true");
		gObj.attachHeader("#select_filter,#text_filter,#text_filter,#select_filter,#text_filter,#select_filter,#select_filter,#text_filter,#select_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#select_filter");
	}
	
	if(strOrderType=='103186'){
		gObj.enableTooltips("true,true,true,true,true,true,true,true,true, true,true,true,true,true,true,true,true,true,true");
		gObj.attachHeader("#select_filter,#text_filter,#text_filter,#select_filter,#text_filter,#select_filter,#select_filter,#text_filter,#select_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#select_filter,#text_filter,");
	}
	gObj.attachFooter(footer);
	gObj.init();	
	gObj.enablePaging(true, 100, 10, "pagingArea", true);
	gObj.setPagingSkin("bricks");
	gObj.loadXMLString(gridData);	
	return gObj;	
}

function fnExport()
{	
	gridObj.toExcel('/phpapp/excel/generate.php');
}


//Description : Load the account name information
function fnLoadAccAjax(loader) {
	var errFlag = false;
	var xmlDoc;
	var pnum;
	var acname = '';
	
	var response = loader.xmlDoc.responseXML;
	if (response != null) {
		pnum = response.getElementsByTagName("acc");
		for (var x = 0; x < pnum.length; x++) {
			acname = parseXmlNode(pnum[x].childNodes[9].firstChild);
		}
		if (acname != null && acname != '') {
			document.frmAccount.searchCbo_AccId.value = acname;
			document.frmAccount.Cbo_AccId.value = document.frmAccount.Txt_AccId.value;
		}
	}
	
}


function fnOpenDOFile(strOrderId){
	
	if(strOrderId !=''){

		windowOpener("/gmDODocumentUpload.do?method=loadOrderUpload&strOpt=Load&haction=View&uploadTypeId=26240412&strOrderId="+strOrderId,"DO","resizable=yes,scrollbars=yes,top=150,left=200,width=1000,height=400");
		
}
}