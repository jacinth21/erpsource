// Description : This function to load the grid data

function initGridData(divRef,gridData)
{
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.init();	
	gObj.loadXMLString(gridData);	
	return gObj;	
}

function fnOnPageLoad() {
	gridObj = initGridData('fieldSalesLockRpt', objGridData);
	var gridrows = gridObj.getAllRowIds(",");
	var fa_id = '';
	var dataDownloadfl =false;
	check_rowId = gridObj.getColIndexById("check");
	fs_rowId = gridObj.getColIndexById("fieldSales");
	tag_rowId = gridObj.getColIndexById("tagQty");
	auditor_rowId = gridObj.getColIndexById("auditor");
	status_rowId = gridObj.getColIndexById("status");

	fa_id = document.frmFieldAuditSetup.auditId.value;
	if(fa_id == 0){
		document.frmFieldAuditSetup.auditName.value = '';
		document.frmFieldAuditSetup.primaryAuditorId.value = 0;
		document.frmFieldAuditSetup.auditStartDt.value = '';
	}
	if(Au_status ==''){
		eval("document.all.btn_Lock").disabled = true;
		eval("document.all.btn_void").disabled = true;
	}else if(Au_status !=''){
		document.frmFieldAuditSetup.regionId.value = 0;
		document.frmFieldAuditSetup.regionId.disabled = true;
		document.frmFieldAuditSetup.btn_Load.disabled = true;
	}
	if(Au_status =='18504'){ //Closed
		document.frmFieldAuditSetup.auditName.disabled = true;
		document.frmFieldAuditSetup.primaryAuditorId.disabled = true;
		document.frmFieldAuditSetup.auditStartDt.disabled = true;
		document.frmFieldAuditSetup.setIds.readOnly = true;
	}
	
	if (gridrows.length == 0) {
		for (i = 0; i <= 3; i++) {
			addRow();
		}
	}

	gridObj.attachEvent("onEditCell", doOnCellEdit);
	gridObj.attachEvent("onCheckbox", doOnCheck);
	gridObj.forEachRow(function(rowId) {
		status_id = gridObj.cellById(rowId, status_rowId).getValue();
		tag_qty = gridObj.cellById(rowId, tag_rowId).getValue();
		gridObj.cellById(rowId, status_rowId).setDisabled(true);
		if (status_id !='') {
			gridObj.cellById(rowId, fs_rowId).setDisabled(true);
		}
		if (status_id !='' && status_id >= 18522) { // Open
			gridObj.cellById(rowId, auditor_rowId).setDisabled(true);
		}
		if(status_id >= 18524 && status_id != 18529){ //Data Downloaded && Voided
			dataDownloadfl = true;
		}
		if(status_id == 18529 || status_id == 18528){ // Voided or Closed
			gridObj.cellById(rowId, check_rowId).setDisabled(true);
		}
	
	}); 
	if(dataDownloadfl){
		document.frmFieldAuditSetup.setIds.readOnly = true;
	}
	
	gridObj.enableTooltips("true,true,true,true,true");
	gridObj.attachHeader("<input type='checkbox' value='no' name='selectFSAll' onClick='javascript:fnChangedFilter(this);'/>,"+'#select_filter,#text_filter,#select_filter,#select_filter');
}
//Description : This function to fire the cell edit event. To get the Tag qty
function doOnCellEdit(stage, rowId, cellInd, nValue, oValue) {
	rowID = rowId;
	if (cellInd == fs_rowId) {
		if (stage == 2 && nValue != '0' && nValue != oValue) {
			fngetTagQty(nValue);
		}
		if (nValue == '0') {
			gridObj.cellById(rowId, tag_rowId).setValue("");
		}

	}
	return true;
}
//Description : This function to load the audit details.
function fnReload() {
	document.frmFieldAuditSetup.strOpt.value = "reload";
	fnStartProgress();
	document.frmFieldAuditSetup.submit();
}
//Description : This function to load grid data (based on the region - get the field sales details).
function fillFieldSalesLockData(loader) {
	var response = loader.xmlDoc.responseXML;
	if(response!= null);
	 {          
		 gridObj.clearAll();
		 gridObj.parse(response);
		 gridObj.attachHeader("<input type='checkbox' value='no' name='selectFSAll' onClick='javascript:fnChangedFilter(this);'/>,"+'#select_filter,#text_filter,#select_filter,#select_filter');
		 var rowcount =  gridObj.rowsCol.length;
		 if(rowcount == '0'){
			 gridObj.setColWidth(0, "0");
			 gridObj.addRow(1,["","<font color='red'; size='2';>"+message_accounts[44]+"</font>","","","",""]);	
			 gridObj.setColWidth(4, "230");
		 }
	}
}
//Description : This function to load the region details.
function fnLoad(){
	fnValidateDropDn('regionId', message_accounts[207]);
	var region = document.frmFieldAuditSetup.regionId.value;
	var pAuditor = document.frmFieldAuditSetup.primaryAuditorId.value;
	if (ErrorCount > 0) {
		document.frmFieldAuditSetup.inputString.value = '';
		Error_Show();
		Error_Clear();
		return false;
	}else{
		document.frmFieldAuditSetup.btn_Load.disabled = true;
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmFieldAuditSetup.do?method=getFieldSalesRegionDtls&regionId='+region+'&primaryAuditorId='+pAuditor+'&ramdomId='+new Date().getTime());
		var loader = dhtmlxAjax.getSync(ajaxUrl);
		fillFieldSalesLockData(loader);
		
		gridObj.forEachRow(function(rowId) {
			status_id = gridObj.cellById(rowId, status_rowId).getValue();
			tag_qty = gridObj.cellById(rowId, tag_rowId).getValue();
			gridObj.cellById(rowId, status_rowId).setDisabled(true);
			dist_id = gridObj.cellById(rowId, fs_rowId).getValue();
			var checkInactive = get(dist_id); // check the field sales active flag
			if(tag_qty =='0' || checkInactive != '' ){
				gridObj.cellById(rowId, check_rowId).setChecked(false);
			}else{
				gridObj.cellById(rowId, check_rowId).setChecked(true);
			}
		}); 
		document.frmFieldAuditSetup.btn_Load.disabled = false;
	}
}
//Description : This function to change the auditor name (grid) based on the primary auditor.
function fnChangeAuditor(obj) {
	var pAuditor = obj.value;
	
	gridObj.forEachRow(function(rowId) {
		fieldSales_id = gridObj.cellById(rowId, fs_rowId).getValue();
		auditor_id = gridObj.cellById(rowId, auditor_rowId).getValue();
		status_id = gridObj.cellById(rowId, status_rowId).getValue();
		if (auditor_id == '0' || status_id == '') {
			gridObj.cellById(rowId, auditor_rowId).setValue(pAuditor);
		}
	});
}
//Description : This function used to get the tag qty.
function fngetTagQty(dist_id) {
	if(dist_id !=0){
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmFieldAuditSetup.do?method=fetchTagQty&distID=' + dist_id + '&ramdomId=' + Math.random());
		dhtmlxAjax.get(ajaxUrl, fnLoadTagQty);
	}else{
		gridObj.cellById(rowID, tag_rowId).setValue('');
		gridObj.cellById(rowID, check_rowId).setChecked(false);
	}
	
}
//Description : This function used to load the tag qty on selected field sales
function fnLoadTagQty(loader) {
	var response = loader.xmlDoc.responseText;
	if (response != null) {
		dist_id = gridObj.cellById(rowID, fs_rowId).getValue();
		//var checkInactive = get(dist_id); -- check the field sales active flag
		if(response > 0){
			gridObj.cellById(rowID, tag_rowId).setValue(response);
			gridObj.cellById(rowID, check_rowId).setChecked(true);
		}else{
			gridObj.cellById(rowID, tag_rowId).setValue(response);
			gridObj.cellById(rowID, check_rowId).setChecked(false);
		}		
	}
}
//Description : This function used to validate the Audit name
function fnvalidateAuditName() {
	var faName = TRIM(document.frmFieldAuditSetup.auditName.value);
	var validText = document.getElementById("validateText");
	var validName = eval("document.all.validateName");
	var selected_fa_name = document.frmFieldAuditSetup.auditId.options[document.frmFieldAuditSetup.auditId.selectedIndex].text;

	if (selected_fa_name != faName) {
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmFieldAuditSetup.do?method=validateAuditName&auditName='+ faName + '&ramdomId=' + Math.random());
		dhtmlxAjax.get(ajaxUrl, fnCheckAuditName);
	}else{
		validName.style.display = 'none';
		validText.innerHTML ="";
	}
}
// check the Audit Name 
function fnCheckAuditName(loader) {
	var response = loader.xmlDoc.responseText;
	var validText = document.getElementById("validateText");
	if (response != null) {
		var validName = eval("document.all.validateName");
		if (response == '0') {
				validName.style.display = 'block';
				validName.src = '/images/success.gif';
				validText.innerHTML = message_accounts[45];
			}else{
				validName.style.display = 'block';
				validName.src = '/images/error.gif';
				validText.innerHTML = message_accounts[46];
			}
		}
}
//Description : This function used to save the audit details.
function fnSave() {
	var inputStr = '';
	fnValidateTxtFld('auditName', message_accounts[196]);
	fnValidateDropDn('primaryAuditorId', message_accounts[197]);
	fnValidateTxtFld('auditStartDt', message_accounts[198]);
	var objAuditStartDt = document.frmFieldAuditSetup.auditStartDt;
	CommonDateValidation(objAuditStartDt,format,message_accounts[47]+ message[611]);
	fnCreateString('SAVE');

	if (ErrorCount > 0) {
		document.frmFieldAuditSetup.inputString.value = '';
		Error_Show();
		Error_Clear();
		return false;
	} else{ 
		var set_ids = document.frmFieldAuditSetup.setIds.value;
		if(set_ids !=''){
			set_ids  = set_ids + ',';
		}
		document.frmFieldAuditSetup.setIds.value =set_ids;
		document.frmFieldAuditSetup.strOpt.value = 'SAVE';
	    //document.body.style.cursor = 'wait'; 
		document.frmFieldAuditSetup.action = '/gmFieldAuditSetup.do?method=saveFieldAuditDetails';
		eval("document.all.btn_Save").disabled = true;
		document.frmFieldAuditSetup.submit();
	}
}
//Description : This function used to Lock the audit details.
function fnLock() {
	var inputStr = '';
	fnValidateTxtFld('auditName', message_accounts[196]);
	fnValidateDropDn('primaryAuditorId', message_accounts[197]);
	fnValidateTxtFld('auditStartDt', message_accounts[198]);
	//fnValidateTxtFld('auditEndDt', 'Audit End Date');
	var objAuditStartDt = document.frmFieldAuditSetup.auditStartDt;
	CommonDateValidation(objAuditStartDt,format,message_accounts[47]+ message[611]);
	fnCreateString('LOCK');

	inputStr = document.frmFieldAuditSetup.inputString.value;

	if(inputStr ==''){
		Error_Details(message_accounts[48]);
	}
	
	if (ErrorCount > 0) {
		document.frmFieldAuditSetup.inputString.value = '';
		Error_Show();
		Error_Clear();
		return false;
	} else if (confirm(message_accounts[49])){
		var set_ids = document.frmFieldAuditSetup.setIds.value;
		if(set_ids !=''){
			set_ids  = set_ids + ',';
		}
		document.frmFieldAuditSetup.setIds.value =set_ids;
		document.frmFieldAuditSetup.strOpt.value ='LOCK';
		document.frmFieldAuditSetup.action = '/gmFieldAuditSetup.do?method=saveFieldAuditDetails';
	    //document.body.style.cursor = 'wait'; 
		document.getElementById("AllButton").disabled = true;
		document.frmFieldAuditSetup.submit();
		eval("document.all.btn_Lock").disabled = true;
	}
}
/*Description : This function used to create a input string (Field sales).
 * Input string --  Field sales, Auditor id, status id
 * Example : 8^303018^18521|702^303084^18521|401^303018^18521|506^303018^18521|; // 18521 - Draft
 */
function fnCreateString(action) {
	var inputStr = "";
	var gridrowid;
	var set_ids = document.frmFieldAuditSetup.setIds.value;
	gridObj.forEachRow(function(rowId) {
		check_id = gridObj.cellById(rowId, check_rowId).getValue();
		auditor_id = gridObj.cellById(rowId, auditor_rowId).getValue();
		fieldSales_id = gridObj.cellById(rowId, fs_rowId).getValue();
		status_id = gridObj.cellById(rowId, status_rowId).getValue();
		tag_qty = gridObj.cellById(rowId, tag_rowId).getValue();
		
		if (check_id == 1) {
			if(action == 'SAVE' && status_id >='18522'){ //18522 - Open
				Error_Clear();
				Error_Details(message_accounts[50]);
				if(status_id =='18529'){ //18529 - Void
					Error_Clear();
					Error_Details(message_accounts[51]);
				}
			}else if(action == 'LOCK'){
				if(status_id ==''){
					Error_Clear();
					Error_Details(message_accounts[52]);
				}else if(status_id !='18521'){ //18521 - Draft
					Error_Clear();
					Error_Details(message_accounts[53]);
				}
				if(status_id =='18529'){//18529 - Void
					Error_Clear();
					Error_Details(message_accounts[51]);
				}
			}
			if(tag_qty == 0){
				Error_Clear();
				Error_Details(message_accounts[54]);
			}
						
			if (fieldSales_id != '0' && auditor_id != '0') {
				inputStr += fieldSales_id + '^' + auditor_id + '^'+ status_id  + '|';
			}
		}
	});
	
	if(set_ids.length > 4000){
		Error_Details(message_accounts[55]);
	}
	
	if(inputStr.length > 4000){
		Error_Details(message_accounts[56]);
	}
	document.frmFieldAuditSetup.inputString.value = inputStr;
	
}

//Description : This function used to add a row(grid)
function addRow() {
	var dataUploadFl = false;
	gridObj.forEachRow(function(rowId) {
		status_id = gridObj.cellById(rowId, status_rowId).getValue();
		
		if(status_id >= 18525 && status_id !=18529){ //Data Uploaded && Voided
			dataUploadFl = true;
		}
	});
	
	if(dataUploadFl){
		Error_Details(message_accounts[57]);
	}
	
	if (ErrorCount > 0) {
		
		Error_Show();
		Error_Clear();
		//return false;
	}else{
		gridObj.addRow(gridObj.getUID(), '');
		var pAuditor = document.frmFieldAuditSetup.primaryAuditorId.value;
		var rows = gridObj.getUID();
		rows = rows -1;
		
		check_id = gridObj.cellById(rows, check_rowId).getValue();
		auditor_id = gridObj.cellById(rows, auditor_rowId).getValue();
		dist_id = gridObj.cellById(rows, fs_rowId).getValue();
		status_id = gridObj.cellById(rows, status_rowId).getValue();
				
		gridObj.cellById(rows, auditor_rowId).setValue(pAuditor);
		gridObj.cellById(rows, fs_rowId).setValue('0');
		gridObj.cellById(rows, status_rowId).setDisabled(true);
	}
	
}
//Description : This function used to remove a row(grid)
function removeSelectedRow() {
	var gridrows = gridObj.getSelectedRowId();
	var status_fl =false;
	var void_fl = false;
	if (gridrows != undefined) {
		var gridrowsarr = gridrows.toString().split(",");
		for ( var rowid = 0; rowid < gridrowsarr.length; rowid++) {
			gridrowid = gridrowsarr[rowid];
			fieldSales_id = gridObj.cellById(gridrowid, fs_rowId).getValue();
			auditor_id = gridObj.cellById(gridrowid, auditor_rowId).getValue();
			status_id = gridObj.cellById(gridrowid, status_rowId).getValue();
			if (status_id == '') {
				gridObj.deleteRow(gridrowid);
			}else if(status_id != '18529'){
				status_fl = true;
			}else{
				void_fl = true;
			}
		}
	}else {
		Error_Details(message_accounts[58]);
	}
	if(status_fl){
		Error_Details(message_accounts[59]);
	}
	if(void_fl){
		Error_Details(message_accounts[60]);
	}	
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		//return false;
		}
}

/*Description : This function used to void the selected filed sales.
 * Input string --  Field Audit id ,Field sales id
 * Example : 12^201|12,8|12,556|12,706|12,740 
 */
function fnVoid(){
	var void_str = '';
	var gridrows = gridObj.getChangedRows(",");
	var parReconFl = false;
	var voidFl = false;
	var emptyStatusFl = false;
	var auditId = document.frmFieldAuditSetup.auditId.value;
	var voidAudit = document.frmFieldAuditSetup.auditId.options[document.frmFieldAuditSetup.auditId.selectedIndex].text;
	gridObj.forEachRow(function(rowId) {
		check_id = gridObj.cellById(rowId, check_rowId).getValue();
		auditor_id = gridObj.cellById(rowId, auditor_rowId).getValue();
		fieldSales_id = gridObj.cellById(rowId, fs_rowId).getValue();
		status_id = gridObj.cellById(rowId, status_rowId).getValue();
		var fscombo = gridObj.getCombo(rowId);
		
		if (check_id == 1) {
			
			if(status_id == 18527){ // Partially Reconciled
				parReconFl = true;
			}
			
			 if(status_id == 18529){ //void
				voidFl = true;
			}
			 if(status_id ==''){
					emptyStatusFl = true;
				}
			void_str = void_str + auditId + '^' + fieldSales_id +'|';
		}
   });
	if(void_str ==''){
		Error_Details(message_accounts[61]);
	}
	if(parReconFl){
		Error_Details(message_accounts[62]);
	}
	if(voidFl){
		Error_Details(message_accounts[51]);
	}
	if(emptyStatusFl){
		Error_Details(message_accounts[63]);
	}
	if (ErrorCount > 0) {
		void_str = '';
		Error_Show();
		Error_Clear();
		return false;
	}else if (confirm(message_accounts[64])){  
		document.frmFieldAuditSetup.hTxnId.value = void_str;
		document.frmFieldAuditSetup.hTxnName.value = voidAudit;
		document.frmFieldAuditSetup.hCancelReturnVal.value = 'true';
		document.frmFieldAuditSetup.action ="/GmCommonCancelServlet";
		document.frmFieldAuditSetup.hAction.value = "Load";		
		document.frmFieldAuditSetup.hCancelType.value = 'VFADTL';
		document.frmFieldAuditSetup.hRedirectURL.value =  "\gmFieldAuditSetup.do?method=loadFieldAuditDetails&strOpt=reload&auditId="+auditId;
		document.frmFieldAuditSetup.hDisplayNm.value = "Field Audit Setup Screen";
		document.frmFieldAuditSetup.submit();
		eval("document.all.btn_void").disabled = true;
	}
}

function fnChangedFilter(obj){
	if(obj.checked)
	{		
		gridObj.forEachRow(function(rowId) {
			check_id = gridObj.cellById(rowId, check_rowId).getValue();
			auditor_id = gridObj.cellById(rowId, auditor_rowId).getValue();
			fieldSales_id = gridObj.cellById(rowId, fs_rowId).getValue();
			status_id = gridObj.cellById(rowId, status_rowId).getValue();

			if(status_id != 18529 && status_id != 18528){ // Voided - Closed
				gridObj.cellById(rowId, check_rowId).setChecked(true);
			}
	   });
	}else
	{
		gridObj.forEachRow(function(rowId) {
			check_id = gridObj.cellById(rowId, check_rowId).getValue();
			auditor_id = gridObj.cellById(rowId, auditor_rowId).getValue();
			fieldSales_id = gridObj.cellById(rowId, fs_rowId).getValue();
			status_id = gridObj.cellById(rowId, status_rowId).getValue();
			
			if(check_id ==1){
				gridObj.cellById(rowId, check_rowId).setChecked(false);
			}
			
	   });			
	}
}
//Description : This function to check the select all (master check box)
function doOnCheck(rowId, cellInd, stage) {
	rowID = rowId;
	if(cellInd == check_rowId) {
		fnSelectAllToggle('selectFSAll');
	}
	return true;
}
//Description : This function to check the select all check box
function fnSelectAllToggle( varControl){
	var objControl = eval("document.frmFieldAuditSetup."+varControl);

	var all_row = gridObj.getAllRowIds(",");
	var all_rowLen = all_row.length; // get the rows id length  
	var checked_row = gridObj.getCheckedRows(check_rowId);
	var void_row = '';
	var finalval;

	gridObj.forEachRow(function(rowId) {
		status_id = gridObj.cellById(rowId, status_rowId).getValue();
		if(status_id == 18529){ // Voided
			void_row = void_row + rowId+',';
		}
   });			

	finalval = eval(checked_row.length) + eval(void_row.length);
	if(all_rowLen == finalval){
		objControl.checked = true;// select all check box to be checked
	}else{
		objControl.checked = false;// select all check box un checked.
	}
}

function put(key,val) 
{ 
    var elementIndex = this.findIt(key); 
    if( elementIndex == (-1) ) 
    {
        this.keyArray.push( key ); 
        this.valArray.push( val ); 
    } 
    else 
    { 
        this.valArray[ elementIndex ] = val; 
    } 
} 
 
function get(key) 
{ 
    var result = null; 
    var elementIndex = this.findIt( key ); 
    if( elementIndex != (-1) ) 
    {    
        result = this.valArray[ elementIndex ];
    } 
    else {
    	result = "";
    }
    return result; 
}
function findIt(key) 
{ 
    var result = (-1); 
 
    for( var i = 0; i < this.keyArray.length; i++ ) 
    { 
        if( this.keyArray[ i ] == key ) 
        { 
            result = i; 
            break; 
        } 
    } 
    return result; 
} 

