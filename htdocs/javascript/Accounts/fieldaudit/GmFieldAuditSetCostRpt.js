function initGridData(divRef,gridData)
{
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);
	gObj.enableTooltips("true,true,true,true,true,true,true,true");
	gObj.attachHeader('#rspan,#text_filter,#text_filter,#numeric_filter,#text_filter,#text_filter,#text_filter,#text_filter');
	gObj.init();	
	gObj.enablePaging(true, 100, 10, "pagingArea", true, "infoArea");
	gObj.setPagingSkin("bricks");
	gObj.loadXMLString(gridData);	
	return gObj;	
}
function fnOnLoad() {
	if (objGridData != '') {
		gridObj = initGridData('SetCostReport',objGridData);
	}
}
function fnSetCostHistory(setid)
{		
		windowOpener("/GmPageControllerServlet?strPgToLoad=/gmAuditTrail.do&auditId=1048&txnId="+ setid, "","resizable=yes,scrollbars=yes,top=250,left=300,width=785,height=300,status=1");

}
function fnDownloadXLS()
{
		gridObj.toExcel('/phpapp/excel/generate.php');
	
}
	

