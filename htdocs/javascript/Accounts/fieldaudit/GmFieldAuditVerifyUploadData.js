//Description : This function will load the distributor data.
function fnReload()
{	
	if (document.frmFieldAuditEditUploadData.distId){		
		document.frmFieldAuditEditUploadData.submit();
	}
}

//Description : This function check the validation for dropdown & load the data.
function fnLoad()
{
	fnValidateDropDn('auditId',message_accounts[199]);
	
	if (ErrorCount > 0)  
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	document.frmFieldAuditEditUploadData.strOpt.value = "report";	
	fnStartProgress();
	document.frmFieldAuditEditUploadData.submit();
}

//Description : This function will call on selection of TagID & assign the value to hidden field.
function fnSetAuditEntryID(val)
{
	document.frmFieldAuditEditUploadData.hauditEntryID.value = val;
}

//Description : This will call click on submit button. Check the validation and pass the value to EditUpload screen.
function fnSubmit()
{
	val = document.frmFieldAuditEditUploadData.Cbo_Action.value;
	auditEntryID=document.frmFieldAuditEditUploadData.hauditEntryID.value;
	var auditId = document.frmFieldAuditEditUploadData.auditId.options[document.frmFieldAuditEditUploadData.auditId.selectedIndex].value;
	var distId = document.frmFieldAuditEditUploadData.distId.options[document.frmFieldAuditEditUploadData.distId.selectedIndex].value;
	var auditorId = document.frmFieldAuditEditUploadData.auditorId.options[document.frmFieldAuditEditUploadData.auditorId.selectedIndex].value;
	
	if (val == '0')
	{
		Error_Details(message[4000]);
	}
	if (auditEntryID =='')
	{
		Error_Details(message[4001]);
	}
	if (ErrorCount > 0)  
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	document.frmFieldAuditEditUploadData.action="/gmFieldAuditEditUploadData.do?method=editUploadData&strOpt=onload_edit_audit_tag&auditEntryID="+auditEntryID+"&hauditId="+auditId+"&hdistId="+distId+"&hauditorId="+auditorId;
	fnStartProgress();
	document.frmFieldAuditEditUploadData.submit();
}

function fnClose(){
	window.close();
}

