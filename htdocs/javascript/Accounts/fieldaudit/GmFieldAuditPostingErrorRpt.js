//Description : This function will load the distributor data.
function fnReload()
{	
	if (document.frmFieldAuditReport.distId){		
		document.frmFieldAuditReport.submit();
	}
}

//Discription: Function will call click on load button. 
function fnLoad()
{
	fnValidateDropDn('auditId',lblAuditList);
	fnValidateDropDn('statusId',lblStatus);
	
	if (ErrorCount > 0)  
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	document.frmFieldAuditReport.strOpt.value = "report";	
	fnStartProgress();
	document.frmFieldAuditReport.submit();
}
