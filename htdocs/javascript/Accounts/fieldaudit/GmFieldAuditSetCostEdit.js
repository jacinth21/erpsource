var hSetInputStr = "";
var hInputStr = "";
function fnSubmit()
{
	hInputStr='';
	hSetInputStr = '';
	var obj = document.frmFieldAuditSetCost.batchData.value;
	var objlen = obj.length;
	var strOpt = "";
	var objval = TRIM(document.frmFieldAuditSetCost.batchData.value);	
	objval = objval.replace(/\r\n/g,',');
	var mySplitval = objval.split(",");
	var count = mySplitval.length;
	var set_Id = '';
	var set_Cost = '';
	var err_Costs = '';
	var err_string = '';
	var dupSetids = '';
	var empty_cost = '';
	objval = objval +',';
	if (objlen == 0){
		Error_Details(message_accounts[38]);
		Error_Show();
		Error_Clear();
		return false;
	}	
	// The Following code helps to validate the entererd Batch Data Format, for example if user given 916.916.1520.30 means 
	// sholud through the validation as incorrect string format.The standard format is SETID,SETCOST,but this code will helps us if the user enter SETID.SETCOST formation.
	
	var batchData = TRIM(document.frmFieldAuditSetCost.batchData.value);
	batchData = batchData.replace(/\r/g,',');
	var objArray = batchData.split("\n");	
	for(var i=0;i<objArray.length;i++){
		if(objArray[i].indexOf(",") == -1){
			err_string = err_string +"<br>"+objArray[i]+"";
		}
	}	
	
	// looping the Enetered Batch data...
	for(i=0;i<count;i++){
		set_Id 	 = objval.substring(0,objval.indexOf(','));
		objval   = objval.substring((set_Id.length)+1,objval.length);
		set_Cost = objval.substring(0,objval.indexOf(','));
		objval   = objval.substring((set_Cost.length)+1,objval.length);
		set_Id   = TRIM(set_Id);
		set_Cost = TRIM(set_Cost);
		
	// forming the hSetInputStr,hInputStr Strings....
		if(set_Id !=''){			
			if(hSetInputStr.indexOf(set_Id+ ",")==-1)
			{
				hSetInputStr += (set_Id + ",");	
				if(set_Cost == ''){
					empty_cost = empty_cost +"<br>"+set_Id;
				}else{
				NumberValidation(set_Cost,'',2);
					if (ErrorCount > 0){
						err_Costs = err_Costs +"<br>"+set_Id+" - "+set_Cost+"";
						Error_Clear();
					}
				}
				hInputStr    = hInputStr + set_Id +'^'+ set_Cost +'|';
			}else
			{
				if(dupSetids.indexOf(set_Id+ ",")==-1)
				{
					dupSetids += (set_Id + ",");
								
				}
			}				
		}		
	}
	
	// Error message for Incorrect data entry..	
	if (err_string != ''){
		Error_Details(Error_Details_Trans(message_accounts[39],err_string));
		Error_Show();
		Error_Clear();
		return false;
	}
	
	// Error message for Empty Cost for a set..	
	if (empty_cost != ''){
		Error_Details(Error_Details_Trans(message_accounts[40],empty_cost));
		Error_Show();
		Error_Clear();
		return false;
	}
	
	// Error message for invalid Set Cost..
	if (err_Costs != ''){
		Error_Details(Error_Details_Trans(message_accounts[41],err_Costs));
		Error_Show();
		Error_Clear();
		return false;
	}
	
	// Error message for Duplicate Set Entries..
	if(dupSetids!='' && TRIM(dupSetids).length>1){
		dupSetids=dupSetids.substr(0,(TRIM(dupSetids).length-1));
		error_msg=Error_Details_Trans(message_accounts[42],dupSetids);	
		Error_Details(error_msg);	
		Error_Show();
		Error_Clear();
		return false;
	}
	else
		{		
		var batch = document.frmFieldAuditSetCost.batchData.value;
		document.frmFieldAuditSetCost.hSetInputStr.value=hSetInputStr;
		document.frmFieldAuditSetCost.hInputStr.value=hInputStr;
		document.frmFieldAuditSetCost.attType.value = "11210";
		document.frmFieldAuditSetCost.strOpt.value = "save";	
		
		if(confirm(message_accounts[43])){
			fnStartProgress();
	    	document.frmFieldAuditSetCost.submit();
			document.frmFieldAuditSetCost.Btn_Submit.disabled = true;
		}
	}
}
function fnReset()
{		
	document.frmFieldAuditSetCost.batchData.value='';
}

