var inputstr ;
function initGridData(divRef,gridData)
{
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);	
	gObj.init();	
	if(deviationType == 'Reconcile'){
		gObj.enablePaging(true, 100, 10, "pagingArea", true, "infoArea");
	}else{
		gObj.enablePaging(true, 50, 10, "pagingArea", true, "infoArea");
	}
	gObj.setPagingSkin("bricks");
	gObj.loadXMLString(gridData);	
	if(deviationType == 'Reconcile'){
		gObj.enableTooltips("true,true,true,true,true,true,true,true,true,true,true,true,true,true,true");
		gObj.attachHeader("<input type='checkbox' value='no' name='selectFSAll' onClick='javascript:fnChangedFilter(this);'/>,"+'#text_filter,#select_filter,#select_filter,#text_filter,#text_filter,#text_filter,#select_filter,#text_filter,#text_filter,#text_filter,#select_filter,#text_filter,#select_filter,#text_filter');
	}else if(deviationType == 'Approval'){
		gObj.enableTooltips("true,true,true,true,true,true,true,true,true,true,true,true,true,true");
		gObj.attachHeader("<input type='checkbox' value='no' name='selectFSAll' onClick='javascript:fnChangedFilter(this);'/>,"+'#text_filter,#select_filter,#select_filter,#text_filter,#text_filter,#text_filter,#select_filter,#text_filter,#text_filter,#text_filter,#select_filter,#text_filter,#text_filter');
	}
	return gObj;	
}

function fnOnLoad() {	
	if (objGridData != '') {
		if(deviationType == 'Approval'){
			gridObj = initGridData('FlagApprovalRpt',objGridData);
		}else if(deviationType == 'Reconcile'){
			gridObj = initGridData('ApprovedReconcileRpt',objGridData);
		}
	}
	gridObj.attachEvent("onCheckbox", doOnCheck);
	check_rowId = gridObj.getColIndexById("check");
	gridObj.checkAll(false);
}
function fnDownloadXLS()
{
	gridObj.setColumnHidden(0,true);
	gridObj.toExcel('/phpapp/excel/generate.php');
	gridObj.setColumnHidden(0,false);
		
}
function fnApproval(){
	var Approved_str = '';	
	
	var chk=gridObj.getCheckedRows(0).split(",");
	if(chk == ''){
		Error_Details(message_accounts[16]);
	}else{
		for (var i=0;i<chk.length;i++){		
			Approved_str = Approved_str + chk[i] + '|';
		}	
	}	
	if (ErrorCount > 0) {
		Approved_str = '';
		Error_Show();
		Error_Clear();
		return false;
	}else {
		document.frmFieldAuditApprReconcile.inputString.value = Approved_str;
		document.frmFieldAuditApprReconcile.action = '/gmFieldAuditApprReconcile.do?method=loadFlaggedForApproval';
		document.frmFieldAuditApprReconcile.haction.value = "save";		
		if (confirm(message_accounts[17])){ 
			fnStartProgress();
			document.frmFieldAuditApprReconcile.submit();
			document.frmFieldAuditApprReconcile.Btn_Approve.disabled = true;
		}
	}
}

function fnReconcile(){
	var reconcile_str = '';	
	var chk=gridObj.getCheckedRows(0).split(",");
	if(chk == ''){
		Error_Details(message_accounts[18]);
	}else{
		for (var i=0;i<chk.length;i++){		
			reconcile_str = reconcile_str + chk[i] + '|';
		}	
	}	
	if (ErrorCount > 0) {
		reconcile_str = '';
		Error_Show();
		Error_Clear();
		return false;
	}else {
		document.frmFieldAuditApprReconcile.inputString.value = reconcile_str;
		document.frmFieldAuditApprReconcile.action = '/gmFieldAuditApprReconcile.do?method=loadApprovedForReconcile';
		document.frmFieldAuditApprReconcile.haction.value = "save";		
		if (confirm(message_accounts[19])){	
			fnStartProgress();
			document.frmFieldAuditApprReconcile.submit();
			document.frmFieldAuditApprReconcile.Btn_Reconcile.disabled = true;
		}
	}	
	
}

function fnChangedFilter(obj){
	if(obj.checked)
	{		
		gridObj.forEachRow(function(rowId) {
			check_id = gridObj.cellById(rowId, check_rowId).getValue();			
				gridObj.cellById(rowId, check_rowId).setChecked(true);
			
	   });
	}else
	{
		gridObj.forEachRow(function(rowId) {
			check_id = gridObj.cellById(rowId, check_rowId).getValue();			
			if(check_id ==1){
				gridObj.cellById(rowId, check_rowId).setChecked(false);
			}
			
	   });			
	}
}
//Description : This function to check the select all (master check box)
function doOnCheck(rowId, cellInd, stage) {
	rowID = rowId;
	if(cellInd == check_rowId) {
		fnSelectAllToggle('selectFSAll');
	}
	return true;
}
//Description : This function to check the select all check box
function fnSelectAllToggle( varControl){
	var objControl = eval("document.frmFieldAuditApprReconcile."+varControl);

	var all_row = gridObj.getAllRowIds(",");
	var all_rowLen = all_row.length; // get the rows id length  
	var checked_row = gridObj.getCheckedRows(check_rowId);	
	var finalval;		

	finalval = eval(checked_row.length);
	if(all_rowLen == finalval){
		objControl.checked = true;// select all check box to be checked
	}else{
		objControl.checked = false;// select all check box un checked.
	}
}