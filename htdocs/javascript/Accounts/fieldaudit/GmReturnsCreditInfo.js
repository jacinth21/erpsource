function initGridData(divRef,gridData)
{
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);
	gObj.enableTooltips("true,true,true,true,true,true,true,true");	
	gObj.init();		
	gObj.loadXMLString(gridData);	
	return gObj;	
}
function fnOnLoad() {
	if (objGridData != '') {
		gridObj = initGridData('Div_ReturnCredit', objGridData);
	}
}
function fnClose()
{
	window.close();
}
function fnSetRAID(obj)
{	
	document.all.hRAID.value=obj.value;
}
function fnApply(){
	var Errorcount = 0;
	var refid = document.all.refId.value;
	var raID = document.all.hRAID.value;
	if(raID != ''){		
	window.opener.fnLoadRADetails(raID,refid);
	window.close();
	}else{
		Errorcount++;
	}
	if(Errorcount > 0){
		Error_Details(message_accounts[68]);
		Error_Show();	
		Error_Clear();			
		return false;
	}else{
		return true;
	} 	
}
function fnOpenRADetails(val)
{
	windowOpener('/GmPrintCreditServlet?hAction=VIEW&hRAID='+val,"Returns","resizable=yes,scrollbars=yes,top=150,left=200,width=660,height=400");
}




