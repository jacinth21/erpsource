function fnEnter(){
		if (event.keyCode == 13){ 
			fnSubmit();
		}
}
function fnReload(){	
	if (document.frmFieldAuditReport.distId){		
		document.frmFieldAuditReport.submit();
	}
}
function fnSubmit(){	
	var frm = document.frmFieldAuditReport;
	if((reportType == 'globalSet') || (reportType == 'globalDist')){
		if((document.frmFieldAuditReport.lockedFrmDt.value == '')||(document.frmFieldAuditReport.lockedToDt.value  == ''))
		{
			fnValidateDropDn('auditId',message_accounts[199]);
		}
	}else{
		fnValidateDropDn('auditId',message_accounts[199]);
	}
	if (document.frmFieldAuditReport.distId){
		fnValidateDropDn('distId', message_accounts[200]);
	}
	if (document.frmFieldAuditReport.setId){
		fnValidateDropDn('setId', message_accounts[201]);
	}
	if (document.frmFieldAuditReport.devQty){
		NumberValidation(document.all.devQty.value,message_accounts[67],0);
	}
	if ((document.frmFieldAuditReport.lockedFrmDt)||(document.frmFieldAuditReport.lockedToDt)){
		objDtfrom 	= frm.lockedFrmDt;
		objDtTo 	= frm.lockedToDt;	
		if(objDtfrom.value != ""){
			CommonDateValidation(objDtfrom,format,Error_Details_Trans(message_accounts[65],format));
		}	
		if(objDtTo.value != ""){
			CommonDateValidation(objDtTo,format,Error_Details_Trans(message_accounts[66],format));
		}
	}	
	if (ErrorCount > 0){
			Error_Show();
			Error_Clear();
			return false;
	}
	document.frmFieldAuditReport.strOpt.value = "load";		
	fnStartProgress();
	document.frmFieldAuditReport.submit();
}

function fnOpenVarianceBySet(value){
	auditId = document.frmFieldAuditReport.auditId.value;
	reportType ='';
	document.frmFieldAuditReport.action="/gmFieldAuditReport.do?method=reportVarianceBySet&strOpt=load&reportType="+reportType+"&auditId="+auditId+"&setId="+value;
	document.frmFieldAuditReport.submit();
}

function fnOpenVarianceByDist(value){
	auditId = document.frmFieldAuditReport.auditId.value;
	reportType ='varianceDist';// added for resolving the error in variance dist (set id null) screen, whenever we are comming from Global Dist Screen
	document.frmFieldAuditReport.action="/gmFieldAuditReport.do?method=reportVarianceByDist&strOpt=load&reportType="+reportType+"&auditId="+auditId+"&distId="+value;
	document.frmFieldAuditReport.submit();
}

function fnOpenFlagVariance(distid, setid, pnum) {
	auditId = document.frmFieldAuditReport.auditId.value;
	var distValue = '';
	if(document.frmFieldAuditReport.reportType.value == 'varianceDist'){
		distValue = document.frmFieldAuditReport.distId.options[document.frmFieldAuditReport.distId.selectedIndex].text;
	}
	var vURL = '';
	
	vURL = fnAjaxURLAppendCmpInfo("/gmFieldAuditFlagVar.do?auditId=" + auditId + "&distId="+ distid + "&setId=" + setid + "&pnum=" + encodeURIComponent(pnum) + "&distName=" + distValue +'&ramdomId=' + Math.random());

	w1 = createDhtmlxWindow(1355,600,true);
    w1.setText("Flag Deviation");
	w1.button("close").hide();
	w1.button("park").show();
	w1.button("minmax1").show();
    w1.addUserButton("close", "close", "close", "close");
	w1.button("close").attachEvent("onClick", fnClose);
	w1.attachURL(vURL);
}

function fnSelect(val){
	document.frmFieldAuditReport.hauditEntryID.value = val;
}

function fnChooseAction(){
	val = document.frmFieldAuditReport.Cbo_Action.value;
	auditEntryID=document.frmFieldAuditReport.hauditEntryID.value;
	distID = document.frmFieldAuditReport.distId.value;

	if (val == '0')
	{
		Error_Details(message[4000]);
	}
	if (auditEntryID =='')
	{
		Error_Details(message[4001]);
	}
	if (ErrorCount > 0)  
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	document.frmFieldAuditReport.action="/gmEditAuditTag.do?strOpt=onload_edit_audit_tag&auditEntryID="+auditEntryID+"&distID="+distID;
	document.frmFieldAuditReport.submit();
}

function fnOpenLog(id,type)
{
	windowOpener("/GmCommonLogServlet?hType="+type+"&hID="+id,"ConsignLog","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=300");
}

//To close the flag deviation screen and reload the Variance screen
function fnClose() {
	window.fnSubmit();
	dhxWins.window("w1").close();
	return false;
}
// To highlight the rows

function fnRowHighlight()
{
	var stroption = document.frmFieldAuditReport.strOpt.value;
	var varId = 'currentRowObject';
	var varStyle = 'lightBrown';
	if(stroption =='load'){
		var table = document.getElementById(varId);
		if (! table) { return; }
		var tbodies = table.getElementsByTagName("tbody");
		var trs = tbodies[0].getElementsByTagName("tr");
		var len = trs.length;
		for (var i = 0; i < len; i++) 
		{
			trs[i].onmouseover=function(){
				this.className = varStyle; return false
			}
			trs[i].onmouseout=function(){
				this.className = this.className.replace(varStyle, ""); return false
			}
		}
	}
}

