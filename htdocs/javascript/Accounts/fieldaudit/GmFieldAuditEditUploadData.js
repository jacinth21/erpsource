
// Description: This function will call on PageLoad
function fnOnload()
{ 
	var strOpt=document.frmFieldAuditEditUploadData.strOpt.value;
	var locType=document.frmFieldAuditEditUploadData.loctyp.value;
	var tag_status = document.frmFieldAuditEditUploadData.tagStatus.value;
	var flag_type = document.frmFieldAuditEditUploadData.flagType.value;
	var locDet = document.getElementById("locationDet");
	
	if(flag_type !='0' && flag_type !='51023' && flag_type !='51025'){

		document.frmFieldAuditEditUploadData.flagType.disabled = true;
		if(flag_type !='18558'){ // Void Tag Only
			document.frmFieldAuditEditUploadData.flagDetID.disabled =true;
		}
		document.frmFieldAuditEditUploadData.loctyp.disabled = true;
		document.frmFieldAuditEditUploadData.loctyp.disabled = true;
		document.frmFieldAuditEditUploadData.locationComments.disabled =true;
		document.frmFieldAuditEditUploadData.locationID.disabled = true;
		document.frmFieldAuditEditUploadData.addressType.disabled = true;
		document.frmFieldAuditEditUploadData.locationDet.disabled = true;
	}
	
	if(locType != '6022' && locType != '0')
	{
		document.frmFieldAuditEditUploadData.addressType.disabled = true;
	}
	else
	{
		document.frmFieldAuditEditUploadData.addressType.disabled = false;
	}
	if(strOpt=="lockedset" || strOpt=="savelockedset")
	{
		 parent.dhxWins.window("w1").setText(message_accounts[20]);
		 parent.dhxWins.window("w1").setDimension(900, 650);
		 document.frmFieldAuditEditUploadData.lockedSet.checked=true;
		 document.frmFieldAuditEditUploadData.lockedSet.disabled=true;
		 document.frmFieldAuditEditUploadData.flagType.disabled =true;
		 document.frmFieldAuditEditUploadData.flagDetID.disabled =true;
		 document.frmFieldAuditEditUploadData.addressType.disabled = true;
		 //document.frmFieldAuditEditUploadData.countedDate.disabled = true;
	}else{
		if(document.frmFieldAuditEditUploadData.approvedFl.value =='Y'){
			document.frmFieldAuditEditUploadData.partNumber.disabled = true;	
			document.frmFieldAuditEditUploadData.setID.disabled = true;
			document.frmFieldAuditEditUploadData.statusID.disabled =true;
			document.frmFieldAuditEditUploadData.flagType.disabled =true;
			document.frmFieldAuditEditUploadData.flagDetID.disabled =true;
		}
		
	}
	if(locType == '6023') // 6023 for N/A Location Type
	{
		document.frmFieldAuditEditUploadData.locationID.disabled = true;
		document.frmFieldAuditEditUploadData.addressType.disabled = true;
		document.frmFieldAuditEditUploadData.locationDet.disabled = true;
		document.frmFieldAuditEditUploadData.locationComments.disabled=false;
	}else{
		document.frmFieldAuditEditUploadData.locationComments.disabled=true;
	}
	
	for(var i = 0; i < locDet.options.length; i++)
    {
		if (locDet.options[i].value > 0 )
		{
			document.frmFieldAuditEditUploadData.locationDet.value = locDet.options[1].value;
		}
    }
	
	if(recon == 'Y'){
		document.frmFieldAuditEditUploadData.partNumber.disabled = true;	
		document.frmFieldAuditEditUploadData.controlNumber.disabled = true;
		document.frmFieldAuditEditUploadData.setID.disabled = true;
		document.frmFieldAuditEditUploadData.comments.disabled =true;
		document.frmFieldAuditEditUploadData.statusID.disabled =true;
		document.frmFieldAuditEditUploadData.flagType.disabled =true;
		document.frmFieldAuditEditUploadData.flagDetID.disabled =true;
		document.frmFieldAuditEditUploadData.loctyp.disabled = true;
		document.frmFieldAuditEditUploadData.locationComments.disabled =true;
		document.frmFieldAuditEditUploadData.locationID.disabled = true;
		document.frmFieldAuditEditUploadData.addressType.disabled = true;
		document.frmFieldAuditEditUploadData.locationDet.disabled = true;
		if(strOpt !="lockedset"){
			document.frmFieldAuditEditUploadData.countedDate.disabled = true;
			document.frmFieldAuditEditUploadData.Img_Date.disabled = true;
		}
	}
}

// Description: This function will call onchange of Location Details.
function fnAddressType()
{
	var locType=document.frmFieldAuditEditUploadData.loctyp.value;
	var locDetValue = '';
	if(locType == '6022')
	{
		var locDetails=document.frmFieldAuditEditUploadData.locationDet.value;
		var addressType=0;
		if(locDetails.indexOf("-")!=-1)
		{
			addressType=locDetails.substring((locDetails.indexOf("-")+1),locDetails.length);
			document.frmFieldAuditEditUploadData.addressType.value=addressType;
		}
	}
	locDetValue = document.frmFieldAuditEditUploadData.locationDet.options[document.frmFieldAuditEditUploadData.locationDet.selectedIndex].text;
	if(locType !== '6022' && locDetValue != '')	
	{
		document.frmFieldAuditEditUploadData.addressType.disabled = true;
	}
}

//Description: This function will call onchange of Location Type.
function fnLocation() 
{
	var locType=document.frmFieldAuditEditUploadData.loctyp.value;
	document.frmFieldAuditEditUploadData.partNumber.disabled = false;	
	document.frmFieldAuditEditUploadData.setID.disabled = false;
	document.frmFieldAuditEditUploadData.statusID.disabled =false;
	document.frmFieldAuditEditUploadData.flagType.disabled =false;
	document.frmFieldAuditEditUploadData.flagDetID.disabled =false;
	if(locType == '6023')
	{
		document.frmFieldAuditEditUploadData.locationID.disabled = true;
		document.frmFieldAuditEditUploadData.addressType.disabled = true;
		document.frmFieldAuditEditUploadData.locationDet.disabled = true;
		document.frmFieldAuditEditUploadData.locationComments.disabled=false;
		return false;
	}
	document.frmFieldAuditEditUploadData.action="/gmFieldAuditEditUploadData.do?method=editUploadData&strOpt=location";
	document.frmFieldAuditEditUploadData.submit();
}

//Description: This function will call onchange of Location
function fnLocationDetails() 
{
	document.frmFieldAuditEditUploadData.partNumber.disabled = false;	
	document.frmFieldAuditEditUploadData.setID.disabled = false;
	document.frmFieldAuditEditUploadData.statusID.disabled =false;
	document.frmFieldAuditEditUploadData.flagType.disabled =false;
	document.frmFieldAuditEditUploadData.flagDetID.disabled =false;
	
	document.frmFieldAuditEditUploadData.action="/gmFieldAuditEditUploadData.do?method=editUploadData&strOpt=locationDetails";
	document.frmFieldAuditEditUploadData.submit();
}

//Description: This function will call to submit the data.
function fnSubmit()
{   
	
	var strOpt=document.frmFieldAuditEditUploadData.strOpt.value;
	var currdate = new Date(strToday);
	var counteddate = '';
	 
	fnValidateTxtFld('partNumber',lblPartNum);	
	fnValidateTxtFld('controlNumber',lblControlNum);	
	fnValidateDropDn('locationID',lblLocation);
	fnValidateDropDn('locationDet',lblLocationDtls);

	var tag_status = document.frmFieldAuditEditUploadData.tagStatus.value;
	var flag_type = document.frmFieldAuditEditUploadData.flagType.value;
	var locType =document.frmFieldAuditEditUploadData.loctyp.value;
	var tagid = document.frmFieldAuditEditUploadData.tagID.value;
	
	if(strOpt=="lockedset" || strOpt=="savelockedset")
	{
		fnValidateDropDn('setID',lblSetId);
		document.frmFieldAuditEditUploadData.strOpt.value='savelockedset';
	}
	else if(strOpt=="onload_edit_audit_tag"||strOpt=="location"||strOpt=="locationDetails" || strOpt=="EDITUPLOAD")
	{
		counteddate = new Date(document.frmFieldAuditEditUploadData.countedDate.value);
		fnValidateTxtFld('countedDate',lblCountDt);
		CommonDateValidation(document.frmFieldAuditEditUploadData.countedDate, format, message[611]);
		
		if (counteddate >  currdate){
			Error_Details(message_accounts[21]);
		}
		
		if(document.frmFieldAuditEditUploadData.locationID.disabled && locType != '6023'){
			Error_Details(message_accounts[22]);
		}
		if(flag_type != '18558'){ // 18558 - Void tag only
			if(flag_type != '0' && document.frmFieldAuditEditUploadData.flagDetID.value == 0){
				Error_Details(message_accounts[23]);
			}
			
			if(flag_type == '0' && document.frmFieldAuditEditUploadData.flagDetID.value != 0){
				Error_Details(message_accounts[24]);
			}
		}
		if(tag_status =='18541' && flag_type != '0'){ // 18541 - Matching tag
			Error_Details(Error_Details_Trans(message_accounts[25],tagid));
		}
		document.frmFieldAuditEditUploadData.strOpt.value='EDITUPLOAD';
	}

	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	document.frmFieldAuditEditUploadData.partNumber.disabled = false;	
	document.frmFieldAuditEditUploadData.controlNumber.disabled = false;
	document.frmFieldAuditEditUploadData.setID.disabled = false;
	document.frmFieldAuditEditUploadData.comments.disabled =false;
	document.frmFieldAuditEditUploadData.statusID.disabled =false;
	document.frmFieldAuditEditUploadData.flagType.disabled = false;
	document.frmFieldAuditEditUploadData.loctyp.disabled = false;
	document.frmFieldAuditEditUploadData.locationComments.disabled =false;
	document.frmFieldAuditEditUploadData.locationID.disabled = false;
	document.frmFieldAuditEditUploadData.addressType.disabled = false;
	document.frmFieldAuditEditUploadData.locationDet.disabled = false;
	if(flag_type !='18558'){ // Void Tag Only
		document.frmFieldAuditEditUploadData.flagDetID.disabled =false;
	}
	
	if(locType == '6023') // 6023 for N/A Location Type
	{
		document.frmFieldAuditEditUploadData.locationID.value = 0;
		document.frmFieldAuditEditUploadData.addressType.value = 0;
		document.frmFieldAuditEditUploadData.locationDet.value = 0;
	}
	fnStartProgress();
	document.frmFieldAuditEditUploadData.submit();
	document.frmFieldAuditEditUploadData.Btn_Submit.disabled = true;
}

//Description: This fucntion will call onclick of Back button.
function fnBack()
{	
	var strOpt=document.frmFieldAuditEditUploadData.strOpt.value;
	
	if (strOpt == "lockedset" || strOpt == "savelockedset")
	{
		/*parent.window.fnSubmit();
		parent.dhxWins.window("w1").close();
		return false; */
		var audit_id = document.frmFieldAuditEditUploadData.auditId.value;
		var dist_id = document.frmFieldAuditEditUploadData.distId.value;
		document.frmFieldAuditEditUploadData.partNumber.disabled = false;
		var part_num =  document.frmFieldAuditEditUploadData.partNumber.value;
		var setid = '';
		if((strOpt == "lockedset" && lockedSetFl =='Y') || (strOpt == "savelockedset" && lockedSetFl =='Y')){
			setid = document.frmFieldAuditEditUploadData.setID.value;
		}
		document.frmFieldAuditEditUploadData.action = "/gmFieldAuditFlagVar.do?auditId=" + audit_id + "&distId="+ dist_id + "&pnum=" + encodeURIComponent(part_num)+ "&setId=" + setid ;
    	document.frmFieldAuditEditUploadData.submit();
	}
	else if(strOpt == "EDITUPLOAD" || strOpt == "onload_edit_audit_tag" || strOpt == "location" || strOpt == "locationDetails")
	{
		auditEntryID=document.frmFieldAuditEditUploadData.auditEntryID.value;
		auditId = document.frmFieldAuditEditUploadData.hauditId.value;
		distId = document.frmFieldAuditEditUploadData.hdistId.value;
		auditorId = document.frmFieldAuditEditUploadData.hauditorId.value;
		
		document.frmFieldAuditEditUploadData.action="/gmFieldAuditEditUploadData.do?method=verifyUploadData&strOpt=report&auditEntryID="+auditEntryID+"&auditId="+auditId+"&distId="+distId+"&auditorId="+auditorId;
		document.frmFieldAuditEditUploadData.submit();
	}
}
// to disable the flag reason

function fnChangeFlag(obj){
	if(obj.value != '0' && obj.value !='51023' && obj.value !='51025'){
		document.frmFieldAuditEditUploadData.flagDetID.disabled =true;
		document.frmFieldAuditEditUploadData.loctyp.disabled = true;
		document.frmFieldAuditEditUploadData.locationComments.disabled =true;
		document.frmFieldAuditEditUploadData.locationID.disabled = true;
		document.frmFieldAuditEditUploadData.addressType.disabled = true;
		document.frmFieldAuditEditUploadData.locationDet.disabled = true;
	}else{
		document.frmFieldAuditEditUploadData.loctyp.disabled = false;
		document.frmFieldAuditEditUploadData.locationComments.disabled =false;
		var locType = document.frmFieldAuditEditUploadData.loctyp.value;
		if(locType != '6023'){  // 6023 for N/A Location Type
			document.frmFieldAuditEditUploadData.locationID.disabled = false;
			document.frmFieldAuditEditUploadData.addressType.disabled = false;
			document.frmFieldAuditEditUploadData.locationDet.disabled = false;
		}	
		if(obj.value != '18558'){ // Not a Deviation
			document.frmFieldAuditEditUploadData.flagDetID.disabled = false;
		}
	}
}