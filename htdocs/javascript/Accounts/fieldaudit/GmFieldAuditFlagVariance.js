var cnt;
var inputString = '';
var retagFl = false;

//This function used to visible the drop down and text box based on the flag type 
function fnChangeReason(obj, count) {

	cnt = count;
	var codeValue = obj.value;
	var ref_detail = document.getElementById("txt_detail" + cnt);
	ref_detail.value = '';

	var ref_id = document.getElementById("txt_tnx" + cnt);
	ref_id.value = '';

	var ref_id_drop = document.getElementById("cbo_dist" + cnt);
	ref_id_drop.value = 0;

	fnInVisibleDiv(count);
	fnVisibleDiv(codeValue,count);
	
}
//This function used to Visible the drop down and text box based on the Flag type
function fnVisibleDiv (codeValue,count){
	var tagType = '';
	
	if (codeValue == "51025" || codeValue == "51026" || codeValue == "51023"
		|| codeValue == "51027") {
	var distObj = document.getElementById("div_dist" + count);
	distObj.style.display = 'block';
	distObj.style.display = 'inline';

	var changeObj = document.getElementById("retag" + count);
	changeObj.style.display = 'block';
} else if (codeValue == "51029") {
	var txnObj = document.getElementById("dev_tnx" + count);
	txnObj.style.display = 'block';
	txnObj.style.display = 'inline';

	var butObj = document.getElementById("dev_but" + count);
	butObj.style.display = 'block';
	butObj.style.display = 'inline';
	
	var changeObj = document.getElementById("retag" + count);
	changeObj.style.display = 'none';
	
} else if (codeValue == "51028") {

	var changeObj = document.getElementById("div_change" + count);
	changeObj.style.display = 'block';
	changeObj.style.display = 'inline';

	var reasonVal = document.getElementById("cbo_change" + count).value;
	if (reasonVal == '51040' || reasonVal == '18566') {
		var detailObj = document.getElementById("div_detail" + count);
		detailObj.style.display = 'block';
		detailObj.style.display = 'inline';
	} else {
		var detailObj = document.getElementById("div_detail" + count);
		detailObj.value = '';
		detailObj.style.display = 'none';
	}
	
	var changeObj = document.getElementById("retag" + count);
	changeObj.style.display = 'none';
} else if (codeValue == "51024") {
	var changeObj = document.getElementById("div_extra" + count);
	changeObj.style.display = 'block';
	changeObj.style.display = 'inline';

	var changeObj = document.getElementById("retag" + count);
	changeObj.style.display = 'block';

} else if (codeValue == "18551") {
	var changeObj = document.getElementById("div_notDevPos" + count);
	changeObj.style.display = 'block';
	changeObj.style.display = 'inline';

	var changeObj = document.getElementById("retag" + count);
	changeObj.style.display = 'block';

} else if (codeValue == "18552") {
	var changeObj = document.getElementById("div_unVoidTag" + count);
	changeObj.style.display = 'block';
	changeObj.style.display = 'inline';

	var changeObj = document.getElementById("retag" + count);
	changeObj.style.display = 'block';

} else if (codeValue == "18556") {
	var changeObj = document.getElementById("div_VerPost" + count);
	changeObj.style.display = 'block';
	changeObj.style.display = 'inline';

	var changeObj = document.getElementById("retag" + count);
	changeObj.style.display = 'block';
} else if (codeValue == "18557") {
	var changeObj = document.getElementById("div_notDevNeg" + count);
	changeObj.style.display = 'block';
	changeObj.style.display = 'inline';

	var changeObj = document.getElementById("retag" + count);
	changeObj.style.display = 'block';
}
	// to disable the returns reasons (text box and 'L' Icon)
	if (codeValue == '51029'){
		var txnObj = document.getElementById("dev_tnx" + count);
		var butObj = document.getElementById("dev_but" + count);
		tagType = document.getElementById("tagType" + count); // to get the tag type (Loaner,Consignment)
		if(tagType.value =='4127'){
			txnObj.style.display = 'none';
			butObj.style.display = 'none';
		}
	}
}

//This function used to in visible the drop down and text box based on the flag type 
function fnInVisibleDiv(count) {

	var distObj = document.getElementById("div_dist" + count);
	distObj.style.display = 'none';

	var txnObj = document.getElementById("dev_tnx" + count);
	txnObj.style.display = 'none';

	var butObj = document.getElementById("dev_but" + count);
	butObj.style.display = 'none';

	var changeObj = document.getElementById("div_change" + count);
	changeObj.style.display = 'none';

	var detailObj = document.getElementById("div_detail" + count);
	detailObj.style.display = 'none';

	var changeObj = document.getElementById("div_extra" + count);
	changeObj.style.display = 'none';

	var changeObj = document.getElementById("div_notDevPos" + count);
	changeObj.style.display = 'none';

	var changeObj = document.getElementById("div_unVoidTag" + count);
	changeObj.style.display = 'none';

	var changeObj = document.getElementById("div_VerPost" + count);
	changeObj.style.display = 'none';

	var changeObj = document.getElementById("div_notDevNeg" + count);
	changeObj.style.display = 'none';

	var changeObj = document.getElementById("retag" + count);
	changeObj.style.display = 'none';
}
//On page load to disabled the approved rows
function fnOnload() {
	var cntPos = document.getElementById("countPos");
	var objApp = '';
	var objValue = '';
	var set_Id = document.frmFieldAuditFlagVariance.setId.value;
	
	parent.dhxWins.window("w1").setText(message_accounts[26]);
	parent.dhxWins.window("w1").setDimension(1355, 600);
	
	
	if (cntPos != null) {
		for ( var i = 0; i <= cntPos.value; i++) {
			objApp = document.getElementById("approve" + i);
			objValue = objApp.value;
			fnOnDisabled(objValue, i);
		}
	}
	var cntNeg = document.getElementById("countNeg");
	objChk = '';
	if (cntNeg != null) {
		for ( var j = 600; j <= cntNeg.value; j++) {
			objApp = document.getElementById("approve" + j);
			objValue = objApp.value;
			fnOnDisabled(objValue, j);
		}
	}

}
//On page load to disabled the approved rows
function fnOnDisabled(objChk, count) {

	var flagObj = document.getElementById("cbo_Flag" + count);
	var distObj = document.getElementById("div_dist" + count);
	var detailObj = document.getElementById("div_detail" + count);
	var txnObj = document.getElementById("dev_tnx" + count);
	var changeObj = document.getElementById("div_change" + count);
	var butObj = document.getElementById("dev_but" + count);
	var retagObj = document.getElementById("retag" + count);
	var flagDateObj = document.getElementById("flagDt" + count);
	var postOptionObj = document.getElementById("cbo_post" + count);
	var postCommentObj = document.getElementById("txt_comment" + count);
	var extraObj = document.getElementById("div_extra" + count);
	var notDevPosObj = document.getElementById("div_notDevPos" + count);
	var unVoidObj = document.getElementById("div_unVoidTag" + count);
	var verPostObj = document.getElementById("div_VerPost" + count);
	var notDevNegOnj = document.getElementById("div_notDevNeg" + count);
	
	if(flagObj.value != 0){
		fnInVisibleDiv(count);
		   fnVisibleDiv(flagObj.value,count);
		}

	if (objChk == 'Y') {

		flagObj.disabled = true;
		distObj.disabled = true;
		detailObj.disabled = true;
		txnObj.disabled = true;
		changeObj.disabled = true;
		butObj.disabled = true;
		retagObj.disabled = true;
		flagDateObj.disabled = true;
		postOptionObj.disabled = true;
		postCommentObj.disabled = true;
		extraObj.disabled = true;
		notDevPosObj.disabled = true;
		unVoidObj.disabled = true;
		verPostObj.disabled = true;
		notDevNegOnj.disabled = true;

	}
}
//This function used to when select the flag type as missing (-) on the time to set the Charge
function fnCharge(obj, cnt) {
	var set_Cost = document.getElementById("setCost");
	var part_Price = document.getElementById("partPrice");
	var ref_detail = document.getElementById("txt_detail" + cnt);
	var detailObj = document.getElementById("div_detail" + cnt);
	var missingCost = document.getElementById("missingCost"+cnt);
	var missingCostType = document.getElementById("missingCostType"+cnt);

	if (obj.value == '51040') {
		detailObj.style.display = 'block';
		detailObj.style.display = 'inline';
		if(missingCost.value ==''){
			ref_detail.value = set_Cost.value;
		}else if(missingCostType.value == obj.value){
			ref_detail.value = missingCost.value;
		}else{
			ref_detail.value = '';
		}		
	} else if (obj.value == '18566') {
		detailObj.style.display = 'block';
		detailObj.style.display = 'inline';
		if(missingCost.value ==''){
			ref_detail.value = ''; //part_Price.value;
		}else if(missingCostType.value == obj.value){
			ref_detail.value = missingCost.value;
		}else{
			ref_detail.value = '';
		}		
	} else {
		ref_detail.value = '';
		detailObj.style.display = 'none';
	}
}
//This function used to save the flag deviation details
function fnFlagDeviation() {
	inputString = '';
	var cntPos = document.getElementById("countPos");
	if (cntPos != null) {
		fnValidateFlag(cntPos.value, '18542');
	}
	var cntNeg = document.getElementById("countNeg");
	if (cntNeg != null) {
		fnValidateFlag(cntNeg.value, '18543');
	}

	if (inputString == '') {
		Error_Details(message_accounts[27]);
	}
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	if (retagFl) {
		if (confirm(message_accounts[28])) {

		} else {
			return false;
		}
	}
	document.frmFieldAuditFlagVariance.strOpt.value = "save";
	document.frmFieldAuditFlagVariance.strInput.value = inputString;
	document.frmFieldAuditFlagVariance.submit();
	document.frmFieldAuditFlagVariance.Btn_Submit.disabled = true;
}
//This function validate the flag selection 
function fnValidateFlag(cnt, type) {

	var objId = '';
	var objFlag = '';
	var ref_id = '';
	var ref_detail = '';
	var codeValue = '';
	var ref_detail = '';
	var approve = '';
	var approved = '';
	var approveVal = '';
	var postOption = '';
	var postComment = '';
	var postOptionVal = '';
	var codeName = '';

	var flagDate = '';
	var flagDateVal = '';

	var tagType = '';
	var reTag = '';
	var reTagVal = '';
	var reTagFlag = '';

	var k = '';
	if (type == '18542') {
		k = 0;
	} else {
		k = 600;
	}

	for ( var i = k; i <= cnt; i++) {
		objId = document.getElementById("txt_id" + i);
		objFlag = document.getElementById("cbo_Flag" + i);
		codeValue = objFlag.value;
		codeName = objFlag.options[objFlag.selectedIndex].text;

		ref_detail = '';
		ref_id = '';
		
		if (type == '18542') {

			if (codeValue == "51025" || codeValue == "51023") {
				ref_id = document.getElementById("cbo_dist" + i);

				if (ref_id.value == '0') {
					Error_Details(message_accounts[29]);
				}
			}

			if (codeValue == "51024") {
				ref_id = document.getElementById("cbo_extra" + i);

				if (ref_id.value == '0') {
					Error_Details(message_accounts[29]);
				}
			}

			if (codeValue == "18551") {
				ref_id = document.getElementById("cbo_notDevPos" + i);

				if (ref_id.value == '0') {
					Error_Details(message_accounts[29]);
				}
			}
			if (codeValue == "18552") {
				ref_id = document.getElementById("cbo_unVoidTag" + i);

				if (ref_id.value == '0') {
					Error_Details(message_accounts[29]);
				}
			}

		}

		if (type == '18543') {

			if (codeValue == "51026" || codeValue == "51027") {
				ref_id = document.getElementById("cbo_dist" + i);

				if (ref_id.value == '0') {
					Error_Details(message_accounts[29]);
				}
			}

			if (codeValue == "51028") {
				ref_id = document.getElementById("cbo_change" + i);
				ref_detail = document.getElementById("txt_detail" + i);

				if ((ref_id.value == '51040' || ref_id.value == '18566')) {
					NumberValidation(ref_detail.value,message_accounts[30], 2);
				}

				if ((ref_id.value == '51040' || ref_id.value == '18566')
						&& ref_detail.value == '') {
					Error_Details(message_accounts[29]);
				}
			}

			if (codeValue == "51029") {
				ref_id = document.getElementById("txt_tnx" + i);
				tagType = document.getElementById("tagType" + i); // to get the tag type (Loaner,Consignment)

				if (ref_id.value == '' && tagType.value !='4127') {
					Error_Details(message_accounts[29]);
				}
			}
			if (codeValue == "18556") {
				ref_id = document.getElementById("cbo_VerPost" + i);

				if (ref_id.value == '0') {
					Error_Details(message_accounts[29]);
				}
			}
			if (codeValue == "18557") {
				ref_id = document.getElementById("cbo_notDevNeg" + i);

				if (ref_id.value == '0') {
					Error_Details(message_accounts[29]);
				}
			}

		}

		flagDate = document.getElementById("flagDt" + i);
		flagDateVal = flagDate.value;

		reTag = document.getElementById("retag" + i);
		reTagVal = reTag.value;

		reTagFlag = document.getElementById("retagFl" + i);
		
		postOption = document.getElementById("cbo_post" + i);
		postComment = document.getElementById("txt_comment" + i);

		if (postOption.value == '0') {
			postOptionVal = '';
		} else {
			postOptionVal = postOption.value;
		}

		if(postOption.value == '0' && postComment.value != '') {
			Error_Details(message_accounts[31]);
		}
		
		if (postOption.value != '0' && postComment.value == ''
				&& objFlag.value != '0') {
			Error_Details(message_accounts[32]);
		}

		if ((postOption.value != '0' || postComment.value != '')
				&& objFlag.value == '0') {
			Error_Details(message_accounts[33]);
		}

		if (flagDateVal != '') {
			CommonDateValidation(flagDate, format, '<b> Flag Type: </B>'+codeName +' - '+  message[611]);
			if (codeValue == "51028" && ref_id.value == '0') {
				Error_Details(message_accounts[34]);
			}

		}

		var flagValue = objFlag.value;
		var flagRefID = ref_id.value;
		var flagRefDetail = ref_detail.value;
		if (typeof (ref_id.value) == "undefined" || ref_id.value == '0') {
			flagRefID = '';
		}
		if (typeof (ref_detail.value) == "undefined" || ref_detail.value == '0'
				|| codeValue == '51029') {
			flagRefDetail = '';
		}
		if (reTagVal != '') {
			retagFl = true;
		}

		var objApp = document.getElementById("approve" + i);
		var objAppValue = objApp.value;
		
		if (codeValue != '0' && flagDateVal ==''){
			Error_Clear();
			Error_Details(message_accounts[35]);
		}

		if(reTagFlag.value == 'Y' && (codeValue == 51028 || codeValue == 51029 ||  codeValue == 18558)){ 
			// Missing, Return, Void tag only
			Error_Details(Error_Details_Trans(message_accounts[36],codeName));
		}
		
		if (codeValue != '0' && objAppValue == '') {
			inputString = inputString + objId.value + '^' + flagValue + '^'
					+ flagRefID + '^' + flagRefDetail + '^' + reTagVal + '^'
					+ flagDateVal + '^' + postOptionVal + '^'
					+ postComment.value + '|';
		}

	}
}
//This function used to only accept the number key. (Charge - text box)
function isNumberKey(evt) {
	var charCode = (evt.which) ? evt.which : event.keyCode;
	if (charCode > 31 && (charCode < 46 || charCode > 57))
		return false;

	return true;
}
//This function used to min and max the reports
function fnShowFilters(val) {
	var obj = eval("document.all." + val);
	var obj1 = eval("document.all." + val + "img");

	if (obj.style.display == 'none') {
		obj.style.display = 'block';
		if (obj1) {
			obj1.src = '/images/minus.gif';
		}
	} else {
		obj.style.display = 'none';
		if (obj1) {
			obj1.src = '/images/plus.gif';
		}
	}
}
//To open the Log Details (when click - phone icon)
function fnOpenLog(id, type) {
	windowOpener("/GmCommonLogServlet?hType=" + type + "&hID=" + id,
			"ConsignLog",
			"resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=300");
}
//To open the Audit entry History (when click - History icon)
function fnAuditEntryHistory(auditEntryId) {
	windowOpener("/gmFieldAuditEntryHistory.do?auditEntryId=" + auditEntryId,
			"AuditHistory",
			"resizable=yes,scrollbars=yes,top=250,left=300,width=800,height=600");
}
//To open the Tag History (when click - TH icon)
function fnTagHistory(tagid) {
	windowOpener("/gmTagHistory.do?tagID=" + tagid, "TagHistory",
			"resizable=yes,scrollbars=yes,top=250,left=300,width=1100,height=650");
}
//To redirect the Edit uploaded screen (when click - Tag id)
function fnLoadEditUpload(auditEntryId) {
	var auditID = document.frmFieldAuditFlagVariance.auditId.value;
	var distId = document.frmFieldAuditFlagVariance.distId.value;
	document.frmFieldAuditFlagVariance.action = "/gmFieldAuditEditUploadData.do?method=editUploadData&strOpt=lockedset&auditEntryID="
			+ auditEntryId + "&hauditId=" + auditID + "&hdistId=" + distId;
	document.frmFieldAuditFlagVariance.submit();
}

// For opening the Returns Credit Info. Screen
function fnOpenPopup(url) {
	windowOpener(url, "ReturnsCreditedInfo",
			"resizable=yes,scrollbars=yes,top=150,left=200,width=1050,height=450");
}

// For Getting the selected RA ID from the Returns Credited Info Screen to the
// Reason Text box in System Qty Section in Flag Negative Quantity Screen
function fnLoadRADetails(id, refid) {
	eval("document.all.txt_tnx" + refid).focus();
	eval("document.all.txt_tnx" + refid).value = id;
}

// For validating the Reason Text Box, if the user select/entered duplicate RA
// ID's, it should through validation for not selecting same RA ID's
function fnVlaidateRA(cnt) {
	var Errorcount = 0;
	var reasonId = eval('document.all.txt_tnx' + cnt).value;
	var cntNeg = document.getElementById("countNeg");
	if (cntNeg != null) { // negative Deviation start at 600
		for ( var i = 600; i <= cntNeg.value; i++) {
			var flagid = eval('document.all.cbo_Flag' + i).value;
			if (flagid == '51029') {
				var reasonValues = eval('document.all.txt_tnx' + i).value;
				if (reasonId == reasonValues && i != cnt) {
					Errorcount++;
				}
			}
		}
	}
	if (Errorcount > 0) {
		Error_Details(message_accounts[37]);
		Error_Show();
		eval('document.all.txt_tnx' + cnt).value = '';
		Error_Clear();
		return false;
	} else {
		return true;
	}

}
//To close the flag deviation screen
function fnClose() {
	parent.window.fnSubmit();
	parent.dhxWins.window("w1").close();
	return false;
}
