// This function will call when - Audit id change 
function fnReload() {
	document.frmFieldAuditConfirmation.strOpt.value = "reload";
	document.frmFieldAuditConfirmation.submit();
}
// This function will call when - Click on load button -- (Report show the JSP page)
function fnLoad() {
	fnValidateDropDn('auditId', lblAuditList);
	fnValidateDropDn('distId', lblDist);

	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	document.frmFieldAuditConfirmation.strOpt.value = "Load";
	fnStartProgress();
	document.frmFieldAuditConfirmation.submit();
}
// This function will call when - Click on Generate button - (pop up show - PDF)
function fnSubmit() {
	fnValidateDropDn('auditId', lblAuditList);
	fnValidateDropDn('distId', lblDist);
	fnValidateDropDn('pdfAction', lblChooseAction);

	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	document.frmFieldAuditConfirmation.strOpt.value = "PDF";
	document.frmFieldAuditConfirmation.submit();
}
//This function used to send the email (Confirmation sheet - Overall reports)
function fnSendEmail(){
	fnValidateDropDn('auditId', lblAuditList);
	fnValidateDropDn('distId', lblDist);
	fnValidateDropDn('pdfAction', lblChooseAction);

	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	document.frmFieldAuditConfirmation.strOpt.value = "SendMail";	
	document.frmFieldAuditConfirmation.submit();
	document.frmFieldAuditConfirmation.btn_sendMail.disabled = true;
}
// This function to be disabled the send email button when select 'Overall Report'
function fnOnChange(obj){
	if(obj.value == 18585 || SwitchUserFl == 'Y'){ // Overall Reports
		document.frmFieldAuditConfirmation.btn_sendMail.disabled = true;
	}else{
		document.frmFieldAuditConfirmation.btn_sendMail.disabled = false;
	}
}