//Description: function will call when enter key pressed.
function fnEnter(){
		if (event.keyCode == 13){ 
			fnLoad();
		}
}

//Description: This function will call onclick load button. check the validation & pass control to action.
function fnLoad() {
	
	var lockedFromDate = document.frmFieldAuditSummary.faSumFromDt.value;
	var lockedToDate = document.frmFieldAuditSummary.faSumToDt.value;

    //DateValidateMMDDYYYY(document.getElementById('faSumFromDt'),'<b>Locked From Date</b> ' + lockedFromDate.trim() + ' is not a valid date in the format MM/DD/YYYY');
    //DateValidateMMDDYYYY(document.getElementById('faSumToDt'),'<b>Locked To Date</b> ' + lockedToDate.trim() + ' is not a valid date in the format MM/DD/YYYY');
    
    CommonDateValidation(document.getElementById('faSumFromDt'),format,Error_Details_Trans(message_accounts[65],format));
	CommonDateValidation(document.getElementById('faSumToDt'),format,Error_Details_Trans(message_accounts[66],format));

    if (ErrorCount > 0)
    {
		Error_Show();
        Error_Clear();
		return false;
    }
	document.frmFieldAuditSummary.action = "/gmFieldAuditSummaryAction.do?method=execute";
	fnStartProgress();
	document.frmFieldAuditSummary.submit();
}

//Description: This function used for initializing the tree.
function fnOnPageLoad() {
	if(objGridData!=''){
		mygrid = new dhtmlXGridObject('GridData');
		mygrid.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
		mygrid.init();
		mygrid.setSkin("dhx_skyblue");
		mygrid.loadXMLString(objGridData);
		mygrid.attachHeader("#text_search,#rspan,#select_filter,#numeric_filter,#text_filter,#numeric_filter,#numeric_filter,#numeric_filter,#numeric_filter");
		mygrid.setColTypes("tree,ro,ro,ro,ro,ron,ron,ron,ron");
    }
}

//Description: This function will used to generate excel.
function fnDownloadXLS() {
	mygrid.expandAll();
	mygrid.toExcel('/phpapp/excel/generate.php');
	mygrid.collapseAll();
}

//Description: Method to take the Field Audit Summary Screen after click on the Counted Qty to Uploaded data Screen.
function fnOpenUploadedData(auditID,distID,strOpt,countBy)
{   
	windowOpener("/gmFieldAuditEditUploadData.do?method=verifyUploadData&auditId="+ auditID + "&distId=" + distID + "&strOpt="+strOpt + "&auditorId=" + countBy,
			"AuditHistory","resizable=yes,scrollbars=yes,top=150,left=250,width=1550,height=750");
}