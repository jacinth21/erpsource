var InvID_Ind	 = '';		var AccID_Ind 		= '';
var AccNM_Ind 	 = '';		var InvDT_Ind 		= '';
var CustPO_Ind 	 = '';		var PayMode_Ind  = '';
var Comments_Ind = '';		var Curr_Ind 		= '';
var InvAmt_Ind 	 = '';		var AmtRec_Ind 		= '';
var TotOutSt_Ind = '';		var AmtPost_Ind 	= '';
var AmtDis_Ind	 = '';      var pamenttype      = '';
var blAjaxCall = false;     var acc_Type        = '';
var invType_Ind = '';
function fnOnPageLoad() {
		var footer=new Array();
		footer[0]="";
		footer[1]="";
		footer[2]="";
		footer[3]="";
		footer[4]="";
		footer[5]="";
		footer[6]="";
		footer[7]="";
		if(arDealerFlag == 'YES'){
			footer[8]="";
			footer[9]= message_accounts[100];
			footer[10]="<b><div id='inv_Amt'>0</div></b>";
			footer[11]="<b><div id='amt_Rec'>0</div></b>";
			footer[12]="<b><div id='tot_Out'>0</div></b>";
			footer[13]="<b><div id='amt_Post'>0</div></b>";
			footer[14]="<b><div id='amt_Dis'>0</div></b>";
		}
		else
			{
			footer[8]= message_accounts[100];
			footer[9]="<b><div id='inv_Amt'>0</div></b>";
			footer[10]="<b><div id='amt_Rec'>0</div></b>";
			footer[11]="<b><div id='tot_Out'>0</div></b>";
			footer[12]="<b><div id='amt_Post'>0</div></b>";
			footer[13]="<b><div id='amt_Dis'>0</div></b>";
			}

		
		gridObj = initGridWithDistributedParsing('issueMemoRpt',objGridData,footer);
		if(arDealerFlag == 'YES'){
		gridObj.enableTooltips("true,true,true,true,true,true,true,true,true,true,true,true");
		}
		else{
		gridObj.enableTooltips("true,true,true,true,true,true,true,true,true,true,true");
		}
		gridObj.attachEvent("onEditCell", doOnCellEdit);
		
		// declare global variable and set index value.
		 InvID_Ind = gridObj.getColIndexById("InvID");
		 AccID_Ind = gridObj.getColIndexById("AccID");
		 AccNM_Ind = gridObj.getColIndexById("AccNM");
		 InvDT_Ind = gridObj.getColIndexById("InvDT");
		 CustPO_Ind = gridObj.getColIndexById("CustPO");
		 PayMode_Ind = gridObj.getColIndexById("payMode");
		 Comments_Ind = gridObj.getColIndexById("Comments");
		 Curr_Ind = gridObj.getColIndexById("Curr");
		 InvAmt_Ind = gridObj.getColIndexById("InvAmt");
		 AmtRec_Ind = gridObj.getColIndexById("AmtRec");
		 TotOutSt_Ind = gridObj.getColIndexById("TotOutSt");
		 AmtPost_Ind = gridObj.getColIndexById("AmtPost");
		 AmtDis_Ind = gridObj.getColIndexById("AmtDis");
		 invType_Ind = gridObj.getColIndexById("invType");
		 pamenttype = gridObj.getColIndexById("payMode");
		 if(arDealerFlag == 'YES'){
		 acc_Type = gridObj.getColIndexById("AccType");
			
		// For Single click 
		
		gridObj.enableEditEvents(true, false, false,false,false,false,true,true,false,false,false,false,true,true);
		}
		else
		{
		gridObj.enableEditEvents(true, false, false,false,false,true,true,false,false,false,false,true,true);
		}
		// For enable the tab order
		gridObj.enableEditTabOnly(true);

		var gridrows = gridObj.getAllRowIds(",");
		if (gridrows.length == 0) {
			for (i = 0; i <= 9; i++) {
				addRow();
			}
		}
	}



	//Description : This function used to add a row(grid)
	function addRow() {
			gridObj.addRow(myRowId, '');
			gridObj.setCellTextStyle(myRowId,InvID_Ind,"background-color:white;  border-width: 1px;border-style: inset;");
			gridObj.cellById(myRowId, PayMode_Ind).setValue("5010");
			gridObj.setCellTextStyle(myRowId,Comments_Ind," background-color:white; border-width: 1px;border-style: inset;");
			gridObj.setCellTextStyle(myRowId,AmtPost_Ind,"background-color:white; border-width: 1px;border-style: inset;");
			gridObj.setCellTextStyle(myRowId,AmtDis_Ind,"background-color:white; border-width: 1px;border-style: inset;");
			myRowId = myRowId +1;
	}

	//Description : This function used to remove a row(grid)
	function removeSelectedRow() {
		var gridrows=gridObj.getSelectedRowId();	
		if(gridrows!=undefined){
			var gridrowsarr = gridrows.toString().split(",");
			for (var rowid = 0; rowid < gridrowsarr.length; rowid++) {
				gridrowid = gridrowsarr[rowid];			
				gridObj.deleteRow(gridrowid);	
				// footer total calculated
				fnSetFooterVal();
			}
		}else{
			Error_Clear();
			Error_Details(message_accounts[58]);				
			Error_Show();
			Error_Clear();
		}
	}

	//Description : This function to fire the cell edit event. To get the Tag qty
	function doOnCellEdit(stage, rowId, cellInd, nValue, oValue) {
		var invId ='';
		if (stage == 2){
			rowID = rowId;
			if (cellInd == 0) {
				if (nValue != oValue && nValue !='') {
					var gridrows = gridObj.getAllRowIds();
					var gridrowsarr = gridrows.toString().split(",");
					for (var rowid = 0; rowid < gridrowsarr.length; rowid++){
						gridrowid = gridrowsarr[rowid];
						if(gridrowid != rowID){
							var OldInvId = gridObj.cellById(gridrowid,InvID_Ind).getValue();
							if(nValue == OldInvId){
								Error_Details(message_accounts[101]);
								setTimeout(function(){fnShowError()},5);
								return true;		
							}
						}
					}
					fngetInvDtls(nValue);
					// footer total is calculated
					fnSetFooterVal();
				}
				if (nValue == '') {
					fnClearValues();
				}
			}else if(cellInd == AmtPost_Ind) {
				if (nValue != oValue) {
					invId = gridObj.cellById(rowId, InvID_Ind).getValue();
					if(invId !=''){
						fnNumberValidate(rowID,cellInd,nValue,oValue);
						var amtToPostVal = document.getElementById("amt_Post");
						amtToPostVal.innerHTML = formatNumber(sumColumn(AmtPost_Ind));
					}				
				}
			}else if(cellInd == AmtDis_Ind) {
				if (nValue != oValue) {
					invId = gridObj.cellById(rowId, InvID_Ind).getValue();
					if(invId !=''){
						fnNumberValidate(rowID,cellInd,nValue,oValue);
						var amtDisVal = document.getElementById("amt_Dis");
						amtDisVal.innerHTML = formatNumber(sumColumn(AmtDis_Ind));
					}				
				}
			}
		}
		return true;
	}

	function fnClearValues(){
		gridObj.cellById(rowID, InvID_Ind).setValue("");
		gridObj.cellById(rowID, AccID_Ind).setValue("");
		gridObj.cellById(rowID, AccNM_Ind).setValue("");
		gridObj.cellById(rowID, InvDT_Ind).setValue("");
		if(arDealerFlag == 'YES'){
			gridObj.cellById(rowID, acc_Type).setValue("");
		}
		
		gridObj.cellById(rowID, CustPO_Ind).setValue("");
		gridObj.cellById(rowID, PayMode_Ind).setValue("5010");
		gridObj.cellById(rowID, Comments_Ind).setValue("");
		gridObj.cellById(rowID, Curr_Ind).setValue("");
		gridObj.cellById(rowID, InvAmt_Ind).setValue("");
		gridObj.cellById(rowID, AmtRec_Ind).setValue("");
		gridObj.cellById(rowID, TotOutSt_Ind).setValue("");
		gridObj.cellById(rowID, AmtPost_Ind).setValue("");
		gridObj.cellById(rowID, AmtDis_Ind).setValue("");
		gridObj.cellById(rowID, invType_Ind).setValue("");
		// footer total is calculated
		fnSetFooterVal();
	}
	//Description : This function used to get the tag qty.
	function fngetInvDtls(invID) {
		blAjaxCall = true;
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmCashApplication.do?method=fetchInvoiceDtls&strInvoiceID=' + TRIM(invID) +'&ramdomId=' + Math.random());
		dhtmlxAjax.get(ajaxUrl, fnLoadInvDtls);
	}

//Description : This function used to load the tag qty on selected field sales
function fnLoadInvDtls(loader) {
		var response = loader.xmlDoc.responseXML;
		var strAccName = '';
		var strQty = '';
		var strPriceEa = '';
		// PC-4687 A/R Module Edge Cash Application
		if (response != null) {
		var InvDet = response.getElementsByTagName("pnum");
		var arSelectedCurr = document.frmCashApp.strCompCurrency.value;
		if(InvDet.length == '0'){
	   		Error_Details(message_accounts[102]);
	   	}
		for (var x=0; x<InvDet.length; x++){
			   	strInvID = parseXmlNode(InvDet[x].childNodes[0].firstChild);
			   	 strAccID = parseXmlNode(InvDet[x].childNodes[2].firstChild); 
				 strAccName = parseXmlNode(InvDet[x].childNodes[1].firstChild);
				 strInvDt = parseXmlNode(InvDet[x].childNodes[3].firstChild);
				 strPO = parseXmlNode(InvDet[x].childNodes[4].firstChild);
				 strCurrency = parseXmlNode(InvDet[x].childNodes[5].firstChild);
				 strInvAmt = parseXmlNode(InvDet[x].childNodes[6].firstChild);
				 strAmtRec = parseXmlNode(InvDet[x].childNodes[7].firstChild);
				 strTotalOut = parseXmlNode(InvDet[x].childNodes[12].firstChild);
				 strInvType = parseXmlNode(InvDet[x].childNodes[8].firstChild);
				 strStatusFl = parseXmlNode(InvDet[x].childNodes[9].firstChild);
				 var strARCurrId = parseXmlNode(InvDet[x].childNodes[10].firstChild);
				 
				 if(arDealerFlag == 'YES'){
				 var strAccType = parseXmlNode(InvDet[x].childNodes[11].firstChild);
				 }
			 // alert(strAccID+"::"+strAccName+"::"+strInvDt+"::"+strPO+"::"+strCurrency+"::"+strInvAmt+"::"+strAmtRec+"::"+strTotalOut+"::"+strInvType+"::"+strStatusFl+"::")
		   	
			if(strInvType =='50200'){ // Regular
			   	gridObj.setRowTextStyle(rowID, "color:green;");
		   	}else if(strInvType =='50201'){ // OBO
		   		gridObj.setRowTextStyle(rowID, "color:purple;");
		   	}else if(strInvType =='50202'){ // Credit
		   		gridObj.setRowTextStyle(rowID, "color:red;");
		   	}else if(strInvType =='50203'){ // Debit
		   		gridObj.setRowTextStyle(rowID, "color:blue;");
		   	}

			// status flag value is 2 closed invoice.
		   	if(strStatusFl == 2){
		   		Error_Details(Error_Details_Trans(message_accounts[103],strInvID));
		    }
		   	//Currency symbol validation
			if(strARCurrId!=arSelectedCurr){
				Error_Details(message_accounts[104]);
			}
		   	
		   	gridObj.cellById(rowID, AccID_Ind).setValue(strAccID);
		   	gridObj.cellById(rowID, AccNM_Ind).setValue(strAccName);
		   	if(arDealerFlag == 'YES'){
				gridObj.cellById(rowID, acc_Type).setValue(strAccType);
			}
			gridObj.cellById(rowID, InvDT_Ind).setValue(strInvDt);
			gridObj.cellById(rowID, CustPO_Ind).setValue(strPO);
			
			//gridObj.cellById(rowID, PayMode_Ind).setValue("5010"); // Check:5010
			gridObj.cellById(rowID, Curr_Ind).setValue(strCurrency);
			gridObj.cellById(rowID, InvAmt_Ind).setValue(formatNumber(strInvAmt));
			gridObj.cellById(rowID, AmtRec_Ind).setValue(formatNumber(strAmtRec));
			gridObj.cellById(rowID, TotOutSt_Ind).setValue(formatNumber(strTotalOut));
			gridObj.cellById(rowID, invType_Ind).setValue(strInvType);

			// footer total is calculated
			fnSetFooterVal();

		    gridObj.setCellTextStyle(rowID,InvID_Ind,"background-color:white;  border-width: 1px;border-style: inset;");
			gridObj.setCellTextStyle(rowID,Comments_Ind," background-color:white; border-width: 1px;border-style: inset;");
			gridObj.setCellTextStyle(rowID,AmtPost_Ind,"background-color:white; border-width: 1px;border-style: inset;");
			gridObj.setCellTextStyle(rowID,AmtDis_Ind,"background-color:white; border-width: 1px;border-style: inset;");
		   }// For Loop
		} else {
			Error_Details(message_accounts[102]);
		}
		blAjaxCall = false;

		   if (ErrorCount > 0) {
			   fnClearValues();
			   Error_Show();
			   Error_Clear();
			   return false;
		   }
}// End function

	function parseNull(val){
		if(val == 'null') return val = "";
		else return val; 
	}
	
	//When submit will call following function. 
	function fnSubmit(){
		var inputStr = "";
		var gridrowid;
		var errmessage = '';
		var blFlag = true;
		var payamt = '';
		var emptyToPost = '';
		var emptyToPaymode = '';
		var maxAmountToPost = '';
		var invTypeVal = '';
		var paidInvoices = '<br>';
		var amtDisVal = document.getElementById("amt_Dis");
		var amtToPostVal = document.getElementById("amt_Post");
		var invAmtVal = document.getElementById("inv_Amt");
		var commentsStr = '';
		var notFullyPostStr = '';

		fnValidateTxtFld('paymentDt', lblPayDate);
		var frmPaymentObj = document.frmCashApp.paymentDt;
		CommonDateValidation(frmPaymentObj,format,message_accounts[105]+ message[611]);
		//PC-4637-payment date validation
		var preDiff = dateDiff(strFromdaysDate,frmPaymentObj.value, format);
		var futDiff = dateDiff(strLastdaysDate,frmPaymentObj.value, format);
		gridObj.editStop();
	    gridObj.forEachRow(function(rowId) {
	    	invId = gridObj.cellById(rowId, InvID_Ind).getValue();
			if(invId != ''){
				accId	 = gridObj.cellById(rowId, AccID_Ind).getValue();
				poId 	 = gridObj.cellById(rowId, CustPO_Ind).getValue();
				paymode  = gridObj.cellById(rowId, PayMode_Ind).getValue();
				comments = gridObj.cellById(rowId, Comments_Ind).getValue();
				InvAmt 	 = parseFloat(RemoveComma(gridObj.cellById(rowId, InvAmt_Ind).getValue()));
				amtPost  = parseFloat(RemoveComma(gridObj.cellById(rowId, AmtPost_Ind).getValue()));
				disCount = parseFloat(RemoveComma(gridObj.cellById(rowId, AmtDis_Ind).getValue()));
				amtRec   = parseFloat(RemoveComma(gridObj.cellById(rowId, AmtRec_Ind).getValue()));
				TotOut   = parseFloat(RemoveComma(gridObj.cellById(rowId, TotOutSt_Ind).getValue()));
				invTypeVal = gridObj.cellById(rowId, invType_Ind).getValue();
				var purpose = TRIM(gridObj.cellById(rowId, pamenttype).getValue());
				if(isNaN(amtRec)) 	amtRec = 0;
				if(isNaN(amtPost)) 	amtPost = 0;
				if(isNaN(disCount)) disCount = 0;
				
				TotalPostAmt = amtRec + amtPost + disCount;
				TotalPostAmt = fnRoundNumber (TotalPostAmt, 2);  //Round off the total Post amount
				blFlag = true;
				if(invTypeVal =='50202'){ // Credit
					if ((amtPost > 0)|| (disCount > 0) || (InvAmt > TotalPostAmt)){
						blFlag = false;
					}
				}else{
					if((amtPost < 0)|| (disCount < 0)){
						blFlag = false;
					}
				}
				if (TotalPostAmt >= 0 && (InvAmt < TotalPostAmt)){
					blFlag = false;
				}
				
				if(paymode == 0 && amtPost != 0){
					paymode = '';
					emptyToPaymode = emptyToPaymode + invId +',';
				}
				if(amtPost == '' || amtPost == '0.00'){
					emptyToPost = emptyToPost + invId +',';
				}
				// to validate the comments field
				if (comments.length > 500){
					commentsStr = commentsStr + invId + ',';
				}
				// to validate the full amount paid invoice 
				if (TotalPostAmt != InvAmt){
					notFullyPostStr = notFullyPostStr + invId + ',';
				}
				
				if ((!blFlag && amtPost !='')){
					maxAmountToPost = maxAmountToPost + invId +',';
				}else{
					payamt = payamt + parseFloat(amtPost) //+ parseFloat(discount); - commenting this for issue 2579
					blFlag = true;
				}
					//Add as part of PMT-8548
					if (purpose == 5010 || purpose == 5011 || purpose == 5012 || purpose == 5023 || purpose == 26240161 || purpose == 26240162) {
						//5010-check , 5011-Bank Transfer , 5012-Credit card , 5023- Wire transfer
						// 2700 -- Payment
						inputStr = inputStr + invId + '^' + poId + '^' + paymode +'^' + comments +'^' +amtPost + '^' + '2700'+ '|';
						
						if(disCount != '' || disCount != '0.00'){
							inputStr = inputStr + invId + '^' + poId + '^' + '5013' +'^' + comments +'^' +disCount + '^' + '2701'+ '|';
						}
					}else{
						
						inputStr = inputStr + invId + '^' + poId + '^' + paymode + '^' + comments +'^' + amtPost + '^' + '2701' + '|';
						
						if(disCount != '' || disCount != '0.00'){
							inputStr = inputStr + invId + '^' + poId + '^' + '5013' +'^' + comments +'^' +disCount + '^' + '2701'+ '|';
						}
					}
				
				
				/*if (purpose == 5010 || purpose == 5011 || purpose == 5012 || purpose == 5023) {
					//5010-check , 5011-Bank Transfer , 5012-Credit card , 5023- Wire transfer
					// 2700 -- Payment
					inputStr = inputStr + invId + '^' + poId + '^' + paymode +'^' + comments +'^' +amtPost + '^' + '2700'+ '|';
				}else{
			 // 2701 -- Discount
					inputStr = inputStr + invId + '^' + poId + '^' + paymode + '^' + comments +'^' + amtPost + '^' + '2701' + '|';
				}*/
				paidInvoices = paidInvoices + invId +', ';
			}
		});

	    if(emptyToPost !=''){
	    	emptyToPost = emptyToPost.substring(0,(emptyToPost.length)-1);
	    	Error_Details(Error_Details_Trans(message_accounts[106],emptyToPost));
	    }
	    if(emptyToPaymode !='')
	    	{
	    	emptyToPaymode = emptyToPaymode.substring(0,(emptyToPaymode.length)-1);
	    	Error_Details(Error_Details_Trans(message_accounts[532],emptyToPaymode));
	    	}
	    if(maxAmountToPost !=''){
	    	maxAmountToPost = maxAmountToPost.substring(0,(maxAmountToPost.length)-1);
	    	Error_Details(Error_Details_Trans(message_accounts[107],maxAmountToPost));
	    }
	    if (commentsStr !=''){
	    	commentsStr = commentsStr.substring(0,(commentsStr.length)-1);
	    	Error_Details(Error_Details_Trans(message_accounts[209],commentsStr));
	    }
	    
	    if (notFullyPostStr !=''){
	    	notFullyPostStr = notFullyPostStr.substring(0,(notFullyPostStr.length)-1);
	    }
	    if(blAjaxCall){
			Error_Details(message_accounts[108]);
		}
	    if(inputStr == '' && ErrorCount == 0){
	    	Error_Details(message_accounts[109]);
	    }
	    //PC-4637-payment date validation
	    if (paymentValidationFl =='Y' && (preDiff < 0 || futDiff > 0)) {
	    	Error_Details(message_accounts[557]);
	    }
		// we are using the existing procedure (used VARCHAR2) so check the flow condition.
	    /*if(inputStr.length > 4000){
			Error_Details(message_accounts[110]);
		}*/
	    // remove the last comma.
	    if(paidInvoices !=''){
	    	paidInvoices = paidInvoices.substring(0,(paidInvoices.length)-2);
	    }
	    
		if (ErrorCount > 0) {
			Error_Show();
			Error_Clear();
			return false;
		}
		//if an amount is not fully posted show the confirmation message. 
	    if(notFullyPostStr !=''){
	    	if(!confirm(message_accounts[208] + notFullyPostStr + message_accounts[210])){
			 	return false;
		 	}
	    }
		fnStartProgress("Y");
		document.frmCashApp.paymentAmt.value = RemoveComma(amtToPostVal.innerHTML);
		document.frmCashApp.inputString.value = inputStr;
		document.frmCashApp.paidInvoiceIDs.value = paidInvoices;
		document.frmCashApp.strOpt.value = "Save";
		document.frmCashApp.action = "/gmCashApplication.do?method=saveCashAppDtls";
		document.frmCashApp.submit();
	}

	function sumColumn(ind) {
	    var out = 0;
	    var invId = '';
	    gridObj.forEachRow(function(rowId) {
	    	invId = gridObj.cellById(rowId, InvID_Ind).getValue();
			if(invId != ''){
				var total = RemoveComma(gridObj.cellById(rowId, ind).getValue());
			    if(total !=''){			    
			    	out = out + parseFloat(total);
				}	        
			}
	    });
	    return out;
	}
	

	
function fnNumberValidate(rowID,cellInd,nValue,oValue){
	if(cellInd == AmtPost_Ind) gridObj.cellById(rowID, AmtPost_Ind).setValue(formatNumber(nValue));
	if(cellInd == AmtDis_Ind) gridObj.cellById(rowID, AmtDis_Ind).setValue(formatNumber(nValue));
}

//This function used to set the footer total values.
function fnSetFooterVal (){
	// footer total is calculated
	var invAmtVal = document.getElementById("inv_Amt");
	invAmtVal.innerHTML = formatNumber(sumColumn(InvAmt_Ind));
    var amtRecor = document.getElementById("amt_Rec");
    amtRecor.innerHTML = formatNumber(sumColumn(AmtRec_Ind));
    var totalOut = document.getElementById("tot_Out");
    totalOut.innerHTML = formatNumber(sumColumn(TotOutSt_Ind));
    var amtToPostVal = document.getElementById("amt_Post");
	amtToPostVal.innerHTML = formatNumber(sumColumn(AmtPost_Ind));
    var amtDisVal = document.getElementById("amt_Dis");
	amtDisVal.innerHTML = formatNumber(sumColumn(AmtDis_Ind));
}
//This function used to press the enter key then submit the page.
function fnEnter() {
	if (event.keyCode == 13) {
		fnSubmit();
	}
}

//This function is used to show the error message.
//Enable the tab order so, the error message go back, here set the time out at
//the time only this function call
function fnShowError() {
	if (ErrorCount > 0) {
		fnClearValues();
		Error_Show();
		Error_Clear();
		return false;
	}
}