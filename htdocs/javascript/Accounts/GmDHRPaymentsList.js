function fnSubmit()
{
    var vendid = document.frmAccount.Cbo_VendorId.value;
	var poid = document.frmAccount.Txt_POId.value;
	var invid = document.frmAccount.Txt_InvId.value;
	
	if (poid == '' && invid == '')
	{
	    if (vendid == 0)
	    {
	    	if (document.frmAccount.Txt_FromDate.value == '' && document.frmAccount.Txt_FromDate.value == '')
	    	{
	    		Error_Details(message_accounts[126]);
	    	}
	    	else
	    	{
	    		diff = compareDates(document.frmAccount.Txt_FromDate.value,document.frmAccount.Txt_ToDate.value);
	    		if (diff > 6)
	    		{
	    			Error_Details(message_accounts[127]);
	    		}
	    	}
	    }
	}
     
    if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	
    document.frmAccount.hAction.value = "ReloadPayReport";
    fnStartProgress();
	document.frmAccount.submit();
}

function fnCallDHR(id)
{
	var venId = document.frmAccount.Cbo_VendorId.value;
	windowOpener("/GmPOReceiveServlet?hAction=ViewDHR&hDHRId="+id,"INVPOP","resizable=yes,scrollbars=yes,top=300,left=300,width=750,height=750");
}

