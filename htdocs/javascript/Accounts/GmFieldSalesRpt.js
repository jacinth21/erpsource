function fnEnter()
{
		if (event.keyCode == 13)
		{ 
			fnSubmit();
		}
}
function fnReload()
{	
	if (document.frmFieldSalesRptForm.distId){		
		document.frmFieldSalesRptForm.submit();
	}
}
function fnSubmit()
{
	fnValidateDropDn('auditId',message_accounts[199]);
	if (document.frmFieldSalesRptForm.distId){
		fnValidateDropDn('distId', message_accounts[200]);
	}
	if (document.frmFieldSalesRptForm.setId){
		fnValidateDropDn('setId', message_accounts[201]);
	}
	if (document.frmFieldSalesRptForm.devQty){
		NumberValidation(document.all.devQty.value,message_accounts[133],0);
	}
	if (ErrorCount > 0)  
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	document.frmFieldSalesRptForm.strOpt.value = "load";
	fnStartProgress();
	document.frmFieldSalesRptForm.submit();
}

function fnOpenVarianceBySet(value){
	auditId = document.frmFieldSalesRptForm.auditId.value;
	reportType ='';
	document.frmFieldSalesRptForm.action="/gmFieldSalesRpt.do?method=reportVarianceRptBySet&strOpt=load&reportType="+reportType+"&auditId="+auditId+"&setId="+value;
	document.frmFieldSalesRptForm.submit();
}

function fnOpenVarianceByDist(value){
	auditId = document.frmFieldSalesRptForm.auditId.value;
	reportType ='';
	document.frmFieldSalesRptForm.action="/gmFieldSalesRpt.do?method=reportVarianceByDist&strOpt=load&reportType="+reportType+"&auditId="+auditId+"&distId="+value;
	document.frmFieldSalesRptForm.submit();
}

function fnOpenFlagVariance(distid,setid,intVarianceQty){	
	auditId = document.frmFieldSalesRptForm.auditId.value;
	windowOpener("/gmFlagVariance.do?auditId="+auditId+"&distId="+distid+"&unResolvedQty="+intVarianceQty+"&setId="+setid,"ViewInv","resizable=yes,scrollbars=yes,top=150,left=200,width=1100,height=600");
	//document.frmFieldSalesRptForm.action.value="/gmFlagVariance.do"
}

function fnSelect(val){
	document.frmFieldSalesRptForm.hauditEntryID.value = val;
}

function fnChooseAction(){
	val = document.frmFieldSalesRptForm.Cbo_Action.value;
	auditEntryID=document.frmFieldSalesRptForm.hauditEntryID.value;
	distID = document.frmFieldSalesRptForm.distId.value;

	if (val == '0')
	{
		Error_Details(message[4000]);
	}
	if (auditEntryID =='')
	{
		Error_Details(message[4001]);
	}
	if (ErrorCount > 0)  
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	document.frmFieldSalesRptForm.action="/gmEditAuditTag.do?strOpt=onload_edit_audit_tag&auditEntryID="+auditEntryID+"&distID="+distID;
	document.frmFieldSalesRptForm.submit();
}

function fnOpenLog(id,type)
{
	windowOpener("/GmCommonLogServlet?hType="+type+"&hID="+id,"ConsignLog","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=300");
}
