var gridObj;
function fnOnPageLoad() {
	if (objGridData.value != '') {
		gridObj = initGridData('OrderCmtLog', objGridData);
	}
}
function initGridData(divRef, gridData) {

	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/extweb/dhtmlx/dhtmlxGrid/imgs/");
	gObj.init();
	gObj.loadXMLString(gridData);
    gObj.attachHeader('#rspan,#rspan,#rspan,#rspan,#rspan,#rspan');   
    //gObj.groupBy(0);
	gObj.enableTooltips("false,false,false,false,false,false"); 
    return gObj;
}

//To download excel;
function fnExcel() { 
	gridObj.toExcel('/phpapp/excel/generate.php');
}
function fnSubmit()
{
	fnStartProgress('Y');
	document.frmAccount.submit();
}
function fnEnterPress(evt){
	evt = (evt) ? evt : window.event
	if(evt.keyCode==13){
		fnSubmit();
	}
}