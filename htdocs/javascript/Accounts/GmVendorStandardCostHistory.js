
/*Load History information using dhtmlx */
var gridObj;
function fnOnPageLoad(){
	if(objGridData.value != ''){ 
		gridObj = initGridData('dataGridDiv',objGridData);
	    gridObj.attachHeader('#rspan,#rspan,#rspan,#rspan,#rspan,#rspan,#rspan');
	    gridObj.enableAutoHeight(true,500,true);
		gridObj.objBox.style.overflowX = "hidden";
	 
	
	}	

}
/*Initialize Grid*/
function initGridData(divRef,gridData){
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);
	gObj.enableTooltips("true,true,true,true,true");
	gObj.init();
	gObj.loadXMLString(gridData);
	return gObj;	
}
