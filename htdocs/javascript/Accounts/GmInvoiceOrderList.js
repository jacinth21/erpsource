function fnSubmit()
{
	var repType  = document.all.Cbo_InvRepType.value ;
	var FromDT= document.frmAccount.Txt_FromDate.value;
	var ToDT= document.frmAccount.Txt_ToDate.value;
	var parentFl  = document.frmAccount.Chk_ParentFl.checked;
	if(repType=='1' && (FromDT!="" || ToDT!="")){ //Report Type "open"
	Error_Details(message_accounts[161]);
	}else if(repType!="1"){	
	fnValidateTxtFld('Txt_FromDate',message_accounts[257]);
	fnValidateTxtFld('Txt_ToDate',message_accounts[258]);
	}
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	fnStartProgress('Y');
    document.frmAccount.hAction.value = "Reload";
	document.frmAccount.submit();
}

function fnChange(val)
{
	if(val=="1"){
		document.frmAccount.Txt_FromDate.value="";
		document.frmAccount.Txt_ToDate.value="";
	}
}

function fnPrintInvoice(varinv)
{
windowOpener('/GmInvoiceInfoServlet?hAction=Print&hInv='+varinv,"PrntInv","resizable=yes,scrollbars=yes,top=150,left=200,width=750,height=500");
}

function fnPrintPack(val)
{
	windowOpener('<%=strServletPath%>/GmEditOrderServlet?hMode=PrintPack&hOrdId='+val,"Pack","resizable=yes,scrollbars=yes,top=150,left=150,width=760,height=610");	
}

function fnPrintCreditMemo(val)
{
	windowOpener('<%=strServletPath%>/GmReportCreditsServlet?hAction=PrintVersion&hOrderId='+val,"Credit","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnPrintCashAdjustMemo(val)
{
	windowOpener('<%=strServletPath%>/GmReportCreditsServlet?hAction=PrintCashAdj&hOrderId='+val,"Credit","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnLoad1()
{
	
	 if(parentFl == 'false'){
			document.frmAccount.Chk_ParentFl.checked = false;
		}
	if (invRepType!= '')
	{
		document.all. Cbo_InvRepType.value = invRepType;
	}

	if (loadScreen=="LoadOrderList")
	{
		// To defaul to all order 
		if (orderType=="") 
		{
			orderType = "1";
		}

		document.frmAccount.Cbo_OrderType.value = orderType;

	} else if(callFrom==""){
		//we implement the auto complete functionality - so, not required to call below function.
		fnDispatchLoad();
	} 
	if(objGridData.indexOf("cell") !='-1'){
		gridObj = initGridData('invoiceList', objGridData);
	}
}

function initGridData(divRef,gridData)
{
	var footer=new Array();
	footer[0]="";
	footer[1]="";
	footer[2]="";
	footer[3]="";
	footer[4]="";
	footer[5]="";
	footer[6]="";
	footer[7]="";
	footer[8]="";
	footer[9]= message_accounts[180];
	footer[10]="<b>{#stat_total}<b>";
	footer[11]="<b>{#stat_total}<b>";
	footer[12]="<b>{#stat_total}<b>";
	footer[13]="<b>{#stat_total}<b>";
	footer[14]="<b>{#stat_total}<b>";
	footer[15]="";
	footer[16]="";
	
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
    gObj.enableDistributedParsing(true);
	gObj.enableTooltips("true,true,true,true,true,true,true,true,true, true,true,true,true,true,true,true,true");
	gObj.attachHeader("#select_filter,#text_filter,#text_filter,#select_filter,#text_filter,#text_filter,#select_filter,#select_filter,#text_filter,#select_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#select_filter,#select_filter");
	gObj.attachFooter(footer);
	gObj.init();	
	gObj.enablePaging(true, 100, 10, "pagingArea", true);
	gObj.setPagingSkin("bricks");
	gObj.loadXMLString(gridData);	
	return gObj;	
}

function fnExport()
{	
	gridObj.toExcel('/phpapp/excel/generate.php');
}


function fnLoad()
{
	var varSource = '<%=strInvSource%>';
	var varIdAccount = '<%=strIdAccount%>';
	
	if (varSource != '')
	{
		document.all.Cbo_InvSource.value = varSource;
	}
	if (varIdAccount!='')
	{
		document.all.idAccount.value = varIdAccount;
	}
	
	var obj = document.frmAccount.Cbo_Action;
	for (var i=0;i<obj.length ;i++ )
	{
		if (document.frmAccount.hMode.value == ooptions[i].value)
		{
			obj.options[i].selected = true;
			break;
		}
	}

}