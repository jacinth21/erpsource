var inpStr = '';
function fnSubmit(){
	fnValidation();
	var objval = TRIM(document.frmFieldSalesConAdj.pnumStr.value);
	inpStr = '';
	if(objval !=''){
		fnCreateInputString();
	}
	if(ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}else{

		document.frmFieldSalesConAdj.pnumStr.value = inpStr;
		document.frmFieldSalesConAdj.strOpt.value = 'save';
		fnStartProgress();
		document.frmFieldSalesConAdj.submit();
	}
	
}

function fnValidation(){
	
	fnValidateDropDn('adjustmentType',lblAdjType); 
	fnValidateDropDn('consignTo',lblConsTo); 
	
	var adjType = document.frmFieldSalesConAdj.adjustmentType.value;
	var consignTo = document.frmFieldSalesConAdj.consignTo.value;
	var part = document.frmFieldSalesConAdj.pnumStr.value;
	var setID = document.frmFieldSalesConAdj.setID.value;
	var tagID = document.frmFieldSalesConAdj.tagID.value;
	var comment = document.frmFieldSalesConAdj.comments.value;
	

	if(adjType == 101200 || adjType == 101201){ // for Write Up set OR Write down set then Set id is mandatory field.
		fnValidateDropDn('setID',lblSetId);
		if(part !='')
			Error_Details(message_accounts[128]);
	}else if(adjType == 101202 || adjType == 101203){ //for Write Up part OR Write down part then Part # is mandatory field.
		fnValidateTxtFld('pnumStr',lblPartNum); 
		if(setID != 0)
			Error_Details(message_accounts[129]);
	}else if(adjType == 101204){ // for Write off Inactive dist then Tag id,Set id and Part # are as blank.
		if(tagID !='' || setID !=0 || part !=''){
			Error_Details(message_accounts[130]);
		}
	}
	if(comment == ''){
		Error_Details(message_accounts[131]);
	}
	
}

function fnReset(){
	document.frmFieldSalesConAdj.adjustmentType.value = 0;
	document.frmFieldSalesConAdj.consignTo.value = 0;
	document.frmFieldSalesConAdj.pnumStr.value = '';
	document.frmFieldSalesConAdj.setID.value = 0;
	document.frmFieldSalesConAdj.tagID.value = '';
	document.frmFieldSalesConAdj.comments.value = '';
}

function fnCreateInputString(){
	var objval = TRIM(document.frmFieldSalesConAdj.pnumStr.value);
	
	objval = objval.replace(/\r\n/g,',');

	var mySplitval = objval.split(",");
	var count = mySplitval.length;
	
	var partnum = '';
	var qty = '';
	objval = objval +',';
	for(i=0;i<count;i++){
		partnum = objval.substring(0,objval.indexOf(','));
		objval = objval.substring((partnum.length)+1,objval.length);
		qty = objval.substring(0,objval.indexOf(','));
		objval = objval.substring((qty.length)+1,objval.length);
		partnum = TRIM(partnum);
		qty = TRIM(qty); 
		if(qty !=''){
			NumberValidation(qty, message_accounts[132], 1);
		}
		if(partnum !='' && qty !=''){
			inpStr =inpStr+ partnum +','+ qty +'|';
		}
		 
	}
}

function fnPageLoad(){
	var partStr = document.frmFieldSalesConAdj.pnumStr.value;
	if(partStr !=''){
		partStr = replaceAll(partStr,"|",",");
		document.frmFieldSalesConAdj.pnumStr.value = partStr; 
	}
	
}