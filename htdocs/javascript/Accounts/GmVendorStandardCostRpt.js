var gridObj;
function fnOnPageLoad(){
	if(objGridData.value != ''){ 
		gridObj = initGridData('dataGridDiv',objGridData);
		gridObj.attachHeader('#rspan,#select_filter,#select_filter,#text_filter,#text_filter,#select_filter,#rspan,#text_filter');  
	}	
}

function initGridData(divRef,gridData){
	var gObj = new dhtmlXGridObject(divRef);
    gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);
	gObj.enableTooltips("true,true,true,true,true");
	gObj.init();
	gObj.loadXMLString(gridData);
	//gObj.enableHeaderMenu("false,true,true,true,true,true,true");
	return gObj;	
}
/*Function Load:Load Standard Cost details*/
function fnLoad(){
	var vendor = document.frmStandardReport.strVendorId.value;

	var part = document.frmStandardReport.strPartNumbers.value;

	var project = document.frmStandardReport.projectId.value;

	if(part =='' && project =="0" && vendor == "0"){
	
		Error_Details("Please select Project Name or Vendor Name or enter the Part Number to proceed"); //Please Enter a Part Number to Proceed
	}
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	document.frmStandardReport.strOpt.value="Load";
	document.frmStandardReport.action = "/gmVendorStandardCost.do?method=loadStandardCostRpt";
	fnStartProgress('Y');
	document.frmStandardReport.submit();
}
/*Edit Standard Cost Details*/
function fnEdit(vendorId, rowid)
{
var part = gridObj.cellById(rowid,3).getValue();

document.frmStandardReport.strOpt.value="Load";
document.frmStandardReport.action = "/gmVendorStandardCost.do?method=saveStandardCost&pnum="+encodeURIComponent(part)+"&vendorId="+vendorId;
document.frmStandardReport.submit();

}
/*Show History Information*/


function fnSetCostHistory(vendorId, rowid){
var part = gridObj.cellById(rowid,3).getValue();
if(part !=''){
			  w4 = createDhtmlxWindow(1000,350,true)
			  w4.setText(message[5293]);
			  var ajaxUrl = fnAjaxURLAppendCmpInfo("/gmVendorStandardCost.do?method=loadStandardCostHistory&vendorId="+vendorId+"&pnum="+encodeURIComponent(part)+"&ramdomId="+Math.random());
			  w4.attachURL(ajaxUrl,"History");
			  return false;	
	}
}

function fnEnter() {
	if (event.keyCode == 13) {
		fnLoad();
	}
}

function fnExport()
{	
	gridObj.setColumnHidden(0,true); 
	gridObj.setColumnHidden(6,true);
	gridObj.toExcel('/phpapp/excel/generate.php');
	gridObj.setColumnHidden(0,false); 
	gridObj.setColumnHidden(6,false); 
}
