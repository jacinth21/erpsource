
/**
   * fnShowCreditType() - This method is used to open the DHTMLX pop up window of credit type
   * 
   */

function fnShowCreditType(){
	
	var accountID = document.frmCreditHold.accountId.value;
	var creditTypeId = document.frmCreditHold.creditHoldType.value;
	if(creditTypeId == 0 || creditTypeId == ''){
	Error_Details(message_accounts[115]);
	}
	if (ErrorCount > 0){
			Error_Show();
			Error_Clear();
			return false;
	}else{
	var acctName =document.frmCreditHold.accountId.options[document.frmCreditHold.accountId.selectedIndex].text;
	dhxWins = new dhtmlXWindows();
	dhxWins.enableAutoViewport(false);
	dhxWins.attachViewportTo(document.body);
	dhxWins.setImagePath("javascript/dhtmlx/dhtmlxWindows/imgs");

	popupWindow = dhxWins.createWindow("w1", 250, 50, 550, 350);
	popupWindow.attachURL('/gmCreditHold.do?method=fetchCreditHoldTypeDetail&strOpt=reload&accountId='+accountID+'&creditHoldType='+creditTypeId+'&accountName='+acctName ,"Account Credit Hold Type","resizable=yes,scrollbars=no,top=250,left=50,width=400,height=500");
	popupWindow.setText(message_accounts[116]);
	}	
}

/**
   * fnAccountCreditHold(accountID) - This method is used to change the account details
   * 
   */

function fnAccountCreditHold(accountID){
	dhtmlxAjax.get('/gmCreditHold.do?method=fetchAcctCreditHold&strOpt=reload&accountId='+accountID+'&randomId='+Math.random(),function(loader){
		//{"CREDITLIMITHISTFL":"","CREDITTYPEHISTFL":"","ACCTID":"7746","CREDITLIMIT":"5000","ACCTNM":"Abilene Regional Medical Center","CREDITTYPE":"400904"};
		var resJSONObj = $.parseJSON(loader.xmlDoc.responseText);
	var creditLimit = resJSONObj["CREDITLIMIT"];
	var creditType = resJSONObj["CREDITTYPE"];
	var accountName = resJSONObj["ACCTID"];
	var creditLimitHist = resJSONObj["CREDITLIMITHISTFL"];
	var creditTypeHist = resJSONObj["CREDITTYPEHISTFL"];
	var outStandBalance = resJSONObj["OUTSTD"];
	var acccurrency  = resJSONObj["ACCCURRENCY"];//PMT-53060
	creditType = (creditType == "")?"0":creditType;
	document.frmCreditHold.creditLimit.value = creditLimit;
	document.frmCreditHold.creditHoldType.value = creditType;
	document.frmCreditHold.hCreditHoldType.value = creditType;
	document.getElementById("outStand").innerHTML = formatCurrency(outStandBalance,acccurrency);
	document.frmCreditHold.arSummary.value ="0";
	document.getElementById("spanCreditLimitHist").style.display=(creditLimitHist == 'Y')?"block":"none";
	document.getElementById("spanCreditTypeHist").style.display=(creditTypeHist == 'Y')?"block":"none";
	});
}

/**
   * fnAcctPaymentInfo(accountID) - This method is used to fetch the account payment info
   * 
   */

function fnAcctPaymentInfo(accountID){
	dhtmlxAjax.get('/gmCreditHold.do?method=fetchAcctPaymentInfo&accountId='+accountID+'&randomId='+Math.random(),function(loader){
	var resJSONObj ={};
	resJSONObj = $.parseJSON(loader.xmlDoc.responseText);
	var lastPayment = resJSONObj["PAYAMT"];
	var lastPaymentDt = resJSONObj["PAYDT"];
	var lastInvoicePayment  = resJSONObj["INVID"];
	var acccurrency  = resJSONObj["ACCCURRENCY"];//PMT-53060
	
	var lastPayDivObj = document.getElementById("lastPayment");
	lastPayDivObj.innerHTML = formatCurrency(lastPayment,acccurrency);
	
	var lastPayDtDivObj = document.getElementById("lastPaymentDate");
	lastPayDtDivObj.innerHTML = lastPaymentDt;
	
	var invoicePayDivObj = document.getElementById("lastInvoicePayment");
	invoicePayDivObj.innerHTML = "<a class=RightText href= javascript:fnPrintInvoice('"+lastInvoicePayment+"')>"+lastInvoicePayment+"</a>";
	});
}

/**
   * fnSubmit() - This method is used to submit the info
   * 
   */

function  fnSubmit(){
	var creditLimit = document.frmCreditHold.creditLimit.value;
	var creditType = document.frmCreditHold.creditHoldType.value;
	var oldCreditType = document.frmCreditHold.hCreditHoldType.value;
	
	NumberValidation(creditLimit,message_accounts[117], 2);
	fnValidateDropDn('creditHoldType',message_accounts[202]);
	fnValidateTxtFld('txt_LogReason',message_accounts[203]);
	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}
	var crediAttrString = '';
	crediAttrString = crediAttrString+'101184'+'^'+creditType+'|';
	
	if(TRIM(creditLimit) != ""){
		crediAttrString = crediAttrString+'101182'+'^'+creditLimit+'|';
	}
	document.frmCreditHold.creditAttrString.value = crediAttrString;	
	document.frmCreditHold.action ="/gmCreditHold.do?method=saveAcctCreditHold";
	//Check Credit hold is added/removed
	if(creditType != oldCreditType){
		if(creditType!="" && creditType!="4000844"){ //4000844 - None
			document.frmCreditHold.emailType.value = "ADDED";
		}else if(creditType == "4000844" && oldCreditType!="4000844"){
			document.frmCreditHold.emailType.value = "REMOVED";
		}
	}
	fnStartProgress();
	document.frmCreditHold.submit();
}

/**
   * fnLoadARSummary() - This method is used to load the AR Summary for Due date and AR Summary for invoice date
   * 
   */


function fnLoadARSummary(){
	var ID = document.frmCreditHold.arSummary.value;
	var accID = document.frmCreditHold.accountId.value;
	if(ID == '' || ID == '0'){	
		Error_Details(message_accounts[118]);
	}
	if (ErrorCount > 0){
			Error_Show();
			Error_Clear();
			return false;
	}
	if(ID == '104980'){
		windowOpener("/GmARSummaryServlet?reportType=ARByInvoiceDt&hScreenType=CrdHld&hAction=ReLoadAR&Cbo_AccId="+accID+"&fromPage=CreditHold","ARSummaryReport","resizable=yes,scrollbars=yes,top=150,left=150,width=1200,height=750");
	}
	else if(ID == '104981'){
		windowOpener("/GmARSummaryServlet?reportType=ARByDueDt&hScreenType=CrdHld&hAction=ReLoadAR&Cbo_AccId="+accID+"&fromPage=CreditHold","ARSummaryReport","resizable=yes,scrollbars=yes,top=150,left=150,width=1200,height=750");
	}
	}

/**
   * fnLoadAccountStatement() - This method is used to load the account statement
   * 
   */


function fnLoadAccountStatement() {	
	var accID = document.frmCreditHold.accountId.value;
	windowOpener("/GmInvoiceServlet?hType=Account&hScreenType=CrdHld&hAction=ACCDET&Cbo_InvSource=50255&Cbo_AccId="+accID+"&Txt_FromDate="+FromDate+"&Txt_ToDate="+ToDate,"AccountStatementReport","resizable=yes,scrollbars=yes,top=150,left=150,width=1200,height=750");	
}

/**
   * fnPageLoad() - This method is used to load the page
   * 
   */

function fnPageLoad(){
	var accountID = document.frmCreditHold.accountId.value;
    fnReset();
	if(accountID != "" && accountID != "0"){
		fnAccountCreditHold(accountID);
		fnAcctPaymentInfo(accountID);
		fnCommentLogAjax(accountID);
	}
}

/**
   * fnReset() - This method will reset the values
   * 
   */

function fnReset(){
	document.frmCreditHold.creditLimit.value = "";
	document.frmCreditHold.creditHoldType.value = "0";
	document.getElementById("outStand").innerHTML = "";
	document.getElementById("lastPayment").innerHTML = "";
	document.getElementById("lastPaymentDate").innerHTML = "";
	document.getElementById("lastInvoicePayment").innerHTML = "";
	document.getElementById("divCommentTable").innerHTML = "";
	document.getElementById("divShowHide").innerHTML = "";
}

/**
   * fnPrintInvoice(varinv) - This method will pop up the Invoice Info
   * 
   */

function fnPrintInvoice(varinv)
{
windowOpener('/GmInvoiceInfoServlet?hAction=Print&hInv='+varinv,"PrntInv","resizable=yes,scrollbars=yes,top=150,left=150,width=800,height=800");
}

/**
   * fnLoadHistory(colId) - This method will pop up the History 
   * 
   */


function fnLoadHistory(colId)
{
	var accountID = document.frmCreditHold.accountId.value;
	windowOpener("/gmAuditTrail.do?auditId="+ colId +"&txnId="+ accountID,"","resizable=yes,scrollbars=yes,top=250,left=300,width=785,height=300,status=1");
	}

/**
   * fnLoadAccInfo(obj) - This method will load the Account info 
   * 
   */

function fnLoadAccInfo(obj){
	document.frmCreditHold.accountId.value = obj.value;	
	fnPageLoad();
}


/**
   * fnAccountChange()- This method will change the Account ID
   * 
   */

function fnAccountChange(){
	var accountId = document.frmCreditHold.accountId.value;
	if(accountId!="0"){
		document.frmCreditHold.acctId.value =accountId;	
	}else{
		document.frmCreditHold.acctId.value ="";
	}
	
	fnPageLoad();
}	
/**
   * fnClosePopup() - This method will close the DHTMLX pop up 
   * 
   */
	
	
function fnClosePopup(){
	//parent.dhxWins.window("w1").close();
	window.close();
}




