//Below four functions are used in DHR Flag payment
function fnCheckOption()
{	
	 varDHR = document.frmDHRFlagPayment.Cbo_Type.value; 
	 obj = document.frmDHRFlagPayment.Cbo_InvoiceType;
	 obj1 = document.frmDHRFlagPayment.Cbo_InvoiceOper;
	 //103301:DHR Verified No INV
	 if (varDHR=="103301") 
	 {

		document.frmDHRFlagPayment.Cbo_InvoiceType.value=0;
		document.frmDHRFlagPayment.Cbo_InvoiceOper.value = "0";
		
		obj.disabled = true; 
		obj1.disabled = true; 
		
		}
		else {
		obj.disabled = false; 
		obj1.disabled = false;  
		}
	 }

 function fnGo()
{
	Error_Clear();	
	var blflag = false;
	
	fnValidateInput();
	
	if(document.frmDHRFlagPayment.Cbo_InvoiceOper.value != "0")
	{	
 		document.frmDHRFlagPayment.hOperatorSign.value=frmDHRFlagPayment.Cbo_InvoiceOper.options[frmDHRFlagPayment.Cbo_InvoiceOper.selectedIndex].text;
 	} 

	var frmdt = document.frmDHRFlagPayment.Txt_FromDate.value;
	var todt = document.frmDHRFlagPayment.Txt_ToDate.value;

	if (frmdt == '' && todt == '')
	{
		blflag = true;
	}

	var type = document.frmDHRFlagPayment.Cbo_Type.value;
	var poid = document.frmDHRFlagPayment.Txt_PORDER.value;
	var invtype = document.frmDHRFlagPayment.Cbo_InvoiceType.value;
	if (type == '103302' || type == '103303' )
	{
		//if ((type == '3'&&blflag&&poid=='')||(type =='2'&&blflag&&poid==''&&invtype=="30"))
		if ( blflag&&poid==''&&invtype=="30" ||type == '3'&&blflag&&poid==''&&invtype=="-1")
		{
			Error_Details(message_accounts[119]);
		}
		else
		{
			diff = compareDates(frmdt,todt);
		    if (diff > 3)
		    {
				Error_Details(message_accounts[120]);
			}
		}

		if (type =='103302'&&blflag&&poid==''&&invtype=="-1")
		{
			Error_Details(message_accounts[121]);
		}
	}


	if (ErrorCount > 0)
	{
		Error_Show();
		return false;
	}
	else
	{
		document.frmDHRFlagPayment.hAction.value = "Reload";
		fnStartProgress('Y');
	 	document.frmDHRFlagPayment.submit();
	}
	
}
 
function fnValidateInput(){
	var invType=document.frmDHRFlagPayment.Cbo_InvoiceType.value;
	var vendid = document.frmDHRFlagPayment.Cbo_VendorId.value;
	var potype = document.frmDHRFlagPayment.Cbo_PoType.value;
	var dhrtype =document.frmDHRFlagPayment.Cbo_Type.value;
	var invtype = document.frmDHRFlagPayment.Cbo_InvoiceType.value;
	var frmDate = TRIM(document.frmDHRFlagPayment.Txt_FromDate.value);
	var toDate =  TRIM(document.frmDHRFlagPayment.Txt_ToDate.value);
	var wo = TRIM(document.frmDHRFlagPayment.Txt_WORDER.value);
	var pnum = TRIM(document.frmDHRFlagPayment.Txt_PARTID.value);
	var po = TRIM(document.frmDHRFlagPayment.Txt_PORDER.value);
	var dhr = TRIM(document.frmDHRFlagPayment.Txt_DHR.value);
	
	
	if(invType=='0' && vendid=='0' && potype == '0' && dhrtype=='0' && invtype=='0' && frmDate=='' && toDate=='' &&  wo=='' &&  
		pnum == '' && po=='' && dhr=='')
		{
			Error_Details(message_accounts[122]);
		}
	
	
}
 //Function to set the Default dropdown value for the Invoice Operator.
function fnSetInvOperator(){
	var invType=document.frmDHRFlagPayment.Cbo_InvoiceType.value;
	if(invType=="0"){
		document.frmDHRFlagPayment.Cbo_InvoiceOper.value = "0";
	}
}

function fnViewDHR(val)
{
	windowOpener("/GmPOReceiveServlet?hAction=ViewDHR&hDHRId="+val,"DHR","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnViewPO(val)
{
	windowOpener("/GmPOServlet?hAction=PrintPO&hPOId="+val,"PO","resizable=yes,scrollbars=yes,top=150,left=50,width=770,height=600");
}

//Below functions are added as part of A/P
function fnDHRPaymentGo(form){
	var poid = form.containerPONumber.value;
	if(poid == '')
	{
		Error_Details(message[38]);
	}

	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}

	form.strOpt.value = '';
	form.action = '/gmDHRPayment.do?'+fnAppendCompanyInfo();
	fnStartProgress('Y');
	form.submit();
}

function fnLoadInvcDtls(paymentId,poid,form)
{
	//loadajaxpage('/gmDHRPayment.do?hpaymentId='+paymentId+'&hpoNumber='+poid+'&containerPONumber='+poid+'&strOpt=loadInvc','ajaxdivcontentarea');	
	loadajaxpage('/gmAPSaveInvoiceDtls.do?hpaymentId='+paymentId+'&hpoNumber='+poid+'&containerPONumber='+poid+'&strOpt=InvcGo&'+fnAppendCompanyInfo(),'ajaxinvoicearea');	
}

function fnNewInvoice(form)
{
	var poid = form.hpoId.value;
	//loadajaxpage('/gmDHRPayment.do?&hpoNumber='+poid+'&strOpt=newInvc','ajaxdivcontentarea');
	loadajaxpage('/gmAPSaveInvoiceDtls.do?hpoNumber='+poid+'&strOpt=newInvc&'+fnAppendCompanyInfo(),'ajaxinvoicearea');
}

function fnVoidInvoice(form)
{
	form.hTxnId.value = form.hpaymentId[1].value;// PC-4729  // In this form have three input hpaymentId it automatiocally take list 
											  				// In exiting (poid = form.hpaymentId.value) we can't apply hpaymentId value here
											 			   //  so we given index then it working fine	
	form.hAction.value = "Load";
	form.hCancelType.value = 'VOINVC';
	form.action ="/GmCommonCancelServlet";
	form.submit();
}

function fnInvoiceGo(form)
{
	// check correct month format entered
	CommonDateValidation(document.all.invoiceDt,format,Error_Details_Trans(message_accounts[123],format));
	var paymentId = form.hpaymentId[1].value; // PC-5304 After checked "Show Open DHR" and click "GO" button - Edge not showing the data
	var poid = form.hpoNumber[1].value;		// PC-5304 After checked "Show Open DHR" and click "GO" button - Edge not showing the data
	var flag;
	if(form.showOpenDHRFlag.checked){
		flag = "on";
	}
	else{
		flag = "";
	}
	if (ErrorCount > 0)  
	{
		Error_Show();
		Error_Clear();
		return false;
	} 
	loadajaxpage('/gmAPSaveInvoiceDtls.do?hpaymentId='+paymentId+'&hpoNumber='+poid+'&containerPONumber='+poid+'&showOpenDHRFlag='+flag+'&strOpt=InvcGo&'+fnAppendCompanyInfo(),'ajaxinvoicearea');	
}

function fnAddRow(id,cboname,form)
{
	var strcboval = form.hstraddl.value;
	var cnt = 1 * document.all.hcnt.value;
	
	var tbody = document.getElementById(id).getElementsByTagName("TBODY")[0];
    var row = document.createElement("TR");

    var td0 = document.createElement("TD");
    td0.innerHTML = '&nbsp;'+ (cnt + 1);
   
    var td1 = document.createElement("TD");
    td1.innerHTML = '<a href="javascript:fnRemoveItem('+cnt+');" tabindex="-1"><img border="0" Alt="Remove from cart" valign="left" src="/images/btn_remove.gif" height="10" width="9"></a>';
    
    var td2 = document.createElement("TD");
     td2.innerHTML = '&nbsp;<input type="text" size="7" value="" class=InputArea name=Txt_Amt'+cnt+' onFocus="changeBgColor(this,\'#AACCE8\');" onBlur="fnCalcAddlChrgTotal(this,'+(cnt+1)+',form);">';
    
    var td3 = document.createElement("TD")
	td3.id = "Lbl_GLAccNo"+cnt;
    td3.align = "Center";
    
    var optionstr = '<option value="0">[Choose One]</option>';
    var arrayAddl = strcboval.split("|");
    for(var i=0;i<arrayAddl.length-1;i++)
    {
    	var arrayAddlType = arrayAddl[i].split("^");
    	optionstr = optionstr + "<option value=" + arrayAddlType[1] + ">"+ arrayAddlType[0] + "</option>"; 
    }
	
	var td4 = document.createElement("TD");
	td4.innerHTML = '&nbsp;<select name='+cboname+cnt+' class=RightText tabindex="-1" onChange="fnPopulateGLNumber(this,this.form);">' + optionstr;

    row.appendChild(td0);
    row.appendChild(td1);
    row.appendChild(td4);
    row.appendChild(td2);
    row.appendChild(td3);
    tbody.appendChild(row);

	cnt++;
	document.all.hcnt.value = cnt;	
}

function fnSaveInvoice(obj,form,action)
{
	var bvoidBtn = false;
	var dhrlen = form.hIlDHRSize.value;
	document.all.saveInvoice.disabled = "true";
	//document.all.voidInvoice.disabled = "true";
    document.all.saveAndNewInvoice.disabled = "true";
	objSesnDt = document.all.sessionDt;
	objInvDt = document.all.invoiceDt;
	CommonDateValidation(objInvDt,format,Error_Details_Trans(message_accounts[123],format));
	diff = dateDiff(objInvDt.value,objSesnDt.value);
	if(diff < 0)
	{
		Error_Details(message[78]);						
	}

	if(document.all.voidInvoice.disabled)
	{
		bvoidBtn = true;//Void Button is already disabled
	}
	else
		document.all.voidInvoice.disabled = "true";

	var poid;
	var paymentid;
	var vendorid;
	var invoiceid;
	var invoicedt;
	var paymentAmt;
	var invoicecomments;
	var invoiceintcomments;

	if(TRIM(form.invoiceId.value) == ""){
	    Error_Details(message[39]);
	}
	
	if(TRIM(form.invoiceDt.value) == ""){
		Error_Details(message[40]);
	}
	
	if(TRIM(form.paymentAmt.value) == ""){
	    Error_Details(message[41]);
	}
	else {
		if(!isNumberKey(form.paymentAmt.value)) {
			Error_Details(message[10619]);
		}
		if(form.paymentAmt.value < 0) {
			Error_Details(message[10620]);
		}
	}
	
	if(dhrlen == 0){
		Error_Details(message[42]);
	}
	
	var inputStringDHR = "";
	var checkFlag = 0;
	var checkNegative = 0;
	var checkPositive = 0;
	
	for(var i=0;i<dhrlen;i++)
	{
		dhrobj = eval("document.getElementById('DHRListFrame').contentWindow.document.all.Chk_DHR" + i);
		
		var objCreAmt = eval("document.getElementById('DHRListFrame').contentWindow.document.all.TXT_CREAMT"+i);
		var objInvamt = eval("document.getElementById('DHRListFrame').contentWindow.document.all.TXT_INVAMT"+i);
		var objExtInvAmt = eval("document.getElementById('DHRListFrame').contentWindow.document.all.hEXTINVAMT"+i);
		var ojbDiff =  eval("document.getElementById('DHRListFrame').contentWindow.document.all.hDIFF"+i);
		var valCreAmt = objCreAmt.value;
		var valInvamt = objInvamt.value;
		var valExtInvAmt = objExtInvAmt.value;
		var valDiff = objExtInvAmt.value;
		if (valCreAmt != "") {
		if(!isNumberKey(valCreAmt)) {
			valCreAmt = 0; 
			Error_Details(message[10618]);
			objCreAmt.style.borderColor = "red";
		} 
		}

		
		if(valInvamt != "") {
		if(!isNumberKey(valInvamt)) {
			valInvamt = 0; 
			Error_Details(message[10619]);
			objInvamt.style.borderColor = "red";
		} 
		}
		
		/*check Invoice amount should be positive value*/
		if(valInvamt < 0){
			checkPositive ++;
			objInvamt.style.borderColor = "red";
		}
		
		/*check credit amount should be negative value*/
		if(valCreAmt > 0){
			checkNegative ++;
			objCreAmt.style.borderColor = "red";
		}

		
		if (dhrobj.checked)
		{
			inputStringDHR = inputStringDHR + eval("document.getElementById('DHRListFrame').contentWindow.document.all.hDHRID"+i).value + "^" + "1" +
			"^" +valInvamt+
			"^" +valExtInvAmt+
			"^" +valCreAmt+
			"^" +valDiff+"|";
			checkFlag = 1;
		}
		else
		{
			inputStringDHR = inputStringDHR + eval("document.getElementById('DHRListFrame').contentWindow.document.all.hDHRID"+i).value + "^" + "0" + 
			"^" +valInvamt+
			"^" +valExtInvAmt+
			"^" +valCreAmt+
			"^" +valDiff+"|";
		}			
	}
	
	if (checkPositive > 0) {
		Error_Details(message[10620]);
	}
	
	if (checkNegative > 0) {
		Error_Details(message[10616]);
	}
	
	if(checkFlag == 0){
		Error_Details(message[43]);
	}
	
	var balTot = 1 * document.all.hTotalBalance.value;


	
	if (ErrorCount > 0)
	{
		obj.disabled = false;
		document.all.saveInvoice.disabled = false;
		
		if(!bvoidBtn)//if void button is already disabled then we need not enable it
			document.all.voidInvoice.disabled = false;

		document.all.saveAndNewInvoice.disabled = false;
		Error_Show();
		Error_Clear();
		return false;
	}

	if(balTot != 0)
	{
		var approve = confirm(message_accounts[124]);
		if(approve == false){
			document.all.saveInvoice.disabled = false;
			if(!bvoidBtn)//if void button is already disabled then we need not enable it
				document.all.voidInvoice.disabled = false;
			document.all.saveAndNewInvoice.disabled = false;
			return false;
        }
	}

	poid = form.hpoNumber[1].value;// PC-4729  // In this form have three input hpoNumber it automatiocally take list 
											  // In exiting (poid = form.hpoNumber.value) we can't apply hpoNumber value here
											 //  so we given index then it working fine	
	paymentid = form.hpaymentId[1].value;	// PC-5304 After invoiced - try to edit invoice amount - showing "invoice already exist error"
	vendorid = document.getElementById('DHRListFrame').contentWindow.document.all.hVID0.value;
	invoiceid = form.invoiceId.value;
	invoicedt = form.invoiceDt.value;
	paymentAmt = form.paymentAmt.value;
	invoicecomments = form.invoiceComments.value;
	invoiceintcomments = form.invoiceIntComments.value;
	var inputStringAddl = "";

	var cnt = 1 * document.all.hcnt.value;

	for(var j=0;j<cnt;j++)
	{
		objAddlType = eval("form.Cbo_AddlChrgType"+j);
		objAddlAmt = eval("form.Txt_Amt"+j);
		if(objAddlType.value != "0" && objAddlAmt.value != "")
		{
			inputStringAddl = inputStringAddl + objAddlType.value + "^" + objAddlAmt.value + "|";
		}
	}
	
	if(action != 'saveAndNew'){
		loadajaxpage('/gmAPSaveInvoiceDtls.do?hpoNumber='+poid+'&hvendorId='+vendorid+'&hpaymentId='+paymentid+'&hdhrId='+inputStringDHR+'&haddlChrgId='+inputStringAddl+'&invoiceId='+invoiceid+'&invoiceDt='+invoicedt+'&invoiceComments='+invoicecomments+'&invoiceIntComments='+invoiceintcomments+'&paymentAmt='+paymentAmt+'&strOpt=save&'+fnAppendCompanyInfo(),'ajaxinvoicearea');
	}
	else{
		
		form.hpoNumber[0].value = poid;// PC-4729  // In this form have three input hpoNumber it automatiocally take list 
									  			  // In exiting (form.hpoNumber.value =poid) we can't apply poid value here
									 			 //  so we given index then it working fine	
		form.hvendorId.value = vendorid;
		form.hpaymentId[0].value = paymentid; // PC-5304 After invoiced - try to edit invoice amount - showing "invoice already exist error"
		form.hdhrId.value = inputStringDHR;
		form.haddlChrgId.value = inputStringAddl;
		form.invoiceId.value = invoiceid;
		form.invoiceDt.value = invoicedt;
        form.paymentAmt.value = paymentAmt;
		form.invoiceComments.value = invoicecomments;
		form.invoiceIntComments.value = invoiceintcomments;
		form.strOpt[0].value = action; // PC-4729  // In this form have three input strOpt it automatiocally take list 
												  // In exiting (form.strOpt.value =action) we can't apply action value here
												 //  so we given index then it working fine
		form.containerPONumber.value = "";		// PC-5304  After entered the invoice details and clicked the "Save & New invoice" button - PO number is not clear
		form.action = "/gmAPSaveInvoiceDtls.do?"+fnAppendCompanyInfo();
		form.submit();
		
		//parent.location = '/gmAPSaveInvoiceDtls.do?hpoNumber='+poid+'&hvendorId='+vendorid+'&hpaymentId='+paymentid+'&hdhrId='+inputStringDHR+'&haddlChrgId='+inputStringAddl+'&invoiceId='+invoiceid+'&invoiceDt='+invoicedt+'&invoiceComments='+invoicecomments+'&paymentAmt='+paymentAmt+'&strOpt=save';
	}
	
	if(paymentid == ''){
		input = 1;
		loadajaxpage('/gmDHRPayment.do?strOpt=loadInvc&hpoNumber='+poid+'&'+fnAppendCompanyInfo(),'ajaxdivcontentarea');
	}
}

function fnSelectAll(dhrlen,form)
{
	var obj;
	var amt = 0;

	var readyTot = 1 * roundNumber(document.all.hReadyTotal.value,2);
	var dhrReadyTot = 1 * roundNumber(document.all.hDHRReadyTotal.value,2);
	var dhrPendingTot = 1 * roundNumber(document.all.hDHRPendingTotal.value,2);
	var addlTot = 1 * roundNumber(document.all.hAddlTotalValue.value,2);
	var invcTot = 1 * roundNumber(document.all.hInvoiceAmount.value,2);
	var totalBalance = 0;

	var dhrInfo;

	var dhrStatus;
	var dhrRecAmt;
	var dhrShelfAmt;

	dhrReadyTot = 0;	
	dhrPendingTot = 0;

	if(form.selectAll.checked)
	{
		for(var i=0;i<dhrlen;i++)
		{
			obj = eval("document.getElementById('DHRListFrame').contentWindow.document.all.Chk_DHR"+i);
			obj.checked = true;
			
			dhrInfo = obj.value;

			dhrInfo = dhrInfo.split("^");
			dhrStatus = 1 * dhrInfo[0];
			dhrRecAmt = 1 * dhrInfo[1];
			dhrShelfAmt = 1 * dhrInfo[2];
			
			if(dhrStatus == 4)
			{
					dhrReadyTot = dhrReadyTot + dhrShelfAmt;		
			}
			else
			{
					dhrPendingTot = dhrPendingTot + dhrRecAmt;
			}
		}
	}
	else
	{
		for(var i=0;i<dhrlen;i++)
		{
			obj = eval("document.getElementById('DHRListFrame').contentWindow.document.all.Chk_DHR"+i);
			obj.checked = false;
		}
	}
	
	readyTot = dhrReadyTot + addlTot;

	document.all.hReadyTotal.value = roundNumber(readyTot,2);
	document.all.hDHRReadyTotal.value = roundNumber(dhrReadyTot,2);
	document.all.hDHRPendingTotal.value = roundNumber(dhrPendingTot,2);
	totalBalance = invcTot - readyTot - dhrPendingTot;
	document.all.hTotalBalance.value = roundNumber(totalBalance,2);

	document.all.Lbl_DHRReadyAmt.innerHTML = "&nbsp;<B> " + formatNumber(document.all.hDHRReadyTotal.value) + "</B>&nbsp;";
	document.all.Lbl_DHRPendingAmt.innerHTML = "&nbsp;<B> " + formatNumber(document.all.hDHRPendingTotal.value) + "</B>&nbsp;";
	document.all.Lbl_ReadyTotal.innerHTML = "&nbsp;<B> " + formatNumber(document.all.hReadyTotal.value) + "</B>&nbsp;"; 
	document.all.Lbl_Balance.innerHTML = "&nbsp;<B> " + formatNumber(document.all.hTotalBalance.value) + "</B>&nbsp;";	
	
	calcPPVAmounts();
}

function fnCalcDHRAmt(id,form)
{
	var amt = 0;
	obj = eval("form."+id);
	var dhrInfo = obj.value;
	dhrInfo = dhrInfo.split("^");
	var dhrStatus = 1 * dhrInfo[0];
	var dhrRecAmt = 1 * dhrInfo[1];
	var dhrShelfAmt = 1 * dhrInfo[2];

	var readyTot = 1 * roundNumber(parent.document.all.hReadyTotal.value,2);
	var dhrReadyTot = 1 * roundNumber(parent.document.all.hDHRReadyTotal.value,2);
	var dhrPendingTot = 1 * roundNumber(parent.document.all.hDHRPendingTotal.value,2);
	var addlTot = 1 * roundNumber(parent.document.all.hAddlTotalValue.value,2);
	var invcTot = 1 * roundNumber(parent.document.all.hInvoiceAmount.value,2);
	var totalBalance = 0;

	
	if (obj.checked)
	{
		if(dhrStatus == 4)
		{
			dhrReadyTot = dhrReadyTot + dhrShelfAmt;
		}
		else
		{
			dhrPendingTot = dhrPendingTot + dhrRecAmt;
		}
	}
	else
	{
		if(dhrStatus == 4)
		{
			dhrReadyTot = dhrReadyTot - dhrShelfAmt;
		}
		else
		{
			dhrPendingTot = dhrPendingTot - dhrRecAmt;
		}
	}

	readyTot = dhrReadyTot + addlTot;

	parent.document.all.hDHRReadyTotal.value = roundNumber(dhrReadyTot,2);
	parent.document.all.hDHRPendingTotal.value = roundNumber(dhrPendingTot,2);
	parent.document.all.hReadyTotal.value = roundNumber(readyTot,2);
	totalBalance = invcTot - readyTot - dhrPendingTot;
	parent.document.all.hTotalBalance.value = roundNumber(totalBalance,2);

	parent.document.all.Lbl_DHRReadyAmt.innerHTML = "&nbsp;<B> " + formatNumber(parent.document.all.hDHRReadyTotal.value) + "</B>&nbsp;";
	parent.document.all.Lbl_DHRPendingAmt.innerHTML = "&nbsp;<B> " + formatNumber(parent.document.all.hDHRPendingTotal.value) + "</B>&nbsp;";
	parent.document.all.Lbl_ReadyTotal.innerHTML = "&nbsp;<B> " + formatNumber(parent.document.all.hReadyTotal.value) + "</B>&nbsp;";
	parent.document.all.Lbl_Balance.innerHTML = "&nbsp;<B> " + formatNumber(parent.document.all.hTotalBalance.value) + "</B>&nbsp;"; 
	
	calcPPVAmounts();
}

function fnFillInvoiceAmt(obj,form)
{
	changeBgColor(obj,'#ffffff');

	var readyTot = 1 * roundNumber(document.all.hReadyTotal.value,2);
	var dhrPendingTot = 1 * roundNumber(document.all.hDHRPendingTotal.value,2);
	var invcTot = 1 * roundNumber(document.all.hInvoiceAmount.value,2);
	var totalBalance = 0;

	invcTot = roundNumber(obj.value,2);

	document.all.hInvoiceAmount.value = roundNumber(invcTot,2);
	totalBalance = invcTot - readyTot - dhrPendingTot;
	document.all.hTotalBalance.value = roundNumber(totalBalance,2);

	document.all.Lbl_InvcTotal.innerHTML = "&nbsp;<B> " + formatNumber(document.all.hInvoiceAmount.value) + "</B>&nbsp;";
	document.all.Lbl_Balance.innerHTML = "&nbsp;<B> " + formatNumber(document.all.hTotalBalance.value) + "</B>&nbsp;";
}

function fnCalcAddlChrgTotal(obj,len,form)
{
	changeBgColor(obj,'#ffffff');
	
	var readyTot = 1 * roundNumber(document.all.hReadyTotal.value,2);
	var addlTot = 1 * roundNumber(document.all.hAddlTotalValue.value,2);
	var invcTot = 1 * roundNumber(document.all.hInvoiceAmount.value,2);
	var dhrPendingTot = 1 * roundNumber(document.all.hDHRPendingTotal.value,2);
	var totalBalance = 0;
	var amt = 0;

	readyTot = readyTot - addlTot;
	addlTot = 0;
	
	for(var i=0;i<len;i++)
	{
		addlObj = eval("form.Txt_Amt"+i);
		readyamt = addlObj.value;
		amt = 1 * readyamt;
		addlTot += amt ;
	}

	readyTot = readyTot + addlTot;

	document.all.hReadyTotal.value = roundNumber(readyTot,2);
	document.all.hAddlTotalValue.value = roundNumber(addlTot,2);
	totalBalance = invcTot - readyTot - dhrPendingTot;
	document.all.hTotalBalance.value = roundNumber(totalBalance,2);
	
	document.all.Lbl_AddlTotal1.innerHTML = "&nbsp;<B> " + formatNumber(document.all.hAddlTotalValue.value) + "</B>&nbsp;";
	document.all.Lbl_AddlTotal2.innerHTML = "&nbsp;<B> " + formatNumber(document.all.hAddlTotalValue.value) + "</B>&nbsp;";
	document.all.Lbl_ReadyTotal.innerHTML = "&nbsp;<B> " + formatNumber(document.all.hReadyTotal.value) + "</B>&nbsp;";
	document.all.Lbl_Balance.innerHTML = "&nbsp;<B> " + formatNumber(document.all.hTotalBalance.value) + "</B>&nbsp;";
}

function fnRemoveItem(val,form)
{
	addlCboObj = eval("document.all.Cbo_AddlChrgType"+val);
	addlCboObj.value = '0';

	var readyTot = 1 * roundNumber(document.all.hReadyTotal.value,2);
	var addlTot = 1 * roundNumber(document.all.hAddlTotalValue.value,2);
	var invcTot = 1 * roundNumber(document.all.hInvoiceAmount.value,2);
	var dhrPendingTot = 1 * roundNumber(document.all.hDHRPendingTotal.value,2);
	var totalBalance = 0;
	addlObj = eval("document.all.Txt_Amt"+val);
	
	addlTot = addlTot - 1 * addlObj.value;
	readyTot = readyTot - 1 * addlObj.value;

	addlObj.value = '';
	
	objGLNumber = eval("document.all.Lbl_GLAccNo"+val);
	objGLNumber.innerHTML = '';
	
	document.all.hReadyTotal.value = roundNumber(readyTot,2);
	document.all.hAddlTotalValue.value = roundNumber(addlTot,2);
	totalBalance = invcTot - readyTot - dhrPendingTot;
	document.all.hTotalBalance.value = roundNumber(totalBalance,2);
	
	document.all.Lbl_AddlTotal1.innerHTML = "&nbsp;<B> " + formatNumber(document.all.hAddlTotalValue.value) + "</B>&nbsp;";
	document.all.Lbl_AddlTotal2.innerHTML = "&nbsp;<B> " + formatNumber(document.all.hAddlTotalValue.value) + "</B>&nbsp;";
	document.all.Lbl_ReadyTotal.innerHTML = "&nbsp;<B> " + formatNumber(document.all.hReadyTotal.value) + "</B>&nbsp;";
	document.all.Lbl_Balance.innerHTML = "&nbsp;<B> " + formatNumber(document.all.hTotalBalance.value) + "</B>&nbsp;";
}

function fnPopulateGLNumber(obj,form)
{
	var objname = obj.name;
	var row = objname.substring(16,objname.length); //Cbo_AddlChrgType has 16 characters

	var arrayAddl = document.all.hstraddl.value.split("|");
    for(var i=0;i<arrayAddl.length-1;i++)
    {
    	var arrayAddlType = arrayAddl[i].split("^");

    	if(obj.value == arrayAddlType[1]){
	    	objGLNumber = eval("document.all.Lbl_GLAccNo"+row);
	    	objGLNumber.innerHTML = arrayAddlType[2];
	    	break;
    	}
    }
}

function focusToNewInvoice(){
	var obj = document.all.containerPONumber;
	obj.focus();
	//obj.diabled = true;
}

function fnFocusPO(val)
{
	//alert(val);
	var elem = document.frmAPInvoiceDtls.containerPONumber;
    if(elem != null)
	{
        if(elem.createTextRange)
		{
			var range = elem.createTextRange();
			range.move('character', 6);
            range.select();
        }
        else 
		{
            if(elem.selectionStart) 
			{
                elem.focus();
				elem.setSelectionRange(15,15);
            }
            else
                elem.focus();
        }
    }
}

function fnShowPartDesc(val)
{
	this.T_WIDTH=120;
	return escape(val);
}

function roundNumber(number, digits) {
	var multiple = Math.pow(10, digits);            
	var rndedNum = Math.round(number * multiple) / multiple;
	return rndedNum;        
}

function fnRefreshInvoiceList()
{
	alert(message_accounts[125]);
}
function fnLoad()
{
	//103301:DHR Verified No INV
	var defValue = '103301';

	if (document.frmDHRFlagPayment.hType.value != "")
	{
		defValue =document.frmDHRFlagPayment.hType.value;
	}
	document.frmDHRFlagPayment.Cbo_Type.value = defValue;
}

//This function is used to activate Load button while pressing the enter key
function fnEnter() {
	if(event.keyCode == 13){
		fnGo();
	}
}

//Function will call on page load. It will append companyInfo in each tab href and initiate ajaxtab.
function fnAjaxTabLoad(){
	fnAjaxTabAppendCmpInfo("#maintab");
	fnMainTab();
}

//initiate ajaxtab
function fnMainTab()
{
	var maintab=new ddajaxtabs("maintab","ajaxdivcontentarea");	

maintab.setpersist(false);
maintab.init();

maintab.onajaxpageload=function(pageurl){	
	if(pageurl.indexOf("gmDHRPayment.do")!=-1){	
		fnAddCompanyParams();
	}
}
fnFocusPO('from container');
	/* 
	maintab.onajaxpageload=function(pageurl){
		alert(pageurl);
	if (pageurl.indexOf("/gmDHRPayment.do")!=-1){					    
		fnFocusPO('from apinvoice');
	}
}
*/
}

function fnInvcDetails(strPaymentID)
{   
     windowOpener("/gmAPSaveInvoiceDtls.do?strOpt=viewInvc&hpaymentId="+strPaymentID,"viewInvc","resizable=yes,scrollbars=yes,top=250,left=300,width=900,height=500");
}


function fnViewPO(val)
{
	windowOpener("/GmPOServlet?hAction=ViewPO&hCloseFlag=Y&hPOId="+val,"PO","resizable=yes,scrollbars=yes,top=150,left=50,width=770,height=600");
}

// Function to calculate variance from invoice amount, ext credit amount and credit amount
function fnCreditAmount(id, form) {
	
	/*Calculating Ext Invoice Amount*/
	var invAmt = eval("form.TXT_INVAMT"+id).value;
	var creditAmt = eval("form.TXT_CREAMT"+id).value;
	
	if (!isNumberKey(invAmt)) {
		if(invAmt == ""){
			invAmt = 0;
		}
		else {			
			return false;
		} 
	}
	
	if (!isNumberKey(creditAmt)) {
		if(creditAmt == "") {
			creditAmt =0;
		}
		else {
			return false;
		} 
	}
	
	var objExtInvAmt = eval("form.hEXTINVAMT"+id);

	var lblExtInvAmt = document.getElementById('LBL_EXTINVAMT'+id);

	var attrQtyRec = eval("form.TXT_INVAMT"+id).getAttribute("attrqtyrec");

	var extInvAmt = parseInt(attrQtyRec) * invAmt;
	lblExtInvAmt.innerHTML = parseFloat(extInvAmt).toFixed(2);
	objExtInvAmt.value = parseFloat(extInvAmt).toFixed(2);
	
	/*Calculating diff Amount*/
	
	var accptedCost = eval("form.hACCEPTCOST"+id).value;
	var hDIFF = document.getElementById('hDIFF'+id);

	var lblVariance =  document.getElementById('LBL_DIFF'+id);

	var sumCreInvAmt = parseFloat(creditAmt) + parseFloat(extInvAmt);

	var differenceAmt = sumCreInvAmt - parseFloat(accptedCost);

	differenceAmt = parseFloat(differenceAmt).toFixed(2);
	lblVariance.innerHTML = differenceAmt;
	hDIFF.value = differenceAmt;
		
	var objPFamyOthers = document.getElementById('hPFamyOthers'+id);
		
	if(typeof(objPFamyOthers) != 'undefined' && objPFamyOthers != null){
		objPFamyOthers.value = differenceAmt; 
	}
	
	var objPFamyimplants = document.getElementById('hPFamyimplants'+id);
	
	if(typeof(objPFamyimplants) != 'undefined' && objPFamyimplants != null){
		objPFamyimplants.value = differenceAmt; 
    } 

	calcPPVAmounts();

}

//Function to calculate PPV values from addtional page - Only checked items
function calcPPVAmounts() {
	var objPFamy= document.getElementsByName('difference');
	var sumPFamyOthers = 0;
	var sumPFamyimplants = 0;
	for (var i=0; i< objPFamy.length; i++) {
		var objPFamyOthers = document.getElementById('hPFamyOthers'+i);
		var objCheck = document.getElementById('Chk_DHR'+i);
		if(objPFamyOthers != undefined) {
			var hPFamyOthers = parseFloat(objPFamyOthers.value);
			if (!isNumberKey(hPFamyOthers)) {
				hPFamyOthers = 0.00;
			}
			if(objCheck.checked) {
				sumPFamyOthers += hPFamyOthers; 
			}
		}
		
		var objPFamyimplants = document.getElementById('hPFamyimplants'+i);
		if (objPFamyimplants != undefined){
			var hPFamyimplants = parseFloat(objPFamyimplants.value);
			if (!isNumberKey(hPFamyimplants)) {
				hPFamyimplants = 0.00;
			}
			if(objCheck.checked) {
				sumPFamyimplants += hPFamyimplants; 
			}
		}
	}
	
	parent.document.getElementById('Lbl_PPV_Implants').innerHTML = "&nbsp;&nbsp;"+ parseFloat(sumPFamyimplants).toFixed(2);
	parent.document.getElementById('Lbl_PPV_Instruments').innerHTML = "&nbsp;&nbsp;"+ parseFloat(sumPFamyOthers).toFixed(2);
}

// Function to validate number and allow decimal and negative values
function isNumberKey(val)
{
	return /^-?(0|[1-9]\d*)(\.\d+)?$/.test(val);
}

function fnOnPageLoad() {
	calcPPVAmounts();
}