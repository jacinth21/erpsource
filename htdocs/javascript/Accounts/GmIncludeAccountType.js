/**
 * 
 */


// this function used to load the account type information
function fnDispatchLoad(){
		fnChangeAccSource(invSource,false) ;
}

// getAccountList - This function used to Change the Account source drop down -
// the reset the auto complete values
function fnChangeAccSource(invSource, blVal) {
	
	boolChange = blVal;
	invSource = document.all.Cbo_InvSource.value;
    var spnCurrLblObj = document.getElementById('spnCurrLbl');
//   PC-4721-added 50253 into the condition to show the currency dropdown
    if(invSource == '50255' || invSource == '50253'){

           if(spnCurrLblObj != undefined){
                  spnCurrLblObj.style.display = "inline";
           }
          
    }else{
           if(spnCurrLblObj != undefined){
        	   document.all.Cbo_Comp_Curr.value= defaultCurrency;
                  spnCurrLblObj.style.display = "none";
           }
    }

	if(invSource == '26240213'){  //26240213:Company Dealer
			if(document.all.Chk_ParentFl != undefined){
				document.all.Chk_ParentFl.checked = false;
				document.all.Chk_ParentFl.disabled =true;
			}
			document.all.Txt_AccId.disabled =true;
			document.getElementById('td_nm_id').innerHTML= lblDealerName;
	}else{
			fnCheckRepFl();
		}		
    
    if(invSource == 'ALL'){
	 	document.all.Chk_ParentFl.checked = false;
		document.all.Chk_ParentFl.disabled =true;
		document.all.Txt_AccId.disabled =true;
		document.getElementById('td_nm_id').innerHTML= lblDealerName +'/'+ lblRepAccountName;
    }

    // on change the Type drop down
	if (blVal == true) {
		document.all.Cbo_AccId.value = '';
		document.all.searchCbo_AccId.value = '';
		document.all.Txt_AccId.value = '';
		// to reset the Collector label
		fnSpanCltrReset();
	}
	var varAccId = document.all.Cbo_AccId.value;
	//to load the collector information
	if (varAccId != "" && varAccId != "0"){
		if (collectorFl == 'Y'){
			fnCollectorLoad();
		}
	}
}

// fnCheckRepFl - this function check if sales rep log in then to disable the parent account flag
function fnCheckRepFl() {
	if (repLblFl == 'Y') {
		document.getElementById('td_nm_id').innerHTML = lblRepAccountName;
	} else {
		document.getElementById('td_nm_id').innerHTML = lblAccountName;
	}
	if(document.all.Chk_ParentFl != undefined){
		document.all.Chk_ParentFl.disabled = false;
	}
	document.all.Txt_AccId.disabled = false;
}

// fnloadAcc - once call the auto complete - to update the account, currency and collector information. 
function fnloadAcc() {
	var accId = document.all.Cbo_AccId.value;
	invSource = document.all.Cbo_InvSource.value;
	if (accId != "0" && accId != '') {
		document.all.Txt_AccId.value = accId;
		// checking the type 50255=company customers and calling ajax to load
		// currency dropdown
		if (invSource == '50255' || invSource == '26240213') {
			fnSetAccCurrSym(accId, document.all.Cbo_Comp_Curr)
		}
		if (collectorFl == 'Y') {
			fnCollectorLoad();
		}
	} else {

		fnSpanCltrReset();
		document.all.Txt_AccId.value = '';
		fnResetAccCurrSymb();
	}
}
// fnSpanCltrReset - this function used to enable disable the control
function fnSpanCltrReset() {
	var objSpanCltr = document.getElementById("spanCollector");
	if (objSpanCltr) {
		document.getElementById("spanCollector").innerHTML = '';
	}
}
/*
 * fnAcIdBlur is Account id onblur function. Used to validate the Account ID
 * exist or not. Here we have all the available accID informations in JS Array
 * (Cbo_AccIdIDarr). Whenever the Account Type is changed, this array also gets
 * respective account information. Logic: First search the entered accid is
 * there in the array or not.If not throw validation. Else Loop through the JS
 * array, get the account name(from Cbo_AccIdNmarr) and set in in Account Name
 * dropdown.
 */
function fnAcIdBlur(obj) {
	Error_Clear();
	var txtAccID = document.all.Txt_AccId.value;
	var tmpInvSource = document.all.Cbo_InvSource.value;
	// to reset the Account values
	document.all.Cbo_AccId.value = "";
	document.all.searchCbo_AccId.value = "";
	if (txtAccID != '' && txtAccID != 0) {
		// 50255 Company Customers
		// 50253 Inter-Company Sale
		// 50257 Inter-Company Account

		var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmAccountInfo.do?method=validateAccountInfo&accountId=' + encodeURIComponent(txtAccID) + '&sourceId=' + tmpInvSource + '&ramdomId=' + Math.random());
		dhtmlxAjax.get(ajaxUrl, fnLoadAccAjax);
		
		// checkign the type 50255=company customers and calling ajax to load
		// currency dropdown
		if (tmpInvSource == '50255') {
			fnSetAccCurrSym(txtAccID, document.all.Cbo_Comp_Curr)
		} else if (txtAccID == '') {
			fnResetAccCurrSymb();

		}
		// When collector Flag is set as Y, call below to fetch the Collector ID
		// details.
		if (collectorFl == 'Y') {
			fnCollectorLoad();
		}

	}
}

// function to get Collector Attributes
function fnCollectorLoad() { // 103050-ACTCTR -- Collector Attribute
	var tmpAccId = document.all.Txt_AccId.value;
	if(tmpAccId != ''){
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/GmAccountServlet?hAction=fetchAccAttr&hAccAttrType=103050&hAccId=' + document.all.Txt_AccId.value + '&ramdomId=' + Math.random());
		dhtmlxAjax.get(ajaxUrl, callbackCollector);
	}
}
// this function used to load the collector information
function callbackCollector(loader) {
	restext = loader.xmlDoc.responseText;
	if(restext !='' || restext != undefined){
		parseCollectorName(restext);
	}
}
// to set the collector name
function parseCollectorName(restext) {
	if (restext != null && restext.length > 0) {
		var msgArr = restext.split("^");
		document.getElementById("spanCollector").innerHTML = msgArr[1];
	}
}
// to reset the account currency 
function fnResetAccCurrSymb() {
	var accCurrObj = document.all.Cbo_Comp_Curr;
	for (var i = 0; i < accCurrObj.options.length; i++) {
		if (accCurrObj.options[i].id == compCurrSign) {
			accCurrObj.options[i].selected = true;
			return;
		}
	}
}
// to send the currency information - based on account
function fnSetAccCurrSym(txtAccID, targetObj) {

	var dhxAccCurrUrl = fnAjaxURLAppendCmpInfo('/GmOrderAjaxServlet?opt=GETACCTCURR&acc='
			+ txtAccID + '&ramdomId=' + Math.random());
	dhtmlxAjax.get(dhxAccCurrUrl, function(loader) {
		var accCurrDtl;
		var response = loader.xmlDoc.responseText;
		if ((response != null || response != '')) {
			accCurrDtl = response.split("|");
			// currencyFmt = accCurrDtl[1];
			if (targetObj != undefined) {
				targetObj.value = accCurrDtl[0];
			}
		}
	});
}


//Description : Load the account/Distributor/Dealer name information
function fnLoadAccAjax(loader) {
	var errFlag = false;
	var xmlDoc;
	var pnum;
	var acname = '';
	
	var response = loader.xmlDoc.responseText;
	obj = JSON.parse(response);

	if (obj.length > 0) {
		acname = obj[0].NAME;
		
		if (acname != null && acname != '') {
			document.all.searchCbo_AccId.value = acname;
			document.all.Cbo_AccId.value = document.all.Txt_AccId.value;
		}
	}
	
	// to show the validation message
	if (document.all.searchCbo_AccId.value == '') {
		Error_Details(Error_Details_Trans(message_accounts[265], document.all.Txt_AccId.value));
		errFlag = true;
	}
	
	if( ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}
}
