function fnOnPageLoad(){
// we will have to load the Grid accordingly. The below function will do loading of Grid.
	fnLoadParentGrid();
}
//Loading the Grid based on the Parent Checkbox selected.
function fnLoadParentGrid()
{
	if (objGridData!=''){
		var footer = new Array();
			footer[0]= message_accounts[97];
			footer[1]="#cspan";
			footer[2]="#cspan";			
			footer[3]=currSymbol + "{#stat_total}";
			footer[4]=currSymbol + "{#stat_total}";
			footer[5]=currSymbol + "{#stat_total}";
			footer[6]=currSymbol + "{#stat_total}";
			footer[7]=currSymbol + "{#stat_total}";
			
		  gridObj = initGridWithDistributedParsing('ardealerdata',objGridData,footer);
		  gridObj.enableHeaderMenu();
		  region_Index = gridObj.getColIndexById("region");
		  credit_rating_Index = gridObj.getColIndexById("credit_rating");
		  last_credit_Index = gridObj.getColIndexById("last_credit");		 
	}
}

// Function resets the lists and the selections
function fnReset()
{	
	var form = document.frmARDealerRpt;
	form.Cbo_AccId.value = '0';
	form.searchCbo_AccId.value = '';
	form.Txt_AccId.value = '';
	form.Cbo_Division.value = '0';
}

// As the Grid loaded for Dealer in the XLS.
function fnDownloadXLS()
	{
	 gridObj.toExcel('/phpapp/excel/generate.php');
	 return false;
	}

function fnDealerSales(haction,url)
{
	loadSalesReport();
	if(haction!= '' && haction!=null && haction!=undefined)
	{
	frmObj.hAction.value = haction;
	}
	if(url!= '' && url!=null && url!=undefined)
	{
	frmObj.action = url;
	}
	frmObj.hCookieFlag.value = "NO";
	
	//below code to find distributor count of checked and unchecked values,to pass lesser value to avoid 1000 limit exception in query
	if(frmObj.Chk_Grp_ALL_Dist.checked == false){
	var distCheckVal=fnGetCheckedValues(frmObj.Chk_GrpDist);
	var distUnCheckVal =fnGetUnCheckedValues(frmObj.Chk_GrpDist);
	var arrDistCheck = new Array();
	var arrDistUnCheck = new Array();
	var arrDistCheckLen = 0;
	var arrDistUnCheckLen = 0;
	if(distCheckVal.indexOf(',') > 0){
	arrDistCheck=distCheckVal.split(",");
	arrDistCheckLen=arrDistCheck.length;
	}else if(distCheckVal!=''){
	arrDistCheckLen = 1;
	}
	
	if(distUnCheckVal.indexOf(',') > 0){
	arrDistUnCheck=distUnCheckVal.split(",");
	arrDistUnCheckLen=arrDistUnCheck.length;
	}else if(distUnCheckVal!=''){
	arrDistUnCheckLen = 1;
	}
	//if distributor checked value is greater than distributor unchecked value, we passing unchecked value
	if(arrDistCheckLen > arrDistUnCheckLen){
	frmObj.hDistFilterUnChk.value=distUnCheckVal;
	frmObj.hDistFilter.value='';
	}else{
	frmObj.hDistFilterUnChk.value='';
	}
	}else if(frmObj.Chk_Grp_ALL_Dist.checked == true){
	frmObj.hDistFilter.value='';
	frmObj.hDistFilterUnChk.value='';
	}
	
	fnStartProgress();
	frmObj.submit();
}

function loadSalesReport(){
	document.frmARDealerRpt.action="/gmARDealerRpt.do?method=loadFilters&hAction=ReLoadAR";
	document.frmARDealerRpt.submit();
}
//