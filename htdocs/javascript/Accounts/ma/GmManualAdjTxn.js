var mygrid='';
// this function is used to fetch MaTypes
//fnFetch is calling in both Screens 
function fnFetch(){
	var varmattyepid = document.frmManualAdjTxn.maTypeId.value;
	var varDeptId = document.frmManualAdjTxn.deptId.value;
	if (varDeptId == 2022)
	document.frmManualAdjTxn.updOprQty.checked = false;
	if (varmattyepid != '0'){
		document.frmManualAdjTxn.maDesc.value = '';
		document.frmManualAdjTxn.haction.value = strOpt1;
		document.frmManualAdjTxn.strOpt.value = "OnLoadMaType";
		document.frmManualAdjTxn.submit();
	}
}
// fetching all parts cost details
//Manual Adjustment Bulk Screen using this function, it's calling while click on fetch cost button 
// fnFetchCost() is calling fnCallAjax() function to fetch cost details 
function fnFetchCost(){
	
	fnValidateDropDn('maTypeId',lblMAType);
	fnValidateTxtFld('maDesc',lblMADesc);
	var gridChrows =mygrid.getChangedRows(",");
	/*if(gridChrows.length==0){
		Error_Details("nothing data modified,as there is no change done.");
	}*/
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	//fnStartProgress('Y');
	if(ErrorCount == 0){
		//alert(gridChrows.length);
		if(gridChrows.length !='0'){
			validateParts(true);
		}
	}
	
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	fnRemoveEmptyRow();
}

function validateParts(costFlag){
	var qtyEmpty = '';
	
	var gridrows = mygrid.getAllRowIds();
	var rowsarr = gridrows.split(",");
	varfromcosttype = document.frmManualAdjTxn.hmaFromType.value;
	
	if (rowsarr.length > 0) {
		
		for ( var rowid = 0; rowid < rowsarr.length; rowid++) {
			var qtyEmptyFl = false;
			var rowcurrentid = rowsarr[rowid];
			var editedFl = mygrid.getRowAttribute(rowcurrentid,"row_modified");
			if((editedFl == 'Y')||(editedFl == undefined)){
				pnum = mygrid.cells(rowcurrentid, mygrid.getColIndexById("part_no")).getValue();
				qty = mygrid.cells(rowcurrentid, mygrid.getColIndexById("ma_qty")).getValue();
				cost = mygrid.cells(rowcurrentid, mygrid.getColIndexById("cost_ea")).getValue();
				partDesc = mygrid.cells(rowcurrentid, mygrid.getColIndexById("part_des")).getValue();     
				extCost = mygrid.cells(rowcurrentid, mygrid.getColIndexById("ext_cost")).getValue();
				accQty = mygrid.cells(rowcurrentid, mygrid.getColIndexById("acc_qoh")).getValue();
				oprQty = mygrid.cells(rowcurrentid, mygrid.getColIndexById("opr_qoh")).getValue();
				
				if( pnum != '' && qty ==''){
					qtyEmpty = qtyEmpty + pnum +',';
					qtyEmptyFl = true;
				}
				if((pnum != '' && qty !='')){	// && ((cost == '')||(partDesc == '')||(extCost == '')||(accQty == '')||(oprQty == ''))
					if(!qtyEmptyFl && costFlag){
						fnCallAjax(rowcurrentid, pnum, qty, varfromcosttype);
					}
				}
				if(rowid == (rowsarr.length-1)){
					//fnStopProgress();
				}
			}
		}
		if(qtyEmpty !=''){
			// use substring to split the last comma.
			Error_Details(Error_Details_Trans(message_accounts[69],qtyEmpty));
		}
		if(!costFlag){
			var errFlag = false;
			for ( var rowid = 0; rowid < rowsarr.length; rowid++) {
				var rowcurrentid = rowsarr[rowid];
			    pnum = mygrid.cells(rowcurrentid, mygrid.getColIndexById("part_no")).getValue();
			    cost = mygrid.cells(rowcurrentid, mygrid.getColIndexById("cost_ea")).getValue();
			    partDesc = mygrid.cells(rowcurrentid, mygrid.getColIndexById("part_des")).getValue();     
				extCost = mygrid.cells(rowcurrentid, mygrid.getColIndexById("ext_cost")).getValue();
				accQty = mygrid.cells(rowcurrentid, mygrid.getColIndexById("acc_qoh")).getValue();
				oprQty = mygrid.cells(rowcurrentid, mygrid.getColIndexById("opr_qoh")).getValue();
				if((cost == '' || partDesc =='' || accQty =='' || oprQty == '') && pnum != '' ){
					errFlag = true;
				}
			}
			if(errFlag){
				Error_Details(message_accounts[70]);
			}
		}
		
	}
	
}
function fnCallAjax(rowcurrentid, pnum, qty, varfromcosttype){
	var urlTest = fnAjaxURLAppendCmpInfo("/GmCartAjaxServlet?pnum=" + encodeURIComponent(pnum)+ "&qty=" + encodeURIComponent(qty)+ "&FROMPG=MA" + "&HMAFROMTYPE=" +varfromcosttype+ "&ramdomId=" + Math.random());
	var loader=dhtmlxAjax.getSync(urlTest);//,
			//function(loader){
				var status = loader.xmlDoc.status;
				if(status == 200){  //On success stauts parsing the response values.
					var responseText = loader.xmlDoc.responseXML;
					var data = responseText.getElementsByTagName("data");
					var datalength = data[0].childNodes.length;
					if (responseText != null && datalength != 0) {
					        parseMessage(rowcurrentid, responseText);
					}else{
						setErrMessage(rowcurrentid, '<span><font color=red>'+message_accounts[71]+'</font></span>',pnum,qty);
					}
				}else{
					setErrMessage(rowcurrentid, '<span><font color=red>'+message_accounts[71]+'</font></span>',pnum,qty);
				}
			//});
}

function fnOnLoad(){
	if(strOpt1 == 'loadGrid'){
		
		if($(document).height() !=undefined || $(document).height() !=null){
			gridHeight = $(document).height() - 200;
		}
		var strDtlsJson = "";
	    mygrid = new dhtmlXGridObject('div_manualAdjGrid');
		mygrid.setHeader("Part Number,MA Qty,Acc QOH,Opr QOH,Part Description,Owner Company,Owner Company,Cost EA,Ext Cost,Local Cost,Ext. Local Cost,Owner Company ID");
		mygrid.setInitWidths("140,80,80,80,*,180,90,90,120,120,120,5");
		mygrid.setColAlign("left,left,left,left,left,left,left,right,right,right,right,left");
		mygrid.setColTypes("ed,ed,ro,ro,ro,coro,coro,ed,ro,ed,ro,ro");
		mygrid.setColSorting("str,int,str,str,str,str,str,int,int,int,int,str");
		mygrid.setColumnIds("part_no,ma_qty,acc_qoh,opr_qoh,part_des,owner_comp,owner_curr,cost_ea,ext_cost,local_cost,local_ext_cost,owner_id");
		mygrid.enableAutoHeight(true, gridHeight, true);
		
		mygrid.init();
		mygrid.enableAlterCss("even","uneven");
		mygrid.parse(strDtlsJson,"js");
		mygrid.setColumnHidden(11, true);
		fnAddFast120Rows();
		
		mygrid.enableBlockSelection();
		mygrid.copyBlockToClipboard();
		mygrid.enableEditEvents(false, true, true);
		mygrid.attachEvent("onKeyPress", onKeyPressed);
		mygrid.attachEvent("onEditCell",doOnCellEdit);
		mygrid.setImagePath("/images/dhtmlxGrid/imgs/");
		mygrid.attachEvent("onRowPaste",function(id){
             mygrid.forEachCell(id,function(obj){
                   obj.cell.wasChanged = true;
                   });
			});
		mygrid.enableUndoRedo();
		mygrid.enableMultiselect(true);
		mygrid.setAwaitedRowHeight(200);
//		PC-4903-copy paste issue in modern browser
//		initialize combo box
		var owner_comp_cell_ind = mygrid.getColIndexById("owner_comp");
		combo = mygrid.getCombo(owner_comp_cell_ind);
		combo.clear();
		combo.put('[Choose one]','[Choose one]');
		for(var i=0;i<ownerCmpArr.length;i++){
		combo.put(ownerCmpArr[i][0],ownerCmpArr[i][1]);
		}
		combo.save();
		var owner_curr_cell_ind =mygrid.getColIndexById("owner_curr");
		combo = mygrid.getCombo(owner_curr_cell_ind);
		combo.clear();
		combo.put('[Choose one]','[Choose one]');
		for(var i=0;i<ownerCurArr.length;i++){
		combo.put(ownerCurArr[i][0],ownerCurArr[i][1]);
		}
		combo.save();
	}	
}
//this function used to remove the empty rows
function fnRemoveEmptyRow() {
	
	mygrid.forEachRow(function(rowId) {
		part_no = TRIM(mygrid.cellById(rowId,mygrid.getColIndexById("part_no")).getValue());
		ma_qty = TRIM(mygrid.cellById(rowId,mygrid.getColIndexById("ma_qty")).getValue());
		if(part_no=='' || ma_qty =='') {
			mygrid.deleteRow(rowId);
        }
	});
}
//This function is used to initialize the grid with 120 rows
function fnAddFast120Rows(){
			mygrid.clearAll();
			mygrid.startFastOperations();
			for (var i = 120; i > 0; i--){
				mygrid.addRow(i,'');
			}
			mygrid.stopFastOperations();
}
function doOnCellEdit(stage, rowId, cellInd, nValue, oValue) {	
//	PC-4903-copy paste issue in modern browser
//	initialize combo box
	owner_comp_cell_ind = mygrid.getColIndexById("owner_comp");
	owner_curr_cell_ind =mygrid.getColIndexById("owner_curr");
	if(stage == 1 && (cellInd == owner_comp_cell_ind)){
	combo = mygrid.getCombo(owner_comp_cell_ind);
	combo.clear();
	combo.put('[Choose one]','[Choose one]');
	for(var i=0;i<ownerCmpArr.length;i++){
	combo.put(ownerCmpArr[i][0],ownerCmpArr[i][1]);
	}
	combo.save();
	}
	
	if(stage == 1 && (cellInd == owner_curr_cell_ind)){
	combo = mygrid.getCombo(owner_curr_cell_ind);
	combo.clear();
	combo.put('[Choose one]','[Choose one]');
	for(var i=0;i<ownerCurArr.length;i++){
	combo.put(ownerCurArr[i][0],ownerCurArr[i][1]);
	}
	combo.save();
	}
	//Below condition is added, to validate comments only when the price is changed.
	if (stage == 2 && nValue != oValue && cellInd == '0' && nValue != ''){
		mygrid.cellById(rowId, mygrid.getColIndexById("ma_qty")).setValue('');
		mygrid.cellById(rowId, mygrid.getColIndexById("acc_qoh")).setValue('');
		mygrid.cellById(rowId, mygrid.getColIndexById("opr_qoh")).setValue('');
		mygrid.cellById(rowId, mygrid.getColIndexById("part_des")).setValue('');
		mygrid.cellById(rowId, mygrid.getColIndexById("cost_ea")).setValue('');
		mygrid.cellById(rowId, mygrid.getColIndexById("ext_cost")).setValue('');
		// International posting changes
		mygrid.cellById(rowId, mygrid.getColIndexById("owner_comp")).setValue('');
		mygrid.cellById(rowId, mygrid.getColIndexById("owner_curr")).setValue('');
		mygrid.cellById(rowId, mygrid.getColIndexById("local_cost")).setValue('');
		mygrid.cellById(rowId, mygrid.getColIndexById("local_ext_cost")).setValue('');
		mygrid.setRowAttribute(rowId,"row_modified","Y");
	}  
	var costea =  mygrid.cells(rowId, mygrid.getColIndexById("cost_ea")).getValue();
	costea = RemoveComma(costea);
	var ma_qty = mygrid.cellById(rowId, mygrid.getColIndexById("ma_qty")).getValue();
	// International changes
    var localCost = mygrid.cells(rowId, mygrid.getColIndexById("local_cost")).getValue();
    localCost = RemoveComma(localCost);
	var owner_comp_id = mygrid.cellById(rowId, mygrid.getColIndexById("owner_comp")).getValue();
	if(compIdVal == owner_comp_id){
		localCost = costea;
	}	
	
	if(stage == 2 && cellInd == '1' && costea != '' && ma_qty != '' && nValue != oValue){
		var extRowCost =costea * ma_qty;
		mygrid.cellById(rowId, mygrid.getColIndexById("ext_cost")).setValue(extRowCost);
		fnsetLocalCost (rowId, owner_comp_id, costea, ma_qty, localCost);
		
	}
	if(stage == 2 && cellInd == '7' && costea != '' && ma_qty != '' && nValue != oValue){
		var extRowCost =costea * ma_qty;
		mygrid.cellById(rowId, mygrid.getColIndexById("ext_cost")).setValue(extRowCost);
		fnsetLocalCost (rowId, owner_comp_id, costea, ma_qty, localCost);
	}
	if(stage == 2 && cellInd == '7' && costea == '' && ma_qty != '' && nValue != oValue){
		mygrid.cellById(rowId, mygrid.getColIndexById("ext_cost")).setValue('');
	}
	if(stage == 2 && cellInd == '9' && localCost != '' && ma_qty != '' && nValue != oValue){
		fnsetLocalCost (rowId, owner_comp_id, costea, ma_qty, localCost);
	}
	return true;
}
function initGridData(divRef,gridData){
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.init();	
	gObj.loadXMLString(gridData);	
	return gObj;	
}
//keyPressed - copies the content of a block selection into clipboard in csv format 
function onKeyPressed(code,ctrl,shift){
	if(code==67&&ctrl){
		if (!mygrid._selectionArea) return alert(message_accounts[72]);
		mygrid.setCSVDelimiter("\t")
		mygrid.copyBlockToClipboard()
	}
	if(code==86&&ctrl){
		mygrid.setCSVDelimiter("\t")
		mygrid.pasteBlockFromClipboard()
		setTimeout(function() {	
					fnAddModified();
					fnRemoveEmptyRow();
					},1000);
	}
return true;
}
//PC-4903-copy paste issue in modern browser
//This function is to pick/diffrentiate the rows which have values
function fnAddModified() {
	mygrid.forEachRow(function(rowId) {
		part_no = TRIM(mygrid.cellById(rowId,mygrid.getColIndexById("part_no")).getValue());
		ma_qty = TRIM(mygrid.cellById(rowId,mygrid.getColIndexById("ma_qty")).getValue());
		if(part_no!='' || ma_qty !='') {
			mygrid.setRowAttribute(rowId,"row_modified","Y");
		}
	});
	//fnSave();
}
function fnConfirm() {
   return confirm(message_accounts[73]);   
 }
// this function is called in both screens
//strOpt-'loadGrid' - calling fnCreateGridOrderString() function for creating ManualAdjustmentTxn  - this function function only for Manual Adjustment(Bulk) screen
//calling another function -fnCreateOrderString() - this function function only for Manual Adjustment screen
function fnSubmit(){
	
	fnValidateDropDn('maTypeId',lblMAType);
	fnValidateTxtFld('maDesc',lblMADesc);

	if(strOpt1 == 'loadGrid'){
		
			document.frmManualAdjTxn.hinputStr.value = fnCreateGridOrderString();
			
			document.frmManualAdjTxn.haction.value = strOpt1;
			document.frmManualAdjTxn.strOpt.value = "save";
			
			validateParts(false);
			if (ErrorCount > 0)
			{
					Error_Show();
					Error_Clear();
					return false;
			}else{
				if(fnConfirm()){
					fnStartProgress('Y');
					document.frmManualAdjTxn.submit();
				}
			}
	}else{
		document.frmManualAdjTxn.hinputStr.value = fnCreateOrderString();
		document.frmManualAdjTxn.strOpt.value = "save";
		
		if (ErrorCount > 0)	{
			Error_Show();
			Error_Clear();
			return false;
		}
		if(fnConfirm()){
			fnStartProgress('Y');
			document.frmManualAdjTxn.submit();
		}
	}
	
	
	//alert(document.frmManualAdjTxn.hinputStr.value);
}

function fnCreateGridOrderString()
{
	var varMAType = document.frmManualAdjTxn.maTypeId.value;
	var varfromcosttype = document.frmManualAdjTxn.hmaFromType.value;
	
	var k;
	var j;
	var i;
	var obj;
	var str;
	var token = '^';
	var cnum = '';
	var status = true;
	var inputstr = '';
	var stockqty = '';
	var qty = 0;
	var objstock;
	var pnum = '';
	var parterrmsg = '';
	var qtyerrmsg = '';
	var objcostea;
	var costea=0;
	var costmsg ='';
	var ownerID = '';
	var localCost = 0;
	var ownerCompMsg = '';
	var localCostMsg = '';
	
	var gridrows = mygrid.getChangedRows(true);
	var rowsarr = gridrows.toString().split(",");
	//alert("rowsarr::::"+rowsarr);
		
	if (rowsarr.length > 0) {
		for ( k = 0; k < rowsarr.length; k++) {
			var rowcurrentid = rowsarr[k];
			pnum = mygrid.cells(rowcurrentid, mygrid.getColIndexById("part_no")).getValue();
			qty =  mygrid.cells(rowcurrentid, mygrid.getColIndexById("ma_qty")).getValue();
			costea =  mygrid.cells(rowcurrentid, mygrid.getColIndexById("cost_ea")).getValue();
			stockqty =  mygrid.cells(rowcurrentid, mygrid.getColIndexById("acc_qoh")).getValue();
			// international changes
			ownerID =  mygrid.cells(rowcurrentid, mygrid.getColIndexById("owner_comp")).getValue();
			localCost =  mygrid.cells(rowcurrentid, mygrid.getColIndexById("local_cost")).getValue();
			pnum = TRIM(pnum);

			if (pnum != ''){
				if(isNaN(qty)){
					j = k + 1;
					qtyerrmsg =  qtyerrmsg + ' '+ j ;
				}else{
					qty = parseInt (qty);
					localCost = parseFloat (RemoveComma(localCost));
				}
				str = pnum + token;
				str = str + qty + token;
				str = str + parseFloat (RemoveComma(costea)) + token;
				// international 
				str = str + ownerID + token;
				str = str + localCost + '|';
				
				inputstr = inputstr + str;
				if((costea == 0.00) && (stockqty != '')){
					costmsg = costmsg + ' ' + pnum +' ';
				}
				// International posting validation
				if(ownerID ==0){
					ownerCompMsg = ownerCompMsg + pnum  +' ';
				}
				if( localCost == 0.00 || localCost == ''){
					localCostMsg = localCostMsg + pnum  +' ';
				}
			}else if (!isNaN(qty) && qty != '')	{
					j = k + 1;
					parterrmsg =  parterrmsg + ' '+ j ;
			}
		}		
	}
	
	
	
	if(parterrmsg != ''){
		Error_Details(Error_Details_Trans(message_accounts[74],parterrmsg));
	}

	if(qtyerrmsg != ''){
		Error_Details(Error_Details_Trans(message_accounts[75],qtyerrmsg));
	}
	if(costmsg != ''){
		Error_Details(Error_Details_Trans(message_accounts[76],costmsg));
	}

	else if(inputstr == '' ){
		Error_Details(message_accounts[77]);
	}
	// International posting changes - validation added
	if(ownerCompMsg !=''){
		Error_Details(Error_Details_Trans(message_accounts[539],ownerCompMsg));
	}
	if(localCostMsg !=''){
		Error_Details(Error_Details_Trans(message_accounts[540],localCostMsg));
	}
	return inputstr;
}
// copies the selected data to clipboard
function docopy(){
    mygrid.setCSVDelimiter("\t");
	mygrid.copyBlockToClipboard();
	mygrid._HideSelection();
}
//paste the data from clipboard
function pasteToGrid(){
	
	if(mygrid._selectionArea!=null){
			mygrid.pasteBlockFromClipboard();
			mygrid._HideSelection();
	}else{
		alert(message_accounts[78]);
	}
}
function doredo(){
	mygrid.doRedo();
	fnCalculateTotal();
}
function doundo(){
	mygrid.doUndo();
	fnCalculateTotal();
}
function addRow(){
	mygrid.addRow(mygrid.getUID(),'');
	var divHeight=document.getElementById('div_manualAdjGrid').offsetHeight;
}
function removeSelectedRow(){

	var gridrows=mygrid.getSelectedRowId();
	if(gridrows!=undefined){
		var NameCellValue ;
		var gridrowsarr = gridrows.toString().split(",");
		var added_row;
		for (var rowid = 0; rowid < gridrowsarr.length; rowid++) {
			gridrowid = gridrowsarr[rowid];
			mygrid.setRowAttribute(gridrowid,"row_modified","N");
			mygrid.deleteRow(gridrowid);
			}
			//mygrid.deleteRow(gridrowid);
			//setRowAsModified(gridrowid,true);
		fnCalculateTotal();
		}
	else{
		Error_Clear();
		Error_Details(message_accounts[58]);				
		Error_Show();
		Error_Clear();
	}
}

// parseMessage() is called by  fnCallAjax() function
// parseMessage() function validate parts and calling two functions setErrMessage(),setMessage
function parseMessage(rowID, xmlDoc) 
{
	var strarr = 'qoh,pdesc,price,oprqoh,oprqohop,ownercd,ownerccy,localccy,localprice,ownerid';
	var pnum = xmlDoc.getElementsByTagName("pnum");
	var data = xmlDoc.getElementsByTagName("data");
	var datalength = data[0].childNodes.length;
	obj = mygrid.cellById(rowID,1).getValue();
	qty = parseFloat(obj);
	if(datalength == 0){
		 setErrMessage(rowID, '<span><font color=red>'+message_accounts[71]+'</font></span>',pnum,qty);
		 return;
	}
	var strobj =  strarr.split(',');

	var strlen = strobj.length;
	var strresult = '';

	for (var i=0;i<strlen ;i++ )
	{
		var str = strobj[i];
		for (var x=0; x<pnum[0].childNodes.length; x++) 
		{
			xmlstr = pnum[0].childNodes[x].tagName;
			
			if (xmlstr == str) 
			{
				strresult = strresult + '^' + parseXmlNode(pnum[0].childNodes[x].firstChild);
				break;
			}
		}
	}

strresult = strresult.substr(1,strresult.length);
var strobj =  strresult.split('^');

var qoh = strobj[0];
var pdesc = strobj[1];
var price = strobj[2];
//to get the owner information
var owner_cd = strobj[5];
var owner_ccy = strobj[6];
var local_ccy = strobj[7];
var local_price = strobj[8];
var ownerID = strobj[9];
var oprqoh = '';
//var deptid=document.frmCart.deptId.value;

if(deptId=='2022')
	oprqoh=strobj[3];
else
	oprqoh=strobj[4];


if(qoh == -9999)
	qoh = '-';

if(oprqoh == -9999)
	oprqoh = '-';

	var amount = qty*price;
	var localCost = parseFloat(local_price);
	var localExt = parseFloat(qty*localCost);
	
	if (pdesc == null || pdesc == '')
	{
		setErrMessage(rowID, '<span><font color=red>'+message_accounts[71]+'</font></span>',pnum,qty);
		return;
	}
	/*else if (price == '')
	{
		setErrMessage('<span class=RightTextBlue>Price is not set for this Part for this Account</span>');
		return;
	}*/
	else
	{
		setMessage(rowID, qoh,pdesc,price,amount,oprqoh,owner_cd,owner_ccy,local_ccy,localCost, localExt, ownerID);
	}
}
//setMessage - this function calling on AJax call
//this function is fetching all parts dec, cost details
//setMessage() is calling fnCalculateTotal() function to fetch total cost 
function setMessage(rowID, qoh,pdesc,price,amount,oprqoh,owner_cd,owner_ccy,local_ccy,localCost, localExt, ownerID) 
{
	qohmin = qoh;

	if(oprqoh != '-' &&  oprqoh <= qoh){
		qohmin = oprqoh;
	}
	mygrid.cellById(rowID, mygrid.getColIndexById("part_des")).setValue("&nbsp;" + pdesc);
	mygrid.cellById(rowID, mygrid.getColIndexById("acc_qoh")).setValue(qoh + "&nbsp;&nbsp;");
	mygrid.cellById(rowID, mygrid.getColIndexById("opr_qoh")).setValue(oprqoh + "&nbsp;&nbsp;");
	mygrid.cellById(rowID, mygrid.getColIndexById("cost_ea")).setValue(formatNumber(price));
	mygrid.cellById(rowID, mygrid.getColIndexById("ext_cost")).setValue(formatNumber(amount) );
	// international posting
	mygrid.cellById(rowID, mygrid.getColIndexById("owner_comp")).setValue(ownerID );
	mygrid.cellById(rowID, mygrid.getColIndexById("owner_curr")).setValue(owner_ccy );
	mygrid.cellById(rowID, mygrid.getColIndexById("local_cost")).setValue(formatNumber(localCost) );
	mygrid.cellById(rowID, mygrid.getColIndexById("local_ext_cost")).setValue(formatNumber(localExt) );
	mygrid.cellById(rowID, mygrid.getColIndexById("owner_id")).setValue(ownerID );

	fnCalculateTotal();
	
}
// fnCalculateTotal() is used to fetch total cost
function fnCalculateTotal()
{
	//var varRowCnt = document.frmCart.hRowCnt.value;
	var k;
	var obj;
	var qty = 0;
	var price = 0.0;
	var total = 0.0;
	var pnum = '';
	
	var gridrows = mygrid.getChangedRows(true);
	var rowsarr = gridrows.toString().split(",");
	//alert("rowsarr::::"+rowsarr);
		
	if (rowsarr.length > 0) {
		for ( var rowid = 0; rowid < rowsarr.length; rowid++) {
			var rowcurrentid = rowsarr[rowid];
			pnum = mygrid.cells(rowcurrentid, mygrid.getColIndexById("part_no")).getValue();
			qty =  mygrid.cells(rowcurrentid, mygrid.getColIndexById("ma_qty")).getValue();
			price =  mygrid.cells(rowcurrentid, mygrid.getColIndexById("local_cost")).getValue();
			if (pnum != ''){			
				total = total + (price * qty);
			}
		}		
	}
	//document.frmCart.Lbl_Grid_Total.innerHTML = '<b>$'+ formatNumber(total)+'</b>&nbsp;';
	document.all.Lbl_Grid_Total.innerHTML = '<b>'+ formatNumber(total)+'</b>&nbsp;';
}
function setErrMessage(rowID, msg,pnum,qty) 
{	
	if(rowID!=''){
		//alert("rowID::inside set error message:::"+rowID);
		mygrid.cellById(rowID, mygrid.getColIndexById("part_des")).setValue(msg);
		mygrid.cellById(rowID, mygrid.getColIndexById("part_no")).setValue(pnum);
		mygrid.cellById(rowID, mygrid.getColIndexById("ma_qty")).setValue(qty);
		mygrid.cellById(rowID, mygrid.getColIndexById("acc_qoh")).setValue('');
		mygrid.cellById(rowID, mygrid.getColIndexById("opr_qoh")).setValue('');
		mygrid.cellById(rowID, mygrid.getColIndexById("cost_ea")).setValue('');
		mygrid.cellById(rowID, mygrid.getColIndexById("ext_cost")).setValue('');
		// to set the empty values
		mygrid.cellById(rowID, mygrid.getColIndexById("owner_comp")).setValue('');
		mygrid.cellById(rowID, mygrid.getColIndexById("owner_curr")).setValue('');
		mygrid.cellById(rowID, mygrid.getColIndexById("local_cost")).setValue('');
		mygrid.cellById(rowID, mygrid.getColIndexById("local_ext_cost")).setValue('');
	}
}
//addRowFromClipboard() is used to copies multiple data from Excel Sheet
function addRowFromClipboard() {
	    var cbData = mygrid.fromClipBoard(); 
		var rowData = cbData.split("\n"); 
		var rowcolumnData='';
		//var ignoredRows ='<b>The Following Part/Group are not added as they are not Mapped to the Sheet. </b><br>';
		//var ignoredRowCount = 0;
	/*
		if(arrLen-1 < rowCount.length-1 ){
			
		}*/
		var gridrows   = mygrid.getAllRowIds();
		var gridrowsarr = gridrows.split(",");		
		var arrLen = gridrowsarr.length;
		var j = 0;
		for (var i=0; i < gridrowsarr.length; i++){
			row_id = gridrowsarr[i];
			var added_row = mygrid.getRowAttribute(row_id,"row_modified");
			var rowcolumnData = '';
			if( added_row != "Y" ){				
				if(rowData[j]){
					rowcolumnData = rowData[j].split("\t");	
					mygrid.cellById(row_id, mygrid.getColIndexById("part_no")).setValue(rowcolumnData[0]);
					mygrid.cellById(row_id, mygrid.getColIndexById("ma_qty")).setValue(rowcolumnData[1]);
					mygrid.setRowAttribute(row_id,"row_modified","Y");
					mygrid.addRow(mygrid.getUID(),'');
					gridrows   = mygrid.getAllRowIds();
					gridrowsarr = gridrows.split(",");	
				}
				j++;
			}
		}
}
// This function used to set the local cost value
function fnsetLocalCost (gridRowID, ownerCompId, cost, ma_qty, localCost){
	if(compIdVal == ownerCompId){
		localCost = cost;
		mygrid.cellById(gridRowID, mygrid.getColIndexById("local_cost")).setValue(localCost);
	}	
	mygrid.cellById(gridRowID, mygrid.getColIndexById("local_ext_cost")).setValue(formatNumber(localCost * ma_qty));
	fnCalculateTotal();
}