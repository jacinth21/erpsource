 var gridObj;
 var rowID = '';
 var check_rowId = '';
 var system_Qty_rowId = '';
 var counted_Qty_rowId = '';
 var override_Qty_rowId = '';
 var variance_Qty_rowId = '';
 var comments_rowId = '';
 var partNum_rowId = '';
 var overrideQtyVal = '';
 var countedQtyVal = '';
  
 
    function fnOnPageLoad(){
    
    	if(objGridData.value != ''){ 
    		gridObj = initGridData('dataGridDiv',objGridData);
    	    //gridObj.attachHeader('#rspan,#rspan,#rspan,#rspan,#rspan,#rspan,#rspan,#rspan,#rspan,#rspan,#rspan,#rspan');
    	    gridObj.attachHeader("#numeric_filter,<input type='checkbox' value='no' name='selectAll' onClick='javascript:fnChangedFilter(this);'/>"
					+ ',#text_filter,#text_filter,#select_filter,#select_filter,#numeric_filter,#numeric_filter,,,#numeric_filter,#select_filter,#select_filter,,,');
    	    gridObj.attachEvent("onEditCell",doOnCellEdit);
    		gridObj.attachEvent("onCheckbox", doOnCheck);
    		gridObj.enableEditTabOnly(true);
    		gridObj.enableEditEvents(true, false, true);
    		//
    		check_rowId = gridObj.getColIndexById("check");
    		partNum_rowId = gridObj.getColIndexById("part_Number");
    		system_Qty_rowId = gridObj.getColIndexById("system_Qty");
    		counted_Qty_rowId = gridObj.getColIndexById("Counted_Qty");
    		override_Qty_rowId = gridObj.getColIndexById("override_system_qty");
    		comments_rowId = gridObj.getColIndexById("comments");
    		variance_Qty_rowId = gridObj.getColIndexById("variance_Qty");

    	    gridObj.forEachRow(function(rowId) {
    	    	varianceQtyVal = gridObj.cellById(rowId, variance_Qty_rowId).getValue();
    	    	overrideQtyVal = gridObj.cellById(rowId, override_Qty_rowId).getValue();
    	    	countedQtyVal = gridObj.cellById(rowId, counted_Qty_rowId).getValue();
                // Comments and Check box to be enable (PBUG-2770)      	    
    	    	if(varianceQtyVal == 0){
    	    		//gridObj.cellById(rowId, check_rowId).setDisabled(true);
    	    		// Style update
    	    		gridObj.setCellTextStyle(rowId,override_Qty_rowId,"border-width: 0px;border-style: inset;");
    	    		//gridObj.setCellTextStyle(rowId,comments_rowId,"border-width: 0px;border-style: inset;");
    	    		// Disable the edit field
    	    		gridObj.cellById(rowId, override_Qty_rowId).setDisabled(true);
    	    		//gridObj.cellById(rowId, comments_rowId).setDisabled(true);	
    	    	}
    	    	if(overrideQtyVal != countedQtyVal && overrideQtyVal!=''){  
        			gridObj.setRowColor(rowId,"PINK");
        		}
    	    }); 
    	
    	 
    	}        
    	
    }
    
    function initGridData(divRef,gridData){
   	 
    	var gObj = new dhtmlXGridObject(divRef);
    	gObj.setSkin("dhx_skyblue");
    	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
    	//gObj.enableDistributedParsing(true);
    	gObj.enableTooltips("true,true,true,true,true");
    	//disable mode when read only cell is not available with tab
    	gObj.enableEditTabOnly(true);
    	gObj.init();
    	gObj.loadXMLString(gridData);
    	return gObj;	
    }
   
    
    
    function doOnCheck(rowId, cellInd, stage) {
    	rowID = rowId;
    	if (cellInd == check_rowId) {
    		fnSelectAllToggle('selectAll');
    	}
    	return true;
    }
    
  //this function is used to perform action when a cell is editing
    function doOnCellEdit(stage, rowId, cellInd, nValue, oValue) {
    	//when Qty updated then sed the modified rows (count_qty_rowID)		
    	if (stage == 1 && (cellInd == override_Qty_rowId || cellInd == comments_rowId)){
    		//gridObj.cellById(rowId, variance_Qty_rowId).
    	}
    	if (stage == 2 && nValue != oValue && (cellInd == override_Qty_rowId || cellInd == comments_rowId)) {
    		gridObj.setRowAttribute(rowId, "row_modified", "Y");
    		gridObj.setRowColor(rowId,"#9FF781");
    		// auto select the check box
    		check_id = gridObj.cellById(rowId, check_rowId).getValue();
    		over_ride_Qty = gridObj.cellById(rowId, override_Qty_rowId).getValue();
    		comment_Val = gridObj.cellById(rowId, comments_rowId).getValue();
    		countedQtyVal = gridObj.cellById(rowId, counted_Qty_rowId).getValue();
    		
    		if(check_id ==0){
    			gridObj.cellById(rowId, check_rowId).setValue('1');
    		}
    		// if values are removed to un check
    		if(over_ride_Qty =='' && comment_Val ==''){
    			gridObj.cellById(rowId, check_rowId).setValue('0');
    			gridObj.setRowColor(rowId,'');
    		}
    		// if any variance show the different color
    		if(over_ride_Qty != countedQtyVal && over_ride_Qty!=''){  
    			gridObj.setRowColor(rowId,"PINK");
    		}
    	}
    	return true;
    }
    
 // Description : This function to check the select all check box
    function fnSelectAllToggle(varControl) {
    	var objControl = eval("document.frmCycleCountApproval." + varControl);

    	var all_row = gridObj.getAllRowIds(",");
    	var all_rowLen = all_row.length; // get the rows id length
    	var checked_row = gridObj.getCheckedRows(check_rowId);
    	var varianceQtyStr = '';
    	var finalval;
    	var varianceQty = '';


    	finalval = eval(checked_row.length);
    	if (all_rowLen == finalval) {
    		objControl.checked = true;// select all check box to be checked
    	} else {
    		objControl.checked = false;// select all check box un checked.
    	}
    }
    
    
 // this function used to select all option (check box)
    function fnChangedFilter(obj) {
    	if (obj.checked) {
    		gridObj.forEachRow(function(rowId) {
    			var check_disable_fl = gridObj.cellById(rowId, check_rowId).isDisabled();
    			if (!check_disable_fl) {
    				gridObj.cellById(rowId, check_rowId).setChecked(true);
    			}
    		});
    	} else {
    		gridObj.forEachRow(function(rowId) {
    			check_id = gridObj.cellById(rowId, check_rowId).getValue();

    			if (check_id == 1) {
    				gridObj.cellById(rowId, check_rowId).setChecked(false);
    			}

    		});
    	}
    }
    
    function fnviewTransLog (pnum){
    	var inventoryType = document.frmCycleCountApproval.invMapId.value;
    	if( inventoryType == ''){
    		Error_Details("Currently No Inventory mapping");
       		Error_Show();
       		Error_Clear();
       		return false;
    	}
    	//
    	if(inventoryType == 'LOCATION'){
    		windowOpener('/gmLocationLog.do?method=fetchLocationLog&strOpt=load&partNum='+encodeURIComponent(pnum)+'&fromDate='+transLogDt+'&toDate='+todayDt, "Pack","resizable=yes,scrollbars=yes,top=150,left=150,width=1050,height=610");
    	}else{
    		windowOpener('/GmTranLogReportServlet?Txt_PartNum='+encodeURIComponent(pnum)+'&Txt_FromDate='+transLogDt+'&Txt_ToDate='+todayDt+'&cboType='+inventoryType,"Pack","resizable=yes,scrollbars=yes,top=150,left=150,width=980,height=610");
    	}
    		
    }
    
    
    function fnDownloadXLS()
    {
    	gridObj.setColumnHidden(check_rowId,true);
    	gridObj.toExcel('/phpapp/excel/generate.php');
    	gridObj.setColumnHidden(check_rowId,false);
    }

    function fnCycleCntSubmit(){
    	
    	document.frmCycleCountApproval.inputString.value = '';
    	fnValidateDropDn('chooseAction', 'Choose Action');
    	if (ErrorCount > 0) {
    		Error_Show();
    		Error_Clear();
    		return false;
    	}
    	gridObj.editStop();
    	
    	var chooseActionVal = document.frmCycleCountApproval.chooseAction.value;
    	// 106835 -- Initiate Recount
    	// 106836 -- Override Qty
    	// 106837 -- View Transaction Log
    	// 106838 -- Approve 
var partNumberStr = '';
var recountStr = '';
var overrideStr = '';
var approveStr = '';
// validation
var nonNumQty = '';
var negativeQty = '';
var nonCommentsStr = '';
var nonOverrideQtyStr = '';
var nonVarianceStr = '';

    	if(chooseActionVal ==  106837){// View Transaction Log
    		
    		// to get the checked rows
    		gridObj.forEachRow(function(rowId) {
    			check_id = gridObj.cellById(rowId, check_rowId).getValue();

    			if (check_id == 1) {
    				partNum = TRIM(gridObj.cellById(rowId, partNum_rowId).getValue());
    				partNumberStr = partNumberStr + partNum + ',';
    			}

    		});

    		if (partNumberStr != ''){
    			// remove the last comma
    			partNumberStr = partNumberStr.substring(0, partNumberStr.length - 1);
    			fnviewTransLog (partNumberStr);    			
    		}
    		
    		return false;
    	}else if(chooseActionVal ==  106835){// Initiate Recount

    		// to get the checked rows
    		gridObj.forEachRow(function(rowId) {
    			check_id = gridObj.cellById(rowId, check_rowId).getValue();

    			if (check_id == 1) {
    				varianceQtyVal = gridObj.cellById(rowId, variance_Qty_rowId).getValue();
    				
    				//
    				if(varianceQtyVal == 0){
    					partNum = gridObj.cellById(rowId, partNum_rowId).getValue();
    					nonVarianceStr = nonVarianceStr + partNum + ', ';
    				}
    				
    				recountStr = recountStr+ rowId +'|';
    			}

    		});

    		// if no variance and try to initiate the Recount show error message
    		if(nonVarianceStr !=''){
    			nonVarianceStr = nonVarianceStr.substring(0, nonVarianceStr.length - 2);
    			Error_Details("You cannot Initiate the Recount, No Variance Quntity for following Part(s) #/Consignment <b>" + nonVarianceStr + "</b>.");
    		}
    		
    		if (recountStr ==''){
    			Error_Details("Please select the Part #/Consignment to recount");			
    		}
			
    		// PC-5928 to remove the input string parameters and set the values in body
			document.frmCycleCountApproval.inputString.value = recountStr;
    		document.frmCycleCountApproval.action = '/gmCycleCountApproval.do?method=saveRecountDtls';
    	}else if(chooseActionVal ==  106836){// Override Qty
    		// to get the checked rows
    		gridObj.forEachRow(function(rowId) {
    			check_id = gridObj.cellById(rowId, check_rowId).getValue();
    			overrideVal = TRIM(gridObj.cellById(rowId, override_Qty_rowId).getValue());
    			commentsVal = TRIM(gridObj.cellById(rowId, comments_rowId).getValue());
    			if (check_id == 1) {
    				partNum = gridObj.cellById(rowId, partNum_rowId).getValue();
    				strQty = overrideVal.replace(/-?[0-9]+/g, ''); // Qty text box

    				if (overrideVal < 0){
    					negativeQty = negativeQty + partNum + ', ';
    				}

    				if (isNaN(overrideVal) || strQty.length > 0) {
    					nonNumQty = nonNumQty + partNum + ', ';
    				}
    				
    				if(commentsVal ==''){
    					nonCommentsStr = nonCommentsStr + partNum + ', ';
    				}
    				
    				if(overrideVal ==''){
    					nonOverrideQtyStr = nonOverrideQtyStr + partNum + ', '; 
    				}
    				overrideStr = overrideStr+ rowId +'^' + overrideVal + '^' + commentsVal +  '|';
    			}

    		});

    		if (nonNumQty != '') {
    			nonNumQty = nonNumQty.substring(0, nonNumQty.length - 2);
    			Error_Details('Entered value in <b>' + nonNumQty + '</b> is incorrect. Please enter numeric value in the Override Qty column');
    		}
    		
    		if (negativeQty != '') {
    			negativeQty = negativeQty.substring(0, negativeQty.length - 2);
    			Error_Details("Entered value in <b>" + negativeQty + "</b> can't be negative. Please enter valid numeric values in the Override Qty column");
    		}
    		
    		if(nonCommentsStr !=''){
    			nonCommentsStr = nonCommentsStr.substring(0, nonCommentsStr.length - 2);
    			Error_Details("Please Enter the <b>Comments</b> for following Part(s) #/Consignment <b>" + nonCommentsStr + "</b>.");
    		}
    		// to validate the Override Qty
    		if(nonOverrideQtyStr !=''){
    			nonOverrideQtyStr = nonOverrideQtyStr.substring(0, nonOverrideQtyStr.length - 2);
    			Error_Details("Please Enter the <b>Override System Qty</b> for following Part(s) #/Consignment <b>" + nonOverrideQtyStr + "</b>.");
    		}
    		
    		if (overrideStr ==''){
    			Error_Details("Please select the Part #/Consignment to override ");			
    		}
    		document.frmCycleCountApproval.inputString.value = overrideStr;
    		document.frmCycleCountApproval.action = '/gmCycleCountApproval.do?method=saveOverrideQtyDtls';
    	}
    	else if(chooseActionVal ==  106838){// Approve
    		// to get the checked rows
    		var checked_row = gridObj.getCheckedRows(check_rowId);
    		if(checked_row !=''){
    			var chkVal = checked_row.split(",");
        		//
        		for (var i=0;i<chkVal.length;i++){
        			check_id = gridObj.cellById(chkVal[i], check_rowId).getValue();
        			commentsVal = gridObj.cellById(chkVal[i], comments_rowId).getValue();
            		
        			if (check_id == 1) {
        				approveStr = approveStr + chkVal[i] +'^' + commentsVal + '|';
        			}
        		}
    		}

    		if (confirm("Are you sure you want to Approve ?")) {
    			document.frmCycleCountApproval.action = '/gmCycleCountApproval.do?method=saveApprovedDtls';
        		document.frmCycleCountApproval.inputString.value = approveStr;
    		}else{
    			return false;
    		}
    		
    	}
    	
    	if (ErrorCount > 0) {
    		Error_Show();
    		Error_Clear();
    		return false;
    	}
		fnStartProgress('Y');
    	document.frmCycleCountApproval.submit ();
    }
    
    function fnQtyHistory (lockDtlsId){
    	windowOpener("/GmPageControllerServlet?strPgToLoad=/gmAuditTrail.do&auditId=1181&txnId="+ lockDtlsId, "","resizable=yes,scrollbars=yes,top=250,left=300,width=785,height=300,status=1");
    	
    }
   // fnLoadCCScan used to PC-3314 Cycle count Review/Approval changes
    function fnLoadCCScan(lockDtlId,pnum,locid,cnid){
    	
    	windowOpener("/gmCycleCountLotRpt.do?method=loadScannedLots&cycleCountId="+lockDtlId+"&part="+encodeURIComponent(pnum)+"&locationId="+locid+"&consignmentId="+cnid+"&strPopupFlag=Y", "","resizable=yes,scrollbars=yes,top=250,left=300,width=1100,height=600,status=1");
    }
    