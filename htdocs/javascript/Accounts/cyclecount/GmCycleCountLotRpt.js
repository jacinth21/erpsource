//GmCycleCountLotRpt.js
//PC-3316 Cycle Count - Scanned Lots Report
var gridObj;
var gObj;

//PC-3495 To load data on report
function fnLoadRpt(){
	var cycleCountId = document.frmCycleCountLotRpt.cycleCountId.value;
	var part = document.frmCycleCountLotRpt.part.value;
    var locationId = document.frmCycleCountLotRpt.locationId.value;
    var consignmentId = document.frmCycleCountLotRpt.consignmentId.value;
    var pnumSuffix = document.frmCycleCountLotRpt.pnumSuffix.value;

	if(cycleCountId == "" && part == "" && locationId == "" && consignmentId == "") {
				Error_Details('Please Enter Any one value to proceed');
		       if (ErrorCount > 0) {
		   		Error_Show();
		   		Error_Clear();
		   		return false;
		   		}
		}else{
	// Using Ajax call for load the report
	fnStartProgress();
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmCycleCountLotRpt.do?method=fetchScannedLots&cycleCountId='+cycleCountId+'&part='+encodeURIComponent(part)+'&locationId='+locationId+'&consignmentId='+consignmentId+'&pnumSuffix='+pnumSuffix);
	var loader = dhtmlxAjax.getSync(ajaxUrl);
	fnLoadScannedLotsAjax(loader);
	fnStopProgress();	
	}
	
}

function fnLoadScannedLotsAjax (loader){
	
	response = loader.xmlDoc.responseText;

  	if(response != '') {

	var setInitWidths = "125,130,150,120,120,180,*,100";
    var setColAlign = "left,left,left,left,right,left,left,left";
    var setColTypes = "ro,ro,ro,ro,ro,ro,ro,ro";
    var setColSorting = "str,str,str,str,str,str,str,str";
    
    var setHeader = ["Cycle Count ID", "Location", "Part Number", "Lot Number", "Qty", "Scanned UDI/Lot", "Scanned By", "Scanned Date"];
    var setFilter = ["#text_filter", "#text_filter", "#text_filter", "#text_filter", "#text_filter", "#text_filter", "#text_filter", "#text_filter"];
    
    var setColIds = "cyccntlockid,locationid,partnm,lotnm,qty,scannedlot,scannedby,scanneddate";
    
    var enableTooltips = "true,true,true,true,true,true,true,true";
    var gridHeight = "300";
    
	
    var footerArry=new Array();
    var format = ""
    var dataHost = {};
    var footerStyles = [];
    
    document.getElementById("DivNothingMessage").style.display = 'none';
    
    gridObj = initGridObject('divScannedLotData');
    
	document.getElementById("divScannedLotData").style.display = 'block';
	document.getElementById("DivExportExcel").style.display = 'block';
    gridObj = fnCustomTypes(gridObj, setHeader, setColIds, setInitWidths,
				setColAlign, setColTypes, setColSorting, enableTooltips,
				setFilter, gridHeight, format,footerArry,footerStyles);
				
	gridObj.enableDistributedParsing(true);
		
	gridObj = loadDhtmlxGridByJsonData(gridObj, response);
	
    }else{
	document.getElementById("divScannedLotData").style.display = 'none';
	document.getElementById("DivNothingMessage").style.display = 'block';
	document.getElementById("DivExportExcel").style.display = 'none';
}	

}

//This Function Used to set custom types conditions in Grid
function fnCustomTypes(gObj, setHeader, setColIds, setInitWidths, setColAlign,
		setColTypes, setColSorting, enableTooltips, setFilter, gridHeight,
		format,footerArry,footerStyles) {
		gObj.setHeader(setHeader);
		var colCount = setHeader.length;
	
		gObj.setInitWidths(setInitWidths);
		gObj.setColAlign(setColAlign);
		gObj.setColTypes(setColTypes);
		gObj.setColSorting(setColSorting);
		gObj.enableTooltips(enableTooltips);
		gObj.attachHeader(setFilter);
		gObj.setColumnIds(setColIds);
		gObj.enableAutoHeight(true, gridHeight, true);
		
		if (footerArry != undefined && footerArry.length > 0) {
			var footstr = eval(footerArry).join(",");
			if (footerStyles != undefined && footerStyles.length > 0)
			gObj.attachFooter(footstr, footerStyles);
			else
			gObj.attachFooter(footstr);
		}

	return gObj;
}

function fnOnPageLoad() {

	var cycleCountId = document.frmCycleCountLotRpt.hcycleCountId.value;
	var part = document.frmCycleCountLotRpt.hpart.value;
    var locationId = document.frmCycleCountLotRpt.hlocationId.value;
    var consignmentId = document.frmCycleCountLotRpt.hconsignmentId.value;
    var strPopupFlag = document.frmCycleCountLotRpt.hstrPopupFlag.value;
    
	document.getElementById("divScannedLotData").style.display = 'none';
	document.getElementById("DivExportExcel").style.display = 'none';
	document.getElementById("DivNothingMessage").style.display = 'block';
	
    if(strPopupFlag == 'Y'){
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmCycleCountLotRpt.do?method=fetchScannedLots&cycleCountId='+cycleCountId+'&part='+encodeURIComponent(part)+'&locationId='+locationId+'&consignmentId='+consignmentId);
		var loader = dhtmlxAjax.getSync(ajaxUrl);
		fnLoadScannedLotsAjax(loader);
	}
}

function fnExcel(){ 
	gridObj.toExcel('/phpapp/excel/generate.php');
}
