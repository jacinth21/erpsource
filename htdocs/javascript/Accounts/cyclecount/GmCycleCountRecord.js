var gridObj;
var part_rowID = '';
var count_qty_rowID = '';
var recount_fl_rowID = '';
var location_row_id='';
var locationVal='';
var partno_row_id='';
var partnoVal='';
var rowID = '';
var hInputStr='';
var hPartInputStr='';
var rank_rowID='';
var lIcon_rowId='';
// this function used to load the grid XML 
function fnOnPageLoad() {

	if (objGridData.value != '') {
		gridObj = initGridData('dataGridDiv', objGridData);
		count_qty_rowID = 4;
		//gridObj.attachHeader('#rspan,#rspan,#rspan,#rspan,#rspan,#rspan,#rspan,#rspan,#rspan');
		if(document.frmCycleCountRecord.countType.value == 106816){ // set
			gridObj
			.attachHeader('#text_filter,#text_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#text_filter,#select_filter,#select_filter,');
		}else{
			gridObj
			.attachHeader('#text_filter,#text_filter,#text_filter,#select_filter,#select_filter,#select_filter,#select_filter,');
		}
		
		part_rowID = gridObj.getColIndexById("part_Number");
		count_qty_rowID = gridObj.getColIndexById("Count_Qty");
		recount_fl_rowID = gridObj.getColIndexById("recount_fl");
		rank_rowID=gridObj.getColIndexById("rank");
		part_fl_rowID=gridObj.getColIndexById("part_fl");
		var statusIdVal = document.frmCycleCountRecord.statusId.value;
		lIcon_rowId=gridObj.getColIndexById("lcon");
			gridObj.forEachRow(function(rowId) {
				
				if(statusIdVal == 106830){ // Recount
				recountFl = TRIM(gridObj.cellById(rowId, recount_fl_rowID).getValue());
				if (recountFl != 'Y') {//style="background-color:white;  border-width: 1px;border-style: inset;"
					//gridObj.cells(rowId, count_qty_rowID).setBgColor('red');
					gridObj.setCellTextStyle(rowId,count_qty_rowID,"border-width: 0px;border-style: inset;");
					gridObj.cellById(rowId, count_qty_rowID).setDisabled(true);
				}else{
					gridObj.setRowColor(rowId,"YELLOW");
				}
				} // end if Recount fl check
				rankStr=gridObj.cellById(rowId, rank_rowID).getValue();
				partStrFl=gridObj.cellById(rowId, part_fl_rowID).getValue();
				if(partStrFl=='Y'){
					gridObj.setRowColor(rowId,"ORANGE");
				}	
				if(statusIdVal == 106829){ // Counted
				   gridObj.cellById(rowId, lIcon_rowId).setValue('');	
				}
			});
	
		//Rank disable
		
	}

}
// this function used to create the DHTMLX object load the XML 
function initGridData(divRef, gridData) {

	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableTooltips("true,true,true,true,true");
	//enable light mouse navigation
	//gObj.enableLightMouseNavigation(true);
	//disable mode when read only cell is not available with tab
	gObj.init();
	gObj.loadXMLString(gridData);
	gObj.attachEvent("onEditCell", doOnCellEdit);
	gObj.enableEditTabOnly(true);
	gObj.enableEditEvents(true, false, true);
	return gObj;
}

// this function used to update the record information (partially)
function fnSave() {
	var inputStr = '';
	var nonNumQty = '';//notNumPrice + partNum +',';
	var negativeQty = '';
	//close opened editor and return value from editor to the cell
	gridObj.editStop();

	//get list of changed rows included added rows
	var gridChrows = gridObj.getChangedRows(',');
	if (gridChrows.length == 0) {
		Error_Details("Nothing to Save as No Modification done.");
	}
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	} else {
		var rowsarr = gridChrows.toString().split(",");
		if (rowsarr.length > 0) {
			for (var rowid = 0; rowid < rowsarr.length; rowid++) {
				rowId = rowsarr[rowid];
				count_qty = TRIM(gridObj.cellById(rowId, count_qty_rowID)
						.getValue());
				partNum = gridObj.cellById(rowId, part_rowID).getValue();
				strQty = count_qty.replace(/-?[0-9]+/g, ''); // Qty text box

				if (count_qty < 0){
					negativeQty = negativeQty + partNum + ', ';
				}

				if (isNaN(count_qty) || strQty.length > 0) {
					nonNumQty = nonNumQty + partNum + ', ';
				}
				inputStr = inputStr + rowId + '^' + count_qty + '|';
			}
		}
	}
	if (nonNumQty != '') {
		nonNumQty = nonNumQty.substring(0, nonNumQty.length - 2);
		Error_Details('Entered value in <b>' + nonNumQty + '</b> is incorrect. Please enter numeric value in the Counted Qty column');
	}
	
	if (negativeQty != '') {
		negativeQty = negativeQty.substring(0, negativeQty.length - 2);
		Error_Details("Entered value in <b>" + negativeQty + "</b> can't be negative. Please enter valid numeric values in the Counted Qty column");
	}
	//
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}

	document.frmCycleCountRecord.inputString.value = inputStr;
	fnStartProgress('Y');
	document.frmCycleCountRecord.action = "/gmCycleCountRecord.do?method=saveRecordInfo&strOpt=Save";
	document.frmCycleCountRecord.submit();
}

// this function used to update the record information with updated the status
function fnSubmit() {
	var inputStr = '';
	var nonNumQty = '';
	var negativeQty = '';
	var countTypeVal = document.frmCycleCountRecord.countType.value;
	var allQtyVal = 'Parts Number';
	//close opened editor and return value from editor to the cell
	gridObj.editStop();
	//get list of changed rows included added rows
	var gridChrows = gridObj.getChangedRows(',');

	if (gridChrows.length != 0) {
		var rowsarr = gridChrows.toString().split(",");
		for (var rowid = 0; rowid < rowsarr.length; rowid++) {
			rowId = rowsarr[rowid];
			count_qty = TRIM(gridObj.cellById(rowId, count_qty_rowID)
					.getValue());
			partNum = gridObj.cellById(rowId, part_rowID).getValue();
			strQty = count_qty.replace(/-?[0-9]+/g, ''); // Qty text box

			if (count_qty < 0){
				negativeQty = negativeQty + partNum + ', ';
			}

			if (isNaN(count_qty) || strQty.length > 0) {
				nonNumQty = nonNumQty + partNum + ', ';
			}
			inputStr = inputStr + rowId + '^' + count_qty + '|';
		}
	}

	if(countTypeVal == 106816){ // Set ID
		allQtyVal = 'Consignment'
	}
	// to get the record complete flag and send to hidden field
	var recordedFl = fnGetRecordedFl ();
	if(!recordedFl){
		Error_Details('Please Enter all the '+allQtyVal +' Qty to Proceed.');
	}

	
	if (nonNumQty != '') {
		nonNumQty = nonNumQty.substring(0, nonNumQty.length - 2);
		Error_Details('Entered value in <b>' + nonNumQty + '</b> is incorrect. Please enter numeric value in the Counted Qty column');
	}
	
	if (negativeQty != '') {
		negativeQty = negativeQty.substring(0, negativeQty.length - 2);
		Error_Details("Entered value in <b>" + negativeQty + "</b> can't be negative. Please enter valid numeric values in the Counted Qty column");
	}
	//
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	
	if (confirm("Are you sure you want to submit ?")) {
		document.frmCycleCountRecord.inputString.value = inputStr;
		document.frmCycleCountRecord.recordCompleteFl.value = 'Y';
		fnStartProgress('Y');
		document.frmCycleCountRecord.action = "/gmCycleCountRecord.do?method=saveRecordInfo&strOpt=Save";
		document.frmCycleCountRecord.submit();
	}

}

//this function is used to perform action when a cell is editing
function doOnCellEdit(stage, rowId, cellInd, nValue, oValue) {
	//when Qty updated then sed the modified rows (count_qty_rowID)				  
	if (stage == 2 && nValue != oValue && (cellInd == count_qty_rowID) && nValue != '') {
		gridObj.setRowAttribute(rowId, "row_modified", "Y");
		gridObj.setRowColor(rowId,"#9FF781");
	}
	return true;
}

// this function used to compare the total Qty and recored qty. If all the
// records are counted then returns to TRUE
function fnGetRecordedFl() {
	var recordCount = 0;
	var recorded_fl = false;
	// to calculate the recored Qty
	gridObj.forEachRow(function(rowId) {

		count_qty = TRIM(gridObj.cellById(rowId, count_qty_rowID).getValue());
		if (count_qty != '') {
			recordCount = recordCount + 1;
		}else{
			gridObj.setRowColor(rowId,"PINK");
		}

	});
	if (recordCount == document.frmCycleCountRecord.totalCount.value) {
		recorded_fl = true;
	}
	// return the recored qty
	return recorded_fl;
}

// this function used to print the cycle count sheet
function fnCycleCountPrint() {
	var lockID = document.frmCycleCountRecord.lockid.value;
	var strLotTrackFl = document.frmCycleCountRecord.strLotTrackFl.value;

	windowOpener('/gmCycleCountRecord.do?method=printDtls&lockid=' + lockID + '&strLotTrackFl=' + strLotTrackFl,
			"Pack",
			"resizable=yes,scrollbars=yes,top=150,left=150,width=900,height=610");

}

//This function used to print the reports
function fnPrint() {
	window.print();
}
// This Function used close the popup window
function fnClose() {
	window.close();
}

//This Function will hide the all button
function hidePrint() {
	strObject = eval("document.all.button");
	tdinnner = strObject.innerHTML;
	strObject.innerHTML = "";
}
//This Function will Show the all button after Print
function showPrint() {
	strObject = eval("document.all.button");
	strObject.innerHTML = tdinnner;
}

// this function used to download the grid data
function fnDownloadXLS() {

	gridObj.toExcel('/phpapp/excel/generate.php');

}
//Add new Button:To add parts

function fnAddNew(){

	var lockID = document.frmCycleCountRecord.lockid.value;
	document.frmCycleCountRecord.action = "/gmCycleCountRecord.do?method=loadPartInfo&strOpt=Load&lockid="+lockID;
	document.frmCycleCountRecord.submit();
	}

var lockDtlID = '';
var countedByRowID ='';
var countedDtRowID ='';
//This function used to open cycle count app when clicked 'L' icon
function fnLoadCCScan(ccLockDtlId){
	lockDtlID = ccLockDtlId;  // lockDtlID need to quantity get on focus window
    gridObj.setRowColor(ccLockDtlId,"#9FF781");
	windowOpener(ccMicroAppURL+'#/GmCCScanLots/'+ccLockDtlId,
			"Cycle Count App",
			"resizable=yes,scrollbars=yes,top=150,left=150,width=1000,height=810");
}

// this is used to parent window focus after child window closed
window.onfocus = function() { 
   if(lockDtlID !=''){
   var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmCycleCountRecord.do?method=fetchCycleCountDtls&lockid='+lockDtlID+'&randomId='+ Math.random());
	 dhtmlxAjax.get(ajaxUrl,function(loader){
	       response = loader.xmlDoc.responseText;
	       if((response != null || response != '') ){
	    	   var obj = JSON.parse(response);
	       
	           count_qty_rowID = gridObj.getColIndexById("Count_Qty");
	           countedByRowID = gridObj.getColIndexById("Counted_By");
	           countedDtRowID = gridObj.getColIndexById("Counted_Date");
	    
	          var selectedId = gridObj.getSelectedRowId();
	          if(obj[0].p_qty != '-999'){
	    	    gridObj.cellById(selectedId, count_qty_rowID).setValue(obj[0].p_qty);	
	    	    gridObj.cellById(selectedId, countedByRowID).setValue(obj[0].countedby);
	    	    gridObj.cellById(selectedId, countedDtRowID).setValue(obj[0].counteddate);
	         }
	       }
	 });
   }
}


