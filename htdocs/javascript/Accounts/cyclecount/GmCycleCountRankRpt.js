var gridObj;

// This function used to load grid at the page load time
function fnOnPageLoad() {
 
	if (objGridData.value != '') {
		//gridObj = initGridData('dataGridDiv', objGridData);
		gridObj = initGridWithDistributedParsing('dataGridDiv',objGridData);
		gridObj.attachHeader('#select_filter,#select_filter,#text_filter,#text_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter');
		gridObj.enableTooltips("true,true,true,true,true,true,true,true,true");
	}

}

// This function used to create the DHTMLX object and load the XML string
function initGridData(divRef, gridData) {

	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/extweb/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);
	gObj.enableTooltips("true,true,true,true,true");
	gObj.init();
	gObj.loadXMLString(gridData);
	return gObj;
}
//This Function Used to download Excel Sheet
function fnDownloadXLS() {
	gridObj.toExcel('/phpapp/excel/generate.php');

}
//the function used to load counted qty details
function fnLoad(){
	
	var cycleCountYear = document.frmCycleCountReport.cycleCountYear.value;
	// to set the Auto complete text values
	var projectNameVal = document.frmCycleCountReport.searchprojectId.value;
	document.frmCycleCountReport.projectName.value = projectNameVal;
	fnValidateDropDn('cycleCountYear', message_accounts[545]);
	
	if (ErrorCount > 0) {
		 Error_Show();
		 Error_Clear();
		 return false;
     }
	
	document.frmCycleCountReport.strOpt.value = "Load";
	fnStartProgress('Y');
	document.frmCycleCountReport.action = "/gmCycleCountReport.do?method=loadRankRpt";
	document.frmCycleCountReport.submit();
}


//This function used to press the enter key then call the Load function.
function fnEnter() {
	if (event.keyCode == 13) {
		fnLoad();
	}
}