var gridObj;

// This function used to load grid at the page load time
function fnOnPageLoad() {

	if (objGridData.value != '') {
		gridObj = initGridData('dataGridDiv', objGridData);
		gridObj.attachHeader('#select_filter,,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter');
	}

}

// This function used to create the DHTMLX object and load the XML string
function initGridData(divRef, gridData) {

	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);
	gObj.enableTooltips("true,true,true,true,true");
	gObj.init();
	gObj.loadXMLString(gridData);
	return gObj;
}
// This function used to load the cycle count dashboard details (based on filter) 
function fnLoad() {
	// validation
	CommonDateValidation(document.frmCycleCountDashBoard.fromDate,date_format,message_accounts[194]+ message[611]);
	CommonDateValidation(document.frmCycleCountDashBoard.toDate,date_format,message_accounts[195]+ message[611]);
	//
	var cyc_cnt_id = TRIM(document.frmCycleCountDashBoard.cycleCountId.value);
	var warehouseId = document.frmCycleCountDashBoard.warehouse.value;
	var statusId = document.frmCycleCountDashBoard.status.value;
	var frmDt = document.frmCycleCountDashBoard.fromDate.value;
	var toDt = document.frmCycleCountDashBoard.toDate.value;
	
	if(cyc_cnt_id =='' && warehouseId ==0 && statusId == 0 && (frmDt =='' || toDt =='') ){
		Error_Details(message[10512]);
	}
	
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	
	
	// if filter based on Cycle count id - Date filter not required - Setting to blank. 
	if(cyc_cnt_id != ''){
		document.frmCycleCountDashBoard.fromDate.value = '';
		//document.frmCycleCountDashBoard.toDate.value = '';
		document.frmCycleCountDashBoard.warehouse.value = 0;
		document.frmCycleCountDashBoard.status.value = 0;
	}
	document.frmCycleCountDashBoard.strOpt.value = "Load";
	document.frmCycleCountDashBoard.action = "/gmCycleCountDashboard.do?method=loadDashboard";
	fnStartProgress('Y');
	document.frmCycleCountDashBoard.submit();
}

// Once click the CC hyper link - load the Record screen
function fnLoadCycleCountLock(val, status)

{
	// 106827 - Open
	// 106828 - In Progress
	// 106830 - Recount 
	if (status == '' || status == 106827 || status == 106828 || status == 106830) {
		document.frmCycleCountDashBoard.action = "/gmCycleCountRecord.do?method=loadRecordInfo&strOpt=Load&lockid="
				+ val;
		document.frmCycleCountDashBoard.submit();
	} else {
		document.frmCycleCountDashBoard.action = "/gmCycleCountApproval.do?method=loadReviewDtls&lockid="
				+ val;
		document.frmCycleCountDashBoard.submit();
	}

}
// This function used to download the grid data
function fnDownloadXLS() {
	gridObj.toExcel('/phpapp/excel/generate.php');

}

//This function used to press the enter key then call the Load function.
function fnEnter() {
	if (event.keyCode == 13) {
		fnLoad();
	}
}