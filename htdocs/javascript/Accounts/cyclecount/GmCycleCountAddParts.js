
var hInputStr='';
var hPartInputStr='';
// this function used to load the grid XML 
function fnOnPageLoad() {
	
	if (objGridData.value != '') {
		gridObj = initGridData('dataGridDiv', objGridData);
	}
}

//this function used to create the DHTMLX object load the XML 
function initGridData(divRef, gridData) {

	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableTooltips("true,true,true,true,true");
	gObj.init();
	gObj.loadXMLString(gridData);
	gObj.enableEditTabOnly(true);
	gObj.enableEditEvents(true, false, true);
	return gObj;
}

//To save partnumber
function fnAddPartsSubmit(){
	var lockID = document.frmCycleCountRecord.lockid.value;
	if (objGridData.value != '') {
		partno_row_id = gridObj.getColIndexById("part_Number");
	   gridObj.forEachRow(function(rowId) {
	    	partnoVal = TRIM(gridObj.cellById(rowId, partno_row_id).getValue());
			hPartInputStr += (partnoVal + ",");
	    	hInputStr    = hInputStr + rowId +'^'+ partnoVal+',' +'|';
 	    });
	    }
	  hPartInputStr.replace(/\,/g,"");
     var hPartInputStrsplit = hPartInputStr.replace(/\,/g,"");
     if(hPartInputStrsplit==''){
    	 Error_Details("Please Enter Part Number to Proceed");
     }
     if (ErrorCount > 0) {
 		Error_Show();
 		Error_Clear();
 		return false;
 	}
	document.frmCycleCountRecord.lockid.value=lockID;
	document.frmCycleCountRecord.partInputString.value=hPartInputStr;
	document.frmCycleCountRecord.inputString.value=hInputStr;
	fnStartProgress('Y');
	document.frmCycleCountRecord.action = "/gmCycleCountRecord.do?method=addnewParts&strOpt=Save";
	document.frmCycleCountRecord.submit();
	}

//this function used to download the grid data
function fnDownloadXLS() {

	gridObj.toExcel('/phpapp/excel/generate.php');

}
