var gridObj;

// This function used to load grid at the page load time
function  fnLoadGridData() {
	 
	
	if (objGridData.value != '') {
		gridObj = initGridData('dataGridDiv', objGridData);
		// to get the column Size value and set the dynamic header filter
		var headerFilter = '#select_filter,#select_filter,#select_filter,#select_filter';
		var columnSize = gridObj.getColumnsNum();
		for(i=0; i< columnSize; i++){
			headerFilter += ',#numeric_filter';
		}
		gridObj.attachHeader(headerFilter);
		
		gridObj.setColumnLabel(0,"Part #");
		gridObj.setColumnLabel(1,"Part Description");
		gridObj.setColumnLabel(2,"Warehouse");
		gridObj.setColumnLabel(3,"Rank");
	}
	
}

// This function used to create the DHTMLX object and load the XML string
function initGridData(divRef, gridData) {

	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/extweb/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableTooltips("true,true,true,true,true");
	gObj.enableDistributedParsing(true);
	gObj.init();
	gObj.loadXMLString(gridData);
	
	return gObj;
}
function fnDownloadXLS() {
	gridObj.toExcel('/phpapp/excel/generate.php');

}
//the function used to load counted qty details
function fnLoad() {
	
	if(validateDate(document.frmCycleCountReport.fromDate.value,'From Date')
			  && validateDate(document.frmCycleCountReport.toDate.value, 'To Date')){
	
	document.frmCycleCountReport.strOpt.value = "Load";
	fnStartProgress('Y');
	document.frmCycleCountReport.action = "/gmCycleCountReport.do?method=loadCountedRpt";
	document.frmCycleCountReport.submit();
	}
}
//This function used to launch calendar 
function fnPageLoad() {
	launchCalendar();
}

//This function used to press the enter key then call the Load function.
function fnEnter() {
	
	if (event.keyCode == 13) {
		fnLoad();
	}
}
//This function used to validate date.
function validateDate(input, control){

	fnValidateTxtFld('fromDate',"From Date");
	fnValidateTxtFld('toDate',"To Date");

	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}else{
		input = input.toUpperCase();
		regExp=new RegExp("^(JAN|FEB|MAR|MAY|APR|JUL|JUN|AUG|OCT|SEP|NOV|DEC)/\\d{2}$");
		if(input.length>0 && input.search(regExp)<0){
			Error_Clear();
			Error_Details("Incorrect Date Format in <b>"+control+"</b>. Please Enter Date in Mon/YY Format.");				
			Error_Show();
			Error_Clear();
			return false;
		}else{
			return true;
		}
	}
}
var mDCal;
function launchCalendar(){
	mDCal = new dhtmlXDoubleCalendar("dhtmlxDblCalendar");
	//Set date format like Ex: Jan/14
	mDCal.setDateFormat("%M/%y");
	mDCal.show();
	document.getElementById("dhtmlxDblCalendar").style.display = "none";
}

function showHideCalendar(fromDate,toDate){
	var obj = document.getElementById("dhtmlxDblCalendar");
	
	//This line for today date will display in BOLD and this method will be in dhtmlxcalendar.js
	mDCal.enableTodayHighlight(true); // or false
	
	mDCal.show();
	
	// We are setting date taking from textbox in set in date picker.
	mDCal.setDates(document.getElementById("fromDate").value,document.getElementById("toDate").value);

	//When click any days and will call bellow code. 
	mDCal.attachEvent("onClick", function(side, date){
		if(side=="left")
			document.getElementById("fromDate").value = mDCal.getFormatedDate("%M/%y",date);
		else document.getElementById("toDate").value = mDCal.getFormatedDate("%M/%y",date);
	});
	// If click calendar icon will show, Once selected date again click calendar icon and will close.
	if(obj.style.display == "none"){
		obj.style.display = "block";
		mDCal.show();
	}else if(obj.style.display == "block"){
		obj.style.display = "none";
		mDCal.hide();
	}
}