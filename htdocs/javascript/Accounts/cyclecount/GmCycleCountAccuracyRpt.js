var gridObj;

// This function used to load grid at the page load time
function fnOnPageLoad() {
	launchCalendar();
	if (objGridData.value != '') {
		gridObj = initGridData('dataGridDiv', objGridData);
		
		// to get the column Size value and set the dynamic header filter
		var headerFilter = '#select_filter';
		var columnSize = gridObj.getColumnsNum();
		for(i=0; i< columnSize - 1; i++){
			headerFilter += ',#numeric_filter';
		}
		gridObj.attachHeader(headerFilter);

	}

}

// This function used to create the DHTMLX object and load the XML string
function initGridData(divRef, gridData) {
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/extweb/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableTooltips("true,true,true,true,true");
	gObj.enableDistributedParsing(true);
	gObj.init();
	gObj.loadXMLString(gridData);
	
	return gObj;
}
//This function used to load the cycle count Full data Reports details (based on filter) 
function fnLoad() {
	
	if(validateDate(document.frmCycleCountReport.fromDate.value,'From Date')
	  && validateDate(document.frmCycleCountReport.toDate.value, 'To Date')){
	document.frmCycleCountReport.strOpt.value = "Load";
	document.frmCycleCountReport.action = "/gmCycleCountReport.do?method=loadAccuracyRpt";
	fnStartProgress('Y');
	document.frmCycleCountReport.submit();
	}
}

//This function used to download the grid data
function fnDownloadXLS() {
	gridObj.toExcel('/phpapp/excel/generate.php');

}

//Calendar Function
var mDCal;
function launchCalendar(){
	mDCal = new dhtmlXDoubleCalendar("dhtmlxDblCalendar");
	//Set date format like Ex: Jan/14
	mDCal.setDateFormat("%M/%y");
	mDCal.show();
	document.getElementById("dhtmlxDblCalendar").style.display = "none";
}
//Hide calendar 
function showHideCalendar(fromDate,toDate){
	var obj = document.getElementById("dhtmlxDblCalendar");
	
	//This line for today date will display in BOLD and this method will be in dhtmlxcalendar.js
	mDCal.enableTodayHighlight(true); // or false
	
	mDCal.show();
	
	// We are setting date taking from textbox in set in date picker.
	mDCal.setDates(document.getElementById("fromDate").value,document.getElementById("toDate").value);

	//When click any days and will call bellow code. 
	mDCal.attachEvent("onClick", function(side, date){
		if(side=="left")
			document.getElementById("fromDate").value = mDCal.getFormatedDate("%M/%y",date);
		else document.getElementById("toDate").value = mDCal.getFormatedDate("%M/%y",date);
	});
	// If click calendar icon will show, Once selected date again click calendar icon and will close.
	if(obj.style.display == "none"){
		obj.style.display = "block";
		mDCal.show();
	}else if(obj.style.display == "block"){
		obj.style.display = "none";
		mDCal.hide();
	}
}
//Date Validation
function validateDate(input, control){
	
	fnValidateTxtFld('fromDate', message_accounts[194]);
	fnValidateTxtFld('toDate', message_accounts[195]);
	fnValidateDropDn('varianceBy', message_accounts[546]);

	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}else{
		input = input.toUpperCase();
		regExp=new RegExp("^(JAN|FEB|MAR|MAY|APR|JUL|JUN|AUG|OCT|SEP|NOV|DEC)/\\d{2}$");
		if(input.length>0 && input.search(regExp)<0){
			Error_Clear();
			Error_Details("Incorrect Date Format in <b>"+control+"</b>. Please Enter Date in Mon/YY Format.");				
			Error_Show();
			Error_Clear();
			return false;
		}else{
			return true;
		}
	}
}