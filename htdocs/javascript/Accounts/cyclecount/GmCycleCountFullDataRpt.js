var gridObj;

// This function used to load grid at the page load time
function fnOnPageLoad() {
	if (objGridData.value != '') {
		gridObj = initGridData('dataGridDiv', objGridData);
		gridObj.attachHeader("#select_filter,#select_filter,#text_filter,#text_filter,#numeric_filter"
				+",#numeric_filter,#numeric_filter,#numeric_filter,#select_filter"
				+",#numeric_filter,#numeric_filter,#numeric_filter,#numeric_filter,#numeric_filter,#select_filter"
				+",#numeric_filter,#numeric_filter,#numeric_filter,#numeric_filter,#numeric_filter,#numeric_filter"
				+',#text_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter');
		gridObj.enableTooltips("true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true");
		//gridObj.setColumnColor("white,#d5f1ff,#d5f1ff");
	}

}

// This function used to create the DHTMLX object and load the XML string
function initGridData(divRef, gridData) {

	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);
	gObj.enableTooltips("true,true,true,true,true");
	gObj.init();
	gObj.loadXMLString(gridData);
	return gObj;
}
//This function used to load the cycle count Full data Reports details (based on filter) 
function fnLoad() {
	// validation
	CommonDateValidation(document.frmCycleCountReport.fromDate,date_format,message_accounts[194]+ message[611]);
	CommonDateValidation(document.frmCycleCountReport.toDate,date_format,message_accounts[195]+ message[611]);
	//
	var cyc_cnt_id = TRIM(document.frmCycleCountReport.cycleCountId.value);
	var warehouseId = document.frmCycleCountReport.warehouse.value;
	var part = document.frmCycleCountReport.part.value;
	var frmDt = document.frmCycleCountReport.fromDate.value;
	var toDt = document.frmCycleCountReport.toDate.value;
	
	if(cyc_cnt_id =='' && warehouseId ==0 && part == 0 && (frmDt =='' || toDt =='') ){
		Error_Details(message[10512]);
	}
	
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	document.frmCycleCountReport.strOpt.value = "Load";
	document.frmCycleCountReport.action = "/gmCycleCountReport.do?method=loadFullDataRpt";
	fnStartProgress('Y');
	document.frmCycleCountReport.submit();
}

//This function used to download the grid data
function fnDownloadXLS() {
	gridObj.toExcel('/phpapp/excel/generate.php');

}