var strPrintCount='';
// This function used to generate the invoices 
//This JS is used in  GmInvoiceEdit.jsp and GmINInvoiceEdit.jsp.
function fnGenerateInvoice(btnId) {
	
	var str = '';
	var accessflg = '';
	var OUSDistFl = '';
	var actionVal = document.frmInvoice.hAction.value;
	// Added accessflg to restrict the user to generate invoice for OUS Distributors	
	if (document.frmInvoice.hAccFlg != undefined) {
		accessflg = eval(document.frmInvoice.hAccFlg).value;		
	}
	if (document.frmInvoice.hOUSDistFl != undefined) {		
		OUSDistFl = eval(document.frmInvoice.hOUSDistFl).value;
	}
	if (actionVal == 'INV') {
		var dateDifference = dateDiff(document.frmInvoice.Txt_InvDate.value, orderDate, dateFmt);
		var invDate = '';
		if (document.frmInvoice.Txt_InvDate != undefined) {
			invDate = TRIM(document.frmInvoice.Txt_InvDate.value);
		}
		if (invDate == '') {
			Error_Details(message_accounts[148]);
		} else {
			CommonDateValidation(document.frmInvoice.Txt_InvDate, dateFmt,
					message_accounts[149] + message[611]);
			if (ErrorCount == 0) {
				diff = dateDiff(todaysDate, invDate, dateFmt);
				if (diff > 0) {
					Error_Details(message_accounts[150]);
				}
			}
			// Tax alert
			if (taxCountryFl == 'Y' && validAddFl != 'Y' && taxDate == 'Y') {
				// 4002 - The address has not been validated and incorrect tax
				// calculation is possible. Do you want to proceed?
				Error_Details(message[4002]);
			}
		}
		//If invoice changed date is less than order raised date #validate
		if(dateDifference>0){
			Error_Details(message_accounts[151]);
		}
		
		if(OUSDistFl == 'Y' && accessflg != 'Y')
			{
			Error_Details(message[803]);
			}
		
		if (ErrorCount > 0) {
			Error_Show();
			Error_Clear();
			return false;
		}
		document.frmInvoice.hAction.value = "GEN";

		if (btnId == 'GenOBO') {
			document.frmInvoice.hInvType.value = "50201";
		} else {
			document.frmInvoice.hInvType.value = "50200";
		}
	} else if (actionVal == 'IC' || actionVal == 'IDB') { // Issue Debit , Issue Credit
		if (actionVal == 'IC') {
			document.frmInvoice.strOpt.value = 'CREDIT';
		} else if (actionVal == 'IDB') {
			document.frmInvoice.strOpt.value = 'DEBIT';
		}
		cnt = document.frmInvoice.hTotalCnt.value;

		var blQty = false;
		var blPrice = false;
		var blTotalPrice = false;
		var blInvQty = false;
		var blNegativePrice = false;
		var errQty = '';
		var errAmt = '';
		var errTotalAmt = '';
		var errNegativeAmt = '';
		var errInvQty = '';
		for ( var i = 0; i < cnt; i++) {
			obj = eval("document.frmInvoice.hOrderID" + i);
			obj1 = eval("document.frmInvoice.hPNum" + i);
			obj2 = eval("document.frmInvoice.Txt_CreditQty" + i);
			obj3 = eval("document.frmInvoice.Txt_CreditAmt" + i);
			obj4 = eval("document.frmInvoice.hTotalPrice" + i);
			objtype = eval("document.frmInvoice.hItemType" + i);
			objinvQty = eval("document.frmInvoice.h_InvQty" + i);
			objItemOrdId = eval("document.frmInvoice.hItemOrdId" + i);
			objunitprice = eval("document.frmInvoice.h_unitprice" + i);
			//PC-2288 - Trying to enter a credit memo system showing the validation message
			objNetUnitPrice = eval("document.frmInvoice.h_NetUnitPrice" + i);			

			orderId = obj.value;
			partNum = obj1.value;
			creditQty = TRIM(obj2.value);
			creditAmt = TRIM(obj3.value);
			totalPrice = TRIM(obj4.value);
			typeval = objtype.value;
			invoiceQuantity = objinvQty.value;
			itemOrdId = objItemOrdId.value;
			unitprice = TRIM(objunitprice.value);
			//PC-2288 - Trying to enter a credit memo system showing the validation message
			netUnitPrice = TRIM(objNetUnitPrice.value);			
			
			strQty = creditQty.replace(/[0-9]+/g, ''); // Qty text box
			strAmt = creditAmt.replace(/-?[.0-9]+/g, ''); // Amt text box
			
			// validate the 0 qyt and price
			if ((creditQty != '' && creditQty <= 0) || (creditAmt != '' && creditQty == '')
					|| (strQty.length > 0)) {
				blQty = true;
				errQty = errQty + "<b>" + obj1.value + "</b>,";
			}
			
			if ((creditAmt != '' && (parseFloat(totalPrice)!=0 && parseFloat(creditAmt)==0)) || (strAmt.length > 0)
					|| (creditQty != '' && creditAmt == '')) {
				
				blPrice = true;
				errAmt = errAmt + "<b>" + obj1.value + "</b>,";
			}
			//PC-2288 - Trying to enter a credit memo system showing the validation message
			if(actionVal == 'IC' && (parseFloat(netUnitPrice) < parseFloat(creditAmt)))   // PMT-39771 - System Validating for A/R Issue Credit quantity and amount
				{
				blTotalPrice = true;
				errTotalAmt = errTotalAmt + "<b>" + obj1.value + "</b>,";
				}
			//PC-2288 - Trying to enter a credit memo system showing the validation message	
			if(actionVal == 'IC' && (parseFloat(creditAmt) < 0 && parseFloat(netUnitPrice) > 0))
				{
				blNegativePrice = true;
				errNegativeAmt = errNegativeAmt + "<b>" + obj1.value + "</b>,";
				}
			/*
			 * The Below Parse int code is added for PMT-5510 to fix the issue
			 * in credit memo screen.The Error Occurs when the order quantity
			 * for the parts is in two digits. When the order quantity is in two
			 * digits the below condition is satisfied and blInvQty is true. So
			 * the below mentioned error is thrown.
			 */
			if (parseInt(creditQty) > parseInt(invoiceQuantity)) {
				blInvQty = true;
				errInvQty = errInvQty + "<b>" + obj1.value + "</b>,";
			}
			if (creditQty != '' && creditAmt != '') {
				str = str + orderId + "^" + partNum + "^" + creditQty + "^" + creditAmt + "^"
						+ typeval + "^" +itemOrdId+ "|";
			}
		}

		if (blQty) {
			errQty = errQty.substring(0, errQty.length - 1);
			Error_Details(Error_Details_Trans(message_accounts[152],errQty));
		}

		if (blPrice) {
			errAmt = errAmt.substring(0, errAmt.length - 1);
			Error_Details(Error_Details_Trans(message_accounts[153],errAmt));
		}
		
		if (blTotalPrice) {
			errTotalAmt = errTotalAmt.substring(0, errTotalAmt.length - 1);
			Error_Details(Error_Details_Trans(message_accounts[314],errTotalAmt));
		}
		if(blNegativePrice) {
			errNegativeAmt = errNegativeAmt.substring(0, errNegativeAmt.length - 1);
			Error_Details(Error_Details_Trans(message_accounts[316],errNegativeAmt));	
		}
		
		if (str == '' && ErrorCount == 0) {
			Error_Details(message_accounts[154]);
		}

		if (blInvQty) {
			errInvQty = errInvQty.substring(0, errInvQty.length - 1);

			if(actionVal == 'IC'){
				Error_Details(Error_Details_Trans(message_accounts[155],errInvQty));
			}	
			if(actionVal == 'IDB'){
				Error_Details(Error_Details_Trans(message_accounts[541],errInvQty));
			}
		}
		
		// used lot recored then validate the qty
		if(orderUsageLotFl =='Y' && orderDDTFl =='Y' && usageLotDataFl == 'Y'){
			fnValidateUsedLot ();
		}

		if (ErrorCount > 0) {
			Error_Show();
			Error_Clear();
			return false;
		}
		document.frmInvoice.hStr.value = str;
		document.frmInvoice.action = "/GmProcessCreditsServlet";
		document.frmInvoice.hAction.value = "CRE";
	} else if (actionVal == 'PAY' || actionVal == 'UPD') {
		fnGetPayDetails();
		fnCheckMode();
		if (ErrorCount > 0) {
			Error_Show();
			Error_Clear();
			return false;
		}
	} else if (actionVal == 'EC') {
		document.frmInvoice.hAction.value = "EC";
	} else {
		document.frmInvoice.hAction.value = "UPD";
	}
	fnStartProgress("Y");
	document.frmInvoice.submit();
}
// This function used to to print the invoice
function fnPrintInvoice() {
	var varinv = document.frmInvoice.hInv.value;
	//for populating three pop up window
	if(strPrintCount =='3'){
		fnPrintInvMultiPopulate("ORG_RECIPIENT", varinv);
		fnPrintInvMultiPopulate("DUP_TRANSPORTER", varinv);
		fnPrintInvMultiPopulate("TRIP_SUPPLIER", varinv);
	}
	else{
		windowOpener('/GmInvoiceInfoServlet?hAction=Print&hInv=' + varinv,"PrntInv","resizable=yes,scrollbars=yes,top=150,left=200,width=790,height=700");
	}
}
//this function is used to populate pop up window multiple times
function fnPrintInvMultiPopulate(printPurpose, varinv){
	windowOpener('/GmInvoiceInfoServlet?hAction=Print&hInv='+varinv +"&PrintPurpose="+ printPurpose,printPurpose,"resizable=yes,scrollbars=yes,top=150,left=200,width=790,height=700");

}
// This function used to print the pack slip details
function fnPrintPack(val) {
	windowOpener('/GmEditOrderServlet?hMode=PrintPack&hOrdId=' + val, "Pack",
			"resizable=yes,scrollbars=yes,top=150,left=150,width=760,height=610");
}
// This function used to print the credit memo details
function fnPrintCreditMemo(val) {
	windowOpener(
			'/GmReportCreditsServlet?hAction=PrintVersion&hOrderId=' + val,
			"Credit",
			"resizable=yes,scrollbars=yes,top=150,left=200,width=820,height=600");
}
// This function used to print the cash adjustment memo details
function fnPrintCashAdjustMemo(val) {
	windowOpener(
			'/GmReportCreditsServlet?hAction=PrintCashAdj&hOrderId=' + val,
			"Credit",
			"resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}
// This function used to update the PO details
function fnUpdatePO() {
	windowOpener(poJsp, "PrntInv",
			"resizable=yes,scrollbars=yes,top=250,left=300,width=650,height=200");
}
// This function used to submit the PO information.
function fnSubmitPO() {
	document.frmInvoice.hAction.value = "UPDPO";
	document.frmInvoice.submit();
}
// This function used to open the comments log information
function fnOpenLog(id) {
	windowOpener("/GmCommonLogServlet?hType=1201&hID=" + id, "PrntInv",
			"resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
}
// This function used to print the paper work
function fnPrint() {
	window.print();
}
// This function used to getting the payment details.
function fnGetPayDetails() {
	var theinputs = document.getElementsByTagName("input");
	var purpose = '';
	var desc = '';
	var amt = 0;
	var date = '';
	var str = '';
	var cnt = 0;
	for ( var n = 0; n <= 5; n++) {
		// val = theinputs[n].value;
		val = n;
		obj = eval("document.frmInvoice.Cbo_Purpose" + val);
		purpose = obj.value;
		obj = eval("document.frmInvoice.Txt_Desc" + val);
		desc = obj.value;
		objAmt = eval("document.frmInvoice.Txt_Amt" + val);
		amt = RemoveComma(objAmt.value);
		objDate = eval("document.frmInvoice.Txt_Date" + val);
		date = objDate.value;

		// str = str + purpose + '^' + desc + '^' + amt + '^' + date + '|';
		if (desc != '' && amt != 0 && date != '') {
			if (purpose == 5010 || purpose == 5011 || purpose == 5012 || purpose == 5023 || purpose == 26240161 || purpose == 26240162) {
				//5010-check , 5011-Bank Transfer , 5012-Credit card , 5023- Wire transfer
				// 2700 -- Payment
				str = str + purpose + '^' + desc + '^' + amt + '^' + date + '^'
						+ '2700' + '|';
			} else {
				 // 2701 -- Discount
				str = str + purpose + '^' + desc + '^' + amt + '^' + date + '^'
						+ '2701' + '|';
			}
		}

		cnt++;
	}

	if (cnt == 0) {
		alert(message_accounts[156]);
		return false;
	}

	document.frmInvoice.hAction.value = "UPD";
	document.frmInvoice.hInputStr.value = str;

}

// When update that time should throw validation if enter any value in the
// payment details section.
function fnCheckMode() {
	var cnt = 0;
	var errmessage = '';
	var checkFlag = false;
	for ( var n = 0; n <= 5; n++) {
		val = n;
		obj = eval("document.frmInvoice.Cbo_Purpose" + val);
		purpose = obj.value;
		obj = eval("document.frmInvoice.Txt_Desc" + val);
		desc = obj.value;
		objAmt = eval("document.frmInvoice.Txt_Amt" + val);
		amt = RemoveComma(objAmt.value);
		objDate = eval("document.frmInvoice.Txt_Date" + val);
		date = objDate.value;
		//PC-4637-payment date validation
		var preDiff = dateDiff(strFromdaysDate,date, dateFmt);
		var futDiff = dateDiff(todaysDate,date, dateFmt);
		// Amount - number validation
		if (isNaN(amt)) {
			var msgVar = val + 1;// To show the value in Error details
			Error_Details(Error_Details_Trans(message_accounts[157],msgVar));
		}
		//PC-4637-payment date validation
	    if (paymentValidationFl =='Y' && (preDiff < 0 || futDiff > 0)) {
	    	var msgVar = val + 1;
	    	Error_Details(message_accounts[557]+" for row "+msgVar);
	    }

		if (desc != '' || date != '' || purpose != '0' || amt != '') {
			checkFlag = true;
		}
		if (checkFlag == true
				&& (desc == '' || date == '' || purpose == '0' || amt == '')) {
			var count = parseInt(n) + parseInt(1);
			errmessage = errmessage + Error_Details_Trans(message_accounts[159],count);
		}
		checkFlag = false;
	}

	if (errmessage != '') {
		errmessage = errmessage.substring(0, errmessage.length - 1);
		Error_Details(Error_Details_Trans(message_accounts[158],errmessage));
		document.frmInvoice.hAction.value = "PAY";
	}

}
// This function is used to update the Orders (POs) to batch
function fnGenerateBatch() {
	
	var invDate = document.frmInvoice.Txt_InvDate.value;
	var dateDifference = dateDiff(invDate, orderDate, dateFmt);
	var diff = dateDiff(todaysDate, invDate, dateFmt);
	// validate the choose action.
	fnValidateDropDn('Cbo_userBatch', message_accounts[204]);
	//Invoice date validation
	fnValidateTxtFld('Txt_InvDate',message_accounts[205]);
	CommonDateValidation(document.frmInvoice.Txt_InvDate, dateFmt,
			message_accounts[149] + message[611]);
	if(dateDifference>0){
		Error_Details(message_accounts[151]);
	}

	if (diff > 0) {
		Error_Details(message_accounts[150]);
	}
	// Tax alert
	if (taxCountryFl == 'Y' && validAddFl != 'Y' && taxDate == 'Y') {
		Error_Details(message[4002]);
	}
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	var addBatch = document.frmInvoice.hAddBatch.value;
	var accID = document.frmInvoice.hAccId.value;
	var custPO = document.frmInvoice.hPO.value;
	var batch = document.frmInvoice.hBatchID.value;
	var cboActionVal = document.frmInvoice.Cbo_userBatch.value;
	var hEmailFlVal = document.frmInvoice.hEmailFl.value;
	var addBatch = document.frmInvoice.hAddBatch.value;
	
	// to pass the window
	if (addBatch == 'Y') {
		fnStartProgress("Y");
		parent.window.location.href = fnAjaxURLAppendCmpInfo("/GmInvoiceServlet?hAction=ADD_BATCH&hEmailFl="
				+ hEmailFlVal
				+ "&hAccId="
				+ accID
				+ "&hPO="
				+ custPO
				+ "&hBatchID="
				+ batch
				+ "&Cbo_userBatch="
				+ cboActionVal
				+ "&hAddBatch="
				+ addBatch
				+ "&Txt_InvDate="
				+ invDate
				+ "&hRedirectURL=/gmProcessTrans.do?haction=Batch");
	} else {
		// form submit.
		fnStartProgress("Y");
		document.frmInvoice.hAction.value = 'ADD_BATCH';
		document.frmInvoice.submit();
	}

}
// this function used to submit the form based on the chosse action
function fnSubmit() {
	// validate the choose action.
	fnValidateDropDn('Cbo_userBatch', message_accounts[204]);
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	var cboActionVal = document.frmInvoice.Cbo_userBatch.value;
	
	if (cboActionVal == 18851) { // Generate invoice
		fnEmailOverride();
		fnGenerateInvoice('Gen');
	} else if (cboActionVal == 18852) { // Generate OBO
		fnGenerateInvoice('GenOBO');
	} else { // Batch options
		fnEmailOverride();
		fnGenerateBatch();
	}
}

function fnActionChange() {
	var cboActionObj = document.frmInvoice.Cbo_userBatch;
	var divEmailOverrideObj = document.getElementById("divEmailOverride")
	var cboActionVal = '';
	if (cboActionObj && divEmailOverrideObj) {
		cboActionVal = cboActionObj.value;
		if (cboActionVal == 18852 || cboActionVal == '0' || cboActionVal == '') {// Generate
																					// OBO
			divEmailOverrideObj.style.display = "none";
		} else {
			divEmailOverrideObj.style.display = "block";
		}
	}

	// to check the used lot flag then, call the AJAX and get the used lot details.
	var actionVal = document.frmInvoice.hAction.value;
	if(orderUsageLotFl =='Y' && orderDDTFl == 'Y' && (actionVal == 'IC' || actionVal == 'IDB')) { // Issue Debit , Issue Credit
		var invoice_id_Val = document.frmInvoice.hInv.value;
		// parameter -  order id, part number, invoice id
		fnFetchOrderUsageDtls('', '', invoice_id_Val);
	}
	
	
}

function fnEmailOverride() {
	var chkEmailObj = document.frmInvoice.chkEmailOverride;
	if (chkEmailObj) {
		if (chkEmailObj.checked) {
			document.frmInvoice.hEmailFl.value = "Y";
		} else {
			document.frmInvoice.hEmailFl.value = "N";
		}
	} else {
		document.frmInvoice.hEmailFl.value = "";
	}
}

//This function used to to print the invoice in selected format
function fnOpenInvoice(obj) {
	var varinv = document.frmInvoice.hInv.value;
	var varlayout = document.frmInvoice.Cbo_inv_layout.value;
	fnValidateDropDn('Cbo_inv_layout', message_accounts[206]);
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	windowOpener('/GmInvoiceInfoServlet?hAction=Print&hInv=' + varinv+'&invLayout='+varlayout,
			"PrntInv",
			"resizable=yes,scrollbars=yes,top=150,left=200,width=790,height=700");
}

var errorPartStr = '';

//this function used to validate the issue qty Vs used Lot qty.
function fnValidateUsedLot (){
	var validDataFl = true;
	//empty the input string values
	document.frmInvoice.hUsedLotStr.value = '';
	// reset the map values
	usedLotMap = new Map;
	returnsQtyMap = new Map;
	
	// to put all the parts to map (issue qty details)
	fnPutIssueQtyMap ();
	// to put all the parts to map (Used lot details)
	fnPutUsedLotQtyMap ();

	// compare the hash map qty
	validDataFl = fnCompareUsedLotQty ();
	
	if(validDataFl){
		// to form the input string
		fnCreateUsedLotInputStr ();
	}
	
	console.log ('Final string ' + document.getElementById('hUsedLotStr').value);

}

//this function used to put all the Returns qty.
function fnPutIssueQtyMap (){
	// declare local variable
	var mapIssue_val = 0;
	
	for ( var i = 0; i < cnt; i++) {
		objPartNum = eval("document.frmInvoice.hPNum" + i);
		objIssueQty = eval("document.frmInvoice.Txt_CreditQty" + i);
		//
		partNum = objPartNum.value;
		issueQtyVal = TRIM(objIssueQty.value);
		
		// adding the qty
		if(issueQtyVal !=''){
			// to get the values from hashmap
			mapIssue_val = returnsQtyMap.get (partNum);
			
			if(mapIssue_val == undefined){
				mapIssue_val = parseInt(0);
			}
			// to add the returns qty values.
			mapIssue_val = mapIssue_val + parseInt(issueQtyVal);
			
			// set the values to map
			returnsQtyMap.set(partNum, mapIssue_val);
		}
	}	
}