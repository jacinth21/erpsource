
function fnSubmit(){
	
	var frm ;
	frm = document.frmTagTransfer;
	fnValidate();

	frm.strOpt.value = "edit";	
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}else{
		if(confirm(message_accounts[85])){
		fnStartProgress('Y');	
		frm.submit();
		}
	}
    
}
function fncheckdate(){
	var missingDate=document.frmTagTransfer.missingDate;

	if(gCmpDateFmt=='MM/dd/yyyy')
		CommonDateValidation(missingDate, gCmpDateFmt,  message[1]);
	if(gCmpDateFmt=='dd/MM/yyyy')
		CommonDateValidation(missingDate, gCmpDateFmt,  message[5316]);		
	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}
}

function fnValidate()
{
	var frm = document.frmTagTransfer;
	var status = frm.status.value;
	var dist = frm.locationType.value;
	var setTypeVal = frm.setType.value;
	var tpnum =document.frmTagTransfer.pnum.value ;
	var tsetid =document.frmTagTransfer.setId.value ;
	var tcntl =document.frmTagTransfer.controlNum.value ;
	var trefid =document.frmTagTransfer.refId.value;
	var tloctype =document.frmTagTransfer.locationType.value;
	var tconto =document.frmTagTransfer.consignedTo.value ;
	var hstatusId = document.frmTagTransfer.hstatus.value ;
	var tcomments=document.frmTagTransfer.comments.value;
	var missingDate=document.frmTagTransfer.missingDate.value;
    var missingdistid=document.frmTagTransfer.missingdistid.value;
	var inventoryType=document.frmTagTransfer.inventoryType.value;
	var today = new Date();
	var missDate = new Date(missingDate);

		if(status == 26240614){ /*missing status*/
			if( inventoryType == 10001 ||  inventoryType == 10002 ||  inventoryType == 10004) {
				Error_Details(message_accounts[550]); /*'Only Consignment Sets can be marked as Missing'*/
			} else if (inventoryType == 10003 && SelectedLocId == '52099') {
				Error_Details(message_accounts[551]);  /*'Cannot mark a returned Tag as Missing */
			} else if (tsetid == '0' || tsetid == null){
				confirm(message_accounts[552]);
				//Error_Details(message_accounts[87]); /*"Tag is not associated to a Set, do you want to proceed?*/
			} if (missingDate=='' || missingDate == null){
				Error_Details(message_accounts[553]);
			}if(missDate>today){
				Error_Details(message_accounts[554]);
			}
		} else if(status ==51011){
		fnValidateTxtFld('tagId',lblTagId); 
		fnValidateTxtFld('status',lblStatus);
		
		if (tpnum != '' || tsetid != 0  || tcntl != '' || trefid != '' || tloctype != 0 || tconto != 0)
		{
			Error_Details(message_accounts[86]);
		}
		
	}else if(status ==51012){
		fnValidateTxtFld('tagId',lblTagId); 
		fnValidateTxtFld('pnum',lblPartNum);
		fnValidateDropDn('setId',lblSetId); 
		fnValidateTxtFld('controlNum',lblCtrlNum);
		if(dist != 4120){
			fnValidateTxtFld('refId',lblRefId);
		}
		fnValidateDropDn('locationType',lblLocType); 
		fnValidateDropDn('consignedTo',lblConsTo); 
	}
		// validate the Tag status
	if(hstatusId == 51013 && (status ==51011 || status ==51012)){
		Error_Details(message_accounts[87]);
	}else if(hstatusId == 51010 && (status ==51011 || status ==51012)){
		Error_Details(message_accounts[88]);
	}else if((hstatusId == 51011 || hstatusId == 51012) && status ==51010){
		Error_Details(message_accounts[89]);
	}else if((hstatusId == 51011 || hstatusId == 51012) && status ==51013){
		Error_Details(message_accounts[90]);
	}else if(tcomments==''){
	fnValidateTxtFld('comments',lblComments);
	}
}
function fnLoad()
{
	var frm = document.frmTagTransfer;
	document.frmTagTransfer.inventoryType.disabled = true;
	fnGetNames();
	fnChkStatus();
	if(voidfl !=''){
		document.frmTagTransfer.pnum.disabled = true;
		document.frmTagTransfer.setId.disabled = true;
		document.frmTagTransfer.controlNum.disabled = true;
		document.frmTagTransfer.refId.disabled = true;
		document.frmTagTransfer.status.disabled = true;
		document.frmTagTransfer.inventoryType.disabled = true;
		document.frmTagTransfer.locationType.disabled = true;
		document.frmTagTransfer.consignedTo.disabled = true;
	}
	document.frmTagTransfer.comments.value = "";
	document.frmTagTransfer.tagId.focus();

}
function fnEnter()
{
		if (event.keyCode == 13)
		{ 
			fnTagLoad();
		}
}
function fnTagLoad()
{
	var tagid = document.frmTagTransfer.tagId.value;
	if(tagid =='' || tagid.length < 7){
		Error_Details(message_accounts[91]);
	}
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}else{
		fnStartProgress('Y');
		document.frmTagTransfer.strOpt.value = "";	
		document.frmTagTransfer.submit();
	}
	
} 
function fnTagHistory()
{ 	
	var tagid = document.frmTagTransfer.tagId.value;
	windowOpener("/gmTagHistory.do?tagID="+tagid,"TagHistory","resizable=yes,scrollbars=yes,top=250,left=300,width=1050,height=400");
}
function fnGetNames()
{	

var val = document.frmTagTransfer.locationType.value;

//var values =document.frmTagTransfer.consignedTo.value;

if (val == 4120) //dist
{
	document.all.consignedTo.options.length = 0;
	document.all.consignedTo.options[0] = new Option("[Choose One]","0");
	for (var i=0;i<DistLen;i++)
	{
		arr = eval("DistArr"+i);
		arrobj = arr.split(",");
		id = arrobj[0];
		nameval = arrobj[1];
		document.all.consignedTo.options[i+1] = new Option(nameval,id);
		if ( id == SelectedLocId ) {
           document.all.consignedTo.options[i+1].selected = true;
		}
	}		
	//document.getElementById('addressid').disabled = true
}


if (val == 40033) //In-house
{
	document.all.consignedTo.options.length = 0;
	document.all.consignedTo.options[0] = new Option("[Choose One]","0");	
	for (var i=0;i<EmpLen;i++)
	{
		arr = eval("EmpArr"+i);
		arrobj = arr.split(",");
		id = arrobj[0];
		nameval = arrobj[1];
		document.all.consignedTo.options[i+1] = new Option(nameval,id);
		if ( id == SelectedLocId ) {
           document.all.consignedTo.options[i+1].selected = true;
		}
	}	
}
if(val == 4124 || val == 0 ){	
	document.all.consignedTo.options.length = 0;
	document.all.consignedTo.options[0] = new Option("[Choose One]","0");
}	
}

function fnVoidTag()
{
		var tagid = document.frmTagTransfer.tagId.value;
		var cancelType = 'VFTAG';
		if(voidfl !=''){
			cancelType = 'UVFTAG';
		}
		document.frmTagTransfer.hTxnId.value = tagid;
		document.frmTagTransfer.action ="/GmCommonCancelServlet";
		document.frmTagTransfer.hAction.value = "Load";
		document.frmTagTransfer.hRedirectURL.value =  "/gmTagTransfer.do?strOpt=fetch&tagId="+tagid;
		document.frmTagTransfer.hDisplayNm.value = "Edit Tag Screen";
		document.frmTagTransfer.hCancelType.value = cancelType;
		document.frmTagTransfer.submit();
}

function fnChkStatus(){
	var status = document.frmTagTransfer.status.value;
	var hstatus_id = document.frmTagTransfer.hstatus.value ;
	var missingdistid=document.frmTagTransfer.missingdistid.value;
	if(status != 51012 || voidfl !=''){
		document.frmTagTransfer.currLocation.disabled = true;
	}else{
		document.frmTagTransfer.currLocation.disabled = false;
	}
	if(status == 51010 || status == 51013 || voidfl !=''){
		document.frmTagTransfer.SubmitButton.disabled = true;
	}else{
		document.frmTagTransfer.SubmitButton.disabled = false;
	}
	if(status==26240343){
	       if(updateFl!='Y'){
	    	   document.frmTagTransfer.SubmitButton.disabled = true;
		}
	}
	var locType = document.frmTagTransfer.locationType.value;
	if(status == 26240614){	
		document.getElementById("miss").style.display = 'inline';	
		document.all.consignedTo.options.length = 0;	
		document.all.consignedTo.options[0] = new Option("Missing", missingdistid);
		document.frmTagTransfer.consignedTo.disabled = true;
		document.frmTagTransfer.locationType.disabled = true;
		 
	} else {
		document.getElementById("miss").style.display = 'none';
		document.all.consignedTo.value=SelectedLocId;
		fnGetNames();
		document.frmTagTransfer.consignedTo.disabled = false;
		document.frmTagTransfer.locationType.disabled = false;
	}	
	//to check the status and disable when status is missing
	if(hstatus_id == 26240614){
		document.frmTagTransfer.status.disabled = true;
	}
	// to check the access and disable the submit button.
	if (hstatus_id == 26240343){
		if(updateFl == 'Y'){
			document.frmTagTransfer.SubmitButton.disabled = false;
		}else {
			document.frmTagTransfer.SubmitButton.disabled = true;
		}
	}
}

function fnCurrLocation(){
	var tagid = document.frmTagTransfer.tagId.value;
	document.frmTagTransfer.strOpt.value = "load";	
	document.frmTagTransfer.action = '\gmTagSetInvMgt.do?method=tagSetLocation&strTagIds='+tagid+'&parentFrmName=frmTagTransfer';
	document.frmTagTransfer.submit();
}

