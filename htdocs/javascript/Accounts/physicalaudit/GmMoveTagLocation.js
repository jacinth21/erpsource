var gridObjData;
var RepArr = new Array(1);
var DistArr = new Array(1);

function doOnCellEdit(stage, rowId, cellInd, nValue, oValue) 
{	
	if(cellInd==5 || cellInd==6 || cellInd==7 || cellInd==8)
	{
	 	if(cellInd==5 && stage==2)
		{  
			var comboDistRep = gridObj.getCustomCombo(rowId,6);
	 		var adp = gridObj.cellById(rowId, 6).getValue();
	 	
			if(nValue =='50221' && oValue!= nValue)
			{	
				comboDistRep.clear();
				for (var j=0;j<distLen;j++ )
				{	
					arr = eval("DistArr"+j);
					arrobj = arr.split(",");
					id = arrobj[0]; 
					name = arrobj[1];
					if(arrobj[2])
					{	
						name = name + "," + arrobj[2];
					}
				 
					comboDistRep.put(id,name);
					}
					gridObj.cellById(rowId, 6).setValue(''); 
				}
				else if(nValue =='50222'&& oValue!= nValue)
				{	
					comboDistRep.clear(); 
					for (var j=0;j<RepLen;j++ )
					{	
						arr = eval("RepArr"+j);
						arrobj = arr.split(",");
						id = arrobj[0]; 
						name = arrobj[1];
						comboDistRep.put(id,name); 
					}
					gridObj.cellById(rowId, 6).setValue(''); 
				}
	        }	
	     
	     if (cellInd==6 && stage==0) 
		 {  

			if(gridObj.cells(rowId, 5).getValue() == ""){
				Error_Details(message_accounts[79]);
				Error_Show();
				Error_Clear();
				return false;
			}

			var comboDistRep = gridObj.getCombo(6);
		    gridObj.editor.combo = comboDistRep;
		    comboDistRep.save();
		  	var distRep = gridObj.cellById(rowId, 5).getValue();
			 
		   	if(distRep =='50222')
			{ 	 
				//gridObj.cellById(rowId, 5).setValue(''); 
				for (var j=0;j<distLen;j++ )
				{	
					arr = eval("DistArr"+j);
					arrobj = arr.split(",");
					id =  arrobj[0]; 
					name = arrobj[1]; 
			 		comboDistRep.remove(id);
				} 
			}
			else if(distRep =='50221')
			{	  
				// gridObj.cellById(rowId, 5).setValue('');  
				for (var j=0;j<RepLen;j++ )
				{	
					arr = eval("RepArr"+j);
					arrobj = arr.split(",");						 
					id =  arrobj[0]; 					 
					name = arrobj[1]; 
				 	comboDistRep.remove(id);
				} 
					 
			}
		}
	     else if ((cellInd == 6) && (stage == 2||stage == 1))
		{	   
			gridObj.getCombo(6).restore();
	  	}
	   
		return true;
		}
		
	else
	{
		return false;
	} 
}

function fnGo()
{
	if(document.frmTagReport.tagIdFrom.value != ""&&document.frmTagReport.tagIdTo.value == ""){
		Error_Details(message_accounts[80]);
		}	
	if(document.frmTagReport.tagIdFrom.value == ""&&document.frmTagReport.tagIdTo.value != ""){
		Error_Details(message_accounts[80]);
		}	 	
	if(document.frmTagReport.setID.value == 0 && document.frmTagReport.distributorId.value == 0 && document.frmTagReport.accId.value =='0' &&document.frmTagReport.tagIdFrom.value == '' &&document.frmTagReport.tagIdTo.value == ''){
		Error_Details(message_accounts[81]);
		}	 	

	if (ErrorCount > 0)  
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	 
	document.frmTagReport.strOpt.value  = "Report";
	fnStartProgress("Y");
	document.frmTagReport.submit();
}


function fnOnPageLoad()
{
	if(gridObjData!="")
	{
	   	gridObj = initGrid('dataGridDiv',gridObjData);
	   	gridObj.enableEditEvents(true, false, false);
	   	gridObj.attachEvent("onEditCell",doOnCellEdit);
	   	
	   	///For edit functionality
		gridObj.attachEvent("onRowSelect" ,function(rowId,cellIndex){
   			if (cellIndex==0){
    			fnTagHistory(rowId);
			}
		});
	
		/////
		gridObj.attachEvent("onMouseOver",function(id,ind)
		{
	 		if (ind==0){
				gridObj.cells(id,ind).setAttribute("title",message_accounts[82]);
				gridObj.setCellTextStyle(id,ind,"cursor:hand;"); }
			else
			{
				return false;
			}
				return true;
		});
	   	
	 }
}

function fnSubmit()
{
	gridObj.editStop();
	var rowIds = gridObj.getChangedRows();
	var idsArr = rowIds.split(",");
	var arrLen = idsArr.length;
	var strInputString = '';
	for (var i=0;i<arrLen;i++)
	{
		if((gridObj.cells(idsArr[i], 5).getValue() != "" && gridObj.cells(idsArr[i], 6).getValue() !="") || (gridObj.cells(idsArr[i], 5).getValue() == "" && gridObj.cells(idsArr[i], 6).getValue() =="")){

			if(gridObj.cells(idsArr[i], 5).getValue()==''){
				gridObj.cells(idsArr[i], 5).setValue(" ");
				//alert("**"+gridObj.cells(idsArr[i], 7).getValue()+"**");
			}
			if(gridObj.cells(idsArr[i], 6).getValue()==''){
				gridObj.cells(idsArr[i], 6).setValue(" ");
				//alert("**"+gridObj.cells(idsArr[i], 7).getValue()+"**");
			}
			if(gridObj.cells(idsArr[i], 7).getValue()==''){
				gridObj.cells(idsArr[i], 7).setValue(" ");
				//alert("**"+gridObj.cells(idsArr[i], 7).getValue()+"**");
			}

			strInputString = strInputString+idsArr[i]+'^'+  gridObj.cells(idsArr[i], 5).getValue()+'^'+  gridObj.cells(idsArr[i], 6).getValue()+'^'+  gridObj.cells(idsArr[i], 7).getValue()+'^'+  gridObj.cells(idsArr[i], 8).getValue()+'|';
		} else {
			Error_Details(message_accounts[83]);		
		}
	}

	if (ErrorCount > 0)  
	{
		Error_Clear();
		Error_Details(message_accounts[83]);		
		Error_Show();
		Error_Clear();
		return false;
	}

	document.frmTagReport.hinputString.value =  strInputString; 
	//document.frmConstructBuilder.hconstructName.value =  document.getElementById("conStaticName").value+' '+document.getElementById("conStaticName2").value; 
	document.frmTagReport.strOpt.value =  'save'; 
	document.frmTagReport.submit();
}

function fnTagHistory(tagid)
{ 	 
	windowOpener("/gmTagHistory.do?tagID="+tagid,"TagHistory","resizable=yes,scrollbars=yes,top=250,left=300,width=800,height=600");
} 

function fnEnter()
{
	if (event.keyCode == 13)
	{ 
		fnGo();
	}
}