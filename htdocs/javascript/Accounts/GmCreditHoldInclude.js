//fnValidateCreditHold - This function is used to validate Account is on credit hold or not through Ajax.
function fnValidateCreditHold(acctId, arrEl, frmName) {
	if(document.all.hIsCreditHold)
		document.all.hIsCreditHold.value = '';
	//disable buttons before call Ajax.
	fnToggleElements(arrEl, frmName, true);
	//Clear credit hold inline message div tag
	var divCreditHoldObj = document.getElementById("divCreditHold");
	divCreditHoldObj.innerHTML = "";
	divCreditHoldObj.style.display = "none";
	
	var additionParam = "&subType=" + encodeURIComponent(subType);
	dhtmlxAjax.get("/gmCreditHold.do?method=validateCreditHold" + additionParam + "&accountId=" + encodeURIComponent(acctId) + "&ramdomId=" + Math.random(), function(loader) {
		if (loader.xmlDoc.responseText != null) {
			var response = loader.xmlDoc.responseText;
			var holdFl = "";
			var creditHoldMsg = "";
			var accessState = false;
			var submitAccessFl = eval("document." + frmName + ".hSubmitAcc");    // Allow security event to Submit Button in PMT-33781
			
			if(submitAccessFl != undefined ){         
			    if(submitAccessFl.value == 'true'){
			    	accessState=true;
			    }
			}
			if (TRIM(response) != "") {
				var arrRes = response.split("|");
				holdFl = arrRes[0];
				creditHoldMsg = arrRes[1];
				if(document.all.hIsCreditHold)
					document.all.hIsCreditHold.value = holdFl;
				
				if (alertFl == "Y") {
					//if alert msg required then show credit hold message as popup message.
					Error_Details(creditHoldMsg);
					Error_Show();
					Error_Clear();
				} else {
					//if inline msg required then show credit hold message in div tag.
					divCreditHoldObj.innerHTML = creditHoldMsg;
					divCreditHoldObj.style.display = "block";
				}
			}
			if (holdFl != "80131") { // 80131 - No
				//Enable buttons if account is not set as credit hold function allowed is 'No'.
				fnToggleElements(arrEl, frmName, accessState);
			}
		}
	});
}

//fnToggleElements - This function is used to disable given buttons in  the arrEl array.
function fnToggleElements(arrEl, frmName, state) {
	if(arrEl != null){
		for ( i = 0; i < arrEl.length; i++) {
			var objEl = eval("document." + frmName + "." + arrEl[i]);
			if(objEl != undefined){
				objEl.disabled = state;
			}
		}
	}
}

