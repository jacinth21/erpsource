function upload()
  {
    if(document.frmInvoiceUpload.file.value == "")
    { 
    	Error_Details(message_accounts[162]); 
    	Error_Show();
		Error_Clear();
		return false;
    }
    else{
		document.frmInvoiceUpload.UploadBtn.disabled = true;
		document.frmInvoiceUpload.action = "/gmInvoiceUpload.do?";
	    document.frmInvoiceUpload.strOpt.value="upload";
	    document.frmInvoiceUpload.submit();    
    }
    toggledisplay();
  }
 
function fnOpenForm(Id)
{
	document.frmInvoiceUpload.action = "/GmCommonFileOpenServlet?sId="+Id+"&uploadString=UPLOAD_INVOICE&appendPath=";
	document.frmInvoiceUpload.submit();

}
function toggledisplay()
{
  var objDivStyle = eval('document.all.pgupload.style');
  objDivStyle.display = 'none';
  
  var objDivStyle1 = eval('document.all.pgtitle.style');
  objDivStyle1.display = 'block';
  
  var objDivStyle2 = eval('document.all.pgbar.style');
  objDivStyle2.display = 'block';
}