
function fnOnPageLoad()
{	
	
	if (objGridData != '')
	{
		
		var footer = new Array();
		footer[0]="";
		footer[1]="";
		footer[2]="";
		if(arRptByDealerFlag == 'YES'){
			footer[3]="";
			footer[4]=message_accounts[180];
			footer[5]="<b>{#stat_total}<b>";
			footer[6]="<b>{#stat_total}<b>";
			gridObj = initGrid('vatSummaryData',objGridData,'',footer);
			gridObj.groupBy(1,["#title","","","","","#stat_total","#stat_total"]);
			gridObj.setColumnHidden(1,true);
			gridObj.attachHeader('#rspan,#rspan,#select_filter,#select_filter,#select_filter,#text_filter,#text_filter');
			gridObj.enableTooltips("false,false,false,false,false,false,false,false");
		}
		else
			{
			footer[3]=message_accounts[180];
			footer[4]="<b>{#stat_total}<b>";
			footer[5]="<b>{#stat_total}<b>";
			gridObj = initGrid('vatSummaryData',objGridData,'',footer);
			gridObj.groupBy(1,["#title","","","","#stat_total","#stat_total"]);
			gridObj.setColumnHidden(1,true);
			gridObj.attachHeader('#rspan,#rspan,#select_filter,#select_filter,#text_filter,#text_filter');
			gridObj.enableTooltips("false,false,false,false,false,false,false");
			}
		
		gridObj.enableBlockSelection(true); 
			
		
	}
}

function fnLoad(){	
		
	objStartDt = document.frmVATSummary.fromDate;
	objEndDt = document.frmVATSummary.toDate;
	format= document.frmVATSummary.format.value;
		
	fnValidateTxtFld('fromDate',message_accounts[257]);
	fnValidateTxtFld('toDate',message_accounts[258]);
	
	if(objStartDt.value != ""){
		CommonDateValidation(objStartDt,format,message_accounts[194]+ message[611]);
		
	}
	if(objEndDt.value != ""){
		CommonDateValidation(objEndDt,format,message_accounts[195]+ message[611]);
		
	}
	
	if (ErrorCount > 0)
		{
			Error_Show();
			Error_Clear();
			return false;
		}	
	document.frmVATSummary.strOpt.value = "Load"
	fnStartProgress();
	frmVATSummary.submit();
	
}

function fnExport(str){
	gridObj.detachHeader(1);
	gridObj.unGroup();
	gridObj.setColumnHidden(1,false);
	gridObj.setColumnHidden(0,true);
	
	if(str=="excel"){
		gridObj.toExcel('/phpapp/excel/generate.php');
	}
	if(arRptByDealerFlag == 'YES'){
	gridObj.attachHeader('#rspan,#rspan,#select_filter,#select_filter,#select_filter,#text_filter,#text_filter');
	}
	else{
	gridObj.attachHeader('#rspan,#rspan,#select_filter,#select_filter,#text_filter,#text_filter');
	}
	gridObj.groupBy(1);
	gridObj.setColumnHidden(1,true);
	gridObj.setColumnHidden(0,false);
}

