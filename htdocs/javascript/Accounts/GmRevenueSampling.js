
function fnOnPageLoad() { 
	 var acct = document.frmRevenueSampling.accountID.value;
	if(acct!=0){
	 document.frmRevenueSampling.Txt_AccId.value = acct;
	}
	if(gridObjData!='')
	{
		//mygrid = initGrid('dataGridDiv',gridObjData);  
	
		var footer=new Array();
		footer[0]=message_accounts[180];
		footer[1]="";
		footer[2]="";
		footer[3]="";
		footer[4]="";
		footer[5]="";
		footer[6]="";
		footer[7]="";
		footer[8]="";
		footer[9]="";
		footer[10]="";
		footer[11]="";
		footer[12]="";
		footer[13]="";
		footer[14]="";
		footer[15]="";
		footer[16]="";
		footer[17]="";
		if(arRptByDealerFlag == 'YES'){
			footer[18]="";
			footer[19]="";
			footer[20]="<b>{#stat_total}<b>";
			footer[21]="";
			footer[22]="";
			footer[23]="";
		}else{
			footer[18]="<b>{#stat_total}<b>";
			footer[19]="";
			footer[20]="";
			footer[21]="";
		}
		
		 
		gridObj = initGridWithDistributedParsing('dataGridDiv',gridObjData,footer);
		if(arRptByDealerFlag == 'YES'){
        gridObj.attachHeader('#text_filter,#text_filter,#select_filter,#rspan,#rspan,#text_filter,#text_filter,#select_filter,#select_filter,#select_filter,#text_filter,#select_filter,#text_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#numeric_filter,#numeric_filter,#select_filter,#numeric_filter,#select_filter');
}
		else
			{
			gridObj.attachHeader('#text_filter,#rspan,#rspan,#text_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#text_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#numeric_filter,#numeric_filter,#select_filter,#numeric_filter,#select_filter');
			}
		
		gridObj.enablePaging(true,100,5,"pagingArea",true);
		gridObj.setPagingSkin("bricks");
		gridObj.enableHeaderMenu();
		
	
	}
}
 

function fnGo()
{	
	var  acct = document.frmRevenueSampling.accountID.value;
	var  rep = document.frmRevenueSampling.repID.value;
	var  region = document.frmRevenueSampling.regionID.value;
	var  status = document.frmRevenueSampling.status.value;
	var  ordFromdt = document.frmRevenueSampling.dtOrderFromDT;
	var  ordTodt = document.frmRevenueSampling.dtOrderToDT;
	var  updateFromdt = document.frmRevenueSampling.dtStatusFromDT;
	var  updateTodt = document.frmRevenueSampling.dtStatusToDT;
	var currentDate   = document.frmRevenueSampling.currentdate;
	//var parentfl  = document.frmRevenueSampling.Chk_ActiveFl.checked;

	if(acct=='0' && rep =='0' && region =='0' && status == '0' && ordFromdt.value=='' && ordTodt.value =='' && updateFromdt.value =='' && updateTodt.value =='')
	{
		Error_Details(message_accounts[181]);
	} 
	if(ordFromdt.value == '' && ordTodt.value != '')
	{
		Error_Details(message_accounts[182]);
	}
	if(ordFromdt.value != '' && ordTodt.value == '')
	{
		Error_Details(message_accounts[183]);
	}
	if(updateFromdt.value == '' && updateTodt.value != '')
	{
		Error_Details(message_accounts[184]);
	} 
	if(updateFromdt.value != '' && updateTodt.value == '')
	{
		Error_Details(message_accounts[185]);
	}
	if(ordFromdt.value != ""){
		CommonDateValidation(ordFromdt,format,Error_Details_Trans(message_accounts[186],format));
	}
	if(ordTodt.value != ""){		
		CommonDateValidation(ordTodt,format,Error_Details_Trans(message_accounts[187],format));
	}
	if(updateFromdt.value != ""){
		CommonDateValidation(updateFromdt,format,Error_Details_Trans(message_accounts[188],format));
	}
	if(updateTodt.value != ""){		
		CommonDateValidation(updateTodt,format,Error_Details_Trans(message_accounts[189],format));
	}
	if(dateDiff(ordTodt.value,ordFromdt.value,format)>0){
		Error_Details(message_accounts[190]);
	}
	if(dateDiff(updateTodt.value,updateFromdt.value,format)>0){
		Error_Details(message_accounts[191]);
	}
	if (ErrorCount > 0)
		 {
			Error_Show();
			Error_Clear();
			return false;
		} 
	fnStartProgress('Y');
	document.frmRevenueSampling.strOpt.value = "Reload";
	fnReloadSales(); // Call the Action in the Filters jsp
	//document.frmRevenueSampling.submit();
}
  

function fnCallPO(val)
{
	document.frmRevenueSampling.hOrdId.value = val;
	document.frmRevenueSampling.hAction.value = "EditPO";
	document.frmRevenueSampling.action = "/GmOrderItemServlet";
	document.frmRevenueSampling.submit();
}

//Order Summary Screen icon is clicked
function fnViewOrder(val)
{ 
	windowOpener('/GmEditOrderServlet?hMode=PrintPrice&hOrdId='+val,"Pack","resizable=yes,scrollbars=yes,top=250,left=250,width=810,height=450");	
}
 

function fnSendMail()
{	  
	var emailstr = '';
	var ordstr = '';
 	var len = document.frmRevenueSampling.hsize.value;  
	var ord = ''; 

	if (len>0)
	{ 
		for (i=1;i<= len;i++)
		{		 
			frmObj = eval("document.frmRevenueSampling.ChkOrdId"+i);
			
			if(frmObj != null)	{	
				
				if (frmObj.checked == true)
				{
					ord = frmObj.value; 
					emailstr = emailstr + ord + '^'; 
				}
			}
		}
		document.frmRevenueSampling.hinputStr.value = emailstr.substr(0,emailstr.length-1);
	} 
	if(emailstr ==''){
		Error_Details(message_accounts[192]);
	}

	if (ErrorCount > 0)
	 {		
		Error_Show();
		Error_Clear();
		return false;
	 }
	document.frmRevenueSampling.strOpt.value = "Reload";
	document.frmRevenueSampling.haction.value = 'SendEmail';
	fnStartProgress('Y');
	document.frmRevenueSampling.submit();
}

function fnViewFile(val,type)
{
	var filename = '';
	if (val == 1)
	{
		filename = 'RevenueSamplingReport.wmv';
	}
	if (type == 'V')
	{
		windowOpener("html/movies/"+filename,"html","resizable=yes,scrollbars=yes,top=150,left=200,width=750,height=600");
	}
	
}

function fnExport()
{	
	if(arRptByDealerFlag == 'YES'){
		gridObj.setColumnHidden(3,true);
		gridObj.setColumnHidden(4,true);
	    }
	else
		{
		gridObj.setColumnHidden(1,true);
		gridObj.setColumnHidden(2,true);
		}
	gridObj.toExcel('/phpapp/excel/generate.php');
	
	if(arRptByDealerFlag == 'YES'){
		gridObj.setColumnHidden(3,false);
		gridObj.setColumnHidden(4,false);
	    }
	else
		{
		gridObj.setColumnHidden(1,false);
		gridObj.setColumnHidden(2,false);
		}

}
//function is used ,When changing the company the id will come in text box
function fnDisplayAccID(obj){
	var accId = document.frmRevenueSampling.accountID.value;
	if(accId != ''){
		document.frmRevenueSampling.Txt_AccId.value = accId;
		fnSetAccCurrSym(accId,document.frmRevenueSampling.strCompCurrency);
	}	else{
		fnResetAccCurrSymb();
		document.frmRevenueSampling.Txt_AccId.value='';
	}
}
//function is used ,when account number is give to the text box automaticallu account drop down will change.
function fnDisplayAccNm(obj){
	var accId = TRIM(obj.value);
	if( accId !=''){
		
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/GmOrderAjaxServlet?opt=GETACC&acc=' + + encodeURIComponent(accId) + '&ramdomId=' + Math.random());
		dhtmlxAjax.get(ajaxUrl, fnLoadAccAjax);
		
		if (accId != '0' && accId != null){
			fnSetAccCurrSym(accId,document.frmRevenueSampling.strCompCurrency);	
		}
		
	}else{
		document.frmRevenueSampling.searchaccountID.value = '';
		fnResetAccCurrSymb();
	}
	
	if(accId==""){
		Error_Details(message_accounts[193]);
	}
	if (ErrorCount > 0)
	 {		
		Error_Show();
		Error_Clear();
		return false;
	 }
}
function fnResetAccCurrSymb(){
	var accCurrObj = document.frmRevenueSampling.strCompCurrency;
	for ( var i = 0; i < accCurrObj.options.length; i++ ) {
        if ( accCurrObj.options[i].id == compCurrSign ) {
        	accCurrObj.options[i].selected = true;
            return;
        }
    }
}

function fnSetAccCurrSym(accId,targetObj){
	   var dhxAccCurrUrl = fnAjaxURLAppendCmpInfo('/GmOrderAjaxServlet?opt=GETACCTCURR&acc='+accId+'&ramdomId=' + Math.random());
    dhtmlxAjax.get(dhxAccCurrUrl,function(loader){
    	   var accCurrDtl;
    	   var response = loader.xmlDoc.responseText;    
              if((response != null || response != '') ){
            	  accCurrDtl =response.split("|");
            	//  currencyFmt = accCurrDtl[1];
            	  if(targetObj != undefined){
            		  targetObj.value =accCurrDtl[0];
            	  }
              }
       });
}



//Description : Load the account name information
function fnLoadAccAjax(loader) {
	var errFlag = false;
	var xmlDoc;
	var pnum;
	var acname = '';
	
	var response = loader.xmlDoc.responseXML;
	if (response != null) {
		pnum = response.getElementsByTagName("acc");
		for (var x = 0; x < pnum.length; x++) {
			acname = parseXmlNode(pnum[x].childNodes[9].firstChild);
		}
		if (acname != null && acname != '') {
			document.frmRevenueSampling.searchaccountID.value = acname;
			document.frmRevenueSampling.accountID.value = document.frmRevenueSampling.Txt_AccId.value;
		}
	}
	
}

function fnOpenDOFile(strOrderId){
	
	if(strOrderId !=''){
		  w4 =  createDhtmlxWindow(1000,400,true);
		  w4.setText("Upload DO Document Information");
		  var ajaxUrl = fnAjaxURLAppendCmpInfo("/gmDODocumentUpload.do?method=loadOrderUpload&strOpt=Load&haction=View&uploadTypeId=26240412&strOrderId="+strOrderId);
		  w4.attachURL(ajaxUrl,"Upload Files");	  
		  return false;	
}
}

