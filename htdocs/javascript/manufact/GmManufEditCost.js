function fnOnPageLoad(){
	var pdesc = document.frmManufCost.partDes.value;
	document.getElementById("partDesc").innerHTML= pdesc;
}
function fnLoad() {
	var part = document.frmManufCost.pnum.value;
	if(part ==''){
		Error_Details(message[201]); //Please Enter a Part Number to Proceed
	}
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	document.frmManufCost.strOpt.value = "Load";
	document.frmManufCost.action = "/gmManufCost.do?method=editManufCost";
	fnStartProgress('Y');
	document.frmManufCost.submit();
}

function fnReset() {
	document.frmManufCost.pnum.value = '';
	document.getElementById("partDesc").innerHTML="";
	document.frmManufCost.partDes.value = '';
	document.frmManufCost.cost.value = '';
}

function fnPartCostHistory() {
	var part = document.frmManufCost.pnum.value;
	if(part !=''){
		windowOpener(
				"/GmPageControllerServlet?strPgToLoad=/gmAuditTrail.do&auditId=1045&txnId="
						+ part, "",
				"resizable=yes,scrollbars=yes,top=250,left=300,width=785,height=300,status=1");
	}
	
}

function fnSubmit() {
	fnValidateTxtFld('pnum','Part #');
	
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	document.frmManufCost.strOpt.value = "Save";
	document.frmManufCost.action = "/gmManufCost.do?method=editManufCost";
	fnStartProgress('Y');
	document.frmManufCost.submit();
}

function fnEnter() {
	if (event.keyCode == 13) {
		fnSubmit();
	}
}

function isNumberKey(evt)
{
   var charCode = (evt.which) ? evt.which : event.keyCode;
   if (charCode > 31 && (charCode < 46 || charCode > 57 ))
      return false;

   return true;
}
