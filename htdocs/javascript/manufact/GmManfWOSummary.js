function fnOnPageLoad() {
	if (objGridData != '') {
		gridObj = initGridData('manfWOSummaryRpt', objGridData);
		gridObj.enableTooltips("false,false,false,false,false,false,false,false,false,false");
	}
}

// This function used to create the DHTMLX object and load the XML string
function initGridData(divRef, gridData) {

	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/extweb/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);
	gObj.enableTooltips("true,true,true,true,true");
	gObj.attachHeader(',#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#select_filter,#text_filter,#select_filter,#select_filter');
	gObj.init();
	gObj.loadXMLString(gridData);
	return gObj;
}
//To download excel;
function fnExcel(){ 
	gridObj.toExcel('/phpapp/excel/generate.php');
}


function fnLoadReport(){	
	fnStartProgress('Y');
	document.frmMfg.submit();
}

function fnReset(){
	document.frmMfg.hAction.value = "Load";
	document.frmMfg.hOpt.value = "LoadWOSum";
	document.frmMfg.submit();
}

function fnSetValues(){	
	document.frmMfg.Cbo_WOStat.value = WOStat;
	document.frmMfg.Cbo_DHRStat.value = Cbo_DHRStat;
	document.frmMfg.Cbo_TransStat.value = Cbo_TransStat;
	document.frmMfg.Cbo_TransType.value = Cbo_TransType;
	document.frmMfg.Cbo_WOType.value = Cbo_WOType;
}
function fnPrintBOM(dhrID){
	windowOpener("/GmMFGInitiateWOServlet?hAction=PrintBOM&hOpt=MATREQDET&hDHRId="+dhrID,"DHR","resizable=yes,scrollbars=yes,top=100,left=200,width=1200,height=610");
}

