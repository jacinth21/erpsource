var hPartInputStr = "";
var hInputStr = "";
function fnBulkUpdate() {

	fnValidateCost();
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}

	else {
		var batch = document.frmManufCost.batchData.value;
		document.frmManufCost.hPartInputStr.value = hPartInputStr;
		document.frmManufCost.hInputStr.value = hInputStr;
		document.frmManufCost.strOpt.value = "save";

		if (confirm("Are you sure you want to submit the Cost?")) {
			fnStartProgress();
			document.frmManufCost.submit();
			document.frmManufCost.Btn_Submit.disabled = true;
		}
	}

}
// Reset Button
function fnReset() {
	document.frmManufCost.batchData.value = '';
}

function fnValidateCost() {
	hInputStr = '';
	hPartInputStr = '';
	var str = '';
	var obj = document.frmManufCost.batchData.value;
	var objlen = obj.length;
	var strOpt = "";
	var objval = TRIM(document.frmManufCost.batchData.value);
	document.frmManufCost.batchData.value = objval;
	var objlen = objval.length;
	var strTemp = objval;
	strTemp = strTemp.replace(/(\r\n|\n|\r)/gm, "|");
	// Get each line and validate
	var arrLines = strTemp.split('|');
	objval = objval.replace(/(\r\n|\n|\r)/gm, ',');
	var part_id = '';
	var set_Cost = '';
	var err_Costs = '';
	var err_string = '';
	var dupPartids = '';
	var empty_cost = '';
	objval = objval + ',';
	if (objlen == 0) {
		Error_Details("Please Enter Batch Upload Data");
	}
	// The Following code helps to validate the entererd Batch Data Format, for
	// example if user given 124.000.30 means
	// sholud through the validation as incorrect string format.The standard
	// format is PARTID,COST,but this code will helps us if the user enter
	// PARTID.COST formation.

	for (var i = 0; i < arrLines.length; i++) {

		if (arrLines[i].indexOf(",") == -1) {
			err_string = err_string + "<br>" + arrLines[i] + "";
		}

		part_id = objval.substring(0, objval.indexOf(','));
		objval = objval.substring((part_id.length) + 1, objval.length);
		set_Cost = objval.substring(0, objval.indexOf(','));
		objval = objval.substring((set_Cost.length) + 1, objval.length);
		part_id = TRIM(part_id);
		set_Cost = TRIM(set_Cost);
		str = part_id;
		part_id = str.toUpperCase();
		// forming the hPartInputStr,hInputStr Strings....
		if (part_id != '') {

			if (hPartInputStr.indexOf(part_id + ",") == -1) {
				hPartInputStr += (part_id + ",");
				
				if (set_Cost == '') {
					empty_cost = empty_cost + "<br>" + part_id;
				} else {
					var validation = NumberValidation(set_Cost, '', 2);
					
					if (validation == false || set_Cost < 0) {
						err_Costs = err_Costs + "<br>" + part_id + " - "
								+ set_Cost + "";
					}
				}
				hInputStr = hInputStr + part_id + '^' + set_Cost + '|';

			} else {
				if (dupPartids.indexOf(part_id + ",") == -1) {
					dupPartids += (part_id + ",");

				}
			}
		}
	}
	if (objlen != '') {
		// Error message for Incorrect data entry..
		if (err_string != '') {
			Error_Details(Error_Details_Trans(
					"The format is incorrect for the following part(s). Please correct it to be Part# , Cost[0]",
					err_string));
		}
		if (err_string == '') {
			// Eror message for Empty Cost for a part..
			if (empty_cost != '') {
				Error_Details(Error_Details_Trans(
						"Cost Should Not Be Empty For -[0]", empty_cost));
			}
		}
		// Error message for invalid Part Cost..
		if (err_Costs != '') {
			Error_Details(Error_Details_Trans(
					"Please enter valid and Positive numbers in Qty for Part# -[0]", err_Costs));
		}

		// Error message for Duplicate Part Entries..
		if (dupPartids != '' && TRIM(dupPartids).length > 1) {
			dupPartids = dupPartids.substr(0, (TRIM(dupPartids).length - 1));
			error_msg = Error_Details_Trans(
					"Duplicate Part Id Entrie(s) Found - [0]", dupPartids);
			Error_Details(error_msg);
		}
	}
}