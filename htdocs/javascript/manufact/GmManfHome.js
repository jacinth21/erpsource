// this function call - Pending QC Inspect - click the Y hyper link 
function fnUpdateDHR(id, val) {
	/*
	 * document.frmAccount.hId.value = id; document.frmAccount.hMode.value =
	 * val; document.frmAccount.hAction.value = "UpdateDHR";
	 * document.frmAccount.hPortal.value = "Manf"; document.frmAccount.submit();
	 */
	urlStr = "?hId=" + id + "&hMode=" + val + "&hPortal=Manf&hAction=UpdateDHR";
	urlStr = document.frmAccount.action + urlStr;
	fnLocationHref(urlStr);
}
// this function call - Pending QC Accept - click the Y hyper link
function fnUpdateSupplyReceipts(id, val) {
	/*
	 * document.frmAccount.hId.value = id; document.frmAccount.hMode.value =
	 * val; document.frmAccount.hAction.value = "UpdateDHR";
	 * document.frmAccount.hPortal.value = "Supply";
	 * document.frmAccount.submit();
	 */
	urlStr = "?hId=" + id + "&hMode=" + val
			+ "&hPortal=Supply&hAction=UpdateDHR";
	urlStr = document.frmAccount.action + urlStr;
	fnLocationHref(urlStr);
}
// This function used to call all the part # drill down
function Toggle(val) {

	var obj = document.getElementById("div" + val);
	var trobj = document.getElementById("tr" + val);
	var tabobj = document.getElementById("tab" + val);

	if (obj.style.display == 'none') {
		obj.style.display = '';
	} else {
		obj.style.display = 'none';
	}
}
// this function used to Pending control (Consignment items section)
function fnCallEditItems(val, type, conType) {
	var parentLcn;
	var urlStr;
	var targetUrlStr;
	document.frmAccount.hId.value = val;
	document.frmAccount.hMode.value = "CONTROL";
	document.frmAccount.hFrom.value = "ItemDashboard";
	document.frmAccount.hAction.value = "EditControl";
	document.frmAccount.hConsignId.value = val;

	document.frmAccount.hType.value = type;
	urlStr = "&hId="
			+ val
			+ "&hMode=CONTROL&hFrom=ItemDashboard&hAction=EditControl&hConsignId="
			+ val + "&hType=" + type;

	if (conType == 'I') {
		document.frmAccount.action = "GmSetAddRemPartsServlet?";

	} else if (conType == 'T') {
		urlStr = "&txnid=" + val
				+ "&mode=CONTROL&haction=EditControl&hConsignId=" + val
				+ "&txntype=" + type + "&loctype=93343";
		document.frmAccount.action = "/gmItemControl.do?";
	} else {
		document.frmAccount.action = "GmConsignItemServlet";
		document.frmAccount.submit();
		return false;
	}
	targetUrlStr = document.frmAccount.action + urlStr;
	parentLcn = parent.location.href;
	if (parentLcn.indexOf("GmEnterprisePortal") != -1) {
		fnLocationHref(targetUrlStr, null);
	} 
	
	else if(parentLcn.indexOf("GmGlobusOnePortal")!= -1)
	{
		fnLocationHref(targetUrlStr, null);
	}
	
	else {
		fnLocationHref(targetUrlStr, parent);
	}

}
// this function used to Pending Shipping Verification (Consignment items
// section)
function fnCallItemsShip(val, type, conType) {
	document.frmAccount.hId.value = val;
	document.frmAccount.hFrom.value = "LoadItemShip";
	document.frmAccount.hConsignId.value = val;
	document.frmAccount.hAction.value = "LoadItemShip";
	document.frmAccount.hMode.value = "VERIFY";
	document.frmAccount.hType.value = type;
	document.frmAccount.hOpt.value = type;

	if (conType == 'T') {
		urlStr = "&txnid=" + val
				+ "&mode=VERIFY&haction=EditVerify&hConsignId=" + val
				+ "&txntype=" + type + "&loctype=93345";
		document.frmAccount.action = "/gmItemVerify.do?";
	} else {
		document.frmAccount.action = "GmReprocessMaterialServlet";
		document.frmAccount.submit();
		return false;
	}

	targetUrlStr = document.frmAccount.action + urlStr;
	parentLcn = parent.location.href;

	if (parentLcn.indexOf("GmEnterprisePortal") != -1) 
	{
		fnLocationHref(targetUrlStr);
	} 
	
	else if(parentLcn.indexOf("GmGlobusOnePortal")!= -1) 
	{
	   fnLocationHref(targetUrlStr);
	} 
	
	else
	 {
		fnLocationHref(targetUrlStr, parent);
	 }

}
// this function used to open the pick slip details (Consignment items section)
function fnPicSlip(val, type) {
	if (type == "40051" || type == "40052" || type == "40053"
			|| type == "40054" || type == "100069" || type == "100075"
			|| type == "100076" || type == "56025" || type == "56045") {
		windowOpener("/GmConsignInHouseServlet?hAction=PicSlip&hConsignId="
				+ val, "SetPic",
				"resizable=yes,scrollbars=yes,top=150,left=150,width=750,height=500");
	}
}