/**
 * 
 */

function fnLoadLotCode() {
	document.frmMfg.hAction.value = 'Reload';
	fnStartProgress('Y');
	document.frmMfg.submit();
}

function fnReconcile() {
	Error_Clear();
	var obj = document.frmMfg;
	hcnt = parseInt(obj.hCnt.value);

	var strErrorMore = '';
	var pnum = '';
	var cnum = '';
	var relqty = 0;
	var retqty = 0;
	var scrqty = 0;
	var tobj = '';
	var tsum = 0;
	var answer = true;

	
	qtymanf = document.frmMfg.Txt_QtyComp.value;

	var returnstr = '';
	var scrapstr = '';
	hcnt = hcnt + 1;
	for (var i = 0; i < hcnt; i++) {
		tobj = eval("document.frmMfg.hPartNum" + i);
		pnum = tobj.value;
		tobj = eval("document.frmMfg.hCnum" + i);
		cnum = tobj.value;
		tobj = eval("document.frmMfg.hQty" + i);
		relqty = parseInt(tobj.value);
		tobj = eval("document.frmMfg.Txt_Ret" + i);
		retqty = parseInt(tobj.value);
		tobj = eval("document.frmMfg.Txt_Scr" + i);
		scrqty = parseInt(tobj.value);
		tsum = retqty + scrqty;
		if (tsum > relqty) {
			strErrorMore = strErrorMore + "," + pnum + ":" + cnum;
			;
		}
		tsum = 0;
	}

	if (strErrorMore != '') {
		Error_Details("Qty reconciled cannot be more than Qty Released for the following Parts: <b>"
				+ strErrorMore + "</b>");
	}

	if (document.frmMfg.Txt_QtyComp.value == '') {
		Error_Details("Please enter valid data for Qty Manufactured");
	}

	if (qtymanf > qtyplanned) {
		answer = confirm("Qty Manufactured (" + qtymanf
				+ ") is greater than Qty planned (" + qtyplanned
				+ "), do you want to proceed?");

	}

	if (ErrorCount > 0) {
		Error_Show();
		return false;
	} else {
		if (answer) {
			for (var i = 0; i < hcnt; i++) {
				tobj = eval("document.frmMfg.hPartNum" + i);
				pnum = tobj.value;
				tobj = eval("document.frmMfg.hCnum" + i);
				cnum = tobj.value;
				tobj = eval("document.frmMfg.Txt_Ret" + i);
				retqty = tobj.value;
				tobj = eval("document.frmMfg.Txt_Scr" + i);
				scrqty = tobj.value;
				if (retqty != '') {
					str = pnum + '^' + retqty + '^' + cnum + '|';
					returnstr = returnstr + str;
					str = '';
				}
				if (scrqty != '') {
					str = pnum + '^' + scrqty + '^' + cnum + '|';
					scrapstr = scrapstr + str;
					str = '';
				}
			}
			// start progress
			fnStartProgress('Y');
			
			document.frmMfg.hReturnStr.value = returnstr;
			document.frmMfg.hScrapStr.value = scrapstr;
			document.frmMfg.hAction.value = 'Reconcile';
			// alert("RETURN:"+document.frmMfg.hReturnStr.value);
			// alert("SCRAP:"+document.frmMfg.hScrapStr.value);
			document.frmMfg.submit();
		}
	}

}

function fnVoidDHR()
{
		document.frmMfg.hTxnId.value = document.frmMfg.hMfgWODHRId.value; 
		document.frmMfg.action ="/GmCommonCancelServlet";
		document.frmMfg.hAction.value = 'Load';		
		document.frmMfg.hCancelType.value = 'VODHR'
		document.frmMfg.submit();
}