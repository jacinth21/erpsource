function fnInitate()
{
	document.frmMfg.hAction.value = 'InitiateWO';
  	document.frmMfg.submit();
}

function fnAddPart()
{
	document.frmMfg.hAction.value = 'AddPart';
	fnStartProgress('Y');
  	document.frmMfg.submit();
}

function fnLoadLotCode()
{
	document.frmMfg.hAction.value = 'Reload';
	fnStartProgress('Y');
  	document.frmMfg.submit();
}

function fnCallReqId(val)
{
	document.frmMfg.hMatReqId.value = val;
	document.frmMfg.hAction.value = 'Reload';
//	document.frmMfg.hOpt.value = 'REQNEW';
  	document.frmMfg.submit();
}

var cnt = -1;

function fnAddRow(id)
{
	var objAddPartCnt = '';
	var addPartCnt = '';
	var counter = '';
		
	objAddPartCnt = document.frmMfg.hAddPartCnt;
	addPartCnt =  objAddPartCnt.value;
	
	// alert (" addPartCnt " +addPartCnt + " cnt b4 loop " + cnt);
	
	if (addPartCnt == -1 && cnt == -1)
	{
		cnt = 0;
	}
	else {
			if (addPartCnt > cnt  ) {
				cnt = addPartCnt;
			 }

			 else if (cnt > addPartCnt){
			 cnt = cnt;
			 }

			 else if (cnt == 0){
			 ++cnt;
			}
		}

    var tbody = document.getElementById(id).getElementsByTagName("TBODY")[0];
    var row = document.createElement("TR");
    var td1 = document.createElement("TD");
    td1.innerHTML = fnCreateCell('TXT','Part',8);
    td1.align = "Center";
    var td2 = document.createElement("TD");
    td2.innerHTML = '&nbsp;';
    td2.align = "Center";
    var td3 = document.createElement("TD");
    td3.innerHTML = fnCreateCell('TXT','QtyQ',3);
    td3.align = "Center";
        
    row.appendChild(td1);
    row.appendChild(td2);
    row.appendChild(td3);
       
    tbody.appendChild(row);
	cnt++;
	// alert(" cnt is : " + cnt);
	varUpdateFlag = "NotClicked";
}

function fnCreateCell(type,val,size)
{
	val = val + cnt;
//	alert (" val is : " + val);
	var html = '';
	if (type == 'TXT')
	{
		html = '<input type=text size='+size+' name=Txt_'+val+' class=InputArea>';
	}
	else if (type == 'CHK')
	{
		html = '<input type=checkbox name=Chk_'+val+'>';
	}
	return html;
}

function fnPartSearch()
	{
	var varAccId;
	var varPartNum;
	var varCount;
	
		windowOpener("/GmSearchServlet?hAction=SearchPart&Txt_PartNum="+encodeURIComponent(varPartNum)+"&hCount="+varCount+"&Txt_AccId="+varAccId,"Search","resizable=yes,scrollbars=yes,top=340,left=420,width=600,height=435");
	}


function fnUpdate()
{
	var partobj;
	var qtyobj;
	var inputstr = '';
	var str = '';
	var counter = cnt;
	var flag = 0;
	
	if(cnt == -1)
	{
		counter = document.frmMfg.hAddPartCnt.value;
	}
	
	// alert(" counter -- " + counter);
			
	for(var i=0; i<counter; i++)
	{
		partobj = eval("document.frmMfg.Txt_Part"+i);
		qtyobj = eval("document.frmMfg.Txt_QtyQ"+i);
		str = partobj.value + '^' + qtyobj.value + '^|';
		inputstr = inputstr + str;
		
		if(partobj.value ==''|| qtyobj.value=='')
		{
			flag = 1;
		}
	}
	if( counter ==-1)
	{
		Error_Details("Nothing is updated.");
	}
	if( flag ==1)
	{
		Error_Details("<b>Part Number or Qty Required field</b> can not be left blank. Please enter valid data.");
	}
	
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear(); 
		return false;
	}
//	alert(" inputstring " + inputstr);
	varUpdateFlag = "Clicked";
	document.frmMfg.hInputStr.value = inputstr;
	document.frmMfg.hAction.value = 'FetchAddPart';
	document.frmMfg.hOpt.value = 'REQNEW';
  	document.frmMfg.submit();
}

var colcnt = 0;
function fnSplit(val,pnum)
{
	var hpnum = '';
	colcnt++;
	var hcnt = parseInt(document.frmMfg.hCnt.value);
	hcnt = hcnt+colcnt;
	//Added new hidden partnumber for fixing the special char issue PC-2120
	if(pnum != null){
		hpnum = pnum.replace(/[^a-zA-Z0-9]/g, "");
	}
	var NewQtyHtml = "<span id='Sp"+colcnt+"'><BR>&nbsp;&nbsp;&nbsp;&nbsp;<input type='hidden' name='hPartNum"+hcnt+"' value='"+hpnum+"'><input type='hidden' name='hPartNumber"+hcnt+"' value='"+pnum+"'><input type='text' size='7' value='' name='Txt_Qty"+hcnt+"' class='InputArea' style='margin:3px 0' onFocus=changeBgColor(this,'#AACCE8'); onBlur=changeBgColor(this,'#ffffff'); tabindex=1></span>";
	var NewCntlHtml = "<span id='SpCnum"+colcnt+"'><BR><input type='text' size='10' value='' name='Txt_CNum"+hcnt+"' class='InputArea' style='margin:3px 0' onFocus=changeBgColor(this,'#AACCE8'); onBlur=changeBgColor(this,'#ffffff'); tabindex=1>&nbsp;<a href=javascript:fnDelete("+colcnt+");><img class='gmVerticalMid' src=/images/btn_remove.gif border=0></a></span>";
	var QtyObj = eval("document.all.Cell"+val);
	QtyObj.insertAdjacentHTML("beforeEnd",NewQtyHtml);
	var CnumObj = eval("document.all.CellCnum"+val);
	CnumObj.insertAdjacentHTML("beforeEnd",NewCntlHtml);
	
}

function fnDelete(val)
{
	obj = eval("document.all.Sp"+val);
	obj.innerHTML = "";
	obj = eval("document.all.SpCnum"+val);
	obj.innerHTML = "";
}

function fnCompleteReq()
{
	Error_Clear();
	hcnt = parseInt(document.frmMfg.hCnt.value);
	hcnt = hcnt+colcnt;
	document.frmMfg.hCnt.value = hcnt;
	var strErrorMore = '';
	var theinputs = document.getElementsByTagName("input");
    var arr = document.frmMfg.pnums.value.split(',');
    var blk = '';
    var cnum = '';
    var qtycnt = 0;
    hcnt++;
    var arrlen = arr.length;
    var inputstr = '';
	var blFlag = false;
//	var req = '';
//	var strErrorMore1 = '';
	
    for(var i=0;i<arrlen;i++)
    {
       qid = arr[i];
       pnum1 = qid.replace(/[^a-zA-Z0-9]/g, ""); //replace condition changed for PC-307
       blk = eval("document.frmMfg.Qty_Blk"+pnum1);
	   req = eval("document.frmMfg.Qty_Req"+pnum1);
	   
		    for(var j=0;j<hcnt;j++)
		    {
				pnumobj = eval("document.frmMfg.hPartNum"+j);

				if (pnumobj)
				{
					if (pnumobj.value == pnum1) //qid changed to pnum1 for PC-2120
					{
						qtyobj = eval("document.frmMfg.Txt_Qty"+j);
						if (qtyobj == undefined || qtyobj == null){
							qtyobj = eval("document.frmMfg.hQtyToRelease"+j);
						}
						cnumobj = eval("document.frmMfg.Txt_CNum"+j);
						if(!isNaN(qtyobj.value))
						{
							qty = parseFloat(qtyobj.value);
							cnum = cnumobj.value;
							if (qty >= 0 && cnum != '')
							{
								qtycnt = qtycnt + qty;
							}

						    if (cnum.trim() == '') {
						        blFlag = true;
						    }
						}
						else
						{
							alert("Non Numeric");
						}
					}
				}
			}

		if (qtycnt > parseFloat(blk.value) )
		{
			strErrorMore  = strErrorMore + ","+ qid;
			// alert();
		}
        /*
        if (qtycnt > parseFloat(req.value) )
		{
			strErrorMore1  = strErrorMore1 + ","+ qid;
			// alert();
		}
		*/
		qtycnt = 0;
	}

	if (blFlag)
	{
		Error_Details("No Control Numbers have been entered. Cannot perform action");
	}

	if (strErrorMore != '')
	{
		Error_Details("Qty entered cannot be more than Qty Available for the following Parts: "+strErrorMore);
	}

	/*if (strErrorMore1 != '')
	{
		Error_Details("Qty entered cannot be more than Qty Required for the following Parts: "+strErrorMore);
	}	*/
	
	if (ErrorCount > 0)
	{
		Error_Show();
		hcnt = 0;
		return false;
	}else{

		for(var i=0; i<hcnt; i++)
		{
			partobj = eval("document.frmMfg.hPartNumber"+i); //hPartNumber changed instead of hPartNum for PC-307
			qtyobj = eval("document.frmMfg.Txt_Qty"+i);
			if (qtyobj == undefined || qtyobj == null){
				qtyobj = eval("document.frmMfg.hQtyToRelease"+i);
			}
			cnumobj = eval("document.frmMfg.Txt_CNum"+i);
			//below undefined check for PC-2120
			if(partobj != undefined && partobj != null){
				str = partobj.value + '^' + qtyobj.value + '^' + cnumobj.value+ '|';
				inputstr = inputstr + str;
			}
		}
		document.frmMfg.hInputStr.value = inputstr;
		document.frmMfg.hAction.value = 'ReleasePart';
	  	document.frmMfg.submit();
	}


}

function fnViewPrint()
{
	windowOpener("/GmPOReceiveServlet?hAction=ViewManfDHRPrint&hDHRId="+strMfgWODhrId+"&hVenId="+strVendorId+"","DHR","resizable=yes,scrollbars=yes,top=150,left=200,width=850,height=800");
}

function fnPrintBOM()
{
	var strMfgQty = document.frmMfg.hMfgQty.value;
	windowOpener("/GmMFGInitiateWOServlet?hAction=PrintBOM&hMatReqId="+strMatReqId+"&hDHRId="+strMfgWODhrId+"&hQtyToManf="+strMfgQty+"","DHR","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

//PC-2135 Inhouse MWO paperwork 2020 - Product Traveler and DHR Review
function fnViewPrintWO()
{
	windowOpener("/GmWOServlet?hAction=PrintWO&hWOId="+strWOId+"&hDocRev="+"F"+"&hDocFl="+"Y","WO","resizable=yes,scrollbars=yes,top=150,left=200,width=850,height=800");
}

function fnRelease()
{
	Error_Clear();
	if (varUpdateFlag == "NotClicked")
	{	
		alert(" Please update the Newly added parts before Releasing ");
		return false;
	}
	
	var partobj;
	var qtyobj;
	var invqtyobj;
	var qty;
	var invqty;
	var inputstr = '';
	var strErrorMore = '';
	var str = '';
	var partRowCnt = document.frmMfg.hAddPartCnt.value;
	
	
	for(var i=0; i<partRowCnt; i++)
	{
		partobj = eval("document.frmMfg.Txt_Part"+i);
		qtyobj = eval("document.frmMfg.Txt_QtyQ"+i);
		invqtyobj = eval("document.frmMfg.Txt_INVQtyQ"+i);
		if(!isNaN(qtyobj.value)&&!isNaN(invqtyobj.value))
		{
				qty = parseFloat(qtyobj.value);
				invqty = parseFloat(invqtyobj.value);
			if (qty > invqty)
			{
				strErrorMore  = strErrorMore + ','+ partobj.value;
				
			}
		}
		str = partobj.value + '^' + qtyobj.value + '^|';
		inputstr = inputstr + str;
	}
	if (strErrorMore != '')
	{
		Error_Details('Required Qty cannot be more than Shelf Qty  for the following Part: '+strErrorMore.substr(1,strErrorMore.length));
	}
	if (ErrorCount > 0)
	{
		Error_Show();
		return false;
	}else{
	document.frmMfg.hInputStr.value = inputstr;
	document.frmMfg.hAction.value = 'AddPart';
  	document.frmMfg.submit();
  	}
}


function fnReleaseBO()
{  
	document.frmMfg.hAction.value = 'ReleaseBO';
  	document.frmMfg.submit();
   
}

function fnVoid()
{
		document.frmMfg.hTxnId.value = document.frmMfg.hMatReqId.value; 
		document.frmMfg.action ="/GmCommonCancelServlet";
		document.frmMfg.hAction.value = "Load";		
		document.frmMfg.hCancelType.value = 'VODMR'
		document.frmMfg.submit();
}

function fnRollback()
{
		document.frmMfg.hTxnId.value = document.frmMfg.hMatReqId.value;
		document.frmMfg.action ="/GmCommonCancelServlet";
		document.frmMfg.hAction.value = "Load";		
		document.frmMfg.hCancelType.value = 'ROBMR'
		document.frmMfg.submit();
}

 function fnVoidDHR()
{
		
		document.frmMfg.hTxnId.value = document.frmMfg.hMfgWODHRId.value; 
		document.frmMfg.action ="/GmCommonCancelServlet";
		document.frmMfg.hAction.value = "Load";		
		document.frmMfg.hCancelType.value = 'VODHR'
		document.frmMfg.submit();
}

