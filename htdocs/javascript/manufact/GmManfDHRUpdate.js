//All JS functions moved from GmManfDHRUpdate.jsp

var gridObj;
function fnViewPrint()
{
	windowOpener("/GmPOReceiveServlet?hAction=ViewManfDHRPrint&hDHRId="+strDhrId+"&hVenId="+strVendorId+"","DHR","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnViewPrintAll()
{
var dhrTransIds='';
var gridrows =gridObj.getAllRowIds();
var gridrowsarr = gridrows.toString().split(",");
var arrLen = gridrowsarr.length;
var rowId='';
var gridval='';
for (var i=0;i<arrLen;i++)
{
    rowId=gridrowsarr[i];
    gridval=gridObj.cellById(rowId,2).getValue();
     if (gridval != '')
     {
	    dhrTransIds=dhrTransIds+ gridval;
    	dhrTransIds=dhrTransIds + "$" + rowId + ',';
     }	
}
windowOpener("/GmDHRUpdateServlet?hAction=DhrPrintAll&hId="+dhrTransIds,"Con","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnOnPageLoad()
{
	objAction = document.frmOrder.hAction.value;
	
	if(objAction == 'V' || objAction == '')
    {
		if(objGridData.indexOf("cell", 0)==-1){
			  return true;
		}
		gridObj = initGrid('dataGridDiv',objGridData);
		gridObj.setEditable(false);
		
		if(gridObj != '')
    	{
			qtyremains = document.getElementById("qty_remains");
    	}
    }else if(objAction == 'P' && (gStrNCMRFl != '1' || gStrNCMRFl == '0')){	
    	if(objGridData.indexOf("cell", 0)==-1){
			  return true;
		}
	  	gridObj = setDefalutGridProperties('dataGridDiv');
	  	gridObj.enableEditTabOnly(true);
	  	initializeDefaultGrid(gridObj,objGridData);
	  	gridObj.enableEditEvents(false, true, true);
	  	gridObj.enableLightMouseNavigation(true);	  	
		gridObj.selectCell(0,1);
		gridObj.editCell();
		if(strrwfl !=""){
		gridObj.cellById(10304704, 1).setValue(qtyPackages);
		}
		gridObj.attachEvent("onEditCell", function(stage, rowId, cellInd) {
	    	    if (stage == 0) {
	    	        //User starting cell editing: row id is" + rowId + ", cell index is " + cellInd
	    	    } else if (stage == 1) {
	    	        //Cell editor opened
	    	    } else if (stage == 2) {
	    	        //Cell editor closed
	    	    	var strCellValue=gridObj.cellById(rowId, cellInd).getValue();
	    	    	if(!IsNumeric(strCellValue)){
	    	    		Error_Details(message[46] + '<b>' + gridObj.cellById(rowId, 0).getValue()+'</b>');
	    	    	}
	    	    	if (ErrorCount > 0)
	    	    	{
	    	    		gridObj.selectCell(rowId,cellInd);
	    	    		gridObj.editCell();	    	    	
	    	    		gridObj.cellById(rowId, cellInd).setValue('');
	    	    		Error_Show();
	    	    		Error_Clear();
	    	    		return false;
	    	    	}
	    	    }
	    	    return true;
	    	});
    	}else{
    		if(objGridData.indexOf("cell", 0)==-1){
			  return true;
			}
			gridObj = initGrid('dataGridDiv',objGridData);
			gridObj.setEditable(false);
    	}
}
function IsNumeric(strString) //  check for valid numeric strings	
{
	if(!/\D/.test(strString)) return true;//IF NUMBER
	else if(/^\d+\.\d+$/.test(strString)) return true;//IF A DECIMAL NUMBER HAVING AN INTEGER ON EITHER SIDE OF THE DOT(.)
	else return false;
}
function fnUpdate()
{
	Error_Clear();	
	obj = document.frmOrder.hAction;
	var qtyrec = parseFloat(document.frmOrder.hRecQty.value);
	//Below code will be used to validate the verification process
	if (obj.value == 'C')
	{
		fnSubmit();
	}
	else
	{
		if (obj.value == 'I')
		{
			obj1 = document.frmOrder.Txt_QtyInsp.value;
			if (obj1 == '')
			{
				Error_Details(message[10]);
			}
			obj1 = parseFloat(obj1);
			obj2 = document.frmOrder.Cbo_InspEmp.value;
			if (obj2 == '0')
			{
				Error_Details(message[11]);
			}
			obj3 = document.frmOrder.Txt_QtyRej.value;
			if (obj3 == '')
			{
				Error_Details(message[12]);
			}
			if (obj1 > qtyrec)
			{
				Error_Details(message[17]);
			}
			if (obj3 > qtyrec)
			{
				Error_Details(message[18]);
			}		
			obj4 = document.frmOrder.Cbo_ReasonType.value;
			if (obj3 > 0 && obj4 == '0')
			{
				Error_Details(message[25]);
			}
		}else if (obj.value == 'P')
		{
			/*var strSterFl=document.frmOrder.hSterFl.value;
			var skip =false;
			if(strSterFl!='1')
				skip = document.frmOrder.Chk_Skip.status;
			
			if (!skip)
			{*/
			if(strrwfl !=""){			
				var strShelf = gridObj.cellById(100071, 1).getValue();
				var strBulk = gridObj.cellById(100072, 1).getValue();
				var strRm = gridObj.cellById(100075, 1).getValue();
				if(strShelf !="" || strBulk !="" || strRm != "")
				{
					Error_Details( " <b>"+strLotNum+"</b> is designated for OUS use only and cannot move to FG/Bulk/Raw Material");
				}
			                }
				var qtyrej = parseFloat(document.frmOrder.hRejQty.value);

			  if(isNaN(qtyrej)){
				  qtyrej = 0;
					}
			
				var qtyclose = parseFloat(document.frmOrder.hCloseQty.value);
				if(isNaN(qtyclose)){
					qtyclose = 0;
				}
				var gridrows =gridObj.getAllRowIds();
				var gridrowsarr = gridrows.toString().split(",");
				var arrLen = gridrowsarr.length;
				var gridval;				
				var obj1=0;
				var inpStr="";
				obj1=0;
				for (var i=0;i<arrLen;i++)
				{
			        rowId=gridrowsarr[i];
			        gridval=gridObj.cellById(rowId,1).getValue();
			        
			        if(gridval!='' && gridval!='0' && gridval!=undefined && gridval!=null){
			        	inpStr=inpStr + rowId;
			        	inpStr = inpStr +',';
			        	inpStr=inpStr + gridval;
			        	inpStr=inpStr + '|'; 
			        	obj1+=parseFloat(gridval);
			        }
				}
				obj1=Math.round(obj1*Math.pow(10,2))/Math.pow(10,2);

				if(qtyrec + qtyclose != qtyrej){
					if (obj1 == '')
					{
						Error_Details(message[13]);
					}
					if (obj1 > qtyrec-qtyrej+qtyclose)
					{
						Error_Details(message[20]);
					}
					if (obj1 < qtyrec-qtyrej+qtyclose)
					{
						Error_Details(message[19]);
					}				
				}
			//}
			document.frmOrder.hInpStr.value=inpStr;
		}else if (obj.value == 'V')
		{
			var qtyrej = parseFloat(document.frmOrder.hRejQty.value);
			
			if(isNaN(qtyrej)){
				qtyrej = 0;
			}
			
			var qtyclose = parseFloat(document.frmOrder.hCloseQty.value);
			if(isNaN(qtyclose)){
				qtyclose = 0;
			}
			obj1 = document.frmOrder.Txt_Qty.value;
			if(qtyrec!=qtyrej){
				if (obj1 == '')
				{
					Error_Details(message[15]);
				}
				obj1 = parseFloat(obj1);
				
				if (obj1 > qtyrec-qtyrej+qtyclose)
				{
					Error_Details(message[22]);
				}
				if (obj1 < qtyrec-qtyrej+qtyclose)
				{
					Error_Details(message[23]);
				}
			}
		}

		if (ErrorCount > 0)
		{
				Error_Show();
				Error_Clear();
				return false;
		}

		document.frmOrder.hAction.value = mode;
	  	document.frmOrder.action=strServletPath+"/GmDHRUpdateServlet";
	  	document.frmOrder.submit();
	 }
}

function fnViewPrintWO()
{
windowOpener("/GmWOServlet?hAction=PrintWO&hWOId="+srtWoId+"&hDocRev="+strDocRev+"&hDocFl="+strDocActiveFl+"","WO1","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=640");
}

function fnViewPrintNCMR()
{
windowOpener("/GmNCMRServlet?hAction=PrintNCMR&hId="+strNcmrId+"","NCMR","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=640");
}

function fnCalDate(val)
{
	var year = '';
	var month = '';
	var NormalYr = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
	var LeapYr = new Array(31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
	var cal = 0;
	var rem = 0;
	var cal1 = 0;
	var date = '';
	var num = val.substring(3,6);

	//Hardcoded till Year 2008
	if (val.charAt(2)=='D') // For 2003
	{
		year = '2003';
		days = NormalYr;
	}
	else if (val.charAt(2)=='E') // For 2004
	{
		year = '2004';
		days = LeapYr;
	}
	else if (val.charAt(2)=='F') // For 2005
	{
		year = '2005';
		days = NormalYr;
	}
	else if (val.charAt(2)=='G') // For 2006
	{
		year = '2006';
		days = NormalYr;
	}
	else if (val.charAt(2)=='H') // For 2007
	{
		year = '2007';
		days = NormalYr;
	}
	else if (val.charAt(2)=='J') // For 2008
	{
		year = '2008';
		days = LeapYr;
	}

	for (var i=0;i<days.length;i++ )
	{
		cal = cal + days[i];
		if (cal>=num)
		{
			cal1=cal-days[i];
			rem = num-cal1;
			if (rem<10)
			{
				rem = "0"+rem;
			}
			month = i+1;
			if (month<10)
			{
				month="0"+month;
			}
			date = month+"/"+rem+"/"+year;
			break;
		}
	}
	return date;
}

function fnSubmit()
{
	var vWO = '';
	var vQty = 0;
	var vCntNum = 0;

	var str = '';
	var obj = '';
	var vPNum = '';
	var vManfDate = '';

	hcnt = parseInt(document.frmOrder.hSubCnt.value);

	for (var i=0;i<hcnt;i++)
	{
		val = eval("document.frmOrder.Txt_CNum"+i);
		if (val)
		{
			vCntNum = val.value;
			if (vCntNum != '')
			{
				val = eval("document.frmOrder.hSubComp"+i);
				vPNum = val.value;
				val = eval("document.frmOrder.hSubDHR"+i);
				vSubDhrId = val.value;
				val = eval("document.frmOrder.Txt_CNum"+i);
				vCntNum = val.value;
				alert(vManfDate);
				if ( vCntNum == 'NOC#')
				{
					vManfDate = strDate;
				}
				else
				{
					vManfDate = fnCalDate(vCntNum);
				}
				str = str + vPNum +"^"+ vSubDhrId +"^"+ vCntNum +"^"+ vManfDate +"|";
			}
		}
	}
	//document.frmOrder.hInputString.value = str.substring(0,str.length-1);
	document.frmOrder.hInputString.value = str;
	//alert("STRING: "+document.frmOrder.hInputString.value);
	document.frmOrder.action=strServletPath+"/GmDHRUpdateServlet";
	document.frmOrder.submit();
}
