//Progbuild verification

function fnOnPageLoad() {
	if (objGridData != '') {
		gridObj = initGrid('manufCostRpt', objGridData, '', '');
		gridObj.enableTooltips("true,true,true,true");

	}
}

function fnLoad() {
	var part = document.frmManufCost.pnum.value;
	var project = document.frmManufCost.projectId.value;
	var strShowZeroCost = document.frmManufCost.strShowZeroCost.checked;
	
	if(part =='' && project =="0"){
		if(strShowZeroCost == false)
		Error_Details("Please select Project Name or enter the Part Number to proceed"); //Please Enter a Part Number to Proceed
	}
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	document.frmManufCost.strOpt.value = "Load";
	document.frmManufCost.action = "/gmManufCost.do?method=fetchCostingReports";
	fnStartProgress('Y');
	document.frmManufCost.submit();
}

function fnEditCost(pnum) {
	document.frmManufCost.pnum.value = pnum;
	document.frmManufCost.strOpt.value = "Load";
	document.frmManufCost.action = "/gmManufCost.do?method=editManufCost";
	document.frmManufCost.submit();
}
function fnEnter() {
	if (event.keyCode == 13) {
		fnLoad();
	}
}