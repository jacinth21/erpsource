function fnLoadWO()
{
	document.frmMfg.hAction.value = 'Reload';
	fnStartProgress('Y');
  	document.frmMfg.submit();
}

function fnInitate()
{
	var pendqty = document.frmMfg.hOrdPending.value;
	var manfqty = TRIM(document.frmMfg.Txt_MfgQty.value);
	var expDate = TRIM(document.frmMfg.Txt_ExpDate.value);
	if(frmMfg.rwflag.checked == true){
		frmMfg.rwflag.value = "Y";
	}else{
		frmMfg.rwflag.value = "N";
	}
	var partNum = document.frmMfg.hPartNum.value;
	// validate the Manuf. Qty
	fnValidateTxtFld('Txt_MfgQty', ' Manufacture Qty ');
	// validate the special character.
	var validManfQty = manfqty.replace(/[.0-9]+/g,''); // Qty text box
	
	if(validManfQty.length >0){
		Error_Details("Please enter a valid Qty for <b>Manufacture</b>.");
	}
	// If the part is sterile then Expiry Date should not be Empty
	if(expDate == '' && vProdClass =='4030')
	{
		Error_Details("<b>Expiry Date</b> cannot be left blank for part(s) " + partNum);
	}
	
	// Expiry Date cannot be a past date (Today or Future Date)
	var currDate = document.frmMfg.hManfDt.value;
	if(dateDiff(currDate,expDate,format)<0){
		//Expiry Date can not be a past date.
		Error_Details("<b>Expiry Date</b> can not be a past date for part(s) " + partNum);		
	}
	
    // Validate Date in session format
	CommonDateValidation(document.frmMfg.Txt_ExpDate,format,' <b>Expiry Date</b> '+ expDate +' is not a valid date in the format '+format);
		
		var varRows = rowsize;   
		var inputBOString = '';  
		var inputString= '';  
		var validReqQty = '';
		var validPnum =''; 
		 
		for (var i =0; i < varRows; i++) 
		{
			obj = eval("document.frmMfg.Chk_BO"+i);
		 	obj1 = eval("document.frmMfg.hPartNum"+(i+1)); 
		 	obj2  = eval("document.frmMfg.reqQty"+i);
		 	obj3  = eval("document.frmMfg.hUnitType"+i);

		 	validReqQty = (obj2.value).replace(/[0-9]+/g,''); // Qty text box
		 	// validate the request Qty
		 	if(validReqQty.length >0){
		 		validPnum = validPnum + obj1.value +', ';
			 }
			if(obj.checked)		
			{ 
				inputBOString = inputBOString  + obj1.value +  '^';
				 
			}
			else
				inputBOString = inputBOString  + '' +  '^'; 

			if(obj3.value !='40012' )
			{	
			 	inputString = inputString  + obj1.value +  ',' + obj2.value +  '|'; 
			}
		   
		}

		if(validPnum !=''){
			validPnum = validPnum.substring(0,(validPnum.length)-2);
			Error_Details('Please enter a valid QTY for the following Part Numbers: <b>'+validPnum+'</b>');
		}
		
		if (ErrorCount > 0) {
			Error_Show();
			Error_Clear();
			return false;
		}else{	 
		document.frmMfg.hboinputStr.value = inputBOString;  
		document.frmMfg.hinputStr.value = inputString;  
		document.frmMfg.hAction.value = 'InitiateWO';
		//document.frmMfg.rwflag.value  = rwFlags;
   	 	document.frmMfg.submit();
  	}
  	
   
}

function fnCalculate()
{
	windowOpener("<%=strJsPath%>/GmNuBoneCalculator.htm","DHR","resizable=yes,scrollbars=yes,top=150,left=200,width=400,height=220");
}

function fnCalcReqAmt()
 {
	var varRows = rowsize;  
	var totalAmt = 0;   
	
	val =  document.frmMfg.Txt_MfgQty.value;
	
	for (var i =0; i < varRows; i++) 
	{ 	 
	 	obj  = eval("document.frmMfg.hSetQty"+(i+1)); 
		varSetAmt = obj.value * 1 ; 
		varQtyMfg = val * 1;
		amt = varSetAmt * varQtyMfg; 
		obj2  = eval("document.frmMfg.hUnitType"+i);
		
		obj3  = eval("document.frmMfg.reqQty"+i);
		if(obj2.value =='40012') //one each info	
		obj3.value =  "";
		else if (obj2.value =='40011') // No multiplication
		obj3.value =  varSetAmt;
		else
		obj3.value = roundNumber( amt, 3);
	}
	   
	}

function roundNumber(number, digits) {
	var multiple = Math.pow(10, digits);            
	var rndedNum = Math.round(number * multiple) / multiple;
	return rndedNum;        
}


