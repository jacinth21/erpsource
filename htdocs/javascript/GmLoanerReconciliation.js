var objGridData;


function fnOnPageLoad(objGridData)
{	
	//alert(deptid);
	
	if(objGridData !=''){
		gridObj = initGrid('grpData',objGridData);
		gridObj.groupBy(1);
		gridObj.customGroupFormat=function(name,count){
		//	alert(name + " : " + count);
            return name;
        }
		gridObj.collapseAllGroups();
		gridObj.expandAllGroups(); 	 	   			   			   
		gridObj.setColumnHidden(1,true);
        
	}
	/*
	
	if (objGridData != '')
	{
		//gridObj = initTreeGrid('grpData',objGridData);
		var header=new Array();
		if(deptid!='S' &&  haction !='hideDates'){
		header[0]="";
		header[1]="";
		header[2]="Tag Id";
		header[3]="Etch. Id";
		header[4]="Set Name";
		header[5]="Trans. Id";
		header[6]="Loaned Date";
		header[7]="Expected Returned Date";
		header[8]="Actual Returned Date";
		header[9]="Days Late";
		header[10]="Received By";
		header[11]="Pending Recon.";
		header[12]="Ticket";
		var colTypes='tree,txt,txt,txt,txt,txt,txt,txt,txt,txt,txt,txt,txt';
		var alignment='left,left,center,left,left,left,left,left,left,left,left,center,left';
	  }else if(haction == 'hideDates'){
		    header[0]="Distributor";
			header[1]="Sales Rep";
			header[2]="Request Id";
			header[3]="Etch. Id";
			header[4]="Set Name";
			header[5]="Days Late";
			header[6]="Pending Recon.";
			//header[10]="Ticket"; 
			var colTypes='tree,txt,txt,txt,txt,txt,txt';
			var alignment='left,left,left,left,left,left,center';
		  }
		else{
		    header[0]="";
			header[1]="";
			header[3]="Etch. Id";
			header[4]="Set Name";
			header[5]="Loaned Date";
			header[6]="Expected Returned Date";
			header[7]="Actual Returned Date";
			header[8]="Days Late";
			header[9]="Pending Recon.";
			//header[10]="Ticket"; 
			var colTypes='tree,txt,txt,txt,txt,txt,txt,txt,txt';
			var alignment='left,left,left,left,left,left,left,left,center';
	  }
		
		gridObj = initTreeGrid('grpData',objGridData,header,colTypes,alignment);
		if(deptid!='S' &&  haction !='hideDates'){
		gridObj.setColWidth(0,"0");
		gridObj.setColWidth(1,"0");
		gridObj.setColWidth(3,"8");
		gridObj.setColWidth(4,"18");
		gridObj.setColWidth(5,"13");
		gridObj.setColWidth(6,"7");
		gridObj.setColWidth(7,"9");
		gridObj.setColWidth(8,"9");
		gridObj.setColWidth(9,"6");
		gridObj.setColWidth(10,"6");
		gridObj.setColWidth(11,"9");
		gridObj.setColWidth(12,"7");
		gridObj.setColWidth(13,"8");
		gridObj.setColSorting("int,na,na,str,str,str,dt,dt,dt,int,str,str,str"); 
		gridObj.groupBy(1);
		gridObj.collapseAllGroups();
		gridObj.expandAllGroups();
		 	 	   			   			   
		gridObj.setColumnHidden(1,true);
		
		}else if(haction == 'hideDates'){
			gridObj.setColWidth(0,"0");
			gridObj.setColWidth(1,"0");
			gridObj.setColWidth(4,"24");
			gridObj.setColWidth(5,"12");
			gridObj.setColWidth(10,"10");
			gridObj.setColWidth(12,"12");
			//gridObj.setColWidth(13,"8");
			gridObj.setColSorting("int,na,str,str,int,str"); 
			gridObj.groupBy(1);
			gridObj.collapseAllGroups();
			gridObj.expandAllGroups(); 	   			   			   
			gridObj.setColumnHidden(1,true);
		}
		else{
			gridObj.setColWidth(0,"0");
			gridObj.setColWidth(1,"0");
			gridObj.setColWidth(2,"20");
			gridObj.setColWidth(3,"11");
			gridObj.setColWidth(4,"7");
			gridObj.setColWidth(5,"7");
			gridObj.setColWidth(6,"8");
			gridObj.setColWidth(7,"8");
			gridObj.setColWidth(8,"10");
			//gridObj.setColWidth(13,"8");
			gridObj.setColSorting("int,na,str,str,dt,dt,dt,int,str");
			gridObj.groupBy(1);
			gridObj.collapseAllGroups();
			gridObj.expandAllGroups(); 	 	   			   			   
			gridObj.setColumnHidden(1,true);
		}
		}*/
		
	}



function fnInHousePicSlip(val)
{
	windowOpener("/GmConsignInHouseServlet?hAction=PicSlip&hConsignId="+val,"SetPic","resizable=yes,scrollbars=yes,top=150,left=150,width=750,height=500");
}



function fnCallRecon(val)
{
	document.frmLoanerReconciliation.hId.value = val;
	document.frmLoanerReconciliation.hAction.value = "LoanRecon";
	document.frmLoanerReconciliation.action = "/GmLoanerReconServlet";
	document.frmLoanerReconciliation.submit();	
}

function  onReload(){	
	
	
	objStartDt = document.frmLoanerReconciliation.fromDt;
	objEndDt = document.frmLoanerReconciliation.toDt;
	var dateDiffVal = dateDiff(todaysDate,objStartDt.value,format);
	
/*	if(dateDiffVal < 0){
		if(dateDiffVal < 0 && document.frmLoanerReconciliation.dist.value == 0 && document.frmLoanerReconciliation.rep.value == 0 ){
			
			Error_Details("Please choose a valid option for the filed: <b>Field Sales / Sales Rep<b>");
		}
	}

	 */
	if(objStartDt.value != ""){
		CommonDateValidation(objStartDt,format, Error_Details_Trans(message[10070],format));
	}

	if(objEndDt.value != ""){
		
		CommonDateValidation(objStartDt,format, Error_Details_Trans(message[10070],format));
	}

	if (ErrorCount > 0)
		{
			Error_Show();
			Error_Clear();
			return false;
		}	
	
	
	if (document.frmLoanerReconciliation.status.value == 50821)
	{
		fnValidateTxtFld('dtType',message[5124]);
		fnValidateTxtFld('fromDt',message[5126]);
		fnValidateTxtFld('toDt',message[5127]);
	}
	if (ErrorCount > 0)
		{
			Error_Show();
			 Error_Clear();
			return false;
		}
	document.frmLoanerReconciliation.strOpt.value = "reLoad"
	fnStartProgress();
	frmLoanerReconciliation.submit();
}

function fnOpenLog(id,type)
{
	windowOpener("/GmCommonLogServlet?hType="+type+"&hID="+id,"ConsignLog","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=300");
}


function fnSendEmail(){
	frmObj = document.frmLoanerReconciliation;
	var flg = true;
	var k = 0;
	var arrReqid=new Array();
		for(i=0;i<frmObj.elements.length;i++)
			{
				if (frmObj.elements[i].type =="checkbox")
				{
					if (frmObj.elements[i].name == 'Chk_reqid' && frmObj.elements[i].checked == true )
					{						
							for(j=0;j<arrReqid.length;j++){ // to put distinct values in the array
								if (arrReqid[j] == frmObj.elements[i].value)
								{
									flg = false;
									break;
								}
								flg = true;
							}
							if (flg == true)
							{
								arrReqid[k] = frmObj.elements[i].value;	
								k++;
							}					
					}
				}
			}
	
	if (arrReqid.toString() == "")
	{
		Error_Details(message[10048]);
	}
	if (ErrorCount > 0)
	{
		Error_Show();
		 Error_Clear();
		return false;
	}
	
	document.frmLoanerReconciliation.hreqids.value = arrReqid.toString();
	document.frmLoanerReconciliation.strOpt.value = "Email";
	document.frmLoanerReconciliation.submit();
}

function addToArray(arrReqid,val)
{
	for(i=0;i<arrReqid.length;i++){
		if (arrReqid[i] == val)
		{
			return arrReqid;
		}					
	}
	arrReqid[arrReqid.length+1] = val;	
	alert(arrReqid);
	return arrReqid;

}
function fnLoadRules()
{
	var cnt = document.frmVendor.hCnt.value; 
	var inputStr = '';
	for (var i=0;i<cnt;i++)
	{
		pnumobj = eval("document.frmVendor.hPartNum"+i);		
		inputStr = inputStr+pnumobj.value + ",";
	}
	if(inputStr.length > 1)
	{
		inputStr = inputStr.substr(0,inputStr.length-1);
		inputStr = "1^50607^"+inputStr + "^|";
	}
	document.frmVendor.action = "/gmRuleReport.do?loadRule=true&inputStr="+inputStr+"&strOpt=reload&txnId=0&consequenceId=0&status=0&initiatedBy=0";
	document.frmVendor.submit();
}
function fnSubmit()
{
	Error_Clear();
	var pnumobj = '';
	var missobj = '';
	var qtyobj = '';
	var priceobj = '';
	var setqty = 0;
	var missqty = 0;
	var strErrorMore = '';
	var strErrorTotal = '';
	var strErrorStock = '';
	var cnt = document.frmVendor.hCnt.value; 
	var str = '';
	var retobj = '';
	var quarobj = '';
	var stockobj = '';
	var repobj = '';
	var retqty = 0
	var quarqty = 0;
	var stockqty = 0;
	var repqty = 0;
	var retstr = '';
	var quarstr = '';
	var reptotal = 0;
	var retstr = '';
	var quarstr = '';
	var repstr = '';
	var strErrorQty = '';
	
	
	for (var i=0;i<cnt;i++)
	{
		reptotal = 0;
		pnumobj = eval("document.frmVendor.hPartNum"+i);
	 	missobj = eval("document.frmVendor.Txt_Miss"+i);
	 	qtyobj = eval("document.frmVendor.hSetQty"+i);
	 	priceobj = eval("document.frmVendor.hPrice"+i);
		retobj = eval("document.frmVendor.Txt_Ret"+i);
		quarobj = eval("document.frmVendor.Txt_Quar"+i);
		stockobj = eval("document.frmVendor.hStock"+i);
		repobj = eval("document.frmVendor.Txt_Rep"+i);

	 	setqty = parseInt(qtyobj.value);
	 	missqty = parseInt(missobj.value);
		retqty = parseInt(retobj.value);
		quarqty = parseInt(quarobj.value);
		stockqty = parseInt(stockobj.value);
		repqty = parseInt(repobj.value);
		
		if(!isNaN(repqty) && repqty > 0) //either 0 or more
		{
			if ( !isNaN(missqty) && !isNaN(quarqty))
			{
				if ((missqty+quarqty) != repqty && (missqty+quarqty) < repqty)
				{
					strErrorQty  = strErrorQty + ","+ pnumobj.value;
				}
			}
			else if (!isNaN(missqty))
			{
				if (missqty != repqty)
				{
					strErrorQty  = strErrorQty + ","+ pnumobj.value;
				}
			}
			else if (!isNaN(quarqty))
			{
				if (quarqty != repqty)
				{
					strErrorQty  = strErrorQty + ","+ pnumobj.value;
				}
			}
			else
			{
				strErrorQty  = strErrorQty + ","+ pnumobj.value;
			}
		}		
	 	if (!isNaN(missqty))
	 	{
	 		if (missqty > setqty)
	 		{
	 			strErrorMore  = strErrorMore + ","+ pnumobj.value;
	 		}else{
	 			str = str + pnumobj.value + ','+ missobj.value + ',,'+priceobj.value+'|';
				reptotal = parseInt(reptotal) + missqty;
	 		}
	 	}


	 	if (!isNaN(retqty))
	 	{
	 		if (retqty > setqty)
	 		{
	 			strErrorMore  = strErrorMore + ","+ pnumobj.value;
	 		}else{
	 			reptotal = parseInt(reptotal) + retqty;
				if (reptotal > setqty)
				{
	 				strErrorTotal  = strErrorTotal + ","+ pnumobj.value;
					reptotal = parseInt(reptotal) - retqty;
				}else{
					retstr = retstr + pnumobj.value + ','+ retqty + ',,'+priceobj.value+'|';
				}
				
	 		}
	 	}

	 	if (!isNaN(quarqty))
	 	{
	 		if (quarqty > setqty)
	 		{
	 			strErrorMore  = strErrorMore + ","+ pnumobj.value;
	 		}else{
	 			reptotal = parseInt(reptotal) + quarqty;
				if (reptotal > setqty)
				{
	 				strErrorTotal  = strErrorTotal + ","+ pnumobj.value;
					reptotal = parseInt(reptotal) - quarqty;
				}else{
					quarstr = quarstr + pnumobj.value + ','+ quarqty + ',,'+priceobj.value+'|';
				}
				
	 		}
	 	}

	 	if (!isNaN(repqty))
	 	{
	 		if (repqty > stockqty)
	 		{
	 			strErrorStock  = strErrorStock + ","+ pnumobj.value;
	 		}else{
	 			repstr = repstr + pnumobj.value + ','+ repqty + ',,'+priceobj.value+'|';
	 		}
	 	}


	}
	
	if (strErrorMore != '')
	{
		Error_Details(Error_Details_Trans(message[10049],strErrorMore.substr(1,strErrorMore.length)));
	}

	if (strErrorTotal != '')
	{
		Error_Details(Error_Details_Trans(message[10050],strErrorTotal.substr(1,strErrorTotal.length)));
	}

	if (strErrorStock != '')
	{
		Error_Details(Error_Details_Trans(message[10051],strErrorStock.substr(1,strErrorStock.length)));
	}
	
	if (strErrorQty != '')
	{
		Error_Details(Error_Details_Trans(message[10052],strErrorQty.substr(1,strErrorQty.length)));
	}

	if (ErrorCount > 0)
	{
		Error_Show();
		return false;
	}else{
		//alert("STR is:"+str);
		//alert("RET STR is:"+retstr);
		//alert("QUAR STR is:"+quarstr);
		//alert("REP STR is:"+repstr);
		
		

		document.frmVendor.hAction.value = 'ProcessReturnSet';
		document.frmVendor.hInputStr.value = str;
		document.frmVendor.hInputRetStr.value = retstr;
		document.frmVendor.hInputQuarStr.value = quarstr;
		document.frmVendor.hInputRepStr.value = repstr;
		
		var inputString = getIncidentsString();
		//alert('inputString = ' + inputString);
		document.frmVendor.hInputChgStr.value = inputString;
	  	document.frmVendor.submit();
  	}

}

function getIncidentsString()
{
	var inputString = '';

	var form = document.frmVendor;
	for(var i=0; i<form.elements.length; i++)
	{
		if(form.elements[i].name.indexOf('charge') != -1 && form.elements[i].name.indexOf('hcharge') == -1)
		{
			var nm = 'h' + form.elements[i].name;
			var objId = document.getElementById(nm).value;
			var objVal = form.elements[i].value;
			
			if(objVal == '' || objVal == '0')
			{
				continue;
			}
			//alert('Adding id = ' + objId + ' and value = ' + objVal);
			inputString += objId + ';' + objVal + '|';
		}
	}
	return inputString;
}

function changeTROBgColor(object,val,BgColor) {
   var obj1 = eval("document.all.tr"+prevtr);
   obj1.style.background = "";
   var obj = eval("document.all.tr"+val);
   prevtr = val;
   obj.style.background = BgColor;
   object.style.background = BgColor;
}

function fnPrintVer(val)
{
	windowOpener("/GmInHouseSetServlet?hAction=ViewPrint&hConsignId="+val+"&hType="+type,"LoanPrint","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}
