var cnt;

function fnAddRow(id)
{
	//alert ("Row Count : "+document.frmTerritory.hRowCnt.value);
	cnt = parseInt(document.frmTerritory.hRowCnt.value);
	
    var tbody = document.getElementById(id).getElementsByTagName("TBODY")[0];
    var row = document.createElement("TR")
       
    var td0 = document.createElement("TD")
    td0.innerHTML = fnCreateCell('TXT','SetID',7);
    td0.width = "60";
    
    var td1 = document.createElement("TD")
    td1.innerHTML = fnCreateCell('TXT','Qty',5);
    td1.align = "center";
    td1.width = "60";    
    
    row.appendChild(td0);
    row.appendChild(td1);
    
    tbody.appendChild(row);
	cnt++;
	document.frmTerritory.hRowCnt.value = cnt;
}



function fnMakeLoadString()
{
var varRowCnt = document.frmTerritory.hRowCnt.value;

var k;

var  objSetCell;
var varSetVal = "";
var varSetNums;

var  objQtyCell;
var varSetQtyVal = "";
var varSetQtys;

	for (k = 0; k < varRowCnt; k ++)
	{
		objSetCell = eval("document.frmTerritory.Txt_SetID"+k);
		
		if (objSetCell == "" || objSetCell == null )
		{
				comma = "";
		}
		else if (objSetCell.value == "")
		{
				comma = "";
		}
		else
		{
			// if(objSetCell.value != undefined)
			 //  {
				varSetNums = varSetNums + objSetCell.value;
				comma = ",";
				varSetVal = varSetVal + objSetCell.value + comma;
               //}
		}	
				
		objQtyCell = eval("document.frmTerritory.Txt_Qty"+k);
		//alert(objQtyCell.value);

		if (objQtyCell == "" || objQtyCell == null )
		{
				comma = "";
		}
		else if (objQtyCell.value == "")
		{
				comma = "";
		}
		else
		{					
			// if(objQtyCell.value != undefined)
			  // {
					varSetQtys = varSetQtys + objQtyCell.value;
					comma = ",";
					varSetQtyVal = varSetQtyVal + objQtyCell.value + comma;		
	           //}
	  }	
  }	

document.frmTerritory.hSetNums.value = varSetVal;
document.frmTerritory.hSetQty.value = varSetQtyVal;

}

function fnMakeInitiateString()
{

var len = document.frmTerritory.length.value;
var type = document.frmTerritory.Cbo_Type.value;
var  objpart;
var objret;
var part;

var  consqty;
var retqty ;
var retqtyint;

var partNums = "";
var partQtys = "";
var pendingQty = "";
var pendingQtyInt = "";
var partNumQtyMsg = "";
var validQtyMsg = "";
var zeroRetQtyMsg = "";

	for(var n=0;n<len;n++)
			{
					objpart = eval("document.frmTerritory.rad"+n); // The checkboxes would be like rad0, rad1 and so on
					
					if(objpart)
					{			
						objret = eval("document.frmTerritory.Txt_RetQty"+n); // The checkboxes would be like rad0, rad1 and so on
						pendingQty = eval("document.frmTerritory.hPendingQty"+n);
						part = objpart.value; // value would the CN or RA #
						consqty = parseInt(objpart.id);
						retqty = objret.value;
						retqtyint = parseInt(objret.value);
						pendingQtyInt = parseInt(pendingQty.value);

							if (objpart.checked)
							{
								if (retqtyint == 0)
								{
									zeroRetQtyMsg += part + ',';
									 //Error_Details(message[728]);
								 } else if(type == '3304' && (retqtyint > pendingQtyInt)){
									 partNumQtyMsg += part + ',';
								 }else if(retqtyint < 0 || isNaN(retqtyint)){
								 	 validQtyMsg += part + ',';
								 }
								else { 
										partNums = partNums +  part +',';
										partQtys = partQtys + retqtyint + ',';										
								}
							}
						}
					}
					
					if(zeroRetQtyMsg != ''){
						Error_Details(message[728]+message_operations[598]+zeroRetQtyMsg.substr(0,zeroRetQtyMsg.length-1));
					}
					
					if(partNumQtyMsg != ''){
						Error_Details(message_operations[599]+partNumQtyMsg.substr(0,partNumQtyMsg.length-1));
					}
					if(validQtyMsg != ''){
						Error_Details(message_operations[600]+validQtyMsg.substr(0,validQtyMsg.length-1));
					}
					
						if ((partNums == '' || partNums == null) && ErrorCount <= 0)
					 {
					 		Error_Details(message[727]); //Please select at least one item to return
					 }

			 	if (partNums.length >= 3900)
					 {
					 		Error_Details(message[722]);
					 }				
		       		
//alert("partNums = "+partNums);
//alert("partQtys = "+partQtys);

   if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}

document.frmTerritory.hPartID.value=partNums;
document.frmTerritory.hPartQty.value = partQtys;
return true;
}

function fnCreateCell(type,val,size)
{
	param = val;
	val = val + cnt;
	// alert(" Cnt is " + cnt);
	var html = '';
	
	if (type == 'TXT')
	{
		if (param == 'SetID') {
			   // alert("Txt_"+val);
				html = '<input type=text size='+size+' name=Txt_'+val+' class=InputArea onChange=fnClearVal('+cnt+'); onFocus=fnSetPartSearch('+cnt+'); value=\'\'>';
						}
		else if (param == 'Qty') {
				html = '<input type=text size='+size+' name=Txt_'+val+' class=InputArea value=\'\'>';
						}
		else if (param == 'hPrice') {
				html = '<input type=text size='+size+' name='+val+' class=InputArea value=\'\'>';
						}
	}	
	else if (type == 'CHK')
	{
		html = '<input type=checkbox name=Chk_'+val+'>';
	}	
	else if (type == 'Lbl')
	{
		if (param == 'InvQty'){
				html = '<td class=RightText id=Lbl_'+val+'>&nbsp; <%=strInvQty%> </td>';
						}
		
		else if (param == 'ExtPrice'){
				html = '<td class=RightText id=Lbl_'+val+'>&nbsp; <%=strExtCost%> </td>';
						}
		
		else if (param == 'PartDesc'){
				html = '<td class=RightText id=Lbl_'+val+'>&nbsp; <%=strPartDesc%> </td>';
						}
		}	
	else if (type == 'Btn')
	{
		var fn = size;
		html = '<input type=button onclick='+fn+' value = PartLookup name=Btn_'+val+' class=Button>';
	}	
	else if (type == 'Cbo')
	{
		var fn = size;
		html = '<select name=Cbo_OrdPartType'+val+' class=RightText  > <option value="50300"> C </option> <option value="50301"> L </option> </select>';
	}	
	return html;
}

/*
function fnSubmit()
{
	if (document.frmTerritory.Chk_ActiveFl.checked)
	{
		document.frmTerritory.Chk_ActiveFl.value = 'Y';
	}
	document.frmTerritory.submit();
}
*/
function fnLoadList(val)
{
if (val == '3301')
	{
		// document.all.list.innerHTML = document.all.Dist.innerHTML;
		document.all.cont.innerHTML = document.all.Set.innerHTML;
		var obj = document.frmTerritory.Cbo_Reason;
		for (var i=0;i<obj.length ;i++ )
		{
			val = obj.options[i].value;
			if (val == "3313")
			{
				obj.options[i].selected = true;
				break;
			}
		}
	}
	else if (val == '3302' || val == "3304") // 3302: Consignment - Items; 3304: Account Consignment - Items
	{
//		document.all.list.innerHTML = document.all.Dist.innerHTML;
		if (document.frmTerritory.hAction.value != 'PopOrd')
		{
			document.all.cont.innerHTML = document.all.PartNums.innerHTML;
		}
	}

}

function fnPrint(val)
{
	windowOpener("/GmPrintCreditServlet?hAction=PRINT&hRAID="+val,"Con1","resizable=yes,scrollbars=yes,top=250,left=200,width=800,height=300");  
}

function fnInitiate()
{
	var objType = document.frmTerritory.Cbo_Type;
	var objReason = document.frmTerritory.Cbo_Reason;
	if(objType.value != '3304'){ //3304:Account Consignment - Items
		fnValidateDropDn('cbo_EmpName',message_operations[607]);
	}
 	//objType.value: 3301 - Consignment - Sets ; 3302- Consignment - Items  and objReason.value:3313-Consignment Return
	//when type is Consignment - Sets, Reason can be: Consignment Return ,Broken, Field Corrective Action
 	if ((objType.value == '3301')&&(objReason.value !='3313' && objReason.value !='3311' && objReason.value !='3253' && objReason.value !='26240385')){
	Error_Details(message[99]);
	}
	//when type is Consignment - items, Reason can be : broken ,Consignment Return , Field Corrective Action, Wear and Tear (objReason.value:3319)
	else if (( objType.value == '3302')&&(objReason.value !='3313' && objReason.value !='3311'&& objReason.value !='3319' && objReason.value != '3253' && objReason.value !='26240385')){
		Error_Details(message_operations[601]);
	}//when type is Consignment - Sets, Reason can be: Consignment Return ,Broken
	else if((objType.value != '3304')&&(objReason.value =='3320')){
		Error_Details(message_operations[602]);
	}
 	if ((objType.value == '3304')&&(objReason.value !='3320')){ // 3304: Account Consignment - Items; 3320: Account Consignment Return
		Error_Details(message_operations[603]);
	}

 	if ((objReason.value == '3253') && (OrderTypevalue != 'Y')){ //3253: QA Evaluation
  		Error_Details(message_operations[604]);
  	}
 	
	if ((objReason.value == '26240385') && (OrderTypevalue != 'Y')){ //26240385: Field Corrective Action
		Error_Details(message_operations[752]);
  	}
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	if(fnValidateInitiateInput())
	 {	   
		fnMakeLoadString();
		if(fnMakeInitiateString())
		  {		
		      if(partNos != '')
	      	{
		        if(!window.confirm(Error_Details_Trans(message_operations[338],partNos)))
		           {
		               return false;
		           }
	    	 }   
		  			
			document.frmTerritory.hAction.value = "Initiate";
			fnStartProgress("Y");
			document.frmTerritory.submit();
		}
	 }	
}

function fnReload()
{
	var type = document.frmTerritory.Cbo_Type.value;
	
	if(fnValidateLoadInput())
	 {
	    fnMakeLoadString();	
        document.frmTerritory.hAction.value = "Reload";
        if (type != "3304") { // Account Item Consignment
			fnValidateDropDn('cbo_EmpName',message_operations[607]);
		}
		if (ErrorCount > 0)
		{
			Error_Show();
			Error_Clear();
			return false;
		}
		fnStartProgress();
		document.frmTerritory.submit();
	 }	
}

function fnValidateLoadInput()
 {
 var len =   document.frmTerritory.hRowCnt.value;
 var type = document.frmTerritory.Cbo_Type.value;
// //alert("length : "+len); 
 	 var setNumTxtFeild;
	 var setQtyTxtFeild;
 	 var setNumTxtFeildOuter;
	 var setNumTxtFeildInner;
	 var duplicateFlg = false;
 
 
 var objType = document.frmTerritory.Cbo_Type;
 var objReason = document.frmTerritory.Cbo_Reason;
 
	if (objType.value == '0')
	{
		Error_Details(message[90]);
	}

 var objDist = document.frmTerritory.Cbo_DistId;
 var objAcct = document.frmTerritory.Cbo_accountId;
 
	if ((objDist.value == '0'&& type != '3304') || (objAcct.value == '0'&& type == '3304'))
	{//3304: Account consignment
		Error_Details(message[92]);
	}
	
 var objEmp = document.frmTerritory.cbo_EmpName;
	if (objDist.value == '01' && objEmp.value == '0')
	{
		Error_Details(message[93]);
	}
	
 var objPartNums  = document.frmTerritory.Txt_PartNums;
 
  if((objType.value == 3302 || objType.value == 3304) && objPartNums.value == '')	
   {
     Error_Details(message[94]);
   }
 
 var chkNull = true;
 
  if(objType.value == 3301)	
   {
	if(objPartNums.value != '')
 	{
 		chkNull = false;
 	}
 	
 	if(chkNull)
 	{	
	 	for(var count = 0; count < len ; count++)
	   	{
	    setNumTxtFeild = eval("document.frmTerritory.Txt_SetID"+count);
	    ////alert(" setNumTxtFeild "+setNumTxtFeild+", Txt_SetID"+count); 
	    if(setNumTxtFeild.value != '')
	     {
	        chkNull = false;
	     }	   
	    } 
 
     if(chkNull)
     {
         Error_Details(message[95]);
     }	   	   
   }
 }
 
 
 for(var count = 0; count < len ; count++)
   {
    setNumTxtFeild = eval("document.frmTerritory.Txt_SetID"+count);
    setQtyTxtFeild = eval("document.frmTerritory.Txt_Qty"+count);
    
   // //alert(setNumTxtFeild + ", "+setQtyTxtFeild);
    
    if(setNumTxtFeild.value != '' && setQtyTxtFeild.value == '')
     {
       setQtyTxtFeild.value = "1";
     }
   }

   for(var count = 0; count < len ; count++)
   {
   var countSet = 0;
	    setNumTxtFeildOuter = eval("document.frmTerritory.Txt_SetID"+count);
	    	  
	    if(setNumTxtFeildOuter.value != '')
	     {
	    //  //alert(setNumTxtFeildOuter.value);
	       for(var countInner = 0; countInner < len ; countInner++)
	  	 	{  
	          setNumTxtFeildInner = eval("document.frmTerritory.Txt_SetID"+countInner);
	          
		          if(setNumTxtFeildOuter.value ==  setNumTxtFeildInner.value)	            
		          {
		         //  //alert(setNumTxtFeildOuter.value + ", " + setNumTxtFeildInner.value);
		            countSet++;
		            if(countSet == 2)
		              {
			            duplicateFlg = true;
			            break;  
			          }  
		          }
	        }       
	     }     
	     
	     if(duplicateFlg)
	      {
	        Error_Details(message[97]);
	        break;
	      }
    }
   
 if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}    
 return true;	 
 }

 function fnAddToCart()
{
	document.frmTerritory.hAction.value = "GoCart";
	document.frmTerritory.submit();
}

function fnRemoveItem(val)
{
	document.frmTerritory.hDelPartNum.value = val;
	document.frmTerritory.hAction.value = "RemoveCart";
	document.frmTerritory.submit();	
}

function fnUpdateCart()
{
	document.frmTerritory.hAction.value = "UpdateCart";
	document.frmTerritory.submit();	
}

function fnPlaceOrder()
{
	var objType = document.frmTerritory.Cbo_Type;
	if (objType.value == '0')
	{
		Error_Details(message[90]);
	}

	var objReason = document.frmTerritory.Cbo_Reason;
	if (objReason.value == '0')
	{
		Error_Details(message[91]);
	}
	
	var objDist = document.frmTerritory.Cbo_Id;
	if (objDist.value == '0')
	{
		Error_Details(message[92]);
	}

	var objEmp = document.frmTerritory.cbo_EmpName;
	if (objDist.value == '01' && objEmp.value == '0')
	{
		Error_Details(message[93]);
	}
	
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}

	document.frmTerritory.hAction.value = "PlaceOrder";
	document.frmTerritory.submit();	
}

function fnSelectAll(obj)
{
	var len = document.frmTerritory.length.value;
    var objId;
	for(var n=0;n<len;n++)
	{
		objId = eval("document.frmTerritory.rad"+n); // The checkboxes would be like rad0, rad1 and so on
			if(objId)
			{			
				if(obj.checked)
				{
					objId.checked = true;
				}else
				{
					objId.checked = false;
				}
			}
	 }		
}

function fnOnCheckRad(count)
{
    var objId;
    var objTxtRetQty;
	objId = eval("document.frmTerritory.Chk_SelectAll");
	objId.checked = false;
	objTxtRetQty = eval("document.frmTerritory.Txt_RetQty"+count);
	objTxtRetQty.value="0";
}

function fnChangeDisplay()
 {
  
  var type = document.frmTerritory.Cbo_Type.value;
  
  var rowCnt = document.frmTerritory.hRowCnt.value; 
  var setNumTxtFeild;
  var setQtyTxtFeild;
  document.frmTerritory.Btn_AddRow.disabled = false;
 
  if(type=="3300") //3300- Sales-Items 
  {
	  Error_Details(message_operations[606]);
	  Error_Show();
	  Error_Clear();
	  document.frmTerritory.Cbo_Type.value =0;
  }
  
  if(type == "3302" || type == "3304" ||type=="26240177")//Account Consignment Items
  { 
	 for(var i=0; i< rowCnt;i++)
	  {
	    setNumTxtFeild = eval("document.frmTerritory.Txt_SetID"+i);
	    setQtyTxtFeild = eval("document.frmTerritory.Txt_Qty"+i);
	   
//	    //alert(setNumTxtFeild +" , "+ setQtyTxtFeild);
	   // if(setNumTxtFeild != undefined)
	     {  
		    setNumTxtFeild.disabled = true;
		    setQtyTxtFeild.disabled = true;        
		 }   
	  }
	  document.frmTerritory.Btn_AddRow.disabled = true;
  }
  else if(type == "3301" || type =="26240178")
   {   
     for(var i=0; i< rowCnt;i++)
	  {
	    setNumTxtFeild = eval("document.frmTerritory.Txt_SetID"+i);
	    setQtyTxtFeild = eval("document.frmTerritory.Txt_Qty"+i);	   
	    // //alert(setNumTxtFeild +" , "+ setQtyTxtFeild);
	    //   if(setNumTxtFeild != undefined)
	     	{  	
		    	setNumTxtFeild.disabled = false;
		    	setQtyTxtFeild.disabled = false;           
		    }
     }
     document.frmTerritory.Btn_AddRow.disabled = false;
  }
  if(type == "3304"){//Account Item consignment
  		var objConsigneeBlk = eval('document.all.consignee_blk.style');
  		objConsigneeBlk.display = 'none';
  		var objAccountBlk = eval('document.all.account_blk.style');
  		objAccountBlk.display = 'block';
  		document.all.Cbo_Reason.value = '3320';// 3320:Account Consignment Return
  		document.all.cbo_EmpName.disabled = true;
  }
  
  if(type != "3304"){
  		var objConsigneeBlk = eval('document.all.consignee_blk.style');
  		objConsigneeBlk.display = 'block';
  		var objAccountBlk = eval('document.all.account_blk.style');
  		objAccountBlk.display = 'none';
  		document.all.cbo_EmpName.disabled = false;
  }
  
 }

 function fnCallEmp()
{
	var objval = document.frmTerritory.Cbo_DistId.value;
	var objEmp = document.frmTerritory.cbo_EmpName;
	var j = 0;
	if (objval == '01')
	{
		objEmp.disabled = false;
		objEmp.options.length = 0;

		objEmp.options[0] = new Option("[Choose One]","0");	
		
		
		for (var i=0;i<EmpLen;i++)
		{
		arr = eval("EmpArr"+i);
		
			arrobj = arr.split(",");
			id = arrobj[0];		
			nameVal = arrobj[1];	 //  nameVal = arrobj[1] instead of name = arrobj[1] changes in PC-3656 Edge Browser fixes for Logistics Module	
			j++;			
			objEmp.options[j] = new Option(nameVal,id);
		}
	}else if (objval != '0')
	{
		objEmp.disabled = false;
		objEmp.options.length = 0;

		objEmp.options[0] = new Option("[Choose One]","0");	
				
		for (var i=0;i<RepLen;i++)
		{
			arr = eval("RepArr"+i);
		
			arrobj = arr.split(",");
			id = arrobj[0];		
			nameVal = arrobj[1];
			did = arrobj[3];
	
				if (did == objval)
				{
					j++;			
					objEmp.options[j] = new Option(nameVal,id);    //  nameVal = arrobj[1] instead of name = arrobj[1] changes in PC-3656 Edge Browser fixes for Logistics Module
				}			
		}		
	}
	else{
	 	objEmp.disabled = true;
		objEmp.options[0].selected = true;
	}
}

function fnSetLookup()
{
   windowOpener("/GmSetReportServlet?hOpt=SETRPT","Search","resizable=yes,scrollbars=yes,top=340,left=420,width=600,height=435");
}


