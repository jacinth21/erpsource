// Function to calculate the discount based on the discount amount given in the Discount field
function fnCalculateDisc(){
	blDiscFlag=true;
	var objDiscountPercentage = fnDiscountPercentage();
	var cnt = document.frmCart.hRowCnt.value;
	var strOpt = parent.document.all.hOpt.value;
	var listPrice = 0;
	var pobj;
	var  strDiscountIndex = parent.document.all.hDiscountIndex.value;
	for (i=0;i<cnt;i++)
	{
		pobj = eval("document.frmCart.Txt_PNum"+i);
		//var eaPrice = parseFloat(RemoveComma(priceObj.value));
		if(pobj.value != '')
		{
			
			// MNTTASK-4744 - Discount Calculation : Added Price EA column in the UI. Always we have to get txt_price value to DB.
			if(vcountryCode == 'en'){
				priceEAObj = eval("document.frmCart.Txt_unit_price"+i);
				listPrice= RemoveComma(eval("document.frmCart.Txt_unit_price"+i).value);
			}else{
				priceEAObj = eval("document.frmCart.Txt_BasePrice"+i);
				listPrice= RemoveComma(eval("document.frmCart.Txt_BasePrice"+i).value);
			}
			
			if(objDiscountPercentage > 0 && listPrice != 0)
				var DisEAPRice = (parseFloat(listPrice) - (parseFloat(listPrice) * objDiscountPercentage));
			else
				DisEAPRice = eval("document.frmCart.Txt_Price"+i).value;
			
			var obj = eval("document.all.Txt_Price"+i);
			obj.value = formatNumber(DisEAPRice);
			var hprice = eval("document.all.Lbl_PriceEA"+i)
			if(hprice != undefined && hprice != '') 
				hprice.value = formatNumber(DisEAPRice);
			fnCalExtPrice(priceEAObj,i);
		}
	}
	eval("parent.document.frmPhOrder.Txt_Quote"+strDiscountIndex).disabled = true;
	document.frmCart.Btn_Calculate.value = resetBtnNm;
}

// Function to reset the discount calculated
function fnResetDisc(){
	var pobj = '';
	var netPrice = '';
	var netPriceObj;
	var  strDiscountIndex = parent.document.all.hDiscountIndex.value;
	for (i=0;i<cnt;i++){
		pobj = eval("document.frmCart.Txt_PNum"+i);
		if(pobj.value != ''){
			netPrice = eval("document.frmCart.hUnitPartPrice"+i);
			netPriceObj = eval("document.frmCart.Txt_unit_price"+i);
			netPriceObj.value = netPrice.value;
			fnCalExtPrice(netPriceObj,i,'Y');
		}
	}
	eval("parent.document.frmPhOrder.Txt_Quote"+strDiscountIndex).value = '' ;
	eval("document.frmCart.Btn_Calculate").disabled = true;
	eval("parent.document.frmPhOrder.Txt_Quote"+strDiscountIndex).disabled = false;
	document.frmCart.Btn_Calculate.value = applyBtnNm;
}