
function fnOnPageLoad()
{
	if(objGridData.indexOf("cell", 0)!=-1){ 
		gridObj = initGridWithDistributedParsing('dataGrid',objGridData);
	}
}
function fnLoad()
{
	fnValidateTxtFld('filterData',' Users/Groups ');
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	document.frmUserGroupMap.grpType.value=grpType;
	document.frmUserGroupMap.strOpt.value = 'fetch';
	fnStartProgress();
	document.frmUserGroupMap.submit();
}
function fnAdd()
{
		gridObj.editStop();// retun value from editor(if presents) to cell and closes editor
		var gridrowid;
		var grpsid='';
		var inputString='';
		var gridrows =gridObj.getCheckedRows(2);
		var gridrowsarr = gridrows.toString().split(",");
		var columnCount = gridObj.getColumnsNum();
		for (var rowid = 0; rowid < gridrowsarr.length; rowid++) {
				gridrowid = gridrowsarr[rowid];
				mtype = gridObj.cellById(gridrowid, 0).getValue();
				id = gridObj.cellById(gridrowid, 1).getValue();
				name = gridObj.cellById(gridrowid, 3).getValue();
				desc = gridObj.cellById(gridrowid, 4).getValue();
				grpsid=gridObj.getUserData(gridrowid,"hGrpId");
				inputString+=mtype+'^'+id+'^'+name+'^'+desc+ '^' + grpsid + '|';
		}
		window.opener.fnAddUserGroup(inputString);
		window.close();	
}
function fnEditDetails(type)
{
	if(type == "SECGRP"){
		strType = "SGREPORT";
	}else if(type == "MODRPT"){
		strType ="MREPORT";
	}
	windowOpener("/gmAccessControlGroup.do?strOpt="+strType,"Details","resizable=yes,scrollbars=yes,top=150,left=200,width=750,height=550");
}