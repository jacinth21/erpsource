function fnSubmit(){
	var frm = document.frmCodeGroup;
	fnValidate();
	var max = eval(frm.lookupNeeded.value);
	if(max>50){
		Error_Details(message[503]);
	}
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	frm.strOpt.value='save';
	frm.strChk.value='';
	frm.codeGrp.disabled=false;
	fnStartProgress();
	frm.submit();	
}

function fnCheck(){
	var frm = document.frmCodeGroup;
	fnValidateTxtFld('codeGrp',' Code Group '); 
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	frm.strOpt.value='check';
	fnStartProgress();
	frm.submit();	
}

function fnDisable(){
	var frm = document.frmCodeGroup;
	if(frm.strOpt.value=='edit' || frm.strChk.value=='no'){
		frm.codeGrp.disabled=true;
	}
}

function fnValidate()
{
	
	fnValidateTxtFld('codeGrp',' Code Group '); 
	fnValidateTxtFld('codeDes',' Code Description ');
	fnValidateTxtFld('lookupNeeded',' No.of Lookup Needed ');
	
}

function fnReset(){
	var frm = document.frmCodeGroup;
	frm.strOpt.value='';
	frm.codeGrp.value='';
	frm.strChk.value='';
	frm.submit();
}

function fnEnter(key){
	
	if(key.keyCode ==  13){
		fnLoad();
	}

	
}
function fnOnPageLoad() {
	if(gridObjData!='')
	{
		mygrid = initGrid('dataGridDiv',gridObjData);		
		mygrid.enableTooltips("false,false,false,false,false,false"); 
		mygrid.attachHeader('#rspan,#text_filter,#numeric_filter,#select_filter,#rspan,#select_filter');
	}
}
function fnLoad(){
	var frm = document.frmCodeGroup; 
	fnValidateTxtFld('codeGrp',' Code Group Name');
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	document.frmCodeGroup.strOpt.value="load";
	fnStartProgress();
	document.frmCodeGroup.submit();
}

function fnEdit(str){
	var frm=document.frmCodeGroup;
	frm.strOpt.value="edit";
	frm.codeGrpID.value=str;
	frm.submit();
}

function fnExport(){
	if(mygrid!=undefined && mygrid!=''){
		mygrid.setColumnHidden(0,true);
		mygrid.toExcel('/phpapp/excel/generate.php');
		mygrid.setColumnHidden(0,false);
	}
}

function fnEditLookup(group){
	var frm=document.frmCodeGroup;
	frm.action='/gmCodeLookupSetup.do?codeGrp='+group;
	frm.strOpt.value="report";
	frm.submit();
}
function fnVoid(str){
	document.frmCodeGroup.action="/GmCommonCancelServlet?hTxnName="+str+"&hCancelType=VCDGR&hTxnId="+str;
	document.frmCodeGroup.submit();
}