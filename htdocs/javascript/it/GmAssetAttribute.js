function fnSubmit()
{
	var frm=document.frmAssetAtt;
	fnValidate();

	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	frm.assetID.disabled=false;
	frm.strOpt.value='save';
	fnStartProgress();
	frm.submit();
}

function fnEnter()
{
		if (event.keyCode == 13)
		{ 
			fnReload();
		}
}
function fnLoad()
{
	var frm=document.frmAssetAtt;
	fnValidate();

	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	frm.assetID.disabled=false;
	frm.strOpt.value='load';
	
	frm.submit();
}
function fnValidate()
{
	var frm = document.frmAssetAtt; 
	fnValidateTxtFld('assetID',' Asset ID '); 
	fnValidateDropDn('attTyp',' Attribute type ');
}
function fnVoid()
{
	 var frm = document.frmAssetAtt; 
	 frm.action = "/GmCommonCancelServlet";
	 frm.hTxnId.value = frm.attId.value;
	 frm.submit();
}

function fnReset(){
	var frm=document.frmAssetAtt;
	frm.strOpt.value='reset';
	frm.submit();
}
function fnDisable(){
	var frm=document.frmAssetAtt;
	var str=frm.strOpt.value;
	if(str=='edit'||str=='editload'){
		frm.assetID.disabled=true;
	}
}
function fnDropdown(){
	var frm=document.frmAssetAtt;
	var str=frm.strOpt.value;
	if(str=='edit'){
		frm.assetID.disabled=false;
		frm.strOpt.value='load';
		frm.submit();
		
	}

}
//methods for report
function fnReload()
{
	var frm=document.frmAssetAtt;
	frm.strOpt.value = 'reload';
	fnStartProgress();
	frm.submit();
}

function fnOnPageLoad()
{	  
	if (objGridData != '')
	{
		gridObj = initGrid('astAttData',objGridData);
		gridObj.enableTooltips("false,true,true,true,true");
	}
}

function fnEdit(id)
{
	var frm=document.frmAssetAtt;
	frm.attId.value = id;
	frm.strOpt.value = 'editload';
	frm.action = "/gmAssetAttribute.do";
	frm.submit();
}