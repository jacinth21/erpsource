var gridObj;
function fnOnPageLoad() {
	if (objGridData != '') {
		gridObj = initGridData('codelookupexclreport',objGridData);
	}
}


function initGridData(divRef,gridData)
{
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.init();		
	gObj.loadXMLString(objGridData);	
	return gObj;	
}
function fnExportExcel() {
	gridObj.toExcel('/phpapp/excel/generate.php');
}

function fnLoad() {
	var codeIDs = TRIM(document.frmCodeGroup.codeid.value);
	var regexp =  (/^(([0-9](,)?)*)+$/);
	var codeval = (/[^a-zA-Z0-9 ]/);
	fnValidateTxtFld('codeid', lblCodeID);
	
	if(codeIDs!="")
	{
	    if(!codeIDs.match(regexp))
	    {
		    Error_Details(message[5278]);
	    }
	    else if(codeIDs.match(codeval)){
	    	Error_Details(message[5279]);
	    }
	}
	if (ErrorCount > 0){
	    Error_Show();
		Error_Clear();
		return false;
	} 
			  document.frmCodeGroup.strOpt.value = 'Load';
	          document.frmCodeGroup.action="/gmCodeLkpExcln.do?method=fetchCodeLookupExclnDtls&codeid="+codeIDs;
			  fnStartProgress();
			  document.frmCodeGroup.submit();
		
}

function fnSubmit()
{
	var frm=document.frmCodeGroup;
	var vcodeid = frm.codeid.value;
	var vtype = frm.type.value;
	var vCompany = fnCheckSelections();
	var v_code = vcodeid+ ",";	
	var sublen = vcodeid.length;
	var vNum =  /^[0-9,]+$/;
	
	fnValidateTxtFld('codeid', message[5263]);
	if(vCompany == ""){
		Error_Details(message[5264]);				
	}	
	fnValidateDropDn ('type' ,message[5265])
	if(vcodeid !='' && sublen>300){
    	Error_Details(message[5266]);
	}
	if(vcodeid!=="" && !vcodeid.match(vNum)){
		Error_Details(message[5267]);
    }
	
	if (ErrorCount > 0){
	    Error_Show();
		Error_Clear();
		return false;
	} 
	else
			{
			frm.companyStr.value = vCompany;
			frm.codeid.value=v_code;			
		 	frm.action = "/gmCodeLkpExcln.do?method=saveCodeLookupExclnValues&strOpt=save";
			fnStartProgress();
		    frm.submit();
		    }
}



function fnReset(){
	var frm = document.frmCodeGroup;	
	frm.codeid.value='';
	frm.type.value='';
	objCheckCompanyArr = document.frmCodeGroup.checkSelectedCompany;
	objCheckCompanyArrLen = objCheckCompanyArr.length;
	for(i = 0; i < objCheckCompanyArrLen; i ++) 
	{	
		objCheckCompanyArr[i].checked = false;
	}
	
}

// fnCheckSelections used for the Checkbox purpose

function fnCheckSelections()
 {
	 var chkdvals=""; 
	 var unchkdvals ="";
	 var chk=0;	
	 var notchk=0;
	 var inputString="";

	objCheckCompanyArrLen = 0;
	setflag  = '';
	objSelAll = document.all.selectAll;
	
	objCheckCompanyArr = document.frmCodeGroup.checkSelectedCompany;
	if(objCheckCompanyArr) 
	{
		objCheckCompanyArrLen = objCheckCompanyArr.length;
		
		for(i = 0; i < objCheckCompanyArrLen; i ++) 
		{	
			if(!objCheckCompanyArr[i].checked)
				{
				 notchk++;
				}
		}
		
		if(notchk!=0)
			objSelAll.checked = false;
		
		
		
		
		for(i = 0; i < objCheckCompanyArrLen; i ++) 
		{	
			if(objCheckCompanyArr[i].checked)
		    {				    		   
				setflag = '1';
				chkdvals += objCheckCompanyArr[i].value+',';			
				chk ++;
				inputString= chkdvals + ",";
				if(chkdvals !='')
				inputString=inputString.substring(0,inputString.length-1);
		    }			
				else
			    {
			    	unchkdvals += objCheckCompanyArr[i].value+',';
					notchk ++;
					 }
		}
		 if(inputString.length>4000)
		 {
			 Error_Details(message_operations[517]);	
		 }
		
		if(setflag=="")
		 {		 
			 document.frmCodeGroup.hsetstatusNotcheck.value="true";
		 }
		 else
		 { 
		 	 document.frmCodeGroup.hsetstatusNotcheck.value="";
		 }
		 
		}
	return inputString;
}

function fnSelectAll(varCmd)
{				
				objSelAll = document.all.selectAll;
				if (objSelAll ) {
				objCheckCompanyArr = document.all.checkSelectedCompany;
					
				if ((objSelAll.checked || varCmd == 'selectAll') && objCheckCompanyArr)
				{
					objSelAll.checked = true;
					objCheckCompanyArrLen = objCheckCompanyArr.length;
					
					objCheckCompanyArr1 = document.frmCodeGroup.checkSelectedCompany;
					
					if (objCheckCompanyArr1.type == 'checkbox')
						{
							objCheckCompanyArr1.checked = true;
						}
													
					for(i = 0; i < objCheckCompanyArrLen; i ++) 
					{	
						objCheckCompanyArr[i].checked = true;
						
					}	 
				}
				else if (!objSelAll.checked  && objCheckCompanyArr ){
					objCheckCompanyArrLen = objCheckCompanyArr.length;
					
					objCheckCompanyArr1 = document.frmCodeGroup.checkSelectedCompany;
					if (objCheckCompanyArr1.type == 'checkbox')
						{
							objCheckCompanyArr1.checked = false;
						}
					
					for(i = 0; i < objCheckCompanyArrLen; i ++) 
					{	
						objCheckCompanyArr[i].checked = false;
					}
			
					}
				}
	}
