var split_At = '';
var w1,dhxWins;

function initGridWithDistributedParsing(divRef,gridData,footerArry){
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	if(footerArry != undefined && footerArry.length >0)
	gObj.attachFooter(footerArry);
	gObj.enableDistributedParsing(true);
	gObj.init();		
	gObj.loadXMLString(gridData);	
	return gObj;
}

function fnOnPageLoad(objGridData){
		gridObj = initGridWithDistributedParsing('usergroupdata',objGridData,'');
		gridObj.attachEvent("onEditCell",doOnCellEdit);
		if(grpType=='TSMD' || grpType=='92262'){
			
			gridObj.setColumnHidden(3,true);
			gridObj.setColumnHidden(4,true);
			gridObj.setColumnHidden(5,true);
			if(grpType=='92262'){
				
				gridObj.setColumnHidden(6,true);
				gridObj.setColumnHidden(7,true);
			}
		}else{
			
			gridObj.setColumnHidden(6,true);
			gridObj.setColumnHidden(7,true);
		}
		if(grpType !='92262'){
			
			gridObj.setColumnHidden(8,true);
		}
		//Hidden the access column based on the grpType
		if(grpType == 'SESG'){
			
			gridObj.setColumnHidden(3,true);
		}if(grpType == 'TSSG'){
		
			gridObj.setColumnHidden(4,true);
		}
			
		if(gridObj.cellById(0, 2).getValue()==''){
			gridObj.setRowHidden(0,true);
		}
		if(grpType=='TSMD' && gridObj.getRowsNum()>0 && gridObj.cellById(0, 2).getValue()!=''){
			document.getElementById("add_module").disabled=true;
		}		
		return false;
}

function doOnCellEdit(stage, rowId, cellInd, nValue, oValue) {	
	gridObj.setRowAttribute(rowId,"rowChanged","Y");
	return true;
}

function fnSubmit()
{
	
	gridObj.editStop();// retun value from editor(if presents) to cell and closes editor
	var gridrowid;
	var void_flag;
	var inputString='';
	//var gridrows =gridObj.getChangedRows(",");
	var gridrows =gridObj.getAllRowIds();
	var groupIds ='';
	if(gridrows.length==0){
		inputString = "";
	}
	else
	{
		var gridrowsarr = gridrows.toString().split(",");
		var columnCount = gridObj.getColumnsNum();
	
		
		for (var rowid = 0; rowid < gridrowsarr.length; rowid++) {
			gridrowid = gridrowsarr[rowid];
			
			if(gridObj.getRowAttribute(gridrowid,"rowChanged")=='Y')
			{
			groupIds+= fnCheckNull(gridObj.getUserData(gridrowid,'hgroupId'));
			inputString+=fnCheckNull(gridObj.getUserData(gridrowid,"hfunctionId"))+'^';
			inputString+=fnCheckNull(gridObj.getUserData(gridrowid,"haccessId"))+'^';
			inputString+=fnCheckNull(gridObj.getUserData(gridrowid,"hgroupId"))+'^';
			inputString+=fnCheckNull(gridObj.getUserData(gridrowid,"hgrpId"))+'^';
			inputString+=fnCheckNull(gridObj.getUserData(gridrowid,"hseqNo"))+'^';
					for(var col=0;col<columnCount;col++){
					
					if(col != 1 && col != 2 && col != 7 && col != 8 && col != 9)
					{
							if(grpType=='TSMD'){
								if(gridObj.cellById(gridrowid, 6).getValue()==""){
									Error_Clear();
									Error_Details("Sequence ID Should be given");
								}
							}
							if(col==0 && gridObj.cellById(gridrowid, col).getValue() !='1')							
								inputString+='0^';
							else
								inputString+=gridObj.cellById(gridrowid, col).getValue()+'^';
					}
					
				}
			inputString+=fnCheckNull(gridObj.getUserData(gridrowid,"hrowSts"));
			inputString+='|';
			groupIds+='|';
			}
		}
		if (ErrorCount > 0)
		{
			Error_Show();
			Error_Clear();
			return false;
		}
	
}	
	
	if(inputString.length>0)
	{
	document.frmUserGroupMap.hinputStr.value = inputString;
	document.frmUserGroupMap.hTaskNameStr.value = groupIds;
	document.frmUserGroupMap.strOpt.value = "OnSave";
	fnStartProgress();
	document.frmUserGroupMap.submit();
	}
	return false;
}

function fnOnFnListLoad()
{
	document.frmUserGroupMap.strOpt.value = "OnFuncLoad";
	document.frmUserGroupMap.submit();
	return false;
}
function fnlookup(grpID,rowid,groupnm)
{
	if(	grpID==null || grpID==undefined)
	{
		grpID='';
	}
	document.frmUserGroupMap.rowId.value=rowid;
	windowOpener("/gmUserNavigation.do?strOpt=SEQLOOKUP&userGroup="+grpID+"&userGroupName="+groupnm,"Lookup","resizable=yes,scrollbars=yes,top=150,left=150,width=350,height=600");
	
}
function fnaddseqno(seqid)
{
	
	var rowid='';
	rowid = document.frmUserGroupMap.rowId.value;
	rowid=rowid-1;
	gridObj.cellById(rowid, 6).setValue(seqid);
	gridObj.setRowAttribute(rowid,"rowChanged","Y");

}

function fnAddGroup()
{
	var funcid = '';
	funcid=document.frmUserGroupMap.funcID.value;
	windowOpener("/gmUserGroupMap.do?method=fetchAddUserGroup&funcID="+funcid,"AddUserGroup","resizable=yes,top=150,left=150,width=750,height=500");
	
}
function fnAddModule()
{
	var funcid = '';
	funcid=document.frmUserGroupMap.funcID.value;
	windowOpener("/gmUserGroupMap.do?method=fetchAddUserGroup&grpType=92265&funcID="+funcid,"AddUserGroup","resizable=yes,top=150,left=150,width=750,height=500");
	
}
function fnAddSecGroup()
{
	var funcid = '';
	funcid=document.frmUserGroupMap.funcID.value;
	windowOpener("/gmUserGroupMap.do?method=fetchAddUserGroup&grpType=92264&funcID="+funcid,"AddUserGroup","resizable=yes,top=150,left=150,width=750,height=500");
	
}

function fnAddUserGroup(addData)
{
	
	var dataString = '';
	var newRowID = '';
	var funcid = '';
	funcid=document.frmUserGroupMap.funcID.value;
	if(addData.length> 0){
		dataString = addData;
		var hTotRows=0;
		hTotRows=gridObj.getRowsNum();
		while(dataString.indexOf('|') > 0){			
			rowString = dataString.substring(0, dataString. indexOf('|'));
	        mtype        = rowString.substring(0,rowString.indexOf('^'));
	        rowString = rowString.substring(rowString.indexOf('^')+1);
	        id        = rowString.substring(0,rowString.indexOf('^'));
	        rowString = rowString.substring(rowString.indexOf('^')+1);
	        name      = rowString.substring(0,rowString.indexOf('^'));
	        rowString = rowString.substring(rowString.indexOf('^')+1);
	        desc      = rowString.substring(0,rowString.indexOf('^'));
	      //  if(desc=="^")
	        //	desc='';
	        rowString = rowString.substring(rowString.indexOf('^')+1);
	        
	        grpid     = rowString;
	        if(!fnIsMapExist(id)){
		        newRowID  = gridObj.getUID();
			    gridObj.addRow(hTotRows,'');
			    gridObj.setUserData(hTotRows,"hgroupId",id);
	        	gridObj.setUserData(hTotRows,"hgrpId",grpid);
			    gridObj.setUserData(hTotRows,"hfunctionId",funcid);
			    gridObj.setUserData(hTotRows,"hrowSts",mtype);
			    gridObj.setCellExcellType(hTotRows,0,'ro');
			    var frmgrpType = document.frmUserGroupMap.grpType.value; 
			    gridObj.cellById(hTotRows, 0).setValue("<a href=\"#\" onclick=\"gridObj.deleteRow(" + hTotRows + ");\"><img src=\"/images/close.gif\" height=\"12\" width=\"10\" alt=\"Remove Item " + name + "\" border=\"0\"></a>");
		        if(mtype=='G')
		        {
		        	gridObj.cellById(hTotRows, 1).setValue("<a href=\"#\" onclick=\"fnUserLookup(\'"+id+ "\',\'"+frmgrpType+"\');\">" + name + "</a>");
		        }else{
		        	gridObj.cellById(hTotRows, 1).setValue(name);
		        }
		        gridObj.cellById(hTotRows, 2).setValue(desc);
		        gridObj.cellById(hTotRows, 3).setValue("Y");
		        gridObj.cellById(hTotRows, 4).setValue("Y");
		        gridObj.cellById(hTotRows, 5).setValue("Y");
		        gridObj.setRowAttribute(hTotRows,"rowChanged","Y");
		        if(frmgrpType == 'TSMD'){
			        if(mtype=='G' && document.frmUserGroupMap.funType.value=='92262')
			        {
			        	gridObj.cellById(hTotRows, 7).setValue('<a href="#" onClick="return fnlookup(\''+id+'\',\''+ (hTotRows+1) +'\',\''+name+'\');"><img src="/images/L.jpg" height="12" width="10" alt="Sequence Lookup for ' + name + '"  border="0"></a>');
			        }else if(document.frmUserGroupMap.funType.value=='92262'){
			        	//gridObj.setCellExcellType(hTotRows,6,'ro');
			        	gridObj.cellById(hTotRows, 7).setValue('<a href="#" onClick="return fnlookup(\''+grpid+'\',\''+ (hTotRows+1) +'\',\''+name+'\');"><img src="/images/L.jpg" height="12" width="10" alt="Sequence Lookup for ' + name + '" border="0"></a>');
			        }
		        }
	        }
	        //gridObj.cells(newRowID, 0).setDisabled(true);
	        dataString = dataString. substring(dataString.indexOf('|')+1);
	        hTotRows=hTotRows+1;
	    }
	}
}

function fnIsMapExist(id){
	var gridrowid;
	var gridrows =gridObj.getAllRowIds();
	var gridrowsarr = gridrows.toString().split(",");
	for (var rowid = 0; rowid < gridrowsarr.length; rowid++) 
	{
		gridrowid = gridrowsarr[rowid];
		var mappedid = fnCheckNull(gridObj.getUserData(gridrowid,'hgroupId'));
		if(id == mappedid)
		{
			return true;
		}		
	}
	return false;
}

function fnCheckNull(data)
{
	if(data==null || data=='null')
		return '';
	else
		return data;
}
function fnUserLookup(id,type){
	if(type == "TSMD"){
		strType ="SMREPORT";
		windowOpener("/gmUserGroup.do?haction=hide&strOpt="+strType+"&groupName="+id,"Lookup","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
	}else{
		//strType ="ReloadReport"; 
		windowOpener("/gmUserGroupMap.do?method=fetchGroupUserLookup&grpId="+id,"UserLookup","resizable=yes,top=150,left=150,width=730,height=550");
	}	
}

function fnToggle(strType){
	var str = document.frmUserGroupMap.funcID.value;
	document.frmUserGroupMap.action="\gmUserGroupMap.do?method=fchFunctDetails&strOpt=OnFuncLoad&funcID="+str + "&grpType=" + strType;
	document.frmUserGroupMap.submit();
}

//PMT-26730 System manager Changes
function fnExport()
{	
	gridObj.deleteColumn(5);
	gridObj.toExcel('/phpapp/excel/generate.php');
}