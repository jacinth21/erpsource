// global column variable are
var codeId_rowId = '';
var hCodeId_rowId = '';
var codeName_rowId = '';
var codeGrp_rowId = '';
var codeAlt_rowId = '';
var codeCType_rowId ='';
var codeSeq_rowId = '';
var codeActive_rowId = '';
var codeLock_rowId ='';

function fnOnPageLoad(){
		if (objGridData != '')
		{
			mygrid = initGrid('mygrid_container',objGridData);
			mygrid.enableTooltips("true,true,false,true");
			mygrid.enableMultiselect(true);
			mygrid.enableBlockSelection(true); 
			mygrid.attachEvent("onKeyPress", keyPressed);
			mygrid.attachEvent("onEditCell",doOnCellEdit);
			// to set the column index
			codeId_rowId = mygrid.getColIndexById("codeId");
			hCodeId_rowId = mygrid.getColIndexById("hcodeId");
			codeName_rowId = mygrid.getColIndexById("name");
			codeGrp_rowId = mygrid.getColIndexById("group");
			codeAlt_rowId = mygrid.getColIndexById("altname");
			codeCType_rowId = mygrid.getColIndexById("contolType");
			codeSeq_rowId = mygrid.getColIndexById("seqno");
			codeActive_rowId = mygrid.getColIndexById("activeflag");
			codeLock_rowId = mygrid.getColIndexById("vLockflag");
			//fnHidden();
			mygrid.setColumnHidden(codeLock_rowId,true);
		}	
}

function fnHidden(){
	
	var frm = document.frmCodeLookupSetup;
	var stropt = frm.strOpt.value;
	
	if(stropt=='void'){
		mygrid.setColumnHidden(0,true);	
		mygrid.setColumnHidden(1,true);	
		mygrid.setColumnHidden(3,true);
		mygrid.setColumnHidden(4,true);
		mygrid.setColumnHidden(6,true);	
		mygrid.setColWidth(5,"150");
	}else{
		alert(stropt);
	}
	mygrid.setColumnHidden(7,true);	
	mygrid.setColumnHidden(8,true);	
}

function doOnCellEdit(stage, rowId, cellInd, nValue, oValue){
	
	var frm = document.frmCodeLookupSetup;
	var stropt = frm.strOpt.value;
	if(stropt=='void'&& eval(stage)==0){
		var lockfl=mygrid.cellById(rowId,codeLock_rowId).getValue();
		if (((cellInd== codeName_rowId)||(cellInd== codeActive_rowId))&&(eval(lockfl)==1)){
		 	Error_Clear();
			Error_Details(message[500]);				
			Error_Show();
			Error_Clear();
			return false;
		}
	}
	//
	if (stage == 2 && cellInd == codeName_rowId && nValue != oValue){
		mygrid.setCellTextStyle(rowId,cellInd,"color:black;font-weight:normal;");
	}
     return true;
 }

function fnSubmit(){
	var gridrows =mygrid.getAllRowIds(",");	
	if(gridrows==undefined || gridrows==null || gridrows=="") {
		Error_Clear();
		Error_Details(message[504]);				
		Error_Show();
		Error_Clear();
	}else{
		var frm=document.frmCodeLookupSetup;
		mygrid.editStop();
	var gridrowsarr = gridrows.toString().split(",");
	var arrLen = gridrowsarr.length;
	var isInValidRe =false;
	var strInputString = '';
	var codegrp=frm.codeGrp.value;
	var nonNumSeqNoStr = '';
	var emptyNameStr = '';
	
	for (var i=0;i<arrLen;i++)
		{
	        rowId=gridrowsarr[i];
	        seqno=mygrid.cellById(rowId, codeSeq_rowId).getValue();
	        codeid=mygrid.cellById(rowId, codeId_rowId).getValue();
	        name=mygrid.cellById(rowId, codeName_rowId ).getValue();
	        hcodeid=mygrid.cellById(rowId, hCodeId_rowId ).getValue();
	        if(name==""){
	        	emptyNameStr = emptyNameStr + codeid +', ';
	        	mygrid.setCellTextStyle(rowId,codeName_rowId,"color:red;font-weight:bold;border:2px solid red;");
	        }
			altname = mygrid.cellById(rowId, codeAlt_rowId).getValue();
			controltype = mygrid.cellById(rowId, codeCType_rowId).getValue();
			activeflag=mygrid.cellById(rowId, codeActive_rowId).getValue();
			lockflag=mygrid.cellById(rowId, codeLock_rowId).getValue();
			if(isNaN(seqno)){
				nonNumSeqNoStr = nonNumSeqNoStr + codeid +', ';
			}
			
			strInputString = strInputString+hcodeid+'^'+codegrp+'^'+name+'^'+altname+'^'+controltype+'^'+activeflag+'^'+lockflag+'^'+seqno+'|';
		}
	if(emptyNameStr !=''){
		Error_Details(message[10591]);
	}
	if(nonNumSeqNoStr !=''){
		nonNumSeqNoStr = nonNumSeqNoStr.substring(0,(nonNumSeqNoStr.length)-2);
		Error_Details(Error_Details_Trans(message[10592],nonNumSeqNoStr));
	}

	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	
	frm.inputString.value = strInputString;
	var stropt = frm.strOpt.value;
	if(stropt=='void' || stropt =='vsave'){
		frm.strOpt.value = 'vsave';
	}else{
		frm.strOpt.value = 'save';
	}
	fnStartProgress();
	frm.submit();	
	}
}

function fnGScript(){
	var frm=document.frmCodeLookupSetup;
	mygrid.editStop();
	var pophtml = '';
	var gridrows = '';
	if(frm.scriptOpt[0].checked){
		gridrows = mygrid.getAllRowIds();
	}else{
		gridrows = mygrid.getSelectedId();
	}
	if(gridrows==undefined || gridrows==null){
		Error_Clear();
		Error_Details(message[502]);				
		Error_Show();
		Error_Clear();
	}else{
	var gridrowsarr = gridrows.toString().split(",");
	var arrLen = gridrowsarr.length;
	var strInputString = '';

	pophtml = '<html><head><title>Globus Medical-Code Lookup Generate Script</title><link rel="stylesheet" type="text/css" href="styles/GmCommon.css"></head><body topmargin=10 leftmargin=10 rightmargin=10><form name=frmError onSubmit=window.close();><table>';
	pophtml+= '<tr><td>&nbsp;</td></tr>';
	pophtml+= '<tr><td align="center"><input type="button" class="button" value="Close" onClick="javascript:window.close();"></td></tr>';
	pophtml+= '<tr><td>&nbsp;</td></tr><tr><td>';
	//For PMT-24666 Master table T901B Insert Script
	strInputString = 'INSERT INTO t901b_code_group_master<br>(C901B_CODE_GRP_ID, C901B_CODE_GRP , C901B_CODE_DESCRIPTION ,C901B_LOOKUP_NEEDED, C901B_CREATED_BY , C901B_CREATED_DATE)<br>VALUES <br>(';
	strInputString += 's901b_code_group_master.NEXTVAL'+',\''+strCodeGrp+'\',\''+strCodeDesc+'\',\'\',\''+strUserId+'\',CURRENT_DATE);'+'<br><br>';
	pophtml+= '<tr><td>'+strInputString+'</td></tr>';
	pophtml+= '<tr><td  style="border-bottom: 1px solid #cccccc;"></td></tr>';
	pophtml+= '<tr><td>&nbsp;</td></tr>';
	for (var i=0;i<arrLen;i++)
		{
	        rowId=gridrowsarr[i];
	        seqno=mygrid.cellById(rowId, codeSeq_rowId).getValue();
	        codeid=mygrid.cellById(rowId, codeId_rowId).getValue();
	        name=mygrid.cellById(rowId, codeName_rowId).getValue();
	        codegrp=mygrid.cellById(rowId, codeGrp_rowId).getValue();
			altname = mygrid.cellById(rowId, codeAlt_rowId).getValue();
			controltype = mygrid.cellById(rowId, codeCType_rowId).getValue();
			activeflag=mygrid.cellById(rowId, codeActive_rowId).getValue();
			lockflag=mygrid.cellById(rowId, codeLock_rowId).getValue();
			hcodeid=mygrid.cellById(rowId, hCodeId_rowId ).getValue();
            //userId=mygrid.cellById(rowId,7).getValue();
			//createdDate=mygrid.cellById(rowId,8).getValue();
			
	
			strInputString = 'INSERT INTO t901_code_lookup<br>(c901_code_id, c901_code_nm, c901_code_grp,c901_active_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type, c901_created_by, c901_created_date)<br>VALUES <br>(';
			strInputString += hcodeid+',\''+name+'\',\''+codegrp+'\',\''+activeflag+'\',\''+seqno+'\',\''+altname+'\',\''+controltype+'\',\''+strUserId+'\',CURRENT_DATE);'+'<br><br>';
			pophtml+= '<tr><td>'+strInputString+'</td></tr>';
		}
	pophtml+= '<tr><td>&nbsp;</td></tr>';
	pophtml+= '<tr><td align="center"><input type="button" value="Close" class="button" onClick="javascript:window.close();"></td></tr>';
	pophtml+= '<tr><td>&nbsp;</td></tr>';
	frm.inputString.value = strInputString;
	pophtml+='</table></form></body></html>'
	winpPop=window.open("","script","toolbars=no,top= 100,left=200,height=450,innerHeight=450,width=650,innerWidth=650,scrollbars=yes,resizeable=no,status=");
	winpPop.document.writeln(pophtml);
	winpPop.show;
	}
}

function removeSelectedRow(){
	var gridrows=mygrid.getSelectedRowId();
	var isExistRow=false;
	if(gridrows!=undefined){
		var NameCellValue ;
		var gridrowsarr = gridrows.toString().split(",");
		var added_row;
		for (var rowid = 0; rowid < gridrowsarr.length; rowid++) {
			gridrowid = gridrowsarr[rowid];
			added_row = mygrid.getRowAttribute(gridrowid,"row_added");
			if(added_row!=undefined && added_row=="Y"){
				mygrid.deleteRow(gridrowid);
			}else{
				isExistRow=true;
			}
		}
		if(isExistRow){
			Error_Clear();
			Error_Details(message[501]);				
			Error_Show();
			Error_Clear();
			
		}
		mygrid.clearSelection();
	}else{
		Error_Clear();
		Error_Details(message[502]);				
		Error_Show();
		Error_Clear();
	}
	
}

function addRow(){
	var uid=mygrid.getUID();
	mygrid.addRow(uid,["","","","","","","","",""]);
	mygrid.setRowAttribute(uid,"row_added","Y");
}
function fnGetCodeCompMapDtls(codeid){
	windowOpener("/gmCodeLkpExcln.do?method=fetchCodeLookupExclnDtls&codeid="+codeid,"","resizable=yes,scrollbars=yes,top=250,left=300,width=1050,height=400");
}
//Checking for Key Events to Copy
function keyPressed(code, ctrl, shift) {
    if (code == 67 && ctrl) {
        /*if (!mygrid._selectionArea)
            return alert("You need to select a block area in grid first");
            */
        mygrid.setCSVDelimiter("\t");
        mygrid.copyBlockToClipboard();
    }
    
    if (code == 86 && ctrl) {
    	if(mygrid._selectionArea!=null){
			var colIndex = mygrid._selectionArea.LeftTopCol;
			var cb_columnName = '';
			
			if(colIndex!=undefined && colIndex==0){
				alert(message[10569]);
			}else{
				mygrid.pasteBlockFromClipboard();
			}
			mygrid._HideSelection();
		}else{
			alert(message[10570]);
		}
    }
	// This code is to block delete the content from the grid.
	if(code==46&&!ctrl&&!shift){
		
		if(mygrid._selectionArea!=null){
			var area=mygrid._selectionArea
			var leftTopCol=area.LeftTopCol;
			var leftTopRow=area.LeftTopRow;
			var rightBottomCol=area.RightBottomCol;
			var rightBottomRow=area.RightBottomRow;
			
			for (var i=leftTopRow; i<=rightBottomRow; i++){
				
				setRowAsModified(mygrid.getRowId(i),true);

				for (var j=leftTopCol; j<=rightBottomCol; j++){
					mygrid.cellByIndex(i,j).setValue("");
				}
			}
			mygrid._HideSelection();
		}
	}
    return true;
}