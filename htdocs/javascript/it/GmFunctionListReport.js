var objGridData;

function fnOnPageLoad()
{	 	
	if(objGridData!=''){
		
		gridObj = initGridData('GridData',objGridData);
		
		taskType_colIndex = gridObj.getColIndexById("task_Type");
		var sec_grp_rowID = gridObj.getColIndexById("security_grp_name");
		var editIcon_rowID = gridObj.getColIndexById("btn_edit");
		
		var hActionVal = document.frmUserGroupMap.haction.value;
		
		if(hActionVal =="SECGRP_SECEVN"){
			gridObj.setColumnHidden(editIcon_rowID,true);
			gridObj.setColumnHidden(sec_grp_rowID,false);
		}
		
		if (strOpt == "TSSG" || hActionVal =="SECGRP_SECEVN"){
			gridObj.setEditable(false);
		}
		
	}
}

function initGridData(divRef,gridData)
{	
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);
	if(strOpt == 'SESG'){
		gObj.attachHeader("#rspan,#text_filter,#text_filter,#text_filter,#select_filter_strict,#select_filter_strict,#select_filter_strict");
	}else if(strOpt == 'TSSG'){
		gObj.attachHeader("#rspan,#text_filter,#text_filter,#text_filter,#text_filter,#select_filter_strict,#select_filter,#select_filter_strict,#select_filter_strict");	
	}else{
		gObj.attachHeader("#rspan,#rspan,,#text_filter,#text_filter,#select_filter_strict,#text_filter,#text_filter,#select_filter_strict,#select_filter,#select_filter_strict,#select_filter_strict");	
	}
	gObj.init();	
	gObj.enablePaging(true, 20, 5, "pagingArea", true, "recinfoArea");
	gObj.setPagingSkin("bricks");
	gObj.enableLightMouseNavigation(true);
	gObj.loadXMLString(gridData);	
	return gObj;	
}

function fnEdit(str,strType){
	windowOpener("/gmUserGroupMap.do?method=fchFunctDetails&strOpt=OnFuncLoad&funcID="+str + "&grpType=" + strType,"UserGroupMap","resizable=yes,scrollbars=yes,top=150,left=150,width=950,height=600");
}

function fnSubmit(){
	var strVoid = '';
	var strTskName = '';
	var strTskDesc = '';
	var strTskType = '';
	var funId = '';
	var errTaskName = '';
	var errTaskDesc = '';
	var cmpName = "vdReason_";
	var gridrowid;
	var errVoidMsg = '';
	var gridrows;
	
	gridObj.editStop();
	
	if(strOpt == 'SESG'){// SESG Starts
		gridrows = gridObj.getChangedRows(1);
		
		if(gridrows.length>0){
			var gridrowsarr = gridrows.toString().split(",");
			var sName_rowID = gridObj.getColIndexById("sName");
			var sDes_rowID = gridObj.getColIndexById("sDesc");
			for (var rowid = 0; rowid < gridrowsarr.length; rowid++) {
				gridrowid = gridrowsarr[rowid];
				funId = fnCheckNull(gridObj.getUserData(gridrowid,"hfunctionId"));
				var strTsk = gridObj.cellById(gridrowid, sName_rowID).getValue();
				if(strTsk == ''){
					errTaskName += "," + funId;
				}				
				strTskName += funId + '^' + strTsk + "|";
				strTsk = gridObj.cellById(gridrowid, sDes_rowID).getValue();
				if(strTsk == ''){
					errTaskDesc += "," + funId;
				}
				strTskDesc += funId + '^' + strTsk + "|";
								
			}
		}
	}// SESG Ends
	else{//TSMM and TSSG
		gridrows = gridObj.getChangedRows(",");
		
		editButton_colIndex = gridObj.getColIndexById("btn_edit");
		voidChk_colIndex = gridObj.getColIndexById("chk_void");
		voidReason_colIndex = gridObj.getColIndexById("void_reason");

		var sName_rowID = gridObj.getColIndexById("sName");
		var sDes_rowID = gridObj.getColIndexById("sDesc");
		var status_rowID = gridObj.getColIndexById("status");
		var reqURI_rowID = gridObj.getColIndexById("req_URI");
		var strOpt_rowID = gridObj.getColIndexById("str_Opt");
		var module_rowID = gridObj.getColIndexById("module_name");
		var transType_rowID = gridObj.getColIndexById("task_Type");
		
		if(gridrows.length>0){
			var gridrowsarr = gridrows.toString().split(",");
			for (var rowid = 0; rowid < gridrowsarr.length; rowid++) {
				gridrowid = gridrowsarr[rowid];
				funId = fnCheckNull(gridObj.getUserData(gridrowid,"hfunctionId"));
				var strTsk = gridObj.cellById(gridrowid, sName_rowID).getValue();
				if(strTsk == ''){
					errTaskName += "," + funId;
				}
				strTskName += funId + '^' + strTsk + "|";
				strTsk = gridObj.cellById(gridrowid, sDes_rowID).getValue();
				if(strTsk == ''){
					errTaskDesc += "," + funId;
				}
				strTskDesc += funId + '^' + strTsk + "|";
				strTsk = gridObj.cellById(gridrowid, transType_rowID).getValue();
				strTskType += funId + '^' + strTsk + "|";
				var check_id = gridObj.cellById(gridrowid, voidChk_colIndex).getValue();
				var cmpid = cmpName + funId;
				var reasonId = gridObj.cellById(gridrowid, voidReason_colIndex).getValue();
				
				if(check_id == 1 && reasonId == 0){
					errVoidMsg += '<br><b>' + gridObj.cellById(gridrowid, sName_rowID).getValue() + '</b>';
				}
				
				strVoid += funId + '^' + reasonId + '^' + check_id + "|";

			}
		}

	}
	
	/*gridrows = gridObj.getCheckedRows(1);
	
	if(gridrows.length>0){
		var gridrowsarr = gridrows.toString().split(",");
		var cmpName = "vdReason_";
		for (var rowid = 0; rowid < gridrowsarr.length; rowid++) {
			gridrowid = gridrowsarr[rowid];
			funId = fnCheckNull(gridObj.getUserData(gridrowid,"hfunctionId"));
			alert("funId ::: " + funId);
			var cmpid = cmpName + funId;
			alert("cmpid ::: " + cmpid);
			var reasonId = document.getElementById(cmpid).value;
			alert("reasonId ::: " + reasonId);
			strVoid += funId + '^' + reasonId + "|";
		}
	}*/
	
	if(errTaskName != ''){
		Error_Details("Task Names should not be empty for the following Task ID: " + errTaskName.substring(1, errTaskName.length));
	}
	if(errTaskDesc != ''){
		Error_Details("Task Description should not be empty for the following Task ID: " + errTaskDesc.substring(1, errTaskDesc.length));
	}
	if(errVoidMsg != ''){
		Error_Details("Please select void reason for the following Task Names: " + errVoidMsg);
	}
	
 	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}

 	document.frmUserGroupMap.hVoidStr.value = strVoid;
 	document.frmUserGroupMap.hTaskNameStr.value = strTskName;
 	document.frmUserGroupMap.hTaskDescStr.value = strTskDesc;
 	document.frmUserGroupMap.hTaskTypeStr.value = strTskType;
 	
 	document.frmUserGroupMap.action = "/gmUserGroupMap.do?method=saveFunctDetails";
 	fnStartProgress();
 	document.frmUserGroupMap.submit();
}

function fnCheckNull(data)
{
	if(data==null || data=='null')
		return '';
	else
		return data;
}

function fnDownloadXLS()
{
	var objAllRowIds = gridObj.getAllRowIds();
	var objAllSplit = objAllRowIds.split(",");
	
	editButton_colIndex = gridObj.getColIndexById("btn_edit");
	voidChk_colIndex = gridObj.getColIndexById("chk_void");
	voidReason_colIndex = gridObj.getColIndexById("void_reason");
	
	gridObj.setColumnHidden(editButton_colIndex,true);
	
	if (strOpt == "TSMD"){
		gridObj.setColumnHidden(voidChk_colIndex,true);
		
		for (var i=0;i<objAllSplit.length;i++){		
			reasonVal=gridObj.cellById(objAllSplit[i],voidReason_colIndex).getValue();
			if(reasonVal=="0"){
				gridObj.cellById(objAllSplit[i], voidReason_colIndex).setValue("");
				}
		}
	}
	gridObj.toExcel('/phpapp/excel/generate.php');
		
	gridObj.setColumnHidden(editButton_colIndex,false);
	
	if (strOpt == "TSMD"){
		gridObj.setColumnHidden(voidChk_colIndex,false);
		
		for (var i=0;i<objAllSplit.length;i++){		
			reasonVal=gridObj.cellById(objAllSplit[i],voidReason_colIndex).getValue();
			if(reasonVal==""){
				gridObj.cellById(objAllSplit[i], voidReason_colIndex).setValue("0");
				}
			}
	}
	var hActionVal = document.frmUserGroupMap.haction.value;
	
	if(hActionVal =="SECGRP_SECEVN"){
		gridObj.setColumnHidden(editButton_colIndex,true);
	}
		
}
