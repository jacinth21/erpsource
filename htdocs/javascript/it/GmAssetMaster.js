/*******************************************************************************************
 * File					: AssetMaster.js
 * Desc					: This has the list of Error / Informative Messages
 * Version				: V1.0
 *******************************************************************************************/

function fnSubmit(){
	 var frm = document.frmAssetMaster; 
	Error_Clear();
	fnValidate();
	if (ErrorCount > 0)
	{
			Error_Show();
			return false;
	}	
	frm.strOpt.value = 'save';
	fnStartProgress();
	frm.submit();
}
function fnEnter()
{
		if (event.keyCode == 13)
		{ 
			fnReload();
		}
}
function fnValidate()
{
	var frm = document.frmAssetMaster; 
	fnValidateTxtFld('assetID',' Asset ID '); 
	fnValidateTxtFld('assetName',' Asset Name '); 
}
function fnVoid()
{
	 var frm = document.frmAssetMaster; 
	 frm.action = "/GmCommonCancelServlet";
	 frm.hTxnId.value = frm.assetID.value;
	 frm.submit();
}

// methods for report
function fnAssetAttribute(){
	 var frm = document.frmAssetMaster; 
	 frm.strOpt.value = "edit";
	 frm.action = "/gmAssetAttribute.do";
	 frm.submit();
}

function fnOnPageLoad(){
/*	var frm = document.frmAssetMaster; 
	document.getElementById("AssetAttribute").style.display="none";
	var strOpt=frm.strOpt.value;
	if(strOpt == 'edit'){
		document.getElementById("AssetAttribute").style.display="block";
	}
	*/
}
function fnReload()
{
	var frm = document.frmAssetMaster;
	frm.strOpt.value = 'reload';
	fnStartProgress();
	frm.submit();
}

function fnOnReportLoad()
{	  
	if (objGridData != '')
	{
		gridObj = initGrid('employeedata',objGridData);
		gridObj.enableTooltips("false,true,true,true,true");
	}
	
}

function fnEditAssetId(id)
{
	var frm = document.frmAssetMaster;
	frm.assetID.value = id;
	frm.strOpt.value = 'editload';
	frm.submit();
}



