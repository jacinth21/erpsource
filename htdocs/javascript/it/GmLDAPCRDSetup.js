// this function used to fetch the LDAP information
function fnReload (obj){
	if(obj.value !=0){
		document.frmLDAPCRD.strOpt.value ="reload";
		document.frmLDAPCRD.submit();
	}
}
function fnSubmit(opt)
{
	fnDisableField(false);
	fnValidateTxtFld('name',' Connection Name '); 
	fnValidateTxtFld('userName',' User Name '); 
	fnValidateTxtFld('password',' Password '); 
	fnValidateTxtFld('hostName',' Host Name '); 
	fnValidateTxtFld('port',' Port '); 
	fnValidateTxtFld('userdn',' User DN ');
	fnValidateTxtFld('base',' Base '); 
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	} 
	document.getElementById("btn"+opt).disabled = true;
 	document.frmLDAPCRD.strOpt.value = opt; //opt will be save or test
 	fnStartProgress();
	document.frmLDAPCRD.submit();
	 
} 

function fnDisableField(state){
	var frm = document.frmLDAPCRD;
	frm.name.disabled = state;
	frm.userName.disabled = state;
	frm.password.disabled = state;
	frm.hostName.disabled = state;
	frm.port.disabled = state;
	frm.userdn.disabled = state;
	frm.ldapID.disabled = state;
	frm.base.disabled = state;
}

function fnPageLoad(msg){
	if(msg == "success")//Test Connection successful - disable all fields
		fnDisableField(true);
}