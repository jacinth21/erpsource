var req;
var trcnt = 0;
var cnt = 0;
var tempPnum = '';


function fnAddToCart()
{ 
		validate();

}

function fnRemoveItem(val)
{	
	obj = eval("document.frmCart.Txt_Qty"+val);
	obj.value = '';
	
    obj = eval("document.all.Lbl_Desc"+val);
    obj.innerHTML = "&nbsp;";
    
	obj = eval("document.all.Lbl_Part"+val);
	pnum = obj.innerHTML;
	obj.innerHTML = "&nbsp;";
	
}

function fnClearCart()
{
	var varRowCnt = document.frmCart.hRowCnt.value;
	for (val=0; val < varRowCnt; val ++)
	{
		obj = eval("document.frmCart.Txt_Qty"+val);
		obj.value = '';
		obj = eval("document.all.Lbl_Part"+val);
		obj.innerHTML = "&nbsp;";
		obj = eval("document.all.Lbl_Desc"+val);
		obj.innerHTML = "&nbsp;";

	}
}

function validate() 
{
	pnum = document.frmCart.Txt_PartNum.value;

	if (pnum == '')
	{
		 Error_Details(message[5041]);
	}
	
	if (ErrorCount > 0)
	 {
		Error_Show();
		Error_Clear();
		return false;
	}

	tempPnum = pnum;
	qty = 1;
	accid = 0;
	repid = 0;
	gpoid = 0;
		
	var url = fnAjaxURLAppendCmpInfo("/GmCartAjaxServlet?pnum=" + encodeURIComponent(pnum)+ "&qty=" + encodeURIComponent(qty)+ "&acc="+encodeURIComponent(accid)+ "&rep="+encodeURIComponent(repid)+ "&gpoid="+encodeURIComponent(gpoid)+"&"+fnAppendCompanyInfo());

	if (typeof XMLHttpRequest != "undefined") {
	 req = new XMLHttpRequest();
	} else if (window.ActiveXObject) {
	 req = new ActiveXObject("Microsoft.XMLHTTP");
	}

	req.open("GET", url, true);
	req.onreadystatechange = callback;

	req.send(null);
 	document.frmCart.Txt_PartNum.value = '';  
}
 
function callback() {
    if (req.readyState == 4) {
        if (req.status == 200) {
	        var xmlDoc = req.responseXML;
	        parseMessage(xmlDoc);
        }
    }
}

function parseMessage(xmlDoc) 
{

	var pnum = xmlDoc.getElementsByTagName("pnum");

	var data = xmlDoc.getElementsByTagName("data");
	var datalength = data[0].childNodes.length;

	 
	
	if(datalength == 0){		
		 setErrMessage(message[10078]); 
		 return;
	}

	var pnumlen = pnum.length;
	var rem = 0;

	for (i=0;i<cnt;i++)
	{
		pnumobj = eval("document.all.Lbl_Part"+i);
		if (pnumobj.innerHTML == '&nbsp;')
		{
			rem++;
		}
	}

	rem = pnumlen - rem;

	for (i=0;i<rem;i++)
	{
		fnAddRow('PartnPricing');
	}

	var partindex = '';
	var pdescindex = '';
	var stockindex = '';

	var saleFlagIndex='';
	var conspriceindex=-1;

if(pnumlen > 0){
	// This loop is to just get the index from the first part tag (pnum[0])

	for (var x=0; x<pnum[0].childNodes.length; x++) 
		{
		
			xmlstr = pnum[0].childNodes[x].tagName;
			//alert(xmlstr);
			if (xmlstr == 'partnum')
			{
				partindex = x;
			} 
			else if (xmlstr == 'pdesc')
			{
				pdescindex = x;
				
			}
			else if (xmlstr == 'qty')
			{
				stockindex = x;
			}
			
			else if (xmlstr == 'rfl')//Release for sale
			{
				saleFlagIndex=x; 
			}
			else if (xmlstr == 'consprice')//part price (Consignment price) 
			{
				conspriceindex = x; 
			}
		}

	// This loop is to set all the values as per the index got from the loop

	for (var x=0; x<pnumlen; x++) 
	{
		var pdesc = parseXmlNode(pnum[x].childNodes[pdescindex].firstChild);
		var part = parseXmlNode(pnum[x].childNodes[partindex].firstChild);

		var reqtype = parent.document.all.requestType.value;
		var releaseForSale='';
		var Txtobj = eval("document.all.Txt_Qty"+x);
		if(saleFlagIndex>0)
		{
			releaseForSale = parseXmlNode(pnum[x].childNodes[saleFlagIndex].firstChild);
		}

		var reqtype = parent.document.all.requestType.value;
		var releaseForSale='';


			releaseForSale = parseXmlNode(pnum[x].childNodes[saleFlagIndex].firstChild);

		if (reqtype == 26240420)// only for the Consignment and ICT , need to check release_for_sale Flag
		{
			if(releaseForSale == 'N'){		
				setErrMessage('<span class=RightTextBlue>Part Number is not released for sale</span>');
				Txtobj = '';
				return;
		}
	}
		var partprice =0;
		if(conspriceindex >= 0)
	 	{
	 		partprice = parseXmlNode(pnum[x].childNodes[conspriceindex].firstChild);
	 	}else{
	 		partprice = -1;
	 	}
		setMessage(part,pdesc,partprice);
	}

}
}
 
function setMessage(part,pdesc,partprice) 
{
	
	var reqtype = parent.document.all.requestType.value;
	var reqdate = parent.document.all.requiredDate.value;
	var reqQty = '1';


	for (i=0;i<cnt;i++)
	{
		pnumobj = eval("document.all.Lbl_Part"+i);
		obj = eval("document.all.Txt_Qty"+i);
		var hStockObj = '';

		if (pnumobj.innerHTML == '&nbsp;')
		{
			pnumobj.innerHTML = part;

			pdescobj = eval("document.all.Lbl_Desc"+i);

			obj.disabled = false;
			obj.style.display = '';
			
			if(partprice==''){
				pdesc = '<span class=RightTextBlue>The Price is not set for this part</span>';

				obj.disabled = true;
			
				obj.style.display = 'none';
				reqQty = '';

			}
			
			pdescobj.innerHTML = "&nbsp;" + pdesc;

			
			obj.value = reqQty;

			break;

		}
	}
}

function setErrMessage(msg) 
{	
	for (i=0;i<cnt;i++){		
		pnumobj = eval("document.all.Lbl_Part"+i);
		if (pnumobj.innerHTML == '&nbsp;')
		{
			var obj = eval("document.all.Lbl_Desc"+i);
			obj.innerHTML = msg;
			
			obj = eval("document.all.Lbl_Part"+i);
			obj.innerHTML = '&nbsp;';
			obj.focus(); 

			break;
		}
	}

}


function fnAddRow(id)
{

	var reqtype = parent.document.all.requestType.value;
	var tbody = document.getElementById(id).getElementsByTagName("TBODY")[0];
    var row = document.createElement("TR");
    var rowLine = document.createElement("TR");


    var td0 = document.createElement("TD");
    td0.innerHTML = Number(cnt)+1;

    var td1 = document.createElement("TD");
    td1.innerHTML = fnCreateCell('IMG','',1);
    
    var td2 = document.createElement("TD");
	td2.id = "Lbl_Part"+cnt;
	td2.innerHTML = "&nbsp;";
   
    var td3 = document.createElement("TD");
	td3.id = "Lbl_Desc"+cnt;
	td3.innerHTML = "&nbsp;";
    td3.align = "Left";
            
    var td4 = document.createElement("TD");
    td4.innerHTML = fnCreateCell('TXT','Qty',3);
    td4.align = "center";
    
    var td5 = document.createElement("TD"); 
    td5.colSpan =5;
    td5.className="LLine";
    td5.height="1";
    
       
    row.appendChild(td0);
	row.appendChild(td1);
    row.appendChild(td2);
    row.appendChild(td3);
    row.appendChild(td4);
    rowLine.appendChild(td5);

    tbody.appendChild(row);
    tbody.appendChild(rowLine);
	cnt++;
	document.frmCart.hRowCnt.value = cnt;

}

function fnCreateCell(type,val,size)
{
	param = val;
	val = val + cnt;
	var html = '';
	var formatJS = document.frmCart.hformatJS.value;
	if (type == 'TXT')
	{
		if (param == 'Qty') {
				html = '&nbsp;<input type=text size='+size+' name=Txt_'+val+' class=InputArea onFocus=changeBgColor(this,\'#AACCE8\'); onBlur=changeBgColor(this,'+cnt+'); tabindex=16 value=\'\'>';
				html = html+ '<input type=hidden name=hStock'+cnt+' value=\'\'>';
						}
		else if (param == 'ReqDate') {
				
				html = '&nbsp;<input type=text size='+size+' name=Txt_'+val+' id=Txt_'+val+' class=InputArea onFocus=changeBgColor(this,\'#AACCE8\'); onBlur=changeBgColor(this,'+cnt+'); value=\'\'>';
				html = html+ ' <img style=cursor:hand onclick=javascript:showSingleCalendar("divTxt_'+val+'","Txt_'+val+'","'+formatJS+'"); title="Click to open Calendar"  src="images/nav_calendar.gif" border=0 align=absmiddle height=18 width=19 maxlength=10/><div id="divTxt_'+val+'" style="position: absolute; z-index: 10;"></div>';
		}
	}
	else if (type == 'IMG')
	{
		var fn = size;
		html = '<a href=javascript:fnRemoveItem('+val+') tabindex=\'-1\';><img border=0 Alt=Remove from cart valign=left src=/images/btn_remove.gif height=10 width=9></a>';
	}

	return html;
}


function fnCreateInputString(){

	var objpnum = '';
	var pnum = '';	
	var objreqqty = '';
	var reqqty = '';
	var token = '^';
	var inputstring = '';
	var str = '';
	var emptyStr = '';

	var reqdate = parent.document.all.requiredDate.value;
	var reqtype = parent.document.all.requestType.value;


	for (val = 0; val < cnt; val++ )
	{

			objpnum = eval("document.all.Lbl_Part"+val);
			pnum = objpnum.innerHTML;
			objreqqty = eval("document.all.Txt_Qty"+val);			
			reqqty = objreqqty.value;
	
			if(reqqty != ''){

					str = pnum + token + reqqty + token + reqdate + '^'+emptyStr+'|';
					inputstring = inputstring + str;
								
			}			
	}
	return inputstring;
}

function fnOpenPart()
{
	pnum = document.frmCart.Txt_PartNum.value;

	windowOpener("/GmSearchServlet?hAction=SearchPart&Txt_PartNum="+encodeURIComponent(pnum),"Search","resizable=yes,scrollbars=yes,top=340,left=420,width=600,height=435");
}

/*Allowed only Numeric values*/
function isNumberKey(evt)
{
   var charCode = (evt.which) ? evt.which : event.keyCode;
   if (charCode > 31 && (charCode < 46 || charCode > 57 ))
      return false;

   return true;
}
