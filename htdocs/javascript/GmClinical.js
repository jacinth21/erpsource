function calAvg(dataObj)
{
	if (dataObj == undefined) {   
		return false;
	}

	if(dataObj.name=="")
		return false;

	startVal = 0;
	endVal = 0;
	count = 0;
	divCount = 0;
	var id = dataObj.name.substring(dataObj.name.length-2, dataObj.name.length) 
	switch (id)
	{
		case '36':
		startVal = 17;
		endVal = 23;
		break;
		case '37': 
		startVal = 24;
		endVal = 28;
		break;
		case '38':
		startVal = 29;
		endVal = 34;
		break;
		case '50':
		startVal = 7401;
		endVal = 7407;
		break;
		case '51':
		startVal = 7410;
		endVal = 7414;
		break;
		case '52':
		startVal = 7417;
		endVal = 7422;
		break;
		case '60':
		startVal = 6901;
		endVal = 6907;
		break;
		case '61':
		startVal = 6910;
		endVal = 6914;
		break;
		
	}
	
	for( k = startVal; k <= endVal; k++ ){
		objSelect = eval("document.frmOrder.hAnsGrpId"+k);
		objSelectVal = objSelect[objSelect.selectedIndex].value;
		if(objSelectVal!=0){
			objSelectTxt = objSelect[objSelect.selectedIndex].text;
			count = count + parseInt(objSelectTxt.substring(0,1));  
			divCount++;
		}
	}

	if(divCount!=0){
		dataObj.value = Math.round((count/divCount)*100)/100;
	}else
		dataObj.value = "";
}

// PMT-37363: Aminos study - Formula changes
// This function used to calculate Amnios study AM6 form

// to calculate Pain, Stiffness, Difficulty, Activity,Social score.
// Based on 5 score values to calculate Avg and populate to Cumulative filed.

function fnCalculateAmniosScore(dataObj) {
	
	if (dataObj == undefined) {
		return false;
	}

	if (dataObj.name == "")
		return false;

	var startVal = 0;
	var endVal = 0;
	var count = 0;
	var divCount = 0;
	var selectedCount = 0;
	var scoreVal = 0;
	var dropDownValueFl = false;

	var id = dataObj.name;

	switch (id) {
	case 'hAnsGrpId26641':
		startVal = 26601;
		endVal = 26607;
		dropDownValueFl = true;
		// divCount = 28;
		break;
	case 'hAnsGrpId26642':
		startVal = 26608;
		endVal = 26614;
		dropDownValueFl = true;
		// divCount = 28;
		break;
	case 'hAnsGrpId26643':
		startVal = 26615;
		endVal = 26625;
		dropDownValueFl = true;
		// divCount = 44;
		break;
	case 'hAnsGrpId26644':
		startVal = 26626;
		endVal = 26628;
		dropDownValueFl = true;
		// divCount = 12;
		break;
	case 'hAnsGrpId26645':
		startVal = 26629;
		endVal = 26634;
		dropDownValueFl = true;
		// divCount = 24;
		break;
	case 'hAnsGrpId26646':
		startVal = 26641;
		endVal = 26645;
		dropDownValueFl = false;
		// divCount = 5;
		break;
	}

	// loop the values and calculate the score
	
	for (k = startVal; k <= endVal; k++) {
		objSelect = eval("document.frmOrder.hAnsGrpId" + k);
		objSelectVal = objSelect.value;

		if (objSelectVal != 0 && objSelectVal != '') {
			// all the drop down values are - calculate 
			if (dropDownValueFl) {
				objSelectTxt = objSelect[objSelect.selectedIndex].text;
				var selectVal = parseInt(objSelectTxt.substring(0, 1));

				if (selectVal != 5) {
					count = count + selectVal;
					selectedCount++;
				}

			} else {
				// text field calculate
				objSelectTxt = objSelectVal;
				count = count + parseFloat(objSelectTxt);
			} // end of drop down check

		} // end of object check
	} // end for loop

	// Based on flag to calculate the values.
	if (count != 0) {
		if (dropDownValueFl) {
			scoreVal = (count / (selectedCount * 4)) * 100;
			// PMT-57326: Round decimal digit updated from 2 to 1 digit
			dataObj.value = fnRoundNumber(scoreVal, 1);
		} else {
			// PMT-57326: Round decimal digit updated from 2 to 1 digit
			dataObj.value = fnRoundNumber((count / 5), 1);
		} // end of drop down check

	} else {
		dataObj.value = "";
	} // end of count check

}

