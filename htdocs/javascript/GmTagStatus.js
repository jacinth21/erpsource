function fnSubmit()
{	
	var fromtagvalue = document.frmTagStatus.tagIdFrom.value;
	var totagvalue = document.frmTagStatus.tagIdTo.value;
	
	fnValidateTxtFld('tagIdFrom',lblTagRangeFrom);
	fnValidateTxtFld('tagIdTo',lblTagRangeTo);
	fnValidateDropDn('typeStatus',lblStatus);
	chkValue(fromtagvalue);
	chkValue(totagvalue);	
	
	if (fromtagvalue > totagvalue)
	{	
		Error_Details(message_accounts[298]);
	}
	if(fromtagvalue.charAt(0) == '-' || totagvalue.charAt(0) == '-')
	{	
		Error_Details(message_accounts[299]);
	}
	
	if (ErrorCount > 0)  
	{	
			Error_Show();
			Error_Clear();
			return false;
	}
	document.frmTagStatus.strOpt.value  = "save";
	fnStartProgress();
	document.frmTagStatus.submit();
}
function chkValue(value){

	   if (isNaN(value))
	    {   	
	    	Error_Details(message[28]);   	
	    }
	}

function fnChkStatus()
{
	frm = document.frmTagStatus;
	if (frm.typeStatus.value == '51012' || frm.typeStatus.value == '51013')
	{
		frm.btn_Submit.disabled=true;
	}else{
		frm.btn_Submit.disabled= false;
	}
	if(SwitchUserFl == 'Y'){
		frm.btn_Submit.disabled=true;
	}
}
