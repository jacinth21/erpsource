function fnReload()
{
	var objCat = document.frmAccount.Chk_GrpCat;
	var objSet = document.frmAccount.Chk_GrpSet;
	var objStat = document.frmAccount.Chk_GrpStat;
	var objDist = document.frmAccount.Chk_GrpDist;
	var objPriority = document.frmAccount.Chk_GrpPriority;
	var prioritylen = 0;
	if (objCat)
	{
		var catlen = objCat.length;
	}
	if (objDist)
	{
		var distlen = objDist.length;
	}

	var setlen = objSet.length;
	var statlen = objStat.length;
	
	if(objPriority != undefined && objPriority!='' ){
		prioritylen = objPriority.length;
	}

	var catstr = '';
	var setstr = '';
	var statstr = '';
	var statgrpstr = '';
	var diststr = '';
	var prioritystr='';
	var priority='';
	
	var temp = '';
	if (objCat)
	{	
		for (var i=0;i<catlen;i++)
		{
			if (objCat[i].checked == true)
			{
				catstr = catstr + objCat[i].value + ',';
			}
		}
		document.frmAccount.hCategories.value = catstr.substr(0,catstr.length-1);
	}
		
	for (var i=0;i<setlen;i++)
	{
		if (objSet[i].checked == true)
		{
			setstr = setstr + objSet[i].value + ',';
		}
		document.frmAccount.hSetIds.value = setstr.substr(0,setstr.length-1);
	}
	
	for (var i=0;i<statlen;i++)
	{
		if (objStat[i].checked == true)
		{
			
			temp = objStat[i].value;			
			statgrpstr = statgrpstr + objStat[i].value + ',';			
			statstr = statstr + temp + ',';	
		}
	}
	
	for (var i=0;i<prioritylen;i++){
		if (objPriority[i].checked == true){		
			prioritystr = prioritystr + objPriority[i].value + ',';
		}
	}
	
	if (objDist)
	{	
		for (var i=0;i<distlen;i++)
		{
			if (objDist[i].checked == true)
			{
				diststr = diststr + objDist[i].value + ',';
			}
		}
		document.frmAccount.hDistIds.value = diststr.substr(0,diststr.length-1);
	}
	
	//added for PC-884
	if(document.frmAccount.hSalesRepName != undefined && document.frmAccount.hSalesRepName.value != ''){
		if(document.frmAccount.Cbo_RepId != undefined && document.frmAccount.Cbo_RepId.value == ''){
			Error_Details('Please enter valid Sales Rep');
			if (ErrorCount > 0)
			{
				Error_Show();
				Error_Clear();
				return false;
			}
		}
	}
	
	document.frmAccount.hStatusGrp.value = statgrpstr.substr(0,statgrpstr.length-1);
	document.frmAccount.hPriorityStr.value = prioritystr.substr(0,prioritystr.length-1);
	document.frmAccount.hStatus.value = statstr.substr(0,statstr.length-1);
	document.frmAccount.hAction.value = 'Reload';
	document.frmAccount.action = sServletPath+'/GmInHouseSetServlet';
	fnStartProgress('Y');
	document.frmAccount.submit();
}

function fnCallEdit()
{
	var strErrMsg = message_operations[591];
	Error_Clear();
	var consignId = document.frmAccount.hConsignId.value;
	var flag = document.frmAccount.hFlagId.value;
	var etch = document.frmAccount.hEtchId.value;
	val = document.frmAccount.Cbo_Action.value;
	
	if (document.frmAccount.hConsignId.value == '')
	{
		Error_Details(message_operations[592]);
	}else{
		
		if (val == '0')
		{
			Error_Details(message_operations[593]);
		}
		
		if (val == 'ETCH' )
		{
			if (flag == '60')
			{
				Error_Details(message_operations[594]);
			}
				document.frmAccount.hAction.value = "Load";
				document.frmAccount.action = sServletPath+'/GmAssignSetCategoryServlet';
			
		}
		//else if (val == 'SHIP' || val == 'RETURN' || val == 'PROCESSRETURN')
		else if ( val == 'RETURN' || val == 'PROCESSRETURN')
		{
			/*if (val == 'SHIP')
			{
				if (stype == '4127')
				{
					Error_Details('Cannot Ship out this Transaction from here');
				}
				if (etch == '')
				{
					Error_Details('This set has to be etched first before shipping out');
				}
				if (flag != '0')
				{
					Error_Details(strErrMsg);
				}
			}*/
			if (val == 'RETURN')
			{
				if (flag != '20' && flag != '23' && flag != '22' && flag != '24' && flag != '21')//flag 21 is disputed status,disputed to allow accept return
				{
					Error_Details(strErrMsg);
				}
			}	
			if (val == 'PROCESSRETURN')
			{
				if (flag != '25')
				{
					Error_Details(strErrMsg);
				}
				document.frmAccount.hAddChargeId.value='92075';
			}	
			document.frmAccount.hAction.value = "EditLoad";
			document.frmAccount.action = sServletPath+'/GmInHouseSetServlet';
		}
		else if(val == 'REPROCESS'){
			if(flag!='50'){
				Error_Details(strErrMsg);
			}
			document.frmAccount.hAction.value ="REPROCESS";
			document.frmAccount.action = sServletPath+'/GmInHouseSetServlet';
		}else if(val == 'MVPROCESS'){
			//	alert(flag);
			if(flag != '30' ){
				Error_Details(strErrMsg);
			}       
			document.frmAccount.action = sServletPath+"/GmCommonCancelServlet?hTxnName="  + consignId + "&hAction=Load&hCancelType=MVPROC&hTxnId="  + consignId;
		}else if(val == 'PICTURE'){
			if(flag!='55'){
				Error_Details(strErrMsg);
			}
			document.getElementById("hAction").value='PICTURE';
			document.frmAccount.action = sServletPath+'/GmInHouseSetServlet';
		}
		else if (val == 'REFILL')
		{
			if (flag == '0' || flag == '30')
			{
				document.frmAccount.hAction.value = "Load";
				document.frmAccount.action = sServletPath+'/GmSetAddRemPartsServlet';			
			}
			else if (flag == '5' || flag == '10'){
				document.frmAccount.hAction.value = "EditLoad";
				document.frmAccount.action = sServletPath+'/GmInHouseSetServlet';
			}		
			else
			{
				Error_Details(strErrMsg);
			}
			
		}else if (val == 'LNRECONFIG'){
			if (flag != '0' && flag != '5' && flag != '10' && flag != '58' ){
				Error_Details(strErrMsg);
			}
			document.frmAccount.hAction.value = "EditLoad";
			document.frmAccount.action = sServletPath+'/GmInHouseSetServlet';
		}
		else if (val == 'REFILLHIST')
		{
			document.frmAccount.hAction.value = "ReloadHist";
			document.frmAccount.action = sServletPath+'/GmInHouseSetServlet';
		}
		else if (val == 'USAGEHIST')
		{
			document.frmAccount.hAction.value = "ReloadUsage";
			document.frmAccount.action = sServletPath+'/GmInHouseSetServlet';
		}
		else if (val == 'ROLLSHIP' || val == 'ROLLRETURN')
		{
			//Able to rollback loaner from Pending shipping
			//Able to rollback loaner from pending check or  pending acceptance status to pending return
			//Able to rollback Disputed Loaner from pending check and rollback disputed loaner to Pending Return
			if ((val == 'ROLLSHIP' && flag != '10') || (val == 'ROLLRETURN' && flag != '25' && flag != '24' && flag != '21'))
			{
				Error_Details(strErrMsg);
			}
			if (val == 'ROLLSHIP')
			{
				document.frmAccount.hCancelType.value = "VDLNT";
			}
			else
			{   // 25 - pending check or  24 - pending acceptance
				if (flag == '24'){
					document.frmAccount.hCancelType.value = "RBPALN";
				}else{
					 document.frmAccount.hCancelType.value = "RBLNR";
				}
			}
			
			document.frmAccount.hAction.value = "Load";
			document.frmAccount.hTxnId.value = document.frmAccount.hConsignId.value;
			document.frmAccount.action = sServletPath+'/GmCommonCancelServlet';
		}
		else if (val == 'EXTENSION')
		{
			
			if(flag != '20'){
				Error_Details(message_operations[595]);
			}
		//	else {
		//		alert("is ok for loaner extension.")
		//	}
			document.frmAccount.hInputEditPart.value = "NotShow";
			document.frmAccount.hAction.value = "EditLoad";
			document.frmAccount.action = sServletPath+'/GmInHouseSetServlet';	
		}
		else if (val == 'RBPROCESS')
		{
			if(flag!='50' && flag!='55' && flag != '30')// && flag != '40' && flag != '0' 
			{
			
				Error_Details(strErrMsg);
			}
			
			if (val == 'RBPROCESS')
			{
			document.frmAccount.hCancelType.value = "RBPRS";
			}
			document.frmAccount.hAction.value = "Load";
			document.frmAccount.hTxnId.value = document.frmAccount.hConsignId.value;
			document.frmAccount.action = sServletPath+'/GmCommonCancelServlet';
		}
		else if (val == 'LNCNSWAP')
		{
			if(flag != '20' && flag != '0' && flag != '5' && flag != '24'){//To Perform Loaner to Consignment swap Action for Pending Acceptance status(24)
				Error_Details(message_operations[596]);
			}
			document.frmAccount.hCancelType.value = "LNSWAP";
			document.frmAccount.hAction.value = "Load";
			document.frmAccount.hTxnId.value = document.frmAccount.hConsignId.value;
			document.frmAccount.hCancelReturnVal.value="true";
			document.frmAccount.action = sServletPath+'/GmCommonCancelServlet';
		}else if(val =='MISSING'){
			if(flag !='20' && flag != '24' && flag !='21'){  //To Perform Move to Missing Action for Pending Acceptance status(24)--Disputed(21)
				Error_Details(strErrMsg);
			}else if(missedFl!='Y'){
				Error_Details(message_operations[597]);
			}
			document.frmAccount.hCancelType.value = "LNSMIS";
			document.frmAccount.hAction.value = "Load";
			document.frmAccount.hTxnId.value = document.frmAccount.hConsignId.value;
			document.frmAccount.action = sServletPath+'/GmCommonCancelServlet';
		}else if(val == "RBPENPUT"){
			if(flag !='58'){
				Error_Details(strErrMsg);
			}
			document.frmAccount.hCancelType.value = "RBLNPP";
			document.frmAccount.hAction.value = "Load";
			document.frmAccount.hTxnId.value = document.frmAccount.hConsignId.value;
			document.frmAccount.action = sServletPath+'/GmCommonCancelServlet';
	
		}else if (val == 'LOTRECONFIG'){//allow if set status in Available(0) or in Pending Return (20)
			if (flag != '0' && flag != '20' ){
				Error_Details(strErrMsg);
			}
			document.frmAccount.hAction.value = "EditLoad";
			document.frmAccount.action = sServletPath+'/GmInHouseSetServlet';
		}
		else if (val == 'PRIORITY'){
			    
			document.frmAccount.action = '/gmSetPrioritization.do?method=fetchChangePriority&consignmentId='+consignId+"&strOpt=Reload";
		}else if(val =='DISPUTE'){
            if(flag !='20'){  //20--Only Pending Return Status can be disputed
                Error_Details(message_operations[856]); // validation message
            }else if(missedFl!='Y'){
				Error_Details(message_operations[597]);
			}
            document.frmAccount.hCancelType.value = "LNDSPT";//Loaner Dispute
            document.frmAccount.hAction.value = "Load";
            document.frmAccount.hTxnId.value = document.frmAccount.hConsignId.value;
            document.frmAccount.action = sServletPath+'/GmCommonCancelServlet';
		}
		else if (val == 'RLDCONS')
		{
			if(flag!='25' && flag!='30') // flag 25 - Pending Check, 30 - WIP Loaners
			{
				Error_Details(strErrMsg);
			}
			
			if(val == 'RLDCONS'){
			document.frmAccount.hCancelType.value = "RLDCNS";
			}
			
			document.frmAccount.hAction.value = "Load";
			document.frmAccount.hTxnId.value = document.frmAccount.hConsignId.value;
			document.frmAccount.action = sServletPath+'/GmCommonCancelServlet';
		}
	}
	if (ErrorCount > 0)
	{
		Error_Show();
		return false;
	}else{	
		fnStartProgress('Y');
		document.frmAccount.submit();
	}
}

function Toggle(val)
{
	
	var obj = document.getElementById("div"+val);
	var trobj = document.getElementById("tr"+val);
	var tabobj = document.getElementById("tab"+val);

	if (obj.style.display == 'none')
	{
		obj.style.display = '';
		trobj.className="ShadeRightTableCaptionBlue";
		//tabobj.style.background="#c6e6fe";
		//tabobj.style.background="#d7ecfd";
		tabobj.style.background="#ecf6fe";
	}
	else
	{
		obj.style.display = 'none';
		trobj.className="";
		tabobj.style.background="#ffffff";
	}
}

function fnCallDiv(val,flag,etch)
{
	document.frmAccount.hConsignId.value = val;
	document.frmAccount.hFlagId.value = flag;
	document.frmAccount.hEtchId.value = etch;
}

function fnPrintVer(val)
{
	windowOpener("/GmInHouseSetServlet?hAction=ViewSet&hConsignId="+val,"Con1","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}


function fnLoad()
{
	var setids = document.frmAccount.hSetIds.value;
	var catids = document.frmAccount.hCategories.value;
	var status = document.frmAccount.hStatusGrp.value;
	var priorityStr = document.frmAccount.hPriorityStr.value;
	var distids = document.frmAccount.hDistIds.value;

	var objgrp = '';
	
	if (setids != '')
	{
		objgrp = document.frmAccount.Chk_GrpSet;
		fnCheckSelections(setids,objgrp);
	}
	
	if (catids != '')
	{
		objgrp = document.frmAccount.Chk_GrpCat;
		fnCheckSelections(catids,objgrp);
	}	
	
	if (status != '')
	{
		objgrp = document.frmAccount.Chk_GrpStat;
		fnCheckSelections(status,objgrp);
	}
	
	if (priorityStr != ''){
		objgrp = document.frmAccount.Chk_GrpPriority;
		fnCheckSelections(priorityStr,objgrp);
	}
	
	if (distids != '')
	{
		objgrp = document.frmAccount.Chk_GrpDist;
		fnCheckSelections(distids,objgrp);
	}
}

// Marks the checkboxes as 'checked'
function fnCheckSelections(val,objgrp)
{
	var valobj = val.split(",");
	var sval = '';
	var arrlen = objgrp.length;
	
	if (arrlen > 0)
	{
		for (var j=0;j< valobj.length;j++ )
		{
			sval = valobj[j];
			for (var i=0;i< arrlen;i++ )
			{
				if (objgrp[i].value == sval)
				{
					objgrp[i].checked = true;
					break;
				}
			}
		}
	}
	else
	{
		objgrp.checked = true;
	}
}

function fnOpenLog(id,type)
{
	windowOpener("/GmCommonLogServlet?hType="+type+"&hID="+id,"ConsignLog","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=300");
}

function fnOpenHistory(strConId){
	windowOpener("/GmLnCnSWAPLog.do?loanerCnId="+strConId,"ConsignLog","resizable=yes,scrollbars=yes,top=250,left=300,width=595,height=425");
}

//PC-884 - function to set Repname in SalesRep filter
function fnSetRepName(setName){
	document.all.hSalesRepName.value = setName;
}