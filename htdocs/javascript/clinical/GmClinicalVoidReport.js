
function fnOnPageLoad()
{
	
	if(objGridData.indexOf("rows", 0)==-1){
 		document.all["dataGridDiv"].innerHTML = "<BR>&nbsp; Please click Load!!!";  // Default Message is changed as part os MNTTASK-3766
		return true;
	}
	
	gridObj = initGrid('dataGridDiv',objGridData);
	gridObj.enableTooltips("false,true,true,true,true,true,true,true,true,true");  // One More column added called "Reason" as part os MNTTASK-3766
	gridObj.attachHeader(",#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,,#text_filter,,"); // One More column added called "Reason" as part os MNTTASK-3766
}

function fnReload()
{ 
	fnStartProgress('Y');
	document.frmStudyFilterForm.strOpt.value = "loadReport";
	document.frmStudyFilterForm.action="gmClinicalVoidReportAction.do?method=fetchVoidedForms";
	document.frmStudyFilterForm.submit();
}

function fnLoadReport()
{ 
	fnStartProgress('Y');
	document.frmStudyFilterForm.strOpt.value = "loadReport";
	document.frmStudyFilterForm.action="gmClinicalVoidReportAction.do?method=fetchVoidedForms";
	document.frmStudyFilterForm.submit();
}

function fnLoadFormData(formId,studyPeriodId,hPatientId,studyPeriodPK,PatListID,EventNo) {
	
	if (EventNo == 0){
		EventNo = '';
	}
	var form = document.getElementsByName('frmStudyFilterForm')[0];
	form.action ="/GmFormDataEntryServlet?hVoidFl=Y&Cbo_Form="+formId+"&Cbo_Period="+studyPeriodId+"&hStPerId="+studyPeriodPK+"&hAction=LoadQues&hPatientId="+hPatientId+"&hPatListID="+PatListID+"&cbo_EventNo="+EventNo;
	form.submit();
}
document.onkeypress = function(){
	if(event.keyCode == 13){
		fnLoadReport();
	}
}

//Description: This function will used to generate excel.
function fnDownloadXLS() {	
	gridObj.toExcel('/phpapp/excel/generate.php');
}