function fnSubmit() {
	// if (fnValidate() != false) {
	fnValidateTxtFld('refId', 'Reference ID');
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	var url = "/gmEditCheckSetupAction.do";
	document.getElementById("form").specification.disabled=false;
	document.getElementById("form").action = url;
	fnStartProgress();
	document.getElementById("form").submit();
}
function fnAddNew() {
	document.getElementById("refId").value = "";
	document.getElementById("description").value = "";
	document.getElementById("specification").value = "";
	document.getElementById("activeFlag").checked = true;
	document.getElementById("strOpt").value = "Save";
	document.getElementById("editID").value = "";
}
function fnSetVoid(str) {
	var n = eval(str);
	if (n == 94150) {
		document.getElementById("voidButton").disabled = false;
	} else {
		document.getElementById("voidButton").disabled = true;
	}
}

function fnVoidEditCheck(str) {
	var url = "/GmCommonCancelServlet?hTxnName=" + str
			+ "&hCancelType=VEDCHK&hTxnId=" + str;
	document.getElementById("form").action = url;
	document.getElementById("form").submit();
}
function fnEdit(str){
	var url = "/gmEditCheckSetupAction.do?strOpt=edit&editId="+str;
	windowOpener(url,"EditCheck","resizable=yes,scrollbars=yes,top=150,left=200,width=750,height=600");
}
function fnKeyRestrict(e) { 
	var pK = e ? e.which : window.event.keyCode;
	return pK != 13;
}
document.onkeypress=fnKeyRestrict;