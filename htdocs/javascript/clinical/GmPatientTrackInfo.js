function fnOnPageLoad()
{
	  var idIndex;
	  if(objGridData.indexOf("cell", 0)==-1){
		  return true;
	  }
			
	  gridObj = initGrid('dataGridDiv',objGridData);
	  gridObj.enableTooltips("true,true,false,false");
	 
	  gridObj.attachEvent("onRowSelect" ,function(rowId,cellIndex){
		  if (cellIndex==1 && accesslvl != 1){
					//pgToLoad = "/GmFormDataEntryServlet?strAction=Reload";
					//actinoUrl = "/menu/GmClinicalLayout.jsp?strPgToLoad="+pgToLoad+"&strDhtmlx=true&strStudyId="+studyId+"&strSiteId="+siteId+"&strPatientId="+patientId+"&strPeriodId="+rowId;
					if(rowId.indexOf("strAccrd") > 0)
						{
							idIndex = rowId.indexOf("?");
							var newActionUrl = rowId.substring(idIndex+1);
							//alert('&'+newActionUrl);
							fnSetSesnParams('&'+newActionUrl);
						}
					fnRedirectClinicalRequest(rowId);
				}
	  });
	  gridObj.attachEvent("onMouseOver",function(id,ind)
	{
		if (ind==1 && accesslvl != 1){
			gridObj.cells(id,ind).setAttribute("title","Click to enter form data");
			gridObj.setCellTextStyle(id,ind,"cursor:hand;"); 
		}
		else if (ind==0 && accesslvl != 1){
			gridObj.cells(id,ind).setAttribute("title","Click to view patient tracking form");
			gridObj.setCellTextStyle(id,ind,"cursor:hand;"); 
		}
		else
		{
			return false;
		}
		return true;
	});
}

function fnSetSesnParams(nodeId)
{
	dhtmlxAjax.get('/gmClinicalAction.do?strNodeId='+escape(nodeId)+'&strTempNodeId='+nodeId+'&ramdomId='+Math.random(),fnGetContextXML);
}
function fnGetContextXML(){}

function fnOpenPatientTracking()
{
	var action;
	action = "/gmRptPatientTracking.do?method=reportPatientTrackForm&strAccrd=report";
	actionUrl = "&strPgToLoad="+action;
	fnSetSesnParams(actionUrl);
	document.frmStudyFilterForm.action = action;
	document.frmStudyFilterForm.submit();
}