
function fnSubmit(){

	var error_msg="Please enter both From Date and To Date.";

	objStartDt = document.frmClinicalIRBReport.startDate;
	objEndDt = document.frmClinicalIRBReport.endDate;

	if(objStartDt.value != ""){
		DateValidateMMDDYYYY(objStartDt, '<b>From Date - </b>'+ message[1]);
	}

	if(objEndDt.value != ""){
		DateValidateMMDDYYYY(objEndDt, '<b>To Date - </b>'+ message[1]);
	}

	if((objStartDt.value=="" && objEndDt.value != "") || (objStartDt.value!="" && objEndDt.value =="")){
		Error_Details(error_msg);
	}

	if (ErrorCount > 0)
		{
			Error_Show();
			Error_Clear();
			return false;
		}	

	document.frmClinicalIRBReport.strOpt.value="search_summary_rpt";	 
	fnStartProgress('Y');
    document.frmClinicalIRBReport.submit();    
}
function fnApprTypeSubmit(){
	var error_msg="Please enter both From Date and To Date.";

	objStartDt = document.frmClinicalIRBReport.startDate;
	objEndDt = document.frmClinicalIRBReport.endDate;

	if(objStartDt.value != ""){
		DateValidateMMDDYYYY(objStartDt, '<b>From Date - </b>'+ message[1]);
	}

	if(objEndDt.value != ""){
		DateValidateMMDDYYYY(objEndDt, '<b>To Date - </b>'+ message[1]);
	}

	if((objStartDt.value=="" && objEndDt.value != "") || (objStartDt.value!="" && objEndDt.value =="")){
		Error_Details(error_msg);
	}

	if (ErrorCount > 0)
		{
			Error_Show();
			Error_Clear();
			return false;
		}	

	document.frmClinicalIRBReport.strOpt.value="search_irb_type_rpt";	 
	fnStartProgress('Y');
    document.frmClinicalIRBReport.submit();    
}

function fnEventSubmit(){
	var error_msg="Please enter both From Date and To Date.";

	objStartDt = document.frmClinicalIRBReport.startDate;
	objEndDt = document.frmClinicalIRBReport.endDate;

	if(objStartDt.value != ""){
		DateValidateMMDDYYYY(objStartDt, '<b>From Date - </b>'+ message[1]);
	}

	if(objEndDt.value != ""){
		DateValidateMMDDYYYY(objEndDt, '<b>To Date - </b>'+ message[1]);
	}

	if((objStartDt.value=="" && objEndDt.value != "") || (objStartDt.value!="" && objEndDt.value =="")){
		Error_Details(error_msg);
	}

	if (ErrorCount > 0)
		{
			Error_Show();
			Error_Clear();
			return false;
		}	

	document.frmClinicalIRBReport.strOpt.value="search_irb_event_rpt";	 
	fnStartProgress('Y');
    document.frmClinicalIRBReport.submit();    
}

function fnMediaTypeSubmit(){	

	document.frmClinicalIRBReport.strOpt.value="search_irb_media_rpt";	 
	fnStartProgress('Y');
    document.frmClinicalIRBReport.submit();    
}
function fnOnPageLoad()
{
	  if(objGridData.indexOf("cell", 0)==-1){	  	
		  return true;
	  }
	  gridObj = initGrid('dataGridDiv',objGridData); 
	  gridObj.enableTooltips("false,true,true,true,true,true,true,true");
}

function fnCheckAppr(){
var apprReason = document.getElementById('irbReasonId');
	  for (i =0; i< apprReason.length ;  i++) {
	    if (apprReason.options[i].value== 60525) { // Renewal is not applicable for Advertisement
	      apprReason.remove(i);
	    }
	  }

}
