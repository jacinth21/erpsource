function fnEditId(val)
{	
	document.frmClinicalSiteMap.hsiteMapID.value = val;
	document.frmClinicalSiteMap.strOpt.value = "edit";
	document.frmClinicalSiteMap.submit();
}

function fnSubmit()
{
//	alert(document.frmClinicalSiteMap.hsiteMapID.value);        
   if(fnValidate())
    {
	   document.frmClinicalSiteMap.strOpt.value = "save";	
	   fnStartProgress();
		document.frmClinicalSiteMap.submit();
	}
}

function fnValidate()
 {
var siteID =  document.frmClinicalSiteMap.siteId.value;
//var appDate = document.frmClinicalSiteMap.approvalDate.value;
var craName = document.frmClinicalSiteMap.craName.value;
//var studyList = document.frmClinicalSiteMap.studyList.value;
var siteNameList = document.frmClinicalSiteMap.siteNameList.value;
var irbTypeName = document.frmClinicalSiteMap.irbTypeName.value;
var studyPktSent = TRIM(document.frmClinicalSiteMap.studyPktSent.value);

if(isNaN(siteID))
{
    Error_Details(message[603]);
}

if(siteID.length < 2)
{
 Error_Details(message[607]);
} 

if(TRIM(siteID) == '' ||  craName == '0' || siteNameList == '0' || irbTypeName =='0' )
{
    Error_Details(message[604]);
}
DateValidateMMDDYYYY(document.frmClinicalSiteMap.studyPktSent,'Please Enter <b>Valid Date Format</b> For Study Packet Sent.')

if(dateDiff(document.frmClinicalSiteMap.currentDate.value,studyPktSent)>0){
	Error_Details(" Study Packet Sent Date Cannot be Greater than Current Date");
}
/* Date Comparision */
/* Commenting the code as the IRB Approval Date is changed to a label.


var firstIndex = appDate.indexOf ("/");

var lastIndex = appDate.lastIndexOf ("/");

var month = parseInt(appDate.substring (0, firstIndex), 10);
//alert(month);
var dat = parseInt(appDate.substring (firstIndex+1, lastIndex), 10);
//alert(dat);
var year = parseInt(appDate.substring (lastIndex+1, appDate.length), 10);
//alert(year);

var approvalDate = new Date;

approvalDate.setDate(dat);
approvalDate.setMonth(month-1); // January = 0
approvalDate.setFullYear(year); 

//alert(approvalDate);
var today = new Date;

if (approvalDate > today) 
 {
    Error_Details(message[609]);
 }*/

if (ErrorCount > 0)
 {
	Error_Show();
	Error_Clear();
	return false;
 }	

  return true;
 }
 
function fnReload()
{ 
 document.frmClinicalSiteMap.strOpt.value = "reload";
 document.frmClinicalSiteMap.submit();
}

function fnReset(res)
{ 
    document.frmClinicalSiteMap.siteNameList.value = 0;
	document.frmClinicalSiteMap.strOpt.value = "";
	document.frmClinicalSiteMap.hsiteMapID.value = "";
	document.frmClinicalSiteMap.siteId.value = "";
//	document.frmClinicalSiteMap.approvalDate.value = "";
	document.frmClinicalSiteMap.craName.value = "0";
	document.frmClinicalSiteMap.irbTypeName.value = "0";
	document.frmClinicalSiteMap.studyPktSent.value = "";
	document.frmClinicalSiteMap.msg.value = "";	
	document.frmClinicalSiteMap.tinNumber.value = "";
    fnReload();
}

function fnVoid()
{
//alert(document.frmClinicalSiteMap.hsiteMapID.value);
		document.frmClinicalSiteMap.action ="/GmCommonCancelServlet";
		document.frmClinicalSiteMap.hTxnId.value = document.frmClinicalSiteMap.hsiteMapID.value;	
		document.frmClinicalSiteMap.hTxnName.value = document.frmClinicalSiteMap.siteNameList.options[document.frmClinicalSiteMap.siteNameList.selectedIndex].text;
		document.frmClinicalSiteMap.hCancelType.value = 'VDSTE';
		document.frmClinicalSiteMap.hAction.value = 'Load';
		document.frmClinicalSiteMap.submit();
}


function fnOnPageLoad()
{
	  if(objGridData.indexOf("cell", 0)==-1){
		  return true;
	  }
		
	  gridObj = initGrid('dataGridDiv',objGridData);
	  gridObj.enableTooltips("true,true,false,false");
	 
	  gridObj.attachEvent("onRowSelect" ,function(rowId,cellIndex){
		  if (cellIndex == 1 || cellIndex == 2)
			  {					
					fnRedirectClinicalRequest(rowId);
			   }
		  else if(cellIndex == 0)
		  {
			fnEditId(gridObj.getUserData(rowId,"STUDYSITEID"));
		  }
			
	  });
		
	gridObj.attachEvent("onMouseOver",function(id,ind)
	{
		if (ind==1 || ind==2){
			gridObj.cells(id,ind).setAttribute("title","Click to load patient list");
			gridObj.setCellTextStyle(id,ind,"cursor:hand;"); 
			window.status='';			
			}
		else
		{
			return false;
		}
		return true;
	});
	gridObj.attachEvent("onMouseOut",function(id,ind)
	{
		if (ind==1 || ind==2)
		{
			window.status='';			
		}
		else
		{
			return false;
		}
		return true;
	});
}