var objGridData;
var queryStatus; 

function fnOnPageLoad()
{
	gridObj = initGrid('dataGridDiv',objGridData);
	gridObj.enableTooltips("true,false,false,false");
	gridObj.enableEditEvents(false, false, false);
	
	///For edit functionality
	gridObj.attachEvent("onRowSelect" ,function(rowId,cellIndex){
    	if (cellIndex==0){
			//alert("View Query");
		}
	});
	
	/////
	gridObj.attachEvent("onMouseOver",function(id,ind)
	{
		if (ind==0){
			gridObj.cells(id,ind).setAttribute("title","Click here to view query");
			gridObj.setCellTextStyle(id,ind,"cursor:hand;"); 
		}
		else
		{
			return false;
		}
		return true;
	});
}

function fnQueryDetailsSubmit(){
	
	var form = document.getElementsByName('frmQueryForm')[0];

	var status = document.frmQueryForm.status.value;
	var comments = document.frmQueryForm.comments.value;
	var queryId = document.frmQueryForm.queryId.value;
	var listQueries = document.frmQueryForm.listQueries.value;

	if(status!="92441" && queryId == ""){
		Error_Details("Please select a <b> Query </b> ");
	}

    if (ErrorCount > 0){
		Error_Show();
        Error_Clear();
        return false;
    }
	
	if(status == ""){
		Error_Details("Please select a <b> Status </b> for the query");
	}

	if(comments == ""){
		Error_Details("Please enter <b> Comments </b>");
	}

    if (ErrorCount > 0){
		Error_Show();
        Error_Clear();
        return false;
    }

	document.frmQueryForm.action = '/gmQuery.do?method=fetchQueryDetails';
	document.frmQueryForm.strOpt.value = 'save';
	document.frmQueryForm.dataSaved.value = 'true';
	fnStartProgress('Y');
	document.frmQueryForm.submit();
}

function fnReload(queryLevelId,patientListId,patientAnsListId,listQueries) {	
	parent.frames[0].location.href = '/gmQuery.do?method=fetchQueryReport&queryLevelId='+queryLevelId+'&patientAnsListId='+patientAnsListId+'&patientListId='+patientListId+'&listQueries='+listQueries;
}

function fnReloadLeftFrame(){
	 var listQueries = document.frmQueryForm.listQueries.value;
	 // alert(listQueries);
            if(document.frmQueryForm.dataSaved.value == 'true'
			 && fromContainer == 'true'){

                       
                        var queryLevelId = document.frmQueryForm.queryLevelId.value;
                        var patientListId = document.frmQueryForm.patientListId.value;
                        var patientAnsListId = document.frmQueryForm.patientAnsListId.value;
                        if(listQueries == 'showAll'){
                                    queryLevelId = "";
                                    patientListId = "";
                                    patientAnsListId = "";
                        }
                        window.setTimeout(fnReload(queryLevelId,patientListId,patientAnsListId,listQueries),5);
            }
			if(queryStatus == 'Closed'){         
						if(document.frmQueryForm.submitBtn!=null)
                        document.frmQueryForm.submitBtn.disabled = "true";
            }
}

function fnNewQuery() {	
	document.frmQueryForm.action = '/gmQuery.do?method=fetchQueryDetails';
	document.frmQueryForm.target = 'RightFrame1';
	fnStartProgress('Y');
	document.frmQueryForm.submit();
}


function fnVoidQuery() {
	var form = document.getElementsByName('frmQueryForm')[0];

	var queryId = document.frmQueryForm.queryId.value;

	if(queryId == "") {
		Error_Details("Please select a <b> Query </b> to void");
	}

    if (ErrorCount > 0) {
		Error_Show();
        Error_Clear();
        return false;
    }


	var answer = confirm("Are you sure?");
	if (!answer) {
	    return false;
    }
	
	form.action ="/GmCommonCancelServlet";
	form.hTxnId.value = queryId;
	form.hTxnName.value = ' Query ID ' + queryId; 
	form.submit();
}

function fnLoadFormData() {
	var formId = document.frmQueryForm.hFormID.value;
	var studyPeriodId = document.frmQueryForm.hStudyPeriodID.value;
	var PatientId = document.frmQueryForm.hPatientID.value;
	var studyPeriodPK = document.frmQueryForm.hStudyPeriodPK.value;
	windowOpener("/GmFormDataEntryServlet?Cbo_Form="+formId+"&Cbo_Period="+studyPeriodId+"&hStPerId="+studyPeriodPK+"&hAction=LoadQues&hPatientId="+PatientId,"","scrollbars=yes,top=150,left=100,width=950,height=490");	
}
