var gridObj = '';
function fnLoadPaymentTo(){
	var paymentTo = document.frmClinicalPayment.paymenttoId.options[document.frmClinicalPayment.paymenttoId.selectedIndex].text;
	if(paymentTo !="None")
	{
		document.getElementById("payable").value= ""+document.frmClinicalPayment.paymenttoId.options[document.frmClinicalPayment.paymenttoId.selectedIndex].text;	 
	}

 // Change of Patient - change the patient id in the grid
	
	frm = document.frmClinicalPayment;
	var paymenttoId = frm.paymenttoId.value;
	var requestTypeId = frm.requestTypeId.value;
	var gridrows =gridObj.getAllRowIds(",");

		if(gridrows.length>0){
			var gridrowid;
			var gridrowsarr = gridrows.toString().split(",");
			var columnCount = gridObj.getColumnsNum();

			 for (var rowid = 0; rowid < gridrowsarr.length; rowid++) {
					gridrowid = gridrowsarr[rowid];
					reasonVal = gridObj.cellById(gridrowid, 1).getValue();
				// This condition is added for Request type to Patient Payment only.
				if(reasonVal != '' && requestTypeId == '60623') 
				 {
					gridObj.cellById(gridrowid, 3).setValue(""+paymenttoId);
				 }
			 }
		}
}

function fnLoadPayable(){
	document.frmClinicalPayment.strOpt.value="load_payable_to";	
    document.frmClinicalPayment.submit();    
}

function fnOnPageLoad()
{	 
	  gridObj = initGrid('dataGridDiv',objGridData); 
	  gridObj.enableTooltips("false,false,false,false,false");
      var gridrows =gridObj.getAllRowIds(",");
	  if(gridrows.length==0){
		  addRow();
		  addRow();
		  addRow();
  		  addRow();
		  addRow();
	  }	

	  frm = document.frmClinicalPayment;
	  
	  if(frm.paymentRequestId.value==null || frm.paymentRequestId.value=='' && SwitchUserFl != 'Y')
	{
		document.getElementById("requestTypeId").disabled = false;
		frm.Btn_Submit.disabled=false;
	}
	  else if(frm.paymentRequestId.value==null || frm.paymentRequestId.value=='' && SwitchUserFl == 'Y')
	{
		 document.getElementById("requestTypeId").disabled = false;
		 frm.Btn_Submit.disabled=true;
	}else
		{
		 document.getElementById("requestTypeId").disabled = true;
		 frm.Btn_Submit.disabled=true;
		}
	 if (frm.strOpt.value == 'edit_payment' && SwitchUserFl != 'Y')
	 {
		 frm.Btn_Submit.disabled=false;
	 }
		 
	gridObj.attachEvent("onEditCell",doOnCellEdit);

	if(frm.statusId.value=='60618' || frm.tinNumber.value=='' ||frm.tinNumber.value=='N/A' ){
		document.getElementById('ReconfRA').style.display='none';
		}

	if((frm.tinReason.value)!=''){
		document.getElementById('Reason').style.display='block';
		document.getElementById('tinReason').value=frm.tinReason.value;
	}	
}
function doOnCellEdit(stage, rowId, cellInd, nValue, oValue) { 
	frm = document.frmClinicalPayment;
	var requestTypeId = frm.requestTypeId.value;
	var paymenttoId = frm.paymenttoId.value;
				
	  if(cellInd == 1)
      { 
		    rowID = rowId;
			var reasonVal = gridObj.cellById(rowID, 1).getValue();
			if(reasonVal == '')
		    {
				gridObj.cellById(rowID, 4).setValue("");
			}
			else if(reasonVal == "60636" || reasonVal == "60639" || reasonVal == "60646" || reasonVal == "60711" || reasonVal == "60717")
			{
				gridObj.cellById(rowID, 4).setValue("6301");
			}
			else if (reasonVal == "60637" || reasonVal == "60641" || reasonVal == "60713")
			{
				gridObj.cellById(rowID, 4).setValue("6305");
			}
			else if (reasonVal == "60638" || reasonVal == "60642" || reasonVal == "60715")
			{
				gridObj.cellById(rowID, 4).setValue("6306");
			}
			else if (reasonVal == "60631" || reasonVal == "60719" || reasonVal == "60661" || reasonVal == "60671" || reasonVal == "60666" || reasonVal == "60720")
			{
				gridObj.cellById(rowID, 4).setValue("");
			}
			
			/*if(requestTypeId == '60623')
			{
				if(reasonVal != '')
				{
					gridObj.cellById(rowID, 2).setValue(""+paymenttoId);
				}
				else
				{
						gridObj.cellById(rowID, 2).setValue("");
				}
			}*/
			
			// The Patient payment 60623 
			if(requestTypeId == '60623'){
				if(reasonVal == '60800' || reasonVal == '60669') {
					var comboTimePt = gridObj.getCustomCombo(rowId,4);
					comboTimePt.clear();
					comboTimePt.put('','[Choose One]'); 												
					for (var j=0;j<PeriodListLen;j++){	
						arr = eval("TimePointArr"+j);
						arrobj = arr.split(",");
						id = arrobj[0]; 
						name = arrobj[1];
						//For Annuals time point 60800 and period of time points 12,24,36,48,60 mo
						if(reasonVal == '60800'){
							if(id == '6305' || id == '6306' || id=='6307' || id == '6308' || id == '6310') 
								comboTimePt.put(id,name);
						}
						// for Others time point 60669 and period of all time points
						if(reasonVal == '60669') {
							comboTimePt.put(id,name); 
						}
					}
					gridObj.cellById(rowId, 4).setValue(''); 
				} // end if for requestTypeId
			} // end if for reasonVal
    } 
 return true;
}
function addRow(){
	gridObj.addRow(gridObj.getUID(),'');
}

function removeSelectedRow(){

	var gridrows=gridObj.getSelectedRowId();	
	if(gridrows!=undefined){
		var NameCellValue ;
		var gridrowsarr = gridrows.toString().split(",");
		var added_row;
		for (var rowid = 0; rowid < gridrowsarr.length; rowid++) {
			gridrowid = gridrowsarr[rowid];			
			gridObj.deleteRow(gridrowid);						
		}
	}else{
		Error_Clear();
		Error_Details(" Please Select a row to Delete.");				
		Error_Show();
		Error_Clear();
	}
}

function fnPrint(id)
{
	  windowOpener("/gmClinicalPayment.do?strOpt=print&paymentRequestId="+id,"","scrollbars=no,top=150,left=100,width=720,height=500");
}

function fnSubmit(){
	var errorPD = 0;
	var errorTP = 0;
	var errorAm1 = 0;
	var errorAm2 = 0;
	var errorAm3 = 0;
	var errorPID1 = 0;
	var errorPID2 = 0;
	if(frm.tinNumber.value=='N/A')
	{
		fnValidateTxtFld('tinReason','Reason');	
	}
	
	
	var tinreason=document.getElementById('tinReason').value.length;
	if(tinreason > 4000)
	{
		Error_Details("Please enter less than 4000 characters in Reason field.");	
	}
	
	document.getElementById("requestTypeId").disabled = false;
	fnValidateDropDn('requestTypeId',' Request Type');
	fnValidateDropDn('paymenttoId',' Payment To');
	fnValidateDropDn('statusId',' Status');
	fnValidateTxtFld('payable',' Payable To');
	objPaymentDt = document.frmClinicalPayment.paymentDate;
	if(objPaymentDt.value!=''){
			DateValidateMMDDYYYY(objPaymentDt, '<b>Payment Date - </b>'+ message[1]);
	}
	var status = document.frmClinicalPayment.statusId.options[document.frmClinicalPayment.statusId.selectedIndex].value;
	if(status=='60617' || status=='60618'){
		fnValidateTxtFld('paymentDate',' Payment Date');
		fnValidateTxtFld('checkNumber',' Check Numbers');
	}

	var gridrows =gridObj.getAllRowIds(",");
	//var gridrows =gridObj.getChangedRows(",");
	var finalData='';
	var requestType = document.frmClinicalPayment.requestTypeId.options[document.frmClinicalPayment.requestTypeId.selectedIndex].value;
	var paymenttoId = document.frmClinicalPayment.paymenttoId.options[document.frmClinicalPayment.paymenttoId.selectedIndex].value;
		if(gridrows.length==0){
			errorPD = errorPD + 1;
			if(errorPD == 1)
				Error_Details("Please enter payment detail.");		
		}
		if(gridrows.length>0){
			var gridrowid;
			var gridrowsarr = gridrows.toString().split(",");
			var columnCount = gridObj.getColumnsNum();

			 for (var rowid = 0; rowid < gridrowsarr.length; rowid++) {
					gridrowid = gridrowsarr[rowid];
					for(var col=0;col<columnCount;col++){
						finalData = finalData+gridObj.cellById(gridrowid, col).getValue();
						if(col!=columnCount-1){
							finalData = finalData+'^';
						}
					}
					finalData = finalData+'|';	
			 }
			var regExp=new RegExp("[^^^^^|]+");			
			 if(!regExp.test(finalData)){
				 errorPD = errorPD + 1;
				 if(errorPD == 1)
					 Error_Details("Please enter payment detail.");		
			 }else{
				 finalData='';
				 for (var rowid = 0; rowid < gridrowsarr.length; rowid++) {
						gridrowid = gridrowsarr[rowid];		
						var tempcoldata='';
						for(var col=0;col<columnCount;col++){
							tempcoldata = tempcoldata+gridObj.cellById(gridrowid, col).getValue();
								if(col!=columnCount-1){
									tempcoldata = tempcoldata+'^';
								}
						}
						if(tempcoldata!="^^^^^"){
								for(var col=1;col<columnCount;col++){
									 if(TRIM(gridObj.getColumnLabel(col))== 'Amount'){
										var amount=gridObj.cellById(gridrowid, col).getValue();										
												 if(amount == '' ){			
                                     				errorAm1 = errorAm1 + 1;
													if(errorAm1 == 1)
														Error_Details("Please enter Amount.");
												 }
												 else if(isNaN(amount)){
													errorAm2 = errorAm2 + 1;
													if(errorAm2 == 1)
														Error_Details("Amount value should be decimal only.");													
												 }else if(amount>99999999.99){
													errorAm3 = errorAm3 + 1;
													if(errorAm3 == 1)
														 Error_Details("Amount value should not be greater than $99999999.99.");													
												 }
									  }
									 if(TRIM(gridObj.getColumnLabel(col))== 'Reason For Payment'){
										var paymentreason=gridObj.cellById(gridrowid, col).getValue();							
										 if(paymentreason == '' ){	
											errorPD = errorPD + 1;
						                    if(errorPD == 1)
											Error_Details("Reason For Payment selection is mandatory.");
										 }
									  }
									 if(TRIM(gridObj.getColumnLabel(col))== 'GL Account'){
											var glAccount = gridObj.cellById(gridrowid, col).getValue();							
											 if(glAccount == '' ){	
												errorPD = errorPD + 1;
							                    if(errorPD == 1)
												Error_Details("<b>GL Account</b> selection is mandatory.");
											 }
									  }	
									 if(TRIM(gridObj.getColumnLabel(col))== 'Patient'){
										var patient=gridObj.cellById(gridrowid, col).getValue();
										 if(patient == '' && (requestType=='60621' || requestType=='60622' || requestType=='60623' || requestType=='60624' || requestType=='60625'|| requestType=='60627')){
											errorPID1 = errorPID1 + 1;
											if(errorPID1 == 1)
												Error_Details("<b>Patient selection</b> is mandatory for selected Payment Request Type.");
										 }else if(requestType=='60623' && patient!=paymenttoId){
											errorPID2 = errorPID2 + 1;
											if(errorPID2 == 1)
												Error_Details("Please select same patient as payment to for request type Patient Payment.");
										 }
									  }	
									 if(TRIM(gridObj.getColumnLabel(col))== 'Time Point'){
										 var timepoint=gridObj.cellById(gridrowid, col).getValue();
										 if(timepoint == '' && (requestType=='60621' || requestType=='60622' || requestType=='60623' || requestType=='60624' || requestType=='60625'|| requestType=='60627')){		
											errorTP = errorTP + 1;
											if(errorTP == 1) 
												Error_Details("<b>Time Point</b> selection is mandatory for selected Payment Request Type.");
										}
									  }	

										finalData = finalData+gridObj.cellById(gridrowid, col).getValue();
										if(col!=columnCount-1){
											finalData = finalData+'^';
										}
										//alert(' Cell value'+gridObj.cellById(gridrowid, col).getValue() +' Column Name'+TRIM(gridObj.getColumnLabel(col)));							
								}					
								finalData = finalData+'|';	
					}
				 }
			 }
			 //alert("finalData:"+finalData);
		}

			if (ErrorCount > 0)
			{
				Error_Show();
				Error_Clear();
				return false;
			}	

			var errorPD = 0;
		errorTP = 0;
		errorAm1 = 0;
		errorAm2 = 0;
		errorAm3 = 0;
		errorPID1 = 0;
		errorPID2 = 0;
		document.frmClinicalPayment.paymentDetailData.value=finalData;
		document.frmClinicalPayment.strOpt.value="save_payment";	 
		fnStartProgress('Y');
		document.frmClinicalPayment.submit();
}
function fnEdit(){
	document.frmClinicalPayment.paymentRequestId.value="GM-PRQ-8";
	document.frmClinicalPayment.strOpt.value="edit_payment";
    document.frmClinicalPayment.submit();    
}
function fnVoidPaymentDetail(paymentId, paymentDetailsId, studyPeriod){
	document.frmClinicalPayment.action ="/GmCommonCancelServlet?hTxnId="+paymentDetailsId+"&hCancelType=VDPRQD&hTxnName="+paymentId+ ' ' +studyPeriod;	
	document.frmClinicalPayment.submit();
}
function fnVoidPaymentRequest(paymentId){   
	document.frmClinicalPayment.action ="/GmCommonCancelServlet?hTxnId="+paymentId+"&hTxnName="+paymentId+"&hCancelType=VDPRQ";
	document.frmClinicalPayment.submit();
}
function fnShowTinReason(){
	document.getElementById('Reason').style.display='block';
	document.getElementById('tin').innerHTML=' &nbsp;N/A';
	document.frmClinicalPayment.tinNumber.value='N/A';
	
	
}

