function fnLoadDownloadHistory(){

	    document.frmClinicalDownload.haction.value="load_download_history";	    
		document.frmClinicalDownload.selFormName.value=document.frmClinicalDownload.formName.options[document.frmClinicalDownload.formName.selectedIndex].text;
	    document.frmClinicalDownload.action ="/gmClinicalDownload.do";
	    document.frmClinicalDownload.submit();    
}
function fnDownload(){
	    document.frmClinicalDownload.haction.value="download_file";	 
		document.frmClinicalDownload.selFormName.value=document.frmClinicalDownload.formName.options[document.frmClinicalDownload.formName.selectedIndex].text;
		document.frmClinicalDownload.action ="/gmClinicalDownload.do";
		fnStartProgress();
	    document.frmClinicalDownload.submit();    
}

function fnShowReason(){	
	if (document.frmClinicalDownload.cancelCheck)
	{
		var e = document.getElementById("cancelDiv");
		if (document.frmClinicalDownload.cancelCheck.checked)
		{
			e.style.display = 'block';
		}
		else if (!document.frmClinicalDownload.cancelCheck.checked)
		{
		  e.style.display = 'none';
		}    
	}
}

function fnSubmit(){

	if (document.frmClinicalDownload.cancelCheck.checked)
	{		
		 fnValidateDropDn('cancelId',' Reason');
	}
	else{
		Error_Details('Please select the Cancel checkbox');
	}
	 if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	document.frmClinicalDownload.haction.value="cancel_download";	
	document.frmClinicalDownload.action ="/gmClinicalDownload.do";
		fnStartProgress();
    document.frmClinicalDownload.submit();    
}


function fnOnPageLoad(){
	if (document.getElementById("downloadDiv"))
	{
		var e = document.getElementById("downloadDiv");
		if(document.frmClinicalDownload.formName.value!= '0')
		   e.style.display = 'block';		
		
	}
	fnShowReason();
}