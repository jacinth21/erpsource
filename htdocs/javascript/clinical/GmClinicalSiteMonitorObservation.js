
function fnSubmit(){
	var objStatus;
	objStatus = document.frmClinicalObservation.status;
	fnValidateDropDn('patientId',' Patient');
	fnValidateDropDn('periodId',' Time Period');
	fnValidateDropDn('actionRequired',' Action Required');
	fnValidateTxtFld('targetResDate',' Target Resolution Date');
	if(objStatus.value == 60487)
		fnValidateTxtFld('actualResDate',' Actual Resolution Date');
	objTargetDt = document.frmClinicalObservation.targetResDate;
	objActualDt = document.frmClinicalObservation.actualResDate;
	DateValidateMMDDYYYY(objTargetDt, '<b>Target Resolution Date - </b>'+ message[1]);
	DateValidateMMDDYYYY(objActualDt, '<b>Actual Resolution Date - </b>'+ message[1]);
	if(objStatus.value == 60486 && objActualDt.value != '')
	{
		Error_Details("Actual resolution date is not valid for the open status.");
	}
	if (ErrorCount > 0)
		{
			Error_Show();
			Error_Clear();
			return false;
		}	

	document.frmClinicalObservation.haction.value="save_observation";	 
	fnStartProgress('Y');
    document.frmClinicalObservation.submit();    
}
function fnAddMore(){
	document.frmClinicalObservation.haction.value="add_more";	 
    document.frmClinicalObservation.submit();    
}
function fnNextIssue(){
	document.frmClinicalObservation.haction.value="load_next_issue";	 
    document.frmClinicalObservation.submit();    
}
function fnVoid(sitenm){
	var val=document.frmClinicalObservation.observationId.value
		document.frmClinicalObservation.action ="/GmCommonCancelServlet";
		document.frmClinicalObservation.hTxnId.value = val;
		document.frmClinicalObservation.hTxnName.value = sitenm;
		document.frmClinicalObservation.submit();
}