function fnSubmit()
{
     fnChangeCheck();    
    //alert(document.frmPatient.hPKeyID.value);
    if(fnValidate())
    {
    	document.frmPatient.hPatientID.value = document.frmPatient.Txt_PatientID.value;
	//	document.frmPatient.hStudyID.value = document.frmPatient.Cbo_StudyID.value;
		document.frmPatient.hAction.value = "Edit";
		document.frmPatient.save.disabled =true;
		fnStartProgress();
		document.frmPatient.submit();
	}
	
}

function fnChangeCheck()
{
 if(document.frmPatient.Chk_SurgeryComplete.checked == true)
    {
    	 document.frmPatient.Chk_SurgeryComplete.value = "Y";
    }
    else
    {
   		 document.frmPatient.Chk_SurgeryComplete.value = "";
    }
}

function fnValidate()
{
//Validating the field Patient IDE
fnValidateTxtFld('Txt_RPatientIDE', 'Patient IDE');

var surgeryDate =  document.frmPatient.Txt_SurgeryDate.value;
var surgeonId =  document.frmPatient.Cbo_SurgeonID.value;
var treatment =  document.frmPatient.Cbo_SurgType.value;
var cFlag = document.frmPatient.Chk_SurgeryComplete.checked;
 
//alert(surgeryDate+" "+surgeonId+ " "+treatment+ " "+ cFlag ); 

  if( surgeryDate != "" && (surgeonId == "0" || treatment == "0") ) 
   {
    Error_Details(message[605]);
  }
  if(cFlag && (surgeryDate == "" || surgeonId == "0" || treatment == "0"))
   {
    Error_Details(message[606]);
   }
  CommonDateValidation(document.frmPatient.Txt_SurgeryDate, format, message[611]);
  if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}	
return true;	
}

function fnVoid()
{
		document.frmPatient.action ="/GmCommonCancelServlet";
		patientId=document.frmPatient.hPatientID.value;
		document.frmPatient.hTxnId.value = patientId;	
		document.frmPatient.hCancelType.value = 'VDPAT';
		document.frmPatient.hAction.value = 'Load';
		document.frmPatient.submit();
}

function fnReset()
{
	//document.frmPatient.hStudyID.value = document.frmPatient.Cbo_StudyID.value;
	document.frmPatient.hAction.value = "Load";		
	document.frmPatient.hPatientID.value  = "";
	document.frmPatient.hPKeyID.value = "";
	document.frmPatient.hCancelType.value  = "";
	document.frmPatient.hTxnId.value  = "";
	document.frmPatient.Cbo_PatientID.value  = "0";
	document.frmPatient.Txt_PatientID.value  = "";
	document.frmPatient.Txt_RPatientIDE.value = "";
	document.frmPatient.Cbo_AccountID.value  = "0";
	document.frmPatient.Cbo_SurgeonID.value  = "0";
	document.frmPatient.Cbo_SurgType.value  = "0";
	document.frmPatient.Txt_SurgeryDate.value  = "";
	document.frmPatient.submit();
}

//function fnLoadStudyList(val)
//{
//	document.frmPatient.hAction.value = "Load";
//	document.frmPatient.hStudyID.value = val;
	//document.frmPatient.submit();
//}


function fnChangePatientID(val)
{
	if (val != 0) {
		document.frmPatient.Txt_PatientID.value = document.frmPatient.Cbo_PatientID.value;
		document.frmPatient.hPatientID.value = val;
	} else {
		document.frmPatient.Txt_PatientID.value = '';
		document.frmPatient.hPatientID.value = '';
	}
 
}

function fnLoadRep(val)
{
	//document.frmPatient.hStudyID.value = document.frmPatient.Cbo_StudyID.value;
	document.frmPatient.Cbo_PatientID.value = val;
	
	  if(document.frmPatient.Cbo_PatientID.selectedIndex == -1)
		{
			document.frmPatient.Cbo_PatientID.selectedIndex = 0;
	   		Error_Details(message[610]);
	 	}
	   if (ErrorCount > 0)
		{
			Error_Show();
			Error_Clear();
			return false;
		}
	
	document.frmPatient.hPatientID.value = val;
	document.frmPatient.hAction.value = "EditLoad";	   
	fnStartProgress('Y');
    document.frmPatient.submit();
} 

function fnLoadAccount(val)
{ 
  //document.frmPatient.hStudyID.value = document.frmPatient.Cbo_StudyID.value;
  document.frmPatient.hPatientID.value = val;
  document.frmPatient.hAction.value = "Reload";
  fnChangeCheck();
  document.frmPatient.submit();
}

function fnStudyBoard()
{
	var action;
	action = "/gmRptPatientTracking.do?method=reportPatientTrackInfo&strAccrd=report";
	actionUrl = "&strPgToLoad="+action;
	fnSetSesnParams(actionUrl);
	//document.frmPatient.hStudyID.value = document.frmPatient.Cbo_StudyID.value;
	document.frmPatient.hPatientID.value = document.frmPatient.Cbo_PatientID.value;
	document.frmPatient.action = action;
	//fnStartProgress();
	document.frmPatient.submit();
}

function fnSetSesnParams(nodeId)
{	
	dhtmlxAjax.get('/gmClinicalAction.do?strNodeId='+escape(nodeId)+'&strTempNodeId='+nodeId+'&ramdomId='+Math.random(),fnGetContextXML);
}
function fnGetContextXML(){}
function fnLoad()
{
 document.frmPatient.hAction.value = objAction;
 //alert(document.frmPatient.hAction.value);
 if(document.frmPatient.hAction.value == "Edit")
  {
   document.frmPatient.Cbo_AccountID.disabled = true;   
  }
  else
  {
   document.frmPatient.Cbo_AccountID.disabled = false;   
  }
}
function fnOnPageLoad()
{
	var patId;
	document.frmPatient.btn_patient_Tracking.disabled = true;
	patId =  document.frmPatient.Txt_PatientID.value;
	if(patId!='' && lockedForm =='' && SwitchUserFl != 'Y')
	{
		document.frmPatient.btn_patient_Tracking.disabled = false;
	}
	if(objAction=="EditLoad" && lockedForm =='' && SwitchUserFl != 'Y'){
		document.frmPatient.btn_surg_Intrvn.disabled = false;
	}
	if(showGrid && objGridData !=''){
		gridObj = initGrid('dataGridDiv',objGridData);
	}
	
	//PMT-39479: Amnios study label changes
	//to get the study id and load default values
	var studyIds = document.frmPatient.hStudyID.value;
	var patientIDEObj = document.frmPatient.Txt_RPatientIDE;
	
	if(studyIds == 'GPR009' && patientIDEObj.value ==''){
		patientIDEObj.value ='09';
	}
}
function fnEditSurgicalDetails(surgInvnNo,patientLId){
	document.frmPatient.hSurgInvnNo.value=surgInvnNo;
	document.frmPatient.hPatientLId.value=patientLId;
	document.frmPatient.Cbo_AccountID.disabled=false;
	var opt_accid = document.getElementById("Cbo_AccountID");
	var siteName = opt_accid.options[opt_accid.selectedIndex].text;
	  document.frmPatient.hSiteName.value = siteName;
	document.frmPatient.hAction.value = "LoadSurg";
	document.frmPatient.submit();
}
function fnLoadSurgicalDetails(){
	document.frmPatient.Cbo_AccountID.disabled=false;
	var opt_accid = document.getElementById("Cbo_AccountID");
	var siteName = opt_accid.options[opt_accid.selectedIndex].text;
	  document.frmPatient.hSiteName.value = siteName;
	  document.frmPatient.hAction.value = "LoadSurg";
	  fnStartProgress();
	  document.frmPatient.submit();
}