var objGridData;

function fnReload()
{ 
	document.frmStudyFilterForm.action="gmClinicalTrainingAction.do?method=fchTrainingSummary"
	document.frmStudyFilterForm.strOpt.value = "reloadReport";
	document.frmStudyFilterForm.submit();
}

function fnLoadReport()
{ 
	document.frmStudyFilterForm.strOpt.value = "loadReport";
		document.frmStudyFilterForm.action="gmClinicalTrainingAction.do?method=fchTrainingSummary"
			fnStartProgress('Y');
	document.frmStudyFilterForm.submit();
}

function fnOnPageLoad()
{

	if(objGridData.indexOf("rows", 0)==-1){
 		document.all["dataGridDiv"].innerHTML = "<BR>&nbsp; No Data Found!!!";
		return true;
	}
	
	gridObj = initGrid('dataGridDiv',objGridData);
	gridObj.enableTooltips("true,false,false,false,false");
	gridObj.enableEditEvents(true, false, false);

	///For edit functionality
	gridObj.attachEvent("onRowSelect" ,function(rowId,cellIndex){
    	if (cellIndex==0){
			document.frmStudyFilterForm.action="gmClinicalTrainingSetupAction.do?strTrainingRecordId="+rowId;
			document.frmStudyFilterForm.submit();
		}
	});
	
	/////
	gridObj.attachEvent("onMouseOver",function(id,ind)
	{
		if (ind==0){
			gridObj.cells(id,ind).setAttribute("title","Click here to Edit");
			gridObj.setCellTextStyle(id,ind,"cursor:hand;"); 
		}
		else
		{
			return false;
		}
		return true;
	});
}	