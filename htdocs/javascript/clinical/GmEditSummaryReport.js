var objGridData;


function fnReload()
{ 
	document.frmStudyFilterForm.action="gmEditCheckReportAction.do?method=fetchEditCheckSummary"
	document.frmStudyFilterForm.strOpt.value = "";
	document.frmStudyFilterForm.submit();
}

function fnLoadReport()
{ 
	document.frmStudyFilterForm.strOpt.value = "loadReport";
	document.frmStudyFilterForm.action="gmEditCheckReportAction.do?method=fetchEditCheckSummary"
		fnStartProgress('Y');
	document.frmStudyFilterForm.submit();
}


function fnLoadIncidentReport(formId,referenceId)
{ 
	
   formId = formId.replace("#","");
   formId = formId.replace("#","");

   referenceId = referenceId.replace("#","");
   referenceId = referenceId.replace("#","");

	document.frmStudyFilterForm.strOpt.value = "loadReport";
	document.frmStudyFilterForm.referenceId.value=referenceId;
	document.frmStudyFilterForm.incidentStatus.value = '';
	document.frmStudyFilterForm.action="gmEditCheckReportAction.do?method=fetchEditCheckIncidentReport";
	document.frmStudyFilterForm.submit();
}

function fnLoadIncidentReportWithStatus(formId,referenceId)
{ 
   formId = formId.replace("#","");
   formId = formId.replace("#","");

   referenceId = referenceId.replace("#","");
   referenceId = referenceId.replace("#","");

	document.frmStudyFilterForm.strOpt.value = "loadReport";
	document.frmStudyFilterForm.referenceId.value=referenceId;
	document.frmStudyFilterForm.incidentStatus.value = '93055';
	document.frmStudyFilterForm.action="gmEditCheckReportAction.do?method=fetchEditCheckIncidentReport";
	document.frmStudyFilterForm.submit();
}

function fnChangeReferenceStatus(rowId, referenceId) {

   rowId = rowId.replace("#","");
   rowId = rowId.replace("#","");

   referenceId = referenceId.replace("#","");
   referenceId = referenceId.replace("#","");

	var form = document.getElementsByName('frmStudyFilterForm')[0];

	form.action ="/GmCommonCancelServlet";
	form.hTxnId.value = rowId;
	form.hTxnName.value = ' Reference ID ' + referenceId; 
	form.hCancelType.value = 'VEDSUM';
	form.submit();
}

function fnVoidReference(rowId, referenceId) {
   rowId = rowId.replace("#","");
   rowId = rowId.replace("#","");

   referenceId = referenceId.replace("#","");
   referenceId = referenceId.replace("#","");
	
	var form = document.getElementsByName('frmStudyFilterForm')[0];

	var answer = confirm("Are you sure you want to void "+referenceId+" ?");
	if (!answer) {
	    return false;
    }
	
	form.action ="/GmCommonCancelServlet";
	form.hTxnId.value = rowId;
	form.hTxnName.value = ' Reference ID ' + referenceId; 
	form.hCancelType.value = 'VEDCHK';
	form.submit();
}

function fnOnPageLoad()
{
	if(objGridData.indexOf("rows", 0)==-1){
 		document.all["dataGridDiv"].innerHTML = "<BR>&nbsp; No Data Found!!!";
		return true;
	}
	gridObj = new dhtmlXGridObject('dataGridDiv');
	gridObj.setSkin("dhx_skyblue");
	gridObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gridObj.enableTooltips  ("false,true,true,true,true,true,true,true,true,true,true");
	gridObj.enableEditEvents("true,false,false,false,false,false,false,false,false,true,false");
	gridObj.enableMultiline(true);
	gridObj.init();
	gridObj.loadXMLString(objGridData);	
}	