function fnLoad()
{
	var opt = document.frmClinicalSurgeonSetup.strOpt.value;
	var lockedForm = document.frmClinicalSurgeonSetup.lockedStudyDisable.value;

	if(opt == 'edit' && SwitchUserFl != 'Y')
	 {
	    document.frmClinicalSurgeonSetup.surgeonNameList.disabled = true;
	    if(lockedForm ==''){
	    	document.frmClinicalSurgeonSetup.btn_void.disabled = false;
	    }
	 }
	 else
	 {
	    document.frmClinicalSurgeonSetup.surgeonNameList.disabled = false;
	    document.frmClinicalSurgeonSetup.btn_void.disabled = true;
	 }
 
	 if(opt == 'save' || opt == 'update' )
	  {
	     document.frmClinicalSurgeonSetup.hsurgeonSetupID.value = "";    
	     document.frmClinicalSurgeonSetup.lastName.value = '';
		 document.frmClinicalSurgeonSetup.firstName.value ='';
		 document.frmClinicalSurgeonSetup.surgeonNameList.value ='0';
		 document.frmClinicalSurgeonSetup.surgeonType.value = '0';
	  }
}

function fnSplitName()
{ 
var name= document.frmClinicalSurgeonSetup.surgeonNameList.options[document.frmClinicalSurgeonSetup.surgeonNameList.selectedIndex].text;
var names = new Array();
names = name.split(' ');
document.frmClinicalSurgeonSetup.firstName.value = names[1];
document.frmClinicalSurgeonSetup.lastName.value = names[0];

	if (name == "[Choose One]")
	{
		document.frmClinicalSurgeonSetup.firstName.value = '';
		document.frmClinicalSurgeonSetup.lastName.value = '';
	}
	document.frmClinicalSurgeonSetup.surgeonType.value = '0';
}


function fnSubmit(val)
{
   if(fnValidate())
    {
        document.frmClinicalSurgeonSetup.surgeonNameList.disabled = false;
        if (document.frmClinicalSurgeonSetup.strOpt.value == "edit")
         {
	        document.frmClinicalSurgeonSetup.strOpt.value = "update";
	     }
	     else
	     {   
	 		document.frmClinicalSurgeonSetup.strOpt.value = "save";	
	 	 }
        fnStartProgress();
		document.frmClinicalSurgeonSetup.submit();
	}
}

function fnValidate()
 {
	
	fnValidateDropDn('surgeonNameList', ' Surgeon List ');
	fnValidateTxtFld('firstName',' Surgeon First Name');
	fnValidateTxtFld('lastName',' Surgeon Last Name');
	fnValidateDropDn('surgeonType', ' Surgeon Type ');

	if (ErrorCount > 0)
	 {
		Error_Show();
		Error_Clear();
		return false;
	 }	
	
	 return true;
  
}

function fnReset(res)
{
	document.frmClinicalSurgeonSetup.hsurgeonSetupID.value = "";
	document.frmClinicalSurgeonSetup.msg.value = "";
	document.frmClinicalSurgeonSetup.lastName.value = '';
	document.frmClinicalSurgeonSetup.firstName.value ='';
	document.frmClinicalSurgeonSetup.surgeonNameList.value ='0';
	document.frmClinicalSurgeonSetup.surgeonType.value = '0';
	document.frmClinicalSurgeonSetup.surgeonNameList.disabled = false;	 
	fnReload();
}

function fnReload()
{ 
 document.frmClinicalSurgeonSetup.strOpt.value = "reload";
 document.frmClinicalSurgeonSetup.submit();
}

function fnVoid()
{
		document.frmClinicalSurgeonSetup.action ="/GmCommonCancelServlet";
		document.frmClinicalSurgeonSetup.hTxnId.value = document.frmClinicalSurgeonSetup.hsurgeonSetupID.value;	
		document.frmClinicalSurgeonSetup.hTxnName.value = document.frmClinicalSurgeonSetup.surgeonNameList.options[document.frmClinicalSurgeonSetup.surgeonNameList.selectedIndex].text;
		document.frmClinicalSurgeonSetup.hCancelType.value = 'VDSUR';
		document.frmClinicalSurgeonSetup.hAction.value = 'Load';
		document.frmClinicalSurgeonSetup.submit();
}

function fnEditId(val)
{  
	document.frmClinicalSurgeonSetup.hsurgeonSetupID.value = val;	
	document.frmClinicalSurgeonSetup.strOpt.value = "edit";
	document.frmClinicalSurgeonSetup.submit();
}