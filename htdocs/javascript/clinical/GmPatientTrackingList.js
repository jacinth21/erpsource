
function fnOnPageLoad()
{ 
	  if(objGridData.indexOf("cell", 0)==-1){
		  return true;
	  }
		
	  gridObj = initGrid('dataGridDiv',objGridData);
	  gridObj.enableTooltips("true,true,false,false");
	  gridObj.attachEvent("onRowSelect" ,function(rowId,cellIndex){
		  // getting DHTMLX Grid first column  index
		  var val= gridObj.cellById(rowId,2).getValue();
		  if(val==''){
			  Error_Details("Need <b>Surgeon</b> and <b>Treatment</b> updated in the <b>Patient Setup</b> to navigate further. ");
		  }
		  
		  if (cellIndex==0 && sessAccLvl > 1 && val !=''){
					fnRedirectClinicalRequest(rowId);
				}
		  if (ErrorCount > 0)  
			{
			  	Error_Show();
				Error_Clear();
				return false;	
			}
	  });
		
	gridObj.attachEvent("onMouseOver",function(id,ind)
	{
		if (ind==0 && sessAccLvl > 1){
			gridObj.cells(id,ind).setAttribute("title","Click to load patient time points");
			gridObj.setCellTextStyle(id,ind,"cursor:hand;"); 
			window.status='';
			}
		else
		{
			return false;
		}
		return true;
	});
	gridObj.attachEvent("onMouseOut",function(id,ind)
	{
		if (ind==0)
		{
			window.status='';
		}
		else
		{
			return false;
		}
		return true;
	});
	
	// PMT-39479: Amnios study label changes
	// to rename the label - based on study
	if(vSstudyId == 'GPR009'){
		//
		gridObj.setColumnLabel( gridObj.getColIndexById("SNAME"),"Investigator Name");
		gridObj.setColumnLabel( gridObj.getColIndexById("SDATE"),"Injection Date");
		gridObj.setColumnLabel( gridObj.getColIndexById("NOSRGINV"),"No. of Adverse Events");
	}
	
}
