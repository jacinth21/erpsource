var objGridData;

function fnOnPageLoad()
{
	gridObj = initGrid('dataGridDiv',objGridData);
	gridObj.enableTooltips("true,false");
	gridObj.attachEvent("onRowSelect" ,function(rowId,cellIndex){
    	if (cellIndex==0){
			fnRedirectClinicalRequest(rowId);
		}
	});
	
	gridObj.attachEvent("onMouseOver",function(id,ind)
	{
		if (ind==0 || ind==1)
		{
			gridObj.cells(id,ind).setAttribute("title","Click here to get Sitelist");
			gridObj.setCellTextStyle(id,ind,"cursor:hand;");
			window.status='';			
		}
		else
		{
			return false;
		}
		return true;
	});
	gridObj.attachEvent("onMouseOut",function(id,ind)
	{
		if (ind==0 || ind==1)
		{
			window.status='';	
		}
		else
		{
			return false;
		}
		return true;
	});
}

