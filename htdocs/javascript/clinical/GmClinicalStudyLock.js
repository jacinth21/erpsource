var status_rowId = '';
var studyNm_rowId = '';
// this function used to on page load to load the grid.
function fnOnPageLoad()
{
	gridObj = initGrid('dataGridDiv',objGridData);
	gridObj.enableTooltips("true,false");
	gridObj.setSkin("dhx_skyblue");
	status_rowId = gridObj.getColIndexById("status_id");
	studyNm_rowId = gridObj.getColIndexById("st_nm");
}
// this function used to on study lock time - call the Common Cancle screen.
function fnSubmit(){
	// validate the study status
	fnValidateDropDn('statusId', ' Study Status');
	var status_id = document.frmClinicalStudyLockForm.statusId.value;
	var study_id = '';
	var studyStr = '';
	var study_status_id = '';
	var study_name = '';
	var blFlag = false;
	//var selectId = document.frmClinicalStudyLockForm.raStudyId.value;
	var arr = document.frmClinicalStudyLockForm.raStudyId;
	//document.frmVendor.dhr.value
	for (var i=0;i<arr.length ;i++ )
	{
		if (arr[i].checked == true)
		{				
			blFlag = true;
			study_id =arr[i].value;
			study_status_id = gridObj.cellById(study_id, status_rowId).getValue();
			study_name = gridObj.cellById(study_id, studyNm_rowId).getValue();
			break;
		}
	}
	
	if(!blFlag){
		Error_Details("Please select at least one Study to Lock/Unlock.");
	}
	var status_Nm = document.frmClinicalStudyLockForm.statusId.options[document.frmClinicalStudyLockForm.statusId.selectedIndex].text;

	if((blFlag && study_status_id == '' && status_id =='3352') || study_status_id == status_id){
		Error_Details("Selected Study already <b>"+status_Nm+"</b>.");
	}
	if (ErrorCount > 0) {
		study_id = '';
		Error_Show();
		Error_Clear();
		return false;
	}else {
		// new common Cancel code.
		studyStr = study_id +'^'+ status_id +'|';
		document.frmClinicalStudyLockForm.action = "/GmCommonCancelServlet?hAction=Load";
		document.frmClinicalStudyLockForm.hTxnName.value = study_name;
		document.frmClinicalStudyLockForm.hTxnId.value = studyStr;
		document.frmClinicalStudyLockForm.hCancelType.value = 'LOUNST';
		document.frmClinicalStudyLockForm.hRedirectURL.value = "/gmClinicalStudyLockAction.do?method=loadStudyLockDtls";
		document.frmClinicalStudyLockForm.hDisplayNm.value = "Study Lock Screen";
		fnStartProgress('Y');
		document.frmClinicalStudyLockForm.submit();
	}	
	
}

// this function used to open the study history rpt.
function fnOpenStudyHistory (studid){
	windowOpener('/gmClinicalStudyLockAction.do?method=loadStudyListRpt&strOpt=FetchLog&studyId='+studid,"StudyHistory","resizable=yes,scrollbars=yes,top=250,left=300,width=980,height=500");
}

//This function used to down load the grid excel file
function fnDownloadXLS() {
	gridObj.toExcel('/phpapp/excel/generate.php');	
}
