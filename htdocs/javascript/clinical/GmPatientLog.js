
function initGridData(divRef,gridData){
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);	
	gObj.enableMultiline(true);
	gObj.init();	
	gObj.loadXMLString(gridData);	
	return gObj;	
}
function fnSubmit(val)
{
	//alert('Submit New:->'+document.all.patientId.value);

	fnValidateDropDn('patientId',' Patient ID');
	fnValidateDropDn('studyPeriod','TimePoint');
	fnValidateTxtFld('logDetail','Comments');

	if (ErrorCount > 0)
		{
			Error_Show();
			Error_Clear();
			return false;
		}	
	document.frmPatientLogForm.strOpt.value='Save';
	fnStartProgress('Y');
	document.frmPatientLogForm.submit();
   
}
function fnVoid(){
		//alert('val is:->'+val);
		
		document.frmPatientLogForm.action ="/GmCommonCancelServlet";
		document.frmPatientLogForm.hTxnId.value = document.frmPatientLogForm.patientLId.value;
		document.frmPatientLogForm.hTxnName.value = document.frmPatientLogForm.patientId.value;
		document.frmPatientLogForm.submit();
		
	//alert('Void:'+val);
}
function fnReset(){
	document.frmPatientLogForm.patientId.value=0;
	document.frmPatientLogForm.studyPeriod.value=0;
	document.frmPatientLogForm.studyForm.value=0;
	document.frmPatientLogForm.patientLId.value='';
	document.frmPatientLogForm.logDetail.value='';
	document.frmPatientLogForm.strOpt.value='';
	document.frmPatientLogForm.submit();
}

function fnLoad()
{
	var patientId = eval(document.frmPatientLogForm.patientId);
	var logFromDt = eval(document.frmPatientLogForm.logFrom);
	var logToDt = eval(document.frmPatientLogForm.logTo);

	if(patientId!= undefined && patientId.value==0 && (logFromDt.value=='' && logToDt.value=='')){
		Error_Details("Please select either Patient ID or Date Range to load report.");
	}
	
	DateValidateMMDDYYYY(logFromDt, '<b>From Date - </b>'+ message[1]);
	DateValidateMMDDYYYY(logToDt, '<b>To Date - </b>'+ message[1]);
	diff = dateDiff(logFromDt.value,logToDt.value);
	if(diff < 0)
	{
		Error_Details("<b>To Date</b> cannot be less then <b>From date</b>.");
	}
	if (ErrorCount > 0)
	 {
		Error_Show();
		Error_Clear();
		return false;
	}
	fnStartProgress('Y');
	document.frmPatientLogForm.submit();
}

function fnOnPageLoad()
{
	  if(objGridData.indexOf("cell", 0)==-1){
		  return true;
	  }
		
	  gridObj = initGridData('dataGridDiv',objGridData);
	  gridObj.enableTooltips("true,false,false,false,false,true,true");
	 
	  gridObj.attachEvent("onRowSelect" ,function(rowId,cellIndex){
		  if (cellIndex == 0 )
			  {					
					var actionUrl = "/gmClinicalPatientLog.do?strOpt=edit&patientLId="+rowId;
					document.frmPatientLogForm.action = actionUrl;
					document.frmPatientLogForm.submit();
			   } 
			
	  });
	gridObj.attachEvent("onMouseOver",function(id,ind)
	{
		if (ind==0){
			gridObj.cells(id,ind).setAttribute("title","Click to Edit");
			gridObj.setCellTextStyle(id,ind,"cursor:hand;"); 
			window.status='';			
			}
		return true;
	});
	gridObj.attachEvent("onMouseOut",function(id,ind)
	{
		if (ind==0)
		{
			window.status='';			
		}
		else
		{
			return false;
		}
		return true;
	});
}
function fnDownloadExcel(){
	gridObj.setColWidth(0, "0");
	gridObj.toExcel('/phpapp/excel/generate.php');
	gridObj.setColWidth(0, "50");
}