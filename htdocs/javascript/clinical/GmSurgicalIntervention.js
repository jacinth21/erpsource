
function fnVerify(){
	if(fnValidate()){
	document.frmSurgInvn.hAction.value="VerifySurg";
	fnStartProgress();
	document.frmSurgInvn.submit();	
	}
}
function fnSubmit()
{
	if(fnValidate()){
		var hSurgInvId = document.frmSurgInvn.hSurgInvnId.value;
		var hSurgType = document.frmSurgInvn.hTypeId.value;
		var hSurgDt = document.frmSurgInvn.hSurgDt.value;
		var hOrgLvlId = document.frmSurgInvn.hOrgLvlId.value;
		
		var surgType = document.frmSurgInvn.Cbo_Type.value;
		var surgDt = document.frmSurgInvn.Txt_SurgeryDate.value;
		var orgLvlId = document.frmSurgInvn.Cbo_Orglevel.value;
		
		if(hSurgInvId !="" && hSurgType == surgType && hSurgDt == surgDt && hOrgLvlId == orgLvlId){
			Error_Details("Please modify any field value before you Submit");
		}
		 if (ErrorCount > 0)
			{
					Error_Show();
					Error_Clear();
					return false;
			}
		document.frmSurgInvn.hAction.value="SaveSurg";
		fnStartProgress();
		document.frmSurgInvn.submit();
	}
}
function fnBack()
{
	document.frmSurgInvn.hAction.value="EditLoad";
	document.frmSurgInvn.submit();
}

function fnVoid(){
	hPatientIdeno = document.frmSurgInvn.hPatientID.value;
	document.frmSurgInvn.action ="/GmCommonCancelServlet?hTxnName="+hPatientIdeno;
	surgInvnNo=document.frmSurgInvn.hSurgInvnId.value;
	document.frmSurgInvn.hTxnId.value = surgInvnNo;	
	document.frmSurgInvn.hCancelType.value = 'VDSINV';
	document.frmSurgInvn.hAction.value = 'Load';
	document.frmSurgInvn.submit();
}

function fnValidate()
{
fnValidateDropDn('Cbo_Type','Type');
fnValidateDropDn('Cbo_Orglevel','Original level Intervention');
fnValidateTxtFld('Txt_SurgeryDate','Surgery Date');
CommonDateValidation(document.frmSurgInvn.Txt_SurgeryDate, format, message[611]);
  if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}	
return true;	
}