var dhxLayout;
function doOnLoad() {
	
	//Create Layout
	dhxLayout = new dhtmlXLayoutObject(document.body, "2U");
	dhxLayout.cells("a").setText("Tree");
    dhxLayout.cells("b").setText("DataGrid");
    dhxLayout.cells("a").setWidth("230");
    dhxLayout.cells("a").progressOn();
    //Toolbar to layout
    dhxToolbar = dhxLayout.attachToolbar();    
    dhxToolbar.setIconsPath("/images/");
	dhtmlxAjax.get('/gmClinicalToolbarAction.do?strTxnId='+strTxnId+'&ramdomId='+Math.random(),fnGetToolbarXML);
	
	//Attach accordian to layout
	dhxLayout.cells("a").hideHeader();
	dhxLayout.cells("a").attachURL("/gmClinicalNavigationAction.do");       
	
	//Attach the data grid to layout
	dhxLayout.cells("b").progressOn();
	dhxLayout.cells("b").hideHeader();
	/*
	if(dhtmlx != '')
		actionURL = actionURL + '&strDhtmlx='+dhtmlx+'&method='+strMethod+'&strOpt='+strOpt;
	dhxLayout.cells("b").attachURL(actionURL);  
	
	*/
	//actionURL = '/gmClinicalDataPanelAction.do?strDhtmlx='+dhtmlx+'&method='+strMethod;
	//dhxLayout.cells("b").attachURL(actionURL);  
	dhxLayout.setSizes();
}
function fnGetToolbarXML(loader)
{
	var toolbarXML = TRIM(loader.xmlDoc.responseText);	
	var idIndex;
	dhxToolbar.loadXMLString(toolbarXML,fnToolbarChanges);
    var event_id = dhxToolbar.attachEvent("onClick", function(Id) {
    				/*
    				idIndex = Id.indexOf("GmClinicalLayout.jsp");
					if(loadLeftMenu != "true" || idIndex < 0)
					{
						fnLoadPanelData(Id);						
					}
					else*/
					{
						//alert(Id.indexOf("strAccrd=reports"));

						// check the study id and site id values in Patient Tracking info screen
						// based on the study id and site id values validate Surgeon name field.
							if (Id.indexOf("strMethod=reportPatientTrackInfo&strStudyId=0&strSiteId=0") > 0) {
								Error_Details("Need <b>Surgeon</b> and <b>Treatment</b> updated in the <b>Patient Setup</b> to navigate further. ");
							}
							
							if (ErrorCount > 0) {
								Error_Show();
								Error_Clear();
								return false;
							}
							
						if(Id.indexOf("strAccrd=reports") > 0)
						{
							idIndex = Id.indexOf("?");
							var newActionUrl = Id.substring(idIndex+1);
							//alert('&'+newActionUrl);
							fnSetSesnParams('&'+newActionUrl);
						}
						fnRedirectClinicalRequest(Id);
					}
				});	
   
}
function fnToolbarChanges()
{
}
function fnLoadPanelData(actionUrl)
{
	//alert(actionUrl);
	dhxLayout.cells("b").attachURL(actionUrl);
	dhxLayout.cells("a").progressOff();
	dhxLayout.cells("b").progressOff();
}

function fnSetSesnParams(nodeId)
{
	dhtmlxAjax.get('/gmClinicalAction.do?strNodeId='+escape(nodeId)+'&strTempNodeId='+nodeId+'&ramdomId='+Math.random(),fnGetContextXML);
}

function fnGetContextXML()
{
}

function incJavascript(jsname) {
var th = document.getElementsByTagName('head')[0];
var s = document.createElement('script');
s.setAttribute('type','text/javascript');
s.setAttribute('src',jsname);
th.appendChild(s);
} 
incJavascript("/javascript/clinical/GmClinicalCommonScript.js");