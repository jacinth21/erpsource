// declare the global variable
patient_id_rowId = '';
p_period_rowId = '';
p_type_rowId = '';
p_view_rowId = '';
p_date_x_rowId = '';
p_upload_date_rowId = '';
p_upload_by_rowId = '';
p_preview_rowId = '';
p_images_rowId = '';

function fnAdd_new(){
	document.frmRadioGraphUpload.hAction.value='new_upload';
	document.getElementById("cbo_Patient").disabled = false
	document.getElementById("cbo_Period").disabled = false
	document.getElementById("cbo_Type").disabled = false
	document.getElementById("cbo_View").disabled = false
	document.getElementById("Txt_XRayDt").disabled =false
	fnStartProgress('Y');
	document.frmRadioGraphUpload.submit();
}

function upload(){
	frm = document.frmRadioGraphUpload;
	if(overwrite=="Yes" && frm.hAction.value == "overwrite_upload")
	{
		var upload = confirm("Do you want to Overwrite this file");
			if(upload)
			{
				upload_image();
			}
	}
	else
	{
		//alert("SD....  "+ frm.hAction.value);
		upload_image();
	}
}

function upload_image()
{

	frm = document.frmRadioGraphUpload;
				action = frm.hAction.value;
				
				if (action != 'edit_attributes' &&  action != 'save_edit_attributes'  ){		
						fnValidateTxtFld('uploadfile','File to upload');
				 }
				DateValidateMMDDYYYY(frm.Txt_XRayDt,'Invalid Date format for field: Date of Xray ')
				fnValidateDropDn('cbo_Patient',' Patient ID');
				fnValidateDropDn('cbo_Period',' Period');
				fnValidateDropDn('cbo_Type',' Type');
				fnValidateDropDn('cbo_View',' View');
				if (frm.cbo_Period.value == "6309"){ //n/a
					fnValidateTxtFld('Txt_XRayDt','X Ray Date');
				}
				
				if (ErrorCount > 0)
				{
						Error_Show();
						Error_Clear();
						return false;
				}
					
				if (action == "overwrite_upload"){
					frm.hAction.value='save_overwrite_upload';
				}
				else if (action == "edit_attributes"){
					frm.hAction.value='save_edit_attributes';
				}
				else if (action == "new_upload"){
					frm.hAction.value='save_new_upload';
				}
				document.getElementById("cbo_Patient").disabled = false
				document.getElementById("cbo_Period").disabled = false
				document.getElementById("cbo_Type").disabled = false
				document.getElementById("cbo_View").disabled = false
				frm.hTypeName.value = frm.cbo_Type.options[frm.cbo_Type.options.selectedIndex].text
				frm.hViewName.value = frm.cbo_View.options[frm.cbo_View.options.selectedIndex].text
				frm.hPeriodName.value = frm.cbo_Period.options[frm.cbo_Period.options.selectedIndex].text
				frm.hPatientide.value=frm.cbo_Patient.options[frm.cbo_Patient.options.selectedIndex].text
				fnStartProgress();
				frm.submit();


}


function fnlistView(value){
	objdst = document.getElementById("cbo_View");
	objdst.options.length = 0;	
	
	if (value == 60391){ // xray
		objsrc = document.getElementById("cbo_XRay");

		 for (i = 0; i<objsrc.length; i++ )
     	 {
		 	addOption(objdst, objsrc.options[i].text, objsrc.options[i].value);
		 }		 		
	}
	else if (value == 60392){				
		objsrc = document.getElementById("cbo_MRI");
		for (i = 0; i<objsrc.length; i++ )
     	 {
		 	addOption(objdst, objsrc.options[i].text, objsrc.options[i].value);
		 }	
		 	objdst.value = 60400;
		//objdst.options[0] = new Option("[Choose One]","0");		
	}
	else if (value == 60393){
		objsrc = document.getElementById("cbo_CTScan");			
		for (i = 0; i<objsrc.length; i++ )
     	 {
		 	addOption(objdst, objsrc.options[i].text, objsrc.options[i].value);
		 }	
		 	objdst.value = 60420;
		//objdst.options[0] = new Option("[Choose One]","0");
	}
	else if (value == 0){	
		objdst.options[0] = new Option("[Choose One]","0");		
	}
	
	if (view != 0)
	{
			objdst.value = view;
	}

}	

function addOption(selectbox,text,value )
{
	var optn = document.createElement("OPTION");
	optn.text = text;
	optn.value = value;
	selectbox.options.add(optn);
}

function fnLoad(){	
	document.getElementById("drpdwn").style.visibility = "hidden";	
	fnlistView(document.getElementById("cbo_Type").value);
	if (hAction == "new_upload"){
		document.getElementById("cbo_Patient").disabled = false
		document.getElementById("cbo_Period").disabled = false
		document.getElementById("cbo_Type").disabled = false
	 	document.getElementById("cbo_View").disabled = false
		document.getElementById("Txt_XRayDt").disabled =true
		document.getElementById("Img_Date").disabled = true
	}
	else if (hAction == "overwrite_upload"){
		document.getElementById("cbo_Patient").disabled = true
		document.getElementById("cbo_Period").disabled = true
		document.getElementById("cbo_Type").disabled = true
	 	document.getElementById("cbo_View").disabled = true
	 	document.getElementById("Txt_XRayDt").disabled = true
		document.getElementById("Img_Date").disabled = true
	}
	else if (hAction == "edit_attributes"){
		document.getElementById("cbo_Patient").disabled = true
		document.getElementById("cbo_Period").disabled = false
		document.getElementById("cbo_Type").disabled = true
	 	document.getElementById("cbo_View").disabled = false
	}
	fnShowDate(document.getElementById("cbo_Period").value)
}

function fnShowDate(value){
	if (value == '6309' )//n/a
	{
		document.getElementById("Txt_XRayDt").disabled = false
		document.getElementById("Img_Date").disabled = false
	}
	else
	{
		document.getElementById("Txt_XRayDt").value="";
		document.getElementById("Txt_XRayDt").disabled = true
		document.getElementById("Img_Date").disabled = true
	}
	if (hAction == "overwrite_upload") 
	{
		document.getElementById("Txt_XRayDt").disabled = true
		document.getElementById("Img_Date").disabled = true
	}
	
}

function fnOnPageLoad()
{
	 document.getElementById("drpdwn").style.visibility = "hidden";
 	 fnlistView(type);
 	 
	  if(objGridData.indexOf("cell", 0)==-1){	  	
	  	  document.getElementById("viewSelected").disabled = "true";
		  return true;
	  }  
	  	
	  gridObj = initGrid('dataGridDiv',objGridData);
	  gridObj.enableTooltips("false,false,false,false,false,false,false");
	  gridObj.enableEditEvents(true, false, false);
	 
}
function fnSearch(){

	document.frmUploadRadioGraph.hAction.value =  "search_radiograph";
	document.frmUploadRadioGraph.hPatientId.value =  document.frmUploadRadioGraph.cbo_Patient.value;;
	document.frmUploadRadioGraph.hTimePointId.value =  document.frmUploadRadioGraph.cbo_Period.value;;
	document.frmUploadRadioGraph.hTypeId.value =  document.frmUploadRadioGraph.cbo_Type.value;
	document.frmUploadRadioGraph.hViewId.value =  document.frmUploadRadioGraph.cbo_View.value;
	fnStartProgress('Y');
	document.frmUploadRadioGraph.submit();
}
function fnAddMore(){
	document.frmUploadRadioGraph.hAction.value = "new_upload";
	document.frmUploadRadioGraph.hPatientId.value =  document.frmUploadRadioGraph.cbo_Patient.value;;
	document.frmUploadRadioGraph.hTimePointId.value =  document.frmUploadRadioGraph.cbo_Period.value;;
	document.frmUploadRadioGraph.hTypeId.value =  document.frmUploadRadioGraph.cbo_Type.value;
	document.frmUploadRadioGraph.hViewId.value =  document.frmUploadRadioGraph.cbo_View.value;	
	document.frmUploadRadioGraph.submit();
}
function fnXRayView(patientRecordId){ //Passing only Patient Record ID to the function fnXRayView
    windowOpener("/GmClinicalUploadServlet?hAction=view_upload_Images&hPatientRecordId="+patientRecordId,"PrntInv","resizable=yes,scrollbars=yes,width=650,height=650");
}
function fnVoid(patientRecordId){ //Passing only Patient Record ID to the function fnVoid
	if(lockedStudy !=''){
		Error_Details("Cannot perform this action, the Study is locked.");
	}
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	var fileName = gridObj.getUserData(patientRecordId,"fileName");
	var typeName = gridObj.getUserData(patientRecordId,"typeName");
	var pideno = gridObj.getUserData(patientRecordId,"patIde");
	var tpname = gridObj.getUserData(patientRecordId,"timePointName");
	var viewname = gridObj.getUserData(patientRecordId,"viewName");
	
	 var srcDir = baseDir+typeName+"\\";
	 var destDir = baseDir+typeName+"\\obsolete\\";
	 document.frmUploadRadioGraph.action="/GmCommonCancelServlet";
	 document.frmUploadRadioGraph.hTxnId.value = patientRecordId;
	 document.frmUploadRadioGraph.hCancelType.value = "VDRGU";
	 document.frmUploadRadioGraph.hSrcDir.value = srcDir ;
	 document.frmUploadRadioGraph.hDestDir.value = destDir; 
	 document.frmUploadRadioGraph.hFileName.value = fileName;  
	 document.frmUploadRadioGraph.hAction.value =  "Load";
	 document.frmUploadRadioGraph.hTxnName.value = pideno+" - "+tpname+" - "+typeName+" - "+viewname;
	 document.frmUploadRadioGraph.submit();
}
function fnViewSelected(){
	var chk=gridObj.getCheckedRows(7).split(",");
	var error_msg="At least one image needs to be selected.";
	var fileid="";
	var typename="";
	var viewname="";
	var patid="";
	var patide="";
	var timepointname="";
	var fileName = "";
	for (var i=0;i<chk.length;i++)
	{
		fileName = fileName+gridObj.getUserData(chk[i],"fileName")+",";
		fileid = fileid+gridObj.getUserData(chk[i],"fileId")+",";
		typename = typename+gridObj.getUserData(chk[i],"typeName")+",";
		typename = typename.replace("/", "").toLocaleUpperCase();
		viewname = viewname+gridObj.getUserData(chk[i],"viewName")+",";
		patid = patid+gridObj.getUserData(chk[i],"patId")+",";
		patide = patide+gridObj.getUserData(chk[i],"patIde")+",";
		timepointname = timepointname+gridObj.getUserData(chk[i],"timePointName")+",";			
	}
	if(chk != ""){
		windowOpener("/clinical/GmRadiologyView.jsp?fileName="+fileName+"&fileId="+fileid+"&typeName="+typename+"&viewName="+viewname+"&patientId="+patid+"&patientIde="+patide+"&timepointName="+timepointname+"&hView=multi","PrntInv","resizable=yes,scrollbars=yes,width=550,height=500");
	}else{
			Error_Clear();	
			Error_Details(error_msg);
			Error_Show();
			Error_Clear();
	}		
}

// this function used to over write the file.
// if study is locked then, show the error message.
function fnOverWriteFile (patientRecordId){
	if(lockedStudy !=''){
		Error_Details("Cannot perform this action, the Study is locked.");
	}
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	document.frmUploadRadioGraph.action="/GmClinicalUploadServlet?hAction=overwrite_upload&hPatientRecordId="+patientRecordId;
	document.frmUploadRadioGraph.submit();
}
