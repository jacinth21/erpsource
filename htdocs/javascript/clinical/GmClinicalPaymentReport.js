var objGridData;

function fnLoadReport()
{ 
	document.frmClinicalPaymentReportForm.strOpt.value = "loadReport";
	document.frmClinicalPaymentReportForm.action="gmClinicalPaymentReportAction.do?method=fetchPaymentTrackingReport"
		fnStartProgress('Y');
	document.frmClinicalPaymentReportForm.submit();
}

function fnLoadPaymentDetailsReport(rowId)
{ 
	document.frmClinicalPaymentReportForm.strOpt.value = "loadReport";
	document.frmClinicalPaymentReportForm.paymentId.value = rowId;
	document.frmClinicalPaymentReportForm.status.value = '';
	document.frmClinicalPaymentReportForm.requestType.value = '';
	document.frmClinicalPaymentReportForm.action="gmClinicalPaymentReportAction.do?method=fetchPaymentTrackingDetailsReport"
		fnStartProgress('Y');
	document.frmClinicalPaymentReportForm.submit();
}

function fnEditPayment(paymentId)
{ 
   paymentId = paymentId.replace("#","");
   paymentId = paymentId.replace("#","");

	alert('In Edit Payment : ' + paymentId );
}

function fnVoidPayment(paymentId){

    paymentId = paymentId.replace("#","");
    paymentId = paymentId.replace("#","");

	document.frmClinicalPaymentReportForm.action ="/GmCommonCancelServlet";
	document.frmClinicalPaymentReportForm.hTxnId.value = paymentId;
	document.frmClinicalPaymentReportForm.hTxnName.value = paymentId;
	document.frmClinicalPaymentReportForm.hCancelType.value='VDPRQ';
	document.frmClinicalPaymentReportForm.submit();
}


function fnOnPageLoad()
{
	var val="";
	if(objGridData.indexOf("rows", 0)==-1){
 	///	document.all["dataGridDiv"].innerHTML = "<BR>&nbsp; No Data Found!!!";
		return true;
	}
	
	gridObj = initGrid('dataGridDiv',objGridData);
	gridObj.setColumnHidden(10,true);
	gridObj.enableTooltips("true,true,false,false,false");
	gridObj.enableEditEvents(true, true, false);
	
	// For DHTMLX Column Count sorting
	    gridObj.attachEvent("onBeforeSorting",function(ind,type,direction){
		if(ind==1){
			gridObj.sortRows(10,"date",direction);
			this.setSortImgState(true,1,direction); 
		}
		return true;
	});
	
	///For edit functionality
	gridObj.attachEvent("onRowSelect" ,function(rowId,cellIndex){
	   	if (cellIndex==1){
			fnLoadPaymentDetailsReport(rowId);
		}
		
	});
	
	/////
	gridObj.attachEvent("onMouseOver",function(id,ind)
	{
		if(ind==1){
			gridObj.cells(id,ind).setAttribute("title","Click here to view Payment Details");
			gridObj.setCellTextStyle(id,ind,"cursor:hand;"); 
		}else{
			return false;
		}
		return true;
	});
}