var objGridData;

function fnLoadReport()
{ 
	document.frmClinicalPaymentReportForm.strOpt.value = "loadReport";
	document.frmClinicalPaymentReportForm.action="gmClinicalPaymentReportAction.do?method=fetchPaymentTrackingDetailsReport"
		fnStartProgress('Y');
	document.frmClinicalPaymentReportForm.submit();
}

function fnEditPayment(paymentId)
{ 
   paymentId = paymentId.replace("#","");
   paymentId = paymentId.replace("#","");

	alert('In Edit Payment : ' + paymentId );
}


function fnVoidPaymentDetail(paymentId, paymentDetailsId, studyPeriod){

	paymentId = paymentId.replace("#","");
	paymentId = paymentId.replace("#","");

	paymentDetailsId = paymentDetailsId.replace("#","");
	paymentDetailsId = paymentDetailsId.replace("#","");

	studyPeriod = studyPeriod.replace("#","");
	studyPeriod = studyPeriod.replace("#","");

	document.frmClinicalPaymentReportForm.action ="/GmCommonCancelServlet";
	document.frmClinicalPaymentReportForm.hTxnId.value = paymentDetailsId;
	document.frmClinicalPaymentReportForm.hTxnName.value = paymentId + ' ' + studyPeriod;
	document.frmClinicalPaymentReportForm.hCancelType.value='VDPRQD';
	document.frmClinicalPaymentReportForm.submit();
}


function fnOnPageLoad()
{
	
	var val="";
	if(objGridData.indexOf("rows", 0)==-1){
 	///	document.all["dataGridDiv"].innerHTML = "<BR>&nbsp; No Data Found!!!";
		return true;
	}
	
	gridObj = initGrid('dataGridDiv',objGridData);
	gridObj.enableTooltips("false,false,false,false,false");
	gridObj.enableEditEvents(true, false, false);
	gridObj.setColumnHidden(12,true);
	
	// For DHTMLX Column Count sorting
	gridObj.attachEvent("onBeforeSorting",function(ind,type,direction){
		if(ind==1){
			gridObj.sortRows(12,"date",direction);
			this.setSortImgState(true,1,direction);
		}
		return true;
	});
	
	///For edit functionality
	gridObj.attachEvent("onRowSelect" ,function(rowId,cellIndex){
	   	if (cellIndex==-1){
			var id = gridObj.cellById(rowId, 11).getValue();
			var obsId = gridObj.cellById(rowId, 5).getValue();
	
			fnLoadSiteMonitorObs(id,obsId);
		}
		
	});
	
	/////
	gridObj.attachEvent("onMouseOver",function(id,ind)
	{
		if (ind==-1){
			gridObj.cells(id,ind).setAttribute("title","Click here to Edit");
			gridObj.setCellTextStyle(id,ind,"cursor:hand;"); 
		}
		else
		{
			return false;
		}
		return true;
	});
}