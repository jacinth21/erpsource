function fnLoadPage(){
	var frm=document.frmPatientNameMapping;
	fnValidateTxtFld('patientIde',' Patient IDE No '); 
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	frm.strOpt.value = 'fetch';
	fnStartProgress('Y');
	frm.submit();
	
}

function fnOnPageLoad(){	  
	var frm=document.frmPatientNameMapping;
	var patentID=frm.patientId.value;
	if(patentID.length>0){
		frm.patientIde.disabled = true;
	}
}

function fnReset(){
	var frm=document.frmPatientNameMapping;
	frm.strOpt.value = '';
	frm.patientId.value='';
	frm.submit();
}

function fnSubmit(){
	var frm=document.frmPatientNameMapping;
	
	fnValidate();
	
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	
	frm.patientIde.disabled = false;
	frm.strOpt.value = 'edit';
	fnStartProgress('Y');
	frm.submit();
}

function fnValidate()
{
	
	fnValidateTxtFld('firstName',' First Name '); 
	fnValidateTxtFld('lastName',' Last Name ');
	
}


