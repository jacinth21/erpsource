var objGridData;

function fnOnPageLoad()
{
	if(objGridData.indexOf("cell", 0)==-1){
 		document.all["dataGridDiv"].innerHTML = "<BR>&nbsp; No Data Found!!!";
		return true;
	}
	
	gridObj = initGrid('dataGridDiv',objGridData);
	gridObj.enableTooltips("false,false,false,false,false,true");
	gridObj.enableEditEvents(false, false, false);

	///For edit functionality
	gridObj.attachEvent("onRowSelect" ,function(rowId,cellIndex){
    	if (cellIndex==0){
    //   		fnSiteMapList(rowId);
		}
	});
	
	/////
	gridObj.attachEvent("onMouseOver",function(id,ind)
	{
		if (ind==5){
			gridObj.cells(id,ind).setAttribute("title","Click here to get sitelist");
			gridObj.setCellTextStyle(id,ind,"cursor:hand;"); 
		}
		else
		{
			return false;
		}
		return true;
	});
}