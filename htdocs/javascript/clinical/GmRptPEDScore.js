/**
 * 
 */

document.onkeypress = function() {
	if (event.keyCode == 13) {
		fnReport();
	}

}

function fnSubmit() {
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	document.frmOutcomeReport.strOpt.value = 'save';
	document.frmOutcomeReport.submit();
}

function fnLoad() {
	document.frmOutcomeReport.selectAll.checked = false;
	document.frmOutcomeReport.questionnairre.value = "";
	document.frmOutcomeReport.score.value = "";
	document.frmOutcomeReport.strOpt.value = "";
	document.frmOutcomeReport.action = "/gmRptOutcome.do?method=reportPEDScore";
	document.frmOutcomeReport.submit();
}

function fnLoadQuestionnaire() {
	// document.frmOutcomeReport.selectAll.checked = false;
	document.frmOutcomeReport.strOpt.value = "";
	document.frmOutcomeReport.action = "/gmRptOutcome.do?method=reportPEDScore";
	document.frmOutcomeReport.submit();
}

function fnReport() {
	fnValidateDropDn('questionnairre', ' Endpoint ');
	fnValidateDropDn('score', ' Score ');

	var actionLink = "/gmRptOutcome.do?method=";

	objSelect = eval("document.frmOutcomeReport.questionnairre");
	objSelectVal = objSelect[objSelect.selectedIndex].value;

	objSelectScore = eval("document.frmOutcomeReport.score");
	objSelectScoreVal = objSelectScore[objSelectScore.selectedIndex].value;

	if (objSelectVal == 60801 || objSelectVal == 60901) {
		if (objSelectScoreVal == 60851 || objSelectScoreVal == 60921) {
			document.frmOutcomeReport.action = actionLink + "reportNDIScore";
		}
	} else if (objSelectVal == 60802 || objSelectVal == 60902) {
		if (objSelectScoreVal == 60851 || objSelectScoreVal == 60921) {
			document.frmOutcomeReport.action = actionLink
					+ "reportNDIPerImprove";
		}
	} else if (objSelectVal == 60803 || objSelectVal == 60903) {
		if (objSelectScoreVal == 60851 || objSelectScoreVal == 60921) {
			document.frmOutcomeReport.action = actionLink
					+ "reportNDIImproveDetail";
		}
	} else if (objSelectVal == 60811 || objSelectVal == 61811) {
		document.frmOutcomeReport.action = actionLink + "reportNDIScore";
	// Amnios Study	
	} else if (objSelectVal == 109380 || objSelectVal == 109381) {
		if (objSelectScoreVal == 109383 || objSelectScoreVal == 109382) {
			document.frmOutcomeReport.action = actionLink + "reportNDIScore";
		}
	} else if (objSelectVal == 60812 || objSelectVal == 61812) {
		document.frmOutcomeReport.action = actionLink + "reportZCQSuccess";
	} else {
		document.frmOutcomeReport.action = actionLink + "reportPEDScore";
	}

	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	} else {
		document.frmOutcomeReport.strOpt.value = "Report";
		fnStartProgress();
		document.frmOutcomeReport.submit();
	}
}

function fnCallLoanerDistDetails(varId) {
	document.frmOutcomeReport.action = "/gmRptOutcome.do?method=reportNDIImproveDetail";
	document.frmOutcomeReport.treatmentId.value = varId;
	document.frmOutcomeReport.questionnairre.value = questionnairre_Val;
	document.frmOutcomeReport.score.value = score_Val;
	document.frmOutcomeReport.strOpt.value = "Report";
	document.frmOutcomeReport.submit();
}
