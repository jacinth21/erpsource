var objGridData;
var gridObj;

function fnReload()
{ 
	document.frmStudyFilterForm.action="gmEditCheckReportAction.do?method=fetchEditCheckIncidentReport"
	document.frmStudyFilterForm.submit();
}

function fnLoadReport()
{ 
	document.frmStudyFilterForm.strOpt.value = "loadReport";
	document.frmStudyFilterForm.action="gmEditCheckReportAction.do?method=fetchEditCheckIncidentReport"
		fnStartProgress('Y');
	document.frmStudyFilterForm.submit();
}

function fnVoidIncident(rowId) {
	var form = document.getElementsByName('frmStudyFilterForm')[0];

	form.action ="/GmCommonCancelServlet";
	form.hTxnId.value = rowId;
	form.hTxnName.value = ' Incident ID : ' + rowId; 
	form.hCancelType.value = 'ECISS';
	form.submit();
}


function fnLoadFormData(formId,studyPeriodId,hPatientId,studyPeriodPK) {
	var form = document.getElementsByName('frmStudyFilterForm')[0];
	form.action ="/GmFormDataEntryServlet?Cbo_Form="+formId+"&Cbo_Period="+studyPeriodId+"&hStPerId="+studyPeriodPK+"&hAction=LoadQues&hPatientId="+hPatientId;
	form.submit();
}

function fnQueryDetails(patientAnsListId, queryLevelId)
{
	var patientListId = "";
	windowOpener("/gmQuery.do?method=fetchQueryContainer&queryLevelId="+queryLevelId+"&patientAnsListId="+patientAnsListId+"&patientListId="+patientListId,"","scrollbars=yes,top=150,left=100,width=1000,height=600");
}

function fnOnPageLoad()
{
	if(objGridData.indexOf("rows", 0)==-1){
 		document.all["dataGridDiv"].innerHTML = "<BR>&nbsp; No Data Found!!!";
		return true;
	}
	
	gridObj = new dhtmlXGridObject('dataGridDiv');
	gridObj.setSkin("dhx_skyblue");
	gridObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gridObj.enableMultiline(true);
	gridObj.enableTooltips("true,false,false,false,false,false");
	gridObj.enableEditEvents("true,false,false,false,false,false");
	gridObj.init();
	gridObj.loadXMLString(objGridData);

	///For edit functionality
	gridObj.attachEvent("onRowSelect" ,function(rowId,cellIndex){
    	if (cellIndex==-1){
       		fnVoidIncident(rowId);
		}
	});
	
	/////
	gridObj.attachEvent("onMouseOver",function(id,ind)
	{
		if (ind==-1){
			gridObj.cells(id,ind).setAttribute("title","Click here to Edit");
			gridObj.setCellTextStyle(id,ind,"cursor:hand;"); 
		}
		else
		{
			return false;
		}
		return true;
	});
}	
function fnClose()
{
	window.close();
}