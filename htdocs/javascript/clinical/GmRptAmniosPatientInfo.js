/**
 * File Name: GmRptAmniosPatientInfo.js
 * This JS file used to load the Amnios study - Demographic information
 * 
 */

// this function used to reload the report
function fnReport() {
	document.frmDgraphReport.action = "/gmRptDgraph.do?method=reportDgraph";
	document.frmDgraphReport.strOpt.value = "rptPatientInfo";
	fnStartProgress();
	document.frmDgraphReport.submit();
}

// this function used to load the report
function fnLoad() {
	document.frmDgraphReport.selectAll.checked = false;
	document.frmDgraphReport.strOpt.value = "";
	document.frmDgraphReport.action = "/gmRptDgraph.do?method=reportDgraph";
	document.frmDgraphReport.submit();
}