	var objGridData;

function fnReload()
{ 
	document.frmClinicalSiteMonitorForm.strOpt.value = "search_observation";
	document.frmClinicalSiteMonitorForm.haction.value = "reload";
	document.frmClinicalSiteMonitorForm.submit();
}

function fnLoadReport()
{ 
	var objFrom = document.frmClinicalSiteMonitorForm.fromDate;
	DateValidateMMDDYYYY(objFrom, '<b>From Date - </b>'+ message[1]);
	var objTo = document.frmClinicalSiteMonitorForm.toDate;
	DateValidateMMDDYYYY(objTo, '<b>To Date - </b>'+ message[1]);

	
	if (ErrorCount > 0)
		{
			Error_Show();
			Error_Clear();
			return false;
		}	
	document.frmClinicalSiteMonitorForm.strOpt.value = "search_observation";
	document.frmClinicalSiteMonitorForm.haction.value = "loadReport";
	fnStartProgress('Y');
	document.frmClinicalSiteMonitorForm.submit();
}

function fnOnPageLoad()
{
var val="";
	if(objGridData.indexOf("rows", 0)==-1){
 	//	document.all["dataGridDiv"].innerHTML = "<BR>&nbsp; No Data Found!!!";
		return true;
	}
	
	gridObj = initGrid('dataGridDiv',objGridData);
	gridObj.enableTooltips("true,false,false,false,false");
	gridObj.enableEditEvents(true, false, false);
	
	///For edit functionality
	gridObj.attachEvent("onRowSelect" ,function(rowId,cellIndex){
		if (cellIndex==0){
			var id = gridObj.cellById(rowId, 8).getValue();
			var obsId = gridObj.cellById(rowId, 1).getValue();
			fnLoadSiteMonitorObs(id,obsId);
		}
		
	});
	
	/////
	gridObj.attachEvent("onMouseOver",function(id,ind)
	{
		if (ind==0){
			gridObj.cells(id,ind).setAttribute("title","Click here to Edit");
			gridObj.setCellTextStyle(id,ind,"cursor:hand;"); 
		}
		else
		{
			return false;
		}
		return true;
	});
}

function fnLoadSiteMonitorObs(mtrid,val){
	//alert('val is:'+mtrid);
	//alert('val is:'+val);

	//document.frmClinicalSiteMonitorForm.action="gmClinicalObservation.do?haction=edit_observation&hsiteMonitorId=44&hstudyId=GPR002&hstudySiteId=31&hpurpose=60451&hstatus=60461&hfromDate=04/10/2010&htoDate=04/10/2010&hcraId=303074&observationId=3";
	document.frmClinicalSiteMonitorForm.action="gmClinicalObservation.do?haction=edit_observation&hsiteMonitorId="+mtrid+"&observationId="+val;
	document.frmClinicalSiteMonitorForm.submit();
}