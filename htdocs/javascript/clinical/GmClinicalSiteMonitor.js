
function fnSubmit(valId)
{
	var inputString='';
	var today = new Date();
	var dd = today.getDate();
	if(dd<10){
		dd= '0'+dd;
	}
	var mm = today.getMonth()+1;//January is 0!
	if(mm<10){
		mm='0'+mm;
	}
	var yyyy = today.getFullYear();
	var frmdt = mm+'/'+dd+'/'+yyyy;
	//alert('currTime:'+currTime);
	fnValidateTxtFld('dateVisited',' Date Of Visit');
	fnValidateTxtFld('ownerOfVisit',' Owner Of Visit');
	objDateVisited = document.frmClinicalSiteMonitorForm.dateVisited;
	DateValidateMMDDYYYY(objDateVisited, '<b>Date Of Visit - </b>'+ message[1]);
	var todate=document.frmClinicalSiteMonitorForm.dateVisited.value;
	var ownervisit=document.frmClinicalSiteMonitorForm.ownerOfVisit.value;
	var purposevisit=document.frmClinicalSiteMonitorForm.purposeOfVisit.value;
	var siteMonitorStatus=document.frmClinicalSiteMonitorForm.siteMonitorStatus.value;
	
	if(siteMonitorStatus==0){
		document.frmClinicalSiteMonitorForm.siteMonitorStatus.value=60461;
	}
	

	if(todate!=''){
		var diff = compareDates(frmdt,todate);
		//alert('diff:'+diff);
		if((siteMonitorStatus==0 || siteMonitorStatus==60461)&&diff < 0){
			Error_Details("Date of Visit cannot be a date that has passed");
		}

		var daydiff = dateDiff(frmdt,todate);

		if(siteMonitorStatus==60462){
			if(daydiff!=0){
				Error_Details("Status - In progress can only be the day the visit is being held");
			}
		}
		else if(siteMonitorStatus==60463){
			if(daydiff>0){
				Error_Details("Status - Completed can be today or in the past");
			}
		}
	}
	
	if(ownervisit==0){
		Error_Details("Please select a value for Owner of Visit");
	}
	if(purposevisit==0){
		Error_Details("Please select a value for Purpose of Visit");
	}
	
	objCheckSiteArr = document.all.otherMonitor;
	objCheckSiteArrLen = objCheckSiteArr.length;
	var cnt=0;
	for(i = 1; i < objCheckSiteArrLen; i++) 
	{	
		if(objCheckSiteArr[i].checked == true && (cnt==0)){
			inputString=inputString+objCheckSiteArr[i].value;
			cnt++;
		}
		else if(objCheckSiteArr[i].checked == true){
			inputString=inputString+','+objCheckSiteArr[i].value;
			cnt++;
		}
		
	}
	//alert('val:'+inputString);
	
	if (ErrorCount > 0)
		{
			Error_Show();
			Error_Clear();
			return false;
		}	
	//alert('Inside fnSubmit'+document.frmClinicalSiteMonitorForm.strOpt.value); 
	document.all.monitorString.value=inputString;
	document.frmClinicalSiteMonitorForm.strOpt.value='Save';
	document.frmClinicalSiteMonitorForm.ID.value=valId;
	fnEnable();
	fnStartProgress();
	document.frmClinicalSiteMonitorForm.submit();
   
}
function fnVoid(val){
		//alert(document.frmClinicalSiteMonitorForm.sessSiteId.value);
		
		document.frmClinicalSiteMonitorForm.action ="/GmCommonCancelServlet";
		document.frmClinicalSiteMonitorForm.hTxnId.value = val;
		document.frmClinicalSiteMonitorForm.hTxnName.value = val;
		document.frmClinicalSiteMonitorForm.hCancelType.value='VDSMT';
		document.frmClinicalSiteMonitorForm.submit();
		
	//alert('Void:'+val);
}
function fnLoad(){
	document.frmClinicalSiteMonitorForm.strOpt.value='Load_Monitor';
	document.frmClinicalSiteMonitorForm.purposeOfVisit.value = '';
	document.frmClinicalSiteMonitorForm.dateVisited.value = '';
	document.frmClinicalSiteMonitorForm.notesOfVisit.value = '';
	document.frmClinicalSiteMonitorForm.siteMonitorStatus.value='';
	
}
function compareDates(dt1, dt2)
{
	var dateRegEx = /^([01]?\d)[\.\-\/\s]?([0123]?\d)[\.\-\/\s]?(\d{4})$/;
	var result1 = dt1.match(dateRegEx);
	var result2 = dt2.match(dateRegEx);
	if(result1 != null){
	     var month1 = result1[1];
	     var day1 = result1[2];
	     var year1 = result1[3];
	}
	if(result2 != null){
	     var month2 = result2[1];
	     var day2 = result2[2];
	     var year2 = result2[3];
	}
	if(result1 && result2){
	     var dif = 0;
	     dif += (month2 - month1);
	     dif += ((year2 - year1) * 12);
		 dif += (day2 - day1 < 0) ? -1 : 0;
		 return dif;
	
	}
	return null;
}
function fnCancel(val){
	//alert(document.frmClinicalSiteMonitorForm.sessSiteId.value);
		
		document.frmClinicalSiteMonitorForm.action ="/GmCommonCancelServlet";
		document.frmClinicalSiteMonitorForm.hTxnId.value = val;
		document.frmClinicalSiteMonitorForm.hTxnName.value = val;
		document.frmClinicalSiteMonitorForm.hCancelType.value='CNSMT';
		document.frmClinicalSiteMonitorForm.submit();
}
function fnDisable(){
	//alert(document.frmClinicalSiteMonitorForm.siteMonitorStatus.value);
	var status = document.frmClinicalSiteMonitorForm.siteMonitorStatus.value;
	if(status == 60462 || status == 60463){
		document.frmClinicalSiteMonitorForm.selectAll.disabled = true;
		document.frmClinicalSiteMonitorForm.purposeOfVisit.disabled = true;
		document.frmClinicalSiteMonitorForm.ownerOfVisit.disabled = true;
		document.frmClinicalSiteMonitorForm.dateVisited.disabled = true;
		document.frmClinicalSiteMonitorForm.otherMonitor.disabled = true;
		document.frmClinicalSiteMonitorForm.notesOfVisit.disabled = true;
		
		document.frmClinicalSiteMonitorForm.Img_Date.disabled = true;

		if(status==60463){
			document.frmClinicalSiteMonitorForm.siteMonitorStatus.disabled = true;
		}
		
		return true;
		
	}

	return false;
}

function fnEnable(){
		document.frmClinicalSiteMonitorForm.selectAll.disabled = false;
		document.frmClinicalSiteMonitorForm.purposeOfVisit.disabled = false;
		document.frmClinicalSiteMonitorForm.ownerOfVisit.disabled = false;
		document.frmClinicalSiteMonitorForm.dateVisited.disabled = false;
		document.frmClinicalSiteMonitorForm.otherMonitor.disabled = false;
		document.frmClinicalSiteMonitorForm.notesOfVisit.disabled = false;
		
		document.frmClinicalSiteMonitorForm.Img_Date.disabled = false;
		document.frmClinicalSiteMonitorForm.siteMonitorStatus.disabled = false;
		return true;
}

function fnObservations(id,studyId,siteId){
	//alert('Inside Observartions:'+id);
//	document.frmFieldSalesRptForm.action="/gmFieldSalesRpt.do?method=reportVarianceRptBySet&strOpt=load&reportType="+reportType+"&auditId="+auditId+"&setId="+value;
		var mtrId = id;
		var purpose = document.frmClinicalSiteMonitorForm.purposeOfVisit.value;
		var owner = document.frmClinicalSiteMonitorForm.ownerOfVisit.value;
		var date = document.frmClinicalSiteMonitorForm.dateVisited.value;
		var status = document.frmClinicalSiteMonitorForm.siteMonitorStatus.value;
		//alert('mtrId(:)'+mtrId+' studyId:'+studyId+' siteId:'+siteId+' owner:'+owner+' date:'+date+' status:'+status);

		
	document.frmClinicalSiteMonitorForm.action ="/gmClinicalObservation.do?haction=load_observation&hsiteMonitorId="+mtrId
		+"&hstudyId="+studyId+"&hstudySiteId="+siteId+"&hpurpose="+purpose+"&hstatus="+status+"&hfromDate="+date+"&htoDate="+date+"&hcraId="+owner;
	fnStartProgress();
	document.frmClinicalSiteMonitorForm.submit();
}



