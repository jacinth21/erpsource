var objGridData;

function fnOnPageLoad()
{
	  if(objGridData.indexOf("cell", 0)==-1){
		  return true;
	  }
		
	  gridObj = initGrid('dataGridDiv',objGridData);
	  gridObj.enableTooltips("true,true,false,false");
	 
	  gridObj.attachEvent("onRowSelect" ,function(rowId,cellIndex){
		  if (cellIndex==0 || cellIndex==1){
					fnRedirectClinicalRequest(rowId);
				}
	  });
		
	gridObj.attachEvent("onMouseOver",function(id,ind)
	{
		if (ind==0 || ind==1){
			gridObj.cells(id,ind).setAttribute("title","Click to load patient list");
			gridObj.setCellTextStyle(id,ind,"cursor:hand;"); 
			window.status='';			
			}
		else
		{
			return false;
		}
		return true;
	});
	gridObj.attachEvent("onMouseOut",function(id,ind)
	{
		if (ind==0 || ind==1)
		{
			window.status='';			
		}
		else
		{
			return false;
		}
		return true;
	});
}