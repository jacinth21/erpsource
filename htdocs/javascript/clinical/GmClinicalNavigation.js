var dhxAccord;
function init() {
	var actionUrl = '';
	var treeSnapshot;
	var treeReport;
	var treeTrans;
	dhxAccord = new dhtmlXAccordion("accordObj");
	if (studyId != '')
	{
		dhxAccord.addItem("a1", "Snapshot");
		treeSnapshot = dhxAccord.cells("a1").attachTree();
		treeSnapshot.setSkin("dhx_skyblue");
		treeSnapshot.setImagePath("/extweb/dhtmlx/dhtmlxTree/imgs/csh_dhx_skyblue/");
		treeSnapshot.setStdImages("leaf.gif", "folderOpen.gif", "folderClosed.gif");
		treeSnapshot.enableSmartXMLParsing(true);
		treeSnapshot.loadXMLString(objSnapshotData);
		//treeSnapshot.loadXML("gmClinicalSnapshot.do");
		/*
		var xmlReportFile = "/javascript/clinical/Snapshot.xml";
		treeSnapshot.loadXML(xmlReportFile);
		*/
		treeSnapshot.attachEvent("onClick", function(id) {
				if(id.indexOf("NOACTION") < 0 )
					window.parent.fnLoadPanelData(id);
											
		});
	}

	if(accessLvl != '1')
	{
		dhxAccord.addItem("a2", "Transactions");
		
		treeTrans = dhxAccord.cells("a2").attachTree();
		treeTrans.setSkin("dhx_skyblue");
		treeTrans.setImagePath("/extweb/dhtmlx/dhtmlxTree/imgs/csh_dhx_skyblue/");
		treeTrans.setStdImages("leaf.gif", "folderOpen.gif", "folderClosed.gif");
		treeTrans.loadXMLString(objTransData);
		treeTrans.attachEvent("onClick", function(nodeId, event) {
				//alert(nodeId);
				//window.parent.fnLoadPanelData(nodeId);
				var actionUrl = treeTrans.getUserData(nodeId,"ACTIONURL");
				//alert(actionUrl);
				if(actionUrl != undefined)
				{
					//alert(nodeId);
					window.parent.fnSetSesnParams(nodeId);
					window.parent.fnLoadPanelData(treeTrans.getUserData(nodeId,"ACTIONURL"));
				}
											
		});
	}
	//if (studyId == '' || accessLvl != '1' || patientId != '')
	{
		dhxAccord.addItem("a3", "Reports");	
		treeReport = dhxAccord.cells("a3").attachTree();
		treeReport.setSkin("dhx_skyblue");
		treeReport.setImagePath("/extweb/dhtmlx/dhtmlxTree/imgs/csh_dhx_skyblue/");
		treeReport.setStdImages("leaf.gif", "folderOpen.gif", "folderClosed.gif");
		treeReport.loadXMLString(objReportData);		
		treeReport.attachEvent("onClick", function(nodeId, event) {
				var actionUrl = treeReport.getUserData(nodeId,"ACTIONURL");
				if(actionUrl != undefined)
				{
					if((actionUrl.indexOf("/gmCrtlEndPoint.do")!= -1) || (actionUrl.indexOf("/gmPatientLogSummary.do")!= -1) ){
							fnStartProgress('N');
					}
					window.parent.fnSetSesnParams(nodeId);					
					var temp = treeReport.getUserData(nodeId,"ACTIONURL");
					setTimeout("window.parent.fnLoadPanelData(\'"+temp+"\')",100);					
				}
											
		});
		
	}
	if (studyId != '')
	{
		dhxAccord.openItem("a1");
	}
	if (accrd =='reports')
	{
		dhxAccord.openItem("a3");
		//alert('open node===='+leftNodeId);
		treeReport.openItem(leftNodeId);
	}
	else if(accrd =='trans')
	{
		dhxAccord.openItem("a2");
		treeTrans.openItem(leftNodeId);
	}
	else
	{
		if (studyId != '')
			dhxAccord.openItem("a1");
	}
	
	var dataPanelURL = '/gmClinicalDataPanelAction.do?strDhtmlx='+window.parent.dhtmlx+'&method='+window.parent.strMethod;
	window.parent.fnLoadPanelData(dataPanelURL);
}		
/*
	xmlReportFile = "/javascript/dhtmlx/dhtmlxTree/TempXML/report_"+xmlFile+".xml";
	var tree3 = dhxAccord.cells("a3").attachTree();
	tree3.setSkin('dhx_skyblue');
	tree3.setImagePath("/javascript/dhtmlx/dhtmlxTree/imgs/csh_yellowbooks/");
	tree3.loadXML(xmlReportFile);
	tree3.attachEvent("onClick", function(nodeId, event) {
			window.parent.fnLoadPanelData(nodeId);													
		});
	*/


