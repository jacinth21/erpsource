var objGridData;

function fnReload()
{ 
	document.frmClinicalSiteMonitorForm.strOpt.value = "monitor_report";
	document.frmClinicalSiteMonitorForm.haction.value = "reload";
	fnStartProgress('Y');
	document.frmClinicalSiteMonitorForm.submit();
}

function fnLoadReport()
{ 
	var objFrom = document.frmClinicalSiteMonitorForm.fromDate;
	DateValidateMMDDYYYY(objFrom, '<b>From Date - </b>'+ message[1]);
	var objTo = document.frmClinicalSiteMonitorForm.toDate;
	DateValidateMMDDYYYY(objTo, '<b>To Date - </b>'+ message[1]);

	
	if (ErrorCount > 0)
		{
			Error_Show();
			Error_Clear();
			return false;
		}	
	document.frmClinicalSiteMonitorForm.strOpt.value = "monitor_report";
	document.frmClinicalSiteMonitorForm.haction.value = "loadReport";
	fnStartProgress('Y');
	document.frmClinicalSiteMonitorForm.submit();
}

function fnLoadObservations(status,purpose,rowId)
{ 
            objPurposeOfVisit = document.frmClinicalSiteMonitorForm.purposeOfVisit;
            
            for( var i=0;i<objPurposeOfVisit.length;i++){
                        if(objPurposeOfVisit.options[i].text==purpose){
                                    document.frmClinicalSiteMonitorForm.purposeOfVisit.value = objPurposeOfVisit.options[i].value;      
                        }
            }

            document.frmClinicalSiteMonitorForm.strOpt.value = "search_observation";
            document.frmClinicalSiteMonitorForm.haction.value = "loadReport";
			document.frmClinicalSiteMonitorForm.siteMtrId.value = rowId;
            document.frmClinicalSiteMonitorForm.status.value = status;
            document.frmClinicalSiteMonitorForm.submit();
}
function fnLoadSiteMonitor(id){
	 document.frmClinicalSiteMonitorForm.strOpt.value = "Edit_Monitor";
     document.frmClinicalSiteMonitorForm.haction.value = "loadReport";
     document.frmClinicalSiteMonitorForm.siteMtrId.value = id;
     document.frmClinicalSiteMonitorForm.submit();
}
function fnVoidSiteMonitor(id,date){
		//alert(document.frmClinicalSiteMonitorForm.sessSiteId.value);
		document.frmClinicalSiteMonitorForm.action ="/GmCommonCancelServlet";
		document.frmClinicalSiteMonitorForm.hTxnId.value = id;
		document.frmClinicalSiteMonitorForm.hTxnName.value = id + ' -  Visit Date: '+date;
		document.frmClinicalSiteMonitorForm.hCancelType.value='VDSMT';
		document.frmClinicalSiteMonitorForm.submit();
		
	//alert('Void:'+val);
}
function fnOnPageLoad()
{
	if(objGridData.indexOf("rows", 0)==-1){
 		//document.all["dataGridDiv"].innerHTML = "<BR>&nbsp; No Data Found!!!";
		return true;
	}
	
	gridObj = initGrid('dataGridDiv',objGridData);
	///gridObj.enableTooltips("true,false,false,false,false,false,false,false,true,true");
	gridObj.enableEditEvents(true,false,false,false,false,false,false,false,true,true);

	///For edit functionality
	gridObj.attachEvent("onRowSelect" ,function(rowId,cellIndex){
		var purpose = gridObj.cellById(rowId, 6).getValue();
		if (cellIndex==9){
       		fnLoadObservations('',purpose,rowId);
		}else if(cellIndex==10){
       		fnLoadObservations(60486,purpose,rowId);
		}else if(cellIndex==2){
			fnOpenLog(rowId);
		}else if(cellIndex==1){
			fnLoadSiteMonitor(rowId);
		}
		/*else if(cellIndex==0){
			fnVoidSiteMonitor(rowId);
		}*/
	});
	
	/////
	gridObj.attachEvent("onMouseOver",function(id,ind)
	{
		if (ind==1){
			gridObj.cells(id,ind).setAttribute("title","Click here to Edit");
			gridObj.setCellTextStyle(id,ind,"cursor:hand;"); 
		}else if(ind==9){
			gridObj.cells(id,ind).setAttribute("title","Click here to view Observations");
			gridObj.setCellTextStyle(id,ind,"cursor:hand;"); 
		}else if(ind==10){
			gridObj.cells(id,ind).setAttribute("title","Click here to view Pending Observations");
			gridObj.setCellTextStyle(id,ind,"cursor:hand;"); 
		}else if (ind==0){
			gridObj.cells(id,ind).setAttribute("title","Click here to Void");
			gridObj.setCellTextStyle(id,ind,"cursor:hand;"); 
		}else if (ind==2){
			gridObj.cells(id,ind).setAttribute("title","Click here to Add Comment");
			gridObj.setCellTextStyle(id,ind,"cursor:hand;"); 
		}
		else{
			return false;
		}
		return true;
	});
}

function fnOpenLog(id)
{	
	windowOpener("/GmCommonLogServlet?hType=1257&hID="+id,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
}


