function fnReload()
{ 
 document.frmClinicalTrainingSetup.strOpt.value = "reload";
 document.frmClinicalTrainingSetup.submit();
}

function fnSubmit()
{
var frm = document.frmClinicalTrainingSetup;
var completionDate = frm.completionDate.value;

fnValidateDropDn('trainingForList',' Training For');
fnValidateDropDn('traineeNameList',' Trainee Name');
fnValidateDropDn('trainingReasonList',' Training Reason');
fnValidateTxtFld('completionDate','Completion Date');

if (completionDate!="" )
{
	DateValidateMMDDYYYY(frm.completionDate,'Invalid Date format for field: Completion Date ')
}

if(dateDiff(currentDate,document.frmClinicalTrainingSetup.completionDate.value)>0){
	Error_Details(" Completion Date Cannot be Greater than Current Date.");
}

if (ErrorCount > 0)
 {
	Error_Show();
	Error_Clear();
	return false;
 }	
 else
 {		
   	   frm.strOpt.value = "save"; 
   	   fnStartProgress();
       frm.submit();
 }	
}

function fnLoad()
{
	var opt = document.frmClinicalTrainingSetup.strOpt.value;
	var lockedForm = document.frmClinicalTrainingSetup.lockedStudyDisable.value;

	if (recordid != "" && lockedForm =='')
	{
		document.getElementById('btnVoid').style.visibility="visible";
		document.getElementById('btnReset').style.visibility="visible";
	}
}

function fnVoid()
{
	
	var trainingFor = document.frmClinicalTrainingSetup.trainingForList;
	var traineeNm = document.frmClinicalTrainingSetup.traineeNameList;
	trainingForVal = trainingFor.options[trainingFor.selectedIndex].text;
	traineeNmVal = traineeNm.options[traineeNm.selectedIndex].text;
	document.frmClinicalTrainingSetup.action="/GmCommonCancelServlet";
	document.frmClinicalTrainingSetup.hTxnId.value = recordid;
	document.frmClinicalTrainingSetup.hCancelType.value = "VDTRG";
	document.frmClinicalTrainingSetup.hAction.value =  "Load";
	document.frmClinicalTrainingSetup.hTxnName.value = trainingForVal+' -  '+traineeNmVal;
	document.frmClinicalTrainingSetup.submit();
}

function fnReset()
{
	document.frmClinicalTrainingSetup.trainingForList.value="";
	document.frmClinicalTrainingSetup.traineeNameList.value="";
	document.frmClinicalTrainingSetup.trainingReasonList.value="";
	document.frmClinicalTrainingSetup.trainingScope.value="";
	document.frmClinicalTrainingSetup.completionDate.value="";
	document.frmClinicalTrainingSetup.strTrainingRecordId.value="";
	document.frmClinicalTrainingSetup.action="/gmClinicalTrainingSetupAction.do";
	document.frmClinicalTrainingSetup.submit();
}