var objGridData;

function fnLoad()
{
	document.frmRptSurgeryByDate.selectAll.checked = false;
	document.frmRptSurgeryByDate.strOpt.value = "";
	document.frmRptSurgeryByDate.action = "/gmRptSurgeryByDate.do?method=reportSurgeryByDate";
	document.frmRptSurgeryByDate.submit();
}


function fnGo()
{
	document.frmRptSurgeryByDate.action = "/gmRptSurgeryByDate.do?method=reportSurgeryByDate&strDhtmlx=true";
	document.frmRptSurgeryByDate.strOpt.value = "Report";
	fnStartProgress();
	document.frmRptSurgeryByDate.submit();
}	

function fnOpenLog(id)
{
	windowOpener("/GmCommonLogServlet?hType=1229&hID="+id,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
}

function fnOnPageLoad()
{
	gridObj = initGrid('dataGridDiv',objGridData);
	gridObj.enableTooltips("true,true,true,false,false,false,true");
	///For edit functionality
	gridObj.attachEvent("onRowSelect" ,function(rowId,cellIndex){
    if (cellIndex==4){
    	fnOpenLog(rowId);
	}
	});
	
	/////
	gridObj.attachEvent("onMouseOver",function(id,ind)
	{
 		
		if (ind==4)
		{
			gridObj.cells(id,ind).setAttribute("title","Click here to add comment");
			gridObj.setCellTextStyle(id,ind,"cursor:hand;"); 
		}
		else
		{
			return false;
		}
			return true;
	});

	/*
	gridObj.attachEvent("onRowSelect",function(rowId,cellIndex)
	{
 		if (cellIndex==2){
			//document.frmRptSurgeryByDate.selectAll.checked = false;
			//document.frmRptSurgeryByDate.strOpt.value = "Report";
			var objStudyListId = document.frmRptSurgeryByDate.studyListId;
			document.frmRptSurgeryByDate.action = "gmClinicalDashBoardDispatch.do?method=pendingForms&Cbo_Patient="+rowId+"&studyListId=GPR002&strOpt=Report&checkSiteName=01";
			document.frmRptSurgeryByDate.submit();
		}
	});
	*/
	
	// PMT-39479: Amnios study label changes
	// to rename the label - based on study id
	
	if(studyIdVal == 'GPR009'){
		gridObj.setColumnLabel( 1,"Investigator Name");
		gridObj.setColumnLabel( 3,"Injection Date");
	}
}