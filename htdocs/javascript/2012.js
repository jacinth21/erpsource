var TREE_ITEMS = [
	['Dashboards', null,
		['Shipping', 'GmShippingReportServlet\',\''],
		['Cust Service', 'GmCustDashBoardServlet\',\''],
	],
	['Transactions', null,
		['Part Number Search', 'GmPartSearchServlet\',\''],
		['New Order', 'GmOrderItemServlet\',\'PHONE'],
		['Modify Order', 'GmOrderVoidServlet\',\''],
		['Back Order', 'GmOrderItemServlet\',\'ProcBO'],		
		['Modify Shipping Details', 'gmOPEditShipDetails.do\',\''],
		['Manage Loaners', 'GmInHouseSetServlet\',\'4127'],
		['Manage InHouse Sets', 'GmInHouseSetServlet\',\'4119'],
		['Physical Audit',null,
			['Auto Generate Tags', 'gmIssueTag.do\',\''], 
			['Manual Generate Tags', 'gmGenerateTag.do\',\''],
			['Tag Control', 'gmPATagControl.do\',\''], 
			['Outstanding Tags', 'gmMissingTag.do\',\'MissingTag'], 			
			['Tag Recording', 'gmEnterTag.do\',\''],
			['Lock vs. Tag', 'gmLockTag.do\',\''], 
			['Create MA', 'gmPAManualAdjustment.do\',\''],
			['Inventory Value Report', 'gmPAInventoryRpt.do\',\''],
			['Locked Inventory Quantity', 'GmPartInvReportServlet?Type=20431\',\'LockInv'],
			['Locked inventory Unit Pricing', 'gmLockedCogsReport.do\',\''],
			['Create HTML Tags', 'gmGenerateHTMLTags.do\',\''],
		],
	],
	['Reports', null,
			['All Accounts Report', 'GmAccountReportServlet\',\'ALL'],
		['Cancel Report',null,
			['Rollback Set Consignment Report', 'GmCommonCancelServlet\',\'cancelreport@RBSCN'],
			['Void Item Consignment Report', 'GmCommonCancelServlet\',\'cancelreport@VODCN'],
		],
		['Part Number Transaction', 'GmPartNumSearchServlet\',\''],
		['Consignments Reports',null,
			['Sets', 'GmConsignmentReportServlet\',\'Sets'],
			['Items', 'GmConsignmentReportServlet\',\'Items'],
		],
		['Returns Reports',null,
			['Sets', 'GmReturnsReportServlet\',\'Sets'],
			['Items', 'GmReportCreditsServlet\',\'Items'],
		],
	]
];