function fnSubmit(val)
{
	document.frmProject.hColumn.value = val;
	document.frmProject.submit();
}

// While clicking project id, it will navigate to Project setup screen to edit the details
function fnCallEdit(val)
{
	document.frmProject.hTxnId.value = val;
	document.frmProject.hAction.value = "EditLoad";
	document.frmProject.action = "/gmPDProjectSetup.do?method=loadProjectInformation&strOpt=fetchcontainer&prjId="+val;
	document.frmProject.submit();
	
}

// While clicking on Project name, it will navigate to Part Number report screen
function fnCallPart(val)
{
	document.frmProject.hId.value = val;
	document.frmProject.hAction.value = "Part";
	document.frmProject.submit();
}

// While clicking on load button
function fnGo()
{
	var frm = document.frmProject;
	var group = frm.Cbo_Group.value;
	var segment = frm.Cbo_Segment.value;
	var technique = frm.Cbo_Technique.value;
	var attributeval = '';
	// Combining the dropdown values to fetch the details based on that
	if(group != '0'){
		attributeval += '\',\''+group;
	}
	if(segment != '0'){
		attributeval += '\',\''+segment;
	}
	if(technique != '0'){
		attributeval += '\',\''+technique;
	}
	if(attributeval != ' '){
		attributeval= attributeval.substring(3,attributeval.length);
	}
	document.frmProject.hAttributeVal.value = attributeval; // to get the combination of group,segment and technique drop down values
	document.frmProject.hAction.value = "ReportProject";
	document.frmProject.Btn_Load.disabled=true; // To disable the Load button
	fnStartProgress();
	document.frmProject.submit();
}

//This function is used to activate Load button while pressing the enter key
function fnEnter() {
	if(event.keyCode == 13){
		fnGo();
	}
}

//This function is used to load the Main Inventory report based on the Project
function fnLookup(projId,projName)
{   
	var strProj ='';
	if((projId != undefined && projId !='') && (projName != undefined && projName !='')){
		strProj = projId+" "+ "/" + " " +projName;
	}
	if(strProj == ''){
		Error_Details(message_operations[251]);
	}
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
    //windowOpener('/GmPartInvReportServlet?hAction=Go&hOpt=MultipleParts&Cbo_Search=0&searchCbo_ProjId='+strProj+'&Cbo_ProjId='+projId+'&subComponentFl=on'+'&enableObsolete=on',"Pack","resizable=yes,scrollbars=yes,top=250,left=250,width=960,height=510");
	windowOpener('/GmPartInvReportServlet?hAction=Go&hOpt=MultipleParts&Cbo_Search=0&searchCbo_ProjId='+strProj+'&Cbo_ProjId='+projId,"Pack","resizable=yes,scrollbars=yes,top=250,left=250,width=960,height=510");
}

