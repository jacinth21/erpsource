var chk_ShareDisable_ColId, chk_Share_ColId, hidden_Share_ColId;
// This function is used to load the subtype fields
function fnPopulateSubType(obj) {
	var val = TRIM(obj.value);
	selectedSubType = document.frmArtifacts.subType.value;
	document.frmArtifacts.subType.options.length = 0;
	document.frmArtifacts.subType.options[0] = new Option("[Choose One]", "0");
	var k = 0;
	for ( var i = 0; i < subTypeLen; i++) {
		arr = eval("arrSubType" + i);
		arrobj = arr.split(",");
		id = arrobj[0];
		name = arrobj[1];
		altid = arrobj[2];
		if (altid == val) {
			document.frmArtifacts.subType.options[k + 1] = new Option(name, id);
			k++;
		}
	}
	document.frmArtifacts.subType.value = selectedSubType;
}
// This function used to on load initiate the grid
function fnOnPageLoad() {
	var uploadType = document.frmArtifacts.uploadType.value;
	var obj = document.frmArtifacts.artifactsType;
	if(obj){
		if (obj.value != "") {
			fnPopulateSubType(obj);
		}
	}
	if (objGridData.value != '') {
		gridObj = initGridData('divGrid', objGridData);
		if ((uploadType != 103092)){ // 103092 -System
			gridObj.setColumnHidden(8, true);
			gridObj.setColumnHidden(11, true);
			gridObj.setColumnHidden(12, true);
		}
		if(document.frmArtifacts.keywordAccess.value != 'Y')
			{
			gridObj.setColumnHidden(12, true);
			}
		gridObj.attachEvent("onCheckbox", doOnCheck);
		chk_Share_ColId = gridObj.getColIndexById("share_chk");
		chk_ShareDisable_ColId = gridObj.getColIndexById("Share_disable");
		hidden_Share_ColId = gridObj.getColIndexById("Share_hidden");
		fnSharableCheckBoxDisable();
	}
}
// This function is used to disable checkbox on pageload
function fnSharableCheckBoxDisable(){
	gridObj.forEachRow(function(rowId) {
		var chk_ShareDisableFl = gridObj.cellById(rowId,
				chk_ShareDisable_ColId).getValue();
		
		if (chk_ShareDisableFl == 'Y') {
			gridObj.cellById(rowId, chk_Share_ColId).setDisabled(true);
		}
	});
}
// This function to check the select all (master check box)
function doOnCheck(rowId, cellInd, stage) {
	rowID = rowId;
	if (cellInd == chk_Share_ColId) {
		fnSelectAll('selectAllFiles');
	}
	return true;
}
// This function to check the select all check box
function fnSelectAll(varControl, chk_value, fileNmVal) {
	var objControl = eval("document.frmArtifacts." + varControl);
	var all_row = gridObj.getAllRowIds(",");
	var all_rowLen = all_row.length; // get the rows id length
	var checked_row = gridObj.getCheckedRows(chk_Share_ColId);
	var finalval;
	finalval = eval(checked_row.length);
	if (all_rowLen == finalval) {
		objControl.checked = true;// select all check box to be checked
	} else {
		objControl.checked = false;// select all check box un checked.
	}
}
// This function used to Check all the check boxs for ShareReqFlag.
function fnCheckShareFlag(obj) {
	var shareReqFlVal = '';
	var checkVal = '';
	var checkColumnID = chk_Share_ColId;
	var checkFlagVal = chk_ShareDisable_ColId;
	if (obj.checked) {
		gridObj.forEachRow(function(rowId) {
			shareReqFlVal = gridObj.cellById(rowId, checkFlagVal).getValue();
			if (shareReqFlVal !== 'Y') {
				gridObj.cellById(rowId, checkColumnID).setChecked(true);
			}
		});
	} else {
		gridObj.forEachRow(function(rowId) {
			checkVal = gridObj.cellById(rowId, checkColumnID).getValue();
			if (checkVal == 1) {
				gridObj.cellById(rowId, checkColumnID).setChecked(false);
			}
		});
	}
}
// This function is used to load the uploaded files
function fnReLoad() {
	var uploadtype = document.frmArtifacts.uploadType.value;
	var refid = document.frmArtifacts.refId.value;
	fnValidateDropDn('uploadType', message_prodmgmnt[350]);
	if (uploadtype == '103092' || uploadtype == '103094') { // 103092-System  103094-Group
		fnValidateDropDn('refId', refLabel);
	} else {
		 fnValidateTxtFld('refId',refLabel);
	}
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	document.frmArtifacts.strOpt.value = "Reload";
	fnStartProgress('Y');
	document.frmArtifacts.action = '/gmShareArtifacts.do';
	document.frmArtifacts.submit();
}
// This function is used to populate the respective uploadtypes
function fnLoad() {
	var uploadtype = document.frmArtifacts.uploadType.value;
	if (uploadtype != '0') {
		document.frmArtifacts.refId.value="";
		document.frmArtifacts.strOpt.value = "load";
		document.frmArtifacts.action = '/gmShareArtifacts.do';
		document.frmArtifacts.submit();
	}
}
// fnSubmit() will prepare the inputstring in passed it to action
function fnSubmit(form) {
	var grid_rowid;
	var inputStr = '';
	var gridrows = gridObj.getCheckedRows(0);
	var rowsarr = gridrows.toString().split(",");
	if (rowsarr.length > 0) {
		for ( var rowid = 0; rowid < rowsarr.length; rowid++) {
			grid_rowid = rowsarr[rowid];
			fileid = grid_rowid;
			newShareVal = gridObj.cellById(grid_rowid, chk_Share_ColId).getValue();
			oldShareVal = gridObj.cellById(grid_rowid, hidden_Share_ColId).getValue();
			if (newShareVal != oldShareVal) {
				shareFl = (newShareVal == 1) ? "Y" : "N";
				inputStr += fileid + '^' + shareFl + '^103217|' // 103217 - FileShareableAttributeType
			}
		}
	}
	if(inputStr!=''){
		document.frmArtifacts.strOpt.value = "Save";
		document.frmArtifacts.inputString.value = inputStr;
		document.frmArtifacts.action = '/gmShareArtifacts.do';
		fnStartProgress('Y');
		document.frmArtifacts.submit();
	}
}
//This function will call once click on file name hyper link.
function fnFileView(fileNm,fileid,filetype,fileGroupNm,fileRefid,artifactNm,subTypeNm){
	filetype = filetype.split('^').join(' ');
	fileGroupNm = fileGroupNm.split('^').join(' ');
	if (filetype == 'Image'){ 
		//for zip file ajax will call and unzip file in server 
		if(subTypeNm=='103202'){//360 view
			 fnPreviewZip(fileNm,fileid,fileGroupNm,fileRefid,artifactNm,subTypeNm);	
		}else{			
			windowOpener("/prodmgmnt/productcatalog/GmUploadView.jsp?fileName="+fileNm+"&fileId="+fileid+"&fileGrpNm="+fileGroupNm+"&artifactNm="+artifactNm+"&subTypeNm="+subTypeNm+"&fileRefId="+fileRefid,"PrntInv","resizable=yes,scrollbars=yes,width=560,height=500");
		}
	}
    else{
    	fnDownloadFile(fileid,fileGroupNm,fileRefid,artifactNm,subTypeNm);
    }
}
// fnDownloadFile() to download file
function fnDownloadFile(fileid,fileGroupNm,fileRefid,artifactNm,subTypeNm){
	document.frmArtifacts.action = '/gmPDFileUpload.do?strOpt=Download&fileID='+fileid+'&fileGroupNM='+fileGroupNm+'&artifactNm='+artifactNm+'&subTypeNm='+subTypeNm+'&typeID='+fileRefid;
	document.frmArtifacts.submit();	 
}
//fnPreviewZip to preview zip file
function fnPreviewZip(fileNm,fileid,fileGroupNm,fileRefid,artifactNm,subTypeNm){
	//ajax will call and unzip the file in server and return sample files names with comma seperator. 
	dhtmlxAjax.get('/gmPDFileUpload.do?strOpt=previewzip&fileID=' + fileid
			+ '&fileGroupNM=' + fileGroupNm + '&artifactNm=' + artifactNm
			+ '&subTypeNm=' + subTypeNm + '&typeID=' + fileRefid + '&ramdomId='
			+ Math.random(), function(loader) {
		var response = loader.xmlDoc.responseText;

		if (response == "") {
			Error_Details("There is no image to preview.");
			Error_Show();
			Error_Clear();
			return false;
		} else if (response != null && response.length > 0) {
			windowOpener(
					"/prodmgmnt/productcatalog/GmUploadView.jsp?viewType=previewzip&fileName="
							+ fileNm + "&fileId=" + fileid + "&fileGrpNm="
							+ fileGroupNm + "&artifactNm=" + artifactNm
							+ "&subTypeNm=" + subTypeNm + "&fileRefId="
							+ fileRefid + "&fileLists=" + response, "PrntInv",
					"resizable=yes,scrollbars=yes,width=580,height=560");
		}

	});
}
// This function is used to activate Load button while pressing the enter key
function fnEnter() {
	if (event.keyCode == 13) {
		fnReLoad();
	}
}
// This function used to down load the excel file
function fnDownloadXLS() {
	gridObj.toExcel('/phpapp/excel/generate.php');
}
function fnKeyword(refid,reftype,systemid){
	windowOpener("/gmKeywordDtl.do?strOpt=Reload&refId="+refid+"&type="+reftype+"&systemId="+systemid, "PrntInv",
			"resizable=yes,scrollbars=yes,width=800,height=450");
	
}
function initGridData(divRef,gridData)
{
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.attachHeader(',#text_filter,#text_filter,#select_filter_strict,#select_filter,,,,'
			+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input  type='checkbox'  value='no' name='selectAllFiles' onClick='javascript:fnCheckShareFlag(this);'/>,"+',,');
	gObj.enableTooltips("false,true,false,true,true,true,true,true,false,false,false,true");
	gObj.init();	
	gObj.loadXMLString(gridData);	
	return gObj;	
}