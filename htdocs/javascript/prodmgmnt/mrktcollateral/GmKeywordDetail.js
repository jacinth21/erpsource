
	var gridObj  = '';
	var chk_Share_ColId ='';
	var chk_ShareDisable_ColId ='';
	var orginal_row_cnt;
	function fnOnLoad(){ 
		
		if (objGridData != '') {
			var rowSize = document.frmKeywordDetails.rows.value;
			gridObj = initGridData('KEYWORDDATA',objGridData);
			primary_ColId = gridObj.getColIndexById("primaryid");
			title_keywordName = gridObj.getColIndexById("tagid");
			hidden_keywordName = gridObj.getColIndexById("hiddentagid");
			gridObj.attachEvent("onCheckbox", doOnCheck);
			 chk_Share_ColId = gridObj.getColIndexById("voidfl");
			 chk_ShareDisable_ColId = gridObj.getColIndexById("Share_disable");
			
			var gridrows =gridObj.getAllRowIds(",");
			var gridrowsarr = gridrows.split(",");
			var all_rowLen = (gridrows != "")?gridrowsarr.length:0;
			
			orginal_row_cnt = all_rowLen;
			if(all_rowLen<rowSize){
				for(var i=0; i<rowSize-all_rowLen;i++){
					  addRow();
				}
			}
			gridObj.attachEvent("onEditCell", doOnCellEdit);
			
		}
	}
	function doOnCheck(rowId, cellInd, stage) {
		
		if (cellInd == chk_Share_ColId) {
			fnSelectAll('selectAllFiles');
		}
		return true;
	}
	// This function to check the select all check box
	function fnSelectAll(name) {
		var objControl = eval("document.frmKeywordDetails." + name);
		var checked_row = gridObj.getCheckedRows(chk_Share_ColId);
		var finalval;
		var ary_checked_row =checked_row.split(",");
		finalval = eval(ary_checked_row.length);
	
		if (orginal_row_cnt == finalval) {
			objControl.checked = true;// select all check box to be checked
		} else {
			objControl.checked = false;// select all check box un checked.
		}
	}
	function addRow(){
		gridObj.addRow(gridObj.getUID(),'');
	}
	//This function is used to initiate grid
	function initGridData(divRef,gridData){
		var gObj = new dhtmlXGridObject(divRef);
		gObj.setSkin("dhx_skyblue");
		gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
		gObj.enableDistributedParsing(true);
		gObj.attachHeader(",<input  type='checkbox'  value='no' name='selectAllFiles' onClick='javascript:fnCheckShareFlag(this);'/></span>");
		gObj.enableTooltips("true,true,true,true,true,true,true,true,true");
		gObj.init();
		gObj.loadXMLString(gridData);	
		return gObj;	
	}
	// This function used to Check all the check boxs for ShareReqFlag.
	function fnCheckShareFlag(obj) {
		var shareReqFlVal = '';
		var checkVal = '';
		var checkColumnID = chk_Share_ColId;
		var checkFlagVal = chk_ShareDisable_ColId;
		
		gridObj.forEachRow(function(rowId) {
				tempkeywordid = TRIM(gridObj.cellById(rowId, primary_ColId).getValue());
				if (tempkeywordid !== '') {
					gridObj.cellById(rowId, checkColumnID).setChecked(obj.checked);
				}
			});
	
	}
	function fnSubmit(){
		var inputstr = '';
		gridObj.editStop();
		Error_Clear();
		var gridrows = gridObj.getChangedRows();
		
		var rowsarr = gridrows.toString().split(",");
		if(rowsarr.length > 0)
		{
			for ( var rowid = 0; rowid < rowsarr.length; rowid++)
			{
				grid_dis_rowid = rowsarr[rowid];
				if(grid_dis_rowid != ''){
					tempkeywordid = TRIM(gridObj.cellById(grid_dis_rowid, primary_ColId).getValue());
					tempkeywordname = TRIM(gridObj.cellById(grid_dis_rowid, title_keywordName).getValue());
					hiddenkeywordname = TRIM(gridObj.cellById(grid_dis_rowid, hidden_keywordName).getValue());
					if(tempkeywordid != '' && tempkeywordname ==''){
						Error_Details(message_prodmgmnt[1]);
						Error_Show();
						Error_Clear();
						return false;
					}
					if(tempkeywordname!='' && hiddenkeywordname != tempkeywordname){
						inputstr += tempkeywordid + '^'+ tempkeywordname+ '|';
					}
				}
				
			}
		}
		if(inputstr == ''){
			Error_Details(message_prodmgmnt[25]);
		}
		if (ErrorCount > 0) {
			Error_Show();
			Error_Clear();
			return false;
		}
		
			document.frmKeywordDetails.strOpt.value = "Save";
			document.frmKeywordDetails.inputString.value = inputstr;
			fnStartProgress('Y');
			document.frmKeywordDetails.submit();
	}
	
	function fnVoid(){
		
		var voidinputstr = gridObj.getCheckedRows(1);
		var type = document.frmKeywordDetails.type.value;
		var refId = document.frmKeywordDetails.refId.value;
		var systemId = document.frmKeywordDetails.systemId.value; 
		
		if(voidinputstr == ''){
			Error_Details(message_prodmgmnt[26]);
		}
		if (ErrorCount > 0) {
			Error_Show();
			Error_Clear();
			return false;
		}		
			
		    document.frmKeywordDetails.action = "/GmCommonCancelServlet?hTxnName="+voidinputstr+"&hCancelType=VDKEY&hTxnId="+voidinputstr+"&hAction=Load";
		    document.frmKeywordDetails.hDisplayNm.value = 'Keywords';
		    document.frmKeywordDetails.hRedirectURL.value = "/gmKeywordDtl.do?strOpt=Reload&refId="+refId+"&type="+type+"&systemId="+systemId;
			document.frmKeywordDetails.submit();	
	}
	
	function fnKeywordHistory(value) {
		
		if(value !=''){
			windowOpener(
					"/GmPageControllerServlet?strPgToLoad=/gmAuditTrail.do&auditId=1055&txnId="
							+ value, "",
					"resizable=yes,scrollbars=yes,,top=250,left=300,width=785,height=300,status=1");
		}
	}
	// doOnCellEdit() will call when there is some change into a grid cell
	function doOnCellEdit(stage, rowId, cellInd, nValue, oValue)
	{
		if(oValue!='' && nValue =='' ){
			Error_Details(message_prodmgmnt[27]);
		}else{
			var counter = 0;
			if(stage == '2'){
				if(nValue.length > 50){
					Error_Details(message_prodmgmnt[28]);
				}else{
					gridObj.forEachRow(function(id){
						val=gridObj.cellById(id, title_keywordName).getValue()
						if(nValue != '' && val==nValue && id != rowId)
						{
						    counter++;
						}
					});
					
					if (counter>0){
						Error_Details(message_prodmgmnt[29]);
					}
				}
				
			}
		}
		if (ErrorCount > 0) {
			Error_Show();
			Error_Clear();
			return true;
		}		
		return true;
	}
	function fnClose(){
		window.opener.fnReLoad();
		window.close();
	}