// This Function will load grid data once page will load

var gridObj;
function fnOnLoad(){ 
	if (objGridData.value != '') { 
		gridObj = initGridData('MissingAttributeReport',objGridData);
	}
}
function initGridData(divRef,gridData){
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.attachHeader("#text_filter,#select_filter,#text_filter,#text_filter,#text_filter,#text_filter,#select_filter,#select_filter,#text_filter,#text_filter,#select_filter,#select_filter,#text_filter,#select_filter,#text_filter,#select_filter,#select_filter,#select_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#select_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#select_filter,#select_filter,#select_filter,#text_filter,#select_filter,#text_filter,#text_filter");
	gObj.enableDistributedParsing(true);
	gObj.init();
	gObj.splitAt(4);
	gObj.loadXMLString(gridData);	
	return gObj;	
}

//Export excel
function fnExportExcel() {
	gridObj.toExcel('/phpapp/excel/generate.php');
}

function fnClose(){
	CloseDhtmlxWindow();
	return false;
}