var check_rowId = '';
var diNum_rowId = '';
var error_eventId = '';
var error_statusId = '';
var bSelectAllFl = false;
	function fnOnPageLoad() {

		var search = document.all.hSearch.value;
		if(search != ''){
			document.all.Cbo_Search.value = search;
		} 
		gridObj = initGridData('dataGridDiv', objGridData);
		gridObj.attachEvent("onCheckbox", doOnCheck);
		if(document.frmUdiSubReportDashboard.screenName.value =='DIATTR'){
			gridObj.setColumnHidden(0,true);
			gridObj.setColumnHidden(1,true);
			gridObj.setColumnHidden(2,true);
			gridObj.setColumnHidden(3,true);
		}
		check_rowId = gridObj.getColIndexById("check");
		error_eventId = gridObj.getColIndexById("err_event_id");
		error_statusId = gridObj.getColIndexById("err_status_id");
		diNum_rowId = gridObj.getColIndexById("diNum");
	}
	function fnLoad() {
		
		var frm = document.frmUdiSubReportDashboard;

		// Below function is used to validate the form
		fnValidation(frm);
		
		if (ErrorCount > 0) {
			Error_Show();
			Error_Clear();
			return false;
		}
		
		fnStartProgress('Y');
		document.frmUdiSubReportDashboard.strOpt.value = "Load";
		document.frmUdiSubReportDashboard.submit();
	}
	function initGridData(divRef,gridData)
	{
		var gObj = new dhtmlXGridObject(divRef);
		gObj.setSkin("dhx_skyblue");
		gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
		gObj.enableDistributedParsing(true);
		gObj.enableMultiline(true);
		gObj.init();	
		if(document.frmUdiSubReportDashboard.screenName.value ==''){
		gObj.enablePaging(true, 100, 10, "pagingArea", true, "infoArea");
		gObj.setPagingSkin("bricks");
		}
		gObj.loadXMLString(gridData);
		//if(document.frmUdiSubReportDashboard.screenName.value ==''){
		gObj.attachHeader("<input type='checkbox' value='no' name='selectErrorAll' onClick='javascript:fnChangedFilter(this);'/>,"+'#text_filter,#text_filter,#text_filter,#select_filter,#text_filter,#select_filter,#text_filter,#text_filter,#text_filter,#text_filter');
		//}
		return gObj;	
	}
	function fnCloseError(){
		var close_Error_str = '';
		var error_str = '';
			var chk = gridObj.getCheckedRows(check_rowId).split(",");
		if(chk == ''){
			Error_Details(message_prodmgmnt[100]);
		}else{
			for (var i=0;i<chk.length;i++){
				var statusId = gridObj.cellById(chk[i], error_statusId).getValue();
				if(statusId == '104900'){ // 104900 Open
					close_Error_str = close_Error_str + chk[i]  +'|' ;
				}else{
					var diNumber = gridObj.cellById(chk[i], diNum_rowId).getValue();
					if(error_str.indexOf(diNumber) == -1){
						error_str = error_str + diNumber +', ';
					}
					gridObj.setRowColor(chk[i],"#FF9696");
				}
			}	
		}
		// Closed Error
		if(error_str != ''){
			error_str = error_str.substring(0,(error_str.length)-2);
	    	Error_Details(Error_Details_Trans(message_prodmgmnt[101],error_str));
		}
		if (ErrorCount > 0) {
			error_str = '';
			Error_Show();
			Error_Clear();
			return false;
		}else {
			fnStartProgress('Y');
			document.frmUdiSubReportDashboard.inputStr.value = close_Error_str;
			document.frmUdiSubReportDashboard.strOpt.value = "Save";
			document.frmUdiSubReportDashboard.submit();
	}
	}
	function fnClose(){
		CloseDhtmlxWindow();
		return false;
	}
	
	
	function fnChangedFilter(obj){
		// first time to load full grid (it used to check all the check box)
		if(!bSelectAllFl){
			bSelectAllFl = true;
			gridObj.checkAll(false);
		}
		if(obj.checked)
		{		
			gridObj.forEachRow(function(rowId) {
				check_id = gridObj.cellById(rowId, check_rowId).getValue();			
					gridObj.cellById(rowId, check_rowId).setChecked(true);
				
		   });
		}else
		{
			gridObj.forEachRow(function(rowId) {
				check_id = gridObj.cellById(rowId, check_rowId).getValue();			
				if(check_id ==1){
					gridObj.cellById(rowId, check_rowId).setChecked(false);
				}
				
		   });			
		}
	}
	//Function to export excel
	function fnExport()
	{	
		gridObj.toExcel('/phpapp/excel/generate.php');
	}
	// Function to validate the date fields
	function fnValidation(frm){
		var diNumber = TRIM(frm.diNumber.value);
		var partNum = TRIM(frm.partNum.value);
		var errorFromDtObj = frm.errorFromDt;
		var errorFromDt = TRIM(frm.errorFromDt.value);
		var errorToDtObj = frm.errorToDt;
		var errorToDt = TRIM(frm.errorToDt.value);
		var errorStatus = TRIM(frm.errorStatus.value);
		var errorCategory = TRIM(frm.errorCategory.value);
		
		if(document.all.Cbo_Search){
			var cboSearch = document.all.Cbo_Search.value;
			if(partNum == '' && cboSearch != '0'){
				Error_Details(message_prodmgmnt[102]);
				errorVal = true;
			}else{
				errorVal = false;
			}
		}
		if(diNumber == '' && partNum == '' && errorFromDt == '' && errorToDt == '' && errorStatus == '0' && errorCategory == '0' && errorVal == false){
			Error_Details(message_prodmgmnt[103]);
		}
		if(errorFromDt != ''){
			CommonDateValidation(errorFromDtObj,format,Error_Details_Trans(message_prodmgmnt[107],format));
		}
		if(errorToDt != ''){
			CommonDateValidation(errorToDtObj,format,Error_Details_Trans(message_prodmgmnt[107],format));
		}
	}
	
	//Description : This function to check the select all (master check box)
	function doOnCheck(rowId, cellInd, stage) {
		if(cellInd == check_rowId) {
			fnSelectAllToggle('selectErrorAll');
		}
		return true;
	}
	//Description : This function to check the select all check box
	function fnSelectAllToggle( varControl){
		var objControl = eval("document.frmUdiSubReportDashboard."+varControl);

		var all_row = gridObj.getAllRowIds(",");
		var all_rowLen = all_row.length; // get the rows id length  
		var checked_row = gridObj.getCheckedRows(check_rowId);
		var finalval;

		finalval = eval(checked_row.length);
		if(all_rowLen == finalval){
			objControl.checked = true;// select all check box to be checked
		}else{
			objControl.checked = false;// select all check box un checked.
		}
	}