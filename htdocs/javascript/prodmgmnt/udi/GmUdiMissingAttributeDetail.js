// This Function will load grid data once page will load

var gridObj;
function fnOnLoad(){ 
	if (objGridData.value != '') { 
		gridObj = initGridData('MissingAttribute',objGridData);
	}
}
function initGridData(divRef,gridData){
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);
	gObj.attachHeader("#text_filter,#text_filter,#select_filter,#select_filter,#select_filter");
	gObj.init();	
	gObj.loadXMLString(gridData);	
	return gObj;	
}
//Export excel
function fnExportExcel() {
	gridObj.toExcel('/phpapp/excel/generate.php');
}

function fnClose(){
	CloseDhtmlxWindow();
	return false;
}