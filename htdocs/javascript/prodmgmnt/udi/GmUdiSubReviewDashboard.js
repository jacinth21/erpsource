
var gridObj;
var check_rowId = '';
var externalStatus_rowId = '';
var extStatusName_rowId = '';
var extStatusFlag_rowId = '';
//This function is used to initiate grid on page load
function fnOnPageLoad(){
	var search = document.all.hSearch.value;
	if(search != ''){
		document.all.Cbo_Search.value = search;
	}
	if(objGridData.value != ''){ 
		gridObj = initGridData('dataGridDiv',objGridData);
		check_rowId = gridObj.getColIndexById("check");
		externalStatus_rowId = gridObj.getColIndexById("external_status_id");
		extStatusFlag_rowId = gridObj.getColIndexById("ext_status_flag");
		extStatusName_rowId = gridObj.getColIndexById("ext_status_nm");
		// disable the check box based on the condition.
		// External status id empty and 104834	Publish to GUDID then only enable otherwise to disable the check box.
		
		gridObj.forEachRow(function(rowId) {
			externalStatusId = gridObj.cellById(rowId, externalStatus_rowId).getValue();
			if (externalStatusId !='' && externalStatusId !='104834') {
				gridObj.cellById(rowId, check_rowId).setDisabled(true);
			}
		}); 
	}
}
//This function used to initiate Grid
function initGridData(divRef,gridData){
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.attachHeader("<input type='checkbox' value='no' name='selectAll' onClick='javascript:fnChangedFilter(this);'/>,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#select_filter,#select_filter,#select_filter,#select_filter," +
			"#select_filter,#numeric_filter,#select_filter,#text_filter,#select_filter,#numeric_filter,#select_filter,#text_filter,#select_filter," +
			"#numeric_filter,#select_filter,#text_filter,#select_filter,#numeric_filter,#select_filter,#text_filter,#select_filter,#select_filter," +
			"#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#text_filter,#text_filter,#text_filter," +
			"#text_filter,#select_filter,#select_filter,#select_filter,#select_filter,#text_filter,#text_filter,#text_filter,#text_filter,#select_filter," +
			"#select_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter," +
			"#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter," +
			"#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter");
	gObj.enableMultiline(true);
	gObj.init();
	gObj.enablePaging(true, 100, 10, "pagingArea", true, "infoArea");
	gObj.setPagingSkin("bricks");
	gObj.loadXMLString(gridData);
	gObj.enableHeaderMenu();
	return gObj;	
}

//When click on Load button, below function perform
function fnLoad(){
	
	 fnCreateInputStr(); // PC-1006 UDI Review Dashboard - Improvement work the diNumber and partNum field – with comma separated values.
	 document.frmUdiSubDashboard.strOpt.value = 'Load';
	 fnStartProgress();
	 document.frmUdiSubDashboard.submit();	
}

function fnTransferToGDSN(){
	
	var checkIdVal = "";
	var inputStr = "";
	var diNumber = "";
	var di_rowId = gridObj.getColIndexById("dinumber");
	var check_rowId = gridObj.getColIndexById("check");
	gridObj.forEachRowA(function(rowId) {
		checkIdVal = gridObj.cellById(rowId, check_rowId).getValue();
		if(checkIdVal == "1"){
			diNumber = gridObj.cellById(rowId, di_rowId).getValue();
			
			inputStr = inputStr+diNumber+'|';
		}
	
	});
	if(inputStr == ''){
		
		Error_Details(message_prodmgmnt[104]);
		
		if (ErrorCount > 0)
		{
				Error_Show();
				Error_Clear();
				return false;
		}
	}
	confirmMsg = fnConfirmMessage();
	if(confirmMsg)	{
	document.frmUdiSubDashboard.strOpt.value = 'Save';
	document.frmUdiSubDashboard.inputStr.value = inputStr;
	fnStartProgress();
	document.frmUdiSubDashboard.submit();	
	}
}
//this function used to select all option (check box)
function fnChangedFilter(obj) {
	var check_rowId = gridObj.getColIndexById("check");
	if (obj.checked) {
		gridObj.forEachRow(function(rowId) {
			var check_disable_fl = gridObj.cellById(rowId, check_rowId).isDisabled();
			if (!check_disable_fl) {
				gridObj.cellById(rowId, check_rowId).setChecked(true);
			}
		});
	} else {
		gridObj.forEachRow(function(rowId) {
			check_id = gridObj.cellById(rowId, check_rowId).getValue();

			if (check_id == 1) {
				gridObj.cellById(rowId, check_rowId).setChecked(false);
			}

		});
	}
}

//Function to export excel
function fnExport()
{
	gridObj.setColumnHidden(check_rowId, true);
	//external status
	gridObj.setColumnHidden(extStatusFlag_rowId, false);
	gridObj.setColumnHidden(extStatusName_rowId, false);
	// part attribute
	/*gridObj.setColumnHidden(26 , false);
	gridObj.setColumnHidden(27 , false);
	gridObj.setColumnHidden(28 , false);
	gridObj.setColumnHidden(29 , false);
	gridObj.setColumnHidden(30 , false);
	gridObj.setColumnHidden(31 , false);
	gridObj.setColumnHidden(32 , false);
	gridObj.setColumnHidden(33 , false);
	gridObj.setColumnHidden(34 , false);
	gridObj.setColumnHidden(35 , false);
	gridObj.setColumnHidden(36 , false);
	gridObj.setColumnHidden(37 , false);
	gridObj.setColumnHidden(38 , false);
	gridObj.setColumnHidden(39 , false);
	gridObj.setColumnHidden(40 , false);
	gridObj.setColumnHidden(41 , false);
	gridObj.setColumnHidden(42 , false);
	gridObj.setColumnHidden(43 , false);
	gridObj.setColumnHidden(44 , false);
	gridObj.setColumnHidden(45 , false);
	gridObj.setColumnHidden(46 , false);
	gridObj.setColumnHidden(47 , false);
	gridObj.setColumnHidden(48 , false);
	gridObj.setColumnHidden(49 , false);
	gridObj.setColumnHidden(50 , false);
	gridObj.setColumnHidden(51 , false);
	gridObj.setColumnHidden(52 , false);
	gridObj.setColumnHidden(53 , false);
	gridObj.setColumnHidden(54 , false);
	gridObj.setColumnHidden(55 , false);
	gridObj.setColumnHidden(56 , false);
	gridObj.setColumnHidden(57 , false);
	gridObj.setColumnHidden(58 , false);
	gridObj.setColumnHidden(59 , false);
	gridObj.setColumnHidden(60 , false);
	gridObj.setColumnHidden(61 , false);
	gridObj.setColumnHidden(62 , false);
	gridObj.setColumnHidden(63 , false);
	gridObj.setColumnHidden(64 , false);
	gridObj.setColumnHidden(65 , false);
	gridObj.setColumnHidden(66 , false);
	gridObj.setColumnHidden(67 , false);
	gridObj.setColumnHidden(68 , false);
	gridObj.setColumnHidden(69 , false);
	gridObj.setColumnHidden(70 , false);
	gridObj.setColumnHidden(71 , false);
	gridObj.setColumnHidden(72 , false);
	gridObj.setColumnHidden(73 , false);
	gridObj.setColumnHidden(74 , false);
	gridObj.setColumnHidden(75 , false);*/
	gridObj.toExcel('/phpapp/excel/generate.php');
	gridObj.setColumnHidden(check_rowId, false);
	//External status
	gridObj.setColumnHidden(extStatusFlag_rowId, true);
	gridObj.setColumnHidden(extStatusName_rowId, true);
	//part attribute
	/*gridObj.setColumnHidden(26 , true);
	gridObj.setColumnHidden(27 , true);
	gridObj.setColumnHidden(28 , true);
	gridObj.setColumnHidden(29 , true);
	gridObj.setColumnHidden(30 , true);
	gridObj.setColumnHidden(31 , true);
	gridObj.setColumnHidden(32 , true);
	gridObj.setColumnHidden(33 , true);
	gridObj.setColumnHidden(34 , true);
	gridObj.setColumnHidden(35 , true);
	gridObj.setColumnHidden(36 , true);
	gridObj.setColumnHidden(37 , true);
	gridObj.setColumnHidden(38 , true);
	gridObj.setColumnHidden(39 , true);
	gridObj.setColumnHidden(40 , true);
	gridObj.setColumnHidden(41 , true);
	gridObj.setColumnHidden(42 , true);
	gridObj.setColumnHidden(43 , true);
	gridObj.setColumnHidden(44 , true);
	gridObj.setColumnHidden(45 , true);
	gridObj.setColumnHidden(46 , true);
	gridObj.setColumnHidden(47 , true);
	gridObj.setColumnHidden(48 , true);
	gridObj.setColumnHidden(49 , true);
	gridObj.setColumnHidden(50 , true);
	gridObj.setColumnHidden(51 , true);
	gridObj.setColumnHidden(52 , true);
	gridObj.setColumnHidden(53 , true);
	gridObj.setColumnHidden(54 , true);
	gridObj.setColumnHidden(55 , true);
	gridObj.setColumnHidden(56 , true);
	gridObj.setColumnHidden(57 , true);
	gridObj.setColumnHidden(58 , true);
	gridObj.setColumnHidden(59 , true);
	gridObj.setColumnHidden(60 , true);
	gridObj.setColumnHidden(61 , true);
	gridObj.setColumnHidden(62 , true);
	gridObj.setColumnHidden(63 , true);
	gridObj.setColumnHidden(64 , true);
	gridObj.setColumnHidden(65 , true);
	gridObj.setColumnHidden(66 , true);
	gridObj.setColumnHidden(67 , true);
	gridObj.setColumnHidden(68 , true);
	gridObj.setColumnHidden(69 , true);
	gridObj.setColumnHidden(70 , true);
	gridObj.setColumnHidden(71 , true);
	gridObj.setColumnHidden(72 , true);
	gridObj.setColumnHidden(73 , true);
	gridObj.setColumnHidden(74 , true);
	gridObj.setColumnHidden(75 , true);*/
}
function fnConfirmMessage(){
	confirmMsg = confirm(message_prodmgmnt[108]);
	if(confirmMsg)	
	return true;
	else 			
	return false;
}
// PC-1006 UDI Review Dashboard - Improvement work the diNumber and partNum field – with comma separated values.
function fnCreateInputStr(){ 
	var diNumberText = "";
	var partNumberText = "";
	
	diNumberText =  TRIM(document.frmUdiSubDashboard.diNumberText.value);
	partNumberText =  TRIM(document.frmUdiSubDashboard.partNumberText.value);
	
	diNumberText = diNumberText.replace(/["',]/g, "\n");
	partNumberText = partNumberText.replace(/["']/g, "");
		
	diNumberText = diNumberText.replace(/\n/g, "','");
	partNumberText = partNumberText.replace(/\n/g, ",");
	
	document.frmUdiSubDashboard.diNumber.value = diNumberText;
	document.frmUdiSubDashboard.partNum.value = partNumberText;
}