// This Function will load grid data once page will load

var gridObj;
function fnOnLoad() {
	var search = document.all.hSearch.value;
	if (search != '') {
		document.all.Cbo_Search.value = search;
	}
	if (objGridData.value != '') {
		gridObj = initGridData('udiWipDashboard', objGridData);
		gridObj.attachEvent("onFilterStart",fnCustomFilter);
	}
}
function initGridData(divRef, gridData) {
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/extweb/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);
	gObj.enableMultiline(true);
	gObj.enablePaging(true, 100, 10, "pagingArea", true, "infoArea");
	gObj.setPagingSkin("bricks");
	gObj.init();
	gObj.loadXMLString(gridData);
	gObj.attachHeader("<input type='checkbox' value='no' name='selectAll' onClick='javascript:fnChangedFilter(this);'/>,#text_filter,,#select_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#select_filter");
	return gObj;
}

function fnLoad() {
	fnCreateInputString(); // PC-2158 UDI WIP Dashboard - Improvement work the diNumber and partNum field – with comma separated values.
	fnStartProgress('Y');
	document.frmUdiSubDashboard.strOpt.value = "load";
	document.frmUdiSubDashboard.submit();
}

function fnExportExcel() {
	gridObj.toExcel('/phpapp/excel/generate.php');
}

function fnOpenMissingAttributeScreen(partnum) {
	dhxWins = new dhtmlXWindows();
	dhxWins.enableAutoViewport(false);
	dhxWins.attachViewportTo(document.body);
	dhxWins.setImagePath("extweb/dhtmlx/dhtmlxWindows/imgs");

	popupWindow = dhxWins.createWindow("w1", 50, 10, 800, 660);
	popupWindow.attachURL(
					'/gmUdiReport.do?method=udiMissingAttributeDetail&ramdomId='
							+ Math.random()
							+ '&strOpt=MissingAttribute&partNum=' + partnum,
					"MissingAttributeDetails",
					"resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
	popupWindow.setText("WIP Missing Attribute Detail");
}
function fnOpenMissingErrorReport(dinumber) {
	dhxWins = new dhtmlXWindows();
	dhxWins.enableAutoViewport(false);
	dhxWins.attachViewportTo(document.body);
	dhxWins.setImagePath("extweb/dhtmlx/dhtmlxWindows/imgs");

	popupWindow = dhxWins.createWindow("w1", 50, 50, 1200, 500);
	popupWindow.attachURL('/gmUdiReport.do?method=udiErrorReport&ramdomId='
					+ Math.random() + '&strOpt=Load&diNumber=' + dinumber
					+ '&errorStatus=0&errorCategory=104922',
					"ErrorReport",
					"resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
	popupWindow.setText("UDI Error Report");
}
function fnOpenDINumAttrDtls(dinumber) {

	 dhxWins = new dhtmlXWindows();
	 dhxWins.enableAutoViewport(false);
	 dhxWins.attachViewportTo(document.body);
	 dhxWins.setImagePath("extweb/dhtmlx/dhtmlxWindows/imgs");
	 
	 popupWindow = dhxWins.createWindow("w1", 50, 50, 1200, 570);
	 popupWindow.attachURL('/gmUdiReport.do?method=udiDiNumberAttributeDtls&ramdomId='+Math.random()+'&strOpt=Load&diNumber='+dinumber+'',"UDI Attribute Details","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
	 popupWindow.setText("UDI Attribute Details");
	 
}
// this function used to select all option (check box)
function fnChangedFilter(obj) {
	var check_rowId = gridObj.getColIndexById("check");
	if (obj.checked) {
		gridObj.forEachRow(function(rowId) {
			var check_disable_fl = gridObj.cellById(rowId, check_rowId)
					.isDisabled();
			if (!check_disable_fl) {
				gridObj.cellById(rowId, check_rowId).setChecked(true);
			}
		});
	} else {
		gridObj.forEachRow(function(rowId) {
			check_id = gridObj.cellById(rowId, check_rowId).getValue();

			if (check_id == 1) {
				gridObj.cellById(rowId, check_rowId).setChecked(false);
			}

		});
	}
}

function fnMissingAttribute() {

	
	var check_rowId = gridObj.getColIndexById("check");
	var Missing_Attr_str = '';
			var chk = gridObj.getCheckedRows(check_rowId).split(",");
		if(chk == ''){
			Error_Details(message_prodmgmnt[105]);
		}else{
			for (var i=0;i<chk.length;i++){
				Missing_Attr_str = Missing_Attr_str +"'"+ gridObj.cellById(chk[i], 2).getValue()+ "'" +"," ;
			}	
		}
		
		if (ErrorCount > 0) {
			Error_Show();
			Error_Clear();
			return false;
		}else {
				
				if(Missing_Attr_str != ""){
					Missing_Attr_str = Missing_Attr_str.substring(0,(Missing_Attr_str.length-1));
				}
				
				dhxWins = new dhtmlXWindows();
				dhxWins.enableAutoViewport(false);
				dhxWins.attachViewportTo(document.body);
				dhxWins.setImagePath("extweb/dhtmlx/dhtmlxWindows/imgs");

				popupWindow = dhxWins.createWindow("w1", 50, 50, 1200, 560);
				popupWindow.attachURL('/gmUdiReport.do?method=udiMissingErrorReport&ramdomId='+ Math.random() + '&inputStr=' + Missing_Attr_str , "ErrorReport","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
				popupWindow.setText("Missing Attribute Report");
		}

}
// Customer Filter added.
function fnCustomFilter(ord,data){
	/* This custom filter to avoid hyper link content column filter issue. 
	 * Because default filter consider all text with in the tag. 
	 * Ex. Let us assume xml as <cell><a ..>text</a></cell>.  If user try to file 'a' text then all rows will show. */
		
		for(i=0;i<data.length;i++){
			if(data[i]!="" && data[i] != undefined && (""+data[i]).indexOf("anonymous")==-1){
				var original=data[i];
				original = original.toLowerCase();
				data[i]=function(value){
							if (value.toString().replace(/<[^>]*>/g,"").toLowerCase().indexOf(original)!=-1)
								return true;
							return false;
							};
			}
				
		}
		gridObj.filterBy(ord,data);
		return false;
	}
// PC-2158 UDI WIP Dashboard - Improvement work the diNumber and partNum field – with comma separated values.
function fnCreateInputString(){
		var diNumberText = "";
		var partNumberText = "";
	
		diNumberText =  TRIM(document.frmUdiSubDashboard.diNumberText.value);
		partNumberText =  TRIM(document.frmUdiSubDashboard.partNumberText.value);
	
		diNumberText = diNumberText.replace(/["',]/g, "\n");
		partNumberText = partNumberText.replace(/["']/g, "");
		
		diNumberText = diNumberText.replace(/\n/g, "','");
		partNumberText = partNumberText.replace(/\n/g, ",");
			
		document.frmUdiSubDashboard.diNumber.value = diNumberText;
		document.frmUdiSubDashboard.partNum.value = partNumberText;
}