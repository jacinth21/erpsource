// This Function will load grid data once page will load

var gridObj;
function fnOnLoad(){ 
	
	var search = document.all.hSearch.value;
		if(search != ''){
			document.all.Cbo_Search.value = search;
		} 
	if (objGridData.value != '') { 
		gridObj = initGridData('distatus',objGridData);
	}
}
function initGridData(divRef,gridData){
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);
	gObj.enableMultiline(true);
	gObj.enablePaging(true, 100, 10, "pagingArea", true, "infoArea");
	gObj.setPagingSkin("bricks");
	gObj.init();	
	gObj.loadXMLString(gridData);	
	gObj.attachHeader("#text_filter,#select_filter,#text_filter,#text_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter");
	return gObj;	
}

function fnLoad(){

	var frm = document.frmUdiSubReportDashboard;
	// Below function is used to validate the form
	fnValidation(frm);
	
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	
	fnCreateInputString(); //PC-2158 UDI SubDiStatus - Improvement work within multiple Di Numbers and Part Numbers
	fnStartProgress('Y');
	document.frmUdiSubReportDashboard.strOpt.value = "load";  
	document.frmUdiSubReportDashboard.submit();
}

function fnExportExcel()
{	
	gridObj.toExcel('/phpapp/excel/generate.php');
}

//Function to validate the date fields
function fnValidation(frm){
	var diNumber = TRIM(frm.diNumberText.value);
	var partNum = TRIM(frm.partNumberText.value);
	var projectId = TRIM(frm.projectId.value);
	var internalSts = TRIM(frm.internalSts.value);
	var externalSts = TRIM(frm.externalSts.value);
	
	if(document.all.Cbo_Search){
		var cboSearch = document.all.Cbo_Search.value;
		if(partNum == '' && cboSearch != '0'){
			Error_Details(message_prodmgmnt[14]);
			errorVal = true;
		}else{
			errorVal = false;
		}
	}
	if(diNumber == '' && partNum == '' && projectId == '0' && internalSts == '0' && externalSts == '0' && errorVal == false){
		Error_Details(message_prodmgmnt[31]);
	}
}
//PC-2158 UDI SubDiStatus - Improvement work within multiple Di Numbers and Part Numbers
function fnCreateInputString(){
	var diNumberText = "";
	var partNumberText = "";
	
	diNumberText =  TRIM(document.frmUdiSubReportDashboard.diNumberText.value);
	partNumberText =  TRIM(document.frmUdiSubReportDashboard.partNumberText.value);
	
	diNumberText = diNumberText.replace(/["',]/g, "\n");
	partNumberText = partNumberText.replace(/["']/g, "");
		
	diNumberText = diNumberText.replace(/\n/g, "','");
	partNumberText = partNumberText.replace(/\n/g, ",");

	document.frmUdiSubReportDashboard.diNumber.value = diNumberText;
	document.frmUdiSubReportDashboard.partNum.value = partNumberText;
}
