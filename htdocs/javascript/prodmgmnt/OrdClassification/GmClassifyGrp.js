
function fnShow(object, sysid)
{
	var obj = document.getElementById(object);
	if (obj.style.display == 'none')
	{
		obj.style.display = 'block';
	}
	else
	{
		obj.style.display = 'none';
		for (var k=0; k < size ; k++) 
		{ 	 
			objSelected = eval("document.frmClassifyGrp.check"+k);   
			objSystem = eval("document.frmClassifyGrp.System"+k);		 
			
			if(objSystem.value == sysid )
			{    
				objSelected.checked = false; 
			} 
		}  
	}

}

function fnUnCheckSelections(objVal)
{	 
	
	 var checkflag = 0; 
		for (var k=0; k < size ; k++) 
		{ 
			objGrpId = eval("document.frmClassifyGrp.GroupId"+k); 			
			objSelected = eval("document.frmClassifyGrp.check"+k);   
			objSystem = eval("document.frmClassifyGrp.System"+k);
			checkSys =objSystem.value.replace(".","");  
			if(objSystem.value == objVal  )
			{	  
				if (objSelected.checked)
				{
					checkflag = 1;
				} 
				objSysCheck = eval("document.frmClassifyGrp.sysCheck"+checkSys);
			} 
		 }
		
			if(checkflag == 0 )
			{	   
				objSysCheck.checked = false;  
				fnShow('div'+objVal, objVal); 
			}  
	
}

function fnCheckSelections()
{ 
	 var tempSys = '';
	 var sysCheck = 0;
		for (var k=0; k < size ; k++) 
		{ 
			objGrpId = eval("document.frmClassifyGrp.GroupId"+k); 			
			objSelected = eval("document.frmClassifyGrp.check"+k);   
			objSystem = eval("document.frmClassifyGrp.System"+k);
			checkSys =objSystem.value.replace(".",""); 
			 
			objSysCheck = eval("document.frmClassifyGrp.sysCheck"+checkSys);
			
			for (var i=0;i<setLen;i++)
			{
				groupId  = eval("GroupArr"+i);
				if(objGrpId.value ==groupId)
				{	 
					objSelected.checked = true; 
					if(objSystem.value !=tempSys  )
					{	 
						sysCheck++;
						objSysCheck.checked = true; 
						
						fnShow('div'+objSystem.value, objSystem.value);
						tempSys = objSystem.value;		  
					} 
					break;	
				} 	
					
			}	 
			
	}	 
}	

function fnSubmit()
{ 
	fnSave();
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}   
	document.frmClassifyGrp.strOpt.value =  'save';
	fnStartProgress('Y');
    document.frmClassifyGrp.submit();
  }


function fnNext()
{   
	 var vSelGrpList =  document.frmClassifyGrp.strSelectedGrpType.value ;
	 var curRuleType = document.frmClassifyGrp.sysRuleTypeID.value;
	 var curRuleSeq = 0;
	 
	 for(var k=1;k<=ruleTypeSize;k++){
			arr = eval("RuleType"+k);
			arrobj = arr.split(",");
			id = arrobj[0];
			grpType = arrobj[1];
		 if(curRuleType == id){
			 curRuleSeq = k;
			 break;
		 }
	 }
	 var fwdGroupType = "";
	 var fwdRuleTypeId = "";	 
	 arrayGrp = vSelGrpList.split(",");
	 arrayGrp.sort(sortfunc);
	 for(var i=0;i<arrayGrp.length;i++){
		 if(curRuleSeq<arrayGrp[i]){
			 arr = eval("RuleType"+arrayGrp[i]);
			 arrobj = arr.split(",");
			 fwdRuleTypeId = arrobj[0];
			 fwdGroupType = arrobj[1];
			 break;
		 }
			 
	 }
	 if(fwdGroupType == ""){
		 document.frmClassifyGrp.fwdGroupType.value = '';
		 document.frmClassifyGrp.strOpt.value = 'confirm'; 
	 }else{
		 document.frmClassifyGrp.fwdRuleTypeID.value = fwdRuleTypeId;
		 document.frmClassifyGrp.fwdGroupType.value = fwdGroupType;
		 document.frmClassifyGrp.strOpt.value = 'next';
	 }
	 fnSave();
	 if (ErrorCount > 0)
		{
				Error_Show();
				Error_Clear();
				return false;
		}  
	document.frmClassifyGrp.submit();
}

function sortfunc(a,b)
{
return a - b;
}


function fnBack()
{    
	
	 var vSelGrpList =  document.frmClassifyGrp.strSelectedGrpType.value ;
	 var curRuleType = document.frmClassifyGrp.sysRuleTypeID.value;
	 var curRuleSeq = 0;
	 for(var k=1;k<=ruleTypeSize;k++){
		 arr = eval("RuleType"+k);
		 arrobj = arr.split(",");
		 id = arrobj[0];
		 grpType = arrobj[1];
		 if(curRuleType == id ){
			 curRuleSeq = k;
			 break;
		 }
	 }
	 var fwdGroupType = "";
	 var fwdRuleTypeId = "";	 
	 arrayGrp = vSelGrpList.split(",");
	 arrayGrp.sort(sortfunc);
	 for(var i=arrayGrp.length-1;i>=0;i--){
		 if(eval(curRuleSeq)> arrayGrp[i]){
			 arr = eval("RuleType"+arrayGrp[i]);
			 arrobj = arr.split(",");
			 fwdRuleTypeId = arrobj[0];
			 fwdGroupType = arrobj[1];
			 break;
		 }
	 }
	 if(fwdGroupType == ""){
		 document.frmClassifyGrp.action = "gmClassifyOrd.do?strOpt=edit";
	 }else{
		 document.frmClassifyGrp.fwdRuleTypeID.value = fwdRuleTypeId;
		 document.frmClassifyGrp.fwdGroupType.value = fwdGroupType;
		 document.frmClassifyGrp.strOpt.value = 'edit';
	 }
	 
	   document.frmClassifyGrp.submit();
}

function fnSave()
{ 
	var inputString = '';   
	var checkflag = 0;   
	for (var k=0; k < size ; k++) 
	{
		
		objGrpId = eval("document.frmClassifyGrp.GroupId"+k); 	
		objSelected = eval("document.frmClassifyGrp.check"+k);  
	 
		if(objSelected.checked == true)
		{
			inputString = inputString + objGrpId.value + ',';
		}
		   
	}  
	for (var j=0; j < size ; j++) 
	{  
		objSelected = eval("document.frmClassifyGrp.check"+j);
		 
			if(objSelected.checked == true)
			{	 
				checkflag = 1;
			} 	 
		
	}	 
	 
	if (checkflag ==0)
	{
		 Error_Details(message_prodmgmnt[2]);	
	}
	document.frmClassifyGrp.hinputStr.value = inputString; 
}
