var objGridData;
function fnOnPageLoad()
{  
  if(document.frmClassifyOrd.systemID.value != '0'){
	  gridObj = initGridWithDistributedParsing('dataGridDiv',objGridData);
	  var gridrows =gridObj.getAllRowIds();
	  var gridrowsarr = gridrows.toString().split(",");
	  var rowCount = (gridrowsarr != '')?(6-gridrowsarr.length):6;
	  for (var rowid = 0; rowid < rowCount; rowid++){
		  addRow();
	  }
	  gridObj.attachEvent("onEditCell",doOnCellEdit);
	  gridObj.attachEvent("onRowSelect",doOnRowSelect);
  }
}

function doOnRowSelect(rowId,cellIndex){
	  gridObj.editCell();
}
function doOnCellEdit(stage, rowId, cellInd, nValue, oValue) {		
	if( stage == 2 ){
		gridObj.clearSelection();
		return true;
	}
}
function fnLoad(val){
	if(val == '0'){
		document.frmClassifyOrd.strOpt.value='load';	
	}else{
		document.frmClassifyOrd.strOpt.value='reload';
	}
	document.frmClassifyOrd.submit();
}

function fnBack(){
	document.frmClassifyOrd.strOpt.value='';
	document.frmClassifyOrd.action='/gmClassifyGrp.do';
	document.frmClassifyOrd.submit();
}
function removeSelectedRow(){
	var gridrows=gridObj.getSelectedRowId();
	var isExistRow=false;
	var deleteRowStr = '';
	if(gridrows!=undefined){
		var NameCellValue ;
		var gridrowsarr = gridrows.toString().split(",");
		var added_row;
		for (var rowid = 0; rowid < gridrowsarr.length; rowid++) {
			gridrowid = gridrowsarr[rowid];
			added_row = gridObj.getRowAttribute(gridrowid,"row_added");
			if(added_row!=undefined && added_row=="Y"){
					gridObj.deleteRow(gridrowid);
			}else{
					vGrpId    = gridObj.cellById(gridrowid, 0).getValue();
					vLinkId1  = gridObj.cellById(gridrowid, 3).getValue();
					vLinkId2  = gridObj.cellById(gridrowid, 6).getValue();
					vQty      = gridObj.cellById(gridrowid, 8).getValue();
					deleteRowStr = vQty+","+vGrpId+"^,";
					if( vLinkId1 != ''){
						deleteRowStr += vLinkId1+"^,";
					}
					if( vLinkId2 != ''){
						deleteRowStr += vLinkId2+"^,";
					}
					deleteRowStr+='|';
			}
		}
		var hDelRowVal = document.frmClassifyOrd.hDeletedRowStr.value;
		document.frmClassifyOrd.hDeletedRowStr.value = hDelRowVal+ deleteRowStr;
		gridObj.deleteRow(gridrowid);
		gridObj.clearSelection();
	}else{
		Error_Clear();
		Error_Details(message[502]);				
		Error_Show();
		Error_Clear();
	}
	
}

function addRow(){
	var uid=gridObj.getUID();
	gridObj.addRow(uid,["","","And/Or","","","And/Or","","",""]);
	gridObj.setRowAttribute(uid,"row_added","Y");
}
function fnFormInputString(){
	gridObj.editStop();// retun value from editor(if presents) to cell and closes editor
	var gridrowid;
	var inputString='';
	var gridrows =gridObj.getChangedRows(",");
	
	if(gridrows.length==0){
		inputString = "";
		
	}
	else
	{
		var gridrowsarr = gridrows.toString().split(",");
		var columnCount = gridObj.getColumnsNum();
		
		for (var rowid = 0; rowid < gridrowsarr.length; rowid++) {
			var ruleString='';
			gridrowid = gridrowsarr[rowid];
			vGrpId    = gridObj.cellById(gridrowid, 0).getValue();
			vGrpType  = gridObj.cellById(gridrowid, 1).getValue();
			vLinkId1  = gridObj.cellById(gridrowid, 3).getValue();
			vLinkGrp1 = gridObj.cellById(gridrowid, 4).getValue();
			vLinkId2  = gridObj.cellById(gridrowid, 6).getValue();
			vLinkGrp2 = gridObj.cellById(gridrowid, 7).getValue();
			vQty      = gridObj.cellById(gridrowid, 8).getValue();

			if(vGrpId == ''){
				if(vGrpType == ''){
					if(vLinkGrp1 != '' || vLinkGrp2 != '' ){
							Error_Details(message_prodmgmnt[3]);
							return;
					}else{
						continue;
					}
				}else{
					if(isNaN(vQty) || vQty == '' ||  vQty <= 0||  vQty > 9){
						Error_Details(message_prodmgmnt[4]);
						return;
					}else{	
						ruleString = vQty+",^"+vGrpType+",";
						if(vLinkGrp1 != ''){
							ruleString += "^"+vLinkGrp1+",";
						}
						if(vLinkGrp2 != ''){
							ruleString += "^"+vLinkGrp2+",";
						}
					}
				}
			}else{
				if(isNaN(vQty) || vQty == '' ){
					Error_Details(message_prodmgmnt[4]);
					return;
				}
				ruleString = vQty+","+vGrpId+"^"+vGrpType+",";
				if( vLinkId1 != '' || vLinkGrp1 != ''){
					ruleString += vLinkId1+"^"+vLinkGrp1+",";
				}
				if( vLinkId2 != '' || vLinkGrp2 != ''){
					ruleString += vLinkId2+"^"+vLinkGrp2+",";
				}
			}	
			inputString+=ruleString+"|";
		}
	
}
	document.frmClassifyOrd.inputString.value = document.frmClassifyOrd.hDeletedRowStr.value + inputString;
}

function fnSubmit()
{
		fnFormInputString();
		if (ErrorCount > 0)
		{
			Error_Show();
			Error_Clear();
			return false;
		}
		var gridrows =gridObj.getAllRowIds();
		if(gridrows != ''){
			var gridrowsarr = gridrows.toString().split(",");
			for (var rowid = 0; rowid < gridrowsarr.length; rowid++){
				gridrowid = gridrowsarr[rowid];
				vGrpType  = gridObj.cellById(gridrowid, 1).getValue();
				if( rowid == 0){
					document.frmClassifyOrd.fwdGroupType.value=vGrpType;
				}
			}
		}
		document.frmClassifyOrd.status.value='11500'
		document.frmClassifyOrd.strOpt.value='save';
		fnStartProgress('Y');
		document.frmClassifyOrd.submit();
}
function fnEdit()
{
		document.frmClassifyOrd.status.value='11500'
		document.frmClassifyOrd.strOpt.value='save';
		document.frmClassifyOrd.submit();
}

function fnNext()
{
		var errFlag = true;
		var gridrows =gridObj.getAllRowIds();
		if(gridrows != ''){
			var gridrowsarr = gridrows.toString().split(",");
			for (var rowid = 0; rowid < gridrowsarr.length; rowid++){
				gridrowid = gridrowsarr[rowid];
				vGrpId    = gridObj.cellById(gridrowid, 0).getValue();
				vGrpType  = gridObj.cellById(gridrowid, 1).getValue();
				vLinkGrp1 = gridObj.cellById(gridrowid, 4).getValue();
				vLinkGrp2 = gridObj.cellById(gridrowid, 7).getValue();
				if(vGrpType != '' || vLinkGrp1 != ''|| vLinkGrp2 != ''){
					errFlag = false;
				}
				if( rowid == 0){
					document.frmClassifyOrd.fwdGroupType.value=vGrpType;
					document.frmClassifyOrd.fwdRuleTypeID.value=vGrpId;
				}
			}
		}
		if(errFlag){
			Error_Details(message_prodmgmnt[5] );
		}else{
			fnFormInputString();
		}
		if (ErrorCount > 0)
		{
			Error_Show();
			Error_Clear();
			return false;
		}
		document.frmClassifyOrd.status.value='11500'
		document.frmClassifyOrd.strOpt.value='next';
		document.frmClassifyOrd.submit();
}

function fnPublish(){
	var errGroups = "";
		for(var i = 0 ;i<listSize;i++){
			var errObj = eval("document.frmClassifyOrd.errGroup"+i);
			if(errObj != undefined){
				errGroups +=", "+errObj.value;
			}
		}
		if(errGroups != ""){
			Error_Details(Error_Details_Trans(message_prodmgmnt[6],errGroups.substring(1, errGroups.length)));
					}
		if (ErrorCount > 0)
		{
			Error_Show();
			Error_Clear();
			return false;
		}
		document.frmClassifyOrd.status.value='11501'
		document.frmClassifyOrd.strOpt.value='publish';
		document.frmClassifyOrd.submit();	
}