var mygrid;
var part_colId, sub_part_colId, part_desc_colId, lprcode_colId, price_colId, tva_colId , sub_cmp_colId;
var arrValuefromClipboard = new Array();
var copyFromExcelFL;
var subPart ='';
var partDesc  ='';
var lpprCode  ='';
var partPrice ='';
var tvpValue  ='';
var partNum  ='';
var subCompId ='';
function fnOnPageLoad(){	

	var strOptVal = document.frmSubComponentMapping.strOpt.value;
	
	if(objGridData){
		//mygrid = initGrid('dataGridDiv',objGridData);
		if(strOptVal == "load"){
			mygrid = initGrid('dataGridDiv',objGridData);
		var gridrows   = mygrid.getAllRowIds();
		var gridrowsarr = gridrows.split(",");	
		
		mygrid.attachHeader("#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter");
		mygrid.enableTooltips("true,true,true,true,true,true,true,true,true");
		mygrid.attachEvent("onEditCell",doOnCellEdit);
		mygrid.attachEvent("onKeyPress", onKeyPressed);
		mygrid.enableBlockSelection(true);
		part_colId = mygrid.getColIndexById("partnum");
		sub_part_colId = mygrid.getColIndexById("subPartNum");
		part_desc_colId = mygrid.getColIndexById("partdesc");
		lprcode_colId = mygrid.getColIndexById("lpprcode");
		price_colId = mygrid.getColIndexById("price");
		tva_colId = mygrid.getColIndexById("tva");
		sub_cmp_colId = mygrid.getColIndexById("subCmpID");		
		mygrid.enableEditTabOnly(true); // Tab functionality for Editable fields only	
		
		
	}else{
	
	    var dataHost = {};
	    var rows = [], i = 0;
	    var JSON_Array_obj = "";
	JSON_Array_obj = JSON.parse(objGridData);
	$.each(JSON_Array_obj, function(index, jsonObject) {
		subPart = jsonObject.SUBPARTNUM;
		partDesc = jsonObject.PARTDESC;
		lpprCode = jsonObject.LPPRCODE;
		partPrice = jsonObject.PARTPRICE;
		tvpValue = jsonObject.TVPVALUE;
		partNum = jsonObject.PARNUM;
		subCompId = jsonObject.SUBCMPID;
		
	
		rows[i] = {};
		rows[i].id = subCompId;
		rows[i].data = [];
		
		rows[i].data[0] = subPart;
		rows[i].data[1] = partDesc;
		rows[i].data[2] = lpprCode;
		rows[i].data[3] = partPrice;
		rows[i].data[4] = tvpValue;
		rows[i].data[5] = partNum;
		rows[i].data[6] = subCompId;
	
		i++;
	});
   dataHost.rows = rows;
	
		mygrid = new dhtmlXGridObject('dataGridDiv');
		mygrid.setImagePath("/extweb/suite/5.2.0/codebase/imgs/");
        mygrid.setHeader("Sub Component Number,Sub Component Description,LPPR Code, Price(?),TVA %,Part Number,Sub Comp ID");
			if(pricingAccess =='Y'){
				if(qualityAccess =='Y'){
				mygrid.setColTypes("ed,ed,ed,ro,ro,ed,ro");
			}else{
				mygrid.setColTypes("ed,ro,ed,ed,ed,ed,ro");
				}
			}else{
				if(qualityAccess =='Y'){
				mygrid.setColTypes("ed,ed,ed,ro,ro,ed,ro");
				}else{
					mygrid.setColTypes("ed,ro,ed,ro,ro,ed,ro");
				}
			}
			
		mygrid.setInitWidths("90,*,100,100,90,90,*");
		mygrid.setColSorting("str,str,int,str,str,str");
		mygrid.setColumnIds("subPart,partDesc,lpprCode,partPrice,tvpValue,partNum,subCompId");
		mygrid.enableEditEvents(false, true, true);
		mygrid.attachEvent("onKeyPress", onKeyPressed);
		mygrid.attachEvent("onEditCell",doOnCellEdit);
		mygrid.enableTooltips("true,true,true,true,true,true,true,true,true");
		mygrid.attachEvent("onRowPaste",function(id){
        mygrid.forEachCell(id,function(obj){
                   obj.cell.wasChanged = true;
                   });
			});		
		
		mygrid.attachHeader("#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter");
		mygrid.init();		
		mygrid.enableAlterCss("even","uneven");
		mygrid.parse(dataHost,"json");	
		mygrid.enableBlockSelection(true);
		mygrid.enableAlterCss("even","uneven");
	    part_colId = mygrid.getColIndexById("partNum");
		sub_part_colId = mygrid.getColIndexById("subPart");
		part_desc_colId = mygrid.getColIndexById("partDesc");
		lprcode_colId = mygrid.getColIndexById("lpprCode");
		price_colId = mygrid.getColIndexById("partPrice");
		tva_colId = mygrid.getColIndexById("tvpValue");
		sub_cmp_colId = mygrid.getColIndexById("subCompId");		
		mygrid.enableEditTabOnly(true); // Tab functionality for Editable fields only	
	
		mygrid.setColumnHidden(sub_cmp_colId, true);
			var gridrows   = mygrid.getAllRowIds();
		var gridrowsarr = gridrows.split(",");	
	
	//add_fast();
	}
		
		
			
	}	
}

// To Initiate the grid
function initGrid(divRef,gridData){
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.init();
	gObj.loadXMLString(gridData);	
	return gObj;	
}


//Funciton will be called while editing the columns in the grid
function doOnCellEdit(stage, rowId, cellInd, nValue, oValue){
	
	if(cellInd == part_colId && (oValue != '' || nValue != '') && stage == 2){//once the value in part number column
		mygrid.setRowAttribute(rowId,"row_modified","Y");
 	}else if(cellInd == sub_part_colId && (oValue != '' || nValue != '') && stage == 2){//once the value in Adjustment code column
 		mygrid.setRowAttribute(rowId,"row_modified","Y");
 	}else if(cellInd == part_desc_colId && (oValue != '' || nValue != '') && stage == 2){//once the value in Adjustment Value column
 		mygrid.setRowAttribute(rowId,"row_modified","Y");
 	}else if(cellInd == lprcode_colId && (oValue != '' || nValue != '') && stage == 2){//once the value in Adjustment Value column
 		mygrid.setRowAttribute(rowId,"row_modified","Y");
 	}else if(cellInd == price_colId && (oValue != '' || nValue != '') && stage == 2){//once the value in Adjustment Value column
 		mygrid.setRowAttribute(rowId,"row_modified","Y");
 	}else if(cellInd == tva_colId && (oValue != '' || nValue != '') && stage == 2){//once the value in Adjustment Value column
 		mygrid.setRowAttribute(rowId,"row_modified","Y");
 	}
	
 	return true;
}


//Use the below function to copy data inside the screen.
function docopy(){
      mygrid.setCSVDelimiter("\t");
      mygrid.copyBlockToClipboard();
     // mygrid._HideSelection();
}

//For doing copy and paste using keyboard short cut
function onKeyPressed(code,ctrl,shift){
   if(code==67&&ctrl){
      // ctrl-c
	   //docopy();
 mygrid.setCSVDelimiter("\t");
      mygrid.copyBlockToClipboard();
   }
   if(code==86&&ctrl){
      //ctrl-v
 mygrid.setCSVDelimiter("\t");
mygrid.pasteBlockFromClipboard();
	 //  pasteToGrid();
   }
   return true;
}

function addRow(){
	//mygrid.addRow(mygrid.getUID(),'');
	var newRowId = mygrid.getUID();
	mygrid.addRow(newRowId, '');
}

//Use the below function to remove the rows from the grid.
function removeSelectedRow(){
      var gridrows=mygrid.getSelectedRowId();   
      if(gridrows!=undefined)
      {
            var NameCellValue ;
            var gridrowsarr = gridrows.toString().split(",");
            var added_row;
            var dtlId;
            for (var rowid = 0; rowid < gridrowsarr.length; rowid++)
            {
                  gridrowid = gridrowsarr[rowid];                  
                  mygrid.setRowAttribute(gridrowid,"row_modified","N");
                  mygrid.deleteRow(gridrowid);                                
            }
      }else{
            Error_Clear();
            Error_Details(message_prodmgmnt[120]);                       
            Error_Show();
            Error_Clear();
      }
}

var removeEmptyFl = false;
var confirmMsg = true;

//To add datas from clipboard.
function addRowFromClipboard() {
	copyFromExcelFL = 'Y';
	var cbData = mygrid.fromClipBoard();
	var subPartNum, subPartDesc, lpprCode , partPrice , partTVA;
    if (cbData == null) {
          Error_Details(message_prodmgmnt[121]);
    }  
    var rowData = cbData.split("\n");
    var arrLen = (rowData.length) - 1;    
    if(rowData.length - 1 == 0){
		Error_Details(message_prodmgmnt[122]);
    }
    
    if(rowData.length-1 > 1000){
		fnStopProgress();
		Error_Details(message_prodmgmnt[123]);		
	}
    
    if (ErrorCount > 0) {
          Error_Show();
          Error_Clear();
          return;
    }
	fnStartProgress('Y');
  
    var newRowData = '';
    setTimeout(function() {	
    mygrid.startFastOperations();
    mygrid.filterBy(0,"");//unfilters the grid
    mygrid._f_rowsBuffer = null; //clears the cache
    for ( var i = 0; i < arrLen; i++) {    	
          newRowData = rowData[i];          
          rowcolumnData = rowData[i].split("\t");
          var row_id = mygrid.getUID();
          mygrid.setCSVDelimiter("\t");
          mygrid.setDelimiter("\t");
          mygrid.addRow(row_id, '');
          mygrid.cellById(row_id, sub_part_colId).setValue(rowcolumnData[0]);
		  mygrid.cellById(row_id, part_desc_colId).setValue(rowcolumnData[1]);
		  mygrid.cellById(row_id, lprcode_colId).setValue(rowcolumnData[2]);
		  mygrid.cellById(row_id, price_colId).setValue(rowcolumnData[3]);
		  mygrid.cellById(row_id, tva_colId).setValue(rowcolumnData[4]);
		  mygrid.cellById(row_id, part_colId).setValue(rowcolumnData[5]);
		  
          mygrid.setRowAttribute(row_id, "row_added", "Y");
          mygrid.setRowAttribute(row_id,"row_modified","Y");
          mygrid.setDelimiter(",");
    }
    mygrid.stopFastOperations();
	fnStopProgress();	    
	},100);
}

//To paste the data to the grid
function pasteToGrid(){
	copyFromExcelFL = 'Y';
	if(mygrid._selectionArea!=null){
		var enterCnt = 0;
		var subPartNum, subPartDesc, lpprCode , partPrice , partTVA , partNumber;
		var topRowCnt, rowID;
		var cbData = mygrid.fromClipBoard();
		if (cbData == null) {
			fnStopProgress();
			Error_Details(message_prodmgmnt[89]);
			Error_Show();
        	Error_Clear();
        	return;
		}
		var emptyRowCnt = 0;
		var partExist,added_row;
		var rowData = cbData.split("\n");
		rowcolumnData = rowData[0].split("\t");
		
		if(rowcolumnData.length > 1){
			for (var i=0; i< rowData.length-1; i++){
				rowcolumnData = rowData[i].split("\t");
				if(i == 0){
					topRowCnt = mygrid.getSelectedBlock().LeftTopRow;// Get the top index of selected row
				}else{
					topRowCnt++;
				}
				rowID=mygrid.getRowId(topRowCnt); // Get the row id to paste the data
				
				if(enterCnt == 0){// check whether there are enough rows to paste the record or not
					var gridrowsarr = '';
					var gridrows=mygrid.getAllRowIds();
					if(gridrows!=undefined) gridrowsarr = gridrows.toString().split(",");
					// Get the empty rows count to paste the record
					for(var j=topRowCnt; j< gridrowsarr.length; j++){
						emptyRowCnt++;
					}

					if(emptyRowCnt < rowData.length-1){
						fnStopProgress();
						Error_Details(message_prodmgmnt[124]);
						Error_Show();
			        	Error_Clear();
			        	return;
					}
					enterCnt++;
				}
							
				subPartNum = rowcolumnData[0];
				subPartDesc = rowcolumnData[1];
				lpprCode = rowcolumnData[2];
				partPrice = rowcolumnData[3];
				partTVA = rowcolumnData[4];
				partNumber = rowcolumnData[5];
									
				mygrid.cellById(rowID, sub_part_colId).setValue(subPartNum);
				mygrid.cellById(rowID, part_desc_colId).setValue(subPartDesc);
				mygrid.cellById(rowID, lprcode_colId).setValue(lpprCode);
				mygrid.cellById(rowID, price_colId).setValue(partPrice);
				mygrid.cellById(rowID, tva_colId).setValue(partTVA);
				mygrid.cellById(rowID, part_colId).setValue(partNumber);
				
				mygrid.setRowAttribute(rowID,"row_modified","Y");
			
			    mygrid._HideSelection();
			    //document.all.Btn_Save.disabled = false;
			}
		}else{
			mygrid.pasteBlockFromClipboard();
	//	    //mygrid._HideSelection();
		}
			
	}else{
		Error_Details(message_prodmgmnt[125]);
		Error_Show();
        Error_Clear();
	}

	
}

function fnReload(){
	var partNumber = document.frmSubComponentMapping.partNum.value;
	var subComponentNumber = document.frmSubComponentMapping.subComponentNum.value;
	var partLike = document.frmSubComponentMapping.partLike.value;
	var subComponentLike = document.frmSubComponentMapping.subPartLike.value;
		
	if(partNumber == '' && (partLike != '' && partLike != '0')){
		Error_Details(message_prodmgmnt[126]);
	}	
	if(subComponentNumber == '' && (subComponentLike != '' && subComponentLike != '0')){
		Error_Details(message_prodmgmnt[127]);
	}
	// Following code is commenting based on the Jeff's comment on email on 04/08/2016
	/*if((partNumber == '' && partLike == '0') && (subComponentNumber == '' && subComponentLike == '0')){
		Error_Details("Please input the data for at least one filter");
	}*/
		
	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}else{		
		document.frmSubComponentMapping.strOpt.value = 'load';
		document.frmSubComponentMapping.action = '/gmSubComponentMapping.do?method=loadPartSubCompDtls';
		fnStartProgress();
		document.frmSubComponentMapping.submit();		
	}	
}

function fnSubmitAction(){
	mygrid.editStop();
	var confirmMsg = true;
	var j;
	var inputstr = '';
	var partInputStr = '';
	var added_row;
	var pnum, subCompPartID, partDesc, lpprCode, partPrice, tvaValue, priceWthTVA ,rowIndex , subCmpID;
	var errorPrice = '';
	var errorTva = '';
	var morePrice = '';
	var moreSubComponent = '';
	var emptyPartNum = '';
	var errorSubParts = '';
	var emptySubParts = '';
	var screenAccess= '';
	
	var gridrows = mygrid.getChangedRows(true);
	var rowsarr = gridrows.toString().split(",");
	if (rowsarr.length > 0 && rowsarr != '') {
			for (var k = 0; k < rowsarr.length; k++) {
				var rowcurrentid = rowsarr[k];
				rowIndex=mygrid.getRowIndex(rowcurrentid);
				pnum = mygrid.cells(rowcurrentid, part_colId).getValue().trim();
				subCompPartID =  mygrid.cells(rowcurrentid, sub_part_colId).getValue().trim();
				subCompPartID = subCompPartID.toUpperCase();
				partDesc =  mygrid.cells(rowcurrentid, part_desc_colId).getValue().trim();
				lpprCode =  mygrid.cells(rowcurrentid, lprcode_colId).getValue().trim();
				partPrice = mygrid.cells(rowcurrentid, price_colId).getValue().trim();
				tvaValue =  mygrid.cells(rowcurrentid, tva_colId).getValue().trim();
				// Validation for cecking the Sub Component Number having the Suffix of A to Z alphabets.				
				if(subCompPartID != ''){
					var lastValue = subCompPartID.substring(subCompPartID.length-1);
					var tempString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
					var isCharAvail = tempString.search(lastValue);
					if(isCharAvail == -1 && pnum != subCompPartID) errorSubParts = errorSubParts + "<br>" + subCompPartID + "";
				}else{
					emptySubParts = emptySubParts + "<BR>" + pnum + "";
				}			
				
				// Validating the Price column for not allowing the Other than Numbers and Nagative Price
				if(isNaN(partPrice) || partPrice < 0){
					errorPrice = errorPrice +"<br>"+subCompPartID+" - "+partPrice+"";
					Error_Clear();
				}
				// Validating the TVA% column for not allowing the Other than Numbers and Nagative Price
				if(isNaN(tvaValue) || tvaValue < 0){
					errorTva = errorTva +"<br>"+subCompPartID+" - "+tvaValue+"";
					Error_Clear();
				}
				// Validating the Price Column for not allowing the price more than 15 digits
				var priceLength = partPrice.length;   
	            if(priceLength>=14){	            	   
	            	morePrice = morePrice +"<br>"+subCompPartID+" - "+partPrice+"";
	            }
	           // Validating the Sub Component Column for not allowing the price more than 20 digits
	            var subCompLength = subCompPartID.length;   
	            if(subCompLength>=19){	            	   
	            	moreSubComponent = moreSubComponent +"<br>"+subCompPartID+"";
	            }
	            // Input String preparation based on the User Access levels
				subCmpID =  mygrid.cells(rowcurrentid, sub_cmp_colId).getValue();
				if(pricingAccess == 'Y') screenAccess = 'PRICING';
				if(qualityAccess == 'Y') screenAccess = 'QUALITY';
				if((qualityAccess == 'Y') && (pricingAccess == 'Y')) screenAccess = '';
				
				if(pnum != ''){
					if(pricingAccess == 'Y' && qualityAccess == 'Y'){
						inputstr = inputstr + pnum.toUpperCase() + '^' + subCompPartID + '^'+ partDesc + '^'+ lpprCode +  '^' + partPrice + '^' + tvaValue + '^' + subCmpID + '|';						
					}else if(qualityAccess == 'Y'){
						inputstr = inputstr + pnum.toUpperCase() + '^' + subCompPartID + '^'+ partDesc + '^'+ '' +  '^' + '' + '^' + '' + '^' + '' + '|';
					}else if(pricingAccess == 'Y'){
						inputstr = inputstr + pnum.toUpperCase() + '^' + subCompPartID + '^'+ '' + '^'+ lpprCode +  '^' + partPrice + '^' + tvaValue + '^' + subCmpID + '|';
					}
					partInputStr = partInputStr + ',' + pnum.toUpperCase();
				}else{
					// Validating the Sub Component Numbers which are not Having the Part Numbers
					emptyPartNum = emptyPartNum +"<br>"+subCompPartID+"";
				}
			}
		}else{
			Error_Details(message_prodmgmnt[128]);
		}
	document.frmSubComponentMapping.partInputString.value = partInputStr;
	// Validating the Sub Component Numbers which are not Having the Part Numbers
	if (emptyPartNum != ''){
		Error_Details(Error_Details_Trans(message_prodmgmnt[129],emptyPartNum));		
	}
	// Error message for invalid Price..
	if (errorPrice != ''){
		Error_Details(Error_Details_Trans(message_prodmgmnt[130],errorPrice));		
	}
	// Error message for invalid TVA..
	if (errorTva != ''){
		Error_Details(Error_Details_Trans(message_prodmgmnt[131],errorTva));	
	}
	// Error message for Price which is greater than the specified size.
	if (morePrice != ''){
		Error_Details(Error_Details_Trans(message_prodmgmnt[132],morePrice));		
	}
	// Validating the Sub Component Column for not allowing the price more than 20 digits
	if (moreSubComponent != ''){
		Error_Details(Error_Details_Trans(message_prodmgmnt[133],moreSubComponent));		
	}
	// Validation for cecking the Sub Component Number having the Suffix of A to Z alphabets.
	if(errorSubParts != ''){
		Error_Details(Error_Details_Trans(message_prodmgmnt[134],errorSubParts));
	}
	// Validation for cecking the Sub Component Number should not be empty for entered Part Number
	if(emptySubParts != ''){
		Error_Details(Error_Details_Trans(message_prodmgmnt[135],emptySubParts));
	}
	
	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}else{
		document.frmSubComponentMapping.strOpt.value = 'save';
		document.frmSubComponentMapping.screenAccessFl.value = screenAccess;
		document.frmSubComponentMapping.strInputString.value = inputstr;
		document.frmSubComponentMapping.action = '/gmSubComponentMapping.do?method=savePartSubCompDtls';
		fnStartProgress();
		document.frmSubComponentMapping.Btn_Save.disabled = true;
		document.frmSubComponentMapping.submit();
	}
}

function fnDownloadXLS(){
	mygrid.toExcel('/phpapp/excel/generate.php');
}
function add_fast(){
 mygrid.startFastOperations();
	for(var i=1 ;i<500 ;i++){
	   addRow();
	}
 mygrid.stopFastOperations();
}