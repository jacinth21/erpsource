//Ajax call to get the Status of the SET.
function fnGetSetStatus(id,  setVal){
	setVal = TRIM(setVal);
	var validSetSuc = eval("document.all.validSetSuc"+id);	
	var validSetErr = eval("document.all.validSetErr"+id);
	var status = eval("document.all.hStatus"+id); 
	if(setVal != ''){
		dhtmlxAjax.get('/gmSystemMapping.do?strOpt=getSetStatus&setGroup=1601&setId=' + setVal +'&ramdomId=' + Math.random(),function(loader){
				var response = loader.xmlDoc.responseText;			
				if(response == ''){
						Error_Details("<b>"+setVal+message_prodmgmnt[342]);
						var obj = eval("document.all.setId"+id); 
						obj.value = '';	
						status.value = '';	
						validSetErr.style.display = 'none';
						validSetSuc.style.display = 'none';
				}
				else if(response!='20367'){ //20367 --Approved status
						//Error_Details(" Cannot include <b>"+setVal+"</b> as the set is not in Approved status");						
						status.value = response;
						validSetErr.style.display = 'block';
						validSetSuc.style.display = 'none';
				}else{
						status.value = response;	
						validSetSuc.style.display = 'block';
						validSetErr.style.display = 'none';
				}
				if(ErrorCount > 0){
						Error_Show();
						Error_Clear();
						return false;
				}
		});
	}else{
			if(validSetSuc.style.display == 'block'){
				validSetSuc.style.display = 'none';
			}
			if(validSetErr.style.display  == 'block'){
				validSetErr.style.display = 'none';
			}
	}
}
//Baseline validation function calling from Set Type, BaseLine DropDown. 
function fnBaselineValidate(rowId){
	var baseLineObj = eval("document.all.setBaseLine"+rowId); 
	var baseLineValue = baseLineObj.value ;
	if(baseLineValue=='Y'){
		var  setTypeobj = eval("document.all.setType"+rowId); 
		var  setTypeVal = setTypeobj.value ;
		if(setTypeVal!='0'){
			if(setTypeVal!='103148' && setTypeVal!='103147'){
					Error_Details(message_prodmgmnt[106]);
					baseLineObj.value = 0;
					Error_Show();
					Error_Clear();
					return false;
			}
		}
	}
}

//To remove passed the line items values
function fnRemoveItem(val){
	var obj = eval("document.all.setId"+val); 
    obj.value = '';
    obj = eval("document.all.setType"+val); 
    obj.value = 0;
	obj = eval("document.all.setBaseLine"+val); 
    obj.value = 0;
	obj = eval("document.all.setShared"+val); 
	if(obj){
	    obj.value = 0;
	}
	obj = eval("document.all.hStatus"+val); 
	obj.value = '';
	var divElement = document.getElementById('validSetSuc'+val);
	divElement.style.display='none';
	var divElement = document.getElementById('validSetErr'+val);
	divElement.style.display='none';
	
	
}

var cnt=2;

//To add more rows.
function fnAddRow(form, typeOptions, baselineOptions, sharedOptions){	
	if(form){
		cnt = document.all.hRowCnt.value;
	};
	cnt++;

    var tbody = document.getElementById("SystemMapping").getElementsByTagName("TBODY")[0];
    var row = document.createElement("TR")

	var td0 = document.createElement("TD")
    td0.innerHTML = '<a href=javascript:fnRemoveItem('+cnt+'); ><img border=0 tabindex=\'-1\' Alt=Remove  title=Remove  valign=left src=/images/btn_remove.gif height=10 width=9></a>';
	td0.align = "right";

    var td1 = document.createElement("TD")
    td1.innerHTML = '<input type=text size=10 name=setId'+cnt+' class=InputArea onBlur= javascript:fnGetSetStatus('+cnt+',this.value); > <input type=hidden name=hStatus'+cnt+' value="">';
	td1.align = "center";

	var td2 = document.createElement("TD")
    td2.innerHTML = '<span id=validSetSuc'+cnt+' style=\'vertical-align:top; display: none;\'> <img tabindex=\'-1\' height=16 width=19  align=left title=\'Set available to Map\' src=/images/success.gif></img></span><span id=validSetErr'+cnt+' style=\'vertical-align:top; display: none;\'>&nbsp;<img tabindex=\'-1\' height=13 title=\'Set doesnot available to Map\' width=13 src=/images/error.gif></img></span>';
	
	var td3 = document.createElement("TD")
    td3.innerHTML = '<select name=setType'+cnt+' class=RightText tabindex="-1"  onchange= javascript:fnBaselineValidate('+cnt+');>'+typeOptions+'</select>';

	var td4 = document.createElement("TD")
    td4.innerHTML = '<select name=setBaseLine'+cnt+' class=RightText tabindex="-1" onchange= javascript:fnBaselineValidate('+cnt+'); >'+baselineOptions+'</select>';
	
	var td5 = document.createElement("TD")
    td5.innerHTML = '<select name=setShared'+cnt+' class=RightText tabindex="-1">'+sharedOptions+'</select>';
	row.appendChild(td0);
	row.appendChild(td1);
	row.appendChild(td2);
	row.appendChild(td3);
    row.appendChild(td4);
    row.appendChild(td5);
    tbody.appendChild(row);

	document.all.hRowCnt.value = cnt;
}

//onload function to load the Grid data.
function fnOnloadSystemMapping(){	

	var divElement = document.getElementById('setMapDtlsData');
	var frm = '';
	var mygrid = '';
	if(document.frmSystemMapping){
		frm = document.frmSystemMapping;
	}else{
		frm = document.all;
	}
	var gridObjData = '';
	if(frm.gridData){
		gridObjData = frm.gridData.value;
	}
	var intSize = 0;
	if(frm.gridData){
		intSize = frm.setMapDtlssize.value;
	}

	if (intSize==0)	{
		divElement.align = 'center';
		divElement.innerHTML='<font color=blue>'+message_prodmgmnt[343]+'</font>';
	}
	else if(gridObjData!=''){
		mygrid = initGridData('setMapDtlsData',gridObjData);
	}
}

function initGridData(divRef,gridData){
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);
	gObj.enableMultiline(true);
	gObj.attachHeader('#rspan,#text_filter,#text_filter,#text_filter,#select_filter,#select_filter,#select_filter');
	gObj.init();	
	gObj.loadXMLString(gridData);	
	return gObj;	
}

//When edit icon is clicked, below function is called and this will assign the values to first row.
//And remove all other rows, Add rows Hyperlink also.
function fnEditSystem(setId, SetTypeId, BaseLineValue, SharedID){
	var rowCnt = document.all.hRowCnt.value;

	if(setId !=''){
		var validSetSuc = eval("document.all.validSetSuc0");	
		var validSetErr = eval("document.all.validSetErr0");
		if(validSetSuc.style.display == 'block'){
			validSetSuc.style.display = 'none';
		}
		if(validSetErr.style.display  == 'block'){
			validSetErr.style.display = 'none';
		}
	}
	var obj = eval("document.all.setId0"); 
	obj.value = setId;
	obj.disabled=true;

	obj = eval("document.all.setType0"); 
	if(SetTypeId != ''){
		obj.value = SetTypeId;
	}else{
		obj.value = 0;
	}

	obj = eval("document.all.setBaseLine0"); 
	if( BaseLineValue  != ''){
		if(BaseLineValue == '20100')
			obj.value = 'Y';
		else
			obj.value='N';
	}else{
		obj.value = 0;
	}

	obj = eval("document.all.setShared0"); 
	if(SharedID  != ''){
		if(obj)
			obj.value = SharedID;
	}else{
		if(obj)
			obj.value = 0;
	}
	var divElement='';
	divElement = document.getElementById('btn_void');
	if(divElement!='' && divElement!=null){
		divElement.disabled=false;
	}
	divElement = document.getElementById('addrow');
	divElement.style.display='none';

	var table = document.getElementById("SystemMapping");
	for ( var i = 3, row; row = table.rows[i]; i++) { 
		row.style.display='none';
	}
	document.all.hRowCnt.value ='1';	
	document.all.haction.value ='edit';
	//To clear all the values form the cart, while clicking on Edit icon, except the first row
	for(var i=1;i<=rowCnt; i++){
		fnRemoveItem(i);	
	}
}

//Fuction to reset the form
function fnResetMapping(form){
	var rowCnt = form.hRowCnt.value;
	for(var i=0;i<=rowCnt; i++){
		fnRemoveItem(i);	
	}
}

//When we click on Edit icon that time only we are displaying Void button.So during VOID, always take the first row set id from the screen.
function fnVoidMapping(form){
	var setId ='';
	if(eval("form.setId0"))	{
		setId = eval("form.setId0").value ;
	}
	var systemId= form.systemId.value;
	var txnId = systemId+','+setId;
	if(setId!=''){
		form.action = "/GmCommonCancelServlet?hTxnName=Unlink Set From System&hCancelType=VSYSMP&hTxnId="+txnId;
		form.submit();
	}
}
//Submit function. form the four input string based on SET TYPE selected.
//Input format for all the 4 type of stringssame and it will be like: "setid,baseline,shared|"   eg: 901.901,Y,20100|
function fnSubmitMapping(form){
	var rowCnt = form.hRowCnt.value;
	var mappedSetSize = form.setMapDtlssize.value;		
	var actionFrom = form.haction.value ;
	// setting the haction value as create by default. 
	if( actionFrom != 'edit'){
		form.haction.value ='create';
	}
	var setidVal ='';
	var setTypeVal ='';
	var setBaseLineVal ='';
	var setSharedVal ='';

	var allSetids='';
	var setidArr='';

	var strInputstdsetids ='';
	var strInputloanerstdsetids ='';
	var strInputaddlsetids ='';
	var strInputloaneraddlsetids ='';	

	var setTypeErr ='';
	var setBaseLinErr='';
	var setIDErr='';
	var statusErr='';
	for(var i=0;i<=rowCnt; i++){				
		var obj = eval("document.all.setId"+i);
		setidVal = TRIM(obj.value) ;
		obj = eval("document.all.setType"+i); 
		setTypeVal = obj.value ;
		obj = eval("document.all.setBaseLine"+i); 
		setBaseLineVal = obj.value ;
		if(setidVal!=''){	
			allSetids = allSetids+setidVal+'|';
			obj = eval("document.all.setShared"+i); 
			setSharedVal = (obj) ? obj.value:'0';

			if(setTypeVal==0){
				setTypeErr = setTypeErr + setidVal+', ';	
			}
			/*else{
				if(setTypeVal=='103148' || setTypeVal=='103147'	){
					if(setBaseLineVal==0){	
						setBaseLinErr = setBaseLinErr + setidVal+', ';	
					}	
				}
			}*/
			var status = eval("document.all.hStatus"+i);
			if(status.value !='20367' && actionFrom != 'edit'){
				statusErr = statusErr + setidVal+', ';	
			}
			if(setBaseLineVal==0){	
				setBaseLinErr = setBaseLinErr + setidVal+', ';	
			}
			var setSharedValue = (setSharedVal==0)?'':setSharedVal;
			if(setTypeVal=='103147'){		   // Standard
				strInputstdsetids = strInputstdsetids +setidVal +','+setBaseLineVal  +','+setSharedVal+'|';
			}else if(setTypeVal=='103148'){    //Loaner Standard
				strInputloanerstdsetids = strInputloanerstdsetids +setidVal  +','+setBaseLineVal  +','+setSharedVal+'|';
			}else if(setTypeVal=='103149'){    //Additional
				strInputaddlsetids = strInputaddlsetids +setidVal  +','+setBaseLineVal  +','+setSharedVal+'|';	
			}else if(setTypeVal=='103150'){    //Loaner Additional
				strInputloaneraddlsetids = strInputloaneraddlsetids +setidVal +','+setBaseLineVal  +','+setSharedVal+'|';
			}
		}else if ((setTypeVal != '' && setTypeVal != 0)||(setBaseLineVal != '' && setBaseLineVal != 0)){	
				setIDErr++;
		}
	}	

	var isErr=0;
	setidArr  = allSetids.split('|');
	if(!checkUniqueSets(setidArr)){
		Error_Details(message_prodmgmnt[109]);
		Error_Show();
		Error_Clear();
		return false;
	}
	if(statusErr!= ''){
		statusErr = statusErr.substring(0, statusErr.length-2);
		Error_Details(Error_Details_Trans(message_prodmgmnt[110],statusErr));
		Error_Show();
		Error_Clear();
		return false;
	}
	if(setIDErr > 0){
		Error_Details(message_prodmgmnt[111]);
		isErr++;
	}
	if(setTypeErr != ''){
		setTypeErr = setTypeErr.substring(0, setTypeErr.length-2);
		Error_Details(Error_Details_Trans(message_prodmgmnt[112],setTypeErr));
		isErr++;
	}
	if(setBaseLinErr!= ''){
		setBaseLinErr = setBaseLinErr.substring(0, setBaseLinErr.length-2);
		Error_Details(Error_Details_Trans(message_prodmgmnt[113],setBaseLinErr));
		isErr++;
	}
	if(	isErr == 0){
		var errFl=0;
		if(strInputstdsetids==''&&strInputloanerstdsetids==''&&strInputaddlsetids==''&&strInputloaneraddlsetids==''){
			Error_Details(message_prodmgmnt[114]);
			errFl++;
		}
		if(errFl==0){
			fnValidateTxtFld('txt_LogReason',message_prodmgmnt[146]);
		}
	}
	if(ErrorCount > 0){
			Error_Show();
			Error_Clear();
			return false;
	}else{		
		form.stdsetidsInputStr.value        = strInputstdsetids;
		form.loanerstdsetidsInputStr.value  = strInputloanerstdsetids;
		form.addlsetidsInputStr.value       = strInputaddlsetids;
		form.loaneraddlsetidsInputStr.value = strInputloaneraddlsetids;

		var systemID=form.systemId.value;
		var requestStr ='/gmSystemMapping.do?strOpt=save&stdsetidsInputStr='+strInputstdsetids+'&loanerstdsetidsInputStr='+strInputloanerstdsetids+'&addlsetidsInputStr='+strInputaddlsetids+'&loaneraddlsetidsInputStr='+strInputloaneraddlsetids+'&systemId='+systemID;
		form.action= requestStr;
		fnStartProgress('Y');
		form.submit();
	}
}

//This function is to validate the presence of any duplicate sets.
function checkUniqueSets(myArray){
  isUnique=true;
	for (var i = 0; i < myArray.length; i++){
		for (var j = 0; j < myArray.length; j++){
			if (i != j){
				if (myArray[i] == myArray[j]){
					isUnique=false;
				}
			}
		}
	}
	return isUnique;
}