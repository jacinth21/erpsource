function initGridData(divRef,gridData){
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);
	gObj.enableTooltips("true,true,true,true,true");
	gObj.attachHeader("#text_filter,#text_filter,#select_filter_strict,#select_filter,#text_filter,");
	gObj.init();	
	gObj.loadXMLString(gridData);	
	return gObj;	
}

// This Function will load grid data once page will load
function fnOnPageLoad() {
	if(parent.window.document.getElementById("ajaxdivcontentarea") != undefined){
  		parent.window.document.getElementById("ajaxdivcontentarea").style.height = "690";
  	}
	if(objGridData!='')	{
		mygrid = initGridData('UploadDetails',objGridData);		
	}
	if (objGridData.indexOf("cell", 0) != -1) {
		document.getElementById("UploadDetails").style.height = "auto";
	}else{
		document.getElementById("UploadDetails").style.height = "54"; //To support Edge giving header height in PC-3662-Edge Browser fixes for Quality Module
		document.getElementById("ShowDetails").style.color = "red";
		document.getElementById("ShowDetails").style.height = "15";
		document.getElementById("ShowDetails").innerHTML = message_prodmgmnt[7];
	}
}

// This function will call once click on file name hyper link.
function fnFileView(fileNm,fileid,filetype,fileGroupNm,fileRefid,artifactNm,subTypeNm){
	fileNm = fileNm.split('^').join(' ');
	filetype = filetype.split('^').join(' ');
	fileGroupNm = fileGroupNm.split('^').join(' ');
	   if (artifactNm == '103112'){ //Images 
		//for zip file ajax will call and unzip file in server 
		if(subTypeNm=='103202'){//360 view
			 fnPreviewZip(fileNm,fileid,fileGroupNm,fileRefid,artifactNm,subTypeNm);	
		}else{		
			windowOpener("/prodmgmnt/productcatalog/GmUploadView.jsp?fileName="+fileNm+"&fileId="+fileid+"&fileGrpNm="+fileGroupNm+"&artifactNm="+artifactNm+"&subTypeNm="+subTypeNm+"&fileRefId="+fileRefid,"PrntInv","resizable=yes,scrollbars=yes,width=560,height=500");
		}
	}
    else{
    	fnDownloadFile(fileid,fileGroupNm,fileRefid,artifactNm,subTypeNm);
    }
}

// fnDownloadFile() to download file
function fnDownloadFile(fileid,fileGroupNm,fileRefid,artifactNm,subTypeNm){
	document.forms[0].action = '/gmPDFileUpload.do?strOpt=Download&fileID='+fileid+'&fileGroupNM='+fileGroupNm+'&artifactNm='+artifactNm+'&subTypeNm='+subTypeNm+'&typeID='+fileRefid;
	document.forms[0].submit();	 
}
//fnPreviewZip to preview zip file
function fnPreviewZip(fileNm,fileid,fileGroupNm,fileRefid,artifactNm,subTypeNm){
	//ajax will call and unzip the file in server and return sample files names with comma seperator. 
	dhtmlxAjax.get('/gmPDFileUpload.do?strOpt=previewzip&fileID=' + fileid
			+ '&fileGroupNM=' + fileGroupNm + '&artifactNm=' + artifactNm
			+ '&subTypeNm=' + subTypeNm + '&typeID=' + fileRefid + '&ramdomId='
			+ Math.random(), function(loader) {
		var response = loader.xmlDoc.responseText;

		if (response == "") {
			Error_Details(message_prodmgmnt[13]);
			Error_Show();
			Error_Clear();
			return false;
		} else if (response != null && response.length > 0) {
			windowOpener(
					"/prodmgmnt/productcatalog/GmUploadView.jsp?viewType=previewzip&fileName="
							+ fileNm + "&fileId=" + fileid + "&fileGrpNm="
							+ fileGroupNm + "&artifactNm=" + artifactNm
							+ "&subTypeNm=" + subTypeNm + "&fileRefId="
							+ fileRefid + "&fileLists=" + response, "PrntInv",
					"resizable=yes,scrollbars=yes,width=580,height=560");
		}

	});
}