var golbal_metaid = '';
var golbal_langnm = '';
function initGridData(divRef,gridData){
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.enableTooltips("true,true,true,true,true,true,true");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableMultiline(true);
	gObj.init();	
	gObj.loadXMLString(gridData);	
	return gObj;	
}

function fnOnPageLoad(){
	var typeId = document.frmTranslateByType.typeId.value;
	var strOpt = document.frmTranslateByType.strOpt.value;

	//Disable the submit button, when language is selected as English  in PMT-37488 - System Publish for Marketing Collateral and Product
	/*if(strOpt == 'FetchTranslate' && SwitchUserFl != 'Y'){
		if(document.frmTranslateByType.langId){
			var langid = document.frmTranslateByType.langId.value;
			if(langid == '103097'){ //103097[English]
				if(typeId == '103092'){//103092[System]
					document.frmTranslateByType.Btn_Submit.disabled = false;
				}else{
					document.frmTranslateByType.Btn_Submit.disabled = true;
				}
			}else{
				document.frmTranslateByType.Btn_Submit.disabled = false;
			}
			if(voidaccess == 'Y' && langid != 0){
				if(langid == '103097'){
					if(typeId == '103092'){//103092[System]
						document.frmTranslateByType.Btn_Void.disabled = false;
					}else{
						document.frmTranslateByType.Btn_Void.disabled = true;
					}
				}else{
					document.frmTranslateByType.Btn_Void.disabled = false;
				}
			}else{
				document.frmTranslateByType.Btn_Void.disabled = true;
			}
		}
		if(langid == 0 || langid == ''){
			document.frmTranslateByType.metaid.value ='';
		}
	}*/
	
	if(document.frmTranslateByType.langId != undefined){
	   fnDisableSubmit(document.frmTranslateByType.langId);
	}
	var objDivStyle = "";	  
	if(typeId == '103090' || typeId == '103091' || typeId == '26240459' ){  //103030[Part] and 103091[Set]
		if (typeId == '103090'){
			objDivStyle = eval('document.all.part_typeid.style');
			objDivStyle.display = 'table-row';                        // added objDivStyle.display = 'table-row'; changes in in PC-3662-Edge Browser fixes for Quality Module
		}
		if (typeId == '103091'){
			objDivStyle = eval('document.all.set_typeid.style');
			objDivStyle.display = 'table-row';                     // added objDivStyle.display = 'table-row'; changes in in PC-3662-Edge Browser fixes for Quality Module
		}
		if (typeId == '26240459'){
			objDivStyle = eval('document.all.setbundle_id.style');
			objDivStyle.display = 'table-row';                  // added objDivStyle.display = 'table-row'; changes in in PC-3662-Edge Browser fixes for Quality Module
		}
	}	
	if(typeId == '103092' || typeId == '103093' || typeId == '103094'){  //103032[System] and 103093[Project] and 103094[Group]
		if (typeId == '103092'){
			objDivStyle = eval('document.all.sys_typelist.style');
			objDivStyle.display = 'table-row';               // added objDivStyle.display = 'table-row'; changes in in PC-3662-Edge Browser fixes for Quality Module
		}
		if (typeId == '103093'){
			objDivStyle = eval('document.all.prj_typelist.style');
			objDivStyle.display = 'table-row';                  // added objDivStyle.display = 'table-row'; changes in in PC-3662-Edge Browser fixes for Quality Module
		}
		if (typeId == '103094'){
			objDivStyle = eval('document.all.grp_typelist.style');
			objDivStyle.display = 'table-row';              // added objDivStyle.display = 'table-row'; changes in in PC-3662-Edge Browser fixes for Quality Module
		}
	}
	if(strOpt == 'FetchTranslate'){
		if (objGridData != ''){
			gridObj = initGridData('TranslateDetails',objGridData);
		}
		if (objGridData.indexOf("cell", 0) != -1) {
			document.getElementById("TranslateDetails").style.height = "200";
		}else{
			document.getElementById("TranslateDetails").style.height = "10";
			document.getElementById("ShowDetails").style.color = "red";
			document.getElementById("ShowDetails").style.height = "15";
			document.getElementById("ShowDetails").innerHTML = "Nothing found to display.";
		}
	}
}

function fnSetTypeList(){
	var typeId = document.frmTranslateByType.typeId.value;
	var strOpt = document.frmTranslateByType.strOpt.value;
	if(typeId == '103090'){  // Part
		document.frmTranslateByType.typesetid.value = '';
	}else if(typeId == '103091'){  // Set
		document.frmTranslateByType.typepartid.value = '';
	}
	if(strOpt == 'FetchTranslate'){
		document.frmTranslateByType.typesetid.value = "";
		document.frmTranslateByType.typepartid.value = "";
		document.frmTranslateByType.typesystemid.value = "";
		document.frmTranslateByType.typeprojectid.value = "";
		document.frmTranslateByType.typegroupid.value = "";
	}
	if(typeId == '103092' || typeId == '103093' || typeId == '103094' || typeId == '26240459'){  //103032[System] and 103093[Project] and 103094[Group] and 26240459[Set Bundle]
		if (typeId == '103092'){
			document.frmTranslateByType.haction.value = 'OnReloadSystem';
		}
		if (typeId == '103093'){
			document.frmTranslateByType.haction.value = 'OnReloadProject';
		}
		if (typeId == '103094'){
			document.frmTranslateByType.haction.value = 'OnReloadGroup';
		}	
		if (typeId == '26240459'){
			document.frmTranslateByType.haction.value = 'OnReloadSetBundle';
		}
	}
	document.frmTranslateByType.strOpt.value = 'Load';
	document.frmTranslateByType.submit();
}

function fnTranslate(obj){
	var typeid = document.frmTranslateByType.typeId.value;
	if(typeid == '103090'){   //103090[Part]
		var partid = document.frmTranslateByType.typepartid.value;
		var strPartArray = partid.split(',');		
		if(partid == '')
			fnValidateTextField(frmTranslateByType, 'typepartid', message_prodmgmnt[338]);
		if(strPartArray.length > 1){
			Error_Details(message_prodmgmnt[310]);			
		}
		document.frmTranslateByType.typepartid.value = partid;		
		document.frmTranslateByType.haction.value = 'OnReloadPart';
		document.frmTranslateByType.strOpt.value = 'Load';
		
	}
	if(typeid == '103091'){   //103091[Set]
		var setid = document.frmTranslateByType.typesetid.value;
		var strSetArray = setid.split(',');	
		if(setid == '')
			fnValidateTextField(frmTranslateByType, 'typesetid', message_prodmgmnt[339]);		
		if(strSetArray.length > 1){
			Error_Details(message_prodmgmnt[12]);			
		}		
		document.frmTranslateByType.typesetid.value = setid;
		document.frmTranslateByType.haction.value = 'OnReloadSet';
		document.frmTranslateByType.strOpt.value = 'Load';
	}
	if(typeid == '103092' || typeid == '103093' || typeid == '103094' || typeid == '26240459' ){  //103032[System] and 103093[Project] and 103094[Group] and 26240459[Set Bundle]
		if (typeid == '103092'){
			document.frmTranslateByType.typesystemid.value = obj.value;
		}
		if (typeid == '103093'){
			document.frmTranslateByType.typeprojectid.value = obj.value;
		}
		if (typeid == '103094'){
			document.frmTranslateByType.typegroupid.value = obj.value;
		}
		if (typeid == '26240459'){
			document.frmTranslateByType.setbundleId.value = obj.value;
		}
		document.frmTranslateByType.strOpt.value = 'FetchTranslate';
	}
	if (ErrorCount > 0) {
		Error_Show();
        Error_Clear();
        return false;
    }
	fnReset();
	document.frmTranslateByType.sysprogrpid.value = "";
	fnStartProgress('Y');
	document.frmTranslateByType.submit();	
}

function fnSubmitLanguage(){
	var typeid = document.frmTranslateByType.typeId.value;
	if(typeid == '103090'){   //103090[Part]
		var partid = document.frmTranslateByType.typepartid.value;
		var strPartArray = partid.split(',');		
		if(partid == '')
			fnValidateTextField(frmTranslateByType, 'typepartid', message_prodmgmnt[338]);
		if(strPartArray.length > 1){
			Error_Details(message_prodmgmnt[310]);			
		}
	}
	if(typeid == '103091'){   //103091[Set]
		var setid = document.frmTranslateByType.typesetid.value;
		var strSetArray = setid.split(',');	
		if(setid == '')
			fnValidateTextField(frmTranslateByType, 'typesetid', message_prodmgmnt[339]);		
		if(strSetArray.length > 1){
			Error_Details(message_prodmgmnt[12]);			
		}
	}	
	if(typeid == '103092' || typeid == '103093' || typeid == '103094'){  //103032[System] and 103093[Project] and 103094[Group]
		if (typeid == '103092'){
			fnValidateDropDn('typesystemid',lblProjectList);
		}
		if (typeid == '103093'){
			fnValidateDropDn('typeprojectid',lblSystemList);
		}
		if (typeid == '103094'){
			fnValidateDropDn('typegroupid',lblGroupList);
		}
	}
	fnValidateDropDown(frmTranslateByType, 'langId', lblLangauge);
	fnValidateTextField(frmTranslateByType, 'langNm', message_prodmgmnt[313]);
	var desclength = document.frmTranslateByType.description.value;
	if(desclength.length > '4000'){
		Error_Details(message_prodmgmnt[311]);
	}	
	if (ErrorCount > 0) {
		Error_Show();
        Error_Clear();
        return false;
    }	
	var typelist = document.frmTranslateByType.sysprogrpid.value;
	document.frmTranslateByType.sysprogrpid.value = typelist;
	document.frmTranslateByType.strOpt.value = 'Save';
	fnStartProgress('Y');
	document.frmTranslateByType.submit();
	
}

function fnValidateTextField(form, val, name) {
	var obj = eval("document." + form.name + "." + val);
	var objVal = TRIM(obj.value);
	if(obj.disabled != true && objVal == '') {
		Error_Details(Error_Details_Trans(message_prodmgmnt[300],name));
	}
}

function fnValidateDropDown(form, val, name) {
	var obj = eval("document." + form.name + "." + val);
	var objVal = TRIM(obj.value);
	if(obj.disabled != true && objVal == '0') {
		Error_Details(Error_Details_Trans(message_prodmgmnt[301],name));
	}
}
function fnSetLangDetails(refid,reftype,langid){
	
	if(reftype == '103090'){   //103090[Part]
		document.frmTranslateByType.typepartid.value=refid;	
	}
	if(reftype == '103091'){   //103091[Set]
		document.frmTranslateByType.typesetid.value=refid;
	}	
	if(reftype == '103092'){
		document.frmTranslateByType.typesystemid.value=refid;
	}
	if(reftype == '103093' ){
		document.frmTranslateByType.typeprojectid.value=refid;
	}
	if(reftype == '103094'){
		document.frmTranslateByType.typegroupid.value=refid;
	}
	
	
	document.frmTranslateByType.typeId.value = reftype;  
	document.frmTranslateByType.langId.value = langid; 
	document.frmTranslateByType.strOpt.value = 'FetchTranslateEdit';
	document.frmTranslateByType.sysprogrpid.value = "";
	fnStartProgress('Y');
	document.frmTranslateByType.submit();
}
function fnDisableSubmit(obj){
	var typeid = '';
	if(document.frmTranslateByType.typeId){
		typeid  = document.frmTranslateByType.typeId.value;
	}
	
	if(document.frmTranslateByType.Btn_Submit != undefined){   //  PMT-37488 - System Publish for Marketing Collateral and Product
		document.frmTranslateByType.Btn_Submit.disabled = true;
	}
	if(document.frmTranslateByType.Btn_Void != undefined){     
		document.frmTranslateByType.Btn_Void.disabled = true;
	}
	
	//Disable the submit button, when language is selected as English  in PMT-37488 - System Publish for Marketing Collateral and Product
	if(SwitchUserFl != 'Y'){       
		if(obj.value != '103097'){  //103097[English]
			/*if(typeid == '103092'){  //103092[System]
				document.frmTranslateByType.Btn_Submit.disabled = false;
			}else{
				document.frmTranslateByType.Btn_Submit.disabled = true;
			}*/
			document.frmTranslateByType.Btn_Submit.disabled = false;      
		}
		/*else{
			document.frmTranslateByType.Btn_Submit.disabled = false;
		}*/
	}
    var metaid = document.frmTranslateByType.metaid.value;
	if(metaid != '' && SwitchUserFl != 'Y'){
		if(obj.value == '103097'){  
			if(typeid == '103092'){  
				document.frmTranslateByType.Btn_Void.disabled = false;
			}else{
				document.frmTranslateByType.Btn_Void.disabled = true;
			}
		}else{
			document.frmTranslateByType.Btn_Void.disabled = false;
		}
	}
	
}
function fnReset(){	
	if(document.frmTranslateByType.langId){
		document.frmTranslateByType.langId.value = '0'; 
	}
	if(document.frmTranslateByType.langNm){
		document.frmTranslateByType.langNm.value = ''; 
	}
	if(document.frmTranslateByType.description){
		document.frmTranslateByType.description.value = ''; 
	}
	if(document.frmTranslateByType.dtl_description){
		document.frmTranslateByType.dtl_description.value = ''; 
	}
	//tinyMCE.setContent("");
	if(tinymce.get('desc')){
		tinymce.get('desc').setContent('');
	}
	if(tinymce.get('dtlDesc')){
		tinymce.get('dtlDesc').setContent('');
	}
}

function fnVoidLanguage(){
	fnValidateDropDown(frmTranslateByType, 'langId', lblLangauge);
	fnValidateTextField(frmTranslateByType, 'langNm', message_prodmgmnt[313]);
	golbal_metaid = document.frmTranslateByType.metaid.value;
	golbal_langnm = document.frmTranslateByType.metaname.value;
	if(golbal_metaid == ''){
		Error_Details(message_prodmgmnt[312]);
	}
	if (ErrorCount > 0) {
		Error_Show();
        Error_Clear();
        return false;
    }
	golbal_metaid = golbal_metaid + "|"; 
	document.frmTranslateByType.action="/GmCommonCancelServlet?hTxnName="+golbal_langnm+"&hCancelType=VLANG&hTxnId="+golbal_metaid+"&hAction=Load";
	document.frmTranslateByType.submit();	
}
