
function initGridData(divRef,gridData){
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.init();	
	gObj.loadXMLString(gridData);	
	return gObj;	
}
function fnOnPageLoad(){
	if (objGroupData != ''){
		gridGrpObj = initGridData('GroupDetails',objGroupData);
	}
	if (objSetData != ''){
		gridSetObj = initGridData('SetDetails',objSetData);
	}
	
	if (objGroupData.indexOf("cell", 0) != -1) {
		document.getElementById("GroupDetails").style.height = "150";
	}else{
		document.getElementById("GroupDetails").style.height = "10";
		document.getElementById("ShowGroupDetails").style.color = "red";
		document.getElementById("ShowGroupDetails").style.height = "15";
		document.getElementById("ShowGroupDetails").innerHTML = message_prodmgmnt[7];
	}
	
	if (objSetData.indexOf("cell", 0) != -1) {
		document.getElementById("SetDetails").style.height = "150";
	}else{
		document.getElementById("SetDetails").style.height = "10";
		document.getElementById("ShowSetDetails").style.color = "red";
		document.getElementById("ShowSetDetails").style.height = "15";
		document.getElementById("ShowSetDetails").innerHTML = message_prodmgmnt[7];
	}	
}