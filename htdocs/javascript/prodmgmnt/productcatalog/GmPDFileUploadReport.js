function fnLoad(){
	fnValidateDropDn('uploadTypeId', message_prodmgmnt[350]);

	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	var uploadTypeId = document.frmPDFileUploadReport.uploadTypeId.value;
	var sysListId = document.all.sysListId.value;
	var fileTypeId = document.frmPDFileUploadReport.fileTypeId.value;
	var typeListId = document.frmPDFileUploadReport.typeListId.value;
	var fileTitle = document.frmPDFileUploadReport.fileTitle.value;
	var fileName = document.frmPDFileUploadReport.fileName.value;
	var partNumId = document.frmPDFileUploadReport.partNumId.value;
	var strPartSearch = document.frmPDFileUploadReport.strPartLikes.value;
	fnStartProgress('Y');
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmPDFileUploadReport.do?method=fetchUploadedFilesReport&uploadTypeId='+uploadTypeId+'&sysListId='+sysListId+
				'&fileTypeId='+fileTypeId+'&typeListId='+typeListId+'&fileTitle='+fileTitle+'&fileName='+fileName+'&partNumId='+partNumId+
				'&strPartLikes='+strPartSearch+'&ramdomId='+new Date().getTime());
		dhtmlxAjax.get(ajaxUrl,fnPopulateFileReport);	
}
//This function is used to populate uploaded files details on the screen
function fnPopulateFileReport(loader) {
	fnStartProgress();
	response = loader.xmlDoc.responseText;
	fnStopProgress();
	if(response != ''){
		setColIds = "uploadtype,systemid,partnum,fileid,filename,filetitle,artifact,subtype,filesize,updatedby,updateddate,refid,artifactid,subtypeid";
		var setColTypes = "ro,ro,ro,ro,fileNm,ro,ro,ro,ro,ro,ro,ro,ro,ro";
		var setColAlign = "left,left,left,left,left,left,left,left,right,left,left,left,left,left";
		var setColSorting = "str,str,str,str,str,str,str,str,str,str,str,str,str,str";
		var enableTooltips = "true,true,true,true,true,true,true,true,true,true,true,true,true,true";
		var setHeader = [ "Upload Type","System","Part Number","File Id","File Name","Title","Artifact Type","Sub Type","Size","Updated By","Updated Date","","","" ];
		var setFilter = [ "#select_filter", "#select_filter", "#text_filter","","#text_filter", "#text_filter", "#select_filter", "#text_filter",
		                  "#text_filter","#select_filter","#text_filter","","",""];
		var setInitWidths = "100,150,150,10,250,100,100,100,100,100,*";
		var gridHeight = "";
		var dataHost = {};	
		format = "Y"
		var footerArry=new Array();	
		var footerStyles = [];
		var footerExportFL = true;
		
		document.getElementById("dataGridDiv").style.display = 'block';
		document.getElementById("DivNothingMessage").style.display = 'none';
		document.getElementById("DivExportExcel").style.display = 'block';
		
		// split the functions to avoid multiple parameters passing in single function
		gridObj = initGridObject('dataGridDiv');
		gridObj = fnCustomTypes(gridObj, setHeader, setColIds, setInitWidths,setColAlign, setColTypes, setColSorting, enableTooltips,
								                 setFilter, 400, footerArry,footerStyles);
		if (format != ""){
			var formatDecimal = "0,000.00";
			gridObj.setNumberFormat(formatDecimal,8,".",",");
		}
		gridObj = loadDhtmlxGridByJsonData(gridObj, response);
		gridObj.attachEvent("onRowSelect",function(rowId,cellIndex){
			if(cellIndex == 4){
				var fileNm = gridObj.cellById(rowId, 4).getValue();
				var fileid = gridObj.cellById(rowId, 3).getValue();
				var filetype = gridObj.cellById(rowId, 6).getValue();
				var fileGroupNm = gridObj.cellById(rowId, 0).getValue();
				var fileRefid = gridObj.cellById(rowId, 11).getValue();
				var artifactNm = gridObj.cellById(rowId, 12).getValue();
				var subTypeNm = gridObj.cellById(rowId, 13).getValue();
				fnFileView(fileNm,fileid,filetype,fileGroupNm,fileRefid,artifactNm,subTypeNm);
			}  
		});
		gridObj.setColumnHidden(3, true);
		gridObj.setColumnHidden(11, true);
		gridObj.setColumnHidden(12, true);		
		gridObj.setColumnHidden(13, true);
	}else {
		document.getElementById("dataGridDiv").style.display = 'none';
		document.getElementById("DivNothingMessage").style.display = '';
		document.getElementById("DivExportExcel").style.display = 'none';		
	}
}
//This Function Used to set custom types conditions in Grid
function fnCustomTypes(gObj, setHeader, setColIds, setInitWidths, setColAlign,
			setColTypes, setColSorting, enableTooltips, setFilter, gridHeight,footerArry,footerStyles) {
	gObj.setHeader(setHeader);
	var colCount = setHeader.length;
	gObj.setInitWidths(setInitWidths);
	gObj.setColAlign(setColAlign);
	gObj.setColTypes(setColTypes);
	gObj.setColSorting(setColSorting);
	gObj.enableTooltips(enableTooltips);
	gObj.attachHeader(setFilter);
	gObj.setColumnIds(setColIds);
	if (footerArry != undefined && footerArry.length > 0) {
		var footstr = eval(footerArry).join(",");
			if (footerStyles != undefined && footerStyles.length > 0)
				gObj.attachFooter(footstr, footerStyles);
			else
				gObj.attachFooter(footstr);
	}  
		gObj.enableAutoHeight(true, gridHeight, true);	
		return gObj;
}

//onchange upload type dropdown
function fnSetTypeList()
{
	var uploadtypeid = document.all.uploadTypeId.value;
	if(uploadtypeid == '103090' || uploadtypeid == '103091'){ 
		if(uploadtypeid == '103090' || uploadtypeid == '103091'){
			document.all.sysListId.value = '';
		}		
		document.all.haction.value = '';
	}
	if(uploadtypeid == '103092' || uploadtypeid == '103093' || uploadtypeid == '103094' || uploadtypeid == '26240459'){
		
		if (uploadtypeid == '103092'){	// 103092-System
			document.all.haction.value = 'OnReloadSystemList';
		}
		
		if (uploadtypeid == '103094'){	// 103094-Group
			document.all.haction.value = 'OnReloadGroupList';
		}	
	}
	document.forms[0].action = '/gmPDFileUploadReport.do?method=loadUploadedFilesReport';
	document.forms[0].submit();
}

function fnOnPageLoad(){
	document.getElementById("dataGridDiv").style.display = 'none';
	document.getElementById("DivExportExcel").style.display = 'none';
	document.getElementById("DivNothingMessage").style.display = 'none';
}
function fnDownloadXLS() {
	gridObj.setColumnHidden(3, true);
	gridObj.setColumnHidden(11, true);
	gridObj.setColumnHidden(12, true);		
	gridObj.setColumnHidden(13, true);
	gridObj.toExcel('/phpapp/excel/generate.php');
	
}

//This function will call once click on file name hyper link.
function fnFileView(fileNm,fileid,filetype,fileGroupNm,fileRefid,artifactNm,subTypeNm){
	fileNm = fileNm.split('^').join(' ');
	filetype = filetype.split('^').join(' ');
	fileGroupNm = fileGroupNm.split('^').join(' ');
    if (artifactNm == '103112'){ //Images 
		//for zip file ajax will call and unzip file in server 
		if(subTypeNm=='103202'){//360 view
			 fnPreviewZip(fileNm,fileid,fileGroupNm,fileRefid,artifactNm,subTypeNm);	
		}else{
			windowOpener("/prodmgmnt/productcatalog/GmUploadView.jsp?fileName="+fileNm+"&fileId="+fileid+"&fileGrpNm="+fileGroupNm+"&artifactNm="+artifactNm+"&subTypeNm="+subTypeNm+"&fileRefId="+fileRefid,"PrntInv","resizable=yes,scrollbars=yes,width=560,height=500");
		}
    }
    else{
    	fnDownloadFile(fileid,fileGroupNm,fileRefid,artifactNm,subTypeNm);
    }
}
// fnDownloadFile() to download file
function fnDownloadFile(fileid,fileGroupNm,fileRefid,artifactNm,subTypeNm){
	document.frmPDFileUploadReport.haction.value = '';
	document.frmPDFileUploadReport.action = 'gmPDFileUploadReport.do?method=loadUploadedFilesReport&haction=Download&fileID='+fileid+'&fileGroupNM='+fileGroupNm+'&artifactNm='+artifactNm+'&subTypeNm='+subTypeNm+'&typeID='+fileRefid;
	document.frmPDFileUploadReport.submit();	 
}

//fnPreviewZip to preview zip file
function fnPreviewZip(fileNm,fileid,fileGroupNm,fileRefid,artifactNm,subTypeNm){
	//ajax will call and unzip the file in server and return sample files names with comma seperator. 
	dhtmlxAjax.get('/gmPDFileUpload.do?strOpt=previewzip&fileID=' + fileid
			+ '&fileGroupNM=' + fileGroupNm + '&artifactNm=' + artifactNm
			+ '&subTypeNm=' + subTypeNm + '&typeID=' + fileRefid + '&ramdomId='
			+ Math.random(), function(loader) {
		var response = loader.xmlDoc.responseText;

		if (response == "") {
			Error_Details(message_prodmgmnt[20]);
			Error_Show();
			Error_Clear();
			return false;
		} else if (response != null && response.length > 0) {
			windowOpener(
					"/prodmgmnt/productcatalog/GmUploadView.jsp?viewType=previewzip&fileName="
							+ fileNm + "&fileId=" + fileid + "&fileGrpNm="
							+ fileGroupNm + "&artifactNm=" + artifactNm
							+ "&subTypeNm=" + subTypeNm + "&fileRefId="
							+ fileRefid + "&fileLists=" + response, "PrntInv",
					"resizable=yes,scrollbars=yes,width=580,height=560");
		}

	});
}
//This function used to create user defined column types in Grid
//This Function for hyperlink ttp name
function eXcell_fileNm(cell) {
	
	// the eXcell name is defined here
	if (cell) { // the default pattern, just copy it
		this.cell = cell;
		this.grid = this.cell.parentNode.grid;
	}
	
	this.setValue = function(val) {
		this.setCValue("<a href=\"#\">"+val+"",val);		
	}

	this.getValue = function() {
		return this.cell.childNodes[0].innerHTML; // gets the value
	}
}
eXcell_fileNm.prototype = new eXcell;
