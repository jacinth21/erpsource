// This Function will load grid data once page will load

function initGridData(divRef,gridData){
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);
	gObj.enableMultiline(true);
	gObj.enableTooltips("true,true,true,true");
	gObj.attachHeader("#text_filter,,,#select_filter");
	gObj.init();	
	gObj.loadXMLString(gridData);	
	return gObj;	
}

function fnLoadLanguageGrid(){
	if(parent.window.document.getElementById("ajaxdivcontentarea") != undefined){
  		parent.window.document.getElementById("ajaxdivcontentarea").style.height = "690";
  	}
	if(objGridData!=''){
		mygrid = initGridData('LanguageDataGrid',objGridData);	
	}
	if (objGridData.indexOf("cell", 0) != -1) {
		document.getElementById("LanguageDataGrid").style.height = "auto";
	}else{
		document.getElementById("LanguageDataGrid").style.height = "54"; //To support Edge giving header height 
		document.getElementById("ShowDetails").style.color = "red";
		document.getElementById("ShowDetails").style.height = "15";
		document.getElementById("ShowDetails").innerHTML = message_prodmgmnt[7];
	}
}