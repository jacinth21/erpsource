var gridObj;
function initGridFile(divRef,gridData){
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);
	gObj.init();	
	gObj.loadXMLString(gridData);	
	return gObj;	
}

function fnOnPageLoad()
{
	var stropt =  document.all.strOpt.value;
	var refid = document.all.refId.value;
	var uploadtypeid =  document.all.uploadTypeId.value;//obj.value;
	var objPartDivStyle = eval('document.all.part_typeid.style');
	var objSetDivStyle = eval('document.all.set_typeid.style');
	var objSysDivStyle = eval('document.all.sys_typelist.style');
	var objPrjDivStyle = eval('document.all.prj_typelist.style');
	var objGrpDivStyle = eval('document.all.grp_typelist.style');
	var objSetBundleDivStyle = eval('document.all.set_bundleid.style');
	
	
	if (stropt == 'FetchVoidUploadFiles')
	{
		if(uploadtypeid == '103090' || uploadtypeid == '103091' || uploadtypeid == '26240459' ){ // 103090-Part Number, 26240459-SetBundle
			if (uploadtypeid == '103090'){
				document.all.partListId.value = refid;
			}
			if (uploadtypeid == '103091'){
				document.all.setListId.value = refid;
			}
			if (uploadtypeid == '26240459'){
				document.all.sbListId.value = refid;
			}
		}
		if(uploadtypeid == '103092' || uploadtypeid == '103093' || uploadtypeid == '103094'){ // 103092-System, 103093-Project, 103094-Group
			if (uploadtypeid == '103092'){
				document.all.sysListId.value = refid;
			}
			if (uploadtypeid == '103093'){
				document.all.prjListId.value = refid;
			}
			if (uploadtypeid == '103094'){
				document.all.grpListId.value = refid;
			}
		}
		fnLoad();
	}
	
	if(uploadtypeid == '103090' || uploadtypeid == '103091' || uploadtypeid == '26240459'){ // 103090-Part Number, 103091-Set ,26240459-SetBundle
		if (uploadtypeid == '103090'){
			objPartDivStyle.display = 'table-row';
		}
		if (uploadtypeid == '103091'){
			objSetDivStyle.display = 'table-row';
		}
		if (uploadtypeid == '26240459'){
			objSetBundleDivStyle.display = 'table-row';
		}
	}
	if(uploadtypeid == '103092' || uploadtypeid == '103093' || uploadtypeid == '103094'){ // 103092-System, 103093-Project, 103094-Group
		if (uploadtypeid == '103092'){
			objSysDivStyle.display = 'table-row';
		}
		if (uploadtypeid == '103093'){
			objPrjDivStyle.display = 'table-row';
		}
		if (uploadtypeid == '103094'){
			objGrpDivStyle.display = 'table-row';
		}
	}
	
	if (objGridData != '') {
		gridObj = initGridFile('uploadedFilesGrid',objGridData);
		gridObj.attachHeader(",<input type='checkbox' value='no' name='selectAllFiles' onClick='javascript:fnChangedFilter(this);'/>,"+',#text_filter,#text_filter,,#select_filter_strict,#select_filter');
		
		filechk_rowId = gridObj.getColIndexById("filechk");
		filechk_hide_rowId = gridObj.getColIndexById("filechk_hidden");
		filenm_rowId = gridObj.getColIndexById("filename");
		title_rowId = gridObj.getColIndexById("title");
		title_hide_rowId = gridObj.getColIndexById("title_hidden");
		type_rowId = gridObj.getColIndexById("type");
		reftype_rowId = gridObj.getColIndexById("type_id");
		size_rowId = gridObj.getColIndexById("size");
		orderby_rowId = gridObj.getColIndexById("orderby");
		orderby_hide_rowId = gridObj.getColIndexById("OrdBy_hidden");
		updby_rowId = gridObj.getColIndexById("uploadby");
		upddate_rowId = gridObj.getColIndexById("uploaddt");
		fileName_hide_rowId = gridObj.getColIndexById("fileName_hidden");
		titleRefId_rowId = gridObj.getColIndexById("titleRefId_hidden");
		titleType_rowId = gridObj.getColIndexById("titleType_hidden");
		subType_rowId = gridObj.getColIndexById("subType_hidden");		
		partnum_rowId = gridObj.getColIndexById("partnum");

		gridObj.attachEvent("onEditCell", doOnCellEdit);
		gridObj.attachEvent("onCheckbox", doOnCheck);
		gridObj.forEachRow(function(rowId) {
			title_nm = gridObj.cellById(rowId, title_rowId).getValue();		
		});
		
	}
}

// doOnCellEdit() will call when there is some change into a grid cell
function doOnCellEdit(stage, rowId, cellInd, nValue, oValue)
{
	var counter = 0;
	
	if(stage == '2'){
		if(nValue!= ''){
			if(!isNormalInteger(nValue)){
				Error_Details(message_prodmgmnt[8]);
				Error_Show();
				Error_Clear();
				return false;
			}
			//The orderby validation is moved to fnValidateOrderBy()
			/*gridObj.forEachRow(function(id){
				val=gridObj.cellById(id, orderby_rowId).getValue()
				if(val==nValue)
				{
				    counter++;
				}
			});
			if (counter>1){
				Error_Details(message_prodmgmnt[30]);
				Error_Show();
				Error_Clear();
				return false;
			}*/
		}
		desableRow (rowId);
	}
	return true;
}

//Description : This function to validate duplicate order by value
function fnValidateOrderBy(){
	var all_row = gridObj.getAllRowIds(",");
	var rowsarr = all_row.split(",");
	if(rowsarr.length>0){
		for ( var rowid = 0; rowid < rowsarr.length; rowid++)
		{
			curr_rowid = rowsarr[rowid];
			orderby = gridObj.cellById(curr_rowid, orderby_rowId).getValue();
			
			var all_row_loop = gridObj.getAllRowIds(",");
			var rowslooparr = all_row_loop.split(",");
			var blDuplicate = false;
			if(orderby != ""){
				for ( var looprowid = 0; looprowid < rowslooparr.length; looprowid++)
				{
					loop_rowid = rowslooparr[looprowid];
					looporderby = gridObj.cellById(loop_rowid, orderby_rowId).getValue();
					if(curr_rowid !=loop_rowid && orderby==looporderby){
						blDuplicate = true;
						break;
					}
				}
			}
			if(blDuplicate){
				
				filename = gridObj.cellById(loop_rowid, fileName_hide_rowId).getValue();
				Error_Details(Error_Details_Trans(message[9],filename));
				return;
			}
		}
	}
	
}
function isNormalInteger(str) {
    var n = ~~Number(str);
    return String(n) === str && n > 0;
}
var disvableCount = 0;
function desableRow (rowId)
{
	old_title_hidden_val = gridObj.cellById(rowId, title_hide_rowId).getValue();
	old_ordby_hidden_val = gridObj.cellById(rowId, orderby_hide_rowId).getValue();
	
	new_title_hidden_val = gridObj.cellById(rowId, title_rowId).getValue();
	new_ordby_hidden_val = gridObj.cellById(rowId, orderby_rowId).getValue();
	
	if((old_title_hidden_val != new_title_hidden_val) || (old_ordby_hidden_val != new_ordby_hidden_val)){
		
		gridObj.cellById(rowId, filechk_rowId).setDisabled(true);
		gridObj.cellById(rowId, filechk_hide_rowId).setValue("checkbox_disable");
		disvableCount++;

	}else if ((old_title_hidden_val == new_title_hidden_val )|| (old_ordby_hidden_val == new_ordby_hidden_val)){

		gridObj.cellById(rowId, filechk_rowId).setDisabled(false);
		gridObj.cellById(rowId, filechk_hide_rowId).setValue("");
		disvableCount--;
	}
}

//Description : This function to check the select all (master check box)
function doOnCheck(rowId, cellInd, stage)
{
	if(cellInd == filechk_rowId) {
		if(stage){
			
			gridObj.cellById(rowId, title_rowId).setDisabled(true);
			gridObj.cellById(rowId, orderby_rowId).setDisabled(true);
		}
		else{
			gridObj.cellById(rowId, title_rowId).setDisabled(false);
			gridObj.cellById(rowId, orderby_rowId).setDisabled(false);
		}
	}
	return true;
}

//When select master check box, it will be select all check box.
function fnChangedFilter(obj){
	if(obj.checked){
		if (disvableCount>0){
			Error_Details(message_prodmgmnt[10]);
		}
		if (ErrorCount > 0) {
			Error_Show();
			Error_Clear();
			obj.checked = false;
			return false;
		}
		gridObj.forEachRow(function(rowId) {
				check_id = gridObj.cellById(rowId, 1).getValue();
				gridObj.cellById(rowId, 1).setChecked(true);
				gridObj.cellById(rowId, title_rowId).setDisabled(true);
				gridObj.cellById(rowId, orderby_rowId).setDisabled(true);		
	   });
	}else{
		gridObj.forEachRow(function(rowId) {
			check_id = gridObj.cellById(rowId, 1).getValue();
			if(check_id == 1){
				gridObj.cellById(rowId, 1).setChecked(false);
				gridObj.cellById(rowId, title_rowId).setDisabled(false);
				gridObj.cellById(rowId, orderby_rowId).setDisabled(false);	
			}
	   });
	}
}

/* fnSetTypeList() will call when selecting Upload Type dropdown
 * to set the second dropdown / textbox
 */
function fnSetTypeList()
{
	var uploadtypeid = document.all.uploadTypeId.value;
	// 103090-Part Number, 103091-Set
	if(uploadtypeid == '103090' || uploadtypeid == '103091'){ 
		if(uploadtypeid == '103090'){
			document.all.partListId.value = '';
		}
		if(uploadtypeid == '103091'){
			document.all.setListId.value = '';
		}
		document.all.haction.value = '';
	}
	
	// 103092-System, 103093-Project, 103094-Group, 26240459-Set Bundle
	if(uploadtypeid == '103092' || uploadtypeid == '103093' || uploadtypeid == '103094' || uploadtypeid == '26240459'){
		
		if (uploadtypeid == '103092'){	// 103092-System
			document.all.haction.value = 'OnReloadSystemList';
			document.all.strSetGroupType.value = '1600,26241209';
		}
		if (uploadtypeid == '103093'){	// 103093-Project
			document.all.haction.value = 'OnReloadProjectList';
		}
		if (uploadtypeid == '103094'){	// 103094-Group
			document.all.haction.value = 'OnReloadGroupList';
		}
		if (uploadtypeid == '26240459'){	// 26240459-Set Bundle
			document.all.haction.value = 'OnReloadSetBundle';
		}
	}
	
	document.all.strOpt.value = 'Load';
	document.forms[0].action = '/gmPDFileUpload.do';
	document.forms[0].submit();
}

// fnLoad() will call when you select second dropdown / hit the load button
function fnLoad()
{
	var uploadtypeid = document.all.uploadTypeId.value;
	
	if(uploadtypeid == '103090' || uploadtypeid == '103091'){
		
		if(uploadtypeid == '103090'){
			var partid = document.all.partListId.value;
			var strPartArray = partid.split(',');	
			if(partid == '')
				fnValidateTextField('partListId', lblPart);		
			if(strPartArray.length > 1){
				Error_Details(message_prodmgmnt[11]);			
			}
			document.all.partListId.value = partid;
			document.all.haction.value = 'OnReloadPart';
			document.all.strOpt.value = 'Load';
		}
		if(uploadtypeid == '103091'){
			var setid = document.all.setListId.value;
			var strSetArray = setid.split(',');	
			if(setid == '')
				fnValidateTextField('setListId', lblSetID);		
			if(strSetArray.length > 1){
				Error_Details(message_prodmgmnt[12]);			
			}
			document.all.setListId.value = setid;
			document.all.haction.value = 'OnReloadSet';
			document.all.strOpt.value = 'Load';
		}

		if (ErrorCount > 0) {
			Error_Show();
	        Error_Clear();
	        return false;
	    }
	}
	
	if(uploadtypeid == '103092' || uploadtypeid == '103093' || uploadtypeid == '103094' || uploadtypeid == '26240459'){
		
		if (uploadtypeid == '103092'){	// 103092-System
			document.all.haction.value = 'OnReloadSystemList';
		}
		if (uploadtypeid == '103093'){	// 103093-Project
			document.all.haction.value = 'OnReloadProjectList';
		}
		if (uploadtypeid == '103094'){	// 103094-Group
			document.all.haction.value = 'OnReloadGroupList';
		}
		if (uploadtypeid == '26240459'){	// 26240459-Set Bundle
			document.all.haction.value = 'OnReloadSetBundle';
		}
		document.all.strOpt.value = 'FETCHUPLOADFILES';
	}
	document.forms[0].action = '/gmPDFileUpload.do';
	fnStartProgress('Y');
	document.forms[0].submit();
}

function fnValidateTextField(val, name) {
	var obj = eval("document.all." + val);
	var objVal = TRIM(obj.value);
	if(obj.disabled != true && objVal == '') {
		Error_Details(Error_Details_Trans(message_prodmgmnt[17],name));
		}
}

// fnSubmit() will prepare the inputstring in passed it to action / servlet
function fnSubmit(form) {
	gridObj.editStop();
	var gridrowid;
	var inputstr = '';
	var gridrows = gridObj.getCheckedRows(0);	
	var tempval = '';
	var tempfilenm = '';
	var temptitle = '';
	var temptype = '';
	var tempsize = '';
	var temporderby = '';
	var chk_disable_val = '';
	
	var all_row = gridObj.getAllRowIds(",");
	var all_rowLen = all_row.length;
	var rowsarr = gridrows.toString().split(",");
	
	var uploadtypeid = document.all.uploadTypeId.value;
	var refid = document.all.refId.value;
	
	//validate duplicate order by values
	fnValidateOrderBy();
	
	if(rowsarr.length > 0)
	{
		for ( var rowid = 0; rowid < rowsarr.length; rowid++)
		{
			grid_dis_rowid = rowsarr[rowid];
			
			temptitle = gridObj.cellById(grid_dis_rowid, title_rowId).getValue();
			temporderby = gridObj.cellById(grid_dis_rowid, orderby_rowId).getValue();
			chk_disable_val = gridObj.cellById(grid_dis_rowid, filechk_hide_rowId).getValue();
			if (chk_disable_val == "checkbox_disable"){
				inputstr += temptitle + '^' + temporderby +'^'+grid_dis_rowid + '|';
			}
		}
	}
	
	if ( inputstr == ''){
		Error_Details(message_prodmgmnt[18]);
	}
	
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	
	if(inputstr != '')	
	{
		document.frmPDFileUpload.fileInputStr.value = inputstr;
		document.frmPDFileUpload.action = '/gmPDFileUpload.do';
		document.frmPDFileUpload.strOpt.value = 'SaveUploadedFiles';
		fnStartProgress('Y');
		document.frmPDFileUpload.submit();
	}
	
}

function fnVoid(){
	var uploadtypeid = document.frmPDFileUpload.uploadTypeId.value;
	var voidinputstr = gridObj.getCheckedRows(1);
	var refid = document.frmPDFileUpload.refId.value;
	
	if(voidinputstr == ''){
		Error_Details(message_prodmgmnt[19]);
	}
	
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}		
		var redirectURL = "/gmPDFileUpload.do?strOpt=FetchVoidUploadFiles&uploadTypeId="+uploadtypeid+"&refId="+refid;
		document.frmPDFileUpload.hRedirectURL.value = redirectURL;
		document.frmPDFileUpload.action = "/GmCommonCancelServlet?hTxnName="+refid+"&hCancelType=VUPDF&hTxnId="+voidinputstr+"&hAction=Load"+"&hDisplayNm=File Upload Screen"+"&hRedirectURL="+encodeURIComponent(redirectURL);
		fnStartProgress('Y');
		document.frmPDFileUpload.submit();

}

function fnEditUploadedFile(rowid)
{
	
	fileName = gridObj.cellById(rowid, fileName_hide_rowId).getValue();
	titleType = gridObj.cellById(rowid, titleType_rowId).getValue();
	subType = gridObj.cellById(rowid, subType_rowId).getValue();
	fileTitle = gridObj.cellById(rowid, title_rowId).getValue();
	fileType = gridObj.cellById(rowid, reftype_rowId).getValue();
	//PMT#37770- to show part number values on upload screen
	partNumber = gridObj.cellById(rowid, partnum_rowId).getValue();
	document.getElementById("parttxtval0").innerHTML=partNumber;
	document.all.fileTypeId0.value = fileType;
	fnArtifactChange(0);
	document.all.subType0.value = subType;
	if(titleType == "103216"){ //Title Attribute for DCOed document.
		fileTitle = gridObj.cellById(rowid, titleRefId_rowId).getValue();
	}
	fnSubTypeChange(0,fileTitle); // 0 index file upload row
	document.all.fileTitle0.value = fileTitle;	
	document.all.hfilename.value = fileName;
	document.all.hfileId.value = rowid; // File ID (primary key)
	document.all.strOpt.value = 'EditUploadFiles';
	document.getElementById('attachMoreFile').innerHTML='';
	var table = document.getElementById("UploadFiles");
	for ( var i = 1, row; row = table.rows[i]; i++) { 
		row.style.display='none';
	}
}

//This function will call once click on file name hyper link.
function fnFileView(fileNm,fileid,filetype,fileGroupNm,fileRefid,artifactNm,subTypeNm){
	fileNm = fileNm.split('^').join(' ');
	filetype = filetype.split('^').join(' ');
	fileGroupNm = fileGroupNm.split('^').join(' ');
    if (artifactNm == '103112'){ //Images 
		//for zip file ajax will call and unzip file in server 
		if(subTypeNm=='103202'){//360 view
			 fnPreviewZip(fileNm,fileid,fileGroupNm,fileRefid,artifactNm,subTypeNm);	
		}else{
			windowOpener("/prodmgmnt/productcatalog/GmUploadView.jsp?fileName="+fileNm+"&fileId="+fileid+"&fileGrpNm="+fileGroupNm+"&artifactNm="+artifactNm+"&subTypeNm="+subTypeNm+"&fileRefId="+fileRefid,"PrntInv","resizable=yes,scrollbars=yes,width=560,height=500");
		}
    }
    else{
    	fnDownloadFile(fileid,fileGroupNm,fileRefid,artifactNm,subTypeNm);
    }
}
// fnDownloadFile() to download file
function fnDownloadFile(fileid,fileGroupNm,fileRefid,artifactNm,subTypeNm){
	document.frmPDFileUpload.haction.value = '';
	document.frmPDFileUpload.action = '/gmPDFileUpload.do?strOpt=Download&fileID='+fileid+'&fileGroupNM='+fileGroupNm+'&artifactNm='+artifactNm+'&subTypeNm='+subTypeNm+'&typeID='+fileRefid;
	document.frmPDFileUpload.submit();	 
}
//fnPreviewZip to preview zip file
function fnPreviewZip(fileNm,fileid,fileGroupNm,fileRefid,artifactNm,subTypeNm){
	//ajax will call and unzip the file in server and return sample files names with comma seperator. 
	dhtmlxAjax.get('/gmPDFileUpload.do?strOpt=previewzip&fileID=' + fileid
			+ '&fileGroupNM=' + fileGroupNm + '&artifactNm=' + artifactNm
			+ '&subTypeNm=' + subTypeNm + '&typeID=' + fileRefid + '&ramdomId='
			+ Math.random(), function(loader) {
		var response = loader.xmlDoc.responseText;

		if (response == "") {
			Error_Details(message_prodmgmnt[20]);
			Error_Show();
			Error_Clear();
			return false;
		} else if (response != null && response.length > 0) {
			windowOpener(
					"/prodmgmnt/productcatalog/GmUploadView.jsp?viewType=previewzip&fileName="
							+ fileNm + "&fileId=" + fileid + "&fileGrpNm="
							+ fileGroupNm + "&artifactNm=" + artifactNm
							+ "&subTypeNm=" + subTypeNm + "&fileRefId="
							+ fileRefid + "&fileLists=" + response, "PrntInv",
					"resizable=yes,scrollbars=yes,width=580,height=560");
		}

	});
}

function fnValidateGridData(fileList)
{
	var fileArrLength = fileList.length;
	var gridfileName = '';
	
	for (i=0; i < fileArrLength; i++)
	{
		gridfilecount = 0;
		var updfilename = fileList[i];
		if(gridObj != undefined){  //  initial grid object check while uploadfile button click  in PMT-37770
			gridObj.forEachRow(function(rowId) {
				gridfileName = gridObj.cellById(rowId, fileName_hide_rowId).getValue();
				if(gridfileName == updfilename){
					gridfilecount++;
					if (gridfilecount>0){
						Error_Details(Error_Details_Trans(message_prodmgmnt[16],gridfileName));
						return false;
					}
				}
		   });
		}else{
			return true;
		}
		
	}
}



