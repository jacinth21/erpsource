/***********************************************************************************
 * Company Mapping
 ***********************************************************************************/

var gridObj;
//Initiating grid when on page loading 
function fnOnPageLoad(){
	
if(parent.window.document.getElementById("ajaxdivcontentarea") != undefined){
  	parent.window.document.getElementById("ajaxdivcontentarea").style.height = "600";
}
if(objGridData.value != ''){ 
		gridObj = initGridData('dataGridDiv',objGridData);
		//
		comp_id_rowId = gridObj.getColIndexById("comp_id");
		comp_name_rowId = gridObj.getColIndexById("comp_name");
		remove_rowId = gridObj.getColIndexById("comp_remove");
}

}
//Initiating the grid
function initGridData(divRef,gridData){
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
//	/gObj.attachHeader("#select_filter,#text_filter,#select_filter,#select_filter,#select_filter");
	gObj.init();
	gObj.loadXMLString(gridData);	
	return gObj;	
}

// this function used to validate the company list and form the project company
// map string - submit the form
function fnSubmit()
{
	var inputStr = '';
	// to form the input string
	gridObj.forEachRow(function(rowId) {
		company_id = gridObj.cellById(rowId, comp_id_rowId).getValue();
		inputStr = inputStr + company_id + '^';
	
	});
	// if no company selected then throw an validation message
	if (inputStr == ''){
		Error_Details(message_prodmgmnt[346]);
	}
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	fnStartProgress('Y');
	document.frmProjectDetails.hinputString.value = inputStr; 
	document.frmProjectDetails.strOpt.value ="save";
	document.frmProjectDetails.submit();
}
//Function to export excel
function fnExport()
{	
	gridObj.toExcel('/phpapp/excel/generate.php');
}

// this function used to add the select company into grid
// validate selected company already exist on the grid
function fnChangeCompany(compObj) {
	if (compObj.value != 0) {
		// loop the grid and validate the selected company exist on grid
		gridObj.forEachRow(function(rowId) {
			company_id = gridObj.cellById(rowId, comp_id_rowId).getValue();
			if (company_id == compObj.value) {
				Error_Details(message_prodmgmnt[347]);
			}
		});

		if (ErrorCount > 0) {
			Error_Show();
			Error_Clear();
			return false;
		}

		var compName = document.frmProjectDetails.projectReleaseCompanyId.options[document.frmProjectDetails.projectReleaseCompanyId.selectedIndex].text;
		// var rows = gridObj.getUID();
		// to add the new row and set the company information.
		gridObj.addRow(RowCount, '');
		gridObj.cellById(RowCount, comp_id_rowId).setValue(compObj.value);
		gridObj.cellById(RowCount, comp_name_rowId).setValue(compName);
		gridObj.cellById(RowCount, remove_rowId).setValue("<img alt='Remove the Company' src='/images/delete.gif' border='0' onClick='fnDeleteCompany("+ RowCount + ");'>");
		RowCount = RowCount + 1;
	}
}

// this function used to delete the selected row
function fnDeleteCompany(id) {
	gridObj.deleteRow(id);
}