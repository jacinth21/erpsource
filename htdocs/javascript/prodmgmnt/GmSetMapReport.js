function fnCallSetMap(setid,setname)
{
	document.frmVendor.hSetId.value = setid;
	document.frmVendor.hAction.value = 'Drilldown';
	document.frmVendor.hOpt.value = 'SETMAP';
	document.frmVendor.hSetNm.value = setname;
	document.frmVendor.submit();
}

function fnGo(){
	document.frmVendor.hAction.value = 'Load';
	document.frmVendor.hOpt.value = stOpt;	
    document.frmVendor.hLoadData.value = 'Y';
	fnStartProgress('Y');
	document.frmVendor.submit();
}


function fnCloseSet(){
		if(document.frmVendor.hFromPage.value == 'Parent'){
			parent.dhxWins.window("w1").close();	
		}else{
	   		CloseDhtmlxWindow();
		}
	   return false;
}

//This function is used to load the main inventory based on the selected set ID
function fnLookup(setId)
{
	var strSetId ='';
	if(setId != undefined && setId !=''){
		strSetId = setId;
	}
	if(strSetId == ''){
		Error_Details(message_operations[251]);
	}
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
    windowOpener('/GmPartInvReportServlet?hAction=Go&Cbo_Search=0&hSetId='+strSetId+'&subComponentFl=on'+'&enableObsolete=on',"Pack","resizable=yes,scrollbars=yes,top=250,left=250,width=960,height=510");
}