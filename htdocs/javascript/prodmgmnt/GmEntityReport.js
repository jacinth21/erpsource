//PMT#40410-region mapping info on popup window
var gridObj;

//to fetch company details respective of region by sending ajax request to action class
function fnOnPageLoad() {

	response = '';
	fnStartProgress();
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmEntity.do?method=fetchEntityDtls&regionId='
			+ strRegionId + '&ramdomId=' + Math.random());
	dhtmlxAjax.get(ajaxUrl, fnCallBack);

}

function fnCallBack(loader) {

	response = loader.xmlDoc.responseText;
	fnStopProgress();
	if (response != "") {
		fnLoadRegisonDetail(loader);

	}

	else {
		
		document.getElementById("dataGridDiv").style.display = 'none';
		document.getElementById("DivNothingMessage").style.display = 'block';
		document.getElementById("DivExportExcel").style.display = 'none';

		fnStopProgress();
		Error_Details(message_prodmgmnt[480]);
		if (ErrorCount > 0) {
			Error_Show();
			Error_Clear();
			return false;
		}

	}

}

//to form company details respective of region in grid
function fnLoadRegisonDetail() {
	var setHeader = [ "Company ID", "Company Name" ];
	var setColIds = "companyid,companyname";
	var setColTypes = "ro,ro";
	var setColAlign = "left,left";
	var setColSorting = "int,str";
	var enableTooltips = "true,true";
	var setFilter = [ "#numeric_filter", "#text_filter" ];
	var setInitWidths = "190,316";
	var gridHeight = "";
	var dataHost = {};
	format = "Y";
	
	document.getElementById("DivNothingMessage").style.display = 'none';

	// split the functions to avoid multiple parameters passing in single
	// function
	gridObj = initGridObject('dataGridDiv');
	gridObj = fnCustomTypes(gridObj, setHeader, setColIds, setInitWidths,
			setColAlign, setColTypes, setColSorting, enableTooltips, setFilter,
			gridHeight, format);
	gridObj = loadDhtmlxGridByJsonData(gridObj, response);

}

//This Function Used to set custom types conditions in Grid

function fnCustomTypes(gObj, setHeader, setColIds, setInitWidths, setColAlign,
		setColTypes, setColSorting, enableTooltips, setFilter, gridHeight,
		format) {

	gObj.setHeader(setHeader);
	var colCount = setHeader.length;

	gObj.setInitWidths(setInitWidths);
	gObj.setColAlign(setColAlign);
	gObj.setColTypes(setColTypes);
	gObj.setColSorting(setColSorting);
	gObj.enableTooltips(enableTooltips);
	gObj.attachHeader(setFilter);
	gObj.setColumnIds(setColIds);
	gObj.enableAutoHeight(true, gridHeight, true);

	return gObj;
}

//This function used to download the Region Mapping details to excel file
function fnExportRegionMappingDtls() {
	gridObj.toExcel('/phpapp/excel/generate.php');
}

// This function used to close popup region map window
function fnClose() {
	window.close();
}