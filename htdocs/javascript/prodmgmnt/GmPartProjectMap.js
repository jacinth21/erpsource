var assocProjectID ='';
var assocProjectName = '';
var voidinputstr = '';
var rowid = '';
function fnOnPageLoad(){ 
	
	var companyId = document.frmPartProjectMapDtls.companyId.value;

	if(companyId == '0'){
		rowid = document.getElementById("Row_AssPrjId");
		rowid.style.display = "none";	
	}
	if (parent.window.document.getElementById("ajaxdivcontentarea") != undefined) {
		parent.window.document.getElementById("ajaxdivcontentarea").style.height = "620";
		parent.window.document.getElementById("ajaxdivcontentarea").style.width = "960";
		}
	if (objGridData.value != '') {
		gridObj = initGridData('ASSOCPROJECTDATA',objGridData);
		assocProjectID = gridObj.getColIndexById("AssocProjectid");
		assocProjectName = gridObj.getColIndexById("AssocProjectName");  
	}
}
//This function is used to initiate grid
function initGridData(divRef,gridData){
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);
	gObj.init();
	gObj.loadXMLString(gridData);	
	return gObj;	
}
//This function is called in Onchange in Project Dropdown To Add a Project to a Grid.
function fnProjectMap(){
	var assocProjectName = Cbo_AssocProjectId.getSelectedText();
	var assocProjectId = Cbo_AssocProjectId.getSelectedValue();
	var assocProjectadded = 'Y';
	if(assocProjectId!='0' && assocProjectId != null ){
	assocprojectsize = assocprojectsize + 1;
	gridObj.addRow(assocprojectsize,'');	
	gridObj.cells(assocprojectsize,0).setValue(assocProjectId);
	gridObj.cells(assocprojectsize,1).setValue(assocProjectName);
	gridObj.cells(assocprojectsize,4).setValue('<a href="#" onClick=fndelete('+assocprojectsize+')><img alt="Click to Delete Row"  width="17" height="17" src="/images/dhtmlxGrid/delete.gif" border="0"></a>');
	gridObj.cells(assocprojectsize,5).setValue(assocProjectadded);		
	}
}
//When Remove Icon is clicked Data will be removed from the grid.
function fndelete(rowid){
	var Projectid = '';	
	//var assocRepid  = gridObj.getColIndexById("AssocRepid");
	var voidflagchk = gridObj.getColIndexById("Voidflag");
	var voidfl = TRIM(gridObj.cellById(rowid, voidflagchk).getValue());
	Projectid = TRIM(gridObj.cellById(rowid, assocProjectID).getValue());
	if(voidfl != 'Y'){
		voidinputstr += Projectid+ ',';
	}
	gridObj.deleteRow(rowid);
	document.frmPartProjectMapDtls.hvoidString.value =voidinputstr ;
}
//On Submit the Available Project will be mapped to the Part.
function fnSubmitAss(){
	var Projectid = '';	
	var ProjectName = '';
	var inputstr = '';
	var voidstring = document.frmPartProjectMapDtls.hvoidString.value;
	var validatestring ='';
	var count = 0;
	gridObj.forEachRow(function(rowId){
		Projectid = TRIM(gridObj.cellById(rowId, assocProjectID).getValue());
		if(validatestring.indexOf(Projectid+",")==-1){
			validatestring += (Projectid + ",");	 
		}else{
			
			ProjectName = TRIM(gridObj.cellById(rowId, assocProjectName).getValue());
			count = count+1;
		}
		
		if(Projectid != ''){
			inputstr += Projectid+',';
		}
	});
	if(count>0){
		Error_Details(Error_Details_Trans(message[5674],ProjectName));
	}
	if(voidstring == '' && inputstr == ''){
		Error_Details(message[5675]);
	}
	if (ErrorCount > 0){
		Error_Show();
	    Error_Clear();
		return false;
	}
	
	document.frmPartProjectMapDtls.hassociatedprojects.value = inputstr;
	document.frmPartProjectMapDtls.action="/gmPartProjectMapDtls.do?method=saveProject&strOpt=save";
	fnStartProgress('Y');
	document.frmPartProjectMapDtls.submit();
}
//onchange based on company load the project
function fnChangeProjectList(){
	
	var partnum = document.frmPartProjectMapDtls.partnumber.value;
	var companyid = document.frmPartProjectMapDtls.companyId.value;
	if(companyid != '0'){
		dhtmlxAjax.get('/gmPartProjectMapDtls.do?method=fetchProject&strOpt=AJAX&partnumber='+partnum+'&companyId='+companyid+'&'+fnAppendCompanyInfo()+'&randomId='+Math.random(),function(loader){
		var resXMLObj = loader.xmlDoc.responseText;
		Cbo_AssocProjectId.loadXMLString(resXMLObj);
	});
		rowid.style.display = "table-row";
	}else{
		rowid.style.display = "none";
	}

}
