var ajax = new Array();
var td0,td1,td2,td3,td4,td5;
function fnAddRow(id)
{
	var tbody = document.getElementById(id).getElementsByTagName("TBODY")[0];
    var row = document.createElement("TR");
	row.style.backgroundColor = "#FFFFFF";
	td0 = document.createElement("TD")
	td0.innerHTML = Number(cnt)+1;
	
	td1 = document.createElement("TD")
	td1.innerHTML = fnCreateCell('IMG','',1);
	td1.nowrap = 'nowrap';
	    
    td2 = document.createElement("TD")
	td2.innerHTML = fnCreateCell('CBOCON','Condition',10);
	td2.nowrap = 'nowrap';
	td2.width ="50";
	
	td3 = document.createElement("TD")
	td3.innerHTML = fnCreateCell('CBOOPR','Operator',10);
	td3.nowrap = 'nowrap';
	td3.width ="100";

	td4 = document.createElement("TD")
	td4.innerHTML = fnCreateCell('CBOVAL','Values',20);
	td4.nowrap = 'nowrap';
	td4.width ="250";

	row.appendChild(td1);
    row.appendChild(td2);
    row.appendChild(td3);
    row.appendChild(td4);

	tbody.appendChild(row);
	cnt++;
	
	document.frmFilter.hRowCnt.value = cnt;
}
function fnCreateCell(type,val,size)
{
	val = val + cnt;
	var html = '';
	if (type == 'TXT')
	{
		html = '<input type=text style=visibility:hidden;display:none size='+size+' name=Txt_'+val+' class=InputArea>';
		
	}
	else if (type == 'IMG')
	{
		var fn = size;
		//var rowid = document.getElementById("ruleRow"+val).rowIndex;
		//alert(rowid);
		html = '&nbsp;&nbsp;<img border=0 Alt="Click to remove conditions" onclick=javascript:fnRemoveCondition(this); align=absmiddle src=/images/btn_remove.gif height=10 width=9 />';
	}
	else if (type == 'CHK')
	{
		html = '<input type=checkbox name=Chk_'+val+'>';		
	}
	else if (type == 'CBOJOIN')
	{
		html = cboJoin.innerHTML;
		var str = "Cbo_RuleJoins";
		var temp = new Array();
		temp = html.split(str);
		val = str + cnt;
		html = temp[0]+val+temp[1];
	}
	else if (type == 'CBOCON')
	{
		html = cboCon.innerHTML;

		var onChangeStr = "fnloadOprValue";
		var temp1 = new Array();
		temp1 = html.split(onChangeStr);
		
		html = temp1[0]+"fnloadOprValue(this,"+cnt+");"+temp1[1];
		
		var str = "Cbo_Conditions";
		var temp = new Array();
		temp = html.split(str);
		val = str + cnt;

		html = temp[0]+val+temp[1]+val+temp[2];
		//alert(html);
	}
	else if (type == 'CBOOPR')
	{
		html = cboOpr.innerHTML;
		var str = "Cbo_Operators";
		var temp = new Array();		
		temp = html.split(str);		
		val = str + cnt;
		//temp[0]+val+" style=visibility:hidden "+temp[1]);
		html = temp[0]+val+"\" style=\"visibility:hidden;display:none"+temp[1]+val+temp[2];
	}
	else if (type == 'CBOVAL')
	{
		html = cboVal.innerHTML;
		var str = "Cbo_Values";
		var str1 = "Txt_Values";
		var str2 = "Img_Values";
		var str3 = "Img_Part";
		var imgval;
		var imgPart;
		var temp = new Array();
		temp = html.split(str);
		val = str + cnt;
		html = temp[0]+val+"\" style=\"visibility:hidden;display:none"+temp[1]+val+temp[2];
		val  = str1 + cnt;
		imgval= str2 + cnt;
		imgPart = str3 + cnt;
		html = html+'<input type=text style=visibility:hidden;display:none size='+size+' name='+val+' id='+val+' class=InputArea />'+'&nbsp;<img id='+imgval+' style=cursor:hand;display:none;visibility:hidden; onclick=javascript:show_calendar(\'all.'+val+'\'); alt="Click to open calendar" align=absmiddle src=images/nav_calendar.gif height=15 width=18 />'+'<img id='+imgPart+' border=1 style=cursor:hand;display:none;visibility:hidden; onclick=javascript:fnOpenPart(); Alt="Click to open part lookup" align=absmiddle src=images/L.jpg height=12 width=13 />';
		
	}	
	return html;
}

function fnloadOprValue(obj, cnt)
{
	fnGetOperatorList(obj,cnt,'');	
	fnGetValueList(obj,cnt,'');	
}

function fnGetOperatorList(sel,cnt, dbval) 
{	
	var conditionVal = sel.options[sel.selectedIndex].value;
	
	var objname = 'Cbo_Operators'+cnt;
	var obj = document.getElementById(objname);
	obj.options.length = 0;

    var objOprName = 'Cbo_Con_Opr'+conditionVal;
	var objOpr = document.getElementById(objOprName);
	
	if(objOpr != null){
		for (i = 0; i<objOpr.length; i++ )
	     {
		    addOption(obj, objOpr.options[i].text, objOpr.options[i].value);
	     }
	}

	obj.style.visibility = 'visible';
	obj.style.display = 'inline';
	obj.disabled='';
}

function addOption(selectbox,text,value )
{
var optn = document.createElement("OPTION");
optn.text = text;
optn.value = value;
selectbox.options.add(optn);
}

function fnGetValueList(sel,cnt, dbval) 
{
	var conditionVal = sel.options[sel.selectedIndex].value;
	var objImgVal = 'Img_Values'+cnt;
	var objImg = document.getElementById(objImgVal);
	var objImgPart = 'Img_Part'+cnt;
	var objPartImg = document.getElementById(objImgPart);
	objImg.style.visibility = 'hidden';
	objImg.style.display = 'none';
	objPartImg.style.visibility = 'hidden';
	objPartImg.style.display = 'none';

	if(conditionVal == 6)
	{		
		objImg.style.visibility = 'visible';
		objImg.style.display = 'inline';
	}
	if(conditionVal == 1)
	{
		
		objPartImg.style.visibility = 'visible';
		objPartImg.style.display = 'inline';
	}
	var objnameCbo = 'Cbo_Values'+cnt;
	var objnameTxt = 'Txt_Values'+cnt;

	var objCbo = document.getElementById(objnameCbo);
	var objTxt = document.getElementById(objnameTxt);
	var obj;
	var objOprName = 'Cbo_Con_Values'+conditionVal;
	var objOpr = document.getElementById(objOprName);
	
	//alert(objOpr.length);

	 if(objOpr != null && objOpr.length > 0 )
	{
		obj =  objCbo; 
		obj.options.length = 0;	

		 for (i = 0; i<objOpr.length; i++ )
		 {
			addOption(obj, objOpr.options[i].text, objOpr.options[i].value);
		 }
		 objTxt.style.visibility = 'hidden';
		 objTxt.style.display = 'none';
		 objTxt.value='';
	}
	else
	{
		 obj = objTxt;
		 obj.value = '';
		 objCbo.style.visibility = 'hidden';
		 objCbo.style.display = 'none';
		 objCbo.value='';

	}

	obj.style.visibility = 'visible';
	obj.style.display = 'inline';
	obj.disabled='';	
	
}

function createValues(index,cnt,dbval)
{
	var objnameCbo = 'Cbo_Values'+cnt;
	var objnameTxt = 'Txt_Values'+cnt;
	var obj = document.getElementById(objnameCbo);
	var objCbo = document.getElementById(objnameCbo);
	var objTxt = document.getElementById(objnameTxt);
	eval(ajax[index].response);
	objTxt.style.visibility = 'hidden';
	objTxt.style.display = 'none';
	objTxt.value='';
	if(obj.value == '')
	{
		var obj = document.getElementById(objnameTxt);
		eval(ajax[index].response);
		objCbo.style.visibility = 'hidden';
		objCbo.style.display = 'none';
		objCbo.value='';	
	}
	obj.style.visibility = 'visible';
	obj.style.display = 'inline';
	objTxt.disabled = '';
}

function fnCallAnalysisReport()
{
	var inputStr =  fnCreateCondtionString('report');
	//alert(inputStr);
	if(inputStr == '')
	{
		Error_Details(message[5184]);
	}
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	windowOpener("/gmRuleAnalysisReport.do?loadRule=true&strOpt=loadAnalysisReport&inputString="+inputStr,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=820,height=400");
	
}
function fnCreateCondtionString(screentype)
{
	//alert('Type: ' + screentype);
	var varRowCnt = document.frmFilter.hRowCnt.value;
	var k;
	var objCbo;
	var objTxt;

	var str;
	var token = '^';
	var inputstr = '';
	var kStr = '';
	var kStrAdded;
	var selValue;
	var showErr = true;

	for (k=0; k < varRowCnt; k ++)
	{
	//	kStrAdded = false;

		obj = eval("document.frmFilter.Cbo_Conditions"+k);
		if(obj != undefined)
		{
			if (obj.value != '' && obj.value != '0')
			{
				if(obj.value == '1' || obj.value == '4' || obj.value == '7')
				{
					 showErr = false;
				}
				str = obj.value + token;
				obj = eval("document.frmFilter.Cbo_Operators"+k);
				str = str + obj.value + token;
				objCbo = eval("document.frmFilter.Cbo_Values"+k);
				objTxt = eval("document.frmFilter.Txt_Values"+k);
				if(objCbo.value != '0' && objCbo.value != '')
				{
					selValue = objCbo.value;
					str = str + objCbo.value + token;				
				}
				else //if(objTxt.value != '0' && objTxt.value != '')
				{
					selValue = (objTxt.value).trim();
					str = str + (objTxt.value).trim() + token;				
				}
				kStr = 	selValue == '' ? ''+(k+1)+";" : '';
				str = str + '|';
				inputstr = inputstr + str;
			}
		}
    	//alert (inputstr);
	}
	if(showErr && typeof(screentype) == 'undefined')
	{
		 kStr = "PRIMARYCONDITIONSNOTSELECTED";
		 
	}
	//alert( kStr);
	if(kStr != '')
	{
		//alert(kStr);
		return kStr;
	}
	
	return inputstr;
}

function fnClearFilter(ruleId)
{
	document.frmFilter.submit();
}

function fnRemoveCondition(val)
{
	//alert(val.parentNode.parentNode.rowIndex);
	document.all.RuleTable.deleteRow(val.parentNode.parentNode.rowIndex);

}