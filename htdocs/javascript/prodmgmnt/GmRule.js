var ajax = new Array();
var partNum=new Array();
var CNum=new Array();
var rulePnumArray = new Array();
var j = 0;
var i;
var partCnt = 0;
function fnRuleHelp(wikiPath){
	var currentTab = document.all.hcurrentTab.value;
	var wikiTitle = "";
	wikiTitle = currentTab;
	var wikiPage = wikiPath+"/"+wikiTitle;
	windowOpener(wikiPage,"Prntwiki","resizable=yes,scrollbars=yes,top=250,left=50,width=950,height=650");	
}

function fnUpdateTabIndex(link) {
	var linkObj = link.getElementsByTagName('a')[0];
	document.all.hcurrentTab.value = linkObj.name;
}

function fnSelectAll(varCmd)
{
	objSelAll = document.all.selectAll;
	if (objSelAll ) {
		objCheckTransaction = document.all.selectedTrans;
		if ((objSelAll.checked || varCmd == 'selectAll') && objCheckTransaction)
		{
			objSelAll.checked = true;
			objCheckIncArrLen = objCheckTransaction.length;
			for(i = 0; i < objCheckIncArrLen; i ++) 
			{	
				objCheckTransaction[i].checked = true;
			}
		}
		else if (!objSelAll.checked  && objCheckTransaction ){
			objCheckIncArrLen = objCheckTransaction.length;
			for(i = 0; i < objCheckIncArrLen; i ++) 
			{	
				objCheckTransaction[i].checked = false;
			}
		}
	}
}

function fnConditionSubmit(form)
{
	var str = '';
	var len = form.selectedTrans.length;
	var objSelTrans = form.selectedTrans;
	var selectStr = '';
	var ruleid = form.ruleIdtemp.value;
	fnValidateTxtFld('ruleName',message[10089]);
	str	= document.getElementById('frmFilter').contentWindow.fnCreateCondtionString();
	if(str == '')
	{
		Error_Details(message[10011]);
	}
	else if (str == 'PRIMARYCONDITIONSNOTSELECTED')
	{
		Error_Details(message[10011]);
	}
	var strArry = str.split(';');	
	if (strArry.length > 1)
	{
		Error_Details(Error_Details_Trans(message[10012],str));
	}
	
	for(i=0;i<len;i++)
	{
		if(form.selectedTrans[i].checked)
		{
			selectStr = selectStr + form.selectedTrans[i].value+',';
		}
	}
	if(selectStr=='')
	{
		Error_Details(message[10013]);
	}
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	/* 
	if(form.strOpt.value!='edit' && ruleid=='')
	{ 
	 	alert("Enter atleast one consequence to make this rule Valid");
	}*/
	form.activeFl.disabled = '';
	form.strOpt.value='save';
	form.conditionsInputStr.value = str;
	form.transStr.value = selectStr;
	form.submit();	
	
}

/* 
 * Common function to generate the request string
 * @param form - the form object
 * @param params - an array of parameter names which should not be included in the request string
 */
function fnGenerateRequestString(form, params) {
	var requestStr = new String(form.action + '?');
	for (var i=0;i<form.length;i++) {
		var obj = form.elements[i];
		var objName = obj.name;
		var objType = obj.type;
		var objValue = obj.value;
		//alert(form.elements[i].name + ' = ' + form.elements[i].type);
		//alert(objName + ' present = ' + fnCheckExists(objName, params));
		if(fnCheckExists(objName, params)) {
			continue;
		}
		if(i != 0) {
			requestStr += '&';
		}
		if(objType == 'checkbox')
		{
			if(obj.checked == true) { objValue = 'on' } else { objValue = 'off' }
			//alert(objName + ' = ' + objValue);
		}
		requestStr += objName + '=' + objValue;
	}
	return requestStr;
}

function fnCheckExists(value, params) {
	if(params != null) {
		for(var k=0;k<params.length;k++) {
			if(value == params[k]) {
				return true;
			}
		}
		return false;
	} else {
		return false;
	}
}

function fnSubmitConsequence(form)
{
	var strConsequences = escape(getConsequenceString());
	var ruleId = form.ruleId.value;
	var consId = form.consequenceId.value;
	var consGrp = form.consequenceGrp.value; 
	var ruleConsId = form.ruleConsequenceId.value;

//	alert(consGrp + " " + ruleConsId);

   if (consGrp == 'CQEML' && consId=='91348' )
   {
	   if(fnValidateEmail())
	   {
		    Error_Details(message[10014]);
	   }
   }

	if (strConsequences == '')
	{
		Error_Details(message[10015]);
	}
	
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}

	loadajaxpage('/gmRuleConsequence.do?strOpt=save&ruleId='+ruleId+'&ruleConsequenceId='+ruleConsId+'&consequenceId='+consId+'&consequenceGrp='+consGrp+'&consequenceStr='+strConsequences,'ajaxdivtabarea');
}
function getConsequenceString()
{
	var inputString = '';

	var form = document.frmRuleCondition;
	for(var i=0; i<form.elements.length; i++)
	{
		if(form.elements[i].name.indexOf('label') != -1 && form.elements[i].name.indexOf('hlabel') == -1)
		{
			var nm = 'h' + form.elements[i].name;
			var type = form.elements[i].type;
			var objId = document.getElementById(nm).value;
			var objVal = form.elements[i].value;
			if(type == 'checkbox')
			{
				if(form.elements[i].checked)
					objVal = 'Y';
				else
					objVal = 'N';

			}
			if(objVal == '')
			{
				return '';
			}
			
			//alert('Adding id = ' + objId + ' and value = ' + objVal);
			inputString += objId + ';' + objVal + '|';
		}
	}	
	return inputString;
}

function fnVoidConsequence(form)
{
	if(form.ruleConsequenceId.value == '')
	{
		alert(message[10018]);
		return;
	}
	form.action ="/GmCommonCancelServlet";	
	var consId = form.consequenceId.value;
	var txnName = '';
	
	if(consId == 91346)
	{
		
		txnName = "Message Consequence";
	}
	else if (consId == 91348)
	{
		txnName = "Email Consequence";
	}
	form.hTxnName.value = txnName;
	form.hTxnId.value = form.ruleConsequenceId.value;				
	form.submit();		
}

function fnVoidRule(form)
{
	var ruleId = form.hruleId.value;
	//alert('1.test111...'+form.ruleName.value+'....'+ruleId);
	if(ruleId == '')
	{
		Error_Details(message[10016]);		
	}
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	form.hTxnName.value = form.ruleName.value;
	form.hTxnId.value = ruleId;
	form.action ="/GmCommonCancelServlet";
	form.submit();
	
}

function fnEnableCheckbox()
{
	var form = document.frmRuleCondition;
	for(var i=0; i<form.elements.length; i++)
	{
		if(form.elements[i].name.indexOf('label') != -1 && form.elements[i].name.indexOf('hlabel') == -1)
		{
			var nm = 'h' + form.elements[i].name;
			var type = form.elements[i].type;
			var objId = document.getElementById(nm).value;
			var objVal = form.elements[i].value;
			if(type == 'checkbox')
			{
				form.elements[i].disabled='';		

			}			
		}
	}	
}

function fnValidateEmail()
{
	           var error = false;

              //  alert('Validate Email');
                var form = document.frmRuleCondition;
                for(var i=0; i<form.elements.length; i++)
                {
                  if(form.elements[i].name.indexOf('label') != -1 && form.elements[i].name.indexOf('hlabel') == -1)
                      {
                         var nm = 'h' + form.elements[i].name;
                         var type = form.elements[i].type;
                         var objId = document.getElementById(nm).value;
                         var objVal = form.elements[i].value;
                         var objRegExp  = /(^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$)/;
                                                
							 if(objId == 91365) // for email
								 {
                                        var objValArr =  objVal.split(",");
                                         for(var arrCount =0 ; arrCount < objValArr.length; arrCount++)
                                             {
                                                  // alert(objValArr[arrCount]);
												   var emailid = objValArr[arrCount];
													emailid = replaceAll(emailid, " ","");
													//alert(emailid);
													if(emailid != "")
													{
														if(!objRegExp.test(emailid))
														   {              
															  error = true;
															   break;                                                                   
															}
													}
                                               }
                                   }
                       }
					if(error)
					{
						 break;
					}
                }   
	return error;			
}

function fnFetchMessages(obj,pnum,txnStatus,txn,txnTypeID) 
{
     for (i = 0;i<partNum.length; i++)
	 {
		 if(partNum[i] == pnum && CNum[i] == obj.value)
		 {
			 partCnt = partCnt + 1;
			 break;
		 }
		 else
		 {
			 partCnt = 0;			
		 }	 
	 }
     if (partCnt == 0)
	{
		var index = ajax.length;
		ajax[index] = new sack();             
		ajax[index].requestFile =fnAjaxURLAppendCmpInfo("/gmRuleEngine.do?RE_FORWARD=gmRuleDisplayAjaxServlet&RE_AJAX_WRAPPER=AJAXWRAPPER&RE_TXN="+txn+"&txnStatus="+txnStatus+"&pnum="+encodeURIComponent(pnum)+"&cntrlnum="+encodeURIComponent(obj.value)+"&TXN_TYPE_ID="+txnTypeID);                                                                
		ajax[index].onCompletion = function(){ addTableRows(index) };               // Specify function that will be executed after file has been found
		ajax[index].runAJAX();
		partNum[j] =  pnum;
		CNum[j] =  obj.value;
		j++;
	}
	
	
}

function fnFetchAllMessages(obj,pnum,txnStatus,txn, setId, country, qty, txnId) 
{
     
	
	for (i = 0;i<partNum.length; i++)
	 {
		 if(partNum[i] == pnum && CNum[i] == obj.value)
		 {
			 partCnt = partCnt + 1;
			 break;
		 }
		 else
		 {
			 partCnt = 0;			
		 }	 
	 }
	if (partCnt == 0)
	{
		var index = ajax.length;
		ajax[index] = new sack();             
		ajax[index].requestFile = fnAjaxURLAppendCmpInfo("/gmRuleEngine.do?RE_FORWARD=gmRuleDisplayAjaxServlet&RE_AJAX_WRAPPER=AJAXWRAPPER&RE_TXN="+txn+"&txnStatus="+txnStatus+"&pnum="+encodeURIComponent(pnum)+"&cntrlnum="+obj.value+"&setId="+setId+"&country="+country+"&qty="+qty+"&txnId="+txnId);
		ajax[index].onCompletion = function(){ addTableRows(index) };               // Specify function that will be executed after file has been found
		ajax[index].runAJAX();
		partNum[j] =  pnum;
		CNum[j] =  obj.value;
		j++;
	}	
	
}
var m=0;
var skiploop = 1;
var cnt = 0;
/***
Function addTableRows is used to iterate through the TBODY elements.
There is logic to get the chile nodes and the rowindex for the specific row.
This function get the rule id and the part number from the returned HTML and concate it togather
and add it to an array for next iterations.

If the ruleid and the partnum is already exist then the row should not be added to the rule table.
****/
function addTableRows(index)
{
	var tempHtml = '';
	var trhtml = '';
	var ruleId = '';
	var pnumHtml = '';
	var ruleIdPnum = '';
	var indexArray = new Array();
	var l=0, p=0;
	var tmpCnt = 0;
	var tempRulePnumArray = new Array();
	var rulePnumLength = 0;
	var tbody = document.getElementById("rt").getElementsByTagName("TBODY")[0];
	if(ajax[index].response.length > 2)
	{
		
		var obj = document.getElementById("ruleDiv");
		obj.style.visibility = 'visible';
		obj.style.display = 'inline';    
		eval(ajax[index].response);
		//alert('chilenode length = '+tbody.childNodes.length);
		rulePnumLength = rulePnumArray.length;
		tempRulePnumArray = rulePnumArray;
		for(n=skiploop;n<tbody.childNodes.length;n++)
		{
			trhtml = tbody.childNodes[n].innerHTML;
			if(trhtml != '')
			{
				tempHtml = trhtml;
				for (y=0;y<pnumCol;y++)
				{
					trhtml = trhtml.substr(trhtml.indexOf("<TD>")+4);				
				}
				pnumHtml = trhtml.substr(0,trhtml.indexOf("<"));
				tempHtml = tempHtml.substr(tempHtml.indexOf("(")+1);
				ruleId = tempHtml.substr(0,tempHtml.indexOf(")"));
				ruleIdPnum = ruleId + pnumHtml;
				if(ruleIdPnum != '')
				{
					for (k=0;k<rulePnumLength;k++)
					{
						//alert(rulePnumArray[k] +' ... '+ruleIdPnum);
						if(rulePnumArray[k] == ruleIdPnum)
						{
							//alert('n...'+n);
							//alert('row index...test ...'+tbody.childNodes[n].rowIndex);
							indexArray[l] = tbody.childNodes[n].rowIndex-1;
							//alert('row index'+indexArray[l]);
							tmpCnt = 0;
							l++;		
							break;
						}
						else
						{
							tmpCnt ++;
						}
					}
					if(rulePnumLength == 0)
					{
						rulePnumArray[rulePnumArray.length] = ruleIdPnum;
						skiploop = skiploop+1;
					}
				}
				if(tmpCnt > 0 )
				{
					//alert('tmpcntvalue = '+ruleIdPnum);
					rulePnumArray[rulePnumArray.length] = ruleIdPnum;					
					skiploop = skiploop+1;
				}
			}		
		}
	}
//	alert('index length = '+indexArray.length);
	var indexLength = indexArray.length;
	var l=0;
	if(indexLength > 0)
	{
		for(m=0;m<indexLength;m++)
		{
			
			if(!isFinite(indexArray[m]))
			{
				break;
			}
			//alert(m+'...'+indexArray[m]);
			tbody.deleteRow(indexArray[m]-l);
			l++;
			//indexArray[m+1] =  indexArray[m];			
			//alert(m+'..111...'+indexArray[m]);
		}
	}
	/*
	else
	{
		if(ruleIdPnum != '')
		{
			rulePnumArray[rulePnumArray.length] = ruleIdPnum;
			skiploop = skiploop+1;
		}
	}*/
	
}
function fnOpenPicture(ruleId)
{
  windowOpener('/gmRuleEngine.do?RE_FORWARD=gmRulePictureDisplay&strOpt=RE_SHOWPICTURE&RULEID='+ruleId,"PICTURES","resizable=yes,scrollbars=yes,top=150,left=150,width=760,height=610");
}


 function fnCallRuleSummary(strRuleId)
 { 
       
    windowOpener("gmRuleReport.do?strOpt=summary&txnId="+strRuleId,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=820,height=400");
    
 }  

 function fnCallRuleException(strRuleId,strPartNum)
 { 	
	strPartNum = strPartNum.replace('#','');
    windowOpener("gmControlNumber.do?txnId="+strRuleId+"&partNumber="+encodeURIComponent(strPartNum)+"&leftLink=false","RuleException","resizable=yes,scrollbars=yes,top=250,left=300,width=820,height=400");
 }
 
function fnNewRule(form)
 {   
   form.ruleId.value = ''; 
   form.action = "/GmPageControllerServlet?strPgToLoad=gmRuleCondition.do&strOpt=&ruleId=";  
   form.submit();
    } 

function fnOpenPart()
	{	 
		var varPartNum = "";
		var varCount = "";
		var varAccId = "";
		//windowOpener("/GmSearchServlet?hAction=SearchPart&Txt_AccId=PARTLOOKUP&Txt_PartNum="+"&hCount=1","Search","resizable=yes,scrollbars=yes,top=340,left=420,width=600,height=435");
		windowOpener("/GmSearchServlet?hAction=SearchPart&Txt_PartNum="+encodeURIComponent(varPartNum)+"&hCount="+varCount+"&Txt_AccId="+varAccId,"Search","resizable=yes,scrollbars=yes,top=340,left=420,width=600,height=435");
	}


function fnLoadHistory(columnId, ruleId)
{
	windowOpener("/GmPageControllerServlet?strPgToLoad=/gmAuditTrail.do&auditId="+ columnId +"&txnId="+ ruleId,"","resizable=yes,scrollbars=yes,top=250,left=300,width=785,height=300,status=1");
  }

function replaceAll(Source,stringToFind,stringToReplace){

  var temp = Source;

    var index = temp.indexOf(stringToFind);

        while(index != -1){

            temp = temp.replace(stringToFind,stringToReplace);

            index = temp.indexOf(stringToFind);

        }

        return temp;

}
function fnLoadRuleTrans(obj)
{
	objCheckTransaction = document.all.selectedTrans;
	objCheckIncArrLen = objCheckTransaction.length;
	objSelAll = document.all.selectAll;
	objSelAll.checked = false;
	for(k = 0; k < objCheckIncArrLen; k++) 
	{
		objCheckTransaction[k].checked = false;
	}
	
	for (i=1;i<ruleTransKeyArray.length;i++ )
	{
		if(obj.value == ruleTransKeyArray[i])
		{
			var tempRuleArray = ruleTransArray[i].split(",");
			//alert(ruleTransArray.length+'.....'+ruleTransArray[i]);
			for(j=0;j<tempRuleArray.length;j++)
			{
				//alert(tempRuleArray[j])
				for(k = 0; k < objCheckIncArrLen; k++) 
				{	
					if(objCheckTransaction[k].value == tempRuleArray[j]) 
					{
						objCheckTransaction[k].checked = true;
						break;
					}

				}
			}
		}
	}
}
function fnUpdtSelAll(obj)
{
	objSelAll = document.all.selectAll;
	if(obj.checked == false)
	{
		objSelAll.checked = false;
	}
}

//Function will call on page load. It will append companyInfo in each tab href and initiate ajaxtab.
function fnAjaxTabLoad(){
	fnAjaxTabAppendCmpInfo("#maintab");
	fnMainTab();
}

//initiate ajaxtab
function fnMainTab(){
	var maintab=new ddajaxtabs("maintab", "ajaxdivcontentarea");				
maintab.setpersist(false);								
maintab.init(); 
maintab.onajaxpageload=function(pageurl){					    
	if (pageurl.indexOf("gmRuleConsequence.do")!=-1){					    
		var consqtab=new ddajaxtabs("consqtab", "ajaxdivtabarea");
		consqtab.setpersist(false);	
		consqtab.setselectedClassTarget("link") //"link" or "linkparent"
		consqtab.init();
	}
}
}

// allow to override lot control number transaction for expired date with in 1 month
function fnCallExpDateRuleException(strRuleId,strPartNum)
{ 	
	    var strsplit = strPartNum.split('|');
	    strPartNum=strsplit[0];
	    var strCntlNm =strsplit[1];
		strPartNum = strPartNum.replace('#','');
		strCntlNm = strCntlNm.replace('#','');
		windowOpener("gmControlNumber.do?txnId="+txnId+"&partNumber="+encodeURIComponent(strPartNum)+"&controlNM="+strCntlNm.trim()+"&strOpt=loadExpDtOverride&leftLink=false","ExpiryRuleException","resizable=yes,scrollbars=yes,top=250,left=300,width=820,height=400");
}

