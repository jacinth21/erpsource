
function fnReset()
{
	document.frmDemandSheetSetup.demandSheetId.value = '0';
	document.frmDemandSheetSetup.demandSheetType.value = '';
	document.frmDemandSheetSetup.demandSheetName.value = '';
	document.frmDemandSheetSetup.demandPeriod.value = '';
	document.frmDemandSheetSetup.forecastPeriod.value = '';
	document.frmDemandSheetSetup.hierarchyId.value = '0';
	document.frmDemandSheetSetup.demandSheetOwner.value = '0';
	document.frmDemandSheetSetup.includeCurrMonth.checked = false;
	document.frmDemandSheetSetup.activeflag.checked = false; 
	document.frmDemandSheetSetup.submit();
}	

function fnFetch()
{	
	fnStartProgress();
	document.frmDemandSheetSetup.strOpt.value = "edit";
	document.frmDemandSheetSetup.submit();
}

function fnSubmit()
{
	fnValidateTxtFld('demandSheetName', 'Demand Sheet Name');
	fnValidateTxtFld('demandPeriod', 'Demand Period');
	fnValidateTxtFld('forecastPeriod', 'Forecast Period');
	fnValidateDropDn('hierarchyId',' Team');
	if (ErrorCount > 0)
		 {
			Error_Show();
			Error_Clear();
			return false;
		}
	document.frmDemandSheetSetup.strOpt.value = 'save';
	fnStartProgress('Y');
	document.frmDemandSheetSetup.submit();
}

function fnJobSetup()
{
	fnValidateDropDn('demandSheetId', ' Demand Sheet Name');
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	varRefId = document.frmDemandSheetSetup.demandSheetId.value;
	varRefTypeText = 'Order Planning Load';
	varRefType = 90720;
	varJobName = document.frmDemandSheetSetup.demandSheetName.value;
	varStrOpt = "edit";
	windowOpener("/GmPageControllerServlet?strPgToLoad=jobSetup.do&refId="+varRefId+"&refTypeText="+varRefTypeText+"&jobName="+varJobName+"&refType="+varRefType+"&strOpt="+varStrOpt,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=750,height=480");
}

function fnMapping()
{
	fnValidateDropDn('demandSheetId',' Demand Sheet Name');
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	varSheetId = document.frmDemandSheetSetup.demandSheetId.value;
	varSheetName = document.frmDemandSheetSetup.demandSheetName.value;
	varRefType = document.frmDemandSheetSetup.demandSheetType.value;
	varRefTypeName = document.frmDemandSheetSetup.demandSheetTypeName.value;
	
	//varRefTypeName = document.frmDemandSheetSetup.demandSheetType.options[document.frmDemandSheetSetup.demandSheetType.selectedIndex].text;
	
	windowOpener("/GmPageControllerServlet?strPgToLoad=gmDemandGroupMap.do&demandSheetId="+varSheetId+"&demandSheetName="+varSheetName+"&demandSheetTypeId="+varRefType,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=785,height=550,status=1");

//	windowOpener("/GmPageControllerServlet?strPgToLoad=gmDemandGroupMap.do","PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
}

function fnSetMapping()
{
	fnValidateDropDn('demandSheetId',' Demand Sheet Name');
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	varSheetId = document.frmDemandSheetSetup.demandSheetId.value;
	  
	windowOpener("/GmPageControllerServlet?strPgToLoad=gmSetMapping.do&demandSheetId="+varSheetId,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=785,height=550,status=1");
  }

function fnTeamSetup()
{
	Error_Details(message_prodmgmnt[15]);
	
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
}

function fnVoid()
{
		fnValidateDropDn('demandSheetId',' Demand Sheet Name');
		if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
		document.frmDemandSheetSetup.action ="/GmCommonCancelServlet";
		document.frmDemandSheetSetup.hTxnId.value = document.frmDemandSheetSetup.demandSheetId.value;
		document.frmDemandSheetSetup.hTxnName.value = document.frmDemandSheetSetup.demandSheetName.value;
		document.frmDemandSheetSetup.submit();
}
function fnOpenLaunch(){
	fnValidateDropDn('demandSheetId', ' Demand Sheet Name');
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	varSheetId = document.frmDemandSheetSetup.demandSheetId.value;	 
	windowOpener("/gmLaunch.do?demandSheetId="+varSheetId,"Launch","resizable=yes,scrollbars=yes, top=350,left=450,width=780,height=550");
}
 

