function initGridData(divRef,gridData)
{
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);
	gObj.enableTooltips("true,true,true,true,true,true");
	gObj.init();	
	gObj.loadXMLString(gridData);	
	return gObj;	
}
function fnOnLoad() {
	if (objGridData != '') {
		gridObj = initGridData('SetFileReport',objGridData);
	}
}

function fnDownloadXLS()
{
		gridObj.toExcel('/phpapp/excel/generate.php');
	
}
function fnLoad(){
	document.all.hReport.value="Y";
	fnStartProgress('Y');
	document.frmVendor.submit();
}