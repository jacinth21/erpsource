function fnCallProject(val)
{
	document.frmVendor.hAction.value = 'Part';
  	document.frmVendor.submit();
}

function fnGo()
{
	document.frmVendor.hAction.value = 'Go';
	document.frmVendor.submit();
}

var prevtr = 0;
function changeTRBgColor(val,BgColor) {
   var obj1 = eval("document.all.tr"+prevtr);
   obj1.style.background = "#ffffff";
   var obj = eval("document.all.tr"+val);
   prevtr = val;
   obj.style.background = BgColor;
}

var cnt = 0;
function fnAddRow(id,varOpt)
{
    var tbody = document.getElementById(id).getElementsByTagName("TBODY")[0];
    var row = document.createElement("TR");
    row.style.backgroundColor = "#FFFFFF";
    var td1 = document.createElement("TD")
    td1.innerHTML = fnCreateCell('TXT','PNum',7)
    var td2 = document.createElement("TD")
    td2.innerHTML = '';
    td2.align = "Center";
    var td3 = document.createElement("TD")
    td3.innerHTML = fnCreateCell('TXT','Qty',2);
    td3.align = "Center";
    var td4 = document.createElement("TD")
    td4.innerHTML = fnCreateCell('CHK','Set',3);
    td4.align = "Center";
    var td5 = document.createElement("TD")
    td5.innerHTML = fnCreateCell('CHK','Crit',3);
    td5.align = "Center";
    var td6 = document.createElement("TD")
    td6.innerHTML = fnCreateCell('CBO','CritType',10);
    td6.align = "Center";
    var td7 = document.createElement("TD")
    td7.innerHTML = fnCreateCell('TXT','CritQty',2);
    td7.align = "Center";    
    var td8 = document.createElement("TD")
    td8.innerHTML = fnCreateCell('TXT','CritTag',2);
    td8.align = "Center";
  //  var varOpt = '<%=strhOpt%>'; 
    
    if(varOpt =='BOMMAP')
    {
	    var td9 = document.createElement("TD")
	    td9.innerHTML = fnCreateCell('CBOU','UnitType',10);
	    td9.align = "Center";
    }
      
    row.appendChild(td1);
    row.appendChild(td2);
    row.appendChild(td3);
    row.appendChild(td4);
    row.appendChild(td5);
    row.appendChild(td7);
    row.appendChild(td6);
    row.appendChild(td8);
    if(varOpt =='BOMMAP')
    {
    	row.appendChild(td9);  
      }    
    tbody.appendChild(row);
	cnt++;
}

function fnCreateCell(type,val,size)
{
	val = val + cnt;
	var html = '';
	if (type == 'TXT')
	{
		html = '<input type=text size='+size+' name=Txt_'+val+' class=InputArea>';
		if(val == 'CritQty'+cnt || val == 'CritTag'+cnt)
		{
			html = '<input type=text '+disabled+' size='+size+' name=Txt_'+val+' class=InputArea>';
		}
	}
	else if (type == 'CHK')
	{
		html = '<input type=checkbox name=Chk_'+val+'>';
		if(val == 'Crit'+cnt)
		{
			html = '<input type=checkbox '+disabled+' name=Chk_'+val+'>';
		}
	}
	else if (type == 'CBO')
	{
		html = cbohtml.innerHTML;
		var str = "Cbo_CritType";
		var temp = new Array();
		temp = html.split(str);
		val = str + cnt;
		html = temp[0]+val+temp[1];
	}
	
	if (type == 'CBOU')
	{
		html = cbouhtml.innerHTML;
		var str = "Cbo_UnitType";
		var temp = new Array();
		temp = html.split(str);
		val = str + cnt;
		html = temp[0]+val+temp[1];
	}
	
	return html;
}

function fnSubmit(varOpt)
{
	var obj = '';
	var i=0;
	var k=0;
	var hcnt = document.frmVendor.hCnt.value;
	var lockstr = '';
	
	var arr = '';
	var len = 0;
	var inputstr = '';
	var str = '';
	var pid = '';
	var vfl = '';
	var varpnum = '';
	var duplparts = '';
 
	var changedval =''; 
	
	for (j=0;j<cnt;j++)
	{
			obj = eval("document.frmVendor.hId"+j);
			if (obj)
			{
				pid = obj.value;
				vfl = 'U';
			}else{
				pid = '0';
				vfl = 'I';			
			}

			obj = eval("document.frmVendor.Txt_PNum"+j);
			varpnum = obj.value;
			changedval = varpnum.replace('\.','\\.');
			inputstr.match(changedval)
			
			
			if (varpnum == ''){
				j++;
				break;
			}
			
			if(inputstr.match(changedval)){
				duplparts = varpnum + " " +duplparts ;
			}
			
			
			str = varpnum+'^';

			obj = eval("document.frmVendor.Txt_Qty"+j);
			str = str + obj.value+'^';

			obj = eval("document.frmVendor.Chk_Set"+j);
			lfl = obj.status;
			if (lfl == true)
			{
				lfl = 'Y';
			}else
			{
				lfl = 'N';
			}
			str = str + lfl +'^';
			
			obj = eval("document.frmVendor.Chk_Crit"+j);
			lfl = obj.status;
			if (lfl == true)
			{
				lfl = 'Y';
			}else
			{
				lfl = 'N';
			}
			str = str + lfl +'^';			
			
			obj = eval("document.frmVendor.Txt_CritQty"+j);
			str = str + obj.value+'^';

			obj = eval("document.frmVendor.Cbo_CritType"+j);
			str = str + obj.value+'^';
			
			if(varOpt =='BOMMAP')
    		{
				obj = eval("document.frmVendor.Cbo_UnitType"+j);
				str = str + obj.value+'^'; 
				}
				else 
				str = str + '' +'^';

			obj = eval("document.frmVendor.Txt_CritTag"+j);
			str = str + obj.value+'^';
		
			str = str + vfl + '^' + pid;

			inputstr = inputstr + str + '|';

	}// End of MAIN FOR
	
	 
	if (duplparts != ''){
			Error_Details(" The Part(s) <B>"+duplparts+"</B> already exist in the list. Duplicate parts are not allowed in the set ");
		}
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	} 
	
	document.frmVendor.inputString.value = inputstr;
	//alert("STR:"+document.frmVendor.inputString.value);	
	document.frmVendor.hAction.value = 'Save';
 	document.frmVendor.submit();
}

function fnSetSelections()
{
	
	cnt=count; 
		
	if (document.frmVendor.hAction.value == "Go")
	{
		cnt = document.frmVendor.hCnt.value;	
	}
}

function checkTaggable()
{
 var strSetType = "";
 var strPartFamily ="";
  var strProdAttributeValue = "";
 
	 document.getElementById("strTaggable_part").style.display = 'none';
	  document.getElementById("strCritical_Qty").style.display = 'none'; 
	 var checkboxname="";
	 
	 for (var i = 0; i <  cnt ; i++ ) 
	 {
			checkboxname = eval("document.frmVendor.Chk_Crit" + i);
			strSetType = eval("document.frmVendor.strSetTYPE" + i).value;
			strPartFamily = eval("document.frmVendor.strPartFamily" + i).value;
			strProdAttributeValue= eval("document.frmVendor.strProdAttributeValue" + i).value;
		  	if (checkboxname.checked != true && (strSetType=='4070' || strSetType=='4071')) 
	 		{
	         document.getElementById("strCritical_Qty").style.display = 'block';
			 flaghA = "false";
			}
			else if(strPartFamily == '4052' && strProdAttributeValue =='Y')
			{
		  		document.getElementById("strTaggable_part").style.display = 'block';
		  		break;
			}
			else if (checkboxname.checked == true) 
			{
			    	document.getElementById("strCritical_Qty").style.display = 'none';
			    	flaghA = "true";
			      	break;
			}
      }
}