function fnEnableConsngFl(){

	var typeVal = document.frmVendor.Cbo_Type.value;
	var typeFl = document.frmVendor.Cbo_Type.disabled;
	parent.document.getElementById("Txt_SetId").value = newSetId;
	parent.document.getElementById("searchCbo_SetId").value = newSetNm;
}

function fnSetFetch(forms){
	
	var setID = forms.Cbo_SetId.value;	
	forms.action = "/GmSetMasterServlet?hForward=FETCHCONTAINER&Cbo_SetId="+setID+"&hAction=Reload";
	fnStartProgress();
	forms.submit();
}

function fnSubmit(form,val){
	//form = document.getElementsByName('frmVendor')[1];
	//var formMaster = document.getElementsByName('frmVendor')[0];
	var setID = parent.document.getElementById("Cbo_SetId").value;
	if(document.getElementById("Cbo_Division") != undefined){
	var div_id  = document.getElementById("Cbo_Division").value;
	}
	document.getElementById("Cbo_Division").disabled = false;
	

	if (val != 'Reload'){
		var varAction = form.hAction.value;	
		fnValidateDropDn('Cbo_Proj',message_prodmgmnt[319]);
		fnValidateDropDn('Cbo_Division',message_prodmgmnt[320]);
		fnValidateTxtFld('Txt_SetId', number);
		fnValidateTxtFld('Txt_SetNm', desc);
		fnValidateTxtFld('Txt_LogReason',message_prodmgmnt[146]);
	//To fix BUG-3673 commented the below code.
	/*  if (form.hType.value == '1602')	{
			fnValidateTxtFld('Txt_SetDesc',' Part # ');
		}	*/
		if ((varAction == 'Load' || varAction == 'Save') && val == 'Save'){
			form.hMode.value = 'Add';
		}else{
			form.hMode.value = 'Edit';
		}
		if (val == 'Save'){
			var filename = form.uploadfile.value;
			var cbofilename = form.cbo_inspection_sheets.value;
			var searchCbofilename = form.searchcbo_inspection_sheets.value;// Get the value of seach box
			if( filename != "" && (cbofilename != "0" && cbofilename != '')){
				Error_Details(message_prodmgmnt[136]);
			}else{
				if( filename != "" ){ 
					form.h_filename.value=filename;
					form.h_uploadfile.value='true';
				}
				if( cbofilename != "0" && cbofilename != ''){ 
					form.h_filename.value=searchCbofilename;
					form.h_uploadfile.value='false';
				}
			}
		}
		var desclength = form.Txt_SetDesc.value;
		var detldesclebgth = form.Txt_SetDetailDesc.value;
		if(desclength.length > '4000'){
			Error_Details(message_prodmgmnt[137]);
		}
		if(detldesclebgth.length > '4000'){
			Error_Details(message_prodmgmnt[138]);
		}			
		if (ErrorCount > 0){
			Error_Show();
			Error_Clear();
			return false;
		}
	}	
	if(document.all.coreSetFl.checked){
		document.all.coreSetFl.value = "Y";
	}else{
		document.all.coreSetFl.value = "";
	}
	//PMT-42618 
	if(document.all.Chk_Lottrack.checked){
		document.all.Chk_Lottrack.value = "Y";
	}else{
		document.all.Chk_Lottrack.value = "";
	}
	//PMT-42618 
	form.action = "/GmSetMasterServlet?hForward=DIVCONTAINER&hOpt=SET&Cbo_SetId="+setID;
	form.hAction.value = val;
	//Without compatibility mode form not present, so here setting the enctype
	form.setAttribute('enctype','multipart/form-data');
	
	fnStartProgress('Y');	
	form.submit();
	
	
	
	
}

function fnAddNew(form){
	//form = document.getElementsByName('frmVendor')[1];
	parent.document.getElementById("Cbo_SetId").value = 0;
	form.hAction.value = 'Load';
	form.action = "/GmSetMasterServlet?hForward=DIVCONTAINER&hAction=Load&hOpt=SET";
	form.submit();
  	}

function fnVoidSet(form){
	//var form = document.getElementsByName('frmVendor')[1];
	var txnId =form.hTxnId.value ;
		form.action ='/GmCommonCancelServlet?hTxnName='+txnId+'&hCancelType=VDSET&hTxnId='+txnId;
		form.hAction.value = "Load";
		form.submit();
}
function fnSetFileReport(form){
	var setid = document.getElementById("hSetId").value;
	windowOpener("/GmSetMasterServlet?hAction=SETFILELIST&hReport=Y&hOpt=SET&hScreenType=Hyperlink&Cbo_SetId="+setid,"InspectionSheet","resizable=yes,scrollbars=yes,top=150,left=150,width=865,height=560");	
}

function fnUpdateTabIndex(link){
	var linkObj = link.getElementsByTagName('a')[0];
	document.all.hcurrentTab.value = linkObj.name;
}

function fnShowFilters(val){
	var obj = eval("document.all."+val);
	var obj1 = eval("document.all."+val+"img");
	if (obj.style.display == 'none'){
		obj.style.display = 'block';
		if(obj1){
			obj1.src = '/images/minus.gif';
		}
	}else{
		obj.style.display = 'none';	
		if(obj1){
			obj1.src = '/images/plus.gif';
		}
	}
}
//The Below Function is added as Part of OP-459. To Validate the Division name Selected for a Set .
//The Validation is done for the division id in the drop down to the division id mapped for the Project. 
function fnValidateDivision(){
	
	var divid =document.getElementById("Cbo_Division").value;//Division Id selected in drop down
	var projid = document.getElementById("Cbo_Proj").value;//Project Id for the Selected Set.
	var divmapsize = document.getElementById("divisionmapsize").value;
	for(var i=0;i<divmapsize;i++){
		//arr = eval("DivisionMapArr"+i);
		arr = DivisionMapArr[i];
		
		//arrobj = arr.split(",");
		arrDivid = arr[0];//Division Id Associated for Project in that Array.
		arrDivname = arr[1];//Division Name Associated for Project in that Array.
		arrProjid = arr[2];//Project Id in the Array.
		arrProjname = arr[3];//Project Name in the Array.
		
		if(projid == arrProjid){	
			if(divid != arrDivid){
				
				Error_Details(Error_Details_Trans(message_prodmgmnt[139],arrDivname));
			}else{
					return true;
			}
				
		}
	}
	
}

function fnDivMap(){
	if(document.getElementById("Cbo_Division") != undefined){
	var divid =document.getElementById("Cbo_Division").value;//Division Id selected in drop down
	}
	if(document.getElementById("Cbo_Proj") != undefined){
	var projid = document.getElementById("Cbo_Proj").value;//Project Id for the Selected Set.
	}
	if(document.getElementById("divisionmapsize") != undefined){
	var divmapsize = document.getElementById("divisionmapsize").value;
	}
	for(var i=0;i<divmapsize;i++){
		arr = DivisionMapArr[i];
		
		arrDivid = arr[0];//Division Id Associated for Project in that Array.
		arrDivname = arr[1];//Division Name Associated for Project in that Array.
		arrProjid = arr[2];//Project Id in the Array.
		arrProjname = arr[3];//Project Name in the Array.
		
		if(projid == arrProjid){
			document.getElementById("Cbo_Division").value = arrDivid;
			document.getElementById("Cbo_Division").disabled = true;
		}
	}
	
	
}
//Function will call on page load. It will append companyInfo in each tab href and initiate ajaxtab.
//function fnAjaxTabLoad(){
	//fnAjaxTabAppendCmpInfo("#maintab");
	//fnMainTab();
//}

//initiate ajaxtab
function fnMainTab(){
	var maintab=new ddajaxtabs("maintab", "ajaxdivcontentarea");
	var haction = "<%=strhAction%>";
	maintab.setpersist(false);								
	maintab.init();	
	maintab.onajaxpageload=function(pageurl)
    {
    	if (pageurl.indexOf("GmSetMasterServlet")!=-1){
    		fnAddCompanyParams();
	    	if(document.getElementById("ajaxdivcontentarea") != undefined){
  				document.getElementById("ajaxdivcontentarea").style.height = "auto";
			}
    	}
    	fnHtmlEditor();
    	if(haction == "Reload"){
    		fnDivMap();
    	}
    }	
}

// Funtion to validation the Set/BOM name
function fnValidateSetBOMName(form){
	var setId = document.getElementById('Txt_SetId').value;
	var hOpt = document.getElementById('hOpt').value;
	hOpt = (hOpt =="SET"||hOpt == "SETMAP")?"1601":"1602";
	// if the SET/BOM id is there, then validate 
	if(setId != ''){
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/GmSetMasterServlet?hAction=validateSet&setId='+setId+'&hOpt='+hOpt);
		fnStartProgress();
		dhtmlxAjax.get(ajaxUrl,function(loader){
			response = loader.xmlDoc.responseText;
			if(response != null && response != ''){
				document.getElementById('Cbo_SetId').value = setId;
				fnSetFetch(form);
				document.getElementById('searchCbo_SetId').value = response;
			}else{
				if(hOpt==1601){	// Set Master Setup
	   				document.frmVendor.searchCbo_SetId.value ='';
	    		    Error_Details(message_prodmgmnt[469]);
	    		}
	    	    else{
	    		   document.frmVendor.searchCbo_SetId.value ='';
	    		   Error_Details(message_prodmgmnt[470]);
	    		}
			}
			if (ErrorCount > 0) {
	    		fnStopProgress();
	   			Error_Show();
	   			Error_Clear();
	   			return false;
	   		} 
		});
	}
	fnStopProgress();
}
