//As of now we are using one HTML editor which will be placed in seperate Iframe (Index:0). 
//So the Group part details Iframe (Index:1) is second one.To get values set the index is 1.
var grpPartDtlIframeIndex=1;

function fnReset()
{
	//var form = document.getElementsByName('frmGroupPartMapping')[1]; //To get the values from Group Part Detail container 
	document.getElementById("setID").value = '0';
	document.getElementById("groupName").value = '';
	document.getElementById("groupType").value = '0';
	document.getElementById("activeflag").checked = false;
} 	

function fnFetch(form)
{	
	var currentTab = form.hcurrentTab.value;
	if(currentTab == 'partdetails'){// Reset is only for Detail tab
		fnReset(); 
	}
	form.action = "/gmGroupPartMap.do?haction=edit";
	//form.haction.value = 'edit';
	form.strForward.value = 'fetchcontainer';
	fnStartProgress();
	form.submit();
}



function fnEditId(val)
{	
	document.frmGroupPartMapping.haction.value = 'edit';
	document.frmGroupPartMapping.hvendCatMapId.value = val;
	document.frmGroupPartMapping.submit();
}

function fnSubmit(form){
	
	if(!form.activeflag.checked) 
	form.hactiveflag.value =  'true';
	else form.hactiveflag.value ="false";
	
	var actionval = form.haction.value;
	var groupName = form.groupName.value;

	var groupid = form.groupId.value;
	var validgrpnm = document.all.hvalidgrpnm.value;
	var index = document.frmGroupPartMapping.groupId;
    var gp_name= form.groupName.value;//value of selected group name.
  
	rowcount = document.getElementById('PartListFrame').contentWindow.fnCheckPartRows();
	// alert("rowcount   is "+ rowcount);  
 	var primaryPartChecks = ''; 
 	if(rowcount == 0)
 	{	 
 		Error_Details(message[5198]); //Changed alert message
 		Error_Show();
		Error_Clear();
		return false;
	}
	
 	if(rowcount >1)
	{	  
	    objCheckPartArr=document.getElementById('PartListFrame').contentWindow.document.all.checkPartNumbers; 
	     
		if(objCheckPartArr)
		{ 
			primaryPartChecks =  document.getElementById('PartListFrame').contentWindow.fnCheckPrimaryPartSelect();
		 
  		  }
    }
    else  {	 
    
    	primaryPartChecks = document.getElementById('PartListFrame').contentWindow.fnCheckPrimaryPartSelect();
    		
    		}

	fnValidateTxtFld('groupName',message[5190]); 
	fnValidateDropDn('setID',message[5191]); 
	fnValidateDropDn('groupType',message[5192]); 
	fnValidateDropDn('pricedType',message[5193]); 
	fnLenValidation(form.groupDesc,message[5194],"4000");// to validate Group Description field for 4000 characters
	//fnLenValidation(form.detailDesc," Detailed Description","4000"); // to validate Detailed Description field for 4000 characters
	//The below validation is added for PMT-4918.If The Group Name is already exist ,it needs to throw the below message. 
	if(validgrpnm != '0' && validgrpnm != ''){
		Error_Details(message[5195]);
	}
	if(primaryPartChecks.length >= 4000 )
	{
	   Error_Details(message[5196]);
	}
	if (ErrorCount > 0)
	 {
		Error_Show();
		Error_Clear();
		return false;
	}
	
	//MNTTASK-6106. When should save, enable GroupName and Priced Type. 
	form.groupName.disabled = false;
	form.pricedType.disabled = false;
	
	form.haction.value =  'save'; //'save';
	form.hinputPrimaryParts.value = primaryPartChecks;
	objCheckedParts = document.getElementById('PartListFrame').contentWindow.document.all.checkPartNumbers;
	var inputPartChecks='';
	var objCheckedPartsArrLen =0;
	
	 
	 if(rowcount > 1)
 	{
 		objCheckedPartsArrLen = objCheckedParts.length;
		for (var k=0; k < objCheckedPartsArrLen; k++) 
			{ // alert("checked value    "+objCheckedParts[k].checked);
				if( objCheckedParts[k].checked == true)
				 	{ 
				 		if(k < objCheckedPartsArrLen -1)
				 		{
					    	inputPartChecks = inputPartChecks + objCheckedParts[k].value +',';
					    	}
					    else 
					    {
					    	inputPartChecks = inputPartChecks + objCheckedParts[k].value;
					    	}			    		 
				 	}
			}
		}
	else{  
			if(objCheckedParts.checked)
				{
				inputPartChecks = objCheckedParts.value;
			 }
		}
		
	//alert("inputPartChecks    "+inputPartChecks);
	form.inputString.value = inputPartChecks;	
	//form.strForward.value = 'default';
	form.strOpt.value = '40045'; // To set strOpt value, 40045:Forecast/Demand Group
	
	form.Btn_Submit.disabled=true; // To disable the Submit button
	fnStartProgress();
	form.submit();

}




function fnVoid(form)
{
		fnValidateDropDn('groupId',message[5197]); // 
		returnval = document.getElementById('PartListFrame').contentWindow.fnVoidValidateCheck();
		if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}

		if(returnval== true)
	{
			form.action ="/GmCommonCancelServlet";
			form.hTxnId.value = document.frmGroupPartMapping.groupId.value;
			form.hTxnName.value = document.frmGroupPartMapping.groupName.value;
			form.submit();
	}
}	

//To limit the no. of characters entering into the text area
function fnLenValidation(obj,name,len){ 
	if(obj.value.length > len){
		var arrmsg = [len,name]
		Error_Details(Error_Details_Trans(message[10523],arrmsg));
	}
}

//To assign the tab id to hidden field, to know which tag is currently open
function fnUpdateTabIndex(link) {
	var linkObj = link.getElementsByTagName('a')[0];
	document.all.hcurrentTab.value = linkObj.id;
}
//Function to toggle the header section.
function fnShowFilters(val){
	var obj = eval("document.all."+val);
	var obj1 = eval("document.all."+val+"img");
	
	if (obj.style.display == 'none'){
		obj.style.display = 'block';
		if(obj1){
			obj1.src = '/images/minus.gif';
		}
	}
	else{
		obj.style.display = 'none';	
		if(obj1){
			obj1.src = '/images/plus.gif';
		}
	}
}
//The below functions are added for PMT-4918 to Validate Group Name.
 
function fnCheckGroupNm(val,form){
	
	var groupName = form.groupName.value;//here we are getting the current value of group name.
	var validName = eval("document.all.validateName");
	
	//var index = document.frmGroupPartMapping.groupId;
	// to get the Auto complete text box value
	
	var selected_gp_name= parent.document.all.searchgroupId.value; //value of selected group name.

    /*when we select the existing group id ,and for this we are not updating the group name at this place 
     * image need should not be displayed.
     */
    validName.style.display = 'none';
	document.all.hvalidgrpnm.value = '0';
	if(selected_gp_name != groupName){
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmGroupPartMap.do?&haction='+ encodeURIComponent(val)+'&groupName='+ groupName +'&ramdomId=' + Math.random());
	dhtmlxAjax.get(ajaxUrl,fnDisplayGroupMsg);
	}
}
//check the Group Name. 
function fnDisplayGroupMsg(loader) {
	var response = loader.xmlDoc.responseText;
	if (response != null) {
		var validName = eval("document.all.validateName");
		if (response == '0') {
				validName.style.display = 'block';
				validName.src = '/images/success.gif';
				validName.alt = 'The group name is available';
			}else{
				validName.style.display = 'block';
				validName.src = '/images/delete.gif';
				validName.alt = 'The group name already exist';
			}
		}
	document.all.hvalidgrpnm.value = response;	
}

function fnCallOnLoad(){
	var groupName = document.frmGroupPartMapping.groupName.value;
	    
	 if(parent.document.frmGroupPartMapping.groupId != undefined){
	        parent.document.all.searchgroupId.value =groupName;   
	   }
	  
 }

