/***********************************************************************************
 * Party Setup Container
 ***********************************************************************************/

var gridObj;
//Initiating grid when on page loading 
function fnOnPageLoad(){
if(parent.window.document.getElementById("ajaxdivcontentarea") != undefined){
  	parent.window.document.getElementById("ajaxdivcontentarea").style.height = "690";
}
if(objGridData.value != ''){ 
		gridObj = initGridData('dataGridDiv',objGridData);
}
if (objGridData.indexOf("cell", 0) != -1) {
	document.getElementById("dataGridDiv").style.height = "500px";
}else{
	document.getElementById("dataGridDiv").style.height = "10";
	document.getElementById("ShowDetails").style.color = "red";
	document.getElementById("ShowDetails").style.height = "15";
	document.getElementById("ShowDetails").innerHTML = message_prodmgmnt[7];
}

}
//Initiating the grid
function initGridData(divRef,gridData){
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.attachHeader("#select_filter,#text_filter,#select_filter,#select_filter,#select_filter");
	gObj.init();
	gObj.loadXMLString(gridData);	
	return gObj;	
}

//Function to export excel
function fnExport()
{	
	gridObj.toExcel('/phpapp/excel/generate.php');
}

function fnUpdateTabIndex(link) {
	var linkObj = link.getElementsByTagName('a')[0];
	document.all.hcurrentTab.value = linkObj.name;
}

function fnPrjHelp(wikiPath){
	var currentTab = document.all.hcurrentTab.value;
	var wikiTitle = "";
	if(currentTab == "Project") {
		wikiTitle = "Project_Setup";
	} else if(currentTab != "Project_Setup" && 
			  currentTab != "Milestones" && 
			  currentTab != "Staffing Requirements" &&
			  currentTab != "Staffing Details") 
	{
		wikiTitle = currentTab + "_Setup";
	} else { 
		wikiTitle = currentTab;
	}
	//alert("wikiTitle = " + wikiTitle);
	var wikiPage = wikiPath+"/"+wikiTitle;
	windowOpener(wikiPage,"Prntwiki","resizable=yes,scrollbars=yes,top=250,left=50,width=950,height=650");	
}

function fnPrjFetch(form) {	
	form.strOpt.value = 'fetchcontainer';
	form.submit();
}

function fnPrjReset(form) {
	form.action = "/gmPDProjectSetup.do?method=loadProjectInformation&strOpt=fetchcontainer&prjId=''";
	form.submit();
}

function fnPrjSubmit(form)
{
	var prjid = document.getElementById("prjId").value;//form.prjId.value;
	var prjtype = document.getElementById("prjType").value;
	
	if(prjtype == '' || prjtype == '0'){
		Error_Details(message_prodmgmnt[62]);
	}
	
	//fnValidateDropDn('prjId',' Project List ');
	fnValidateTxtFld('prjNm',message_prodmgmnt[319]);
	fnValidateTxtFld('projectID',message_prodmgmnt[345]);
	fnValidateDropDn('prjType',message_prodmgmnt[321]); //verify why not working
	fnValidateDropDn('divisionId',message_prodmgmnt[320]);
	fnValidateDropDn('prjIniBy',message_prodmgmnt[322]);
	fnValidateTxtFld('txt_LogReason',message_prodmgmnt[146]);
	fnLenValidation(form.prjDesc,message_prodmgmnt[323],"4000");
	fnLenValidation(form.prjDtlDesc,message_prodmgmnt[324],"4000");

	var projectStatusId=form.prjStatusId.value;
	if(projectStatusId=='20300'){  //when the status is initiated, we need to validate the Approved by dropdown. 		
		var approve = confirm(message_prodmgmnt[325]);
		if(approve == true)
		{
			fnValidateDropDn('prjAprvdBy',message_prodmgmnt[326]);
			form.prjStatusId.value='20301';
			form.prjIniBy.disabled=false;
		}else{
			return false;
		}
	}

	var deviceGrpId = form.deviceClassGrpId.value;
	var groupGrpId = form.groupGrpId.value;
	var segmentGrpId = form.segmentGrpId.value;
	var techniqueGrpId = form.techniqueGrpId.value;
	
	var devicelen = '';
	var grouplen = '';
	var segmentlen = '';
	var techlen = '';
	var devicestr = '';
	var groupstr = '';
	var segmentstr = '';
	var techniquestr = '';
	var deviceInputStr = '';
	var groupInputStr = '';
	var segmentInputStr = '';
	var techniqueInputStr = '';
	
	var objDevice = form.Chk_GrpDevice;
	var objGroup = form.Chk_GrpGroup;
	var objSegment = form.Chk_GrpSegment;
	var objTech = form.Chk_GrpTechnique;
	
	if (objDevice){
		devicelen = objDevice.length;
	}
	if (objGroup){
		grouplen = objGroup.length;
	}
	if (objSegment){
		segmentlen = objSegment.length;
	}
	if (objTech){
		techlen = objTech.length;
	}
	
	if (objDevice)
	{	
		for (var i=0;i<devicelen;i++)
		{
			if (objDevice[i].checked)
			{
				devicestr = devicestr + objDevice[i].value + ',';
				deviceInputStr = deviceInputStr + deviceGrpId + '^' + objDevice[i].value + '|';
			}
		}
		devicestr = devicestr.substr(0,devicestr.length-1);
		form.hDeviceIds.value = devicestr;
	}
	
	if (objGroup)
	{	
		for (var i=0;i<grouplen;i++)
		{
			if (objGroup[i].checked)
			{
				groupstr = groupstr + objGroup[i].value + ',';
				groupInputStr = groupInputStr + groupGrpId + '^' + objGroup[i].value + '|';
			}
		}
		groupstr = groupstr.substr(0,groupstr.length-1);
		form.hGroupIds.value = groupstr;
	}
	if(groupstr == ''){
		Error_Details(message_prodmgmnt[307]);
	}
	if (objSegment)
	{	
		for (var i=0;i<segmentlen;i++)
		{
			if (objSegment[i].checked)
			{
				segmentstr = segmentstr + objSegment[i].value + ',';
				segmentInputStr = segmentInputStr + segmentGrpId + '^' + objSegment[i].value + '|';
			}
		}
		segmentstr = segmentstr.substr(0,segmentstr.length-1);
		form.hSegmentIds.value = segmentstr;
	}
	if(segmentstr == ''){
		Error_Details(message_prodmgmnt[308]);
	}
	if (objTech)
	{	
		for (var i=0;i<techlen;i++)
		{
			if (objTech[i].checked)
			{
				techniquestr = techniquestr + objTech[i].value + ',';
				techniqueInputStr = techniqueInputStr + techniqueGrpId + '^' + objTech[i].value + '|';
			}
		}
		techniquestr = techniquestr.substr(0,techniquestr.length-1);
		form.hTechniqueIds.value = techniquestr;
	}
	if(techniquestr == ''){
		Error_Details(message_prodmgmnt[309]);
	}
	var projectStatusId=form.prjStatusId.value;
	if(projectStatusId=='20300'){  //when the status is initiated, we need to validate the Approved by dropdown. 		
		fnValidateDropDn('prjAprvdBy',message_prodmgmnt[330]);
	}
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	
	form.projAttrInputStr.value =  deviceInputStr + groupInputStr + segmentInputStr + techniqueInputStr;
	form.attrTypeInputStr.value = deviceGrpId + ',' + groupGrpId + ',' + segmentGrpId + ',' + techniqueGrpId;
	//form.strOpt.value = 'save';
	form.prjIniBy.disabled=false;
	form.prjAprvdBy.disabled=false;
	// to enable the project id field
	form.projectID.disabled=false;
	fnStartProgress('Y');
	form.action = '/gmPDProjectSetup.do?method=loadProjectInformation&strOpt=save&prjId='+prjid;
	form.submit();
}

function fnApproveProject(form)
{
	var approve = confirm(message_prodmgmnt[331]);
	if(approve == true)
	{
		fnValidateDropDn('prjAprvdBy',message_prodmgmnt[330]);
		if (ErrorCount > 0)
		{
			Error_Show();
			Error_Clear();
			return false;
		}
		form.strOpt.value='approve';
		form.prjStatusId.value='20301';
		form.prjIniBy.disabled=false;
		form.submit();
	}
}

function fnChangePrjStatus(form)
{
	var prjId = document.getElementById("prjId").value;
	if(prjId == 0) {
		Error_Details(message_prodmgmnt[63]);
	}
    if (ErrorCount > 0) {
		Error_Show();
        Error_Clear();
        return false;
    }
    form.action ="/GmCommonCancelServlet?REASONLABEL="+message_prodmgmnt[441];
    form.hTxnId.value = prjId;
    form.hTxnName.value = message_prodmgmnt[332] + prjId; 
    form.submit();
}

function fnOpenLog(id, type)
{
	windowOpener("/GmCommonLogServlet?hType="+type+"&hID="+id,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
}


/* 
 * Common function to generate the request string
 * @param form - the form object
 * @param params - an array of parameter names which should not be included in the request string
 */
function fnGenerateRequestString(form, params) {
	var requestStr = new String(form.action + '?');
	for (var i=0;i<form.length;i++) {
		var obj = form.elements[i];
		var objName = obj.name;
		var objType = obj.type;
		var objValue = obj.value;
		//alert(form.elements[i].name + ' = ' + form.elements[i].type);
		//alert(objName + ' present = ' + fnCheckExists(objName, params));
		if(fnCheckExists(objName, params)) {
			continue;
		}
		
		if(objType == 'checkbox')
		{
			if(obj.checked == true) { objValue = 'on' } else { objValue = 'off' }
			//alert(objName + ' = ' + objValue);
		}
		// JBOSS compatible change
		if(objValue != '' && objType != 'button'){
			requestStr += '&'+ objName + '=' + escape(objValue);
		}
	}
	requestStr = requestStr.replace("?&","?");
	return requestStr;
}

function fnCheckExists(value, params) {
	if(params != null) {
		for(var k=0;k<params.length;k++) {
			if(value == params[k]) {
				return true;
			}
		}
		return false;
	} else {
		return false;
	}
}
/***For Project Milestone*****/
function fnMileStoneReset(form) {
	/*
	var projectId= form.prjId.value;
	var form = document.getElementsByName('frmPdPrjMilestoneSetup')[0];
	var params = new Array("prjId");
	var requestStr = fnGenerateRequestString(form, params);
	requestStr += '&prjId='+projectId;
	//alert(requestStr);
	loadajaxpage(requestStr, 'ajaxdivcontentarea');
	*/
	form.submit();
}
function fnMilestoneSubmit()
{
	var chk_length1 =  document.frmPdPrjMilestoneSetup.projectedDate.length;
	var chk_length2 =  document.frmPdPrjMilestoneSetup.actualDate.length;
	var form = document.getElementsByName('frmPdPrjMilestoneSetup')[0];
	var params = new Array("strOpt","hinputString");
	var requestStr = fnGenerateRequestString(form, params);
	var inputString = "";
	
	if(chk_length1 == undefined)
	{
		var val = 1;
		
		objDate1 = eval("document.frmPdPrjMilestoneSetup.prjDt"+val);		
		objDate2 = eval("document.frmPdPrjMilestoneSetup.actDt"+val);	
		
		if(objDate1.value !="" || objDate2.value != "")
		{
			inputString = inputString + document.frmPdPrjMilestoneSetup.projectedDate.value + "^" +objDate1.value+ "@" +objDate2.value+"|";
		}			
	}
	
	for(var n=0;n<chk_length1;n++)
	{
		var val = n+1;
		objDate1 = eval("document.frmPdPrjMilestoneSetup.prjDt"+val);	
		objDate2 = eval("document.frmPdPrjMilestoneSetup.actDt"+val);		
		DateValidateMMDDYYYY(objDate1,message_prodmgmnt[333]);	//DateValidateMMDDYY(objDate1.value, 'Invalid value entered for <B>Projected Date</B>');
		DateValidateMMDDYYYY(objDate2,message_prodmgmnt[334]);	//DateValidateMMDDYY(objDate2.value, 'Invalid value entered for <B>Actual Date</B>');		
		
		//alert('objDate2 = ' + objDate2.value);
		//alert('objDate2.length = ' + objDate2.length);
		if(objDate2.value != undefined && objDate2.value != "")
		{
			//alert('objDate2.value = ' + objDate2.value);
			var dateVals = objDate2.value.split("/");
			var actDate = new Date();
			var currDate = new Date();

			dateVals[0] = dateVals[0].replace("0", "");
			dateVals[1] = dateVals[1].replace("0", "");
	
			//alert('day = ' + parseInt(dateVals[1]));
			//alert('month = ' + parseInt(dateVals[0]) - 1);
			//alert('year = ' + parseInt(dateVals[2]));
			
			actDate.setDate(parseInt(dateVals[1]));
			actDate.setMonth(parseInt(dateVals[0]) - 1);
			actDate.setFullYear(parseInt(dateVals[2]));
			
			//alert('actDate = ' + actDate.toString());
			//alert('currDate = ' + currDate.toString());

			if(actDate > currDate)
			{
				Error_Details(message_prodmgmnt[64]);
			}
		}		
		if(objDate1.value !="" || objDate2.value != "")
		{
			inputString = inputString + document.frmPdPrjMilestoneSetup.projectedDate[n].value + "^" +objDate1.value+ "@" +objDate2.value+"|";
		}		
	}
	if(inputString == "")
	    {
	      Error_Details(message_prodmgmnt[65]);
	    }
		if (ErrorCount > 0)
		 {
			Error_Show();
			Error_Clear();
			return false;
		}	
	
	document.frmPdPrjMilestoneSetup.hinputString.value = inputString;
	document.frmPdPrjMilestoneSetup.strOpt.value =  'save'; 
	fnStartProgress('Y');
	form.submit();
	/*requestStr += '&strOpt=save&hinputString='+inputString;
	loadajaxpage(requestStr, 'ajaxdivcontentarea');
	*/
}

function fnReqHistoryPrjDt(id)
{
	windowOpener("/GmPageControllerServlet?strPgToLoad=/gmAuditTrail.do&auditId=1009&txnId="+id,"","resizable=yes,scrollbars=yes,top=250,left=300,width=785,height=300,status=1");
 }

 function fnReqHistoryActDt(id)
{
	windowOpener("/GmPageControllerServlet?strPgToLoad=/gmAuditTrail.do&auditId=1010&txnId="+id,"","resizable=yes,scrollbars=yes,top=250,left=300,width=785,height=300,status=1");
 }

/***End****/

/***For Staffing Count*****/
function fnStaffingCntReset(form)
{
	
	/*var projectId= form.prjId.value;
	var form = document.getElementsByName('frmStaffingDetails')[0];
	var params = new Array("prjId");
	var requestStr = fnGenerateRequestString(form, params);
	requestStr += '&prjId='+projectId;
	//alert(requestStr);
	loadajaxpage(requestStr, 'ajaxdivcontentarea');
	*/
	form.submit();
}

function fnStaffingCntSubmit()
{
	var chk_length =  document.frmStaffingDetails.staffCount.length;
	var form = document.getElementsByName('frmStaffingDetails')[0];
	var params = new Array("strOpt","hinputString");
	var requestStr = fnGenerateRequestString(form, params);
	var inputString = "";
	
	if(chk_length == undefined)
	{
		var val = 0;
		objDate = eval("document.frmStaffingDetails.staffingCnt"+val);
		NumberValidation(objDate.value,message_prodmgmnt[335], 0);
		if(objDate.value!="")
		{
			inputString = inputString + document.frmStaffingDetails.staffCount.value + "^" +objDate.value+"|";
		}
	}
	for(var n=0;n<chk_length;n++)
	{
		var val = n;
		objDate = eval("document.frmStaffingDetails.staffingCnt"+val);	
		NumberValidation(objDate.value,message_prodmgmnt[335], 0);
		if(objDate.value!="")
		{
			inputString = inputString + document.frmStaffingDetails.staffCount[n].value + "^" +objDate.value+"|";
		}
	}
	if(inputString == "")
	    {
	      Error_Details(message_prodmgmnt[65]);
	    }
		if (ErrorCount > 0)
		 {
			Error_Show();
			Error_Clear();
			return false;
		}	
	//alert(inputString);
	document.frmStaffingDetails.hinputString.value = inputString;
	document.frmStaffingDetails.strOpt.value =  'save'; 
	fnStartProgress('Y');
	form.submit();
	//requestStr += '&strOpt=save&hinputString='+inputString;
	//loadajaxpage(requestStr, 'ajaxdivcontentarea');

}

/***End****/
/***For Staffing Detail*****/
function fnLoadTeamRec(form)
{
	form.action = '/gmPDStaffingDetail.do';
	var params = new Array("strOpt","method");
	var requestStr = fnGenerateRequestString(form, params);
	requestStr += '&strOpt=reload&method=loadStaffingDetailsSetup';
	//alert(requestStr);
	loadajaxpage(requestStr, 'ajaxdivcontentarea');
//	form.submit();
}

function fnPrjTeamSubmit(form)
{
	fnValidateDropDn('partyType',message_prodmgmnt[336]);
	fnValidateDropDn('partyId',message_prodmgmnt[337]);
	if (ErrorCount > 0)
		 {
			Error_Show();
			Error_Clear();
			return false;
		}
	//var form = document.getElementsByName('frmStaffingDetails')[0];
	form.action = '/gmPDStaffingDetail.do';
	var params = new Array("strOpt");
	var requestStr = fnGenerateRequestString(form, params);
	requestStr += '&strOpt=save&method=loadStaffingDetailsSetup';
	//alert(requestStr);
	loadajaxpage(requestStr, 'ajaxdivcontentarea');
}

function fnEdtId(seq)
{
	
	var form = document.getElementsByName('frmProjectDetails')[0];
	var params = new Array("strOpt","hstaffingdetailsId","method");
	var requestStr = fnGenerateRequestString(form, params);
	requestStr += '&strOpt=edit&method=loadStaffingDetailsSetup&hstaffingdetailsId='+seq;
	loadajaxpage(requestStr, 'ajaxdivcontentarea');
}

function fnStaffReset(form) {
	/*form.strOpt.value = '';
	form.partyType.value = 0;
	form.submit();*/
	//var form = document.getElementsByName('frmStaffingDetails')[0];
	var params = new Array("partyType","method");
	var requestStr = fnGenerateRequestString(form, params);
	requestStr += '&partyType=0&method=loadStaffingDetailsSetup';
	//alert(requestStr);
	loadajaxpage(requestStr, 'ajaxdivcontentarea');
}
function fnVoidStaff(form)
{
	form.action ="/GmCommonCancelServlet";
	form.hTxnId.value = form.hstaffingdetailsId.value;	
	form.hTxnName.value = form.partyType.options[form.partyType.selectedIndex].text+ ' - ' + form.partyId.options[form.partyId.selectedIndex].text;  	
	form.hCancelType.value = 'VSTAF';			
	form.submit();

}

/***End****/

/***Reports related functions****/

function fnViewPartySummary(partyId, partyType)
{
	window.location.href="gmPartySummary.do?partyId="+partyId+"&partyType="+partyType;
}

function fnShowProjectSetup(val)
{
	window.location.href="/gmPDProjectSetup.do?method=loadProjectInformation&prjId="+val;
}

function fnViewProjects(partyId)
{
	windowOpener('/gmPDStaffingDetail.do?method=loadProjectsByParty&partyId='+partyId,'Projects','resizable=yes,scrollbars=yes,top=20,left=20,width=600,height=250');
}

function fnStaffingReportSubmit() 
{
	document.frmStaffingDetails.strOpt.value = 'fetch';
	fnStartProgress('Y');
	document.frmStaffingDetails.submit();
}

function fnViewProjectMembers(projectId, partyType) {
	windowOpener('/gmPDStaffingDetail.do?method=loadStaffingDetailsReport&TYPE=Report&prjId='+projectId+'&partyType='+partyType,'Team','resizable=yes,scrollbars=yes,top=10,left=10,width=500,height=200');
}

function fnShowProjectsReport() {
	fnCrossTabSubmit();
	document.frmProjectsReport.strOpt.value = 'fetch';
	fnStartProgress('Y');
	document.frmProjectsReport.submit();
}

function fnSelectMilestones() {
	document.frmProjectsReport.strOpt.value = 'reload';
	document.frmProjectsReport.submit();
}

function fnShowFilters(val){
	var obj = eval("document.all."+val);
	var obj1 = eval("document.all."+val+"img");
	
	if (obj.style.display == 'none'){
		obj.style.display = 'block';
		if(obj1){
			obj1.src = '/images/minus.gif';
		}
	}
	else{
		obj.style.display = 'none';	
		if(obj1){
			obj1.src = '/images/plus.gif';
		}
	}
}

function fnCheckProjectAttributes()
{
	var form = document.getElementsByName('frmProjectDetails')[0];
	//var form2 = document.getElementsByName('frmProjectDetails')[1];
	var prjid = form.prjId.value;
	
	var objgrp = '';
	if (prjid != "" && prjid != "0"){
		if(form)
		{
			
			var deviceids = document.getElementById("hDeviceIds").value;
			var groupids = document.getElementById("hGroupIds").value;
			var segmentids = document.getElementById("hSegmentIds").value;
			var techniqueids = document.getElementById("hTechniqueIds").value;
			
			if (deviceids != '')
		 	{
		 		objgrp = document.getElementsByName("Chk_GrpDevice");
		 		fnCheckSelections(deviceids,objgrp);
		 	}
			
			if (groupids != '')
		 	{
		 		objgrp = document.getElementsByName("Chk_GrpGroup");
		 		fnCheckSelections(groupids,objgrp);
		 	}
			if (segmentids != '')
		 	{
		 		objgrp = document.getElementsByName("Chk_GrpSegment");
		 		fnCheckSelections(segmentids,objgrp);
		 	}
			if (techniqueids != '')
		 	{
		 		objgrp = document.getElementsByName("Chk_GrpTechnique");
		 		fnCheckSelections(techniqueids,objgrp);
		 	}
		}
	}
}


function fnCheckSelections(val,objgrp)
{
	var valobj = val.split(",");
	var arrlen = objgrp.length;
			
	if (arrlen > 0)
	{
		for (i=0;i<valobj.length ;i++ )
		{
			for (j=0;j<arrlen ;j++ )
			{
				if (valobj[i] == objgrp[j].value)
				{
						objgrp[j].checked = true;
				}
			}
		}
	}
}

//To limit the no. of characters entering into the text area
function fnLenValidation(obj,name,len){ 
	if(obj.value.length > len){
		Error_Details(Error_Details_Trans(message_prodmgmnt[66],name));
	}
}

//Function will call on page load. It will append companyInfo in each tab href and initiate ajaxtab.
function fnAjaxTabLoad(){
	fnAjaxTabAppendCmpInfo("#maintab");
	fnMainTab();
}

//initiate ajaxtab
function fnMainTab(){
	var maintab=new ddajaxtabs("maintab", "ajaxdivcontentarea");				
	maintab.setpersist(false);								
	maintab.init();					    
	
	maintab.onajaxpageload=function(pageurl){					    
		if (pageurl.indexOf("gmProfileSetup.do")!=-1){					    
			var profiletab=new ddajaxtabs("profiletab", "profiledivcontentarea");
			profiletab.setpersist(false);	
			profiletab.setselectedClassTarget("link") //"link" or "linkparent"
			profiletab.init();				
		}
		if(pageurl.indexOf("gmPDProjectSetup.do")!=-1){	
			fnAddCompanyParams();
			fnCheckProjectAttributes();
			if(document.getElementById("ajaxdivcontentarea") != undefined){
	  			document.getElementById("ajaxdivcontentarea").style.height = "auto";
			}
		}
		if(pageurl.indexOf("gmPDStaffingDetail.do")!=-1){
			if(document.getElementById("ajaxdivcontentarea") != undefined){
	  			document.getElementById("ajaxdivcontentarea").style.height = "auto";
			}
		}
		fnHtmlEditor();
	}	
}	

//To clear the Project Id validation images.
function fnPRJIDClearDiv(){	
		document.getElementById("DivShowPRJIDExists").style.display='none';
	    document.getElementById("DivShowPRJIDAvail").style.display='none';
}
var projectID='';

//To Validate Project Id
function fnCheckProjectID(form){  
		 projectID =form.projectID.value;
		if(TRIM(projectID) == ''){
			return false;
		}
		var ajaxUrl = fnAjaxURLAppendCmpInfo("/gmPDProjectSetup.do?method=checkProjectID&prjId="+projectID+"&ramdomId="+Math.random() );
		dhtmlxAjax.get(ajaxUrl, fnShowPRJIDImage);
}

//To open the Project details, when click on "L" icon
function fnLookupProjectInfo(){ 
	windowOpener("/gmPDProjectSetup.do?method=lookupProjectInfo&prjId="+projectID,'OrderDetails',"resizable=yes,scrollbars=yes,top=375,left=320,width=800,height=200");
}

//To show the Project Id field validation images
function fnShowPRJIDImage(loader) { 
	        var xmlDoc = loader.xmlDoc.responseText;
	        if(xmlDoc.indexOf("N")!= -1){
	        	document.getElementById("Btn_Submit").disabled=true;
	        	document.getElementById("DivShowPRJIDAvail").style.display = 'none';
	        	document.getElementById("DivShowPRJIDExists").style.display = 'inline';
	        	
	        	
	        }else if(xmlDoc.indexOf("Y")!= -1){  
	        	document.getElementById("Btn_Submit").disabled=false;
	        	document.getElementById("DivShowPRJIDExists").style.display = 'none';
	        	document.getElementById("DivShowPRJIDAvail").style.display = 'inline';
	        	
	        }
}
// Project Id Field validation 
function fnCheckTxtFldPattern(form)
{
	var letters = /^[0-9a-zA-Z-]+$/; 
	var prjId=form.projectID.value;	
	if(prjId!=""){		
	form.projectID.value=form.projectID.value.toUpperCase();		
	if(!prjId.match(letters))
	{ 
		Error_Details(message_prodmgmnt[344]);
		form.projectID.value='';
		fnPRJIDClearDiv();
		if (ErrorCount > 0)
		 {
			Error_Show();
			Error_Clear();
			return false;
		}		
	return false; 
	} 
	} 
	else
		{
	      fnPRJIDClearDiv();
		}
}