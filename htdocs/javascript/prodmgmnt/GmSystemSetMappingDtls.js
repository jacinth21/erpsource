var gridObj;
var gridSetObj;
var setID = '';
var setName = '';
var projectName = '';
var setType = '';
var baseLineSet = '';
var shared = '';
var comment = '';   // for history popup window
var createdBy = '';   // for history popup window
var createdDate = '';   // for history popup window
var	response = '';

// function used to onPage load for System Set count details
function fnOnPageLoad(){
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmSystemPublish.do?method=fetchSystemSetMappingDtls&setId='+setId+'&regionId='+regionId+'&setGroupType='+strGroupType+'&ramdomId='+ Math.random());  
			dhtmlxAjax.get(ajaxUrl, fnCallBackSysSetDtls);
}

// function used to System Set count details Using Ajax call response
function fnCallBackSysSetDtls(loader) {
	response = loader.xmlDoc.responseText;
	fnStopProgress();
	if (response != "") {
		fnLoadSetCountDetail(loader);
	}else{
			document.getElementById("dataGridDiv").style.display = 'none';
			document.getElementById("DivExportExcel").style.display = 'none';
			document.getElementById("DivNothingMessage").style.display = 'block';
		}
  }

// function used to Load grid data for Set Count details
function fnLoadSetCountDetail(){
	var setHeader = [ "Set ID", "Set Name", "Project Name", "Set Type",
	      			"Baseline Set", "Shared" ];
	var setColIds = "SETID,SETNM,PROJECTNM,SETTYPE,BASELINESET,SHARED";
	var setColTypes = "ro,ro,ro,ro,ro,ro";
	var setColAlign = "left,left,left,center,center,left";
	var setColSorting = "str,str,str,str,str,str";
	var enableTooltips = "true,true,true,true,true,true";
	var setFilter = [ "#text_filter", "#text_filter","#text_filter","#select_filter",
	    			  "#select_filter", "#select_filter"];
	var setInitWidths = "100,250,200,150,150,150";
	var gridHeight = "";
	var dataHost = {};
	format = "Y";
	
	gridSetObj = initGridObject('dataGridDiv');
	gridSetObj = fnCustomTypes(gridSetObj, setHeader, setColIds, setInitWidths,
				setColAlign, setColTypes, setColSorting, enableTooltips, setFilter,
				gridHeight, format);
	gridSetObj = loadDhtmlxGridByJsonData(gridSetObj, response);
	
	document.getElementById("dataGridDiv").style.display = 'block';
	document.getElementById("DivNothingMessage").style.display = 'none';
	document.getElementById("DivExportExcel").style.display = 'block';
}

// function on page load for History icon hyper link
function fnHistoryOnPageLoad(){
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmSystemPublish.do?method=fetchSystemReleaseHistory&setId='+setId+'&regionId='+regionId+'&ramdomId='+ Math.random());
	dhtmlxAjax.get(ajaxUrl, fnCallBackHistory);
}

// Function used to call back history details 
function fnCallBackHistory(loader) {
	response = loader.xmlDoc.responseText;
	fnStopProgress();
	if (response != "") {
		fnLoadHistoryDetail(response);
	}else{
			document.getElementById("dataGridDiv").style.display = 'none';
			document.getElementById("DivExportExcel").style.display = 'none';
			document.getElementById("DivNothingMessage").style.display = 'block';
		}
}

// Function used to layout for grid data to History details
function fnLoadHistoryDetail(response){
	var setHeader = [ "Created By", "Comments", "Created Date"];
	var setColIds = "CREATEDBY,COMMENTS,CREATEDDATE";
	var setColTypes = "ro,ro,ro";
	var setColAlign = "left,left,left";
	var setColSorting = "str,str,str";
	var enableTooltips = "true,true,true";
	var setFilter = [ "#text_filter", "#text_filter","#text_filter"];
	var setInitWidths = "150,250,100";
	var gridHeight = "";
	var dataHost = {};
	format = "Y";
	
	gridObj = initGridObject('dataGridDiv');
	gridObj = fnCustomTypes(gridObj, setHeader, setColIds, setInitWidths,
				setColAlign, setColTypes, setColSorting, enableTooltips, setFilter,
				gridHeight, format);
	gridObj = loadDhtmlxGridByJsonData(gridObj, response);
	document.getElementById("dataGridDiv").style.display = 'block';
	document.getElementById("DivNothingMessage").style.display = 'none';
	document.getElementById("DivExportExcel").style.display = 'block';
}
	
// function used to configure the grid data 
function fnCustomTypes(gObj, setHeader, setColIds, setInitWidths, setColAlign,
		setColTypes, setColSorting, enableTooltips, setFilter, gridHeight,
		format) {
	    gObj.setHeader(setHeader);
	    var colCount = setHeader.length;
		gObj.setInitWidths(setInitWidths);
		gObj.setColAlign(setColAlign);
		gObj.setColTypes(setColTypes);
		gObj.setColSorting(setColSorting);
		gObj.enableTooltips(enableTooltips);
		gObj.attachHeader(setFilter);
		gObj.setColumnIds(setColIds);
		gObj.enableAutoHeight(true, gridHeight, true);

	return gObj;
}

//Description: This function will used to generate excel.
function fnDownloadXLS() {
	gridSetObj.toExcel('/phpapp/excel/generate.php');
}
