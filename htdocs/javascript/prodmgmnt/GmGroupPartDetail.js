function fnLoad()
{
	document.frmGroupPartMapping.haction.value = 'loadParts';
	var enableSubComp='';
	var form = parent.document.getElementsByName('frmGroupPartMapping')[1]; // To get the parent form
	var enableSubComObj = parent.document.getElementById("enableSubComponent");
	if(enableSubComObj !=null){
		if(enableSubComObj.checked)
		{
			enableSubComp = 'on';	
		}
	}
	if(disableBtn == 'Y'){// If disableBtn = Y, no need to show close button
		document.frmGroupPartMapping.action="/gmGroupPartMap.do?strPartListFlag=Y&strDisableBtn=Y&groupId="+groupid+'&enableSubComponent='+enableSubComp;
	}else{
		document.frmGroupPartMapping.action="/gmGroupPartMap.do?strPartListFlag=Y&groupId="+groupid+'&enableSubComponent='+enableSubComp;
	}
	fnStartProgress();
	document.frmGroupPartMapping.submit();
}

function fnAllPartHistory() 
{ 	
	windowOpener("/gmGroupPartHistory.do?groupId="+groupid,"","resizable=yes,scrollbars=yes,top=250,left=300,width=1050,height=400");
} 

 

function fnCheckPartRows()
{
	  
	 return rowsize;
}

function fnCheckPartSelect()
{
	 
	objCheckPartArr = document.getElementById("checkPartNumbers");
	 
	 return objCheckPartArr;
}

function fnCheckPrimaryPartSelect()
{ 
	var primaryPartChecks = '';
 	if(rowsize>1)
 	{
		 objCheckPartArr = document.getElementsByName("checkPartNumbers");

		for (var i=0; i < rowsize; i++) 
					{ 

						objPrimary = eval("document.all.Chk_BO"+i);
						objPrimaryLock = eval("document.all.Chk_PL"+i);

					 	if( objCheckPartArr[i].checked == true)
					 	{
						    primaryPartChecks = primaryPartChecks + fnPrimarySecondaryParts(objPrimary,objPrimaryLock);
					 	}
					} 
	}	
	else {
					objPrimary = eval("document.all.Chk_BO0");
					objPrimaryLock = eval("document.all.Chk_PL0");	
		    		if( document.frmGroupPartMapping.checkPartNumbers.checked == true)
				 	{	 		    			
					    primaryPartChecks = primaryPartChecks + fnPrimarySecondaryParts(objPrimary,objPrimaryLock);						
		    		}		
			}	 
 	return  primaryPartChecks;
}

 function fnPrimarySecondaryParts(objPS,objPL)
 {
 	var psParts = '';
	var plValue ='N';	
	if(objPL.value != ''){
		if(objPL.checked == true){
			plValue='Y';
		}
	 }

 	if(objPS.value != '')	//if it's shared part or not
	{ 
	 	if(objPS.checked == true  )
	   		{ 
	   			psParts = psParts + groupid + '^'+ objPS.value + '^'+ '52080'+'^'+ plValue +'|'; 
   			}
	   		else{
	   		 	psParts = psParts + groupid + '^'+ objPS.value + '^'+ '52081'+'^' + plValue +'|'; 
	   		}					    		 
	}
	return psParts;
 }


 function fnCheckSelections()
{	 
	if(rowsize ==1)
	{	
		document.frmGroupPartMapping.checkPartNumbers.checked = true;
		}
		objCheckPartArr = document.frmGroupPartMapping.checkPartNumbers;
		 
		 	if (objCheckPartArr) {
			objCheckPartArrLen = objCheckPartArr.length;
			objSelAll = document.frmGroupPartMapping.selectAll;
			var varselcount = 0;
		
			for(i = 0; i < objCheckPartArrLen; i ++) 
				{	
					if (!objCheckPartArr[i].checked)
					{
						objSelAll.checked = false;
						break	
					}
					else {
						objSelAll.checked = true;
						varselcount ++;
					}
				}
		
			if (varselcount == 0)
			{
				for(i = 0; i < objCheckPartArrLen; i ++) 
				{	
					objCheckPartArr[i].checked = true;
				}
				objSelAll.checked = true;
			}		
}	
	 	

}
 
function fnSelectAll()
{
				objSelAll = document.frmGroupPartMapping.selectAll;
				if (objSelAll) {
				objCheckPartArr = document.frmGroupPartMapping.checkPartNumbers;
				objCheckPartArrLen = objCheckPartArr.length;			
				if(objCheckPartArrLen == undefined){
					if (objSelAll.checked)
					{
						document.frmGroupPartMapping.checkPartNumbers.checked = true;
					}else{
						document.frmGroupPartMapping.checkPartNumbers.checked = false;
					}
					 
					 return false;
				}
				
				if (objSelAll.checked)
				{
					for(i = 0; i < objCheckPartArrLen; i ++) 
					{	
						objCheckPartArr[i].checked = true;
					}
				}
				else {
					for(i = 0; i < objCheckPartArrLen; i ++) 
					{	
						objCheckPartArr[i].checked = false;
					}
				}
				}
	}
	
 

function fnViewPartGrp(pnum)
{
	document.frmGroupPartMapping.hpnum.value = pnum;
	windowOpener("/gmMultiPartGroup.do?hpnum="+encodeURIComponent(pnum),"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=800,height=220");
}


	

function fnPartHistory(pnum)
{ 
	windowOpener("/gmGroupPartHistory.do?hpnum="+encodeURIComponent(pnum),"","resizable=yes,scrollbars=yes,top=250,left=300,width=1050,height=400");
} 


function fnChangeFocus(frmElement)
 {
    
	if(event.keyCode == 13)
	 {	 
		 
			  if( frmElement == 'submit')
		    	{
		      		fnLoad();
		    	}
		    	 
			 		
	 }
 }


function fnVoidValidateCheck()
{ 
	 
 	if(rowsize>1)
 	{ 
		for (var i=0; i < rowsize; i++) 
					{ 
						objPrimary = eval("document.all.Chk_BO"+i);
						objPriceType = eval("document.all.Chk_PartPriceType"+i);						

					 	if(objPrimary!=undefined && objPrimary.value != '')	//if it's shared part or not
						{ 

							if(objPrimary.checked == true || objPriceType.value == '52080'){

									Error_Details(message_prodmgmnt[21]); 
									break;

							}
						}

					} 
	}	
	else {
			objPrimary = eval("document.all.Chk_BO0");
			objPriceType = eval("document.all.Chk_PartPriceType0");						
		    		if(objPrimary!=undefined && objPrimary.value != '')	//if it's shared part or not
						{ 
 							   if(objPrimary.checked == true || objPriceType.value == '52080')
								{ 
									 Error_Details(message_prodmgmnt[21]);									
							 
								}
																					 
						}	
			}	  
										
		if (ErrorCount > 0)
				 {
					Error_Show();
					Error_Clear();
					return false;
				}
				else {
					return true;
				}	  
}

function fnClose(){
	   CloseDhtmlxWindow();
	   return false;
}
