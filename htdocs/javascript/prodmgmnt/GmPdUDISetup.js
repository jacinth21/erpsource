var check_rowId = '';
var part_rowId = '';
var project_rowId = '';
var partdes_rowId = '';
var contains_rowId = '';
var reqSter_rowId = '';
var sterMethod_rowId = '';
var moreSize_rowId = '';
var size1Type_rowId = '';
var size1Text_rowId = '';
var size1Value_rowId = '';
var size1Uom_rowId = '';

var size2Type_rowId = '';
var size2Text_rowId = '';
var size2Value_rowId = '';
var size2Uom_rowId = '';

var size3Type_rowId = '';
var size3Text_rowId = '';
var size3Value_rowId = '';
var size3Uom_rowId = '';

var size4Type_rowId = '';
var size4Text_rowId = '';
var size4Value_rowId = '';
var size4Uom_rowId = '';
var quality_rowId = '';
var pd_rowId = '';
var q_partfl_rowId = '';

// steril method variables
var SterilCol = '';
var SterilColArr = '';
var SterilId = '';
var SterilNm = '';
var SterilVal = '';

// size1type local variables
var SizetypeCol = '';
var SizetypeColArr = '';
var SizetypeId = '';
var SizetypeNm = '';
var Size1typeVal = '';
var Size2typeVal = '';
var Size3typeVal = '';
var Size4typeVal = '';

// size1UOM local variables
var UOMCol = '';
var UOMColArr = '';
var UOMId = '';
var UOMNm = '';
var UOM1Val = '';
var UOM2Val = '';
var UOM3Val = '';
var UOM4Val = '';

var checkIdVal = '';
var partIdVal = '';
var projectIdVal = '';
var partdesIdVal = '';
var containsIdVal = '';
var reqSterIdVal = '';
var sterMethodIdVal = '';
var moreSizeIdVal = '';
var size1TypeIdVal = '';
var size1TextIdVal = '';
var size1ValueIdVal = '';
var size1UomIdVal = '';

var size2TypeIdVal = '';
var size2TextIdVal = '';
var size2ValueIdVal = '';
var size2UomIdVal = '';

var size3TypeIdVal = '';
var size3TextIdVal = '';
var size3ValueIdVal = '';
var size3UomIdVal = '';

var size4TypeIdVal = '';
var size4TextIdVal = '';
var size4ValueIdVal = '';
var size4UomIdVal = '';
var emptyFl = false;
var duplicatePartStr = '';
var existingPartStr = '';
var emptyProjectStr = '';
var nonProjectStr = '';
var deletedPartStr = '';
var removeEmptyFl = false;
var finalInputStr = '';
var finalPdId = '';
var wrongUOMFl = false;
var arrValuefromClipboard = new Array();
var notAnumberFl = false;
var distParsingCnt = 100;
var countDownTime = 0;

var part='';
var project='';
var partDesc='';
var containsLatex='';
var req='';
var steril='';
var isDev='';
var size1Type='';
var size1Text='';
var size1Value='';
var uom1='';
var size2Type='';
var size2Text='';
var size2Value='';
var uom2='';
var size3Type='';
var size3Text='';
var size3Value='';
var uom3='';
var size4Type='';
var size4Text='';
var size4Value='';
var uom4='';
var quality_fl='';
var pdid='';
var q_part_fl='';
// this function used to click the enter button to load the screen.
function fnEnter() {
	if (event.keyCode == 13) {
		if(confirm(message_prodmgmnt[32])){
			fnLoad();
		}
	}
}
// this function used to reload the screen.
function fnLoad() {
	var projectVal = document.frmUDIProductDevelopment.projectId.value;
	var partNumVal = TRIM(document.frmUDIProductDevelopment.partNumber.value);
	var partDesVal = TRIM(document.frmUDIProductDevelopment.partDesc.value);
	var searchVal = document.frmUDIProductDevelopment.Cbo_Search.value;
	if (projectVal == 0 && partNumVal == '' && partDesVal == '') {
		Error_Details(message_prodmgmnt[33]);
	}
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return;
	}
	fnStartProgress('Y');
	setTimeout(function() {
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmUDIProductDevelopment.do?method=pdUDISetup&strOpt=Reload&projectId='+projectVal+'&partNumber='+ encodeURIComponent(partNumVal) +'&partDesc='+ partDesVal + '&Cbo_Search='+ searchVal+'&ramdomId='+new Date().getTime());
		var loader = dhtmlxAjax.getSync(ajaxUrl);
		fillPdDataSet(loader);
	},100);
}
// this function used to load the grid data
function fillPdDataSet(loader) {
	var response = loader.xmlDoc.responseText;
	if(response!= null);
	 {   
		
		  var dataHost = {};
	    var rows = [], i = 0;
	    var JSON_Array_obj = "";
	JSON_Array_obj = JSON.parse(response);
	$.each(JSON_Array_obj, function(index, jsonObject) {
		part = jsonObject.PNUM;
		project = jsonObject.PROJECTID;
		partDesc = jsonObject.PART_DESC;
		containsLatex = jsonObject.LATEX;
		req = jsonObject.REQ_STERILE;
		steril = jsonObject.STERILE_METHOD;
		isDev = jsonObject.MORE_SIZE;
		size1Type = jsonObject.SIZE1;
		size1Text = jsonObject.SIZE1_TEXT;
	    size1Value = jsonObject.SIZE1_VALUE;
		uom1 = jsonObject.SIZE1_UOM;
		size2Type = jsonObject.SIZE2;
		size2Text = jsonObject.SIZE2_TEXT;
		size2Value = jsonObject.SIZE2_VALUE;
		uom2 = jsonObject.SIZE2_UOM;
		size3Type = jsonObject.SIZE3;
		size3Text = jsonObject.SIZE3_TEXT;
		size3Value = jsonObject.SIZE3_VALUE;
		uom3 = jsonObject.SIZE3_UOM;
	    size4Type = jsonObject.SIZE4;
		size4Text = jsonObject.SIZE4_TEXT;
		size4Value = jsonObject.SIZE4_VALUE;
		uom4 = jsonObject.SIZE4_UOM;
	    quality_fl = jsonObject.QUALITY_FL;
	    pdid = jsonObject.PD_ID;
	    q_part_fl = jsonObject.Q_PART_FL;
	
		
		rows[i] = {};
		rows[i].id = pdid;
		rows[i].data = [];
		
		rows[i].data[0] = '';
		rows[i].data[1] = part;
		rows[i].data[2] = project;
		rows[i].data[3] = partDesc;
		rows[i].data[4] = containsLatex;
		rows[i].data[5] = req;
		rows[i].data[6] = steril;
		rows[i].data[7] = isDev;
		rows[i].data[8] = size1Type;
		rows[i].data[9] = size1Text;
		rows[i].data[10] = size1Value;
		rows[i].data[11] = uom1;
		rows[i].data[12] = size2Type;
		rows[i].data[13] = size2Text;
		rows[i].data[14] = size2Value;
		rows[i].data[15] = uom2; 
		rows[i].data[16] = size3Type; 
		rows[i].data[17] = size3Text; 
		rows[i].data[18] = size3Value;
		rows[i].data[19] = uom3;
		rows[i].data[20] = size4Type;
		rows[i].data[21] = size4Text;
		rows[i].data[22] = size4Value;
		rows[i].data[23] = uom4;
		rows[i].data[24] = quality_fl;
		rows[i].data[25] = pdid;
		rows[i].data[26] = q_part_fl;
		i++;
	});


   dataHost.rows = rows;
		
		 countDownTime = 0;
		 gridObj.startFastOperations();
		 gridObj.clearAll();
		 fnStopProgress();
	
		 gridObj.parse(dataHost,"json");
		 gridObj.stopFastOperations();
		/* gridObj.attachHeader("<input type='checkbox' value='no' name='selectAll' onClick='javascript:fnChangedFilter(this);'/>"
					+ ',#text_filter,#select_filter,#text_filter,#select_filter,#select_filter,#select_filter,#select_filter,'+message_prodmgmnt[442]+','+message_prodmgmnt[443]+', #,'+message_prodmgmnt[444]+','+message_prodmgmnt[442]+','+message_prodmgmnt[443]+', #,'+message_prodmgmnt[444]+','+message_prodmgmnt[442]+','+message_prodmgmnt[443]+', #,'+message_prodmgmnt[444]+','+message_prodmgmnt[442]+','+message_prodmgmnt[443]+', #,'+message_prodmgmnt[444]);*/
		 var rowcount =  gridObj.rowsBuffer.length;

		// No data found
		 if(rowcount == 0){
			 document.getElementById("trNoDataFound").style.display = "table-row";
			 document.getElementById("trUserManipulation").style.display = "none";
			 document.getElementById("trDiv").style.display = "none";
			 document.getElementById("trButton").style.display = "none";
		 }else{
			 document.getElementById("trNoDataFound").style.display = "none";
			 document.getElementById("trUserManipulation").style.display = "table-row";
			 document.getElementById("trDiv").style.display = "table-row";
			 document.getElementById("trButton").style.display = "table-row";
		 }
		 
		 if(rowcount > distParsingCnt){
			 var tot = rowcount/distParsingCnt;
			 tot = tot * 3;
			 countDownTime = Math.round(tot);
			 document.getElementById("loadingTr").style.display="block";
			 document.getElementById("saveSuccessfully").style.display="block";
			 fnShowingCountdownTimer();
		 }else{
			 gridObj.forEachRow(function(rowId) {
					qualityVal = gridObj.cellById(rowId, quality_rowId).getValue();
					q_partFlVal = gridObj.cellById(rowId, q_partfl_rowId).getValue();
					size1TypeIdVal = gridObj.cellById(rowId, size1Type_rowId).getValue();
					size2TypeIdVal = gridObj.cellById(rowId, size2Type_rowId).getValue();
					size3TypeIdVal = gridObj.cellById(rowId, size3Type_rowId).getValue();
					size4TypeIdVal = gridObj.cellById(rowId, size4Type_rowId).getValue();
					
					if (qualityVal == 'Y') {
						gridObj.cellById(rowId, check_rowId).setDisabled(true);
						gridObj.lockRow(rowId,true);
					}
					if(q_partFlVal == 'Y'){
						gridObj.cellById(rowId, part_rowId).setDisabled(true);
						gridObj.cellById(rowId, project_rowId).setDisabled(true);
						gridObj.cellById(rowId, partdes_rowId).setDisabled(true);
					}
					if (size1TypeIdVal == '104047') {
						gridObj.cellById(rowId, size1Value_rowId).setDisabled(true);
						gridObj.cellById(rowId, size1Uom_rowId).setDisabled(true);
					}
					if (size2TypeIdVal == '104047') {
						gridObj.cellById(rowId, size2Value_rowId).setDisabled(true);
						gridObj.cellById(rowId, size2Uom_rowId).setDisabled(true);
					}
					if (size3TypeIdVal == '104047') {
						gridObj.cellById(rowId, size3Value_rowId).setDisabled(true);
						gridObj.cellById(rowId, size3Uom_rowId).setDisabled(true);
					}
					if (size4TypeIdVal == '104047') {
						gridObj.cellById(rowId, size4Value_rowId).setDisabled(true);
						gridObj.cellById(rowId, size4Uom_rowId).setDisabled(true);
					}
				}); 
		 }
	}
}
var combobox;
var codeId,codeNm;
//this function used to load the grid data.
function fnOnPageLoad() {
	var sel = document.all.hSearch.value;
	if (sel != ''){
		document.all.Cbo_Search.value = sel;
	}
	var strOptVal = document.frmUDIProductDevelopment.strOpt.value;
	validate_fl = true;

	if (objGridData != '') {
		//gridObj = setDefalutGridProperties('partNumData');
		gridObj = new dhtmlXGridObject('partNumData');
		gridObj.setImagePath("/extweb/suite/5.2.0/codebase/imgs/");
		gridObj.setHeader("Select All,Part #,Project Id,Part Description,Contains Latex?,Requires Sterilization Prior to use,Sterilization method,More than one size?,Size 1,#cspan,#cspan,#cspan,Size 2,#cspan,#cspan,#cspan,Size 3,#cspan,#cspan,#cspan,Size 4,#cspan,#cspan,#cspan,,,");
		gridObj.setInitWidths("48,100,75,152,40,40,100,40,100,50,30,80,100,50,30,80,100,50,30,77,100,50,30,77,0,0,0");
		gridObj.setColAlign("center,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left");
		gridObj.setColTypes("ch,ed,coro,ed,coro,coro,coro,coro,coro,ed,ed,coro,coro,ed,ed,coro,coro,ed,ed,coro,coro,ed,ed,coro,ro,ro,ro");
		gridObj.setColSorting("na,na,na,na,na,na,na,na,na,na,na,na,na,na,na,na,na,na,na,na,na,na,na,na,na,na,na");
		gridObj.setColumnIds("check,part,project,partDesc,containsLatex,req,steril,isDev,size1Type,size1Text,size1Value,uom1,size2Type,size2Text,size2Value,uom2,size3Type,size3Text,size3Value,uom3,size4Type,size4Text,size4Value,uom4,quality_fl,pdid,q_part_fl");
		
		//set drop down values for grid body to show pttype
	    combobox = gridObj.getCombo(2);
		combobox.clear();

		for(var i=0;i<projectLen;i++){
			arr=eval("projectArr"+i);
			arrobj = arr.split(",");
			codeId = arrobj[0];
			codeNm =  arrobj[1];
			combobox.put("0","[Choose one]");
		combobox.put(codeId,codeNm);
		}
		//combobox.save();
		
		//set drop down values for grid body to show pttype
	    combobox = gridObj.getCombo(4);
		combobox.clear();
		
		for(var i=0;i<containsLatexLen;i++){
			arr=eval("containsLatex"+i);
			arrobj = arr.split(",");
			codeId = arrobj[0];
			codeNm =  arrobj[1];
			combobox.put("0","[Choose one]");
		    combobox.put(codeId,codeNm);
		}
		
		//set drop down values for grid body to show pttype
		 combobox = gridObj.getCombo(5);
		combobox.clear();

		for(var i=0;i<containsLatexLen;i++){
			arr=eval("containsLatex"+i);
			arrobj = arr.split(",");
			codeId = arrobj[0];
			codeNm =  arrobj[1];
			combobox.put("0","[Choose one]");
		    combobox.put(codeId,codeNm);
		}
		
		//set drop down values for grid body to sterile method
		 combobox = gridObj.getCombo(6);
		combobox.clear();

		for(var i=0;i<sterilMethodLen;i++){
			arr=eval("sterilMethod"+i);
			arrobj = arr.split(",");
			codeId = arrobj[0];
			codeNm =  arrobj[1];
			combobox.put("0","[Choose one]");
		    combobox.put(codeId,codeNm);
		}
		
		//set drop down values for grid body to show pttype
		combobox = gridObj.getCombo(7);
		combobox.clear();
		
		for(var i=0;i<containsLatexLen;i++){
			arr=eval("containsLatex"+i);
			arrobj = arr.split(",");
			codeId = arrobj[0];
			codeNm =  arrobj[1];
			combobox.put("0","[Choose one]");
		    combobox.put(codeId,codeNm);
		}
		
		//set drop down values for grid body to size type
		combobox = gridObj.getCombo(8);
		combobox.clear();

		for(var i=0;i<sizeTypeLen;i++){
			arr=eval("sizeType"+i);
			arrobj = arr.split(",");
			codeId = arrobj[0];
			codeNm =  arrobj[1];
			combobox.put("0","[Choose one]");
		    combobox.put(codeId,codeNm);
		}
		
		//set drop down values for grid body to size type
		combobox = gridObj.getCombo(11);
		combobox.clear();

		for(var i=0;i<sizeUOMLen;i++){
			arr=eval("sizeUOM"+i);
			arrobj = arr.split(",");
			codeId = arrobj[0];
			codeNm =  arrobj[1];
			combobox.put("0","[Choose one]");
		    combobox.put(codeId,codeNm);
		}
		
		//set drop down values for grid body to size type
		combobox = gridObj.getCombo(12);
		combobox.clear();

		for(var i=0;i<sizeTypeLen;i++){
			arr=eval("sizeType"+i);
			arrobj = arr.split(",");
			codeId = arrobj[0];
			codeNm =  arrobj[1];
			combobox.put("0","[Choose one]");
		    combobox.put(codeId,codeNm);
		}
		
		//set drop down values for grid body to size type
		combobox = gridObj.getCombo(15);
		combobox.clear();

		for(var i=0;i<sizeUOMLen;i++){
			arr=eval("sizeUOM"+i);
			arrobj = arr.split(",");
			codeId = arrobj[0];
			codeNm =  arrobj[1];
			combobox.put("0","[Choose one]");
		    combobox.put(codeId,codeNm);
		}
		
		//set drop down values for grid body to size type
		combobox = gridObj.getCombo(16);
		combobox.clear();

		for(var i=0;i<sizeTypeLen;i++){
			arr=eval("sizeType"+i);
			arrobj = arr.split(",");
			codeId = arrobj[0];
			codeNm =  arrobj[1];
			combobox.put("0","[Choose one]");
		    combobox.put(codeId,codeNm);
		}
		
		//set drop down values for grid body to size type
		combobox = gridObj.getCombo(19);
		combobox.clear();

		for(var i=0;i<sizeUOMLen;i++){
			arr=eval("sizeUOM"+i);
			arrobj = arr.split(",");
			codeId = arrobj[0];
			codeNm =  arrobj[1];
			combobox.put("0","[Choose one]");
		    combobox.put(codeId,codeNm);
		}
		
		//set drop down values for grid body to size type
		combobox = gridObj.getCombo(20);
		combobox.clear();

		for(var i=0;i<sizeTypeLen;i++){
			arr=eval("sizeType"+i);
			arrobj = arr.split(",");
			codeId = arrobj[0];
			codeNm =  arrobj[1];
			combobox.put("0","[Choose one]");
		    combobox.put(codeId,codeNm);
		}
		
		//set drop down values for grid body to size type
		combobox = gridObj.getCombo(23);
		combobox.clear();
	
		for(var i=0;i<sizeUOMLen;i++){
			arr=eval("sizeUOM"+i);
			arrobj = arr.split(",");
			codeId = arrobj[0];
			codeNm =  arrobj[1];
			combobox.put("0","[Choose one]");
		    combobox.put(codeId,codeNm);
		}
		
		
		combobox.save();
		
		gridObj.attachHeader("<input type='checkbox' value='no' name='selectAll' onClick='javascript:fnChangedFilter(this);'/>"
				+ ',#text_filter,#select_filter,#text_filter,#select_filter,#select_filter,#select_filter,#select_filter,'+message_prodmgmnt[442]+','+message_prodmgmnt[443]+', #,'+message_prodmgmnt[444]+','+message_prodmgmnt[442]+','+message_prodmgmnt[443]+', #,'+message_prodmgmnt[444]+','+message_prodmgmnt[442]+','+message_prodmgmnt[443]+', #,'+message_prodmgmnt[444]+','+message_prodmgmnt[442]+','+message_prodmgmnt[443]+', #,'+message_prodmgmnt[444]);
		
		gridObj.init();
		gridObj.enableAlterCss("even","uneven");
		gridObj.enableDistributedParsing(true,distParsingCnt,2000);
		//gridObj.parse(objGridData,"js");
		//gridObj.loadXMLString(objGridData);
		//gridObj = initializeDefaultGrid(gridObj, objGridData);
		gridObj.enableTooltips("false,true,true,true,true,true,true,true,true,true,true");
		
		gridObj.enableBlockSelection(true);
		gridObj.forceLabelSelection(true);
		gridObj.attachEvent("onKeyPress", keyPressed);
		gridObj.attachEvent("onEditCell",doOnCellEdit);
		gridObj.attachEvent("onCheckbox", doOnCheck);
		gridObj.attachEvent("onDistributedEnd", doOnDistributedEnd);
		gridObj.attachEvent("onBeforeBlockselected", doOnBeforeBlockSelect);
		gridObj.enableMultiselect(true);
		check_rowId = gridObj.getColIndexById("check");
		part_rowId = gridObj.getColIndexById("part");
		project_rowId = gridObj.getColIndexById("project");
		partdes_rowId = gridObj.getColIndexById("partDesc");
		contains_rowId = gridObj.getColIndexById("containsLatex");
		reqSter_rowId = gridObj.getColIndexById("req");
		sterMethod_rowId = gridObj.getColIndexById("steril");
		moreSize_rowId = gridObj.getColIndexById("isDev");
		size1Type_rowId = gridObj.getColIndexById("size1Type");
		size1Text_rowId = gridObj.getColIndexById("size1Text");
		size1Value_rowId = gridObj.getColIndexById("size1Value");
		size1Uom_rowId = gridObj.getColIndexById("uom1");

		size2Type_rowId = gridObj.getColIndexById("size2Type");
		size2Text_rowId = gridObj.getColIndexById("size2Text");
		size2Value_rowId = gridObj.getColIndexById("size2Value");
		size2Uom_rowId = gridObj.getColIndexById("uom2");

		size3Type_rowId = gridObj.getColIndexById("size3Type");
		size3Text_rowId = gridObj.getColIndexById("size3Text");
		size3Value_rowId = gridObj.getColIndexById("size3Value");
		size3Uom_rowId = gridObj.getColIndexById("uom3");

		size4Type_rowId = gridObj.getColIndexById("size4Type");
		size4Text_rowId = gridObj.getColIndexById("size4Text");
		size4Value_rowId = gridObj.getColIndexById("size4Value");
		size4Uom_rowId = gridObj.getColIndexById("uom4");
		quality_rowId = gridObj.getColIndexById("quality_fl");
		pd_rowId = gridObj.getColIndexById("pdid");
		q_partfl_rowId = gridObj.getColIndexById("q_part_fl");
		// load the empty rows
		if (strOptVal == '') {

				add_fast();
		}
		var allRowIds = gridObj.getAllRowIds(",").split(",");
		var all_rowLen = allRowIds.length; 
		if(all_rowLen > distParsingCnt){
			//fnStartProgress();
			/*document.frmUDIProductDevelopment.btn_submit.disabled = true;
			document.frmUDIProductDevelopment.btn_Void.disabled = true;
			document.frmUDIProductDevelopment.btn_Print.disabled = true;*/
		}else{
			// if part available in quality table then all rows to be disable.
			gridObj.forEachRow(function(rowId) {
				qualityVal = gridObj.cellById(rowId, quality_rowId).getValue();
				if (qualityVal == 'Y') {
					gridObj.cellById(rowId, check_rowId).setDisabled(true);
					gridObj.lockRow(rowId,true);
				}
			}); 
		}
		
	}
}
// this function used to select all option (check box)
function fnChangedFilter(obj) {
	if (obj.checked) {
		gridObj.forEachRow(function(rowId) {
			var check_disable_fl = gridObj.cellById(rowId, check_rowId).isDisabled();
			if (!check_disable_fl) {
				gridObj.cellById(rowId, check_rowId).setChecked(true);
			}
		});
	} else {
		gridObj.forEachRow(function(rowId) {
			check_id = gridObj.cellById(rowId, check_rowId).getValue();

			if (check_id == 1) {
				gridObj.cellById(rowId, check_rowId).setChecked(false);
			}

		});
	}
}
//this function used to copy the grid data 
function keyPressed(code, ctrl) {
	
	  if (code == 67 && ctrl) {
    	gridObj.setCSVDelimiter("\t");
    	gridObj.copyBlockToClipboard()
    }
    
    if (code == 86 && ctrl) {
	pasteToGrid();
		/*gridObj.setCSVDelimiter("\t");
		gridObj.pasteBlockFromClipboard();*/
	}
	gridObj.refreshFilters();
	return true;
}

//this function used to edit the cell in the grid 
function doOnCellEdit(stage, rowId, cellInd, nValue, oValue) {
	if (cellInd == project_rowId || cellInd == partdes_rowId || cellInd == contains_rowId || cellInd == reqSter_rowId  || cellInd == sterMethod_rowId || cellInd == moreSize_rowId ||
		cellInd == size1Type_rowId || cellInd == size2Type_rowId || cellInd == size3Type_rowId || cellInd == size4Type_rowId ||
		cellInd == size1Text_rowId || cellInd == size2Text_rowId || cellInd == size3Text_rowId || cellInd == size4Text_rowId ||
		cellInd == size1Value_rowId || cellInd == size2Value_rowId || cellInd == size3Value_rowId || cellInd == size4Value_rowId ||
		cellInd == size1Uom_rowId || cellInd == size2Uom_rowId || cellInd == size3Uom_rowId || cellInd == size4Uom_rowId || cellInd == part_rowId) {
		if (stage == 0) {
			if (cellInd == size1Type_rowId || cellInd == size2Type_rowId
					|| cellInd == size3Type_rowId || cellInd == size4Type_rowId
					|| cellInd == size1Text_rowId || cellInd == size2Text_rowId
					|| cellInd == size3Text_rowId || cellInd == size4Text_rowId
					|| cellInd == size1Value_rowId
					|| cellInd == size2Value_rowId
					|| cellInd == size3Value_rowId
					|| cellInd == size4Value_rowId || cellInd == size1Uom_rowId
					|| cellInd == size2Uom_rowId || cellInd == size3Uom_rowId
					|| cellInd == size4Uom_rowId) {
				var moreSizeVal = gridObj.cellById(rowId, moreSize_rowId).getValue();
				if (moreSizeVal == 80131) {
					gridObj.cellById(rowId, cellInd).setDisabled(true);
					return false;
				}
				if(cellInd == size1Text_rowId){
					var type1 = gridObj.cellById(rowId, size1Type_rowId).getValue();
					if (type1 != 104047) { // Others
						return false;
					}
				}else if(cellInd == size2Text_rowId){
					var type2 = gridObj.cellById(rowId, size2Type_rowId).getValue();
					if (type2 != 104047) { // Others
						return false;
					}
				}else if(cellInd == size3Text_rowId){
					var type3 = gridObj.cellById(rowId, size3Type_rowId).getValue();
					if (type3 != 104047) { // Others
						return false;
					}
				} else if(cellInd == size4Text_rowId){
					var type4 = gridObj.cellById(rowId, size4Type_rowId).getValue();
					if (type4 != 104047) { // Others
						return false;
					}
				}  
				
				
			}
			// set the default values
			if (cellInd == sterMethod_rowId) {
				var str = gridObj.cellById(rowId, sterMethod_rowId).getValue();
				var reqSt = gridObj.cellById(rowId, reqSter_rowId).getValue();
				
				if (str == '') {
					gridObj.cellById(rowId, sterMethod_rowId).setValue('0');
				}
			} else if (cellInd == size1Type_rowId) {
				var type1 = gridObj.cellById(rowId, size1Type_rowId).getValue();
				if (type1 == '') {
					gridObj.cellById(rowId, size1Type_rowId).setValue('0');
				} else if (type1 == 104047) { // Others
					gridObj.cellById(rowId, size1Text_rowId).setDisabled(false);
				}
			} else if (cellInd == size1Uom_rowId) {
				var uom1 = gridObj.cellById(rowId, size1Uom_rowId).getValue();
				if (uom1 == '') {
					gridObj.cellById(rowId, size1Uom_rowId).setValue('0');
				}
			} else if (cellInd == size2Type_rowId) {
				var type2 = gridObj.cellById(rowId, size2Type_rowId).getValue();
				if (type2 == '') {
					gridObj.cellById(rowId, size2Type_rowId).setValue('0');
				} else if (type2 == 104047) { // Others
					gridObj.cellById(rowId, size2Text_rowId).setDisabled(false);
				}
			} else if (cellInd == size2Uom_rowId) {
				var uom2 = gridObj.cellById(rowId, size2Uom_rowId).getValue();
				if (uom2 == '') {
					gridObj.cellById(rowId, size2Uom_rowId).setValue('0');
				}
			} else if (cellInd == size3Type_rowId) {
				var type3 = gridObj.cellById(rowId, size3Type_rowId).getValue();
				if (type3 == '') {
					gridObj.cellById(rowId, size3Type_rowId).setValue('0');
				} else if (type3 == 104047) { // Others
					gridObj.cellById(rowId, size3Text_rowId).setDisabled(false);
				}
			} else if (cellInd == size3Uom_rowId) {
				var uom3 = gridObj.cellById(rowId, size3Uom_rowId).getValue();
				if (uom3 == '') {
					gridObj.cellById(rowId, size3Uom_rowId).setValue('0');
				}
			} else if (cellInd == size4Type_rowId) {
				var type4 = gridObj.cellById(rowId, size4Type_rowId).getValue();
				if (type4 == '') {
					gridObj.cellById(rowId, size4Type_rowId).setValue('0');
				} else if (type4 == 104047) { // Others
					gridObj.cellById(rowId, size4Text_rowId).setDisabled(false);
				}
			} else if (cellInd == size4Uom_rowId) {
				var uom4 = gridObj.cellById(rowId, size4Uom_rowId).getValue();
				if (uom4 == '') {
					gridObj.cellById(rowId, size4Uom_rowId).setValue('0');
				}
			}
		}
		if (stage == 2) {
			if(cellInd == moreSize_rowId){
				var sizeFieldVal = gridObj.cellById(rowId, moreSize_rowId).getValue();
				if(sizeFieldVal == 80130){ // Yes 80130
					fnSizeDisable (rowId, false);
				}else{
					fnSizeDisable (rowId, true);
					fnSetSizeColor (rowId);
				}
			}else if (cellInd == reqSter_rowId){
				reqSterVal = gridObj.cellById(rowId, reqSter_rowId).getValue();
				if(reqSterVal == 0){
					gridObj.cellById(rowId, sterMethod_rowId).setValue('');
				}
				//
				if(oValue != nValue)
					gridObj.setCellTextStyle(rowId,sterMethod_rowId,"color:black;font-weight:normal;");
			}
			fnEnableDisableSizeText (rowId, size1Text_rowId, gridObj.cellById(rowId, size1Type_rowId).getValue());
			fnEnableDisableSizeText (rowId, size2Text_rowId, gridObj.cellById(rowId, size2Type_rowId).getValue());
			fnEnableDisableSizeText (rowId, size3Text_rowId, gridObj.cellById(rowId, size3Type_rowId).getValue());
			fnEnableDisableSizeText (rowId, size4Text_rowId, gridObj.cellById(rowId, size4Type_rowId).getValue());
			gridObj.setCellTextStyle(rowId,cellInd,"color:black;font-weight:normal;");
		}
		if (stage == 2 && oValue != nValue) {
			if (cellInd == size1Type_rowId) {
				// gridObj.setCellTextStyle(rowId,cellInd,"background-color:#D8D8D8;");
				gridObj.setCellTextStyle(rowId, size1Uom_rowId, "background-color:#D8D8D8;");
				var newArrayObj1 = new Array();
				var comboUom1 = gridObj.getCustomCombo(rowId, size1Uom_rowId);
				if(nValue == 104047){
				gridObj.cells(rowId,10).setValue("");
				gridObj.cellById(rowId, size1Uom_rowId).setDisabled(true);
				gridObj.cellById(rowId, size1Value_rowId).setDisabled(true);
				}else {
				gridObj.cellById(rowId, size1Uom_rowId).setDisabled(false);
				gridObj.cellById(rowId, size1Value_rowId).setDisabled(false);		
				}
				comboUom1.clear();
				comboUom1.put(0, '[Choose One]');
				newArrayObj1 = fnSetUomDropDown(comboUom1, gridObj.cellById(rowId, size1Type_rowId).getValue());
				gridObj.cellById(rowId, size1Uom_rowId).setValue(0);
				// to set the block color
				gridObj.setCellTextStyle(rowId,size1Value_rowId,"color:black;font-weight:normal;");
			} else if (cellInd == size2Type_rowId) {
				gridObj.setCellTextStyle(rowId, size2Uom_rowId, "background-color:#D8D8D8;");
				var newArrayObj2 = new Array();
				var comboUom2 = gridObj.getCustomCombo(rowId, size2Uom_rowId);
				if(nValue == 104047){
				gridObj.cells(rowId,14).setValue("");
				gridObj.cellById(rowId, size2Uom_rowId).setDisabled(true);
				gridObj.cellById(rowId, size2Value_rowId).setDisabled(true);
				}else {
				gridObj.cellById(rowId, size2Uom_rowId).setDisabled(false);
				gridObj.cellById(rowId, size2Value_rowId).setDisabled(false);	
				}
				comboUom2.clear();
				comboUom2.put(0, '[Choose One]');
				newArrayObj2 = fnSetUomDropDown(comboUom2, gridObj.cellById(rowId, size2Type_rowId).getValue());
				gridObj.cellById(rowId, size2Uom_rowId).setValue(0);
				// to set the block color
				gridObj.setCellTextStyle(rowId,size2Value_rowId,"color:black;font-weight:normal;");
			} else if (cellInd == size3Type_rowId) {
				gridObj.setCellTextStyle(rowId, size3Uom_rowId, "background-color:#D8D8D8;");
				var newArrayObj3 = new Array();
				var comboUom3 = gridObj.getCustomCombo(rowId, size3Uom_rowId);
				if(nValue == 104047){
				gridObj.cells(rowId,18).setValue("");
				gridObj.cellById(rowId, size3Uom_rowId).setDisabled(true);
				gridObj.cellById(rowId, size3Value_rowId).setDisabled(true);
				}else {
				gridObj.cellById(rowId, size3Uom_rowId).setDisabled(false);
				gridObj.cellById(rowId, size3Value_rowId).setDisabled(false);	
				}
				comboUom3.clear();
				comboUom3.put(0, '[Choose One]');
				newArrayObj3 = fnSetUomDropDown(comboUom3, gridObj.cellById(rowId, size3Type_rowId).getValue());
				gridObj.cellById(rowId, size3Uom_rowId).setValue(0);
				// to set the block color
				gridObj.setCellTextStyle(rowId,size3Value_rowId,"color:black;font-weight:normal;");
			} else if (cellInd == size4Type_rowId) {
				gridObj.setCellTextStyle(rowId, size4Uom_rowId, "background-color:#D8D8D8;");
				var newArrayObj4 = new Array();
				var comboUom4 = gridObj.getCustomCombo(rowId, size4Uom_rowId);
				if(nValue == 104047){
				gridObj.cells(rowId,22).setValue("");
				gridObj.cellById(rowId, size4Uom_rowId).setDisabled(true);
				gridObj.cellById(rowId, size4Value_rowId).setDisabled(true);
				}else {
				gridObj.cellById(rowId, size4Uom_rowId).setDisabled(false);
				gridObj.cellById(rowId, size4Value_rowId).setDisabled(false);			
				}
				comboUom4.clear();
				comboUom4.put(0, '[Choose One]');
				newArrayObj4 = fnSetUomDropDown(comboUom4, gridObj.cellById(rowId, size4Type_rowId).getValue());
				gridObj.cellById(rowId, size4Uom_rowId).setValue(0);
				// to set the block color
				gridObj.setCellTextStyle(rowId,size4Value_rowId,"color:black;font-weight:normal;");
			}
			gridObj.setCellTextStyle(rowId,part_rowId,"color:black;font-weight:normal;");
			gridObj.setRowColor(rowId,"#9FF781");
			//gridObj.setCellTextStyle(rowId,cellInd,"");
		}
	}
	return true;
}

// To add rows into grid.
function addRow() {
	var newRowId = gridObj.getUID();
	gridObj.addRow(newRowId, '');
	
	// to set the empty records
	gridObj.cellById(newRowId, project_rowId).setValue('0');
	gridObj.cellById(newRowId, contains_rowId).setValue('0');
	gridObj.cellById(newRowId, reqSter_rowId).setValue('0');
	gridObj.cellById(newRowId, sterMethod_rowId).setValue('0');
	gridObj.cellById(newRowId, moreSize_rowId).setValue('0');
	gridObj.cellById(newRowId, size1Type_rowId).setValue('0');
	gridObj.cellById(newRowId, size1Uom_rowId).setValue('0');
	gridObj.cellById(newRowId, size2Type_rowId).setValue('0');
	gridObj.cellById(newRowId, size2Uom_rowId).setValue('0');
	gridObj.cellById(newRowId, size3Type_rowId).setValue('0');
	gridObj.cellById(newRowId, size3Uom_rowId).setValue('0');
	gridObj.cellById(newRowId, size4Type_rowId).setValue('0');
	gridObj.cellById(newRowId, size4Uom_rowId).setValue('0');
}

// Use the below function to copy data inside the screen.
function docopy() {
	gridObj.setCSVDelimiter("\t");
	gridObj.copyBlockToClipboard();
	var startPoint = 0;
	arrValuefromClipboard = new Array();
	if (gridObj._selectionArea != null) {
		var area = gridObj._selectionArea;
		var leftTopCol = area.LeftTopCol;
		var leftTopRow = area.LeftTopRow;
		var rightBottomCol = area.RightBottomCol;
		var rightBottomRow = area.RightBottomRow;
		for ( var i = leftTopRow; i <= rightBottomRow; i++) {
			for ( var j = leftTopCol; j <= rightBottomCol; j++) {
				setValuefromClipboard = gridObj.cellByIndex(i, j).getValue();
				var e = gridObj.cellByIndex(i, j);
				if (e.combo) {
					for ( var g = e.combo.values, y = 0; y < g.length; y++){
						if (setValuefromClipboard == e.combo.keys[y]) {
							setValuefromClipboard = e.combo.values[y];
							g = null;
							break;
						}
					}
				}
				arrValuefromClipboard.push(setValuefromClipboard);
				startPoint++;
			}
		}
	}
	gridObj._HideSelection();
}
// to copy the clip board data to grid
function pasteToGrid() {

	if (gridObj._selectionArea != null) {
		//gridObj.pasteBlockFromClipboard();
		var area = gridObj._selectionArea;
		var leftTopCol = area.LeftTopCol;
		var leftTopRow = area.LeftTopRow;
		var rightBottomCol = area.RightBottomCol;
		var rightBottomRow = area.RightBottomRow;
		for ( var i = leftTopRow; i <= rightBottomRow; i++) {
			var m = 0;
			//for ( var m = 0; m < arrValuefromClipboard.length; m++) {
			for ( var j = leftTopCol; j <= rightBottomCol; j++) {
				var newRowId = gridObj.getRowId(i);
				var disbleFl = false;//gridObj.cellByIndex(i, j).isDisabled();
				if(j == part_rowId || j == project_rowId || j== partdes_rowId ){
					disbleFl = gridObj.cellByIndex(i, j).isDisabled();
				}else if(j == size1Type_rowId || j == size2Type_rowId || j == size3Type_rowId || j == size4Type_rowId
						|| j == size1Text_rowId || j == size2Text_rowId || j == size3Text_rowId || j == size4Text_rowId
						|| j == size1Value_rowId || j == size2Value_rowId || j == size3Value_rowId || j == size4Value_rowId 
						|| j == size1Uom_rowId || j == size2Uom_rowId || j == size3Uom_rowId || j == size4Uom_rowId){
					var sizeFieldVal = gridObj.cellByIndex(i, moreSize_rowId).getValue();
					if(sizeFieldVal == 80131){ // Yes 80130
						disbleFl = true;
					}
				}
				var qualityFl = gridObj.cellById(newRowId, quality_rowId).getValue();
				if (qualityFl == 'Y') {
					disbleFl = true;
				}
				var comboValFl = false;
				if(!disbleFl){
					var e = gridObj.cellByIndex(i, j);
					if (e.combo) {
						for ( var g = e.combo.values, y = 0; y < g.length; y++){
							if (arrValuefromClipboard[m] == e.combo.values[y]) {
								e.setValue(e.combo.keys[y]);
								comboValFl = true;
								g = null;
								break;
							}
						}
						if (!comboValFl){
							e.setValue('');
						}
					}else{
						//gridObj.cellByIndex(i, j).setValue(arrValuefromClipboard[m]);
						gridObj.setCSVDelimiter("\t");
		                gridObj.pasteBlockFromClipboard();
					}
					gridObj.setCellTextStyle(newRowId, j,"color:black;font-weight:normal;");
					// to enable the filed
					if(j == sterMethod_rowId){
						var reqVal = gridObj.cellByIndex(i, reqSter_rowId).getValue();
						if(reqVal == 0){
							gridObj.cellByIndex(i, sterMethod_rowId).setValue('');
						}
					}else if (j == moreSize_rowId){
						var sizeFieldVal = gridObj.cellByIndex(i, j).getValue();
						if(sizeFieldVal == 80130){ // Yes 80130
							fnSizeDisable (newRowId, false);
						}else if(sizeFieldVal == 80131){
							fnSizeDisable (newRowId, true);
						}
					}else if(j == size1Text_rowId || j == size1Type_rowId){
						var type1 = gridObj.cellByIndex(i, size1Type_rowId).getValue();
						fnEnableDisableSizeText (newRowId, size1Text_rowId, type1);
					}else if(j == size2Text_rowId || j == size2Type_rowId){
						var type2 = gridObj.cellByIndex(i, size2Type_rowId).getValue();
						fnEnableDisableSizeText (newRowId, size2Text_rowId, type2);
					}else if(j == size3Text_rowId || j == size3Type_rowId){
						var type3 = gridObj.cellByIndex(i, size3Type_rowId).getValue();
						fnEnableDisableSizeText (newRowId, size3Text_rowId, type3);
					}else if(j == size4Text_rowId || j == size4Type_rowId){
						var type4 = gridObj.cellByIndex(i, size4Type_rowId).getValue();
						fnEnableDisableSizeText (newRowId, size4Text_rowId, type4);
					}
					gridObj.setRowColor(newRowId,"#9FF781");
				}
				m++;
				/*
				 * if(j == rightBottomCol){ break; }
				 */
			}
		 }
		//gridObj._HideSelection();
		gridObj.refreshFilters();
	} else {
		alert(message_prodmgmnt[34]);
	}
}

// Use the below function to past data inside the screen as well as from excel
// to screen.
function addRowFromClipboard() {
	var cbData = gridObj.fromClipBoard();
	// Replace "the" with "a".
	if (cbData == null) {
		Error_Details(message_prodmgmnt[35]);
	}
	fnStartProgress('Y');
	var yesVal = /Yes/g;
	var noVal = /No/g;
	cbData = '	'+cbData;
	cbData = cbData.replace(/\n/g,'\n	');
	//cbData = cbData.replace(yesVal,'80130');
	//cbData = cbData.replace(noVal,'80131');
	// to set sterial values
	/*for ( var i = 0; i < arraysteril.length; i++) {
		SterilCol = arraysteril[i];
		SterilColArr = SterilCol.split(',');
		SterilId = SterilColArr[0];
		SterilNm = SterilColArr[1];
		var regSterVal = new RegExp(SterilNm, "g");
		cbData = cbData.replace(regSterVal,SterilId);
	}
	// type value setter
	for ( var i = 0; i < arraysizetype.length; i++) {
		SizetypeCol = arraysizetype[i];
		SizetypeColArr = SizetypeCol.split(',');
		SizetypeId = SizetypeColArr[0];
		SizetypeNm = SizetypeColArr[1];
		var regTypeVal = new RegExp(SizetypeNm, "g");
		cbData = cbData.replace(regTypeVal,SizetypeId);
	}
	// UOM type 
	for ( var i = 0; i < arraysizeUOM.length; i++) {
		UOMCol = arraysizeUOM[i];
		UOMColArr = UOMCol.split(',');
		UOMId = UOMColArr[0];
		UOMNm = UOMColArr[1];
		var regUomVal = new RegExp(UOMNm, "g");
		cbData = cbData.replace(regUomVal,UOMId);
	}*/
	var rowData = cbData.split("\n");
	var rowcolumnData = '';
	var arrLen = (rowData.length) - 1;
	if (arrLen > maxPdData) {
		Error_Details(Error_Details_Trans(message_prodmgmnt[36],maxPdData));
			}
	if (ErrorCount > 0) {
		fnStopProgress();
		Error_Show();
		Error_Clear();
		return;
	}
	if(!removeEmptyFl){
		removeEmptyFl = true;
		fnRemoveEmptyRow();
	}
	//gridObj.setCSVDelimiter("\t");
	
	setTimeout(function() {	
	gridObj.startFastOperations();
	 for ( var i = 0; i < arrLen; i++) {
	    	
         newRowData = rowData[i];
         var rowcolumnData = '';
         newRowData = ''+newRowData;    
       
         var row_id = gridObj.getUID();
         gridObj.setCSVDelimiter("\t");
         gridObj.setDelimiter("\t");
         gridObj.addRow(row_id, newRowData);
         //gridObj.setRowAttribute(row_id, "row_added", "Y");
         gridObj.setDelimiter(",");
         fnSetDropDownVal(row_id);
   }
	/*if(arrLen > 150){
	document.frmUDIProductDevelopment.btn_submit.disabled = true;
	document.frmUDIProductDevelopment.btn_Void.disabled = true;
	document.frmUDIProductDevelopment.btn_Print.disabled = true;
	}*/
	//gridObj.loadCSVString (cbData);
	//gridObj.setDelimiter(",");
	gridObj.stopFastOperations();
	//var allRowIds = gridObj.getAllRowIds(",");
	//var lastRowId = allRowIds.substring(allRowIds.lastIndexOf(',')+1, allRowIds.length);
	//gridObj.deleteRow(lastRowId);
	gridObj.refreshFilters();
	fnStopProgress();
	},100);
}
// this function used - set the drop down value based on the code id.
function fnSetDropDownVal(row_id) {
	//gridObj.cellById(row_id, check_rowId).setValue(1);
	gridObj.cellById(row_id, contains_rowId).setValue(fnGetYesNoId(gridObj.cellById(row_id, contains_rowId).getValue()));
	gridObj.cellById(row_id, reqSter_rowId).setValue(fnGetYesNoId(gridObj.cellById(row_id, reqSter_rowId).getValue()));
	gridObj.cellById(row_id, sterMethod_rowId).setValue(fnGetSterilId(gridObj.cellById(row_id, sterMethod_rowId).getValue()));
	gridObj.cellById(row_id, moreSize_rowId).setValue(fnGetYesNoId(gridObj.cellById(row_id, moreSize_rowId).getValue()));
	var sizeFieldVal = gridObj.cellById(row_id, moreSize_rowId).getValue();
	reqSterVal = gridObj.cellById(row_id, reqSter_rowId).getValue();
	if(sizeFieldVal != 80130){ // Yes 80130
		fnSizeDisable (row_id, true);
	}else {
		gridObj.cellById(row_id, size1Type_rowId).setValue(fnGetTypeId(gridObj.cellById(row_id, size1Type_rowId).getValue()));
		gridObj.cellById(row_id, size1Uom_rowId).setValue(fnGetUomId(row_id, size1Uom_rowId, size1Type_rowId));
		gridObj.cellById(row_id, size2Type_rowId).setValue(fnGetTypeId(gridObj.cellById(row_id, size2Type_rowId).getValue()));
		gridObj.cellById(row_id, size2Uom_rowId).setValue(fnGetUomId(row_id, size2Uom_rowId, size2Type_rowId));
		gridObj.cellById(row_id, size3Type_rowId).setValue(fnGetTypeId(gridObj.cellById(row_id, size3Type_rowId).getValue()));
		gridObj.cellById(row_id, size3Uom_rowId).setValue(fnGetUomId(row_id, size3Uom_rowId, size3Type_rowId));
		gridObj.cellById(row_id, size4Type_rowId).setValue(fnGetTypeId(gridObj.cellById(row_id, size4Type_rowId).getValue()));
		gridObj.cellById(row_id, size4Uom_rowId).setValue(fnGetUomId(row_id, size4Uom_rowId, size4Type_rowId));
		fnEnableDisableSizeText (row_id, size1Text_rowId, gridObj.cellById(row_id, size1Type_rowId).getValue());
		fnEnableDisableSizeText (row_id, size2Text_rowId, gridObj.cellById(row_id, size2Type_rowId).getValue());
		fnEnableDisableSizeText (row_id, size3Text_rowId, gridObj.cellById(row_id, size3Type_rowId).getValue());
		fnEnableDisableSizeText (row_id, size4Text_rowId, gridObj.cellById(row_id, size4Type_rowId).getValue());
	}
}
// this function used to remove the empty rows
function fnRemoveEmptyRow() {
	gridObj.forEachRow(function(row_id) {
		part_val = gridObj.cellById(row_id, part_rowId).getValue();
		if (part_val == '') {
			gridObj.deleteRow(row_id);
		}
	});
}
// this function used to return the code id based on the code name.
function fnGetYesNoId(textVal) {
	if (textVal != '' && textVal != undefined) {
		if (TRIM(textVal).toUpperCase() == "YES") {
			return "80130";
		} else if (TRIM(textVal).toUpperCase() == "NO") {
			return "80131";
		} else {
			return '';
		}
	}

}
//this function used to return the sterile id based on the code name.
function fnGetSterilId(textVal) {
	var steril_Id = '';

	if (textVal != '' && textVal != undefined) {
		textVal = TRIM(textVal).toUpperCase();
		for ( var i = 0; i < arraysteril.length; i++) {
			SterilFl = false;
			SterilCol = arraysteril[i];
			SterilColArr = SterilCol.split(',');
			SterilId = SterilColArr[0];
			SterilNm = SterilColArr[1];
			if (textVal == (SterilNm).toUpperCase() || textVal == SterilId) {
				steril_Id = SterilId;
				break;
			}
		}
	}

	return steril_Id;
}
//this function used to return the size type id based on the code name.
function fnGetTypeId(textVal) {
	var type_id = '';
	if (textVal != '' && textVal != undefined) {
		// alert('come inside '+ textVal);
		textVal = TRIM(textVal).toUpperCase();
		for ( var i = 0; i < arraysizetype.length; i++) {
			SizetypeFl = false;
			SizetypeCol = arraysizetype[i];
			SizetypeColArr = SizetypeCol.split(',');
			SizetypeId = SizetypeColArr[0];
			SizetypeNm = SizetypeColArr[1];
			if (TRIM(SizetypeNm).toUpperCase() == textVal
					|| SizetypeId == textVal) {
				type_id = SizetypeId;
				break;
			}
		}
	}
	return type_id;
}
// this function used to get the uom id based on code name
function fnGetUomId(rowid, colInd, typeColInd) {
	var typeVal = gridObj.cellById(rowid, typeColInd).getValue();
	var textVal = gridObj.cellById(rowid, colInd).getValue();
	var uom_id = '';

	if (textVal != '' && textVal != undefined) {
		textVal = TRIM(textVal).toUpperCase();
		for ( var i = 0; i < arraysizeUOM.length; i++) {
			UOMCol = arraysizeUOM[i];
			UOMColArr = UOMCol.split(',');
			UOMId = UOMColArr[0];
			UOMNm = UOMColArr[1];
			if (TRIM(UOMNm).toUpperCase() == textVal || UOMId == textVal) {
				uom_id = UOMId;
				break;
			}
		}
	}
	return uom_id;
}
// Use the below function to remove the rows from the grid.
function removeSelectedRow() {
	var gridrows = gridObj.getSelectedRowId();
	if (gridrows != undefined) {
		gridObj.startFastOperations();
		var NameCellValue = '';
		var gridrowsarr = gridrows.toString().split(",");
		var added_row;
		for ( var rowid = 0; rowid < gridrowsarr.length; rowid++) {
			gridrowid = gridrowsarr[rowid];
			var delete_partVal = gridObj.cellById(gridrowid, part_rowId).getValue();
			var pdIdVal = gridObj.cellById(gridrowid, pd_rowId).getValue();
			var delete_qualityVal = gridObj.cellById(gridrowid, quality_rowId).getValue();
			// if row exist then form the string
			// quality values always come Y or N
			if(pdIdVal ==''){
				gridObj.deleteRow(gridrowid);
			}else{
				NameCellValue = NameCellValue + delete_partVal+', ';
			}
		}
		gridObj.stopFastOperations();
	} else {
		Error_Details(message_prodmgmnt[37]);
	}
	fnAddErrorDetails ((message[38],NameCellValue));
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	gridObj.refreshFilters();
}
// this function used to validate the all the grid data values.
function fnValidate() {
	var check_id = '';
	var contain_id = '';
	var oneProjectFl = true;
	var firstProjectId = '';
	// validation check
	var emptyPartStr = '';
	var emptyPartDesStr = '';
	var emptyContainsLatStr = '';
	var emptyReqSterStr = '';
	var emptySterMethodStr = '';
	var emptyMoreSizeStr = '';
	var emptySizeTypeStr = '';
	var emptyType1TextStr = '';
	var emptyType2TextStr = '';
	var emptyType3TextStr = '';
	var emptyType4TextStr = '';
	var emptySize1Value = '';
	var emptySize2Value = '';
	var emptySize3Value = '';
	var emptySize4Value = '';
	var emptySize1UOM = '';
	var emptySize2UOM = '';
	var emptySize3UOM = '';
	var emptySize4UOM = '';
	// duplicate
	
	var wrongUOM1Str = '';
	var wrongUOM2Str = '';
	var wrongUOM3Str = '';
	var wrongUOM4Str = '';
	var wrongSizeValue1Str = '';
	var wrongSizeValue2Str = '';
	var wrongSizeValue3Str = '';
	var wrongSizeValue4Str = '';
	var typeSelectFl = false;
	notAnumberFl = false;
	emptyFl = false;
	wrongUOMFl = false;
	gridObj.startFastOperations();
	this.keyArray = new Array();
	this.valArray = new Array();
	gridObj.forEachRowA(function(rowId) {
		checkIdVal = gridObj.cellById(rowId, check_rowId).getValue();
		if (checkIdVal == 1) {
			pdIdVal = gridObj.cellById(rowId, pd_rowId).getValue();
			partIdVal = gridObj.cellById(rowId, part_rowId).getValue();
			projectIdVal = gridObj.cellById(rowId, project_rowId).getValue();
			partdesIdVal = gridObj.cellById(rowId, partdes_rowId).getValue();
			containsIdVal = gridObj.cellById(rowId, contains_rowId).getValue();
			reqSterIdVal = gridObj.cellById(rowId, reqSter_rowId).getValue();
			sterMethodIdVal = gridObj.cellById(rowId, sterMethod_rowId).getValue();
			moreSizeIdVal = gridObj.cellById(rowId, moreSize_rowId).getValue();
			size1TypeIdVal = gridObj.cellById(rowId, size1Type_rowId).getValue();
			size1TextIdVal = gridObj.cellById(rowId, size1Text_rowId).getValue();
			size1ValueIdVal = gridObj.cellById(rowId, size1Value_rowId).getValue();
			size1UomIdVal = gridObj.cellById(rowId, size1Uom_rowId).getValue();

			size2TypeIdVal = gridObj.cellById(rowId, size2Type_rowId).getValue();
			size2TextIdVal = gridObj.cellById(rowId, size2Text_rowId).getValue();
			size2ValueIdVal = gridObj.cellById(rowId, size2Value_rowId).getValue();
			size2UomIdVal = gridObj.cellById(rowId, size2Uom_rowId).getValue();

			size3TypeIdVal = gridObj.cellById(rowId, size3Type_rowId).getValue();
			size3TextIdVal = gridObj.cellById(rowId, size3Text_rowId).getValue();
			size3ValueIdVal = gridObj.cellById(rowId, size3Value_rowId).getValue();
			size3UomIdVal = gridObj.cellById(rowId, size3Uom_rowId).getValue();

			size4TypeIdVal = gridObj.cellById(rowId, size4Type_rowId).getValue();
			size4TextIdVal = gridObj.cellById(rowId, size4Text_rowId).getValue();
			size4ValueIdVal = gridObj.cellById(rowId, size4Value_rowId).getValue();
			size4UomIdVal = gridObj.cellById(rowId, size4Uom_rowId).getValue();
			
			typeSelectFl = false;
			if (partIdVal == '') {
				emptyPartStr = emptyPartStr + partIdVal + ', ';
			}else{
				/*fnValidateMandatoryField(projectIdVal, 'Dropdown', partIdVal, rowId, project_rowId);
				fnValidateMandatoryField(partdesIdVal, 'Textbox', partIdVal, rowId, partdes_rowId);
				fnValidateMandatoryField(containsIdVal, 'Dropdown', partIdVal, rowId, contains_rowId);
				fnValidateMandatoryField(reqSterIdVal, 'Dropdown', partIdVal, rowId, reqSter_rowId);
				fnValidateMandatoryField(moreSizeIdVal, 'Dropdown', partIdVal, rowId, moreSize_rowId);
				// 80130 Yes
				// 80131 No
				if (reqSterIdVal == 80130) {
					fnValidateMandatoryField(sterMethodIdVal, 'Dropdown', partIdVal, rowId, sterMethod_rowId);
				}
				
				// device size - Yes
				if(moreSizeIdVal == 80130){
						if(size1TypeIdVal !='' && size1TypeIdVal !='0'){
							//size 1 validation
							//emptySizeTypeStr = fnValidateEmptyField(size1TypeIdVal, 'Dropdown', emptySizeTypeStr, partIdVal, rowId, size1Type_rowId);
							fnValidateMandatoryField(size1ValueIdVal, 'Textbox', partIdVal, rowId, size1Value_rowId);
							fnValidateMandatoryField(size1UomIdVal, 'Dropdown', partIdVal, rowId, size1Uom_rowId);
							fnValidateSizeValue (size1ValueIdVal, partIdVal, rowId, size1Value_rowId);
						// 104047 Others, Specify
							if (size1TypeIdVal == 104047) {
								fnValidateMandatoryField(size1TextIdVal, 'Textbox', partIdVal, rowId, size1Text_rowId);
								} else {
								fnValidateUOM(size1TypeIdVal, size1UomIdVal, partIdVal,rowId,size1Uom_rowId);
							}
							typeSelectFl = true;
						}
					// size 2 validation.
						//emptySizeTypeStr = fnValidateEmptyField(size2TypeIdVal, 'Dropdown', emptySizeTypeStr, partIdVal, rowId, size2Type_rowId);
					if(size2TypeIdVal !='' && size2TypeIdVal !='0'){
						fnValidateMandatoryField(size2ValueIdVal, 'Textbox',  partIdVal, rowId, size2Value_rowId);
						fnValidateMandatoryField(size2UomIdVal, 'Dropdown',  partIdVal, rowId, size2Uom_rowId);
						fnValidateSizeValue (size2ValueIdVal, partIdVal, rowId, size2Value_rowId);
						if (size2TypeIdVal == 104047) {
							fnValidateMandatoryField(size2TextIdVal, 'Textbox', partIdVal, rowId, size2Text_rowId);
							} else {
								fnValidateUOM(size2TypeIdVal, size2UomIdVal, partIdVal, rowId,size2Uom_rowId);
						}
						typeSelectFl = true;
					}
					// size 3 validation
					//emptySizeTypeStr = fnValidateEmptyField(size3TypeIdVal, 'Dropdown', emptySizeTypeStr, partIdVal, rowId, size3Type_rowId);
					if(size3TypeIdVal !='' && size3TypeIdVal !='0'){
						fnValidateMandatoryField(size3ValueIdVal, 'Textbox',  partIdVal, rowId, size3Value_rowId);
						fnValidateMandatoryField(size3UomIdVal, 'Dropdown',  partIdVal, rowId, size3Uom_rowId);
						fnValidateSizeValue (size3ValueIdVal, partIdVal, rowId, size3Value_rowId);
						if (size3TypeIdVal == 104047) {
							fnValidateMandatoryField(size3TextIdVal, 'Textbox', partIdVal, rowId, size3Text_rowId);
							} else {
								fnValidateUOM(size3TypeIdVal, size3UomIdVal, partIdVal, rowId,size3Uom_rowId);
						}
						typeSelectFl = true;
					}
					// size 4 validation
					//emptySizeTypeStr = fnValidateEmptyField(size4TypeIdVal, 'Dropdown', emptySizeTypeStr, partIdVal, rowId, size4Type_rowId);
					if(size4TypeIdVal !='' && size4TypeIdVal !='0'){
						fnValidateMandatoryField(size4ValueIdVal, 'Textbox', partIdVal, rowId, size4Value_rowId);
						fnValidateMandatoryField(size4UomIdVal, 'Dropdown', partIdVal, rowId, size4Uom_rowId);
						fnValidateSizeValue (size4ValueIdVal, partIdVal, rowId, size4Value_rowId);
						if (size4TypeIdVal == 104047) {
							fnValidateMandatoryField(size4TextIdVal, 'Textbox', partIdVal, rowId, size4Text_rowId);
							} else {
							 fnValidateUOM(size4TypeIdVal, size4UomIdVal, partIdVal, rowId,size4Uom_rowId);
							}
						typeSelectFl = true;
						}
					if(!typeSelectFl){
						emptySizeTypeStr = emptySizeTypeStr + partIdVal +', ';
					}
			}
			}
			containsIdVal = fnCorrectValDropdown(containsIdVal, rowId, contains_rowId);
			reqSterIdVal = fnCorrectValDropdown(reqSterIdVal, rowId, reqSter_rowId);
			sterMethodIdVal = fnCorrectValDropdown(sterMethodIdVal, rowId, sterMethod_rowId);
			moreSizeIdVal = fnCorrectValDropdown(moreSizeIdVal, rowId, moreSize_rowId);
			size1TypeIdVal = fnCorrectValDropdown(size1TypeIdVal, rowId, size1Type_rowId);
			size1UomIdVal = fnCorrectValDropdown(size1UomIdVal, rowId, size1Uom_rowId);
			size2TypeIdVal = fnCorrectValDropdown(size2TypeIdVal, rowId, size2Type_rowId);
			size2UomIdVal = fnCorrectValDropdown(size2UomIdVal, rowId, size2Uom_rowId);
			size3TypeIdVal = fnCorrectValDropdown(size3TypeIdVal, rowId, size3Type_rowId);
			size3UomIdVal = fnCorrectValDropdown(size3UomIdVal, rowId, size3Uom_rowId);
			size4TypeIdVal = fnCorrectValDropdown(size4TypeIdVal, rowId, size4Type_rowId);
			size4UomIdVal = fnCorrectValDropdown(size4UomIdVal, rowId, size4Uom_rowId);*/
			
			// forming an inputstr to save udi values
			finalInputStr = finalInputStr + pdIdVal +'^'+ partIdVal + '^' + projectIdVal + '^'
					+ partdesIdVal + '^' + containsIdVal + '^'
					+ reqSterIdVal + '^' + sterMethodIdVal + '^' + moreSizeIdVal
					+ '^' + size1TypeIdVal + '^' + size1TextIdVal + '^'
					+ size1ValueIdVal + '^' + size1UomIdVal + '^' + size2TypeIdVal
					+ '^' + size2TextIdVal + '^' + size2ValueIdVal + '^'
					+ size2UomIdVal + '^' + size3TypeIdVal + '^' + size3TextIdVal
					+ '^' + size3ValueIdVal + '^' + size3UomIdVal + '^'
					+ size4TypeIdVal + '^' + size4TextIdVal + '^'
					+ size4ValueIdVal + '^' + size4UomIdVal + '^' + rowId +'|';
			finalPdId = finalPdId +rowId+',';
		}
		}

		// alert('Check row id '+check_id);
		// alert('contain_id row id '+containsIdVal);
	});
	gridObj.stopFastOperations();
	/*if (!oneProjectFl) {
		Error_Details(message_prodmgmnt[52]);
	}
	if (emptyPartStr != '') {
		emptyPartStr = emptyPartStr.substring(0, (emptyPartStr.length) - 2);
		Error_Details(message_prodmgmnt[57]);
	}
	if(emptyFl){
		Error_Details(message_prodmgmnt[58]);
	}
	if(wrongUOMFl){
		Error_Details(message_prodmgmnt[59]);
	}
	// validate all the grid fileds.
	fnAddErrorDetails ('Please remove the following duplicate part(s): <b>', duplicatePartStr);
	fnAddErrorDetails ('Please remove the following existing part(s): <b>', existingPartStr);
	fnAddErrorDetails ('<b>Project ID</b> cannot be left blank. Please select an option from the drop down: <b>', emptyProjectStr);
	fnAddErrorDetails ('Please contact Quality team to create this <b>new project</b> for following part(s): <b>', nonProjectStr);
	fnAddErrorDetails ('<b>Part Description</b> cannot be left blank. Please enter valid data: <b>', emptyPartDesStr);
	fnAddErrorDetails ('<b>Contains Latex? </b> cannot be left blank. Please select an option from the drop down: <b>', emptyContainsLatStr);
	fnAddErrorDetails ('<b>Requires sterilization prior to use</b> cannot be left blank. Please select an option from the drop down: <b>', emptyReqSterStr);
	fnAddErrorDetails ('<b>Is Device available in more than one size</b> cannot be left blank. Please select an option from the drop down: <b>', emptyMoreSizeStr);
	fnAddErrorDetails ('<b>Size Type</b> cannot be left blank. Please select an option from the drop down: <b>', emptySizeTypeStr);
	fnAddErrorDetails ('<b>Sterilization method</b> cannot be left blank. Please select an option from the drop down: <b>', emptySterMethodStr);
	fnAddErrorDetails ('<b>Size1 Text</b> cannot be left blank. Please enter the valid data: <b>', emptyType1TextStr);
	fnAddErrorDetails ('<b>Size2 Text</b> cannot be left blank. Please enter the valid data: <b>', emptyType2TextStr);
	fnAddErrorDetails ('<b>Size3 Text</b> cannot be left blank. Please enter the valid data: <b>', emptyType3TextStr);
	fnAddErrorDetails ('<b>Size4 Text</b> cannot be left blank. Please enter the valid data: <b>', emptyType4TextStr);
	fnAddErrorDetails ('<b>Size1 Value</b> cannot be left blank. Please enter the valid data: <b>', emptySize1Value);
	fnAddErrorDetails ('<b>Size2 Value</b> cannot be left blank. Please enter the valid data: <b>', emptySize2Value);
	fnAddErrorDetails ('<b>Size3 Value</b> cannot be left blank. Please enter the valid data: <b>', emptySize3Value);
	fnAddErrorDetails ('<b>Size4 Value</b> cannot be left blank. Please enter the valid data: <b>', emptySize4Value);
	fnAddErrorDetails ('<b>Size1 UOM</b> cannot be left blank. Please select an option from the drop down: <b>', emptySize1UOM);
	fnAddErrorDetails ('<b>Size2 UOM</b> cannot be left blank. Please select an option from the drop down: <b>', emptySize2UOM);
	fnAddErrorDetails ('<b>Size3 UOM</b> cannot be left blank. Please select an option from the drop down: <b>', emptySize3UOM);
	fnAddErrorDetails ('<b>Size4 UOM</b> cannot be left blank. Please select an option from the drop down: <b>', emptySize4UOM);
	fnAddErrorDetails ('Please select valid type 1 UOM field for following part number(s): <b>', wrongUOM1Str);
	fnAddErrorDetails ('Please select valid type 2 UOM field for following part number(s): <b>', wrongUOM2Str);
	fnAddErrorDetails ('Please select valid type 3 UOM field for following part number(s): <b>', wrongUOM3Str);
	fnAddErrorDetails ('Please select valid type 4 UOM field for following part number(s): <b>', wrongUOM4Str);
	fnAddErrorDetails ('Please enter the numeric number for <b> Size1 Value</b> field: <b>', wrongSizeValue1Str);
	fnAddErrorDetails ('Please enter the numeric number for <b> Size2 Value</b> field: <b>', wrongSizeValue2Str);
	fnAddErrorDetails ('Please enter the numeric number for <b> Size3 Value</b> field: <b>', wrongSizeValue3Str);
	fnAddErrorDetails ('Please enter the numeric number for <b> Size4 Value</b> field: <b>', wrongSizeValue4Str);
	if(notAnumberFl){
		Error_Details (message_prodmgmnt[60]);
	}
	
	if (ErrorCount > 0) {
		fnStopProgress();
		document.frmUDIProductDevelopment.btn_submit.disabled = false;
		Error_Show();
		Error_Clear();
		return false;
	}*/
	return true;
}
//this function is used to highlight the error values in the cell
function fnErrorCellStyle(rowId,indexId){
	gridObj.setCellTextStyle(rowId,indexId,"color:red;font-weight:bold;border:2px solid red;");
}

function fnAddErrorDetails(errorMessage, errorPartStr) {
	if (errorPartStr != '') {
		errorPartStr = errorPartStr.substring(0, (errorPartStr.length) - 2);
		Error_Details(errorMessage + errorPartStr + '</b>');
	}
}

function fnValidateEmptyField(fieldVal, fieldType, emptyString, appendVal,rowId,indexId) {
	if (fieldType == 'Dropdown') {
		if (fieldVal == '' || fieldVal == '0') {
			emptyString = emptyString + appendVal + ', ';
			emptyFl = true;
			fnErrorCellStyle(rowId,indexId);
		}
	} else {
		if (fieldVal == '') {
			emptyString = emptyString + appendVal + ', ';
			emptyFl = true;
			fnErrorCellStyle(rowId,indexId);
		}
	}
	return emptyString;
}
function fnValidateMandatoryField(fieldVal, fieldType, appendVal,rowId,indexId) {
	if (fieldType == 'Dropdown') {
		if (fieldVal == '' || fieldVal == '0') {
			emptyFl = true;
			fnErrorCellStyle(rowId,indexId);
		}
	} else {
		if (fieldVal == '') {
			emptyFl = true;
			fnErrorCellStyle(rowId,indexId);
		}
	}
}


function fnValidateUOM(sizeType, sizeUOM, appendVal,rowId,indexId) {
	if (sizeType == '' || sizeType == 0 || sizeUOM =='' || sizeUOM == 0) {
		return false;
	}
	var uomFl = false;
	// 104032 Circumference
	// 104033 Depth
	// 104035 Outer Diameter
	// 104036 Height
	// 104037 Length
	// 104038 Lumen/Inner Diameter
	// 104041 Width
	// 104044 Pore Size
	if (sizeType == 104032 || sizeType == 104033 || sizeType == 104035
			|| sizeType == 104036 || sizeType == 104037 || sizeType == 104038
			|| sizeType == 104041 || sizeType == 104044) {
		fnLoopArray(arrayLength, sizeUOM, appendVal,rowId,indexId);
	}else
	// 104045 Area/Surface Area
	if (sizeType == 104045) {
		fnLoopArray(arrayArea, sizeUOM, appendVal,rowId,indexId);
	}else
	// 104042 Weight
	if (sizeType == 104042) {
		fnLoopArray(arrayWeight, sizeUOM, appendVal,rowId,indexId);
	}else
	// 104040 Total Volume
	if (sizeType == 104040) {
		fnLoopArray(arrayTotalVol, sizeUOM, appendVal,rowId,indexId);
	}else
	// 104034 Catheter Gauge
	// 104039 Needle Gauge	
	if (sizeType == 104034 || sizeType == 104039) {
		fnLoopArray(arrayGauge, sizeUOM, appendVal,rowId,indexId);
	}else
	// 104046 Angle
	if (sizeType == 104046) {
		fnLoopArray(arrayAngle, sizeUOM, appendVal,rowId,indexId);
	}else
	// 104043 Pressure
	if (sizeType == 104043) {
		fnLoopArray(arrayPressure, sizeUOM, appendVal,rowId,indexId);
	}
}

function fnLoopArray(arrayObj, codeId, appendVal,rowId,indexId) {
	var avilableFl = false;
	if (codeId != '' || codeId != '0') {
		for ( var i = 0; i < arrayObj.length; i++) {
			arrayObjCol = arrayObj[i];
			arrayObjColArr = arrayObjCol.split(',');
			arrayCodeId = arrayObjColArr[0];
			arrayCodeNm = arrayObjColArr[1];
			if (arrayCodeId == codeId) {
				avilableFl = true;
				break;
			}
		}
	}
	if (!avilableFl) {
		wrongUOMFl = true;
		fnErrorCellStyle(rowId,indexId);
	}
}

function fnShowImage(loader) {
	var response = loader.xmlDoc.responseText;
	if (response == 1) {
		Error_Details(message_prodmgmnt[39]);
	}
	return false;
}
// this function used to form the string and submit the data.
function fnSubmit() {
	var inputStr = '';
	existingPartStr = '';
	emptyProjectStr = '';
	duplicatePartStr ='';
	nonProjectStr = '';
	finalInputStr = '';
	finalPdId = '';
	var duplicatePartFl = false;
	var invalidProject = false;
	var errorFl = false;
	var mandatoryFl = false;
	var qualityPartFl = false;
	var partExistFl = false;
	var partMaxLenFl = false;
	var projectErrFl = false;
	var uomemptyemptyfl = false;
	var valueemptyfl = false;
	

	document.frmUDIProductDevelopment.btn_submit.disabled = true;
	gridObj.editStop();
	fnStartProgress('Y');
	setTimeout(function() {
	if (fnValidate()) {
		if (finalInputStr == '') {
			Error_Details(message_prodmgmnt[40]);
		}
		
		var rowLen = finalPdId.split(',').length;
		if ((rowLen-1) > maxPdData) {
			Error_Details(Error_Details_Trans(message_prodmgmnt[41],maxPdData));
		}
		
		if (ErrorCount > 0) {
			fnStopProgress();
			document.frmUDIProductDevelopment.btn_submit.disabled = false;
			Error_Show();
			Error_Clear();
			return false;
		}
		document.frmUDIProductDevelopment.deletePartStr.value = '';
		document.frmUDIProductDevelopment.strOpt.value = 'Save';
		// to encode the String [When enter part des. - & char not allow to save issue fix]
		finalInputStr = encodeURIComponent(finalInputStr);
		document.frmUDIProductDevelopment.inputStr.value = finalInputStr;
		document.frmUDIProductDevelopment.deletePartStr.value = '';
		//document.frmUDIProductDevelopment.submit();
		
		var loader = dhtmlxAjax.postSync('/gmUDIProductDevelopment.do?method=pdUDISetup','strOpt=Save&inputStr='+finalInputStr +'&'+fnAppendCompanyInfo());
		
		var status = loader.xmlDoc.status;
		if(status == 300) {
			var response = loader.xmlDoc.responseText;
				var errorPart = response.split('|');
				var errSize = errorPart.length; 
				for(i=0;i<errSize-1;i++){
					if(errorPart[i] !=''){
						currRow = errorPart[i].split('^');
						currlen = currRow.length;
						if(currlen>1){
							for(j=1,m=2;j<currlen;j+=2,m+=2){
								if(currRow[j] != ''){
									/*if(currRow[j] =='part' && currRow[m] == 'duplicate'){
										duplicatePartFl = true;
									}else if(currRow[j] =='project' && currRow[m] == 'invalid'){
										invalidProject = true;
									}*/
									
										if(currRow[m] == 'mandatory'){
											mandatoryFl = true;
										}else if (currRow[m] == 'duplicate'){
											duplicatePartFl = true;
										}else if (currRow[m] == 'invalid'){
											invalidProject = true;
										}else if (currRow[m] == 'qualityPart'){
											qualityPartFl = true;
										}else if (currRow[m] == 'alreadyExist'){
											partExistFl = true;
										}else if (currRow[m] == 'maxLen'){
											partMaxLenFl = true;
										}else if(currRow[m] == 'invalidProject'){
											projectErrFl = true;
										}else if(currRow[m] == 'uomempty'){
											uomemptyemptyfl= true;
										}else if(currRow[m] == 'valueempty'){									
											valueemptyfl= true;
										}
										//gridObj.setRowColor(currRow[0],"F5A9A9");
										gridObj.setCellTextStyle(currRow[0],gridObj.getColIndexById(currRow[j]),"color:red;font-weight:bold;border:2px solid red;");
									}
								} 
							}
						}
					}
				
				/*if(duplicatePartFl != true){
					errorFl = true;
				}*/
			/*dhtmlx.message({
			    text: "The entered/selected data was incorrect, please correct the data and try again.",
			    expire:2000,
			    type:"myErrorCss" // 'customCss' - css class
			});*/
		}else
		if (status == 200) {
			var response = loader.xmlDoc.responseText;
			var successParts = response.split('|');
			var succSize = successParts.length; 
			for(i=0;i<succSize-1;i++){
				if(successParts[i] !=''){
					currRow = successParts[i].split('^');
					gridObj.cellById(currRow[0], check_rowId).setValue();	
					gridObj.setRowColor(currRow[0],"");
					gridObj.setCellTextStyle(currRow[0], part_rowId,"color:green;font-weight:bold;");
					gridObj.cellById(currRow[0], pd_rowId).setValue(currRow[1]);
	   				gridObj.changeRowId (currRow[0], currRow[1]);
					}
				}
			document.getElementById("progress").style.display="block";
			document.getElementById("msg").style.color="green";
			document.getElementById("msg").style.display="inline";		
			document.getElementById("msg").innerHTML="Record saved successfully.";
			setTimeout(function fnexpiretime(){
				document.getElementById("progress").style.display='none';
				document.getElementById("msg").style.display = 'none';
			}, 4000);
			//document.getElementById("saveLine1").style.display='block';
			//document.getElementById("saveSuccessfully").style.display='block';
			/*dhtmlx.message({
			    text:"Record saved successfully!",
			    expire:1000,
			    type:"myCss" // 'customCss' - css class
			});*/
			var objControl = eval("document.frmUDIProductDevelopment.selectAll");
			if(objControl.checked){
				objControl.checked = false;
			}
		}
	}
	fnStopProgress();
	document.frmUDIProductDevelopment.btn_submit.disabled = false;
	if(duplicatePartFl){
		Error_Details(message_prodmgmnt[42]);
	}
	if(qualityPartFl){
		Error_Details(message_prodmgmnt[43]);
	}
	if(partExistFl){
		Error_Details(message_prodmgmnt[44]);
	}
	if(partMaxLenFl){
		Error_Details(message_prodmgmnt[45]);
	}
	if(projectErrFl){
		Error_Details(message_prodmgmnt[46]);
	}
	if(mandatoryFl){
		Error_Details(message_prodmgmnt[47]);
	}
	if(invalidProject){
		Error_Details(message_prodmgmnt[48]);
	}
	if(errorFl){
		Error_Details(message_prodmgmnt[49]);
	}
	if(uomemptyemptyfl){
		Error_Details(message_prodmgmnt[50]);
	}
	if(valueemptyfl){
		Error_Details(message_prodmgmnt[51]);
	}
	
	if (ErrorCount > 0) {
		document.getElementById("progress").style.display="none";
		document.getElementById("msg").style.display="none";		
		Error_Show();
		Error_Clear();
		return false;
	}
	},100);
}
// this fucntion used to pop up the selected parts to print
function fnPrint() {
	var inputStr = '';
	var printProject = '';
	var oneProjectFl = true;
	var firstProjectId = '';
	var existingDataFl = false;
	gridObj.startFastOperations();
	gridObj.forEachRowA(function(rowId) {
		checkIdVal = gridObj.cellById(rowId, check_rowId).getValue();
		pdIdVal = gridObj.cellById(rowId, pd_rowId).getValue();
		partIdVal = gridObj.cellById(rowId, part_rowId).getValue();
		projectIdVal = gridObj.cellById(rowId, project_rowId).getValue();
		qualityVal = gridObj.cellById(rowId, quality_rowId).getValue();
		// to get the selected part #
		if (checkIdVal == 1 && partIdVal!='') {
			// validate the same project id
			if (firstProjectId == '') {
				firstProjectId = projectIdVal;
			}
			if (firstProjectId != projectIdVal) {
				oneProjectFl = false;
			}
			inputStr = inputStr + partIdVal + ",";
			if (printProject == '') {
				printProject = firstProjectId;
			}
			if (pdIdVal != '') {
				existingDataFl = true;
			}
		}
	});
	gridObj.stopFastOperations();
	if (!oneProjectFl) {
		Error_Details(message_prodmgmnt[52]);
	}
	if (inputStr == '') {
		Error_Details(message_prodmgmnt[53]);
	}
	if(!existingDataFl){
		Error_Details(message_prodmgmnt[54]);
	}
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	
	pophtml = '<html><body><form name="frmUDIProductDevelopment" method="post" action="/gmUDIProductDevelopment.do?method=pdUDISetupPrint&strOpt=Print">';
	pophtml +='<input type=hidden name=partNumStr value="'+inputStr+'"/>';
	pophtml +='<input type=hidden name=printProjectId value="'+printProject+'"/>';
	pophtml +='</form></body><script>document.frmUDIProductDevelopment.submit();</script></html>';
	winpPop=window.open("/gmUDIProductDevelopment.do?method=pdUDISetupPrint&strOpt=Print","script","resizable=yes,scrollbars=yes,top=200,left=300,width=970,height=700");
	winpPop.document.writeln(pophtml);
	winpPop.show;
}

this.keyArray = new Array();
this.valArray = new Array();
this.put = put;
this.get = get;
this.findIt = findIt;
// this fucntion used to put tha value into array
function put(key, val) {
	var elementIndex = this.findIt(key);
	if (elementIndex == (-1)) {
		this.keyArray.push(key);
		this.valArray.push(val);
	} else {
		this.valArray[elementIndex] = val;
	}
}
// this fucntion used to get the values based on key
function get(key) {
	var result = null;
	var elementIndex = this.findIt(key);
	if (elementIndex != (-1)) {
		result = this.valArray[elementIndex];
	} else {
		result = "";
	}
	return result;
}
//this function used to find the key availablity.
function findIt(key) {
	var result = (-1);

	for ( var i = 0; i < this.keyArray.length; i++) {
		if (this.keyArray[i] == key) {
			result = i;
			break;
		}
	}
	return result;
}
// this function used to validate the part # and project id (AJAX).
function fnCallAjax(partNum, projectVal) {
	var ajaxUrl = fnAjaxURLAppendCmpInfo("/gmUDIProductDevelopment.do?method=validatePartNumber&pnum="
			+ encodeURIComponent(partNum)
			+ "&project="
			+ encodeURIComponent(projectVal)
			+ "&ramdomId="
			+ Math.random());
	var loader = dhtmlxAjax.getSync(ajaxUrl);
	var status = loader.xmlDoc.status;
	if (status == 200) { // On success stauts parsing the response values.
		var responseText = loader.xmlDoc.responseXML;
		var data = responseText.getElementsByTagName("data");
		var partVal = data[0].childNodes[0].text;
		var projectId = data[0].childNodes[1].text;
		if (partVal != 0) {
			existingPartStr = existingPartStr + partNum + ', ';
		}
		if (projectVal !='' && projectVal !=0 && projectId != 1) {
			nonProjectStr = nonProjectStr + partNum +', ';
		}

	} else {
		existingPartStr = existingPartStr + partNum + ', ';
	}
}
// this function used to validate the numbers
function fnValidateSizeValue(fieldVal, appendVal, rowId, indexId){
	var wrongVal = '';
	if(fieldVal !='')
	{
		wrongVal = fieldVal.replace(/-?[.0-9]+/g,''); // value text box
		if(wrongVal.length >0){
			emptyFl = true;
			fnErrorCellStyle(rowId,indexId);
		}
	}
}
//to return the dropdown values
function fnCorrectValDropdown (selectedVal, rowId, cellId){
	var correctVal = selectedVal;
	if(correctVal == 0){
		correctVal = '';
	}
	if(correctVal !='' && !notAnumberFl){
		notAnumberFl = isNaN (correctVal);
	}
	if(correctVal !=''){
		if(isNaN (correctVal)){
			fnErrorCellStyle(rowId,cellId);
		}
	}
	return correctVal;
}
// this function is used to verify the size filed.
function fnSizeDisable(row_id, blFlag){
	if(blFlag){
		gridObj.cellById(row_id, size1Type_rowId).setValue('');
		gridObj.cellById(row_id, size1Text_rowId).setValue('');
		gridObj.cellById(row_id, size1Value_rowId).setValue('');
		gridObj.cellById(row_id, size1Uom_rowId).setValue('');
		gridObj.cellById(row_id, size2Type_rowId).setValue('');
		gridObj.cellById(row_id, size2Text_rowId).setValue('');
		gridObj.cellById(row_id, size2Value_rowId).setValue('');
		gridObj.cellById(row_id, size2Uom_rowId).setValue('');
		gridObj.cellById(row_id, size3Type_rowId).setValue('');
		gridObj.cellById(row_id, size3Text_rowId).setValue('');
		gridObj.cellById(row_id, size3Value_rowId).setValue('');
		gridObj.cellById(row_id, size3Uom_rowId).setValue('');
		gridObj.cellById(row_id, size4Type_rowId).setValue('');
		gridObj.cellById(row_id, size4Text_rowId).setValue('');
		gridObj.cellById(row_id, size4Value_rowId).setValue('');
		gridObj.cellById(row_id, size4Uom_rowId).setValue('');
	}
	gridObj.cellById(row_id, size1Type_rowId).setDisabled(blFlag);
	//gridObj.cellById(row_id, size1Text_rowId).setDisabled(blFlag);
	gridObj.cellById(row_id, size1Value_rowId).setDisabled(blFlag);
	gridObj.cellById(row_id, size1Uom_rowId).setDisabled(blFlag);
	gridObj.cellById(row_id, size2Type_rowId).setDisabled(blFlag);
	//gridObj.cellById(row_id, size2Text_rowId).setDisabled(blFlag);
	gridObj.cellById(row_id, size2Value_rowId).setDisabled(blFlag);
	gridObj.cellById(row_id, size2Uom_rowId).setDisabled(blFlag);
	gridObj.cellById(row_id, size3Type_rowId).setDisabled(blFlag);
	//gridObj.cellById(row_id, size3Text_rowId).setDisabled(blFlag);
	gridObj.cellById(row_id, size3Value_rowId).setDisabled(blFlag);
	gridObj.cellById(row_id, size3Uom_rowId).setDisabled(blFlag);
	gridObj.cellById(row_id, size4Type_rowId).setDisabled(blFlag);
	//gridObj.cellById(row_id, size4Text_rowId).setDisabled(blFlag);
	gridObj.cellById(row_id, size4Value_rowId).setDisabled(blFlag);
	gridObj.cellById(row_id, size4Uom_rowId).setDisabled(blFlag);
}
// Description : This function to check the select all (master check box)
function doOnCheck(rowId, cellInd, stage) {
	rowID = rowId;
	if (cellInd == check_rowId) {
		fnSelectAllToggle('selectAll');
	}
	return true;
}
// Description : This function to check the select all check box
function fnSelectAllToggle(varControl) {
	var objControl = eval("document.frmUDIProductDevelopment." + varControl);

	var all_row = gridObj.getAllRowIds(",");
	var all_rowLen = all_row.length; // get the rows id length
	var checked_row = gridObj.getCheckedRows(check_rowId);
	var quality_row = '';
	var finalval;
	var qualityVal = '';

	gridObj.forEachRow(function(rowId) {
		qualityVal = gridObj.cellById(rowId, quality_rowId).getValue();
		if (qualityVal == 'Y') {
			quality_row = quality_row + rowId + ',';
		}
	});

	finalval = eval(checked_row.length) + eval(quality_row.length);
	if (all_rowLen == finalval) {
		objControl.checked = true;// select all check box to be checked
	} else {
		objControl.checked = false;// select all check box un checked.
	}
}
// this functio used to call the combo - field function
function fnSetUomDropDown (combObj, sizeType){
	// 104032 Circumference
	// 104033 Depth
	// 104035 Outer Diameter
	// 104036 Height
	// 104037 Length
	// 104038 Lumen/Inner Diameter
	// 104041 Width
	// 104044 Pore Size
	var newArrayObj = new Array();
	if (sizeType == 104032 || sizeType == 104033 || sizeType == 104035
			|| sizeType == 104036 || sizeType == 104037 || sizeType == 104038
			|| sizeType == 104041 || sizeType == 104044) {
		newArrayObj = fnSetUomArray(arrayLength, combObj);
	}else
	// 104045 Area/Surface Area
	if (sizeType == 104045) {
		newArrayObj = fnSetUomArray(arrayArea, combObj);
	}else
	// 104042 Weight
	if (sizeType == 104042) {
		newArrayObj = fnSetUomArray(arrayWeight, combObj);
	}else
	// 104040 Total Volume
	if (sizeType == 104040) {
		newArrayObj = fnSetUomArray(arrayTotalVol, combObj);
	}else
	// 104034 Catheter Gauge
	// 104039	Needle Gauge
	if (sizeType == 104034 || sizeType == 104039) {
		newArrayObj = fnSetUomArray(arrayGauge, combObj);
	}else
	// 104046 Angle
	if (sizeType == 104046) {
		newArrayObj = fnSetUomArray(arrayAngle, combObj);
	}else
	// 104043 Pressure
	if (sizeType == 104043) {
		newArrayObj = fnSetUomArray(arrayPressure, combObj);
	}else if (sizeType == 104047) {
		newArrayObj = fnSetUomArray(arraysizeUOM, combObj);
	}
		
	return newArrayObj;
}
// this function used to set the uom combo values
function fnSetUomArray (arrayObj, combObj){
	for ( var i = 0; i < arrayObj.length; i++) {
		arrayObjCol = arrayObj[i];
		arrayObjColArr = arrayObjCol.split(',');
		arrayCodeId = arrayObjColArr[0];
		arrayCodeNm = arrayObjColArr[1];
		combObj.put(arrayCodeId, arrayCodeNm);
	}
	combObj.save();
	return arrayObj;
}
// 
function fnEnableDisableSizeText(rowId, textCellId, sizeType){
	if (sizeType == 104047) { // Other
		gridObj.cellById(rowId, textCellId).setDisabled(false);
	}else{
		gridObj.cellById(rowId, textCellId).setValue('');
		gridObj.cellById(rowId, textCellId).setDisabled(true);
		
	}
}
//
function fnVoid() {
	deletedPartStr = '';
	var nonSavedPart = '';
	var deletePdId = '';
	var existingDataFl = false;
	fnStartProgress('Y');
	setTimeout(function() {
	gridObj.startFastOperations();
	gridObj.forEachRowA(function(rowId) {
		var delete_partVal = gridObj.cellById(rowId, part_rowId).getValue();
		var delete_qualityVal = gridObj.cellById(rowId, quality_rowId).getValue();
		var pdIdVal = gridObj.cellById(rowId, pd_rowId).getValue();
		// if row exist then form the string
		// quality values always come Y or N
		var check_id = gridObj.cellById(rowId, check_rowId).getValue();
		if (check_id == 1) {
			if (pdIdVal != '') {
				deletedPartStr = deletedPartStr + pdIdVal + ',';
			} else {
				nonSavedPart = nonSavedPart + delete_partVal + ', ';
			}
		}
	});
	gridObj.stopFastOperations();
	// alert(deletedPartStr);
	if (deletedPartStr == '' && nonSavedPart =='') {
		Error_Details(message_prodmgmnt[55]);
	}
	if(nonSavedPart !=''){
		Error_Details(Error_Details_Trans(message_prodmgmnt[56],nonSavedPart.substring(0, (nonSavedPart.length) - 2)));
	}    
	if (ErrorCount > 0) {
		fnStopProgress();
		Error_Show();
		Error_Clear();
		return false;
	}
	var loader = dhtmlxAjax.postSync(
			'/gmUDIProductDevelopment.do?method=pdUDISetup',
			'strOpt=Void&deletePartStr=' + deletedPartStr +'&'+fnAppendCompanyInfo());

	var status = loader.xmlDoc.status;
	if(status == 300) {
		var response = loader.xmlDoc.responseText;
		dhtmlx.message({
		    text: response,
		    expire:5000,
		    type:"myErrorCss" // 'customCss' - css class
		});
	}else
	if (status == 200) {
		var response = loader.xmlDoc.responseText;
		var voidedParts = response.split('|');
		var voidSize = voidedParts.length; 
		var objControl = eval("document.frmUDIProductDevelopment.selectAll");
		if(objControl.checked){
			objControl.checked = false;
			gridObj.clearAll(); // clear grid
		}else{
			gridObj.startFastOperations();
			for(i=0;i<voidSize-1;i++){
			   	gridObj.deleteRow(voidedParts[i]);
			}
			gridObj.stopFastOperations();
		}
		document.getElementById("progress").style.display="block";
		document.getElementById("msg").style.color="green";
		document.getElementById("msg").style.display="inline";		
		document.getElementById("msg").innerHTML="Record voided successfully.";
		setTimeout(function fnexpiretime(){
			document.getElementById("progress").style.display='none';
			document.getElementById("msg").style.display = 'none';
		}, 4000);
		//document.getElementById("voidLine1").style.display = 'block';
		//document.getElementById("voidSuccessfully").style.display = 'block';
		/*dhtmlx.message({
		    text:"Record voided successfully!",
		    expire:1000,
		    type:"myCss" // 'customCss' - css class
		});*/
	}
	fnStopProgress();
	},100);
} 


function fnSetDropDownValues (eObj, dropDownVal ){
	var valSetFl = false;
	if(dropDownVal !=0 && dropDownVal !=''){
		var g=eObj.combo.values;
		for(m=0;m<g.length;m++){
			if(dropDownVal == g[m]){
				eObj.setValue(eObj.combo.keys[m]);
				valSetFl = true;
				g=null;
				break;
				}
		}
	}
	if(!valSetFl){
		eObj.setValue(0);
	}
}
function doOnDistributedEnd(){
	document.getElementById("saveSuccessfully").style.display="none";
	document.getElementById("loadingTr").style.display="none";
	document.frmUDIProductDevelopment.btn_submit.disabled = false;
	document.frmUDIProductDevelopment.btn_Void.disabled = false;
	document.frmUDIProductDevelopment.btn_Print.disabled = false;
	//gridObj.startFastOperations();
	// if part available in quality table then all rows to be disable.
	gridObj.forEachRow(function(rowId) {
		qualityVal = gridObj.cellById(rowId, quality_rowId).getValue();
		q_partFlVal = gridObj.cellById(rowId, q_partfl_rowId).getValue();
		size1TypeIdVal = gridObj.cellById(rowId, size1Type_rowId).getValue();
		size2TypeIdVal = gridObj.cellById(rowId, size2Type_rowId).getValue();
		size3TypeIdVal = gridObj.cellById(rowId, size3Type_rowId).getValue();
		size4TypeIdVal = gridObj.cellById(rowId, size4Type_rowId).getValue();
		
		if (qualityVal == 'Y') {
			gridObj.cellById(rowId, check_rowId).setDisabled(true);
			gridObj.lockRow(rowId,true);
		}
		if(q_partFlVal == 'Y'){
			gridObj.cellById(rowId, part_rowId).setDisabled(true);
			gridObj.cellById(rowId, project_rowId).setDisabled(true);
			gridObj.cellById(rowId, partdes_rowId).setDisabled(true);
		}
		if (size1TypeIdVal == '104047') {
			gridObj.cellById(rowId, size1Value_rowId).setDisabled(true);
			gridObj.cellById(rowId, size1Uom_rowId).setDisabled(true);
		}
		if (size2TypeIdVal == '104047') {
			gridObj.cellById(rowId, size2Value_rowId).setDisabled(true);
			gridObj.cellById(rowId, size2Uom_rowId).setDisabled(true);
		}
		if (size3TypeIdVal == '104047') {
			gridObj.cellById(rowId, size3Value_rowId).setDisabled(true);
			gridObj.cellById(rowId, size3Uom_rowId).setDisabled(true);
		}
		if (size4TypeIdVal == '104047') {
			gridObj.cellById(rowId, size4Value_rowId).setDisabled(true);
			gridObj.cellById(rowId, size4Uom_rowId).setDisabled(true);
		}
		
	}); 
	//gridObj.stopFastOperations();
	fnStopProgress();
	gridObj.refreshFilters();
}
// this function used to avoid the check box row to copy and paste operation.
function doOnBeforeBlockSelect(rowId, cellInd){
	if(cellInd == check_rowId){
		return false;
	}
	return true;
}
// this function used to change the color
function fnSetSizeColor(row_id){
	// to set the block color
	gridObj.setCellTextStyle(row_id,size1Type_rowId,"color:black;font-weight:normal;");
	gridObj.setCellTextStyle(row_id,size1Text_rowId,"color:black;font-weight:normal;");
	gridObj.setCellTextStyle(row_id,size1Value_rowId,"color:black;font-weight:normal;");
	gridObj.setCellTextStyle(row_id,size1Uom_rowId,"color:black;font-weight:normal;");
	//
	gridObj.setCellTextStyle(row_id,size2Type_rowId,"color:black;font-weight:normal;");
	gridObj.setCellTextStyle(row_id,size2Text_rowId,"color:black;font-weight:normal;");
	gridObj.setCellTextStyle(row_id,size2Value_rowId,"color:black;font-weight:normal;");
	gridObj.setCellTextStyle(row_id,size2Uom_rowId,"color:black;font-weight:normal;");
	//
	gridObj.setCellTextStyle(row_id,size3Type_rowId,"color:black;font-weight:normal;");
	gridObj.setCellTextStyle(row_id,size3Text_rowId,"color:black;font-weight:normal;");
	gridObj.setCellTextStyle(row_id,size3Value_rowId,"color:black;font-weight:normal;");
	gridObj.setCellTextStyle(row_id,size3Uom_rowId,"color:black;font-weight:normal;");
	//
	gridObj.setCellTextStyle(row_id,size4Type_rowId,"color:black;font-weight:normal;");
	gridObj.setCellTextStyle(row_id,size4Text_rowId,"color:black;font-weight:normal;");
	gridObj.setCellTextStyle(row_id,size4Value_rowId,"color:black;font-weight:normal;");
	gridObj.setCellTextStyle(row_id,size4Uom_rowId,"color:black;font-weight:normal;");
}
// this function used to show the count down timer on grid load
function fnShowingCountdownTimer() {
	var strcount = 1;
	document.getElementById('saveSuccessfully').innerHTML = 'Data is still loading. Estimated completion time <b>'+ countDownTime + '</b> seconds.';
	if (countDownTime > 0) {
		countDownTime = countDownTime - strcount;
		tt = fnCallTimeout();
	}
}
// this function used to reset the time intervel
function fnCallTimeout() {
	var refresh = 1000; // Refresh rate in milli seconds
	mytime = setTimeout('fnShowingCountdownTimer()', refresh);
}
function add_fast(){
 gridObj.startFastOperations();
	for(var i=1 ;i<500 ;i++){
	   addRow();
	}
 gridObj.stopFastOperations();
}