function fnCallProject(val)
{
	document.frmVendor.hAction.value = 'Part';
  	document.frmVendor.submit();
}

function fnGo()
{
	var hOptValue = document.frmVendor.hOpt.value;
	var setName = message_prodmgmnt[144];
	if(hOptValue == 'BOMMAP'){
		setName = message_prodmgmnt[145];
	}
	// validate the set name drop down filed.
	fnValidateDropDn('Cbo_Set', setName);
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	document.frmVendor.hAction.value = 'Go';
	fnStartProgress();
	document.frmVendor.submit();
}

var prevtr = 0;
function changeTRBgColor(val,BgColor) {
   var obj1 = eval("document.all.tr"+prevtr);
   obj1.style.background = "#ffffff";
   var obj = eval("document.all.tr"+val);
   prevtr = val;
   obj.style.background = BgColor;
}

var cnt = 0;
function fnAddRow(id,varOpt)
{
	var varOpt = document.frmVendor.hOpt.value;
	var tbody = document.getElementById(id).getElementsByTagName("TBODY")[0];
    var row = document.createElement("TR");
    row.style.backgroundColor = "#FFFFFF";
    var td1 = document.createElement("TD")
    td1.innerHTML = fnCreateCell('TXT','PNum',7)
    var td2 = document.createElement("TD")
    td2.innerHTML = '';
    td2.align = "Center";
    var td3 = document.createElement("TD")
    td3.innerHTML = fnCreateCell('TXT','Qty',2);
    td3.align = "Center";
    var td4 = document.createElement("TD")
    td4.innerHTML = fnCreateCell('CHK','Set',3);
    td4.align = "Center";
    var td5 = document.createElement("TD")
    td5.innerHTML = fnCreateCell('CHK','Crit',3);
    td5.align = "Center";
    var td6 = document.createElement("TD")
    td6.innerHTML = fnCreateCell('CBO','CritType',10);
    td6.align = "Center";
    var td7 = document.createElement("TD")
    td7.innerHTML = fnCreateCell('TXT','CritQty',2);
    td7.align = "Center";    
    var td8 = document.createElement("TD")
    td8.innerHTML = fnCreateCell('TXT','CritTag',2);
    td8.align = "Center";
  //  var varOpt = '<%=strhOpt%>'; 
    
    if(varOpt =='BOMMAP')
    {
	    var td9 = document.createElement("TD")
	    td9.innerHTML = fnCreateCell('CBOU','UnitType',10);
	    td9.align = "Center";
    }
      
    row.appendChild(td1);
    row.appendChild(td2);
    row.appendChild(td3);
    row.appendChild(td4);
    row.appendChild(td5);
    row.appendChild(td7);
    row.appendChild(td6);
    row.appendChild(td8);
    if(varOpt =='BOMMAP')
    {
    	row.appendChild(td9);  
      }    
    tbody.appendChild(row);
	cnt++;
}

function fnCreateCell(type,val,size)
{
	val = val + cnt;
	var html = '';
	if (type == 'TXT')
	{
		html = '<input type=text size='+size+' name=Txt_'+val+' class=InputArea>';
		if(val == 'CritQty'+cnt || val == 'CritTag'+cnt)
		{
			html = '<input type=text '+disabled+' size='+size+' name=Txt_'+val+' class=InputArea>';
		}
	}
	else if (type == 'CHK')
	{
		html = '<input type=checkbox name=Chk_'+val+'>';
		if(val == 'Crit'+cnt)
		{
			html = '<input type=checkbox '+disabled+' name=Chk_'+val+'>';
		}
	}
	else if (type == 'CBO')
	{
		html = cbohtml.innerHTML;
		var str = "Cbo_CritType";
		var temp = new Array();
		temp = html.split(str);
		val = str + cnt;
		html = temp[0]+val+temp[1]+val+temp[2];
	}
	
	if (type == 'CBOU')
	{
		html = cbouhtml.innerHTML;
		var str = "Cbo_UnitType";
		var temp = new Array();
		temp = html.split(str);
		val = str + cnt;
		html = temp[0]+val+temp[1]+val+temp[2];
	}
	
	return html;
}

function fnAddErrorDetails(errorMessage, errorPartStr) {
	if (errorPartStr != '') {
		errorPartStr = errorPartStr.substring(0, (errorPartStr.length) - 2);
		Error_Details(errorMessage + errorPartStr + '</b>');
	}
}

var validatesetFlagReqStr = '';
var validateFlagReqStr = '';
var checkFlagReqStr = '';
var checkQtyReqStr = '';

function fnSubmit(varOpt)
{
	var varOpt = document.frmVendor.hOpt.value;
	var obj = '';
	var i=0;
	var k=0;
	var hcnt = document.frmVendor.hCnt.value;
	var lockstr = '';
	
	var arr = '';
	var len = 0;
	var inputstr = '';
	var str = '';
	var pid = '';
	var vfl = '';
	var varpnum = '';
	var duplparts = '';
	var pattern = '';
	var changedval =''; 
	var setqty = '';
	var qty = '';
	validatesetFlagReqStr = '';
	validateFlagReqStr = '';
	checkFlagReqStr ='';
	checkQtyReqStr = '';
	var arrPart = new Array();
	for (j=0;j<cnt;j++)
	{
			obj = eval("document.frmVendor.hId"+j);
			if (obj)
			{
				pid = obj.value;
				vfl = 'U';
			}else{
				pid = '0';
				vfl = 'I';			
			}

			obj = eval("document.frmVendor.Txt_PNum"+j);
			varpnum = TRIM(obj.value);
			changedval = varpnum.replace('\.','\\.');
			inputstr.match(changedval)
			
			
			if (varpnum == ''){
				j++;
				break;
			}
            //Validate the duplicate parts are not allowed
			var pCount =0;
				for(var k=0;k<arrPart.length;k++){
					if(arrPart[k] == varpnum){
						pCount++;
						break;
					}
				}

			if(pCount > 0){
				duplparts = varpnum + " " +duplparts ;
			}else{
				arrPart[arrPart.length] = varpnum;
			}
		
			str = varpnum+'^';

			obj = eval("document.frmVendor.Txt_Qty"+j);
			setqty = obj.value;
			qty = setqty.substring(0, 1);
			if(qty == '.'){
				setqty = '0'+setqty;	
			}
			str = str + obj.value+'^';

			obj = eval("document.frmVendor.Chk_Set"+j);
			lfl = obj.checked;
			if (lfl == true)
			{
				lfl = 'Y';
			}else
			{
				lfl = 'N';
			}
			str = str + lfl +'^';
			//Validate setqty and setflag during submit
			pattern = /^[0-9]+([\.][0-9]+)?$/;
			if(!pattern.test(setqty) && (setqty != "" )){
				checkFlagReqStr = checkFlagReqStr +'<b>'+varpnum +'</b>, ';
			}else{
				fnValidate(setqty, lfl, varpnum);
			}
			obj = eval("document.frmVendor.Chk_Crit"+j);
			lfl = obj.checked;
			if (lfl == true)
			{
				lfl = 'Y';
			}else
			{
				lfl = 'N';
			}
			str = str + lfl +'^';			
			
			obj = eval("document.frmVendor.Txt_CritQty"+j);
			str = str + obj.value+'^';

			obj = eval("document.frmVendor.Cbo_CritType"+j);
			str = str + obj.value+'^';
			
			if(varOpt =='BOMMAP')
    		{
				obj = eval("document.frmVendor.Cbo_UnitType"+j);
				str = str + obj.value+'^'; 
				}
				else 
				str = str + '' +'^';

			obj = eval("document.frmVendor.Txt_CritTag"+j);
			str = str + obj.value+'^';
		
			str = str + vfl + '^' + pid;

			inputstr = inputstr + str + '|';

	}// End of MAIN FOR
	fnAddErrorDetails (message_prodmgmnt[141], validatesetFlagReqStr );
	fnAddErrorDetails (message_prodmgmnt[142], validateFlagReqStr );
	fnAddErrorDetails (message_prodmgmnt[143], checkFlagReqStr );
	// validate the set name drop down filed.
	var hOptValue = document.frmVendor.hOpt.value;
	var setName = message_prodmgmnt[144];
	if(hOptValue == 'BOMMAP'){
		setName = message_prodmgmnt[145];
	}
	fnValidateDropDn('Cbo_Set', setName);
	fnValidateTxtFld('Txt_LogReason',message_prodmgmnt[146]); 
	if (duplparts != ''){
			Error_Details(Error_Details_Trans(message_prodmgmnt[147],duplparts));
		}
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	} 
	//var setName = TRIM(document.frmVendor.hSetName.value);
	document.frmVendor.inputString.value = inputstr;
	//alert("STR:"+document.frmVendor.inputString.value);	
	//document.frmVendor.hAction.value = 'Save';
	document.frmVendor.btn_submit.disabled = true;
	document.frmVendor.action = "/GmSetMapServlet?hAction=Save";
	fnStartProgress(); 
 	document.frmVendor.submit();
}

//Based on the setQty & setFlag validation will be thrown during submit
function fnValidate(qty, status, partnum)
{
	if(qty == '' && status == 'Y'){
		checkFlagReqStr = checkFlagReqStr +'<b>'+partnum +'</b>, ';
	}
	if(qty == '0' && status == 'Y'){
		validatesetFlagReqStr = validatesetFlagReqStr +'<b>'+partnum +'</b>, ';
	}
	if(qty > '0' && status == 'N'){
		validateFlagReqStr = validateFlagReqStr +'<b>'+partnum +'</b>, ';
	}
	
}

function fnSetSelections()
{
	
	cnt=count; 
	if(document.frmVendor.Cbo_Set.value == '')
		{
	document.getElementById("Txt_SetId").focus();
		}
	if (document.frmVendor.hAction.value == "Go")
	{
		cnt = document.frmVendor.hCnt.value;	
	}
}

function checkTaggable()
{
 var strSetType = "";
 var strPartFamily ="";
  var strProdAttributeValue = "";
 
	 document.getElementById("strTaggable_part").style.display = 'none';
	  document.getElementById("strCritical_Qty").style.display = 'none'; 
	 var checkboxname="";
	 
	 for (var i = 0; i <  cnt ; i++ ) 
	 {
			checkboxname = eval("document.frmVendor.Chk_Crit" + i);
			strSetType = eval("document.frmVendor.strSetTYPE" + i).value;
			strPartFamily = eval("document.frmVendor.strPartFamily" + i).value;
			strProdAttributeValue= eval("document.frmVendor.strProdAttributeValue" + i).value;
		  	if (checkboxname.checked != true && (strSetType=='4070' || strSetType=='4071')) 
	 		{
	         document.getElementById("strCritical_Qty").style.display = 'block';
			 flaghA = "false";
			}
			else if(strPartFamily == '4052' && strProdAttributeValue =='Y')
			{
		  		document.getElementById("strTaggable_part").style.display = 'block';
		  		break;
			}
			else if (checkboxname.checked == true) 
			{
			    	document.getElementById("strCritical_Qty").style.display = 'none';
			    	flaghA = "true";
			      	break;
			}
      }
}
//Based on the setID & setName validation will be thrown during load
function fnGO(obj)
{
	  
	   var Cbo_Set ='';
	   var response='';
	   var setName= '';
	   var setId ='';
	   var oldSetId ='';
	   if(obj=='id'){
		   setId =TRIM(document.frmVendor.Txt_SetId.value);
		   document.frmVendor.Cbo_Set.value = '';
		   oldSetId = TRIM(document.frmVendor.hSetId.value);
	   if(oldSetId==setId||setId==''){
		   document.frmVendor.Txt_SetId.value = setId;
			return;
		}
	   }else{
		   setId = TRIM(document.frmVendor.Cbo_Set.value);
		   document.frmVendor.Txt_SetId.value = '';
	   }
	    if(setId==''){
	        return;
	   }
	   fnStartProgress('Y');
	   var ajaxUrl = fnAjaxURLAppendCmpInfo('/GmSetMapServlet?hAction=validateSet&setId='+setId+'&strType='+strType+'&setName='+setName);
	   dhtmlxAjax.get(ajaxUrl, function(loader){
	   response = loader.xmlDoc.responseText;
	   		     		
	   if(response !="")
	   		    {
	   			Cbo_Set = setId;
	   			document.getElementById('searchCbo_Set').value = response;
	   			document.frmVendor.action = "/GmSetMapServlet?hAction=Go&Cbo_Set="+Cbo_Set;
	   			document.frmVendor.submit(); 	
	   		    }
	   else{
	   			 if(strType=='SET'||strType=='SETMAP')
	    		   {	
	   				document.frmVendor.searchCbo_Set.value ='';
	    		    Error_Details("Set ID does not exist, please enter a valid Set ID");
	    		   
	    		   }
	    	   else
	    		   {
	    		   document.frmVendor.searchCbo_Set.value ='';
	    		   Error_Details("BOM ID does not exist, please enter a valid BOM ID");
	    		   }
	    	   }
	    	   
	    	   if (ErrorCount > 0) {
	    		fnStopProgress();
	   			Error_Show();
	   			Error_Clear();
	   			return false;
	   		} 
	    	   
					
        });

    }


