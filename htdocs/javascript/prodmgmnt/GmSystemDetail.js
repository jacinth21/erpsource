
var gridObj;
//Initiating grid when on page loading 
function fnOnPageLoad(){

if(parent.window.document.getElementById("ajaxdivcontentarea") != undefined){
  	parent.window.document.getElementById("ajaxdivcontentarea").style.height = "690";
}
if(objGridData.value != ''){ 
		gridObj = initGridData('dataGridDiv',objGridData);
}
if (objGridData.indexOf("cell", 0) != -1) {
	document.getElementById("dataGridDiv").style.height = "500px";
}else{
	document.getElementById("dataGridDiv").style.height = "10";
	document.getElementById("ShowDetails").style.color = "red";
	document.getElementById("ShowDetails").style.height = "15";
	document.getElementById("ShowDetails").innerHTML = "Nothing found to display.";
}

}
//Initiating the grid
function initGridData(divRef,gridData){
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.attachHeader("#select_filter,#text_filter,#select_filter,#select_filter,#select_filter");
	gObj.init();
	gObj.loadXMLString(gridData);	
	return gObj;	
}

//Function to export excel
function fnExport()
{	
	gridObj.toExcel('/phpapp/excel/generate.php');
}

function fnUpdateTabIndex(link) {
	var linkObj = link.getElementsByTagName('a')[0];
	document.all.hcurrentTab.value = linkObj.name;
	
}

//function to load the SYSTEM details when a SYSTEM is selected in the SYSTEM LIST.
function fnfetchSystemDet(form) {	
	document.getElementById("strOpt").value = 'fetchcontainer';
	var setID = form.setId.value;
	if(setID=='0'){
		form.setId.value='';
	}
	fnStartProgress('Y');
	form.submit();
}

//Function to reset the form.
function fnSystemReset(form) {
	document.getElementById("setId").value = 0;
	form.strOpt.value = 'fetchcontainer';
	form.setId.value = '';
	form.submit();
}

//Function to void the sytem.
function fnVoidSystem(form){
	var set_id=form.setId.value;
	form.action = "/GmCommonCancelServlet?hTxnName="+set_id+"&hCancelType=VDSYS&hTxnId="+set_id;
	form.submit();		
}

//function to submit the form.This will save the form details.
function fnSystemSubmit(form)
{
	var setid = form.setId.value;
	var setName = form.setNm.value;

	//fnValidateDropDn('setId',' System List ');
	fnValidateTxtFld('setNm',message_prodmgmnt[314]);
	fnValidateDropDn('divisionId',message_prodmgmnt[315]);
	//fnLenValidation(form.setNm," System Name","200");  //since the maxlength is given as 200 for this field.So no need of validation. 
	fnLenValidation(form.setDesc,message_prodmgmnt[316],"4000");   //since this column in DB has 4000, need to have this length validation here.
	fnValidateTxtFld('txt_LogReason',message_prodmgmnt[146]);

	var groupAttrId = form.groupAttrId.value;
	var segmentAttrId = form.segmentAttrId.value;
	var techniqueAttrId = form.techniqueAttrId.value;

	var groupInputStr = '';
	var segmentInputStr = '';
	var techniqueInputStr = '';
	
	var objGroup = form.Chk_GrpGroup;
	var objSegment = form.Chk_GrpSegment;
	var objTech = form.Chk_GrpTechnique;

	if (objGroup){
		groupInputStr = fnFormAttributeString(objGroup, groupAttrId);
	}

	if (objSegment){
		segmentInputStr = fnFormAttributeString(objSegment, segmentAttrId);
	}

	if (objTech){
		techniqueInputStr = fnFormAttributeString(objTech, techniqueAttrId);
	}
	
	if (ErrorCount > 0)	{
		Error_Show();
		Error_Clear();
		return false;
	}
	
	form.setAttrInputStr.value =  groupInputStr + segmentInputStr + techniqueInputStr;
	form.attrTypeInputStr.value = groupAttrId + ',' + segmentAttrId + ',' + techniqueAttrId;
	form.strOpt.value = 'save';
	//form.setNm.disabled = false;
	fnStartProgress('Y');
	form.submit();	
}
//Thie below function is used to form the input stirng for the Check box groups.
function fnFormAttributeString(obj, AttrId){
	var inputStr='';
	//when there is only one value in the check box Group, we should not loop to form the input String since it is considering as an object.
	if(obj.length == undefined && obj.checked == true){
		inputStr = inputStr + AttrId + '^' + obj.value + '|';
	}else{
		for (var i=0;i<obj.length;i++){
			if (obj[i].checked == true){
				inputStr = inputStr + AttrId + '^' + obj[i].value + '|';
			}
		}
	}
	return inputStr;
}


//Function to toggle the headers.
function fnShowFilters(val){
	var obj = eval("document.all."+val);
	var obj1 = eval("document.all."+val+"img");
	
	if (obj.style.display == 'none'){
		obj.style.display = 'block';
		if(obj1){
			obj1.src = '/images/minus.gif';
		}
	}
	else{
		obj.style.display = 'none';	
		if(obj1){
			obj1.src = '/images/plus.gif';
		}
	}
}

//function to check the set atrributes while loading the system details.
function fnCheckSetAttributes()
{
	var form = document.getElementsByName('frmSystemDetails')[0];
	//var form2 = document.getElementsByName('frmSystemDetails')[1];
	var setid = document.getElementById("setId").value;
	var objgrp = '';
	if (setid != "" && setid != "0"){
		if(form){			
			var groupids = document.getElementById("hGroupIds").value;
			var segmentids = document.getElementById("hSegmentIds").value;
			var techniqueids = document.getElementById("hTechniqueIds").value;
			var hPublishInIds = document.getElementById("hPublishInIds").value;
			if (groupids != '')	{
		 		objgrp = document.getElementsByName("Chk_GrpGroup");
		 		fnCheckSelections(groupids,objgrp);
		 	}
			if (segmentids != '')	{
		 		objgrp = document.getElementsByName("Chk_GrpSegment");
		 		fnCheckSelections(segmentids,objgrp);
		 	}
			if (techniqueids != '')	{
		 		objgrp = document.getElementsByName("Chk_GrpTechnique");
		 		fnCheckSelections(techniqueids,objgrp);
		 	}
			if (hPublishInIds != ''){
		 		objgrp = document.getElementsByName("Chk_GrpPublish");
		 		fnCheckSelections(hPublishInIds,objgrp);
		 	}			
		}
	}
}


function fnCheckSelections(val,objgrp)
{
	var valobj = val.split(",");
	var arrlen = objgrp.length;
			
	if (arrlen > 0)	{
		for (i=0;i<valobj.length ;i++ )	{
			for (j=0;j<arrlen ;j++ ){
				if (valobj[i] == objgrp[j].value){
						objgrp[j].checked = true;
				}
			}
		}
	}
	//when there is only one value in the check box Group, during onload the values are not getting checked by default.
	if(arrlen == undefined && objgrp.checked == false && valobj == objgrp.value){
		objgrp.checked = true;
	}
}

//To limit the no. of characters entering into the text area
function fnLenValidation(obj,name,len){ 
	if(obj.value.length > len){
		var arrmsg = [len,name];
		Error_Details(Error_Details_Trans(message_prodmgmnt[119],arrmsg));
	}
}

//Description : This function used to validate the System name whether it is already exist or not
function fnvalidateSystemName(setName) {
	setName = TRIM(setName);
	var validName = eval("document.all.validateName");				
	var validText = document.getElementById("validateText");
	if(setName!=''){
		var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmSystemSetup.do?strOpt=validateSystem&setNm='+ encodeURIComponent(setName)+ '&ramdomId=' + Math.random());
		dhtmlxAjax.get(ajaxUrl, function(loader){
			var response = loader.xmlDoc.responseText;
			if (response != '' && response != 'undefined') {
					if (response == 'AVAILABLE') {	
							validName.style.display = 'block';
							validName.src = '/images/success.gif';
							validText.innerHTML ="Available";
						}else{
							validName.style.display = 'block';
							validName.src = '/images/error.gif';
							validText.innerHTML ="Not Available";
						}
					}		
		});
	}else{
			if(validName.style.display == 'block'){
				validName.style.display = 'none';
			}
			if(validText.innerHTML){
				validText.innerHTML ='';
			}
	}
}

//Function to preview the system.
function fnPreviewSystem(form){
	var setid = form.setId.value;
	var setName = form.setNm.value;	
	windowOpener('/gmSystemPreview.do?strOpt=Load&systemId='+setid+'&systemNm='+setName,"SystemDetails","resizable=yes,scrollbars=yes,top=150,left=150,width=865,height=800");
}

//Function will call on page load. It will append companyInfo in each tab href and initiate ajaxtab.
function fnAjaxTabLoad(){
	fnAjaxTabAppendCmpInfo("#maintab");
	fnMainTab();
}

//initiate ajaxtab
function fnMainTab(){
	var maintab=new ddajaxtabs("maintab", "ajaxdivcontentarea");				
	maintab.setpersist(false);								
	maintab.init();
	
	maintab.onajaxpageload=function(pageurl){
		if (pageurl.indexOf("gmSystemSetup.do")!=-1){
			fnAddCompanyParams();
			fnCheckSetAttributes();
			if(document.getElementById("ajaxdivcontentarea") != undefined){
	  			document.getElementById("ajaxdivcontentarea").style.height = "auto";
			}
		}
		if (pageurl.indexOf("gmSystemMapping.do")!=-1){
			fnOnloadSystemMapping();
			if(document.getElementById("ajaxdivcontentarea") != undefined){
	  			document.getElementById("ajaxdivcontentarea").style.height = "auto";
			}	
		}
		fnHtmlEditor();
	}			
}	

// PMT-37488 - System Publish for Marketing Collateral and Product Catalog
// Function used to Publish the System
function fnSystemPublish(form,sysType){
	fnValidateTxtFld('txt_LogReason',message_prodmgmnt[146]);
	 if (ErrorCount > 0) {  
			Error_Show();
			Error_Clear();
			return false;
			}
	if(mapCount=='0'){
		 Error_Details(message_prodmgmnt[476]);
		 if (ErrorCount > 0) { 
				Error_Show();
				Error_Clear();
				return false;
				}
		}else{
		var setId = document.getElementById("setId").value;
		var comment = document.getElementById("comments").value;
			if(sysType!='' && setId!=''){
						if(sysType=='108660'){  
								if(!confirm(message_prodmgmnt[475])){   // publish 
									return false;
								} 
						}else{
							if(!confirm(message_prodmgmnt[484])){  // Unpublish
								return false;
							}
						}
						 fnStartProgress();
						 var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmSystemPublish.do?method=saveSystemPublishDtls&setId='+ encodeURIComponent(setId)+'&strSysType='+ encodeURIComponent(sysType)+'&txt_LogReason='+encodeURIComponent(comment)+'&ramdomId=' + Math.random());
						
						 var loader = dhtmlxAjax.postSync(ajaxUrl);
						 var response = loader.xmlDoc.responseText;
						 fnCallBackSysType(response,sysType);
						 return false;	
						 
					 }
		      }
		}

// function used to call back reponse used messge show and publish or unpublish button show
function fnCallBackSysType(response,sysType){
	if (response != '' || response != undefined){  //used show to publish messges from response
			//splitting the response
			var temp = new Array();
			temp = response.split('^^');
			var comment = document.getElementById("comments").value;
			
			if(sysType=='108660'){
				document.getElementById("publisBtn").style.display='none';
				document.getElementById("unPublisBtn").style.display='inline-block';
				document.all.publishMsg.innerHTML = temp[0];
				var systemName = '-&nbsp<b>'+document.frmSystemDetails.setNm.value+ '</b> System published';
			}else{
				document.getElementById("publisBtn").style.display='inline-block';
				document.getElementById("unPublisBtn").style.display='none';
				document.all.publishMsg.innerHTML = '';
				var systemName = '-&nbsp<b>'+document.frmSystemDetails.setNm.value+ '</b> System unpublished';
				
		    }
			
			//setting log section 
			 $( "#logsection table tr:nth-of-type(6)" ).after( ' <tr> <td class="RightText" height="20">&nbsp'+temp[2]+'</td> ' +
			 		'' +'<td class="RightText">&nbsp'+temp[1]+'</td>'+
			 		'' +'<td style="width: 500px;" width="500" class="RightText">'+comment+' '+systemName+'</td></tr>' );
			 
			 document.getElementById("comments").value = '';
	}
	 fnStopProgress();
}
