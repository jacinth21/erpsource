//PMT#37505-system release to region in system setup
var gridObj;
var check_rowId = '';
var rowID = '';
var region_rowId = '';
var entityName = '';
var releasedRegion = '';
var unReleasedRegion = '';
var releasedRegName = '';
var unReleasedRegName = '';
var systemStatus = '';
var release_id = '';
var check_count = '';
var count_rowId = '';
var code_rowId = '';
var release_flag='';
var codename_rowId = '';
var updatedBy = '';
var updatedDate = '';
var releaseHistory = '';
	
// to fetch system release details by sending ajax request to action class
function fnOnPageLoad() {

	response = '';
	fnStartProgress();
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmSystemPublish.do?method=fetchSystemReleaseDtls&setId='
			+ strSetId
			+ '&setGroupType='
			+ strGroupType
			+ '&ramdomId='
			+ Math.random());
	dhtmlxAjax.get(ajaxUrl, fnCallBack);

}

// to check response from ajax call to load grid data
function fnCallBack(loader) {

	response = loader.xmlDoc.responseText;
	fnStopProgress();
	if (response != "") {
		fnSystemReleaseData(loader);

	} else {
		fnStopProgress();
		Error_Details(message_prodmgmnt[480]);
		if (ErrorCount > 0) {
			Error_Show();
			Error_Clear();
			return false;
		}
	}
}

// to form system release detail in grid
function fnSystemReleaseData() {

	var setHeader = [ "All", "Region", "Sets", "Status", "", "Updated By",
			"Updated Date", "Status", "entityId" ];

	var setColIds = "chk_box,entity_name,set_count,system_status,release_history,updated_by,updated_date,status_id,entity_id";
	var setColTypes = "ch,entityName,setCount,ro,ro,ro,ro,ro,ro";
	var setColAlign = "center,left,center,left,center,left,left,left,left";
	var setColSorting = "na,str,int,str,na,str,str,str,str";
	var enableTooltips = "true,true,true,true,true,true,true,true,true";

	var setFilter = [
			"<input type='checkbox' value='no'  name='selectAll' onClick='javascript:fnChangedFilter(this);'/>",
			"", "", "", "", "", "" ];
	if(strFromPage == 'setsetup'){ //PMT_37772_Set release to Region in Set Setup screen
		var setInitWidths = "40,120,70,115,50,115,*";
	}
	else{
		var setInitWidths = "40,120,70,115,50,115,150";
	}
	var gridHeight = "";
	var dataHost = {};
	format = "Y";

	// split the functions to avoid multiple parameters passing in single
	// function
	gridObj = initGridObject('dataGridDiv');
	gridObj = fnCustomTypes(gridObj, setHeader, setColIds, setInitWidths,
			setColAlign, setColTypes, setColSorting, enableTooltips, setFilter,
			gridHeight, format);
	
	gridObj = loadDhtmlxGridByJsonData(gridObj, response);
	check_rowId = gridObj.getColIndexById("chk_box");
	region_rowId = gridObj.getColIndexById("status_id");
	count_rowId = gridObj.getColIndexById("set_count");
	code_rowId = gridObj.getColIndexById("entity_id");
	codename_rowId = gridObj.getColIndexById("entity_name");
	updatedBy = gridObj.getColIndexById("updated_by");
	updatedDate = gridObj.getColIndexById("updated_date");
	systemStatus = gridObj.getColIndexById("system_status");
	releaseHistory = gridObj.getColIndexById("release_history");

	gridObj.setColumnHidden(7, true);
	gridObj.setColumnHidden(8, true);
	if(strFromPage == 'setsetup'){ //PMT_37772_Set release to Region in Set Setup screen
		gridObj.setColumnHidden(2, true);
	}
	gridObj.attachEvent("onRowSelect", doOnRowSelect);
	gridObj.attachEvent("onCheckbox", doOnCheck);
	
	var rowCount=gridObj.getRowsNum();
	var checkedCount='';
	var checkDisableCount='';
	var checkedValue ='';
	

	
	   // to check validation for released and set count details for each region
	    gridObj.forEachRow(function(rowId) {
				check_count = gridObj.cellById(rowId, count_rowId).getValue();			
				if (check_count == '0' && strFromPage != 'setsetup') {
					gridObj.cellById(rowId, check_rowId).setDisabled(true);
					checkDisableCount++;
				} else {
					release_id = gridObj.cellById(rowId, region_rowId)
							.getValue();
					if (release_id == '105361') {
						gridObj.cellById(rowId, check_rowId).setChecked(true);
						
					}
				}

				// to show history flag based on history flag value
				release_flag = gridObj.cellById(rowId, releaseHistory).getValue();
				if (release_flag == 'Y') {
					gridObj
							.cellById(rowId, releaseHistory)
							.setValue(
									'<a href=\"#\"><img alt=\"System Release Detail\" src=\"/images/icon_History.gif\" border=\"0\"></a>');
				}

				// get count for checked value in each row
				var checkedValue = gridObj.cellById(rowId, check_rowId)
						.getValue();
				if (checkedValue == 1) {
					checkedCount++;
				}

			});

	// to set checked check box in setFileter when all rows check box value is selected
	if (rowCount == checkedCount) {

		gridObj.detachHeader(1);
		var setFilter = [
				"<input type='checkbox' value='no' name='selectAll' onClick='javascript:fnChangedFilter(this);' checked/>",
				"", "", "", "", "", "" ];
		gridObj.attachHeader(setFilter);

	}
	
	// to set disabled check box in setFileter when all rows check box value is disabled
	if (rowCount == checkDisableCount) {
		
		gridObj.detachHeader(1);
		var setFilter = [
				"<input type='checkbox' value='no' name='selectAll' disabled='true' />",
				"", "", "", "", "", "" ];
		gridObj.attachHeader(setFilter);

	}
	
}

// This Function Used to set custom types conditions in Grid
function fnCustomTypes(gObj, setHeader, setColIds, setInitWidths, setColAlign,
		setColTypes, setColSorting, enableTooltips, setFilter, gridHeight,
		format) {

	gObj.setHeader(setHeader);
	var colCount = setHeader.length;
	gObj.setInitWidths(setInitWidths);
	gObj.setColAlign(setColAlign);
	gObj.setColTypes(setColTypes);
	gObj.setColSorting(setColSorting);
	gObj.enableTooltips(enableTooltips);
	gObj.attachHeader(setFilter);
	gObj.setColumnIds(setColIds);
	gObj.enableAutoHeight(true, gridHeight, true);

	return gObj;
}

//This function used to create user defined column types in Grid
function eXcell_entityName(cell) { // the eXcell name is defined here
	if (cell) { // the default pattern, just copy it
		this.cell = cell;
		this.grid = this.cell.parentNode.grid;
	}
	this.setValue = function(val) {
		this.setCValue("<a href=\"#\">" + val + "</a>", val);

	}
	this.getValue = function() {
		return this.cell.childNodes[0].innerHTML; // gets the value
	}
}
eXcell_entityName.prototype = new eXcell;

function eXcell_setCount(cell) { // the eXcell name is defined here
	if (cell) { // the default pattern, just copy it
		this.cell = cell;
		this.grid = this.cell.parentNode.grid;
	}
	this.setValue = function(val) {
		this.setCValue("<a href=\"#\">" + val + "</a>", val);

	}
	this.getValue = function() {
		return this.cell.childNodes[0].innerHTML; // gets the value
	}
}
eXcell_setCount.prototype = new eXcell;




//Function is used to Check Box Check
function doOnCheck(rowId, cellInd, stage) {
	rowID = rowId;
	if (cellInd == check_rowId) {
		
		fnSelectAllToggle('selectAll');		
	}
	return true;
}


//Description : This function to check the select all check box
function fnSelectAllToggle(varControl) {
	
	var objControl = eval("document.frmSystemDetails." + varControl);
	var all_row = gridObj.getAllRowIds(",");
	var all_rowLen = all_row.length; // get the rows id length
	var checked_row = gridObj.getCheckedRows(check_rowId);
	var finalval;

	finalval = eval(checked_row.length);
	if (all_rowLen == finalval) {
		objControl.checked = true;// select all check box to be checked
	} else {
		objControl.checked = false;// select all check box un checked.
	}
}


//This Function Used to load loadSetPriorityDetailsReport screen based on validation 
function doOnRowSelect(rowId, cellInd) {
	var entName = gridObj.cellById(rowId, codename_rowId).getValue();
	var entId = gridObj.cellById(rowId, code_rowId).getValue();
	var relHistory = gridObj.cellById(rowId, releaseHistory).getValue();
	
		if (cellInd == 1) { //This call used to load the company details related to selected region in new window
			windowOpener("/gmEntity.do?method=loadEntityReport&regionId=" + entId
					+ "&regionName=" + entName, "Region - Company Info",
					"resizable=yes,scrollbars=yes,top=200,left=150,width=580,height=550");
		 }
		if(cellInd == 4 && relHistory!=''){   // Used to History Icon hyper link call for pop up window opener   
			var setId = strSetId+'-'+entName;
			  windowOpener("/gmAuditTrail.do?auditId="+'313'+"&txnId="+ encodeURIComponent(setId), "","resizable=yes,scrollbars=yes,top=250,left=300,width=785,height=300,status=1");
		}
		if(cellInd == 2){  // Used to SET COUNT hyper link call for pop up window open
			windowOpener("/gmSystemPublish.do?method=loadSystemSetMappingdtls&setId="+strSetId+"&regionId="+entId+"&setGroupType="+strGroupType+"&regionName=" + entName+"&ramdomId="+ Math.random(),"SetMap","resizable=yes,scrollbars=yes,width=1100,height=300");
		}
}

// submit action will release or unrelease system details
function fnSubmit() {

	var strReleasedRegion = '';
	var strUnreleaseRegion = '';
	var strReleasedRegionName = '';
	var strUnReleasedRegionName = '';
	var strParamInfo = '';
	var strSubReleased = '';
	var strSubUnreleased = '';
	var strConform = '';

	gridObj.forEachRow(function(rowId) {
		check_id = gridObj.cellById(rowId, check_rowId).getValue();
		release_id = gridObj.cellById(rowId, region_rowId).getValue();

		// check if system is released to selected region
		if (check_id == 1 && release_id != '105361') {
			releasedRegion = gridObj.cellById(rowId, code_rowId).getValue();
			releasedRegName = gridObj.cellById(rowId, codename_rowId)
					.getValue();
			strReleasedRegion += releasedRegion + ',' + '105361' + '|';
			strReleasedRegionName += releasedRegName + ', ';

			// check if system is unreleased to selected region		
		} else if (check_id == 0 && release_id == '105361') {
			unReleasedRegion = gridObj.cellById(rowId, code_rowId)
					.getValue();
			unReleasedRegName = gridObj.cellById(rowId, codename_rowId)
					.getValue();
			strUnreleaseRegion += unReleasedRegion + ',' + '10304547' + '|';
			strUnReleasedRegionName += unReleasedRegName + ', ';
		}
	});

	strSubReleased = strReleasedRegionName.substring(0,
			strReleasedRegionName.length - 2);
	strSubUnreleased = strUnReleasedRegionName.substring(0,
			strUnReleasedRegionName.length - 2);
	strParamInfo += strReleasedRegion + strUnreleaseRegion;
	
	
	if(strFromPage == 'setsetup'){ //shows confirm message for set
		
		if (strSubReleased != '') {
			strConform += message_prodmgmnt[482] +' '+strSubReleased+' region(s)?' + '\n';
		}
	
		if (strSubUnreleased != '') {
			strConform += message_prodmgmnt[483] +' '+strSubUnreleased+' region(s)?';
		}
	}
	else // shows confirm message for system
		{
		
		if (strSubReleased != '') {
			strConform += message_prodmgmnt[477] +' '+strSubReleased+' region(s)?' + '\n';
		}
	
		if (strSubUnreleased != '') {
			strConform += message_prodmgmnt[478] +' '+strSubUnreleased+' region(s)?';
		}
		
		
		}
	
	
	
	
	
	if (strConform != '') {
		if (confirm(strConform)) {

			//This is save parts - change the AJAX type to post method 
			var ajaxUrl = '/gmSystemPublish.do?method=saveSystemReleaseDtls';
			var paramString = '&setId=' + strSetId + '&setGroupType='
					+ strGroupType + '&systemReleaseDtls=' + strParamInfo + '&'
					+ fnAppendCompanyInfo() + '&ramdomId=' + Math.random();

			dhtmlxAjax.post(ajaxUrl, paramString, fnCallBackSuccess);
		}
	}

	else {
		Error_Details(message_prodmgmnt[481]);
		if (ErrorCount > 0) {
			Error_Show();
			Error_Clear();
			return false;
		}
	}
}

// to reload the page after release or unrelease system details
function fnCallBackSuccess(loader) {

	fnStopProgress();
	var status = loader.xmlDoc.status;
	if (status == 200) {
		fnOnPageLoad();
	} else {
		fnStopProgress();
		Error_Details("message_prodmgmnt[480]");
		if (ErrorCount > 0) {
			Error_Show();
			Error_Clear();
			return false;
		}
	}
}

// to select all check box when click select all option
function fnChangedFilter(obj) {
	if (obj.checked) {
		gridObj.forEachRow(function(rowId) {
			var check_disable_fl = gridObj.cellById(rowId, check_rowId)
					.isDisabled();
			if (!check_disable_fl) {
				gridObj.cellById(rowId, check_rowId).setChecked(true);
			}
		});
	} else {
		gridObj.forEachRow(function(rowId) {
			check_id = gridObj.cellById(rowId, check_rowId).getValue();
			if (check_id == 1) {
				gridObj.cellById(rowId, check_rowId).setChecked(false);
			}

		});
	}
}

//Description: This function will used to generate excel.
function fnDownloadXLS() {
	gridObj.toExcel('/phpapp/excel/generate.php');
}
