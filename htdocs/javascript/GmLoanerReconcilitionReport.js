var objGridData;
document.onkeypress = function(){
	if(event.keyCode == 13){
		fnLoad();
	}
}
function initGridData(divRef,gridData)
{	
	var gObj = new dhtmlXGridObject(divRef);
	var frmTransType = document.frmLoanerReconciliation.loanerType.value;
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);
	if(frmTransType == '4119'){
		gObj.enableTooltips("true,true,true,true,true,true,true,true,true,true,true,true,true,true");
		gObj.attachHeader('#select_filter,#text_filter,#text_filter,#rspan,#text_filter,#text_filter,#text_filter,#rspan,#rspan,#rspan,#rspan,#rspan,#rspan,#rspan');
	}else{
		gObj.enableTooltips("true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true");
		gObj.attachHeader('#select_filter,#select_filter,#select_filter,#text_filter,#text_filter,#rspan,#select_filter,#text_filter,#text_filter,#rspan,#rspan,#rspan,#rspan,#rspan,#rspan,#rspan,#rspan,#rspan');
	}
	gObj.init();	
	gObj.enablePaging(true, 20, 10, "pagingArea", true, "infoArea");
	gObj.setPagingSkin("bricks");
	gObj.loadXMLString(gridData);	
	return gObj;	
}

function fnOnPageLoad()
{	
	//document.frmLoanerReconciliation.loanerType.value = '4119';
	fnLoadDist(document.frmLoanerReconciliation.loanerType.value);
	
	if(document.frmLoanerReconciliation.loanerType.value == '4119'){
		document.frmLoanerReconciliation.Btn_CrTi.style.visibility="hidden";
	}
	if(deptid == 'S'){
		document.getElementById("Btn_CrTi").style.visibility="hidden"; 
	}		
	
	if(objGridData !=''){	
		gridObj = initGridData('gridData',objGridData);  		
	}

	if(document.frmLoanerReconciliation.loanToName)
		document.frmLoanerReconciliation.loanToName.disabled = false;
}

function fnDownloadExcel(){		
	gridObj.detachHeader(1);
	gridObj.toExcel('/phpapp/excel/generate.php');	
	gridObj.attachHeader('#select_filter,#select_filter,#text_filter,#text_filter,#text_filter,#select_filter,#text_filter,#rspan,#rspan,#rspan,#rspan,#rspan,#rspan,#rspan');
}

function fnInHousePicSlip(val)
{
	windowOpener("/GmConsignInHouseServlet?hAction=PicSlip&hConsignId="+val,"SetPic","resizable=yes,scrollbars=yes,top=150,left=150,width=765,height=500");
}

function fnCallRecon(val)
{
	document.frmLoanerReconciliation.hId.value = val;
	document.frmLoanerReconciliation.hAction.value = "LoanRecon";
	document.frmLoanerReconciliation.action = "/GmLoanerReconServlet";
	document.frmLoanerReconciliation.submit();	
}

function fnUnRecon(val)
{	
	document.frmLoanerReconciliation.hId.value = val;
	document.frmLoanerReconciliation.hAction.value = "LoadUnRecon";
	document.frmLoanerReconciliation.action = "/GmLoanerReconServlet";
	document.frmLoanerReconciliation.submit();	
}



function  fnLoad(){		
	
	objStartDt = document.frmLoanerReconciliation.fromDt;
	objEndDt = document.frmLoanerReconciliation.toDt;
	var dateDiffVal = dateDiff(todaysDate,objStartDt.value,format);
	var strLoanTo = document.frmLoanerReconciliation.loanTo.value;
	var fromToDiff = dateDiff(objStartDt.value, objEndDt.value, format);
	var fromCurrDiff = dateDiff(objStartDt.value, todaysDate, format);
	var toCurrDiff  = dateDiff(objEndDt.value, todaysDate, format);
	
	if(objStartDt.value != "" && objEndDt.value == ""){
		Error_Details(message[10034]);
	}
	if(objEndDt.value != "" && objStartDt.value == ""){
		Error_Details(message[10035]);
	}	
	if(objStartDt.value != ""){
		CommonDateValidation(objStartDt,format,Error_Details_Trans(message[10002],format));
	}
	if(objEndDt.value != ""){
		CommonDateValidation(objEndDt,format,Error_Details_Trans(message[10002],format));
	}
	var dtFrom = document.frmLoanerReconciliation.fromDt.value;
    var dtTo = document.frmLoanerReconciliation.toDt.value;

     if(fromToDiff < 0){
    	 Error_Details(message[10041]);
	 }
	 if(fromCurrDiff < 0){
		 Error_Details(message[10046]);
	 }
	 if(toCurrDiff < 0){
		 Error_Details(message[10047]);
	 }

	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}
/*	if(strLoanTo == '19518'){
		document.frmLoanerReconciliation.dist.value = document.frmLoanerReconciliation.loanToName.value; 
	}*/
	fnStartProgress('Y');
	document.frmLoanerReconciliation.strOpt.value = "Load";
	document.frmLoanerReconciliation.submit();
	document.frmLoanerReconciliation.Btn_Load.disabled = true;
}

function fnOpenLog(id,type)
{
	windowOpener("/GmCommonLogServlet?hType="+type+"&hID="+id,"ConsignLog","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=300");
}


function fnSendEmail(){
	frmObj = document.frmLoanerReconciliation;
	var flg = true;
	var k = 0;
	var arrReqid=new Array();
		for(i=0;i<frmObj.elements.length;i++)
			{
				if (frmObj.elements[i].type =="checkbox")
				{
					if (frmObj.elements[i].name == 'Chk_reqid' && frmObj.elements[i].checked == true )
					{						
							for(j=0;j<arrReqid.length;j++){ // to put distinct values in the array
								if (arrReqid[j] == frmObj.elements[i].value)
								{
									flg = false;
									break;
								}
								flg = true;
							}
							if (flg == true)
							{
								arrReqid[k] = frmObj.elements[i].value;	
								k++;
							}					
					}
				}
			}
	
	if (arrReqid.toString() == "")
	{
		Error_Details(message[10048]);
	}
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}	
	document.frmLoanerReconciliation.hreqids.value = arrReqid.toString();
	document.frmLoanerReconciliation.strOpt.value = "Email";
	fnStartProgress();
	document.frmLoanerReconciliation.submit();
}

function addToArray(arrReqid,val)
{
	for(i=0;i<arrReqid.length;i++){
		if (arrReqid[i] == val)
		{
			return arrReqid;
		}					
	}
	arrReqid[arrReqid.length+1] = val;	
	//alert(arrReqid);
	return arrReqid;

}
function fnLoadRules()
{
	var cnt = document.frmVendor.hCnt.value; 
	var inputStr = '';
	for (var i=0;i<cnt;i++)
	{
		pnumobj = eval("document.frmVendor.hPartNum"+i);		
		inputStr = inputStr+pnumobj.value + ",";
	}
	if(inputStr.length > 1)
	{
		inputStr = inputStr.substr(0,inputStr.length-1);
		inputStr = "1^50607^"+inputStr + "^|";
	}
	document.frmVendor.action = "/gmRuleReport.do?loadRule=true&inputStr="+inputStr+"&strOpt=reload&txnId=0&consequenceId=0&status=0&initiatedBy=0";
	document.frmVendor.submit();
}
function fnSubmit()
{
	Error_Clear();
	var pnumobj = '';
	var missobj = '';
	var qtyobj = '';
	var priceobj = '';
	var setqty = 0;
	var missqty = 0;
	var strErrorMore = '';
	var strErrorTotal = '';
	var strErrorStock = '';
	var cnt = document.frmVendor.hCnt.value; 
	var str = '';
	var retobj = '';
	var quarobj = '';
	var stockobj = '';
	var repobj = '';
	var retqty = 0
	var quarqty = 0;
	var stockqty = 0;
	var repqty = 0;
	var retstr = '';
	var quarstr = '';
	var reptotal = 0;
	var retstr = '';
	var quarstr = '';
	var repstr = '';
	var strErrorQty = '';
	
	
	for (var i=0;i<cnt;i++)
	{
		reptotal = 0;
		pnumobj = eval("document.frmVendor.hPartNum"+i);
	 	missobj = eval("document.frmVendor.Txt_Miss"+i);
	 	qtyobj = eval("document.frmVendor.hSetQty"+i);
	 	priceobj = eval("document.frmVendor.hPrice"+i);
		retobj = eval("document.frmVendor.Txt_Ret"+i);
		quarobj = eval("document.frmVendor.Txt_Quar"+i);
		stockobj = eval("document.frmVendor.hStock"+i);
		repobj = eval("document.frmVendor.Txt_Rep"+i);

	 	setqty = parseInt(qtyobj.value);
	 	missqty = parseInt(missobj.value);
		retqty = parseInt(retobj.value);
		quarqty = parseInt(quarobj.value);
		stockqty = parseInt(stockobj.value);
		repqty = parseInt(repobj.value);
		
		if(!isNaN(repqty) && repqty > 0) //either 0 or more
		{
			if ( !isNaN(missqty) && !isNaN(quarqty))
			{
				if ((missqty+quarqty) != repqty && (missqty+quarqty) < repqty)
				{
					strErrorQty  = strErrorQty + ","+ pnumobj.value;
				}
			}
			else if (!isNaN(missqty))
			{
				if (missqty != repqty)
				{
					strErrorQty  = strErrorQty + ","+ pnumobj.value;
				}
			}
			else if (!isNaN(quarqty))
			{
				if (quarqty != repqty)
				{
					strErrorQty  = strErrorQty + ","+ pnumobj.value;
				}
			}
			else
			{
				strErrorQty  = strErrorQty + ","+ pnumobj.value;
			}
		}		
	 	if (!isNaN(missqty))
	 	{
	 		if (missqty > setqty)
	 		{
	 			strErrorMore  = strErrorMore + ","+ pnumobj.value;
	 		}else{
	 			str = str + pnumobj.value + ','+ missobj.value + ',,'+priceobj.value+'|';
				reptotal = parseInt(reptotal) + missqty;
	 		}
	 	}


	 	if (!isNaN(retqty))
	 	{
	 		if (retqty > setqty)
	 		{
	 			strErrorMore  = strErrorMore + ","+ pnumobj.value;
	 		}else{
	 			reptotal = parseInt(reptotal) + retqty;
				if (reptotal > setqty)
				{
	 				strErrorTotal  = strErrorTotal + ","+ pnumobj.value;
					reptotal = parseInt(reptotal) - retqty;
				}else{
					retstr = retstr + pnumobj.value + ','+ retqty + ',,'+priceobj.value+'|';
				}
				
	 		}
	 	}

	 	if (!isNaN(quarqty))
	 	{
	 		if (quarqty > setqty)
	 		{
	 			strErrorMore  = strErrorMore + ","+ pnumobj.value;
	 		}else{
	 			reptotal = parseInt(reptotal) + quarqty;
				if (reptotal > setqty)
				{
	 				strErrorTotal  = strErrorTotal + ","+ pnumobj.value;
					reptotal = parseInt(reptotal) - quarqty;
				}else{
					quarstr = quarstr + pnumobj.value + ','+ quarqty + ',,'+priceobj.value+'|';
				}
				
	 		}
	 	}

	 	if (!isNaN(repqty))
	 	{
	 		if (repqty > stockqty)
	 		{
	 			strErrorStock  = strErrorStock + ","+ pnumobj.value;
	 		}else{
	 			repstr = repstr + pnumobj.value + ','+ repqty + ',,'+priceobj.value+'|';
	 		}
	 	}


	}
	
	if (strErrorMore != '')
	{
		Error_Details(Error_Details_Trans(message[10049],strErrorMore.substr(1,strErrorMore.length)));
	}

	if (strErrorTotal != '')
	{
		Error_Details(Error_Details_Trans(message[10050],strErrorTotal.substr(1,strErrorTotal.length)));
	}

	if (strErrorStock != '')
	{
		Error_Details(Error_Details_Trans(message[10051],strErrorStock.substr(1,strErrorStock.length)));
	}
	
	if (strErrorQty != '')
	{
		Error_Details(Error_Details_Trans(message[10052],strErrorQty.substr(1,strErrorQty.length)));
	}

	if (ErrorCount > 0)
	{
		Error_Show();
		return false;
	}else{
		//alert("STR is:"+str);
		//alert("RET STR is:"+retstr);
		//alert("QUAR STR is:"+quarstr);
		//alert("REP STR is:"+repstr);
		
		

		document.frmVendor.hAction.value = 'ProcessReturnSet';
		document.frmVendor.hInputStr.value = str;
		document.frmVendor.hInputRetStr.value = retstr;
		document.frmVendor.hInputQuarStr.value = quarstr;
		document.frmVendor.hInputRepStr.value = repstr;
		
		var inputString = getIncidentsString();
		//alert('inputString = ' + inputString);
		document.frmVendor.hInputChgStr.value = inputString;
	  	document.frmVendor.submit();
  	}

}

function getIncidentsString()
{
	var inputString = '';

	var form = document.frmVendor;
	for(var i=0; i<form.elements.length; i++)
	{
		if(form.elements[i].name.indexOf('charge') != -1 && form.elements[i].name.indexOf('hcharge') == -1)
		{
			var nm = 'h' + form.elements[i].name;
			var objId = document.getElementById(nm).value;
			var objVal = form.elements[i].value;
			
			if(objVal == '' || objVal == '0')
			{
				continue;
			}
			//alert('Adding id = ' + objId + ' and value = ' + objVal);
			inputString += objId + ';' + objVal + '|';
		}
	}
	return inputString;
}

function changeTROBgColor(object,val,BgColor) {
   var obj1 = eval("document.all.tr"+prevtr);
   obj1.style.background = "";
   var obj = eval("document.all.tr"+val);
   prevtr = val;
   obj.style.background = BgColor;
   object.style.background = BgColor;
}

function fnPrintVer(val)
{
	windowOpener("/GmInHouseSetServlet?hAction=ViewPrint&hConsignId="+val+"&hType="+type,"LoanPrint","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnLoadLoanToNames(obj){
	var loanto = obj.value;	
	if(loanto == '0'){
		document.getElementById("loanToName").value = '0';	
		document.frmLoanerReconciliation.loanToName.disabled = true;
	}
	if(loanto != '0' && loanto != '' ){
		document.frmLoanerReconciliation.loanToName.disabled = false;
		if (loanto == '19518') 
		{
			document.frmLoanerReconciliation.loanToName.options.length = 0;
			document.frmLoanerReconciliation.loanToName.options[0] = new Option("[Choose One]","0");
			for (var i=0;i<RepLen;i++)
			{
				arr = eval("alRepListArr"+i);
				arrobj = arr.split(",");
				id = arrobj[0];
				name = arrobj[1];
				document.frmLoanerReconciliation.loanToName.options[i+1] = new Option(name,id);
			}
			//document.getElementById("eventLoanToName").value = '0';
		}else if(loanto == '19523'){
			document.frmLoanerReconciliation.loanToName.options.length = 0;
			document.frmLoanerReconciliation.loanToName.options[0] = new Option("[Choose One]","0");
			for (var i=0;i<RepLen;i++)
			{
				arr = eval("alOUSListArr"+i);
				arrobj = arr.split(",");
				id = arrobj[0];
				name = arrobj[1];
				document.frmLoanerReconciliation.loanToName.options[i+1] = new Option(name,id);
			}
		}
		else if(loanto == '106400')
		{
		
			document.frmLoanerReconciliation.loanToName.options.length = 0;
			document.frmLoanerReconciliation.loanToName.options[0] = new Option("[Choose One]","0");
			for (var i=0;i<EmpLen;i++)
			{
				arr = eval("alEmpListArr"+i);
				arrobj = arr.split(",");
				id = arrobj[0];
				name = arrobj[1];
				document.frmLoanerReconciliation.loanToName.options[i+1] = new Option(name,id);
			}
			//document.getElementById("eventLoanToName").value = '0';		
		}
	}
}

function fnLoadDist(strType){
	if(strType == '4127' || strType == '0'){
		document.getElementById("PLL").style.display = "table-row";
		document.getElementById("IHL").style.display = "none";
	}else{
		document.getElementById("PLL").style.display = "none";
		document.getElementById("IHL").style.display = "table-row";
	}
}