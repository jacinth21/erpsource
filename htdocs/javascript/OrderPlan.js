function fnEnableLots(rowID){		
	val = eval("document.frmVendor.Validate_Flag"+rowID);
	vFl = val.checked;	
	if(vFl){		
		var textObj = eval("document.frmVendor.Lots_No"+rowID);
		textObj.disabled = false;		
	}else{		
		var textObj = eval("document.frmVendor.Lots_No"+rowID);
		textObj.value= '';
		textObj.disabled = true;
	}
}


/*
 *  This function will check if the WO DCO validation is applicable or not.
 *  The WO DCO validation is applicable for GMNA and for Product, Rework, Manufacture FG type only.
 */

function fnWODCOValidation(){
	
	var potype ='';
	var poarr ='';
	var potypefl='N';
	
	if(document.frmVendor.Cbo_PoType != undefined)
		potype = document.frmVendor.Cbo_PoType.value;
	
	
	if(wodco_vldn_potype!= undefined ){
		 poarr = wodco_vldn_potype.split(",");	
		 
		 // Check if the select PO type needs to be validated or not.
		for(var i=0;i<poarr.length;i++){
			if(poarr[i]  == potype ){
				potypefl = 'Y';
			}
		}	
		
		
		if (wodco_vldn_company == 'Y' && potypefl == 'Y'){
			return  'Y';
		}		
	}	
}


function fnSubmit()
{
	var vVend = '';
	var vPart = '';
	var vQty = 0;
	var vCost = 0;
	var vRev = '';
	var vFl = '';	
	var vVendTot = new Array();
	var vVendTotFinal = new Array();
	var cnt = 0;
	var fl = '';
	var amt = 0;
	var temp = 0;
	var str = '';
	var vDhr = 'N';
	var vVFL = '';
	var LotParts = '';
	var FARFlQty = '';
	var LotFieldVal = '';
	var LotPartsQty = '';
	var FARzeroQty = '';
	var MoreLotQty = '';
	var InvalidQty = '';
	var InvalidLot = '';
	var ZeroQty = '';
	var obj = document.frmVendor.elements;
	var objlen = obj.length;
	var textboxlen = document.frmVendor.hTextboxCnt.value;
	var vFAR = '';
	var vFARQty = 1;
	var vPriceId = '';
	var flag = 0;
	var wodcovldnfl= '';
	var wodcoid = '';
	var wodcomissingparts = '';
	var wodcomissingpartsfl = '';
	
	var potype = document.frmVendor.Cbo_PoType.value
	Error_Clear();
	if (potype == '0')
	{
		 Error_Details('Choose Type of PO');
	}
	else if (potype == '3101')
	{
		 Error_Details(message[801]);
	}
	
	wodcovldnfl = fnWODCOValidation();
	
var obj99 = '';
	for (var i=0;i<textboxlen;i++)
	{
		val = 'Txt_Qty'+i;
		obj99 = eval("document.frmVendor.Txt_Qty"+i);		
		if (obj99)
		{
			val = eval("document.frmVendor.hPartNum"+i);
			vPart = val.value;
			val = eval("document.frmVendor.Validate_Flag"+i);
			vVALIDATEFL = val.checked;
			vQty = obj99.value;
			
			if ((obj99.value).trim() != '')
			{	flag = 1;
				cnt++;
				vQty = obj99.value.trim();
				val = eval("document.frmVendor.hVend"+i);
				vVend = val.value;
				val = eval("document.frmVendor.hPartNum"+i);
				vPart = val.value;
				val = eval("document.frmVendor.hRev"+i);
				vRev = val.value;
				val = eval("document.frmVendor.Chk_Flag"+i);
				vFl = val.checked;
				val = eval("document.frmVendor.Chk_FAR"+i);
				vFAR = val.checked;
				
				val = eval("document.frmVendor.hWODCOID"+i);
				wodcoid = val.value;
				
				if(potype == '3100'){
				val = eval("document.frmVendor.Validate_Flag"+i);
				vVALIDATEFL = val.checked;					
				var lotTextObj = eval("document.frmVendor.Lots_No"+i);
				vLotCount = lotTextObj.value;
				}else{
					vVALIDATEFL  = '';					
					vLotCount    = '';
				}
				val = eval("document.frmVendor.hPid"+i);
				vPriceId = val.value;

				if (vRev =='')
				{
					alert("There are some Part Numbers that dont have Revision Numbers. \nPlease assign one before continuing");
					return false;
				}
				val = eval("document.frmVendor.hCost"+i);
				vCost = val.value;
				temp = vCost*vQty;
				
				// For Critical Flag
				if (vFl)
				{
					vFl = 'Y';
				}else
				{
					vFl = 'N';
				}
				// For Validate Flag
				if(vVALIDATEFL){
					vVFL = 'Y';
				}else{
					vVFL = 'N';
				}
				
				// Check if WO DCO is enabled and the value is set to No
				if(wodcoid == '103361' && wodcovldnfl =='Y' ){
					wodcomissingparts = wodcomissingparts +"<br>"+vPart;
					wodcomissingpartsfl = 'Y';
					Error_Clear();	
				}
				
				
				// For Checking the Negative values for Qty Tp Place and # of Lots Fields			
				if(vQty != ''){
					var invalidQtyFl = NumberValidation(vQty,'','');			
						if(!invalidQtyFl){
							InvalidQty = InvalidQty + "<br>"+vPart;	
							Error_Clear();
						}else if(vQty < 0){
							InvalidQty = InvalidQty + "<br>"+vPart;	
							Error_Clear();
						}
				}
				if(vVALIDATEFL && vLotCount != ''){
					var invalidLotVal = NumberValidation(vLotCount,'','');			
						if(!invalidLotVal){
							InvalidLot = InvalidLot + "<br>"+vPart;	
							vVALIDATEFL = false;
							Error_Clear();
						}else if(vLotCount < 0){
							InvalidLot = InvalidLot + "<br>"+vPart;	
							vVALIDATEFL = false;
							Error_Clear();
						}
				}	// End of Checking the Negative values for Qty Tp Place and # of Lots Fields		
				
				// For checking the Lot number is more than requseted Qty.
				if(vVALIDATEFL && (eval(vLotCount) > eval(vQty))){
					MoreLotQty = MoreLotQty+"<br>"+vPart;
					Error_Clear();
					vVALIDATEFL = false;
				}
				// For checking the # of Lots field is Empty and having the ZERO value.
				if(vVALIDATEFL && (vLotCount == '' || vLotCount =='0' )){
					LotParts = LotParts +"<br>"+vPart;
					Error_Clear();
					vVALIDATEFL = false;
				}				
				// For checking the FAR FLAG and Validated FLAG selection.
			    var FARErrorFl = true;
				if (vFAR){
					// For checking the Qty To Place having ONE when FAR selected.
					if(vQty == '1'){						
						FARzeroQty = FARzeroQty +"<br>"+vPart;
						Error_Clear();
					}
					if(vVALIDATEFL){
						var divsion = vQty%vLotCount;
						// For checking the Lot number is ONE and Qty to Place is divisible by 2 or not.
							if(vLotCount =='1' && (vQty%2) != 1)
							{
								FARFlQty = FARFlQty +"<br>"+vPart;
								FARErrorFl = false;
								Error_Clear();
							}else if((divsion != 1  && (vQty%2) != 1) && vLotCount !='1'){ // For Checking the requested Qty is divisible by Lot number and Lot Number should not be ONE.
								FARFlQty = FARFlQty +"<br>"+vPart;
								FARErrorFl = false;
								Error_Clear();
							}
					}else{
						vLotCount = '0';
					}
				if(FARErrorFl == true){ // if above two conditions fails the QTy will decrease one.
				vQty = vQty - 1;
				vFAR = 'Y';
				str = str + vPart+"^"+vRev+"^"+vVend+"^"+vFARQty+"^"+vCost+"^"+vFl+"^"+vVFL+"^"+vLotCount+"^"+vDhr+"^"+vFAR+"^"+vPriceId+"|";
				vFAR = ' ';
				}
				}else{
					vFAR = 'N';
				}
				
				// For checking the only for Validation check box selection.
				if(vVALIDATEFL && vLotCount != ''){
					var divsion = vQty%vLotCount;
					var mod = vQty%2;
					
					if(vLotCount =='1' && mod != 0)
					{
						LotFieldVal = LotFieldVal +"<br>"+vPart;
					}else if(divsion != '0' && vLotCount !='1'){
						LotFieldVal = LotFieldVal +"<br>"+vPart;
					}						
				}else{
					vLotCount = '0';
				}
				
				if (vQty != 0 && vQty > 0)
				{
					str = str + vPart+"^"+vRev+"^"+vVend+"^"+vQty+"^"+vCost+"^"+vFl+"^"+vVFL+"^"+vLotCount+"^"+vDhr+"^"+vFAR+"^"+vPriceId+"|";
				}else{
					ZeroQty = ZeroQty +"<br>"+vPart;
					Error_Clear();
				}

				if (cnt == 1)
				{
					vVendTot[cnt] = new Array(2);
					vVendTot[cnt][0] = vVend;
					vVendTot[cnt][1] = temp;
				}
				else
				{
					for (var t=1;t<vVendTot.length;t++)
					{
						if (vVend == vVendTot[t][0])
						{
							cnt--;
							amt = vVendTot[t][1];
							vVendTot[t][1] = amt + temp;
							fl = 1;
							break;
						}
						else
						{
							fl = 0;
						}
					}
					if (fl != 1)
					{
						vVendTot[cnt] = new Array(2);
						vVendTot[cnt][0] = vVend;
						vVendTot[cnt][1] = temp;
					}
				}
			}else if((vVALIDATEFL) &&  (vQty == '')){ // if  Validation Flag selected but no data in QTy Tp Place.
				LotPartsQty = LotPartsQty +"<br>"+vPart;
				flag = 1;
				Error_Clear();
			}
		}
	}
	fnValidationErrorMessages(ZeroQty,InvalidQty,InvalidLot,MoreLotQty,LotParts,LotPartsQty,FARzeroQty,FARFlQty,LotFieldVal,wodcomissingparts);
    // Raising the validation is Qty to Place is Empty.
	if (flag == 0)
	{
		Error_Details('Please fill Qty to place value to proceed further.');
	}
	vVendTot.splice(0,1);
	document.frmVendor.hPOString.value = vVendTot.join('|');
	document.frmVendor.hWOString.value = str.substring(0,str.length-1);
	document.frmVendor.hAction.value = "Initiate";
	//alert("WO: "+document.frmVendor.hWOString.value);
	//alert("PO: "+document.frmVendor.hPOString.value);
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	if (viaCellCompFlag =='Y'){
		fnValidatePOPlant()
	}else{
		fnStartProgress("Y");
		document.frmVendor.submit();
	}
}
function fnValidationErrorMessages(ZeroQty,InvalidQty,InvalidLot,MoreLotQty,LotParts,LotPartsQty,FARzeroQty,FARFlQty,LotFieldVal,wodcomissingparts){
	// Raising validation if Qty to Place having ZERO.
	if(ZeroQty!=''){
		Error_Details("<b>Qty To Place</b> should not be ZERO for Part #"+ZeroQty);
	}
	// Raising validation if Qty to Place having invalid characters and Negative Numbers.
	if(InvalidQty!=''){
		Error_Details("Please enter valid and Positive numbers in <b>Qty To Place</b> for Part #"+InvalidQty);
	}
	// Raising validation if # of Lots having Negative Numbers and Invalid characters.
	if(InvalidLot!=''){
		Error_Details("Please enter valid and Positive numbers in <b># of Lots</b> for Part #"+InvalidLot);
	}	
	// Raising validation if # of Lots having more number than Requested Qty.
	if(MoreLotQty!=''){
		Error_Details("<b># of Lots </b> should not be more than requested Qty for Part #"+MoreLotQty);
	}
	// Raising validation for # of Lots field as Mandatory.
	if(LotParts!= ''){
		Error_Details("<b># of Lots </b> should not be ZERO and Empty for Part #"+LotParts);		
	}
	// Raising the validation when # of Lots having the Value but nothing is entered in Qty to place.
	if(LotPartsQty!= ''){
		Error_Details("Please enter the value in <b>Qty To Place</b> for part # "+LotPartsQty);		
	}
	// IF Qty To Place value is '1' and FAR gets selected then Qty to Place becomes 'ZERO', to avoid this scenario
	if(FARzeroQty!= ''){
		Error_Details("<b>Qty to Place</b> should be more than ONE(1),when FAR Flag selected. for part # "+FARzeroQty);		
	}
	// Raising validation for FAR Flag as well as Validation Flag as EAR Qty needs to add to Qty to Place.
	if(FARFlQty!= ''){		
		Error_Details("Please include FAR Qty in <b>Qty To Place</b> for part # "+FARFlQty);		
	}
	// Raising the validation for Validation FLag as enter Even Qty to raise PO.
	if(LotFieldVal!= ''){		
		Error_Details("unable to split the work orders as the Quantity requested is not an even quantity to split for part # "+LotFieldVal);			
	}
	
	if(wodcomissingparts!=''){
		Error_Details(" <b>Work Order DCO </b> not done for the following Part(s) "+wodcomissingparts);
	}
}

//Ajax call for getting the Error message [While initiating the PO ,validating the PO based on product,part number and project]
function fnValidatePOPlant(){
	var vWOString = document.frmVendor.hWOString.value;
	var vProjId = document.frmVendor.hProjId.value;
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/GmOrderPlanServlet?hAction=validatePlant&projId='+vProjId+'&woString='+vWOString+'&ramdomId='+Math.random());
	dhtmlxAjax.get(ajaxUrl,fnGetValidMsg);

}

function fnGetValidMsg(loader){
	var response = TRIM(loader.xmlDoc.responseText);
	var indexof;
	var msg = ''; 
	if((response != null || response != '') ){
		indexof = response.indexOf(":");
		msg = (indexof != -1)?response.substring(indexof+1,response.length):'';//Getting the validation message from Database
		if(msg != ''){
			Error_Details(msg);
			Error_Show();
			Error_Clear();
			return false;
		}else{
			fnStartProgress("Y");
			document.frmVendor.submit();
		}
	}
	
}