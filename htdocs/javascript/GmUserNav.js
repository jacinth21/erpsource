var strLeftMenuTree;
String.prototype.trim = function() { return this.replace(/^\s+|\s+$/, ''); };
function setLeftMenuTree(strTree)
{
	strLeftMenuTree = strTree;
}

function hidetoc()
{
	top.fstMain.cols = "0,*";
	top.TopFrame.document.all("showtoc").style.display = "block";
}

function mouseover(item)
{
	if (GLOBAL_BrowserType == "SAFARI" || GLOBAL_ClientSysType == "IPAD")
	  {
		return true;
	  }
	switch (item)
    {
    case "moveprevious" :
      window.status = "Click to Close the Tree node";
      document.all.imgMovePrevious.src = strImagePath+"/moveprevious2.gif";
      break;

    case "movenext" :
      window.status = "Click to Expand the Tree node";
      document.all.imgMoveNext.src = strImagePath+"/movenext2.gif";
      break;

    case "hidetoc" :
      window.status = "Hide Portal Navigator";
      document.all.imgHideToc.src = strImagePath+"/hidetoc2.gif";
      //hidetoc();
      break;
   }
}

function mouseout(item)
  {
	if (GLOBAL_BrowserType == "SAFARI" || GLOBAL_ClientSysType == "IPAD")
	  {
		return true;
	  }
	  switch (item)
	   {
	    case "moveprevious" :
	     window.status = "";
	      document.all.imgMovePrevious.src = strImagePath+"/moveprevious1.gif";
	      break;
	
	    case "movenext" :
	      window.status = "";
	      document.all.imgMoveNext.src = strImagePath+"/movenext1.gif";
	      break;
	
	    case "hidetoc" :
	      window.status = "";
	      document.all.imgHideToc.src = strImagePath+"/hidetoc1.gif";
	      break;
	    }
 }

function fnReload(obj)
{
	val = obj.value;
	//alert('val='+val);
	text = obj[obj.selectedIndex].text;
	sess = obj[obj.selectedIndex].id;
	//Disable the Company,Plant dropdown is sales(100000000) module selected.
	if(val == '100000000' || val == '2005' || val == '2015'){
		fnToggleCompany('Y');	
	}else{
		fnToggleCompany('N');	
	}
	
	if(val == '100000142'){ // CRM PMT-10388
		top.location.href = "/GmLogonServlet?hAction=CRM";
	}
	else if(val != 2007)
	{
		self.location.href = "/gmUserNavigation.do?userGroup="+val+"&userGrpName="+text+"&strPgToLoad="+sess+"&"+fnAppendCompanyInfo();
	}
	else
	{
		top.location.href = "/GmClinicalPortal.jsp?";
	}
}

var tree;

function fnLoad()
{
	tree = new dhtmlXTreeObject("treeboxbox_tree", "100%", "100%", 0);	
	tree.setSkin('dhx_skyblue');
	tree.setImagePath("/extweb/dhtmlx/dhtmlxTree/imgs/csh_dhx_skyblue/");
	tree.enableSmartXMLParsing(true);	
	tree.enableDragAndDrop(true);
	tree.enableThreeStateCheckboxes(true);
	tree.setOnClickHandler(fnSelect);
	tree.setStdImages("leaf.gif", "folderOpen.gif", "folderClosed.gif");	
	tree.enableHighlighting(true); // is switched off by default
	tree.loadXMLString(strLeftMenuTree);
	
	var divReference=eval(document.getElementById('treeboxbox_tree'));
	
	if(GLOBAL_ClientSysType=='IPAD')
	{
		divReference.style.height="650px";
		divReference.style.overflow="auto";
		
	}else
	{
		if(GLOBAL_BrowserType=='SAFARI')
		{
			divReference.style.height="100%";
			divReference.style.overflow="auto";
		}else
		{
			var divHeight = ($(document).height() - $(divReference).offset().top) - 28;
			divReference.style.height= divHeight + "px";
			divReference.style.overflow="auto";
		}
	}
	var arr = tree.getAllChildless();
	var childless = arr.split(",");
	
	for ( var i=0, len=childless.length; i<len; ++i )
	{
		var id = childless[i];
		var access = tree.getUserData(id,"ACCESS");
		
		  if (access == "TRUE")
			 {
			  tree.setItemStyle(id,'color:black');
			 }
		  else
		  {
			  tree.setItemStyle(id,'color:lightgray');
		  }
	}
	
	//var childlessNew = tree.getAllChildless(); // returns the array of nodes ids
	//var selectid = childlessNew.substring(0,childlessNew.indexOf(","));
  //  tree.openItem(selectid); // expand the indicated item
  //  tree.selectItem(selectid,false,false);
  //  var sqid = tree.getUserData(selectid,"SQID");
  //  if(!sqid){
//	fnSelect(selectid);
  //}
}

function fnSelect(id){

	 var url = tree.getUserData(id,"URL");
	 var opt = tree.getUserData(id,"STROPT");
	 var sqid = tree.getUserData(id,"SQID");
	 var access = tree.getUserData(id,"ACCESS");
	 var fnname = tree.getUserData(id,"FUNCTION_NM");
	 var mobileurl = tree.getUserData(id,"URL");
	 var microappurl = tree.getUserData(id,"URL");  // PC-3071 - Set Out Of Policy
	 //Hide the Company,Plant dropdown when sales modules links are loaded from My Favorites.
	 var hideCompanyFl = getParameterByName('hideCompany', url);
	 // When map the Task to module mapping screen called this function.
	 // At the time top frame not present (here checking condition)
	 if(window.parent.TopFrame != undefined){
		 fnToggleCompany(hideCompanyFl);
	 }
	 if(sqid){
		 var isFolder = (tree.getUserData(id,"URL") == undefined)?true:false;
		 if(isFolder ==false ){
			 document.getElementById("msg").innerHTML="Select folder to add the task!";
		 }else{
			 window.opener.fnaddseqno(sqid);
			 document.getElementById("msg").innerHTML="";
			 window.close();
		 }
	 }else{
		 if(url)
		{
				  if (access == "TRUE")
				 {
					  var vDelimiter= "&"; //(url.indexOf("?")>0)?"&":"?";
					  //Getting company info. as JSON string from GmEnterprisePortal.jsp and append to url.
					  url += vDelimiter + fnAppendCompanyInfo();
					  if (GLOBAL_ClientSysType == "IPAD")
					  {	
						   //Below condition is to enable the progress bar for the sales module - GlobusOne Enterprise Portal - CRM : ipad 	
							if (hideCompanyFl == "Y") 
							{
								fnStartProgress('Y');
							}
						  top.RightFrame.location = "/GmPageControllerServlet?strPgToLoad="+url+"&strOpt="+opt ;
						  hidetoc();
					  } else if(opt == "GLOBUSMOBILEAPP"){//opt value GLOBUSMOBILEAPP needs to call GmMobileInterface.html to get the frame set and path for call the code location
						  	 fnSetUserInfo(mobileurl);//function to get the all UserInformation
							 top.RightFrame.location = "/GmMobileInterface.html";//set the screen in right frame location						   
					  } else if(opt == "NEWTAB"){		//opt value NEWTAB needs to call SOP index.html to get the frame set and path for call the code location 
						  	 window.open(microappurl);	//set the SOP screen in right frame location when click on SOP Left Link 
					  } else
					  {
						//Below condition is to enable the progress bar for the sales module - GlobusOne Enterprise Portal - Portal
							if (hideCompanyFl == "Y")
							{
							fnStartProgress('Y');
							}
						  parent.RightFrame.location.href = "/GmPageControllerServlet?strPgToLoad="+url+"&strOpt="+opt ;
					  }
				 }
				 else
				 {
				 			Error_Details(Error_Details_Trans(message[10079],fnname));
							Error_Show();
							Error_Clear();
							return false;
				 }			
	    } 
	 }
}

function parseNull(val){
	if(val=='null')
		return val="";
	else
		return val; 
}

//function to set the session information in userinfo JSON
function fnSetUserInfo(mobileurl){
	var companyid ;
	var plantid ;
	if(!(top.gCompanyInfo == undefined || top.gCompanyInfo == null))
	 {
		companyid = top.gCompanyInfo.cmpid;
		plantid = top.gCompanyInfo.plantid;
	 }
	
var userInfo = {};		
var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmPortalUserNavigation.do?method=fetchSessionInformation&ramdomId='+ Math.random()+'&cmpid='+companyid+'&plantid='+plantid);
dhtmlxAjax.get(ajaxUrl, function(loader){
	var response = loader.xmlDoc.responseText;
	var mySplitResult = response.split("^");
	userInfo.token = parseNull(mySplitResult[0]);
	userInfo.userid = parseNull(mySplitResult[1]);       
	userInfo.partyid = parseNull(mySplitResult[8]);			        
	userInfo.plantid = parseNull(mySplitResult[15]);			        
	userInfo.companyid = parseNull(mySplitResult[13]);	
	userInfo.cmptzone = parseNull(mySplitResult[16]);			       
	userInfo.cmpdfmt = parseNull(mySplitResult[17]);		        
	userInfo.usernm = parseNull(mySplitResult[2]);			        
	userInfo.deptid = parseNull(mySplitResult[3]);		       
	userInfo.deptseq =parseNull(mySplitResult[4]);			       
	userInfo.shname = parseNull(mySplitResult[5]);		       
	userInfo.acid =	parseNull(mySplitResult[6]);		       
	userInfo.logints = parseNull(mySplitResult[7]);		       
	userInfo.extaccess = parseNull(mySplitResult[9]);			        
	userInfo.divid = parseNull(mySplitResult[10]);		       
	userInfo.fname = parseNull(mySplitResult[11]);		        
	userInfo.lname = parseNull(mySplitResult[12]);		        
	userInfo.switchuser = parseNull(mySplitResult[14]);
	userInfo.cmplangid = parseNull(mySplitResult[18]);
	userInfo.cmpcurrsmb = parseNull(mySplitResult[19]);
	userInfo.sessionOption = "#"+mobileurl;	
	sessionStorage.setItem("userInfo", JSON.stringify(userInfo));
	});
}

function fnFavPop(){
	var funid=tree.getSelectedItemId();
	var funnm=tree.getSelectedItemText();
	// encode the function name 
	funnm = encodeURI(funnm);
	var isFolder = (tree.getUserData(funid,"URL") == undefined)?true:false;
	var grpid=document.frmUserNavigationSetup.userGroup.value;
	if(isFolder == true){
		  Error_Details(message[10080]);
		  Error_Show();
		  Error_Clear();
		  return false;
	}
	if(funid =="")
	 {
		  Error_Details(message[10081]);
		  Error_Show();
		  Error_Clear();
		  return false;
	 }
	 
	 var access = tree.getUserData(funid,"ACCESS");
	 
	 if (access != "TRUE")
	 {
		  Error_Details(message[10082]);
		  Error_Show();
		  Error_Clear();
		  return false;

	 }
	windowOpener("/gmUserNavigation.do?strOpt=RENAME&funId="+funid+"&funName="+funnm + '&grpId=' + grpid ,"RENAME","resizable=yes,scrollbars=no,top=150,left=150,width=370,height=150");
}
function fnAddFav(){	
	 var funnm=document.frmUserNavigationSetup.funName.value;
	 if(funnm.trim() == ''){
			document.getElementById("msg").innerHTML = "Please enter the Link Name";
			document.frmUserNavigationSetup.funName.value='';
			document.frmUserNavigationSetup.funName.focus();
			 return false;
	 }
	 var blVal = fnSkipSpec(document.frmUserNavigationSetup.funName);
	 if(blVal){
		 document.frmUserNavigationSetup.action = '\gmUserNavigation.do?grpId=' + grpid + '&funId=' + funid + '&funName=' + funnm +  '&strOpt=AddFav&randomId=' + Math.random();
		 document.frmUserNavigationSetup.submit();
	 }
}


function fnKeyPress(evt){
	if(evt.keyCode==13){
		alert('yes');
	}
}
function fnRemFav(){
	var funid=tree.getSelectedItemId();
	var grpid=document.frmUserNavigationSetup.userGroup.value;
	
	 if(funid =="")
	 {
		  Error_Details(message[10083]);
		  Error_Show();
		  Error_Clear();
		  return false;
	 }
	 
	 document.frmUserNavigationSetup.action = '\gmUserNavigation.do?grpId=' + grpid + '&funId=' + funid + '&strOpt=RemFav';
	 document.frmUserNavigationSetup.submit();
}

function fnSkipSpec(obj) 
{
	var input = obj.value;
	var strSpecFound = input.replace(/[a-zA-Z->() 0-9]+/g,'');
	if(strSpecFound.length > 0){
		document.getElementById("msg").innerHTML = "Special characters are not allowed except ( -,>,() )";
		  obj.focus();
		  return false;
	}else{
		document.getElementById("msg").innerHTML = "";
		return true;
	}
	//obj.value = input.replace(/[^a-zA-Z- 0-9]+/g,'');
}