var TREE_ITEMS = [
	['Surgeon Profile', null,
		['Setup', null,
			['Surgeon Setup', 'gmPartySetup.do?partyType=7000\',\'']	
		],
		['Reports', null,
            ['Surgeon Search', 'gmPartyCommonSearch.do?partyType=7000\',\'mainpage'],
			['Surgeon Rep Search', 'gmSurgeonSearchReport.do\',\'']
		]
	],
	['Team Staffing', null,
		['Setup', null,
			['Project Setup', 'gmPDProjectSetup.do\',\'']	
		],
		['Reports', null,
            ['Projects Report', 'gmProjectsReport.do\',\'load'],
			['Projects Dashboard', 'gmProjectsDashboard.do\',\'fetch'],
			['Project Staffing Report', 'gmProjectStaffingReport.do\',\'']
		]
	],
	['Reports', null,
			['Field Sales/Accounts Report', 'GmAccountReportServlet\',\'ALL'],
	]
];