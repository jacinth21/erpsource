//To support edge browser set grid from table 
function initGridFromTable(htmlTableId)
{
    var gridTable =dhtmlXGridFromTable(htmlTableId);
	gridTable.setSkin("dhx_skyblue");
	gridTable.setImagePath("/extweb/dhtmlx/dhtmlxGrid/imgs/");	
	gridTable.enableAutoWidth(true);
	gridTable.enableAutoHeight(true);
	
	return gridTable;
}
