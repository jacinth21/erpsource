var foSize = new Array();		 /* Holds column length */
//This code will format the string and add the same into the listbox
function AddItem(listbox , optionValue ) {
	var no = new Option();
	no.value = listbox.options.length + 1;
	no.text = FormatOption(foSize , optionValue);
	listbox.options[listbox.options.length] = no;
}

// This code will format the string and will return the same to you 
function FormatOption(arraysize , arrayvalue) {
	
	var string;
	var stringSize;
	
	var len = arrayvalue[0].length;
	string = "";

	for(var i = 0 ; i <= (TotalColumn); i++) {
		stringSize = arrayvalue[i];
		
			
		if(arrayvalue[0][i] != null && arrayvalue[0][i].length > arraysize[i])
		{
			
			string = string + new String(arrayvalue[0][i]).substring(0,arraysize[i]-5) + "..   ";
		}
		else
		{
			string = string + arrayvalue[0][i] + OptionSpace(arraysize[i] -  arrayvalue[0][i].length);
		}	
	}

	return string;
}
function UpdateItem(listbox , optionValue ) {
	var len = listbox.options.length;
	var currentPosition; 
	
	var no = new Option();
	var ArrayIndex = -1;

	for(var i = (len-1); i >= 0; i--) {
	if ((listbox.options[i] != null) && (listbox.options[i].selected == true)) {
		currentPosition = i; 
		}
	}

	if (currentPosition != null)  {

		no.value = currentPosition;
		no.text = FormatOption(foSize , optionValue);	
		listbox.options[currentPosition] = no;
		
		ArrayIndex = no.value;
	}

	return ArrayIndex;
}

function OptionSpace(size) {
	
	var string;
	string = "";
	for(var i = 0 ; i < size; i++) {
		string = string + " "
	}
	
	return string;
}



/*!-- This code is used to remove all the record from the listbox --*/
function RemoveAllItem(listbox) {
	var len = listbox.options.length;
	for(var i = (len-1); i >= 0; i--) {
		listbox.options[i] = null;
		}
}

/*!-- This code is used to remove the record from the listbox --*/
function RemoveItem(listbox) {
	
	var string;
	var len = listbox.options.length;
	for(var i = (len-1); i >= 0; i--) {
	if ((listbox.options[i] != null) && (listbox.options[i].selected == true)) {
		listbox.options[i] = null;
		}
	}
}


/* Sorted the table based on user input */
function ListSort(ColumnNo,ListArray,listbox) {
	
	var len = ListArray.length;

	for (var j = 0; j<len; j++){
		for(var i=0; i<len - 1; i++){
			var l = 1 * i + 1;
			if(1 * ListArray[i][ColumnNo] > 1 * ListArray[i+1][ColumnNo]){   
				exchange(i,i+1,ListArray);  
			}
		}
	}

	RemoveAllItem(listbox);
	ReLoad_Array(ListArray,listbox);  

}

/* used to swap the record */
function exchange(CurrentIndex , NextIndex , ListArray) {

	var len = ListArray[0].length  ;	<!-- To get the array length -->
	var optionValue = new Array();
	optionValue[0] = new Array(len)	
	var no = new Option();
	
	/* Move the first value*/
	for(var i = 0 ; i <= (len); i++) {
		optionValue[0][i] = ListArray[CurrentIndex][i] 
	}	
	
	/* Get the next value*/
	for(var i = 0 ; i <= (len); i++) {
		ListArray[CurrentIndex][i]  = ListArray[NextIndex][i]  ;
	}


	/* To Assign the moved value*/
	for(var i = 0 ; i <= (len); i++) {
		ListArray[NextIndex][i]  =  optionValue[0][i] ;
	}
		
}

function ReLoad_Array(ListArray,listbox) {

	var len = ListArray.length;	<!-- To get the array length -->

	for(var i = 0 ; i <= (len-1); i++) {
		var optionValue = new Array();
		optionValue[0] = new Array(TotalColumn)
		var no = new Option();
		string = "";
		
		for(var j = 0 ; j <= (TotalColumn); j++) {
			optionValue[0][j] = foListArray[i][j]; 
		}
		AddItem(listbox,optionValue)
	}

}



