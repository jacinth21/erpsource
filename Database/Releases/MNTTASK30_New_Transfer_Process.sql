



--New Column For Tag
ALTER TABLE t507_returns_item
 ADD C5010_TAG_ID  VARCHAR2(40 BYTE);
 
 
--  select get_code_name(54011)  from dual
 INSERT INTO t901_code_lookup
			(c901_code_id, c901_code_nm, c901_code_grp, c901_active_fl, c901_code_seq_no, c902_code_nm_alt
		   , c901_control_type
			)
	 VALUES (54011, 'Tag Validation during transfer', 'TFTAG', 1, 1, NULL
		   , NULL
			);
-- select get_code_name(7006) from dual
-- select * from t101_party where c101_party_id=303497
 /*INSERT INTO t101_party
                     (c101_party_id, c101_last_nm, c101_first_nm,
                      c101_middle_initial, c901_party_type, c101_party_nm,
                      c101_active_fl, c101_created_by, c101_created_date
                     )
              VALUES (303497, 'Patel', 'Mihir',
                      '', 7006, 'Mihir Patel',
                      'Y', 303497, SYSDATE
                     );*/

-- select * from t1500_group where c1500_group_id = '26'

INSERT INTO t1500_group
            (c1500_group_id, c1500_group_nm, c1500_group_desc, c1500_void_fl,
             c1500_created_by, c1500_created_date, c1500_last_updated_by,
             c1500_last_updated_date
            )
     VALUES ('26', 'Transfer Tag  Missing Supervisor', 'Transfer Tag  Missing Supervisor', NULL,
             '303497', SYSDATE, NULL,
             NULL
            );
            
--select max(c1501_group_mapping_id) from  t1501_group_mapping 
--select *  from  t1501_group_mapping where c1501_group_mapping_id = 5761
--select get_user_name(303209) from dual

INSERT INTO t1501_group_mapping
            (c1501_group_mapping_id, c101_mapped_party_id,
             c1500_mapped_group_id, c1500_group_id
            )
     VALUES (5761, 303209, -- use  the required user ID for access 303209 Nancy Undercofler 
             NULL, '26'
            );
   
 --select get_code_name(92260)   from dual
 --select max(c1520_function_id) from t1520_function_list  
 -- select * from t1520_function_list  where c1520_function_id = '3250006'
  
INSERT INTO t1520_function_list
            (c1520_function_id, c1520_function_nm, c1520_function_desc,
             c1520_inherited_from_id, c901_type, c1520_void_fl,
             c1520_created_by, c1520_created_date, c1520_last_updated_by,
             c1520_last_updated_date, c1520_request_uri
            )
     VALUES ('3250006', 'Transfer Tag Missing', 'Transfer Tag Missing', -- Use a new function ID  code id
             NULL, NULL, NULL,
             '303497', SYSDATE, NULL,
             NULL, NULL
            );     

INSERT INTO t1530_access
            (c1530_access_id, c1500_group_id, c1520_function_id,
             c101_party_id, c1530_update_access_fl, c1530_read_access_fl,
             c1530_void_access_fl, c1530_void_fl, c1530_created_by,
             c1530_created_date, c1530_last_updated_by,
             c1530_last_updated_date
            )
     VALUES (s1530_access.nextval, '26', '3250006',
             NULL, 'Y', 'N',
             'N', NULL, '303497',
             SYSDATE, NULL,
             NULL
            );

@ "E:\database\Packages\Operations\gm_pkg_op_return.pkg"

@ "E:\database\Packages\Operations\gm_pkg_op_return.bdy"

@ "E:\Database\Packages\Operations\Gm_Pkg_Op_Excess_Return.bdy"

@"E:\database\packages\accounting\gm_pkg_ac_audit_reconciliation.bdy"

@"E:\Database\Packages\CustomerServices\gm_pkg_cs_transfer.PKG";
@"E:\Database\Packages\CustomerServices\gm_pkg_cs_transfer.BDY";
       