/*
 * This view is used to CRM perceptorship module reports
 */
CREATE OR REPLACE VIEW v6660_case_req_dlt
AS
  SELECT t6660.C6660_CASE_REQUEST_ID,
  	t6660.C2090_SEGMENT_ID,
  	t6660.C6630_ACTIVITY_ID,
  	t6660.C101_SURGEON_PARTY_ID,
  	t6660.C6660_FROM_DT,
  	t6660.C6660_TO_DT,
  	t6660.C901_STATUS,
  	t6660.C901_TYPE,
  	t6660.C6660_NOTE,
  	t6660.C6660_HOTEL,
  	t6660.C6660_HOTEL_PRICE,
  	t6660.C6660_TRAVEL,
  	t6660.C6660_TRAVEL_PRICE,
  	t6660.C6660_PCRF_CRF_STATUS,
  	t6660.C6660_TRF_STATUS,
  	t6660.C6660_LAB_INVOICE,
  	t6660.C6660_LAB_ADDRESS,
  	t6660.C6660_PECIMEN_INVOICE,
  	t6660.C6660_CREDENTIAL_FL,
  	t6660.C6660_SURVEY_FL,
  	t6660.C6660_VOID_FL,
  	t6660.C6660_UPDATED_BY,
  	t6660.C6660_UPDATED_DT,
  	t6660.C6660_CREATED_BY,
  	t6660.C6660_CREATED_DT,
  	t6660.C901_WORK_FLOW,
  	t6660.C1910_DIVISION_ID,
  	t6660.C901_PRECEPTOR_TYPE,
  	t6660.C6660_CALL_OBJECTIVES,
--  	t6660.T6660_CASE_REQUEST,
  	t6660.C6660_PATHOLOGY,
  	t6660.C6660_UPDATE_FL,
  	t6660.C901_REGION,
  	t6660.C901_ZONE,
  	t6660.C901_ADMIN_STATUS,
  	t6660.C901_ATTENDEE_TYPE,
  	t6660.C6630_ATTENDEE_NAME,
    t6661.assocnames,
    t6662.setnames,
    t6663.HOSPITAL
  FROM t6660_case_request t6660,
    (SELECT c6660_case_request_id,
      LISTAGG(GET_CRM_PARTY_NAME(c101_party_id), ' / ') WITHIN GROUP(
    ORDER BY c6660_case_request_id) AS assocnames
    FROM t6661_case_request_assoc
    WHERE c6661_void_fl IS NULL
    AND c901_role        = '105561'
    GROUP BY c6660_case_request_id
    ) T6661,
    (SELECT c6660_case_request_id,
      LISTAGG(GET_SET_NAME(c207_set_id), ' / ') WITHIN GROUP (
    ORDER BY c6660_case_request_id) AS SETNAMES
    FROM t6662_case_request_detail
    WHERE c6662_void_fl IS NULL
    GROUP BY c6660_case_request_id
    ) t6662,
    (SELECT c6660_case_request_id,
      LISTAGG(C6663_ATTRIBUTE_NM, ' / ') WITHIN GROUP (
    ORDER BY c6660_case_request_id) AS HOSPITAL
    FROM t6663_case_req_attr
    WHERE c6663_void_fl IS NULL
    AND C901_ATTRIBUTE_TYPE = '107101'
    GROUP BY c6660_case_request_id
    ) t6663
  WHERE T6660.C6660_VOID_FL      IS NULL
  AND t6660.c6660_case_request_id = t6662.c6660_case_request_id (+)
  AND t6660.c6660_case_request_id = t6661.c6660_case_request_id (+)
  AND t6660.c6660_case_request_id = t6663.c6660_case_request_id (+);
  /
