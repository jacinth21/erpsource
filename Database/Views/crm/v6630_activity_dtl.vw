/*
 * This view is used to CRM perceptorship module reports
 */

CREATE OR REPLACE VIEW v6630_activity_dtl
AS
  SELECT t6630.C6630_ACTIVITY_ID,
  	t6630.C6630_ACTIVITY_NM,
  	t6630.C6630_ACTIVITY_DESC,
  	t6630.C6630_VOID_FL,
  	t6630.C6630_FROM_DT,
  	t6630.C6630_TO_DT,
  	t6630.C6630_LAST_UPDATED_BY,
  	t6630.C901_ACTIVITY_TYPE,
  	t6630.C6630_LAST_UPDATED_DATE,
  	t6630.C901_ACTIVITY_STATUS,
  	t6630.C901_ACTIVITY_CATEGORY,
  	t6630.C901_SOURCE_TYPE,
  	t6630.C6630_SOURCE,
  	t6630.C6630_CREATED_BY,
  	t6630.C6630_CREATED_DATE,
  	t6630.C6630_FROM_TM,
  	t6630.C6630_TO_TM,
  	t6630.C6630_PARTY_ID,
  	t6630.C6630_PARTY_NM,
  	t6630.C703_SALES_REP_ID,
  	t6630.C6630_EVENT_ITEM_ID,
  	t6630.C6630_OTHER_INSTRUMENT,
  	t6630.C6630_TOP_PRIORITY,
  	t6630.C1910_DIVISION_ID,
  	t6630.C6630_NOTE,
  	t6630.C6630_UPDATE_FL,
  	t6630.C6630_DIETARY,
  	t6630.C2090_SEGMENT_ID,
  	t6630.C901_REGION,
  	t6630.C6630_PATHOLOGY,
  	t6630.C901_WORK_FLOW,
  	t6630.C901_ZONE,
  	t6630.C901_ADMIN_STATUS,
    surgeons.names surgeonnms,
    surgeons.ids surgeonids,fieldsales.names fieldsalesnms,
    prod.names prodnms,
    get_segment_name(t6630.c2090_segment_id) segnm,
    get_code_name( t6630.c901_activity_status) actstatus
  FROM t6630_activity t6630,
    (SELECT t6631.c6630_activity_id,
      LISTAGG(GET_CRM_PARTY_NAME(t6631.c101_party_id), ' / ') WITHIN
      GROUP (
    ORDER BY t6631.c101_party_id) AS names,
      LISTAGG(t6631.c101_party_id, ' / ') WITHIN GROUP (
    ORDER BY t6631.c101_party_id) AS IDS
    FROM t6631_activity_party_assoc t6631
    WHERE t6631.c901_activity_party_role IN ('105564')
    AND t6631.c6631_void_fl              IS NULL
    GROUP BY t6631.c6630_activity_id
    ) surgeons,
    (SELECT t6631.c6630_activity_id,
      LISTAGG(GET_CRM_PARTY_NAME(t6631.c101_party_id), ' / ') WITHIN
      GROUP (
    ORDER BY t6631.c101_party_id) AS names,
      LISTAGG(t6631.c101_party_id, ' / ') WITHIN GROUP (
    ORDER BY t6631.c101_party_id) AS IDS
    FROM t6631_activity_party_assoc t6631
    WHERE t6631.c901_activity_party_role IN ('105561')
    AND t6631.c6631_void_fl              IS NULL
    GROUP BY t6631.c6630_activity_id
    ) fieldsales,
    (SELECT t6632.c6630_activity_id,
      LISTAGG(get_set_name(t6632.c6632_attribute_value),' / ' ) WITHIN GROUP(
    ORDER BY t6632.c6632_attribute_value) AS Names
    FROM t6632_activity_attribute t6632
    WHERE t6632.c6632_void_fl IS NULL
    AND t6632.C901_ATTRIBUTE_TYPE  = 105582
    GROUP BY t6632.c6630_activity_id
    ) PROD
  WHERE t6630.c6630_void_fl        IS NULL
  AND t6630.c6630_activity_id       = prod.c6630_activity_id(+)
  AND t6630.c6630_activity_id       = surgeons.c6630_activity_id(+)
  AND t6630.c6630_activity_id       = fieldsales.c6630_activity_id(+)
  AND t6630.c901_activity_category in ('105272');
  /
