-- @ "C:\Projects\branches\PMT\db\Views\common\v901_order_type_grp.vw";

-- Creating Materialized view to store the order types exclusion:
-- View used in GmSalesFilterConditionBean.java
-- PMT-20656: Author :gpalani Jun 2018
DROP  MATERIALIZED VIEW V901_ORDER_TYPE_GRP;

CREATE MATERIALIZED VIEW V901_ORDER_TYPE_GRP
NOCACHE
LOGGING
NOCOMPRESS
NOPARALLEL
REFRESH FORCE ON DEMAND
AS
SELECT t906.c906_rule_value FROM t906_rules t906 WHERE t906.c906_rule_grp_id IN('ORDTYPE','ORDERTYPE') AND c906_rule_id = 'EXCLUDE';