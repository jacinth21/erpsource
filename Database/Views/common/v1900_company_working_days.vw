--@"C:\Database\Views\common\v1900_company_working_days.vw";

/************************************************************************************************************************
* Description      : Using this view to get the company working days count (exculde Sat , Sunday and company holidays)
* 
* Example		   : before use the view need to set the company context and single in list context
* 
* 				   		: 1. gm_pkg_cor_client_context.gm_sav_client_context(1000,3000)
* 						: 2. my_context.set_my_inlist_ctx ('01/01/2019')
* 
* Author           : mmuthusamy
*************************************************************************************************************************/

CREATE OR REPLACE VIEW v1900_company_working_days
AS
     SELECT date_key_id date_key_id, cal_date cal_date, quarter_number quarter_number
      , week_number week_number, month_number month_number, rownum working_day
       FROM
        (
             SELECT date_key_id, cal_date, quarter_number
              , week_number, month_number
               FROM date_dim
              WHERE cal_date BETWEEN TRUNC (to_date (
                (
                     SELECT token FROM v_in_list
                )
                , 'MM/dd/yyyy'), 'YEAR')
                AND ADD_MONTHS (TRUNC (to_date (
                (
                     SELECT token FROM v_in_list
                )
                , 'MM/dd/yyyy'), 'YEAR'), 12) - 1
                AND DAY NOT                          IN ('Sat', 'Sun')
                AND date_key_id NOT                  IN
                (
                     SELECT date_key_id
                       FROM company_holiday_dim
                      WHERE holiday_date BETWEEN TRUNC (to_date (
                        (
                             SELECT token FROM v_in_list
                        )
                        , 'MM/dd/yyyy'), 'YEAR')
                        AND ADD_MONTHS (TRUNC (to_date (
                        (
                             SELECT token FROM v_in_list
                        )
                        , 'MM/dd/yyyy'), 'YEAR'), 12) - 1
                        AND holiday_fl        = 'Y'
                        AND c1900_company_id IN
                        (
                             SELECT get_compid_frm_cntx () FROM dual
                        )
                )
           ORDER BY cal_date
        ) ;
    /