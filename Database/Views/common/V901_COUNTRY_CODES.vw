--@"C:\Database\Views\common\V901_COUNTRY_CODES.vw";

-- view to get country codes
CREATE OR REPLACE VIEW V901_COUNTRY_CODES
AS
    SELECT  C906_RULE_VALUE COUNTRY_ID
                  FROM T906_RULES T906
                WHERE T906.C906_RULE_GRP_ID = 'COUNTRYCODE' 
                   AND T906.C906_RULE_ID = 'INCLUDE'
                   AND T906.C906_VOID_FL IS NULL;
/                 
