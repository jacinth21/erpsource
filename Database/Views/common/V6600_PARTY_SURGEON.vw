--@"C:\pmt\db\Views\common\V6600_PARTY_SURGEON.vw";

-- view to party surgeon details;  --26230761:NPI
CREATE OR REPLACE VIEW V6600_PARTY_SURGEON
AS
	    SELECT t6600.c6600_surgeon_npi_id,
	  t6600.c101_party_surgeon_id,
	  t101.c101_party_id,    
	  DECODE(c101_party_nm , NULL, c101_first_nm
	  || ' '
	  || c101_last_nm
	  || ' '
	  || c101_middle_initial
	  ||', '
	  ||t901.c902_code_nm_alt , c101_party_nm) partynm,
	  t106.c106_add1,
	  t106.c106_add2,
	  t106.c106_city,
	  get_code_name(t106.c901_state) state ,
	  t106.c106_zip_code,
	  get_code_name(t106.c901_country) country,
	  t6600.C1900_COMPANY_ID compid,
	  t6600.C5040_PLANT_ID plantid
	FROM t6600_party_surgeon t6600,
	  t101_party t101,
	  t106_address t106,
	  t901_code_lookup t901
	WHERE t6600.c6600_void_fl(+)      IS NULL
	AND t6600.c101_party_surgeon_id(+) = t101.c101_party_id  -- changed the main table as t101
	AND t101.c901_party_type='7000' -- Surgeon Type
	AND t101.c101_void_fl       IS NULL
	AND t101.c101_party_id          = t106.c101_party_id(+)
	AND t106.c106_primary_fl(+)     ='Y'
	AND t106.c106_void_fl(+)       IS NULL
	AND t106.c901_state             = t901.c901_code_id  (+)
	AND t901.c901_void_fl(+)          IS NULL
	AND (t101.c101_active_fl IS NULL OR t101.c101_active_fl = 'Y');
/                 
