CREATE OR REPLACE FORCE VIEW v502_item_order_attribute
AS
   SELECT t502.c502_item_order_id, t502.c501_order_id,
          t502.c205_part_number_id, t502.c502_control_number,
          t502.c502_construct_fl, t502.c502_item_price, t502.c502_item_qty,
          t502.c502_item_seq_no, t502.c502_ref_id, t502.c502_vat_rate,
          t502.c901_type, prc_discrepency c502_price_disc_fl,
          tag_id c5010_tag_id, t502.c502_delete_fl, t502.c502_void_fl
     FROM t502_item_order t502,
          (SELECT   t502a.c501_order_id, t502a.c205_part_number_id,
                    MAX (DECODE (t502a.c901_attribute_type,
                                 100820, t502a.c502a_attribute_value,
                                 NULL
                                )
                        ) prc_discrepency,
                    MAX (DECODE (t502a.c901_attribute_type,
                                 100821, t502a.c502a_attribute_value,
                                 NULL
                                )
                        ) tag_id
               FROM t502a_item_order_attribute t502a
           GROUP BY t502a.c501_order_id, t502a.c205_part_number_id) t502a
    WHERE t502.c501_order_id = t502a.c501_order_id(+)
          AND t502.c205_part_number_id = t502a.c205_part_number_id(+);
