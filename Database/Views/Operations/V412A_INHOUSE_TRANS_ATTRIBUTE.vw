CREATE OR REPLACE VIEW V412A_INHOUSE_TRANS_ATTRIBUTE
AS
	SELECT max(decode(t412a.C901_ATTRIBUTE_TYPE,104521,t412a.C412A_ATTRIBUTE_VALUE, 'N')) "LABELS_PRINTED"
          ,max(decode(t412a.C901_ATTRIBUTE_TYPE,104522,t412a.C412A_ATTRIBUTE_VALUE, 'N')) "QC_VERIFIED"
          ,t412a.C412_INHOUSE_TRANS_ID, t412.c412_status_fl STATUS_FL
          ,max(decode(t412a.C901_ATTRIBUTE_TYPE,104523,t412a.C412A_ATTRIBUTE_VALUE, 'N')) "LABEL_PRE_STAGE"
         ,max(decode(t412a.C901_ATTRIBUTE_TYPE,104524,t412a.C412A_ATTRIBUTE_VALUE, 'N')) "LABEL_STAGE"
    FROM t412a_inhouse_trans_attribute t412a, t412_inhouse_transactions t412 
    WHERE t412a.c412_inhouse_trans_id = t412.c412_inhouse_trans_id
    and c412a_void_fl is NULL
    GROUP BY t412a.C412_INHOUSE_TRANS_ID,T412.C412_status_fl;
/