--@"C:\Database\Views\Operations\V205_sales_alloc_inv.sql";
/***********************************************************************
Filename: V205_sales_alloc_inv.vw
Component:View
Package:
Designer: Globus Medical
Developer: Aprasath
Version: 1.0
Description: Creating materialized view to get all the order transaction related allocated quantity. On Commit fast
refresh used to refresh data automatically whenever transaction happend in  t501_order,
t502_item_order
**********************************************************************/
DROP materialized VIEW log ON t501_order;
DROP materialized VIEW log ON t502_item_order;
DROP materialized VIEW V205_sales_alloc_inv;
-- materialized view log is used to create a log of activity on a materialized view�s base table
CREATE materialized VIEW log ON t501_order
NOLOGGING
WITH rowid, sequence (c5040_plant_id, c501_order_id, c901_order_type, c501_status_fl, c501_shipping_date, c501_void_fl,
    c901_ext_country_id,c207_set_id) including NEW VALUES;
CREATE materialized VIEW log ON t502_item_order
NOLOGGING
WITH rowid, sequence (c501_order_id, c205_part_number_id, c502_item_qty, c901_type) including NEW VALUES;

-- COUNT(*) must always be present to guarantee all types of fast refresh. Otherwise, you may be limited to fast refresh after inserts only
-- COUNT(expr) should required when used SUM(expr) used in fast refresh materialized view
 
CREATE materialized VIEW V205_sales_alloc_inv
NOLOGGING
REFRESH FAST ON COMMIT
AS
     SELECT a.c5040_plant_id, a.c901_order_type, a.c901_ext_country_id, b.c205_part_number_id
      , SUM (b.c502_item_qty) c502_item_qty, COUNT ( *) cnt, COUNT (b.c502_item_qty) qtycnt
       FROM t501_order a, t502_item_order b
      WHERE a.c501_order_id                      = b.c501_order_id
        AND a.c501_status_fl                     < 3
        AND a.c501_status_fl                     > 0
        AND a.c501_void_fl                      IS NULL
        AND NVL (a.c901_order_type, - 9999) NOT IN (2524, 2520, 101260) -- To avoid 2524:Price Adjustment, 2520:
        -- Quote and 101260:Acknowledgement Order
        AND a.c501_shipping_date IS NULL
        AND b.c502_item_qty       > 0
        AND a.c207_set_id is null
        AND b.c901_type           = 50300
   GROUP BY a.c5040_plant_id, a.c901_order_type, a.c901_ext_country_id, b.c205_part_number_id ;