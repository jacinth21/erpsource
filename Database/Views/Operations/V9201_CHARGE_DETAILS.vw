CREATE OR REPLACE VIEW V9201_CHARGE_DETAILS
AS
SELECT * FROM 
		(SELECT t9800.c9800_ticket_id tkt_id
		      , t701.c701_distributor_id dist_id 
		      , t701.c701_distributor_name dist_name
		      , t504a.c703_sales_rep_id rep_id
		      , get_rep_name(t504a.c703_sales_rep_id ) rep_name
		      , t525.c525_product_request_id req_id
		      , t504a.c504a_return_dt incident_dt
		      , t504a.c504_consignment_id cn_id
		      , get_set_name(t526.c207_set_id) set_nm
		      , t504.c504a_etch_id etch_id
		      , ih_txn.c205_part_number_id p_s_num
		      , get_partnum_desc(ih_txn.c205_part_number_id) pdesc
		      , ih_txn.missing_qty 
		      , ih_txn.reconciled_qty		      
		      , DECODE(v_in_list.token,'MissingCharges', ih_txn.charge_qty, NULL) ih_charge_qty 
		      , TO_NUMBER(get_part_price(ih_txn.c205_part_number_id, 'L')) list_price
		      , ih_txn.charge ih_charge
		      , t701.c901_distributor_type dist_type
		      , t504a.c526_product_request_detail_id req_dtl_id
		      , get_log_flag(t504a.c526_product_request_detail_id,4000316) reconcmtcnt 
		      , c412_inhouse_trans_id trans_id
		      , t526.c207_set_id set_id     
		   FROM t504a_loaner_transaction t504a 
		      , t526_product_request_detail t526
		      , t9800_jira_ticket t9800       
		      , t525_product_request t525
		      , t504a_consignment_loaner t504
		      , t701_distributor t701
          , v_in_list
          , (SELECT t412.c412_inhouse_trans_id
			          , t413.c205_part_number_id 
			          , t412.c504a_loaner_transaction_id    
			          , SUM(DECODE(t413.c413_status_fl,'Q',t413.c413_item_qty,0)) missing_qty
			          , SUM(DECODE(t413.c413_status_fl,'Q',0,t413.c413_item_qty)) reconciled_qty
			          , SUM(DECODE(t413.c413_status_fl,'Q',t413.c413_item_qty,0)) - 
			            SUM(DECODE(t413.c413_status_fl,'Q',0,t413.c413_item_qty)) charge_qty       
			          , gm_pkg_cm_loaner_jobs.get_missing_part_price(t413.c205_part_number_id) charge			         
		           FROM t412_inhouse_transactions t412
		              , t413_inhouse_trans_items t413 
                  , v_in_list
		          WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id          
		            AND t412.c412_type = 50159
		            AND (     c412_status_fl  = DECODE(v_in_list.token,'MissingCharges',1,c412_status_fl)
		                   OR c412_status_fl  = DECODE(v_in_list.token,'MissingCharges',2,c412_status_fl)
		                   OR c412_status_fl  = DECODE(v_in_list.token,'MissingCharges',3,c412_status_fl)
		                 ) 
		            AND c412_void_fl IS NULL
		            AND t413.c413_void_fl IS NULL
		            AND t413.c205_part_number_id NOT IN (
		                     SELECT c205_part_number_id 
		                       FROM t208_set_details 
		                      WHERE c207_set_id IN (   SELECT c906_rule_value
		                                                FROM t906_rules
		                                               WHERE c906_rule_grp_id = 'CHARGES'
		                                                 AND c906_rule_id     = 'SETS'
		                                                 AND c906_void_fl IS NULL
		                                            )    
		                       AND c208_void_fl IS NULL
		                    )
		       GROUP BY t412.c412_inhouse_trans_id
		              , t413.c205_part_number_id 
		              , t412.c504a_loaner_transaction_id                   
		         ) ih_txn
		   WHERE  ih_txn.c504a_loaner_transaction_id = t504a.c504a_loaner_transaction_id
		     AND t504a.c526_product_request_detail_id = t526.c526_product_request_detail_id 
		     AND t526.c525_product_request_id = t9800.c9800_ref_id
		     AND t525.c525_product_request_id = t526.c525_product_request_id
		     AND t504.c504_consignment_id = t504a.c504_consignment_id
		     AND t504a.c504a_consigned_to_id = t701.c701_distributor_id
		     AND t504a.c901_consigned_to = 50170 
		     AND t504a.c504a_void_fl IS NULL
		     AND t526.c526_void_fl IS NULL
		     AND t9800.c9800_ref_type = 10056 
		     AND t9800.c9800_void_fl IS NULL
		     AND t525.c525_void_fl IS NULL
		     AND t504.c504a_void_fl IS NULL   
		     AND t701.c701_void_fl IS NULL
	  UNION ALL
		 SELECT t9800.c9800_ticket_id tkt_id
		      , t701.c701_distributor_id dist_id 
		      , t701.c701_distributor_name dist_name
		      , t504a.c703_sales_rep_id rep_id
		      , get_rep_name( t504a.c703_sales_rep_id ) rep_name
		      , t525.c525_product_request_id req_id
		      , t504a.c504a_return_dt incident_dt
		      , t504a.c504_consignment_id cn_id
		      , get_set_name(t526.c207_set_id) set_nm
		      , t504.c504a_etch_id etch_id
		      , ih_txn.c207_set_id p_s_num
		      , '' pdesc
		      , ih_txn.missing_qty 
		      , ih_txn.reconciled_qty
		      , DECODE(v_in_list.token,'MissingCharges', ih_txn.charge_qty, NULL) ih_charge_qty 
		      , TO_NUMBER('') list_price
		      , ih_txn.charge ih_charge
		      , t701.c901_distributor_type dist_type
		      , t504a.c526_product_request_detail_id req_dtl_id
		      , get_log_flag(t504a.c526_product_request_detail_id,4000316) reconcmtcnt 
		      , c412_inhouse_trans_id trans_id
		      , t526.c207_set_id set_id     		    
		   FROM t504a_loaner_transaction t504a 
		      , t526_product_request_detail t526
		      , t9800_jira_ticket t9800       
		      , t525_product_request t525
		      , t504a_consignment_loaner t504
		      , t701_distributor t701
          , v_in_list
		      , (SELECT DISTINCT t412.c412_inhouse_trans_id
		              , t526.c207_set_id
		              , t412.c504a_loaner_transaction_id   
		              , 1 missing_qty
		              , 0 reconciled_qty
		              , 1 charge_qty
		              , get_rule_value ('SET_CHARGE', 'CHARGES') charge		              
		           FROM t413_inhouse_trans_items t413
		        	  , t412_inhouse_transactions t412
		        	  , t504a_loaner_transaction t504a
		        	  , t526_product_request_detail t526
                , v_in_list
		          WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
		            AND t413.c205_part_number_id  IN
		                    (
		                         SELECT c205_part_number_id
		                           FROM t208_set_details
		                          WHERE c207_set_id IN
		                            	(
		                                 SELECT c906_rule_value
		                                   FROM t906_rules
		                                  WHERE c906_rule_grp_id = 'CHARGES'
		                                    AND c906_rule_id = 'SETS'
		                                    AND c906_void_fl IS NULL
		                            	)
		                            AND c208_void_fl IS NULL
		                    )       
		             AND ( c412_status_fl  = DECODE(v_in_list.token,'MissingCharges',1,c412_status_fl)
		                   OR c412_status_fl  = DECODE(v_in_list.token,'MissingCharges',2,c412_status_fl)
		                   OR c412_status_fl  = DECODE(v_in_list.token,'MissingCharges',3,c412_status_fl)
		                  ) 
		            AND c412_type = 50159
		            AND t412.c504a_loaner_transaction_id = t504a.c504a_loaner_transaction_id
		            AND t504a.c526_product_request_detail_id = t526.c526_product_request_detail_id
		            AND t413.c413_void_fl IS NULL
		            AND t412.c412_void_fl IS NULL
		            AND t504a.c504a_void_fl IS NULL
		            AND t526.c526_void_fl IS NULL ) ih_txn
		   WHERE  ih_txn.c504a_loaner_transaction_id = t504a.c504a_loaner_transaction_id
		     AND t504a.c526_product_request_detail_id = t526.c526_product_request_detail_id 
		     AND t526.c525_product_request_id = t9800.c9800_ref_id
		     AND t525.c525_product_request_id = t526.c525_product_request_id
		     AND t504.c504_consignment_id = t504a.c504_consignment_id
		     AND t504a.c504a_consigned_to_id = t701.c701_distributor_id
		     AND t504a.c901_consigned_to = 50170 
		     AND t504a.c504a_void_fl IS NULL
		     AND t526.c526_void_fl IS NULL
		     AND t9800.c9800_ref_type = 10056 
		     AND t9800.c9800_void_fl IS NULL
		     AND t525.c525_void_fl IS NULL
		     AND t504.c504a_void_fl IS NULL   
		     AND t701.c701_void_fl IS NULL) ih_txn
		   , (SELECT t9200.c9200_ref_id ch_req_id
		           , TO_CHAR(t9201.c9201_deduction_date,'MM/DD/YYYY') ded_date
		           , TO_CHAR(t9201.c9201_deduction_date,'MM/DD/YYYY') ded_date_excel
		           , TO_CHAR(t9201.c9201_credited_date,'MM/DD/YYYY') crd_date
		           , TO_CHAR(t9201.c9201_credited_date,'MM/DD/YYYY') crd_date_excel
		           , get_user_name(NVL(t9200.c9200_updated_by, t9200.c9200_created_by)) last_updated_by
		           , NVL(TRUNC(t9200.c9200_updated_date)
		           , TRUNC(t9200.c9200_created_date)) last_updated_date
		           , SUM(t9201.c9201_amount) edit_chrg
		           , t9201.c901_recon_comments recon_comments
		           , get_code_name(t9201.c901_recon_comments) recon_comment
		           , MAX(c9201_charge_details_id) charge_id
		           , t9201.c9201_status status_fl
		           , decode(t9201.c9201_status,10,'Open',20,'Approved',25,'Credited',30,'Closed',40,'Cancelled','') status_fl_excel
		           , t9200.c9200_incident_value ch_set_id
		           , t9201.c205_part_number pnum
		           , SUM(t9201.c9201_qty_missing) qty_missing
		        FROM t9200_incident t9200
		           , t9201_charge_details t9201
               , v_in_list
		       WHERE t9200.c9200_incident_id = t9201.c9200_incident_id
		         AND t9200.c901_incident_type = 92069 
		         AND (     c9201_status =  DECODE(v_in_list.token,'MissingCharges', 10, NULL)   
		                OR c9201_status =  DECODE(v_in_list.token,'LoanerCharges', 20 ,10)   
		                OR c9201_status =  DECODE(v_in_list.token,'LoanerCharges', 25 ,10)   
		                OR c9201_status =  DECODE(v_in_list.token,'LoanerCharges', 30 ,10)   
		                OR c9201_status =  DECODE(v_in_list.token,'LoanerCharges', 40 ,10)    
		             ) 
		         AND c9200_ref_id IS NOT NULL
		         AND t9200.c9200_void_fl IS NULL
		         AND t9201.c9201_void_fl IS NULL
		    GROUP BY t9200.c9200_ref_id
		           , t9201.c9201_deduction_date
		           , t9201.c9201_credited_date
		           , t9200.c9200_created_by
		           , TRUNC(t9200.c9200_created_date)
		           , t9200.c9200_updated_by
		           , TRUNC(t9200.c9200_updated_date)
		           , t9201.c901_recon_comments
		           , t9201.c9201_status
		           , t9200.c9200_incident_value
		           , t9201.c205_part_number
		         ) charges
		WHERE NVL(ih_txn.ih_charge_qty,charges.qty_missing) > 0
		  AND ih_txn.req_id = charges.ch_req_id(+)
		  AND ih_txn.p_s_num = NVL(charges.pnum(+),charges.ch_set_id(+))
          AND 1 = (SELECT COUNT(1) from v_in_list where token is not null);
