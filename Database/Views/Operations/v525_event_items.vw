--@"C:\Database\Views\Operations\v525_event_items.vw";
CREATE OR REPLACE VIEW V525_EVENT_ITEMS
AS
    SELECT     OPEN_TXN.pnum Pnum   , get_partnum_desc (OPEN_TXN.pnum) pdesc, OPEN_TXN.ship_ref_id
          , OPEN_TXN.Transid Transid, SUM (OPEN_TXN.reqqty) reqqty          , OPEN_TXN.Status status
          , DECODE (t907.c901_ship_to, '4121', Gm_pkg_cm_shipping_info.get_salesrepaddress (t907.c106_address_id,t907.c907_ship_to_id)
          							 , '4123',Gm_pkg_cm_shipping_info.get_empaddress(t907.c106_address_id,t907.c907_ship_to_id)
          							 , gm_pkg_cm_shipping_info.get_address (t907.c901_ship_to, t907.c907_ship_to_id)) shipaddress 
          , OPEN_TXN.sortorder
        FROM
            (
            --For the request those are in pending approval and open status
            SELECT  t526.c205_part_number_id pnum                
            		, t526.c526_qty_requested reqqty
            		, TO_CHAR (t526.c526_product_request_detail_id) ship_ref_id
            		, TO_CHAR (t526.c526_product_request_detail_id) Transid                                         
            		, DECODE (T526.C526_Void_Fl, 'Y', 'Void',Get_Loaner_Request_Status (T526.C526_Product_Request_Detail_Id)) Status
            		--sortorder: 1-Pending approval(5),2-Open(10),3-Rejected(40),4-Void
            		, DECODE(T526.C526_void_fl,NULL,DECODE(T526.C526_Status_Fl,5,1,10,2,40,3),4) sortorder
                FROM t526_product_request_detail t526, t525_product_request t525, v_in_list vlist
                WHERE T526.C525_Product_Request_Id = T525.C525_Product_Request_Id
                    AND t525.c525_product_request_id = vlist.Token
                    AND t526.c526_status_fl IN ('5', '10','40')
                    AND t526.c207_set_id IS NULL
            UNION ALL
           --For IHLN only
            SELECT    t505.c205_part_number_id pnum
            		, t505.c505_item_qty, t504.c504_consignment_id ship_ref_id
                  	, t504.c504_consignment_id Transid
                  	, DECODE (T504.C504_Void_Fl, 'Y', 'Void', 
                  		DECODE (T504.C504_Status_Fl, '2', 'Pending Control', '3', 'Pending Shipping', '4', 'Shipped Out', NULL)) Status
                  	--sortorder: 10-Pending ctrl(2),11-Pending shipping(3),12-shipout(4),5-Void
                  	, DECODE(t504.c504_void_fl,NULL,DECODE(t504.c504_status_fl,2,10,3,11,4,12),5) sortorder
                FROM T504_Consignment T504, T505_Item_Consignment T505, v_in_list vlist
                WHERE t504.c504_ref_id = vlist.Token
                    AND t504.c504_consignment_id = t505.c504_consignment_id
                    AND t504.c504_type = 9110
            UNION ALL
            --For IHRQ only
            SELECT    t413.c205_part_number_id pnum      
            		, t413.c413_item_qty                
            		, TO_CHAR (get_req_dtl_id (vlist.Token, t413.c205_part_number_id)) ship_ref_id
            		, t412.c412_inhouse_trans_id Transid
            		, DECODE ( T412.C412_Void_Fl, 'Y', 'Void', DECODE ( T412.C412_Status_Fl, '0', 'Open', '3', 'Reviewed', NULL)) Status
            		--sortorder: 6-Open(0),7-Reviewed(3),5-Void
            		, DECODE(T412.C412_Void_Fl,NULL,DECODE(T412.C412_Status_Fl,0,6,3,7),5) sortorder
                FROM t412_inhouse_transactions t412, t413_inhouse_trans_items t413, v_in_list vlist
                WHERE T412.C412_Inhouse_Trans_Id = T413.C412_Inhouse_Trans_Id
                    AND t412.c412_ref_id = vlist.Token
                    AND t412.c412_status_fl IN ('0', '3')
                    AND t413.c413_item_qty > 0
                    AND t412.c412_type = 1006483
            UNION ALL
            --For FGIH only which refers c525
            SELECT    t413.c205_part_number_id pnum      
            		, t413.c413_item_qty                
            		, TO_CHAR (get_req_dtl_id (vlist.Token,t413.c205_part_number_id)) ship_ref_id
            		, t412.c412_inhouse_trans_id Transid
            		, DECODE (T412.C412_Void_Fl, 'Y', 'Void', 
            				DECODE ( T412.C412_Status_Fl, '2', 'Initiated', '3','Pending Verification', NULL)) Status
            		--sortorder: 8-Pending ctrl(2),9-Pending Verify(3),5-Void
            		, DECODE(T412.C412_Void_Fl,NULL,DECODE(T412.C412_Status_Fl,2,8,3,9),5) sortorder
                FROM t412_inhouse_transactions t412, t413_inhouse_trans_items t413, v_in_list vlist
                WHERE T412.C412_Inhouse_Trans_Id = T413.C412_Inhouse_Trans_Id
                    AND t412.c412_ref_id = vlist.Token
                    AND t412.c412_status_fl < 4
                    AND t412.c412_type = 9106
            UNION ALL
           --For FGIH/BLIH which refers IHRQ only
            SELECT    t413.c205_part_number_id pnum      
            		, t413.c413_item_qty                 
            		, TO_CHAR (get_req_dtl_id (vlist.Token, t413.c205_part_number_id)) ship_ref_id
            		, t412a.c412_inhouse_trans_id Transid
            		, DECODE (t412a.c412_type, 9117, 'Void', 
            				DECODE ( t412a.c412_status_fl, '2', 'Initiated', '3','Pending Verification', NULL)) Status
            		--sortorder: 8-Pending ctrl(2),9-Pending Verify(3),5-Void
 					, DECODE(T412a.C412_Void_Fl,NULL,DECODE (t412a.c412_type, 9117, 5,DECODE(T412a.C412_Status_Fl,2,8,3,9)),5) sortorder           				
                FROM t412_inhouse_transactions t412a, t412_inhouse_transactions t412b, t413_inhouse_trans_items t413
                  , v_in_list vlist
                WHERE t412a.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
                    AND T412a.C412_Ref_Id = t412b.C412_Inhouse_Trans_Id
                    AND t412b.c412_ref_id = vlist.Token
                    AND DECODE (t412a.c412_type, 9104, t412a.c412_status_fl, 9106, t412a.c412_status_fl, '-9999') <
                    DECODE ( t412a.c412_type, 9104, 4, 9106, 4, '-9998')
                    AND t412a.c412_void_fl IS NULL
                    AND t412a.c412_type IN (9104, 9106, 9117)
            ) OPEN_TXN, T907_Shipping_Info T907
        WHERE OPEN_TXN.ship_ref_id = t907.c907_ref_id
        GROUP BY Open_Txn.Pnum, Open_Txn.Status  , Open_Txn.ship_ref_id
          , OPEN_TXN.Transid  , T907.C901_Ship_To, T907.C106_Address_Id
          , T907.C907_Ship_To_Id, OPEN_TXN.sortorder
        ORDER BY OPEN_TXN.sortorder;
/