CREATE OR REPLACE VIEW v4081a_dhr_bulk_attribute
AS
select max(decode(c4081a_attribute_value,104520,'Y', 'N')) "FORM_42"
        , max(decode(c4081a_attribute_value,104521,'Y', 'N')) "LABELS_PRINTED"
        , max(decode(c4081a_attribute_value,104522,'Y', 'N')) "QC_VERIFIED"
        , max(decode(c4081a_attribute_value,104523,'Y', 'N')) "LABEL_PRE_STAGE"
        , max(decode(c4081a_attribute_value,104524,'Y', 'N')) "LABEL_STAGE"
        , c4081_dhr_bulk_id
     from t4081a_dhr_bulk_attribute t4081a
       where t4081a.c4081a_void_fl is NULL
     GROUP BY c4081_dhr_bulk_id;
 /