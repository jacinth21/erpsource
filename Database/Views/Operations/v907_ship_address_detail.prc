/* Formatted on 2009/11/03 17:42 (Formatter Plus v4.8.0) */
--@"C:\Database\Views\Operations\v907_ship_address_detail.prc";
CREATE OR REPLACE VIEW v907_ship_address_detail (key_id
											   , shipto
											   , shiptoid
											   , addressid
											   , src
											   , company
											   , add1
											   , add2
											   , city
											   , state
											   , zip
											   , country
											   , ph_no
											   , email
											   , dmode_id
											   , dmode_name
											   , dcarrier
											   , returnfl
											   , delivery_email
											   , exception_email
											   , retattn
											   , retcompanynm
											   , retadd
											   , retcity
											   , retstate
											   , retzip
											   , retphone
											   , custref
											   , pkgtype
											   , attn
											   , satmode
												)
AS
	SELECT t907.c907_ref_id key_id, t907.c901_ship_to shipto, t907.c907_ship_to_id shiptoid
		 , t907.c106_address_id addressid, t907.c901_source src
		 , UPPER (DECODE (t907.c901_ship_to
						, 4120, t701.c701_distributor_name
						, 4122, t704.c704_account_nm
						, 4121, ''
						, 4123, get_user_name (t101.c101_user_id)
						 )
				 ) company
		 , UPPER (DECODE (t907.c901_ship_to
						, 4120, t701.c701_ship_add1
						, 4122, t704.c704_ship_add1
						, 4121, t106.c106_add1
						, 4123, t106.c106_add1
						, ''
						 )
				 ) add1
		 , UPPER (DECODE (t907.c901_ship_to
						, 4120, t701.c701_ship_add2
						, 4122, t704.c704_ship_add2
						, 4121, t106.c106_add2
						, 4123, t106.c106_add2
						, ''
						 )
				 ) add2
		 , UPPER (DECODE (t907.c901_ship_to
						, 4120, t701.c701_ship_city
						, 4122, t704.c704_ship_city
						, 4121, t106.c106_city
						, 4123, t106.c106_city
						, ''
						 )
				 ) city
		 , get_code_name_alt (DECODE (t907.c901_ship_to
									, 4120, t701.c701_ship_state
									, 4122, t704.c704_ship_state
									, 4121, t106.c901_state
									, 4123, t106.c901_state
									, ''
									 )
							 ) state
		 , DECODE (t907.c901_ship_to
				 , 4120, t701.c701_ship_zip_code
				 , 4122, t704.c704_ship_zip_code
				 , 4121, t106.c106_zip_code
				 , 4123, t106.c106_zip_code
				 , ''
				  ) zip
		 , get_code_name_alt (DECODE (t907.c901_ship_to
									, 4120, t701.c701_ship_country
									, 4122, t704.c704_ship_country
									, 4121, t106.c901_country
									, 4123, t106.c901_country
									, ''
									 )
							 ) country
		 , DECODE (t907.c901_ship_to
				 , 4120, t701.c701_phone_number
				 , 4122, t704.c704_phone
				 , 4123, t101.c101_ph_extn
				 , 4121, gm_pkg_cm_contact.get_contact_value (t703.c101_party_id, '90450')
				 , ''
				  ) ph_no
		 , NVL (TRIM (DECODE (t907.c901_ship_to
							, 4120, t701.c701_email_id
							, 4122, t704.c704_email_id
							, 4123, t101.c101_email_id
							, 4121, gm_pkg_cm_contact.get_contact_value (t703.c101_party_id, '90452')
							, ''
							 )
					 )
			  , default_email
			   ) email
		 , t907.c901_delivery_mode dmode_id, get_rule_value (t907.c901_delivery_mode, 'FEDEX') dmode_name
		 , get_code_name (t907.c901_delivery_carrier) dcarrier, DECODE (t907.c901_source, 50182, 'Y', 'N') returnfl
		 , delivery_email, exception_email, DECODE (t907.c901_source, 50182, retattn)
		 , DECODE (t907.c901_source, 50182, retcompanynm) retcompanynm, DECODE (t907.c901_source
																			  , 50182, retadd
																			   ) retadd
		 , DECODE (t907.c901_source, 50182, retcity) retcity, DECODE (t907.c901_source, 50182, retstate) retstate
		 , DECODE (t907.c901_source, 50182, retzip) retzip, DECODE (t907.c901_source, 50182, retphone) retphone
		 , DECODE (t907.c901_source, 50180, 'ORDER', 50181, 'CONSIGNMENTS', 'LOANERS') custref
		 , DECODE (t907.c901_source
				 , 50180, 3
				 , 50181, DECODE (gm_pkg_op_consignment.get_consign_type (t907.c907_ref_id), 'ITEM', 3, 'SET', 1)
				 , 50182, 1
				 , 50183, 1
				  ) pkgtype
		 , UPPER (gm_pkg_cm_contact.get_contact_name (t907.c907_ship_to_id, t907.c901_ship_to, t907.c106_address_id)
				 ) attn
		 , DECODE (t907.c901_delivery_mode, 5006, 'Y', 'N') satmode
	  FROM t907_shipping_info t907
		 , t701_distributor t701
		 , t704_account t704
		 , t106_address t106
		 , t703_sales_rep t703
		 , t101_user t101
		 , (SELECT get_rule_value ('DEFEMAIL', 'FEDEX') default_email
			  FROM DUAL) c
		 , (SELECT get_rule_value ('DELIVERY', 'FEDEX') delivery_email
			  FROM DUAL) d
		 , (SELECT get_rule_value ('EXCEPTION', 'FEDEX') exception_email
			  FROM DUAL) e
		 , (SELECT get_rule_value ('ATTN', 'FEDEX') retattn, get_rule_value ('COMPANY', 'FEDEX') retcompanynm
				 , get_rule_value ('ADD', 'FEDEX') retadd, get_rule_value ('CITY', 'FEDEX') retcity
				 , get_rule_value ('STATE', 'FEDEX') retstate, get_rule_value ('ZIP', 'FEDEX') retzip
				 , get_rule_value ('PHONE', 'FEDEX') retphone
			  FROM DUAL) f
	 WHERE t907.c106_address_id = t106.c106_address_id(+)
	   AND t907.c907_ship_to_id = t701.c701_distributor_id(+)
	   AND t907.c907_ship_to_id = t704.c704_account_id(+)
	   AND t907.c907_ship_to_id = t703.c703_sales_rep_id(+)
	   AND t907.c907_ship_to_id = t101.c101_user_id(+)
	   AND t907.c907_status_fl = 30
	   AND t907.c907_active_fl IS NULL
	   AND t907.c907_void_fl IS NULL;
 /