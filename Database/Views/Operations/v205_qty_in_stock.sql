/* Formatted on 2011/02/23 13:39 (Formatter Plus v4.8.0) */
--@"C:\Database\Views\Operations\v205_qty_in_stock.sql";
CREATE OR REPLACE VIEW v205_qty_in_stock
AS
	SELECT	 t205.c205_part_number_id c205_part_number_id, t205.c205_part_num_desc c205_part_num_desc
		, c205_rev_num c205_drawrev, t205.c202_project_id c202_project_id
		/************** Quarantine Transaction ********************/
		, (NVL(qnavail.qn_avail_qty,0) - NVL(v205_qty_alloc.c205_quaralloc,0)) c205_inquaran
		, NVL(v205_qty_alloc.c205_quaralloc,0) c205_quaralloc
		, NVL(v205_other_alloc.quar_alloc_to_inv,0) c205_quar_alloc_to_inv
		, NVL(v205_other_alloc.quar_alloc_to_scrap,0) c205_quar_alloc_to_scrap
		/*************** Bulk Transaction ***************/
		, (NVL(bulkavail.bulk_avail_qty,0)+ NVL (intranswipset.alloc_qty,0)) - ( NVL(v205_qty_alloc.c205_bulkalloc,0)
				+ NVL (in_wip_set, 0) + NVL (tbe_alloc, 0) ) c205_inbulk
		, NVL(v205_qty_alloc.c205_bulkalloc,0) c205_bulkalloc
		, (NVL (in_wip_set, 0)- NVL (intranswipset.alloc_qty,0)) c205_in_wip_set, NVL(v205_other_alloc.bulk_alloc_to_inv,0) c205_bulk_alloc_to_inv
		, NVL (tbe_alloc, 0) c205_tbe_alloc
		/*************** Repacking ***************/
		, NVL (repackageavail. repackage_avail_qty, 0) - (NVL(v205_qty_alloc.c205_repackalloc,0)
								  ) c205_repackavail
		, NVL(v205_qty_alloc.c205_repackalloc,0) c205_repackalloc
		/*************** Returns Hold ***************/
		, NVL(returnsavail.returns_avail_qty, 0) inreturns
		, (NVL(returnsavail.returns_avail_qty, 0) - (NVL(v205_qty_alloc.c205_returnalloc,0))) c205_returnavail
		, NVL(v205_qty_alloc.c205_returnalloc,0) c205_returnalloc
		/*************** Raw Material ***************/
		, NVL (rm_avail, 0) - NVL (v205_qty_alloc.rm_alloc_qty, 0) rmqty
		, NVL (v205_qty_alloc.rm_alloc_qty, 0) rm_alloc_qty
		/*************** Shelf ***************/
		, NVL(fgavail.fg_avail_qty, 0) - NVL(v205_qty_alloc.c205_shelfalloc,0) c205_inshelf
		, NVL(v205_qty_alloc.c205_shelfalloc,0) c205_shelfalloc
		, NVL(v205_other_alloc.bulk_alloc_to_set,0) c205_bulk_alloc_to_set, NVL(v205_other_alloc.quar_alloc_self_quar,0) c205_quar_alloc_self_quar
		, NVL(v205_other_alloc.sales_allocated,0) sales_allocated
		, NVL(v205_other_alloc.cons_alloc,0) c205_cons_alloc
		, NVL(v205_other_alloc.inv_alloc_to_pack,0) c205_inv_alloc_to_pack
		/*************** DHR ***************/
		, get_partnumber_po_pend_qty (t205.c205_part_number_id) c205_popend
		, get_partnumber_po_wip_qty (t205.c205_part_number_id) c205_powip, mfg_tbr, mfg_release, dhr_wip
		, dhr_pen_qc_ins, dhr_pen_qc_rel, dhr_qc_appr
		, gm_pkg_op_ld_inventory.get_pnum_pdhr_wip_qty (t205.c205_part_number_id) c205_dhrqty
		, NVL(v205_qty_alloc.c205_dhralloc,0) c205_dhralloc
		/***************SHIPPING ***************/
		, NVL(v205_qty_alloc.c205_shipalloc,0) c205_shipalloc
		/*************** Built Set Changes ************** */
		, build_set_qty
		/* QTY In Stock mainly used for order planning */
		, (NVL(fgavail.fg_avail_qty, 0) + NVL (repackageavail. repackage_avail_qty, 0) + NVL (rm_avail, 0) 
				+ NVL(bulkavail.bulk_avail_qty,0) + NVL(v205_other_alloc.other_avail,0) ) 
			   - (NVL(v205_other_alloc.other_alloc,0)
				  + get_qty_in_transaction_rm (t205.c205_part_number_id)				  
				 ) c205_instock,(nvl(boqty,0)) c205_boqty, NVL (inhouse.loanqty, 0) C205_Inhouse_Loanqty  
       /***************PI 2011  ***************/
		,NVL(bulkavail.bulk_avail_qty,0) c205_bulkall
         ,NVL(fgavail.fg_avail_qty,0) c205_stockall
		,NVL(repackageavail. repackage_avail_qty,0)  c205_packall
		,Nvl(qnavail.qn_avail_qty,0) C205_Quarall
        /***************IH Inventory  ***************/
      	, NVL (ihavail.ih_avail_qty, 0) - NVL (v205_qty_alloc.c205_ih_alloc_qty,0)  c205_ih_avail_qty
		, NVL (v205_qty_alloc.c205_ih_alloc_qty,0)  c205_ih_alloc_qty
		, (nvl(inhouse.loanqty,0))  c205_ih_set_qty
		, (nvl(ihitem.ih_item_qty,0))  c205_ih_item_qty
		, (nvl(loan.loanqty,0)) c205_loanqty
		/***************RW Inventory  ***************/
      	, NVL (rw_avail.rw_avail_qty, 0) - NVL (v205_qty_alloc.c205_rw_alloc_qty,0) c205_rw_avail_qty
		,  NVL (v205_qty_alloc.c205_rw_alloc_qty,0) c205_rw_alloc_qty
		/***************In Transit Inventory  ***************/
		, NVL (intransavail.intransavailqty,0) c205_intrans_avail_qty
		, NVL (intransalloc.intransallocqty,0) c205_intrans_alloc_qty
		FROM t205_part_number t205
		   , my_temp_part_list mylist
		   ,(SELECT t205c.c205_part_number_id, NVL (c205_available_qty, 0) fg_avail_qty
		       FROM t205c_part_qty t205c, my_temp_part_list mylist
		      WHERE C901_Transaction_Type             = 90800 --90800 - Inventory Qty
		        AND t205c.c205_part_number_id         = mylist.c205_part_number_id
		        AND t205c.c5040_plant_id			  = NVL(sys_context('CLIENTCONTEXT', 'PLANT_ID') ,get_rule_value('DEFAULT_PLANT','ONEPORTAL') ) 
		        AND get_v205_query_exe_status (70017) = 0
			)fgavail,
			(SELECT t205c.c205_part_number_id, NVL (c205_available_qty, 0) bulk_avail_qty
			       FROM t205c_part_qty t205c, my_temp_part_list mylist
			      WHERE c901_transaction_type             = 90814 -- 90814 - Bulk Qty
			        AND t205c.c205_part_number_id         = mylist.c205_part_number_id
			        AND t205c.c5040_plant_id			  = NVL(sys_context('CLIENTCONTEXT', 'PLANT_ID') ,get_rule_value('DEFAULT_PLANT','ONEPORTAL') ) 
			        AND get_v205_query_exe_status (70018) = 0
			)bulkavail, 
			(SELECT t205c.c205_part_number_id, NVL (c205_available_qty, 0) qn_avail_qty
			       FROM t205c_part_qty t205c, my_temp_part_list mylist
			      WHERE c901_transaction_type             = 90813 --90813 - Quarantine Inventory
			        AND t205c.c205_part_number_id         = mylist.c205_part_number_id
			        AND t205c.c5040_plant_id			  = NVL(sys_context('CLIENTCONTEXT', 'PLANT_ID') ,get_rule_value('DEFAULT_PLANT','ONEPORTAL') ) 
			        AND get_v205_query_exe_status (70019) = 0
			)qnavail,
			(SELECT t205c.c205_part_number_id, NVL (c205_available_qty, 0) returns_avail_qty
			       FROM t205c_part_qty t205c, my_temp_part_list mylist
			      WHERE c901_transaction_type             = 90812 --90812 - Returns Hold Inventory
			        AND t205c.c205_part_number_id         = mylist.c205_part_number_id
			        AND t205c.c5040_plant_id			  = NVL(sys_context('CLIENTCONTEXT', 'PLANT_ID') ,get_rule_value('DEFAULT_PLANT','ONEPORTAL') ) 
			        AND get_v205_query_exe_status (70020) = 0
			)returnsavail, 
			(SELECT t205c.c205_part_number_id, NVL (c205_available_qty, 0) repackage_avail_qty
			       FROM t205c_part_qty t205c, my_temp_part_list mylist
			      WHERE c901_transaction_type             = 90815 --90815 - Packaging Inventory
			        AND t205c.c205_part_number_id         = mylist.c205_part_number_id
			        AND t205c.c5040_plant_id			  = NVL(sys_context('CLIENTCONTEXT', 'PLANT_ID') ,get_rule_value('DEFAULT_PLANT','ONEPORTAL') ) 
			        AND get_v205_query_exe_status (70021) = 0
			)repackageavail
		   , (SELECT t205.c205_part_number_id, SUM (DECODE (v205c.c901_transaction_type, '60001', v205c.c205_allocated_qty, 0))
				    c205_shelfalloc, SUM (DECODE (v205c.c901_transaction_type, '60002', v205c.c205_allocated_qty, 0)) rm_alloc_qty, SUM (
				    DECODE (v205c.c901_transaction_type, '60003', v205c.c205_allocated_qty, 0)) c205_bulkalloc, SUM (DECODE (
				    v205c.c901_transaction_type, '60005', v205c.c205_allocated_qty, 0)) c205_repackalloc, SUM (DECODE (
				    v205c.c901_transaction_type, '60006', v205c.c205_allocated_qty, 0)) c205_quaralloc, SUM (DECODE (
				    v205c.c901_transaction_type, '60007', v205c.c205_allocated_qty, 0)) c205_returnalloc, SUM (DECODE (
				    v205c.c901_transaction_type, '60010', v205c.c205_allocated_qty, 0)) c205_dhralloc, SUM (DECODE (
				    v205c.c901_transaction_type, '60011', v205c.c205_allocated_qty, 0)) c205_shipalloc, SUM (DECODE (
				    v205c.c901_transaction_type, '60012', v205c.c205_allocated_qty, 0)) c205_ih_alloc_qty, SUM (DECODE (
				    v205c.c901_transaction_type, '60014', v205c.c205_allocated_qty, 0)) C205_RW_ALLOC_QTY
				   FROM t205_part_number t205, v205c_part_allocated_qty v205c, my_temp_part_list mylist
				  WHERE t205.c205_part_number_id = mylist.c205_part_number_id
				    AND v205c.c5040_plant_id     = NVL(sys_context('CLIENTCONTEXT', 'PLANT_ID') ,get_rule_value('DEFAULT_PLANT','ONEPORTAL') ) 
				    AND t205.c205_part_number_id = v205c.c205_part_number_id (+)
				GROUP BY t205.c205_part_number_id ) v205_qty_alloc		  
		   ,v205_other_alloc_qty v205_other_alloc
		   ,(SELECT   a.c205_part_number_id
		   			 , SUM (DECODE (a.c505_control_number, 'TBE', 0, a.c505_item_qty))  in_wip_set					 
		   			 , SUM (DECODE (a.c505_control_number, 'TBE', a.c505_item_qty, 0)) tbe_alloc
				  FROM t505_item_consignment a, t504_consignment b, my_temp_part_list mylist
				 WHERE a.c504_consignment_id = b.c504_consignment_id
				   AND TRIM (a.c505_control_number) IS NOT NULL
				   AND b.c504_status_fl < '3'
				   AND b.c207_set_id IS NOT NULL
				   AND b.c504_verify_fl = '0'
				   AND b.c504_void_fl IS NULL
				   AND a.c205_part_number_id = mylist.c205_part_number_id
				   AND b.c5040_plant_id      = NVL(sys_context('CLIENTCONTEXT', 'PLANT_ID') ,get_rule_value('DEFAULT_PLANT','ONEPORTAL') ) 
				   AND get_v205_query_exe_status(70004) = 0
			  GROUP BY a.c205_part_number_id) wip		   
	,		 (SELECT t205c.c205_part_number_id, NVL (c205_available_qty, 0) rm_avail
				FROM t205c_part_qty t205c, my_temp_part_list mylist
			   WHERE c901_transaction_type = 90802 
			     AND t205c.c205_part_number_id = mylist.c205_part_number_id
			     AND t205c.c5040_plant_id      = NVL(sys_context('CLIENTCONTEXT', 'PLANT_ID') ,get_rule_value('DEFAULT_PLANT','ONEPORTAL') ) 
			     AND get_v205_query_exe_status(70005) = 0
			  ) rmavail		   
		   , (SELECT   t304.c205_part_number_id, NVL (SUM (t304.c304_item_qty), 0) mfg_total_alloc
					 , SUM (DECODE (t303.c303_status_fl, 20, t304.c304_item_qty, 30, t304.c304_item_qty, 0)) mfg_tbr
					 , SUM (DECODE (t303.c303_status_fl, 40, t304.c304_item_qty, 0)) mfg_release
				  FROM t303_material_request t303
					 , t304_material_request_item t304
					 , t408_dhr t408
					 , my_temp_part_list mylist
				 WHERE t303.c303_material_request_id = t304.c303_material_request_id
				   AND t408.c408_dhr_id = t303.c408_dhr_id
				   AND t408.c408_status_fl = 0
				   AND t304.c205_part_number_id = mylist.c205_part_number_id
				   AND t408.c408_void_fl IS NULL
				   AND t303.c303_void_fl IS NULL
			       AND t304.c304_delete_fl IS NULL
			       AND get_v205_query_exe_status(70007) = 0 
			       AND t408.c5040_plant_id = NVL(sys_context('CLIENTCONTEXT', 'PLANT_ID') ,get_rule_value('DEFAULT_PLANT','ONEPORTAL') ) 
			  GROUP BY t304.c205_part_number_id) mfgrun
		   ,(SELECT   t408.c205_part_number_id, SUM (NVL (t408.c408_shipped_qty, 0) - NVL (t408.c408_qty_rejected, 0)+ NVL (t409.c409_closeout_qty, 0)) powip
			, SUM (DECODE (t408.c408_status_fl, 0, (NVL (t408.c408_qty_received, 0) ), 0)) dhr_wip
			, SUM (DECODE (t408.c408_status_fl, 1, (  NVL (t408.c408_shipped_qty, 0) - NVL (t408.c408_qty_rejected, 0) + NVL (t409.c409_closeout_qty, 0)), 0 )) dhr_pen_qc_ins
			, SUM (DECODE (t408.c408_status_fl, 2, (  NVL (t408.c408_shipped_qty, 0) - NVL (t408.c408_qty_rejected, 0) + NVL (t409.c409_closeout_qty, 0)), 0)) dhr_pen_qc_rel
			, SUM (DECODE (t408.c408_status_fl, 3, (  NVL (t408.c408_shipped_qty, 0) - NVL (t408.c408_qty_rejected, 0) + NVL (t409.c409_closeout_qty, 0)), 0)) dhr_qc_appr
			 FROM t408_dhr t408, t409_ncmr t409 ,my_temp_part_list mylist
			 WHERE t408.c408_dhr_id = t409.c408_dhr_id(+) AND t408.c408_status_fl < 4                                                                                     
			 AND t408.c408_void_fl IS NULL
			 AND t409.c409_void_fl(+) IS NULL
			AND t408.c205_part_number_id = mylist.C205_PART_NUMBER_ID
			AND t408.c5040_plant_id = NVL(sys_context('CLIENTCONTEXT', 'PLANT_ID') ,get_rule_value('DEFAULT_PLANT','ONEPORTAL') ) 
			AND t408.c5040_plant_id = t409.c5040_plant_id(+)
			AND get_v205_query_exe_status(70008) = 0   
			 GROUP BY t408.c205_part_number_id) dhr
		   , (SELECT   b.c205_part_number_id, SUM (b.c505_item_qty) build_set_qty
				  FROM t504_consignment a, t505_item_consignment b, my_temp_part_list mylist
				 WHERE a.c504_consignment_id = b.c504_consignment_id
				   AND a.c504_status_fl IN ('1.20', '2', '2.20', '3')			-- built set should include 'pending putaway 1.20' and 'pending pick 2.20'
				   AND a.c207_set_id IS NOT NULL
				   AND a.c504_verify_fl = '1'
				   AND a.c504_ship_date IS NULL
				   AND a.c504_void_fl IS NULL
				   AND b.c205_part_number_id = mylist.c205_part_number_id
				   AND TRIM (b.c505_control_number) IS NOT NULL
				   AND get_v205_query_exe_status(70009) = 0
				   AND a.c5040_plant_id = NVL(sys_context('CLIENTCONTEXT', 'PLANT_ID') ,get_rule_value('DEFAULT_PLANT','ONEPORTAL') ) 
			  Group By B.C205_Part_Number_Id) Consbuilt 
                   /***************IH Inventory  ***************/
            ,(SELECT t205c.c205_part_number_id, NVL (c205_available_qty, 0) ih_avail_qty
				From T205c_Part_Qty T205c, My_Temp_Part_List Mylist
			   Where C901_Transaction_Type in (90816)
                 And T205c.C205_Part_Number_Id = Mylist.C205_Part_Number_Id
                 AND get_v205_query_exe_status(70010) = 0
                 AND T205c.c5040_plant_id = NVL(sys_context('CLIENTCONTEXT', 'PLANT_ID') ,get_rule_value('DEFAULT_PLANT','ONEPORTAL') ) 
               ) Ihavail
		   ,
			(  SELECT c205_part_number_id pnum, SUM(qty) ih_item_qty 
				FROM 
				( 
					SELECT t507.c205_part_number_id , SUM (t507.c507_item_qty) qty
					  FROM t506_returns t506, t507_returns_item t507, My_Temp_Part_List Mylist
					WHERE t507.c205_part_number_id = mylist.c205_part_number_id
					  AND t507.c506_rma_id         = t506.c506_rma_id
					  AND ( ( t506.c506_status_fl    = '0' AND t507.c507_status_fl = 'Q') 
                         OR (  t506.c506_status_fl    = '1' AND t507.c507_status_fl = 'R') )
					  AND t506.c901_ref_type       = 9110
					  AND t506.c506_void_fl	IS NULL
					  AND get_v205_query_exe_status(70012) = 0
					  AND t506.c5040_plant_id = NVL(sys_context('CLIENTCONTEXT', 'PLANT_ID') ,get_rule_value('DEFAULT_PLANT','ONEPORTAL') ) 
					  GROUP BY t507.c205_part_number_id
					UNION ALL 
					SELECT   t413.c205_part_number_id,
						 SUM (DECODE (t413.c413_status_fl,'Q', c413_item_qty, c413_item_qty * -1)) qty
					FROM t412_inhouse_transactions t412,
						 t413_inhouse_trans_items t413,
						 my_temp_part_list mylist
					WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
					 AND t412.c412_verified_date IS NULL
					 AND t412.c412_void_fl IS NULL
					 AND t412.c412_status_fl NOT IN (4)
					 AND t412.c412_type IN (9112)
					 AND t413.c205_part_number_id = mylist.c205_part_number_id
					 AND get_v205_query_exe_status(70012) = 0
					 AND t412.c5040_plant_id = NVL(sys_context('CLIENTCONTEXT', 'PLANT_ID') ,get_rule_value('DEFAULT_PLANT','ONEPORTAL') ) 
					GROUP BY t413.c205_part_number_id
				) GROUP BY c205_part_number_id
			 ) ihitem,
			  /*********************Loaner Inventory***************************/
			  ( SELECT pl_consign.c205_part_number_id, loanqty - NVL (miss_qty, 0) loanqty
				  FROM (SELECT SUM (t505.C505_ITEM_QTY) LOANQTY, t505.C205_PART_NUMBER_ID
							   FROM t504_consignment t504, t504a_consignment_loaner t504a, t505_item_consignment t505
							  , t205_part_number t205, My_Temp_Part_List Mylist
							  WHERE t504.c504_status_fl              = '4'
							    AND t504.c504_verify_fl              = '1'
							    AND t504.c207_set_id                IS NOT NULL
							    AND t504.c504_type                   = 4127
							    AND t504.c504_consignment_id         = t504a.c504_consignment_id (+)
							    AND t504.C504_CONSIGNMENT_ID         = t505.C504_CONSIGNMENT_ID
							    AND trim (t505.C505_CONTROL_NUMBER) IS NOT NULL
							    AND t504.C504_VOID_FL               IS NULL
							    AND t505.C505_VOID_FL               IS NULL
							    AND T504A.c504a_status_fl           != '60'
							    AND t205.c205_part_number_id         = t505.c205_part_number_id
							    AND t505.c205_part_number_id         = mylist.c205_part_number_id      
							    AND get_v205_query_exe_status(70013) = 0
							    AND t504a.c5040_plant_id = NVL(sys_context('CLIENTCONTEXT', 'PLANT_ID') ,get_rule_value('DEFAULT_PLANT','ONEPORTAL') )
							    AND t504.c5040_plant_id  = t504a.c5040_plant_id 
							GROUP BY t505.C205_PART_NUMBER_ID) pl_consign,
			                (SELECT   t413.c205_part_number_id,
												 SUM (t413.c413_item_qty) miss_qty
											FROM t412_inhouse_transactions t412,
												 t413_inhouse_trans_items t413,
												 t504a_consignment_loaner t504a,
												 t504_consignment t504,  --PC#206 t504 filter is added to remove voided loaners
												 my_temp_part_list mylist
										   WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
											 AND t412.c412_void_fl IS NULL
											 AND t413.c413_void_fl IS NULL
											 AND t413.c413_status_fl = 'Q'
											 AND t412.c412_type = 50159
											 AND t412.c412_ref_id = t504a.c504_consignment_id
                                             AND T504A.c504a_status_fl  != '60'
											 AND t413.c205_part_number_id = mylist.c205_part_number_id
											 AND t504.c504_status_fl = '4'  --PC#206 t504 filter is added to remove voided loaners
					                         AND t504.c504_verify_fl = '1'
					                         AND t504.c207_set_id IS NOT NULL
					                         AND t504.c504_type = 4127
					                         AND t504.c504_consignment_id = t504a.c504_consignment_id 
					                         AND t504.c504_void_fl IS NULL
											 AND get_v205_query_exe_status(70013) = 0
											 AND t412.c5040_plant_id = NVL(sys_context('CLIENTCONTEXT', 'PLANT_ID') ,get_rule_value('DEFAULT_PLANT','ONEPORTAL') ) 
										GROUP BY t413.c205_part_number_id) pl_miss
					WHERE pl_consign.c205_part_number_id = pl_miss.c205_part_number_id(+) ) loan,
						( SELECT t413.c205_part_number_id,SUM (t413.c413_item_qty) BOQTY
                                    FROM T412_INHOUSE_TRANSACTIONS t412 ,
                                      t413_inhouse_trans_items t413,
                                      t504a_loaner_transaction t504a,my_temp_part_list mylist
                                    WHERE t412.c412_inhouse_trans_id     = t413.c412_inhouse_trans_id
                                    AND t412.c504a_loaner_transaction_id = t504a.c504a_loaner_transaction_id (+)
                                    AND C412_STATUS_FL                   = '0'
                                    AND t412.c412_type                   = 100062
                                    AND t413.C413_VOID_FL               IS NULL
                                    AND C412_VOID_FL                    IS NULL
                                     AND t413.c205_part_number_id = mylist.c205_part_number_id
                                     AND t412.c5040_plant_id = NVL(sys_context('CLIENTCONTEXT', 'PLANT_ID') ,get_rule_value('DEFAULT_PLANT','ONEPORTAL') ) 
                                    GROUP BY t413.c205_part_number_id ) boqty,--Inhouse Loaner Qty
			      /************************ In-House Inventory******************************/
			      ( SELECT consign.c205_part_number_id, loanqty - NVL (miss_qty, 0) loanqty
					  FROM (SELECT   SUM (t505.c505_item_qty) loanqty, t505.c205_part_number_id
								FROM t504_consignment t504,
									 t504a_consignment_loaner t504a,
									 t505_item_consignment t505,
									 my_temp_part_list mylist
							   WHERE t504.c504_status_fl = '4'
								 AND t504.c504_verify_fl = '1'
								 AND t504.c207_set_id IS NOT NULL
								 AND t504.c504_type = 4119
								 AND t504.c504_consignment_id = t504a.c504_consignment_id(+)
								 AND t504.c504_consignment_id = t505.c504_consignment_id
								 AND TRIM (t505.c505_control_number) IS NOT NULL
								 AND t504.c504_void_fl IS NULL
								 AND t505.c505_void_fl IS NULL
								 AND t504a.c504a_status_fl != '60'
								 AND t505.c205_part_number_id = mylist.c205_part_number_id
								 AND get_v205_query_exe_status(70014) = 0
								 AND t504a.c5040_plant_id = NVL(sys_context('CLIENTCONTEXT', 'PLANT_ID') ,get_rule_value('DEFAULT_PLANT','ONEPORTAL') )
							     AND t504.c5040_plant_id  = t504a.c5040_plant_id  
							GROUP BY t505.c205_part_number_id) consign,							
						   (SELECT   t413.c205_part_number_id,
									 SUM (t413.c413_item_qty) miss_qty
								FROM t412_inhouse_transactions t412,
									 t413_inhouse_trans_items t413,
									 t504a_consignment_loaner t504a,
									 t504_consignment t504,
									 my_temp_part_list mylist
							   WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
								 AND t412.c412_void_fl IS NULL
								 AND t413.c413_void_fl IS NULL
								 AND t413.c413_status_fl = 'Q'
								 AND t412.c412_type in (1006571)  --remove 50151 from this condition
								 AND t412.c412_ref_id = t504a.c504_consignment_id
                                 AND T504A.c504a_status_fl  != '60'
								 AND t413.c205_part_number_id = mylist.c205_part_number_id
								 AND t504.c504_status_fl = '4'
								 AND t504.c504_verify_fl = '1'
                                 AND t504.c207_set_id IS NOT NULL
                                 AND t504.c504_type = 4119
                                 AND t504.c504_consignment_id = t504a.c504_consignment_id
                                 AND t504.c504_void_fl IS NULL
								 AND get_v205_query_exe_status(70014) = 0
								 AND t412.c5040_plant_id = NVL(sys_context('CLIENTCONTEXT', 'PLANT_ID') ,get_rule_value('DEFAULT_PLANT','ONEPORTAL') ) 
							GROUP BY t413.c205_part_number_id) miss
					 WHERE consign.c205_part_number_id = miss.c205_part_number_id(+)  ) inhouse,
				/************************ Restricted Warehouse Inventory******************************/
				(
				SELECT     t205c.c205_part_number_id, NVL (c205_available_qty, 0) rw_avail_qty
				    FROM T205c_Part_Qty T205c, my_temp_part_list mylist
				    WHERE C901_Transaction_Type IN (56001)
				      AND T205c.C205_Part_Number_Id = Mylist.C205_Part_Number_Id
				      AND get_v205_query_exe_status(70015) = 0
				      AND T205c.c5040_plant_id = NVL(sys_context('CLIENTCONTEXT', 'PLANT_ID') ,get_rule_value('DEFAULT_PLANT','ONEPORTAL') ) 
				)rw_avail,								 
             /**************************In-Transit Inventroy - (PMT-18235)******************************************/
			   (
    		   	SELECT t505.c205_part_number_id , SUM (t505.c505_item_qty) alloc_qty
				  FROM t504_consignment t504, t505_item_consignment t505 , t504e_consignment_in_trans t504e , my_temp_part_list mylist
				 WHERE t504.c504_consignment_id = t505.c504_consignment_id				     
				   AND t504.c504_reprocess_id   = t504e.c504_consignment_id
				   AND t505.c205_part_number_id = mylist.c205_part_number_id
				   AND t504.c504_status_fl < '4'
				   AND t504.c504_verify_fl = '0'
				   AND t504.c504_void_fl IS NULL
				   AND t505.c505_void_fl IS NULL
				   AND t504e.c504e_void_fl IS NULL					 
				   AND get_v205_query_exe_status(70004) = 0
				   AND t504.c5040_plant_id     = NVL(sys_context('CLIENTCONTEXT', 'PLANT_ID') ,get_rule_value('DEFAULT_PLANT','ONEPORTAL') )
           GROUP BY t505.c205_part_number_id )intranswipset,
        	   (
		       SELECT  t505.c205_part_number_id,SUM(t505.c505_item_qty) intransavailqty
		         FROM t504_consignment t504,
		            t505_item_consignment t505,
		            t504e_consignment_in_trans t504e,
		            my_temp_part_list mylist
		        WHERE t504.c504_consignment_id = t505.c504_consignment_id
		          AND t504.c504_consignment_id   = t504e.c504_consignment_id
		          AND t504e.c901_status 		IS NULL
		          AND t504.c504_void_fl         IS NULL
		          AND t505.c505_void_fl         IS NULL
		          AND t504e.c504e_void_fl       IS NULL
		          AND t505.c205_part_number_id   = mylist.c205_part_number_id
		          AND get_v205_query_exe_status(70022) = 0
		          AND t504e.c1900_company_id     = NVL( sys_context('CLIENTCONTEXT', 'COMPANY_ID') ,get_rule_value('DEFAULT_COMPANY','ONEPORTAL') )
		        GROUP BY t505.c205_part_number_id
		        )intransavail,
		       (
		       SELECT c205_part_number_id , SUM(alloc_qty) intransallocqty 
				 FROM (     
	             SELECT t413.c205_part_number_id,SUM (t413.c413_item_qty) alloc_qty
        		   FROM t412_inhouse_transactions t412, t413_inhouse_trans_items t413
      			  WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
          			AND t412.c412_verified_date IS NULL
        			AND c412_status_fl = '3'
              	    AND t412.c412_void_fl IS NULL
              	    AND t413.c413_void_fl IS NULL
                    AND t412.c412_type IN (26240358,26240359,26240360)
                    AND get_v205_query_exe_status(70023) = 0
                    AND t412.c5040_plant_id = NVL(sys_context('CLIENTCONTEXT', 'PLANT_ID') ,get_rule_value('DEFAULT_PLANT','ONEPORTAL') )                    
                  GROUP BY t413.c205_part_number_id
	   			UNION ALL 
				  SELECT t505.c205_part_number_id , SUM (t505.c505_item_qty) alloc_qty
				    FROM t504_consignment t504, t505_item_consignment t505 , t504e_consignment_in_trans t504e
				   WHERE t504.c504_consignment_id = t505.c504_consignment_id				     
				     AND t504.c504_reprocess_id   = t504e.c504_consignment_id
					 AND t504.c504_status_fl < '4'
					 AND t504.c504_verify_fl = '0'
					 AND t504.c504_void_fl IS NULL
					 AND t505.c505_void_fl IS NULL
					 AND t504e.c504e_void_fl IS NULL					 
					 AND get_v205_query_exe_status(70023) = 0
					 AND t504.c5040_plant_id     = NVL(sys_context('CLIENTCONTEXT', 'PLANT_ID') ,get_rule_value('DEFAULT_PLANT','ONEPORTAL') )
           GROUP BY t505.c205_part_number_id           
           ) GROUP BY c205_part_number_id)intransalloc		         
	   WHERE t205.c205_part_number_id = mylist.c205_part_number_id
	     AND t205.c205_part_number_id = v205_qty_alloc.c205_part_number_id(+)
		 AND t205.c205_part_number_id = v205_other_alloc.c205_part_number_id(+)
		 AND t205.c205_part_number_id = wip.c205_part_number_id(+)
		 AND t205.c205_part_number_id = rmavail.c205_part_number_id(+)		 
         AND T205.C205_Part_Number_Id = Ihavail.C205_Part_Number_Id(+)	
		 AND T205.C205_Part_Number_Id = rw_avail.C205_Part_Number_Id(+)		
		 AND t205.c205_part_number_id = ihitem.pnum(+)
		 AND t205.c205_part_number_id = NVL(mfgrun.c205_part_number_id(+),-999)
		 AND t205.c205_part_number_id = dhr.c205_part_number_id(+)
		 AND t205.c205_part_number_id = consbuilt.c205_part_number_id(+)
		 AND t205.c205_part_number_id = loan.c205_part_number_id(+)
         AND t205.c205_part_number_id = boqty.c205_part_number_id (+)
		 AND t205.c205_part_number_id = inhouse.c205_part_number_id(+)
		 AND t205.c205_part_number_id = fgavail.c205_part_number_id(+) 
		 AND t205.c205_part_number_id = bulkavail.c205_part_number_id(+)
		 AND t205.c205_part_number_id = qnavail.c205_part_number_id(+)
		 AND t205.c205_part_number_id = returnsavail.c205_part_number_id(+)
		 AND t205.c205_part_number_id = repackageavail.c205_part_number_id(+)
		 AND t205.c205_part_number_id = intransalloc.c205_part_number_id(+)
		 AND t205.c205_part_number_id = intransavail.c205_part_number_id(+)
		 AND t205.c205_part_number_id = intranswipset.c205_part_number_id(+)
	ORDER BY t205.c205_part_number_id;
/
