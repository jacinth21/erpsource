--@"C:\Database\Views\Operations\V205_PARTS_WITH_EXT_VENDOR.vw";
/*
Modified to use all the tissue parts in split transactions for PMT-39890 
*/
CREATE OR REPLACE VIEW V205_PARTS_WITH_EXT_VENDOR
AS
     SELECT c205_part_number_id, c202_project_id ,c906_rule_value  ,t906.c1900_company_id
       FROM t205_part_number t205, t906_rules t906
      WHERE t205.c202_project_id           = t906.c906_rule_id
        AND t906.c906_rule_grp_id     = 'EXVND'
        AND t906.c906_void_fl        IS NULL
        AND t205.c205_product_material=100845 --tissue
		AND NVL(t205.c205_product_family,-999) <> 4053 --Demo-Material
        AND t205.c205_active_fl='Y'; 
