--@"C:\Database\Views\Operations\loaner_late_fee_view.vw";
/***********************************************************************
Filename: loaner_late_fee_view.vw
Component:View 
Description: Creating materialized view to get fetch the loaner late fee report details
**********************************************************************/

DROP MATERIALIZED VIEW v_loaner_late_fee;

CREATE MATERIALIZED VIEW v_loaner_late_fee
NOLOGGING
REFRESH COMPLETE START WITH (SYSDATE) NEXT (SYSDATE+1) + 4/24 WITH ROWID
AS
SELECT t504a.*,
  t504b.c504a_etch_id,t526.c525_product_request_id,t526.c207_set_id,GET_TAG_ID(t504a.c504_consignment_id) tagid,
  t504b.c504a_status_fl lnstatus 
FROM
  (SELECT t504b.c526_product_request_detail_id,
    t504b.c504_consignment_id,
    t504b.c504a_expected_return_dt,
    t504b.c504a_loaner_dt,
    t504b.c504a_return_dt
  FROM t504a_loaner_transaction t504b
  WHERE  t504b.c1900_company_id =1000 and t504b.c504a_loaner_transaction_id =
    (SELECT MAX(to_number(t504a.c504a_loaner_transaction_id))
    FROM t504a_loaner_transaction t504a
    WHERE t504a.c526_product_request_detail_id =
      t504b.c526_product_request_detail_id
    AND t504a.c504a_void_fl IS NULL
    and t504a.c1900_company_id =1000
    GROUP BY c526_product_request_detail_id
    )
  ) t504a,
  t504a_consignment_loaner t504b,
  t526_product_request_detail t526
WHERE t504a.c504_consignment_id = t504b.c504_consignment_id
AND  t504a.c526_product_request_detail_id =t526.c526_product_request_detail_id
and t504b.c504a_void_fl is null
;

