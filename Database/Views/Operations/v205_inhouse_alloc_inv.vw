--@"C:\Database\Views\Operations\v205_inhouse_alloc_inv.vw";
/***********************************************************************
Filename: v205_inhouse_alloc_inv.vw
Component:View
Package:
Designer: Globus Medical
Developer: Aprasath
Version: 1.0
Description: Creating materialized view to get all the inhouse transaction related allocated quantity. On Commit fast
refresh used to refresh data automatically whenever transaction happend in  t412_inhouse_transactions,
t413_inhouse_trans_items
**********************************************************************/
DROP materialized VIEW log ON t412_inhouse_transactions;
DROP materialized VIEW log ON t413_inhouse_trans_items;
DROP materialized VIEW V205_inhouse_alloc_inv;
-- materialized view log is used to create a log of activity on a materialized view�s base table
CREATE materialized VIEW log ON t412_inhouse_transactions 
NOLOGGING
WITH rowid, sequence (c5040_plant_id, c412_inhouse_trans_id, c412_verified_date, c412_void_fl, c412_status_fl, c412_type) including NEW VALUES;
CREATE materialized VIEW log ON t413_inhouse_trans_items
NOLOGGING
WITH rowid, sequence (c412_inhouse_trans_id, c205_part_number_id, c413_item_qty, c413_void_fl) including NEW VALUES;

-- COUNT(*) must always be present to guarantee all types of fast refresh. Otherwise, you may be limited to fast refresh after inserts only
-- COUNT(expr) should required when used SUM(expr) used in fast refresh materialized view

CREATE materialized VIEW v205_inhouse_alloc_inv 
NOLOGGING
REFRESH FAST ON COMMIT
AS
     SELECT a.c5040_plant_id, b.c205_part_number_id, a.c412_type, a.c412_status_fl
      , SUM (b.c413_item_qty) c413_item_qty, COUNT ( *) cnt, COUNT (b.c413_item_qty) qtycnt
       FROM t412_inhouse_transactions a, t413_inhouse_trans_items b, t906_rules t906
      WHERE a.c412_inhouse_trans_id = b.c412_inhouse_trans_id
        AND a.c412_verified_date   IS NULL
        AND a.c412_void_fl         IS NULL
        AND b.c413_void_fl         IS NULL
		AND a.c412_status_fl       != '0'                
        AND TO_CHAR (a.c412_type)   = t906.c906_rule_value
        AND c906_rule_id           IN ('INHOUSE', 'RAWMAT', 'RESTWAREHOUSE', 'IHWAREHOUSE')
        AND c906_rule_grp_id        = 'V205C_ALLOCATED_QTY'
   GROUP BY a.c5040_plant_id, b.c205_part_number_id, a.c412_type, a.c412_status_fl;