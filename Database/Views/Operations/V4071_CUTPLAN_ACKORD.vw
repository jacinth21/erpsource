--@"C:\Database\Views\Operations\V4071_CUTPLAN_ACKORD.vw"
CREATE OR REPLACE VIEW V4071_CUTPLAN_ACKORD
AS
     SELECT TO_CHAR (LOCK_DATE, 'MM') C4071_MONTH, TO_CHAR (LOCK_DATE, 'YYYY') C4071_YEAR, pnum C205_PART_NUMBER_ID
      , customer_po C501_CUSTOMER_PO, pendqty C4071_PO_BALANCE_QTY, (price * pendqty) C4071_PO_BALANCE_PRICE
      , DECODE (acc_type, '4000559', 'Y', 'N') C4071_INTERNATIONAL, DECODE (atr_pnum, '', 'N', 'Y') C4071_FORECAST_ONLY
        , requiredate C4071_PO_NEEDED_BY, inventory_id C250_INVENTORY_LOCK_ID
       FROM
        (
             SELECT t502.c205_part_number_id pnum, t502.c502_item_qty ordqty, (t502.c502_item_qty - get_released_qty (
                t502.c205_part_number_id, t501.c501_order_id, t502.c502_item_price)) pendqty, get_order_attribute_value
                (t501.c501_order_id, '103926') requiredate, T501.C501_CUSTOMER_PO customer_po, T502.C502_ITEM_PRICE
                price, t250.inventory_id, t250.LOCK_DATE
              , T205d.C205_PART_NUMBER_ID atr_pnum, T704.C901_ACCOUNT_TYPE acc_type
               FROM T253E_ORDER_LOCK t501, t253f_item_order_lock t502
                --Take Latest Inventory Lock Record
              , (
                     SELECT MAX (c250_inventory_lock_id) inventory_id, MAX (C250_LOCK_DATE) LOCK_DATE
                       FROM t250_inventory_lock
                      WHERE c901_lock_type = '4000564'
                        AND C250_VOID_FL  IS NULL
                )
                t250,
                --To Determine if the Part is Forecast Only based on Product Specification Type.
                (
                     SELECT T205D.C205_PART_NUMBER_ID
                       FROM T205D_PART_ATTRIBUTE T205d
                      WHERE T205D.C205D_VOID_FL         IS NULL
                        AND T205D.C901_ATTRIBUTE_TYPE    = '103729'
                        AND T205D.C205D_ATTRIBUTE_VALUE IN ('104531', '104532', '104536', '104534')
                )
                t205d, T704_ACCOUNT T704
              WHERE
                --Consider the Orders which has parts not shipped out
                t502.c501_order_id              = t501.c501_order_id
                AND T501.C250_INVENTORY_LOCK_ID = t250.inventory_id
                AND t502.C250_INVENTORY_LOCK_ID = t250.inventory_id
                AND t502.c205_part_number_id    = t205d.C205_PART_NUMBER_ID(+)
                AND T501.C704_ACCOUNT_ID        = T704.C704_ACCOUNT_ID
                AND t501.c1900_company_id 		= 1001 -- BBA Company id
                AND t501.c901_order_type        = '101260' -- Ack Order
                AND t501.c501_void_fl          IS NULL
                AND t501.c501_status_fl        <> '3'
                AND t502.c502_void_fl          IS NULL
                AND t502.c502_item_qty          > get_released_qty (t502.c205_part_number_id, t501.c501_order_id,
                t502.c502_item_price)
                -- ORDER BY t502.c205_part_number_id
        )
        ack_order;
/