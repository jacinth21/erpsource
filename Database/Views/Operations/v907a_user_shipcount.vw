/*
File name: v907a_user_shipcount.vw
Created By: MAzarudeen
PMT # :- PMT-42104
Description: Shipping User Count details 
1000 -> Globus Medical NorthAmerica
*/

CREATE OR REPLACE  VIEW v907a_user_shipcount ("Ref_ID", "STATUS","USERNAME") AS 
SELECT GET_SET_ID_FROM_CN(C907A_REF_ID) ref_id,C907a_status status,
upper(regexp_replace(get_user_name(c907a_transacted_by) ,'(^| )([^ ])([^ ])*','\2')) initials
FROM globus_app.T907a_shipping_status_log
WHERE TRUNC(C907A_TRANSACTED_DATE) = TRUNC(SYSDATE)
AND c1900_company_id = 1000;

/

