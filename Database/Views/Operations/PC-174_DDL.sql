 CREATE INDEX v_ln_late_fee_c526_product_request_detail_id ON v_loaner_late_fee
    (
      c526_product_request_detail_id ASC
    );
    CREATE INDEX v_ln_late_fee_c504_consignment_id ON v_loaner_late_fee
    (
      c504_consignment_id ASC
    );
    
    CREATE INDEX v_ln_late_fee_c207_set_id ON v_loaner_late_fee
    (
      c207_set_id ASC
    );
    
    CREATE INDEX v_ln_late_fee_tagid ON v_loaner_late_fee
    (
      tagid ASC
    );
    