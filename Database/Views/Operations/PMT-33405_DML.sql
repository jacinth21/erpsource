--Create left link for Lot Expiry Dashboard for sterile parts
INSERT INTO t1520_function_list 
(C1520_FUNCTION_ID,
C1520_FUNCTION_NM,
C1520_FUNCTION_DESC,
C901_TYPE,
C1520_CREATED_BY,
C1520_CREATED_DATE,
C1520_REQUEST_URI,
C1520_REQUEST_STROPT,
C901_DEPARTMENT_ID,
C901_LEFT_MENU_TYPE) 
VALUES 
('LOT_EXPIRY_DSH_STR',
'Lot Expiry Dashboard-Sterile Parts',
'This reports is used to display the expiry details of lots on sterile parts',
92262,
'303013',
current_date,
'/gmLotExpiryReport.do?method=loadLotExpiryDashboard',
'loadSterile',
2009,101921);

--Create left link for Lot Expiry Dashboard for Tissue parts
INSERT INTO t1520_function_list 
(C1520_FUNCTION_ID,
C1520_FUNCTION_NM,
C1520_FUNCTION_DESC,
C901_TYPE,
C1520_CREATED_BY,
C1520_CREATED_DATE,
C1520_REQUEST_URI,
C1520_REQUEST_STROPT,
C901_DEPARTMENT_ID,
C901_LEFT_MENU_TYPE) 
VALUES 
('LOT_EXP_DSH_TISSUE',
'Lot Expiry Dashboard-Tissue Parts',
'This reports is used to display the expiry details of lots on tissue parts',
92262,
'303013',
current_date,
'/gmLotExpiryReport.do?method=loadLotExpiryDashboard', 
'loadTissue',
2009,101921);

--create synonynm for new package
 create or replace public synonym gm_pkg_op_lot_expiry_report for GLOBUS_APP.gm_pkg_op_lot_expiry_report;
 
 create or replace public synonym V205_STERILE_PARTS for GLOBUS_APP.V205_STERILE_PARTS;

 create or replace public synonym V205_TISSUE_PARTS for GLOBUS_APP.V205_TISSUE_PARTS;