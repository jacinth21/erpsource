CREATE OR REPLACE VIEW V205_TISSUE_PARTS
AS
    SELECT c205_part_number_id, c202_project_id
       FROM t205_part_number t205
      WHERE t205.c205_product_material=100845 --tissue
        AND NVL(t205.c205_product_family,-999) <> 4053 --Demo-Material
        AND t205.c205_active_fl='Y';