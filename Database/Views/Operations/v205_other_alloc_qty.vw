--@"C:\Database\Views\Operations\v205_other_alloc_qty.vw";
/***********************************************************************
Filename: v205_other_alloc_qty.vw
Component:View
Package:
Designer: Globus Medical
Developer: Aprasath
Version: 1.0
Description: Creating view to get additional allocated quantity values used in v205_qty_in_stock 
and it is used in order planning related files.
**********************************************************************/
CREATE OR REPLACE VIEW v205_other_alloc_qty
AS
      SELECT mylist.c205_part_number_id, SUM (DECODE (c906_rule_id, 'quar_alloc_to_inv', c205_item_qty, 0)) quar_alloc_to_inv
      , SUM(DECODE (c906_rule_id, 'quar_alloc_to_scrap', c205_item_qty, 0)) quar_alloc_to_scrap
      , SUM (DECODE (c906_rule_id,'bulk_alloc_to_inv', c205_item_qty, 0)) bulk_alloc_to_inv
      , SUM (DECODE (c906_rule_id, 'bulk_alloc_to_set',c205_item_qty, 0)) bulk_alloc_to_set
      , SUM (DECODE (c906_rule_id, 'quar_alloc_self_quar', c205_item_qty, 0)) quar_alloc_self_quar
      , SUM (DECODE (c906_rule_id, 'sales_allocated', c205_item_qty, 0)) sales_allocated
      , SUM (DECODE (c906_rule_id, 'cons_alloc', c205_item_qty, 0)) cons_alloc
      , SUM (DECODE (c906_rule_id, 'inv_alloc_to_pack',c205_item_qty, 0)) inv_alloc_to_pack
      , SUM (DECODE (c906_rule_id, 'other_avail', c205_item_qty, 0)) other_avail
      , SUM (DECODE (c906_rule_id, 'other_alloc', c205_item_qty, 0)) other_alloc
   FROM
    (
         SELECT v205.c5040_plant_id, v205.c205_part_number_id, t906.c906_rule_value c901_transaction_type, SUM (v205.c502_item_qty) c205_item_qty
           FROM V205_sales_alloc_inv v205, t906_rules t906, my_temp_part_list mylist
          WHERE (v205.c901_ext_country_id IS NULL
            OR EXISTS
            (
                 SELECT country_id
                   FROM v901_country_codes
                  WHERE country_id = v205.c901_ext_country_id
            ))
            AND v205.c205_part_number_id = mylist.c205_part_number_id
            AND t906.c906_rule_grp_id = 'V205C_ALLOCATED_QTY'
            AND t906.c906_rule_id          = 'ORDER'
            AND v205.c5040_plant_id		   = NVL(sys_context('CLIENTCONTEXT', 'PLANT_ID') ,get_rule_value('DEFAULT_PLANT','ONEPORTAL') ) 
            AND NOT EXISTS
            (
                 SELECT c906_rule_value
                   FROM t906_rules 
                  WHERE c906_rule_value  = NVL (v205.c901_order_type, - 9999)
                    AND c906_rule_grp_id = 'ORDTYPE'
                    AND c906_rule_id          = 'EXCLUDE'
            )
       GROUP BY v205.c5040_plant_id, v205.c205_part_number_id, t906.c906_rule_value
      UNION ALL
        --CONSIGNMENT
         SELECT v205.c5040_plant_id, v205.c205_part_number_id, t906.c906_rule_value c901_transaction_type, SUM (v205.c505_item_qty) c205_item_qty
           FROM V205_consignment_alloc_inv v205, t906_rules t906, my_temp_part_list mylist 
          WHERE NVL (v205.c901_warehouse_type, 999) = DECODE (get_rule_value ('CURRENT_DB', 'COUNTRYCODE'), 'en', DECODE (
            v205.c504_type, 4110, 90800, 4112, 90800, NVL (v205.c901_warehouse_type, 999)), NVL (v205.c901_warehouse_type, 999))
            AND v205.c205_part_number_id = mylist.c205_part_number_id
            AND t906.c906_rule_grp_id = 'V205C_ALLOCATED_QTY'
			AND t906.c906_rule_id ='CONSIGNMENT'
            AND t906.c906_rule_value       = TO_CHAR (v205.c504_type)
            AND v205.c5040_plant_id		   = NVL(sys_context('CLIENTCONTEXT', 'PLANT_ID') ,get_rule_value('DEFAULT_PLANT','ONEPORTAL') )
       GROUP BY v205.c5040_plant_id, v205.c205_part_number_id, t906.c906_rule_value
      UNION ALL
        --INHOUSE
         SELECT v205.c5040_plant_id, v205.c205_part_number_id, t906.c906_rule_value c901_transaction_type, SUM (NVL (v205.c413_item_qty, 0))
            c205_item_qty
           FROM v205_inhouse_alloc_inv v205, t906_rules t906, my_temp_part_list mylist 
          WHERE t906.c906_rule_id         IN ('INHOUSE')
            AND t906.c906_rule_grp_id      = 'V205C_ALLOCATED_QTY'
            AND t906.c906_rule_value       = TO_CHAR (v205.c412_type)
            AND v205.c205_part_number_id   = mylist.c205_part_number_id
            AND v205.c5040_plant_id		   = NVL(sys_context('CLIENTCONTEXT', 'PLANT_ID') ,get_rule_value('DEFAULT_PLANT','ONEPORTAL') )
       GROUP BY v205.c5040_plant_id, v205.c205_part_number_id, t906.c906_rule_value
    )
    v205, t906_rules t906, my_temp_part_list mylist, t205_part_number t205 
  WHERE t205.c205_part_number_id = mylist.c205_part_number_id
	AND mylist.c205_part_number_id = v205.c205_part_number_id(+)
    AND t906.c906_rule_grp_id(+)  = 'V205_QTY_IN_STOCK'
    AND t906.c906_rule_value(+)       = v205.c901_transaction_type
    GROUP BY mylist.c205_part_number_id;
    /
