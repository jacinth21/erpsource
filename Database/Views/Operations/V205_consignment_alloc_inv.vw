--@"C:\Database\Views\Operations\V205_consignment_alloc_inv.sql";
/***********************************************************************
Filename: V205_consignment_alloc_inv.vw
Component:View
Package:
Designer: Globus Medical
Developer: Aprasath
Version: 1.0
Description: Creating materialized view to get all the consignment transaction related allocated quantity. On Commit fast
refresh used to refresh data automatically whenever transaction happend in  t504_consignment,
t505_item_consignment
**********************************************************************/
DROP materialized VIEW log ON t504_consignment;
DROP materialized VIEW log ON t505_item_consignment;
DROP materialized VIEW log ON t906_rules;
DROP materialized VIEW V205_consignment_alloc_inv;
-- materialized view log is used to create a log of activity on a materialized view�s base table
CREATE materialized VIEW log ON t504_consignment
NOLOGGING
WITH rowid, sequence (c5040_plant_id, c504_type, c504_consignment_id, c504_status_fl, c207_set_id, c504_void_fl, c504_ship_date)
    including NEW VALUES;
CREATE materialized VIEW log ON t505_item_consignment
NOLOGGING
WITH rowid, sequence (c505_item_qty, c504_consignment_id, c205_part_number_id, c901_warehouse_type) including NEW
    VALUES;
CREATE materialized VIEW log ON t906_rules
NOLOGGING
WITH rowid, SEQUENCE (c906_rule_id, c906_rule_value, c906_rule_grp_id) including NEW VALUES;

-- COUNT(*) must always be present to guarantee all types of fast refresh. Otherwise, you may be limited to fast refresh after inserts only
-- COUNT(expr) should required when used SUM(expr) used in fast refresh materialized view

CREATE MATERIALIZED VIEW V205_consignment_alloc_inv
NOLOGGING
REFRESH FAST ON COMMIT
AS
     SELECT a.c5040_plant_id, b.c205_part_number_id, a.c504_type, b.c901_warehouse_type
      , a.c504_status_fl, SUM (b.c505_item_qty) c505_item_qty, COUNT ( *) cnt
      , COUNT (b.c505_item_qty) qtycnt
       FROM t504_consignment a, t505_item_consignment b, t906_rules t906
      WHERE a.c504_consignment_id = b.c504_consignment_id
        AND a.c504_status_fl      < 4
        AND a.c207_set_id        IS NULL
        AND TO_CHAR (a.c504_type) = t906.c906_rule_value
        AND t906.c906_rule_id     = 'CONSIGNMENT'
        AND t906.c906_rule_grp_id = 'V205C_ALLOCATED_QTY'
        AND a.c504_void_fl       IS NULL
        AND a.c504_ship_date     IS NULL
   GROUP BY a.c5040_plant_id, b.c205_part_number_id, a.c504_type, b.c901_warehouse_type
      , a.c504_status_fl;