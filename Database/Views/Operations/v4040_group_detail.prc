/* Formatted on 2007/09/03 09:44 (Formatter Plus v4.8.8) */
CREATE OR REPLACE  VIEW  v4040_group_detail
AS
SELECT t4011.c205_part_number_id, t4010.c4010_group_id FROM t4040_demand_sheet t4040
, t4041_demand_sheet_mapping t4041 ,  t4010_group t4010,  t4011_group_detail t4011
WHERE t4040.c4040_demand_sheet_id = t4041.c4040_demand_sheet_id
AND t4041.c901_ref_type = 40030
AND t4041.c4041_ref_id = t4010.c4010_group_id
AND t4011.c4010_group_id = t4010.c4010_group_id;
/
