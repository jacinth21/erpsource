--@"C:\Database\Views\Operations\V502A_BO_fulfill_list.vw";
/********************************************************************************************************************
Filename: V502A_BO_fulfill_list.vw
Component:View
Designer: Globus Medical
Developer: Karthik
Description: Creating view to get the FG quantity ,Back Order quantity for all the parts available in the Backorder 
**********************************************************************************************************************/
CREATE OR REPLACE VIEW V502A_BO_fulfill_list
AS
  SELECT C205_PART_NUMBER_ID,
    BOQTY,
    INSHELF,
    CASE
      WHEN INSHELF <= 0
      THEN '3'
      WHEN INSHELF < BOQTY
      THEN '2'
      WHEN INSHELF >= BOQTY
      THEN '1'
    END BOPRIORITY
  FROM
    (SELECT T502.C205_PART_NUMBER_ID,
      BOQTY,
      NVL(fg_avail_qty,0) - NVL(c205_shelfalloc,0) INSHELF
    FROM
      (SELECT T502.C205_PART_NUMBER_ID,
        SUM(T502.C502_ITEM_QTY) BOQTY
      FROM T501_ORDER T501,
        T502_ITEM_ORDER T502
      WHERE T501.C501_ORDER_ID   = T502.C501_ORDER_ID
      AND T501.C901_ORDER_TYPE   = 2525
      AND T501.C501_DELETE_FL   IS NULL
      AND T501.C501_VOID_FL     IS NULL
      AND T501.C501_STATUS_FL    = 0
      AND T502.C502_VOID_FL     IS NULL
      AND T501.C5040_PLANT_ID    = NVL(sys_context('CLIENTCONTEXT', 'PLANT_ID') ,get_rule_value('DEFAULT_PLANT','ONEPORTAL') )
      GROUP BY T502.C205_PART_NUMBER_ID
      ) t502 ,
      (SELECT t205c.c205_part_number_id,
        NVL (c205_available_qty, 0) fg_avail_qty
      FROM t205c_part_qty t205c
      WHERE C901_Transaction_Type = 90800 --90800 - Inventory Qty
      AND t205c.c5040_plant_id    =  NVL(sys_context('CLIENTCONTEXT', 'PLANT_ID') ,get_rule_value('DEFAULT_PLANT','ONEPORTAL') )
      ) T205C ,
      (SELECT t205.c205_part_number_id,
        SUM (DECODE (v205c.c901_transaction_type, '60001', v205c.c205_allocated_qty, 0)) c205_shelfalloc
      FROM t205_part_number t205,
        v205c_part_allocated_qty v205c
      WHERE v205c.c5040_plant_id   =  NVL(sys_context('CLIENTCONTEXT', 'PLANT_ID') ,get_rule_value('DEFAULT_PLANT','ONEPORTAL') )
      AND t205.c205_part_number_id = v205c.c205_part_number_id (+)
      GROUP BY t205.c205_part_number_id
      ) T205_ALLOC
    WHERE t502.C205_PART_NUMBER_ID = t205c.c205_part_number_id(+)
    AND t502.C205_PART_NUMBER_ID   = T205_ALLOC.c205_part_number_id(+)
    );
  /