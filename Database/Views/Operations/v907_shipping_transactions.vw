/*
File name: v907_shipping_transactions.vw
Created By: MAzarudeen
PMT # :- PMT-42104
Description: Shipping Transaction
*/


/*
File name: pbi_Shipping_Order.vw
Created By: MAzarudeen
PMT # :- PMT-42104
Description: Shipping Transaction
*/
CREATE OR REPLACE VIEW v907_shipping_transactions
AS
  SELECT *
  FROM
    (SELECT COUNT,
      LABEL
    FROM
      (SELECT 'Orders_Pending_control' Label,
		COUNT (1) COUNT
	  FROM t501_order t501,
		t5057_building t5057
	  WHERE t501.c501_status_fl     IS NOT NULL
	  AND t501.c501_status_fl        = '1'  --PC-4747 Tuning - T501_ORDER
	  AND t501.c501_shipping_date   IS NULL
	  AND t501.c501_void_fl         IS NULL
	  AND t5057.c5057_active_fl(+)   = 'Y'
	  AND t5057.c5057_void_fl       IS NULL
	  AND t501.c501_delete_fl       IS NULL
	  AND t501.c5057_building_id     = t5057.c5057_building_id(+)
	  AND (t501.c901_ext_country_id IS NULL
	  OR t501.c901_ext_country_id   IN
	  (SELECT country_id FROM v901_country_codes))
	  AND (t501.c901_order_type IS NULL
	  OR t501.c901_order_type   IN
	  (SELECT t906.c906_rule_value
	  FROM t906_rules t906
	  WHERE t906.c906_rule_grp_id = 'WIP_ORDER_GRP'
	  AND c906_rule_id            = 'WIP_ORDER'
	  AND c906_void_fl           IS NULL))
	  AND (t501.c1900_company_id = 3000
	  OR t501.c5040_plant_id     = 3000)
      UNION ALL
      -- Order - Pending Shipping
      SELECT 'Orders_Pending_Shipping' Label,
        COUNT (1) COUNT
      FROM t907_shipping_info t907
      WHERE t907.c901_source    = 50180
      AND T907.C1900_COMPANY_ID = 1000
      AND t907.c5040_plant_id   = 3000
      AND t907.c907_status_fl   = 30 --Pending shipping
      AND t907.c907_active_fl  IS NULL
      AND t907.c907_void_fl    IS NULL
      UNION ALL
      -- Order - Packing Inprogress
      SELECT 'Orders_Packing_Inprogress' Label,
        COUNT (1) COUNT
      FROM t907_shipping_info t907
      WHERE t907.c901_source    = 50180
      AND T907.C1900_COMPANY_ID = 1000
      AND t907.c5040_plant_id   = 3000
      AND t907.c907_status_fl   = 33 --Packing in progress
      AND t907.c907_active_fl  IS NULL
      AND t907.c907_void_fl    IS NULL
      UNION ALL
      ----   Order - Ready for Pickup
      SELECT 'Orders_Ready_shipping' LABEL,
        COUNT (1) COUNT
      FROM t907_shipping_info t907
      WHERE t907.c901_source    = 50180
      AND T907.C1900_COMPANY_ID = 1000
      AND t907.c5040_plant_id   = 3000
      AND t907.c907_status_fl   = 36
      AND t907.C907_ACTIVE_FL  IS NULL
      AND T907.C907_VOID_FL    IS NULL
      UNION ALL
      -- Item consignment - Pendign Control Number
      SELECT 'Consignment_Pending_control' LABEL,
        COUNT(1)
      FROM
        (SELECT t504.c504_consignment_id
        FROM t907_shipping_info t907,
          t504_consignment t504
        WHERE t504.c504_void_fl  IS NULL
        AND t907.c907_ref_id      = t504.c504_consignment_id
        AND t907.c901_source      = 50181
        AND t907.c1900_company_id = 1000
        AND t907.c5040_plant_id = 3000  --PC-559
        AND T907.C907_STATUS_FL   = 15
        AND t504.c207_set_id     IS NULL
        AND t907.C907_ACTIVE_FL  IS NULL
        AND t504.c504_status_fl   = '2'
        AND t504.c504_type       IN (4110, 40057)
        AND t907.c907_void_fl    IS NULL
        UNION ALL
        SELECT t504.c504_consignment_id
        FROM t504_consignment t504
        WHERE t504.c504_status_fl  = '2.20'
        AND T504.C504_TYPE        IN (4110, 40057)
        AND T504.C504_Void_Fl     IS NULL
        AND (t504.c1900_company_id = 1000
        OR t504.c5040_plant_id     = 1000)
        )
      UNION ALL
      -- Consignment - Pending Shipping
      SELECT 'Consignment_Pending_Shipping' Label,
        COUNT (1) COUNT
      FROM t907_shipping_info t907
      WHERE t907.c901_source    = 50181
      AND t907.c1900_company_id = 1000
      AND t907.c5040_plant_id = 3000   --PC-559
      AND t907.c907_status_fl  = 30 --Pending shipping
      AND t907.c907_active_fl IS NULL
      AND t907.c907_void_fl   IS NULL
      UNION ALL
      -- Consignment - Packing Inprogress
      SELECT 'Consignment_Packing_Inprogress' Label,
        COUNT (1) COUNT
      FROM t907_shipping_info t907
      WHERE t907.c901_source    = 50181
      AND t907.c1900_company_id = 1000
      AND t907.c5040_plant_id = 3000  --PC-559
      AND t907.c907_status_fl  = 33 --Packing in progress
      AND t907.c907_active_fl IS NULL
      AND T907.C907_VOID_FL   IS NULL
      UNION ALL
      SELECT 'Consignment_Ready_shipping' LABEL,
        COUNT (1) COUNT
      FROM t907_shipping_info t907
      WHERE t907.c901_source    = 50181
      AND t907.c1900_company_id = 1000
      AND t907.c5040_plant_id = 3000  --PC-559
      AND t907.c907_status_fl  = 36
      AND t907.c907_void_fl   IS NULL
      AND t907.C907_ACTIVE_FL IS NULL
      UNION ALL
      SELECT 'Loaner_Pending_control' LABEL,
        COUNT(1)
      FROM t504a_consignment_loaner t504b,
        t504a_loaner_transaction t504a,
        t525_product_request t525 ,
        t526_product_request_detail t526,
        t5010_tag t5010,
        t5053_location_part_mapping t5053,
        t504_consignment t504     --PC-559
      WHERE t504b.c504a_status_fl              = '7' --(pending Pick)
      AND t504b.c504a_void_fl                 IS NULL
      AND t504.c504_void_fl IS NULL    --PC-559
      AND t504a.c504_consignment_id            = t5010.c5010_last_updated_trans_id (+)
      AND t5010.c5010_tag_id                   = t5053.c5010_tag_id (+)
      AND t504b.c504_consignment_id            = t504a.c504_consignment_id
      AND t504a.c504_consignment_id            = t504.c504_consignment_id  --PC-559
      AND t504b.c504_consignment_id            = t504.c504_consignment_id  --PC-559
      AND t504a.c504a_void_fl                 IS NULL
      AND t504a.c526_product_request_detail_id = t526.c526_product_request_detail_id
      AND T525.C525_PRODUCT_REQUEST_ID         = T526.C525_PRODUCT_REQUEST_ID
      AND t525.c525_product_request_id        IN
        (SELECT DISTINCT T525.C525_PRODUCT_REQUEST_ID
        FROM t504a_consignment_loaner t504b,
          t504a_loaner_transaction t504a,
          t525_product_request t525 ,
          t526_product_request_detail t526,
        t504_consignment t504     --PC-559
        WHERE t504b.c504a_status_fl              = '7' --(pending Pick)
        AND t504b.c504a_void_fl                 IS NULL
        AND t504.c504_void_fl IS NULL    --PC-559
        AND t504b.c504_consignment_id            = t504a.c504_consignment_id
        AND t504a.c504_consignment_id            = t504.c504_consignment_id  --PC-559
        AND t504b.c504_consignment_id            = t504.c504_consignment_id  --PC-559
        AND t504a.c504a_void_fl                 IS NULL
        AND t504a.c504a_return_dt               IS NULL
        AND t504a.c526_product_request_detail_id = t526.c526_product_request_detail_id
        AND T525.C525_PRODUCT_REQUEST_ID         = T526.C525_PRODUCT_REQUEST_ID
        AND T526.C901_REQUEST_TYPE               = 4127
        AND t525.c525_void_fl                   IS NULL
        AND t526.c526_void_fl                   IS NULL
        AND t504a.c1900_company_id               = 1000
        AND t504a.C1900_COMPANY_ID               = t525.C1900_COMPANY_ID
        AND t504a.C5040_PLANT_ID                 = T504B.C5040_PLANT_ID
        AND t504a.C5040_PLANT_ID                 = t525.C5040_PLANT_ID
        )
      AND t525.c525_void_fl      IS NULL
      AND t526.c526_void_fl      IS NULL
      AND t504a.c1900_company_id  = 1000
      AND T5010.C5010_Void_Fl(+) IS NULL
      AND t504a.C1900_COMPANY_ID  = t525.C1900_COMPANY_ID
      AND t504a.C5040_PLANT_ID    = T504B.C5040_PLANT_ID
      AND t504a.c5040_plant_id    = t525.c5040_plant_id
      UNION ALL
      -- Loaners - Pending Shipping
      SELECT 'Loaner_Pending_Shipping' LABEL,
        COUNT (1) COUNT
      FROM t907_shipping_info t907
      WHERE t907.c901_source    = 50182
      AND t907.c1900_company_id = 1000
        --AND t907.c5040_plant_id = 3000
      AND t907.c907_status_fl  = 30--Pending shipping
      AND t907.C907_ACTIVE_FL IS NULL
      AND t907.c907_void_fl   IS NULL
      UNION ALL
      -- Loaners - Packing Inprogress
      SELECT 'Loaner_Packing_Inprogress' LABEL,
        COUNT (1) COUNT
      FROM t907_shipping_info t907
      WHERE t907.c901_source    = 50182
      AND t907.c1900_company_id = 1000
        --AND t907.c5040_plant_id = 3000
      AND t907.c907_status_fl  = 33 --Packing in progress
      AND t907.C907_ACTIVE_FL IS NULL
      AND t907.c907_void_fl   IS NULL
      UNION ALL
      -- Loaners - that are ready for pickup
      SELECT 'Loaner_Ready_shipping' Label,
        COUNT (1) COUNT
      FROM t907_shipping_info t907
      WHERE t907.c901_source    = 50182
      AND t907.c1900_company_id = 1000
        --AND t907.c5040_plant_id = 3000
      AND t907.c907_status_fl  = 36
      AND t907.c907_void_fl   IS NULL
      AND t907.C907_ACTIVE_FL IS NULL
      UNION ALL
      --   Loaner Extension - Pending Control
      SELECT 'Loaner_Replenishment_Pending_control' Label,
        COUNT (1) COUNT
      FROM T907_SHIPPING_INFO T907,
        T412_INHOUSE_TRANSACTIONS T412
      WHERE T907.C907_REF_ID    = T412.C412_INHOUSE_TRANS_ID
      AND T907.C901_SOURCE      = 50183
      AND t412.c412_void_fl    IS NULL
      AND T907.C1900_COMPANY_ID = 1000
        --AND t907.c5040_plant_id   = 3000
      AND T907.C907_STATUS_FL  = 15
      AND T412.c412_status_fl IN ('2','3')
      AND T907.C907_ACTIVE_FL IS NULL
      AND T907.C907_VOID_FL   IS NULL
      UNION ALL
      ----   Loaner Extension - Ready for Pic
      --   Loaner Extension - Pending Shipping
      SELECT 'Loaner_Replenishment_Pending_Shipping' LABEL,
        COUNT (1) COUNT
      FROM t907_shipping_info t907
      WHERE t907.c901_source    = 50183
      AND T907.C1900_COMPANY_ID = 1000
        --AND t907.c5040_plant_id = 3000
      AND t907.c907_status_fl  = 30
      AND t907.c907_active_fl IS NULL
      AND T907.C907_VOID_FL   IS NULL
      UNION ALL
      --   Loaner Extension - Packing InProgress
      SELECT 'Loaner_Replenishment_Packing_InProgress' LABEL,
        COUNT (1) COUNT
      FROM t907_shipping_info t907
      WHERE t907.c901_source    = 50183
      AND t907.c1900_company_id = 1000
        --AND t907.c5040_plant_id = 3000
      AND t907.c907_status_fl  = 33
      AND t907.c907_active_fl IS NULL
      AND T907.C907_VOID_FL   IS NULL
      UNION ALL
      ----   Loaner Extension - Ready for Pickup / Completed
      SELECT 'Loaner_Replenishment_Ready_shipping' LABEL,
        COUNT (1) COUNT
      FROM t907_shipping_info t907
      WHERE t907.c901_source    = 50183
      AND T907.C1900_COMPANY_ID = 1000
        --AND t907.c5040_plant_id = 3000
      AND t907.c907_status_fl  = 36
      AND T907.C907_ACTIVE_FL IS NULL
      AND T907.C907_VOID_FL   IS NULL
      )
    ) PIVOT (SUM (COUNT) FOR LABEL IN ('Orders_Pending_control', 'Orders_Pending_Shipping','Orders_Packing_Inprogress', 'Orders_Ready_shipping', 'Consignment_Pending_control', 'Consignment_Ready_shipping', 'Consignment_Pending_Shipping','Consignment_Packing_Inprogress', 'Loaner_Pending_control', 'Loaner_Ready_shipping', 'Loaner_Pending_Shipping','Loaner_Packing_Inprogress', 'Loaner_Replenishment_Pending_control', 'Loaner_Replenishment_Ready_shipping', 'Loaner_Replenishment_Pending_Shipping','Loaner_Replenishment_Packing_InProgress'));
                   
  /



/

