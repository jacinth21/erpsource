--@"C:\database\Views\Operations\V205_LBL_PARAM_FOR_PRINTING.vw";

CREATE OR REPLACE VIEW  V205_LBL_PARAM_FOR_PRINTING 
AS
  SELECT
    t2550.pnum productno ,
    t2550.cnum allograftcode ,
    t2550.expdate expirationdate ,
    t2550.donorno iddonor ,
    t2550.packslip loadrunno ,
    t2550.createddate entereddate ,
    t2550.receiveddate receiveddate,
    t2550.dhrid dhrid,
    t2550.customsize customsize,
    v205g.c205d_general_spec productdescriptiongeneric ,
    t205.c205_part_num_desc description ,
    v205g.c205d_patient_label productlabelpatientversionno ,
    v205g.c205d_box_label productlabelboxversionno ,
    v205g.c205d_package_label productlabelpackageversionno ,
    v205g.c205d_cpc_patient_label cpclblgraftptvernoinproduct,
    v205g.c205d_cpc_package_label cpclblgraftpkgvernoinproduct,
    v205g.c205d_label_size lblsize ,
    v205g.c205d_sample issample ,
    v205g.c205d_cpc_logo_path cpclogographicfilepath,
    preservemet.c901_code_nm tissuetypedescription,
    storage.c901_code_nm tissuetypestorageinstructions,
    t704.c704b_cpc_name cpc_name,
    t704.c704b_cpc_address cpc_address,
    t704.c704b_cpc_barcode_lbl_ver cpc_lbl_ver,
    t704.c704b_cpc_final_cpc_ver cpc_final_ver
  FROM
    (
	/* 
	 * Fetch all the grafts from Part / Control table, 
	 * exclude the control numbers that are initiated for Part Resignation as those 
	 * are included in the following SQL
	 */
     SELECT t2550.c205_part_number_id pnum,
            t2550.c2550_control_number cnum,
            t2550.c2550_packing_slip packslip,
            t2540.c2540_donor_number donorno,
            t2550.c2550_expiry_date expdate,
            t2550.c2550_last_updated_date createddate,
            t2550.c2550_last_updated_date receiveddate,
            t2550.c2550_last_updated_trans_id dhrid,
            t2550.c2550_custom_size customsize
       FROM t2550_part_control_number t2550,
            t2540_donor_master t2540
        WHERE t2550.c2540_donor_id = t2540.c2540_donor_id 
        AND t2540.c2540_void_fl IS NULL
        AND t2550.c2550_control_number NOT IN 
             ( 
             SELECT t413.c413_control_number
               FROM t412_inhouse_transactions t412 ,
                    t413_inhouse_trans_items t413
              WHERE t412.c412_inhouse_trans_id         = t413.c412_inhouse_trans_id
                AND t412.c412_status_fl              != '4'      --Not VERIFIED
                AND t412.c412_type                   = '103932' -- PTRD
                AND t412.c412_void_fl               IS NULL
                AND t413.c413_void_fl               IS NULL
                AND t413.c205_client_part_number_id IS NOT NULL
             )
  UNION ALL
	/* 
	 * Grafts won't have entries in control number table until DHR is verified
	 * , so need to fetch grafts from OPen DHR's 
	 */	
	SELECT t408a.c205_part_number_id pnum,
		   t408a.c408a_conrol_number cnum,
		   t408.c408_packing_slip packslip,
		   t2540.c2540_donor_number donorno,
		   t408.c2550_expiry_date expdate,
		   t408.c408_created_date createddate,
		   t408.c408_received_date receiveddate,
		   t408.c408_dhr_id dhrid,
		   t408a.c408a_custom_size
	  FROM t408_dhr t408 ,
		   t408a_dhr_control_number t408a ,
		   t2540_donor_master t2540
	 WHERE t408a.c408_dhr_id      = t408.c408_dhr_id
	   AND t2540.c2540_donor_id = t408.c2540_donor_id
	   AND t408.c408_void_fl   is null
	   AND t2540.c2540_void_fl is null
	   AND t408.c408_status_fl != 4  
  UNION ALL
	/* 
	 * Grafts that are initiated for Part Redesignation need to 
	 * be fetched from inhouse txn table with Client part number 
	 * as the part number
	 */	
    SELECT
        t413.c205_client_part_number_id pnum,
        t413.c413_control_number cnum,
        t413.c412_inhouse_trans_id packslip,
        t2540.c2540_donor_number donorno,
        t2550.c2550_expiry_date expdate,
        t413.c412_created_date createddate,
        t413.c412_last_updated_date receiveddate,
        t413.c412_inhouse_trans_id dhrid,
        t2550.c2550_custom_size customsize
      FROM
        (
          SELECT
            t412.c412_inhouse_trans_id ,
            t413.c205_client_part_number_id ,
            t413.c205_part_number_id,
            t413.c413_control_number ,
            MIN(t412.c412_created_date) c412_created_date,
            MIN(t412.c412_last_updated_date) c412_last_updated_date
          FROM
            t412_inhouse_transactions t412 ,
            t413_inhouse_trans_items t413
          WHERE
            t412.c412_inhouse_trans_id         = t413.c412_inhouse_trans_id
          AND t412.c412_status_fl              != '4'      --Not VERIFIED
          AND t412.c412_type                   = '103932' -- PTRD
          AND t412.c412_void_fl               IS NULL
          AND t413.c413_void_fl               IS NULL
          AND t413.c205_client_part_number_id IS NOT NULL
          GROUP BY
            t412.c412_inhouse_trans_id ,
            t413.c205_client_part_number_id ,
            t413.c205_part_number_id ,
            t413.c413_control_number
        )
        t413 ,
        t2540_donor_master t2540 ,
        t2550_part_control_number t2550
      WHERE
        t2540.c2540_donor_id         = t2550.c2540_donor_id
      AND t2550.c205_part_number_id  = t413.c205_part_number_id
      AND t2550.c2550_control_number = t413.c413_control_number
      AND t2540.c2540_void_fl       IS NULL 
    )
    t2550,
    v205g_part_label_parameter v205g ,
    t205_part_number t205 ,
	/* Fetch CPC information, this is used to display in the label */
    (
      SELECT
        v205g.c205_part_number_id,
        t704b.c704b_cpc_name,
        t704b.c704b_cpc_address,
        t704b.c704b_cpc_barcode_lbl_ver,
        t704b.c704b_cpc_final_cpc_ver
      FROM
        v205g_part_label_parameter v205g,
        t704b_account_cpc_mapping t704b
      WHERE
        NVL(v205g.c205d_contract_process_client,'-999') = NVL(TO_CHAR(
        t704b.c704b_account_cpc_map_id(+)),'-999')
      AND t704b.c704b_void_fl(+)   IS NULL
      AND t704b.c704b_active_fl(+) IS NOT NULL
    )
    t704,
    (
      SELECT
        c901_code_id,
        c901_code_nm
      FROM
        t901_code_lookup
      WHERE
        c901_code_grp    = 'STRGTP'-- Storage Temperature
      AND c901_active_fl = '1'
      AND c901_void_fl  IS NULL
    )
    STORAGE,
    (
      SELECT
        c901_code_id,
        c901_code_nm
      FROM
        t901_code_lookup
      WHERE
        c901_code_grp    = 'PREMET' -- Preservation Method
      AND c901_active_fl = '1'
      AND c901_void_fl  IS NULL
    )
    preservemet
  WHERE
    t2550.pnum                                    = t205.c205_part_number_id
  AND NVL(v205g.c205d_preservation_method,-999) = preservemet.c901_code_id (+)
  AND NVL(v205g.c205d_storage_temperature,-999) = storage.c901_code_id (+)
  AND t2550.pnum                                  = v205g.c205_part_number_id (+)
  AND t2550.pnum                                  = t704.c205_part_number_id (+);
