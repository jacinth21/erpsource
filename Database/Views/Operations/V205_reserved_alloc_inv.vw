--@"C:\Database\Views\Operations\V205_reserved_alloc_inv.sql";
/***********************************************************************
Filename: V205_reserved_alloc_inv.vw
Component:View
Package:
Designer: Globus Medical
Developer: Aprasath
Version: 1.0
Description: Creating materialized view to get all the reserved transaction related allocated quantity. On Commit fast
refresh used to refresh data automatically whenever transaction happend in  t5062_control_number_reserve,
t5060_control_number_inv
**********************************************************************/
DROP materialized VIEW log ON t5062_control_number_reserve;
DROP materialized VIEW log ON t5060_control_number_inv;
DROP materialized VIEW V205_reserved_alloc_inv;
-- materialized view log is used to create a log of activity on a materialized view�s base table
CREATE materialized VIEW log ON t5062_control_number_reserve
NOLOGGING
WITH rowid, sequence (c5040_plant_id, c5062_reserved_qty, c5060_control_number_inv_id, c901_status, c901_transaction_type,
    c5062_void_fl) including NEW VALUES;
CREATE materialized VIEW log ON t5060_control_number_inv
NOLOGGING
WITH rowid, sequence (c5040_plant_id, c205_part_number_id, c5060_control_number_inv_id) including NEW VALUES;

-- COUNT(*) must always be present to guarantee all types of fast refresh. Otherwise, you may be limited to fast refresh after inserts only
-- COUNT(expr) should required when used SUM(expr) used in fast refresh materialized view

CREATE materialized VIEW V205_reserved_alloc_inv
NOLOGGING
REFRESH FAST ON COMMIT
AS
     SELECT t5062.c5040_plant_id, t5060.c205_part_number_id, SUM (c5062_reserved_qty) c5062_reserved_qty, COUNT ( *) cnt
      , COUNT (c5062_reserved_qty) qtycnt
       FROM t5062_control_number_reserve t5062, t5060_control_number_inv t5060
      WHERE t5060.c5060_control_number_inv_id = t5062.c5060_control_number_inv_id
     	AND t5062.c5040_plant_id 			  = t5060.c5040_plant_id
        AND c901_status                      IN ('103960') -- open
        AND c901_transaction_type            IN ('103980') -- order trans
        AND c5062_void_fl                    IS NULL
   GROUP BY t5062.c5040_plant_id, t5060.c205_part_number_id;
