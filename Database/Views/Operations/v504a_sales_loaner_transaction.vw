/*
File name: v504a_sales_loaner_transaction.vw
Created By: jbalaraman
Description: Shipping Transaction
PC-3905 Field Inventory Report performance improvement
*/
DROP MATERIALIZED VIEW v504a_sales_loaner_transaction;

CREATE MATERIALIZED VIEW v504a_sales_loaner_transaction REFRESH COMPLETE START WITH
  (
    SYSDATE
  )
  NEXT (TRUNC(LAST_DAY(SYSDATE)) + 1) 
AS
SELECT t504.c504_consignment_id, t504a.c504a_consigned_to_id, t504.c207_set_id
  , get_set_name (T504.c207_set_id) c207_set_nm
  , v207.c207_set_nm c207_system_name
  , c207_seq_no, TRUNC(t504a.c504a_loaner_dt) c504a_loaner_dt
   FROM t504_consignment t504, t504a_loaner_transaction t504a, v207a_set_consign_link V207
  WHERE T504.c207_set_id               = V207.c207_actual_set_id
    AND t504.c504_consignment_id       = t504a.c504_consignment_id
    AND t504a.c504a_loaner_dt >= TRUNC (ADD_MONTHS (CURRENT_DATE,    - 3), 'MM')
    AND t504a.c504a_loaner_dt <= LAST_DAY (ADD_MONTHS (CURRENT_DATE, - 1))
    AND t504.c504_type                 = 4127
    AND v207.c901_link_type            = 20100
    AND t504.c504_void_fl             IS NULL
    AND t504a.c504a_void_fl           IS NULL
    AND t504.c207_set_id              IS NOT NULL;
    
create index I504A_MV_SALES_LOANER_01 ON v504a_sales_loaner_transaction (C504A_CONSIGNED_TO_ID, C207_SET_ID, C504A_LOANER_DT);
create index I504A_MV_SALES_LOANER_CONSIGNED_TO ON v504a_sales_loaner_transaction (C504A_CONSIGNED_TO_ID);
create index I504A_MV_SALES_LOANER_SET_ID ON v504a_sales_loaner_transaction (C207_SET_ID);
create index I504A_MV_SALES_LOANER_LOANER_DT ON v504a_sales_loaner_transaction (C504A_LOANER_DT);

/