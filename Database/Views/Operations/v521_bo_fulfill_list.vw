CREATE OR REPLACE VIEW v521_bo_fulfill_list AS
    SELECT
        c205_part_number_id,
        boqty,
        inshelf,
        CASE
            WHEN inshelf <= 0     THEN
                '3'
            WHEN inshelf < boqty  THEN
                '2'
            WHEN inshelf >= boqty THEN
                '1'
        END bopriority
    FROM
        (
            SELECT
                t502.c205_part_number_id,
                boqty,
                nvl(fg_avail_qty, 0) - nvl(c205_shelfalloc, 0) inshelf
            FROM
                (
                    SELECT
                        t521.c205_part_number_id,
                        SUM(t521.c521_qty) boqty
                    FROM
                        t520_request          t520,
                        t521_request_detail   t521
                    WHERE
                        t520.c520_request_id = t521.c520_request_id
                        AND t520.c520_status_fl = 10
                        AND t520.c520_request_to IS NOT NULL
                        AND t520.c5040_plant_id = nvl(sys_context('CLIENTCONTEXT', 'PLANT_ID'), get_rule_value('DEFAULT_PLANT', 'ONEPORTAL'
                        ))
                        AND t520.c520_void_fl IS NULL
                        AND t520.c207_set_id IS NULL
                        AND ( t520.c520_master_request_id IS NULL
                              OR t520.c520_master_request_id IN (
                            SELECT
                                c520_request_id
                            FROM
                                t520_request
                            WHERE
                                c207_set_id IS NULL
                        ) )
                    GROUP BY
                        t521.c205_part_number_id
                ) t502,
                (
                    SELECT
                        t205c.c205_part_number_id,
                        nvl(c205_available_qty, 0) fg_avail_qty
                    FROM
                        t205c_part_qty t205c
                    WHERE
                        c901_transaction_type = 90800
                        AND t205c.c5040_plant_id = nvl(sys_context('CLIENTCONTEXT', 'PLANT_ID'), get_rule_value('DEFAULT_PLANT', 'ONEPORTAL'
                        ))
                ) t205c,
                (
                    SELECT
                        t205.c205_part_number_id,
                        SUM(decode(v205c.c901_transaction_type, '60001', v205c.c205_allocated_qty, 0)) c205_shelfalloc
                    FROM
                        t205_part_number           t205,
                        v205c_part_allocated_qty   v205c
                    WHERE
                        v205c.c5040_plant_id = nvl(sys_context('CLIENTCONTEXT', 'PLANT_ID'), get_rule_value('DEFAULT_PLANT', 'ONEPORTAL'
                        ))
                        AND t205.c205_part_number_id = v205c.c205_part_number_id (+)
                    GROUP BY
                        t205.c205_part_number_id
                ) t205_alloc
            WHERE
                t502.c205_part_number_id = t205c.c205_part_number_id (+)
                AND t502.c205_part_number_id = t205_alloc.c205_part_number_id (+)
        );
/