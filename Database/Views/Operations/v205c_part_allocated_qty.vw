--@"C:\Database\Views\Operations\v205c_part_allocated_qty.sql";
/***********************************************************************
Filename: v205_inhouse_alloc_inv.vw
Component:View
Package:
Designer: Globus Medical
Developer: Aprasath
Version: 1.0
Description: Creating materialized view to get all the allocated quantity from below views.
V205_sales_alloc_inv, V205_consignment_alloc_inv, v205_inhouse_alloc_inv, v205_reserved_alloc_inv, V205_raw_material_alloc_inv
**********************************************************************/
CREATE OR REPLACE VIEW v205c_part_allocated_qty
AS
     SELECT c5040_plant_id, c205_part_number_id, SUM (c205_item_qty) c205_allocated_qty, c901_transaction_type
       FROM
        (
            --SALES
             SELECT v205.c5040_plant_id, v205.c205_part_number_id, t906.c906_rule_desc c901_transaction_type, SUM (v205.c502_item_qty)
                c205_item_qty
               FROM V205_sales_alloc_inv v205, t906_rules t906
              WHERE (v205.c901_ext_country_id IS NULL
                OR EXISTS
                (
                     SELECT country_id
                       FROM v901_country_codes --view to get country codes from t906_rules and used to view all the sales belongs to available countries in view
                      WHERE country_id = v205.c901_ext_country_id
                ))
                AND t906.c906_rule_grp_id = 'V205C_ALLOCATED_QTY'
                AND t906.c906_rule_id          = 'ORDER'
                AND NOT EXISTS
                (
                     SELECT c906_rule_value
                       FROM t906_rules 
                      WHERE c906_rule_value  = NVL (v205.c901_order_type, - 9999)
                        AND c906_rule_grp_id = 'ORDTYPE'
                        AND c906_rule_id          = 'EXCLUDE'
                )
           GROUP BY v205.c5040_plant_id, v205.c205_part_number_id, t906.c906_rule_desc
          UNION ALL
            --CONSIGNMENT
             SELECT v205.c5040_plant_id, v205.c205_part_number_id, t906.c906_rule_desc c901_transaction_type, SUM (v205.c505_item_qty) c205_item_qty
               FROM V205_consignment_alloc_inv v205, t906_rules t906
              WHERE NVL (v205.c901_warehouse_type, 999) = DECODE (get_rule_value ('CURRENT_DB', 'COUNTRYCODE'), 'en', DECODE
                (v205.c504_type, 4110, 90800, 4112, 90800, NVL (v205.c901_warehouse_type, 999)), NVL (v205.c901_warehouse_type, 999))
                AND t906.c906_rule_grp_id = 'V205C_ALLOCATED_QTY'
				AND t906.c906_rule_id ='CONSIGNMENT'
                AND t906.c906_rule_value       = TO_CHAR (v205.c504_type)
           GROUP BY v205.c5040_plant_id, v205.c205_part_number_id, t906.c906_rule_desc
          UNION ALL
            --INHOUSE
             SELECT v205.c5040_plant_id, v205.c205_part_number_id, t906.c906_rule_desc c901_transaction_type, SUM (NVL (v205.c413_item_qty, 0))
                c205_item_qty
               FROM v205_inhouse_alloc_inv v205, t906_rules t906
              WHERE t906.c906_rule_id         IN ('INHOUSE')
                AND t906.c906_rule_grp_id      = 'V205C_ALLOCATED_QTY'
                AND t906.c906_rule_value       = TO_CHAR (v205.c412_type)
           GROUP BY v205.c5040_plant_id, v205.c205_part_number_id, t906.c906_rule_desc
          UNION ALL
            --RESERVED
             SELECT v205.c5040_plant_id, v205.c205_part_number_id, t906.c906_rule_desc c901_transaction_type, v205.c5062_reserved_qty c205_item_qty
               FROM v205_reserved_alloc_inv v205, t906_rules t906
              WHERE t906.c906_rule_grp_id = 'V205C_ALLOCATED_QTY'
                AND t906.c906_rule_id          = 'RESERVED'
          UNION ALL
            --RAW MATERIAL
             SELECT c5040_plant_id, c205_part_number_id, t906.c906_rule_desc c901_transaction_type, NVL (SUM (Qty), 0) c205_item_qty
               FROM
                (
                     SELECT v205.c5040_plant_id, v205.c205_part_number_id, c304_item_qty qty
                       FROM V205_raw_material_alloc_inv v205
                  UNION ALL
                     SELECT v205_ih.c5040_plant_id, v205_ih.c205_part_number_id, SUM (NVL (c413_item_qty, 0)) qty
                       FROM V205_inhouse_alloc_inv v205_ih
                      WHERE c412_type IN (40051, 40052, 40053, 56025)
                   GROUP BY v205_ih.c5040_plant_id, v205_ih.c205_part_number_id
                )
              , t906_rules t906
              WHERE t906.c906_rule_grp_id = 'V205C_ALLOCATED_QTY'
                AND t906.c906_rule_id          = 'RAWMATERIAL'
           GROUP BY c5040_plant_id, c205_part_number_id, t906.c906_rule_desc
          UNION ALL
            --INHOUSE INVENTORY
             SELECT c5040_plant_id, c205_part_number_id, t906.c906_rule_desc c901_transaction_type, NVL (SUM (Qty), 0) c205_item_qty
               FROM
                (
                     SELECT v205_ih.c5040_plant_id, v205_ih.c205_part_number_id, SUM (NVL (v205_ih.c413_item_qty, 0)) qty
                       FROM V205_inhouse_alloc_inv v205_ih, t906_rules t906
                      WHERE v205_ih.c412_status_fl     IN (2, 3)
                        AND t906.c906_rule_grp_id       = 'V205C_ALLOCATED_QTY'
                        AND TO_CHAR (v205_ih.c412_type) = t906.c906_rule_value
                        AND t906.c906_rule_id           = 'IHWAREHOUSE'
                   GROUP BY v205_ih.c5040_plant_id, v205_ih.c205_part_number_id
                  UNION ALL
				  --the following query qill get part numbers from inhouse sets also. This will not available in our v205_consignment_alloc_inv
                     SELECT T504.c5040_plant_id, T505.C205_Part_Number_Id, NVL (SUM (T505.C505_Item_Qty), 0) Qty
                       FROM T504_Consignment T504, T505_Item_Consignment T505, T205_Part_Number T205
                      WHERE T504.C504_Status_Fl     IN (2, 3)
                        AND t504.c504_type          IN ('9110')
                        AND T504.C504_Consignment_Id = T505.C504_Consignment_Id
                        AND t504.C504_VOID_FL       IS NULL
                        AND T505.C505_Void_Fl       IS NULL
                        AND T205.C205_Part_Number_Id = T505.C205_Part_Number_Id
                   GROUP BY T504.c5040_plant_id, t505.c205_part_number_id
                )
              , t906_rules t906
              WHERE t906.c906_rule_grp_id = 'V205C_ALLOCATED_QTY'
                AND t906.c906_rule_id          = 'INHOUSEINV'
           GROUP BY c5040_plant_id, c205_part_number_id, t906.c906_rule_desc
          UNION ALL
            --RESTRICTED WAREHOUSE
             SELECT c5040_plant_id, c205_part_number_id, t906.c906_rule_desc c901_transaction_type, NVL (SUM (Qty), 0) c205_item_qty
               FROM
                (
                     SELECT v205_rw.c5040_plant_id, v205_rw.c205_part_number_id, SUM (NVL (c413_item_qty, 0)) qty
                       FROM v205_inhouse_alloc_inv v205_rw, t906_rules t906a, t906_rules t906B
                      WHERE c412_status_fl        IN (2, 3)
                        AND v205_rw.c412_type      = t906B.c906_rule_grp_id
                        AND t906A.c906_rule_grp_id = t906B.c906_rule_grp_id
                        AND t906a.c906_rule_id     = 'MINUS'
                        AND t906a.c906_rule_value  = 'QTY_IN_INV'
                        AND t906b.c906_rule_id     = 'TRANS_TABLE'
                        AND t906b.c906_rule_value  = 'IN_HOUSE'
                   GROUP BY v205_rw.c5040_plant_id, v205_rw.c205_part_number_id
                  UNION ALL
                     SELECT v205_cons.c5040_plant_id, v205_cons.c205_part_number_id, NVL (SUM (v205_cons.c505_item_qty), 0) qty
                       FROM v205_consignment_alloc_inv v205_cons
                      WHERE v205_cons.c504_type IN (4110, 4112)
                        AND c504_status_fl      IN (2, 3)
                        AND c901_warehouse_type  = 56001
                   GROUP BY v205_cons.c5040_plant_id, v205_cons.c205_part_number_id
                )
              , t906_rules t906
              WHERE t906.c906_rule_grp_id = 'V205C_ALLOCATED_QTY'
                AND t906.c906_rule_id          = 'RESTRICTEDWH'
           GROUP BY c5040_plant_id, c205_part_number_id, t906.c906_rule_desc
        )
        v205
   GROUP BY c5040_plant_id, c205_part_number_id, c901_transaction_type;
    /
