/* Formatted on 2009/11/03 17:42 (Formatter Plus v4.8.0) */
--@"C:\Database\Views\Operations\v5001_ cs_ today_process_info.vw";

/*
 * Description: This view will be used by 
 *              Braxtel Communications, USA
 *              to display the values in the phone system
 */
CREATE OR REPLACE VIEW v5001_cs_today_process_info (sales_dollar
											   , sales_count
											   , item_consignment
											   , set_consignment
											   , loaner_shipping_today
											   , open_loaner_request
											   , loaner_replenishment
												)
AS
	SELECT 
       -- Total Sales
       (
				SELECT NVL (SUM (T502.C502_CORP_ITEM_PRICE * T502.C502_ITEM_QTY), 0) 
		   FROM T501_ORDER T501, T502_ITEM_ORDER T502
		  WHERE T501.C501_ORDER_DATE                    = TRUNC (CURRENT_DATE)
			AND T501.C501_ORDER_ID                      = T502.C501_ORDER_ID
			AND NVL (T501.C901_ORDER_TYPE, - 9999) NOT IN ('2533')
			AND NVL (c901_order_type,      - 9999) NOT IN
			(
				SELECT t906.c906_rule_value
				   FROM t906_rules t906
				  WHERE t906.c906_rule_grp_id = 'ORDTYPE'
					AND c906_rule_id          = 'EXCLUDE'
			)
			AND NVL (c901_order_type, - 9999) NOT IN
			(
				 SELECT t906.c906_rule_value
				   FROM t906_rules t906
				  WHERE t906.c906_rule_grp_id = 'ORDERTYPE'
					AND c906_rule_id          = 'EXCLUDE'
			)
			AND T501.C703_SALES_REP_ID IN
			(
				SELECT DISTINCT REP_ID
				   FROM V700_TERRITORY_MAPPING_DETAIL
				  WHERE REP_COMPID NOT IN
					(
						 SELECT C906_RULE_ID
						   FROM T906_RULES
						  WHERE C906_RULE_GRP_ID = 'ADDNL_COMP_IN_SALES'
							AND C906_VOID_FL    IS NULL
					)
			)
			AND t501.c704_account_id IN
			(
				 SELECT ac_id
				   FROM v700_territory_mapping_detail
				  WHERE (COMPID                      = DECODE (100800, 100803, NULL, 100800)
					OR DECODE (100800, 100803, 1, 2) = 1)
			)
			AND t501.c703_sales_rep_id IN
			(
				 SELECT REP_ID FROM v700_territory_mapping_detail WHERE divid IN (100823)
			)
			AND T501.C501_VOID_FL   IS NULL
			AND T501.C501_DELETE_FL IS NULL
			AND T502.C502_VOID_FL   IS NULL 			
	    ) sales_dollar
        -- No of Orders
       , (
			SELECT COUNT(1) FROM (
				SELECT DISTINCT T501.c501_order_id
				   FROM T501_ORDER T501, T502_ITEM_ORDER T502
				  WHERE T501.C501_ORDER_DATE                    = TRUNC (CURRENT_DATE)
					AND T501.C501_ORDER_ID                      = T502.C501_ORDER_ID
					AND NVL (T501.C901_ORDER_TYPE, - 9999) NOT IN ('2533')
					AND NVL (c901_order_type,      - 9999) NOT IN
					(
						SELECT t906.c906_rule_value
						   FROM t906_rules t906
						  WHERE t906.c906_rule_grp_id = 'ORDTYPE'
							AND c906_rule_id          = 'EXCLUDE'
					)
					AND NVL (c901_order_type, - 9999) NOT IN
					(
						 SELECT t906.c906_rule_value
						   FROM t906_rules t906
						  WHERE t906.c906_rule_grp_id = 'ORDERTYPE'
							AND c906_rule_id          = 'EXCLUDE'
					)
					AND T501.C703_SALES_REP_ID IN
					(
						SELECT DISTINCT REP_ID
						   FROM V700_TERRITORY_MAPPING_DETAIL
						  WHERE REP_COMPID NOT IN
							(
								 SELECT C906_RULE_ID
								   FROM T906_RULES
								  WHERE C906_RULE_GRP_ID = 'ADDNL_COMP_IN_SALES'
									AND C906_VOID_FL    IS NULL
							)
					)
					AND t501.c704_account_id IN
					(
						 SELECT ac_id
						   FROM v700_territory_mapping_detail
						  WHERE (COMPID = DECODE (100800, 100803, NULL, 100800)
							OR DECODE (100800, 100803, 1, 2) = 1)
					)
					AND t501.c703_sales_rep_id IN
					(
						 SELECT REP_ID FROM v700_territory_mapping_detail WHERE divid IN (100823)
					)
					AND T501.C501_VOID_FL   IS NULL
					AND T501.C501_DELETE_FL IS NULL
					AND T502.C502_VOID_FL   IS NULL 
				)
	      ) sales_count
        -- No of item consignment
       , (SELECT COUNT (1)
            FROM t520_request t520, t504_consignment t504
           WHERE t520.c520_void_fl IS NULL
             AND t520.c520_request_id = t504.c520_request_id(+)
             AND t504.c504_status_fl IN (2.20, 3, 3.30, 3.60, 4)
             AND t520.c520_master_request_id IS NULL
             AND t520.c207_set_id IS NULL
             AND t520.c520_request_for = 40021
             AND t504.c504_last_updated_date BETWEEN TRUNC (SYSDATE) AND (SYSDATE)) item_consignment
        -- No of Set  consignment
       , (SELECT COUNT (1) set_consignment
            FROM t520_request t520, t504_consignment t504
           WHERE t520.c520_void_fl IS NULL
             AND t520.c520_request_id = t504.c520_request_id(+)
             AND t504.c504_status_fl IN (2.20, 3, 3.30, 3.60, 4)
             AND t520.c520_master_request_id IS NULL
             AND t520.c207_set_id IS NOT NULL
             AND t520.c520_request_for = 40021
             AND t504.c504_last_updated_date BETWEEN TRUNC (SYSDATE) AND (SYSDATE)) set_consignment
        -- No of loaners shipped today
       , (SELECT COUNT (1)
            FROM t526_product_request_detail t526
               , t504a_loaner_transaction t504a
               , t504a_consignment_loaner t504acl
           WHERE t526.c526_product_request_detail_id = t504a.c526_product_request_detail_id
             AND t526.c526_void_fl IS NULL
             AND t504a.c504a_void_fl IS NULL
             AND t526.c526_required_date = TRUNC (SYSDATE)
             AND t504a.c504_consignment_id = t504acl.c504_consignment_id
             AND t504acl.c504a_status_fl IN (16, 20)) loaner_shipping_today
        -- No of loaners Pending shipping
       , (SELECT setdetails.setcount+requestdetails.requestcount FROM 
           (SELECT count(1) setcount
            FROM t526_product_request_detail t526
               , t504a_loaner_transaction t504a
               , t504a_consignment_loaner t504acl
           WHERE t526.c526_product_request_detail_id = t504a.c526_product_request_detail_id
             AND t526.c526_void_fl IS NULL
             AND t504a.c504a_void_fl IS NULL
             AND t526.c526_required_date = TRUNC (SYSDATE)
             AND t504a.c504_consignment_id = t504acl.c504_consignment_id
             AND t504acl.c504a_status_fl IN (7,13))setdetails,   --Pending Pick status(Loaner set)--7, PIP status--13                      
             (SELECT count(1) requestcount
            FROM t526_product_request_detail t526, t504a_loaner_transaction t504a
           WHERE t526.c526_product_request_detail_id = t504a.c526_product_request_detail_id(+)
             AND t526.c526_void_fl IS NULL
             AND t504a.c504a_void_fl IS NULL
             AND t526.c526_required_date = TRUNC (SYSDATE)
             AND t526.c207_set_id is not null
             AND t526.c526_status_fl IN (5, 10)) requestdetails) open_loaner_request
        -- No of Loaners Replenishment's
       , (SELECT COUNT (1)
            FROM t412_inhouse_transactions t412
           WHERE t412.c412_type = 50154   -- FGLE
             AND t412.c412_void_fl IS NULL
             AND t412.c412_created_date BETWEEN TRUNC (SYSDATE) AND (SYSDATE)) loaner_replenishment
  FROM DUAL
/