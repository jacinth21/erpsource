--@"C:\Database\Views\Operations\v1600_issuing_agency_symbol.vw";
CREATE OR REPLACE VIEW v1600_issuing_agency_symbol
AS
     SELECT C1600_ISSUING_AGENCY_ID, C1601_ISSUE_AG_IDENTIFIER_ID, C1601_IDENTIFY_SYMBOL
      , SUBSTR (SYS_CONNECT_BY_PATH (TRANS_ID, '^'), 2) IDENTIFIER_SYMBOL_ID, SUBSTR (SYS_CONNECT_BY_PATH (TRANS_NAME,
        '^'), 2) IDENTIFIER_SYMBOL_NAME
       FROM
        (
             SELECT T1601.C1600_ISSUING_AGENCY_ID, T1602.C1601_ISSUE_AG_IDENTIFIER_ID, C1601_IDENTIFY_SYMBOL
              , T1602.C901_IDENTIFIER||DECODE (T1602.C1602_IDENTIFIER_FORMAT, NULL, NULL, '['||
                T1602.C1602_IDENTIFIER_FORMAT||']') TRANS_ID, get_code_name (T1602.C901_IDENTIFIER) ||DECODE (
                T1602.C1602_IDENTIFIER_FORMAT, NULL, NULL, '['||T1602.C1602_IDENTIFIER_FORMAT||']') TRANS_NAME,
                ROW_NUMBER () over (partition BY T1602.C1601_ISSUE_AG_IDENTIFIER_ID order by
                C1602_ISSUE_AGENCY_SYMBOL_ID) SEQ, COUNT ( *) over (partition BY T1602.C1601_ISSUE_AG_IDENTIFIER_ID)
                cnt
               FROM T1601_ISSUE_AGENCY_IDENTIFIER T1601, T1602_ISSUE_AGENCY_SYMBOL_DTL T1602
              WHERE T1601.C1601_ISSUE_AG_IDENTIFIER_ID = T1602.C1601_ISSUE_AG_IDENTIFIER_ID
                --and T1601.C1601_ISSUE_AG_IDENTIFIER_ID = 8
                AND T1601.C1601_VOID_FL IS NULL
                AND T1602.C1602_VOID_FL IS NULL
        )
        inn
      WHERE seq                                    = cnt
        START WITH seq                             = 1
        CONNECT BY prior seq + 1                   = seq
        AND prior inn.c1601_issue_ag_identifier_id = c1601_issue_ag_identifier_id;