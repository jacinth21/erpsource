--@"C:\Database\Views\Operations\V205_raw_material_alloc_inv.sql";
/***********************************************************************
Filename: V205_raw_material_alloc_inv.vw
Component:View
Package:
Designer: Globus Medical
Developer: Aprasath
Version: 1.0
Description: Creating materialized view to get all the raw material transaction related allocated quantity. On Commit fast
refresh used to refresh data automatically whenever transaction happend in  t303_material_request,
t304_material_request_item
**********************************************************************/
DROP materialized VIEW log ON t303_material_request;
DROP materialized VIEW log ON t304_material_request_item;
DROP materialized VIEW V205_raw_material_alloc_inv;
-- materialized view log is used to create a log of activity on a materialized view�s base table
CREATE materialized VIEW log ON t303_material_request
NOLOGGING
WITH rowid, sequence (c303_material_request_id, c303_status_fl, c303_void_fl) including NEW VALUES;
CREATE materialized VIEW log ON t304_material_request_item
NOLOGGING
WITH rowid, sequence (c303_material_request_id, c205_part_number_id, c304_item_qty) including NEW VALUES;


-- COUNT(*) must always be present to guarantee all types of fast refresh. Otherwise, you may be limited to fast refresh after inserts only
-- COUNT(expr) should required when used SUM(expr) used in fast refresh materialized view

CREATE materialized VIEW V205_raw_material_alloc_inv
NOLOGGING
REFRESH FAST ON COMMIT
AS
     SELECT 3000 c5040_plant_id, t304.c205_part_number_id, SUM (t304.c304_item_qty) c304_item_qty, COUNT ( *) cnt
      , COUNT (t304.c304_item_qty) qtycnt
       FROM t303_material_request t303, t304_material_request_item t304
      WHERE t303.c303_material_request_id = t304.c303_material_request_id
        AND t303.c303_status_fl           < 40
        AND t303.c303_void_fl            IS NULL
        AND t304.c304_item_qty           IS  NOT NULL
   GROUP BY t304.c205_part_number_id;


			   