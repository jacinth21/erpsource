/*
File name: v4082_Stage_Dashboard.vw
Created By: MAzarudeen
PMT # :- PMT-43224
Description: Stage Dashboard
*/

CREATE OR REPLACE VIEW  v4082_Stage_Dashboard AS
SELECT txn_id, donor_number, part_count
  , qty_received,  due_date
  , labels_printed, pre_staging, staging
  , pi
   FROM
    (
         SELECT dhr.dhr_id txn_id, t2540.c2540_donor_number donor_number, part_count
          , qty_received, dhr.due_date due_date, to_number ( (
            CASE
                WHEN v4081a.labels_printed = 'Y'
                THEN '1'
                ELSE '0'
            END), '9999d99', 'nls_numeric_characters=,.') labels_printed, to_number ( (
            CASE
                WHEN v4081a.label_pre_stage = 'Y'
                THEN '1'
                ELSE '0'
            END), '9999d99', 'nls_numeric_characters=,.') pre_staging, to_number ( (
            CASE
                WHEN v4081a.label_stage = 'Y'
                THEN '1'
                ELSE '0'
            END), '9999d99', 'nls_numeric_characters=,.') staging, dhr.pi pi
           FROM t4081_dhr_bulk_receive t4081, v4081a_dhr_bulk_attribute v4081a, t2540_donor_master t2540
          , (
                 SELECT t4082.c4081_dhr_bulk_id DHR_ID, COUNT ( t408.c205_part_number_id) part_count, SUM (
                    t408.c408_qty_received) qty_received, SUM (t408.c408_qty_rejected) qty_rejected, to_number ( (
                    CASE
                        WHEN t408.c408_status_fl > 1 -- Bug Fix PMT-46916
                        THEN '1'
                        ELSE '0'
                    END), '9999d99', 'nls_numeric_characters=,.') pi, TO_CHAR (t4081a.c4081_stage_date, 'mm/dd/yyyy')
                    due_date
                   FROM t4082_bulk_dhr_mapping t4082, t408_dhr t408, t4081_dhr_bulk_receive t4081a
                  WHERE t408.c408_dhr_id         = t4082.c408_dhr_id
                    AND t408.c408_status_fl     != 4
                    AND t4082.c4082_void_fl     IS NULL
                    AND t4081a.c4081_dhr_bulk_id = t4082.c4081_dhr_bulk_id
                    AND t4081a.c4081_void_fl    IS NULL
                    AND t408.c408_void_fl       IS NULL
                    AND t408.c1900_company_id    = '1001'
               GROUP BY t4082.c4081_dhr_bulk_id, t408.c408_status_fl,t4081a.c4081_stage_date
            )
            dhr
          WHERE t4081.c4081_dhr_bulk_id = v4081a.c4081_dhr_bulk_id (+)
            AND t4081.c2540_donor_id    = t2540.c2540_donor_id(+)
            AND t4081.c4081_dhr_bulk_id = dhr.DHR_ID
            AND t4081.c4081_stage_date <= CURRENT_DATE
            AND t4081.c4081_stage_date IS NOT NULL
            AND t4081.c4081_void_fl    IS NULL
            AND t4081.c1900_company_id  = '1001'
            AND t2540.c2540_void_fl    IS NULL
      UNION ALL
        -- PTRD transaction query
         SELECT t412.c412_inhouse_trans_id txn_id, t412.c2540_donor_number donor_number, COUNT (
             t413.c205_part_number_id) part_count, SUM ( t413.c413_item_qty) qty_received, TO_CHAR (MIN (
            t412.c412_stage_date), 'mm/dd/yyyy') due_date, to_number ( (
            CASE
                WHEN v412a.labels_printed = 'Y'
                THEN '1'
                ELSE '0'
            END), '9999d99', 'nls_numeric_characters=,.') labels_printed, to_number ( (
            CASE
                WHEN v412a.label_pre_stage = 'Y'
                THEN '1'
                ELSE '0'
            END), '9999d99', 'nls_numeric_characters=,.') pre_staging, to_number ( (
            CASE
                WHEN v412a.label_stage = 'Y'
                THEN '1'
                ELSE '0'
            END), '9999d99', 'nls_numeric_characters=,.') staging, to_number ( (
            CASE
                WHEN t412.c412_status_fl > 1
                THEN '1'
                ELSE '0'
            END), '9999d99', 'nls_numeric_characters=,.') pi
           FROM t412_inhouse_transactions t412, t413_inhouse_trans_items t413, v412a_inhouse_trans_attribute v412a
          WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
            AND t412.c412_status_fl        < 4
            AND t412.c412_type             = 103932 --part re-designation
            AND t412.c412_inhouse_trans_id = V412a.C412_Inhouse_Trans_Id(+)
            AND t412.c412_stage_date      IS NOT NULL
            AND t412.c1900_company_id      = '1001'
            AND c412_void_fl              IS NULL
            AND t412.c412_stage_date      <= CURRENT_DATE
       GROUP BY t412.c412_inhouse_trans_id, t412.c2540_donor_number, v412a.labels_printed
          , v412a.qc_verified, t412.c412_status_fl, t412.c412_stage_date
          , v412a.label_pre_stage, v412a.label_stage, t412.c412_type
    )
    stage
ORDER BY stage.due_date ;
    /