--@"C:\database\Views\ProdDevelopement\v205g_part_pd_parameter.vw";

CREATE OR REPLACE VIEW v205g_part_pd_parameter
AS
	 SELECT t205d.c205_part_number_id, t205.c202_project_id projectId, t205.c205_part_num_desc part_desc
  	, t205d.c205d_contains_latex
	, t205d.c205d_require_steril, t205d.c205d_steril_method, t205d.c205d_device_available, t205d.c205d_size1_type
	, t205d.c205d_size1_type_text, t205d.c205d_size1_value, t205d.c205d_size1_uom, t205d.c205d_size2_type, t205d.c205d_size2_type_text
	, t205d.c205d_size2_value, t205d.c205d_size2_uom, t205d.c205d_size3_type, t205d.c205d_size3_type_text, t205d.c205d_size3_value
	, t205d.c205d_size3_uom, t205d.c205d_size4_type, t205d.c205d_size4_type_text, t205d.c205d_size4_value, t205d.c205d_size4_uom
	, t205.c901_status_id part_status,t205d.c205d_serial_num_need
   FROM t205_part_number t205,
    (
     SELECT c205_part_number_id, 
        --PD Parameters
        MAX (DECODE (c901_attribute_type, 104460, c205d_attribute_value)) c205d_contains_latex,
        MAX (DECODE (c901_attribute_type, 104461, c205d_attribute_value)) c205d_require_steril,
        MAX (DECODE (c901_attribute_type, 104462, c205d_attribute_value)) c205d_steril_method,
        MAX (DECODE (c901_attribute_type, 104463, c205d_attribute_value)) c205d_device_available,
        MAX (DECODE (c901_attribute_type, 104464, c205d_attribute_value)) c205d_size1_type,
        MAX (DECODE (c901_attribute_type, 104465, c205d_attribute_value)) c205d_size1_type_text,
        MAX (DECODE (c901_attribute_type, 104466, c205d_attribute_value)) c205d_size1_value,
        MAX (DECODE (c901_attribute_type, 104467, c205d_attribute_value)) c205d_size1_uom,
        MAX (DECODE (c901_attribute_type, 104468, c205d_attribute_value)) c205d_size2_type,
        MAX (DECODE (c901_attribute_type, 104469, c205d_attribute_value)) c205d_size2_type_text,
        MAX (DECODE (c901_attribute_type, 104470, c205d_attribute_value)) c205d_size2_value,
        MAX (DECODE (c901_attribute_type, 104471, c205d_attribute_value)) c205d_size2_uom,
        MAX (DECODE (c901_attribute_type, 104472, c205d_attribute_value)) c205d_size3_type,
        MAX (DECODE (c901_attribute_type, 104473, c205d_attribute_value)) c205d_size3_type_text,
        MAX (DECODE (c901_attribute_type, 104474, c205d_attribute_value)) c205d_size3_value,
        MAX (DECODE (c901_attribute_type, 104475, c205d_attribute_value)) c205d_size3_uom,
        MAX (DECODE (c901_attribute_type, 104476, c205d_attribute_value)) c205d_size4_type,
        MAX (DECODE (c901_attribute_type, 104477, c205d_attribute_value)) c205d_size4_type_text,
        MAX (DECODE (c901_attribute_type, 104478, c205d_attribute_value)) c205d_size4_value,
        MAX (DECODE (c901_attribute_type, 104479, c205d_attribute_value)) c205d_size4_uom,
        MAX (DECODE (c901_attribute_type, 104480, c205d_attribute_value)) c205d_serial_num_need
FROM MV205G_PART_PD_PARAMETER WHERE C205d_VOID_FL IS NULL GROUP BY c205_part_number_id) t205d
       WHERE t205.c205_part_number_id = t205d.c205_part_number_id;