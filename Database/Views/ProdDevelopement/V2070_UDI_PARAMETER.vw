--@"C:\database\Views\ProdDevelopement\V2070_UDI_PARAMETER.vw";

 /************************************************************************
    * Description : This View used for UDI submission xml generation and  UDI Review dashboard
    * Author   : Paddy
    ************************************************************************/
/*      Column name starts with ME* are GMDN mandatory attributes 
        Column name starts with DE* are Data elements from T205d Attribute
        Column name starts with ODE* are Optional Data elements*/
CREATE OR REPLACE VIEW V2070_UDI_PARAMETER
AS
    (
    SELECT 
        GDSNMandate.informationProvider AS ME2_INFOPROVIDER ,
        C2060_DI_NUMBER                     AS DE2_PRIMARY_DI_NUMBER ,
        GDSNMandate.targetMarketCountryCode AS ME4_TGT_MKT_COUNTRY_CODE ,
        GDSNMandate.tradeItemUnitDescriptor AS ME5_TRADE_ITEM_UNIT ,
        C2060_DI_NUMBER                     AS DE2_GTIN_NAME ,
        GDSNMandate.informationProvider ME2_BRANDOWNER_GLN ,
        Brand_Num                              AS DE8_Brand_Num ,
        GDSNMandate.classificationCategoryCode AS ME22_CLASSIFICATION_CODE ,
        GDSNMandate.functionalName             AS ME21_FUNCTIONAL_NAME ,
        GDSNMandate.isTradeItemAConsumerUnit   AS ME7_IS_CONSUMER_UNIT ,
        GDSNMandate.isTradeItemABaseUnit       AS ME6_IS_BASE_UNIT ,
        GDSNMandate.isTradeItemAnOrderableUnit AS ME8_IS_ORDERABLE_UNIT ,
        GDSNMandate.isTradeItemADespatchUnit   AS ME10_IS_DESPATCH_UNIT ,
        GDSNMANDATE.ISTRADEITEMANINVOICEUNIT   AS ME9_IS_INVOICE_UNIT ,
        NVL(LOTBATCH.C902_CODE_NM_ALT,'N') DE41_LOT_BATCH,
        GDSNMandate.netContent     AS ME19_NET_CONTENT ,
        GDSNMandate.netContentUoM  AS ME20_NET_CONTENT_UOM ,
        /*MJR3 change: PMT-7655
          GDSNMandate.grossWeight    AS ME11_GROSS_WEIGHT ,
        GDSNMandate.grossWeightUoM AS ME12_GROSS_WEIGHT_UOM ,
        GDSNMandate.depth          AS ME17_GROSS_DEPTH ,
        GDSNMandate.depthUoM       AS ME18_GROSS_DEPTH_UOM ,
        GDSNMandate.height         AS ME13_GROSS_HEIGHT ,
        GDSNMandate.heightUoM      AS ME14_GROSS_HEIGHT_UOM ,
        GDSNMandate.width          AS ME15_GROSS_WIDTH ,
        GDSNMandate.widthUoM       AS ME16_GROSS_WIDTH_UOM,*/
        XDATE.XML_DATE             AS ME23_START_AVAILABILITY_DATE ,
        DECODE(GUDID_PUBLISH_FL,'Y',To_char(FDA_PUBLISH_DATE,'YYYY-MM-DD')||'T'||to_char(FDA_PUBLISH_DATE,'HH24:MI:SS'),
        NVL(GDSNMANDATE.HARD_PUBLISH_DT,(to_char(SYSDATE+GDSNMANDATE.NTH_PUBLISH_DT,'YYYY-MM-DD')||'T'||to_char(SYSDATE,'HH24:MI:SS'))))as DE12_FDAGUDIDPublishDate ,  
        NULL                                AS ODE13_COMMERCIAL_END_DATE ,
        PACKAGE_DISCONTINUE_DATE DE24_PACKAGE_DISCONTINUE_DATE ,
        NVL(QUANTITY_PER_PACKAGE ,1) AS DE21_QUANTITY_PER_PACKAGE ,
        'MNO'                        AS DE9_MODEL_NUMBER_P ,
        C205_PART_NUMBER_ID          AS DE9_MODEL_NUMBER_S ,
        LABELER_DUNS DE5_LABELER_DUNS_NUMBER_B ,
        NULL DE5_LABELER_DUNS_NUMBER_M ,
        C205_PART_NUM_DESC AS ME24_PRODUCT_DESCRIPTION ,
        /* Product Desc - Prt Num desc query */
        NULL AS ODE11_DEVICE_DESCRIPTION ,
        CONTACT_TYPE Contact_type ,-- PMT-7655 MJr3; moved the harded coded value to rule and updated rule value from CUSTOMER_SUPPORT to CXC or CYC
        'EMAIL'                AS LBL_COM_CH11_EMAIL ,
        CUSTOMER_CONTACT_EMAIL AS DE27_CUSTOMER_CONTACT_EMAIL ,
        'TELEPHONE'            AS LBL_COM_CH1_PHONE ,
        CUSTOMER_CONTACT_PHONE DE26_CUSTOMER_CONTACT_PHONE ,
        NULL                                              AS ODE23_PACKAGE_TYPE ,
        GMDN                                              AS DE37_GMDNCODE ,
        'GMDN'                                            AS DE37_GMDNCODE_L ,
        NULL                                              AS ODE47_DEVICE_CONTAIN_LATEX ,
        get_rule_value(EXPIRATION_DATE,'UDI_EXPIRY_DATE') AS DE44_EXPIRATION_DATE,
        NULL HUMANBLOODDERIVATIVE ,
        CONTAIN_LAT.C902_CODE_NM_ALT                                               AS DE46_NATURAL_RUBBER_LATEX,
        DECODE(FOR_SINGLE_USE.C902_CODE_NM_ALT,'Y' ,'SINGLE_USE' ,'N' ,'REUSABLE') AS DE40_FOR_SINGLE_USE ,
        NULL REUSABLE_CYCLE ,
        UNIT_DI_NUMBER                                                                            AS DE4_UNIT_DI_NUMBER ,
        DEVICE_SUBJECT_DM.C902_CODE_NM_ALT                                                        AS DE15_DEVICE_SUBJECT_DM ,
        DM_DI_NUMBER                                                                              AS DE17_DM_DI_Number ,
        DEV_EXEMPT_FROM_PRE_MARKET.C902_CODE_NM_ALT                                               AS DE31_DEV_EXEMPT_FROM_PREMARKET ,
        FDA_LIST                                                                                  AS DE36_FDA_LISTING_NUMBER ,
        DECODE(NVL(SERIAL_NUMBER.C902_CODE_NM_ALT,'N') ,'Y' ,'MARKED_ON_PACKAGING' ,'NOT_MARKED') AS DE43_SERIAL_NUMBER ,
        NULL                                                                                      AS ODE48_PRESCRIPTION_USE ,
        WHAT_MRI_SAFETY.C902_CODE_NM_ALT DE50_MRI_SAFETY_STATUS ,
        get_rule_value(TYPE1 ,'UDI_SIZE_TYPE') DE51_TYPE1 ,
        VALUE1                                 AS DE52_VALUE1 ,
        UOM1.C901_CONTROL_TYPE                 AS DE53_UOM1 ,
        OTHER1                                 AS DE54_SIZE_TYPE_TEXT1 ,
        get_rule_value(TYPE2 ,'UDI_SIZE_TYPE') AS DE51_TYPE2,
        VALUE2                                 AS DE52_VALUE2 ,
        UOM2.C901_CONTROL_TYPE                 AS DE53_UOM2,
        OTHER2                                 AS DE54_SIZE_TYPE_TEXT2 ,
        get_rule_value(TYPE3 ,'UDI_SIZE_TYPE') AS DE51_TYPE3 ,
        VALUE3                                 AS DE52_VALUE3 ,
        UOM3.C901_CONTROL_TYPE                 AS DE53_UOM3,
        OTHER3                                 AS DE54_SIZE_TYPE_TEXT3 ,
        get_rule_value(TYPE4 ,'UDI_SIZE_TYPE') AS DE51_TYPE4 ,
        VALUE4                                 AS DE52_VALUE4 ,
        UOM4.C901_CONTROL_TYPE                 AS DE53_UOM4,
        OTHER4                                 AS DE54_SIZE_TYPE_TEXT4 ,
        NULL                                   AS ODE56_STORAGE_PLOW_VALUE ,
        NULL                                   AS ODE58_STORAGE_PLOW_UOM ,
        NULL                                   AS ODE57_STORAGE_PMAX_VALUE ,
        NULL                                   AS ODE58_STORAGE_PMAX_UOM ,
        NULL                                   AS ODE56_STORAGE_TLOW_VALUE ,
        NULL                                   AS ODE58_STORAGE_TLOW_UOM ,
        NULL                                   AS ODE57_STORAGE_TMAX_VALUE ,
        NULL                                   AS ODE58_STORAGE_TMAX_UOM ,
        NULL                                   AS ODE56_STORAGE_HLOW_VALUE ,
        NULL                                   AS ODE58_STORAGE_HLOW_UOM ,
        NULL                                   AS ODE57_STORAGE_HMAX_VALUE ,
        NULL                                   AS ODE58_STORAGE_HMAX_UOM ,
        NULL                                   AS ODE56_TRANS_EN_PLOW_VALUE ,
        NULL                                   AS ODE58_TRANS_EN_PLOW_UOM ,
        NULL                                   AS ODE57_TRANS_EN_PMAX_VALUE ,
        NULL                                   AS ODE58_TRANS_EN_PMAX_UOM ,
        NULL                                   AS ODE56_TRANS_HLOW_VALUE ,
        NULL                                   AS ODE58_TRANS_HLOW_UOM ,
        NULL                                   AS ODE57_TRANS_HMAX_VALUE ,
        NULL                                   AS ODE58_TRANS_HMAX_UOM ,
        NULL                                   AS ODE56_TRANS_TLOW_VALUE ,
        NULL                                   AS ODE58_TRANS_TLOW_UOM ,
        NULL                                   AS ODE57_TRANS_TMAX_VALUE ,
        NULL                                   AS ODE58_TRANS_TMAX_UOM ,
        NULL                                   AS ODE59_SPECIAL_STORAGE_COND ,
        NULL                                   AS ODE59_SPECIAL_STORAGE_LANG ,
        -- get_rule_value (STERILIZATION_METHOD ,'PD_STERIL_METHOD') as DE62_STERILIZATION_METHOD ,
        DECODE(C205_PRODUCT_CLASS,'4031',get_rule_value(STERILIZATION_METHOD,'PD_STERIL_METHOD'),NULL) DE62_STERILIZATION_METHOD,      ----Non-Sterile
        DECODE(C205_PRODUCT_CLASS,'4030',get_rule_value(STERILIZATION_METHOD,'PD_STERIL_METHOD'),NULL) AS DE60_DEVICE_PACKAGED_STERILE , --Sterile
        DECODE(NVL(MANUFACTURING_DATE.C902_CODE_NM_ALT,'N')  ,'Y' ,'PRODUCTION_DATE' ,NULL)            AS DE42_MANUFACTURING_DATE ,-- PMT-7655 MJr3
        'DUNS'                                                                                         AS LBL_LABELER_DUNS ,
        LABELER_DUNS                                                                                   AS DE5_LABELER_DUNS_NUMBER ,
        HUMAN_CELL_TISSUE.C902_CODE_NM_ALT                                                             AS DE28_HCTP ,
        NULL                                                                                           AS ODE32_KIT_COMBINATION_PROD ,
        NULL                                                                                           AS PRODUCTRANGE ,
        NVL(DONATION_IDENTIFICATION.C902_CODE_NM_ALT ,'N')                                             AS DE45_DONATION_IDENTIFICATION ,
        DEVICE_COUNT                                                                                   AS DE3_DEVICE_COUNT ,
        XML_REPRESENTEDPARTY ,
        XML_RECEIVER ,
        XML_BATCHUSERID ,
        XDATE.XML_DATE AS XML_EFFECTIVEDATE ,
        XDATE.XML_DATE AS XML_CREATIONDATE ,
        DI_INTERNAL_SUBMIT_STATUS STATUS_ID,
        DI_INTERNAL_SUBMIT_STATUS.C901_CODE_NM STATUS_NAME,
        DI_EXTERNAL_SUBMIT_STATUS external_status_id,
        DI_EXTERNAL_SUBMIT_STATUS.C901_CODE_NM external_status_name,
        ME25_DATA_CARRIER  --MJR3 Change PMT-7655
    FROM V906_GDSNMANDATE GDSNMANDATE,
	 V2070_UDI_DBDATA_ATTRI DB_Data ,
        (SELECT TO_CHAR(SYSDATE ,'YYYY-MM-DD')||'T'||TO_CHAR(SYSDATE ,'HH24:MI:SS')XML_DATE FROM DUAL) XDATE,
        T901_CODE_LOOKUP LOTBATCH,
        T901_CODE_LOOKUP CONTAIN_LAT,
        T901_CODE_LOOKUP FOR_SINGLE_USE,
        T901_CODE_LOOKUP DEVICE_SUBJECT_DM,
        T901_CODE_LOOKUP DEV_EXEMPT_FROM_PRE_MARKET,
        T901_CODE_LOOKUP SERIAL_NUMBER,
        T901_CODE_LOOKUP WHAT_MRI_SAFETY,
        T901_CODE_LOOKUP UOM1,
        T901_CODE_LOOKUP UOM2,
        T901_CODE_LOOKUP UOM3,
        T901_CODE_LOOKUP UOM4,
        T901_CODE_LOOKUP MANUFACTURING_DATE,
        T901_CODE_LOOKUP HUMAN_CELL_TISSUE,
        T901_CODE_LOOKUP DONATION_IDENTIFICATION,
        T901_CODE_LOOKUP DI_INTERNAL_SUBMIT_STATUS,
        T901_CODE_LOOKUP DI_EXTERNAL_SUBMIT_STATUS
    WHERE DB_Data.LOT_BATCH               =LOTBATCH.C901_CODE_ID(+)
    AND DB_Data.CONTAIN_LAT               =CONTAIN_LAT.C901_CODE_ID(+)
    AND DB_Data.FOR_SINGLE_USE            =FOR_SINGLE_USE.C901_CODE_ID(+)
    AND DB_Data.DEVICE_SUBJECT_DM         =DEVICE_SUBJECT_DM.C901_CODE_ID(+)
    AND DB_Data.DEV_EXEMPT_FROM_PRE_MARKET=DEV_EXEMPT_FROM_PRE_MARKET.C901_CODE_ID(+)
    AND DB_Data.SERIAL_NUMBER             =SERIAL_NUMBER.C901_CODE_ID(+)
    AND DB_Data.WHAT_MRI_SAFETY           =WHAT_MRI_SAFETY.C901_CODE_ID(+)
    AND DB_Data.UOM1                      =UOM1.C901_CODE_ID(+)
    AND DB_Data.UOM2                      =UOM2.C901_CODE_ID(+)
    AND DB_Data.UOM3                      =UOM3.C901_CODE_ID(+)
    AND DB_Data.UOM4                      =UOM4.C901_CODE_ID(+)
    AND DB_Data.MANUFACTURING_DATE        =MANUFACTURING_DATE.C901_CODE_ID(+)
    AND DB_Data.HUMAN_CELL_TISSUE         =HUMAN_CELL_TISSUE.C901_CODE_ID(+)
    AND DB_Data.DONATION_IDENTIFICATION   =DONATION_IDENTIFICATION.C901_CODE_ID(+)
    AND DB_Data.DI_INTERNAL_SUBMIT_STATUS =DI_INTERNAL_SUBMIT_STATUS.C901_CODE_ID(+)
    AND DB_Data.DI_EXTERNAL_SUBMIT_STATUS =DI_EXTERNAL_SUBMIT_STATUS.C901_CODE_ID(+)
    AND C2060_DI_NUMBER                  IS NOT NULL
    );
