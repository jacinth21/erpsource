CREATE OR REPLACE FORCE VIEW "V2070_UDI_MULTI_ATTRIBUTES" ("C205_PART_NUMBER_ID", "C2060_DI_NUMBER", "C906_RULE_ID", "C205D_ATTRIBUTE_VALUE", "C906_RULE_GRP_ID") AS 
select T2060.C205_PART_NUMBER_ID, T2060.C2060_DI_NUMBER, T906.C906_RULE_ID, T9031.C9031_ATTRIBUTE_VALUE , T906.C906_RULE_GRP_ID
    from T906_RULES T906, T9031_JOB_SOURCE_SYSTEM T9031, T2072_DI_GUDID_STATUS T2072 , T2060_DI_PART_MAPPING T2060
    where T906.C906_RULE_ID = T9031.C9031_ATTRIBUTE_TYPE 
    and T9031.C9031_REF_ID = T2072.C2060_DI_NUMBER
    and T9031.C9030_JOB_ACTIVITY_ID = T2072.C9030_SOURCE_ACTIVITY_ID
    and T2060.C2060_DI_NUMBER = T2072.C2060_DI_NUMBER 
    AND T906.C906_RULE_GRP_ID='DE34_FDA_PRODUCTCODE'
    and T906.C906_VOID_FL is null
    and T9031.C9031_ATTRIBUTE_VALUE is not null
    --and T2060.C2060_DI_NUMBER='00889095179071'
    UNION ALL
    SELECT C205_PART_NUMBER_ID,DE2_PRIMARY_DI_NUMBER,C906_RULE_ID C906_RULE_ID,DE37_GMDNCODE_P AS C9031_ATTRIBUTE_VALUE,'DE32_FDA_SUB_NUM' AS C906_RULE_GRP_ID
    FROM
    (select C205_PART_NUMBER_ID,DE2_PRIMARY_DI_NUMBER
         ,max(case when dummy_row_id = 1 then SUB1 || (case when SUP1 is not null then ':'|| SUP1 end)
                when dummy_row_id = 2 then SUB2 || (case when SUP2 is not null then ':'|| SUP2  end)
                when dummy_row_id = 3 then SUB3 || (case when SUP3 is not null then ':'|| SUP3  end)
                when dummy_row_id = 4 then SUB4 || (case when SUP4 is not null then ':'|| SUP4  end)
                when dummy_row_id = 5 then SUB5 || (case when SUP5 is not null then ':'|| SUP5  end)
                 when dummy_row_id = 6 then SUB6 || (case when SUP6 is not null then ':'|| SUP6  end)
                  when dummy_row_id = 7 then SUB7 || (case when SUP7 is not null then ':'|| SUP7  end)
                   when dummy_row_id = 8 then SUB8 || (case when SUP8 is not null then ':'|| SUP8  end)
                    when dummy_row_id = 9 then SUB9 || (case when SUP9 is not null then ':'|| SUP9  end)
                     when dummy_row_id = 10 then SUB10 || (case when SUP10 is not null then ':'|| SUP10  end)
                         end) DE37_GMDNCODE_P
        ,max(case when dummy_row_id = 1 then SUB100  
                when dummy_row_id = 2 then SUB200 
                when dummy_row_id = 3 then SUB300 
                when dummy_row_id = 4 then SUB400 
                when dummy_row_id = 5 then SUB500 
                 when dummy_row_id = 6 then SUB600 
                  when dummy_row_id = 7 then SUB700 
                   when dummy_row_id = 8 then SUB800 
                    when dummy_row_id = 9 then SUB900 
                     when dummy_row_id = 10 then SUB1000 end ) C906_RULE_ID
    FROM (
    SELECT C205_PART_NUMBER_ID , DE2_PRIMARY_DI_NUMBER
    ,MAX( decode(attribute_type,'104643',attribute_value,null)) SUB1
    ,MAX(decode(attribute_type,'104644',attribute_value,null)) SUB2
    ,MAX(decode(attribute_type,'104645',attribute_value,null)) SUB3
    ,MAX(decode(attribute_type,'104646',attribute_value,null)) SUB4
    ,MAX(decode(attribute_type,'104647',attribute_value,null)) SUB5
    ,MAX(decode(attribute_type,'104648',attribute_value,null)) SUB6
    ,MAX(decode(attribute_type,'104649',attribute_value,null)) SUB7
    ,MAX(decode(attribute_type,'104650',attribute_value,null)) SUB8
    ,MAX(decode(attribute_type,'104651',attribute_value,null)) SUB9
    ,MAX(decode(attribute_type,'104652',attribute_value,null)) SUB10
    ,MAX(decode(attribute_type,'104656',attribute_value,null)) Sup1
    ,MAX(decode(attribute_type,'104657',attribute_value,null)) Sup2
    ,MAX(decode(attribute_type,'104658',attribute_value,null)) Sup3
    ,MAX(decode(attribute_type,'104659',attribute_value,null)) Sup4
    ,MAX(DECODE(ATTRIBUTE_TYPE,'104660',ATTRIBUTE_VALUE,null)) SUP5
    ,MAX(DECODE(ATTRIBUTE_TYPE,'104661',ATTRIBUTE_VALUE,null)) SUP6
    ,MAX(DECODE(ATTRIBUTE_TYPE,'104662',ATTRIBUTE_VALUE,null)) SUP7
    ,MAX(DECODE(ATTRIBUTE_TYPE,'104663',ATTRIBUTE_VALUE,null)) SUP8
    ,MAX(DECODE(ATTRIBUTE_TYPE,'104664',ATTRIBUTE_VALUE,null)) SUP9
    ,MAX(DECODE(ATTRIBUTE_TYPE,'104665',ATTRIBUTE_VALUE,null)) SUP10
    --To bring rule id in select query
    ,'104643' SUB100
    ,'104644' SUB200
    ,'104645' SUB300
    ,'104646' SUB400
    ,'104647' SUB500
    ,'104648' SUB600
    ,'104649' SUB700
    ,'104650' SUB800
    ,'104650' SUB900
    ,'104650' SUB1000
    FROM (select T2060.C205_PART_NUMBER_ID, T2060.C2060_DI_NUMBER as DE2_PRIMARY_DI_NUMBER, T906.C906_RULE_ID as attribute_type, T9031.C9031_ATTRIBUTE_VALUE as attribute_value, T906.C906_RULE_GRP_ID
    from T906_RULES T906, T9031_JOB_SOURCE_SYSTEM T9031, T2072_DI_GUDID_STATUS T2072 , T2060_DI_PART_MAPPING T2060
    where T906.C906_RULE_ID = T9031.C9031_ATTRIBUTE_TYPE 
    and T9031.C9031_REF_ID = T2072.C2060_DI_NUMBER
    and T9031.C9030_JOB_ACTIVITY_ID = T2072.C9030_SOURCE_ACTIVITY_ID
    and T2060.C2060_DI_NUMBER = T2072.C2060_DI_NUMBER 
    AND T906.C906_RULE_GRP_ID in ('DE32_FDA_SUB_NUM','DE33_FDA_SUPP_NUM')
    and T906.C906_VOID_FL is null
    and T9031.C9031_ATTRIBUTE_VALUE is not null
    --and T2060.C2060_DI_NUMBER='00889095179071'
    )
    group by C205_PART_NUMBER_ID, DE2_PRIMARY_DI_NUMBER
    ) , (SELECT ROWNUM AS dummy_row_id FROM dual
    CONNECT BY ROWNUM <=10)
    GROUP BY C205_PART_NUMBER_ID , DE2_PRIMARY_DI_NUMBER,dummy_row_id)
    WHERE DE37_GMDNCODE_P IS NOT NULL;

