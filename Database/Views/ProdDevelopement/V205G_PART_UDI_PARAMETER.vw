--@"C:\database\Views\ProdDevelopement\v205g_part_udi_parameter.vw";

CREATE OR REPLACE VIEW v205g_part_udi_parameter
AS
	 SELECT t205d.c205_part_number_id, t205.c202_project_id, t205.c205_part_num_desc
  	, t205d.c205d_subject_Dm_Exempt, t205d.c205d_mri_Safety_Information, t205d.c205d_dm_Di_Primary_Di
	, t205d.c205d_dm_Di_Number, t205d.c205d_hctp, t205d.c205d_single_Use, t205d.c205d_expiration_Dt
	, t205d.c205d_dev_pkg_sterile, t205d.c205d_device_Count, t205d.c205d_unit_Use_Di_Number, t205d.c205d_pack_Di_Number
	, t205d.c205d_quantity_Per_Pack, t205d.c205d_contains_Di_Pack, t205d.c205d_package_Discon_Dt
	, t205d.c205d_override_Company_Nm, t205d.c205d_duns_Number, t205d.c205d_cust_Contact_Phone
	, t205d.c205d_cust_Contact_Mail, t205d.c205d_lot_Or_Batch_Num, t205d.c205d_manufaturing_Dt
	, t205d.c205d_serial_Num, t205d.c205d_donation_Iden_Num, t205d.c205d_issuing_Agency_Conf
	, t205d.c205d_di_Number_Fl, t205d.c205d_wo_Created, t205d.c205d_udi_Etch_Req, t205d.c205d_di_Only_Etch_Req
	, t205d.c205d_pi_Only_Etch_Req, t205d.c205d_vendor_Design, t205d.c205d_bulk_Package
   FROM t205_part_number t205,
    (
     SELECT c205_part_number_id, 
        -- UDI Parameters
        MAX (DECODE (c901_attribute_type, 103341, c205d_attribute_value)) c205d_mri_Safety_Information,
        MAX (DECODE (c901_attribute_type, 103342, c205d_attribute_value)) c205d_subject_Dm_Exempt,
        MAX (DECODE (c901_attribute_type, 103343, c205d_attribute_value)) c205d_dm_Di_Primary_Di,
        MAX (DECODE (c901_attribute_type, 103344, c205d_attribute_value)) c205d_dm_Di_Number,
        MAX (DECODE (c901_attribute_type, 103345, c205d_attribute_value)) c205d_hctp,
        MAX (DECODE (c901_attribute_type, 103346, c205d_attribute_value)) c205d_single_Use,
        MAX (DECODE (c901_attribute_type, 103347, c205d_attribute_value)) c205d_expiration_Dt,
        MAX (DECODE (c901_attribute_type, 103348, c205d_attribute_value)) c205d_dev_pkg_sterile,
        MAX (DECODE (c901_attribute_type, 103785, c205d_attribute_value)) c205d_device_Count,
        MAX (DECODE (c901_attribute_type, 4000542, c205d_attribute_value)) c205d_unit_Use_Di_Number,
        MAX (DECODE (c901_attribute_type, 4000543, c205d_attribute_value)) c205d_pack_Di_Number,
        MAX (DECODE (c901_attribute_type, 4000544, c205d_attribute_value)) c205d_quantity_Per_Pack,
        MAX (DECODE (c901_attribute_type, 4000545, c205d_attribute_value)) c205d_contains_Di_Pack,
        MAX (DECODE (c901_attribute_type, 103780, c205d_attribute_value)) c205d_package_Discon_Dt,
        MAX (DECODE (c901_attribute_type, 103781, c205d_attribute_value)) c205d_override_Company_Nm,
        MAX (DECODE (c901_attribute_type, 4000546, c205d_attribute_value)) c205d_duns_Number,
        MAX (DECODE (c901_attribute_type, 4000547, c205d_attribute_value)) c205d_cust_Contact_Phone,
        MAX (DECODE (c901_attribute_type, 4000548, c205d_attribute_value)) c205d_cust_Contact_Mail,
        MAX (DECODE (c901_attribute_type, 103782, c205d_attribute_value)) c205d_lot_Or_Batch_Num,
        MAX (DECODE (c901_attribute_type, 103783, c205d_attribute_value)) c205d_manufaturing_Dt,
        MAX (DECODE (c901_attribute_type, 103784, c205d_attribute_value)) c205d_serial_Num,
        MAX (DECODE (c901_attribute_type, 103786, c205d_attribute_value)) c205d_donation_Iden_Num,
        MAX (DECODE (c901_attribute_type, 4000539, c205d_attribute_value)) c205d_issuing_Agency_Conf,
        MAX (DECODE (c901_attribute_type, 103379, c205d_attribute_value)) c205d_di_Number_Fl,
        MAX (DECODE (c901_attribute_type, 103380, c205d_attribute_value)) c205d_wo_Created,
        MAX (DECODE (c901_attribute_type, 103381, c205d_attribute_value)) c205d_udi_Etch_Req,
        MAX (DECODE (c901_attribute_type, 103382, c205d_attribute_value)) c205d_di_Only_Etch_Req,
        MAX (DECODE (c901_attribute_type, 103383, c205d_attribute_value)) c205d_pi_Only_Etch_Req,
        MAX (DECODE (c901_attribute_type, 4000517, c205d_attribute_value)) c205d_vendor_Design,
        MAX (DECODE (c901_attribute_type, 4000540, c205d_attribute_value)) c205d_bulk_Package
FROM MV205G_PART_UDI_PARAMETER WHERE C205d_VOID_FL IS NULL GROUP BY c205_part_number_id) t205d
       WHERE t205.c205_part_number_id = t205d.c205_part_number_id;
/