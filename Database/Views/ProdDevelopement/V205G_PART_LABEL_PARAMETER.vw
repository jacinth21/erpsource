--@"C:\database\Views\ProdDevelopement\V205G_PART_LABEL_PARAMETER.vw";

CREATE OR REPLACE VIEW v205g_part_label_parameter 
 AS 
 SELECT t205d.c205_part_number_id,t205d.c205d_artifacts,t205d.c205d_taggable_part,t205d.c205d_nappi_code,
t205d.c205d_shelf_life 
 ,t205d.c205d_structural_type,t205d.c205d_preservation_method,t205d.c205d_storage_temperature,
t205m.c901_proc_spec_type c205d_processing_spec_type 
 ,t205d.c205d_master_product,t205m.c901_contract_proc_client c205d_contract_process_client,t205d.c205d_is_fixed_size,t205d.c205d_tollerance 
 ,t205m.c205m_generic_spec c205d_general_spec,t205m.c205m_size c205d_label_size 
 ,t205m.c205m_size_format c205d_size_format,t205m.c205m_box_label c205d_box_label,t205m.c205m_patient_label c205d_patient_label,t205m.c205m_package_label c205d_package_label,t205d.c205d_cpc_patient_label 
 ,t205d.c205d_cpc_package_label,t205m.c205m_sample c205d_sample,t205d.c205d_cpc_logo_path, t205d.c205d_size_example, t205m.c205m_material_spec c205d_material_spec, t205m.c205m_part_drawing c205d_part_drawing 
     FROM 
     t205_part_number t205, t205m_part_label_parameter t205m, 
     ( 
      SELECT c205_part_number_id, 
       -- Label Parameter 
         MAX (DECODE (c901_attribute_type, 4000444, c205d_attribute_value)) c205d_artifacts, 
         MAX (DECODE (c901_attribute_type, 92340, c205d_attribute_value)) c205d_taggable_part, 
         MAX (DECODE (c901_attribute_type, 200001, c205d_attribute_value)) c205d_nappi_code, 
         MAX (DECODE (c901_attribute_type, 103725, c205d_attribute_value)) c205d_shelf_Life, 
         MAX (DECODE (c901_attribute_type, 103726, c205d_attribute_value)) c205d_structural_Type, 
         MAX (DECODE (c901_attribute_type, 103727, c205d_attribute_value)) c205d_preservation_Method, 
         MAX (DECODE (c901_attribute_type, 103728, c205d_attribute_value)) c205d_storage_temperature, 
         MAX (DECODE (c901_attribute_type, 103730, c205d_attribute_value)) c205d_master_Product, 
         MAX (DECODE (c901_attribute_type, 103732, c205d_attribute_value)) c205d_is_fixed_size, 
         MAX (DECODE (c901_attribute_type, 103733, c205d_attribute_value)) c205d_tollerance, 
         MAX (DECODE (c901_attribute_type, 103743, c205d_attribute_value)) c205d_cpc_patient_label, 
         MAX (DECODE (c901_attribute_type, 103744, c205d_attribute_value)) c205d_cpc_package_label, 
         MAX (DECODE (c901_attribute_type, 103746, c205d_attribute_value)) c205d_cpc_logo_path, 
         MAX (DECODE (c901_attribute_type, 103747, c205d_attribute_value)) c205d_size_example 
         FROM MV205G_PART_LABEL_PARAMETER WHERE C205d_VOID_FL IS NULL GROUP BY c205_part_number_id)t205d 
        WHERE t205.c205_part_number_id = t205d.c205_part_number_id 
        and t205.c205_part_number_id = t205m.c205_part_number_id(+);
/