--@"C:\database\Views\ProdDevelopement\v205g_part_regulatory_param.vw";

CREATE OR REPLACE VIEW v205g_part_regulatory_param
AS
	 SELECT t205d.c205_part_number_id, t205.c202_project_id, t205.c205_part_num_desc
  , t205d.c205d_Brand_Name, t205d.c205d_US_Reg_Classification, t205d.c205d_US_Reg_Pathway
  , t205d.c205d_FDA_Sub_No1, t205d.c205d_FDA_Sub_No2, t205d.c205d_FDA_Sub_No3
  , t205d.c205d_FDA_Sub_No4, t205d.c205d_FDA_Sub_No5, t205d.c205d_FDA_Sub_No6
  , t205d.c205d_FDA_Sub_No7, t205d.c205d_FDA_Sub_No8, t205d.c205d_FDA_Sub_No9
  , t205d.c205d_FDA_Sub_No10, t205d.c205d_FDA_Approval_Date, t205d.c205d_FDA_NTF_Reported
  , t205d.c205d_FDA_Listing, t205d.c205d_PMA_Sup_No1, t205d.c205d_PMA_Sup_No2
  , t205d.c205d_PMA_Sup_No3, t205d.c205d_PMA_Sup_No4, t205d.c205d_PMA_Sup_No5
  , t205d.c205d_PMA_Sup_No6, t205d.c205d_PMA_Sup_No7, t205d.c205d_PMA_Sup_No8
  , t205d.c205d_PMA_Sup_No9, t205d.c205d_PMA_Sup_No10, t205d.c205d_FDA_Product_Code1
  , t205d.c205d_FDA_Product_Code2, t205d.c205d_FDA_Product_Code3, t205d.c205d_FDA_Product_Code4
  , t205d.c205d_FDA_Product_Code5, t205d.c205d_FDA_Product_Code6, t205d.c205d_FDA_Product_Code7
  , t205d.c205d_FDA_Product_Code8, t205d.c205d_FDA_Product_Code9, t205d.c205d_FDA_Product_Code10
  , t205d.c205d_EU_Reg_Pathway, t205d.c205d_GMDN_Code, t205d.c205d_EU_CE_Tech_File_Num
  , t205d.c205d_EU_CE_Tech_File_Rev, t205d.c205d_Notes, t205d.c205d_exempt_submission
   FROM t205_part_number t205,
    (
     SELECT c205_part_number_id, 
        --Regulatory Parameters
        MAX (DECODE (c901_attribute_type, 104640, c205d_attribute_value)) c205d_Brand_Name ,
		MAX (DECODE (c901_attribute_type, 104641, c205d_attribute_value)) c205d_US_Reg_Classification ,
		MAX (DECODE (c901_attribute_type, 104642, c205d_attribute_value)) c205d_US_Reg_Pathway ,
		MAX (DECODE (c901_attribute_type, 104643, c205d_attribute_value)) c205d_FDA_Sub_No1 ,
		MAX (DECODE (c901_attribute_type, 104644, c205d_attribute_value)) c205d_FDA_Sub_No2 ,
		MAX (DECODE (c901_attribute_type, 104645, c205d_attribute_value)) c205d_FDA_Sub_No3 ,
		MAX (DECODE (c901_attribute_type, 104646, c205d_attribute_value)) c205d_FDA_Sub_No4 ,
		MAX (DECODE (c901_attribute_type, 104647, c205d_attribute_value)) c205d_FDA_Sub_No5 ,
		MAX (DECODE (c901_attribute_type, 104648, c205d_attribute_value)) c205d_FDA_Sub_No6 ,
		MAX (DECODE (c901_attribute_type, 104649, c205d_attribute_value)) c205d_FDA_Sub_No7 ,
		MAX (DECODE (c901_attribute_type, 104650, c205d_attribute_value)) c205d_FDA_Sub_No8 ,
		MAX (DECODE (c901_attribute_type, 104651, c205d_attribute_value)) c205d_FDA_Sub_No9 ,
		MAX (DECODE (c901_attribute_type, 104652, c205d_attribute_value)) c205d_FDA_Sub_No10 ,
		MAX (DECODE (c901_attribute_type, 104653, c205d_attribute_value)) c205d_FDA_Approval_Date ,
		MAX (DECODE (c901_attribute_type, 104654, c205d_attribute_value)) c205d_FDA_NTF_Reported ,
		MAX (DECODE (c901_attribute_type, 104655, c205d_attribute_value)) c205d_FDA_Listing ,
		MAX (DECODE (c901_attribute_type, 104656, c205d_attribute_value)) c205d_PMA_Sup_No1 ,
		MAX (DECODE (c901_attribute_type, 104657, c205d_attribute_value)) c205d_PMA_Sup_No2 ,
		MAX (DECODE (c901_attribute_type, 104658, c205d_attribute_value)) c205d_PMA_Sup_No3 ,
		MAX (DECODE (c901_attribute_type, 104659, c205d_attribute_value)) c205d_PMA_Sup_No4 ,
		MAX (DECODE (c901_attribute_type, 104660, c205d_attribute_value)) c205d_PMA_Sup_No5 ,
		MAX (DECODE (c901_attribute_type, 104661, c205d_attribute_value)) c205d_PMA_Sup_No6 ,
		MAX (DECODE (c901_attribute_type, 104662, c205d_attribute_value)) c205d_PMA_Sup_No7 ,
		MAX (DECODE (c901_attribute_type, 104663, c205d_attribute_value)) c205d_PMA_Sup_No8 ,
		MAX (DECODE (c901_attribute_type, 104664, c205d_attribute_value)) c205d_PMA_Sup_No9 ,
		MAX (DECODE (c901_attribute_type, 104665, c205d_attribute_value)) c205d_PMA_Sup_No10 ,
		MAX (DECODE (c901_attribute_type, 104666, c205d_attribute_value)) c205d_FDA_Product_Code1 ,
		MAX (DECODE (c901_attribute_type, 104667, c205d_attribute_value)) c205d_FDA_Product_Code2 ,
		MAX (DECODE (c901_attribute_type, 104668, c205d_attribute_value)) c205d_FDA_Product_Code3 ,
		MAX (DECODE (c901_attribute_type, 104669, c205d_attribute_value)) c205d_FDA_Product_Code4 ,
		MAX (DECODE (c901_attribute_type, 104670, c205d_attribute_value)) c205d_FDA_Product_Code5 ,
		MAX (DECODE (c901_attribute_type, 104671, c205d_attribute_value)) c205d_FDA_Product_Code6 ,
		MAX (DECODE (c901_attribute_type, 104672, c205d_attribute_value)) c205d_FDA_Product_Code7 ,
		MAX (DECODE (c901_attribute_type, 104673, c205d_attribute_value)) c205d_FDA_Product_Code8 ,
		MAX (DECODE (c901_attribute_type, 104674, c205d_attribute_value)) c205d_FDA_Product_Code9 ,
		MAX (DECODE (c901_attribute_type, 104675, c205d_attribute_value)) c205d_FDA_Product_Code10 ,
		MAX (DECODE (c901_attribute_type, 104676, c205d_attribute_value)) c205d_EU_Reg_Pathway ,
		MAX (DECODE (c901_attribute_type, 104677, c205d_attribute_value)) c205d_GMDN_Code ,
		MAX (DECODE (c901_attribute_type, 104678, c205d_attribute_value)) c205d_EU_CE_Tech_File_Num ,
		MAX (DECODE (c901_attribute_type, 104679, c205d_attribute_value)) c205d_EU_CE_Tech_File_Rev ,
		MAX (DECODE (c901_attribute_type, 104680, c205d_attribute_value)) c205d_Notes,
		MAX (DECODE (c901_attribute_type, 104701, c205d_attribute_value)) c205d_exempt_submission
FROM MV205G_PART_REGULATORY_PARAM WHERE C205d_VOID_FL IS NULL GROUP BY c205_part_number_id)t205d
       WHERE t205.c205_part_number_id = t205d.c205_part_number_id;
/