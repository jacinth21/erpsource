--@"C:\Database\Views\Account\v2656_audit_entry_address.vw";

CREATE OR REPLACE VIEW v2656_audit_entry_address
AS
    SELECT t2656.c2656_audit_entry_id audit_entry_id, t2656.c5010_tag_id tagid, t2656.c2656_location_id location_id 
         , t2656.c901_location_type location_type_id, get_code_name (t2656.c901_location_type) location_type_nm
         , v2656_audit.phonenumber, v2656_audit.emailid, v2656_audit.addresstype, v2656_audit.name
         , v2656_audit.address1, v2656_audit.address2, v2656_audit.city, v2656_audit.state
         , v2656_audit.country, v2656_audit.zipcode, v2656_audit.billname, v2656_audit.billaddress1
         , v2656_audit.billaddress2, v2656_audit.billcity, v2656_audit.billstate
         , v2656_audit.billcountry, v2656_audit.billzipcode
	FROM t2656_audit_entry t2656
		, (SELECT t2656.c2656_audit_entry_id audit_entry_id, t2656.c5010_tag_id tagid, t703.c703_sales_rep_name NAME
				, t703.c703_phone_number phonenumber, t703.c703_email_id emailid, t106.c901_address_type addresstype
				, t106.c106_add1 address1, t106.c106_add2 address2, t106.c106_city city, t106.c901_state state
				, t106.c901_country country, t106.c106_zip_code zipcode, null billname, null billaddress1
				, null billaddress2, null billcity, null billstate, null billcountry, null billzipcode
			FROM t2656_audit_entry t2656, t703_sales_rep t703, t106_address t106
			WHERE t2656.c2656_location_id = t703.c703_sales_rep_id
			AND t2656.c901_location_type=6022   --Sales rep
			AND t2656.c106_address_id = t106.c106_address_id
			AND t106.c101_party_id = t703.c101_party_id
			AND t106.c106_void_fl IS NULL
			AND t703.c703_void_fl IS NULL
			AND t703.c901_ext_country_id IS NULL
			AND t2656.c2656_void_fl IS NULL
            AND t2656.c901_entry_type = 51030 --Audited
            UNION ALL
            SELECT t2656.c2656_audit_entry_id audit_entry_id, t2656.c5010_tag_id tagid, t704.c704_account_nm NAME
				 , t704.c704_phone phonenumber, t704.c704_email_id emailid, null addresstype, t704.c704_ship_add1 address1
				 , t704.c704_ship_add2 address2, t704.c704_ship_city city, t704.c704_ship_state state
				 , t704.c704_ship_country country, t704.c704_ship_zip_code zipcode, t704.c704_bill_name billname
                 , t704.c704_bill_add1 billaddress1, t704.c704_bill_add2 billaddress2, t704.c704_bill_city billcity
                 , t704.c704_bill_state billstate, t704.c704_bill_country billcountry, t704.c704_bill_zip_code billzipcode
            FROM t2656_audit_entry t2656, t704_account t704
			WHERE t2656.c2656_location_id = t704.c704_account_id
			AND t2656.c901_location_type=6021   --Account
			AND t2656.c2656_void_fl IS NULL
			AND t704.c704_void_fl IS NULL
			AND t704.c901_ext_country_id IS NULL 
            AND t2656.c901_entry_type = 51030 --Audited
            UNION ALL 
            SELECT t2656.c2656_audit_entry_id audit_entry_id, t2656.c5010_tag_id tagid, t701.c701_ship_name NAME
				 , t701.c701_phone_number phonenumber, t701.c701_email_id emailid,  null addresstype
				 , t701.c701_ship_add1 address1, t701.c701_ship_add2 address2, t701.c701_ship_city city
				 , t701.c701_ship_state state, t701.c701_ship_country country, t701.c701_ship_zip_code zipcode
				 , t701.c701_bill_name, t701.c701_bill_add1 billaddress1, t701.c701_bill_add2 billaddress2
				 , t701.c701_bill_city billcity, t701.c701_bill_state billstate, t701.c701_bill_country billcountry
				 , t701.c701_bill_zip_code billzipcode
            FROM t2656_audit_entry t2656, t701_distributor t701
			WHERE t2656.c2656_location_id = t701.c701_distributor_id
			AND t2656.c901_location_type=6020   -- Distributor
			AND t2656.c2656_void_fl IS NULL
			AND t701.c701_void_fl IS NULL
			AND t701.c901_ext_country_id IS NULL		
            AND t2656.c901_entry_type = 51030 --Audited
            ) v2656_audit
	WHERE t2656.c2656_audit_entry_id = v2656_audit.audit_entry_id;
/