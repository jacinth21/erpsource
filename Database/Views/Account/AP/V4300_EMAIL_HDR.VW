  
/*************************************************************
Purpose: View to get the RPA Email Header data
Author: Yoga
PC: 5353
*************************************************************/ 
CREATE OR REPLACE VIEW v4300_email_hdr
AS
  SELECT t4300.c4300_msgid msg_id
    ,t4300.c301_vendor_name vendor_name,
    t4300.c4300_sender_email_id sender_email_id,
    t4300.c4300_received_date received_date,
    to_char(t4300.c4300_received_date,'MM/dd/yyy hh:mm:ss AM') received_date_txt,
    t4300.c4300_received_date_num received_date_num,
    t4300.c4300_subject subject,
    t4300.c4300_current_folder current_folder,
    t4300.c4300_target_folder target_folder,
    t901_status.c901_code_nm status
  FROM t4300_rpa_email_hdr t4300,
    t901_code_lookup t901_status
  WHERE t4300.c4300_status       = t901_status.c901_code_id(+)
    AND t4300.c4300_void_fl       IS NULL
    AND t901_status.c901_active_fl =1
    AND t901_status.c901_void_fl  IS NULL
  ORDER BY c4300_received_date DESC;
  /

 