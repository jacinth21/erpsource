CREATE OR REPLACE VIEW v810_posting_txn
AS
SELECT   c810_account_txn_id            
		,c901_account_txn_type          
		,c810_txn_date                  
		,c810_acct_date                 
		,c810_desc                      
		,c810_dr_amt                    
		,c810_cr_amt                    
		,c810_qty                       
		,c810_party_id                  
		,c810_txn_id                    
		,NULL	c820_costing_id                
		,c803_period_id                 
		,c205_part_number_id            
		,c801_account_element_id        
		,c810_delete_fl
		,c901_country_id
		,c901_company_id
		,c810_created_by                
		,c810_created_date
		,c1900_company_id
FROM 	 t810_ap_txn		
UNION ALL
SELECT   c810_account_txn_id            
		,c901_account_txn_type          
		,c810_txn_date                  
		,c810_acct_date                 
		,c810_desc                      
		,c810_dr_amt                    
		,c810_cr_amt                    
		,c810_qty                       
		,c810_party_id                  
		,c810_txn_id                    
		,c820_costing_id                
		,c803_period_id                 
		,c205_part_number_id            
		,c801_account_element_id        
		,c810_delete_fl
		,c901_country_id
		,c901_company_id
		,c810_created_by                
		,c810_created_date
		,c1900_company_id		
FROM 	 t810_inventory_txn		
UNION ALL
SELECT   c810_account_txn_id            
		,c901_account_txn_type          
		,c810_txn_date                  
		,c810_acct_date                 
		,c810_desc                      
		,c810_dr_amt                    
		,c810_cr_amt                    
		,c810_qty                       
		,c810_party_id                  
		,c810_txn_id                    
		,c820_costing_id                
		,c803_period_id                 
		,c205_part_number_id            
		,c801_account_element_id        
		,c810_delete_fl  
		,c901_country_id
		,c901_company_id
		,c810_created_by                
		,c810_created_date
		,c1900_company_id
FROM 	 t810_consignment_txn		
UNION ALL
SELECT   c810_account_txn_id            
		,c901_account_txn_type          
		,c810_txn_date                  
		,c810_acct_date                 
		,c810_desc                      
		,c810_dr_amt                    
		,c810_cr_amt                    
		,c810_qty                       
		,c810_party_id                  
		,c810_txn_id                    
		,c820_costing_id                
		,c803_period_id                 
		,c205_part_number_id            
		,c801_account_element_id        
		,c810_delete_fl  
		,c901_country_id
		,c901_company_id
		,c810_created_by                
		,c810_created_date
		,c1900_company_id		
FROM 	 t810_other_txn		
/