--@"C:\PMT\db\Views\Account\v704_ics_accounts.vw";

-- view to get ics Accounts;

CREATE OR REPLACE VIEW v704_ics_accounts
AS
    SELECT t704.C704_ACCOUNT_ID,
           t704.C704_ACCOUNT_NM_EN,
           t704.C704_ACCOUNT_NM
    FROM t704_account t704,
         t703_sales_rep t703,
         t701_distributor t701
    WHERE t704.C704_VOID_FL   IS NULL
    AND   t704.C703_SALES_REP_ID = t703.C703_SALES_REP_ID
    AND   t703.C703_VOID_FL IS NULL
    AND   t703.C701_DISTRIBUTOR_ID = t701.C701_DISTRIBUTOR_ID 
    AND   t701.C701_VOID_FL IS NULL
    AND   t701.C901_DISTRIBUTOR_TYPE = 70105 
    AND   t704.C901_ACCOUNT_TYPE NOT IN(70114);
 /