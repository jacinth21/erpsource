  CREATE OR REPLACE FORCE EDITIONABLE VIEW V205_STERILE_PARTS (C205_PART_NUMBER_ID, C202_PROJECT_ID) AS 
  SELECT t205.c205_part_number_id, t202.c202_project_id 
    FROM t205_part_number t205,t202_project t202
   WHERE t205.c205_product_class = 4030 --Sterile 
     AND t205.c202_project_id = t202.c202_project_id
     AND t202.c202_lot_track_fl = 'Y'
     AND t205.c205_active_fl='Y';