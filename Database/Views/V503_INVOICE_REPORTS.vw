/* Formatted on 2009/12/23 14:22 (Formatter Plus v4.8.0) */

--@"C:\database\Views\V503_INVOICE_REPORTS.vw";

CREATE OR REPLACE VIEW v503_invoice_reports (v503_account_id
										   , v503_invoice_id
										   , v503_customer_po
										   , v503_invoice_date
										   , v503_account_nm
										   , v503_account_nm_en
										   , v503_created_by
										   , v503_created_date
										   , v503_invoice_type
										   , v503_distributor_id
										   , v503_distributor_name
										   , v503_region
										   , v503_credit_rating
										   , v503_credit_rating_date
										   , v503_inv_amt
										   , v503_inv_paid
										   , v503_status_fl
										   , v503_invoice_source
										   , v503_currency
										   , v503_terms_id
										   , v503_terms
										   , v503_terms_days -- Added to get the data into A/R summary By Due Date report based on due date
										   -- tax
										   , v503_order_amt
										   , v503_ship_cost
										   , v503_tax_amt
										   , v503_paracct_id
										   , v503_company_id
										   , v503_paracct_nm
										   , v503_acc_currency_id
										   , v503_acc_currency_nm
										   , v503_dealer_id
										   , v503_dealer_nm_en
										   , v503_dealer_nm
										   , v503_dealer_collector_id
										   ,v503_division_name
										   ,v503_division_id
										   ,v503_rtf_id --PMT25904-Store file reference
										   ,v503_pdf_id
										   ,v503_csv_id
										   ,v503_xml_id
										   ,v503_rep_name
										   ,v503_inv_last_payment_rece_date
										   ,v503_acc_last_payment_rece_date
										   ,v503_dealer_last_payment_rece_date
											)
AS
	SELECT t704.c704_account_id v503_account_id, t503.c503_invoice_id v503_invoice_id
		 , t503.c503_customer_po v503_customer_po, t503.c503_invoice_date v503_invoice_date
		 , t704.c704_account_nm v503_account_nm,t704.c704_account_nm_en v503_account_nm_en, t503.c503_created_by v503_created_by, t503.c503_created_date v503_created_date
		 , t503.c901_invoice_type v503_invoice_type, t701.c701_distributor_id v503_distributor_id
		 , t701.c701_distributor_name v503_distributor_name
		  -- if  Customer Dealer(26240213) get the region using dealer else from account
		 , DECODE(t503.c901_invoice_source,'26240213' ,dealer.dealer_region,NVL (t704.region_id, t701.c701_region)) v503_region
		 , t704.C704_CREDIT_RATING v503_credit_rating
		 , T704.C704_CREDIT_RATING_DATE v503_credit_rating_date
		 , DECODE (t503.c901_invoice_source, 50256, t503.C503_INV_AMT,50253,t503.C503_INV_AMT,t503.C503_INV_AMT) v503_inv_amt
	     , t503.C503_INV_PYMNT_AMT v503_inv_paid, t503.c503_status_fl v503_status_fl
		 , t503.c901_invoice_source v503_invoice_source
		 , get_code_name (t1900.c901_txn_currency) v503_currency
		 --26240213 Customer Dealer
		 , DECODE(t503.c901_invoice_source,'26240213',dealer.paymentTerms,t704.c704_payment_terms) v503_terms_id
         , DECODE(t503.c901_invoice_source,'26240213',get_code_name (dealer.paymentTerms),get_code_name (t704.c704_payment_terms)) v503_terms
		 -- to get the data into A/R summary By Due Date report based on due date
		 , DECODE(t503.c901_invoice_source,'26240213',get_payment_terms(t503.c503_invoice_date,dealer.paymentTerms)-t503.c503_invoice_date ,TRIM(get_code_name_alt(t704.c704_payment_terms))) v503_terms_days
		 -- Tax code
		 , t503.C503_INV_AMT_WO_TAX v503_order_amt,t503.C503_INV_SHIP_COST v503_ship_cost, t503.C503_INV_TAX_AMT v503_tax_amt
		 , t704.c101_party_id v503_paracct_id, t503.C1900_COMPANY_ID v503_company_id, t704.c101_party_nm v503_paracct_nm
		 , DECODE(t503.c901_invoice_source,'26240213',get_acct_curr_id_by_dealer(dealer.dealerid),t704.C901_CURRENCY) v503_acc_currency_id
		 , DECODE(t503.c901_invoice_source,'26240213',get_acct_curr_symb_by_dealer(dealer.dealerid),get_code_name (t704.C901_CURRENCY)) v503_acc_currency_nm
		 , dealer.dealerid v503_dealer_id,dealer.dealername_en v503_dealer_nm_en,dealer.dealernm v503_dealer_nm,dealer.collector_id v503_dealer_collector_id
		 , t1910.C1910_DIVISION_NAME v503_division_name,t1910.c1910_division_id v503_division_id,t503.c903_rtf_file_id v503_rtf_id
		 ,t503.c903_pdf_file_id v503_pdf_id, t503.c903_csv_file_id  v503_csv_id, t503.c903_xml_file_id  v503_xml_id,t704.rep_name repname
		 -- PC-3144: AR Summary report (to capture last payment received date)
		 , t503.c503_last_payment_received_date v503_inv_last_payment_rece_date
		 , t704.acc_last_payment_rece_date v503_acc_last_payment_rece_date
		 , dealer.dealer_last_payment_rece_date v503_dealer_last_payment_rece_date
	  FROM t503_invoice t503, t1900_company t1900,t1910_division t1910
		 ,  (SELECT t704.c704_account_id, t704.c704_account_nm,t704.c704_account_nm_en,
			t704.c704_credit_rating, t704.c704_credit_rating_date,t704.C901_CURRENCY,
			t704.c704_payment_terms, v700.region_id,t101.c101_party_id,t101.c101_party_nm,
            t704.c901_account_type, v700.rep_name,
            t704.c704_last_payment_received_date acc_last_payment_rece_date
			FROM v700_territory_mapping_detail v700, t704_account t704,t101_party t101
			WHERE v700.ac_id = t704.c704_account_id
			AND t704.c101_party_id = t101.c101_party_id(+)) t704 ,	
			(select distinct t101.c101_party_id dealerid,t101.c101_party_nm_en dealername_en ,c101_party_nm dealernm
			,t101P.C901_PAYMENT_TERMS paymentTerms ,t101p.c901_collector_id collector_id ,'' dealer_region,
			t101p.c101_last_payment_received_date dealer_last_payment_rece_date
			from t101_party t101,t101_party_invoice_details t101P,v700_territory_mapping_detail v700
            where t101.c101_party_id=t101P.c101_party_id
            and v700.dealer_id(+) = t101.c101_party_id
            and t101.c901_party_type = '26230725'
            and t101.c101_void_fl is null
            and t101p.c101_void_fl is null) dealer
		 , t701_distributor t701		 		  
	 WHERE t503.c503_void_fl IS NULL
	   AND t503.c1910_division_id     = t1910.c1910_division_id(+)
	   AND t503.c901_ext_country_id is NULL
	   AND t503.c704_account_id = t704.c704_account_id(+)
	   AND t503.c701_distributor_id = t701.c701_distributor_id(+)
	   AND t503.c1900_company_id =  t1900.c1900_company_id
	   AND t503.c503_void_fl IS NULL
	   AND t503.c101_dealer_id=dealer.dealerid(+)
	   AND (t503.c704_account_id IS NOT NULL OR t503.c701_distributor_id IS NOT NULL OR t503.c101_dealer_id IS NOT NULL)
/