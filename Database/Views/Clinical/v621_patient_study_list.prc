/* Formatted on 03/26/2010 15:56 (Formatter Plus v4.8.0) */

EXEC DBMS_MVIEW.REFRESH ('v621_patient_study_list') ;

DROP MATERIALIZED VIEW v621_patient_study_list;

CREATE MATERIALIZED VIEW v621_patient_study_list
	AS (SELECT	 t621.c621_patient_id
		, t621.c621_patient_ide_no
		, c901_surgery_type
		, get_code_name(c901_surgery_type) c621_surgery_nm
		, t612.c611_study_id
		, t614.c614_study_site_id
		, t614.c704_account_id
		, t612.c612_study_list_id
		, t612.c612_study_form_ds
		, t613.c613_study_period_id
		, t613.c613_study_period_ds
		, t613.c613_duration
		, t613.c901_period_type
		, get_code_name(t613.c901_period_type) c621_period_nm
		, t613.c613_grace_period_before
		, c613_grace_period_after
		, c901_grace_period_type
		, get_code_name(c901_grace_period_type) c621_grace_period_nm
		, pt_sc10.c622_surgery_date
		, DECODE(NVL(pt_sc10.c622_surgery_date,t621.c621_surgery_date), NULL, TO_DATE(NULL,'MM/DD/YYYY'),	get_cr_cal_exam_date (NVL(pt_sc10.c622_surgery_date,t621.c621_surgery_date)
					,t613.c901_period_type,t613.c613_duration, t613.c613_grace_period_before, c901_grace_period_type, 'S')) c621_exp_date_start
		, DECODE(NVL(pt_sc10.c622_surgery_date,t621.c621_surgery_date), NULL, TO_DATE(NULL,'MM/DD/YYYY'), get_cr_cal_exam_date (NVL(pt_sc10.c622_surgery_date,t621.c621_surgery_date)
				,t613.c901_period_type,t613.c613_duration, NULL, NULL, 'E')) c621_exp_date
		, DECODE(NVL(pt_sc10.c622_surgery_date,t621.c621_surgery_date), NULL, TO_DATE(NULL,'MM/DD/YYYY'), get_cr_cal_exam_date (NVL(pt_sc10.c622_surgery_date,t621.c621_surgery_date)
				,t613.c901_period_type,t613.c613_duration,t613.c613_grace_period_after, c901_grace_period_type, 'F')) c621_exp_date_final
		, t613.c613_seq_no
		, NVL(t621.c621_surgery_comp_fl, 'N')c621_surgery_comp_fl
		, t614.c101_user_id
		, t614. c614_site_id
		, t612.c601_form_id
		, t613.c901_study_period
		, t612.c612_seq
		, t621.c101_surgeon_id
		,t611.c611_study_nm
	FROM t621_patient_master t621
		,t612_study_form_list  t612
		,t613_study_period t613
		,t614_study_site t614
		,t611_clinical_study t611
		, ( SELECT c621_patient_id, c622_date c622_surgery_date FROM t622_patient_list
		WHERE c601_form_id IN ( 10,211,48,70) AND NVL(C622_DELETE_FL,'N')='N' ) pt_sc10
	WHERE t621.c621_delete_fl = 'N'
	AND t621.c704_account_id IS NOT NULL
	AND t612.c611_study_id = t621.c611_study_id
	AND t612.c612_study_list_id = t613.c612_study_list_id
	AND t614.c611_study_id = t612.c611_study_id
	AND t614.c704_account_id = t621.c704_account_id
	AND t614.c614_void_fl IS NULL
	AND t621.c901_surgery_type IS NOT NULL
	AND t621.c621_patient_id =	pt_sc10.c621_patient_id (+)
	AND t613.c901_study_period NOT IN (6309)   -- Maps to NA period (no time point)
	AND t611.c611_study_id = t612.c611_study_id
	AND t611.c611_delete_fl <> 'Y'
	--AND t621.c621_patient_id	NOT IN (  SELECT c621_patient_id  FROM t622_patient_list
	--	WHERE c601_form_id = 15 and c613_study_period_id = 91 ) -- Adverse Event before study period
--	AND NVL(t621.c621_surgery_comp_fl, 'N') = 'Y'
--	AND T612.C612_STUDY_LIST_ID IN (10, 5)
--	AND T613.C613_STUDY_PERIOD_ID NOT IN (26)
);
