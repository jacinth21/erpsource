CREATE OR REPLACE VIEW V600_XML_DATA
(PRIM_KEY, STUDY_ID, FORM_ID, FORM_NAME, IDE, TRTMT,
 VDATE,CDATE, PERIOD, XML_TAG, ANSWER, OTHER_ANSWER, 
 OTHER_XML_TAG, QUESTION_SEQNO, ANSWER_GRP_ID, ANSWER_SEQNO, ANSWER_LIST_ID, STUDY_LIST_ID )
AS 
(SELECT t621.c621_patient_ide_no|| '-' ||trim(t613.c613_study_period_ds) || '-' ||TO_CHAR(t622.c622_date,'MM/DD/YYYY') || t622.C622_PATIENT_EVENT_NO prim_key
       , t612.C611_STUDY_ID study_id
       , t623.C601_FORM_ID form_id
       , t612.c612_study_form_ds form_name
       , t621.c621_patient_ide_no ide
       , get_code_name(C901_SURGERY_TYPE) TRTMT
       , TO_CHAR(t622.c622_date,'MM/DD/YYYY') || t622.C622_PATIENT_EVENT_NO vdate
       , TO_CHAR(t625.c625_date,'MM/DD/YYYY') cdate
       --, TO_CHAR(t622.c622_date,'MM/DD/YYYY')
       , t613.c613_study_period_ds period
       , t603.c603_xml_tag_id xml_tag
 	   , NVL (DECODE (t603.c901_control_type, 6100, DECODE(t603.c603_answer_grp_id,7205,TO_CHAR (t605.c605_answer_value)|| '-' || t605.c605_answer_list_nm,TO_CHAR (t605.c605_answer_value)), TO_CHAR (t623.c904_answer_ds))
       , ' ') answer
       , DECODE (t603.c901_control_type, 6100, t623.c904_answer_ds, '') other_answer
       /*, (SELECT t605.c605_xml_tag_id
            FROM t603_answer_grp t603, t605_answer_list t605
           WHERE t603.c904_answer_grp = t605.c605_answer_grp
             AND t603.c603_answer_grp_id = t623.c603_answer_grp_id
             AND ROWNUM = 1) other_xml_tag*/
 , (SELECT t605.c605_xml_tag_id
            FROM t605_answer_list t605
           WHERE t605.C605_ANSWER_LIST_ID = t623.C605_ANSWER_LIST_ID
             AND t605.C605_CMT_REQUIRED_FL = 'Y' ) other_xml_tag               
       , t604.c604_seq_no question_seqno
       , t603.c603_answer_grp_id answer_grp_id  
       , t603.C603_ANSWER_SEQ_NO answer_seqno   
       , t623.C605_ANSWER_LIST_ID answer_list_id
       , t612.c612_study_list_id study_list_id
    FROM t622_patient_list t622
       , t623_patient_answer_list t623
       , t603_answer_grp t603
       , t613_study_period t613
       , t612_study_form_list t612
       , t605_answer_list t605
       , t604_form_ques_link t604
       , t621_patient_master t621
       , t625_patient_verify_list t625
   WHERE t622.c622_patient_list_id = t623.c622_patient_list_id
    -- AND t612.c612_study_form_ds = 'SC5'
     AND t625.c622_patient_list_id=t622.c622_patient_list_id
     AND c901_verify_level=6190     --created date verified level id
     AND c625_delete_fl is null	
     AND t603.c603_answer_grp_id = t623.c603_answer_grp_id
     AND t603.c603_xml_required_fl = 'Y'
     AND t623.c605_answer_list_id = t605.c605_answer_list_id(+)
     AND t613.c613_study_period_id = t622.c613_study_period_id
     AND t612.c612_study_list_id = t613.c612_study_list_id
     AND t604.c601_form_id = t623.c601_form_id
     AND t604.c602_question_id = t623.c602_question_id
     AND t621.c621_patient_id = t622.c621_patient_id
     AND t621.c621_delete_fl NOT IN ('Y', 'D')
     AND t604.c604_delete_fl = 'N'   -- For SC14- Adverse Events
     AND t603.c603_delete_fl = 'N'   -- For SC14- Adverse Events
     AND t622.c622_date IS NOT NULL   -- Added for SC9 , since there were some records that didnt have dates James 2008
     AND t623.c623_delete_fl = 'N'
     AND t622.C622_DELETE_FL = 'N'
     and t603.c602_question_id = t604.c602_question_id -- Added by velu for IMPCOMP and DEVDESC column displaying two times in the report.
     AND t622.c622_missing_follow_up_fl is null)
/