--@"C:\Database\Views\Pricing\v_gm_pricing_current.vw";
/***********************************************************************
Filename: v_gm_pricing_current.vw
Component:View
Package:
Designer: Globus Medical
Developer: Aprasath
Version: 1.0
Description: Creating view for initiate pricing - current price details
**********************************************************************/
CREATE OR REPLACE VIEW v_gm_pricing_current 
AS 
--Below query will fetch the group price which are in construct or any groups
SELECT t4010.c4010_group_id,
  NULL c205_part_number_id ,
  t7540_main_price.c7540_unit_price current_price ,
  t7540_grp_price.c7540_unit_price gpo_current_price,
  t7540_main_price.c101_party_id
FROM t4010_group t4010 ,
  (SELECT *
  FROM t7540_account_group_pricing t7540
  WHERE t7540.c101_party_id =
    (SELECT token FROM v_double_in_list WHERE token IS NOT NULL
    )
  AND t7540.c205_part_number_id IS NULL
  AND t7540.c7540_void_fl       IS NULL
  ) t7540_main_price ,
  (SELECT *
  FROM t7540_account_group_pricing t7540
  WHERE t7540.c101_party_id =
    (SELECT tokenii FROM v_double_in_list WHERE tokenii IS NOT NULL
    ) -- Should be gpo id if available
  AND t7540.c205_part_number_id IS NULL
  AND t7540.c7540_void_fl       IS NULL
  ) t7540_grp_price
WHERE ( EXISTS
  (SELECT v_construct.c4010_group_id
  FROM v_gm_pricing_construct v_construct
  WHERE v_construct.c4010_group_id=t4010.c4010_group_id
  )
OR t4010.c207_set_id IN
  (SELECT token FROM v_in_list
  ) )
AND t4010.c4010_group_id = t7540_main_price.c4010_group_id(+)
AND t4010.c4010_group_id = t7540_grp_price.c4010_group_id(+)
UNION ALL
--Below query will fetch the Individual part price
SELECT t4010.c4010_group_id,
  t4010.c205_part_number_id ,
  t7540_main_price.c7540_unit_price current_price ,
  t7540_grp_price.c7540_unit_price gpo_current_price,
  t7540_main_price.c101_party_id
FROM
  (SELECT t4010.c4010_group_id,
    t4011.c205_part_number_id
  FROM t4010_group t4010 ,
    t4011_group_detail t4011
  WHERE (NOT EXISTS
    (SELECT v_construct.c4010_group_id
  FROM v_gm_pricing_construct v_construct
  WHERE v_construct.c4010_group_id=t4010.c4010_group_id
    )
  OR t4010.c207_set_id IN
    (SELECT token FROM v_in_list
    ) )
  AND t4010.c901_priced_type = 52111 --Individual
  AND t4010.c4010_group_id   = t4011.c4010_group_id
  ) t4010 ,
  (SELECT *
  FROM t7540_account_group_pricing t7540
  WHERE t7540.c101_party_id =
    (SELECT token FROM v_double_in_list WHERE token IS NOT NULL
    )
  AND t7540.c205_part_number_id IS NOT NULL
  AND t7540.c7540_void_fl       IS NULL
  ) t7540_main_price ,
  (SELECT *
  FROM t7540_account_group_pricing t7540
  WHERE t7540.c101_party_id =
    (SELECT tokenii FROM v_double_in_list WHERE tokenii IS NOT NULL
    ) -- Should be gpo id if available
  AND t7540.c205_part_number_id IS NOT NULL
  AND t7540.c7540_void_fl       IS NULL
  ) t7540_grp_price
WHERE t4010.c4010_group_id    = t7540_main_price.c4010_group_id (+)
AND t4010.c205_part_number_id = t7540_main_price.c205_part_number_id (+)
AND t4010.c4010_group_id      = t7540_grp_price.c4010_group_id (+)
AND t4010.c205_part_number_id = t7540_grp_price.c205_part_number_id (+);
/


