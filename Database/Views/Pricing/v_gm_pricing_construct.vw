--@"C:\Database\Views\Pricing\v_gm_pricing_construct.vw";
/***********************************************************************
Filename: v_gm_pricing_construct.vw
Component:View
Package:
Designer: Globus Medical
Developer: Aprasath
Version: 1.0
Description: Creating view for initiate pricing - construct details
**********************************************************************/
CREATE OR REPLACE VIEW v_gm_pricing_construct 
AS 
SELECT t7003.c7003_system_construct_id ,
  t7003.c7003_construct_name ,
  t7003.c901_construct_level ,
  t207.c207_set_nm ,
  t7003.c7003_principal_fl ,
  NVL(t7004.c7004_qty,0) c7004_qty,
  t7003.c207_set_id,
  t4010.c4010_group_id,
  NVL(t4010.c4010_parent_group_id,t4010.c4010_group_id) c4010_parent_group_id,
  get_group_name(NVL(t4010.c4010_parent_group_id,t4010.c4010_group_id)) c4010_parent_group_nm,
  t4010.c4010_group_nm,
  t4010.c901_group_type,
  t4010.c901_priced_type,
  t4010.c4010_specl_group_fl
FROM t7003_system_construct t7003 ,
  t7004_construct_mapping t7004 ,
  t4010_group t4010 ,
  T207_set_master T207
WHERE t7003.c207_set_id IN
  (SELECT token FROM v_in_list
  )
AND t207.c207_set_id                = t7003.c207_set_id
AND t207.c207_void_fl              IS NULL
AND t7004.c7003_system_construct_id = t7003.c7003_system_construct_id
AND t7003.c7003_void_fl            IS NULL
AND t7003.c901_status               = 52071--Active 
AND t7004.c4010_group_id            = t4010.c4010_group_id
AND t4010.c4010_void_fl            IS NULL
AND t4010.c901_type                 = 40045--Forecast/Demand Group
AND t4010.c901_group_type          IS NOT NULL
AND t4010.c4010_inactive_fl        IS NULL
AND t4010.c4010_publish_fl         IS NOT NULL
;
/


