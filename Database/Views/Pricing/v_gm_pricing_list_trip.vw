--@"C:\Database\Views\Pricing\v_gm_pricing_list_trip.vw";
/***********************************************************************
Filename: v_gm_pricing_list_trip.vw
Component:View
Package:
Designer: Globus Medical
Developer: Aprasath
Version: 1.0
Description: Creating view for initiate pricing - list and trip details
**********************************************************************/
CREATE OR REPLACE VIEW v_gm_pricing_list_trip 
AS 
--Below query will fetch the group price which are in construct or any groups
SELECT t4010.c4010_group_id,
  NULL c205_part_number_id,
  t7010_lp.c7010_price unitlistprice ,
  t7010_tp.c7010_price unittripwire
FROM t4010_group t4010,
  t7010_group_price_detail t7010_lp,
  t7010_group_price_detail t7010_tp
WHERE ( EXISTS
  (SELECT v_construct.c4010_group_id
  FROM v_gm_pricing_construct v_construct
  WHERE v_construct.c4010_group_id=t4010.c4010_group_id
  )
OR t4010.c207_set_id IN
  (SELECT token FROM v_in_list
  ) )
AND (t4010.c4010_group_id        = t7010_lp.c4010_group_id(+)
AND t7010_lp.c1900_company_id(+) = SYS_CONTEXT('CLIENTCONTEXT', 'COMPANY_ID')
AND t7010_lp.c901_price_type(+)  = 52060-- List Price
AND t7010_lp.c7010_void_fl      IS NULL)
AND (t4010.c4010_group_id        = t7010_tp.c4010_group_id(+)
AND t7010_tp.c1900_company_id(+) = SYS_CONTEXT('CLIENTCONTEXT', 'COMPANY_ID')
AND t7010_tp.c901_price_type(+)  = 52063-- TripWire Price
AND t7010_lp.c7010_void_fl      IS NULL)
UNION ALL
--Below query will fetch the Individual part price
SELECT t4010.c4010_group_id,
  t4011.c205_part_number_id,
  t2052.c2052_list_price,
  t2052.c2052_equity_price
FROM t4010_group t4010,
  t4011_group_detail t4011,
  t2052_part_price_mapping t2052
WHERE t4010.c207_set_id IN
  (SELECT token FROM v_in_list
  )
AND t4010.c901_priced_type     = 52111 --Individual
--AND t4010.c901_group_type NOT IN (52037) --Instruments and Cases
AND NOT EXISTS (
                 SELECT c906_rule_value            
                  FROM t906_rules
                  WHERE c906_rule_id   = 'EXCLUDE'
                  AND c906_rule_grp_id = 'GROUPTYPE'
                  AND C1900_COMPANY_ID = SYS_CONTEXT('CLIENTCONTEXT', 'COMPANY_ID')
                  AND C906_VOID_FL    IS NULL
                  AND t4010.c901_group_type = c906_rule_value
                 )--Instruments and Cases & Demo
AND t4010.c4010_group_id       = t4011.c4010_group_id
AND t4011.c205_part_number_id  = t2052.c205_part_number_id
AND t2052.c1900_company_id     = SYS_CONTEXT('CLIENTCONTEXT', 'COMPANY_ID');
/


