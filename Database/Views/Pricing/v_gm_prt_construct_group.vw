--@"C:\pmt\db\Views\Pricing\v_gm_prt_construct_group.vw";
/***********************************************************************
Filename: v_gm_prt_construct_group.vw
Component:View
Package:
Designer: Globus Medical
Developer: Aprasath
Version: 1.0
Description: Creating view for  pricing - construct and groups details
**********************************************************************/
CREATE OR REPLACE VIEW v_gm_prt_construct_group 
AS 
SELECT v_construct.c207_set_id  ,		 
       v_construct.c7003_system_construct_id ,
       v_construct.c901_construct_level  ,
	   v_construct.c4010_group_id  ,		  
       NULL c205_part_number_id,
       v_construct.c7004_qty ,		  
	   v_list_trip.unitlistprice  ,
	   ROUND(v_list_trip.unittripwire,0)  unittripwire,
	   DECODE(v_current.gpo_current_price,NULL,v_current.current_price,v_current.gpo_current_price) current_price ,
	   v_construct.c7003_principal_fl,
       v_construct.c4010_specl_group_fl SPECLGRPFLAG
		FROM globus_app.v_gm_pricing_construct v_construct,
		 globus_app.v_gm_pricing_list_trip v_list_trip,
		 globus_app.v_gm_pricing_current v_current
		WHERE v_construct.c4010_group_id = v_current.c4010_group_id
		AND v_construct.c4010_group_id   = v_list_trip.c4010_group_id
		UNION ALL
	 /* Fetching groups details from  t4010_group and 
      * fetching list and trip price of each group which is in constructor from globus_app.v_gm_pricing_list_trip and
      * fetching current price of each group which is in constructor from globus_app.v_gm_pricing_current
     */
		SELECT t207.c207_set_id  ,
		  NULL c7003_system_construct_id,		 
		  0 c901_construct_level ,
		  t4010.c4010_group_id  ,
		  NULL c205_part_number_id,
          1 QUANTITY ,
          v_list_trip.unitlistprice  ,
		  ROUND(v_list_trip.unittripwire,0)  unittripwire,
		  DECODE(v_current.gpo_current_price,NULL,v_current.current_price,v_current.gpo_current_price) current_price ,		  
		  NULL PRINCIPALFLAG, 
          T4010.c4010_specl_group_fl SPECLGRPFLAG
		FROM t207_set_master T207,
		  t4010_group T4010,
		  globus_app.v_gm_pricing_list_trip v_list_trip,
		  globus_app.v_gm_pricing_current v_current
		WHERE t4010.c4010_group_id           = v_current.c4010_group_id
		AND t4010.c4010_group_id             = v_list_trip.c4010_group_id
		AND v_list_trip.c205_part_number_id IS NULL
		AND v_current.c205_part_number_id   IS NULL
		AND t4010.c207_set_id                = t207.c207_set_id
		AND NOT EXISTS
		  (SELECT v_construct.c4010_group_id
		  FROM globus_app.v_gm_pricing_construct v_construct
		  WHERE v_construct.c4010_group_id=t4010.c4010_group_id
		  )
		AND t4010.c901_group_type     IS NOT NULL
		AND t4010.c4010_void_fl       IS NULL
		AND t4010.c4010_inactive_fl   IS NULL
		AND t4010.c4010_publish_fl    IS NOT NULL
		--AND t4010.c901_group_type NOT IN (52037)--Instruments and Cases
		AND NOT EXISTS (
                                SELECT c906_rule_value            
                                  FROM t906_rules
                                  WHERE c906_rule_id   = 'EXCLUDE'
                                  AND c906_rule_grp_id = 'GROUPTYPE'
                                  AND C1900_COMPANY_ID = SYS_CONTEXT('CLIENTCONTEXT', 'COMPANY_ID')
                                  AND C906_VOID_FL    IS NULL
                                  AND t4010.c901_group_type = c906_rule_value
                                )--Instruments and Cases & Demo
		AND t4010.c901_type            = 40045--Forecast/Demand Group
		AND t4010.c901_priced_type     = 52110 --Group
		AND t207.c207_set_id          IN
		  (SELECT TOKEN FROM v_in_list
		  ) 
		UNION ALL
	 /* Fetching Individual groups details from  t4010_group,t4011_group_detail and 
      * fetching list and trip price of each group which is in constructor from globus_app.v_gm_pricing_list_trip and
      * fetching current price of each group which is in constructor from globus_app.v_gm_pricing_current
     */
		SELECT t207.c207_set_id ,
		  NULL c7003_system_construct_id,		 
		  0 c901_construct_level ,
		  t4010.c4010_group_id  ,
		  t205.c205_part_number_id ,
		  1 QUANTITY,
		  v_list_trip.unitlistprice  ,
		  ROUND(v_list_trip.unittripwire,0)  unittripwire,
		  DECODE(v_current.gpo_current_price,NULL,v_current.current_price,v_current.gpo_current_price) current_price ,		  
		  NULL PRINCIPALFLAG ,
		  T4010.c4010_specl_group_fl SPECLGRPFLAG
		FROM t4011_group_detail t4011,
		  t207_set_master t207,
		  t4010_group t4010,
		  t205_part_number t205,
		  globus_app.v_gm_pricing_list_trip v_list_trip,
		  globus_app.v_gm_pricing_current v_current
		WHERE t4010.c4010_group_id           = v_current.c4010_group_id
		AND t4010.c4010_group_id             = v_list_trip.c4010_group_id
		AND v_list_trip.c205_part_number_id IS NOT NULL
		AND v_current.c205_part_number_id   IS NOT NULL
		AND t207.c207_set_id                 = t4010.c207_set_id
		AND t4010.c4010_group_id             = t4011.c4010_group_id
		AND t4011.c901_part_pricing_type     = 52080 --Primary
		AND t4011.c205_part_number_id        = t205.c205_part_number_id
		AND t4011.c205_part_number_id        = v_current.c205_part_number_id
		AND t4011.c205_part_number_id        = v_list_trip.c205_part_number_id
		AND t4010.c901_priced_type           = 52111--Individual
		--AND t4010.c901_group_type NOT       IN (52037)--Instruments and Cases
		AND NOT EXISTS (
                                SELECT c906_rule_value            
                                  FROM t906_rules
                                  WHERE c906_rule_id   = 'EXCLUDE'
                                  AND c906_rule_grp_id = 'GROUPTYPE'
                                  AND C1900_COMPANY_ID = SYS_CONTEXT('CLIENTCONTEXT', 'COMPANY_ID')
                                  AND C906_VOID_FL    IS NULL
                                  AND t4010.c901_group_type = c906_rule_value
                                )--Instruments and Cases & Demo
		AND t4010.c4010_void_fl             IS NULL
		AND t4010.c901_type                  = 40045 --Forecast/Demand Group
		AND t207.c207_set_id                IN
		  (SELECT TOKEN FROM v_in_list
		  )
		AND t4010.c901_group_type   IS NOT NULL
		AND t4010.c4010_inactive_fl IS NULL
		AND t4010.c4010_publish_fl  IS NOT NULL
;
/


