/*
File name: pbi_date_dim_two_yrs.vw
Created By: djayaraj
Description: Fetch last two years bussiness days upto end of this year
SVN Location: @"C:\Projects\Branches\PMT\db\Views\DWH\DW\Power_BI\Two_Years_Data\pbi_date_dim_two_yrs.vw";
*/

CREATE OR REPLACE VIEW pbi_date_dim_two_yrs
AS
  (SELECT cal_date,
    year_month,
    holiday_ind,
    bus_day_ind,
    day_number,
    DECODE(TRUNC(cal_date),TRUNC(sysdate), 1,NULL) today,
    DECODE(TRUNC(cal_date),TRUNC(Last_Business_Day), 1,NULL) Last_Business_Day
  FROM
    (SELECT cal_date,
      year_month,
      holiday_ind,
      bus_day_ind,
      Last_Business_Day,
      day_number
    FROM date_dim,
      (SELECT TRUNC(MAX(cal_date)) Last_Business_Day
      FROM date_dim
      WHERE TRUNC(cal_date)<TRUNC(sysdate)
      AND bus_day_ind      ='Y'
      )
    WHERE year_number >= TO_CHAR(TRUNC(add_months( sysdate, -12*2 ),'YEAR'),'yyyy')
    AND year_number   <= TO_CHAR(TRUNC(sysdate,'YEAR'),'yyyy')
    AND TRUNC(cal_date)=Last_Business_Day(+)
    )
  );