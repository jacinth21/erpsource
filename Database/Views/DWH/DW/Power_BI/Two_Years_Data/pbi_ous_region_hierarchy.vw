/*
File name: pbi_ous_region_hierarchy.vw
Created By: djayaraj
Description: OUS hierarchy mapping view
SVN Location: @"C:\Projects\Branches\PMT\db\Views\DWH\DW\Power_BI\Two_Years_Data\pbi_ous_region_hierarchy.vw";
*/


CREATE OR REPLACE VIEW pbi_ous_region_hierarchy
AS
  (
SELECT c711_zone_group_name zone_group_name,
  c901_zone_id zone_id,
  c901_code_nm zone ,
  c901_region_id region_id,
  region_group,
  seq_no,
  region_name region,
  region_type,
  c713_region_level region_level
FROM t711_zone_group t711,
  t712_zone_group_map t712 ,
  (SELECT DISTINCT gp_id,
    gp_name,
    c901_region_id,
    t901_region_nm.c901_code_nm region_name,
    t901_region_grp.c901_code_nm region_Group,
    t901_region_grp.c901_code_seq_no seq_no,
    c713_region_level,
    t901_region_typ.c901_code_nm region_type
  FROM t713_region t713,
    v700_territory_mapping_detail v700,
    t901_code_lookup t901_region_nm,
    t901_code_lookup t901_region_grp,
    t901_code_lookup t901_region_typ
  WHERE region_id      =c901_region_id
  AND c901_region_id   =t901_region_nm.c901_code_id (+)
  AND c901_region_group=t901_region_grp.c901_code_id (+)
  AND c901_region_type =t901_region_typ.c901_code_id (+)
  AND c713_void_fl    IS NULL
  ) region_map,
  t901_code_lookup t901_zone
WHERE t711.c711_zone_group_id=t712.c711_zone_group_id
AND region_map.gp_id         =c901_zone_id
AND c901_zone_id             =t901_zone.c901_code_id (+)
AND c712_void_fl            IS NULL
AND c711_void_fl            IS NULL);