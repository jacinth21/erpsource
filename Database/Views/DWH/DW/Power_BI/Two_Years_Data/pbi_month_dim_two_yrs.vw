/*
File name: pbi_month_dim_two_yrs.vw
Created By: djayaraj
Description: Fetch last two years monthly total business days and sales  business days as of current date
SVN Location: @"C:\Projects\Branches\PMT\Datawarehouse\DW_BI\Power_BI_Dashboard\2 Years Data\pbi_month_dim_two_yrs.vw";
*/

CREATE OR REPLACE VIEW pbi_month_dim_two_yrs AS
  (
SELECT date_dim_all.month_nm_year month_year,date_dim_all.year_month year_month,
	  date_dim_all.business_days_this_month ,
	  date_dim_until.sales_business_days_this_month
	FROM
	  (SELECT year_month,month_year,
		month_nm_year,
		COUNT (globus_bi_realtime.date_dim.day_number) sales_business_days_this_month
	  FROM globus_bi_realtime.date_dim
	  WHERE cal_date <=
		(SELECT TRUNC (SYSDATE) FROM dual
		)
	  AND bus_day_ind = 'Y'
	  AND year_month  >= TO_CHAR (trunc(add_months( sysdate, -12*2 ),'YEAR'), 'YYYY/MM')
	  GROUP BY year_month,month_year,
		month_nm_year
	  ) date_dim_until ,
	  (SELECT year_month,month_year,month_year,
		month_nm_year,
		COUNT (globus_bi_realtime.date_dim.day_number) business_days_this_month
	  FROM globus_bi_realtime.date_dim
	  WHERE bus_day_ind = 'Y'
	  AND year_month     >= TO_CHAR (trunc(add_months( sysdate, -12*2 ),'YEAR'), 'YYYY/MM')
	  GROUP BY year_month,month_year,
		month_nm_year
	  ) date_dim_all
	WHERE date_dim_until.year_month = date_dim_all.year_month );