/*
File name: pbi_sales_rep_dim_two_yrs.vw
Created By: djayaraj
Description: Fetch non voided sales rep data along with VP id,ad id, email, region id and region name
SVN Location: @"C:\Projects\Branches\PMT\db\Views\DWH\DW\Power_BI\Two_Years_Data\pbi_sales_rep_dim.vw";
*/
CREATE OR REPLACE VIEW pbi_sales_rep_dim_two_yrs
AS
  (SELECT to_number(t703.c703_sales_rep_id) sales_rep_id,
  NVL(t703.c703_sales_rep_name_en, t703.c703_sales_rep_name) sales_rep_name, --For Japan use English name
  to_number(t703.c701_distributor_id) distributor_id ,
  t703.c702_territory_id territory_id ,
  t703.c703_active_fl active_fl ,
  t703.c703_sales_fl sales_fl ,
  t703.c1900_company_id company_id,
  dist_vp.vp_id vp_id,
  dist_vp.vp_name vp_name,
  dist_ad.ad_id ad_id,
  dist_ad.ad_name ad_name,
  dist_vp.c101_email_id vp_email ,
  dist_ad.c101_email_id ad_email,
  dist_vp.region_id region_id,
  dist_vp.region_name region_name
FROM t703_sales_rep t703,
  (SELECT t708.c101_user_id vp_id,
    c101_user_f_name
    || ' '
    || c101_user_l_name vp_name,
    t701.c701_distributor_id dist_id,
    t101.c101_email_id,
    t708.c901_region_id region_id,
    t901_region.c901_code_nm region_name
  FROM t701_distributor t701,
    t708_region_asd_mapping t708,
    t101_user t101 ,
    t901_code_lookup t901_region
  WHERE t708.c901_region_id   = t701.c701_region
  AND t101.c101_user_id       = t708.c101_user_id
  AND t708.c901_region_id     = t901_region.c901_code_id (+)
  AND c708_delete_fl         IS NULL
  AND t708.c901_user_role_type=8001
  ) dist_vp,
  (SELECT t708.c101_user_id ad_id,
    c101_user_f_name
    || ' '
    || c101_user_l_name ad_name,
    t701.c701_distributor_id dist_id,
    t101.c101_email_id,
    t708.c901_region_id region_id,
    t901_region.c901_code_nm region_name
  FROM t701_distributor t701,
    t708_region_asd_mapping t708,
    t101_user t101 ,
    t901_code_lookup t901_region
  WHERE t708.c901_region_id   = t701.c701_region
  AND t101.c101_user_id       = t708.c101_user_id
  AND t708.c901_region_id     = t901_region.c901_code_id (+)
  AND c708_delete_fl         IS NULL
  AND t708.c901_user_role_type=8000
  ) dist_ad
WHERE t703.c701_distributor_id = dist_vp.dist_id (+) and t703.c701_distributor_id = dist_ad.dist_id (+)
AND t703.c703_void_fl         IS NULL
  );