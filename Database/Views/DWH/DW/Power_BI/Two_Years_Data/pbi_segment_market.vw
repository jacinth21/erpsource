/*
File name: pbi_segment_market.vw
Created By: djayaraj
Description: Fetch segment wise market details
SVN Location: @"C:\Projects\Branches\PMT\Datawarehouse\DW_BI\Power_BI_Dashboard\2 Years Data\pbi_segment_market.vw";
*/

CREATE OR REPLACE VIEW pbi_segment_market
AS
  (Select T2090.C2090_Segment_Id Segment_Id,
    T2092.C2092_Size Us_Market_Size,
    T2092.C2092_ous_Size OUS_Market_Size,
    C2092_Date Market_Date
  FROM t2090_segment t2090,
    T2092_segment_market_size t2092
  WHERE t2090.c2090_segment_id=t2092.c2090_segment_id
  And C2092_Void_Fl          Is Null
  );