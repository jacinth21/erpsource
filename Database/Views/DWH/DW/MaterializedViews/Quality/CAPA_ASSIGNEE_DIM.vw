-- @"c:\database\dw_bi\materializedviews\Quality\CAPA_ASSIGNEE_DIM.vw"
DROP MATERIALIZED VIEW CAPA_ASSIGNEE_DIM;

CREATE MATERIALIZED VIEW CAPA_ASSIGNEE_DIM
AS
(
SELECT 
C3151_CAPA_ASSIGNEE_ID capa_assinee_id
, C3151_CAPA_ASSIGNEE_ID capa_assinee_key_id
, c3150_capa_id capa_key_id
, get_user_name(C101_ASSIGNED_TO) capa_assignee
FROM globus_app.T3151_CAPA_ASSIGNEE
);

