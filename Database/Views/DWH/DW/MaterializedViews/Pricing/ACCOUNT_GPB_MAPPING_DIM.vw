DROP MATERIALIZED VIEW ACCOUNT_GPB_MAPPING_DIM;
CREATE MATERIALIZED VIEW ACCOUNT_GPB_MAPPING_DIM
AS
  (SELECT t704.c704_account_id account_id,
    t740.c101_party_id gpb_id,
    t704.c101_party_id parent_account_id
  FROM t704_account t704,
    t740_gpo_account_mapping t740
  WHERE t704.C704_ACCOUNT_ID=t740.C704_ACCOUNT_ID(+)
  AND C704_ACTIVE_FL        ='Y'
  AND C704_VOID_FL         IS NULL
  AND c740_void_fl         IS NULL
  ); 