-- @"d:\database\dw_bi\materializedviews\pricing\ict_pricing_fact.vw";
DROP materialized VIEW ict_pricing_fact;
CREATE materialized VIEW ict_pricing_fact nocache logging nocompress noparallel refresh force ON demand
AS
  (SELECT t705.c205_part_number_id part_key_id,
    t705.c704_account_id account_key_id,
    t101.c101_party_id party_key_id,
    NVL(t101.c101_party_nm,t101.c101_first_nm
    ||' '
    ||t101.c101_last_nm) ictname,
    date_dim.date_key_id,
    t705.c705_unit_price price,
    t705.c705_old_price oldprice,
    t705.c705_discount_offered discountoffered,
    t705.c705_created_date createddate,
    t705.c705_history_fl history_fl,
    t101.c901_ext_country_id countryid,
    NVL(t901.c901_code_nm,' ') countryname,
    NVL(t101_createdby.c101_user_f_name
    ||' '
    ||t101_createdby.c101_user_l_name,' ') createdby,
    c705_last_updated_date last_updated_date,
    NVL(t101_last_updated_by.c101_user_f_name
    ||' '
    ||t101_last_updated_by.c101_user_l_name,' ') last_updated_by
  FROM t705_account_pricing t705,
    t101_party t101,
    date_dim date_dim,
    t901_code_lookup t901,
    t101_user t101_createdby,
    t101_user t101_last_updated_by
  WHERE t705.c705_void_fl          IS NULL
  AND t101.c101_party_id            = t705.c101_party_id(+)
  AND TRUNC(t705.c705_created_date) = date_dim.cal_date
  AND t101.c901_ext_country_id      =t901.c901_code_id(+)
  AND c705_created_by               =t101_createdby.c101_user_id(+)
  AND c705_last_updated_by          =t101_last_updated_by.c101_user_id(+)
  AND t101.c901_party_type          ='7004' -- intercompany account
  UNION
  SELECT part_dim.part_id part_key_id,
    '-999' account_key_id,
    -999 party_key_id,
    '-999' ictname,
    -999 date_key_id,
    -999 price,
    -999 oldprice,
    -999 discountoffered,
    NULL createddate,
    '-999' history_fl,
    -999 countryid,
    '-999' countryname,
    '-999' createdby,
    NULL last_updated_date,
    '-999' last_updated_by
  FROM part_dim part_dim
  );