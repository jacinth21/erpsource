/* Formatted on 2009/11/09 11:56 (Formatter Plus v4.8.0) */
DROP MATERIALIZED VIEW SET_DIM;

CREATE MATERIALIZED VIEW SET_DIM
NOCACHE
NOLOGGING
NOCOMPRESS
NOPARALLEL
REFRESH FORCE ON DEMAND
AS
(
SELECT
  t207.c207_set_id set_key_id
, t207.c207_set_id set_id
, t207.c207_set_nm set_name
, t207.c207_set_desc set_description
, t901_type.c901_code_nm TYPE
, t207.c202_project_id project_id
, t901_set_category.c901_code_nm set_category
, t207.c207_category Set_Category_Id
, t901_set_group_type.c901_code_nm set_group_type
, t207.c901_set_grp_type set_group_type_Id
, t901_consignment_report_id.c901_code_nm Consignment_Report_Id
, DECODE (t207.c901_cons_rpt_id, 20100,'Yes', 'No') baselinenm 
, t901_minset_logic.c901_code_nm minset_logic
, t207.c901_minset_logic minset_logic_Id
, t901_status.c901_code_nm status
, t207.c901_status_id Status_Id
, t207.c207_rev_lvl revision_level
FROM t207_set_master t207,
t901_code_lookup t901_type,
t901_code_lookup t901_set_category,
t901_code_lookup t901_set_group_type,
t901_code_lookup t901_Consignment_Report_Id,
t901_code_lookup t901_minset_logic,
t901_code_lookup t901_status
WHERE t207.c207_void_fl IS NULL 
AND t207.c207_obsolete_fl IS NULL
AND t207.c207_type = t901_type.c901_code_id(+)
AND t207.c207_category = t901_set_category.c901_code_id(+)
AND t207.c901_set_grp_type = t901_set_group_type.c901_code_id(+)
AND t207.c901_cons_rpt_id = t901_consignment_report_id.c901_code_id(+)
AND t207.c901_minset_logic = t901_minset_logic.c901_code_id(+)
AND t207.c901_status_id = t901_status.c901_code_id(+)
UNION
SELECT
  '-1' set_key_id
, '-1' set_id
, 'Unknown' set_name
, 'Unknown' set_description
, 'Unknown' TYPE
, '-1' project_id
, 'Unknown' set_category
, -1 Set_Category_Id
, 'Unknown' set_group_type
, -1 set_group_type_Id
, 'Unknown' consignment_report_id
, 'No' baselinenm
, 'Unknown' minset_logic
, -1 minset_logic_Id
, 'Unknown' status
, -1 Status_Id
, 'Unknown' revision_level
FROM dual);


ALTER TABLE SET_DIM ADD 
CONSTRAINT SET_DIM_PK
 PRIMARY KEY (SET_KEY_ID)
 ENABLE VALIDATE;