DROP MATERIALIZED VIEW PART_ATTRIBUTE_DIM;

CREATE MATERIALIZED VIEW PART_ATTRIBUTE_DIM 
NOCACHE 
NOLOGGING 
NOCOMPRESS 
NOPARALLEL 
REFRESH FORCE ON DEMAND
AS
  (SELECT Part_num part_key_id,
    taggable,
    MRI.C901_CODE_NM MRI_Safety_Info ,
    Sub_DM_Exempt.C901_CODE_NM Subj_DM_Exempt ,
    DM_DI_PRIME.C901_CODE_NM DM_DI_is_not_Primary_DI ,
    DMDINUM.C901_CODE_NM DM_DI_Number ,
    HCTP.C901_CODE_NM HCTP ,
    FSU.C901_CODE_NM For_Single_Use ,
    EXP_DT.C901_CODE_NM Expiration_Date ,
    DPS.C901_CODE_NM Device_Pack_Sterile ,
    (
    CASE
      WHEN DI_NUMBER IS NULL
      THEN ''
      ELSE 'Yes'
    END) DI_NUMBER_FL ,
    DI_NUMBER ,
    WO_CREATE.C901_CODE_NM WO_Created ,
    UDIETCH.C901_CODE_NM UDI_Etch_Required ,
    DIETCH.C901_CODE_NM DI_Only_Etch_Required ,
    PIETCH.C901_CODE_NM PI_Only_Etch_Required ,
    VENDORDES.C901_CODE_NM Vendor_Design
    -- Regulatory Parameters
    ,
    Brand_Name ,
    US_REG_CLASS.C901_CODE_NM US_Reg_Classification ,
    US_REG_PWAY.C901_CODE_NM US_Regulatory_Pathway ,
    FDA_Submission_No_1 ,
    FDA_Submission_No_2 ,
    FDA_Submission_No_3 ,
    FDA_Submission_No_4 ,
    FDA_Submission_No_5 ,
    FDA_Submission_No_6 ,
    FDA_Submission_No_7 ,
    FDA_Submission_No_8 ,
    FDA_Submission_No_9 ,
    FDA_Submission_No_10 ,
    FDA_Approval_Date ,
    FDA_NTF_Reported ,
    FDA_Listing ,
    PMA_Supplement_No_1 ,
    PMA_Supplement_No_2 ,
    PMA_Supplement_No_3 ,
    PMA_Supplement_No_4 ,
    PMA_Supplement_No_5 ,
    PMA_Supplement_No_6 ,
    PMA_Supplement_No_7 ,
    PMA_Supplement_No_8 ,
    PMA_Supplement_No_9 ,
    PMA_Supplement_No_10 ,
    FDA_Product_Code_1 ,
    FDA_Product_Code_2 ,
    FDA_Product_Code_3 ,
    FDA_Product_Code_4 ,
    FDA_Product_Code_5 ,
    FDA_Product_Code_6 ,
    FDA_Product_Code_7 ,
    FDA_Product_Code_8 ,
    FDA_Product_Code_9 ,
    FDA_Product_Code_10 ,
    EU_REG_PWAY.C901_CODE_NM EU_Regulatory_Pathway ,
    GMDN_Code ,
    EU_CE_Tech_File_Number ,
    EU_CE_Tech_File_Rev ,
    regulatory_notes
    -- PD Attributes
    ,
    CL.C901_CODE_NM Contains_Latex ,
    RS.C901_CODE_NM Requires_Sterilization ,
    SMethod.C901_CODE_NM Sterilization_Method ,
    IDA.C901_CODE_NM Is_Device_Available ,
    Size1Type.C901_CODE_NM Size1_Type ,
    Size1_Type_Text ,
    Size1_Value ,
    Size1UOM.C901_CODE_NM Size1_U_of_M ,
    Size2Type.C901_CODE_NM Size2_Type ,
    Size2_Type_Text ,
    Size2_Value ,
    Size2UOM.C901_CODE_NM Size2_U_of_M ,
    Size3Type.C901_CODE_NM Size3_Type ,
    Size3_Type_Text ,
    Size3_Value ,
    Size3UOM.C901_CODE_NM Size3_U_of_M ,
    Size4Type.C901_CODE_NM Size4_Type ,
    Size4_Type_Text ,
    Size4_Value ,
    Size4UOM.C901_CODE_NM Size4_U_of_M
    -- Default Parameters.
    ,
    Package_Discontinue_Date ,
    OCN.C901_CODE_NM Override_Company_Name ,
    LBN.C901_CODE_NM Lot_or_Batch_Number ,
    MD.C901_CODE_NM Manufacturing_Date ,
    SN.C901_CODE_NM Serial_Number ,
    Device_Count ,
    DIN.C901_CODE_NM Donation_Identification_No ,
    gm_dw_pkg_pd_partdata.GET_ISSUE_AGENCY_CONFIG_NM(Issuing_Agency_Config) Issuing_Agency_Config ,
    Bulk_Pack.C901_CODE_NM Bulk_Packaging ,
    Unit_of_Use_DI_Number ,
    Package_DI_Number ,
    Quantity_per_Package ,
    Contains_DI_Package ,
    Labeler_DUNS_Number ,
    Customer_Contact_Phone ,
    Customer_Contact_Email ,
    Tolerance ,
    Shelf_Life ,
    STYPE.C901_CODE_NM Structural_type ,
    PM.C901_CODE_NM Preservation_Method ,
    S_Temp.C901_CODE_NM Storage_Temperature ,
    PST.C901_CODE_NM Processing_Spec_Type ,
    Master_Product ,
    gm_dw_pkg_pd_partdata.GET_CPC_NAME(Contract_Process_Client) Contract_Process_Client ,
    Fixed_Size
  FROM
    (SELECT DISTINCT pivot_sq.pnum Part_num,
      taggable,
      MRI_Safety_Info,
      Subj_DM_Exempt,
      DM_DI_is_not_Primary_DI,
      DM_DI_Number,
      HCTP,
      For_Single_Use,
      Expiration_Date,
      Device_Pack_Sterile,
      DI_NUMBER,
      WO_Created,
      UDI_Etch_Required,
      DI_Only_Etch_Required ,
      PI_Only_Etch_Required,
      Vendor_Design,
      Brand_Name,
      US_Reg_Classification,
      US_Regulatory_Pathway,
      FDA_Submission_No_1,
      FDA_Submission_No_2,
      FDA_Submission_No_3,
      FDA_Submission_No_4,
      FDA_Submission_No_5,
      FDA_Submission_No_6 ,
      FDA_Submission_No_7,
      FDA_Submission_No_8,
      FDA_Submission_No_9,
      FDA_Submission_No_10,
      FDA_Approval_Date,
      FDA_NTF_Reported,
      FDA_Listing,
      PMA_Supplement_No_1,
      PMA_Supplement_No_2,
      PMA_Supplement_No_3,
      PMA_Supplement_No_4 ,
      PMA_Supplement_No_5,
      PMA_Supplement_No_6,
      PMA_Supplement_No_7,
      PMA_Supplement_No_8,
      PMA_Supplement_No_9,
      PMA_Supplement_No_10,
      FDA_Product_Code_1,
      FDA_Product_Code_2,
      FDA_Product_Code_3,
      FDA_Product_Code_4,
      FDA_Product_Code_5 ,
      FDA_Product_Code_6,
      FDA_Product_Code_7,
      FDA_Product_Code_8,
      FDA_Product_Code_9,
      FDA_Product_Code_10,
      EU_Regulatory_Pathway,
      GMDN_Code,
      EU_CE_Tech_File_Number,
      EU_CE_Tech_File_Rev,
      regulatory_notes,
      Contains_Latex,
      Requires_Sterilization ,
      Sterilization_Method,
      Is_Device_Available,
      Size1_Type,
      Size1_Type_Text,
      Size1_Value,
      Size1_U_of_M,
      Size2_Type,
      Size2_Type_Text,
      Size2_Value,
      Size2_U_of_M,
      Size3_Type,
      Size3_Type_Text,
      Size3_Value,
      Size3_U_of_M,
      Size4_Type,
      Size4_Type_Text ,
      Size4_Value,
      Size4_U_of_M,
      Package_Discontinue_Date,
      Override_Company_Name,
      Lot_or_Batch_Number,
      Manufacturing_Date,
      Serial_Number,
      Device_Count,
      Donation_Identification_No,
      Issuing_Agency_Config,
      Bulk_Packaging,
      Unit_of_Use_DI_Number ,
      Package_DI_Number,
      Quantity_per_Package,
      Contains_DI_Package,
      Labeler_DUNS_Number,
      Customer_Contact_Phone,
      Customer_Contact_Email,
      Tolerance,
      SHELF_LIFE,
      Structural_type,
      Preservation_Method,
      Storage_Temperature,
      Processing_Spec_Type ,
      Master_Product,
      Contract_Process_Client,
      Fixed_Size
    FROM
      (SELECT DISTINCT t205d.c205_part_number_id pnum ,
        t205d.c205d_attribute_value attribute_id ,
        (
        CASE
          WHEN t205d.c205d_attribute_value = NULL
          THEN 'N'
          ELSE 'Y'
        END) attribute_value ,
        c901_attribute_type attribute_type
      FROM t205d_part_attribute t205d
      WHERE t205d.c901_attribute_type IN (80180,92340,103341,103342,103343,103344,103345,103346,103347,103348,4000517,103379,103380,103381,103382,103383, 104640,104641,104642,104643,104644,104645,104646,104647,104648,104649,104650,104651,104652,104653, 104654,104655,104656,104657,104658,104659,104660,104661,104662,104663,104664,104665,104666,104667, 104668,104669,104670,104671,104672,104673,104674,104675,104676,104677,104678,104679,104680, 104460,104461,104462,104463,104464,104465,104466,104467,104468,104469,104470,104471,104472,104473, 104474,104475,104476,104477,104478,104479,103780,103781,103782,103783,103784,103785,103786,4000542, 4000543,4000544,4000545,4000546,4000547,4000548,103344,103343,103342,103345,103346,103347,103348, 4000540,103341,4000539 ,103733,103725,103726,103727,103728,103729,103730,103731,103732)
      AND t205d.C205D_VOID_FL         IS NULL
      ) pivot (MAX(attribute_id) FOR attribute_type IN ('103341' MRI_Safety_Info,'103342' Subj_DM_Exempt,'103343' DM_DI_is_not_Primary_DI,'103344' DM_DI_Number,'103345' HCTP,'103346' For_Single_Use,'103347' Expiration_Date,'103348' Device_Pack_Sterile,'103380' WO_Created ,'103381' UDI_Etch_Required,'103382' DI_Only_Etch_Required,'103383' PI_Only_Etch_Required,'4000517' Vendor_Design,'104640' Brand_Name,'104641' US_Reg_Classification,'104642' US_Regulatory_Pathway,'104643' FDA_Submission_No_1 ,'104644' FDA_Submission_No_2,'104645' FDA_Submission_No_3,'104646' FDA_Submission_No_4,'104647' FDA_Submission_No_5,'104648' FDA_Submission_No_6,'104649' FDA_Submission_No_7,'104650' FDA_Submission_No_8 ,'104651' FDA_Submission_No_9,'104652' FDA_Submission_No_10,'104653' FDA_Approval_Date,'104654' FDA_NTF_Reported,'104655' FDA_Listing,'104656' PMA_Supplement_No_1,'104657' PMA_Supplement_No_2,'104658' PMA_Supplement_No_3 ,'104659' PMA_Supplement_No_4,'104660' PMA_Supplement_No_5,'104661'
      PMA_Supplement_No_6,'104662' PMA_Supplement_No_7,'104663' PMA_Supplement_No_8,'104664' PMA_Supplement_No_9,'104665' PMA_Supplement_No_10 ,'104666' FDA_Product_Code_1,'104667' FDA_Product_Code_2,'104668' FDA_Product_Code_3,'104669' FDA_Product_Code_4,'104670' FDA_Product_Code_5,'104671' FDA_Product_Code_6,'104672' FDA_Product_Code_7,'104673' FDA_Product_Code_8 ,'104674' FDA_Product_Code_9,'104675' FDA_Product_Code_10,'104676' EU_Regulatory_Pathway,'104677' GMDN_Code,'104678' EU_CE_Tech_File_Number,'104679' EU_CE_Tech_File_Rev,'104680' regulatory_notes,'104460' Contains_Latex ,'104461' Requires_Sterilization,'104462' Sterilization_Method,'104463' Is_Device_Available,'104464' Size1_Type,'104465' Size1_Type_Text,'104466' Size1_Value,'104467' Size1_U_of_M,'104468' Size2_Type,'104469' Size2_Type_Text ,'104470' Size2_Value,'104471' Size2_U_of_M,'104472' Size3_Type,'104473' Size3_Type_Text,'104474' Size3_Value,'104475' Size3_U_of_M,'104476' Size4_Type,'104477' Size4_Type_Text,'104478'
      Size4_Value,'104479' Size4_U_of_M ,'103780' Package_Discontinue_Date,'103781' Override_Company_Name,'103782' Lot_or_Batch_Number,'103783' Manufacturing_Date,'103784' Serial_Number,'103785' Device_Count,'103786' Donation_Identification_No,'4000539' Issuing_Agency_Config ,'4000540' Bulk_Packaging,'4000542' Unit_of_Use_DI_Number,'4000543' Package_DI_Number,'4000544' Quantity_per_Package,'4000545' Contains_DI_Package,'4000546' Labeler_DUNS_Number,'4000547' Customer_Contact_Phone ,'4000548' Customer_Contact_Email,'103733' Tolerance,'103725' Shelf_Life,'103726' Structural_type,'103727' Preservation_Method,'103728' Storage_Temperature,'103729' Processing_Spec_Type,'103730' Master_Product ,'103731' Contract_Process_Client,'103732' Fixed_Size) )  pivot_sq,
      (SELECT pnum,
        MAX(
        CASE
          WHEN attribute_type='92340'
          THEN attribute_value
          ELSE 'N'
        END) taggable
        ,
        MAX(gm_pkg_pd_rpt_udi.get_part_di_number(pnum)) DI_NUMBER
      FROM
        (SELECT DISTINCT t205d.c205_part_number_id pnum ,
          t205d.c205d_attribute_value attribute_id ,
          (
          CASE
            WHEN t205d.c205d_attribute_value is NULL
            THEN 'N'
            ELSE 'Y'
          END) attribute_value ,
          c901_attribute_type attribute_type
        FROM t205d_part_attribute t205d
        WHERE t205d.c901_attribute_type IN (80180,92340,103341,103342,103343,103344,103345,103346,103347,103348,4000517,103379,103380,103381,103382,103383, 104640,104641,104642,104643,104644,104645,104646,104647,104648,104649,104650,104651,104652,104653, 104654,104655,104656,104657,104658,104659,104660,104661,104662,104663,104664,104665,104666,104667, 104668,104669,104670,104671,104672,104673,104674,104675,104676,104677,104678,104679,104680, 104460,104461,104462,104463,104464,104465,104466,104467,104468,104469,104470,104471,104472,104473, 104474,104475,104476,104477,104478,104479,103780,103781,103782,103783,103784,103785,103786,4000542, 4000543,4000544,4000545,4000546,4000547,4000548,103344,103343,103342,103345,103346,103347,103348, 4000540,103341,4000539 ,103733,103725,103726,103727,103728,103729,103730,103731,103732)
        AND t205d.C205D_VOID_FL         IS NULL
        )
      GROUP BY pnum --GROUP BY pnum
      ) taggable_sq
    WHERE  pivot_sq.pnum=taggable_sq.pnum
    )part_attribute ,
    T901_CODE_LOOKUP MRI ,
    T901_CODE_LOOKUP HCTP ,
    T901_CODE_LOOKUP FSU
    -- Part Attributes
    ,
    T901_CODE_LOOKUP PST ,
    T901_CODE_LOOKUP S_Temp ,
    T901_CODE_LOOKUP PM ,
    T901_CODE_LOOKUP STYPE ,
    -- Default Parameters.
    T901_CODE_LOOKUP Bulk_Pack ,
    T901_CODE_LOOKUP OCN ,
    T901_CODE_LOOKUP LBN ,
    T901_CODE_LOOKUP MD ,
    T901_CODE_LOOKUP SN ,
    T901_CODE_LOOKUP DIN
    -- PD Attributes.
    ,
    T901_CODE_LOOKUP CL ,
    T901_CODE_LOOKUP RS ,
    T901_CODE_LOOKUP SMethod ,
    T901_CODE_LOOKUP IDA ,
    T901_CODE_LOOKUP Size1Type ,
    T901_CODE_LOOKUP Size1UOM ,
    T901_CODE_LOOKUP Size2Type ,
    T901_CODE_LOOKUP Size2UOM ,
    T901_CODE_LOOKUP Size3Type ,
    T901_CODE_LOOKUP Size3UOM ,
    T901_CODE_LOOKUP Size4Type ,
    T901_CODE_LOOKUP Size4UOM ,
    T901_CODE_LOOKUP US_REG_CLASS ,
    T901_CODE_LOOKUP US_REG_PWAY ,
    T901_CODE_LOOKUP EU_REG_PWAY ,
    T901_CODE_LOOKUP Sub_DM_Exempt ,
    T901_CODE_LOOKUP DM_DI_PRIME ,
    T901_CODE_LOOKUP DMDINUM ,
    T901_CODE_LOOKUP EXP_DT ,
    T901_CODE_LOOKUP DPS ,
    T901_CODE_LOOKUP WO_CREATE ,
    T901_CODE_LOOKUP UDIETCH ,
    T901_CODE_LOOKUP DIETCH ,
    T901_CODE_LOOKUP PIETCH ,
    T901_CODE_LOOKUP VENDORDES
  WHERE part_attribute.MRI_Safety_Info         =MRI.C901_CODE_ID(+)
  AND part_attribute.HCTP                      =HCTP.C901_CODE_ID(+)
  AND part_attribute.For_Single_Use            =FSU.C901_CODE_ID(+)
  AND part_attribute.HCTP                      =HCTP.C901_CODE_ID(+)
  AND part_attribute.Structural_type           =STYPE.C901_CODE_ID(+)
  AND part_attribute.Processing_Spec_Type      =PST.C901_CODE_ID(+)
  AND part_attribute.Storage_Temperature       =S_Temp.C901_CODE_ID(+)
  AND part_attribute.Preservation_Method       =PM.C901_CODE_ID(+)
  AND part_attribute.Bulk_Packaging            =Bulk_Pack.C901_CODE_ID(+)
  AND part_attribute.Override_Company_Name     =OCN.C901_CODE_ID(+)
  AND part_attribute.Lot_or_Batch_Number       =LBN.C901_CODE_ID(+)
  AND part_attribute.Manufacturing_Date        =MD.C901_CODE_ID(+)
  AND part_attribute.Serial_Number             =SN.C901_CODE_ID(+)
  AND part_attribute.Donation_Identification_No=DIN.C901_CODE_ID(+)
  AND part_attribute.Contains_Latex            = CL.C901_CODE_ID(+)
  AND part_attribute.Requires_Sterilization    = RS.C901_CODE_ID(+)
  AND part_attribute.Sterilization_Method      = SMethod.C901_CODE_ID(+)
  AND part_attribute.Is_Device_Available       = IDA.C901_CODE_ID(+)
  AND part_attribute.Size1_Type                = Size1Type.C901_CODE_ID(+)
  AND part_attribute.Size1_U_of_M              = Size1UOM.C901_CODE_ID(+)
  AND part_attribute.Size2_Type                = Size2Type.C901_CODE_ID(+)
  AND part_attribute.Size2_U_of_M              = Size2UOM.C901_CODE_ID(+)
  AND part_attribute.Size3_Type                = Size3Type.C901_CODE_ID(+)
  AND part_attribute.Size3_U_of_M              = Size3UOM.C901_CODE_ID(+)
  AND part_attribute.Size4_Type                = Size4Type.C901_CODE_ID(+)
  AND part_attribute.Size4_U_of_M              = Size4UOM.C901_CODE_ID(+)
  AND part_attribute.US_Reg_Classification     = US_REG_CLASS.C901_CODE_ID(+)
  AND part_attribute.US_Regulatory_Pathway     = US_REG_PWAY.C901_CODE_ID(+)
  AND part_attribute.EU_Regulatory_Pathway     = EU_REG_PWAY.C901_CODE_ID(+)
  AND part_attribute.Subj_DM_Exempt            = Sub_DM_Exempt.C901_CODE_ID(+)
  AND part_attribute.DM_DI_is_not_Primary_DI   = DM_DI_PRIME.C901_CODE_ID(+)
  AND part_attribute.DM_DI_Number              = DMDINUM.C901_CODE_ID(+)
  AND part_attribute.Expiration_Date           = EXP_DT.C901_CODE_ID(+)
  AND part_attribute.Device_Pack_Sterile       = DPS.C901_CODE_ID(+)
  AND part_attribute.WO_Created                = WO_CREATE.C901_CODE_ID(+)
  AND part_attribute.UDI_Etch_Required         = UDIETCH.C901_CODE_ID(+)
  AND part_attribute.DI_Only_Etch_Required     = DIETCH.C901_CODE_ID(+)
  AND part_attribute.PI_Only_Etch_Required     = PIETCH.C901_CODE_ID(+)
  AND part_attribute.Vendor_Design             = VENDORDES.C901_CODE_ID(+)
  );