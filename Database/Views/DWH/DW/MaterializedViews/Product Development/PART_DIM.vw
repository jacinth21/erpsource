/* Formatted on 2010/12/21 18:28 (Formatter Plus v4.8.0) */
-- select my_mv_capabilities( 'PART_DIM', 'REFRESH' ) as mv_report from dual ;

DROP MATERIALIZED VIEW part_dim;

CREATE MATERIALIZED VIEW part_dim
NOCACHE
NOLOGGING
NOCOMPRESS
NOPARALLEL
REFRESH FORCE ON DEMAND
AS
(
SELECT t205.c205_part_number_id part_key_id
, t205.c205_part_number_id part_id
, t205.c205_part_num_desc part_num_desc
, T901_product_family.C901_CODE_NM product_family
, T901_product_class.C901_CODE_NM product_class
, t205.c202_project_id project_id
, t202.C202_PROJECT_NM project_name
, T901_product_material.C901_CODE_NM product_material
, c205_core_part_fl core_part_flag
--, get_ac_cogs_value(t205.c205_part_number_id,4900) COST
--, GET_SALESCSG_INV_COGS_VALUE(t205.c205_part_number_id) SALESCSG_INV_COGS
, c205_crossover_fl crossover_flag
, c205_filler_details filler_details
, c205_hard_test_fl hard_test_flag
, c205_insert_id insert_id
, c205_inspection_criteria inspection_criteria
, c205_inspection_file_id inspection_file_id
, c205_inspection_rev inspection_rev
, c205_material_cert_fl material_cert_flag
, T901_material_spec.C901_CODE_NM material_spec
, T901_measuring_dev.C901_CODE_NM measuring_dev
, c205_multiple_proj_fl multiple_proj_flag
, c205_part_drawing part_drawing
, c205_release_for_sale release_for_sale
, c205_rev_num rev_num
, c205_sub_asmbly_fl sub_asmbly_fl
, c205_sub_component_fl sub_component_fl
, c205_supply_fl supply_fl
, c205_tolerance_lvl tolerance_lvl
, c205_tracking_implant_fl tracking_implant_fl
, T901_TYPE.C901_CODE_NM TYPE
, 0 AS qty_in_stock
, c205_created_date created_date
, NVL(TRIM(gm_dw_pkg_pd_partdata.get_part_group_name(t205.c205_part_number_id)),'NOT GROUPED') op_group_name
, NVL(TRIM(gm_dw_pkg_pd_partdata.get_part_group_type(t205.c205_part_number_id)),'UNKNOWN') op_group_type
, NVL(TRIM(gm_dw_pkg_pd_partdata.get_product_part_group(t205.c205_part_number_id)),'UNKNOWN') product_group_type
, NVL(TRIM(gm_dw_pkg_pd_partdata.get_product_part_segment(t205.c205_part_number_id)),'UNKNOWN') product_segment_type
, T901_part_status.C901_CODE_NM part_status
, T901_rpf.C901_CODE_NM rpf
FROM t205_part_number t205,
  t901_code_lookup T901_product_family,
  t901_code_lookup T901_product_class,
  t901_code_lookup T901_product_material,
  t901_code_lookup T901_material_spec,
  t901_code_lookup T901_measuring_dev,
  t901_code_lookup T901_TYPE,
  t901_code_lookup T901_part_status,
  t901_code_lookup T901_rpf,
  T202_Project T202
WHERE t205.c205_product_family=T901_product_family.C901_CODE_ID(+)
AND t205.c205_product_class=T901_product_class.C901_CODE_ID(+)
AND t205.c205_product_material=T901_product_material.C901_CODE_ID(+)
AND t205.c205_material_spec=T901_material_spec.C901_CODE_ID(+)
AND t205.c205_measuring_dev=T901_measuring_dev.C901_CODE_ID(+)
AND t205.c901_type=T901_TYPE.C901_CODE_ID(+)
AND t205.c901_status_id=T901_part_status.C901_CODE_ID(+)
AND t205.c901_rpf=T901_rpf.C901_CODE_ID(+)
AND t205.c202_project_id=t202.c202_project_id(+)
);

ALTER TABLE part_dim ADD
CONSTRAINT part_dim_pk
 PRIMARY KEY (part_key_id)
 ENABLE VALIDATE;
