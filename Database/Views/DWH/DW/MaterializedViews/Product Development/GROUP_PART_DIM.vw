DROP materialized VIEW group_part_dim;
CREATE materialized VIEW group_part_dim nocache nologging nocompress noparallel refresh force ON demand
AS
  (SELECT t4011.c4011_group_detail_id group_detail_key_id,
    t4011.c4011_group_detail_id group_detail_id,
    t4011.c205_part_number_id part_id,
    t4011.c4010_group_id group_id,
    t4011.c901_part_pricing_type part_pricing_type_id,
    t901.c901_code_nm part_pricing_type_name
  FROM t4011_group_detail t4011,
    t901_code_lookup t901
  WHERE t4011.c901_part_pricing_type = t901.c901_code_id(+)
  );
  ALTER TABLE group_part_dim ADD CONSTRAINT group_part_dim_pk PRIMARY KEY (group_detail_key_id) Enable VALIDATE;
  /