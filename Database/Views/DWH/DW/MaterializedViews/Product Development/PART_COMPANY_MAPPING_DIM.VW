DROP MATERIALIZED VIEW PART_COMPANY_MAPPING_DIM;
CREATE materialized VIEW PART_COMPANY_MAPPING_DIM
AS
  (SELECT c205_part_number_id part_key_id,
    t901.c901_code_nm part_mapping_type_name,
    c1900_company_id company_id
  FROM t2023_part_company_mapping t2023,
    t901_code_lookup t901
  WHERE t2023.c901_part_mapping_type=t901.c901_code_id (+)
  AND c2023_void_fl                IS NULL
  );
