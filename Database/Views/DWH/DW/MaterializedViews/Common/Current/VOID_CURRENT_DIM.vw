/*
File name: VOID_CURRENT_DIM.vw
Created By: gvaithiyanathan
Description: This Materialized view contains void data for current year  
SVN Location: @"C:\Projects\Branches\PMT\db\Views\DWH\DW\MaterializedViews\Common\Current\VOID_CURRENT_DIM.vw";
*/
DROP MATERIALIZED VIEW VOID_CURRENT_DIM;
CREATE MATERIALIZED VIEW VOID_CURRENT_DIM nocache nologging nocompress noparallel refresh force ON demand
AS
  (SELECT c907_cancel_log_id void_log_key_id,
    c907_cancel_log_id void_log_id,
    TRUNC(c907_cancel_date) void_date,
    c907_cancel_date void_date_time,
    c901_type void_type_id,
    get_code_name(c901_type) void_type_name,
    c907_ref_id void_ref_id,
    c907_comments void_comments,
    get_code_name(c901_cancel_cd) void_code,
    get_user_name(c907_created_by) created_by,
    TRUNC(c907_created_date) created_date
  FROM t907_cancel_log
  WHERE c907_cancel_log_id IN
    (SELECT MAX(c907_cancel_log_id)
    FROM t907_cancel_log
    WHERE c907_void_fl IS NULL
    GROUP BY c907_ref_id,
      c901_type
    )
  AND c907_created_date >= TRUNC(add_months( sysdate, 0 ),'year')
  );
  /
 ALTER TABLE VOID_CURRENT_DIM ADD CONSTRAINT VOID_CURRENT_DIM_PK PRIMARY KEY (VOID_LOG_KEY_ID) ENABLE VALIDATE; 