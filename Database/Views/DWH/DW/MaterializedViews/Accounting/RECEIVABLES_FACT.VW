DROP MATERIALIZED VIEW receivables_fact;
CREATE MATERIALIZED VIEW receivables_fact
AS
    (SELECT c508_receivables_id  AS receivable_key_id,
        t503.c503_invoice_id     AS invoice_key_id,
        t503.c704_account_id     AS account_key_id,
        t503.c701_distributor_id AS distributor_key_id,
        date_dim.date_key_id,
        t508.c508_payment_amt AS payment_amt
    FROM t508_receivables t508,
        t503_invoice t503,
        date_dim
    WHERE t508.c503_invoice_id  =t503.c503_invoice_id
    AND t508.c508_received_date =date_dim.cal_date
    AND t503.c503_void_fl      IS NULL
    );