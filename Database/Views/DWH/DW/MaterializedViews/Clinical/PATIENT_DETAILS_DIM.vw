/* Formatted on 2010/09/09 19:13 (Formatter Plus v4.8.0) */
CREATE OR REPLACE VIEW globus_bi_realtime.patient_details_dim
AS
   SELECT    t622.c622_patient_list_id
          || ' - '
          || v621.c613_study_period_id
          || ' - '
          || t622.c601_form_id patient_primary_key,
          v621.c621_patient_id patient_key_id,
          t622.c622_patient_list_id patient_details_key_id,
          v621.c612_study_list_id study_form_key_id,
          v621.c613_study_period_id study_period_key_id,
          v621.c614_study_site_id study_site_key_id,
          v621.c611_study_id study_key_id, t622.c601_form_id form_key_id,
          CASE
             WHEN v621.c621_surgery_date IS NOT NULL
             AND (   t622.c613_study_period_id IS NULL
                  OR TRIM (t622.c622_missing_follow_up_fl) = 'Y'
                 )
                THEN 'Y'
             ELSE 'N'
          END missing_follow_up_fl,
          CASE
             WHEN v621.c621_surgery_date IS NOT NULL
             AND (   t622.c613_study_period_id IS NULL
                  OR TRIM (t622.c622_missing_follow_up_fl) = 'Y'
                 )
                THEN 1
             ELSE 0
          END missing_follow_up_flag_cnt,
          DECODE (t622.c622_out_of_window_fl,
                  NULL, 'N',
                  t622.c622_out_of_window_fl
                 ) out_of_window_fl,
          v621.c621_exp_date_start expected_start_date,
          v621.c621_exp_date exp_date,
          v621.c621_exp_date_final expected_final_date,
          DECODE (SIGN (t622.c622_date - v621.c621_exp_date_start),
                  -1, ROUND (t622.c622_date - v621.c621_exp_date_start),
                  ROUND (t622.c622_date - v621.c621_exp_date_final)
                 ) out_of_window_days,
          CASE
             WHEN TRUNC (SYSDATE) BETWEEN v621.c621_exp_date_start
                                      AND v621.c621_exp_date_final
                THEN 'Y'
             ELSE 'N'
          END isdue,
          CASE
             WHEN TRUNC (SYSDATE) >
                                 v621.c621_exp_date_start
                THEN 'Y'
             ELSE 'N'
          END hasreachedtimepoint,
	  CASE
             WHEN TRUNC (SYSDATE) >
                                 v621.c621_exp_date_final
                THEN 'Y'
             ELSE 'N'
          END haspassedtimepoint,
          c622_date patient_details_date, c622_initials initials,
          c621_cal_value cal_value, c622_verify_fl verify_fl,
          c901_type patient_det_type_id,
          get_code_name (c901_type) patient_det_type,
          c622_patient_event_no patient_event_no,
          c622_date_history_fl date_history_fl,
          c622_initials_history_fl initials_history_fl,
          get_user_name (c622_created_by) created_by,
          c622_created_date created_date,
          c622_last_updated_date last_updated_date,
          get_user_name (c622_last_updated_by) last_updated_by
     FROM (SELECT t621.c621_patient_id,
                  t621.c621_patient_ide_no c621_patient_ide_no,
                  t612.c611_study_id, t614.c614_study_site_id,
                  t614.c704_account_id, t612.c612_study_list_id,
                  t613.c613_study_period_id, t614.c101_user_id,
                  t614.c614_site_id, t612.c601_form_id, t611.c611_study_nm,
                  t613.c613_study_period_ds c613_study_period_ds,
                  t612.c612_study_form_ds c612_study_form_ds,
                  t613.c901_study_period, t621.c621_surgery_comp_fl,
		  t621.c621_surgery_date,
                  DECODE
                     (t613.c613_study_period_ds,
                      'N/A', TO_DATE (NULL, 'MM/DD/YYYY'),
                      get_cr_cal_exam_date (t621.c621_surgery_date,
                                            t613.c901_period_type,
                                            t613.c613_duration,
                                            t613.c613_grace_period_before,
                                            c901_grace_period_type,
                                            'S'
                                           )
                     ) c621_exp_date_start,
                  DECODE
                     (t613.c613_study_period_ds,
                      'N/A', TO_DATE (NULL, 'MM/DD/YYYY'),
                      get_cr_cal_exam_date (t621.c621_surgery_date,
                                            t613.c901_period_type,
                                            t613.c613_duration,
                                            NULL,
                                            NULL,
                                            'E'
                                           )
                     ) c621_exp_date,
                  DECODE
                     (t613.c613_study_period_ds,
                      'N/A', TO_DATE (NULL, 'MM/DD/YYYY'),
                      get_cr_cal_exam_date (t621.c621_surgery_date,
                                            t613.c901_period_type,
                                            t613.c613_duration,
                                            t613.c613_grace_period_after,
                                            c901_grace_period_type,
                                            'F'
                                           )
                     ) c621_exp_date_final
             FROM t621_patient_master t621,
                  t612_study_form_list t612,
                  t613_study_period t613,
                  t614_study_site t614,
                  t611_clinical_study t611,
                  t601_form_master t601
            WHERE t621.c621_delete_fl = 'N'
              AND t621.c704_account_id IS NOT NULL
              AND t612.c611_study_id = t621.c611_study_id
              AND t612.c612_study_list_id = t613.c612_study_list_id
              AND t614.c611_study_id = t612.c611_study_id
              AND t614.c704_account_id = t621.c704_account_id
              AND t601.c601_form_id = t612.c601_form_id
              AND t614.c614_void_fl IS NULL
              AND t621.c901_surgery_type IS NOT NULL
              AND t611.c611_study_id = t612.c611_study_id
              AND t611.c611_delete_fl <> 'Y') v621,
          t622_patient_list t622
    WHERE v621.c621_patient_id = t622.c621_patient_id(+)
          AND v621.c613_study_period_id = t622.c613_study_period_id(+)
/
