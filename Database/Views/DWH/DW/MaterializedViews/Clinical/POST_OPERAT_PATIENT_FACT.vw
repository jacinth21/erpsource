CREATE OR REPLACE VIEW globus_bi_realtime.POST_OPER_PATIENT_FACT
AS 
SELECT 
PATIENT_KEY_ID 
,PATIENT_ID 
,DATE_KEY_ID 
,PATIENT_DETAILS_ID 
,PATIENT_DETAILS_KEY_ID 
,PATIENT_DETAILS_DATE 
,FORM_ID 
,FORM_KEY_ID 
,STUDY_LIST_ID
,STUDY_PERIOD_ID
,Max(Alcohol_since_last_study_visit) Alcohol_since_last_study_visit
,Max(Collar_or_Brace_still_used) Collar_or_Brace_still_used
,Max(Discontinued_work) Discontinued_work
,Max(DOCTOR_PERCEPTION_OF_RESlTS) DOCTOR_PERCEPTION_OF_RESlTS
,Max(Post_Operat_COMMENTS) Post_Operat_COMMENTS
,Max(Patient_Returned_to_work) Patient_Returned_to_work
,Max(Date_returned_to_work) Date_returned_to_work
,Max(Tobacco_since_last_study_visit) Tobacco_since_last_study_visit
,Max(adjacent_level_conditions)  adjacent_level_conditions
,Max(treated_adjacent_level)  treated_adjacent_level
,Max(index_level_since_surgery)  index_level_since_surgery
,Max(Any_ADVERSE_events)  Any_ADVERSE_events
,Max(addtl_treatment_for_pain)  addtl_treatment_for_pain
,Max(q10_what_treatment)  q10_what_treatment
,Max(q10_which_adjacent_level)  q10_which_adjacent_level
,Max(q3_what_treatment)  q3_what_treatment
,Max(q5_what_treatment)  q5_what_treatment
,Max(q7_which_condition)  q7_which_condition
,Max(q7_which_adjacent_level)  q7_which_adjacent_level
FROM ( 
SELECT 
C623_PATIENT_ANS_LIST_ID PATIENT_ANS_LIST_ID 
,C623_PATIENT_ANS_LIST_ID PATIENT_ANS_LST_KEY_ID 
,T623_PATIENT_ANSWER_LIST.C622_PATIENT_LIST_ID PATIENT_DETAILS_ID 
,T623_PATIENT_ANSWER_LIST.C622_PATIENT_LIST_ID PATIENT_DETAILS_KEY_ID 
,DATE_DIM.DATE_KEY_ID 
,T623_PATIENT_ANSWER_LIST.C621_PATIENT_ID PATIENT_ID 
,T623_PATIENT_ANSWER_LIST.C621_PATIENT_ID PATIENT_KEY_ID 
,T612_STUDY_FORM_LIST.C601_FORM_ID FORM_ID 
,T612_STUDY_FORM_LIST.C601_FORM_ID FORM_KEY_ID 
,T612_STUDY_FORM_LIST.C612_STUDY_LIST_ID STUDY_LIST_ID
,T613_STUDY_PERIOD.C613_STUDY_PERIOD_ID STUDY_PERIOD_ID
,T623_PATIENT_ANSWER_LIST.C602_QUESTION_ID QUESTION_ID 
,T623_PATIENT_ANSWER_LIST.C602_QUESTION_ID QUESTION_KEY_ID 
,T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID ANSWER_LIST_ID 
,T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID ANSWER_LIST_KEY_ID 
,T603_ANSWER_GRP.C603_ANSWER_GRP_ID ANSWER_GRP_ID 
,T603_ANSWER_GRP.C603_ANSWER_GRP_ID ANSWER_GRP_KEY_ID 
,T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID ANS_GRP_DW_REF_ID 
,CASE WHEN T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'ALCHOHOL_STATUS_TR' OR T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'ALCOHOL_POST'  then DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), 
T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS) end  Alcohol_since_last_study_visit
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'BRACE', DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), 
T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) Collar_or_Brace_still_used
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'DISCDT',DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), 
T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) Discontinued_work
,CASE WHEN T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'PERCEPTION_STATUS_TR' OR T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'PERCEPT'  then DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), 
T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS) end  DOCTOR_PERCEPTION_OF_RESlTS
,DECODE(T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID, 'POSTCOMM', DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), 
T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS)) Post_Operat_COMMENTS
,CASE WHEN T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'DATE_STATUS_TR' OR T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'RETDT'  then DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), 
T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS) end Date_returned_to_work
,CASE WHEN T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'TOBACCO_STATUS_TR' OR T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'TOBACCO_POST'  then DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), 
T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS) end   Tobacco_since_last_study_visit
,CASE WHEN T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'WORK_STATUS1_TR' OR T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'WORKSTAT'  then DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), 
T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS) end  Patient_Returned_to_work
,CASE WHEN T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'ADJACENT_LEVEL_STATUS_TR' then DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), 
T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS) end  adjacent_level_conditions
,CASE WHEN T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'TREAT_ADJACENT_STATUS_TR' then DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), 
T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS) end  treated_adjacent_level
,CASE WHEN T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'INDEX_STATUS_TR' then DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), 
T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS) end  index_level_since_surgery
,CASE WHEN T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'ADVERSE_STATUS_TR' then DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), 
T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS) end  Any_ADVERSE_events
,CASE WHEN T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'TREATMENT_STATUS_TR' then DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), 
T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS) end  addtl_treatment_for_pain
,CASE WHEN T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'Q10_TREATMENT_STATUS_TR' then DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), 
T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS) end  q10_what_treatment
,CASE WHEN T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'Q10_ADJACENT_STATUS_TR' then DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), 
T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS) end  q10_which_adjacent_level
,CASE WHEN T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'Q3_TREATMENT_STATUS_TR' then DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), 
T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS) end  q3_what_treatment
,CASE WHEN T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'Q5_TREATMET_STATUS_TR' then DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), 
T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS) end  q5_what_treatment
,CASE WHEN T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'Q7_CONDITION_STATUS_TR' then DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), 
T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS) end  q7_which_condition
,CASE WHEN T603_ANSWER_GRP.C603_ANSWER_GROUP_DW_REF_ID = 'Q7_ADJACENT_STATUS_TR' then DECODE(GET_CODE_NAME(T603_ANSWER_GRP.C901_CONTROL_TYPE),'COMBOBOX', GET_ANSWER_LIST_NAME (T623_PATIENT_ANSWER_LIST.C605_ANSWER_LIST_ID), 
T623_PATIENT_ANSWER_LIST.C904_ANSWER_DS) end  q7_which_adjacent_level
,T622_PATIENT_LIST.C622_DATE PATIENT_DETAILS_DATE 
,C623_HISTORY_FLAG HISTORY_FLAG 
,GET_USER_NAME(C623_CREATED_BY) CREATED_BY 
,C623_CREATED_DATE CREATED_DATE 
,C623_LAST_UPDATED_DATE LAST_UPDATED_DATE 
,GET_USER_NAME(C623_LAST_UPDATED_BY) LAST_UPDATED_BY 
FROM T612_STUDY_FORM_LIST, T613_STUDY_PERIOD, T623_PATIENT_ANSWER_LIST, T622_PATIENT_LIST, T603_ANSWER_GRP, T602_QUESTION_MASTER, DATE_DIM 
WHERE 
T612_STUDY_FORM_LIST.C601_FORM_ID IN (12, 212, 51) 
AND T602_QUESTION_MASTER.C602_QUESTION_ID = T603_ANSWER_GRP.C602_QUESTION_ID 
AND T612_STUDY_FORM_LIST.C612_STUDY_LIST_ID = T613_STUDY_PERIOD.C612_STUDY_LIST_ID
AND T623_PATIENT_ANSWER_LIST.C601_FORM_ID = T612_STUDY_FORM_LIST.C601_FORM_ID 
AND T622_PATIENT_LIST.C622_DATE = DATE_DIM.CAL_DATE 
AND T623_PATIENT_ANSWER_LIST.C622_PATIENT_LIST_ID =  T622_PATIENT_LIST.C622_PATIENT_LIST_ID 
AND T623_PATIENT_ANSWER_LIST.C602_QUESTION_ID = T602_QUESTION_MASTER.C602_QUESTION_ID 
AND T623_PATIENT_ANSWER_LIST.C603_ANSWER_GRP_ID = T603_ANSWER_GRP.C603_ANSWER_GRP_ID 
AND (T623_PATIENT_ANSWER_LIST.C623_DELETE_FL IS NULL OR T623_PATIENT_ANSWER_LIST.C623_DELETE_FL = 'N') 
AND (T602_QUESTION_MASTER.C602_DELETE_FL IS NULL OR T602_QUESTION_MASTER.C602_DELETE_FL = 'N') 
) GROUP BY 
PATIENT_KEY_ID  
,PATIENT_ID 
,PATIENT_DETAILS_ID 
,PATIENT_DETAILS_KEY_ID 
,DATE_KEY_ID 
,PATIENT_DETAILS_DATE 
,FORM_ID 
,FORM_KEY_ID
,STUDY_LIST_ID
,STUDY_PERIOD_ID;