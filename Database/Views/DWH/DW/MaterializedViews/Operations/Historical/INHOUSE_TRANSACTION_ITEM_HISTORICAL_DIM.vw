/*
File name: INHOUSE_TRANSACTION_ITEM_HISTORICAL_DIM.vw
Created By: gvaithiyanathan
Description: This Materialized view contains Inhouse_transactions_items data upto last 2years. 
SVN Location: @"C:\Projects\Branches\PMT\db\Views\DWH\DW\MaterializedViews\Operations\Historical\INHOUSE_TRANSACTION_ITEM_HISTORICAL_DIM.vw";
*/
DROP MATERIALIZED VIEW INHOUSE_TRANSACTION_ITEM_HISTORICAL_DIM;
CREATE MATERIALIZED VIEW INHOUSE_TRANSACTION_ITEM_HISTORICAL_DIM nocache logging nocompress noparallel refresh force ON demand
AS
  (SELECT t413.c413_trans_id inhouse_trans_item_key_id,
    t413.c412_inhouse_trans_id inhouse_trans_id,
    t413.c413_control_number control_number,
    t413.c413_item_qty item_quantity,
    t413.c413_item_price item_price,
    t413.c413_void_fl void_flag,
    t413.c205_part_number_id part_number_id,
    DECODE(t413.c413_status_fl,'S','Sold','C','Consigned','W','Rep Will Be Charged','R','Returned','Q','Quarantine') Inhouse_Trans_Item_Status --Q null
  FROM t413_inhouse_trans_items t413,
    t412_inhouse_transactions t412
  WHERE t413.c412_inhouse_trans_id = t412.c412_inhouse_trans_id
  AND t412.c412_created_date       <= LAST_DAY(ADD_MONTHS(SYSDATE,-13))
  UNION ALL
  SELECT item_consignment_key_id inhouse_trans_item_key_id,
    consignment_key_id inhouse_trans_id,
    control_number control_number,
    consignment_qty item_quantity,
    item_price item_price,
    NULL void_flag,
    part_key_id part_number_id,
    NULL inhouse_trans_item_status
  FROM item_consignment_master_historical_dim
  );