/*
File name: CONSIGNMENT_HISTORICAL_DIM.vw
Created By: gvaithiyanathan
Description: This Materialized view contains Consignment data upto last 2years. 
SVN Location: @"C:\Projects\Branches\PMT\db\Views\DWH\DW\MaterializedViews\Operations\Historical\CONSIGNMENT_HISTORICAL_DIM.vw";
*/
DROP MATERIALIZED VIEW CONSIGNMENT_HISTORICAL_DIM;
CREATE materialized VIEW CONSIGNMENT_HISTORICAL_DIM
AS
  (SELECT consignment_key_id ,
    consignment_id ,
    consignment_distributor_id ,
    consignment_ship_date ,
    comments ,
    TRUNC(consignment_created_date) consignment_created_date ,
    consignment_created_date consignment_created_date_time ,
    consignment_created_by ,
    TRUNC(consignment_updated_date) consignment_updated_date ,
    consignment_updated_date consignment_updated_date_time ,
    consignment_updated_by ,
    consignment_status ,
    void_fl ,
    consignment_set_id ,
    consignment_account_name ,
    final_comments ,
    consignment_inhouse_purpose ,
    consignment_type_id ,
    consignment_type_name ,
    consignment_ship_to_id ,
    DECODE(consignment_type_id,4112,get_user_name(consignment_ship_to_id)) inhouse_consignment_emp_name ,
    TRUNC(consignment_verified_date) consignment_verified_date ,
    consignment_verified_date consignment_verified_date_time ,
    consignment_verified_by ,
    consignment_reprocess_id ,
    master_consignment_id ,
    request_id ,
    product_request_detail_id ,
    company_key_id ,
    plant_key_id ,
    division_key_id
  FROM consignment_master_historical_dim
  WHERE (consignment_type_id IN (4128,40050,40057,4112,4129,40021,4110,4077,4113,4114,4115,4116,4119,4127,9110,103932,400058,400059,400060,400062,400063,400064)
  OR consignment_type_id     IS NULL)
  );
  /*
  4128 transfer
  40050 hospital consignment
  40057 ict
  4112 in-house consignment
  4129 dummy consignment
  40021 consignment
  4110 consignment
  4077 bone models
  4113 shelf to quarantine
  4114 shelf to repack
  4115 quarantine to scrap
  4116 quarantine to shelf
  4119 in-house loaner
  4127 product loaner
  9110 ih loaner item
  103932 part re-designation
  400058 returns hold to repack
  400059 bulk to quarantine
  400060 bulk to repack
  400062 bulk to inv adjustment
  400063 repack to shelf
  400064 returns hold to quarantine
  null sets from returns ( not given a type if there are no open requests)
  */
  -- company filter is present im CONSIGNMENT_HISTORICAL_DIM
  ALTER TABLE CONSIGNMENT_HISTORICAL_DIM ADD CONSTRAINT CONSIGNMENT_HISTORICAL_DIM_pk PRIMARY KEY (consignment_key_id) enable VALIDATE;
