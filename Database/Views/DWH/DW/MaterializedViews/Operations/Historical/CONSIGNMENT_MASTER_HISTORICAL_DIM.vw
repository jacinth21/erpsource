/*
File name: CONSIGNMENT_MASTER_HISTORICAL_DIM.vw
Created By: gvaithiyanathan
Description: This Materialized view contains Consignment data upto last 2years. 
SVN Location: @"C:\Projects\Branches\PMT\db\Views\DWH\DW\MaterializedViews\Operations\Historical\CONSIGNMENT_MASTER_HISTORICAL_DIM.vw";
*/
DROP MATERIALIZED VIEW CONSIGNMENT_MASTER_HISTORICAL_DIM;
CREATE MATERIALIZED VIEW CONSIGNMENT_MASTER_HISTORICAL_DIM
AS
  (SELECT t504.c504_consignment_id consignment_key_id ,
    t504.c504_consignment_id consignment_id ,
    t504.c701_distributor_id consignment_distributor_id ,
    t504.c504_ship_date consignment_ship_date ,
    t504.c504_comments comments ,
    t504.c504_created_date consignment_created_date ,
    NVL(t101_consignment_created_by.c101_user_f_name
    ||' '
    ||t101_consignment_created_by.c101_user_l_name,' ') consignment_created_by ,
    t504.c504_last_updated_date consignment_updated_date ,
    NVL(t101_consignment_updated_by.c101_user_f_name
    ||' '
    ||t101_consignment_updated_by.c101_user_l_name,' ') consignment_updated_by ,
    DECODE (t504.c504_status_fl,'0', 'Initiated', '1', 'WIP', '1.10','Completed set', '1.20', 'Pending Putaway','2', 'Built set','2.20','Pending Pick', '3', 'Pending Shipping', '3.30', 'Packing In Progress', '3.60', 'Ready For Pickup',4, 'shipped',6,'Transfer Not Verified', t504.c504_status_fl) consignment_status ,
    t504.c504_void_fl void_fl ,
    t504.c207_set_id consignment_set_id ,
    get_account_name(t504.c704_account_id) consignment_account_name ,
    t504.c504_final_comments final_comments ,
    NVL(t901_consignment_inhouse.c901_code_nm,' ') consignment_inhouse_purpose ,
    t504.c504_type consignment_type_id ,
    NVL(t901_consignment_type_name.c901_code_nm,' ') consignment_type_name ,
    t504.c504_ship_to_id consignment_ship_to_id ,
    t504.c504_verified_date consignment_verified_date ,
    t504.c504_verify_fl verify_flag ,
    NVL(t101_consignment_verified_by.c101_user_f_name
    ||' '
    ||t101_consignment_verified_by.c101_user_l_name,' ') consignment_verified_by ,
    t504.c504_reprocess_id consignment_reprocess_id ,
    t504.c504_master_consignment_id master_consignment_id ,
    t504.c520_request_id request_id ,
    c526_product_request_detail_id product_request_detail_id ,
    c1900_company_id company_key_id ,
    c1910_division_id division_key_id ,
    c5040_plant_id plant_key_id
  FROM t504_consignment t504,
    t101_user t101_consignment_created_by,
    t101_user t101_consignment_updated_by,
    t101_user t101_consignment_verified_by ,
    t901_code_lookup t901_consignment_inhouse,
    t901_code_lookup t901_consignment_type_name
  WHERE --c1900_company_id       	= 1000 and
    t504.c504_created_by       		=t101_consignment_created_by.c101_user_id(+)
  AND t504.c504_last_updated_by		=t101_consignment_updated_by.c101_user_id(+)
  AND t504.c504_verified_by    		=t101_consignment_verified_by.c101_user_id(+)
  AND t504.c504_inhouse_purpose		=t901_consignment_inhouse.c901_code_id(+)
  AND t504.c504_type           		=t901_consignment_type_name.c901_code_id(+)
  AND TRUNC(t504.c504_created_date) < = TRUNC(LAST_DAY(ADD_MONTHS(SYSDATE,-13)))
  );
  /
  ALTER TABLE CONSIGNMENT_MASTER_HISTORICAL_DIM ADD CONSTRAINT CONSIGNMENT_MASTER_HISTORICAL_DIM_PK PRIMARY KEY (consignment_key_id) enable VALIDATE; 