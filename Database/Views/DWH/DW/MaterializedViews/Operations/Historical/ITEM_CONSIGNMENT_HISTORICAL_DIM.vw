/*
File name: ITEM_CONSIGNMENT_HISTORICAL_DIM.vw
Created By: gvaithiyanathan
Description: This Materialized view contains item_Consignment data upto last 2years. 
SVN Location: @"C:\Projects\Branches\PMT\db\Views\DWH\DW\MaterializedViews\Operations\Historical\ITEM_CONSIGNMENT_HISTORICAL_DIM.vw";
*/
DROP MATERIALIZED VIEW ITEM_CONSIGNMENT_HISTORICAL_DIM ;
CREATE MATERIALIZED VIEW ITEM_CONSIGNMENT_HISTORICAL_DIM nocache logging nocompress noparallel refresh force ON demand
AS
  (SELECT item_consignment_key_id,
    consignment_key_id,
    control_number,
    part_key_id,
    consignment_qty consignment_qty,
    item_price,
    set_part_qty
  FROM
    (SELECT consign_item.item_consignment_key_id,
      NVL(consign_item.consignment_id,set_details.consignment_id) consignment_key_id,
      consign_item.consignment_set_id set_key_id,
      consign_item.part_key_id part_key_id,
      consign_item.control_number,
      NVL(consign_item.item_quantity,0) consignment_qty,
      consign_item.item_price,
      set_details.set_part_qty
    FROM
      (SELECT t505.c505_item_consignment_id item_consignment_key_id,
        consignment_dim.consignment_id,
        consignment_dim.consignment_set_id,
        t505.c205_part_number_id part_key_id,
        consignment_dim.consignment_created_date created_date,
        consignment_dim.request_id request_key_id,
        consignment_dim.consignment_distributor_id distributor_key_id,
        t505.c505_control_number control_number,
        t505.c505_item_qty item_quantity,
        t505.c505_item_price item_price
      FROM consignment_dim,
        t505_item_consignment t505
      WHERE consignment_dim.consignment_id    = t505.c504_consignment_id
      AND consignment_dim.consignment_set_id IS NOT NULL
      ) consign_item,
      (SELECT consignment_dim.consignment_id,
        set_part_dim.set_id,
        set_part_dim.part_number_id,
        set_part_dim.set_part_qty
      FROM set_part_dim,
        consignment_dim
      WHERE set_part_dim.set_id               =consignment_dim.consignment_set_id(+)
      AND set_part_dim.inset_fl               ='Y'
      AND set_part_dim.set_part_qty           >0
      AND consignment_dim.consignment_set_id IS NOT NULL
      ) set_details
    WHERE set_details.consignment_id (+) = consign_item.consignment_id
    AND set_details.part_number_id (+)   = consign_item.part_key_id
    AND consign_item.created_date        <= LAST_DAY(ADD_MONTHS(SYSDATE,-13))
    UNION ALL
    SELECT t505.c505_item_consignment_id item_consignment_key_id,
      t505.c504_consignment_id consignment_key_id,
      NULL set_key_id,
      t505.c205_part_number_id part_key_id,
      t505.c505_control_number control_number,
      t505.c505_item_qty consignment_qty,
      t505.c505_item_price item_price,
      NULL set_part_qty
    FROM t505_item_consignment t505,
      consignment_dim
    WHERE consignment_dim.consignment_id         = t505.c504_consignment_id (+)
    AND consignment_dim.consignment_set_id      IS NULL
    AND consignment_dim.consignment_created_date <= LAST_DAY(ADD_MONTHS(SYSDATE,-13))
    )
  );
  /
