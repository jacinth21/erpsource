-- create or replace view log on t704_account with PRIMARY kEY , rowid, sequence;
-- select my_mv_capabilities( 'd101_mv_account', 'REFRESH' ) as mv_report from dual ;
DROP MATERIALIZED VIEW vendor_dim;
CREATE MATERIALIZED VIEW vendor_dim
AS
  (SELECT T301.C301_VENDOR_ID VENDOR_KEY_ID
  , T301.C301_VENDOR_ID VENDOR_ID
  , T301.C101_PARTY_ID PARTY_KEY_ID
  , T301.C101_PARTY_ID PARTY_ID
  , NVL(T301.C301_ACTIVE_FL,'N') ACTIVE_FL
  , T301.C301_BILL_ADD1 BILL_ADD1
  , T301.C301_BILL_ADD2 BILL_ADD2
  , T301.C301_BILL_CITY BILL_CITY
  , get_code_name(T301.C301_BILL_COUNTRY) BILL_COUNTRY
  , get_code_name(T301.C301_BILL_STATE) BILL_STATE
  , T301.C301_BILL_ZIP BILL_ZIP
  , T301.C301_CONTACT_PERSON VENDOR_CONTACT
  , T301.C301_EMAIL_ID VENDOR_EMAIL_ID
  , T301.C301_FAX VENDOR_FAX
  , T301.C301_INCEPTION_DATE INCEPTION_DATE
  , T301.C301_LOT_CODE LOT_CODE
  , NVL(T301.C301_RM_INVOICE_FL,'N') RM_INVOICE_FL
  , T301.C301_RM_PERCENTAGE RM_PERCENTAGE
  , T301.C301_SAGE_VENDOR_ID SAGE_VENDOR_ID
  , NVL(T301.C301_SEC199_FL,'N') SEC199_FL
  , T301.C301_VENDOR_NAME VENDOR_NAME
  , T301.C301_VENDOR_SH_NAME VENDOR_SH_NAME
  , get_code_name(T301.C301_VENDOR_TYPE) VENDOR_TYPE
  , get_code_name(T301.C901_STATUS_ID) VENDOR_STATUS
  , get_user_name(T301.C301_CREATED_BY) VENDOR_CREATED_BY
  , T301.C301_CREATED_DATE VENDOR_CREATED_DATE
  , get_user_name(T301.C301_LAST_UPDATED_BY) VENDOR_LAST_UPDATED_BY
  , T301.C301_LAST_UPDATED_DATE VENDOR_LAST_UPDATED_DATE
  , C1900_COMPANY_ID Company_Key_Id
  FROM T301_VENDOR T301
  WHERE C1900_COMPANY_ID = 1000 -- 1000:Globus North America
  );
  ALTER TABLE VENDOR_DIM ADD CONSTRAINT VENDOR_DIM_PK PRIMARY KEY (VENDOR_KEY_ID) ENABLE VALIDATE;
  /

