/*
File name: INHOUSE_TRANSACTION_CURRENT_DIM.vw
Created By: gvaithiyanathan
Description: This Materialized view contains Inhouse_transactions data for last year and current year 
SVN Location: @"C:\Projects\Branches\PMT\db\Views\DWH\DW\MaterializedViews\Operations\Current\INHOUSE_TRANSACTION_CURRENT_DIM.vw";
*/
DROP MATERIALIZED VIEW INHOUSE_TRANSACTION_CURRENT_DIM;
CREATE MATERIALIZED VIEW INHOUSE_TRANSACTION_CURRENT_DIM
AS
  (SELECT c412_inhouse_trans_id inhouse_trans_key_id ,
    c412_inhouse_trans_id inhouse_trans_id ,
    c412_comments trans_comments ,
    DECODE(c412_status_fl, 0,'Initiated/Back Order',2,'Pending Control',3,'Pending Verification', '3.30','Packing In Progress','3.60','Ready For Pickup',4,'Completed') inhouse_trans_status ,
    c412_void_fl void_flag ,
    c412_release_to release_to_code_id ,
    DECODE(c412_release_to,50625,t101_release_to_employee.c101_user_f_name
    ||' '
    ||t101_release_to_employee.c101_user_l_name,0,t101_release_to_employee.c101_user_f_name
    ||' '
    ||t101_release_to_employee.c101_user_l_name) release_to_employee ,
    DECODE(c412_release_to,4121,get_rep_name(c412_release_to_id)) release_to_rep_name ,
    c412_inhouse_purpose inhouse_trans_purpose_code_id ,
    NVL(t901_inhouse_trans_purpose.c901_code_nm,' ') inhouse_trans_purpose_name ,
    c412_type inhouse_trans_type_code_id ,
    NVL(t901_inhouse_trans_type.c901_code_nm,' ') inhouse_trans_type_name ,
    c412_update_inv_fl update_inv_flag ,
    c412_verified_date verified_date ,
    NVL(t101_verified_by.c101_user_f_name
    ||' '
    ||t101_verified_by.c101_user_l_name,' ') verified_by ,
    c412_verify_fl verify_flag ,
    NVL(t101_created_by.c101_user_f_name
    ||' '
    ||t101_created_by.c101_user_l_name,' ') created_by ,
    c412_created_date created_date ,
    c412_last_updated_date last_updated_date ,
    NVL(t101_last_updated_by.c101_user_f_name
    ||' '
    ||t101_last_updated_by.c101_user_l_name,' ') last_updated_by ,
    c412_ref_id ref_id ,
    c504a_loaner_transaction_id loaner_transaction_id ,
    c412_expected_return_date expected_return_date
  FROM t412_inhouse_transactions t412,
    t101_user t101_release_to_employee,
    t101_user t101_verified_by,
    t101_user t101_created_by,
    t101_user t101_last_updated_by,
    t901_code_lookup t901_inhouse_trans_purpose ,
    t901_code_lookup t901_inhouse_trans_type
  WHERE t412.c1900_company_id = 1000 -- 1000:globus north america
  AND c412_release_to_id      =t101_release_to_employee.c101_user_id(+)
  AND c412_verified_by        =t101_verified_by.c101_user_id(+)
  AND c412_created_by         =t101_created_by.c101_user_id(+)
  AND c412_last_updated_by    =t101_last_updated_by.c101_user_id(+)
  AND c412_inhouse_purpose    =t901_inhouse_trans_purpose.c901_code_id(+)
  AND c412_type               =t901_inhouse_trans_type.c901_code_id(+)
  AND c412_created_date       >= TRUNC(ADD_MONTHS(SYSDATE,-12),'MM')
  UNION
  SELECT consignment_key_id inhouse_trans_key_id ,
    consignment_id inhouse_trans_id ,
    comments trans_comments ,
    consignment_status inhouse_trans_status ,
    void_fl void_flag ,
    NULL release_to_code_id ,
    NULL release_to_employee ,
    NULL release_to_rep_name ,
    NULL inhouse_trans_purpose_code_id ,
    consignment_inhouse_purpose inhouse_trans_purpose_name ,
    consignment_type_id inhouse_trans_type_code_id ,
    consignment_type_name inhouse_trans_type_name ,
    NULL update_inv_flag ,
    consignment_verified_date verified_date ,
    consignment_verified_by verified_by ,
    NULL verify_flag ,
    consignment_created_by created_by ,
    consignment_created_date created_date ,
    consignment_updated_date last_updated_date ,
    consignment_updated_by last_updated_by ,
    master_consignment_id ref_id ,
    NULL loaner_transaction_id ,
    NULL expected_return_date
  FROM consignment_master_current_dim
  WHERE consignment_type_id IN (4113,4114,4115,4116,9110,56028,104622,400058,400059,400060,400062,400063,400064,400076,400086)
    -- 4113:shelf to quarantine,4114:shelf to repack,4115:quarantine to scrap,4116:quarantine to shelf,9110:ih loaner item,
    -- 56028:ps to rw inventory,104622:qty to quarantine,400058:returns hold to repack,400059:bulk to quarantine,
    -- 400060:bulk to repack,400062:bulk to inv adjustment,400063:repack to shelf,400064:returns hold to quarantine,
    -- 400076:quarantine to bulk,400086:shelf to returns
    -- consignment_master_dim is already filtered on c1900_company_id = 1000
  );
  /
  ALTER TABLE INHOUSE_TRANSACTION_CURRENT_DIM ADD CONSTRAINT INHOUSE_TRANSACTION_CURRENT_DIM_PK PRIMARY KEY (inhouse_trans_key_id) enable VALIDATE;