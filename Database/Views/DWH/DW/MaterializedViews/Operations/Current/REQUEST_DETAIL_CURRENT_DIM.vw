/*
File name: REQUEST_DETAIL_CURRENT_DIM.vw
Created By: gvaithiyanathan
Description: This Materialized view contains Request_details data for last year and current year 
SVN Location: @"C:\Projects\Branches\PMT\db\Views\DWH\DW\MaterializedViews\Operations\Current\REQUEST_DETAIL_CURRENT_DIM.vw";
*/
DROP MATERIALIZED VIEW REQUEST_DETAIL_CURRENT_DIM;
CREATE MATERIALIZED VIEW REQUEST_DETAIL_CURRENT_DIM nocache logging nocompress noparallel refresh force ON demand
AS
  (SELECT t521.c521_request_detail_id request_detail_key_id,
    t521.c520_request_id request_key_id,
    t521.c205_part_number_id part_key_id,
    t521.c521_qty quantity
  FROM t521_request_detail t521,
    t520_request t520
  WHERE t521.c520_request_id  =t520.c520_request_id
  AND t520.c520_created_date  >= TRUNC(ADD_MONTHS(SYSDATE,-12),'MM')
  );