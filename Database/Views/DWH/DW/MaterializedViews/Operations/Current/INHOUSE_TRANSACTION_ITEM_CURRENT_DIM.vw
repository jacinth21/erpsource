/*
File name: INHOUSE_TRANSACTION_ITEM_CURRENT_DIM.vw
Created By: gvaithiyanathan
Description: This Materialized view contains Inhouse_trans_Item data for last year and current year 
SVN Location: @"C:\Projects\Branches\PMT\db\Views\DWH\DW\MaterializedViews\Operations\Current\INHOUSE_TRANSACTION_ITEM_CURRENT_DIM.vw";
*/
DROP MATERIALIZED VIEW INHOUSE_TRANSACTION_ITEM_CURRENT_DIM;
CREATE MATERIALIZED VIEW INHOUSE_TRANSACTION_ITEM_CURRENT_DIM nocache logging nocompress noparallel refresh force ON demand
AS
  (SELECT t413.c413_trans_id inhouse_trans_item_key_id,
    t413.c412_inhouse_trans_id inhouse_trans_id,
    t413.c413_control_number control_number,
    t413.c413_item_qty item_quantity,
    t413.c413_item_price item_price,
    t413.c413_void_fl void_flag,
    t413.c205_part_number_id part_number_id,
    Decode(C413_STATUS_FL,'S','Sold','C','Consigned','W','Rep Will Be Charged','R','Returned','Q','Quarantine') inhouse_trans_item_status --q null
  FROM t413_inhouse_trans_items t413,
    t412_inhouse_transactions t412
  WHERE t413.c412_inhouse_trans_id = t412.c412_inhouse_trans_id
  AND t412.c412_created_date      >= TRUNC(ADD_MONTHS(SYSDATE,-12),'MM')
  UNION ALL
  SELECT item_consignment_key_id inhouse_trans_item_key_id,
    consignment_key_id inhouse_trans_id,
    control_number control_number,
    consignment_qty item_quantity,
    item_price item_price,
    NULL void_flag,
    part_key_id part_number_id,
    NULL inhouse_trans_item_status
  FROM item_consignment_master_current_dim
  );
