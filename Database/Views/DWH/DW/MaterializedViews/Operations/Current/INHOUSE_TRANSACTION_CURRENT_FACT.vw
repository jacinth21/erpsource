/*
File name: INHOUSE_TRANSACTION_CURRENT_FACT.vw
Created By: gvaithiyanathan
Description: This Materialized view contains Inhouse_transactions data for last year and current year 
SVN Location: @"C:\Projects\Branches\PMT\db\Views\DWH\DW\MaterializedViews\Operations\Current\INHOUSE_TRANSACTION_CURRENT_FACT.vw";
*/
DROP MATERIALIZED VIEW INHOUSE_TRANSACTION_CURRENT_FACT;
CREATE MATERIALIZED VIEW INHOUSE_TRANSACTION_CURRENT_FACT NOCACHE NOLOGGING NOCOMPRESS NOPARALLEL REFRESH FORCE ON DEMAND
AS
  (SELECT inhouse_transaction_dim.inhouse_trans_key_id,
    inhouse_transaction_item_dim.inhouse_trans_item_key_id,
    inhouse_transaction_item_dim.part_number_id part_key_id,
    inhouse_transaction_dim.loaner_transaction_id loaner_key_id,
    date_dim.date_key_id,
    inhouse_transaction_dim.ref_id,
    inhouse_transaction_item_dim.control_number,
    inhouse_transaction_item_dim.item_quantity,
    inhouse_transaction_item_dim.item_price
  FROM inhouse_transaction_current_dim inhouse_transaction_dim,
    inhouse_transaction_item_current_dim inhouse_transaction_item_dim,
    date_dim
  WHERE inhouse_transaction_dim.inhouse_trans_id  =inhouse_transaction_item_dim.inhouse_trans_id
  AND TRUNC(inhouse_transaction_dim.created_date) = date_dim.cal_date
  ); 