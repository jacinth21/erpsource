/*
File name: REQUEST_CURRENT_FACT.vw
Created By: gvaithiyanathan
Description: This Materialized view contains Request data for last year and current year 
SVN Location: @"C:\Projects\Branches\PMT\db\Views\DWH\DW\MaterializedViews\Operations\Current\REQUEST_CURRENT_FACT.vw";
*/
DROP MATERIALIZED VIEW REQUEST_CURRENT_FACT;
CREATE MATERIALIZED VIEW REQUEST_CURRENT_FACT NOCACHE NOLOGGING NOCOMPRESS NOPARALLEL REFRESH FORCE ON DEMAND
AS
  (SELECT Request_dim.request_key_id Request_Key_Id,
    Request_Detail_Dim.Request_detail_key_Id Request_Detail_Key_ID,
    Request_dim.set_id Set_Key_Id,
    Request_Detail_Dim.Part_Key_id Part_Key_Id,
    Date_Dim.date_key_id Date_Key_id,
    request_shipping_dim.Shipping_Id Shipping_Key_Id,
    request_Void_dim.void_log_id Void_Log_Key_Id,
    DECODE(Request_Dim.Request_For_ID,40021,Request_Dim.Request_To_ID) Distributor_Key_id,
    DECODE(Request_Dim.Request_For_ID,40022,Request_Dim.Request_To_ID) Employee_Key_id,
    DECODE(Request_Dim.Request_SHIP_TO,4120,Request_Dim.Request_SHIP_TO_ID) Shipto_Distributor_Key_id,
    DECODE(Request_Dim.Request_SHIP_TO,4121,Request_Dim.Request_SHIP_TO_ID) Shipto_Sales_Rep_Key_id,
    DECODE(Request_Dim.Request_SHIP_TO,4122,Request_Dim.Request_SHIP_TO_ID) Shipto_Account_Key_id,
    DECODE(Request_Dim.Request_SHIP_TO,4123,Request_Dim.Request_SHIP_TO_ID) Shipto_Employee_Key_id,
    Request_Detail_Dim.Quantity Request_Qty
  FROM Request_current_Dim Request_Dim,
    request_detail_current_dim Request_Detail_Dim,
    Date_Dim,
    (SELECT shipping_id,
      ref_id
    FROM shipping_dim
    WHERE source_code_id    =50184
    AND NVL(active_fl,'Y') !='N'
    ) request_shipping_dim,
    (SELECT void_log_id,
      void_type_id,
      void_ref_id
    FROM void_dim
    WHERE void_type_id=90199
    ) request_Void_dim
  WHERE Request_Dim.request_key_Id     = Request_Detail_Dim.Request_key_ID(+)
  AND TRUNC(Request_Dim.Required_Date) = Date_Dim.cal_date(+)
  AND Request_Dim.Request_key_Id       = request_shipping_dim.ref_Id(+)
  AND Request_dim.request_key_id       = request_Void_dim.void_ref_id (+)
  );