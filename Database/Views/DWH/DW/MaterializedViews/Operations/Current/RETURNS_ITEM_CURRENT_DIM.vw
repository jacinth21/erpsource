/*
File name: RETURNS_ITEM_CURRENT_DIM.vw
Created By: gvaithiyanathan
Description: This Materialized view contains Returns_item data for last year and current year 
SVN Location: @"C:\Projects\Branches\PMT\db\Views\DWH\DW\MaterializedViews\Operations\Current\RETURNS_ITEM_CURRENT_DIM.vw";
*/
DROP MATERIALIZED VIEW RETURNS_ITEM_CURRENT_DIM;
CREATE MATERIALIZED VIEW RETURNS_ITEM_CURRENT_DIM nocache logging nocompress noparallel refresh force ON demand
AS
  SELECT t507.c507_returns_item_id returns_item_key_id ,
    t507.c506_rma_id returns_key_id ,
    t507.c205_part_number_id part_key_id ,
    t507.c507_item_qty item_qty ,
    t507.c507_item_price item_price ,
    t507.c507_control_number control_number ,
    DECODE(t507.c507_status_fl,'C','Return','M','Missing','Q','Initiated','R','Return','W','Write Off') status_fl ,
    t507.c507_org_control_number original_control_number ,
    t507.c901_type returns_item_type_id ,
    t901.c901_code_nm returns_item_type ,
    t507.c507_hold_fl hold_flag ,
    t507.c507_missing_fl missing_flag ,
    t507.c5010_tag_id tag_id ,
    t507.c901_missing_tag_reason missing_tag_reason_id ,
    NVL(t901_missing_tag_reason.c901_code_nm,' ') missing_tag_reason ,
    t507.c507_reconciled_date reconciled_date ,
    t507.c507_reconciled_by reconciled_by
  FROM t507_returns_item t507,
    t506_returns t506,
    t901_code_lookup t901,
    t901_code_lookup t901_missing_tag_reason
  WHERE t507.c506_rma_id          =t506.c506_rma_id
  AND t507.c901_type              = t901.c901_code_id(+)
  AND t507.c901_missing_tag_reason=t901_missing_tag_reason.c901_code_id(+)
  AND t506.c506_created_date     >= TRUNC(ADD_MONTHS(SYSDATE,-12),'MM');