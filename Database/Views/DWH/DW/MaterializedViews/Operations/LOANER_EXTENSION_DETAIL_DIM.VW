DROP MATERIALIZED VIEW LOANER_EXTENSION_DETAIL_DIM;

CREATE MATERIALIZED VIEW LOANER_EXTENSION_DETAIL_DIM
NOCACHE
NOLOGGING
NOCOMPRESS
NOPARALLEL
REFRESH FORCE ON DEMAND
AS
(
SELECT
c504a_loaner_transaction_id Loaner_Extension_Key_Id,
c504a_loaner_transaction_id  Loaner_Transaction_Id,
c504a_expected_return_dt Expected_Date,
c901_code_nm Reason_Type,
decode(c901_status,'1901',c504a_extension_comments,c504a_appr_rej_comments) Comments,
c101_user_f_name || ' ' || c101_user_l_name Created_By,  
decode(c901_status,'1901',c504a_ext_requested_date,c504a_ext_appr_rej_dt) Created_Date
FROM t504a_loaner_transaction t504a, t901_code_lookup t901, t101_user t101
WHERE t901.c901_code_id = t504a.c901_reason_type
AND decode(c901_status,1901,t504a.c504a_ext_requested_by,t504a.c504a_status_changed_by) = t101.c101_user_id(+)
AND c901_status is not null
AND c901_void_fl is null
AND c504a_void_fl  is null);

ALTER TABLE LOANER_EXTENSION_DETAIL_DIM ADD 
CONSTRAINT LOANER_EXTENSION_DETAIL_DIM_PK
PRIMARY KEY  (Loaner_Extension_Key_Id)
ENABLE VALIDATE;