/*
File name: RETURNS_FACT.vw
Created By: gvaithiyanathan
Description: This is a view combines both RETURNS_HISTORICAL_FACT and RETURNS_CURRENT_FACT
SVN Location: @"C:\Projects\Branches\PMT\db\Views\DWH\DW\MaterializedViews\Opreations\RETURNS_FACT.vw";
*/

CREATE OR REPLACE VIEW RETURNS_FACT
AS
  (SELECT * FROM RETURNS_HISTORICAL_FACT
  UNION ALL
  SELECT * FROM RETURNS_CURRENT_FACT
  );
  
  /
