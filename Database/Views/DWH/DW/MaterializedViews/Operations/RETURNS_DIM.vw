/*
File name: RETURNS_DIM.vw
Created By: gvaithiyanathan
Description: This is a view combines both RETURNS_HISTORICAL_DIM and RETURNS_CURRENT_DIM
SVN Location: @"C:\Projects\Branches\PMT\db\Views\DWH\DW\MaterializedViews\Opreations\RETURNS_DIM.vw";
*/

CREATE OR REPLACE VIEW RETURNS_DIM
AS
  (SELECT * FROM RETURNS_HISTORICAL_DIM
  UNION ALL
  SELECT * FROM RETURNS_CURRENT_DIM
  );
  
  /