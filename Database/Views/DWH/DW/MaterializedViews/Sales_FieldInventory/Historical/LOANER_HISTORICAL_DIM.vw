/*
File name: LOANER_HISTORICAL_DIM.vw
Created By: gvaithiyanathan
Description: This Materialized view contains loaner data since beginning to last but before year. 
SVN Location: @"C:\Projects\Branches\PMT\db\Views\DWH\DW\MaterializedViews\Sales_FieldInventory\Historical\LOANER_HISTORICAL_DIM.vw";
This is modified for PMT-27831 and retrieve loaner status from function
*/
DROP MATERIALIZED VIEW LOANER_HISTORICAL_DIM;
CREATE MATERIALIZED VIEW LOANER_HISTORICAL_DIM
AS
  (SELECT to_number (t504a.c504a_loaner_transaction_id) loaner_key_id ,
    to_number (t504a.c504a_loaner_transaction_id) loaner_id ,
    t504a.c504a_loaner_dt loaner_date ,
    t504a.c504a_expected_return_dt expected_return_date ,
    c504a_return_dt return_date ,
    NVL(c901_code_nm,' ') consign_to_party_type ,
    c504a_is_loaner_extended is_loaner_extended ,
    c504a_is_replenished is_replenished ,
    t504b.c504a_etch_id etch_id ,
    t504b.c504a_available_date availabl_date ,
    REPLACE(get_cs_rep_loaner_bill_nm_add(t504a.c504_consignment_id),'<BR>','; ') consign_to_party ,
    t504a.c504_consignment_id consignment_id ,
    (NVL(c504a_return_dt, TRUNC(sysdate)) - t504a.c504a_loaner_dt) usage_days ,
    DECODE(C504a_Return_Dt, NULL,WORKING_DAYS(TRUNC(T504a.C504a_Loaner_Dt),TRUNC(CURRENT_DATE)),0) Bus_Usage_Days ,
    DECODE(c504a_return_dt, NULL,TRUNC(sysdate)- t504a.c504a_expected_return_dt,0) days_overdue ,
    DECODE(C504a_Return_Dt, NULL,WORKING_DAYS(TRUNC(T504a.C504a_Expected_Return_Dt),TRUNC(CURRENT_DATE)),0) Bus_Days_Overdue ,
    get_dw_loaner_status(c504a_status_fl) consigned_loaner_status ,
    t504a.c901_consigned_to consigned_to_code_id ,
    DECODE(t504a.c901_consigned_to,50170,'Distributor',50172,'Employee',50171,'Hospital') consigned_to ,
    DECODE(t504a.c901_consigned_to,50170,t504a.c504a_consigned_to_id) consigned_distributor_id ,
    DECODE(t504a.c901_consigned_to,50172, t101_consigned_employee_name.c101_user_f_name
    || ' '
    || t101_consigned_employee_name.c101_user_l_name) consigned_employee_name ,
    DECODE(t504a.c901_consigned_to,50171,t504a.c504a_consigned_to_id) consigned_account_id ,
    NVL(t101_consigment_created_by.c101_user_f_name
    || ' '
    || t101_consigment_created_by.c101_user_l_name,' ') consigment_created_by ,
    t504a.c504a_created_date consignment_created_date ,
    NVL(t101_last_updated_by.c101_user_f_name
    || ' '
    || t101_last_updated_by.c101_user_l_name,' ') last_updated_by ,
    t504a.c504a_last_updated_date last_updated_date ,
    t504a.c703_sales_rep_id sales_rep_id ,
    T704.C704_Account_Nm Account_Name,
    T703.C703_Sales_Rep_Name ASS_Sales_Rep_Name,
    t504a.c504a_parent_loaner_txn_id parent_loaner_transaction_id ,
    t504a.c526_product_request_detail_id product_request_detail_id ,
    c504a_transf_ref_id product_transfer_reference_id ,
    c504a_transf_date product_transfer_date ,
    NVL(t101_product_transfer_by.c101_user_f_name
    || ' '
    || t101_product_transfer_by.c101_user_l_name,' ') product_transfer_by ,
    c504a_transf_email_flg product_transfer_email_flag ,
    t504a.c1900_company_id company_key_id ,
    t504a.c5040_plant_id plant_key_id
  FROM t504a_loaner_transaction t504a ,
    t504a_consignment_loaner t504b ,
    t101_user t101_consigned_employee_name ,
    t101_user t101_consigment_created_by ,
    t101_user t101_last_updated_by ,
    t101_user t101_product_transfer_by ,
    t901_code_lookup t901,
    T703_Sales_Rep T703,
    t704_account t704
  WHERE t504a.c504_consignment_id = t504b.c504_consignment_id
  AND t504a.c504a_void_fl        IS NULL
  AND t504b.c504a_void_fl        IS NULL
    -- AND t504a.c5040_plant_id        = 3000 -- 3000:audubon
    --AND t504b.c5040_plant_id        = 3000 -- 3000:audubon
  AND t504a.c504a_consigned_to_id =t101_consigned_employee_name.c101_user_id(+)
  AND t504a.c504a_created_by      =t101_consigment_created_by.c101_user_id(+)
  AND t504a.c504a_last_updated_by =t101_last_updated_by.c101_user_id(+)
  AND c504a_transf_by             =t101_product_transfer_by.c101_user_id(+)
  AND t901.c901_code_id(+)        =c901_consigned_to
  AND T504a.C703_Ass_Rep_Id       =T703.C703_Sales_Rep_Id(+)
  AND T504a.C704_Account_Id       =T704.C704_Account_Id (+)
  AND t504a.c504a_created_date    <= LAST_DAY(ADD_MONTHS(SYSDATE,-13))
  );
  
  ALTER TABLE LOANER_HISTORICAL_DIM ADD CONSTRAINT LOANER_HISTORICAL_DIM_PK PRIMARY KEY (loaner_key_id) enable VALIDATE;  