/*
File name: ITEM_ORDER_HISTORICAL_DIM.vw
Created By: gvaithiyanathan
Description: This Materialized view contains item_order data upto last 2years. 
SVN Location: @"C:\Projects\Branches\PMT\db\Views\DWH\DW\MaterializedViews\Sales_FieldInventory\Historical\ITEM_ORDER_HISTORICAL_DIM.vw";
Change History: PMT-30713 Changed to Order date from created date to avoid time stamp
*/
DROP MATERIALIZED VIEW ITEM_ORDER_HISTORICAL_DIM;
CREATE MATERIALIZED VIEW ITEM_ORDER_HISTORICAL_DIM nocache logging nocompress noparallel refresh force ON demand
AS
  (SELECT to_number(t502.c502_item_order_id) item_order_key_id ,
    TO_CHAR(t502.c502_item_order_id) item_order_id ,
    t502.c502_control_number control_num ,
    NVL(t901_part_type.c901_code_nm,' ') order_part_type ,
    t502.c502_construct_fl construct_fl ,
    t502.c501_order_id order_id ,
    t502.c205_part_number_id part_id ,
    t502.c502_item_qty item_qty ,
    t502.c502_corp_item_price item_price ,
    t502.c502_item_price ext_item_price ,
    t901_unit_price_code.c901_code_nm unit_price_adj_code,
    t502.c502_unit_price_adj_value unit_price_adj_value,
    t901_discount_type.c901_code_nm discount_type,
    t502.c502_system_cal_unit_price system_cal_unit_price,
    t502.c502_discount_offered discount_offered,
    t502.c502_do_unit_price do_unit_price,
    t502.c502_tax_amt tax_amt,
    t502.c502_vat_rate ext_vat_rate
  FROM t502_item_order t502,
    t501_order t501,
    t901_code_lookup t901_part_type,
    t901_code_lookup t901_unit_price_code,
    t901_code_lookup t901_discount_type
  WHERE t501.c501_order_id                    = t502.c501_order_id
  AND t502.c502_void_fl                       IS NULL
  AND t502.c502_delete_fl                     IS NULL
  AND t502.c901_type                          =t901_part_type.c901_code_id(+)
  AND NVL(t502.c901_unit_price_adj_code,-999) =t901_unit_price_code.c901_code_id(+)
  AND NVL(t502.c901_discount_type,-999)       =t901_discount_type.c901_code_id(+)
  AND t501.c501_order_date                    <= LAST_DAY(ADD_MONTHS(TRUNC(SYSDATE),-13))
  );
  ALTER TABLE ITEM_ORDER_HISTORICAL_DIM ADD CONSTRAINT ITEM_ORDER_HISTORICAL_DIM_PK PRIMARY KEY (item_order_key_id) enable VALIDATE;