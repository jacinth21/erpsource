-- @"c:\database\dw_bi\materializedviews\sales_fieldinventory\sales_fact_till_previous_year.vw"
DROP materialized VIEW sales_fact_till_previous_year;
CREATE materialized VIEW sales_fact_till_previous_year nocache logging nocompress noparallel USING INDEX refresh force ON demand
AS
  (SELECT DISTINCT order_dim.order_key_id ,
    order_dim.order_id ,
    gm_dw_pkg_sm_salesfact.get_do_id(order_dim.order_id) do_id ,
	order_dim.invoice_number as invoice_key_id,
    item_order_dim.item_order_key_id ,
    item_order_dim.item_order_id ,
    order_dim.account_key_id ,
    order_dim.account_id ,
    order_dim.sales_rep_key_id ,
    order_dim.sales_rep_id ,
	item_order_dim.part_id part_key_id ,
    item_order_dim.part_id part_id ,
    set_dim.set_key_id system_key_id ,
    set_dim.set_key_id system_id ,
    gm_dw_pkg_pd_partdata.get_main_set_id_for_part(item_order_dim.part_id, set_dim.set_key_id) set_key_id ,
    date_dim.date_key_id date_key_id ,
    order_dim.ous_country_id ,
    to_number(DECODE(order_dim.order_type_id, '2524', NULL, item_order_dim.item_qty)) item_qty ,
    date_dim.cal_date order_dt ,
    item_order_dim.item_price ,
    item_order_dim.ext_item_price ,
	item_order_dim.unit_price_adj_value,
	item_order_dim.system_cal_unit_price,
	item_order_dim.do_unit_price,
	item_order_dim.tax_amt,
    item_order_dim.item_price     * item_order_dim.item_qty item_sales ,
    item_order_dim.ext_item_price * item_order_dim.item_qty ext_item_sales ,
    NVL(item_order_dim.ext_vat_rate,0) ext_vat_rate ,
    item_order_dim.ext_item_price * item_order_dim.item_qty +(item_order_dim.ext_item_price * item_order_dim.item_qty * (NVL(item_order_dim.ext_vat_rate,0)/100)) ext_item_sales_withvat ,
    order_dim.order_type ,
    order_dim.ship_cost ,
    order_dim.ext_ship_cost
    --, get_ac_cogs_value(item_order_dim.c205_part_number_id,4900) cogs_cost
    --, decode(gm_dw_pkg_sm_salesfact.get_first_order_for_acc(order_dim.c704_account_id), order_dim.c501_order_id, 'y','n') first_sale_flag
    ,
    shipping_key_id shipping_key_id
  FROM order_dim order_dim ,
    item_order_dim item_order_dim ,
    set_dim set_dim ,
    set_part_dim set_part_dim ,
    date_dim date_dim ,
    (SELECT DISTINCT ref_id,shipping_key_id FROM shipping_dim
    ) shipping_dim
  WHERE item_order_dim.order_id   = order_dim.order_key_id
  AND set_dim.set_key_id          = set_part_dim.set_key_id
  AND set_part_dim.part_number_id = item_order_dim.part_id
  AND order_dim.order_key_id      = shipping_dim.ref_id (+)
    --and c207_void_fl is null
    --and order_dim.c501_void_fl is null   checked void in dim table
    --and order_dim.c501_delete_fl is null
    --and item_order_dim.c502_void_fl is null
    --and item_order_dim.c502_delete_fl is null
  AND set_dim.set_group_type_id = 1600
    --and nvl (order_dim.order_type, -9999) not in ('2533','2518','2519') --validated in dim
  AND date_dim.cal_date   = order_dim.order_dt
  AND order_dim.order_dt <= TRUNC(sysdate, 'year')
  );