/*
File name: ORDER_CURRENT_DIM.vw
Created By: gvaithiyanathan
Description: This Materialized view contains order data for last year and current year  
SVN Location: @"C:\Projects\Branches\PMT\db\Views\DWH\DW\MaterializedViews\Sales_FieldInventory\Current\ORDER_CURRENT_DIM.vw";
Change History: PMT-30713 Changed to Order date from created date to avoid time stamp
*/
DROP MATERIALIZED VIEW ORDER_CURRENT_DIM;
CREATE MATERIALIZED VIEW ORDER_CURRENT_DIM
AS
  (SELECT t501.c501_order_id order_key_id ,
    t501.c501_order_id order_id ,
    t501.c501_parent_order_id parent_order_id ,
    t501.c501_order_date order_dt ,
    c501_customer_po customer_po ,
    c501_order_date_time order_date_time ,
    NVL(t901_receive_mode.c901_code_nm,' ') receive_mode ,
    DECODE(t501.c501_status_fl,'0','Back Order','1','Pending Control Number','2','Pending Shipment','2.30', 'Packing In Progress','2.60','Ready For Pickup','3','Shipped','4','Shipped','5','Shipped') status ,
    NVL(t901_order_type.c901_code_nm,' ') order_type ,
    c901_order_type order_type_id ,
    gm_dw_pkg_sm_salesfact.get_do_id(t501.c501_order_id) do_id ,
    t501.c503_invoice_id invoice_number ,
    t501.c501_shipping_date ship_date ,
    t501. c501_surgery_date surgery_date,
    t501.c501_tracking_number tracking_number ,
    NVL(t901_reason_type.c901_code_nm,' ') reason_type ,
    c501_hold_fl hold_flag ,
    c501_hist_hold_fl hist_hold_fl,
    NVL(t901_delivery_carrier.c901_code_nm,' ') delivery_carrier ,
    NVL(t901_delivery_mode.c901_code_nm,' ') delivery_mode ,
    NVL(t101_order_created_by.c101_user_f_name
    || ' '
    || t101_order_created_by.c101_user_l_name,' ') order_created_by ,
    NVL(t101_order_last_updated_by.c101_user_f_name
    || ' '
    || t101_order_last_updated_by.c101_user_l_name,' ') order_last_updated_by ,
    t501.c501_last_updated_date order_last_updated_date ,
    dealer.dealer_name ,
    dealer.party_id dealer_id,
    t501.c501_ship_charge_fl ship_charge_fl ,
    t501.c501_update_inv_fl update_inventory_flag ,
    t501.c506_rma_id return_id ,
    NVL(c501_ext_ref_id,t501.c501_order_id) ous_order_id ,
    c901_ext_country_id ous_country_id ,
    NVL(t901_ous_country_name.c901_code_nm,' ') ous_country_name ,
    to_number(t501.c704_account_id) account_key_id ,
    t501.c704_account_id account_id ,
    t501.c703_sales_rep_id sales_rep_key_id ,
    t501.c703_sales_rep_id sales_rep_id ,
    t501.c501_ship_cost ship_cost ,
    c501_ext_ship_cost ext_ship_cost ,
    c1900_company_id company_key_id ,
    c5040_plant_id plant_key_id
  FROM t501_order t501 ,
    t901_code_lookup t901_receive_mode ,
    t901_code_lookup t901_order_type ,
    t901_code_lookup t901_reason_type ,
    t901_code_lookup t901_delivery_carrier ,
    t901_code_lookup t901_delivery_mode ,
    t901_code_lookup t901_ous_country_name ,
    t101_user t101_order_created_by ,
    t101_user t101_order_last_updated_by,
    (SELECT c101_party_id party_id,
      NVL(c101_party_nm_en, ' ') dealer_name
    FROM t101_party
    WHERE c101_void_fl IS NULL
    )dealer
  WHERE t501.c501_void_fl                   IS NULL
  AND t501.c501_delete_fl                   IS NULL
  AND NVL (t501.c901_order_type, -9999) NOT IN ('2533','2518','2519','101260','2520','2534','102364')
    -- 2519:pending cs confirmation, 2518:draft, 2533:ics, 101260:acknowledgement order,2520:quote,2534:ict returns order,102364:stock transfer
  AND c501_receive_mode         =t901_receive_mode.c901_code_id(+)
  AND c901_order_type           =t901_order_type.c901_code_id(+)
  AND t501.c901_reason_type     =t901_reason_type.c901_code_id(+)
  AND t501.c501_delivery_carrier=t901_delivery_carrier.c901_code_id(+)
  AND t501.c501_delivery_mode   =t901_delivery_mode.c901_code_id(+)
  AND c901_ext_country_id       =t901_ous_country_name.c901_code_id(+)
  AND t501.c501_created_by      =t101_order_created_by.c101_user_id(+)
  AND t501.c501_last_updated_by =t101_order_last_updated_by.c101_user_id(+)
  AND t501.c101_dealer_id       =dealer.party_id(+)
  AND t501.c501_order_date      >= TRUNC(ADD_MONTHS(TRUNC(SYSDATE),-12),'MM')
  );
  ALTER TABLE ORDER_CURRENT_DIM ADD CONSTRAINT ORDER_CURRENT_DIM_PK PRIMARY KEY (order_key_id) enable VALIDATE; 