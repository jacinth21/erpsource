DROP MATERIALIZED VIEW DISTRIBUTOR_DIM;
CREATE MATERIALIZED VIEW DISTRIBUTOR_DIM
AS
  ( SELECT DISTINCT TO_NUMBER(t701.c701_distributor_id) distributor_key_id
  , t701.c701_distributor_id distributor_id
  , nvl(t701.c701_distributor_name_en,t701.c701_distributor_name) distributor_name
  , t701.c701_region region_key_id
  , t701.C701_CONTRACT_START_DATE Contract_start_date
  , t701.C701_CONTRACT_END_DATE Contract_end_date
  , C701_ACTIVE_FL active_fl
  , get_code_name(t701.c701_region) region_name
  , v700.ad_id ad_key_id
  , v700.ad_id ad_id
  , v700.ad_name ad_name
  , t701.c701_ship_add1 ship_address1
  , t701.c701_ship_add2 ship_address2
  , t701.c701_ship_zip_code ship_zip_code
  , t701.c701_ship_city ship_city
  , get_code_name(t701.c701_ship_state) ship_state
  , get_code_name(t701.c701_ship_country) ship_country
  , t701.c901_distributor_type Distributor_type_id
  , get_code_name(t701.c901_distributor_type) TYPE
  , C701_BILL_ADD1
    || C701_BILL_ADD2 Billing_address
  , C701_BILL_CITY Billing_city
  , get_code_name(C701_BILL_STATE) Billing_state
  , get_code_name(C701_BILL_COUNTRY) Billing_country
  , t701.c701_bill_zip_code Billing_Zip_Code
  , C701_EXT_REF_ID ext_ref_id
  , DECODE(t701.c901_distributor_type,70105,t701.c701_region,C901_EXT_COUNTRY_ID) ext_country_id
  , get_code_name(DECODE(t701.c901_distributor_type,70105,t701.c701_region,C901_EXT_COUNTRY_ID)) ext_country_name
  , C701_BILL_LATITUDE billing_latitude
  , C701_BILL_LONGITUDE billing_longitude
  , C701_SHIP_LATITUDE shipping_latitude
  , C701_SHIP_LONGITUDE shipping_longitude
  , get_code_name(t701.c901_commission_type) commission_type
    --, get_mapping_countryid((t701.c701_ship_country)) RFS_Entity_Id
  , (
    CASE
      WHEN t701.c901_distributor_type IN ('70101','70102','70100')
      THEN 80120
      ELSE get_mapping_countryid((t701.c701_ship_country))
    END) RFS_Entity_Id
  , get_code_name(
    CASE
      WHEN t701.c901_distributor_type IN ('70101','70102','70100')
      THEN 80120
      ELSE get_mapping_countryid(t701.c701_ship_country)
    END) RFS_Entity_Name
  , NVL(C101_PARTY_INTRA_MAP_ID,-999) PARTYID
    --, get_code_name(get_mapping_countryid(t701.c701_ship_country)) RFS_Entity_Name
  , C1900_COMPANY_ID Company_Key_Id
  FROM t701_distributor t701
  , v700_territory_mapping_detail v700
  WHERE t701.c701_distributor_id = v700.d_id (+)
  AND t701.c701_void_fl         IS NULL
  );
  -- 70101 Direct Rep,  70102 Commission, 70100 Distributor
  ALTER TABLE DISTRIBUTOR_DIM ADD CONSTRAINT DISTRIBUTOR_DIM_PK PRIMARY KEY (DISTRIBUTOR_KEY_ID) ENABLE VALIDATE;