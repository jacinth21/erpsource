DROP MATERIALIZED VIEW CURRENT_SALES;

CREATE MATERIALIZED VIEW CURRENT_SALES
NOCACHE
LOGGING
NOCOMPRESS
NOPARALLEL
REFRESH FORCE ON DEMAND
AS 
(SELECT MAX(T501.c501_order_date) CURRENT_SALES_DATE FROM t501_order T501);

CREATE OR REPLACE VIEW CURRENT_INFO
AS
(
SELECT MAX(CAL_DATE) CURRENT_BUSINESS_DATE  FROM DATE_DIM WHERE CAL_DATE <= TRUNC(SYSDATE) AND BUS_DAY_IND = 'Y'
);

CREATE OR REPLACE VIEW globus_bi_realtime.mtd_sales_info
AS
   (SELECT   TO_CHAR (t501.c501_order_date, 'MM/YYYY') mm_yyyy,
             SUM (t502.c502_item_price * t502.c502_item_qty) mtd_sales
        FROM t501_order t501, t502_item_order t502
       WHERE t502.c501_order_id = t501.c501_order_id
         AND t501.c501_void_fl IS NULL
         AND t501.c501_delete_fl IS NULL
         AND t502.c502_void_fl IS NULL
         AND t502.c502_delete_fl IS NULL
         AND NVL (t501.c901_order_type, -9999) NOT IN ('2533','2518','2519')
         AND t501.c501_order_date >= trunc(last_day(add_months(sysdate,-1))+1)
    GROUP BY TO_CHAR (t501.c501_order_date, 'MM/YYYY'));