-- This is a derived fact table ACCOUNT_SYSTEM_FIRST_FACT that is created in the globus_information_universe

SELECT  system_key_id, account_key_id, MIN (first_order_date) as first_sale_date,
                  MAX (last_order_date) as last_sale_date,
                  MIN (first_sale_date_key_id)  as first_sale_date_key_id,
                  MAX (last_sale_date_key_id) as last_sale_date_key_id,
                  SUM(ITEM_SALES) KEEP (DENSE_RANK FIRST ORDER BY (to_char(first_order_date,'YYYY/MM' )) )  first_mon_item_sales
             FROM (SELECT   sf.system_key_id,sf.account_key_id,
                            MIN (sf.order_dt) first_order_date,
                            MAX (sf.order_dt) last_order_date,
                            MIN (sf.date_key_id) first_sale_date_key_id,
                            MAX (sf.date_key_id) last_sale_date_key_id,
                       SUM(sf.ITEM_SALES) item_sales
                   FROM     sales_fact sf
                   GROUP BY sf.system_key_id,sf.account_key_id, TRUNC (sf.order_dt, 'MONTH')
                     HAVING SUM (sf.item_sales) > 0)
GROUP by system_key_id, account_key_id      
