/*
File name: ORDER_DIM.vw
Created By: gvaithiyanathan
Description: This is a view combines both ORDER_HISTORICAL_DIM and ORDER_CURRENT_DIM
SVN Location: @"C:\Projects\Branches\PMT\db\Views\DWH\DW\MaterializedViews\Sales_FieldInventory\ORDER_DIM.vw";
*/

CREATE OR REPLACE VIEW ORDER_DIM
AS
  (SELECT * FROM ORDER_HISTORICAL_DIM
   UNION ALL
  SELECT * FROM ORDER_CURRENT_DIM
  ); 
/  