-- @"C:\Datawarehouse\DW_BI\scripts\Globus_SQL_Query_BO_Universe\Accounting\Scheduled_Reports\Month_End_Reports\GMNL\VDW_GMNL_Acc_Ops_Sales_CN_Rpt.vw";

CREATE OR REPLACE VIEW VDW_GMNL_Acc_Ops_Sales_CN_Rpt
AS
      SELECT p_num, p_desc, prod_family
      , fs_warehouse_qty, acc_warehouse_qty, ops_qty
      , cogs_qty, get_plant_name(plant_id) plant_name
       FROM
        (
             SELECT t205.c205_part_number_id p_num, t205.c205_part_num_desc p_desc, get_code_name (
                t205.c205_product_family) prod_family, NVL (fs_warehouse.field_ws_qty, 0) fs_warehouse_qty, NVL (
                account_warehouse.acc_ws_qty, 0) acc_warehouse_qty, (NVL (fs_warehouse.field_ws_qty, 0) + NVL (
                account_warehouse.acc_ws_qty, 0)) ops_qty, NVL (accounting_qty.account_qty, 0) cogs_qty, accounting_qty.plant_id plant_id
                --, NVL ( op_total.f_value, 0) + NVL(bill_only_cn.bill_only_CN_qty,0) ops_qty
               FROM t205_part_number t205, T2023_PART_COMPANY_MAPPING t2023, (
                     SELECT t820.c205_part_number_id p_num, SUM (NVL (t820.c820_qty_on_hand, 0)) account_qty, t820.C5040_PLANT_ID plant_id
                       FROM t820_costing t820
                      WHERE t820.c901_status      IN (4800, 4801) -- 4800 Pending Progress --4801 Active
                        AND t820.c901_costing_type = 4904 -- US sales consignment
                        AND t820.c820_delete_fl                   = 'N'
                        AND t820.C1900_COMPANY_ID  = 1010
                   GROUP BY t820.c205_part_number_id, t820.C5040_PLANT_ID
                )
                accounting_qty, (
                     SELECT t5053.c205_part_number_id pnum, SUM (NVL (t5053.c5053_curr_qty, 0)) field_ws_qty, t5052.C5040_PLANT_ID plant_id
                       FROM t5053_location_part_mapping t5053, t5051_inv_warehouse t5051, t5052_location_master t5052
                      , t205_part_number t205
                      WHERE t5051.c5051_inv_warehouse_id = t5052.c5051_inv_warehouse_id
                        AND t205.c205_part_number_id     = t5053.c205_part_number_id
                        AND t5053.c5052_location_id      = t5052.c5052_location_id
                        AND t5051.c5051_inv_warehouse_id = 3 -- FS warehouse
                        AND t5052.c5052_void_fl IS NULL
                        AND (t5052.c1900_company_id      = 1010
                        OR t5052.c1900_company_id       IN
                        (
                             SELECT c1900_company_id
                               FROM t5041_plant_company_mapping
                              WHERE c5040_plant_id = 3003
                        ))
                   GROUP BY t5053.c205_part_number_id, t5052.C5040_PLANT_ID 
                )
                fs_warehouse, (
                     SELECT t5053.c205_part_number_id pnum, SUM (NVL (t5053.c5053_curr_qty, 0)) acc_ws_qty, t5052.C5040_PLANT_ID plant_id
                       FROM t5053_location_part_mapping t5053, t5051_inv_warehouse t5051, t5052_location_master t5052
                      , t205_part_number t205
                      WHERE t5051.c5051_inv_warehouse_id = t5052.c5051_inv_warehouse_id
                        AND t205.c205_part_number_id     = t5053.c205_part_number_id
                        AND t5053.c5052_location_id      = t5052.c5052_location_id
                        AND t5051.c5051_inv_warehouse_id = 5 -- Account warehouse
                        AND t5052.c5052_void_fl IS NULL
                        AND (t5052.c1900_company_id      = 1010
                        OR t5052.c1900_company_id       IN
                        (
                             SELECT c1900_company_id
                               FROM t5041_plant_company_mapping
                              WHERE c5040_plant_id = 3003
                        ))
                   GROUP BY t5053.c205_part_number_id, t5052.C5040_PLANT_ID 
                )
                account_warehouse
              WHERE t205.c205_part_number_id = t2023.c205_part_number_id
                AND t205.c205_part_number_id = accounting_qty.p_num(+)
                AND t2023.C1900_COMPANY_ID   = 1010
                AND t2023.c2023_void_fl      IS NULL
                AND t205.c205_part_number_id = fs_warehouse.pnum (+)
                AND t205.c205_part_number_id = account_warehouse.pnum (+)
        )
      WHERE (fs_warehouse_qty <> 0
        OR acc_warehouse_qty  <> 0
        OR ops_qty            <> 0
        OR cogs_qty           <> 0)
/        