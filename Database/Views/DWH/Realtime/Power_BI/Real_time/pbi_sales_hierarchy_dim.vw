/*
File name: pbi_sales_hierarchy_dim.vw
Created By: YKannan
Description: Fetch non voided v700_territory_mapping_detail data which is needed to drill down the sales by different factors. 
	Zone short name is available in t901_code_lookup alt_name column. So joined with that.
SVN Location: @"C:\Projects\Branches\PMT\Datawarehouse\DW_BI\Power_BI_Dashboard\Real_time\pbi_sales_hierarchy_dim.vw";
*/

CREATE OR REPLACE VIEW pbi_sales_hierarchy_dim
AS
  ( SELECT DISTINCT divid universe_id,
    divname universe_name,
    gp_id zone_id ,
    gp_name zone_name,
	t901_zone.c902_code_nm_alt zone_short_name,
    region_id region_id,
    region_name region_name,
    ter_id territory_id,
    ter_name territory_name,
    ad_id area_director_id ,
    ad_name area_director_name ,
    ac_id     account_id,
    ac_name || ''     account_name,
    vp_id vice_president_id ,
    vp_name vice_president_name,
    rep_id sales_rep_id,
    rep_name sales_rep_name,
	d_id distributor_id,
    d_name distributor_name,
    compid division_id,
    compname division_name,
	d_compid company_id
  FROM v700_territory_mapping_detail v700, t901_code_lookup t901_zone
  WHERE v700.gp_id = t901_zone.c901_code_id (+)
  AND t901_zone.c901_void_fl is null
  );