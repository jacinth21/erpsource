/*
File name: pbi_company_dim.vw
Created By: YKannan
Description: Fetch non voided active company dimension data
SVN Location: @"C:\Projects\Branches\PMT\Datawarehouse\DW_BI\Power_BI_Dashboard\Real_time\pbi_company_dim.vw";
*/

CREATE OR REPLACE VIEW pbi_company_dim
AS
  (SELECT 
    c1900_company_id company_id,
    c1900_company_name company_name,
    c1900_company_cd company_short_name,    
    c901_comp_status status,
    c901_country_id country_id
  FROM t1900_company
  WHERE c1900_void_fl IS NULL
    AND c901_comp_status =105320 -- Active
  );
  