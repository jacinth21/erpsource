/*
File name: pbi_region_company_dim.vw
Created By: YKannan
Description: Fetch non voided region and company mapping data
SVN Location: @"C:\Projects\Branches\PMT\Datawarehouse\DW_BI\Power_BI_Dashboard\Real_time\pbi_region_company_dim.vw";
*/

CREATE OR REPLACE VIEW pbi_region_company_dim
AS
  (SELECT 
    c1900_company_id company_id,
    c901_region_id region_id  
  FROM t707_region_company_mapping
  WHERE c707_void_fl IS NULL
  );