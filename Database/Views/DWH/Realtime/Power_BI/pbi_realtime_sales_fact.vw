/*
File name: pbi_realtime_sales_fact.vw
Created By: YKannan
Description: Fetch non voided sales data from t501_order, t502_item_order tables and pbi_mv_system_dim for current month by 
	order type excluding following order types, 
	2519:Pending CS Confirmation, 2518:Draft, 2533:ICS, 101260:Acknowledgement Order,2520:Quote,2534:ICT returns Order,102364:Stock Transfer
SVN Location: @"C:\Projects\Branches\PMT\Datawarehouse\DW_BI\Power_BI_Dashboard\Real_time\pbi_realtime_sales_fact.vw";
*/

CREATE OR REPLACE VIEW pbi_realtime_sales_fact AS
  (SELECT t501.c501_order_id order_id,
    t501.c501_order_date order_date,
    t501.c704_account_id account_id ,
    t501.c703_sales_rep_id sales_rep_id ,
    to_number(t501.c501_ad_id) ad_id,
    to_number(t501.c501_vp_id) vp_id,
    system_dim.system_id system_id,
	system_dim.system_name system_name,
    t502.c205_part_number_id part_id ,
    to_number(t501.c1900_company_id) company_id,
    to_number(v700.divid) market_id,
    to_number(v700.compid) division_id,
    to_number(v700.gp_id) zone_id,
    to_number(v700.region_id) region_id,
    to_number(v700.d_id) distributor_id,
    to_number(v700.ter_id) territory_id,
	to_number(decode(t501.c901_order_type, 2524, NULL, t502.c502_item_qty)) item_qty ,
    t502.c502_item_price item_price,
    t502.c502_corp_item_price corp_item_price,
    t502.c502_item_price * t502.c502_item_qty sales_amount ,
    t502.c502_corp_item_price * t502.c502_item_qty corp_sales_amount 	
  FROM t501_order t501 ,
      t502_item_order t502,
      pbi_mv_system_dim system_dim,
      v700_territory_mapping_detail v700
  WHERE t501.c501_order_date                  >= TRUNC(SYSDATE, 'MONTH')
  AND t502.c501_order_id = t501.c501_order_id
    AND t502.c205_part_number_id = system_dim.Part_Id (+)
    AND v700.ac_id = t501.c704_account_id
	AND NVL (t501.c901_order_type, -9999)     NOT IN ('2533','2518','2519','101260','2520','2534','102364')
	AND t502.c502_void_fl                IS NULL
    AND t501.c501_void_fl                     IS NULL
    AND t501.c501_delete_fl                   IS NULL
  );