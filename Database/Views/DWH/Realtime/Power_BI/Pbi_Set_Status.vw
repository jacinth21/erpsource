CREATE OR REPLACE VIEW Pbi_Set_Status
AS
  (SELECT T504.C207_Set_Id Setid ,
    T207.C207_Set_Nm Set_Name,
    T906.C906_Rule_value Setstatus,
    COUNT(1) SETCOUNT
  FROM T504_Consignment T504,
    T504a_Consignment_Loaner T504a,
    T906_Rules T906,
    t207_set_master t207
  WHERE T504A.C504_CONSIGNMENT_ID = T504.C504_CONSIGNMENT_ID
  AND T504.C504_TYPE              = 4127
  AND T504.C504_VOID_FL          IS NULL
  AND T504A.c504a_status_fl      !=60
  AND T504.C207_Set_Id           IS NOT NULL
  AND T504.C207_Set_Id            =t207.c207_set_id
  AND T504a.C504a_Status_Fl       =T906.C906_Rule_Id
  AND t906.C906_RULE_GRP_ID       ='LOANS'
  AND ( T504.C1900_company_id     = 1000)
  AND T504.C5040_Plant_Id         = T504a.C5040_Plant_Id
  GROUP BY T504.C207_Set_Id,
    t207.C207_Set_Nm,
    T504a.C504a_Status_Fl,
    t906.C906_RULE_value
  UNION ALL
  SELECT T504.C207_Set_Id Setid ,
    t207.c207_set_nm set_name,
    'Opn Req' SETSTATUS,
    NVL(open_count.open_sum,0) SETCOUNT
  FROM T504_CONSIGNMENT T504,
    T504a_Consignment_Loaner T504a ,
    t207_set_master t207,
    (SELECT C207_Set_Id ,
      SUM (C526_Qty_Requested) Open_Sum
    FROM T526_Product_Request_Detail T526
    WHERE c901_request_type     = 4127
    AND c526_status_fl          = 10
    AND C526_Void_Fl           IS NULL
    AND ( t526.C1900_company_id = 1000)
    GROUP BY c207_set_id
    ) open_count
  WHERE T504A.C504_CONSIGNMENT_ID = T504.C504_CONSIGNMENT_ID
  AND T504.C504_TYPE              = 4127
  AND T504.C504_VOID_FL          IS NULL
  AND T504.C207_Set_Id            = Open_Count.C207_Set_Id(+)
  AND t207.C207_Set_id            =t504.c207_set_id
  AND T504A.c504a_status_fl      !=60
  AND T504.C207_SET_ID           IS NOT NULL
  AND ( T504.C1900_company_id     = 1000)
  AND T504.C5040_Plant_Id         = T504a.C5040_Plant_Id
  GROUP BY T504.C207_SET_ID ,
    t207.C207_Set_nm,
    Open_Count.Open_Sum
  HAVING open_count.open_sum >0
  UNION ALL
  SELECT T504.C207_Set_Id Setid ,
    t207.c207_set_nm set_name,
    'Pnd Apr Req' SETSTATUS,
    NVL(pnd_count.pnd_sum,0) SETCOUNT
  FROM T504_CONSIGNMENT T504,
    T504a_Consignment_Loaner T504a ,
    t207_set_master t207,
    (SELECT C207_Set_Id,
      SUM (C526_Qty_Requested) Pnd_Sum
    FROM T526_Product_Request_Detail T526
    WHERE c901_request_type     =4127
    AND c526_status_fl          = 5
    AND C526_Void_Fl           IS NULL
    AND ( t526.C1900_company_id = 1000)
    GROUP BY c207_set_id
    ) pnd_count
  WHERE T504A.C504_CONSIGNMENT_ID = T504.C504_CONSIGNMENT_ID
  AND T504.C504_TYPE              = 4127
  AND T504.C504_VOID_FL          IS NULL
  AND T504.C207_Set_Id            = Pnd_Count.C207_Set_Id(+)
  AND t207.C207_Set_id            =t504.c207_set_id
  AND T504A.c504a_status_fl      !=60
  AND T504.C207_SET_ID           IS NOT NULL
  AND ( T504.C1900_company_id     = 1000)
  AND T504.C5040_Plant_Id         = T504a.C5040_Plant_Id
  GROUP BY T504.C207_Set_Id,
    T207.C207_Set_Nm,
    Pnd_Count.Pnd_Sum
  HAVING  pnd_count.pnd_sum >0
  );