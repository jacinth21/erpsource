/*
File name: pbi_realtime_date_dim.vw
Created By: YKannan
Description: Fetch current month total business days and sales  business days as of current date
SVN Location: @"C:\Projects\Branches\PMT\Datawarehouse\DW_BI\Power_BI_Dashboard\Real_time\pbi_realtime_date_dim.vw";
*/

DROP VIEW pbi_realtime_date_dim;
DROP MATERIALIZED VIEW pbi_realtime_date_dim;
CREATE MATERIALIZED VIEW  pbi_realtime_date_dim
NOLOGGING
REFRESH COMPLETE START WITH (SYSDATE) NEXT  (SYSDATE+60/1440) WITH ROWID
AS
  ( SELECT 
	  cal_date, 
      year_month,
      holiday_ind, 
      bus_day_ind,
      day_number,
      decode(cal_date,trunc(SYSDATE), 1,null) today
    FROM date_dim 
	WHERE year_month = to_char(sysdate,'yyyy/mm')
  );
 
CREATE INDEX idx_rt_date_id ON pbi_realtime_date_dim (cal_date);
CREATE INDEX idx_rt_today ON pbi_realtime_date_dim (today);
