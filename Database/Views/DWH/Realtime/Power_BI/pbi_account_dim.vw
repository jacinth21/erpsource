/*
File name: pbi_account_dim.vw
Created By: YKannan
Description: Fetch all non voided account data. (c1900_company_id=1018 OR c901_account_type =70114) -> filters for ICS accounts
	Account Type 70114 - InterCompany Sale(ICS) 
	Company id 1018 - Globus Medical International HQ
SVN Location: @"C:\Projects\Branches\PMT\Datawarehouse\DW_BI\Power_BI_Dashboard\Real_time\pbi_account_dim.vw";
*/


DROP VIEW pbi_account_dim;
DROP MATERIALIZED VIEW pbi_account_dim;
CREATE MATERIALIZED VIEW pbi_account_dim 
NOLOGGING
REFRESH COMPLETE START WITH (SYSDATE) NEXT  (SYSDATE+60/1440) WITH ROWID
AS
    (SELECT  t101.c101_party_id Parent_acc_id, 
        nvl(t101.c101_party_nm_en,t101.c101_party_nm) Parent_acc_name,
        dealer.c101_party_id dealer_id, 
        nvl(dealer.c101_party_nm_en,dealer.c101_party_nm) dealer_name,
        to_number(t704.c704_account_id) account_id ,
        nvl(t704.c704_account_nm_en, t704.c704_account_nm) || '' account_name,
        t704.c703_sales_rep_id sales_rep_id ,
        t704.c901_account_type account_type_id ,
        t704.c1900_company_id company_id ,
        t704.c901_company_id division_id ,
        t704.c704_active_fl active_fl, 
        t901_state.c901_code_nm state,
        t901_country.c901_code_nm Country,
        t901_country.c901_code_nm
        ||','
        ||c704_bill_city city,
        c704_bill_latitude lattitude,
        c704_bill_longitude longitude,
        c704_ship_city ship_city,
        t901_ship_state.c901_code_nm ship_state
    FROM t101_party t101,t704_account t704,t901_code_lookup t901_state,t901_code_lookup t901_country,t901_code_lookup t901_ship_state,t101_party dealer
    WHERE t704.c101_party_id=t101.c101_party_id(+)
    AND t101.c901_party_type     (+)=4000877           -- Party Type Account 
    AND dealer.c901_party_type     (+)=26230725        -- Party Type Dealer
    AND t704.c704_void_fl  IS NULL
    AND c704_bill_state=t901_state.c901_code_id(+)
    AND c704_bill_country=t901_country.c901_code_id(+)
    AND c704_ship_state=t901_ship_state.c901_code_id(+)
    AND t704.c101_dealer_id=dealer.c101_party_id(+) 
    );
    
CREATE INDEX idx_account_id ON pbi_account_dim (account_id);
CREATE INDEX idx_account_rep_id ON pbi_account_dim (sales_rep_id);
CREATE INDEX idx_account_division_id ON pbi_account_dim (division_id);
CREATE INDEX idx_account_dealer_id ON pbi_account_dim (dealer_id);
CREATE INDEX idx_account_company_id ON pbi_account_dim (company_id);
CREATE INDEX idx_account_parent_acc_id ON pbi_account_dim (parent_acc_id);