CREATE OR REPLACE VIEW V705_PART_PRICE_IN_ACC_BO
AS
  (SELECT c705_account_pricing_id account_pricing_id,
    c704_account_id account_id,
    c101_party_id account_party_id ,
    price_source price_source_key_id,
    nvl(price_source,c101_party_id) party_key_id,
    c205_part_number_id part_key_id,
    c705_unit_price unit_price,
    DECODE (c101_party_id,price_source,'ACCOUNT','GPB') price_source,
    c705_history_fl history_flag,
    c705_created_date created_date,
    c705_created_by created_by,
    c705_last_updated_date last_updated_date,
    c705_last_updated_by last_updated_by,
    c705_void_fl acc_pri_vf,
    c740_void_fl gpo_vf,
    c1900_company_id company_id
  FROM
    (SELECT c704_account_id,
      c705_account_pricing_id,
      c101_party_id ,
      price_source ,
      c205_part_number_id ,
      c705_unit_price ,
      c705_created_by,
      c705_created_date,
      c705_last_updated_by,
      c705_last_updated_date,
      c705_history_fl,
      c705_void_fl,
      c740_void_fl,
      c1900_company_id,
      /*
      *  following two columns help to decide to keep one of the two rows fetched for the same part
      *  example, part number: 124.454  unit price: $200 ( group price )
      *           part number: 124.454  unit price: $250 ( account price )
      *  we need to keep the 1st row and remove the second row.
      */
      row_number() over ( partition BY c101_party_id, c205_part_number_id order by prc_type) row_num ,
      COUNT(1) over ( partition BY c101_party_id, c205_part_number_id) cnt
    FROM
      (
      -- fetch the account price for the given account(s) and part(s)
      SELECT t704.c704_account_id,
        t705.c705_account_pricing_id,
        t704.c101_party_id ,
        t704.c101_party_id price_source,
        t705.c205_part_number_id ,
        t705.c705_unit_price ,
        t705.c705_created_by,
        t705.c705_created_date,
        t705.c705_last_updated_by,
        t705.c705_last_updated_date,
        c705_history_fl,
        c705_void_fl,
        NULL c740_void_fl,
        t705.c1900_company_id,
        1 prc_type
      FROM t705_account_pricing t705 ,
        t704_account t704,
        t205_part_number t205,
        (SELECT DISTINCT c704_account_id FROM my_temp_acc_list_bo
        ) my_list
      WHERE t704.c101_party_id     = t705.c101_party_id
      AND t704.c704_account_id    IS NOT NULL
      AND t704.c704_account_id     = my_list.c704_account_id
      AND t705.c205_part_number_id = t205.c205_part_number_id
      AND t704.c704_void_fl       IS NULL
      UNION ALL
      -- fetch the group price for the given account(s) and part(s)
      SELECT t704.c704_account_id,
        t705.c705_account_pricing_id,
        t704.c101_party_id ,
        t740.c101_party_id price_source,
        t705.c205_part_number_id ,
        t705.c705_unit_price ,
        t705.c705_created_by,
        t705.c705_created_date,
        t705.c705_last_updated_by,
        t705.c705_last_updated_date,
        c705_history_fl,
        c705_void_fl,
        c740_void_fl,
        t705.c1900_company_id,
        2 prc_type
      FROM t705_account_pricing t705 ,
        t740_gpo_account_mapping t740,
        t704_account t704,
        t205_part_number t205,
        (SELECT DISTINCT c704_account_id FROM my_temp_acc_list_bo
        ) my_list
      WHERE t705.c101_party_id    IS NOT NULL
      AND t705.c101_party_id       = t740.c101_party_id
      AND t740.c704_account_id     = t704.c704_account_id
      AND t704.c704_account_id     = my_list.c704_account_id
      AND t705.c205_part_number_id = t205.c205_part_number_id
      AND t704.c704_void_fl       IS NULL
      )
    )
  WHERE row_num = cnt
  );
