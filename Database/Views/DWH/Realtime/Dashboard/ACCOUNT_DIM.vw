CREATE OR REPLACE VIEW globus_bi_realtime.account_dim
AS
  (SELECT TO_NUMBER(t704.c704_account_id) account_key_id ,
    t704.c704_account_id account_id ,
    nvl(c704_account_nm_en,c704_account_nm) account_nm ,
    c703_sales_rep_id sales_rep_id ,
    c703_sales_rep_id sales_rep_key_id ,
    c704_account_sh_name account_sh_name ,
    c704_contact_person contact_person ,
    c704_phone phone ,
    c704_fax fax ,
    c704_bill_add1 add1 ,
    t901_pricing_category.c901_code_nm pricing_category ,
    c704_bill_name bill_name ,
    c704_bill_add2 add2 ,
    c704_bill_city city ,
    t901_bill_state.c901_code_nm bill_state ,
    t901_bill_country.c901_code_nm bill_country ,
    c704_bill_zip_code zip_code ,
    t901_payment_terms.c901_code_nm payment_terms ,
    c704_inception_date inception_date ,
    c704_ship_name ship_name ,
    c704_active_fl active_fl ,
    c704_ship_add1 ship_add1 ,
    c704_ship_add2 ship_add2 ,
    c704_ship_city ship_city ,
    t901_ship_state.c901_code_nm ship_state ,
    t901_ship_country.c901_code_nm ship_country ,
    c704_ship_zip_code ship_zip_code ,
    c704_comments comments ,
    c704_email_id email_id ,
	t704a.c704a_attribute_value gsttin_no ,
    c704_price_cat_fl price_cat_fl ,
    c704_void_fl void_fl ,
    t70_gpo.party_nm gpo_name ,
    c901_company_id company_id ,
    t901_company_name.c901_code_nm company_name ,
    c704_ext_ref_id ext_ref_id ,
    c901_ext_country_id ext_country_id ,
    c901_account_type account_type_id ,
    NVL(ics_acc_dim.account_type_id_fdsr,t704.c901_account_type) account_type_id_fdsr ,
    t901_account_type.c901_code_nm account_type ,
    t704.c1900_company_id company_key_id
  FROM t704_account t704,
    ics_account_dim ics_acc_dim,
    t901_code_lookup t901_pricing_category,
    t901_code_lookup t901_bill_state,
    t901_code_lookup t901_bill_country,
    t901_code_lookup t901_payment_terms,
    t901_code_lookup t901_ship_state,
    t901_code_lookup t901_ship_country,
    t901_code_lookup t901_company_name,
    t901_code_lookup t901_account_type,
    (SELECT c704_account_id ,
      DECODE (c1900_company_id,'1026',NVL(c101_party_nm_en,DECODE(c101_party_nm , NULL, c101_first_nm
      || ' '
      || c101_last_nm
      || ' '
      || c101_middle_initial , c101_party_nm) ),DECODE(c101_party_nm , NULL, c101_first_nm
      || ' '
      || c101_last_nm
      || ' '
      || c101_middle_initial , c101_party_nm)) party_nm
    FROM t101_party t101,
      t740_gpo_account_mapping t740
    WHERE t101.c101_party_id(+) = t740.c101_party_id
    )t70_gpo,
	(SELECT c704_account_id,c704a_attribute_value
	 FROM t704a_account_attribute
	 WHERE c704a_void_fl IS NULL
	 AND c901_attribute_type=26240114)t704a
  WHERE c704_void_fl      IS NULL
  AND t704.c704_account_id =ics_acc_dim.account_key_id(+)
  AND c704_pricing_category=t901_pricing_category.c901_code_id(+)
  AND c704_bill_state      =t901_bill_state.c901_code_id(+)
  AND c704_bill_country    =t901_bill_country.c901_code_id(+)
  AND c704_payment_terms   =t901_payment_terms.c901_code_id(+)
  AND c704_ship_state      =t901_ship_state.c901_code_id(+)
  AND c704_ship_country    =t901_ship_country.c901_code_id(+)
  AND c901_company_id      =t901_company_name.c901_code_id(+)
  AND c901_account_type    =t901_account_type.c901_code_id(+)
  AND t704.c704_account_id =t70_gpo.c704_account_id(+)
  AND t704.c704_account_id =t704a.c704_account_id(+)
  );