CREATE OR REPLACE VIEW globus_bi_realtime.item_order_dim
AS
  (SELECT TO_NUMBER(t502.c502_item_order_id) Item_Order_Key_Id
  , TO_CHAR(t502.c502_item_order_id) item_order_id
  , t502.c502_control_number control_num
  , get_code_name(t502.c901_type) order_part_type
  , t502.C502_CONSTRUCT_FL CONSTRUCT_FL
  , t502.C501_ORDER_ID order_id
  , t502.C205_PART_NUMBER_ID part_id
  , t502.C502_ITEM_QTY ITEM_QTY
  , t502.C502_CORP_ITEM_PRICE item_price
  , t502.C502_Item_Price Ext_Item_Price
  , t502.c502_unit_price unit_price
  , t502.C502_VAT_RATE EXT_VAT_RATE
  , t502.C502_CGST_RATE EXT_CGST_RATE
  , t502.C502_IGST_RATE EXT_IGST_RATE
  , t502.C502_SGST_RATE EXT_SGST_RATE
  , NVL(t502.C502_CGST_RATE,0)+NVL(t502.C502_IGST_RATE,0)+NVL(t502.C502_SGST_RATE,0) EXT_GST_RATE
  FROM t502_item_order t502
  WHERE t502.c502_void_fl IS NULL
  AND t502.c502_delete_fl IS NULL
  );
  
  

