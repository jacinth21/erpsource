DROP TABLE globus_bi_realtime.SYSTEM_SET_HIERARCHY_DIM;

CREATE TABLE globus_bi_realtime.SYSTEM_SET_HIERARCHY_DIM
(
SYSTEM_ID VARCHAR2(20)
, LEVEL1_TYPE  VARCHAR2(20)
, LEVEL_1_SETID  VARCHAR2(20)
, LEVEL2_TYPE  VARCHAR2(20)
, LEVEL_2_SETID  VARCHAR2(20)
, LEVEL_3_TYPE	VARCHAR2(20)
, LEVEL_3_SETID  VARCHAR2(20)
, ACTUAL_SET_TYPE	VARCHAR2(20)
, ACTUAL_SET_ID  VARCHAR2(20)
);