CREATE OR REPLACE VIEW globus_bi_realtime.system_dim 
AS
(
SELECT  
  t207.c207_set_id system_key_id
, t207.c207_set_id system_id
, regexp_replace (t207.c207_set_nm, '.?trade;|.?reg;|.?\(.*\)') system_nm
, t207.c207_set_desc set_desc
, t207.c207_seq_no seq_no
, MAX(DECODE(t240.c901_type , 20175,t240.c240_value,0 )) baseline_case
, MAX(DECODE(t240.c901_type, 20176,t240.c240_value,0 )) organization_list_turns
, MAX(DECODE(t240.c901_type, 20177, t240.c240_value,0 )) organization_baseline_case
, MAX(DECODE(t240.c901_type, 20178, t240.c240_value,0 )) baseline_cogs_setup
FROM t207_set_master t207, t240_baseline_value t240
WHERE t207.c901_set_grp_type = 1600 AND t207.c207_void_fl IS NULL 
      AND t207.c207_set_id = t240.c207_set_id (+)
GROUP BY t207.c207_set_id, c207_set_nm, c207_set_desc, c207_seq_no
UNION
SELECT  
  '-1' system_key_id
, '-1' system_id
, 'No System' system_nm
, 'N/A' set_desc
, -1 seq_no
, Null baseline_case
, Null organization_list_turns
, Null organization_baseline_case
, Null baseline_cogs_setup
FROM Dual
);
