DROP TABLE globus_bi_realtime.QUARTER_DIM;

CREATE TABLE globus_bi_realtime.QUARTER_DIM as select  distinct YEAR_QUARTER, QUARTER_NUMBER, YEAR_NUMBER, 0 BUS_DAYS_FOR_QUARTER from globus_bi_realtime.date_dim;
