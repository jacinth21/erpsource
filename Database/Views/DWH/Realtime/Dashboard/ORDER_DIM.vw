CREATE OR REPLACE VIEW globus_bi_realtime.order_dim
AS
  (SELECT t501.c501_order_id order_key_id
  , t501.c501_order_id order_id
  , t501.c501_parent_order_id parent_order_id
  , t501.c501_order_date order_dt
  , t101_created_by.c101_user_f_name||' '|| t101_created_by.c101_user_l_name created_by
  , c501_created_date created_date
  , t501.c501_customer_po customer_po
  , c501_order_date_time order_date_time
  , c501_surgery_date surgery_date
  , t503.c503_invoice_date invoice_date
  , T901_REC_MODE.C901_CODE_NM receive_mode
  , DECODE(t501.c501_status_fl,0,'Back Order',1,'Pending Control Number',2,'Pending Shipment',3,'Shipped',4,'Shipped',5,'Shipped') status
  , T901_ORDER_TYPE.C901_CODE_NM order_type
  , C901_ORDER_TYPE Order_Type_Id
  , gm_dw_pkg_sm_salesfact.get_do_id(t501.c501_order_id) do_id
  , t501.c503_invoice_id invoice_number
  , t501.C501_SHIPPING_DATE ship_date
  , t501.C501_TRACKING_NUMBER tracking_number
  , t501.C901_REASON_TYPE reason_type
  , C501_HOLD_FL Hold_Flag
  , C501_EXT_REF_ID OUS_ORDER_ID
  , t501.C901_EXT_COUNTRY_ID OUS_COUNTRY_ID
  , T901_OUS_COUNTRY_NAME.C901_CODE_NM OUS_Country_Name
  , TO_NUMBER(t501.c704_account_id) account_key_id
  , t501.c704_account_id account_id
  , t501.c101_dealer_id dealer_id
  , t501.c703_sales_rep_id sales_rep_key_id
  , t501.c703_sales_rep_id sales_rep_id
  , t501.c501_ship_cost ship_cost
  , nvl(c501_ext_ship_cost,t501.c501_ship_cost) EXT_Ship_cost
  , t501.C1900_COMPANY_ID Company_Key_Id
  , t501.C5040_PLANT_ID Plant_Key_Id
  FROM t501_order t501, t901_code_lookup t901_rec_mode, t901_code_lookup t901_order_type, t901_code_lookup t901_OUS_Country_Name, t503_invoice t503,t101_user t101_created_by
  WHERE t501.c501_receive_mode=t901_rec_mode.c901_code_id(+)
  AND t501.C901_ORDER_TYPE=t901_order_type.c901_code_id(+)
  AND t501.C901_EXT_COUNTRY_ID =t901_OUS_Country_Name.c901_code_id(+)
  AND t501.c503_invoice_id=t503.c503_invoice_id(+)
  AND t501.c501_void_fl                   IS NULL
  AND t501.c501_delete_fl                   IS NULL
  AND NVL (t501.c901_order_type, -9999) NOT IN ('2533','2518','2519','101260','2520','2534','102364')
   -- 2519:Pending CS Confirmation, 2518:Draft, 2533:ICS, 101260:Acknowledgement Order,2520:Quote,2534:ICT returns Order,102364:Stock Transfer
  AND t501.c501_order_date >= ADD_MONTHS (TRUNC (SYSDATE,'YEAR'), -12) --Get data from prior year start
  AND t501.c501_created_by=t101_created_by.c101_user_id (+)
  );