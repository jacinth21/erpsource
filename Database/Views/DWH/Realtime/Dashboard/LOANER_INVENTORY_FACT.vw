CREATE OR REPLACE VIEW globus_bi_realtime.LOANER_INVENTORY_FACT 
AS 
(
SELECT
  TO_NUMBER (c504a_loaner_transaction_id) loaner_key_id
, TO_NUMBER (c504a_loaner_transaction_id) loaner_id
, t504a.c504a_consigned_to_id DISTRIBUTOR_KEY_ID
, t504a.c504a_consigned_to_id DISTRIBUTOR_ID
, t504a.c703_sales_rep_id SALES_REP_KEY_ID
, t504a.c703_sales_rep_id SALES_REP_ID
, t504a.c704_account_id ACCOUNT_KEY_ID
, t504a.c704_account_id ACCOUNT_ID
, t504.c207_set_id set_key_id
, t504.c207_set_id set_id
, loan_date_dim.date_key_id loan_date_key_id
, SUM(t505.C505_ITEM_PRICE * T505.C505_ITEM_QTY) Total_Loaner_Value
FROM t504a_loaner_transaction t504a
       , t504_consignment t504
       , t504a_consignment_loaner t504b
       , t505_item_consignment t505
       , date_dim loan_date_dim
WHERE
	t504a.c504_consignment_id = t504.c504_consignment_id
     AND t504a.c504_consignment_id = t504b.c504_consignment_id
     AND t504a.c504_consignment_id = t505.C504_CONSIGNMENT_ID
     AND loan_date_dim.cal_date = t504a.c504a_loaner_dt
     AND t504.C504_TYPE = '4127'
     AND t504a.C901_CONSIGNED_TO = 50170
     AND t504a.c504a_void_fl IS NULL
     AND t504.C504_VOID_FL IS NULL
     AND T505.C505_VOID_FL IS NULL
GROUP by c504a_loaner_transaction_id, c504a_consigned_to_id, loan_date_dim.date_key_id, c207_set_id, 
c504a_return_dt, t504a.c504a_loaner_dt,t504a.c703_sales_rep_id, t504a.c704_account_id
);