CREATE OR REPLACE VIEW globus_bi_realtime.loaner_dim
AS
  (SELECT TO_NUMBER (c504a_loaner_transaction_id) loaner_key_id
  , TO_NUMBER (c504a_loaner_transaction_id) loaner_id
  , t504a.c504a_loaner_dt loaner_date
  , t504a.c504a_expected_return_dt EXPECTED_RETURN_DATE
  , c504a_return_dt return_date
  , get_code_name (c901_consigned_to) consign_to_party_type
  , c504a_is_loaner_extended is_loaner_extended
  , c504a_is_replenished is_replenished
  , t504b.c504a_etch_id etch_id
  , REPLACE(get_cs_rep_loaner_bill_nm_add(t504a.c504_consignment_id),'<BR>','; ') consign_to_party
  , t504a.c504_consignment_id consignment_id
  , (NVL(c504a_return_dt, TRUNC(sysdate))      - t504a.c504a_loaner_dt) usage_days
  , DECODE(c504a_return_dt, NULL,TRUNC(SYSDATE)- t504a.c504a_expected_return_dt,0) days_overdue
  , CASE
      WHEN TO_CHAR(t504a.c504a_loaner_dt,'YYYYMM') = TO_CHAR(t504a.c504a_loaner_dt,'YYYY')
        ||'01'
      AND TO_CHAR(c504a_return_dt,'YYYYMM') = TO_CHAR(c504a_return_dt,'YYYY')
        ||'01'
      THEN c504a_return_dt - t504a.c504a_loaner_dt
      WHEN TO_CHAR(t504a.c504a_loaner_dt,'YYYYMM') = TO_CHAR(t504a.c504a_loaner_dt,'YYYY')
        ||'01'
      AND TO_CHAR(c504a_return_dt,'YYYYMM') > TO_CHAR(c504a_return_dt,'YYYY')
        ||'01'
      THEN LAST_DAY(t504a.c504a_loaner_dt) - t504a.c504a_loaner_dt
      WHEN TO_CHAR(t504a.c504a_loaner_dt,'YYYYMM') < TO_CHAR(t504a.c504a_return_dt,'YYYY')
        ||'01'
      AND TO_CHAR(c504a_return_dt,'YYYYMM') = TO_CHAR(c504a_return_dt,'YYYY')
        ||'01'
      THEN c504a_return_dt - to_date('01-'
        ||TO_CHAR(c504a_return_dt,'MON-YYYY'))
      WHEN TO_CHAR(t504a.c504a_loaner_dt,'YYYYMM') < TO_CHAR(t504a.c504a_return_dt,'YYYY')
        ||'01'
      AND TO_CHAR(c504a_return_dt,'YYYYMM') > TO_CHAR(c504a_return_dt,'YYYY')
        ||'01'
      THEN 31
    END Jan_Usage_Days
  , CASE
      WHEN TO_CHAR(t504a.c504a_loaner_dt,'YYYYMM') = TO_CHAR(t504a.c504a_loaner_dt,'YYYY')
        ||'02'
      AND TO_CHAR(c504a_return_dt,'YYYYMM') = TO_CHAR(c504a_return_dt,'YYYY')
        ||'02'
      THEN c504a_return_dt - t504a.c504a_loaner_dt
      WHEN TO_CHAR(t504a.c504a_loaner_dt,'YYYYMM') = TO_CHAR(t504a.c504a_loaner_dt,'YYYY')
        ||'02'
      AND TO_CHAR(c504a_return_dt,'YYYYMM') > TO_CHAR(c504a_return_dt,'YYYY')
        ||'02'
      THEN LAST_DAY(t504a.c504a_loaner_dt) - t504a.c504a_loaner_dt
      WHEN TO_CHAR(t504a.c504a_loaner_dt,'YYYYMM') < TO_CHAR(t504a.c504a_return_dt,'YYYY')
        ||'02'
      AND TO_CHAR(c504a_return_dt,'YYYYMM') = TO_CHAR(c504a_return_dt,'YYYY')
        ||'02'
      THEN c504a_return_dt - to_date('01-'
        ||TO_CHAR(c504a_return_dt,'MON-YYYY'))
      WHEN TO_CHAR(t504a.c504a_loaner_dt,'YYYYMM') < TO_CHAR(t504a.c504a_return_dt,'YYYY')
        ||'02'
      AND TO_CHAR(c504a_return_dt,'YYYYMM') > TO_CHAR(c504a_return_dt,'YYYY')
        ||'02'
      THEN 28
    END Feb_Usage_Days
  , CASE
      WHEN TO_CHAR(t504a.c504a_loaner_dt,'YYYYMM') = TO_CHAR(t504a.c504a_loaner_dt,'YYYY')
        ||'03'
      AND TO_CHAR(c504a_return_dt,'YYYYMM') = TO_CHAR(c504a_return_dt,'YYYY')
        ||'03'
      THEN c504a_return_dt - t504a.c504a_loaner_dt
      WHEN TO_CHAR(t504a.c504a_loaner_dt,'YYYYMM') = TO_CHAR(t504a.c504a_loaner_dt,'YYYY')
        ||'03'
      AND TO_CHAR(c504a_return_dt,'YYYYMM') > TO_CHAR(c504a_return_dt,'YYYY')
        ||'03'
      THEN LAST_DAY(t504a.c504a_loaner_dt) - t504a.c504a_loaner_dt
      WHEN TO_CHAR(t504a.c504a_loaner_dt,'YYYYMM') < TO_CHAR(t504a.c504a_return_dt,'YYYY')
        ||'03'
      AND TO_CHAR(c504a_return_dt,'YYYYMM') = TO_CHAR(c504a_return_dt,'YYYY')
        ||'03'
      THEN c504a_return_dt - to_date('01-'
        ||TO_CHAR(c504a_return_dt,'MON-YYYY'))
      WHEN TO_CHAR(t504a.c504a_loaner_dt,'YYYYMM') < TO_CHAR(t504a.c504a_return_dt,'YYYY')
        ||'03'
      AND TO_CHAR(c504a_return_dt,'YYYYMM') > TO_CHAR(c504a_return_dt,'YYYY')
        ||'03'
      THEN 31
    END Mar_Usage_Days
  , CASE
      WHEN TO_CHAR(t504a.c504a_loaner_dt,'YYYYMM') = TO_CHAR(t504a.c504a_loaner_dt,'YYYY')
        ||'04'
      AND TO_CHAR(c504a_return_dt,'YYYYMM') = TO_CHAR(c504a_return_dt,'YYYY')
        ||'04'
      THEN c504a_return_dt - t504a.c504a_loaner_dt
      WHEN TO_CHAR(t504a.c504a_loaner_dt,'YYYYMM') = TO_CHAR(t504a.c504a_loaner_dt,'YYYY')
        ||'04'
      AND TO_CHAR(c504a_return_dt,'YYYYMM') > TO_CHAR(c504a_return_dt,'YYYY')
        ||'04'
      THEN LAST_DAY(t504a.c504a_loaner_dt) - t504a.c504a_loaner_dt
      WHEN TO_CHAR(t504a.c504a_loaner_dt,'YYYYMM') < TO_CHAR(t504a.c504a_return_dt,'YYYY')
        ||'04'
      AND TO_CHAR(c504a_return_dt,'YYYYMM') = TO_CHAR(c504a_return_dt,'YYYY')
        ||'04'
      THEN c504a_return_dt - to_date('01-'
        ||TO_CHAR(c504a_return_dt,'MON-YYYY'))
      WHEN TO_CHAR(t504a.c504a_loaner_dt,'YYYYMM') < TO_CHAR(t504a.c504a_return_dt,'YYYY')
        ||'04'
      AND TO_CHAR(c504a_return_dt,'YYYYMM') > TO_CHAR(c504a_return_dt,'YYYY')
        ||'04'
      THEN 30
    END Apr_Usage_Days
  , CASE
      WHEN TO_CHAR(t504a.c504a_loaner_dt,'YYYYMM') = TO_CHAR(t504a.c504a_loaner_dt,'YYYY')
        ||'05'
      AND TO_CHAR(c504a_return_dt,'YYYYMM') = TO_CHAR(c504a_return_dt,'YYYY')
        ||'05'
      THEN c504a_return_dt - t504a.c504a_loaner_dt
      WHEN TO_CHAR(t504a.c504a_loaner_dt,'YYYYMM') = TO_CHAR(t504a.c504a_loaner_dt,'YYYY')
        ||'05'
      AND TO_CHAR(c504a_return_dt,'YYYYMM') > TO_CHAR(c504a_return_dt,'YYYY')
        ||'05'
      THEN LAST_DAY(t504a.c504a_loaner_dt) - t504a.c504a_loaner_dt
      WHEN TO_CHAR(t504a.c504a_loaner_dt,'YYYYMM') < TO_CHAR(t504a.c504a_return_dt,'YYYY')
        ||'05'
      AND TO_CHAR(c504a_return_dt,'YYYYMM') = TO_CHAR(c504a_return_dt,'YYYY')
        ||'05'
      THEN c504a_return_dt - to_date('01-'
        ||TO_CHAR(c504a_return_dt,'MON-YYYY'))
      WHEN TO_CHAR(t504a.c504a_loaner_dt,'YYYYMM') < TO_CHAR(t504a.c504a_return_dt,'YYYY')
        ||'05'
      AND TO_CHAR(c504a_return_dt,'YYYYMM') > TO_CHAR(c504a_return_dt,'YYYY')
        ||'05'
      THEN 31
    END May_Usage_Days
  , CASE
      WHEN TO_CHAR(t504a.c504a_loaner_dt,'YYYYMM') = TO_CHAR(t504a.c504a_loaner_dt,'YYYY')
        ||'06'
      AND TO_CHAR(c504a_return_dt,'YYYYMM') = TO_CHAR(c504a_return_dt,'YYYY')
        ||'06'
      THEN c504a_return_dt - t504a.c504a_loaner_dt
      WHEN TO_CHAR(t504a.c504a_loaner_dt,'YYYYMM') = TO_CHAR(t504a.c504a_loaner_dt,'YYYY')
        ||'06'
      AND TO_CHAR(c504a_return_dt,'YYYYMM') > TO_CHAR(c504a_return_dt,'YYYY')
        ||'06'
      THEN LAST_DAY(t504a.c504a_loaner_dt) - t504a.c504a_loaner_dt
      WHEN TO_CHAR(t504a.c504a_loaner_dt,'YYYYMM') < TO_CHAR(t504a.c504a_return_dt,'YYYY')
        ||'06'
      AND TO_CHAR(c504a_return_dt,'YYYYMM') = TO_CHAR(c504a_return_dt,'YYYY')
        ||'06'
      THEN c504a_return_dt - to_date('01-'
        ||TO_CHAR(c504a_return_dt,'MON-YYYY'))
      WHEN TO_CHAR(t504a.c504a_loaner_dt,'YYYYMM') < TO_CHAR(t504a.c504a_return_dt,'YYYY')
        ||'06'
      AND TO_CHAR(c504a_return_dt,'YYYYMM') > TO_CHAR(c504a_return_dt,'YYYY')
        ||'06'
      THEN 30
    END Jun_Usage_Days
  , CASE
      WHEN TO_CHAR(t504a.c504a_loaner_dt,'YYYYMM') = TO_CHAR(t504a.c504a_loaner_dt,'YYYY')
        ||'07'
      AND TO_CHAR(c504a_return_dt,'YYYYMM') = TO_CHAR(c504a_return_dt,'YYYY')
        ||'07'
      THEN c504a_return_dt - t504a.c504a_loaner_dt
      WHEN TO_CHAR(t504a.c504a_loaner_dt,'YYYYMM') = TO_CHAR(t504a.c504a_loaner_dt,'YYYY')
        ||'07'
      AND TO_CHAR(c504a_return_dt,'YYYYMM') > TO_CHAR(c504a_return_dt,'YYYY')
        ||'07'
      THEN LAST_DAY(t504a.c504a_loaner_dt) - t504a.c504a_loaner_dt
      WHEN TO_CHAR(t504a.c504a_loaner_dt,'YYYYMM') < TO_CHAR(t504a.c504a_return_dt,'YYYY')
        ||'07'
      AND TO_CHAR(c504a_return_dt,'YYYYMM') = TO_CHAR(c504a_return_dt,'YYYY')
        ||'07'
      THEN c504a_return_dt - to_date('01-'
        ||TO_CHAR(c504a_return_dt,'MON-YYYY'))
      WHEN TO_CHAR(t504a.c504a_loaner_dt,'YYYYMM') < TO_CHAR(t504a.c504a_return_dt,'YYYY')
        ||'07'
      AND TO_CHAR(c504a_return_dt,'YYYYMM') > TO_CHAR(c504a_return_dt,'YYYY')
        ||'07'
      THEN 31
    END Jul_Usage_Days
  , CASE
      WHEN TO_CHAR(t504a.c504a_loaner_dt,'YYYYMM') = TO_CHAR(t504a.c504a_loaner_dt,'YYYY')
        ||'08'
      AND TO_CHAR(c504a_return_dt,'YYYYMM') = TO_CHAR(c504a_return_dt,'YYYY')
        ||'08'
      THEN c504a_return_dt - t504a.c504a_loaner_dt
      WHEN TO_CHAR(t504a.c504a_loaner_dt,'YYYYMM') = TO_CHAR(t504a.c504a_loaner_dt,'YYYY')
        ||'08'
      AND TO_CHAR(c504a_return_dt,'YYYYMM') > TO_CHAR(c504a_return_dt,'YYYY')
        ||'08'
      THEN LAST_DAY(t504a.c504a_loaner_dt) - t504a.c504a_loaner_dt
      WHEN TO_CHAR(t504a.c504a_loaner_dt,'YYYYMM') < TO_CHAR(t504a.c504a_return_dt,'YYYY')
        ||'08'
      AND TO_CHAR(c504a_return_dt,'YYYYMM') = TO_CHAR(c504a_return_dt,'YYYY')
        ||'08'
      THEN c504a_return_dt - to_date('01-'
        ||TO_CHAR(c504a_return_dt,'MON-YYYY'))
      WHEN TO_CHAR(t504a.c504a_loaner_dt,'YYYYMM') < TO_CHAR(t504a.c504a_return_dt,'YYYY')
        ||'08'
      AND TO_CHAR(c504a_return_dt,'YYYYMM') > TO_CHAR(c504a_return_dt,'YYYY')
        ||'08'
      THEN 31
    END Aug_Usage_Days
  , CASE
      WHEN TO_CHAR(t504a.c504a_loaner_dt,'YYYYMM') = TO_CHAR(t504a.c504a_loaner_dt,'YYYY')
        ||'09'
      AND TO_CHAR(c504a_return_dt,'YYYYMM') = TO_CHAR(c504a_return_dt,'YYYY')
        ||'09'
      THEN c504a_return_dt - t504a.c504a_loaner_dt
      WHEN TO_CHAR(t504a.c504a_loaner_dt,'YYYYMM') = TO_CHAR(t504a.c504a_loaner_dt,'YYYY')
        ||'09'
      AND TO_CHAR(c504a_return_dt,'YYYYMM') > TO_CHAR(c504a_return_dt,'YYYY')
        ||'09'
      THEN LAST_DAY(t504a.c504a_loaner_dt) - t504a.c504a_loaner_dt
      WHEN TO_CHAR(t504a.c504a_loaner_dt,'YYYYMM') < TO_CHAR(t504a.c504a_return_dt,'YYYY')
        ||'09'
      AND TO_CHAR(c504a_return_dt,'YYYYMM') = TO_CHAR(c504a_return_dt,'YYYY')
        ||'09'
      THEN c504a_return_dt - to_date('01-'
        ||TO_CHAR(c504a_return_dt,'MON-YYYY'))
      WHEN TO_CHAR(t504a.c504a_loaner_dt,'YYYYMM') < TO_CHAR(t504a.c504a_return_dt,'YYYY')
        ||'09'
      AND TO_CHAR(c504a_return_dt,'YYYYMM') > TO_CHAR(c504a_return_dt,'YYYY')
        ||'09'
      THEN 30
    END Sep_Usage_Days
  , CASE
      WHEN TO_CHAR(t504a.c504a_loaner_dt,'YYYYMM') = TO_CHAR(t504a.c504a_loaner_dt,'YYYY')
        ||'10'
      AND TO_CHAR(c504a_return_dt,'YYYYMM') = TO_CHAR(c504a_return_dt,'YYYY')
        ||'10'
      THEN c504a_return_dt - t504a.c504a_loaner_dt
      WHEN TO_CHAR(t504a.c504a_loaner_dt,'YYYYMM') = TO_CHAR(t504a.c504a_loaner_dt,'YYYY')
        ||'10'
      AND TO_CHAR(c504a_return_dt,'YYYYMM') > TO_CHAR(c504a_return_dt,'YYYY')
        ||'10'
      THEN LAST_DAY(t504a.c504a_loaner_dt) - t504a.c504a_loaner_dt
      WHEN TO_CHAR(t504a.c504a_loaner_dt,'YYYYMM') < TO_CHAR(t504a.c504a_return_dt,'YYYY')
        ||'10'
      AND TO_CHAR(c504a_return_dt,'YYYYMM') = TO_CHAR(c504a_return_dt,'YYYY')
        ||'10'
      THEN c504a_return_dt - to_date('01-'
        ||TO_CHAR(c504a_return_dt,'MON-YYYY'))
      WHEN TO_CHAR(t504a.c504a_loaner_dt,'YYYYMM') < TO_CHAR(t504a.c504a_return_dt,'YYYY')
        ||'10'
      AND TO_CHAR(c504a_return_dt,'YYYYMM') > TO_CHAR(c504a_return_dt,'YYYY')
        ||'10'
      THEN 31
    END Oct_Usage_Days
  , CASE
      WHEN TO_CHAR(t504a.c504a_loaner_dt,'YYYYMM') = TO_CHAR(t504a.c504a_loaner_dt,'YYYY')
        ||'11'
      AND TO_CHAR(c504a_return_dt,'YYYYMM') = TO_CHAR(c504a_return_dt,'YYYY')
        ||'11'
      THEN c504a_return_dt - t504a.c504a_loaner_dt
      WHEN TO_CHAR(t504a.c504a_loaner_dt,'YYYYMM') = TO_CHAR(t504a.c504a_loaner_dt,'YYYY')
        ||'11'
      AND TO_CHAR(c504a_return_dt,'YYYYMM') > TO_CHAR(c504a_return_dt,'YYYY')
        ||'11'
      THEN LAST_DAY(t504a.c504a_loaner_dt) - t504a.c504a_loaner_dt
      WHEN TO_CHAR(t504a.c504a_loaner_dt,'YYYYMM') < TO_CHAR(t504a.c504a_return_dt,'YYYY')
        ||'11'
      AND TO_CHAR(c504a_return_dt,'YYYYMM') = TO_CHAR(c504a_return_dt,'YYYY')
        ||'11'
      THEN c504a_return_dt - to_date('01-'
        ||TO_CHAR(c504a_return_dt,'MON-YYYY'))
      WHEN TO_CHAR(t504a.c504a_loaner_dt,'YYYYMM') < TO_CHAR(t504a.c504a_return_dt,'YYYY')
        ||'11'
      AND TO_CHAR(c504a_return_dt,'YYYYMM') > TO_CHAR(c504a_return_dt,'YYYY')
        ||'11'
      THEN 30
    END Nov_Usage_Days
  , CASE
      WHEN TO_CHAR(t504a.c504a_loaner_dt,'YYYYMM') = TO_CHAR(t504a.c504a_loaner_dt,'YYYY')
        ||'12'
      AND TO_CHAR(c504a_return_dt,'YYYYMM') = TO_CHAR(c504a_return_dt,'YYYY')
        ||'12'
      THEN c504a_return_dt - t504a.c504a_loaner_dt
      WHEN TO_CHAR(t504a.c504a_loaner_dt,'YYYYMM') = TO_CHAR(t504a.c504a_loaner_dt,'YYYY')
        ||'12'
      AND TO_CHAR(c504a_return_dt,'YYYYMM') > TO_CHAR(c504a_return_dt,'YYYY')
        ||'12'
      THEN LAST_DAY(t504a.c504a_loaner_dt) - t504a.c504a_loaner_dt
      WHEN TO_CHAR(t504a.c504a_loaner_dt,'YYYYMM') < TO_CHAR(t504a.c504a_return_dt,'YYYY')
        ||'12'
      AND TO_CHAR(c504a_return_dt,'YYYYMM') = TO_CHAR(c504a_return_dt,'YYYY')
        ||'12'
      THEN c504a_return_dt - to_date('01-'
        ||TO_CHAR(c504a_return_dt,'MON-YYYY'))
      WHEN TO_CHAR(t504a.c504a_loaner_dt,'YYYYMM') < TO_CHAR(t504a.c504a_return_dt,'YYYY')
        ||'12'
      AND TO_CHAR(c504a_return_dt,'YYYYMM') > TO_CHAR(c504a_return_dt,'YYYY')
        ||'12'
      THEN 31
    END Dec_Usage_Days
  , t504a.C1900_COMPANY_ID Company_Key_Id
  , t504a.C5040_PLANT_ID Plant_Key_Id
  FROM t504a_loaner_transaction t504a
  , t504a_consignment_loaner t504b
  WHERE t504a.c504a_void_fl    IS NULL
  AND t504a.c504_consignment_id = t504b.c504_consignment_id
  AND t504a.C1900_COMPANY_ID = 1000 -- 1000:Globus North America
  );
