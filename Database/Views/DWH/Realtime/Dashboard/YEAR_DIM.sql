DROP TABLE globus_bi_realtime.YEAR_DIM;

CREATE TABLE globus_bi_realtime.YEAR_DIM as select  distinct YEAR_NUMBER,0 BUS_DAYS_FOR_YEAR, 'N' CURR_YEAR_IND from globus_bi_realtime.date_dim;

COMMIT;