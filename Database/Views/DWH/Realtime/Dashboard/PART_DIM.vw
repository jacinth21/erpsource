CREATE OR REPLACE VIEW globus_bi_realtime.PART_DIM 
AS 
(
 SELECT 
   t205.c205_part_number_id part_key_id
 , t205.c205_part_number_id part_id
 , t205.c205_part_num_desc part_num_desc
 , GET_CODE_NAME(t205.c205_product_family) product_family
 , GET_CODE_NAME(t205.c205_product_class) product_class
 , t205.C202_PROJECT_ID project_id
 , get_project_name(t205.C202_PROJECT_ID) project_name
 , GET_code_name(C205_Product_material) Product_material
 , C205_CORE_PART_FL CORE_PART_FLAG
 , get_ac_cogs_value(t205.c205_part_number_id,4900) COST
 , C205_CROSSOVER_FL CROSSOVER_FLAG
 , C205_FILLER_DETAILS FILLER_DETAILS
 , C205_HARD_TEST_FL HARD_TEST_FLAG
 , C205_INSERT_ID INSERT_ID
 , C205_INSPECTION_CRITERIA INSPECTION_CRITERIA
 , C205_INSPECTION_FILE_ID INSPECTION_FILE_ID
 , C205_INSPECTION_REV INSPECTION_REV
 , C205_MATERIAL_CERT_FL MATERIAL_CERT_FLAG
 , get_code_name(C205_MATERIAL_SPEC) MATERIAL_SPEC
 , get_code_name(C205_MEASURING_DEV) MEASURING_DEV
 , C205_MULTIPLE_PROJ_FL MULTIPLE_PROJ_FLAG
 , C205_PART_DRAWING PART_DRAWING
 , C205_RELEASE_FOR_SALE RELEASE_FOR_SALE
 , C205_REV_NUM REV_NUM
 , C205_SUB_ASMBLY_FL SUB_ASMBLY_FL
 , C205_SUB_COMPONENT_FL SUB_COMPONENT_FL
 , C205_SUPPLY_FL SUPPLY_FL
 , C205_TOLERANCE_LVL TOLERANCE_LVL
 , C205_TRACKING_IMPLANT_FL TRACKING_IMPLANT_FL
 , get_code_name(C901_TYPE) TYPE
 , 0 qty_in_stock
 , c205_loaner_price loaner_price -- used while loaning set
 , c205_list_price list_price -- used while placing order
 , c205_consignment_price consignment_price -- used while sending out consignment
 , C205_CREATED_DATE created_date
 , NVL(TRIM(gm_dw_pkg_pd_partdata.get_part_group_name(t205.c205_part_number_id)),'NOT GROUPED') op_group_name
 , NVL(TRIM(gm_dw_pkg_pd_partdata.get_part_group_type(t205.c205_part_number_id)),'UNKNOWN') op_group_type
 , NVL(TRIM(gm_dw_pkg_pd_partdata.get_product_part_group(t205.c205_part_number_id)),'UNKNOWN') product_group_type
 , NVL(TRIM(gm_dw_pkg_pd_partdata.get_product_part_segment(t205.c205_part_number_id)),'UNKNOWN') product_segment_type
 , get_code_name(C901_STATUS_ID) part_status
 --, c205_qty_in_quarantine qty_in_quarantine
 --, c205_qty_in_returns_hold qty_in_returns_hold
  FROM t205_part_number t205
   );