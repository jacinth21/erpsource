CREATE OR REPLACE VIEW globus_bi_realtime.MONTH_DIM
AS
(SELECT DISTINCT month_year,
        year_month,
        year_quarter,
        month_number,
        year_number,
        month_name,
        month_short_name ,
        month_nm_year,
        bus_days_for_month,
        'N' curr_month_ind,
        bus_days_for_month sales_business_days ,
        0 remain_bus_days_for_month
    FROM globus_bi_realtime.date_dim
    WHERE year_month < TO_CHAR (
        (SELECT TRUNC (SYSDATE) FROM DUAL
        ), 'YYYY/MM')
    UNION ALL
    SELECT curr_month.month_year,
        curr_month.year_month,
        curr_month.year_quarter,
        curr_month.month_number ,
        curr_month.year_number,
        curr_month.month_name,
        curr_month.month_short_name,
        curr_month.month_nm_year ,
        biz_days_month.bus_days_for_month,
        'Y' curr_month_ind ,
        sale_bus_days.sales_business_days sales_business_days ,
        biz_days_month.bus_days_for_month - sale_bus_days.sales_business_days remain_bus_days_for_month
    FROM
        (SELECT DISTINCT month_year,
            year_month,
            year_quarter,
            month_number,
            year_number,
            month_name ,
            month_short_name,
            month_nm_year,
            bus_days_for_month,
            'Y' curr_month_ind ,
            0 sales_business_days,
            0 remain_bus_days_for_month
        FROM globus_bi_realtime.date_dim
        WHERE year_month = TO_CHAR (
            (SELECT TRUNC (SYSDATE) FROM DUAL
            ), 'YYYY/MM')
        ) curr_month ,
        (SELECT year_month,
            COUNT (globus_bi_realtime.date_dim.day_number) sales_business_days
        FROM globus_bi_realtime.date_dim
        WHERE cal_date <=
            (SELECT TRUNC (SYSDATE) FROM DUAL
            )
        AND bus_day_ind = 'Y'
        AND year_month  = TO_CHAR (
            (SELECT TRUNC (SYSDATE) FROM DUAL
            ), 'YYYY/MM')
        GROUP BY year_month
        ) sale_bus_days ,
        (SELECT year_month,
            COUNT (globus_bi_realtime.date_dim.day_number) bus_days_for_month
        FROM globus_bi_realtime.date_dim
        WHERE bus_day_ind = 'Y'
        AND year_month    = TO_CHAR (SYSDATE, 'YYYY/MM')
        GROUP BY year_month
        ) biz_days_month
    WHERE curr_month.year_month = sale_bus_days.year_month
    AND curr_month.year_month   = biz_days_month.year_month
    UNION ALL
    SELECT DISTINCT month_year,
        year_month,
        year_quarter,
        month_number,
        year_number,
        month_name,
        month_short_name ,
        month_nm_year,
        bus_days_for_month,
        'N' curr_month_ind,
        0 sales_business_days ,
        bus_days_for_month remain_bus_days_for_month
    FROM globus_bi_realtime.date_dim
    WHERE year_month > TO_CHAR (
        (SELECT TRUNC (SYSDATE) FROM DUAL
        ), 'YYYY/MM')
);