   
	 
CREATE OR REPLACE  VIEW  V205D_PART_ATTR (TOTAL,PNUM) AS 
  SELECT count(1), t205d.c205_part_number_id
			  FROM   t205d_part_attribute t205d
			 WHERE    t205d.c901_attribute_type = 80180
       AND t205d.c205d_attribute_value IS NOT NULL
       AND t205d.c205d_void_fl IS NULL 
       AND t205d.C205D_ATTRIBUTE_VALUE = NVL(GET_RULE_VALUE_BY_COMPANY('RFS_CNTRY','PART_ATTR'
       													, NVL(sys_context('CLIENTCONTEXT', 'COMPANY_ID') ,get_rule_value('DEFAULT_COMPANY','ONEPORTAL')))
       										, t205d.C205D_ATTRIBUTE_VALUE)
      GROUP BY  t205d.c205_part_number_id;
       
       