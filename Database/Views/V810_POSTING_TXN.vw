CREATE OR REPLACE FORCE VIEW V810_POSTING_TXN
(C810_ACCOUNT_TXN_ID, C901_ACCOUNT_TXN_TYPE, C810_TXN_DATE, C810_ACCT_DATE, C810_DESC, 
 C810_DR_AMT, C810_CR_AMT, C810_QTY, C810_PARTY_ID, C810_TXN_ID, 
 C820_COSTING_ID, C803_PERIOD_ID, C205_PART_NUMBER_ID, C801_ACCOUNT_ELEMENT_ID, C810_DELETE_FL, 
 C810_CREATED_BY, C810_CREATED_DATE)
AS 
SELECT   c810_account_txn_id
  ,c901_account_txn_type
  ,c810_txn_date
  ,c810_acct_date
  ,c810_desc
  ,c810_dr_amt
  ,c810_cr_amt
  ,c810_qty
  ,c810_party_id
  ,c810_txn_id
  ,NULL c820_costing_id
  ,c803_period_id
  ,c205_part_number_id
  ,c801_account_element_id
  ,c810_delete_fl
  ,c810_created_by
  ,c810_created_date
FROM   t810_ap_txn
UNION
SELECT   c810_account_txn_id
  ,c901_account_txn_type
  ,c810_txn_date
  ,c810_acct_date
  ,c810_desc
  ,c810_dr_amt
  ,c810_cr_amt
  ,c810_qty
  ,c810_party_id
  ,c810_txn_id
  ,c820_costing_id
  ,c803_period_id
  ,c205_part_number_id
  ,c801_account_element_id
  ,c810_delete_fl
  ,c810_created_by
  ,c810_created_date
FROM   t810_inventory_txn
UNION
SELECT   c810_account_txn_id
  ,c901_account_txn_type
  ,c810_txn_date
  ,c810_acct_date
  ,c810_desc
  ,c810_dr_amt
  ,c810_cr_amt
  ,c810_qty
  ,c810_party_id
  ,c810_txn_id
  ,c820_costing_id
  ,c803_period_id
  ,c205_part_number_id
  ,c801_account_element_id
  ,c810_delete_fl
  ,c810_created_by
  ,c810_created_date
FROM   t810_consignment_txn
UNION
SELECT   c810_account_txn_id
  ,c901_account_txn_type
  ,c810_txn_date
  ,c810_acct_date
  ,c810_desc
  ,c810_dr_amt
  ,c810_cr_amt
  ,c810_qty
  ,c810_party_id
  ,c810_txn_id
  ,c820_costing_id
  ,c803_period_id
  ,c205_part_number_id
  ,c801_account_element_id
  ,c810_delete_fl
  ,c810_created_by
  ,c810_created_date
FROM   t810_other_txn
ORDER BY c810_acct_date;



