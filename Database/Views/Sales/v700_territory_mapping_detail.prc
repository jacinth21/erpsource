/* Formatted on 2009/07/09 09:45 (Formatter Plus v4.8.0) */
--@"C:\Database\Views\Sales\v700_territory_mapping_detail.prc";
EXEC DBMS_MVIEW.REFRESH ('v700_territory_mapping_detail') ;

DROP MATERIALIZED VIEW	v700_territory_mapping_detail;

CREATE	MATERIALIZED VIEW  v700_territory_mapping_detail
AS
SELECT	 NVL(acct_info.comp_id,dist_reg_info.compid) compid
        ,NVL(TRIM(acct_info.comp_name),dist_reg_info.compname)compname
        ,dist_reg_info.divid
        ,dist_reg_info.divname
        ,dist_reg_info.gp_id
        ,dist_reg_info.gp_name        
        ,dist_reg_info.region_id
		,dist_reg_info.region_name
        ,NVL(vp_info.vp_id, -9999)	vp_id
        ,NVL(vp_info.vp_name , '^ No VP')  vp_name		
        ,NVL(ad_info.ad_id,-9999) ad_id
		,NVL(ad_info.ad_name,'^ No AD') ad_name		
		,dist_reg_info.d_id
		,dist_reg_info.d_name
        ,dist_reg_info.d_name_en
		,dist_reg_info.d_compid
		,dist_reg_info.d_active_fl        
		,ter_rep_info.ter_id
		,ter_rep_info.ter_name
		,ter_rep_info.rep_id
		,NVL(ter_rep_info.rep_name, '^ No Rep') rep_name
        ,NVL(ter_rep_info.rep_name_en, '^ No Rep') rep_name_en
		,ter_rep_info.rep_compid
		,ter_rep_info.rep_partyid
		,acct_info.ac_id
		,acct_info.ac_name
        ,acct_info.ac_name_en
        ,dealer_ifo.dealer_id
        ,dealer_ifo.dealer_nm
    	,dealer_ifo.dealer_nm_en
		,dist_reg_info.disttypeid
		,dist_reg_info.disttypenm
 FROM
(SELECT  t703.c701_distributor_id	d_id
		,NVL(t702.c702_territory_id, -1 * t703.c703_sales_rep_id )	ter_id
		,NVL(t702.c702_territory_name,'^ No Territory') ter_name
		,t703.c703_sales_rep_id 	rep_id
		,t703.c703_sales_rep_name	rep_name
        ,t703.c703_sales_rep_name_en rep_name_en
		,t703.c1900_company_id rep_compid
		,t703.c101_party_id rep_partyid
FROM	 t702_territory t702
		,t703_sales_rep t703
WHERE	t703.c702_territory_id = t702.c702_territory_id  (+)
AND 	NVL(t702.c702_delete_fl,'N') = 'N'
AND 	NVL(t702.c702_active_fl,'Y') = 'Y'
AND 	t703.c703_void_fl IS NULL
) ter_rep_info,
(SELECT  t701.c701_distributor_id			d_id
		,t701.c701_distributor_name 		d_name
        ,t701.c701_distributor_name_en d_name_en
		,t710.c901_area_id				region_id
		,t901.c901_code_nm	            region_name
		,NVL(t701.c701_active_fl,'N')		d_active_fl
        ,t710.c901_zone_id gp_id
		,get_code_name(t710.c901_zone_id) gp_name        
        ,t710.c901_company_id compid
        ,get_code_name(t710.c901_company_id) compname
        ,t710.c901_division_id divid
        ,get_code_name(t710.c901_division_id) divname
        ,t701.c901_distributor_type disttypeid
        ,get_code_name(t701.c901_distributor_type) disttypenm
        ,t701.c1900_company_id d_compid
FROM	 t701_distributor t701
        ,t710_sales_hierarchy t710
		,t901_code_lookup t901
WHERE	 t701.c701_region		= t710.c901_area_id
and     t901.c901_code_id = t710.c901_area_id
and      t710.c710_active_fl = 'Y'
and      t701.c701_void_fl is null
) dist_reg_info ,
(SELECT  c704_account_id		ac_id
		,c704_account_nm		ac_name
        ,c704_account_nm_en ac_name_en
		,c703_sales_rep_id		rep_id
		,c901_company_id	    comp_id
		,get_code_name(c901_company_id) comp_name
		,c101_dealer_id ac_dealer_id
 FROM	 t704_account
) acct_info,
(SELECT c101_party_id dealer_id,
  		c101_party_nm dealer_nm,
  		c101_party_nm_en dealer_nm_en
   FROM t101_party
  WHERE c901_party_type = '26230725'
  	AND c101_void_fl IS NULL
  	AND NVL(c101_active_fl,'Y') = 'Y'
)dealer_ifo,
( SELECT get_user_name(c101_user_id) ad_name, c901_region_id rg_id, c101_user_id ad_id
FROM t708_region_asd_mapping t708
WHERE t708.c901_user_role_type = 8000 AND t708.c708_delete_fl IS NULL) ad_info,
( SELECT get_user_name(c101_user_id) vp_name, c901_region_id rg_id, c101_user_id vp_id
FROM t708_region_asd_mapping t708
WHERE t708.c901_user_role_type = 8001 AND t708.c708_delete_fl IS NULL) vp_info
WHERE	 ter_rep_info.d_id (+)	= dist_reg_info.d_id
AND 	 ter_rep_info.rep_id = acct_info.rep_id (+)
AND 	dist_reg_info.region_id = ad_info.rg_id(+)
AND 	dist_reg_info.region_id = vp_info.rg_id(+)
AND      acct_info.ac_dealer_id = dealer_ifo.dealer_id(+)
/
