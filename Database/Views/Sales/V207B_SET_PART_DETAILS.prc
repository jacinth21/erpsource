DROP MATERIALIZED VIEW v207b_set_part_details;

CREATE MATERIALIZED VIEW v207b_set_part_details
	AS (SELECT
			 t207.c207_set_id c207_set_id,
			 t207.c207_set_nm c207_set_nm,
			 t208.c205_part_number_id c205_part_number_id,
			 t205.c205_tracking_implant_fl c205_tracking_implant_fl,
			 NVL(bvalue.c240_value,0) c204_value,
			 t207.c207_seq_no c207_seq_no,
			 t207.c901_vanguard_type c901_vanguard_type
	  FROM
			t207_set_master t207,
			  t208_set_details t208,
			t205_part_number t205,
			(SELECT
					c207_set_id,
					c240_value
			 FROM
					t240_baseline_value
			 WHERE
					c901_type = 20175
					AND c240_void_fl IS NULL) bvalue
	  WHERE
			t207.c901_set_grp_type = 1600
			AND t207.c207_set_id = t208.c207_set_id
			AND t207.c207_skip_rpt_fl IS NULL
			AND t207.c207_obsolete_fl IS NULL
			AND t207.c207_set_id = bvalue.c207_set_id (+)
			AND t205.c205_part_number_id = t208.c205_part_number_id);
