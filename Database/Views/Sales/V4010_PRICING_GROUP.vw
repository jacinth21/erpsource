/* Formatted on 2010/07/07 10:38 (Formatter Plus v4.8.0) */
CREATE OR REPLACE VIEW v4010_pricing_group (c4010_group_id,
                                            c4010_group_name,
                                            c207_set_id,
                                            c4010_publish_date,
                                            c4010_group_type,
                                            c4010_priced_type
                                           )
AS
   SELECT t4010.c4010_group_id, t4010.c4010_group_nm, t4010.c207_set_id,
          t4010.c4010_group_publish_date, t4010.c901_group_type,
          t4010.c901_priced_type
     FROM t4010_group t4010
    WHERE t4010.c4010_void_fl IS NULL
      AND t4010.c901_type = 40045
      AND t4010.c4010_publish_fl = 'Y'
      AND t4010.c4010_inactive_fl IS NULL
/
