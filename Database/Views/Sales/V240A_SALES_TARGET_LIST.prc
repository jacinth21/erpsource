DROP MATERIALIZED VIEW V240A_SALES_TARGET_LIST;

CREATE MATERIALIZED VIEW V240A_SALES_TARGET_LIST
	AS (SELECT 
				t207.c207_set_id C207_SET_ID ,  		  	
				t207.c207_set_nm C207_SET_NAME,
				targetprocedure.c240_value C240_TARGET_CASES,
				targetlist.c240_value C240_TARGET_LIST,
				targetcogslist.c240_value C240_TARGET_COGS_LIST,
				t207.c207_seq_no C207_SEQ_NO
		FROM
				t207_set_master t207,
				(SELECT 
						c207_set_id,		
						c240_value
				 FROM
				 		t240_baseline_value
				 WHERE
				   		c901_type = 20177
				   		AND c240_void_fl IS NULL) targetprocedure,
				 (SELECT
						c207_set_id,		
						c240_value
				  FROM
				 		t240_baseline_value
				  WHERE
				   		c901_type = 20176
				   		AND c240_void_fl IS NULL) targetlist,
				 (SELECT c207_set_id,		
						 c240_value
				  FROM  t240_baseline_value
				  WHERE	c901_type = 20178 -- for COGS
				   		AND c240_void_fl IS NULL) targetcogslist
			WHERE				
					 t207.c207_set_id = targetprocedure.c207_set_id(+)
					 AND t207.c207_set_id = targetlist.c207_set_id(+)
					 AND t207.c207_set_id = targetcogslist.c207_set_id(+)
					 AND t207.c901_set_grp_type = 1600
					 AND t207.c207_obsolete_fl IS NULL 
					 AND t207.c207_void_fl IS NULL);
