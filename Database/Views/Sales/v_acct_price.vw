CREATE OR REPLACE VIEW v_acct_price
AS
SELECT  t705.c705_account_pricing_id apid         
                , t704.c704_account_id acctid
                , t705.c101_party_id gpoid
                , t705.c205_part_number_id pnum
                , NULL grpid
                , NULL grouptype
                , t705.c705_unit_price price
                , t205.C205_PART_NUM_DESC pdesc                                                   
                , t705.c705_discount_offered disc
                , t705.c205_part_number_id ID
                , t705.c705_created_date cdate
                , t101.c101_user_id cuser        
                , t705.c705_last_updated_date ldate    
                , t705.c705_last_updated_by luser
                , t705.c705_history_fl hisfl
                , 1 type
                ,NVL(t705.c705_void_fl ,0) void_fl
                ,t705.c705_void_fl voidfl
            FROM t705_account_pricing t705, T205_PART_NUMBER t205,
                 t101_user t101, t207_set_master t207 ,
                 t208_set_details t208, v_double_in_list v_doublelist,t704_account t704
            WHERE t705.C205_PART_NUMBER_ID = t205.C205_PART_NUMBER_ID
                AND t705.C705_CREATED_BY = t101.C101_USER_ID
                AND t207.c901_set_grp_type = 1600
                AND T207.C207_SET_ID = T208.C207_SET_ID
                AND T208.C205_PART_NUMBER_ID = T205.C205_PART_NUMBER_ID
                AND v_doublelist.token IS NOT NULL
                AND t705.C101_PARTY_ID = v_doublelist.tokenii
               AND t704.c704_account_id =  v_doublelist.token
                AND t704.C101_PARTY_ID = v_doublelist.tokenii
                ORDER BY type, id;
  /