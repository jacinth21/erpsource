/**********************************************************************
Purpose: Materialized view with refresh email tracking event details
Author: mmuthusamy
Location:@"C:\PMT\bitbucket\erpGlobus\Database\Views\Sales\mv9161_email_tracking_master.vw"
**********************************************************************/
DROP MATERIALIZED VIEW mv9161_email_tracking_master;

CREATE MATERIALIZED VIEW mv9161_email_tracking_master
    REFRESH
        COMPLETE
        START WITH ( sysdate )
        NEXT ( sysdate + 30 / 1440 )
        WITH ROWID
AS
    ( SELECT
        t9161.c9161_email_sent_master_id    email_sent_master_id,
        t9163.c9163_email_sent_dtls_id      email_dtls_id,
        t9161.c703_sales_rep_id             rep_id,
        t9161.c9161_email_sent_by           email_sent_by,
        t9161.c101_ad_id                    ad_id,
        t703.c703_sales_rep_name            rep_name,
        t9161.c701_distributor_id           dist_id,
        t9161.c101_vp_id                    vp_id
            --, v700.AD_NAME, v700.D_NAME
        ,
        t9161.c9161_email_sent_date         email_sent_date,
        t9161.c9161_email_notification_fl   notification_fl,
        t9161.c9161_transaction_id          txn_id,
        t9161.c9161_message_id              msg_id,
        decode(t9163.c101_party_id, NULL, t9163.c9163_email_id, get_party_name(t9163.c101_party_id)
                                                            || '('
                                                            || t9163.c9163_email_id
                                                            || ')') recipient_name,
        t9163.c9163_email_id                email_id,
        t9163.c9163_message_id              unique_msg_id,
        t9163.c101_party_id                 party_id,
        t9163.c9163_clicked_count           clicked_cnt,
        t9163.c9163_open_count              open_cnt,
        t9163.c9163_first_open_date         first_open_date,
        t9163.c9163_first_clicked_date      first_click_date,
        t9164.c901_event_type               event_type,
        t9164.c9164_event_type_name         event_type_name,
        t9164.c9164_last_updated_date       event_date
    FROM
        t9161_email_sent_master     t9161,
        t9163_email_sent_dtls       t9163,
        t9164_email_event_tracker   t9164,
        t1016_share                 t1016,
        t703_sales_rep              t703
    WHERE
        t9161.c9161_email_sent_master_id = t9163.c9161_email_sent_master_id
        AND t9163.c9163_email_sent_dtls_id = t9164.c9163_email_sent_dtls_id 
        AND t1016.c1016_share_id = t9161.c9161_transaction_id
            --AND V700.D_ID = t9161.C701_DISTRIBUTOR_ID (+)
            --AND t9164.C901_EVENT_TYPE IN (110690,110694)
        AND t703.c703_sales_rep_id = t9161.c703_sales_rep_id
        AND t9161.c9161_void_fl IS NULL
        AND t9163.c9163_void_fl IS NULL
        AND t9164.c9164_void_fl IS NULL
    );