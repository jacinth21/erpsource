--@"C:\Database\Views\Sales\v5010a_tag_attribute.vw";
CREATE OR REPLACE VIEW v5010a_tag_attribute 
AS
	SELECT	 t5010a.c5010_tag_id
		   , DECODE (COUNT (DECODE (c901_attribute_type, 11310, t5010a.c5010a_attribute_value, NULL))
				   , 1, 'Y'
				   , 'N'
					) c5010_decommissioned
		   , DECODE (COUNT (DECODE (c901_attribute_type, 11305, t5010a.c5010a_attribute_value, NULL))
				   , 1, 'Y'
				   , 'N'
					) c5010_lockset
		FROM t5010a_tag_attribute t5010a
	   WHERE t5010a.c5010a_void_fl IS NULL
	     AND c5010a_attribute_value = 'Y'
	GROUP BY t5010a.c5010_tag_id;
/