/*
File name: v_segment_system_map.vw
Created By: Gopinathan
Description: Get the Segment and system mapping data in materialized view with fast refresh option to improve performance.
			 For this, need to create log on t207_set_master(log created in pbi_mv_system_dim.vm), t2090_segment and T2091_segment_system_map
			 for specific fields when those data changes materialized view will refresh automatically
			 Also create index on segment_id and system_id to make fast data retrieval
			 Restrictions on Fast Refresh on Materialized Views with Joins Only
			 Rowids of all the tables in the FROM list must appear in the SELECT list of the query.
SVN Location: "/Database/Views/Sales/v_segment_system_map.vw";
*/
DROP VIEW v_segment_system_map;
DROP materialized VIEW log ON t2090_segment;
DROP materialized VIEW log ON T2091_segment_system_map;
DROP materialized VIEW v_segment_system_map;

CREATE MATERIALIZED VIEW LOG ON t2090_segment
WITH rowid,
  sequence (c2090_segment_id,c2090_segment_name,c2090_void_fl)
  including NEW VALUES;
  
CREATE MATERIALIZED VIEW LOG ON T2091_segment_system_map
WITH rowid,
  sequence (c2090_segment_id,c207_set_id,c2091_void_fl) including NEW
  VALUES;
  
CREATE MATERIALIZED VIEW v_segment_system_map NOLOGGING CACHE BUILD
  IMMEDIATE REFRESH FAST ON COMMIT
AS
  (SELECT t2090.c2090_segment_id Segment_id,
    t2090.c2090_segment_name segment_name,
    t207.c207_set_id system_id,
    t207.c207_set_nm system_nm,
    t2090.rowid t2090_rowid,
    t2091.rowid t2091_rowid,
    t207.rowid t207_rowid
  FROM t2090_segment t2090,
    T2091_segment_system_map t2091,
    t207_set_master t207
  WHERE t2090.c2090_segment_id = t2091.c2090_segment_id
  AND t2091.c207_set_id        = t207.c207_set_id
  AND t207.c901_set_grp_type   = 1600
  AND t207.c207_void_fl       IS NULL
  AND t2090.c2090_void_fl     IS NULL
  AND t2091.c2091_void_fl     IS NULL
  );
  
  CREATE INDEX xif1v_segment_system_mapt ON v_segment_system_map
    (
      Segment_id ASC
    );
    
  CREATE INDEX xif2v_segment_system_mapt ON v_segment_system_map
    (
      system_id ASC
    );
/