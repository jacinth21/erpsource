CREATE OR REPLACE FORCE VIEW V705_PART_PRICE_IN_ACC (
    c101_party_id, c205_part_number_id, c705_unit_price, c205_list_price
  , c901_discount_type, c7052_discount_offered, c7052_disc_offered_dollars
  , c7052_disc_list_dollars, c705_calculated_unit_price, c705_calculated_list_price
  , C1706_WASTE, c1706_revision, c1706_waste_price_list, c1706_revision_price_list
  , c705_unit_prc_from, c7052_disc_prc_from, C1706_wast_rev_from
)
as 
select unit_adj.c101_party_id
     , c205_part_number_id
     , ROUND(NVL(c705_unit_price,0),2) c705_unit_price
     , ROUND(NVL(c2052_list_price,0),2) c205_list_price
     , c901_discount_type
     , c7052_discount_offered
	 , c7052_disc_offered_dollars
	 , c7052_disc_list_dollars
	 , c705_calculated_unit_price
	 , c705_calculated_list_price
     , wasted c1706_waste
     , revision c1706_revision
     , wasted_price_type c1706_waste_price_list
     , revision_price_type c1706_revision_price_list
     , unit_prc_from c705_unit_prc_from
     , disc_prc_from  c7052_disc_prc_from
     , wast_rev_adj_from C1706_wast_rev_from
from
(
SELECT
    unit_prc.c101_party_id ,
    unit_prc.c205_part_number_id ,
    unit_prc.c705_unit_price ,
    unit_prc.c2052_list_price , 
    disc_prc.c901_discount_type ,
    disc_prc.c7052_discount_offered ,
	(
	 CASE
            WHEN c901_discount_type = 105283 -- Dollars
            	 THEN ROUND(NVL(c7052_discount_offered,0),2)
            WHEN c901_discount_type = 105284 -- Percentage
           		 THEN ROUND(((( NVL(c705_unit_price,0) * NVL( c7052_discount_offered,0) ) / 100 )),2)
            WHEN c901_discount_type IS NULL 
           		 THEN ROUND(0,2)
          	END
	 ) c7052_disc_offered_dollars ,
	 (
	 CASE
            WHEN c901_discount_type = 105283 -- Dollars
            	 THEN ROUND(NVL(c7052_discount_offered,0),2)
            WHEN c901_discount_type = 105284  -- Percentage
           		 THEN ROUND(((( NVL(c2052_list_price,0) * NVL( c7052_discount_offered,0) ) / 100 )),2)
            WHEN c901_discount_type IS NULL
           		 THEN ROUND(0,2)
          	END
	 ) c7052_disc_list_dollars ,
    decode(unit_prc.row_num,1,'ACCT','GPB') unit_prc_from ,
    DECODE(disc_prc.row_num,1,'ACCT','GPB') disc_prc_from ,
    (
      CASE
        WHEN c901_discount_type = 105283 -- Dollars  
        	 THEN ROUND((NVL(c705_unit_price,0) - NVL(c7052_discount_offered,0)),2)
        WHEN c901_discount_type = 105284 -- Percentage
        	 THEN ROUND((NVL(c705_unit_price,0) - ( ( NVL(c705_unit_price,0) * NVL(c7052_discount_offered,0) ) / 100 )),2)
        WHEN c901_discount_type IS NULL
        	 THEN ROUND(c705_unit_price, 2)
      END 
    ) c705_calculated_unit_price,
     (
      CASE
        WHEN c901_discount_type = 105283 -- Dollars 
        	 THEN ROUND((NVL(c2052_list_price,0) - NVL(c7052_discount_offered,0)),2)
        WHEN c901_discount_type = 105284 -- Percentage
        	 THEN ROUND((NVL(c2052_list_price,0) - ( ( NVL(c2052_list_price,0) * NVL(c7052_discount_offered,0) ) / 100 )),2)
        WHEN c901_discount_type IS NULL
        	 THEN ROUND(c2052_list_price,2)
      END 
      ) c705_calculated_list_price
  FROM
    (
    /* 
     *  Fetch the price details, 
     *  for the given part and account, if group price and account price exists
     *  fetch the group price. if the part does not have a group price
     *  then fetch the account price
     */    
      SELECT
        c101_party_id ,
        c205_part_number_id ,
        c705_unit_price,
        c2052_list_price,
        row_num
      FROM
        (
          SELECT
            c101_party_id ,
            c205_part_number_id ,
            c705_unit_price ,
            c2052_list_price,
         /*  
         *  Following two columns help to decide to keep one of the two rows fetched for the same part
         *  Example, part number: 124.454  unit price: $200 ( group price )
         *           part number: 124.454  unit price: $250 ( account price )
         *  we need to keep the 1st row and remove the second row.
         */
            row_number() over ( partition BY c101_party_id, c205_part_number_id order by prc_type) row_num ,
            COUNT(1) OVER ( PARTITION BY c101_party_id, c205_part_number_id)
            cnt
          FROM
            (
              -- Fetch the account price for the given account(s) and part(s)
           SELECT
                t704.c101_party_id ,
                t705.c205_part_number_id ,
                t705.c705_unit_price ,
                t2052.c2052_list_price, 
                1 prc_type
              FROM
                t705_account_pricing t705 ,
                t704_account t704,
                t205_part_number t205,
                t2052_part_price_mapping t2052,
                (SELECT DISTINCT c704_account_id, c205_part_number_id FROM MY_TEMP_ACC_PART_LIST) my_list
              WHERE
                  t704.c101_party_id = t705.c101_party_id
              AND t704.c704_account_id IS NOT NULL
              AND t704.c704_account_id = my_list.c704_account_id
              AND t705.c205_part_number_id = my_list.c205_part_number_id
              AND t705.c205_part_number_id = t205.c205_part_number_id
              AND t205.c205_part_number_id = t2052.c205_part_number_id
              AND t2052.c1900_company_id  = t704.c1900_company_id
              AND t704.c704_void_fl is null
              AND t705.c705_void_fl IS NULL
              UNION ALL
               -- Fetch the group price for the given account(s) and part(s)
              SELECT
                t704.c101_party_id ,
                t705.c205_part_number_id ,
                t705.c705_unit_price ,
                t2052.c2052_list_price, 
                2 prc_type
              FROM
                t705_account_pricing t705 ,
                t740_gpo_account_mapping t740,
                t704_account t704,
                t205_part_number t205,
                t2052_part_price_mapping t2052,
                (SELECT DISTINCT c704_account_id, c205_part_number_id FROM MY_TEMP_ACC_PART_LIST) my_list
              WHERE
                t705.c101_party_id        IS NOT NULL
              AND t705.c101_party_id       = t740.c101_party_id
              AND t740.c704_account_id     = t704.c704_account_id
              AND t704.c704_account_id     = my_list.c704_account_id
              AND t705.c205_part_number_id = my_list.c205_part_number_id
              AND t705.c205_part_number_id = t205.c205_part_number_id
              AND t205.c205_part_number_id = t2052.c205_part_number_id
              AND t2052.c1900_company_id  = t704.c1900_company_id
              AND t705.c705_void_fl IS NULL
              AND t704.c704_void_fl IS NULL
              AND t740.c740_void_fl IS NULL
            )
        )
      WHERE
        row_num = cnt
    )
    unit_prc ,
    (
     /* 
     *  Fetch the part level discount details , 
     *  for the given part and account, if discount price is set at group level and account level
     *  fetch the discount at the account level. if the part does not have a account level discount
     *  then fetch the group level discount
     */
      SELECT
        c101_party_id ,
        c205_part_number_id ,
        c901_discount_type ,
        c7052_discount_offered ,
        row_num
      FROM
        (
          SELECT
            c101_party_id ,
            c205_part_number_id ,
            c901_discount_type ,
            c7052_discount_offered ,
             /*  
             *  Following two columns help to decide to keep one of the two rows fetched for the same part
             *  Example, part number: 124.454  discount: $100 ( group level discount )
             *           part number: 124.454  discount: $250 ( account level discount )
             *  we need to keep the 2nd row and remove the first row.
             */
            row_number() over ( partition BY c101_party_id, c205_part_number_id order by dsc_type DESC) row_num ,
            COUNT(1) OVER ( PARTITION BY c101_party_id, c205_part_number_id) cnt
          FROM
            (
               --Fetch Discount at the account level for the given account(s) and part(s)
              SELECT
                t704.c101_party_id ,
                t7052.c205_part_number_id ,
                t7052.c901_discount_type ,
                t7052.c7052_discount_offered ,
                1 dsc_type
              FROM
                t7052_account_pricing_discount t7052 ,
                t704_account t704 ,
                (SELECT DISTINCT c704_account_id, c205_part_number_id FROM MY_TEMP_ACC_PART_LIST) my_list
              WHERE
                t7052.c101_party_id         = t704.c101_party_id
              AND t7052.c7052_void_fl      IS NULL
              AND t704.c704_void_fl        IS NULL
              AND t704.c704_account_id      = my_list.c704_account_id
              AND t7052.c205_part_number_id = my_list.c205_part_number_id
              UNION ALL
               --Fetch Discount at the group level for the given account(s) and part(s)
              SELECT
                t704.c101_party_id ,
                t7052.c205_part_number_id ,
                t7052.c901_discount_type ,
                t7052.c7052_discount_offered ,
                2 dsc_type
              FROM
                t7052_account_pricing_discount t7052 ,
                t740_gpo_account_mapping t740 ,
                t704_account t704 ,
                (SELECT DISTINCT c704_account_id, c205_part_number_id FROM MY_TEMP_ACC_PART_LIST) my_list
              WHERE
                t7052.c101_party_id         = t740.c101_party_id
              AND t740.c704_account_id      = t704.c704_account_id
              AND t7052.c7052_void_fl      IS NULL
              AND t740.c740_void_fl        IS NULL
              AND t704.c704_void_fl        IS NULL
              AND t704.c704_account_id      = my_list.c704_account_id
              AND t7052.c205_part_number_id = my_list.c205_part_number_id                
            )
        )
      WHERE
        row_num = cnt
    )
    disc_prc
  WHERE unit_prc.c101_party_id = disc_prc.c101_party_id(+)
    AND unit_prc.c205_part_number_id = disc_prc.c205_part_number_id(+)
) unit_adj
,
(
    /* 
     *  Fetch the pricing parameter discounts details , 
     *  for the given part and account, if discount price is set at group level and account level
     *  fetch the discount at the group level. if the part does not have a group level discount
     *  then fetch the acount level discount
     */
select c101_party_id
     , wasted
     , revision
     , wasted_price_type
     , revision_price_type
     , DECODE(row_num,1,'ACCT','GPB') wast_rev_adj_from
from
(
  select c101_party_id
       , wasted     
       , revision
       , wasted_price_type
       , revision_price_type
    /*  
     *  Following two columns help to decide to keep one of the two rows fetched for the same part
     *  Example, Part number: 124.454  Waste: 15%  Revision 25%  ( group level discount )
     *           part number: 124.454  Waste: 10%  Revision 20%  ( account level discount )
     *  we need to keep the 1st row and remove the second row.
     */
       , row_number() over ( partition by c101_party_id order by prc_type) row_num
       , count(1) over ( partition by c101_party_id) cnt
    from    
    (
      (
       --Fetch Discount at the account level for the given account(s) and part(s)
      select c101_party_id
           , NVL(max(wasted),0) wasted 
           , NVL(max(revision),0) revision
           , NVL(max(wasted_price_type),0) wasted_price_type
           , NVL(max(revision_price_type),0) revision_price_type
           , 1 prc_type
      from
          (
            select t704.c101_party_id
               , decode(c901_adj_type, 107970, t1706.c1706_adj_value, null) wasted  
               , decode(c901_adj_type, 107971, t1706.c1706_adj_value, null) revision  
               , decode(c901_adj_type, 107970, t1706.c901_price_type, null) wasted_price_type  
               , decode(c901_adj_type, 107971, t1706.c901_price_type, null) revision_price_type
            from t1706_party_price_adj t1706
             , t704_account t704
             , (select distinct c704_account_id from my_temp_acc_part_list) my_list
                 where t1706.c101_party_id = t704.c101_party_id
                   and t704.c704_account_id = my_list.c704_account_id
                   and t704.c704_void_fl is null
                   and t1706.c1706_void_fl is null
          )
      group by c101_party_id
      ) 
      UNION ALL
      (
     	   --Fetch Discount at the group level for the given account(s) and part(s)
           select c101_party_id
               , NVL(max(wasted),0) wasted 
               , NVL(max(revision),0) revision
               , NVL(max(wasted_price_type),0) wasted_price_type
           	   , NVL(max(revision_price_type),0) revision_price_type
               , 2 prc_type
          from
              (select t704.c101_party_id
                   , decode(c901_adj_type, 107970, c1706_adj_value, null) wasted  
                   , decode(c901_adj_type, 107971, c1706_adj_value, null) revision 
                   , decode(c901_adj_type, 107970, t1706.c901_price_type, null) wasted_price_type  
               	   , decode(c901_adj_type, 107971, t1706.c901_price_type, null) revision_price_type
                from t1706_party_price_adj t1706
                   , t704_account t704
	               , t740_gpo_account_mapping t740
	               , (select distinct c704_account_id from my_temp_acc_part_list) my_list
               where t1706.c101_party_id = t740.c101_party_id
                 and t740.c704_account_id = t704.c704_account_id
               	 and t704.c704_account_id = my_list.c704_account_id
               	 and t704.c704_void_fl is null
               	 and t740.c740_void_fl is null
                 and t1706.c1706_void_fl is null
              		)
              where (wasted > 0 OR  revision > 0 )
          group by c101_party_id
        )   
      )
)
where row_num = cnt
) grp_acct_adj
where unit_adj.c101_party_id = grp_acct_adj.c101_party_id (+);
/