--@"C:\Database\Views\Sales\v5010_curr_address.vw";

CREATE OR REPLACE VIEW v5010_curr_address
AS
	SELECT t5010.c5010_tag_id tagid, t5010.c5010_sub_location_id currownid, t5010.c901_sub_location_type currlcnid
		 , get_code_name (t5010.c901_sub_location_type) currlcnnm, v5010_address.phonenumber, v5010_address.emailid
		 , v5010_address.name, v5010_address.address1, v5010_address.address2, v5010_address.city, v5010_address.state
         , v5010_address.country, v5010_address.zipcode, v5010_address.billname, v5010_address.billaddress1
         , v5010_address.billaddress2, v5010_address.billcity, v5010_address.billstate,v5010_address.billcountry
         ,v5010_address.billzipcode
		 ,v5010_address.SHIPEMPTY
	  FROM t5010_tag t5010
		 , (SELECT t5010.c5010_tag_id tagid, t703.c703_sales_rep_name NAME, t703.c703_phone_number phonenumber
				 , t703.c703_email_id emailid, t106.c106_add1 address1, t106.c106_add2 address2, t106.c106_city city
				 , t106.c901_state state, t106.c901_country country, t106.c106_zip_code zipcode
				 , null billname
                 , null billaddress1, null billaddress2, null billcity
                 , null billstate, null billcountry, null billzipcode
                 , null SHIPEMPTY
			  FROM t5010_tag t5010, t703_sales_rep t703, t106_address t106
			 WHERE t5010.c5010_sub_location_id = t703.c703_sales_rep_id
			   AND t5010.c901_sub_location_type = 50222   --Sales rep
			   AND t5010.c106_address_id = t106.c106_address_id
			   AND t106.c101_party_id = t703.c101_party_id
			   AND t106.c106_void_fl IS NULL
			   AND t703.c703_void_fl IS NULL
			   AND t703.c901_ext_country_id IS NULL
			   AND t5010.c5010_void_fl IS NULL
			   AND t5010.c901_status = 51012
			UNION ALL
			SELECT t5010.c5010_tag_id tagid, t704.c704_account_nm NAME, t704.c704_phone phonenumber
				 , t704.c704_email_id emailid, t704.c704_ship_add1 address1, t704.c704_ship_add2 address2
				 , t704.c704_ship_city city, t704.c704_ship_state state, t704.c704_ship_country country
				 , t704.c704_ship_zip_code zipcode
				 , t704.c704_bill_name billname
                 , t704.c704_bill_add1 billaddress1, t704.c704_bill_add2 billaddress2, t704.c704_bill_city billcity
                 , t704.c704_bill_state billstate, t704.c704_bill_country billcountry, t704.c704_bill_zip_code billzipcode
                 , NVL(t704.c704_ship_add1,'SHIPEMPTY') SHIPEMPTY
			  FROM t5010_tag t5010, t704_account t704
			 WHERE t5010.c5010_sub_location_id = t704.c704_account_id
			   AND t5010.c901_sub_location_type = 11301   --Account
			   AND t5010.c5010_void_fl IS NULL
			   AND t5010.c901_status = 51012
			   AND t704.c704_void_fl IS NULL
			   AND t704.c901_ext_country_id IS NULL
			UNION ALL
			SELECT t5010.c5010_tag_id tagid, t701.c701_ship_name NAME, t701.c701_phone_number phonenumber
				 , t701.c701_email_id emailid, t701.c701_ship_add1 address1, t701.c701_ship_add2 address2
				 , t701.c701_ship_city city, t701.c701_ship_state state, t701.c701_ship_country country
				 , t701.c701_ship_zip_code zipcode
				 , t701.c701_bill_name
                 , t701.c701_bill_add1 billaddress1, t701.c701_bill_add2 billaddress2, t701.c701_bill_city billcity
                 , t701.c701_bill_state billstate, t701.c701_bill_country billcountry, t701.c701_bill_zip_code billzipcode
                 , NVL( t701.c701_ship_add1,'SHIPEMPTY') SHIPEMPTY
			  FROM t5010_tag t5010, t701_distributor t701
			 WHERE t5010.c5010_sub_location_id = t701.c701_distributor_id
			   AND t5010.c901_sub_location_type = 50221   -- Distributor
			   AND t5010.c5010_void_fl IS NULL
			   AND t5010.c901_status = 51012
			   AND t701.c701_void_fl IS NULL
			   AND t701.c901_ext_country_id IS NULL	) v5010_address
	 WHERE t5010.c5010_tag_id = v5010_address.tagid;
/
