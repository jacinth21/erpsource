--@"C:\Database\Views\Sales\v7000_pricing_request.vw";
CREATE OR REPLACE VIEW v7000_pricing_request (c7000_account_price_request_id
                                            , c704_account_id
                                            , c4010_group_id
                                            , c207_set_id
                                            ,C901_REQUEST_STATUS
                                            ,c101_gpo_id
                                             )
AS
    SELECT DISTINCT t7000.c7000_account_price_request_id, t7000.c704_account_id, v4010.c4010_group_id
                  , v4010.c207_set_id, t7000.C901_REQUEST_STATUS
                  ,t7000.c101_gpo_id
               FROM v4010_pricing_group v4010, t7000_account_price_request t7000, t7001_group_part_pricing t7001
              WHERE t7001.c7000_account_price_request_id = t7000.c7000_account_price_request_id
                AND v4010.c4010_group_id = t7001.c7001_ref_id
                AND t7001.c901_ref_type = 52000
		AND t7000.c7000_void_fl is null
		AND t7001.c7001_void_fl is null
                AND t7000.C901_REQUEST_STATUS NOT IN (52189,52190);
/
                