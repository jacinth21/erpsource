/* Formatted on 2011/11/04 19:04 (Formatter Plus v4.8.0) */
--@"C:\Database\Views\Sales\v7100_ship_address.vw";
CREATE OR REPLACE VIEW v7100_ship_address
AS
	SELECT t7104.casesetid casesetid, t907.c907_ref_id refid, t907.c907_ship_to_id shiptoid, t907.c901_ship_to shiplcntoid, v907_address.source
		 , get_code_name (t907.c901_ship_to) shiptolcnnm, v907_address.phonenumber, v907_address.emailid
		 , v907_address.address1, v907_address.address2, v907_address.city, v907_address.state, v907_address.country
		 , v907_address.zipcode, t907.c907_ship_from_id shipfromid, t907.c901_ship_from shiplcnfromid
		 , get_code_name (t907.c901_ship_from) shipfromlcnnm,v907_address.addressid,v907_address.name 
		 , v907_address.dmode
	  FROM t907_shipping_info t907
		 , (SELECT  t7104.c7100_case_info_id, t7104.c7104_case_set_id casesetid, nvl(loaner.cnid,nvl(t7104.c526_product_request_detail_id,t7104.c7104_case_set_id)) refid
		            , T526.C525_Product_Request_Id prdreqid, t7104.c7104_shipped_del_fl shippeddelfl
		      FROM t7104_case_set_information t7104, T526_Product_Request_Detail t526,
		    (SELECT DISTINCT t504a.c504a_etch_id,t504a.c504_consignment_id cnid, C526_Product_Request_Detail_Id prdreqdtlid
           FROM t504a_consignment_loaner t504a, t504a_loaner_transaction t504a_txn
          WHERE t504a.c504_consignment_id                = t504a_txn.c504_consignment_id
            AND t504a.c504a_void_fl  IS NULL
            AND t504a_txn.c504a_void_fl IS NULL
            AND c526_product_request_detail_id IS NOT NULL) loaner
            WHERE t7104.c526_product_request_detail_id = loaner.prdreqdtlid(+)
            AND t7104.c526_product_request_detail_id = t526.c526_product_request_detail_id(+)
            AND t526.C526_void_fl IS NULL
            AND t7104.c7104_void_fl IS NULL) t7104
		 , t7100_case_information t7100
		 , (SELECT t907.c907_ref_id refid, t703.c703_sales_rep_name NAME, t703.c703_phone_number phonenumber, t907.c901_source source
				 , t703.c703_email_id emailid, t106.c106_add1 address1, t106.c106_add2 address2, t106.c106_city city
				 , t106.c901_state state, t106.c901_country country, t106.c106_zip_code zipcode,t907.c907_ship_to_id shiptoid,t106.c106_address_id addressid, t907.c525_product_request_id prdreqid
				 , t907.c901_delivery_mode dmode
			  FROM t907_shipping_info t907, t703_sales_rep t703, t106_address t106
			 WHERE t907.c907_ship_to_id = t703.c703_sales_rep_id
			   AND t907.c901_ship_to = 4121   --Sales rep
			   AND t907.c106_address_id = t106.c106_address_id
			   AND t106.c101_party_id = t703.c101_party_id
			   AND t106.c106_void_fl IS NULL
			   AND t703.c703_void_fl IS NULL
			   AND t907.c907_void_fl IS NULL
			   AND t703.c901_ext_country_id IS NULL
			UNION ALL
			SELECT t907.c907_ref_id refid, t704.c704_account_nm NAME, t704.c704_phone phonenumber, t907.c901_source source
				 , t704.c704_email_id emailid, t704.c704_ship_add1 address1, t704.c704_ship_add2 address2
				 , t704.c704_ship_city city, t704.c704_ship_state state, t704.c704_ship_country country
				 , t704.c704_ship_zip_code zipcode,t907.c907_ship_to_id shiptoid ,NULL addressid, t907.c525_product_request_id prdreqid
				 , t907.c901_delivery_mode dmode
			  FROM t907_shipping_info t907, t704_account t704
			 WHERE t907.c907_ship_to_id = t704.c704_account_id
			   AND t907.c901_ship_to = 4122   --Account
			   AND t907.c907_void_fl IS NULL
			   AND t704.c704_void_fl IS NULL
			   AND t704.c901_ext_country_id IS NULL
			UNION ALL
			SELECT t907.c907_ref_id refid, t701.c701_distributor_name NAME, t701.c701_phone_number phonenumber, t907.c901_source source
				 , t701.c701_email_id emailid, t701.c701_ship_add1 address1, t701.c701_ship_add2 address2
				 , t701.c701_ship_city city, t701.c701_ship_state state, t701.c701_ship_country country
				 , t701.c701_ship_zip_code zipcode,t907.c907_ship_to_id shiptoid ,NULL addressid, t907.c525_product_request_id prdreqid
				 , t907.c901_delivery_mode dmode
			  FROM t907_shipping_info t907, t701_distributor t701
			 WHERE t907.c907_ship_to_id = t701.c701_distributor_id
			   AND t907.c901_ship_to = 4120   -- Distributor
			   AND t907.c907_void_fl IS NULL
			   AND t701.c701_void_fl IS NULL
			   AND t701.c901_ext_country_id IS NULL) v907_address
	 WHERE t907.c907_ref_id = v907_address.refid
	   AND NVL(t907.c525_product_request_id,-9999) = NVL(v907_address.prdreqid,-9999)
	   AND NVL(t907.c525_product_request_id,-9999) = NVL(T7104.prdreqid,-9999)
	   AND t907.c901_source = v907_address.source
	   AND t907.c907_ref_id = TO_CHAR (t7104.refid)
	   AND t7104.c7100_case_info_id = t7100.c7100_case_info_id
	   AND t7100.c7100_void_fl IS NULL
	   AND t7100.c901_type  = 1006503  -- CASE
	   AND (t7100.c901_case_status = 11091 OR (t7100.c901_case_status IN(11092,11093) AND t7104.shippeddelfl ='Y'))
       AND t907.c907_ship_to_id = v907_address.shiptoid 
       AND t907.c907_void_fl IS NULL;
/