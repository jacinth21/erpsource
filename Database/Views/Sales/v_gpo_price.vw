CREATE OR REPLACE VIEW v_gpo_price
AS
SELECT    t705.c705_account_pricing_id apid                                                  
        , t705.c704_account_id acctid
        , t705.c101_party_id gpoid
        , t705.c205_part_number_id pnum
        , NULL grpid
        , NULL grouptype
        , t705.c705_unit_price price
        , t205.C205_PART_NUM_DESC pdesc
        , t705.c205_part_number_id ID
        , 1 type
        , t705.c705_discount_offered disc        
        , t705.c705_created_date cdate
        , t101.c101_user_id cuser                                                  
        , t705.c705_last_updated_date ldate    
        , t705.c705_last_updated_by luser                                                
        , t705.c705_history_fl hisfl
        ,NVL(t705.c705_void_fl ,0) void_fl
        ,t705.c705_void_fl voidfl
    FROM t705_account_pricing t705                                                      
        , T205_PART_NUMBER t205
        , t101_user t101
        , v_in_list v_list
    WHERE t705.C205_PART_NUMBER_ID = t205.C205_PART_NUMBER_ID
        AND t705.C705_CREATED_BY = t101.C101_USER_ID        
        AND v_list.token IS NOT NULL
        AND v_list.token = t705.c101_party_id
UNION ALL
SELECT    t7051.c7051_account_group_pricing_id apid                                                           
        , t7051.c704_account_id acctid
        , t7051.c101_gpo_id gpoid
        , NULL pnum
        , c7051_ref_id grpid
        , t4010.c901_type grouptype
        , c7051_price price
        , t4010.c4010_group_nm pdesc 
        , c7051_ref_id ID  
        , 2 type
        , 0 disc
        , t7051.c7051_created_date cdate
        , to_number(t7051.C7051_CREATED_BY) cuser                                                     
        , t7051.c7051_last_updated_date ldate    
        , t7051.c7051_last_updated_by luser                                                
        , C7051_PRICE_HISTORY_FL hisfl
        ,NVL(t7051.c7051_void_fl ,0) void_fl
        ,t7051.c7051_void_fl voidfl 
    FROM t7051_account_group_pricing t7051                                                
        , t4010_group t4010
        , v_in_list v_list
    WHERE t7051.c7051_active_fl = 'Y'       
        AND TO_CHAR (t4010.c4010_group_id) = t7051.c7051_ref_id
        AND v_list.token IS NOT NULL
        AND v_list.token = t7051.c101_gpo_id       
        AND c7051_ref_id IN
        (SELECT DISTINCT TO_CHAR (c4010_group_id)
            FROM t4011_group_detail t4011, t205_part_number t205
            WHERE t4011.c205_part_number_id = t205.c205_part_number_id
        )
UNION ALL
SELECT    t7051.c7051_account_group_pricing_id apid
		, t7051.c704_account_id acctid
		, t7051.c101_gpo_id gpoid
		, c7051_ref_id pnum
		, NULL grpid
		, 0 grouptype
		, c7051_price price
		, t205.c205_part_num_desc pdesc  
		, c7051_ref_id ID                                                                
		, 3 type
		, 0 disc                 
		, t7051.c7051_created_date cdate
		, to_number(t7051.C7051_CREATED_BY) cuser                                                     
		, t7051.c7051_last_updated_date ldate    
		, t7051.c7051_last_updated_by luser                                                
		, C7051_PRICE_HISTORY_FL hisfl
		,NVL(t7051.c7051_void_fl ,0) void_fl
		,t7051.c7051_void_fl voidfl 
	FROM t7051_account_group_pricing t7051                                                
		, T205_PART_NUMBER T205
		, v_in_list v_list
	WHERE t7051.c7051_active_fl = 'Y'		
		AND t7051.C901_REF_TYPE = 52001
		AND T205.C205_PART_NUMBER_ID = c7051_ref_id	   
		AND c7051_ref_id IN
		(SELECT DISTINCT t205.c205_part_number_id
			FROM t4011_group_detail t4011, t205_part_number t205
			WHERE t4011.c205_part_number_id = t205.c205_part_number_id
		)
		AND v_list.token IS NOT NULL
		AND v_list.token = t7051.c101_gpo_id
	ORDER BY type, id;
        /