--@"C:\Database\Views\Sales\v7001_account_unpriced_system.vw";
CREATE OR REPLACE VIEW v7001_account_unpriced_system (c4010_group_id
                                            , c704_account_id
                                            , c207_set_id)
AS
SELECT TO_CHAR (v4010.c4010_group_id) c4010_group_id, token c704_account_id,
       t207.c207_set_id
  FROM v4010_pricing_group v4010, v_in_list, t207_set_master t207
 WHERE token IS NOT NULL
   AND t207.c207_release_for_sale = 'Y'
   AND t207.c207_void_fl IS NULL
   AND c207_obsolete_fl IS NULL
   AND t207.c207_set_id = v4010.c207_set_id
MINUS
(SELECT TO_CHAR (t705.c4010_group_id) c4010_group_id, t705.c704_account_id,
        c207_set_id
   FROM t705_account_pricing_by_group t705
  WHERE t705.c704_account_id IN (SELECT *
                                   FROM v_in_list
                                  WHERE token IS NOT NULL)
 UNION ALL
 SELECT t7051.c7051_ref_id c4010_group_id, t7051.c704_account_id, t4010.c207_set_id
   FROM t7051_account_group_pricing t7051, t4010_group t4010
  WHERE t7051.c901_ref_type = '52000'                                --'GROUP'
    AND t7051.c7051_active_fl = 'Y'
    AND t4010.C4010_GROUP_ID = t7051.c7051_ref_id
    AND t7051.c704_account_id IN (SELECT *
                                    FROM v_in_list
                                   WHERE token IS NOT NULL));                