/**********************************************************************
Purpose: Materialized view with refresh email tracking event details
Author: mmuthusamy
Location: @"C:\PMT\bitbucket\erpGlobus\Database\Views\Sales\mv9161_email_tracking_details.vw"
**********************************************************************/
DROP MATERIALIZED VIEW mv9161_email_tracking_details;

CREATE MATERIALIZED VIEW mv9161_email_tracking_details
    REFRESH
        COMPLETE
        START WITH ( sysdate )
        NEXT ( sysdate + 30 / 1440 )
        WITH ROWID
AS
    ( 
    SELECT
    t9161.c9161_email_sent_master_id   email_sent_master_id,
    t9163.c9163_email_sent_dtls_id     email_dtls_id,
    decode(t9163.c101_party_id, NULL, t9163.c9163_email_id, get_party_name(t9163.c101_party_id)
                                                            || '('
                                                            || t9163.c9163_email_id
                                                            || ')') recipient_name,
    t9163.c9163_email_id                    email_id,                                                        
    t1016.c1016_share_id               share_id,
    t1018.c1018_share_file_id          share_file_id,
    -- , email_dtls.party_name
    t9161.c9161_transaction_id          txn_id,
    t1018.C1018_LINK_ID                url ,  
    t9163.c9163_open_count             open_cnt,
    t9163.c9163_clicked_count          clicked_cnt,
    t1018.c903_upload_file_list        upload_file_list_id,
    get_code_name(t903.c901_ref_grp) file_ref_grp,
    get_code_name(t903.c901_ref_type) file_ref_type,
    get_code_name(t903.c901_type) file_type,
    t903.c903_file_size                file_size,
    t903.c903_ref_id                   file_ref_id,
    get_set_name(t903.c903_ref_id) system_name
    -- 103092 - System , 103114 - DCOed Documents
    ,
    decode(t903.c901_ref_grp, '103092', decode(t903.c901_ref_type, '103114', get_partnum_desc(t903a.title), t903.c901_file_title)
    , t903.c901_file_title) file_title,
    lower(substr(t903.c903_file_name, instr(t903.c903_file_name, '.', - 1, 1) + 1)) fileextension,
    t9161.c703_sales_rep_id            sales_rep_id,
    t9161.c701_distributor_id          dist_id,
    t9161.c101_ad_id                   ad_id,
    t9161.c101_vp_id                   vp_id
    --,        email_master.url
FROM
    globus_app.t1016_share             t1016,
    globus_app.t1018_share_file        t1018,
    globus_app.t903_upload_file_list   t903,
    t9161_email_sent_master            t9161
    --
    ,
    t9163_email_sent_dtls              t9163,
    (
        SELECT
            c903_upload_file_list fileid,
            MAX(decode(c901_attribute_type, 103216, c903a_attribute_value)) title,
            MAX(decode(c901_attribute_type, 103217, c903a_attribute_value)) shareable
        FROM
            t903a_file_attribute
        WHERE
            c903a_void_fl IS NULL
        GROUP BY
            c903_upload_file_list
    ) t903a
WHERE
    t1016.c1016_share_id = t1018.c1016_share_id
    AND t903.c903_upload_file_list = t1018.c903_upload_file_list
    AND t1016.c1016_share_id = t9161.c9161_transaction_id
    --
    AND t9161.c9161_email_sent_master_id = t9163.c9161_email_sent_master_id
    AND t903.c903_upload_file_list = t903a.fileid (+)
    AND t1016.c1016_void_fl IS NULL
    AND t1018.c1018_void_fl IS NULL
    AND t903.c903_delete_fl IS NULL
    AND t9163.c9163_void_fl IS NULL
    AND t9161.c9161_void_fl IS NULL
    );