/* Formatted on 2009/03/18 15:09 (Formatter Plus v4.8.0) */
 --@"c:\database\Procedures\tmp\tmp_gm_reverse_transfer.prc";
-- exec tmp_gm_reverse_transfer ('TF-1163', '303043');
CREATE OR REPLACE PROCEDURE tmp_gm_reverse_transfer (
	p_trns_id	IN	 t920_transfer.c920_transfer_id%TYPE
  , p_userid	IN	 t902_log.c902_created_by%TYPE
)
AS
	v_from_id	   t920_transfer.c920_from_ref_id%TYPE;
	v_to_id 	   t920_transfer.c920_to_ref_id%TYPE;
BEGIN
	SELECT	   c920_from_ref_id, c920_to_ref_id
		  INTO v_from_id, v_to_id
		  FROM t920_transfer
		 WHERE c920_transfer_id = p_trns_id
	FOR UPDATE;

	UPDATE t504_consignment
	   SET c701_distributor_id = v_from_id
	 WHERE c504_consignment_id IN (SELECT c921_ref_id
									 FROM t921_transfer_detail
									WHERE c920_transfer_id = p_trns_id AND c901_link_type = 90360);

	UPDATE t506_returns
	   SET c701_distributor_id = v_from_id
	 WHERE c506_rma_id IN (SELECT c921_ref_id
							 FROM t921_transfer_detail
							WHERE c920_transfer_id = p_trns_id AND c901_link_type = 90361);

	UPDATE t920_transfer
	   SET c920_void_fl = 'Y'
	 WHERE c920_transfer_id = p_trns_id;

	-- save comments
	/* there are 3 types for void transfer, we using 90282		Wrong Transfer
		90280		Wrong Distributor
		90281		Wrong Parts
		90282		Wrong Transfer
	*/
	INSERT INTO t902_log
				(c902_log_id, c902_ref_id, c902_comments, c902_type, c902_created_by, c902_created_date
				)
		 VALUES (s507_log.NEXTVAL, p_trns_id, 'This Transfer is void', 90282, p_userid, SYSDATE
				);
--
END tmp_gm_reverse_transfer;
/
