/* Formatted on 2009/01/15 14:57 (Formatter Plus v4.8.0) */

--@"C:\Database\Temp Load Procedure\tmp_orders_shipping.prc";
/*	SQL to be verified before executing the procedure
SELECT * FROM
		( 
		SELECT	 t501.c501_order_id ordid, t501.C501_ORDER_DATE,  t501.c501_shipping_date shipdate
			   , DECODE (t501.C501_SHIPPING_DATE, NULL, DECODE (t501.c501_status_fl, 0, 10, 1, 10, 2, 20, 3, 40,40), 40	) stflg
			   , t501.c501_void_fl vdflg
			   , DECODE (t501.c501_ship_to, 0, 4124,t501.c501_ship_to ) shipto
			   , DECODE(t501.c501_ship_to, 4120, GET_DISTRIBUTOR_ID(c703_sales_rep_id), 4122, t501.C704_ACCOUNT_ID ,t501.c501_ship_to_id )shiptoid	 --4124  N/A
			   , DECODE (t501.c501_delivery_carrier, 0, 5040,t501.c501_delivery_carrier) delcar
			   , DECODE (t501.c501_delivery_mode, 0, 5031,t501.c501_delivery_mode) delmode	 --5031  N/A, 5040	N/A
			   , t501.c501_tracking_number trackno, t501.c501_ship_cost COST
			   , DECODE (t501.c501_ship_to, 4121, temp_get_address_id (c703_sales_rep_id),NULL) address_id
			   ,  t501.C704_ACCOUNT_ID
			   ,c901_order_type
			   --, c703_sales_rep_id
			   --, GET_REP_NAME(c703_sales_rep_id)
			FROM t501_order t501
			 WHERE t501.c501_void_fl IS NULL AND NVL(c901_order_type,1) NOT IN (2523,2524,2528,2526,2531,2533) --(c901_order_type = 2525 OR c901_order_type IS NULL)	 --Back Order
		) WHERE STFLG < 40
		
		SELECT COUNT(1) FROM t907_shipping_info ;

		UPDATE  t907_shipping_info t907
			SET t907.C106_ADDRESS_ID =  temp_get_address_id(t907.C907_SHIP_TO_ID)
			WHERE t907.c901_source <>  '50180'
			and t907.C901_SHIP_TO =  4120

*/

CREATE OR REPLACE PROCEDURE tmp_orders_shipping
AS
	CURSOR cur_orders
	IS
		SELECT	 t501.c501_order_id ordid, t501.c501_order_date ordate, t501.c501_shipping_date shipdate
			   , DECODE (t501.c501_shipping_date
					   , NULL, DECODE (t501.c501_status_fl, 0, 10, 1, 10, 2, 20, 3, 40, 40)
					   , 40
						) stflg
			   , t501.c501_void_fl vdflg, DECODE (t501.c501_ship_to, 0, 4124, t501.c501_ship_to) shipto
			   , DECODE (t501.c501_ship_to
					   , 4120, get_distributor_id (c703_sales_rep_id)
					   , 4122, t501.c704_account_id
					   , t501.c501_ship_to_id
						) shiptoid	 --4124  N/A
			   , DECODE (t501.c501_delivery_carrier, 0, 5040, t501.c501_delivery_carrier) delcar
			   , DECODE (t501.c501_delivery_mode, 0, 5031, t501.c501_delivery_mode) delmode   --5031  N/A, 5040  N/A
			   , t501.c501_tracking_number trackno, t501.c501_ship_cost COST
			   , DECODE (t501.c501_ship_to, 4121, temp_get_address_id (NVL(t501.c501_ship_to_id, c703_sales_rep_id)), NULL) address_id
			 --  , t501.c704_account_id, c901_order_type
			--, c703_sales_rep_id
			--, GET_REP_NAME(c703_sales_rep_id)
		FROM	 t501_order t501
		   WHERE t501.c501_void_fl IS NULL
			 AND NVL (c901_order_type, 1) NOT IN
					 (2523, 2524, 2528, 2526, 2531, 2533,2529)	--(c901_order_type = 2525 OR c901_order_type IS NULL)	--Back Order
		ORDER BY c501_order_id;
BEGIN
	FOR curindex IN cur_orders
	LOOP
		--DBMS_OUTPUT.put_line ('From and To Date  ****');
		INSERT INTO t907_shipping_info
					(c907_shipping_id, c907_ref_id, c901_source, c106_address_id, c907_shipped_dt, c907_status_fl
				   , c901_ship_to, c907_ship_to_id, c901_delivery_carrier, c901_delivery_mode, c907_tracking_number
				   , c907_frieght_amt, c907_created_by, c907_created_date, c907_release_dt, c907_ship_to_dt
					)
			 VALUES (s907_ship_id.NEXTVAL, curindex.ordid, 50180, curindex.address_id, curindex.shipdate, curindex.stflg
				   , curindex.shipto, curindex.shiptoid, curindex.delcar, curindex.delmode, curindex.trackno
				   , curindex.COST, 30301, SYSDATE, DECODE (curindex.stflg, 40, curindex.shipdate, NULL), curindex.ordate
					);
	--50180  orders
	END LOOP;
END tmp_orders_shipping;
