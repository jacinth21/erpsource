/* Formatted on 2009/05/27 09:27 (Formatter Plus v4.8.0) */
/* while formatting in formatter plus, this pl/sql will give error since CONNECT_BY_ROOT was introduced in oracle 10g and this formatter doesnt recognize it */
-- @"C:\database\packages\tmp\tmp_gm_pkg_set_newgrouping.bdy";
-- exec tmp_gm_pkg_set_newgrouping.tmp_gm_set_newgroup_mapping;

CREATE OR REPLACE PACKAGE BODY tmp_gm_pkg_set_newgrouping
IS
/*
0. Update C901_HIERARCHY in set_master to mark main / additional set

1. For every 1600 (like 300.117) - main group of setIds, insert it
	2. Insert a new SET.134 for {System_Name} Additional Inventory
		2.1 Insert 300.117 into  T207a_set_link and map to SET.134, 20002

	3. Fetch all 1601 (like ('965.901', '965.902', '965.903', '965.904', '965.905')) -
		3.1. For every id - 965.905, insert it as 1601
		3.2. IF setId (965.905) is mainSetId THEN create new SET.133 - Grp (1604)
			3.2.1. Insert AI.001 for 965.905 etc with 1603
			3.2.1. Insert 300.117 into	T207a_set_link and map to SET.133, 20002
			3.2.2. Insert SET.133 into	T207a_set_link and map to 965.905, 20003
			3.2.3. Insert AI.001 into  T207a_set_link and map to 965.905, 20004
		3.3 ELSE
			3.3.1. Insert AI.003 for 965.902 etc with 1603
			3.3.2. Insert SET.134 into	T207a_set_link and map to AI.003, 20003
			3.3.3. Insert AI.003  into	T207a_set_link and map to 965.902, 20004

	4. Create new set AA.117 {System Name} - Items with type 1605
		4.1 Insert SET.134 into  T207a_set_link and map to AA.117, 20003
*/
	PROCEDURE tmp_gm_set_newgroup_mapping
	AS
		v_root_set_id  t207_set_master.c207_set_id%TYPE;
		v_root_set_name t207_set_master.c207_set_nm%TYPE;
		v_pd_set_id	 t207_set_master.c207_set_id%TYPE;
		v_pd_set_name  t207_set_master.c207_set_nm%TYPE;
		v_pd_cons_rpt_id t207_set_master.c901_cons_rpt_id%TYPE;
		v_main_set_id  t207_set_master.c207_set_id%TYPE;
		v_main_ai_set_id t207_set_master.c207_set_id%TYPE;
		v_ai_set_id	 t207_set_master.c207_set_id%TYPE;
		v_aae_set_id t207_set_master.c207_set_id%TYPE;
		v_SET_seq_no NUMBER;
		v_seq_no	   NUMBER := 100;
		v_main_seq_no  NUMBER := 0;
		v_main_seq_no_sec NUMBER := 50;
		v_pd_category  t207_set_master.c207_category%TYPE;
		v_set_hierarchy t207_set_master.c901_hierarchy%TYPE;
		v_setgrptype	 t207_set_master.c901_set_grp_type%TYPE;
		

		CURSOR cur_set_list_1600
		IS
			SELECT DISTINCT T207.C207_SET_SALES_SYSTEM_ID root_set_id, regexp_replace (get_set_name(T207.C207_SET_SALES_SYSTEM_ID), '.?trade;|.?reg;|.?\(.*\)') root_set_name
				FROM t207_set_master t207;


		CURSOR cur_set_list_1601
		IS
			SELECT C207_SET_SALES_SYSTEM_ID, c207_set_id pd_set_id, t207.c207_set_nm pd_set_name, c901_cons_rpt_id consrptid
				 , c207_category CATEGORY--, c901_hierarchy hierarchyv
			  FROM t207_set_master t207
				 WHERE C207_SET_SALES_SYSTEM_ID = v_root_set_id
				 ORDER BY pd_set_name;
	BEGIN
--
-- 0.
/*
		*********** DONT RUN THIS AS DATA ALREADY MAPPED TROUGH UPDATE SCRIPT ---------------------------
				tmp_gm_pkg_set_newgrouping.tmp_gm_upd_mainset;
		tmp_gm_pkg_set_newgrouping.tmp_map_system_id;
*/
		FOR set_val_1600 IN cur_set_list_1600
		LOOP
			v_root_set_id := set_val_1600.root_set_id;
			v_root_set_name := set_val_1600.root_set_name;
			v_seq_no	:= 100;
			v_main_seq_no := 0;
			v_main_seq_no_sec := 50;

			SELECT 'SET.' || SUBSTR (v_root_set_id, INSTR (v_root_set_id, '.') + 1) --|| s207_set_master_main_set.NEXTVAL
			  INTO v_main_ai_set_id
			  FROM DUAL;

			-- 2. Insert a new SET.134 for {System_Name} Additional Inventory. Insert 300.117 into	T207a_set_link and map to SET.134, 20002
			tmp_sav_set_master (v_main_ai_set_id
							  , v_root_set_name || ' Additional Inventory'
							  , 'Additional Inventory details for ' || v_root_set_name
							  , 4060
							  , 1604
							  , 999	 -- seq #
							  , 20101
							  , 20701	
							   );	-- should we need a new type for Additional Inventory ?
			tmp_gm_sav_set_link (v_root_set_id, v_main_ai_set_id, 20002);
			
				-- will form string like AAE.117 if root set is 300.117
			 SELECT  'AAE.' || SUBSTR (v_root_set_id, INSTR (v_root_set_id, '.') + 1) INTO v_aae_set_id FROM DUAL;
			tmp_sav_set_master (v_aae_set_id
							  , v_root_set_name || ' - Additional Items'
							  , v_root_set_name || ' - Additional Items'
							  , NULL
							  , 1604
							  , 299
							  , NULL
							  , 20706
							   );	-- should we need a new type for Additional Inventory ?
			tmp_gm_sav_set_link (v_main_ai_set_id, v_aae_set_id, 20003);
			
			-- will form string like AA.117 if root set is 300.117
			 SELECT   'AA.' || SUBSTR (v_root_set_id, INSTR (v_root_set_id, '.') + 1) INTO v_ai_set_id FROM DUAL;
			tmp_sav_set_master (v_ai_set_id
							  , v_root_set_name || ' - Items'
							  , v_root_set_name || ' - Items'
							  , NULL
							  , 1605
							  , 199
							  , NULL
							  , 20703
							   );	-- should we need a new type for Additional Inventory ?
			tmp_gm_sav_set_link (v_aae_set_id, v_ai_set_id, 20005);

			-- 3. Fetch all 1601 (like ('965.901', '965.902', '965.903', '965.904', '965.905')) -
			FOR set_list_main IN cur_set_list_1601
			LOOP
				v_pd_set_id := set_list_main.pd_set_id;   -- 965.905
				v_pd_set_name := set_list_main.pd_set_name;   -- COLONIAL Instrument Set
				v_pd_cons_rpt_id := set_list_main.consrptid;
				v_pd_category := set_list_main.CATEGORY;
				--v_set_hierarchy := set_list_main.hierarchyv;

				-- this wont throw NO_DATA_FOUND...
				-- get this from cursor
		SELECT c901_hierarchy
				  INTO v_set_hierarchy
				  FROM t207_set_master
				 WHERE c207_set_id = v_pd_set_id;

				-- 3.1.1 IF setId (965.905) is mainSetId THEN create new SET.133 - Grp (1604)
				IF (v_set_hierarchy = 20700)
				THEN
				/*	SELECT 'SET.' || s207_set_master_main_set.NEXTVAL
					  INTO v_main_set_id
					  FROM DUAL;
				*/
					IF v_pd_cons_rpt_id = 20100
					THEN
						v_setgrptype := 1606;
						v_main_seq_no := v_main_seq_no + 1;
						v_SET_seq_no := v_main_seq_no;

						UPDATE t207_set_master
							 SET c207_seq_no = v_main_seq_no
						 WHERE c207_set_id = v_pd_set_id;
					ELSE
						v_setgrptype := 1604;
						v_main_seq_no_sec := v_main_seq_no_sec + 1;
						v_SET_seq_no := v_main_seq_no_sec;

						UPDATE t207_set_master
							 SET c207_seq_no = v_main_seq_no_sec
						 WHERE c207_set_id = v_pd_set_id;
					END IF;

				/*	-- 2. Insert a new SET.133 for 965.905. Insert 300.117 into T207a_set_link and map to SET.134, 20002
					tmp_sav_set_master (v_main_set_id
									  , v_pd_set_name
									  , v_pd_set_name || ' - Main'
									  , v_pd_category
									  , v_setgrptype
									  , v_SET_seq_no
									  , v_pd_cons_rpt_id
									  , v_set_hierarchy
										);	-- should we need a new type for Additional Inventory ?
				*/
					SELECT 'AI.' || s207_set_master_ai_set.NEXTVAL
					  INTO v_ai_set_id
					  FROM DUAL;

					v_main_seq_no := v_main_seq_no + 1;
					v_main_seq_no_sec := v_main_seq_no_sec + 1;
					-- 2. Insert AI.001 for 965.905 etc with 1603
					tmp_sav_set_master (v_ai_set_id
									  , v_pd_set_name || ' Additional Item'
									  , v_pd_set_name || ' - Additional Inventory'
									  , v_pd_category
									  , 1603
									  , v_main_seq_no_sec
									  , NULL
									  , 20704 -- Additional From Main Set
										);	-- should we need a new type for Additional Inventory ?
				/*	tmp_gm_sav_set_link (v_root_set_id, v_main_set_id, 20002);
					tmp_gm_sav_set_link (v_main_set_id, v_pd_set_id, 20003); */
					tmp_gm_sav_set_link (v_root_set_id, v_pd_set_id, 20002);
					tmp_gm_sav_set_link (v_main_ai_set_id, v_ai_set_id, 20003);
					tmp_gm_sav_set_link (v_ai_set_id, v_pd_set_id, 20004);
				ELSE
					UPDATE t207_set_master
						SET c207_seq_no = v_main_seq_no
					 WHERE c207_set_id = v_pd_set_id;	-- 965.902 comes at the top

					SELECT 'AI.' || s207_set_master_ai_set.NEXTVAL
					  INTO v_ai_set_id
					  FROM DUAL;

					-- 2. Insert AI.001 for 965.905 etc with 1603
					tmp_sav_set_master (v_ai_set_id
									  , v_pd_set_name || ' Additional Item'
									  , v_pd_set_name || ' - Additional Inventory'
									  , v_pd_category
									  , 1603
									  , v_seq_no
									  , NULL
									  , 20705 -- Additional From Additional Inventory Set
										);	-- should we need a new type for Additional Inventory ?
					tmp_gm_sav_set_link (v_main_ai_set_id, v_pd_set_id, 20003);
					tmp_gm_sav_set_link (v_ai_set_id, v_pd_set_id, 20004);
					tmp_gm_sav_set_link (v_aae_set_id, v_ai_set_id, 20005);
					v_seq_no	:= v_seq_no + 1;
				END IF;
			END LOOP;
		END LOOP;
	END tmp_gm_set_newgroup_mapping;

/*
This procedure will Update C901_HIERARCHY in set_master to mark main set
*/
	PROCEDURE tmp_gm_upd_mainset
	AS
	BEGIN
-- 0. Update all the
		UPDATE t207_set_master t207
			 SET c901_hierarchy = 20700	-- main set
		 WHERE c207_set_id IN (
				   SELECT	CONNECT_BY_ROOT t207a.c207_link_set_id lkid
						 FROM clone_T207A_SET_LINK t207a
						WHERE t207a.c207_main_set_id IN (SELECT t207.c207_set_id
															 FROM clone_t207_set_master t207
															WHERE t207.c901_set_grp_type = 1600)
							AND tmp_gm_pkg_set_newgrouping.tmp_get_current_set_count (t207a.c207_link_set_id) = 1
							AND LEVEL = 2
				   CONNECT BY PRIOR c207_main_set_id = t207a.c207_link_set_id);

		UPDATE t207_set_master t207
			 SET c901_hierarchy = 20702	-- AIS set
		 WHERE c207_set_id IN (
				   SELECT		CONNECT_BY_ROOT t207a.c207_link_set_id
						 FROM t207a_set_link t207a
						--, t207_set_master t207
				   WHERE	  t207a.c207_main_set_id IN (
													SELECT t207.c207_set_id
													  FROM t207_set_master t207
													 WHERE t207.c901_set_grp_type = 1600)
							AND gm_pkg_sm_part_consign_info.get_current_set_count (t207a.c207_link_set_id) > 1
							AND LEVEL = 2
				   CONNECT BY PRIOR c207_main_set_id = t207a.c207_link_set_id)
			 AND t207.c901_set_grp_type = 1601;

		COMMIT;
	END tmp_gm_upd_mainset;

/*
This function gives a count of the number of child references in set_link. We use this count to mark a main / additional set
*/
	FUNCTION tmp_get_current_set_count (
		p_set_id	 t207_set_master.c207_set_id%TYPE
	)
		RETURN NUMBER
	IS
		v_value 	   NUMBER;
	BEGIN
--
		SELECT COUNT (1)
			INTO v_value
			FROM clone_T207A_SET_LINK t207a
		 WHERE t207a.c207_main_set_id = p_set_id AND t207a.c901_type = 20003;

		--
		RETURN v_value;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN 0;
	END tmp_get_current_set_count;

/*
Procedure to insert into set_link table
*/
	PROCEDURE tmp_gm_sav_set_link (
		p_main_set_id	  t207a_set_link.c207_main_set_id%TYPE
	  , p_link_set_id	  t207a_set_link.c207_link_set_id%TYPE
	  , p_set_link_type   t207a_set_link.c901_type%TYPE
	)
	AS
	BEGIN
		INSERT INTO t207a_set_link
			 VALUES (s207a_set_link.NEXTVAL, p_main_set_id, p_link_set_id, p_set_link_type,NULL);
	END tmp_gm_sav_set_link;

/*
Procedure to insert into set_master table
*/
	PROCEDURE tmp_sav_set_master (
		p_set_id	   t207_set_master.c207_set_id%TYPE
	  , p_set_name	   t207_set_master.c207_set_nm%TYPE
	  , p_set_desc	   t207_set_master.c207_set_desc%TYPE
	  , p_category	   t207_set_master.c207_category%TYPE
	  , p_setgrptype	t207_set_master.c901_set_grp_type%TYPE
	  , p_seqno 		 t207_set_master.c207_seq_no%TYPE
	  , p_consrptid	t207_set_master.c901_cons_rpt_id%TYPE
	  , p_hierarchy	t207_set_master.c901_hierarchy%TYPE
	)
	AS
	BEGIN
		INSERT INTO t207_set_master
					(c207_set_id, c207_set_nm, c207_set_desc, c207_created_date, c207_created_by, c207_category
				   , c901_set_grp_type, c207_seq_no, c901_cons_rpt_id, c901_hierarchy
					)
			 VALUES (p_set_id, p_set_name, p_set_desc, SYSDATE, 30301, p_category
				   , p_setgrptype, p_seqno, p_consrptid, p_hierarchy
					);
	END tmp_sav_set_master;
	
	/*
	* Map the System ID ie 300.117 to the corresponding sets like 965.905
	*/
	PROCEDURE tmp_map_system_id
	AS
	v_root_set_id  t207_set_master.c207_set_id%TYPE;
		v_pd_set_id	 t207_set_master.c207_set_id%TYPE;
	CURSOR cur_set_list_1600
		IS
			SELECT t207.c207_set_id root_set_id
				  FROM t207_set_master t207
			 WHERE t207.c901_set_grp_type = 1600 AND C207_OBSOLETE_FL IS NULL AND C207_VOID_FL IS NULL;	--AND t207.c207_set_id = '300.112';

		CURSOR cur_set_list_1601
		IS
			SELECT distinct tmp.lkid pd_set_id
			  FROM clone_t207_set_master t207
				 , (SELECT	   t207a.c207_main_set_id mainid    , CONNECT_BY_ROOT t207a.c207_link_set_id lkid
							FROM clone_T207A_SET_LINK t207a
						 WHERE t207a.c207_main_set_id IN (SELECT t207.c207_set_id
															FROM clone_t207_set_master t207
															 WHERE t207.c901_set_grp_type = 1600)
					CONNECT BY PRIOR c207_main_set_id = t207a.c207_link_set_id) tmp
			 WHERE tmp.lkid = t207.c207_set_id
			   AND t207.c901_set_grp_type = 1601
			   AND tmp.mainid = v_root_set_id
			   AND NVL (t207.c207_seq_no, 1) <> 9999;
	BEGIN
	
		FOR set_val_1600 IN cur_set_list_1600
		LOOP
			v_root_set_id := set_val_1600.root_set_id;
			
			FOR set_list_main IN cur_set_list_1601
			LOOP
				v_pd_set_id := set_list_main.pd_set_id;   --
				
				UPDATE t207_set_master
				SET C207_SET_SALES_SYSTEM_ID = v_root_set_id
				WHERE  c207_set_id = v_pd_set_id;
			END LOOP;	
			
		END LOOP;
			
	END tmp_map_system_id;
	
	
	/*

	--Save Set master

PROCEDURE tmp_gm_save_setmaster (
	p_projid		 IN 	  t207_set_master.c202_project_id%TYPE
  , p_setnm 		 IN 	  t207_set_master.c207_set_nm%TYPE
  , p_setdesc		 IN 	  t207_set_master.c207_set_desc%TYPE
  , p_setcat		 IN 	  t207_set_master.c207_category%TYPE
  , p_settype		 IN 	  t207_set_master.c207_type%TYPE
  , p_type			 IN 	  t207_set_master.c901_set_grp_type%TYPE
  , p_userid		 IN 	  t207_set_master.c207_created_by%TYPE
  , p_action		 IN 	  VARCHAR2
  , p_status		 IN 	  t207_set_master.c901_status_id%TYPE
  , p_revlvl		 IN 	  t207_set_master.c207_rev_lvl%TYPE
  , p_coresetfl 	 IN 	  t207_set_master.c207_core_set_fl%TYPE
  , p_sethierarchy	 IN 	  t207_set_master.c901_hierarchy%TYPE
  , p_baselinefl	 IN 	  VARCHAR2
  , p_salesgroup	 IN 	  t207_set_master.c207_set_id%TYPE
  , p_setid 		 IN OUT   t207_set_master.c207_set_id%TYPE
)
AS
	v_tmp_setid    t207_set_master.c207_set_id%TYPE;
	v_status	   t207_set_master.c901_status_id%TYPE;
	v_setcat	   t207_set_master.c207_category%TYPE;
	v_consrptid    t207_set_master.c901_cons_rpt_id%TYPE;
	v_main_set_id  t207_set_master.c207_set_id%TYPE;
	v_ai_set_id    t207_set_master.c207_set_id%TYPE;
	v_link_cnt	   NUMBER;
	v_seq_no	   NUMBER;
	v_root_set_id  t207_set_master.c207_set_id%TYPE;
	v_pd_set_id    t207_set_master.c207_set_id%TYPE;
	v_set_type	   t207_set_master.c207_type%TYPE;
	v_set_hierarchy t207_set_master.c901_hierarchy%TYPE;
	v_main_ai_set_id t207_set_master.c207_set_id%TYPE;
	v_link_set_id  t207_set_master.c207_set_id%TYPE;
BEGIN

	IF p_action = 'Add'
	THEN
		v_setcat	:= p_setcat;
		v_status	:= 20365;	-- For created status

		BEGIN
			SELECT c207_set_id
			  INTO v_tmp_setid
			  FROM t207_set_master
			 WHERE c207_set_id = p_setid;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				v_tmp_setid := '';
		END;

		IF p_type = 1600
		THEN
			IF p_setid IS NULL
			THEN
--				v_setcat	:= 4060;   -- For category is implants
				v_status	:= NULL;

				SELECT '300.' || s207_set_master_system.NEXTVAL
				  INTO p_setid
				  FROM DUAL;
			END IF;
		END IF;

		IF v_tmp_setid = p_setid
		THEN
			raise_application_error (-20032, '');
		ELSE
			--Condition for base line flag..
			IF p_baselinefl = 'Y'	--discuss for the on more condition of 1600 ...?
			THEN
				v_consrptid := 20100;
			ELSE
				v_consrptid := 20101;
			END IF;

			INSERT INTO t207_set_master
						(c202_project_id, c207_set_id, c207_set_nm, c207_set_desc, c207_category, c207_type
					   , c901_set_grp_type, c207_created_by, c207_created_date, c901_status_id, c207_rev_lvl
					   , c207_core_set_fl, c901_cons_rpt_id, c901_hierarchy
						)
				 VALUES (p_projid, p_setid, p_setnm, p_setdesc, v_setcat, p_settype
					   , p_type, p_userid, SYSDATE, v_status, p_revlvl
					   , p_coresetfl, v_consrptid, p_sethierarchy
						);

			IF p_type = 1601   --fROM THE SETUP SCREEN
			THEN
				IF p_settype = 4070
				--for consignment (discuss for the on more condition of 1600 ...?)
				THEN
					v_pd_set_id := p_setid;
					v_root_set_id := p_salesgroup;

					BEGIN
						SELECT t207a.c207_link_set_id
						  INTO v_link_set_id
						  FROM t207a_set_link t207a
						 WHERE t207a.t207a_set_link_id IN (SELECT MIN (t207a_set_link_id)
															 FROM t207a_set_link
															WHERE c207_main_set_id = v_root_set_id);
					EXCEPTION
						WHEN NO_DATA_FOUND
						THEN
							raise_application_error (-20156, '');
					END;

				--	need to test following case
					SELECT COUNT (*)
					  INTO v_link_cnt
					  FROM t207a_set_link
					 WHERE c207_link_set_id = p_setid;

					IF v_link_cnt > 0
					THEN
						RETURN;
					
					ELSE
						IF p_sethierarchy = 20700	--for Standard Set
						THEN
							SELECT 'SET.' || s207_set_master_main_set.NEXTVAL
							  INTO v_main_set_id
							  FROM DUAL;

							tmp_gm_pkg_set_newgrouping.gm_sav_set_master (v_main_set_id
																	, p_setnm
																	, p_setdesc || ' - Main'
																	, v_setcat
																	, p_type
																	, NULL
																	, v_consrptid
																	, p_sethierarchy
																	, p_userid
																	 );

							SELECT 'AI.' || s207_set_master_ai_set.NEXTVAL
							  INTO v_ai_set_id
							  FROM DUAL;

							tmp_gm_pkg_set_newgrouping.gm_sav_set_master (v_ai_set_id
																	, p_setnm || ' Additional Item'
																	, p_setdesc || ' - Additional Inventory'
																	, v_setcat
																	, 1603
																	, NULL
																	, v_consrptid
																	, p_sethierarchy
																	, p_userid
																	 );
							tmp_gm_pkg_set_newgrouping.tmp_gm_sav_set_link (v_root_set_id, v_main_set_id, 20002);
							tmp_gm_pkg_set_newgrouping.tmp_gm_sav_set_link (v_main_set_id, v_pd_set_id, 20003);
							tmp_gm_pkg_set_newgrouping.tmp_gm_sav_set_link (v_link_set_id, v_ai_set_id, 20003);
							tmp_gm_pkg_set_newgrouping.tmp_gm_sav_set_link (v_ai_set_id, v_pd_set_id, 20004);
						ELSE
							SELECT 'AI.' || s207_set_master_ai_set.NEXTVAL
							  INTO v_ai_set_id
							  FROM DUAL;

							tmp_gm_pkg_set_newgrouping.gm_sav_set_master (v_ai_set_id
																	, p_setnm || ' Additional Item'
																	, p_setdesc || ' - Additional Inventory'
																	, v_setcat
																	, 1603
																	, NULL
																	, v_consrptid
																	, p_sethierarchy
																	, p_userid
																	 );
							tmp_gm_pkg_set_newgrouping.tmp_gm_sav_set_link (v_link_set_id, v_pd_set_id, 20003);
							tmp_gm_pkg_set_newgrouping.tmp_gm_sav_set_link (v_link_set_id, v_ai_set_id, 20003);
							tmp_gm_pkg_set_newgrouping.tmp_gm_sav_set_link (v_ai_set_id, v_pd_set_id, 20004);
						END IF;
					END IF;
				END IF;
			ELSIF p_type = 1600   --FROM THE SYSTEM SCREEN
			THEN
				v_root_set_id := p_setid;

				SELECT s207_set_master_main_set.NEXTVAL
				  INTO v_seq_no
				  FROM DUAL;

				v_main_ai_set_id := 'SET.' || v_seq_no;
				tmp_gm_pkg_set_newgrouping.gm_sav_set_master (v_main_ai_set_id
														, p_setnm || ' Additional Inventory'
														, ' - Additional Inventory details for ' || p_setdesc
														, 4060
														, 1604
														, (999 - v_seq_no)
														, 20101
														, NULL
														, p_userid
														 );
				tmp_gm_pkg_set_newgrouping.tmp_gm_sav_set_link (v_root_set_id, v_main_ai_set_id, 20002);
				v_ai_set_id := 'AA.' || SUBSTR (v_root_set_id, INSTR (v_root_set_id, '.') + 1);
				tmp_gm_pkg_set_newgrouping.gm_sav_set_master (v_ai_set_id
														, p_setnm || '- Items'
														, p_setnm || '- Items'
														, NULL
														, 1605
														, NULL
														, NULL
														, NULL
														, p_userid
														 );
				tmp_gm_pkg_set_newgrouping.tmp_gm_sav_set_link (v_main_ai_set_id, v_ai_set_id, 20003);
			END IF;   --end if for type != 1600
		END IF;
	ELSE
		IF p_baselinefl = 'Y'	--discuss for the on more condition of 1600 ...?
		THEN
			v_consrptid := 20100;
		ELSE
			v_consrptid := 20101;
		END IF;

		tmp_gm_pkg_set_newgrouping.gm_remove_set_link (p_setid, p_settype, p_sethierarchy, p_salesgroup, p_userid);

		UPDATE t207_set_master
		   SET c207_set_nm = p_setnm
			 , c207_set_desc = p_setdesc
			 , c207_category = p_setcat
			 , c207_type = p_settype
			 , c207_last_updated_by = p_userid
			 , c207_last_updated_date = SYSDATE
			 , c901_status_id = p_status
			 , c207_rev_lvl = p_revlvl
			 , c207_core_set_fl = p_coresetfl
			 , c901_cons_rpt_id = v_consrptid
			 , c901_hierarchy = p_sethierarchy
		 WHERE c207_set_id = p_setid;
	END IF;
END tmp_gm_save_setmaster;*/

END tmp_gm_pkg_set_newgrouping;
/
