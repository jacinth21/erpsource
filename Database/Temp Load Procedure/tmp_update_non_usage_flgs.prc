/* Formatted on 2009/06/26 17:21 (Formatter Plus v4.8.0) */

--@"C:\database\Procedures\temp load procedure\tmp_update_non_usage_flgs.prc";

/************************************************************************
 * Purpose: This Prc is a data load prc run once to update the new columns added for the old data
 * Created By Brinal
 ************************************************************************/
-- @"C:\Database\Temp Load Procedure\tmp_update_non_usage_flgs.prc";
-- testing load - 
-- the processed_date_cursor query should not return any data after the load is run.
CREATE OR REPLACE PROCEDURE tmp_update_non_usage_flgs
AS
	v_count 	   NUMBER;
	v_processed_date DATE;

	CURSOR returned_date_cursor
	IS
		SELECT t504a.c504a_loaner_transaction_id loanid, t504a.c504a_return_dt retdt, t504a.c504a_created_date crdt
			 , t504a.c504a_last_updated_date updt
		  FROM t412_inhouse_transactions t412, t504a_loaner_transaction t504a
		 WHERE TRUNC (c412_created_date) > TO_DATE ('04/01/2009', 'mm/dd/yyyy')
		   AND c412_type = 50159
		   AND c412_status_fl < 4
		   AND t412.c504a_loaner_transaction_id = t504a.c504a_loaner_transaction_id
		   AND t504a.c504a_return_dt IS NULL
		   AND t504a.c504a_void_fl IS NULL;

	CURSOR exp_returned_date_cursor
	IS
		SELECT t504a.c504a_loaner_transaction_id loanid, t504a.c504a_return_dt retdt
		  FROM t412_inhouse_transactions t412, t504a_loaner_transaction t504a
		 WHERE TRUNC (c412_created_date) >= TO_DATE ('04/01/2009', 'mm/dd/yyyy')
		   AND c412_type = 50159
		   AND t412.c504a_loaner_transaction_id = t504a.c504a_loaner_transaction_id
		   AND t504a.c504a_void_fl IS NULL;

	CURSOR processed_date_cursor
	IS
		SELECT c504a_loaner_transaction_id loanid, c504a_return_dt retdt
		  FROM t504a_loaner_transaction t504a, t504a_consignment_loaner t504b
		 WHERE TRUNC (c504a_created_date) >= TO_DATE ('04/01/2009', 'mm/dd/yyyy')
		   AND t504a.c504_consignment_id = t504b.c504_consignment_id
		   AND t504a.c504a_return_dt IS NOT NULL
		   AND c504a_processed_date IS NULL
		   AND t504a.c504a_void_fl IS NULL;
BEGIN
-- data correction - to update the return date where its null
	FOR currindex1 IN returned_date_cursor
	LOOP
		UPDATE t504a_loaner_transaction
		   SET c504a_return_dt = NVL (currindex1.updt, currindex1.crdt)
		 WHERE c504a_loaner_transaction_id = currindex1.loanid;
	END LOOP;

	-- exp_ret date
	FOR currindex2 IN exp_returned_date_cursor
	LOOP
		UPDATE t412_inhouse_transactions
		   SET c412_expected_return_date = currindex2.retdt + 30
		 WHERE c504a_loaner_transaction_id = currindex2.loanid;
	END LOOP;

-- to update the processed date
	FOR currindex3 IN processed_date_cursor
	LOOP
		UPDATE t504a_loaner_transaction
		   SET c504a_processed_date = currindex3.retdt
		 WHERE c504a_loaner_transaction_id = currindex3.loanid;
	END LOOP;

	-- to close the old trans
	-- Check what sql
	UPDATE t525_product_request
	   SET c525_charge_status = 'N'
		 , c525_charge_date = TO_DATE ('03/31/2009', 'mm/dd/yyyy')
	 WHERE TRUNC (c525_created_date) < TO_DATE ('04/01/2009', 'mm/dd/yyyy');
END tmp_update_non_usage_flgs;
/
