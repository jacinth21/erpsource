CREATE OR REPLACE FUNCTION qb_get_item
(
 p_qb_item_listid  gm_qb_item.qb_item_listid%TYPE
)
RETURN VARCHAR2
IS
--
v_c205_part_number_id gm_qb_item.c205_part_number_id%TYPE;
--
BEGIN
 --
 SELECT c205_part_number_id
 INTO  v_c205_part_number_id
 FROM gm_qb_item
 WHERE qb_item_listid =  p_qb_item_listid;
 --
 RETURN v_c205_part_number_id;
EXCEPTION
 WHEN NO_DATA_FOUND
    THEN
      RETURN NULL;
END qb_get_item;
/
