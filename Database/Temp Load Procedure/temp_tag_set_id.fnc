create or replace
FUNCTION temp_get_tag_set_id (
    p_trans_id  IN  VARCHAR2
  , p_part_number  IN  VARCHAR2
)
                RETURN VARCHAR2
IS
                v_set_id                 NUMBER;
                v_count                  NUMBER := 0;
                v_trans_type            VARCHAR2(50);
BEGIN
                SELECT t504.c504_type
                  INTO v_trans_type
                  FROM t504_consignment t504
                WHERE t504.c504_consignment_id = p_trans_id;

                IF v_trans_type IN ('40050', '40021', '4112', '40057', '4110', '4129') THEN
                  BEGIN
                      SELECT COUNT (DISTINCT (t207.c207_set_id))
                      INTO v_count
                      FROM t207_set_master t207, t208_set_details t208
                      WHERE t207.c207_set_id = t208.c207_set_id
                      AND t207.c207_type = 4070
                      AND t208.c205_part_number_id = p_part_number;

                    IF v_count > 1 THEN
                      RETURN NULL;
                    ELSE
                      SELECT DISTINCT t207.c207_set_id
                      INTO v_set_id
                      FROM t207_set_master t207, t208_set_details t208
                      WHERE t207.c207_set_id = t208.c207_set_id
                      AND t207.c207_type = 4070
                      AND t208.c205_part_number_id = p_part_number;
                    END IF;
                                END;
                END IF;

                IF v_trans_type IN ('4127', '4119') THEN
                                v_count               := 0;

                                BEGIN
                    SELECT COUNT (DISTINCT (t207.c207_set_id))
                    INTO v_count
                    FROM t207_set_master t207, t208_set_details t208
                    WHERE t207.c207_set_id = t208.c207_set_id
                    AND t207.c207_type = 4074
                    AND t208.c205_part_number_id = p_part_number;

                IF v_count > 1 THEN
                     RETURN NULL;
                ELSE
                     SELECT DISTINCT t207.c207_set_id
                     INTO v_set_id
                     FROM t207_set_master t207, t208_set_details t208
                     WHERE t207.c207_set_id = t208.c207_set_id
                     AND t207.c207_type = 4074
                     AND t208.c205_part_number_id = p_part_number;
                 END IF;
                 END;
                END IF;

                      RETURN v_set_id;
EXCEPTION
  WHEN NO_DATA_FOUND
   THEN
                  IF p_trans_id like ('TF%') THEN
                   v_count               := 0;
                  BEGIN
                      SELECT COUNT (DISTINCT (t207.c207_set_id))
                      INTO v_count
                      FROM t207_set_master t207, t208_set_details t208
                      WHERE t207.c207_set_id = t208.c207_set_id
                      AND t207.c207_type = 4070
                      AND t208.c205_part_number_id = p_part_number;

                    IF v_count > 1 THEN
                      RETURN NULL;
                    ELSE
                      SELECT DISTINCT t207.c207_set_id
                      INTO v_set_id
                      FROM t207_set_master t207, t208_set_details t208
                      WHERE t207.c207_set_id = t208.c207_set_id
                      AND t207.c207_type = 4070
                      AND t208.c205_part_number_id = p_part_number;
                    END IF;
                    END;
                   END IF;                   
         RETURN v_set_id;
  END temp_get_tag_set_id;

/