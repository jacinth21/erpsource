--@"C:\Database\Temp Load Procedure\gm_sav_loaner_set_attribute.prc"

CREATE OR REPLACE PROCEDURE  gm_sav_loaner_set_attribute
AS
CURSOR c_setlist IS
   	   SELECT DISTINCT (t504.c207_set_id) setid
           FROM t504_consignment t504, t504a_consignment_loaner t504a, T207_SET_MASTER t207
          WHERE t504.c504_consignment_id = t504a.c504_consignment_id
            AND t207.c207_set_id = t504.c207_set_id
            AND T504.C504_VOID_FL IS NULL
            AND t504.c504_type = 4127 -- only product loaners
            AND t504.c504_status_fl = '4'
            AND t504a.C504A_STATUS_FL != '60'
            AND t504a.c504a_void_fl IS NULL
            AND t207.C207_VOID_FL IS NULL
       		ORDER BY t504.c207_set_id;
   	   
	   BEGIN
		   FOR v_loanersetlist IN c_setlist
		   LOOP
		   		gm_pkg_op_set_master.gm_sav_set_attribute(v_loanersetlist.setid, '104740', 'Y', NULL,'303149');
		   END LOOP;
END gm_sav_loaner_set_attribute;
/