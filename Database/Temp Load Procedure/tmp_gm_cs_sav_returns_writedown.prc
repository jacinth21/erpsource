/* Formatted on 2009/03/20 15:52 (Formatter Plus v4.8.0) */

---@"c:\database\Procedures\GM_RETURNS_LOAD.prc"

CREATE OR REPLACE PROCEDURE tmp_gm_cs_sav_ret_writedown (
	p_consignment_id   t504_consignment.c504_consignment_id%TYPE
  , p_comments		   t506_returns.c506_comments%TYPE
)
AS
	v_string	   VARCHAR2 (20);
	v_comments	   t504_consignment.c504_comments%TYPE;
	v_distributor_id t504_consignment.c701_distributor_id%TYPE;
	v_set_id	   t504_consignment.c207_set_id%TYPE;
	v_consignment_id t504_consignment.c504_consignment_id%TYPE;
	v_type		   t506_returns.c506_type%TYPE;

	CURSOR v_get_item_consigment_cur
	IS
		SELECT t505.c205_part_number_id part_number_id, t505.c505_item_qty item_qty, t505.c505_item_price item_price
		  FROM t505_item_consignment t505
		 WHERE t505.c504_consignment_id = p_consignment_id AND TRIM (t505.c504_consignment_id) IS NOT NULL;
BEGIN
	SELECT 'GM-RA-' || s506_return.NEXTVAL
	  INTO v_string
	  FROM DUAL;

	DBMS_OUTPUT.put_line ('RMA_id....' || v_string);

	SELECT c701_distributor_id, c207_set_id
	  INTO v_distributor_id, v_set_id
	  FROM t504_consignment
	 WHERE c504_consignment_id = p_consignment_id;

	SELECT DECODE ((TRIM (v_set_id)), NULL, 3302, 3301)
	  INTO v_type
	  FROM DUAL;

	INSERT INTO t506_returns
				(c506_rma_id, c506_comments, c506_created_date, c506_type, c701_distributor_id, c506_reason
			   , c506_status_fl, c506_created_by, c506_expected_date, c207_set_id, c506_credit_date, c506_return_date
				)
		 VALUES (v_string, p_comments, SYSDATE, v_type, v_distributor_id, 3313
			   , '2', 30301, TRUNC (SYSDATE), v_set_id, TRUNC (SYSDATE), TRUNC (SYSDATE)
				);

	FOR recs IN v_get_item_consigment_cur
	LOOP
		INSERT INTO t507_returns_item
					(c507_returns_item_id, c507_item_qty, c507_item_price, c205_part_number_id, c507_control_number
				   , c506_rma_id, c507_status_fl
					)
			 VALUES (s507_return_item.NEXTVAL, recs.item_qty, recs.item_price, recs.part_number_id, 'NOC#'
				   , v_string, 'C'
					);

		gm_save_ledger_posting (4841   -- Suchi
							  , SYSDATE
							  , recs.part_number_id   -- part#
							  , v_distributor_id   -- DistributorID
							  , v_string   -- RAID
							  , recs.item_qty	--QTY
							  , get_ac_cogs_value (recs.part_number_id, 4904)	--
							  , 30301	-- 30003
							   );
	END LOOP;
END tmp_gm_cs_sav_ret_writedown;
