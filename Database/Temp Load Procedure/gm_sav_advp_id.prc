/* Formatted on 2009/06/26 17:21 (Formatter Plus v4.8.0) */

--@"C:\database\Procedures\temp load procedure\gm_sav_advp_id.prc";

/************************************************************************
 * Purpose: This Prc is to update the AD VP and distributor columns for Order table.
 ************************************************************************/
CREATE OR REPLACE PROCEDURE gm_sav_advp_id
AS
	CURSOR c501_tmp
    IS
        SELECT c501_order_id ordid, c703_sales_rep_id repid FROM T501_Temp t501_tmp;
    i NUMBER;
    j NUMBER;
BEGIN
    i := 0;
    j := 0;
    FOR rec IN c501_tmp
    LOOP
        i := i + 1;
        BEGIN
            UPDATE t501_order
                SET c501_ad_id =
                    (SELECT DISTINCT v700.ad_id
                        FROM v700_territory_mapping_detail v700
                        WHERE v700.rep_id = rec.repid
                    ),
                   c501_vp_id =
                    (SELECT DISTINCT v700.vp_id
                        FROM v700_territory_mapping_detail v700
                        WHERE v700.rep_id = rec.repid
                    ), 
                   C501_distributor_id = (SELECT DISTINCT v700.d_id
                        FROM v700_territory_mapping_detail v700
                        Where V700.Rep_Id = Rec.Repid
                    ),
                c501_last_updated_by = 303503, c501_last_updated_date = sysdate
                WHERE C501_ORDER_id = rec.ordid
                    AND C501_VOID_FL IS NULL
                    AND C501_DELETE_FL IS NULL
                    AND NVL (C901_ORDER_TYPE, - 9999) NOT IN ('2533')
                    AND NVL (c901_order_type, - 9999) NOT IN
                    (SELECT     t906.c906_rule_value
                        FROM t906_rules t906
                        WHERE t906.c906_rule_grp_id = 'ORDTYPE'
                            AND c906_rule_id = 'EXCLUDE'
                    ) ;
            IF (SQL%ROWCOUNT = 0) THEN
                j := j + 1;
            END IF;
        EXCEPTION
        WHEN OTHERS THEN
            j := j + 1;
        END;
    END LOOP;
    dbms_output.put_line ('Total rows : '||i ||'--Not updated rows :'||j) ;
END gm_sav_advp_id;
/
