/* Formatted on 2012/05/03 18:22 (Formatter Plus v4.8.0) */
--	@"C:\database\Procedures\Temp Load Procedure\CustomerServices\GM_SAVE_ORDER_TEMP.prc"
/*This procedure is calling by ETL Job, If there is any changes then please coordinate with DWH team during design phase */
CREATE OR REPLACE PROCEDURE gm_save_order_temp (
	p_orderid	  IN	   t501_order.c501_order_id%TYPE
  , p_accid 	  IN	   t501_order.c704_account_id%TYPE
  , p_omode 	  IN	   t501_order.c501_receive_mode%TYPE
  , p_pname 	  IN	   t501_order.c501_received_from%TYPE
  , p_comments	  IN	   t501_order.c501_comments%TYPE
  , p_po		  IN	   t501_order.c501_customer_po%TYPE
  , p_total 	  IN	   t501_order.c501_total_cost%TYPE
  , p_userid	  IN	   t501_order.c501_created_by%TYPE
  , p_str		  IN	   CLOB
  , p_ordtype	  IN	   t501_order.c901_reason_type%TYPE
  , p_conscode	  IN	   t741_construct.c741_construct_id%TYPE
  , p_parentid	  IN	   t501_order.c501_parent_order_id%TYPE
  , p_shipto	  IN	   t907_shipping_info.c901_ship_to%TYPE
  , p_shiptoid	  IN	   t907_shipping_info.c907_ship_to_id%TYPE
  , p_shipcarr	  IN	   t907_shipping_info.c901_delivery_carrier%TYPE
  , p_shipmode	  IN	   t907_shipping_info.c901_delivery_mode%TYPE
  , p_addressid   IN	   t907_shipping_info.c106_address_id%TYPE
  , p_trackno     IN 	   t907_shipping_info.c907_tracking_number%TYPE
  , p_caseid      IN	   t7100_case_information.c7100_case_id%TYPE
  , p_hard_po	  IN	   t501_order.c501_hard_po_fl%TYPE
  , p_attn		  IN	   t907_shipping_info.c907_override_attn_to%TYPE
  , p_ship_instruc IN	   t907_shipping_info.c907_ship_instruction%TYPE
  , p_ref_ordid	   IN	 t501_order.c501_order_id%TYPE
  , p_ship_cost	  IN		t501_order.c501_ship_cost%TYPE
  , p_order_dt    IN DATE
  , p_ship_dt     IN DATE
--INVOICE PARAM
  , p_payterms	   IN		t503_invoice.c503_terms%TYPE
  , p_invdate	   IN		DATE
  , p_action	   IN		VARCHAR2
  , p_invtype	   IN		t503_invoice.c901_invoice_type%TYPE
  , p_inv_source   IN		t503_invoice.c901_invoice_source%TYPE
  , p_dealer_id    IN		t503_invoice.c101_dealer_id%TYPE
  , p_batch_type   IN		t9600_batch.c901_batch_type%TYPE
  , p_inv_id	   IN		t503_invoice.c503_invoice_id%TYPE
)

AS
	p_shipid VARCHAR2(100);
BEGIN
	gm_save_item_order_TEMP(p_orderid,p_accid,p_omode,p_pname,p_comments,p_po,p_total,p_userid,p_str,p_ordtype,p_conscode,p_parentid,p_shipto,p_shiptoid,p_shipcarr,p_shipmode,p_addressid,p_shipid,p_caseid,p_hard_po,p_attn,p_ship_instruc,p_ref_ordid,p_ship_cost);

	UPDATE t501_order
	   SET c501_status_fl = 3
	     , c501_shipping_date = p_ship_dt
		 , c501_order_date = TRUNC(p_order_dt)
		 , c501_surgery_date = TRUNC(p_order_dt)
	     , c501_order_date_time = p_order_dt
		 , c501_last_updated_by = p_userid
		 , c501_last_updated_date = CURRENT_DATE
	 WHERE c501_order_id = p_orderid;


	UPDATE t907_shipping_info
	   SET c907_status_fl = 40
	   , c907_tracking_number = p_trackno
		 , c907_shipped_dt = p_ship_dt
		 , c907_last_updated_by = p_userid
		 , c907_last_updated_date = CURRENT_DATE
	 WHERE c907_ref_id = p_orderid AND c901_source = '50180' AND c907_void_fl IS NULL
	   AND c907_void_fl IS NULL;

	IF (p_batch_type = '18756')
	THEN
		GM_SAV_SINGLE_DEALER_INV_TEMP(p_po,p_accid,p_dealer_id,p_payterms,p_invdate,p_userid,p_action,p_invtype,p_inv_source,'','',p_inv_id);	
	ELSE
		gm_sav_single_invoice_temp (p_po,p_accid,p_payterms,p_invdate,p_userid,p_action,p_invtype,p_inv_source,'','',p_inv_id);
	END IF;
	
	
	--EXCEPTION WHEN OTHERS THEN
	--  DBMS_OUTPUT.PUT_LINE ('ERROR IN ORDER IS '|| p_orderid ||' INVOICE Id'|| p_inv_id || ':' || sqlerrm);
			   
END gm_save_order_temp;
/
