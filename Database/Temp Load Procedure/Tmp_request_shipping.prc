/* Formatted on 2009/02/18 12:00 (Formatter Plus v4.8.0) */

--@"C:\Database\Temp Load Procedure\Tmp_request_shipping.prc";

CREATE OR REPLACE PROCEDURE tmp_request_shipping
AS
	v_shipdate	   t504_consignment.c504_ship_date%TYPE;
	v_delcar	   t504_consignment.c504_delivery_carrier%TYPE;
	v_delmode	   t504_consignment.c504_delivery_mode%TYPE;
	v_trackno	   t504_consignment.c504_tracking_number%TYPE;
	v_cost		   t504_consignment.c504_ship_cost%TYPE;
	v_addressid    VARCHAR2 (100);

	CURSOR cur_req
	IS
		SELECT t520.c520_request_id reqid
			 , DECODE (CASE
						   WHEN t520.c520_status_fl <= 20
							   THEN '15'
						   WHEN t520.c520_status_fl = 30
							   THEN '30'
					   END
					 , '15', 15
					 , '30', 30
					  ) stflg
			 , t520.c520_void_fl vdflg, t520.c901_ship_to shipto, t520.c520_ship_to_id shiptoid
		  FROM t520_request t520
		 WHERE c520_void_fl IS NULL AND c901_ship_to IS NOT NULL AND c901_ship_to != 0 AND t520.c520_status_fl <> 40;
BEGIN
	FOR curindex IN cur_req
	LOOP
		IF curindex.stflg = 30
		THEN
			SELECT DECODE (t504.c504_delivery_carrier, 0, 5040, t504.c504_delivery_carrier)
				 , DECODE (t504.c504_delivery_mode, 0, 5031, t504.c504_delivery_mode)
				 , DECODE (t504.c504_ship_to, 4121, temp_get_address_id (t504.c504_ship_to_id), NULL) addressid
			  INTO v_delcar
				 , v_delmode
				 , v_addressid
			  FROM t504_consignment t504
			 WHERE t504.c520_request_id = curindex.reqid;
		ELSE
			v_delcar	:= 5040;
			v_delmode	:= 5031;
			v_addressid := '';
		END IF;

		INSERT INTO t907_shipping_info
					(c907_shipping_id, c907_ref_id, c901_source, c907_status_fl, c907_void_fl, c901_ship_to
				   , c907_ship_to_id, c106_address_id, c901_delivery_carrier, c901_delivery_mode, c907_created_by
				   , c907_created_date, c907_active_fl
					)
			 VALUES (s907_ship_id.NEXTVAL, curindex.reqid, 50184, curindex.stflg, curindex.vdflg, curindex.shipto
				   , curindex.shiptoid, v_addressid, v_delcar, v_delmode, 30301
				   , SYSDATE, 'N'
					);
	END LOOP;

	COMMIT;
END tmp_request_shipping;
