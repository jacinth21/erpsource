create or replace PROCEDURE 
STL_DM_Part_Price_Migration(
  p_company_id     IN	   t1900_company.c1900_company_id%TYPE
, p_plant_id 	   IN	   T5040_Plant_Master.C5040_PLANT_ID%TYPE
)

AS
  v_user t101_user.c101_user_id%TYPE		:='303043';
  V_PORTAL_COM_DATE_FMT VARCHAR2 (20) ;
  v_err_count number:=0;
  v_success_count number:=0;
  v_price_script CLOB;
  v_company_dtfrmt T901_Code_Lookup.c901_code_id%TYPE;
  v_error_msg    Varchar2(1000);

  CURSOR CUR_part_price IS 
    Select Sno,Part,List_Price,Trip_Price,Company_Id,User_Id,Comments  FROM STL_DM_PART_PRICE pp, t205_part_number t205
    WHERE pp.part = t205.c205_part_number_id and
    pp.Migrated Is Null
    AND pp.Err_Dtls IS NULL
    ORDER BY TO_NUMBER(pp.SNO); 

 BEGIN
  
	  BEGIN
		SELECT C901_DATE_FORMAT INTO v_company_dtfrmt 
		  FROM T1900_COMPANY 
		WHERE C1900_COMPANY_ID=p_company_id;
		EXCEPTION WHEN no_data_found THEN
			raise_application_error ('-20999', ' Company id is not available ');
	END;
   
      FOR  price IN CUR_part_price
      LOOP
        BEGIN
	        
	       IF TRIM(price.List_Price) IS NULL THEN
	       	v_error_msg:= ' Part Pricing is Null for  '||price.sno;
	       END IF;
	       
	      IF v_error_msg IS NULL THEN
		       BEGIN
						GM_PKG_PD_PART_PRICING.gm_sav_part_pricing(price.Part,price.Trip_Price,price.List_Price,price.List_Price,price.List_Price
						,p_company_id,price.User_Id,price.Comments);
			         
						--Update as Migrated
						UPDATE STL_DM_PART_PRICE SET MIGRATED='Y' WHERE sno= price.sno;
			        
			          v_success_count:=v_success_count+1;
			          COMMIT;
			        EXCEPTION 
			        WHEN NO_DATA_FOUND THEN
			          DBMS_OUTPUT.PUT_LINE('*********No Data Found. Sno:' || price.sno);
			            v_error_msg:= 'Part Pricing insert NO data Found';
			         	v_err_count:=v_err_count+1;
			         	ROLLBACK;
			         	UPDATE STL_DM_PART_PRICE SET Err_Dtls=v_error_msg WHERE sno=price.sno;
			         	COMMIT;
			        WHEN OTHERS THEN
			        	v_error_msg := sqlerrm || chr(10) ||dbms_utility.format_error_backtrace;
			            DBMS_OUTPUT.PUT_LINE('********* Sno:' || price.sno || ' Error:' || v_error_msg);
			            v_err_count:=v_err_count+1;
			            ROLLBACK;
			         	UPDATE STL_DM_PART_PRICE SET Err_Dtls=v_error_msg WHERE sno=price.sno;
			         	COMMIT;
			   END;
        ELSE
        	UPDATE STL_DM_PART_PRICE SET Err_Dtls=v_error_msg WHERE sno=price.sno;
         	COMMIT;
         	v_error_msg:=NULL;
        END IF;
       END;
		v_error_msg:=NULL;
    END LOOP;
    DBMS_OUTPUT.PUT_LINE('v_err_count:'|| v_err_count);
    DBMS_OUTPUT.PUT_LINE('v_success_count:'||v_success_count);
  END STL_DM_Part_Price_Migration;
/