create or replace PROCEDURE 
STL_DM_ACCOUNT_PRICE_MIGRATION(
  p_company_id     IN	   t1900_company.c1900_company_id%TYPE
, p_plant_id 	   IN	   T5040_Plant_Master.C5040_PLANT_ID%TYPE
)
AS
v_party_id VARCHAR2(100);
v_project_id VARCHAR2(100);
v_approved_by VARCHAR2(100):='303043';
v_err_count NUMBER:=0;
v_success_count NUMBER:=0;
v_err_party_miss_string CLOB;
v_company_dtfrmt T901_Code_Lookup.c901_code_id%TYPE;
v_error_msg    Varchar2(1000);

CURSOR cur_ap IS
  SELECT sno,part,ACCOUNT,account_price,migrated,script, t704.c704_account_id portal_account_id, 
    t704.c101_party_id acc_party_id, t202.c202_project_id project_id  
    FROM STL_DM_ACCOUNT_PRICE ac, t205_part_number t205, t704_account t704, t202_project t202
    WHERE ac.part = t205.c205_part_number_id 
    AND t205.c202_project_id = t202.c202_project_id
    AND t704.c704_ext_ref_id = ac.ACCOUNT
    --AND to_number(sno)>1020 AND to_number(sno)<1030   --ACCOUNT IN ('029790') and --Sno In ('3890') And
    AND migrated IS NULL
    AND Err_Dtls IS NULL
    AND t704.c1900_company_id = p_company_id
    AND t202.c1900_company_id = p_company_id
    AND to_number(account_price)>0
  ORDER BY to_number(sno) ;

BEGIN

  
   BEGIN
		SELECT C901_DATE_FORMAT INTO v_company_dtfrmt 
		  FROM T1900_COMPANY 
		WHERE C1900_COMPANY_ID=p_company_id;
		EXCEPTION WHEN no_data_found THEN
			raise_application_error ('-20999', ' Company id is not available ');
	END;

  FOR  acc IN  cur_ap
    loop
    
      v_party_id := acc.acc_party_id;
      
--      dbms_output.put_line('acc.Source_Account_Id=' || acc.ACCOUNT || ', Portal Account Id=' || acc.portal_account_id || ', p_party_id='|| v_party_id);
      
      v_project_id := acc.project_id;
      
      BEGIN  
        UPDATE t705_account_pricing
        SET c705_unit_price = acc.account_price
        ,c705_last_updated_by = v_approved_by
        ,c705_last_updated_date = CURRENT_DATE
        WHERE c101_party_id = v_party_id
        AND c205_part_number_id = acc.part
        AND c705_void_fl IS  NULL;
        
        IF(SQL%rowcount = 0) THEN
       
          INSERT INTO t705_account_pricing
          (c705_account_pricing_id 
          ,c101_party_id
          ,c205_part_number_id
          ,c705_unit_price
           ,c202_project_id
           ,c705_created_date
           ,c705_created_by
           ,c1900_company_id
          )
          VALUES (s704_price_account.nextval, v_party_id, acc.part, acc.account_price
           ,  v_project_id, CURRENT_DATE, v_approved_by
           , p_company_id
          );

--          dbms_output.put_line(' Account Pricing inserted for part=' || acc.part || ', Acc Party='  || v_party_id);

        END IF;
        
        -- Mark successful saved records as Migrated
        UPDATE STL_DM_ACCOUNT_PRICE SET migrated='Y' WHERE sno=acc.sno;
        COMMIT;
        v_success_count:=v_success_count+1;
      exception WHEN no_data_found THEN
        v_err_count:=v_err_count+1;
        dbms_output.put_line('********* Account Pricing insert NO data Found');
        v_error_msg:= 'Account Pricing insert NO data Found';
        ROLLBACK;
         UPDATE STL_DM_ACCOUNT_PRICE SET Err_Dtls=v_error_msg WHERE sno=acc.sno;
         COMMIT;
      WHEN others THEN
      	v_error_msg := sqlerrm  || chr(10) ||dbms_utility.format_error_backtrace;
         v_err_count:=v_err_count+1;
         ROLLBACK;
         UPDATE STL_DM_ACCOUNT_PRICE SET Err_Dtls=v_error_msg WHERE sno=acc.sno;
         COMMIT;
      END;

  END loop;
  
  dbms_output.put_line('v_err_count:'||v_err_count);
  dbms_output.put_line('v_success_count:'||v_success_count);
  dbms_output.put_line('v_err_party_miss_string:'||v_err_party_miss_string);
  
  
END STL_DM_ACCOUNT_PRICE_MIGRATION;
/