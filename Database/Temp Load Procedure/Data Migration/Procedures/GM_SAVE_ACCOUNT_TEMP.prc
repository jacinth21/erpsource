-- @"C:\database\Procedures\Sales\GM_SAVE_ACCOUNT_temp.prc";
CREATE OR REPLACE
PROCEDURE GM_SAVE_ACCOUNT_TEMP (
        p_rep_acc_input_str    IN VARCHAR2,
        p_parent_acc_input_str IN VARCHAR2,
        p_action               IN VARCHAR2,
        p_userid               IN t704_account.c704_created_by%TYPE,
        p_out_rep_acc_id OUT VARCHAR2,
        p_out_parent_party_id OUT VARCHAR2)
AS
    v_acc_id                                      NUMBER;
    v_string                                      VARCHAR2 (10) ;
    v_old_price_cat                               NUMBER;
    v_count                                       NUMBER;
    v_acct_grp_id                                 VARCHAR2 (20) ;
    v_price_lvl                                   NUMBER;
    v_old_price_lvl                               NUMBER;
    v_InputStr                                    VARCHAR2 (2000) ;
    v_distid t701_distributor.C701_DISTRIBUTOR_ID %TYPE ;
    v_old_party_id t101_party.c101_party_id%TYPE;
    v_portal_comp_id t704_account.C1900_COMPANY_ID%TYPE;
    --
    v_rep_acc_id t704_account.c704_account_id%TYPE;
    v_rep_acc_name t704_account.c704_account_nm%TYPE;
    v_rep_acc_name_en t704_account.c704_account_nm_en%TYPE;
    v_rep_id t704_account.c703_sales_rep_id%TYPE;
    v_rep_short_nm t704_account.c704_account_sh_name%TYPE;
    v_inception_dt t704_account.C704_LAST_UPDATED_DATE%TYPE ;
    v_rep_acc_active_fl t704_account.c704_active_fl%TYPE;
    v_comp_id t704_account.c901_company_id%TYPE;
    --
    v_parent_party_id t704_account.c101_party_id%TYPE;
    v_parent_party_name t101_party.c101_party_nm%TYPE;
    v_account_type t704_account.c901_account_type%TYPE;
    v_contact_per t704_account.c704_contact_person%TYPE;
    v_phone t704_account.C704_PHONE%TYPE;
    v_fax t704_account.C704_FAX%TYPE;
    v_acc_info t704_account.C704_COMMENTS%TYPE;
    --
    v_bill_name t704_account.c704_bill_name%TYPE;
    v_bill_add1 t704_account.c704_bill_add1%TYPE;
    v_bill_add2 t704_account.c704_bill_add2%TYPE;
    v_bill_city t704_account.c704_bill_city%TYPE;
    v_bill_state t704_account.c704_bill_state%TYPE;
    v_bill_country t704_account.c704_bill_country%TYPE;
    v_bill_zipcode t704_account.c704_bill_zip_code%TYPE;
    --
    v_ship_name t704_account.c704_bill_name%TYPE;
    v_ship_add1 t704_account.c704_bill_add1%TYPE;
    v_ship_add2 t704_account.c704_bill_add2%TYPE;
    v_ship_city t704_account.c704_bill_city%TYPE;
    v_ship_state t704_account.c704_bill_state%TYPE;
    v_ship_country t704_account.c704_bill_country%TYPE;
    v_ship_zipcode t704_account.c704_bill_zip_code%TYPE;
    --
    v_payment_term t704_account.c704_payment_terms%TYPE;
    v_credit_rat t704_account.c704_credit_rating%TYPE;
    v_dealer_id t704_account.c101_dealer_id%TYPE;
    --
    v_terms VARCHAR2 (20) ;
    v_old_comp_id t704_account.c901_company_id%TYPE;
    v_old_acc_name        VARCHAR2 (4000) ;
    v_out_parent_party_id VARCHAR2 (20) ;
    v_rule_value t906_rules.c906_rule_value%TYPE;
    v_currency t704_account.C901_CURRENCY%TYPE;

    --
    v_cnt NUMBER;
    --
    CURSOR rep_account_cur
    IS
         SELECT v_rep_acc_id id FROM dual
      UNION
     SELECT c704_account_id id
       FROM t704_account
      WHERE c101_party_id = v_out_parent_party_id
        AND c704_void_fl IS NULL;
BEGIN
    --
    v_distid         := GET_DISTRIBUTOR_ID (v_rep_id) ;
    IF v_account_type = 70114 THEN
        IF v_distid   = ' ' THEN
            raise_application_error ('-20622', '') ; -- If we submit with dummy Rep as
        END IF;
        v_terms     := get_ict_payment_terms (v_distid) ;
        IF (v_terms != TO_CHAR (v_payment_term)) THEN
            raise_application_error ('-20616', '') ; -- Payment Term Should be same as in Distributor Setup
            -- Screen.
        END IF;
    END IF;
    --
     SELECT get_compid_frm_cntx () INTO v_portal_comp_id FROM DUAL;
    -- break the rep account string
    gm_pkg_sm_acct_trans_temp.gm_extract_rep_ac_param (p_rep_acc_input_str, v_rep_acc_id, v_rep_acc_name,v_rep_acc_name_en, v_comp_id,
    v_rep_id, v_rep_short_nm, v_rep_acc_active_fl, v_inception_dt,v_currency,v_dealer_id) ;
    -- break the parent acccount string
    gm_pkg_sm_acct_trans_temp.gm_extract_parent_ac_param (p_parent_acc_input_str, v_parent_party_id, v_parent_party_name,
    v_account_type, v_contact_per, v_phone, v_fax, v_acc_info, v_bill_name, v_bill_add1, v_bill_add2, v_bill_city,
    v_bill_state, v_bill_country, v_bill_zipcode, v_ship_name, v_ship_add1, v_ship_add2, v_ship_city, v_ship_state,
    v_ship_country, v_ship_zipcode, v_payment_term, v_credit_rat) ;
    --
     BEGIN
         SELECT NVL (c101_party_id, '-999')
           INTO v_old_party_id
           FROM t704_account
          WHERE c704_account_id = v_rep_acc_id;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_old_party_id := '-999';
     END;
     
        
      IF p_action  = 'Add' THEN
     
        SELECT s704_account.NEXTVAL INTO v_rep_acc_id FROM DUAL;
     
        
        
        -- to create the new account
        gm_pkg_sm_acct_trans_temp.gm_sav_acct_info (v_rep_acc_id, v_rep_acc_name,v_rep_acc_name_en, v_rep_id, v_rep_short_nm, v_inception_dt,
        v_rep_acc_active_fl, v_comp_id, v_parent_party_id, v_parent_party_name, v_account_type, v_contact_per, v_phone,
        v_fax, v_acc_info, v_bill_name, v_bill_add1, v_bill_add2, v_bill_city, v_bill_state, v_bill_country,
        v_bill_zipcode, v_ship_name, v_ship_add1, v_ship_add2, v_ship_city, v_ship_state, v_ship_country,
        v_ship_zipcode, v_payment_term, v_credit_rat, v_portal_comp_id,v_currency, p_userid,v_dealer_id) ;
    ELSE
        -- to update the rep account information
        gm_pkg_sm_acct_trans_temp.gm_upd_rep_acct_info (v_rep_acc_id, v_rep_acc_name,v_rep_acc_name_en, v_rep_id, v_rep_short_nm,
        v_inception_dt, v_rep_acc_active_fl, v_comp_id,v_currency, p_userid,v_dealer_id) ;
    END IF;
    --
    v_out_parent_party_id := v_parent_party_id;
    --
   
   
    -- to update/create the new party (parent account)
    gm_pkg_cm_party.gm_cm_sav_party_id (v_parent_party_id, v_parent_party_name, NULL, NULL, v_parent_party_name,
    4000877, 'Y', p_userid, v_out_parent_party_id) ;
    --
     
    FOR rep_acc IN rep_account_cur
    LOOP
        gm_pkg_sm_acct_trans_temp.gm_upd_acct_info (rep_acc.id, v_out_parent_party_id, v_parent_party_name, v_account_type,
        v_contact_per, v_phone, v_fax, v_acc_info, v_bill_name, v_bill_add1, v_bill_add2, v_bill_city, v_bill_state,
        v_bill_country, v_bill_zipcode, v_ship_name, v_ship_add1, v_ship_add2, v_ship_city, v_ship_state,
        v_ship_country, v_ship_zipcode, v_payment_term, v_credit_rat, v_portal_comp_id, p_userid) ;
        --
    END LOOP;
    
    -- when parent changed - to sync the account attribute value
    IF v_old_party_id <> v_out_parent_party_id THEN
     
        -- Invoice parameter sync
        v_rule_value := get_rule_value ('INVOICE_PARAM', 'ACC_ATTRIBUTE_TYPE') ;
        gm_pkg_sm_acct_trans_temp.gm_sav_sync_parent_acct_attr (v_rep_acc_id, v_rule_value, p_userid) ;
         
        -- credit hold parameter sync
        v_rule_value := get_rule_value ('CREDIT_HOLD_PARAM', 'ACC_ATTRIBUTE_TYPE') ;
        gm_pkg_sm_acct_trans_temp.gm_sav_sync_parent_acct_attr (v_rep_acc_id, v_rule_value, p_userid) ;
        
        -- Pricing parameter sync
        v_rule_value := get_rule_value ('PRICING_SETUP_PARAM', 'ACC_ATTRIBUTE_TYPE') ;
        gm_pkg_sm_acct_trans_temp.gm_sav_sync_parent_acct_attr (v_rep_acc_id, v_rule_value, p_userid) ;
        
        -- GPO ID sync
        gm_pkg_sm_acct_trans_temp.gm_sav_sync_parent_gpo_acct (v_rep_acc_id, p_userid) ;
    END IF;
  

    -- refresh the view
    --dbms_mview.REFRESH ('v700_territory_mapping_detail') ;
    --
    p_out_rep_acc_id      := v_rep_acc_id;
    p_out_parent_party_id := v_out_parent_party_id;
END GM_SAVE_ACCOUNT_TEMP;
/