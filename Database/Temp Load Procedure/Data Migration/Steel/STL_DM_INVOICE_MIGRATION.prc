/*
************************************************
* The purpose of this procedure is to load the invoice in Spine IT
* Author: Mahavishnu
************************************************* 
*/

CREATE OR REPLACE
PROCEDURE STL_DM_INVOICE_MIGRATION(
  p_company_id     IN	   t1900_company.c1900_company_id%TYPE
, p_plant_id 	   IN	   T5040_Plant_Master.C5040_PLANT_ID%TYPE
)
AS
 v_user t101_user.c101_user_id%TYPE		:='303043';
 
  v_err_count NUMBER:=0;
  v_success_count NUMBER:=0; 
  
  v_order_id t501_order.c501_order_id%TYPE;
  v_acc_id t704_account.c704_account_id%TYPE;
  v_inputstr CLOB;
  v_payment_term t704_account.c704_payment_terms%TYPE;
  v_order_date VARCHAR2(20);
  v_ship_date VARCHAR2(20);
  v_source_ship_to_id VARCHAR2(20);
  v_source_bill_to_id VARCHAR2(20);
  v_tax NUMBER;
  v_cust_po varchar2 (30);
  v_count number:=0;
  v_company_dtfrmt T901_Code_Lookup.C901_CODE_NM%TYPE;
  v_company_timezone  T901_Code_Lookup.c901_code_id%TYPE;
  v_error_msg    Varchar2(1000);
  v_inv_dt DATE;
  
 CURSOR po_cur IS
    SELECT  distinct C501_CUSTOMER_PO cust_po,C503_INVOICE_DATE invdt,SRC_INVOICE_ID src_inv_id,INVOICE_ID invid,PAYMENT_TERM payterm 
        FROM STL_DM_INVOICE 
        where MIGRATED IS NULL
        AND ERROR_DETAILS IS NULL;

BEGIN

	BEGIN
		SELECT C901_TIMEZONE,GET_CODE_NAME(C901_DATE_FORMAT) INTO v_company_timezone,v_company_dtfrmt 
		  FROM T1900_COMPANY 
		WHERE C1900_COMPANY_ID=p_company_id;
		EXCEPTION WHEN no_data_found THEN
			raise_application_error ('-20999', ' Company id is not available ');
	END;
   
	 -- Set company context
  	gm_pkg_cor_client_context.gm_sav_client_context(p_company_id,get_code_name(v_company_timezone),v_company_dtfrmt,p_plant_id);
	
		  FOR  custpo IN  po_cur
		    loop
		    
		    
		    --Validation block
		    -- Fetching Account id from t501_order table by customer PO id
		    BEGIN
			    
			    SELECT t501.C704_ACCOUNT_ID,t501.c501_customer_po
			    INTO v_acc_id,v_cust_po
			       FROM t501_order t501, t704_account t704
			    WHERE t704.c704_account_id=t501.c704_account_id
			       AND t501.c1900_company_id=p_company_id 
			       AND t501.c501_void_fl is null
			       AND t501.c501_customer_po = custpo.cust_po
			       --AND t501.c503_invoice_id is null
			       AND rownum=1;
			    EXCEPTION WHEN no_data_found THEN
			    	v_acc_id:=null;
			    	v_error_msg:= ' Source data not found || ';
			    WHEN OTHERS THEN
			    	 v_error_msg:= v_error_msg || SQLERRM||chr(10)||dbms_utility.format_error_backtrace || ' || ';
			END;
			
			
			-- Validating payment trem is valid
			 		BEGIN 	
					      SELECT C901_CODE_ID INTO v_payment_term 
					        FROM T901_CODE_LOOKUP
					      WHERE TRIM(Upper(C901_CODE_NM))=TRIM(Upper(custpo.payterm)) 
					        AND C901_CODE_GRP='PAYM'
					      AND C901_VOID_FL IS NULL;
					      EXCEPTION WHEN no_data_found THEN
			            	v_error_msg:= v_error_msg || ' payment term is not available || ';
			              WHEN OTHERS THEN
			    	 		v_error_msg:= v_error_msg || SQLERRM||chr(10)||dbms_utility.format_error_backtrace || ' || ';
		       		 END;
			
			
			 BEGIN
			     SELECT to_date(custpo.invdt ,  v_company_dtfrmt ) INTO v_inv_dt 
				   FROM DUAL;
				 EXCEPTION WHEN OTHERS THEN
				  	v_inv_dt:=null;
				    v_error_msg:= v_error_msg || ' Invoice date was null || ';
			 END;
		     
			-- validation block ended

			IF v_error_msg IS  NULL THEN
				    BEGIN
					    
						 gm_sav_single_invoice_temp(v_cust_po,v_acc_id,v_payment_term,v_inv_dt,v_user,'ADD','50200','50255','','',custpo.invid);
						 dbms_output.put_line(' Invoice generated '||custpo.invid);
						
						UPDATE t501_order 
				        SET c503_invoice_id=custpo.invid
				        WHERE c501_customer_po=v_cust_po
				        AND C501_VOID_FL IS NULL 
				        AND C1900_COMPANY_ID = p_company_id;
				        
				        --update tax rate
				        gm_pkg_ac_invoice_tax.gm_sav_taxrate_by_invoice(custpo.invid,v_cust_po,v_user);
						 
				        UPDATE T503_INVOICE  SET C503_EXT_REF_ID=custpo.src_inv_id,C503_COMMENTS='Invoice Migrated from Syteline(Source System)' WHERE C503_INVOICE_ID=custpo.invid;
				        
						UPDATE STL_DM_INVOICE SET MIGRATED='Y',Migrated_Date=SYSDATE WHERE C501_CUSTOMER_PO=v_cust_po;
				
						COMMIT;
						 v_success_count:=v_success_count+1;
						
					 EXCEPTION WHEN others THEN
						v_error_msg:=SQLERRM||chr(10)||dbms_utility.format_error_backtrace;
						 dbms_output.put_line('**************Invoice error -CUST PO :'|| custpo.cust_po  || v_error_msg);
				      	ROLLBACK;
				        UPDATE STL_DM_INVOICE SET ERROR_DETAILS=v_error_msg,MIGRATED_DATE=SYSDATE WHERE C501_CUSTOMER_PO=custpo.cust_po;
				        COMMIT;
				        v_error_msg:=NULL;
				        v_err_count:=v_err_count+1;
				      END;
			ELSE
				UPDATE STL_DM_INVOICE SET ERROR_DETAILS=v_error_msg WHERE C501_CUSTOMER_PO=custpo.cust_po;
	      		 DBMS_OUTPUT.PUT_LINE('*********Invoice - Cust po:' || custpo.cust_po || ' Error:' || v_error_msg);
	      		 COMMIT;
	      		 v_err_count:=v_err_count+1;
	      		 v_error_msg:=NULL;
				      
			END IF; 
				    
		      v_error_msg:=NULL;
		    END loop;

	  dbms_output.put_line('v_err_count:'||v_err_count);
	  dbms_output.put_line('v_success_count:'||v_success_count);

End STL_DM_INVOICE_MIGRATION;
/
