/*
************************************************
* The purpose of this procedure is to load the sales data
* Author:Mahavishnu
************************************************* 
*/

CREATE OR REPLACE
PROCEDURE STL_DM_SALES_MIGRATION(
  p_company_id     IN	   t1900_company.c1900_company_id%TYPE
, p_plant_id 	   IN	   T5040_Plant_Master.C5040_PLANT_ID%TYPE
)
AS
 v_user t101_user.c101_user_id%TYPE		:='303043';
 
  v_err_count NUMBER:=0;
  v_success_count NUMBER:=0; 
  p_salesrepid VARCHAR2(100);
  v_rep_name T703_Sales_Rep.C703_SALES_REP_NAME%TYPE;
  v_rep_id T703_Sales_Rep.C703_EXT_REF_ID%TYPE;
  v_order_id t501_order.c501_order_id%TYPE;
  v_src_ord_id t501_order.c501_order_id%TYPE;
  v_acc_id t704_account.c704_account_id%TYPE;
  v_inputstr CLOB;
  v_order_date VARCHAR2(20);
  v_ship_date VARCHAR2(20);
  v_source_ship_to_id VARCHAR2(20);
  v_source_bill_to_id VARCHAR2(20);
  v_tax NUMBER;
  v_ship_type  VARCHAR2(20);
  v_order_type VARCHAR2(20);
  v_order_mode VARCHAR2(20);
  v_cust_po varchar2 (30);
  v_count number:=0;
  v_error_msg    Varchar2(1000);
  v_ship_to   VARCHAR2(20);
  v_ship_to_id VARCHAR2(20);
  v_order_mode_id VARCHAR2(20);
  v_ship_carr  VARCHAR2(20);
  v_ship_mode VARCHAR2(20);
  v_ship_carr_id  VARCHAR2(20);
  v_ship_mode_id VARCHAR2(20);
  v_company_dtfrmt T901_Code_Lookup.C901_CODE_NM%TYPE;
  v_company_timezone  T901_Code_Lookup.c901_code_id%TYPE;
  
 CURSOR cus_sales IS
    SELECT DISTINCT order_id 
    FROM STL_DM_SALES_LOAD WHERE 
    migrated IS NULL
    AND ERROR_DETAILS IS NULL;

BEGIN
	
	
	 BEGIN
		SELECT C901_TIMEZONE,GET_CODE_NAME(C901_DATE_FORMAT) INTO v_company_timezone,v_company_dtfrmt 
		  FROM T1900_COMPANY 
		WHERE C1900_COMPANY_ID=p_company_id;
		EXCEPTION WHEN no_data_found THEN
			raise_application_error ('-20999', ' Company id is not available ');
	END;

 
    -- Set company context
  	gm_pkg_cor_client_context.gm_sav_client_context(p_company_id,get_code_name(v_company_timezone),v_company_dtfrmt,p_plant_id);
  
  
		  FOR  sale IN  cus_sales
		    loop
		      v_order_id:= sale.order_id;
		      
		      
		       BEGIN
		          SELECT  order_date, ship_date , shiptoid, vat, billtoid,customer_po,REPNAME,SHIPTOTYPE,ORDER_MODE,Order_Type,SHIP_CARRIER,SHIP_MODE,SOURCE_ORDER_ID,REPID
		          INTO  v_order_date,v_ship_date,v_source_ship_to_id,v_tax,v_source_bill_to_id,v_cust_po,
		          v_rep_name,v_ship_type,v_order_mode,v_order_type,v_ship_carr,v_ship_mode,v_src_ord_id,v_rep_id
		            FROM STL_DM_SALES_LOAD
		          WHERE order_id =sale.order_id
		            AND ROWNUM=1;
		        EXCEPTION WHEN no_data_found THEN
		            v_error_msg:= ' Source order is not found || ';
		        WHEN OTHERS THEN
			      v_error_msg:= v_error_msg || SQLERRM||chr(10)||dbms_utility.format_error_backtrace || ' || ';
		        END;
		        
		     --Validation Block    
			 -- Validation Account is present in spineit
		        BEGIN
			        SELECT C704_ACCOUNT_ID INTO v_acc_id FROM T704_ACCOUNT WHERE c704_ext_ref_id=v_source_bill_to_id
			        AND C1900_COMPANY_ID=p_company_id;
			     EXCEPTION WHEN no_data_found THEN
		            v_error_msg:=v_error_msg || ' Account id is not available || ';
		         WHEN OTHERS THEN
			      v_error_msg:= v_error_msg || SQLERRM||chr(10)||dbms_utility.format_error_backtrace || ' || ';
		         END;
		         
		         -- validating Sales rep is present in spineit
		         BEGIN
			         SELECT C703_SALES_REP_ID INTO p_salesrepid 
			         FROM T703_SALES_REP
		         	 WHERE C703_ext_ref_id=v_rep_id
		         	  AND c1900_company_id=p_company_id;
		         	EXCEPTION  WHEN no_data_found THEN
		         		BEGIN
					       	 SELECT C703_SALES_REP_ID INTO p_salesrepid 
				              FROM T703_SALES_REP
				         	 WHERE TRIM(Upper(C703_SALES_REP_NAME_EN))=TRIM(Upper(v_rep_name))
				            AND c1900_company_id=p_company_id;
				       		EXCEPTION WHEN no_data_found THEN
				            v_error_msg:=v_error_msg || ' Rep id is not available || ';
				            WHEN OTHERS THEN
				      		v_error_msg:= v_error_msg || SQLERRM||chr(10)||dbms_utility.format_error_backtrace || ' || ';
			         	END;
		           	WHEN OTHERS THEN
			      	v_error_msg:= v_error_msg || SQLERRM||chr(10)||dbms_utility.format_error_backtrace || ' || ';
		         END;
		         
		         
		        IF v_ship_type ='Account' THEN
			        v_ship_to_id:=v_acc_id;
			       	v_ship_to:= 4122;-- Ship to type as Account
		       ELSIF v_ship_type ='Rep' THEN
		       		 v_ship_to_id:=p_salesrepid;
			       	 v_ship_to:= 4121;-- Ship to type as Sales rep		
		         
			   END IF;
		    
			-- Validation block ended
			
	
     IF v_error_msg IS NULL THEN
			   
		        
		      -- Get the input string
		      BEGIN
		         SELECT  RTRIM ((XMLAGG (XMLELEMENT (e, input_str || '|')) .EXTRACT ('//text()')).getclobval(), '|')
				   INTO v_inputstr
				   FROM
					(
						 SELECT order_id, input_str FROM globus_app.STL_DM_SALES_LOAD
						 WHERE order_id = v_order_id
					)
				GROUP BY order_id ; 					

 
		        
		        v_inputstr:= v_inputstr || '|';
		
		      
		        dbms_output.put_line('v_inputstr=' || v_inputstr);
		        
		        
		        BEGIN
			         SELECT C901_CODE_ID INTO v_order_mode_id 
				       FROM T901_CODE_LOOKUP
				     WHERE TRIM(Upper(C901_CODE_NM))=TRIM(Upper(v_order_mode)) 
				       AND C901_CODE_GRP='ORDMO'
				       AND C901_VOID_FL IS NULL;
			    EXCEPTION WHEN no_data_found THEN
		           v_order_mode_id:=5017;-- Assigning default order mode as Phone
		           WHEN OTHERS THEN
			     	v_order_mode_id:=5017;-- Assigning default order mode as Phone
		         END;
			     
		        BEGIN
			         SELECT C901_CODE_ID INTO v_ship_carr_id 
				       FROM T901_CODE_LOOKUP
				     WHERE TRIM(Upper(C901_CODE_NM))=TRIM(Upper(v_ship_carr)) 
				       AND C901_CODE_GRP='DCAR'
				       AND C901_VOID_FL IS NULL;
			    EXCEPTION WHEN no_data_found THEN
		           v_ship_carr_id:=5001;-- Assigning default  as FEDEX
		        WHEN OTHERS THEN
		            v_ship_carr_id:=5001;-- Assigning default  as FEDEX
		         END;
		         
		          BEGIN
			         SELECT C901_CODE_ID INTO v_ship_mode_id 
				       FROM T901_CODE_LOOKUP
				     WHERE TRIM(Upper(C901_CODE_NM))=TRIM(Upper(v_ship_mode)) 
				       AND C901_CODE_GRP='DMODE'
				       AND C901_VOID_FL IS NULL;
			    EXCEPTION WHEN no_data_found THEN
		           v_ship_mode_id:=5004;-- Assigning default  as Priority Overnight
		        WHEN OTHERS THEN
		           v_ship_mode_id:=5004;-- Assigning default  as Priority Overnight
		         END;
		        
				-- Cannot insert null value in c501_total_cost column , so assigning value as 100 
		        stl_tmp_save_order(v_order_id,v_acc_id,v_order_mode_id,NULL,NULL,v_cust_po,100,v_user,v_inputstr,NULL,NULL,NULL,
		        v_ship_to,v_ship_to_id,v_ship_carr_id,v_ship_mode_id,NULL,'TRACK',NULL,NULL,NULL,NULL,NULL,NULL,
		        to_date(v_order_date,v_company_dtfrmt),to_date(v_ship_date,v_company_dtfrmt),p_salesrepid);
		
		        dbms_output.put_line('v_order_id=' || v_order_id || ' created successfully' );
		
		        --update Order data
		        UPDATE t501_order 
		        SET  c501_ext_ref_id= v_src_ord_id, c501_update_inv_fl=1,C501_HOLD_ORDER_PROCESS_FL='Y',C501_HOLD_ORDER_PROCESS_DATE=SYSDATE
		        WHERE c501_order_id = v_order_id;
		        
		        --update VAT rate, rebate rate and rebate flag
		        UPDATE t502_item_order 
		        SET c502_vat_rate=v_tax,C7200_REBATE_RATE=0,C502_REBATE_PROCESS_FL='Y'
		        WHERE c501_order_id = v_order_id;
		        
		        UPDATE STL_DM_SALES_LOAD SET migrated='Y',MIGRATED_DATE=SYSDATE WHERE ORDER_ID=sale.order_id;
		
		        v_success_count:=v_success_count+1;
		        COMMIT;
		      exception WHEN others THEN
		        v_error_msg:=SQLERRM||chr(10)||dbms_utility.format_error_backtrace;
		        DBMS_OUTPUT.PUT_LINE('*********Order id ' || v_order_id || ' Error:' || v_error_msg);
		        ROLLBACK;
		        UPDATE STL_DM_SALES_LOAD SET ERROR_DETAILS=v_error_msg WHERE ORDER_ID=v_order_id;
		        COMMIT;
		        v_err_count:=v_err_count+1;
		        v_error_msg:=null;
		      END;
		 ELSE
			UPDATE STL_DM_SALES_LOAD SET ERROR_DETAILS=v_error_msg WHERE ORDER_ID=v_order_id;
	      		 DBMS_OUTPUT.PUT_LINE('*********order Id -:' || v_order_id || ' Error:' || v_error_msg);
	      		 COMMIT;
	      		 v_err_count:=v_err_count+1;
	      		 v_error_msg:=NULL;
		
		  END IF;     
		      
			v_error_msg:=NULL;
		    END loop;

		
		    
		    
  dbms_output.put_line('v_err_count:'||v_err_count);
  dbms_output.put_line('v_success_count:'||v_success_count);

End STL_DM_SALES_MIGRATION;
/