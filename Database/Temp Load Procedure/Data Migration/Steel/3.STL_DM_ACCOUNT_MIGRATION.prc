/*
************************************************
* The purpose of this procedure is to Load the Account Creation Master Data
* Author: Mahavishnu
************************************************** */

CREATE OR REPLACE
PROCEDURE STL_DM_ACCOUNT_MIGRATION(
  p_company_id     IN	   t1900_company.c1900_company_id%TYPE
, p_plant_id 	   IN	   T5040_Plant_Master.C5040_PLANT_ID%TYPE
)
AS

p_dist_id VARCHAR2(100);
p_salesrepid VARCHAR2(100);
p_source_salesrepid VARCHAR2(100);
p_addressid VARCHAR2(100);
p_party_id  VARCHAR2(100);
p_log_msg VARCHAR2(1000);

v_acc_script CLOB;
v_address_script CLOB;

p_out_rep_acc_id VARCHAR2(100);
p_out_parent_party_id VARCHAR2(100);

v_rep_acc_str CLOB;
v_acct_type VARCHAR2(100);
v_payterm_type VARCHAR2(100);
v_err_count number:=0;
v_success_count number:=0;
v_ship_state    T901_Code_Lookup.c901_code_id%TYPE;
v_ship_con     T901_Code_Lookup.c901_code_id%TYPE;
v_bill_state   T901_Code_Lookup.c901_code_id%TYPE;
v_bill_con     T901_Code_Lookup.c901_code_id%TYPE;
v_user_id NUMBER:=303043;
v_parent_acc_str CLOB;
v_company_dtfrmt T901_Code_Lookup.C901_CODE_NM%TYPE;
v_company_timezone  T901_Code_Lookup.c901_code_id%TYPE;
v_error_msg    Varchar2(1000);

 CURSOR cur_account IS 
    SELECT * FROM STL_DM_ACCOUNT 
    WHERE
    migrated IS NULL
    AND ERROR_DETAILS IS NULL;

BEGIN
  
	
	BEGIN
		SELECT C901_TIMEZONE,GET_CODE_NAME(C901_DATE_FORMAT) INTO v_company_timezone,v_company_dtfrmt 
		  FROM T1900_COMPANY 
		WHERE C1900_COMPANY_ID=p_company_id;
		EXCEPTION WHEN no_data_found THEN
			raise_application_error ('-20999', ' Company id is not available ');
	END;
	
 
   -- Set company context
  	gm_pkg_cor_client_context.gm_sav_client_context(p_company_id,get_code_name(v_company_timezone),v_company_dtfrmt,p_plant_id);
  	
  	
		  FOR  acc IN   cur_account
		    loop
		       
		    --Validation block
		    -- Validation source id is present
		     IF acc.ACCOUNT_SOURCE_ID IS NULL THEN
				       v_error_msg:= ' Source ID is null || ';
			  END IF;
		    
			  --Validating Sales rep is present in spineit
		       BEGIN
		       	 SELECT C703_SALES_REP_ID INTO p_salesrepid 
		            FROM T703_SALES_REP
		          WHERE C703_EXT_REF_ID=acc.rep_id
		            AND c1900_company_id=p_company_id;
		        EXCEPTION 
		      WHEN no_data_found THEN
			      	BEGIN
				      SELECT C703_SALES_REP_ID INTO p_salesrepid 
			            FROM T703_SALES_REP
			          WHERE TRIM(Upper(C703_SALES_REP_NAME_EN))=TRIM(Upper(acc.REP_NAME))
			            AND c1900_company_id=p_company_id;
			           EXCEPTION 
			      		WHEN no_data_found THEN 
			      		v_error_msg:= v_error_msg ||' Sales rep is not available ||  ';
				    END;
			  WHEN OTHERS THEN
			      v_error_msg:= v_error_msg || SQLERRM ||chr(10)||dbms_utility.format_error_backtrace || chr(10) || ' || ';
		      END;
		       
		      --Validating Shipping state is valid
		        BEGIN 
				  SELECT C901_CODE_ID INTO v_ship_state 
				    FROM T901_CODE_LOOKUP
				  WHERE TRIM(Upper(C901_CODE_NM))=TRIM(Upper(acc.SHIP_STATE)) 
				    AND C901_CODE_GRP='STATE'
				    AND C901_VOID_FL IS NULL;
				  EXCEPTION WHEN no_data_found THEN
		           v_error_msg:= v_error_msg ||' Ship state is not available ||  ';
		          WHEN OTHERS THEN
			      v_error_msg:= v_error_msg || SQLERRM ||chr(10)||dbms_utility.format_error_backtrace || chr(10) || ' || ';
		       	END;
		      
		       --Validating Shipping country is valid
		       	BEGIN 
				    SELECT C901_CODE_ID INTO v_ship_con 
				       FROM T901_CODE_LOOKUP
				     WHERE TRIM(Upper(C901_CODE_NM))=TRIM(Upper(acc.SHIP_COUNTRY))
				       AND C901_CODE_GRP='CNTY'
				       AND C901_VOID_FL IS NULL;
				     EXCEPTION WHEN no_data_found THEN
		            	v_error_msg:= v_error_msg ||' Ship country is not available ||  ';
		             WHEN OTHERS THEN
			            v_error_msg:= v_error_msg || SQLERRM ||chr(10)||dbms_utility.format_error_backtrace || chr(10) || ' || ';
		       	END;
		       
		       	 --Validating Billing state is valid
		       	BEGIN 
				   SELECT C901_CODE_ID INTO v_bill_state 
				     FROM T901_CODE_LOOKUP
				   WHERE TRIM(Upper(C901_CODE_NM))=TRIM(Upper(acc.BILL_STATE)) 
				      AND C901_CODE_GRP='STATE'
				      AND C901_VOID_FL IS NULL;
				    EXCEPTION WHEN no_data_found THEN
		            v_error_msg:= v_error_msg ||' Bill  state is not available ||  ';
		            WHEN OTHERS THEN
			         v_error_msg:= v_error_msg || SQLERRM ||chr(10)||dbms_utility.format_error_backtrace || chr(10) || ' || ';
		       	END;
		       	 
		       	--Validating Billing country is valid
		       	BEGIN 
				    SELECT C901_CODE_ID INTO v_bill_con 
				      FROM T901_CODE_LOOKUP
				    WHERE TRIM(Upper(C901_CODE_NM))=TRIM(Upper(acc.BILL_COUNTRY)) 
				      AND C901_CODE_GRP='CNTY'
				       AND C901_VOID_FL IS NULL;
				EXCEPTION WHEN no_data_found THEN
		            	v_error_msg:= v_error_msg ||' Bill  Country is not available ||  ';
		        WHEN OTHERS THEN
			      v_error_msg:= v_error_msg || SQLERRM ||chr(10)||dbms_utility.format_error_backtrace || chr(10) || ' || ';
		        END;
		        
		        --Validating payment term is valid
		        BEGIN 
				    SELECT C901_CODE_ID INTO v_payterm_type 
				      FROM T901_CODE_LOOKUP
				    WHERE TRIM(Upper(C901_CODE_NM))=TRIM(Upper(acc.payment_term)) 
				      AND C901_CODE_GRP='PAYM'
				       AND C901_VOID_FL IS NULL;
				EXCEPTION WHEN no_data_found THEN
		            	v_error_msg:= v_error_msg ||' Payment term is not available ||  ';
		        WHEN OTHERS THEN
			      v_error_msg:= v_error_msg || SQLERRM ||chr(10)||dbms_utility.format_error_backtrace || chr(10) || ' || ';
		        END;
		
		    -- validation block end
		    
		        -- Account procedure call
		    IF v_error_msg IS NULL THEN    
			      BEGIN  
			        -- Check for bill state
			          
				      v_rep_acc_str:= REPLACE(acc.REP_ACC_INPUT_STR, 'v_rep_id', p_salesrepid);
				       dbms_output.put_line('v_rep_acc_str='||v_rep_acc_str);
				      v_parent_acc_str:= REPLACE(acc.PARENT_ACC_INPUT_STR, 'v_acct_type', acc.account_type);
				      
				      v_parent_acc_str:=REPLACE(v_parent_acc_str, 'v_payterm_type', v_payterm_type);
				      dbms_output.put_line('pay term='||v_payterm_type);
				      v_parent_acc_str:= REPLACE(v_parent_acc_str, 'rep_id', p_salesrepid);
				      
				      v_parent_acc_str:=REPLACE(v_parent_acc_str, 'v_bill_state', v_bill_state);
				      
				      v_parent_acc_str:=REPLACE(v_parent_acc_str, 'v_bill_country', v_bill_con);
				      
				      v_parent_acc_str:=REPLACE(v_parent_acc_str, 'v_ship_state', v_ship_state);
				      
				      v_parent_acc_str:=REPLACE(v_parent_acc_str, 'v_ship_country', v_ship_con);
				       dbms_output.put_line('v_parent_acc_str='||v_parent_acc_str);
				           
				      gm_save_account_temp(v_rep_acc_str,v_parent_acc_str,'Add',v_user_id,p_out_rep_acc_id,p_out_parent_party_id);
				      
			        dbms_output.put_line('p_out_rep_acc_id='||p_out_rep_acc_id);
			        --Update source distributor id in ext_ref_id column
			        UPDATE t704_account SET c704_ext_ref_id = acc.account_source_id WHERE c704_account_id =  p_out_rep_acc_id;
			
			        -- Insert Log for sales rep,  1213 contact and address in GPLOG code group
			        gm_update_log(p_out_rep_acc_id, 'Data migrated from source system',1209,303043,p_log_msg);
			        
			        dbms_output.put_line('acc.Account_English_Name=' || trim(acc.Account_English_Name) || ',p_out_rep_acc_id = '|| p_out_rep_acc_id || ', p_log_msg=' || p_log_msg);
			
			        -- Mark successful saved records as Migrated
			        UPDATE STL_DM_ACCOUNT SET migrated='Y' WHERE sno=acc.sno;
			        
			        v_success_count:=v_success_count+1;
			        COMMIT;
			      exception WHEN NO_DATA_FOUND THEN
			        v_error_msg:= '********* Account insert - Sno:'||acc.sno || ' NO data Found';
			        ROLLBACK;
			        dbms_output.put_line('********* Account insert - Sno:'||acc.sno || ' NO data Found');
			        UPDATE STL_DM_ACCOUNT SET ERROR_DETAILS=v_error_msg,MIGRATED_DATE=SYSDATE WHERE SNO=acc.SNO;
			        COMMIT;
			        v_err_count:=v_err_count+1;
			         v_error_msg:=NULL;
			      WHEN others THEN
			      	v_error_msg:=SQLERRM ||chr(10)||dbms_utility.format_error_backtrace || chr(10);
			      	ROLLBACK;
			        dbms_output.put_line('********* Account insert - Sno:'||acc.sno || v_error_msg);
			        UPDATE STL_DM_ACCOUNT SET ERROR_DETAILS=v_error_msg,MIGRATED_DATE=SYSDATE WHERE SNO=acc.SNO;
			        COMMIT;
			        v_err_count:=v_err_count+1;
			         v_error_msg:=NULL;
			      END;
		    ELSE
		    	UPDATE STL_DM_ACCOUNT SET ERROR_DETAILS=v_error_msg WHERE SNO=acc.SNO;
	      		 DBMS_OUTPUT.PUT_LINE('*********Address update - Sno:' || acc.sno || ' Error:' || v_error_msg);
	      		 COMMIT;
	      		 v_err_count:=v_err_count+1;
	      		 v_error_msg:=NULL;
		    
		    END IF;
		 v_error_msg:=NULL;
		  END loop;
  
  dbms_output.put_line('v_err_count:'||v_err_count);
  dbms_output.put_line('v_success_count:'||v_success_count);

END STL_DM_ACCOUNT_MIGRATION;
/

