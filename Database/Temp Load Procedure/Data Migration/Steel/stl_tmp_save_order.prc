/*
************************************************
* The purpose of this procedure is to save the sales  data
* Author:Mahavishnu
************************************************* 
*/
CREATE OR REPLACE PROCEDURE stl_tmp_save_order (
	p_orderid	  IN	   t501_order.c501_order_id%TYPE
  , p_accid 	  IN	   t501_order.c704_account_id%TYPE
  , p_omode 	  IN	   t501_order.c501_receive_mode%TYPE
  , p_pname 	  IN	   t501_order.c501_received_from%TYPE
  , p_comments	  IN	   t501_order.c501_comments%TYPE
  , p_po		  IN	   t501_order.c501_customer_po%TYPE
  , p_total 	  IN	   t501_order.c501_total_cost%TYPE
  , p_userid	  IN	   t501_order.c501_created_by%TYPE
  , p_str		  IN	   CLOB
  , p_ordtype	  IN	   t501_order.c901_reason_type%TYPE
  , p_conscode	  IN	   t741_construct.c741_construct_id%TYPE
  , p_parentid	  IN	   t501_order.c501_parent_order_id%TYPE
  , p_shipto	  IN	   t907_shipping_info.c901_ship_to%TYPE
  , p_shiptoid	  IN	   t907_shipping_info.c907_ship_to_id%TYPE
  , p_shipcarr	  IN	   t907_shipping_info.c901_delivery_carrier%TYPE
  , p_shipmode	  IN	   t907_shipping_info.c901_delivery_mode%TYPE
  , p_addressid   IN	   t907_shipping_info.c106_address_id%TYPE
  , p_trackno     IN 	   t907_shipping_info.c907_tracking_number%TYPE
  , p_caseid      IN	   t7100_case_information.c7100_case_id%TYPE
  , p_hard_po	  IN	   t501_order.c501_hard_po_fl%TYPE
  , p_attn		  IN	   t907_shipping_info.c907_override_attn_to%TYPE
  , p_ship_instruc IN	   t907_shipping_info.c907_ship_instruction%TYPE
  , p_ref_ordid	   IN	 t501_order.c501_order_id%TYPE
  , p_ship_cost	  IN		t501_order.c501_ship_cost%TYPE
  , p_order_dt    IN DATE
  , p_ship_dt     IN DATE
  , p_rep_id       IN      t501_order.C703_SALES_REP_ID%TYPE DEFAULT NULL
)

AS
	p_shipid VARCHAR2(100);
BEGIN
	stl_gm_save_item_order_TEMP(p_orderid,p_accid,p_omode,p_pname,p_comments,p_po,p_total,p_userid,p_str,p_ordtype,p_conscode,p_parentid,p_shipto,p_shiptoid,p_shipcarr,p_shipmode,p_addressid,p_shipid,p_caseid,p_hard_po,p_attn,p_ship_instruc,p_ref_ordid,p_ship_cost,p_rep_id);

	UPDATE t501_order
	   SET c501_status_fl = 3
	     , c501_shipping_date = p_ship_dt
		 , c501_order_date = TRUNC(p_order_dt)
		 , c501_surgery_date = TRUNC(p_order_dt)
	     , c501_order_date_time = p_order_dt
		 , c501_last_updated_by = p_userid
		 , c501_last_updated_date = CURRENT_DATE
	 WHERE c501_order_id = p_orderid;


	UPDATE t907_shipping_info
	   SET c907_status_fl = 40
	   , c907_tracking_number = p_trackno
		 , c907_shipped_dt = p_ship_dt
		 , c907_last_updated_by = p_userid
		 , c907_last_updated_date = CURRENT_DATE
	 WHERE c907_ref_id = p_orderid AND c901_source = '50180' AND c907_void_fl IS NULL
	   AND c907_void_fl IS NULL;

END stl_tmp_save_order;
/
