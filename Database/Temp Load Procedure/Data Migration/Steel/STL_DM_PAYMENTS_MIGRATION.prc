/*
************************************************
* The purpose of this procedure is to load the payments
* Author: Mahavishnu
************************************************* 
*/

CREATE OR REPLACE
PROCEDURE STL_DM_PAYMENTS_MIGRATION(
  p_company_id     IN	   t1900_company.c1900_company_id%TYPE
, p_plant_id 	   IN	   T5040_Plant_Master.C5040_PLANT_ID%TYPE
)
AS
V_USER T101_USER.C101_USER_ID%TYPE		:='303043';
 

  V_ERR_COUNT NUMBER:=0;
  V_SUCCESS_COUNT NUMBER:=0; 
  V_SCRIPT CLOB;
  v_user_id NUMBER:=303043;
  P_ACC_ID  VARCHAR2(100);
  P_ACC_SOURCE_ID  VARCHAR2(100);
  P_ACC_TYPE  VARCHAR2(100);
  p_pay_term   VARCHAR2(100);
  v_pay_str CLOB;
  V_INVOICE_ID T503_INVOICE.C503_INVOICE_ID%TYPE;
  V_CUST_PO T503_INVOICE.C503_CUSTOMER_PO%TYPE;
  INVOICE_STATUS NUMBER;
  ISSUE_DATE DATE;
  v_pay_date DATE;
  DEALER_INV_CLOSING_DATE_ID NUMBER;
  v_count NUMBER;
  v_inv_pending NUMBER;
  v_company_dtfrmt T901_Code_Lookup.C901_CODE_NM%TYPE;
  v_company_timezone  T901_Code_Lookup.c901_code_id%TYPE;
  
  v_error_msg    Varchar2(1000);
  
  CURSOR CUR_PM IS 
    SELECT * FROM STL_DM_PAYMENT 
    WHERE 
    MIGRATED IS NULL
    AND ERROR_DETAILS IS NULL;
  
  BEGIN
 
	  
	   BEGIN
		SELECT C901_TIMEZONE,GET_CODE_NAME(C901_DATE_FORMAT) INTO v_company_timezone,v_company_dtfrmt 
		  FROM T1900_COMPANY 
		WHERE C1900_COMPANY_ID=p_company_id;
		EXCEPTION WHEN no_data_found THEN
			raise_application_error ('-20999', ' Company id is not available ');
	END;
	  
 

  -- Set company context
  	gm_pkg_cor_client_context.gm_sav_client_context(p_company_id,get_code_name(v_company_timezone),v_company_dtfrmt,p_plant_id);
  
  
  	FOR  PM IN   CUR_PM
      LOOP
      
      	 -- Validation block
      	 -- validating invoice source id is not null and invoice id is available in 
      	 IF PM.C503_INVOICE_ID IS NULL THEN
			   v_error_msg:= ' Source Invoice ID is null || ';
	     ELSE
	     	BEGIN
	     	SELECT C503_INVOICE_ID,C503_CUSTOMER_PO,C704_ACCOUNT_ID INTO  V_INVOICE_ID,V_CUST_PO,P_ACC_ID FROM T503_INVOICE
	     	WHERE C503_EXT_REF_ID= PM.C503_INVOICE_ID AND C1900_COMPANY_ID=p_company_id;
	     	EXCEPTION WHEN no_data_found THEN
			     	v_error_msg:= v_error_msg || ' Invocie id is not available || ';
			WHEN OTHERS THEN
			      v_error_msg:= v_error_msg || SQLERRM || ' || ';
			END;
		 END IF;
		 
		 
--		 IF PM.Payment_Receive_Date IS NULL THEN
--		 	v_error_msg:= v_error_msg || ' Received date is Null || ';
--		 
--		 ELSE
		 		 BEGIN
			        SELECT to_date(PM.Payment_Receive_Date ,  v_company_dtfrmt ) INTO v_pay_date 
				          FROM DUAL;
				     EXCEPTION WHEN OTHERS THEN
				       	v_pay_date:=null;
				        v_error_msg:= v_error_msg || SQLERRM || ' || ';
			  		END;
--		 EMD IF;
		 
		 
		 --Validation block ended
		 
		 IF v_error_msg IS NULL THEN
		 
		 BEGIN
			  
		 	BEGIN 	
			 SELECT C901_CODE_ID INTO p_pay_term 
			   FROM T901_CODE_LOOKUP
			 WHERE TRIM(Upper(C901_CODE_NM))=TRIM(Upper(PM.Payment_Received_Mode)) 
			   AND C901_CODE_GRP='PAYME'
			   AND C901_VOID_FL IS NULL;
		  EXCEPTION WHEN no_data_found THEN
			  	p_pay_term:='5011'; -- set default as Bank transfer
		  END;
		  
		  v_pay_str:=REPLACE(PM.INPUT_STR, 'v_inv_id', V_INVOICE_ID);
		  
		  v_pay_str:=REPLACE(v_pay_str, 'v_custpo', V_CUST_PO);
		  
		  v_pay_str:=REPLACE(v_pay_str, 'v_pay_mode', p_pay_term);
		  
		  GM_UPDATE_INVOICE_MULTIPLE(v_pay_str,v_pay_date,p_pay_term,PM.Payment_Details,v_user_id,P_ACC_ID,PM.Payment_Received);
		  
		  
		SELECT  NVL(TO_NUMBER(PM.C503_INVOICE_AMOUNT),0)-NVL(TO_NUMBER(PM.Payment_Received),0)    INTO v_inv_pending FROM DUAL;
		 
		SELECT DECODE(v_inv_pending,0,2,0)  INTO INVOICE_STATUS      FROM DUAL;
		 
		UPDATE T503_INVOICE SET C503_STATUS_FL= INVOICE_STATUS WHERE C503_INVOICE_ID=V_INVOICE_ID;
		 
		UPDATE STL_DM_PAYMENT SET MIGRATED='Y',MIGRATED_DATE=SYSDATE  WHERE SNO=PM.sno;

        	V_SUCCESS_COUNT:=V_SUCCESS_COUNT+1;
        	
		 COMMIT;
		  EXCEPTION WHEN OTHERS THEN
		        v_error_msg:=SQLERRM;
		        DBMS_OUTPUT.PUT_LINE('*********Payment update - SNO:' || PM.sno || ' Error:' || v_error_msg);
		        ROLLBACK;
		         UPDATE STL_DM_PAYMENT SET ERROR_DETAILS=v_error_msg WHERE SNO=PM.sno;
		        V_ERR_COUNT:=V_ERR_COUNT+1;
		        v_error_msg:=null;
		        COMMIT;
		      END;
		 
		 ELSE
		 		UPDATE STL_DM_PAYMENT SET ERROR_DETAILS=v_error_msg WHERE SNO=PM.SNO;
	      		 DBMS_OUTPUT.PUT_LINE('*********Payment update - SNO:' || PM.sno || ' Error:' || v_error_msg);
	      		 COMMIT;
	      		 V_ERR_COUNT:=V_ERR_COUNT+1;
	      		 v_error_msg:=NULL;
		 
		 END IF;
         v_error_msg:=NULL;
      END LOOP;
  
	   DBMS_OUTPUT.PUT_LINE('v_success_count:'||V_SUCCESS_COUNT);
  	    DBMS_OUTPUT.PUT_LINE('v_err_count:'||V_ERR_COUNT);	
  END STL_DM_PAYMENTS_MIGRATION;
/
