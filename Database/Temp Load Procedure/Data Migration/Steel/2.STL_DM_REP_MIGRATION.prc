/*
************************************************
* The purpose of this procedure is to load the Sales Rep master Data
* * Author  : Mahavishnu
************************************************* 
*/

CREATE OR REPLACE
PROCEDURE STL_DM_REP_MIGRATION(
  p_company_id     IN	   t1900_company.c1900_company_id%TYPE
, p_plant_id 	   IN	   T5040_Plant_Master.C5040_PLANT_ID%TYPE
)
AS
P_DIST_ID VARCHAR2(100);
P_SALESREPID VARCHAR2(100);
v_addressid VARCHAR2(100);
P_PARTY_ID  VARCHAR2(100);
P_LOG_MSG VARCHAR2(1000);
REP_START_DATE VARCHAR2 (20) ;
v_rep_err_count number:=0;
v_user_id NUMBER:=303043;
v_login_name t102_user_login.C102_LOGIN_USERNAME%TYPE;
v_territory_id T702_Territory.C702_Territory_Id%TYPE;
v_contract_date  T703_Sales_Rep.C703_START_DATE%TYPE;
v_contract_end   T703_Sales_Rep.C703_END_DATE%TYPE;
v_designation   T901_Code_Lookup.c901_code_id%TYPE;
v_terr_id	   NUMBER;
v_division     T1910_Division.C1910_DIVISION_ID%TYPE;
v_company_dtfrmt T901_Code_Lookup.C901_CODE_NM%TYPE;
v_company_timezone  T901_Code_Lookup.c901_code_id%TYPE;
v_error_msg    Varchar2(1000);

 CURSOR CUR_SR IS 
    SELECT * FROM STL_DM_SALES_REP 
    WHERE 
    MIGRATED IS NULL
    AND ERROR_DETAILS IS NULL;

BEGIN

	
   BEGIN
		SELECT C901_TIMEZONE,GET_CODE_NAME(C901_DATE_FORMAT) INTO v_company_timezone,v_company_dtfrmt 
		  FROM T1900_COMPANY 
		WHERE C1900_COMPANY_ID=p_company_id;
		EXCEPTION WHEN no_data_found THEN
			raise_application_error ('-20999', ' Company id is not available ');
	END;
  
   
   	-- Set company context
  	gm_pkg_cor_client_context.gm_sav_client_context(p_company_id,get_code_name(v_company_timezone),v_company_dtfrmt,p_plant_id);
    	
	  FOR  SR IN   CUR_SR
	    LOOP
	      
	    --- Validation block
		      --Validating employee id is present
			 	 IF SR.EMPLOYEE_ID IS NULL THEN
				       v_error_msg:= ' Source ID is null || ';
				 END IF;
    
		      --Validating Distributor is present in spineit
			      BEGIN
				      SELECT C701_DISTRIBUTOR_ID INTO P_DIST_ID 
				        FROM T701_Distributor 
				      WHERE TRIM(Upper(C701_DISTRIBUTOR_NAME)) = TRIM(Upper(SR.DISTRIBUTOR_NAME))
				        AND C1900_COMPANY_ID=p_company_id
				        AND C701_VOID_FL IS NULL;	
				  EXCEPTION WHEN no_data_found THEN
			       		 v_error_msg:= v_error_msg ||' Distributor is not available || ';
			      WHEN OTHERS THEN
			               v_error_msg:= v_error_msg || SQLERRM ||chr(10)||dbms_utility.format_error_backtrace ||chr(10) || ' || ';
			      END;
			      
			    
				      
			      --Validating Designation is valid
			       BEGIN 
					        SELECT C901_CODE_ID INTO v_designation 
					          FROM T901_CODE_LOOKUP
					        WHERE TRIM(Upper(C901_CODE_NM))=TRIM(Upper(SR.DESIGNATION))
					          AND C901_CODE_GRP='DSGTP'
					          AND C901_VOID_FL IS NULL;
					      EXCEPTION WHEN no_data_found THEN
			            	v_error_msg:= v_error_msg || ' Designation is not available || ';
			              WHEN OTHERS THEN
			               v_error_msg:= v_error_msg || SQLERRM ||chr(10)||dbms_utility.format_error_backtrace ||chr(10) || ' || ';
		       	   END;
		       		
		       	   --Validating Division is valid
		       		BEGIN 
					        SELECT C1910_DIVISION_ID INTO v_division 
					          FROM T1910_Division
					        WHERE TRIM(Upper(C1910_DIVISION_NAME))=TRIM(Upper(SR.PRIMARY_DIVISION))
					          AND C1910_VOID_FL IS NULL;
					      EXCEPTION WHEN no_data_found THEN
			            	v_error_msg:= v_error_msg || ' Division is not available || ';
			                WHEN OTHERS THEN
			                v_error_msg:= v_error_msg || SQLERRM ||chr(10)||dbms_utility.format_error_backtrace ||chr(10) || ' || ';
		       		 END; 
		       		 
		       		 
		       		 
			      -- Converting contract start date format to company date format
			      BEGIN
					  SELECT to_date(SR.START_DATE ,  v_company_dtfrmt ) INTO v_contract_date 
					    FROM DUAL;
					   EXCEPTION WHEN OTHERS THEN
					       	v_contract_date:=null;
					       	v_error_msg:= v_error_msg ||' Start date is null || ';
				  END;
			  			
			  			
			  	-- Converting contract start date format to company date format		
			  	  BEGIN
					  SELECT to_date(SR.END_DATE ,  v_company_dtfrmt ) INTO v_contract_end 
					    FROM DUAL;
					  EXCEPTION WHEN OTHERS THEN
						     v_contract_end:=null;
				  END;
			
			-- Validation block end  	
			-- Sales rep procedure call	  
	     
		  IF v_error_msg IS NULL THEN
	    		
		  		BEGIN
			  		
			  		-- If territory is not present,creating new territory as sales rep name
			  		  BEGIN
				      SELECT C702_Territory_Id INTO v_territory_id 
				        FROM T702_Territory
				      WHERE TRIM(Upper(C702_Territory_Name))=TRIM(Upper(SR.TERRITORY_NAME));
				   	  EXCEPTION WHEN no_data_found THEN
			       		SELECT s702_territory.NEXTVAL	INTO v_territory_id  FROM DUAL;
			      	-- Creating new territory as sales rep name		
			       		INSERT INTO t702_territory
			       		(c702_territory_id, c702_territory_name, c702_created_by, c702_created_date, c702_active_fl,C1900_COMPANY_ID					)
			 			VALUES (v_territory_id, SR.SALES_REP_NAME_EN, v_user_id, SYSDATE, 'Y', p_company_id);
			       		DBMS_OUTPUT.PUT_LINE('Territory created --'|| v_territory_id);
			      	 END;
			  		
			      	 -- before executing GM_SAVE_SALESREP procedure,Assigning P_SALESREPID value as NULL.
			      	 P_SALESREPID:=NULL;
		    		
					GM_SAVE_SALESREP(SR.FIRST_NAME,SR.LAST_NAME,SR.SALES_REP_NAME_EN,P_DIST_ID,SR.REP_CATEGORY,P_SALESREPID,v_user_id,
										'Add',v_territory_id,SR.ACTIVE_FL,NULL,v_contract_date,v_contract_end,v_designation,NULL,NULL,v_division);
	    				 DBMS_OUTPUT.PUT_LINE('p_salesrepid:'||P_SALESREPID);
				       -- Get the Party id and sales rep id of the sales rep by using distributor english name as distributor and rep name are same
				        BEGIN
							SELECT  C101_PARTY_ID  INTO  P_PARTY_ID
					          FROM T703_SALES_REP 
					        WHERE C703_SALES_REP_ID =  p_salesrepid
                              AND c703_void_fl is null;
				        	EXCEPTION WHEN no_data_found THEN
				       		 v_error_msg:= ' Party details is not created for sales rep || ';
				        END;
				      			 DBMS_OUTPUT.PUT_LINE('partyid:'||P_PARTY_ID);	      
				        IF v_error_msg IS NULL THEN
						        -- Insert Log for sales rep,  1211 rep in GPLOG code group
						        GM_UPDATE_LOG(P_SALESREPID, 'Data migrated from source system',1211,303043,P_LOG_MSG);
						
						        --Update source distributor id in ext_ref_id column
						        UPDATE T703_SALES_REP SET C703_EXT_REF_ID = SR.EMPLOYEE_ID WHERE C703_SALES_REP_ID =  P_SALESREPID;
						        
						        UPDATE STL_DM_SALES_REP SET MIGRATED='Y',MIGRATED_DATE=SYSDATE WHERE SNO=SR.SNO;
						        
						      -- Added the below insert to add the email id for the user. This is required to create the login credentials for the user
							  
								select c101_user_id, lower(concat(substr(c101_user_f_name,1,1), c101_user_l_name)) into v_user_id,v_login_name from t101_user  where c101_party_id=P_PARTY_ID
                                and C901_USER_STATUS =311 ;
                
								INSERT INTO t102_user_login (C102_USER_LOGIN_ID,C101_USER_ID,C102_LOGIN_USERNAME,C102_USER_PASSWORD,C102_PASSWORD_EXPIRED_FL,C102_NEW_USER_FL,C102_USER_LOCK_FL,C901_AUTH_TYPE,C102_LOGIN_PIN,C102_LOGIN_LOCK_FL,C102_LOGIN_ATTEMPT_CNT,C102_LOGIN_ATTEMPT_TIME,C102_PASSWORD_UPDATED_DATE,C102_LAST_UPDATED_BY,C102_LAST_UPDATED_DATE,C102_EXT_ACCESS,C906C_LDAP_MASTER_ID)
								VALUES (S102_USER_LOGIN.nextval,v_user_id,v_login_name,'Globus!234',NULL,'Y',NULL,321,NULL,NULL,NULL,NULL,NULL,v_user_id,CURRENT_DATE,'N',1);
								
								INSERT INTO T703a_Salesrep_Div_Mapping Values(S703a_Salesrep_Div_Mapping.nextval,p_salesrepid,v_division,'105442',NULL,303043, CURRENT_DATE,null );
						         DBMS_OUTPUT.PUT_LINE('final p_salesrepid:'||P_SALESREPID);
						         COMMIT;
						 ELSE
						       	ROLLBACK;
						       	UPDATE STL_DM_SALES_REP SET ERROR_DETAILS=v_error_msg WHERE SNO=SR.SNO;
						      	DBMS_OUTPUT.PUT_LINE('*********Sales rep update party err - Sno:' || SR.sno || ' Error:' || v_error_msg);
						      	COMMIT;
						      	v_rep_err_count:=v_rep_err_count+1;
						      	v_error_msg:=NULL;
				       
				        END IF;
	         
		      EXCEPTION WHEN OTHERS THEN
		        v_error_msg:=SQLERRM ||chr(10)||dbms_utility.format_error_backtrace || chr(10);
		        DBMS_OUTPUT.PUT_LINE('*********Sales Rep update - Sno:' || SR.sno || ' Error:' || v_error_msg);
		        ROLLBACK;
		         UPDATE STL_DM_SALES_REP SET ERROR_DETAILS=v_error_msg WHERE SNO=SR.SNO;
		         COMMIT;
		        v_rep_err_count:=v_rep_err_count+1;
		        v_error_msg:=null;
		      END;
	        
	     ELSE 
	     		ROLLBACK;
	       		UPDATE STL_DM_SALES_REP SET ERROR_DETAILS=v_error_msg WHERE SNO=SR.SNO;
	      		 DBMS_OUTPUT.PUT_LINE('*********Sales Rep update - Sno:' || SR.sno || ' Error:' || v_error_msg);
	      		 COMMIT;
	      		 v_rep_err_count:=v_rep_err_count+1;
	      		 v_error_msg:=NULL;
	     END IF;
	 	v_error_msg:=null;
	  END LOOP;
	  DBMS_OUTPUT.PUT_LINE('v_rep_err_count:'||v_rep_err_count);
	 

END STL_DM_REP_MIGRATION;
/

