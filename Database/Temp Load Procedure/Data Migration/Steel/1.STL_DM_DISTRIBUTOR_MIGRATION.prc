/*
************************************************
* The purpose of this procedure is to load the Field Sales Rep master Data
* Author : Mahavishnu
************************************************* 
*/

CREATE OR REPLACE
PROCEDURE STL_DM_DISTRIBUTOR_MIGRATION(
  p_company_id     IN	   t1900_company.c1900_company_id%TYPE
, p_plant_id 	   IN	   T5040_Plant_Master.C5040_PLANT_ID%TYPE
)
AS
P_MESSAGE VARCHAR2(100);
P_DIST_ID VARCHAR2(100);
P_Log_Msg Varchar2(1000);
v_dist_err_count number:=0;
v_user_id NUMBER:=303043;
v_contract_date  t701_distributor.C701_CONTRACT_START_DATE%TYPE;
v_contract_end   t701_distributor.C701_CONTRACT_START_DATE%TYPE;
v_region         T901_Code_Lookup.c901_code_id%TYPE;
v_ship_state    T901_Code_Lookup.c901_code_id%TYPE;
v_ship_con     T901_Code_Lookup.c901_code_id%TYPE;
v_bill_state   T901_Code_Lookup.c901_code_id%TYPE;
v_bill_con     T901_Code_Lookup.c901_code_id%TYPE;
v_commission_type T901_Code_Lookup.c901_code_id%TYPE;
v_dist_type  T901_Code_Lookup.c901_code_id%TYPE;
v_company_dtfrmt T901_Code_Lookup.C901_CODE_NM%TYPE;
v_company_timezone  T901_Code_Lookup.c901_code_id%TYPE;
v_error_msg    Varchar2(1000);

CURSOR CUR_FS IS 
    SELECT * FROM STL_DM_DISTRIBUTOR 
    WHERE 
    MIGRATED IS NULL 
    AND ERROR_DETAILS IS NULL;

BEGIN
  
	
   BEGIN
		SELECT C901_TIMEZONE,GET_CODE_NAME(C901_DATE_FORMAT) INTO v_company_timezone,v_company_dtfrmt 
		  FROM T1900_COMPANY 
		WHERE C1900_COMPANY_ID=p_company_id;
		EXCEPTION WHEN no_data_found THEN
			raise_application_error ('-20999', ' Company id is not available ');
	END;
  

   -- Set company context
  	gm_pkg_cor_client_context.gm_sav_client_context(p_company_id,get_code_name(v_company_timezone),v_company_dtfrmt,p_plant_id);
  
  	
    FOR  FS IN   CUR_FS
      LOOP
      
		        -- Validation block
		        -- Validating employee id is present
			        IF FS.EMPLOYEE_ID IS NULL  THEN
			        	v_error_msg:= ' Source ID is null || ';
			        END IF;
		        
			    -- Validating Distributor type is valid
		        	BEGIN 
					        SELECT C901_CODE_ID INTO v_dist_type 
					          FROM T901_CODE_LOOKUP
					        WHERE TRIM(Upper(C901_CODE_NM))=TRIM(Upper(FS.DISTRIBUTOR_TYPE))
					          AND C901_CODE_GRP='DSTYP'
					          AND C901_VOID_FL IS NULL;
					      EXCEPTION WHEN no_data_found THEN
			            	v_error_msg:= v_error_msg || ' Distributor type is not available || ';
			              WHEN OTHERS THEN
			               v_error_msg:= v_error_msg || SQLERRM || ' || ';
		       		 END;
		        	
		        -- Validating Region is valid
			        BEGIN 
					        SELECT C901_CODE_ID INTO v_region 
					          FROM T901_CODE_LOOKUP
					        WHERE TRIM(Upper(C901_CODE_NM))=TRIM(Upper(FS.REGION))
					          AND C901_CODE_GRP='REGN'
					          AND C901_VOID_FL IS NULL;
					      EXCEPTION WHEN no_data_found THEN
			            	v_error_msg:= v_error_msg || ' Region is not available || ';
			              WHEN OTHERS THEN
			               v_error_msg:= v_error_msg || SQLERRM || ' || ';
		       		 END;
		       		
		       		 -- Validating Ship state is valid 
		       		 BEGIN 
					        SELECT C901_CODE_ID INTO v_ship_state 
					          FROM T901_CODE_LOOKUP
					        WHERE TRIM(Upper(C901_CODE_NM))=TRIM(Upper(FS.SHIP_STATE)) 
					          AND C901_CODE_GRP='STATE'
					          AND C901_VOID_FL IS NULL;
					      EXCEPTION WHEN no_data_found THEN
			            	v_error_msg:= v_error_msg || ' Ship state is not available || ';
			              WHEN OTHERS THEN
			               v_error_msg:= v_error_msg || SQLERRM || ' || ';
		       		 END;
		       		 
		       		  -- Validating Ship country is valid
		       		 BEGIN 
					        SELECT C901_CODE_ID INTO v_ship_con 
					          FROM T901_CODE_LOOKUP
					        WHERE TRIM(Upper(C901_CODE_NM))=TRIM(Upper(FS.SHIP_COUNTRY)) 
					          AND C901_CODE_GRP='CNTY'
					          AND C901_VOID_FL IS NULL;
					      EXCEPTION WHEN no_data_found THEN
			            	v_error_msg:= v_error_msg || ' Ship country is not available || ';
			              WHEN OTHERS THEN
			               v_error_msg:= v_error_msg || SQLERRM || ' || ';
		       		 END;
		       		 
		       		  -- Validating Bill state is valid
		       		 BEGIN 
					        SELECT C901_CODE_ID INTO v_bill_state 
					          FROM T901_CODE_LOOKUP
					        WHERE TRIM(Upper(C901_CODE_NM))=TRIM(Upper(FS.BILL_STATE)) 
					          AND C901_CODE_GRP='STATE'
					          AND C901_VOID_FL IS NULL;
					      EXCEPTION WHEN no_data_found THEN
			            	v_error_msg:= v_error_msg || ' Bill state is not available || ';
			              WHEN OTHERS THEN
			               v_error_msg:= v_error_msg || SQLERRM || ' || ';
		       		 END;
		       		 
		       		  -- Validating Bill country is valid
		       		 BEGIN 
					        SELECT C901_CODE_ID INTO v_bill_con 
					          FROM T901_CODE_LOOKUP
					        WHERE TRIM(Upper(C901_CODE_NM))=TRIM(Upper(FS.BILL_COUNTRY)) 
					          AND C901_CODE_GRP='CNTY'
					          AND C901_VOID_FL IS NULL;
					      EXCEPTION WHEN no_data_found THEN
			            	v_error_msg:= v_error_msg || ' Bill country is not available || ';
			              WHEN OTHERS THEN
			               v_error_msg:= v_error_msg || SQLERRM || ' || ';
		       		 END;
				        
		       		  -- Validating Commission type is valid
				    BEGIN 	
					      SELECT C901_CODE_ID INTO v_commission_type 
					        FROM T901_CODE_LOOKUP
					      WHERE TRIM(Upper(C901_CODE_NM))=TRIM(Upper(FS.COMMISSION_TYPE)) 
					        AND C901_CODE_GRP='CNTYP'
					      AND C901_VOID_FL IS NULL;
					      EXCEPTION WHEN no_data_found THEN
			            	v_error_msg:= v_error_msg || ' Commission type is not available || ';
			              WHEN OTHERS THEN
			               v_error_msg:= v_error_msg || SQLERRM || ' || ';
		       		 END;
		       		 
			        -- Validation block end
			        
			        -- Converting contract start date format to company date format
			        BEGIN
				        SELECT to_date(FS.CONTRACT_START_DATE ,  v_company_dtfrmt ) INTO v_contract_date 
				          FROM DUAL;
				        EXCEPTION WHEN OTHERS THEN
				        	v_error_msg:= v_error_msg || SQLERRM || ' || ';
			  		END;
		  			
		  			
		  			 -- Converting contract end date format to company date format
		  			 BEGIN
				        SELECT to_date(FS.CONTRACT_END_DATE ,  v_company_dtfrmt ) INTO v_contract_end 
				          FROM DUAL;
				        EXCEPTION WHEN OTHERS THEN
				        	v_contract_end:=null;
			  		END;
	  			
	  			
		
		
		--Calling distributor procedure
	         IF v_error_msg IS NULL THEN
	         
	         	BEGIN
			         gm_save_distributor(FS.DISTRIBUTOR_NAME,FS.DISTRIBUTOR_NAME_EN,v_region,FS.DISTRIBUTOR_INCHARGE,FS.CONTACT_PERSON,FS.BILL_NAME,FS.SHIP_NAME,
			         					  FS.BILL_ADD1,FS.SHIP_ADD1,FS.BILL_ADD2,FS.SHIP_ADD2,FS.BILL_CITY,FS.SHIP_CITY,v_bill_state,v_ship_state,v_bill_con,
			         					  v_ship_con,FS.BILL_ZIP_CODE,FS.SHIP_ZIP_CODE,FS.PHONE_NUMBER,FS.FAX,v_contract_date,v_contract_end,FS.COMMENTS,
			         					  FS.ACTIVE_FL,'',v_user_id,'Add',FS.COMMISSION_PERCENT,v_dist_type,'','',
			         					  v_commission_type,P_MESSAGE,P_DIST_ID,NULL);
		  			
		         
			          -- Insert Log for distributor  1210 distributor in GPLOG code group
			          GM_UPDATE_LOG(P_DIST_ID, 'Data migrated from source system',1210,303043,P_LOG_MSG);
			        
			         --Update source distributor id in ext_ref_id column
			          UPDATE T701_DISTRIBUTOR SET C701_EXT_REF_ID = FS.EMPLOYEE_ID WHERE C701_DISTRIBUTOR_ID =  P_DIST_ID;
			         
			          -- Mark successful saved records as Migrated
			          UPDATE STL_DM_DISTRIBUTOR SET MIGRATED='Y',MIGRATED_DATE=SYSDATE WHERE SNO=FS.SNO;
			  		 
			          COMMIT;
			         DBMS_OUTPUT.PUT_LINE('*********Distributor update - Sno:' || FS.sno || ' Success and P_DIST_ID=== '||P_DIST_ID );
		        
		        EXCEPTION WHEN OTHERS THEN
		        	v_error_msg:=SQLERRM;
		          	DBMS_OUTPUT.PUT_LINE('*********Distributor update - Sno:' || FS.sno || ' Error:' || v_error_msg);
		          	ROLLBACK;
		           
		          	UPDATE STL_DM_DISTRIBUTOR SET ERROR_DETAILS=v_error_msg WHERE SNO=FS.SNO;
		          	COMMIT;
		           
		          	v_dist_err_count:=v_dist_err_count+1;
		         	v_error_msg:=NULL;
	        	END;
	        	
	  		ELSE
	  			 
	      		 UPDATE STL_DM_DISTRIBUTOR SET ERROR_DETAILS=v_error_msg WHERE SNO=FS.SNO;
	      		 DBMS_OUTPUT.PUT_LINE('*********Distributor update - Sno:' || FS.sno || ' Error:' || v_error_msg);
	      		 COMMIT;
	      		 v_dist_err_count:=v_dist_err_count+1;
	      		 v_error_msg:=NULL;
	      		 
	  		END IF;
	  		
	  		v_error_msg:=NULL;
  		
    END LOOP;
  		DBMS_OUTPUT.PUT_LINE('v_dist_err_count:'||v_dist_err_count);
END STL_DM_DISTRIBUTOR_MIGRATION;
/
