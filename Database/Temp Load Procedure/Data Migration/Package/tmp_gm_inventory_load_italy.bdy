create or replace
PACKAGE BODY tmp_gm_inventory_load_italy

IS
	
/***********************************************************************************
    *Description : Procedure to create set consignment 
	*Process IABL,GMCN,Control,Verify,Putaway and Shipout
**************************************************************************************/
		PROCEDURE tmp_gm_set_load_trans
		AS
  
 
      v_commit_count NUMBER :='0';
	  v_set_validation_flag varchar2(10);
	  v_out_consign varchar2(100);
	  v_out_request_id varchar2(100);
	  v_err_dtls clob;
	  v_dist_id t701_distributor.c701_distributor_id%TYPE;
	  v_rep_id  t703_sales_rep.c703_sales_rep_id%TYPE;
	  v_acc_id  t704_account.c704_account_id%TYPE;
    
	CURSOR cur_set
    IS

        SELECT DISTINCT (C207_SET_ID) sets_id, C5010_tag_id tag_id,TEAM_SYSTEM_ID TEAM_ID,
        TYPE type
        FROM globus_app.ITALY_SET_LOAD_STG ITY
        WHERE 
        ITY.set_loaded_fl IS NULL
        and ITY.c207_set_id IS NOT NULL
        order by C207_SET_ID, C5010_tag_id;
      
       
 BEGIN   
    FOR set_list IN cur_set
	
    LOOP
	    BEGIN 
		-- Added to insert new tag id and update taggable part flag as Y
		    tmp_gm_tag_load(set_list.tag_id,set_list.sets_id);
			
			tmp_gm_set_validation(set_list.sets_id, set_list.tag_id,set_list.TEAM_ID,set_list.type,v_set_validation_flag,v_dist_id,v_rep_id,v_acc_id);
			
		-- Type as inhouse , hardcoded distributor id as globus medical italy
			IF(set_list.type='INHOUSE') THEN
				
				v_dist_id:= 1805; --1805 globus medical italy
				v_rep_id:= null;
				v_acc_id:= null;
			END IF;
			
		IF (v_set_validation_flag='PASS') THEN
				
		-- Creating iabl transactionv
		-- Setting context as netherland
				gm_pkg_cor_client_context.gm_sav_client_context('1010',get_code_name('4000906'),get_code_name('105131'),'3003');
	            tmp_gm_iabl (set_list.sets_id, set_list.tag_id);
	            
	      -- Set buit  
		  -- Setting context as italy
	            gm_pkg_cor_client_context.gm_sav_client_context('1020',get_code_name('10306118'),get_code_name('105131'),'3003');
	            tmp_gm_set_build (set_list.sets_id, set_list.tag_id,set_list.TEAM_ID,set_list.type,v_dist_id,v_rep_id,v_acc_id, v_out_consign,v_out_request_id);	
	           
	            dbms_output.put_line('v_out_consign:'||v_out_consign||'  v_out_request_id:'||v_out_request_id);
	      -- Ship out
	      		
	      		-- Shipout 
	            tmp_set_ship_trans(v_out_consign,v_out_request_id,set_list.sets_id, set_list.tag_id,set_list.type,v_acc_id,v_rep_id,v_dist_id);
	            
				
                
                    UPDATE ITALY_SET_LOAD_STG
                    SET set_loaded_fl    = 'SHIPPED'
                    WHERE c207_SET_ID  = set_list.sets_id
                    AND c5010_tag_id = set_list.tag_id;           
        END IF;
    
   			 EXCEPTION
    
   				 WHEN OTHERS THEN
					gm_pkg_cor_client_context.gm_sav_client_context('1020',get_code_name('10306118'),get_code_name('105131'),'3003');
					
   				   dbms_output.put_line ('Set Build Processing failed for the set:'||set_list.sets_id||'And Tag ID:'||
	    		    set_list.tag_id||SQLERRM);
	    		    Rollback;  
        
	            UPDATE ITALY_SET_LOAD_STG
	            SET set_loaded_fl    = 'Set Build Processing failed'
	              WHERE c207_SET_ID  = set_list.sets_id
	                AND c5010_tag_id = set_list.tag_id;
	             
	                          
		          v_err_dtls :='Set Build Processing failed: Error message: '||SQLERRM;
	              INSERT INTO ITALY_SET_STATUS_LOG(C207_SET_ID,C5010_TAG_ID,ERR_DTLS) VALUES(set_list.sets_id,set_list.tag_id,v_err_dtls);
	           	  commit;
     
    END;
    
             
   COMMIT;   
		
 END LOOP;
END tmp_gm_set_load_trans;

/***********************************************************************************
    *Description : Procedure to insert new tag id and update taggable part value as Y in temp table
 **************************************************************************************/
PROCEDURE tmp_gm_tag_load( 
p_tag_id  IN t5010_tag.C5010_TAG_ID%TYPE,
p_set_id  IN t208_set_details.C207_SET_ID%TYPE)
AS
v_tag_check number:=0;
v_tag_Count number :=0;
v_tag_fl    varchar2(10);


CURSOR cur_set IS

	  SELECT ITY.c205_part_number_id PART_NO
	  FROM ITALY_SET_LOAD_STG ITY
	  WHERE ITY.c207_SET_ID=p_set_id
	  AND ITY.c5010_tag_id=p_tag_id
	  AND ITY.set_loaded_fl IS NULL
	  AND ITY.physical_set_qty > 0
	  GROUP BY ITY.c205_part_number_id;

BEGIN
		   
	 		  SELECT count(1) INTO v_tag_check
	   		  FROM T5010_TAG 
	   		  WHERE C5010_TAG_ID=p_tag_id
	   		  and c5010_void_FL is null;
	   		  
	   -- Insert into tag table  
	   IF (v_tag_check='0') THEN
	          
	   	INSERT INTO t5010_tag(c5010_tag_id, c901_status, c5010_created_by, c5010_created_date, c5010_history_fl)  
	   	VALUES (p_tag_id,'51011','839135',current_date,'Y');
	   END IF;
	   
	   FOR  inv_set IN cur_set

 		 LOOP
  -- checks part is taggable part
  
 		 Select Count(1) Into V_Tag_Count From T205_Part_Number T205, T208_Set_Details T208
          WHERE   T205.C205_Product_Family=4052 --4052 -Graphic Cases
	       AND T208.C205_Part_Number_Id  =T205.C205_Part_Number_Id
           AND t208.C208_SET_QTY <>0
           AND t208.c207_set_id=p_set_id
           AND T205.C205_Part_Number_Id =inv_set.PART_NO;
		   
  			v_tag_fl:= get_part_attribute_value(inv_set.PART_NO,'92340');
  			
  			IF(v_tag_fl='Y' AND v_tag_Count <>0) THEN
  			
  				UPDATE ITALY_SET_LOAD_STG SET TAGGABLE_PART='Y' 
  				WHERE C205_PART_NUMBER_ID=inv_set.PART_NO 
  				AND c207_SET_ID = p_set_id
  				AND c5010_tag_id = p_tag_id
  				AND set_loaded_fl IS NULL;
  				
  				
  			END IF;
  			
 		 END LOOP;
	
END tmp_gm_tag_load;

/***********************************************************************************
    *Description : Procedure to Validate set,account,sales rep,tag,part
    **************************************************************************************/
PROCEDURE tmp_gm_set_validation
  	   (
        p_set_id  IN t208_set_details.C207_SET_ID%TYPE,
        p_tag_id  IN t5010_tag.C5010_TAG_ID%TYPE,
        p_team_id IN ITALY_SET_LOAD_STG.TEAM_SYSTEM_ID%TYPE,
      	p_type    IN ITALY_SET_LOAD_STG.TYPE%TYPE,
        p_set_validation_flag OUT VARCHAR,
        p_dist_id    OUT t701_distributor.c701_distributor_id%TYPE,
        p_rep_id     OUT t703_sales_rep.c703_sales_rep_id%TYPE,
        p_acc_id     OUT t704_account.c704_account_id%TYPE
		)
    AS
        v_part_num T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE;
        v_source_QTY NUMBER;
        v_set_id T207_SET_MASTER.C207_SET_ID%TYPE;
        V_SET_QTY NUMBER;
        V_SET_PART  T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE;
        v_set_part_num T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE;
        v_temp_acc t704_account.c704_account_id%TYPE;
        v_err_dtls  CLOB; -- varchar2(10000);
        v_set_type T207_SET_MASTER.C207_TYPE%TYPE;
        v_flag VARCHAR2(100);
        v_tag_part_inp_str clob :=null;
        v_tag_count number:=0;
        v_company_id t1900_company.c1900_company_id%TYPE;
        v_plant_id T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
		v_count_num number:=0;
		v_tag_status VARCHAR2(100);
        v_part_number_count NUMBER:=0;
        v_set_count number:=0;
        v_tag_check number:=0;
        v_taggable_cnt NUMBER;
        v_dist_id t701_distributor.c701_distributor_id%TYPE;
      /*  CURSOR cur_source_set
        IS
 SELECT C205_PART_NUMBER_ID PART_NO
   FROM ITALY_SET_LOAD_STG
  WHERE C207_SET_ID=p_set_id
AND C5010_TAG_ID=p_tag_id
GROUP BY C205_PART_NUMBER_ID
MINUS
SELECT t2023.C205_PART_NUMBER_ID PART_NO
FROM T2023_PART_COMPANY_MAPPING t2023
where t2023.C1900_COMPANY_ID = v_company_id
AND t2023.C2023_VOID_FL IS NULL;	
*/   

-- part price check
CURSOR part_price_dtls_cur
IS
select C205_PART_NUMBER_ID pnum from ITALY_SET_LOAD_STG where C207_SET_ID=p_set_id
AND C5010_TAG_ID=p_tag_id 
GROUP BY C205_PART_NUMBER_ID
minus
select c205_part_number_id pnum from T2052_PART_PRICE_MAPPING where C1900_COMPANY_ID = v_company_id and C2052_VOID_FL IS NULL;

           
    BEGIN
	    
        p_set_validation_flag :='PASS';
        
         
  		  SELECT get_plantid_frm_cntx  INTO v_plant_id FROM dual;
  		  SELECT get_compid_frm_cntx() INTO v_company_id FROM dual;
                 
      --- Account Availablity check 
           
        IF (p_type = 'ACCOUNT') THEN

         BEGIN
	    
	          SELECT count(1) INTO v_count_num
	          FROM t704_account 
	          WHERE 
	          C704_EXT_REF_ID=p_team_id
	          and c704_void_fl is null
	          and c1900_company_id=v_company_id;
	          
	       IF (v_count_num='0') THEN
	          	v_err_dtls :='SpineIT Account ID was not created for the Italy Acct ID: '||p_team_id;
	          
	         	 INSERT INTO ITALY_SET_STATUS_LOG(C207_SET_ID,C5010_TAG_ID,ACC_ID,ERR_DTLS)
	         	 VALUES(p_set_id,p_tag_id,p_team_id,v_err_dtls);
	          	 p_set_validation_flag :='FAIL';
	          
	       ELSE  	 
	       		
	       	BEGIN
				SELECT v700.D_ID,v700.REP_ID,v700.AC_ID INTO p_dist_id,p_rep_id,p_acc_id FROM 
				t704_account t704,v700_territory_mapping_detail v700
				WHERE t704.c704_account_id = v700.AC_ID
   				AND t704.c704_ext_ref_id=p_team_id
   				 AND t704.c1900_company_id =v_company_id;   
   			--	DBMS_OUTPUT.PUT_LINE ('D_ID ='|| p_dist_id ||' REP_ID ='|| p_rep_id|| ' AC_ID ='|| p_acc_id  ) ;
		       	 Exception
      		 WHEN NO_DATA_FOUND THEN
      		 	v_err_dtls :='SpineIT Account ID was not Mapped for the DIST or REP: '||p_team_id;
	         	 INSERT INTO ITALY_SET_STATUS_LOG(C207_SET_ID,C5010_TAG_ID,ACC_ID,ERR_DTLS)
	         	 VALUES(p_set_id,p_tag_id,p_team_id,v_err_dtls);
	          	 p_set_validation_flag :='FAIL';
	          	 p_dist_id := null;
	          	 p_rep_id:= null;
	          	 p_acc_id:=null;
		    END;
	       
	       END IF;
	      
	     END;
	 END IF;
	 
	 
	 -- Rep check
	 
	 IF (p_type = 'REP') THEN

         BEGIN
	    
	          SELECT count(1) INTO v_count_num
	          FROM t703_sales_rep
	          WHERE 
	          C703_EXT_REF_ID=p_team_id
	          and c703_void_fl is null
	          and c1900_company_id=v_company_id;
	       
	       IF (v_count_num='0') THEN
	          	v_err_dtls :='SpineIT Rep ID was not created for the Italy rep ID: '||p_team_id;
	          
	         	 INSERT INTO ITALY_SET_STATUS_LOG(C207_SET_ID,C5010_TAG_ID,REP_ID,ERR_DTLS)
	         	 VALUES(p_set_id,p_tag_id,p_team_id,v_err_dtls);
	          	 p_set_validation_flag :='FAIL';
	       ELSE
	       BEGIN
		   	
		   	SELECT DISTINCT v700.D_ID,v700.REP_ID INTO p_dist_id,p_rep_id FROM 
		   	t703_sales_rep t703,v700_territory_mapping_detail v700
			WHERE   v700.REP_ID = t703.c703_sales_rep_id 
			 AND t703.C703_EXT_REF_ID=p_team_id
			  AND t703.c1900_company_id =v_company_id;
			  
			  p_acc_id := null;
		       	 Exception
      		 WHEN NO_DATA_FOUND THEN
      		 	v_err_dtls :='SpineIT Rep ID was not Mapped for the DIST  : '||p_team_id;
	         	 INSERT INTO ITALY_SET_STATUS_LOG(C207_SET_ID,C5010_TAG_ID,ACC_ID,ERR_DTLS)
	         	 VALUES(p_set_id,p_tag_id,p_team_id,v_err_dtls);
	          	 p_set_validation_flag :='FAIL';
	          	  p_dist_id := null;
	          	  p_rep_id:= null;
	          	  p_acc_id:=null;
		    END;   	 
	          	 
	       END IF;
	      
	     END;
	 END IF;
     
	
	   -- Set ID check   

	 
	     BEGIN
	         
		      SELECT count(1) into v_set_count
	   		  FROM T207_SET_MASTER
	   		  WHERE C207_SET_ID=p_set_id
	   		  and c207_void_FL IS NULL;
	          
	   		
	          IF (v_set_count=0) THEN
	          
		          v_err_dtls :='Set ID Not Found in the Set Master Table : '||p_set_id;
		          
		          INSERT INTO ITALY_SET_STATUS_LOG(C207_SET_ID,C5010_TAG_ID,ERR_DTLS) VALUES(p_set_id,p_tag_id,v_err_dtls);
		          p_set_validation_flag :='FAIL';
		          
	          
	           -- Set Type Check
   
	         ELSE
	         
	   		  SELECT NVL(C207_TYPE,'-999') INTO v_set_type FROM
	   		  T207_SET_MASTER
	   		  WHERE C207_SET_ID=p_set_id
	   		  and c207_void_FL IS NULL;
	          
	   		  -- 4070 : Consignment 
	          IF (v_set_type NOT IN ('4070')) THEN
	          
		          v_err_dtls :='Set Type is not a Consignment . Please change the set type to 4070 (Consignment Set) for the Set ID : '||p_set_id;
		          
		          INSERT INTO ITALY_SET_STATUS_LOG(C207_SET_ID,C5010_TAG_ID,ERR_DTLS) VALUES(p_set_id,p_tag_id,v_err_dtls);
		          p_set_validation_flag :='FAIL';
		          
	          END IF;
	        
	         END IF;
	        
	      END;    
	          
	  --- Tag  check 
	  	      
	      BEGIN	      
	   		  SELECT count(1) INTO v_tag_check
	   		  FROM T5010_TAG 
	   		  WHERE C5010_TAG_ID=p_tag_id
	   		  and c5010_void_FL is null;
	          
	          IF (v_tag_check='0') THEN
	          
	          v_err_dtls :='Tag  not available in tag table for the tag id : '||p_tag_id;
	          
              INSERT INTO ITALY_SET_STATUS_LOG(C207_SET_ID,C5010_TAG_ID,ERR_DTLS) VALUES(p_set_id,p_tag_id,v_err_dtls);
	          p_set_validation_flag :='FAIL';
	           
	            
      		 ELSE
	      
	 			--- Tag Status check 
         	      
	   		  SELECT NVL(c901_status,'-999') INTO v_tag_status
	   		  FROM T5010_TAG 
	   		  WHERE C5010_TAG_ID=p_tag_id
	   		  and c5010_void_FL is null;
	          
	          IF (v_tag_status NOT IN ('51011','51012')) THEN
	          
	          v_err_dtls :='Tag Status is not in released or in Active status for the Tag id : '||p_tag_id;
	          
              INSERT INTO ITALY_SET_STATUS_LOG(C207_SET_ID,C5010_TAG_ID,ERR_DTLS) VALUES(p_set_id,p_tag_id,v_err_dtls);
	          p_set_validation_flag :='FAIL';
	           
	           END IF;
	      END IF;
	   END; 
          
                  
	      
         /*BEGIN   	 
           
           FOR cur_set IN cur_source_set
             LOOP
              
                  V_PART_NUM      := cur_set.PART_NO;
                  
                  
                  v_err_dtls :='Part number: '||V_PART_NUM||' not found in Part company mapping table. Set ID: '||p_set_id||' TagID '||p_tag_id;
                  INSERT INTO ITALY_SET_STATUS_LOG(C207_SET_ID,C5010_TAG_ID,ERR_DTLS) VALUES(p_set_id,p_tag_id,v_err_dtls);
                  p_set_validation_flag :='FAIL';
                  
                 
             END LOOP;
           END; */
         
         -- part price check
	    FOR part_price_dtls IN part_price_dtls_cur
	    LOOP
	    	
	    --
	    v_err_dtls :='Part # price not available : '||part_price_dtls.pnum ||' Set ID: '||p_set_id||' TagID '||p_tag_id;
	    
	      INSERT INTO ITALY_SET_STATUS_LOG(C207_SET_ID,C5010_TAG_ID,ERR_DTLS) VALUES(p_set_id,p_tag_id,v_err_dtls);
          p_set_validation_flag :='FAIL';
	    
	    END LOOP; 
	    
	     -- taggable part check
	     SELECT count(1) INTO v_tag_count from  ITALY_SET_LOAD_STG
	     WHERE c207_SET_ID  = p_set_id
				AND   c5010_tag_id = p_tag_id
				AND TAGGABLE_PART = 'Y';
		
		 IF v_tag_count = 0
         THEN
         	 v_err_dtls :='No tagabble part associated with the Set for tag : '||p_tag_id||' , the Setid:  '||p_set_id;
              INSERT INTO ITALY_SET_STATUS_LOG(C207_SET_ID,C5010_TAG_ID,ERR_DTLS) VALUES(p_set_id,p_tag_id,v_err_dtls);
              p_set_validation_flag :='FAIL';
         END IF;
         
         IF (v_tag_count > 1) THEN
         
         	  v_err_dtls :='More than 1 tagabble part associated with the Set in the Tag Attribute for tag : '||p_tag_id||' , the Setid:  '||p_set_id||' Taggable parts are '||v_tag_part_inp_str;
              INSERT INTO ITALY_SET_STATUS_LOG(C207_SET_ID,C5010_TAG_ID,ERR_DTLS) VALUES(p_set_id,p_tag_id,v_err_dtls);
              p_set_validation_flag :='FAIL';
	           
         END IF;
         
         
         IF (p_set_validation_flag ='FAIL') THEN 
         
            	UPDATE ITALY_SET_LOAD_STG
				SET set_loaded_fl    = 'Record Skipped. Check ITALY_SET_STATUS_LOG table for details'
				WHERE c207_SET_ID  = p_set_id
				AND   c5010_tag_id = p_tag_id; 
	      
		 END IF;
	DBMS_OUTPUT.PUT_LINE (' Set ID  '|| p_set_id || ' TAG ID '|| p_tag_id ||' Valid Status '||p_set_validation_flag);
		 
    END tmp_gm_set_validation;
    
    
    /***********************************************************************************
    *Description : Procedure to Create IABL transaction
    **************************************************************************************/
    
   PROCEDURE tmp_gm_iabl
		(
        p_set_id IN t208_set_details.C207_SET_ID%TYPE,
        p_tag_id IN t5010_tag.C5010_TAG_ID%TYPE
		)

AS
  v_user t101_user.c101_user_id%TYPE;
  v_initiate_input_str CLOB;
  v_control_input_str CLOB;
  v_verify_input_str CLOB;
  v_part_no t205_part_number.c205_part_number_id%TYPE;
  v_txn_id t504_consignment.c504_consignment_id%TYPE;
  v_cost t820_costing.c820_purchase_amt%TYPE;
  v_purchase_amt t820_costing.c820_purchase_amt%TYPE;
  v_company_cost  t820_costing.C820_LOCAL_COMPANY_COST%TYPE;
  v_message varchar2(20);
  v_reason NUMBER;
  v_txntype NUMBER;
  v_wh_type VARCHAR2(100);
  v_cur_wh  VARCHAR2(50); 
  v_txn_type_name varchar2(50);
  v_source_wh NUMBER;
  v_target_wh NUMBER;
  v_out VARCHAR2(100);
  v_costing_id NUMBER;
  v_company_id NUMBER;
  v_plant_id NUMBER;
  v_portal_com_date_fmt VARCHAR2 (20) ;
  V_TRIP_PRICE NUMBER;
  v_qty NUMBER;
  v_control_num VARCHAR2(40);
  v_costing_type NUMBER;
  v_count number :=0;
  v_part_company number;
  v_control_loc_str CLOB;

 
CURSOR cur_set IS

	  SELECT ITY.c205_part_number_id PART_NO, ITY.LOT_NUMBER CONTROL_NUMBER,
	  SUM(ITY.physical_set_qty) QTY
	  FROM ITALY_SET_LOAD_STG ITY
	  WHERE ITY.c207_SET_ID=p_set_id
	  AND ITY.c5010_tag_id=p_tag_id
	  AND ITY.set_loaded_fl IS NULL
	  AND ITY.physical_set_qty > 0
	  GROUP BY ITY.c205_part_number_id , ITY.LOT_NUMBER 
	  ;
 
BEGIN

-- 105131 -- dd/mm/yyyy
  SELECT get_compdtfmt_frm_cntx () INTO v_portal_com_date_fmt FROM dual;
  SELECT get_plantid_frm_cntx  INTO v_plant_id FROM dual;
  SELECT get_compid_frm_cntx() INTO v_company_id FROM dual;
  -- Below line of code will get the user who is executing the script. This will avoid the need to pass the user id as a parameter
  v_user := IT_PKG_CM_USER.GET_VALID_USER; 
  

  
FOR  inv_set IN cur_set

  LOOP
  
   BEGIN
	   
      v_part_no :=inv_set.part_no;
      v_qty := inv_set.qty;
      v_control_num :=inv_set.control_number;
      v_txn_type_name:='INVAD-BULK';
      v_txntype:= '400080';
      v_target_wh:='90814';
      v_source_wh:='90814';
      v_cur_WH := 'BL';
      v_wh_type:='90814';
      v_costing_type  :='4900';
      v_reason :='90814';
    
 BEGIN
	    
     select COST, company_cost INTO v_purchase_amt, v_company_cost 
     from ITY_DM_INVENTORY 
     where part=v_part_no 
     and warehouse='FG';     
       

     Exception
    
       WHEN NO_DATA_FOUND THEN
       v_purchase_amt:='0';
       v_company_cost :='0';

 END;
   	   
	   SELECT NVL(TRIM(GET_PART_PRICE(v_part_no,'E')),0) INTO V_TRIP_PRICE
	    FROM DUAL;
		 
   -- Get Inventory cost or insert if not available
   -- **Review Check in Costing table and not from the function ** --
    --  SELECT get_inventory_cogs_value(v_part_no)  INTO v_cost FROM dual;
    
      SELECT count(1) INTO v_cost FROM t820_costing
      WHERE c205_part_number_id=v_part_no
      AND c820_delete_fl = 'N'
      --and c901_status='4801'
      AND c1900_company_id =  v_company_id
      and c901_costing_type=4900;
--      
 
     IF v_cost = 0 THEN
    -- to get the owner company id 
	SELECT C1900_COMPANY_ID INTO v_part_company FROM  t2023_part_company_mapping t2023
      WHERE t2023.c205_part_number_id=v_part_no
      and c901_part_mapping_type='105360';
     
      --
       INSERT INTO t820_costing
	   ( c820_costing_id,c205_part_number_id,c901_costing_type,c820_part_received_dt,c820_qty_received,c820_purchase_amt,c820_qty_on_hand 
    	,c901_status,c820_delete_fl,c820_created_by,c820_created_date,c1900_company_id,c5040_plant_id,C820_LOCAL_COMPANY_COST
		,C1900_OWNER_COMPANY_ID,c901_owner_currency,C901_LOCAL_COMPANY_CURRENCY       
		) 
	   VALUES ( s820_costing.nextval,v_part_no,v_costing_type,CURRENT_DATE,0,v_purchase_amt,0,'4802','N','2277820',CURRENT_DATE,v_company_id        
			    ,v_plant_id,v_company_cost,v_part_company,'1','2');
        -- Build input string 
        END IF;
        
        v_initiate_input_str := v_initiate_input_str || v_part_no || ',' || v_qty || ',,,' || V_TRIP_PRICE || '|';        
        v_control_input_str := v_control_input_str || v_part_no || ',' || v_qty || ',' || v_control_num || ',,' || V_TRIP_PRICE || '|';
       
        v_verify_input_str := NULL;
       
       
      IF v_txn_id IS NULL THEN
      
         SELECT get_next_consign_id(v_txn_type_name) 
         INTO v_txn_id
         FROM dual;
         
      END IF;

   -- Commented the below code since there is no need to update the bulk location qty during IABL
   -- 	gm_pkg_op_inv_field_sales_tran.gm_sav_fs_inv_loc_part_details (v_bl_location_id,v_txn_id, v_part_no,v_qty,'400080','4301',null,current_date, v_user);    
     END;
     
     
	v_count :=v_count+1;

		if(v_count>20) then 
			
				
		  -- IABL initiate 
		          gm_save_inhouse_item_consign(v_txn_id,v_txntype , v_reason ,0,'','IABL created for the Set ID:'||p_set_id||':Tag ID:'||p_tag_id,v_user,v_initiate_input_str,v_out);
		        dbms_output.put_line('IABL Txn=' || v_txn_id || ' Initiated inner ');
		  -- IABL Control 
		          gm_ls_upd_inloanercsg(v_txn_id,v_txntype,'0','0',NULL,'Italy Set Data Load:'||p_tag_id,v_user,v_control_input_str,null,'ReleaseControl',v_message);
		            dbms_output.put_line('IABL Txn=' || v_txn_id || ' Controlled inner');
		  -- IABL Verify
		  
		          gm_ls_upd_inloanercsg(v_txn_id,v_txntype,'0','0',NULL,'Italy Set Data load:'||p_tag_id,v_user,v_verify_input_str,null,'Verify',v_message);
		          dbms_output.put_line('IABL Txn=' || v_txn_id || ' Verified');
        
		   	     
		          UPDATE t412_inhouse_transactions SET c412_comments='IABL Inventory Update for Italy Set Building:'||p_tag_id , c412_last_updated_date=CURRENT_DATE, c412_last_updated_by=v_user
			 	  where c412_inhouse_trans_id=v_txn_id;
		        
		        
			      v_txn_id := NULL;
			      v_count :=0;
			      v_initiate_input_str :=null;
			      v_control_input_str :=null;
		END IF;

  END LOOP;
  

          -- Outer Loop
          
   IF (v_initiate_input_str is not null) THEN

	  
		   -- IABL initiate 
		          gm_save_inhouse_item_consign(v_txn_id,v_txntype , v_reason ,0,'','IABL created for the Set ID:'||p_set_id||':Tag ID:'||p_tag_id,v_user,v_initiate_input_str,v_out);
                   dbms_output.put_line('IABL Txn=' || v_txn_id || ' Initiated Outer ');
		      
		   -- IABL Control 
		          GM_LS_UPD_INLOANERCSG(v_txn_id,v_txntype,'0','0',NULL,'Italy Set Data Load:'||p_tag_id,v_user,v_control_input_str,null,'ReleaseControl',v_message);
		          dbms_output.put_line('IABL Txn=' || v_txn_id || ' Controlled outer');
		   -- IABL Verify
		  
		    	  gm_ls_upd_inloanercsg(v_txn_id,v_txntype,'0','0',NULL,'Italy Set Data load:'||p_tag_id,v_user,v_verify_input_str,null,'Verify',v_message);
		          dbms_output.put_line('IABL Txn=' || v_txn_id || ' Verified');
		        
	        
		          UPDATE t412_inhouse_transactions SET c412_comments='IABL Inventory Update for ITY Set Building:'||p_tag_id , c412_last_updated_date=CURRENT_DATE, c412_last_updated_by=v_user
				  where c412_inhouse_trans_id=v_txn_id;
				  v_txn_id := NULL;
			      v_count :=0;
			      v_initiate_input_str :=null;
				  v_control_input_str :=null;
			    
  END IF;
  
  END tmp_gm_iabl;
  
  
      /***********************************************************************************
    *Description : Procedure to initiate set consignment ,control and verify
    **************************************************************************************/
  PROCEDURE tmp_gm_set_build
		(
        p_set_id IN t208_set_details.C207_SET_ID%TYPE,
        p_tag_id IN t5010_tag.C5010_TAG_ID%TYPE,
        p_team_id IN ITALY_SET_LOAD_STG.TEAM_SYSTEM_ID%TYPE,
      	p_type    IN ITALY_SET_LOAD_STG.TYPE%TYPE,
      	p_dist_id    IN t701_distributor.c701_distributor_id%TYPE,
        p_rep_id     IN t703_sales_rep.c703_sales_rep_id%TYPE,
        p_acc_id     IN t704_account.c704_account_id%TYPE,
        p_out_consign_id OUT t504_consignment.c504_consignment_id%TYPE,
        p_out_request_id OUT t520_request.c520_request_id%TYPE
		)
AS
	  v_user t101_user.c101_user_id%TYPE;
	  v_initiate_input_str_set_build CLOB;
	  v_part_no t205_part_number.c205_part_number_id%TYPE;
	  v_tag_flag VARCHAR2 (2) ;
	  v_tag_part_number t205_part_number.c205_part_number_id%TYPE;
	  v_tag_control_number VARCHAR2(40);
	  v_input_str_tag_creation CLOB;
	  v_company_id t1900_company.C1900_COMPANY_ID%TYPE;
	  v_plant_id t5040_plant_master.C5040_PLANT_ID%TYPE;
	  v_portal_com_date_fmt VARCHAR2 (100) ;
	  v_qty NUMBER;
	  v_control_num VARCHAR2(40);
	  v_errmsg VARCHAR2 (4000) ;
	  v_tagable_flag VARCHAR(2);
	  v_input_str_putaway CLOB;
	  v_msg varchar2(4000);
	  --
	  v_etch_id t504a_consignment_loaner.c504a_etch_id%TYPE;
	  
CURSOR cur_set IS

	  SELECT ITY.C205_PART_NUMBER_ID PART_NO, ITY.LOT_NUMBER CONTROL_NUMBER,
	  ITY.PHYSICAL_SET_QTY QTY, ITY.TAGGABLE_PART TAG_FLAG
	  FROM ITALY_SET_LOAD_STG ITY
	  WHERE ITY.C207_SET_ID=p_set_id
	  AND ITY.C5010_TAG_ID=p_tag_id
	  and ITY.set_loaded_fl is null;
	 
	 
	BEGIN

	  SELECT get_compdtfmt_frm_cntx () INTO v_portal_com_date_fmt FROM dual;
	  v_user := IT_PKG_CM_USER.GET_VALID_USER;  
	  
	  
	  
	FOR  inv_set IN cur_set
	 
	 LOOP
	  
	   BEGIN
		   
		   v_qty         :=inv_set.QTY;
		   v_part_no     :=inv_set.PART_NO;
		   v_control_num :=inv_set.CONTROL_NUMBER;   
					 
		 -- Build Input String for Set Build
		   v_initiate_input_str_set_build := v_initiate_input_str_set_build||v_part_no||'^'||v_qty||'^'||v_control_num||'^|';  
		  
		  v_tag_flag    := inv_set.TAG_FLAG;
			
		IF v_tag_flag         	  = 'Y' THEN
		
			v_tagable_flag 		 :='Y'; 
			v_tag_part_number    := v_part_no;
			v_tag_control_number := v_control_num;
				
		END IF;
		  
	  END;
		
	END LOOP;

			gm_pkg_op_process_request.gm_sav_initiate_set(NULL,to_char(CURRENT_DATE, v_portal_com_date_fmt),'50618',NULL,p_set_id,'40021',
			p_dist_id,'50626',v_user,'4120',NULL,NULL,'15',v_user,NULL,CURRENT_DATE,p_out_request_id,p_out_consign_id);
			 dbms_output.put_line('p_out_consign_id:'||p_out_consign_id||'  p_out_request_id:'||p_out_request_id);
	  IF (v_tagable_flag ='Y') THEN

			v_input_str_tag_creation:=p_tag_id||'^'||v_tag_control_number||'^'||v_tag_part_number||'^'||p_set_id||'^'||'51000^'||p_out_consign_id||'^'||'40033^|';
			gm_pkg_ac_tag_info.gm_sav_tag_detail(v_input_str_tag_creation,v_user,v_errmsg);

		 
		 ELSE
			dbms_output.put_line('The set ID**'||p_set_id||'and the tag id '||p_tag_id||' does not have a tagable part');
			
	  END IF;
	 
		dbms_output.put_line('set build initiate ');
		-- Set build Processing Lot Number verification step
			gm_save_set_build(p_out_consign_id, v_initiate_input_str_set_build,'ITY Data Migration tag#'||p_tag_id, '1.10', '0', v_user, v_errmsg,'set_consignment');
			
			dbms_output.put_line('Controlled');
		-- Moving the Set to Pending putaway
			gm_save_set_build(p_out_consign_id, v_initiate_input_str_set_build,'ITY Data Migration tag#'||p_tag_id, '1.20', '1', v_user, v_errmsg,'set_consignment');
			dbms_output.put_line('Verfied');
           	-- END Italy migration
--			--    
		
	 
END tmp_gm_set_build;  

    /***********************************************************************************
    *Description : Procedure to Shipout set consignments
    **************************************************************************************/
PROCEDURE tmp_set_ship_trans
(
 p_out_consign_id IN t504_consignment.c504_consignment_id%TYPE,
 p_out_request_id IN t520_request.c520_request_id%TYPE,
 p_set_id IN t208_set_details.C207_SET_ID%TYPE,
 p_tag_id IN t5010_tag.C5010_TAG_ID%TYPE,
 p_type   IN ITALY_SET_LOAD_STG.TYPE%TYPE,
 p_acc_id IN t704_account.c704_account_id%TYPE,
 p_rep_id IN t703_sales_rep.c703_sales_rep_id%TYPE,
 p_dist_id IN t701_distributor.c701_distributor_id%TYPE       
)
AS
v_shipid number;
v_ship_to     NUMBER;
v_ship_to_id   NUMBER;
v_input_str_pending_put CLOB;
v_user t101_user.c101_user_id%TYPE;
v_ship_out_user NUMBER := 303137; -- John Davidar      
v_track_upd VARCHAR2(100); 
v_shipflag varchar2(10);

BEGIN
	
	 v_user := IT_PKG_CM_USER.GET_VALID_USER;  
	
	  IF(p_type ='ACCOUNT') THEN
			v_ship_to   := '4122';  -- Account
			v_ship_to_id := p_acc_id;
	  ELSIF(p_type ='REP') THEN
			v_ship_to   := '4121'; -- sales rep
			v_ship_to_id := p_rep_id;
	  ELSIF(p_type ='INHOUSE') THEN
	  v_ship_to   := '4120'; -- sales rep
	  v_ship_to_id := p_dist_id;
	  END IF;

-- Save shipping details
	   gm_pkg_cm_shipping_trans.gm_sav_shipping (p_out_request_id, '50184', '4120', p_dist_id, '5001', '5004', NULL,
                '0', NULL, v_ship_out_user, v_shipid, NULL, NULL) ;
                
         -- Added for italy migration 
			-- It move the consignment set and shipout
			
			
			-- Processing the Pending putaway
			v_input_str_pending_put :=p_out_consign_id||'^'||null||'^'||p_tag_id||'|';
			
			gm_pkg_op_set_put_txn.gm_sav_set_put_batch(v_input_str_pending_put,v_user);
				
			dbms_output.put_line('Pending Shipping');
			SELECT substr(p_out_consign_id||':'||p_tag_id, 0, 39) INTO v_track_upd FROM dual;
			
			gm_pkg_cm_shipping_trans.gm_sav_shipout(p_out_consign_id,'50181',v_ship_to,v_ship_to_id,'5001','5004'
           	,v_track_upd ,0,0,v_ship_out_user,0,v_shipid,NULL,v_shipflag);    
			dbms_output.put_line('Shipped Out');
           	
           	UPDATE t504_consignment
			SET C504_LAST_UPDATED_BY=v_user, C504_LAST_UPDATED_DATE =CURRENT_DATE, c504_comments='Italy Data Migration tag: '||p_tag_id
			WHERE c504_consignment_id=p_out_consign_id;
--			
--		   
			update ITALY_SET_LOAD_STG
			set NEW_TXN_ID=p_out_consign_id	
			where c207_set_id=p_set_id
			and c5010_tag_id=p_tag_id;
	    
			INSERT INTO ITY_SET_LOAD_STATUS (tag_id,set_id,New_CN_ID, Comments) VALUES(p_set_id,p_tag_id,p_out_consign_id,'Item consignment shipped out');       

END tmp_set_ship_trans;




    /***********************************************************************************
    *Description : Procedure to initiate item consignment,IAFG,GMCN,Control,Verify,shipout
    **************************************************************************************/
PROCEDURE tmp_gm_main_ity_loose_item
	(
	p_opt IN varchar2

	)
AS
	  v_set_validation_flag varchar2(10);
	  v_err_dtls clob;
	  v_comp_dt_fmt VARCHAR2(20);
	  v_dist_id   t701_distributor.c701_distributor_id%TYPE;
	  v_rep_id    t703_sales_rep.c703_sales_rep_id%TYPE;
	  v_acc_id    t704_account.c704_account_id%TYPE;
	  v_company_id t1900_company.C1900_COMPANY_ID%TYPE;

    CURSOR cur_acct
    IS
			
        SELECT TEAM_ID TEAM_ACC_ID
        FROM globus_app.ity_loose_inv_stg ity
        WHERE 
        ity.item_loaded_fl ='IAFG_PROCESSED'
        and ity.loose_item_type= p_opt
        and ity.TEAM_ID is not null
        group by TEAM_ID;
        
   CURSOR cur_FSWH
    IS

        SELECT TEAM_ID team_rep_id
        FROM globus_app.ity_loose_inv_stg ity
        WHERE 
         ity.loose_item_type=p_opt
   		and TEAM_ID is not null
   		and item_loaded_fl = 'IAFG_PROCESSED'
        group by TEAM_ID;
        
             
 BEGIN   
	 
select get_compdtfmt_frm_cntx () INTO v_comp_dt_fmt  from dual;
	IF (p_opt='ACCOUNT') THEN
		-- iafg 
		gm_pkg_cor_client_context.gm_sav_client_context('1010',get_code_name('10306118'),get_code_name('105131'),'3003');
		tmp_gm_loose_item_iafg(p_opt);
		  FOR loose_item_list IN cur_acct
	
   		  LOOP
    
	 		  BEGIN     	
		 		  
	--dbms_output.put_line ('Start'); 
				 gm_pkg_cor_client_context.gm_sav_client_context('1020',get_code_name('10306118'),get_code_name('105131'),'3003');
		           
				 SELECT get_compid_frm_cntx() INTO v_company_id FROM dual;
				 
				 SELECT v700.D_ID,v700.REP_ID,t704.c704_account_id  INTO v_dist_id,v_rep_id,v_acc_id FROM 
				t704_account t704,v700_territory_mapping_detail v700
				 WHERE  t704.c704_account_id = v700.AC_ID
	   			 AND t704.c704_ext_ref_id=loose_item_list.TEAM_ACC_ID
	   			 AND t704.c1900_company_id =v_company_id;
				 
--		           tmp_gm_loose_item_initiate(p_opt,loose_item_list.TEAM_ACC_ID);                      
	               tmp_gm_fswh_load(v_dist_id,v_rep_id,v_acc_id,loose_item_list.TEAM_ACC_ID,p_opt);  
                    UPDATE ity_loose_inv_stg
                    SET item_loaded_fl    = 'Item INITIATED'
                    WHERE 
                    TEAM_ID=loose_item_list.TEAM_ACC_ID
                    AND item_loaded_fl = 'Loose Item Initiated'
                    ;             
     			 
        
        
    	    EXCEPTION    
   				 WHEN OTHERS THEN
   				 	Rollback;              
   				  
   				 	dbms_output.put_line ('Loose Item Failure for Acct:'||loose_item_list.TEAM_ACC_ID||'And ERR '||SQLERRM);        
	                
	                v_err_dtls :='Loose Item Failure for Acct: Error message: '||SQLERRM;
		          
	              INSERT INTO ity_loose_item_status_log(ACCT_ID,ERR_DTLS) VALUES(loose_item_list.TEAM_ACC_ID,v_err_dtls);
	           	  commit;
     
     		END;
     		
      
             
   		COMMIT;   
		
 	END LOOP;
 			
 	ELSIF (p_opt='REP') THEN 
 	-- iafg
 	 gm_pkg_cor_client_context.gm_sav_client_context('1010',get_code_name('10306118'),get_code_name('105131'),'3003');
 	  tmp_gm_loose_item_iafg(p_opt);
 	  gm_pkg_cor_client_context.gm_sav_client_context('1020',get_code_name('10306118'),get_code_name('105131'),'3003');
		          
 	 FOR loose_item_fswh IN cur_FSWH
	
   		  LOOP
    
	 		  BEGIN     	
						
			 		 SELECT get_compid_frm_cntx() INTO v_company_id FROM dual;
			 		 
					SELECT DISTINCT v700.D_ID,t703.c703_sales_rep_id  INTO v_dist_id,v_rep_id FROM
					t703_sales_rep t703,v700_territory_mapping_detail v700
					WHERE  v700.REP_ID = t703.c703_sales_rep_id 
	   				AND t703.c703_ext_ref_id=loose_item_fswh.team_rep_id
	   				 AND t703.c1900_company_id =v_company_id;
			      
			         v_acc_id:= null;
			         
		           tmp_gm_fswh_load(v_dist_id,v_rep_id,v_acc_id,loose_item_fswh.team_rep_id,p_opt);                   
        
        
    	    EXCEPTION    
   				 WHEN OTHERS THEN
   				 	Rollback;              
   				  
   				 	dbms_output.put_line ('Loose Item Failure for fswh for distributor '||loose_item_fswh.team_rep_id || ' '|| SQLERRM);        
	                
	                v_err_dtls :='Loose Item Failure for dist: Error message: '||SQLERRM;
		          
	              INSERT INTO ity_loose_item_status_log(DIST_ID,ERR_DTLS) VALUES(loose_item_fswh.team_rep_id,v_err_dtls);
	           	  commit;
     
     		END;
     		
      
             
   		COMMIT;   
		
 	END LOOP;
 	
 	ELSIF (p_opt = 'INHOUSE')
 		THEN
			BEGIN
				gm_pkg_cor_client_context.gm_sav_client_context('1010',get_code_name('10306118'),get_code_name('105131'),'3003');
		           tmp_gm_loose_item_iafg(p_opt);
		            gm_pkg_cor_client_context.gm_sav_client_context('1020',get_code_name('10306118'),get_code_name('105131'),'3003');
                    tmp_gm_fswh_load(1805,null,null,'ITY',p_opt);    
     			
        	EXCEPTION    
   			 	WHEN OTHERS THEN
   			 Rollback;              
   				dbms_output.put_line (' IAFG Creation failed ' ||SQLERRM);        
     		END;
       
   		COMMIT;   
		
 	END IF;
 	
 	
END tmp_gm_main_ity_loose_item;
  
    /***********************************************************************************
    *Description : Procedure to validate part,account,rep
    **************************************************************************************/
PROCEDURE tmp_gm_loose_item_validation
  	   
    AS
      v_company_id t1900_company.c1900_company_id%TYPE;
      v_plant_id t5040_plant_master.c5040_plant_id%TYPE;
      V_ERR_DTLS VARCHAR2(2000);
      v_part_num T205_part_number.c205_part_number_id%TYPE;
      v_cnt NUMBER := 0;
      v_type  VARCHAR2(20);
 			   

 	 
 	 --  check Part # valid 
	/*	CURSOR part_num_dtls_cur
		IS
		
 SELECT C205_PART_NUMBER_ID pnum
   FROM ity_loose_inv_stg
  WHERE item_loaded_fl      IS NULL
GROUP BY C205_PART_NUMBER_ID
MINUS
SELECT t2023.C205_PART_NUMBER_ID pnum
FROM T2023_PART_COMPANY_MAPPING t2023
where t2023.C1900_COMPANY_ID = v_company_id
AND t2023.C2023_VOID_FL IS NULL;
*/
--  Account id
CURSOR account_dtls_cur
IS
SELECT team_id acc_id
   FROM ity_loose_inv_stg 
  WHERE item_loaded_fl     IS NULL
    AND team_id IS NOT NULL
GROUP BY team_id
  MINUS
 SELECT t704.C704_EXT_REF_ID acc_id
   FROM T704_ACCOUNT t704
  WHERE t704.C1900_COMPANY_ID = v_company_id
    AND t704.C704_void_fl    IS NULL
GROUP BY t704.C704_EXT_REF_ID;
--Rep id
CURSOR rep_dtls_cur
IS
SELECT team_id rep_id
   FROM ity_loose_inv_stg 
  WHERE item_loaded_fl     IS NULL
    AND team_id IS NOT NULL
GROUP BY team_id
  MINUS
 SELECT t703.C703_EXT_REF_ID rep_id
   FROM T703_SALES_REP t703
  WHERE t703.C1900_COMPANY_ID = v_company_id
    AND t703.C703_void_fl    IS NULL
GROUP BY t703.C703_EXT_REF_ID;
-- part # not available skip all account
CURSOR account_part_skip_cur
IS
 SELECT TEAM_ID acc_id
   FROM ity_loose_inv_stg
  WHERE C205_PART_NUMBER_ID = v_part_num
    AND team_id IS NOT NULL
    AND item_loaded_fl IS NULL
GROUP BY team_id;

-- part price check
CURSOR part_price_dtls_cur
IS
select C205_PART_NUMBER_ID pnum from ity_loose_inv_stg
minus
select c205_part_number_id pnum from T2052_PART_PRICE_MAPPING where C1900_COMPANY_ID = v_company_id and C2052_VOID_FL IS NULL;

-- invalid Qty
CURSOR invalid_Qty_cur
IS
SELECT ity.C205_part_number_id PART_NO
           FROM ity_loose_inv_stg ity
           WHERE 
           ity.item_loaded_fl is null
           AND ity.physical_qty <= 0
           GROUP BY ity.C205_part_number_id;          


    BEGIN
  		  SELECT get_compid_frm_cntx(), get_plantid_frm_cntx () INTO v_company_id, v_plant_id FROM dual;
  		  
	    --
	    v_cnt := 0;
	    -- Part # available check
	    /* FOR part_num_dtls IN part_num_dtls_cur
	    LOOP
	    	--
	    	v_part_num := part_num_dtls.pnum;
	    	--
	    	update ity_loose_inv_stg set item_loaded_fl = 'Skipped' where c205_part_number_id =part_num_dtls.pnum
	    AND item_loaded_fl IS NULL;
	    --
	    v_err_dtls :='Part # not available : '||part_num_dtls.pnum;
	          
	         	 INSERT INTO ity_loose_item_status_log(part_num,ERR_DTLS)  VALUES(part_num_dtls.pnum,v_err_dtls);
	    */
	         -- to check account combination
	         FOR  account_part_skip IN account_part_skip_cur
	         LOOP
	         	update ity_loose_inv_stg set item_loaded_fl = 'Skipped' where team_id =account_part_skip.acc_id
	    		AND item_loaded_fl IS NULL;
	    		--
	    		 v_err_dtls :='Part number not available : skip the account '||account_part_skip.acc_id;
	          
	         	 INSERT INTO ity_loose_item_status_log(ACCT_ID ,ERR_DTLS)  VALUES(account_part_skip.acc_id,v_err_dtls);
	         	 
	        -- END LOOP;
	         --
	         v_cnt := v_cnt + 1;
	    END LOOP;
	    DBMS_OUTPUT.PUT_LINE (' Invalid Part Number Count : '|| v_cnt);
	    --
	    v_cnt := 0;
	    -- part price check
	    FOR part_price_dtls IN part_price_dtls_cur
	    LOOP
	    		--
	    	v_part_num := part_price_dtls.pnum;
	    	--
	    	update jp_loose_inv_stg set item_loaded_fl = 'Skipped' where c205_part_number_id =part_price_dtls.pnum
	    AND item_loaded_fl IS NULL;
	    --
	    v_err_dtls :='Part # price not available : '||part_price_dtls.pnum;
	     INSERT INTO ity_loose_item_status_log(part_num,ERR_DTLS)  VALUES(part_price_dtls.pnum,v_err_dtls);
	    -- 
	    v_cnt := v_cnt + 1;
	    END LOOP;
	    DBMS_OUTPUT.PUT_LINE (' Invalid Part Price Count : '|| v_cnt);
	    --
	    v_cnt := 0;
	    -- account id validate
	    IF v_type = 'ACCOUNT' THEN
	    FOR account_dtls IN account_dtls_cur
	    LOOP
	    
	    update ity_loose_inv_stg set item_loaded_fl = 'Skipped' where team_id =account_dtls.acc_id
	    AND item_loaded_fl IS NULL;
	    --
	    v_err_dtls :='Team Account id not available : '||account_dtls.acc_id;
	          
	         	 INSERT INTO ity_loose_item_status_log(ACCT_ID,ERR_DTLS)  VALUES(account_dtls.acc_id,v_err_dtls);
	    --
	    v_cnt := v_cnt + 1;
	    END LOOP;
	    
	    
	    DBMS_OUTPUT.PUT_LINE (' Invalid Account Count : '|| v_cnt);
	    END IF;
	    -- Invalid Qty records found
	    v_cnt := 0;
	    --
	    
	    IF v_type = 'REP' THEN
	    FOR rep_dtls IN rep_dtls_cur
	    LOOP
	    
	    update ity_loose_inv_stg set item_loaded_fl = 'Skipped' where team_id =rep_dtls.rep_id
	    AND item_loaded_fl IS NULL;
	    --
	    v_err_dtls :='Team Rep id not available : '||rep_dtls.rep_id;
	          
	         	 INSERT INTO ity_loose_item_status_log(ACCT_ID,ERR_DTLS)  VALUES(rep_dtls.rep_id,v_err_dtls);
	    --
	    v_cnt := v_cnt + 1;
	    END LOOP;
	    
	    
	    DBMS_OUTPUT.PUT_LINE (' Invalid Rep Count : '|| v_cnt);
	    END IF;
	    -- Invalid Qty records found
	    v_cnt := 0;
	    
	    FOR invalid_Qty IN invalid_Qty_cur
	    LOOP
	    --
	    update ity_loose_inv_stg set item_loaded_fl = 'Skipped' where c205_part_number_id =invalid_Qty.PART_NO
	    AND physical_qty <= 0
	    AND item_loaded_fl IS NULL;
	    --
	     v_err_dtls :='Negative Qty for the Part # : '||invalid_Qty.PART_NO;
	          
	     INSERT INTO ity_loose_item_status_log(part_num ,ERR_DTLS)  VALUES(invalid_Qty.PART_NO,v_err_dtls);
	    --
	    v_cnt := v_cnt + 1;
	    END LOOP;
	    --
		DBMS_OUTPUT.PUT_LINE (' Negative Part Qty Count : '|| v_cnt);
	
    END tmp_gm_loose_item_validation;
 

        /***********************************************************************************
    *Description : Procedure to Create IAFG transaction
    **************************************************************************************/
    PROCEDURE tmp_gm_loose_item_iafg
(
 p_opt     IN varchar2
)

AS
    v_user t101_user.c101_user_id%TYPE;
    v_initiate_input_str_IAFG CLOB;
    v_control_input_str_IAFG CLOB;
    v_verify_input_str_IAFG CLOB;
    v_part_no t205_part_number.c205_part_number_id%TYPE;
    v_company_id          NUMBER;
    v_plant_id            NUMBER;
    v_portal_com_date_fmt VARCHAR2(20) ;
    v_qty                 NUMBER;
    v_purchase_amt        NUMBER := 0;
    v_lot_number   		  VARCHAR2(40) ;
    v_reason	          NUMBER;
    v_txntype             NUMBER;
    v_wh_type             VARCHAR2(100) ;
    v_cur_wh              VARCHAR2(50) ;
    v_txn_type_name       VARCHAR2(50) ;
    v_message             VARCHAR2(100) ;
    v_source_wh           NUMBER;
    v_target_wh           NUMBER;
    v_costing_type        NUMBER;
    V_TXN_ID              VARCHAR2(20) ;
    V_OUT                 NUMBER;
    v_fg_location_id t5052_location_master.C5052_LOCATION_ID%TYPE;
    v_part_price t205_part_number.c205_equity_price%TYPE;
    v_cost t820_costing.c820_purchase_amt%TYPE;
    v_company_cost  t820_costing.C820_LOCAL_COMPANY_COST%TYPE;
    v_count number := 0;
    v_pkey NUMBER;
    v_part_company number;
    v_comp_dt_fmt VARCHAR2(20);
	   
    CURSOR cur_item_iafg
    IS
         SELECT ity.C205_part_number_id PART_NO, sum(ity.PHYSICAL_QTY) QTY, ity.lot_number LOT_NUMBER
           FROM ity_loose_inv_stg ity
           WHERE 
           ity.item_loaded_fl is null
           and ity.loose_item_type=p_opt
           AND ity.physical_qty > 0
           GROUP BY ity.C205_part_number_id, ity.lot_number;
           
       
BEGIN
	
   BEGIN
    
	  v_user := IT_PKG_CM_USER.GET_VALID_USER; 
	  select get_compdtfmt_frm_cntx () INTO v_comp_dt_fmt  from dual;

		
	SELECT get_compdtfmt_frm_cntx () INTO v_portal_com_date_fmt FROM dual;
    SELECT get_plantid_frm_cntx  INTO v_plant_id FROM dual;
    SELECT get_compid_frm_cntx() INTO v_company_id FROM dual;
     
     -- Create Location for FG and BL
  BEGIN
	  
    SELECT c5052_location_id INTO v_fg_location_id
    FROM t5052_location_master
    WHERE c5052_location_cd='011-B-004-B-4' 
    and c5040_plant_id= v_plant_id
    AND c5052_void_fl is null;
    
     EXCEPTION WHEN OTHERS THEN
     dbms_output.put_line('*********Location error' || ' Error:' || sqlerrm);
  END;
  
  -- delete temp table
  delete from MY_TEMP_KEY_VALUE;
  
  FOR inv_item IN cur_item_iafg
  
    LOOP
        BEGIN
		
                v_qty     		:= inv_item.QTY;
                v_part_no 		:= inv_item.PART_NO;
                v_lot_number    := inv_item.LOT_NUMBER;
                v_txn_type_name := 'InvAdj-Shelf';
                v_txntype       := '400068';
                v_target_wh     := '90800';
                v_source_wh     := '400069';
                v_cur_WH        := 'FG';
                v_wh_type       := '90800';
                v_costing_type  := '4900';
                v_reason        := '90800';                
                --v_pkey 			:= inv_item.pkey;
                 
                BEGIN
                     SELECT COST, company_cost
                       INTO v_purchase_amt, v_company_cost
                       FROM ITY_DM_INVENTORY
                       WHERE part      = v_part_no
                       AND warehouse = 'FG';
                EXCEPTION
                WHEN NO_DATA_FOUND THEN
        
                    v_purchase_amt := '0';
                    v_company_cost :='0';
                END;
                
                SELECT C1900_COMPANY_ID INTO v_part_company FROM  t2023_part_company_mapping t2023
                WHERE t2023.c205_part_number_id=v_part_no
                and c901_part_mapping_type='105360'
                AND C2023_void_fl is null;
                
              BEGIN
                SELECT count(1) INTO v_cost FROM t820_costing
                WHERE c205_part_number_id=v_part_no
                AND c820_delete_fl = 'N'
                --and c901_status=4801
                and c901_costing_type=4900
                and c1900_company_id=v_company_id;
                 
                  EXCEPTION
                WHEN OTHERS THEN
        
                  v_cost:=NULL;
                END;
                
              
      
    IF v_cost = 0 THEN
              
		       INSERT INTO t820_costing (
		           c820_costing_id
		          ,c205_part_number_id
		          ,c901_costing_type
		          ,c820_part_received_dt
		          ,c820_qty_received
		          ,c820_purchase_amt
		          ,c820_qty_on_hand       
		          ,c901_status
		          ,c820_delete_fl
		          ,c820_created_by
		          ,c820_created_date
		          ,c1900_company_id
		          ,c5040_plant_id
		          ,C820_LOCAL_COMPANY_COST  
		          ,C1900_OWNER_COMPANY_ID
		          ,c901_owner_currency
		          ,C901_LOCAL_COMPANY_CURRENCY
		        ) 
		        VALUES 
		        (	 s820_costing.nextval
		          ,v_part_no
		          ,v_costing_type
		          ,CURRENT_DATE
		          ,0
		          ,v_purchase_amt
		          ,0
		          ,4801
		          ,'N'
		          ,V_USER
		          ,CURRENT_DATE
		          ,v_company_id
		          ,v_plant_id
		          ,v_company_cost
		          ,v_part_company
		          ,'1'
		          ,'2'
		        );
                         
   END IF;               
                                         
    --Get Part price
      BEGIN
	      
		        SELECT	c2052_equity_price price
		        INTO v_part_price
		        FROM t2052_part_price_mapping  
		        WHERE c205_part_number_id = v_part_no
		        AND c1900_company_id =v_company_id;
       
      EXCEPTION  WHEN no_data_found THEN
      
		          dbms_output.put_line('v_part_no=' || v_part_no || ' Price not found');
		          v_part_price := null;
		          dbms_output.put_line(v_part_no ||' - Without Equity Price, Inventory migration cannot be done, skip to next');
		          continue;
          
      END;
      
      	IF (v_txn_id is null) THEN
                
      		   		SELECT get_next_consign_id (v_txn_type_name) INTO v_txn_id FROM dual;
                
      	END IF;
           
	        		INSERT INTO MY_TEMP_KEY_VALUE (MY_TEMP_TXN_ID,MY_TEMP_TXN_KEY, my_temp_txn_value) values(p_opt ,v_part_no, v_lot_number);
	  			
	              
	                v_initiate_input_str_IAFG := v_initiate_input_str_IAFG || v_part_no || ',' || v_qty || ',,,' ||v_part_price || '|';
	                
	                v_control_input_str_IAFG := v_control_input_str_IAFG || v_part_no || ',' || v_qty || ',' ||v_lot_number || ',,' || v_part_price || '|';
	                
	                v_verify_input_str_IAFG := v_verify_input_str_IAFG || v_part_no || ',' || v_qty ||','|| v_lot_number|| ','||v_fg_location_id||',' || v_target_wh || '|';
	                
	                -- Calling FS location procedure to update the part qty
	                gm_pkg_op_inv_field_sales_tran.gm_sav_fs_inv_loc_part_details (v_fg_location_id,v_txn_id, v_part_no,v_qty,'400068',4301,null,current_date, v_user);
                
         v_count :=v_count+1;
           IF(v_count=20) then 
                
	                 gm_save_inhouse_item_consign (v_txn_id, v_txntype, v_reason, '0', '', 'IAFG created for the Loose item Adjustment', v_user, v_initiate_input_str_IAFG, v_out) ;
	                 
	                 GM_LS_UPD_INLOANERCSG (v_txn_id, v_txntype, '0', '0', NULL, 'Italy Set Data Load', v_user,v_control_input_str_IAFG, NULL, 'ReleaseControl', v_message) ;
	            
	                  
	            	 gm_ls_upd_inloanercsg (v_txn_id, v_txntype, '', '0', NULL, 'Italy -- Manual Data --load', v_user,v_verify_input_str_IAFG, NULL, 'Verify', v_message) ;
	            
	            	 --UPDATE MY_TEMP_KEY_VALUE_IAFG SET     MY_TEMP_TXN_VALUE= v_txn_id  WHERE MY_TEMP_TXN_VALUE IS NULL;
	              DBMS_OUTPUT.PUT_LINE (' TRANS ID: ' || v_txn_id);
	             	 UPDATE t412_inhouse_transactions SET c412_comments='IAFG Inventory Update - '|| p_opt  , c412_last_updated_date=CURRENT_DATE, c412_last_updated_by=v_user
				 	 where c412_inhouse_trans_id=v_txn_id;
		
				 	 	--dbms_output.put_line (' Values ' || p_opt|| p_acct_id || p_box_id || p_date || p_dist_id);
				 	 UPDATE ity_loose_inv_stg T1 SET item_loaded_fl='IAFG_PROCESSED',  new_cn_id = v_txn_id
						WHERE c205_part_number_id IN (select MY_TEMP_TXN_KEY from MY_TEMP_KEY_VALUE where  MY_TEMP_TXN_ID = p_opt)
						AND lot_number IN (select my_temp_txn_value from MY_TEMP_KEY_VALUE where  MY_TEMP_TXN_ID = p_opt)
						and loose_item_type=P_OPT
						AND item_loaded_fl IS NULL;
			-- Re-Setting the flag for the next IAFG creation.
			
		            v_count :='0';
		            v_initiate_input_str_IAFG :=NULL;
		            v_control_input_str_IAFG :=NULL;
		            v_verify_input_str_IAFG :=NULL;
		            v_txn_id := NULL;
		            --
		            DELETE FROM MY_TEMP_KEY_VALUE;
            
          END IF;
          
       END;
              
 END LOOP;
            
           IF (v_initiate_input_str_IAFG IS NOT NULL) THEN
            
		              gm_save_inhouse_item_consign (v_txn_id, v_txntype, v_reason, '0', '', 'IAFG created for the Loose item Adjustment', v_user, v_initiate_input_str_IAFG, v_out) ;
		            
		              GM_LS_UPD_INLOANERCSG (v_txn_id, v_txntype, '0', '0', NULL, 'Italy Set Data Load', v_user,
		              v_control_input_str_IAFG, NULL, 'ReleaseControl', v_message) ;
		            
		              gm_ls_upd_inloanercsg (v_txn_id, v_txntype, '', '0', NULL, 'Italy -- Manual Data --load', v_user,
		              v_verify_input_str_IAFG, NULL, 'Verify', v_message) ;
		            
		              DBMS_OUTPUT.PUT_LINE (' v_txn_id ' || v_txn_id);
		              
		              --UPDATE MY_TEMP_KEY_VALUE_IAFG SET     MY_TEMP_TXN_VALUE= v_txn_id  WHERE MY_TEMP_TXN_VALUE IS NULL;
					
		              UPDATE t412_inhouse_transactions SET c412_comments='IAFG Inventory Update - '|| p_opt ,c412_last_updated_date=CURRENT_DATE, c412_last_updated_by=v_user
					  where c412_inhouse_trans_id=v_txn_id;
			 
					  --dbms_output.put_line (' Values 222 '|| p_opt|| p_acct_id || p_box_id || p_date || p_dist_id);
					  UPDATE ity_loose_inv_stg T1 SET item_loaded_fl='IAFG_PROCESSED', new_cn_id = v_txn_id
						WHERE c205_part_number_id IN (select MY_TEMP_TXN_KEY from MY_TEMP_KEY_VALUE where  MY_TEMP_TXN_ID = p_opt)
						AND lot_number IN (select my_temp_txn_value from MY_TEMP_KEY_VALUE where  MY_TEMP_TXN_ID = p_opt)
						and loose_item_type=P_OPT
						AND item_loaded_fl IS NULL;
						
			          v_count :='0';
			          v_initiate_input_str_IAFG :=NULL;
			          v_control_input_str_IAFG :=NULL;
			          v_verify_input_str_IAFG :=NULL;
			          v_txn_id := NULL;
			          --
			          DELETE FROM MY_TEMP_KEY_VALUE;
          
     	   END IF;  
         
		
			
       	COMMIT;
       	
    EXCEPTION WHEN OTHERS then
    	  DBMS_OUTPUT.PUT_LINE('Loose Item IAFG ERROR for part '||v_part_no || ' Error details: '||SQLERRM);
    ROLLBACK;
 
  END; 
END tmp_gm_loose_item_iafg;


 /***********************************************************************************
    *Description : Procedure to load field sales warehouse
    **************************************************************************************/
PROCEDURE tmp_gm_fswh_load
(
 p_dist in t701_distributor.c701_distributor_id%TYPE,
 p_rep_id IN    t703_sales_rep.c703_sales_rep_id%TYPE,
 p_acc_id IN    t704_account.c704_account_id%type,
 p_team_id IN ity_loose_inv_stg.team_id%TYPE,
 p_opt IN VARCHAR2

)


AS

    v_user t101_user.c101_user_id%TYPE;
    v_input_str_item_consign CLOB;
    v_control_str_item_consign CLOB;
    v_part_no t205_part_number.c205_part_number_id%TYPE;
    v_company_id          t1900_company.C1900_COMPANY_ID%TYPE;
    v_plant_id            t5040_plant_master.C5040_PLANT_ID%TYPE;
    v_portal_com_date_fmt VARCHAR2 (20) ;
    v_qty                 NUMBER;
    v_dist_id t701_distributor.C701_DISTRIBUTOR_ID%TYPE;
    v_lot_number       VARCHAR2 (40) ;
    v_txntype          NUMBER;
    v_ship_date        VARCHAR2 (100) ;
    v_out_cn_id        VARCHAR2 (20) ;
    v_req_id           VARCHAR2 (20) ;
    v_strTxnType         NUMBER        := '4110';
    v_strRelVerFl        VARCHAR2 (10) := 'on';
    v_strLocType         NUMBER        := '93343';
    v_shipid           NUMBER;
    v_shipflag         VARCHAR2 (10) ;
    v_fg_location_id t5052_location_master.C5052_LOCATION_ID%TYPE;
    v_count      NUMBER := 0;
    v_count_item NUMBER := 0;
    v_pkey number;
    v_ship_to NUMBER;
    v_ship_to_id NUMBER;
    --
    v_ship_out_user NUMBER := 303137; -- John Davidar        
	
	   
    CURSOR cur_item_initiate
    IS
         SELECT ITY.C205_part_number_id PART_NO, ITY.PHYSICAL_QTY QTY, ITY.lot_number LOT_NUMBER, ITY.pkey pkey
         FROM ity_loose_inv_stg ITY
         WHERE  TEAM_ID= p_team_id
		 AND ITY.loose_item_type = p_opt
		 AND ITY.physical_qty > 0
		AND ITY.item_loaded_fl='IAFG_PROCESSED'
         ORDER BY C205_part_number_id;
BEGIN
	  	v_user := IT_PKG_CM_USER.GET_VALID_USER;
        SELECT get_compdtfmt_frm_cntx () INTO v_portal_com_date_fmt FROM dual;
        SELECT get_plantid_frm_cntx INTO v_plant_id FROM dual;
        SELECT get_compid_frm_cntx () INTO v_company_id FROM dual;
    -- Create Location for FG and BL
    BEGIN
         SELECT c5052_location_id
         INTO v_fg_location_id
         FROM t5052_location_master
         WHERE c5052_location_cd = '011-B-004-B-4'
         AND c5040_plant_id= v_plant_id
         AND c5052_void_fl is null;
    
    EXCEPTION
	
		WHEN OTHERS THEN
        dbms_output.put_line ('*********Location error fiekd sales warehouse ' || ' Error:' || sqlerrm) ;
    END;
    
    BEGIN
	    v_user := IT_PKG_CM_USER.GET_VALID_USER;
    
	     IF(p_opt ='ACCOUNT') THEN
			v_ship_to   := '4122';  -- Account
			v_ship_to_id := p_acc_id;
	  ELSIF(p_opt ='REP') THEN
			v_ship_to   := '4121'; -- sales rep
			v_ship_to_id := p_rep_id;
	   ELSIF(p_opt ='INHOUSE') THEN
			v_ship_to   := '4120'; -- Distributor
			v_ship_to_id := p_dist;
	  END IF;
	    dbms_output.put_line ('Dist id '||p_dist||'  Rep id  '||p_rep_id||'  Acc id '|| p_acc_id);
       v_dist_id := p_dist;
			
				
				   FOR inv_item_consign IN cur_item_initiate
				
				     LOOP
						v_qty        := inv_item_consign.QTY;
						v_part_no    := inv_item_consign.PART_NO;
						v_lot_number := inv_item_consign.LOT_NUMBER;
						v_pkey 		 := inv_item_consign.pkey;
         
                    INSERT INTO MY_TEMP_KEY_VALUE_loose_item(MY_TEMP_TXN_ID,MY_TEMP_TXN_KEY) values(v_pkey,v_part_no);
                 
					v_input_str_item_consign   := v_input_str_item_consign|| v_part_no||'^'||v_qty||'^'||v_ship_date||'^|';
					v_control_str_item_consign := v_control_str_item_consign||v_part_no||','||v_qty||','||v_lot_number||','||v_fg_location_id||','||'TBE'||','||'90800'||'|';
                
					v_count_item                 := v_count_item + 1;
                
                IF (v_count_item >= 20) THEN
                
                  	  v_input_str_item_consign := v_input_str_item_consign||'~5001^5004|';
                    
                   	   gm_pkg_op_process_prod_request.gm_sav_initiate_item (NULL, CURRENT_DATE, 50618, NULL, 40021, v_dist_id, 50625, V_USER, 4120, NULL, NULL, 15, V_USER, v_input_str_item_consign, 50060, '',CURRENT_DATE, v_req_id, v_out_cn_id, NULL) ;
                     
                   	   dbms_output.put_line('v_out_cn_id :'||v_out_cn_id);
                 	   gm_pkg_cm_shipping_trans.gm_sav_shipping (v_req_id, '50184', '4120', v_dist_id, '5001', '5004', NULL,'0', NULL, v_ship_out_user, v_shipid, NULL, NULL);
                    -- controlling the Item consignment
                    	gm_pkg_op_item_control_txn.gm_sav_item_control_main (v_out_cn_id, v_strTxnType, v_strRelVerFl,v_control_str_item_consign, v_strLocType, V_USER);
                    	 dbms_output.put_line('After Controlled :');
                    	gm_pkg_cm_shipping_trans.gm_sav_shipout (v_out_cn_id, '50181', v_ship_to, v_ship_to_id, '5001', '5004',v_out_cn_id|| ':FSWH', 0, 0, v_ship_out_user, 0, v_shipid, NULL, v_shipflag);
                    	 dbms_output.put_line('Shipped :');
                    	v_count_item               := 0;
                    	v_input_str_item_consign   := NULL;
                    	v_control_str_item_consign := NULL;
                    	v_shipid                   := NULL;
                        
                    	UPDATE t504_consignment SET C504_LAST_UPDATED_BY = v_user, C504_LAST_UPDATED_DATE = CURRENT_DATE, c504_comments ='FSWH inventory update for the distributor: '||v_dist_id
                      	WHERE c504_consignment_id = v_out_cn_id;
                      
                        UPDATE MY_TEMP_KEY_VALUE_loose_item SET    MY_TEMP_TXN_VALUE= v_out_cn_id  WHERE MY_TEMP_TXN_VALUE IS NULL;
                    	
                      	dbms_output.put_line ('FSWH inventory update for the Distributor '||v_dist_id);
                END IF;
            END LOOP;
            
            IF (v_input_str_item_consign IS NOT NULL) THEN
            
                v_input_str_item_consign := v_input_str_item_consign||'~5001^5004|';
                  dbms_output.put_line ('v_input_str_item_consign'||v_input_str_item_consign);
                gm_pkg_op_process_prod_request.gm_sav_initiate_item (NULL, CURRENT_DATE, 50618, NULL, 40021, v_dist_id,50625, V_USER, 4120, NULL, NULL, 15, V_USER, v_input_str_item_consign, 50060, '', CURRENT_DATE,v_req_id, v_out_cn_id, NULL) ;
              		dbms_output.put_line('v_out_cn_id :'||v_out_cn_id);      
                gm_pkg_cm_shipping_trans.gm_sav_shipping (v_req_id, '50184', '4120', v_dist_id, '5001', '5004', NULL, '0', NULL, v_ship_out_user, v_shipid, NULL, NULL) ;
                --dbms_output.put_line ('sHIPPED');
                -- controlling the Item consignment
                gm_pkg_op_item_control_txn.gm_sav_item_control_main (v_out_cn_id, v_strTxnType, v_strRelVerFl,v_control_str_item_consign, v_strLocType, V_USER) ;
               -- dbms_output.put_line ('cONTROL');
                gm_pkg_cm_shipping_trans.gm_sav_shipout (v_out_cn_id, '50181', v_ship_to, v_ship_to_id, '5001', '5004',v_out_cn_id|| ':FSWH', 0, 0, v_ship_out_user, 0, v_shipid, NULL, v_shipflag) ;
                
                 UPDATE t504_consignment
                 SET C504_LAST_UPDATED_BY = v_user, C504_LAST_UPDATED_DATE = CURRENT_DATE, c504_comments ='FSWH inventory update for the distributor: '||v_dist_id   WHERE c504_consignment_id = v_out_cn_id;
                  
                 UPDATE MY_TEMP_KEY_VALUE_loose_item SET MY_TEMP_TXN_VALUE= v_out_cn_id  WHERE MY_TEMP_TXN_VALUE IS NULL;
                    
                 dbms_output.put_line ('FSWH inventory updated for the Distributor: '||v_dist_id);
                 
                 
                 v_dist_id                  := NULL;
                 v_count_item               := 0;
                 v_input_str_item_consign   := NULL;
                 v_control_str_item_consign := NULL;
                 v_shipid                   := NULL;
             
			END IF;
                 
                 UPDATE ity_loose_inv_stg T1 SET item_loaded_fl='FSWH Initiated',T1.NEW_CN_ID = (SELECT T2.MY_TEMP_TXN_VALUE FROM MY_TEMP_KEY_VALUE_loose_item T2 WHERE T2.MY_TEMP_TXN_ID = T1.PKEY)
			     WHERE T1.PKEY IN (SELECT T2.MY_TEMP_TXN_ID FROM MY_TEMP_KEY_VALUE_loose_item T2 WHERE T2.MY_TEMP_TXN_ID = T1.PKEY)
			     AND item_loaded_fl='IAFG_PROCESSED'
			     AND TEAM_ID= p_team_id
			     AND LOOSE_ITEM_TYPE=P_OPT;
                 
	      
		COMMIT;
	  	   		 	   
		    EXCEPTION WHEN OTHERS then
		    
		    v_dist_id                  := NULL;
            v_count_item               := 0;
            v_input_str_item_consign   := NULL;
            v_control_str_item_consign := NULL;
            v_shipid                   := NULL;
            
            DBMS_OUTPUT.PUT_LINE('FSWH inventory error '||v_part_no||SQLERRM);
           ROLLBACK;
   	      end;
   	      
    

END tmp_gm_fswh_load;

/***********************************************************************************
    *Description : Procedure to validate all sets details 
 **************************************************************************************/
PROCEDURE tmp_set_validation
AS
 v_set_validation_flag varchar2(10);
 v_dist_id t701_distributor.c701_distributor_id%TYPE;
 v_rep_id  t703_sales_rep.c703_sales_rep_id%TYPE;
 v_acc_id  t704_account.c704_account_id%TYPE;


CURSOR cur_set IS

	 SELECT DISTINCT (C207_SET_ID) sets_id, C5010_tag_id tag_id,TEAM_SYSTEM_ID TEAM_ID,
        TYPE type
        FROM globus_app.ITALY_SET_LOAD_STG ITY
        WHERE 
        ITY.set_loaded_fl IS NULL
        and ITY.c207_set_id IS NOT NULL
        order by C207_SET_ID, C5010_tag_id;

BEGIN
	 FOR set_list IN cur_set
	
    LOOP
	   
		-- Added to insert new tag id and update taggable part flag as Y
		    tmp_gm_tag_load(set_list.tag_id,set_list.sets_id);
			
			tmp_gm_set_validation(set_list.sets_id, set_list.tag_id,set_list.TEAM_ID,set_list.type,v_set_validation_flag,v_dist_id,v_rep_id,v_acc_id);
			
			COMMIT; 	
	 END LOOP;	
	
END tmp_set_validation;

END tmp_gm_inventory_load_italy;  
/  
  