create or replace
PACKAGE BODY STL_TMP_GM_INVENTORY_LOAD

IS
  
 /*
* Purpose: Main procedure to be invoked for the Loaner Set building data load migration
* Steps:
*  1. Set Config Qty validation
*  2. IABL initiation
*  3. Set Initiation and Set building
*  4. Loaner Initiate
*  Atec 403 - Set List Data Load Migration. July 2017
*  Author: mahavishnu. Reviewed by Rajesh
*/

PROCEDURE tmp_gm_main_stl_set_field
AS
   -- p_flag t5054_inv_location_log.C5054_LOT_STATUS_FL%TYPE := 'Y';
 
      v_commit_count NUMBER :='0';
	  v_set_validation_flag varchar2(10);
	  p_out_consign varchar2(100);
	  v_err_dtls clob;
	  v_out_request_id  T520_Request.C520_REQUEST_ID%TYPE;

    CURSOR cur_set
    IS
        SELECT DISTINCT (C207_SET_ID) sets_id, C5010_tag_id tag_id,SOURCE_ACCOUNT ACCT_NM,DISTRIBUTOR_NAME DIST_NM
        FROM globus_app.STL_LOANER_INV_STG STL
        WHERE 
        STL.set_loaded_fl IS NULL
        and STL.c207_set_id IS NOT NULL
        and STL.SOURCE_ACCOUNT is not null
        and STL.type = 'Loaner'
        order by C207_SET_ID, C5010_tag_id;
      
       
 BEGIN   
    FOR set_list IN cur_set
	
    LOOP
	    BEGIN 
			tmp_gm_set_validation(set_list.sets_id, set_list.tag_id,set_list.ACCT_NM,set_list.DIST_NM,'Loaner',v_set_validation_flag);
		
		IF (v_set_validation_flag='PASS') THEN
				
	            tmp_gm_iabl (set_list.sets_id, set_list.tag_id,'Loaner');
	            tmp_gm_set_build (set_list.sets_id, set_list.tag_id,'Loaner',NULL,p_out_consign,v_out_request_id);	
	            tmp_gm_Loaner_Initiate (set_list.sets_id, set_list.ACCT_NM, set_list.tag_id,p_out_consign);
	                      
                
                    UPDATE STL_LOANER_INV_STG
                    SET set_loaded_fl    = 'LOANER INITIATED'
                    WHERE c207_SET_ID  = set_list.sets_id
                    AND type = 'Loaner'
                    AND c5010_tag_id = set_list.tag_id;           
        END IF;
    
   			 EXCEPTION
    
   				 WHEN OTHERS THEN

   				   dbms_output.put_line ('Set Build Processing failed for the set:'||set_list.sets_id||'And Tag ID:'||
	    		    set_list.tag_id||SQLERRM||chr(10)||dbms_utility.format_error_backtrace);
	    		    Rollback;  
        
	            UPDATE STL_LOANER_INV_STG
	            SET set_loaded_fl    = 'Set Build Processing failed'
	              WHERE c207_SET_ID  = set_list.sets_id
	               AND type = 'Loaner'
	                AND c5010_tag_id = set_list.tag_id;
	             
	                          
		          v_err_dtls :='Set Build Processing failed: Error message: '||SQLERRM||chr(10)||dbms_utility.format_error_backtrace;
	              INSERT INTO STL_LOANER_STATUS_LOG(C207_SET_ID,C5010_TAG_ID,ERR_DTLS) VALUES(set_list.sets_id,set_list.tag_id,v_err_dtls);
	           	  commit;
     
    END;
    /* 
     v_commit_count    := v_commit_count + 1;
        IF (v_commit_count = 100) THEN
        
          END IF;
          v_commit_count:='0';
           
    */
             
   COMMIT;   
		
 END LOOP;
END tmp_gm_main_stl_set_field;

  
  
  
  /*
* Purpose: Main procedure to be invoked for the Loaner Set which are in warehouse.
* Purpose: Main procedure to be invoked for the Loaner Set which are in warehouse.
* Steps:
*  1. Set Config Qty validation
*  2. IABL initiation
*  3. Set Initiation and Set building
*  Atec 403 - Set List Data Load Migration. July 2017
*  Author: mahavishnu. Reviewed by Rajesh
*/

PROCEDURE tmp_gm_main_stl_set_build

AS

	  v_set_validation_flag VARCHAR2(10);
      v_commit_count NUMBER :='0';
      p_out_consign varchar2(100);
      v_err_dtls clob;
      v_out_request_id  T520_Request.C520_REQUEST_ID%TYPE;
   
    CURSOR cur_set
    IS

        SELECT DISTINCT (C207_SET_ID) sets_id, C5010_tag_id tag_id
        FROM globus_app.STL_LOANER_INV_STG STL
        WHERE
        STL.SET_LOADED_FL is null
        AND DISTRIBUTOR_NAME IS NULL
        order by C207_SET_ID, C5010_tag_id;
             
       BEGIN 
      
        
    FOR set_list IN cur_set
    
    LOOP
       Begin
	       
	    	tmp_gm_set_validation(set_list.sets_id, set_list.tag_id,null,null,'Consignment',v_set_validation_flag);

	    IF (v_set_validation_flag='PASS') THEN
	       
            tmp_gm_iabl (set_list.sets_id, set_list.tag_id,'Consignment');
            tmp_gm_set_build (set_list.sets_id, set_list.tag_id,'Consignment',NULL, p_out_consign,v_out_request_id);
            dbms_output.put_line ('Set Build Processing competed for the Tag:'||set_list.tag_id);
                
            UPDATE STL_LOANER_INV_STG
            SET set_loaded_fl    = 'Set Building Process completed.'
            WHERE c207_SET_ID  = set_list.sets_id
            AND c5010_tag_id = set_list.tag_id;
       	 END IF;         
                
	    EXCEPTION
	    
	    WHEN OTHERS THEN
	    
	        dbms_output.put_line ('Set Build Processing failed for the set:'||set_list.sets_id||'And Tag ID:'||
	        set_list.tag_id||SQLERRM||chr(10)||dbms_utility.format_error_backtrace);
	         ROLLBACK;
	         
	         UPDATE STL_LOANER_INV_STG
              SET set_loaded_fl    = 'Set Build Processing failed'
              WHERE c207_SET_ID  = set_list.sets_id
              AND c5010_tag_id = set_list.tag_id;
             
              
	          v_err_dtls :='Set Build Processing failed: Error message: '||SQLERRM||chr(10)||dbms_utility.format_error_backtrace;
              INSERT INTO Stl_Loaner_Status_Log(C207_SET_ID,C5010_TAG_ID,ERR_DTLS) VALUES(set_list.sets_id,set_list.tag_id,v_err_dtls);
           	  commit;
	    END;
	     
       COMMIT;
        
  END LOOP;
END tmp_gm_main_stl_set_build;
  
 

/************************************************
*-- The purpose of this procedure is to validate the following pre-requiste to avoid Set building failure
*-- Account Availablity check
*-- Set Type check
*-- Tag Status check      
*-- Tag Attribute check 
*-- Part number check
*-- Author: Mahavishnu. Verified by Rajesh V.
*************************************************/

	PROCEDURE tmp_gm_set_validation
  	   (
        p_set_id  IN t208_set_details.C207_SET_ID%TYPE,
        p_tag_id  IN t5010_tag.C5010_TAG_ID%TYPE,
        p_acct_nm IN t704_account.C704_ACCOUNT_NM_EN%TYPE,
        p_dist_nm IN t701_DISTRIBUTOR.C701_DISTRIBUTOR_NAME_EN%TYPE,
        p_type    IN VARCHAR2,
        p_set_validation_flag OUT VARCHAR
		)
    AS
        v_part_num T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE;
        v_source_QTY NUMBER;
        v_set_id T207_SET_MASTER.C207_SET_ID%TYPE;
        V_SET_QTY NUMBER;
        V_SET_PART  T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE;
        v_set_part_num T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE;
        v_temp_acc t704_account.c704_account_id%TYPE;
        v_err_dtls  CLOB; -- varchar2(10000);
        v_set_type T207_SET_MASTER.C207_TYPE%TYPE;
        v_flag VARCHAR2(100);
        v_tag_part_inp_str clob :=null;
        v_tag_count number:=0;
        v_company_id t1900_company.c1900_company_id%TYPE;
        v_plant_id T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
		v_count_num number:=0;
		v_tag_status VARCHAR2(100);
        v_part_number_count NUMBER:=0;
        v_set_count number:=0;
        v_tag_check number:=0;
        v_taggable_cnt NUMBER;
        v_tag_fl    varchar2(10);
        
        CURSOR cur_source_set
        IS
 SELECT C205_PART_NUMBER_ID PART_NO
   FROM STL_LOANER_INV_STG
  WHERE C207_SET_ID=p_set_id
AND C5010_TAG_ID=p_tag_id
GROUP BY C205_PART_NUMBER_ID
MINUS
SELECT t2023.C205_PART_NUMBER_ID PART_NO
FROM T2023_PART_COMPANY_MAPPING t2023
where t2023.C1900_COMPANY_ID = v_company_id
AND t2023.C2023_VOID_FL IS NULL;	   

-- part price check
CURSOR part_price_dtls_cur
IS
select C205_PART_NUMBER_ID pnum from STL_LOANER_INV_STG where C207_SET_ID=p_set_id
AND C5010_TAG_ID=p_tag_id 
GROUP BY C205_PART_NUMBER_ID
minus
select c205_part_number_id pnum from T2052_PART_PRICE_MAPPING where C1900_COMPANY_ID = v_company_id and C2052_VOID_FL IS NULL;

  -- taggable part flag
CURSOR tag_cur
IS
select C205_PART_NUMBER_ID pnum,C5010_TAG_ID tagid,C207_SET_ID setid from STL_LOANER_INV_STG where C207_SET_ID=p_set_id
AND C5010_TAG_ID=p_tag_id 
GROUP BY C205_PART_NUMBER_ID,C5010_TAG_ID,C207_SET_ID;       

    BEGIN
	    
        p_set_validation_flag :='PASS';
        
        
  		  SELECT get_plantid_frm_cntx  INTO v_plant_id FROM dual;
  		  SELECT get_compid_frm_cntx() INTO v_company_id FROM dual;
                 
      --- Account Availablity check 
           
        IF (p_acct_nm is not null) THEN

         BEGIN
	    
	          SELECT count(1) INTO v_count_num
	          FROM t704_account 
	          WHERE Upper(C704_ACCOUNT_NM_EN)=TRIM(Upper(p_acct_nm))
	          and c704_void_fl is null
	          and c1900_company_id=v_company_id;
	          
	       IF (v_count_num='0') THEN
	          	v_err_dtls :='SpineIT Account ID was not created for the  Acct : '||p_acct_nm;
	          
	         	 INSERT INTO STL_LOANER_STATUS_LOG(C207_SET_ID,C5010_TAG_ID,SOURCE_ACCT,ERR_DTLS)
	         	 VALUES(p_set_id,p_tag_id,p_acct_nm,v_err_dtls);
	          	 p_set_validation_flag :='FAIL';
	          
	       END IF;
	      
	     END;
	 END IF;
	 
	  IF (p_dist_nm is not null) THEN

         BEGIN
	    
	          SELECT count(1) INTO v_count_num
	          FROM t701_DISTRIBUTOR 
	          WHERE Upper(C701_DISTRIBUTOR_NAME_EN)=TRIM(Upper(p_dist_nm))
	          and c701_void_fl is null
	          and c1900_company_id=v_company_id;
	          
	       IF (v_count_num='0') THEN
	          	v_err_dtls :='SpineIT Distributor ID was not created for the  Dist : '||p_dist_nm;
	          
	         	 INSERT INTO STL_LOANER_STATUS_LOG(C207_SET_ID,C5010_TAG_ID,SOURCE_ACCT,ERR_DTLS)
	         	 VALUES(p_set_id,p_tag_id,p_dist_nm,v_err_dtls);
	          	 p_set_validation_flag :='FAIL';
	          
	       END IF;
	      
	     END;
	 END IF;
     
	   -- Set ID check   
	 
	     BEGIN
	         
		      SELECT count(1) into v_set_count
	   		  FROM T207_SET_MASTER
	   		  WHERE C207_SET_ID=p_set_id
	   		  and c207_void_FL IS NULL;
	          
	   		
	          IF (v_set_count=0) THEN
	          
		          v_err_dtls :='Set ID Not Found in the Set Master Table : '||p_set_id;
		          
		          INSERT INTO STL_LOANER_STATUS_LOG(C207_SET_ID,C5010_TAG_ID,ERR_DTLS) VALUES(p_set_id,p_tag_id,v_err_dtls);
		          p_set_validation_flag :='FAIL';
		          
	          
	           -- Set Type Check
   
	         ELSE
	         
	   		  SELECT NVL(C207_TYPE,'-999') INTO v_set_type FROM
	   		  T207_SET_MASTER
	   		  WHERE C207_SET_ID=p_set_id
	   		  and c207_void_FL IS NULL;
	          
	   		  -- 4074 : Loaner 
	          IF (v_set_type NOT IN ('4074') AND p_type='Loaner') THEN
	          
		          v_err_dtls :='Set Type is not a Loaner . Please change the set type to 4074 (Loaner Set) for the Set ID : '||p_set_id;
		          
		          INSERT INTO STL_LOANER_STATUS_LOG(C207_SET_ID,C5010_TAG_ID,ERR_DTLS) VALUES(p_set_id,p_tag_id,v_err_dtls);
		          p_set_validation_flag :='FAIL';
		          
	          END IF;
	          
	          IF (v_set_type NOT IN ('4070') AND p_type='Consignment') THEN
	          
		          v_err_dtls :='Set Type is not a Consignment . Please change the set type to 4070 (Consignment Set) for the Set ID : '||p_set_id;
		          
		          INSERT INTO STL_LOANER_STATUS_LOG(C207_SET_ID,C5010_TAG_ID,ERR_DTLS) VALUES(p_set_id,p_tag_id,v_err_dtls);
		          p_set_validation_flag :='FAIL';
		          
	          END IF;
	        
	         END IF;
	        
	      END;    
	          
	  --- Tag  check 
	      BEGIN	      
	   		  SELECT count(1) INTO v_tag_check
	   		  FROM T5010_TAG 
	   		  WHERE C5010_TAG_ID=p_tag_id
	   		  and c5010_void_FL is null;
	          
	          IF (v_tag_check='0') THEN
	          	
	         	 	INSERT INTO t5010_tag(c5010_tag_id, c901_status, c5010_created_by, c5010_created_date, c5010_history_fl)  
	   				VALUES (p_tag_id,'51011','839135',current_date,'Y');
	           	
      		 ELSE
	      
	 			--- Tag Status check 
         	      
	   		  SELECT NVL(c901_status,'-999') INTO v_tag_status
	   		  FROM T5010_TAG 
	   		  WHERE C5010_TAG_ID=p_tag_id
	   		  and c5010_void_FL is null;
	          
	          IF (v_tag_status NOT IN ('51011','51012')) THEN
	          
	          v_err_dtls :='Tag Status is not in released or in Active status for the Tag id : '||p_tag_id;
	          
              INSERT INTO STL_LOANER_STATUS_LOG(C207_SET_ID,C5010_TAG_ID,ERR_DTLS) VALUES(p_set_id,p_tag_id,v_err_dtls);
	          p_set_validation_flag :='FAIL';
	           
	           END IF;
	      END IF;
	   END; 
          
	      
         BEGIN   	 
           
           FOR cur_set IN cur_source_set
             LOOP
              
                  V_PART_NUM      := cur_set.PART_NO;
                  
                  
                  v_err_dtls :='Part number: '||V_PART_NUM||' not found in Part company mapping table. Set ID: '||p_set_id||' TagID '||p_tag_id;
                  INSERT INTO STL_LOANER_STATUS_LOG(C207_SET_ID,C5010_TAG_ID,ERR_DTLS) VALUES(p_set_id,p_tag_id,v_err_dtls);
                  p_set_validation_flag :='FAIL';
                  
                 
             END LOOP;
           END;
           --taggable part flag
           
         FOR tag_fl_det IN tag_cur
         LOOP
         
         	v_tag_fl:= get_part_attribute_value(tag_fl_det.pnum,'92340');
         	
         	IF v_tag_fl='Y' THEN
         	
         		UPDATE STL_LOANER_INV_STG SET TAGGABLE_PART='Y' WHERE C205_PART_NUMBER_ID=tag_fl_det.pnum
         		AND C207_SET_ID=tag_fl_det.setid and C5010_TAG_ID=tag_fl_det.tagid;
         	
         	END IF;
         
         END LOOP;
         
         -- part price check
	    FOR part_price_dtls IN part_price_dtls_cur
	    LOOP
	    	
	    --
	    v_err_dtls :='Part # price not available : '||part_price_dtls.pnum ||' Set ID: '||p_set_id||' TagID '||p_tag_id;
	    
	      INSERT INTO STL_LOANER_STATUS_LOG(C207_SET_ID,C5010_TAG_ID,ERR_DTLS) VALUES(p_set_id,p_tag_id,v_err_dtls);
          p_set_validation_flag :='FAIL';
	    
	    END LOOP; 
	     -- taggable part check
	     SELECT count(1) INTO v_tag_count from  STL_LOANER_INV_STG
	     WHERE c207_SET_ID  = p_set_id
				AND   c5010_tag_id = p_tag_id
				AND TAGGABLE_PART = 'Y';
		
		 IF v_tag_count = 0
         THEN
         	 v_err_dtls :='No tagabble part associated with the Set for tag : '||p_tag_id||' , the Setid:  '||p_set_id;
              INSERT INTO STL_LOANER_STATUS_LOG(C207_SET_ID,C5010_TAG_ID,ERR_DTLS) VALUES(p_set_id,p_tag_id,v_err_dtls);
              p_set_validation_flag :='FAIL';
         END IF;
         
         IF (v_tag_count > 1) THEN
         
         	  v_err_dtls :='More than 1 tagabble part associated with the Set in the Tag Attribute for tag : '||p_tag_id||' , the Setid:  '||p_set_id||' Taggable parts are '||v_tag_part_inp_str;
              INSERT INTO STL_LOANER_STATUS_LOG(C207_SET_ID,C5010_TAG_ID,ERR_DTLS) VALUES(p_set_id,p_tag_id,v_err_dtls);
              p_set_validation_flag :='FAIL';
	           
         END IF;
         
         IF (p_set_validation_flag ='FAIL') THEN 
         
            	UPDATE STL_LOANER_INV_STG
				SET set_loaded_fl    = 'Record Skipped. Check STL_LOANER_STATUS_LOG table for details'
				WHERE c207_SET_ID  = p_set_id
				AND   c5010_tag_id = p_tag_id; 
	      
		 END IF;
	DBMS_OUTPUT.PUT_LINE (' Set ID  '|| p_set_id || ' TAG ID '|| p_tag_id ||' Valid Status '||p_set_validation_flag);
		 
    END tmp_gm_set_validation;
 
  
 
/*
 * Purpose: Procedure to invoke the IABL for the Loaner set list data load migraion
 *  Data load Migration - Steel
 * July 2017. Author mahavishnu, Reviewed by Rajesh
 */
PROCEDURE tmp_gm_iabl
		(
        p_set_id IN t208_set_details.C207_SET_ID%TYPE,
        p_tag_id IN t5010_tag.C5010_TAG_ID%TYPE,
        p_type   IN VARCHAR2
		)

AS
  v_user t101_user.c101_user_id%TYPE;
  v_initiate_input_str CLOB;
  v_control_input_str CLOB;
  v_verify_input_str CLOB;
  v_part_no t205_part_number.c205_part_number_id%TYPE;
  v_txn_id t504_consignment.c504_consignment_id%TYPE;
  v_cost t820_costing.c820_purchase_amt%TYPE;
  v_purchase_amt t820_costing.c820_purchase_amt%TYPE;
  v_company_cost  t820_costing.C820_LOCAL_COMPANY_COST%TYPE;
  v_message varchar2(20);
  v_reason NUMBER;
  v_txntype NUMBER;
  v_wh_type VARCHAR2(100);
  v_cur_wh  VARCHAR2(50); 
  v_txn_type_name varchar2(50);
  v_source_wh NUMBER;
  v_target_wh NUMBER;
  v_out VARCHAR2(100);
  v_costing_id NUMBER;
  v_company_id NUMBER;
  v_plant_id NUMBER;
  v_portal_com_date_fmt VARCHAR2 (20) ;
  v_bl_location_id t5052_location_master.C5052_LOCATION_ID%TYPE;
  V_TRIP_PRICE NUMBER;
  v_qty NUMBER;
  v_control_num VARCHAR2(40);
  v_costing_type NUMBER;
  v_count number :=0;
  v_part_company number;
 
CURSOR cur_set IS

	  SELECT STL.c205_part_number_id PART_NO, STL.LOT_NUMBER CONTROL_NUMBER,
	  SUM(STL.physical_set_qty) QTY
	  FROM STL_LOANER_INV_STG STL
	  WHERE STL.c207_SET_ID=p_set_id
	  AND STL.c5010_tag_id=p_tag_id
	  AND STL.set_loaded_fl IS NULL
	  AND STL.physical_set_qty > 0
	  AND STL.TYPE=p_type
	  GROUP BY STL.c205_part_number_id , STL.LOT_NUMBER 
	  ;
 
BEGIN

-- 105131 -- dd/mm/yyyy
  SELECT get_compdtfmt_frm_cntx () INTO v_portal_com_date_fmt FROM dual;
  SELECT get_plantid_frm_cntx  INTO v_plant_id FROM dual;
  SELECT get_compid_frm_cntx() INTO v_company_id FROM dual;
  -- Below line of code will get the user who is executing the script. This will avoid the need to pass the user id as a parameter
  v_user := IT_PKG_CM_USER.GET_VALID_USER;
  

--  -- Create Location for BL
--  
--  BEGIN
--	  
--    SELECT c5052_location_id INTO v_bl_location_id
--    FROM t5052_location_master WHERE c5052_location_cd='STL_BL_LOCATION'and c1900_company_id= v_company_id and c5052_void_fl is null;
--    
--    EXCEPTION WHEN OTHERS THEN
--    dbms_output.put_line('*********Location error' || ' Error:' || sqlerrm);
--    
--  END;
  
FOR  inv_set IN cur_set

  LOOP
  
   BEGIN
	   
      v_part_no :=inv_set.part_no;
      v_qty := inv_set.qty;
      v_control_num :=inv_set.control_number;
      v_txn_type_name:='INVAD-BULK';
      v_txntype:= '400080';
      v_target_wh:='90814';
      v_source_wh:='90814';
      v_cur_WH := 'BL';
      v_wh_type:='90814';
      v_costing_type  :='4900';
      v_reason :='90814';
    
 BEGIN
	    
     select COST, company_cost INTO v_purchase_amt, v_company_cost 
     from STL_DM_INVENTORY 
     where part=v_part_no 
     and warehouse='FG';     
     Exception
       WHEN NO_DATA_FOUND THEN
       v_purchase_amt:='0';
       v_company_cost :='0';
 --   dbms_output.put_line('v_purchase_amt=' || v_purchase_amt);

 END;
   	   
	   SELECT NVL(TRIM(GET_PART_PRICE(v_part_no,'E')),0) INTO V_TRIP_PRICE
	    FROM DUAL;
		 
   -- Get Inventory cost or insert if not available
   -- **Review Check in Costing table and not from the function ** --
    --  SELECT get_inventory_cogs_value(v_part_no)  INTO v_cost FROM dual;
    
      SELECT count(1) INTO v_cost FROM t820_costing
      WHERE c205_part_number_id=v_part_no
      AND c820_delete_fl = 'N'
      --and c901_status='4801'
      AND c1900_company_id =  v_company_id
      and c901_costing_type=4900;
      
     IF v_cost = 0 THEN
    -- to get the owner company id 
	SELECT C1900_COMPANY_ID INTO v_part_company FROM  t2023_part_company_mapping t2023
      WHERE t2023.c205_part_number_id=v_part_no
      and c901_part_mapping_type='105360';
      --
       INSERT INTO t820_costing
	   ( c820_costing_id,c205_part_number_id,c901_costing_type,c820_part_received_dt,c820_qty_received,c820_purchase_amt,c820_qty_on_hand 
    	,c901_status,c820_delete_fl,c820_created_by,c820_created_date,c1900_company_id,c5040_plant_id,C820_LOCAL_COMPANY_COST
		,C1900_OWNER_COMPANY_ID,c901_owner_currency,C901_LOCAL_COMPANY_CURRENCY       
		) 
	   VALUES ( s820_costing.nextval,v_part_no,v_costing_type,CURRENT_DATE,0,v_purchase_amt,0,'4802','N','2277820',CURRENT_DATE,v_company_id        
			    ,v_plant_id,v_company_cost,v_part_company,'1','4000917');
          
        -- Build input string 
        END IF;
        
        v_initiate_input_str := v_initiate_input_str || v_part_no || ',' || v_qty || ',,,' || V_TRIP_PRICE || '|';         
        v_control_input_str := v_control_input_str || v_part_no || ',' || v_qty || ',' || v_control_num || ',,' || V_TRIP_PRICE || '|';
        v_verify_input_str := NULL;
       
       
      IF v_txn_id IS NULL THEN
      
         SELECT get_next_consign_id(v_txn_type_name) 
         INTO v_txn_id
         FROM dual;
         
      END IF;

   
    	--gm_pkg_op_inv_field_sales_tran.gm_sav_fs_inv_loc_part_details (v_bl_location_id,v_txn_id, v_part_no,v_qty,'400080','4301',null,current_date, v_user);    
     END;
     
	v_count :=v_count+1;

		if(v_count>20) then 
			
				
		  -- IABL initiate 
		          gm_save_inhouse_item_consign(v_txn_id,v_txntype , v_reason ,0,'','IABL created for the Set ID:'||p_set_id||':Tag ID:'||p_tag_id,v_user,v_initiate_input_str,v_out);
		        dbms_output.put_line('IABL Txn=' || v_txn_id || ' Initiated inner ');
		  -- IABL Control 
		          gm_ls_upd_inloanercsg(v_txn_id,v_txntype,'0','0',NULL,'Steel Set Data Load:'||p_tag_id,v_user,v_control_input_str,null,'ReleaseControl',v_message);
		            dbms_output.put_line('IABL Txn=' || v_txn_id || ' Controlled inner');
		  -- IABL Verify
		  
		          gm_ls_upd_inloanercsg(v_txn_id,v_txntype,'0','0',NULL,'Steel Set Data load:'||p_tag_id,v_user,v_verify_input_str,null,'Verify',v_message);
		          dbms_output.put_line('IABL Txn=' || v_txn_id || ' Verified');
        
		   	     
		          UPDATE t412_inhouse_transactions SET c412_comments='IABL Inventory Update for Steel Set Building:'||p_tag_id , c412_last_updated_date=CURRENT_DATE, c412_last_updated_by=v_user
			 	  where c412_inhouse_trans_id=v_txn_id;
		        
		        
			      v_txn_id := NULL;
			      v_count :=0;
			      v_initiate_input_str :=null;
			      v_control_input_str :=null;
		END IF;

  END LOOP;
  

          -- Outer Loop
          
   IF (v_initiate_input_str is not null) THEN

	  
		   -- IABL initiate 
		          gm_save_inhouse_item_consign(v_txn_id,v_txntype , v_reason ,0,'','IABL created for the Set ID:'||p_set_id||':Tag ID:'||p_tag_id,v_user,v_initiate_input_str,v_out);
                   dbms_output.put_line('IABL Txn=' || v_txn_id || ' Initiated Outer ');
		      
		   -- IABL Control 
		          GM_LS_UPD_INLOANERCSG(v_txn_id,v_txntype,'0','0',NULL,'Steel Set Data Load:'||p_tag_id,v_user,v_control_input_str,null,'ReleaseControl',v_message);
		          dbms_output.put_line('IABL Txn=' || v_txn_id || ' Controlled outer');
		   -- IABL Verify
		  
		    	  gm_ls_upd_inloanercsg(v_txn_id,v_txntype,'0','0',NULL,'Steel Set Data load:'||p_tag_id,v_user,v_verify_input_str,null,'Verify',v_message);
		          dbms_output.put_line('IABL Txn=' || v_txn_id || ' Verified');
		        
	        
		          UPDATE t412_inhouse_transactions SET c412_comments='IABL Inventory Update for Steel Set Building:'||p_tag_id , c412_last_updated_date=CURRENT_DATE, c412_last_updated_by=v_user
				  where c412_inhouse_trans_id=v_txn_id;
				  v_txn_id := NULL;
			      v_count :=0;
			      v_initiate_input_str :=null;
				  v_control_input_str :=null;
			    
  END IF;

  END tmp_gm_iabl;    

  

 
 /*
 ************************************************
 * The purpose of this procedure is to Initiate a Loaner Set build for the given Set ID
 * Steel Set List Data migration. 
 * Author: Mahavishnu. Verified by Rajesh.
************************************************* */

	PROCEDURE tmp_gm_set_build
		(
        p_set_id IN t208_set_details.C207_SET_ID%TYPE,
        p_tag_id IN t5010_tag.C5010_TAG_ID%TYPE,
        p_type   IN VARCHAR2,
        p_dist_nm t701_DISTRIBUTOR.C701_DISTRIBUTOR_NAME_EN%TYPE,
        p_out_consign_id OUT t504_consignment.c504_consignment_id%TYPE,
        p_out_request_id OUT T520_Request.C520_REQUEST_ID%TYPE
		)
AS
	  v_user t101_user.c101_user_id%TYPE;
	  v_initiate_input_str_set_build CLOB;
	  v_release_input_str_set_build CLOB;
	  v_part_no t205_part_number.c205_part_number_id%TYPE;
	  v_tag_flag VARCHAR2 (2) ;
	  v_tag_part_number t205_part_number.c205_part_number_id%TYPE;
	  v_tag_control_number VARCHAR2(40);
	  v_input_str_tag_creation CLOB;
	  v_company_id t1900_company.C1900_COMPANY_ID%TYPE;
	  v_plant_id t5040_plant_master.C5040_PLANT_ID%TYPE;
	  v_portal_com_date_fmt VARCHAR2 (100) ;
	  v_qty NUMBER;
	  v_control_num VARCHAR2(40);
	  v_out_request_id VARCHAR2 (20) ;
	  v_out_new_request_id VARCHAR2 (20) ; 
	  v_out_consign_id VARCHAR2 (20) ;
	  v_errmsg VARCHAR2 (4000) ;
	  v_tagable_flag VARCHAR(2);
	  v_input_str_putaway CLOB;
	  v_msg varchar2(4000);
	  v_out_inhtxn_id  t412_inhouse_transactions.c412_inhouse_trans_id%TYPE;
	  --
	  v_etch_id t504a_consignment_loaner.c504a_etch_id%TYPE;
	  v_control_status  VARCHAR2 (20);
	  v_init_type   VARCHAR2 (20);
	  v_init_emp_type VARCHAR2 (20);
	  v_request_to VARCHAR2 (20);

v_tag_check  NUMBER;
CURSOR cur_set IS

	  SELECT STL.C205_PART_NUMBER_ID PART_NO, STL.LOT_NUMBER CONTROL_NUMBER,
	  STL.PHYSICAL_SET_QTY QTY, STL.TAGGABLE_PART TAG_FLAG
	  FROM STL_LOANER_INV_STG STL
	  WHERE STL.C207_SET_ID=p_set_id
	  AND STL.C5010_TAG_ID=p_tag_id
	  AND STL.TYPE= p_type
	  and STL.set_loaded_fl is null;
	 
	 
	BEGIN

	  SELECT get_compdtfmt_frm_cntx () INTO v_portal_com_date_fmt FROM dual;
	    SELECT get_compid_frm_cntx() INTO v_company_id FROM dual;
	  v_user := IT_PKG_CM_USER.GET_VALID_USER;  
	 	
	 		  SELECT count(1) INTO v_tag_check
	   		  FROM T5010_TAG 
	   		  WHERE C5010_TAG_ID=p_tag_id
	   		  and c5010_void_FL is null; 
	   		  
	   		  IF p_dist_nm IS NOT NULL THEN
		   		  SELECT C701_DISTRIBUTOR_ID INTO v_request_to
		          FROM t701_DISTRIBUTOR 
		          WHERE Upper(C701_DISTRIBUTOR_NAME_EN)=TRIM(Upper(p_dist_nm))
		          and c701_void_fl is null
		          and c1900_company_id=v_company_id;
	          END IF;
	   		
	          -- Added to set status for laoner and consignment
	          dbms_output.put_line('p_type<><><'||p_type);
	   		  IF   p_type='Loaner' THEN
		   		v_control_status:='1.20';
		   		v_init_type:='4127';
		   		v_init_emp_type:='50625';
		   		v_request_to:=NULL;
		   		ELSE
		   		v_control_status:='1.10';
		   		v_init_type:='40021';
		   		v_init_emp_type:='50626';
		   	  END IF;
	   		 dbms_output.put_line('v_request_to<><><'||v_request_to);
	   		 dbms_output.put_line('v_init_type<><><'||v_init_type);
	   		 dbms_output.put_line('v_init_emp_type<><><'||v_init_emp_type);
	   		
	FOR  inv_set IN cur_set
	 
	 LOOP
	  
	   BEGIN
		   
		   v_qty         :=inv_set.QTY;
		   v_part_no     :=inv_set.PART_NO;
		   v_control_num :=inv_set.CONTROL_NUMBER;   
					 
		 -- Build Input String for Set Build
		   v_initiate_input_str_set_build := v_initiate_input_str_set_build||v_part_no||'^'||v_qty||'^'||v_control_num||'^|';  
	   	   v_release_input_str_set_build  := v_release_input_str_set_build||v_part_no||','||v_qty||','||'90809|';
		  
		  v_tag_flag    := inv_set.TAG_FLAG;
			
		IF v_tag_flag         	  = 'Y' THEN
		
			v_tagable_flag 		 :='Y'; 
			v_tag_part_number    := v_part_no;
			v_tag_control_number := v_control_num;
				
		END IF;
		  
	  END;
		
	END LOOP;

	
			gm_pkg_op_process_request.gm_sav_initiate_set(NULL,to_char(CURRENT_DATE, v_portal_com_date_fmt),'50618',NULL,p_set_id,v_init_type,
			v_request_to,v_init_emp_type,v_user,'4120',NULL,NULL,'15',v_user,NULL,CURRENT_DATE,v_out_request_id,v_out_consign_id);
			
			p_out_request_id:=v_out_request_id;
			
			-- Added below code to release back order to consignment.
			gm_pkg_op_request_summary.gm_sav_reconfig_part(v_out_request_id,v_release_input_str_set_build,v_user,v_out_new_request_id,v_out_consign_id,v_out_inhtxn_id);
			dbms_output.put_line('v_out_consign_id<><><><><>'||v_out_consign_id);
			
	  IF (v_tagable_flag ='Y') THEN

			v_input_str_tag_creation:=p_tag_id||'^'||v_tag_control_number||'^'||v_tag_part_number||'^'||p_set_id||'^'||'51000^'||v_out_consign_id||'^'||'40033^|';
			gm_pkg_ac_tag_info.gm_sav_tag_detail(v_input_str_tag_creation,v_user,v_errmsg);

		 
		 ELSE
			dbms_output.put_line('The set ID**'||p_set_id||'and the tag id '||p_tag_id||' does not have a tagable part');
			
	  END IF;
		dbms_output.put_line('set build initiate ');
		-- Set build Processing Lot Number verification step
		
			gm_save_set_build(v_out_consign_id, v_initiate_input_str_set_build,'Steel Data Migration tag#'||p_tag_id, v_control_status, '0', v_user, v_errmsg,'LOANERPOOL');
		
		-- Moving the Set to Loaner Common Pool
			gm_save_set_build(v_out_consign_id, v_initiate_input_str_set_build,'Steel Data Migration tag#'||p_tag_id, '1.20', '1', v_user, v_errmsg,'LOANERPOOL');
			
		-- below call dor only loaner sets
			IF p_type='Loaner' THEN
				dbms_output.put_line('before process picture ' || v_out_consign_id);		
				
					gm_pkg_op_loaner.gm_sav_process_picture(v_out_consign_id,v_user,v_msg);
					
				dbms_output.put_line('After process picture ' || v_out_consign_id);
				
					gm_pkg_op_set_put_txn.gm_sav_set_put (p_tag_id,'080-J-001-J-1',v_user,v_out_consign_id);
				 
					dbms_output.put_line(v_out_consign_id||' : is Moved to Loaner Common Pool');
			END IF;
			
			p_out_consign_id :=v_out_consign_id;
			
			-- to get the etch id
			BEGIN
				SELECT etch_id
				   INTO v_etch_id
				   FROM STL_LOANER_INV_STG
				  WHERE c207_set_id  = p_set_id
				    AND c5010_tag_id = p_tag_id 
				      AND TYPE= p_type
				    group by etch_id;
			EXCEPTION    
   				 WHEN OTHERS THEN
   				v_etch_id := NULL;
   			END;	 
			--    
			UPDATE t504_consignment
			SET C504_LAST_UPDATED_BY=v_user, C504_LAST_UPDATED_DATE =CURRENT_DATE, c504_comments='Steel Data Migration tag: '||p_tag_id
			WHERE c504_consignment_id=v_out_consign_id;
			
			UPDATE t504a_consignment_loaner
			SET C504a_LAST_UPDATED_BY=v_user, C504a_LAST_UPDATED_DATE =CURRENT_DATE, c504a_etch_id= v_etch_id
			WHERE c504_consignment_id=v_out_consign_id;
		   
			update STL_LOANER_INV_STG
			set NEW_TXN_ID=v_out_consign_id	
			where c207_set_id=p_set_id
			 AND TYPE= p_type
			and c5010_tag_id=p_tag_id;
	    
			INSERT INTO STL_INV_LOAD_TXN_STATUS (tag_id,set_id,New_CN_ID, Comments) VALUES(p_set_id,p_tag_id,v_out_consign_id,'Set Building Completed. Loaner is in Available Status');
	 
END tmp_gm_set_build;     

/*
 ************************************************
 * The purpose of this procedure is to Initiate a Loaner Set for the given Set ID
 * Steel Loaner Set List Data migration. 
 * Author: Mahavishnu. Verified by Rajesh.
************************************************* */

PROCEDURE tmp_gm_Loaner_Initiate 
	 (
        p_set_id     IN t208_set_details.C207_SET_ID%TYPE,
        p_acc_nm     IN T704_ACCOUNT.C704_ACCOUNT_NM_EN%TYPE,
        p_tag_id     IN t5010_tag.C5010_TAG_ID%TYPE,
        p_consign_id IN t504_consignment.c504_consignment_id%TYPE
	 )
AS
    -- Loaner request and Approval
    v_req_id         VARCHAR2 (20) ;
    v_reqdet_litpnum VARCHAR2 (20) ;
    v_req_det_id     VARCHAR2 (20) ;
    v_req_dtl_id     VARCHAR2 (20) ;
    v_dist_id v700_territory_mapping_detail.D_ID%TYPE;
    v_rep_id v700_territory_mapping_detail.rep_id%TYPE;
    v_user NUMBER;
    v_input_str_LN_Initiate CLOB;
    v_request_for NUMBER := '4127';
    v_ship_to     NUMBER := '4120';
    v_request_by  NUMBER := '50626';
    V_LN_CONSIGN_ID T504_CONSIGNMENT.C504_CONSIGNMENT_ID%TYPE;
    v_GMNA_Acct_id t704_account.C704_ACCOUNT_ID%TYPE;
    v_loaner_dt VARCHAR2(40);
    v_dateformat varchar2(100);
    v_shipid number;
    v_shipflag varchar2(10);
    v_input_str_pending_pick CLOB;
    v_status_fl number;
	v_ship_out_user NUMBER := 303137; -- John Davidar       
	v_track_upd VARCHAR2(100); 
   
BEGIN
	    

   select get_compdtfmt_frm_cntx() into v_dateformat from dual;
   v_user := IT_PKG_CM_USER.GET_VALID_USER;
    
      select C704_ACCOUNT_ID INTO v_GMNA_Acct_id from t704_account
      where UPPER(trim(C704_ACCOUNT_NM_EN)) = UPPER(trim(p_acc_nm));
    
      SELECT d_id, v_rep_id
      INTO v_dist_id, v_rep_id
      FROM v700_territory_mapping_detail
      WHERE ac_id= TRIM(v_GMNA_Acct_id);
      
      v_loaner_dt :=to_char(CURRENT_DATE,get_compdtfmt_frm_cntx()); 

      v_input_str_LN_Initiate := p_set_id||'^1'||'^'||v_loaner_dt||'|';

      -- Loaner Initiate
   	  gm_pkg_op_product_requests.gm_sav_product_requests (CURRENT_DATE, v_request_for, v_dist_id,
      v_request_by, v_user, v_ship_to, NULL, v_user, v_input_str_LN_Initiate, v_rep_id, v_GMNA_Acct_id, v_req_id, NULL, NULL) ;
           
     -- Fetching Request ID
      SELECT C526_PRODUCT_REQUEST_DETAIL_ID
      INTO v_req_det_id
      FROM t526_product_request_detail
      WHERE c525_product_request_id = v_req_id;
      
      --
      SELECT substr(p_consign_id||':'||p_tag_id, 0, 28) INTO v_track_upd FROM dual;
      --
      dbms_output.put_line (' v_req_det_id ' ||v_req_det_id); 
     -- Creating Shipping Record      
      gm_pkg_cm_shipping_trans.gm_sav_shipping(v_req_det_id,'50182','4120',v_dist_id,'5001'
     ,'5004',null,'0',null,v_ship_out_user,v_shipid,null,null);
   	 
       gm_pkg_sm_loaner_allocation.gm_sav_loaner_req_appv (v_req_det_id, v_user, '10', v_req_dtl_id, v_reqdet_litpnum,v_req_det_id) ;     
	 
       v_input_str_pending_pick :=p_consign_id||'^'||null||'^'||p_tag_id||'|';
       gm_pkg_op_set_pick_txn.gm_sav_set_pick_batch(v_input_str_pending_pick, v_user);
            
       gm_pkg_cm_shipping_trans.gm_sav_shipout(p_consign_id,'50182','4120',v_dist_id,'5001','5004'
           	,v_track_upd ,0,0,v_ship_out_user,0,v_shipid,NULL,v_shipflag);    

	   INSERT INTO STL_INV_LOAD_TXN_STATUS (tag_id,set_id,New_CN_ID, Comments) VALUES(p_set_id,p_tag_id,p_consign_id,'Loaner Initiated and in pending return status');
		
END tmp_gm_Loaner_Initiate;


/*
************************************************
* The purpose of this procedure is to 
* Initiate Account Consignment, FG inventory load and Field Sales warehouse update. This will be based on the strOpt sent.
* Steel Loose Item consignment.
* Author: Mahavishnu. Verified by Rajesh. 
************************************************* */
-- p_opt ACWH, FSWH, FG

PROCEDURE tmp_gm_main_stl_loose_item
	(
	p_opt IN varchar2

	)
AS
 
      
	  v_set_validation_flag varchar2(10);
	  v_err_dtls clob;
	  v_comp_dt_fmt VARCHAR2(20);
	 
	  v_company_id VARCHAR2(20);

 
        
   CURSOR cur_FSWH
    IS

        SELECT DISTRIBUTOR_NAME Dist_nm
        FROM globus_app.STL_LOOSE_INV_STG STL
        WHERE 
         STL.loose_item_type=p_opt
   		and DISTRIBUTOR_NAME is not null
   		and item_loaded_fl = 'IAFG_PROCESSED'
        group by DISTRIBUTOR_NAME;
        
    
             
 BEGIN   
	 
select get_compdtfmt_frm_cntx () INTO v_comp_dt_fmt  from dual;
  SELECT get_compid_frm_cntx() INTO v_company_id FROM dual;
	 
	
 			
 	IF (p_opt='FSWH') THEN 
 	-- iafg
 	tmp_gm_loose_item_iafg(p_opt);
 	 
 	 FOR loose_item_fswh IN cur_FSWH
	
   		  LOOP
    
	 		  BEGIN     	
		 		  
	--tmp_gm_loose_item_validation(p_opt,null, null,null, loose_item_fswh.Dist_id,v_set_validation_flag);   			 

	--			IF (v_set_validation_flag='PASS') THEN
				 
		           tmp_gm_fswh_load(loose_item_fswh.Dist_nm,p_opt);                   
	                
      --  		END IF;
     			 
        
        
    	    EXCEPTION    
   				 WHEN OTHERS THEN
   				 	Rollback;              
   				  
   				 	dbms_output.put_line ('Loose Item Failure for fswh for distributor '||loose_item_fswh.Dist_nm || ' '|| SQLERRM||chr(10)||dbms_utility.format_error_backtrace);        
	                
	                v_err_dtls :='Loose Item Failure for dist: Error message: '||SQLERRM||chr(10)||dbms_utility.format_error_backtrace;
		          
	              INSERT INTO stl_loose_item_status_log(DIST_NAME,ERR_DTLS) VALUES(loose_item_fswh.Dist_nm,v_err_dtls);
	           	  commit;
     
     		END;
     		
      
             
   		COMMIT;   
		
 	END LOOP;
 	
 	ELSIF (p_opt = 'FG')
 		THEN
 				 --tmp_gm_loose_item_validation(p_opt,null, null,NULL,NULL,v_set_validation_flag);
			BEGIN
			--	IF (v_set_validation_flag='PASS') THEN
				
		           tmp_gm_loose_item_iafg(p_opt);
                        
        	--END IF;
     	
        	EXCEPTION    
   			 	WHEN OTHERS THEN
   			 Rollback;              
   				dbms_output.put_line (' IAFG Creation failed ' ||SQLERRM||chr(10)||dbms_utility.format_error_backtrace);        
     		END;
       
   		COMMIT;   
   		
   	ELSIF (p_opt = 'BL')
 		THEN
 				 --tmp_gm_loose_item_validation(p_opt,null, null,NULL,NULL,v_set_validation_flag);
			BEGIN
			--	IF (v_set_validation_flag='PASS') THEN
				
		           tmp_gm_loose_item_iafg(p_opt);
                        
        	--END IF;
     	
        	EXCEPTION    
   			 	WHEN OTHERS THEN
   			 Rollback;              
   				dbms_output.put_line (' IABL Creation failed ' ||SQLERRM||chr(10)||dbms_utility.format_error_backtrace);        
     		END;
       
   		COMMIT;   
		
 	END IF;
 	
 	
END tmp_gm_main_stl_loose_item;



/************************************************
*-- The purpose of this procedure is to validate the following pre-requiste to avoid Loose item failure
*-- Account Availablity check
*-- Distributor ID check
*-- Part number check
*-- Author: Mahavishnu. Verified by Rajesh V.
*************************************************/

	PROCEDURE tmp_gm_loose_item_validation
  	   
    AS
      v_company_id t1900_company.c1900_company_id%TYPE;
      v_plant_id t5040_plant_master.c5040_plant_id%TYPE;
      V_ERR_DTLS VARCHAR2(2000);
      v_part_num T205_part_number.c205_part_number_id%TYPE;
      v_cnt NUMBER := 0;
 			   
 	 -- 1 Cursor Dist id check
 	 CURSOR dist_dtls_cur
 	 IS
 	  SELECT UPPER(TRIM(DISTRIBUTOR_NAME)) d_nm
   FROM STL_LOOSE_INV_STG
  WHERE item_loaded_fl      IS NULL
    AND DISTRIBUTOR_NAME IS NOT NULL
GROUP BY DISTRIBUTOR_NAME
  MINUS
 SELECT UPPER(TRIM(t701.C701_DISTRIBUTOR_NAME_EN)) d_nm
   FROM t701_distributor t701
  WHERE t701.c1900_company_id = v_company_id
    AND t701.c701_void_fl    IS NULL
GROUP BY t701.C701_DISTRIBUTOR_NAME_EN;
 	 -- 2 check Part # valid 
		CURSOR part_num_dtls_cur
		IS
		
 SELECT C205_PART_NUMBER_ID pnum
   FROM STL_LOOSE_INV_STG
  WHERE item_loaded_fl      IS NULL
GROUP BY C205_PART_NUMBER_ID
MINUS
SELECT t2023.C205_PART_NUMBER_ID pnum
FROM T2023_PART_COMPANY_MAPPING t2023
where t2023.C1900_COMPANY_ID = v_company_id
AND t2023.C2023_VOID_FL IS NULL;


-- part price check
CURSOR part_price_dtls_cur
IS
select C205_PART_NUMBER_ID pnum from STL_LOOSE_INV_STG
minus
select c205_part_number_id pnum from T2052_PART_PRICE_MAPPING where C1900_COMPANY_ID = v_company_id and C2052_VOID_FL IS NULL;

-- invalid Qty
CURSOR invalid_Qty_cur
IS
SELECT STL.C205_part_number_id PART_NO
           FROM STL_LOOSE_INV_STG STL
           WHERE 
           STL.item_loaded_fl is null
           AND STL.physical_qty <= 0
           GROUP BY STL.C205_part_number_id;          

-- distributor check

    BEGIN
  		  SELECT get_compid_frm_cntx(), get_plantid_frm_cntx () INTO v_company_id, v_plant_id FROM dual;
  		  
	    --1. to validate the FSWH
	    --a. Distributor Id
	    --b. part #
	    FOR dist_dtls IN dist_dtls_cur
	    LOOP
	    
	    update STL_LOOSE_INV_STG set item_loaded_fl = 'Skipped' where UPPER(TRIM(DISTRIBUTOR_NAME)) =dist_dtls.d_nm
	    AND item_loaded_fl IS NULL;
	    --
	    v_err_dtls :='Distributor id not available : '||dist_dtls.d_nm;
	          
	         	 INSERT INTO stl_loose_item_status_log(DIST_NAME,ERR_DTLS)  VALUES(dist_dtls.d_nm,v_err_dtls);
	    --     	 
	    v_cnt := v_cnt + 1;
	    
	    END LOOP;
	    DBMS_OUTPUT.PUT_LINE (' Invalid Distributor Count : '|| v_cnt);
	    --
	    v_cnt := 0;
	    -- Part # available check
	    FOR part_num_dtls IN part_num_dtls_cur
	    LOOP
	    	--
	    	v_part_num := part_num_dtls.pnum;
	    	--
	    	update STL_LOOSE_INV_STG set item_loaded_fl = 'Skipped' where c205_part_number_id =part_num_dtls.pnum
	    AND item_loaded_fl IS NULL;
	    --
	    v_err_dtls :='Part # not available : '||part_num_dtls.pnum;
	          
	         	 INSERT INTO stl_loose_item_status_log(part_num,ERR_DTLS)  VALUES(part_num_dtls.pnum,v_err_dtls);
	    

	         v_cnt := v_cnt + 1;
	    END LOOP;
	    DBMS_OUTPUT.PUT_LINE (' Invalid Part Number Count : '|| v_cnt);
	    --
	    v_cnt := 0;
	    -- part price check
	    FOR part_price_dtls IN part_price_dtls_cur
	    LOOP
	    		--
	    	v_part_num := part_price_dtls.pnum;
	    	--
	    	update STL_LOOSE_INV_STG set item_loaded_fl = 'Skipped' where c205_part_number_id =part_price_dtls.pnum
	    AND item_loaded_fl IS NULL;
	    --
	    v_err_dtls :='Part # price not available : '||part_price_dtls.pnum;
	     INSERT INTO stl_loose_item_status_log(part_num,ERR_DTLS)  VALUES(part_price_dtls.pnum,v_err_dtls);
	    -- 
	    v_cnt := v_cnt + 1;
	    END LOOP;
	    DBMS_OUTPUT.PUT_LINE (' Invalid Part Price Count : '|| v_cnt);
	    --
	    v_cnt := 0;
	   	    
	
	    -- Invalid Qty records found
	    v_cnt := 0;
	    --
	    FOR invalid_Qty IN invalid_Qty_cur
	    LOOP
	    --
	    update STL_LOOSE_INV_STG set item_loaded_fl = 'Skipped' where c205_part_number_id =invalid_Qty.PART_NO
	    AND physical_qty <= 0
	    AND item_loaded_fl IS NULL;
	    --
	     v_err_dtls :='Negative Qty for the Part # : '||invalid_Qty.PART_NO;
	          
	     INSERT INTO stl_loose_item_status_log(part_num ,ERR_DTLS)  VALUES(invalid_Qty.PART_NO,v_err_dtls);
	    --
	    v_cnt := v_cnt + 1;
	    END LOOP;
	    --
		DBMS_OUTPUT.PUT_LINE (' Negative Part Qty Count : '|| v_cnt);
	
    END tmp_gm_loose_item_validation;
 



/*
************************************************
* The purpose of this procedure is to Initiate IAFG for the Loose Items
* Steel Loaner Set List Data migration. July 2017
* Author: Mahavishnu. Verified by Rajesh. 
************************************************* */

PROCEDURE tmp_gm_loose_item_iafg
(
 p_opt     IN varchar2
)

AS
    v_user t101_user.c101_user_id%TYPE;
    v_initiate_input_str_IAFG CLOB;
    v_control_input_str_IAFG CLOB;
    v_verify_input_str_IAFG CLOB;
    v_part_no t205_part_number.c205_part_number_id%TYPE;
    v_company_id          NUMBER;
    v_plant_id            NUMBER;
    v_portal_com_date_fmt VARCHAR2(20) ;
    v_qty                 NUMBER;
    v_purchase_amt        NUMBER := 0;
    v_lot_number   		  VARCHAR2(40) ;
    v_reason	          NUMBER;
    v_txntype             NUMBER;
    v_wh_type             VARCHAR2(100) ;
    v_cur_wh              VARCHAR2(50) ;
    v_txn_type_name       VARCHAR2(50) ;
    v_message             VARCHAR2(100) ;
    v_source_wh           NUMBER;
    v_target_wh           NUMBER;
    v_costing_type        NUMBER;
    V_TXN_ID              VARCHAR2(20) ;
    V_OUT                 NUMBER;
    v_fg_location_id t5052_location_master.C5052_LOCATION_ID%TYPE;
    v_part_price t205_part_number.c205_equity_price%TYPE;
    v_cost t820_costing.c820_purchase_amt%TYPE;
    v_company_cost  t820_costing.C820_LOCAL_COMPANY_COST%TYPE;
    v_count number := 0;
    v_pkey NUMBER;
    v_part_company number;
    v_comp_dt_fmt VARCHAR2(20);
    v_location_cd t5052_location_master.c5052_location_cd%TYPE;
	   
    CURSOR cur_item_iafg
    IS
         SELECT STL.C205_part_number_id PART_NO, sum(STL.PHYSICAL_QTY) QTY, STL.lot_number LOT_NUMBER
           FROM STL_LOOSE_INV_STG STL
           WHERE 
           STL.item_loaded_fl is null
           and STL.loose_item_type=p_opt
           AND STL.physical_qty > 0
           GROUP BY STL.C205_part_number_id, STL.lot_number;
           
       
BEGIN
	
   BEGIN
    
	  v_user := IT_PKG_CM_USER.GET_VALID_USER;
	  select get_compdtfmt_frm_cntx () INTO v_comp_dt_fmt  from dual;

	SELECT get_compdtfmt_frm_cntx () INTO v_portal_com_date_fmt FROM dual;
    SELECT get_plantid_frm_cntx  INTO v_plant_id FROM dual;
    SELECT get_compid_frm_cntx() INTO v_company_id FROM dual;
        
     -- Create Location for FG and BL
      IF p_opt <> 'BL'THEN
		  BEGIN
--          v_fg_location_id:= '1958';

		    SELECT c5052_location_id INTO v_fg_location_id
		    FROM t5052_location_master
		    WHERE c5052_location_cd='STL-FG-LOCATION' 
		    and c1900_company_id= v_company_id
		    AND c5052_void_fl is null;
		    
		     EXCEPTION WHEN OTHERS THEN
		     dbms_output.put_line('*********Location error' || ' Error:' || SQLERRM||chr(10)||dbms_utility.format_error_backtrace);
		  END;
 	END IF;
  
  -- delete temp table
  delete from MY_TEMP_KEY_VALUE;
  
  FOR inv_item IN cur_item_iafg
  
    LOOP
        BEGIN
		
                v_qty     		:= inv_item.QTY;
                v_part_no 		:= inv_item.PART_NO;
                v_lot_number    := inv_item.LOT_NUMBER;
                
                IF p_opt = 'BL'THEN
	    			v_txn_type_name := 'INVAD-BULK';
	                v_txntype       := '400080';
	                v_target_wh     := '90814';
	                v_source_wh     := '90814';
	                v_cur_WH        := 'BL';
	                v_wh_type       := '90814';
	                v_costing_type  := '4900';
	                v_reason        := '90814'; 
    			ELSE
	    			v_txn_type_name := 'InvAdj-Shelf';
	                v_txntype       := '400068';
	                v_target_wh     := '90800';
	                v_source_wh     := '400069';
	                v_cur_WH        := 'FG';
	                v_wh_type       := '90800';
	                v_costing_type  := '4900';
	                v_reason        := '90800'; 
    			END IF;
                
                              
                --v_pkey 			:= inv_item.pkey;
                
                BEGIN
                     SELECT COST, company_cost
                       INTO v_purchase_amt, v_company_cost
                       FROM STL_DM_INVENTORY
                       WHERE part      = v_part_no
                       AND warehouse = 'FG';
                EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    v_purchase_amt := '0';
                    v_company_cost :='0';
                END;
                
                SELECT C1900_COMPANY_ID INTO v_part_company FROM  t2023_part_company_mapping t2023
                WHERE t2023.c205_part_number_id=v_part_no
                and c901_part_mapping_type='105360'
                AND C2023_void_fl is null;
           
              BEGIN
                SELECT count(1) INTO v_cost FROM t820_costing
                WHERE c205_part_number_id=v_part_no
                AND c820_delete_fl = 'N'
                --and c901_status=4801
                and c901_costing_type=4900
                and c1900_company_id=v_company_id;
                
                  EXCEPTION
                WHEN OTHERS THEN
                  v_cost:=NULL;
                END;
                
      
    IF v_cost = 0 THEN
              
		       INSERT INTO t820_costing (
		           c820_costing_id
		          ,c205_part_number_id
		          ,c901_costing_type
		          ,c820_part_received_dt
		          ,c820_qty_received
		          ,c820_purchase_amt
		          ,c820_qty_on_hand       
		          ,c901_status
		          ,c820_delete_fl
		          ,c820_created_by
		          ,c820_created_date
		          ,c1900_company_id
		          ,c5040_plant_id
		          ,C820_LOCAL_COMPANY_COST  
		          ,C1900_OWNER_COMPANY_ID
		          ,c901_owner_currency
		          ,C901_LOCAL_COMPANY_CURRENCY
		        ) 
		        VALUES 
		        (	 s820_costing.nextval
		          ,v_part_no
		          ,v_costing_type
		          ,CURRENT_DATE
		          ,0
		          ,v_purchase_amt
		          ,0
		          ,4801
		          ,'N'
		          ,V_USER
		          ,CURRENT_DATE
		          ,v_company_id
		          ,v_plant_id
		          ,v_company_cost
		          ,v_part_company
		          ,'1'
		          ,'1'
		        );
                               
   END IF;               
                                         
    --Get Part price
      BEGIN
	      
		        SELECT	c2052_equity_price price
		        INTO v_part_price
		        FROM t2052_part_price_mapping  
		        WHERE c205_part_number_id = v_part_no
		        AND c1900_company_id =v_company_id;
        
      EXCEPTION  WHEN no_data_found THEN
      
		          dbms_output.put_line('v_part_no=' || v_part_no || ' Price not found');
		          v_part_price := null;
		          dbms_output.put_line(v_part_no ||' - Without Equity Price, Inventory migration cannot be done, skip to next');
		          continue;
          
      END;
      	IF (v_txn_id is null) THEN
                
      		   		SELECT get_next_consign_id (v_txn_type_name) INTO v_txn_id FROM dual;
                
      	END IF;
           
	        		INSERT INTO MY_TEMP_KEY_VALUE (MY_TEMP_TXN_ID,MY_TEMP_TXN_KEY, my_temp_txn_value) values(p_opt ,v_part_no, v_lot_number);
	  			
	                v_initiate_input_str_IAFG := v_initiate_input_str_IAFG || v_part_no || ',' || v_qty || ',,,' ||v_part_price || '|';
	                --dbms_output.put_line ('v_initiate_input_str_IAFG '|| v_initiate_input_str_IAFG);
	                
	                v_control_input_str_IAFG := v_control_input_str_IAFG || v_part_no || ',' || v_qty || ',' ||v_lot_number || ',,' || v_part_price || '|';
	                
	                v_verify_input_str_IAFG := v_verify_input_str_IAFG || v_part_no || ',' || v_qty ||','|| v_lot_number|| ','||v_fg_location_id||',' || v_target_wh || '|';
	                
	                 IF p_opt <> 'BL'THEN
	                -- Calling FS location procedure to update the part qty
	               		gm_pkg_op_inv_field_sales_tran.gm_sav_fs_inv_loc_part_details (v_fg_location_id,v_txn_id, v_part_no,v_qty,'400068',4301,null,current_date, v_user);
        			END IF;
        			
	                v_count :=v_count+1;
                
           IF(v_count=20) then 
            IF p_opt = 'BL'THEN
            	 v_verify_input_str_IAFG :=NULL;
            END IF;

	                 	                 gm_save_inhouse_item_consign (v_txn_id, v_txntype, v_reason, '0', '', 'IAFG created for the Loose item Adjustment', v_user, v_initiate_input_str_IAFG, v_out) ;
	                 
	                 GM_LS_UPD_INLOANERCSG (v_txn_id, v_txntype, '0', '0', NULL, 'Steel Set Data Load', v_user,v_control_input_str_IAFG, NULL, 'ReleaseControl', v_message) ;
	            
	            	 gm_ls_upd_inloanercsg (v_txn_id, v_txntype, '', '0', NULL, 'Steel -- Manual Data --load', v_user,v_verify_input_str_IAFG, NULL, 'Verify', v_message) ;
	            
	            	 --UPDATE MY_TEMP_KEY_VALUE_IAFG SET     MY_TEMP_TXN_VALUE= v_txn_id  WHERE MY_TEMP_TXN_VALUE IS NULL;
	             
	             	 UPDATE t412_inhouse_transactions SET c412_comments='IA'||v_cur_WH||' Inventory Update - '|| p_opt  , c412_last_updated_date=CURRENT_DATE, c412_last_updated_by=v_user
				 	 where c412_inhouse_trans_id=v_txn_id;
		
				 	 	--dbms_output.put_line (' Values ' || p_opt|| p_acct_id || p_box_id || p_date || p_dist_id);
				 	 UPDATE STL_LOOSE_INV_STG T1 SET item_loaded_fl='IA'||v_cur_WH||'_PROCESSED',  new_cn_id = v_txn_id
						WHERE c205_part_number_id IN (select MY_TEMP_TXN_KEY from MY_TEMP_KEY_VALUE where  MY_TEMP_TXN_ID = p_opt)
						AND lot_number IN (select my_temp_txn_value from MY_TEMP_KEY_VALUE where  MY_TEMP_TXN_ID = p_opt)
						and loose_item_type=P_OPT
						AND item_loaded_fl IS NULL;
			-- Re-Setting the flag for the next IAFG creation.
			
		            v_count :='0';
		            v_initiate_input_str_IAFG :=NULL;
		            v_control_input_str_IAFG :=NULL;
		            v_verify_input_str_IAFG :=NULL;
		            v_txn_id := NULL;
		            --
		            DELETE FROM MY_TEMP_KEY_VALUE;
            
          END IF;
          
       END;
              
 END LOOP;
            
           IF (v_initiate_input_str_IAFG IS NOT NULL) THEN
           
             IF p_opt = 'BL'THEN
            	 v_verify_input_str_IAFG :=NULL;
             END IF;

		              gm_save_inhouse_item_consign (v_txn_id, v_txntype, v_reason, '0', '', 'IA'||v_cur_WH||' created for the Loose item Adjustment', v_user, v_initiate_input_str_IAFG, v_out) ;
		          
		              
		              GM_LS_UPD_INLOANERCSG (v_txn_id, v_txntype, '0', '0', NULL, 'Steel Set Data Load', v_user,
		              v_control_input_str_IAFG, NULL, 'ReleaseControl', v_message) ;
		             
		            
		              gm_ls_upd_inloanercsg (v_txn_id, v_txntype, '', '0', NULL, 'Steel -- Manual Data --load', v_user,
		              v_verify_input_str_IAFG, NULL, 'Verify', v_message) ;
		            
		              
		              DBMS_OUTPUT.PUT_LINE (' v_txn_id ' || v_txn_id);
		              
		              --UPDATE MY_TEMP_KEY_VALUE_IAFG SET     MY_TEMP_TXN_VALUE= v_txn_id  WHERE MY_TEMP_TXN_VALUE IS NULL;
					
		              UPDATE t412_inhouse_transactions SET c412_comments='IA'||v_cur_WH||' Inventory Update - '|| p_opt ,c412_last_updated_date=CURRENT_DATE, c412_last_updated_by=v_user
					  where c412_inhouse_trans_id=v_txn_id;
			 
					
					  --dbms_output.put_line (' Values 222 '|| p_opt|| p_acct_id || p_box_id || p_date || p_dist_id);
					  UPDATE STL_LOOSE_INV_STG T1 SET item_loaded_fl='IA'||v_cur_WH||'_PROCESSED', new_cn_id = v_txn_id
						WHERE c205_part_number_id IN (select MY_TEMP_TXN_KEY from MY_TEMP_KEY_VALUE where  MY_TEMP_TXN_ID = p_opt)
						AND lot_number IN (select my_temp_txn_value from MY_TEMP_KEY_VALUE where  MY_TEMP_TXN_ID = p_opt)
						and loose_item_type=P_OPT
						AND item_loaded_fl IS NULL;
						
			          v_count :='0';
			          v_initiate_input_str_IAFG :=NULL;
			          v_control_input_str_IAFG :=NULL;
			          v_verify_input_str_IAFG :=NULL;
			          v_txn_id := NULL;
			          --
			          DELETE FROM MY_TEMP_KEY_VALUE;
          
     	   END IF;  
         
		 
			
       	COMMIT;
       	
    EXCEPTION WHEN OTHERS then
    	  DBMS_OUTPUT.PUT_LINE('Loose Item IA'||v_cur_WH||' ERROR for part '||v_part_no || ' Error details: '||SQLERRM||chr(10)||dbms_utility.format_error_backtrace);
    ROLLBACK;
 
  END; 
END tmp_gm_loose_item_iafg;



/*
This procedure is created to create the Item consignment for the field sales warehouse.
Author :mahavishnu
TSK: 8279

*/

PROCEDURE tmp_gm_fswh_load
(
 p_dist in t701_distributor.c701_distributor_id%TYPE,
 p_opt IN VARCHAR2
)


AS

    v_user t101_user.c101_user_id%TYPE;
    v_input_str_item_consign CLOB;
    v_control_str_item_consign CLOB;
    v_part_no t205_part_number.c205_part_number_id%TYPE;
    v_company_id          t1900_company.C1900_COMPANY_ID%TYPE;
    v_plant_id            t5040_plant_master.C5040_PLANT_ID%TYPE;
    v_portal_com_date_fmt VARCHAR2 (20) ;
    v_qty                 NUMBER;
    v_dist_id t701_distributor.C701_DISTRIBUTOR_ID%TYPE;
    v_lot_number       VARCHAR2 (40) ;
    v_txntype          NUMBER;
    v_ship_date        VARCHAR2 (100) ;
    v_out_cn_id        VARCHAR2 (20) ;
    v_req_id           VARCHAR2 (20) ;
    v_strTxnType         NUMBER        := '4110';
    v_strRelVerFl        VARCHAR2 (10) := 'on';
    v_strLocType         NUMBER        := '93343';
    v_shipid           NUMBER;
    v_shipflag         VARCHAR2 (10) ;
    v_fg_location_id t5052_location_master.C5052_LOCATION_ID%TYPE;
    v_count      NUMBER := 0;
    v_count_item NUMBER := 0;
    v_pkey number;
    v_dist T701_Distributor.C701_DISTRIBUTOR_ID%TYPE;
    --
    v_ship_out_user NUMBER := 303137; -- John Davidar  
	
	   
    CURSOR cur_item_initiate
    IS
         SELECT STL.C205_part_number_id PART_NO, STL.PHYSICAL_QTY QTY, STL.lot_number LOT_NUMBER, STL.pkey pkey
         FROM STL_LOOSE_INV_STG STL
         WHERE STL.TAG_ID      IS NULL
		 AND DISTRIBUTOR_NAME= p_dist
		 AND STL.loose_item_type = p_opt
		 AND STL.physical_qty > 0
		AND STL.item_loaded_fl='IAFG_PROCESSED'
         ORDER BY C205_part_number_id;
BEGIN
    
	  	v_user := IT_PKG_CM_USER.GET_VALID_USER;
        SELECT get_compdtfmt_frm_cntx () INTO v_portal_com_date_fmt FROM dual;
        SELECT get_plantid_frm_cntx INTO v_plant_id FROM dual;
        SELECT get_compid_frm_cntx () INTO v_company_id FROM dual;
    -- Create Location for FG and BL
    
        BEGIN
		 		  SELECT C701_DISTRIBUTOR_ID INTO v_dist
		 		   FROM T701_Distributor
		 		  WHERE Upper(C701_DISTRIBUTOR_NAME_EN)=TRIM(Upper(p_dist))
		 		   AND C1900_COMPANY_ID=v_company_id;
				   EXCEPTION
    				WHEN NO_DATA_FOUND THEN	
    					dbms_output.put_line('p_dist=' || p_dist || ' Distributor not found');
		          		v_dist := null;
   	       END;
        
    BEGIN
         SELECT c5052_location_id
         INTO v_fg_location_id
         FROM t5052_location_master
         WHERE c5052_location_cd = 'STL-FG-LOCATION'
         AND c1900_company_id  = v_company_id
         AND c5052_void_fl is null;
         
--		v_fg_location_id:='1958';
    
    EXCEPTION
	
		WHEN OTHERS THEN
        dbms_output.put_line ('*********Location error fiekd sales warehouse ' || ' Error:' || SQLERRM||chr(10)||dbms_utility.format_error_backtrace) ;
    END;
    
    BEGIN
	    v_user := IT_PKG_CM_USER.GET_VALID_USER;
    
       v_dist_id := v_dist;
			
				
				   FOR inv_item_consign IN cur_item_initiate
				
				     LOOP
						v_qty        := inv_item_consign.QTY;
						v_part_no    := inv_item_consign.PART_NO;
						v_lot_number := inv_item_consign.LOT_NUMBER;
						v_pkey 		 := inv_item_consign.pkey;
         
                    INSERT INTO MY_TEMP_KEY_VALUE_loose_item(MY_TEMP_TXN_ID,MY_TEMP_TXN_KEY) values(v_pkey,v_part_no);
                 
					v_input_str_item_consign   := v_input_str_item_consign|| v_part_no||'^'||v_qty||'^'||v_ship_date||'^|';
					v_control_str_item_consign := v_control_str_item_consign||v_part_no||','||v_qty||','||v_lot_number||','||v_fg_location_id||','||'TBE'||','||'90800'||'|';
                
					v_count_item                 := v_count_item + 1;
                
                IF (v_count_item >= 20) THEN
                
                  	  v_input_str_item_consign := v_input_str_item_consign||'~5001^5004|';
                    
                   	   gm_pkg_op_process_prod_request.gm_sav_initiate_item (NULL, CURRENT_DATE, 50618, NULL, 40021, v_dist_id, 50625, V_USER, 4120, NULL, NULL, 15, V_USER, v_input_str_item_consign, 50060, '',CURRENT_DATE, v_req_id, v_out_cn_id, NULL) ;
                    
                 	   gm_pkg_cm_shipping_trans.gm_sav_shipping (v_req_id, '50184', '4120', v_dist_id, '5001', '5004', NULL,'0', NULL, v_ship_out_user, v_shipid, NULL, NULL);
                  	  
                    -- controlling the Item consignment
                    	gm_pkg_op_item_control_txn.gm_sav_item_control_main (v_out_cn_id, v_strTxnType, v_strRelVerFl,v_control_str_item_consign, v_strLocType, V_USER);
                    	
                    	gm_pkg_cm_shipping_trans.gm_sav_shipout (v_out_cn_id, '50181', '4120', v_dist_id, '5001', '5004',v_out_cn_id|| ':FSWH', 0, 0, v_ship_out_user, 0, v_shipid, NULL, v_shipflag);
                    	v_count_item               := 0;
                    	v_input_str_item_consign   := NULL;
                    	v_control_str_item_consign := NULL;
                    	v_shipid                   := NULL;
                        
                    	UPDATE t504_consignment SET C504_LAST_UPDATED_BY = v_user, C504_LAST_UPDATED_DATE = CURRENT_DATE, c504_comments ='FSWH inventory update for the distributor: '||v_dist_id
                      	WHERE c504_consignment_id = v_out_cn_id;
                      
                        UPDATE MY_TEMP_KEY_VALUE_loose_item SET    MY_TEMP_TXN_VALUE= v_out_cn_id  WHERE MY_TEMP_TXN_VALUE IS NULL;
                    	
                      	dbms_output.put_line ('FSWH inventory update for the Distributor '||v_dist_id);
                END IF;
            END LOOP;
            
            IF (v_input_str_item_consign IS NOT NULL) THEN
            
                v_input_str_item_consign := v_input_str_item_consign||'~5001^5004|';
                
                gm_pkg_op_process_prod_request.gm_sav_initiate_item (NULL, CURRENT_DATE, 50618, NULL, 40021, v_dist_id,50625, V_USER, 4120, NULL, NULL, 15, V_USER, v_input_str_item_consign, 50060, '', CURRENT_DATE,v_req_id, v_out_cn_id, NULL) ;
              				      
                gm_pkg_cm_shipping_trans.gm_sav_shipping (v_req_id, '50184', '4120', v_dist_id, '5001', '5004', NULL, '0', NULL, v_ship_out_user, v_shipid, NULL, NULL) ;
                
                -- controlling the Item consignment
                gm_pkg_op_item_control_txn.gm_sav_item_control_main (v_out_cn_id, v_strTxnType, v_strRelVerFl,v_control_str_item_consign, v_strLocType, V_USER) ;
                
                gm_pkg_cm_shipping_trans.gm_sav_shipout (v_out_cn_id, '50181', '4120', v_dist_id, '5001', '5004',v_out_cn_id|| ':FSWH', 0, 0, v_ship_out_user, 0, v_shipid, NULL, v_shipflag) ;
                
                 UPDATE t504_consignment
                 SET C504_LAST_UPDATED_BY = v_user, C504_LAST_UPDATED_DATE = CURRENT_DATE, c504_comments ='FSWH inventory update for the distributor: '||v_dist_id   WHERE c504_consignment_id = v_out_cn_id;
                  
                 UPDATE MY_TEMP_KEY_VALUE_loose_item SET MY_TEMP_TXN_VALUE= v_out_cn_id  WHERE MY_TEMP_TXN_VALUE IS NULL;
                    
                 dbms_output.put_line ('FSWH inventory updated for the Distributor: '||v_dist_id);
                 
                 v_dist_id                  := NULL;
                 v_count_item               := 0;
                 v_input_str_item_consign   := NULL;
                 v_control_str_item_consign := NULL;
                 v_shipid                   := NULL;
            
			END IF;
                    
                 UPDATE STL_LOOSE_INV_STG T1 SET item_loaded_fl='FSWH Initiated',T1.NEW_CN_ID = (SELECT T2.MY_TEMP_TXN_VALUE FROM MY_TEMP_KEY_VALUE_loose_item T2 WHERE T2.MY_TEMP_TXN_ID = T1.PKEY)
			     WHERE T1.PKEY IN (SELECT T2.MY_TEMP_TXN_ID FROM MY_TEMP_KEY_VALUE_loose_item T2 WHERE T2.MY_TEMP_TXN_ID = T1.PKEY)
			     AND item_loaded_fl='IAFG_PROCESSED'
			     AND DISTRIBUTOR_NAME= p_dist
			     AND LOOSE_ITEM_TYPE=P_OPT;
                 
	      
		COMMIT;
	  	   		 	   
		    EXCEPTION WHEN OTHERS then
		    
		    v_dist_id                  := NULL;
            v_count_item               := 0;
            v_input_str_item_consign   := NULL;
            v_control_str_item_consign := NULL;
            v_shipid                   := NULL;
            
            DBMS_OUTPUT.PUT_LINE('FSWH inventory error '||v_part_no||SQLERRM||chr(10)||dbms_utility.format_error_backtrace);
           ROLLBACK;
   	      end;
   	      
    

END tmp_gm_fswh_load;


PROCEDURE tmp_gm_main_set_cosign
AS
 
      v_commit_count NUMBER :='0';
	  v_set_validation_flag varchar2(10);
	  p_out_consign varchar2(100);
	  v_err_dtls clob;
	  v_out_request_id  T520_Request.C520_REQUEST_ID%TYPE;

    CURSOR cur_set
    IS
        SELECT DISTINCT (C207_SET_ID) sets_id, C5010_tag_id tag_id,    SOURCE_ACCOUNT ACCT_NM,DISTRIBUTOR_NAME DIST_NM
        FROM globus_app.STL_LOANER_INV_STG STL
        WHERE 
        STL.set_loaded_fl IS NULL
        and STL.c207_set_id IS NOT NULL
        and STL.DISTRIBUTOR_NAME is not null
        and STL.type = 'Consignment'
        order by C207_SET_ID, C5010_tag_id;
      
       
 BEGIN   
    FOR set_list IN cur_set
	
    LOOP
	    BEGIN 
			tmp_gm_set_validation(set_list.sets_id, set_list.tag_id,NULL,set_list.DIST_NM,'Consignment',v_set_validation_flag);
		
		IF (v_set_validation_flag='PASS') THEN
				
	            tmp_gm_iabl (set_list.sets_id, set_list.tag_id,'Consignment');
	            tmp_gm_set_build (set_list.sets_id, set_list.tag_id,'Consignment',set_list.DIST_NM,p_out_consign,v_out_request_id);	
	            dbms_output.put_line ('p_out_consign<><><'||p_out_consign);
	            dbms_output.put_line ('v_out_request_id<><><'||v_out_request_id);
	            tmp_set_ship_trans(p_out_consign,v_out_request_id,set_list.sets_id, set_list.tag_id,set_list.DIST_NM);
	                      
                
                    UPDATE STL_LOANER_INV_STG
                    SET set_loaded_fl    = 'SET CONSIGNMENT INITIATED'
                    WHERE c207_SET_ID  = set_list.sets_id
                    AND type = 'Consignment'
                    AND c5010_tag_id = set_list.tag_id;           
        END IF;
    
   			 EXCEPTION
    
   				 WHEN OTHERS THEN

   				   dbms_output.put_line ('Set Build Processing failed for the set:'||set_list.sets_id||'And Tag ID:'||
	    		    set_list.tag_id||SQLERRM||chr(10)||dbms_utility.format_error_backtrace);
	    		    Rollback;  
        
	            UPDATE STL_LOANER_INV_STG
	            SET set_loaded_fl    = 'Set Build Processing failed'
	              WHERE c207_SET_ID  = set_list.sets_id
	               AND type = 'Consignment'
	                AND c5010_tag_id = set_list.tag_id;
	             
	                          
		          v_err_dtls :='Set Build Processing failed: Error message: '||SQLERRM||chr(10)||dbms_utility.format_error_backtrace;
	              INSERT INTO STL_LOANER_STATUS_LOG(C207_SET_ID,C5010_TAG_ID,ERR_DTLS) VALUES(set_list.sets_id,set_list.tag_id,v_err_dtls);
	           	  commit;
     
    END;
    /* 
     v_commit_count    := v_commit_count + 1;
        IF (v_commit_count = 100) THEN
        
          END IF;
          v_commit_count:='0';
           
    */
             
   COMMIT;   
		
 END LOOP;
END tmp_gm_main_set_cosign;

PROCEDURE tmp_set_ship_trans
(
 p_out_consign_id IN t504_consignment.c504_consignment_id%TYPE,
 p_out_request_id IN t520_request.c520_request_id%TYPE,
 p_set_id IN t208_set_details.C207_SET_ID%TYPE,
 p_tag_id IN t5010_tag.C5010_TAG_ID%TYPE,
 p_dist_nm IN t701_distributor.C701_DISTRIBUTOR_NAME_EN%TYPE       
)
AS
v_shipid number;
v_ship_to     NUMBER;
v_dist_id   NUMBER;
v_input_str_pending_put CLOB;
v_user t101_user.c101_user_id%TYPE;
v_ship_out_user NUMBER := 303137; -- John Davidar      
v_track_upd VARCHAR2(100); 
v_shipflag varchar2(10);
v_company_id t1900_company.c1900_company_id%TYPE;
BEGIN
	
	 v_user := IT_PKG_CM_USER.GET_VALID_USER;  
	
	  SELECT get_compid_frm_cntx() INTO v_company_id FROM dual;
	  
	  v_ship_to   := '4120'; -- Distributor
	  
	  
 			  SELECT C701_DISTRIBUTOR_ID INTO v_dist_id
	          FROM t701_DISTRIBUTOR 
	          WHERE Upper(C701_DISTRIBUTOR_NAME_EN)=TRIM(Upper(p_dist_nm))
	          and c701_void_fl is null
	          and c1900_company_id=v_company_id;
	  
-- Save shipping sdetails

	   gm_pkg_cm_shipping_trans.gm_sav_shipping (p_out_request_id, '50184', '4120', v_dist_id, '5001', '5004', NULL,
                '0', NULL, v_ship_out_user, v_shipid, NULL, NULL) ;
                
			-- It move the consignment set and shipout
			
		
			
			-- Processing the Pending putaway
			v_input_str_pending_put :=p_out_consign_id||'^'||null||'^'||p_tag_id||'|';
			
			gm_pkg_op_set_put_txn.gm_sav_set_put_batch(v_input_str_pending_put,v_user);
			
			
			dbms_output.put_line('Pending Shipping');
			SELECT substr(p_out_consign_id||':'||p_tag_id, 0, 39) INTO v_track_upd FROM dual;
			
			gm_pkg_cm_shipping_trans.gm_sav_shipout(p_out_consign_id,'50181',v_ship_to,v_dist_id,'5001','5004'
           	,v_track_upd ,0,0,v_ship_out_user,0,v_shipid,NULL,v_shipflag);    
			dbms_output.put_line('Shipped Out');
           	
           	UPDATE t504_consignment
			SET C504_LAST_UPDATED_BY=v_user, C504_LAST_UPDATED_DATE =CURRENT_DATE, c504_comments='Steel Data Migration tag: '||p_tag_id
			WHERE c504_consignment_id=p_out_consign_id;
--			
--		  

END tmp_set_ship_trans;


/***********************************************************************************
* Description : The procedure used to load stelkast parts to Quarntine inventory (IAQN)
*              To create the transactions/Controlled and verify
**************************************************************************************/

PROCEDURE tmp_gm_loose_item_iaqn (
        p_opt IN VARCHAR2)
AS
    v_user t101_user.c101_user_id%TYPE;
    v_initiate_input_str_IAQN CLOB;
    v_control_input_str_IAQN CLOB;
    v_verify_input_str_IAQN CLOB;
    v_part_no t205_part_number.c205_part_number_id%TYPE;
    v_company_id          NUMBER;
    v_plant_id            NUMBER;
    v_portal_com_date_fmt VARCHAR2 (20) ;
    v_qty                 NUMBER;
    v_purchase_amt        NUMBER := 0;
    v_lot_number          VARCHAR2 (40) ;
    v_reason              NUMBER;
    v_txntype             NUMBER;
    v_wh_type             VARCHAR2 (100) ;
    v_cur_wh              VARCHAR2 (50) ;
    v_txn_type_name       VARCHAR2 (50) ;
    v_message             VARCHAR2 (100) ;
    v_source_wh           NUMBER;
    v_target_wh           NUMBER;
    v_costing_type        NUMBER;
    V_TXN_ID              VARCHAR2 (20) ;
    V_OUT                 NUMBER;
    v_qn_location_id t5052_location_master.C5052_LOCATION_ID%TYPE;
    v_part_price t205_part_number.c205_equity_price%TYPE;
    v_cost t820_costing.c820_purchase_amt%TYPE;
    v_company_cost t820_costing.C820_LOCAL_COMPANY_COST%TYPE;
    v_count        NUMBER := 0;
    v_pkey         NUMBER;
    v_part_company NUMBER;
    v_comp_dt_fmt  VARCHAR2 (20) ;
    --
    CURSOR cur_item_iafg
    IS
         SELECT STL.C205_part_number_id part_no, SUM (STL.physical_qty) QTY
           FROM stl_loose_inv_stg STL
          WHERE STL.item_loaded_fl IS NULL
            AND STL.loose_item_type = p_opt
            AND STL.physical_qty    > 0
       GROUP BY STL.C205_part_number_id;
     --  
BEGIN
	--
    BEGIN
	    -- get the user name
        v_user := IT_PKG_CM_USER.GET_VALID_USER;
        
        -- get the values from context
        
         SELECT get_compdtfmt_frm_cntx () INTO v_comp_dt_fmt FROM dual;
         SELECT get_compdtfmt_frm_cntx () INTO v_portal_com_date_fmt FROM dual;
         SELECT get_plantid_frm_cntx INTO v_plant_id FROM dual;
         SELECT get_compid_frm_cntx () INTO v_company_id FROM dual;
         
        -- delete temp table
         DELETE FROM MY_TEMP_KEY_VALUE;
        -- 
        FOR inv_item IN cur_item_iafg
        LOOP
            BEGIN
                v_qty           := inv_item.QTY;
                v_part_no       := inv_item.PART_NO;
                v_lot_number    := 'NOC#';
                v_txn_type_name := 'INVAD-QUARAN';
                v_txntype       := '400081';
                v_target_wh     := '90800';
                v_source_wh     := '120618';
                v_cur_WH        := 'QN';
                v_wh_type       := '90800';
                v_costing_type  := '4902';
                v_reason        := '90800';
                --
                --v_pkey    := inv_item.pkey;
                BEGIN
                     SELECT COST, company_cost
                       INTO v_purchase_amt, v_company_cost
                       FROM STL_DM_INVENTORY
                      WHERE part      = v_part_no
                        AND warehouse = 'QUARAN';
                EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    v_purchase_amt := '0';
                    v_company_cost := '0';
                END;
                --
                 SELECT C1900_COMPANY_ID
                   INTO v_part_company
                   FROM t2023_part_company_mapping t2023
                  WHERE t2023.c205_part_number_id = v_part_no
                    AND c901_part_mapping_type    = '105360'
                    AND C2023_void_fl            IS NULL;
                  --
                    
                BEGIN
                     SELECT COUNT (1)
                       INTO v_cost
                       FROM t820_costing
                      WHERE c205_part_number_id = v_part_no
                        AND c820_delete_fl      = 'N'
                        --and c901_status=4801
                        AND c901_costing_type = 4900
                        AND c1900_company_id  = v_company_id;
                        
                EXCEPTION
                WHEN OTHERS THEN
                    v_cost := NULL;
                END;
                --
                
                IF v_cost = 0 THEN
                     INSERT
                       INTO t820_costing
                        (
                            c820_costing_id, c205_part_number_id, c901_costing_type
                          , c820_part_received_dt, c820_qty_received, c820_purchase_amt
                          , c820_qty_on_hand, c901_status, c820_delete_fl
                          , c820_created_by, c820_created_date, c1900_company_id
                          , c5040_plant_id, C820_LOCAL_COMPANY_COST, C1900_OWNER_COMPANY_ID
                          , c901_owner_currency, C901_LOCAL_COMPANY_CURRENCY
                        )
                        VALUES
                        (
                            s820_costing.nextval, v_part_no, 4900
                          , CURRENT_DATE, 0, v_purchase_amt
                          , 0, 4801, 'N'
                          , V_USER, CURRENT_DATE, v_company_id
                          , v_plant_id, v_company_cost, v_part_company
                          , '1', '1'
                        ) ;
                END IF;
                
                --Get Part price
                BEGIN
                     SELECT c2052_equity_price price
                       INTO v_part_price
                       FROM t2052_part_price_mapping
                      WHERE c205_part_number_id = v_part_no
                        AND c1900_company_id    = v_company_id;
                EXCEPTION
                WHEN no_data_found THEN
                    dbms_output.put_line ('v_part_no=' || v_part_no || ' Price not found') ;
                    v_part_price := NULL;
                    dbms_output.put_line (v_part_no ||
                    ' - Without Equity Price, Inventory migration cannot be done, skip to next') ;
                    CONTINUE;
                END;
                --
                
                IF (v_txn_id IS NULL) THEN
                     SELECT get_next_consign_id (v_txn_type_name) INTO v_txn_id FROM dual;
                END IF;
                --
                 INSERT
                   INTO MY_TEMP_KEY_VALUE
                    (
                        MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, my_temp_txn_value
                    )
                    VALUES
                    (
                        p_opt, v_part_no, v_lot_number
                    ) ;
                --    
                v_initiate_input_str_IAQN := v_initiate_input_str_IAQN || v_part_no || ',' || v_qty || ',,,' ||
                v_part_price || '|';
                
                --dbms_output.put_line ('v_initiate_input_str_IAQN '|| v_initiate_input_str_IAQN);
                
                --
                v_control_input_str_IAQN := v_control_input_str_IAQN || v_part_no || ',' || v_qty || ',' ||v_lot_number
                || ',,' || v_part_price || '|';
                
                --
                v_verify_input_str_IAQN := v_verify_input_str_IAQN || v_part_no || ',' || v_qty ||','|| v_lot_number||
                ','||v_qn_location_id||',' || v_target_wh || '|';
                
                --
                v_count    := v_count + 1;
                
                --
                IF (v_count = 20) THEN
                    DBMS_OUTPUT.PUT_LINE (' Inside every 20 txn id :: ' || v_txn_id) ;
                    
                    gm_save_inhouse_item_consign (v_txn_id, v_txntype, v_reason, '0', '',
                    'IAQN created for the Loose item Adjustment', v_user, v_initiate_input_str_IAQN, v_out) ;
                    
                    --
                    
                    GM_LS_UPD_INLOANERCSG (v_txn_id, v_txntype, '0', '0', NULL, 'Steel Set Data Load', v_user,
                    v_control_input_str_IAQN, NULL, 'ReleaseControl', v_message) ;
                    
                    gm_ls_upd_inloanercsg (v_txn_id, v_txntype, '', '0', NULL, 'Steel -- Manual Data --load', v_user,
                    v_verify_input_str_IAQN, NULL, 'Verify', v_message) ;
                    
                    --UPDATE MY_TEMP_KEY_VALUE_IAFG SET     MY_TEMP_TXN_VALUE= v_txn_id  WHERE MY_TEMP_TXN_VALUE IS
                    -- NULL;
                    
                     UPDATE t412_inhouse_transactions
                    SET c412_comments             = 'IAQN Inventory Update - '|| p_opt, c412_last_updated_date = CURRENT_DATE,
                        c412_last_updated_by      = v_user
                      WHERE c412_inhouse_trans_id = v_txn_id;
                      
                    --dbms_output.put_line (' Values ' || p_opt|| p_acct_id || p_box_id || p_date || p_dist_id);
                    
                     UPDATE STL_LOOSE_INV_STG T1
                    SET item_loaded_fl           = 'IAQN_PROCESSED', new_cn_id = v_txn_id
                      WHERE c205_part_number_id IN
                        (
                             SELECT MY_TEMP_TXN_KEY FROM MY_TEMP_KEY_VALUE WHERE MY_TEMP_TXN_ID = p_opt
                        )
                        --AND lot_number IN (select my_temp_txn_value from MY_TEMP_KEY_VALUE where  MY_TEMP_TXN_ID =
                        -- p_opt)
                        AND loose_item_type = P_OPT
                        AND item_loaded_fl IS NULL;
                        
                    -- Re-Setting the flag for the next IAFG creation.
                    
                    v_count                   := '0';
                    v_initiate_input_str_IAQN := NULL;
                    v_control_input_str_IAQN  := NULL;
                    v_verify_input_str_IAQN   := NULL;
                    v_txn_id                  := NULL;
                    --
                     DELETE FROM MY_TEMP_KEY_VALUE;
                END IF;
            END;
        END LOOP;
        --
        
        IF (v_initiate_input_str_IAQN IS NOT NULL)
        THEN
        	--
            gm_save_inhouse_item_consign (v_txn_id, v_txntype, v_reason, '0', '',
            'IAQN created for the Loose item Adjustment', v_user, v_initiate_input_str_IAQN, v_out) ;
            --
            
            GM_LS_UPD_INLOANERCSG (v_txn_id, v_txntype, '0', '0', NULL, 'Steel Set Data Load', v_user,
            v_control_input_str_IAQN, NULL, 'ReleaseControl', v_message) ;
            
            --
            gm_ls_upd_inloanercsg (v_txn_id, v_txntype, '', '0', NULL, 'Steel -- Manual Data --load', v_user,
            v_verify_input_str_IAQN, NULL, 'Verify', v_message) ;
            
            --
            DBMS_OUTPUT.PUT_LINE (' v_txn_id ' || v_txn_id) ;
            --
            
            --UPDATE MY_TEMP_KEY_VALUE_IAFG SET     MY_TEMP_TXN_VALUE= v_txn_id  WHERE MY_TEMP_TXN_VALUE IS NULL;
             UPDATE t412_inhouse_transactions
            SET c412_comments             = 'IAQN Inventory Update - '|| p_opt, c412_last_updated_date = CURRENT_DATE,
                c412_last_updated_by      = v_user
              WHERE c412_inhouse_trans_id = v_txn_id;
              
            --dbms_output.put_line (' Values 222 '|| p_opt|| p_acct_id || p_box_id || p_date || p_dist_id);
            
             UPDATE STL_LOOSE_INV_STG T1
            SET item_loaded_fl           = 'IAQN_PROCESSED', new_cn_id = v_txn_id
              WHERE c205_part_number_id IN
                (
                     SELECT MY_TEMP_TXN_KEY FROM MY_TEMP_KEY_VALUE WHERE MY_TEMP_TXN_ID = p_opt
                )
                --AND lot_number IN (select my_temp_txn_value from MY_TEMP_KEY_VALUE where  MY_TEMP_TXN_ID = p_opt)
                AND loose_item_type    = P_OPT
                AND item_loaded_fl    IS NULL;
                
            --
                
            v_count                   := '0';
            v_initiate_input_str_IAQN := NULL;
            v_control_input_str_IAQN  := NULL;
            v_verify_input_str_IAQN   := NULL;
            v_txn_id                  := NULL;
            --
             DELETE FROM MY_TEMP_KEY_VALUE;
        END IF;
        --
        COMMIT;
        
    EXCEPTION
    WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE ('Loose Item IAQN ERROR for part '||v_part_no || ' Error details: '||SQLERRM||chr (10) ||
        dbms_utility.format_error_backtrace) ;
        ROLLBACK;
    END;
    --
END tmp_gm_loose_item_iaqn;


/***********************************************************************************
* Description : The procedure used to load update the QNFG transactions to temp table.
*              Once warehouse team created the QNFG - updated the txn details to TAG id column.
**************************************************************************************/
PROCEDURE tmp_update_qnfg_transaction (
        p_user_id          IN t101_user.c101_user_id%TYPE,
        p_txn_created_date IN DATE,
        p_project_ids      IN VARCHAR2)
AS
    v_cnt NUMBER := 0;
    --
    CURSOR qnfg_txn_dtls_cur
    IS
         SELECT t504.C504_CONSIGNMENT_ID cn_id, t505.C205_PART_NUMBER_ID part_number
           FROM T504_CONSIGNMENT t504, T505_ITEM_CONSIGNMENT t505, T205_PART_NUMBER t205
          WHERE t504.C504_CONSIGNMENT_ID = t505.C504_CONSIGNMENT_ID
            AND t205.C205_PART_NUMBER_ID = t505.C205_PART_NUMBER_ID
            --
            AND t504.c504_status_fl       = 2 -- Pending control
            AND t504.C504_FINAL_COMMENTS IS NULL -- comments
            --
            AND t504.C504_CREATED_BY           = p_user_id
            AND TRUNC (t504.C504_CREATED_DATE) = TRUNC (p_txn_created_date)
            AND t205.C202_PROJECT_ID          IN
            (
                 SELECT token FROM v_in_list
            )
        AND t505.C505_VOID_FL IS NULL
        AND t504.C504_VOID_FL IS NULL;
        
BEGIN
    -- set the project into context
    my_context.set_my_inlist_ctx (p_project_ids) ;
    --
    FOR qnfg_update IN qnfg_txn_dtls_cur
    LOOP
        v_cnt := v_cnt + 1;
        --
        DBMS_OUTPUT.PUT_LINE (v_cnt ||' :: Transations Dtls '|| qnfg_update.cn_id ||' Part # ::'||
        qnfg_update.part_number) ;
        --
         UPDATE stl_loose_inv_stg
        SET tag_id                  = qnfg_update.cn_id
          WHERE c205_part_number_id = qnfg_update.part_number
            AND loose_item_type     = 'IAQN'
            AND item_loaded_fl      = 'IAQN_PROCESSED';
       --     
        DBMS_OUTPUT.PUT_LINE (' Rows updated dtls :: '|| SQL%ROWCOUNT) ;
        --
    END LOOP;
    --
END tmp_update_qnfg_transaction;

/***********************************************************************************
* Description : The procedure used to load the LOT number details to QNFG transactions
*              Transactions move upto pending verification status.
**************************************************************************************/

PROCEDURE tmp_gm_upd_qnfg_lot_details
AS
    v_user t101_user.c101_user_id%TYPE;
    v_initiate_input_str_IAQN CLOB;
    v_control_input_str_IAQN CLOB;
    v_verify_input_str_IAQN CLOB;
    v_part_no t205_part_number.c205_part_number_id%TYPE;
    v_company_id               NUMBER;
    v_plant_id                 NUMBER;
    v_portal_com_date_fmt      VARCHAR2 (20) ;
    v_qty                      NUMBER;
    v_purchase_amt             NUMBER := 0;
    v_lot_number               VARCHAR2 (40) ;
    v_txn_id                   VARCHAR2 (20) ;
    v_part_location_id         VARCHAR2 (20) ;
    v_strTxnType               NUMBER        := '4116'; -- Quarantine To Shelf
    v_strRelVerFl              VARCHAR2 (10) := 'on';
    v_strLocType               NUMBER        := '93343'; -- Pick Transacted
    v_control_str_item_consign VARCHAR2 (30000) ;
    v_total_txn_cnt            NUMBER := 0;
    v_part_cnum_id T2550_PART_CONTROL_NUMBER.c2550_part_control_number_id%TYPE;
    --
    CURSOR cur_qnfg_master
    IS
         SELECT stl.tag_id qn_id
           FROM stl_loose_inv_stg STL
          WHERE STL.tag_id         IS NOT NULL
            AND stl.item_loaded_fl  = 'IAQN_PROCESSED'
            AND stl.loose_item_type = 'IAQN'
            --AND tag_id             <> 'QNFG-7800877'
       GROUP BY stl.tag_id
       ORDER BY stl.tag_id;
    --
    CURSOR cur_qnfg_dtls
    IS
         SELECT STL.C205_part_number_id PART_NO, SUM (STL.PHYSICAL_QTY) QTY, STL.LOT_NUMBER cnum
           FROM stl_loose_inv_stg STL
          WHERE STL.tag_id      IS NOT NULL
            AND stl.tag_id       = v_txn_id
            AND STL.physical_qty > 0
       GROUP BY STL.C205_part_number_id, STL.LOT_NUMBER;
BEGIN
    --
    v_user := IT_PKG_CM_USER.GET_VALID_USER;
    --
     SELECT get_plantid_frm_cntx (), get_compid_frm_cntx ()
       INTO v_plant_id, v_company_id
       FROM dual;
    --
    FOR qnfg_master IN cur_qnfg_master
    LOOP
        --
        v_txn_id := qnfg_master.qn_id;
        --
        v_control_str_item_consign := NULL;
        v_total_txn_cnt            := v_total_txn_cnt + 1;
        --
        FOR qnfg_dtls IN cur_qnfg_dtls
        LOOP
            --
            v_part_no    := qnfg_dtls.PART_NO;
            v_qty        := qnfg_dtls.QTY;
            v_lot_number := qnfg_dtls.cnum;
            --
            -- pass location always null
            v_part_location_id := NULL;
            --
            v_control_str_item_consign := v_control_str_item_consign||v_part_no||','||v_qty||','||v_lot_number||','||
            v_part_location_id||','||'TBE'||','||'90800'||'|';
            --
            v_part_cnum_id := NULL;
            -- insert control # to our tabel
            BEGIN
                 SELECT c2550_part_control_number_id
                   INTO v_part_cnum_id
                   FROM t2550_part_control_number
                  WHERE c205_part_number_id  = v_part_no
                    AND c2550_control_number = v_lot_number;
            EXCEPTION
            WHEN OTHERS THEN
                v_part_cnum_id := NULL;
            END;
            
            -- pkey, part, lot, size, vendor id, mfg date, doner , exp date, user id, pack slip, rev level, udi
            gm_pkg_op_partcontrol_txn.gm_save_part_control_info (v_part_cnum_id, v_part_no, v_lot_number, NULL, NULL,
            NULL, NULL, NULL, v_user, NULL, NULL, NULL) ;
            
        END LOOP;
        
        -- controlling the Item consignment
        gm_pkg_op_item_control_txn.gm_sav_item_control_main (v_txn_id, v_strTxnType, v_strRelVerFl,
        v_control_str_item_consign, v_strLocType, V_USER) ;
        --
        DBMS_OUTPUT.PUT_LINE ('QNFG Transactions details '||v_txn_id || '  Contorl # :: '||v_control_str_item_consign)
        ;
        --
        --dbms_output.put_line (' Values 222 '|| p_opt|| p_acct_id || p_box_id || p_date || p_dist_id);
         UPDATE stl_loose_inv_stg T1
        SET item_loaded_fl = 'QNFG_CNUM'
          WHERE tag_id     = v_txn_id
            --AND lot_number IN (select my_temp_txn_value from MY_TEMP_KEY_VALUE where  MY_TEMP_TXN_ID = p_opt)
            AND C205_part_number_id = v_part_no
            AND item_loaded_fl      = 'IAQN_PROCESSED'
            AND LOOSE_ITEM_TYPE     = 'IAQN';
        --
         UPDATE t504_consignment
        SET c504_final_comments     = 'QNFG processed'
          WHERE c504_consignment_id = v_txn_id;
        --
    END LOOP;
    --
    DBMS_OUTPUT.PUT_LINE (' Total transactions updated ' || v_total_txn_cnt) ;
    --
END tmp_gm_upd_qnfg_lot_details;


END STL_TMP_GM_INVENTORY_LOAD;
 /