-- @"C:\Database\Packages\Sales\gm_pkg_sm_acct_trans_temp.bdy";
CREATE OR REPLACE
PACKAGE BODY gm_pkg_sm_acct_trans_temp
IS
    /****************************************************************
    * Description : Procedure used to fetch the rep account infromation
    * Author      : mmuthusamy
    *****************************************************************/
PROCEDURE gm_fch_rep_account_info (
        p_rep_account_id t704_account.c704_account_id%TYPE,
        p_out OUT Types.cursor_type)
AS
    v_portal_comp_id t704_account.c1900_company_id%TYPE;
BEGIN
    --
     SELECT get_compid_frm_cntx () INTO v_portal_comp_id FROM DUAL;
    --
    OPEN p_out FOR SELECT C704_ACCOUNT_ID ID,
    C704_ACCOUNT_NM ANAME,C704_ACCOUNT_NM_EN ANAME_EN,
    C901_COMPANY_ID COMPID,
    GET_DISTRIBUTOR_ID (C703_SALES_REP_ID) DISTID,
    C703_SALES_REP_ID REPID,
    C704_ACCOUNT_SH_NAME SNAME,
    C704_ACTIVE_FL AFLAG,
    C704_INCEPTION_DATE INCDATE,
    C101_PARTY_ID PARTYID,
    c1900_company_id PORTAL_COMPANY_ID, 
    t704a.ACCGROUP ACCGROUP,
    t704a.INVCLOSEDT INVCLOSEDT,
    t704.c901_currency ACC_CURR_ID,
    GET_CODE_NAME(t704.c901_currency) ACC_CURR_SYMB,
    GET_CODE_NAME_ALT(t704.c901_currency) ACC_CURR_SYMB_NM,
    get_rule_value_by_company (t704.c1900_company_id, 'AVATAX', v_portal_comp_id) company_avatax_fl
    ,c101_dealer_id dealerid
    FROM T704_ACCOUNT t704,(
         SELECT C704_ACCOUNT_ID act_id,
            MAX (DECODE (c901_attribute_type, 7008, c704a_attribute_value)) ACCGROUP
                       ,MAX (DECODE (c901_attribute_type, 106120, c704a_attribute_value))  INVCLOSEDT 
           FROM t704a_account_attribute
          WHERE C704_ACCOUNT_ID = p_rep_account_id 
            AND c901_attribute_type IN (7008,106120)-- 7008 Account group type, 106120 invoice_closing_date_type
            AND c704a_void_fl       IS NULL
       GROUP BY C704_ACCOUNT_ID
    )
    t704a
    WHERE t704.c704_account_id = p_rep_account_id
    AND t704.c704_account_id = t704a.ACT_ID(+)
    AND c1900_company_id= NVL( v_portal_comp_id,c1900_company_id) 
    AND c704_void_fl  IS NULL;
END gm_fch_rep_account_info;
/****************************************************************
* Description : Procedure used to fetch the parent account infromation
* Author      : mmuthusamy
*****************************************************************/
PROCEDURE gm_fch_parent_account_info (
        p_party_id t704_account.c101_party_id%TYPE,
        p_out OUT Types.cursor_type)
AS
    v_portal_comp_id t704_account.c1900_company_id%TYPE;
BEGIN
    --
     SELECT get_compid_frm_cntx ()
       INTO v_portal_comp_id
       FROM DUAL;
    --
    OPEN p_out FOR SELECT t101.C101_PARTY_ID PARTYID,
    t101.c101_first_nm || t101.c101_last_nm PARTYNM,
    c901_account_type ACCTYPE,
    C704_CONTACT_PERSON CPERSON,
    C704_PHONE PHONE,
    C704_FAX FAX,
    C704_COMMENTS COMMENTS,
    C704_BILL_NAME BNAME,
    C704_BILL_ADD1 BADD1,
    C704_BILL_ADD2 BADD2,
    C704_BILL_CITY BCITY,
    C704_BILL_STATE BSTATE,
    C704_BILL_COUNTRY BCOUNTRY,
    C704_BILL_ZIP_CODE BZIP,
    C704_SHIP_NAME CNAME,
    C704_SHIP_ADD1 SADD1,
    C704_SHIP_ADD2 SADD2,
    C704_SHIP_CITY SCITY,
    C704_SHIP_STATE SSTATE,
    C704_SHIP_COUNTRY SCOUNTRY,
    C704_SHIP_ZIP_CODE SZIP,
    C901_CARRIER CARRIER --Is it preferred carrier
    ,
    C704_PAYMENT_TERMS PTERM,
    C704_CREDIT_RATING CREDITRATING,
    C704_RATING_HISTORY_FL RATING_FL,
    t704a.COLLECTORID,
    t704a.ACCGROUP -- Do We need this
    ,
    t704a.ACCTSOURCE,
    t704a.SOURCEHISTFL -- Do we need this
    ,
    t704a.SHIPADDFL,
    t704a.ACCTAXTYPE,
    t704a.ACCTCATVALUE FROM T704_ACCOUNT t704, (
         SELECT C704_ACCOUNT_ID act_id, MAX (DECODE (c901_attribute_type, 103050, c704a_attribute_value)) COLLECTORID,
            MAX (DECODE (c901_attribute_type, 7008, c704a_attribute_value)) ACCGROUP, MAX (DECODE (c901_attribute_type,
            104800, c704a_attribute_value)) ACCTSOURCE, MAX (DECODE (c901_attribute_type, 104800, c704a_history_fl, '')
            ) SOURCEHISTFL, MAX (DECODE (c901_attribute_type, 101191, c704a_attribute_value)) SHIPADDFL, MAX (DECODE (
            c901_attribute_type, 5504, c704a_attribute_value)) ACCTCATVALUE,
            MAX (DECODE (c901_attribute_type, 101170, c704a_attribute_value)) ACCTAXTYPE
           FROM t704a_account_attribute
          WHERE C704_ACCOUNT_ID IN
            (
                 SELECT C704_ACCOUNT_ID FROM T704_ACCOUNT WHERE C101_PARTY_ID = p_party_id
            )
            AND c901_attribute_type IN (103050, 7008, 104800, 101191, 5504, 101170)
            AND c704a_void_fl       IS NULL
       GROUP BY C704_ACCOUNT_ID
    )
    t704a,
    t101_party t101 WHERE t101.C101_PARTY_ID = p_party_id AND t704.C704_ACCOUNT_ID = t704a.act_id(+) AND
    t704.C101_PARTY_ID (+)                   = t101.C101_PARTY_ID AND t101.c1900_company_id = NVL (v_portal_comp_id,
    t101.c1900_company_id) AND ROWNUM        = 1;
END gm_fch_parent_account_info;
/****************************************************************
* Description : Procedure used to loop the rep account infromation
* Author      : mmuthusamy
*****************************************************************/
PROCEDURE gm_extract_rep_ac_param (
        p_rep_acc_input_str IN VARCHAR2,
        p_rep_acc_id OUT t704_account.c704_account_id%TYPE,
        p_rep_acc_name OUT t704_account.c704_account_nm%TYPE,
        p_rep_acc_name_en OUT t704_account.c704_account_nm_en%TYPE,
        p_comp_id OUT t704_account.c901_company_id%TYPE,
        p_rep_id OUT t704_account.c703_sales_rep_id%TYPE,
        p_rep_short_nm OUT t704_account.c704_account_sh_name%TYPE,
        p_rep_acc_active_fl OUT t704_account.c704_active_fl%TYPE,
        p_inception_dt OUT t704_account.C704_LAST_UPDATED_DATE%TYPE,
        p_currency OUT t704_account.C901_CURRENCY%TYPE,
        p_dealer_id OUT t704_account.c101_dealer_id%TYPE)
AS
    v_strlen    NUMBER          := NVL (LENGTH (p_rep_acc_input_str), 0) ;
    v_string    VARCHAR2 (4000) := p_rep_acc_input_str;
    v_substring VARCHAR2 (4000) ;
    v_portal_com_date_fmt VARCHAR2 (20) ;
    v_inception_dt VARCHAR2 (20) ;
BEGIN
	 SELECT get_compdtfmt_frm_cntx () INTO v_portal_com_date_fmt FROM dual;
    IF v_strlen > 0 THEN
        --
        v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
        v_string    := SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
        --
        p_rep_acc_id        := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        v_substring         := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        p_rep_acc_name      := TRIM(SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1)) ;
        v_substring         := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        p_rep_acc_name_en      := TRIM(SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1)) ;
        v_substring         := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        p_comp_id           := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        v_substring         := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        p_rep_id            := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        v_substring         := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        p_rep_short_nm      := TRIM(SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1)) ;
        v_substring         := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        p_rep_acc_active_fl := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        v_substring         := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        v_inception_dt      := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        p_inception_dt :=TO_DATE (v_inception_dt, v_portal_com_date_fmt);
        v_substring         := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        p_currency         	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        v_substring         := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        p_dealer_id			:= TRIM(v_substring) ;

    END IF;
END gm_extract_rep_ac_param;
/****************************************************************
* Description : Procedure used to loop the parent account infromation
* Author      : mmuthusamy
*****************************************************************/
PROCEDURE gm_extract_parent_ac_param (
        p_acc_input_str IN VARCHAR2,
        p_parent_party_id OUT t704_account.c101_party_id%TYPE,
        p_parent_party_name OUT t704_account.c704_account_nm%TYPE,
        p_account_type OUT t704_account.c901_account_type%TYPE,
        p_contact_per OUT t704_account.c704_contact_person%TYPE,
        p_phone OUT t704_account.C704_PHONE%TYPE,
        p_fax OUT t704_account.C704_FAX%TYPE,
        p_acc_info OUT t704_account.C704_COMMENTS%TYPE,
        p_bill_name OUT t704_account.c704_bill_name%TYPE,
        p_bill_add1 OUT t704_account.c704_bill_add1%TYPE,
        p_bill_add2 OUT t704_account.c704_bill_add2%TYPE,
        p_bill_city OUT t704_account.c704_bill_city%TYPE,
        p_bill_state OUT t704_account.c704_bill_state%TYPE,
        p_bill_country OUT t704_account.c704_bill_country%TYPE,
        p_bill_zipcode OUT t704_account.c704_bill_zip_code%TYPE,
        p_ship_name OUT t704_account.c704_bill_name%TYPE,
        p_ship_add1 OUT t704_account.c704_bill_add1%TYPE,
        p_ship_add2 OUT t704_account.c704_bill_add2%TYPE,
        p_ship_city OUT t704_account.c704_bill_city%TYPE,
        p_ship_state OUT t704_account.c704_bill_state%TYPE,
        p_ship_country OUT t704_account.c704_bill_country%TYPE,
        p_ship_zipcode OUT t704_account.c704_bill_zip_code%TYPE,
        p_payment_term OUT t704_account.c704_payment_terms%TYPE,
        p_credit_rat OUT t704_account.c704_credit_rating%TYPE)
AS
    v_strlen    NUMBER          := NVL (LENGTH (p_acc_input_str), 0) ;
    v_string    VARCHAR2 (4000) := p_acc_input_str;
    v_substring VARCHAR2 (4000) ;
    --
    v_old_acc_name      VARCHAR2 (4000) ;
    v_out_parent_acc_id VARCHAR2 (20) ;
    v_distid t701_distributor.C701_DISTRIBUTOR_ID%TYPE ;
    v_rep_id t704_account.c703_sales_rep_id%TYPE;
    v_terms VARCHAR2 (20) ;
BEGIN
    IF v_strlen > 0 THEN
        --
        v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
        v_string    := SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
        --
        p_parent_party_id   := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        v_substring         := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        p_parent_party_name := TRIM(SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1)) ;
        v_substring         := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        p_account_type      := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        v_substring         := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        p_contact_per       := TRIM(SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1)) ;
        v_substring         := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        p_phone             := TRIM(SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1)) ;
        v_substring         := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        p_fax               := TRIM(SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1)) ;
        v_substring         := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        p_acc_info          := TRIM(SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1)) ;
        v_substring         := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        --
        p_bill_name    := TRIM(SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1)) ;
        v_substring    := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        p_bill_add1    := TRIM(SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1)) ;
        v_substring    := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        p_bill_add2    := TRIM(SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1)) ;
        v_substring    := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        p_bill_city    := TRIM(SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1)) ;
        v_substring    := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        p_bill_state   := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        v_substring    := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        p_bill_country := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        v_substring    := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        p_bill_zipcode := TRIM(SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1)) ;
        v_substring    := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        --
        p_ship_name    := TRIM(SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1)) ;
        v_substring    := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        p_ship_add1    := TRIM(SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1)) ;
        v_substring    := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        p_ship_add2    := TRIM(SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1)) ;
        v_substring    := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        p_ship_city    := TRIM(SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1)) ;
        v_substring    := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        p_ship_state   := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        v_substring    := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        p_ship_country := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        v_substring    := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        p_ship_zipcode := TRIM(SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1)) ;
        v_substring    := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        --
        p_payment_term := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        v_substring    := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        p_credit_rat   := TRIM(v_substring);
        --
    END IF;
END gm_extract_parent_ac_param;
/****************************************************************
* Description : Procedure used to save parent account infromation
* Author      : mmuthusamy
*****************************************************************/
PROCEDURE gm_sav_acct_info (
        p_rep_acc_id        IN t704_account.c704_account_id%TYPE,
        p_rep_acc_name      IN t704_account.c704_account_nm%TYPE,
        p_rep_acc_name_en   IN t704_account.c704_account_nm_en%TYPE,
        p_rep_id            IN t704_account.c703_sales_rep_id%TYPE,
        p_rep_short_nm      IN t704_account.c704_account_sh_name%TYPE,
        p_inception_dt      IN t704_account.C704_LAST_UPDATED_DATE%TYPE,
        p_rep_acc_active_fl IN t704_account.c704_active_fl%TYPE,
        p_comp_id           IN t704_account.c901_company_id%TYPE,
        p_parent_party_id   IN OUT t704_account.c101_party_id%TYPE,
        p_parent_party_name IN t704_account.c704_account_nm%TYPE,
        p_account_type      IN t704_account.c901_account_type%TYPE,
        p_contact_per       IN t704_account.c704_contact_person%TYPE,
        p_phone             IN t704_account.C704_PHONE%TYPE,
        p_fax               IN t704_account.C704_FAX%TYPE,
        p_acc_info          IN t704_account.C704_COMMENTS%TYPE,
        p_bill_name         IN t704_account.c704_bill_name%TYPE,
        p_bill_add1         IN t704_account.c704_bill_add1%TYPE,
        p_bill_add2         IN t704_account.c704_bill_add2%TYPE,
        p_bill_city         IN t704_account.c704_bill_city%TYPE,
        p_bill_state        IN t704_account.c704_bill_state%TYPE,
        p_bill_country      IN t704_account.c704_bill_country%TYPE,
        p_bill_zipcode      IN t704_account.c704_bill_zip_code%TYPE,
        p_ship_name         IN t704_account.c704_bill_name%TYPE,
        p_ship_add1         IN t704_account.c704_bill_add1%TYPE,
        p_ship_add2         IN t704_account.c704_bill_add2%TYPE,
        p_ship_city         IN t704_account.c704_bill_city%TYPE,
        p_ship_state        IN t704_account.c704_bill_state%TYPE,
        p_ship_country      IN t704_account.c704_bill_country%TYPE,
        p_ship_zipcode      IN t704_account.c704_bill_zip_code%TYPE,
        p_payment_term      IN t704_account.c704_payment_terms%TYPE,
        p_credit_rat        IN t704_account.c704_credit_rating%TYPE,
        p_portal_comp_id    IN t704_account.C1900_COMPANY_ID%TYPE,
        p_currency 			IN t704_account.C901_CURRENCY%TYPE,
        p_user_id           IN t704_account.c704_created_by%TYPE,
        p_dealer_id 		IN t704_account.c101_dealer_id%TYPE)
AS

BEGIN
   
  
     INSERT
       INTO t704_account
        (
            c704_account_id, c704_account_nm, c704_account_nm_en, c703_sales_rep_id
          , c704_account_sh_name, c704_inception_date, c704_active_fl
          , c901_company_id, c704_price_cat_fl, c101_party_id
          , c901_account_type, c704_contact_person, C704_PHONE
          , C704_FAX, C704_COMMENTS, c704_payment_terms
          , c704_credit_rating, c704_created_by, c704_created_date
          , c704_bill_name, c704_ship_name, c704_bill_add1
          , c704_ship_add1, c704_bill_add2, c704_ship_add2
          , c704_bill_city, c704_ship_city, c704_bill_state
          , c704_ship_state, c704_bill_country, c704_ship_country
          , c704_bill_zip_code, c704_ship_zip_code, C1900_COMPANY_ID, C901_CURRENCY,c101_dealer_id
        )
        VALUES
        (
            p_rep_acc_id, p_rep_acc_name,p_rep_acc_name_en, p_rep_id
          , p_rep_short_nm, p_inception_dt, p_rep_acc_active_fl
          , p_comp_id, 'Y', p_parent_party_id
          , p_account_type, p_contact_per, p_phone
          , p_fax, p_acc_info, p_payment_term
          , p_credit_rat, p_user_id, CURRENT_DATE
          , p_bill_name, p_ship_name, p_bill_add1
          , p_ship_add1, p_bill_add2, p_ship_add2
          , p_bill_city, p_ship_city, p_bill_state
          , p_ship_state, p_bill_country, p_ship_country
          , p_bill_zipcode, p_ship_zipcode, p_portal_comp_id, p_currency, p_dealer_id
        ) ;
    --
END gm_sav_acct_info;
/****************************************************************
* Description : Procedure used to update rep account information
* Author      : mmuthusamy
*****************************************************************/
PROCEDURE gm_upd_rep_acct_info
    (
        p_rep_acc_id        IN t704_account.c704_account_id%TYPE,
        p_rep_acc_name      IN t704_account.c704_account_nm%TYPE,
        p_rep_acc_name_en   IN t704_account.c704_account_nm_en%TYPE,
        p_rep_id            IN t704_account.c703_sales_rep_id%TYPE,
        p_rep_short_nm      IN t704_account.c704_account_sh_name%TYPE,
        p_inception_dt      IN t704_account.C704_LAST_UPDATED_DATE%TYPE,
        p_rep_acc_active_fl IN t704_account.c704_active_fl%TYPE,
        p_comp_id           IN t704_account.c901_company_id%TYPE,
        p_currency 			IN t704_account.C901_CURRENCY%TYPE,
        p_user_id           IN t704_account.c704_created_by%TYPE,
        p_dealer_id 		IN t704_account.c101_dealer_id%TYPE
    )
AS
  
    v_old_comp_id t704_account.c901_company_id%TYPE;
BEGIN
    --
     SELECT c901_company_id
       INTO v_old_comp_id
       FROM t704_account
      WHERE c704_account_id = p_rep_acc_id;
    --
    IF v_old_comp_id <> p_comp_id THEN
        raise_application_error ('-20521', '') ;
    END IF;
    --
     
     UPDATE t704_account
    SET c704_account_nm        = p_rep_acc_name, c704_account_nm_en = p_rep_acc_name_en,c703_sales_rep_id = p_rep_id, c704_account_sh_name = p_rep_short_nm
      , c901_company_id        = p_comp_id, c704_inception_date = p_inception_dt,
        c704_active_fl         = p_rep_acc_active_fl, c704_price_cat_fl = 'Y', c704_last_updated_by = p_user_id
      , c704_last_updated_date = CURRENT_DATE
      ,	c901_currency = p_currency
      , c101_dealer_id = p_dealer_id
      WHERE c704_account_id    = p_rep_acc_id;
END gm_upd_rep_acct_info;
/****************************************************************
* Description : Procedure used to save rep account information
* Author      : mmuthusamy
*****************************************************************/
PROCEDURE gm_upd_acct_info (
        p_rep_acc_id        IN t704_account.c704_account_id%TYPE,
        p_parent_party_id   IN t704_account.c101_party_id%TYPE,
        p_parent_party_name IN t704_account.c704_account_nm%TYPE,
        p_account_type      IN t704_account.c901_account_type%TYPE,
        p_contact_per       IN t704_account.c704_contact_person%TYPE,
        p_phone             IN t704_account.C704_PHONE%TYPE,
        p_fax               IN t704_account.C704_FAX%TYPE,
        p_acc_info          IN t704_account.C704_COMMENTS%TYPE,
        p_bill_name         IN t704_account.c704_bill_name%TYPE,
        p_bill_add1         IN t704_account.c704_bill_add1%TYPE,
        p_bill_add2         IN t704_account.c704_bill_add2%TYPE,
        p_bill_city         IN t704_account.c704_bill_city%TYPE,
        p_bill_state        IN t704_account.c704_bill_state%TYPE,
        p_bill_country      IN t704_account.c704_bill_country%TYPE,
        p_bill_zipcode      IN t704_account.c704_bill_zip_code%TYPE,
        p_ship_name         IN t704_account.c704_bill_name%TYPE,
        p_ship_add1         IN t704_account.c704_bill_add1%TYPE,
        p_ship_add2         IN t704_account.c704_bill_add2%TYPE,
        p_ship_city         IN t704_account.c704_bill_city%TYPE,
        p_ship_state        IN t704_account.c704_bill_state%TYPE,
        p_ship_country      IN t704_account.c704_bill_country%TYPE,
        p_ship_zipcode      IN t704_account.c704_bill_zip_code%TYPE,
        p_payment_term      IN t704_account.c704_payment_terms%TYPE,
        p_credit_rat        IN t704_account.c704_credit_rating%TYPE,
        p_portal_comp_id    IN t704_account.C1900_COMPANY_ID%TYPE,
        p_user_id           IN t704_account.c704_created_by%TYPE)
AS
BEGIN
    --
     UPDATE t704_account
    SET c901_account_type    = DECODE (p_account_type, 0, NULL, p_account_type), c101_party_id = p_parent_party_id,
        c704_contact_person  = p_contact_per, c704_phone = p_phone, c704_fax = p_fax
      , c704_comments        = p_acc_info, c704_bill_name = p_bill_name, c704_ship_name = p_ship_name
      , c704_bill_add1       = p_bill_add1, c704_ship_add1 = p_ship_add1, c704_bill_add2 = p_bill_add2
      , c704_ship_add2       = p_ship_add2, c704_bill_city = p_bill_city, c704_ship_city = p_ship_city
      , c704_bill_state      = p_bill_state, c704_ship_state = p_ship_state, c704_bill_country = p_bill_country
      , c704_ship_country    = p_ship_country, c704_bill_zip_code = p_bill_zipcode, c704_ship_zip_code = p_ship_zipcode
      , c704_payment_terms   = p_payment_term, C704_credit_rating = p_credit_rat, C1900_COMPANY_ID = p_portal_comp_id
      , c704_last_updated_by = p_user_id, c704_last_updated_date = CURRENT_DATE
      WHERE c704_account_id  = p_rep_acc_id;
END gm_upd_acct_info;
/****************************************************************
* Description : Procedure used to save the account attribute values
* Author      : mmuthusamy
*****************************************************************/
PROCEDURE gm_sav_acct_attr (
        p_accid        IN t704_account.c704_account_id%TYPE,
        p_acc_att_val  IN T704A_ACCOUNT_ATTRIBUTE.c704a_attribute_value%TYPE,
        p_acc_att_type IN T704A_ACCOUNT_ATTRIBUTE.c901_attribute_type%TYPE,
        p_userid       IN t704a_account_attribute.c704a_created_by%TYPE)
AS
    v_account_attr_id t704a_account_attribute.c704a_account_attribute_id%TYPE;
BEGIN
    -- to update the account attribute value
     UPDATE t704a_account_attribute
    SET c704a_attribute_value   = p_acc_att_val, c704a_last_updated_by = p_userid, c704a_last_updated_date = CURRENT_DATE
      WHERE c704_account_id     = p_accid
        AND c901_attribute_type = p_acc_att_type;
    --
    IF (SQL%ROWCOUNT = 0) THEN
         SELECT s704a_account_attribute.NEXTVAL INTO v_account_attr_id FROM DUAL;
         INSERT
           INTO T704A_ACCOUNT_ATTRIBUTE
            (
                c704a_account_attribute_id, c704_account_id, c704a_attribute_value
              , c704a_created_by, c704a_created_date, c901_attribute_type
            )
            VALUES
            (
                v_account_attr_id, p_accid, p_acc_att_val
              , p_userid, CURRENT_DATE, p_acc_att_type
            ) ;
    END IF;
END gm_sav_acct_attr;
/****************************************************************
* Description : Procedure used to sync account attribute based on parent account details
* Author      : mmuthusamy
*****************************************************************/
PROCEDURE gm_sav_sync_acct_attr
    (
        p_accid        IN t704_account.c704_account_id%TYPE,
        p_acc_att_type IN VARCHAR2,
        p_userid       IN t704a_account_attribute.c704a_created_by%TYPE
    )
AS
    v_party_id t704_account.c101_party_id%TYPE;
    v_account_id t704_account.c704_account_id%TYPE;
    --
    v_acc_att_val T704A_ACCOUNT_ATTRIBUTE.c704a_attribute_value%TYPE;
    --
    CURSOR v_account_info
    IS
         SELECT c704_account_id id
           FROM t704_account
          WHERE c101_party_id    = v_party_id
            AND c704_account_id <> p_accid
            AND c704_void_fl    IS NULL;
    ---
    CURSOR v_account_att
    IS
         SELECT c704_account_id acc_id, c704a_attribute_value att_val, c901_attribute_type att_type
           FROM T704A_ACCOUNT_ATTRIBUTE
          WHERE c704_account_id      = p_accid
            AND c901_attribute_type IN
            (
                 SELECT token FROM v_in_list
            )
        AND c704a_void_fl IS NULL;
    --
BEGIN
    --
    my_context.set_my_inlist_ctx (p_acc_att_type) ;
    --
    BEGIN
         SELECT c101_party_id
           INTO v_party_id
           FROM t704_account
          WHERE c704_account_id = p_accid;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_party_id := NULL;
    END;
    --
    FOR v_acc_info IN v_account_info
    LOOP
        v_account_id := v_acc_info.id;
        --
        FOR v_acc_att IN v_account_att
        LOOP
            v_acc_att_val := v_acc_att.att_val;
            --
            gm_sav_acct_attr (v_account_id, v_acc_att_val, v_acc_att.att_type, p_userid) ;
        END LOOP;
    END LOOP;
END gm_sav_sync_acct_attr;
/****************************************************************
* Description : Procedure used to sync parent account attribute based on account
* Author      : mmuthusamy
*****************************************************************/
PROCEDURE gm_sav_sync_parent_acct_attr (
        p_accid        IN t704_account.c704_account_id%TYPE,
        p_acc_att_type IN VARCHAR2,
        p_userid       IN t704a_account_attribute.c704a_created_by%TYPE)
AS
    v_party_id t704_account.c101_party_id%TYPE;
    v_account_id t704_account.c704_account_id%TYPE;
    v_accid t704_account.c704_account_id%TYPE;
    --
    v_new_parent_fl VARCHAR2 (5) := 'N';
    v_acc_att_val T704A_ACCOUNT_ATTRIBUTE.c704a_attribute_value%TYPE;
    --
    v_acc_att_cnt NUMBER;
    -- using this cursor to fetch all the accounts based on party
    CURSOR v_account_info_cur
    IS
         SELECT c704_account_id id
           FROM t704_account
          WHERE c101_party_id = v_party_id
            AND c704_void_fl IS NULL;
    -- using this cursor to fetch the new party account attribute value
    CURSOR v_account_att_cur
    IS
         SELECT c704_account_id acc_id, c704a_attribute_value att_val, c901_attribute_type att_type
           FROM T704A_ACCOUNT_ATTRIBUTE
          WHERE c704_account_id      = v_accid
            AND c901_attribute_type IN
            (
                 SELECT token FROM v_in_list
            )
        AND c704a_void_fl IS NULL;
    -- using this cursor to fetch the new party account empty attribute type
    CURSOR v_empty_account_att_cur
    IS
         SELECT c901_attribute_type att_type
           FROM T704A_ACCOUNT_ATTRIBUTE
          WHERE c704_account_id      = p_accid
            AND c901_attribute_type IN
            (
                 SELECT token FROM v_in_list
            )
        AND c704a_void_fl IS NULL;
BEGIN
    --
    my_context.set_my_inlist_ctx (p_acc_att_type) ;
    -- based on account to get the party id
    BEGIN
         SELECT c101_party_id
           INTO v_party_id
           FROM t704_account
          WHERE c704_account_id = p_accid;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_party_id := NULL;
    END;
         

    -- based on the party to get the existing account id
    BEGIN
         SELECT c704_account_id
           INTO v_accid
           FROM t704_account
          WHERE c101_party_id    = v_party_id
            AND c704_account_id <> p_accid
            AND c704_void_fl    IS NULL
            AND ROWNUM           = 1;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_accid         := p_accid;
        v_new_parent_fl := 'Y';
    END;
    -- getting - party account attribute count
     SELECT COUNT (1)
       INTO v_acc_att_cnt
       FROM T704A_ACCOUNT_ATTRIBUTE
      WHERE c704_account_id      = v_accid
        AND c901_attribute_type IN
        (
             SELECT token FROM v_in_list
        )
        AND c704a_void_fl IS NULL;
    -- party account count is 0 then no attribute mapped. 
    -- To set the null values
    IF v_acc_att_cnt = 0 THEN
        FOR v_empty     IN v_empty_account_att_cur
        LOOP
            gm_sav_acct_attr (p_accid, NULL, v_empty.att_type, p_userid) ;
        END LOOP;
    END IF;
    FOR v_acc_info IN v_account_info_cur
    LOOP
        v_account_id := v_acc_info.id;
        --
        FOR v_acc_att IN v_account_att_cur
        LOOP
            v_acc_att_val := v_acc_att.att_val;
            --
            IF v_new_parent_fl = 'Y' THEN
                v_acc_att_val := NULL;
            END IF;
            --
            gm_sav_acct_attr (v_account_id, v_acc_att_val, v_acc_att.att_type, p_userid) ;
        END LOOP;
    END LOOP;
   
END gm_sav_sync_parent_acct_attr;
/****************************************************************
* Description : Procedure used to sync GPO accounts based on parent account details from Pricing parameter Screen
* Author      : Mihir
*****************************************************************/
PROCEDURE gm_sav_sync_gpo_acct
    (
        p_accid        IN t704_account.c704_account_id%TYPE,
        p_gpo_id	 IN VARCHAR2,
        p_userid       IN t704a_account_attribute.c704a_created_by%TYPE
    )
AS
    v_party_id t704_account.c101_party_id%TYPE;
   --
    CURSOR v_account_info
    IS
         SELECT c704_account_id id
           FROM t704_account
          WHERE c101_party_id    = v_party_id
            AND c704_account_id <> p_accid
            AND c704_void_fl    IS NULL;
      --
BEGIN
   
    BEGIN
         SELECT c101_party_id
           INTO v_party_id
           FROM t704_account
          WHERE c704_account_id = p_accid;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_party_id := NULL;
    END;
    --
    FOR v_acc_info IN v_account_info
    LOOP
       gm_pkg_sm_pricparam_setup.gm_sav_grp_account_mapping(v_acc_info.id,NULL,p_gpo_id,p_userid);
    
    END LOOP;
END gm_sav_sync_gpo_acct;
/****************************************************************
* Description : Procedure used to sync GPO accounts based on parent account details from basic info screen.
* Author      : Mihir
*****************************************************************/
PROCEDURE gm_sav_sync_parent_gpo_acct
    (
        p_accid        IN t704_account.c704_account_id%TYPE,
        p_userid       IN t704a_account_attribute.c704a_created_by%TYPE
    )
AS
    v_party_id t704_account.c101_party_id%TYPE;
    v_accid t704_account.c704_account_id%TYPE;
   v_gpo_id t740_gpo_account_mapping.c101_party_id%TYPE:=0;
   v_no_map_fl VARCHAR2(1):='N';
   --
BEGIN
   
    BEGIN
         SELECT c101_party_id
           INTO v_party_id
           FROM t704_account
          WHERE c704_account_id = p_accid;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_party_id := NULL;
    END;
    --
     BEGIN
       SELECT c704_account_id
           INTO v_accid
           FROM t704_account
          WHERE c101_party_id    = v_party_id
            AND c704_account_id <> p_accid
            AND c704_void_fl    IS NULL
            AND ROWNUM           = 1;
       EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_accid := NULL;
    END;
    
    BEGIN
       SELECT c101_party_id 
		INTO v_gpo_id 
		FROM t740_gpo_account_mapping
		WHERE c704_account_id = v_accid
		AND C740_VOID_FL IS NULL;
		  EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_gpo_id := 0;
        v_no_map_fl :='Y';
    END;
		
	IF v_no_map_fl= 'N'
	THEN
    	gm_pkg_sm_pricparam_setup.gm_sav_grp_account_mapping(p_accid,NULL,v_gpo_id,p_userid);
  	END IF;
END gm_sav_sync_parent_gpo_acct;
END gm_pkg_sm_acct_trans_temp;
/