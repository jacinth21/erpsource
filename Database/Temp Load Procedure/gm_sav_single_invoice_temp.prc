CREATE OR REPLACE PROCEDURE gm_sav_single_invoice_temp (
	p_custpo	   IN		t503_invoice.c503_customer_po%TYPE
  , p_accid 	   IN		t503_invoice.c704_account_id%TYPE
  , p_payterms	   IN		t503_invoice.c503_terms%TYPE
  , p_invdate	   IN		DATE
  , p_userid	   IN		t503_invoice.c503_created_by%TYPE
  , p_action	   IN		VARCHAR2
  , p_invtype	   IN		t503_invoice.c901_invoice_type%TYPE
  , p_inv_source   IN		t503_invoice.c901_invoice_source%TYPE
  , p_shiptoid     IN           t501_order.C501_SHIP_TO_ID%TYPE
  , p_ship_dt	   IN		VARCHAR2
  , p_inv_id	   IN		t503_invoice.c503_invoice_id%TYPE
	)
AS

	v_invoice_id   VARCHAR2 (20);
	v_cnt		   NUMBER;
	v_vat_fl	   VARCHAR2(5);
	v_con_vat_fl   VARCHAR2(5);	
	v_company_id   t1900_company.c1900_company_id%TYPE;
	v_division_id  t1910_division.c1910_division_id%TYPE;
	v_rep_id	   t703_sales_rep.c703_sales_rep_id%TYPE;
	v_order_date   DATE;
	
		CURSOR orderlog_cur
	IS
     	SELECT c501_order_id orderid
       	  FROM t501_order
      	WHERE c503_invoice_id  = v_invoice_id
          	AND c503_invoice_id IS NOT NULL
          	AND c501_void_fl    IS NULL;
          	
BEGIN
    
	-- Company needs to be taken from the Account and not from Context.
	IF p_accid IS NOT NULL
	THEN
		SELECT C1900_COMPANY_ID 
	      INTO v_company_id  
	      FROM T704_ACCOUNT
	     WHERE C704_ACCOUNT_ID = p_accid;
	ELSE
	SELECT get_compid_frm_cntx() 
      INTO v_company_id  
      FROM dual;
	END IF;
     
	-- getting rep id
	v_rep_id := TRIM(get_rep_id(p_accid));
	-- getting the division Id
	v_division_id := gm_pkg_sm_mapping.get_salesrep_division(v_rep_id);
	
  --If invoice generate date is prior to order created date .validating the invoice date
	         SELECT MIN(c501_order_date) 
	           INTO v_order_date
		       FROM t501_order t501
		      WHERE  t501.c704_account_id =  p_accid
		        AND t501.c501_customer_po = p_custpo
		        AND t501.c501_delete_fl      IS NULL
		        AND t501.c501_void_fl        IS NULL
		        AND T501.C503_INVOICE_ID IS NULL
		        AND NVL (c901_order_type, -9999) NOT IN (
               SELECT t906.c906_rule_value
                  FROM t906_rules t906
                 WHERE t906.c906_rule_grp_id = 'ORDERTYPE'
                   AND c906_rule_id = 'EXCLUDE');
	
	IF v_order_date > p_invdate OR CURRENT_DATE < p_invdate
	THEN
		raise_application_error (-20672, '');
	END IF;
	

	SELECT COUNT (1)
	  INTO v_cnt
	  FROM t501_order
	 WHERE c501_customer_po = p_custpo 
	   AND c704_account_id = p_accid 
	   AND c501_void_fl IS NULL
	   AND c501_hold_fl IS NOT NULL;
	-- Yoga comment
	IF  1=2--v_cnt > 0
	THEN
		-- Error Messaage. Cannot Invoice this PO as atleast one DO is on Hold Status.
		raise_application_error (-20888, '');
	ELSE
				
		--PMT_8548:Function to get invoice # based on company id and source
--		SELECT get_invoice_id(v_company_id,p_inv_source)
--			INTO v_invoice_id
--		FROM DUAL;	  

		v_invoice_id := p_inv_id; 
		
		--yoga comment: If invoice already available don't create
		begin
			select c503_invoice_id into v_invoice_id 
			from t503_invoice 
			where  c503_invoice_id = v_invoice_id;
			
		exception  when no_data_found then
			v_invoice_id := p_inv_id;
			
			INSERT INTO t503_invoice
					(c503_invoice_id, c704_account_id, c503_invoice_date, c503_terms, c503_customer_po
				   , c503_created_by, c503_created_date, c503_status_fl, c901_invoice_type, c901_invoice_source,c1900_company_id
				   , c1910_division_id
					)
			 VALUES (v_invoice_id, p_accid, p_invdate, p_payterms, p_custpo
				   , p_userid, CURRENT_DATE, 0, p_invtype, p_inv_source	,v_company_id -- 50255 - generic invoice code
				   , v_division_id
					);
		end;
		
		

/************
   IF p_action = 'ADD'
   THEN
	  UPDATE t501_order
		 SET c503_invoice_id = v_invoice_id
	   WHERE c501_customer_po = p_custpo AND c704_account_id = p_accid AND c503_invoice_id IS NULL;
   END IF;
**************/
		IF p_action = 'ADD'
		THEN
			UPDATE t501_order
			   SET c503_invoice_id = v_invoice_id
			   	 , c501_last_updated_by = p_userid
				 , c501_last_updated_date = CURRENT_DATE
			 WHERE c501_customer_po = p_custpo AND c704_account_id = p_accid AND c503_invoice_id IS NULL AND (c901_ext_country_id IS NULL
			 OR c901_ext_country_id  in (select country_id from v901_country_codes))
			 AND c501_void_fl is null
			 AND NVL(C501_SHIP_TO_ID,'-9999') = NVL(p_shiptoid,NVL(C501_SHIP_TO_ID,'-9999'))
			 AND DECODE(p_ship_dt , NULL, '-999', TRUNC(c501_shipping_date)) = NVL(p_ship_dt , '-999')
			 AND NVL (c901_order_type, -9999) NOT IN (
                SELECT t906.c906_rule_value
                  FROM t906_rules t906
                 WHERE t906.c906_rule_grp_id = 'ORDERTYPE'
                   AND c906_rule_id = 'EXCLUDE')	;

			--The Closed Status update statement is moved to the below mentioned procedure,This procedure will be called when we are doing issue credit /Debit.
			--This code change is done as part of PMT-5091.
			gm_pkg_ac_invoice.gm_upd_invoice_status(p_inv_source,v_invoice_id,p_userid);

			SELECT COUNT (1)
			  INTO v_cnt
			  FROM t906_rules t906, t906_rules rul
			 WHERE UPPER (t906.c906_rule_grp_id) = 'APPLN'
			   AND UPPER (t906.c906_rule_value) = UPPER (rul.c906_rule_grp_id)
			   AND UPPER (rul.c906_rule_id) = 'VAT' 
			   AND t906.C906_VOID_FL is null ;
            
			--If Exclude VAT given 'Y' in mapp account param screen then VAT not calculated			    
			v_vat_fl := NVL(GET_ACCOUNT_ATTRB_VALUE(p_accid,'101183'),'N');  
			
			IF v_cnt > 0
			THEN
			  IF v_vat_fl <> 'Y' 
				THEN
					v_con_vat_fl := NVL(get_rule_value (v_company_id, 'CONSTRUCT_VAT_FL'),'N');
					IF v_con_vat_fl = 'Y'  
					THEN
						gm_pkg_ac_order.gm_sav_construct_vatrate (v_invoice_id, p_custpo, p_accid, 50205);
					ELSE
						gm_pkg_ac_order.gm_sav_vatrate (v_invoice_id, p_custpo, p_accid, 50205);
					END IF;
				END IF;
			END IF;
			FOR orderlog IN orderlog_cur
			LOOP
			gm_pkg_cm_status_log.gm_sav_status_details (orderlog.orderid, '', p_userid, NULL, CURRENT_DATE, 91179, 91102);
			END LOOP;
		END IF;
	END IF;

	--p_inv_id	:= v_invoice_id;
END gm_sav_single_invoice_temp;
/