--@C:\PC\ERPJOBS\Database\packages\operations\gm_pkg_op_reconfig_loaner.bdy;

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_reconfig_loaner
IS  
  
	/******************************************************************
	 * Description : Procedure for fetch pending check loaner details 
	 * Author 	 : tramasamy
	 ****************************************************************/
	PROCEDURE gm_fch_consign_dtls (
	p_status_id  IN    t504a_consignment_loaner.c504a_status_fl%TYPE,
	p_company_id  IN    t1900_company.c1900_company_id%TYPE,
	p_plant_id    IN    T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE,
	p_consign_dtls OUT  TYPES.cursor_type 
	)
	AS
	 v_company_id   t1900_company.c1900_company_id%TYPE;
	 v_plant_id T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;   
   BEGIN
	   SELECT get_compid_frm_cntx(),get_plantid_frm_cntx()
            INTO v_company_id,v_plant_id
            FROM DUAL;
	
	OPEN p_consign_dtls FOR
	
	   SELECT t504.c504_consignment_id CONID ,t504.c704_account_id ACCID  
         FROM t504_consignment t504, t504a_consignment_loaner t504a
        WHERE t504.c504_status_fl = '4' 
          AND t504.c504_verify_fl = '1'  
			--AND t504.c504_consignment_id = t504b.c504_consignment_id(+) 
		  AND (t504.C5040_PLANT_ID     = p_plant_id  OR t504.c1900_company_id   = p_company_id)  
		  AND T504.C504_VOID_FL IS NULL  
		  AND T504a.C504A_VOID_FL is null
		  AND t504.c207_set_id IS NOT NULL 
		  AND T504.C5040_PLANT_ID = T504A.C5040_PLANT_ID  
		  AND t504.c504_type IN (4127,4119) 
		  AND t504a.c504a_status_fl = p_status_id -- 25--pending check ,58-pending putaway, 0-Available,'20 - Pending return
		  AND t504.c504_consignment_id = t504a.c504_consignment_id; 
				
	END gm_fch_consign_dtls;


/******************************************************************
* Description : Procedure for updating and inserting printer and user mapping
* Author   : gpalani
****************************************************************/
PROCEDURE gm_update_printer_user(
    p_print_user_id IN t504p_loaner_user_print_mapping.c504p_user_id %TYPE,
    p_printer_nm    IN t504p_loaner_user_print_mapping.c504p_printer_name%TYPE,
    p_user_id       IN t504p_loaner_user_print_mapping.c504p_created_by%TYPE)
	
AS

  v_company_id t1900_company.c1900_company_id%TYPE;
  v_plant_id T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
  
BEGIN

  UPDATE t504p_loaner_user_print_mapping
  SET c504p_printer_name    =p_printer_nm,
    C504P_LAST_UPDATED_BY   =p_user_id,
    C504P_LAST_UPDATED_DATE = CURRENT_DATE
  WHERE c504p_user_id       =p_print_user_id
  AND C504P_VOID_FL        IS NULL;
  
  IF (SQL%ROWCOUNT = 0) THEN
  
    INSERT
    INTO GLOBUS_APP.T504P_LOANER_USER_PRINT_MAPPING
      (
        C504P_LOANER_USER_PRINT_MAPPING_ID ,
        C504P_USER_ID,
        C504P_PRINTER_NAME,
        C504P_CREATED_DATE,
        C504P_CREATED_BY
      )
      VALUES
      (
        S504P_LOANER_USER_PRINT_MAPPING.NEXTVAL,
        p_print_user_id,
        p_printer_nm,
        CURRENT_DATE,
        p_user_id
      );
  END IF;
  
END gm_update_printer_user;
/******************************************************************
* Description : Procedure for updating rule value for loaner flag
* Author   : gpalani
****************************************************************/
PROCEDURE gm_update_loaner_auto_print
  (
    p_loaner_status_fl IN t906_rules.c906_rule_value%TYPE,
    p_rule_grp_id      IN t906_rules.c906_rule_grp_id%TYPE,
    p_user_id          IN t504p_loaner_user_print_mapping.c504p_created_by%TYPE,
	p_company_id	   IN t1900_company.c1900_company_id%TYPE
  )

AS

  v_company_id t1900_company.c1900_company_id%TYPE;
  v_plant_id T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
  
BEGIN

  UPDATE t906_rules
  SET c906_rule_value      =p_loaner_status_fl,
    C906_last_updated_by   =p_user_id,
    c906_last_updated_date = CURRENT_DATE
  WHERE c906_rule_grp_id   =p_rule_grp_id
  AND c906_void_fl        IS NULL
  AND c1900_company_id=p_company_id;
  
  END gm_update_loaner_auto_print;
	  
END gm_pkg_op_reconfig_loaner;
/