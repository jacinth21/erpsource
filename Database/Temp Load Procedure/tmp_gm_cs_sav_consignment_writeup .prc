/* Formatted on 2009/03/20 13:45 (Formatter Plus v4.8.0) */

CREATE OR REPLACE PROCEDURE tmp_gm_cs_sav_cons_writeup (
	p_setid    t207_set_master.c207_set_id%TYPE
  , p_distid   t701_distributor.c701_distributor_id%TYPE
)
/******************************************************************************
 * Description : This procedure is written as temp solution to create consignment 
 *				 if parts are not shipped and the same in identified in field audit
 *				-------------------------------------------------
 *				Below procedure will consign new part to field sales 
 *				p_consignment_id		- consignment
 *				
 *				sample exec statement 
 *				exec tmp_gm_cs_sav_cons_writeup('910.902',71);
 *			
 *				Sample select statement 
 *				select  t504.c504_consignment_id, t504.c207_set_id, GET_DISTRIBUTOR_NAME(t504.C701_DISTRIBUTOR_ID)
			, ( SELECT SUM(C810_CR_AMT * C810_QTY) FROM  T810_OTHER_TXN T810
				WHERE T810.C810_TXN_ID  = t504.c504_consignment_id )
			, t504.c504_verified_date
			from t504_consignment t504
			where t504.c504_verified_date = trunc(sysdate)
			and c504_created_by = '30301'
 *******************************************************************************/
AS
	v_request_id   t520_request.c520_request_id%TYPE;
	v_consignment_id t504_consignment.c504_consignment_id%TYPE;
	v_item_consigment_id t505_item_consignment.c505_item_consignment_id%TYPE;
	v_shipid	   t907_shipping_info.c907_shipping_id%TYPE;

	CURSOR c_get_setdtl
	IS
		SELECT	 t208.c207_set_id ID, t208.c205_part_number_id pnum, NVL (t208.c208_set_qty, 0) qty
			   , t208.c208_set_details_id mid, get_partnum_desc (t208.c205_part_number_id) pdesc
			   , t208.c208_inset_fl insetfl, NVL (get_part_price (t208.c205_part_number_id, 'L'), 0) lprice
			FROM t208_set_details t208
		   WHERE t208.c208_void_fl IS NULL
			 AND t208.c207_set_id = p_setid
			 AND t208.c208_set_qty <> 0
			 AND t208.c208_inset_fl = 'Y'
		ORDER BY t208.c205_part_number_id, t208.c208_critical_fl DESC, t208.c208_inset_fl DESC;
BEGIN
	SELECT 'GM-RQ-' || s520_request.NEXTVAL
	  INTO v_request_id
	  FROM DUAL;

	INSERT INTO t520_request
				(c520_created_date, c901_request_source, c520_required_date, c520_request_to, c520_status_fl
			   , c901_ship_to, c520_request_id, c520_created_by, c520_request_for, c207_set_id, c520_ship_to_id
			   , c520_request_date, c901_request_by_type
				)
		 VALUES (SYSDATE, 50618, TRUNC (SYSDATE), p_distid, 40
			   , 4120, v_request_id, '30301', 40021, p_setid, p_distid
			   , TRUNC (SYSDATE), 50626
				);

	SELECT get_next_consign_id ('consignment')
	  INTO v_consignment_id
	  FROM DUAL;

	INSERT INTO t504_consignment
				(c504_verified_date, c504_delivery_carrier, c520_request_id, c504_consignment_id, c504_delivery_mode
			   , c504_status_fl, c504_type, c504_ship_to, c504_created_by, c701_distributor_id, c504_ship_to_id
			   , c504_update_inv_fl, c504_ship_req_fl, c207_set_id, c504_ship_date, c504_verify_fl, c504_created_date
			   , c504_comments
				)
		 VALUES (TRUNC (SYSDATE), 5040, v_request_id, v_consignment_id, 5031
			   , 4, 4110, 4120, '30301', p_distid, p_distid
			   , '1', '1', p_setid, TRUNC (SYSDATE), '1', SYSDATE
			   , 'Consignment Write-up for Great Lakes'
				);

	SELECT s907_ship_id.NEXTVAL
	  INTO v_shipid
	  FROM DUAL;

	INSERT INTO t907_shipping_info
				(c907_shipping_id, c907_ref_id, c901_ship_to, c907_ship_to_id, c907_ship_to_dt, c901_source
			   , c901_delivery_mode, c901_delivery_carrier, c907_status_fl, c907_created_by, c907_created_date
			   , c907_active_fl, c907_release_dt, c907_shipped_dt
				)
		 VALUES (v_shipid, v_consignment_id, 4120, p_distid, TRUNC (SYSDATE), 50181
			   , 5031, 5040, 40, '30301', SYSDATE
			   , 'N', TRUNC (SYSDATE), TRUNC (SYSDATE)
				);

	FOR rec IN c_get_setdtl
	LOOP
		SELECT s504_consign_item.NEXTVAL
		  INTO v_item_consigment_id
		  FROM DUAL;

		INSERT INTO t505_item_consignment
					(c505_item_price, c504_consignment_id, c505_item_consignment_id, c505_item_qty
				   , c205_part_number_id, c505_control_number
					)
			 VALUES (rec.lprice, v_consignment_id, v_item_consigment_id, rec.qty
				   , rec.pnum, 'NOC#'
					);

		--Posting
		gm_save_ledger_posting (4839
							  , SYSDATE
							  , rec.pnum
							  , p_distid
							  , v_consignment_id
							  , rec.qty
							  , NVL(get_ac_cogs_value (rec.pnum, 4904) ,  get_ac_cogs_value (rec.pnum, 4900))
							  , '30301'
							   );
	END LOOP;
END tmp_gm_cs_sav_cons_writeup;
/
