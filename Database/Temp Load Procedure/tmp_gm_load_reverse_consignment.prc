/* Formatted on 2009/04/17 12:32 (Formatter Plus v4.8.0) */
-- @"C:\Database\Temp Load Procedure\tmp_gm_load_reverse_consignment.prc";

CREATE OR REPLACE PROCEDURE tmp_gm_load_reverse_cons (
/******************************************************************************
 * Description : This procedure is written as temp solution to reverse consignment 
 *				 Created as part of Audit -- Physical Adjustment 
 *				-------------------------------------------------
 *				Below procedure will reverse and void the consignment 
 *		p_consignment_id		- consignment
 *
 *
  Sample SQL 
  ===========
 select * from t413_inhouse_trans_items where c412_inhouse_trans_id =
'GM-LN-125509' and c205_part_number_id = '150.226';

exec tmp_load_gm_reconciliation(140468,'150.226',1,'S','R');

 select * from t413_inhouse_trans_items where c412_inhouse_trans_id =
'GM-LN-125509' and c205_part_number_id = '150.226';


 *******************************************************************************/
	p_consignment_id   t504_consignment.c504_consignment_id%TYPE
)
AS
	v_string	   VARCHAR2 (20);

	v_distributor_id t504_consignment.c701_distributor_id%TYPE;
	v_consignment_id t504_consignment.c504_consignment_id%TYPE;

	CURSOR v_get_item_consigment_cur
	IS
		SELECT t505.c205_part_number_id part_number_id, t505.c505_item_qty item_qty, t505.c505_item_price item_price
		  FROM t505_item_consignment t505
		 WHERE t505.c504_consignment_id = p_consignment_id AND TRIM (t505.c504_consignment_id) IS NOT NULL;
BEGIN
	SELECT c701_distributor_id
	  INTO v_distributor_id
	  FROM t504_consignment
	 WHERE c504_consignment_id = p_consignment_id;

	DBMS_OUTPUT.put_line ('Distributor ID' || v_distributor_id);
	DBMS_OUTPUT.put_line ('Distributor NAME' || GET_DISTRIBUTOR_NAME(v_distributor_id));
	DBMS_OUTPUT.put_line ('Consignment ID' || p_consignment_id);

	FOR recs IN v_get_item_consigment_cur
	LOOP
		gm_save_ledger_posting (4841   -- Suchi
										 , SYSDATE
										 , recs.part_number_id	 -- part#
										 , v_distributor_id   -- DistributorID
										 , p_consignment_id   --consignment_id
										 , recs.item_qty   --QTY
										 , get_ac_cogs_value (recs.part_number_id, 4904)   --
										 , 30301   -- 30003
										  );
	END LOOP;

	DBMS_OUTPUT.put_line ('Posting Completed.....' );

	UPDATE t504_consignment
	   SET c504_void_fl = 'Y'
		 , c504_last_updated_by = 30301
		 , c504_last_updated_date = SYSDATE
	 WHERE c504_consignment_id = p_consignment_id;

	DBMS_OUTPUT.put_line ('Voding CN Completed.....' );

END tmp_gm_load_reverse_cons;
/
