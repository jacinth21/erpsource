CREATE OR REPLACE PROCEDURE qb_migrate_invoice
AS
	CURSOR qb_inv_cursor IS
		SELECT   qb_customerreflistid           
				,qa_refnumber                   
				,qb_invoice_txnid               
				,qb_txndate                     
				,qb_customerreffullname         
				,qb_salesrepreflistid           
				,qb_salerepreffullname          
				,qa_shipdate                    
				,qa_ponumber                    
				,qa_subtotal                    
			FROM	 qb_invoice
			WHERE	qb_txndate	<= TO_DATE('10/31/2004','MM/DD/YYYY') ; 
			-- AND		qb_customerreflistid  IN ('440000-1057938754','470000-1057944890','460000-1057944603' ) 
			--WHERE	qb_invoice_txnid  = '1E2F-1065559262'; --'40AD4-1096493200';
		-- '460000-1057944603' -- Memorial  Savannah
		--  '440000-1057938754' -- Ocala
		--  '470000-1057944890' -- St. Joseph's - Savannah
	v_gm_account_id			gm_qb_account_mapping.c704_account_id%TYPE;
	v_gm_sales_rep_id		t704_account.c703_sales_rep_id%TYPE;
	v_c501_ship_to			t901_code_lookup.c901_code_id%TYPE;
	v_c501_ship_to_id		t501_order.c501_ship_to_id%TYPE;
	v_customerreflistid		qb_invoice.qb_customerreflistid%TYPE;
	v_qa_refnumber			qb_invoice.qa_refnumber%TYPE;
	v_qa_count				NUMBER(2);
BEGIN
--
	FOR qb_inv_val IN qb_inv_cursor
	LOOP
		v_customerreflistid := qb_inv_val.qb_customerreflistid;
		v_gm_account_id		:= qb_get_account(v_customerreflistid);
		v_c501_ship_to		:= qb_get_account_shipto(qb_inv_val.qb_customerreflistid);
		v_gm_sales_rep_id 	:= qb_get_salesrepInfo(v_gm_account_id);
		v_qa_refnumber		:= 'QB' || qb_inv_val.qa_refnumber;
		-- If the account is mapped to a sales rep then get the sales rep id 
		--DBMS_OUTPUT.PUT_LINE('v_c501_ship_to ' || v_c501_ship_to) ;
		IF (v_c501_ship_to = 4121)
		THEN
			v_c501_ship_to_id := v_gm_sales_rep_id;
		ELSE
			v_c501_ship_to_id := NULL;
		END IF;
		--DBMS_OUTPUT.PUT_LINE(gm_account_id) ;
		SELECT  COUNT(1) 
		INTO	v_qa_count
		FROM	T501_ORDER
		WHERE	C501_ORDER_ID = v_qa_refnumber;	
		if ( v_qa_count = 1) 
		THEN 
			v_qa_refnumber := v_qa_refnumber || '.' ;
		END IF;
		INSERT INTO T501_ORDER
		  (  C501_ORDER_ID				,C704_ACCOUNT_ID
			,C703_SALES_REP_ID          ,C501_ORDER_DESC        
			,C501_CUSTOMER_PO           ,C501_ORDER_DATE        
			,C501_SHIPPING_DATE         ,C501_TOTAL_COST        
			,C501_DELIVERY_CARRIER      ,C501_DELIVERY_MODE     
			,C501_CREATED_DATE          ,C501_CREATED_BY        
			,C501_TRACKING_NUMBER       ,C501_LAST_UPDATED_DATE 
			,C501_LAST_UPDATED_BY       ,C501_SHIP_TO           
			,C501_STATUS_FL             ,C501_VOID_FL           
			,C501_DELETE_FL             ,C501_RECEIVE_MODE      
			,C501_RECEIVED_FROM         ,C501_SHIP_CHARGE_FL    
			,C501_COMMENTS              ,C501_SHIP_TO_ID        
			,C501_SHIP_COST             ,C501_UPDATE_INV_FL     
			)
		  VALUES
		  (  v_qa_refnumber				,v_gm_account_id
			,v_gm_sales_rep_id			,NULL
			,substr(qb_inv_val.qa_ponumber,1,20)	,qb_inv_val.qb_txndate	-- PO and Trans Date
			,qb_inv_val.qa_shipdate		,qb_inv_val.qa_subtotal
			,5000						, 5005						-- UPS and next day by air
			,SYSDATE					,30300						-- QBMigration User
			,NULL						,NULL
			,NULL						,v_c501_ship_to				-- shipto process
			,4							,NULL						-- Status Flag
			,NULL						,5015						-- Received More
			,NULL						,NULL						-- Ship Charge Flag
			,NULL						,v_c501_ship_to_id			-- Ship to ID To be determined
			,0							,NULL						-- Updated Invoice Flag 
		  );
		  --if ( (gm_account_id IS NULL ) OR (gm_account_id = ''))
		  --then 
		  --  DBMS_OUTPUT.PUT_LINE(gm_account_id) ;
		  --end if ; 
	END LOOP;
	--
EXCEPTION WHEN OTHERS THEN 
--
	 DBMS_OUTPUT.PUT_LINE('Invoice No ' || v_qa_refnumber  || ' Account List '|| v_customerreflistid);
     DBMS_OUTPUT.PUT_LINE(SQLERRM);
--	
END qb_migrate_invoice;
/
