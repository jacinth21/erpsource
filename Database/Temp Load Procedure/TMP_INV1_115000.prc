CREATE OR REPLACE PROCEDURE tmp_inv1_115000
AS
    v_csgid       VARCHAR2 (20);
    v_did         VARCHAR2 (20);
    v_pnum        VARCHAR2 (20);
    v_qty         NUMBER (20);
    v_itemprice   NUMBER (20);
    v_rmaid       VARCHAR2 (20);
    v_dummycsgid VARCHAR2 (20);

    CURSOR csg_cur
    IS
       SELECT t504.c504_consignment_id csgid, t504.c701_distributor_id d_id, t505.c205_part_number_id pnum
             , t505.c505_item_qty qty, t505.c505_item_price item_price
          FROM t504_consignment t504, t505_item_consignment t505
         WHERE t505.c205_part_number_id = '115.000'
           AND t504.c701_distributor_id IS NOT NULL
           AND t504.c504_consignment_id = t505.c504_consignment_id
           AND t504.c504_status_fl = 4
           AND t504.c504_type IN (4110, 4129)
           AND TRIM (t505.c505_control_number) IS NOT NULL
           AND c504_void_fl IS NULL
           AND t504.C701_DISTRIBUTOR_ID NOT IN (  
                SELECT T506.C701_DISTRIBUTOR_ID
                FROM T506_RETURNS T506
                WHERE T506.C506_RMA_ID IN ('GM-RA-9579','GM-RA-9578','GM-RA-10204','GM-RA-10205'))
           ORDER BY D_ID;
BEGIN
    OPEN csg_cur;

    LOOP
        FETCH csg_cur
         INTO v_csgid, v_did, v_pnum, v_qty, v_itemprice;

        EXIT WHEN csg_cur%NOTFOUND;

        --
        SELECT 'GM-RA-' || s506_return.NEXTVAL
          INTO v_rmaid
          FROM DUAL;
         -- Initating Returns
        INSERT INTO t506_returns t506
                    (t506.c506_created_by, t506.c506_created_date, t506.c506_return_date, t506.c506_rma_id
                   , t506.c506_status_fl, t506.c506_type, t506.c701_distributor_id, t506.c506_reason
                   , t506.c506_expected_date, t506.c506_update_inv_fl, t506.C501_REPROCESS_DATE, t506.C506_CREDIT_DATE
           , t506.C506_COMMENTS
                    )
             VALUES ('303097', SYSDATE, SYSDATE, v_rmaid
                   , 2, 3302, v_did, 3313
                   , TRUNC (SYSDATE), 1,SYSDATE, TRUNC(SYSDATE)
           , 'As Requested by Dale for correction'
                    );

        INSERT INTO t507_returns_item t507
                    (t507.c506_rma_id, t507.c507_returns_item_id, t507.c205_part_number_id, t507.c507_item_price
                   , t507.c507_item_qty, t507.c507_status_fl, t507.c901_type, t507.C507_CONTROL_NUMBER
                    )
             VALUES (v_rmaid, s507_return_item.NEXTVAL, v_pnum, v_itemprice
                   , v_qty, 'C', 50300, 'NOC#'
                    );
        -- Updating Inventory
        UPDATE t205_part_number
           SET c205_qty_in_stock = c205_qty_in_stock + v_qty
             , c901_action = 4301
             , c901_type = 4312                                                                                -- Manual
             , c205_last_update_trans_id = v_rmaid
             , c205_last_updated_by = 303097
         WHERE c205_part_number_id = v_pnum;
         
         -- Creating Dummy Consignments
        SELECT 'DM-' || s506_return.NEXTVAL
          INTO v_dummycsgid
          FROM DUAL;
         
         INSERT INTO T504_CONSIGNMENT T504
            (T504.C504_CONSIGNMENT_ID, t504.C504_CREATED_BY , t504.C504_CREATED_date, t504.C504_TYPE, t504.C504_SHIP_TO, c504_ship_req_fl, t504.C701_DISTRIBUTOR_ID, t504.C504_STATUS_FL, T504.C504_SHIP_DATE, T504.C504_COMMENTS,C504_LAST_UPDATED_BY )
            VALUES (v_dummycsgid, 303097, TRUNC(SYSDATE),4129,4120,1, v_did,4, TRUNC(SYSDATE),'As Requested by Dale for correction',303097);
         
         INSERT INTO T505_ITEM_CONSIGNMENT T505
            ( T505.C505_ITEM_CONSIGNMENT_ID , T505.C504_CONSIGNMENT_ID, T505.c205_part_number_id, t505.C505_CONTROL_NUMBER,  C505_ITEM_QTY, C505_ITEM_PRICE)
            VALUES (S504_consign_item.nextval, v_dummycsgid, v_pnum, 'NOC##', v_qty, v_itemprice);            
    --
    END LOOP;

    CLOSE csg_cur;
END tmp_inv1_115000;
/
