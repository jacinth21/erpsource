/* Formatted on 2009/02/18 12:00 (Formatter Plus v4.8.0) */

--@"C:\Database\Temp Load Procedure\Tmp_consignment_shipping.prc";

CREATE OR REPLACE PROCEDURE tmp_consignment_shipping
AS
	CURSOR cur_consg
	IS
		SELECT t504.c504_consignment_id consgid, t504.c504_ship_date shipdate, t504.c504_void_fl vdflg
			 , DECODE (t504.c504_ship_to, 0, 4124, t504.c504_ship_to) shipto, t504.c504_ship_to_id shiptoid
			 , DECODE (t504.c504_delivery_carrier, 0, 5040, t504.c504_delivery_carrier) delcar
			 , DECODE (t504.c504_delivery_mode, 0, 5031, t504.c504_delivery_mode) delmode
			 , DECODE (t504.c504_ship_to, 4121, temp_get_address_id (t504.c504_ship_to_id), NULL) addressid
			 , t504.c504_tracking_number trackno, t504.c504_ship_cost COST
			 , DECODE (CASE
						   WHEN t504.c504_status_fl < 3
							   THEN '15'
						   WHEN t504.c504_status_fl = 3
							   THEN '30'
						   WHEN t504.c504_status_fl > 3
							   THEN '40'
					   END
					 , 15, 15
					 , 30, 30
					 , 40, 40
					  ) stflg
		  FROM t504_consignment t504
		 WHERE t504.c504_void_fl IS NULL AND c504_ship_to IS NOT NULL AND c504_ship_to != 0;
BEGIN
	FOR curindex IN cur_consg
	LOOP
		INSERT INTO t907_shipping_info
					(c907_shipping_id, c907_ref_id, c901_source, c907_shipped_dt, c907_status_fl, c907_void_fl
				   , c901_ship_to, c907_ship_to_id, c106_address_id, c901_delivery_carrier, c901_delivery_mode
				   , c907_tracking_number, c907_frieght_amt, c907_created_by, c907_created_date
					)
			 VALUES (s907_ship_id.NEXTVAL, curindex.consgid, 50181, curindex.shipdate, curindex.stflg, curindex.vdflg
				   , curindex.shipto, curindex.shiptoid, curindex.addressid, curindex.delcar, curindex.delmode
				   , curindex.trackno, curindex.COST, 30301, SYSDATE
					);
	END LOOP;

	COMMIT;
END tmp_consignment_shipping;
