/* Formatted on 2012/05/03 18:22 (Formatter Plus v4.8.0) */
--	@"C:\database\Temp Load Procedure\gm_save_item_order_temp.prc"
/*This procedure is calling by ETL Job, If there is any changes then please coordinate with DWH team during design phase */
CREATE OR REPLACE PROCEDURE gm_save_item_order_temp (
	p_orderid	  IN	   t501_order.c501_order_id%TYPE
  , p_accid 	  IN	   t501_order.c704_account_id%TYPE
  , p_omode 	  IN	   t501_order.c501_receive_mode%TYPE
  , p_pname 	  IN	   t501_order.c501_received_from%TYPE
  , p_comments	  IN	   t501_order.c501_comments%TYPE
  , p_po		  IN	   t501_order.c501_customer_po%TYPE
  , p_total 	  IN	   t501_order.c501_total_cost%TYPE
  , p_userid	  IN	   t501_order.c501_created_by%TYPE
  , p_str		  IN	   VARCHAR2
  , p_ordtype	  IN	   t501_order.c901_reason_type%TYPE
  , p_conscode	  IN	   t741_construct.c741_construct_id%TYPE
  , p_parentid	  IN	   t501_order.c501_parent_order_id%TYPE
  , p_shipto	  IN	   t907_shipping_info.c901_ship_to%TYPE
  , p_shiptoid	  IN	   t907_shipping_info.c907_ship_to_id%TYPE
  , p_shipcarr	  IN	   t907_shipping_info.c901_delivery_carrier%TYPE
  , p_shipmode	  IN	   t907_shipping_info.c901_delivery_mode%TYPE
  , p_addressid   IN	   t907_shipping_info.c106_address_id%TYPE
  , p_shipid	  OUT	   VARCHAR2
  , p_caseid      IN	   t7100_case_information.c7100_case_id%TYPE DEFAULT NULL
  , p_hard_po	  IN	   t501_order.c501_hard_po_fl%TYPE DEFAULT NULL
  , p_attn		  IN	   t907_shipping_info.c907_override_attn_to%TYPE DEFAULT NULL
  , p_ship_instruc IN	   t907_shipping_info.c907_ship_instruction%TYPE DEFAULT NULL
  , p_ref_ordid	IN	 t501_order.c501_order_id%TYPE DEFAULT NULL
  , p_ship_cost		IN		t501_order.c501_ship_cost%TYPE DEFAULT NULL

)
AS
	v_flag		   VARCHAR2 (20);
	v_flag1 	   VARCHAR2 (20);
	v_rep_id	   VARCHAR2 (20);
	v_substring    VARCHAR2 (1000);
	v_cnt		   NUMBER;
	v_id		   NUMBER;
	v_statusfl	   CHAR (1);
	v_action	   VARCHAR2 (20);
	v_shipdt	   DATE;
	v_ordtype	   NUMBER;
	v_shipcost	   NUMBER;
	v_convalue	   NUMBER (15, 2);
	v_allocate_fl  VARCHAR2 (1);
	v_thirdpartyaccid t906_rules.c906_rule_value%TYPE;
	v_out_log	   VARCHAR2 (100);
	v_parentorderid t501_order.c501_parent_order_id%TYPE;
	v_case_order_id t501_order.c501_parent_order_id%TYPE;
	v_parent_cnt   NUMBER;
	v_caseinfoid   t7100_case_information.c7100_case_info_id%TYPE;
	v_caseaccid   t7100_case_information.c704_account_id %TYPE;
	v_acc_type    t704_account.c901_account_type%TYPE;
	v_count		  NUMBER;
	v_ref_id      t501_order.c501_ref_id%TYPE;
	v_ship_cost  t501_order.c501_ship_cost%TYPE;
	v_company_id    t501_order.c1900_company_id%TYPE;
	v_plant_id      t501_order.c5040_plant_id%TYPE;
	v_division		t501_order.c1910_division_id%TYPE;
	v_acc_cmp_id    t704_account.c704_account_id%TYPE;
	v_context_cmp_name  VARCHAR2(100);
	
	CURSOR order_cursor   -- cursor to fetch backorder details for order
	IS
		SELECT c501_order_id
		  FROM t501_order
		 WHERE c501_parent_order_id = p_orderid AND c501_void_fl IS NULL AND (c901_ext_country_id IS NULL
		 OR c901_ext_country_id  in (select country_id from v901_country_codes));
BEGIN
	v_ship_cost := p_ship_cost;
	
--Getting company id from context.   

    SELECT get_compid_frm_cntx()
	  INTO v_company_id 
	  FROM dual;
	--Getting plant id from context.
	SELECT  get_plantid_frm_cntx()
		INTO  v_plant_id
	FROM DUAL;
	
	--Case info validation
	IF p_caseid IS NOT NULL THEN
		BEGIN
			  SELECT c704_account_id, c7100_case_info_id
			    INTO v_caseaccid, v_caseinfoid
			    FROM t7100_case_information 
			   WHERE c7100_case_id = TRIM(p_caseid)
			     AND c901_case_status = 11091 --Active
			    -- AND c901_type = 1006503    -- CASE
			     AND c7100_void_fl IS NULL;
    	 EXCEPTION
    		WHEN NO_DATA_FOUND THEN	
    			raise_application_error('-20797',''); --The entered Case ID is not valid.
		END;
		
		IF v_caseaccid <> p_accid THEN
			raise_application_error('-20798',''); --The Account entered is not associated with this Case.
		END IF;
	END IF;
	
	BEGIN
		SELECT get_rep_id (p_accid)
		  INTO v_rep_id
		  FROM DUAL;
		-- Getting the Company for the selected Account 
		SELECT c1900_company_id , get_company_name(v_company_id)
		  INTO v_acc_cmp_id , v_context_cmp_name
		  FROM T704_account 
		 WHERE c704_account_id = p_accid 
		   AND c704_void_fl IS NULL;
	
		-- Validating the Context COmpany with the Selected Account Company 
		IF v_acc_cmp_id <> v_company_id THEN
			GM_RAISE_APPLICATION_ERROR('-20999','371',v_context_cmp_name);
		END IF;
		  		  
		  -- Get the division
		  v_division := gm_pkg_sm_mapping.get_salesrep_division(v_rep_id);
		  
		-- Restricted new order if account type - ICS Account.
		v_acc_type   := get_account_type (p_accid) ;
		IF v_acc_type = 70114 AND p_ordtype NOT IN (2533,102080)-- 70114 - ICS Account, 2533 - ICS Order, 102080 - OUS Distributor.
    	THEN
    		raise_application_error ('-20588', '') ;
		END IF;
		
		v_ordtype	:= p_ordtype;
				
		-- getting the count from rules table corresponding to the order type to update the shipping table
		SELECT COUNT(1) 
			INTO v_count 
			FROM T906_RULES T906 
			WHERE T906.C906_RULE_ID = 'ORDTYPE'                               
			AND T906.C906_RULE_GRP_ID = 'ORDTYPEGRP'
			AND T906.C906_RULE_VALUE = v_ordtype
			AND T906.C906_VOID_FL IS NULL;

		-- Bill Only Orders
		IF	  v_ordtype = 2526
		   OR v_ordtype = 2530
		   OR v_ordtype = 2532	 --2526, bill only, 2530, bill only Loaner, 2532, bill only from sales consignment
		THEN
          
			--updating status flag as 3, even if customer po is not updated
	       	v_statusfl	:= 3;
	       
			v_action	:= 'BILLONLY';
			v_shipdt	:= CURRENT_DATE;
		ELSE
			v_statusfl	:= 1;
			v_action	:= 'INSERT';
			v_shipdt	:= '';

			IF v_ordtype = 2521
			THEN
				v_ordtype	:= NULL;
			END IF;
		END IF;

		--
		-- To calculate Shipping Cost
		SELECT NVL (GET_ACCOUNT_ATTRB_VALUE (p_accid,'91980'), '0')
		  INTO v_thirdpartyaccid
		  FROM DUAL;

		-- No need to calculate the ship cost as below, as it is getting from the screen
		--IF v_shipcost = -1
		--THEN
			--v_shipcost	:= NVL (TRIM (get_code_name_alt (p_shipmode)), 0);
		--END IF;

		--validation added here, to make sure if order's parent order exist, need get it as new order's parent order
		IF TRIM(p_parentid) IS NOT NULL
		THEN
		--If order is child order , ship cost should  be 0
			v_ship_cost :=0;
		
			SELECT COUNT (*)
			  INTO v_parent_cnt
			  FROM t501_order
			 WHERE c501_order_id = p_parentid
			   AND c501_delete_fl IS NULL
			   AND c501_void_fl IS NULL
			   AND c704_account_id = p_accid;

			IF v_parent_cnt = 0
			THEN
				raise_application_error (-20799, '');
			ELSE
				SELECT NVL (c501_parent_order_id, c501_order_id) orderid
				  INTO v_parentorderid
				  FROM t501_order
				 WHERE c501_order_id = p_parentid
				   AND c501_delete_fl IS NULL
				   AND c501_void_fl IS NULL
				   AND c704_account_id = p_accid;
			--	p_parentorderid := v_parentorderid;
			END IF;
		END IF;
		
		--Case parent DO validation
		IF v_caseinfoid IS NOT NULL 
		THEN
			BEGIN
		     	SELECT c501_order_id
		     	  INTO v_parentorderid
				  FROM t501_order
				 WHERE c7100_case_info_id = v_caseinfoid
				   AND c501_delete_fl IS NULL
				   AND c501_void_fl IS NULL
				   AND c501_parent_order_id IS NULL;
	   		EXCEPTION
	           WHEN NO_DATA_FOUND
	           THEN
	         	 v_parentorderid := NULL;
		    END;
		    --check existing order shipping status
		  	BEGIN		
			  SELECT c501_order_id INTO v_case_order_id
			    FROM t501_order 
			   WHERE c7100_case_info_id = v_caseinfoid
			     AND c501_shipping_date IS NULL
			     AND c501_void_fl IS NULL;
	      	EXCEPTION
	         WHEN NO_DATA_FOUND
	         THEN
	         	v_case_order_id := NULL;
		  	END;
		  	
		    IF v_parentorderid IS NOT NULL AND p_parentid IS NULL THEN
		    	GM_RAISE_APPLICATION_ERROR('-20999','41',v_parentorderid);
	        ELSIF NVL(v_parentorderid,'-9999') <> NVL(p_parentid,'-9999') THEN
	           raise_application_error('-20800',''); --The Parent DO entered is not associated with this Case.
	        ELSIF v_case_order_id IS NOT NULL THEN
	        	GM_RAISE_APPLICATION_ERROR('-20999','42',v_case_order_id);
	        END IF;  
	    END IF;		

		--
		      
		INSERT INTO t501_order
					(c501_order_id, c704_account_id, c501_receive_mode, c501_received_from, c703_sales_rep_id
				   , c501_ship_to, c501_ship_to_id, c501_comments, c501_customer_po, c501_total_cost, c501_order_date
				   , c501_created_by, c501_created_date, c501_status_fl, c501_order_date_time, c901_order_type
				   , c501_delivery_carrier, c501_delivery_mode, c501_ship_cost, c501_parent_order_id, c7100_case_info_id
				   , c501_hard_po_fl ,c501_ref_id, c1900_company_id, c5040_plant_id, c1910_division_id 
					)
			 VALUES (p_orderid, p_accid, p_omode, p_pname, v_rep_id
				   , p_shipto, p_shiptoid, NULL, p_po, p_total, TRUNC (CURRENT_DATE)
				   , p_userid, CURRENT_DATE, v_statusfl, CURRENT_DATE, v_ordtype
				   , p_shipcarr, p_shipmode, v_ship_cost, v_parentorderid, v_caseinfoid
				   , p_hard_po, p_ref_ordid, v_company_id, v_plant_id, v_division
					);
				
		 IF v_ordtype ='101260' --  101260 Acknowledgement Order
		 THEN
		 	v_ordtype := null; 
		 END IF;
		

		--Need to update the shipping table, if order type is null or if it is available in rules table
		IF v_ordtype IS NULL OR v_count = 1		
		THEN
			gm_pkg_cm_shipping_trans.gm_sav_shipping (p_orderid
													, 50180   --orders
													, p_shipto
													, p_shiptoid
													, p_shipcarr
													, p_shipmode
													, NULL
													, p_addressid
													, v_ship_cost
													, p_userid
													, p_shipid
													, p_attn
													, p_ship_instruc
													 );
		END IF;

		--
		-- Saving the Construct Code
		IF p_conscode <> 0
		THEN
			SELECT c741_construct_value
			  INTO v_convalue
			  FROM t741_construct
			 WHERE c741_construct_id = p_conscode;

			--
			INSERT INTO t512_order_construct
						(c512_order_construct_id, c501_order_id, c741_construct_id, c512_construct_value
						)
				 VALUES (s512_ord_construct.NEXTVAL, p_orderid, p_conscode, v_convalue
						);
		END IF;

		--
		IF p_ordtype = 2528
		THEN
			gm_save_item_ord_items_temp (p_orderid, p_str, v_statusfl, p_userid, 'CREDITORDER', p_ordtype, v_parentorderid);
		ELSE
			gm_save_item_ord_items_temp (p_orderid, p_str, v_statusfl, p_userid, v_action, p_ordtype, v_parentorderid);
		END IF;

		--save comments for orders if the account is third party account
		IF (v_thirdpartyaccid != '0')
		THEN
			gm_update_log (p_orderid
						 , 'Third Party Account Number is ' || v_thirdpartyaccid
						 , '1200'
						 , p_userid
						 , v_out_log
						  );

			FOR curindex IN order_cursor
			LOOP
				--save comments for backorders if the account is third party account
				gm_update_log (curindex.c501_order_id
							 , 'Third Party Account Number is ' || v_thirdpartyaccid
							 , '1200'
							 , p_userid
							 , v_out_log
							  );
			END LOOP;
		END IF;

		--
		/* PMT-617[Null entered in as Control Number]
		 * NOC# is not updated to Bill only Loaner items and it reflects when order items are returned.
		 * We are fetching order type here because order type getting changed in gm_save_item_ord_items procedure
		 * hence check the order type with v_ordertype instead of p_ordertype 
		 */
		SELECT NVL (c901_order_type, 2521)
		  INTO v_ordtype
		  FROM t501_order
		 WHERE c501_order_id = p_orderid 
		   AND c501_void_fl IS NULL;

		-- For Bill Only Orders and Credit Order (type - 2529) ---2530, bill only Loaner, 2532, bill only from sales consignment
		IF v_ordtype IS NOT NULL AND v_ordtype = 2526 OR v_ordtype = 2528 OR v_ordtype = 2532 OR v_ordtype = 2530
		THEN			
			UPDATE t501_order
			   SET c501_shipping_date = CURRENT_DATE
				 , c501_ship_cost = 0
				 , c501_last_updated_by = p_userid
				 , c501_last_updated_date = CURRENT_DATE
			 WHERE c501_order_id = p_orderid;

			 UPDATE t502_item_order
			   SET c502_control_number = 'NOC#'
			 WHERE c501_order_id = p_orderid;

			-- status log for bill only Loaner and bill only sales consignment (Initiated)
			gm_pkg_cm_status_log.gm_sav_status_details (p_orderid, '', p_userid, NULL, CURRENT_DATE, 91170, 91102);
		END IF;

		-- For Discount Order Creation
		gm_save_dsorder (p_orderid);

		-- Save the order id to invoice batch (based on the PO)
		gm_pkg_ac_ar_batch_trans.gm_sav_batch_updated_po (p_orderid, NVL(p_po,'-999'), 18751,p_userid);
		--
		--
		UPDATE t703_sales_rep
		   SET c703_sales_fl = 'Y'
		     , c703_last_updated_by = p_userid
			 , c703_last_updated_date = CURRENT_DATE   
		 WHERE c703_sales_rep_id = v_rep_id;

		--END;
		-- only for regular order save the ship details
		-- plus save allocation logic
		
		IF v_ordtype = 2521
		THEN
			-- Code added for allocation logic

			-- Uncommented the code to handle order which has a loaner part + 999 part.
			-- As both are not picked, the order shdn't be allocated.
			v_allocate_fl := gm_pkg_op_inventory.chk_txn_details (p_orderid, 93001);

			SELECT c901_order_type
			  INTO v_ordtype
			  FROM t501_order
			 WHERE c501_order_id = p_orderid
			   AND c501_void_fl IS NULL;

			IF (NVL (v_ordtype, '2521') != '2525' AND NVL (v_allocate_fl, 'N') = 'Y')	-- Back Order
			THEN
				gm_pkg_allocation.gm_ins_invpick_assign_detail (93001, p_orderid, p_userid);
			END IF;
		END IF;
		
	END;
END gm_save_item_order_TEMP;
/
