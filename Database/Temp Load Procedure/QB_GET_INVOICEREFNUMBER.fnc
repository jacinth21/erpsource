CREATE OR REPLACE FUNCTION qb_get_invoicerefnumber
(
 p_qb_invoice_txnid  qb_invoice.qb_invoice_txnid%TYPE
)
RETURN VARCHAR2
IS
--
v_qa_refnumber  qb_invoice.qa_refnumber%TYPE;
--
BEGIN
 --
 SELECT  qa_refnumber
 INTO v_qa_refnumber
 FROM qb_invoice
 WHERE qb_invoice_txnid = p_qb_invoice_txnid;
 --
 RETURN v_qa_refnumber;
EXCEPTION
--
 WHEN NO_DATA_FOUND
 THEN
  RETURN NULL;
--
END qb_get_invoicerefnumber;
/
