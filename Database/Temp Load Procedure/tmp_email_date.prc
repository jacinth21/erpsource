/* Formatted on 11/12/2009 16:20 (Formatter Plus v4.8.0) */

--@"C:\database\Temp Load Procedure\tmp_email_date.prc";

CREATE OR REPLACE PROCEDURE tmp_email_date
IS
	v_date		   DATE;

	CURSOR curr_recs
	IS
		SELECT	 c902_ref_id reqid, MAX (TRUNC (c902_created_date)) dt
			FROM t902_log t902
		   WHERE c902_type = 1247
			 AND c902_comments LIKE '%Missing parts email sent%'
			 AND c902_ref_id IN (
					 SELECT DISTINCT (t526.c525_product_request_id)
								FROM t412_inhouse_transactions t412
								   , t504a_loaner_transaction t504a
								   , t526_product_request_detail t526
								   , t525_product_request t525
							   WHERE t412.c412_status_fl < 4
								 AND t504a.c504a_loaner_transaction_id = t412.c504a_loaner_transaction_id
								 AND t526.c526_product_request_detail_id = t504a.c526_product_request_detail_id
								 AND t525.c525_product_request_id = t526.c525_product_request_id
								 AND t412.c412_type = 50159
								 AND t525.c525_email_count > 0
								 -- and t525.C525_EMAIL_DATE is null
								 AND t412.c412_void_fl IS NULL
								 AND t504a.c504a_void_fl IS NULL
								 AND t526.c526_void_fl IS NULL)
		GROUP BY c902_ref_id
		ORDER BY c902_ref_id;
BEGIN
	FOR currindex IN curr_recs
	LOOP
		SELECT	 MAX (TRUNC (c902_created_date)) dt
			INTO v_date
			FROM t902_log t902
		   WHERE c902_type = 1247 AND c902_comments LIKE '%Missing parts email sent%' AND c902_ref_id = currindex.reqid
		GROUP BY c902_ref_id;

		UPDATE t525_product_request
		   SET c525_email_date = v_date
		   ,C525_LAST_UPDATED_BY = 30301
		   , C525_LAST_UPDATED_DATE =sysdate
		 WHERE c525_product_request_id = currindex.reqid;
	END LOOP;
END tmp_email_date;
/
