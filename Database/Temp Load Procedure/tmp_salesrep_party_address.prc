/* Formatted on 2008/11/17 10:46 (Formatter Plus v4.8.0) */

--@"C:\Database\Temp Load Procedure\tmp_salesrep_party_address.prc"

CREATE OR REPLACE PROCEDURE tmp_salesrep_party_address
AS
	v_fname 	   t101_user.c101_user_f_name%TYPE;
	v_lname 	   t101_user.c101_user_l_name%TYPE;
	v_partyid	   t101_party.c101_party_id%TYPE;

	CURSOR cur_sales_rep
	IS
		SELECT	 c703_sales_rep_id, c703_sales_rep_name, c703_ship_add1, c703_ship_add2, c703_ship_city
			   , DECODE (c703_ship_state, 0, 1055, c703_ship_state) state
			   , DECODE (c703_ship_country, 0, 1109, c703_ship_country) country, c703_ship_zip_code, c703_phone_number
			   , c703_pager_number, c703_email_id, c703_void_fl
			FROM t703_sales_rep
		   WHERE c703_void_fl IS NULL
		ORDER BY c703_sales_rep_id;
BEGIN
	FOR curindex IN cur_sales_rep
	LOOP
		BEGIN
			SELECT t101.c101_user_f_name, t101.c101_user_l_name
			  INTO v_fname, v_lname
			  FROM t101_user t101
			 WHERE t101.c101_user_id = curindex.c703_sales_rep_id;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				v_fname 	:= curindex.c703_sales_rep_name;
				v_lname 	:= '';
		END;

		SELECT s101_party.NEXTVAL
		  INTO v_partyid
		  FROM DUAL;

		INSERT INTO t101_party
					(c101_party_id, c901_party_type, c101_first_nm, c101_last_nm, c101_void_fl, c101_created_by
				   , c101_created_date
					)
			 VALUES (v_partyid, 7005, v_fname, v_lname, curindex.c703_void_fl, 303341
				   , SYSDATE
					);

		UPDATE t703_sales_rep
		   SET c101_party_id = v_partyid
			 , c703_last_updated_by = 303341
			 , c703_last_updated_date = SYSDATE
		 WHERE c703_sales_rep_id = curindex.c703_sales_rep_id;

		IF (curindex.c703_ship_add1 IS NOT NULL)
		THEN
			INSERT INTO t106_address
						(c106_address_id, c101_party_id, c106_add1, c106_add2
					   , c106_city, c901_state, c901_country, c106_zip_code
					   , c106_void_fl, c106_created_by, c106_created_date, c106_primary_fl, c901_address_type
						)
				 VALUES (s106_address.NEXTVAL, v_partyid, curindex.c703_ship_add1, curindex.c703_ship_add2
					   , curindex.c703_ship_city, curindex.state, curindex.country, curindex.c703_ship_zip_code
					   , curindex.c703_void_fl, 303341, SYSDATE, 'Y', 90400
						);
		END IF;

		IF (curindex.c703_phone_number IS NOT NULL)
		THEN
			INSERT INTO t107_contact
						(c107_contact_id, c101_party_id, c901_mode, c107_contact_type, c107_contact_value
					   , c107_primary_fl
						)
				 VALUES (s107_contact.NEXTVAL, v_partyid, 90450, 'business', curindex.c703_phone_number
					   , 'Y'
						);
		END IF;

		IF (curindex.c703_pager_number IS NOT NULL)
		THEN
			INSERT INTO t107_contact
						(c107_contact_id, c101_party_id, c901_mode, c107_contact_type, c107_contact_value
					   , c107_primary_fl
						)
				 VALUES (s107_contact.NEXTVAL, v_partyid, 90451, 'business', curindex.c703_pager_number
					   , 'Y'
						);
		END IF;

		IF (curindex.c703_email_id IS NOT NULL)
		THEN
			INSERT INTO t107_contact
						(c107_contact_id, c101_party_id, c901_mode, c107_contact_type, c107_contact_value
					   , c107_primary_fl
						)
				 VALUES (s107_contact.NEXTVAL, v_partyid, 90452, 'business', curindex.c703_email_id
					   , 'Y'
						);
		END IF;
	END LOOP;
END tmp_salesrep_party_address;
/
