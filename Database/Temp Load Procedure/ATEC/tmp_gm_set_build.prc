/*
 ************************************************
 * The purpose of this procedure is to Initiate a Loaner Set build for the given Set ID
 * Ticket: Atec 403 : Japan Set List Data migration. July 2017 
 * Author: Gomathi Palani. Verified by Richard K.
************************************************* */

CREATE OR REPLACE PROCEDURE tmp_gm_set_build(
        p_set_id IN t208_set_details.C207_SET_ID%TYPE,
        p_tag_id IN t5010_tag.C5010_TAG_ID%TYPE)

AS
  v_user t101_user.c101_user_id%TYPE		:='303043';
  v_initiate_input_str_set_build CLOB;
  v_part_no t205_part_number.c205_part_number_id%TYPE;
  v_tag_flag VARCHAR2 (2) ;
  tag_part_number t205_part_number.c205_part_number_id%TYPE;
  tag_control_number VARCHAR2(40);
  v_input_str_tag_creation CLOB;
  v_company_id t1900_company.C1900_COMPANY_ID%TYPE;
  v_plant_id t5040_plant_master.C5040_PLANT_ID%TYPE;
  v_portal_com_date_fmt VARCHAR2 (100) ;
  v_qty NUMBER;
  v_control_num VARCHAR2(40);
  P_OUT_REQUEST_ID VARCHAR2 (20) ;
  P_OUT_CONSIGN_ID VARCHAR2 (20) ;
  P_ERRMSG VARCHAR2 (200) ;
  v_date VARCHAR2(20);
  v_tagable_flag VARCHAR(2);
  v_input_str_putaway CLOB;
  p_msg varchar2(10);


CURSOR cur_set IS

  SELECT JPN.C205_PART_NUMBER_ID PART_NO, JPN.LOT_NUMBER CONTROL_NUMBER,
  JPN.PHYSICAL_SET_QTY QTY
  FROM JP_LOANER_INV_STG JPN
  WHERE JPN.C207_SET_ID=p_set_id
  AND JPN.C5010_TAG_ID=p_tag_id
  and jpn.set_loaded_fl is null;
  

 
BEGIN

  SELECT get_compdtfmt_frm_cntx () INTO v_portal_com_date_fmt FROM dual;
  --SELECT get_plantid_frm_cntx  INTO v_plant_id FROM dual;
  --SELECT get_compid_frm_cntx() INTO v_company_id FROM dual;
  
 
 FOR  inv_set IN cur_set
  LOOP
   BEGIN
   v_qty :=inv_set.QTY;
   v_part_no :=inv_set.PART_NO;
   v_control_num :=inv_set.CONTROL_NUMBER;   
   -- v_date :=inv_set.req_date;
               
       
     -- Build Input String for Set Build
        v_initiate_input_str_set_build := v_initiate_input_str_set_build||v_part_no||'^'||v_qty||'^'||v_control_num||'^|';  
   
       -- To fetch the tagable part and control number for the set
        v_tag_flag             := get_part_attribute_value (v_part_no, 92340) ;
        IF v_tag_flag           = 'Y' THEN
        v_tagable_flag :='Y'; 
        tag_part_number    := v_part_no;
        tag_control_number := v_control_num;
        END IF;
     END;
  END LOOP;
    
  
---- dbms_output.put_line('v_initiate_input_str_set_build'||v_initiate_input_str_set_build);
gm_pkg_op_process_request.gm_sav_initiate_set(NULL,to_char(CURRENT_DATE, v_portal_com_date_fmt),'50618',NULL,p_set_id,'4127',NULL,'50625',v_user,'4120',NULL,NULL,'15',v_user,NULL,CURRENT_DATE,p_out_request_id,p_out_consign_id);
-- dbms_output.put_line('RQ:'||p_out_request_id);
-- dbms_output.put_line('CN:'||p_out_consign_id);

--	dbms_output.put_line('p_set_id'||p_set_id);

IF (v_tagable_flag ='Y') THEN

       v_input_str_tag_creation:=p_tag_id||'^'||tag_control_number||'^'||tag_part_number||'^'||p_set_id||'^'||'51000^'||p_out_consign_id||'^'||'40033^|';
       gm_pkg_ac_tag_info.gm_sav_tag_detail(v_input_str_tag_creation,v_user,p_errmsg);
     --  dbms_output.put_line('v_input_str_tag_creation'||v_input_str_tag_creation);
     
	  ELSE
	  	dbms_output.put_line('The set ID**'||p_set_id||'and the tag id '||p_tag_id||' does not have a tagable part');
  	END IF;
    
    -- Set build Processing Lot Number verification step
  	GM_SAVE_SET_BUILD(p_out_consign_id, v_initiate_input_str_set_build,'JPN Data Migration tag#'||p_tag_id, '1.20', '0', v_user, p_errmsg,'LOANERPOOL');
    

    -- Moving the Set to Loaner Common Pool
    GM_SAVE_SET_BUILD(p_out_consign_id, v_initiate_input_str_set_build,'JPN Data Migration tag#'||p_tag_id, '1.20', '1', v_user, p_errmsg,'LOANERPOOL');
    
   -- v_input_str_putaway :=v_input_str_putaway||p_out_consign_id||'^'||'080-J-001-J-1'||'^'||p_tag_id||'^|';
     gm_pkg_op_loaner.gm_sav_process_picture(p_out_consign_id,v_user,p_msg);

      gm_pkg_op_set_put_txn.gm_sav_set_put (p_tag_id,'080-J-001-J-1',v_user,p_out_consign_id);
     
    dbms_output.put_line(p_out_consign_id||' : is Moved to Loaner Common Pool');

     
    UPDATE t504_consignment
    SET C504_LAST_UPDATED_BY=v_user, C504_LAST_UPDATED_DATE =CURRENT_DATE, c504_comments='JPN Data Migration tag: '||p_tag_id
    WHERE c504_consignment_id=p_out_consign_id;
    
   UPDATE t504a_consignment_loaner
    SET C504a_LAST_UPDATED_BY=v_user, C504a_LAST_UPDATED_DATE =CURRENT_DATE, c504a_etch_id=p_tag_id
    WHERE c504_consignment_id=p_out_consign_id;
   
   
     /*
    UPDATE t504_consignment
    SET C504_LAST_UPDATED_BY=v_user, C504_LAST_UPDATED_DATE =CURRENT_DATE, C504_VERIFY_FL =1
      , C504_VERIFIED_DATE=CURRENT_DATE, C504_UPDATE_INV_FL=1, C504_STATUS_FL=4
      WHERE c504_consignment_id=p_out_consign_id;
      
    UPDATE t504a_consignment_loaner 
    SET c504a_status_fl='0', c504a_last_updated_date=CURRENT_DATE, c504a_last_updated_by=v_user
    WHERE c504_consignment_id = p_out_consign_id;*/
    
    update JP_LOANER_INV_STG
    set NEW_TXN_ID=p_out_consign_id	
    where c207_set_id=p_set_id
    and c5010_tag_id=p_tag_id;
    
 
END tmp_gm_set_build;     
/

