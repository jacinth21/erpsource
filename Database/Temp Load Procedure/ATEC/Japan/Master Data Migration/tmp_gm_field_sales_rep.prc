/*
************************************************
* The purpose of this procedure is to load the Field Sales Rep master Data
************************************************* 
*/

CREATE OR REPLACE
PROCEDURE tmp_gm_field_sales_rep
AS
P_MESSAGE VARCHAR2(100);
P_DIST_ID VARCHAR2(100);
P_SALESREPID VARCHAR2(100);
v_addressid VARCHAR2(100);
P_PARTY_ID  VARCHAR2(100);
P_LOG_MSG VARCHAR2(1000);
V_PORTAL_COM_DATE_FMT VARCHAR2 (20) ;
REP_START_DATE VARCHAR2 (20) ;
v_dist_err_count number:=0;
v_rep_err_count number:=0;
V_DIST_SCRIPT CLOB;
V_REP_SCRIPT CLOB;
V_ADDRESS_SCRIPT CLOB;
v_user_id t101_user.c101_user_id%TYPE;
v_login_name t102_user_login.C102_LOGIN_USERNAME%TYPE;

 CURSOR CUR_FS IS 
    SELECT * FROM JPN_DM_DISTRIBUTOR 
    WHERE 
    MIGRATED IS NULL;

BEGIN
 -- GM_PKG_COR_CLIENT_CONTEXT.GM_SAV_CLIENT_CONTEXT('1026',GET_CODE_NAME('10306121'),GET_CODE_NAME('105131'),'3017');   --105131 --dd/mm/yyyy
  
   SELECT GET_COMPDTFMT_FRM_CNTX () INTO V_PORTAL_COM_DATE_FMT FROM DUAL;

  FOR  FS IN   CUR_FS
    LOOP
      BEGIN
        V_DIST_SCRIPT:= REPLACE(FS.SCRIPT, 'p_contract_date',  'to_date(''' || FS.CONTRACT_START_DATE || ''', ''' || V_PORTAL_COM_DATE_FMT || ''')');

        DBMS_OUTPUT.PUT_LINE('fs.script ' || V_DIST_SCRIPT );
        EXECUTE IMMEDIATE  'DECLARE p_message VARCHAR2(100);p_dist_id VARCHAR2(100); BEGIN ' || V_DIST_SCRIPT || ' DBMS_OUTPUT.PUT_LINE(''p_dist_id:''||p_dist_id); END;';
     
        -- Get Distributor id
        SELECT C701_DISTRIBUTOR_ID 
          INTO P_DIST_ID
          FROM T701_DISTRIBUTOR 
        WHERE C701_DISTRIBUTOR_NAME_EN =  FS.DISTRIBUTOR_NAME_EN;
        
        -- Insert Log for distributor  1210 distributor in GPLOG code group
        GM_UPDATE_LOG(P_DIST_ID, 'Data migrated from source system',1210,303043,P_LOG_MSG);
      
        --Update source distributor id in ext_ref_id column
        UPDATE T701_DISTRIBUTOR SET C701_EXT_REF_ID = FS.EMPLOYEE_ID WHERE C701_DISTRIBUTOR_ID =  P_DIST_ID;
        
      EXCEPTION WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE('*********Distributor update - Sno:' || FS.sno || ' Error:' || SQLERRM);
        v_dist_err_count:=v_dist_err_count+1;
      END;

      BEGIN
        --Get Sales Rep data which is of same number
        SELECT SCRIPT INTO V_REP_SCRIPT FROM JPN_DM_SALES_REP
        WHERE TRIM(SNO) = TRIM(FS.SNO);

        SELECT START_DATE INTO REP_START_DATE FROM JPN_DM_SALES_REP WHERE SNO=FS.SNO;

        -- 
        V_REP_SCRIPT:=REPLACE(V_REP_SCRIPT,'p_dist_id',P_DIST_ID);
      
        V_REP_SCRIPT:= REPLACE(V_REP_SCRIPT, 'p_start_date',  'to_date(''' || REP_START_DATE || ''', ''' || V_PORTAL_COM_DATE_FMT || ''')');

        DBMS_OUTPUT.PUT_LINE('Rep Script' || V_REP_SCRIPT);

        EXECUTE IMMEDIATE  'DECLARE p_salesrepid VARCHAR2(100); BEGIN ' || V_REP_SCRIPT || ' DBMS_OUTPUT.PUT_LINE(''p_salesrepid:''||p_salesrepid); END;';

       -- Get the Party id and sales rep id of the sales rep by using distributor english name as distributor and rep name are same
        SELECT C703_SALES_REP_ID, C101_PARTY_ID 
          INTO P_SALESREPID, P_PARTY_ID
          FROM T703_SALES_REP 
        WHERE C703_SALES_REP_NAME_EN =  FS.DISTRIBUTOR_NAME_EN;
        
        -- Insert Log for sales rep,  1211 rep in GPLOG code group
        GM_UPDATE_LOG(P_SALESREPID, 'Data migrated from source system',1211,303043,P_LOG_MSG);

        --Update source distributor id in ext_ref_id column
        UPDATE T703_SALES_REP SET C703_EXT_REF_ID = FS.EMPLOYEE_ID WHERE C703_SALES_REP_ID =  P_SALESREPID;
        
        
      EXCEPTION WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE('*********Sales Rep update - Sno:' || FS.sno || ' Error:' || SQLERRM);
        v_rep_err_count:=v_rep_err_count+1;
      END;

      BEGIN
        --Get Address for the dealer
        SELECT SCRIPT INTO V_ADDRESS_SCRIPT FROM JPN_DM_SALES_REP_ADDRESS
        WHERE TRIM(SNO) = TRIM(FS.SNO);
      
        V_ADDRESS_SCRIPT:= REPLACE(V_ADDRESS_SCRIPT, 'p_party_id', P_PARTY_ID);
      
        DBMS_OUTPUT.PUT_LINE('Address Script' || V_ADDRESS_SCRIPT);

        EXECUTE IMMEDIATE  'DECLARE P_LOG_MSG VARCHAR2(1000);p_addressid varchar2(20); BEGIN ' || V_ADDRESS_SCRIPT || 'DBMS_OUTPUT.PUT_LINE(''p_addressid:''||p_addressid);    END;';
        
        -- Get the inserted address id
        SELECT t106.c106_address_id INTO v_addressid FROM t106_address t106, t703_sales_rep t703
        WHERE t703.c101_party_id = t106.c101_party_id
        AND t703.C703_SALES_REP_ID = P_SALESREPID;
        
        -- Insert Log for sales rep,  1211 rep in GPLOG code group
        Gm_Update_Log(P_PARTY_ID, 'Data migrated from source system',1240,303043,P_Log_Msg); 
        
        -- Mark successful saved records as Migrated
        UPDATE JPN_DM_DISTRIBUTOR SET MIGRATED='Y' WHERE SNO=FS.SNO;
        UPDATE JPN_DM_SALES_REP SET MIGRATED='Y' WHERE SNO=FS.SNO;
        UPDATE JPN_DM_SALES_REP_ADDRESS SET MIGRATED='Y' WHERE SNO=FS.SNO;
      -- Added the below insert to add the email id for the user. This is required to create the login credentials for the user
	  
        INSERT INTO t107_contact (c107_contact_id, c101_party_id, c901_mode,c107_contact_type, c107_contact_value, c107_primary_fl)
        VALUES(s107_contact.NEXTVAL, P_PARTY_ID, 90452,'Email', 'djames@globusmedical.com', 'Y') ;
		
		select c101_user_id, lower(concat(substr(c101_user_f_name,1,1), c101_user_l_name)) into v_user_id,v_login_name from t101_user  where c101_party_id=P_PARTY_ID;

		INSERT INTO t102_user_login (C102_USER_LOGIN_ID,C101_USER_ID,C102_LOGIN_USERNAME,C102_USER_PASSWORD,C102_PASSWORD_EXPIRED_FL,C102_NEW_USER_FL,C102_USER_LOCK_FL,C901_AUTH_TYPE,C102_LOGIN_PIN,C102_LOGIN_LOCK_FL,C102_LOGIN_ATTEMPT_CNT,C102_LOGIN_ATTEMPT_TIME,C102_PASSWORD_UPDATED_DATE,C102_LAST_UPDATED_BY,C102_LAST_UPDATED_DATE,C102_EXT_ACCESS)
		VALUES (S102_USER_LOGIN.nextval,v_user_id,v_login_name,'Globus!234',NULL,'Y',NULL,321,NULL,NULL,NULL,NULL,NULL,'2277820',CURRENT_DATE,'N');
		
		Insert Into T703a_Salesrep_Div_Mapping Values(S703a_Salesrep_Div_Mapping.nextval,p_salesrepid,'2006','105442',NULL,303043, CURRENT_DATE,null );
 
      EXCEPTION WHEN OTHERS THEN
       DBMS_OUTPUT.PUT_LINE('*********Sales Rep Address update - Sno:' || FS.sno || ' Error:' || SQLERRM);
      END;
      
      DBMS_OUTPUT.PUT_LINE('final p_dist_id:'||P_DIST_ID);
      DBMS_OUTPUT.PUT_LINE('final p_salesrepid:'||P_SALESREPID);
      DBMS_OUTPUT.PUT_LINE('v_addressid:'||v_addressid);

  END LOOP;
  DBMS_OUTPUT.PUT_LINE('v_dist_err_count:'||v_dist_err_count);
  DBMS_OUTPUT.PUT_LINE('v_rep_err_count:'||v_rep_err_count);

END tmp_gm_field_sales_rep;
/

