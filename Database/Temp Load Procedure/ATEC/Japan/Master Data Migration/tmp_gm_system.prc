CREATE OR REPLACE PROCEDURE
tmp_gm_system
(
p_system_name IN varchar2,
p_company_id  IN t1900_company.c1900_company_id%TYPE,
p_user_id     IN t101_user.c101_user_id%TYPE,
p_comments    IN varchar2,
p_division_id IN t1910_division.C1910_DIVISION_ID%TYPE,
p_helpdesk_id IN NUMBER
)

AS
v_out_sysid t207_set_master.c207_set_id%TYPE;
v_message VARCHAR2(100);
v_user_id t101_user.c101_user_id%TYPE;

BEGIN

	it_pkg_cm_user.gm_cm_sav_helpdesk_log (p_helpdesk_id, NULL);
		
	GM_PKG_PD_SYSTEM_TRANS.GM_SAV_SYSTEM_DETAIL(null,p_system_name,p_system_name,p_system_name,p_comments,p_user_id,p_division_id,p_company_id,V_OUT_SYSID);
    
	gm_pkg_pd_system_trans.gm_sav_set_attribute(v_out_sysid,'103116^4000117|103117^4000118|','103116,103117,103118,103119',p_user_id);
    
	gm_pkg_pd_system_trans.gm_sav_system_company_map(v_out_sysid,p_company_id,105360,p_user_id);
   
    dbms_output.put_line ('New System created for JOM Division. System ID: '||v_out_sysid|| 'System Name:'||p_system_name) ;

END tmp_gm_system;
/