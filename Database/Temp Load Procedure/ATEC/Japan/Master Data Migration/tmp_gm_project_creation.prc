CREATE OR REPLACE PROCEDURE
tmp_gm_project_creation
(
p_project_id IN t202_project.c202_project_id%TYPE,
p_project_nm IN varchar2,
p_company_id IN t1900_company.c1900_company_id%TYPE,
p_comments 	 IN varchar2,
p_division_id   IN t1910_division.C1910_DIVISION_ID%TYPE
)

AS
v_StrPrjIdFromDb t202_project.C202_PROJECT_ID%TYPE;
v_message VARCHAR2(100);
v_user_id t101_user.c101_user_id%TYPE;

BEGIN

	it_pkg_cm_user.gm_cm_sav_helpdesk_log ('16610', NULL) ;
    v_user_id := IT_PKG_CM_USER.GET_VALID_USER;
	
    gm_pkg_pd_project_trans.gm_sav_projectdetails(p_project_nm ,p_project_nm ,p_project_id,'20301','303011','1500','303011','303011',p_division_id,p_comments,p_company_id,p_project_id,v_message,v_StrPrjIdFromDb);
	GM_PKG_PD_PROJECT_TRANS.GM_SAV_PROJECT_ATTRIBUTE(V_STRPRJIDFROMDB,'103115^4001|103116^4000117|103117^4000118|103118^4000490|','103115,103116,103117,103118',V_USER_ID);
	gm_update_log(v_StrPrjIdFromDb,p_comments,v_user_id,'4000397',v_message);
	gm_pkg_pd_project_trans.gm_sav_project_company_map(v_StrPrjIdFromDb,p_company_id,105360,v_user_id);
    gm_pkg_pd_project_trans.gm_chg_projectstatus (p_project_id,'20301','303011');
    dbms_output.put_line ('Project created for JOM Division. Project ID: '||p_project_id|| 'Project Name:'||p_project_nm) ;

END tmp_gm_project_creation;
/