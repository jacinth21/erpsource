
CREATE OR REPLACE PROCEDURE 
tmp_account_price_migration

AS
v_party_id VARCHAR2(100);
v_project_id VARCHAR2(100);
v_approved_by VARCHAR2(100):='30301';
v_err_count NUMBER:=0;
v_success_count NUMBER:=0;
v_err_party_miss_string CLOB;

v_company_id NUMBER;

CURSOR cur_ap IS
  SELECT sno,part,ACCOUNT,account_price,migrated,script, t704.c704_account_id portal_account_id, 
    t704.c101_party_id acc_party_id, t202.c202_project_id project_id  
    FROM jpn_dm_account_price ac, t205_part_number t205, t704_account t704, t202_project t202
    WHERE ac.part = t205.c205_part_number_id 
    AND t205.c202_project_id = t202.c202_project_id
    AND t704.c704_ext_ref_id = ac.ACCOUNT
    --AND to_number(sno)>1020 AND to_number(sno)<1030   --ACCOUNT IN ('029790') and --Sno In ('3890') And
    AND migrated IS NULL
    AND to_number(account_price)>0
  ORDER BY to_number(sno) ;

BEGIN
  -- gm_pkg_cor_client_context.gm_sav_client_context('1026',get_code_name('10306121'),get_code_name('105131'),'3017');   --105131 --dd/mm/yyyy
  
  SELECT get_compid_frm_cntx() INTO v_company_id FROM dual;

  FOR  acc IN  cur_ap
    loop
    
      -- Commit and set context, to make it fast
      IF (mod(to_number(acc.sno),1000)=0) THEN
        COMMIT;
        dbms_output.put_line('commited at sno=' || acc.sno);
      END IF;

      v_party_id := acc.acc_party_id;
      
      dbms_output.put_line('acc.Source_Account_Id=' || acc.ACCOUNT || ', Portal Account Id=' || acc.portal_account_id || ', p_party_id='|| v_party_id);
      
      v_project_id := acc.project_id;
      
      BEGIN  
        UPDATE t705_account_pricing
        SET c705_unit_price = acc.account_price
        ,c705_last_updated_by = v_approved_by
        ,c705_last_updated_date = CURRENT_DATE
        WHERE c101_party_id = v_party_id
        AND c205_part_number_id = acc.part
        AND c705_void_fl IS  NULL;
        
        IF(SQL%rowcount = 0) THEN
       
          INSERT INTO t705_account_pricing
          (c705_account_pricing_id 
          ,c101_party_id
          ,c205_part_number_id
          ,c705_unit_price
           ,c202_project_id
           ,c705_created_date
           ,c705_created_by
           ,c1900_company_id
          )
          VALUES (s704_price_account.nextval, v_party_id, acc.part, acc.account_price
           ,  v_project_id, CURRENT_DATE, v_approved_by
           , v_company_id
          );

          dbms_output.put_line(' Account Pricing inserted for part=' || acc.part || ', Acc Party='  || v_party_id);

        END IF;
        
        -- Mark successful saved records as Migrated
        UPDATE jpn_dm_account_price SET migrated='Y' WHERE sno=acc.sno;
        
        v_success_count:=v_success_count+1;
      exception WHEN no_data_found THEN
        v_err_count:=v_err_count+1;
        dbms_output.put_line('********* Account Pricing insert NO data Found');
      WHEN others THEN
        dbms_output.put_line('********* Account Pricing insert '|| sqlerrm);
        v_err_count:=v_err_count+1;
      END;

  END loop;
  
  dbms_output.put_line('v_err_count:'||v_err_count);
  dbms_output.put_line('v_success_count:'||v_success_count);
  dbms_output.put_line('v_err_party_miss_string:'||v_err_party_miss_string);
  
  
END tmp_account_price_migration;
/
