/*
************************************************
* The purpose of this procedure is to Load the Account Creation Master Data
************************************************** */

CREATE OR REPLACE
PROCEDURE tmp_account_migration
AS

p_message VARCHAR2(100);
p_dist_id VARCHAR2(100);
p_salesrepid VARCHAR2(100);
p_source_salesrepid VARCHAR2(100);
p_addressid VARCHAR2(100);
p_party_id  VARCHAR2(100);
p_log_msg VARCHAR2(1000);

v_acc_script CLOB;
v_address_script CLOB;

p_out_rep_acc_id VARCHAR2(100);
p_out_parent_party_id VARCHAR2(100);

v_dealer_id t101_party.c101_party_id%TYPE;
v_bill_name VARCHAR2(100);
v_bill_add1 VARCHAR2(200);
v_bill_add2 VARCHAR2(200);
v_bill_city VARCHAR2(200);
v_bill_state VARCHAR2(200);
v_bill_country VARCHAR2(200);
v_bill_zip VARCHAR2(200);
v_ship_state VARCHAR2(200);

v_acct_type VARCHAR2(100);
v_payterm_type VARCHAR2(100);
v_err_count number:=0;
v_success_count number:=0;

v_company_id NUMBER;

 CURSOR cur_account IS 
    SELECT * FROM jpn_dm_account 
    WHERE
    migrated IS NULL
    ORDER BY TO_NUMBER(sno) ;

BEGIN
--  gm_pkg_cor_client_context.gm_sav_client_context('1025',get_code_name('10306121'),get_code_name('105131'),'3016');   --105131 --dd/mm/yyyy
  
  SELECT get_compid_frm_cntx() INTO v_company_id FROM dual;

  FOR  acc IN   cur_account
    loop
       
      BEGIN
        -- Get dealer id from Account dealer source id
        select c101_party_id INTO p_party_id  
        from t101_party 
        where c101_ext_ref_id=acc.DEALER_ID
        AND c1900_company_id=v_company_id;
      EXCEPTION 
      WHEN no_data_found THEN
        dbms_output.put_line('*********Get Dealer and Rep - s.no=' || acc.sno || ',Dealer not available for Dealer source id ' || acc.dealer_id  );
      WHEN others THEN
        dbms_output.put_line('*********Get Dealer and Rep - s.no=' || acc.sno || ',Dealer - Err:' || sqlerrm );
      END;
      dbms_output.put_line('acc.dealer_id' || acc.dealer_id || 'p_party_id='|| p_party_id);

      BEGIN
        -- Get sales rep id from dealer id of account
        IF (trim(acc.rep_id) IS NULL) THEN
          SELECT c703_sales_rep_id INTO p_salesrepid 
          FROM t703_sales_rep WHERE c1900_company_id = v_company_id
          AND ROWNUM=1;--Because sales rep id cannot be null, assign first return sales rep for company id
           dbms_output.put_line('p_salesrepid='||p_salesrepid);
        ELSE
          select C703_SALES_REP_ID INTO p_salesrepid 
          from T703_SALES_REP
          where C703_EXT_REF_ID=acc.rep_id
          AND c1900_company_id=v_company_id;

          dbms_output.put_line('p_salesrepid='||p_salesrepid);
        END IF;
     EXCEPTION 
      WHEN no_data_found THEN
        P_SALESREPID:=null;
        dbms_output.put_line('*********Get Rep - s.no=' || acc.sno || ',Dealer not available for Dealer source id ' || acc.rep_id  );
      WHEN others THEN
        dbms_output.put_line('*********Get Rep - s.no=' || acc.sno || ',Dealer - Err:' || sqlerrm );
      END;
      
     -- Bill address data comes from Dealer. If no dealer, direct account, Ship address data and bill address are same
      BEGIN
        SELECT t101.c101_party_id, t101.c101_party_nm, t106.c106_add1,t106.c106_add2,t106.c106_city,t106.c901_state,t106.c901_country,t106.c106_zip_code
          INTO v_dealer_id,v_bill_name,v_bill_add1,v_bill_add2 ,v_bill_city ,v_bill_state ,v_bill_country,v_bill_zip 
        FROM t101_party t101, t106_address t106
        WHERE t101.c101_party_id = t106.c101_party_id 
        AND t101.c101_ext_ref_id = acc.dealer_id
        AND t101.c1900_company_id=v_company_id;
      exception WHEN NO_DATA_FOUND THEN
        v_dealer_id:=NULL;--Temp
        v_bill_name := acc.ship_name;
        v_bill_add1:= acc.ship_add1; 
        v_bill_add2:= acc.ship_add2; 
        v_bill_city := acc.ship_city; 
        v_bill_state := get_code_id(acc.ship_state,'STATE'); 
        v_bill_country:= acc.ship_country; 
        v_bill_zip := acc.ship_zipcode; 
       WHEN OTHERS THEN 
        dbms_output.put_line('********* Account Address s.no=' || acc.sno || ' - Dealer source id ' || acc.dealer_id || ' Err:' || sqlerrm );
      END;
         
      BEGIN  
        -- Check for bill state
        SELECT decode(v_bill_state,NULL,1057,v_bill_state) INTO v_bill_state FROM dual;
        
        v_ship_state:= get_code_id(acc.ship_state,'STATE');
        SELECT decode(v_ship_state,NULL,1057,v_ship_state) INTO v_ship_state FROM dual;

      
        -- Assign Dealer id
        v_acc_script:= REPLACE(acc.script, 'v_dealer_id', v_dealer_id);
        
        -- Assign rep id 
        v_acc_script:= REPLACE(v_acc_script, 'v_rep_id', p_salesrepid);
        
        -- Assign Account type
        v_acc_script:= REPLACE(v_acc_script, 'v_acct_type', acc.account_type);
        
        -- Assign pay term type
        v_acc_script:= REPLACE(v_acc_script, 'v_payterm_type', get_code_id(acc.payment_term,'PAYM'));
        
        -- Assign billing data
        v_acc_script:=REPLACE(v_acc_script,'v_bill_name^v_bill_add1^v_bill_add2^v_bill_city^v_bill_state^v_bill_country^v_bill_zip', v_bill_name || '^' || v_bill_add1 || '^' || v_bill_add2|| '^' || v_bill_city|| '^' || v_bill_state || '^' ||v_bill_country|| '^' ||v_bill_zip);

        -- Assign ship state
        v_acc_script:= REPLACE(v_acc_script, 'v_ship_state', v_ship_state);

        --Assign rep
        v_acc_script:= REPLACE(v_acc_script, 'rep_id', p_salesrepid);
        
        dbms_output.put_line('acc.script ' || v_acc_script );

        EXECUTE IMMEDIATE  'DECLARE p_out_rep_acc_id VARCHAR2(100);p_out_parent_party_id VARCHAR2(100); BEGIN ' || v_acc_script|| ' DBMS_OUTPUT.PUT_LINE(''p_out_rep_acc_id:''||p_out_rep_acc_id); DBMS_OUTPUT.PUT_LINE(''p_out_parent_party_id:''||p_out_parent_party_id); END;';

        -- Get distributor id
        SELECT get_account_id_by_name(trim(acc.Account_English_Name)) INTO p_out_rep_acc_id FROM dual;

        --Update source distributor id in ext_ref_id column
        UPDATE t704_account SET c704_ext_ref_id = acc.account_source_id WHERE c704_account_id =  p_out_rep_acc_id;

        -- Insert Log for sales rep,  1213 contact and address in GPLOG code group
        gm_update_log(p_out_rep_acc_id, 'Data migrated from source system',1209,303043,p_log_msg);
        
        dbms_output.put_line('acc.Account_English_Name=' || trim(acc.Account_English_Name) || ',p_out_rep_acc_id = '|| p_out_rep_acc_id || ', p_log_msg=' || p_log_msg);

        -- Mark successful saved records as Migrated
        UPDATE jpn_dm_account SET migrated='Y' WHERE sno=acc.sno;
        
        v_success_count:=v_success_count+1;
      exception WHEN NO_DATA_FOUND THEN
        dbms_output.put_line('********* Account insert NO data Found');
      WHEN others THEN
        dbms_output.put_line('********* Account insert '|| sqlerrm);
        v_err_count:=v_err_count+1;
      END;

  END loop;
  
  dbms_output.put_line('v_err_count:'||v_err_count);
  dbms_output.put_line('v_success_count:'||v_success_count);


END tmp_account_migration;
/

