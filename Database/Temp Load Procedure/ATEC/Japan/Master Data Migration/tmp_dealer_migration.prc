CREATE OR REPLACE PROCEDURE
TMP_DEALER_MIGRATION
AS
  v_new_party_id     t101_party.c101_party_id%TYPE;
  v_addressid     VARCHAR2(20);
  v_contact_phone_id     VARCHAR2(20);
  v_contact_fax_id     VARCHAR2(20);
  v_contactid_phone      VARCHAR2(20);
  v_contactid_fax    VARCHAR2(20);
  P_LOG_MSG  VARCHAR2(100);
  v_Fax_value  VARCHAR2(100);
  v_err_count number:=0;
  
  v_address_script CLOB;
  v_contact_script_phone CLOB;
  v_contact_script_fax CLOB;
  v_inv_param_script CLOB;

  CURSOR cur_dealer IS 
    SELECT * FROM globus_app.jpn_dm_dealer 
    WHERE --sno  IN ('1') AND 
    migrated IS NULL;

  BEGIN
    -- Set Context for Japan
    -- gm_pkg_cor_client_context.gm_sav_client_context('1025',get_code_name('10306121'),get_code_name('105131'),'3016');   --105131 --dd/mm/yyyy

    FOR  dealer IN   cur_dealer
    loop
      BEGIN
        EXECUTE IMMEDIATE  'DECLARE v_party_id     t101_party.c101_party_id%TYPE; BEGIN ' || dealer.script || '  END;';
        
        -- Get the inserted party id
        SELECT c101_party_id INTO v_new_party_id FROM t101_party
        WHERE C101_PARTY_NM = dealer.PARTY_NAME and NVL(C101_MIDDLE_INITIAL,'-999')=NVL(dealer.MIDDLE_INITIAL,'-999');
       
        -- Update Ext_Ref_Id 
        UPDATE t101_party SET c101_ext_ref_id = dealer.employee_id WHERE c101_party_id =  v_new_party_id;
        
         -- Insert Log for distributor  1210 distributor in GPLOG code group
        Gm_Update_Log(v_new_party_id, 'Data migrated from JOM source system',1210,303043,p_log_msg);
      exception WHEN others THEN
       dbms_output.put_line('*********Dealer Master update - Sno:' || dealer.sno || ' Error:' || sqlerrm);
       v_err_count:=v_err_count+1;
      END;
      
      dbms_output.put_line('Dealer.script ' || dealer.script);

      -- If v_new_party_id is null, move to next
      IF (v_new_party_id IS NULL) THEN
        CONTINUE;
      END IF;

      BEGIN
         --Get Address for the dealer
        SELECT script INTO v_address_script FROM jpn_dm_dealer_address
        WHERE trim(sno) = trim(dealer.sno);
      
        v_address_script:= REPLACE(v_address_script, 'v_new_party_id', v_new_party_id);
      
        dbms_output.put_line('Address Script ' || v_address_script);
        
        EXECUTE IMMEDIATE  'DECLARE v_addressid varchar2(20); BEGIN ' || v_address_script || ' dbms_output.put_line(''v_addressid:''||v_addressid);  END;';
        
        -- Get the inserted address id
        SELECT c106_address_id INTO v_addressid FROM t106_address
        WHERE c101_party_id = v_new_party_id;
        
        -- Insert Log for distributor  1213 address and contact in GPLOG code group
        Gm_Update_Log(v_new_party_id, 'Data migrated from JOM source system',1240,303043,p_log_msg);
        GM_UPDATE_LOG(v_new_party_id, 'Data migrated from JOM source system',1236,303043,P_LOG_MSG);

      exception WHEN others THEN
       dbms_output.put_line('*********Dealer Address update - Sno:' || dealer.sno || 'Error:' || sqlerrm);
      END;

      BEGIN
        --Get Contact for the dealer
        SELECT script INTO v_contact_script_phone FROM jpn_dm_dealer_contact_phone
        WHERE trim(sno) = trim(dealer.sno);
      
        v_contact_script_phone:= REPLACE(v_contact_script_phone, 'v_new_party_id', v_new_party_id);
      
        dbms_output.put_line('Contact_script_phone Script ' || v_contact_script_phone);
        
        EXECUTE IMMEDIATE  'DECLARE v_contactid varchar2(20); BEGIN ' || v_contact_script_phone || ' dbms_output.put_line(''v_contactid_phone:''||v_contactid); END;';
       
        -- Get the inserted contact id
        SELECT c107_contact_id INTO v_contact_phone_id FROM t107_contact
        WHERE c101_party_id = v_new_party_id;
      
        -- Insert Log for distributor  1213 address and contact in GPLOG code group
        Gm_Update_Log(v_new_party_id, 'Data migrated from JOM source system',1231,303043,p_log_msg);
        
        dbms_output.put_line('v_contact_phone_id ' || v_contact_phone_id);
      exception WHEN others THEN
        dbms_output.put_line('*********Dealer contact Phone update - Sno:' || dealer.sno || ' Error:' || sqlerrm);
      END;

      BEGIN
       
         --Get Contact for the dealer
        SELECT script, contact_value INTO v_contact_script_fax,v_Fax_value FROM jpn_dm_dealer_contact_fax
        WHERE trim(sno) = trim(dealer.sno);
        
        IF (trim(v_Fax_value)<>'0') THEN
          v_contact_script_fax:= REPLACE(v_contact_script_fax, 'v_new_party_id', v_new_party_id);
        
          dbms_output.put_line('contact_script_fax Script ' || v_contact_script_fax);
        
        EXECUTE IMMEDIATE  'DECLARE P_LOG_MSG VARCHAR2(1000);v_contactid varchar2(20); BEGIN ' || v_contact_script_fax || ' dbms_output.put_line(''v_contactid_fax:''||v_contactid);   END;';
        END IF;
        
        UPDATE jpn_dm_dealer SET migrated='Y' WHERE sno=dealer.sno;
        UPDATE jpn_dm_dealer_address SET migrated='Y' WHERE sno=dealer.sno;
        UPDATE jpn_dm_dealer_contact_phone SET migrated='Y' WHERE sno=dealer.sno;
        UPDATE jpn_dm_dealer_contact_fax SET migrated='Y' WHERE sno=dealer.sno;
  
        -- Delete if Fax value=0
        --DELETE t107_contact WHERE c107_contact_value='0' AND c107_contact_type=90454 AND c101_party_id = v_new_party_id;
		
		INSERT INTO t107_contact (c107_contact_id, c101_party_id, c901_mode,c107_contact_type, c107_contact_value, c107_primary_fl)
        VALUES(s107_contact.NEXTVAL, v_new_party_id, 26230748,'Business', 'TBE', 'Y') ;
        
      exception WHEN others THEN
        dbms_output.put_line('*********Dealer Contact Fax update - Sno:' || dealer.sno || ' Error:' || sqlerrm);
      END;

      dbms_output.put_line('v_new_party_id:'||v_new_party_id);

      dbms_output.put_line('v_addressid:'||v_addressid);
		GM_UPDATE_LOG(v_new_party_id, 'Data migrated from JOM source system',26230747,303043,P_LOG_MSG);
END loop;
  dbms_output.put_line('v_err_count:'||v_err_count);
END;
/