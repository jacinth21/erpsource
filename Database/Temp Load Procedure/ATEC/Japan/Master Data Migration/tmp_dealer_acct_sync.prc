CREATE OR REPLACE PROCEDURE
tmp_dealer_acct_sync

AS
 
    v_accInputStr 				VARCHAR2(4000);
  	v_inv_cust_type       t101_party_invoice_details.C901_INVOICE_CUST_TYPE%TYPE;
    v_email_fl            t101_party_invoice_details.c101_email_fl%TYPE;
  	v_email_file_fmt			t101_party_invoice_details.C901_EMAIL_FILE_FMT%TYPE;
  	v_recipt_email				t101_party_invoice_details.c101_recipients_email%TYPE;
  	v_csv_template_nm			t101_party_invoice_details.C901_CSV_TEMPLATE_NM%TYPE;
  	v_inv_layout				  t101_party_invoice_details.C901_INVOICE_LAYOUT%TYPE;
  	v_vendor_id					  t101_party_invoice_details.c101_vendor_id%TYPE;
  	v_payment_terms				t101_party_invoice_details.c901_payment_terms%TYPE;
  	v_credit_rate				  t101_party_invoice_details.c101_credit_rating%TYPE;
  	v_collector_id				t101_party_invoice_details.C901_COLLECTOR_ID%TYPE;
  	v_consolidate_billing       t101_party_invoice_details.C101_CONSOLIDATED_BILL_FLAG%TYPE;
    v_inv_close_dt				t101_party_invoice_details.C901_INVOICE_CLOSING_DATE%TYPE;
 
 
 	
        CURSOR c_dealer_info IS
        
        	  SELECT c101_party_id dealerid
				FROM t101_party
			   WHERE c901_party_type = '26230725'
				 AND c101_void_fl     IS NULL;
         
        CURSOR c_dealer_acct_cur (p_dealer_id in varchar2)
	  		IS
          SELECT c704_account_id acct_id
            FROM t704_account
         WHERE c101_dealer_id = p_dealer_id
             AND c704_void_fl    IS NULL;
              
               
        
    BEGIN
        FOR v_dealer_info in c_dealer_info
        Loop
        	   
        	BEGIN
				 SELECT c901_invoice_cust_type, c101_email_fl, c901_email_file_fmt,c101_recipients_email,
				        c901_csv_template_nm, c901_invoice_layout, c101_vendor_id, c901_payment_terms,
		  				c101_credit_rating, c901_collector_id, c901_invoice_closing_date,C101_CONSOLIDATED_BILL_FLAG
		  		   INTO v_inv_cust_type,v_email_fl,v_email_file_fmt,v_recipt_email,
		  		        v_csv_template_nm,v_inv_layout,v_vendor_id,v_payment_terms,
		  		        v_credit_rate,v_collector_id,v_inv_close_dt,v_consolidate_billing
		  	 	   FROM t101_party_invoice_details
		  		  WHERE C101_PARTY_ID = v_dealer_info.dealerid 
		  			AND c101_void_fl is null;
  		
  		 	EXCEPTION WHEN NO_DATA_FOUND
  			THEN
  				v_inv_cust_type := '';
  			END;
        
        	v_accInputStr := '91983^' || v_email_fl || '|' || '91984^' || v_email_file_fmt || '|' || '91985^' || v_recipt_email || '|' || 
  		                 '91986^' || v_csv_template_nm || '|' || '10304532^' || v_inv_layout || '|' || '106642^' || v_vendor_id || '|' ||
  		                 '103050^' || v_collector_id || '|' || '106200^' || v_inv_cust_type || '|' || '106201^' || v_consolidate_billing ||  '|' || '106202^' || v_inv_close_dt || '|' ;

        	FOR v_dealer_acct_id_cur IN c_dealer_acct_cur(v_dealer_info.dealerid)
  			LOOP

            gm_save_account_attribute(v_dealer_acct_id_cur.acct_id,v_accInputStr,'','30301');
  			
  			END LOOP;	
  		                 
        END Loop;
        
   END tmp_dealer_acct_sync;
   /