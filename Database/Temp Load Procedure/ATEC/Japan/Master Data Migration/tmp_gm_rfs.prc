CREATE OR REPLACE PROCEDURE tmp_gm_rfs
(
	p_project_id   IN  t202_project.c202_project_id%TYPE,
	p_country 	   IN  VARCHAR2,
	p_comments	   IN  VARCHAR2,
	p_user_id	   IN  T202_PROJECT.C202_CREATED_BY%TYPE,
	p_part_num     IN  CLOB DEFAULT NULL
)

AS

v_invalid_parts clob;
v_out_comments varchar2(100);
v_out_partinfo TYPES.cursor_type;
v_message CLOB;
v_part_num_input_str clob:=null;

	 cursor c_part
        IS
        select c205_part_number_id pnum
        FROM  t205_part_number t205 
	   WHERE c202_project_id=p_project_id;
BEGIN

	BEGIN
	  	IF (p_part_num IS NOT NULL) THEN
	  	
	  		v_part_num_input_str:=p_part_num||',';
	  		
		END IF;
		
		IF (p_part_num IS NULL) THEN	  			
	  			 
				for V_CUR in C_PART	   
	  			 
	 				LOOP	     
						V_PART_NUM_INPUT_STR:=V_PART_NUM_INPUT_STR||V_CUR.PNUM||',';
	     
					END LOOP;
	    END IF;
	
	END;

			gm_pkg_qa_rfs.gm_qa_sav_rfs 
			(
			p_country||'|'
			,v_part_num_input_str
			,'70171' 
			,p_comments
			,p_user_id
			,'80180'
			,v_message
			,v_invalid_parts
			,V_OUT_COMMENTS
			, v_out_partinfo
			);

END tmp_gm_rfs;
/