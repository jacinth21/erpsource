/*
************************************************
* The purpose of this procedure is to Sync the Dealer and Account Invoice Parameters
************************************************* */
CREATE OR REPLACE
PROCEDURE tmp_dealer_Inv_sync
AS
  v_user t101_user.c101_user_id%TYPE		:='30301';
  v_company_id NUMBER;
  v_err_count NUMBER:=0;
  v_success_count NUMBER:=0; 
  v_script CLOB;

  p_party_id  VARCHAR2(100);
  v_invoice_id t503_invoice.c503_invoice_id%TYPE;

  CURSOR cur_dlr IS
    SELECT * 
    FROM jpn_gm_dealer_invoice_details WHERE --sno in (1,2) and
    migrated IS NULL;

BEGIN
 -- gm_pkg_cor_client_context.gm_sav_client_context('1025',get_code_name('10306121'),get_code_name('105131'),'3016');   --105131 --dd/mm/yyyy
  SELECT get_compid_frm_cntx() INTO v_company_id FROM dual;
  
  FOR  dlr IN  cur_dlr
    loop

      v_script:=dlr.script;

      --Get party_id
       BEGIN
        
        select c101_party_id INTO p_party_id  
        from t101_party 
        where c101_ext_ref_id=dlr.Employee_Id
        AND c1900_company_id=v_company_id;
      EXCEPTION 
      WHEN no_data_found THEN
          dbms_output.put_line('*********Get Dealer - s.no=' || dlr.sno || ',Dealer not available for Dealer source id ' || dlr.Employee_Id  );
          v_err_count:=v_err_count+1;
          continue;
      WHEN others THEN
          dbms_output.put_line('*********Get Dealer - s.no=' || dlr.sno || ',Dealer - Err:' || sqlerrm );
          v_err_count:=v_err_count+1;
          continue;
      END;
      dbms_output.put_line('source dealer_id=' || dlr.Employee_Id || ',p_party_id='|| p_party_id);
          
      v_script:=replace(v_script, 'v_party_id',p_party_id);
      
      BEGIN    
        Execute immediate 'BEGIN ' || v_script || 'END;' ;
        
        --Update Account Payterm
        UPDATE t704_account set C704_PAYMENT_TERMS = dlr.Pay_Term_id 
        WHERE C101_dealer_ID = p_party_id 
        and C1900_COMPANY_ID=V_COMPANY_ID;        

        UPDATE jpn_gm_dealer_invoice_details SET migrated='Y' WHERE sno=dlr.sno;

        v_success_count:=v_success_count+1;
      exception WHEN no_data_found THEN 
        dbms_output.put_line( 'Loop No data found' );
        v_err_count:=v_err_count+1;
      WHEN others THEN
        dbms_output.put_line('**************Err:' || sqlerrm);
        v_err_count:=v_err_count+1;
      END;

    END loop;

  dbms_output.put_line('v_err_count:'||v_err_count);
  dbms_output.put_line('v_success_count:'||v_success_count);


END tmp_dealer_Inv_sync;
/