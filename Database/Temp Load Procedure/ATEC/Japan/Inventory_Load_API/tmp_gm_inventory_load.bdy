create or replace
PACKAGE BODY tmp_gm_inventory_load

IS
  
 /*
* Purpose: Main procedure to be invoked for the Loaner Set building data load migration
* Steps:
*  1. Set Config Qty validation
*  2. IABL initiation
*  3. Set Initiation and Set building
*  4. Loaner Initiate
*  Atec 403 - Set List Data Load Migration. July 2017
*  Author: gpalani. Reviewed by Richard K
*/

PROCEDURE tmp_gm_main_jpn_set_field
AS
   -- p_flag t5054_inv_location_log.C5054_LOT_STATUS_FL%TYPE := 'Y';
 
      v_commit_count NUMBER :='0';
	  v_set_validation_flag varchar2(10);
	  p_out_consign varchar2(100);
	  v_err_dtls clob;

    CURSOR cur_set
    IS

        SELECT DISTINCT (C207_SET_ID) sets_id, C5010_tag_id tag_id,    ALLADIN_ACCOUNT_ID ACCT_ID
        FROM globus_app.JP_LOANER_INV_STG JPN
        WHERE 
        JPN.set_loaded_fl IS NULL
        and jpn.c207_set_id IS NOT NULL
        and jpn.alladin_account_id is not null
        order by C207_SET_ID, C5010_tag_id;
      
       
 BEGIN   
    FOR set_list IN cur_set
	
    LOOP
	    BEGIN 
			tmp_gm_set_validation(set_list.sets_id, set_list.tag_id,set_list.ACCT_ID,v_set_validation_flag);
		
		IF (v_set_validation_flag='PASS') THEN
				
	            tmp_gm_iabl (set_list.sets_id, set_list.tag_id);
	            tmp_gm_set_build (set_list.sets_id, set_list.tag_id, p_out_consign);	
	            tmp_gm_Loaner_Initiate (set_list.sets_id, set_list.ACCT_ID, set_list.tag_id,p_out_consign);
	                      
                
                    UPDATE JP_LOANER_INV_STG
                    SET set_loaded_fl    = 'LOANER INITIATED'
                    WHERE c207_SET_ID  = set_list.sets_id
                    AND c5010_tag_id = set_list.tag_id;           
        END IF;
    
   			 EXCEPTION
    
   				 WHEN OTHERS THEN

   				   dbms_output.put_line ('Set Build Processing failed for the set:'||set_list.sets_id||'And Tag ID:'||
	    		    set_list.tag_id||SQLERRM);
	    		    Rollback;  
        
	            UPDATE JP_LOANER_INV_STG
	            SET set_loaded_fl    = 'Set Build Processing failed'
	              WHERE c207_SET_ID  = set_list.sets_id
	                AND c5010_tag_id = set_list.tag_id;
	             
	                          
		          v_err_dtls :='Set Build Processing failed: Error message: '||SQLERRM;
	              INSERT INTO jp_loaner_status_log(C207_SET_ID,C5010_TAG_ID,ERR_DTLS) VALUES(set_list.sets_id,set_list.tag_id,v_err_dtls);
	           	  commit;
     
    END;
    /* 
     v_commit_count    := v_commit_count + 1;
        IF (v_commit_count = 100) THEN
        
          END IF;
          v_commit_count:='0';
           
    */
             
   COMMIT;   
		
 END LOOP;
END tmp_gm_main_jpn_set_field;

  
  
  
  /*
* Purpose: Main procedure to be invoked for the Loaner Set which are in warehouse.
* Purpose: Main procedure to be invoked for the Loaner Set which are in warehouse.
* Steps:
*  1. Set Config Qty validation
*  2. IABL initiation
*  3. Set Initiation and Set building
*  4. Loaner Initiate
*  Atec 403 - Set List Data Load Migration. July 2017
*  Author: gpalani. Reviewed by Richard K
*/

PROCEDURE tmp_gm_main_jpn_set_build

AS

	  v_set_validation_flag VARCHAR2(10);
      v_commit_count NUMBER :='0';
      p_out_consign varchar2(100);
      v_err_dtls clob;
   
    CURSOR cur_set
    IS

        SELECT DISTINCT (C207_SET_ID) sets_id, C5010_tag_id tag_id
        FROM globus_app.JP_LOANER_INV_STG JPN
        WHERE
        JPN.SET_LOADED_FL is null
        AND ALLADIN_ACCOUNT_ID IS NULL
        order by C207_SET_ID, C5010_tag_id;
             
       BEGIN 
      
        
    FOR set_list IN cur_set
    
    LOOP
       Begin
	       
	    	tmp_gm_set_validation(set_list.sets_id, set_list.tag_id,null,v_set_validation_flag);

	    IF (v_set_validation_flag='PASS') THEN
	       
            tmp_gm_iabl (set_list.sets_id, set_list.tag_id);
            tmp_gm_set_build (set_list.sets_id, set_list.tag_id, p_out_consign);
            dbms_output.put_line ('Set Build Processing competed for the Tag:'||set_list.tag_id);
                
            UPDATE JP_LOANER_INV_STG
            SET set_loaded_fl    = 'Set Building Process completed.'
            WHERE c207_SET_ID  = set_list.sets_id
            AND c5010_tag_id = set_list.tag_id;
       	 END IF;         
                
	    EXCEPTION
	    
	    WHEN OTHERS THEN
	    
	        dbms_output.put_line ('Set Build Processing failed for the set:'||set_list.sets_id||'And Tag ID:'||
	        set_list.tag_id||SQLERRM);
	         ROLLBACK;
	         
	         UPDATE JP_LOANER_INV_STG
              SET set_loaded_fl    = 'Set Build Processing failed'
              WHERE c207_SET_ID  = set_list.sets_id
              AND c5010_tag_id = set_list.tag_id;
             
              
	          v_err_dtls :='Set Build Processing failed: Error message: '||SQLERRM;
              INSERT INTO jp_loaner_status_log(C207_SET_ID,C5010_TAG_ID,ERR_DTLS) VALUES(set_list.sets_id,set_list.tag_id,v_err_dtls);
           	  commit;
	    END;
	     
       COMMIT;
        
  END LOOP;
END tmp_gm_main_jpn_set_build;
  
 

/************************************************
*-- The purpose of this procedure is to validate the following pre-requiste to avoid Set building failure
*-- Account Availablity check
*-- Set Type check
*-- Tag Status check      
*-- Tag Attribute check 
*-- Part number check
*-- Author: Gomathi Palani. Verified by Rajesh V.
*************************************************/

	PROCEDURE tmp_gm_set_validation
  	   (
        p_set_id  IN t208_set_details.C207_SET_ID%TYPE,
        p_tag_id  IN t5010_tag.C5010_TAG_ID%TYPE,
        p_acct_id IN t704_account.c704_account_id%TYPE,
        p_set_validation_flag OUT VARCHAR
		)
    AS
        v_part_num T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE;
        v_source_QTY NUMBER;
        v_set_id T207_SET_MASTER.C207_SET_ID%TYPE;
        V_SET_QTY NUMBER;
        V_SET_PART  T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE;
        v_set_part_num T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE;
        v_temp_acc t704_account.c704_account_id%TYPE;
        v_err_dtls  CLOB; -- varchar2(10000);
        v_set_type T207_SET_MASTER.C207_TYPE%TYPE;
        v_flag VARCHAR2(100);
        v_tag_part_inp_str clob :=null;
        v_tag_count number:=0;
        v_company_id t1900_company.c1900_company_id%TYPE;
        v_plant_id T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
		v_count_num number:=0;
		v_tag_status VARCHAR2(100);
        v_part_number_count NUMBER:=0;
        v_set_count number:=0;
        v_tag_check number:=0;
        v_taggable_cnt NUMBER;
        
        CURSOR cur_source_set
        IS
 SELECT C205_PART_NUMBER_ID PART_NO
   FROM JP_LOANER_INV_STG
  WHERE C207_SET_ID=p_set_id
AND C5010_TAG_ID=p_tag_id
GROUP BY C205_PART_NUMBER_ID
MINUS
SELECT t2023.C205_PART_NUMBER_ID PART_NO
FROM T2023_PART_COMPANY_MAPPING t2023
where t2023.C1900_COMPANY_ID = v_company_id
AND t2023.C2023_VOID_FL IS NULL;	   

-- part price check
CURSOR part_price_dtls_cur
IS
select C205_PART_NUMBER_ID pnum from JP_LOANER_INV_STG where C207_SET_ID=p_set_id
AND C5010_TAG_ID=p_tag_id 
GROUP BY C205_PART_NUMBER_ID
minus
select c205_part_number_id pnum from T2052_PART_PRICE_MAPPING where C1900_COMPANY_ID = v_company_id and C2052_VOID_FL IS NULL;

           
    BEGIN
	    
        p_set_validation_flag :='PASS';
        
        
  		  SELECT get_plantid_frm_cntx  INTO v_plant_id FROM dual;
  		  SELECT get_compid_frm_cntx() INTO v_company_id FROM dual;
                 
      --- Account Availablity check 
           
        IF (p_acct_id is not null) THEN

         BEGIN
	    
	          SELECT count(1) INTO v_count_num
	          FROM t704_account 
	          WHERE c704_ext_ref_id=p_acct_id
	          and c704_void_fl is null
	          and c1900_company_id=v_company_id;
	          
	       IF (v_count_num='0') THEN
	          	v_err_dtls :='SpineIT Account ID was not created for the Alladin Acct ID: '||p_acct_id;
	          
	         	 INSERT INTO jp_loaner_status_log(C207_SET_ID,C5010_TAG_ID,ALLADIN_ACCT_ID,ERR_DTLS)
	         	 VALUES(p_set_id,p_tag_id,p_acct_id,v_err_dtls);
	          	 p_set_validation_flag :='FAIL';
	          
	       END IF;
	      
	     END;
	 END IF;
     
	   -- Set ID check   

	 
	     BEGIN
	         
		      SELECT count(1) into v_set_count
	   		  FROM T207_SET_MASTER
	   		  WHERE C207_SET_ID=p_set_id
	   		  and c207_void_FL IS NULL;
	          
	   		
	          IF (v_set_count=0) THEN
	          
		          v_err_dtls :='Set ID Not Found in the Set Master Table : '||p_set_id;
		          
		          INSERT INTO jp_loaner_status_log(C207_SET_ID,C5010_TAG_ID,ERR_DTLS) VALUES(p_set_id,p_tag_id,v_err_dtls);
		          p_set_validation_flag :='FAIL';
		          
	          
	           -- Set Type Check
   
	         ELSE
	         
	   		  SELECT NVL(C207_TYPE,'-999') INTO v_set_type FROM
	   		  T207_SET_MASTER
	   		  WHERE C207_SET_ID=p_set_id
	   		  and c207_void_FL IS NULL;
	          
	   		  -- 4074 : Loaner 
	          IF (v_set_type NOT IN ('4074')) THEN
	          
		          v_err_dtls :='Set Type is not a Loaner . Please change the set type to 4074 (Loaner Set) for the Set ID : '||p_set_id;
		          
		          INSERT INTO jp_loaner_status_log(C207_SET_ID,C5010_TAG_ID,ERR_DTLS) VALUES(p_set_id,p_tag_id,v_err_dtls);
		          p_set_validation_flag :='FAIL';
		          
	          END IF;
	        
	         END IF;
	        
	      END;    
	          
	  --- Tag  check 
	  	      
	      BEGIN	      
	   		  SELECT count(1) INTO v_tag_check
	   		  FROM T5010_TAG 
	   		  WHERE C5010_TAG_ID=p_tag_id
	   		  and c5010_void_FL is null;
	          
	          IF (v_tag_check='0') THEN
	          
	          v_err_dtls :='Tag  not available in tag table for the tag id : '||p_tag_id;
	          
              INSERT INTO jp_loaner_status_log(C207_SET_ID,C5010_TAG_ID,ERR_DTLS) VALUES(p_set_id,p_tag_id,v_err_dtls);
	          p_set_validation_flag :='FAIL';
	           
	            
      		 ELSE
	      
	 			--- Tag Status check 
         	      
	   		  SELECT NVL(c901_status,'-999') INTO v_tag_status
	   		  FROM T5010_TAG 
	   		  WHERE C5010_TAG_ID=p_tag_id
	   		  and c5010_void_FL is null;
	          
	          IF (v_tag_status NOT IN ('51011','51012')) THEN
	          
	          v_err_dtls :='Tag Status is not in released or in Active status for the Tag id : '||p_tag_id;
	          
              INSERT INTO jp_loaner_status_log(C207_SET_ID,C5010_TAG_ID,ERR_DTLS) VALUES(p_set_id,p_tag_id,v_err_dtls);
	          p_set_validation_flag :='FAIL';
	           
	           END IF;
	      END IF;
	   END; 
          
                  
	      
         BEGIN   	 
           
           FOR cur_set IN cur_source_set
             LOOP
              
                  V_PART_NUM      := cur_set.PART_NO;
                  
                  
                  v_err_dtls :='Part number: '||V_PART_NUM||' not found in Part company mapping table. Set ID: '||p_set_id||' TagID '||p_tag_id;
                  INSERT INTO jp_loaner_status_log(C207_SET_ID,C5010_TAG_ID,ERR_DTLS) VALUES(p_set_id,p_tag_id,v_err_dtls);
                  p_set_validation_flag :='FAIL';
                  
                 
             END LOOP;
           END;
         
         -- part price check
	    FOR part_price_dtls IN part_price_dtls_cur
	    LOOP
	    	
	    --
	    v_err_dtls :='Part # price not available : '||part_price_dtls.pnum ||' Set ID: '||p_set_id||' TagID '||p_tag_id;
	    
	      INSERT INTO jp_loaner_status_log(C207_SET_ID,C5010_TAG_ID,ERR_DTLS) VALUES(p_set_id,p_tag_id,v_err_dtls);
          p_set_validation_flag :='FAIL';
	    
	    END LOOP; 
	    
	     -- taggable part check
	     SELECT count(1) INTO v_tag_count from  JP_LOANER_INV_STG
	     WHERE c207_SET_ID  = p_set_id
				AND   c5010_tag_id = p_tag_id
				AND TAGGABLE_PART = 'Y';
		
		 IF v_tag_count = 0
         THEN
         	 v_err_dtls :='No tagabble part associated with the Set for tag : '||p_tag_id||' , the Setid:  '||p_set_id;
              INSERT INTO jp_loaner_status_log(C207_SET_ID,C5010_TAG_ID,ERR_DTLS) VALUES(p_set_id,p_tag_id,v_err_dtls);
              p_set_validation_flag :='FAIL';
         END IF;
         
         IF (v_tag_count > 1) THEN
         
         	  v_err_dtls :='More than 1 tagabble part associated with the Set in the Tag Attribute for tag : '||p_tag_id||' , the Setid:  '||p_set_id||' Taggable parts are '||v_tag_part_inp_str;
              INSERT INTO jp_loaner_status_log(C207_SET_ID,C5010_TAG_ID,ERR_DTLS) VALUES(p_set_id,p_tag_id,v_err_dtls);
              p_set_validation_flag :='FAIL';
	           
         END IF;
         
         
         IF (p_set_validation_flag ='FAIL') THEN 
         
            	UPDATE JP_LOANER_INV_STG
				SET set_loaded_fl    = 'Record Skipped. Check jp_loaner_status_log table for details'
				WHERE c207_SET_ID  = p_set_id
				AND   c5010_tag_id = p_tag_id; 
	      
		 END IF;
	DBMS_OUTPUT.PUT_LINE (' Set ID  '|| p_set_id || ' TAG ID '|| p_set_id ||' Valid Status '||p_set_validation_flag);
		 
    END tmp_gm_set_validation;
 
  
 
/*
 * Purpose: Procedure to invoke the IABL for the Loaner set list data load migraion
 * Atec 403 Data load Migration - API Japan
 * July 2017. Author gpalani, Reviewed by Richard K
 */
PROCEDURE tmp_gm_iabl
		(
        p_set_id IN t208_set_details.C207_SET_ID%TYPE,
        p_tag_id IN t5010_tag.C5010_TAG_ID%TYPE
		)

AS
  v_user t101_user.c101_user_id%TYPE;
  v_initiate_input_str CLOB;
  v_control_input_str CLOB;
  v_verify_input_str CLOB;
  v_part_no t205_part_number.c205_part_number_id%TYPE;
  v_txn_id t504_consignment.c504_consignment_id%TYPE;
  v_cost t820_costing.c820_purchase_amt%TYPE;
  v_purchase_amt t820_costing.c820_purchase_amt%TYPE;
  v_company_cost  t820_costing.C820_LOCAL_COMPANY_COST%TYPE;
  v_message varchar2(20);
  v_reason NUMBER;
  v_txntype NUMBER;
  v_wh_type VARCHAR2(100);
  v_cur_wh  VARCHAR2(50); 
  v_txn_type_name varchar2(50);
  v_source_wh NUMBER;
  v_target_wh NUMBER;
  v_out VARCHAR2(100);
  v_costing_id NUMBER;
  v_company_id NUMBER;
  v_plant_id NUMBER;
  v_portal_com_date_fmt VARCHAR2 (20) ;
  v_bl_location_id t5052_location_master.C5052_LOCATION_ID%TYPE;
  V_TRIP_PRICE NUMBER;
  v_qty NUMBER;
  v_control_num VARCHAR2(40);
  v_costing_type NUMBER;
  v_count number :=0;
  v_part_company number;
 
CURSOR cur_set IS

	  SELECT JPN.c205_part_number_id PART_NO, JPN.LOT_NUMBER CONTROL_NUMBER,
	  SUM(JPN.physical_set_qty) QTY
	  FROM JP_LOANER_INV_STG JPN
	  WHERE JPN.c207_SET_ID=p_set_id
	  AND JPN.c5010_tag_id=p_tag_id
	  AND JPN.set_loaded_fl IS NULL
	  AND JPN.physical_set_qty > 0
	  GROUP BY JPN.c205_part_number_id , JPN.LOT_NUMBER 
	  ;
 
BEGIN

-- 105131 -- dd/mm/yyyy
  SELECT get_compdtfmt_frm_cntx () INTO v_portal_com_date_fmt FROM dual;
  SELECT get_plantid_frm_cntx  INTO v_plant_id FROM dual;
  SELECT get_compid_frm_cntx() INTO v_company_id FROM dual;
  -- Below line of code will get the user who is executing the script. This will avoid the need to pass the user id as a parameter
  v_user := IT_PKG_CM_USER.GET_VALID_USER;
  

  -- Create Location for BL
  
  BEGIN
	  
    SELECT c5052_location_id INTO v_bl_location_id
    FROM t5052_location_master WHERE c5052_location_cd='JPN_BL_LOCATION'and c1900_company_id= v_company_id and c5052_void_fl is null;
    
    EXCEPTION WHEN OTHERS THEN
    dbms_output.put_line('*********Location error' || ' Error:' || sqlerrm);
    
  END;
  
FOR  inv_set IN cur_set

  LOOP
  
   BEGIN
	   
      v_part_no :=inv_set.part_no;
      v_qty := inv_set.qty;
      v_control_num :=inv_set.control_number;
      v_txn_type_name:='INVAD-BULK';
      v_txntype:= '400080';
      v_target_wh:='90814';
      v_source_wh:='90814';
      v_cur_WH := 'BL';
      v_wh_type:='90814';
      v_costing_type  :='4900';
      v_reason :='90814';
    
 BEGIN
	    
     select COST, company_cost INTO v_purchase_amt, v_company_cost 
     from jpn_dm_inventory 
     where part=v_part_no 
     and warehouse='FG';     
       

     Exception
    
       WHEN NO_DATA_FOUND THEN
       v_purchase_amt:='0';
       v_company_cost :='0';
 --   dbms_output.put_line('v_purchase_amt=' || v_purchase_amt);

 END;
   	   
	   SELECT NVL(TRIM(GET_PART_PRICE(v_part_no,'E')),0) INTO V_TRIP_PRICE
	    FROM DUAL;
		 
   -- Get Inventory cost or insert if not available
   -- **Review Check in Costing table and not from the function ** --
    --  SELECT get_inventory_cogs_value(v_part_no)  INTO v_cost FROM dual;
    
      SELECT count(1) INTO v_cost FROM t820_costing
      WHERE c205_part_number_id=v_part_no
      AND c820_delete_fl = 'N'
      --and c901_status='4801'
      AND c1900_company_id =  v_company_id
      and c901_costing_type=4900;
      
     IF v_cost = 0 THEN
    -- to get the owner company id 
	SELECT C1900_COMPANY_ID INTO v_part_company FROM  t2023_part_company_mapping t2023
      WHERE t2023.c205_part_number_id=v_part_no
      and c901_part_mapping_type='105360';
      --
       INSERT INTO t820_costing
	   ( c820_costing_id,c205_part_number_id,c901_costing_type,c820_part_received_dt,c820_qty_received,c820_purchase_amt,c820_qty_on_hand 
    	,c901_status,c820_delete_fl,c820_created_by,c820_created_date,c1900_company_id,c5040_plant_id,C820_LOCAL_COMPANY_COST
		,C1900_OWNER_COMPANY_ID,c901_owner_currency,C901_LOCAL_COMPANY_CURRENCY       
		) 
	   VALUES ( s820_costing.nextval,v_part_no,v_costing_type,CURRENT_DATE,0,v_purchase_amt,0,'4802','N','2277820',CURRENT_DATE,v_company_id        
			    ,v_plant_id,v_company_cost,v_part_company,'1','4000917');
          
        -- Build input string 
        END IF;
        
        v_initiate_input_str := v_initiate_input_str || v_part_no || ',' || v_qty || ',,,' || V_TRIP_PRICE || '|';         
        v_control_input_str := v_control_input_str || v_part_no || ',' || v_qty || ',' || v_control_num || ',,' || V_TRIP_PRICE || '|';
        v_verify_input_str := NULL;
       
       
      IF v_txn_id IS NULL THEN
      
         SELECT get_next_consign_id(v_txn_type_name) 
         INTO v_txn_id
         FROM dual;
         
      END IF;

   -- Commented the below code since there is no need to update the bulk location qty during IABL
   -- 	gm_pkg_op_inv_field_sales_tran.gm_sav_fs_inv_loc_part_details (v_bl_location_id,v_txn_id, v_part_no,v_qty,'400080','4301',null,current_date, v_user);    
     END;
     
	v_count :=v_count+1;

		if(v_count>20) then 
			
				
		  -- IABL initiate 
		          gm_save_inhouse_item_consign(v_txn_id,v_txntype , v_reason ,0,'','IABL created for the Set ID:'||p_set_id||':Tag ID:'||p_tag_id,v_user,v_initiate_input_str,v_out);
		        dbms_output.put_line('IABL Txn=' || v_txn_id || ' Initiated inner ');
		  -- IABL Control 
		          gm_ls_upd_inloanercsg(v_txn_id,v_txntype,'0','0',NULL,'Japan Set Data Load:'||p_tag_id,v_user,v_control_input_str,null,'ReleaseControl',v_message);
		            dbms_output.put_line('IABL Txn=' || v_txn_id || ' Controlled inner');
		  -- IABL Verify
		  
		          gm_ls_upd_inloanercsg(v_txn_id,v_txntype,'0','0',NULL,'Japan Set Data load:'||p_tag_id,v_user,v_verify_input_str,null,'Verify',v_message);
		          dbms_output.put_line('IABL Txn=' || v_txn_id || ' Verified');
        
		   	     
		          UPDATE t412_inhouse_transactions SET c412_comments='IABL Inventory Update for JPN Set Building:'||p_tag_id , c412_last_updated_date=CURRENT_DATE, c412_last_updated_by=v_user
			 	  where c412_inhouse_trans_id=v_txn_id;
		        
		        
			      v_txn_id := NULL;
			      v_count :=0;
			      v_initiate_input_str :=null;
			      v_control_input_str :=null;
		END IF;

  END LOOP;
  

          -- Outer Loop
          
   IF (v_initiate_input_str is not null) THEN

	  
		   -- IABL initiate 
		          gm_save_inhouse_item_consign(v_txn_id,v_txntype , v_reason ,0,'','IABL created for the Set ID:'||p_set_id||':Tag ID:'||p_tag_id,v_user,v_initiate_input_str,v_out);
                   dbms_output.put_line('IABL Txn=' || v_txn_id || ' Initiated Outer ');
		      
		   -- IABL Control 
		          GM_LS_UPD_INLOANERCSG(v_txn_id,v_txntype,'0','0',NULL,'Japan Set Data Load:'||p_tag_id,v_user,v_control_input_str,null,'ReleaseControl',v_message);
		          dbms_output.put_line('IABL Txn=' || v_txn_id || ' Controlled outer');
		   -- IABL Verify
		  
		    	  gm_ls_upd_inloanercsg(v_txn_id,v_txntype,'0','0',NULL,'Japan Set Data load:'||p_tag_id,v_user,v_verify_input_str,null,'Verify',v_message);
		          dbms_output.put_line('IABL Txn=' || v_txn_id || ' Verified');
		        
	        
		          UPDATE t412_inhouse_transactions SET c412_comments='IABL Inventory Update for JPN Set Building:'||p_tag_id , c412_last_updated_date=CURRENT_DATE, c412_last_updated_by=v_user
				  where c412_inhouse_trans_id=v_txn_id;
				  v_txn_id := NULL;
			      v_count :=0;
			      v_initiate_input_str :=null;
				  v_control_input_str :=null;
			    
  END IF;

  END tmp_gm_iabl;    

  

 
 /*
 ************************************************
 * The purpose of this procedure is to Initiate a Loaner Set build for the given Set ID
 * Ticket: Atec 403 : Japan Set List Data migration. July 2017 
 * Author: Gomathi Palani. Verified by Richard K.
************************************************* */

	PROCEDURE tmp_gm_set_build
		(
        p_set_id IN t208_set_details.C207_SET_ID%TYPE,
        p_tag_id IN t5010_tag.C5010_TAG_ID%TYPE,
        p_out_consign_id OUT t504_consignment.c504_consignment_id%TYPE
		)
AS
	  v_user t101_user.c101_user_id%TYPE;
	  v_initiate_input_str_set_build CLOB;
	  v_part_no t205_part_number.c205_part_number_id%TYPE;
	  v_tag_flag VARCHAR2 (2) ;
	  v_tag_part_number t205_part_number.c205_part_number_id%TYPE;
	  v_tag_control_number VARCHAR2(40);
	  v_input_str_tag_creation CLOB;
	  v_company_id t1900_company.C1900_COMPANY_ID%TYPE;
	  v_plant_id t5040_plant_master.C5040_PLANT_ID%TYPE;
	  v_portal_com_date_fmt VARCHAR2 (100) ;
	  v_qty NUMBER;
	  v_control_num VARCHAR2(40);
	  v_out_request_id VARCHAR2 (20) ;
	  v_out_consign_id VARCHAR2 (20) ;
	  v_errmsg VARCHAR2 (200) ;
	  v_tagable_flag VARCHAR(2);
	  v_input_str_putaway CLOB;
	  v_msg varchar2(10);
	  --
	  v_etch_id t504a_consignment_loaner.c504a_etch_id%TYPE;


CURSOR cur_set IS

	  SELECT JPN.C205_PART_NUMBER_ID PART_NO, JPN.LOT_NUMBER CONTROL_NUMBER,
	  JPN.PHYSICAL_SET_QTY QTY, JPN.TAGGABLE_PART TAG_FLAG
	  FROM JP_LOANER_INV_STG JPN
	  WHERE JPN.C207_SET_ID=p_set_id
	  AND JPN.C5010_TAG_ID=p_tag_id
	  and jpn.set_loaded_fl is null;
	 
	 
	BEGIN

	  SELECT get_compdtfmt_frm_cntx () INTO v_portal_com_date_fmt FROM dual;
	  v_user := IT_PKG_CM_USER.GET_VALID_USER;  
	 
	FOR  inv_set IN cur_set
	 
	 LOOP
	  
	   BEGIN
		   
		   v_qty         :=inv_set.QTY;
		   v_part_no     :=inv_set.PART_NO;
		   v_control_num :=inv_set.CONTROL_NUMBER;   
					 
		 -- Build Input String for Set Build
		   v_initiate_input_str_set_build := v_initiate_input_str_set_build||v_part_no||'^'||v_qty||'^'||v_control_num||'^|';  
	   
		  
		  v_tag_flag    := inv_set.TAG_FLAG;
			
		IF v_tag_flag         	  = 'Y' THEN
		
			v_tagable_flag 		 :='Y'; 
			v_tag_part_number    := v_part_no;
			v_tag_control_number := v_control_num;
				
		END IF;
		  
	  END;
		
	END LOOP;

			gm_pkg_op_process_request.gm_sav_initiate_set(NULL,to_char(CURRENT_DATE, v_portal_com_date_fmt),'50618',NULL,p_set_id,'4127',
			NULL,'50625',v_user,'4120',NULL,NULL,'15',v_user,NULL,CURRENT_DATE,v_out_request_id,v_out_consign_id);

	  IF (v_tagable_flag ='Y') THEN

			v_input_str_tag_creation:=p_tag_id||'^'||v_tag_control_number||'^'||v_tag_part_number||'^'||p_set_id||'^'||'51000^'||v_out_consign_id||'^'||'40033^|';
			gm_pkg_ac_tag_info.gm_sav_tag_detail(v_input_str_tag_creation,v_user,v_errmsg);

		 
		 ELSE
			dbms_output.put_line('The set ID**'||p_set_id||'and the tag id '||p_tag_id||' does not have a tagable part');
			
	  END IF;
		
		-- Set build Processing Lot Number verification step
			gm_save_set_build(v_out_consign_id, v_initiate_input_str_set_build,'JPN Data Migration tag#'||p_tag_id, '1.20', '0', v_user, v_errmsg,'LOANERPOOL');
		
		-- Moving the Set to Loaner Common Pool
			gm_save_set_build(v_out_consign_id, v_initiate_input_str_set_build,'JPN Data Migration tag#'||p_tag_id, '1.20', '1', v_user, v_errmsg,'LOANERPOOL');
		
			gm_pkg_op_loaner.gm_sav_process_picture(v_out_consign_id,v_user,v_msg);

			gm_pkg_op_set_put_txn.gm_sav_set_put (p_tag_id,'080-J-001-J-1',v_user,v_out_consign_id);
		 
			dbms_output.put_line(v_out_consign_id||' : is Moved to Loaner Common Pool');
			
			p_out_consign_id :=v_out_consign_id;
			-- to get the etch id
			BEGIN
				SELECT etch_id
				   INTO v_etch_id
				   FROM jp_loaner_inv_stg
				  WHERE c207_set_id  = p_set_id
				    AND c5010_tag_id = p_tag_id 
				    group by etch_id;
			EXCEPTION    
   				 WHEN OTHERS THEN
   				v_etch_id := NULL;
   			END;	 
			--    
			UPDATE t504_consignment
			SET C504_LAST_UPDATED_BY=v_user, C504_LAST_UPDATED_DATE =CURRENT_DATE, c504_comments='JPN Data Migration tag: '||p_tag_id
			WHERE c504_consignment_id=v_out_consign_id;
			
			UPDATE t504a_consignment_loaner
			SET C504a_LAST_UPDATED_BY=v_user, C504a_LAST_UPDATED_DATE =CURRENT_DATE, c504a_etch_id= v_etch_id
			WHERE c504_consignment_id=v_out_consign_id;
		   
			update JP_LOANER_INV_STG
			set NEW_TXN_ID=v_out_consign_id	
			where c207_set_id=p_set_id
			and c5010_tag_id=p_tag_id;
	    
			INSERT INTO JPN_INV_LOAD_TXN_STATUS (tag_id,set_id,New_CN_ID, Comments) VALUES(p_set_id,p_tag_id,v_out_consign_id,'Set Building Completed. Loaner is in Available Status');
	 
END tmp_gm_set_build;     

/*
 ************************************************
 * The purpose of this procedure is to Initiate a Loaner Set for the given Set ID
 * Ticket: Atec 403 : Japan Loaner Set List Data migration. July 2017 
 * Author: Gomathi Palani. Verified by Richard K.
************************************************* */

PROCEDURE tmp_gm_Loaner_Initiate 
	 (
        p_set_id     IN t208_set_details.C207_SET_ID%TYPE,
        p_acc_id     IN T704_ACCOUNT.C704_EXT_REF_ID%TYPE,
        p_tag_id     IN t5010_tag.C5010_TAG_ID%TYPE,
        p_consign_id IN t504_consignment.c504_consignment_id%TYPE
	 )
AS
    -- Loaner request and Approval
    v_req_id         VARCHAR2 (20) ;
    v_reqdet_litpnum VARCHAR2 (20) ;
    v_req_det_id     VARCHAR2 (20) ;
    v_req_dtl_id     VARCHAR2 (20) ;
    v_dist_id v700_territory_mapping_detail.D_ID%TYPE;
    v_rep_id v700_territory_mapping_detail.rep_id%TYPE;
    v_user NUMBER;
    v_input_str_LN_Initiate CLOB;
    v_request_for NUMBER := '4127';
    v_ship_to     NUMBER := '4120';
    v_request_by  NUMBER := '50626';
    V_LN_CONSIGN_ID T504_CONSIGNMENT.C504_CONSIGNMENT_ID%TYPE;
    v_GMNA_Acct_id t704_account.C704_ACCOUNT_ID%TYPE;
    v_loaner_dt VARCHAR2(40);
    v_dateformat varchar2(100);
    v_shipid number;
    v_shipflag varchar2(10);
    v_input_str_pending_pick CLOB;
    v_status_fl number;
	v_ship_out_user NUMBER := 303137; -- John Davidar        
   
BEGIN
	    
    -- Setting Japan Context
-- gm_pkg_cor_client_context.gm_sav_client_context ('1022', get_code_name ('10306121'), get_code_name ('105131'), '3012');
   select get_compdtfmt_frm_cntx() into v_dateformat from dual;
   v_user := IT_PKG_CM_USER.GET_VALID_USER;
    
      select C704_ACCOUNT_ID INTO v_GMNA_Acct_id from t704_account
      where trim(c704_ext_ref_id) = trim(p_acc_id);
    
      SELECT d_id, v_rep_id
      INTO v_dist_id, v_rep_id
      FROM v700_territory_mapping_detail
      WHERE ac_id= TRIM(v_GMNA_Acct_id);
      
      v_loaner_dt :=to_char(CURRENT_DATE,get_compdtfmt_frm_cntx()); 

      v_input_str_LN_Initiate := p_set_id||'^1'||'^'||v_loaner_dt||'|';

      -- Loaner Initiate
   	  gm_pkg_op_product_requests.gm_sav_product_requests (CURRENT_DATE, v_request_for, v_dist_id,
      v_request_by, v_user, v_ship_to, NULL, v_user, v_input_str_LN_Initiate, v_rep_id, v_GMNA_Acct_id, v_req_id, NULL, NULL) ;
           
     -- Fetching Request ID
      SELECT C526_PRODUCT_REQUEST_DETAIL_ID
      INTO v_req_det_id
      FROM t526_product_request_detail
      WHERE c525_product_request_id = v_req_id;
      
     -- Creating Shipping Record      
      gm_pkg_cm_shipping_trans.gm_sav_shipping(v_req_det_id,'50182','4120',v_dist_id,'5001'
     ,'5004',null,'0',null,v_ship_out_user,v_shipid,null,null);
   	 
       gm_pkg_sm_loaner_allocation.gm_sav_loaner_req_appv (v_req_det_id, v_user, '10', v_req_dtl_id, v_reqdet_litpnum,v_req_det_id) ;     
	 
       v_input_str_pending_pick :=p_consign_id||'^'||null||'^'||p_tag_id||'|';
       gm_pkg_op_set_pick_txn.gm_sav_set_pick_batch(v_input_str_pending_pick, v_user);
            
       gm_pkg_cm_shipping_trans.gm_sav_shipout(p_consign_id,'50182','4120',v_dist_id,'5001','5004'
           	,p_consign_id||':'||p_tag_id,0,0,v_ship_out_user,0,v_shipid,NULL,v_shipflag);    

	   INSERT INTO JPN_INV_LOAD_TXN_STATUS (tag_id,set_id,New_CN_ID, Comments) VALUES(p_set_id,p_tag_id,p_consign_id,'Loaner Initiated and in pending return status');
		
END tmp_gm_Loaner_Initiate;


/*
************************************************
* The purpose of this procedure is to 
* Initiate Account Consignment, FG inventory load and Field Sales warehouse update. This will be based on the strOpt sent.
* Ticket: Atec 403 : Japan Loose Item consignment.
* Author: Gomathi Palani. Verified by Richard K. 
************************************************* */
-- p_opt ACWH, FSWH, FG

PROCEDURE tmp_gm_main_jpn_loose_item
	(
	p_opt IN varchar2

	)
AS
 
      
	  v_set_validation_flag varchar2(10);
	  v_err_dtls clob;
	  v_comp_dt_fmt VARCHAR2(20);

    CURSOR cur_acct
    IS
			
        SELECT ALLADIN_ACCOUNT_ID ACCT_ID, alladin_box_id tag_id, to_char(ship_date,v_comp_dt_fmt) ship_dt
        FROM globus_app.jp_loose_inv_stg JPN
        WHERE 
        jpn.item_loaded_fl ='IAFG_PROCESSED'
        and jpn.loose_item_type= p_opt
        and jpn.alladin_box_id IS NOT NULL
        and jpn.alladin_account_id is not null
        
        group by alladin_account_id, alladin_box_id,ship_date;
        
   CURSOR cur_FSWH
    IS

        SELECT C701_DISTRIBUTOR_ID Dist_id
        FROM globus_app.jp_loose_inv_stg JPN
        WHERE 
        
         jpn.loose_item_type=p_opt
   		and C701_DISTRIBUTOR_ID is not null
   		and item_loaded_fl = 'IAFG_PROCESSED'
        group by C701_DISTRIBUTOR_ID;
        
    
             
 BEGIN   
	 
select get_compdtfmt_frm_cntx () INTO v_comp_dt_fmt  from dual;
	 
	IF (p_opt='ACWH') THEN
		-- iafg 
	tmp_gm_loose_item_iafg(p_opt);
		  FOR loose_item_list IN cur_acct
	
   		  LOOP
    
	 		  BEGIN     	
		 		  
	   			 --tmp_gm_loose_item_validation(p_opt,loose_item_list.tag_id,loose_item_list.ACCT_ID,loose_item_list.ship_dt,null,v_set_validation_flag);

				--IF (v_set_validation_flag='PASS') THEN
				
		           
		           tmp_gm_loose_item_initiate(p_opt,loose_item_list.ACCT_ID,loose_item_list.tag_id, loose_item_list.ship_dt);                      
	               
                    UPDATE jp_loose_inv_stg
                    SET item_loaded_fl    = 'Item INITIATED'
                    WHERE 
                    alladin_box_id = loose_item_list.tag_id
                    AND TO_CHAR(TRUNC(SHIP_DATE), v_comp_dt_fmt) = loose_item_list.ship_dt
                    AND ALLADIN_ACCOUNT_ID=loose_item_list.ACCT_ID
                    AND item_loaded_fl = 'Loose Item Initiated'
                    ;             
        		--END IF;
     			 
        
        
    	    EXCEPTION    
   				 WHEN OTHERS THEN
   				 	Rollback;              
   				  
   				 	dbms_output.put_line ('Loose Item Failure for Acct:'||loose_item_list.ACCT_ID||'And Tag ID:'||loose_item_list.tag_id||SQLERRM);        
	                
	                v_err_dtls :='Loose Item Failure for Acct: Error message: '||SQLERRM;
		          
	              INSERT INTO jp_loose_item_status_log(alladin_box_id,ALLADIN_ACCT_ID,ERR_DTLS) VALUES(loose_item_list.tag_id,loose_item_list.ACCT_ID,v_err_dtls);
	           	  commit;
     
     		END;
     		
      
             
   		COMMIT;   
		
 	END LOOP;
 			
 	ELSIF (p_opt='FSWH') THEN 
 	-- iafg
 	tmp_gm_loose_item_iafg(p_opt);
 	 
 	 FOR loose_item_fswh IN cur_FSWH
	
   		  LOOP
    
	 		  BEGIN     	
		 		  
	--tmp_gm_loose_item_validation(p_opt,null, null,null, loose_item_fswh.Dist_id,v_set_validation_flag);   			 

	--			IF (v_set_validation_flag='PASS') THEN
				
		           
		           tmp_gm_fswh_load(loose_item_fswh.Dist_id,p_opt);                   
	                
      --  		END IF;
     			 
        
        
    	    EXCEPTION    
   				 WHEN OTHERS THEN
   				 	Rollback;              
   				  
   				 	dbms_output.put_line ('Loose Item Failure for fswh for distributor '||loose_item_fswh.Dist_id || ' '|| SQLERRM);        
	                
	                v_err_dtls :='Loose Item Failure for dist: Error message: '||SQLERRM;
		          
	              INSERT INTO jp_loose_item_status_log(DIST_ID,ERR_DTLS) VALUES(loose_item_fswh.Dist_id,v_err_dtls);
	           	  commit;
     
     		END;
     		
      
             
   		COMMIT;   
		
 	END LOOP;
 	
 	ELSIF (p_opt = 'FG')
 		THEN
 				 --tmp_gm_loose_item_validation(p_opt,null, null,NULL,NULL,v_set_validation_flag);
			BEGIN
			--	IF (v_set_validation_flag='PASS') THEN
				
		           tmp_gm_loose_item_iafg(p_opt);
                        
        	--END IF;
     	
        	EXCEPTION    
   			 	WHEN OTHERS THEN
   			 Rollback;              
   				dbms_output.put_line (' IAFG Creation failed ' ||SQLERRM);        
     		END;
       
   		COMMIT;   
		
 	END IF;
 	
 	
END tmp_gm_main_jpn_loose_item;







/************************************************
*-- The purpose of this procedure is to validate the following pre-requiste to avoid Loose item failure
*-- Account Availablity check
*-- Distributor ID check
*-- Part number check
*-- Author: Gomathi Palani. Verified by Rajesh V.
*************************************************/

	PROCEDURE tmp_gm_loose_item_validation
  	   
    AS
      v_company_id t1900_company.c1900_company_id%TYPE;
      v_plant_id t5040_plant_master.c5040_plant_id%TYPE;
      V_ERR_DTLS VARCHAR2(2000);
      v_part_num T205_part_number.c205_part_number_id%TYPE;
      v_cnt NUMBER := 0;
 			   
 	 -- 1 Cursor Dist id check
 	 CURSOR dist_dtls_cur
 	 IS
 	  SELECT c701_distributor_id d_id
   FROM jp_loose_inv_stg
  WHERE item_loaded_fl      IS NULL
    AND c701_distributor_id IS NOT NULL
GROUP BY c701_distributor_id
  MINUS
 SELECT t701.c701_distributor_id d_id
   FROM t701_distributor t701
  WHERE t701.c1900_company_id = v_company_id
    AND t701.c701_void_fl    IS NULL
GROUP BY t701.c701_distributor_id;
 	 -- 2 check Part # valid 
		CURSOR part_num_dtls_cur
		IS
		
 SELECT C205_PART_NUMBER_ID pnum
   FROM jp_loose_inv_stg
  WHERE item_loaded_fl      IS NULL
GROUP BY C205_PART_NUMBER_ID
MINUS
SELECT t2023.C205_PART_NUMBER_ID pnum
FROM T2023_PART_COMPANY_MAPPING t2023
where t2023.C1900_COMPANY_ID = v_company_id
AND t2023.C2023_VOID_FL IS NULL;
-- 3. Account id
CURSOR account_dtls_cur
IS
SELECT ALLADIN_ACCOUNT_ID acc_id
   FROM jp_loose_inv_stg
  WHERE item_loaded_fl     IS NULL
    AND ALLADIN_ACCOUNT_ID IS NOT NULL
GROUP BY ALLADIN_ACCOUNT_ID
  MINUS
 SELECT t704.C704_EXT_REF_ID acc_id
   FROM T704_ACCOUNT t704
  WHERE t704.C1900_COMPANY_ID = v_company_id
    AND t704.C704_void_fl    IS NULL
GROUP BY t704.C704_EXT_REF_ID;
-- part # not available skip all account
CURSOR account_part_skip_cur
IS
 SELECT ALLADIN_ACCOUNT_ID acc_id
   FROM jp_loose_inv_stg
  WHERE C205_PART_NUMBER_ID = v_part_num
    AND ALLADIN_ACCOUNT_ID IS NOT NULL
    AND item_loaded_fl IS NULL
GROUP BY ALLADIN_ACCOUNT_ID;

-- part price check
CURSOR part_price_dtls_cur
IS
select C205_PART_NUMBER_ID pnum from JP_LOOSE_INV_STG
minus
select c205_part_number_id pnum from T2052_PART_PRICE_MAPPING where C1900_COMPANY_ID = v_company_id and C2052_VOID_FL IS NULL;

-- invalid Qty
CURSOR invalid_Qty_cur
IS
SELECT JPN.C205_part_number_id PART_NO
           FROM jp_loose_inv_stg JPN
           WHERE 
           jpn.item_loaded_fl is null
           AND jpn.physical_qty <= 0
           GROUP BY JPN.C205_part_number_id;          

-- distributor check

    BEGIN
  		  SELECT get_compid_frm_cntx(), get_plantid_frm_cntx () INTO v_company_id, v_plant_id FROM dual;
  		  
	    --1. to validate the FSWH
	    --a. Distributor Id
	    --b. part #
	    FOR dist_dtls IN dist_dtls_cur
	    LOOP
	    
	    update jp_loose_inv_stg set item_loaded_fl = 'Skipped' where c701_distributor_id =dist_dtls.d_id
	    AND item_loaded_fl IS NULL;
	    --
	    v_err_dtls :='Distributor id not available : '||dist_dtls.d_id;
	          
	         	 INSERT INTO jp_loose_item_status_log(dist_id,ERR_DTLS)  VALUES(dist_dtls.d_id,v_err_dtls);
	    --     	 
	    v_cnt := v_cnt + 1;
	    
	    END LOOP;
	    DBMS_OUTPUT.PUT_LINE (' Invalid Distributor Count : '|| v_cnt);
	    --
	    v_cnt := 0;
	    -- Part # available check
	    FOR part_num_dtls IN part_num_dtls_cur
	    LOOP
	    	--
	    	v_part_num := part_num_dtls.pnum;
	    	--
	    	update jp_loose_inv_stg set item_loaded_fl = 'Skipped' where c205_part_number_id =part_num_dtls.pnum
	    AND item_loaded_fl IS NULL;
	    --
	    v_err_dtls :='Part # not available : '||part_num_dtls.pnum;
	          
	         	 INSERT INTO jp_loose_item_status_log(part_num,ERR_DTLS)  VALUES(part_num_dtls.pnum,v_err_dtls);
	    
	         -- to check account combination
	         FOR  account_part_skip IN account_part_skip_cur
	         LOOP
	         	update jp_loose_inv_stg set item_loaded_fl = 'Skipped' where alladin_account_id =account_part_skip.acc_id
	    		AND item_loaded_fl IS NULL;
	    		--
	    		 v_err_dtls :='Part number not available : skip the account '||account_part_skip.acc_id;
	          
	         	 INSERT INTO jp_loose_item_status_log(ALLADIN_ACCT_ID ,ERR_DTLS)  VALUES(account_part_skip.acc_id,v_err_dtls);
	         	 
	         END LOOP;
	         --
	         v_cnt := v_cnt + 1;
	    END LOOP;
	    DBMS_OUTPUT.PUT_LINE (' Invalid Part Number Count : '|| v_cnt);
	    --
	    v_cnt := 0;
	    -- part price check
	    FOR part_price_dtls IN part_price_dtls_cur
	    LOOP
	    		--
	    	v_part_num := part_price_dtls.pnum;
	    	--
	    	update jp_loose_inv_stg set item_loaded_fl = 'Skipped' where c205_part_number_id =part_price_dtls.pnum
	    AND item_loaded_fl IS NULL;
	    --
	    v_err_dtls :='Part # price not available : '||part_price_dtls.pnum;
	     INSERT INTO jp_loose_item_status_log(part_num,ERR_DTLS)  VALUES(part_price_dtls.pnum,v_err_dtls);
	    -- 
	    v_cnt := v_cnt + 1;
	    END LOOP;
	    DBMS_OUTPUT.PUT_LINE (' Invalid Part Price Count : '|| v_cnt);
	    --
	    v_cnt := 0;
	    -- account id validate
	    FOR account_dtls IN account_dtls_cur
	    LOOP
	    
	    update jp_loose_inv_stg set item_loaded_fl = 'Skipped' where alladin_account_id =account_dtls.acc_id
	    AND item_loaded_fl IS NULL;
	    --
	    v_err_dtls :='Alladin Account id not available : '||account_dtls.acc_id;
	          
	         	 INSERT INTO jp_loose_item_status_log(ALLADIN_ACCT_ID
,ERR_DTLS)  VALUES(account_dtls.acc_id,v_err_dtls);
	    --
	    v_cnt := v_cnt + 1;
	    END LOOP;
	    
	    DBMS_OUTPUT.PUT_LINE (' Invalid Account Count : '|| v_cnt);
	    -- Invalid Qty records found
	    v_cnt := 0;
	    --
	    FOR invalid_Qty IN invalid_Qty_cur
	    LOOP
	    --
	    update jp_loose_inv_stg set item_loaded_fl = 'Skipped' where c205_part_number_id =invalid_Qty.PART_NO
	    AND physical_qty <= 0
	    AND item_loaded_fl IS NULL;
	    --
	     v_err_dtls :='Negative Qty for the Part # : '||invalid_Qty.PART_NO;
	          
	     INSERT INTO jp_loose_item_status_log(part_num ,ERR_DTLS)  VALUES(invalid_Qty.PART_NO,v_err_dtls);
	    --
	    v_cnt := v_cnt + 1;
	    END LOOP;
	    --
		DBMS_OUTPUT.PUT_LINE (' Negative Part Qty Count : '|| v_cnt);
	
    END tmp_gm_loose_item_validation;
 
  


/*
************************************************
* The purpose of this procedure is to Initiate IAFG for the Loose Items
* Ticket: Atec 403 : Japan Loaner Set List Data migration. July 2017
* Author: Gomathi Palani. Verified by Richard K. 
************************************************* */

PROCEDURE tmp_gm_loose_item_iafg
(
 p_opt     IN varchar2
)

AS
    v_user t101_user.c101_user_id%TYPE;
    v_initiate_input_str_IAFG CLOB;
    v_control_input_str_IAFG CLOB;
    v_verify_input_str_IAFG CLOB;
    v_part_no t205_part_number.c205_part_number_id%TYPE;
    v_company_id          NUMBER;
    v_plant_id            NUMBER;
    v_portal_com_date_fmt VARCHAR2(20) ;
    v_qty                 NUMBER;
    v_purchase_amt        NUMBER := 0;
    v_lot_number   		  VARCHAR2(40) ;
    v_reason	          NUMBER;
    v_txntype             NUMBER;
    v_wh_type             VARCHAR2(100) ;
    v_cur_wh              VARCHAR2(50) ;
    v_txn_type_name       VARCHAR2(50) ;
    v_message             VARCHAR2(100) ;
    v_source_wh           NUMBER;
    v_target_wh           NUMBER;
    v_costing_type        NUMBER;
    V_TXN_ID              VARCHAR2(20) ;
    V_OUT                 NUMBER;
    v_fg_location_id t5052_location_master.C5052_LOCATION_ID%TYPE;
    v_part_price t205_part_number.c205_equity_price%TYPE;
    v_cost t820_costing.c820_purchase_amt%TYPE;
    v_company_cost  t820_costing.C820_LOCAL_COMPANY_COST%TYPE;
    v_count number := 0;
    v_pkey NUMBER;
    v_part_company number;
    v_comp_dt_fmt VARCHAR2(20);
	   
    CURSOR cur_item_iafg
    IS
         SELECT JPN.C205_part_number_id PART_NO, sum(JPN.PHYSICAL_QTY) QTY, jpn.lot_number LOT_NUMBER
           FROM jp_loose_inv_stg JPN
           WHERE 
           jpn.item_loaded_fl is null
           and jpn.loose_item_type=p_opt
           AND jpn.physical_qty > 0
           GROUP BY JPN.C205_part_number_id, jpn.lot_number;
           
       
BEGIN
	
   BEGIN
    
	  v_user := IT_PKG_CM_USER.GET_VALID_USER;
	  select get_compdtfmt_frm_cntx () INTO v_comp_dt_fmt  from dual;

		
	SELECT get_compdtfmt_frm_cntx () INTO v_portal_com_date_fmt FROM dual;
    SELECT get_plantid_frm_cntx  INTO v_plant_id FROM dual;
    SELECT get_compid_frm_cntx() INTO v_company_id FROM dual;
        
     -- Create Location for FG and BL
  BEGIN
	  
    SELECT c5052_location_id INTO v_fg_location_id
    FROM t5052_location_master
    WHERE c5052_location_cd='JPN-FG-LOCATION' 
    and c1900_company_id= v_company_id
    AND c5052_void_fl is null;
    
     EXCEPTION WHEN OTHERS THEN
     dbms_output.put_line('*********Location error' || ' Error:' || sqlerrm);
  END;
  
  -- delete temp table
  delete from MY_TEMP_KEY_VALUE;
  
  FOR inv_item IN cur_item_iafg
  
    LOOP
        BEGIN
		
                v_qty     		:= inv_item.QTY;
                v_part_no 		:= inv_item.PART_NO;
                v_lot_number    := inv_item.LOT_NUMBER;
                v_txn_type_name := 'InvAdj-Shelf';
                v_txntype       := '400068';
                v_target_wh     := '90800';
                v_source_wh     := '400069';
                v_cur_WH        := 'FG';
                v_wh_type       := '90800';
                v_costing_type  := '4900';
                v_reason        := '90800';                
                --v_pkey 			:= inv_item.pkey;
                
                BEGIN
                     SELECT COST, company_cost
                       INTO v_purchase_amt, v_company_cost
                       FROM jpn_dm_inventory
                       WHERE part      = v_part_no
                       AND warehouse = 'FG';
                EXCEPTION
                WHEN NO_DATA_FOUND THEN
        
                    v_purchase_amt := '0';
                    v_company_cost :='0';
                END;
                
                SELECT C1900_COMPANY_ID INTO v_part_company FROM  t2023_part_company_mapping t2023
                WHERE t2023.c205_part_number_id=v_part_no
                and c901_part_mapping_type='105360'
                AND C2023_void_fl is null;
                
              BEGIN
                SELECT count(1) INTO v_cost FROM t820_costing
                WHERE c205_part_number_id=v_part_no
                AND c820_delete_fl = 'N'
                --and c901_status=4801
                and c901_costing_type=4900
                and c1900_company_id=v_company_id;
                
                  EXCEPTION
                WHEN OTHERS THEN
        
                  v_cost:=NULL;
                END;
                
              
      
    IF v_cost = 0 THEN
              
		       INSERT INTO t820_costing (
		           c820_costing_id
		          ,c205_part_number_id
		          ,c901_costing_type
		          ,c820_part_received_dt
		          ,c820_qty_received
		          ,c820_purchase_amt
		          ,c820_qty_on_hand       
		          ,c901_status
		          ,c820_delete_fl
		          ,c820_created_by
		          ,c820_created_date
		          ,c1900_company_id
		          ,c5040_plant_id
		          ,C820_LOCAL_COMPANY_COST  
		          ,C1900_OWNER_COMPANY_ID
		          ,c901_owner_currency
		          ,C901_LOCAL_COMPANY_CURRENCY
		        ) 
		        VALUES 
		        (	 s820_costing.nextval
		          ,v_part_no
		          ,v_costing_type
		          ,CURRENT_DATE
		          ,0
		          ,v_purchase_amt
		          ,0
		          ,4801
		          ,'N'
		          ,V_USER
		          ,CURRENT_DATE
		          ,v_company_id
		          ,v_plant_id
		          ,v_company_cost
		          ,v_part_company
		          ,'1'
		          ,'4000917'
		        );
                               
   END IF;               
                                         
    --Get Part price
      BEGIN
	      
		        SELECT	c2052_equity_price price
		        INTO v_part_price
		        FROM t2052_part_price_mapping  
		        WHERE c205_part_number_id = v_part_no
		        AND c1900_company_id =v_company_id;
        
      EXCEPTION  WHEN no_data_found THEN
      
		          dbms_output.put_line('v_part_no=' || v_part_no || ' Price not found');
		          v_part_price := null;
		          dbms_output.put_line(v_part_no ||' - Without Equity Price, Inventory migration cannot be done, skip to next');
		          continue;
          
      END;
      
      	IF (v_txn_id is null) THEN
                
      		   		SELECT get_next_consign_id (v_txn_type_name) INTO v_txn_id FROM dual;
                
      	END IF;
           
	        		INSERT INTO MY_TEMP_KEY_VALUE (MY_TEMP_TXN_ID,MY_TEMP_TXN_KEY, my_temp_txn_value) values(p_opt ,v_part_no, v_lot_number);
	  			
	              
	                v_initiate_input_str_IAFG := v_initiate_input_str_IAFG || v_part_no || ',' || v_qty || ',,,' ||v_part_price || '|';
	                --dbms_output.put_line ('v_initiate_input_str_IAFG '|| v_initiate_input_str_IAFG);
	                
	                v_control_input_str_IAFG := v_control_input_str_IAFG || v_part_no || ',' || v_qty || ',' ||v_lot_number || ',,' || v_part_price || '|';
	                
	                v_verify_input_str_IAFG := v_verify_input_str_IAFG || v_part_no || ',' || v_qty ||','|| v_lot_number|| ','||v_fg_location_id||',' || v_target_wh || '|';
	                
	                -- Calling FS location procedure to update the part qty
	                gm_pkg_op_inv_field_sales_tran.gm_sav_fs_inv_loc_part_details (v_fg_location_id,v_txn_id, v_part_no,v_qty,'400068',4301,null,current_date, v_user);
                
         v_count :=v_count+1;
                
           IF(v_count=20) then 
                
	                 gm_save_inhouse_item_consign (v_txn_id, v_txntype, v_reason, '0', '', 'IAFG created for the Loose item Adjustment', v_user, v_initiate_input_str_IAFG, v_out) ;
	                 
	                 GM_LS_UPD_INLOANERCSG (v_txn_id, v_txntype, '0', '0', NULL, 'Japan Set Data Load', v_user,v_control_input_str_IAFG, NULL, 'ReleaseControl', v_message) ;
	            
	            	 gm_ls_upd_inloanercsg (v_txn_id, v_txntype, '', '0', NULL, 'Japan -- Manual Data --load', v_user,v_verify_input_str_IAFG, NULL, 'Verify', v_message) ;
	            
	            	 --UPDATE MY_TEMP_KEY_VALUE_IAFG SET     MY_TEMP_TXN_VALUE= v_txn_id  WHERE MY_TEMP_TXN_VALUE IS NULL;
	             
	             	 UPDATE t412_inhouse_transactions SET c412_comments='IAFG Inventory Update - '|| p_opt  , c412_last_updated_date=CURRENT_DATE, c412_last_updated_by=v_user
				 	 where c412_inhouse_trans_id=v_txn_id;
		
				 	 	--dbms_output.put_line (' Values ' || p_opt|| p_acct_id || p_box_id || p_date || p_dist_id);
				 	 UPDATE jp_loose_inv_stg T1 SET item_loaded_fl='IAFG_PROCESSED',  new_cn_id = v_txn_id
						WHERE c205_part_number_id IN (select MY_TEMP_TXN_KEY from MY_TEMP_KEY_VALUE where  MY_TEMP_TXN_ID = p_opt)
						AND lot_number IN (select my_temp_txn_value from MY_TEMP_KEY_VALUE where  MY_TEMP_TXN_ID = p_opt)
						and loose_item_type=P_OPT
						AND item_loaded_fl IS NULL;
			-- Re-Setting the flag for the next IAFG creation.
			
		            v_count :='0';
		            v_initiate_input_str_IAFG :=NULL;
		            v_control_input_str_IAFG :=NULL;
		            v_verify_input_str_IAFG :=NULL;
		            v_txn_id := NULL;
		            --
		            DELETE FROM MY_TEMP_KEY_VALUE;
            
          END IF;
          
       END;
              
 END LOOP;
            
           IF (v_initiate_input_str_IAFG IS NOT NULL) THEN
            
		              gm_save_inhouse_item_consign (v_txn_id, v_txntype, v_reason, '0', '', 'IAFG created for the Loose item Adjustment', v_user, v_initiate_input_str_IAFG, v_out) ;
		            
		              GM_LS_UPD_INLOANERCSG (v_txn_id, v_txntype, '0', '0', NULL, 'Japan Set Data Load', v_user,
		              v_control_input_str_IAFG, NULL, 'ReleaseControl', v_message) ;
		            
		              gm_ls_upd_inloanercsg (v_txn_id, v_txntype, '', '0', NULL, 'Japan -- Manual Data --load', v_user,
		              v_verify_input_str_IAFG, NULL, 'Verify', v_message) ;
		            
		              DBMS_OUTPUT.PUT_LINE (' v_txn_id ' || v_txn_id);
		              
		              --UPDATE MY_TEMP_KEY_VALUE_IAFG SET     MY_TEMP_TXN_VALUE= v_txn_id  WHERE MY_TEMP_TXN_VALUE IS NULL;
					
		              UPDATE t412_inhouse_transactions SET c412_comments='IAFG Inventory Update - '|| p_opt ,c412_last_updated_date=CURRENT_DATE, c412_last_updated_by=v_user
					  where c412_inhouse_trans_id=v_txn_id;
			 
					  --dbms_output.put_line (' Values 222 '|| p_opt|| p_acct_id || p_box_id || p_date || p_dist_id);
					  UPDATE jp_loose_inv_stg T1 SET item_loaded_fl='IAFG_PROCESSED', new_cn_id = v_txn_id
						WHERE c205_part_number_id IN (select MY_TEMP_TXN_KEY from MY_TEMP_KEY_VALUE where  MY_TEMP_TXN_ID = p_opt)
						AND lot_number IN (select my_temp_txn_value from MY_TEMP_KEY_VALUE where  MY_TEMP_TXN_ID = p_opt)
						and loose_item_type=P_OPT
						AND item_loaded_fl IS NULL;
						
			          v_count :='0';
			          v_initiate_input_str_IAFG :=NULL;
			          v_control_input_str_IAFG :=NULL;
			          v_verify_input_str_IAFG :=NULL;
			          v_txn_id := NULL;
			          --
			          DELETE FROM MY_TEMP_KEY_VALUE;
          
     	   END IF;  
         
		 
			
       	COMMIT;
       	
    EXCEPTION WHEN OTHERS then
    	  DBMS_OUTPUT.PUT_LINE('Loose Item IAFG ERROR for part '||v_part_no || ' Error details: '||SQLERRM);
    ROLLBACK;
 
  END; 
END tmp_gm_loose_item_iafg;



/*
************************************************
* The purpose of this procedure is to Initiate Account Initite for the Loose items
* Ticket: Atec 403 : Japan Loaner Set List Data migration. July 2017
* Author: Gomathi Palani. Verified by Richard K.
************************************************* */

PROCEDURE tmp_gm_loose_item_initiate
(
p_opt VARCHAR2,
p_acct_id t704_account.c704_account_id%TYPE,
p_box_id  t5010_tag.c5010_tag_id%TYPE,
p_date IN VARCHAR2


)

AS
    v_user t101_user.c101_user_id%TYPE;
    v_input_str_item_consign CLOB;
    v_control_str_item_consign CLOB;
    v_part_no t205_part_number.c205_part_number_id%TYPE;
    v_company_id          t1900_company.C1900_COMPANY_ID%TYPE;
    v_plant_id            t5040_plant_master.C5040_PLANT_ID%TYPE;
    v_portal_com_date_fmt VARCHAR2 (20) ;
    v_qty                 NUMBER;
    v_acc_id T704_ACCOUNT.c704_ext_ref_id%TYPE;
    v_lot_number       VARCHAR2 (40) ;
    v_ship_date        VARCHAR2 (100) ;
    v_req_id           VARCHAR2 (20) ;
    v_out_cn_id        VARCHAR2 (20) ;
    V_OUT              NUMBER;
    v_GMNA_Acct_id    t704_account.C704_ACCOUNT_ID%TYPE;
    v_strTxnType       NUMBER        := '4110';
    v_strRelVerFl      VARCHAR2 (10) := 'on';
    v_strLocType       NUMBER        := '93343';
    v_shipid           NUMBER;
    v_shipflag         VARCHAR2 (10) ;
    v_fg_location_id t5052_location_master.C5052_LOCATION_ID%TYPE;
    v_pkey number;
    --
    v_ship_out_user NUMBER := 303137; -- John Davidar
    
       
    CURSOR cur_item_initiate
    IS
         SELECT JPN.C205_part_number_id PART_NO, JPN.PHYSICAL_QTY QTY, jpn.lot_number LOT_NUMBER, jpn.pkey pkey
           FROM jp_loose_inv_stg JPN
          WHERE JPN.alladin_box_id  	   =p_box_id
            AND JPN.ALLADIN_ACCOUNT_ID = p_acct_id
            AND TO_CHAR(TRUNC(jpn.SHIP_DATE), v_portal_com_date_fmt) = p_date
            AND jpn.loose_item_type=p_opt
            AND jpn.item_loaded_fl      = 'IAFG_PROCESSED'
       ORDER BY C205_part_number_id;
       
BEGIN
	
    
     SELECT get_compdtfmt_frm_cntx () INTO v_portal_com_date_fmt FROM dual;
     SELECT get_plantid_frm_cntx INTO v_plant_id FROM dual;
     SELECT get_compid_frm_cntx () INTO v_company_id FROM dual;
  
 	  v_user := IT_PKG_CM_USER.GET_VALID_USER;  

 	  BEGIN
				
			     SELECT c5052_location_id
			       INTO v_fg_location_id
			       FROM t5052_location_master
			      WHERE c5052_location_CD = 'JPN-FG-LOCATION'
			        AND c1900_company_id  = v_company_id
			        AND c5052_void_fl    IS NULL;
			        
				EXCEPTION
				WHEN OTHERS THEN
			    DBMS_OUTPUT.PUT_LINE ('*********Location error item init ' || ' Error:' || SQLERRM) ;
			END;           

			 SELECT C704_ACCOUNT_ID
				  INTO v_GMNA_Acct_id
				  FROM t704_account
				  WHERE c704_ext_ref_id = p_acct_id
				  AND c1900_company_id  = v_company_id;
    
BEGIN
	
	 
	            
  	 FOR inv_item_consign IN cur_item_initiate
    
        LOOP
             v_qty        := inv_item_consign.QTY;
             v_part_no    := inv_item_consign.PART_NO;
             v_lot_number := inv_item_consign.LOT_NUMBER;
             v_pkey       := inv_item_consign.pkey;
                
				 
		           
      			INSERT INTO MY_TEMP_KEY_VALUE_loose_item(MY_TEMP_TXN_ID,MY_TEMP_TXN_KEY) values(v_pkey,v_part_no);
                    
                v_input_str_item_consign   := v_input_str_item_consign|| v_part_no||'^'||v_qty||'^'||v_ship_date||'^|';
                
     			v_control_str_item_consign := v_control_str_item_consign||v_part_no||','||v_qty||','||v_lot_number||','||v_fg_location_id||','||'TBE'||','||'90800'||'|';
                
   END LOOP;
            
                v_input_str_item_consign := v_input_str_item_consign||'~5001^5004|';
                DBMS_OUTPUT.PUT_LINE(' v_input_str_item_consign '|| v_input_str_item_consign);
                 
                gm_pkg_op_account_request.gm_sav_initiate_request(NULL,CURRENT_DATE,50618,NULL,NULL,40025,v_GMNA_Acct_id,50625,V_USER,4120,NULL,NULL,
                15,V_USER,v_input_str_item_consign,50060,'',v_GMNA_Acct_id,'',CURRENT_DATE,'',v_req_id,v_out_cn_id,NULL);
	                
   	            dbms_output.put_line ('v_req_id: '||v_req_id) ;
   	            dbms_output.put_line ('v_out_cn_id: '||v_out_cn_id) ;
   	                  
   	             -- Added the below procedure to enable the Initate RA flag
   	             gm_pkg_op_return_txn.gm_sav_return_fl_for_req(v_req_id,'Y',V_USER);
                      dbms_output.put_line (' Returns initate : ') ;
   	              -- Creating Shipping record    
                 gm_pkg_cm_shipping_trans.gm_sav_shipping (v_req_id, '50184', '4120', v_GMNA_Acct_id, '5001', '5004', NULL,
                '0', NULL, v_ship_out_user, v_shipid, NULL, NULL) ;
                  	       dbms_output.put_line (' ship out request : ') ;
                  	                    	  
                -- controlling the Account consignment
               	gm_pkg_op_item_control_txn.gm_sav_item_control_main (v_out_cn_id, v_strTxnType, v_strRelVerFl,
               	v_control_str_item_consign, v_strLocType, V_USER);
                    	dbms_output.put_line (' Item controld main : ') ;
    			-- Ship Out
                gm_pkg_cm_shipping_trans.gm_sav_shipout (v_out_cn_id, '50181', '4120', v_GMNA_Acct_id, '5001', '5004',
                v_out_cn_id|| ':Item', 0, 0, v_ship_out_user, 0, v_shipid, NULL, v_shipflag) ;
                    	dbms_output.put_line (' ship out cn main : '|| v_out_cn_id || ' v_GMNA_Acct_id '|| v_GMNA_Acct_id) ;
                -- Account Net Qty by Lot (Net Number Procedure)
                --commit;
                gm_pkg_set_lot_track.GM_PKG_SET_LOT_UPDATE(v_out_cn_id,V_USER,v_GMNA_Acct_id,'50181','5');
                
dbms_output.put_line (' LOT update : ') ;
                UPDATE t504_consignment
                SET C504_LAST_UPDATED_BY = v_user, C504_LAST_UPDATED_DATE = CURRENT_DATE, c504_comments =
                'JPN Data Migration:Loose Items for Alladin Acc: '||p_acct_id
                WHERE c504_consignment_id = v_out_cn_id;
                      
                UPDATE MY_TEMP_KEY_VALUE_loose_item SET MY_TEMP_TXN_VALUE= v_out_cn_id 
                WHERE MY_TEMP_TXN_VALUE IS NULL;
                    	
                dbms_output.put_line ('Loose Item Initiated for the Acct ID: '||v_GMNA_Acct_id) ;
 
                    
		        UPDATE jp_loose_inv_stg T1 SET item_loaded_fl='Loose Item Initiated',
				T1.NEW_CN_ID = (SELECT T2.MY_TEMP_TXN_VALUE FROM MY_TEMP_KEY_VALUE_loose_item T2 WHERE T2.MY_TEMP_TXN_ID = T1.PKEY)
				WHERE T1.PKEY IN (SELECT T2.MY_TEMP_TXN_ID FROM MY_TEMP_KEY_VALUE_loose_item T2 WHERE T2.MY_TEMP_TXN_ID = T1.PKEY)
				AND item_loaded_fl='IAFG_PROCESSED';
			
				-- Need to update the GM-CN
			--	INSERT INTO jp_loose_item_status_log (alladin_box_id,ALLADIN_ACCT_ID,New_CN_ID, Comments) VALUES(p_set_id,p_box_id,v_out_consign_id,'Set Building Completed. Loaner in Available Status');
	      
			--	COMMIT;
	  	   		 	   
		    EXCEPTION WHEN OTHERS then
            DBMS_OUTPUT.PUT_LINE('Loose Item CONSIGNMENT ERROR for part '||v_part_no||SQLERRM);
          --  ROLLBACK;
   	      END;
        
   	  
    
	
END tmp_gm_loose_item_initiate;

/*
This procedure is created to create the Item consignment for the field sales warehouse.
Author :gpalani
TSK: 8279

*/

PROCEDURE tmp_gm_fswh_load
(
 p_dist in t701_distributor.c701_distributor_id%TYPE,
 p_opt IN VARCHAR2

)


AS

    v_user t101_user.c101_user_id%TYPE;
    v_input_str_item_consign CLOB;
    v_control_str_item_consign CLOB;
    v_part_no t205_part_number.c205_part_number_id%TYPE;
    v_company_id          t1900_company.C1900_COMPANY_ID%TYPE;
    v_plant_id            t5040_plant_master.C5040_PLANT_ID%TYPE;
    v_portal_com_date_fmt VARCHAR2 (20) ;
    v_qty                 NUMBER;
    v_dist_id t701_distributor.C701_DISTRIBUTOR_ID%TYPE;
    v_lot_number       VARCHAR2 (40) ;
    v_txntype          NUMBER;
    v_ship_date        VARCHAR2 (100) ;
    v_out_cn_id        VARCHAR2 (20) ;
    v_req_id           VARCHAR2 (20) ;
    v_strTxnType         NUMBER        := '4110';
    v_strRelVerFl        VARCHAR2 (10) := 'on';
    v_strLocType         NUMBER        := '93343';
    v_shipid           NUMBER;
    v_shipflag         VARCHAR2 (10) ;
    v_fg_location_id t5052_location_master.C5052_LOCATION_ID%TYPE;
    v_count      NUMBER := 0;
    v_count_item NUMBER := 0;
    v_pkey number;
    --
    v_ship_out_user NUMBER := 303137; -- John Davidar  
	
	   
    CURSOR cur_item_initiate
    IS
         SELECT JPN.C205_part_number_id PART_NO, JPN.PHYSICAL_QTY QTY, jpn.lot_number LOT_NUMBER, jpn.pkey pkey
         FROM jp_loose_inv_stg JPN
         WHERE JPN.alladin_box_id      IS NULL
		 AND C701_DISTRIBUTOR_ID= p_dist
		 AND jpn.loose_item_type = p_opt
		 AND jpn.physical_qty > 0
		AND JPN.item_loaded_fl='IAFG_PROCESSED'
         ORDER BY C205_part_number_id;
BEGIN
    -- gm_pkg_cor_client_context.gm_sav_client_context ('1022', get_code_name ('10306121'), get_code_name ('105131'),'3012');
	  	v_user := IT_PKG_CM_USER.GET_VALID_USER;
        SELECT get_compdtfmt_frm_cntx () INTO v_portal_com_date_fmt FROM dual;
        SELECT get_plantid_frm_cntx INTO v_plant_id FROM dual;
        SELECT get_compid_frm_cntx () INTO v_company_id FROM dual;
    -- Create Location for FG and BL
    BEGIN
         SELECT c5052_location_id
         INTO v_fg_location_id
         FROM t5052_location_master
         WHERE c5052_location_cd = 'JPN-FG-LOCATION'
         AND c1900_company_id  = v_company_id
         AND c5052_void_fl is null;
    
    EXCEPTION
	
		WHEN OTHERS THEN
        dbms_output.put_line ('*********Location error fiekd sales warehouse ' || ' Error:' || sqlerrm) ;
    END;
    
    BEGIN
	    v_user := IT_PKG_CM_USER.GET_VALID_USER;
    
       v_dist_id := p_dist;
			
				
				   FOR inv_item_consign IN cur_item_initiate
				
				     LOOP
						v_qty        := inv_item_consign.QTY;
						v_part_no    := inv_item_consign.PART_NO;
						v_lot_number := inv_item_consign.LOT_NUMBER;
						v_pkey 		 := inv_item_consign.pkey;
         
                    INSERT INTO MY_TEMP_KEY_VALUE_loose_item(MY_TEMP_TXN_ID,MY_TEMP_TXN_KEY) values(v_pkey,v_part_no);
                 
					v_input_str_item_consign   := v_input_str_item_consign|| v_part_no||'^'||v_qty||'^'||v_ship_date||'^|';
					v_control_str_item_consign := v_control_str_item_consign||v_part_no||','||v_qty||','||v_lot_number||','||v_fg_location_id||','||'TBE'||','||'90800'||'|';
                
					v_count_item                 := v_count_item + 1;
                
                IF (v_count_item >= 20) THEN
                
                  	  v_input_str_item_consign := v_input_str_item_consign||'~5001^5004|';
                    
                   	   gm_pkg_op_process_prod_request.gm_sav_initiate_item (NULL, CURRENT_DATE, 50618, NULL, 40021, v_dist_id, 50625, V_USER, 4120, NULL, NULL, 15, V_USER, v_input_str_item_consign, 50060, '',CURRENT_DATE, v_req_id, v_out_cn_id, NULL) ;
                    
                 	   gm_pkg_cm_shipping_trans.gm_sav_shipping (v_req_id, '50184', '4120', v_dist_id, '5001', '5004', NULL,'0', NULL, v_ship_out_user, v_shipid, NULL, NULL);
                  	  
                    -- controlling the Item consignment
                    	gm_pkg_op_item_control_txn.gm_sav_item_control_main (v_out_cn_id, v_strTxnType, v_strRelVerFl,v_control_str_item_consign, v_strLocType, V_USER);
                    	
                    	gm_pkg_cm_shipping_trans.gm_sav_shipout (v_out_cn_id, '50181', '4120', v_dist_id, '5001', '5004',v_out_cn_id|| ':FSWH', 0, 0, v_ship_out_user, 0, v_shipid, NULL, v_shipflag);
                    	v_count_item               := 0;
                    	v_input_str_item_consign   := NULL;
                    	v_control_str_item_consign := NULL;
                    	v_shipid                   := NULL;
                        
                    	UPDATE t504_consignment SET C504_LAST_UPDATED_BY = v_user, C504_LAST_UPDATED_DATE = CURRENT_DATE, c504_comments ='FSWH inventory update for the distributor: '||v_dist_id
                      	WHERE c504_consignment_id = v_out_cn_id;
                      
                        UPDATE MY_TEMP_KEY_VALUE_loose_item SET    MY_TEMP_TXN_VALUE= v_out_cn_id  WHERE MY_TEMP_TXN_VALUE IS NULL;
                    	
                      	dbms_output.put_line ('FSWH inventory update for the Distributor '||v_dist_id);
                END IF;
            END LOOP;
            
            IF (v_input_str_item_consign IS NOT NULL) THEN
            
                v_input_str_item_consign := v_input_str_item_consign||'~5001^5004|';
                
                gm_pkg_op_process_prod_request.gm_sav_initiate_item (NULL, CURRENT_DATE, 50618, NULL, 40021, v_dist_id,50625, V_USER, 4120, NULL, NULL, 15, V_USER, v_input_str_item_consign, 50060, '', CURRENT_DATE,v_req_id, v_out_cn_id, NULL) ;
              				      
                gm_pkg_cm_shipping_trans.gm_sav_shipping (v_req_id, '50184', '4120', v_dist_id, '5001', '5004', NULL, '0', NULL, v_ship_out_user, v_shipid, NULL, NULL) ;
                
                -- controlling the Item consignment
                gm_pkg_op_item_control_txn.gm_sav_item_control_main (v_out_cn_id, v_strTxnType, v_strRelVerFl,v_control_str_item_consign, v_strLocType, V_USER) ;
                
                gm_pkg_cm_shipping_trans.gm_sav_shipout (v_out_cn_id, '50181', '4120', v_dist_id, '5001', '5004',v_out_cn_id|| ':FSWH', 0, 0, v_ship_out_user, 0, v_shipid, NULL, v_shipflag) ;
                
                 UPDATE t504_consignment
                 SET C504_LAST_UPDATED_BY = v_user, C504_LAST_UPDATED_DATE = CURRENT_DATE, c504_comments ='FSWH inventory update for the distributor: '||v_dist_id   WHERE c504_consignment_id = v_out_cn_id;
                  
                 UPDATE MY_TEMP_KEY_VALUE_loose_item SET MY_TEMP_TXN_VALUE= v_out_cn_id  WHERE MY_TEMP_TXN_VALUE IS NULL;
                    
                 dbms_output.put_line ('FSWH inventory updated for the Distributor: '||v_dist_id);
                 
                 v_dist_id                  := NULL;
                 v_count_item               := 0;
                 v_input_str_item_consign   := NULL;
                 v_control_str_item_consign := NULL;
                 v_shipid                   := NULL;
            
			END IF;
                    
                 UPDATE jp_loose_inv_stg T1 SET item_loaded_fl='FSWH Initiated',T1.NEW_CN_ID = (SELECT T2.MY_TEMP_TXN_VALUE FROM MY_TEMP_KEY_VALUE_loose_item T2 WHERE T2.MY_TEMP_TXN_ID = T1.PKEY)
			     WHERE T1.PKEY IN (SELECT T2.MY_TEMP_TXN_ID FROM MY_TEMP_KEY_VALUE_loose_item T2 WHERE T2.MY_TEMP_TXN_ID = T1.PKEY)
			     AND item_loaded_fl='IAFG_PROCESSED'
			     AND C701_DISTRIBUTOR_ID= p_dist
			     AND LOOSE_ITEM_TYPE=P_OPT;
                 
	      
		COMMIT;
	  	   		 	   
		    EXCEPTION WHEN OTHERS then
		    
		    v_dist_id                  := NULL;
            v_count_item               := 0;
            v_input_str_item_consign   := NULL;
            v_control_str_item_consign := NULL;
            v_shipid                   := NULL;
            
            DBMS_OUTPUT.PUT_LINE('FSWH inventory error '||v_part_no||SQLERRM);
           ROLLBACK;
   	      end;
   	      
    

END tmp_gm_fswh_load;

END tmp_gm_inventory_load;
 /