CREATE OR REPLACE PACKAGE BODY tmp_gm_pkg_swap_set_dtls
IS
    /*
    * Purpose: Procedure to write off the old parts and create the costing layer for new parts
    */
PROCEDURE gm_sav_set_swap_correction (
        p_cn_id t504_consignment.c504_consignment_id%TYPE)
AS
    v_user t101_user.c101_user_id%TYPE;
    v_cnt NUMBER;
    v_inv_to_loaner_ma_id t215_part_qty_adjustment.c215_part_qty_adj_id%TYPE;
    v_loaner_to_inv_ma_id t215_part_qty_adjustment.c215_part_qty_adj_id%TYPE;
    v_part_company t1900_company.c1900_company_id%TYPE;
    --
    v_purchase_amt t820_costing.c820_purchase_amt%TYPE;
    v_company_cost t820_costing.C820_LOCAL_COMPANY_COST%TYPE;
    --
    v_txn_company_id t1900_company.c1900_company_id%TYPE;
    v_txn_plant_id t5040_plant_master.c5040_plant_id%TYPE;
    --
    CURSOR set_swap_cur
    IS
         SELECT tag_id tag, set_id set_id, OLD_TAGGABLE_PART_NUM old_pnum
          , NEW_TAGGABLE_PART_NUM new_pnum
           FROM JPN_SETID_SWAP_TXN_STATUS
          WHERE NEW_CN_ID    = p_cn_id
            AND status_flag IS NULL;
BEGIN
    -- validate the CN
     SELECT COUNT (1)
       INTO v_cnt
       FROM JPN_SETID_SWAP_TXN_STATUS
      WHERE NEW_CN_ID    = p_cn_id
        AND status_flag IS NULL;
    --
    IF v_cnt = 0 THEN
        raise_application_error ('-20999', ' Invalid consignment id '|| p_cn_id) ;
    END IF;
    --
    IF v_cnt > 1 THEN
        raise_application_error ('-20999', ' Morethan one taggable part present - please verify the consignment id '||
        p_cn_id) ;
    END IF;
    -- to set the company context
     SELECT c1900_company_id, c5040_plant_id
       INTO v_txn_company_id, v_txn_plant_id
       FROM t504_consignment
      WHERE c504_consignment_id = p_cn_id
        AND c504_void_fl       IS NULL;
    --
    IF v_txn_company_id IS NULL OR v_txn_plant_id IS NULL THEN
        raise_application_error ('-20999', ' Invalid company/plant information please verify the CN id '|| p_cn_id) ;
    END IF;
    --
    gm_pkg_cor_client_context.gm_sav_client_context (v_txn_company_id, v_txn_plant_id) ;
    v_user := 303043; -- Richard
    --1. create the MA
    -- 4845 Loaner to Inv Adjustment
    v_loaner_to_inv_ma_id := 'GM-MA-' || s215_part_qty_adjustment.NEXTVAL;
     INSERT
       INTO t215_part_qty_adjustment
        (
            c215_part_qty_adj_id, c215_adj_date, c901_adjustment_source
          , c215_comments, c215_validated_by, c215_approved_by
          , c215_created_by, c215_created_date, c215_update_inv_fl
          , c1900_company_id
        )
        VALUES
        (
            v_loaner_to_inv_ma_id, CURRENT_DATE, 4845
          , 'Loaner to inv adjustment (Set swap) ' || p_cn_id, v_user, v_user
          , v_user, CURRENT_DATE, NULL
          , 1026
        ) ;
    DBMS_OUTPUT.PUT_LINE ('Loaner to Inventory Adj - (Set swap) ' || p_cn_id ||' MA IDs '|| v_loaner_to_inv_ma_id) ;
    --2. create the MA
    -- 48063 Inv Adjustment to Loaner
    v_inv_to_loaner_ma_id := 'GM-MA-' || s215_part_qty_adjustment.NEXTVAL;
     INSERT
       INTO t215_part_qty_adjustment
        (
            c215_part_qty_adj_id, c215_adj_date, c901_adjustment_source
          , c215_comments, c215_validated_by, c215_approved_by
          , c215_created_by, c215_created_date, c215_update_inv_fl
          , c1900_company_id
        )
        VALUES
        (
            v_inv_to_loaner_ma_id, CURRENT_DATE, 48063
          , 'Inv Adj to Loaner (Set swap) ' || p_cn_id, v_user, v_user
          , v_user, CURRENT_DATE, NULL
          , 1026
        ) ;
    DBMS_OUTPUT.PUT_LINE ('Inventory Adj to Loaner - (Set swap) ' || p_cn_id ||' MA IDs '|| v_inv_to_loaner_ma_id) ;
    --
    FOR set_swap_dtls IN set_swap_cur
    LOOP
        --3 old part #
        -- to get the owner company id
         SELECT C1900_COMPANY_ID
           INTO v_part_company
           FROM t2023_part_company_mapping t2023
          WHERE t2023.c205_part_number_id = set_swap_dtls.old_pnum
            AND c901_part_mapping_type    = '105360'
            AND t2023.C2023_VOID_FL      IS NULL;
        -- to get the cost
        BEGIN
             SELECT COST, company_cost
               INTO v_purchase_amt, v_company_cost
               FROM jpn_dm_inventory
              WHERE part      = set_swap_dtls.old_pnum
                AND warehouse = 'FG';
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_purchase_amt := '0';
            v_company_cost := '0';
            --   dbms_output.put_line('v_purchase_amt=' || v_purchase_amt);
        END;
        --
        DBMS_OUTPUT.PUT_LINE ('Loaner to Inv -- CN id ' || p_cn_id ||' Old Part # '|| set_swap_dtls.old_pnum) ;
        --
         INSERT
           INTO t216_part_qty_adj_details
            (
                c216_part_qty_adj_id, c215_part_qty_adj_id, c205_part_number_id
              , c216_qty_to_be_adjusted, c216_item_price, c1900_owner_company_id
              , c216_local_item_price, c216_created_by, c216_created_date
            )
            VALUES
            (
                s216_part_qty_adj_details.NEXTVAL, v_loaner_to_inv_ma_id, set_swap_dtls.old_pnum
              , 1, v_purchase_amt, v_part_company
              , v_company_cost, v_user, CURRENT_DATE
            ) ;
        --4. new part #
        -- to get the owner company id
         SELECT C1900_COMPANY_ID
           INTO v_part_company
           FROM t2023_part_company_mapping t2023
          WHERE t2023.c205_part_number_id = set_swap_dtls.new_pnum
            AND c901_part_mapping_type    = '105360'
            AND t2023.C2023_VOID_FL      IS NULL;
        -- to get the cost
        BEGIN
             SELECT COST, company_cost
               INTO v_purchase_amt, v_company_cost
               FROM jpn_dm_inventory
              WHERE part      = set_swap_dtls.new_pnum
                AND warehouse = 'FG';
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_purchase_amt := '0';
            v_company_cost := '0';
            --   dbms_output.put_line('v_purchase_amt=' || v_purchase_amt);
        END;
        --
        DBMS_OUTPUT.PUT_LINE ('Inv to Loaner -- CN id ' || p_cn_id ||' New Part # '|| set_swap_dtls.new_pnum) ;
        --
         INSERT
           INTO t216_part_qty_adj_details
            (
                c216_part_qty_adj_id, c215_part_qty_adj_id, c205_part_number_id
              , c216_qty_to_be_adjusted, c216_item_price, c1900_owner_company_id
              , c216_local_item_price, c216_created_by, c216_created_date
            )
            VALUES
            (
                s216_part_qty_adj_details.NEXTVAL, v_inv_to_loaner_ma_id, set_swap_dtls.new_pnum
              , 1, v_purchase_amt, v_part_company
              , v_company_cost, v_user, CURRENT_DATE
            ) ;
        --
    END LOOP;
    -- 5. posting for Loaner to inv adjustment
    gm_pkg_ac_ma.gm_save_manual_ledger_posting (v_loaner_to_inv_ma_id) ;
    -- 6. posting for Inv adjustement to Loaner
    gm_pkg_ac_ma.gm_save_manual_ledger_posting (v_inv_to_loaner_ma_id) ;
    -- 7. to updaate the comment to posting done
     UPDATE JPN_SETID_SWAP_TXN_STATUS
    SET status_flag   = 'Reverse Posting Done'
      WHERE NEW_CN_ID = p_cn_id;
    --
    DBMS_OUTPUT.PUT_LINE ('Reverse posting completed -- CN id ' || p_cn_id) ;
END gm_sav_set_swap_correction;
END tmp_gm_pkg_swap_set_dtls;
/