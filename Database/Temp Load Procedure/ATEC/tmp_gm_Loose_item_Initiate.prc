/*
************************************************
* The purpose of this procedure is to Initiate a Set build for the given Set ID
* Ticket: Atec 403 : Japan Loaner Set List Data migration. July 2017
* Author: Gomathi Palani. Verified by Richard K.
************************************************* */
CREATE OR REPLACE
PROCEDURE tmp_gm_loose_item_initiate
AS
    v_user t101_user.c101_user_id%TYPE := '303043';
    v_input_str_item_consign CLOB;
    v_initiate_input_str_IAFG CLOB;
    v_control_input_str_IAFG CLOB;
    v_verify_input_str_IAFG CLOB;
    v_control_str_item_consign CLOB;
    v_part_no t205_part_number.c205_part_number_id%TYPE;
    v_company_id          t1900_company.C1900_COMPANY_ID%TYPE;
    v_plant_id            t5040_plant_master.C5040_PLANT_ID%TYPE;
    v_portal_com_date_fmt VARCHAR2 (20) ;
    v_qty                 NUMBER;
    v_purchase_amt        NUMBER := '0';
    P_ERRMSG              VARCHAR2 (200) ;
    v_date                VARCHAR2 (40) ;
    v_dist_id             NUMBER;
    v_req_source          NUMBER;
    v_request_for         NUMBER;
    v_request_to_dist_id  NUMBER;
    v_request_by_type     NUMBER;
    v_acc_id T704_ACCOUNT.c704_ext_ref_id%TYPE;
    v_lot_number       VARCHAR2 (40) ;
    V_TRIP_PRICE       NUMBER;
    v_reason           NUMBER;
    v_txntype          NUMBER;
    v_wh_type          VARCHAR2 (100) ;
    v_cur_wh           VARCHAR2 (50) ;
    v_txn_type_name    VARCHAR2 (50) ;
    p_message          VARCHAR2 (100) ;
    v_ship_date        VARCHAR2 (100) ;
    dist_id            NUMBER;
    p_req_id           VARCHAR2 (20) ;
    p_out_cn_id        VARCHAR2 (20) ;
    p_source_wh        NUMBER;
    p_target_wh        NUMBER;
    v_costing_type     NUMBER;
    V_TXN_ID           VARCHAR2 (40) ;
    V_CONTROL_NUM      VARCHAR2 (40);
    V_OUT              NUMBER;
    GMNA_Acct_id       t704_account.C704_ACCOUNT_ID%TYPE;
    strTxnType         NUMBER        := '4110';
    strRelVerFl        VARCHAR2 (10) := 'on';
    strLocType         NUMBER        := '93343';
    v_shipid           NUMBER;
    p_shipflag         VARCHAR2 (10) ;
    v_tag_flag         VARCHAR2 (10) ;
    v_tagable_flag     VARCHAR2 (10) ;
    tag_part_number    VARCHAR2 (100) ;
    tag_control_number VARCHAR2 (100) ;
    v_input_str_tag_creation CLOB;
    v_location_id t5052_location_master.C5052_LOCATION_ID%TYPE;
    v_fg_location_id t5052_location_master.C5052_LOCATION_ID%TYPE;
    v_part_price t205_part_number.c205_equity_price%TYPE;
    v_cost t820_costing.c820_purchase_amt%TYPE;
    v_company_cost t820_costing.C820_LOCAL_COMPANY_COST%TYPE;
    v_count      NUMBER := 0;
    v_count_item NUMBER := 0;
    v_pkey number;
    CURSOR cur_item_acc
    IS
        SELECT DISTINCT (JPN.ALLADIN_ACCOUNT_ID) acct_id
           FROM JP_LOANER_INV_STG JPN
          WHERE JPN.C5010_tag_id       IS NULL
            AND JPN.ALLADIN_ACCOUNT_ID IS NOT NULL
            AND JPN.C207_SET_ID        IS NULL
            AND jpn.set_loaded_fl       = 'P'
       ORDER BY ALLADIN_ACCOUNT_ID;
    CURSOR cur_item_initiate
    IS
         SELECT JPN.C205_part_number_id PART_NO, JPN.physical_set_QTY QTY, jpn.lot_number LOT_NUMBER, jpn.pkey pkey
           FROM JP_LOANER_INV_STG JPN
          WHERE JPN.C5010_tag_id      IS NULL
            AND JPN.ALLADIN_ACCOUNT_ID = v_acc_id
            AND JPN.C207_SET_ID       IS NULL
            AND jpn.set_loaded_fl      = 'P'
       ORDER BY C205_part_number_id;
BEGIN
 --   gm_pkg_cor_client_context.gm_sav_client_context ('1022', get_code_name ('10306121'), get_code_name ('105131'),
    -- '3012') ;
     SELECT get_compdtfmt_frm_cntx () INTO v_portal_com_date_fmt FROM dual;
     SELECT get_plantid_frm_cntx INTO v_plant_id FROM dual;
     SELECT get_compid_frm_cntx () INTO v_company_id FROM dual;
    -- Create Location for FG and BL
    BEGIN
         SELECT c5052_location_id
           INTO v_fg_location_id
           FROM t5052_location_master
          WHERE c5052_location_cd = 'JPN_FG_LOCATION'
            AND c1900_company_id  = v_company_id
            AND c5052_void_fl is null
            ;
        --dbms_output.put_line('v_fg_location_id=' || v_fg_location_id || ', v_bl_location_id=' || v_bl_location_id);
    EXCEPTION
    WHEN OTHERS THEN
        dbms_output.put_line ('*********Location error' || ' Error:' || sqlerrm) ;
    END;
    -- IF (P_ACCT_ID  IS NULL AND P_SET_ID IS NULL) THEN
    BEGIN
        -- dbms_output.put_line ('P_SET_ID=' || P_SET_ID || '**P_ACCT_ID'||P_ACCT_ID||'***P_TAG_ID'||P_TAG_ID) ;
        FOR inv_item_acc IN cur_item_acc
        
        LOOP
            v_acc_id             := inv_item_acc.acct_id;
            begin
            FOR inv_item_consign IN cur_item_initiate
            
            LOOP
                v_qty        := inv_item_consign.QTY;
                v_part_no    := inv_item_consign.PART_NO;
                v_lot_number := inv_item_consign.LOT_NUMBER;
                v_pkey 		 := inv_item_consign.pkey;
                --           v_acc_id             := inv_item_consign.acct_id;
                 SELECT C704_ACCOUNT_ID
                   INTO GMNA_Acct_id
                   FROM t704_account
                  WHERE c704_ext_ref_id = v_acc_id;
                  
                 SELECT d_id
                   INTO dist_id
                   FROM v700_territory_mapping_detail
                  WHERE ac_id               = GMNA_Acct_id;
                  
          			INSERT INTO MY_TEMP_KEY_VALUE_loose_item(MY_TEMP_TXN_ID,MY_TEMP_TXN_KEY) values(v_pkey,v_part_no);

                     
                v_input_str_item_consign   := v_input_str_item_consign|| v_part_no||'^'||v_qty||'^'||v_ship_date||'^|';
                
                v_control_str_item_consign := v_control_str_item_consign||v_part_no||','||v_qty||','||v_lot_number||','
                ||v_fg_location_id||','||'TBE'||','||'90800'||'|';
                
                v_count_item                 := v_count_item + 1;
                
                IF (v_count_item             >= 20) THEN
                
                  	  v_input_str_item_consign := v_input_str_item_consign||'~5001^5004|';
                    
                   	  gm_pkg_op_process_prod_request.gm_sav_initiate_item (NULL, CURRENT_DATE, 50618, NULL, 40021,
                  	  dist_id, 50625, V_USER, 4120, NULL, NULL, 15, V_USER, v_input_str_item_consign, 50060, '',
                  	  CURRENT_DATE, p_req_id, p_out_cn_id, NULL) ;
                    
                 	   gm_pkg_cm_shipping_trans.gm_sav_shipping (p_req_id, '50184', '4120', dist_id, '5001', '5004', NULL,
                  	  '0', NULL, v_user, v_shipid, NULL, NULL) ;
                  	  
                    -- controlling the Item consignment
                    	gm_pkg_op_item_control_txn.gm_sav_item_control_main (p_out_cn_id, strTxnType, strRelVerFl,
                    	v_control_str_item_consign, strLocType, V_USER) ;
                    	
                    --  gm_pkg_op_item_control_txn.gm_sav_item_control_main(v_txn_id, v_txntype, 'on',
                    -- v_control_input_str,93343,
                    -- v_user);
                    	gm_pkg_cm_shipping_trans.gm_sav_shipout (p_out_cn_id, '50181', '4120', dist_id, '5001', '5004',
                    	p_out_cn_id|| ':Item', 0, 0, '303043', 0, v_shipid, NULL, p_shipflag) ;
                    	v_count_item               := 0;
                    	v_input_str_item_consign   := NULL;
                    	v_control_str_item_consign := NULL;
                    	v_shipid                   := NULL;
                        
                    	UPDATE t504_consignment
                    	SET C504_LAST_UPDATED_BY = v_user, C504_LAST_UPDATED_DATE = CURRENT_DATE, c504_comments =
                        'JPN Data Migration:Loose Items for Alladin Acc: '||v_acc_id
                      	WHERE c504_consignment_id = p_out_cn_id;
                      
                        UPDATE MY_TEMP_KEY_VALUE_loose_item SET     MY_TEMP_TXN_VALUE= p_out_cn_id  WHERE MY_TEMP_TXN_VALUE IS NULL;
                    	
                      	dbms_output.put_line ('Loose Item Initiated for the Acct ID: '||v_acc_id) ;
                END IF;
            END LOOP;
            
            IF (v_input_str_item_consign IS NOT NULL) THEN
            
                v_input_str_item_consign := v_input_str_item_consign||'~5001^5004|';
                
                gm_pkg_op_process_prod_request.gm_sav_initiate_item (NULL, CURRENT_DATE, 50618, NULL, 40021, dist_id,
                
              					  50625, V_USER, 4120, NULL, NULL, 15, V_USER, v_input_str_item_consign, 50060, '', CURRENT_DATE,
              				      p_req_id, p_out_cn_id, NULL) ;
              				      
                gm_pkg_cm_shipping_trans.gm_sav_shipping (p_req_id, '50184', '4120', dist_id, '5001', '5004', NULL, '0'
                , NULL, v_user, v_shipid, NULL, NULL) ;
                
                -- controlling the Item consignment
                gm_pkg_op_item_control_txn.gm_sav_item_control_main (p_out_cn_id, strTxnType, strRelVerFl,
                v_control_str_item_consign, strLocType, V_USER) ;
                
                --  gm_pkg_op_item_control_txn.gm_sav_item_control_main(v_txn_id, v_txntype, 'on', v_control_input_str,
                -- 93343,
                -- v_user);
                gm_pkg_cm_shipping_trans.gm_sav_shipout (p_out_cn_id, '50181', '4120', dist_id, '5001', '5004',
                p_out_cn_id|| ':Item', 0, 0, '303043', 0, v_shipid, NULL, p_shipflag) ;
                
                 UPDATE t504_consignment
                SET C504_LAST_UPDATED_BY = v_user, C504_LAST_UPDATED_DATE = CURRENT_DATE, c504_comments =
                    'JPN Data Migration:Loose Items for Alladin Acc: '||v_acc_id
                  WHERE c504_consignment_id = p_out_cn_id;
                  
                UPDATE MY_TEMP_KEY_VALUE_loose_item SET     MY_TEMP_TXN_VALUE= p_out_cn_id  WHERE MY_TEMP_TXN_VALUE IS NULL;
                    
                dbms_output.put_line ('Loose Item Initiated for the Acct ID: '||v_acc_id) ;
                v_acc_id                   := NULL;
                v_count_item               := 0;
                v_input_str_item_consign   := NULL;
                v_control_str_item_consign := NULL;
                v_shipid                   := NULL;
            END IF;
                    
        UPDATE JP_LOANER_INV_STG T1 SET set_loaded_fl='C',
				T1.NEW_TXN_ID = (SELECT T2.MY_TEMP_TXN_VALUE FROM MY_TEMP_KEY_VALUE_loose_item T2 WHERE T2.MY_TEMP_TXN_ID = T1.PKEY)
			WHERE T1.PKEY IN (SELECT T2.MY_TEMP_TXN_ID FROM MY_TEMP_KEY_VALUE_loose_item T2 WHERE T2.MY_TEMP_TXN_ID = T1.PKEY)
			AND SET_LOADED_FL='P';
	      
		COMMIT;
	  	   		 	   
		    EXCEPTION WHEN OTHERS then
            DBMS_OUTPUT.PUT_LINE('Loose Item CONSIGNMENT ERROR for part '||v_part_no||SQLERRM);
            ROLLBACK;
   	      end;
   	      
   
		  
    END LOOP;
END;	
END tmp_gm_loose_item_initiate;
/
