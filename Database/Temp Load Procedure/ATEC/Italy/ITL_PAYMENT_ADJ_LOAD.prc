create or replace procedure ITL_PAYMENT_ADJ_LOAD
AS           
     
     v_ord_info_id 	NUMBER;
     v_ord_itm_id 	NUMBER;
     v_cnt  		NUMBER;
     v_total 		NUMBER;
     v_pnum			VARCHAR2(20) :='999.009';
     p_userid 		VARCHAR2(20) :='2205478';
     
                 CURSOR cur_ord              
				 IS
                  select c501_order_id ord_id,DR_CR_AMT price from globus_app.ITALY_INVOICE_LOAD_STG where  DR_CR_AMT<>0;
    

        
   BEGIN
                  
        FOR ord_cur IN cur_ord  
        
        LOOP
                 
         
          select count(1) INTO v_cnt from t502_item_order 
          where c501_order_id=ord_cur.ord_id
          and c205_part_number_id = v_pnum;
          
          IF v_cnt = 0 THEN
          v_ord_info_id:=s500_order_info.nextval;
          v_ord_itm_id:=s502_item_order.nextval;
          
          INSERT INTO t500_order_info (C500_ORDER_INFO_ID,C501_ORDER_ID,C205_PART_NUMBER_ID,C500_ITEM_QTY,C500_ITEM_PRICE,C500_CREATED_BY,C500_CREATED_DATE)
          values (v_ord_info_id,ord_cur.ord_id,v_pnum, 1, ord_cur.price,839135,sysdate);
          
          INSERT INTO t502_item_order(c502_item_order_id, c501_order_id, c205_part_number_id, c502_item_qty, c502_control_number
                                                                   , c502_item_price, c901_type,c500_order_info_id)
          VALUES (v_ord_itm_id, ord_cur.ord_id, v_pnum, 1, null
                                                                   , ord_cur.price, 50300,v_ord_info_id);
           
           
          INSERT INTO t502b_item_order_usage(C502B_ITEM_ORDER_USAGE_ID,C501_ORDER_ID,C205_PART_NUMBER_ID,C502B_ITEM_QTY,C500_ORDER_INFO_ID,C502B_CREATED_BY,C502B_CREATED_DATE)
          VALUES(s502B_ITEM_ORDER_USAGE.nextval,ord_cur.ord_id, v_pnum, 1,v_ord_info_id,839135,sysdate);
          
          SELECT SUM (NVL (TO_NUMBER (c502_item_qty) * TO_NUMBER (c502_item_price), 0))
                                  INTO v_total
                                  FROM t502_item_order
                                WHERE c501_order_id = ord_cur.ord_id
                                AND c502_void_fl IS NULL;
        
          UPDATE t501_order
                                   SET c501_total_cost = NVL (v_total, 0)
                                       , c501_last_updated_by = p_userid
                                       , c501_last_updated_date = SYSDATE
                                WHERE c501_order_id = ord_cur.ord_id;

          END IF;
          
               END LOOP;
    
END ITL_PAYMENT_ADJ_LOAD;
/
