create or replace procedure ITL_PAYMENT_LOAD
AS

CURSOR payment_string
    IS
   SELECT T503.C503_INVOICE_ID||'^'||T503.C503_CUSTOMER_PO||'^'||5011||'^^'||ipl.c508_payment_amt||'^'||2700||'|' as vstring,
ipl.c508_received_date rec_date,T503.C704_ACCOUNT_ID acc_id,T503.C503_INVOICE_ID inv_id,ipl.c508_payment_amt payment_amt
--||',to_date('''||to_char(ipl.c508_received_date,'dd/mm/yyyy')||''',(''dd/mm/yyyy''),'||5011||','||''||','||30301||','||C704_ACCOUNT_ID||','||17280 as vstring
 FROM ITALY_PAYMENT_LOAD_STG IPL, T503_INVOICE T503 
WHERE IPL.C503_INVOICE_ID=T503.C503_INVOICE_ID
AND IPL.LOAD_FL IS NULL;

BEGIN
    GM_PKG_COR_CLIENT_CONTEXT.GM_SAV_CLIENT_CONTEXT('1020',GET_CODE_NAME('10306118'),GET_CODE_NAME('105131'),'3003');
	
	UPDATE ITALY_PAYMENT_LOAD_STG SET C503_INVOICE_ID ='39-'||TO_CHAR(C503_INVOICE_DATE,'YYYY')||'-'||SRC_INVOICE_ID WHERE C503_INVOICE_ID IS NULL;
        
	DBMS_OUTPUT.put_line ('Payment load starttime: '||sysdate);
	
      FOR payment_upd IN payment_string
      LOOP
        BEGIN
         GM_UPDATE_INVOICE_MULTIPLE(payment_upd.vstring,payment_upd.rec_date,5011,'','30301',payment_upd.acc_id,payment_upd.payment_amt);
         
         UPDATE ITALY_PAYMENT_LOAD_STG SET LOAD_FL='Y',LOAD_DATE=SYSDATE WHERE C503_INVOICE_ID=payment_upd.inv_id;
         DBMS_OUTPUT.put_line ('Invoice Id updated: '||payment_upd.inv_id);
        
         Exception
      		 WHEN OTHERS THEN  
				ROLLBACK;
			UPDATE ITALY_PAYMENT_LOAD_STG SET LOAD_FL='N' WHERE C503_INVOICE_ID=payment_upd.inv_id;
		END;	          
        END LOOP;
        COMMIT;
        
        DBMS_OUTPUT.put_line ('Payment load endtime: '||sysdate);

END ITL_PAYMENT_LOAD;
/