create or replace procedure ITL_INVOICE_LOAD
AS

v_err_dtls clob;
v_ord_count NUMBER;
v_dup_count NUMBER;
icount number :=0;
p_userid varchar(20) :='2205478';
v_company_id NUMBER;

    CURSOR customer_po
    IS
       SELECT C501_ORDER_ID ORD_ID,C501_CUSTOMER_PO CPO,C501_CUSTOMER_PO_DATE CPO_DATE,SRC_INVOICE_ID src_inv,C503_INVOICE_DATE src_inv_date FROM ITALY_INVOICE_LOAD_STG WHERE LOAD_FL IS NULL; 
       
    CURSOR invoice_string
    IS
      SELECT  distinct
        t501.c704_account_id acc_id
        ,t704.c704_payment_terms payment_terms_id
        ,t501.c501_customer_po cust_po
        ,CASE WHEN (t501.C501_ORDER_DATE>INL.C503_INVOICE_DATE) THEN t501.C501_ORDER_DATE ELSE INL.C503_INVOICE_DATE END as inv_date
        ,INL.C503_INVOICE_ID inv_id
        ,INL.LOAD_FL load_fl
        from t501_order t501, t704_account t704,ITALY_INVOICE_LOAD_STG INL
        where t704.c704_account_id=t501.c704_account_id
        and inl.c501_order_id=t501.c501_order_id 
        and INL.LOAD_FL IS NULL
        and t501.c1900_company_id=v_company_id 
        and t501.c501_void_fl is null        
        order by t501.c501_customer_po ;
       
       
       CURSOR valid_order
    IS
       SELECT INLS.C501_ORDER_ID ORD_ID,INLS.C501_CUSTOMER_PO CPO,CASE WHEN (t501.C501_ORDER_DATE>INLS.C501_CUSTOMER_PO_DATE) THEN t501.C501_ORDER_DATE ELSE INLS.C501_CUSTOMER_PO_DATE END AS CPO_DATE,INLS.LOAD_FL load_fl,INLS.SRC_INVOICE_ID src_inv 
		FROM ITALY_INVOICE_LOAD_STG INLS, T501_ORDER T501
		WHERE INLS.C501_ORDER_ID=T501.C501_ORDER_ID
		AND INLS.LOAD_FL IS NULL
		AND T501.C1900_COMPANY_ID=1020;

BEGIN

    GM_PKG_COR_CLIENT_CONTEXT.GM_SAV_CLIENT_CONTEXT('1020',GET_CODE_NAME('10306118'),GET_CODE_NAME('105131'),'3003');
    
    SELECT get_compid_frm_cntx() INTO v_company_id   FROM dual;

	UPDATE ITALY_INVOICE_LOAD_STG SET C503_INVOICE_ID ='39-'||TO_CHAR(C503_INVOICE_DATE,'YYYY')||'-'||SRC_INVOICE_ID WHERE C503_INVOICE_ID IS NULL;
	
	UPDATE ITALY_INVOICE_LOAD_STG SET ERR_DTLS='Free of chareg Customer PO or Invoice id or Customer PO blank ',LOAD_FL='N' 
	WHERE (C501_CUSTOMER_PO='0' OR trim(C501_CUSTOMER_PO)='FREE OF CHARGE' OR SRC_INVOICE_ID='0' OR SRC_INVOICE_ID IS NULL);
    
     FOR validation_ord IN customer_po
     LOOP
      --------Validation
      -- order validation
        SELECT count(1) into v_ord_count
	   		  FROM T501_ORDER
	   		  WHERE C501_ORDER_ID=validation_ord.ORD_ID
	   		  AND C501_VOID_FL IS NULL
              AND C1900_COMPANY_ID=v_company_id;
	          	   		
	          IF (v_ord_count=0) THEN
	          
		          v_err_dtls :='Order not exist in spineIT : '||validation_ord.ORD_ID;
               
                UPDATE ITALY_INVOICE_LOAD_STG SET ERR_DTLS=v_err_dtls,LOAD_FL='N' WHERE C501_ORDER_ID=validation_ord.ORD_ID ;   
                v_ord_count :=0;
              END IF;  
            
              
              SELECT count(1) into v_dup_count
	   		  FROM ITALY_INVOICE_LOAD_STG
	   		  WHERE C501_ORDER_ID=validation_ord.ORD_ID
	   		  AND LOAD_FL IS NULL;
	          	   		
	          IF (v_dup_count>1) THEN
	         
		      v_err_dtls :='One Order have more than one Invoice or Duplicate order : '||validation_ord.ORD_ID;
               
                UPDATE ITALY_INVOICE_LOAD_STG SET ERR_DTLS=v_err_dtls,LOAD_FL='N' WHERE C501_ORDER_ID=validation_ord.ORD_ID ;  
                v_dup_count :=0;
              END IF; 
        /*    -- invoice id validation
            IF (validation_ord.src_inv IS NULL OR validation_ord.src_inv='0')
            THEN
            v_err_dtls :='Invoice Id is blank: '||validation_ord.src_inv;
            UPDATE ITALY_INVOICE_LOAD_STG SET ERR_DTLS=ERR_DTLS||v_err_dtls,LOAD_FL='N' WHERE C501_ORDER_ID=validation_ord.ORD_ID; 
            END IF;
			
            -- customer po validation
            IF (validation_ord.CPO IS NULL OR validation_ord.CPO='0' OR validation_ord.CPO='FREE OF CHARGE')
            THEN
            v_err_dtls :='Customer PO is blank : '||validation_ord.CPO;
            UPDATE ITALY_INVOICE_LOAD_STG SET ERR_DTLS=ERR_DTLS||v_err_dtls,LOAD_FL='N' WHERE C501_ORDER_ID=validation_ord.ORD_ID; 
            END IF;*/
            
            -- invoice date validation
            IF validation_ord.src_inv_date IS NULL
            THEN
            v_err_dtls :='Invoice date is null : '||validation_ord.src_inv_date;
            UPDATE ITALY_INVOICE_LOAD_STG SET ERR_DTLS=ERR_DTLS||v_err_dtls,LOAD_FL='N' WHERE C501_ORDER_ID=validation_ord.ORD_ID; 
            END IF;
            
			 COMMIT;	
    END LOOP;
    ---------
      DBMS_OUTPUT.put_line ('Customer PO update starttime: '||to_char(sysdate, 'dd-mm-yyyy hh24:mi:ss'));
    
      FOR update_cpo IN valid_order
      LOOP
            IF update_cpo.load_fl IS NULL
            THEN
             
			 UPDATE t501_order t501  SET C501_CUSTOMER_PO=update_cpo.CPO,C501_CUSTOMER_PO_DATE=update_cpo.CPO_DATE,C501_LAST_UPDATED_DATE=SYSDATE,C501_LAST_UPDATED_BY=p_userid
             where c1900_company_id=v_company_id and C501_ORDER_ID=update_cpo.ORD_ID;
             
			 icount := icount + 1;
			 IF (icount = 1000)
			 THEN
				COMMIT;
			 END IF;
			 
            END IF;
      END LOOP;
	  
	  COMMIT;
	  
      DBMS_OUTPUT.put_line ('Count of DO updated: '||icount);
      DBMS_OUTPUT.put_line ('Customer PO update endtime and Inv gen starttime: '||to_char(sysdate, 'dd-mm-yyyy hh24:mi:ss'));
      

    
      FOR inv_call IN invoice_string
      LOOP
      IF inv_call.load_fl IS NULL
        THEN
          BEGIN
          
		  --gm_sav_single_invoice_temp (p_po,p_accid,p_payterms,p_invdate,p_userid,p_action,p_invtype,p_inv_source,'','',p_inv_id);
          
		  gm_sav_single_invoice_temp(inv_call.cust_po,inv_call.acc_id,inv_call.payment_terms_id,inv_call.inv_date,p_userid,'ADD','50200','50255','','',inv_call.inv_id);
        
             UPDATE ITALY_INVOICE_LOAD_STG SET LOAD_FL='Y',LOAD_DATE=SYSDATE WHERE C501_CUSTOMER_PO=inv_call.cust_po AND C503_INVOICE_ID=inv_call.inv_id;

			 DBMS_OUTPUT.put_line ('cust_po: '||inv_call.cust_po||' inv_id: '||inv_call.inv_id); 
			COMMIT;
        Exception
      		 WHEN OTHERS THEN  
				ROLLBACK;
			 v_err_dtls := NULL;
             v_err_dtls :='Invoice generation Processing failed: Error message: '||SQLERRM;
             UPDATE ITALY_INVOICE_LOAD_STG SET ERR_DTLS=v_err_dtls,LOAD_FL='N' WHERE C501_CUSTOMER_PO=inv_call.cust_po AND C503_INVOICE_ID=inv_call.inv_id; 
			 COMMIT;
			 

			 END;
        END IF; 
       END LOOP;
       DBMS_OUTPUT.put_line ('Invoice Generation endtime: '||to_char(sysdate, 'dd-mm-yyyy hh24:mi:ss'));
	   
	   COMMIT;
      
END  ITL_INVOICE_LOAD;
/