--To Remove Public Account VAT and Tax Rate
create or replace procedure ITL_VAT_REMOVE_PUBLIC
AS

 V_A_id         number;
 p_userid 		VARCHAR2(20) :='2205478';
 
  CURSOR ord_details
  IS
       Select Distinct Ity.C501_Order_Id ord_id,C704_Account_Id Acc_id from  globus_app.ITALY_INVOICE_LOAD_STG ity , t501_order t501 where ity.c501_order_id=t501.c501_order_id and c1900_company_id=1020;

	   BEGIN
  FOR tax_upd  IN ord_details
  LOOP 
  BEGIN
      select nvl(c704a_attribute_value,0) into V_A_id from T704A_ACCOUNT_ATTRIBUTE where C901_ATTRIBUTE_TYPE = 5504 AND C704_ACCOUNT_ID =tax_upd.acc_id and C704a_Void_Fl Is Null;
      EXCEPTION
     WHEN OTHERS THEN
     V_A_id :=0;
      DBMS_OUTPUT.put_line('Error Occurred while execution: ' ||' ACC_ID='||  tax_upd.acc_id);
      END;
	  
        if(V_A_id=5501) then --5501-Public
          UPDATE T502_ITEM_ORDER SET C502_VAT_RATE=NULL, C502_TAX_AMT=NULL where C501_ORDER_ID=tax_upd.ord_id;
          update  t501_order set C501_TAX_TOTAL_COST=null, c501_last_updated_by = p_userid, c501_last_updated_date = SYSDATE where C501_ORDER_ID=tax_upd.ord_id;
 END IF;  
 END LOOP;
End ITL_VAT_REMOVE_PUBLIC;
  /