-- Procedure to insert usage lot details 
CREATE OR REPLACE PROCEDURE tmp_insert_usage_lot_details(
p_order_id		    IN 	  	t502b_item_order_usage.C501_ORDER_ID%TYPE,
p_part_num          IN   	t502b_item_order_usage.C205_PART_NUMBER_ID%TYPE,
p_item_qty          IN      t502b_item_order_usage.C502B_ITEM_QTY%TYPE,
p_order_info_id     IN      t502b_item_order_usage.C500_ORDER_INFO_ID%TYPE,
p_trans_id          IN      t502b_item_order_usage.c502b_trans_id%TYPE,
p_user_id           IN      t502b_item_order_usage.C502B_CREATED_BY%TYPE)
 AS
  
 BEGIN

		INSERT
		INTO t502b_item_order_usage
		  (
		    C502B_ITEM_ORDER_USAGE_ID,
		    C501_ORDER_ID,
		    C205_PART_NUMBER_ID,
		    C502B_ITEM_QTY,
		    C500_ORDER_INFO_ID,
		    c502b_trans_id,
		    C502B_CREATED_BY,
		    C502B_CREATED_DATE
		  )
		  VALUES
		  (
		    s502B_ITEM_ORDER_USAGE.nextval,
		    p_order_id,
		    p_part_num,
		    p_item_qty,
		    p_order_info_id,
		    p_trans_id,
		    p_user_id,
		    sysdate
		  );	
	 	 
END tmp_insert_usage_lot_details;
/


 
 
        