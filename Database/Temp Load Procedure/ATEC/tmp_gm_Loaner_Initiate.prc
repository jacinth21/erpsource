/*
 ************************************************
 * The purpose of this procedure is to Initiate a Loaner Set for the given Set ID
 * Ticket: Atec 403 : Japan Loaner Set List Data migration. July 2017 
 * Author: Gomathi Palani. Verified by Richard K.
************************************************* */

CREATE OR REPLACE
PROCEDURE tmp_gm_Loaner_Initiate (
        p_set_id    IN t208_set_details.C207_SET_ID%TYPE,
        p_acc_id    IN T704_ACCOUNT.C704_EXT_REF_ID%TYPE,
        p_tag_id    IN t5010_tag.C5010_TAG_ID%TYPE)
AS
    -- Loaner request and Approval
    p_req_id         VARCHAR2 (20) ;
    p_reqdet_litpnum VARCHAR2 (20) ;
    p_req_det_id     VARCHAR2 (20) ;
    v_req_det_id     VARCHAR2 (20) ;
    p_req_dtl_id     VARCHAR2 (20) ;
    dist_id v700_territory_mapping_detail.D_ID%TYPE;
    rep_id v700_territory_mapping_detail.REP_ID%TYPE;
    v_user NUMBER := '303043';
    v_input_str_LN_Initiate CLOB;
    v_request_for NUMBER := '4127';
    v_ship_to     NUMBER := '4120';
    v_request_by  NUMBER := '50626';
    V_LN_CONSIGN_ID T504_CONSIGNMENT.C504_CONSIGNMENT_ID%TYPE;
    GMNA_Acct_id t704_account.C704_ACCOUNT_ID%TYPE;
    p_loaner_dt VARCHAR2(40);
    v_dateformat varchar2(100);
    v_company_id t1900_company.C1900_COMPANY_ID%TYPE;
    v_plant_id t5040_plant_master.C5040_PLANT_ID%TYPE;
    V_SURGEY_DATE VARCHAR2(40);
    v_shipid number;
    p_shipflag varchar2(10);
    v_input_str_pending_pick CLOB;
    v_status_fl number;
    status_count number;
    
   
BEGIN
	    
    -- Setting Japan Context
-- gm_pkg_cor_client_context.gm_sav_client_context ('1022', get_code_name ('10306121'), get_code_name ('105131'), '3012');
   select get_compdtfmt_frm_cntx() into v_dateformat from dual;

    
     select C704_ACCOUNT_ID INTO GMNA_Acct_id from t704_account
     where c704_ext_ref_id=p_acc_id;
    
     SELECT d_id, rep_id
       INTO dist_id, rep_id
       FROM v700_territory_mapping_detail
      WHERE ac_id= GMNA_Acct_id;
      
  --  dbms_output.put_line('dist_id and rep_id'||dist_id||rep_id);
      p_loaner_dt :=to_char(CURRENT_DATE,get_compdtfmt_frm_cntx()); 
--    V_SURGEY_DATE :=  TO_DATE(CURRENT_DATE,v_dateformat);
      v_input_str_LN_Initiate := p_set_id||'^1'||'^'||p_loaner_dt||'|';
  --  dbms_output.put_line('v_input_str_LN_Initiate'||v_input_str_LN_Initiate);
   -- dbms_output.put_line('v_dateformat'||v_dateformat);
   -- dbms_output.put_line('V_SURGEY_DATE'||V_SURGEY_DATE);

     -- dbms_output.put_line('Loaner Initiation process started for Set ID: '||p_set_id||'Tag ID:'||p_tag_id||' Acct ID:'||p_acc_id);

      -- Loaner Initiate
    gm_pkg_op_product_requests.gm_sav_product_requests (CURRENT_DATE, v_request_for, dist_id,
    v_request_by, v_user, v_ship_to, NULL, v_user, v_input_str_LN_Initiate, rep_id, GMNA_Acct_id, p_req_id, NULL, NULL) ;
    
     -- dbms_output.put_line('Loaner Initiated. Request Id is:'||p_req_id);
     
         -- Loaner Approval
     SELECT C526_PRODUCT_REQUEST_DETAIL_ID
       INTO v_req_det_id
       FROM t526_product_request_detail
      WHERE c525_product_request_id = p_req_id;
      
     -- dbms_output.put_line('Product Rqst detail id is : '||v_req_det_id);
           
      gm_pkg_cm_shipping_trans.gm_sav_shipping(v_req_det_id
            ,'50182'
            ,'4120'
            ,dist_id
            ,'5001'
            ,'5004'
            ,null
            ,'0'
            ,null
            ,v_user
            ,v_shipid
            ,null
            ,null
            );
  -- dbms_output.put_line('After inseting in Shipping'||v_shipid);


/*BEGIN
     SELECT C5010_LAST_UPDATED_TRANS_ID
 	 INTO V_LN_CONSIGN_ID FROM t5010_tag
 	 WHERE C5010_TAG_ID=p_tag_id;
 	 
 	 Exception
 	  WHEN OTHERS THEN 
 	 V_LN_CONSIGN_ID:=NULL;
 	END;
 	     dbms_output.put_line('V_LN_CONSIGN_ID'||V_LN_CONSIGN_ID);*/


 	-- Take the CN id from the previous procedure
 	 SELECT  DISTINCT(NEW_TXN_ID) INTO V_LN_CONSIGN_ID FROM JP_LOANER_INV_STG
     where c207_set_id=p_set_id
     and c5010_tag_id=p_tag_id;
     dbms_output.put_line('NEW_TXN_ID is:'||V_LN_CONSIGN_ID);
   

	SELECT c526_status_fl 
	INTO v_status_fl 
	FROM t526_product_request_detail 
	WHERE c526_product_request_detail_id=v_req_det_id;
			       
	
     	 
    gm_pkg_sm_loaner_allocation.gm_sav_loaner_req_appv (v_req_det_id, v_user, '10', p_req_dtl_id, p_reqdet_litpnum,p_req_det_id) ;
    
    SELECT c526_status_fl 
	INTO v_status_fl 
	FROM t526_product_request_detail 
	WHERE c526_product_request_detail_id=v_req_det_id;
	
    

    
    -- dbms_output.put_line('Loaner Allocated REQUEST DETAILS ID IS'||p_req_det_id);
    -- dbms_output.put_line('p_reqdet_litpnum'||p_reqdet_litpnum);
    -- dbms_output.put_line('p_req_dtl_id'||p_req_dtl_id);

     -- dbms_output.put_line('p_req_det_id'||p_req_det_id);
     
    
    
   

    v_input_str_pending_pick :=V_LN_CONSIGN_ID||'^'||null||'^'||p_tag_id||'|';
    gm_pkg_op_set_pick_txn.gm_sav_set_pick_batch(v_input_str_pending_pick, v_user);
     

    --  gm_pkg_op_loaner_set_txn.gm_sav_loaner_status (V_LN_CONSIGN_ID, 'PICK', v_user);
     
  --  dbms_output.put_line('Shipping Id: '||Gm_pkg_cm_shipping_info.get_shipping_id(v_req_det_id,'50182','fetch'));
   
     gm_pkg_cm_shipping_trans.gm_sav_shipout(
            V_LN_CONSIGN_ID --Ref id
           	,'50182' -- Source : loaners
           	,'4120' -- Ship to
           	,dist_id -- Shipto ID DIst, Sales rep
           	,'5001' -- Carrier
           	,'5004'  -- Delivery mode
           	,V_LN_CONSIGN_ID||':'||p_tag_id -- Tracking number
           	,0 -- address idC
           	,0 -- Frieght AMount
           	,v_user -- User id
           	,0
           	,v_shipid -- Out Ship Id
           	,NULL
           	,p_shipflag
           	);    
    --    dbms_output.put_line('CN : '||V_LN_CONSIGN_ID||' is Shipped out. Shipping id is: '||v_shipid); 
     
    
 -- Below update statements are to void the randomly allocated set and allocate the set which was in the hospital
 -- This scenario is applicable only for the Data load migration.
 -- Check if the same CN is assigned, if yes, no need to update
 
 /*    -- Add the company check
    UPDATE t504_consignment 
    SET c701_distributor_id=null, c704_account_id=null, c504_last_updated_by=v_user, c504_last_updated_date=CURRENT_DATE
    WHERE c504_consignment_id 
    IN (SELECT c504_consignment_id FROM t504a_loaner_transaction 
    WHERE c526_product_request_detail_id = v_req_det_id
     );
     
    UPDATE t504a_consignment_loaner
    SET c504a_status_fl          = '0' , c504a_last_updated_by=v_user, c504a_last_updated_date=CURRENT_DATE
     WHERE c504_consignment_id IN
    (
    SELECT c504_consignment_id
    FROM t504a_loaner_transaction
    WHERE c526_product_request_detail_id = v_req_det_id
    );
    
    
    UPDATE t504a_loaner_transaction
    SET c504_consignment_id          = V_LN_CONSIGN_ID,  c504a_last_updated_by=v_user, c504a_last_updated_date=CURRENT_DATE
    WHERE c504_consignment_id IN
    (
    SELECT c504_consignment_id
    FROM t504a_loaner_transaction
    WHERE c526_product_request_detail_id = v_req_det_id
       ) ;
       
                 
       
    UPDATE t504_consignment
    SET c701_distributor_id=dist_id, c704_account_id=GMNA_Acct_id, c504_last_updated_by=v_user, c504_last_updated_date=CURRENT_DATE
    WHERE c504_consignment_id=V_LN_CONSIGN_ID;
              
          
       
   -- UPDATE t504a_consignment_loaner
  --  SET c504a_status_fl          = '20', c504a_last_updated_by=v_user, c504a_last_updated_date=CURRENT_DATE
  --  WHERE c504_consignment_id=V_LN_CONSIGN_ID;
    
            UPDATE t504a_consignment_loaner
   SET c504a_status_fl          = '20', c504a_last_updated_by=v_user, c504a_last_updated_date=CURRENT_DATE
   WHERE c504_consignment_id=V_LN_CONSIGN_ID;
*/
     	


END tmp_gm_Loaner_Initiate;
/