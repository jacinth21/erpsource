/*
 * Purpose: Procedure to invoke the IABL for the Loaner set list data load migraion
 * Atec 403 Data load Migration - API Japan
 * July 2017. Author gpalani, Reviewed by Richard K
 */
CREATE OR REPLACE PROCEDURE tmp_gm_iabl(
        p_set_id IN t208_set_details.C207_SET_ID%TYPE,
        p_tag_id IN t5010_tag.C5010_TAG_ID%TYPE)

AS
  v_user t101_user.c101_user_id%TYPE		:='303043';
  v_initiate_input_str CLOB;
  v_initiate_input_str_set_build CLOB;
  v_control_input_str CLOB;
  v_verify_input_str CLOB;
  v_part_number t205_part_number.c205_part_number_id%TYPE;
  v_part_no t205_part_number.c205_part_number_id%TYPE;
  v_part_price t205_part_number.c205_equity_price%TYPE;
  v_txn_id t504_consignment.c504_consignment_id%TYPE;
  v_cost t820_costing.c820_purchase_amt%TYPE;
  v_tag_flag VARCHAR2 (2) ;
  v_purchase_amt t820_costing.c820_purchase_amt%TYPE;
  v_company_cost  t820_costing.C820_LOCAL_COMPANY_COST%TYPE;
  p_message varchar2(20);

  tag_part_number t205_part_number.c205_part_number_id%TYPE;
  tag_control_number VARCHAR2(40);
  v_input_str_tag_creation CLOB;
  
 
  v_reason NUMBER;
  v_txntype NUMBER;
  v_wh_type VARCHAR2(100);
  v_cur_wh  VARCHAR2(50); 
  v_txn_type_name varchar2(50);
  
  p_qty NUMBER;
  p_source_wh NUMBER;
  p_target_wh NUMBER;
  v_out VARCHAR2(100);
  v_costing_id NUMBER;
  v_company_id NUMBER;
  v_plant_id NUMBER;
  v_portal_com_date_fmt VARCHAR2 (20) ;
  v_bl_location_id t5052_location_master.C5052_LOCATION_ID%TYPE;
  V_TRIP_PRICE NUMBER;
  v_qty NUMBER;
  v_control_num VARCHAR2(40);
  v_costing_type NUMBER;
  P_OUT_REQUEST_ID VARCHAR2 (20) ;
  P_OUT_CONSIGN_ID VARCHAR2 (20) ;
  v_count number :='0';
 
CURSOR cur_set IS

	  SELECT JPN.c205_part_number_id PART_NO, JPN.LOT_NUMBER CONTROL_NUMBER,
	  JPN.physical_set_qty QTY
	  FROM JP_LOANER_INV_STG JPN
	  WHERE JPN.c207_SET_ID=p_set_id
	  AND JPN.c5010_tag_id=p_tag_id;
 
BEGIN

-- 105131 -- dd/mm/yyyy
  SELECT get_compdtfmt_frm_cntx () INTO v_portal_com_date_fmt FROM dual;
  SELECT get_plantid_frm_cntx  INTO v_plant_id FROM dual;
  SELECT get_compid_frm_cntx() INTO v_company_id FROM dual;


  -- Create Location for BL
  
  BEGIN
    SELECT c5052_location_id INTO v_bl_location_id
    FROM t5052_location_master WHERE c5052_location_cd='JPN_BL_LOCATION'and c1900_company_id= v_company_id and c5052_void_fl is null;
    
  
  EXCEPTION WHEN OTHERS THEN
    dbms_output.put_line('*********Location error' || ' Error:' || sqlerrm);
  END;
  
 FOR  inv_set IN cur_set
  LOOP
   BEGIN
      v_part_no :=inv_set.part_no;
      v_qty := inv_set.qty;
      v_control_num :=inv_set.control_number;
      v_txn_type_name:='INVAD-BULK';
      v_txntype:= '400080';
      p_target_wh:='90814';
      p_source_wh:='90814';
      v_cur_WH := 'BL';
      v_wh_type:='90814';
      v_costing_type  :='4900';
      v_reason :='90814';
    
    BEGIN
     select COST, company_cost INTO v_purchase_amt, v_company_cost 
     from jpn_dm_inventory 
     where part=v_part_no 
     and warehouse='FG';     
       

    Exception
    WHEN NO_DATA_FOUND THEN
    v_purchase_amt:='0';
     v_company_cost :='0';
 --   dbms_output.put_line('v_purchase_amt=' || v_purchase_amt);

   END;
   
   BEGIN
   SELECT GET_PART_PRICE(v_part_no,'E') INTO V_TRIP_PRICE
    FROM DUAL;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
 --   dbms_output.put_line('V_TRIP_PRICE=' || V_TRIP_PRICE);

    V_TRIP_PRICE :=0;
END;
 --   dbms_output.put_line('V_TRIP_PRICE=' || V_TRIP_PRICE);

  
   -- Get Inventory cost or insert if not available
      SELECT get_inventory_cogs_value(v_part_no)  INTO v_cost FROM dual;
    --  dbms_output.put_line('v_cost is: '||v_cost);
     IF v_cost IS NULL THEN
        
       INSERT INTO t820_costing (
           c820_costing_id
          ,c205_part_number_id
          ,c901_costing_type
          ,c820_part_received_dt
          ,c820_qty_received
          ,c820_purchase_amt
          ,c820_qty_on_hand       
          ,c901_status
          ,c820_delete_fl
          ,c820_created_by
          ,c820_created_date
          ,c1900_company_id
          ,c5040_plant_id
          ,C820_LOCAL_COMPANY_COST
          ,C1900_OWNER_COMPANY_ID
          ,c901_owner_currency
          ,C901_LOCAL_COMPANY_CURRENCY
          
        ) 
        VALUES 
        ( s820_costing.nextval
          ,v_part_no
          ,v_costing_type
          ,CURRENT_DATE
          ,0
          ,v_purchase_amt
          ,0
          ,'4801'
          ,'N'
          ,'2277820'
          ,CURRENT_DATE
          ,v_company_id        
          ,v_plant_id
          ,v_company_cost
          ,'1018'
          ,'1'
          ,'4000917'
        );
          
        -- Build input string 
        END IF;
        v_initiate_input_str := v_initiate_input_str || v_part_no || ',' || v_qty || ',,,' || V_TRIP_PRICE || '|';         
        v_control_input_str := v_control_input_str || v_part_no || ',' || v_qty || ',' || v_control_num || ',,' || V_TRIP_PRICE || '|';
       -- v_verify_input_str := v_verify_input_str || v_part_no || ',' || v_qty || ',' || v_control_num || ',,' || p_target_wh || '|';
       v_verify_input_str := NULL;
       
       
      IF v_txn_id IS NULL THEN
         SELECT get_next_consign_id(v_txn_type_name) 
         INTO v_txn_id
         FROM dual;
      END IF;

   --    dbms_output.put_line('v_txntype=' || v_txntype || ', p_target_wh=' || p_target_wh || ', p_source_wh=' || p_source_wh);
   
      gm_pkg_op_inv_field_sales_tran.gm_sav_fs_inv_loc_part_details (v_bl_location_id,v_txn_id, v_part_no,v_qty,'400080','4301',null,current_date, v_user);    
     END;
     
v_count :=v_count+1;

		if(v_count>20) then 
			
				
		  -- IABL initiate 
		          gm_save_inhouse_item_consign(v_txn_id,v_txntype , v_reason ,0,'','IABL created for the Set ID:'||p_set_id||':Tag ID:'||p_tag_id,v_user,v_initiate_input_str,v_out);
		    
		         
		
		
		  -- IABL Control 
		        GM_LS_UPD_INLOANERCSG(v_txn_id,v_txntype,'0','0',NULL,'Japan Set Data Load',v_user,v_control_input_str,null,'ReleaseControl',p_message);
		        --  gm_pkg_op_item_control_txn.gm_sav_item_control_main(v_txn_id, v_txntype, 'on', v_control_input_str,93343,v_user);
		    
		          
		
		  -- IABL Verify
		  
		       -- gm_pkg_op_item_control_txn.gm_sav_item_verify_main(v_txn_id,v_txntype,v_verify_input_str,93345,v_user);
		      --  dbms_output.put_line('Before Verification');
		
		        gm_ls_upd_inloanercsg(v_txn_id,v_txntype,'0','0',NULL,'Japan Set Data load',v_user,v_verify_input_str,null,'Verify',p_message);
		        dbms_output.put_line('IABL Txn=' || v_txn_id || ' Verified');
        
		   	   /*  update t820_costing set C820_delete_fl='Y', C820_LAST_UPDATED_BY='2277820',c901_status='4802', C820_LAST_UPDATED_DATE=CURRENT_DATE, c1900_company_id='', c5040_plant_id=''
		        WHERE C820_CREATED_BY='2277820'
		        and c1900_company_id='1022'; */
		         
		        UPDATE t412_inhouse_transactions SET c412_comments='IABL Inventory Update for JPN Set Building' , c412_last_updated_date=CURRENT_DATE, c412_last_updated_by=v_user
			 where c412_inhouse_trans_id=v_txn_id;
		        
		        
		        v_txn_id := NULL;
		        v_count :=0;
		        v_initiate_input_str :=null;
		        v_control_input_str :=null;
END IF;

  END LOOP;
-- Get Next IABL ID based on lot warehouse
     --     dbms_output.put_line('v_initiate_input_str=' ||v_initiate_input_str);
     --     dbms_output.put_line('v_control_input_str=' ||v_control_input_str);
     --     dbms_output.put_line('v_verify_input_str=' ||v_verify_input_str);

   if (v_initiate_input_str is not null) then

  
	   -- IABL initiate 
	          gm_save_inhouse_item_consign(v_txn_id,v_txntype , v_reason ,0,'','IABL created for the Set ID:'||p_set_id||':Tag ID:'||p_tag_id,v_user,v_initiate_input_str,v_out);
	      
	         
	
	
	  -- IABL Control 
	        GM_LS_UPD_INLOANERCSG(v_txn_id,v_txntype,'0','0',NULL,'Japan Set Data Load',v_user,v_control_input_str,null,'ReleaseControl',p_message);
	        --  gm_pkg_op_item_control_txn.gm_sav_item_control_main(v_txn_id, v_txntype, 'on', v_control_input_str,93343,v_user);
	      
	          
	
	  -- IABL Verify
	  
	       -- gm_pkg_op_item_control_txn.gm_sav_item_verify_main(v_txn_id,v_txntype,v_verify_input_str,93345,v_user);
	      
	
	        gm_ls_upd_inloanercsg(v_txn_id,v_txntype,'0','0',NULL,'Japan Set Data load',v_user,v_verify_input_str,null,'Verify',p_message);
	        dbms_output.put_line('IABL Txn=' || v_txn_id || ' Verified');
	        
	      /*  update t820_costing set C820_delete_fl='Y', C820_LAST_UPDATED_BY='2277820',c901_status='4802', C820_LAST_UPDATED_DATE=CURRENT_DATE, c1900_company_id='', c5040_plant_id=''
	        WHERE C820_CREATED_BY='2277820'
	        and c1900_company_id='1022'; */
	        
	         UPDATE t412_inhouse_transactions SET c412_comments='IABL Inventory Update for JPN Set Building' , c412_last_updated_date=CURRENT_DATE, c412_last_updated_by=v_user
			 where c412_inhouse_trans_id=v_txn_id;
			 
	        v_txn_id := NULL;
	        v_count :=0;
	        v_initiate_input_str :=null;
		    v_control_input_str :=null;
		    
	END IF;

END tmp_gm_iabl;    
/

