/*
************************************************
* The purpose of this procedure is to Initiate a Set build for the given Set ID
* Ticket: Atec 403 : Japan Loaner Set List Data migration. July 2017
* Author: Gomathi Palani. Verified by Richard K. 
************************************************* */
CREATE OR REPLACE
PROCEDURE tmp_gm_loose_item_iafg_ins
AS
    v_user t101_user.c101_user_id%TYPE := '303043';
    v_input_str_item_consign CLOB;
    v_initiate_input_str_IAFG CLOB;
    v_control_input_str_IAFG CLOB;
    v_verify_input_str_IAFG CLOB;
    v_control_str_item_consign CLOB;
    v_part_no t205_part_number.c205_part_number_id%TYPE;
    v_company_id          NUMBER;
    v_plant_id            NUMBER;
    v_portal_com_date_fmt VARCHAR2 (20) ;
    v_qty                 NUMBER;
    v_purchase_amt        NUMBER := '0';
    P_ERRMSG              VARCHAR2 (200) ;
    v_date                VARCHAR2 (20) ;
    v_dist_id             NUMBER;
    v_req_source          NUMBER;
    v_request_for         NUMBER;
    v_request_to_dist_id  NUMBER;
    v_request_by_type     NUMBER;
    v_acc_id T704_ACCOUNT.C704_ACCOUNT_ID%TYPE;
    v_lot_number    VARCHAR2 (40) ;
    V_TRIP_PRICE    NUMBER;
    v_reason        NUMBER;
    v_txntype       NUMBER;
    v_wh_type       VARCHAR2 (100) ;
    v_cur_wh        VARCHAR2 (50) ;
    v_txn_type_name VARCHAR2 (50) ;
    p_message       VARCHAR2 (100) ;
    
    dist_id        NUMBER;
    p_req_id       VARCHAR2 (20) ;
    p_out_cn_id    VARCHAR2 (20) ;
    p_source_wh    NUMBER;
    p_target_wh    NUMBER;
    v_costing_type NUMBER;
    V_TXN_ID       VARCHAR2 (20) ;
    V_CONTROL_NUM  NUMBER;
    V_OUT          NUMBER;
    GMNA_Acct_id   VARCHAR2 (200) ;
    strTxnType		NUMBER :='4110';
    strRelVerFl varchar2(10) :='on';
    strLocType number :='93343';
    v_shipid number;
    p_shipflag VARCHAR2(10);
    v_tag_flag varchar2(10);
    v_tagable_flag varchar2(10);
    tag_part_number varchar2(100);
    tag_control_number varchar2(100);
    v_input_str_tag_creation clob;
    v_location_id t5052_location_master.C5052_LOCATION_ID%TYPE;
    v_fg_location_id t5052_location_master.C5052_LOCATION_ID%TYPE;
    v_part_price t205_part_number.c205_equity_price%TYPE;
    v_cost t820_costing.c820_purchase_amt%TYPE;
    v_company_cost  t820_costing.C820_LOCAL_COMPANY_COST%TYPE;
    v_count number := '0';
    v_count_item NUMBER :='0';
    v_pkey number;
    CURSOR cur_item_iafg
    IS
         SELECT JPN.C205_part_number_id PART_NO, JPN.physical_set_QTY QTY, jpn.lot_number LOT_NUMBER, jpn.pkey pkey
          
           FROM JP_LOANER_INV_STG JPN
          WHERE JPN.C5010_tag_id is null
           -- AND JPN.ALLADIN_ACCOUNT_ID IS NULL
           AND JPN.C207_SET_ID IS NULL
           and jpn.set_loaded_fl='L'
       --    and jpn.C205_part_number_id='47000-065-045'
       ORDER BY C205_part_number_id;
       

BEGIN
	-- gm_pkg_cor_client_context.gm_sav_client_context ('1022', get_code_name ('10306121'), get_code_name ('105131'), '3012');
    SELECT get_compdtfmt_frm_cntx () INTO v_portal_com_date_fmt FROM dual;
    SELECT get_plantid_frm_cntx  INTO v_plant_id FROM dual;
    SELECT get_compid_frm_cntx() INTO v_company_id FROM dual;
    
    
     -- Create Location for FG and BL
  BEGIN
    SELECT c5052_location_id INTO v_fg_location_id
    FROM t5052_location_master WHERE c5052_location_cd='JPN_FG_INS_LOCATION' and c1900_company_id= v_company_id AND c5052_void_fl is null;
    
    
    --dbms_output.put_line('v_fg_location_id=' || v_fg_location_id || ', v_bl_location_id=' || v_bl_location_id);
  EXCEPTION WHEN OTHERS THEN
    dbms_output.put_line('*********Location error' || ' Error:' || sqlerrm);
  END;
    -- IF (P_ACCT_ID  IS NULL AND P_SET_ID IS NULL) THEN
        FOR inv_item IN cur_item_iafg
        LOOP
            BEGIN
	          
	       --     dbms_output.put_line ('P_SET_ID=' || P_SET_ID || '**P_ACCT_ID'||P_ACCT_ID||'***P_TAG_ID'||P_TAG_ID) ;
                v_qty     := inv_item.QTY;
                v_part_no := inv_item.PART_NO;
                -- v_control_num :=inv_set.CONTROL_NUMBER;
                v_acc_id        := null;
                v_lot_number    := inv_item.LOT_NUMBER;
                v_txn_type_name := 'InvAdj-Shelf';
                v_txntype       := '400068';
                p_target_wh     := '90800';
                p_source_wh     := '400069';
                v_cur_WH        := 'FG';
                v_wh_type       := '90800';
                v_costing_type  := '4900';
                v_reason        := '90800';
                
                v_pkey 			 := inv_item.pkey;
                BEGIN
                     SELECT COST, company_cost
                       INTO v_purchase_amt, v_company_cost
                       FROM jpn_dm_inventory
                      WHERE part      = v_part_no
                        AND warehouse = 'FG';
                EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    v_purchase_amt := '0';
                     v_company_cost :='0';
                END;
                
                
       SELECT get_inventory_cogs_value(v_part_no)  INTO v_cost FROM dual;
      
      IF v_cost IS NULL THEN
              
       INSERT INTO t820_costing (
           c820_costing_id
          ,c205_part_number_id
          ,c901_costing_type
          ,c820_part_received_dt
          ,c820_qty_received
          ,c820_purchase_amt
          ,c820_qty_on_hand       
          ,c901_status
          ,c820_delete_fl
          ,c820_created_by
          ,c820_created_date
          ,c1900_company_id
          ,c5040_plant_id
          ,C820_LOCAL_COMPANY_COST
          ,C1900_OWNER_COMPANY_ID
          ,c901_owner_currency
          ,C901_LOCAL_COMPANY_CURRENCY
        ) 
        VALUES 
        (	 s820_costing.nextval
          ,v_part_no
          ,v_costing_type
          ,CURRENT_DATE
          ,0
          ,v_purchase_amt
          ,0
          ,4801
          ,'N'
          ,V_USER
          ,CURRENT_DATE
          ,v_company_id
          ,v_plant_id
          ,v_company_cost
          ,'1018'
          ,'1'
          ,'4000917'
        );
                               
     end if;               
                                         
    --Get Part price
      BEGIN
        SELECT	c2052_equity_price price
        INTO v_part_price
        FROM t2052_part_price_mapping  
        WHERE c205_part_number_id = v_part_no
        AND c1900_company_id =v_company_id;
      EXCEPTION  WHEN no_data_found THEN
          dbms_output.put_line('v_part_no=' || v_part_no || ' Price not found');
          v_part_price := null;
          dbms_output.put_line(v_part_no ||' - Without Equity Price, Inventory migration cannot be done, skip to next');
          continue;
      END;
                if (v_txn_id is null) THEN
                
                SELECT get_next_consign_id (v_txn_type_name) INTO v_txn_id FROM dual;
                
                END IF;
                	INSERT INTO MY_TEMP_KEY_VALUE_IAFG_INS(MY_TEMP_TXN_ID,MY_TEMP_TXN_KEY) values(v_pkey,v_part_no);
                v_initiate_input_str_IAFG := v_initiate_input_str_IAFG || v_part_no || ',' || v_qty || ',,,' ||
                v_part_price || '|';
                v_control_input_str_IAFG := v_control_input_str_IAFG || v_part_no || ',' || v_qty || ',' ||
                v_lot_number || ',,' || v_part_price || '|';
                v_verify_input_str_IAFG := v_verify_input_str_IAFG || v_part_no || ',' || v_qty ||','|| v_lot_number|| ','||v_fg_location_id||',' || p_target_wh || '|';
                -- Build Input String for Item Consignment
                -- IAFG INITIATE
                -- IAFG CONTROL
                -- IAFG VERIFY
                
                gm_pkg_op_inv_field_sales_tran.gm_sav_fs_inv_loc_part_details (v_fg_location_id,v_txn_id, v_part_no,v_qty,'400068',4301,null,current_date, v_user);
                v_count :=v_count+1;
                if(v_count=20) then 
                 gm_save_inhouse_item_consign (v_txn_id, v_txntype, v_reason, '0', '', 'IAFG created for the Loose item Adjustment INS', v_user, v_initiate_input_str_IAFG, v_out) ;
            -- gm_pkg_op_item_control_txn.gm_sav_item_control_main(v_txn_id,v_txntype,'on',v_control_input_str_IAFG,'0'
            -- ,
            -- v_user);
            GM_LS_UPD_INLOANERCSG (v_txn_id, v_txntype, '0', '0', NULL, 'Japan Set Data Load', v_user,
            v_control_input_str_IAFG, NULL, 'ReleaseControl', p_message) ;
            -- gm_pkg_op_item_control_txn.gm_sav_item_verify_main(v_txn_id,v_txntype,v_verify_input_str,'93345',v_user)
            -- ;
            gm_ls_upd_inloanercsg (v_txn_id, v_txntype, '', '0', NULL, 'Japan -- Manual Data --load', v_user,
            v_verify_input_str_IAFG, NULL, 'Verify', p_message) ;
            dbms_output.put_line ('IAFG INS Loc Txn=' || v_txn_id || ' Verified');
            UPDATE MY_TEMP_KEY_VALUE_IAFG_INS SET     MY_TEMP_TXN_VALUE= v_txn_id  WHERE MY_TEMP_TXN_VALUE IS NULL;
			
           
			UPDATE t412_inhouse_transactions SET c412_comments='IAFG Inventory Update for Inspection Location' , c412_last_updated_date=CURRENT_DATE, c412_last_updated_by=v_user
			where c412_inhouse_trans_id=v_txn_id;

            v_count :='0';
            v_initiate_input_str_IAFG :=NULL;
            v_control_input_str_IAFG :=NULL;
            v_verify_input_str_IAFG :=NULL;
            v_txn_id := NULL;
            
           end if;
              end;
            END LOOP;
            
            IF (v_initiate_input_str_IAFG IS NOT NULL) THEN
              gm_save_inhouse_item_consign (v_txn_id, v_txntype, v_reason, '0', '', 'IAFG created for the Loose item Adjustment INS', v_user, v_initiate_input_str_IAFG, v_out) ;
            -- gm_pkg_op_item_control_txn.gm_sav_item_control_main(v_txn_id,v_txntype,'on',v_control_input_str_IAFG,'0'
            -- ,
            -- v_user);
            GM_LS_UPD_INLOANERCSG (v_txn_id, v_txntype, '0', '0', NULL, 'Japan Set Data Load', v_user,
            v_control_input_str_IAFG, NULL, 'ReleaseControl', p_message) ;
            -- gm_pkg_op_item_control_txn.gm_sav_item_verify_main(v_txn_id,v_txntype,v_verify_input_str,'93345',v_user)
            -- ;
            gm_ls_upd_inloanercsg (v_txn_id, v_txntype, '', '0', NULL, 'Japan -- Manual Data --load', v_user,
            v_verify_input_str_IAFG, NULL, 'Verify', p_message) ;
            dbms_output.put_line ('IAFG INS Loc Txn=' || v_txn_id || ' Verified');
             UPDATE MY_TEMP_KEY_VALUE_IAFG_INS SET     MY_TEMP_TXN_VALUE= v_txn_id  WHERE MY_TEMP_TXN_VALUE IS NULL;
			
             UPDATE t412_inhouse_transactions SET c412_comments='IAFG Inventory Update for Inspection Location' , c412_last_updated_date=CURRENT_DATE, c412_last_updated_by=v_user
			 where c412_inhouse_trans_id=v_txn_id;
            v_count :='0';
            v_initiate_input_str_IAFG :=NULL;
            v_control_input_str_IAFG :=NULL;
            v_verify_input_str_IAFG :=NULL;
            v_txn_id := NULL;
           
          END IF;  
 /*       update t820_costing set C820_delete_fl='Y', C820_LAST_UPDATED_BY='2277820',c901_status='4802', C820_LAST_UPDATED_DATE=CURRENT_DATE, C1900_COMPANY_ID='', C5040_PLANT_ID=''
        WHERE C820_CREATED_BY='2277820'
        and  C1900_COMPANY_ID='1022'; */

        
       
        UPDATE JP_LOANER_INV_STG T1 SET set_loaded_fl='M',
				T1.NEW_TXN_ID = (SELECT T2.MY_TEMP_TXN_VALUE FROM MY_TEMP_KEY_VALUE_IAFG_INS T2 WHERE T2.MY_TEMP_TXN_ID = T1.PKEY)
			WHERE T1.PKEY IN (SELECT T2.MY_TEMP_TXN_ID FROM MY_TEMP_KEY_VALUE_IAFG_INS T2 WHERE T2.MY_TEMP_TXN_ID = T1.PKEY)
			AND set_loaded_fl='L';
       COMMIT;
    EXCEPTION WHEN OTHERS then
    DBMS_OUTPUT.PUT_LINE('Loose Item IAFG ins ERROR for part'||v_part_no||SQLERRM);
    ROLLBACK;
   
    END tmp_gm_loose_item_iafg_ins;
    /
