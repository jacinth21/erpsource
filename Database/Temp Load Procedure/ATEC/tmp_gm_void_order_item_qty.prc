-- Procedure to void T502_item_order , T500_order_info and T502b_item_order_usage line items
CREATE OR REPLACE PROCEDURE tmp_gm_void_order_item_qty(
p_item_order_id		 IN 	  t502_item_order.C502_ITEM_ORDER_ID%TYPE,
p_user_id            IN       t501_order.C501_LAST_UPDATED_BY%TYPE)
    AS
  v_order_info_id t500_order_info.C500_ORDER_INFO_ID%TYPE;
  v_order_id      t501_order.C501_ORDER_ID%TYPE;
  
 BEGIN

	 -- Fetching order id and order info id from item order 
	 	SELECT C500_ORDER_INFO_ID,C501_ORDER_ID INTO v_order_info_id,v_order_id
	 	FROM T502_ITEM_ORDER
	 	WHERE C502_ITEM_ORDER_ID=p_item_order_id 
	 	AND C502_VOID_FL IS NULL;
	
	 	-- updating order last updated details
	 	UPDATE t501_order set 
	 	c501_last_updated_by=p_user_id,c501_last_updated_date=sysdate 
	 	where c501_order_id=v_order_id;

	 	-- updating item quantity as 0 and void the line item
		UPDATE t502_item_order SET 
		c502_item_qty=0,c502_void_fl='Y',c502_last_updated_date_utc=SYSDATE 
		WHERE C502_ITEM_ORDER_ID=p_item_order_id;
		
		-- voiding the order info line item
		update t500_order_info set 
		c500_void_fl='Y',c500_last_updated_by=p_user_id,c500_last_updated_date=sysdate 
		where c500_order_info_id=v_order_info_id;

		-- voiding the item usage order line item
		update t502b_item_order_usage set 
		c502b_void_fl='Y',c502b_last_updated_by=p_user_id,c502b_last_updated_date=sysdate 
		where c500_order_info_id=v_order_info_id;
	 	 
END tmp_gm_void_order_item_qty;
/


 
 
        