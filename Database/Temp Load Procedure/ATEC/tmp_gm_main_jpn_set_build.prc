/*
* Purpose: Main procedure to be invoked for the Loaner Set building data load migration
* Steps:
*  1. Set Config Qty validation
*  2. IABL initiation
*  3. Set Initiation and Set building
*  4. Loaner Initiate
*  Atec 403 - Set List Data Load Migration. July 2017
*  Author: gpalani. Reviewed by Richard K
*/
CREATE OR REPLACE
PROCEDURE tmp_gm_main_jpn_set_build
AS
   -- p_flag t5054_inv_location_log.C5054_LOT_STATUS_FL%TYPE := 'Y';
   
    CURSOR cur_set
    IS

     SELECT DISTINCT (C207_SET_ID) sets_id, C5010_tag_id tag_id,    ALLADIN_ACCOUNT_ID ACCT_ID
        FROM globus_app.JP_LOANER_INV_STG JPN
        WHERE
        jpn.set_loaded_fl is null
        order by C207_SET_ID, C5010_tag_id;
      
        
       BEGIN 
       
   --    gm_pkg_cor_client_context.gm_sav_client_context ('1022', get_code_name ('10306121'), get_code_name ('105131'), '3012') ;
        
    FOR set_list IN cur_set
    LOOP
       Begin
        IF (set_list.sets_id IS NULL) THEN
          BEGIN
            tmp_gm_item_initiate (set_list.tag_id, set_list.ACCT_ID,set_list.sets_id) ;
                EXCEPTION
                WHEN OTHERS THEN
                    dbms_output.put_line ('Item Consignment failed for the tag:'||set_list.tag_id||SQLERRM) ;
           ROLLBACK;
                END;
          
        ELSE
            -- tmp_gm_set_validation(set_list.sets_id, set_list.tag_id, p_flag);
            tmp_gm_iabl (set_list.sets_id, set_list.tag_id) ;
            tmp_gm_set_build (set_list.sets_id, set_list.tag_id) ;
            
            dbms_output.put_line ('Set Build Processing competed for the Tag:'||set_list.tag_id) ;
            
      /*   
            IF (set_list.ACCT_ID IS NOT NULL) THEN
                tmp_gm_Loaner_Initiate (set_list.sets_id, set_list.ACCT_ID, set_list.tag_id) ;
                dbms_output.put_line ('Loaner Successfully Initiated:'||set_list.sets_id||'And Tag ID'||set_list.tag_id);
                            
            END IF;
*/
            UPDATE JP_LOANER_INV_STG
            SET set_loaded_fl    = 'Y'
              WHERE c207_SET_ID  = set_list.sets_id
                AND c5010_tag_id = set_list.tag_id;
                
            
        END IF;
    EXCEPTION
    WHEN OTHERS THEN
        dbms_output.put_line ('Set Build Processing failed for the set:'||set_list.sets_id||'And Tag ID:'||
        set_list.tag_id||SQLERRM);
        ROLLBACK;
    END;
     
    commit;
END LOOP;
END tmp_gm_main_jpn_set_build;
/