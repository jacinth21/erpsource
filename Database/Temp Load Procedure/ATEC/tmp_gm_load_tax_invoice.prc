/*
************************************************
* The purpose of this procedure to update tax amount for historical invoices
* Author: Mahavishnu
************************************************* */
CREATE OR REPLACE
PROCEDURE tmp_gm_load_tax_invoice
      
AS

	CURSOR cur_invoice
    IS
    	select c503_invoice_id invid,c503_customer_po cuspo from t503_invoice where c503_void_fl is null and c1900_company_id=1026;
    	
   BEGIN
	   
	   gm_pkg_cor_client_context.gm_sav_client_context('1026',get_code_name('10306121'),get_code_name('105131'),'3017');

	    FOR inv_cur IN cur_invoice
        
        LOOP
        	
        	gm_pkg_ac_invoice_tax.gm_sav_taxrate_by_invoice(inv_cur.invid,inv_cur.cuspo,'303510');
        	
        END LOOP;
       
END tmp_gm_load_tax_invoice;
/
