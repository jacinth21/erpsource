-- Procedure to remove parent order relation
CREATE OR REPLACE PROCEDURE tmp_gm_remove_parent_order(
p_order_id		     IN 	      t501_order.C501_ORDER_ID%TYPE,
p_user_id            IN       t501_order.C501_LAST_UPDATED_BY%TYPE)
  AS
  
  
 BEGIN

	 	-- Removing parent order id for sales adjustment orders
	 	UPDATE t501_order set 
	 	 C501_parent_ORDER_ID='',c501_last_updated_by=p_user_id,c501_last_updated_date=sysdate 
	 	where c501_order_id=p_order_id;

	
END tmp_gm_remove_parent_order;
/


 
 
        