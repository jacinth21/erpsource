CREATE OR REPLACE PROCEDURE TMP_CLEAR_PO_DEATAILS
(
	  p_invoice_id     IN  t503_invoice.C503_INVOICE_ID%TYPE,
	  p_userid         IN  t503_invoice.C503_CREATED_BY%TYPE  
)
AS
	v_void_fl       	t503_invoice.C503_VOID_FL%TYPE;
	v_cust_po_id        t503_invoice.C503_CUSTOMER_PO%TYPE;
	
	
	BEGIN
		
		IF p_invoice_id is NULL THEN
			GM_RAISE_APPLICATION_ERROR('-20999','401','Invoice Id');
 	 	END IF;
	
 	 	BEGIN
	 	 	
	 	 	SELECT C503_VOID_FL,
 			C503_CUSTOMER_PO
			INTO v_void_fl,
 			v_cust_po_id
			FROM t503_invoice
			WHERE C503_INVOICE_ID = p_invoice_id
			AND c1900_company_id  ='1026';
	 	 	EXCEPTION
 	 	    WHEN NO_DATA_FOUND 
 	 	    THEN
 	 	    GM_RAISE_APPLICATION_ERROR('-20999','403',p_invoice_id);
	 	 	
	    END;
	    
	    IF v_void_fl IS NULL THEN
			
	 	 		GM_RAISE_APPLICATION_ERROR('-20999','409',p_invoice_id);
 	 		
	 	ELSE 
	 		
	 		UPDATE t503b_invoice_attribute
			SET c503b_void_fl        ='Y',
  			c503b_last_updated_by  =p_userid,
  			c503b_last_updated_date=SYSDATE
			WHERE c503_invoice_id    = p_invoice_id;
	 		
	 		
	 		UPDATE t9601_batch_details
			SET c9601_void_fl         = 'Y',
 			c9601_last_update_by    = p_userid,
  			c9601_last_updated_date = SYSDATE
			WHERE c9601_ref_id       IN
  			(
	  			SELECT c501_order_id
	  			FROM t501_order
	  			WHERE c501_customer_po = v_cust_po_id
	  			AND c1900_company_id   ='1026'
	  			AND c501_void_fl      IS NULL
  			)
			AND c9601_void_fl IS NULL; 
			
			
			UPDATE t501_order
			SET C501_CUSTOMER_PO     = NULL ,
 			c501_last_updated_by   = p_userid ,
  			c501_last_updated_date = SYSDATE
			WHERE c501_customer_po   =v_cust_po_id
			AND	c1900_company_id     ='1026'
			AND c501_void_fl        IS NULL;

	 		
	 	END IF;
	

END TMP_CLEAR_PO_DEATAILS;
/
