/*
This procedure is created to create the RA transaction to match the Qty provided by API team for the field sales warehouse
Author :gpalani
TSK: 8279

*/

CREATE OR REPLACE PROCEDURE tmp_gm_ra
AS   

    p_rma_id  VARCHAR2 (100) ;
    p_rma_ids VARCHAR2 (4000) ;
    v_cur_RA_dtls TYPES.cursor_type;
    v_pnum t507_returns_item.c205_part_number_id%TYPE;
    v_pdesc VARCHAR2 (4000) ;
    v_qty t507_returns_item.c507_item_qty%TYPE;
    v_cons_qty NUMBER;
    v_init_qty NUMBER;
    v_cnum t507_returns_item.c507_control_number%TYPE;
    v_itype t507_returns_item.c901_type%TYPE;
    v_tagfl           VARCHAR2 (200) ;
    v_part_mtrel_type VARCHAR2 (200) ;
    v_price t507_returns_item.C507_ITEM_PRICE%TYPE;
    v_unit_price t507_returns_item.c507_unit_price%TYPE;
    v_unit_price_adj_val t507_returns_item.c507_unit_price_adj_value%TYPE;
    v_unit_price_adj_code t507_returns_item.c901_unit_price_adj_code%TYPE;
    v_do_unit_price t507_returns_item.C507_DO_UNIT_PRICE%TYPE;
    v_adj_code_val t507_returns_item.C507_ADJ_CODE_VALUE%TYPE;
    v_err_dtls t507_returns_item.c507_error_dtls%TYPE;
    v_equity_price T2052_PART_PRICE_MAPPING.C2052_EQUITY_PRICE%TYPE;
    v_user t101_user.C101_USER_ID%TYPE := '303043';
    V_OUT NUMBER;
    v_initiate_input_str_FGIA CLOB;
    v_control_input_str_FGIA CLOB;
    v_verify_input_str_FGIA CLOB;
    strRelVerFl       VARCHAR2 (10) := 'on';
    v_err_fl          VARCHAR2 (200) ;
    p_input_str       VARCHAR2 (4000) ;
    p_RHPN_input_str  VARCHAR2 (4000) ;
    p_err_fl          VARCHAR2 (200) ;
    p_errmsg          VARCHAR2 (4000) ;
    p_returnids       VARCHAR2 (4000) ;
    p_rhpn_err_msg    VARCHAR2 (4000) ;
    p_rhpn_verify_msg VARCHAR2 (4000) ;
    p_pnfg_id         VARCHAR2 (200) ;
    p_fgia_id         VARCHAR2 (200) ;
    p_pnfg_input_str CLOB;
    p_pnfg_in_ctrl_str CLOB;
    p_pnfg_verify_str CLOB;
    p_pnfg_err_fl VARCHAR2 (200) ;
    user_id       VARCHAR2 (200) := '303043';
    CURSOR CUR_RTN
    IS
         SELECT TRA.dist_ID dist_id, TRA.exp_date exp_date, TRA.REASON reason
          , TRA.c506_type type, TRA.ACCT_ID emp_id, TRA.qty pqty
          , TRA.part pid, TRA.credit cr_date, tra.control_num cnum
           FROM JPN_DM_RA TRA, t701_distributor T701
          WHERE TRA.dist_ID = t701.C701_DISTRIBUTOR_ID
          AND processed_flag IS NULL
		  order by TRA.part;
BEGIN
   --  gm_pkg_cor_client_context.gm_sav_client_context ('1022', get_code_name ('10306121'), get_code_name ('105131'), '3012') ;
    --gm_pkg_cor_client_context.gm_sav_client_context('1000',get_code_name('105440'),get_code_name('105130'),'3000');
    p_pnfg_input_str          := NULL;
    p_pnfg_in_ctrl_str        := NULL;
    p_pnfg_verify_str         := NULL;
    v_initiate_input_str_FGIA := NULL;
    v_control_input_str_FGIA  := NULL;
    v_verify_input_str_FGIA   := NULL;
    FOR RTN  IN CUR_RTN
    LOOP
        BEGIN
            gm_pkg_op_return.gm_op_sav_return (RTN.dist_id, RTN.exp_date, RTN.reason, user_id, RTN.type, RTN.emp_id,
            RTN.pqty, RTN.pid, '', p_rma_id) ;
            DBMS_OUTPUT.PUT_LINE ('RA initiated:'||p_rma_id) ;
            --   COMMIT;
        EXCEPTION
        WHEN OTHERS THEN
            dbms_output.put_line ('RA Initiate failed for the DIST'||RTN.dist_id || 'for the Part(s)' ||RTN.pid) ;
            ROLLBACK;
        END;
        BEGIN
            gm_pkg_op_return.GM_OP_FCH_RETURN_ACCP (p_rma_id, '', '', v_cur_RA_dtls) ;
            LOOP
                FETCH v_cur_RA_dtls
                   INTO v_pnum, v_pdesc, v_qty
                  , v_cons_qty, v_init_qty, v_cnum
                  , v_itype, v_tagfl, v_part_mtrel_type
                  , v_price, v_unit_price, v_unit_price_adj_val
                  , v_unit_price_adj_code, v_do_unit_price, v_adj_code_val
                  , v_err_dtls;
                EXIT
            WHEN v_cur_RA_dtls%NOTFOUND;
            END LOOP;
            p_input_str := NULL;
            p_input_str := p_input_str || v_pnum || ',' || v_qty || ',' || RTN.cnum || ',' || v_cnum || ',' || v_itype|| ',' || v_price || ',' || v_unit_price || ',' || v_unit_price_adj_val || ',' || v_unit_price_adj_code ||'|' ;
            DBMS_OUTPUT.PUT_LINE ('p_input_str: '||p_input_str) ;
        END;
        BEGIN
            GM_PKG_OP_RETURN.gm_sav_return_lot_num (p_rma_id, '101723', p_input_str, '', '303043', '93343', p_err_fl) ;
            DBMS_OUTPUT.PUT_LINE ('RA lot flag:'||p_err_fl) ;
            --   COMMIT;
        EXCEPTION
        WHEN OTHERS THEN
            dbms_output.put_line ('RA lot failed for the DIST'||p_rma_id) ;
            ROLLBACK;
        END;
        IF p_err_fl = 'N' OR p_err_fl IS NULL OR p_err_fl = '' THEN
            DBMS_OUTPUT.PUT_LINE ('p_rma_id: '|| p_rma_id || 'p_input_str: '|| p_input_str || 'RTN.exp_date: ' || RTN.exp_date);
            BEGIN
                GM_PKG_OP_RETURN.GM_OP_SAV_RETURN_ACCP (p_rma_id, p_input_str, RTN.exp_date, 1, '303043', p_errmsg) ;
                DBMS_OUTPUT.PUT_LINE ('RA(' || p_rma_id || ')Accepted :'||p_errmsg) ;
                --  COMMIT;
            EXCEPTION
            WHEN OTHERS THEN
                dbms_output.put_line ('RA(' || p_rma_id || ') not Accepted :'||SQLERRM) ;
                ROLLBACK;
            END;
        END IF;
        BEGIN
            gm_pkg_op_return.gm_op_sav_return_forcredit (p_rma_id, RTN.cr_date, '303043') ;
            DBMS_OUTPUT.PUT_LINE ('RA Credited : ' || p_rma_id) ;
            --     COMMIT;
        EXCEPTION
        WHEN OTHERS THEN
            dbms_output.put_line ('RA not Credited : '|| p_rma_id||SQLERRM) ;
                   ROLLBACK;
        END;
        BEGIN
            p_rma_ids := NULL;
            -- p_rma_ids :=  RTN.pid || ',' || p_rma_id ;
            p_rma_ids        := p_rma_id;
            p_RHPN_input_str := NULL;
            p_RHPN_input_str := p_RHPN_input_str || RTN.pid || '^' || RTN.cnum || '^' || RTN.pqty || '|';
            dbms_output.put_line ('p_RHPN_input_str  :'||p_RHPN_input_str) ;
            GM_PKG_OP_REPROCESS.GM_SAVE_REPROCESS_RETURNS (p_rma_id, p_rma_id, '', '', p_RHPN_input_str, '0', '303043','', 'RA For RHPN JPN Data Migration', '', '', p_returnids, p_rhpn_err_msg) ;
            dbms_output.put_line ('Created RHPN ID :'||p_returnids) ;
            --      COMMIT;
             SELECT trim (',' FROM p_returnids) INTO p_returnids FROM dual;
            dbms_output.put_line (' RHPN ID :'||p_returnids) ;
            --verify
            GM_UPDATE_REPROCESS_MATERIAL (p_returnids, 'JPN Data Migration', '400058', 303043, 303043, p_rhpn_verify_msg) ;
            dbms_output.put_line (' RHPN Verified :'||p_rhpn_verify_msg) ;
            --   COMMIT;
        EXCEPTION
        WHEN OTHERS THEN
            dbms_output.put_line ('RHPN not created : '|| p_rhpn_err_msg||SQLERRM) ;
                ROLLBACK;
        END;
        BEGIN
             SELECT T2052.C2052_EQUITY_PRICE PRICE
               INTO v_equity_price
               FROM T205_PART_NUMBER T205, T2052_PART_PRICE_MAPPING T2052
              WHERE T205.C205_PART_NUMBER_ID  = RTN.pid
                AND T205.C205_PART_NUMBER_ID  = T2052.C205_PART_NUMBER_ID(+)
                AND T205.C205_PART_NUMBER_ID IN
                (
                     SELECT T2023.C205_PART_NUMBER_ID
                       FROM T2023_PART_COMPANY_MAPPING T2023
                      WHERE REGEXP_LIKE (T2023.C205_PART_NUMBER_ID, RTN.pid)
                        AND T2023.C2023_VOID_FL          IS NULL
                        AND T2023.C1900_COMPANY_ID        = '1022'
                        AND T2023.C901_PART_MAPPING_TYPE IN (105360, 105361, 10304547)
                )
                AND T2052.C1900_COMPANY_ID(+) = '1022'
                AND t2052.c2052_void_fl(+)   IS NULL ;
            p_pnfg_input_str                 := NULL;
            p_pnfg_input_str                 := p_pnfg_input_str || RTN.pid || ',' || RTN.pqty ||',' || ',' ||v_equity_price ||'|';
            dbms_output.put_line ('p_pnfg_input_str :'||p_pnfg_input_str) ;
             SELECT GET_NEXT_CONSIGN_ID ('400063') INTO p_pnfg_id FROM dual;
            GM_SAVE_ITEM_CONSIGN (p_pnfg_id, 400063, 120420, '01', '', '50', 'RA For JPN PNFG Data Migration', '1','303043', p_pnfg_input_str) ;
            dbms_output.put_line ('Created PNFG ID :'||p_pnfg_id) ;
            -- Control
            p_pnfg_in_ctrl_str := NULL;
            p_pnfg_in_ctrl_str := p_pnfg_in_ctrl_str || RTN.pid || ',' || RTN.pqty ||','|| RTN.cnum || ',' || ',' ||RTN.cnum ||','|| '90800' ||'|';
            dbms_output.put_line ('p_pnfg_in_ctrl_str :'||p_pnfg_in_ctrl_str) ;
            gm_pkg_op_item_control_txn.gm_sav_item_control_main (p_pnfg_id, '400063', 'on', p_pnfg_in_ctrl_str, 93343,303043) ;
            dbms_output.put_line ('Controlled PNFG ID :') ;
            p_pnfg_verify_str := NULL;
            p_pnfg_verify_str := RTN.pid || ',' || RTN.pqty ||','|| RTN.cnum || ',' ||''|| ',' || RTN.cnum ||','||'90800' ||'|';
            dbms_output.put_line ('p_pnfg_verify_str:'|| p_pnfg_verify_str) ;
            gm_pkg_op_item_control_txn.gm_sav_item_verify_main (p_pnfg_id, 400063, p_pnfg_verify_str, 93345, '303043');
            gm_pkg_op_inv_field_sales_tran.gm_sav_fs_inv_loc_part_details ('66839',p_pnfg_id, RTN.pid,RTN.pqty,'400063','4301',null,current_date, v_user);

            dbms_output.put_line (' PNFG Verified ') ;
            --               UPDATE JPN_DM_RA SET processed_flag = 'Y' WHERE c701_distributor_id = RTN.dist_id;
            --       COMMIT;
        EXCEPTION
        WHEN OTHERS THEN
            dbms_output.put_line ('PNFG not created '||SQLERRM) ;
                ROLLBACK;
        END;
        BEGIN
             SELECT GET_NEXT_CONSIGN_ID ('400065') INTO p_fgia_id FROM dual;
            -- Input String creation for FGIA
	        v_initiate_input_str_FGIA := NULL;
            v_initiate_input_str_FGIA := v_initiate_input_str_FGIA||RTN.pid||','||RTN.pqty || ',,'||v_equity_price||'|' ;
			
			v_control_input_str_FGIA := NULL;
			v_control_input_str_FGIA := v_control_input_str_FGIA||RTN.pid||','||RTN.pqty||','||RTN.cnum||',,'|| v_equity_price||','||'900800'||'|';
			
			v_verify_input_str_FGIA := NULL;
            v_verify_input_str_FGIA := v_verify_input_str_FGIA||RTN.pid||','||RTN.pqty||','||RTN.cnum||','||',' || RTN.cnum||','||'900800'|| '|';
			
            -- Calling the FGIA Initiation procedure.
            gm_save_inhouse_item_consign (p_fgia_id, 400065, '400067', '0', '', 'FGIA created for the RA', v_user,v_initiate_input_str_FGIA, v_out) ;
			
            gm_pkg_op_item_control_txn.gm_sav_item_lot_num (p_fgia_id, 400065, v_control_input_str_FGIA, '93343', v_user, strRelVerFl, v_err_fl) ;
			
            gm_pkg_op_item_control_txn.gm_sav_item_control_main (p_fgia_id, 400065, 'on', v_control_input_str_FGIA, '93343', v_user) ;
			
            gm_pkg_op_item_control_txn.gm_sav_item_verify_main (p_fgia_id, 400065, v_verify_input_str_FGIA, '93345', v_user) ;
			
            gm_pkg_op_inv_field_sales_tran.gm_sav_fs_inv_loc_part_details ('66839',p_fgia_id, RTN.pid,(RTN.pqty*-1),'400065','4302',null,current_date, v_user);
			
			dbms_output.put_line ('FGIA Verified '||p_fgia_id);
			
			UPDATE JPN_DM_RA SET NEW_TXN_ID=p_rma_id, processed_flag='S' WHERE DIST_ID=RTN.dist_id AND PART=RTN.pid;
			
			
			
        END;
    END LOOP;
END tmp_gm_ra;
/
