CREATE OR REPLACE PROCEDURE TMP_UPDATE_MONTHLY_INV_ATT
(
	  p_invoice_id     IN  t503_invoice.C503_INVOICE_ID%TYPE,
	  p_opening_bal    IN  t503b_invoice_attribute.C503B_UNPAID_INVOICE_AMT%TYPE,
	  p_paid_amt       IN  t503b_invoice_attribute.C503B_PAID_INVOICE_AMT%TYPE,
	  p_sales_amt      IN  t503b_invoice_attribute.C503B_TRANSACTION_AMT%TYPE,
	  p_tax_amt        IN  t503b_invoice_attribute.C503B_CONSUMPTION_TAX_AMOUNT%TYPE,
	  p_userid         IN  t503_invoice.C503_CREATED_BY%TYPE  
)
AS
	v_void_fl       	t503_invoice.C503_VOID_FL%TYPE;
	v_total_sale        t503b_invoice_attribute.C503B_TOTAL_SALES_AMT%TYPE;
	v_end_balance   	t503b_invoice_attribute.C503B_ENDING_AR_BAL%TYPE;
	
	
	BEGIN
		
		IF p_invoice_id is NULL THEN
			GM_RAISE_APPLICATION_ERROR('-20999','401','Invoice Id');
 	 	END IF;
	
 	 	BEGIN
	 	 	
	 	 	SELECT C503_VOID_FL
			INTO v_void_fl
			FROM t503_invoice
			WHERE C503_INVOICE_ID = p_invoice_id 
			AND C1900_COMPANY_ID ='1026';
	 	 	EXCEPTION
 	 	    WHEN NO_DATA_FOUND 
 	 	    THEN
 	 	    GM_RAISE_APPLICATION_ERROR('-20999','403',p_invoice_id);
	    END;
	    
	    IF v_void_fl = 'Y' THEN
			
	 	 		GM_RAISE_APPLICATION_ERROR('-20999','402',p_invoice_id);
	 	 
 	 		
	 	ELSE 
	 		v_total_sale := NVL(p_sales_amt,0) + NVL(p_tax_amt,0);
	 		v_end_balance := (v_total_sale +  NVL(p_opening_bal,0)) -  NVL(p_paid_amt,0);
	 		
	 		UPDATE t503b_invoice_attribute
			SET C503B_UNPAID_INVOICE_AMT  =p_opening_bal,
  			C503B_PAID_INVOICE_AMT      =p_paid_amt,
  			C503B_TRANSACTION_AMT       =p_sales_amt,
 			C503B_CONSUMPTION_TAX_AMOUNT=p_tax_amt,
  			C503B_TOTAL_SALES_AMT       =v_total_sale,
  			C503B_ENDING_AR_BAL         =v_end_balance,
  			C503B_LAST_UPDATED_BY       =p_userid,
  			C503B_LAST_UPDATED_DATE     =SYSDATE
			WHERE C503_INVOICE_ID         = p_invoice_id
			AND C503B_VOID_FL            IS NULL;
	 		
	 	END IF;
	

END TMP_UPDATE_MONTHLY_INV_ATT;
/
