/*
This procedure is created to create the Item consignment for the field sales warehouse.
Author :gpalani
TSK: 8279

*/
CREATE OR REPLACE
PROCEDURE tmp_gm_fswh_load
AS

  v_user t101_user.c101_user_id%TYPE := '303043';
    v_input_str_item_consign CLOB;
    v_control_str_item_consign CLOB;
    v_part_no t205_part_number.c205_part_number_id%TYPE;
    v_company_id          t1900_company.C1900_COMPANY_ID%TYPE;
    v_plant_id            t5040_plant_master.C5040_PLANT_ID%TYPE;
    v_portal_com_date_fmt VARCHAR2 (20) ;
    v_qty                 NUMBER;
    v_dist_id t701_distributor.C701_DISTRIBUTOR_ID%TYPE;
    v_lot_number       VARCHAR2 (40) ;
    v_txntype          NUMBER;
    v_ship_date        VARCHAR2 (100) ;
    dist_id           t701_distributor.C701_DISTRIBUTOR_ID%TYPE;
    p_req_id           VARCHAR2 (20) ;
    p_out_cn_id        VARCHAR2 (20) ;
    strTxnType         NUMBER        := '4110';
    strRelVerFl        VARCHAR2 (10) := 'on';
    strLocType         NUMBER        := '93343';
    v_shipid           NUMBER;
    p_shipflag         VARCHAR2 (10) ;
    v_fg_location_id t5052_location_master.C5052_LOCATION_ID%TYPE;
    v_count      NUMBER := 0;
    v_count_item NUMBER := 0;
    v_pkey number;
	
    CURSOR cur_item_acc
    IS
        SELECT DISTINCT (JPN.C701_DISTRIBUTOR_ID) dist_id
        FROM JP_LOANER_INV_STG JPN
        WHERE JPN.C5010_tag_id       IS NULL
   		AND C701_DISTRIBUTOR_ID IS NOT NULL
        AND JPN.C207_SET_ID        IS NULL
		AND JPN.TYPE ='FSWH'
        AND JPN.set_loaded_fl       = 'P'
        ORDER BY C701_DISTRIBUTOR_ID;
	   
    CURSOR cur_item_initiate
    IS
         SELECT JPN.C205_part_number_id PART_NO, JPN.physical_set_QTY QTY, jpn.lot_number LOT_NUMBER, jpn.pkey pkey
         FROM JP_LOANER_INV_STG JPN
         WHERE JPN.C5010_tag_id      IS NULL
		 AND C701_DISTRIBUTOR_ID= v_dist_id
         AND JPN.C207_SET_ID       IS NULL
		 AND JPN.TYPE ='FSWH'
         AND JPN.set_loaded_fl='P'
         ORDER BY C205_part_number_id;
BEGIN
    -- gm_pkg_cor_client_context.gm_sav_client_context ('1022', get_code_name ('10306121'), get_code_name ('105131'),'3012');
	
        SELECT get_compdtfmt_frm_cntx () INTO v_portal_com_date_fmt FROM dual;
        SELECT get_plantid_frm_cntx INTO v_plant_id FROM dual;
        SELECT get_compid_frm_cntx () INTO v_company_id FROM dual;
    -- Create Location for FG and BL
    BEGIN
         SELECT c5052_location_id
         INTO v_fg_location_id
         FROM t5052_location_master
         WHERE c5052_location_cd = 'JPN_FG_LOCATION'
         AND c1900_company_id  = v_company_id
         AND c5052_void_fl is null;
    
    EXCEPTION
	
		WHEN OTHERS THEN
        dbms_output.put_line ('*********Location error' || ' Error:' || sqlerrm) ;
    END;
    
    BEGIN
    
        FOR inv_item_acc IN cur_item_acc
        
			LOOP
				v_dist_id            := inv_item_acc.dist_id;
				
			    BEGIN
				
				   FOR inv_item_consign IN cur_item_initiate
				
				     LOOP
						v_qty        := inv_item_consign.QTY;
						v_part_no    := inv_item_consign.PART_NO;
						v_lot_number := inv_item_consign.LOT_NUMBER;
						v_pkey 		 := inv_item_consign.pkey;
         
                    INSERT INTO MY_TEMP_KEY_VALUE_loose_item(MY_TEMP_TXN_ID,MY_TEMP_TXN_KEY) values(v_pkey,v_part_no);
                 
					v_input_str_item_consign   := v_input_str_item_consign|| v_part_no||'^'||v_qty||'^'||v_ship_date||'^|';
					v_control_str_item_consign := v_control_str_item_consign||v_part_no||','||v_qty||','||v_lot_number||','||v_fg_location_id||','||'TBE'||','||'90800'||'|';
                
					v_count_item                 := v_count_item + 1;
                
                IF (v_count_item >= 20) THEN
                
                  	  v_input_str_item_consign := v_input_str_item_consign||'~5001^5004|';
                    
                   	   gm_pkg_op_process_prod_request.gm_sav_initiate_item (NULL, CURRENT_DATE, 50618, NULL, 40021, v_dist_id, 50625, V_USER, 4120, NULL, NULL, 15, V_USER, v_input_str_item_consign, 50060, '',CURRENT_DATE, p_req_id, p_out_cn_id, NULL) ;
                    
                 	   gm_pkg_cm_shipping_trans.gm_sav_shipping (p_req_id, '50184', '4120', v_dist_id, '5001', '5004', NULL,'0', NULL, v_user, v_shipid, NULL, NULL);
                  	  
                    -- controlling the Item consignment
                    	gm_pkg_op_item_control_txn.gm_sav_item_control_main (p_out_cn_id, strTxnType, strRelVerFl,v_control_str_item_consign, strLocType, V_USER);
                    	
                    --  gm_pkg_op_item_control_txn.gm_sav_item_control_main(v_txn_id, v_txntype, 'on', v_control_input_str,93343, v_user);
                    	gm_pkg_cm_shipping_trans.gm_sav_shipout (p_out_cn_id, '50181', '4120', v_dist_id, '5001', '5004',p_out_cn_id|| ':FSWH', 0, 0, '303043', 0, v_shipid, NULL, p_shipflag);
                    	v_count_item               := 0;
                    	v_input_str_item_consign   := NULL;
                    	v_control_str_item_consign := NULL;
                    	v_shipid                   := NULL;
                        
                    	UPDATE t504_consignment SET C504_LAST_UPDATED_BY = v_user, C504_LAST_UPDATED_DATE = CURRENT_DATE, c504_comments ='FSWH inventory update for the distributor: '||v_dist_id
                      	WHERE c504_consignment_id = p_out_cn_id;
                      
                        UPDATE MY_TEMP_KEY_VALUE_loose_item SET    MY_TEMP_TXN_VALUE= p_out_cn_id  WHERE MY_TEMP_TXN_VALUE IS NULL;
                    	
                      	dbms_output.put_line ('FSWH inventory update for the Distributor '||v_dist_id);
                END IF;
            END LOOP;
            
            IF (v_input_str_item_consign IS NOT NULL) THEN
            
                v_input_str_item_consign := v_input_str_item_consign||'~5001^5004|';
                
                gm_pkg_op_process_prod_request.gm_sav_initiate_item (NULL, CURRENT_DATE, 50618, NULL, 40021, v_dist_id,50625, V_USER, 4120, NULL, NULL, 15, V_USER, v_input_str_item_consign, 50060, '', CURRENT_DATE,p_req_id, p_out_cn_id, NULL) ;
              				      
                gm_pkg_cm_shipping_trans.gm_sav_shipping (p_req_id, '50184', '4120', v_dist_id, '5001', '5004', NULL, '0', NULL, v_user, v_shipid, NULL, NULL) ;
                
                -- controlling the Item consignment
                gm_pkg_op_item_control_txn.gm_sav_item_control_main (p_out_cn_id, strTxnType, strRelVerFl,v_control_str_item_consign, strLocType, V_USER) ;
                
                --  gm_pkg_op_item_control_txn.gm_sav_item_control_main(v_txn_id, v_txntype, 'on', v_control_input_str,93343,v_user);
                gm_pkg_cm_shipping_trans.gm_sav_shipout (p_out_cn_id, '50181', '4120', v_dist_id, '5001', '5004',p_out_cn_id|| ':FSWH', 0, 0, '303043', 0, v_shipid, NULL, p_shipflag) ;
                
                 UPDATE t504_consignment
                 SET C504_LAST_UPDATED_BY = v_user, C504_LAST_UPDATED_DATE = CURRENT_DATE, c504_comments ='FSWH inventory update for the distributor: '||v_dist_id   WHERE c504_consignment_id = p_out_cn_id;
                  
                 UPDATE MY_TEMP_KEY_VALUE_loose_item SET MY_TEMP_TXN_VALUE= p_out_cn_id  WHERE MY_TEMP_TXN_VALUE IS NULL;
                    
                 dbms_output.put_line ('FSWH inventory updated for the Distributor: '||v_dist_id);
                 v_dist_id                  := NULL;
                 v_count_item               := 0;
                 v_input_str_item_consign   := NULL;
                 v_control_str_item_consign := NULL;
                 v_shipid                   := NULL;
            
			END IF;
                    
                 UPDATE JP_LOANER_INV_STG T1 SET set_loaded_fl='M',T1.NEW_TXN_ID = (SELECT T2.MY_TEMP_TXN_VALUE FROM MY_TEMP_KEY_VALUE_loose_item T2 WHERE T2.MY_TEMP_TXN_ID = T1.PKEY)
			     WHERE T1.PKEY IN (SELECT T2.MY_TEMP_TXN_ID FROM MY_TEMP_KEY_VALUE_loose_item T2 WHERE T2.MY_TEMP_TXN_ID = T1.PKEY)
			     AND SET_LOADED_FL='P';
	      
		-- COMMIT;
	  	   		 	   
		    EXCEPTION WHEN OTHERS then
		    
		    v_dist_id                  := NULL;
            v_count_item               := 0;
            v_input_str_item_consign   := NULL;
            v_control_str_item_consign := NULL;
            v_shipid                   := NULL;
            
            DBMS_OUTPUT.PUT_LINE('FSWH inventory error '||v_part_no||SQLERRM);
           -- ROLLBACK;
   	      end;
   	      
    END LOOP;
END;	
END tmp_gm_fswh_load;
/