/* Formatted on 2008/04/15 16:25 (Formatter Plus v4.8.8) */
-- @"c:\database\procedures\tmp\tmp_gm_bo_datacorrection.prc"
-- exec tmp_gm_bo_datacorrection('904.906');

CREATE OR REPLACE PROCEDURE tmp_gm_bo_datacorrection (
    p_set_id     t504_consignment.c207_set_id%TYPE
--  , p_validate   NUMBER
)
AS
    v_consign_id   t504_consignment.c504_consignment_id%TYPE;
    v_set_id       t520_request.c207_set_id%TYPE;
    v_2_set_id     t520_request.c207_set_id%TYPE;
    v_request_to   t520_request.c520_request_to%TYPE;
    v_request_by_type t520_request.c901_request_by_type%TYPE;
    v_out_request_id t520_request.c520_request_id%TYPE;
    v_out_child_request_id t520_request.c520_request_id%TYPE;
    v_pnum_id      t205_part_number.c205_part_number_id%TYPE;
    v_item_qty     t505_item_consignment.c505_item_qty%TYPE;
    v_out_request_detail_id t521_request_detail.c521_request_detail_id%TYPE;
    v_csg_created_date t504_consignment.c504_created_date%TYPE;
    v_csg_lastupd_date t504_consignment.c504_last_updated_date%TYPE;
    v_child_material_request_bool BOOLEAN := FALSE;
    v_out_reconfigure_log_id t530_reconfigure_log.c530_reconfigure_log_id%TYPE;
    v_out_reconfig_log_detail_id t531_reconfigure_log_detail.c531_reconfigure_log_detail_id%TYPE;
    v_cnt          NUMBER;
	v_status	   t520_request.c520_status_fl%TYPE;	

    CURSOR cur_backlog_mr
    IS
        SELECT t504.c504_consignment_id csgid, t504.c207_set_id setid, t504.c504_created_date cdate
				, t504.c504_status_fl , t504.c504_verify_fl
          FROM t504_consignment t504
         WHERE t504.c207_set_id IS NOT NULL
           AND t504.c504_status_fl <= 3
           AND t504.c504_void_fl IS NULL
		   AND c520_request_id IS NULL;
           --AND t504.c207_set_id = p_set_id; -- to be removed later

    CURSOR cur_backorder_mr (
        p_consign_id   VARCHAR2
    )
    IS
        SELECT t505.c205_part_number_id pnum, t505.c505_item_qty qty, t504.c207_set_id setid
             , t504.c504_last_updated_date ldate
          FROM t504_consignment t504, t505_item_consignment t505
         WHERE t504.c504_consignment_id = t505.c504_consignment_id
           AND t504.c504_status_fl IN (0, 1, 2)
           AND t504.c504_verify_fl = '0'
           AND t504.c504_void_fl IS NULL
           AND TRIM (t505.c505_control_number) IS NULL
           AND t504.c504_consignment_id = p_consign_id;
           --AND t504.c207_set_id = p_set_id; -- to be removed later
BEGIN
-- 1. validate if there are no sets in Ready to ship status
    --IF p_validate = 1
    --THEN
    --    SELECT COUNT (*)
    --      INTO v_cnt
    --      FROM t504_consignment t504
    --     WHERE t504.c207_set_id IS NOT NULL AND t504.c504_status_fl = 3 AND t504.c504_void_fl IS NULL AND c520_request_id IS NULL ;

    --    IF v_cnt > 0
    --    THEN
    --        raise_application_error (-20075, '');
    --    END IF;
   -- END IF;

    FOR backlog_row IN cur_backlog_mr
    LOOP
        v_consign_id := backlog_row.csgid;
        v_set_id    := backlog_row.setid;
        v_csg_created_date := backlog_row.cdate;
        v_child_material_request_bool := FALSE;

		SELECT	DECODE(backlog_row.c504_status_fl,3,'20',1,'15',2, 
					DECODE(backlog_row.c504_verify_fl,'0','15','20'))
		INTO	v_status
		FROM DUAL;

-- 2.    All sets in consignment table which havent been shipped yet (ie status flag < 3 ) will need a backlog MR
        gm_pkg_op_request_master.gm_sav_request (NULL
                                               , SYSDATE
                                               , 50617   -- consignment
                                               , v_consign_id
                                               , v_set_id
                                               , 40021 -- consignment 
                                               , NULL
                                               , NULL
                                               , NULL
                                               , NULL
                                               , NULL
                                               , NULL
                                               , v_status
                                               , 30301   -- This is the Manual Load user
					       , NULL
                                               , v_out_request_id
                                                );
        gm_procedure_log ('v_out_request_id', v_out_request_id);

        -- request date should be the date on which the consignment was created.
        UPDATE t520_request t520
           SET t520.c520_created_date = v_csg_created_date
         WHERE t520.c520_request_id = v_out_request_id;

        UPDATE t504_consignment t504
           set t504.c520_request_id = v_out_request_id
			 --, c504_last_updated_date  = sysdate
			 --, c504_last_updated_by		= 30301
         WHERE t504.c504_consignment_id = v_consign_id;

        FOR backorder_row IN cur_backorder_mr (v_consign_id)
        LOOP
            v_pnum_id   := backorder_row.pnum;
            v_item_qty  := backorder_row.qty;
            v_2_set_id  := backorder_row.setid;
            v_csg_lastupd_date := backorder_row.ldate;

--    3.    If the set consignment is in WIP (ie the status is < 3 and C504_VERIFY_FL is 0) AND if a part is not available (ie there is a part with no control #)
--        Then initate a child MR with the status as backorder and C520_REQUEST_TXN_ID as the consignment id
            IF (NOT v_child_material_request_bool)
            THEN
                gm_pkg_op_request_master.gm_sav_request (NULL
                                                       , SYSDATE
                                                       , 50617   -- consignment
                                                       , NULL
                                                       , v_set_id
                                                       , 40021	-- consignment 
                                                       , NULL
                                                       , NULL
                                                       , NULL
                                                       , NULL
                                                       , NULL
                                                       , v_out_request_id
                                                       , 10
                                                       , 30301   -- This is the Manual Load user
													   , NULL
                                                       , v_out_child_request_id
                                                        );
                v_child_material_request_bool := TRUE;

                UPDATE t520_request t520
                   SET t520.c520_last_updated_date = v_csg_lastupd_date
                 WHERE t520.c520_request_id = v_out_child_request_id;

                -- 4. Log the MR info into Reconfigure Log
                gm_pkg_op_log_request.gm_log_reconfig_request (90808   -- consignment
                                                             , v_consign_id
                                                             , v_out_child_request_id
                                                             , 30301
                                                             , v_out_reconfigure_log_id
                                                              );
            END IF;

-- 5.    Insert the part#s whose bulk qty < set qty (ie the parts with no control #) into T521_REQUEST_DETAIL
            gm_pkg_op_request_master.gm_sav_request_detail (v_out_child_request_id
                                                          , v_pnum_id
                                                          , v_item_qty
                                                          , v_out_request_detail_id
                                                           );
-- 6. Update the Reconfigure log detail with the parts that have been inserted to the material request detail
            gm_pkg_op_log_request.gm_log_reconfig_request_detail (v_out_reconfigure_log_id
                                                                , v_pnum_id
                                                                , 'TBE'
                                                                , v_item_qty
                                                                , 90803
                                                                , v_out_reconfig_log_detail_id
                                                                 );
        END LOOP;

-- 7.    Delete the part#s whose bulk qty < set qty from T505_ITEM_CONSIGNMENT
        DELETE FROM t505_item_consignment t505
              WHERE t505.c504_consignment_id = v_consign_id AND TRIM (t505.c505_control_number) IS NULL;
    END LOOP;
END tmp_gm_bo_datacorrection;
/