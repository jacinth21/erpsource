/* Formatted on 2009/06/29 11:35 (Formatter Plus v4.8.0) */

--@"C:\Database\Temp Load Procedure\tmp_non_usage_load.prc";
/************************************************************************
 * Purpose: This Prc is a data load prc run once to update the loaners and mark them either usage or non-usage or open
 * Created By Brinal
 ************************************************************************/

CREATE OR REPLACE PROCEDURE tmp_non_usage_load
AS
	CURSOR req_details
	IS
		SELECT c525_product_request_id prod_req_id
		  FROM t525_product_request
		 WHERE TRUNC (c525_created_date) >= TO_DATE ('04/01/2009', 'mm/dd/yyyy')
		   AND c525_status_fl = 40
		   AND c525_void_fl IS NULL
		   AND c525_product_request_id IN (SELECT t526.c525_product_request_id
											 FROM t526_product_request_detail t526
											WHERE c526_status_fl = 30);

	v_count 	   NUMBER;
	v_processed_date DATE;
	v_inhouse_trans_id VARCHAR2 (20);
	v_inhouse_trans_id1 VARCHAR2 (20);
BEGIN
-- to update the usage_date column
	FOR currindex IN req_details
	LOOP
		SELECT COUNT (c504a_loaner_transaction_id)
		  INTO v_count
		  FROM t504a_loaner_transaction t504al
		 WHERE t504al.c526_product_request_detail_id IN (
				   SELECT c526_product_request_detail_id
					 FROM t526_product_request_detail
					WHERE c525_product_request_id = currindex.prod_req_id
					  AND c526_status_fl != 40	 -- to avoid cancelled trans
					  AND c526_void_fl IS NULL)
		   AND c504a_return_dt IS NULL;

		IF v_count = 0	 -- all req are processed
		THEN
			SELECT MAX (c504a_processed_date)
			  INTO v_processed_date
			  FROM t504a_loaner_transaction t504al
			 WHERE t504al.c526_product_request_detail_id IN (
					   SELECT c526_product_request_detail_id
						 FROM t526_product_request_detail
						WHERE c525_product_request_id = currindex.prod_req_id
						  AND c526_status_fl != 40	 -- to avoid cancelled trans
						  AND c526_void_fl IS NULL);

			UPDATE t525_product_request
			   SET c525_usage_date = v_processed_date
				 , c525_last_updated_by = 30301
				 , c525_last_updated_date = SYSDATE
			 WHERE c525_product_request_id = currindex.prod_req_id;

			--chk if transaction exists in t412 tbl, if no rec then its non-usage
			BEGIN
				SELECT t412.c412_inhouse_trans_id
				  INTO v_inhouse_trans_id
				  FROM t412_inhouse_transactions t412, t504a_loaner_transaction t504a, t526_product_request_detail t526
				 WHERE t412.c504a_loaner_transaction_id = t504a.c504a_loaner_transaction_id
				   AND t504a.c526_product_request_detail_id = t526.c526_product_request_detail_id
				   AND t526.c525_product_request_id = currindex.prod_req_id
				   AND ROWNUM = 1;
			EXCEPTION
				WHEN NO_DATA_FOUND
				THEN
					gm_pkg_op_charges.gm_usage_status_update (currindex.prod_req_id, 20, 30301);   -- non-usage flag
			END;

			-- chk if transaction with status flg <4 exists in t412, if yes then mark as missing
			BEGIN
				SELECT t412.c412_inhouse_trans_id
				  INTO v_inhouse_trans_id
				  FROM t412_inhouse_transactions t412, t504a_loaner_transaction t504a, t526_product_request_detail t526
				 WHERE t412.c504a_loaner_transaction_id = t504a.c504a_loaner_transaction_id
				   AND t504a.c526_product_request_detail_id = t526.c526_product_request_detail_id
				   AND t526.c525_product_request_id = currindex.prod_req_id
				   AND t412.c412_status_fl < 4
				   AND ROWNUM = 1;
			EXCEPTION
				WHEN NO_DATA_FOUND
				THEN
					v_inhouse_trans_id := '0';
			END;

			IF v_inhouse_trans_id != '0'
			THEN
				gm_pkg_op_charges.gm_usage_status_update (currindex.prod_req_id, 10, 30301);   -- open flag(missing)
			ELSE
				gm_pkg_op_charges.gm_usage_status_update (currindex.prod_req_id, 20, 30301);   -- non-usage flag(missing). if usage it will overwritten
			END IF;
		END IF;

--		else skip this record too
--		DBMS_OUTPUT.put_line ('prod_req_id ' || currindex.prod_req_id);

		-- to mark the usage_flg
		BEGIN
			--if sold record found then mark it as usage
			SELECT t413.c412_inhouse_trans_id
			  INTO v_inhouse_trans_id1
			  FROM t413_inhouse_trans_items t413
				 , t412_inhouse_transactions t412
				 , t504a_loaner_transaction t504a
				 , t526_product_request_detail t526
			 WHERE t413.c412_inhouse_trans_id = t412.c412_inhouse_trans_id
			   AND t412.c504a_loaner_transaction_id = t504a.c504a_loaner_transaction_id
			   AND t504a.c526_product_request_detail_id = t526.c526_product_request_detail_id
			   AND t526.c525_product_request_id = currindex.prod_req_id
			   AND t413.c413_status_fl = 'S'
			   AND t413.c413_void_fl IS NULL
			   AND ROWNUM = 1;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				v_inhouse_trans_id1 := '0';
		END;

		--DBMS_OUTPUT.put_line ('aft exec');
		IF v_inhouse_trans_id1 != '0'
		THEN
			gm_pkg_op_charges.gm_usage_status_update (currindex.prod_req_id, 30, 30301);   -- usage flag
		END IF;
	END LOOP;
END tmp_non_usage_load;
/
