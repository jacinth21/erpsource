/* Formatted on 2009/04/17 15:29 (Formatter Plus v4.8.0) */

---@"C:\database\Temp Load Procedure\tmp_load_gm_reconciliation.prc"

CREATE OR REPLACE PROCEDURE tmp_load_gm_reconciliation (
/******************************************************************************
 * Description :	Correcting the reconciliation Status from sales to return or return to sales
 *					'******************'
 *			p_part_num		- Part Number
 *			p_item_qty		- Item quantity
 *			p_trans_id		- trans id
 *		p_status_current	- current Status
 *		p_status_change 	- status need to be changed
 *					4320	- Inventory to Loaner
 *					4846	-'Loaner to Inventory
 *					4301	- Plus
 *					4302	- Minus
 *
   Sample SQL 
  ===========
 select * from t413_inhouse_trans_items where c412_inhouse_trans_id =
'GM-LN-125509' and c205_part_number_id = '150.226';

exec tmp_load_gm_reconciliation(140468,'150.226',1,'S','R');

 select * from t413_inhouse_trans_items where c412_inhouse_trans_id =
'GM-LN-125509' and c205_part_number_id = '150.226';

 *******************************************************************************/
	p_trans_id		   IN	t413_inhouse_trans_items.c413_trans_id%TYPE
  , p_part_num		   IN	t413_inhouse_trans_items.c205_part_number_id%TYPE
  , p_item_qty		   IN	t413_inhouse_trans_items.c413_item_qty%TYPE
  -- , v_inhouse_trans_id		IN	 t413_inhouse_trans_items.c412_inhouse_trans_id%TYPE
,	p_status_current   IN	t413_inhouse_trans_items.c413_status_fl%TYPE
  , p_new_status	   IN	t413_inhouse_trans_items.c413_status_fl%TYPE
)
AS
	v_status	   t413_inhouse_trans_items.c413_status_fl%TYPE;
	v_trans_id_change t413_inhouse_trans_items.c413_trans_id%TYPE;
	v_inhouse_trans_id t413_inhouse_trans_items.c412_inhouse_trans_id%TYPE;
	v_price 	   t413_inhouse_trans_items.c413_item_price%TYPE;
	val_part_num   t413_inhouse_trans_items.c205_part_number_id%TYPE;
	val_status	   t413_inhouse_trans_items.c413_status_fl%TYPE;
	--p_trans_id		 VARCHAR2 (25) := NULL;
	v_item_qty_cur_change NUMBER := 0;
	v_total_qty    NUMBER := 0;
	v_postingtype 	   NUMBER := 0;

	v_action  NUMBER := 0;
	v_temp_qty	   NUMBER := 0;
	v_item_qty_cur NUMBER := 0;
BEGIN
	IF p_new_status = 'R'
	THEN
		v_postingtype 	:= 4846;
		v_action := 4301;

		IF p_status_current != 'S'
		THEN
			raise_application_error (-20004, ' Current flag is R. So new status flag should be S');
		END IF;
	ELSIF p_new_status = 'S'
	THEN
		v_postingtype 	:= 4320;
		v_action := 4302;

		IF p_status_current != 'R'
		THEN
			raise_application_error (-20003, 'Current flag is S. So new status flag should be R');
		END IF;
	ELSE
		raise_application_error (-20002, 'Status Flag should be R or S');
	END IF;

	IF p_item_qty < 0
	THEN
		raise_application_error (-20001, 'Quantity should  be not negative');
	END IF;

	DBMS_OUTPUT.put_line ('val_part_num=' || val_part_num);
	DBMS_OUTPUT.put_line ('p_trans_id=' || p_trans_id);

--to fetch all the records from table for corresponding trans id
	SELECT c412_inhouse_trans_id, c413_item_qty, c413_item_price, c205_part_number_id, c413_status_fl
	  INTO v_inhouse_trans_id, v_item_qty_cur, v_price, val_part_num, val_status
	  FROM t413_inhouse_trans_items
	 WHERE c413_trans_id = p_trans_id;

	/* SELECT TRUNC (c412_created_date)
	  INTO v_created_dt
	  FROM t412_inhouse_transactions
	 WHERE c412_inhouse_trans_id = v_inhouse_trans_id; */

----User entered parameter values are validated with table values -------
	IF p_part_num != val_part_num
	THEN
		raise_application_error (-20002, 'Part no sholud match for the corresponding trans id');
	ELSIF val_status != p_status_current
	THEN
		raise_application_error (-20003, 'current status Flag sholud matc for the corresponding trans id');
	ELSIF p_new_status = p_status_current
	THEN
		raise_application_error (-20001, 'current status Flag and new status flag shold not be the same.');
	ELSIF p_item_qty < v_item_qty_cur
	THEN
		raise_application_error (-20001, 'Quantity should  be not more than the existing');
	END IF;

/******************************************************************************
					****** Correcting accounting posting ******
 *******************************************************************************/

	gm_save_ledger_posting (v_postingtype, SYSDATE, p_part_num, NULL, v_inhouse_trans_id, v_item_qty_cur, NULL, 30301);

/******************************************************************************
							****** Correcting Operation Sites******
 *******************************************************************************/
	IF v_action = 4301   --Plus
	THEN
		UPDATE t205_part_number t205
		   SET t205.c205_qty_in_stock = (c205_qty_in_stock + p_item_qty)
			 , t205.c205_last_update_trans_id = v_inhouse_trans_id
			 , t205.c901_action = v_action
			 , t205.c901_type = 4127
			 , t205.c205_last_updated_by = '30301'
		 WHERE t205.c205_part_number_id = p_part_num;
	ELSIF v_action = 4302	 --Minus
	THEN
		UPDATE t205_part_number t205
		   SET t205.c205_qty_in_stock = (c205_qty_in_stock - p_item_qty)
			 , t205.c205_last_update_trans_id = v_inhouse_trans_id
			 , t205.c901_action = v_action
			 , t205.c901_type = 4127
			 , t205.c205_last_updated_by = '30301'
		 WHERE t205.c205_part_number_id = p_part_num;
	END IF;

	BEGIN
		/******************************************************************************
			For the change status flag if the record is there, then update t413 table or insert record
		*******************************************************************************/
		SELECT c413_trans_id, c413_item_qty
		  INTO v_trans_id_change, v_item_qty_cur_change
		  FROM t413_inhouse_trans_items
		 WHERE c205_part_number_id = p_part_num
		   AND c412_inhouse_trans_id = v_inhouse_trans_id
		   AND c413_status_fl = p_new_status
		   AND c413_control_number IS NOT NULL;

		DBMS_OUTPUT.put_line ('v_item_qty_cur_change= ' || v_item_qty_cur_change || 'p_item_qty=' || p_item_qty);
		v_total_qty := (v_item_qty_cur_change + p_item_qty);
		DBMS_OUTPUT.put_line ('v_total_qty= ' || v_total_qty);

		UPDATE t413_inhouse_trans_items
		   SET c413_item_qty = v_total_qty
		 WHERE c413_trans_id = v_trans_id_change;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			INSERT INTO t413_inhouse_trans_items
						(c413_trans_id, c413_control_number, c205_part_number_id, c413_item_qty
					   , c412_inhouse_trans_id, c413_item_price, c413_status_fl
						)
				 VALUES (s413_inhouse_item.NEXTVAL, 'NOC#', p_part_num, p_item_qty
					   , v_inhouse_trans_id, v_price, p_new_status
						);
	END;

	DBMS_OUTPUT.put_line ('v_item_qty_cur..' || v_item_qty_cur || 'p_item_qty...' || p_item_qty);
	v_temp_qty	:= (v_item_qty_cur - p_item_qty);
	DBMS_OUTPUT.put_line ('v_temp_qty= ' || v_temp_qty);

	/******************************************************************************
		 For the wrong status flag if item quantity is zero, then delete the record or update the quantity
	 *******************************************************************************/
	IF (v_temp_qty <= 0)
	THEN
		DBMS_OUTPUT.put_line ('DELETEing ');

		DELETE FROM t413_inhouse_trans_items
			  WHERE c413_trans_id = p_trans_id;
	ELSE
		DBMS_OUTPUT.put_line ('Updating ');

		UPDATE t413_inhouse_trans_items
		   SET c413_item_qty = v_temp_qty
		 WHERE c413_trans_id = p_trans_id;
	END IF;

EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		ROLLBACK;
		raise_application_error (-20001, 'Please enter proper parameter');
END tmp_load_gm_reconciliation;
/
