CREATE OR REPLACE PROCEDURE qb_update_payment_check_flag
AS
 CURSOR qb_cursor IS
  SELECT   c501_order_id
    ,c501_customer_po
    ,c704_account_id
  FROM t501_order
  WHERE c501_status_fl <> 5;

 v_invoice_count   NUMBER(2);
 v_rec_payment_count  NUMBER(2);
 v_temp     VARCHAR2(20);

 v_qb_refnumber   qb_invoicelink.qb_refnumber%TYPE;

 v_qb_order    t501_order.c501_order_id%TYPE;
 v_customer_po   t501_order.c501_customer_po%TYPE;
 v_account_id   t501_order.c704_account_id%TYPE;

 v_invoice_id   t503_invoice.c503_invoice_id%type;


BEGIN
--
FOR qb_val IN qb_cursor
LOOP
 v_customer_po   := qb_val.c501_customer_po;
 v_qb_order    := qb_val.c501_order_id ;
 v_account_id   := qb_val.c704_account_id;
 --
 -- Below code is used to check of the order is associated with invoice record
 --
 SELECT COUNT(1)
 INTO v_invoice_count
 FROM t503_invoice
 WHERE c704_account_id  = v_account_id
 AND  c503_customer_po = v_customer_po;
 --
 --
 if ( v_invoice_count <> 0)
 THEN
  --
  -- get invoice id for the selected order
  --
  SELECT c503_invoice_id
  INTO v_invoice_id
  FROM t503_invoice
  WHERE c704_account_id  = v_account_id
  AND  c503_customer_po = v_customer_po;
  --
  -- check if the payment is received
  SELECT COUNT(1)
  INTO v_rec_payment_count
  FROM t508_receivables
  WHERE c503_invoice_id  = v_invoice_id;
  --
  --
  -- if payment received then update inventory
  --
  IF (v_rec_payment_count <> 0 ) THEN

     UPDATE  t501_order
     SET   c501_status_fl = 5
     ,c501_last_updated_date = SYSDATE
     ,c501_last_updated_by = 30300
     WHERE c501_order_id   = v_qb_order;
  END IF;
 --
 END IF;
END LOOP;
 --
EXCEPTION WHEN OTHERS THEN
--
  DBMS_OUTPUT.PUT_LINE(' Order #'|| v_qb_order || ' Invoice count ' || v_invoice_count );
     DBMS_OUTPUT.PUT_LINE(SQLERRM);
--
END qb_update_payment_check_flag;
/
