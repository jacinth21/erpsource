/* Formatted on 2009/03/19 17:32 (Formatter Plus v4.8.0) */


CREATE OR REPLACE PROCEDURE tmp_sav_inactive_dist_writeup (
	p_distid   t701_distributor.c701_distributor_id%TYPE
)
AS
	v_rma_id	   VARCHAR2 (30);
	
	CURSOR c_get_writeup
	IS
		SELECT	 pending_qty qty, price, pnum, 'NOC#', 'W'
			FROM (SELECT c205_part_number_id pnum, (NVL (consign, 0) - NVL (cons_return, 0)) pending_qty
					   , t205.c205_equity_price price, t205.c205_product_family
					FROM t205_part_number t205
					   , (SELECT   t505.c205_part_number_id partnum, SUM (t505.c505_item_qty) consign
							  FROM t504_consignment t504, t505_item_consignment t505
							 WHERE t504.c504_consignment_id = t505.c504_consignment_id
							   AND t504.c701_distributor_id = p_distid
							   AND c504_type = 4110
							   AND t504.c504_status_fl = 4
							   AND c504_void_fl IS NULL
							   AND TRIM (t505.c505_control_number) IS NOT NULL
						  GROUP BY t505.c205_part_number_id) cons
					   , (SELECT   t507.c205_part_number_id partnum, SUM (c507_item_qty) cons_return
							  FROM t506_returns t506, t507_returns_item t507
							 WHERE t506.c506_rma_id = t507.c506_rma_id
							   AND t506.c701_distributor_id = p_distid
							   AND t506.c506_type IN (3301, 3302, 3306)
							   AND t506.c506_void_fl IS NULL
							   AND t506.c506_status_fl = 2
							   AND TRIM (t507.c507_control_number) IS NOT NULL
							   AND t507.c507_status_fl IN ('C', 'W')
						  GROUP BY t507.c205_part_number_id) retn
				   WHERE t205.c205_part_number_id = cons.partnum AND t205.c205_part_number_id = retn.partnum(+))
		   WHERE pending_qty > 0
		ORDER BY c205_product_family, pnum;
BEGIN
	SELECT 'GM-RA-' || s506_return.NEXTVAL
	  INTO v_rma_id
	  FROM DUAL;

	INSERT INTO t506_returns
				(c506_rma_id, c506_comments, c506_created_date, c506_type, c701_distributor_id, c506_reason
			   , c506_status_fl, c506_created_by, c506_expected_date, c506_credit_date, c506_return_date
			   , c506_update_inv_fl, c501_reprocess_date
				)
		 VALUES (v_rma_id, 'Write-Off of Sales consignment for inactive distributor', SYSDATE, 3306, p_distid, 3318
			   , 2, 30301, TRUNC (SYSDATE), TRUNC (SYSDATE), TRUNC (SYSDATE)
			   , 1, TRUNC (SYSDATE)
				);

	FOR rec IN c_get_writeup
	LOOP
		INSERT INTO t507_returns_item
					(c507_returns_item_id, c507_item_qty, c507_item_price, c205_part_number_id, c507_control_number
				   , c506_rma_id, c507_status_fl
					)
			 VALUES (s507_return_item.NEXTVAL, rec.qty, rec.price, rec.pnum, 'NOC#'
				   , v_rma_id, 'W'
					);

		gm_save_ledger_posting (4841
							  , SYSDATE
							  , rec.pnum   -- part#
							  , p_distid   -- DistributorID
							  , v_rma_id   -- RA ID
							  , rec.qty   --QTY
							  , get_ac_cogs_value (rec.pnum, 4904)
							  , '30301'
							   );
	END LOOP;

	--Create Excess Return
	tmp_excess_move (p_distid);
	
	-- Void Dummy Consignment 
	UPDATE t504_consignment
	   SET c504_void_fl = 'Y'
		 , c504_last_updated_by = 30301
		 , c504_last_updated_date = SYSDATE
	 WHERE c504_consignment_id IN (SELECT c504_consignment_id
									 FROM t504_consignment
									WHERE c701_distributor_id = p_distid AND c504_type = 4129);

--	gm_pkg_sm_lad_consignment.gm_sm_lad_consignment;   --Need to remove when in stage/Production..
END tmp_sav_inactive_dist_writeup;
/
