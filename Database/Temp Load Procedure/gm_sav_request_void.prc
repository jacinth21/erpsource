--@"C:\Database\Temp Load Procedure\gm_sav_request_void.prc"

CREATE OR REPLACE PROCEDURE  gm_sav_request_void
AS
CURSOR c_reqsav IS
   	    SELECT C503_INVOICE_ID invoiceid,C901_INVOICE_SOURCE invsource
   		FROM GLOBUS_APP.T503_INVOICE WHERE C503_STATUS_FL <>2 AND C503_VOID_FL IS NULL
  		AND GET_AC_FCH_INVOICE_AMT(C503_INVOICE_ID) = 0 
  		AND C901_INVOICE_SOURCE in('50253','50255')
  		AND C901_EXT_COUNTRY_ID IS NULL
  		ORDER BY C503_CREATED_DATE DESC ;
   	   
	   BEGIN
		   FOR v_reqlist IN c_reqsav
		   LOOP
		   		gm_pkg_ac_invoice.gm_upd_invoice_status(v_reqlist.invsource,v_reqlist.invoiceid,'303149');
		   END LOOP;
END gm_sav_request_void;
/