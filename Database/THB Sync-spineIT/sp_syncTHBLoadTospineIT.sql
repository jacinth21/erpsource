USE [bba]
GO
/****** Object:  StoredProcedure [access].[sp_syncTHBLoadTospineIT]    Script Date: 4/11/2019 9:11:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<gpalani>
-- Create date: <01-Apr-2019>
-- Description:	<This Procedure is to fetch the THB Load details and sync it to spineIT>
-- Occurance: Adhoc [DB Scheudler] weekly
-- =============================================
ALTER PROCEDURE [access].[sp_syncTHBLoadTospineIT]
	-- Add the parameters for the stored procedure here
	--<@Param1, sysname, @p1> <Datatype_For_Param1, , int> = <Default_Value_For_Param1, , 0>, 
	--<@Param2, sysname, @p2> <Datatype_For_Param2, , int> = <Default_Value_For_Param2, , 0>
AS

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--DECLARE THE VARIABLES FOR HOLDING DATA.
	DECLARE   @LoadNum INT,@LoadBoxNum VARCHAR(300),@LoadProcessedBy VARCHAR(300)
             ,@LoadProcessedDate Date,@AllograftCode VARCHAR(50),@Product_Num	VARCHAR(50)
			 ,@Donor_Num    VARCHAR(50),@Qty INT,@THB_MASTER_LOAD_ID INT
			 ,@THB_MASTER_LOAD_DETAIL_ID INT
			 ,@RN INT

-- DECLARE THE VARIABLES FOR Exception Handling.			
   DECLARE @ErrorMessage VARCHAR(8000), @ErrorLine INT, @ErrorProc VARCHAR(200), 
           @ErrorNum INT, @ErrorSeverity INT, @ErrorState INT

-- SET COUNTER.           
	SET @QTY=1
	
     BEGIN TRY

	   DECLARE FETCHMASTERLOAD CURSOR READ_ONLY

		FOR
		
	  -- Fetch THB Load Number (Distinct) from THB Allograft Tracking Table.
	  -- By using Distinct, the problem is if multiple user add/Modify allograft to same load/box number there may be a duplicate
				/*  
		  SELECT DISTINCT(allografts.LoadNumber),allografts.LoadBoxId,allografts.RecordLastModifiedBy,allografts.DateLoad 
		  FROM THB.vu_AllograftTracking AS allografts
		  WHERE 
		    --   allografts.spineITSyncFl IS NULL
			allografts.LoadNumber='19033'
		   AND allografts.LoadBoxId IS NOT NULL
		   ORDER BY allografts.LoadNumber,allografts.LoadBoxId
		*/
		
		
    -- The below query is added to avoid the duplicate records when using Distinct
	
			WITH Allograft AS
		(   SELECT allografts_load.LoadNumber,allografts_load.LoadBoxId,allografts_load.RecordLastModifiedBy,allografts_load.DateLoad , 
		    ROW_NUMBER() OVER (PARTITION BY allografts_load.LoadNumber,allografts_load.LoadBoxId ORDER BY allografts_load.LoadNumber
			,allografts_load.LoadBoxId DESC) AS rn
			FROM THB.vu_AllograftTracking AS allografts_load
			WHERE allografts_load.spineITSyncFl IS NULL
		)
			SELECT *
			FROM Allograft
			WHERE rn = 1
      
     OPEN FETCHMASTERLOAD
	    FETCH NEXT FROM FETCHMASTERLOAD INTO
		@LoadNum, @LoadBoxNum, @LoadProcessedBy,@LoadProcessedDate, @RN
		   
--LOOP UNTIL RECORDS ARE AVAILABLE.
	    WHILE @@FETCH_STATUS = 0
   
	BEGIN   
	-- Inserting in the spineIT Master table.
	
		 INSERT INTO [GMITESTDB]..[GLOBUS_APP].[T2560_THB_LOAD] 
		  (C2560_THB_LOAD_NUM,C2560_THB_BOX_NUM,C2560_THB_PROCESSED_BY,C2560_THB_PROCESSED_DATE,
		  C901_LOAD_STATUS) VALUES (@LoadNum,@LoadBoxNum,@LoadProcessedBy,@LoadProcessedDate,107880)
		
-- Fetching the Master Load ID which is created in the above script.	       	
	       SELECT   @THB_MASTER_LOAD_ID=(C2560_THB_LOAD_ID)   FROM [GMITESTDB]..[GLOBUS_APP].[T2560_THB_LOAD] THB_LOAD  
		   WHERE    C2560_THB_LOAD_NUM=@LoadNum AND C2560_THB_BOX_NUM=@LoadBoxNum

    DECLARE FETCHDETAILLOAD CURSOR READ_ONLY
	
    	FOR
	-- -- Fetching the Allograft, Part Num for the 'nth' Box for the Load number.
			 SELECT allografts.Allograftcode,  products.ProductNo,donor.RecoveryAgencyDonorNumber
			 FROM [THB-APP-SRV1].THB.dbo.[tblAllograftIDNoTracking] allografts, [THB-APP-SRV1].THB.dbo.[tblProducts]
			  products, [THB-APP-SRV1].THB.dbo.[tblDonor] donor
	           WHERE 
			       allografts.LoadBoxId=@LoadBoxNum
			   AND allografts.LoadNumber=CAST (@LoadNum AS VARCHAR(50))
               AND allografts.DonorId = donor.id
               AND products.id= allografts.ProductId
			   and products.ProductNo<>'QC'  -- Excluding this part as it is included in the THB Load for QC but this part is not in spineIT
               ORDER BY allografts.LoadNumber;
	 
	OPEN FETCHDETAILLOAD

		FETCH NEXT FROM FETCHDETAILLOAD INTO
	      @AllograftCode, @Product_Num,@Donor_Num
	 
--LOOP UNTIL RECORDS ARE AVAILABLE.THB0852533

	 WHILE @@FETCH_STATUS = 0	  		  
   
   BEGIN
   
   	  INSERT INTO [GMITESTDB]..[GLOBUS_APP].[T2561_THB_LOAD_ITEM_DETAIL] 
		  (C2560_THB_LOAD_ID,C205_PART_NUMBER_ID,C2561_CONTROL_NUMBER,C2561_DONOR_NUMBER
		  ,C2561_THB_QTY
		  ) VALUES (@THB_MASTER_LOAD_ID,@Product_Num,@AllograftCode,@Donor_Num,@QTY);
		  
  
  -- update query to change the flag in THB
	  UPDATE  [THB-APP-SRV1].THB.dbo.tblAllograftIDNoTracking 
		 SET [spineITSyncFl]='Y'
	  WHERE loadNumber=@LoadNum 
	  AND LoadBoxId=@LoadBoxNum 
	  AND [allograftcode]=@AllograftCode
	  

	FETCH NEXT FROM FETCHDETAILLOAD INTO
      @AllograftCode, @Product_Num, @Donor_Num 
   END
 CLOSE FETCHDETAILLOAD
DEALLOCATE FETCHDETAILLOAD
-- End Of inner cursor

-- Outer loop starts [Fetch next in the loop]
FETCH NEXT FROM FETCHMASTERLOAD INTO
  @LoadNum, @LoadBoxNum, @LoadProcessedBy,@LoadProcessedDate, @RN
  
     END
   CLOSE FETCHMASTERLOAD
  DEALLOCATE FETCHMASTERLOAD

 END TRY
 
 -- Exception Handling
	BEGIN CATCH
		IF @@TRANCOUNT <> 0
		BEGIN
		print ' Transaction ROLLBACK for Load Number';	
			ROLLBACK TRANSACTION
		END
			
		SELECT @ErrorNum = ERROR_NUMBER(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE(), @ErrorProc = ERROR_PROCEDURE(),	@ErrorLine = ERROR_LINE(), @ErrorMessage = ERROR_MESSAGE() + ' Procedure:%s'
		RAISERROR (@ErrorMessage, @ErrorSeverity, 7, @ErrorProc)-- WITH LOG
	END CATCH;
	
END
