--@"c:\database\packages\clinical\gm_pkg_cr_enroll_rpt.bdy";
CREATE OR REPLACE PACKAGE BODY gm_pkg_cr_enroll_rpt
IS
--
/*******************************************************
 * Purpose: Package holds Clinical Research
 * Report Methods
 * Created By VPrasath
 *******************************************************/
--
/*******************************************************
 * Purpose: This Procedure is used to fetch
 * enrolled patients count by month
 *******************************************************/
--
   PROCEDURE gm_cr_fch_enroll_site_month (
      p_studyid           IN       t614_study_site.c611_study_id%TYPE,
      p_fromdate          IN       VARCHAR2,
      p_todate            IN       VARCHAR2,
      p_header_cur        OUT      TYPES.cursor_type,
      p_sitelist_cur      OUT      TYPES.cursor_type,
      p_sitedetails_cur   OUT      TYPES.cursor_type
   )
   AS
   BEGIN
      OPEN p_header_cur
       FOR
          SELECT DISTINCT TO_CHAR (month_value, 'Mon YY') monyy, month_value
                     FROM v9001_month_list vmonthlist
                    WHERE vmonthlist.month_value >=
                                           TO_DATE (p_fromdate, 'MM/DD/YYYY')
                      AND vmonthlist.month_value <=
                                              TO_DATE (p_todate, 'MM/DD/YYYY')
                      AND vmonthlist.month_value >=
                                       (SELECT t611.c611_start_date
                                          FROM t611_clinical_study t611
                                         WHERE t611.c611_study_id = p_studyid)
                 ORDER BY month_value;

      OPEN p_sitelist_cur
       FOR
          SELECT   c614_site_id ID, c614_site_id NAME,
                   TO_CHAR (sitelist.month_value, 'Mon YY') monyy,
                   DECODE
                      (scount,
                       NULL, DECODE (SIGN (  sitelist.month_value
                                           - sitelist.c614_irb_approval_date
                                          ),
                                     -1, '*',
                                     0
                                    ),
                       scount
                      ) amount
              FROM (SELECT   c704_account_id acc_id,
                             TO_DATE (   '01/'
                                      || TO_CHAR (c621_surgery_date,
                                                  'MM/YYYY'),
                                      'DD/MM/YYYY'
                                     ) smonyy,
                             COUNT (1) scount
                        FROM t621_patient_master t621
                       WHERE c621_surgery_date IS NOT NULL
                         AND c621_delete_fl = 'N'
                         AND c704_account_id IS NOT NULL
                         AND c621_surgery_comp_fl IS NOT NULL
                         AND c611_study_id = p_studyid 
                    GROUP BY c704_account_id,
                             TO_DATE (   '01/'
                                      || TO_CHAR (c621_surgery_date,
                                                  'MM/YYYY'),
                                      'DD/MM/YYYY'
                                     )) sitepatientlist,
                   (SELECT c704_account_id acc_id, c614_irb_approval_date,
                           c614_site_id, month_value
                      FROM t614_study_site, v9001_month_list vmonthlist
                     WHERE c614_void_fl IS NULL
                       AND c611_study_id = p_studyid
                       AND c614_irb_approval_date IS NOT NULL
                       AND vmonthlist.month_value >=
                                       (SELECT t611.c611_start_date
                                          FROM t611_clinical_study t611
                                         WHERE t611.c611_study_id = p_studyid)
                       AND vmonthlist.month_value >=
                                            TO_DATE (p_fromdate, 'MM/DD/YYYY')
                       AND vmonthlist.month_value <=
                                              TO_DATE (p_todate, 'MM/DD/YYYY')) sitelist
             WHERE sitelist.acc_id = sitepatientlist.acc_id(+)
                   AND sitelist.month_value = sitepatientlist.smonyy(+)
          --ORDER BY c614_site_id, sitelist.month_value
          union
          SELECT c614_site_id ID, c614_site_id NAME,'' monyy,'' amount from t614_study_site where c614_irb_approval_date IS  NULL 
          and c611_study_id = p_studyid and c614_void_fl IS NULL order by 1,2;

      OPEN p_sitedetails_cur
       FOR
          SELECT   c614_site_id ID,
                   get_sh_account_name (t614.c704_account_id) sname,
                   get_cr_fch_prinicipal_inv (c614_study_site_id) pinv
              FROM t614_study_site t614
             WHERE t614.c614_void_fl IS NULL
                   AND t614.c611_study_id = p_studyid
          GROUP BY c614_site_id,
                   t614.c704_account_id,
                   c614_study_site_id,
                   t614.c611_study_id;
   END gm_cr_fch_enroll_site_month;

--
/*******************************************************
 * Purpose: This Procedure is used to fetch
 * the details of the surgery planned
 *******************************************************/
--
   PROCEDURE gm_cr_fch_plan_surgery (
      p_studyid           IN       t614_study_site.c611_study_id%TYPE,
      p_acc_id            IN       VARCHAR2,
      p_surgery_type      IN       t621_patient_master.c901_surgery_type%TYPE,
      p_status            IN       VARCHAR2,
      p_surgeryplan_cur   OUT      TYPES.cursor_type
   )
   AS
   BEGIN
      -- To assign Accounts Information
      my_context.set_my_inlist_ctx (p_acc_id);

      OPEN p_surgeryplan_cur
       FOR
          SELECT   c621_patient_ide_no ID,
                   get_cr_fch_prinicipal_inv (t614.c614_study_site_id) pinv,
                   get_party_name (c101_surgeon_id) sname,
                   get_code_name (c901_surgery_type) treatment,
                   TO_CHAR (c621_surgery_date, 'MM/DD/YYYY') sdate,
                   get_log_flag (c621_patient_ide_no, 1229) clog
              FROM t621_patient_master t621, t614_study_site t614
             WHERE t621.c611_study_id = t614.c611_study_id
               AND t621.c704_account_id = t614.c704_account_id
               AND c621_surgery_date IS NOT NULL
               AND c621_delete_fl = 'N'
               AND t614.c614_void_fl IS NULL
               AND t621.c704_account_id IS NOT NULL
               AND t614.c614_study_site_id IN (SELECT token
                                                 FROM v_in_list)
               AND t621.c611_study_id = p_studyid
               AND c901_surgery_type =
                      DECODE (p_surgery_type,
                              '', c901_surgery_type,
                              p_surgery_type
                             )
               AND NVL (c621_surgery_comp_fl, -999) =
                      DECODE (p_status,
                              '60110', 'Y',
                              '60111', -999,
                              NVL (c621_surgery_comp_fl, -999)
                             )
          ORDER BY c621_patient_ide_no;
   END gm_cr_fch_plan_surgery;

--
/*******************************************************
 * Purpose: This Procedure is used to fetch
 * enrolled patients count by treatment
 *******************************************************/
--
   PROCEDURE gm_cr_fch_enroll_treatment (
      p_studyid           IN       t614_study_site.c611_study_id%TYPE,
      p_header_cur        OUT      TYPES.cursor_type,
      p_sitelist_cur      OUT      TYPES.cursor_type,
      p_sitedetails_cur   OUT      TYPES.cursor_type
   )
   AS
   BEGIN
     
      gm_pkg_cm_codelookup_util.gm_cm_fch_allcodelist(p_studyid, p_header_cur);
     

      OPEN p_sitelist_cur
       FOR
	 		  SELECT t614.c614_site_id ID, t614.c614_site_id NAME,
					   get_code_name(t621.c901_surgery_type) codenm, count(t621.c901_surgery_type) count 
				 FROM t614_study_site t614, 
				      t611_clinical_study t611, 
				      t621_patient_master t621 
				WHERE t614.c611_study_id = t611.c611_study_id
				  AND t611.c611_study_id = t621.c611_study_id 
				  AND t621.c704_account_id = t614.c704_account_id
				  AND c621_surgery_date IS NOT NULL
				  AND c621_delete_fl = 'N'
				  AND c621_surgery_comp_fl IS NOT NULL
				  AND t614.c614_void_fl IS NULL
				  AND t621.c704_account_id IS NOT NULL
				  AND t621.c611_study_id = p_studyid
			GROUP BY t614.c614_site_id, t621.c704_account_id, t614.c614_study_site_id, c901_surgery_type
			--ORDER BY ID, NAME, codenm
			 UNION
 				SELECT t614.c614_site_id ID, t614.c614_site_id NAME, '' codenm
  				, to_number (NULL) COUNT
   				FROM t614_study_site t614
  				WHERE t614.c614_void_fl   IS NULL
    			AND t614.c611_study_id   = p_studyid
   				 AND c704_account_id NOT IN
    			(
        		SELECT c704_account_id
          		FROM t621_patient_master
          		WHERE c611_study_id         = p_studyid
            	AND c621_delete_fl        = 'N'
            	AND c621_surgery_comp_fl IS NOT NULL 
            	AND c621_surgery_date IS NOT NULL
    			) order by 1,2,3;   
			    
     OPEN p_sitedetails_cur
       FOR
          SELECT   c614_site_id ID,
                   get_sh_account_name (t614.c704_account_id) sname,
                   get_cr_fch_prinicipal_inv (c614_study_site_id) pinv
              FROM t614_study_site t614
             WHERE t614.c614_void_fl IS NULL
                   AND t614.c611_study_id = p_studyid
          GROUP BY c614_site_id,
                   t614.c704_account_id,
                   c614_study_site_id,
                   t614.c611_study_id;
   END gm_cr_fch_enroll_treatment;


END gm_pkg_cr_enroll_rpt; 
/