/* Formatted on 2010/09/16 09:55 (Formatter Plus v4.8.0) */
--- @"c:\database\packages\clinical\gm_pkg_cr_void_clinical_form.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_cr_void_clinical_form
IS
/*******************************************************
 * Purpose: Package holds Clinical Research
 * Save and Fetch Methods
 * Created By : 
 *******************************************************/

 /*******************************************************
   * Description : Procedure to update void form 
   * Author   	 : Kulanthaivelu
 *******************************************************/
   
PROCEDURE	 gm_cr_sav_void_form(
  		p_patient_list_id	IN  t622_patient_list.c622_patient_list_id%TYPE
  	    ,p_userid           IN  t102_user_login.c102_user_login_id%TYPE
  	   )
AS 
  	   v_delete_fl	   t622_patient_list.c622_delete_fl%TYPE;
  	   v_patient_ans_list_id t6514_ec_incident.c623_patient_ans_list_id%type;

  BEGIN
	
	  SELECT c622_delete_fl INTO v_delete_fl 
	  FROM t622_patient_list 
	  WHERE  c622_patient_list_id = p_patient_list_id 
	  FOR UPDATE;
  
	  
	  IF v_delete_fl ='Y'
		THEN
			raise_application_error (-20129, '');
		ELSE

		UPDATE t6514_ec_incident
			SET c6514_void_fl = 'Y'
		 	, c6514_last_updated_by = p_userid
			, c6514_last_updated_date = SYSDATE
	    WHERE  (c623_patient_ans_list_id in(SELECT c623_patient_ans_list_id 
		  	FROM t623_patient_answer_list
  			WHERE c622_patient_list_id = p_patient_list_id)
  			OR
  			C623_LINKED_PT_ANS_LIST_ID in (SELECT c623_patient_ans_list_id 
		  	FROM t623_patient_answer_list
  			WHERE c622_patient_list_id = p_patient_list_id)
  			);
		
		UPDATE t6510_query 
			SET c6510_void_fl = 'Y'
			 , c6510_last_updated_by = p_userid
			 , c6510_last_updated_date = SYSDATE
	    WHERE c622_patient_list_id   = p_patient_list_id;
		 
		UPDATE t622_patient_list
			   SET c622_delete_fl = 'Y'
				 , c622_last_updated_by = p_userid
				 , c622_last_updated_date = SYSDATE
	    WHERE c622_patient_list_id   = p_patient_list_id;
	    
	    UPDATE t623_patient_answer_list
			   SET c623_delete_fl = 'Y'
			     , C623_EDIT_CHECK_FL = ''
				 , c623_last_updated_by = p_userid
				 , c623_last_updated_date = SYSDATE
			 WHERE c622_patient_list_id  =  p_patient_list_id;
			 
	   UPDATE t625_patient_Verify_list
			   SET c625_delete_fl ='Y' 
				 , c625_last_updated_by = p_userid
				 , c625_last_updated_date = SYSDATE
			 WHERE c622_patient_list_id   =  p_patient_list_id 
             AND NVL(c625_delete_fl,'N') = 'N';
		END  IF;

END gm_cr_sav_void_form;
	
/*******************************************************
   * Description : Procedure to fetch void form report. 
   * Author   	 : Kulanthaivelu
 *******************************************************/
 	
	PROCEDURE gm_fch_void_report_form(
	 	p_studyid			IN		 t6512_edit_check.c611_study_id%TYPE
	   ,p_studysiteid		IN		 VARCHAR2
	   ,p_patientid	 		IN		 t621_patient_master.c621_patient_ide_no%TYPE
	   ,p_periodid			IN	  	 t613_study_period.c901_study_period%TYPE
	   ,p_formid			IN		 t601_form_master.c601_form_id%TYPE
	   ,p_strDateFrom 		IN 		 VARCHAR2
	   ,p_strDateTo			IN 		 VARCHAR2
	   ,p_ReasonForClosing	IN		 VARCHAR2
	   ,p_LastUpdateBy	 	IN 		 VARCHAR2
 	   ,p_recordset 		OUT 	 TYPES.cursor_type
	)
	
	AS
		v_code_id 		t901_code_lookup.c901_code_id%TYPE;
		
	BEGIN
	
		SELECT c901_code_id 
		  INTO v_code_id
		  FROM t901_code_lookup
		 WHERE c902_code_nm_alt = 'VDFORM';

		OPEN p_recordset
		 FOR
	
	     SELECT  c907_ref_id,t614.c614_site_id ||' - '|| get_sh_account_name(t614.c704_account_id) SITE_ID,
	        t622.C601_FORM_ID FORMID,
	        get_form_ds(t622.C601_FORM_ID,t614.c611_study_id)||' - '||get_form_name(t622.C601_FORM_ID) FORMNAME,
	        t621.C621_PATIENT_ID PATID,t613.c901_study_period STUDY_PERIOD,
	        T621.C621_PATIENT_IDE_NO PATIENT_IDE,
	        get_code_name(t613.c901_study_period) STUDY_PERIOD_NM,
	        t622.C613_STUDY_PERIOD_ID STDPERID,
	        t622.C622_PATIENT_LIST_ID PAT_LIST_ID, 
	        NVL(t622.C622_PATIENT_EVENT_NO,'0') EVENT_NO,  
	        TRANSLATE (t907.c907_comments, CHR(10)||CHR(13), ' ') COMMENTS, get_user_name (c907_created_by) CREATEDBY,
	        TO_CHAR (c907_cancel_date, 'MM/DD/YYYY') CANCELDATE ,
	        get_code_name(t907.c901_cancel_cd) REASON  -- New Column included, which is showing on the Voided Forms Report Screen as part of MNTTASK-3766
          	FROM t622_patient_list t622 ,  
            t621_patient_master t621,
            t614_study_site t614,
            t613_study_period t613,
          	t907_cancel_log t907
			WHERE t622.c622_delete_fl = 'Y'
		    AND t621.c621_patient_id = t622.c621_patient_id
		    AND t613.c613_study_period_id = t622.c613_study_period_id
        	AND t621.c611_study_id = t614.c611_study_id
        	AND t614.c704_account_id = t621.c704_account_id
			AND t622.C601_FORM_ID = DECODE(p_formid,'0',t622.C601_FORM_ID,p_formid)
			AND t622.c621_patient_id = NVL(p_patientid,t622.c621_patient_id)
			AND t621.c611_study_id = DECODE(p_studyid,'0',t621.c611_study_id,p_studyid) 
			AND t614.c614_site_id = NVL(p_studysiteid,t614.c614_site_id)
			AND t613.c901_study_period =DECODE(p_periodid,'0',t613.c901_study_period,p_periodid) 
			AND t907.c901_type = v_code_id
			AND t907.c907_ref_id = t622.c622_patient_list_id
			AND t907.c901_cancel_cd = DECODE(p_ReasonForClosing,'0',t907.c901_cancel_cd, p_ReasonForClosing) 
			AND t907.c907_created_by =  DECODE(p_LastUpdateBy,'0', t907.c907_created_by, p_LastUpdateBy)		
    		AND t907.c907_cancel_date BETWEEN NVL (TO_DATE (p_strDateFrom, 'MM/DD/YYYY'), t907.c907_cancel_date)
			AND NVL (TO_DATE (p_strDateTo, 'MM/DD/YYYY'), t907.c907_cancel_date)
			AND t907.c907_void_fl IS  NULL;
			
   END gm_fch_void_report_form;
   
   /*****************************************************************
      * Description         : Procedure to UnVoid the Adverse Form. 
      * Author   	        : Hreddi
      * Added date - Reason : 21st Aug 2012 as part of MNTTASK-3766
    *****************************************************************/
   PROCEDURE gm_un_void_report_form(
   		p_patientlistid   IN   	T622_PATIENT_LIST.C622_PATIENT_LIST_ID%TYPE
   	  , p_patientid       IN    T622_PATIENT_LIST.C621_PATIENT_ID%TYPE
   	  , p_formid		  IN    T622_PATIENT_LIST.C601_FORM_ID%TYPE
   	  , p_userid          IN    T622_PATIENT_LIST.C622_LAST_UPDATED_BY%TYPE
     ) 
   AS 	
   		v_code_id 		t901_code_lookup.c901_code_id%TYPE;
   		v_id            NUMBER;
   		v_msg           VARCHAR2 (1000);
   BEGIN	  
	   SELECT c901_code_id 
		  INTO v_code_id
		  FROM t901_code_lookup
		 WHERE c902_code_nm_alt = 'VDFORM';
		 
	  	UPDATE T622_PATIENT_LIST  
	  	   		SET C622_DELETE_FL         = 'N' 
	  		   	  ,	C622_LAST_UPDATED_BY   = p_userid 
	  		   	  ,	C622_LAST_UPDATED_DATE = SYSDATE
	  	 	  WHERE C622_PATIENT_LIST_ID   = p_patientlistid 
	  	   		AND C621_PATIENT_ID        = p_patientid 
	  	   		AND C601_FORM_ID           = p_formid
	  	   		AND C622_DELETE_FL		   = 'Y';	  	
	  		  
	  	IF(SQL%ROWCOUNT = 0)		
	  	   THEN 
	    	  raise_application_error (-99999, 'Unable to Un-void  the Form/Data,Please contact Administrator');
	    END IF;
	   
	  	UPDATE T623_PATIENT_ANSWER_LIST  
	  		   SET C623_DELETE_FL 		  = 'N' 
	  		     , C623_LAST_UPDATED_BY   = p_userid 
	  		     , C623_LAST_UPDATED_DATE = SYSDATE
	  		 WHERE C622_PATIENT_LIST_ID   = p_patientlistid 
	  		   AND C621_PATIENT_ID        = p_patientid 
	  		   AND C601_FORM_ID           = p_formid
	  		   AND C623_DELETE_FL         = 'Y';
	   
	    IF( SQL%ROWCOUNT = 0)		
	       THEN 
	    	  raise_application_error (-99999, 'Unable to Un-void  the Form/Data,Please contact Administrator');
	    END IF;
	    
	    UPDATE t907_cancel_log 
	       SET c907_void_fl				= 'Y'
	         , C907_LAST_UPDATED_BY   	= p_userid 
	  		 , C907_LAST_UPDATED_DATE 	= SYSDATE 
	     WHERE to_char(c907_ref_id)     = p_patientlistid 
	       AND c907_void_fl 			is null 
	       AND to_char(c901_type)       = v_code_id;
	       
	        --
   SELECT s625_patient_verify.NEXTVAL
     INTO v_id
     FROM DUAL;

	--
   INSERT INTO t625_patient_verify_list
               (c625_pat_verify_list_id, c622_patient_list_id, c101_user_id,
                c625_date, c901_verify_level
               )
        VALUES (v_id, p_patientlistid, p_userid,
                TRUNC (SYSDATE), '6196'
               );

   --
   -- Code to save the user entered comments information
   --
      
   gm_update_log (v_id, 'Form Unvoided', 1204, p_userid, v_msg);

   END gm_un_void_report_form ;
	
END gm_pkg_cr_void_clinical_form;
/
