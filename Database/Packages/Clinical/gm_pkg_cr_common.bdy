/* Formatted on 2010/09/16 09:55 (Formatter Plus v4.8.0) */
-- @"c:\database\packages\clinical\gm_pkg_cr_common.bdy" ;

CREATE OR REPLACE PACKAGE BODY gm_pkg_cr_common
IS
/*******************************************************
 * Purpose: Package holds Clinical Research
 * Save and Fetch Methods
 * Created By VPrasath
 *******************************************************/
--
/*******************************************************
 * Purpose: This Procedure is used to save the
 * sites mapped to the study
 *******************************************************/
--
	PROCEDURE gm_cr_sav_site_map_info (
		p_studyid			 IN 	  t614_study_site.c611_study_id%TYPE
	  , p_accid 			 IN 	  t614_study_site.c704_account_id%TYPE
	  , p_siteid			 IN 	  t614_study_site.c614_site_id%TYPE
	  , p_craid 			 IN 	  t614_study_site.c101_user_id%TYPE
	  , p_userid			 IN 	  t614_study_site.c614_created_by%TYPE
	  , p_irbtype			 IN 	  t614_study_site.c901_irb_type%TYPE
	  , p_stdypktdt 		 IN 	  VARCHAR2
	  , p_tin_no   			 IN 	  VARCHAR2
	  , p_study_siteid_out	 OUT	  VARCHAR2
	)
	AS
		v_siteid_cnt   NUMBER;
	BEGIN
		UPDATE t614_study_site
		   SET c611_study_id = p_studyid
			 , c704_account_id = p_accid
			 , c614_site_id = p_siteid
			 , c101_user_id = p_craid
			 , c614_last_updated_by = p_userid
			 , c901_irb_type = p_irbtype
			 , c614_study_pkt_date = TO_DATE (p_stdypktdt, 'MM/DD/YYYY')
			 , c614_last_updated_date = SYSDATE
			 , c614_tin_number=p_tin_no 
		 WHERE c611_study_id = p_studyid AND c704_account_id = p_accid AND c614_void_fl IS NULL;

		IF (SQL%ROWCOUNT = 0)
		THEN
			SELECT s614_study_site.NEXTVAL
			  INTO p_study_siteid_out
			  FROM DUAL;

			INSERT INTO t614_study_site
						(c614_study_site_id, c611_study_id, c704_account_id, c614_site_id, c101_user_id
					   , c614_created_by, c614_created_date, c901_irb_type, c614_study_pkt_date,c614_tin_number
						)
				 VALUES (p_study_siteid_out, p_studyid, p_accid, p_siteid, p_craid
					   , p_userid, SYSDATE, p_irbtype, TO_DATE (p_stdypktdt, 'MM/DD/YYYY'), p_tin_no
						);
		ELSE
			SELECT c614_study_site_id
			  INTO p_study_siteid_out
			  FROM t614_study_site
			 WHERE c611_study_id = p_studyid AND c704_account_id = p_accid AND c614_void_fl IS NULL;
		END IF;

		SELECT COUNT (c614_study_site_id)
		  INTO v_siteid_cnt
		  FROM t614_study_site
		 WHERE c614_site_id = p_siteid AND c611_study_id = p_studyid AND c614_void_fl IS NULL;

		IF v_siteid_cnt > 1
		THEN
			raise_application_error ('-20053', '');
		END IF;
		--existing parameter study site id passed to update site mapping changes to v621_patient_study_list
		gm_pkg_cr_common.gm_sav_site_patient_details(NULL,p_study_siteid_out,NULL);
	END gm_cr_sav_site_map_info;

--
--
/*******************************************************
 * Purpose: This Procedure is used to fetch the
 * sites mapped to the study
 *******************************************************/
--
	PROCEDURE gm_cr_fch_site_map_info (
		p_studyid	 IN 	  t614_study_site.c611_study_id%TYPE
	  , p_site_cur	 OUT	  TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_site_cur
		 FOR
			 SELECT   c614_study_site_id ID, c614_site_id siteid, get_account_name (c704_account_id) sitename
					, TO_CHAR (c614_irb_approval_date, 'MM/DD/YYYY') irbapprovaldate
					, get_user_name (c101_user_id) craname
				 FROM t614_study_site
				WHERE c611_study_id = p_studyid AND c614_void_fl IS NULL
			 ORDER BY siteid;
	END gm_cr_fch_site_map_info;

--
/*******************************************************
 * Purpose: This Procedure is used to fetch the
 * study site details for the given study site id
 *******************************************************/
--
	PROCEDURE gm_cr_fch_site_map (
		p_study_site_id   IN	   t614_study_site.c614_study_site_id%TYPE
	  , p_site_cur		  OUT	   TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_site_cur
		 FOR
			 SELECT c614_study_site_id hsitemapid, c611_study_id studylist, c614_site_id siteid
				  , c704_account_id sitenamelist, TO_CHAR (c614_irb_approval_date, 'MM/DD/YYYY') approvaldate
				  , c101_user_id craname
			   FROM t614_study_site
			  WHERE c614_study_site_id = p_study_site_id AND c614_void_fl IS NULL;
	END gm_cr_fch_site_map;

/*******************************************************
 * Purpose: This Procedure is used to fetch all the sites
 * mapped to the study
 *******************************************************/
--
	PROCEDURE gm_cr_fch_site_list (
		p_study_id	 IN 	  t614_study_site.c611_study_id%TYPE
	  , p_site_cur	 OUT	  TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_site_cur
		 FOR
			 SELECT   c614_study_site_id ID, c614_site_id || '-' || get_sh_account_name (c704_account_id) NAME
				 FROM t614_study_site
				WHERE c611_study_id = p_study_id AND c614_void_fl IS NULL
			 ORDER BY c614_site_id;
	END gm_cr_fch_site_list;

--
/*******************************************************
 * Purpose: This Procedure is used to save the
 * Surgeons mapped to the site
 *******************************************************/
--
	PROCEDURE gm_cr_sav_surgeon_map_info (
		p_study_id		  IN   t614_study_site.c611_study_id%TYPE
	  , p_study_site_id   IN   t614_study_site.c614_study_site_id%TYPE
	  , p_firstname 	  IN   t101_party.c101_first_nm%TYPE
	  , p_lastname		  IN   t101_party.c101_last_nm%TYPE
	  , p_party_id		  IN   t101_party.c101_party_id%TYPE
	  , p_surgeontype	  IN   t615_site_surgeon.c901_surgeon_type%TYPE
	  , p_userid		  IN   t615_site_surgeon.c615_created_by%TYPE
	)
	AS
--
		v_party_id	   t101_party.c101_party_id%TYPE;
		v_surgeontype_cnt NUMBER;
		v_cnt		   NUMBER;
		v_new_party_id	   t101_party.c101_party_id%TYPE;
--
	BEGIN
		
		SELECT DECODE (p_party_id, 0, NULL, p_party_id) INTO v_new_party_id FROM DUAL;
		
		gm_pkg_cm_party.gm_cm_sav_party_id (v_new_party_id, p_firstname, p_lastname, '', '', 7000, '', p_userid
										  , v_party_id);

		IF TRIM (v_new_party_id) IS NOT NULL AND TRIM (v_party_id) IS NULL
		THEN
			v_party_id	:= v_new_party_id;
		END IF;

		UPDATE t615_site_surgeon
		   SET c614_study_site_id = p_study_site_id
			 , c101_party_id = v_party_id
			 , c901_surgeon_type = p_surgeontype
			 , c615_last_updated_by = p_userid
			 , c615_last_updated_date = SYSDATE
		 WHERE c101_party_id = v_party_id
		   AND c614_study_site_id IN (SELECT c614_study_site_id
										FROM t614_study_site
									   WHERE c611_study_id = p_study_id)
		   AND c615_void_fl IS NULL;

		IF (SQL%ROWCOUNT = 0)
		THEN
			INSERT INTO t615_site_surgeon
						(c615_site_surgeon_id, c614_study_site_id, c101_party_id, c901_surgeon_type, c615_created_by
					   , c615_created_date
						)
				 VALUES (s615_site_surgeon.NEXTVAL, p_study_site_id, v_party_id, p_surgeontype, p_userid
					   , SYSDATE
						);
		END IF;

		SELECT COUNT (c901_surgeon_type)
		  INTO v_surgeontype_cnt
		  FROM t615_site_surgeon
		 WHERE c614_study_site_id = p_study_site_id AND c901_surgeon_type = 60200 AND c615_void_fl IS NULL;

		IF v_surgeontype_cnt > 1
		THEN
			raise_application_error (-20054, '');
		END IF;
	END gm_cr_sav_surgeon_map_info;

--
/*******************************************************
 * Purpose: This Procedure is used to fetch all the
 * surgeons mapped to the site
 *******************************************************/
--
	PROCEDURE gm_cr_fch_surgeon_map_details (
		p_study_site_id   IN	   t614_study_site.c614_study_site_id%TYPE
	  , p_surgeon_cur	  OUT	   TYPES.cursor_type
	)
	AS
	BEGIN
		gm_cr_fch_surgeon_map_details (p_study_site_id, NULL, p_surgeon_cur);
	END gm_cr_fch_surgeon_map_details;

	PROCEDURE gm_cr_fch_surgeon_map_details (
		p_study_site_id   IN	   t614_study_site.c614_study_site_id%TYPE
	  , p_surgeontype	  IN	   t615_site_surgeon.c901_surgeon_type%TYPE
	  , p_surgeon_cur	  OUT	   TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_surgeon_cur
		 FOR
			 SELECT c615_site_surgeon_id SID, c101_party_id ID, get_party_name (c101_party_id) NAME
				  , get_code_name (c901_surgeon_type) surgeontype
			   FROM t615_site_surgeon
			  WHERE c614_study_site_id = p_study_site_id
				AND c901_surgeon_type = NVL (p_surgeontype, c901_surgeon_type)
				AND c615_void_fl IS NULL;
	END gm_cr_fch_surgeon_map_details;

--
/*******************************************************
 * Purpose: This Procedure is used to fetch the
 * surgeon mapped to the study and the site
 *******************************************************/
--
	PROCEDURE gm_cr_fch_surgeon_map_info (
		p_site_surgeon_id	IN		 t615_site_surgeon.c615_site_surgeon_id%TYPE
	  , p_surgeon_cur		OUT 	 TYPES.cursor_type
	)
	AS
--
		v_study_site_id t614_study_site.c614_study_site_id%TYPE;
--
	BEGIN
		OPEN p_surgeon_cur
		 FOR
			 SELECT c611_study_id studylist, t615.c614_study_site_id sitenamelist, t101.c101_party_id surgeonnamelist
				  , t101.c101_first_nm firstname, t101.c101_last_nm lastname, c901_surgeon_type surgeontype
			   FROM t615_site_surgeon t615, t101_party t101, t614_study_site t614
			  WHERE t615.c101_party_id = t101.c101_party_id
				AND t615.c614_study_site_id = t614.c614_study_site_id
				AND c615_site_surgeon_id = p_site_surgeon_id;
	END gm_cr_fch_surgeon_map_info;

--
/***************************************************************************************
 * Description	 :This procedure is called to FETCH the Pending Forms list of form that need to be filled
 * Parameters	 :N/A
 *				 :p_recordset	- holds list of list of due form for the entire patient
 **************************************************************************************/
--
	PROCEDURE gm_pending_form_list (
		p_study_id	  IN	   t611_clinical_study.c611_study_id%TYPE
	  , p_cra_id	  IN	   t101_user.c101_user_id%TYPE
	  , p_rsetforms   OUT	   TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_rsetforms
		 FOR
			 SELECT   v621.c611_study_id s_id, v621.c613_study_period_ds s_ds, v621.c901_study_period s_p_id
					, v621.c613_study_period_id s_p_key, c704_account_id a_id
					,	 DECODE (v621.c614_site_id, '', '', v621.c614_site_id || '-')
					  || get_account_name (v621.c704_account_id)
					  || DECODE (v621.c101_user_id, '', '', '-' || get_user_name (v621.c101_user_id)) a_name
					, v621.c621_patient_ide_no p_ide_no, v621.c621_patient_id p_key, v621.c601_form_id f_id
					, v621.c612_study_form_ds || ' - ' || get_form_name (v621.c601_form_id) f_name
					, TO_CHAR (c621_exp_date_start, 'MM/DD/YYYY') exp_start_date
					, TO_CHAR (v621.c621_exp_date, 'MM/DD/YYYY') exp_date
					, TO_CHAR (v621.c621_exp_date_final, 'MM/DD/YYYY') final_due_date
					, DECODE (SIGN (SYSDATE - DECODE (c621_exp_date_final, '', SYSDATE, c621_exp_date_final))
							, 1, 'Y'
							, 'N'
							 ) over_due
					, c612_seq seq
				 FROM v621_patient_study_list v621
				WHERE (v621.c621_patient_id, v621.c613_study_period_id) NOT IN (
																 SELECT t622.c621_patient_id
																	  , t622.c613_study_period_id
																   FROM t622_patient_list t622
																  WHERE t622.c621_patient_id = v621.c621_patient_id
																  AND NVL(t622.C622_DELETE_FL,'N')='N')
				  AND v621.c621_exp_date_start <= SYSDATE	-- Only retrieves the due information
				  AND v621.c621_exp_date_final >= SYSDATE
				  AND v621.c101_user_id = DECODE (p_cra_id, '', v621.c101_user_id, p_cra_id)
				  AND v621.c611_study_id = DECODE (p_study_id, '', v621.c611_study_id, p_study_id)
				  AND v621.c621_patient_id NOT IN (SELECT c621_patient_id
													 FROM t622_patient_list
													WHERE c601_form_id = 15 AND c613_study_period_id = 91
													AND NVL(C622_DELETE_FL,'N')='N'
													)
			 ORDER BY s_id, a_name, p_ide_no, s_p_id, seq;
	END gm_pending_form_list;

--
/***************************************************************************************
 * Description	 :This procedure is called to FETCH the Out Of Window list of form that need to be filled
 * Parameters	 :N/A
 *				 :p_recordset	- holds list of list of due form for the entire patient
 **************************************************************************************/
--
	PROCEDURE gm_out_of_window_verify_list (
		p_study_id			IN		 t611_clinical_study.c611_study_id%TYPE
	  , p_cra_id			IN		 t101_user.c101_user_id%TYPE
	  , p_rsetoutofwindow	OUT 	 TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_rsetoutofwindow
		 FOR
			 SELECT   v621.c611_study_id s_id, v621.c613_study_period_ds s_ds, v621.c901_study_period s_p_id
					, v621.c613_study_period_id s_p_key, c704_account_id a_id
					,	 DECODE (v621.c614_site_id, '', '', v621.c614_site_id || '-')
					  || get_account_name (v621.c704_account_id)
					  || DECODE (v621.c101_user_id, '', '', '-' || get_user_name (v621.c101_user_id)) a_name
					, v621.c621_patient_ide_no p_ide_no, v621.c621_patient_id p_key, v621.c601_form_id f_id
					, v621.c612_study_form_ds || ' - ' || get_form_name (v621.c601_form_id) f_name
					, TO_CHAR (c621_exp_date_start, 'MM/DD/YYYY') exp_start_date
					, TO_CHAR (v621.c621_exp_date, 'MM/DD/YYYY') exp_date
					, TO_CHAR (v621.c621_exp_date_final, 'MM/DD/YYYY') final_due_date
					, DECODE (SIGN (SYSDATE - DECODE (c621_exp_date_final, '', SYSDATE, c621_exp_date_final))
							, 1, 'Y'
							, 'N'
							 ) over_due
					, c612_seq seq
				 FROM v621_patient_study_list v621
				WHERE (v621.c621_patient_id, v621.c613_study_period_id) NOT IN (
																 SELECT t622.c621_patient_id
																	  , t622.c613_study_period_id
																   FROM t622_patient_list t622
																  WHERE t622.c621_patient_id = v621.c621_patient_id
																  AND NVL(t622.C622_DELETE_FL,'N')='N'
																  )
				  AND v621.c621_exp_date_final < SYSDATE
				  AND v621.c101_user_id = DECODE (p_cra_id, '', v621.c101_user_id, p_cra_id)
				  AND v621.c611_study_id = DECODE (p_study_id, '', v621.c611_study_id, p_study_id)
				  AND v621.c621_patient_id NOT IN (SELECT c621_patient_id
													 FROM t622_patient_list
													WHERE c601_form_id = 15 AND c613_study_period_id = 91
													AND NVL(C622_DELETE_FL,'N')='N'
													)
			 ORDER BY s_id, a_name, p_ide_no, s_p_id, seq;
	END gm_out_of_window_verify_list;

--
/***************************************************************************************
 * Description	 :This procedure is called to FETCH the Pending Verification of form that need to be filled
 * Parameters	 :N/A
 *				 :p_recordset	- holds list of list of due form for the entire patient
 **************************************************************************************/
--
	PROCEDURE gm_pending_verification_list (
		p_study_id			 IN 	  t611_clinical_study.c611_study_id%TYPE
	  , p_cra_id			 IN 	  t101_user.c101_user_id%TYPE
	  , p_rsetverification	 OUT	  TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_rsetverification
		 FOR
			 SELECT   t621.c611_study_id s_id, t613.c613_study_period_ds s_ds, t613.c901_study_period s_p_id
					, t613.c613_study_period_id s_p_key, get_code_seq_no (c901_study_period) seq_no
					, t621.c704_account_id a_id
					,	 DECODE (t614.c614_site_id, '', '', t614.c614_site_id || '-')
					  || get_account_name (t614.c704_account_id)
					  || DECODE (t614.c101_user_id, '', '', '-' || get_user_name (t614.c101_user_id)) a_name
					, t621.c621_patient_ide_no p_ide_no, t621.c621_patient_id p_key, t612.c601_form_id f_id
					, t612.c612_study_form_ds || ' - ' || get_form_name (t612.c601_form_id) f_name
					, TO_CHAR (NVL (t622.c622_date, ''), 'MM/DD/YYYY') e_date
					, get_user_name (NVL (t622.c622_last_updated_by, t622.c622_created_by)) entered_by
				 FROM t621_patient_master t621
					, t612_study_form_list t612
					, t613_study_period t613
					, t614_study_site t614
					, t622_patient_list t622
				WHERE t621.c621_delete_fl = 'N'
				  AND t612.c611_study_id = t621.c611_study_id
				  AND t621.c611_study_id = t614.c611_study_id
				  AND t612.c612_study_list_id = t613.c612_study_list_id
				  AND t621.c621_patient_id = t622.c621_patient_id
				  AND t612.c611_study_id = DECODE (p_study_id, '', t612.c611_study_id, p_study_id)
				  AND t613.c613_study_period_id = t622.c613_study_period_id
				  AND t621.c704_account_id = t614.c704_account_id
				  AND NVL (t622.c622_verify_fl, 'N') = 'N'
				  AND NVL(t622.C622_DELETE_FL,'N')='N'
			 ORDER BY s_id, a_name, p_ide_no, c612_seq;
	END gm_pending_verification_list;

	--
/***************************************************************************************
 * Description	 :This procedure is called to FETCH the gm_load_no_of_event
 * Parameters	 :N/A
 *				 :p_recordset	- holds list of list of due form for the entire patient
 **************************************************************************************/
--
	PROCEDURE gm_load_no_of_event (
		p_study_id	   IN		VARCHAR2
	  , p_form_id	   IN		VARCHAR2
	  , p_patient_id   IN		VARCHAR2
	  , p_rsetforms    OUT		TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_rsetforms
		 FOR
			 SELECT   c622_patient_event_no event_no
				 FROM t622_patient_list
				WHERE c613_study_period_id = p_study_id AND c601_form_id = p_form_id AND c621_patient_id = p_patient_id
				AND NVL(C622_DELETE_FL,'N')='N'
			 ORDER BY c622_patient_event_no;
	END gm_load_no_of_event;

	--
/*******************************************************
 * Purpose: This Procedure is used to fetch
 * the data about study

 *******************************************************/
--
	PROCEDURE gm_all_study_form (
		p_studyid	  IN	   t611_clinical_study.c611_study_id%TYPE
	  , p_recordset   OUT	   TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_recordset
		 FOR
			 SELECT   t613.c901_study_period periodid, t601.c601_form_id formid
					, t612.c612_study_form_ds || ' - ' || t601.c601_form_nm formnm, t612.c611_study_id stid
					, t612.c612_seq formseq, t612.c612_study_list_id stlistid, c613_study_period_id stperid
					, t612.c612_initial_fl inireqfl, t613.c613_study_exam_type flag
				 FROM t601_form_master t601, t612_study_form_list t612, t613_study_period t613
				WHERE t601.c601_form_id = t612.c601_form_id
				  AND t612.c611_study_id = p_studyid
				  AND t613.c612_study_list_id = t612.c612_study_list_id
			 ORDER BY t612.c612_seq, t613.c901_study_period;
	END gm_all_study_form;

/*******************************************************
 * Purpose: This Procedure is used to fetch
 * the data about study for specific period

 *******************************************************/
--
	PROCEDURE gm_fch_prd_study_form (
		p_studyid	  IN	   t611_clinical_study.c611_study_id%TYPE
	  , p_periodid	  IN	   t613_study_period.c901_study_period%TYPE
	  , p_recordset   OUT	   TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_recordset
		 FOR
			 SELECT   t613.c901_study_period periodid, t601.c601_form_id formid
					, t612.c612_study_form_ds || ' - ' || t601.c601_form_nm formnm, t612.c611_study_id stid
					, t612.c612_seq formseq, t612.c612_study_list_id stlistid, c613_study_period_id stperid
					, t612.c612_initial_fl inireqfl, t613.c613_study_exam_type flag
				 FROM t601_form_master t601, t612_study_form_list t612, t613_study_period t613
				WHERE t601.c601_form_id = t612.c601_form_id
				  AND t612.c611_study_id = p_studyid
				  AND t613.c901_study_period = p_periodid
				  AND t613.c612_study_list_id = t612.c612_study_list_id
			 ORDER BY t613.c901_study_period, t612.c612_seq;
	END gm_fch_prd_study_form;

--
/***************************************************************************************
 * Description	 :This procedure is called to FETCH the gm_load_no_of_event
 * Parameters	 :N/A
 *				 :p_recordset	- holds list of list of due form for the entire patient
 **************************************************************************************/
--
	PROCEDURE gm_load_no_of_event_des (
		p_study_id	   IN		VARCHAR2
	  , p_form_id	   IN		VARCHAR2
	  , p_patient_id   IN		VARCHAR2
	  , p_voidFlag	   IN		VARCHAR2
	  , p_rsetforms    OUT		TYPES.cursor_type
	)
	AS
		v_adv_forms varchar2(100);
	BEGIN
		
		v_adv_forms := get_rule_value(p_study_id,p_form_id);
		
	my_context.set_my_inlist_ctx (v_adv_forms);
		OPEN p_rsetforms
		 FOR
			 SELECT   t622.c622_patient_event_no event_no
					, (t622.c622_patient_event_no || '-' || NVL(t623.c904_answer_ds,get_answer_list_name(t623.c605_answer_list_id))) des
				 FROM t622_patient_list t622, t623_patient_answer_list t623
				WHERE t622.c621_patient_id = t623.c621_patient_id
				  AND t622.c622_patient_list_id = t623.c622_patient_list_id
				  AND t622.c613_study_period_id = p_study_id
				  AND t622.c601_form_id = p_form_id
				  AND t622.c621_patient_id = p_patient_id
				  AND to_char(t623.c603_answer_grp_id) in(SELECT token FROM v_in_list) 
				  AND NVL(t623.c605_answer_list_id,1) != 0 
				  AND NVL(t622.C622_DELETE_FL,'N')= NVL(p_voidFlag,'N')
				  AND NVL(t623.C623_DELETE_FL,'N')= NVL(p_voidFlag,'N')
			 ORDER BY t622.c622_patient_event_no;
	END gm_load_no_of_event_des;

	--
/***************************************************************************************
 * Description	 :This procedure is called to save the patient details
 * Parameters	 :N/A
 **************************************************************************************/
--
	PROCEDURE gm_sav_patient_record (
		p_patient_id	   IN		t624_patient_record.c621_patient_id%TYPE
	  , p_timepoint 	   IN		t624_patient_record.c901_study_period%TYPE
	  , p_type			   IN		t624_patient_record.c901_record_grp%TYPE
	  , p_view			   IN		t624_patient_record.c901_record_type%TYPE
	  , p_filename		   IN		t903_upload_file_list.c903_file_name%TYPE
	  , p_userid		   IN		t624_patient_record.c624_created_by%TYPE
	  , p_studyid		   IN		t624_patient_record.c611_study_id%TYPE
	  , p_record_dt 	   IN		VARCHAR2
	  , p_patient_rec_id   IN OUT	t624_patient_record.c624_patient_rec_id%TYPE
	)
	AS
		v_count 	   NUMBER;
		v_upload_file_list t903_upload_file_list.c903_upload_file_list%TYPE;
		v_patient_rec_id t624_patient_record.c624_patient_rec_id%TYPE;
	BEGIN
--Algorithm:
--1.CHECK FOR the existence OF a RECORD IN t624_patient_record FOR patient,TYPE,VIEW,Timepoint/RECORD DATE,study.
--2.WHEN there are no records , CREATE a RECORD IN the T903_UPLOAD_FILE_LIST, get the newly created ROW id.
--	INSERT a RECORD IN t624_patient_record.
--3.IF the RECORD exist AND COUNT >1, there IS a similar RECORD FOR the combination AND USER cannot UPDATE the attribute , EXCEPTION will be
--thrown.
--4.IF the RECORD exist AND COUNT =1,UPDATE the TImepoint,VIEW FOR the patient IN t624_patient_record,UPDATE the filename IN T903_UPLOAD_FILE_LIST.
		IF (p_patient_rec_id = 0)	-- if no record exists
		THEN
			gm_pkg_upload_info.gm_sav_upload_info (p_view, p_filename, p_userid, p_type, v_upload_file_list);

			SELECT s624_patient_rec_id.NEXTVAL
			  INTO p_patient_rec_id
			  FROM DUAL;

			INSERT INTO t624_patient_record
						(c624_patient_rec_id, c621_patient_id, c611_study_id, c901_study_period, c901_record_type
					   , c901_record_grp, c624_radiogrp_dt, c624_void_fl, c903_upload_file_list, c624_created_date
					   , c624_created_by
						)
				 VALUES (p_patient_rec_id, p_patient_id, p_studyid, p_timepoint, p_view
					   , p_type, TO_DATE (p_record_dt, 'MM/DD/YYYY'), '', v_upload_file_list, TRUNC (SYSDATE)
					   , p_userid
						);
		END IF;

		SELECT COUNT (1)
		  INTO v_count
		  FROM t624_patient_record t624
		 WHERE t624.c901_study_period = p_timepoint
		   AND t624.c621_patient_id = p_patient_id
		   AND t624.c901_record_type = p_view
		   AND t624.c624_void_fl IS NULL
		   AND t624.c611_study_id = p_studyid
		   AND NVL (t624.c624_radiogrp_dt, SYSDATE) =
				   DECODE (TO_DATE (p_record_dt, 'MM/DD/YYYY')
						 , NULL, NVL (t624.c624_radiogrp_dt, SYSDATE)
						 , TO_DATE (p_record_dt, 'MM/DD/YYYY')
						  );

		IF (v_count = 0)
		THEN
			SELECT c903_upload_file_list
			  INTO v_upload_file_list
			  FROM t624_patient_record t624
			 WHERE c624_patient_rec_id = p_patient_rec_id;

			UPDATE t624_patient_record
			   SET c901_record_type = p_view
				 , c901_study_period = p_timepoint
				 , c624_radiogrp_dt = TO_DATE (p_record_dt, 'MM/DD/YYYY')
				 , c624_last_updated_by = p_userid
				 , c624_last_updated_date = TRUNC (SYSDATE)
			 WHERE c624_patient_rec_id = p_patient_rec_id;

			UPDATE t903_upload_file_list
			   SET c903_last_updated_by = p_userid
				 , c903_last_updated_date = SYSDATE
				 , c903_file_name = p_filename
				 , c903_ref_id = p_view
			 WHERE c903_upload_file_list = v_upload_file_list;
		END IF;

		IF v_count = 1
		THEN
			SELECT t624.c624_patient_rec_id
			  INTO v_patient_rec_id
			  FROM t624_patient_record t624
			 WHERE t624.c901_study_period = p_timepoint
			   AND t624.c621_patient_id = p_patient_id
			   AND t624.c901_record_type = p_view
			   AND t624.c624_void_fl IS NULL
			   AND t624.c611_study_id = p_studyid
			   AND NVL (t624.c624_radiogrp_dt, SYSDATE) =
					   DECODE (TO_DATE (p_record_dt, 'MM/DD/YYYY')
							 , NULL, NVL (t624.c624_radiogrp_dt, SYSDATE)
							 , TO_DATE (p_record_dt, 'MM/DD/YYYY')
							  );

			IF (v_patient_rec_id != p_patient_rec_id)
			THEN
				raise_application_error (-20240, '');
			END IF;
		END IF;

		-- Error Message :
		--raise_application_error ('A Radiograph already exists for timepoint: '||(select c613_study_period_ds from t613_study_period where c613_study_period_id=p_timepoint)||' and View '||get_code_name(p_view) . Update Cannot be done.','');
		IF (p_type = '60391' AND v_count > 1)
		THEN
			raise_application_error (-20240, '');
		END IF;
	END gm_sav_patient_record;

--
/*******************************************************
 * Purpose: This Procedure is used to fetch
 * the data about patient records

 *******************************************************/
--
	PROCEDURE gm_cr_fch_patient_record (
		p_patient_id	   IN		t624_patient_record.c621_patient_id%TYPE
	  , p_timepointid	   IN		t624_patient_record.c901_study_period%TYPE
	  , p_viewid		   IN		t624_patient_record.c901_record_type%TYPE
	  , p_typeid		   IN		t624_patient_record.c901_record_grp%TYPE
	  , p_patient_rec_id   IN		t624_patient_record.c624_patient_rec_id%TYPE
	  , p_study_id		   IN		t614_study_site.c611_study_id%TYPE
	  , p_site_id		   IN		t614_study_site.c614_site_id%TYPE
	  , p_patient_record   OUT		TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_patient_record
		 FOR
			 SELECT DISTINCT t624.c621_patient_id pat_id, t621.c621_patient_ide_no pat_ide
						   , t624.c901_study_period time_pnt_id, get_code_name (t624.c901_study_period) time_pnt_name
						   , get_code_name (t624.c901_record_grp) rad_type_name, t624.c901_record_grp rad_type_id
						   , get_code_name (t624.c901_record_type) rad_view_name, t624.c901_record_type rad_view_id
						   , TO_CHAR (t624.c624_radiogrp_dt, 'MM/DD/YYYY') radiogrp_date
						   , TO_CHAR (t903.c903_created_date, 'MM/DD/YYYY') upld_dt
						   , get_user_name (t624.c624_created_by) upld_by, t903.c903_file_name file_nm
						   , t624.c903_upload_file_list file_id, t624.c624_patient_rec_id pat_rec_id
						FROM t624_patient_record t624
						   , t903_upload_file_list t903
						   , t621_patient_master t621
						   , v621_patient_study_list v621
					   WHERE t624.c621_patient_id = NVL (p_patient_id, t624.c621_patient_id)
						 AND t621.c621_patient_id = t624.c621_patient_id
						 AND t624.c901_study_period = NVL (p_timepointid, t624.c901_study_period)
						 AND t624.c901_record_type = NVL (p_viewid, t624.c901_record_type)
						 AND t624.c901_record_grp = NVL (p_typeid, t624.c901_record_grp)
						 AND t624.c903_upload_file_list = t903.c903_upload_file_list(+)
						 AND t624.c624_patient_rec_id = NVL (p_patient_rec_id, t624.c624_patient_rec_id)
						 AND t624.c611_study_id = NVL (p_study_id, t624.c624_patient_rec_id)
						 AND v621.c614_site_id = NVL (p_site_id, t624.c624_patient_rec_id)
						 AND v621.c611_study_id = t624.c611_study_id
						 AND v621.c621_patient_id = t624.c621_patient_id
						 --As we do not have N/A time point in the view, the join will fail for N/A timepoint.
						 --AND v621.c901_study_period = t624.c901_study_period
						 AND t624.c624_void_fl IS NULL;
	END gm_cr_fch_patient_record;

--
/*******************************************************
 * Purpose: This procedure will create/update a record in the T622_Patient_list  table
 *******************************************************/
--
	PROCEDURE gm_sav_patient_list (
		p_date					 IN 	  VARCHAR2
	  , p_initital				 IN 	  t622_patient_list.c622_initials%TYPE
	  , p_cal_value 			 IN 	  t622_patient_list.c621_cal_value%TYPE
	  , p_form_id				 IN 	  t601_form_master.c601_form_id%TYPE
	  , p_patient_id			 IN 	  t621_patient_master.c621_patient_id%TYPE
	  , p_type					 IN 	  t622_patient_list.c901_type%TYPE
	  , p_study_period_id		 IN 	  t613_study_period.c613_study_period_id%TYPE
	  , p_out_of_window_fl		 IN 	  t622_patient_list.c622_out_of_window_fl%TYPE
	  , p_missing_follow_up_fl	 IN 	  t622_patient_list.c622_missing_follow_up_fl%TYPE
	  , p_patient_event_no		 IN 	  t622_patient_list.c622_patient_event_no%TYPE
	  , p_user					 IN 	  t622_patient_list.c622_created_by%TYPE
	  , p_pat_list_id			 IN OUT   t622_patient_list.c622_patient_list_id%TYPE
	)
	AS
	BEGIN
		UPDATE t622_patient_list
		   SET c622_date = TO_DATE (p_date, 'MM/DD/YYYY')
			 , c622_initials = p_initital
			 , c621_cal_value = p_cal_value
			 , c601_form_id = p_form_id
			 , c621_patient_id = p_patient_id
			 , c622_last_updated_by = p_user
			 , c901_type = p_type
			 , c613_study_period_id = p_study_period_id
			 , c622_last_updated_date = SYSDATE
			 , c622_out_of_window_fl = p_out_of_window_fl
			 , c622_missing_follow_up_fl = p_missing_follow_up_fl
			 , c622_patient_event_no = p_patient_event_no
		 WHERE c622_patient_list_id = p_pat_list_id;

		IF (SQL%ROWCOUNT = 0)
		THEN
			SELECT s622_patient_list.NEXTVAL
			  INTO p_pat_list_id
			  FROM DUAL;

			INSERT INTO t622_patient_list
						(c622_patient_list_id, c622_date, c622_initials, c621_cal_value, c622_delete_fl
					   , c622_created_by, c622_created_date, c622_verify_fl, c601_form_id, c621_patient_id, c901_type
					   , c613_study_period_id, c622_out_of_window_fl, c622_missing_follow_up_fl, c622_patient_event_no
						)
				 VALUES (p_pat_list_id, TO_DATE (p_date, 'MM/DD/YYYY'), p_initital, p_cal_value, 'N'
					   , p_user, SYSDATE, 'N', p_form_id, p_patient_id, p_type
					   , p_study_period_id, p_out_of_window_fl, p_missing_follow_up_fl, p_patient_event_no
						);
		END IF;
	END gm_sav_patient_list;

--
/*******************************************************
 * Purpose: This procedure will create/update a record in the T623_PATIENT_ANSWER_LIST table
 *******************************************************/
--
	PROCEDURE gm_sav_patient_answer_list (
		p_pat_list_id		IN		 t622_patient_list.c622_patient_list_id%TYPE
	  , p_question_id		IN		 t602_question_master.c602_question_id%TYPE
	  , p_ans_list_id		IN		 t605_answer_list.c605_answer_list_id%TYPE
	  , p_ans_grp_id		IN		 t623_patient_answer_list.c603_answer_grp_id%TYPE
	  , p_ans_desc			IN		 t623_patient_answer_list.c904_answer_ds%TYPE
	  , p_form_id			IN		 t601_form_master.c601_form_id%TYPE
	  , p_patient_id		IN		 t621_patient_master.c621_patient_id%TYPE
	  , p_user				IN		 t623_patient_answer_list.c623_created_by%TYPE
	  , p_pat_ans_list_id	IN OUT	 t623_patient_answer_list.c623_patient_ans_list_id%TYPE
	)
	AS
	BEGIN
		UPDATE t623_patient_answer_list
		   SET c622_patient_list_id = p_pat_list_id
			 , c602_question_id = p_question_id
			 , c605_answer_list_id = p_ans_list_id
			 , c603_answer_grp_id = p_ans_grp_id
			 , c904_answer_ds = p_ans_desc
			 , c601_form_id = p_form_id
			 , c623_last_updated_date = SYSDATE
			 , c623_last_updated_by = p_user
			 , c621_patient_id = p_patient_id
		 WHERE c623_patient_ans_list_id = p_pat_ans_list_id;

		IF (SQL%ROWCOUNT = 0)
		THEN
			SELECT s623_patient_answer_list.NEXTVAL
			  INTO p_pat_ans_list_id
			  FROM DUAL;

			INSERT INTO t623_patient_answer_list
						(c623_patient_ans_list_id, c622_patient_list_id, c602_question_id, c605_answer_list_id
					   , c603_answer_grp_id, c904_answer_ds, c601_form_id, c623_delete_fl, c623_created_by
					   , c623_created_date, c621_patient_id
						)
				 VALUES (p_pat_ans_list_id, p_pat_list_id, p_question_id, p_ans_list_id
					   , p_ans_grp_id, p_ans_desc, p_form_id, 'N', p_user
					   , SYSDATE, p_patient_id
						);
		END IF;
	END gm_sav_patient_answer_list;

--
/*******************************************************
 * Purpose: This procedure is used to verify if there is any patient who is having lock set on the form (SD-6).
   This is a check to get a mismatch between the patient form downloaded and the patient for which the data is uploaded.
 *******************************************************/
--
	PROCEDURE gm_fch_patient_form_lock (
		p_studyid			   IN		t624_patient_record.c611_study_id%TYPE
	  , p_upload_form_id	   IN		t601_form_master.c601_form_id%TYPE
	  , p_downloaded_form_id   IN		t601_form_master.c601_form_id%TYPE
	  , p_recordset 		   OUT		TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_recordset
		 FOR
			 /*   SELECT t621.c621_patient_ide_no pat_ide, t622.c601_form_id form_id
					   , t625.c625_pat_verify_list_id verify_list
					FROM t622_patient_list t622, t621_patient_master t621, t625_patient_verify_list t625
				   WHERE t621.c611_study_id = p_studyid
					 AND t622.c621_patient_id NOT IN (SELECT c621_patient_id
														FROM t622_patient_list
													   WHERE c601_form_id IN (p_downloaded_form_id))
					 AND t622.c601_form_id = p_upload_form_id
					 AND t625.c622_patient_list_id = t622.c622_patient_list_id
					 AND t621.c621_patient_id = t622.c621_patient_id
					 AND NVL (t622.c622_verify_fl, 'N') = 'Y'
					 AND t625.c625_delete_fl IS NULL
					 AND t625.c901_verify_level = 6195
					 AND NVL (t622.c622_delete_fl, 'N)') = 'N'
					 AND NVL (t621.c621_delete_fl, 'N') = 'N';*/
			 SELECT t621.c621_patient_ide_no pat_ide, t622.c601_form_id form_id, t622.c622_patient_list_id
				  , t625.c625_pat_verify_list_id verify_list
			   FROM t622_patient_list t622
				  , t621_patient_master t621
				  , t613_study_period t613
				  , t625_patient_verify_list t625
			  WHERE (t613.c613_study_period_ds, t622.c621_patient_id) IN (
						SELECT t613.c613_study_period_ds, a.c621_patient_id
						  FROM t622_patient_list a, t613_study_period t613
						 WHERE a.c601_form_id = p_downloaded_form_id
						   AND t613.c613_study_period_id = a.c613_study_period_id
						   AND NVL(a.C622_DELETE_FL,'N')='N'
						MINUS
						SELECT t613.c613_study_period_ds, a.c621_patient_id
						  FROM t622_patient_list a, t613_study_period t613
						 WHERE a.c601_form_id = p_upload_form_id AND t613.c613_study_period_id = a.c613_study_period_id
						 AND NVL(a.C622_DELETE_FL,'N')='N')
				AND t622.c601_form_id = p_downloaded_form_id
				AND t621.c611_study_id = p_studyid
				AND t621.c621_patient_id = t622.c621_patient_id
				AND NVL (t622.c622_verify_fl, 'N') = 'Y'
				AND t621.c621_delete_fl = 'N'
				AND t622.c622_delete_fl = 'N'
				AND t613.c613_study_period_id = t622.c613_study_period_id
				AND t622.c622_date IS NOT NULL
				AND t622.c622_missing_follow_up_fl IS NULL
				AND t625.c622_patient_list_id = t622.c622_patient_list_id
				AND t625.c901_verify_level = 6195
				AND t625.c625_delete_fl IS NULL;
	END gm_fch_patient_form_lock;

--
/*******************************************************
 * Purpose: This procedure is used to release the form lock of the patient.
The form (SD-6)will be locked during download and when the data is uploaded for the patient, the form (SD-6) should be un-locked
 *******************************************************/
--
	PROCEDURE gm_sav_patient_verify_list (
		p_patient_id	IN	 t624_patient_record.c621_patient_id%TYPE
	  , p_form_id		IN	 t601_form_master.c601_form_id%TYPE
	  , p_timepointid	IN	 t624_patient_record.c901_study_period%TYPE
	  , p_exam_date 	IN	 VARCHAR2
	  , p_userid		IN	 t625_patient_verify_list.c625_last_updated_by%TYPE
	)
	AS
		v_pat_list_id  t625_patient_verify_list.c625_pat_verify_list_id%TYPE;
	BEGIN
		/*
			SELECT t625.c625_pat_verify_list_id
			  INTO v_pat_list_id
			  FROM t622_patient_list t622, t625_patient_verify_list t625
			 WHERE t622.c622_patient_list_id = t625.c622_patient_list_id
			   AND t625.c901_verify_level = 6195
			   AND t622.c601_form_id = p_form_id
			   --AND t622.c613_study_period_id = p_timepointid
			   AND t622.c621_patient_id = p_patient_id
			   AND t622.c622_date = TO_DATE (p_exam_date, 'MM/DD/YYYY')
			   AND t622.c622_delete_fl = 'N'
			   AND t625.c625_delete_fl IS NULL; */
			   
		UPDATE t625_patient_verify_list
		   SET c625_delete_fl = 'Y'
			 , c625_last_updated_date = SYSDATE
			 , c625_last_updated_by = p_userid
		 WHERE c625_pat_verify_list_id IN (
				   SELECT t625.c625_pat_verify_list_id
					 FROM t622_patient_list t622, t625_patient_verify_list t625
					WHERE t622.c622_patient_list_id = t625.c622_patient_list_id
					  AND t625.c901_verify_level = 6195
					  AND t622.c601_form_id = p_form_id
					  --AND t622.c613_study_period_id = p_timepointid
					  AND t622.c621_patient_id = p_patient_id
					  AND t622.c622_date = TO_DATE (p_exam_date, 'MM/DD/YYYY')
					  AND t622.c622_delete_fl = 'N'
					  AND t625.c625_delete_fl IS NULL);
	END gm_sav_patient_verify_list;

		--
/*******************************************************
 * Purpose: This Procedure is used to fetch
 * the all forms for study

 *******************************************************/
--
	PROCEDURE gm_study_form_list (
		p_studyid	  IN	   t611_clinical_study.c611_study_id%TYPE
	  , p_recordset   OUT	   TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_recordset
		 FOR
			 SELECT   t601.c601_form_id formid, t612.c612_study_form_ds || ' - ' || t601.c601_form_nm formnm
					, t612.c611_study_id stid, t612.c612_seq formseq, t612.c612_study_list_id stlistid
				 FROM t601_form_master t601, t612_study_form_list t612
				WHERE t601.c601_form_id = t612.c601_form_id AND t612.c611_study_id = p_studyid
			 ORDER BY t612.c612_seq;
	END gm_study_form_list;

--

	/*******************************************************
 * Purpose: This Procedure is used to fetch
 * site coordinator list for a given study site id.

 *******************************************************/
--
	PROCEDURE gm_fch_site_coordinator (
		p_studysiteid	IN		 t611_clinical_study.c611_study_id%TYPE
	  , p_recordset 	OUT 	 TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_recordset
		 FOR
			 SELECT c101_party_id ID, get_party_name (c101_party_id) NAME
			   FROM t616_site_party_mapping
			  WHERE c614_study_site_id = p_studysiteid AND c616_void_fl IS NULL;
	END gm_fch_site_coordinator;

--
--
/*******************************************************
 * Purpose: This Function is used to fetch
  event desc.

 *******************************************************/
--
	FUNCTION get_event_desc (
		p_study_id	   IN	VARCHAR2
	  , p_form_id	   IN	VARCHAR2
	  , p_patient_id   IN	VARCHAR2
	  , p_eventno	   IN	VARCHAR2
	)
		RETURN VARCHAR2
	IS
		v_evendesc	   VARCHAR2 (20);
	BEGIN
		SELECT SUBSTR (t623.c904_answer_ds, 1, 20)
		  INTO v_evendesc
		  FROM t622_patient_list t622, t623_patient_answer_list t623
		 WHERE t622.c621_patient_id = t623.c621_patient_id
		   AND t622.c622_patient_list_id = t623.c622_patient_list_id
		   AND t622.c613_study_period_id = p_study_id
		   AND t622.c601_form_id = p_form_id
		   AND t622.c621_patient_id = p_patient_id
		   AND t623.c603_answer_grp_id IN (477, 598,5001)
		   AND t622.c622_patient_event_no = TO_NUMBER (p_eventno)
		   AND NVL(t622.C622_DELETE_FL,'N')='N'
		   AND NVL(t623.C623_DELETE_FL,'N')='N';
		RETURN v_evendesc;
	END;

/*******************************************************
 * Purpose: This Procedure is used to fetch
 * patient details.

 *******************************************************/
--
	PROCEDURE gm_fch_patient_details (
		p_patientid   IN	   t621_patient_master.c621_patient_id%TYPE
	  , p_recordset   OUT	   TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_recordset
		 FOR
			 SELECT t621.c621_patient_ide_no patient_ide, c621_surgery_comp_fl cmpltnfl, c101_surgeon_id surgeonid
				  , c621_surgery_date surgerydt, c704_account_id acctid, c901_surgery_type surgerytype
				  , c611_study_id studyid
			   FROM t621_patient_master t621
			  WHERE t621.c621_delete_fl = 'N' AND t621.c621_patient_id = p_patientid;
	END gm_fch_patient_details;

	/*******************************************************
	 * Purpose: This Function is used to return valid date 
	  based on passed user parameter 
	 *******************************************************/
	--
	FUNCTION get_valid_cr_date (p_strdate VARCHAR2)
	   RETURN DATE
	IS
	   v_cr_valid_date   DATE;
	BEGIN
	   --
	   SELECT DECODE (length (p_strdate),
			  8, TO_DATE (p_strdate, 'MM/DD/YY'),
			  TO_DATE (p_strdate, 'MM/DD/YYYY')
			 )
	     INTO v_cr_valid_date
	     FROM DUAL;

	   RETURN v_cr_valid_date;
	EXCEPTION
	   WHEN OTHERS
	   THEN
	      RETURN NULL;
	END get_valid_cr_date;
	
	
	/*******************************************************
	 * Purpose: This Function is used to return valid number 
	  based on passed user parameter 
	 *******************************************************/
	--
	FUNCTION get_valid_cr_number (p_strnumber VARCHAR2)
	   RETURN NUMBER
	IS
	   v_cr_valid_number   NUMBER;
	BEGIN
	   --
	   SELECT TO_NUMBER(p_strnumber) 
	   INTO v_cr_valid_number FROM dual;

	   RETURN v_cr_valid_number;
	EXCEPTION
	   WHEN OTHERS
	   THEN
	      RETURN NULL;
	END get_valid_cr_number;
	
/*******************************************************************
* Description : this procedure is used to update patient and site map
* changes to v621_patient_study_list
* Author      : Dhana Reddy
*******************************************************************/
PROCEDURE gm_sav_site_patient_details (
        p_patient_id    IN t621_patient_master.c621_patient_id%TYPE,
        p_study_site_id IN t621_patient_master.c611_study_id%TYPE,
        p_study_id      IN t614_study_site.c614_site_id%TYPE)
AS
BEGIN
     DELETE
       FROM v621_patient_study_list
      WHERE c621_patient_id    = NVL (p_patient_id, c621_patient_id)
        AND c614_study_site_id = NVL (p_study_site_id, c614_study_site_id)
        AND c611_study_id      = NVL (p_study_id, c611_study_id) ;
        
     INSERT INTO v621_patient_study_list
     SELECT t621.c621_patient_id, t621.c621_patient_ide_no, c901_surgery_type,
        get_code_name (c901_surgery_type) c621_surgery_nm, t612.c611_study_id, t614.c614_study_site_id,
        t614.c704_account_id, t612.c612_study_list_id, t612.c612_study_form_ds,
        t613.c613_study_period_id, t613.c613_study_period_ds, t613.c613_duration,
        t613.c901_period_type, get_code_name (t613.c901_period_type) c621_period_nm, t613.c613_grace_period_before,
        c613_grace_period_after, c901_grace_period_type, get_code_name (c901_grace_period_type) c621_grace_period_nm,
        t621.c621_surgery_date c622_surgery_date, DECODE (t621.c621_surgery_date, NULL, TO_DATE (NULL, 'MM/DD/YYYY'),
        get_cr_cal_exam_date (t621.c621_surgery_date, t613.c901_period_type, t613.c613_duration,
        t613.c613_grace_period_before, c901_grace_period_type, 'S')) c621_exp_date_start, DECODE (
        t621.c621_surgery_date, NULL, TO_DATE (NULL, 'MM/DD/YYYY'), get_cr_cal_exam_date (t621.c621_surgery_date,
        t613.c901_period_type, t613.c613_duration, NULL, NULL, 'E')) c621_exp_date, DECODE (t621.c621_surgery_date,
        NULL, TO_DATE (NULL, 'MM/DD/YYYY'), get_cr_cal_exam_date (t621.c621_surgery_date, t613.c901_period_type,
        t613.c613_duration, t613.c613_grace_period_after, c901_grace_period_type, 'F')) c621_exp_date_final,
        t613.c613_seq_no, NVL (t621.c621_surgery_comp_fl, 'N') c621_surgery_comp_fl, t614.c101_user_id,
        t614. c614_site_id, t612.c601_form_id, t613.c901_study_period,
        t612.c612_seq, t621.c101_surgeon_id, t611.c611_study_nm
       FROM t621_patient_master t621, t612_study_form_list t612, t613_study_period t613,
        t614_study_site t614, t611_clinical_study t611
      WHERE t621.c621_delete_fl         = 'N'
        AND t621.c704_account_id       IS NOT NULL
        AND t612.c611_study_id          = t621.c611_study_id
        AND t612.c612_study_list_id     = t613.c612_study_list_id
        AND t614.c611_study_id          = t612.c611_study_id
        AND t614.c704_account_id        = t621.c704_account_id
        AND t614.c614_void_fl          IS NULL
        AND t621.c901_surgery_type     IS NOT NULL
        AND t613.c901_study_period NOT IN (6309)
        AND t611.c611_study_id          = t612.c611_study_id
        AND t611.c611_delete_fl        <> 'Y'
        AND t621.c621_patient_id        = NVL (p_patient_id, t621.c621_patient_id)
        AND t614.c614_study_site_id     = NVL (p_study_site_id, t614.c614_study_site_id)
        AND t611.c611_study_id          = NVL (p_study_id, t611.c611_study_id) ;    
END gm_sav_site_patient_details;

/*******************************************************
* Purpose: This Procedure is used to fetch study details.
*******************************************************/
--
PROCEDURE gm_fch_study_dtls (
	        p_study_list OUT TYPES.cursor_type)
	AS
	BEGIN
		OPEN p_study_list FOR SELECT t611.c611_study_id studyid, t611.c611_study_nm study_nm, t611.c901_study_status status_id
		  , get_code_name (t611.c901_study_status) status_nm, t611.c611_start_date start_dt
		  , get_user_name (t611.c611_created_by) created_by, t611.c611_created_date created_dt
		  , t611.c611_delete_fl void_fl, get_user_name(t611.c611_last_updated_by) last_up_by
		  , t611.c611_last_updated_date last_up_dt, t611.c611_history_fl history_fl
		  , t611.c901_study_log_reason reason_id, get_code_name (t611.c901_study_log_reason) reason_nm
		   FROM t611_clinical_study t611 
		  WHERE t611.c611_delete_fl ='N' 
		ORDER BY t611.c611_study_id;
	END gm_fch_study_dtls;
	/******************************************************************************************
	* Purpose: This Procedure is used to update study status, comments and reason details.
	******************************************************************************************/
	PROCEDURE gm_sav_study_lock_dtls (
	        p_txn_id       IN t907_cancel_log.c907_ref_id%TYPE,
	        p_cancelreason IN t907_cancel_log.c901_cancel_cd%TYPE,
	        p_cancelid     IN t907_cancel_log.c907_cancel_log_id%TYPE,
	        p_comments     IN t907_cancel_log.c907_comments%TYPE,
	        p_userid       IN t102_user_login.c102_user_login_id%TYPE)
	AS
	    v_status             NUMBER;
	    v_lock_study_code_id NUMBER;
	    v_string             VARCHAR2 (4000) := p_txn_id;
	    v_substring          VARCHAR2 (4000) ;
	    v_study_id t611_clinical_study.c611_study_id%TYPE;
	    v_study_status t611_clinical_study.c901_study_status%TYPE;
	BEGIN
	    WHILE INSTR (v_string, '|') <> 0
	    LOOP
	        v_substring    := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
	        v_string       := SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
	        v_study_id     := NULL;
	        v_study_status := NULL;
	        --
	        v_study_id     := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
	        v_substring    := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
	        v_study_status := TO_NUMBER (v_substring) ;
	        --
	         SELECT NVL (c901_study_status, - 999)
	           INTO v_status
	           FROM t611_clinical_study
	          WHERE c611_study_id = v_study_id;
	        --
	        IF v_status <> v_study_status THEN
	             UPDATE t611_clinical_study
					SET c901_study_status    = v_study_status, c611_study_comments = p_comments
						, c901_study_log_reason = p_cancelreason, c611_last_updated_by = p_userid
						, c611_last_updated_date = SYSDATE
	  			WHERE c611_study_id    = v_study_id
	  			AND c611_delete_fl ='N';
	        END IF;
	        --
	    END LOOP;
	END gm_sav_study_lock_dtls;
	/******************************************************************************************
	* Purpose: This function is used to return the study status.
	******************************************************************************************/
	--
	FUNCTION get_study_status (
	        p_study_id t611_clinical_study.c611_study_id%TYPE)
	    RETURN NUMBER
	IS
	    v_status NUMBER;
	BEGIN
	    --
		     SELECT c901_study_status
		       INTO v_status
		       FROM t611_clinical_study
		      WHERE c611_study_id = p_study_id
		      	AND c611_delete_fl ='N';
		    RETURN v_status;
		EXCEPTION
		WHEN OTHERS THEN
		    RETURN NULL;
	END get_study_status;
	/******************************************************************************************
	* Purpose: This procedure is used to fetch the study history details.
	******************************************************************************************/
	PROCEDURE gm_fch_study_history (
	        p_study_id IN t611_clinical_study.c611_study_id%TYPE,
	        p_study_history OUT TYPES.cursor_type)
	AS
	BEGIN
	    OPEN p_study_history FOR SELECT t611a.c611_study_id studyid,
	    t611a.c611_study_log_nm study_nm,
	    t611a.c901_study_log_status status_id,
	    get_code_name (t611a.c901_study_log_status) status_nm,
	    get_user_name (t611a.c611_log_created_by) created_by,
	    t611a.c611_log_created_date created_dt,
	    t611a.c611_study_log_comments comments,
	    t611a.c901_study_log_reason reason_id,
	    get_code_name (t611a.c901_study_log_reason) reason_nm
	    FROM t611_clinical_study_log t611a
	    WHERE t611a.c611_study_id = p_study_id ORDER BY t611a.c611_study_log_id DESC;
	END gm_fch_study_history;
/******************************************************************************************
* Purpose: This procedure is used to save the study log details.
******************************************************************************************/
PROCEDURE gm_sav_study_log (
	        p_study_id     IN t611_clinical_study_log.c611_study_id%TYPE,
	        p_study_nm     IN t611_clinical_study_log.c611_study_log_nm%TYPE,
	        p_log_status   IN t611_clinical_study_log.c901_study_log_status%TYPE,
	        p_log_comments IN t611_clinical_study_log.c611_study_log_comments%TYPE,
	        p_log_reason   IN t611_clinical_study_log.c901_study_log_reason%TYPE,
	        p_userid       IN t102_user_login.c102_user_login_id%TYPE)
	AS
	    v_log_id t611_clinical_study_log.c611_study_log_id%TYPE;
	BEGIN
		--
	     SELECT S611_clinical_study_log.nextval INTO v_log_id FROM DUAL;
	    --
	     INSERT
	       INTO t611_clinical_study_log
	        (
	            C611_STUDY_LOG_ID, C611_STUDY_ID, C611_STUDY_LOG_NM
	          , C901_STUDY_LOG_STATUS, C611_STUDY_LOG_COMMENTS, C901_STUDY_LOG_REASON
	          , C611_LOG_CREATED_BY, C611_LOG_CREATED_DATE
	        )
	        VALUES
	        (
	            v_log_id, p_study_id, p_study_nm
	          , p_log_status, p_log_comments, p_log_reason
	          , p_userid, SYSDATE
	        ) ;
	END gm_sav_study_log;
END gm_pkg_cr_common;
/
