/* Formatted on 2010/07/06 19:23 (Formatter Plus v4.8.0) */
CREATE OR REPLACE PACKAGE BODY gm_pkg_cr_irb
IS
   PROCEDURE gm_cr_sav_irb_approval (
      p_studyid          IN       t614_study_site.c611_study_id%TYPE,
      p_study_site_id    IN       t614_study_site.c614_study_site_id%TYPE,
      p_approvaltype     IN       t626_irb.c901_approval_type%TYPE,
      p_approvalreason   IN       t626_irb.c901_approval_reason%TYPE,
      p_status           IN       t626_irb.c901_status%TYPE,
      p_review_type      IN       t626_irb.c901_review_type%TYPE,
      p_review_period    IN       t626_irb.c901_review_period%TYPE,
      p_approval_date    IN       t626_irb.c626_approval_date%TYPE,
      p_expiry_date      IN       t626_irb.c626_expiry_date%TYPE,
      p_userid           IN       t626_irb.c626_created_by%TYPE,
      p_irb_id           IN OUT   t626_irb.c626_irb_id%TYPE
   )
   AS
      v_irb_type            t614_study_site.c901_irb_type%TYPE;
      v_approvalreason_nm   VARCHAR2 (20);
      v_cnt                 NUMBER;
   BEGIN
      SELECT c901_irb_type
        INTO v_irb_type
        FROM t614_study_site
       WHERE c614_study_site_id = p_study_site_id AND c614_void_fl IS NULL;

      SELECT get_code_name (p_approvalreason)
        INTO v_approvalreason_nm
        FROM DUAL;

      IF v_irb_type IS NULL
      THEN
         -- IRB Type is not defined for the Site.
         raise_application_error ('-20258', '');
      END IF;

      IF p_irb_id IS NULL
      THEN
         SELECT s628_irb_media.NEXTVAL
           INTO p_irb_id
           FROM DUAL;

         INSERT INTO t626_irb
                     (c626_irb_id, c614_study_site_id, c901_approval_type,
                      c901_approval_reason, c901_status, c901_review_type,
                      c901_review_period,
                      c626_approval_date,
                      c626_expiry_date, c626_created_by, c626_created_date
                     )
              VALUES (p_irb_id, p_study_site_id, p_approvaltype,
                      p_approvalreason, p_status, p_review_type,
                      p_review_period,
                      TO_DATE (p_approval_date, 'MM/DD/YYYY'),
                      TO_DATE (p_expiry_date, 'MM/DD/YYYY'), p_userid, SYSDATE
                     );
      ELSE
         UPDATE t626_irb
            SET c614_study_site_id = p_study_site_id,
                c901_approval_type = p_approvaltype,
                c901_approval_reason = p_approvalreason,
                c901_status = p_status,
                c901_review_type = p_review_type,
                c901_review_period = p_review_period,
                c626_approval_date = TO_DATE (p_approval_date, 'MM/DD/YYYY'),
                c626_expiry_date = TO_DATE (p_expiry_date, 'MM/DD/YYYY'),
                c626_last_updated_by = p_userid,
                c626_last_updated_date = SYSDATE
          WHERE c626_irb_id = p_irb_id;
      END IF;

      IF v_approvalreason_nm = 'Initial'
      THEN
         SELECT COUNT (1)
           INTO v_cnt
           FROM t626_irb t626
          WHERE c614_study_site_id = p_study_site_id
            AND c901_approval_type = p_approvaltype
            AND c901_approval_reason = p_approvalreason
            AND c626_void_fl IS NULL;

         IF v_cnt >= 2
         THEN
            -- Cannot have duplicate records with Initial status.
            raise_application_error ('-20259', '');
         END IF;
      END IF;
   END gm_cr_sav_irb_approval;

   PROCEDURE gm_cr_fch_irb_approval (
      p_irb_id           IN       t626_irb.c626_irb_id%TYPE,
      p_study_site_id    IN       t614_study_site.c614_study_site_id%TYPE,
      p_approvaltype     IN       t626_irb.c901_approval_type%TYPE,
      p_approvalreason   IN       t626_irb.c901_approval_reason%TYPE,
      p_status           IN       t626_irb.c901_status%TYPE
                                                           /*, p_review_type IN     t626_irb.c901_review_type%TYPE
                                                           , p_review_period IN     t626_irb.c901_review_period%TYPE
                                                           , p_approval_date IN     t626_irb.c626_approval_date%TYPE
                                                           , p_expiry_date    IN    t626_irb.c626_expiry_date%TYPE
                                                           , p_userid         IN    t626_irb.c626_created_by%TYPE*/
   ,
      p_irb_approval     OUT      TYPES.cursor_type
   )
   AS
   BEGIN
      OPEN p_irb_approval
       FOR
          SELECT c626_irb_id irbid, t626.c614_study_site_id studysiteid,
                 c901_approval_type approvaltype,
                 get_code_name (c901_approval_type) approvaltypename,
                 c901_approval_reason approvalreason,
                 get_code_name (c901_approval_reason) approvalreasonname,
                 c901_status status, get_code_name (c901_status) statusname,
                 c901_review_type reviewtype,
                 get_code_name (c901_review_type) reviewtypename,
                 c901_review_period,
                 get_code_name (c901_review_period) reviewperiodname,
                 c611_study_nm studyname,
                 get_account_name (c704_account_id) sitename
            FROM t626_irb t626,
                 t611_clinical_study t611,
                 t614_study_site t614
           WHERE t626.c614_study_site_id =
                                NVL (p_study_site_id, t626.c614_study_site_id)
             AND t626.c901_approval_type =
                                      NVL (p_approvaltype, c901_approval_type)
             AND t626.c901_approval_reason =
                                  NVL (p_approvalreason, c901_approval_reason)
             AND NVL (t626.c901_status, -9999) =
                                  NVL (p_status, NVL (t626.c901_status, -9999))
             AND t626.c626_irb_id = NVL (p_irb_id, c626_irb_id)
             AND t614.c614_study_site_id = t626.c614_study_site_id
             AND t614.c611_study_id = t611.c611_study_id
             AND t611.c611_delete_fl = 'N'
             AND t614.c614_void_fl IS NULL
             AND t626.c626_void_fl IS NULL;
   END gm_cr_fch_irb_approval;

   PROCEDURE gm_cr_sav_irb_event (
      p_irb_id          IN       t626_irb.c626_irb_id%TYPE,
      p_actiontaken     IN       t627_irb_event.c901_action_taken%TYPE,
      p_eventdate       IN       VARCHAR,
      p_notes           IN       t627_irb_event.c627_notes%TYPE,
      p_userid          IN       t627_irb_event.c627_created_by%TYPE,
      p_review_type     IN       t626_irb.c901_review_type%TYPE,
      p_review_period   IN       t626_irb.c901_review_period%TYPE,
      p_irbeventid      IN OUT   t627_irb_event.c627_irb_event_id%TYPE
   )
   AS
      v_irb_type            t614_study_site.c901_irb_type%TYPE;
      v_cnt                 NUMBER;
      v_msg                 VARCHAR2 (500);
      v_action_taken        VARCHAR2 (50);
      v_status              VARCHAR2 (50);
      v_month               VARCHAR2 (2);
      v_expirationdate      DATE;
      v_approvaltype        t626_irb.c901_approval_type%TYPE;
      v_approvaldate        DATE;
      v_studysiteid         t626_irb.c614_study_site_id%TYPE;
      v_approvaltype_nm     t901_code_lookup.c901_code_nm%TYPE;
      v_approvalreason_nm   t901_code_lookup.c901_code_nm%TYPE;
   BEGIN
      SELECT get_code_name (p_actiontaken)
        INTO v_action_taken
        FROM DUAL;

      SELECT c901_approval_type
        INTO v_approvaltype
        FROM t626_irb
       WHERE c626_irb_id = p_irb_id AND c626_void_fl IS NULL;

      /*IF v_action_taken = 'Approval'
      THEN
         SELECT get_code_name (c901_status)
           INTO v_status
           FROM t626_irb
          WHERE c626_irb_id = p_irb_id AND c626_void_fl IS NULL;

         IF v_status != 'Approved'
         THEN
            raise_application_error ('-20262', '');
         END IF;
      END IF;*/
      /*IF v_approvaltype = 60522 AND p_actiontaken =60550
      THEN
         SELECT COUNT (1)
           INTO v_cnt
           FROM t627_irb_event t627, t626_irb t626
          WHERE t626.c626_irb_id = p_irb_id
            AND t626.c626_irb_id = t627.c626_irb_id
            AND t626.c901_status = 60530 -- Approved
            AND t626.c901_approval_type =v_approvaltype   -- Advertisment
            AND c901_action_taken = p_actiontaken   -- Approval
            AND c627_void_fl IS NULL;

         IF v_cnt = 0
         THEN
            -- Need to change the Error code.
            raise_application_error ('-20262', '');
         END IF;
      END IF;*/
      IF p_irbeventid IS NULL
      THEN
         SELECT s627_irb_event.NEXTVAL
           INTO p_irbeventid
           FROM DUAL;

         INSERT INTO t627_irb_event
                     (c627_irb_event_id, c626_irb_id, c901_action_taken,
                      c627_event_date, c627_notes, c627_created_date,
                      c627_created_by
                     )
              VALUES (p_irbeventid, p_irb_id, p_actiontaken,
                      TO_DATE (p_eventdate, 'MM/DD/YYYY'), p_notes, SYSDATE,
                      p_userid
                     );
      ELSE
         UPDATE t627_irb_event
            SET c901_action_taken = p_actiontaken,
                c627_event_date = TO_DATE (p_eventdate, 'MM/DD/YYYY'),
                c627_notes = p_notes,
                c627_last_updated_by = p_userid,
                c627_last_updated_date = SYSDATE
          WHERE c627_irb_event_id = p_irbeventid AND c627_void_fl IS NULL;
      END IF;

      SELECT COUNT (1)
        INTO v_cnt
        FROM t627_irb_event
       WHERE c626_irb_id = p_irb_id
         AND c901_action_taken = p_actiontaken
         AND c627_void_fl IS NULL;

      IF v_cnt >= 2
      THEN
         -- Duplicate Action Taken Not Allowed.
         v_msg := 'Cannot have duplicate records with	' || v_action_taken;
         raise_application_error ('-20265', v_msg);
      END IF;

      IF (v_action_taken = 'Withdrawn' OR v_action_taken = 'Denied')
      THEN
         UPDATE t626_irb
            SET c901_status =
                   DECODE (v_action_taken,
                           'Withdrawn', '60531',
                           'Denied', '60532'
                          ),
                c626_last_updated_by = p_userid,
                c626_last_updated_date = SYSDATE
          WHERE c626_irb_id = p_irb_id;
      END IF;

      IF v_action_taken = 'Approval'
      THEN
         IF p_review_period IS NOT NULL
         THEN
            SELECT c902_code_nm_alt
              INTO v_month
              FROM t901_code_lookup
             WHERE c901_code_id = p_review_period;

            SELECT ADD_MONTHS (TO_DATE (p_eventdate, 'MM/DD/YYYY'), v_month)
              INTO v_expirationdate
              FROM DUAL;

            UPDATE t626_irb
               SET c901_review_type = NVL (p_review_type, c901_review_type),
                   c901_review_period =
                                     NVL (p_review_period, c901_review_period),
                   c626_approval_date =
                      NVL (TO_DATE (p_eventdate, 'MM/DD/YYYY'),
                           c626_approval_date
                          ),
                   c626_expiry_date = NVL (v_expirationdate, c626_expiry_date),
                   c901_status = 60530                             -- Approved
                                      ,
                   c626_last_updated_by = p_userid,
                   c626_last_updated_date = SYSDATE
             WHERE c626_irb_id = p_irb_id;
         END IF;

         UPDATE t626_irb
            SET c901_review_type = NVL (p_review_type, c901_review_type),
                c901_status = 60530                                -- Approved
                                   ,
                c626_last_updated_by = p_userid,
                c626_last_updated_date = SYSDATE
          WHERE c626_irb_id = p_irb_id;

         SELECT t626.c614_study_site_id, t626.c626_approval_date,
                get_code_name (t626.c901_approval_type),
                get_code_name (t626.c901_approval_reason)
           INTO v_studysiteid, v_approvaldate,
                v_approvaltype_nm,
                v_approvalreason_nm
           FROM t626_irb t626
          WHERE c626_irb_id = p_irb_id;

         IF v_approvaltype_nm = 'Protocol' AND v_approvalreason_nm = 'Initial'
         THEN
            UPDATE t614_study_site
               SET c614_irb_approval_date = v_approvaldate
             WHERE c614_study_site_id = v_studysiteid;
         END IF;
      END IF;
   END gm_cr_sav_irb_event;

   PROCEDURE gm_cr_fch_irb_event (
      p_irb_id       IN       t626_irb.c626_irb_id%TYPE,
      p_irbeventid   IN       t627_irb_event.c627_irb_event_id%TYPE,
      p_irbevent     OUT      TYPES.cursor_type
   )
   AS
   BEGIN
      OPEN p_irbevent
       FOR
          SELECT   t627.c627_irb_event_id irbeventid, t626.c626_irb_id irbid,
                   get_code_name (c901_approval_type) approvaltypename,
                   c901_action_taken actiontaken,
                   c901_review_type reviewtype,
                   c901_review_period reviewperiod,
                   get_code_name (c901_review_type) reviewtypename,
                   get_code_name (c901_review_period) reviewperiodname,
                   get_code_name (c901_action_taken) actiontakenname,
                   TO_CHAR (c627_event_date, 'MM/DD/YYYY') eventdate,
                   c627_notes notes,
                   get_user_name (c627_created_by) updatedby,
                   TO_CHAR (c627_created_date, 'MM/DD/YYYY') updatedate
              FROM t626_irb t626, t627_irb_event t627
             WHERE t626.c626_irb_id = t627.c626_irb_id
               AND t626.c626_irb_id = NVL (p_irb_id, t626.c626_irb_id)
               AND t627.c627_irb_event_id =
                                         NVL (p_irbeventid, c627_irb_event_id)
               AND t626.c626_void_fl IS NULL
               AND t627.c627_void_fl IS NULL
          ORDER BY c627_created_date DESC;
   END gm_cr_fch_irb_event;

   PROCEDURE gm_cr_sav_irb_media (
      p_irb_id       IN       t626_irb.c626_irb_id%TYPE,
      p_mediatype    IN       t628_irb_media.c901_media_type%TYPE,
      p_notes        IN       t628_irb_media.c628_notes%TYPE,
      p_userid       IN       t628_irb_media.c628_created_by%TYPE,
      p_irbmediaid   IN OUT   t628_irb_media.c628_irb_media_id%TYPE
   )
   AS
      v_cnt   NUMBER;
   BEGIN
      SELECT COUNT (1)
        INTO v_cnt
        FROM t627_irb_event t627, t626_irb t626
       WHERE t626.c626_irb_id = p_irb_id
         AND t626.c626_irb_id = t627.c626_irb_id
         AND t626.c901_status = 60530
         AND t626.c901_approval_type = 60522                   -- Advertisment
         AND c901_action_taken = 60550                             -- Approval
         AND c627_void_fl IS NULL;

      IF v_cnt = 0
      THEN
         -- Need to change the Error code.
         raise_application_error ('-20265', '');
      END IF;

      IF p_irbmediaid IS NULL
      THEN
         SELECT s628_irb_media.NEXTVAL
           INTO p_irbmediaid
           FROM DUAL;

         INSERT INTO t628_irb_media
                     (c628_irb_media_id, c626_irb_id, c901_media_type,
                      c628_notes, c628_created_by, c628_created_date
                     )
              VALUES (p_irbmediaid, p_irb_id, p_mediatype,
                      p_notes, p_userid, SYSDATE
                     );
      ELSE
         UPDATE t628_irb_media
            SET c901_media_type = p_mediatype,
                c628_notes = p_notes,
                c628_last_updated_by = p_userid,
                c628_last_updated_date = SYSDATE
          WHERE c628_irb_media_id = p_irbmediaid AND c628_void_fl IS NULL;
      END IF;
   END gm_cr_sav_irb_media;

   PROCEDURE gm_cr_fch_irb_media (
      p_irb_id       IN       t628_irb_media.c626_irb_id%TYPE,
      p_irbmediaid   IN       t628_irb_media.c628_irb_media_id%TYPE,
      p_irbmedia     OUT      TYPES.cursor_type
   )
   AS
   BEGIN
      OPEN p_irbmedia
       FOR
          SELECT   t628.c628_irb_media_id irbmediaid, t626.c626_irb_id irbid,
                   get_code_name (c901_approval_type) approvaltypename,
                   c901_media_type mediatype,
                   get_code_name (c901_media_type) mediatypename,
                   c628_notes irbmedianotes,
                   get_user_name (c628_created_by) updatedby,
                   TO_CHAR (c628_created_date, 'MM/DD/YYYY') updatedate
              FROM t626_irb t626, t628_irb_media t628
             WHERE t626.c626_irb_id = t628.c626_irb_id
               AND t626.c626_irb_id = NVL (p_irb_id, t626.c626_irb_id)
               AND t628.c628_irb_media_id =
                                         NVL (p_irbmediaid, c628_irb_media_id)
               AND t626.c626_void_fl IS NULL
               AND t628.c628_void_fl IS NULL
          ORDER BY c628_created_date DESC;
   END gm_cr_fch_irb_media;

   PROCEDURE gm_cr_irb_email
   AS
      CURSOR set_8_weeks_cursor
      IS
         SELECT DISTINCT c626_irb_id,
                         get_code_name (c901_approval_type) approval_type,
                         t614.c614_study_site_id, t614.c614_site_id site_id,
                         get_sh_account_name (t614.c704_account_id)
                                                                   site_name,
                         c901_approval_type,
                         get_user_emailid (t616.c101_party_id) sc_email,
                         get_user_emailid (t614.c101_user_id) cra_email,
                         t611.c611_study_nm study_name,
                         c626_expiry_date expiry_date,
                         ADD_MONTHS (t626.c626_expiry_date,
                                     -8 / 4
                                    ) renewal_date
                    FROM t616_site_party_mapping t616,
                         t626_irb t626,
                         t614_study_site t614,
                         t611_clinical_study t611
                   WHERE t626.c614_study_site_id = t614.c614_study_site_id
                     AND t614.c614_study_site_id = t616.c614_study_site_id
                     AND t616.c901_party_type = '7007'
                     AND t616.c616_primary_fl = 'Y'
                     AND MONTHS_BETWEEN (LAST_DAY (t626.c626_expiry_date),
                                         LAST_DAY (SYSDATE)
                                        ) = 2                       -- 8 Weeks
                     AND (t626.c626_expiry_date,
                          t626.c901_approval_type,
                          t626.c614_study_site_id
                         ) IN (
                            SELECT   MAX (expiry_date), approval_type,
                                     study_site_id
                                FROM (SELECT c626_expiry_date expiry_date,
                                             c901_approval_type approval_type,
                                             c614_study_site_id study_site_id
                                        FROM t626_irb a
                                       WHERE c626_void_fl IS NULL
                                         AND c901_status = '60530'
                                         AND c901_approval_type NOT IN
                                                                    ('60522'))
                            GROUP BY approval_type, study_site_id)
                     AND t626.c901_approval_type IN
                                  (60520, 60521) -- Protocol, Informed Consent
                     AND t614.c611_study_id = t611.c611_study_id
                     AND t626.c626_void_fl IS NULL
                     AND t616.c616_void_fl IS NULL
                     AND t614.c614_void_fl IS NULL;

      CURSOR set_4_weeks_cursor
      IS
         SELECT DISTINCT c626_irb_id,
                         get_code_name (c901_approval_type) approval_type,
                         t614.c614_study_site_id, t614.c614_site_id site_id,
                         get_sh_account_name (t614.c704_account_id) site_name,
                         get_user_emailid (t616.c101_party_id) sc_email,
                         get_user_emailid (t614.c101_user_id) cra_email,
                         t611.c611_study_nm study_name,
                         c626_expiry_date expiry_date,
                         ADD_MONTHS (t626.c626_expiry_date,
                                     -4 / 4
                                    ) renewal_date
                    FROM t616_site_party_mapping t616,
                         t626_irb t626,
                         t614_study_site t614,
                         t611_clinical_study t611
                   WHERE t626.c614_study_site_id = t614.c614_study_site_id
                     AND t614.c614_study_site_id = t616.c614_study_site_id
                     AND t616.c901_party_type = '7007'
                     AND t616.c616_primary_fl = 'Y'
                     AND MONTHS_BETWEEN (LAST_DAY (t626.c626_expiry_date),
                                         LAST_DAY (SYSDATE)
                                        ) = 1                       -- 4 Weeks
                     AND (t626.c626_expiry_date,
                          t626.c901_approval_type,
                          t626.c614_study_site_id
                         ) IN (
                            SELECT   MAX (expiry_date), approval_type,
                                     study_site_id
                                FROM (SELECT c626_expiry_date expiry_date,
                                             c901_approval_type approval_type,
                                             c614_study_site_id study_site_id
                                        FROM t626_irb a
                                       WHERE c626_void_fl IS NULL
                                         AND c901_status = '60530'
                                         AND c901_approval_type NOT IN
                                                                    ('60522'))
                            GROUP BY approval_type, study_site_id)
                     AND t626.c901_approval_type IN
                                  (60520, 60521) -- Protocol, Informed Consent
                     AND t614.c611_study_id = t611.c611_study_id
                     AND t626.c626_void_fl IS NULL
                     AND t616.c616_void_fl IS NULL
                     AND t614.c614_void_fl IS NULL;

      CURSOR expiration_cursor
      IS
         SELECT DISTINCT c626_irb_id,
                         get_code_name (c901_approval_type) approval_type,
                         t614.c614_study_site_id, t614.c614_site_id site_id,
                         get_sh_account_name (t614.c704_account_id) site_name,
                         c901_approval_type,
                         get_user_emailid (t616.c101_party_id) sc_email,
                         get_user_emailid (t614.c101_user_id) cra_email,
                         t611.c611_study_nm study_name,
                         c626_expiry_date expiry_date,
                         t626.c626_expiry_date + 7 renewal_date
                    FROM t616_site_party_mapping t616,
                         t626_irb t626,
                         t614_study_site t614,
                         t611_clinical_study t611
                   WHERE t626.c614_study_site_id = t614.c614_study_site_id
                     AND t614.c614_study_site_id = t616.c614_study_site_id
                     AND t616.c901_party_type = '7007'
                     AND t616.c616_primary_fl = 'Y'
                     AND t626.c626_expiry_date + 7 = TRUNC (SYSDATE)
                     AND (t626.c626_expiry_date,
                          t626.c901_approval_type,
                          t626.c614_study_site_id
                         ) IN (
                            SELECT   MAX (expiry_date), approval_type,
                                     study_site_id
                                FROM (SELECT c626_expiry_date expiry_date,
                                             c901_approval_type approval_type,
                                             c614_study_site_id study_site_id
                                        FROM t626_irb a
                                       WHERE c626_void_fl IS NULL
                                         AND c901_status = '60530'
                                         AND c901_approval_type NOT IN
                                                                    ('60522'))
                            GROUP BY approval_type, study_site_id)
                     AND t626.c901_approval_type IN
                                  (60520, 60521) -- Protocol, Informed Consent
                     AND t614.c611_study_id = t611.c611_study_id
                     AND t626.c626_void_fl IS NULL
                     AND t616.c616_void_fl IS NULL
                     AND t614.c614_void_fl IS NULL;

      v_email_cc   VARCHAR2 (1000)
                      := get_rule_value ('IRB_NOTIF_USERS', 'IRB_NOTIF_USERS');
      v_body       VARCHAR2 (4000);
      v_subject    VARCHAR2 (4000);
      v_to_mail    VARCHAR2 (4000);
      v_crlf       VARCHAR2 (2)    := CHR (13) || CHR (10);
      v_start_dt   DATE;
   BEGIN
      FOR weeks_8_val IN set_8_weeks_cursor
      LOOP
         v_subject :=
               'IRB renewal notification for '
            || weeks_8_val.site_name
            || ' - '
            || weeks_8_val.site_id;
         v_to_mail :=
               weeks_8_val.cra_email
            || ','
            || v_email_cc
            || ','
            || weeks_8_val.sc_email;
         v_body :=
               'The Protocol and Informed Consent for '
            || weeks_8_val.study_name
            || ' requires approval for renewal by your IRB by '
            || TO_CHAR (weeks_8_val.renewal_date, 'MON DD, YYYY')
            || '. Please note that it is due to expire on '
            || TO_CHAR (weeks_8_val.expiry_date, 'MON DD, YYYY')
            || '. Please contact your IRB to initiate the renewal process.'
            || v_crlf
            || v_crlf
            || 'Thank you for your continued participation,'
            || v_crlf
            || v_crlf
            || 'Globus Medical Clinical Affairs';
         gm_com_send_email_prc (v_to_mail, v_subject, v_body);
      END LOOP;

      FOR weeks_4_val IN set_4_weeks_cursor
      LOOP
         v_subject :=
               'IRB renewal notification for '
            || weeks_4_val.site_name
            || ' - '
            || weeks_4_val.site_id;
         v_to_mail :=
               weeks_4_val.cra_email
            || ','
            || v_email_cc
            || ','
            || weeks_4_val.sc_email;
         v_body :=
               'The Protocol and Informed Consent for '
            || weeks_4_val.study_name
            || ' requires approval for renewal by your IRB by '
            || TO_CHAR (weeks_4_val.renewal_date, 'MON DD, YYYY')
            || '. Please note that it is due to expire on '
            || TO_CHAR (weeks_4_val.expiry_date, 'MON DD, YYYY')
            || '. Please contact your IRB to initiate the renewal process.'
            || v_crlf
            || v_crlf
            || 'Thank you for your continued participation,'
            || v_crlf
            || v_crlf
            || 'Globus Medical Clinical Affairs';
         gm_com_send_email_prc (v_to_mail, v_subject, v_body);
      END LOOP;

      FOR expiration_val IN expiration_cursor
      LOOP
         v_subject :=
               'IRB renewal notification for '
            || expiration_val.site_name
            || ' - '
            || expiration_val.site_id;
         v_to_mail := expiration_val.cra_email || ',' || v_email_cc;
         v_body :=
               'The Protocol and Informed Consent for '
            || expiration_val.study_name
            || ' required renewal by '
            || TO_CHAR (expiration_val.expiry_date, 'MON DD, YYYY')
            || '. If renewal has been approved, please update the IRB Tracker with the approval information.'
            || v_crlf
            || v_crlf
            || 'Thank you for your continued participation,'
            || v_crlf
            || v_crlf
            || 'Globus Medical Clinical Affairs';
         gm_com_send_email_prc (v_to_mail, v_subject, v_body);
      END LOOP;

      gm_pkg_cm_job.gm_cm_notify_load_info
                                     (v_start_dt,
                                      SYSDATE,
                                      'S',
                                      'SUCCESS',
                                      'gm_pkg_cr_site_monitor.gm_cr_irb_email'
                                     );
      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         --
         gm_pkg_cm_job.gm_cm_notify_load_info
                                    (v_start_dt,
                                     SYSDATE,
                                     'E',
                                     SQLERRM,
                                     'gm_pkg_cr_site_monitor.gm_cr_irb_email'
                                    );
         COMMIT;
   END gm_cr_irb_email;
END gm_pkg_cr_irb;
/