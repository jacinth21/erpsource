/* Formatted on 2010/08/18 11:37 (Formatter Plus v4.8.0) */
--@"c:\database\packages\clinical\gm_pkg_cr_common.pkg";

CREATE OR REPLACE PACKAGE gm_pkg_cr_common
IS
--
/*******************************************************
 * Purpose: Package holds Clinical Research
 * Save and Fetch Methods
 * Created By VPrasath
 *******************************************************/
--
/*******************************************************
 * Purpose: This Procedure is used to save the
 * sites mapped to the study
 *******************************************************/
--
	PROCEDURE gm_cr_sav_site_map_info (
		p_studyid			 IN 	  t614_study_site.c611_study_id%TYPE
	  , p_accid 			 IN 	  t614_study_site.c704_account_id%TYPE
	  , p_siteid			 IN 	  t614_study_site.c614_site_id%TYPE
	  , p_craid 			 IN 	  t614_study_site.c101_user_id%TYPE
	  , p_userid			 IN 	  t614_study_site.c614_created_by%TYPE
	  , p_irbtype			 IN 	  t614_study_site.c901_irb_type%TYPE
	  , p_stdypktdt 		 IN 	  VARCHAR2
	  , p_tin_no   			 IN 	  VARCHAR2
	  , p_study_siteid_out	 OUT	  VARCHAR2
	);

--
--
/*******************************************************
 * Purpose: This Procedure is used to fetch the
 * sites mapped to the study
 *******************************************************/
--
	PROCEDURE gm_cr_fch_site_map_info (
		p_studyid	 IN 	  t614_study_site.c611_study_id%TYPE
	  , p_site_cur	 OUT	  TYPES.cursor_type
	);

--
--
/*******************************************************
 * Purpose: This Procedure is used to fetch all the sites present
 *******************************************************/
--
	PROCEDURE gm_cr_fch_site_map (
		p_study_site_id   IN	   t614_study_site.c614_study_site_id%TYPE
	  , p_site_cur		  OUT	   TYPES.cursor_type
	);

--
/*******************************************************
 * Purpose: This Procedure is used to fetch all the study site details
 *******************************************************/
--
	PROCEDURE gm_cr_fch_site_list (
		p_study_id	 IN 	  t614_study_site.c611_study_id%TYPE
	  , p_site_cur	 OUT	  TYPES.cursor_type
	);

--
--
/*******************************************************
 * Purpose: This Procedure is used to save the
 * Surgeons mapped to the site
 *******************************************************/
--
	PROCEDURE gm_cr_sav_surgeon_map_info (
		p_study_id		  IN   t614_study_site.c611_study_id%TYPE
	  , p_study_site_id   IN   t614_study_site.c614_study_site_id%TYPE
	  , p_firstname 	  IN   t101_party.c101_first_nm%TYPE
	  , p_lastname		  IN   t101_party.c101_last_nm%TYPE
	  , p_party_id		  IN   t101_party.c101_party_id%TYPE
	  , p_surgeontype	  IN   t615_site_surgeon.c901_surgeon_type%TYPE
	  , p_userid		  IN   t615_site_surgeon.c615_created_by%TYPE
	);

--
--
/*******************************************************
 * Purpose: This Procedure is used to fetch the
 * surgeons mapped to  study and site
 *******************************************************/
--
	PROCEDURE gm_cr_fch_surgeon_map_details (
		p_study_site_id   IN	   t614_study_site.c614_study_site_id%TYPE
	  , p_surgeontype	  IN	   t615_site_surgeon.c901_surgeon_type%TYPE
	  , p_surgeon_cur	  OUT	   TYPES.cursor_type
	);

	PROCEDURE gm_cr_fch_surgeon_map_details (
		p_study_site_id   IN	   t614_study_site.c614_study_site_id%TYPE
	  , p_surgeon_cur	  OUT	   TYPES.cursor_type
	);

--
--
/*******************************************************
 * Purpose: This Procedure is used to fetch the
 * surgeons mapped to the study and site
 *******************************************************/
--
	PROCEDURE gm_cr_fch_surgeon_map_info (
		p_site_surgeon_id	IN		 t615_site_surgeon.c615_site_surgeon_id%TYPE
	  , p_surgeon_cur		OUT 	 TYPES.cursor_type
	);

--
/***************************************************************************************
 * Description	 :This procedure is called to FETCH the Pending Forms list of form that need to be filled
 * Parameters	 :N/A
 *				 :p_recordset	- holds list of list of due form for the entire patient
 **************************************************************************************/
--
	PROCEDURE gm_pending_form_list (
		p_study_id	  IN	   t611_clinical_study.c611_study_id%TYPE
	  , p_cra_id	  IN	   t101_user.c101_user_id%TYPE
	  , p_rsetforms   OUT	   TYPES.cursor_type
	);

--
/***************************************************************************************
 * Description	 :This procedure is called to FETCH the Out Of Window list of form that need to be filled
 * Parameters	 :N/A
 *				 :p_recordset	- holds list of list of due form for the entire patient
 **************************************************************************************/
--
	PROCEDURE gm_out_of_window_verify_list (
		p_study_id			IN		 t611_clinical_study.c611_study_id%TYPE
	  , p_cra_id			IN		 t101_user.c101_user_id%TYPE
	  , p_rsetoutofwindow	OUT 	 TYPES.cursor_type
	);

--
/***************************************************************************************
 * Description	 :This procedure is called to FETCH the Pending Verification of form that need to be filled
 * Parameters	 :N/A
 *				 :p_recordset	- holds list of list of due form for the entire patient
 **************************************************************************************/
--
	PROCEDURE gm_pending_verification_list (
		p_study_id			 IN 	  t611_clinical_study.c611_study_id%TYPE
	  , p_cra_id			 IN 	  t101_user.c101_user_id%TYPE
	  , p_rsetverification	 OUT	  TYPES.cursor_type
	);

	--
/***************************************************************************************
 * Description	 :This procedure is called to FETCH the Pending Forms list of form that need to be filled
 * Parameters	 :N/A
 *				 :p_recordset	- holds list of list of due form for the entire patient
 **************************************************************************************/
--
	PROCEDURE gm_load_no_of_event (
		p_study_id	   IN		VARCHAR2
	  , p_form_id	   IN		VARCHAR2
	  , p_patient_id   IN		VARCHAR2
	  , p_rsetforms    OUT		TYPES.cursor_type
	);

	--
/*******************************************************
 * Purpose: This Procedure is used to fetch
 * the data about study

 *******************************************************/
--
	PROCEDURE gm_all_study_form (
		p_studyid	  IN	   t611_clinical_study.c611_study_id%TYPE
	  , p_recordset   OUT	   TYPES.cursor_type
	);

--
/*******************************************************
 * Purpose: This Procedure is used to fetch
 * the data about study for specific period

 *******************************************************/
--
	PROCEDURE gm_fch_prd_study_form (
		p_studyid	  IN	   t611_clinical_study.c611_study_id%TYPE
	  , p_periodid	  IN	   t613_study_period.c901_study_period%TYPE
	  , p_recordset   OUT	   TYPES.cursor_type
	);

--
/***************************************************************************************
 * Description	 :This procedure is called to FETCH the Pending Forms list of form that need to be filled
 * Parameters	 :N/A
 *				 :p_recordset	- holds list of list of due form for the entire patient
 **************************************************************************************/
--
	PROCEDURE gm_load_no_of_event_des (
		p_study_id	   IN		VARCHAR2
	  , p_form_id	   IN		VARCHAR2
	  , p_patient_id   IN		VARCHAR2
	   , p_voidFlag	   IN		VARCHAR2
	  , p_rsetforms    OUT		TYPES.cursor_type
	);

--
/***************************************************************************************
 * Description	 :This procedure is called to save the patient details
 * Parameters	 :N/A
 **************************************************************************************/
--
	PROCEDURE gm_sav_patient_record (
		p_patient_id	   IN		t624_patient_record.c621_patient_id%TYPE
	  , p_timepoint 	   IN		t624_patient_record.c901_study_period%TYPE
	  , p_type			   IN		t624_patient_record.c901_record_grp%TYPE
	  , p_view			   IN		t624_patient_record.c901_record_type%TYPE
	  , p_filename		   IN		t903_upload_file_list.c903_file_name%TYPE
	  , p_userid		   IN		t624_patient_record.c624_created_by%TYPE
	  , p_studyid		   IN		t624_patient_record.c611_study_id%TYPE
	  , p_record_dt 	   IN		VARCHAR2
	  , p_patient_rec_id   IN OUT	t624_patient_record.c624_patient_rec_id%TYPE
	);

--
/*******************************************************
 * Purpose: This Procedure is used to fetch
 * the data about patient records

 *******************************************************/
--
	PROCEDURE gm_cr_fch_patient_record (
		p_patient_id	   IN		t624_patient_record.c621_patient_id%TYPE
	  , p_timepointid	   IN		t624_patient_record.c901_study_period%TYPE
	  , p_viewid		   IN		t624_patient_record.c901_record_type%TYPE
	  , p_typeid		   IN		t624_patient_record.c901_record_grp%TYPE
	  , p_patient_rec_id   IN		t624_patient_record.c624_patient_rec_id%TYPE
	  , p_study_id		   IN		t614_study_site.c611_study_id%TYPE
	  , p_site_id		   IN		t614_study_site.c614_site_id%TYPE
	  , p_patient_record   OUT		TYPES.cursor_type
	);

--
/*******************************************************
 * Purpose: This procedure will create/update a record in the T622_Patient_list  table
 *******************************************************/
--
	PROCEDURE gm_sav_patient_list (
		p_date					 IN 	  VARCHAR2
	  , p_initital				 IN 	  t622_patient_list.c622_initials%TYPE
	  , p_cal_value 			 IN 	  t622_patient_list.c621_cal_value%TYPE
	  , p_form_id				 IN 	  t601_form_master.c601_form_id%TYPE
	  , p_patient_id			 IN 	  t621_patient_master.c621_patient_id%TYPE
	  , p_type					 IN 	  t622_patient_list.c901_type%TYPE
	  , p_study_period_id		 IN 	  t613_study_period.c613_study_period_id%TYPE
	  , p_out_of_window_fl		 IN 	  t622_patient_list.c622_out_of_window_fl%TYPE
	  , p_missing_follow_up_fl	 IN 	  t622_patient_list.c622_missing_follow_up_fl%TYPE
	  , p_patient_event_no		 IN 	  t622_patient_list.c622_patient_event_no%TYPE
	  , p_user					 IN 	  t622_patient_list.c622_created_by%TYPE
	  , p_pat_list_id			 IN OUT   t622_patient_list.c622_patient_list_id%TYPE
	);

--
/*******************************************************
 * Purpose: This procedure will create/update a record in the T623_PATIENT_ANSWER_LIST table
 *******************************************************/
--
	PROCEDURE gm_sav_patient_answer_list (
		p_pat_list_id		IN		 t622_patient_list.c622_patient_list_id%TYPE
	  , p_question_id		IN		 t602_question_master.c602_question_id%TYPE
	  , p_ans_list_id		IN		 t605_answer_list.c605_answer_list_id%TYPE
	  , p_ans_grp_id		IN		 t623_patient_answer_list.c603_answer_grp_id%TYPE
	  , p_ans_desc			IN		 t623_patient_answer_list.c904_answer_ds%TYPE
	  , p_form_id			IN		 t601_form_master.c601_form_id%TYPE
	  , p_patient_id		IN		 t621_patient_master.c621_patient_id%TYPE
	  , p_user				IN		 t623_patient_answer_list.c623_created_by%TYPE
	  , p_pat_ans_list_id	IN OUT	 t623_patient_answer_list.c623_patient_ans_list_id%TYPE
	);

--
/*******************************************************
 * Purpose: This procedure is used to verify if there is any patient who is having lock set on the form (SD-6).
   This is a check to get a mismatch between the patient form downloaded and the patient for which the data is uploaded.
 *******************************************************/
--
	PROCEDURE gm_fch_patient_form_lock (
		p_studyid			   IN		t624_patient_record.c611_study_id%TYPE
	  , p_upload_form_id	   IN		t601_form_master.c601_form_id%TYPE
	  , p_downloaded_form_id   IN		t601_form_master.c601_form_id%TYPE
	  , p_recordset 		   OUT		TYPES.cursor_type
	);

--
/*******************************************************
 * Purpose: This procedure is used to release the form lock of the patient.
The form (SD-6)will be locked during download and when the data is uploaded for the patient, the form (SD-6) should be un-locked
 *******************************************************/
--
	PROCEDURE gm_sav_patient_verify_list (
		p_patient_id	IN	 t624_patient_record.c621_patient_id%TYPE
	  , p_form_id		IN	 t601_form_master.c601_form_id%TYPE
	  , p_timepointid	IN	 t624_patient_record.c901_study_period%TYPE
	  , p_exam_date 	IN	 VARCHAR2
	  , p_userid		IN	 t625_patient_verify_list.c625_last_updated_by%TYPE
	);

/*******************************************************
 * Purpose: This Procedure is used to fetch
 * the all forms for study

 *******************************************************/
--
	PROCEDURE gm_study_form_list (
		p_studyid	  IN	   t611_clinical_study.c611_study_id%TYPE
	  , p_recordset   OUT	   TYPES.cursor_type
	);

--

	/*******************************************************
 * Purpose: This Procedure is used to fetch
 * site coordinator list for a given study site id.

 *******************************************************/
--
	PROCEDURE gm_fch_site_coordinator (
		p_studysiteid	IN		 t611_clinical_study.c611_study_id%TYPE
	  , p_recordset 	OUT 	 TYPES.cursor_type
	);

/*******************************************************
 * Purpose: This Function is used to fetch
  event desc.

 *******************************************************/
--
	FUNCTION get_event_desc (
		p_study_id	   IN	VARCHAR2
	  , p_form_id	   IN	VARCHAR2
	  , p_patient_id   IN	VARCHAR2
	  , p_eventno	   IN	VARCHAR2
	)
		RETURN VARCHAR2;

--
/*******************************************************
 * Purpose: This Procedure is used to fetch
 * patient details.

 *******************************************************/
--
	PROCEDURE gm_fch_patient_details (
		p_patientid   IN	   t621_patient_master.c621_patient_id%TYPE
	  , p_recordset   OUT	   TYPES.cursor_type
	);
/*******************************************************
 * Purpose: This Function is used to return valid date 
  based on passed user parameter 
 *******************************************************/
--
FUNCTION get_valid_cr_date (
   p_strdate         VARCHAR2
)
    RETURN DATE;
    
/*******************************************************
	 * Purpose: This Function is used to return valid number 
	  based on passed user parameter 
*******************************************************/
	--
	FUNCTION get_valid_cr_number (
	p_strnumber VARCHAR2
	)
	   RETURN NUMBER;
	   
/*******************************************************************
* Description : this procedure is used to update patient and site map
* changes to v621_patient_study_list
* Author      : Dhana Reddy
*******************************************************************/
PROCEDURE gm_sav_site_patient_details (
        p_patient_id    IN t621_patient_master.c621_patient_id%TYPE,
        p_study_site_id IN t621_patient_master.c611_study_id%TYPE,
        p_study_id      IN t614_study_site.c614_site_id%TYPE) ;

/*******************************************************
* Purpose: This Procedure is used to fetch study details.
*******************************************************/
PROCEDURE gm_fch_study_dtls (
	        p_study_list OUT TYPES.cursor_type) ;
/******************************************************************************************
* Purpose: This Procedure is used to update study status, comments and reason details.
******************************************************************************************/
PROCEDURE gm_sav_study_lock_dtls (
        p_txn_id       IN t907_cancel_log.c907_ref_id%TYPE,
        p_cancelreason IN t907_cancel_log.c901_cancel_cd%TYPE,
        p_cancelid     IN t907_cancel_log.c907_cancel_log_id%TYPE,
        p_comments     IN t907_cancel_log.c907_comments%TYPE,
        p_userid       IN t102_user_login.c102_user_login_id%TYPE) ;
/******************************************************************************************
* Purpose: This function is used to return the study status.
******************************************************************************************/
    --
FUNCTION get_study_status (
            p_study_id t611_clinical_study.c611_study_id%TYPE)
        RETURN NUMBER;
/******************************************************************************************
* Purpose: This procedure is used to fetch the study history details.
******************************************************************************************/
PROCEDURE gm_fch_study_history (
            p_study_id IN t611_clinical_study.c611_study_id%TYPE,
            p_study_history OUT TYPES.cursor_type) ;

/******************************************************************************************
* Purpose: This procedure is used to save the study log details.
******************************************************************************************/
PROCEDURE gm_sav_study_log (
        p_study_id     IN t611_clinical_study_log.c611_study_id%TYPE,
        p_study_nm     IN t611_clinical_study_log.c611_study_log_nm%TYPE,
        p_log_status   IN t611_clinical_study_log.c901_study_log_status%TYPE,
        p_log_comments IN t611_clinical_study_log.c611_study_log_comments%TYPE,
        p_log_reason   IN t611_clinical_study_log.c901_study_log_reason%TYPE,
        p_userid       IN t102_user_login.c102_user_login_id%TYPE) ;  
END gm_pkg_cr_common;
/
