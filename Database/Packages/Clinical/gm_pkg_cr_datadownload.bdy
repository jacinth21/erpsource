/* Formatted on 04/09/2010 11:28 (Formatter Plus v4.8.0) */
--@"C:\database\Packages\Clinical\gm_pkg_cr_datadownload.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_cr_datadownload
IS
--
/*******************************************************
 * Purpose: Package holds Clinical Research Data Download
 * Report Methods
 * Created By hparikh
 *******************************************************/

	--
/*******************************************************
 * Purpose: This Procedure is used to fetch
 * list of download form
 *******************************************************/
--
	PROCEDURE gm_fetch_download_form (
		p_study_id	  IN	   t611_clinical_study.c611_study_id%TYPE
	  , p_recordset   OUT	   TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_recordset
		 FOR
			 SELECT c601_form_id ID, c612_study_form_ds NAME
			   FROM t612_study_form_list
			  WHERE c612_download_fl = 'Y' AND c611_study_id = p_study_id AND NVL (c612_delete_fl, 'N') = 'N';
	END gm_fetch_download_form;

--
/*******************************************************
 * Purpose: This Procedure is used to fetch
 * form data for a given form.
 *******************************************************/
--
	PROCEDURE gm_fetch_form_data (
		p_study_id		   IN		t611_clinical_study.c611_study_id%TYPE
	  , p_form_id		   IN		t612_study_form_list.c601_form_id%TYPE
	  , p_userid		   IN		t625_patient_verify_list.c101_user_id%TYPE
	  , p_filename		   IN		t903_upload_file_list.c903_file_name%TYPE
	  , p_calculatedsfid   IN		NUMBER
	  , p_headerlist	   OUT		TYPES.cursor_type
	  , p_datalist		   OUT		TYPES.cursor_type
	  , p_headerdata	   OUT		TYPES.cursor_type
	)
	AS
		v_cnt		   NUMBER;
		v_msg		   VARCHAR2 (1000);
		v_upload_file_list t903_upload_file_list.c903_upload_file_list%TYPE;
		v_pat_verify_list_id t625_patient_verify_list.c625_pat_verify_list_id%TYPE;

		CURSOR patient_list
		IS
			SELECT t622.c622_patient_list_id plid
			  FROM t622_patient_list t622, t621_patient_master t621, t613_study_period t613
			 WHERE 
			 -- Downloading all SD6/SC6 which are verified. SD6 may or maynot have matching SF-36.
			 
			 /*(t613.c613_study_period_ds, t622.c621_patient_id) IN (
					   SELECT t613.c613_study_period_ds, a.c621_patient_id
						 FROM t622_patient_list a, t613_study_period t613
						WHERE a.c601_form_id = p_form_id AND t613.c613_study_period_id = a.c613_study_period_id
					   MINUS
					   SELECT t613.c613_study_period_ds, a.c621_patient_id
						 FROM t622_patient_list a, t613_study_period t613
						WHERE a.c601_form_id = p_calculatedsfid AND t613.c613_study_period_id = a.c613_study_period_id)
			   AND */
			    t622.c601_form_id = p_form_id
			   AND t621.c611_study_id = p_study_id
			   AND t621.c621_patient_id = t622.c621_patient_id
			   AND NVL (t622.c622_verify_fl, 'N') = 'Y'
			   AND t621.c621_delete_fl = 'N'
			   AND t622.c622_delete_fl = 'N'
			   AND t613.c613_study_period_id = t622.c613_study_period_id
			   AND t622.c622_date IS NOT NULL
			   AND t622.c622_missing_follow_up_fl IS NULL;
	BEGIN
		SELECT COUNT (*)
		  INTO v_cnt
		  FROM t612_study_form_list
		 WHERE c611_study_id = p_study_id
		   AND c601_form_id = p_form_id
		   AND c612_download_lock_fl IS NOT NULL
		   AND NVL (c612_delete_fl, 'N') = 'N';

		IF v_cnt > 0
		THEN
			raise_application_error (-20255, '');
		END IF;

		gm_pkg_upload_info.gm_sav_upload_info (p_form_id, p_filename, p_userid, 60446, v_upload_file_list);

		UPDATE t612_study_form_list
		   SET c612_download_lock_fl = 'Y'
			 , c903_upload_file_list = v_upload_file_list
			 , c612_last_updated_by = p_userid
			 , c612_last_updated_date = TRUNC (SYSDATE)
		 WHERE c611_study_id = p_study_id AND c601_form_id = p_form_id;

		-- 1255 is part of group GPLOG for 'Clinical Cancel Data Download'
		gm_update_log (v_upload_file_list, '', 1255, p_userid, v_msg);

		OPEN p_headerlist
		 FOR
			 SELECT   c906_rule_value ansgrpid
				 FROM t906_rules
				WHERE c906_rule_grp_id = p_study_id || '_' || p_form_id
			 ORDER BY c906_rule_seq_id;

		OPEN p_headerdata
		 FOR
			 SELECT DISTINCT t621.c621_patient_ide_no || '_' || TO_CHAR (c622_date, 'MM/DD/YYYY') ID
						FROM t622_patient_list t622
						   , t621_patient_master t621
						   , t623_patient_answer_list t623
						   , t605_answer_list t605
						   , t604_form_ques_link t604
						   , t906_rules t906
						   , t613_study_period t613
					   WHERE 
					   -- Downloading all SD6/SC6 which are verified. SD6 may or maynot have matching SF-36.
			 
			 			/*
					   (t613.c613_study_period_ds, t622.c621_patient_id) IN (
								 SELECT t613.c613_study_period_ds, a.c621_patient_id
								   FROM t622_patient_list a, t613_study_period t613
								  WHERE a.c601_form_id = p_form_id
										AND t613.c613_study_period_id = a.c613_study_period_id
								 MINUS
								 SELECT t613.c613_study_period_ds, a.c621_patient_id
								   FROM t622_patient_list a, t613_study_period t613
								  WHERE a.c601_form_id = p_calculatedsfid
									AND t613.c613_study_period_id = a.c613_study_period_id)
						 AND*/
						 t622.c601_form_id = p_form_id
						 AND t621.c611_study_id = p_study_id
						 AND t621.c621_patient_id = t622.c621_patient_id
						 AND NVL (t622.c622_verify_fl, 'N') = 'Y'
						 AND t623.c601_form_id = t622.c601_form_id
						 AND t623.c621_patient_id = t622.c621_patient_id
						 AND t623.c622_patient_list_id = t622.c622_patient_list_id
						 AND t605.c605_answer_list_id = t623.c605_answer_list_id
						 AND t604.c602_question_id = t623.c602_question_id
						 AND t604.c601_form_id = t622.c601_form_id
						 AND t623.c623_delete_fl = 'N'
						 AND t621.c621_delete_fl = 'N'
						 AND NVL(t622.C622_DELETE_FL,'N')='N'
						 -- The condition will get all the answers based on the answer id present in	rules table.
						 AND t906.c906_rule_grp_id = p_study_id || '_' || p_form_id
						 AND t906.c906_rule_id = t623.c603_answer_grp_id
						 AND t613.c613_study_period_id = t622.c613_study_period_id
						 AND t622.c622_date IS NOT NULL
						 AND t622.c622_missing_follow_up_fl IS NULL;

		OPEN p_datalist
		 FOR
			 SELECT   t621.c621_patient_ide_no || '_' || TO_CHAR (c622_date, 'MM/DD/YYYY') ID
					, t621.c621_patient_ide_no || '_' || TO_CHAR (c622_date, 'MM/DD/YYYY') dummy_ansgrpid
					, t906.c906_rule_value ansgrpid, t605.c605_answer_value ans, t621.c621_patient_ide_no recordid
					, TO_CHAR (c622_date, 'MM/DD/YYYY') surveydate, t604.c604_seq_no qno, t605.c605_answer_value ans
				 FROM t622_patient_list t622
					, t621_patient_master t621
					, t623_patient_answer_list t623
					, t605_answer_list t605
					, t604_form_ques_link t604
					, t906_rules t906
					, t613_study_period t613
				WHERE 
				-- Downloading all SD6/SC6 which are verified. SD6 may or maynot have matching SF-36.
			 
			 	/*
				(t613.c613_study_period_ds, t622.c621_patient_id) IN (
						  SELECT t613.c613_study_period_ds, a.c621_patient_id
							FROM t622_patient_list a, t613_study_period t613
						   WHERE a.c601_form_id = p_form_id AND t613.c613_study_period_id = a.c613_study_period_id
						  MINUS
						  SELECT t613.c613_study_period_ds, a.c621_patient_id
							FROM t622_patient_list a, t613_study_period t613
						   WHERE a.c601_form_id = p_calculatedsfid
								 AND t613.c613_study_period_id = a.c613_study_period_id)
				  AND 
				  */
				  t622.c601_form_id = p_form_id
				  AND t621.c611_study_id = p_study_id
				  AND t621.c621_patient_id = t622.c621_patient_id
				  AND NVL (t622.c622_verify_fl, 'N') = 'Y'
				  AND t623.c601_form_id = t622.c601_form_id
				  AND t623.c621_patient_id = t622.c621_patient_id
				  AND t623.c622_patient_list_id = t622.c622_patient_list_id
				  AND t605.c605_answer_list_id = t623.c605_answer_list_id
				  AND t604.c602_question_id = t623.c602_question_id
				  AND t604.c601_form_id = t622.c601_form_id
				  AND t623.c623_delete_fl = 'N'
				  AND t621.c621_delete_fl = 'N'
				  AND NVL(t622.C622_DELETE_FL,'N')='N'
				  -- The condition will get all the answers based on the answer id present in  rules table.
				  AND t906.c906_rule_grp_id = p_study_id || '_' || p_form_id
				  AND t906.c906_rule_id = t623.c603_answer_grp_id
				  AND t613.c613_study_period_id = t622.c613_study_period_id
				  AND t622.c622_date IS NOT NULL
				  AND t622.c622_missing_follow_up_fl IS NULL
			 ORDER BY t622.c621_patient_id, c622_date, t604.c604_seq_no;

		FOR currpatient IN patient_list
		LOOP
			-- 6195 is part of group 'FMVRF' for form level lock
			-- 1204 is part of group 'GPLOG' for clinical form log
			gm_save_pat_verify_list (currpatient.plid, p_userid, 6195, v_pat_verify_list_id);
			gm_update_log (v_pat_verify_list_id, 'Locked During Data Download', 1204, p_userid, v_msg);
		END LOOP;
	END gm_fetch_form_data;

--
/*******************************************************
 * Purpose: This Procedure is used to save
 * patient verify list
 *******************************************************/
--
	PROCEDURE gm_save_pat_verify_list (
		p_patient_list_id	   IN		t622_patient_list.c622_patient_list_id%TYPE
	  , p_userid			   IN		t625_patient_verify_list.c101_user_id%TYPE
	  , p_verify_lvl		   IN		t625_patient_verify_list.c901_verify_level%TYPE
	  , p_pat_verify_list_id   OUT		t625_patient_verify_list.c625_pat_verify_list_id%TYPE
	)
	AS
	BEGIN
		SELECT s625_patient_verify.NEXTVAL
		  INTO p_pat_verify_list_id
		  FROM DUAL;

		INSERT INTO t625_patient_verify_list
					(c625_pat_verify_list_id, c622_patient_list_id, c101_user_id, c625_date, c901_verify_level
				   , c625_created_by, c625_created_date
					)
			 VALUES (p_pat_verify_list_id, p_patient_list_id, p_userid, TRUNC (SYSDATE), p_verify_lvl
				   , p_userid, TRUNC (SYSDATE)
					);
	END gm_save_pat_verify_list;

--
/*******************************************************
 * Purpose: This Procedure is used to retrieve
 * download history
 *******************************************************/
--
	PROCEDURE gm_fetch_download_history (
		p_study_id	  IN	   t611_clinical_study.c611_study_id%TYPE
	  , p_formid	  IN	   t601_form_master.c601_form_id%TYPE
	  , p_recordset   OUT	   TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_recordset
		 FOR
			 SELECT   t903.c903_upload_file_list ID, t903.c903_file_name file_name
					, TO_CHAR (t903.c903_created_date, 'MM/DD/YYYY') created_date
					, get_user_name (t903.c903_created_by) created_by, t612.c612_download_lock_fl lock_fl
					, t902.c902_comments comments
				 FROM t903_upload_file_list t903, t612_study_form_list t612, t902_log t902
				WHERE t903.c903_ref_id = p_formid
				  AND t903.c901_ref_type = '60446'
				  AND TO_CHAR (t903.c903_upload_file_list) = t902.c902_ref_id
				  AND t902.c902_type = '1255'
				  AND t903.c903_upload_file_list = t612.c903_upload_file_list(+)
				  AND NVL (t903.c903_delete_fl, 'N') = 'N'
				  AND NVL (t612.c612_delete_fl, 'N') = 'N'
			 ORDER BY c903_created_date DESC;
	END gm_fetch_download_history;

--
/*******************************************************
 * Purpose: This Procedure is used to cancel
 * download data for a given form.
 *******************************************************/
--
	PROCEDURE gm_cancel_download (
		p_study_id		   IN	t611_clinical_study.c611_study_id%TYPE
	  , p_form_id		   IN	t612_study_form_list.c601_form_id%TYPE
	  , p_userid		   IN	t625_patient_verify_list.c101_user_id%TYPE
	  , p_cancel_reason    IN	t901_code_lookup.c901_code_id%TYPE
	  , p_type			   IN	VARCHAR2
	  , p_calculatedsfid   IN	NUMBER
	)
	AS
		v_cnt		   NUMBER;
		v_temp		   NUMBER;
		v_msg		   VARCHAR2 (1000);
		v_upload_file_list_id t903_upload_file_list.c903_upload_file_list%TYPE;
		v_study_list_id t612_study_form_list.c612_study_list_id%TYPE;

		CURSOR patient_verify_list
		IS
			SELECT t625.c625_pat_verify_list_id pat_verify_list_id
			  FROM t622_patient_list t622
				 , t621_patient_master t621
				 , t625_patient_verify_list t625
				 , t613_study_period t613
			 WHERE t621.c611_study_id = p_study_id
			   AND (   'COMPLETE' = DECODE (p_type, 'COMPLETE', p_type, 'CANCEL')
					
					/*OR ((t613.c613_study_period_ds, t622.c621_patient_id) IN (
							SELECT t613.c613_study_period_ds, a.c621_patient_id
							  FROM t622_patient_list a, t613_study_period t613
							 WHERE a.c601_form_id = p_form_id AND t613.c613_study_period_id = a.c613_study_period_id
							MINUS
							SELECT t613.c613_study_period_ds, a.c621_patient_id
							  FROM t622_patient_list a, t613_study_period t613
							 WHERE a.c601_form_id = p_calculatedsfid
							   AND t613.c613_study_period_id = a.c613_study_period_id)
					   )
					   */
				   )
			   AND t622.c601_form_id = p_form_id
			   AND t625.c622_patient_list_id = t622.c622_patient_list_id
			   AND t621.c621_patient_id = t622.c621_patient_id
			   AND NVL (t622.c622_verify_fl, 'N') = 'Y'
			   AND NVL (t625.c625_delete_fl, 'N') = 'N'
			   AND NVL(t622.C622_DELETE_FL,'N') = 'N'
			   AND t625.c901_verify_level = '6195'
			   AND t613.c613_study_period_id = t622.c613_study_period_id
			   AND t622.c622_date IS NOT NULL
			   AND t622.c622_missing_follow_up_fl IS NULL;
	BEGIN
		SELECT c903_upload_file_list
		  INTO v_upload_file_list_id
		  FROM t612_study_form_list t612
		 WHERE c611_study_id = p_study_id
		   AND c601_form_id = p_form_id
		   AND c612_download_lock_fl = 'Y'
		   AND NVL (c612_delete_fl, 'N') = 'N';

		SELECT c612_study_list_id
		  INTO v_study_list_id
		  FROM t612_study_form_list t612
		 WHERE c611_study_id = p_study_id
		   AND c601_form_id = p_form_id
		   AND c612_download_lock_fl = 'Y'
		   AND c903_upload_file_list = v_upload_file_list_id
		   AND NVL (c612_delete_fl, 'N') = 'N';

		IF (SQL%ROWCOUNT = 0)
		THEN
			--throw message 'cancel is already done.'
			raise_application_error (-20056, '');
		END IF;

		SELECT	   c612_study_list_id
			  INTO v_temp
			  FROM t612_study_form_list
			 WHERE c611_study_id = p_study_id
			   AND c601_form_id = p_form_id
			   AND c903_upload_file_list = v_upload_file_list_id
			   AND NVL (c612_delete_fl, 'N') = 'N'
		FOR UPDATE;

		FOR currpatient IN patient_verify_list
		LOOP
			UPDATE t625_patient_verify_list
			   SET c625_delete_fl = 'Y'
				 , c625_last_updated_by = p_userid
				 , c625_last_updated_date = SYSDATE
			 WHERE c625_pat_verify_list_id = currpatient.pat_verify_list_id;
		END LOOP;

		UPDATE t612_study_form_list
		   SET c612_download_lock_fl = NULL
			 , c903_upload_file_list = NULL
			 , c612_last_updated_by = p_userid
			 , c612_last_updated_date = SYSDATE
		 WHERE c612_study_list_id = v_study_list_id;

		IF (p_type = 'CANCEL')
		THEN
			UPDATE t902_log
			   SET c902_comments = get_code_name (p_cancel_reason)
				 , c902_last_updated_by = p_userid
				 , c902_last_updated_date = SYSDATE
			 WHERE c902_ref_id = to_char(v_upload_file_list_id) AND c902_type = '1255';
		END IF;
	END gm_cancel_download;
END gm_pkg_cr_datadownload;
/
