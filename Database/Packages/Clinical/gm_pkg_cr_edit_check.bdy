/* Formatted on 2010/06/17 12:30 (Formatter Plus v4.8.0) */
--@"C:\Database\Packages\Clinical\gm_pkg_cr_edit_check.bdy";
-- exec gm_pkg_cr_edit_check.gm_sav_incidents(24500,30301);

CREATE OR REPLACE PACKAGE BODY gm_pkg_cr_edit_check
IS
--
	/*********************************************************************
	* Description : To fetch all the edit checks for the given
	*				form and and call the "process procedure" for each
	*				check and if the check fails, call the "check and
	*				clear" procedure to clear already saved incidents,
	*				if any.
	*
	* Author		: VPrasath
	**********************************************************************/
	PROCEDURE gm_sav_incidents (
		p_patient_list_id	IN	 t622_patient_list.c622_patient_list_id%TYPE
	  , p_user_id			IN	 t622_patient_list.c622_last_updated_by%TYPE
	)
	AS
		v_incident_occured BOOLEAN := FALSE;
		v_patient_id   t622_patient_list.c621_patient_id%TYPE;
		v_study_period_id t6513_edit_check_details.c613_study_period_id%TYPE;

		CURSOR v_edit_chk_cur
		IS
			SELECT t6513.c6513_edit_check_details_id edit_chk_details_id
			  FROM t6512_edit_check t6512, t6513_edit_check_details t6513, t622_patient_list t622
			 WHERE t622.c622_patient_list_id = p_patient_list_id
			   AND t6512.c6512_edit_check_id = t6513.c6512_edit_check_id
			   AND (t622.c601_form_id = t6513.c601_form_id OR t622.c601_form_id = t6513.c601_linked_form_id)
			   AND t6512.c6512_void_fl IS NULL
			   AND t6513.c6513_void_fl IS NULL
			   AND t622.c622_delete_fl = 'N'
			   AND t6512.c6512_active_fl = 'Y'
			   AND t6513.c6513_edit_check_details_id NOT IN (
					   SELECT DISTINCT t6514.c6513_edit_check_details_id
								  FROM t6514_ec_incident t6514, t623_patient_answer_list t623
								 WHERE t6514.c623_patient_ans_list_id = t623.c623_patient_ans_list_id
								   AND t623.c622_patient_list_id = p_patient_list_id
								   AND t6514.c621_patient_id = v_patient_id
								   AND t6514.c6514_status = 20	 -- Closed
								   AND t6514.c901_status_reason IN
										   (93050, 93052, 93053)   -- Protocol Deviation, Data is Correct, Edit Check Invalid
								   AND t6514.c6514_void_fl IS NULL
								   AND t623.c623_delete_fl = 'N')
			   AND t6513.c6513_edit_check_details_id NOT IN (
					   SELECT TO_NUMBER (t6513a.c6513_consequence_ref_id)
						 FROM t6513_edit_check_details t6513a
						WHERE t6513a.c6513_consequence_ref_id IS NOT NULL
						  AND t6513a.c6513_consequence_ref_id = t6513.c6513_edit_check_details_id);
	BEGIN
		SELECT	   t622.c621_patient_id patient_id, t622.c613_study_period_id
			  INTO v_patient_id, v_study_period_id
			  FROM t622_patient_list t622
			 WHERE t622.c622_patient_list_id = p_patient_list_id AND t622.c622_delete_fl = 'N'
		FOR UPDATE;

		FOR ec_index IN v_edit_chk_cur
		LOOP
			v_global_desc := '';
			v_global_ec_details_id := ec_index.edit_chk_details_id;
			v_global_patient_ans_list_id := NULL;
			v_global_source_tp := NULL;
			v_global_dest_tp := NULL;
			v_global_dest_pat_ans_list_id := NULL;
			v_global_pat_list_id := NULL;
			gm_sav_process_ec (p_patient_list_id
							 , v_global_ec_details_id
							 , v_patient_id
							 , p_user_id
							 , v_study_period_id
							 , v_incident_occured
							  );

			IF NOT v_incident_occured
			THEN
				gm_sav_check_and_clear (v_global_ec_details_id
									  , v_patient_id
									  , p_user_id
									  , 93051
									  , NULL
									  , v_global_patient_ans_list_id
									   );
			END IF;
		END LOOP;
	END gm_sav_incidents;

	/*************************************************************************
	* Description :  This is the main procedure that is used to check
	*				 all the rules against the form values and
	*				 if the check is true then will call the "save incident"
	*				 procedure, if the check has associated checks then
	*				 this procedure will be called recursively to address
	*				 all the associated checks.
	*
	* Author		: VPrasath
	**************************************************************************/
	PROCEDURE gm_sav_process_ec (
		p_patient_list_id	 IN 	  t622_patient_list.c622_patient_list_id%TYPE
	  , p_ec_details_id 	 IN 	  t6513_edit_check_details.c6513_edit_check_details_id%TYPE
	  , p_patient_id		 IN 	  t621_patient_master.c621_patient_id%TYPE
	  , p_user_id			 IN 	  t622_patient_list.c622_last_updated_by%TYPE
	  , p_study_period_id	 IN 	  t6513_edit_check_details.c613_study_period_id%TYPE
	  , p_incident_occured	 OUT	  BOOLEAN
	)
	AS
		v_source_form_id t6513_edit_check_details.c601_form_id%TYPE;
		v_source_study_period_id t6513_edit_check_details.c613_study_period_id%TYPE;
		v_source_ans_grp_id t6513_edit_check_details.c603_answer_grp_id%TYPE;
		v_source_con_chk t6513_edit_check_details.c901_precondition_check%TYPE;
		v_source_con_value t6513_edit_check_details.c6513_pre_condition_value%TYPE;
		v_source_patient_ans_list_id t623_patient_answer_list.c623_patient_ans_list_id%TYPE;
		v_dest_form_id t6513_edit_check_details.c601_linked_form_id%TYPE;
		v_dest_study_period_id t6513_edit_check_details.c613_linked_study_period_id%TYPE;
		v_dest_ans_grp_id t6513_edit_check_details.c603_linked_answer_grp_id%TYPE;
		v_dest_con_chk t6513_edit_check_details.c901_post_condition_check%TYPE;
		v_dest_con_value t6513_edit_check_details.c6513_post_condition_value%TYPE;
		v_dest_patient_ans_list_id t623_patient_answer_list.c623_patient_ans_list_id%TYPE;
		v_consequence_type t6513_edit_check_details.c901_consequence_type%TYPE;
		v_source_ec_type t6513_edit_check_details.c901_edit_check_type%TYPE;
		v_dest_ec_type t6513_edit_check_details.c901_edit_check_type%TYPE;
		v_consequence_ref_id t6513_edit_check_details.c6513_consequence_ref_id%TYPE;
		v_compare_value_chk t6513_edit_check_details.c901_compare_value_check%TYPE;
		v_compare_values t6513_edit_check_details.c6513_compare_values%TYPE;
		v_source_form_value VARCHAR2 (255);
		v_dest_form_value VARCHAR2 (255);
		v_source_return_fl BOOLEAN := FALSE;
		v_dest_return_fl BOOLEAN := FALSE;
		v_compare_return_fl BOOLEAN := FALSE;
		v_extra_operand t6513_edit_check_details.c6513_extra_operand%TYPE;
		v_submitted_form_id t622_patient_list.c601_form_id%TYPE;
	BEGIN
		p_incident_occured := FALSE;

		/* Fetch Details of the Edit Check */
		SELECT t6513.c601_form_id form_id, t6513.c613_study_period_id, t6513.c603_answer_grp_id
			 , t6513.c901_precondition_check, t6513.c6513_pre_condition_value, t6513.c601_linked_form_id
			 , t6513.c613_linked_study_period_id, t6513.c603_linked_answer_grp_id, t6513.c901_post_condition_check
			 , t6513.c6513_post_condition_value, t6513.c901_consequence_type, t6513.c901_edit_check_type
			 , t6513.c901_linked_edit_check_type, t6513.c6513_consequence_ref_id, t6513.c901_compare_value_check
			 , t6513.c6513_compare_values, t6513.c6513_extra_operand
		  INTO v_source_form_id, v_source_study_period_id, v_source_ans_grp_id
			 , v_source_con_chk, v_source_con_value, v_dest_form_id
			 , v_dest_study_period_id, v_dest_ans_grp_id, v_dest_con_chk
			 , v_dest_con_value, v_consequence_type, v_source_ec_type
			 , v_dest_ec_type, v_consequence_ref_id, v_compare_value_chk
			 , v_compare_values, v_extra_operand
		  FROM t6513_edit_check_details t6513
		 WHERE t6513.c6513_edit_check_details_id = p_ec_details_id;

		/* If a study period is given in the rule, then the rule should fire for that particular study period */
		BEGIN
			SELECT c601_form_id
			  INTO v_submitted_form_id
			  FROM t622_patient_list t622
			 WHERE t622.c622_patient_list_id = p_patient_list_id AND t622.c622_delete_fl = 'N';
			EXCEPTION WHEN NO_DATA_FOUND THEN
			-- When t he form is not filled in return. Happens during the Scheduled job execution.
			RETURN;
		END;

		/* if rule has a study period defined in it then the rule should not fire for any other time period */
		IF v_submitted_form_id = v_source_form_id AND p_study_period_id != v_source_study_period_id
		THEN
			RETURN;
		ELSIF	  v_source_form_id != v_dest_form_id
			  AND v_submitted_form_id = v_dest_form_id
			  AND p_study_period_id != v_dest_study_period_id
		THEN
			RETURN;
		END IF;

		/*
		 * Take the Source and destination Study Period
		 * if it is not defined in the rule.
		 */
		IF v_source_form_id IS NOT NULL AND v_source_study_period_id IS NULL
		THEN
			v_source_study_period_id :=
									get_linked_study_period_id (v_source_form_id, p_study_period_id, p_patient_list_id);
		END IF;

		IF v_dest_form_id IS NOT NULL AND v_dest_study_period_id IS NULL
		THEN
			v_dest_study_period_id := get_linked_study_period_id (v_dest_form_id, p_study_period_id, p_patient_list_id);
		END IF;

		/*
		 * Pre Condition Check
		 */
		/* Fetch the form value for the pre condition */
		gm_fch_form_value (p_patient_list_id
						 , v_source_form_id
						 , v_source_ans_grp_id
						 , p_patient_id
						 , v_source_study_period_id
						 , v_source_ec_type
						 , v_source_form_value
						 , v_source_patient_ans_list_id
						  );

		IF (v_global_patient_ans_list_id IS NULL)
		THEN
			v_global_patient_ans_list_id := v_source_patient_ans_list_id;
		END IF;

		IF (v_global_dest_pat_ans_list_id IS NULL)
		THEN
			v_global_dest_pat_ans_list_id := v_dest_patient_ans_list_id;
		END IF;

		IF (v_global_source_tp IS NULL)
		THEN
			v_global_source_tp := v_source_study_period_id;
		END IF;

		IF (v_global_dest_tp IS NULL)
		THEN
			v_global_dest_tp := v_dest_study_period_id;
		END IF;

		IF (v_global_pat_list_id IS NULL)
		THEN
			v_global_pat_list_id := p_patient_list_id;
		END IF;

		/*	Compare the precondition and the form value and return the boolean value in v_return_fl */
		gm_fch_compare (v_source_form_value, v_source_con_chk, v_source_con_value, v_extra_operand, v_source_return_fl);

		/* if the returned value is false, don't proceed */
		IF NOT v_source_return_fl
		THEN
			RETURN;
		END IF;

		/*
		 * Self Check
		 */

		/* If the type is self check, no need to check the post condition and no need to compare the values */
		IF v_source_ec_type = 93038 OR v_source_ec_type = 93037   -- SELF CHECK OR PART NUMBER
		THEN
			gm_sav_incident_details (p_ec_details_id
								   , v_source_patient_ans_list_id
								   , p_patient_id
								   , v_source_study_period_id
								   , v_source_form_value
								   , NULL
								   , NULL
								   , v_dest_patient_ans_list_id
								   , v_consequence_type
								   , v_consequence_ref_id
								   , p_user_id
								   , p_incident_occured
									);
			--p_incident_occured := TRUE;
			RETURN;
		END IF;

		/*
		 * Post Condition Check
		 */
		/* Fetch the form value for the post condition */
		gm_fch_form_value (p_patient_list_id
						 , v_dest_form_id
						 , v_dest_ans_grp_id
						 , p_patient_id
						 , v_dest_study_period_id
						 , v_dest_ec_type
						 , v_dest_form_value
						 , v_dest_patient_ans_list_id
						  );
		/*	Compare the precondition and the form value and return the boolean value in  v_return_fl */
		gm_fch_compare (v_dest_form_value, v_dest_con_chk, v_dest_con_value, v_extra_operand, v_dest_return_fl);

		/* if the returned value is false, don't proceed */
		IF NOT v_dest_return_fl
		THEN
			RETURN;
		END IF;

		IF v_compare_values = 'Y'
		THEN
			/*	Compare the precondition and the form value and return the boolean value in  v_return_fl */
			gm_fch_compare (v_source_form_value
						  , v_compare_value_chk
						  , v_dest_form_value
						  , v_extra_operand
						  , v_compare_return_fl
						   );

			/* if the returned value is false, don't proceed and p_consequence_type <> Fire Another Rule */
			IF (NOT v_compare_return_fl)
			THEN
				RETURN;
			END IF;
		END IF;

		/* Now save the incident details for both the ans_list_id's */
		gm_sav_incident_details (p_ec_details_id
							   , v_source_patient_ans_list_id
							   , p_patient_id
							   , v_source_study_period_id
							   , v_source_form_value
							   , v_dest_study_period_id
							   , v_dest_form_value
							   , v_dest_patient_ans_list_id
							   , v_consequence_type
							   , v_consequence_ref_id
							   , p_user_id
							   , p_incident_occured
								);
	--p_incident_occured := TRUE;
	END gm_sav_process_ec;

	/***********************************************************************************
	 * Description : The Purpose of this procedure is to fire the
	 *				 corresponding consequence based on the Consequence type.
	 *				 we call this as sav_incident_details bcoz save can happen
	 *				 only through this procedure. Currently, there are only two types
	 *				 of consquences "Save Details", "Fire Another Rule".
	 *
	 * Author		 : VPrasath
	 ***********************************************************************************/
	PROCEDURE gm_sav_incident_details (
		p_ec_details_id 		   IN		t6513_edit_check_details.c6513_edit_check_details_id%TYPE
	  , p_patient_ans_list_id	   IN		t623_patient_answer_list.c623_patient_ans_list_id%TYPE
	  , p_patient_id			   IN		t621_patient_master.c621_patient_id%TYPE
	  , p_source_study_period_id   IN		t6513_edit_check_details.c613_study_period_id%TYPE
	  , p_source_form_value 	   IN		t6514_ec_incident.c6514_form_value%TYPE
	  , p_dest_study_period_id	   IN		t6513_edit_check_details.c613_study_period_id%TYPE
	  , p_dest_form_value		   IN		t6514_ec_incident.c6514_form_value%TYPE
	  , p_dest_ans_list_id		   IN		t623_patient_answer_list.c623_patient_ans_list_id%TYPE
	  , p_consequence_type		   IN		t6513_edit_check_details.c901_consequence_type%TYPE
	  , p_consequence_ref_id	   IN		t6513_edit_check_details.c6513_consequence_ref_id%TYPE
	  , p_user_id				   IN		t6513_edit_check_details.c6513_created_by%TYPE
	  , p_incident_occured		   OUT		BOOLEAN
	)
	AS
		v_patient_list_id t623_patient_answer_list.c622_patient_list_id%TYPE;
		v_incident_occured BOOLEAN;
		v_ec_incident_id t6514_ec_incident.c6514_ec_incident_id%TYPE;
		v_consq_form_id t6513_edit_check_details.c601_form_id%TYPE;
		v_consq_study_period_id t613_study_period.c613_study_period_id%TYPE;
		v_conq_plist_id t623_patient_answer_list.c622_patient_list_id%TYPE;
		v_desc		   VARCHAR2 (4000);
	BEGIN
		SELECT t623.c622_patient_list_id
			  INTO v_patient_list_id
			  FROM t623_patient_answer_list t623
			 WHERE t623.c623_patient_ans_list_id = NVL (v_global_patient_ans_list_id, p_patient_ans_list_id)
			 AND NVL(t623.C623_DELETE_FL,'N')='N';
		

		/* Create the description for the values fetched */
		gm_fch_incident_description (p_ec_details_id
								   , p_source_study_period_id
								   , p_source_form_value
								   , p_dest_study_period_id
								   , p_dest_form_value
								   , v_desc
									);
		/* Assign the fetched value to the global variable
			so that this description can grow even for multiple calls */
		v_global_desc := v_global_desc || v_desc;

		-- Fire the consequence based on the type
		IF (p_consequence_type = 93030)   -- Edit Check
		THEN
			BEGIN
				p_incident_occured := TRUE;

				SELECT c6514_ec_incident_id
				  INTO v_ec_incident_id
				  FROM t6514_ec_incident t6514
				 WHERE t6514.c6513_edit_check_details_id = v_global_ec_details_id
				   AND (   NVL (t6514.c623_patient_ans_list_id, -999) =
													NVL (NVL (v_global_patient_ans_list_id, p_patient_ans_list_id)
													   , -999)
						OR NVL (t6514.c623_patient_ans_list_id, -999) =
													  NVL (NVL (v_global_dest_pat_ans_list_id, p_dest_ans_list_id)
														 , -999)
					   )
				   AND (   NVL (t6514.c623_linked_pt_ans_list_id, -999) =
													NVL (NVL (v_global_patient_ans_list_id, p_patient_ans_list_id)
													   , -999)
						OR NVL (t6514.c623_linked_pt_ans_list_id, -999) =
													  NVL (NVL (v_global_dest_pat_ans_list_id, p_dest_ans_list_id)
														 , -999)
					   )
				   AND t6514.c621_patient_id = p_patient_id
				   AND t6514.c6514_status = 10;   -- Open
			EXCEPTION
				WHEN NO_DATA_FOUND
				THEN
					v_ec_incident_id := '';
			END;

			--	Mark the question has a edit check and store the incident conditions and values
			-- v_global_src_ans_grp_id added for multiple Edit Check issue
			UPDATE t623_patient_answer_list t623
			   SET c623_edit_check_fl = 'Y'
				 , c623_last_updated_date = SYSDATE
				 , c623_last_updated_by = p_user_id
			 WHERE (   t623.c623_patient_ans_list_id = NVL (v_global_patient_ans_list_id, p_patient_ans_list_id)
					OR t623.c623_patient_ans_list_id = NVL (v_global_dest_pat_ans_list_id, p_dest_ans_list_id)
				   );

			-- This procedure is to do master save (update/insert)
			gm_sav_ec_incident (v_global_ec_details_id
							  , NVL (v_global_patient_ans_list_id, p_patient_ans_list_id)
							  , NVL (v_global_dest_pat_ans_list_id, p_dest_ans_list_id)
							  , p_patient_id
							  , NVL (v_global_source_tp, p_source_study_period_id)
							  , p_source_form_value
							  , NVL (v_global_dest_tp, p_dest_study_period_id)
							  , p_dest_form_value
							  , v_global_desc
							  , 10
							  , p_user_id
							  , NULL
							  , v_ec_incident_id
							   );
		ELSIF (p_consequence_type = 93031)	 -- Fire Another Rule
		THEN
			/*
			 * if  another rule need to be fired then add the ' AND ' to the global description.
			 * P.S. Need to remove the decoration from the data once the solution in DHTMLX is ready
			 */
			v_global_desc := v_global_desc || '<font color=red> AND	</font>';

			/*
			 * The consquence_ref_id is nothing but another rule ID, to fire this rule,
			 *	we need to fetch basic info for this rule. Fetch the form id
			 * , study period id, patient_list id and call the main procedure.
			 */
			IF (p_consequence_ref_id IS NULL)
			THEN
				-- If consiquence has no associated id display error
				raise_application_error ('-20378'
									   ,	'p_consequence_ref_id iS NULL check rule p_ec_details_id	*** '
										 || p_ec_details_id
										);
			END IF;

			SELECT t6513.c601_form_id, t6513.c613_study_period_id
				  INTO v_consq_form_id, v_consq_study_period_id
				  FROM t6513_edit_check_details t6513
				 WHERE t6513.c6513_edit_check_details_id = p_consequence_ref_id;   -- cons_ref_id			
			
			IF v_consq_study_period_id IS NULL
			THEN
				v_consq_study_period_id :=
							  get_linked_study_period_id (v_consq_form_id, p_source_study_period_id, v_patient_list_id);
			END IF;

			BEGIN
				SELECT t622.c622_patient_list_id
				  INTO v_conq_plist_id
				  FROM t622_patient_list t622
				 WHERE t622.c601_form_id = v_consq_form_id
				   AND t622.c621_patient_id = p_patient_id
				   AND t622.c613_study_period_id = v_consq_study_period_id
				   AND NVL(t622.C622_DELETE_FL,'N')='N';
			--AND ROWNUM = 1;
			EXCEPTION
				-- IN CASE OF SD13 there will be more than one AE for above mentioned criteria, in which case, we will use
				-- previously retrieved patient list id.
				WHEN TOO_MANY_ROWS
				THEN
					v_conq_plist_id := v_global_pat_list_id;
				WHEN NO_DATA_FOUND
				THEN
					--The exception happens when the Form is not filled in, happens during scheduled edit check job. 
					NULL;
					
			END;

			-- Get the corresponding rule and fire it by calling the method recursively
			gm_sav_process_ec (v_conq_plist_id
							 , p_consequence_ref_id
							 , p_patient_id
							 , p_user_id
							 , v_consq_study_period_id
							 , v_incident_occured
							  );
			p_incident_occured := v_incident_occured;
		END IF;
	END gm_sav_incident_details;

	/***********************************************************************************
	 * Description : This is the master procedure for the table t6514_ec_incident, it
	 *				 does the update and insert for the table. History for the answer
	 *				 will be created as an incident is saved.
	 *
	 * Author		 : VPrasath
	 ***********************************************************************************/
	PROCEDURE gm_sav_ec_incident (
		p_ec_details_id 		   IN		t6514_ec_incident.c6513_edit_check_details_id%TYPE
	  , p_patient_ans_list_id	   IN		t6514_ec_incident.c623_patient_ans_list_id%TYPE
	  , p_dest_ans_list_id		   IN		t623_patient_answer_list.c623_patient_ans_list_id%TYPE
	  , p_patient_id			   IN		t6514_ec_incident.c621_patient_id%TYPE
	  , p_study_period_id		   IN		t6514_ec_incident.c613_study_period_id%TYPE
	  , p_source_form_value 	   IN		t6514_ec_incident.c6514_linked_form_value%TYPE
	  , p_linked_study_period_id   IN		t6514_ec_incident.c613_study_period_id%TYPE
	  , p_dest_form_value		   IN		t6514_ec_incident.c6514_linked_form_value%TYPE
	  , p_incident_description	   IN		t6514_ec_incident.c6514_incident_description%TYPE
	  , p_status				   IN		t6514_ec_incident.c6514_status%TYPE
	  , p_user_id				   IN		t6514_ec_incident.c6514_created_by%TYPE
	  , p_reason				   IN		t6514_ec_incident.c901_status_reason%TYPE
	  , p_ec_incident_id		   IN OUT	t6514_ec_incident.c6514_ec_incident_id%TYPE
	)
	AS
	BEGIN
		UPDATE t6514_ec_incident t6514
		   SET c6514_form_value = p_source_form_value
			 , c6514_linked_form_value = p_dest_form_value
			 , c6514_incident_description = p_incident_description
			 , c6514_last_updated_by = p_user_id
			 , c6514_last_updated_date = SYSDATE
		 WHERE t6514.c6514_ec_incident_id = p_ec_incident_id;

		IF SQL%ROWCOUNT = 0
		THEN
			SELECT s6514_ec_incident.NEXTVAL
			  INTO p_ec_incident_id
			  FROM DUAL;

			INSERT INTO t6514_ec_incident
						(c6514_ec_incident_id, c6514_form_value, c6514_linked_form_value, c6514_status
					   , c6514_created_by, c6514_created_date, c6514_incident_description, c6513_edit_check_details_id
					   , c613_linked_study_period_id, c613_study_period_id, c621_patient_id, c623_patient_ans_list_id
					   , c901_status_reason, c623_linked_pt_ans_list_id
						)
				 VALUES (p_ec_incident_id, p_source_form_value, p_dest_form_value, p_status
					   , p_user_id, SYSDATE, p_incident_description, p_ec_details_id
					   , p_linked_study_period_id, p_study_period_id, p_patient_id, p_patient_ans_list_id
					   , p_reason, p_dest_ans_list_id
						);
		END IF;

		/*
		 *	when ever an incident is saved the history for that particular answer
		 *	need to be updated. 92473-> Edit Check Entered.
		 */
		gm_sav_form_history (p_ec_incident_id, 92473, p_user_id);
	END gm_sav_ec_incident;

	/***********************************************************************************
	 * Description : This is one of the important procedures in edit check, this will fetch
	 *				 form value that is stored for the given form_id, ans_grp_id
	 *				 , patient_id and study_period_id . Based on the edit check
	 *				 type the form_value and the patient_ans_list_id will be fetched.
	 * Author		 : VPrasath
	 ***********************************************************************************/
	PROCEDURE gm_fch_form_value (
		p_patient_list_id		IN		 t622_patient_list.c622_patient_list_id%TYPE
	  , p_form_id				IN		 t6513_edit_check_details.c601_form_id%TYPE
	  , p_ans_grp_id			IN		 t6513_edit_check_details.c603_answer_grp_id%TYPE
	  , p_patient_id			IN		 t621_patient_master.c621_patient_id%TYPE
	  , p_study_period_id		IN		 t6513_edit_check_details.c613_study_period_id%TYPE
	  , p_ec_type				IN		 t6513_edit_check_details.c901_edit_check_type%TYPE
	  , p_form_value			OUT 	 VARCHAR2
	  , p_patient_ans_list_id	OUT 	 t623_patient_answer_list.c623_patient_ans_list_id%TYPE
	)
	AS
		v_calculated_value VARCHAR2 (255);
		v_pnum		   VARCHAR2 (20);
		v_spvalue	   VARCHAR2 (75);
		v_spgrp 	   NUMBER;
		v_sptimeexclude CHAR (1);
		v_sppatlistexclude CHAR (1);
	BEGIN
		IF p_ec_type IS NULL
		THEN
			RETURN;
		END IF;

		IF (p_ec_type = 93035 OR p_ec_type = 93038)
		THEN   -- Form OR Self Check
			BEGIN
				/* This is the most common type of edit check */
				SELECT DECODE (t603.c901_control_type
							 , 6101, t623.c904_answer_ds   -- TEXTBOX
							 , 6104, t623.c904_answer_ds   -- TEXTAREA
							 , 6105, t623.c904_answer_ds   -- TEXTREADONLY
							 , 6100, t605.c605_answer_value
								|| DECODE (t623.c605_answer_list_id, NULL, '', c904_answer_ds)	 -- COMBOBOX
							 , 6102, t623.c904_answer_ds   -- CHECKBOX
							  ) VALUE
					 , t623.c623_patient_ans_list_id
				  INTO p_form_value
					 , p_patient_ans_list_id
				  FROM t623_patient_answer_list t623
					 , t603_answer_grp t603
					 , t605_answer_list t605
					 , t622_patient_list t622
				 WHERE t622.c622_patient_list_id = t623.c622_patient_list_id
				   AND t623.c603_answer_grp_id = t603.c603_answer_grp_id
				   AND t623.c605_answer_list_id = t605.c605_answer_list_id(+)
				   AND t622.c601_form_id = p_form_id
				   AND t623.c603_answer_grp_id = p_ans_grp_id
				   AND t622.c621_patient_id = p_patient_id
				   AND t622.c613_study_period_id = p_study_period_id
				   AND t623.c623_delete_fl = 'N'
				   AND t603.c603_delete_fl = 'N'
				   AND t622.c622_delete_fl = 'N';
			EXCEPTION
				WHEN TOO_MANY_ROWS
				THEN
					-- Too many row fetch is an issue with SD13 multiple row issue
					-- Only for N/A Time point
					BEGIN
						SELECT DECODE (t603.c901_control_type
									 , 6101, t623.c904_answer_ds   -- TEXTBOX
									 , 6104, t623.c904_answer_ds   -- TEXTAREA
									 , 6105, t623.c904_answer_ds   -- TEXTREADONLY
									 , 6100, t605.c605_answer_value
										|| DECODE (t623.c605_answer_list_id, NULL, '', c904_answer_ds)	 -- COMBOBOX
									 , 6102, t623.c904_answer_ds   -- CHECKBOX
									  ) VALUE
							 , t623.c623_patient_ans_list_id
						  INTO p_form_value
							 , p_patient_ans_list_id
						  FROM t623_patient_answer_list t623
							 , t603_answer_grp t603
							 , t605_answer_list t605
							 , t622_patient_list t622
						 WHERE t622.c622_patient_list_id = t623.c622_patient_list_id
						   AND t623.c603_answer_grp_id = t603.c603_answer_grp_id
						   AND t623.c605_answer_list_id = t605.c605_answer_list_id(+)
						   AND t622.c601_form_id = p_form_id
						   AND t623.c603_answer_grp_id = p_ans_grp_id
						   AND t622.c621_patient_id = p_patient_id
						   AND t622.c613_study_period_id = p_study_period_id
						   AND t623.c623_delete_fl = 'N'
						   AND t603.c603_delete_fl = 'N'
						   AND t622.c622_delete_fl = 'N'
						   AND t623.c622_patient_list_id = p_patient_list_id;
					EXCEPTION
						WHEN NO_DATA_FOUND
						THEN
							p_form_value := NULL;
							p_patient_ans_list_id := NULL;
					END;
				WHEN NO_DATA_FOUND
				THEN
					p_form_value := NULL;
					p_patient_ans_list_id := NULL;
			END;
		ELSIF (p_ec_type = 93037)
		THEN   -- Part Number
			BEGIN
				/* This is an exceptional case, this will be called for the form SD11, Question number 9 in flexus */
				SELECT DECODE (t603.c901_control_type
							 , 6101, t623.c904_answer_ds   -- TEXTBOX
							 , 6104, t623.c904_answer_ds   -- TEXTAREA
							 , 6105, t623.c904_answer_ds   -- TEXTREADONLY
							 , 6100, t605.c605_answer_value   -- COMBOBOX
							 , 6102, t623.c904_answer_ds   -- CHECKBOX
							  ) VALUE
					 , t623.c623_patient_ans_list_id
				  INTO v_pnum
					 , p_patient_ans_list_id
				  FROM t623_patient_answer_list t623
					 , t603_answer_grp t603
					 , t605_answer_list t605
					 , t622_patient_list t622
				 WHERE t622.c622_patient_list_id = t623.c622_patient_list_id
				   AND t623.c603_answer_grp_id = t603.c603_answer_grp_id
				   AND t623.c605_answer_list_id = t605.c605_answer_list_id(+)
				   AND t623.c601_form_id = p_form_id
				   AND t623.c603_answer_grp_id = p_ans_grp_id
				   AND t623.c621_patient_id = p_patient_id
				   AND t622.c613_study_period_id = p_study_period_id
				   AND t623.c623_delete_fl = 'N'
				   AND t603.c603_delete_fl = 'N'
				   AND t622.c622_delete_fl = 'N';

				/*
				 * After getting the part number, if the number is in between the range
				 * then return '327.*' else return the actual part number fetched. In rule the
				 * data is stored as '327.*' and the operator as '='. So if the returned value
				 * is checked against the db value it will return true or false.
				 */
				SELECT CASE
						   WHEN COUNT (1) > 0
							   THEN '327.*'
						   ELSE v_pnum
					   END
				  INTO p_form_value
				  FROM t205_part_number t205
				 WHERE (   (t205.c205_part_number_id BETWEEN '327.108' AND '327.120')
						OR (t205.c205_part_number_id BETWEEN '327.208' AND '327.220')
						OR (t205.c205_part_number_id BETWEEN '327.308' AND '327.320')
						OR (t205.c205_part_number_id BETWEEN '327.408' AND '327.420')
						OR (t205.c205_part_number_id BETWEEN '327.508' AND '327.520')
						OR (t205.c205_part_number_id BETWEEN '327.608' AND '327.620')
					   )
				   AND t205.c205_part_number_id = v_pnum
				   AND t205.c205_active_fl = 'Y';

				/*
				 * If the form is not saved the p_form_value will be Null
				 * , if Null then return '327.*'
				 */
				IF p_form_value IS NULL
				THEN
					p_form_value := '327.*';
				END IF;
			EXCEPTION
				WHEN NO_DATA_FOUND
				THEN
					/*
					 * If the form is not saved the p_form_value will be Null
					 * , if Null then return '327.*'
					 */
					p_form_value := '327.*';
					p_patient_ans_list_id := NULL;
			END;
		ELSIF (p_ec_type = 93036)
		THEN   -- SD8 Calculation
			BEGIN
				/*
				 * This is an exceptional case, Calculate the Sum of the answers for
				 * questions 1 To 10 for the Form SD8 (Flexus) and multiply it with 2
				 */
				SELECT SUM (NVL (t605.c605_answer_value, 0)) * 2
				  INTO p_form_value
				  FROM t623_patient_answer_list t623
					 , t603_answer_grp t603
					 , t605_answer_list t605
					 , t622_patient_list t622
				 WHERE t622.c622_patient_list_id = t623.c622_patient_list_id
				   AND t623.c603_answer_grp_id = t603.c603_answer_grp_id
				   AND t623.c605_answer_list_id = t605.c605_answer_list_id(+)
				   AND t623.c601_form_id = p_form_id
				   AND t623.c603_answer_grp_id IN (609, 610, 611, 612, 613, 614, 615, 616, 617, 618)   -- Hard coded value
				   AND t623.c621_patient_id = p_patient_id
				   AND t622.c613_study_period_id = p_study_period_id
				   AND t623.c623_delete_fl = 'N'
				   AND t603.c603_delete_fl = 'N'
				   AND t622.c622_delete_fl = 'N';

				SELECT t623.c623_patient_ans_list_id
				  INTO p_patient_ans_list_id
				  FROM t623_patient_answer_list t623
					 , t603_answer_grp t603
					 , t605_answer_list t605
					 , t622_patient_list t622
				 WHERE t622.c622_patient_list_id = t623.c622_patient_list_id
				   AND t623.c603_answer_grp_id = t603.c603_answer_grp_id
				   AND t623.c605_answer_list_id = t605.c605_answer_list_id(+)
				   AND t623.c601_form_id = p_form_id
				   AND t623.c603_answer_grp_id = 725
				   AND t623.c621_patient_id = p_patient_id
				   AND t622.c613_study_period_id = p_study_period_id
				   AND t623.c623_delete_fl = 'N'
				   AND t603.c603_delete_fl = 'N'
				   AND t622.c622_delete_fl = 'N';
			EXCEPTION
				WHEN NO_DATA_FOUND
				THEN
					p_form_value := NULL;
					p_patient_ans_list_id := NULL;
			END;
		ELSIF (p_ec_type = 93040)
		THEN   -- Normal check for SD9 Calculation ( can be used for other calculation)
			BEGIN
				/*
				 * Its an exceptional case, Calculate the Sum of the answers for
				 * for the selected answer group and return the Average -- Currently used for
				 * SD9 Flexus
				 */
				my_context.set_my_inlist_ctx (get_rule_value (p_ans_grp_id, 'CR_EDIT_CHECK_AVG'));

				SELECT ROUND (AVG (NVL (t605.c605_answer_value, 0)), 2)
				  INTO p_form_value
				  FROM t623_patient_answer_list t623
					 , t603_answer_grp t603
					 , t605_answer_list t605
					 , t622_patient_list t622
				 WHERE t622.c622_patient_list_id = t623.c622_patient_list_id
				   AND t623.c603_answer_grp_id = t603.c603_answer_grp_id
				   AND t623.c605_answer_list_id = t605.c605_answer_list_id(+)
				   AND t623.c601_form_id = p_form_id
				   AND t623.c603_answer_grp_id IN (SELECT *
													 FROM v_in_list)   -- Value from rule table
				   AND t623.c621_patient_id = p_patient_id
				   AND t622.c613_study_period_id = p_study_period_id
				   AND t623.c623_delete_fl = 'N'
				   AND t603.c603_delete_fl = 'N'
				   AND t622.c622_delete_fl = 'N';

				/* Since its an average calculation not associated to any patient list value*/
				p_patient_ans_list_id := NULL;
			/* SELECT t623.c623_patient_ans_list_id
			  INTO p_patient_ans_list_id
			  FROM t623_patient_answer_list t623
				 , t603_answer_grp t603
				 , t605_answer_list t605
				 , t622_patient_list t622
			 WHERE t622.c622_patient_list_id = t623.c622_patient_list_id
			   AND t623.c603_answer_grp_id = t603.c603_answer_grp_id
			   AND t623.c605_answer_list_id = t605.c605_answer_list_id(+)
			   AND t623.c601_form_id = p_form_id
			   AND t623.c603_answer_grp_id = p_ans_grp_id
			   AND t623.c621_patient_id = p_patient_id
			   AND t622.c613_study_period_id = p_study_period_id
			   AND t623.c623_delete_fl = 'N'
			   AND t603.c603_delete_fl = 'N'
			   AND t622.c622_delete_fl = 'N'; */
			EXCEPTION
				WHEN NO_DATA_FOUND
				THEN
					p_form_value := NULL;
					p_patient_ans_list_id := NULL;
			END;
		ELSIF (p_ec_type = 93041)
		THEN   -- check for N/A Time point , multiple forms will be available for close check
			BEGIN
				/*
				 * Its an exceptional case, Special Check
				 * For Selected form and selected group and for selected value
				 * SD9 Flexus
				 */
				v_spvalue	:= get_rule_value (p_ans_grp_id, 'CR_EDIT_CHECK_SPVAL');
				v_spgrp 	:= get_rule_value (p_ans_grp_id, 'CR_EDIT_CHECK_SPGRP');
				v_sptimeexclude := NVL (get_rule_value (p_ans_grp_id, 'CR_EDIT_CHECK_SPEXC'), 'N');
				v_sppatlistexclude := NVL (get_rule_value (p_ans_grp_id, 'CR_EDIT_CHECK_SPPLT'), 'N');

				SELECT VALUE, c623_patient_ans_list_id
				  INTO p_form_value, p_patient_ans_list_id
				  FROM (SELECT	 DECODE (t603.c901_control_type
									   , 6101, t623.c904_answer_ds	 -- TEXTBOX
									   , 6104, t623.c904_answer_ds	 -- TEXTAREA
									   , 6105, t623.c904_answer_ds	 -- TEXTREADONLY
									   , 6100, t605.c605_answer_value	-- COMBOBOX
									   , 6102, t623.c904_answer_ds	 -- CHECKBOX
										) VALUE
							   , t623.c623_patient_ans_list_id
							FROM t623_patient_answer_list t623
							   , t603_answer_grp t603
							   , t605_answer_list t605
							   , t622_patient_list t622
						   WHERE t622.c622_patient_list_id = t623.c622_patient_list_id
							 AND t623.c603_answer_grp_id = t603.c603_answer_grp_id
							 AND t623.c605_answer_list_id = t605.c605_answer_list_id(+)
							 AND t623.c601_form_id = p_form_id
							 AND t623.c603_answer_grp_id = p_ans_grp_id
							 AND t623.c621_patient_id = p_patient_id
							 --AND t622.c613_study_period_id = p_study_period_id
							 AND (t623.c904_answer_ds IS NOT NULL OR t623.c605_answer_list_id IS NOT NULL)
							 AND t623.c623_delete_fl = 'N'
							 AND t603.c603_delete_fl = 'N'
							 AND t622.c622_delete_fl = 'N'
							 AND t623.c622_patient_list_id IN (
									 SELECT t623.c622_patient_list_id	--, t605.C605_ANSWER_VALUE, t605.C605_ANSWER_LIST_NM
									   FROM t623_patient_answer_list t623
										  , t603_answer_grp t603
										  , t605_answer_list t605
										  , t622_patient_list t622
									  WHERE t622.c622_patient_list_id = t623.c622_patient_list_id
										AND t623.c603_answer_grp_id = t603.c603_answer_grp_id
										AND t623.c605_answer_list_id = t605.c605_answer_list_id(+)
										AND t623.c601_form_id = p_form_id
										AND t623.c603_answer_grp_id = v_spgrp
										AND (
												--t605.c605_answer_list_nm = v_spvalue OR t623.c904_answer_ds LIKE v_spvalue
												NVL (t605.c605_answer_list_nm, -9999) =
													DECODE (v_spvalue
														  , NULL, NVL (t605.c605_answer_list_nm, -9999)
														  , v_spvalue
														   )
											 OR NVL (t605.c605_answer_list_nm, -9999) LIKE
													DECODE (v_spvalue
														  , NULL, NVL (t605.c605_answer_list_nm, -9999)
														  , v_spvalue
														   )
											)
										AND t622.c613_study_period_id NOT IN
															   (DECODE (v_sptimeexclude, 'Y', p_study_period_id, -9999))
										AND t622.c622_patient_list_id NOT IN
															(DECODE (v_sppatlistexclude, 'Y', p_patient_list_id, -9999))
										AND t623.c621_patient_id = p_patient_id
										AND t623.c623_delete_fl = 'N'
										AND t603.c603_delete_fl = 'N'
										AND t622.c622_delete_fl = 'N')
						ORDER BY t623.c623_patient_ans_list_id ASC)
				 WHERE ROWNUM = 1;
			EXCEPTION
				WHEN NO_DATA_FOUND
				THEN
					p_form_value := NULL;
					p_patient_ans_list_id := NULL;
			END;
		ELSIF (p_ec_type = 93039)	-- Exam Date
		THEN
			/*
			 * If the type is exam date, the form_value will have the exam date and
			 * the patient_ans_list_id will be returned as NULL.
			 */
			BEGIN
				SELECT TO_CHAR (t622.c622_date, 'MM/DD/YYYY')
					 --ROUND (		MONTHS_BETWEEN (t622.c622_date ,
					 -- TO_DATE (t623.c904_answer_ds, NVL (get_rule_value (t623.c603_answer_grp_id, 'CR_EDIT_CHECK_DT_FMT'), 'MM/DD/YYYY')))/ 12, 1)
				,	   ''	-- Explicity passing NULL Answerlist ID as this editcheck will get only the Exam Date .
				  INTO p_form_value
					 , p_patient_ans_list_id
				  FROM t622_patient_list t622, t623_patient_answer_list t623
				 WHERE t622.c601_form_id = p_form_id
				   AND t622.c621_patient_id = p_patient_id
				   AND t622.c613_study_period_id = p_study_period_id
				   AND t622.c622_delete_fl = 'N'
				   AND t623.c622_patient_list_id = t622.c622_patient_list_id
				   AND t623.c603_answer_grp_id = p_ans_grp_id
				   AND t623.c623_delete_fl = 'N';
			EXCEPTION
				WHEN NO_DATA_FOUND
				THEN
					p_form_value := NULL;
					p_patient_ans_list_id := NULL;
			END;
		ELSE
			raise_application_error ('-20379', 'Not a valid edit check id  p_ec_type ** ' || p_ec_type);
		END IF;
	END gm_fch_form_value;

	/***********************************************************************************
	 * Description : This procedure is used to compare the two given values with the
	 *				 given operator and will return a boolean based on the comparision.
	 * Author	   : VPrasath
	 ***********************************************************************************/
	PROCEDURE gm_fch_compare (
		p_source_value	  IN	   VARCHAR2
	  , p_con_chk		  IN	   t6513_edit_check_details.c901_precondition_check%TYPE
	  , p_dest_value	  IN	   VARCHAR2
	  , p_extra_operand   IN	   VARCHAR2
	  , p_return_fl 	  OUT	   BOOLEAN
	)
	AS
		v_source_value VARCHAR2 (255);
		v_dest_value   VARCHAR2 (255);
		v_date		   DATE;
		v_number	   NUMBER;
	BEGIN
		p_return_fl := FALSE;

		/* To make the comparision easy, if the source_value or dest_value is  NULL
		 * we are replacing it with '^' and assign it to a local variable.
		 */
		IF p_source_value IS NULL
		THEN
			v_source_value := '^';
		ELSE
			v_source_value := p_source_value;
		END IF;

		IF p_dest_value IS NULL
		THEN
			v_dest_value := '^';
		ELSE
			v_dest_value := p_dest_value;
		END IF;

		/* Try doing a number comparision, if there is an exception
		 * then do a string comparision because 10.1 and 10.10 is equal in
		 * number comparision
		 */
		IF p_con_chk = 93015
		THEN   -- '='
			BEGIN
				IF TO_NUMBER (v_source_value) = TO_NUMBER (v_dest_value)
				THEN
					p_return_fl := TRUE;
				END IF;
			EXCEPTION
				WHEN INVALID_NUMBER OR VALUE_ERROR
				THEN
					BEGIN
						-- If not number check do date check
						SELECT CASE
								   WHEN (gm_get_to_date (v_dest_value) = gm_get_to_date (v_source_value))
									   THEN 1
								   ELSE 0
							   END
						  INTO v_number
						  FROM DUAL;

						-- Since we can only use 1 and 0 (no boolean allowed) numers used
						IF (v_number = 1)
						THEN
							p_return_fl := TRUE;
						END IF;
					EXCEPTION
						WHEN OTHERS
						THEN
							-- String compare
							IF v_source_value = v_dest_value
							THEN
								p_return_fl := TRUE;
							END IF;
					END;
			END;
		ELSIF p_con_chk = 93016
		THEN   -- '>'
			BEGIN
				IF TO_NUMBER (v_source_value) > TO_NUMBER (v_dest_value)
				THEN
					p_return_fl := TRUE;
				END IF;
			EXCEPTION
				WHEN INVALID_NUMBER OR VALUE_ERROR
				THEN
					-- If not number check do date check
					SELECT CASE
							   WHEN (gm_get_to_date (v_source_value) > gm_get_to_date (v_dest_value))
								   THEN 1
							   ELSE 0
						   END
					  INTO v_number
					  FROM DUAL;

					-- Since we can only use 1 and 0 (no boolean allowed) numers used
					IF (v_number = 1)
					THEN
						p_return_fl := TRUE;
					END IF;
			END;
		ELSIF p_con_chk = 93017
		THEN   -- '<'
			BEGIN
				IF TO_NUMBER (v_source_value) < TO_NUMBER (v_dest_value)
				THEN
					p_return_fl := TRUE;
				END IF;
			EXCEPTION
				WHEN INVALID_NUMBER OR VALUE_ERROR
				THEN
					-- If not number check do date check
					SELECT CASE
							   WHEN (gm_get_to_date (v_source_value) < gm_get_to_date (v_dest_value))
								   THEN 1
							   ELSE 0
						   END
					  INTO v_number
					  FROM DUAL;

					-- Since we can only use 1 and 0 (no boolean allowed) numers used
					IF (v_number = 1)
					THEN
						p_return_fl := TRUE;
					END IF;
			END;
		ELSIF p_con_chk = 93018
		THEN   -- '!='
			BEGIN
				IF TO_NUMBER (v_source_value) != TO_NUMBER (v_dest_value)
				THEN
					p_return_fl := TRUE;
				END IF;
			EXCEPTION
				WHEN INVALID_NUMBER OR VALUE_ERROR
				THEN
					BEGIN
						-- If not number check do date check
						SELECT CASE
								   WHEN (gm_get_to_date (v_source_value) != gm_get_to_date (v_dest_value))
									   THEN 1
								   ELSE 0
							   END
						  INTO v_number
						  FROM DUAL;

						-- Since we can only use 1 and 0 (no boolean allowed) numers used
						IF (v_number = 1)
						THEN
							p_return_fl := TRUE;
						END IF;
					EXCEPTION
						WHEN OTHERS
						THEN
							-- String compare
							IF v_source_value != v_dest_value
							THEN
								p_return_fl := TRUE;
							END IF;
					END;
			END;
		ELSIF p_con_chk = 93019
		THEN   --'RETURNTRUE'
			/* No need to compare anything*/
			p_return_fl := TRUE;
		ELSIF p_con_chk = 93020
		THEN   --'< YEAR'
			/*
			 * Extra operand will have the value to be compared.
			 * "For example, Source Value ='01/01/2000', dest value = '01/01/2009'
			 * the get_year_difference method will return 9 the extra operand
			 * will have a value entered for the rule, here we will
			 * take as 5, so 9 < 5 will return false"
			 */
			IF gm_get_year_difference (v_source_value, v_dest_value) < TO_NUMBER (p_extra_operand)
			THEN
				p_return_fl := TRUE;
			END IF;
		ELSIF p_con_chk = 93012
		THEN   --'> YEAR'
			/*
			 * Extra operand will have the value to be compared.
			 * "For example, Source Value ='01/01/2009', dest value = '01/01/2000'
			 * the get_year_difference method will return 9 the extra operand
			 * will have a value entered for the rule, here we will
			 * take as 5, so 5 > 9 will return false"
			 */
			IF gm_get_year_difference (v_source_value, v_dest_value) > TO_NUMBER (p_extra_operand)
			THEN
				p_return_fl := TRUE;
			END IF;
		ELSIF p_con_chk = 93021
		THEN   --'> MONTHS'
			/*
			 * Extra operand will have the value to be compared.
			 * "For example, Source Value ='01/01/2009', dest value = '04/01/2009'
			 * the get_month_difference method will return 4 the extra operand
			 * will have a value entered for the rule, here we will
			 * take as 6, so 6 < 4 will return true"
			 */
			IF gm_get_month_difference (v_source_value, v_dest_value) < TO_NUMBER (p_extra_operand)
			THEN
				p_return_fl := TRUE;
			END IF;
		ELSIF p_con_chk = 93024
		THEN   --'< Day'
			/*
			 * Extra operand will have the value to be compared.
			 * "For example, Source Value ='01/01/2009', dest value = '04/04/2009'
			 * the get_day_difference method will return 3 the extra operand
			 * will have a value entered for the rule, here we will
			 * take as 6, so 6 < 4 will return true"
			 */
			IF gm_get_day_difference (v_source_value, v_dest_value) < TO_NUMBER (p_extra_operand)
			THEN
				p_return_fl := TRUE;
			END IF;
		ELSIF p_con_chk = 93013
		THEN   --'> Days'
			/*
			 * Extra operand will have the value to be compared.
			 * "For example, Source Value ='04/04/2009', dest value = '01/01/2009'
			 * the get_day_difference method will return 3 the extra operand
			 * will have a value entered for the rule, here we will
			 * take as 6, so 6 > 3 will return true"
			 */
			IF gm_get_day_difference (v_source_value, v_dest_value) > TO_NUMBER (p_extra_operand)
			THEN
				p_return_fl := TRUE;
			END IF;
		ELSIF p_con_chk = 93014
		THEN   --'> Months'
			/*
			 * Extra operand will have the value to be compared.
			 * "For example, Source Value ='04/01/2009', dest value = '01/01/2009'
			 * the get_month_difference method will return 4 the extra operand
			 * will have a value entered for the rule, here we will
			 * take as 6, so 4 > 6 will return true"
			 */
			IF gm_get_month_difference (v_source_value, v_dest_value) > TO_NUMBER (p_extra_operand)
			THEN
				p_return_fl := TRUE;
			END IF;
		ELSIF p_con_chk = 93022
		THEN
			--'DATEFORMAT'
			/*
			 * Extra operand will have the Format that need to be validated.
			 * "For example, Source Value ='01/01/2009' the extra operand
			 * will have a value entered for the rule, here we will
			 * take as 'MM/DD/YYYY', if the date is not in 'MM/DD/YYYY'
			 * format then an exception will be thrown and then the
			 * return variable will be set to false otherwise true
			 */
			IF get_isdate_valid_format (v_source_value, p_extra_operand) = 'N'
			THEN
				p_return_fl := TRUE;
			ELSE
				p_return_fl := FALSE;
			END IF;
		ELSIF p_con_chk = 93023
		THEN   --'> CURRENT DATE Greated than'
			/*
			 * Check if the date entered is greater than current date and if date is in specified format
			 * If date not in specified format then 93022 Data Format check will handle the same
			 */
			BEGIN
				IF gm_get_to_date (v_source_value) >= TRUNC (SYSDATE)
				THEN
					p_return_fl := TRUE;
				ELSE
					p_return_fl := FALSE;
				END IF;
			EXCEPTION
				WHEN OTHERS
				THEN
					p_return_fl := FALSE;
			END;
		ELSE
			-- If no check should return true
			p_return_fl := TRUE;
		END IF;
	EXCEPTION
		WHEN OTHERS
		THEN
			p_return_fl := FALSE;
	END gm_fch_compare;

	/***********************************************************************************
	  * Description : This Function will return a Date based on the input char length
	  *
	  * Author		: Richard
	  ***********************************************************************************/
	FUNCTION gm_get_to_date (
		p_date	 VARCHAR2
	)
		RETURN DATE
	IS
	BEGIN
		IF LENGTH (p_date) = 10
		THEN
			RETURN TO_DATE (p_date, 'MM/DD/YYYY');
		ELSIF LENGTH (p_date) = 8
		THEN
			RETURN TO_DATE (p_date, 'MM/DD/YY');
		ELSIF LENGTH (p_date) = 7
		THEN
			RETURN TO_DATE (p_date, 'MM/YYYY');
		END IF;
	END gm_get_to_date;

	/***********************************************************************************
	  * Description : This Function will return a Number which is the 'year'
	  * 			  difference between the two different dates.
	  * Reference	:
	  * Author		: VPrasath
	  ***********************************************************************************/
	FUNCTION gm_get_year_difference (
		p_source_value	 VARCHAR2
	  , p_dest_value	 VARCHAR2
	)
		RETURN NUMBER
	IS
		v_num		   NUMBER;
	BEGIN
		SELECT ROUND (MONTHS_BETWEEN (gm_get_to_date (p_source_value), gm_get_to_date (p_dest_value)) / 12, 1)
		  INTO v_num
		  FROM DUAL;

		RETURN v_num;
	EXCEPTION
		WHEN OTHERS
		THEN
			RETURN NULL;
	END gm_get_year_difference;

	/***********************************************************************************
			 * Description : This Function will return a Number which is the 'month'
			 *				difference between the two different dates.
			 * Reference	:
			 * Author	 : VPrasath
			 ***********************************************************************************/
	FUNCTION gm_get_month_difference (
		p_source_value	 VARCHAR2
	  , p_dest_value	 VARCHAR2
	)
		RETURN NUMBER
	IS
		v_num		   NUMBER;
	BEGIN
		SELECT MONTHS_BETWEEN (gm_get_to_date (p_source_value), gm_get_to_date (p_dest_value))
		  INTO v_num
		  FROM DUAL;

		RETURN v_num;
	EXCEPTION
		WHEN OTHERS
		THEN
			RETURN NULL;
	END gm_get_month_difference;

	/***********************************************************************************
			 * Description : This Function will return a Number which is the 'day'
			 *				difference between the two different dates.
			 * Reference	:
			 * Author	 : Richard
			 ***********************************************************************************/
	FUNCTION gm_get_day_difference (
		p_source_value	 VARCHAR2
	  , p_dest_value	 VARCHAR2
	)
		RETURN NUMBER
	IS
		v_num		   NUMBER;
	BEGIN
		SELECT gm_get_to_date (p_source_value) - gm_get_to_date (p_dest_value)
		  INTO v_num
		  FROM DUAL;

		RETURN v_num;
	EXCEPTION
		WHEN OTHERS
		THEN
			RETURN NULL;
	END gm_get_day_difference;

	/******************************************************************************************
			 * Description : If a Edit Check didn't fire for the given data, then this procedure
			 *				will check if the edit check is fired already for the same patient, if any
			 *				incident is found it will close that incident as "Modified Correction"
			 *				( Meaning data is corrected by the user ). This procedure is get called
			 *				in two places, during the form data entry submit and when ever the
			 *				CRA or DM forcefully closing an incident with any one of the following
			 *				reasons "Protocol Deviation", "Data is Correct", "Edit Check Invalid".
			 * Author	 : VPrasath
			 *******************************************************************************************/
	PROCEDURE gm_sav_check_and_clear (
		p_edit_check_id 		IN	 t6513_edit_check_details.c6513_edit_check_details_id%TYPE
	  , p_patient_id			IN	 t622_patient_list.c621_patient_id%TYPE
	  , p_user_id				IN	 t622_patient_list.c622_created_by%TYPE
	  , p_reason				IN	 t6514_ec_incident.c901_status_reason%TYPE
	  , p_incident_id			IN	 t6514_ec_incident.c6514_ec_incident_id%TYPE
	  , p_patient_ans_list_id	IN	 t623_patient_answer_list.c623_patient_ans_list_id%TYPE
	)
	AS
		v_ans_list_count NUMBER;
		v_linked_ans_list_count NUMBER;

		CURSOR v_cursor
		IS
			SELECT t6514.c6514_ec_incident_id ID, t6514.c623_patient_ans_list_id ans_list_id
				 , t6514.c623_linked_pt_ans_list_id linked_ans_list_id
			  FROM t6514_ec_incident t6514, t6513_edit_check_details t6513
			 WHERE t6514.c6513_edit_check_details_id = t6513.c6513_edit_check_details_id
			   AND t6513.c6513_edit_check_details_id = p_edit_check_id
			   AND t6514.c621_patient_id = p_patient_id
			   AND t6514.c6514_status = 10	 -- Open
			   AND t6514.c6514_ec_incident_id = NVL (p_incident_id, t6514.c6514_ec_incident_id)
			   AND t6514.c623_patient_ans_list_id = NVL (p_patient_ans_list_id, t6514.c623_patient_ans_list_id);
	BEGIN
		FOR v_index IN v_cursor
		LOOP
			UPDATE t6514_ec_incident t6514
			   SET c6514_status = 20   -- Closed
				 , c901_status_reason = p_reason
				 , c6514_last_updated_by = p_user_id
				 , c6514_last_updated_date = SYSDATE
			 WHERE t6514.c6514_ec_incident_id = v_index.ID;

			/*
			 * There can be more than one edit check for the same patient_ans_list_id
			 * Check if any patient_ans_list id is marked as edit check occured
			 * and if atleast one of the incident is open don't remove the check
			 * from the patient_ans_list_id.
			 */
			SELECT COUNT (1)
			  INTO v_ans_list_count
			  FROM t6514_ec_incident t6514
			 WHERE (   t6514.c623_linked_pt_ans_list_id = v_index.ans_list_id
					OR t6514.c623_patient_ans_list_id = v_index.ans_list_id
				   )
			   AND t6514.c6514_status = 10;   -- Open

			IF v_ans_list_count = 0
			THEN
				UPDATE t623_patient_answer_list t623
				   SET c623_edit_check_fl = ''
					 , c623_last_updated_date = SYSDATE
					 , c623_last_updated_by = p_user_id
				 WHERE t623.c623_patient_ans_list_id = v_index.ans_list_id;
			END IF;

			/*
			 * There can be more than one edit check for the same linked_ans_list_id
			 * Check if any linked_ans_list_id is marked as edit check occured
			 * and if atleast one of the incident is open don't remove the check
			 * from the patient_ans_list_id.
			 */
			SELECT COUNT (1)
			  INTO v_linked_ans_list_count
			  FROM t6514_ec_incident t6514
			 WHERE (   t6514.c623_patient_ans_list_id = v_index.linked_ans_list_id
					OR t6514.c623_patient_ans_list_id = v_index.linked_ans_list_id
				   )
			   AND t6514.c6514_status = 10;   --Open

			IF v_linked_ans_list_count = 0
			THEN
				UPDATE t623_patient_answer_list t623
				   SET c623_edit_check_fl = ''
					 , c623_last_updated_date = SYSDATE
					 , c623_last_updated_by = p_user_id
				 WHERE t623.c623_patient_ans_list_id = v_index.linked_ans_list_id;
			END IF;

			/* whenever an incident is updated, the history needs to be updated, 92474 -> Edit Check Cleared */
			gm_sav_form_history (v_index.ID, 92474, p_user_id);
		END LOOP;
	END gm_sav_check_and_clear;

	/*************************************************************************************
			 * Description : This procedure will form an incident based on the incident happened.
			 * P.S. 		: Need to remove the decoration from the data once the
			 *				solution in DHTMLX is ready
			 * Author	 : VPrasath
			 *************************************************************************************/
	PROCEDURE gm_fch_incident_description (
		p_ec_details_id 		   IN		t6513_edit_check_details.c6513_edit_check_details_id%TYPE
	  , p_study_period_id		   IN		t6513_edit_check_details.c613_study_period_id%TYPE
	  , p_source_form_value 	   IN		t6514_ec_incident.c6514_form_value%TYPE
	  , p_linked_study_period_id   IN		t6513_edit_check_details.c613_study_period_id%TYPE
	  , p_dest_form_value		   IN		t6514_ec_incident.c6514_form_value%TYPE
	  , p_incident_description	   OUT		VARCHAR2
	)
	AS
		v_source_form  t612_study_form_list.c612_study_form_ds%TYPE;
		v_destination_form t612_study_form_list.c612_study_form_ds%TYPE;
		v_source_question VARCHAR2 (255);
		v_dest_question VARCHAR2 (255);
		v_source_study_period VARCHAR2 (255);
		v_dest_study_period VARCHAR2 (255);
		v_form_lbl	   VARCHAR2 (20) := ' FORM = ';
		v_study_period_lbl VARCHAR2 (20) := ' , STUDY PERIOD = ';
		v_question_lbl VARCHAR2 (20) := ', QUESTION = ';
		v_answer_lbl   VARCHAR2 (20) := ', ANSWER = ';
		v_dest_form_lbl VARCHAR2 (20) := ' FORM = ';
		v_dest_study_period_lbl VARCHAR2 (20) := ' , STUDY PERIOD = ';
		v_dest_question_lbl VARCHAR2 (20) := ', QUESTION = ';
		v_dest_answer_lbl VARCHAR2 (20) := ', ANSWER = ';
		v_dest_ec_type t6513_edit_check_details.c901_linked_edit_check_type%TYPE;
	BEGIN
		SELECT t612a.c612_study_form_ds source_form
			 , DECODE (t604a.c604_disp_ques_no, '', t602a.c602_question_ds, t604a.c604_seq_no) source_question
		  INTO v_source_form
			 , v_source_question
		  FROM t6512_edit_check t6512
			 , t6513_edit_check_details t6513
			 , t612_study_form_list t612a
			 , t603_answer_grp t603a
			 , t604_form_ques_link t604a
			 , t602_question_master t602a
		 WHERE t6512.c6512_edit_check_id = t6513.c6512_edit_check_id
		   AND t6513.c6513_edit_check_details_id = p_ec_details_id
		   AND t612a.c611_study_id = t6512.c611_study_id
		   AND t612a.c601_form_id = t6513.c601_form_id
		   AND t603a.c603_answer_grp_id = t6513.c603_answer_grp_id
		   AND t604a.c602_question_id = t603a.c602_question_id
		   AND t604a.c601_form_id = t6513.c601_form_id
		   AND t603a.c602_question_id = t602a.c602_question_id;

		SELECT get_code_name (t613.c901_study_period)
		  INTO v_source_study_period
		  FROM t613_study_period t613
		 WHERE t613.c613_study_period_id = p_study_period_id;

		BEGIN
			SELECT t6513.c901_linked_edit_check_type
			  INTO v_dest_ec_type
			  FROM t6513_edit_check_details t6513
			 WHERE t6513.c6513_edit_check_details_id = p_ec_details_id;

			IF v_dest_ec_type = 93039
			THEN
				v_dest_question_lbl := '';
				v_dest_question := '';
				v_dest_answer_lbl := ', EXAM DATE = ';

				SELECT t612b.c612_study_form_ds
				  INTO v_destination_form
				  FROM t6512_edit_check t6512, t6513_edit_check_details t6513, t612_study_form_list t612b
				 WHERE t6512.c6512_edit_check_id = t6513.c6512_edit_check_id
				   AND t6513.c6513_edit_check_details_id = p_ec_details_id
				   AND t612b.c611_study_id = t6512.c611_study_id
				   AND t612b.c601_form_id = t6513.c601_linked_form_id;
			ELSE
				SELECT t612b.c612_study_form_ds
					 , DECODE (t604b.c604_disp_ques_no, '', t602b.c602_question_ds, t604b.c604_seq_no)
				  INTO v_destination_form
					 , v_dest_question
				  FROM t6512_edit_check t6512
					 , t6513_edit_check_details t6513
					 , t612_study_form_list t612b
					 , t603_answer_grp t603b
					 , t604_form_ques_link t604b
					 , t602_question_master t602b
				 WHERE t6512.c6512_edit_check_id = t6513.c6512_edit_check_id
				   AND t6513.c6513_edit_check_details_id = p_ec_details_id
				   AND t612b.c611_study_id = t6512.c611_study_id
				   AND t612b.c601_form_id = t6513.c601_linked_form_id
				   AND t603b.c603_answer_grp_id = t6513.c603_linked_answer_grp_id
				   AND t604b.c602_question_id = t603b.c602_question_id
				   AND t604b.c601_form_id = t6513.c601_linked_form_id
				   AND t603b.c602_question_id = t602b.c602_question_id;
			END IF;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				v_destination_form := '';
				v_dest_question := '';
		END;

		BEGIN
			SELECT get_code_name (t613.c901_study_period)
			  INTO v_dest_study_period
			  FROM t613_study_period t613
			 WHERE t613.c613_study_period_id = p_linked_study_period_id;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				v_dest_study_period := '';
		END;

		IF v_source_form = 'SD8' AND v_source_question = '11'
		THEN
			v_source_question := ' 1 TO 10 ';
			v_answer_lbl := ', SUM * 2 =	';
			v_question_lbl := ', QUESTIONS = ';
		END IF;

		p_incident_description :=
			   '<font color=blue>'
			|| v_form_lbl
			|| v_source_form
			|| v_study_period_lbl
			|| v_source_study_period
			|| v_question_lbl
			|| v_source_question
			|| '<b>'
			|| v_answer_lbl
			|| '</b>'
			|| '<b>'
			|| p_source_form_value
			|| '</b>'
			|| ' </font> ';

		IF v_destination_form IS NOT NULL
		THEN
			p_incident_description :=
				   p_incident_description
				|| '<font color=red> AND  </font>'
				|| '<font color=green>'
				|| v_dest_form_lbl
				|| v_destination_form
				|| v_dest_study_period_lbl
				|| v_dest_study_period
				|| v_dest_question_lbl
				|| v_dest_question
				|| '<b>'
				|| v_dest_answer_lbl
				|| '</b>'
				|| '<b>'
				|| p_dest_form_value
				|| '</b>'
				|| ' </font> ';
		END IF;
	END gm_fch_incident_description;

	/**********************************************************************
			 * Description : This is a job that will call the edit check procedure
			 *				that if any study is scheduled on	the particular day
			 * Author	 : VPrasath
			 ***********************************************************************/
	PROCEDURE gm_sav_edit_chk_job
	AS
		v_count 	   NUMBER;
		v_study_id	   t611_clinical_study.c611_study_id%TYPE;

		CURSOR v_cursor
		IS
			SELECT t622.c622_patient_list_id ID
			  FROM t621_patient_master t621, t622_patient_list t622
			 WHERE t621.c621_patient_id = t622.c621_patient_id
			   AND t621.c611_study_id = v_study_id
			   AND t621.c621_delete_fl = 'N'
			   AND t622.c622_delete_fl = 'N';

		CURSOR v_study_cur
		IS
			SELECT t611.c611_study_id study_id
			  FROM t611_clinical_study t611
			 WHERE TRUNC (t611.c611_ec_job_scheduled_date) = TRUNC (SYSDATE) AND t611.c611_delete_fl = 'N';
	BEGIN
		FOR v_study_index IN v_study_cur
		LOOP
			v_study_id	:= v_study_index.study_id;

			FOR v_index IN v_cursor
			LOOP
				gm_sav_incidents (v_index.ID, 30301);
			END LOOP;
		END LOOP;
	END gm_sav_edit_chk_job;

	/*************************************************************************
			 * Description : This procedure will create a history for the incident
			 *				happened or cleared on a particular patient_ans_list_id
			 * Author	 : VPrasath
			 *************************************************************************/
	PROCEDURE gm_sav_form_history (
		p_incident_id	t6514_ec_incident.c6514_ec_incident_id%TYPE
	  , p_status		t623a_patient_answer_history.c901_status%TYPE
	  , p_user_id		t623a_patient_answer_history.c623_created_by%TYPE
	)
	AS
		v_patient_answer_list_id t6514_ec_incident.c623_patient_ans_list_id%TYPE;
		v_linked_pt_ans_list_id t6514_ec_incident.c623_patient_ans_list_id%TYPE;
	BEGIN
		SELECT t6514.c623_patient_ans_list_id, t6514.c623_linked_pt_ans_list_id
		  INTO v_patient_answer_list_id, v_linked_pt_ans_list_id
		  FROM t6514_ec_incident t6514
		 WHERE t6514.c6514_ec_incident_id = p_incident_id;

		DELETE FROM t623a_patient_answer_history t623a
			  WHERE t623a.c6514_ec_incident_id = p_incident_id;

		INSERT INTO t623a_patient_answer_history
					(c623a_patient_ans_his_id, c601_form_id, c602_question_id, c603_answer_grp_id, c605_answer_list_id
				   , c904_answer_ds, c622_patient_list_id, c623_delete_fl, c623_created_by, c623_created_date
				   , c623_patient_ans_list_id, c621_patient_id, c6514_ec_incident_id, c6511_query_text, c901_status)
			(SELECT s623a_patient_answer_history.NEXTVAL, t623.c601_form_id, t623.c602_question_id
				  , t623.c603_answer_grp_id, t623.c605_answer_list_id, t623.c904_answer_ds, t623.c622_patient_list_id
				  , t623.c623_delete_fl, p_user_id, SYSDATE, t6514.c623_patient_ans_list_id, t623.c621_patient_id
				  , t6514.c6514_ec_incident_id
				  , DECODE (p_status
						  , 92473, t6514.c6514_incident_description
						  , 'Check Closed as ' || get_code_name (t6514.c901_status_reason)
						   )
				  , p_status
			   FROM t6514_ec_incident t6514, t623_patient_answer_list t623
			  WHERE t6514.c6514_ec_incident_id = p_incident_id
				AND t623.c623_patient_ans_list_id = t6514.c623_patient_ans_list_id
				AND NVL(t623.C623_DELETE_FL,'N')='N');

		INSERT INTO t623a_patient_answer_history
					(c623a_patient_ans_his_id, c601_form_id, c602_question_id, c603_answer_grp_id, c605_answer_list_id
				   , c904_answer_ds, c622_patient_list_id, c623_delete_fl, c623_created_by, c623_created_date
				   , c623_patient_ans_list_id, c621_patient_id, c6514_ec_incident_id, c6511_query_text, c901_status)
			(SELECT s623a_patient_answer_history.NEXTVAL, t623.c601_form_id, t623.c602_question_id
				  , t623.c603_answer_grp_id, t623.c605_answer_list_id, t623.c904_answer_ds, t623.c622_patient_list_id
				  , t623.c623_delete_fl, p_user_id, SYSDATE, t6514.c623_linked_pt_ans_list_id, t623.c621_patient_id
				  , t6514.c6514_ec_incident_id
				  , DECODE (p_status
						  , 92473, t6514.c6514_incident_description
						  , 'Check Closed as ' || get_code_name (t6514.c901_status_reason)
						   )
				  , p_status
			   FROM t6514_ec_incident t6514, t623_patient_answer_list t623
			  WHERE t6514.c6514_ec_incident_id = p_incident_id
			  	AND NVL(t623.C623_DELETE_FL,'N')='N'
				AND t623.c623_patient_ans_list_id = t6514.c623_linked_pt_ans_list_id);

		UPDATE t623_patient_answer_list t623
		   SET t623.c623_history_flag = 'Y'
			 , t623.c623_last_updated_by = p_user_id
			 , t623.c623_last_updated_date = SYSDATE
		 WHERE (   t623.c623_patient_ans_list_id = v_patient_answer_list_id
				OR t623.c623_patient_ans_list_id = v_linked_pt_ans_list_id
			   );
	END gm_sav_form_history;

	/*************************************************************************
			 * Description : This Function will return the study_period_id for the
			 *				given info.
			 * Author	 : VPrasath
			 *************************************************************************/
	FUNCTION get_linked_study_period_id (
		p_source_form_id	IN	 t612_study_form_list.c601_form_id%TYPE
	  , p_study_period_id	IN	 t613_study_period.c613_study_period_id%TYPE
	  , p_patient_list_id	IN	 t622_patient_list.c622_patient_list_id%TYPE
	)
		RETURN NUMBER
	IS
		v_study_period_id t613_study_period.c613_study_period_id%TYPE;
	BEGIN
		SELECT t613.c613_study_period_id
		  INTO v_study_period_id
		  FROM t612_study_form_list t612
			 , t613_study_period t613
			 , t613_study_period t613a
			 , t622_patient_list t622
			 , t621_patient_master t621
		 WHERE t612.c612_study_list_id = t613.c612_study_list_id
		   AND t613.c901_study_period = t613a.c901_study_period
		   AND t622.c621_patient_id = t621.c621_patient_id
		   AND t612.c611_study_id = t621.c611_study_id
		   AND t612.c601_form_id = p_source_form_id
		   AND t613a.c613_study_period_id = p_study_period_id
		   AND t622.c622_patient_list_id = p_patient_list_id;

		RETURN v_study_period_id;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN NULL;
	END get_linked_study_period_id;

	/*************************************************************************
			 * Description : This Function will return Y or N based on the date
			 *				format value
			 * Author		: Richard
			 *************************************************************************/
	FUNCTION get_isdate_valid_format (
		p_date	   VARCHAR2
	  , p_format   VARCHAR2
	)
		RETURN VARCHAR2
	AS
		v_date_value   DATE;
		RESULT		   NUMBER;
	BEGIN
		-- '^' in the begining NULL replace with ^ Symbol
		-- only for clinical
		IF p_date = '^'
		THEN
			RETURN 'Y';
		END IF;

		v_date_value := TO_DATE (p_date, p_format);

		-- Verify the format
		IF	   v_date_value IS NOT NULL
		   AND INSTR (SUBSTR (p_date, 1, 3), '/') = 3
		   AND INSTR (SUBSTR (p_date, 4, 3), '/') = 3
		   AND LENGTH (p_date) = LENGTH (p_format)
		THEN
			RETURN 'Y';
		ELSE
			RETURN 'N';
		END IF;
	EXCEPTION
		WHEN OTHERS
		THEN
			RETURN 'N';
	END get_isdate_valid_format;
PROCEDURE gm_fch_editcheck(
	p_ref_id IN T6512_EDIT_CHECK.C6512_EDIT_REF_ID%TYPE,
	p_out_data OUT Types.cursor_type
)
AS
BEGIN
	OPEN p_out_data	
	FOR SELECT 
		C6512_EDIT_REF_ID REFID,
		C6512_EDIT_DESCRIPTION DESCRIPTION,
		C6512_EDIT_SPEC SPECIFICATION,
		C6512_ACTIVE_FL ACTIVEFLAG 
		FROM 
			T6512_EDIT_CHECK
		WHERE 
			C6512_EDIT_REF_ID=p_ref_id;
END gm_fch_editcheck;


PROCEDURE gm_sav_editcheck (
		p_ref_id IN T6512_EDIT_CHECK.C6512_EDIT_REF_ID%TYPE,
		p_desc IN T6512_EDIT_CHECK.C6512_EDIT_DESCRIPTION%TYPE,
		p_spec IN T6512_EDIT_CHECK.C6512_EDIT_SPEC%TYPE,
		p_active_fl IN T6512_EDIT_CHECK.C6512_ACTIVE_FL%TYPE,
		p_userid IN T6512_EDIT_CHECK.C6512_CREATED_BY%TYPE,
		p_chk_id IN OUT T6512_EDIT_CHECK.C6512_EDIT_CHECK_ID%TYPE)
	AS
	v_chkid NUMBER;
	BEGIN
		UPDATE T6512_EDIT_CHECK
			SET
				T6512_EDIT_CHECK.C6512_EDIT_REF_ID=p_ref_id,
				T6512_EDIT_CHECK.C6512_EDIT_DESCRIPTION=p_desc,
				--T6512_EDIT_CHECK.C6512_EDIT_SPEC=p_spec,
				T6512_EDIT_CHECK.C6512_ACTIVE_FL=p_active_fl,
				T6512_EDIT_CHECK.C6512_LAST_UPDATED_BY=p_userid,
				T6512_EDIT_CHECK.C6512_LAST_UPDATED_DATE=SYSDATE 
			WHERE
				T6512_EDIT_CHECK.C6512_EDIT_REF_ID=p_ref_id;
				
	IF (SQL%ROWCOUNT = 0) THEN
		SELECT S6512_EDIT_CHECK.NEXTVAL INTO v_chkid FROM DUAL;
		INSERT 
			INTO T6512_EDIT_CHECK
			(
				C6512_EDIT_CHECK_ID,
				C6512_EDIT_REF_ID,
				C6512_EDIT_DESCRIPTION,
				--C6512_EDIT_SPEC,
				C6512_ACTIVE_FL,
				C611_STUDY_ID,
				C6512_CREATED_BY,
				C6512_CREATED_DATE
			) VALUES 
			(
				v_chkid,
				p_ref_id,
				p_desc,
				--p_spec,
				p_active_fl,
				'GPR003',
				p_userid,
				SYSDATE
			);
			p_chk_id:=TO_CHAR (v_chkid);
	END IF;
END gm_sav_editcheck;

END gm_pkg_cr_edit_check;
/
