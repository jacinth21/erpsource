/* Formatted on 2010/08/17 12:48 (Formatter Plus v4.8.0) */
--@"c:\database\packages\Clinical\gm_pkg_cr_patient_track.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_cr_patient_track
IS
--
/*******************************************************
 * Purpose: Package holds Clinical Research
 * Report Methods
 * Created By VPrasath
 *******************************************************/

	--
/*******************************************************
 * Purpose: This Procedure is used to fetch
 * the details of the surgery planned
 *******************************************************/
--
	PROCEDURE gm_fch_patient_window_list (
		p_studyid			IN		 t614_study_site.c611_study_id%TYPE
	  , p_site_id			IN		 VARCHAR2
	  , p_patientlist_cur	OUT 	 TYPES.cursor_type
	)
	AS
		v_form_id	   t622_patient_list.c601_form_id%TYPE;
		v_study_period_id NUMBER;
	BEGIN
		-- To assign Accounts Information
		my_context.set_my_inlist_ctx (p_site_id);

		SELECT t901.c902_code_nm_alt, t901.c901_code_nm
		  INTO v_form_id, v_study_period_id
		  FROM t901_code_lookup t901
		 WHERE t901.c901_code_id = get_rule_value (p_studyid, 'FCH_PATIENT_TRACK');

		OPEN p_patientlist_cur
		 FOR
			 SELECT   v621.c621_patient_id ID, v621.c621_patient_ide_no NAME, v621.c613_study_period_ds period
					, v621.c621_exp_date_start w_open, v621.c621_exp_date_final w_close
					, (SELECT NVL (c622_missing_follow_up_fl, '')
						 FROM t622_patient_list
						WHERE c622_missing_follow_up_fl IS NOT NULL
						  AND c621_patient_id = v621.c621_patient_id
						  AND c613_study_period_id = v621.c613_study_period_id) missing_cur_fl
					, (SELECT DECODE (SIGN (0 - COUNT (c622_missing_follow_up_fl)), -1, 'Y', '')
						 FROM t622_patient_list
						WHERE c622_missing_follow_up_fl IS NOT NULL
						  AND c621_patient_id = v621.c621_patient_id
						  AND NVL(C622_DELETE_FL,'N')='N'
						  AND c601_form_id IN (v_form_id)) missing_fl
					, v621.c611_study_id study_id, v621.c614_study_site_id site_id
				 FROM v621_patient_study_list v621
				WHERE (v621.c621_patient_ide_no, v621.c621_exp_date_start, v621.c621_exp_date_final) IN (
						  SELECT   v621.c621_patient_ide_no, MAX (v621.c621_exp_date_start)
								 , MAX (v621.c621_exp_date_final)
							  FROM v621_patient_study_list v621
							 WHERE (v621.c621_exp_date_start <= SYSDATE OR v621.c621_exp_date_final <= SYSDATE
								   )   -- Only retrieves the due information
						  GROUP BY v621.c621_patient_ide_no)
				  AND v621.c621_surgery_comp_fl = 'Y'
				  AND v621.c601_form_id IN (v_form_id)
				  AND v621.c611_study_id = p_studyid
				  AND v621.c614_study_site_id IN DECODE (p_site_id
													   , '0', v621.c614_study_site_id
													   , '', v621.c614_study_site_id
													   , (SELECT *
															FROM v_in_list)
														)
			 ORDER BY v621.c621_patient_ide_no, c901_surgery_type, c613_seq_no;
	END gm_fch_patient_window_list;

   --
/*******************************************************
 * Purpose: This Procedure is used to fetch
 * the details of the surgery planned
 *******************************************************/
--
	PROCEDURE gm_fch_patient_window_info (
		p_studyid			IN		 t614_study_site.c611_study_id%TYPE
	  , p_site_id			IN		 VARCHAR2
	  , p_patient_id		IN		 NUMBER
	  , p_patientlist_cur	OUT 	 TYPES.cursor_type
	)
	AS
		v_form_id	   t622_patient_list.c601_form_id%TYPE;
		v_study_period_id NUMBER;
	BEGIN
		-- To assign Accounts Information
		my_context.set_my_inlist_ctx (p_site_id);

		SELECT t901.c902_code_nm_alt, t901.c901_code_nm
		  INTO v_form_id, v_study_period_id
		  FROM t901_code_lookup t901
		 WHERE t901.c901_code_id = get_rule_value (p_studyid, 'FCH_PATIENT_TRACK');

		OPEN p_patientlist_cur
		 FOR
			 SELECT   v621.c621_patient_id ID, v621.c621_patient_ide_no NAME, v621.c613_study_period_id period_id
					, v621.c613_study_period_ds period, v621.c621_exp_date_start w_open
					, v621.c621_exp_date_final w_close, t622.c622_missing_follow_up_fl missing_cur_fl
				 FROM v621_patient_study_list v621, t622_patient_list t622
				WHERE v621.c621_patient_id = t622.c621_patient_id(+)
				  AND NVL(t622.C622_DELETE_FL,'N') = 'N'
				  AND v621.c613_study_period_id = t622.c613_study_period_id(+)
				  AND v621.c611_study_id = p_studyid
				  AND v621.c601_form_id IN (v_form_id)
				  AND v621.c613_study_period_id NOT IN (v_study_period_id)
				  AND v621.c621_patient_ide_no = p_patient_id
				  AND v621.c614_study_site_id IN DECODE (p_site_id
													   , '0', v621.c614_study_site_id
													   , '', v621.c614_study_site_id
													   , (SELECT *
															FROM v_in_list)
														)
			 ORDER BY v621.c613_seq_no;
	END gm_fch_patient_window_info;

   --
/*******************************************************
 * Purpose: This Procedure is used to fetch
 * the details of the surgery planned
 *******************************************************/
--
	PROCEDURE gm_fch_patient_track_form (
		p_studyid			IN		 t614_study_site.c611_study_id%TYPE
	  , p_site_id			IN		 VARCHAR2
	  , p_patient_id		IN		 NUMBER
	  , p_out_header		OUT 	 TYPES.cursor_type
	  , p_patientform_cur	OUT 	 TYPES.cursor_type
	)
	AS
		v_form_id	   t622_patient_list.c601_form_id%TYPE;
		v_study_period_id NUMBER;
		v_advform_id   t622_patient_list.c601_form_id%TYPE;
		v_advans_grp_id NUMBER;
	BEGIN
		-- To assign Accounts Information
		my_context.set_my_inlist_ctx (p_site_id);

		SELECT t901.c902_code_nm_alt, t901.c901_code_nm
		  INTO v_form_id, v_study_period_id
		  FROM t901_code_lookup t901
		 WHERE t901.c901_code_id = get_rule_value (p_studyid, 'FCH_PATIENT_TRACK');

		SELECT t901.c902_code_nm_alt, t901.c901_code_nm
		  INTO v_advform_id, v_advans_grp_id
		  FROM t901_code_lookup t901
		 WHERE t901.c901_code_id = get_rule_value (p_studyid, 'FCH_PATIENT_FTRACK');

		OPEN p_out_header
		 FOR
			 SELECT DISTINCT c612_study_form_ds, c612_seq
						FROM t612_study_form_list
					   WHERE c611_study_id = p_studyid
					ORDER BY c612_seq;

		OPEN p_patientform_cur
		 FOR
			 SELECT   v621.c621_patient_ide_no ID, v621.c613_study_period_ds, v621.c612_study_form_ds
					, CASE
						  WHEN t622.c613_study_period_id IS NULL OR TRIM (t622.c622_missing_follow_up_fl) = 'Y'
							  THEN '  '
						  ELSE '*'
					  END cnt
					, v621.c612_seq, v621.c613_seq_no, v621.c621_exp_date_final duedate
				 FROM t622_patient_list t622, v621_patient_study_list v621
				WHERE v621.c621_patient_id = t622.c621_patient_id(+)
				  AND v621.c601_form_id = t622.c601_form_id(+)
				  AND v621.c613_study_period_id = t622.c613_study_period_id(+)
				  AND (v621.c621_exp_date_start <= SYSDATE OR v621.c621_exp_date_final <= SYSDATE)
				  AND v621.c621_surgery_comp_fl = 'Y'
				  AND t622.c622_delete_fl = 'N'
				  AND v621.c611_study_id = p_studyid
				  AND v621.c621_patient_ide_no = p_patient_id
				  AND v621.c614_study_site_id IN DECODE (p_site_id
													   , '0', v621.c614_study_site_id
													   , '', v621.c614_study_site_id
													   , (SELECT *
															FROM v_in_list)
														)
			 UNION
			 SELECT   v621.c621_patient_ide_no ID, v621.c613_study_period_ds, v621.c612_study_form_ds
					, CASE
						  WHEN t622.c613_study_period_id IS NULL OR TRIM (t622.c622_missing_follow_up_fl) = 'Y'
							  THEN '  '
						  ELSE '*'
					  END cnt
					, v621.c612_seq, v621.c613_seq_no, TO_DATE (NULL) duedate
				 FROM t622_patient_list t622, v621_patient_study_list v621
				WHERE v621.c621_patient_id = t622.c621_patient_id(+)
				  AND v621.c601_form_id = t622.c601_form_id(+)
				  AND v621.c613_study_period_id = t622.c613_study_period_id(+)
				  AND (v621.c621_exp_date_start > SYSDATE OR v621.c621_exp_date_final > SYSDATE)
				  AND v621.c621_surgery_comp_fl = 'Y'
				  AND t622.c622_delete_fl = 'N'
				  AND v621.c611_study_id = p_studyid
				  AND v621.c621_patient_ide_no = p_patient_id
				  AND v621.c614_study_site_id IN DECODE (p_site_id
													   , '0', v621.c614_study_site_id
													   , '', v621.c614_study_site_id
													   , (SELECT *
															FROM v_in_list)
														)
			 UNION
			 SELECT   t1.ID ID, t1.c613_study_period_ds, t1.c612_study_form_ds, cnt, t1.c612_seq, v621.c613_seq_no
					, TO_DATE (SYSDATE) duedate
				 FROM (SELECT	t621.c621_patient_ide_no ID
							  , get_time_point (p_studyid, p_patient_id, t623.c904_answer_ds) c613_study_period_ds
							  , t612.c612_study_form_ds, TO_CHAR (COUNT (1)) cnt, t612.c612_seq
						   FROM t621_patient_master t621, t623_patient_answer_list t623, t612_study_form_list t612
						  WHERE t621.c621_patient_id = t623.c621_patient_id
							AND t612.c601_form_id = t623.c601_form_id
							AND t621.c621_surgery_comp_fl = 'Y'
							AND t621.c621_patient_ide_no = p_patient_id
							AND t623.c603_answer_grp_id = v_advans_grp_id
							AND t623.c601_form_id = v_advform_id
							AND NVL(t623.C623_DELETE_FL,'N')='N'
					   GROUP BY t621.c621_patient_ide_no
							  , get_time_point (p_studyid, p_patient_id, t623.c904_answer_ds)
							  , t612.c612_study_form_ds
							  , t612.c612_seq) t1
					, v621_patient_study_list v621
				WHERE v621.c613_study_period_ds = t1.c613_study_period_ds
				  AND v621.c621_patient_ide_no = t1.ID
				  AND v621.c601_form_id IN (v_form_id)
				  AND v621.c614_study_site_id IN DECODE (p_site_id
													   , '0', v621.c614_study_site_id
													   , '', v621.c614_study_site_id
													   , (SELECT *
															FROM v_in_list)
														)
			 ORDER BY c613_seq_no, c612_seq;
	END gm_fch_patient_track_form;

/******************************************************************
	* Description : Procedure to select the patient details
	* Author : Arockia Prasath
	****************************************************************/
	PROCEDURE gm_fch_patientmapping (
		p_patientide_no    IN		t621_patient_master.c621_patient_ide_no%TYPE
	  , p_out_patientmap   OUT		TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_out_patientmap
		 FOR
			 SELECT c621_patient_id patientid, c621_patient_ide_no patientide, c621_first_name firstname
				  , c621_last_name lastname
			   FROM t621_patient_master
			  WHERE c621_patient_ide_no = p_patientide_no AND c621_delete_fl = 'N';
	END gm_fch_patientmapping;

	/*******************************************************
	* Description : Procedure to update the patient details
	* Author		: Arockia Prasath
	*******************************************************/
	PROCEDURE gm_sav_patient_details (
		p_patient_id   IN	t621_patient_master.c621_patient_id%TYPE
	  , p_first_name   IN	t621_patient_master.c621_first_name%TYPE
	  , p_last_name    IN	t621_patient_master.c621_last_name%TYPE
	  , p_userid	   IN	t621_patient_master.c621_last_updated_by%TYPE
	)
	AS
		v_cnt		   NUMBER;
	BEGIN
		
			UPDATE t621_patient_master
			   SET c621_first_name = p_first_name
				 , c621_last_name = p_last_name
				 , c621_last_updated_by = p_userid
				 , c621_last_updated_date = SYSDATE
			 WHERE c621_patient_id = p_patient_id;    
	END gm_sav_patient_details;
END gm_pkg_cr_patient_track;
/
