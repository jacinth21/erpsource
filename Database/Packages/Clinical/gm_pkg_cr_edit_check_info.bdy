/* Formatted on 2010/06/10 09:48 (Formatter Plus v4.8.0) */
--@"C:\database\Packages\Clinical\gm_pkg_cr_edit_check_info.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_cr_edit_check_info
IS
--
/*******************************************************
 * Purpose: This Procedure is used to fetch
 * the data about form ID and Name

 *******************************************************/
--
	PROCEDURE gm_fch_edit_check_summary (
		p_studyid		IN		 t6512_edit_check.c611_study_id%TYPE
	  , p_formid		IN		 t6512_edit_check.c601_form_id%TYPE
	  , p_status		IN		 t6512_edit_check.c6512_active_fl%TYPE
	  , p_craid 		IN		 t6512_edit_check.c6512_created_by%TYPE
	  , p_referenceid	IN		 t6512_edit_check.c6512_edit_ref_id%TYPE
	  , p_recordset 	OUT 	 TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_recordset
		 FOR
			 SELECT t6512.c6512_edit_check_id edit_check_id, t6512.c6512_edit_ref_id reference_id
				  , t6512.c6512_edit_description description, t6512.c6512_edit_spec spec
				  , get_study_name(t6512.c611_study_id) study_name, t6512.c601_form_id form_id, t612.c612_study_form_ds form_ds
				  , get_user_name (t6512.c6512_created_by) created_by
				  , TO_CHAR (t6512.c6512_created_date, 'MM/DD/YYYY') created_date
				  , DECODE (t6512.c6512_active_fl, 'Y', get_code_name (93060), NULL, get_code_name (93061)) status
				  , NVL (incident.COUNT, 0) incident_count, NVL (open_incident.COUNT, 0) open_inc_count
			   FROM t6512_edit_check t6512
         , t612_study_form_list t612
				  , (SELECT   t6513.c6512_edit_check_id ID, COUNT (t6514.c6514_ec_incident_id) COUNT
						 FROM t6513_edit_check_details t6513, t6514_ec_incident t6514
						WHERE t6513.c6513_edit_check_details_id = t6514.c6513_edit_check_details_id
						  AND t6513.c6513_void_fl IS NULL
						  AND t6514.c6514_void_fl IS NULL
					 GROUP BY t6513.c6512_edit_check_id) incident
				  , (SELECT   t6513.c6512_edit_check_id ID, COUNT (t6514.c6514_ec_incident_id) COUNT
						 FROM t6513_edit_check_details t6513, t6514_ec_incident t6514
						WHERE t6513.c6513_edit_check_details_id = t6514.c6513_edit_check_details_id
						  AND t6513.c6513_void_fl IS NULL
						  AND t6514.c6514_void_fl IS NULL
						  AND t6514.c6514_status = 10
					 GROUP BY t6513.c6512_edit_check_id) open_incident
			  WHERE t6512.c6512_edit_check_id = incident.ID(+)
				AND t6512.c6512_edit_check_id = open_incident.ID(+)
				AND t6512.c6512_void_fl IS NULL
        AND t612.c601_form_id(+) = t6512.c601_form_id
        -- the following are the param from the screen
				AND t6512.c611_study_id = DECODE (p_studyid, '0', t6512.c611_study_id, p_studyid)
				AND nvl(t6512.c601_form_id,-999) = DECODE (p_formid, '0', nvl(t6512.c601_form_id,-999), p_formid)
		        AND t6512.c6512_created_by = DECODE (p_craid, '0', t6512.c6512_created_by, p_craid)
				AND NVL(t6512.c6512_active_fl,'-999') = DECODE (p_status, '93060', 'Y', '93061','-999', NVL(t6512.c6512_active_fl,'-999'))
				AND t6512.c6512_edit_ref_id = NVL (p_referenceid, t6512.c6512_edit_ref_id);       
	END gm_fch_edit_check_summary;

--
/*******************************************************
 * Purpose: This Procedure is used to make reference Id active or inactive
 *
 *******************************************************/
--
	PROCEDURE gm_sav_edit_check_summary (
		p_edit_check_id   IN   t6512_edit_check.c6512_edit_check_id%TYPE
	  , p_type			  IN   VARCHAR2
	  , p_userid		  IN   t101_user.c101_user_id%TYPE
	)
	AS
		v_active_fl    VARCHAR2 (1);
		v_incident_cnt NUMBER;
	BEGIN
		IF p_type = '92426'
		THEN
			v_active_fl := 'Y';
		ELSE
			v_active_fl := 'N';

			SELECT COUNT (1)
			  INTO v_incident_cnt
			  FROM t6512_edit_check t6512, t6513_edit_check_details t6513, t6514_ec_incident t6514
			 WHERE t6512.c6512_edit_check_id = t6513.c6512_edit_check_id
			   AND t6513.c6513_edit_check_details_id = t6514.c6513_edit_check_details_id
			   AND t6512.c6512_edit_check_id = p_edit_check_id
			   AND t6514.c6514_status = 10;   -- 10 is for open

			IF v_incident_cnt > 1
			THEN
				raise_application_error ('-20266', '');
			END IF;
		END IF;

		UPDATE t6512_edit_check
		   SET c6512_active_fl = v_active_fl
			 , c6512_last_updated_by = p_userid
			 , c6512_last_updated_date = SYSDATE
		 WHERE c6512_edit_check_id = p_edit_check_id;
	END gm_sav_edit_check_summary;

--

	--
/*******************************************************
 * Purpose: Procedure to void a reference Id
 *
 *******************************************************/
	PROCEDURE gm_sav_void_edit_check (
		p_edit_check_id   IN   t6512_edit_check.c6512_edit_check_id%TYPE
	  , p_type			  IN   VARCHAR2
	  , p_userid		  IN   t101_user.c101_user_id%TYPE
	)
	AS
		v_incident_cnt NUMBER;
	BEGIN
		SELECT COUNT (1)
		  INTO v_incident_cnt
		  FROM t6512_edit_check t6512, t6513_edit_check_details t6513, t6514_ec_incident t6514
		 WHERE t6512.c6512_edit_check_id = t6513.c6512_edit_check_id
		   AND t6513.c6513_edit_check_details_id = t6514.c6513_edit_check_details_id
		   AND t6512.c6512_edit_check_id = p_edit_check_id
		   AND t6514.c6514_status = 10;   -- 10 is for open

		IF v_incident_cnt > 1
		THEN
			raise_application_error ('-20268', '');
		END IF;

		UPDATE t6512_edit_check
		   SET c6512_void_fl = 'Y'
			 , c6512_last_updated_by = p_userid
			 , c6512_last_updated_date = SYSDATE
		 WHERE c6512_edit_check_id = p_edit_check_id;
	END gm_sav_void_edit_check;

--
/*******************************************************
 * Purpose: This Procedure is used to make incident
 *
 *******************************************************/
--
	PROCEDURE gm_sav_ec_incident (
		p_edit_check_incident_id   IN	t6514_ec_incident.c6514_ec_incident_id%TYPE
	  , p_type					   IN	VARCHAR2
	  , p_userid				   IN	t101_user.c101_user_id%TYPE
	)
	AS
		v_edit_check_details_id t6514_ec_incident.c6513_edit_check_details_id%TYPE;
		v_patient_id   t6514_ec_incident.c621_patient_id%TYPE;
		v_pat_ans_list_id t6514_ec_incident.c623_patient_ans_list_id%TYPE;
	BEGIN
		BEGIN
			SELECT t6514.c6513_edit_check_details_id, t6514.c621_patient_id, t6514.c623_patient_ans_list_id
			  INTO v_edit_check_details_id, v_patient_id, v_pat_ans_list_id
			  FROM t6514_ec_incident t6514
			 WHERE t6514.c6514_ec_incident_id = p_edit_check_incident_id AND t6514.c6514_status = 10;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				raise_application_error ('-20275', '');
		END;

		gm_pkg_cr_edit_check.gm_sav_check_and_clear (v_edit_check_details_id
												   , v_patient_id
												   , p_userid
												   , p_type
												   , p_edit_check_incident_id
												   ,v_pat_ans_list_id
													);
	END gm_sav_ec_incident;

--
	PROCEDURE gm_sav_schedule_ec (
		p_cancelreason	 IN   t907_cancel_log.c901_cancel_cd%TYPE
	  , p_user_id		 IN   t611_clinical_study.c611_last_updated_by%TYPE
	)
	AS
		v_study_id	   t611_clinical_study.c611_study_id%TYPE;
	BEGIN
		SELECT c902_code_nm_alt
		  INTO v_study_id
		  FROM t901_code_lookup
		 WHERE c901_code_id = p_cancelreason;

		UPDATE t611_clinical_study t611
		   SET c611_ec_job_scheduled_date = TRUNC (SYSDATE)
			 , c611_last_updated_by = p_user_id
			 , c611_last_updated_date = SYSDATE
		 WHERE t611.c611_study_id = v_study_id;
	END gm_sav_schedule_ec;
END gm_pkg_cr_edit_check_info;
/
