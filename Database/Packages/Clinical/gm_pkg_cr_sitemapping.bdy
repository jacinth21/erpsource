/* Formatted on 2010/05/17 18:07 (Formatter Plus v4.8.0) */
--@"C:\Database\Packages\Clinical\gm_pkg_cr_sitemapping.bdy"

CREATE OR REPLACE PACKAGE BODY gm_pkg_cr_sitemapping
IS
--
	/******************************************************************
	* Description : Procedure to get the contact info for a specific party
	****************************************************************/
	PROCEDURE gm_fch_site_mapping_summary (
		p_partyid				  IN	   t101_party.c101_party_id%TYPE
	  , p_sitemappingsummarycur   OUT	   TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_sitemappingsummarycur
		 FOR
			 SELECT t616.c616_site_party_id party_id, t101.c101_first_nm || ' ' || t101.c101_last_nm party_nm
				  , t611.c611_study_nm study_name
				  , t614.c614_site_id || '-' || get_sh_account_name (t704.c704_account_id) acc_name
				  , t616.c616_primary_fl primaryfl
			   FROM t616_site_party_mapping t616
				  , t614_study_site t614
				  , t611_clinical_study t611
				  , t704_account t704
				  , t101_party t101
			  WHERE t101.c101_party_id = p_partyid
				AND t616.c101_party_id = t101.c101_party_id
				AND t611.c611_study_id = t614.c611_study_id
				AND t704.c704_account_id = t614.c704_account_id
				AND t616.c616_void_fl IS NULL
				AND t616.c614_study_site_id = t614.c614_study_site_id
				AND t614.c614_void_fl IS NULL;
	END gm_fch_site_mapping_summary;

	PROCEDURE gm_fch_edit_mapping_info (
		p_sitepartyid	   IN		t616_site_party_mapping.c616_site_party_id%TYPE
	  , p_sitemappingcur   OUT		TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_sitemappingcur
		 FOR
			 SELECT t614.c611_study_id studylistid, t614.c704_account_id sitelistid, t616.c101_party_id partyid
				  , t101.c101_party_nm party_nm, DECODE (UPPER (t616.c616_primary_fl), 'Y', 'on', 'off') primaryflag
			   FROM t616_site_party_mapping t616, t614_study_site t614, t101_party t101
			  WHERE t616.c616_site_party_id = p_sitepartyid
				AND t616.c616_void_fl IS NULL
				AND t616.c614_study_site_id = t614.c614_study_site_id
				AND t616.c101_party_id = t101.c101_party_id
				AND t614.c614_void_fl IS NULL;
	END gm_fch_edit_mapping_info;

	PROCEDURE gm_fch_site_sc_info (
		p_studysiteid	IN		 t614_study_site.c614_study_site_id%TYPE
	  , p_sclist		OUT 	 TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_sclist
		 FOR
			 SELECT t101.c101_party_id ID, c101_first_nm || ' ' || c101_last_nm || ' ' || c101_middle_initial NAME
			   FROM t616_site_party_mapping t616, t101_party t101
			  WHERE t616.c101_party_id = t101.c101_party_id
				AND t616.c614_study_site_id = p_studysiteid
				AND t616.c616_void_fl IS NULL;
	END gm_fch_site_sc_info;

	PROCEDURE gm_fch_training_detail (
		p_trainingid	 IN 	  t635_clinical_training_record.c635_clinical_training_rcd_id%TYPE
	  , p_traininglist	 OUT	  TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_traininglist
		 FOR
			 SELECT c635_clinical_training_rcd_id trainingid, c611_study_id studyid, c614_study_site_id siteid
				  , c901_training_for trainingfor, c635_trainee_ref_id trainingname
				  , c901_training_reason trainingreason, c635_training_scope trainingscope
				  , TO_CHAR (c635_completion_date, 'MM/DD/YYYY') completiondate, c901_status status
			   FROM t635_clinical_training_record
			  WHERE t635_clinical_training_record.c635_clinical_training_rcd_id = TO_NUMBER (p_trainingid)
				AND t635_clinical_training_record.c635_void_fl IS NULL;
	END gm_fch_training_detail;

	PROCEDURE gm_sav_training_record (
		p_study_id			IN		 t614_study_site.c611_study_id%TYPE
	  , p_site_id			IN		 t614_study_site.c614_study_site_id%TYPE
	  , p_training_for		IN		 t635_clinical_training_record.c901_training_for%TYPE
	  , p_trainee_ref_id	IN		 t635_clinical_training_record.c635_trainee_ref_id%TYPE
	  , p_training_reason	IN		 t635_clinical_training_record.c901_training_reason%TYPE
	  , p_training_scope	IN		 t635_clinical_training_record.c635_training_scope%TYPE
	  , p_completion_date	IN		 VARCHAR2
	  , p_status			IN		 t635_clinical_training_record.c901_status%TYPE
	  , p_user_id			IN		 t635_clinical_training_record.c635_created_by%TYPE
	  , p_training_id		IN OUT	 t635_clinical_training_record.c635_clinical_training_rcd_id%TYPE
	)
	AS
	BEGIN
		UPDATE t635_clinical_training_record
		   SET c611_study_id = p_study_id
			 , c614_study_site_id = p_site_id
			 , c901_training_for = p_training_for
			 , c635_trainee_ref_id = p_trainee_ref_id
			 , c901_training_reason = p_training_reason
			 , c635_training_scope = p_training_scope
			 , c635_completion_date = TO_DATE (p_completion_date, 'MM/DD/YYYY')
			 , c901_status = p_status
			 , c635_created_by = p_user_id
			 , c635_created_date = SYSDATE
		 WHERE c635_clinical_training_rcd_id = p_training_id;

		IF (SQL%ROWCOUNT = 0)
		THEN
			SELECT s635_clinical_training_record.NEXTVAL
			  INTO p_training_id
			  FROM DUAL;

			INSERT INTO t635_clinical_training_record
						(c635_clinical_training_rcd_id, c611_study_id, c614_study_site_id, c901_training_for
					   , c635_trainee_ref_id, c901_training_reason, c635_training_scope
					   , c635_completion_date, c901_status, c635_created_by, c635_created_date
						)
				 VALUES (p_training_id, p_study_id, p_site_id, p_training_for
					   , p_trainee_ref_id, p_training_reason, p_training_scope
					   , TO_DATE (p_completion_date, 'MM/DD/YYYY'), p_status, p_user_id, SYSDATE
						);
		END IF;
		
		
	END gm_sav_training_record;

	PROCEDURE gm_sav_sitemapping (
		p_site_coordinator_id	IN		 t101_party.c101_party_id%TYPE
	  , p_study_id				IN		 t614_study_site.c611_study_id%TYPE
	  , p_site_id				IN		 t614_study_site.c614_site_id%TYPE
	  , p_user_id				IN		 t1012_party_other_info.c1012_created_by%TYPE
	  , p_primary_fl			IN		 t616_site_party_mapping.c616_primary_fl%TYPE
	  , p_site_party_id 		IN OUT	 t616_site_party_mapping.c616_site_party_id%TYPE
	)
	AS
		v_primary_fl   t616_site_party_mapping.c616_primary_fl%TYPE;
		v_study_site_id t614_study_site.c614_study_site_id%TYPE;
		v_site_coordinator_exists NUMBER;
		v_user_status  NUMBER;
		v_study_site_id_count NUMBER;
		v_sel_study_id t614_study_site.c611_study_id%TYPE;
		v_sel_site_id  t614_study_site.c614_site_id%TYPE;
		v_sel_primary_fl t616_site_party_mapping.c616_primary_fl%TYPE;

		CURSOR v_site_co_cur
		IS
			SELECT t614.c611_study_id study_id, t614.c614_site_id site_id, t616.c616_primary_fl primary_fl
			  FROM t616_site_party_mapping t616, t614_study_site t614
			 WHERE t616.c101_party_id = p_site_coordinator_id
			   AND t616.c614_study_site_id = t614.c614_study_site_id
			   AND t616.c616_void_fl IS NULL
			   AND t614.c614_void_fl IS NULL;
	BEGIN
		--Check if the user is inactive
		SELECT c901_user_status
		  INTO v_user_status
		  FROM t101_user
		 WHERE c101_party_id = p_site_coordinator_id;

		IF v_user_status IS NULL OR v_user_status <> 311
		THEN
			raise_application_error ('-20248', '');
		END IF;

		--Check if the site and study combination exists
		BEGIN
			SELECT	   t614.c614_study_site_id
				  INTO v_study_site_id
				  FROM t614_study_site t614
				 WHERE t614.c611_study_id = p_study_id AND t614.c704_account_id = p_site_id
					   AND t614.c614_void_fl IS NULL
			FOR UPDATE;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				raise_application_error ('-20238', '');
		END;

		IF p_site_party_id IS NULL
		THEN
			FOR v_index IN v_site_co_cur
			LOOP
				IF v_index.study_id = p_study_id
				THEN
					raise_application_error ('-20247', '');
				END IF; 
			
				/*IF v_index.site_id != p_site_id
				THEN
					raise_application_error ('-20249', '');
				END IF;*/ 
			END LOOP;
		END IF;

		
		gm_pkg_cr_sitemapping.gm_sav_site_party_mapping (p_site_coordinator_id
													   , v_study_site_id
													   , p_study_id
													   , p_site_id
													   , p_user_id
													   , p_primary_fl
													   , p_site_party_id
														);
    SELECT COUNT (1)
		  INTO v_sel_primary_fl
		  FROM t616_site_party_mapping t616
		 WHERE t616.c614_study_site_id = v_study_site_id AND t616.c616_void_fl IS NULL AND t616.c616_primary_fl = 'Y';

		IF p_primary_fl = 'Y' AND v_sel_primary_fl > 1
		THEN
			raise_application_error ('-20239', '');
		END IF; 
	END gm_sav_sitemapping;

	PROCEDURE gm_sav_site_party_mapping (
		p_site_coordinator_id	IN		 t101_party.c101_party_id%TYPE
	  , p_study_site_id 		IN		 t614_study_site.c614_study_site_id%TYPE
	  , p_study_id				IN		 t614_study_site.c611_study_id%TYPE
	  , p_site_id				IN		 t614_study_site.c614_site_id%TYPE
	  , p_user_id				IN		 t1012_party_other_info.c1012_created_by%TYPE
	  , p_primary_fl			IN		 t616_site_party_mapping.c616_primary_fl%TYPE
	  , p_site_party_id 		IN OUT	 t616_site_party_mapping.c616_site_party_id%TYPE
	)
	AS
	VCOUNT NUMBER;
	BEGIN
		  
		UPDATE t616_site_party_mapping
		   SET c614_study_site_id = p_study_site_id
			 , c101_party_id = p_site_coordinator_id
			 , c616_primary_fl = p_primary_fl
			 , c616_last_updated_by = p_user_id
			 , c616_last_updated_date = SYSDATE
		 WHERE c616_site_party_id = p_site_party_id AND c616_void_fl IS NULL;
		 

		IF (SQL%ROWCOUNT = 0)
		THEN
			SELECT s616_site_party_mapping.NEXTVAL
			  INTO p_site_party_id
			  FROM DUAL;

			INSERT INTO t616_site_party_mapping
						(c616_site_party_id, c614_study_site_id, c616_primary_fl, c101_party_id, c901_party_type
					   , c616_created_by, c616_created_date
						)
				 VALUES (p_site_party_id, p_study_site_id, p_primary_fl, p_site_coordinator_id, 7007
					   , p_user_id, SYSDATE
						);
			END IF;
		
		
		SELECT count(1)  into VCOUNT
		FROM t616_site_party_mapping t616
		   , t614_study_site t614, t611_clinical_study t611
		WHERE t616.c101_party_id = p_site_coordinator_id  
        	  AND t611.c611_study_id=p_study_id
        	  AND t611.c611_study_id = t614.c611_study_id
        	  AND t616.c614_study_site_id = t614.c614_study_site_id
			  AND t616.c616_void_fl IS NULL;
			  
      IF VCOUNT>1  ---0,1 2
		THEN		
		raise_application_error ('-20291', '');
	  END IF;
		
	END gm_sav_site_party_mapping;

	PROCEDURE gm_fch_sc_report (
		p_study_id		   IN		t614_study_site.c611_study_id%TYPE
	  , p_sitemappingcur   OUT		TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_sitemappingcur
		 FOR
			 SELECT t616.c616_site_party_id site_party_id, t101.c101_user_f_name || t101.c101_user_l_name NAME
				  , t614.c614_site_id || '-' || get_sh_account_name (t614.c704_account_id) site
				  , DECODE (t616.c616_primary_fl, 'Y', 'Yes', 'No') primary_fl, t101.c101_email_id email_id
				  , t107.c107_contact_value phone, get_code_name (t101.c901_user_status) status
				  , get_user_name (NVL (t616.c616_last_updated_by, t616.c616_created_by)) updated_by
				  , TO_CHAR (NVL (t616.c616_last_updated_date, t616.c616_created_date), 'MM/DD/YYYY') updated_date
			   FROM t616_site_party_mapping t616
				  , t614_study_site t614
				  , t101_user t101
				  , t107_contact t107
				  , t101_party t101p
			  WHERE t616.c614_study_site_id = t614.c614_study_site_id
				AND t101.c101_party_id = t616.c101_party_id
				AND t101.c101_party_id = t107.c101_party_id(+)
				AND t101.c101_party_id = t101p.c101_party_id
				AND t107.c901_mode(+) = 90450
				AND t107.c107_primary_fl(+) = 'Y'
				AND t614.c611_study_id = NVL (p_study_id, c611_study_id)
				AND t616.c616_void_fl IS NULL
				AND t614.c614_void_fl IS NULL
				AND t107.c107_void_fl IS NULL
				AND t101p.c101_void_fl IS NULL;
	END gm_fch_sc_report;
END gm_pkg_cr_sitemapping;
/
