/* Formatted on 2010/04/15 15:15 (Formatter Plus v4.8.0) */
--@"C:\Database\Packages\Clinical\gm_pkg_cr_dgraph_rpt.bdy";
CREATE OR REPLACE PACKAGE BODY gm_pkg_cr_dgraph_rpt
IS
--
/*******************************************************
 * Purpose: Package holds Clinical Research
 * Demographic Report Methods
 * Created By VPrasath
 *******************************************************/
--
/*******************************************************
 * Purpose: This Procedure is used to fetch
 * patients information
 *******************************************************/
--
	PROCEDURE gm_cr_fch_patient_info (
		p_studyid		   IN		t621_patient_master.c611_study_id%TYPE
	  , p_study_site_ids   IN		VARCHAR2
	  , p_return_cur	   OUT		TYPES.cursor_type
	)
	AS
		v_study_listid t612_study_form_list.c612_study_list_id%TYPE;
		v_study_prdid_in v621_patient_study_list.c613_study_period_id%TYPE;
		v_study_grp_id  varchar2(100);
		v_cnt		   NUMBER;
	BEGIN
-- To assign Accounts Information
		my_context.set_my_inlist_ctx (p_study_site_ids);

		SELECT COUNT (1)
		  INTO v_cnt
		  FROM v_in_list
		 WHERE token IS NOT NULL;
	BEGIN
		SELECT get_rule_value (c906_rule_value, 'STUDY_LIST_ID_GRP')
			 , get_rule_value (c906_rule_value, 'STUDY_PRD_ID_IN_GRP')
		  INTO v_study_listid
			 , v_study_prdid_in
		  FROM t906_rules
		 WHERE c906_rule_value = get_rule_value (p_studyid, 'CR_FCH_PATINFO');
		 
		 		 EXCEPTION WHEN NO_DATA_FOUND
			 THEN
			 	  v_study_listid :='';
			  	  v_study_prdid_in :='';
	END;
		IF  v_study_prdid_in IS NOT NULL
	 	THEN
		OPEN p_return_cur
		 FOR
			 SELECT   NVL (treatment, NVL (sgroup, 'ALL PATIENTS')) treatment, maleno, maleper, femaleno, femaleper
					, ageavg, (to_char(UNISTR('\00B1')) || agesd) agesd, heightavg, (to_char(UNISTR('\00B1')) || heightsd) heightsd, weightavg
					, (to_char(UNISTR('\00B1')) || weightsd) weightsd
					, DECODE (grpingid, 0, DECODE (sgroup, NULL, 2, grpingid), grpingid) groupingcount
					, NVL (sgroup, DECODE (grpingid, 0, 'XXXX')) sgroup
				 FROM (SELECT	t901.c901_code_nm treatment, SUM (male) maleno
							  , get_formatted_number (((SUM (male) / (COUNT (1))) * 100), 1) maleper
							  , SUM (female) femaleno
							  , get_formatted_number (((SUM (female) / (COUNT (1))) * 100), 1) femaleper
							  , t901.c902_code_nm_alt sgroup, t901.c901_code_nm stype
							  , get_formatted_number (AVG (age), 1) ageavg, get_formatted_number (STDDEV (age)
																								, 1) agesd
							  , get_formatted_number (AVG (height) / 12, 1) heightavg
							  , get_formatted_number (STDDEV (height), 1) heightsd
							  , get_formatted_number (AVG (weight), 1) weightavg
							  , get_formatted_number (STDDEV (weight), 1) weightsd
							  , GROUPING_ID (t901.c901_code_nm, t901.c902_code_nm_alt) grpingid
							  , NVL (t901.c901_code_seq_no, 0) sseqno
						   FROM t901_code_lookup t901
							  , (SELECT   t622.c621_patient_id, v621.c901_surgery_type
										, MAX (DECODE (c603_answer_grp_id
													--Replaced Trunc to Round Function by displaying 1 decimal place for age.
													 , 95,	ROUND (	MONTHS_BETWEEN (v621.c622_surgery_date, TO_DATE (c904_answer_ds, 'MM/DD/YYYY')) / 12, 1 )
													 ,6301, ROUND (	MONTHS_BETWEEN (v621.c622_surgery_date, TO_DATE (c904_answer_ds, 'MM/DD/YYYY')) / 12, 1 )
													 , 0
													  )
											  ) age --Based on the surgery date and date of birth patient age can be calculated for patient data screen
										, MAX (DECODE (c603_answer_grp_id
													 , 96,DECODE (c605_answer_list_id, 5, 1, 0)
													 ,6302, DECODE (c605_answer_list_id, 6301, 1, 0)
													 , 0
													  )
											  ) male
										, MAX (DECODE (c603_answer_grp_id
													 , 96, DECODE (c605_answer_list_id, 6, 1, 0)
													 ,6302, DECODE (c605_answer_list_id, 6302, 1, 6303,1, 0)
													 , 0
													  )
											  ) female
										, MAX (DECODE (c603_answer_grp_id 
														,101,gm_pkg_cr_common.get_valid_cr_number(t623.c904_answer_ds)
														,6305, gm_pkg_cr_common.get_valid_cr_number(t623.c904_answer_ds), 0)) weight
										, (  MAX (DECODE (c603_answer_grp_id, 100, gm_pkg_cr_common.get_valid_cr_number(t623.c904_answer_ds), 0)) * 12
										   + MAX (DECODE (c603_answer_grp_id
										   				, 531, gm_pkg_cr_common.get_valid_cr_number(t623.c904_answer_ds)
										   				, 6304, gm_pkg_cr_common.get_valid_cr_number(t623.c904_answer_ds), 0))
										  ) height
									 FROM t623_patient_answer_list t623
										, t622_patient_list t622
										, v621_patient_study_list v621
									WHERE t623.c603_answer_grp_id IN ( SELECT c906_rule_value  FROM t906_rules WHERE 
										  C906_RULE_GRP_ID = 'DEMOPATIENT' and C906_RULE_ID = p_studyid) 
									  AND t622.c622_patient_list_id = t623.c622_patient_list_id
									  AND v621.c612_study_list_id = v_study_listid
									  AND v621.c622_surgery_date IS NOT NULL
									  AND v621.c621_patient_id = t622.c621_patient_id
									  AND v621.c901_surgery_type IS NOT NULL
									  AND t622.c613_study_period_id = v_study_prdid_in
									  AND (   ('-999' = DECODE (v_cnt, 0, '-999', '-9999'))
										   OR v621.c614_study_site_id IN (SELECT token
																			FROM v_in_list)
										  )
									  AND v621.c611_study_id = p_studyid
									  AND t622.c622_delete_fl = 'N'
									  AND t623.c623_delete_fl = 'N'
									  AND v621.c621_surgery_comp_fl = 'Y'
								 GROUP BY t622.c621_patient_id, v621.c901_surgery_type) summ
						  WHERE t901.c901_code_id = summ.c901_surgery_type
					   GROUP BY ROLLUP (t901.c902_code_nm_alt, (t901.c901_code_seq_no, t901.c901_code_nm))
						 HAVING (	t901.c902_code_nm_alt IS NOT NULL
								 OR t901.c901_code_nm IS NOT NULL
								 OR GROUPING_ID (t901.c901_code_nm, t901.c902_code_nm_alt) <> 2
								))
			 ORDER BY sgroup, groupingcount, sseqno;
       END IF;
       -- Because of no value in rule table for Transition & Acadia Pilot study we are fixing the code for avoiding Cursor is closed error in patient data screen.
	      IF  v_study_listid IS NULL
	      THEN
	      OPEN p_return_cur FOR
	      		SELECT * FROM DUAL WHERE 1=0;
	      END IF;
     
	END gm_cr_fch_patient_info;

--
--
/*******************************************************
 * Purpose: This Procedure is used to fetch
 * the count of patients for level treated
 *******************************************************/
--
	PROCEDURE gm_cr_fch_level_treated (
		p_studyid		   IN		t621_patient_master.c611_study_id%TYPE
	  , p_study_site_ids   IN		VARCHAR2
	  , p_return_cur	   OUT		TYPES.cursor_type
	  , p_type			   IN		VARCHAR2
	)
	AS
		v_form_id			VARCHAR2(20);
        v_question_id		VARCHAR2(20);
        v_study_prd_id		VARCHAR2(20);
        v_study_grp_id		VARCHAR2(20);
        v_c34 				VARCHAR2(20);
		v_c45 				VARCHAR2(20);
		v_c56 				VARCHAR2(20);
		v_c67 				VARCHAR2(20);
		v_c78 				VARCHAR2(20);
	BEGIN
-- To assign Accounts Information
		my_context.set_my_inlist_ctx (p_study_site_ids);
		
		BEGIN
			SELECT get_rule_value('FORM_ID',c906_rule_value),
			       get_rule_value('QUEST_ID',c906_rule_value),
	               get_rule_value('STDY_PRD_ID',c906_rule_value),
	               get_rule_value('STDY_GRP_ID',c906_rule_value),
	               get_rule_value('C34', get_rule_value('QUEST_ID',c906_rule_value)),
				   get_rule_value('C45', get_rule_value('QUEST_ID',c906_rule_value)),
				   get_rule_value('C56', get_rule_value('QUEST_ID',c906_rule_value)),
				   get_rule_value('C67', get_rule_value('QUEST_ID',c906_rule_value)),
				   get_rule_value('C78', get_rule_value('QUEST_ID',c906_rule_value))
	          INTO v_form_id,
	               v_question_id,
	               v_study_prd_id,
	               v_study_grp_id,
	               v_c34,v_c45,v_c56,v_c67,v_c78		       
		      FROM t906_rules
			 WHERE c906_rule_value= get_rule_value(p_studyid,'DLVLANT'); 
			 
			 EXCEPTION WHEN NO_DATA_FOUND
			 THEN
			 	   v_form_id      :='';
	               v_question_id  :='';
	               v_study_prd_id :='';
	               v_c34          :='';
				   v_c45          :='';
				   v_c56          :='';
				   v_c67          :='';
				   v_c78          :='';
	               
		END;
		IF p_type = '60606'
		THEN
			OPEN p_return_cur
			 FOR
				 SELECT   NVL (stype, NVL (sgroup, 'ALL PATIENTS')) treatment, c34
						, get_formatted_number ((c34 / (c34 + c45 + c56 + c67)) * 100, 1) c34_per, c45
						, get_formatted_number ((c45 / (c34 + c45 + c56 + c67)) * 100, 1) c45_per, c56
						, get_formatted_number ((c56 / (c34 + c45 + c56 + c67)) * 100, 1) c56_per, c67
						, get_formatted_number ((c67 / (c34 + c45 + c56 + c67)) * 100, 1) c67_per
						, ROUND (c34 + c45 + c56 + c67) total_no
						, DECODE (grpingid, 0, DECODE (sgroup, NULL, 2, grpingid), grpingid) groupingcount
						, NVL (sgroup, DECODE (grpingid, 0, 'XXXX')) sgroup
					 FROM (SELECT	t901.c902_code_nm_alt sgroup, t901.c901_code_nm stype
								  , GROUPING_ID (t901.c901_code_nm, t901.c902_code_nm_alt) grpingid
								  , NVL (t901.c901_code_seq_no, 0) sseqno
								  , SUM (DECODE (t623.c605_answer_list_id, 200, 1, 0)) c34
								  , SUM (DECODE (t623.c605_answer_list_id, 201, 1, 0)) c45
								  , SUM (DECODE (t623.c605_answer_list_id, 202, 1, 0)) c56
								  , SUM (DECODE (t623.c605_answer_list_id, 203, 1, 0)) c67
							   FROM t623_patient_answer_list t623
								  , t622_patient_list t622
								  , t621_patient_master t621
								  , t901_code_lookup t901
								  , t614_study_site t614
							  WHERE t621.c611_study_id = t614.c611_study_id
								AND t621.c704_account_id = t614.c704_account_id
								AND t622.c613_study_period_id = 68
								AND t622.c622_patient_list_id = t623.c622_patient_list_id
								AND t623.c623_delete_fl = 'N'
								AND t622.c622_delete_fl = 'N'
								AND t621.c621_delete_fl = 'N'
								AND t621.c621_surgery_comp_fl IS NOT NULL
								AND t614.c614_void_fl IS NULL
								AND c621_surgery_date IS NOT NULL
								AND t623.c602_question_id = 1004
								AND t621.c621_patient_id = t622.c621_patient_id
								AND c901_surgery_type = t901.c901_code_id
								AND c901_surgery_type IS NOT NULL
								AND t614.c614_study_site_id IN (SELECT token
																  FROM v_in_list)
								AND t621.c611_study_id = p_studyid
						   GROUP BY ROLLUP (t901.c902_code_nm_alt, (t901.c901_code_seq_no, t901.c901_code_nm)))
					WHERE sgroup IS NOT NULL OR stype IS NOT NULL OR grpingid <> 2
				 ORDER BY sgroup, groupingcount, sseqno;
		ELSIF p_type = '60601' OR p_type = '60608' 
		THEN
		
			OPEN p_return_cur
			 FOR
				 --LEVEL TREATED LEVEL 1
				 SELECT   NVL (stype, NVL (sgroup, 'ALL PATIENTS')) treatment, c34
						, get_formatted_number ((c34 / total_no) * 100, 1) c34_per, c45
						, get_formatted_number ((c45 / total_no) * 100, 1) c45_per, c56
						, get_formatted_number ((c56 / total_no) * 100, 1) c56_per, c67
						, get_formatted_number ((c67 / total_no) * 100, 1) c67_per, c78
						, get_formatted_number ((c78 / total_no) * 100, 1) c78_per, total_no
						, DECODE (grpingid, 0, DECODE (sgroup, NULL, 2, grpingid), grpingid) groupingcount
						, NVL (sgroup, DECODE (grpingid, 0, 'XXXX')) sgroup
					 FROM (SELECT	t901.c902_code_nm_alt sgroup, t901.c901_code_nm stype
								  , GROUPING_ID (t901.c901_code_nm, t901.c902_code_nm_alt) grpingid
								  , NVL (t901.c901_code_seq_no, 0) sseqno
								  , SUM (CASE
											 WHEN t623.c603_answer_grp_id = v_c34  AND t623.c904_answer_ds = 'checked'
												 THEN 1
											 ELSE 0
										 END
										) c34
								  , SUM (CASE
											 WHEN t623.c603_answer_grp_id = v_c45 AND t623.c904_answer_ds = 'checked'
												 THEN 1
											 ELSE 0
										 END
										) c45
								  , SUM (CASE
											 WHEN t623.c603_answer_grp_id = v_c56 AND t623.c904_answer_ds = 'checked'
												 THEN 1
											 ELSE 0
										 END
										) c56
								  , SUM (CASE
											 WHEN t623.c603_answer_grp_id = v_c67 AND t623.c904_answer_ds = 'checked'
												 THEN 1
											 ELSE 0
										 END
										) c67
								  , SUM (CASE
											 WHEN t623.c603_answer_grp_id = v_c78 AND t623.c904_answer_ds = 'checked'
												 THEN 1
											 ELSE 0
										 END
										) c78
								  , SUM (1) / 5 total_no
							   FROM t623_patient_answer_list t623
								  , t622_patient_list t622
								  , t621_patient_master t621
								  , t901_code_lookup t901
								  , t614_study_site t614
							  WHERE t621.c611_study_id = t614.c611_study_id
								AND t621.c704_account_id = t614.c704_account_id
								AND t622.c613_study_period_id = v_study_prd_id
								AND t622.c622_patient_list_id = t623.c622_patient_list_id
								AND t623.c623_delete_fl = 'N'
								AND t622.c622_delete_fl = 'N'
								AND t621.c621_delete_fl = 'N'
								AND t621.c621_surgery_comp_fl IS NOT NULL
								AND t614.c614_void_fl IS NULL
								AND c621_surgery_date IS NOT NULL
								AND t623.c601_form_id = v_form_id
								AND t623.c602_question_id = v_question_id
								AND t621.c621_patient_id = t622.c621_patient_id
								AND c901_surgery_type = t901.c901_code_id
								AND c901_surgery_type IS NOT NULL
								AND t614.c614_study_site_id IN (SELECT token
																  FROM v_in_list)
								AND t621.c611_study_id = p_studyid
								AND t623.c621_patient_id IN (
										SELECT DISTINCT t623.c621_patient_id
												   FROM t603_answer_grp t603, t623_patient_answer_list t623
												  WHERE t623.c603_answer_grp_id = t603.c603_answer_grp_id
													AND t603.c603_answer_grp_id IN (v_c34,v_c45,v_c56,v_c67,v_c78)
													AND t623.c904_answer_ds = 'checked'
											   GROUP BY t623.c621_patient_id
												 HAVING COUNT (t623.c621_patient_id) = 1)
						   GROUP BY ROLLUP (t901.c902_code_nm_alt, (t901.c901_code_seq_no, t901.c901_code_nm)))
					WHERE sgroup IS NOT NULL OR stype IS NOT NULL OR grpingid <> 2
				 ORDER BY sgroup, groupingcount, sseqno;
		ELSIF p_type = '60602'
		THEN
			OPEN p_return_cur
			 FOR
				 --LEVEL TREATED LEVEL 2
				 SELECT   NVL (stype, NVL (sgroup, 'ALL PATIENTS')) treatment, c23
						, get_formatted_number ((c23 / total_no) * 100, 1) c23_per, c34
						, get_formatted_number ((c34 / total_no) * 100, 1) c34_per, c45
						, get_formatted_number ((c45 / total_no) * 100, 1) c45_per, c56
						, get_formatted_number ((c56 / total_no) * 100, 1) c56_per, total_no
						, DECODE (grpingid, 0, DECODE (sgroup, NULL, 2, grpingid), grpingid) groupingcount
						, NVL (sgroup, DECODE (grpingid, 0, 'XXXX')) sgroup
					 FROM (SELECT	t901.c902_code_nm_alt sgroup, t901.c901_code_nm stype
								  , GROUPING_ID (t901.c901_code_nm, t901.c902_code_nm_alt) grpingid
								  , NVL (t901.c901_code_seq_no, 0) sseqno
								  ,   SUM (CASE
											   WHEN val1 = 1 AND val2 = 1 AND val3 = 0 AND val4 = 0 AND val5 = 0
												   THEN 1
											   ELSE 0
										   END
										  )
									/ 5 AS c23
								  ,   SUM (CASE
											   WHEN val1 = 0 AND val2 = 1 AND val3 = 1 AND val4 = 0 AND val5 = 0
												   THEN 1
											   ELSE 0
										   END
										  )
									/ 5 AS c34
								  ,   SUM (CASE
											   WHEN val1 = 0 AND val2 = 0 AND val3 = 1 AND val4 = 1 AND val5 = 0
												   THEN 1
											   ELSE 0
										   END
										  )
									/ 5 AS c45
								  ,   SUM (CASE
											   WHEN val1 = 0 AND val2 = 0 AND val3 = 0 AND val4 = 1 AND val5 = 1
												   THEN 1
											   ELSE 0
										   END
										  )
									/ 5 AS c56
								  , SUM (1) / 5 total_no
							   FROM t623_patient_answer_list t623
								  , t622_patient_list t622
								  , t621_patient_master t621
								  , t901_code_lookup t901
								  , t614_study_site t614
								  , (SELECT DISTINCT t1.c621_patient_id, val1, val2, val3, val4, val5
												FROM (SELECT t623.c621_patient_id
														   , DECODE (t623.c904_answer_ds, 'checked', 1, 0) val1
														FROM t603_answer_grp t603, t623_patient_answer_list t623
													   WHERE t623.c603_answer_grp_id = t603.c603_answer_grp_id
														 AND t603.c603_answer_grp_id IN (712)) t1
												   , (SELECT t623.c621_patient_id
														   , DECODE (t623.c904_answer_ds, 'checked', 1, 0) val2
														FROM t603_answer_grp t603, t623_patient_answer_list t623
													   WHERE t623.c603_answer_grp_id = t603.c603_answer_grp_id
														 AND t603.c603_answer_grp_id IN (713)) t2
												   , (SELECT t623.c621_patient_id
														   , DECODE (t623.c904_answer_ds, 'checked', 1, 0) val3
														FROM t603_answer_grp t603, t623_patient_answer_list t623
													   WHERE t623.c603_answer_grp_id = t603.c603_answer_grp_id
														 AND t603.c603_answer_grp_id IN (714)) t3
												   , (SELECT t623.c621_patient_id
														   , DECODE (t623.c904_answer_ds, 'checked', 1, 0) val4
														FROM t603_answer_grp t603, t623_patient_answer_list t623
													   WHERE t623.c603_answer_grp_id = t603.c603_answer_grp_id
														 AND t603.c603_answer_grp_id IN (715)) t4
												   , (SELECT t623.c621_patient_id
														   , DECODE (t623.c904_answer_ds, 'checked', 1, 0) val5
														FROM t603_answer_grp t603, t623_patient_answer_list t623
													   WHERE t623.c603_answer_grp_id = t603.c603_answer_grp_id
														 AND t603.c603_answer_grp_id IN (749)) t5
											   WHERE t1.c621_patient_id = t2.c621_patient_id
												 AND t2.c621_patient_id = t3.c621_patient_id
												 AND t3.c621_patient_id = t4.c621_patient_id
												 AND t4.c621_patient_id = t5.c621_patient_id
												 AND 1 =
														 CASE
															 WHEN (   (val1 = 1 AND val2 = 1)
																   OR (val2 = 1 AND val3 = 1)
																   OR (val3 = 1 AND val4 = 1)
																   OR (val4 = 1 AND val5 = 1)
																  )
															 AND (val1 + val2 + val3 + val4 + val5 = 2)
																 THEN 1
														 END) t1
							  WHERE t621.c611_study_id = t614.c611_study_id
								AND t621.c704_account_id = t614.c704_account_id
								AND t622.c613_study_period_id = 179
								AND t622.c622_patient_list_id = t623.c622_patient_list_id
								AND t623.c623_delete_fl = 'N'
								AND t622.c622_delete_fl = 'N'
								AND t621.c621_delete_fl = 'N'
								AND t621.c621_surgery_comp_fl IS NOT NULL
								AND t614.c614_void_fl IS NULL
								AND c621_surgery_date IS NOT NULL
								AND t623.c601_form_id = 211
								AND t623.c602_question_id = 21104
								AND t621.c621_patient_id = t622.c621_patient_id
								AND c901_surgery_type = t901.c901_code_id
								AND c901_surgery_type IS NOT NULL
								AND t614.c614_study_site_id IN (SELECT token
																  FROM v_in_list)
								AND t621.c611_study_id = p_studyid
								AND t623.c621_patient_id = t1.c621_patient_id
						   GROUP BY ROLLUP (t901.c902_code_nm_alt, (t901.c901_code_seq_no, t901.c901_code_nm)))
					WHERE sgroup IS NOT NULL OR stype IS NOT NULL OR grpingid <> 2
				 ORDER BY sgroup, groupingcount, sseqno;
				 
		ELSIF p_type = '61608'
		THEN
			OPEN p_return_cur
			 FOR	 
				 
				 SELECT   NVL (stype, NVL (sgroup, 'ALL PATIENTS')) treatment, c56
						, get_formatted_number ((c56 / total_no) * 100, 1) c56_per, c67
						, get_formatted_number ((c67 / total_no) * 100, 1) c67_per, c78
						, get_formatted_number ((c78 / total_no) * 100, 1) c78_per, total_no
						, DECODE (grpingid, 0, DECODE (sgroup, NULL, 2, grpingid), grpingid) groupingcount
						, NVL (sgroup, DECODE (grpingid, 0, 'XXXX')) sgroup
					 FROM (SELECT	t901.c902_code_nm_alt sgroup, t901.c901_code_nm stype
								  , GROUPING_ID (t901.c901_code_nm, t901.c902_code_nm_alt) grpingid
								  , NVL (t901.c901_code_seq_no, 0) sseqno
								  
                                 , SUM (CASE
											 WHEN t623.c603_answer_grp_id = 7007 AND t623.c605_answer_list_id = v_c56
												 THEN 1
											 ELSE 0
										 END
										) c56
								  , SUM (CASE
											 WHEN t623.c603_answer_grp_id = 7007 AND t623.c605_answer_list_id = v_c67
												 THEN 1
											 ELSE 0
										 END
										) c67
								  , SUM (CASE
											 WHEN t623.c603_answer_grp_id = 7007 AND t623.c605_answer_list_id =  v_c78
												 THEN 1
											 ELSE 0
										 END
										) c78
								  , SUM (1)  total_no
							   FROM t623_patient_answer_list t623
								  , t622_patient_list t622
								  , t621_patient_master t621
								  , t901_code_lookup t901
								  , t614_study_site t614
							  WHERE t621.c611_study_id = t614.c611_study_id
								AND t621.c704_account_id = t614.c704_account_id
								AND t622.c613_study_period_id = v_study_prd_id
								AND t622.c622_patient_list_id = t623.c622_patient_list_id
								AND t623.c623_delete_fl = 'N'
								AND t622.c622_delete_fl = 'N'
								AND t621.c621_delete_fl = 'N'
								AND t621.c621_surgery_comp_fl IS NOT NULL
								AND t614.c614_void_fl IS NULL
								AND c621_surgery_date IS NOT NULL
								AND t623.c601_form_id = v_form_id
								AND t623.c602_question_id = v_question_id
								AND t621.c621_patient_id = t622.c621_patient_id
								AND c901_surgery_type = t901.c901_code_id
								AND c901_surgery_type IS NOT NULL
								AND t614.c614_study_site_id IN (SELECT token
																  FROM v_in_list)
								AND t621.c611_study_id = p_studyid  
								AND t623.c621_patient_id IN (
										SELECT DISTINCT t623.c621_patient_id
												   FROM t603_answer_grp t603, t623_patient_answer_list t623
												  WHERE t623.c603_answer_grp_id = t603.c603_answer_grp_id
													AND t603.c603_answer_grp_id IN (v_study_grp_id)
													AND (t623.c605_answer_list_id <> 0   and t623.c605_answer_list_id is not null)
											   GROUP BY t623.c621_patient_id
												 HAVING COUNT (t623.c621_patient_id) = 1)
						   GROUP BY ROLLUP (t901.c902_code_nm_alt, (t901.c901_code_seq_no, t901.c901_code_nm)))
					WHERE sgroup IS NOT NULL OR stype IS NOT NULL OR grpingid <> 2
				 ORDER BY sgroup, groupingcount, sseqno;
		END IF;
	END gm_cr_fch_level_treated;

--
--
/*******************************************************
 * Purpose: This Procedure is used to fetch
 * surgical results for the given study and sites
 *******************************************************/
--
	PROCEDURE gm_cr_fch_surgical_result (
		p_studyid		   IN		t621_patient_master.c611_study_id%TYPE
	  , p_study_site_ids   IN		VARCHAR2
	  , p_return_cur	   OUT		TYPES.cursor_type
	)
	AS
		v_form_id	   t622_patient_list.c601_form_id%TYPE;
		v_question_id  t901_code_lookup.c901_code_nm%TYPE;
		v_cnt		   NUMBER;
		v_ans_hrs	   NUMBER;
		v_ans_min	   NUMBER;
		v_ans_mls	   NUMBER;
	BEGIN
-- To assign Accounts Information
		my_context.set_my_inlist_ctx (p_study_site_ids);

		SELECT COUNT (1)
		  INTO v_cnt
		  FROM v_in_list
		 WHERE token IS NOT NULL;
		BEGIN
			SELECT t901.c902_code_nm_alt, t901.c901_code_nm, 
			       get_rule_value ('ANS_HRS', t901.c901_code_grp ),
			       get_rule_value ('ANS_MIN', t901.c901_code_grp ),
			       get_rule_value ('ANS_MLS', t901.c901_code_grp )
			  INTO v_form_id, v_question_id, v_ans_hrs, v_ans_min, v_ans_mls
			  FROM t901_code_lookup t901
			 WHERE t901.c901_code_grp = get_rule_value (p_studyid, 'CR_FCH_SURGRES')
			   AND t901.c901_void_fl IS NULL;
			
			 EXCEPTION
					WHEN NO_DATA_FOUND
					THEN
						v_form_id 	  :=0;
						v_question_id :=0;
						v_ans_hrs     :=0;
						v_ans_min     :=0;
						v_ans_mls     :=0; 
		END;
		OPEN p_return_cur
		 FOR
			 SELECT DISTINCT NVL (stype, NVL (sgroup, 'ALL PATIENTS')) treatment, avgopt, (to_char(UNISTR('\00B1')) || sdopt) sdopt
						   , avgbls, (to_char(UNISTR('\00B1')) || sdbls) sdbls
						   , DECODE (grpingid, 0, DECODE (sgroup, NULL, 2, grpingid), grpingid) groupingcount, sseqno
						   , NVL (sgroup, DECODE (grpingid, 0, 'XXXX')) sgroup
						FROM (SELECT   t901.c902_code_nm_alt sgroup, t901.c901_code_nm stype
									 , t901.c901_code_nm treatment, get_formatted_number (AVG (opmin), 1) avgopt
									 , get_formatted_number (STDDEV (opmin), 1) sdopt
									 , get_formatted_number (AVG (bloodloss), 1) avgbls
									 , get_formatted_number (STDDEV (bloodloss), 1) sdbls
									 , GROUPING_ID (t901.c901_code_nm, t901.c902_code_nm_alt) grpingid
									 , NVL (t901.c901_code_seq_no, 0) sseqno
								  FROM t901_code_lookup t901
									 , (SELECT	 v621.c621_patient_ide_no, t622.c621_patient_id, v621.c901_surgery_type
											   ,	 MAX (DECODE (c603_answer_grp_id, v_ans_hrs, gm_pkg_cr_common.get_valid_cr_number(t623.c904_answer_ds), 0)
														 )
												   * 60
												 + MAX (DECODE (c603_answer_grp_id, v_ans_min, gm_pkg_cr_common.get_valid_cr_number(t623.c904_answer_ds), 0)) opmin
											   , MAX (DECODE (c603_answer_grp_id, v_ans_mls, gm_pkg_cr_common.get_valid_cr_number(t623.c904_answer_ds), 0)
													 ) bloodloss
											FROM t623_patient_answer_list t623
											   , t622_patient_list t622
											   , v621_patient_study_list v621
										   WHERE t623.c603_answer_grp_id IN (v_ans_hrs, v_ans_min, v_ans_mls)
											 AND t622.c601_form_id = v_form_id
											 AND t622.c622_patient_list_id = t623.c622_patient_list_id
											 AND v621.c601_form_id = t622.c601_form_id
											 AND v621.c622_surgery_date IS NOT NULL
											 AND v621.c621_patient_id = t622.c621_patient_id
											 AND v621.c901_surgery_type IS NOT NULL
											 AND (	 ('-999' = DECODE (v_cnt, 0, '-999', '-9999'))
												  OR v621.c614_study_site_id IN (SELECT token
																				   FROM v_in_list)
												 )
											 AND v621.c611_study_id = p_studyid
											 AND t622.c622_delete_fl = 'N'
											 AND t623.c623_delete_fl = 'N'
											 AND v621.c621_surgery_comp_fl = 'Y'
										GROUP BY v621.c621_patient_ide_no, t622.c621_patient_id, v621.c901_surgery_type) summ
								 WHERE t901.c901_code_id = summ.c901_surgery_type
							  GROUP BY ROLLUP (t901.c902_code_nm_alt, (t901.c901_code_seq_no, t901.c901_code_nm))
								HAVING (   t901.c902_code_nm_alt IS NOT NULL
										OR t901.c901_code_nm IS NOT NULL
										OR GROUPING_ID (t901.c901_code_nm, t901.c902_code_nm_alt) <> 2
									   ))
					ORDER BY sgroup, groupingcount, sseqno;
	END gm_cr_fch_surgical_result;

--
/*******************************************************
 * Purpose: This Procedure is used to fetch
 * diagnosis for the given study and sites
 *******************************************************/
--
	PROCEDURE gm_cr_fch_diagnosis_info (
		p_studyid		IN		 t611_clinical_study.c611_study_id%TYPE
	  , p_studysiteid	IN		 VARCHAR2
	  , p_out_header	OUT 	 TYPES.cursor_type
	  , p_out_details	OUT 	 TYPES.cursor_type
	)
	AS
		v_cnt		   NUMBER;
		v_rule_grp     VARCHAR2(20);
		v_question_id  VARCHAR2(20);
		v_study_prd_id VARCHAR2(20);
		v_ans_grp_a    VARCHAR2(20);
	    v_ans_grp_b    VARCHAR2(20);
		v_ans_grp_c    VARCHAR2(20);
		v_ans_grp_d    VARCHAR2(20);
		v_ans_grp_e    VARCHAR2(20);

	BEGIN
		-- The input String in the form of 101.210,101.211 will be set into this temp view.
		my_context.set_my_inlist_ctx (p_studysiteid);

		SELECT COUNT (1)
		  INTO v_cnt
		  FROM v_in_list
		 WHERE token IS NOT NULL;
		
		 BEGIN
			 SELECT GET_RULE_VALUE(p_studyid,'DMDIAGNS')
			   INTO v_rule_grp
			   FROM DUAL;
			   
			 SELECT GET_RULE_VALUE('QUST_ID',v_rule_grp),
			        GET_RULE_VALUE('STUDY_PRD_ID',v_rule_grp),
			        GET_RULE_VALUE('ANS_GRP_A',v_rule_grp),
			        GET_RULE_VALUE('ANS_GRP_B',v_rule_grp),
			        GET_RULE_VALUE('ANS_GRP_C',v_rule_grp),
			        GET_RULE_VALUE('ANS_GRP_D',v_rule_grp),
			        nvl(GET_RULE_VALUE('ANS_GRP_E',v_rule_grp),'0')
			   INTO v_question_id,
			        v_study_prd_id,
			        v_ans_grp_a,
			        v_ans_grp_b,
			        v_ans_grp_c,
			        v_ans_grp_d,
			        v_ans_grp_e
			   FROM DUAL;
			   
			   EXCEPTION WHEN NO_DATA_FOUND
			   THEN
			   	v_rule_grp     :='';
				v_question_id  :='';
				v_study_prd_id :='';
				v_ans_grp_a    :='';
			    v_ans_grp_b    :='';
				v_ans_grp_c    :='';
				v_ans_grp_d    :='';
				v_ans_grp_e    :='';
		END;
		 
		OPEN p_out_header
		 FOR
			 SELECT DISTINCT	MAX (DECODE (dgval, 1, dg, NULL))
							 || MAX (DECODE (dgval, 2, dg, NULL))
							 || MAX (DECODE (dgval, 3, dg, NULL))
							 || MAX (DECODE (dgval, 4, dg, NULL))
							 || MAX (DECODE (dgval, 5, dg, NULL))dg
						FROM (SELECT t622.c621_patient_id pid, t621.c901_surgery_type surtype
								   , DECODE (t623.c603_answer_grp_id, v_ans_grp_a, 'a', v_ans_grp_b, 'b', v_ans_grp_c, 'c', v_ans_grp_d, 'd',v_ans_grp_e, 'e') dg
								   , DECODE (t623.c603_answer_grp_id, v_ans_grp_a, '1', v_ans_grp_b, '2', v_ans_grp_c , '3', v_ans_grp_d, '4',v_ans_grp_e, '5') dgval
								FROM t623_patient_answer_list t623
								   , t622_patient_list t622
								   , t621_patient_master t621
								   , t614_study_site t614
							   WHERE t621.c611_study_id = t614.c611_study_id
								 AND t621.c704_account_id = t614.c704_account_id
								 AND t622.c613_study_period_id = v_study_prd_id
								 AND t622.c622_patient_list_id = t623.c622_patient_list_id
								 AND t623.c623_delete_fl = 'N'
								 AND t622.c622_delete_fl = 'N'
								 AND t621.c621_delete_fl = 'N'
								 AND t621.c621_surgery_comp_fl IS NOT NULL
								 AND t614.c614_void_fl IS NULL
								 AND t623.c602_question_id = v_question_id
								 AND t621.c621_patient_id = t622.c621_patient_id
								 AND t623.c603_answer_grp_id IN (v_ans_grp_a ,v_ans_grp_b ,v_ans_grp_c ,v_ans_grp_d ,v_ans_grp_e)
								 AND t623.c904_answer_ds IS NOT NULL
								 AND c901_surgery_type IS NOT NULL
								 AND (	 ('-999' = DECODE (v_cnt, 0, '-999', '-9999'))
									  OR t614.c614_study_site_id IN (SELECT token
																	   FROM v_in_list)
									 )
								 AND t621.c611_study_id = p_studyid)
					GROUP BY pid, surtype
					ORDER BY LENGTH (dg), dg;

		OPEN p_out_details
		 FOR
			 SELECT   'A'||ROWNUM ID, NVL (stype, NVL (sgroup, 'ALL PATIENTS')) NAME, dg, cnt
					, DECODE (grpingid, 0, DECODE (sgroup, NULL, 2, grpingid), grpingid) groupingcount, sseqno
					, NVL (sgroup, DECODE (grpingid, 0, 'XXXX')) sgroup
				 FROM (SELECT	t901.c902_code_nm_alt sgroup, t901.c901_code_nm stype, dg, COUNT (1) cnt
							  , GROUPING_ID (t901.c901_code_nm, t901.c902_code_nm_alt) grpingid
							  , NVL (t901.c901_code_seq_no, 0) sseqno
						   FROM (SELECT   pid, surtype
										,	 MAX (DECODE (dgval, 1, dg, NULL))
										  || MAX (DECODE (dgval, 2, dg, NULL))
										  || MAX (DECODE (dgval, 3, dg, NULL))
										  || MAX (DECODE (dgval, 4, dg, NULL))
										  || MAX (DECODE (dgval, 5, dg, NULL))dg
									 FROM (SELECT t622.c621_patient_id pid, t621.c901_surgery_type surtype
												, DECODE (t623.c603_answer_grp_id
														, v_ans_grp_a, 'a'
														, v_ans_grp_b, 'b'
														, v_ans_grp_c, 'c'
														, v_ans_grp_d, 'd'
														, v_ans_grp_e, 'e'
														 ) dg
												, DECODE (t623.c603_answer_grp_id
														, v_ans_grp_a, '1'
														, v_ans_grp_b, '2'
														, v_ans_grp_c, '3'
														, v_ans_grp_d, '4'
														, v_ans_grp_e, '5'
														 ) dgval
											 FROM t623_patient_answer_list t623
												, t622_patient_list t622
												, t621_patient_master t621
												, t614_study_site t614
											WHERE t621.c611_study_id = t614.c611_study_id
											  AND t621.c704_account_id = t614.c704_account_id
											  AND t622.c613_study_period_id = v_study_prd_id
											  AND t622.c622_patient_list_id = t623.c622_patient_list_id
											  AND t623.c623_delete_fl = 'N'
											  AND t622.c622_delete_fl = 'N'
											  AND t621.c621_delete_fl = 'N'
											  AND t621.c621_surgery_comp_fl IS NOT NULL
											  AND t614.c614_void_fl IS NULL
											  AND t623.c602_question_id = v_question_id
											  AND t621.c621_patient_id = t622.c621_patient_id
											  AND t623.c603_answer_grp_id IN (v_ans_grp_a ,v_ans_grp_b ,v_ans_grp_c ,v_ans_grp_d ,v_ans_grp_e)
											  AND t623.c904_answer_ds IS NOT NULL
											  AND c901_surgery_type IS NOT NULL
											  AND (   ('-999' = DECODE (v_cnt, 0, '-999', '-9999'))
												   OR t614.c614_study_site_id IN (SELECT token
																					FROM v_in_list)
												  )
											  AND t621.c611_study_id = p_studyid)
								 GROUP BY pid, surtype)
							  , t901_code_lookup t901
						  WHERE surtype = t901.c901_code_id
					   GROUP BY dg, ROLLUP (t901.c902_code_nm_alt, (t901.c901_code_seq_no, t901.c901_code_nm))
					   ORDER BY dg)
				WHERE sgroup IS NOT NULL OR stype IS NOT NULL OR grpingid <> 2
			 ORDER BY sgroup, groupingcount, sseqno;
	END gm_cr_fch_diagnosis_info;

--
--
--
/*******************************************************
 * Purpose: This Procedure is used to fetch
 * implant info the given study, sites and part type
 *******************************************************/
-- Need to work on this SQL on monday ( 09 /31/ 07)
	PROCEDURE gm_cr_fch_implant_info (
		p_studyid		   IN		t621_patient_master.c611_study_id%TYPE
	  , p_study_site_ids   IN		VARCHAR2
	  , p_type			   IN		VARCHAR2
	  , p_return_cur	   OUT		TYPES.cursor_type
	)
	AS
		v_question_id  NUMBER;
		v_ans_grp_id   NUMBER;
		v_form_id      NUMBER;
	BEGIN
		-- The input String in the form of 101.210, 101.211 will be set into this temp view.
		my_context.set_my_inlist_ctx (p_study_site_ids);

		-- Default Assignments for SEC-C Footprint and SEC-C SHAPE
		IF p_type = '60256'
		THEN   -- ACDF - Allograft Type
			OPEN p_return_cur
			 FOR
				 SELECT pinfo, NO, ROUND (((NO / totcount) * 100), 1) per
				   FROM (SELECT   DECODE (INSTR (UPPER (c904_answer_ds), 'MAINTAIN', 1, 1)
										, 0, DECODE (INSTR (UPPER (c904_answer_ds), 'ALLOFUSE', 1, 1)
												   , 0, 'Others'
												   , 'Allofuse'
													)
										, 'Maintain'
										 ) pinfo
								, SUM (1) NO
							 FROM t623_patient_answer_list t623
								, t622_patient_list t622
								, t621_patient_master t621
								, t614_study_site t614
							WHERE t622.c613_study_period_id = 68
							  AND t623.c621_patient_id = t621.c621_patient_id
							  AND t622.c622_patient_list_id = t623.c622_patient_list_id
							  AND t621.c611_study_id = t614.c611_study_id
							  AND t621.c704_account_id = t614.c704_account_id
							  AND t623.c623_delete_fl = 'N'
							  AND t622.c622_delete_fl = 'N'
							  AND t621.c621_delete_fl = 'N'
							  AND t621.c621_surgery_comp_fl IS NOT NULL
							  AND t614.c614_void_fl IS NULL
							  AND c904_answer_ds IS NOT NULL
							  AND t623.c602_question_id IN (1011)
							  AND t623.c603_answer_grp_id IN (423)
							  AND t614.c614_study_site_id IN (SELECT token
																FROM v_in_list)
							  AND t621.c611_study_id = p_studyid
						 GROUP BY DECODE (INSTR (UPPER (c904_answer_ds), 'MAINTAIN', 1, 1)
										, 0, DECODE (INSTR (UPPER (c904_answer_ds), 'ALLOFUSE', 1, 1)
												   , 0, 'Others'
												   , 'Allofuse'
													)
										, 'Maintain'
										 ))
					  , (SELECT SUM (1) totcount
						   FROM t623_patient_answer_list t623
							  , t622_patient_list t622
							  , t621_patient_master t621
							  , t614_study_site t614
						  WHERE t622.c613_study_period_id = 68
							AND t623.c621_patient_id = t621.c621_patient_id
							AND t622.c622_patient_list_id = t623.c622_patient_list_id
							AND t621.c611_study_id = t614.c611_study_id
							AND t621.c704_account_id = t614.c704_account_id
							AND t623.c623_delete_fl = 'N'
							AND t622.c622_delete_fl = 'N'
							AND t621.c621_delete_fl = 'N'
							AND t621.c621_surgery_comp_fl IS NOT NULL
							AND t614.c614_void_fl IS NULL
							AND c904_answer_ds IS NOT NULL
							AND t623.c602_question_id IN (1011)
							AND t623.c603_answer_grp_id IN (423)
							AND t614.c614_study_site_id IN (SELECT token
															  FROM v_in_list)
							AND t621.c611_study_id = p_studyid);
		ELSIF p_type = '60611'
		THEN
			OPEN p_return_cur
			 FOR
				 SELECT   pinfo, NO, ROUND (((NO / totcount) * 100), 1) per
						, DECODE (pinfo, 'Self-Distracting', 1, 'Cylindrical', 2) ord
					 FROM (SELECT	CASE
										WHEN (NVL (TRIM (t623.c904_answer_ds), 0) <> 0)
										AND (	SUBSTR (TRIM (t623.c904_answer_ds), 1, 5) = '327.1'
											 OR SUBSTR (TRIM (t623.c904_answer_ds), 1, 5) = '327.2'
											 OR SUBSTR (TRIM (t623.c904_answer_ds), 1, 5) = '327.3'
											)
											THEN 'Self-Distracting'
										WHEN (NVL (TRIM (t623.c904_answer_ds), 0) <> 0)
										AND (	SUBSTR (TRIM (t623.c904_answer_ds), 1, 5) = '327.4'
											 OR SUBSTR (TRIM (t623.c904_answer_ds), 1, 5) = '327.5'
											 OR SUBSTR (TRIM (t623.c904_answer_ds), 1, 5) = '327.6'
											)
											THEN 'Cylindrical'
									END pinfo
								  , SUM (1) NO
							   FROM t623_patient_answer_list t623, t621_patient_master t621, t614_study_site t614
							  WHERE t623.c621_patient_id = t621.c621_patient_id
								AND t621.c611_study_id = t614.c611_study_id
								AND t621.c704_account_id = t614.c704_account_id
								AND t623.c623_delete_fl = 'N'
								AND t621.c621_delete_fl = 'N'
								AND t621.c621_surgery_comp_fl IS NOT NULL
								AND t614.c614_void_fl IS NULL
								AND c904_answer_ds IS NOT NULL
								AND t623.c602_question_id IN (21109)
								AND t623.c603_answer_grp_id IN (717, 719)
								AND t623.c603_answer_grp_id IS NOT NULL
								AND t614.c614_study_site_id IN (SELECT token
																  FROM v_in_list)
						   GROUP BY CASE
										WHEN (NVL (TRIM (t623.c904_answer_ds), 0) <> 0)
										AND (	SUBSTR (TRIM (t623.c904_answer_ds), 1, 5) = '327.1'
											 OR SUBSTR (TRIM (t623.c904_answer_ds), 1, 5) = '327.2'
											 OR SUBSTR (TRIM (t623.c904_answer_ds), 1, 5) = '327.3'
											)
											THEN 'Self-Distracting'
										WHEN (NVL (TRIM (t623.c904_answer_ds), 0) <> 0)
										AND (	SUBSTR (TRIM (t623.c904_answer_ds), 1, 5) = '327.4'
											 OR SUBSTR (TRIM (t623.c904_answer_ds), 1, 5) = '327.5'
											 OR SUBSTR (TRIM (t623.c904_answer_ds), 1, 5) = '327.6'
											)
											THEN 'Cylindrical'
									END)
						, (SELECT COUNT (t623.c621_patient_id) totcount
							 FROM t623_patient_answer_list t623, t621_patient_master t621, t614_study_site t614
							WHERE t623.c621_patient_id = t621.c621_patient_id
							  AND t621.c611_study_id = t614.c611_study_id
							  AND t621.c704_account_id = t614.c704_account_id
							  AND t623.c623_delete_fl = 'N'
							  AND t621.c621_delete_fl = 'N'
							  AND t621.c621_surgery_comp_fl IS NOT NULL
							  AND t614.c614_void_fl IS NULL
							  AND c904_answer_ds IS NOT NULL
							  AND t623.c602_question_id IN (21109)
							  AND t623.c603_answer_grp_id IN (717, 719)
							  AND t614.c614_study_site_id IN (SELECT token
																FROM v_in_list)
							  AND t621.c611_study_id = p_studyid)
					WHERE pinfo IS NOT NULL
				 ORDER BY 4;
		ELSE
			v_question_id := 1008;
			v_ans_grp_id := 418;
			v_form_id := 10;

			-- SEC-C Core Height
			IF p_type = '60255'
			THEN
				v_question_id := 1009;
				v_ans_grp_id := 420;
			ELSIF p_type IN ('60931','60933') --this condition for Triumph report
			THEN
				BEGIN
					/*We get the formID, question ID and, ans_grp ID from lookup and rule tables.*/
					SELECT TO_NUMBER(c902_code_nm_alt),
						   TO_NUMBER(c901_code_nm),
						   TO_NUMBER(get_rule_value(c901_code_nm,'DIUT'))
					  INTO v_form_id,v_question_id,v_ans_grp_id
					  FROM t901_code_lookup 
					 WHERE c901_code_id IN (SELECT c902_code_nm_alt 
					   							FROM t901_code_lookup 
					   						   WHERE c901_code_id=p_type
					   						     AND c901_void_fl IS NULL)
					   AND c901_void_fl IS NULL;
					   						  
					EXCEPTION WHEN NO_DATA_FOUND
			   		THEN
						v_form_id 	  :=0;
						v_question_id :=0;
						v_ans_grp_id  :=0;
				END;
			END IF;

			OPEN p_return_cur
			 FOR
				 SELECT pinfo, NO, ROUND ((NO / totcount) * 100, 1) per
				   FROM (SELECT   c4010_group_nm pinfo, SUM (1) NO, totcount
							 FROM t4010_group t4010
								, t4011_group_detail t4011
								, (SELECT TRIM (c904_answer_ds) pid
									 FROM t623_patient_answer_list t623, t621_patient_master t621, t614_study_site t614
									WHERE t623.c621_patient_id = t621.c621_patient_id
									  AND t621.c611_study_id = t614.c611_study_id
									  AND t621.c704_account_id = t614.c704_account_id
									  AND t623.c601_form_id = v_form_id
									  AND t623.c623_delete_fl = 'N'
									  AND t621.c621_delete_fl = 'N'
									  AND t621.c621_surgery_comp_fl IS NOT NULL
									  AND t614.c614_void_fl IS NULL
									  AND c904_answer_ds IS NOT NULL
									  AND t623.c602_question_id IN (v_question_id)
									  AND t623.c603_answer_grp_id IN (v_ans_grp_id)
									  AND t614.c614_study_site_id IN (SELECT token
																		FROM v_in_list)
									  AND t621.c611_study_id = p_studyid) partlist
								, (SELECT SUM (1) totcount
									 FROM t4010_group t4010
										, t4011_group_detail t4011
										, (SELECT TRIM (c904_answer_ds) pid
											 FROM t623_patient_answer_list t623
												, t621_patient_master t621
												, t614_study_site t614
											WHERE t623.c621_patient_id = t621.c621_patient_id
											  AND t621.c611_study_id = t614.c611_study_id
											  AND t621.c704_account_id = t614.c704_account_id
											  AND t623.c601_form_id = v_form_id
											  AND t623.c623_delete_fl = 'N'
											  AND t621.c621_delete_fl = 'N'
											  AND t621.c621_surgery_comp_fl IS NOT NULL
											  AND t614.c614_void_fl IS NULL
											  AND c904_answer_ds IS NOT NULL
											  AND t623.c602_question_id IN (v_question_id)
											  AND t623.c603_answer_grp_id IN (v_ans_grp_id)
											  AND t614.c614_study_site_id IN (SELECT token
																				FROM v_in_list)
											  AND t621.c611_study_id = p_studyid) partlist
									WHERE t4010.c4010_group_id = t4011.c4010_group_id
									  AND t4010.c901_type = p_type
									  AND t4011.c205_part_number_id = partlist.pid) totlist
							WHERE t4010.c4010_group_id = t4011.c4010_group_id
							  AND t4010.c901_type = p_type
							  AND t4011.c205_part_number_id = partlist.pid
						 GROUP BY c4010_group_nm, totcount);
		END IF;
	END gm_cr_fch_implant_info;
	
	/*********************************************************************
	 * Purpose: This procedure used to fetch the Amnios study Demographic from details
	 * 
	 **********************************************************************/
	
	PROCEDURE gm_cr_fch_amnios_patient_info (
		p_studyid		   IN		t621_patient_master.c611_study_id%TYPE
	  , p_study_site_ids   IN		VARCHAR2
	  , p_return_cur	   OUT		TYPES.cursor_type
	)
	AS
		v_study_listid t612_study_form_list.c612_study_list_id%TYPE;
		v_study_prdid_in v621_patient_study_list.c613_study_period_id%TYPE;
		v_study_grp_id  varchar2(100);
		v_cnt		   NUMBER;
		--
		v_deviation_symbol VARCHAR2 (10);
	BEGIN
-- To set the study site id information to context

		my_context.set_my_inlist_ctx (p_study_site_ids);

		-- to get the Deviation Symbol
		
		SELECT UNISTR ('\00B1') INTO v_deviation_symbol FROM dual;
		
		-- to get the site id count
		
		SELECT COUNT (1)
		  INTO v_cnt
		  FROM v_in_list
		 WHERE token IS NOT NULL;
		 
		 -- to get the study list and period information
	BEGIN
		SELECT get_rule_value (c906_rule_value, 'STUDY_LIST_ID_GRP')
			 , get_rule_value (c906_rule_value, 'STUDY_PRD_ID_IN_GRP')
		  INTO v_study_listid
			 , v_study_prdid_in
		  FROM t906_rules
		 WHERE c906_rule_value = get_rule_value (p_studyid, 'CR_FCH_PATINFO')
		  AND c906_rule_grp_id = 'CR_FCH_PATINFO'
		  AND c906_rule_id = p_studyid
		  AND c906_void_fl IS NULL;
		 
		 		 EXCEPTION WHEN NO_DATA_FOUND
			 THEN
			 	  v_study_listid :='';
			  	  v_study_prdid_in :='';
	END;
	
	-- Once got the Study period id to fetch the Demorgrahic information.
	
		IF  v_study_prdid_in IS NOT NULL
	 	THEN
		OPEN p_return_cur
		 FOR
			 SELECT   NVL (treatment, NVL (sgroup, 'ALL PATIENTS')) treatment, maleno, maleper, femaleno, femaleper
					, ageavg, (to_char(v_deviation_symbol) || agesd) agesd, heightavg, (to_char(v_deviation_symbol) || heightsd) heightsd, weightavg
					, (to_char(v_deviation_symbol) || weightsd) weightsd
					--
					, bmiavg
					, (to_char(v_deviation_symbol) || bmisd) bmisd
					--
					, Tob_yes_num
					, tob_yes_per
					, tob_no_num
					, tob_no_per
					, tob_past_num
					, tob_past_per
					--
					, race_asian_no
					, race_asian_per
					
					,race_black_no
					,race_black_per
					
					,race_cau_no
					,race_cau_per
					,race_his_no
					,race_his_per
					,race_other_no
					,race_other_per
					--
					, DECODE (grpingid, 0, DECODE (sgroup, NULL, 2, grpingid), grpingid) groupingcount
					, NVL (sgroup, DECODE (grpingid, 0, 'XXXX')) sgroup
				 FROM (SELECT	t901.c901_code_nm treatment, SUM (male) maleno
							  , get_formatted_number (((SUM (male) / (COUNT (1))) * 100), 1) maleper
							  , SUM (female) femaleno
							  , get_formatted_number (((SUM (female) / (COUNT (1))) * 100), 1) femaleper
							  , t901.c902_code_nm_alt sgroup, t901.c901_code_nm stype
							  , get_formatted_number (AVG (age), 1) ageavg, get_formatted_number (STDDEV (age)
																								, 1) agesd
							  , get_formatted_number (AVG (height), 1) heightavg
							  , get_formatted_number (STDDEV (height), 1) heightsd
							  , get_formatted_number (AVG (weight), 1) weightavg
							  , get_formatted_number (STDDEV (weight), 1) weightsd
							  , GROUPING_ID (t901.c901_code_nm, t901.c902_code_nm_alt) grpingid
							  , NVL (t901.c901_code_seq_no, 0) sseqno
							  --
							  , get_formatted_number (AVG (bmi), 1) bmiavg
							  , get_formatted_number (STDDEV (bmi), 1) bmisd
							  --
							  , SUM (Tobacco_yes) Tob_yes_num
							  , get_formatted_number (((SUM (Tobacco_yes) / (COUNT (1))) * 100), 1) tob_yes_per
							  , SUM (Tobacco_no) Tob_no_num
							  , get_formatted_number (((SUM (Tobacco_no) / (COUNT (1))) * 100), 1) tob_no_per
							  , SUM (Tobacco_past) Tob_past_num
							  , get_formatted_number (((SUM (Tobacco_past) / (COUNT (1))) * 100), 1) tob_past_per
							  --
							  , SUM (race_asian) race_asian_no
							  , get_formatted_number (((SUM (race_asian) / (COUNT (1))) * 100), 1) race_asian_per
							   , SUM (race_black) race_black_no
							  , get_formatted_number (((SUM (race_black) / (COUNT (1))) * 100), 1) race_black_per
							   , SUM (race_cau) race_cau_no
							  , get_formatted_number (((SUM (race_cau) / (COUNT (1))) * 100), 1) race_cau_per
							   , SUM (race_his) race_his_no
							  , get_formatted_number (((SUM (race_his) / (COUNT (1))) * 100), 1) race_his_per
							   , SUM (race_other) race_other_no
							  , get_formatted_number (((SUM (race_other) / (COUNT (1))) * 100), 1) race_other_per
							  
						   FROM t901_code_lookup t901
							  , (SELECT   t622.c621_patient_id, v621.c901_surgery_type
										, MAX (DECODE (c603_answer_grp_id
													--Get the Amnios study Age information.
													 ,26202, c904_answer_ds
													 , 0
													  )
											  ) age --Based on the surgery date and date of birth patient age can be calculated for patient data screen
										, MAX (DECODE (c603_answer_grp_id
										-- get the Amnios study - Gender information
													 , 26203, DECODE (c605_answer_list_id, 26201, 1, 0)
													 , 0
													  )
											  ) male
										, MAX (DECODE (c603_answer_grp_id
										-- get the Amnios study - Gender information
													 , 26203, DECODE (c605_answer_list_id, 26202, 1, 0)
													 , 0
													  )
											  ) female
										, MAX (DECODE (c603_answer_grp_id 
										-- get the Amnios study - Weight information
														,26205, gm_pkg_cr_common.get_valid_cr_number(t623.c904_answer_ds)
														, 0)) weight
										, (  MAX (DECODE (c603_answer_grp_id, 100, gm_pkg_cr_common.get_valid_cr_number(t623.c904_answer_ds), 0)) * 12
										   + MAX (DECODE (c603_answer_grp_id
										  -- get the patient height information
										   				, 26204, gm_pkg_cr_common.get_valid_cr_number(t623.c904_answer_ds)
										   				, 0))
										  ) height
										  --
										  , ( MAX (DECODE (c603_answer_grp_id
										   				, 26206, t623.c904_answer_ds
										   				, 0))
										  ) bmi
										  --
										 , ( MAX (DECODE (c603_answer_grp_id
										 				, 26207, DECODE (c605_answer_list_id, 26203, 1, 0)
										   			    , 0))
										  ) Tobacco_yes
										  , ( MAX (DECODE (c603_answer_grp_id
										 				, 26207, DECODE (c605_answer_list_id, 26204, 1, 0)
										   			    , 0))
										  ) Tobacco_no
										  , ( MAX (DECODE (c603_answer_grp_id
										 				, 26207, DECODE (c605_answer_list_id, 26205, 1, 0)
										   			    , 0))
										  ) Tobacco_past
										  --
										 , ( MAX (DECODE (c603_answer_grp_id
										   				, 26209, DECODE (c904_answer_ds, 'checked', 1, 0), 0))	
									 	   ) race_asian
									 	 
									 	 , ( MAX (DECODE (c603_answer_grp_id
										   				, 26210, DECODE (c904_answer_ds, 'checked', 1, 0), 0))	
									 	   ) race_black  
									 	 , ( MAX (DECODE (c603_answer_grp_id
										   				, 26211, DECODE (c904_answer_ds, 'checked', 1, 0), 0))	
									 	   ) race_cau  
									 	  , ( MAX (DECODE (c603_answer_grp_id
										   				, 26212, DECODE (c904_answer_ds, 'checked', 1, 0), 0))	
									 	   ) race_his
									 	  , ( MAX (DECODE (c603_answer_grp_id
										   				, 26213, DECODE (c904_answer_ds, NULL, 0, 1), 0))	
									 	   ) race_other 
									 FROM t623_patient_answer_list t623
										, t622_patient_list t622
										, v621_patient_study_list v621
									WHERE t623.c603_answer_grp_id IN ( SELECT c906_rule_value  FROM t906_rules WHERE 
										  C906_RULE_GRP_ID = 'DEMOPATIENT' and C906_RULE_ID = p_studyid) 
									  AND t622.c622_patient_list_id = t623.c622_patient_list_id
									  AND v621.c612_study_list_id = v_study_listid
									  AND v621.c622_surgery_date IS NOT NULL
									  AND v621.c621_patient_id = t622.c621_patient_id
									  AND v621.c901_surgery_type IS NOT NULL
									  AND t622.c613_study_period_id = v_study_prdid_in
									  AND (   ('-999' = DECODE (v_cnt, 0, '-999', '-9999'))
										   OR v621.c614_study_site_id IN (SELECT token
																			FROM v_in_list)
										  )
									  AND v621.c611_study_id = p_studyid
									  AND t622.c622_delete_fl = 'N'
									  AND t623.c623_delete_fl = 'N'
									  AND v621.c621_surgery_comp_fl = 'Y'
								 GROUP BY t622.c621_patient_id, v621.c901_surgery_type) summ
						  WHERE t901.c901_code_id = summ.c901_surgery_type
					   GROUP BY ROLLUP (t901.c902_code_nm_alt, (t901.c901_code_seq_no, t901.c901_code_nm))
						 HAVING (	t901.c902_code_nm_alt IS NOT NULL
								 OR t901.c901_code_nm IS NOT NULL
								 OR GROUPING_ID (t901.c901_code_nm, t901.c902_code_nm_alt) <> 2
								))
			 ORDER BY sgroup, groupingcount, sseqno;
       END IF;
       -- Because of no value in rule table for Transition & Acadia Pilot study we are fixing the code for avoiding Cursor is closed error in patient data screen.
	      IF  v_study_listid IS NULL
	      THEN
	      OPEN p_return_cur FOR
	      		SELECT * FROM DUAL WHERE 1=0;
	      END IF;
     
	END gm_cr_fch_amnios_patient_info;

	/*********************************************************************
	 * Purpose: This procedure used to fetch the Amnios study Demographic from details
	 * 
	 **********************************************************************/
	
	PROCEDURE gm_cr_fch_reflect_patient_info (
		p_studyid		   IN		t621_patient_master.c611_study_id%TYPE
	  , p_study_site_ids   IN		VARCHAR2
	  , p_return_cur	   OUT		TYPES.cursor_type
	)
	AS
		v_study_listid t612_study_form_list.c612_study_list_id%TYPE;
		v_study_prdid_in v621_patient_study_list.c613_study_period_id%TYPE;
		v_study_grp_id  varchar2(100);
		v_cnt		   NUMBER;
		--
		v_deviation_symbol VARCHAR2 (10);
	BEGIN
-- To set the study site id information to context

		my_context.set_my_inlist_ctx (p_study_site_ids);

		-- to get the Deviation Symbol
		
		SELECT UNISTR ('\00B1') INTO v_deviation_symbol FROM dual;
		
		-- to get the site id count
		
		SELECT COUNT (1)
		  INTO v_cnt
		  FROM v_in_list
		 WHERE token IS NOT NULL;
		 
		 -- to get the study list and period information
		 v_study_listid := '';
		 v_study_listid := '';
	
	-- Once got the Study period id to fetch the Demorgrahic information.
	
		OPEN p_return_cur
		 FOR
			 SELECT   NVL (treatment, NVL (sgroup, 'ALL PATIENTS')) treatment, maleno, maleper, femaleno, femaleper
					, ageavg, (to_char(v_deviation_symbol) || agesd) agesd, heightavg, (to_char(v_deviation_symbol) || heightsd) heightsd, weightavg
					, (to_char(v_deviation_symbol) || weightsd) weightsd
					--
					--
					, race_asian_no
					, race_asian_per
					
					,race_black_no
					,race_black_per
					
					,race_cau_no
					,race_cau_per
					,race_his_no
					,race_his_per
					,race_other_no
					,race_other_per
					--
					, DECODE (grpingid, 0, DECODE (sgroup, NULL, 2, grpingid), grpingid) groupingcount
					, NVL (sgroup, DECODE (grpingid, 0, 'XXXX')) sgroup
				 FROM (SELECT	t901.c901_code_nm treatment, SUM (male) maleno
							  , get_formatted_number (((SUM (male) / (COUNT (1))) * 100), 1) maleper
							  , SUM (female) femaleno
							  , get_formatted_number (((SUM (female) / (COUNT (1))) * 100), 1) femaleper
							  , t901.c902_code_nm_alt sgroup, t901.c901_code_nm stype
							  , get_formatted_number (AVG (age), 1) ageavg, get_formatted_number (STDDEV (age)
																								, 1) agesd
							  , get_formatted_number (AVG (height) / 12, 1) heightavg
							  , get_formatted_number (STDDEV (height), 1) heightsd
							  , get_formatted_number (AVG (weight), 1) weightavg
							  , get_formatted_number (STDDEV (weight), 1) weightsd
							  --
							  , SUM (race_asian) race_asian_no
							  , get_formatted_number (((SUM (race_asian) / (COUNT (1))) * 100), 1) race_asian_per
							   , SUM (race_black) race_black_no
							  , get_formatted_number (((SUM (race_black) / (COUNT (1))) * 100), 1) race_black_per
							   , SUM (race_cau) race_cau_no
							  , get_formatted_number (((SUM (race_cau) / (COUNT (1))) * 100), 1) race_cau_per
							   , SUM (race_his) race_his_no
							  , get_formatted_number (((SUM (race_his) / (COUNT (1))) * 100), 1) race_his_per
							   , SUM (race_other) race_other_no
							  , get_formatted_number (((SUM (race_other) / (COUNT (1))) * 100), 1) race_other_per
							  --
							  , GROUPING_ID (t901.c901_code_nm, t901.c902_code_nm_alt) grpingid
							  , NVL (t901.c901_code_seq_no, 0) sseqno
							  
						   FROM t901_code_lookup t901
							  , (SELECT   t622.c621_patient_id, v621.c901_surgery_type
										, MAX (DECODE (c603_answer_grp_id
													 -- reflect study
													 , 27201, ROUND (MONTHS_BETWEEN (v621.c622_surgery_date, TO_DATE (c904_answer_ds, 'MM/DD/YYYY')) /12, 1 )
													 , 0
													  )
											  ) age --Based on the surgery date and date of birth patient age can be calculated for patient data screen
										, MAX (DECODE (c603_answer_grp_id
													 -- reflect changes
													 , 27202, DECODE(c605_answer_list_id, 27201, 1, 0)
													 , 0
													  )
											  ) male
										, MAX (DECODE (c603_answer_grp_id
													 -- reflect changes
													 , 27202, DECODE(c605_answer_list_id, 27202, 1, 0)
													 , 0
													  )
											  ) female
										, MAX (DECODE (c603_answer_grp_id 
														, 27302, gm_pkg_cr_common.get_valid_cr_number (t623.c904_answer_ds), 0)) weight
										, (  MAX (DECODE (c603_answer_grp_id
											, 27301, gm_pkg_cr_common.get_valid_cr_number (t623.c904_answer_ds), 0))
										  ) height
										  --
										 , ( MAX (DECODE (c603_answer_grp_id
										   				, 27203, DECODE (c904_answer_ds, 'checked', 1, 0), 0))	
									 	   ) race_asian
									 	 
									 	 , ( MAX (DECODE (c603_answer_grp_id
										   				, 27204, DECODE (c904_answer_ds, 'checked', 1, 0), 0))	
									 	   ) race_black  
									 	 , ( MAX (DECODE (c603_answer_grp_id
										   				, 27205, DECODE (c904_answer_ds, 'checked', 1, 0), 0))	
									 	   ) race_cau  
									 	  , ( MAX (DECODE (c603_answer_grp_id
										   				, 27206, DECODE (c904_answer_ds, 'checked', 1, 0), 0))	
									 	   ) race_his
									 	  , ( MAX (DECODE (c603_answer_grp_id
										   				, 27207, DECODE (c904_answer_ds, NULL, 0, 1), 0))	
									 	   ) race_other 
									 FROM t623_patient_answer_list t623
										, t622_patient_list t622
										, v621_patient_study_list v621
									WHERE t623.c602_question_id IN (27201, 27202, 27301, 27302, 27203, 27204, 27205, 27206, 27207) 
									  AND t622.c622_patient_list_id = t623.c622_patient_list_id
									  AND v621.c612_study_list_id IN (27202,  27303)
									  AND v621.c622_surgery_date IS NOT NULL
									  AND v621.c621_patient_id = t622.c621_patient_id
									  AND v621.c901_surgery_type IS NOT NULL
									  AND t622.c613_study_period_id IN (1046, 1045)
									  AND (   ('-999' = DECODE (v_cnt, 0, '-999', '-9999'))
										   OR v621.c614_study_site_id IN (SELECT token
																			FROM v_in_list)
										  )
									  AND v621.c611_study_id = p_studyid
									  AND t622.c622_delete_fl = 'N'
									  AND t623.c623_delete_fl = 'N'
									  AND v621.c621_surgery_comp_fl = 'Y'
								 GROUP BY t622.c621_patient_id, v621.c901_surgery_type) summ
						  WHERE t901.c901_code_id = summ.c901_surgery_type
					   GROUP BY ROLLUP (t901.c902_code_nm_alt, (t901.c901_code_seq_no, t901.c901_code_nm))
						 HAVING (	t901.c902_code_nm_alt IS NOT NULL
								 OR t901.c901_code_nm IS NOT NULL
								 OR GROUPING_ID (t901.c901_code_nm, t901.c902_code_nm_alt) <> 2
								))
			 ORDER BY sgroup, groupingcount, sseqno;
     
	END gm_cr_fch_reflect_patient_info;
	
	
END gm_pkg_cr_dgraph_rpt;
/
