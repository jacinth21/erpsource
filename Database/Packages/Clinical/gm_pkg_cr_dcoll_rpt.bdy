--@"C:\Database\Packages\Clinical\gm_pkg_cr_dcoll_rpt.bdy";
CREATE OR REPLACE PACKAGE BODY gm_pkg_cr_dcoll_rpt
IS
/*******************************************************
* Purpose: Package holds Procs for Data Collection Reports
*
*******************************************************/
--
/*******************************************************
* Purpose: This Procedure is used to fetch the expected and actual follow up by treatment
*******************************************************/
--
   PROCEDURE gm_cr_fch_exp_act_treatment (
      p_studyid        IN       t611_clinical_study.c611_study_id%TYPE,
      p_studysiteid    IN       VARCHAR2,
      p_out_expected   OUT      TYPES.cursor_type,
      p_out_actual     OUT      TYPES.cursor_type
   )
   AS
      v_preop_slid   VARCHAR2 (400);
      v_tos_slid     t612_study_form_list.c612_study_list_id%TYPE;
      v_preop_spid   v621_patient_study_list.c613_study_period_id%TYPE;
      v_tos_spid     v621_patient_study_list.c613_study_period_id%TYPE;
      v_cnt          NUMBER;
   BEGIN
      -- The input String in the form of 101.210,101.211 will be set into this temp view.
      my_context.set_my_inlist_ctx (p_studysiteid);

      SELECT COUNT (1)
        INTO v_cnt
        FROM v_in_list
       WHERE token IS NOT NULL;

      gm_cr_fch_dcoll_values (p_studyid,
                              v_preop_slid,
                              v_tos_slid,
                              v_preop_spid,
                              v_tos_spid
                             );

      -- PMT-39479: Amnios Label changes 
      -- to set values to clob
      my_context.set_my_cloblist (v_preop_slid ||',' || v_tos_slid ||',');
      
      OPEN p_out_expected FOR
         SELECT   'A'||ROWNUM ID, NVL (stype, NVL (sgroup, 'ALL PATIENTS')) NAME,
                  c613_study_period_ds period, cnt,
                  DECODE (grpingid,
                          0, DECODE (sgroup, NULL, 2, grpingid),
                          grpingid
                         ) groupingcount,
                  sseqno, pseqno,
                  NVL (sgroup, DECODE (grpingid, 0, 'XXXX')) sgroup
             FROM (SELECT   t901.c902_code_nm_alt sgroup,
                            t901.c901_code_nm stype,
                            v621.c613_study_period_ds,
                            v621.c613_seq_no pseqno, COUNT (1) cnt,
                            GROUPING_ID (t901.c901_code_nm,
                                         t901.c902_code_nm_alt
                                        ) grpingid,
                            NVL (t901.c901_code_seq_no, 0) sseqno
                       FROM v621_patient_study_list v621,
                            t901_code_lookup t901
                      WHERE v621.c621_exp_date_start <= TRUNC (SYSDATE)
                        AND v621.c622_surgery_date IS NOT NULL
                        AND v621.c612_study_list_id IN
                                                   (SELECT c205_part_number_id FROM my_t_part_list)
                        AND v621.c613_study_period_id NOT IN (v_preop_spid)
                        AND v621.c621_surgery_comp_fl = 'Y'
                        AND v621.c611_study_id = p_studyid
                        AND (   ('-999' = DECODE (v_cnt, 0, '-999', '-9999')
                                )
                             OR v621.c614_study_site_id IN (SELECT *
                                                              FROM v_in_list)
                            )
                        AND v621.c901_surgery_type = t901.c901_code_id
                   GROUP BY v621.c613_study_period_ds,
                            v621.c613_seq_no,
                            ROLLUP (t901.c902_code_nm_alt,
                                    (t901.c901_code_seq_no,
                                     t901.c901_code_nm
                                    ))
                   --  , t901.C901_CODE_ID
                   ORDER BY v621.c613_study_period_ds, v621.c613_seq_no)
            WHERE sgroup IS NOT NULL OR stype IS NOT NULL OR grpingid <> 2
         ORDER BY sgroup, groupingcount, sseqno, pseqno;

      OPEN p_out_actual FOR
         SELECT  'A'||ROWNUM ID, NVL (stype, NVL (sgroup, 'ALL PATIENTS')) NAME,
                  c613_study_period_ds period, cnt,
                  DECODE (grpingid,
                          0, DECODE (sgroup, NULL, 2, grpingid),
                          grpingid
                         ) grpingid,
                  NVL (sseqno, 0) sseqno, pseqno,
                  NVL (sgroup, DECODE (grpingid, 0, 'XXXX')) sgroup
             FROM (SELECT   sgroup, stype, c613_study_period_ds, codeid,
                            pseqno, SUM (cnt) cnt, sseqno,
                            DECODE (stype,
                                    'Out-of-Window Visits', 4,
                                    'Missing Follow-Up', 5,
                                    GROUPING_ID (stype, sgroup)
                                   ) grpingid
                       FROM (SELECT   t901.c902_code_nm_alt sgroup,
                                      t901.c901_code_nm stype,
                                      v621.c613_study_period_ds,
                                      t901.c901_code_id codeid,
                                      v621.c613_seq_no pseqno, COUNT (1) cnt,
                                      NVL (t901.c901_code_seq_no, 0) sseqno
                                 FROM v621_patient_study_list v621,
                                      t901_code_lookup t901,
                                      t622_patient_list t622
                                WHERE v621.c622_surgery_date IS NOT NULL
                                  AND v621.c612_study_list_id IN
                                                   (SELECT c205_part_number_id FROM my_t_part_list)
                                  AND v621.c613_study_period_id NOT IN
                                                               (v_preop_spid)
                                  AND v621.c611_study_id = p_studyid
                                  AND (   ('-999' =
                                              DECODE (v_cnt,
                                                      0, '-999',
                                                      '-9999'
                                                     )
                                          )
                                       OR v621.c614_study_site_id IN (
                                                                SELECT *
                                                                  FROM v_in_list)
                                      )
                                  AND v621.c621_surgery_comp_fl = 'Y'
                                  AND v621.c621_patient_id =
                                                          t622.c621_patient_id
                                  AND v621.c613_study_period_id =
                                                     t622.c613_study_period_id
                                  -- AND t622.c622_out_of_window_fl IS NULL
                                  AND t622.c622_missing_follow_up_fl IS NULL
                                   AND NVL(t622.C622_DELETE_FL,'N')='N'
                                  AND v621.c901_surgery_type =
                                                             t901.c901_code_id
                             GROUP BY t901.c902_code_nm_alt,
                                      t901.c901_code_id,
                                      t901.c901_code_nm,
                                      v621.c613_study_period_ds,
                                      v621.c613_seq_no,
                                      t901.c901_code_seq_no)
                   GROUP BY c613_study_period_ds,
                            pseqno,
                            ROLLUP (sgroup, (sseqno, stype, codeid)))
            WHERE sgroup IS NOT NULL OR stype IS NOT NULL OR grpingid <> 2
         UNION
         SELECT  'A' ID, 'Out-of-Window Visits' NAME,
                  v621.c613_study_period_ds period, COUNT (1) cnt, 4 grpingid,
                  6 sseqno, v621.c613_seq_no pseqno, '' sgroup
             FROM v621_patient_study_list v621, t622_patient_list t622
            WHERE v621.c621_exp_date_start <= TRUNC (SYSDATE)
              AND v621.c612_study_list_id IN (SELECT c205_part_number_id FROM my_t_part_list)
              AND v621.c613_study_period_id NOT IN (v_preop_spid, v_tos_spid)
              AND v621.c621_patient_id = t622.c621_patient_id
              AND v621.c613_study_period_id = t622.c613_study_period_id
              AND v621.c621_surgery_comp_fl = 'Y'
              AND t622.c622_delete_fl = 'N'
              AND t622.c622_out_of_window_fl IS NOT NULL
              AND t622.c622_missing_follow_up_fl IS NULL
              AND v621.c611_study_id = p_studyid
              AND (   ('-999' = DECODE (v_cnt, 0, '-999', '-9999'))
                   OR v621.c614_study_site_id IN (SELECT *
                                                    FROM v_in_list)
                  )
         GROUP BY v621.c613_study_period_ds, v621.c613_seq_no
         UNION
         SELECT   'B' ID, 'Missing Follow-Up' NAME,
                  v621.c613_study_period_ds period, COUNT (1) cnt, 5 grpingid,
                  7 sseqno, v621.c613_seq_no pseqno, '' sgroup
             FROM v621_patient_study_list v621
            WHERE v621.c621_exp_date_final <= TRUNC (SYSDATE)
              AND v621.c612_study_list_id IN (SELECT c205_part_number_id FROM my_t_part_list)
              AND v621.c613_study_period_id NOT IN (v_preop_spid, v_tos_spid)
              AND v621.c621_surgery_comp_fl = 'Y'
              AND v621.c611_study_id = p_studyid
              AND (   ('-999' = DECODE (v_cnt, 0, '-999', '-9999'))
                   OR v621.c614_study_site_id IN (SELECT *
                                                    FROM v_in_list)
                  )
              AND NOT EXISTS  (
                        SELECT t622.c621_patient_id,
                               nvl(t622.c613_study_period_id,-9999)
                          FROM t622_patient_list t622
                         WHERE t622.c622_missing_follow_up_fl IS NULL
                         AND NVL(t622.C622_DELETE_FL,'N')='N'
                         AND NVL(t622.c613_study_period_id,-9999) = v621.c613_study_period_id
                         AND t622.c621_patient_id = v621.c621_patient_id )
         GROUP BY v621.c613_study_period_ds, v621.c613_seq_no
         ORDER BY sgroup, grpingid, sseqno, pseqno;
   END gm_cr_fch_exp_act_treatment;

   /*******************************************************
    * Purpose: This Procedure is used to fetch header for the expected and actual follow up by treatment
   *******************************************************/
   PROCEDURE gm_cr_fch_header_exp_act (
      p_out_header_expected   OUT      TYPES.cursor_type,
      p_studyid               IN       t611_clinical_study.c611_study_id%TYPE
   )
   AS
      v_preop_slid   VARCHAR2 (400);
      v_tos_slid     t612_study_form_list.c612_study_list_id%TYPE;
      v_preop_spid   v621_patient_study_list.c613_study_period_id%TYPE;
      v_tos_spid     v621_patient_study_list.c613_study_period_id%TYPE;
   BEGIN
	   
      gm_cr_fch_dcoll_values (p_studyid,
                              v_preop_slid,
                              v_tos_slid,
                              v_preop_spid,
                              v_tos_spid
                             );
      -- PMT-39479: Amnios Label changes 
      -- to set values to clob
      my_context.set_my_cloblist (v_preop_slid ||',' || v_tos_slid ||',');
      
      
	  IF  v_preop_slid IS NOT NULL 
	  THEN
      OPEN p_out_header_expected FOR
         SELECT DISTINCT c613_study_period_ds, c613_seq_no
                    FROM t613_study_period
                   WHERE c612_study_list_id IN (SELECT c205_part_number_id FROM my_t_part_list)
                     AND c613_study_period_id NOT IN (v_preop_spid)
                ORDER BY c613_seq_no;
      END IF;
      -- Because of no value in rule table for Transition & Acadia Pilot study we are fixing the code for avoiding Cursor is closed error.
      IF  v_preop_spid IS NULL
      THEN
      OPEN p_out_header_expected FOR
      		SELECT * FROM DUAL WHERE 1=0;
      END IF;
      
     END gm_cr_fch_header_exp_act;

   /*******************************************************
* Purpose: This Procedure is used to fetch details for out of window visits page
*******************************************************/
--
   PROCEDURE gm_cr_fch_out_of_window (
      p_studyid            IN       t611_clinical_study.c611_study_id%TYPE,
      p_studysiteid        IN       VARCHAR2,
      p_out_header         OUT      TYPES.cursor_type,
      p_out_surgeon_trmt   OUT      TYPES.cursor_type,
      p_out_period_data    OUT      TYPES.cursor_type
   )
   AS
      v_preop_slid   VARCHAR2 (400);
      v_tos_slid     t612_study_form_list.c612_study_list_id%TYPE;
      v_preop_spid   v621_patient_study_list.c613_study_period_id%TYPE;
      v_tos_spid     v621_patient_study_list.c613_study_period_id%TYPE;
      v_cnt          NUMBER;
   BEGIN
      my_context.set_my_inlist_ctx (p_studysiteid);

      SELECT COUNT (1)
        INTO v_cnt
        FROM v_in_list
       WHERE token IS NOT NULL;

      gm_cr_fch_dcoll_values (p_studyid,
                              v_preop_slid,
                              v_tos_slid,
                              v_preop_spid,
                              v_tos_spid
                             );

                             
     -- PMT-39479: Amnios Label changes 
     -- to set values to clob
      my_context.set_my_cloblist (v_preop_slid ||',' || v_tos_slid ||',');
      
      OPEN p_out_header FOR
         SELECT DISTINCT c613_study_period_ds, c613_seq_no
                    FROM t613_study_period
                   WHERE c612_study_list_id IN (SELECT c205_part_number_id FROM my_t_part_list)
                     AND c613_study_period_id NOT IN
                                                   (v_preop_spid, v_tos_spid)
                ORDER BY c613_seq_no;

      OPEN p_out_surgeon_trmt FOR
         SELECT   t621.c621_patient_id ID, t621.c621_patient_ide_no NAME,
                  NVL (get_party_name (t621.c101_surgeon_id),
                       'N/A'
                      ) surgeonname,
                  NVL (get_code_name (t621.c901_surgery_type),
                       'N/A'
                      ) treatment
             FROM t621_patient_master t621
            WHERE t621.c621_delete_fl = 'N'
              AND NVL (t621.c621_surgery_comp_fl, 'N') = 'Y'
         ORDER BY t621.c621_patient_ide_no;

      OPEN p_out_period_data FOR
         SELECT   v621.c621_patient_id ID, v621.c621_patient_ide_no NAME,
                  c613_study_period_ds period,
                  DECODE (SIGN (t622.c622_date - v621.c621_exp_date_start),
                          -1, ROUND (t622.c622_date - v621.c621_exp_date_start),
                          ROUND (t622.c622_date - v621.c621_exp_date_final)
                         ) dateval
             --, t622.C622_DATE , v621.C621_EXP_DATE_FINAL
         FROM     v621_patient_study_list v621, t622_patient_list t622
            WHERE v621.c621_exp_date_start <= TRUNC (SYSDATE)
              AND v621.c612_study_list_id IN (SELECT c205_part_number_id FROM my_t_part_list)
              AND v621.c613_study_period_id NOT IN (v_preop_spid, v_tos_spid)
              AND v621.c621_patient_id = t622.c621_patient_id
              AND v621.c613_study_period_id = t622.c613_study_period_id
              AND v621.c621_surgery_comp_fl = 'Y'
              AND t622.c622_delete_fl = 'N'
              AND t622.c622_out_of_window_fl IS NOT NULL
              AND v621.c611_study_id = p_studyid
              AND (   ('-999' = DECODE (v_cnt, 0, '-999', '-9999'))
                   OR v621.c614_study_site_id IN (SELECT *
                                                    FROM v_in_list)
                  )
         ORDER BY v621.c621_patient_ide_no, c901_surgery_type, c613_seq_no;
   END gm_cr_fch_out_of_window;

   /*******************************************************
* Purpose: This Procedure is used to fetch details for Missing Follow up
*******************************************************/
--
   PROCEDURE gm_cr_fch_missing_follow_up (
      p_studyid            IN       t611_clinical_study.c611_study_id%TYPE,
      p_studysiteid        IN       VARCHAR2,
      p_out_header         OUT      TYPES.cursor_type,
      p_out_surgeon_trmt   OUT      TYPES.cursor_type,
      p_out_period_data    OUT      TYPES.cursor_type
   )
   AS
      v_preop_slid   VARCHAR2 (400);
      v_tos_slid     t612_study_form_list.c612_study_list_id%TYPE;
      v_preop_spid   v621_patient_study_list.c613_study_period_id%TYPE;
      v_tos_spid     v621_patient_study_list.c613_study_period_id%TYPE;
      v_cnt          NUMBER;
   BEGIN
      my_context.set_my_inlist_ctx (p_studysiteid);

      SELECT COUNT (1)
        INTO v_cnt
        FROM v_in_list
       WHERE token IS NOT NULL;

      gm_cr_fch_dcoll_values (p_studyid,
                              v_preop_slid,
                              v_tos_slid,
                              v_preop_spid,
                              v_tos_spid
                             );

      -- PMT-39479: Amnios Label changes 
      -- to set values to clob
      my_context.set_my_cloblist (v_preop_slid ||',' || v_tos_slid ||',');
      
      OPEN p_out_header FOR
         SELECT DISTINCT c613_study_period_ds, c613_seq_no
                    FROM t613_study_period
                   WHERE c612_study_list_id IN (SELECT c205_part_number_id FROM my_t_part_list)
                     AND c613_study_period_id NOT IN
                                                   (v_preop_spid, v_tos_spid)
                ORDER BY c613_seq_no;

      OPEN p_out_surgeon_trmt FOR
         SELECT   t621.c621_patient_id ID, t621.c621_patient_ide_no NAME,
                  NVL (get_party_name (t621.c101_surgeon_id),
                       'N/A'
                      ) surgeonname,
                  NVL (get_code_name (t621.c901_surgery_type),
                       'N/A'
                      ) treatment
             FROM t621_patient_master t621
         ORDER BY t621.c621_patient_ide_no;

      OPEN p_out_period_data FOR
         SELECT v621.c621_patient_id ID, c621_patient_ide_no NAME,
                c613_study_period_ds period,
                'M' miss                  --,get_code_name (c901_surgery_type)
           FROM v621_patient_study_list v621
          WHERE c621_exp_date_final <= TRUNC (SYSDATE)
            AND v621.c612_study_list_id IN (SELECT c205_part_number_id FROM my_t_part_list)
            AND v621.c613_study_period_id NOT IN (v_preop_spid, v_tos_spid)
            AND v621.c621_surgery_comp_fl = 'Y'
--AND    v621.C621_PATIENT_ID = 104
            AND v621.c611_study_id = p_studyid
            AND (   ('-999' = DECODE (v_cnt, 0, '-999', '-9999'))
                 OR v621.c614_study_site_id IN (SELECT *
                                                  FROM v_in_list)
                )
            AND (c621_patient_id, c613_study_period_id) NOT IN (
                        SELECT t622.c621_patient_id,
                               nvl(t622.c613_study_period_id,-9999)
                          FROM t622_patient_list t622
                         WHERE t622.c622_missing_follow_up_fl IS NULL
                         AND NVL(t622.C622_DELETE_FL,'N')='N')
                         
         UNION
         SELECT v621.c621_patient_id ID, c621_patient_ide_no NAME,
                c613_study_period_ds period,
                'Y' miss                -- ,get_code_name (c901_surgery_type),
           FROM t622_patient_list t622, v621_patient_study_list v621
          WHERE v621.c621_patient_id = t622.c621_patient_id
            AND v621.c613_study_period_id = t622.c613_study_period_id
            AND t622.c621_patient_id IN (
                   SELECT v621.c621_patient_id
                     FROM v621_patient_study_list v621
                    WHERE c621_exp_date_final <= TRUNC (SYSDATE)
                      AND v621.c612_study_list_id IN (SELECT c205_part_number_id FROM my_t_part_list)
                      AND v621.c613_study_period_id NOT IN
                                                   (v_preop_spid, v_tos_spid)
                      AND v621.c621_surgery_comp_fl = 'Y'
--AND    v621.C621_PATIENT_ID = 104
                      AND (c621_patient_id, c613_study_period_id) NOT IN (
                             SELECT t622.c621_patient_id,
                                    nvl(t622.c613_study_period_id,-9999)
                               FROM t622_patient_list t622
                              WHERE t622.c622_missing_follow_up_fl IS NULL
                               AND NVL(t622.C622_DELETE_FL,'N')='N'))
            AND v621.c612_study_list_id IN (SELECT c205_part_number_id FROM my_t_part_list)
            AND v621.c613_study_period_id NOT IN (v_preop_spid)
            AND t622.c622_missing_follow_up_fl IS NULL
            AND v621.c611_study_id = p_studyid
            AND (   ('-999' = DECODE (v_cnt, 0, '-999', '-9999'))
                 OR v621.c614_study_site_id IN (SELECT *
                                                  FROM v_in_list)
                )
            AND t622.c622_delete_fl = 'N';
   END gm_cr_fch_missing_follow_up;

/*******************************************************
* Purpose: This Procedure is used to fetch the values for dcoll reports
*******************************************************/
--
   PROCEDURE gm_cr_fch_dcoll_values (
      p_studyid    IN       t611_clinical_study.c611_study_id%TYPE,
      preop_slid   OUT      VARCHAR2,
      tos_slid     OUT      t612_study_form_list.c612_study_list_id%TYPE,
      preop_spid   OUT      v621_patient_study_list.c613_study_period_id%TYPE,
      tos_spid     OUT      v621_patient_study_list.c613_study_period_id%TYPE
   )
   AS
   BEGIN
	 SELECT get_rule_value (t906.c906_rule_value, 'PREOP_SLID'),
             get_rule_value (t906.c906_rule_value, 'TOS_SLID'),
             get_rule_value (t906.c906_rule_value, 'PREOP_SPID'),
             get_rule_value (t906.c906_rule_value, 'TOS_SPID')
        INTO preop_slid,
             tos_slid,
             preop_spid,
             tos_spid
        FROM t906_rules t906
       WHERE t906.c906_rule_value = get_rule_value (p_studyid, 'DCGRP');
       	 EXCEPTION WHEN NO_DATA_FOUND
			 THEN
			 	  preop_slid :='';
			  	  tos_slid :='';
			  	  preop_spid :='';
			  	  tos_spid :='';
   END gm_cr_fch_dcoll_values;
END gm_pkg_cr_dcoll_rpt;
/
