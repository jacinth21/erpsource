/* Formatted on 2010/05/13 19:57 (Formatter Plus v4.8.0) */
--@"C:\database\Packages\Clinical\gm_pkg_cr_dataupload.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_cr_dataupload
IS
--
/*******************************************************
 * Purpose: This Procedure is used to fetch
 * the data about form ID and Name

 *******************************************************/
--
	PROCEDURE gm_report_form (
		p_studyid	  IN	   t611_clinical_study.c611_study_id%TYPE
	  , p_recordset   OUT	   TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_recordset
		 FOR
			 SELECT c601_form_id ID, c612_study_form_ds NAME
			   FROM t612_study_form_list
			  WHERE c612_upload_fl = 'Y' AND c611_study_id = p_studyid;
	END gm_report_form;

--
/*******************************************************
 * Purpose: This Procedure is used to fetch
 * the study_list_id by studyid and formid

 *******************************************************/
--
	PROCEDURE gm_fetch_study_list_id (
		p_studyid	  IN	   t612_study_form_list.c611_study_id%TYPE
	  , p_formid	  IN	   t612_study_form_list.c601_form_id%TYPE
	  , p_recordset   OUT	   TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_recordset
		 FOR
			 SELECT c612_study_list_id
			   FROM t612_study_form_list
			  WHERE c611_study_id = p_studyid AND c601_form_id = p_formid;
	END gm_fetch_study_list_id;

--
/*******************************************************
 * Purpose: This Procedure is used to update
 * patient_list record
 * userName = manual load('30301')

 *******************************************************/
--
	PROCEDURE gm_update_patient_list (
		p_patient_list_id	IN	 t623_patient_answer_list.c622_patient_list_id%TYPE
	  , p_answer_grp_id 	IN	 t623_patient_answer_list.c603_answer_grp_id%TYPE
	  , p_valueds			IN	 t623_patient_answer_list.c904_answer_ds%TYPE
	  , p_valueid			IN	 t623_patient_answer_list.c605_answer_list_id%TYPE
	  , p_userid			IN	 t623_patient_answer_list.c623_last_updated_by%TYPE
	  , p_hmtype			IN	 VARCHAR2
	)
	AS
	BEGIN
		IF p_hmtype = 'DS'
		THEN
			UPDATE t623_patient_answer_list
			   SET c904_answer_ds = p_valueds
				 , c623_last_updated_by = p_userid
				 , c623_last_updated_date = SYSDATE
			 WHERE c622_patient_list_id = p_patient_list_id AND c603_answer_grp_id = p_answer_grp_id;
		ELSE
			UPDATE t623_patient_answer_list
			   SET c605_answer_list_id = p_valueid
				 , c623_last_updated_by = p_userid
				 , c623_last_updated_date = SYSDATE
			 WHERE c622_patient_list_id = p_patient_list_id AND c603_answer_grp_id = p_answer_grp_id;
		END IF;
	END gm_update_patient_list;

--
/*******************************************************
 * Purpose: This Procedure is used to insert
 * patient_list_id in my_temp_list

 *******************************************************/
--
	PROCEDURE gm_update_temp_list (
		p_patient_list_id	IN	 my_temp_list.my_temp_txn_id%TYPE
	)
	AS
	BEGIN
		INSERT INTO my_temp_list
					(my_temp_txn_id
					)
			 VALUES (p_patient_list_id
					);
	END gm_update_temp_list;

--
/*******************************************************
 * Purpose: This Procedure is used to update
 * verify list according to patient_list_id
 * userName = manual load('30301')
 * verify_level = Moddified correction ('6193')
 *******************************************************/
--
	PROCEDURE gm_update_verify_list (
		p_patient_list_id	IN	 t622_patient_list.c622_patient_list_id%TYPE
	  , p_comments			IN	 t902_log.c902_comments%TYPE
	  , p_userid			IN	 t625_patient_verify_list.c101_user_id%TYPE
	  , p_skipverify		IN	 t622_patient_list.c622_verify_fl%TYPE
	)
	AS
		v_flag		   VARCHAR2 (1);
		v_id		   NUMBER;
		v_msg		   VARCHAR2 (1000);
	BEGIN
		SELECT NVL(c622_verify_fl, 'N')
		  INTO v_flag
		  FROM t622_patient_list
		 WHERE c622_patient_list_id = p_patient_list_id
		 AND NVL(C622_DELETE_FL,'N')='N';

		IF (v_flag = 'N')
		THEN
			gm_update_temp_list (p_patient_list_id);
			
			gm_pkg_cr_datadownload.gm_save_pat_verify_list(p_patient_list_id,p_userid,90901,v_id);--Updated as Data Upload in Verification History section.
						
			IF NVL (p_skipverify, 'N') = 'N'
			THEN
				UPDATE t622_patient_list
				   SET c622_verify_fl = 'Y'
					 , c622_last_updated_by = p_userid
					 , c622_last_updated_date = SYSDATE
				 WHERE c622_patient_list_id = p_patient_list_id;
			END IF;

			gm_update_log (v_id, p_comments, 1204, p_userid, v_msg);
			
			IF NVL (p_skipverify, 'N') = 'N'
			THEN

			gm_pkg_cr_datadownload.gm_save_pat_verify_list(p_patient_list_id,p_userid,6192,v_id);--Updated as Verified in Verification History section after data upload.
			
			gm_update_log (v_id, 'File Verified', 1204, p_userid, v_msg);--File verified as shown in comments section
			END IF;
		END IF;
	END gm_update_verify_list;

--
/*******************************************************
 * Purpose: This Function is used to fetch
 * patient_id according to p_patient_id and p_study_id

 *******************************************************/
--
	FUNCTION get_patient_id (
		p_patient_id   IN	t621_patient_master.c621_patient_ide_no%TYPE
	  , p_study_id	   IN	t621_patient_master.c611_study_id%TYPE
	)
		RETURN VARCHAR2
	IS
		v_patient_id   t621_patient_master.c621_patient_id%TYPE;
	BEGIN
		SELECT c621_patient_id
		  INTO v_patient_id
		  FROM t621_patient_master
		 WHERE c621_patient_ide_no = p_patient_id AND c611_study_id = p_study_id;

		RETURN v_patient_id;
	END get_patient_id;

--
/*******************************************************
 * Purpose: This Function is used to fetch
 * patient_list_id according to p_patient_id and p_study_id

 *******************************************************/
--
	FUNCTION get_patient_list_id (
		p_patient_id   IN	t622_patient_list.c621_patient_id%TYPE
	  , p_study_id	   IN	t622_patient_list.c613_study_period_id%TYPE
	)
		RETURN VARCHAR2
	IS
		v_patient_list_id t622_patient_list.c622_patient_list_id%TYPE;
	BEGIN
		SELECT c622_patient_list_id
		  INTO v_patient_list_id
		  FROM t622_patient_list
		 WHERE c621_patient_id = p_patient_id AND c613_study_period_id = p_study_id
		 AND NVL(C622_DELETE_FL,'N')='N';

		RETURN v_patient_list_id;
	END get_patient_list_id;

--
/*******************************************************
 * Purpose: This Function is used to fetch
 * study_period_id according to p_study_period and p_study_list_id

 *******************************************************/
--
	FUNCTION get_study_period_id (
		p_study_period	  IN   t613_study_period.c901_study_period%TYPE
	  , p_study_list_id   IN   t613_study_period.c612_study_list_id%TYPE
	)
		RETURN VARCHAR2
	IS
		v_study_period_id t613_study_period.c613_study_period_id%TYPE;
	BEGIN
		SELECT c613_study_period_id
		  INTO v_study_period_id
		  FROM t613_study_period
		 WHERE c901_study_period = p_study_period AND c612_study_list_id = p_study_list_id;

		RETURN v_study_period_id;
	END get_study_period_id;

--
/*******************************************************
 * Purpose: This Function is used to get the timepoint for a patient , exam date, form

 *******************************************************/
--
	FUNCTION get_patient_period_id (
		p_patient_id	   IN	t622_patient_list.c621_patient_id%TYPE
	  , p_form_id		   IN	t601_form_master.c601_form_id%TYPE
	  , p_exam_date 	   IN	VARCHAR2
	  , p_study_id		   IN	t612_study_form_list.c611_study_id%TYPE
	  , p_upload_form_id   IN	t601_form_master.c601_form_id%TYPE
	)
		RETURN VARCHAR2
	IS
		v_study_period_id t622_patient_list.c613_study_period_id%TYPE;
	BEGIN
		BEGIN
			/*SELECT t622.c613_study_period_id
			  INTO v_study_period_id
			  FROM t622_patient_list t622
			 WHERE t622.c601_form_id = p_form_id
			   AND t622.c621_patient_id = p_patient_id
			   AND t622.c622_date = TO_DATE (p_exam_date, 'MM/DD/YYYY')
			   AND t622.c622_delete_fl = 'N';
			   */
			SELECT t613.c613_study_period_id
			  INTO v_study_period_id
			  FROM t613_study_period t613, t612_study_form_list t612
			 WHERE t612.c601_form_id = p_upload_form_id
			   AND t612.c611_study_id = p_study_id
			   AND t612.c612_study_list_id = t613.c612_study_list_id
			   AND c613_study_period_ds =
					   (SELECT t613.c613_study_period_ds
						  FROM t622_patient_list t622, t613_study_period t613
						 WHERE t622.c601_form_id = p_form_id
						   AND t622.c621_patient_id = p_patient_id
						   AND t622.c622_date = TO_DATE (p_exam_date, 'MM/DD/YYYY')
						   AND t622.c622_delete_fl = 'N'
						   AND t622.c613_study_period_id = t613.c613_study_period_id);
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				RETURN '';
		END;

		RETURN v_study_period_id;
	END get_patient_period_id;

--
/*******************************************************
 * Purpose: This FUNCTION is used to check if the patient has a SF36 form

 *******************************************************/
--
	FUNCTION get_patient_form (
		p_patient_id   IN	t622_patient_list.c621_patient_id%TYPE
	  , p_form_id	   IN	t601_form_master.c601_form_id%TYPE
	  , p_exam_date    IN	VARCHAR2
	)
		RETURN NUMBER
	IS
		v_patlistid    t622_patient_list.c622_patient_list_id%TYPE;
	BEGIN
		BEGIN
			--Modified to fix 9521
			SELECT t622.c622_patient_list_id
			  INTO v_patlistid
			  FROM t622_patient_list t622
			 WHERE t622.c621_patient_id = p_patient_id
			   AND t622.c601_form_id = p_form_id
			   AND t622.c622_date = TO_DATE (p_exam_date, 'MM/DD/YYYY')
			   AND t622.c622_delete_fl = 'N';
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				RETURN 0;
		END;

		RETURN v_patlistid;
	END get_patient_form;

--
	/*******************************************************
 * Purpose: This FUNCTION is used to check if the form is locked

 *******************************************************/
--
	FUNCTION get_form_lock_value (
		p_study_id	 IN   t612_study_form_list.c611_study_id%TYPE
	  , p_form_id	 IN   t601_form_master.c601_form_id%TYPE
	)
		RETURN VARCHAR2
	IS
		v_lock_fl	   t612_study_form_list.c612_download_lock_fl%TYPE;
	BEGIN
		BEGIN
			SELECT c612_download_lock_fl
			  INTO v_lock_fl
			  FROM t612_study_form_list
			 WHERE c601_form_id = p_form_id AND c611_study_id = p_study_id AND c612_delete_fl = 'N';
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				RETURN '';
		END;

		RETURN v_lock_fl;
	END get_form_lock_value;

	/*******************************************************
 * Purpose: This Procedure is used to get the patient answere list id and update
 * or insert into t623_patient_answer_list

 *******************************************************/
--
	PROCEDURE gm_save_patient_answer (
		p_pat_list_id		IN		 t622_patient_list.c622_patient_list_id%TYPE
	  , p_question_id		IN		 t602_question_master.c602_question_id%TYPE
	  , p_ans_list_id		IN		 t605_answer_list.c605_answer_list_id%TYPE
	  , p_ans_grp_id		IN		 t623_patient_answer_list.c603_answer_grp_id%TYPE
	  , p_ans_desc			IN		 t623_patient_answer_list.c904_answer_ds%TYPE
	  , p_form_id			IN		 t601_form_master.c601_form_id%TYPE
	  , p_patient_id		IN		 t621_patient_master.c621_patient_id%TYPE
	  , p_timepoint 		IN		 t622_patient_list.c613_study_period_id%TYPE
	  , p_user				IN		 t623_patient_answer_list.c623_created_by%TYPE
	  , p_pat_ans_list_id	OUT 	 t623_patient_answer_list.c623_patient_ans_list_id%TYPE
	)
	AS
	BEGIN

		BEGIN
			SELECT t623.c623_patient_ans_list_id
			  INTO p_pat_ans_list_id
			  FROM t622_patient_list t622, t623_patient_answer_list t623
			 WHERE t622.c621_patient_id = p_patient_id
			   AND t622.c601_form_id = p_form_id
			   AND t622.c613_study_period_id = p_timepoint
			   AND t622.c622_patient_list_id = t623.c622_patient_list_id
			   AND t623.c602_question_id = p_question_id
			   AND NVL(t622.C622_DELETE_FL,'N')='N';
														--	 AND NVL (t622.c622_verify_fl, 'N') = 'Y';
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				p_pat_ans_list_id := NULL;
		END;

		gm_pkg_cr_common.gm_sav_patient_answer_list (p_pat_list_id
												   , p_question_id
												   , p_ans_list_id
												   , p_ans_grp_id
												   , p_ans_desc
												   , p_form_id
												   , p_patient_id
												   , p_user
												   , p_pat_ans_list_id
													);
	END gm_save_patient_answer;
END gm_pkg_cr_dataupload;
/
