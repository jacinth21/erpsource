/* Formatted on 2009/06/22 14:33 (Formatter Plus v4.8.0) */

--@"c:\database\packages\clinical\gm_pkg_cr_dynamic_data_load_body.bdy"

CREATE OR REPLACE PACKAGE BODY gm_pkg_cr_dynamic_data_load
IS
/*******************************************************
 * Purpose: Package holds Clinical Affairs Fetch Methods
 * Created By Brinal
 *******************************************************/
--

	/*******************************************************
 * Purpose: This Procedure is used to fetch all the forms
 *******************************************************/
--
	PROCEDURE gm_fch_forms_list (
		p_studyid	 IN 	  t612_study_form_list.c611_study_id%TYPE
	  , p_form_cur	 OUT	  TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_form_cur
		 FOR
			 SELECT   t612.c612_study_list_id studylistid
					, t612.c612_study_form_ds || ' - ' || t601.c601_form_nm formname
				 FROM t612_study_form_list t612, t601_form_master t601
				WHERE t612.c611_study_id = p_studyid
				  AND t612.c612_delete_fl = 'N'
				  AND t612.c601_form_id = t601.c601_form_id
			 ORDER BY t612.c612_seq;
	END gm_fch_forms_list;

/*******************************************************
 * Purpose: This Procedure is used to fetch all the questions
 *******************************************************/
--
	PROCEDURE gm_fch_questions_list (
		p_studylistid	 IN 	  t612_study_form_list.c612_study_list_id%TYPE
	  , p_question_cur	 OUT	  TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_question_cur
		 FOR
			 SELECT DISTINCT 'Ques ' || t604.c604_seq_no qno, t602.c602_question_id qid, t602.c602_question_ds qdesc
						   , t604.c604_seq_no seqno
						FROM t602_question_master t602, t604_form_ques_link t604, t612_study_form_list t612
					   WHERE t612.c612_study_list_id = p_studylistid   -- input from the screen
						 AND t612.c601_form_id = t604.c601_form_id
						 AND t604.c602_question_id = t602.c602_question_id
						 AND t604.c604_delete_fl = 'N'
					ORDER BY t604.c604_seq_no;
	END gm_fch_questions_list;

/*******************************************************
 * Purpose: This Procedure is used to fetch all the timepoints
 *******************************************************/
--
	PROCEDURE gm_fch_timepoints_list (
		p_studylistid	   IN		t612_study_form_list.c612_study_list_id%TYPE
	  , p_timepoints_cur   OUT		TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_timepoints_cur
		 FOR
			 SELECT   t613.c613_study_period_id periodid, get_code_name (t613.c901_study_period) perioddesc
					, get_code_seq_no (t613.c901_study_period) seqno
				 FROM t612_study_form_list t612, t613_study_period t613
				WHERE t612.c612_study_list_id = p_studylistid	-- input from the screen
				  AND t612.c612_study_list_id = t613.c612_study_list_id
			 ORDER BY seqno;
	END gm_fch_timepoints_list;

/*******************************************************
 * Purpose: This Procedure is used to fetch the answerlist
 *******************************************************/
--
	PROCEDURE gm_fch_answers_list (
		p_questionid	IN		 VARCHAR
	  , p_studylistid	IN		 t612_study_form_list.c612_study_list_id%TYPE
	  , p_answers_cur	OUT 	 TYPES.cursor_type
	)
	AS
	BEGIN
		my_context.set_my_inlist_ctx (p_questionid);

		OPEN p_answers_cur
		 FOR
			 SELECT   DECODE (t603.c603_xml_tag_id
							, NULL, ('Q' || t604.c604_seq_no || ' Ans ' || t603.c603_answer_seq_no)
							, t603.c603_xml_tag_id
							 ) ansno
					, t603.c603_answer_grp_id ansgrpid, t603.c603_answer_grp_ds ansdesc
				 FROM t603_answer_grp t603, t604_form_ques_link t604, t612_study_form_list t612
				WHERE t603.c602_question_id IN (SELECT token
												  FROM v_in_list)	-- question id
				  AND t603.c602_question_id = t604.c602_question_id
				  AND t612.c612_study_list_id = p_studylistid
				  AND t612.c601_form_id = t604.c601_form_id
			 ORDER BY t604.c604_seq_no, t603.c603_answer_seq_no;
	-- Revalidate the seq no
	END gm_fch_answers_list;

/*******************************************************
 * Purpose: This Procedure is used to fetch the header and details list for timepoints
 *******************************************************/
--
	PROCEDURE gm_fch_tpoints_details (
		p_tokeni				IN		 VARCHAR
	  , p_studylistid			IN		 t612_study_form_list.c612_study_list_id%TYPE
	  , p_questionid			IN		 t623_patient_answer_list.c602_question_id%TYPE
	  , p_answergroupid 		IN		 t623_patient_answer_list.c603_answer_grp_id%TYPE
	  , p_tpoints_header_cur	OUT 	 TYPES.cursor_type
	  , p_tpoints_details_cur	OUT 	 TYPES.cursor_type
	)
	AS
	BEGIN
		my_context.set_double_inlist_ctx (p_tokeni);

		OPEN p_tpoints_header_cur
		 FOR
			 SELECT   'PatientId' timepoints, 1 seqno
				 FROM DUAL
			 UNION
			 SELECT   'Treatment' timepoints, 2 seqno
				 FROM DUAL
			 UNION
			 SELECT   'Surgeon' timepoints, 3 seqno
				 FROM DUAL
			 UNION
			 SELECT   'SurgeryDate' timepoints, 4 seqno
				 FROM DUAL
			 UNION
			 SELECT   t613.c613_study_period_ds timepoints, ROWNUM + 4 seqno
				 FROM t613_study_period t613
				WHERE t613.c613_study_period_id IN (SELECT tokenii
													  FROM v_double_in_list
													 WHERE token = 'TP')
			 ORDER BY seqno;

		OPEN p_tpoints_details_cur
		 FOR
			 SELECT   t621.c621_patient_id pid
					,	 t621.c621_patient_ide_no
					  || DECODE (c622_patient_event_no, '', '', '-' || c622_patient_event_no) dummy_patientid
					, t613.c613_study_period_ds timepoints
					, NVL (DECODE (t603.c901_control_type
								 , 6100, t605.c605_answer_value
									|| DECODE (t605.c605_cmt_required_fl, 'Y', ' - ' || t623.c904_answer_ds, '')
								 , t623.c904_answer_ds
								  )
						 , ' '
						  ) ans
					, t621.c621_patient_ide_no ||'&nbsp;' patientid, get_code_name (t621.c901_surgery_type) treatment
					, get_party_name (t621.c101_surgeon_id) surgeon
					, TO_CHAR (t621.c621_surgery_date, 'MM/DD/YYYY') surgerydate
				 FROM t623_patient_answer_list t623
					, t622_patient_list t622
					, t613_study_period t613
					, t621_patient_master t621
					, t612_study_form_list t612
					, t614_study_site t614
					, t605_answer_list t605
					, t603_answer_grp t603
				WHERE t612.c612_study_list_id = p_studylistid	-- FORM ID its SC1 ot SC2
				  AND t622.c622_delete_fl = 'N'
				  AND t621.c611_study_id = t612.c611_study_id
				  AND t622.c621_patient_id = t621.c621_patient_id
				  AND t613.c613_study_period_id IN (SELECT tokenii
													  FROM v_double_in_list
													 WHERE token = 'TP')
				  AND t622.c613_study_period_id = t613.c613_study_period_id
				  AND t622.c601_form_id = t612.c601_form_id
				  AND t622.c622_patient_list_id = t623.c622_patient_list_id
				  AND t623.c602_question_id = p_questionid
				  AND t623.c605_answer_list_id = t605.c605_answer_list_id(+)
				  AND t623.c602_question_id = t603.c602_question_id
				  AND t623.c603_answer_grp_id = t603.c603_answer_grp_id
				  AND t623.c603_answer_grp_id = p_answergroupid
				  AND t621.c704_account_id = t614.c704_account_id
				  AND t614.c614_study_site_id IN (SELECT tokenii
													FROM v_double_in_list
												   WHERE token = 'SITE')
				  AND t621.c621_delete_fl = 'N'
			 ORDER BY patientid, t613.c613_seq_no;
	END gm_fch_tpoints_details;

/*******************************************************
* Purpose: This Procedure is used to fetch the header and details list for Answers
*******************************************************/
	PROCEDURE gm_fch_answers_details (
		p_tokeni			IN		 VARCHAR
	  , p_studylistid		IN		 t612_study_form_list.c612_study_list_id%TYPE
	  , p_ans_header_cur	OUT 	 TYPES.cursor_type
	  , p_ans_details_cur	OUT 	 TYPES.cursor_type
	)
	AS
	BEGIN
		my_context.set_double_inlist_ctx (p_tokeni);

		OPEN p_ans_header_cur
		 FOR
			 SELECT   'PatientId' qaid, 0 que, 1 ans
				 FROM DUAL
			 UNION
			 SELECT   'Treatment' qaid, 0 que, 2 ans
				 FROM DUAL
			 UNION
			 SELECT   'Surgeon' qaid, 0 que, 3 ans
				 FROM DUAL
			 UNION
			 SELECT   'SurgeryDate' qaid, 0 que, 4 ans
				 FROM DUAL
			 UNION
			 SELECT   'ExamDate' qaid, 0 que, 5 ans
				 FROM DUAL
			 UNION
			 SELECT   'Timepoint' qaid, 0 que, 6 ans
				 FROM DUAL
			 UNION
			 SELECT   DECODE (t603.c603_xml_tag_id
							, NULL, 'Q' || t604.c604_seq_no || ' Ans ' || t603.c603_answer_seq_no
							, t603.c603_xml_tag_id
							 ) qaid
					, t604.c604_seq_no que, t603.c603_answer_seq_no ans
				 FROM t603_answer_grp t603
					, t604_form_ques_link t604
					, t612_study_form_list t612
					, t602_question_master t602
				WHERE t603.c602_question_id IN (SELECT tokenii
												  FROM v_double_in_list
												 WHERE token = 'QUES')	 -- question id
				  AND t603.c603_answer_grp_id IN (SELECT tokenii
													FROM v_double_in_list
												   WHERE token = 'ANS')
				  AND t603.c602_question_id = t604.c602_question_id
				  AND t612.c612_study_list_id = p_studylistid
				  AND t604.c601_form_id = t612.c601_form_id
				  AND t604.c602_question_id = t602.c602_question_id
			 ORDER BY que, ans;

		OPEN p_ans_details_cur
		 FOR
			 SELECT   t621.c621_patient_id pid
					,	 t621.c621_patient_ide_no
					  || DECODE (c622_patient_event_no, '', '', '-' || c622_patient_event_no)
					  || DECODE (t613.c613_study_period_ds, '', '', '-' || t613.c613_study_period_ds) dummy_patientid
					--, SUBSTR (NVL (t603.c603_answer_grp_ds, t602.c602_question_ds), 1, 10) qaid
					  --, 'Q' || t604.c604_seq_no || ' Ans ' || t603.c603_answer_seq_no qaid
					  ,DECODE (t603.c603_xml_tag_id
							 , NULL, 'Q' || t604.c604_seq_no || ' Ans ' || t603.c603_answer_seq_no
							 , t603.c603_xml_tag_id
							  ) qaid
					, NVL (DECODE (t603.c901_control_type
								 , 6100, t605.c605_answer_value
									|| DECODE (t605.c605_cmt_required_fl
											 , 'Y', DECODE (t623.c904_answer_ds
														  , NULL, ''
														  , ' - ' || t623.c904_answer_ds
														   )
											 , ''
											  )
								 , t623.c904_answer_ds
								  )
						 , ' '
						  ) ans
					, t621.c621_patient_ide_no ||'&nbsp;' patientid, get_code_name (t621.c901_surgery_type) treatment
					, get_party_name (t621.c101_surgeon_id) surgeon
					, TO_CHAR (t621.c621_surgery_date, 'MM/DD/YYYY') surgerydate
					, TO_CHAR (t622.c622_date, 'MM/DD/YYYY') examdate, t613.c613_study_period_ds timepoint
				 FROM t623_patient_answer_list t623
					, t622_patient_list t622
					, t613_study_period t613
					, t621_patient_master t621
					, t612_study_form_list t612
					, t604_form_ques_link t604
					, t603_answer_grp t603
					, t614_study_site t614
					, t602_question_master t602
					, t605_answer_list t605
				WHERE t612.c612_study_list_id = p_studylistid
				  AND t622.c622_delete_fl = 'N'
				  AND t621.c611_study_id = t612.c611_study_id
				  AND t622.c621_patient_id = t621.c621_patient_id
				  AND t613.c613_study_period_id IN (SELECT tokenii
													  FROM v_double_in_list
													 WHERE token = 'TP')
				  AND t622.c613_study_period_id = t613.c613_study_period_id
				  AND t622.c601_form_id = t612.c601_form_id
				  AND t622.c622_patient_list_id = t623.c622_patient_list_id
				  AND t623.c602_question_id IN (SELECT tokenii
												  FROM v_double_in_list
												 WHERE token = 'QUES')
				  AND t623.c603_answer_grp_id IN (SELECT tokenii
													FROM v_double_in_list
												   WHERE token = 'ANS')
				  AND t604.c602_question_id = t623.c602_question_id
				  AND t623.c605_answer_list_id = t605.c605_answer_list_id(+)
				  AND t604.c601_form_id = t623.c601_form_id
				  AND t604.c602_question_id = t602.c602_question_id
				  AND t623.c602_question_id = t603.c602_question_id
				  AND t623.c603_answer_grp_id = t603.c603_answer_grp_id
				  AND t621.c704_account_id = t614.c704_account_id
				  AND t614.c614_study_site_id IN (SELECT tokenii
													FROM v_double_in_list
												   WHERE token = 'SITE')
				  AND t621.c621_delete_fl = 'N'
			 --and T621.C621_PATIENT_IDE_NO = '0102'
			 ORDER BY patientid, dummy_patientid, t613.c613_seq_no;
	END gm_fch_answers_details;
END gm_pkg_cr_dynamic_data_load;
/
