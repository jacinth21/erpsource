/* Formatted on 2010/06/22 11:32 (Formatter Plus v4.8.0) */
--@"C:\database\Packages\Clinical\gm_pkg_cr_pyt.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_cr_pyt
IS
	PROCEDURE gm_cr_sav_payment (
		p_studyid			  IN	   t640_clinical_payment.c611_study_id%TYPE
	  , p_studysiteid		  IN	   t640_clinical_payment.c614_study_site_id%TYPE
	  , p_requesttype		  IN	   t640_clinical_payment.c901_request_type%TYPE
	  , p_paymentto 		  IN	   t640_clinical_payment.c640_payment_to%TYPE
	  , p_payableto 		  IN	   t640_clinical_payment.c640_payable_to%TYPE
	  , p_paymentdate		  IN	   VARCHAR2
	  , p_checknumber		  IN	   t640_clinical_payment.c640_check_number%TYPE
	  , p_comments			  IN	   t640_clinical_payment.c640_comments%TYPE
	  , p_status			  IN	   t640_clinical_payment.c901_status%TYPE
	  , p_userid			  IN	   t640_clinical_payment.c640_created_by%TYPE
	  , p_paymentdetaildata   IN	   VARCHAR2
	  , p_tin_no 			  IN       t640_clinical_payment.c640_site_tin%TYPE
	  , p_tin_reason 		  IN       t640_clinical_payment.c640_tin_reason%TYPE
	  , p_paymentid 		  IN OUT   t640_clinical_payment.c640_clinical_payment_id%TYPE
	)
	AS
		v_string	   VARCHAR2 (4000) := p_paymentdetaildata;
		v_substring    VARCHAR2 (4000);
		v_paymentreason t641_clinical_payment_detail.c901_reason_for_payment%TYPE;
		v_patientid    t641_clinical_payment_detail.c621_patient_id%TYPE;
		v_studyperiodid t641_clinical_payment_detail.c901_study_period%TYPE;
		v_amount	   t641_clinical_payment_detail.c641_amount%TYPE;
		v_paymentdetailid t641_clinical_payment_detail.c641_clinical_payment_detail%TYPE;
		v_cnt		   NUMBER;
		v_email_id	   VARCHAR2 (4000);
		v_cra_email_id VARCHAR2 (4000);
		v_msg		   VARCHAR2 (4000);
		v_msg_payment  VARCHAR2 (4000);
		v_glaccountid  t641_clinical_payment_detail.c641_glaccount_id%TYPE;
		v_paymentid    t640_clinical_payment.c640_clinical_payment_id%TYPE;
		--crlf		   VARCHAR2 (4) := CHR (13) || CHR (10);
    v_patientnm varchar2(200);
    v_payableto varchar2(200);
	BEGIN
		v_paymentid := p_paymentid;
		SELECT get_user_emailid (c101_user_id)
		  INTO v_cra_email_id
		  FROM t614_study_site
		 WHERE c614_study_site_id = p_studysiteid;
    
  		v_payableto :=  p_payableto;          
    	v_email_id	:= v_cra_email_id || ',';
		v_email_id	:= v_email_id || get_rule_value ('PAYMENT_NOTIF_USERS', 'PAYMENT_NOTIF_USERS');
		v_msg		:=
			   'This email is to notify you that a request for payment was initiated on '
			|| TO_CHAR (SYSDATE, 'MM/DD/YYYY')
			|| ' for the following: '
			|| '<br><br>'
      || 'Payment Request Type: '
			|| get_code_name (p_requesttype)
			|| '<br>'
			|| 'Study Name: '
			|| get_study_name (p_studyid)
			|| '<br>'
			|| 'Payable To: '
			|| v_payableto
        	|| '<br>'
        	|| 'Comments: '
        	|| p_comments
			|| '<br>'
			|| 'TIN: '
        	|| p_tin_no;

IF p_tin_reason IS NOT NULL THEN
V_msg:=v_msg || '<br>'	
        	|| 'Reason: '
        	|| p_tin_reason;

END IF;

V_msg:=v_msg || '<br><br>';
			

		IF p_paymentid IS NULL
		THEN
			SELECT s640_clinical_payment.NEXTVAL
			  INTO p_paymentid
			  FROM DUAL;
			  
			p_paymentid := 'GM-PRQ-' || p_paymentid;

			INSERT INTO t640_clinical_payment
						(c640_clinical_payment_id, c611_study_id, c614_study_site_id, c901_request_type
					   , c640_payment_to, c640_payable_to, c640_payment_date, c640_check_number, c640_comments
					   , c901_status, c640_created_by, c640_created_date, c640_site_tin, c640_tin_reason 
						)
				 VALUES (p_paymentid, p_studyid, p_studysiteid, p_requesttype
					   , p_paymentto, p_payableto, TO_DATE (p_paymentdate, 'MM/DD/YYYY'), p_checknumber, p_comments
					   , p_status, p_userid, SYSDATE, p_tin_no, p_tin_reason
					   );
		ELSE
			UPDATE t640_clinical_payment
			   SET c901_request_type = p_requesttype
				 , c640_payment_to = p_paymentto
				 , c640_payable_to = p_payableto
				 , c640_payment_date = TO_DATE (p_paymentdate, 'MM/DD/YYYY')
				 , c640_check_number = p_checknumber
				 , c640_comments = p_comments
				 , c901_status = p_status
				 , c640_last_updated_by = p_userid
				 , c640_last_updated_date = SYSDATE
				 , c640_site_tin = p_tin_no
				 , c640_tin_reason = p_tin_reason
			 WHERE c640_clinical_payment_id = p_paymentid AND c640_void_fl IS NULL;
		END IF;

		DELETE FROM t641_clinical_payment_detail
			  WHERE c640_clinical_payment_id = p_paymentid;

		v_msg_payment := '<br>';

		WHILE INSTR (v_string, '|') <> 0
		LOOP
			v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
			v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
			--07^2009^20381^3|08^2009^20382^4|
			v_paymentreason := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
			v_glaccountid := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
			v_patientid := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
			v_studyperiodid := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
			v_amount	:= v_substring;
						
			IF (p_requesttype = '60621' OR p_requesttype = '60622' OR p_requesttype = '60623' OR p_requesttype = '60624' OR p_requesttype = '60627'
			   )
			THEN
				SELECT COUNT (*)
				  INTO v_cnt
				  FROM t641_clinical_payment_detail
				 WHERE c901_reason_for_payment = v_paymentreason
				   AND c621_patient_id = v_patientid
				   AND c901_study_period = v_studyperiodid
				   AND c641_void_fl IS NULL;

				IF (v_cnt > 0)
				THEN
					-- paymentreason,patient combination is exist already.
					raise_application_error (-20269, '');
				END IF;

				SELECT COUNT (*)
				  INTO v_cnt
				  FROM t901_code_lookup a
				 WHERE a.c901_code_id = v_paymentreason AND a.c902_code_nm_alt = p_requesttype;

				IF (v_cnt = 0)
				THEN
					-- Selected Payment Reason is not part of selected Payment Request Type.
					raise_application_error (-20270, '');
				END IF;

				IF (   (v_paymentreason = '60636' AND v_studyperiodid <> '6301')
					OR ((v_paymentreason = '60637' OR v_paymentreason = '60641') AND v_studyperiodid <> '6305')
					OR ((v_paymentreason = '60638' OR v_paymentreason = '60642') AND v_studyperiodid <> '6306')
				   )
				THEN
					-- Wrong Selection, Reason for payment must be relavant with Timepoint.
					raise_application_error (-20276, '');
				END IF;
			END IF;
			SELECT s641_clinical_payment_detail.NEXTVAL
			  INTO v_paymentdetailid
			  FROM DUAL;

			INSERT INTO t641_clinical_payment_detail
						(c641_clinical_payment_detail, c901_reason_for_payment, c621_patient_id, c901_study_period
					   , c641_amount, c641_created_by, c641_created_date, c640_clinical_payment_id,c641_glaccount_id
						)
				 VALUES (v_paymentdetailid, v_paymentreason, v_patientid, v_studyperiodid
					   , v_amount, p_userid, TRUNC (SYSDATE), p_paymentid,v_glaccountid
						);
			v_msg_payment :=
				   v_msg_payment
				
				|| 'Payment Reason: '
				|| get_code_name (v_paymentreason)
				|| '<br>'
        		|| 'GlAccountID: '
				|| v_glaccountid
				|| '<br>'
				|| 'Patient: '
				|| get_patient_ide (v_patientid)
				|| '<br>'
				|| 'Time Point: '
				|| get_code_name (v_studyperiodid)
				|| '<br>'
				|| 'Amount: '
				|| LTRIM (TO_CHAR (v_amount, '$99999999.99'))
				||'<br>'
				|| '<br>';
		END LOOP;
		v_msg_payment := v_msg_payment || 'Thank You ' || '<br>' || 'Globus Medical Clinical Affairs Team';
    
		IF p_status = 60615 
		THEN
			v_msg		:= v_msg || v_msg_payment;
			
				
			gm_com_send_html_email(v_email_id, 'Payment Request', 'test', v_msg); 
      --gm_com_send_email_prc (v_email_id, 'Payment Request', v_msg);
	END IF;
	END gm_cr_sav_payment;

	PROCEDURE gm_cr_fch_payment (
		p_paymentid 		  IN	   t640_clinical_payment.c640_clinical_payment_id%TYPE
	  , p_paymentdata		  OUT	   TYPES.cursor_type
	  , p_paymentdetaildata   OUT	   TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_paymentdata
		 FOR
			 SELECT c640_clinical_payment_id paymentrequestid, c901_request_type requesttypeid
				  , c640_payment_to paymenttoid, c640_payable_to payable
				  , TO_CHAR (c640_payment_date, 'MM/DD/YYYY') paymentdate, c640_check_number checknumber
				  , c640_comments comments, c901_status statusid, c640_site_tin tinNumber,c640_tin_reason tinReason
			   FROM t640_clinical_payment
			  WHERE c640_clinical_payment_id = p_paymentid AND c640_void_fl IS NULL;

		OPEN p_paymentdetaildata
		 FOR
			 SELECT c641_clinical_payment_detail paymentdetail, c901_reason_for_payment paymentreason
				  , get_code_name (c901_study_period) studyperiod, c621_patient_id patientid
				  , c901_study_period studyperiodid, c641_amount amount, c640_clinical_payment_id paymentid
				  , c641_glaccount_id glaccountid
			   FROM t641_clinical_payment_detail
			  WHERE c640_clinical_payment_id = p_paymentid AND c641_void_fl IS NULL;
	END gm_cr_fch_payment;

	FUNCTION get_patient_ide (
		p_patient_id   IN	t621_patient_master.c621_patient_id%TYPE
	)
		RETURN VARCHAR
	IS
		v_patient_ide  t621_patient_master.c621_patient_ide_no%TYPE;
	BEGIN
		SELECT c621_patient_ide_no
		  INTO v_patient_ide
		  FROM t621_patient_master
		 WHERE c621_patient_id = p_patient_id;

		RETURN v_patient_ide;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN '';
	END get_patient_ide;

	PROCEDURE gm_cr_fch_payment_detail_rpt (
		p_study_id			 IN 	  t640_clinical_payment.c611_study_id%TYPE
	  , p_study_site_id 	 IN 	  t640_clinical_payment.c614_study_site_id%TYPE
	  , p_requesttype		 IN 	  t640_clinical_payment.c901_request_type%TYPE
	  , p_reasonforpayment	 IN 	  t641_clinical_payment_detail.c901_reason_for_payment%TYPE
	  , p_status			 IN 	  t640_clinical_payment.c901_status%TYPE
	  , p_paymentid 		 IN 	  t640_clinical_payment.c640_clinical_payment_id%TYPE
	  , p_paymentrptcursor	 OUT	  TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_paymentrptcursor
		 FOR
			 SELECT t641.c641_clinical_payment_detail payment_detail_id, t640.c640_clinical_payment_id payment_id
				  , t640.c611_study_id study_id, get_study_name (t640.c611_study_id) study_name
				  , t640.c640_check_number check_no, t640.c614_study_site_id site_id
				  , (SELECT get_account_name (t614.c704_account_id)
					   FROM t614_study_site t614
					  WHERE t614.c614_study_site_id = t640.c614_study_site_id) site_name
				  , gm_study_period_ds (t640.c614_study_site_id) study_period_ds, t640.c640_payable_to payable_to
				  , get_code_name (t640.c901_request_type) request_type
				  , get_code_name (t641.c901_reason_for_payment) reason_for_payment, t641.c621_patient_id patient_id
				  , (SELECT t621.c621_patient_ide_no
					   FROM t621_patient_master t621
					  WHERE t621.c621_patient_id = t641.c621_patient_id) patient_ide_no
				  , get_code_name (t641.c901_study_period) study_period, t641.c641_amount amount
				  , get_user_name (t640.c640_created_by) initiated_by
				  , TO_CHAR (t640.c640_payment_date, 'MM/DD/YYYY') payment_date
				  , get_code_name (t640.c901_status) status, TO_CHAR (t640.c640_created_date, 'MM/DD/YYYY HH24:mi:ss') created_date
				  ,c641_glaccount_id glaccountid, t640.c640_site_tin site_tin
			   FROM t640_clinical_payment t640, t641_clinical_payment_detail t641
			  WHERE t640.c640_clinical_payment_id = t641.c640_clinical_payment_id
				AND t640.c640_void_fl IS NULL
				AND t641.c641_void_fl IS NULL
				AND t640.c640_clinical_payment_id = NVL (p_paymentid, t640.c640_clinical_payment_id)
				AND t640.c611_study_id = DECODE (p_study_id, '', t640.c611_study_id, p_study_id)
				AND t640.c614_study_site_id = NVL (p_study_site_id, t640.c614_study_site_id)
				AND t640.c901_request_type = NVL (p_requesttype, t640.c901_request_type)
				AND t641.c901_reason_for_payment = NVL (p_reasonforpayment, t641.c901_reason_for_payment)
				AND t640.c901_status = NVL (p_status, t640.c901_status);
	END gm_cr_fch_payment_detail_rpt;

	PROCEDURE gm_cr_fch_payment_rpt (
		p_study_id			 IN 	  t640_clinical_payment.c611_study_id%TYPE
	  , p_study_site_id 	 IN 	  t640_clinical_payment.c614_study_site_id%TYPE
	  , p_requesttype		 IN 	  t640_clinical_payment.c901_request_type%TYPE
	  , p_status			 IN 	  t640_clinical_payment.c901_status%TYPE
	  , p_paymentrptcursor	 OUT	  TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_paymentrptcursor
		 FOR
			 SELECT t640.c640_clinical_payment_id payment_id, get_study_name (t640.c611_study_id) study_name
				  , c640_check_number check_no, (SELECT get_account_name (t614.c704_account_id)
												   FROM t614_study_site t614
												  WHERE t614.c614_study_site_id = t640.c614_study_site_id) site_name
				  , t640.c640_payable_to payable_to, get_code_name (t640.c901_request_type) request_type
				  , (SELECT   SUM (t641.c641_amount)
						 FROM t641_clinical_payment_detail t641
						WHERE t640.c640_clinical_payment_id = t641.c640_clinical_payment_id
						  AND t641.c641_void_fl IS NULL
					 GROUP BY t640.c640_clinical_payment_id) amount
				  , get_user_name (t640.c640_created_by) initiated_by
				  , TO_CHAR (t640.c640_payment_date, 'MM/DD/YYYY') payment_date
				  , get_code_name (t640.c901_status) status,
				  TO_CHAR (t640.c640_created_date, 'MM/DD/YYYY HH24:mi:ss') created_date
          		, t640.c640_comments comments, t640.c640_site_tin site_tin
			   FROM t640_clinical_payment t640
			  WHERE t640.c640_void_fl IS NULL
				AND t640.c611_study_id = DECODE (p_study_id, '', t640.c611_study_id, p_study_id)
				AND t640.c614_study_site_id = NVL (p_study_site_id, t640.c614_study_site_id)
				AND t640.c901_request_type = NVL (p_requesttype, t640.c901_request_type)
				AND t640.c901_status = NVL (p_status, t640.c901_status);
	END gm_cr_fch_payment_rpt;

	PROCEDURE gm_cr_void_payment (
		p_txn_id   IN	t907_cancel_log.c907_ref_id%TYPE
	  , p_userid   IN	t102_user_login.c102_user_login_id%TYPE
	)
	AS
	BEGIN
		UPDATE t640_clinical_payment
		   SET c640_void_fl = 'Y'
			 , c640_last_updated_by = p_userid
			 , c640_last_updated_date = SYSDATE
		 WHERE c640_clinical_payment_id = p_txn_id;

		UPDATE t641_clinical_payment_detail
		   SET c641_void_fl = 'Y'
			 , c641_last_updated_by = p_userid
			 , c641_last_updated_date = SYSDATE
		 WHERE c640_clinical_payment_id = p_txn_id;
	END gm_cr_void_payment;

	PROCEDURE gm_cr_void_payment_details (
		p_txn_id   IN	t907_cancel_log.c907_ref_id%TYPE
	  , p_userid   IN	t102_user_login.c102_user_login_id%TYPE
	)
	AS
	BEGIN
		UPDATE t641_clinical_payment_detail
		   SET c641_void_fl = 'Y'
			 , c641_last_updated_by = p_userid
			 , c641_last_updated_date = SYSDATE
		 WHERE c641_clinical_payment_detail = p_txn_id;
	END gm_cr_void_payment_details;

	/**********************************************************
	 * Description : Procedure to fetch the details of a payment
	 *				 to print
	 ***********************************************************/
	PROCEDURE gm_cr_fch_payment_print (
		p_payment_id		   IN		t640_clinical_payment.c640_clinical_payment_id%TYPE
	  , p_out_cursor		   OUT		TYPES.cursor_type
	  , p_out_details_cursor   OUT		TYPES.cursor_type
	)
	AS
  v_requesttype t640_clinical_payment.c901_request_type%TYPE;
  v_patientnm varchar2(200);
  v_paymentto t640_clinical_payment.c640_payment_to%TYPE;
  v_payableto t640_clinical_payment.c640_payable_to%TYPE;
	BEGIN
    
    SELECT t640.c901_request_type, t640.c640_payment_to, t640.c640_payable_to
    INTO   v_requesttype, v_paymentto, v_payableto
    FROM  t640_clinical_payment t640 
    WHERE t640.c640_clinical_payment_id = p_payment_id AND t640.c640_void_fl IS NULL; 
    
    IF v_requesttype = 60623 THEN    
        select c621_first_name||' '||c621_last_name patientnm 
        INTO v_patientnm
        from t621_patient_master where c621_patient_id = v_paymentto AND c621_delete_fl = 'N';
        
        IF v_patientnm = ' ' THEN 
            raise_application_error (-20505, 'Patient name is not mapped for patient id '||v_payableto);
        END IF;
    END IF;    
		OPEN p_out_cursor
		 FOR
			 SELECT t640.c640_check_number checkno
												  --, get_benificiary_name (p_payment_id, t640.c640_payment_to, t640.c901_request_type) NAME
					, DECODE(t640.c901_request_type,60623,v_patientnm,t640.c640_payable_to) NAME
											   --, get_benificiary_address (t640.c640_payment_to, t640.c901_request_type) address
					, ' ' address, NVL (t640.c640_last_updated_by, t640.c640_created_by) craid
				  , get_user_name (NVL (t640.c640_last_updated_by, t640.c640_created_by)) craname
				  , TO_CHAR (SYSDATE, 'Month DD, YYYY') todate, t640.c640_check_number check_no
			   FROM t640_clinical_payment t640
			  WHERE t640.c640_clinical_payment_id = p_payment_id AND t640.c640_void_fl IS NULL;

		OPEN p_out_details_cursor
		 FOR
			 SELECT get_code_name (t641.c901_reason_for_payment) reason, t621.c621_patient_ide_no patient_id
				  , get_code_name (t641.c901_study_period) timepoint, t641.c641_amount amount
			   FROM t641_clinical_payment_detail t641, t640_clinical_payment t640, t621_patient_master t621
			  WHERE t640.c640_clinical_payment_id = t641.c640_clinical_payment_id
				AND t641.c621_patient_id = t621.c621_patient_id(+)
				AND t640.c640_clinical_payment_id = p_payment_id
				AND t640.c640_void_fl IS NULL
				AND t641.c641_void_fl IS NULL;
	END gm_cr_fch_payment_print;

	FUNCTION get_benificiary_address (
		p_payment_to	 IN   t640_clinical_payment.c640_payment_to%TYPE
	  , p_request_type	 IN   t640_clinical_payment.c901_request_type%TYPE
	)
		RETURN VARCHAR
	IS
		v_benificiary_address VARCHAR2 (1000);
	BEGIN
		IF p_request_type = 60621
		THEN   -- Site Coordinator
			SELECT t101.c101_address
			  INTO v_benificiary_address
			  FROM t101_user t101
			 WHERE t101.c101_party_id = p_payment_to;
		ELSIF p_request_type = 60622
		THEN   -- Surgeon Payment
			SELECT t101.c101_address
			  INTO v_benificiary_address
			  FROM t615_site_surgeon t615, t101_user t101
			 WHERE c615_site_surgeon_id = p_payment_to AND t615.c101_party_id = t101.c101_party_id;
		ELSIF p_request_type = 60623
		THEN   -- Patient Payment
			v_benificiary_address := NULL;
		ELSIF p_request_type = 60624
		THEN   -- Anesthesia Payment
			v_benificiary_address := NULL;
		ELSIF p_request_type = 60625
		THEN   -- Hospital Payment
			SELECT	  t704.c704_bill_add1
				   || ' '
				   || t704.c704_bill_add2
				   || ' '
				   || t704.c704_bill_city
				   || ' '
				   || t704.c704_bill_state
				   || ' '
				   || t704.c704_bill_zip_code
				   || t704.c704_bill_country
			  INTO v_benificiary_address
			  FROM t704_account t704
			 WHERE t704.c704_account_id = p_payment_to;
		ELSIF p_request_type = 60626
		THEN   -- Advertisement Payment
			v_benificiary_address := NULL;
		ELSIF p_request_type = 60627
		THEN   -- Research Fund Payment
			v_benificiary_address := NULL;
		ELSIF p_request_type = 60628
		THEN   -- Special Payment
			v_benificiary_address := NULL;
		END IF;

		RETURN v_benificiary_address;
	END get_benificiary_address;

	FUNCTION get_benificiary_name (
		p_payment_id	 IN   t640_clinical_payment.c640_clinical_payment_id%TYPE
	  , p_payment_to	 IN   t640_clinical_payment.c640_payment_to%TYPE
	  , p_request_type	 IN   t640_clinical_payment.c901_request_type%TYPE
	)
		RETURN VARCHAR
	IS
		v_benificiary_name VARCHAR2 (200);
	BEGIN
		IF p_request_type = 60621
		THEN   -- Site Coordinator
			SELECT t101.c101_user_f_name || ' ' || t101.c101_user_l_name
			  INTO v_benificiary_name
			  FROM t101_user t101
			 WHERE t101.c101_party_id = p_payment_to;
		ELSIF p_request_type = 60622
		THEN   -- Surgeon Payment
			SELECT get_party_name (t615.c101_party_id)
			  INTO v_benificiary_name
			  FROM t615_site_surgeon t615
			 WHERE c615_site_surgeon_id = p_payment_to;
		ELSIF p_request_type = 60623
		THEN   -- Patient Payment
			SELECT 'Patient ' || t621.c621_patient_ide_no
			  INTO v_benificiary_name
			  FROM t621_patient_master t621
			 WHERE t621.c621_patient_id = p_payment_to;
		ELSIF p_request_type = 60624
		THEN   -- Anesthesia Payment
			v_benificiary_name := NULL;
		ELSIF p_request_type = 60625
		THEN   -- Hospital Payment
			SELECT t704.c704_account_nm
			  INTO v_benificiary_name
			  FROM t704_account t704
			 WHERE t704.c704_account_id = p_payment_to;
		ELSIF p_request_type = 60626
		THEN   -- Advertisement Payment
			v_benificiary_name := NULL;
		ELSIF p_request_type = 60627
		THEN
			-- Research Fund Payment
			v_benificiary_name := NULL;
		ELSIF p_request_type = 60628
		THEN
			-- Special Payment
			v_benificiary_name := NULL;
		END IF;

		IF v_benificiary_name IS NULL
		THEN
			SELECT c640_payable_to
			  INTO v_benificiary_name
			  FROM t640_clinical_payment t640
			 WHERE t640.c640_clinical_payment_id = p_payment_id AND t640.c640_void_fl IS NULL;
		END IF;

		RETURN v_benificiary_name;
	END get_benificiary_name;
END gm_pkg_cr_pyt;
/
