/* Formatted on 2010/04/19 11:29 (Formatter Plus v4.8.0) */
--@"C:\database\Packages\Clinical\gm_pkg_cr_patient_log.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_cr_patient_log
IS
	PROCEDURE gm_sav_patient_log (
		p_patient_log_id   IN OUT	t630_patient_log.c630_patient_log_id%TYPE
	  , p_patient_id	   IN		t630_patient_log.c621_patient_id%TYPE
	  , p_study_period	   IN		t630_patient_log.c901_study_period%TYPE
	  , p_log_detail	   IN		t630_patient_log.c621_log_detail%TYPE
	  , p_user_id		   IN		t630_patient_log.c621_created_by%TYPE
	  , p_formid		   IN		t630_patient_log.c601_form_id%TYPE
	)
	AS
		v_patient_log_id t630_patient_log.c630_patient_log_id%TYPE;
	BEGIN
		UPDATE t630_patient_log
		   SET c621_patient_id = p_patient_id
			 , c901_study_period = p_study_period
			 , c621_log_detail = p_log_detail
			 , c621_last_updated_by = p_user_id
			 , c621_last_updated_date = SYSDATE
			 , c601_form_id = DECODE(p_formid,0,NULL,p_formid)
		 WHERE c630_patient_log_id = p_patient_log_id;

		IF (SQL%ROWCOUNT = 0)
		THEN
			SELECT s630_patient_log.NEXTVAL
			  INTO v_patient_log_id
			  FROM DUAL;

			INSERT INTO t630_patient_log
						(c630_patient_log_id, c621_patient_id, c901_study_period, c621_log_date, c621_log_detail
					   , c621_created_by, c621_created_date, c601_form_id
						)
				 VALUES (v_patient_log_id, p_patient_id, p_study_period, SYSDATE, p_log_detail
					   , p_user_id, SYSDATE, DECODE(p_formid,0,NULL,p_formid)
						);

			p_patient_log_id := v_patient_log_id;
		END IF;
	END gm_sav_patient_log;

	PROCEDURE gm_fch_patient_log_summary (
		p_patient_id		 IN 	  t630_patient_log.c621_patient_id%TYPE
	  , p_study_period		 IN 	  t630_patient_log.c901_study_period%TYPE
	  , p_formid			 IN 	  t630_patient_log.c601_form_id%TYPE
	  , p_studyid			 IN 	  t614_study_site.c611_study_id%TYPE
	  , p_studysiteid		 IN 	  t614_study_site.c614_study_site_id%TYPE
	  , p_from_dt			 IN 	  VARCHAR2
	  , p_to_dt 			 IN 	  VARCHAR2
	  , p_patient_log_dtls	 OUT	  TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_patient_log_dtls
		 FOR
			 SELECT t630.c630_patient_log_id patientlid, t621.c621_patient_ide_no ideno
				  , t621.c621_patient_id patientid, get_code_name (t630.c901_study_period) timepoint
				  , t630.c901_study_period studyperiod, TO_CHAR (t630.c621_created_date, 'mm/dd/yyyy') createddate
				  , get_user_name(t630.c621_created_by) createdby
				  , t630.c621_log_detail logdetail, DECODE(t630.c601_form_id,NULL,'ALL',get_form_ds(t630.c601_form_id,t621.c611_study_id)) frmname, t630.c601_form_id studyform
			   FROM t630_patient_log t630, t621_patient_master t621, t614_study_site t614
			  WHERE t630.c621_patient_id =
								 DECODE (p_patient_id
									   , 0, t630.c621_patient_id
									   , '', t630.c621_patient_id
									   , p_patient_id
										)
				AND t630.c901_study_period =
						  DECODE (p_study_period
								, 0, t630.c901_study_period
								, '', t630.c901_study_period
								, p_study_period
								 )
				AND (t630.c601_form_id = DECODE (p_formid, 0, t630.c601_form_id, '', t630.c601_form_id, p_formid)
				OR t630.c601_form_id  IS NULL)
		                AND TRUNC (t630.c621_created_date) >=
							    CASE
							       WHEN p_from_dt IS NOT NULL
								  THEN TO_DATE (p_from_dt, 'MM/DD/YYYY')
							       ELSE TRUNC (t630.c621_created_date)
							    END
				AND TRUNC (t630.c621_created_date) <=
							    CASE
							       WHEN p_to_dt IS NOT NULL
								  THEN TO_DATE (p_to_dt, 'MM/DD/YYYY')
							       ELSE TRUNC (t630.c621_created_date)
							    END

				AND t630.c621_patient_id = t621.c621_patient_id
				AND t621.c611_study_id = p_studyid
				AND t614.c611_study_id = t621.c611_study_id
				AND t614.c614_study_site_id = DECODE(p_studysiteid,0,t614.c614_study_site_id,p_studysiteid) -- Decode Condition added for getting the Comments Summary Report under Study Level as part of PMT-890.
				AND t614.c704_account_id = t621.c704_account_id
				AND t614.c614_void_fl IS NULL
				AND t630.c621_void_fl IS NULL;
	END gm_fch_patient_log_summary;

	PROCEDURE gm_fch_patient_log (
		p_patient_log_id	 IN 	  t630_patient_log.c630_patient_log_id%TYPE
	  , p_patient_log_dtls	 OUT	  TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_patient_log_dtls
		 FOR
			 SELECT t630.c630_patient_log_id patientlid, t630.c621_patient_id patientid
				  , t630.c901_study_period studyperiod, t630.c621_log_detail logdetail, t630.c601_form_id studyform
			   FROM t630_patient_log t630
			  WHERE t630.c630_patient_log_id =
						DECODE (p_patient_log_id
							  , 0, t630.c630_patient_log_id
							  , '', t630.c630_patient_log_id
							  , p_patient_log_id
							   );
	END gm_fch_patient_log;
END gm_pkg_cr_patient_log;
/
