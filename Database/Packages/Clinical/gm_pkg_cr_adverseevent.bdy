/* Formatted on 2011/09/09 12:00 (Formatter Plus v4.8.0) */
-- @"C:\Database\Packages\Clinical\gm_pkg_cr_adverseevent.bdy"

CREATE OR REPLACE PACKAGE BODY gm_pkg_cr_adverseevent_rpt
IS
/******************************************************************************
   NAME:	GM_PKG_CR_ADVERSEEVENT_RPT
   PURPOSE:    This procedure is used to load data for the Adverse Event report
   REVISIONS:
   Ver		  Date		  Author		Description
   ---------  ----------  ---------------  ------------------------------------
   1.0		  9/13/2007    Suchitra  1. Created this procedure.
   1.1		  02/26/2008   Richard	 1. Added Flexus changes
******************************************************************************/
	PROCEDURE gm_cr_fch_patient_adverseevent (
		p_studyid		IN		 t611_clinical_study.c611_study_id%TYPE
	  , p_studysiteid	IN		 VARCHAR2
	  , p_patientid 	IN		 t621_patient_master.c621_patient_ide_no%TYPE
	  , p_option		IN		 VARCHAR2
	  , p_timepoint 	IN		 VARCHAR2
	  , p_adverseinfo	OUT 	 TYPES.cursor_type
	)
	AS
		v_cnt		   NUMBER;
	BEGIN
		-- The input String in the form of 101.210,101.211 will be set into this temp view.
		my_context.set_my_inlist_ctx (p_studysiteid);

		SELECT COUNT (1)
		  INTO v_cnt
		  FROM v_in_list
		 WHERE token IS NOT NULL;

		IF p_timepoint IS NULL
		THEN
			OPEN p_adverseinfo
			 FOR
				 SELECT   *
					 FROM (SELECT	t622.c622_patient_event_no event_id, v621.c621_patient_id pid
								  , v621.c621_patient_ide_no pide, TO_CHAR (t622.c622_date, 'MM/DD/YYYY') examdt
								  , MAX (DECODE (t623.c603_answer_grp_id
											   , 477, c904_answer_ds
											   , 598, c904_answer_ds
											   , 5001, c904_answer_ds
                                               , 7205, DECODE(c605_answer_list_id,7255,c904_answer_ds, get_answer_list_name (c605_answer_list_id))
                                               , 7206, decode(c605_answer_list_id,7255,c904_answer_ds,get_answer_list_name (c605_answer_list_id))
                                               , 9805, get_answer_list_name (c605_answer_list_id)
                                               , 9806, decode(c605_answer_list_id,9854,c904_answer_ds,get_answer_list_name (c605_answer_list_id))
                                               -- Amnios study
                                               , 26801, c904_answer_ds
											   , ''
												)
										) adverseevent
								  , TO_CHAR (MAX (t621.c621_surgery_date), 'MM/DD/YYYY') surg_dt
								  , MAX (DECODE (t623.c603_answer_grp_id
											   , 478, c904_answer_ds
											   , 599, c904_answer_ds
											   , 5003, c904_answer_ds
                                               , 7201, c904_answer_ds
                                               , 9801, c904_answer_ds
                                               -- Amnios study
                                               , 26802, c904_answer_ds
											   , ''
												)
										) onset_dt
								   , MAX (DECODE (t623.c603_answer_grp_id
											   , 479, c904_answer_ds
											   , 600, c904_answer_ds
											   , 5004, c904_answer_ds
                                               , 7212, c904_answer_ds
                                               , 9812, c904_answer_ds
                                               -- Amnios study
                                               , 26803, c904_answer_ds
											   , ''
												)
										) resol_dt 
								  , NVL((  MAX (DECODE (t623.c603_answer_grp_id
                                               , 478, gm_pkg_cr_common.get_valid_cr_date(c904_answer_ds)
                                               , 599, gm_pkg_cr_common.get_valid_cr_date(c904_answer_ds)
                                               , 5003, gm_pkg_cr_common.get_valid_cr_date(c904_answer_ds)
                                               , 7201, gm_pkg_cr_common.get_valid_cr_date(c904_answer_ds)
                                               , 9801, gm_pkg_cr_common.get_valid_cr_date(c904_answer_ds)
                                               -- Amnios study
                                               , 26802, gm_pkg_cr_common.get_valid_cr_date(c904_answer_ds)
                                               , NULL)
                                               ) - MAX (t621.c621_surgery_date)),0 
                                         )  onset_day
								  , MAX (DECODE (t623.c603_answer_grp_id
											   , 481, get_answer_list_name (c605_answer_list_id)
											   , 602, get_answer_list_name (c605_answer_list_id)
											   , 5006, get_answer_list_name (c605_answer_list_id)
                                               , 7209, get_answer_list_name (c605_answer_list_id)
                                               , 9809, get_answer_list_name (c605_answer_list_id) 
                                               -- Amnios study
                                               ,26805, get_answer_list_name (c605_answer_list_id)
											   , NULL
												)
										) intensity
								  , MAX (DECODE (t623.c603_answer_grp_id
											   , 482, get_answer_list_name (c605_answer_list_id)
											   , 603, get_answer_list_name (c605_answer_list_id)
											   , 5007, get_answer_list_name (c605_answer_list_id)
                                               ,7207, get_answer_list_name (c605_answer_list_id)
                                               ,9806, get_answer_list_name (c605_answer_list_id)
                                               -- Amnios study
                                               ,26807, get_answer_list_name (c605_answer_list_id)
											   , NULL
												)
										) rtodevice
								 , MAX (DECODE (t623.c603_answer_grp_id ,7208, get_answer_list_name (c605_answer_list_id)
								 			   , 9808, 	get_answer_list_name (c605_answer_list_id)
								 			   -- Amnios study
								 			   ,26808, get_answer_list_name (c605_answer_list_id)
                                               , NULL
												)
										) rtoprocedure
								 , MAX (DECODE (t623.c603_answer_grp_id ,7204, get_answer_list_name (c605_answer_list_id)
                                               , NULL
												)
										) sae
								  , MAX (DECODE (t623.c603_answer_grp_id
											   , 480, get_answer_list_name (c605_answer_list_id)
											   , 601, get_answer_list_name (c605_answer_list_id)
											   , 5005, get_answer_list_name (c605_answer_list_id)
											   , 7203, get_answer_list_name (c605_answer_list_id)
											   , 9808, get_answer_list_name (c605_answer_list_id)
											   -- Amnios study
											   ,26806, get_answer_list_name (c605_answer_list_id)
											   , NULL
												)
										) uade
								  , MAX (DECODE (t623.c603_answer_grp_id
											   , 485, get_answer_list_name (c605_answer_list_id)
											   , 606, get_answer_list_name (c605_answer_list_id)
											   , 5010, get_answer_list_name (c605_answer_list_id)
											   --answer group id changed for getting dropdown value in Adverse event report when the form filled
											   , 21320, get_answer_list_name (c605_answer_list_id) 
											   , 9813, regexp_replace(substr(c904_answer_ds,1,15), '[^[:print:]]')
											   -- Amnios study
											   ,26815, get_answer_list_name (c605_answer_list_id)
											   , NULL
												)
										) outcome
								  , MAX (DECODE (t623.c603_answer_grp_id
											   , 540, get_answer_list_name (c605_answer_list_id)
											   , 755, get_answer_list_name (c605_answer_list_id)
											   , 5015, get_answer_list_name (c605_answer_list_id)
											  , 5053, get_answer_list_name (c605_answer_list_id)
											  -- Amnios study
											  , 26817, get_answer_list_name (c605_answer_list_id)
											   , NULL
												)
										) coding
								  , v621.c613_study_period_id studyperiodid
								  , MAX (DECODE (t623.c603_answer_grp_id
											   , 483, get_answer_list_name (c605_answer_list_id)
											   , 604, get_answer_list_name (c605_answer_list_id)
											   , 5008, get_answer_list_name (c605_answer_list_id)
											   --answer group id changed for getting dropdown value in Adverse event report when the form filled
											   , 21319, get_answer_list_name (c605_answer_list_id)
											   , 9811, regexp_replace(substr(c904_answer_ds,1,15), '[^[:print:]]')
											   -- Amnios study
											   , 26809, DECODE(c904_answer_ds, 'checked', 'None', NULL)
											   , 26810, DECODE(c904_answer_ds, 'checked', 'Medication', NULL)
											   , 26811, DECODE(c904_answer_ds, 'checked', 'Physical therapy', NULL)
											   , 26812, DECODE(c904_answer_ds, 'checked', 'Injection therapy', NULL)
											   , 26813, DECODE(c904_answer_ds, 'checked', 'Surgery', NULL)
											   , 26814, c904_answer_ds, NULL
												)
										) actiontaken
								  , v621.c621_surgery_nm treatment 
								  -- Amnios study
								  -- 263 (AM3) 26305 - Affected Foot
								  , DECODE (v621.c611_study_id, 'GPR009', gm_pkg_cr_adverseevent_rpt.get_affected_foot_count (v621.c621_patient_id, 263, 26305), NULL) affected_foot
								  , MAX (DECODE (t623.c603_answer_grp_id
											   , 26818, get_answer_list_name (c605_answer_list_id), NULL )) cec_review
							   FROM t622_patient_list t622, t623_patient_answer_list t623, v621_patient_study_list v621 , t621_patient_master t621
							  WHERE t622.c601_form_id IN (14, 213, 50 ,72 ,98, 268)
								AND t622.c622_patient_list_id = t623.c622_patient_list_id
								AND v621.c621_patient_id = t622.c621_patient_id
								AND NVL (t622.c622_delete_fl, 'N') = 'N'
								AND NVL (t623.c623_delete_fl, 'N') = 'N'
								AND t621.c621_patient_id = t622.c621_patient_id
								AND v621.c612_study_list_id IN (10, 2511, 3510 ,7001 ,9601, 26303)
								AND (t623.c904_answer_ds IS NOT NULL OR NVL (t623.c605_answer_list_id, 0) <> 0)
								AND v621.c611_study_id = p_studyid
								AND (	('-999' = DECODE (v_cnt, 0, '-999', '-9999'))
									 OR v621.c614_study_site_id IN (SELECT *
																	  FROM v_in_list)
									)
								AND v621.c621_patient_ide_no =
													   DECODE (p_patientid
															 , NULL, v621.c621_patient_ide_no
															 , p_patientid
															  )
						   GROUP BY v621.c621_patient_id
								  , v621.c621_patient_ide_no
								  , t622.c622_date
								  , v621.c613_study_period_id
								  , t622.c622_patient_event_no
								  , v621.c621_surgery_nm
								  , v621.c611_study_id) adv_event
								  
					WHERE NVL (adv_event.resol_dt, '01/01/0001') =
									DECODE (p_option
										  , 'Dashboard', '01/01/0001'
										  , NVL (adv_event.resol_dt, '01/01/0001')
										   )
				 ORDER BY adv_event.pide, adv_event.event_id;
		ELSE
			OPEN p_adverseinfo
			 FOR
				 SELECT   *
					 FROM (SELECT t1.event_id, pid, t1.pide, examdt, adverseevent, surg_dt, onset_dt, resol_dt
								, onset_day, intensity, rtodevice, uade, outcome, coding, actiontaken, treatment
							 FROM (SELECT	t622.c622_patient_event_no event_id, v621.c621_patient_id pid
										  , v621.c621_patient_ide_no pide, TO_CHAR (t622.c622_date
																				  , 'MM/DD/YYYY') examdt
										  , MAX (DECODE (t623.c603_answer_grp_id
													   , 477, c904_answer_ds
													   , 598, c904_answer_ds
													   , 5001, c904_answer_ds
													   , ''
														)
												) adverseevent
										  , TO_CHAR (MAX (t621.c621_surgery_date), 'MM/DD/YYYY') surg_dt
										  , MAX (DECODE (t623.c603_answer_grp_id
													   , 478, c904_answer_ds
													   , 599, c904_answer_ds
													   , 5003, c904_answer_ds
													   , ''
														)
												) onset_dt
										  , MAX (DECODE (t623.c603_answer_grp_id
													   , 479, c904_answer_ds
													   , 600, c904_answer_ds
													   , 5004, c904_answer_ds
													   , ''
														)
												) resol_dt
										  , MAX (DECODE (t623.c603_answer_grp_id
													   , 478, NVL( (  gm_pkg_cr_common.get_valid_cr_date(c904_answer_ds)
															   - v621.c622_surgery_date
														  ), 0)
													   , 599, NVL( (  gm_pkg_cr_common.get_valid_cr_date(c904_answer_ds)
															   - v621.c622_surgery_date
														  )  , 0)
													   , 5003, NVL( (  gm_pkg_cr_common.get_valid_cr_date(c904_answer_ds)
																- v621.c622_surgery_date
														  ),0)
													   , 0
														)
												) onset_day
										  , MAX (DECODE (t623.c603_answer_grp_id
													   , 481, get_answer_list_name (c605_answer_list_id)
													   , 602, get_answer_list_name (c605_answer_list_id)
													   , 5006, get_answer_list_name (c605_answer_list_id)
													   , NULL
														)
												) intensity
										  , MAX (DECODE (t623.c603_answer_grp_id
													   , 482, get_answer_list_name (c605_answer_list_id)
													   , 603, get_answer_list_name (c605_answer_list_id)
													   , 5007, get_answer_list_name (c605_answer_list_id)
													   , NULL
														)
												) rtodevice
										  , MAX (DECODE (t623.c603_answer_grp_id
													   , 480, get_answer_list_name (c605_answer_list_id)
													   , 601, get_answer_list_name (c605_answer_list_id)
													   , 5005, get_answer_list_name (c605_answer_list_id)
													   , NULL
														)
												) uade
										  , MAX (DECODE (t623.c603_answer_grp_id
													   , 485, get_answer_list_name (c605_answer_list_id)
													   , 606, get_answer_list_name (c605_answer_list_id)
													   , 5010, get_answer_list_name (c605_answer_list_id)
													   , NULL
														)
												) outcome
										  , MAX (DECODE (t623.c603_answer_grp_id
													   , 540, get_answer_list_name (c605_answer_list_id)
													   , 755, get_answer_list_name (c605_answer_list_id)
													   , 5015, get_answer_list_name (c605_answer_list_id)
													   , NULL
														)
												) coding
										  , v621.c613_study_period_id studyperiodid
										  , MAX (DECODE (t623.c603_answer_grp_id
													   , 483, get_answer_list_name (c605_answer_list_id)
													   , 604, get_answer_list_name (c605_answer_list_id)
													   , 5008, get_answer_list_name (c605_answer_list_id)
													   , NULL
														)
												) actiontaken
										  , v621.c621_surgery_nm treatment
									   FROM t622_patient_list t622
										  , t623_patient_answer_list t623
										  , v621_patient_study_list v621
										  , t621_patient_master t621
									  WHERE t622.c601_form_id IN (14, 213, 50 ,72)
										AND t622.c622_patient_list_id = t623.c622_patient_list_id
										AND v621.c621_patient_id = t622.c621_patient_id
										AND NVL (t622.c622_delete_fl, 'N') = 'N'
										AND NVL (t623.c623_delete_fl, 'N') = 'N'
										AND t621.c621_patient_id = t622.c621_patient_id
										AND v621.c612_study_list_id IN (10, 2511, 3510)
										AND (t623.c904_answer_ds IS NOT NULL OR NVL (t623.c605_answer_list_id, 0) <> 0)
										AND v621.c611_study_id = p_studyid
										AND (	('-999' = DECODE (v_cnt, 0, '-999', '-9999'))
											 OR v621.c614_study_site_id IN (SELECT *
																			  FROM v_in_list)
											)
										AND v621.c621_patient_ide_no =
													   DECODE (p_patientid
															 , NULL, v621.c621_patient_ide_no
															 , p_patientid
															  )
								   GROUP BY v621.c621_patient_id
										  , v621.c621_patient_ide_no
										  , t622.c622_date
										  , v621.c613_study_period_id
										  , t622.c622_patient_event_no
										  , v621.c621_surgery_nm) t1
								, (SELECT t622.c622_patient_event_no event_id, v621.c621_patient_ide_no pide
									 FROM t622_patient_list t622
										, t623_patient_answer_list t623
										, v621_patient_study_list v621
									WHERE t622.c601_form_id IN (14, 213, 50 ,72, 268)
									  AND v621.c612_study_list_id IN (5, 2505)
									  AND t622.c622_patient_list_id = t623.c622_patient_list_id
									  AND t622.c621_patient_id = t623.c621_patient_id
									  AND v621.c621_patient_id = t622.c621_patient_id
									  AND NVL (t622.c622_delete_fl, 'N') = 'N'
									  AND NVL (t623.c623_delete_fl, 'N') = 'N'
									  AND v621.c621_patient_ide_no = p_patientid
									  AND (   t623.c603_answer_grp_id = 478
										   OR t623.c603_answer_grp_id = 599
										   OR t623.c603_answer_grp_id = 5003
										  )
									  AND ( 	 v621.c621_exp_date_start
											   - TO_NUMBER (get_rule_value (p_studyid, CONCAT (p_timepoint, '-S'))) <=
																				  gm_pkg_cr_common.get_valid_cr_date(c904_answer_ds)
										   AND	 v621.c621_exp_date_final
											   + TO_NUMBER (get_rule_value (p_studyid, CONCAT (p_timepoint, '-E'))) >=
																				  gm_pkg_cr_common.get_valid_cr_date(c904_answer_ds)
										  )
									  AND v621.c613_study_period_ds = p_timepoint) t2
							WHERE t1.pide = t2.pide AND t1.event_id = t2.event_id) adv_event
					WHERE NVL (adv_event.resol_dt, '01/01/0001') =
									DECODE (p_option
										  , 'Dashboard', '01/01/0001'
										  , NVL (adv_event.resol_dt, '01/01/0001')
										   )
				 ORDER BY adv_event.pide, adv_event.event_id;
		END IF;
	END gm_cr_fch_patient_adverseevent;
	
	-- PC-1807 Reflect study - Adverse Event report
	
	/*******************************************************
	* Description : Function used to get the AE report data for Reflect account
	* Parameters  : 1. Study Id - Id of the selected Study, account, time point
	********************************************************/
	
	PROCEDURE gm_cr_fch_reflect_patient_adverseevent (
		p_studyid		IN		 t611_clinical_study.c611_study_id%TYPE
	  , p_studysiteid	IN		 VARCHAR2
	  , p_patientid 	IN		 t621_patient_master.c621_patient_ide_no%TYPE
	  , p_option		IN		 VARCHAR2
	  , p_timepoint 	IN		 VARCHAR2
	  , p_adverseinfo	OUT 	 TYPES.cursor_type
	)
	AS
		v_cnt		   NUMBER;
	BEGIN
		-- The input String in the form of 101.210,101.211 will be set into this temp view.
		my_context.set_my_inlist_ctx (p_studysiteid);

		SELECT COUNT (1)
		  INTO v_cnt
		  FROM v_in_list
		 WHERE token IS NOT NULL;

			OPEN p_adverseinfo
			 FOR
				 SELECT   *
					 FROM (SELECT	t622.c622_patient_event_no event_id, v621.c621_patient_id pid
								  , v621.c621_patient_ide_no pide, TO_CHAR (t622.c622_date, 'MM/DD/YYYY') examdt
								  , MAX (DECODE (t623.c603_answer_grp_id, 27901, c904_answer_ds, NULL)) adverseevent
								  , TO_CHAR (MAX (t621.c621_surgery_date), 'MM/DD/YYYY') surg_dt
								  , MAX (DECODE (t623.c603_answer_grp_id, 27902,c904_answer_ds, NULL)) onset_dt
								  , MAX (DECODE (t623.c603_answer_grp_id, 27903,c904_answer_ds, NULL)) resol_dt 
								  , MAX (DECODE (t623.c603_answer_grp_id, 27902,NVL((  gm_pkg_cr_common.get_valid_cr_date(c904_answer_ds)- t621.c621_surgery_date), 0 )))  onset_day
								  , MAX (DECODE (t623.c603_answer_grp_id, 27904, get_answer_list_name (c605_answer_list_id), NULL)) timing
								  , MAX (DECODE (t623.c603_answer_grp_id, 27905, get_answer_list_name (c605_answer_list_id), NULL)) over_under_correction
                                  , MAX (DECODE (t623.c603_answer_grp_id, 27906, get_answer_list_name (c605_answer_list_id), NULL)) intensity
                                  , MAX (DECODE (t623.c603_answer_grp_id, 27907, get_answer_list_name (c605_answer_list_id), NULL)) rtodevice
                                  , MAX (DECODE (t623.c603_answer_grp_id, 27908, get_answer_list_name (c605_answer_list_id), NULL)) rtosurgery
								  , NULL rtoprocedure
								  , NULL sae
								  , NULL uade
								  , NULL outcome
								   , MAX (DECODE (t623.c603_answer_grp_id, 27914, get_answer_list_name (c605_answer_list_id), NULL)) coding
								  , v621.c613_study_period_id studyperiodid
								    , MAX (DECODE (t623.c603_answer_grp_id, 27909, DECODE (c904_answer_ds, 'checked', 'None', NULL)
								  		, 27910, DECODE (c904_answer_ds, 'checked',	'Medication', NULL)
								  		, 27911, DECODE (c904_answer_ds, 'checked', 'Surgery at treated levels', NULL)
								  		, 27912, DECODE (c904_answer_ds, 'checked', 'Others', NULL))) actiontaken
								  , v621.c621_surgery_nm treatment 
								  , MAX (DECODE (t623.c603_answer_grp_id, 27915, get_answer_list_name (c605_answer_list_id), NULL)) cec_review
                                  , MAX (DECODE (t623.c603_answer_grp_id, 27918, get_answer_list_name (c605_answer_list_id), NULL)) re_review_cec
						   FROM t622_patient_list t622, t623_patient_answer_list t623, v621_patient_study_list v621 , t621_patient_master t621
						   WHERE t622.c601_form_id = 279
								  AND t622.c622_patient_list_id = t623.c622_patient_list_id
								  AND v621.c621_patient_id = t622.c621_patient_id
								  AND NVL (t622.c622_delete_fl, 'N') = 'N'
								  AND NVL (t623.c623_delete_fl, 'N') = 'N'
								  AND t621.c621_patient_id = t622.c621_patient_id
								  AND v621.c612_study_list_id = 27202
								  AND (t623.c904_answer_ds IS NOT NULL OR NVL (t623.c605_answer_list_id, 0) <> 0)
								  AND v621.c611_study_id = p_studyid
								  AND (('-999' = DECODE (v_cnt, 0, '-999', '-9999')) OR v621.c614_study_site_id IN (SELECT * FROM v_in_list))
								  AND v621.c621_patient_ide_no = DECODE (p_patientid, NULL, v621.c621_patient_ide_no, p_patientid)
						   GROUP BY v621.c621_patient_id
								  , v621.c621_patient_ide_no
								  , t622.c622_date
								  , v621.c613_study_period_id
								  , t622.c622_patient_event_no
								  , v621.c621_surgery_nm
								  , v621.c611_study_id) adv_event
								  
					WHERE NVL (adv_event.resol_dt, '01/01/0001') = DECODE (p_option, 'Dashboard', '01/01/0001', NVL (adv_event.resol_dt, '01/01/0001'))
			ORDER BY adv_event.pide, adv_event.event_id;
			
	END gm_cr_fch_reflect_patient_adverseevent;

	 /*******************************************************
	* Description : Function used to get the AE date for respective account and time point
	* Parameters  : 1. Study Id - Id of the selected Study, account, time point
	********************************************************/
	FUNCTION get_ae_count (
		p_studyid		IN	 t611_clinical_study.c611_study_id%TYPE
	  , p_studysiteid	IN	 t614_study_site.c614_study_site_id%TYPE
	  , p_studyprd_ds	IN	 t613_study_period.c613_study_period_ds%TYPE
	  , p_patientid	   IN	t621_patient_master.c621_patient_id%Type
	)
		RETURN NUMBER
	AS
		v_ae_count	   VARCHAR2 (20);
		v_ae_anslistid VARCHAR2 (50);
	BEGIN
		SELECT SUM (ae_cnt.tmp_cnt)
	       INTO v_ae_count
	       FROM
	        (SELECT DISTINCT ae_pt.patient_ide_no, v621.c613_study_period_ds study_prd_ds, v621.c613_seq_no seq_no,
	            CASE WHEN TRUNC (t629.c629_surgery_date) < TRUNC (v621.c621_exp_date_final) THEN 1 ELSE 0 END tmp_cnt
	           FROM t629_surgical_intervention t629, v621_patient_study_list v621,
	            (SELECT t621.c621_patient_id patient_id, t621.c621_patient_ide_no patient_ide_no, t622.c622_patient_list_id
	                pt_listid
	               FROM t621_patient_master t621, t622_patient_list t622
	              WHERE t622.c621_patient_id = t621.c621_patient_id
	                AND t621.c611_study_id   = p_studyid
	                AND EXISTS
	                (SELECT t629.c622_patient_list_id
	                   FROM t629_surgical_intervention t629
	                  WHERE t629.c622_patient_list_id = t622.c622_patient_list_id
	                    AND t629.c621_patient_id      = t621.c621_patient_id
	                    AND c629_surgery_date         =
	                    (SELECT MIN (c629_surgery_date)
	                       FROM t629_surgical_intervention t629_1
	                      WHERE t629_1.c621_patient_id = t629.c621_patient_id
	                        AND t629_1.c629_delete_fl='N' 
                            AND t629_1.c629_verify_fl='Y'
	                    )
	                    AND t629.c629_delete_fl='N' 
                        AND t629.c629_verify_fl='Y'
	                )
	            ) ae_pt
	          WHERE t629.c621_patient_id               = ae_pt.patient_id
	            AND v621.c621_patient_id           = t629.c621_patient_id
	            AND v621.c613_study_period_ds NOT IN ('Preoperative', 'Time of Surgery')
	            AND v621.c611_study_id             = p_studyid --   p_studyid
	            AND t629.c622_patient_list_id      = ae_pt.pt_listid
	            AND v621.c614_study_site_id        = NVL (p_studysiteid, v621.c614_study_site_id) --p_studysiteid
	            AND v621.c613_study_period_ds      = p_studyprd_ds                                --p_studyprd_ds
	            AND v621.c621_patient_id           = p_patientid                                  -- p_patientid
	            AND c629_surgery_date              =
	            (SELECT MIN (c629_surgery_date)
	               FROM t629_surgical_intervention t629_1
	              WHERE t629_1.c621_patient_id = t629.c621_patient_id
	                AND t629_1.c629_delete_fl='N' 
                    AND t629_1.c629_verify_fl='Y'
	            )
	           AND t629.c629_delete_fl='N' 
               AND t629.c629_verify_fl='Y'
	       ORDER BY ae_pt.patient_ide_no, v621.c613_seq_no
	        ) ae_cnt
	   GROUP BY ae_cnt.study_prd_ds, ae_cnt.seq_no;
	RETURN v_ae_count;
	EXCEPTION
		WHEN OTHERS
		THEN
			RETURN 0;
	END get_ae_count;
	
	
	
	 /*******************************************************
	* Description : Function used to get the affected foot count details (Amnios study)
	* Parameters  : 1. Study Id - Id of the selected Study, account, time point
	********************************************************/
	FUNCTION get_affected_foot_count (
		p_patient_id IN t622_patient_list.c621_patient_id%TYPE,
		p_form_id IN t622_patient_list.c601_form_id%TYPE,
	   p_answer_grp_id	IN	 t623_patient_answer_list.c603_answer_grp_id%TYPE
	)
		RETURN VARCHAR2
	AS
		v_patient_list_id t622_patient_list.c622_patient_list_id%TYPE;
		v_affected_foot_val VARCHAR2 (4000);
	BEGIN
		
		SELECT C622_PATIENT_LIST_ID INTO v_patient_list_id
		FROM T622_PATIENT_LIST where C621_PATIENT_ID = p_patient_id and C601_FORM_ID = p_form_id
		and C622_DELETE_FL = 'N';
		
		--
		SELECT get_answer_list_name (c605_answer_list_id)
		INTO v_affected_foot_val
   FROM t623_patient_answer_list
  WHERE C622_PATIENT_LIST_ID = v_patient_list_id
    AND C603_ANSWER_GRP_ID   = p_answer_grp_id;
		
	RETURN v_affected_foot_val;
	EXCEPTION
		WHEN OTHERS
		THEN
			RETURN NULL;
	END get_affected_foot_count;
END gm_pkg_cr_adverseevent_rpt;
/
