
--  @"c:\database\packages\clinical\gm_pkg_cr_outcome_rpt.bdy";

create or replace
PACKAGE BODY gm_pkg_cr_outcome_rpt
IS
/*******************************************************
 * Purpose: Package holds Procs for Outcome reports
 *
 *******************************************************/
--
/*******************************************************
 * Purpose: This Procedure is used to fetch the expected and actual follow up by treatment
 *******************************************************/
--
	PROCEDURE gm_cr_fch_ndi_score (
	    p_studyid		IN	 t611_clinical_study.c611_study_id%TYPE
	  , p_studysiteid	IN	 VARCHAR2
	  , p_out_header	OUT 	 TYPES.cursor_type
	  , p_out_details	OUT 	 TYPES.cursor_type
	  , p_score		IN	 VARCHAR2
	)
	AS
		v_form_id		t622_patient_list.c601_form_id%TYPE;
		v_question_id		t623_patient_answer_list.c602_question_id%TYPE;
  		v_study_listid		t612_study_form_list.c612_study_list_id%TYPE;
		v_ans_grpid		t623_patient_answer_list.c603_answer_grp_id%TYPE;
		v_study_prdid1		NUMBER;
		v_study_prdid2		NUMBER;
	BEGIN
		-- The input String in the form of 101.210,101.211 will be set into this temp view.
		my_context.set_my_inlist_ctx (p_studysiteid);
		
		-- STUDY_LIST_ID_GRP, FORM_ID,QUESTION_ID use having two values for ACADIA in single score id.
		SELECT t901.c902_code_nm_alt, t901.c901_code_nm
		--,get_rule_value(p_score,'STUDY_LIST_ID_GRP')
		, get_rule_value(p_score,'ANSWER_GRP_ID_GRP')
		  INTO v_form_id, v_question_id--, v_study_listid
		  , v_ans_grpid
		  FROM t901_code_lookup t901
		WHERE t901.c901_code_id = p_score;

		IF p_score = '60863' or p_score = '61863' THEN
		  v_study_prdid1 := 68;
 		  v_study_prdid2 := 26;
		ELSE
		  v_study_prdid1 := 68;
 		  v_study_prdid2 := 0;
		END IF;
		OPEN p_out_header
		 FOR
			 SELECT DISTINCT	DECODE (c613_study_period_ds, 'Preoperative', 'PreOp', c613_study_period_ds)
							 || '  <br>Avg' period
						   , c613_seq_no
						FROM t613_study_period
					   WHERE c612_study_list_id IN (10, 5) AND c613_study_period_id NOT IN (v_study_prdid1, v_study_prdid2)
			 UNION
			 SELECT DISTINCT	DECODE (c613_study_period_ds, 'Preoperative', 'PreOp', c613_study_period_ds)
							 || ' <br>'
							 || to_char(UNISTR('\00B1'))
							 || 'SD' period
						   , c613_seq_no
						FROM t613_study_period
					   WHERE c612_study_list_id IN (10, 5) AND c613_study_period_id NOT IN (v_study_prdid1, v_study_prdid2)
					ORDER BY c613_seq_no;

		OPEN p_out_details
		 FOR
			 SELECT   'A'||ROWNUM ID, NVL (stype, NVL (sgroup, 'ALL PATIENTS')) NAME
					, DECODE (period, 'Preoperative', 'PreOp', period) period, average val
					, DECODE (grpingid, 0, DECODE (sgroup, NULL, 2, grpingid), grpingid) groupingcount, stddv, sseqno
					, pseqno, NVL (sgroup, DECODE (grpingid, 0, 'XXXX')) sgroup
				 FROM (SELECT	t901.c902_code_nm_alt sgroup, t901.c901_code_nm stype, c613_study_period_ds period
							  , c613_seq_no pseqno, format_number (AVG (TO_NUMBER (t623.c904_answer_ds) ), 1) average
							  , to_char(UNISTR('\00B1')) || To_Char(Format_Number (Stddev (To_Number (T623.C904_Answer_Ds) ), 1),'990D0') Stddv
							  , GROUPING_ID (t901.c901_code_nm, t901.c902_code_nm_alt) grpingid
							  , NVL (t901.c901_code_seq_no, 0) sseqno
						   FROM t622_patient_list t622, v621_patient_study_list v621, t901_code_lookup t901, t623_patient_answer_list t623
						  WHERE v621.c621_patient_id = t622.c621_patient_id
							AND v621.c901_surgery_type = t901.c901_code_id
							AND v621.c613_study_period_id = t622.c613_study_period_id
							AND c612_study_list_id IN (SELECT c612_study_list_id FROM t612_study_form_list WHERE c601_form_id in (SELECT to_number(c906_rule_value) FROM t906_rules WHERE c906_rule_id = p_score AND c906_rule_grp_id='FORM_ID'))
							AND t622.c622_patient_list_id = t623.c622_patient_list_id
							AND NVL(t622.C622_DELETE_FL,'N')='N'
							AND NVL(t623.C623_DELETE_FL,'N')='N'
							AND t623.c602_question_id in (SELECT to_number(c906_rule_value) FROM t906_rules WHERE c906_rule_id = p_score AND c906_rule_grp_id='QUESTION_ID')
							AND to_char(t623.c601_form_id) IN (SELECT to_number(c906_rule_value) FROM t906_rules WHERE c906_rule_id = p_score AND c906_rule_grp_id='FORM_ID')
							AND v621.c611_study_id = p_studyid
							AND v621.c621_surgery_comp_fl = 'Y'
							AND t623.c603_answer_grp_id IN (SELECT c603_answer_grp_id FROM t603_answer_grp WHERE c602_question_id IN (SELECT to_number(c906_rule_value) FROM t906_rules WHERE c906_rule_id = p_score AND c906_rule_grp_id='QUESTION_ID'))
							AND v621.c614_study_site_id IN (SELECT *
															  FROM v_in_list)
					   GROUP BY c613_study_period_ds
							  , c613_seq_no
							  , ROLLUP (t901.c902_code_nm_alt, (t901.c901_code_seq_no, t901.c901_code_nm))
					   ORDER BY period, c613_seq_no)
				WHERE sgroup IS NOT NULL OR stype IS NOT NULL OR grpingid <> 2
			 ORDER BY sgroup, groupingcount, sseqno, pseqno;
	END gm_cr_fch_ndi_score;

/*******************************************************
 * Purpose: This Procedure is used to fetch the NDI per improve
 *******************************************************/
--
	PROCEDURE gm_cr_fch_ndi_per_improve (
	    p_studyid		IN	 t611_clinical_study.c611_study_id%TYPE
	  , p_studysiteid	IN	 VARCHAR2
  	  , p_score		IN	 VARCHAR2
	  , p_out_header	OUT 	 TYPES.cursor_type
	  , p_out_details	OUT 	 TYPES.cursor_type
	)
	AS
		v_form_id		t622_patient_list.c601_form_id%TYPE;
		v_question_id		t623_patient_answer_list.c602_question_id%TYPE;
  		v_study_listid		t612_study_form_list.c612_study_list_id%TYPE;
	    	v_study_prdid_in	v621_patient_study_list.c613_study_period_id%TYPE;
		v_study_prdid_notin	v621_patient_study_list.c613_study_period_id%TYPE;
		v_ans_grpid		t623_patient_answer_list.c603_answer_grp_id%TYPE;
		v_ans_lstid_in		t623_patient_answer_list.c605_answer_list_id%TYPE;
		v_ans_lstid_notin	t623_patient_answer_list.c605_answer_list_id%TYPE;
		v_cval			NUMBER;
	BEGIN
		-- The input String in the form of 101.210,101.211 will be set into this temp view.
		my_context.set_my_inlist_ctx (p_studysiteid);
		
		-- STUDY_LIST_ID_GRP, FORM_ID,QUESTION_ID  having two values in T906 for ACADIA in single score id.
		SELECT t901.c902_code_nm_alt, t901.c901_code_nm,
		     --  get_rule_value(p_score,'STUDY_LIST_ID_GRP'),
		       get_rule_value(p_score,'STUDY_PRD_ID_IN_GRP'),
		       get_rule_value(p_score,'STY_PRD_ID_NOTIN_GRP'),
		       get_rule_value(p_score,'ANSWER_GRP_ID_GRP'),
		       get_rule_value(p_score,'ANS_LIST_ID_IN_GRP'),
		       get_rule_value(p_score,'ANS_LISTID_NOTIN_GRP'),
		       TO_NUMBER(get_rule_value(p_score,'COMPARE_VAL'))
		  INTO v_form_id, v_question_id,-- v_study_listid,
		       v_study_prdid_in, v_study_prdid_notin,
		       v_ans_grpid, v_ans_lstid_in, v_ans_lstid_notin,
		       v_cval
		  FROM t901_code_lookup t901
		 WHERE t901.c901_code_id = p_score;

		OPEN p_out_header
		 FOR
			 SELECT DISTINCT c613_study_period_ds || ' <BR> n' period, c613_seq_no, 1 a
						FROM t613_study_period
					   WHERE c612_study_list_id IN (10, 5) AND c613_study_period_id NOT IN (26, 68)
			 UNION
			 SELECT DISTINCT c613_study_period_ds || ' <BR> N' period, c613_seq_no, 2 a
						FROM t613_study_period
					   WHERE c612_study_list_id IN (10, 5) AND c613_study_period_id NOT IN (26, 68)
			 UNION
			 SELECT DISTINCT c613_study_period_ds || ' <BR> n/N%' period, c613_seq_no, 3 a
						FROM t613_study_period
					   WHERE c612_study_list_id IN (10, 5) AND c613_study_period_id NOT IN (26, 68)
					ORDER BY c613_seq_no, a;

		OPEN p_out_details
		 FOR
			 SELECT DISTINCT cid ID, NVL (stype, NVL (sgroup, 'ALL PATIENTS')) NAME, speriod, patgrowth
						   , DECODE (grpingid, 0, DECODE (sgroup, NULL, 2, grpingid), grpingid) groupingcount
						   , patcount, pat_g25, avgimp_g25, avgimp, sseqno, sno pseqno
						   , NVL (sgroup, DECODE (grpingid, 0, 'XXXX')) sgroup
						FROM (SELECT   NVL (t901.c901_code_id
										  , DECODE (t901.c902_code_nm_alt, 'ALL SECURE-C', '6211', '0')
										   ) cid
									 , t901.c902_code_nm_alt sgroup, t901.c901_code_nm stype
									 , c613_study_period_ds speriod, c613_seq_no sno, SUM (1) totalpat
									 , GROUPING_ID (t901.c901_code_nm, t901.c902_code_nm_alt) grpingid
									 , SUM (DECODE (SIGN (imp - v_cval), -1, 0, 1)) pat_g25, SUM (1) patcount
									 , ((SUM (DECODE (SIGN (imp - v_cval), -1, 0, 1)) / SUM (1)) * 100) avgpat_g25
									 , (SUM (preopcal_value) - SUM (curcalvalue)) / SUM (preopcal_value) * 100 avgimp
									 ,	 SUM (DECODE (SIGN (imp - v_cval), -1, 0, imp))
									   / SUM (DECODE (SIGN (imp - v_cval), -1, NULL, 1)) avgimp_g25
									 , (SUM (DECODE (SIGN (imp - v_cval), -1, 0, 1)) / SUM (1) * 100) patgrowth
									 , NVL (t901.c901_code_seq_no, 0) sseqno
								  FROM (SELECT ndivalue.c621_patient_id, ndivalue.c901_surgery_type pid
											 , ndivalue.c613_study_period_ds, c613_seq_no, ndivalue.curcalvalue
											 , ndipreopt.preopcal_value
											 , DECODE (ndivalue.c621_patient_id
													 , NULL, (	(  (ndipreopt.preopcal_value - ndivalue.curcalvalue)
																 / ndipreopt.preopcal_value
																)
															  * 100
															)
														|| ''
													 , (  DECODE (ndivalue.curcalvalue
																, NULL, 0
																,	(ndipreopt.preopcal_value - ndivalue.curcalvalue)
																  / ndipreopt.preopcal_value
																 )
														* 100
													   )
													  ) imp
										  FROM (SELECT v621.c621_patient_id, c901_surgery_type, c613_study_period_ds
													 , c613_seq_no, TO_NUMBER (t623.c904_answer_ds)  curcalvalue
												  FROM t622_patient_list t622, v621_patient_study_list v621 , t623_patient_answer_list t623
												 WHERE v621.c621_patient_id = t622.c621_patient_id
												   AND v621.c613_study_period_id = t622.c613_study_period_id
												   AND c612_study_list_id IN (SELECT to_number(C906_RULE_VALUE) FROM T906_RULES WHERE C906_RULE_GRP_ID='STUDY_LIST_ID_GRP' and C906_RULE_ID = p_score)
												   AND v621.c613_study_period_id NOT IN (v_study_prdid_notin)
												   AND t622.c622_patient_list_id = t623.c622_patient_list_id
												   AND NVL(t622.C622_DELETE_FL,'N')='N'
												   AND NVL(t623.C623_DELETE_FL,'N')='N'
												   AND t622.c601_form_id IN(SELECT to_number(c906_rule_value) FROM t906_rules WHERE c906_rule_id = p_score AND c906_rule_grp_id='FORM_ID')
												   AND t623.c602_question_id IN (SELECT to_number(c906_rule_value) FROM t906_rules WHERE c906_rule_id = p_score AND c906_rule_grp_id='QUESTION_ID')
												   AND v621.c621_surgery_comp_fl = 'Y') ndivalue
											 , (SELECT v621.c621_patient_id
													 , TO_NUMBER (t623.c904_answer_ds)  preopcal_value
													 , v621.c611_study_id, v621.c614_study_site_id
												  FROM t622_patient_list t622, v621_patient_study_list v621, t623_patient_answer_list t623
												 WHERE v621.c621_patient_id = t622.c621_patient_id
												   AND v621.c613_study_period_id = t622.c613_study_period_id
												   AND c612_study_list_id IN (SELECT to_number(C906_RULE_VALUE) FROM T906_RULES WHERE C906_RULE_GRP_ID='STUDY_LIST_ID_GRP' and C906_RULE_ID = p_score)
												   AND v621.c613_study_period_id IN (v_study_prdid_in)
												   AND t622.c622_patient_list_id = t623.c622_patient_list_id
												   AND NVL(t622.C622_DELETE_FL,'N')='N'
												   AND NVL(t623.C623_DELETE_FL,'N')='N'
   												   AND t622.c601_form_id IN(SELECT to_number(c906_rule_value) FROM t906_rules WHERE c906_rule_id = p_score AND c906_rule_grp_id='FORM_ID')
													AND t623.c602_question_id IN (SELECT to_number(c906_rule_value) FROM t906_rules WHERE c906_rule_id = p_score AND c906_rule_grp_id='QUESTION_ID')
												   AND v621.c621_surgery_comp_fl = 'Y') ndipreopt
										 WHERE ndivalue.c621_patient_id = ndipreopt.c621_patient_id
										   AND ndipreopt.c614_study_site_id IN (SELECT *
																				  FROM v_in_list)) innq
									 , t901_code_lookup t901
								 WHERE innq.pid = t901.c901_code_id
							  GROUP BY c613_study_period_ds
									 , c613_seq_no
									 , ROLLUP (t901.c902_code_nm_alt
											 , (t901.c901_code_seq_no, t901.c901_code_id, t901.c901_code_nm))
							  ORDER BY c613_study_period_ds, c613_seq_no)
					   WHERE sgroup IS NOT NULL OR stype IS NOT NULL OR grpingid <> 2
					ORDER BY sgroup, groupingcount, sseqno, pseqno;
	END gm_cr_fch_ndi_per_improve;

/*******************************************************
 * Purpose: This Procedure is used to fetch the NDI improve detail
 *******************************************************/
--
	PROCEDURE gm_cr_fch_ndi_improve_detail (
	    p_studyid		IN	 t611_clinical_study.c611_study_id%TYPE
	  , p_studysiteid	IN	 VARCHAR2
	  , p_treatmentid	IN	 v621_patient_study_list.c901_surgery_type%TYPE
	  , p_out_header	OUT 	 TYPES.cursor_type
	  , p_out_details	OUT 	 TYPES.cursor_type
	  , p_out_preop 	OUT 	 TYPES.cursor_type
	)
	AS
		v_cnt		     NUMBER := 0;
		v_score		     VARCHAR2(20); 
		v_question_id    VARCHAR2(20);
		v_study_listid   VARCHAR2(20);
		v_study_prdid_in VARCHAR2(20);
	BEGIN
		
		SELECT get_rule_value(p_studyid,'OUTNDISCORE') 
		  INTO v_score
		  FROM DUAL;
		
		  -- STUDY_LIST_ID_GRP, FORM_ID,QUESTION_ID  having two values in T906 for ACADIA in single score id.
		SELECT t901.c901_code_nm,
		     --  get_rule_value(v_score,'STUDY_LIST_ID_GRP'),
		       get_rule_value(v_score,'STUDY_PRD_ID_IN_GRP')
		  INTO v_question_id, 
		  	   --v_study_listid,
		       v_study_prdid_in
		  FROM t901_code_lookup t901
		 WHERE t901.c901_code_id = v_score;
		
		
		-- The input String in the form of 101.210,101.211 will be set into this temp view.
		my_context.set_my_inlist_ctx (p_studysiteid);

		SELECT COUNT (token)
		  INTO v_cnt
		  FROM v_in_list;

		OPEN p_out_header
		 FOR
			 SELECT DISTINCT c613_study_period_ds || ' <br>%imp' period, c613_seq_no seq
						FROM t613_study_period
					   WHERE c612_study_list_id IN (SELECT to_number(C906_RULE_VALUE) FROM T906_RULES WHERE C906_RULE_GRP_ID='STUDY_LIST_ID_GRP' and C906_RULE_ID = v_score) AND c613_study_period_id NOT IN (v_study_prdid_in)
			 UNION
			 SELECT DISTINCT c613_study_period_ds || ' >=25%' period, c613_seq_no seq
						FROM t613_study_period
					   WHERE c612_study_list_id IN (SELECT to_number(C906_RULE_VALUE) FROM T906_RULES WHERE C906_RULE_GRP_ID='STUDY_LIST_ID_GRP' and C906_RULE_ID = v_score) AND c613_study_period_id NOT IN (v_study_prdid_in)
			 UNION
			 SELECT   'Treatment' period, 1 seq
				 FROM DUAL
			 /* UNION
			  SELECT   'PreOpt' period, 2 seq
				  FROM DUAL*/
			 ORDER BY seq;

		OPEN p_out_details
		 FOR
			 SELECT ID, NAME, period, treatment, grpingid, DECODE (imp, 'M', 'M', format_number (imp, 2)) imp
				  , DECODE (imp, 'M', '-', NVL (impflg, '0')) impflg, TO_CHAR (NVL (c613_seq_no, '0')) c613_seq_no
			   FROM (SELECT   DECODE (ndivalue.c621_patient_id, NULL, 'ALL PATIENTS', ndivalue.c621_patient_id) ID
							, DECODE (ndivalue.c621_patient_ide_no
									, NULL, 'ALL PATIENTS'
									, ndivalue.c621_patient_ide_no
									 ) NAME
							, ndivalue.c613_study_period_ds period, get_code_name (c901_surgery_type) treatment
							, GROUPING_ID (ndivalue.c613_study_period_ds
										 , ndivalue.c621_patient_id
										 , ndivalue.c621_patient_ide_no
										  ) grpingid
							, DECODE (ndivalue.c621_patient_id
									, NULL, AVG (  (  (  DECODE (ndivalue.curcalvalue
															   , NULL, NULL
															   , ndipreopt.preopcal_value
																)
													   - ndivalue.curcalvalue
													  )
													/ DECODE (ndivalue.curcalvalue
															, NULL, NULL
															, ndipreopt.preopcal_value
															 )
												   )
												 * 100
												)
									   || ''
									, (DECODE (ndivalue.curcalvalue
											 , NULL, 'M'
											 , (  (  (SUM (ndipreopt.preopcal_value) - SUM (ndivalue.curcalvalue))
												   / SUM (ndipreopt.preopcal_value)
												  )
												* 100
											   )
											  )
									  )
									 ) imp
							, DECODE (ndivalue.c621_patient_id
									, NULL, SUM (DECODE (SIGN (  (	(  (ndipreopt.preopcal_value - ndivalue.curcalvalue
																	   )
																	 / ndipreopt.preopcal_value
																	)
																  * 100
																 )
															   - 25
															  )
													   , 1, 1
													   , 0, 1
														)
												)
									   || ''
									, DECODE (ndivalue.curcalvalue
											, NULL, NULL
											, MIN (DECODE (SIGN (  (  (  (ndipreopt.preopcal_value
																		  - ndivalue.curcalvalue
																		 )
																	   / ndipreopt.preopcal_value
																	  )
																	* 100
																   )
																 - 25
																)
														 , -1, 'N'
														 , 'Y'
														  )
												  )
											 )
									 ) impflg
							, c613_seq_no
						 FROM (SELECT v621.c621_patient_id, v621.c621_patient_ide_no, c901_surgery_type
									, c613_study_period_ds, c613_seq_no, TO_NUMBER (t623.c904_answer_ds) curcalvalue
								 FROM t622_patient_list t622, v621_patient_study_list v621 ,t623_patient_answer_list t623
								WHERE v621.c621_patient_id = t622.c621_patient_id
								  AND v621.c613_study_period_id = t622.c613_study_period_id
								  AND t622.c622_patient_list_id = t623.c622_patient_list_id
								  AND NVL(t622.C622_DELETE_FL,'N')='N'
								  AND NVL(t623.C623_DELETE_FL,'N')='N'
								  AND t623.c602_question_id IN(SELECT to_number(c906_rule_value) FROM t906_rules WHERE c906_rule_id = v_score AND c906_rule_grp_id='QUESTION_ID')
								  AND c612_study_list_id IN (SELECT to_number(C906_RULE_VALUE) FROM T906_RULES WHERE C906_RULE_GRP_ID='STUDY_LIST_ID_GRP' and C906_RULE_ID = v_score)
								  AND v621.c613_study_period_id NOT IN (v_study_prdid_in)
								  AND v621.c621_surgery_comp_fl = 'Y'
							   UNION
							   SELECT v621.c621_patient_id, v621.c621_patient_ide_no, c901_surgery_type
									, c613_study_period_ds, c613_seq_no, NULL curcalvalue
								FROM v621_patient_study_list v621
								WHERE v621.c621_exp_date_final <= TRUNC (SYSDATE)
								  AND (v621.c621_patient_id, v621.c613_study_period_id) NOT IN (
																  SELECT t622.c621_patient_id
																	   , t622.c613_study_period_id
																	FROM t622_patient_list t622
																	WHERE NVL(C622_DELETE_FL,'N')='N')
								  AND c612_study_list_id IN (SELECT to_number(C906_RULE_VALUE) FROM T906_RULES WHERE C906_RULE_GRP_ID='STUDY_LIST_ID_GRP' and C906_RULE_ID = v_score)
								  AND v621.c613_study_period_id NOT IN (v_study_prdid_in)
								  AND v621.c621_surgery_comp_fl = 'Y') ndivalue
							, (SELECT v621.c621_patient_id, TO_NUMBER (t623.c904_answer_ds) preopcal_value
									, v621.c611_study_id, v621.c614_study_site_id
								 FROM t622_patient_list t622, v621_patient_study_list v621,t623_patient_answer_list t623
								WHERE v621.c621_patient_id = t622.c621_patient_id
								  AND v621.c613_study_period_id = t622.c613_study_period_id
								  AND t622.c622_patient_list_id = t623.c622_patient_list_id
								  AND NVL(t622.C622_DELETE_FL,'N')='N'
								  AND NVL(t623.C623_DELETE_FL,'N')='N'
								  AND t623.c602_question_id IN(SELECT to_number(c906_rule_value) FROM t906_rules WHERE c906_rule_id = v_score AND c906_rule_grp_id='QUESTION_ID')
								  AND v621.c613_study_period_id IN (v_study_prdid_in)
								  AND c612_study_list_id IN (SELECT to_number(C906_RULE_VALUE) FROM T906_RULES WHERE C906_RULE_GRP_ID='STUDY_LIST_ID_GRP' and C906_RULE_ID = v_score)
								  AND v621.c621_surgery_comp_fl = 'Y') ndipreopt
						WHERE ndivalue.c621_patient_id = ndipreopt.c621_patient_id
						  AND ndipreopt.c611_study_id = p_studyid
						  AND (   ndipreopt.c614_study_site_id IN DECODE (v_cnt, 0, ndipreopt.c614_study_site_id)
							   OR ndipreopt.c614_study_site_id IN (SELECT *
																	 FROM v_in_list)
							  )
						  AND (   ndivalue.c901_surgery_type IN DECODE (p_treatmentid
																	  , 0, ndivalue.c901_surgery_type
																	  , 6211, NULL
                                    								  , 6212, NULL
                                    								  , 6213, NULL
																	  , p_treatmentid
																	   )
							   OR ndivalue.c901_surgery_type IN
									  (DECODE (p_treatmentid, 6211, 6200, NULL)
									 , DECODE (p_treatmentid, 6211, 6202, NULL)
                                     , DECODE (p_treatmentid, 6212, 6201, NULL)
                                     , DECODE (p_treatmentid, 6213, 6210, NULL)
									  )
							  )
					 GROUP BY ROLLUP (ndivalue.c613_study_period_ds
									, (ndivalue.c621_patient_id
									 , ndivalue.c621_patient_ide_no
									 , ndivalue.c613_study_period_ds
									 , ndivalue.c901_surgery_type
									 , ndivalue.curcalvalue
									 , c613_seq_no
									  ))
					   HAVING GROUPING_ID (ndivalue.c613_study_period_ds) <> 1
					 ORDER BY ndivalue.c621_patient_ide_no, ndivalue.c613_seq_no);
	/* OPEN p_out_preop FOR
		 SELECT v621.c621_patient_id ID, TO_NUMBER (t622.c621_cal_value) preopcal_value, v621.c611_study_id
			  , v621.c614_study_site_id
			 FROM t622_patient_list t622, v621_patient_study_list v621
			WHERE v621.c621_patient_id = t622.c621_patient_id
			AND v621.c613_study_period_id = t622.c613_study_period_id
			AND v621.c613_study_period_id IN (26)
			AND c612_study_list_id IN (5)
			AND v621.c611_study_id = p_studyid
			AND v621.c614_study_site_id IN (DECODE (p_studysiteid, NULL, v621.c614_study_site_id, (SELECT *
																									 FROM v_in_list)))
			AND c901_surgery_type = DECODE (p_treatmentid, 0, c901_surgery_type, p_treatmentid); */
	END gm_cr_fch_ndi_improve_detail;

	/*******************************************************
 * Purpose: This Procedure is used to fetch the NDI per improve
 *******************************************************/
--
	PROCEDURE gm_cr_fch_outcome_score (
	    p_studyid		IN	 t611_clinical_study.c611_study_id%TYPE
	  , p_studysiteid	IN	 VARCHAR2
	  , p_type		IN	 t901_code_lookup.c902_code_nm_alt%TYPE
	  , p_out_header	OUT 	 TYPES.cursor_type
	  , p_out_details	OUT 	 TYPES.cursor_type
	)
	AS
		v_form_id	   t622_patient_list.c601_form_id%TYPE;
		v_question_id  t623_patient_answer_list.c602_question_id%TYPE;
		v_ans_id       VARCHAR(10);
	BEGIN
		-- The input String in the form of 101.210,101.211 will be set into this temp view.
		my_context.set_my_inlist_ctx (p_studysiteid);

		SELECT t901.c902_code_nm_alt, t901.c901_code_nm, get_rule_value(p_type,'FCH_OUT_SCR')
		  INTO v_form_id, v_question_id, v_ans_id
		  FROM t901_code_lookup t901
		 WHERE t901.c901_code_id = p_type;

		OPEN p_out_header
		 FOR
			 SELECT DISTINCT	DECODE (c613_study_period_ds, 'Preoperative', 'PreOp', c613_study_period_ds)
							 || '  <br>Avg' period
						   , c613_seq_no
						FROM t613_study_period
					   WHERE c612_study_list_id IN (10, 5) AND c613_study_period_id NOT IN (68)
			 UNION
			 SELECT DISTINCT	DECODE (c613_study_period_ds, 'Preoperative', 'PreOp', c613_study_period_ds)
							 || ' <br>'
							 || to_char(UNISTR('\00B1'))
							 || 'SD' period
						   , c613_seq_no
						FROM t613_study_period
					   WHERE c612_study_list_id IN (10, 5) AND c613_study_period_id NOT IN (68)
					ORDER BY c613_seq_no;

	IF p_studyid = 'GPR005' then

		OPEN p_out_details
		 FOR
			 SELECT DISTINCT 'A'||ROWNUM ID, NVL (stype, NVL (sgroup, 'ALL PATIENTS')) NAME
						   , DECODE (speriod, 'Preoperative', 'PreOp', speriod) speriod, NVL (avgval, 0) averageval
						   , DECODE (grpingid, 0, DECODE (sgroup, NULL, 2, grpingid), grpingid) groupingcount
						   , (to_char(UNISTR('\00B1')) || stdevval) stdevval, sseqno, pseqno
						   , NVL (sgroup, DECODE (grpingid, 0, 'XXXX')) sgroup
						FROM (SELECT   t901.c902_code_nm_alt sgroup, t901.c901_code_nm stype
									 , v621.c613_study_period_ds speriod
									 , GROUPING_ID (t901.c901_code_nm, t901.c902_code_nm_alt) grpingid
									 , v621.c613_seq_no pseqno
                   					 , format_number (AVG (decode ( p_type,'61126',decode (gm_pkg_cr_common.get_valid_cr_date(c904_answer_ds),null,0,1),c904_answer_ds)),1) avgval
									 , format_number (STDDEV (to_number(decode ( p_type,'61126',decode (gm_pkg_cr_common.get_valid_cr_date(c904_answer_ds),null,0,1),c904_answer_ds))), 1) stdevval 
									 , NVL (t901.c901_code_seq_no, 0) sseqno
								  FROM t622_patient_list t622
									 , t623_patient_answer_list t623
									 , v621_patient_study_list v621
									 , t901_code_lookup t901
								 WHERE t622.c601_form_id   IN (select c906_rule_value from t906_rules WHERE c906_rule_id = p_type AND c906_rule_grp_id='FORM_ID')
								   AND t622.c622_patient_list_id = t623.c622_patient_list_id
								   AND NVL(t622.C622_DELETE_FL,'N')='N'
								   AND NVL(t623.C623_DELETE_FL,'N')='N'
								   AND v621.c621_patient_id = t622.c621_patient_id
								   AND v621.c613_study_period_id = t622.c613_study_period_id
								   AND v621.c901_surgery_type = t901.c901_code_id
								   AND t623.c602_question_id  in (select c906_rule_value from t906_rules WHERE c906_rule_id = p_type AND c906_rule_grp_id='QUESTION_ID')
								   AND t623.c904_answer_ds IS NOT NULL
								   AND t623.c603_answer_grp_id in (select c603_answer_grp_id from t603_answer_grp where c602_question_id in (select c906_rule_value from t906_rules WHERE c906_rule_id = p_type AND c906_rule_grp_id='QUESTION_ID'))
								   AND v621.c611_study_id = p_studyid
								   AND v621.c621_surgery_comp_fl = 'Y'
								   AND v621.c614_study_site_id IN (SELECT *
																	 FROM v_in_list)
							  GROUP BY c613_study_period_ds
									 , c613_seq_no
									 , ROLLUP (t901.c902_code_nm_alt, (t901.c901_code_seq_no, t901.c901_code_nm))
							  ORDER BY c613_study_period_ds, c613_seq_no)
					   WHERE sgroup IS NOT NULL OR stype IS NOT NULL OR grpingid <> 2
					ORDER BY sgroup, groupingcount, sseqno, pseqno;

	else

		OPEN p_out_details
		 FOR
			 SELECT DISTINCT 'A'||ROWNUM ID, NVL (stype, NVL (sgroup, 'ALL PATIENTS')) NAME
						   , DECODE (speriod, 'Preoperative', 'PreOp', speriod) speriod, NVL (avgval, 0) averageval
						   , DECODE (grpingid, 0, DECODE (sgroup, NULL, 2, grpingid), grpingid) groupingcount
						   , (to_char(UNISTR('\00B1')) || stdevval) stdevval, sseqno, pseqno
						   , NVL (sgroup, DECODE (grpingid, 0, 'XXXX')) sgroup
						FROM (SELECT   t901.c902_code_nm_alt sgroup, t901.c901_code_nm stype
									 , v621.c613_study_period_ds speriod
									 , GROUPING_ID (t901.c901_code_nm, t901.c902_code_nm_alt) grpingid
									 , v621.c613_seq_no pseqno, format_number (AVG (c904_answer_ds), 1) avgval
									 , format_number (STDDEV (c904_answer_ds), 1) stdevval
									 , NVL (t901.c901_code_seq_no, 0) sseqno
								  FROM t622_patient_list t622
									 , t623_patient_answer_list t623
									 , v621_patient_study_list v621
									 , t901_code_lookup t901
								 WHERE t622.c601_form_id = v_form_id
								   AND t622.c622_patient_list_id = t623.c622_patient_list_id
								   AND NVL(t622.C622_DELETE_FL,'N')='N'
								   AND NVL(t623.C623_DELETE_FL,'N')='N'
								   AND v621.c621_patient_id = t622.c621_patient_id
								   AND v621.c613_study_period_id = t622.c613_study_period_id
								   AND v621.c901_surgery_type = t901.c901_code_id
								   AND t623.c602_question_id = v_question_id
								   AND t623.c904_answer_ds IS NOT NULL
								   AND t623.c603_answer_grp_id =
														DECODE (v_ans_id
															  , '', t623.c603_answer_grp_id
															  , null, t623.c603_answer_grp_id
															  , v_ans_id
															   )
								   AND v621.c611_study_id = p_studyid
								   AND v621.c621_surgery_comp_fl = 'Y'
								   AND v621.c614_study_site_id IN (SELECT *
																	 FROM v_in_list)
							  GROUP BY c613_study_period_ds
									 , c613_seq_no
									 , ROLLUP (t901.c902_code_nm_alt, (t901.c901_code_seq_no, t901.c901_code_nm))
							  ORDER BY c613_study_period_ds, c613_seq_no)
					   WHERE sgroup IS NOT NULL OR stype IS NOT NULL OR grpingid <> 2
					ORDER BY sgroup, groupingcount, sseqno, pseqno;

	END IF;
	
          
	END gm_cr_fch_outcome_score;

/*******************************************************
 * Purpose: This Procedure is used to fetch the patient satisfaction
 *******************************************************/
--
	PROCEDURE gm_cr_fch_patient_satisfaction (
	    p_studyid		IN	 t611_clinical_study.c611_study_id%TYPE
	  , p_studysiteid	IN	 VARCHAR2
	  , p_timeperiod	IN	 t613_study_period.c613_study_period_ds%TYPE
	  , p_out_details	OUT 	 TYPES.cursor_type
	)
	AS
		v_form_id      t622_patient_list.c601_form_id%TYPE;
		v_question_id  t623_patient_answer_list.c602_question_id%TYPE;
		v_timeperiod   t613_study_period.c613_study_period_ds%TYPE;
	BEGIN
		-- The input String in the form of 101.210,101.211 will be set into this temp view.
		my_context.set_my_inlist_ctx (p_studysiteid);

		SELECT t901.c902_code_nm_alt, t901.c901_code_nm
		  INTO v_form_id, v_question_id
		  FROM t901_code_lookup t901
		 WHERE t901.c901_code_id = get_rule_value(p_studyid,'CAPS_GRP');

		SELECT DISTINCT c613_study_period_ds
   		INTO v_timeperiod 
   		FROM t613_study_period
  		WHERE c612_study_list_id       IN (10, 5)
    	AND c613_study_period_id NOT IN (26, 68)
    	AND c613_seq_no               = p_timeperiod;

		 
		OPEN p_out_details
		 FOR
			 SELECT   'A'||ROWNUM ID, NVL (stype, NVL (sgroup, 'ALL PATIENTS')) NAME, period, dtrue
					, DECODE (grpingid, 0, DECODE (sgroup, NULL, 2, grpingid), grpingid) groupingcount, sseqno, pseqno
					, NVL (sgroup, DECODE (grpingid, 0, 'XXXX')) sgroup
					, format_number (((dtrue / tcount) * 100), 1) dtrue_per, mtrue
					, format_number (((mtrue / tcount) * 100), 1) mtrue_per, dknow
					, format_number (((dknow / tcount) * 100), 1) dknow_per, mfalse
					, format_number (((mfalse / tcount) * 100), 1) mfalse_per, dfalse
					, format_number (((dfalse / tcount) * 100), 1) dfalse_per
				 FROM (SELECT	t901.c902_code_nm_alt sgroup, t901.c901_code_nm stype, c613_study_period_ds period
							  , GROUPING_ID (t901.c901_code_nm, t901.c902_code_nm_alt) grpingid
							  , SUM (DECODE (t623.c605_answer_list_id, 95, 1, (DECODE (t623.c605_answer_list_id, 8201, 1, 0)))) dtrue
							  , SUM (DECODE (t623.c605_answer_list_id, 96, 1, (DECODE (t623.c605_answer_list_id, 8202, 1, 0)))) mtrue
							  , SUM (DECODE (t623.c605_answer_list_id, 97, 1, (DECODE (t623.c605_answer_list_id, 8203, 1, 0)))) dknow
							  , SUM (DECODE (t623.c605_answer_list_id, 98, 1, (DECODE (t623.c605_answer_list_id, 8204, 1, (DECODE (t623.c605_answer_list_id, 8205, 1, 0)))))) mfalse
							  , SUM (DECODE (t623.c605_answer_list_id, 99, 1, (DECODE (t623.c605_answer_list_id, 8206, 1, 0)))) dfalse, SUM (1) tcount
							  , NVL (t901.c901_code_seq_no, 0) sseqno, c613_seq_no pseqno
						   FROM t622_patient_list t622
							  , t623_patient_answer_list t623
							  , v621_patient_study_list v621
							  , t901_code_lookup t901
						  WHERE t622.c601_form_id = v_form_id
							AND t622.c622_patient_list_id = t623.c622_patient_list_id
							AND NVL(t622.C622_DELETE_FL,'N')='N'
							AND NVL(t623.C623_DELETE_FL,'N')='N'
							AND v621.c621_patient_id = t622.c621_patient_id
							AND v621.c613_study_period_id = t622.c613_study_period_id
							AND v621.c901_surgery_type = t901.c901_code_id
							AND t623.c602_question_id = v_question_id
							AND c613_study_period_ds = v_timeperiod
							AND v621.c611_study_id = p_studyid
							AND v621.c621_surgery_comp_fl = 'Y'
							AND v621.c614_study_site_id IN (SELECT *
															  FROM v_in_list)
					   GROUP BY v621.c613_study_period_ds
							  , c613_seq_no
							  , ROLLUP (t901.c902_code_nm_alt, (t901.c901_code_seq_no, t901.c901_code_nm))
					   ORDER BY period, c613_seq_no)
				WHERE sgroup IS NOT NULL OR stype IS NOT NULL OR grpingid <> 2
			 ORDER BY sgroup, groupingcount, sseqno, pseqno;
	END gm_cr_fch_patient_satisfaction;

/*******************************************************
 * Purpose: This Procedure is used to load the period for drop down
 *******************************************************/
--
	PROCEDURE gm_cr_fch_period_ddown (
		p_out_details	OUT   TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_out_details
		 FOR
			 SELECT DISTINCT c613_study_period_ds period, c613_seq_no seq
						FROM t613_study_period
					   WHERE c612_study_list_id IN (10, 5) AND c613_study_period_id NOT IN (26, 68)
					ORDER BY seq;
	END gm_cr_fch_period_ddown;
/*******************************************************
 * Purpose: This Procedure is used to fetch the Neurological Status
 *******************************************************/
--
	PROCEDURE gm_cr_fch_neurological_status (
	    p_studyid		IN		 t611_clinical_study.c611_study_id%TYPE
	  , p_studysiteid	IN		 VARCHAR2
  	  , p_score		IN		 VARCHAR2
	  , p_out_header	OUT 	 TYPES.cursor_type
	  , p_out_details	OUT 	 TYPES.cursor_type
	)
	AS
		v_form_id			t622_patient_list.c601_form_id%TYPE;
		v_question_id		VARCHAR2(100);
  		v_study_listid		t612_study_form_list.c612_study_list_id%TYPE;
	   	v_study_prdid_in	VARCHAR2(100);
		v_study_prdid_notin	VARCHAR2(100);
		v_ans_grpid			VARCHAR2(100);
		v_ans_lstid_in		VARCHAR2(100);
		v_ans_lstid_notin	VARCHAR2(100);
		v_cval				NUMBER;
		v_ans_lstid 		VARCHAR2(100);
		local_val			VARCHAR2(100);
		 v_cnt          NUMBER;
	BEGIN
		
		SELECT get_rule_value(p_studyid,'NRS_STUDYID')
			   ,get_rule_value (p_studyid, 'NRS_FORMID')
			   ,get_rule_value (p_studyid, 'NRS_ALIST')
		INTO v_study_prdid_in
		 	   ,v_form_id
		 	   ,v_ans_lstid
	    FROM DUAL;
    	 
		-- The input String in the form of 101.210,101.211 will be set into this temp view.
		my_context.set_my_inlist_ctx (p_studysiteid);

		SELECT COUNT (1)
        INTO v_cnt
        FROM v_in_list
       WHERE token IS NOT NULL;
       
		OPEN p_out_header
		 FOR
			 SELECT DISTINCT c613_study_period_ds || ' <BR> n' period, c613_seq_no, 1 a
						FROM t613_study_period
					   WHERE c612_study_list_id IN (Select c612_study_list_id from T612_study_form_list where c601_form_id=v_form_id and c611_study_id=p_studyid) AND c613_study_period_id NOT IN (v_study_prdid_in)
			 UNION
			 SELECT DISTINCT c613_study_period_ds || ' <BR> N' period, c613_seq_no, 2 a
						FROM t613_study_period
					   WHERE c612_study_list_id IN (Select c612_study_list_id from T612_study_form_list where c601_form_id=v_form_id and c611_study_id=p_studyid) AND c613_study_period_id NOT IN (v_study_prdid_in)
			 UNION
			 SELECT DISTINCT c613_study_period_ds || ' <BR> n/N%' period, c613_seq_no, 3 a
						FROM t613_study_period
					   WHERE c612_study_list_id IN (Select c612_study_list_id from T612_study_form_list where c601_form_id=v_form_id and c611_study_id=p_studyid) AND c613_study_period_id NOT IN (v_study_prdid_in)
					ORDER BY c613_seq_no, a;

		OPEN p_out_details
		 FOR
		 
						SELECT DISTINCT cid ID, NVL (stype, NVL (sgroup, 'ALL PATIENTS')) NAME, speriod, patgrowth
						   , DECODE (grpingid, 0, DECODE (sgroup, NULL, 2, grpingid), grpingid) groupingcount
						   , patcount, pat_g25, avgimp_g25, DECODE(avgimp,'',0) avgimp, sseqno, sno pseqno
						   , NVL (sgroup, DECODE (grpingid, 0, 'XXXX')) sgroup
						FROM (SELECT   NVL (t901.c901_code_id
										  , DECODE (t901.c902_code_nm_alt, 'ALL SECURE-C', '6211', '0')
										   ) cid
									 , t901.c902_code_nm_alt sgroup, t901.c901_code_nm stype
									 , c613_study_period_ds speriod, c613_seq_no sno, SUM (curcalvalue) totalpat
									 , GROUPING_ID (t901.c901_code_nm, t901.c902_code_nm_alt) grpingid
									 , SUM (curcalvalue) pat_g25, SUM (preopcal_value) patcount
									 , SUM(preopcal_value) avgpat_g25
									 , (SUM (curcalvalue) / SUM (preopcal_value)) * 100 avgimp
									 ,	ROUND( SUM (DECODE (SIGN (imp), -1, 0, imp))
									   / SUM (DECODE (SIGN (imp), -1, NULL, 1)),1) avgimp_g25
									 , ROUND((SUM (curcalvalue) / SUM (preopcal_value) * 100),1) patgrowth
									 , NVL (t901.c901_code_seq_no, 0) sseqno
								  FROM (SELECT ndipreopt.c621_patient_id, ndipreopt.c901_surgery_type pid
											 , ndipreopt.c613_study_period_ds, ndipreopt.c613_seq_no, DECODE(ndivalue.curcalvalue,'',0,null,0,ndivalue.curcalvalue) curcalvalue
											 , ndipreopt.preopcal_value
											 ,  DECODE (ndivalue.curcalvalue
																, NULL, 0, '', 0
																, (ndivalue.curcalvalue / ndipreopt.preopcal_value) * 100
												  	   )
													  imp,ndipreopt.c614_study_site_id
										  FROM (
										  SELECT v621.c621_patient_id, c901_surgery_type, c613_study_period_ds, c614_study_site_id
													 , c613_seq_no, COUNT(1)/4  curcalvalue
												  FROM t622_patient_list t622, v621_patient_study_list v621 , t623_patient_answer_list t623
												 WHERE v621.c621_patient_id = t622.c621_patient_id
												   AND v621.c613_study_period_id = t622.c613_study_period_id
												   AND c612_study_list_id IN (Select c612_study_list_id from T612_study_form_list where c601_form_id=v_form_id and c611_study_id=p_studyid)
												   AND v621.c613_study_period_id NOT IN (v_study_prdid_in)
												   AND t622.c622_patient_list_id = t623.c622_patient_list_id
												   AND t622.c601_form_id = v_form_id
												   AND t623.c602_question_id IN (select c906_rule_value from t906_rules where c906_rule_id=p_studyid and c906_rule_grp_id='NRS_QUES')
												   AND t623.c603_answer_grp_id in (select c906_rule_value from t906_rules where c906_rule_id=p_studyid and c906_rule_grp_id='NRS_AGRP')
												   AND t623.c605_answer_list_id in (v_ans_lstid)
												   AND v621.c621_surgery_comp_fl = 'Y'
												   AND NVL(t622.C622_DELETE_FL,'N')='N'
												   AND NVL(t623.C623_DELETE_FL,'N')='N'
												   GROUP BY v621.c621_patient_id, c901_surgery_type, c613_study_period_ds, c613_seq_no, c614_study_site_id
												   HAVING COUNT(1)=4
												   ) ndivalue RIGHT JOIN
											  (


											 SELECT v621.c621_patient_id, c901_surgery_type, c613_study_period_ds, c613_seq_no, c614_study_site_id
													  ,SUM(1)/4 preopcal_value
												  FROM t622_patient_list t622, v621_patient_study_list v621, t623_patient_answer_list t623
												 WHERE v621.c621_patient_id = t622.c621_patient_id
												   AND v621.c613_study_period_id = t622.c613_study_period_id
												   AND c612_study_list_id IN (Select c612_study_list_id from T612_study_form_list where c601_form_id=v_form_id and c611_study_id=p_studyid)
												   AND v621.c613_study_period_id NOT IN (v_study_prdid_in)
												   AND t622.c622_patient_list_id = t623.c622_patient_list_id
												   AND NVL(t622.C622_DELETE_FL,'N')='N'
												   AND NVL(t623.C623_DELETE_FL,'N')='N'
   												   AND t622.c601_form_id = v_form_id
												   AND t623.c602_question_id IN (select c906_rule_value from t906_rules where c906_rule_id=p_studyid and c906_rule_grp_id='NRS_QUES')
												   AND t623.c603_answer_grp_id in (select c906_rule_value from t906_rules where c906_rule_id=p_studyid and c906_rule_grp_id='NRS_AGRP')
 												   AND v621.c621_surgery_comp_fl = 'Y'
												   AND t623.c605_answer_list_id in (select c906_rule_value from t906_rules where c906_rule_id=p_studyid and c906_rule_grp_id='NRS_ALIST1')
 												   GROUP BY v621.c621_patient_id, c901_surgery_type, c613_study_period_ds, c613_seq_no, c614_study_site_id
												   ) ndipreopt
										 ON ndivalue.c621_patient_id = ndipreopt.c621_patient_id
										   AND ndivalue.c901_surgery_type = ndipreopt.c901_surgery_type
										   AND ndivalue.c613_study_period_ds  = ndipreopt.c613_study_period_ds
										   AND ndivalue.c613_seq_no = ndipreopt.c613_seq_no
										   AND ndivalue.c614_study_site_id = ndipreopt.c614_study_site_id) innq
										  
									 , t901_code_lookup t901
								 WHERE innq.pid = t901.c901_code_id
								 AND innq.c614_study_site_id IN (select c614_study_site_id
								 FROM	t614_study_site t614iq WHERE t614iq.c611_study_id = p_studyid AND t614iq.c614_site_ID IS NOT NULL
                				 AND (('-999' = DECODE (v_cnt, 0, '-999', '-9999'))
                 				 OR t614iq.c614_study_site_id in (SELECT * FROM v_in_list)))
                 			  	 GROUP BY c613_study_period_ds
									 , c613_seq_no
									 , ROLLUP (t901.c902_code_nm_alt
									 , (t901.c901_code_seq_no, t901.c901_code_id, t901.c901_code_nm))
							  ORDER BY c613_study_period_ds, c613_seq_no)
					   WHERE sgroup IS NOT NULL OR stype IS NOT NULL OR grpingid <> 2
					ORDER BY sgroup, groupingcount, sseqno, pseqno;
					
	END gm_cr_fch_neurological_status;

--
/*******************************************************
 * Purpose: This Procedure is used to fetch the ZCQ Success Reports
 *******************************************************/
--
	PROCEDURE gm_cr_fch_zcq_success (
	    p_studyid		IN	 t611_clinical_study.c611_study_id%TYPE
	  , p_studysiteid	IN	 VARCHAR2
  	  , p_score		IN	 VARCHAR2
	  , p_out_header	OUT 	 TYPES.cursor_type
	  , p_out_details	OUT 	 TYPES.cursor_type
	)
		AS
		v_form_id		t622_patient_list.c601_form_id%TYPE;
		v_question_id		t623_patient_answer_list.c602_question_id%TYPE;
  		v_study_listid		t612_study_form_list.c612_study_list_id%TYPE;
	    	v_study_prdid_in	v621_patient_study_list.c613_study_period_id%TYPE;
		v_study_prdid_notin	v621_patient_study_list.c613_study_period_id%TYPE;
		v_ans_grpid		t623_patient_answer_list.c603_answer_grp_id%TYPE;
		v_ans_lstid_in		t623_patient_answer_list.c605_answer_list_id%TYPE;
		v_ans_lstid_notin	t623_patient_answer_list.c605_answer_list_id%TYPE;
		v_cval			NUMBER;
		v1_score		     VARCHAR2(10);
		v2_score		     VARCHAR2(10);
		v3_score		     VARCHAR2(10);
	BEGIN
		-- The input String in the form of 101.210,101.211 will be set into this temp view.
		my_context.set_my_inlist_ctx (p_studysiteid);

		IF p_score = '60871' OR p_score = '60872' OR p_score = '60873'  OR p_score = '61871' OR p_score = '61872' OR p_score = '61873'  THEN
		BEGIN
		SELECT t901.c902_code_nm_alt, t901.c901_code_nm,
		       get_rule_value(p_score,'STUDY_LIST_ID_GRP'),
		       get_rule_value(p_score,'STUDY_PRD_ID_IN_GRP'),
		       get_rule_value(p_score,'STY_PRD_ID_NOTIN_GRP'),
		       get_rule_value(p_score,'ANSWER_GRP_ID_GRP'),
		       TO_NUMBER(get_rule_value(p_score,'COMPARE_VAL'))
		  INTO v_form_id, v_question_id, v_study_listid,
		       v_study_prdid_in, v_study_prdid_notin,
		       v_ans_grpid, v_cval
		  FROM t901_code_lookup t901
		 WHERE t901.c901_code_id = p_score;
		END;
		END IF;

		OPEN p_out_header
		 FOR
			 SELECT DISTINCT c613_study_period_ds || ' <BR> n' period, c613_seq_no, 1 a
						FROM t613_study_period
					   WHERE c612_study_list_id IN (2509) AND c613_study_period_id NOT IN (154)
			 UNION
			 SELECT DISTINCT c613_study_period_ds || ' <BR> N' period, c613_seq_no, 2 a
						FROM t613_study_period
					   WHERE c612_study_list_id IN (2509) AND c613_study_period_id NOT IN (154)
			 UNION
			 SELECT DISTINCT c613_study_period_ds || ' <BR> n/N%' period, c613_seq_no, 3 a
						FROM t613_study_period
					   WHERE c612_study_list_id IN (2509) AND c613_study_period_id NOT IN (154)
					ORDER BY c613_seq_no, a;

		IF p_score = '60871' OR p_score = '60872' OR p_score = '61871' OR p_score = '61872' THEN
			BEGIN
			OPEN p_out_details
			 FOR
					    SELECT DISTINCT cid ID, NVL (stype, NVL (sgroup, 'ALL PATIENTS')) NAME, speriod, patgrowth
					       , DECODE (grpingid, 0, DECODE (sgroup, NULL, 2, grpingid), grpingid) groupingcount
					       , patcount, pat_g25, avgimp_g25, avgimp, sseqno, sno pseqno
					       , NVL (sgroup, DECODE (grpingid, 0, 'XXXX')) sgroup
					    FROM (SELECT   NVL (t901.c901_code_id
								      , DECODE (t901.c902_code_nm_alt, 'ALL SECURE-C', '6211', '0')
								       ) cid
							       , t901.c902_code_nm_alt sgroup, t901.c901_code_nm stype
							       , c613_study_period_ds speriod, c613_seq_no sno, SUM (1) totalpat
							       , GROUPING_ID (t901.c901_code_nm, t901.c902_code_nm_alt) grpingid
							       , SUM (DECODE (SIGN (imp - 0.5), -1, 0, 0, 0, 1)) pat_g25, SUM (1) patcount
							       , ((SUM (DECODE (SIGN (imp - 0.5), -1, 0, 0, 0, 1)) / SUM (1)) * 100) avgpat_g25
							       , (SUM (preopcal_value) - SUM (curcalvalue)) / SUM (preopcal_value) * 100 avgimp
							       ,    SUM (DECODE (SIGN (imp - 0.5), -1, 0,0, 0, imp))
								 / SUM (DECODE (SIGN (imp - 0.5), -1, NULL, 1)) avgimp_g25
							       , (SUM (DECODE (SIGN (imp - 0.5), -1, 0, 0,0, 1)) / SUM (1) * 100) patgrowth
							       , NVL (t901.c901_code_seq_no, 0) sseqno
							  FROM (SELECT ndivalue.c621_patient_id, ndivalue.c901_surgery_type pid
									   , ndivalue.c613_study_period_ds, c613_seq_no, ndivalue.curcalvalue
									   , ndipreopt.preopcal_value
									   , DECODE (ndivalue.c621_patient_id
										       , NULL, ((ndipreopt.preopcal_value - ndivalue.curcalvalue)
												  )
											    || ''
										       , (  DECODE (ndivalue.curcalvalue
													, NULL, 0
													,     (ndipreopt.preopcal_value - ndivalue.curcalvalue)

													 )
											 )
											) imp
								      FROM (SELECT v621.c621_patient_id, c901_surgery_type, c613_study_period_ds
										       , c613_seq_no, TO_NUMBER (t623.c904_answer_ds)  curcalvalue
										  FROM t622_patient_list t622, v621_patient_study_list v621 , t623_patient_answer_list t623
										 WHERE v621.c621_patient_id = t622.c621_patient_id
										   AND v621.c613_study_period_id = t622.c613_study_period_id
										   AND c612_study_list_id IN (select c612_study_list_id from t612_study_form_list where c601_form_id in (select c906_rule_value from t906_rules WHERE c906_rule_id = p_score AND c906_rule_grp_id='FORM_ID'))
										   AND v621.c613_study_period_id NOT IN (v_study_prdid_notin)--studyperiod id
										   AND t622.c622_patient_list_id = t623.c622_patient_list_id
										   AND NVL(t622.C622_DELETE_FL,'N')='N'
										   AND NVL(t623.C623_DELETE_FL,'N')='N'
										   AND t623.c602_question_id in ((select c906_rule_value from t906_rules WHERE c906_rule_id = p_score AND c906_rule_grp_id='QUESTION_ID'))
										   AND to_char(t623.c601_form_id) IN (select c906_rule_value from t906_rules WHERE c906_rule_id = p_score AND c906_rule_grp_id='FORM_ID')
										   AND v621.c621_surgery_comp_fl = 'Y'
										   AND t623.c603_answer_grp_id IN (select c603_answer_grp_id from t603_answer_grp where c602_question_id in (select c906_rule_value from t906_rules WHERE c906_rule_id = p_score AND c906_rule_grp_id='QUESTION_ID'))) ndivalue
									   , (SELECT v621.c621_patient_id
										       , TO_NUMBER (t623.c904_answer_ds)  preopcal_value
										       , v621.c611_study_id, v621.c614_study_site_id
										  FROM t622_patient_list t622, v621_patient_study_list v621, t623_patient_answer_list t623
										 WHERE v621.c621_patient_id = t622.c621_patient_id
										   AND v621.c613_study_period_id = t622.c613_study_period_id
										   AND NVL(t622.C622_DELETE_FL,'N')='N'
										   AND NVL(t623.C623_DELETE_FL,'N')='N'
										   AND v621.c613_study_period_id IN (v_study_prdid_in)
										   AND t622.c622_patient_list_id = t623.c622_patient_list_id
										   AND c612_study_list_id IN (select c612_study_list_id from t612_study_form_list where c601_form_id in (select c906_rule_value from t906_rules WHERE c906_rule_id = p_score AND c906_rule_grp_id='FORM_ID'))
										   AND t623.c602_question_id in ((select c906_rule_value from t906_rules WHERE c906_rule_id = p_score AND c906_rule_grp_id='QUESTION_ID'))
										   AND to_char(t623.c601_form_id) IN (select c906_rule_value from t906_rules WHERE c906_rule_id = p_score AND c906_rule_grp_id='FORM_ID')
										   AND v621.c621_surgery_comp_fl = 'Y'
										   AND t623.c603_answer_grp_id IN (select c603_answer_grp_id from t603_answer_grp where c602_question_id in (select c906_rule_value from t906_rules WHERE c906_rule_id = p_score AND c906_rule_grp_id='QUESTION_ID'))) ndipreopt
								     WHERE ndivalue.c621_patient_id = ndipreopt.c621_patient_id
								     AND ndipreopt.c614_study_site_id IN (SELECT *
																FROM v_in_list)
																			 ) innq
							       , t901_code_lookup t901
							 WHERE innq.pid = t901.c901_code_id
						    GROUP BY c613_study_period_ds
							       , c613_seq_no
							       , ROLLUP (t901.c902_code_nm_alt
									   , (t901.c901_code_seq_no, t901.c901_code_id, t901.c901_code_nm))
						    ORDER BY c613_study_period_ds, c613_seq_no)
					 WHERE sgroup IS NOT NULL OR stype IS NOT NULL OR grpingid <> 2
				      ORDER BY sgroup, groupingcount, sseqno, pseqno;
			END;
			ELSIF p_score = '60873' OR p_score = '61873' THEN
				IF p_score = '60873' THEN
					v_study_prdid_notin := 105;
				ELSE
					v_study_prdid_notin := 444;
				END IF;
			BEGIN
			OPEN p_out_details
			 FOR

				   SELECT DISTINCT cid ID, NVL (stype, NVL (sgroup, 'ALL PATIENTS')) NAME, speriod, patgrowth
                                       , DECODE (grpingid, 0, DECODE (sgroup, NULL, 2, grpingid), grpingid) groupingcount
                                       , patcount, pat_g25, avgimp_g25, avgimp, sseqno, sno pseqno
                                       , NVL (sgroup, DECODE (grpingid, 0, 'XXXX')) sgroup
                                    FROM (SELECT   NVL (t901.c901_code_id
                                                              , DECODE (t901.c902_code_nm_alt, 'ALL SECURE-C', '6211', '0')
                                                               ) cid
                                                       , t901.c902_code_nm_alt sgroup, t901.c901_code_nm stype
                                                       , c613_study_period_ds speriod, c613_seq_no sno, SUM (1) totalpat
                                                       , GROUPING_ID (t901.c901_code_nm, t901.c902_code_nm_alt) grpingid
                                                       , SUM (DECODE (SIGN (2.5 - imp ), -1, 0, 0, 0, 1)) pat_g25, SUM (1) patcount
                                                       , ((SUM (DECODE (SIGN (2.5 - imp ), -1, 0, 0, 0, 1)) / SUM (1)) * 100) avgpat_g25
                                                       , (SUM (preopcal_value) - SUM (curcalvalue)) / SUM (preopcal_value) * 100 avgimp
                                                       ,    SUM (DECODE (SIGN (2.5 - imp), -1, 0,0, 0, imp))
                                                         / SUM (DECODE (SIGN (2.5 - imp), -1, NULL, 1)) avgimp_g25
                                                       , (SUM (DECODE (SIGN (2.5 - imp), -1, 0, 0,0, 1)) / SUM (1) * 100) patgrowth
                                                       , NVL (t901.c901_code_seq_no, 0) sseqno
                                                  FROM (SELECT ndivalue.c621_patient_id, ndivalue.c901_surgery_type pid
                                                                   , ndivalue.c613_study_period_ds, c613_seq_no, ndivalue.curcalvalue
                                                                   , ndipreopt.preopcal_value
                                                                   , DECODE (ndivalue.c621_patient_id
                                                                               , NULL, ((ndivalue.curcalvalue)
                                                                                          )
                                                                                    || ''
                                                                               , (  DECODE (ndivalue.curcalvalue
                                                                                                , NULL, 0
                                                                                                ,     (ndivalue.curcalvalue)

                                                                                                 )
                                                                                 )
                                                                                ) imp
                                                              FROM (SELECT v621.c621_patient_id, c901_surgery_type, c613_study_period_ds
                                                                               , c613_seq_no, TO_NUMBER (t623.c904_answer_ds)  curcalvalue
                                                                          FROM t622_patient_list t622, v621_patient_study_list v621 , t623_patient_answer_list t623
                                                                         WHERE v621.c621_patient_id = t622.c621_patient_id
                                                                           AND v621.c613_study_period_id = t622.c613_study_period_id
                                                                           AND c612_study_list_id IN (select c612_study_list_id from t612_study_form_list where c601_form_id in (select c906_rule_value from t906_rules WHERE c906_rule_id = p_score AND c906_rule_grp_id='FORM_ID'))
                                                                           AND v621.c613_study_period_id NOT IN (v_study_prdid_notin)--studyperiod id
                                                                           AND t622.c622_patient_list_id = t623.c622_patient_list_id
                                                                           AND NVL(t622.C622_DELETE_FL,'N')='N'
                                                                           AND NVL(t623.C623_DELETE_FL,'N')='N'
                                                                           AND t623.c602_question_id in ((select c906_rule_value from t906_rules WHERE c906_rule_id = p_score AND c906_rule_grp_id='QUESTION_ID'))
                                                                           AND to_char(t623.c601_form_id) IN (select c906_rule_value from t906_rules WHERE c906_rule_id = p_score AND c906_rule_grp_id='FORM_ID')
                                                                           AND v621.c621_surgery_comp_fl = 'Y'
                                                                           AND t623.c603_answer_grp_id IN (select c603_answer_grp_id from t603_answer_grp where c602_question_id in (select c906_rule_value from t906_rules WHERE c906_rule_id = p_score AND c906_rule_grp_id='QUESTION_ID'))) ndivalue
                                                                   , (SELECT v621.c621_patient_id
                                                                               , TO_NUMBER (t623.c904_answer_ds)  preopcal_value
                                                                               , v621.c611_study_id, v621.c614_study_site_id
                                                                          FROM t622_patient_list t622, v621_patient_study_list v621, t623_patient_answer_list t623
                                                                         WHERE v621.c621_patient_id = t622.c621_patient_id
                                                                           AND v621.c613_study_period_id = t622.c613_study_period_id
                                                                           AND v621.c613_study_period_id NOT IN (v_study_prdid_notin)
                                                                           AND t622.c622_patient_list_id = t623.c622_patient_list_id
                                                                           AND NVL(t622.C622_DELETE_FL,'N')='N'
                                                                           AND NVL(t623.C623_DELETE_FL,'N')='N'
                                                                           AND c612_study_list_id IN (select c612_study_list_id from t612_study_form_list where c601_form_id in (select c906_rule_value from t906_rules WHERE c906_rule_id = p_score AND c906_rule_grp_id='FORM_ID'))
                                                                           AND t623.c602_question_id in ((select c906_rule_value from t906_rules WHERE c906_rule_id = p_score AND c906_rule_grp_id='QUESTION_ID'))
                                                                           AND to_char(t623.c601_form_id) IN (select c906_rule_value from t906_rules WHERE c906_rule_id = p_score AND c906_rule_grp_id='FORM_ID')
                                                                           AND v621.c621_surgery_comp_fl = 'Y'
                                                                           AND t623.c603_answer_grp_id IN (select c603_answer_grp_id from t603_answer_grp where c602_question_id in (select c906_rule_value from t906_rules WHERE c906_rule_id = p_score AND c906_rule_grp_id='QUESTION_ID'))) ndipreopt
                                                             WHERE ndivalue.c621_patient_id = ndipreopt.c621_patient_id
                                                             AND ndipreopt.c614_study_site_id IN (SELECT * FROM v_in_list)
                                                                                                                        ) innq
                                                       , t901_code_lookup t901
                                                 WHERE innq.pid = t901.c901_code_id
                                            GROUP BY c613_study_period_ds
                                                       , c613_seq_no
                                                       , ROLLUP (t901.c902_code_nm_alt
                                                                   , (t901.c901_code_seq_no, t901.c901_code_id, t901.c901_code_nm))
                                            ORDER BY c613_study_period_ds, c613_seq_no)
                                 WHERE sgroup IS NOT NULL OR stype IS NOT NULL OR grpingid <> 2
                               ORDER BY sgroup, groupingcount, sseqno, pseqno;
		END;
		ELSIF p_score = '60874' AND p_studyid='GPR003' THEN
		BEGIN
		
            
            OPEN p_out_details
                  FOR

                 
                           SELECT DISTINCT cid ID, NVL (stype, NVL (sgroup, 'ALL PATIENTS')) NAME, speriod, patgrowth
                                       , DECODE (grpingid, 0, DECODE (sgroup, NULL, 2, grpingid), grpingid) groupingcount
                                       , patcount, pat_g25, avgimp_g25, /*avgimp,*/ sseqno, sno pseqno
                                       , NVL (sgroup, DECODE (grpingid, 0, 'XXXX')) sgroup
                                    FROM (SELECT   NVL (t901.c901_code_id
                                                              , DECODE (t901.c902_code_nm_alt, 'ALL SECURE-C', '6211', '0')
                                                               ) cid
                                                       , t901.c902_code_nm_alt sgroup, t901.c901_code_nm stype
                                                       , c613_study_period_ds speriod, c613_seq_no sno, SUM (1) totalpat
                                                       , GROUPING_ID (t901.c901_code_nm, t901.c902_code_nm_alt) grpingid

                                           , SUM( CASE WHEN DECODE(SIGN(imp - 0.5), -1, 0, 0, 0, 1)=1 AND DECODE(SIGN(imp2 - 0.5), -1, 0, 0, 0, 1)=1 AND DECODE(SIGN (2.5 - imp1), -1, 0, 0, 0, 1)=1 THEN 1 ELSE 0 END ) pat_g25
                                           , SUM (1) patcount
                                           , ((SUM (CASE WHEN DECODE(SIGN(imp - 0.5), -1, 0, 0, 0, 1)=1 AND DECODE(SIGN(imp2 - 0.5), -1, 0, 0, 0, 1)=1 AND DECODE(SIGN (2.5 - imp1), -1, 0, 0, 0, 1)=1 THEN 1 ELSE 0 END ) / SUM (1)) * 100) avgpat_g25
                                           , SUM (DECODE (SIGN (imp - 0.5), -1, 0,0, 0, imp)) /
                                             SUM (DECODE (CASE WHEN DECODE(SIGN(imp - 0.5), -1, 0, 0, 0, 1)=1 AND DECODE(SIGN(imp2 - 0.5), -1, 0, 0, 0, 1)=1 AND DECODE(SIGN (2.5 - imp1), -1, 0, 0, 0, 1)=1 THEN 1 ELSE 0 END, -1, NULL, 1)) avgimp_g25
                                           , (SUM (CASE WHEN DECODE(SIGN(imp - 0.5), -1, 0, 0, 0, 1)=1 AND DECODE(SIGN(imp2 - 0.5), -1, 0, 0, 0, 1)=1 AND DECODE(SIGN (2.5 - imp1), -1, 0, 0, 0, 1)=1 THEN 1 ELSE 0 END) / SUM (1) * 100) patgrowth

                                                       , NVL (t901.c901_code_seq_no, 0) sseqno
                                           FROM (
                                                  SELECT ndivalue.c621_patient_id, ndivalue.c901_surgery_type pid
                                                                   , ndivalue.c613_study_period_ds, c613_seq_no

                                                         , DECODE (ndivalue.c621_patient_id
                                                                   , NULL, ((ndipreopt.preopcal_value1 - ndivalue.curcalvalue1)
                                                                          )
                                                                      || ''
                                                                   , (  DECODE (ndivalue.curcalvalue1
                                                                              , NULL, 0
                                                                              ,     (ndipreopt.preopcal_value1 - ndivalue.curcalvalue1)

                                                                              )
                                                                  )
                                                                  ) imp
                                                         , DECODE (ndivalue.c621_patient_id
                                                                   , NULL, ((ndipreopt.preopcal_value2 - ndivalue.curcalvalue2)
                                                                          )
                                                                      || ''
                                                                   , (  DECODE (ndivalue.curcalvalue2
                                                                              , NULL, 0
                                                                              ,     (ndipreopt.preopcal_value2 - ndivalue.curcalvalue2)

                                                                              )
                                                                  )
                                                                  ) imp2

                                                          , DECODE (ndivalue.c621_patient_id
                                               , NULL, ((ndivalue.curcalvalue3)
                                                  )
                                                || ''
                                               , (  DECODE (ndivalue.curcalvalue3
                                                   , NULL, 0
                                                   ,     (ndivalue.curcalvalue3)
                                                    )
                                              )
                                           ) imp1

                                                                     FROM (



                                                                                            SELECT t1.c621_patient_id, t1.c901_surgery_type, t1.c613_study_period_ds
                                                                     , t1.c613_seq_no, curcalvalue1, curcalvalue2, curcalvalue3
                                                                                            from
                                                                                            (SELECT v621.c621_patient_id, c901_surgery_type, c613_study_period_ds
                                                                               , c613_seq_no,
                                                                                                                     TO_NUMBER (t623.c904_answer_ds)  curcalvalue1
                                                                          FROM t622_patient_list t622, v621_patient_study_list v621 , t623_patient_answer_list t623
                                                                         WHERE v621.c621_patient_id = t622.c621_patient_id
                                                                           AND v621.c613_study_period_id = t622.c613_study_period_id
                                                                           AND c612_study_list_id IN (2505)
                                                                           AND v621.c613_study_period_id NOT IN (105)--studyperiod id
                                                                           AND t622.c622_patient_list_id = t623.c622_patient_list_id
                                                                           AND NVL(t622.C622_DELETE_FL,'N')='N'
                                                                           AND NVL(t623.C623_DELETE_FL,'N')='N'
                                                                           AND t623.c602_question_id IN (2519)
                                                                           AND t623.c601_form_id = 25
                                                         				   AND t623.C603_ANSWER_GRP_ID IN (36)
                                                                           AND v621.c621_surgery_comp_fl = 'Y'

                                                                                                )T1,
                                                                                             (SELECT v621.c621_patient_id, c901_surgery_type, c613_study_period_ds
                                                                              , c613_seq_no,
                                                                                                                     TO_NUMBER (t623.c904_answer_ds)  curcalvalue2
                                                                          FROM t622_patient_list t622, v621_patient_study_list v621 , t623_patient_answer_list t623
                                                                         WHERE v621.c621_patient_id = t622.c621_patient_id
                                                                           AND v621.c613_study_period_id = t622.c613_study_period_id
                                                                           AND c612_study_list_id IN (2505)
                                                                           AND v621.c613_study_period_id NOT IN (105)--studyperiod id
                                                                           AND t622.c622_patient_list_id = t623.c622_patient_list_id
                                                                           AND NVL(t622.C622_DELETE_FL,'N')='N'
                                                                           AND NVL(t623.C623_DELETE_FL,'N')='N'
                                                                           AND t623.c602_question_id IN (2520)
                                                                           AND t623.c601_form_id = 25
                                                         				   AND t623.C603_ANSWER_GRP_ID IN (37)
                                                                           AND v621.c621_surgery_comp_fl = 'Y'

                                                                                                )T2,
                                                                                                (SELECT v621.c621_patient_id, c901_surgery_type, c613_study_period_ds
                                                                               , c613_seq_no,
                                                                                                                     TO_NUMBER (t623.c904_answer_ds)  curcalvalue3
                                                                          FROM t622_patient_list t622, v621_patient_study_list v621 , t623_patient_answer_list t623
                                                                         WHERE v621.c621_patient_id = t622.c621_patient_id
                                                                           AND v621.c613_study_period_id = t622.c613_study_period_id
                                                                           AND c612_study_list_id IN (2505)
                                                                           AND v621.c613_study_period_id NOT IN (105)--studyperiod id
                                                                           AND t622.c622_patient_list_id = t623.c622_patient_list_id
                                                                           AND NVL(t622.C622_DELETE_FL,'N')='N'
                                                                           AND NVL(t623.C623_DELETE_FL,'N')='N'
                                                                           AND t623.c602_question_id IN (2521)
                                                                           AND t623.c601_form_id = 25
                                                         				   AND t623.C603_ANSWER_GRP_ID IN (38)
                                                                           AND v621.c621_surgery_comp_fl = 'Y'

                                                                           )T3
                                                                          WHERE t1.c621_patient_id = t2.c621_patient_id
                                                                          AND t2.c621_patient_id = t3.c621_patient_id
                                                                          AND   t1.c901_surgery_type = t2.c901_surgery_type
                                                                          AND t2.c901_surgery_type = t3.c901_surgery_type
                                                                          AND   t1.c613_study_period_ds = t2.c613_study_period_ds
                                                                          AND t2.c613_study_period_ds = t3.c613_study_period_ds
                                                                         ) ndivalue
                                                        				, (
                                                                        SELECT t1.c621_patient_id
                                                                          , preopcal_value1, preopcal_value2, preopcal_value3
                                                                          , t1.c611_study_id, t1.c614_study_site_id
                                                                        FROM
                                                                          (SELECT v621.c621_patient_id
                                                                               , TO_NUMBER (t623.c904_answer_ds)  preopcal_value1
                                                                               , v621.c611_study_id, v621.c614_study_site_id
                                                                          FROM t622_patient_list t622, v621_patient_study_list v621, t623_patient_answer_list t623
                                                                         WHERE v621.c621_patient_id = t622.c621_patient_id
                                                                           AND v621.c613_study_period_id = t622.c613_study_period_id
                                                                           AND v621.c613_study_period_id IN (105)
                                                                           AND t622.c622_patient_list_id = t623.c622_patient_list_id
                                                                           AND NVL(t622.C622_DELETE_FL,'N')='N'
                                                                           AND NVL(t623.C623_DELETE_FL,'N')='N'
                                                                           AND c612_study_list_id IN (2505)
                                                                           AND t623.c602_question_id IN (2519)
                                                                           AND t623.c601_form_id = 25
                                                                           AND v621.c621_surgery_comp_fl = 'Y'
                                                                           )t1,
                                                                          (SELECT v621.c621_patient_id
                                                                            , TO_NUMBER (t623.c904_answer_ds)  preopcal_value2
                                                                               , v621.c611_study_id, v621.c614_study_site_id
                                                                          FROM t622_patient_list t622, v621_patient_study_list v621, t623_patient_answer_list t623
                                                                         WHERE v621.c621_patient_id = t622.c621_patient_id
                                                                           AND v621.c613_study_period_id = t622.c613_study_period_id
                                                                           AND v621.c613_study_period_id IN (105)
                                                                           AND t622.c622_patient_list_id = t623.c622_patient_list_id
                                                                           AND NVL(t622.C622_DELETE_FL,'N')='N'
                                                                           AND NVL(t623.C623_DELETE_FL,'N')='N'
                                                                           AND c612_study_list_id IN (2505)
                                                                           AND t623.c602_question_id IN (2520)
                                                                           AND t623.c601_form_id = 25
                                                                           AND v621.c621_surgery_comp_fl = 'Y'
                                                                           )t2,
                                                                           (SELECT v621.c621_patient_id
                                                                               , TO_NUMBER (t623.c904_answer_ds)  preopcal_value3
                                                                               , v621.c611_study_id, v621.c614_study_site_id
                                                                          FROM t622_patient_list t622, v621_patient_study_list v621, t623_patient_answer_list t623
                                                                         WHERE v621.c621_patient_id = t622.c621_patient_id
                                                                           AND v621.c613_study_period_id = t622.c613_study_period_id
                                                                           AND v621.c613_study_period_id IN (105)
                                                                           AND t622.c622_patient_list_id = t623.c622_patient_list_id
                                                                           AND NVL(t622.C622_DELETE_FL,'N')='N'
                                                                           AND NVL(t623.C623_DELETE_FL,'N')='N'
                                                                           AND c612_study_list_id IN (2505)
                                                                           AND t623.c602_question_id IN (2521)
                                                                           AND t623.c601_form_id = 25
                                                                           AND v621.c621_surgery_comp_fl = 'Y'
                                                                           )t3
                                                                          WHERE t1.c621_patient_id = t2.c621_patient_id
                                                                          AND t2.c621_patient_id = t3.c621_patient_id
                                                                         ) ndipreopt
                                                             WHERE ndivalue.c621_patient_id = ndipreopt.c621_patient_id
                                                             AND ndipreopt.c614_study_site_id IN (SELECT * FROM v_in_list)
                                                            ) innq
                                                       , t901_code_lookup t901
                                                 WHERE innq.pid = t901.c901_code_id
                                            GROUP BY c613_study_period_ds
                                                       , c613_seq_no--, C603_ANSWER_GRP_ID
                                                       , ROLLUP (t901.c902_code_nm_alt
                                                                   , (t901.c901_code_seq_no, t901.c901_code_id, t901.c901_code_nm))
                                            ORDER BY c613_study_period_ds, c613_seq_no)
                                 WHERE sgroup IS NOT NULL OR stype IS NOT NULL OR grpingid <> 2
                               ORDER BY sgroup, groupingcount, sseqno, pseqno;
                               
                  END;  
                  
                  
                  ELSIF p_score = '61874' AND p_studyid='GPR005' THEN
		BEGIN
		
            
            OPEN p_out_details
                  FOR

                 
                          SELECT DISTINCT cid ID, NVL (stype, NVL (sgroup, 'ALL PATIENTS')) NAME, speriod, patgrowth
                                       , DECODE (grpingid, 0, DECODE (sgroup, NULL, 2, grpingid), grpingid) groupingcount
                                       , patcount, pat_g25, avgimp_g25, /*avgimp,*/ sseqno, sno pseqno
                                       , NVL (sgroup, DECODE (grpingid, 0, 'XXXX')) sgroup
                                    FROM (SELECT   NVL (t901.c901_code_id
                                                              , DECODE (t901.c902_code_nm_alt, 'ALL SECURE-C', '6211', '0')
                                                               ) cid
                                                       , t901.c902_code_nm_alt sgroup, t901.c901_code_nm stype
                                                       , c613_study_period_ds speriod, c613_seq_no sno, SUM (1) totalpat
                                                       , GROUPING_ID (t901.c901_code_nm, t901.c902_code_nm_alt) grpingid

						       , SUM( CASE WHEN DECODE(SIGN(imp - 0.5), -1, 0, 0, 0, 1)=1 AND DECODE(SIGN(imp2 - 0.5), -1, 0, 0, 0, 1)=1 AND DECODE(SIGN (2.5 - imp1), -1, 0, 0, 0, 1)=1 THEN 1 ELSE 0 END ) pat_g25
						       , SUM (1) patcount
						       , ((SUM (CASE WHEN DECODE(SIGN(imp - 0.5), -1, 0, 0, 0, 1)=1 AND DECODE(SIGN(imp2 - 0.5), -1, 0, 0, 0, 1)=1 AND DECODE(SIGN (2.5 - imp1), -1, 0, 0, 0, 1)=1 THEN 1 ELSE 0 END ) / SUM (1)) * 100) avgpat_g25
						       , SUM (DECODE (SIGN (imp - 0.5), -1, 0,0, 0, imp)) /
						         SUM (DECODE (CASE WHEN DECODE(SIGN(imp - 0.5), -1, 0, 0, 0, 1)=1 AND DECODE(SIGN(imp2 - 0.5), -1, 0, 0, 0, 1)=1 AND DECODE(SIGN (2.5 - imp1), -1, 0, 0, 0, 1)=1 THEN 1 ELSE 0 END, -1, NULL, 1)) avgimp_g25
						       , (SUM (CASE WHEN DECODE(SIGN(imp - 0.5), -1, 0, 0, 0, 1)=1 AND DECODE(SIGN(imp2 - 0.5), -1, 0, 0, 0, 1)=1 AND DECODE(SIGN (2.5 - imp1), -1, 0, 0, 0, 1)=1 THEN 1 ELSE 0 END) / SUM (1) * 100) patgrowth

                                                       , NVL (t901.c901_code_seq_no, 0) sseqno
                                           FROM (
								  SELECT ndivalue.c621_patient_id, ndivalue.c901_surgery_type pid
                                                                   , ndivalue.c613_study_period_ds, c613_seq_no

									   , DECODE (ndivalue.c621_patient_id
										       , NULL, ((ndipreopt.preopcal_value1 - ndivalue.curcalvalue1)
												  )
											    || ''
										       , (  DECODE (ndivalue.curcalvalue1
													, NULL, 0
													,     (ndipreopt.preopcal_value1 - ndivalue.curcalvalue1)

													 )
											 )
											) imp
									   , DECODE (ndivalue.c621_patient_id
										       , NULL, ((ndipreopt.preopcal_value2 - ndivalue.curcalvalue2)
												  )
											    || ''
										       , (  DECODE (ndivalue.curcalvalue2
													, NULL, 0
													,     (ndipreopt.preopcal_value2 - ndivalue.curcalvalue2)

													 )
											 )
											) imp2

									    , DECODE (ndivalue.c621_patient_id
                                               , NULL, ((ndivalue.curcalvalue3)
                                                  )
                                                || ''
                                               , (  DECODE (ndivalue.curcalvalue3
                                                   , NULL, 0
                                                   ,     (ndivalue.curcalvalue3)
                                                    )
                                              )
                                           ) imp1

								                     FROM (
															  SELECT t1.c621_patient_id, t1.c901_surgery_type, t1.c613_study_period_ds
                                                                     , t1.c613_seq_no, curcalvalue1, curcalvalue2, curcalvalue3
                                                                    from
                                                                      (SELECT v621.c621_patient_id, c901_surgery_type, c613_study_period_ds
                                                                               , c613_seq_no,
                                                                                TO_NUMBER (t623.c904_answer_ds)  curcalvalue1
                                                                          FROM t622_patient_list t622, v621_patient_study_list v621 , t623_patient_answer_list t623
                                                                         WHERE v621.c621_patient_id = t622.c621_patient_id
                                                                           AND v621.c613_study_period_id = t622.c613_study_period_id
                                                                          -- AND c612_study_list_id IN (2505)
                                                                           AND c612_study_list_id IN  (Select c612_study_list_id from T612_study_form_list where c601_form_id IN (select c906_rule_value from t906_rules WHERE c906_rule_id = '61871' AND c906_rule_grp_id='FORM_ID') and c611_study_id='GPR005')
                                                                           AND v621.c613_study_period_id NOT IN (444)--studyperiod id
                                                                           AND t622.c622_patient_list_id = t623.c622_patient_list_id
                                                                           AND NVL(t622.C622_DELETE_FL,'N')='N'
                                                                           AND NVL(t623.C623_DELETE_FL,'N')='N'
                                                                           --AND t623.c602_question_id IN (2519)
                                                                           AND t623.c602_question_id IN (select c906_rule_value from t906_rules where c906_rule_id= '61871' and c906_rule_grp_id='QUESTION_ID')
                                                                           --AND t623.c601_form_id = 25
                                                                           AND to_char(t623.c601_form_id) IN (select c906_rule_value from t906_rules WHERE c906_rule_id = '61871' AND c906_rule_grp_id='FORM_ID')
                                                                        --AND t623.C603_ANSWER_GRP_ID IN (36)
                                                                          AND t623.C603_ANSWER_GRP_ID in (select c603_answer_grp_id from t603_answer_grp where c602_question_id in (select c906_rule_value from t906_rules WHERE c906_rule_id = '61871' AND c906_rule_grp_id='QUESTION_ID'))
                                                                           AND v621.c621_surgery_comp_fl = 'Y'
                                                                          )T1,
                                                                      (SELECT v621.c621_patient_id, c901_surgery_type, c613_study_period_ds
                                                                              , c613_seq_no,
                                                                              TO_NUMBER (t623.c904_answer_ds)  curcalvalue2
                                                                          FROM t622_patient_list t622, v621_patient_study_list v621 , t623_patient_answer_list t623
                                                                         WHERE v621.c621_patient_id = t622.c621_patient_id
                                                                           AND v621.c613_study_period_id = t622.c613_study_period_id
                                                                          AND c612_study_list_id IN  (Select c612_study_list_id from T612_study_form_list where c601_form_id IN (select c906_rule_value from t906_rules WHERE c906_rule_id = '61872' AND c906_rule_grp_id='FORM_ID') and c611_study_id='GPR005')
                                                                           AND v621.c613_study_period_id NOT IN (444)--studyperiod id
                                                                           AND t622.c622_patient_list_id = t623.c622_patient_list_id
                                                                           AND NVL(t622.C622_DELETE_FL,'N')='N'
                                                                           AND NVL(t623.C623_DELETE_FL,'N')='N'
                                                                           AND t623.c602_question_id IN (select c906_rule_value from t906_rules where c906_rule_id= '61872' and c906_rule_grp_id='QUESTION_ID')
                                                                           --AND t623.c601_form_id = 25
                                                                           AND to_char(t623.c601_form_id) IN (select c906_rule_value from t906_rules WHERE c906_rule_id = '61872' AND c906_rule_grp_id='FORM_ID')
                                                                          AND t623.C603_ANSWER_GRP_ID in (select c603_answer_grp_id from t603_answer_grp where c602_question_id in (select c906_rule_value from t906_rules WHERE c906_rule_id = '61872' AND c906_rule_grp_id='QUESTION_ID'))
                                                                           AND v621.c621_surgery_comp_fl = 'Y'
                                                                         )T2,
                                                                      (SELECT v621.c621_patient_id, c901_surgery_type, c613_study_period_ds
                                                                               , c613_seq_no,
                                                                               TO_NUMBER (t623.c904_answer_ds)  curcalvalue3
                                                                          FROM t622_patient_list t622, v621_patient_study_list v621 , t623_patient_answer_list t623
                                                                         WHERE v621.c621_patient_id = t622.c621_patient_id
                                                                           AND v621.c613_study_period_id = t622.c613_study_period_id
                                                                           AND c612_study_list_id IN  (Select c612_study_list_id from T612_study_form_list where c601_form_id IN (select c906_rule_value from t906_rules WHERE c906_rule_id = '61873' AND c906_rule_grp_id='FORM_ID') and c611_study_id='GPR005')
                                                                           AND v621.c613_study_period_id NOT IN (444)--studyperiod id
                                                                           AND t622.c622_patient_list_id = t623.c622_patient_list_id
                                                                           AND NVL(t622.C622_DELETE_FL,'N')='N'
                                                                           AND NVL(t623.C623_DELETE_FL,'N')='N'
                                                                           AND t623.c602_question_id IN (select c906_rule_value from t906_rules where c906_rule_id= '61873' and c906_rule_grp_id='QUESTION_ID')
                                                                           AND to_char(t623.c601_form_id) IN (select c906_rule_value from t906_rules WHERE c906_rule_id = '61873' AND c906_rule_grp_id='FORM_ID')
                                                                           AND t623.C603_ANSWER_GRP_ID in (select c603_answer_grp_id from t603_answer_grp where c602_question_id in (select c906_rule_value from t906_rules WHERE c906_rule_id = '61873' AND c906_rule_grp_id='QUESTION_ID'))
                                                                           AND v621.c621_surgery_comp_fl = 'Y'
                                                                          )T3
                                                                           WHERE t1.c621_patient_id = t2.c621_patient_id
                                                                           AND t2.c621_patient_id = t3.c621_patient_id
                                                                           AND       t1.c901_surgery_type = t2.c901_surgery_type
                                                                           AND t2.c901_surgery_type = t3.c901_surgery_type
                                                                           AND       t1.c613_study_period_ds = t2.c613_study_period_ds
                                                                           AND t2.c613_study_period_ds = t3.c613_study_period_ds
                                                                           ) ndivalue
                                                        					, (
                                                                      	SELECT t1.c621_patient_id
                                                                               , preopcal_value1, preopcal_value2
                                                                               --, preopcal_value3
                                                                               , t1.c611_study_id, t1.c614_study_site_id
                                                                         FROM
                                                                          (SELECT v621.c621_patient_id
                                                                               , TO_NUMBER (t623.c904_answer_ds)  preopcal_value1
                                                                               , v621.c611_study_id, v621.c614_study_site_id
                                                                          FROM t622_patient_list t622, v621_patient_study_list v621, t623_patient_answer_list t623
                                                                         WHERE v621.c621_patient_id = t622.c621_patient_id
                                                                           AND v621.c613_study_period_id = t622.c613_study_period_id
                                                                           AND v621.c613_study_period_id IN (444)
                                                                           AND t622.c622_patient_list_id = t623.c622_patient_list_id
                                                                           AND NVL(t622.C622_DELETE_FL,'N')='N'
                                                                           AND NVL(t623.C623_DELETE_FL,'N')='N'
                                                                           AND c612_study_list_id IN  (Select c612_study_list_id from T612_study_form_list where c601_form_id IN (select c906_rule_value from t906_rules WHERE c906_rule_id = '61871' AND c906_rule_grp_id='FORM_ID') and c611_study_id='GPR005')
                                                                           AND t623.c602_question_id IN (select c906_rule_value from t906_rules where c906_rule_id= '61871' and c906_rule_grp_id='QUESTION_ID')
                                                                            AND to_char(t623.c601_form_id) IN (select c906_rule_value from t906_rules WHERE c906_rule_id = '61871' AND c906_rule_grp_id='FORM_ID')
                                                                           AND v621.c621_surgery_comp_fl = 'Y'
                                                                           )t1,
                                                                         (SELECT v621.c621_patient_id
                                                                            , TO_NUMBER (t623.c904_answer_ds)  preopcal_value2
                                                                               , v621.c611_study_id, v621.c614_study_site_id
                                                                          FROM t622_patient_list t622, v621_patient_study_list v621, t623_patient_answer_list t623
                                                                         WHERE v621.c621_patient_id = t622.c621_patient_id
                                                                           AND v621.c613_study_period_id = t622.c613_study_period_id
                                                                           AND v621.c613_study_period_id IN (444)
                                                                           AND t622.c622_patient_list_id = t623.c622_patient_list_id
                                                                           AND NVL(t622.C622_DELETE_FL,'N')='N'
                                                                           AND NVL(t623.C623_DELETE_FL,'N')='N'
                                                                           AND c612_study_list_id IN  (Select c612_study_list_id from T612_study_form_list where c601_form_id IN (select c906_rule_value from t906_rules WHERE c906_rule_id = '61872' AND c906_rule_grp_id='FORM_ID') and c611_study_id='GPR005')
                                                                           AND t623.c602_question_id IN (select c906_rule_value from t906_rules where c906_rule_id= '61872' and c906_rule_grp_id='QUESTION_ID')
                                                                           AND to_char(t623.c601_form_id) IN (select c906_rule_value from t906_rules WHERE c906_rule_id = '61872' AND c906_rule_grp_id='FORM_ID')
                                                                           AND v621.c621_surgery_comp_fl = 'Y'
                                                                           )t2
                                                                           /*,
                                                                          (SELECT v621.c621_patient_id
                                                                               , TO_NUMBER (t623.c904_answer_ds)  preopcal_value3
                                                                               , v621.c611_study_id, v621.c614_study_site_id
                                                                          FROM t622_patient_list t622, v621_patient_study_list v621, t623_patient_answer_list t623
                                                                         WHERE v621.c621_patient_id = t622.c621_patient_id
                                                                           AND v621.c613_study_period_id = t622.c613_study_period_id
                                                                           AND v621.c613_study_period_id IN (444)
                                                                           AND t622.c622_patient_list_id = t623.c622_patient_list_id
                                                                           AND NVL(t622.C622_DELETE_FL,'N')='N'
                                                                           AND NVL(t623.C623_DELETE_FL,'N')='N'
                                                                           AND c612_study_list_id IN  (Select c612_study_list_id from T612_study_form_list where c601_form_id IN (select c906_rule_value from t906_rules WHERE c906_rule_id = '61873' AND c906_rule_grp_id='FORM_ID') and c611_study_id='GPR005')
                                                                           AND t623.c602_question_id IN (select c906_rule_value from t906_rules where c906_rule_id= '61873' and c906_rule_grp_id='QUESTION_ID')
                                                                           AND to_char(t623.c601_form_id) IN (select c906_rule_value from t906_rules WHERE c906_rule_id = '61873' AND c906_rule_grp_id='FORM_ID')
                                                                           AND v621.c621_surgery_comp_fl = 'Y'
                                                                           )t3
                                                                           */
                                                                           WHERE t1.c621_patient_id = t2.c621_patient_id
                                                                           --AND t2.c621_patient_id = t3.c621_patient_id
                                                                           ) ndipreopt
                                                             WHERE ndivalue.c621_patient_id = ndipreopt.c621_patient_id
                                                           AND ndipreopt.c614_study_site_id IN (SELECT * FROM v_in_list)
                                                            ) innq
                                                       , t901_code_lookup t901
                                                 WHERE innq.pid = t901.c901_code_id
                                            GROUP BY c613_study_period_ds
                                                       , c613_seq_no--, C603_ANSWER_GRP_ID
                                                       , ROLLUP (t901.c902_code_nm_alt
                                                                   , (t901.c901_code_seq_no, t901.c901_code_id, t901.c901_code_nm))
                                            ORDER BY c613_study_period_ds, c613_seq_no)
                                 WHERE sgroup IS NOT NULL OR stype IS NOT NULL OR grpingid <> 2
                               ORDER BY sgroup, groupingcount, sseqno, pseqno;
                               
                  END;    
               
                   END IF;
           
           

	END gm_cr_fch_zcq_success;
	
	/******************************************************************
	* Description : Procedure to fetch Amnios study FFI form details (Outcome report)
	* 				PMT-43699: Other outcome report changes (Amnios study)
	****************************************************************/
	PROCEDURE gm_cr_fch_amnios_outcome_score (
        p_studyid     IN t611_clinical_study.c611_study_id%TYPE,
        p_studysiteid IN VARCHAR2,
        p_type        IN t901_code_lookup.c902_code_nm_alt%TYPE,
        p_out_header OUT TYPES.cursor_type,
        p_out_details OUT TYPES.cursor_type)
	AS
	    v_form_id t622_patient_list.c601_form_id%TYPE;
	    v_question_id t623_patient_answer_list.c602_question_id%TYPE;
	    v_ans_id VARCHAR (10) ;
	BEGIN
	    -- to set the site id to context
	    my_context.set_my_inlist_ctx (p_studysiteid) ;
	    
	    -- To fetch the Form and quetion details
	    BEGIN
		    
	     SELECT t901.c902_code_nm_alt, t901.c901_code_nm, get_rule_value (p_type, 'FCH_OUT_SCR')
	       INTO v_form_id, v_question_id, v_ans_id
	       FROM t901_code_lookup t901
	      WHERE t901.c901_code_id = p_type
	       AND c901_void_fl IS NULL;
	       
	     EXCEPTION WHEN NO_DATA_FOUND
	     THEN
	     	v_form_id := 0;
	     	v_question_id := 0;
	     	v_ans_id := 0;
	     END;
	     
	     --26606 : AM6 - FFI-R Foot Function Index Form 
	     -- to fetch Header details 
	     
	    OPEN p_out_header FOR
	    SELECT DISTINCT c613_study_period_ds || '  <br>Avg' period, c613_seq_no
	       FROM t613_study_period
	      WHERE c612_study_list_id IN (26606)
	      AND c613_delete_fl = 'N'
	      UNION
	    SELECT DISTINCT c613_study_period_ds || ' <br>' || TO_CHAR (UNISTR ('\00B1')) || 'SD' period, c613_seq_no
	       FROM t613_study_period
	      WHERE c612_study_list_id IN (26606)
	      AND c613_delete_fl = 'N'
	   ORDER BY c613_seq_no;
	   
	   -- To fetch details cursor
	    OPEN p_out_details FOR
	    SELECT DISTINCT 'A'||ROWNUM ID, NVL (stype, NVL (sgroup, 'ALL PATIENTS')) NAME, DECODE (speriod, 'Preoperative',
	        'PreOp', speriod) speriod, NVL (avgval, 0) averageval, DECODE (grpingid, 0, DECODE (sgroup, NULL, 2, grpingid),
	        grpingid) groupingcount, (TO_CHAR (UNISTR ('\00B1')) || stdevval) stdevval, sseqno
	      , pseqno, NVL (sgroup, DECODE (grpingid, 0, 'XXXX')) sgroup
	       FROM
	        (
	             SELECT t901.c902_code_nm_alt sgroup, t901.c901_code_nm stype, v621.c613_study_period_ds speriod
	              , GROUPING_ID (t901.c901_code_nm, t901.c902_code_nm_alt) grpingid, v621.c613_seq_no pseqno, format_number
	                (AVG (c904_answer_ds), 1) avgval, format_number (STDDEV (c904_answer_ds), 1) stdevval, NVL (
	                t901.c901_code_seq_no, 0) sseqno
	               FROM t622_patient_list t622, t623_patient_answer_list t623, v621_patient_study_list v621
	              , t901_code_lookup t901
	              WHERE t622.c601_form_id              = v_form_id
	                AND t622.c622_patient_list_id      = t623.c622_patient_list_id
	                AND NVL (t622.C622_DELETE_FL, 'N') = 'N'
	                AND NVL (t623.C623_DELETE_FL, 'N') = 'N'
	                AND v621.c621_patient_id           = t622.c621_patient_id
	                AND v621.c613_study_period_id      = t622.c613_study_period_id
	                AND v621.c901_surgery_type         = t901.c901_code_id
	                AND t623.c602_question_id          = v_question_id
	                AND t623.c904_answer_ds           IS NOT NULL
	                AND t623.c603_answer_grp_id        = DECODE (v_ans_id, '', t623.c603_answer_grp_id, NULL,
	                t623.c603_answer_grp_id, v_ans_id)
	                AND v621.c611_study_id        = p_studyid
	                AND v621.c621_surgery_comp_fl = 'Y'
	                AND v621.c614_study_site_id  IN
	                (
	                     SELECT * FROM v_in_list
	                )
	           GROUP BY c613_study_period_ds, c613_seq_no, ROLLUP (t901.c902_code_nm_alt, (t901.c901_code_seq_no,
	                t901.c901_code_nm))
	           ORDER BY c613_study_period_ds, c613_seq_no
	        )
	      WHERE sgroup  IS NOT NULL
	        OR stype    IS NOT NULL
	        OR grpingid <> 2
	   ORDER BY sgroup, groupingcount, sseqno
	      , pseqno;
	      
	END gm_cr_fch_amnios_outcome_score;

	/*******************************************************
	* Description: This Procedure is used to fetch the Amnios study period information.
	*******************************************************/
	--
	PROCEDURE gm_cr_fch_amnios_period_ddown (
	        p_out_details OUT TYPES.cursor_type)
	AS
	BEGIN
		/*
		 * Study List id information
		 * 26505 VAS Foot Pain Form
		 * 27010 Study Completion Form
		 * 
		 */
		
		/*
		 * Study Period information.
		 * 23 Pre-Injection 
		 */
		
	    OPEN p_out_details FOR
	    SELECT DISTINCT c613_study_period_ds period, c613_seq_no seq
	       FROM t613_study_period
	      WHERE c612_study_list_id       IN (26505)
	        AND c613_study_period_id NOT IN (23)
	   ORDER BY seq;
	   
	END gm_cr_fch_amnios_period_ddown;

	/*******************************************************
	* Description: This Procedure is used to fetch Amnois study Patient Satisfactions details
	*     PMT-50759: Amnios study report changes
	*******************************************************/
	PROCEDURE gm_cr_fch_amnios_patient_satisfaction (
	        p_studyid     IN t611_clinical_study.c611_study_id%TYPE,
	        p_studysiteid IN VARCHAR2,
	        p_timeperiod  IN t613_study_period.c613_study_period_ds%TYPE,
	        p_out_details OUT TYPES.cursor_type)
	AS
	    v_form_id t622_patient_list.c601_form_id%TYPE;
	    v_question_id t623_patient_answer_list.c602_question_id%TYPE;
	    v_timeperiod t613_study_period.c613_study_period_ds%TYPE;
	BEGIN
	    -- to set the site values to context
	    my_context.set_my_inlist_ctx (p_studysiteid) ;
	    
	    -- to get the form and question id details
	    BEGIN
		    
	     SELECT t901.c902_code_nm_alt, t901.c901_code_nm
	       INTO v_form_id, v_question_id
	       FROM t901_code_lookup t901
	      WHERE t901.c901_code_id = get_rule_value (p_studyid, 'CAPS_GRP')
	      AND t901.c901_void_fl IS NULL;
	      
	    EXCEPTION WHEN NO_DATA_FOUND
	    THEN
	    	v_form_id := 0;
	    	v_question_id := 0;
	    END;
	    
	     -- to get the time period (t613) 
	    BEGIN
		    
	    SELECT DISTINCT c613_study_period_ds
	       INTO v_timeperiod
	       FROM t613_study_period
	      WHERE c612_study_list_id       IN (26505)
	        AND c613_study_period_id NOT IN (23)
	        AND c613_delete_fl = 'N'
	        AND c613_seq_no               = p_timeperiod;
	     
	     EXCEPTION WHEN OTHERS
	     THEN
	     	v_timeperiod := 0;
	     END;
	     
	     -- based on the filter - to get the patient satisfaction report
	         
	    OPEN p_out_details FOR SELECT 'A'||ROWNUM ID,
	    NVL (stype, NVL (sgroup, 'ALL PATIENTS')) NAME,
	    period,
	    dtrue,
	    DECODE (grpingid, 0, DECODE (sgroup, NULL, 2, grpingid), grpingid) groupingcount,
	    sseqno,
	    pseqno,
	    NVL (sgroup, DECODE (grpingid, 0, 'XXXX')) sgroup,
	    format_number ( ( (dtrue / tcount) * 100), 1) dtrue_per,
	    mtrue,
	    format_number ( ( (mtrue / tcount) * 100), 1) mtrue_per,
	    dknow,
	    format_number ( ( (dknow / tcount) * 100), 1) dknow_per,
	    mfalse,
	    format_number ( ( (mfalse / tcount) * 100), 1) mfalse_per,
	    dfalse,
	    format_number ( ( (dfalse / tcount) * 100), 1) dfalse_per FROM
	    (
	         SELECT t901.c902_code_nm_alt sgroup, t901.c901_code_nm stype, c613_study_period_ds period
	          , GROUPING_ID (t901.c901_code_nm, t901.c902_code_nm_alt) grpingid, SUM (DECODE (t623.c605_answer_list_id, 26701,
	            1, 0)) dtrue, SUM (DECODE (t623.c605_answer_list_id, 26702, 1, 0)) mtrue, SUM (DECODE (t623.c605_answer_list_id, 26703, 1, 0)) dknow
                , SUM (DECODE (t623.c605_answer_list_id, 26704, 1, 0)) mfalse, SUM
	            (DECODE (t623.c605_answer_list_id, 26705, 1, 0)) dfalse, SUM (1)
	            tcount, NVL (t901.c901_code_seq_no, 0) sseqno, c613_seq_no pseqno
	           FROM t622_patient_list t622, t623_patient_answer_list t623, v621_patient_study_list v621
	          , t901_code_lookup t901
	          WHERE t622.c601_form_id              = v_form_id
	            AND t622.c622_patient_list_id      = t623.c622_patient_list_id
	            AND NVL (t622.C622_DELETE_FL, 'N') = 'N'
	            AND NVL (t623.C623_DELETE_FL, 'N') = 'N'
	            AND v621.c621_patient_id           = t622.c621_patient_id
	            AND v621.c613_study_period_id      = t622.c613_study_period_id
	            AND v621.c901_surgery_type         = t901.c901_code_id
	            AND t623.c602_question_id          = v_question_id
	            AND c613_study_period_ds           = v_timeperiod
	            AND v621.c611_study_id             = p_studyid
	            AND v621.c621_surgery_comp_fl      = 'Y'
	            AND v621.c614_study_site_id       IN
	            (
	                 SELECT * FROM v_in_list
	            )
	       GROUP BY v621.c613_study_period_ds, c613_seq_no, ROLLUP (t901.c902_code_nm_alt, (t901.c901_code_seq_no,
	            t901.c901_code_nm))
	       ORDER BY period, c613_seq_no
	    )
	    WHERE sgroup IS NOT NULL OR stype IS NOT NULL OR grpingid <> 2 ORDER BY sgroup,
	    groupingcount,
	    sseqno,
	    pseqno;
	    
	END gm_cr_fch_amnios_patient_satisfaction;
	
	/*******************************************************
	* Description: This Procedure is used to fetch Amnois study primary endpoint details
	* based on study and score
	*
	*     PMT-43698: Amnios study - VAS report changes
	*******************************************************/
	PROCEDURE gm_cr_fch_amnios_primary_endpoint_dtls (
	        p_studyid     IN t611_clinical_study.c611_study_id%TYPE,
	        p_studysiteid IN VARCHAR2,
	        p_score       IN VARCHAR2,
	        p_out_header OUT TYPES.cursor_type,
	        p_out_details OUT TYPES.cursor_type)
	AS
	    v_form_id t622_patient_list.c601_form_id%TYPE;
	    v_question_id t623_patient_answer_list.c602_question_id%TYPE;
	    v_study_listid t612_study_form_list.c612_study_list_id%TYPE;
	    v_ans_grpid t623_patient_answer_list.c603_answer_grp_id%TYPE;
	    v_study_prdid1 NUMBER;
	    v_study_prdid2 NUMBER;
	    --
	    v_answer_list_id NUMBER;
	BEGIN
	    -- The input String in the study site ids details (101,102,103)
	    my_context.set_my_inlist_ctx (p_studysiteid) ;
	    
	    -- based on score id to get the question id
	    
	    BEGIN
	    --
	     SELECT c901_code_nm, DECODE (c901_code_id, 109382, 26303, 26304)
	       INTO v_question_id, v_answer_list_id
	       FROM t901_code_lookup
	      WHERE c901_code_id = p_score;
	      
	   EXCEPTION WHEN OTHERS
	   THEN
	   	v_question_id := NULL;
	   END;
	   
	    -- remove temp table records
	     DELETE
	       FROM my_temp_list;
	       
	    --
	     INSERT INTO my_temp_list
	        (my_temp_txn_id
	        )
	     SELECT c621_patient_id
	       FROM t623_patient_answer_list t623_f
	      WHERE c601_form_id              = 263
	        AND c602_question_id          = 26304
	        AND c605_answer_list_id       = v_answer_list_id
	        AND c623_delete_fl = 'N';
	        
	    --26303 Right
	    --26304 Left
	    
	    -- to get the header details
	        
	    OPEN p_out_header FOR
	    SELECT DISTINCT DECODE (c613_study_period_ds, 'Pre-Injection', 'PreInj', c613_study_period_ds) || '  <br>Avg'
	        period, c613_seq_no
	       FROM t613_study_period
	      WHERE c612_study_list_id = 26505
	      AND c613_delete_fl = 'N'
	      UNION
	    SELECT DISTINCT DECODE (c613_study_period_ds, 'Pre-Injection', 'PreInj', c613_study_period_ds) || ' <br>' ||
	        TO_CHAR (UNISTR ('\00B1')) || 'SD' period, c613_seq_no
	       FROM t613_study_period
	      WHERE c612_study_list_id = 26505
	      AND c613_delete_fl = 'N'
	   ORDER BY c613_seq_no;
	   
	   -- to get the primary endpoint details
	   
	   -- affected
	    OPEN p_out_details FOR SELECT 'A'||ROWNUM ID,
	    NVL (stype, NVL (sgroup, 'ALL Patients')) || ' Affected Foot' NAME,
	    DECODE (period, 'Pre-Injection', 'PreInj', period) period,
	    average val,
	    DECODE (grpingid, 0, DECODE (sgroup, NULL, 2, grpingid), grpingid) groupingcount,
	    stddv,
	    '1' || sseqno sseqno,
	    pseqno,
	    NVL (sgroup, DECODE (grpingid, 0, 'XXXX')) sgroup FROM
	    (
	         SELECT t901.c902_code_nm_alt sgroup, t901.c901_code_nm stype, c613_study_period_ds period
	          , c613_seq_no pseqno, format_number (AVG (TO_NUMBER (t623.c904_answer_ds)), 1) average, TO_CHAR (UNISTR (
	            '\00B1')) || TO_CHAR (Format_Number (Stddev (To_Number (T623.C904_Answer_Ds)), 1), '990D0') Stddv,
	            GROUPING_ID (t901.c901_code_nm, t901.c902_code_nm_alt) grpingid, NVL (t901.c901_code_seq_no, 0) sseqno
	           FROM t622_patient_list t622, v621_patient_study_list v621, t901_code_lookup t901
	          , t623_patient_answer_list t623
	          WHERE v621.c621_patient_id      = t622.c621_patient_id
	            AND v621.c901_surgery_type    = t901.c901_code_id
	            AND v621.c613_study_period_id = t622.c613_study_period_id
	            AND c612_study_list_id       = 26505
	            AND t622.c622_patient_list_id = t623.c622_patient_list_id
	            AND t622.c622_delete_fl = 'N'
	            AND t623.c623_delete_fl = 'N'
	            AND t622.c601_form_id              = 265
	            -- question
	            -- right 26501
	            -- left 26502
	            AND t623.c602_question_id     = v_question_id
	            AND v621.c611_study_id        = p_studyid
	            AND v621.c621_surgery_comp_fl = 'Y'
	            AND v621.c614_study_site_id  IN
	            (
	                 SELECT token FROM v_in_list
	            )
	            AND v621.c621_patient_id IN
	            (
	                 SELECT my_temp_txn_id FROM MY_TEMP_LIST
	            )
	       GROUP BY c613_study_period_ds, c613_seq_no, ROLLUP (t901.c902_code_nm_alt, (t901.c901_code_seq_no,
	            t901.c901_code_nm))
	       ORDER BY period, c613_seq_no
	    )
	      WHERE sgroup  IS NOT NULL
	        OR stype    IS NOT NULL
	        OR grpingid <> 2
	   ORDER BY sgroup, groupingcount, sseqno
	      , pseqno;
	      
	END gm_cr_fch_amnios_primary_endpoint_dtls;

	/*******************************************************
	* Description: This Procedure is used to fetch Reflect study Patient Satisfactions details
	*     PC-1809: Reflect study report changes
	*******************************************************/
	PROCEDURE gm_cr_fch_reflect_patient_satisfaction (
	        p_studyid     IN t611_clinical_study.c611_study_id%TYPE,
	        p_studysiteid IN VARCHAR2,
	        p_timeperiod  IN t613_study_period.c613_study_period_ds%TYPE,
	        p_out_details OUT TYPES.cursor_type) 
	        
	      AS
	    v_form_id t622_patient_list.c601_form_id%TYPE;
	    v_question_id t623_patient_answer_list.c602_question_id%TYPE;
	    v_timeperiod t613_study_period.c613_study_period_ds%TYPE;
	BEGIN
	    -- to set the site values to context
	    my_context.set_my_inlist_ctx (p_studysiteid) ;
	    
	    -- to get the form and question id details
	    BEGIN
		    
	     SELECT t901.c902_code_nm_alt, t901.c901_code_nm
	       INTO v_form_id, v_question_id
	       FROM t901_code_lookup t901
	      WHERE t901.c901_code_id = get_rule_value (p_studyid, 'CAPS_GRP')
	      AND t901.c901_void_fl IS NULL;
	      
	    EXCEPTION WHEN NO_DATA_FOUND
	    THEN
	    	v_form_id := 0;
	    	v_question_id := 0;
	    END;
	    
	     -- to get the time period (t613) 
	    BEGIN
		    
	    SELECT DISTINCT c613_study_period_ds
	       INTO v_timeperiod
	       FROM t613_study_period
	      WHERE c612_study_list_id       IN (27606)
	        AND c613_study_period_id  IN (179298)
	        AND c613_delete_fl = 'N'
	        AND c613_seq_no               = p_timeperiod;
	     
	     EXCEPTION WHEN OTHERS
	     THEN
	     	v_timeperiod := 0;
	     END;
	     
	     -- based on the filter - to get the patient satisfaction report
	         
	    OPEN p_out_details FOR SELECT 'A'||ROWNUM ID,
	    NVL (stype, NVL (sgroup, 'ALL PATIENTS')) NAME,
	    period,
	    dtrue,
	    DECODE (grpingid, 0, DECODE (sgroup, NULL, 2, grpingid), grpingid) groupingcount,
	    sseqno,
	    pseqno,
	    NVL (sgroup, DECODE (grpingid, 0, 'XXXX')) sgroup,
	    format_number ( ( (dtrue / tcount) * 100), 1) dtrue_per,
	    mtrue,
	    format_number ( ( (mtrue / tcount) * 100), 1) mtrue_per,
	    dknow,
	    format_number ( ( (dknow / tcount) * 100), 1) dknow_per,
	    mfalse,
	    format_number ( ( (mfalse / tcount) * 100), 1) mfalse_per,
	    dfalse,
	    format_number ( ( (dfalse / tcount) * 100), 1) dfalse_per FROM
	    (
	         SELECT t901.c902_code_nm_alt sgroup, t901.c901_code_nm stype, c613_study_period_ds period
	          , GROUPING_ID (t901.c901_code_nm, t901.c902_code_nm_alt) grpingid, SUM (DECODE (t623.c605_answer_list_id, 27608,
	            1, 0)) dtrue, SUM (DECODE (t623.c605_answer_list_id, 27616, 1, 0)) mtrue, SUM (DECODE (t623.c605_answer_list_id, 26703, 1, 0)) dknow
                , SUM (DECODE (t623.c605_answer_list_id, 26704, 1, 0)) mfalse, SUM
	            (DECODE (t623.c605_answer_list_id, 26705, 1, 0)) dfalse, SUM (1)
	            tcount, NVL (t901.c901_code_seq_no, 0) sseqno, c613_seq_no pseqno
	           FROM t622_patient_list t622, t623_patient_answer_list t623, v621_patient_study_list v621
	          , t901_code_lookup t901
	          WHERE t622.c601_form_id              = v_form_id
	            AND t622.c622_patient_list_id      = t623.c622_patient_list_id
	            AND NVL (t622.C622_DELETE_FL, 'N') = 'N'
	            AND NVL (t623.C623_DELETE_FL, 'N') = 'N'
	            AND v621.c621_patient_id           = t622.c621_patient_id
	            AND v621.c613_study_period_id      = t622.c613_study_period_id
	            AND v621.c901_surgery_type         = t901.c901_code_id
	            AND t623.c602_question_id          = v_question_id
	            AND c613_study_period_ds           = v_timeperiod
	            AND v621.c611_study_id             = p_studyid
	            AND v621.c621_surgery_comp_fl      = 'Y'
	            AND v621.c614_study_site_id       IN
	            (
	                 SELECT * FROM v_in_list
	            )
	       GROUP BY v621.c613_study_period_ds, c613_seq_no, ROLLUP (t901.c902_code_nm_alt, (t901.c901_code_seq_no,
	            t901.c901_code_nm))
	       ORDER BY period, c613_seq_no
	    )
	    WHERE sgroup IS NOT NULL OR stype IS NOT NULL OR grpingid <> 2 ORDER BY sgroup,
	    groupingcount,
	    sseqno,
	    pseqno;
		        
		        
		        
	END gm_cr_fch_reflect_patient_satisfaction; 
		        
	/******************************************************************
	* Description : Procedure to fetch Reflect study FFI form details (Outcome report)
	* 				PC-1809: Other outcome report changes (Reflect study)
	****************************************************************/
	PROCEDURE gm_cr_fch_reflect_outcome_score (
        p_studyid     IN t611_clinical_study.c611_study_id%TYPE,
        p_studysiteid IN VARCHAR2,
        p_type        IN t901_code_lookup.c902_code_nm_alt%TYPE,
        p_out_header OUT TYPES.cursor_type,
        p_out_details OUT TYPES.cursor_type)
	AS
	    v_form_id t622_patient_list.c601_form_id%TYPE;
	    v_question_id t623_patient_answer_list.c602_question_id%TYPE;
	    v_ans_id VARCHAR (10) ;
		v_cnt NUMBER;
	BEGIN
	    -- to set the site id to context
	    my_context.set_my_inlist_ctx (p_studysiteid) ;
	    
		SELECT COUNT (1)
		  INTO v_cnt
		  FROM v_in_list
		 WHERE token IS NOT NULL;
		 
	    -- To fetch the Form and quetion details
	    BEGIN
		    
	     SELECT t901.c902_code_nm_alt, t901.c901_code_nm, get_rule_value (p_type, 'FCH_OUT_SCR')
	       INTO v_form_id, v_question_id, v_ans_id
	       FROM t901_code_lookup t901
	      WHERE t901.c901_code_id = p_type
	       AND c901_void_fl IS NULL;
	       
	     EXCEPTION WHEN NO_DATA_FOUND
	     THEN
	     	v_form_id := 0;
	     	v_question_id := 0;
	     	v_ans_id := 0;
	     END;
	     
	     -- to fetch Header details 
	     
	    OPEN p_out_header FOR
	    SELECT DISTINCT c613_study_period_ds || '  <br>Avg' period, c613_seq_no
	       FROM t613_study_period
	      WHERE c612_study_list_id IN (27606)
	      AND c613_delete_fl = 'N'
	      UNION
	    SELECT DISTINCT c613_study_period_ds || ' <br>' || TO_CHAR (UNISTR ('\00B1')) || 'SD' period, c613_seq_no
	       FROM t613_study_period
	      WHERE c612_study_list_id IN (27606)
	      AND c613_delete_fl = 'N'
	   ORDER BY c613_seq_no;
	   
	   -- To fetch details cursor
	    OPEN p_out_details FOR
	    SELECT DISTINCT 'A'||ROWNUM ID, NVL (stype, NVL (sgroup, 'ALL PATIENTS')) NAME, speriod
	    	, NVL (avgval, 0) averageval, DECODE (grpingid, 0, DECODE (sgroup, NULL, 2, grpingid),
	        grpingid) groupingcount, (TO_CHAR (UNISTR ('\00B1')) || stdevval) stdevval, sseqno
	      , pseqno, NVL (sgroup, DECODE (grpingid, 0, 'XXXX')) sgroup
	       FROM
	        (
	             SELECT t901.c902_code_nm_alt sgroup, t901.c901_code_nm stype, v621.c613_study_period_ds speriod
	              , GROUPING_ID (t901.c901_code_nm, t901.c902_code_nm_alt) grpingid, v621.c613_seq_no pseqno, format_number
	                (AVG (c904_answer_ds), 1) avgval, format_number (STDDEV (c904_answer_ds), 1) stdevval, NVL (
	                t901.c901_code_seq_no, 0) sseqno
	               FROM t622_patient_list t622, t623_patient_answer_list t623, v621_patient_study_list v621
	              , t901_code_lookup t901
	              WHERE t622.c601_form_id              = v_form_id
	                AND t622.c622_patient_list_id      = t623.c622_patient_list_id
	                AND NVL (t622.C622_DELETE_FL, 'N') = 'N'
	                AND NVL (t623.C623_DELETE_FL, 'N') = 'N'
	                AND v621.c621_patient_id           = t622.c621_patient_id
	                AND v621.c613_study_period_id      = t622.c613_study_period_id
	                AND v621.c901_surgery_type         = t901.c901_code_id
	                AND t623.c602_question_id          = v_question_id
	                AND t623.c904_answer_ds           IS NOT NULL
	                AND t623.c603_answer_grp_id        = DECODE (v_ans_id, '', t623.c603_answer_grp_id, NULL, t623.c603_answer_grp_id, v_ans_id)
	                AND v621.c611_study_id        = p_studyid
	                AND v621.c621_surgery_comp_fl = 'Y'
					AND (	('-999' = DECODE (v_cnt, 0, '-999', '-9999'))
						OR v621.c614_study_site_id  IN ( SELECT * FROM v_in_list)
						)
	           GROUP BY c613_study_period_ds, c613_seq_no, ROLLUP (t901.c902_code_nm_alt, (t901.c901_code_seq_no,
	                t901.c901_code_nm))
	           ORDER BY c613_study_period_ds, c613_seq_no
	        )
	      WHERE sgroup  IS NOT NULL
	        OR stype    IS NOT NULL
	        OR grpingid <> 2
	   ORDER BY sgroup, groupingcount, sseqno
	      , pseqno;
	      
	END gm_cr_fch_reflect_outcome_score;		        

	/*******************************************************
	 * Purpose: This Procedure is used to fetch the Dynamic Other outcome data
	 *******************************************************/
	--
    PROCEDURE gm_cr_fch_dynamic_outcome_score (
        p_studyid       IN       t611_clinical_study.c611_study_id%TYPE
      , p_studysiteid   IN       VARCHAR2
      , p_type          IN       t901_code_lookup.c902_code_nm_alt%TYPE
      , p_out_header    OUT      TYPES.cursor_type
      , p_out_details   OUT      TYPES.cursor_type
    )
    AS
    v_dynamic_proc_value t906_rules.c906_rule_value%TYPE;
    v_exec_string VARCHAR2 (300);
    BEGIN
	    
	    -- Dynamic Code for void functionalities added
			SELECT get_rule_value (p_studyid, 'CLI_DYN_OUTCOME_RPT')
			  INTO v_dynamic_proc_value
			  FROM DUAL;

			IF v_dynamic_proc_value IS NULL
			THEN
				GM_RAISE_APPLICATION_ERROR('-20999','58','');
			END IF;
			
			v_exec_string := 'BEGIN ' || v_dynamic_proc_value || '(:p_studyid, :p_studysiteid, :p_type, :p_out_header, :p_out_details); END;';

		EXECUTE IMMEDIATE (v_exec_string)  USING p_studyid,
	    p_studysiteid, p_type, 
	    OUT p_out_header, OUT p_out_details;
	    
	    END gm_cr_fch_dynamic_outcome_score;
	    
END gm_pkg_cr_outcome_rpt;
/