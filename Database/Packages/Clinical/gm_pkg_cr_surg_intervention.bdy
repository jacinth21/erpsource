--@"C:\Database\Packages\Clinical\gm_pkg_cr_surg_intervention.bdy";
CREATE OR REPLACE
PACKAGE body gm_pkg_cr_surg_intervention
IS
    /*************************************************************************
    * Description : This procedure is used to fetch Surgical Intervention details by patient id
    * Author      : Dhana Reddy
    *************************************************************************/
PROCEDURE gm_fch_surgical_inv (
        p_surg_inv_no     IN t629_surgical_intervention.c629_surgical_inv_no%TYPE,
        p_patient_list_id IN t622_patient_list.c622_patient_list_id%TYPE,
        p_out_surgical_info OUT types.cursor_type,
        p_out_verify_info OUT types.cursor_type)
AS
BEGIN
    OPEN p_out_surgical_info 
    FOR SELECT c629_surgical_inv_no surginvno, get_code_name (c901_type) surgtypenm, c901_type
    surgtypeid, TO_CHAR (c629_surgery_date, 'MM/DD/YYYY') surgdt, get_code_name (c629_original_level_inv) orglvlinvnm,
    c629_original_level_inv orglvlinvid, c629_verify_fl verifyfl 
     FROM t629_surgical_intervention 
    WHERE c629_surgical_inv_no = p_surg_inv_no 
      AND c629_delete_fl = 'N';
    
    OPEN p_out_verify_info 
    FOR SELECT T625.C622_PATIENT_LIST_ID PATLISTID, TO_CHAR (T625.C625_DATE, 'MM/DD/YYYY')
    VERDATE, GET_USER_NAME (T625.C101_USER_ID) CUSER, GET_CODE_NAME (T625.C901_VERIFY_LEVEL) VLEVEL, T902.C902_COMMENTS
    CMNTS, T625.C901_VERIFY_LEVEL VLEVEL_ID 
     FROM T625_PATIENT_VERIFY_LIST T625, T902_LOG T902 
    WHERE T625.C622_PATIENT_LIST_ID = p_patient_list_id 
      AND T902.C902_REF_ID (+) = TO_CHAR (T625.C625_PAT_VERIFY_LIST_ID) 
      AND C902_TYPE                 = '1281' 
      AND NVL (t625.c625_delete_fl, 'N') = 'N' 
 ORDER BY C625_PAT_VERIFY_LIST_ID;
 
END gm_fch_surgical_inv;

/*********************************************************************
* Description : This procedure is used to void Surgical Intervention
* Author      : Dhana Reddy
*********************************************************************/
PROCEDURE gm_void_surgical_inv (
        p_surg_inv_no IN t629_surgical_intervention.c629_surgical_inv_no%TYPE,
        p_user_id     IN t101_user.c101_user_id%TYPE)
AS
    v_delete_fl t629_surgical_intervention.c629_delete_fl%TYPE;
BEGIN
     SELECT c629_delete_fl
       INTO v_delete_fl
       FROM t629_surgical_intervention
      WHERE c629_surgical_inv_no = p_surg_inv_no FOR UPDATE OF c629_delete_fl;
      
    IF v_delete_fl               = 'Y' THEN
        --This Surgical Intervention has already been voided.
        raise_application_error ( - 20579, '') ;
    END IF;
     UPDATE t629_surgical_intervention
    SET c629_delete_fl           = 'Y', c629_last_updated_by = p_user_id, c629_last_updated_date = SYSDATE
      WHERE c629_surgical_inv_no = p_surg_inv_no;
      
END gm_void_surgical_inv;

/*************************************************************************
* Description : This procedure is used to create/verify the Surgical Intervention
* Author      : Dhana Reddy
*************************************************************************/
PROCEDURE gm_sav_surgical_inv (
        p_surg_inv_no    IN t629_surgical_intervention.c629_surgical_inv_no%TYPE,
        p_patient_ide_no IN t621_patient_master.c621_patient_ide_no%TYPE,
        p_type           IN t629_surgical_intervention.c901_type%TYPE,
        p_org_level      IN t629_surgical_intervention.c629_original_level_inv%type,
        p_surgery_date   IN t629_surgical_intervention.c629_surgery_date%TYPE,
        p_verify_fl      IN t629_surgical_intervention.c629_verify_fl%TYPE,        
        p_comments       IN t902_log.c902_comments%TYPE,
        p_user_id        IN t629_surgical_intervention.c629_created_by%TYPE,
        p_out_user_nm OUT VARCHAR2,
        p_out_created_dt OUT VARCHAR2,
        p_out_treatment_type  OUT VARCHAR2,
        p_out_study_type  OUT VARCHAR2
        )
AS
    v_patient_id t621_patient_master.c621_patient_id%TYPE;
    v_surg_inv_no t629_surgical_intervention.c629_surgical_inv_no%TYPE;
    v_intervention_cnt NUMBER;
    v_user_nm          VARCHAR2 (100) ;
    v_surgery_date DATE;
    v_updated_by t629_surgical_intervention.c629_created_by%TYPE;
    v_created_by t629_surgical_intervention.c629_created_by%TYPE;
    v_patient_list_id NUMBER;
    v_verify_list_id  NUMBER;
    v_verify_level    NUMBER:=6190;--Created By
    v_msg             VARCHAR2 (1000);
    v_treatment       t621_patient_master.c901_surgery_type%TYPE;
    v_created_date    DATE;
    v_treatment_name  VARCHAR2 (1000);
    v_study           t621_patient_master.c611_study_id%TYPE;
    v_study_name      VARCHAR2 (1000);    
BEGIN
    -- fetch the patient id and surgery date
    BEGIN
         SELECT c621_patient_id, c621_surgery_date,c901_surgery_type,get_code_name(c901_surgery_type),c611_study_id,get_study_name(c611_study_id)
           INTO v_patient_id, v_surgery_date, v_treatment,v_treatment_name,v_study,v_study_name
           FROM t621_patient_master
          WHERE c621_patient_ide_no = p_patient_ide_no
            AND c621_delete_fl      = 'N';
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_patient_id   := NULL;
        v_surgery_date := NULL;
    END;
    
     IF p_surgery_date > SYSDATE THEN
        --Secondary surgery date should not be greater than current date.
        raise_application_error ( - 20584, '') ;
    END IF;
    
    IF v_surgery_date > p_surgery_date THEN
        --Secondary surgery date should be greater than primary surgery date.
        raise_application_error ( - 20581, '') ;
    END IF;
    
    -- Same person cannot verify his work
    IF p_surg_inv_no IS NOT NULL THEN
         SELECT c629_created_by, c629_last_updated_by,c622_patient_list_id,c629_created_date
           INTO v_created_by, v_updated_by,v_patient_list_id ,v_created_date
           FROM t629_surgical_intervention
          WHERE c629_surgical_inv_no = p_surg_inv_no
            AND c629_delete_fl       = 'N';
        IF ( (v_created_by           = p_user_id AND v_updated_by IS NULL AND p_verify_fl = 'Y') OR
            (
                v_updated_by IS NOT NULL AND v_updated_by = p_user_id AND p_verify_fl = 'Y'
            )
            ) THEN
            raise_application_error ( - 20582, '') ;
            -- This Surgical Intervention needs to be verified by a different user.
        END IF;
    END IF;
    
     UPDATE t629_surgical_intervention
    SET c901_type                = p_type, c629_original_level_inv = p_org_level, c629_surgery_date = p_surgery_date,
        c621_patient_id          = v_patient_id, c629_verify_fl = p_verify_fl, c629_last_updated_by = p_user_id,
        c629_last_updated_date   = sysdate
      WHERE c629_surgical_inv_no = p_surg_inv_no
        AND c629_delete_fl       = 'N';
        
    IF SQL%ROWCOUNT = 0 THEN
         SELECT s_surgical_intervention.NEXTVAL INTO v_surg_inv_no FROM DUAL;
         SELECT s622_patient_list.NEXTVAL INTO v_patient_list_id FROM DUAL;
         INSERT
           INTO t622_patient_list
            (
                c622_patient_list_id, c622_date, c622_initials,
                c601_form_id, c621_patient_id, c613_study_period_id,
                c622_created_by, c622_created_date, c901_type,
                c622_delete_fl, c621_cal_value, c622_missing_follow_up_fl,
                c622_verify_fl, c622_patient_event_no
            )
            VALUES
            (
                v_patient_list_id, p_surgery_date, NULL,
                215, v_patient_id, 872,
                p_user_id, SYSDATE, 6151,
                'N', NULL, NULL,
                NULL, NULL
            ) ;
         INSERT
           INTO t629_surgical_intervention
            (
                c629_surgical_inv_no, c901_type, c629_original_level_inv,
                c629_surgery_date, c621_patient_id, c629_delete_fl,
                c622_patient_list_id, c629_verify_fl, c629_created_by,
                c629_created_date
            )
            VALUES
            (
                v_surg_inv_no, p_type, p_org_level,
                p_surgery_date, v_patient_id, 'N',
                v_patient_list_id, 'N', p_user_id,
                sysdate
            ) ;
    END IF;
    -- duplicate surgery check
     SELECT COUNT (1)
       INTO v_intervention_cnt
       FROM t629_surgical_intervention
      WHERE c901_type               = p_type
        AND c629_original_level_inv = p_org_level
        AND c629_surgery_date       = p_surgery_date
        AND c621_patient_id         = v_patient_id
        AND c629_delete_fl          = 'N';
        
    IF v_intervention_cnt           > 1 THEN
        --Record with this type,level and sugery date exist for this patient.
        raise_application_error ( - 20580, '') ;
    END IF;
    
    -- to save verify history
     SELECT s625_patient_verify.NEXTVAL
     INTO v_verify_list_id
     FROM DUAL;
     
     IF p_surg_inv_no IS NOT NULL AND p_verify_fl = 'Y'
     THEN
     	v_verify_level:=6192; --Verified 
     ELSIF p_surg_inv_no IS NOT NULL AND p_verify_fl = 'N'
     THEN
        v_verify_level:=6193; -- Modified correction
     END IF;
--
   INSERT INTO t625_patient_verify_list
               (c625_pat_verify_list_id, c622_patient_list_id, c101_user_id,
                c625_date, c901_verify_level
               )
        VALUES (v_verify_list_id, v_patient_list_id, p_user_id,
                TRUNC (SYSDATE), v_verify_level
               );

   -- Code to save the user entered comments information

   gm_update_log (v_verify_list_id, p_comments, 1281, p_user_id, v_msg);
   
   --get user name and created date to send email
   
   p_out_user_nm := get_user_name (p_user_id);
   
   p_out_created_dt := TO_CHAR(NVL(v_created_date,sysdate),'MM/DD/YYYY') ;
   
   p_out_treatment_type := v_treatment_name;
   
   p_out_study_type := v_study_name;
   
END gm_sav_surgical_inv;

/************************************************************************
* Description : This procedure is used fetch Surgical Intervention list
* Author      : Dhana Reddy
*************************************************************************/
PROCEDURE gm_fch_surgical_inv_rpt (
        p_study_id       IN t611_clinical_study.c611_study_id%TYPE,
        p_site_id        IN t614_study_site.c614_study_site_id%TYPE,
        p_patient_ide_no IN t621_patient_master.c621_patient_ide_no%TYPE,
        p_verify_fl      IN VARCHAR2,
        p_out_surgical_inv_rpt OUT TYPES.cursor_type)
AS
    v_patient_id t621_patient_master.c621_patient_id%TYPE;
BEGIN
    BEGIN
         SELECT c621_patient_id
           INTO v_patient_id
           FROM t621_patient_master
          WHERE c621_patient_ide_no = p_patient_ide_no
            AND c621_delete_fl      = 'N';
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_patient_id   := NULL;
    END;
    OPEN p_out_surgical_inv_rpt 
    FOR
    SELECT t629.c629_surgical_inv_no surginvno,
        get_account_name (t621.c704_account_id) sitename,
        get_code_name (T621.C901_SURGERY_TYPE) treatment, 
        t621.c621_patient_ide_no patientideno,
        get_party_name(t621.c101_surgeon_id)surgeon, 
        get_code_name (t629.c629_original_level_inv) orglvlinv,
        get_code_name (t629.c901_type) invtype,
        TO_CHAR (t621.c621_surgery_date, 'MM/DD/YYYY') orgsurgdt,
        TO_CHAR (t629.c629_surgery_date, 'MM/DD/YYYY') secsurgdt,
        (TRUNC (t629.c629_surgery_date) - TRUNC (t621.c621_surgery_date)) ptdays,
        t629.c629_verify_fl verifyfl,t629.c622_patient_list_id patientlid
     FROM t621_patient_master t621, t629_surgical_intervention t629,t614_study_site t614 
    WHERE t621.c621_patient_id = t629.c621_patient_id 
      AND t614.c611_study_id = t621.c611_study_id
      AND t621.c704_account_id   = t614.c704_account_id 
      AND t621.c611_study_id = p_study_id 
      AND t614.c614_site_id  = NVL (p_site_id, t614.c614_site_id) 
      AND t629.c621_patient_id = NVL(v_patient_id,t629.c621_patient_id)
      AND t629.c629_verify_fl = NVL(p_verify_fl,t629.c629_verify_fl)
      AND t621.c621_delete_fl = 'N'
      AND t629.c629_delete_fl  = 'N'
 ORDER BY t621.c704_account_id,t621.c621_patient_ide_no,t629.c629_surgical_inv_no;

END gm_fch_surgical_inv_rpt;

END gm_pkg_cr_surg_intervention;
/