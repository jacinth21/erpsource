/* Formatted on 2010/05/13 18:33 (Formatter Plus v4.8.0) */
--@"C:\database\Packages\Clinical\gm_pkg_cr_site_monitor.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_cr_site_monitor
IS
	PROCEDURE gm_cr_sav_site_monitor (
		p_site_id		  IN	   t617_site_monitor.c614_study_site_id%TYPE
	  , p_study_id		  IN	   t617_site_monitor.c611_study_id%TYPE
	  , p_surgeon_id	  IN	   VARCHAR
	  , p_visitdt		  IN	   VARCHAR
	  , p_purpose		  IN	   t617_site_monitor.c901_purpose%TYPE
	  , p_notes 		  IN	   t617_site_monitor.c617_notes%TYPE
	  , p_status		  IN	   t617_site_monitor.c901_status%TYPE
	  , p_monitor_owner   IN	   t617_site_monitor.c101_monitor_owner%TYPE
	  , p_user_id		  IN	   t617_site_monitor.c617_created_by%TYPE
	  , p_monitor_list	  IN	   VARCHAR
	  , p_no_patient	  IN	   NUMBER
	  , p_site_mtr_id	  IN OUT   VARCHAR
	)
	AS
		v_no_patient   t617_site_monitor.c617_no_of_patient%TYPE;
		v_sm_id 	   t617_site_monitor.c617_site_mtr_id%TYPE;
		v_surgeon_id   VARCHAR2 (10);
	BEGIN
--SURGEON ID'S REFERENTIAL INTE. NEEDS TO BE REMOVED
		IF p_site_mtr_id IS NULL
		THEN
			SELECT s617_site_monitor.NEXTVAL
			  INTO p_site_mtr_id
			  FROM DUAL;

			IF p_surgeon_id IS NULL
			THEN
				BEGIN
					SELECT t615.c101_party_id
					  INTO v_surgeon_id
					  FROM t615_site_surgeon t615
					 WHERE t615.c614_study_site_id = p_site_id AND t615.c901_surgeon_type = '60200';
				EXCEPTION
					WHEN NO_DATA_FOUND
					THEN
						v_surgeon_id := '';
				END;
			END IF;

			-- Patient Count needs to be calculated when the monitor is created.
			-- Not applicable during update of Site Monitor.
			SELECT COUNT (t621.c621_patient_id)
			  INTO v_no_patient
			  FROM t621_patient_master t621, t614_study_site t614
			 WHERE t621.c704_account_id = t614.c704_account_id
			   AND t621.c611_study_id = t614.c611_study_id
			   AND t614.c614_study_site_id = p_site_id
			   AND t614.c611_study_id = p_study_id
			   AND t621.c621_delete_fl = 'N'
			   AND t614.c614_void_fl IS NULL;

			INSERT INTO t617_site_monitor
						(c617_site_mtr_id, c614_study_site_id, c611_study_id
					   , c101_surgeon_id
					   , c617_visit_dt, c901_purpose, c617_notes, c901_status, c617_no_of_patient
					   , c101_monitor_owner, c617_created_by, c617_created_date
						)
				 VALUES (p_site_mtr_id, p_site_id, p_study_id
					   , DECODE (p_surgeon_id, NULL, v_surgeon_id, TO_NUMBER (p_surgeon_id))
					   , TO_DATE (p_visitdt, 'MM/DD/YYYY'), p_purpose, p_notes, p_status, v_no_patient
					   , p_monitor_owner, p_user_id, SYSDATE
						);
		ELSIF (p_status = 60462 OR p_status = 60463)
		THEN
			UPDATE t617_site_monitor
			   SET c901_status = p_status
				 , c617_last_updated_by = p_user_id
				 , c617_last_updated_date = SYSDATE
			 WHERE c617_site_mtr_id = TO_NUMBER (p_site_mtr_id) AND c617_void_fl IS NULL;
		ELSE
			UPDATE t617_site_monitor
			   SET c617_visit_dt = TO_DATE (p_visitdt, 'MM/DD/YYYY')
				 , c901_purpose = p_purpose
				 , c617_notes = p_notes
				 , c901_status = p_status
				 , c101_monitor_owner = p_monitor_owner
				 , c617_last_updated_by = p_user_id
				 , c617_last_updated_date = SYSDATE
			 WHERE c617_site_mtr_id = TO_NUMBER (p_site_mtr_id) AND c617_void_fl IS NULL;
		END IF;

		IF p_monitor_list IS NOT NULL
		THEN
			gm_cr_sav_site_monitor_map (p_monitor_list, p_site_mtr_id);
		END IF;
	END gm_cr_sav_site_monitor;

	PROCEDURE gm_cr_sav_site_monitor_map (
		p_monitor_list	 IN   VARCHAR
	  , p_site_mtr_id	 IN   t617_site_monitor.c617_site_mtr_id%TYPE
	)
	AS
		v_email_id	   VARCHAR2 (4000) := '';

		CURSOR cur_cra
		IS
			SELECT token cra_id
			  FROM v_in_list;
	BEGIN
		my_context.set_my_inlist_ctx (p_monitor_list);

		DELETE FROM t618_site_monitor_mapping
			  WHERE c617_site_mtr_id = p_site_mtr_id;

		FOR cra_val IN cur_cra
		LOOP
			INSERT INTO t618_site_monitor_mapping
						(c618_site_mtr_map_id, c617_site_mtr_id, c101_cra_id
						)
				 VALUES (s618_site_monitor_mapping.NEXTVAL, p_site_mtr_id, cra_val.cra_id
						);
		END LOOP;
	END gm_cr_sav_site_monitor_map;

	PROCEDURE gm_cr_fch_site_monitor (
		p_site_mtr_id	  IN	   t617_site_monitor.c617_site_mtr_id%TYPE
	  , p_study_id		  IN	   t617_site_monitor.c611_study_id%TYPE
	  , p_study_site_id   IN	   t617_site_monitor.c614_study_site_id%TYPE
	  , p_purpose		  IN	   t617_site_monitor.c901_purpose%TYPE
	  , p_status		  IN	   t617_site_monitor.c901_status%TYPE
	  , p_fromdate		  IN	   VARCHAR
	  , p_todate		  IN	   VARCHAR
	  , p_cra_id		  IN	   t617_site_monitor.c101_monitor_owner%TYPE
	  , p_sitemonitor	  OUT	   TYPES.cursor_type
	  , p_sitemtrmap	  OUT	   TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_sitemonitor
		 FOR
			 SELECT c617_site_mtr_id sitemtrid, TO_CHAR (c617_visit_dt, 'mm/dd/yyyy') datevisited
				  , c101_monitor_owner ownerofvisit, get_user_name (c101_monitor_owner) owner
				  , get_account_name (t614.c704_account_id) acc_name, c617_notes notesofvisit, c101_surgeon_id
				  , get_party_name (c101_surgeon_id) party_name, c617_no_of_patient patient_count
				  , c901_purpose purposeofvisit, c901_status sitemonitorstatus
				  , gm_pkg_cr_site_monitor.get_no_of_observations (c617_site_mtr_id, NULL) obs
				  , gm_pkg_cr_site_monitor.get_no_of_observations (c617_site_mtr_id, 60471) pending_obs
				  , get_log_flag (c617_site_mtr_id, 1257) logflag
			   FROM t617_site_monitor t617, t614_study_site t614
			  WHERE t614.c614_study_site_id = t617.c614_study_site_id
				AND t617.c611_study_id = NVL (p_study_id, t617.c611_study_id)
				AND t617.c614_study_site_id = NVL (p_study_site_id, t617.c614_study_site_id)
				AND t617.c901_purpose = NVL (p_purpose, t617.c901_purpose)
				AND t617.c901_status = NVL (p_status, t617.c901_status)
				AND t617.c617_visit_dt >=
							  CASE
								  WHEN p_fromdate IS NOT NULL
									  THEN TO_DATE (p_fromdate, 'MM/DD/YYYY')
								  ELSE t617.c617_visit_dt
							  END
				AND t617.c617_visit_dt <=
								  CASE
									  WHEN p_todate IS NOT NULL
										  THEN TO_DATE (p_todate, 'MM/DD/YYYY')
									  ELSE t617.c617_visit_dt
								  END
				AND t617.c617_void_fl IS NULL
				AND t617.c617_site_mtr_id = NVL (p_site_mtr_id, c617_site_mtr_id);

		OPEN p_sitemtrmap
		 FOR
			 SELECT c101_cra_id ID
			   FROM t618_site_monitor_mapping
			  WHERE c617_site_mtr_id = p_site_mtr_id;
	END gm_cr_fch_site_monitor;

	PROCEDURE gm_cr_email_cra (
		p_subject		IN	 VARCHAR
	  , p_site_mtr_id	IN	 VARCHAR
	  , p_type			IN	 VARCHAR
	)
	AS
		v_email_id	   VARCHAR2 (4000);
		v_name		   VARCHAR2 (50);
		v_study_site_id t614_study_site.c614_study_site_id%TYPE;
		v_username	   VARCHAR2 (200);
		v_date		   VARCHAR2 (10);
		v_msg		   VARCHAR2 (1000);

		CURSOR cra_cur
		IS
			SELECT c101_cra_id cra_id
			  FROM t618_site_monitor_mapping
			 WHERE c617_site_mtr_id = p_site_mtr_id;
	BEGIN
		FOR cra_val IN cra_cur
		LOOP
			v_email_id	:= v_email_id || get_user_emailid (cra_val.cra_id) || ',';
		END LOOP;

		v_email_id	:= v_email_id || get_rule_value ('SMT_NOTIF_USERS', 'SMT_NOTIF_USERS');

		SELECT c614_study_site_id
			 , get_user_name (DECODE (c617_created_by, NULL, c617_last_updated_by, c617_created_by))
			 , TO_CHAR (c617_visit_dt, 'MM/DD/YYYY')
		  INTO v_study_site_id
			 , v_username
			 , v_date
		  FROM t617_site_monitor
		 WHERE c617_site_mtr_id = p_site_mtr_id AND c617_void_fl IS NULL;

		SELECT c614_site_id || '-' || get_sh_account_name (c704_account_id)
		  INTO v_name
		  FROM t614_study_site
		 WHERE c614_study_site_id = v_study_site_id;

		v_msg		:=
			   'A Site Monitoring visit for '
			|| v_name
			|| ' has been '
			|| get_code_name (p_type)
			|| ' by '
			|| v_username
			|| ' for '
			|| v_date
			|| '.';
		gm_com_send_email_prc (v_email_id, p_subject, v_msg);
	END gm_cr_email_cra;

	PROCEDURE gm_cr_reminder_email
	AS
--
   -- 6 month visit logic
		CURSOR set_6_month_cursor
		IS
			SELECT *
			  FROM (SELECT t614.c614_study_site_id, get_account_name (t614.c704_account_id) aname
						 , get_user_name (t614.c101_user_id) cra, get_user_emailid (t614.c101_user_id) toemail
						 , schedule_6_month.last_visit_date
						 , MONTHS_BETWEEN (TRUNC (SYSDATE)
										 , NVL (schedule_6_month.last_visit_date, t614.c614_irb_approval_date)
										  ) lastvisit
						 , ADD_MONTHS (NVL (schedule_6_month.last_visit_date, t614.c614_irb_approval_date), 6) duedate
					  FROM t614_study_site t614
						 , (SELECT	 t617.c614_study_site_id, MAX (t617.c617_visit_dt) last_visit_date
								FROM t617_site_monitor t617
							   WHERE t617.c617_void_fl IS NULL AND t617.c901_purpose = 60451
							GROUP BY t617.c614_study_site_id) schedule_6_month
					 WHERE t614.c614_void_fl IS NULL AND t614.c614_study_site_id = schedule_6_month.c614_study_site_id(+))
			 WHERE lastvisit > 6;

		-- 10th patient visit logic
		CURSOR set_10_patient_cursor
		IS
			SELECT *
			  FROM (SELECT patient_list.c614_study_site_id, aname, toemail, currentcount
						 , NVL (last_visit_count, 0) last_visit_count
						 , (FLOOR (currentcount / 10) - FLOOR (NVL (last_visit_count, 0) / 10)) visitdiff
					  FROM (SELECT	 t614.c614_study_site_id, get_account_name (t614.c704_account_id) aname
								   , get_user_emailid (t614.c101_user_id) toemail, COUNT (1) currentcount
								FROM t621_patient_master t621, t614_study_site t614
							   WHERE c621_delete_fl = 'N'
								 AND t614.c704_account_id = t621.c704_account_id
								 AND t621.c611_study_id = t614.c611_study_id
								 AND t614.c614_void_fl IS NULL
							GROUP BY t614.c614_study_site_id, t614.c704_account_id, t614.c101_user_id) patient_list
						 , (SELECT	 t617.c614_study_site_id, MAX (t617.c617_no_of_patient) last_visit_count
								FROM t617_site_monitor t617
							   WHERE t617.c617_void_fl IS NULL AND t617.c901_purpose = 60452
							GROUP BY t617.c614_study_site_id) schedule_10_patient
					 WHERE patient_list.c614_study_site_id = schedule_10_patient.c614_study_site_id(+))
			 WHERE visitdiff > 0;

		--
		v_email_cc	   VARCHAR2 (1000) := get_rule_value ('SMT_NOTIF_USERS', 'SMT_NOTIF_USERS');
		v_body		   VARCHAR2 (4000);
		v_subject	   VARCHAR2 (4000);
		v_to_mail	   VARCHAR2 (4000);
		v_crlf		   VARCHAR2 (2) := CHR (13) || CHR (10);
		v_start_dt	   DATE;
--
	BEGIN
--
   --
		FOR set_6_month_val IN set_6_month_cursor
		LOOP
			--
			v_subject	:= ' 6 month visit reminder email for site : ' || set_6_month_val.aname;
			--
			v_to_mail	:= set_6_month_val.toemail || ',' || v_email_cc;
			v_body		:=
				   'Please note that Site:- '
				|| set_6_month_val.aname
				|| ' is due for a Site Monitoring Visit as of :'
				|| TO_CHAR (set_6_month_val.duedate, 'MM/DD/YYYY')
				|| '. '
				|| v_crlf
				|| v_crlf
				|| 'Please access the Site Monitoring Tracker to schedule a visit.';
			--
			gm_com_send_email_prc (v_to_mail, v_subject, v_body);
		--
		END LOOP;

		--
		FOR set_10_patient_val IN set_10_patient_cursor
		LOOP
			--
			v_subject	:= ' 10th patient visit reminder email for site : ' || set_10_patient_val.aname;
			--
			v_to_mail	:= set_10_patient_val.toemail || ',' || v_email_cc;
			v_body		:=
				   'Please note that Site:- '
				|| set_10_patient_val.aname
				|| ' is due for a 10th Patient visit.There are currently '
				|| set_10_patient_val.currentcount
				|| ' patients. '
				|| v_crlf
				|| v_crlf
				|| 'Please access the Site Monitoring Tracker to schedule a visit.';
			--
			gm_com_send_email_prc (v_to_mail, v_subject, v_body);
		--
		END LOOP;

		gm_pkg_cm_job.gm_cm_notify_load_info (v_start_dt
											, SYSDATE
											, 'S'
											, 'SUCCESS'
											, 'gm_pkg_cr_site_monitor.gm_cr_reminder_email'
											 );
		COMMIT;
	EXCEPTION
		WHEN OTHERS
		THEN
			ROLLBACK;
			--
			gm_pkg_cm_job.gm_cm_notify_load_info (v_start_dt
												, SYSDATE
												, 'E'
												, SQLERRM
												, 'gm_pkg_cr_site_monitor.gm_cr_reminder_email'
												 );
			COMMIT;
--
	END gm_cr_reminder_email;

	PROCEDURE gm_cr_fch_scheduled_site_mtr
	AS
	BEGIN
		UPDATE t617_site_monitor
		   SET c901_status = 60462
			 , c617_last_updated_date = SYSDATE
		 WHERE c617_site_mtr_id IN (
					   SELECT c617_site_mtr_id
						 FROM t617_site_monitor a
						WHERE a.c617_void_fl IS NULL AND a.c901_status = 60461
							  AND (a.c617_visit_dt - 1) = TRUNC (SYSDATE))
		   AND c617_void_fl IS NULL;
	END gm_cr_fch_scheduled_site_mtr;

	FUNCTION get_no_of_observations (
		p_mtr_id			   NUMBER
	  , p_observation_status   NUMBER
	)
		RETURN NUMBER
	IS
		v_no_of_observations NUMBER;
	BEGIN
		SELECT	 COUNT (1)
			INTO v_no_of_observations
			FROM t617_site_monitor t617, t619_site_mtr_observation t619
		   WHERE t617.c617_site_mtr_id = t619.c617_site_mtr_id
			 AND t619.c901_status = NVL (p_observation_status, t619.c901_status)
			 AND t617.c617_site_mtr_id = p_mtr_id
			 AND t619.c619_void_fl IS NULL
		GROUP BY t617.c617_site_mtr_id;

		RETURN v_no_of_observations;
	EXCEPTION
		WHEN OTHERS
		THEN
			RETURN 0;
	END get_no_of_observations;

	FUNCTION get_study_name (
		p_study_id	 IN   t617_site_monitor.c611_study_id%TYPE
	)
		RETURN VARCHAR
	IS
		v_study_nm	   t611_clinical_study.c611_study_nm%TYPE;
	BEGIN
		SELECT c611_study_nm
		  INTO v_study_nm
		  FROM t611_clinical_study
		 WHERE c611_study_id = p_study_id;

		RETURN v_study_nm;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN '';
	END get_study_name;

	PROCEDURE gm_cr_fch_site_monitor (
		p_site_mtr_id	  IN	   t617_site_monitor.c617_site_mtr_id%TYPE
	  , p_study_id		  IN	   t617_site_monitor.c611_study_id%TYPE
	  , p_study_site_id   IN	   t617_site_monitor.c614_study_site_id%TYPE
	  , p_purpose		  IN	   t617_site_monitor.c901_purpose%TYPE
	  , p_status		  IN	   t617_site_monitor.c901_status%TYPE
	  , p_fromdate		  IN	   VARCHAR
	  , p_todate		  IN	   VARCHAR
	  , p_sitemonitor	  OUT	   TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_sitemonitor
		 FOR
			 SELECT c617_site_mtr_id mtr_id, t617.c611_study_id study_id
				  , TO_CHAR (c617_visit_dt, 'MM/DD/YYYY') visit_dt, c101_monitor_owner owner_id
				  , get_study_name (t617.c611_study_id) study_name, get_user_name (c101_monitor_owner) owner
				  , get_account_name (t614.c704_account_id) acc_name, c101_surgeon_id
				  , get_party_name (c101_surgeon_id) party_name, c617_no_of_patient patient_count
				  , get_code_name (c901_purpose) purpose, get_code_name (c901_status) status
				  , gm_pkg_cr_site_monitor.get_no_of_observations (c617_site_mtr_id, NULL) obs
				  , gm_pkg_cr_site_monitor.get_no_of_observations (c617_site_mtr_id, 60471) pending_obs
				  , get_log_flag (c617_site_mtr_id, 1257) logflag
			   FROM t617_site_monitor t617, t614_study_site t614
			  WHERE t614.c614_study_site_id = t617.c614_study_site_id
				AND t617.c611_study_id = NVL (p_study_id, t617.c611_study_id)
				AND t617.c614_study_site_id = NVL (p_study_site_id, t617.c614_study_site_id)
				AND t617.c901_purpose = NVL (p_purpose, t617.c901_purpose)
				AND t617.c901_status = NVL (p_status, t617.c901_status)
				AND t617.c617_visit_dt >=
							  CASE
								  WHEN p_fromdate IS NOT NULL
									  THEN TO_DATE (p_fromdate, 'MM/DD/YYYY')
								  ELSE t617.c617_visit_dt
							  END
				AND t617.c617_visit_dt <=
								  CASE
									  WHEN p_todate IS NOT NULL
										  THEN TO_DATE (p_todate, 'MM/DD/YYYY')
									  ELSE t617.c617_visit_dt
								  END
				AND t617.c617_void_fl IS NULL;
	END gm_cr_fch_site_monitor;

	PROCEDURE gm_cr_fch_site_mtr_observation (
		p_site_mtr_obs_id	IN		 t619_site_mtr_observation.c619_site_mtr_obs_id%TYPE
	  , p_study_id			IN		 t617_site_monitor.c611_study_id%TYPE
	  , p_study_site_id 	IN		 t617_site_monitor.c614_study_site_id%TYPE
	  , p_purpose			IN		 t617_site_monitor.c901_purpose%TYPE
	  , p_status			IN		 t617_site_monitor.c901_status%TYPE
	  , p_fromdate			IN		 VARCHAR
	  , p_todate			IN		 VARCHAR
	  , p_cra_id			IN		 NUMBER
	  , p_site_mtr_id		IN		 t617_site_monitor.c617_site_mtr_id%TYPE
	  , p_patientid 		IN		 VARCHAR
	  , p_observation		OUT 	 TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_observation
		 FOR
			 SELECT t617.c617_site_mtr_id mtr_id, t617.c611_study_id study_id
				  , TO_CHAR (c617_visit_dt, 'MM/DD/YYYY') visit_dt, t617.c101_monitor_owner owner_id
				  , get_user_name (t617.c101_monitor_owner) owner, get_account_name (t614.c704_account_id) acc_name
				  , t617.c101_surgeon_id, get_party_name (t617.c101_surgeon_id) party_name
				  , get_code_name (t617.c901_purpose) purpose, get_code_name (t619.c901_status) status
				  , t619.c901_status status_id, t619.c901_action action, t619.c901_responsiable_by res_by
				  , t619.c619_resolution_desc res_desc, t619.c619_site_mtr_obs_id obs_id
				  , t619.c621_patient_id patient_id, t619.c619_issue_desc issue_desc
				  , TO_CHAR (t619.c619_tgt_res_dt, 'MM/DD/YYYY') tgt_res_dt
				  , TO_CHAR (t619.c619_actual_res_dt, 'MM/DD/YYYY') actual_res_dt, t619.c901_study_period study_period
				  , t621.c621_patient_ide_no ideno, get_code_name(t619.c901_study_period) period_desc,  get_code_name(t619.c901_responsiable_by) res_by
				  , get_code_name(t619.c901_action) action_desc
			   FROM t617_site_monitor t617
				  , t614_study_site t614
				  , t619_site_mtr_observation t619
				  , t621_patient_master t621
			  WHERE t614.c614_study_site_id = t617.c614_study_site_id
				AND t619.c617_site_mtr_id = t617.c617_site_mtr_id
				AND t619.c621_patient_id = t621.c621_patient_id
				AND t617.c617_site_mtr_id = NVL (p_site_mtr_id, t617.c617_site_mtr_id)
				AND t619.c619_site_mtr_obs_id = NVL (p_site_mtr_obs_id, t619.c619_site_mtr_obs_id)
				AND t617.c101_monitor_owner = NVL (p_cra_id, t617.c101_monitor_owner)
				AND t617.c611_study_id = NVL (p_study_id, t617.c611_study_id)
				AND t617.c614_study_site_id = NVL (p_study_site_id, t617.c614_study_site_id)
				AND t617.c901_purpose = NVL (p_purpose, t617.c901_purpose)
				AND t619.c901_status = NVL (p_status, t619.c901_status)
				AND t617.c617_visit_dt >=
							  CASE
								  WHEN p_fromdate IS NOT NULL
									  THEN TO_DATE (p_fromdate, 'MM/DD/YYYY')
								  ELSE t617.c617_visit_dt
							  END
				AND t617.c617_visit_dt <=
								  CASE
									  WHEN p_todate IS NOT NULL
										  THEN TO_DATE (p_todate, 'MM/DD/YYYY')
									  ELSE t617.c617_visit_dt
								  END
				AND t619.c619_void_fl IS NULL
				AND t621.c621_patient_id =
								  DECODE (p_patientid
										, '', t621.c621_patient_id
										, '0', t621.c621_patient_id
										, p_patientid
										 )
				AND t617.c617_void_fl IS NULL;
	END gm_cr_fch_site_mtr_observation;

	PROCEDURE gm_cr_sav_site_mtr_obs (
		p_site_mtr_id	   IN		t619_site_mtr_observation.c617_site_mtr_id%TYPE
	  , p_patient_id	   IN		t619_site_mtr_observation.c621_patient_id%TYPE
	  , p_study_period	   IN		t619_site_mtr_observation.c901_study_period%TYPE
	  , p_issue_desc	   IN		t619_site_mtr_observation.c619_issue_desc%TYPE
	  , p_action		   IN		t619_site_mtr_observation.c901_action%TYPE
	  , p_res_by		   IN		t619_site_mtr_observation.c901_responsiable_by%TYPE
	  , p_tgt_res_dt	   IN		VARCHAR
	  , p_res_desc		   IN		t619_site_mtr_observation.c619_resolution_desc%TYPE
	  , p_act_res_dt	   IN		VARCHAR
	  , p_status		   IN		t619_site_mtr_observation.c901_status%TYPE
	  , p_userid		   IN		t619_site_mtr_observation.c619_created_by%TYPE
	  , p_observation_id   IN OUT	t619_site_mtr_observation.c619_site_mtr_obs_id%TYPE
	)
	AS
	BEGIN
		IF (p_observation_id = 0)
		THEN
			SELECT s619_site_mtr_observation.NEXTVAL
			  INTO p_observation_id
			  FROM DUAL;

			INSERT INTO t619_site_mtr_observation
						(c619_site_mtr_obs_id, c617_site_mtr_id, c621_patient_id, c901_study_period, c619_issue_desc
					   , c901_action, c901_responsiable_by, c619_tgt_res_dt, c619_resolution_desc
					   , c619_actual_res_dt, c901_status, c619_created_by, c619_created_date
						)
				 VALUES (p_observation_id, p_site_mtr_id, p_patient_id, p_study_period, p_issue_desc
					   , p_action, p_res_by, TO_DATE (p_tgt_res_dt, 'MM/DD/YYYY'), p_res_desc
					   , TO_DATE (p_act_res_dt, 'MM/DD/YYYY'), p_status, p_userid, TRUNC (SYSDATE)
						);
		ELSE
			UPDATE t619_site_mtr_observation
			   SET c617_site_mtr_id = p_site_mtr_id
				 , c621_patient_id = p_patient_id
				 , c901_study_period = p_study_period
				 , c619_issue_desc = p_issue_desc
				 , c901_action = p_action
				 , c901_responsiable_by = p_res_by
				 , c619_tgt_res_dt = TO_DATE (p_tgt_res_dt, 'MM/DD/YYYY')
				 , c619_resolution_desc = p_res_desc
				 , c619_actual_res_dt = TO_DATE (p_act_res_dt, 'MM/DD/YYYY')
				 , c901_status = p_status
				 , c619_last_updated_by = p_userid
				 , c619_last_updated_date = TRUNC (SYSDATE)
			 WHERE c619_site_mtr_obs_id = p_observation_id AND c619_void_fl IS NULL;
		END IF;
	END gm_cr_sav_site_mtr_obs;

	PROCEDURE gm_cr_fch_next_mtr_observation (
		p_site_mtr_obs_id	IN		 t619_site_mtr_observation.c619_site_mtr_obs_id%TYPE
	  , p_study_id			IN		 t617_site_monitor.c611_study_id%TYPE
	  , p_study_site_id 	IN		 t617_site_monitor.c614_study_site_id%TYPE
	  , p_status			IN		 t617_site_monitor.c901_status%TYPE
	  , p_visitdate 		IN		 VARCHAR
	  , p_observation		OUT 	 TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_observation
		 FOR
			 SELECT *
			   FROM (SELECT   t619.c619_site_mtr_obs_id sit_mtr_obs_id, t619.c901_study_period study_period_id
							, t619.c901_status status_id, get_code_name (t619.c901_status) status_name
							, t619.c621_patient_id patient_id, t619.c619_issue_desc issue_desc
							, t619.c619_resolution_desc res_desc
							, TO_CHAR (t619.c619_tgt_res_dt, 'MM/DD/YYYY') tgt_res_dt
							, TO_CHAR (t619.c619_actual_res_dt, 'MM/DD/YYYY') actual_res_dt, t619.c901_action action
							, t619.c901_responsiable_by responsible_by
						 FROM t617_site_monitor t617
							, t614_study_site t614
							, t619_site_mtr_observation t619
							, t621_patient_master t621
						WHERE t614.c614_study_site_id = t617.c614_study_site_id
						  AND t619.c617_site_mtr_id = t617.c617_site_mtr_id
						  AND t619.c621_patient_id = t621.c621_patient_id
						  --  AND t619.c619_site_mtr_obs_id > p_site_mtr_obs_id
						  AND t617.c611_study_id = NVL (p_study_id, t617.c611_study_id)
						  AND t617.c614_study_site_id = NVL (p_study_site_id, t617.c614_study_site_id)
						  AND t619.c901_status = NVL (60486, t619.c901_status)	 -- OPEN STATUS
						  AND t617.c617_visit_dt =
								  CASE
									  WHEN p_visitdate IS NOT NULL
										  THEN TO_DATE (p_visitdate, 'MM/DD/YYYY')
									  ELSE t617.c617_visit_dt
								  END
						  AND t619.c619_void_fl IS NULL
					 ORDER BY t619.c619_site_mtr_obs_id ASC)
			  WHERE ROWNUM < 2;
	END gm_cr_fch_next_mtr_observation;

	PROCEDURE gm_cr_cancel_site_monitor (
		p_site_mtr_id	IN	 t617_site_monitor.c617_site_mtr_id%TYPE
	  , p_comments		IN	 t907_cancel_log.c907_comments%TYPE
	  , p_canceltype	IN	 t901_code_lookup.c902_code_nm_alt%TYPE
	  , p_userid		IN	 t102_user_login.c102_user_login_id%TYPE
	)
	AS
		v_subject	   VARCHAR2 (100) := 'Site Monitoring Visit ';
		v_type		   VARCHAR2 (20);
		v_void_fl	   t617_site_monitor.c617_void_fl%TYPE;
		v_id		   VARCHAR2 (20);
	BEGIN
		SELECT c617_void_fl
		  INTO v_void_fl
		  FROM t617_site_monitor
		 WHERE c617_site_mtr_id = p_site_mtr_id;

--
		IF (v_void_fl IS NOT NULL)
		THEN
			-- Error message is: Vist is Already Voided.
			raise_application_error (-20260, '');
		END IF;

		IF p_canceltype = 'VDSMT'
		THEN
			UPDATE t617_site_monitor
			   SET c617_void_fl = 'Y'
				 , c617_last_updated_date = SYSDATE
				 , c617_last_updated_by = p_userid
			 WHERE c617_site_mtr_id = p_site_mtr_id;

			v_subject	:= v_subject || ' Voided';
			v_type		:= '60465';
		ELSIF p_canceltype = 'CNSMT'
		THEN
			UPDATE t617_site_monitor
			   SET c901_status = 60464
				 , c617_last_updated_date = SYSDATE
				 , c617_last_updated_by = p_userid
			 WHERE c617_site_mtr_id = p_site_mtr_id;

			v_subject	:= v_subject || ' Cancelled';
			v_type		:= '60464';
		END IF;

		SELECT TO_CHAR (p_site_mtr_id)
		  INTO v_id
		  FROM DUAL;

		COMMIT;
		gm_cr_email_cra (v_subject, v_id, v_type);
	END gm_cr_cancel_site_monitor;

	PROCEDURE gm_cr_cancel_site_mtr_obs (
		p_site_mtr_obs_id	IN	 t619_site_mtr_observation.c619_site_mtr_obs_id%TYPE
	  , p_userid			IN	 t102_user_login.c102_user_login_id%TYPE
	)
	AS
		v_void_fl	   t619_site_mtr_observation.c619_void_fl%TYPE;
	BEGIN
		SELECT c619_void_fl
		  INTO v_void_fl
		  FROM t619_site_mtr_observation
		 WHERE c619_site_mtr_obs_id = p_site_mtr_obs_id;

--
		IF (v_void_fl IS NOT NULL)
		THEN
			-- Error message is Observation Already Voided.
			raise_application_error (-20261, '');
		END IF;

		UPDATE t619_site_mtr_observation
		   SET c619_void_fl = 'Y'
			 , c619_last_updated_date = SYSDATE
			 , c619_last_updated_by = p_userid
		 WHERE c619_site_mtr_obs_id = p_site_mtr_obs_id;
	END gm_cr_cancel_site_mtr_obs;

	PROCEDURE gm_cr_update_visit_status
	AS
	BEGIN
		UPDATE t617_site_monitor t617
		   SET c901_status = 60462
		 WHERE c901_status = 60461 AND c617_visit_dt = TRUNC (SYSDATE + 1) AND c617_void_fl IS NULL;
	END gm_cr_update_visit_status;
END gm_pkg_cr_site_monitor;
/
