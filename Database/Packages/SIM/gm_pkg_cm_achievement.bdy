/* Formatted on 2008/11/10 12:54 (Formatter Plus v4.8.0) */
--@"c:\database\packages\SIM\gm_pkg_cm_achievement.bdy"

CREATE OR REPLACE PACKAGE BODY gm_pkg_cm_achievement
IS
/******************************************************************
   * Description : Procedure to get the achievement info for a specific party
   ****************************************************************/
	PROCEDURE gm_fch_achievementinfo (
		p_partyid	   IN		t101_party.c101_party_id%TYPE
	  , p_outinfocur   OUT		TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_outinfocur
		 FOR
			 SELECT c1013_party_acheivement_id ID, c101_party_id partyid, c1013_title title, c1013_detail detail
				  , get_code_name (c901_month) || '  ' || c1013_year dt
			   FROM t1013_party_acheivement
			  WHERE c101_party_id = p_partyid AND c1013_void_fl IS NULL;
	END gm_fch_achievementinfo;

/******************************************************************
   * Description : Procedure to get the achievement info for a specific party for editting
   ****************************************************************/
	PROCEDURE gm_fch_editachievementinfo (
		p_achievementid   IN	   t1013_party_acheivement.c1013_party_acheivement_id%TYPE
	  , p_outinfocur	  OUT	   TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_outinfocur
		 FOR
			 SELECT c1013_party_acheivement_id ID, c101_party_id partyid, c1013_title title, c1013_detail description
				  , c1013_year YEAR, c901_month MONTH
			   FROM t1013_party_acheivement
			  WHERE c1013_party_acheivement_id = p_achievementid AND c1013_void_fl IS NULL;
	END gm_fch_editachievementinfo;

	 /******************************************************************
	* Description : Procedure to save the achievement info
	****************************************************************/
	PROCEDURE gm_sav_achievementinfo (
		p_partyid		  IN   t1013_party_acheivement.c101_party_id%TYPE
	  , p_title 		  IN   t1013_party_acheivement.c1013_title%TYPE
	  , p_detail		  IN   t1013_party_acheivement.c1013_detail%TYPE
	  , p_year			  IN   t1013_party_acheivement.c1013_year%TYPE
	  , p_month 		  IN   t1013_party_acheivement.c901_month%TYPE
	  , p_achievementid   IN   t1013_party_acheivement.c1013_party_acheivement_id%TYPE
	  , p_userid		  IN   t1013_party_acheivement.c1013_last_updated_by%TYPE
	)
	AS
		v_achievement_id t1013_party_acheivement.c1013_party_acheivement_id%TYPE;
	BEGIN
		--
		UPDATE t1013_party_acheivement
		   SET c101_party_id = p_partyid
			 , c1013_title = p_title
			 , c1013_detail = p_detail
			 , c1013_year = p_year
			 , c901_month = p_month
			 , c1013_last_updated_by = p_userid
			 , c1013_last_updated_date = SYSDATE
		 WHERE c1013_party_acheivement_id = p_achievementid AND c1013_void_fl IS NULL;

		IF (SQL%ROWCOUNT = 0)
		THEN
			SELECT s1013_party_acheivement.NEXTVAL
			  INTO v_achievement_id
			  FROM DUAL;

			INSERT INTO t1013_party_acheivement
						(c1013_party_acheivement_id, c101_party_id, c1013_title, c1013_detail, c1013_year, c901_month
					   , c1013_last_updated_by, c1013_last_updated_date
						)
				 VALUES (v_achievement_id, p_partyid, p_title, p_detail, p_year, p_month
					   , p_userid, SYSDATE
						);
		END IF;
	END gm_sav_achievementinfo;

	 /******************************************************************
	* Description : Procedure to cancel the achievement info
	****************************************************************/
	PROCEDURE gm_cancel_achievementinfo (
		p_achievementid   IN   t1013_party_acheivement.c1013_party_acheivement_id%TYPE
	  , p_userid		  IN   t1013_party_acheivement.c1013_last_updated_by%TYPE
	)
	AS
	BEGIN
		COMMIT;

		UPDATE t1013_party_acheivement
		   SET c1013_void_fl = 'Y'
			 , c1013_last_updated_by = p_userid
			 , c1013_last_updated_date = SYSDATE
		 WHERE c1013_party_acheivement_id = p_achievementid;
	END gm_cancel_achievementinfo;
END gm_pkg_cm_achievement;
/
