/* Formatted on 2007/04/14 15:07 (Formatter Plus v4.8.8) */
 /******************************************************************
   * Description : Package for contact setup/report related functionalities
   ****************************************************************/

CREATE OR REPLACE PACKAGE gm_pkg_cm_contact
IS
--
 /******************************************************************
   * Description : Procedure to get the address info for a specific party
   ****************************************************************/
    PROCEDURE gm_cm_fch_addressinfo (
        p_partyid        IN       t101_party.c101_party_id%TYPE
      , p_partyinfocur   OUT      TYPES.cursor_type
    );

    /********************************************************************************
      * Description : Procedure to get the specific address info for a specific party
      ********************************************************************************/
    PROCEDURE gm_cm_fch_editaddressinfo (
        p_partyaddressid   IN       t106_address.c106_address_id%TYPE
      , p_partyinfocur     OUT      TYPES.cursor_type
    );

    /*******************************************************
      * Description : Procedure to save party address info
      * Parameters  :   1. p_partyId
                        2. p_addressType
                        3. p_addLine1
                        4. p_addLine2
                        5. p_city
                        6. p_state
                        7. p_country
                        8. p_zipcode
                        9. p_preference
                        10. p_userId
                        11. p_inactivefl
      *
      *******************************************************/
    PROCEDURE gm_cm_sav_addressinfo (
        p_partyid       IN   t101_party.c101_party_id%TYPE
      , p_addresstype   IN   t106_address.c901_address_type%TYPE
      , p_addline1      IN   t106_address.c106_add1%TYPE
      , p_addline2      IN   t106_address.c106_add2%TYPE
      , p_city          IN   t106_address.c106_city%TYPE
      , p_state         IN   t106_address.c901_state%TYPE
      , p_country       IN   t106_address.c901_country%TYPE
      , p_zipcode       IN   t106_address.c106_zip_code%TYPE
      , p_preference    IN   t106_address.c106_seq_no%TYPE
      , p_userid        IN   t106_address.c106_created_by%TYPE
      , p_inactivefl    IN   t106_address.c106_inactive_fl%TYPE
    );

    /*******************************************************
      * Description : Procedure to update party address info
      * Parameters  :   1. p_addressId
                        2. p_partyId
                        3. p_addressType
                        4. p_addLine1
                        5. p_addLine2
                        6. p_city
                        7. p_state
                        8. p_country
                        9. p_zipcode
                        10. p_preference
                        11. p_userId
                        12. p_inactivefl
      *
      *******************************************************/
    PROCEDURE gm_cm_upd_addressinfo (
        p_addressid     IN   t106_address.c106_address_id%TYPE
      , p_partyid       IN   t101_party.c101_party_id%TYPE
      , p_addresstype   IN   t106_address.c901_address_type%TYPE
      , p_addline1      IN   t106_address.c106_add1%TYPE
      , p_addline2      IN   t106_address.c106_add2%TYPE
      , p_city          IN   t106_address.c106_city%TYPE
      , p_state         IN   t106_address.c901_state%TYPE
      , p_country       IN   t106_address.c901_country%TYPE
      , p_zipcode       IN   t106_address.c106_zip_code%TYPE
      , p_preference    IN   t106_address.c106_seq_no%TYPE
      , p_userid        IN   t106_address.c106_last_updated_by%TYPE
      , p_inactivefl    IN   t106_address.c106_inactive_fl%TYPE
    );

    /******************************************************************
      * Description : Procedure to get the contact info for a specific party
      ****************************************************************/
    PROCEDURE gm_cm_fch_contactinfo (
        p_partyid               IN       t101_party.c101_party_id%TYPE
      , p_partycontactinfocur   OUT      TYPES.cursor_type
    );

    /********************************************************************************
      * Description : Procedure to get the specific contact info for a specific party
      ********************************************************************************/
    PROCEDURE gm_cm_fch_editcontactinfo (
        p_partycontactid        IN       t107_contact.c107_contact_id%TYPE
      , p_partycontactinfocur   OUT      TYPES.cursor_type
    );

    /*******************************************************
      * Description : Procedure to save party contact info
      * Parameters  :   1. p_partyId
                        2. p_mode
                        3. p_contactType
                        4. p_contactValue
                        5. p_preference
                        6. p_inactivefl
      *
      *******************************************************/
    PROCEDURE gm_cm_sav_contactinfo (
        p_partyid        IN   t101_party.c101_party_id%TYPE
      , p_contactmode    IN   t107_contact.c901_mode%TYPE
      , p_contacttype    IN   t107_contact.c901_type%TYPE
      , p_contactvalue   IN   t107_contact.c107_contact_value%TYPE
      , p_preference     IN   t107_contact.c107_seq_no%TYPE
      , p_inactivefl     IN   t107_contact.c107_inactive_fl%TYPE
      , p_userid         IN   t107_contact.c107_created_by%TYPE
    );

    /*******************************************************
      * Description : Procedure to update contact address info
      * Parameters  :   1. p_contactId
                        2. p_partyId
                        3. p_mode
                        4. p_contactType
                        5. p_contactValue
                        6. p_preference
                        7. p_inactivefl
      *
      *******************************************************/
    PROCEDURE gm_cm_upd_contactinfo (
        p_contactid      IN   t107_contact.c107_contact_id%TYPE
      , p_partyid        IN   t101_party.c101_party_id%TYPE
      , p_contactmode    IN   t107_contact.c901_mode%TYPE
      , p_contacttype    IN   t107_contact.c901_type%TYPE
      , p_contactvalue   IN   t107_contact.c107_contact_value%TYPE
      , p_preference     IN   t107_contact.c107_seq_no%TYPE
      , p_inactivefl     IN   t107_contact.c107_inactive_fl%TYPE
      , p_userid         IN   t107_contact.c107_last_updated_by%TYPE
    );
--
END gm_pkg_cm_contact;
/