CREATE OR REPLACE PACKAGE BODY gm_pkg_sim_project
IS
--
 /******************************************************************
   * Description : Procedure to get the setup project details 
   ****************************************************************/
    PROCEDURE gm_sim_fch_projectinfo (
        p_projectid        IN       t203_project_status.c202_project_id%TYPE
      , p_projectinfocur   OUT      TYPES.cursor_type
    )
	AS
	 BEGIN
        OPEN p_projectinfocur FOR
            SELECT t203.c202_project_id projectid, t203.c901_status_id status,  TO_CHAR (t203.c203_exp_launch_date,'MM/DD/YYYY') launchdate
                 ,  TO_CHAR (t203.c203_exp_start_date,'MM/DD/YYYY') startdate
              FROM t203_project_status t203, t202_project t202
             WHERE t203.c202_project_id = p_projectid AND t203.c202_project_id = t202.c202_project_id; -- AND c101_void_fl IS NULL
    END gm_sim_fch_projectinfo;	


    /*******************************************************
      * Description : Procedure to save project info
      * Parameters  :   1. p_projectid 
                        2. p_status
                        3. p_launchdate
                        4. p_startdate
                        5. p_userid
      *
      *******************************************************/
    PROCEDURE gm_sim_sav_projectinfo (
        p_projectid      IN   t203_project_status.c202_project_id%TYPE
      , p_status		 IN   t203_project_status.c901_status_id%TYPE
      , p_launchdate     IN   VARCHAR2
      , p_startdate      IN   VARCHAR2
	  , p_userid        IN   t203_project_status.c203_created_by%TYPE
    )
	AS
	v_project_status_id t203_project_status.c203_project_status_id%TYPE;

	BEGIN
	    SELECT s203_project_status.NEXTVAL
          INTO v_project_status_id
          FROM DUAL;

		INSERT INTO t203_project_status
				(  c203_project_status_id
				  ,c202_project_id
				  ,c901_status_id
				  ,c203_exp_launch_date
				  ,c203_exp_start_date
				  ,c203_created_by
				  ,c203_created_date
				)
		VALUES (
				v_project_status_id
				, p_projectid
				, p_status
				, DECODE(p_launchdate,NULL,NULL,TO_DATE(p_launchdate,'MM/DD/YYYY'))
				, DECODE(p_startdate,NULL,NULL,TO_DATE(p_startdate,'MM/DD/YYYY'))
				, p_userid
				, SYSDATE
				);

	  END gm_sim_sav_projectinfo;	
    /*******************************************************
      * Description : Procedure to update project setup info
      * Parameters  :   1. p_projectid 
                        2. p_status
                        3. p_launchdate
                        4. p_startdate
                        5. p_userid
      *
      *******************************************************/
    PROCEDURE gm_sim_upd_projectinfo (
        p_projectid      IN   t203_project_status.c202_project_id%TYPE
      , p_status		 IN   t203_project_status.c901_status_id%TYPE
      , p_launchdate     IN   VARCHAR2
      , p_startdate      IN   VARCHAR2
	  , p_userid        IN   t203_project_status.c203_last_updated_by%TYPE
    )
	AS
    BEGIN
        UPDATE t203_project_status
           SET c901_status_id = p_status
             , c203_exp_launch_date = DECODE(p_launchdate,NULL,NULL,TO_DATE(p_launchdate,'MM/DD/YYYY'))
			 , c203_exp_start_date = DECODE(p_startdate,NULL,NULL,TO_DATE(p_startdate,'MM/DD/YYYY'))
			 , c203_last_updated_by = p_userid
			 , c203_last_updated_date = SYSDATE
         WHERE c202_project_id = p_projectid; --AND c101_void_fl IS NULL AND c101_delete_fl IS NULL;
    END gm_sim_upd_projectinfo;	

END gm_pkg_sim_project;
/