/* Formatted on 2009/05/04 10:55 (Formatter Plus v4.8.0) */
-- @ "C:\Database\Packages\SIM\gm_pkg_sim_surgeon.bdy"

CREATE OR REPLACE PACKAGE BODY gm_pkg_sim_surgeon
IS
--
  /******************************************************************
   * Description : Procedure to fetch the setup details for a specific surgeon
   ****************************************************************/
	PROCEDURE gm_fch_surgeon_report (
		p_firstname   IN	   t101_party.c101_first_nm%TYPE
	  , p_lastname	  IN	   t101_party.c101_last_nm%TYPE
	  , p_phone 	  IN	   t107_contact.c107_contact_value%TYPE
	  , p_regions	  IN	   VARCHAR2
	  , p_states	  IN	   VARCHAR2
	  , p_cursor	  OUT	   TYPES.cursor_type
	)
	AS
	BEGIN
		my_context.set_double_inlist_ctx (p_regions || p_states);

		OPEN p_cursor
		 FOR
			 SELECT t101.c101_party_id ID
				  , (t101.c101_last_nm || ' ' || t101.c101_first_nm || ' ' || t101.c101_middle_initial) NAME
				  , v700.rep_name, get_code_name (c703_ship_state) state, t101.c901_party_type partytype
				  , DECODE (t101.c101_active_fl, 'Y', 'Inactive', 'Active') status
				  , get_party_name (t108.c101_to_party_id) rep, ad_name adname, region_name region
			   FROM t101_party t101
				  , t108_party_to_party_link t108
				  , (SELECT DISTINCT v700.rep_id, v700.rep_name, v700.ad_name, v700.region_name, v700.region_id
								   , t703.c101_party_id, t703.c703_ship_state
								FROM v700_territory_mapping_detail v700, t703_sales_rep t703
							   WHERE t703.c703_sales_rep_id = v700.rep_id AND (t703.c901_ext_country_id is NULL
							   OR t703.c901_ext_country_id  in (select country_id from v901_country_codes)))v700
			  WHERE t101.c901_party_type = 7000
				AND UPPER (t101.c101_first_nm) LIKE
												UPPER (DECODE (p_firstname, '', t101.c101_first_nm, p_firstname || '%'))
				AND UPPER (t101.c101_last_nm) LIKE UPPER (DECODE (p_lastname, '', t101.c101_last_nm, p_lastname || '%'))
				AND t101.c101_party_id = t108.c101_from_party_id(+)
				AND t108.c108_primary_fl(+) = 'Y'
				AND v700.c101_party_id(+) = t108.c101_to_party_id
				AND (	t101.c101_party_id = DECODE (p_phone, '', t101.c101_party_id, -999)
					 OR t101.c101_party_id IN (
							SELECT c101_party_id
							  FROM t107_contact t107
							 WHERE t107.c107_contact_value LIKE p_phone
							   AND t107.c901_mode = 90450
							   AND t107.c107_void_fl IS NULL)
					)
				AND (	DECODE (p_states, '', -1, -2) = DECODE (p_states, '', -1, -999)
					 OR v700.c703_ship_state IN (SELECT tokenii
												   FROM v_double_in_list
												  WHERE token = 'STATE')
					)
				AND (	DECODE (p_regions, '', -1, -2) = DECODE (p_regions, '', -1, -999)
					 OR v700.region_id IN (SELECT tokenii
											 FROM v_double_in_list
											WHERE token = 'REGION')
					);
	END gm_fch_surgeon_report;
END gm_pkg_sim_surgeon;
/
