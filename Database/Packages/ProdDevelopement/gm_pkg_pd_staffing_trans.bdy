/* Formatted on 2010/05/14 16:10 (Formatter Plus v4.8.0) */
-- @"C:\Database\Packages\ProdDevelopement\gm_pkg_pd_staffing_trans.bdy"

CREATE OR REPLACE PACKAGE BODY gm_pkg_pd_staffing_trans
IS
	/*******************************************************
	* Description : Procedure to save staffing requirements
	* Author		: Ritesh Shah
	*******************************************************/
	PROCEDURE gm_sav_staffingcount (
		p_projectid 	t202_project.c202_project_id%TYPE
	  , p_inputstring	VARCHAR2
	  , p_userid		t2005_project_milestone.c2005_created_by%TYPE
	)
	AS
		v_string	   VARCHAR2 (4000) := p_inputstring;
		v_substring    VARCHAR2 (4000);
		v_staffcntid   VARCHAR2 (20);
		v_staffcnt	   VARCHAR2 (3);
	BEGIN
		WHILE INSTR (v_string, '|') <> 0
		LOOP
			v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
			v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
			v_staffcntid := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
			v_staffcnt	:= SUBSTR (v_substring, INSTR (v_substring, '^') + 1);

			UPDATE t2004_project_team_count
			   SET c2004_role_count = TO_NUMBER (v_staffcnt)
				 , c2004_last_updated_by = p_userid
				 , c2004_last_updated_date = SYSDATE
			 WHERE c2004_project_team_count_id = TO_NUMBER (v_staffcntid) AND c2004_void_fl IS NULL;
		END LOOP;
	END gm_sav_staffingcount;

	/*******************************************************
	* Description : Procedure to save staffing details for project team id
	* Author		: Ritesh Shah
	*******************************************************/
	PROCEDURE gm_sav_staffingdetails (
		p_projectteamid   IN OUT   VARCHAR
	  , p_memberid		  IN	   t101_party.c101_party_id%TYPE
	  , p_role			  IN	   t204_project_team.c901_role_id%TYPE
	  , p_status		  IN	   t204_project_team.c901_status_id%TYPE
	  , p_projectid 	  IN	   t204_project_team.c202_project_id%TYPE
	  , p_userid		  IN	   t101_party.c101_party_id%TYPE
	)
	AS
		v_seq		   NUMBER;
	BEGIN
		UPDATE t204_project_team
		   SET c202_project_id = p_projectid
			 , c101_party_id = DECODE (p_memberid, 0, NULL, p_memberid)
			 , c901_role_id = DECODE (p_role, 0, NULL, p_role)
			 , c901_status_id = DECODE (p_status, 0, NULL, p_status)
			 , c204_last_updated_by = p_userid
			 , c204_last_updated_date = SYSDATE
		 WHERE c204_project_team_id = TO_NUMBER (p_projectteamid) AND c204_void_fl IS NULL;

		IF (SQL%ROWCOUNT = 0)
		THEN
			SELECT s204_project_team.NEXTVAL
			  INTO v_seq
			  FROM DUAL;

			INSERT INTO t204_project_team
						(c204_project_team_id, c202_project_id, c101_party_id
					   , c901_role_id, c901_status_id, c204_created_by, c204_created_date
						)
				 VALUES (v_seq, p_projectid, DECODE (p_memberid, 0, NULL, p_memberid)
					   , DECODE (p_role, 0, NULL, p_role), DECODE (p_status, 0, NULL, p_status), p_userid, SYSDATE
						);

			p_projectteamid := TO_CHAR (v_seq);
		END IF;
	END gm_sav_staffingdetails;

	/*******************************************************
	* Description : Procedure to fetch all Invoices for Swiss
	* Author		: Mihir Patel
	*******************************************************/
	PROCEDURE gm_fch_invoices_swiss (
		p_all_invoices	 OUT   TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_all_invoices
		 FOR
			 SELECT t501.c503_invoice_id invoiceid, get_code_name (t501.c901_order_type) invtype
				  , t501.c901_order_type invtypeid
				  , (DECODE (t504.c504_inhouse_purpose, 50061, 0, t501.c501_total_cost) + t501.c501_ship_cost) invamt
				  , 'USD' currency,   --ict will alwys be USD
								   t501.c501_order_id orderid
				  , DECODE (t701.c901_distributor_type
						  , 70103, get_distributor_name (t504.c701_distributor_id)
						  , get_account_name (t501.c704_account_id)
						   ) acctype
				  , t503.c503_invoice_date
			   FROM t504_consignment t504
				  , t504c_consignment_ict_detail t504c
				  , t501_order t501
				  , t701_distributor t701
				  , t503_invoice t503
			  WHERE t504.c504_consignment_id = t504c.c504_consignment_id
				AND t501.c501_order_id = t504c.c501_order_id
				AND t701.c701_distributor_id = t504.c701_distributor_id
				AND t503.c503_invoice_id = t501.c503_invoice_id
				AND t504.c701_distributor_id = 375
				AND t504c.c504_consignment_id = 'GM-CN-50397'
				AND t503.c503_invoice_date >= TO_DATE ('12/24/2008', 'MM/DD/YYYY')
				AND t503.c503_invoice_date <= TO_DATE ('12/24/2008', 'MM/DD/YYYY')
				AND NVL(t501.C901_ORDER_TYPE,-9999) = 2533
				--AND t503.c503_invoice_id IN ('GM63229', 'GM63166')
				AND t501.c704_account_id = 5549;
	END gm_fch_invoices_swiss;

	/*******************************************************
	* Description : Procedure to fetch all Invoice and order details for Swiss
	* Author		: Mihir Patel
	*******************************************************/
	PROCEDURE gm_fch_invoice_details (
		p_invoice_id	   IN		t503_invoice.c503_invoice_id%TYPE
	  , p_order_id		   IN		t501_order.c501_order_id%TYPE
	  , p_invoice_det	   OUT		TYPES.cursor_type
	  , p_invoice_orddet   OUT		TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_invoice_det
		 FOR
			 SELECT b.c704_account_id acid, NVL (c.c503_invoice_id, 'TO-BE') invfl, c.c503_invoice_id invoiceid
				  , get_dist_rep_name (get_rep_id (b.c704_account_id)) dist, c.c503_customer_po custpo
				  , get_bill_add (b.c704_account_id) billaddress
				  , DECODE (c.c503_invoice_date
						  , '', TO_CHAR (SYSDATE, 'MM/DD/YYYY')
						  , TO_CHAR (c.c503_invoice_date, 'mm/dd/yyyy')
						   ) udate
				  , DECODE (c.c503_invoice_date
						  , '', TO_CHAR (SYSDATE + 30, 'MM/DD/YYYY')
						  , TO_CHAR (c.c503_invoice_date + 30, 'mm/dd/yyyy')
						   ) duedt
				  , b.c704_payment_terms terms, get_code_name (b.c704_payment_terms) paynm, c.c503_status_fl sfl
				  , c.c901_invoice_type invtype, get_code_name_alt (c.c901_invoice_type) invtitle
				  , DECODE (c.c901_invoice_type, 50202, get_parent_invoice_id ('p_invoice_id'), '') parent_inv_id
				  , get_log_flag (c.c503_invoice_id, 1201) call_flag
				  , DECODE(get_account_attrb_value (b.c704_account_id,91987),'Y','2587','') thirdpartyid
				  , get_rule_value (b.c704_account_id, 'THIRD-PARTY-DATE') thirdpartydt
			   FROM t704_account b, t503_invoice c
			  WHERE c.c503_invoice_id = p_invoice_id AND b.c704_account_id = c.c704_account_id
					AND c.c503_void_fl IS NULL AND (b.c901_ext_country_id is NULL OR b.c901_ext_country_id in
					(select country_id from v901_country_codes)) AND (c.c901_ext_country_id is NULL OR
					c.c901_ext_country_id in (select country_id from v901_country_codes)); 

		OPEN p_invoice_orddet
		 FOR
			 SELECT *
			   FROM (SELECT DISTINCT (a.c205_part_number_id) pnum, get_partnum_desc (a.c205_part_number_id) pdesc
								   , get_order_part_qty_bfr_invoice (a.c501_order_id
																   , a.c205_part_number_id
																   , a.c502_item_price
																   , a.c901_type
																   , DECODE (a.c502_construct_fl
																		   , NULL, 'N'
																		   , a.c502_construct_fl
																			)
																	) iqty
								   , a.c502_item_price price, a.c901_type typ, a.c502_construct_fl confl
								   , b.c501_ship_cost scost, TO_CHAR (b.c501_order_date, 'mm/dd/yyyy') odt
								FROM t502_item_order a, t501_order b
							   WHERE a.c501_order_id = p_order_id
								 AND a.c502_void_fl IS NULL
								 AND a.c502_delete_fl IS NULL
								 AND (b.c901_ext_country_id is NULL
								 OR  b.c901_ext_country_id  in (select country_id from v901_country_codes))
								 AND a.c501_order_id = b.c501_order_id
							ORDER BY a.c205_part_number_id)
			  WHERE iqty != 0;
	END gm_fch_invoice_details;

	/*******************************************************
	* Description : Procedure to fetch other Invoices for Swiss
	* Author		: Mihir Patel
	*******************************************************/
	PROCEDURE gm_fch_invoices_others (
		p_other_invoices   OUT	 TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_other_invoices
		 FOR
			 SELECT t504.c504_consignment_id consignid, TO_CHAR (t504.c504_ship_date, 'MM/DD/YYYY') shipdate
				  -- SQL for Foreign Distributor
					,get_user_name (t504.c504_last_updated_by) transby, t503a.c503_invoice_id invoiceid
				  , t503a.c503a_customer_po poid, get_code_name (t503a.c901_ref_type) invtype
				  , t503a.c901_ref_type invtypeid
				  , (DECODE (t504.c504_inhouse_purpose, 50061, 0, t503a.c503a_total_amt) + t504.c504_ship_cost) invamt
				  , get_rule_value (50218, get_distributor_inter_party_id (t504.c701_distributor_id)) currency
				  , t504.c504_consignment_id orderid, get_distributor_name (t504.c701_distributor_id) acctype
			   FROM t503a_invoice_txn t503a, t504_consignment t504, t503_invoice t503
			  WHERE t504.c504_consignment_id = t503a.c503a_ref_id
				AND t503.c503_invoice_id = t503a.c503_invoice_id
				AND t504.c701_distributor_id = 375
				AND t504.c504_consignment_id = 'GM-CN-50397'
				AND t503.c503_invoice_date >= TO_DATE ('12/24/2008', 'MM/DD/YYYY')
				AND t503.c503_invoice_date <= TO_DATE ('12/24/2008', 'MM/DD/YYYY');
	--AND t503.c503_invoice_id IN ('GM6901600', 'GM57670');
	END gm_fch_invoices_others;
END gm_pkg_pd_staffing_trans;
/
