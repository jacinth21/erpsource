/* Formatted on 2012/05/23 10:15 (Formatter Plus v4.8.0) */

--@"C:\Database\Packages\ProdDevelopement\Orders\gm_pkg_pd_proc_by_order_job.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_pd_proc_by_order_job
IS
	  /***********************************************************
	* Description : Procedure to fetch all no-proc Do and
	* do system rule check
	* Author		: Xun
	*************************************************************/
	PROCEDURE gm_chk_all_no_proc_orders
	AS
		v_count 	   NUMBER;
		v_start_dt	   DATE;
		v_orderid	   t501_order.c501_order_id%TYPE;

		CURSOR cur_proc_order
		IS
			SELECT NVL (t501.c501_parent_order_id, t501.c501_order_id) orderid, t501.c501_order_id gm_order_id
			  FROM t501a_order_attribute t501a, t501_order t501
			 WHERE t501.c501_order_id = t501a.c501_order_id(+)
			   AND t501a.c901_attribute_type(+) = 53060   --53060 procedure DO,
			   AND t501a.c501a_attribute_value IS NULL	 --not procedure DO
			   AND t501a.c501a_void_fl IS NULL
			   AND t501.c501_void_fl IS NULL
			   AND NVL (t501.c901_order_type, -9999) NOT IN (2518, 2524, 2535, 2533, 2534, 2519);
	--2518 Draft,2519 pending CS confirmation, 2524 Price Adjustment,	2533, 2534-ICS,ICT returns Order, 2535 Sales Discount
	BEGIN
		v_start_dt	:= SYSDATE;

		FOR var_rec IN cur_proc_order
		LOOP
			BEGIN
				---check to see if the order already is procedure DO.
				SELECT COUNT (1)
				  INTO v_count
				  FROM t501a_order_attribute t501a
				 WHERE t501a.c501_order_id = var_rec.gm_order_id
				   AND t501a.c901_attribute_type = 53060
				   AND t501a.c501a_attribute_value = 'Y'
				   AND t501a.c501a_void_fl IS NULL;

				v_orderid	:= var_rec.orderid;

				IF v_count = 0
				THEN
					gm_pkg_pd_proc_by_order_txn.gm_chk_proc_by_order (var_rec.orderid);
					--PC-5722 DO Classification - Clear classify flag for Parent Order 
					gm_pkg_do_classification_validation.gm_chk_clear_classification_flag(var_rec.orderid);
				END IF;

				COMMIT;
			EXCEPTION
				WHEN OTHERS
				THEN
					ROLLBACK;
					--
					gm_pkg_cm_job.gm_cm_notify_load_info (v_start_dt
														, SYSDATE
														, 'E'
														, SQLERRM || ' Err Order Id ' || v_orderid
														, 'gm_chk_all_no_proc_orders'
														 );
					COMMIT;
			END;
		END LOOP;
	EXCEPTION
		WHEN OTHERS
		THEN
			ROLLBACK;
			--
			gm_pkg_cm_job.gm_cm_notify_load_info (v_start_dt
												, SYSDATE
												, 'E'
												, SQLERRM || ' Order Id ' || v_orderid
												, 'gm_chk_all_no_proc_orders'
												 );
			COMMIT;
	END gm_chk_all_no_proc_orders;

	/*****************************************************
	* Description: Procedure to fetch all orders which
	*				c9729_is_rule_applied_fl is NULL in the
	*				t9729_system_rule_cleanup_log
	* do system
	* Author:	   Rick Schmid
	******************************************************/
	PROCEDURE gm_chk_all_rule_cleanup_orders
	AS
		v_order_attr_id NUMBER;
		v_start_dt	   DATE;
		v_orderid	   t501_order.c501_order_id%TYPE;
		v_sysid 	   t9729_system_rule_cleanup_log.c207_system_id%TYPE;
		v_partnum	   t9729_system_rule_cleanup_log.c205_part_number_id%TYPE;
		v_count_sys    NUMBER := 0;
		v_count_part   NUMBER := 0;

		-- Fetch all records from t9729_system_rule_cleanup_log where c9729_is_rule_applied_fl is NULL
		CURSOR curr_master
		IS
			SELECT c207_system_id sys_id, c205_part_number_id part_num, c9729_system_rule_cleanup_id rule_cleanup_id
			  FROM t9729_system_rule_cleanup_log
			 WHERE c9729_is_rule_applied_fl IS NULL AND c9729_void_fl IS NULL;

		-- Fetch all orders which c9729_is_rule_applied_fl is NULL in the t9729_system_rule_cleanup_log
		CURSOR curr_part_order (
			v_partid   VARCHAR2
		)
		IS
			SELECT DISTINCT t502.c501_order_id order_id
					   FROM t502_item_order t502, t9729_system_rule_cleanup_log t9729
					  WHERE t502.c205_part_number_id = t9729.c205_part_number_id
						AND t9729.c9729_is_rule_applied_fl IS NULL
						AND t9729.c9729_void_fl IS NULL
						AND t502.c502_void_fl IS NULL
						AND t9729.c205_part_number_id = v_partid;

		-- Fetch all orders from system
		CURSOR curr_sys_order (
			v_systemid	 VARCHAR2
		)
		IS
			SELECT DISTINCT t502.c501_order_id order_id
					   FROM t502_item_order t502
						  , t4011_group_detail t4011
						  , t9720_system_rule t9720
						  , t9722_system_rule_grp t9722
						  , t9729_system_rule_cleanup_log t9729
					  WHERE t9720.c207_system_id = t9729.c207_system_id
						AND t9720.c207_system_id = t9722.c207_system_id
						AND t9722.c4010_group_id = t4011.c4010_group_id
						AND t4011.c205_part_number_id = t502.c205_part_number_id
						AND t9729.c9729_is_rule_applied_fl IS NULL
						AND t9729.c9729_void_fl IS NULL
						AND t502.c502_void_fl IS NULL
						AND t9729.c207_system_id = v_systemid;
	BEGIN
		v_start_dt	:= SYSDATE;

		FOR var_rec IN curr_master
		LOOP
			v_sysid 	:= var_rec.sys_id;
			v_partnum	:= var_rec.part_num;

			IF (var_rec.sys_id IS NOT NULL)
			THEN
				FOR var_sys_rec IN curr_sys_order (var_rec.sys_id)
				LOOP
					BEGIN
						v_orderid	:= var_sys_rec.order_id;
						v_order_attr_id := gm_pkg_pd_proc_by_order_txn.get_order_attribute_id (var_sys_rec.order_id);
						gm_pkg_cm_order_attribute.gm_sav_order_attribute (var_sys_rec.order_id
																		, 53060
																		, NULL
																		, v_order_attr_id
																		, 30301
																		 );
                        
						-- PC-5285 changes																		
                        --Clear the Order for DO Tag Classification job. - new					
                        gm_pkg_do_classification_validation.gm_chk_clear_classification_flag(var_sys_rec.order_id);
                                                                         
						v_count_sys := v_count_sys + 1;

						IF v_count_sys = 100
						THEN
							COMMIT;
							v_count_sys := 0;
						END IF;
					EXCEPTION
						WHEN OTHERS
						THEN
							ROLLBACK;
							--
							gm_pkg_cm_job.gm_cm_notify_load_info (v_start_dt
																, SYSDATE
																, 'E'
																, SQLERRM || ' Err system Order Id ' || v_orderid
																, 'gm_chk_all_rule_cleanup_orders'
																 );
							COMMIT;
					END;
				END LOOP;
			END IF;

			IF (var_rec.part_num IS NOT NULL)
			THEN
				FOR var_part_rec IN curr_part_order (var_rec.part_num)
				LOOP
					BEGIN
						v_orderid	:= var_part_rec.order_id;
						v_order_attr_id := gm_pkg_pd_proc_by_order_txn.get_order_attribute_id (var_part_rec.order_id);
						gm_pkg_cm_order_attribute.gm_sav_order_attribute (var_part_rec.order_id
																		, 53060
																		, NULL
																		, v_order_attr_id
																		, 30301
																		 );
						v_count_part := v_count_part + 1;

						IF v_count_part = 100
						THEN
							COMMIT;
							v_count_part := 0;
						END IF;
					EXCEPTION
						WHEN OTHERS
						THEN
							ROLLBACK;
							--
							gm_pkg_cm_job.gm_cm_notify_load_info (v_start_dt
																, SYSDATE
																, 'E'
																, SQLERRM || ' Err part Order Id ' || v_orderid
																, 'gm_chk_all_rule_cleanup_orders'
																 );
							COMMIT;
					END;
				END LOOP;
			END IF;

			-- Set rule applied flag
			UPDATE t9729_system_rule_cleanup_log
			   SET c9729_is_rule_applied_fl = 'Y'
				 , c9729_last_updated_by = 30301
				 , c9729_last_updated_date = SYSDATE
			 WHERE c9729_system_rule_cleanup_id = var_rec.rule_cleanup_id
			   AND c9729_is_rule_applied_fl IS NULL
			   AND c9729_void_fl IS NULL;
		END LOOP;

		COMMIT;
	EXCEPTION
		WHEN OTHERS
		THEN
			ROLLBACK;
			--
			gm_pkg_cm_job.gm_cm_notify_load_info (v_start_dt
												, SYSDATE
												, 'E'
												, SQLERRM || ' system Id ' || v_sysid || ' or part number ' || v_partnum
												, 'gm_chk_all_rule_cleanup_orders'
												 );
			COMMIT;
	END gm_chk_all_rule_cleanup_orders;

	  /***********************************************************
	* Description : Procedure to fetch all draft systems and
	* send email to kelly
	* Author		: Xun
	*************************************************************/
	PROCEDURE gm_chk_all_draft_systems
	AS
		subject 	   VARCHAR2 (200);
		to_mail 	   VARCHAR2 (200);
		mail_body	   VARCHAR2 (4000);
		mail_content   VARCHAR2 (400);
		crlf		   VARCHAR2 (2) := CHR (13) || CHR (10);

		-- Draft:	  11500
		-- Published: 11501
		CURSOR cur_draft_sys
		IS
			SELECT	 get_set_name (t9720.c207_system_id) sysname, get_user_name (t9720.c9720_created_by) crtby
				   , t9720.c9720_created_date crtdate, get_user_name (t9720.c9720_last_updated_by) uptby
				   , t9720.c9720_last_updated_date uptdate
				FROM t9720_system_rule t9720
			   WHERE t9720.c901_status = 11500 AND t9720.c9720_void_fl IS NULL
			ORDER BY t9720.c9720_last_updated_date, t9720.c9720_created_date;
	BEGIN
		to_mail 	:= get_rule_value ('DO_ENGINE', 'EMAILDORULE');
		subject 	:= 'Draft system list (DO classification)';

		FOR var_rec IN cur_draft_sys
		LOOP
			IF var_rec.uptby = ' '
			THEN
				mail_content := ' created by ' || var_rec.crtby || ' on ' || var_rec.crtdate;
			ELSE
				mail_content := ' last updated by ' || var_rec.uptby || ' on ' || var_rec.uptdate;
			END IF;

			mail_body	:=
				   mail_body || ' System - ' || var_rec.sysname || ': status is draft, ' || mail_content || crlf || crlf;
		END LOOP;

		gm_com_send_email_prc (to_mail, subject, mail_body);
	END gm_chk_all_draft_systems;

	/***************************************************************
	* Description : Procedure to check any group is created
	* Author		: Xun
	***************************************************************/
	PROCEDURE gm_chk_all_new_groups
	AS
		subject 	   VARCHAR2 (200);
		to_mail 	   VARCHAR2 (200);
		mail_body	   VARCHAR2 (4000);
		crlf		   VARCHAR2 (2) := CHR (13) || CHR (10);

		CURSOR cur_new_group
		IS
			SELECT t4010.c4010_group_nm groupnm, t4010.c4010_created_date created_date
				 , get_user_name (t4010.c4010_created_by) owner
			  FROM t4010_group t4010
			 WHERE t4010.c4010_created_date >= TRUNC (SYSDATE) AND t4010.c4010_void_fl IS NULL;
	BEGIN
		to_mail 	:= get_rule_value ('DO_ENGINE', 'EMAILDORULE');
		subject 	:= 'New group is created';

		FOR var_rec IN cur_new_group
		LOOP
			mail_body	:=
				   mail_body
				|| ' New Group - '
				|| var_rec.groupnm
				|| ' is created on '
				|| var_rec.created_date
				|| ' by  '
				|| var_rec.owner
				|| crlf
				|| crlf;
		END LOOP;

		gm_com_send_email_prc (to_mail, subject, mail_body);
	END gm_chk_all_new_groups;
END gm_pkg_pd_proc_by_order_job;
/
