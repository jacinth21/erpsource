/* Formatted on 2012/04/27 09:52 (Formatter Plus v4.8.0) */

--@"C:\Database\Packages\ProdDevelopement\Orders\gm_pkg_pd_proc_by_order_txn.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_pd_proc_by_order_txn
IS
	/***********************************************************
	* Description : Procedure to pick one DO and check system rule
	* Author		: Xun
	*************************************************************/
	PROCEDURE gm_chk_proc_by_order (
		p_orderid	IN	 t501_order.c501_order_id%TYPE
	)
	AS
		v_rule_flag    VARCHAR2 (1);
		v_systems	   TYPES.cursor_type;

		--fetch all the systems by DO
		CURSOR cur_sys
		IS
			SELECT	 t9720.c207_system_id sysid
				   , DECODE (t502.c901_type, 50300, 50300, NULL, 50300, 50301) cong_ln_type   --50300 Sales Consignment, 50301	  Loaner
				FROM t501_order t501
				   , t502_item_order t502
				   , t9720_system_rule t9720
				   , t9722_system_rule_grp t9722
				   , t4011_group_detail t4011
			   WHERE t501.c501_order_id = t502.c501_order_id
				 AND (t502.c501_order_id = p_orderid OR t501.c501_parent_order_id = p_orderid)
				 AND t502.c205_part_number_id = t4011.c205_part_number_id
				 AND t4011.c4010_group_id = t9722.c4010_group_id
				 AND t9720.c207_system_id = t9722.c207_system_id
				 AND t501.c501_void_fl IS NULL
				 AND t502.c502_void_fl IS NULL
				 AND t502.c502_delete_fl IS NULL
				 AND t9720.c9720_void_fl IS NULL
				 AND t9722.c9722_void_fl IS NULL
				 AND t9720.c901_status = 11501	 --11501 published
			GROUP BY t9720.c207_system_id, t502.c901_type
			ORDER BY t9720.c207_system_id, t502.c901_type;
	BEGIN
		gm_upt_order_by_system (p_orderid);   --flag all the existing systems for the DO

		FOR v_rec IN cur_sys
		LOOP
			--check the system rule of the order
			gm_pkg_pd_proc_system_engine.gm_check_system_rule (p_orderid, v_rec.sysid, v_rule_flag);

			IF v_rule_flag = 'Y'
			THEN
				gm_sav_order_by_system (p_orderid, v_rec.sysid, v_rec.cong_ln_type);
			END IF;
		END LOOP;

		--void all unused system of DO and reset update flag to null
		gm_void_unused_system (p_orderid);
		--if all systems of the order satisfied the rule, mark the Do as procedure in T501a
		gm_sav_proc_order_attribute (p_orderid);
	END gm_chk_proc_by_order;

	 /***********************************************************
	* Description : Procedure to update order by system  table
	* Author		: Xun
	*************************************************************/
	PROCEDURE gm_upt_order_by_system (
		p_orderid	IN	 t501_order.c501_order_id%TYPE
	)
	AS
	BEGIN
		UPDATE t501b_order_by_system
		   SET c501b_it_update_fl = 'X'
		 WHERE c501_order_id = p_orderid;
	END gm_upt_order_by_system;

	/***********************************************************
	* Description : Procedure to update order by system  table
	* Author		: Xun
	*************************************************************/
	PROCEDURE gm_void_unused_system (
		p_orderid	IN	 t501_order.c501_order_id%TYPE
	)
	AS
	BEGIN
		UPDATE t501b_order_by_system
		   SET c501b_it_update_fl = NULL
			 , c501b_void_fl = 'Y'
			 , c501b_last_updated_by = 30301
			 , c501b_last_updated_date = SYSDATE
		 WHERE c501_order_id = p_orderid AND c501b_it_update_fl = 'X';
	END gm_void_unused_system;

	 /***********************************************************
	* Description : Procedure to update order by system  table
	* Author		: Xun
	*************************************************************/
	PROCEDURE gm_sav_order_by_system (
		p_orderid	IN	 t501_order.c501_order_id%TYPE
	  , p_sysid 	IN	 t9720_system_rule.c207_system_id%TYPE
	  , p_type		IN	 NUMBER
	)
	AS
	BEGIN
		UPDATE t501b_order_by_system
		   SET c501b_it_update_fl = NULL
			 , c501b_void_fl = NULL
			 , c901_type = p_type
			 , c501b_last_updated_by = 30301
			 , c501b_last_updated_date = SYSDATE
		 WHERE c501_order_id = p_orderid AND c207_system_id = p_sysid;

		IF (SQL%ROWCOUNT = 0)
		THEN
			INSERT INTO t501b_order_by_system
						(c501b_order_by_system_id, c207_system_id, c501b_qty, c501_order_id, c901_type
					   , c501b_last_updated_by, c501b_last_updated_date
						)
				 VALUES (s501b_order_by_system.NEXTVAL, p_sysid, 1, p_orderid, p_type
					   , 30301, SYSDATE
						);
		END IF;
	END gm_sav_order_by_system;

	 /***********************************************************
	* Description : Procedure to update order by system  table
	* Author		: Xun
	*************************************************************/
	PROCEDURE gm_sav_proc_order_attribute (
		p_orderid	IN	 t501_order.c501_order_id%TYPE
	)
	AS
		v_rule_flag    t501_order.c501_order_id%TYPE;
		v_order_attr_id t501a_order_attribute.c501a_order_attribute_id%TYPE;

		CURSOR cur_proc_order
		IS
			SELECT t501.c501_order_id orderid
			  FROM t501_order t501
			 WHERE (t501.c501_order_id = p_orderid OR t501.c501_parent_order_id = p_orderid)
			   AND t501.c501_void_fl IS NULL;
	BEGIN
		FOR v_rec IN cur_proc_order
		LOOP
			v_order_attr_id := get_order_attribute_id (v_rec.orderid);
			gm_pkg_cm_order_attribute.gm_sav_order_attribute (v_rec.orderid, 53060, 'Y', v_order_attr_id, 30301);
		END LOOP;
	END gm_sav_proc_order_attribute;

	/****************************************************************
	  * Description : This function used to get sum qty by DO
	  * attribute id by order id
	  * Author		: Xun
	  *************************************************************/
	FUNCTION get_order_attribute_id (
		p_orderid	IN	 t501_order.c501_order_id%TYPE
	)
		RETURN NUMBER
	IS
		v_order_attr_id NUMBER;
	BEGIN
		SELECT c501a_order_attribute_id
		  INTO v_order_attr_id
		  FROM t501a_order_attribute
		 WHERE c901_attribute_type = 53060 AND c501_order_id = p_orderid AND c501a_void_fl IS NULL;

		RETURN v_order_attr_id;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN 0;
	END get_order_attribute_id;
END gm_pkg_pd_proc_by_order_txn;
/
