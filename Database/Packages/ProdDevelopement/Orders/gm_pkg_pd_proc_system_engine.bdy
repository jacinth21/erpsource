/* Formatted on 2012/05/02 16:24 (Formatter Plus v4.8.0) */

--@"C:\Database\Packages\ProdDevelopement\Orders\gm_pkg_pd_proc_system_engine.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_pd_proc_system_engine
IS
	  /***********************************************************
	* Description : Procedure to do system rule check
	* Author		: Xun
	*************************************************************/
	PROCEDURE gm_check_system_rule (
		p_orderid	  IN	   t501_order.c501_order_id%TYPE
	  , p_sysid 	  IN	   t9720_system_rule.c207_system_id%TYPE
	  , p_rule_flag   OUT	   VARCHAR2
	)
	AS
		v_group_type   NUMBER;
		v_qty		   NUMBER;
		v_item_qtys    NUMBER;

		CURSOR cur_grp
		IS
			SELECT c9721_qty rule_qty, c9721_system_rule_type_id rule_type_id
			  FROM t9721_system_rule_type
			 WHERE c207_system_id = p_sysid AND c9721_link_system_rule_type_id IS NULL AND c9721_void_fl IS NULL;
	BEGIN
		FOR var_grp IN cur_grp
		LOOP
			v_item_qtys := get_qty_by_order (p_orderid, var_grp.rule_type_id);

			IF (NVL (v_item_qtys, 0) < var_grp.rule_qty)
			THEN
				p_rule_flag := 'N';
				RETURN;
			END IF;
		END LOOP;

		p_rule_flag := 'Y';
	END gm_check_system_rule;

	/***********************************************************
	  * Description : This function used to get sum qty by DO
	  * Author		: Xun
	  *************************************************************/
	FUNCTION get_qty_by_order (
		p_orderid	 IN   t501_order.c501_order_id%TYPE
	  , p_grp_type	 IN   t9721_system_rule_type.c9721_link_system_rule_type_id%TYPE
	)
		RETURN NUMBER
	IS
		v_qty		   NUMBER;
	BEGIN
		SELECT SUM (t502.c502_item_qty)
		  INTO v_qty
		  FROM t501_order t501, t502_item_order t502, t4010_group t4010, t4011_group_detail t4011
		 WHERE (t501.c501_order_id = p_orderid OR t501.c501_parent_order_id = p_orderid)
		   AND t501.c501_order_id = t502.c501_order_id	 --include all child DO
		   AND t4010.c4010_void_fl IS NULL
		   AND t501.c501_void_fl IS NULL
		   AND t502.c502_void_fl IS NULL
		   AND t4010.c4010_group_id = t4011.c4010_group_id
		   AND t4011.c205_part_number_id = t502.c205_part_number_id
		   AND t4010.c4010_group_id IN (
				   SELECT t9722.c4010_group_id
					 FROM t9721_system_rule_type t9721, t9722_system_rule_grp t9722
					WHERE t9721.c9721_system_rule_type_id = t9722.c9721_system_rule_type_id
					  AND (   t9721.c9721_link_system_rule_type_id = p_grp_type
						   OR t9721.c9721_system_rule_type_id = p_grp_type
						  )
					  AND t9721.c9721_void_fl IS NULL
					  AND t9722.c9722_void_fl IS NULL);   -- count all linked group types

		RETURN v_qty;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN 0;
	END get_qty_by_order;
END gm_pkg_pd_proc_system_engine;
/
