/* Formatted on 2012/03/11 18:14 (Formatter Plus v4.8.0) */
--@"C:\Database\Packages\ProdDevelopement\Orders\gm_pkg_pd_proc_order_master.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_pd_proc_order_master
IS
   PROCEDURE gm_sav_order_by_sys_log (
      p_OBS_id       IN   T501B_ORDER_BY_SYSTEM.C501B_ORDER_BY_SYSTEM_ID%TYPE,
      p_order_id     IN   T501B_ORDER_BY_SYSTEM.C501_ORDER_ID%TYPE,
      p_system_id    IN   T501B_ORDER_BY_SYSTEM.C207_SYSTEM_ID%TYPE,
      p_qty          IN   T501B_ORDER_BY_SYSTEM.C501B_QTY%TYPE,
      p_void_fl      IN   T501B_ORDER_BY_SYSTEM.C501B_VOID_FL%TYPE,
      p_user_id      IN   T501B_ORDER_BY_SYSTEM.C501B_LAST_UPDATED_BY%TYPE,
      p_last_updt_dt IN   T501B_ORDER_BY_SYSTEM.C501B_LAST_UPDATED_DATE%TYPE,
      p_out          OUT  T501B_ORDER_BY_SYSTEM_LOG.C501B_ORDER_BY_SYSTEM_LOG_ID%TYPE
   )
   AS
      v_system_log_id    t501B_ORDER_BY_SYSTEM_LOG.C501B_ORDER_BY_SYSTEM_LOG_ID%TYPE;
   BEGIN

        SELECT S501B_ORDER_BY_SYSTEM_LOG.nextval
	  INTO v_system_log_id
	  FROM DUAL;
	
	INSERT INTO T501B_ORDER_BY_SYSTEM_LOG(
		C501B_ORDER_BY_SYSTEM_LOG_ID,
		C501B_ORDER_CLASSIFICATION_ID,
		C501_ORDER_ID,
		C207_SYSTEM_ID,
		C501B_QTY,
		C501B_VOID_FL,
		C501B_LAST_UPDATED_BY,
		C501B_LAST_UPDATED_DATE
	) VALUES (
		v_system_log_id,
		p_OBS_id,
		p_order_id,
		p_system_id,
		p_qty,
		p_void_fl,
		p_user_id,
		p_last_updt_dt
	);

	p_out := v_system_log_id;

   END gm_sav_order_by_sys_log;

   PROCEDURE gm_sav_sys_rule_cleanup_log (
      p_type         IN    t9720_SYSTEM_RULE.C901_STATUS%TYPE,
      p_system_id    IN    t9720_SYSTEM_RULE.C207_SYSTEM_ID%TYPE,
      p_part_num     IN    t502_ITEM_ORDER.C205_PART_NUMBER_ID%TYPE,
      p_user_id      IN    t9720_SYSTEM_RULE.C9720_CREATED_BY%TYPE,
      p_create_dt    IN    t9720_SYSTEM_RULE.C9720_CREATED_DATE%TYPE,
      p_last_upd_id  IN    t9720_SYSTEM_RULE.C9720_LAST_UPDATED_BY%TYPE,
      p_last_updt_dt IN    t9720_SYSTEM_RULE.C9720_LAST_UPDATED_DATE%TYPE,
      p_out          OUT   t9729_SYSTEM_RULE_CLEANUP_LOG.C9729_SYSTEM_RULE_CLEANUP_ID%TYPE
   )
   AS
       v_system_log_id    t9729_SYSTEM_RULE_CLEANUP_LOG.C9729_SYSTEM_RULE_CLEANUP_ID%TYPE;
   BEGIN

        SELECT S9729_SYSTEM_RULE_CLEANUP_LOG.nextval
	  INTO v_system_log_id
	  FROM DUAL;

   	INSERT INTO T9729_SYSTEM_RULE_CLEANUP_LOG (
		C9729_SYSTEM_RULE_CLEANUP_ID,
		C901_TYPE,
		C207_SYSTEM_ID,
		C205_PART_NUMBER_ID,
		C9729_IS_RULE_APPLIED_FL,
		C9729_VOID_FL,
		C9729_CREATED_BY,
		C9729_CREATED_DATE,
		C9729_LAST_UPDATED_BY,
		C9729_LAST_UPDATED_DATE
	) VALUES (
		v_system_log_id,
		p_type,
		p_system_id,
		p_part_num,
		null,
		null,
                p_user_id,
                p_create_dt,
                p_last_upd_id,
                p_last_updt_dt
	);

	p_out := v_system_log_id;

   END gm_sav_sys_rule_cleanup_log;

END gm_pkg_pd_proc_order_master;
/