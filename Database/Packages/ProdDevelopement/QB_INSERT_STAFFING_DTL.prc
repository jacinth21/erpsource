 CREATE OR REPLACE PROCEDURE qb_insert_staffing_dtl
 as
 cursor c_projects is
 select c202_project_id projectid, c202_project_nm 
 from t202_project 
 where c202_project_id not in (select c202_project_id from t2005_project_milestone) and not c202_project_nm like 'test%' 
 order by c202_project_id;
 begin
   for rec in c_projects
   loop
     INSERT INTO t2005_project_milestone
       (c2005_project_milestone_id, c202_project_id, c901_milestone, c901_group, c2005_created_by
         , c2005_created_date)
     SELECT s2005_project_milestone.NEXTVAL, rec.projectid, c901_code_id, TO_NUMBER (c902_code_nm_alt), '303043'
       , SYSDATE
       FROM t901_code_lookup
      WHERE c901_code_grp = 'PROMS';
   INSERT INTO t2004_project_team_count
       (c2004_project_team_count_id, c202_project_id, c901_role, c2004_created_by, c2004_created_date)
     SELECT s2004_project_team_count.NEXTVAL, rec.projectid, c901_code_id, '303043', SYSDATE
       FROM t901_code_lookup
      WHERE c901_code_grp IN ('GROLE', 'SROLE');   
   end loop;
   
 end;
 /