/* Formatted on 2011/11/04 11:52 (Formatter Plus v4.8.0) */

--@"C:\Database\Packages\ProdDevelopement\OrdClassification\gm_pkg_pd_classify_ord_rpt.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_pd_classify_ord_rpt
IS
--
	/*******************************************************
	* Description : Procedure to fetch System Rule Info
	* Author	  : Gopinathan
	*******************************************************/
	PROCEDURE gm_fch_system_rule(
		p_system_id		  IN	   t9720_system_rule.c207_system_id%TYPE
	  , p_out_system	  OUT		TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_out_system
			FOR
				 SELECT c207_system_id SYSTEMID, get_set_name (c207_system_id) SYSTEMNM, c901_status STATUS, c9720_history_fl HISTORYFL
				      , get_code_name(c901_status) STATUSNM, c9720_published_by PUBLISHEDBY, get_user_name(c9720_published_by) PUBLISHEDNM
				      , c9720_published_dt PUBLISHEDDATE, get_user_name(c9720_created_by) INITIATEDNM, c9720_created_date INITIATEDDATE
				   FROM t9720_system_rule
				  WHERE c207_system_id = p_system_id
				    AND c9720_void_fl IS NULL;
	END gm_fch_system_rule;

	/*******************************************************
	* Description : Procedure to fetch System Rule Type
	* Author	  : Gopinathan
	*******************************************************/
	PROCEDURE gm_fch_sys_rule_type(
		p_system_id		  IN	   t9720_system_rule.c207_system_id%TYPE
	  , p_out_rule_type	  OUT		TYPES.cursor_type
	  , p_out_link_rule	  OUT		TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_out_rule_type
			FOR
				SELECT t9721.c9721_system_rule_type_id SYSRULETYPEID, t9721.c901_group_type GRPTYPEID
					 , t9721.c207_system_id SYSTEMID, t9721.c9721_qty QTY, get_code_name(c901_group_type) GROUPTYPENM
					 , t9721.c9721_system_rule_type_id SRULETYPID
			      FROM t9721_system_rule_type t9721
			     WHERE t9721.c9721_link_system_rule_type_id IS NULL
			       AND t9721.c207_system_id                  = p_system_id
			       AND t9721.c9721_void_fl                  IS NULL
			  ORDER BY t9721.c9721_system_rule_type_id;
		OPEN p_out_link_rule
			FOR
				SELECT t9721.c9721_system_rule_type_id LSYSRULETYPEID, t9721.c901_group_type LGRPTYPEID, t9721.c207_system_id LSYSTEMID
					 , t9721.c9721_link_system_rule_type_id LINKEDRULETYPEID, t9721.c9721_qty QTY
					 , t9721.c9721_system_rule_type_id SRULETYPID,get_code_name(c901_group_type) GROUPTYPENM
			      FROM t9721_system_rule_type t9721
			     WHERE t9721.c9721_link_system_rule_type_id IS NOT NULL
			       AND t9721.c207_system_id                  = p_system_id
			       AND t9721.c9721_void_fl                  IS NULL
			  ORDER BY t9721.c9721_system_rule_type_id;
	END gm_fch_sys_rule_type;

	/*******************************************************
	* Description : Procedure to fetch System All Rule Type
	* Author	  : Gopinathan
	*******************************************************/
	PROCEDURE gm_fch_sys_all_rule_type(
		p_system_id		  IN	   t9720_system_rule.c207_system_id%TYPE
	  , p_out_rule_type	  OUT		TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_out_rule_type
			FOR
				SELECT t9721.c9721_system_rule_type_id SYSRULETYPEID, t9721.c901_group_type GRPTYPEID, t9721.c9721_qty QTY
					 , t9721.c207_system_id SYSTEMID, get_code_name(c901_group_type) GROUPTYPENM, t9721.c9721_link_system_rule_type_id LINKEDRULETYPEID 
			      FROM t9721_system_rule_type t9721
			     WHERE t9721.c207_system_id                  = p_system_id
			       AND t9721.c9721_void_fl                  IS NULL
			  ORDER BY NVL(c9721_link_system_rule_type_id, c9721_system_rule_type_id),
			           c9721_system_rule_type_id;
	END gm_fch_sys_all_rule_type;

	/***********************************************************
	* Description : Procedure to fetch all Sets base on group
	* Author	  : Gopinathan
	*************************************************************/
	PROCEDURE gm_fch_mapped_sets_grp(
		p_system_id		 IN	   	   t9720_system_rule.c207_system_id%TYPE
	  , p_grptype	     IN		   t4010_group.c901_group_type%TYPE
	  , p_sets_grp       OUT		TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_sets_grp
			FOR
				 SELECT DECODE(c207_set_id, p_system_id, 1, 2) seq, c207_set_id SYSTEMID,get_set_name (c207_set_id) SYSTEMNM, c4010_group_id GROUPID,c4010_group_nm GROUPNM
				   FROM t4010_group
				  WHERE c901_group_type  = p_grptype
				    AND c4010_void_fl   IS NULL
				    AND c4010_publish_fl = 'Y'
				    AND c207_set_id IS NOT NULL
			   ORDER BY seq, systemnm,groupnm;
	END gm_fch_mapped_sets_grp;

	/***********************************************************
	* Description : Procedure to fetch system rule group
	* Author	  : Gopinathan
	*************************************************************/
	PROCEDURE gm_fch_sys_rule_grp(
		p_sys_rule_typeid  IN		t9721_system_rule_type.c9721_system_rule_type_id%TYPE
	  , p_sets_grp         OUT		TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_sets_grp
			FOR
			 SELECT t9722.c9722_classification_grp_id CLASSGRPID, t9722.c4010_group_id GROUPID, t4010.c4010_group_nm GROUPNM
			      , t9722.c9721_system_rule_type_id SYSRULETYPEID, t9722.c207_system_id SYSTEMID
			   FROM t9722_system_rule_grp t9722, t4010_group t4010 
			  WHERE t9722.c9721_system_rule_type_id = p_sys_rule_typeid
			    AND t9722.c4010_group_id = t4010.c4010_group_id
			    AND t9722.c9722_void_fl   IS NULL
			    AND t4010.c4010_void_fl   IS NULL
				AND t4010.c4010_publish_fl = 'Y'
				AND t4010.c207_set_id IS NOT NULL;
	END gm_fch_sys_rule_grp;
	
	/*******************************************************
	* Description : Procedure to fetch System Rule Type By Group Type
	* Author	  : Gopinathan
	*******************************************************/
	PROCEDURE gm_fch_sys_ruletype_by_grptype(
		p_system_id		  IN	   t9720_system_rule.c207_system_id%TYPE
	  , p_sys_rule_typeid IN	   t9721_system_rule_type.c9721_system_rule_type_id%TYPE
      , p_group_type	  IN       t9721_system_rule_type.c901_group_type%TYPE
	  , p_out       	  OUT	   TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_out
			FOR
				SELECT t9721.c9721_system_rule_type_id SYSRULETYPEID, t9721.c901_group_type CURGROUPTYPE
					 , t9721.c207_system_id SYSTEMID, t9721.c9721_qty QTY, get_code_name(t9721.c901_group_type) SCREENTITLE
					 , get_doclassify_grp_type(t9721.c207_system_id) STRSELECTEDGRPTYPE, get_set_name (c207_system_id) SYSTEMNM
			      FROM t9721_system_rule_type t9721
			     WHERE t9721.c9721_system_rule_type_id = NVL(p_sys_rule_typeid,t9721.c9721_system_rule_type_id)
			       AND t9721.c901_group_type = NVL(p_group_type,t9721.c901_group_type)
			       AND t9721.c207_system_id  = p_system_id
			       AND t9721.c9721_void_fl   IS NULL
			       AND ROWNUM = 1;
	END gm_fch_sys_ruletype_by_grptype;
END gm_pkg_pd_classify_ord_rpt;
/