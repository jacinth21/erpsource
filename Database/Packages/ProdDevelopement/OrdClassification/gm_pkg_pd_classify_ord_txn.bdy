/* Formatted on 2012/04/24 12:45 (Formatter Plus v4.8.0) */

--@"C:\Database\Packages\ProdDevelopement\OrdClassification\gm_pkg_pd_classify_ord_txn.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_pd_classify_ord_txn
IS
--

	/*******************************************************
	* Description : Procedure to save System Rule Info
	* Author	  : Gopinathan
	*******************************************************/
	PROCEDURE gm_sav_system_rule (
		p_system_id 	 IN   t9720_system_rule.c207_system_id%TYPE
	  , p_status		 IN   t9720_system_rule.c901_status%TYPE
	  , p_published_by	 IN   t9720_system_rule.c9720_published_by%TYPE
	  , p_published_dt	 IN   t9720_system_rule.c9720_published_dt%TYPE
	  , p_inputstr		 IN   VARCHAR2
	  , p_userid		 IN   t9720_system_rule.c9720_created_by%TYPE
	)
	AS
		v_strlen	   NUMBER := NVL (LENGTH (p_inputstr), 0);
		v_string	   VARCHAR2 (30000) := p_inputstr;
		v_substring1   VARCHAR2 (1000);
		v_substring2   VARCHAR2 (1000);
		v_status	   t9720_system_rule.c901_status%TYPE;
		v_link_typeid  t9721_system_rule_type.c9721_link_system_rule_type_id%TYPE;
		v_qty		   t9721_system_rule_type.c9721_qty%TYPE;
		v_sys_rule_typeid t9721_system_rule_type.c9721_system_rule_type_id%TYPE;
		v_grp_type	   t9721_system_rule_type.c901_group_type%TYPE;
	BEGIN
		BEGIN
			SELECT c901_status
			  INTO v_status
			  FROM t9720_system_rule
			 WHERE c207_system_id = p_system_id AND c9720_void_fl IS NULL;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				v_status	:= 0;
		END;

		UPDATE t9720_system_rule
		   SET c901_status = p_status
			 , c9720_published_by = NVL (p_published_by, c9720_published_by)
			 , c9720_published_dt = NVL (p_published_dt, c9720_published_dt)
			 , c9720_last_updated_by = p_userid
			 , c9720_last_updated_date = SYSDATE
		 WHERE c207_system_id = p_system_id AND c9720_void_fl IS NULL;

		IF (SQL%ROWCOUNT = 0)
		THEN
			INSERT INTO t9720_system_rule
						(c207_system_id, c901_status, c9720_published_by, c9720_published_dt, c9720_created_by
					   , c9720_created_date
						)
				 VALUES (p_system_id, p_status, p_published_by, p_published_dt, p_userid
					   , SYSDATE
						);
		END IF;

		-- inputString Format is 'Qty,Pkey^Screws,Pkey^Hooks,|Qty,Pkey^Rods,|Qty,Pkey^Caps,|'
		WHILE INSTR (v_string, '|') <> 0
		LOOP
			v_substring1 := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
			v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
			v_qty		:= TO_NUMBER (SUBSTR (v_substring1, 1, INSTR (v_substring1, ',') - 1));
			v_substring1 := SUBSTR (v_substring1, INSTR (v_substring1, ',') + 1);

			WHILE INSTR (v_substring1, ',') <> 0
			LOOP
				v_substring2 := SUBSTR (v_substring1, 1, INSTR (v_substring1, ',') - 1);
				v_substring1 := SUBSTR (v_substring1, INSTR (v_substring1, ',') + 1);
				v_sys_rule_typeid := SUBSTR (v_substring2, 1, INSTR (v_substring2, '^') - 1);
				v_substring2 := SUBSTR (v_substring2, INSTR (v_substring2, '^') + 1);
				v_grp_type	:= TO_NUMBER (v_substring2);
				gm_sav_sys_rule_type (v_sys_rule_typeid
									, p_system_id
									, v_status
									, v_grp_type
									, v_qty
									, v_link_typeid
									, p_userid
									 );

				BEGIN
					--get root system_rule_type_id
					SELECT NVL (c9721_link_system_rule_type_id, c9721_system_rule_type_id)
					  INTO v_link_typeid
					  FROM t9721_system_rule_type
					 WHERE c9721_system_rule_type_id = v_sys_rule_typeid AND c9721_void_fl IS NULL;
				EXCEPTION
					WHEN NO_DATA_FOUND
					THEN
						v_link_typeid := NULL;
				END;
			END LOOP;

			v_link_typeid := NULL;
		END LOOP;
	END gm_sav_system_rule;

	/*******************************************************
	* Description : Procedure to save System Rule Type
	* Author	  : Gopinathan
	*******************************************************/
	PROCEDURE gm_sav_sys_rule_type (
		p_sys_rule_typeid	IN OUT	 t9721_system_rule_type.c9721_system_rule_type_id%TYPE
	  , p_system_id 		IN		 t9720_system_rule.c207_system_id%TYPE
	  , p_sys_status		IN		 t9720_system_rule.c901_status%TYPE
	  , p_grp_type			IN		 t9721_system_rule_type.c901_group_type%TYPE
	  , p_qty				IN		 t9721_system_rule_type.c9721_qty%TYPE
	  , p_link_typeid		IN		 t9721_system_rule_type.c9721_link_system_rule_type_id%TYPE
	  , p_userid			IN		 t9721_system_rule_type.c9721_last_updated_by%TYPE
	)
	AS
		v_sys_rule_typeid NUMBER;
	BEGIN
		IF p_grp_type IS NULL
		THEN
			gm_void_sys_rule_type (p_sys_rule_typeid, p_sys_status, p_userid);
		ELSE
			UPDATE t9721_system_rule_type
			   SET c901_group_type = p_grp_type
				 , c9721_qty = p_qty
				 , c9721_link_system_rule_type_id = NVL (p_link_typeid, c9721_link_system_rule_type_id)
				 , c9721_last_updated_by = p_userid
				 , c9721_last_updated_date = SYSDATE
			 WHERE c9721_system_rule_type_id = p_sys_rule_typeid AND c9721_void_fl IS NULL;

			IF (SQL%ROWCOUNT = 0)
			THEN
				SELECT s9721_system_rule_type.NEXTVAL
				  INTO v_sys_rule_typeid
				  FROM DUAL;

				INSERT INTO t9721_system_rule_type
							(c9721_system_rule_type_id, c207_system_id, c901_group_type, c9721_qty
						   , c9721_link_system_rule_type_id, c9721_last_updated_by, c9721_last_updated_date
							)
					 VALUES (v_sys_rule_typeid, p_system_id, p_grp_type, p_qty
						   , p_link_typeid, p_userid, SYSDATE
							);

				p_sys_rule_typeid := v_sys_rule_typeid;
			END IF;
		END IF;
	END gm_sav_sys_rule_type;

	/*******************************************************
	* Description : Procedure to void System Rule Type
	* Author	  : Gopinathan
	*******************************************************/
	PROCEDURE gm_void_sys_rule_type (
		p_sys_rule_typeid	IN	 t9721_system_rule_type.c9721_system_rule_type_id%TYPE
	  , p_sys_status		IN	 t9720_system_rule.c901_status%TYPE
	  , p_userid			IN	 t9721_system_rule_type.c9721_last_updated_by%TYPE
	)
	AS
		v_cnt_link	   NUMBER;
		v_link_typeid  t9721_system_rule_type.c9721_link_system_rule_type_id%TYPE;
	BEGIN
		SELECT COUNT (1)
		  INTO v_cnt_link
		  FROM t9721_system_rule_type
		 WHERE c9721_link_system_rule_type_id = p_sys_rule_typeid AND c9721_void_fl IS NULL;

		--update linked records link_system_rule_id with first linked record system_rule_id
		IF v_cnt_link > 0
		THEN
			BEGIN
				SELECT MIN (c9721_system_rule_type_id)
				  INTO v_link_typeid
				  FROM t9721_system_rule_type
				 WHERE c9721_link_system_rule_type_id = p_sys_rule_typeid AND c9721_void_fl IS NULL;
			EXCEPTION
				WHEN NO_DATA_FOUND
				THEN
					v_link_typeid := NULL;
			END;

			UPDATE t9721_system_rule_type
			   SET c9721_link_system_rule_type_id = NULL
				 , c9721_last_updated_by = p_userid
				 , c9721_last_updated_date = SYSDATE
			 WHERE c9721_system_rule_type_id = v_link_typeid AND c9721_void_fl IS NULL;

			UPDATE t9721_system_rule_type
			   SET c9721_link_system_rule_type_id = v_link_typeid
				 , c9721_last_updated_by = p_userid
				 , c9721_last_updated_date = SYSDATE
			 WHERE c9721_link_system_rule_type_id = p_sys_rule_typeid AND c9721_void_fl IS NULL;
		END IF;

		UPDATE t9721_system_rule_type
		   SET c9721_void_fl = 'Y'
			 , c9721_last_updated_by = p_userid
			 , c9721_last_updated_date = SYSDATE
		 WHERE c9721_system_rule_type_id = p_sys_rule_typeid AND c9721_void_fl IS NULL;

		UPDATE t9722_system_rule_grp
		   SET c9722_void_fl = 'Y'
			 , c9722_last_updated_by = p_userid
			 , c9722_last_updated_date = SYSDATE
		 WHERE c9721_system_rule_type_id = p_sys_rule_typeid AND c9722_void_fl IS NULL;

	END gm_void_sys_rule_type;

	/*******************************************************
	* Description : Procedure to save System Rule Group
	* Author	  : Gopinathan
	*******************************************************/
	PROCEDURE gm_sav_sys_rule_grp (
		p_system_id 		IN	 t9722_system_rule_grp.c207_system_id%TYPE
	  , p_sys_rule_typeid	IN	 t9721_system_rule_type.c9721_system_rule_type_id%TYPE
	  , p_inputstr			IN	 VARCHAR2
	  , p_userid			IN	 t9722_system_rule_grp.c9722_last_updated_by%TYPE
	)
	AS
		v_strlen	   NUMBER := NVL (LENGTH (p_inputstr), 0);
		v_string	   VARCHAR2 (30000) := p_inputstr;
		v_grp_id	   t9722_system_rule_grp.c4010_group_id%TYPE;
	BEGIN
		my_context.set_my_inlist_ctx (p_inputstr);
		UPDATE t9722_system_rule_grp
		   SET c9722_void_fl = 'Y'
		 WHERE c9722_classification_grp_id IN (
				   SELECT c9722_classification_grp_id
					 FROM t9722_system_rule_grp
					WHERE c207_system_id = p_system_id
					  AND c9721_system_rule_type_id = p_sys_rule_typeid
					  AND c4010_group_id NOT IN (SELECT token
												   FROM v_in_list WHERE token IS NOT NULL))
		   AND c9721_system_rule_type_id = p_sys_rule_typeid;

		WHILE INSTR (v_string, ',') <> 0
		LOOP
			v_grp_id	:= SUBSTR (v_string, 1, INSTR (v_string, ',') - 1);
			v_string	:= SUBSTR (v_string, INSTR (v_string, ',') + 1);

			UPDATE t9722_system_rule_grp
			   SET c4010_group_id = v_grp_id
				 , c9722_last_updated_by = p_userid
				 , c9722_last_updated_date = SYSDATE
			 WHERE c9721_system_rule_type_id = p_sys_rule_typeid
			   AND c207_system_id = p_system_id
			   AND c4010_group_id = v_grp_id
			   AND c9722_void_fl IS NULL;

			IF (SQL%ROWCOUNT = 0)
			THEN
				INSERT INTO t9722_system_rule_grp
							(c9722_classification_grp_id, c4010_group_id, c9721_system_rule_type_id, c207_system_id
						   , c9722_last_updated_by, c9722_last_updated_date
							)
					 VALUES (s9722_system_rule_grp.NEXTVAL, v_grp_id, p_sys_rule_typeid, p_system_id
						   , p_userid, SYSDATE
							);
			END IF;
		END LOOP;
	END gm_sav_sys_rule_grp;
END gm_pkg_pd_classify_ord_txn;
/
