-- @"C:\Database\Packages\ProdDevelopement\gm_pkg_pd_system_trans.pkg"
CREATE OR REPLACE
PACKAGE gm_pkg_pd_system_trans
IS
    --
    /*****************************************************************************************
    * Description : This procedure is used to save new system
    *******************************************************************************************/
PROCEDURE gm_sav_system_detail (
        p_system_id   IN t207_set_master.c207_set_id%type,
        p_system_name IN t207_set_master.c207_set_nm%type,
        p_sys_desc    IN t207_set_master.c207_set_desc%TYPE,
        p_det_desc    IN t207_set_master.c207_set_dtl_desc%TYPE,
        p_comment     IN t902_log.c902_comments%TYPE,
        p_user_id     IN t207_set_master.c207_created_by%TYPE,
        p_division_id IN t207_set_master.c1910_division_id%TYPE,
        p_company_id  IN t207_set_master.c1900_company_id%TYPE,
        p_out_sysid OUT VARCHAR2) ;
    /*****************************************************************************************
    * Description : This procedure is creat links for Standard and Additional  sets
    * std set string format : '978.901,N|978.902,Y'
    * set name format: 'REVLOK Implant and Instrument Set^REVLOK Disposable Instruments Set'
    * addtional set format : '978.903,978.904'
    * locaner set format : '978.901LN,978.902LN'
    *******************************************************************************************/
PROCEDURE gm_sav_system_map (
        p_system_id           IN t207_set_master.c207_set_id%TYPE,
        p_std_set_ids         IN VARCHAR2,
        p_addl_set_ids        IN VARCHAR2,
        p_loaner_std_set_ids  IN VARCHAR2,
        p_loaner_addl_set_ids IN VARCHAR2,
        p_user_id             IN t207_set_master.c207_created_by%TYPE,
        p_action              IN VARCHAR2,
        p_division_id         IN t207_set_master.c1910_division_id%TYPE,
        p_company_id          IN t207_set_master.c1900_company_id%TYPE) ;
    /*****************************************************************************************
    * Description : This procedure is creat links for Standard sets
    *******************************************************************************************/
PROCEDURE gm_link_std_set (
        p_std_set_ids IN VARCHAR2,
        p_set_type    IN t207_set_master.c207_type%TYPE,
        p_user_id     IN t101_user.c101_user_id%TYPE,
        p_system_id   IN t207_set_master.c207_set_id%TYPE,
        p_action      IN VARCHAR2,
        p_division_id IN t207_set_master.c1910_division_id%TYPE,
        p_company_id  IN t207_set_master.c1900_company_id%TYPE) ;
    /*****************************************************************************************
    * Description : This procedure is creat links for Additional available sets
    *******************************************************************************************/
PROCEDURE gm_link_additional_set (
        p_additional_set_ids IN VARCHAR2,
        p_set_type           IN t207_set_master.c207_type%TYPE,
        p_user_id            IN t101_user.c101_user_id%TYPE,
        p_system_id          IN t207_set_master.c207_set_id%TYPE,
        p_action             IN VARCHAR2,
        p_division_id         IN t207_set_master.c1910_division_id%TYPE,
        p_company_id          IN t207_set_master.c1900_company_id%TYPE) ;
    /*****************************************************************************************
    * Description : This procedure is save new set in set master table
    *******************************************************************************************/
PROCEDURE gm_sav_set_master (
        p_set_id       IN t207_set_master.c207_set_id%TYPE,
        p_set_nm       IN t207_set_master.c207_set_nm%TYPE,
        p_set_desc     IN t207_set_master.c207_set_desc%TYPE,
        p_user_id      IN t205_part_number.c205_last_updated_by%TYPE,
        p_category     IN t207_set_master.c207_category%TYPE,
        p_set_grp_type IN t207_set_master.c901_set_grp_type%TYPE,
        p_seq_no       IN t207_set_master.c207_seq_no%TYPE,
        p_rpt_id       IN t207_set_master.c901_cons_rpt_id%TYPE,
        p_hierarchy    IN t207_set_master.c901_hierarchy%TYPE,
        p_set_dtl_desc IN t207_set_master.c207_set_dtl_desc%TYPE,
        p_division_id  IN t207_set_master.c1910_division_id%TYPE,
        p_company_id   IN t207_set_master.c1900_company_id%TYPE) ;
    /*****************************************************************************************
    * Description : This procedure updates the system details as well as linked set details when a system is edited
    *******************************************************************************************/
PROCEDURE gm_sav_upd_linked_set_master (
        p_sys_id       IN t207_set_master.c207_set_id%TYPE,
        p_set_nm       IN t207_set_master.c207_set_nm%TYPE,
        p_set_desc     IN t207_set_master.c207_set_desc%TYPE,
        p_user_id      IN t205_part_number.c205_last_updated_by%TYPE,
        p_set_dtl_desc IN t207_set_master.c207_set_dtl_desc%TYPE,
        p_division_id  IN t207_set_master.c1910_division_id%TYPE,
        p_company_id   IN t207_set_master.c1900_company_id%TYPE) ;
    /*****************************************************************************************
    * Description : This procedure update set detail when a system detail is edited
    *******************************************************************************************/
PROCEDURE gm_sav_upd_set_master (
        p_set_id       IN t207_set_master.c207_set_id%TYPE,
        p_set_nm       IN t207_set_master.c207_set_nm%TYPE,
        p_set_desc     IN t207_set_master.c207_set_desc%TYPE,
        p_user_id      IN t205_part_number.c205_last_updated_by%TYPE,
        p_set_dtl_desc IN t207_set_master.c207_set_dtl_desc%TYPE,
        p_division_id  IN t207_set_master.c1910_division_id%TYPE,
        p_company_id   IN t207_set_master.c1900_company_id%TYPE) ;
    /*****************************************************************************************
    * Description : This procedure is save new record in set link table
    *******************************************************************************************/
PROCEDURE gm_sav_set_link (
        p_main_setid  IN t207a_set_link.c207_main_set_id%TYPE,
        p_set_linkid  IN t207a_set_link.c207_link_set_id%TYPE,
        p_type        IN t207a_set_link.c901_type%TYPE,
        p_set_type    IN t207_set_master.c207_type%TYPE,
        p_shared      IN t207a_set_link.c901_shared_status%type,
        p_hierarcy_tp IN t207_set_master.c901_hierarchy%type,
        p_action      IN VARCHAR2,
        p_division_id IN t207_set_master.c1910_division_id%TYPE,
        p_company_id  IN t207_set_master.c1900_company_id%TYPE) ;
PROCEDURE gm_save_validate_set_link (
        p_main_setid  IN t207a_set_link.c207_main_set_id%TYPE,
        p_set_linkid  IN t207a_set_link.c207_link_set_id%TYPE,
        p_type        IN t207a_set_link.c901_type%type,
        p_set_type    IN t207_set_master.c207_type%type,
        p_shared      IN t207a_set_link.c901_shared_status%type,
        p_hierarcy_tp IN t207_set_master.c901_hierarchy%type,
        p_action      IN VARCHAR2,
        p_division_id IN t207_set_master.c1910_division_id%TYPE,
        p_company_id  IN t207_set_master.c1900_company_id%TYPE) ;
    /*****************************************************************************************
    * Description : This procedure is creat mappings for parent and itz AI child sets.
    * Eg: for 965.905 and its Additional Inventory AI.XXXX
    *******************************************************************************************/
PROCEDURE gm_sav_set_link_mappings (
        p_main_setid   IN t207a_set_link.c207_main_set_id%TYPE,
        p_set_linkid   IN t207a_set_link.c207_link_set_id%TYPE,
        p_child_set_id IN t207a_set_link.c207_link_set_id%TYPE,
        p_type         IN t207a_set_link.c901_type%TYPE,
        p_set_type     IN t207_set_master.c207_type%TYPE,
        p_existing_fl  IN VARCHAR2,
        p_shared       IN t207a_set_link.c901_shared_status%type) ;
    /*****************************************************************************************
    * Description : This procedure is insert mappings in the set link table
    *******************************************************************************************/
PROCEDURE gm_insert_set_link (
        p_main_setid IN t207a_set_link.c207_main_set_id%TYPE,
        p_set_linkid IN t207a_set_link.c207_link_set_id%TYPE,
        p_type       IN t207a_set_link.c901_type%TYPE,
        p_shared     IN t207a_set_link.c901_shared_status%type) ;
    /*****************************************************************************************
    * Description : This procedure is update record in set master table
    *******************************************************************************************/
PROCEDURE gm_upt_set_master (
        p_set_id IN t207_set_master.c207_set_id%TYPE,
        p_user_id t205_part_number.c205_last_updated_by%TYPE,
        p_systemid     IN t207_set_master.c207_set_sales_system_id%TYPE,
        p_minset_logic IN t207_set_master.c901_minset_logic%TYPE,
        p_hierarchy    IN t207_set_master.c901_hierarchy%TYPE,
        p_baseline_fl  IN VARCHAR2,
        p_void_fl      IN t207_set_master.c207_void_fl%TYPE) ;
    /*****************************************************************************************
    * Description : This procedure is update record in set detail table
    *This procedure will also assign parts to common pool if a set is removed from a system.
    *******************************************************************************************/
PROCEDURE gm_upt_set_details (
        p_max_set      IN t208_set_details.c207_set_id%TYPE,
        p_set_id       IN t208_set_details.c207_set_id%TYPE,
        p_unassign_sys IN t208_set_details.c207_set_id%TYPE,
        p_user_id      IN t205_part_number.c205_last_updated_by%TYPE) ;
    /*****************************************************************************************
    * Description : This procedure is to create a new AI Set, if it wasnt there already
    *******************************************************************************************/
PROCEDURE gm_sav_ai_set (
        p_set_linkid  IN t207a_set_link.c207_link_set_id%TYPE,
        p_type        IN t207a_set_link.c901_type%TYPE,
        p_set_type    IN t207_set_master.c207_type%TYPE,
        p_division_id IN t207_set_master.c1910_division_id%TYPE,
        p_company_id  IN t207_set_master.c1900_company_id%TYPE) ;
    /*****************************************************************************************
    * Description : This procedure is to edit links when a set is edited
    *******************************************************************************************/
PROCEDURE gm_sav_edit_linked_set (
        p_main_set_id   IN t207a_set_link.c207_main_set_id%type,
        p_linked_set_id IN t207a_set_link.c207_link_set_id%type,
        p_type          IN t207a_set_link.c901_type%TYPE,
        p_shared        IN t207a_set_link.c901_shared_status%type,
        p_hierarcy_type IN t207_set_master.c901_hierarchy%type,
        p_set_type      IN t207_set_master.c207_type%TYPE,
        p_division_id   IN t207_set_master.c1910_division_id%TYPE,
        p_company_id    IN t207_set_master.c1900_company_id%TYPE) ;
    /*****************************************************************************************
    * Description : Common Proc to delete sets links
    *******************************************************************************************/
PROCEDURE gm_sav_del_linked_set (
        p_main_set_id       IN t207a_set_link.c207_main_set_id%type,
        p_linked_set_id     IN t207a_set_link.c207_link_set_id%type,
        p_old_hierarcy_type IN t207_set_master.c901_hierarchy%type,
        p_out_change_settype OUT VARCHAR2) ;
    /*****************************************************************************************
    * Description : This procedure is to void a system
    *******************************************************************************************/
PROCEDURE gm_void_system (
        p_sys_id  IN t207_set_master.c207_set_id%TYPE,
        p_user_id IN t207_set_master.c207_created_by%type) ;
    /*****************************************************************************************
    * Description : This procedure is to void/unlink  a set mapped in a system
    *******************************************************************************************/
PROCEDURE gm_void_system_map (
        p_inputstr VARCHAR2,
        p_user_id IN t207_set_master.c207_created_by%type) ;
    /**************************************************************
    * Description : Procedure to save set attributes details
    * Author  : Elango
    ***************************************************************/
PROCEDURE gm_sav_set_attribute (
        p_setid             IN t207c_set_attribute.c207_set_id%TYPE,
        p_setattrInputstr   IN CLOB,
        p_attrtype_inputstr IN CLOB,
        p_userid            IN t207c_set_attribute.c207c_created_by%TYPE) ;
    /**************************************************************
    * Description : Procedure to save set(System) attributes details
    * Author  : Jignesh Shah
    ***************************************************************/
PROCEDURE gm_sav_set_attr (
        p_setid      IN t207c_set_attribute.c207_set_id%TYPE,
        p_attr_type  IN t207c_set_attribute.c901_attribute_type%TYPE,
        p_attr_value IN t207c_set_attribute.c207c_attribute_value%TYPE,
        p_userid     IN t207c_set_attribute.c207c_created_by%TYPE) ;
    /**************************************************************
    * Description : Procedure to save System company mapping details
    * Author  : rdinesh
    ***************************************************************/
PROCEDURE gm_sav_system_company_map (
        p_setid      IN t207_set_master.c207_set_id%TYPE,
        p_company_id IN t207_set_master.c1900_company_id%TYPE,
        p_map_type   IN t2080_set_company_mapping.c901_set_mapping_type%TYPE,
        p_userid     IN t2080_set_company_mapping.c2080_last_updated_by%TYPE) ;
   /**************************************************************
    * Description : Procedure to void System company mapping details
    * Author  : rdinesh
    ***************************************************************/      
        
PROCEDURE gm_void_system_company_map (
  	p_setid      		IN t207_set_master.c207_set_id%TYPE
  , p_company_id        IN      t2021_project_company_mapping.c1900_company_id%TYPE	  
  , p_userid			IN		t2021_project_company_mapping.c2021_last_updated_by%TYPE
);

    /**************************************************************
    * Description : Procedure to save System company mapping details
    * Author  : 
    ***************************************************************/
PROCEDURE gm_sav_system_company_map (
        p_setid      IN t207_set_master.c207_set_id%TYPE,
        p_company_id IN t207_set_master.c1900_company_id%TYPE,
        p_map_type   IN t2080_set_company_mapping.c901_set_mapping_type%TYPE,
        p_statusId   IN t2080_set_company_mapping.c901_ipad_released_status%TYPE,
        p_userid     IN t2080_set_company_mapping.c2080_last_updated_by%TYPE) ;
END gm_pkg_pd_system_trans;
/