--@"C:\Database\Packages\ProdDevelopement\gm_pkg_cm_entity_rpt.bdy";
CREATE OR REPLACE
PACKAGE BODY gm_pkg_cm_entity_rpt
IS
  /*****************************************************************************
  * Description : This procedure used to fetch region-company details [PMT-40410]
  * Author      : Ramachandiran T S
  ******************************************************************************/
  
  PROCEDURE gm_fch_entity_company_dtls(
  
  p_entity_type          IN      t1902_entity_company_map.c1900_company_id%TYPE,
  p_out_company_dtls     OUT     VARCHAR2)
  
  AS
  
  BEGIN
  
  	SELECT JSON_ARRAYAGG(
 		JSON_OBJECT 
  		('companyid' value t1902.c1900_company_id,
  		 'companyname' value t1900.C1900_company_name)
  		ORDER BY t1900.c1900_company_name ASC returning CLOB)
  		INTO p_out_company_dtls
		FROM t1902_entity_company_map t1902,
			 t1900_COMPANY t1900
		WHERE t1902.c901_entity_id=p_entity_type
		AND t1900.c1900_company_id = t1902.c1900_company_id
		AND t1900.C1900_void_fl is null
		AND t1902.c1902_void_fl IS null;

  END gm_fch_entity_company_dtls; 
  
 END gm_pkg_cm_entity_rpt; 
  /