/* Formatted on 2010/07/12 10:58 (Formatter Plus v4.8.0) */
--@"C:\Database\Packages\ProdDevelopement\gm_pkg_pd_group_pricing.bdy";
CREATE OR REPLACE
PACKAGE BODY gm_pkg_group_part_email
IS
    --
    /**************************************************************************************************
    Description : This procedure is used to fetch Groupname, PartNumber details  and Account Details
    Whenever new part is added into a new group or an existing group
    **************************************************************************************************/
PROCEDURE gm_new_group_part_map_email (
        p_email_status IN NUMBER,
        p_group_dtls OUT TYPES.cursor_type,
        p_part_dtls OUT TYPES.cursor_type,
        p_account_dtls OUT TYPES.cursor_type)
AS
    v_part_ids VARCHAR2 (4000) ;
    v_company_id T1900_COMPANY.C1900_COMPANY_ID%TYPE;
    v_default_company T1900_COMPANY.C1900_COMPANY_ID%TYPE;
    
 /*   
    CURSOR curr_group
    IS
         SELECT t4011.c205_part_number_id partid
           FROM t4011_group_detail t4011, t4010_group t4010
          WHERE t4011.c4011_created_date    IS NOT NULL
            AND t4011.c4010_group_id         = t4010.c4010_group_id
            AND t4010.c4010_void_fl         IS NULL
            AND t4010.c4010_publish_fl       = 'Y'
            AND t4011.c901_part_pricing_type = 52080 -- Primary Part
            AND t4010.c901_type              = 40045 -- Forecast/Demand Group
            AND t4011.c4011_email_flag      IS NULL
            AND NVL(t4011.c901_email_status,'-999')      = NVL (p_email_status, '-999') ;
   */         
BEGIN
	
     SELECT get_compid_frm_cntx () INTO v_company_id FROM DUAL;
     
    v_default_company := NVL (get_rule_value ('DEFAULT_COMPANY', 'ONEPORTAL'), '1000') ;
    
    IF v_company_id   IS NULL THEN
        v_company_id  := v_default_company;
    END IF;
    
/*
    FOR currindex1 IN curr_group
    
    LOOP
        v_part_ids := v_part_ids || ',' || currindex1.partid;
    END LOOP;
    
    my_context.set_my_inlist_ctx (v_part_ids) ;
 */  
    OPEN p_group_dtls 
    FOR 
       SELECT t4010.c4010_group_nm gname,
          t4010.c4010_group_id g_id,
	      t4010.c4010_last_updated_by updated_by 
	    FROM t4010_group t4010 
	    WHERE t4010.c4010_void_fl IS NULL 
	    AND t4010.c901_type=40045 --Forecast/Demand Group
	    AND t4010.c4010_publish_fl = 'Y' 
	    AND t4010.c4010_email_flag IS NULL
	    AND NVL(T4010.C901_EMAIL_STATUS, '-999') = NVL(p_email_status,'-999');
    
    OPEN p_part_dtls 
    FOR 
	    SELECT t4011.c205_part_number_id partno,
	    t205.c205_part_num_desc partdesc,
	    t4010.c4010_group_nm gname,
	    (t101.C101_USER_F_NAME||' '||t101.C101_USER_L_NAME) updateby,
	    t2052.c2052_list_price lprice,
	    t2052.c2052_equity_price tprice,
	    t4011.c4010_group_id g_id
	    FROM t4011_group_detail t4011,
	    t4010_group t4010,
	    t205_part_number t205,
	    t2052_part_price_mapping t2052,
	    t101_user t101
	    WHERE t4011.c4011_created_date  IS NOT NULL 
	    AND t4010.c901_type = 40045 --Forecast/Demand Group
	    AND t4011.c901_part_pricing_type = 52080 -- Primary Part
	    AND t4011.c4010_group_id = t4010.c4010_group_id 
	    AND t4010.c4010_void_fl IS NULL 
	    AND t4010.c4010_publish_fl = 'Y'
	    AND t101.c101_user_id = t4011.c4011_created_by 
	    AND t205.c205_part_number_id = t4011.c205_part_number_id
	    AND t2052.c205_part_number_id =t205.c205_part_number_id
	    AND t2052.c205_part_number_id = t4011.c205_part_number_id
	    AND t4011.c4011_email_flag IS NULL 
	    AND T2052.c1900_company_id(+)= v_company_id
	    AND NVL(t4011.c901_email_status,'-999') = NVL(p_email_status, '-999') ;
    
    OPEN p_account_dtls 
    FOR
  
    SELECT t4011.c205_part_number_id partno, T7051.C704_ACCOUNT_ID accno, t704.c704_account_nm name
  , NVL (globus_app.get_account_gpo_name (NVL (T7051.c101_gpo_id, get_account_gpo_id (T7051.C704_ACCOUNT_ID))), '')
    gpoid, DECODE (t704d.c901_contract, 903100, 'Yes', 'No') cflag
    FROM t7051_account_group_pricing T7051, t4011_group_detail t4011, t4010_group t4010
  , t704d_account_affln t704d, t704_account t704
    WHERE t4010.c4010_group_id       = t4011.c4010_group_id
    AND t4011.c901_part_pricing_type = 52080 -- primary part
    AND t4010.c901_type = 40045 -- Forecast/Demand Group
    AND ( (t7051.c901_ref_type = 52000 -- Group Type
    AND T7051.c7051_ref_id = t4011.c4010_group_id)
    OR (t7051.c901_ref_type  = 52001 -- Individual Type
    AND T7051.c7051_ref_id  = t4011.c205_part_number_id))
    AND T7051.c704_account_id = t704d.c704_account_id (+)
    AND T7051.c704_account_id = T704.c704_account_id
    AND t7051.c7051_void_fl IS NULL
    AND t4010.c4010_void_fl IS NULL
    AND t7051.c7051_active_fl = 'Y'
    AND t4010.c4010_publish_fl = 'Y'
    AND t704d.c704d_void_fl (+) IS NULL
    AND t4011.c4011_email_flag IS NULL
    AND NVL (t4011.c901_email_status, '-999') = NVL (p_email_status, '-999')
    AND DECODE (T704D.C901_CONTRACT, 903100, '903100', '903101') = '903100' -- Contract Flag is Y
ORDER BY GET_ACCOUNT_NAME (T7051.C704_ACCOUNT_ID) ;
    
END gm_new_group_part_map_email;
/****************************************************************************
* Description : This procedure is update the email status for the Group and part email
* notification. Refer columne t4010_group.c901_email_status and t4011_group_detail.c901_email_status column
*
*****************************************************************************/
PROCEDURE gm_group_part_map_email_flag (
        p_email_status IN VARCHAR)
AS
    v_group_dtls TYPES.cursor_type;
    v_part_dtls TYPES.cursor_type;
    v_acct_dtls TYPES.cursor_type;
    v_grp_nm t4010_group.c4010_GROUP_NM%TYPE;
    v_grp_id t4010_group.c4010_group_id%TYPE;
    v_part_num_id t205_part_number.c205_part_number_id%TYPE;
    v_part_num_desc t205_part_number.c205_part_num_desc%TYPE;
    v_created_by t101_user.C101_USER_F_NAME%TYPE;
    v_email_status NUMBER;
    v_updated_by t101_user.c101_user_id%TYPE;
    v_list_price t2052_part_price_mapping.c2052_list_price%TYPE;
    v_trip_price t2052_part_price_mapping.c2052_equity_price%TYPE;
    v_user_id t101_user.c101_user_id%TYPE;
BEGIN
    -- 106620 : Group Part Email Notification Started
    -- 106621 : Group Part Email Sent
    -- 106622 : Group Part Email Job Failed.
    -- When this code calls first time by the job we have to set the email_status to NULL because it is the first step
    -- in the job, so that job will pick
    -- the new records and below it will update status as "Job Started"
     SELECT DECODE (p_email_status, 106620, NULL, 106620)
       INTO v_email_status
       FROM DUAL;
       
    gm_pkg_group_part_email.gm_new_group_part_map_email (v_email_status, v_group_dtls, v_part_dtls, v_acct_dtls);
    
  BEGIN
	 LOOP
       FETCH v_group_dtls
         INTO v_grp_nm, v_grp_id, v_updated_by;
  
  EXIT WHEN v_group_dtls%NOTFOUND; 
   
        UPDATE t4010_group
          SET c901_email_status   = p_email_status
        ,c4010_email_flag         = DECODE (p_email_status, 106621, 'Y', 106622, 'F',c4010_email_flag)
        ,c4010_last_updated_by    = v_updated_by
        ,c4010_last_updated_date  = CURRENT_DATE
          WHERE c4010_group_id    = v_grp_id
          AND c4010_void_fl         IS NULL
          AND c901_type           = 40045 --Forecast/Demand Group
          AND c4010_email_flag IS NULL;
    
    END LOOP;
   CLOSE v_group_dtls;
   
  EXCEPTION
    WHEN OTHERS THEN
        CLOSE v_group_dtls;
        RAISE; 

 END;
 
 BEGIN
  LOOP
    FETCH v_part_dtls
       INTO v_part_num_id, v_part_num_desc, v_grp_nm
      ,v_created_by,v_list_price,v_trip_price,v_grp_id;
      
EXIT WHEN v_part_dtls%NOTFOUND;   

		select c4010_last_updated_by INTO 
		v_user_id from t4010_group 
		where c4010_group_id=v_grp_id;
     
          UPDATE t4010_group
           SET c4010_last_updated_by =v_user_id , c4010_last_updated_date = CURRENT_DATE
          WHERE c4010_group_id    = v_grp_id
          AND c4010_void_fl    IS NULL
          AND c901_type         = 40045; --Forecast/Demand Group
         
         UPDATE t4011_group_detail
           SET c901_email_status = p_email_status, c4011_email_flag = DECODE (p_email_status, 106621, 'Y', 106622, 'F', c4011_email_flag)
           , c4011_last_updated_by=v_user_id
           , c4011_last_updated_date=CURRENT_DATE
           WHERE c4010_group_id         = v_grp_id
          AND c901_part_pricing_type = 52080
          AND c205_part_number_id    = v_part_num_id;
          
    END LOOP;
  CLOSE v_part_dtls;
  
 EXCEPTION
    WHEN OTHERS THEN
        CLOSE v_part_dtls;
        RAISE; 
 END;
  END gm_group_part_map_email_flag;
END GM_PKG_GROUP_PART_EMAIL;
/