--@"C:\Database\Packages\ProdDevelopement\gm_pkg_pd_group_txn.pkg";

CREATE OR REPLACE PACKAGE BODY gm_pkg_pd_group_txn
IS

	/********************************************************
 	 * Description :This procedure will be called when a Group is voided. This will
	 * void all the Company Mapping in T4012_GROUP_COMPANY_MAPPING 
	 * for the Group
	 * Author : Rajeshwaran
	 *********************************************************/
	PROCEDURE gm_void_group_company_map (
		p_group_id 		IN 	T4010_GROUP.C4010_GROUP_ID%TYPE,
		p_company_id	IN   T1900_COMPANY.C1900_COMPANY_ID%TYPE,
		p_userid        IN   T4010_GROUP.C4010_LAST_UPDATED_BY%TYPE
		)
	AS
	BEGIN
			 UPDATE T4012_GROUP_COMPANY_MAPPING
				SET C4012_VOID_FL = 'Y',
					C4012_UPDATED_DATE = CURRENT_DATE,
					C4012_UPDATED_BY = p_userid
			  WHERE C4010_GROUP_ID = p_group_id
				AND C1900_COMPANY_ID = NVL(p_company_id,C1900_COMPANY_ID)
				--When the Company ID is not passed , we will void all mappings the Group is associated with.
				AND C4012_VOID_FL IS NULL;
	
	END gm_void_group_company_map;
	
	/********************************************************
 	 * Description :This procedure will Sync all the Groups which belongs to a System
 	 * to all the Companies ,where the System was already Released to.
	 * Author : Rajeshwaran
	 *********************************************************/
	PROCEDURE gm_sync_group_to_all_company (
		p_group_id 		IN 	T4010_GROUP.C4010_GROUP_ID%TYPE,
		p_system_id 	IN 	 T207_SET_MASTER.C207_SET_ID%TYPE,		
		p_map_type		IN	 T4012_GROUP_COMPANY_MAPPING.C901_GROUP_COM_MAP_TYP%TYPE,
		p_userid        IN   T4010_GROUP.C4010_LAST_UPDATED_BY%TYPE
	)
	AS
		cur_system_company  	TYPES.CURSOR_TYPE;
		v_comp_id				T1900_COMPANY.C1900_COMPANY_ID%TYPE;
		v_comp_nm				T1900_COMPANY.C1900_COMPANY_NAME%TYPE;
		v_status				VARCHAR2(100);
		v_Released_by			VARCHAR2(100);
		v_released_dt 			t2080_set_company_mapping.c2080_last_updated_date%TYPE;
		
	BEGIN
		gm_pkg_pd_system_rpt.gm_fch_system_comp_map(p_system_id, cur_system_company);
		
		LOOP
        FETCH cur_system_company
        INTO v_comp_id,v_comp_nm,v_status,v_Released_by,v_released_dt;
			
        	 gm_pkg_pd_group_txn.gm_sync_group_to_company(p_group_id,p_system_id, v_comp_id, p_map_type,p_userid);
        
        EXIT WHEN cur_system_company%NOTFOUND;	 
        END LOOP;
        
		
	END gm_sync_group_to_all_company;
	/********************************************************
 	 * Description :This procedure will Sync all the Groups which belongs to a System
 	 * to all the Companies ,where the System was already Released to.
	 * Author : Rajeshwaran
	 *********************************************************/
	PROCEDURE gm_sync_group_to_company (
		p_group_id 		IN 	T4010_GROUP.C4010_GROUP_ID%TYPE,
		p_system_id 	IN 	T207_SET_MASTER.C207_SET_ID%TYPE,
		p_company_id	IN   T1900_COMPANY.C1900_COMPANY_ID%TYPE,		
		p_map_type		IN	 T4012_GROUP_COMPANY_MAPPING.C901_GROUP_COM_MAP_TYP%TYPE,
		p_userid        IN   T4010_GROUP.C4010_LAST_UPDATED_BY%TYPE
	)
	AS
	cur_group_det  	TYPES.CURSOR_TYPE;
	v_group_id		T4010_GROUP.C4010_GROUP_ID%TYPE;
	group_nm		T4010_GROUP.C4010_GROUP_NM%TYPE;
	v_group_desc	T4010_GROUP.C4010_GROUP_DESC%TYPE;
	v_group_d_desc	T4010_GROUP.C4010_GROUP_DTL_DESC%TYPE;
	v_group_pubdt	T4010_GROUP.C4010_GROUP_PUBLISH_DATE%TYPE;
	v_group_pubfl	T4010_GROUP.C4010_PUBLISH_FL%TYPE;
	v_group_type	T4010_GROUP.C901_GROUP_TYPE%TYPE;
	v_group_prc_type	T4010_GROUP.C901_PRICED_TYPE%TYPE;
	
	BEGIN
		gm_pkg_pd_group_info.gm_fch_groups_by_system(p_system_id,p_group_id,cur_group_det);
		
		 
		LOOP
        FETCH cur_group_det
        INTO v_group_id,group_nm,v_group_desc,v_group_d_desc,
        	 v_group_pubdt,v_group_pubfl,v_group_type,v_group_prc_type;
			
        	 gm_pkg_pd_group_txn.gm_sav_system_group_tosync(v_group_id, p_system_id, p_company_id,p_map_type, p_userid);
        
        EXIT WHEN cur_group_det%NOTFOUND;	 
        END LOOP;
        
	END gm_sync_group_to_company;

	/********************************************************
 	 * Description :This procedure will Sync all the Groups which belongs to a System
 	 * to all the Companies ,where the System was already Released to.
	 * Author : Rajeshwaran
	 *********************************************************/

	PROCEDURE gm_sav_system_group_tosync (
		p_group_id 		IN 	T4010_GROUP.C4010_GROUP_ID%TYPE,
		p_system_id 	IN 	T207_SET_MASTER.C207_SET_ID%TYPE,
		p_company_id	IN   T1900_COMPANY.C1900_COMPANY_ID%TYPE,	
		p_map_type		IN	 T4012_GROUP_COMPANY_MAPPING.C901_GROUP_COM_MAP_TYP%TYPE,
		p_userid        IN   T4010_GROUP.C4010_LAST_UPDATED_BY%TYPE
	)
	AS
		v_count NUMBER;
	BEGIN
	
	SELECT COUNT(1) 
	INTO	v_count
	FROM	T4012_GROUP_COMPANY_MAPPING
	WHERE	C4010_GROUP_ID = p_group_id
	AND		C1900_COMPANY_ID = p_company_id ;
	
	IF v_count = 0
	THEN
		INSERT INTO T4012_GROUP_COMPANY_MAPPING(C4012_GRP_COM_MAP_ID,C1900_COMPANY_ID,C4010_GROUP_ID,C901_GROUP_COM_MAP_TYP
				,C4012_UPDATED_BY,C4012_UPDATED_DATE)
		VALUES (S4012_GROUP_COMPANY_MAPPING.NEXTVAL,p_company_id,p_group_id,p_map_type,p_userid,CURRENT_DATE);
	
	ELSIF v_count = 1
	THEN
		UPDATE 	T4012_GROUP_COMPANY_MAPPING
		SET		C4012_VOID_FL = '',
				C4012_UPDATED_BY = p_userid,
				C4012_UPDATED_DATE = CURRENT_DATE
		WHERE
				C4010_GROUP_ID = p_group_id
		AND		C1900_COMPANY_ID = p_company_id ;
		
	END IF;
	
	END gm_sav_system_group_tosync;
	
	
	
	/********************************************************
 	 * Description :This procedure will remove all the Groups which belongs to a System
 	 * to all the Companies ,where the System was already Released to.
	 * Author : Rajeshwaran
	 *********************************************************/
	PROCEDURE gm_remove_group_to_company (
		p_system_id 	IN 	T207_SET_MASTER.C207_SET_ID%TYPE,
		p_company_id	IN   T1900_COMPANY.C1900_COMPANY_ID%TYPE,
		p_userid        IN   T4010_GROUP.C4010_LAST_UPDATED_BY%TYPE
	)
	AS
	cur_group_det  	TYPES.CURSOR_TYPE;
	v_group_id		T4010_GROUP.C4010_GROUP_ID%TYPE;
	group_nm		T4010_GROUP.C4010_GROUP_NM%TYPE;
	v_group_desc	T4010_GROUP.C4010_GROUP_DESC%TYPE;
	v_group_d_desc	T4010_GROUP.C4010_GROUP_DTL_DESC%TYPE;
	v_group_pubdt	T4010_GROUP.C4010_GROUP_PUBLISH_DATE%TYPE;
	v_group_pubfl	T4010_GROUP.C4010_PUBLISH_FL%TYPE;
	v_group_type	T4010_GROUP.C901_GROUP_TYPE%TYPE;
	v_group_prc_type	T4010_GROUP.C901_PRICED_TYPE%TYPE;
	BEGIN
		gm_pkg_pd_group_info.gm_fch_groups_by_system(p_system_id,'',cur_group_det);
		
		 
		LOOP
        FETCH cur_group_det
        INTO v_group_id,group_nm,v_group_desc,v_group_d_desc,
        	 v_group_pubdt,v_group_pubfl,v_group_type,v_group_prc_type;
			
        	 gm_pkg_pd_group_txn.gm_void_group_company_map(v_group_id, p_company_id, p_userid);
       
        EXIT WHEN cur_group_det%NOTFOUND;	 
        END LOOP;
        
	END gm_remove_group_to_company;
END gm_pkg_pd_group_txn;

/

