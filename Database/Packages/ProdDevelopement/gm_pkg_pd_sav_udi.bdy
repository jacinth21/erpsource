/* Formatted on 2010/03/10 09:56 (Formatter Plus v4.8.0) */
-- @"C:\database\Packages\ProdDevelopement\gm_pkg_pd_sav_udi.bdy"
CREATE OR REPLACE
PACKAGE BODY gm_pkg_pd_sav_udi
IS
    --
    /*******************************************************
    * Description : This procedure will map the part number to its DI in the t2060_di_part_mapping
    * Author   : Mani
    *******************************************************/
    --
PROCEDURE gm_sav_di_part_map (
        p_partnumber IN t205_part_number.c205_part_number_id%TYPE,
        p_issue_agency_id IN t1600_issue_agency.c1600_issuing_agency_id%TYPE,
        p_user_id    IN t205_part_number.c205_last_updated_by%TYPE)
AS
    v_di_part_mapping_id t2060_di_part_mapping.c2060_di_number%TYPE;
    v_cnt NUMBER;
    --
    v_vendor_design t205d_part_attribute.c205d_attribute_value%TYPE;
    --
    v_di_number t2060_di_part_mapping.c2060_di_number%TYPE;
    v_udi_attr_cnt_wip_id t2071_udi_attribute_count_wip.c2071_udi_attrb_count_wip_id%TYPE;
BEGIN
    --
     SELECT COUNT (1)
       INTO v_cnt
       FROM t2060_di_part_mapping
      WHERE c205_part_number_id = p_partnumber;
      --
      v_vendor_design := TRIM(GET_PART_ATTRIBUTE_VALUE (p_partnumber, '4000517'));
      -- 103420 - Yes
    IF (v_cnt                   <> 0 OR v_vendor_design = '103420') THEN
        -- Throw validation message
        RETURN;
    END IF;
    --
    BEGIN
     SELECT MIN (c2060_di_part_mapping_id)
       INTO v_di_part_mapping_id
       FROM t2060_di_part_mapping
      WHERE c205_part_number_id IS NULL
      	AND c1600_issuing_agency_id = p_issue_agency_id
        AND c901_status          = 103391; -- Available
        --
     SELECT c2060_di_part_mapping_id, c2060_di_number
	   INTO v_di_part_mapping_id, v_di_number
	   FROM t2060_di_part_mapping
	  WHERE c2060_di_part_mapping_id = v_di_part_mapping_id FOR UPDATE;
    EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			v_di_part_mapping_id := NULL;
		END;
    --
    IF	v_di_part_mapping_id IS NULL
    THEN
    	raise_application_error ('-20631', ''); -- DI Number is not available
    END IF;
    -- 103392	In-Use
     UPDATE t2060_di_part_mapping
    SET c205_part_number_id          = p_partnumber, c901_status = 103392, c2060_last_updated_by = p_user_id
      , c2060_last_updated_date      = SYSDATE
      WHERE c2060_di_part_mapping_id = v_di_part_mapping_id;
     -- add UDI attribute count wip (t2071)
	 gm_pkg_pd_udi_submission_trans.gm_sav_attribute_count_wip (v_udi_attr_cnt_wip_id, p_partnumber, v_di_number, NULL, NULL
	 , NULL, NULL, 'N', NULL, NULL, p_user_id) ;
	 -- add UDI DI status (t2072) 104821 - Wip
	 gm_pkg_pd_udi_submission_trans.gm_sav_gudid_stat_data (v_di_number, 104821, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
	 NULL, NULL, NULL, NULL, NULL, p_user_id, SYSDATE, NULL, NULL, NULL) ;
	 --
END gm_sav_di_part_map;
/*******************************************************
* Description : Procedure to use to save the DI part mapping Log details
* Author   : Mani
*******************************************************/
--
PROCEDURE gm_sav_di_part_map_log (
        p_di_part_map_id IN t2060_di_part_map_log.C2060_DI_PART_MAPPING_ID%TYPE,
        p_di_number      IN t2060_di_part_map_log.C2060_DI_NUMBER%TYPE,
        p_part_number    IN t2060_di_part_map_log.C205_PART_NUMBER_ID%TYPE,
        p_status         IN t2060_di_part_map_log.C901_STATUS%TYPE,
        p_set_uuid       IN t2060_di_part_map_log.c2060_set_uuid%TYPE,
        p_model_uuid     IN t2060_di_part_map_log.c2060_model_uuid%TYPE,
        p_user_id        IN t2060_di_part_map_log.c2060_last_updated_by%TYPE,
        p_last_upd_dt    IN t2060_di_part_map_log.c2060_last_updated_date%TYPE)
AS
BEGIN
    --
     INSERT
       INTO t2060_di_part_map_log
        (
            C2060_DI_PART_MAP_LOG_ID, C2060_DI_PART_MAPPING_ID, C2060_DI_NUMBER
          , C205_PART_NUMBER_ID, C901_STATUS, c2060_set_uuid
          , c2060_model_uuid, c2060_last_updated_by, c2060_last_updated_date
        )
        VALUES
        (
            s2060_di_part_map_log.nextval, p_di_part_map_id, p_di_number
          , p_part_number, p_status, p_set_uuid
          , p_model_uuid, p_user_id, p_last_upd_dt
        ) ;
END gm_sav_di_part_map_log;
--
/*******************************************************
* Description : Procedure to use to save UDI parameters to attribute table
* Author   : Mani
*******************************************************/
/*
This procedure will save the below UDI parameters which are based on logic
1. HCTP
2. For Single Use
3. Device Packaged as Sterile
4. Expiration Date
5. Subject to DM but exempt
*/
--
PROCEDURE gm_sav_qa_udi_param
    (
        p_partnumber IN t205_part_number.c205_part_number_id%TYPE,
        p_user_id    IN t205_part_number.c205_last_updated_by%TYPE
    )
AS
    v_hctp VARCHAR2(40);
    v_single_use              VARCHAR2 (40) ;
    v_device_packaged_sterile VARCHAR2 (40) ;
    v_expiration_date         VARCHAR2 (40) ;
    v_di_number t2060_di_part_mapping.c2060_di_number%TYPE;
    v_udi_etch_req t205d_part_attribute.c205d_attribute_value%TYPE;
    v_di_etch_req t205d_part_attribute.c205d_attribute_value%TYPE;
    v_mri_safety_info t205d_part_attribute.c205d_attribute_value%TYPE;
    v_final_val VARCHAR2 (40) := NULL;
    --
    v_attr_id t205d_part_attribute.c2051_part_attribute_id%TYPE;
    v_parent_cnt NUMBER;
BEGIN
	--
	--100845	Tissue
	--4050	Implants
	--4030	Sterile
	--103381	UDI Etch Required
	--103382	DI Only Etch Required
	--103341	MRI Safety Information
	--
	--20375	Yes
	--20376	No
	BEGIN
     SELECT DECODE (t205.c205_product_material, '100845', '20375', '20376'), DECODE (t205.c205_product_family, '4050', '20375',
	    DECODE (t205.c205_product_class, '4030', '20375', '20376')), DECODE (t205.c205_product_class, '4030', '20375', '20376'),
	    DECODE (t205.c205_product_class, '4030', '20375', '20376'), gm_pkg_pd_rpt_udi.get_part_di_number (
	    t205.c205_part_number_id), TRIM(GET_PART_ATTRIBUTE_VALUE (t205.C205_PART_NUMBER_ID, '103381')), TRIM(GET_PART_ATTRIBUTE_VALUE
	    (C205_PART_NUMBER_ID, '103382')), TRIM(GET_PART_ATTRIBUTE_VALUE (t205.C205_PART_NUMBER_ID, '103341'))
	   INTO v_hctp, v_single_use, v_device_packaged_sterile
	  , v_expiration_date, v_di_number, v_udi_etch_req
	  , v_di_etch_req, v_mri_safety_info
	   FROM t205_part_number t205
	  WHERE t205.c205_part_number_id = p_partnumber;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			v_hctp := '20376';
			v_single_use := '20376';
			v_device_packaged_sterile := '20376';
	  		v_expiration_date := '20376';
	  		v_di_number := NULL;
	  		v_udi_etch_req := NULL;
	  		v_di_etch_req := NULL;
	  		v_mri_safety_info := NULL;
	END;  
    --
    -- 1 HCTP
    gm_pkg_pd_partnumber.gm_sav_part_attribute (p_partnumber, '103345', v_hctp, p_user_id, v_attr_id) ;
    -- 2 For Single Use
    gm_pkg_pd_partnumber.gm_sav_part_attribute (p_partnumber, '103346', v_single_use, p_user_id, v_attr_id) ;
    -- 3 Device Packaged as Sterile
    gm_pkg_pd_partnumber.gm_sav_part_attribute (p_partnumber, '103348', v_device_packaged_sterile, p_user_id, v_attr_id
    ) ;
    -- 4 Expiration Date
    --gm_pkg_pd_partnumber.gm_sav_part_attribute (p_partnumber, '103347', v_expiration_date, p_user_id, v_attr_id) ;
    --
    -- 103364 - UDI Etch Required - No
    -- 103367 - DI Only Etch Required - No
    IF (v_single_use = '20376' AND v_di_number IS NOT NULL AND (v_udi_etch_req = '103364' AND v_di_etch_req = '103367')) THEN
        v_final_val := '20375';
    END IF;
    --
    -- 5 Subject to DM Exempt
    gm_pkg_pd_partnumber.gm_sav_part_attribute (p_partnumber, '103342', NVL(v_final_val, 20376), p_user_id, v_attr_id) ;
    --
    IF (v_mri_safety_info IS NULL)
    THEN
	    -- 6 MRI Safety Information (NVL - to set - Labeling does not contain MRI Safety information)
	    gm_pkg_pd_partnumber.gm_sav_part_attribute (p_partnumber, '103341', NVL(v_mri_safety_info,103390), p_user_id, v_attr_id) ;
	END IF;    
    --
    -- to update the DM DI parameter values.
    --gm_pkg_pd_sav_udi.gm_sav_qa_dmdi_param (p_partnumber, p_user_id);
    -- to update the Parent DM DI parameter values.
    /*SELECT COUNT (1)
         INTO v_parent_cnt
         FROM t205a_part_mapping T205A
         WHERE T205A.C901_TYPE                                   = 30031 -- Sub Component
          START WITH T205A.C205_FROM_PART_NUMBER_ID             = p_partnumber
          CONNECT BY NOCYCLE PRIOR T205A.C205_TO_PART_NUMBER_ID = T205A.C205_FROM_PART_NUMBER_ID
          AND T205A.C205A_VOID_FL IS NULL;  ---  added new column c205a_void_fl in t205a used to PMT-50777 - Create BOM and sub-component Mapping
      IF v_parent_cnt <> 0
      THEN    
      gm_pkg_pd_sav_udi.gm_sav_qa_parent_dmdi_param (p_partnumber, p_user_id);
    END IF;*/     
    gm_pkg_pd_sav_udi.gm_sav_qa_parent_dmdi_param (p_partnumber, p_user_id);   
	gm_pkg_pd_sav_udi.gm_sav_pd_part_attribute (p_partnumber, p_user_id, 'Y');
	
    --
END gm_sav_qa_udi_param;
/*
This procedure will save the below DMDI parameters which are based on logic
1. DM DI is not Primary DI
2. DM DI Number
*/
PROCEDURE gm_sav_qa_dmdi_param (
        p_partnumber IN t205_part_number.c205_part_number_id%TYPE,
        p_user_id    IN t205_part_number.c205_last_updated_by%TYPE)
AS
    v_di_number t2060_di_part_mapping.c2060_di_number%TYPE;
    v_dm_di_primary_di VARCHAR2 (40) ;
    v_dm_di_number     VARCHAR2 (40) ;
    --
    v_attr_id t205d_part_attribute.c2051_part_attribute_id%TYPE;
BEGIN
	--
	--20375	Yes
	--20376	No
	--
	BEGIN
     SELECT t2060.c2060_di_number, '20376' --gm_pkg_pd_rpt_udi.get_dm_di_primary_di (t205.c205_part_number_id)
       INTO v_di_number, v_dm_di_primary_di
       FROM t2060_di_part_mapping t2060, t205_part_number t205
      WHERE t205.c205_part_number_id  = t2060.c205_part_number_id (+)
        AND t205.c205_part_number_id = p_partnumber;
     EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			v_di_number := NULL;
			v_dm_di_primary_di := '20376';
		END;
    --
    -- to call part attribute save procedure.
    -- 1 DM DI is not Primary DI
    gm_pkg_pd_partnumber.gm_sav_part_attribute (p_partnumber, '103343', v_dm_di_primary_di, p_user_id, v_attr_id) ;
    -- 2 DM DI Number
    v_dm_di_number := NULL;
    --
    IF v_dm_di_primary_di = '20375'
    THEN
    	-- to get the DM DI number
    	BEGIN
    	SELECT gm_pkg_pd_rpt_udi.get_dm_di_number (p_partnumber) INTO v_dm_di_number FROM DUAL;
    	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			v_dm_di_number :=NULL;
		END;
    END IF;
    --
    gm_pkg_pd_partnumber.gm_sav_part_attribute (p_partnumber, '103344', v_dm_di_number, p_user_id, v_attr_id) ;
    --
END gm_sav_qa_dmdi_param;
/*
This procedure will save the below parent DMDI parameters which are based on logic
1. DM DI is not Primary DI
2. DM DI Number
*/
PROCEDURE gm_sav_qa_parent_dmdi_param (
        p_partnumber IN t205_part_number.c205_part_number_id%TYPE,
        p_user_id    IN t205_part_number.c205_last_updated_by%TYPE)
AS
    v_parent_partnum t205_part_number.c205_part_number_id%TYPE;
    v_level NUMBER;
    v_di_number t2060_di_part_mapping.c2060_di_number%TYPE;
    --
    CURSOR parent_sub_comp_cur
    IS
        
	SELECT T205A.C205_FROM_PART_NUMBER_ID PNUM,  LEVEL lvl
	, gm_pkg_pd_rpt_udi.get_part_di_number (T205A.C205_FROM_PART_NUMBER_ID) di_number
	FROM T205A_PART_MAPPING T205A,
  	T205_PART_NUMBER T205
	WHERE T205A.C205_FROM_PART_NUMBER_ID                      = T205.C205_PART_NUMBER_ID
	AND t205a.c901_type                                       = 30031 -- Sub-Component
  	START WITH T205A.C205_TO_PART_NUMBER_ID                 = p_partnumber
  	CONNECT BY NOCYCLE PRIOR T205.C205_PART_NUMBER_ID = T205A.C205_TO_PART_NUMBER_ID
  	AND T205A.C205A_VOID_FL IS NULL   ---  added new column c205a_void_fl in t205a used to PMT-50777 - Create BOM and sub-component Mapping
	UNION ALL
	SELECT p_partnumber, 0,gm_pkg_pd_rpt_udi.get_part_di_number (p_partnumber) di_number FROM dual ORDER BY LVL;

BEGIN
    --
    FOR parent_parts IN parent_sub_comp_cur
    LOOP
        v_parent_partnum := parent_parts.PNUM;
        v_di_number := parent_parts.di_number;
        --
        IF (v_di_number IS NOT NULL)
        THEN
        	gm_pkg_pd_sav_udi.gm_sav_qa_dmdi_param (v_parent_partnum, p_user_id) ;
        END IF;	
    END LOOP;
END gm_sav_qa_parent_dmdi_param;

/***********************************
*This procedure will save the UDI bulk packaging values (main)
************************************/
PROCEDURE gm_sav_sub_com_bulk_parameter (
        p_partnumber IN t205_part_number.c205_part_number_id%TYPE,
        p_user_id    IN t205_part_number.c205_last_updated_by%TYPE)
AS
    CURSOR subcomp_part_cur
    IS
         SELECT T205A.C205_TO_PART_NUMBER_ID PNUM, LEVEL lvl
           FROM T205A_PART_MAPPING T205A, T205_PART_NUMBER T205
          WHERE T205A.C205_FROM_PART_NUMBER_ID                    = T205.C205_PART_NUMBER_ID
          	AND T205A.C901_TYPE                                   = 30031 -- Sub-Component
            START WITH T205A.C205_FROM_PART_NUMBER_ID             = p_partnumber
            CONNECT BY nocycle prior T205A.C205_TO_PART_NUMBER_ID = T205.C205_PART_NUMBER_ID
            AND T205A.C205A_VOID_FL IS NULL   ---  added new column c205a_void_fl in t205a used to PMT-50777 - Create BOM and sub-component Mapping
        UNION ALL
        SELECT T205A.C205_FROM_PART_NUMBER_ID PNUM, LEVEL lvl
           FROM T205A_PART_MAPPING T205A, T205_PART_NUMBER T205
          WHERE T205A.C205_FROM_PART_NUMBER_ID                      = T205.C205_PART_NUMBER_ID
          	AND T205A.C901_TYPE                                     = 30031 --Sub-Component
            START WITH T205A.C205_TO_PART_NUMBER_ID                 = p_partnumber
            CONNECT BY NOCYCLE PRIOR T205.C205_PART_NUMBER_ID = T205A.C205_TO_PART_NUMBER_ID
                                 AND T205A.C205A_VOID_FL IS NULL   ---  added new column c205a_void_fl in t205a used to PMT-50777 - Create BOM and sub-component Mapping
  		UNION ALL
     		SELECT p_partnumber pnum, 0 lvl FROM dual ORDER BY lvl;
BEGIN
    FOR subcomp IN subcomp_part_cur
    LOOP
        gm_pkg_pd_sav_udi.gm_sav_qa_udi_bulk_pkg_param (subcomp.PNUM, p_user_id) ;
    END LOOP;
END gm_sav_sub_com_bulk_parameter;
/*
This procedure will save the UDI Default parameter details
*/
PROCEDURE gm_sav_qa_udi_bulk_pkg_param (
        p_partnumber IN t205_part_number.c205_part_number_id%TYPE,
        p_user_id    IN t205_part_number.c205_last_updated_by%TYPE)
AS
    v_bulk_pack        NUMBER ;
    v_device_cnt      NUMBER ;
    v_unit_of_use_di  VARCHAR2 (40) ;
    v_qty_per_package VARCHAR2 (40) ;
    v_contains_di_pkg VARCHAR2 (40) ;
    v_pkg_di_number   VARCHAR2 (40) ;
    v_attr_id t205d_part_attribute.c2051_part_attribute_id%TYPE;
    v_company_id t901_code_lookup.c901_code_id%TYPE;
    v_company_name_cnt NUMBER;
    --
    v_di_number t2060_di_part_mapping.c2060_di_number%TYPE;
    v_part_di_num	t2060_di_part_mapping.c2060_di_number%TYPE;
    --
    CURSOR di_subcomp_part_cur
    IS
         SELECT T205A.C205_TO_PART_NUMBER_ID PNUM, gm_pkg_pd_rpt_udi.get_part_di_number (T205A.C205_TO_PART_NUMBER_ID)
            dinumber, T205A.C205A_QTY qty, LEVEL lvl
          , CONNECT_BY_ISLEAF leafnode
           FROM T205A_PART_MAPPING T205A, T205_PART_NUMBER T205
          WHERE T205A.C205_FROM_PART_NUMBER_ID                    = T205.C205_PART_NUMBER_ID
          	AND T205A.C901_TYPE                                   = 30031 -- Sub-Component
            START WITH T205A.C205_FROM_PART_NUMBER_ID             = p_partnumber
            CONNECT BY nocycle prior T205A.C205_TO_PART_NUMBER_ID = T205.C205_PART_NUMBER_ID
            AND T205A.C205A_VOID_FL IS NULL   ---  added new column c205a_void_fl in t205a used to PMT-50777 - Create BOM and sub-component Mapping
       ORDER BY lvl;
    --
    CURSOR parent_sub_comp_cur
    IS
         SELECT T205A.C205_FROM_PART_NUMBER_ID PNUM, LEVEL lvl, gm_pkg_pd_rpt_udi.get_part_di_number (
            T205A.C205_FROM_PART_NUMBER_ID) dinumber
           FROM T205A_PART_MAPPING T205A, T205_PART_NUMBER T205
          WHERE T205A.C205_FROM_PART_NUMBER_ID                      = T205.C205_PART_NUMBER_ID
          	AND T205A.C901_TYPE                                     = 30031 -- Sub-Component
            START WITH T205A.C205_TO_PART_NUMBER_ID                 = p_partnumber
            CONNECT BY NOCYCLE PRIOR T205.C205_PART_NUMBER_ID = T205A.C205_TO_PART_NUMBER_ID
            AND T205A.C205A_VOID_FL IS NULL    ---  added new column c205a_void_fl in t205a used to PMT-50777 - Create BOM and sub-component Mapping
            AND LEVEL                                         = 1; -- only check immediate parent
BEGIN
     SELECT COUNT (1)
       INTO v_bulk_pack
       FROM t205d_part_attribute
      WHERE C901_ATTRIBUTE_TYPE   = 4000540 -- 4000540 Bulk Packaging
        AND c205_part_number_id   = p_partnumber
        AND c205d_attribute_value = 20375 -- yes
        AND C205D_VOID_FL        IS NULL;
    --
		v_part_di_num := gm_pkg_pd_rpt_udi.get_part_di_number (p_partnumber);
    	-- to update the DUNS information details
    	SELECT COUNT (1)
       		INTO v_company_name_cnt
       FROM t205d_part_attribute
      	WHERE C901_ATTRIBUTE_TYPE   = 103781 -- 103781 Override Company Name 
	        AND c205_part_number_id   = p_partnumber
	        AND C205D_VOID_FL        IS NULL;
	    -- for first time only to get the part number company and update DUNS infor.
	    IF v_company_name_cnt = 0
	    THEN    
			SELECT get_companyid_from_pnum (p_partnumber) INTO v_company_id FROM dual;
			gm_pkg_pd_sav_udi.gm_sav_qa_udi_duns_information (p_partnumber, v_company_id, p_user_id);
		END IF;	-- end of v_company_name_cnt
		--
		IF (v_part_di_num IS NOT NULL)
		THEN
          IF (v_bulk_pack = 1) THEN
          SELECT NVL (ROUND (EXP (SUM (LN (qty))), 0), 1) product
           INTO v_device_cnt
           FROM
            (
                 SELECT T205A.C205_TO_PART_NUMBER_ID PNUM, T205A.C205A_QTY qty, LEVEL lvl
                  , CONNECT_BY_ISLEAF leafnode
                   FROM T205A_PART_MAPPING T205A, T205_PART_NUMBER T205
                  WHERE T205A.C205_FROM_PART_NUMBER_ID                    = T205.C205_PART_NUMBER_ID
                  	AND T205A.C901_TYPE                                   = 30031 -- Sub-Component
                  	AND gm_pkg_pd_rpt_udi.get_part_di_number (T205A.C205_TO_PART_NUMBER_ID) IS NOT NULL
                    START WITH T205A.C205_FROM_PART_NUMBER_ID             = p_partnumber
                    CONNECT BY NOCYCLE PRIOR T205A.C205_TO_PART_NUMBER_ID = T205.C205_PART_NUMBER_ID
                                         AND T205A.C205A_VOID_FL IS NULL   ---  added new column c205a_void_fl in t205a used to PMT-50777 - Create BOM and sub-component Mapping
               ORDER BY PNUM
            ) ;
        --IF v_device_cnt <> 1 THEN
            -- unit of use DI, Qty per package, Contains DI package
            FOR di_sub_comp IN di_subcomp_part_cur
            LOOP
                v_di_number     := di_sub_comp.dinumber;
                IF (v_di_number IS NOT NULL) THEN
                    -- Qty per package, Contains DI package (Immediate Sub comp)
                    IF (v_qty_per_package  IS NULL AND di_sub_comp.lvl = 1) THEN
                        v_qty_per_package := di_sub_comp.qty;
                        v_contains_di_pkg := v_di_number;
                    END IF;
                    -- unit of use DI
                    v_unit_of_use_di := di_sub_comp.dinumber;
                END IF;
            END LOOP;
            --
        --END IF; -- end of v_device_cnt
        -- Package DI number
        FOR parent_sub IN parent_sub_comp_cur
        LOOP
            IF (parent_sub.dinumber IS NOT NULL) THEN
                v_pkg_di_number     := parent_sub.dinumber;
                EXIT;
            END IF;
        END LOOP;
	--
	ELSE
		v_device_cnt := 1;
    END IF; -- end of v_bulk_pack    
    END IF; -- end v_part_di_num    
    --103785 � Device Count
    gm_pkg_pd_partnumber.gm_sav_part_attribute (p_partnumber, '103785', v_device_cnt, p_user_id, v_attr_id) ;
    --4000542 Unit of Use DI Number
    gm_pkg_pd_partnumber.gm_sav_part_attribute (p_partnumber, '4000542', v_unit_of_use_di, p_user_id, v_attr_id) ;
    --4000543 Package DI Number
    gm_pkg_pd_partnumber.gm_sav_part_attribute (p_partnumber, '4000543', v_pkg_di_number, p_user_id, v_attr_id) ;
    --4000544 Quantity per Package
    gm_pkg_pd_partnumber.gm_sav_part_attribute (p_partnumber, '4000544', v_qty_per_package, p_user_id, v_attr_id) ;
    --4000545 Contains DI Package
    gm_pkg_pd_partnumber.gm_sav_part_attribute (p_partnumber, '4000545', v_contains_di_pkg, p_user_id, v_attr_id) ;
END gm_sav_qa_udi_bulk_pkg_param;
/*
This procedure will save the UDI DUNS information details
*/
PROCEDURE gm_sav_qa_udi_duns_information (
        p_partnumber IN t205_part_number.c205_part_number_id%TYPE,
        p_company_id IN t901_code_lookup.c901_code_id%TYPE,
        p_user_id    IN t205_part_number.c205_last_updated_by%TYPE)
AS
    v_duns_number    VARCHAR2 (40) ;
    v_customer_phone VARCHAR2 (40) ;
    v_customer_email VARCHAR2 (40) ;
    v_attr_id t205d_part_attribute.c2051_part_attribute_id%TYPE;
BEGIN
    --
    IF (p_company_id IS NOT NULL) THEN
         SELECT get_rule_value (p_company_id, 'DUNS_NUMBER'), get_rule_value (p_company_id, 'COMPANY_PHONE'),
            get_rule_value (p_company_id, 'COMPANY_EMAIL')
           INTO v_duns_number, v_customer_phone, v_customer_email
           FROM dual;
    END IF;
    --
    --103781 Override Company Name
    gm_pkg_pd_partnumber.gm_sav_part_attribute (p_partnumber, '103781', p_company_id, p_user_id, v_attr_id) ;
    --4000546 Labeler DUNS Number
    gm_pkg_pd_partnumber.gm_sav_part_attribute (p_partnumber, '4000546', v_duns_number, p_user_id, v_attr_id) ;
    --4000547 Customer Contact Phone
    gm_pkg_pd_partnumber.gm_sav_part_attribute (p_partnumber, '4000547', v_customer_phone, p_user_id, v_attr_id) ;
    --4000548 Customer Contact Email
    gm_pkg_pd_partnumber.gm_sav_part_attribute (p_partnumber, '4000548', v_customer_email, p_user_id, v_attr_id) ;
END gm_sav_qa_udi_duns_information;

/*********************************************
This procedure used to save the Issue agency configuration details (Main procedure)
**********************************************/
PROCEDURE gm_sav_issue_agency_dtls (
        p_issue_agency_id   IN t1600_issue_agency.c1600_issuing_agency_id%TYPE,
        p_issue_config_name IN T1610_ISSUE_AGENCY_CONFIG.c1610_issue_agency_config_nm%TYPE,
        p_input_str         IN VARCHAR2,
        p_user_id           IN T1610_ISSUE_AGENCY_CONFIG.c1610_last_updated_by%TYPE,
        p_issue_agn_con_id  IN OUT VARCHAR2)
AS
    v_cnt       NUMBER;
    v_strlen    NUMBER          := NVL (LENGTH (p_input_str), 0) ;
    v_string    VARCHAR2 (4000) := p_input_str;
    v_substring VARCHAR2 (4000) ;
    v_issue_agn_iden_id T1611_ISSUE_AGENCY_CONFIG_DTL.c1601_issue_ag_identifier_id%TYPE;
    v_issue_agn_con_id T1610_ISSUE_AGENCY_CONFIG.c1610_issue_agency_config_id%TYPE;
    v_issue_agn_nm T1610_ISSUE_AGENCY_CONFIG.c1610_issue_agency_config_nm%TYPE;
    v_issue_short_nm t1600_issue_agency.c1600_agency_short_nm%TYPE;
    v_issue_agn_con_dtls_id T1611_ISSUE_AGENCY_CONFIG_DTL.C1611_ISSUE_AGENCY_CONF_DTL_ID%TYPE;
    v_seq_no NUMBER;
    v_void_fl	VARCHAR2(1);
    --
    CURSOR existing_agency_part_cur IS
    SELECT c205_part_number_id pnum
   FROM t205d_part_attribute
  WHERE c901_attribute_type   = 4000539
    AND c205d_attribute_value = TO_CHAR(v_issue_agn_con_id)
    AND c205d_void_fl        IS NULL;
BEGIN
    v_issue_agn_con_id := p_issue_agn_con_id;
    --
     SELECT c1600_agency_short_nm
       INTO v_issue_short_nm
       FROM t1600_issue_agency
      WHERE c1600_issuing_agency_id = p_issue_agency_id;
    --
    v_issue_agn_nm := v_issue_short_nm || p_issue_config_name;
    -- to save the issue agency config.
    gm_pkg_pd_sav_udi.gm_sav_issue_agency_config (p_issue_agency_id, v_issue_agn_nm, NULL, p_user_id,
    v_issue_agn_con_id) ;
    -- This while loop used to save p_lbl_attr info to part attribute table
    IF v_strlen                      > 0 THEN
        WHILE INSTR (v_string, '|') <> 0
        LOOP
            v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
            v_string    := SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
            --
            v_issue_agn_iden_id     := NULL;
            v_seq_no                := NULL;
            v_issue_agn_con_dtls_id := NULL;
            v_void_fl				:= NULL;
            --
            v_issue_agn_con_dtls_id := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            v_substring             := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
            v_issue_agn_iden_id     := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            v_substring             := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
            v_seq_no                := TO_NUMBER (v_substring) ;
            -- validate the agency ident. id
            IF (v_issue_agn_iden_id = 0)
            THEN
            	v_issue_agn_iden_id := NULL;
            	v_void_fl := 'Y';
            END IF;
            -- to save the issue agency config. details
            gm_pkg_pd_sav_udi.gm_sav_issue_agn_config_dtls (v_issue_agn_con_id, v_issue_agn_iden_id, v_seq_no, v_void_fl,
            p_user_id, v_issue_agn_con_dtls_id) ;
        END LOOP;
    END IF;
    --
    SELECT COUNT (1) INTO v_cnt
   FROM t1611_issue_agency_config_dtl
  WHERE c1610_issue_agency_config_id = v_issue_agn_con_id
    AND c1611_void_fl  IS NULL;
    -- if all records voided then void the issue agency
    IF v_cnt = 0 
    THEN
    	v_void_fl := 'Y';
    	-- to void the Issue Agency config table (T1610_ISSUE_AGENCY_CONFIG)
    	gm_pkg_pd_sav_udi.gm_sav_issue_agency_config (p_issue_agency_id, v_issue_agn_nm, v_void_fl, p_user_id,
    	v_issue_agn_con_id) ;
    END IF;
    --
    FOR agn_part IN existing_agency_part_cur
    LOOP
 		gm_pkg_pd_sav_udi.gm_upd_product_identifiers(agn_part.pnum, p_user_id);   
    END LOOP;
    p_issue_agn_con_id := v_issue_agn_con_id;
END gm_sav_issue_agency_dtls;
/*********************************************
This procedure used to save the Issue agency configuration (t1610)
**********************************************/
PROCEDURE gm_sav_issue_agency_config (
        p_issue_agency_id   IN t1600_issue_agency.c1600_issuing_agency_id%TYPE,
        p_issue_config_name IN T1610_ISSUE_AGENCY_CONFIG.c1610_issue_agency_config_nm%TYPE,
        p_void_fl           IN T1610_ISSUE_AGENCY_CONFIG.C1610_VOID_FL%TYPE,
        p_user_id           IN T1610_ISSUE_AGENCY_CONFIG.c1610_last_updated_by%TYPE,
        p_issue_agn_con_id  IN OUT VARCHAR2)
AS
    v_issue_agn_con_id T1610_ISSUE_AGENCY_CONFIG.c1610_issue_agency_config_id%TYPE;
    v_void_cnt	NUMBER;
BEGIN
    v_issue_agn_con_id := p_issue_agn_con_id;
    -- Void flag check
    SELECT COUNT (1) INTO v_void_cnt
   FROM t1610_issue_agency_config
  WHERE c1610_issue_agency_config_id = v_issue_agn_con_id
    AND c1610_void_fl                = 'Y';
    --
    IF (v_void_cnt = 1)
    THEN
    	raise_application_error ('-20650', ''); -- Issue Agency Configuration already voided
    END IF;
    --
     UPDATE t1610_issue_agency_config
    SET c1600_issuing_agency_id          = p_issue_agency_id, c1610_issue_agency_config_nm = p_issue_config_name, c1610_void_fl
                                         = p_void_fl, c1610_last_updated_by = p_user_id, c1610_last_updated_date = SYSDATE
      WHERE c1610_issue_agency_config_id = v_issue_agn_con_id
        AND c1610_void_fl               IS NULL;
    --
    IF (SQL%ROWCOUNT = 0) THEN
        --
         SELECT s1610_ISSUE_AGENCY_CONFIG.nextval INTO v_issue_agn_con_id FROM DUAL;
        --
         INSERT
           INTO T1610_ISSUE_AGENCY_CONFIG
            (
                c1610_issue_agency_config_id, c1610_issue_agency_config_nm, c1600_issuing_agency_id
            )
            VALUES
            (
                v_issue_agn_con_id, p_issue_config_name, p_issue_agency_id
            ) ;
    END IF;
    --
    p_issue_agn_con_id := v_issue_agn_con_id;
END gm_sav_issue_agency_config;
/*********************************************
This procedure used to save the Issue agency configuration details (t1611)
**********************************************/
PROCEDURE gm_sav_issue_agn_config_dtls
    (
        p_issue_agn_con_id      IN T1610_ISSUE_AGENCY_CONFIG.C1610_ISSUE_AGENCY_CONFIG_ID%TYPE,
        p_issue_agn_ident_id    IN T1611_ISSUE_AGENCY_CONFIG_DTL.C1601_ISSUE_AG_IDENTIFIER_ID%TYPE,
        p_seq_no                IN T1611_ISSUE_AGENCY_CONFIG_DTL.C1611_SEQ_NO%TYPE,
        p_void_fl               IN T1611_ISSUE_AGENCY_CONFIG_DTL.C1611_VOID_FL%TYPE,
        p_user_id               IN T1610_ISSUE_AGENCY_CONFIG.c1610_last_updated_by%TYPE,
        p_issue_agn_con_dtls_id IN OUT VARCHAR2
    )
AS
    v_issue_agn_con_dtls_id T1611_ISSUE_AGENCY_CONFIG_DTL.C1611_ISSUE_AGENCY_CONF_DTL_ID%TYPE;
BEGIN
    v_issue_agn_con_dtls_id := p_issue_agn_con_dtls_id;
    --
     UPDATE T1611_ISSUE_AGENCY_CONFIG_DTL
    SET c1601_issue_ag_identifier_id       = p_issue_agn_ident_id, c1610_issue_agency_config_id = p_issue_agn_con_id,
        c1611_seq_no                       = p_seq_no, c1611_void_fl = p_void_fl, c1611_last_updated_by = p_user_id
      , c1611_last_updated_date            = SYSDATE
      WHERE c1611_issue_agency_conf_dtl_id = v_issue_agn_con_dtls_id
        AND c1611_void_fl                 IS NULL;
    --
    IF (SQL%ROWCOUNT = 0) THEN
        --
         SELECT s1611_ISSUE_AGENCY_CONFIG_DTL.nextval
           INTO v_issue_agn_con_dtls_id
           FROM DUAL;
        --
         INSERT
           INTO T1611_ISSUE_AGENCY_CONFIG_DTL
            (
                c1611_issue_agency_conf_dtl_id, c1601_issue_ag_identifier_id, c1610_issue_agency_config_id
              , C1611_SEQ_NO
            )
            VALUES
            (
                v_issue_agn_con_dtls_id, p_issue_agn_ident_id, p_issue_agn_con_id
              , p_seq_no
            ) ;
    END IF;
    --
    p_issue_agn_con_dtls_id := v_issue_agn_con_dtls_id;
END gm_sav_issue_agn_config_dtls;
	/******************************************************************
	* Description : Procedure to update the default param (Lot/Manf dt/ Exp Dt/ Serial/ Donor Id)
	* Author  :
	*******************************************************************/
PROCEDURE gm_upd_product_identifiers(
        p_partnumber t205_part_number.C205_PART_NUMBER_ID%TYPE,
        p_user_id IN t205_part_number.c205_last_updated_by%TYPE)
AS
    --
    v_issue_agn_conf_id t1610_issue_agency_config.c1610_issue_agency_config_id%TYPE;
    v_attr_id t205d_part_attribute.c2051_part_attribute_id%TYPE;
    v_sterile_part   VARCHAR2 (2) ;
    v_code_id        NUMBER;
    v_lot_val        NUMBER;
    v_manuf_val      NUMBER;
    v_serial_val     NUMBER;
    v_expiration_val NUMBER;
    v_donar_val      NUMBER;
    --
    CURSOR issue_agency_iden_cur
    IS
        SELECT DISTINCT t1602.c901_identifier codeid
           FROM t1611_issue_agency_config_dtl t1611, t1601_issue_agency_identifier t1601, t1602_issue_agency_symbol_dtl
            t1602
          WHERE t1601.c1601_issue_ag_identifier_id = t1611.c1601_issue_ag_identifier_id
            AND t1601.c1601_issue_ag_identifier_id = t1602.c1601_issue_ag_identifier_id
            AND t1611.c1610_issue_agency_config_id = v_issue_agn_conf_id
            AND t1602.c901_identifier             <> 103940 -- DI
            AND t1611.c1611_void_fl               IS NULL
            AND t1602.c1602_void_fl               IS NULL
            AND t1601.c1601_void_fl               IS NULL;
BEGIN
    v_issue_agn_conf_id := TRIM (get_part_attribute_value (p_partnumber, 4000539)) ;
    --
    IF v_issue_agn_conf_id IS NOT NULL THEN
        --
        --103941 MFG date
        --103942 EXP date
        --103943 LOT
        --103944 Serial #
        --103945 Donor ID
        --4000688 Supplemental serial #
        v_lot_val        := 20376;
        v_manuf_val      := 20376;
        v_serial_val     := 20376;
        v_expiration_val := 20376;
        v_donar_val      := 20376;
        --
        FOR issue_agency_iden IN issue_agency_iden_cur
        LOOP
            v_code_id := issue_agency_iden.codeid;
            IF v_code_id     = '103941' THEN
                v_manuf_val := 20375; -- Yes
            ELSIF v_code_id  = '103942' THEN
                 SELECT DECODE (c205_product_class, '4030', 'Y', 'N')
                   INTO v_sterile_part
                   FROM t205_part_number
                  WHERE c205_PART_NUMBER_ID = p_partnumber;
                --
                --IF v_sterile_part     = 'Y' THEN
                    v_expiration_val := 20375; -- Yes
                --END IF;
            ELSIF v_code_id   = '103943' THEN
                v_lot_val    := 20375; -- Yes
            ELSIF (v_code_id  = '103944' OR v_code_id = '4000688') THEN
                v_serial_val := 20375; -- Yes
            ELSIF v_code_id   = '103945' THEN
                v_donar_val  := 20375; -- Yes
            END IF;
        END LOOP;
        --
    ELSE
        v_lot_val        := 20377; -- N/A
        v_manuf_val      := 20377;
        v_serial_val     := 20377;
        v_expiration_val := 20377;
        v_donar_val      := 20377;
    END IF;
    --103782 Lot or Batch Number
    --103783 Manufacturing Date
    --103784 Serial Number
    --103347 Expiration Date
    --103786 Donation Identification Number
    gm_pkg_pd_partnumber.gm_sav_part_attribute (p_partnumber, 103782, v_lot_val, p_user_id, v_attr_id) ;
    gm_pkg_pd_partnumber.gm_sav_part_attribute (p_partnumber, 103783, v_manuf_val, p_user_id, v_attr_id) ;
    gm_pkg_pd_partnumber.gm_sav_part_attribute (p_partnumber, 103784, v_serial_val, p_user_id, v_attr_id) ;
    gm_pkg_pd_partnumber.gm_sav_part_attribute (p_partnumber, 103347, v_expiration_val, p_user_id, v_attr_id) ;
    gm_pkg_pd_partnumber.gm_sav_part_attribute (p_partnumber, 103786, v_donar_val, p_user_id, v_attr_id) ;
END gm_upd_product_identifiers;
/*******************************************************
* Description : This procedure used to split the PD attribute value (main procedrue)
* Author   :
*******************************************************/
PROCEDURE gm_sav_prod_dev_udi_dtls (
        p_inputstr IN CLOB,
        p_userid   IN t205g_pd_part_number.c205g_created_by%TYPE,
        p_out_parts OUT CLOB,
        p_out_err	OUT CLOB)
AS
    CURSOR temp_pd_dtls_cur
    IS
     SELECT temp.c205g_pd_part_number_id pd_id, temp.c205_part_number_id pnum, temp.c202_project_id project_id
  , temp.c205_part_num_desc part_des, temp.c901_has_latex latex, temp.c901_requires_sterilization req_sterile
  , temp.c901_sterilization_method sterile_method, temp.c901_has_more_than_one_Size more_size, temp.c901_size1_type
    size1, temp.c205g_size1_type_text size1_text, temp.c205g_size1_value size1_value
  , temp.c901_size1_uom size1_uom, temp.c901_size2_type size2, temp.c205g_size2_type_text size2_text
  , temp.c205g_size2_value size2_value, temp.c901_size2_uom size2_uom, temp.c901_size3_type size3
  , temp.c205g_size3_type_text size3_text, temp.c205g_size3_value size3_value, temp.c901_size3_uom size3_uom
  , temp.c901_size4_type size4, temp.c205g_size4_type_text size4_text, temp.c205g_size4_value size4_value
  , temp.c901_size4_uom size4_uom, temp.C205G_DHTMLX_ROWID grid_rowid
   FROM my_temp_pd_part_number temp WHERE temp.c205g_void_fl IS NULL;
BEGIN
    --
	gm_pkg_pd_sav_udi.gm_validate_pd_part_number (p_inputstr, p_userid, p_out_err) ;
	--
    IF (p_out_err IS NULL) THEN
        FOR tmp_pd IN temp_pd_dtls_cur
    	LOOP
                -- to save the PD information details
                gm_pkg_pd_sav_udi.gm_sav_pd_part_number (tmp_pd.pd_id, tmp_pd.pnum, tmp_pd.project_id, tmp_pd.part_des, tmp_pd.latex,
                tmp_pd.req_sterile, tmp_pd.sterile_method, tmp_pd.more_size, tmp_pd.size1, tmp_pd.size1_text, tmp_pd.size1_value,
                tmp_pd.size1_uom, tmp_pd.size2, tmp_pd.size2_text, tmp_pd.size2_value, tmp_pd.size2_uom, tmp_pd.size3, tmp_pd.size3_text,
                tmp_pd.size3_value, tmp_pd.size3_uom, tmp_pd.size4, tmp_pd.size4_text, tmp_pd.size4_value, tmp_pd.size4_uom,
                NULL, p_userid) ;
                --
                gm_pkg_pd_sav_udi.gm_sav_pd_part_attribute (tmp_pd.pnum, p_userid, 'Y');
                --
                p_out_parts := p_out_parts || tmp_pd.grid_rowid ||'^' || tmp_pd.pd_id ||'|';
                --
         END LOOP;
         --
         END IF; -- end of v_error_fl
            
END gm_sav_prod_dev_udi_dtls;
/*******************************************************
* Description : This procedure used to save the PD attribute values to t205g
* Author   :
*******************************************************/
PROCEDURE gm_sav_pd_part_number (
		p_pd_id		IN OUT t205g_pd_part_number.c205g_pd_part_number_id%TYPE,
        p_partnumber         IN t205g_pd_part_number.c205_part_number_id%TYPE,
        p_project_id         IN t205g_pd_part_number.c202_project_id%TYPE,
        p_part_desc          IN t205g_pd_part_number.c205_part_num_desc%TYPE,
        p_has_latex          IN t205g_pd_part_number.c901_has_latex%TYPE,
        p_req_sterile        IN t205g_pd_part_number.c901_requires_sterilization%TYPE,
        p_sterile_method     IN t205g_pd_part_number.c901_sterilization_method%TYPE,
        p_dev_more_size      IN t205g_pd_part_number.c901_has_more_than_one_Size%TYPE,
        p_size1_type         IN t205g_pd_part_number.c901_size1_type%TYPE,
        p_size1_text         IN t205g_pd_part_number.c205g_size1_type_text%TYPE,
        p_size1_value        IN t205g_pd_part_number.c205g_size1_value%TYPE,
        p_size1_uom          IN t205g_pd_part_number.c901_size1_uom%TYPE,
        p_size2_type         IN t205g_pd_part_number.c901_size2_type%TYPE,
        p_size2_text         IN t205g_pd_part_number.c205g_size2_type_text%TYPE,
        p_size2_value        IN t205g_pd_part_number.c205g_size2_value%TYPE,
        p_size2_uom          IN t205g_pd_part_number.c901_size2_uom%TYPE,
        p_size3_type         IN t205g_pd_part_number.c901_size3_type%TYPE,
        p_size3_text         IN t205g_pd_part_number.c205g_size3_type_text%TYPE,
        p_size3_value        IN t205g_pd_part_number.c205g_size3_value%TYPE,
        p_size3_uom          IN t205g_pd_part_number.c901_size3_uom%TYPE,
        p_size4_type         IN t205g_pd_part_number.c901_size4_type%TYPE,
        p_size4_text         IN t205g_pd_part_number.c205g_size4_type_text%TYPE,
        p_size4_value        IN t205g_pd_part_number.c205g_size4_value%TYPE,
        p_size4_uom          IN t205g_pd_part_number.c901_size4_uom%TYPE,
        p_part_setup_sync_fl IN t205g_pd_part_number.c205g_part_setup_sync_fl%TYPE,
        p_userid             IN t205g_pd_part_number.c205g_last_updated_by%TYPE)
AS
    v_pd_part_number_id t205g_pd_part_number.c205g_pd_part_number_id%TYPE;
BEGIN
    --
    v_pd_part_number_id := p_pd_id; 
    --
    IF (v_pd_part_number_id IS NULL)
    THEN
	    BEGIN
	    	SELECT t205g.c205g_pd_part_number_id
	    		INTO v_pd_part_number_id
		   FROM t205g_pd_part_number t205g
		  WHERE t205g.c205_part_number_id = p_partnumber;
		  EXCEPTION
	    WHEN NO_DATA_FOUND THEN
	    	v_pd_part_number_id := NULL;
	    END;	
    END IF;
    --
     UPDATE t205g_pd_part_number
    SET c205_part_num_desc              = p_part_desc, c202_project_id = p_project_id, c901_has_latex = p_has_latex
      , c901_requires_sterilization     = p_req_sterile, c901_sterilization_method = p_sterile_method,
        c901_has_more_than_one_Size     = p_dev_more_size, c901_size1_type = p_size1_type, c205g_size1_type_text =
        p_size1_text, c205g_size1_value = p_size1_value, c901_size1_uom = p_size1_uom
      , c901_size2_type                 = p_size2_type, c205g_size2_type_text = p_size2_text, c205g_size2_value =
        p_size2_value, c901_size2_uom   = p_size2_uom, c901_size3_type = p_size3_type
      , c205g_size3_type_text           = p_size3_text, c205g_size3_value = p_size3_value, c901_size3_uom = p_size3_uom
      , c901_size4_type                 = p_size4_type, c205g_size4_type_text = p_size4_text, c205g_size4_value =
        p_size4_value, c901_size4_uom   = p_size4_uom, c205g_part_setup_sync_fl = p_part_setup_sync_fl
      , c205g_void_fl = NULL  
      , c205g_last_updated_by           = p_userid, c205g_last_updated_date = SYSDATE
      , C205_part_number_id         = p_partnumber
      WHERE c205g_pd_part_number_id         = v_pd_part_number_id;
    --
    IF (SQL%ROWCOUNT = 0) THEN
        --
         SELECT s205G_PD_PART_NUMBER.nextval INTO v_pd_part_number_id FROM DUAL;
        --
         INSERT
           INTO t205g_pd_part_number
            (
                c205g_pd_part_number_id, C205_part_number_id, c205_part_num_desc
              , c202_project_id, c901_has_latex, c901_requires_sterilization
              , c901_sterilization_method, c901_has_more_than_one_Size, c901_size1_type
              , c205g_size1_type_text, c205g_size1_value, c901_size1_uom
              , c901_size2_type, c205g_size2_type_text, c205g_size2_value
              , c901_size2_uom, c901_size3_type, c205g_size3_type_text
              , c205g_size3_value, c901_size3_uom, c901_size4_type
              , c205g_size4_type_text, c205g_size4_value, c901_size4_uom
              , c205g_part_setup_sync_fl, c205g_created_by, c205g_created_date
            )
            VALUES
            (
                v_pd_part_number_id, p_partnumber, p_part_desc
              , p_project_id, p_has_latex, p_req_sterile
              , p_sterile_method, p_dev_more_size, p_size1_type
              , p_size1_text, p_size1_value, p_size1_uom
              , p_size2_type, p_size2_text, p_size2_value
              , p_size2_uom, p_size3_type, p_size3_text
              , p_size3_value, p_size3_uom, p_size4_type
              , p_size4_text, p_size4_value, p_size4_uom
              , p_part_setup_sync_fl, p_userid, SYSDATE
            ) ;
    END IF;
    --
    p_pd_id := v_pd_part_number_id;
END gm_sav_pd_part_number;
/*******************************************************
* Description : This procedure used to save the PD Part Number Details in t205 part attribute table
* Author   :
*******************************************************/
PROCEDURE gm_sav_part_pd_udi_dtls
    (
        p_inputstr IN CLOB,
        p_num_id   IN t205_part_number.c205_part_number_id%TYPE,
        p_user_id  IN t205_part_number.c205_last_updated_by%TYPE
    )
AS
    v_strlen NUMBER := NVL(LENGTH (p_inputstr), 0);
    v_string    VARCHAR2 (4000) := p_inputstr;
    v_substring VARCHAR2 (4000) ;
    v_attr_type t205d_part_attribute.c901_attribute_type%TYPE;
    v_attr_value t205d_part_attribute.c205d_attribute_value%TYPE;
    v_attr_id t205d_part_attribute.c2051_part_attribute_id%TYPE;
    --
    v_pd_cnt NUMBER;
    v_project_id t205g_pd_part_number.c202_project_id%TYPE;
    v_part_num_desc t205g_pd_part_number.c205_part_num_desc%TYPE;
    v_pd_id t205g_pd_part_number.c205g_pd_part_number_id%TYPE;
BEGIN
    -- This while loop used to save part_pd_udi_dtls info to part attribute table
    IF v_strlen                      > 0 THEN
        WHILE INSTR (v_string, '|') <> 0
        LOOP
            v_attr_type  := NULL;
            v_attr_value := NULL;
            v_substring  := SUBSTR (v_string, 1, INSTR (v_string, '|')       - 1) ;
            v_string     := SUBSTR (v_string, INSTR (v_string, '|')          + 1) ;
            v_attr_type  := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            v_substring  := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
            v_attr_value := v_substring;
            --
            gm_pkg_pd_partnumber.gm_sav_part_attribute (p_num_id, v_attr_type, v_attr_value, p_user_id, v_attr_id) ;
        END LOOP;
        --
        SELECT COUNT (1)
       INTO v_pd_cnt
       FROM T205G_PD_PART_NUMBER
      WHERE c205_part_number_id = p_num_id;
	      IF v_pd_cnt = 0
	      THEN
	      	SELECT T205.C202_PROJECT_ID, T205.C205_PART_NUM_DESC
	           INTO v_project_id, v_part_num_desc
	           FROM T205_PART_NUMBER T205
	          WHERE T205.C205_PART_NUMBER_ID = p_num_id;
	        --  
	        gm_pkg_pd_sav_udi.gm_sav_pd_part_number (v_pd_id,p_num_id, v_project_id, v_part_num_desc, NULL,
	                NULL, NULL, NULL, NULL, NULL, NULL,
	                NULL, NULL, NULL, NULL, NULL, NULL, NULL,
	                NULL, NULL, NULL, NULL, NULL, NULL,
	                'Y', p_user_id) ;
		END IF; -- end of v_pd_cnt                
    END IF;
END gm_sav_part_pd_udi_dtls;
/*******************************************************
* Description : When creating new part this procedure save the part attribute table
* from pd new table (t205g) and ETL also - calling the same procedure. 
* So Added the new parameter (p_check_status_fl) -Via ETL, to avoid the part # status check.
* Becuase ETL upload the part # status is Approved. At the time need to sync the PD data.  
* Author   : Mani
*******************************************************/
PROCEDURE gm_sav_pd_part_attribute
    (
        p_partnumber IN T205G_PD_PART_NUMBER.c205_part_number_id%TYPE,
        p_user_id    IN T205G_PD_PART_NUMBER.c205G_last_updated_by%TYPE,
        p_check_status_fl IN VARCHAR2 DEFAULT NULL
    )
AS
    v_cnt      NUMBER;
    v_attr_cnt	NUMBER;
    v_inputstr VARCHAR2 (4000) ;
    v_has_latex t205g_pd_part_number.c901_has_latex%TYPE;
    v_req_sterile t205g_pd_part_number.c901_requires_sterilization%TYPE;
    v_sterile_method t205g_pd_part_number.c901_sterilization_method%TYPE;
    v_dev_more_size t205g_pd_part_number.c901_has_more_than_one_Size%TYPE;
    v_size1_type t205g_pd_part_number.c901_size1_type%TYPE;
    v_size1_text t205g_pd_part_number.c205g_size1_type_text%TYPE;
    v_size1_value t205g_pd_part_number.c205g_size1_value%TYPE;
    v_size1_uom t205g_pd_part_number.c901_size1_uom%TYPE;
    v_size2_type t205g_pd_part_number.c901_size2_type%TYPE;
    v_size2_text t205g_pd_part_number.c205g_size2_type_text%TYPE;
    v_size2_value t205g_pd_part_number.c205g_size2_value%TYPE;
    v_size2_uom t205g_pd_part_number.c901_size2_uom%TYPE;
    v_size3_type t205g_pd_part_number.c901_size3_type%TYPE;
    v_size3_text t205g_pd_part_number.c205g_size3_type_text%TYPE;
    v_size3_value t205g_pd_part_number.c205g_size3_value%TYPE;
    v_size3_uom t205g_pd_part_number.c901_size3_uom%TYPE;
    v_size4_type t205g_pd_part_number.c901_size4_type%TYPE;
    v_size4_text t205g_pd_part_number.c205g_size4_type_text%TYPE;
    v_size4_value t205g_pd_part_number.c205g_size4_value%TYPE;
    v_size4_uom t205g_pd_part_number.c901_size4_uom%TYPE;
    v_part_cnt NUMBER;
BEGIN
     SELECT COUNT (1)
       INTO v_cnt
       FROM T205G_PD_PART_NUMBER
      WHERE c205_part_number_id = p_partnumber
      	AND c205g_void_fl IS NULL
      	AND c205g_part_setup_sync_fl IS NULL;
      --
      SELECT COUNT (1)
      INTO v_attr_cnt
   FROM t205_part_number
  WHERE c205_part_number_id = p_partnumber
    AND c901_status_id    = DECODE(p_check_status_fl, 'Y', 20367, -999);
    --
    v_part_cnt := gm_pkg_pd_rpt_udi.get_part_number_cnt (p_partnumber);
    --
    IF (v_cnt                   <> 0 AND v_part_cnt <> 0 AND v_attr_cnt = 0 ) THEN
         SELECT t205g.c901_has_latex latex, t205g.c901_requires_sterilization req_sterile,
            t205g.c901_sterilization_method sterile_method, t205g.c901_has_more_than_one_Size more_size,
            t205g.c901_size1_type size1, t205g.c205g_size1_type_text size1_text, t205g.c205g_size1_value size1_value
          , t205g.c901_size1_uom size1_uom, t205g.c901_size2_type size2, t205g.c205g_size2_type_text size2_text
          , t205g.c205g_size2_value size2_value, t205g.c901_size2_uom size2_uom, t205g.c901_size3_type size3
          , t205g.c205g_size3_type_text size3_text, t205g.c205g_size3_value size3_value, t205g.c901_size3_uom size3_uom
          , t205g.c901_size4_type size4, t205g.c205g_size4_type_text size4_text, t205g.c205g_size4_value size4_value
          , t205g.c901_size4_uom size4_uom
           INTO v_has_latex, v_req_sterile, v_sterile_method
          , v_dev_more_size, v_size1_type, v_size1_text
          , v_size1_value, v_size1_uom, v_size2_type
          , v_size2_text, v_size2_value, v_size2_uom
          , v_size3_type, v_size3_text, v_size3_value
          , v_size3_uom, v_size4_type, v_size4_text
          , v_size4_value, v_size4_uom
           FROM t205g_pd_part_number t205g
          WHERE t205g.c205_part_number_id = p_partnumber;
        v_inputstr                       := '104460^'||v_has_latex||'|104461^'||v_req_sterile || '|104462^'||
        v_sterile_method ||'|104463^'||v_dev_more_size || '|104464^'||v_size1_type ||'|104465^'||v_size1_text||
        '|104466^'||v_size1_value ||'|104467^'||v_size1_uom || '|104468^'||v_size2_type ||'|104469^'||v_size2_text ||
        '|104470^'||v_size2_value ||'|104471^'||v_size2_uom || '|104472^'||v_size3_type ||'|104473^'||v_size3_text ||
        '|104474^'||v_size3_value ||'|104475^'||v_size3_uom || '|104476^'||v_size4_type ||'|104477^'||v_size4_text ||
        '|104478^'||v_size4_value ||'|104479^'||v_size4_uom ||'|';
        gm_pkg_pd_sav_udi.gm_sav_part_pd_udi_dtls (v_inputstr, p_partnumber, p_user_id) ;
        -- If part # present in Part number setup - then, to update the Sync flag to t205g
        IF v_part_cnt <> 0
        THEN
        	gm_pkg_pd_sav_udi.gm_upd_proj_partdesc (p_partnumber, p_user_id);
        END IF;
    END IF;
END gm_sav_pd_part_attribute;
/*******************************************************
* Description : This procedure used to update the PD data set (project id and part des.)
* Author   :
*******************************************************/
PROCEDURE gm_upd_proj_partdesc (
        p_partnumber IN T205G_PD_PART_NUMBER.c205_part_number_id%TYPE,
        p_user_id    IN T205G_PD_PART_NUMBER.c205G_last_updated_by%TYPE)
AS
    v_cnt NUMBER;
    v_project_id t205g_pd_part_number.c202_project_id%TYPE;
    v_part_num_desc t205g_pd_part_number.c205_part_num_desc%TYPE;
    v_part_sterile_fl VARCHAR2(10);
    v_req_sterile_val NUMBER := 80131;
    v_attr_id t205d_part_attribute.c2051_part_attribute_id%TYPE;
BEGIN
     SELECT COUNT (1)
       INTO v_cnt
       FROM T205G_PD_PART_NUMBER
      WHERE c205_part_number_id = p_partnumber;
    IF v_cnt                   <> 0 THEN
         SELECT T205.C202_PROJECT_ID, T205.C205_PART_NUM_DESC, DECODE(c205_product_class, '4030', 'Y', 'N')
           INTO v_project_id, v_part_num_desc, v_part_sterile_fl
           FROM T205_PART_NUMBER T205
          WHERE T205.C205_PART_NUMBER_ID = p_partnumber;
          --
         UPDATE T205G_PD_PART_NUMBER
        SET C202_PROJECT_ID         = v_project_id, C205_PART_NUM_DESC = v_part_num_desc, C205G_LAST_UPDATED_BY = p_user_id
          , c901_requires_sterilization = DECODE(v_part_sterile_fl, 'Y', v_req_sterile_val, c901_requires_sterilization)
          , C205G_LAST_UPDATED_DATE = SYSDATE
          , c205g_part_setup_sync_fl = 'Y'
          WHERE C205_PART_NUMBER_ID = p_partnumber;
          --
          IF v_part_sterile_fl = 'Y'
          THEN
          	gm_pkg_pd_partnumber.gm_sav_part_attribute (p_partnumber, '104461', v_req_sterile_val, p_user_id, v_attr_id) ;
          END IF;
    END IF;
END gm_upd_proj_partdesc;
/*******************************************************
* Description : This procedure used to update the void flag (Y/null)
* Author   :
*******************************************************/
PROCEDURE gm_upd_void_pd_part_num (
        p_pd_id IN T205G_PD_PART_NUMBER.c205g_pd_part_number_id%TYPE,
        p_void_fl    IN T205G_PD_PART_NUMBER.c205g_void_fl%TYPE,
        p_user_id    IN T205G_PD_PART_NUMBER.c205G_last_updated_by%TYPE)
AS
BEGIN
     UPDATE T205G_PD_PART_NUMBER
    SET c205g_void_fl           = p_void_fl, C205G_LAST_UPDATED_BY = p_user_id, C205G_LAST_UPDATED_DATE = SYSDATE
      WHERE c205g_pd_part_number_id = p_pd_id;
END gm_upd_void_pd_part_num; 
/*******************************************************
* Description : This procedure used to void the selected parts
* Author   :
*******************************************************/
PROCEDURE gm_void_prod_dev_udi_dtls (
        p_del_part_str IN CLOB,
        p_userid       IN t205g_pd_part_number.c205g_created_by%TYPE,
        p_out_parts OUT CLOB)
AS
    v_void_strlen NUMBER := NVL (LENGTH (p_del_part_str), 0) ;
    v_void_string CLOB   := p_del_part_str;
    v_void_substring VARCHAR2 (4000) ;
    v_pd_id T205G_PD_PART_NUMBER.c205g_pd_part_number_id%TYPE;
BEGIN
    --
    IF v_void_strlen                      > 0 THEN
        WHILE INSTR (v_void_string, ',') <> 0
        LOOP
            v_void_substring := SUBSTR (v_void_string, 1, INSTR (v_void_string, ',') - 1) ;
            v_void_string    := SUBSTR (v_void_string, INSTR (v_void_string, ',')    + 1) ;
            --
           gm_pkg_pd_sav_udi.gm_upd_void_pd_part_num (v_void_substring, 'Y', p_userid) ;
            --
            p_out_parts := p_out_parts || v_void_substring ||'|';
            --
        END LOOP;
    END IF;
    --
END gm_void_prod_dev_udi_dtls;
/*******************************************************
* Description : This procedure used to validate the PD dtls
* Author   :
*******************************************************/
PROCEDURE gm_validate_pd_part_number (
        p_pd_part_str IN CLOB,
        p_userid      IN t205g_pd_part_number.c205g_created_by%TYPE,
        p_out_err OUT CLOB)
AS
    v_strlen NUMBER := NVL (LENGTH (p_pd_part_str), 0) ;
    v_string CLOB   := p_pd_part_str;
    v_substring VARCHAR2 (4000) ;
    --
    v_out_pd_id CLOB;
    v_partnumber VARCHAR2 (4000);
    v_project_id VARCHAR2 (4000);
    v_part_desc t205g_pd_part_number.c205_part_num_desc%TYPE;
    v_has_latex      VARCHAR2 (20) ;
    v_req_sterile    VARCHAR2 (20) ;
    v_sterile_method VARCHAR2 (20) ;
    v_dev_more_size  VARCHAR2 (20) ;
    v_size1_type     VARCHAR2 (20) ;
    v_size1_text t205g_pd_part_number.c205g_size1_type_text%TYPE;
    v_size1_value VARCHAR2 (20) ;
    v_size1_uom   VARCHAR2 (20) ;
    v_size2_type  VARCHAR2 (20) ;
    v_size2_text t205g_pd_part_number.c205g_size2_type_text%TYPE;
    v_size2_value VARCHAR2 (20) ;
    v_size2_uom   VARCHAR2 (20) ;
    v_size3_type  VARCHAR2 (20) ;
    v_size3_text t205g_pd_part_number.c205g_size3_type_text%TYPE;
    v_size3_value VARCHAR2 (20) ;
    v_size3_uom   VARCHAR2 (20) ;
    v_size4_type  VARCHAR2 (20) ;
    v_size4_text t205g_pd_part_number.c205g_size4_type_text%TYPE;
    v_size4_value VARCHAR2 (20) ;
    v_size4_uom   VARCHAR2 (20) ;
    v_part_setup_sync_fl t205g_pd_part_number.c205g_part_setup_sync_fl%TYPE;
    v_pd_id t205g_pd_part_number.c205g_pd_part_number_id%TYPE;
    v_dhtmlx_rowid VARCHAR2 (4000) ;
    --
    v_cnt             NUMBER;
    v_part_count  	  NUMBER;
    v_err_project_str VARCHAR2 (4000) ;
    v_err_part_str    VARCHAR2 (4000) ;
    v_error_fl        VARCHAR2 (1) ;
    --
    v_out_msg VARCHAR2 (4000) ;
    v_out_wo_cur TYPES.cursor_type;
    --
    v_duplicate_part CLOB;
    v_invalid_project CLOB;
    v_final_err CLOB;
    v_part_max_len	NUMBER;
    v_quality_part_cnt NUMBER;
BEGIN
    --
     DELETE FROM my_temp_pd_part_number;
     DELETE FROM my_temp_key_value;
    IF v_strlen                      > 0 THEN
        WHILE INSTR (v_string, '|') <> 0
        LOOP
            v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
            v_string    := SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
            --
            v_pd_id              := NULL;
            v_partnumber         := NULL;
            v_project_id         := NULL;
            v_part_desc          := NULL;
            v_has_latex          := NULL;
            v_req_sterile        := NULL;
            v_sterile_method     := NULL;
            v_dev_more_size      := NULL;
            v_size1_type         := NULL;
            v_size1_text         := NULL;
            v_size1_value        := NULL;
            v_size1_uom          := NULL;
            v_size2_type         := NULL;
            v_size2_text         := NULL;
            v_size2_value        := NULL;
            v_size2_uom          := NULL;
            v_size3_type         := NULL;
            v_size3_text         := NULL;
            v_size3_value        := NULL;
            v_size3_uom          := NULL;
            v_size4_type         := NULL;
            v_size4_text         := NULL;
            v_size4_value        := NULL;
            v_size4_uom          := NULL;
            v_part_setup_sync_fl := NULL;
            v_dhtmlx_rowid       := NULL;
            --
            v_pd_id          := TO_NUMBER (SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1)) ;
            v_substring      := SUBSTR (v_substring, INSTR (v_substring, '^')               + 1) ;
            v_partnumber     := SUBSTR (v_substring, 1, INSTR (v_substring, '^')            - 1) ;
            v_substring      := SUBSTR (v_substring, INSTR (v_substring, '^')               + 1) ;
            v_project_id     := SUBSTR (v_substring, 1, INSTR (v_substring, '^')            - 1) ;
            v_substring      := SUBSTR (v_substring, INSTR (v_substring, '^')               + 1) ;
            v_part_desc      := SUBSTR (v_substring, 1, INSTR (v_substring, '^')            - 1) ;
            v_substring      := SUBSTR (v_substring, INSTR (v_substring, '^')               + 1) ;
            v_has_latex      := SUBSTR (v_substring, 1, INSTR (v_substring, '^')            - 1) ;
            v_substring      := SUBSTR (v_substring, INSTR (v_substring, '^')               + 1) ;
            v_req_sterile    := SUBSTR (v_substring, 1, INSTR (v_substring, '^')            - 1) ;
            v_substring      := SUBSTR (v_substring, INSTR (v_substring, '^')               + 1) ;
            v_sterile_method := SUBSTR (v_substring, 1, INSTR (v_substring, '^')            - 1) ;
            v_substring      := SUBSTR (v_substring, INSTR (v_substring, '^')               + 1) ;
            v_dev_more_size  := SUBSTR (v_substring, 1, INSTR (v_substring, '^')            - 1) ;
            v_substring      := SUBSTR (v_substring, INSTR (v_substring, '^')               + 1) ;
            v_size1_type     := SUBSTR (v_substring, 1, INSTR (v_substring, '^')            - 1) ;
            v_substring      := SUBSTR (v_substring, INSTR (v_substring, '^')               + 1) ;
            v_size1_text     := SUBSTR (v_substring, 1, INSTR (v_substring, '^')            - 1) ;
            v_substring      := SUBSTR (v_substring, INSTR (v_substring, '^')               + 1) ;
            v_size1_value    := SUBSTR (v_substring, 1, INSTR (v_substring, '^')            - 1) ;
            v_substring      := SUBSTR (v_substring, INSTR (v_substring, '^')               + 1) ;
            v_size1_uom      := SUBSTR (v_substring, 1, INSTR (v_substring, '^')            - 1) ;
            v_substring      := SUBSTR (v_substring, INSTR (v_substring, '^')               + 1) ;
            v_size2_type     := SUBSTR (v_substring, 1, INSTR (v_substring, '^')            - 1) ;
            v_substring      := SUBSTR (v_substring, INSTR (v_substring, '^')               + 1) ;
            v_size2_text     := SUBSTR (v_substring, 1, INSTR (v_substring, '^')            - 1) ;
            v_substring      := SUBSTR (v_substring, INSTR (v_substring, '^')               + 1) ;
            v_size2_value    := SUBSTR (v_substring, 1, INSTR (v_substring, '^')            - 1) ;
            v_substring      := SUBSTR (v_substring, INSTR (v_substring, '^')               + 1) ;
            v_size2_uom      := SUBSTR (v_substring, 1, INSTR (v_substring, '^')            - 1) ;
            v_substring      := SUBSTR (v_substring, INSTR (v_substring, '^')               + 1) ;
            v_size3_type     := SUBSTR (v_substring, 1, INSTR (v_substring, '^')            - 1) ;
            v_substring      := SUBSTR (v_substring, INSTR (v_substring, '^')               + 1) ;
            v_size3_text     := SUBSTR (v_substring, 1, INSTR (v_substring, '^')            - 1) ;
            v_substring      := SUBSTR (v_substring, INSTR (v_substring, '^')               + 1) ;
            v_size3_value    := SUBSTR (v_substring, 1, INSTR (v_substring, '^')            - 1) ;
            v_substring      := SUBSTR (v_substring, INSTR (v_substring, '^')               + 1) ;
            v_size3_uom      := SUBSTR (v_substring, 1, INSTR (v_substring, '^')            - 1) ;
            v_substring      := SUBSTR (v_substring, INSTR (v_substring, '^')               + 1) ;
            v_size4_type     := SUBSTR (v_substring, 1, INSTR (v_substring, '^')            - 1) ;
            v_substring      := SUBSTR (v_substring, INSTR (v_substring, '^')               + 1) ;
            v_size4_text     := SUBSTR (v_substring, 1, INSTR (v_substring, '^')            - 1) ;
            v_substring      := SUBSTR (v_substring, INSTR (v_substring, '^')               + 1) ;
            v_size4_value    := SUBSTR (v_substring, 1, INSTR (v_substring, '^')            - 1) ;
            v_substring      := SUBSTR (v_substring, INSTR (v_substring, '^')               + 1) ;
            v_size4_uom      := SUBSTR (v_substring, 1, INSTR (v_substring, '^')            - 1) ;
            v_substring      := SUBSTR (v_substring, INSTR (v_substring, '^')               + 1) ;
            v_dhtmlx_rowid   := v_substring;
            --
            --Check for the duplicate Part while submit
	 		 SELECT COUNT (C205_part_number_id)
	       		INTO v_part_count
	         FROM my_temp_pd_part_number
	       	 WHERE C205_part_number_id    = v_partnumber;
	       	 -- check the max length
	       	 BEGIN
	       	 SELECT LENGTH (v_partnumber) INTO v_part_max_len FROM dual;
	       	 EXCEPTION
		    WHEN NO_DATA_FOUND THEN
		        v_part_max_len := 0;
		    END;
		    
		    IF v_part_max_len > 20
		    THEN
		    	INSERT 
            	   INTO my_temp_key_value
	 				( 
	 				  MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE 
	 				)
	 			SELECT v_dhtmlx_rowid, v_partnumber ,'part^maxLen' FROM Dual;
		    ELSE
	       	 IF v_part_count = 0  
      			THEN		 
             INSERT
               INTO my_temp_pd_part_number
                (
                    c205g_pd_part_number_id, C205_part_number_id, c205_part_num_desc
                  , c202_project_id, c901_has_latex, c901_requires_sterilization
                  , c901_sterilization_method, c901_has_more_than_one_Size, c901_size1_type
                  , c205g_size1_type_text, c205g_size1_value, c901_size1_uom
                  , c901_size2_type, c205g_size2_type_text, c205g_size2_value
                  , c901_size2_uom, c901_size3_type, c205g_size3_type_text
                  , c205g_size3_value, c901_size3_uom, c901_size4_type
                  , c205g_size4_type_text, c205g_size4_value, c901_size4_uom
                  , c205g_part_setup_sync_fl, c205g_last_updated_by, c205g_last_updated_date
                  , c205g_dhtmlx_rowid
                )
                VALUES
                (
                    v_pd_id, v_partnumber, v_part_desc
                  , DECODE(v_project_id, '0', NULL, v_project_id), DECODE(v_has_latex,'0', NULL, v_has_latex), DECODE(v_req_sterile, '0', NULL, v_req_sterile)
                  , DECODE(v_sterile_method, '0', NULL, v_sterile_method), DECODE(v_dev_more_size, '0', NULL, v_dev_more_size), DECODE(v_size1_type, '0', NULL, v_size1_type)
                  , v_size1_text, v_size1_value, DECODE(v_size1_uom, '0', NULL, v_size1_uom)
                  , DECODE(v_size2_type, '0', NULL, v_size2_type), v_size2_text, v_size2_value
                  , DECODE(v_size2_uom, '0', NULL, v_size2_uom), DECODE(v_size3_type, '0', NULL, v_size3_type), v_size3_text
                  , v_size3_value, DECODE(v_size3_uom, '0', NULL, v_size3_uom), DECODE(v_size4_type, '0', NULL, v_size4_type)
                  , v_size4_text, v_size4_value,  DECODE(v_size4_uom, '0', NULL, v_size4_uom)
                  , v_part_setup_sync_fl, NULL, NULL
                  , v_dhtmlx_rowid
                ) ;
            --
				-- validate the duplicate part.
				 SELECT COUNT (1)
				   INTO v_quality_part_cnt
				   FROM t205_part_number
				  WHERE c205_part_number_id = v_partnumber
				    AND c901_status_id      = 20367;
				--    
				IF v_quality_part_cnt      <> 0 THEN
				     INSERT
				       INTO my_temp_key_value
				        (
				            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
				        )
				     SELECT v_dhtmlx_rowid, v_partnumber, 'part^qualityPart' FROM Dual;
				END IF; --  v_quality_part_cnt
            ELSE 
            	INSERT 
            	   INTO my_temp_key_value
	 				( 
	 				  MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE 
	 				)
	 			SELECT v_dhtmlx_rowid, v_partnumber ,'part^duplicate' FROM Dual;
	 		
    		END IF;
		END IF;    		
                
        END LOOP;
    END IF;
    -- validate the duplicate part.
     INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
     SELECT tmp.c205g_dhtmlx_rowid, tmp.c205_part_number_id, 'part^alreadyExist'
       FROM my_temp_pd_part_number tmp, t205g_pd_part_number t205g
      WHERE tmp.c205g_pd_part_number_id IS NULL
        AND t205g.c205_part_number_id    = tmp.c205_part_number_id
        AND t205g.c205g_void_fl IS NULL
        UNION
       SELECT tmp.c205g_dhtmlx_rowid, tmp.c205_part_number_id, 'part^alreadyExist'
       FROM my_temp_pd_part_number tmp, t205g_pd_part_number t205g
      WHERE tmp.c205g_pd_part_number_id IS NOT NULL
        AND t205g.c205_part_number_id    = tmp.c205_part_number_id
        AND tmp.c205g_pd_part_number_id <> t205g.c205g_pd_part_number_id
        AND t205g.c205g_void_fl IS NULL;
      	
     
       -- validate the project
    INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'part^mandatory'
       FROM my_temp_pd_part_number
      WHERE c205_part_number_id IS NULL;  
    -- validate the invalid project
     INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'project^invalidProject'
       FROM my_temp_pd_part_number
      WHERE c202_project_id NOT IN
        (
             SELECT c202_project_id
               FROM t202_project
              WHERE C202_VOID_FL   IS NULL
                AND C901_STATUS_ID >= '20301'
        );
    -- validate the project
    INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'project^mandatory'
       FROM my_temp_pd_part_number
      WHERE c202_project_id IS NULL;  
    -- validate the part des
     INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'partDesc^mandatory'
       FROM my_temp_pd_part_number
      WHERE c205_part_num_desc IS NULL;
    --
    -- validate the non numeric value.
     INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'containsLatex^invalid'
       FROM my_temp_pd_part_number
      WHERE REGEXP_LIKE (c901_has_latex, '[^0-9]+')
      UNION
       SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'containsLatex^invalid'
	   FROM my_temp_pd_part_number tmp
	  WHERE tmp.c901_has_latex     IS NOT NULL
	    AND tmp.c901_has_latex NOT IN
	    (
	         SELECT TO_CHAR(C901_CODE_ID)
	           FROM T901_CODE_LOOKUP
	          WHERE C901_CODE_GRP IN ('YES/NO')
	            AND C901_ACTIVE_FL = '1'
	    ) ;
      -- validate the filed
      INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'containsLatex^mandatory'
       FROM my_temp_pd_part_number
      WHERE c901_has_latex IS NULL;
    -- req sterile
     INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'req^invalid'
       FROM my_temp_pd_part_number
      WHERE REGEXP_LIKE (c901_requires_sterilization, '[^0-9]+')
       UNION
       SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'req^invalid'
	   FROM my_temp_pd_part_number tmp
	  WHERE tmp.c901_requires_sterilization     IS NOT NULL
	    AND tmp.c901_requires_sterilization NOT IN
	    (
	         SELECT TO_CHAR(C901_CODE_ID)
	           FROM T901_CODE_LOOKUP
	          WHERE C901_CODE_GRP IN ('YES/NO')
	            AND C901_ACTIVE_FL = '1'
	    ) ;       
     --
     INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'req^mandatory'
       FROM my_temp_pd_part_number
      WHERE c901_requires_sterilization IS NULL;   
    --sterile method
     INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'steril^invalid'
       FROM my_temp_pd_part_number
      WHERE (REGEXP_LIKE (c901_sterilization_method, '[^0-9]+'))
         UNION
       SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'steril^invalid'
	   FROM my_temp_pd_part_number tmp
	  WHERE tmp.c901_sterilization_method     IS NOT NULL
	    AND tmp.c901_sterilization_method NOT IN
	    (
	         SELECT TO_CHAR(C901_CODE_ID)
	           FROM T901_CODE_LOOKUP
	          WHERE C901_CODE_GRP IN ('PDSTER')
	            AND C901_ACTIVE_FL = '1'
	    ) ;
     --
     INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'steril^mandatory'
       FROM my_temp_pd_part_number
      WHERE c901_requires_sterilization = '80130'
        AND (c901_sterilization_method IS NULL) ;   
     --sterile method
     INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'steril^invalid'
       FROM my_temp_pd_part_number
      WHERE c901_requires_sterilization IS NULL
        AND c901_sterilization_method IS NOT NULL;   
    -- is device avilable more
     INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'isDev^invalid'
       FROM my_temp_pd_part_number
      WHERE REGEXP_LIKE (c901_has_more_than_one_Size, '[^0-9]+')
       UNION
       SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'isDev^invalid'
	   FROM my_temp_pd_part_number tmp
	  WHERE tmp.c901_has_more_than_one_Size     IS NOT NULL
	    AND tmp.c901_has_more_than_one_Size NOT IN
	    (
	         SELECT TO_CHAR(C901_CODE_ID)
	           FROM T901_CODE_LOOKUP
	          WHERE C901_CODE_GRP IN ('YES/NO')
	            AND C901_ACTIVE_FL = '1'
	    ) ;
    --
    INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'isDev^mandatory'
       FROM my_temp_pd_part_number
      WHERE c901_has_more_than_one_Size IS NULL;
    -- type validate
     INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'size1Type^mandatory'
       FROM my_temp_pd_part_number
      WHERE c901_has_more_than_one_Size = '80130'
        AND (c901_size1_type           IS NULL
        AND c901_size2_type            IS NULL
        AND c901_size3_type            IS NULL
        AND c901_size4_type            IS NULL); 
    -- invalid size 1 type
     INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'size1Type^invalid'
       FROM my_temp_pd_part_number
      WHERE (REGEXP_LIKE (c901_size1_type, '[^0-9]+'))
       UNION
       SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'size1Type^invalid'
	   FROM my_temp_pd_part_number tmp
	  WHERE tmp.c901_size1_type     IS NOT NULL
	    AND tmp.c901_size1_type NOT IN
	    (
	         SELECT TO_CHAR(C901_CODE_ID)
	           FROM T901_CODE_LOOKUP
	          WHERE C901_CODE_GRP IN ('PDSIZE')
	            AND C901_ACTIVE_FL = '1'
	    ) ;
    -- empty check the size 1 text filed
     INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'size1Text^mandatory'
       FROM my_temp_pd_part_number
      WHERE c901_size1_type        = '104047'
        AND c205g_size1_type_text IS NULL;
      -- empty check the size 1 text filed
     INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
      SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'size1Text^invalid'
       FROM my_temp_pd_part_number
      WHERE (c901_size1_type IS NULL OR c901_size1_type <> '104047')
        AND c205g_size1_type_text IS NOT NULL;
        
    -- validate the size 1 value
     INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'size1Value^invalid'
       FROM my_temp_pd_part_number
      WHERE c901_size1_type IS NOT NULL
        AND (REGEXP_LIKE (c205g_size1_value, '[^0-9/./-]+'))
        OR c205g_size1_value = '0'
        UNION ALL
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'size1Value^invalid'
     FROM my_temp_pd_part_number
     WHERE c901_size1_type IS NULL
     AND c205g_size1_value IS NOT NULL;
      --  
      INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'size1Value^mandatory'
       FROM my_temp_pd_part_number
      WHERE c901_size1_type IS NOT NULL
      AND c901_size1_type <>'104047' -- To Avoid Type Others
        AND c205g_size1_value IS NULL;  
    -- size 1 uom
     INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'uom1^invalid'
       FROM my_temp_pd_part_number
      WHERE c901_size1_type IS NOT NULL
        AND (REGEXP_LIKE (c901_size1_uom, '[^0-9]+'))
        UNION
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'uom1^invalid'
       FROM my_temp_pd_part_number
       WHERE c901_size1_type IS NULL
       AND c901_size1_uom IS NOT NULL
       UNION
       SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'uom1^invalid'
	   FROM my_temp_pd_part_number tmp
	  WHERE tmp.c901_size1_uom     IS NOT NULL
	    AND tmp.c901_size1_uom NOT IN
	    (
	         SELECT TO_CHAR(C901_CODE_ID)
	           FROM T901_CODE_LOOKUP
	          WHERE C901_CODE_GRP IN ('PDUOM')
	            AND C901_ACTIVE_FL = '1'
	    ) ;
    --
    -- size 1 uom
     INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'uom1^mandatory'
       FROM my_temp_pd_part_number
      WHERE c901_size1_type IS NOT NULL
      AND c901_size1_type <>'104047' -- To Avoid Type Others
        AND c901_size1_uom IS NULL;
        
            -- size 1 uom
   INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'uom1^uomempty'
       FROM my_temp_pd_part_number
      WHERE c901_size1_type IS NOT NULL
        AND c901_size1_uom IS NOT NULL
        AND c901_size1_type = '104047';
        
    -- need to filed as empty
     INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'size1Type^invalid'
       FROM my_temp_pd_part_number
      WHERE c901_has_more_than_one_Size = '80131'
        AND c901_size1_type IS NOT NULL;
    -- empty check the size 1 text filed
     INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'size1Text^invalid'
       FROM my_temp_pd_part_number
      WHERE c901_has_more_than_one_Size = '80131'
        AND c205g_size1_type_text IS NOT NULL;
    -- validate the size 1 value
     INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'size1Value^invalid'
       FROM my_temp_pd_part_number
      WHERE c901_has_more_than_one_Size = '80131'
        AND c205g_size1_value IS NOT NULL;
    -- size 1 uom
     INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'uom1^invalid'
       FROM my_temp_pd_part_number
      WHERE c901_has_more_than_one_Size = '80131'
        AND c901_size1_uom IS NOT NULL;
     
     -- need to filed as empty
     INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'size2Type^invalid'
       FROM my_temp_pd_part_number
      WHERE c901_has_more_than_one_Size = '80131'
        AND c901_size2_type IS NOT NULL;
    -- empty check the size 1 text filed
     INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'size2Text^invalid'
       FROM my_temp_pd_part_number
      WHERE c901_has_more_than_one_Size = '80131'
        AND c205g_size2_type_text IS NOT NULL;
    -- validate the size 1 value
     INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'size2Value^invalid'
       FROM my_temp_pd_part_number
      WHERE c901_has_more_than_one_Size = '80131'
        AND c205g_size2_value IS NOT NULL;
    -- size 1 uom
     INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'uom2^invalid'
       FROM my_temp_pd_part_number
      WHERE c901_has_more_than_one_Size = '80131'
        AND c901_size2_uom IS NOT NULL;
        
      -- need to filed as empty
     INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'size3Type^invalid'
       FROM my_temp_pd_part_number
      WHERE c901_has_more_than_one_Size = '80131'
        AND c901_size3_type IS NOT NULL;
    -- empty check the size 1 text filed
     INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'size3Text^invalid'
       FROM my_temp_pd_part_number
      WHERE c901_has_more_than_one_Size = '80131'
        AND c205g_size3_type_text IS NOT NULL;
    -- validate the size 1 value
     INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'size3Value^invalid'
       FROM my_temp_pd_part_number
      WHERE c901_has_more_than_one_Size = '80131'
        AND c205g_size3_value IS NOT NULL;
    -- size 1 uom
     INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'uom3^invalid'
       FROM my_temp_pd_part_number
      WHERE c901_has_more_than_one_Size = '80131'
        AND c901_size3_uom IS NOT NULL;
        
        
      -- need to filed as empty
     INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'size4Type^invalid'
       FROM my_temp_pd_part_number
      WHERE c901_has_more_than_one_Size = '80131'
        AND c901_size4_type IS NOT NULL;
    -- empty check the size 1 text filed
     INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'size4Text^invalid'
       FROM my_temp_pd_part_number
      WHERE c901_has_more_than_one_Size = '80131'
        AND c205g_size4_type_text IS NOT NULL;
    -- validate the size 1 value
     INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'size4Value^invalid'
       FROM my_temp_pd_part_number
      WHERE c901_has_more_than_one_Size = '80131'
        AND c205g_size4_value IS NOT NULL;
    -- size 1 uom
     INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'uom4^invalid'
       FROM my_temp_pd_part_number
      WHERE c901_has_more_than_one_Size = '80131'
        AND c901_size4_uom IS NOT NULL;  
    -- invalid size 2 type
     INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'size2Type^invalid'
       FROM my_temp_pd_part_number
      WHERE REGEXP_LIKE (c901_size2_type, '[^0-9]+')
      UNION
       SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'size2Type^invalid'
	   FROM my_temp_pd_part_number tmp
	  WHERE tmp.c901_size2_type     IS NOT NULL
	    AND tmp.c901_size2_type NOT IN
	    (
	         SELECT TO_CHAR(C901_CODE_ID)
	           FROM T901_CODE_LOOKUP
	          WHERE C901_CODE_GRP IN ('PDSIZE')
	            AND C901_ACTIVE_FL = '1'
	    ) ;
    -- empty check the size 1 text filed
     INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'size2Text^mandatory'
       FROM my_temp_pd_part_number
      WHERE c901_size2_type        = '104047'
        AND c205g_size2_type_text IS NULL;
      -- empty check the size 1 text filed
     INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
      SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'size2Text^invalid'
       FROM my_temp_pd_part_number
      WHERE (c901_size2_type IS NULL OR c901_size2_type <> '104047')
        AND c205g_size2_type_text IS NOT NULL;
        
    -- validate the size 1 value
     INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'size2Value^invalid'
       FROM my_temp_pd_part_number
      WHERE c901_size2_type IS NOT NULL
        AND (REGEXP_LIKE (c205g_size2_value, '[^0-9/./-]+'))
         OR c205g_size2_value = '0'
UNION ALL
         
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'size2Value^invalid'
     FROM my_temp_pd_part_number
     WHERE c901_size2_type IS NULL
     AND c205g_size2_value IS NOT NULL;
     -- validate the size 1 value
     INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
     
        
         SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'size2Value^mandatory'
       FROM my_temp_pd_part_number
      WHERE c901_size2_type IS NOT NULL
      AND c901_size2_type <>'104047' -- To Avoid Type Others
        AND c205g_size2_value IS NULL; 
    -- size 1 uom
     INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'uom2^invalid'
       FROM my_temp_pd_part_number
      WHERE c901_size2_type IS NOT NULL
        AND (REGEXP_LIKE (c901_size2_uom, '[^0-9]+'))
        UNION
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'uom2^invalid'
       FROM my_temp_pd_part_number
       WHERE c901_size2_type IS NULL
       AND c901_size2_uom IS NOT NULL
       UNION
       SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'uom2^invalid'
	   FROM my_temp_pd_part_number tmp
	  WHERE tmp.c901_size2_uom     IS NOT NULL
	    AND tmp.c901_size2_uom NOT IN
	    (
	         SELECT TO_CHAR(C901_CODE_ID)
	           FROM T901_CODE_LOOKUP
	          WHERE C901_CODE_GRP IN ('PDUOM')
	            AND C901_ACTIVE_FL = '1'
	    ) ;
    --
    -- size 1 uom
     INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'uom2^mandatory'
       FROM my_temp_pd_part_number
      WHERE c901_size2_type IS NOT NULL
      AND c901_size2_type <>'104047' -- To Avoid Type Others
        AND c901_size2_uom IS NULL;
  -- size 1 uom
  
        INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'uom2^uomempty'
       FROM my_temp_pd_part_number
      WHERE c901_size2_type IS NOT NULL
        AND c901_size2_uom IS NOT NULL
        AND c901_size2_type = '104047';

        
    -- invalid size 3 type
     INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'size3Type^invalid'
       FROM my_temp_pd_part_number
      WHERE REGEXP_LIKE (c901_size3_type, '[^0-9]+')
       UNION
       SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'size3Type^invalid'
	   FROM my_temp_pd_part_number tmp
	  WHERE tmp.c901_size3_type     IS NOT NULL
	    AND tmp.c901_size3_type NOT IN
	    (
	         SELECT TO_CHAR(C901_CODE_ID)
	           FROM T901_CODE_LOOKUP
	          WHERE C901_CODE_GRP IN ('PDSIZE')
	            AND C901_ACTIVE_FL = '1'
	    ) ;        
     -- empty check the size 1 text filed
     INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'size3Text^mandatory'
       FROM my_temp_pd_part_number
      WHERE c901_size3_type        = '104047'
        AND c205g_size3_type_text IS NULL;
      -- empty check the size 1 text filed
     INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
      SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'size3Text^invalid'
       FROM my_temp_pd_part_number
      WHERE (c901_size3_type IS NULL OR c901_size3_type <> '104047')
        AND c205g_size3_type_text IS NOT NULL;
        
   -- validate the size 1 value
     INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'size3Value^invalid'
       FROM my_temp_pd_part_number
      WHERE c901_size3_type IS NOT NULL
        AND (REGEXP_LIKE (c205g_size3_value, '[^0-9/./-]+'))
        OR c205g_size3_value = '0'
UNION ALL
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'size3Value^invalid'
     FROM my_temp_pd_part_number
     WHERE c901_size3_type IS NULL
     AND c205g_size3_value IS NOT NULL;
     -- validate the size 1 value
     INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'size3Value^mandatory'
       FROM my_temp_pd_part_number
      WHERE c901_size3_type IS NOT NULL
      AND c901_size3_type <>'104047' -- To Avoid Type Others
        AND (c205g_size3_value IS NULL);   
    -- size 1 uom
     INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'uom3^invalid'
       FROM my_temp_pd_part_number
      WHERE c901_size3_type IS NOT NULL
        AND (REGEXP_LIKE (c901_size3_uom, '[^0-9]+'))
        UNION
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'uom3^invalid'
       FROM my_temp_pd_part_number
       WHERE c901_size3_type IS NULL
       AND c901_size3_uom IS NOT NULL
       UNION
       SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'uom3^invalid'
	   FROM my_temp_pd_part_number tmp
	  WHERE tmp.c901_size3_uom     IS NOT NULL
	    AND tmp.c901_size3_uom NOT IN
	    (
	         SELECT TO_CHAR(C901_CODE_ID)
	           FROM T901_CODE_LOOKUP
	          WHERE C901_CODE_GRP IN ('PDUOM')
	            AND C901_ACTIVE_FL = '1'
	    ) ;
    --
    -- size 1 uom
     INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'uom3^mandatory'
       FROM my_temp_pd_part_number
      WHERE c901_size3_type IS NOT NULL
       AND c901_size3_type <>'104047' -- To Avoid Type Others
        AND c901_size3_uom IS NULL;
            -- size 1 uom
            
        INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'uom3^uomempty'
       FROM my_temp_pd_part_number
      WHERE c901_size3_type IS NOT NULL
        AND c901_size3_uom IS NOT NULL
        AND c901_size3_type = '104047';

        
    -- invalid size 4 type
     INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'size4Type^invalid'
       FROM my_temp_pd_part_number
      WHERE REGEXP_LIKE (c901_size4_type, '[^0-9]+')
       UNION
       SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'size4Type^invalid'
	   FROM my_temp_pd_part_number tmp
	  WHERE tmp.c901_size4_type     IS NOT NULL
	    AND tmp.c901_size4_type NOT IN
	    (
	         SELECT TO_CHAR(C901_CODE_ID)
	           FROM T901_CODE_LOOKUP
	          WHERE C901_CODE_GRP IN ('PDSIZE')
	            AND C901_ACTIVE_FL = '1'
	    ) ;
    -- empty check the size 1 text filed
     INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'size4Text^mandatory'
       FROM my_temp_pd_part_number
      WHERE c901_size4_type        = '104047'
        AND c205g_size4_type_text IS NULL;
      -- empty check the size 1 text filed
     INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
      SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'size4Text^invalid'
       FROM my_temp_pd_part_number
      WHERE (c901_size4_type IS NULL OR c901_size4_type <> '104047')
        AND c205g_size4_type_text IS NOT NULL;
        
   -- validate the size 1 value
     INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'size4Value^invalid'
       FROM my_temp_pd_part_number
      WHERE c901_size4_type IS NOT NULL
        AND (REGEXP_LIKE (c205g_size4_value, '[^0-9/./-]+'))
        OR c205g_size4_value = '0'
UNION ALL
         
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'size4Value^invalid'
     FROM my_temp_pd_part_number
     WHERE c901_size4_type IS NULL
     AND c205g_size4_value IS NOT NULL;
     -- validate the size 1 value
     INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
   
        
          SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'size4Value^mandatory'
       FROM my_temp_pd_part_number
      WHERE c901_size4_type IS NOT NULL
       AND c901_size4_type <>'104047' -- To Avoid Type Others
        AND (c205g_size4_value IS NULL); 
    -- size 1 uom
     INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'uom4^invalid'
       FROM my_temp_pd_part_number
      WHERE c901_size4_type IS NOT NULL
        AND (REGEXP_LIKE (c901_size4_uom, '[^0-9]+'))
        UNION
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'uom4^invalid'
       FROM my_temp_pd_part_number
       WHERE c901_size4_type IS NULL
       AND c901_size4_uom IS NOT NULL
       UNION
       SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'uom4^invalid'
	   FROM my_temp_pd_part_number tmp
	  WHERE tmp.c901_size4_uom     IS NOT NULL
	    AND tmp.c901_size4_uom NOT IN
	    (
	         SELECT TO_CHAR(C901_CODE_ID)
	           FROM T901_CODE_LOOKUP
	          WHERE C901_CODE_GRP IN ('PDUOM')
	            AND C901_ACTIVE_FL = '1'
	    ) ;
    --
    -- size 1 uom
INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'uom4^uomempty'
       FROM my_temp_pd_part_number
      WHERE c901_size4_type IS NOT NULL
        AND c901_size4_uom IS NOT NULL
        AND c901_size4_type = '104047';

        
            -- size 1 uom
     INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'uom4^mandatory'
       FROM my_temp_pd_part_number
      WHERE c901_size4_type IS NOT NULL
      AND c901_size4_type <> '104047' -- To Avoid Type Others
        AND c901_size4_uom IS NULL;
    --
    
        
     SELECT COUNT (1)
       INTO v_cnt
       FROM my_temp_key_value
      WHERE my_temp_txn_value like 'size1Type%'
      OR my_temp_txn_value like 'uom1%';
    IF v_cnt                   = 0 THEN
        -- size 1 uom type validate
         INSERT
           INTO my_temp_key_value
            (
                MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
            )
         SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'uom1^invalid'
           FROM my_temp_pd_part_number tmp
          WHERE tmp.c901_size1_type    IN ('104032', '104033', '104035', '104036', '104037', '104038', '104041', '104044')
            AND tmp.c901_size1_uom     IS NOT NULL
            AND tmp.c901_size1_uom NOT IN
            (
                 SELECT C901_CODE_ID CODEID
                   FROM T901_CODE_LOOKUP
                  WHERE C901_CODE_GRP IN ('PDUOM')
                    AND C901_ACTIVE_FL = '1'
                    AND c901_code_id  IN
                    (
                         SELECT T901A.C901_CODE_ID
                           FROM T901A_LOOKUP_ACCESS T901A
                          WHERE T901A.C901_ACCESS_CODE_ID IN (104037)
                    )
            )
      UNION ALL
         SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'uom1^invalid'
           FROM my_temp_pd_part_number tmp
          WHERE tmp.c901_size1_type    IN ('104045')
            AND tmp.c901_size1_uom     IS NOT NULL
            AND tmp.c901_size1_uom NOT IN
            (
                 SELECT C901_CODE_ID CODEID
                   FROM T901_CODE_LOOKUP
                  WHERE C901_CODE_GRP IN ('PDUOM')
                    AND C901_ACTIVE_FL = '1'
                    AND c901_code_id  IN
                    (
                         SELECT T901A.C901_CODE_ID
                           FROM T901A_LOOKUP_ACCESS T901A
                          WHERE T901A.C901_ACCESS_CODE_ID IN (104045)
                    )
            )
      UNION ALL
         SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'uom1^invalid'
           FROM my_temp_pd_part_number tmp
          WHERE tmp.c901_size1_type    IS NOT NULL
            AND tmp.c901_size1_type    IN ('104045')
            AND tmp.c901_size1_uom     IS NOT NULL
            AND tmp.c901_size1_uom NOT IN
            (
                 SELECT C901_CODE_ID CODEID
                   FROM T901_CODE_LOOKUP
                  WHERE C901_CODE_GRP IN ('PDUOM')
                    AND C901_ACTIVE_FL = '1'
                    AND c901_code_id  IN
                    (
                         SELECT T901A.C901_CODE_ID
                           FROM T901A_LOOKUP_ACCESS T901A
                          WHERE T901A.C901_ACCESS_CODE_ID IN (104045)
                    )
            )
      UNION ALL
         SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'uom1^invalid'
           FROM my_temp_pd_part_number tmp
          WHERE tmp.c901_size1_type    IS NOT NULL
            AND tmp.c901_size1_type    IN ('104042')
            AND tmp.c901_size1_uom     IS NOT NULL
            AND tmp.c901_size1_uom NOT IN
            (
                 SELECT C901_CODE_ID CODEID
                   FROM T901_CODE_LOOKUP
                  WHERE C901_CODE_GRP IN ('PDUOM')
                    AND C901_ACTIVE_FL = '1'
                    AND c901_code_id  IN
                    (
                         SELECT T901A.C901_CODE_ID
                           FROM T901A_LOOKUP_ACCESS T901A
                          WHERE T901A.C901_ACCESS_CODE_ID IN (104042)
                    )
            )
      UNION ALL
         SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'uom1^invalid'
           FROM my_temp_pd_part_number tmp
          WHERE tmp.c901_size1_type    IS NOT NULL
            AND tmp.c901_size1_type    IN ('104040')
            AND tmp.c901_size1_uom     IS NOT NULL
            AND tmp.c901_size1_uom NOT IN
            (
                 SELECT C901_CODE_ID CODEID
                   FROM T901_CODE_LOOKUP
                  WHERE C901_CODE_GRP IN ('PDUOM')
                    AND C901_ACTIVE_FL = '1'
                    AND c901_code_id  IN
                    (
                         SELECT T901A.C901_CODE_ID
                           FROM T901A_LOOKUP_ACCESS T901A
                          WHERE T901A.C901_ACCESS_CODE_ID IN (104040)
                    )
            )
      UNION ALL
         SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'uom1^invalid'
           FROM my_temp_pd_part_number tmp
          WHERE tmp.c901_size1_type    IS NOT NULL
            AND tmp.c901_size1_type    IN ('104034','104039')
            AND tmp.c901_size1_uom     IS NOT NULL
            AND tmp.c901_size1_uom NOT IN
            (
                 SELECT C901_CODE_ID CODEID
                   FROM T901_CODE_LOOKUP
                  WHERE C901_CODE_GRP IN ('PDUOM')
                    AND C901_ACTIVE_FL = '1'
                    AND c901_code_id  IN
                    (
                         SELECT T901A.C901_CODE_ID
                           FROM T901A_LOOKUP_ACCESS T901A
                          WHERE T901A.C901_ACCESS_CODE_ID IN (104034)
                    )
            )
      UNION ALL
         SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'uom1^invalid'
           FROM my_temp_pd_part_number tmp
          WHERE tmp.c901_size1_type    IS NOT NULL
            AND tmp.c901_size1_type    IN ('104046')
            AND tmp.c901_size1_uom     IS NOT NULL
            AND tmp.c901_size1_uom NOT IN
            (
                 SELECT C901_CODE_ID CODEID
                   FROM T901_CODE_LOOKUP
                  WHERE C901_CODE_GRP IN ('PDUOM')
                    AND C901_ACTIVE_FL = '1'
                    AND c901_code_id  IN
                    (
                         SELECT T901A.C901_CODE_ID
                           FROM T901A_LOOKUP_ACCESS T901A
                          WHERE T901A.C901_ACCESS_CODE_ID IN (104046)
                    )
            )
      UNION ALL
         SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'uom1^invalid'
           FROM my_temp_pd_part_number tmp
          WHERE tmp.c901_size1_type    IS NOT NULL
            AND tmp.c901_size1_type    IN ('104043')
            AND tmp.c901_size1_uom     IS NOT NULL
            AND tmp.c901_size1_uom NOT IN
            (
                 SELECT C901_CODE_ID CODEID
                   FROM T901_CODE_LOOKUP
                  WHERE C901_CODE_GRP IN ('PDUOM')
                    AND C901_ACTIVE_FL = '1'
                    AND c901_code_id  IN
                    (
                         SELECT T901A.C901_CODE_ID
                           FROM T901A_LOOKUP_ACCESS T901A
                          WHERE T901A.C901_ACCESS_CODE_ID IN (104043)
                    )
            ) ;
    END IF;
     SELECT COUNT (1)
       INTO v_cnt
       FROM my_temp_key_value
      WHERE my_temp_txn_value LIKE 'size2Type%'
      OR my_temp_txn_value LIKE 'uom2%';
    IF v_cnt                   = 0 THEN
        --size 2 uom filed validate
         INSERT
           INTO my_temp_key_value
            (
                MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
            )
         SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'uom2^invalid'
           FROM my_temp_pd_part_number tmp
          WHERE tmp.c901_size2_type    IN ('104032', '104033', '104035', '104036', '104037', '104038', '104041', '104044')
            AND tmp.c901_size2_uom     IS NOT NULL
            AND tmp.c901_size2_uom NOT IN
            (
                 SELECT C901_CODE_ID CODEID
                   FROM T901_CODE_LOOKUP
                  WHERE C901_CODE_GRP IN ('PDUOM')
                    AND C901_ACTIVE_FL = '1'
                    AND c901_code_id  IN
                    (
                         SELECT T901A.C901_CODE_ID
                           FROM T901A_LOOKUP_ACCESS T901A
                          WHERE T901A.C901_ACCESS_CODE_ID IN (104037)
                    )
            )
      UNION ALL
         SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'uom2^invalid'
           FROM my_temp_pd_part_number tmp
          WHERE tmp.c901_size2_type    IN ('104045')
            AND tmp.c901_size2_uom     IS NOT NULL
            AND tmp.c901_size2_uom NOT IN
            (
                 SELECT C901_CODE_ID CODEID
                   FROM T901_CODE_LOOKUP
                  WHERE C901_CODE_GRP IN ('PDUOM')
                    AND C901_ACTIVE_FL = '1'
                    AND c901_code_id  IN
                    (
                         SELECT T901A.C901_CODE_ID
                           FROM T901A_LOOKUP_ACCESS T901A
                          WHERE T901A.C901_ACCESS_CODE_ID IN (104045)
                    )
            )
      UNION ALL
         SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'uom2^invalid'
           FROM my_temp_pd_part_number tmp
          WHERE tmp.c901_size2_type    IS NOT NULL
            AND tmp.c901_size2_type    IN ('104045')
            AND tmp.c901_size2_uom     IS NOT NULL
            AND tmp.c901_size2_uom NOT IN
            (
                 SELECT C901_CODE_ID CODEID
                   FROM T901_CODE_LOOKUP
                  WHERE C901_CODE_GRP IN ('PDUOM')
                    AND C901_ACTIVE_FL = '1'
                    AND c901_code_id  IN
                    (
                         SELECT T901A.C901_CODE_ID
                           FROM T901A_LOOKUP_ACCESS T901A
                          WHERE T901A.C901_ACCESS_CODE_ID IN (104045)
                    )
            )
      UNION ALL
         SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'uom2^invalid'
           FROM my_temp_pd_part_number tmp
          WHERE tmp.c901_size2_type    IS NOT NULL
            AND tmp.c901_size2_type    IN ('104042')
            AND tmp.c901_size2_uom     IS NOT NULL
            AND tmp.c901_size2_uom NOT IN
            (
                 SELECT C901_CODE_ID CODEID
                   FROM T901_CODE_LOOKUP
                  WHERE C901_CODE_GRP IN ('PDUOM')
                    AND C901_ACTIVE_FL = '1'
                    AND c901_code_id  IN
                    (
                         SELECT T901A.C901_CODE_ID
                           FROM T901A_LOOKUP_ACCESS T901A
                          WHERE T901A.C901_ACCESS_CODE_ID IN (104042)
                    )
            )
      UNION ALL
         SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'uom2^invalid'
           FROM my_temp_pd_part_number tmp
          WHERE tmp.c901_size2_type    IS NOT NULL
            AND tmp.c901_size2_type    IN ('104040')
            AND tmp.c901_size2_uom     IS NOT NULL
            AND tmp.c901_size2_uom NOT IN
            (
                 SELECT C901_CODE_ID CODEID
                   FROM T901_CODE_LOOKUP
                  WHERE C901_CODE_GRP IN ('PDUOM')
                    AND C901_ACTIVE_FL = '1'
                    AND c901_code_id  IN
                    (
                         SELECT T901A.C901_CODE_ID
                           FROM T901A_LOOKUP_ACCESS T901A
                          WHERE T901A.C901_ACCESS_CODE_ID IN (104040)
                    )
            )
      UNION ALL
         SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'uom2^invalid'
           FROM my_temp_pd_part_number tmp
          WHERE tmp.c901_size2_type    IS NOT NULL
            AND tmp.c901_size2_type    IN ('104034','104039')
            AND tmp.c901_size2_uom     IS NOT NULL
            AND tmp.c901_size2_uom NOT IN
            (
                 SELECT C901_CODE_ID CODEID
                   FROM T901_CODE_LOOKUP
                  WHERE C901_CODE_GRP IN ('PDUOM')
                    AND C901_ACTIVE_FL = '1'
                    AND c901_code_id  IN
                    (
                         SELECT T901A.C901_CODE_ID
                           FROM T901A_LOOKUP_ACCESS T901A
                          WHERE T901A.C901_ACCESS_CODE_ID IN (104034)
                    )
            )
      UNION ALL
         SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'uom2^invalid'
           FROM my_temp_pd_part_number tmp
          WHERE tmp.c901_size2_type    IS NOT NULL
            AND tmp.c901_size2_type    IN ('104046')
            AND tmp.c901_size2_uom     IS NOT NULL
            AND tmp.c901_size2_uom NOT IN
            (
                 SELECT C901_CODE_ID CODEID
                   FROM T901_CODE_LOOKUP
                  WHERE C901_CODE_GRP IN ('PDUOM')
                    AND C901_ACTIVE_FL = '1'
                    AND c901_code_id  IN
                    (
                         SELECT T901A.C901_CODE_ID
                           FROM T901A_LOOKUP_ACCESS T901A
                          WHERE T901A.C901_ACCESS_CODE_ID IN (104046)
                    )
            )
      UNION ALL
         SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'uom2^invalid'
           FROM my_temp_pd_part_number tmp
          WHERE tmp.c901_size2_type    IS NOT NULL
            AND tmp.c901_size2_type    IN ('104043')
            AND tmp.c901_size2_uom     IS NOT NULL
            AND tmp.c901_size2_uom NOT IN
            (
                 SELECT C901_CODE_ID CODEID
                   FROM T901_CODE_LOOKUP
                  WHERE C901_CODE_GRP IN ('PDUOM')
                    AND C901_ACTIVE_FL = '1'
                    AND c901_code_id  IN
                    (
                         SELECT T901A.C901_CODE_ID
                           FROM T901A_LOOKUP_ACCESS T901A
                          WHERE T901A.C901_ACCESS_CODE_ID IN (104043)
                    )
            ) ;
    END IF;
     SELECT COUNT (1)
       INTO v_cnt
       FROM my_temp_key_value
      WHERE my_temp_txn_value LIKE 'size3Type%'
      OR my_temp_txn_value LIKE 'uom3%' ;
    IF v_cnt                   = 0 THEN
        -- size 3 uom type validate
         INSERT
           INTO my_temp_key_value
            (
                MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
            )
         SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'uom3^invalid'
           FROM my_temp_pd_part_number tmp
          WHERE tmp.c901_size3_type    IN ('104032', '104033', '104035', '104036', '104037', '104038', '104041', '104044')
            AND tmp.c901_size3_uom     IS NOT NULL
            AND tmp.c901_size3_uom NOT IN
            (
                 SELECT C901_CODE_ID CODEID
                   FROM T901_CODE_LOOKUP
                  WHERE C901_CODE_GRP IN ('PDUOM')
                    AND C901_ACTIVE_FL = '1'
                    AND c901_code_id  IN
                    (
                         SELECT T901A.C901_CODE_ID
                           FROM T901A_LOOKUP_ACCESS T901A
                          WHERE T901A.C901_ACCESS_CODE_ID IN (104037)
                    )
            )
      UNION ALL
         SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'uom3^invalid'
           FROM my_temp_pd_part_number tmp
          WHERE tmp.c901_size3_type    IN ('104045')
            AND tmp.c901_size3_uom     IS NOT NULL
            AND tmp.c901_size3_uom NOT IN
            (
                 SELECT C901_CODE_ID CODEID
                   FROM T901_CODE_LOOKUP
                  WHERE C901_CODE_GRP IN ('PDUOM')
                    AND C901_ACTIVE_FL = '1'
                    AND c901_code_id  IN
                    (
                         SELECT T901A.C901_CODE_ID
                           FROM T901A_LOOKUP_ACCESS T901A
                          WHERE T901A.C901_ACCESS_CODE_ID IN (104045)
                    )
            )
      UNION ALL
         SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'uom3^invalid'
           FROM my_temp_pd_part_number tmp
          WHERE tmp.c901_size3_type    IS NOT NULL
            AND tmp.c901_size3_type    IN ('104045')
            AND tmp.c901_size3_uom     IS NOT NULL
            AND tmp.c901_size3_uom NOT IN
            (
                 SELECT C901_CODE_ID CODEID
                   FROM T901_CODE_LOOKUP
                  WHERE C901_CODE_GRP IN ('PDUOM')
                    AND C901_ACTIVE_FL = '1'
                    AND c901_code_id  IN
                    (
                         SELECT T901A.C901_CODE_ID
                           FROM T901A_LOOKUP_ACCESS T901A
                          WHERE T901A.C901_ACCESS_CODE_ID IN (104045)
                    )
            )
      UNION ALL
         SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'uom3^invalid'
           FROM my_temp_pd_part_number tmp
          WHERE tmp.c901_size3_type    IS NOT NULL
            AND tmp.c901_size3_type    IN ('104042')
            AND tmp.c901_size3_uom     IS NOT NULL
            AND tmp.c901_size3_uom NOT IN
            (
                 SELECT C901_CODE_ID CODEID
                   FROM T901_CODE_LOOKUP
                  WHERE C901_CODE_GRP IN ('PDUOM')
                    AND C901_ACTIVE_FL = '1'
                    AND c901_code_id  IN
                    (
                         SELECT T901A.C901_CODE_ID
                           FROM T901A_LOOKUP_ACCESS T901A
                          WHERE T901A.C901_ACCESS_CODE_ID IN (104042)
                    )
            )
      UNION ALL
         SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'uom3^invalid'
           FROM my_temp_pd_part_number tmp
          WHERE tmp.c901_size3_type    IS NOT NULL
            AND tmp.c901_size3_type    IN ('104040')
            AND tmp.c901_size3_uom     IS NOT NULL
            AND tmp.c901_size3_uom NOT IN
            (
                 SELECT C901_CODE_ID CODEID
                   FROM T901_CODE_LOOKUP
                  WHERE C901_CODE_GRP IN ('PDUOM')
                    AND C901_ACTIVE_FL = '1'
                    AND c901_code_id  IN
                    (
                         SELECT T901A.C901_CODE_ID
                           FROM T901A_LOOKUP_ACCESS T901A
                          WHERE T901A.C901_ACCESS_CODE_ID IN (104040)
                    )
            )
      UNION ALL
         SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'uom3^invalid'
           FROM my_temp_pd_part_number tmp
          WHERE tmp.c901_size3_type    IS NOT NULL
            AND tmp.c901_size3_type    IN ('104034','104039')
            AND tmp.c901_size3_uom     IS NOT NULL
            AND tmp.c901_size3_uom NOT IN
            (
                 SELECT C901_CODE_ID CODEID
                   FROM T901_CODE_LOOKUP
                  WHERE C901_CODE_GRP IN ('PDUOM')
                    AND C901_ACTIVE_FL = '1'
                    AND c901_code_id  IN
                    (
                         SELECT T901A.C901_CODE_ID
                           FROM T901A_LOOKUP_ACCESS T901A
                          WHERE T901A.C901_ACCESS_CODE_ID IN (104034)
                    )
            )
      UNION ALL
         SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'uom3^invalid'
           FROM my_temp_pd_part_number tmp
          WHERE tmp.c901_size3_type    IS NOT NULL
            AND tmp.c901_size3_type    IN ('104046')
            AND tmp.c901_size3_uom NOT IN
            (
                 SELECT C901_CODE_ID CODEID
                   FROM T901_CODE_LOOKUP
                  WHERE C901_CODE_GRP IN ('PDUOM')
                    AND C901_ACTIVE_FL = '1'
                    AND c901_code_id  IN
                    (
                         SELECT T901A.C901_CODE_ID
                           FROM T901A_LOOKUP_ACCESS T901A
                          WHERE T901A.C901_ACCESS_CODE_ID IN (104046)
                    )
            )
      UNION ALL
         SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'uom3^invalid'
           FROM my_temp_pd_part_number tmp
          WHERE tmp.c901_size3_type    IS NOT NULL
            AND tmp.c901_size3_type    IN ('104043')
            AND tmp.c901_size3_uom     IS NOT NULL
            AND tmp.c901_size3_uom NOT IN
            (
                 SELECT C901_CODE_ID CODEID
                   FROM T901_CODE_LOOKUP
                  WHERE C901_CODE_GRP IN ('PDUOM')
                    AND C901_ACTIVE_FL = '1'
                    AND c901_code_id  IN
                    (
                         SELECT T901A.C901_CODE_ID
                           FROM T901A_LOOKUP_ACCESS T901A
                          WHERE T901A.C901_ACCESS_CODE_ID IN (104043)
                    )
            ) ;
    END IF;
    -- size 4 uom filed validate
     SELECT COUNT (1)
       INTO v_cnt
       FROM my_temp_key_value
      WHERE my_temp_txn_value LIKE 'size4Type%'
      OR my_temp_txn_value LIKE 'uom4%' ;
    IF v_cnt                   = 0 THEN
         INSERT
           INTO my_temp_key_value
            (
                MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
            )
         SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'uom4^invalid'
           FROM my_temp_pd_part_number tmp
          WHERE tmp.c901_size4_type    IN ('104032', '104033', '104035', '104036', '104037', '104038', '104041', '104044')
            AND tmp.c901_size4_uom     IS NOT NULL
            AND tmp.c901_size4_uom NOT IN
            (
                 SELECT C901_CODE_ID CODEID
                   FROM T901_CODE_LOOKUP
                  WHERE C901_CODE_GRP IN ('PDUOM')
                    AND C901_ACTIVE_FL = '1'
                    AND c901_code_id  IN
                    (
                         SELECT T901A.C901_CODE_ID
                           FROM T901A_LOOKUP_ACCESS T901A
                          WHERE T901A.C901_ACCESS_CODE_ID IN (104037)
                    )
            )
      UNION ALL
         SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'uom4^invalid'
           FROM my_temp_pd_part_number tmp
          WHERE tmp.c901_size4_type    IN ('104045')
            AND tmp.c901_size4_uom     IS NOT NULL
            AND tmp.c901_size4_uom NOT IN
            (
                 SELECT C901_CODE_ID CODEID
                   FROM T901_CODE_LOOKUP
                  WHERE C901_CODE_GRP IN ('PDUOM')
                    AND C901_ACTIVE_FL = '1'
                    AND c901_code_id  IN
                    (
                         SELECT T901A.C901_CODE_ID
                           FROM T901A_LOOKUP_ACCESS T901A
                          WHERE T901A.C901_ACCESS_CODE_ID IN (104045)
                    )
            )
      UNION ALL
         SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'uom4^invalid'
           FROM my_temp_pd_part_number tmp
          WHERE tmp.c901_size4_type    IS NOT NULL
            AND tmp.c901_size4_type    IN ('104045')
            AND tmp.c901_size4_uom     IS NOT NULL
            AND tmp.c901_size4_uom NOT IN
            (
                 SELECT C901_CODE_ID CODEID
                   FROM T901_CODE_LOOKUP
                  WHERE C901_CODE_GRP IN ('PDUOM')
                    AND C901_ACTIVE_FL = '1'
                    AND c901_code_id  IN
                    (
                         SELECT T901A.C901_CODE_ID
                           FROM T901A_LOOKUP_ACCESS T901A
                          WHERE T901A.C901_ACCESS_CODE_ID IN (104045)
                    )
            )
      UNION ALL
         SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'uom4^invalid'
           FROM my_temp_pd_part_number tmp
          WHERE tmp.c901_size4_type    IS NOT NULL
            AND tmp.c901_size4_type    IN ('104042')
            AND tmp.c901_size4_uom     IS NOT NULL
            AND tmp.c901_size4_uom NOT IN
            (
                 SELECT C901_CODE_ID CODEID
                   FROM T901_CODE_LOOKUP
                  WHERE C901_CODE_GRP IN ('PDUOM')
                    AND C901_ACTIVE_FL = '1'
                    AND c901_code_id  IN
                    (
                         SELECT T901A.C901_CODE_ID
                           FROM T901A_LOOKUP_ACCESS T901A
                          WHERE T901A.C901_ACCESS_CODE_ID IN (104042)
                    )
            )
      UNION ALL
         SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'uom4^invalid'
           FROM my_temp_pd_part_number tmp
          WHERE tmp.c901_size4_type    IS NOT NULL
            AND tmp.c901_size4_type    IN ('104040')
            AND tmp.c901_size4_uom     IS NOT NULL
            AND tmp.c901_size4_uom NOT IN
            (
                 SELECT C901_CODE_ID CODEID
                   FROM T901_CODE_LOOKUP
                  WHERE C901_CODE_GRP IN ('PDUOM')
                    AND C901_ACTIVE_FL = '1'
                    AND c901_code_id  IN
                    (
                         SELECT T901A.C901_CODE_ID
                           FROM T901A_LOOKUP_ACCESS T901A
                          WHERE T901A.C901_ACCESS_CODE_ID IN (104040)
                    )
            )
      UNION ALL
         SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'uom4^invalid'
           FROM my_temp_pd_part_number tmp
          WHERE tmp.c901_size4_type    IS NOT NULL
            AND tmp.c901_size4_type    IN ('104034','104039')
            AND tmp.c901_size4_uom     IS NOT NULL
            AND tmp.c901_size4_uom NOT IN
            (
                 SELECT C901_CODE_ID CODEID
                   FROM T901_CODE_LOOKUP
                  WHERE C901_CODE_GRP IN ('PDUOM')
                    AND C901_ACTIVE_FL = '1'
                    AND c901_code_id  IN
                    (
                         SELECT T901A.C901_CODE_ID
                           FROM T901A_LOOKUP_ACCESS T901A
                          WHERE T901A.C901_ACCESS_CODE_ID IN (104034)
                    )
            )
      UNION ALL
         SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'uom4^invalid'
           FROM my_temp_pd_part_number tmp
          WHERE tmp.c901_size4_type    IS NOT NULL
            AND tmp.c901_size4_type    IN ('104046')
            AND tmp.c901_size4_uom     IS NOT NULL
            AND tmp.c901_size4_uom NOT IN
            (
                 SELECT C901_CODE_ID CODEID
                   FROM T901_CODE_LOOKUP
                  WHERE C901_CODE_GRP IN ('PDUOM')
                    AND C901_ACTIVE_FL = '1'
                    AND c901_code_id  IN
                    (
                         SELECT T901A.C901_CODE_ID
                           FROM T901A_LOOKUP_ACCESS T901A
                          WHERE T901A.C901_ACCESS_CODE_ID IN (104046)
                    )
            )
      UNION ALL
         SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'uom4^invalid'
           FROM my_temp_pd_part_number tmp
          WHERE tmp.c901_size4_type    IS NOT NULL
            AND tmp.c901_size4_type    IN ('104043')
            AND tmp.c901_size4_uom     IS NOT NULL
            AND tmp.c901_size4_uom NOT IN
            (
                 SELECT C901_CODE_ID CODEID
                   FROM T901_CODE_LOOKUP
                  WHERE C901_CODE_GRP IN ('PDUOM')
                    AND C901_ACTIVE_FL = '1'
                    AND c901_code_id  IN
                    (
                         SELECT T901A.C901_CODE_ID
                           FROM T901A_LOOKUP_ACCESS T901A
                          WHERE T901A.C901_ACCESS_CODE_ID IN (104043)
                    )
            ) ;
    END IF;
    --
   -- validate the size value filed (0.00xxxxx)
	 SELECT COUNT (1)
	   INTO v_cnt
	   FROM my_temp_key_value
	  WHERE my_temp_txn_value = 'size1Value^invalid';
	--
	IF v_cnt = 0 THEN
	     INSERT
	       INTO my_temp_key_value
	        (
	            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
	        )
	     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'size1Value^invalid'
	       FROM my_temp_pd_part_number
	      WHERE to_number (ROUND (c205g_size1_value, 2)) = 0;
	END IF;
	--
	 SELECT COUNT (1)
	   INTO v_cnt
	   FROM my_temp_key_value
	  WHERE my_temp_txn_value = 'size2Value^invalid';
	--
	IF v_cnt = 0 THEN
	     INSERT
	       INTO my_temp_key_value
	        (
	            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
	        )
	     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'size2Value^invalid'
	       FROM my_temp_pd_part_number
	      WHERE to_number (ROUND (c205g_size2_value, 2)) = 0;
	END IF;
	--
	 SELECT COUNT (1)
	   INTO v_cnt
	   FROM my_temp_key_value
	  WHERE my_temp_txn_value = 'size3Value^invalid';
	--
	IF v_cnt = 0 THEN
	     INSERT
	       INTO my_temp_key_value
	        (
	            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
	        )
	     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'size3Value^invalid'
	       FROM my_temp_pd_part_number
	      WHERE to_number (ROUND (c205g_size3_value, 2)) = 0;
	END IF;
	--
	 SELECT COUNT (1)
	   INTO v_cnt
	   FROM my_temp_key_value
	  WHERE my_temp_txn_value = 'size4Value^invalid';
	--
	IF v_cnt = 0 THEN
	     INSERT
	       INTO my_temp_key_value
	        (
	            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
	        )
	     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'size4Value^invalid'
	       FROM my_temp_pd_part_number
	      WHERE to_number (ROUND (c205g_size4_value, 2)) = 0;
	END IF;
	
	--size value code here
	
	     INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'size1Value^valueempty'
       FROM my_temp_pd_part_number
      WHERE c901_size1_type IS NOT NULL
       AND c901_size1_type = '104047'
        AND c205g_size1_value IS NOT NULL;
	--
	
	     INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'size2Value^valueempty'
       FROM my_temp_pd_part_number
      WHERE c901_size2_type IS NOT NULL
   AND c901_size2_type = '104047'
        AND c205g_size2_value IS NOT NULL;

	--
	     INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'size3Value^valueempty'
       FROM my_temp_pd_part_number
      WHERE c901_size3_type IS NOT NULL
   AND c901_size3_type = '104047'
        AND c205g_size3_value IS NOT NULL;
	--
	     INSERT
       INTO my_temp_key_value
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
        )
     SELECT c205g_dhtmlx_rowid, c205_part_number_id, 'size4Value^valueempty'
       FROM my_temp_pd_part_number
      WHERE c901_size4_type IS NOT NULL
   AND c901_size4_type = '104047'
      AND c205g_size4_value IS NOT NULL;
	--
    BEGIN
         SELECT RTRIM (XMLAGG (XMLELEMENT (N, errcolumn || '|') .EXTRACT ('//text()')) .GetClobVal (), ', ')
           INTO v_final_err
           FROM
            (
                 SELECT temp.my_temp_txn_id ||'^'|| RTRIM (XMLAGG (XMLELEMENT (N, temp.my_temp_txn_value || '^'))
                    .EXTRACT ('//text()'), ', ') errcolumn
                   FROM my_temp_key_value temp
                  WHERE my_temp_txn_id IS NOT NULL
               GROUP BY my_temp_txn_id
               ORDER BY temp.my_temp_txn_id
            ) ;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_final_err := NULL;
    END;
    --
    IF v_final_err IS NOT NULL THEN
        p_out_err  := v_final_err;
    END IF;
    --
END gm_validate_pd_part_number;
        
END gm_pkg_pd_sav_udi;
/
