/* Formatted on 2009/02/16 12:30 (Formatter Plus v4.8.0) */
--@"C:\Database\Packages\ProdDevelopement\productcatalog\gm_pkg_pdpc_prodcatrpt.bdy";

CREATE OR REPLACE PACKAGE BODy gm_pkg_pdpc_prodcatrpt
IS
	/*************************************************************************************************
	 * Description :  This procedure will set the input p_sync_type id's from the device last sync time.
	 * Example     :  This procedure will check is there any modified parts avaiable after last sync for
	 *                parts for given device. If updated parts available, then it will add all updated
	 *                parts into temp table.
	**************************************************************************************************/
	
	PROCEDURE gm_fch_device_updates (
		p_device_infoid  IN  t9151_device_sync_dtl.c9150_device_info_id%TYPE
	  , p_language_id    IN  t9151_device_sync_dtl.c901_language_id%TYPE
	  , p_sync_type      IN  t9151_device_sync_dtl.c901_sync_type%TYPE
	  , p_sync_count     OUT NUMBER
	)
	AS 
	
	v_last_rec	 t9151_device_sync_dtl.C9151_LAST_SYNC_REC%TYPE ;

	BEGIN
		gm_fch_device_updates(p_device_infoid,p_language_id,p_sync_type,NULL,p_sync_count,v_last_rec);
	END gm_fch_device_updates;
	/*************************************************************************************************
	 * Description :  This procedure will set the input p_sync_type id's from the device last sync time.
	 * Example     :  This procedure will check is there any modified parts avaiable after last sync for
	 *                parts for given device. If updated parts available, then it will add all updated
	 *                parts into temp table and return last synced page's last record key.
	**************************************************************************************************/	
	PROCEDURE gm_fch_device_updates(
		p_device_infoid  IN  t9151_device_sync_dtl.c9150_device_info_id%TYPE
	  , p_language_id    IN  t9151_device_sync_dtl.c901_language_id%TYPE
	  , p_sync_type      IN  t9151_device_sync_dtl.c901_sync_type%TYPE
	  , p_pageno	    IN  NUMBER
	  , p_sync_count     OUT NUMBER
	  , p_last_rec	     OUT t9151_device_sync_dtl.C9151_LAST_SYNC_REC%TYPE 
	)
	AS 
	
	v_update_count    NUMBER := 0;
	v_hist_sync_count NUMBER := 0;
	v_lang_cnt		  NUMBER := 0;

	v_company_id   	t1900_company.c1900_company_id%TYPE; 
	BEGIN
	SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL')) INTO v_company_id FROM dual; 
		
		
		
	SELECT COUNT (1) INTO v_lang_cnt
	  FROM T901_Code_Lookup
	 WHERE C901_CODE_ID   = p_language_id
	   AND C901_Code_Grp  = 'MLANG'
	   AND C901_ACTIVE_FL = 1
	   AND C901_VOID_FL  IS NULL;	
	   
	IF v_lang_cnt = 0
	THEN
		raise_application_error (-20606, '');  --No Language Defined
	END IF;	
	
	/*When t9151_device_sync_dtl is having sync record and  t2001_master_data_updates having no updates to sync,
	  that time only we need to do Master Sync. so below count is taken.*/ 
	SELECT count(1) 
	  INTO v_hist_sync_count
	  FROM t9151_device_sync_dtl
	 WHERE c9150_device_info_id = p_device_infoid
	   AND c901_sync_type       = p_sync_type
	   AND c901_language_id     = p_language_id
	   AND (c901_sync_status     = 103121  --success
	   OR ( c901_sync_status = 103120 --Initiated 
	   AND c9151_last_sync_date IS NOT NULL))
	   AND c9151_void_fl IS NULL;
	   
	
	   
	-- Get the latest update details from the device's last sync time.	
	DELETE FROM my_temp_prod_cat_info;
	
	INSERT INTO my_temp_prod_cat_info(ref_id,void_fl)
   		     SELECT c2001_ref_id refid, DECODE(c901_action,'4000412','Y',NULL) void_fl
           FROM t2001_master_data_updates t2001, t9151_device_sync_dtl t9151
          WHERE t9151.c901_sync_type = t2001.c901_ref_type             
            AND t9151.c901_language_id = t2001.c901_language_id
            AND t2001.c2001_ref_updated_date >= t9151.c9151_last_sync_date 
            AND t9151.c9150_device_info_id = p_device_infoid
            AND t9151.c901_sync_type       = p_sync_type
		    AND t9151.c901_language_id     = p_language_id
      	    AND ((t9151.c901_sync_status     = 103121 OR t9151.c901_sync_status     = 103124) --103121 -success,103124-failure
	        OR  ( t9151.c901_sync_status = 103120 --Initiated 
	        AND t9151.c9151_last_sync_date IS NOT NULL))
            AND t2001.c2001_void_fl IS NULL
            AND t9151.c9151_void_fl IS NULL
            and NVL(t2001.c1900_company_id ,v_company_id) = v_company_id  ;
            
         
    SELECT count(ref_id) INTO v_update_count FROM my_temp_prod_cat_info where ref_id IS NOT NULL;
    --when v_hist_sync_count > 0,v_update_count=0 no need to do master sync.
    

    
    IF v_hist_sync_count > 0 and  v_update_count = 0
    THEN
   	    p_sync_count  := -1;
    ELSE 
        p_sync_count := v_update_count;
    END IF;
    
    BEGIN
	    SELECT C9151_LAST_SYNC_REC
		  INTO p_last_rec
		  FROM t9151_device_sync_dtl
		 WHERE c9150_device_info_id = p_device_infoid
		   AND c901_sync_type       = p_sync_type
		   AND c901_language_id     = p_language_id
		   AND c901_sync_status     = 103120  --initiated
		   AND NVL(p_pageno,-999) <> 1 --avoid last record logic while calling first page.
		   AND c9151_void_fl IS NULL;    
	    EXCEPTION WHEN NO_DATA_FOUND THEN
	    p_last_rec := NULL;
    END;
    
	END gm_fch_device_updates;
	
   /*******************************************************
	 * Description : Procedure to fch all system tosync
	*******************************************************/
	PROCEDURE gm_fch_all_system_tosync 
	AS 
	BEGIN
		DELETE FROM my_temp_prod_cat_info;
		
		INSERT INTO my_temp_prod_cat_info(ref_id)
			 SELECT DISTINCT (T207.c207_set_id) setid
			            FROM t207_set_master T207, t207c_set_attribute t207c
			           WHERE T207.c207_set_id = t207c.c207_set_id
			             AND T207.c901_set_grp_type = 1600   --Sales Report Grp
			             AND T207.c207_void_fl IS NULL
			             AND t207c.c207c_void_fl IS NULL
			             AND c901_attribute_type = '103119'    --Publish to
			             AND c207c_attribute_value = '103085'; --prod catalog				    
	END gm_fch_all_system_tosync;
	
   /*******************************************************
	 * Description : Procedure to fch all code group tosync
	 * It had been changed to refer the code group from t901f_code_lkp_ipad_map instead of T906_rules for Efficiency
	*******************************************************/
	PROCEDURE gm_fch_all_codegroup_tosync 
	AS 
	BEGIN
		DELETE FROM my_temp_prod_cat_info;
		
		INSERT INTO my_temp_prod_cat_info(ref_id)
			 select C901f_CODE_GRP from t901f_code_lkp_ipad_map where C901_LOOKUP_TYPE = 103107 AND C901F_VOID_FL IS NULL;
	END gm_fch_all_codegroup_tosync;
	
	 /********************************************************************
	 * Description : Procedure to fch all code lookup exclusion tosync
	 * It had been changed to refer the code group from t901f_code_lkp_ipad_map instead of T906_rules for Efficiency
	**********************************************************************/
	PROCEDURE gm_fch_all_code_excl_tosync 
	AS 
	BEGIN
		DELETE FROM my_temp_prod_cat_info;
		
		INSERT INTO my_temp_prod_cat_info(ref_id)          
             select C901f_CODE_GRP from t901f_code_lkp_ipad_map where C901_LOOKUP_TYPE = 26230802 AND C901F_VOID_FL IS NULL;	
	END gm_fch_all_code_excl_tosync;
	
	/********************************************************************
	 * Description : Procedure to fch all code lookup access tosync
	 * It had been changed to refer the code group from t901f_code_lkp_ipad_map instead of T906_rules for Efficiency
	**********************************************************************/
	PROCEDURE gm_fch_all_code_access_tosync 
	AS 
	BEGIN
		DELETE FROM my_temp_prod_cat_info;
		
		INSERT INTO my_temp_prod_cat_info(ref_id)
			select C901f_CODE_GRP from t901f_code_lkp_ipad_map where C901_LOOKUP_TYPE = 26230804 AND C901F_VOID_FL IS NULL;
	END gm_fch_all_code_access_tosync;
	
   /*******************************************************
 	* Description : Procedure to fch all parts tosync
	*******************************************************/
--	PROCEDURE gm_fch_all_parts_attr_tosync
--	     (p_system_id  IN t207_set_master.c207_set_id%type DEFAULT NULL)
--	AS 
--	v_company_id   	t1900_company.c1900_company_id%TYPE; 
--	BEGIN
--	SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL')) INTO v_company_id FROM dual; 
--		DELETE FROM my_temp_prod_cat_info;
--		
--		INSERT INTO my_temp_prod_cat_info(ref_id)
--		          SELECT t205.c205_part_number_id PNUM
--                              FROM t205_part_number T205, t2023_part_company_mapping t2023,(
--                                  	SELECT DISTINCT
--      								t205d.c205_part_number_id
--							    	FROM
--							      t205d_part_attribute T205D
--							      , ( select c901_rfs_id, c1900_company_id
--							            from t901c_rfs_company_mapping
--							            where c901c_void_fl is null
--							           union
--							          select 80120 c901_rfs_id
--							                 ,1000 c1900_company_id
--							            from dual ) rfs
--							    WHERE
--							      T205D.c205d_void_fl        IS NULL
--							    AND T205D.c901_attribute_type = '80180' -- Release for sales
--							    and t205d.c205d_attribute_value = TO_CHAR(rfs.c901_rfs_id)
--							    and rfs.c1900_company_id = v_company_id
--							 )
--                               t205d,
--                               (SELECT c205_part_number_id pnum 
--                                                                 FROM t207_set_master t207,t208_set_details t208
--                                                                WHERE t207.c901_set_grp_type = '1600'   --sales Report Grp 
--                                           AND t208.c207_set_id = t207.c207_set_id
--                                                                     AND t207.c207_void_fl IS NULL 
--                                                                     
--                                                                     AND t207.c207_set_id = NVL(p_system_id,t207.c207_set_id)
--                                                                     --The below code is commented for MDO-88.All The parts need to be synced not only the groups which are published to product catalog. 
--                                                                     --AND t207c.c901_attribute_type = '103119'    --Publish to 
--                                                                     --AND t207c.c207c_attribute_value = '103085'
--                                                                     ) --Product Catalog
--                                                                     sysdata
--                             WHERE T205.c205_active_fl      IS NOT NULL
--                               AND T205.c205_part_number_id  = T205d.c205_part_number_id
--                               AND t205.c205_part_number_id  = sysdata.pnum
--                               AND t205.c205_part_number_id  = t2023.c205_part_number_id
--                                  AND t2023.c2023_void_fl IS NULL
--                           AND t2023.c1900_company_id =v_company_id
--                               AND T205.c205_product_family IN (SELECT C906_RULE_VALUE
--                                                                                            FROM T906_RULES
--                                                                                           WHERE C906_RULE_ID     = 'PART_PROD_FAMILY'
--                                                                                             AND C906_RULE_GRP_ID = 'PROD_CAT_SYNC'
--                                                                                             AND c906_void_fl    IS NULL);
--				    -- 4050	- Implants, 4051 - Instruments, 4052 - Graphic Cases , 4056 - Literature Product family
--	END gm_fch_all_parts_attr_tosync;	
	
	 /*******************************************************
 	* Description : Procedure to fch all Parts to sync
 	* It has been changed to sync all the parts that hasd been released ato atleast any one company
 	* So Removing RFS based filtering from t205d and checking with the c205_release_for_sale
	*******************************************************/
	PROCEDURE gm_fch_all_parts_tosync
	     (p_system_id  IN t207_set_master.c207_set_id%type DEFAULT NULL)
	AS 
	v_company_id   	t1900_company.c1900_company_id%TYPE; 
	BEGIN
	SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL')) INTO v_company_id FROM dual; 
	DELETE FROM my_temp_prod_cat_info;
		
			INSERT INTO my_temp_prod_cat_info(ref_id)
		          SELECT distinct t205.c205_part_number_id PNUM
                              FROM t205_part_number T205, t2023_part_company_mapping t2023,
                               (SELECT c205_part_number_id pnum 
                                                                 FROM t207_set_master t207,t208_set_details t208
                                                                WHERE t207.c901_set_grp_type = '1600'   --sales Report Grp 
                                           AND t208.c207_set_id = t207.c207_set_id
                                                                     AND t207.c207_void_fl IS NULL 
                                                                     
                                                                     AND t207.c207_set_id = NVL(p_system_id,t207.c207_set_id)
                                                                     --The below code is commented for MDO-88.All The parts need to be synced not only the groups which are published to product catalog. 
                                                                     --AND t207c.c901_attribute_type = '103119'    --Publish to 
                                                                     --AND t207c.c207c_attribute_value = '103085'
                                                                     ) --Product Catalog
                                                                     sysdata
                             WHERE T205.c205_active_fl      IS NOT NULL
								and t205.c205_release_for_sale ='Y'
                               AND t205.c205_part_number_id  = sysdata.pnum
                               AND t205.c205_part_number_id  = t2023.c205_part_number_id
                                  AND t2023.c2023_void_fl IS NULL
                           AND t2023.c1900_company_id =v_company_id
                               AND T205.c205_product_family IN (SELECT C906_RULE_VALUE
                                                                                            FROM T906_RULES
                                                                                           WHERE C906_RULE_ID     = 'PART_PROD_FAMILY'
                                                                                             AND C906_RULE_GRP_ID = 'PROD_CAT_SYNC'
                                                                                             AND c906_void_fl    IS NULL);
	END gm_fch_all_parts_tosync;	
	
	 /*******************************************************
 	* Description : Procedure to fch all RFS Company mapping ID
	*******************************************************/
	
	PROCEDURE gm_fch_all_rfs_parts_tosync
	AS 
	BEGIN
		DELETE FROM my_temp_prod_cat_info;
		
		INSERT INTO my_temp_prod_cat_info(ref_id)
		 SELECT T901C.C901C_RFS_COMP_MAP_ID RFSMAPID 
				FROM T901C_RFS_COMPANY_MAPPING T901C
				WHERE T901C.C901C_VOID_FL IS NULL;
				
	END gm_fch_all_rfs_parts_tosync;
		
   /*******************************************************
 	* Description : Procedure to fch all group tosync
	*******************************************************/
	PROCEDURE gm_fch_all_group_tosync
	AS 
	BEGIN
		DELETE FROM my_temp_prod_cat_info;
		
		INSERT INTO my_temp_prod_cat_info(ref_id)
			 SELECT t4010.c4010_group_id groupid
			   FROM t4010_group t4010, (
			        SELECT DISTINCT (t207.c207_set_id) setid
			           FROM t207_set_master t207
			          WHERE  t207.c901_set_grp_type = '1600' --sales report grp
			            AND t207.c207_void_fl     IS NULL
			            --The below code is commented for MDO-88.All The groups need to be synced not only the groups which are published to product catalog. 
			            --AND c901_attribute_type    = '103119' --Publish to
			            --AND c207c_attribute_value  = '103085' --Product Catalog
			    )
			    SYSDATA
			  WHERE t4010.c207_set_id        = SYSDATA.setid
			    AND t4010.c901_type          = 40045 --Forecast/Demand Group
			    AND t4010.c4010_void_fl     IS NULL
			    AND t4010.c4010_publish_fl  IS NOT NULL
			    AND t4010.c4010_inactive_fl IS NULL; 
		
	END gm_fch_all_group_tosync;	
   /*******************************************************
	* Description : Procedure to fch all setmaster tosync
	*******************************************************/
	PROCEDURE gm_fch_all_setmaster_tosync 
	AS 
	v_company_id   	t1900_company.c1900_company_id%TYPE; 
	v_cmp_exc_fl    t906_rules.c906_rule_value%TYPE;
	v_set_sync_val  t906_rules.c906_rule_value%TYPE;
	BEGIN
	SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL')) INTO v_company_id FROM dual; 
	
--Fetch company exclude flag
	SELECT NVL(get_rule_value_by_company(v_company_id,'EXC_SET_IPAD_FL',v_company_id),'N')INTO v_cmp_exc_fl FROM DUAL;
	
--Fetch exclude flag value
	SELECT get_rule_value(v_cmp_exc_fl,'IPAD_SET_SYNC')INTO v_set_sync_val FROM DUAL;

	
		DELETE FROM my_temp_prod_cat_info;
		
		INSERT INTO my_temp_prod_cat_info(ref_id)	
	          SELECT t207.c207_set_id systemid
			    FROM t207_set_master t207, T2080_SET_COMPANY_MAPPING T2080, (
			        SELECT DISTINCT (T207.c207_set_id) systemid
			           FROM t207_set_master T207
			          WHERE T207.c901_set_grp_type = '1600' --System Report Grp
			            AND T207.c207_void_fl     IS NULL
			            --The below code comented as part of MIM-2, since all the set are needed to create a request
			            --AND c901_attribute_type    = '103119' --Publish to
			            --AND c207c_attribute_value  = '103085' --Product Catalog
			    )
			    sysdata
			  WHERE T207.C207_SET_SALES_SYSTEM_ID       = sysdata.systemid(+)
			    AND T207.c901_set_grp_type = 1601
                AND T207.c901_status_id    ='20367'  --Approved status
                AND T207.C207_SET_ID       = T2080.C207_SET_ID
                AND T2080.C2080_VOID_FL   IS NULL
                AND T2080.C1900_COMPANY_ID IN (v_company_id)
                --Removing the below for Japan IPAD chnages to get the JPN Sets
                --AND T207.C207_EXCLUDE_IPAD_FL IS NULL
                --added the below code to get the JPN sets for Japan if the rule value is Y, otherwise get the C207_EXCLUDE_IPAD_FL = NULL by below condition
                AND DECODE(v_cmp_exc_fl,'Y', v_set_sync_val ,NVL(T207.C207_EXCLUDE_IPAD_FL,v_set_sync_val)) = v_set_sync_val
			    AND t207.c207_void_fl     IS NULL;
				    
	END gm_fch_all_setmaster_tosync;
	
   /**************************************************************
	 * Description : Procedure to fch all system attributes tosync
	**************************************************************/
	PROCEDURE gm_fch_all_systemattrib_tosync 
	AS 
	BEGIN
		--This prc will add all System which are publish to PC
		gm_fch_all_system_tosync;
	END gm_fch_all_systemattrib_tosync;
   /**************************************************************
	 * Description : Procedure to fch all uploaded files info tosync
	**************************************************************/
	PROCEDURE gm_fch_all_uploaded_fileinfo
	AS
	BEGIN
		DELETE FROM my_temp_prod_cat_info;
		
		INSERT INTO my_temp_prod_cat_info(ref_id)
		 	SELECT t903.c903_upload_file_list
  			  FROM t903_upload_file_list t903 
 			 WHERE (T903.C901_REF_GRP in (103112,107994,107993) 
 			 		OR t903.c901_ref_type  IN(SELECT c906_rule_value FROM t906_rules 
 					WHERE c906_rule_id ='FILE_TYPE' 
    			  	 and C906_RULE_GRP_ID = 'PROD_CAT_SYNC' 
 			 							  	  and C906_VOID_FL is null))
  			   AND t903.c903_delete_fl IS NULL;
	END gm_fch_all_uploaded_fileinfo;
	/*******************************************************
	 * Description : Procedure to fch paging details
	 * Example	   : If pageno is null then, it will return 
	 *               p_page_no is 1, p_startrow is 1, p_endrow is 200 (base on pagesize) and p_page_size is 200 (from rule table).
	 *               If pageno is 4 then it will return
	 *               p_page_no is 4, p_startrow is 601, p_endrow is 800 (base on pagesize) and p_page_size is 200 (from rule table).
	*******************************************************/
	PROCEDURE gm_fch_paging_dtl(
		p_table_nm  IN      t906_rules.c906_rule_id%TYPE
	  , p_rule_grp  IN  	t906_rules.C906_Rule_Grp_Id%TYPE
	  , p_page_no   IN OUT  NUMBER
	  , p_startrow  OUT     NUMBER
	  , p_endrow    OUT     NUMBER
	  , p_page_size OUT     NUMBER	  
	)
	AS
	    v_page_size NUMBER;
	BEGIN
	 	SELECT NVL(get_rule_value(p_table_nm, p_rule_grp),1500) INTO v_page_size FROM DUAL;
       
	    IF p_page_no IS NULL THEN
	        p_startrow := 1;
	        p_endrow   := v_page_size;
	        p_page_no := 1;
	    ELSE
	        p_startrow := (p_page_no - 1) * v_page_size + 1;
	        p_endrow   := p_page_no  * v_page_size;
	    END IF;
	    
	    p_page_size := v_page_size;
	    
	END gm_fch_paging_dtl;
	/*************************************************************************************
	* Description : Procedure to fetch the data sync update detail for product catalog.
	**************************************************************************************/	
	PROCEDURE gm_fch_all_datasync_update  (
              p_token              IN            t101a_user_token.c101a_token_id%TYPE,
              p_langid             IN            NUMBER,
              p_page_no            IN            NUMBER,
              p_uuid               IN            t9150_device_info.c9150_device_uuid%TYPE,
              p_party_id           IN			 T101_PARTY.C101_PARTY_ID%TYPE,
              p_data_cur           OUT           TYPES.CURSOR_TYPE
       )
       AS
              v_device_infoid  	t9150_device_info.c9150_device_info_id%TYPE;
              v_date_fmt  		VARCHAR2(20);
              v_count           NUMBER;
              v_page_no   		NUMBER;
              v_page_size 		NUMBER;
              v_start     		NUMBER;
           	  v_end       		NUMBER;    
              v_company_id      t1900_company.c1900_company_id%TYPE; 
       BEGIN
              SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL')) INTO v_company_id FROM dual;
              
              v_device_infoid := get_device_id(p_token, p_uuid) ;
              v_date_fmt := get_rule_value( 'DATEFMT','DATEFORMAT');
              v_page_no := p_page_no;
              -- get the paging details
              gm_pkg_pdpc_prodcatrpt.gm_fch_paging_dtl('DATA_SYNC','PAGESIZE',v_page_no,v_start,v_end,v_page_size); 

              OPEN p_data_cur
                     FOR    
                     SELECT COUNT (1) OVER () pagesize, result_count totalsize, v_page_no pageno, rwnum, CEIL (result_count / v_page_size) totalpages
                           , device_sync.*
                        FROM
                         (
                              SELECT device_sync.*, ROWNUM rwnum, COUNT (1) OVER () result_count
                                FROM
                                 (
                                        SELECT
                                                  t2001.c2001_ref_id refid,
                                                  t2001.c901_ref_type reftype,
                                                  t2001.c901_action action,
                                                  t2001.c901_app_id appid ,
                                                  t2001.c2001_last_updated_by updatedby,
                                                  TO_CHAR(t2001.c2001_last_updated_date,v_date_fmt) updateddt,
                                                  t2001.c2001_void_fl voidfl
                                                FROM
                                                  t2001_master_data_updates t2001,
                                                  t9151_device_sync_dtl t9151
                                                WHERE
                                                  t2001.c901_language_id       = t9151.c901_language_id
                                                AND T2001.C901_REF_TYPE        = T9151.C901_SYNC_TYPE
                                                AND t9151.c9150_device_info_id = v_device_infoid
                                                AND t9151.c901_language_id     = p_langid
                                             	AND T2001.C2001_REF_UPDATED_DATE > t9151.c9151_last_sync_date
                                                AND NVL(t2001.c1900_company_id,v_company_id)   = v_company_id
                                                AND T9151.C9151_VOID_FL   IS NULL
                                                AND t2001.c2001_void_fl   IS NULL
                                                AND t2001.c901_ref_type   IN
                                                  (
                                                    SELECT DISTINCT
                                                      t906_updates.c906_rule_value
                                                    FROM
                                                      T1530_access T1530 ,
                                                      t906_rules T906_PAGES ,
                                                      t906_rules T906_MOBILE ,
                                                      T906_RULES T906_UPDATES ,
                                                      t1501_group_mapping t1501
                                                    WHERE
                                                      t1501.c101_mapped_party_id      = p_party_id -- party id
                                                    AND t1501.c1500_group_id          = T1530.c1500_group_id
                                                    AND t1530.c1520_function_id       = T906_PAGES.c906_rule_grp_id
                                                    AND T906_PAGES.C906_RULE_VALUE   IN ('SYNC_DATA') -- page names
                                                    AND T906_MOBILE.c906_rule_grp_id  = 'MOBILESECURITYEVENTS'
                                                    AND T906_MOBILE.c906_rule_id      = T906_PAGES.c906_rule_grp_id
                                                    AND t1530.c1530_update_access_fl  ='Y'
                                                    AND T906_UPDATES.c906_rule_grp_id = 'SYNC_UPDATES'
                                                    AND t906_pages.c906_rule_id       = t906_updates.c906_rule_id
                                                    AND t1530.c1530_void_fl          IS NULL
                                                    AND t1501.c1501_void_fl          IS NULL
                                                    AND T906_PAGES.c906_void_fl      IS NULL
                                                    AND T906_MOBILE.c906_void_fl     IS NULL
                                                    AND t906_updates.c906_void_fl    IS NULL
                                                    UNION
                                                    SELECT
                                                      c906_rule_value
                                                    FROM
                                                      (
                                                        SELECT
                                                          c906_rule_value,
                                                          GRP_MAP_COUNT
                                                        FROM
                                                          (
                                                            SELECT
                                                              c906_rule_value
                                                            FROM
                                                              t906_rules T906
                                                            WHERE
                                                              t906.c906_rule_grp_id = 'SYNC_UPDATES'
                                                            AND c906_void_fl       IS NULL
                                                          )
                                                          ,
                                                          (
                                                           SELECT
                                                              COUNT(1) GRP_MAP_COUNT
                                                            FROM
                                                              T1530_access T1530 ,
                                                              t906_rules T906_PAGES ,
                                                              t906_rules T906_MOBILE ,
                                                              t1501_group_mapping t1501
                                                            WHERE t1501.c101_mapped_party_id = p_party_id -- party id
                                                              AND t1501.c1500_group_id     = T1530.c1500_group_id
                                                              AND t1530.c1520_function_id  = T906_PAGES.c906_rule_grp_id
                                                              AND T906_PAGES.C906_RULE_VALUE    IN ('SYNC_DATA') -- page names
                                                              AND T906_MOBILE.c906_rule_grp_id = 'MOBILESECURITYEVENTS'
                                                              AND T906_MOBILE.c906_rule_id     = T906_PAGES.c906_rule_grp_id
                                                              AND T1530.C1530_VOID_FL IS NULL
                                                              AND t1501.C1501_VOID_FL IS NULL
                                                              AND T906_PAGES.c906_void_fl    IS NULL
                                                              AND T906_MOBILE.c906_void_fl    IS NULL
                                                          )
                                                        WHERE
                                                          GRP_MAP_COUNT = 0
                                                      )
                                                  )
                                  ORDER BY
                                    t2001.c901_ref_type,
                                    t2001.c901_language_id
                                                                   )device_sync
                                                            )device_sync
                                                         WHERE rwnum BETWEEN v_start AND v_end;               
       END gm_fch_all_datasync_update;

	/**************************************************************************************
	 * Description : Procedure to fetch all Rule group related to Marketing Collateral tosync
	*************************************************************************************/
	PROCEDURE gm_fch_all_rulegrps_tosync 
	AS 
	BEGIN
		DELETE FROM my_temp_prod_cat_info;
		
		INSERT INTO my_temp_prod_cat_info(ref_id)
		
		 SELECT distinct c906_rule_grp_id 
       FROM T906_RULES t906, (
             SELECT t906.c906_rule_value codegrp
               FROM t906_rules t906
              WHERE t906.c906_rule_id IN ('SYNC_RULE_GRP')
                AND T906.C906_RULE_GRP_ID IN ('PROD_CAT_SYNC')
                AND T906.C906_VOID_FL    IS NULL
        )
        GRP
      WHERE t906.C906_RULE_GRP_ID = grp.codegrp
        AND t906.c906_void_fl    IS NULL;
			    
	END gm_fch_all_rulegrps_tosync;
	/*******************************************************
	 * Description : Procedure to fetch all keywords tosync
	 *******************************************************/
	PROCEDURE gm_fch_all_keywords_tosync(
      p_ref_id    IN t2050_keyword_ref.c2050_ref_id%TYPE DEFAULT NULL,
      p_ref_type IN t2050_keyword_ref.c901_ref_type%TYPE DEFAULT NULL
      ) 
    AS 
    BEGIN
            DELETE FROM my_temp_prod_cat_info;
            
            INSERT INTO my_temp_prod_cat_info(ref_id)
                  SELECT t2050.c2050_keyword_ref_id
                      FROM t2050_keyword_ref T2050, 
                       (
                           SELECT DISTINCT (T207.c207_set_id) systemid
                              FROM t207_set_master T207, t207c_set_attribute t207c
                             WHERE T207.c207_set_id       = t207c.c207_set_id
                               AND T207.c901_set_grp_type = '1600' --System Report Grp
                               AND T207.c207_void_fl     IS NULL
                               -- ref_id will come from portal(Keyword Detail screen) that time no need to check System publish to PC
                               AND (p_ref_id 			 IS NOT NULL
                               OR (c901_attribute_type    = '103119' --Publish to
                               AND c207c_attribute_value  = '103085')
                               AND t207c.c207c_void_fl   IS NULL) --Product Catalog
                       )  sysdata                      
                      WHERE t2050.c2050_void_fl IS NULL
                      AND SYSDATA.SYSTEMID = T2050.C2050_REF_ID
                      AND t2050.c901_ref_type = NVL(p_ref_type,'103108') --System  
                      AND t2050.c2050_ref_id =  NVL(p_ref_id,t2050.c2050_ref_id) 
                     UNION ALL
                      SELECT T2050.C2050_KEYWORD_REF_ID
                      FROM t2050_keyword_ref T2050, t903_upload_file_list t903, 
                       (
                           SELECT DISTINCT (T207.c207_set_id) systemid
                              FROM t207_set_master T207, t207c_set_attribute t207c
                             WHERE T207.c207_set_id       = t207c.c207_set_id
                               AND T207.c901_set_grp_type = '1600' --System Report Grp
                               AND T207.c207_void_fl     IS NULL
                               -- ref_id will come from portal(Keyword Detail screen) that time no need to check System publish to PC
                               AND (p_ref_id 			 IS NOT NULL  
                               OR ( c901_attribute_type    = '103119' --Publish to
                               AND c207c_attribute_value  = '103085')
                               AND t207c.c207c_void_fl   IS NULL) --Product Catalog
                       )  SYSDATA                      
                      WHERE t2050.c901_ref_type = NVL(p_ref_type,'4000410') --File
                      AND t2050.c2050_ref_id =  NVL(p_ref_id,t2050.c2050_ref_id)
                      AND t2050.c2050_ref_id = t903.c903_upload_file_list
                      AND t903.c903_ref_id = sysdata.systemid
                      AND t2050.c2050_void_fl IS NULL
                      AND T903.C903_DELETE_FL IS NULL;                                      
    END gm_fch_all_keywords_tosync;

	/*******************************************************
	 * Description : Procedure used for web service used to 
	 * fetch all Party ID from party table of type 7000
	 *******************************************************/
	PROCEDURE gm_fch_all_parites_tosync
	AS 
	v_company_id   	t1900_company.c1900_company_id%TYPE; 
	BEGIN
		SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL')) INTO v_company_id FROM dual;

		DELETE FROM my_temp_prod_cat_info;
		
		INSERT INTO my_temp_prod_cat_info(ref_id)
			  SELECT t101.c101_party_id
			  FROM t101_party t101
			  WHERE t101.c901_party_type in (SELECT c906_rule_value FROM t906_rules 
 			 								  WHERE c906_rule_id ='PARTY_TYPE' 
 			 							  	   AND c906_rule_grp_id = 'PROD_CAT_SYNC' 
 			 							  	    AND c906_void_fl is null)
			  AND ( C101_ACTIVE_FL IS NULL OR C101_ACTIVE_FL = 'Y' )			 							 
			  AND t101.c101_void_fl is null
			  AND t101.c1900_company_id  = v_company_id;
	END gm_fch_all_parites_tosync ;
	
	/*****************************************************************************************************
	 * Description :  This procedure used for web service which is used to fetch contact id from contact table 
	   where The contact mode for contact id should be 90452(E-mail-id) and 
	   for the rescpective party type  for party id should be 7000 and 
	   the records returned from procedure are of surgeon type. 
	 ***************************************************************************************************/
	PROCEDURE gm_fch_party_contact_tosync
	AS 
	v_company_id   	t1900_company.c1900_company_id%TYPE;
	BEGIN
		SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL')) INTO v_company_id FROM dual;
		DELETE FROM my_temp_prod_cat_info;
		
		INSERT INTO my_temp_prod_cat_info(ref_id)
        	SELECT t107.c107_contact_id
			FROM t101_party t101,t107_contact t107
			WHERE t101.c101_party_id =t107.c101_party_id
			AND t107.c901_mode      IN (SELECT c906_rule_value
  										FROM t906_rules
  										WHERE c906_rule_id   ='CONTACT_MODE'
  										AND c906_rule_grp_id = 'PROD_CAT_SYNC'
  										AND C906_VOID_FL  IS NULL)
			
  			AND t101.c901_party_type IN (SELECT c906_rule_value
  										 FROM t906_rules
  										 WHERE c906_rule_id   ='PARTY_TYPE'
  										 AND C906_RULE_GRP_ID = 'PROD_CAT_SYNC'
  										 AND C906_VOID_FL    IS NULL )
  			AND ( C101_ACTIVE_FL IS NULL OR T101.C101_ACTIVE_FL ='Y')							 
			AND T101.C101_VOID_FL   IS NULL
			AND T101.C101_DELETE_FL IS NULL
			AND C107_INACTIVE_FL IS NULL
			AND t107.c107_void_fl   IS NULL
			AND t101.c1900_company_id  = v_company_id;
	END gm_fch_party_contact_tosync ;
	/*******************************************************
	 *  Description :  This procedure used for web service which is used to fetch fetch Party Contact Details 
	   for party id. The contact mode for party id should be 90452(E-mail-id) and party type should be 7000 and 
	   this method will return 1000 records at a time from procedure.
	   the records returned from procedure are of surgeon type. 
	 *******************************************************/
	PROCEDURE gm_fch_all_party_contact(
		p_token  			IN 		t101a_user_token.c101a_token_id%TYPE,
		p_langid			IN		NUMBER,
		p_page_no			IN		NUMBER,
	    p_uuid		        IN      t9150_device_info.c9150_device_uuid%TYPE,
		p_party_contactlist_cur		OUT		TYPES.CURSOR_TYPE
	) 
	AS
		v_deviceid  t9150_device_info.c9150_device_info_id%TYPE;
		v_count	    NUMBER;
   	    v_last_rec	t9151_device_sync_dtl.C9151_LAST_SYNC_REC%TYPE;	   
	 BEGIN
		--get the device info id based on user token
		v_deviceid := get_device_id(p_token, p_uuid);
		
		
		/*below prc check the device's last updated date for Rules reftype and
		if record exist then it checks is there any Contacts updated after the device updated date.
		if updated groups available then add all Contacts into v_inlist and return count of v_inlist records*/
		gm_fch_device_updates(v_deviceid,p_langid,4000448,p_page_no,v_count,v_last_rec); -- 4000448 - Party Contact reftype
		
		IF v_count = 0 THEN
			gm_fch_party_contact_tosync;
		END IF;
		gm_fch_all_party_contact_dtl(p_page_no,v_last_rec,p_party_contactlist_cur);
		
			  --commit;
	END gm_fch_all_party_contact;

	
	/*******************************************************
	   Description :  This procedure used for web service which is used to fetch fetch Party Contact Details 
	   for party id. The contact mode for party id should be 90452(E-mail-id) and party type should be 7000 and 
	   this method will return 1000 records at a time from procedure.
	   the records returned from procedure are of surgeon type. 
	 *******************************************************/
	PROCEDURE gm_fch_all_party_contact_dtl(
		p_pageno           IN     NUMBER,
		p_last_rec		   IN 	  t9151_device_sync_dtl.C9151_LAST_SYNC_REC%TYPE,	
		p_party_contactlist_cur  OUT 	  TYPES.cursor_type
    )
    AS
    	v_page_no   NUMBER;
		v_page_size NUMBER;
	  	v_start     NUMBER;
	    v_end       NUMBER;
    BEGIN
   
		v_page_no       := p_pageno;
		-- get the paging details
		gm_fch_paging_dtl('PARTYCONTACT','PAGESIZE',v_page_no,v_start,v_end,v_page_size);
		OPEN p_party_contactlist_cur
			FOR		
			 SELECT COUNT (1) OVER () pagesize,result_count totalsize,v_page_no pageno, rwnum,CEIL(result_count / v_page_size) totalpages,t107.*
			 	FROM(
					SELECT t107.*, ROWNUM rwnum, COUNT (1) OVER () result_count, MAX(DECODE(t107.contactid,p_last_rec,ROWNUM,NULL)) OVER() LASTREC
			    		FROM(
							SELECT t101.c101_party_id partyid,t107.c107_contact_value contactval,t101.c101_party_nm,
							       NVL(temp.void_fl,t107.c107_void_fl) voidfl,t107.c107_primary_fl primaryfl,
							       t107.c107_contact_id contactid,t107.c901_mode contactmode,
							       t107.c107_contact_type contacttype,t107.c107_inactive_fl inactivefl
							FROM   t101_party t101,t107_contact t107 ,my_temp_prod_cat_info temp
							WHERE  t107.c107_contact_id  = temp.ref_id
							AND    t101.c101_party_id  = t107.c101_party_id
							ORDER BY t107.c107_contact_id
							)
			            t107
			    )
			    t107
			  WHERE RWNUM BETWEEN NVL(LASTREC+1,DECODE(p_last_rec,NULL,v_start,v_start-1)) AND v_end;
	END gm_fch_all_party_contact_dtl;
	 /*************************************************************************************************
	 * Description :  This function is used to get the last sync time from the device
	**************************************************************************************************/
	FUNCTION get_last_sync_date (
		p_device_infoid  IN  T9151_DEVICE_SYNC_DTL.C9150_DEVICE_INFO_ID%TYPE
	  , p_language_id    IN  T9151_DEVICE_SYNC_DTL.C901_LANGUAGE_ID%TYPE
	  , p_sync_type      IN  T9151_DEVICE_SYNC_DTL.C901_SYNC_TYPE%TYPE
	  , p_party_id       IN  t101_user.c101_party_id%TYPE DEFAULT NULL
	  , p_type			 IN  t2001_master_data_updates.c901_ref_type%TYPE
	) RETURN DATE
	IS
		v_date DATE;
	BEGIN
		 SELECT distinct(t9151.c9151_last_sync_date)
		   INTO v_date
           FROM t2001_master_data_updates t2001, t9151_device_sync_dtl t9151
           WHERE t9151.c901_sync_type = t2001.c901_ref_type             
            AND t9151.c901_language_id = t2001.c901_language_id
            AND t2001.c2001_ref_updated_date >= t9151.c9151_last_sync_date 
            AND t9151.c9150_device_info_id = p_device_infoid
            AND t9151.c901_sync_type       = p_sync_type
		    AND t9151.c901_language_id     = p_language_id
      	    AND (t9151.c901_sync_status     = 103121  --success
	        OR  ( t9151.c901_sync_status = 103120 --Initiated 
	        AND t9151.c9151_last_sync_date IS NOT NULL))
            AND t2001.c901_ref_type = p_type
            AND t2001.c2001_void_fl IS NULL
            AND t9151.c9151_void_fl IS NULL;
        RETURN v_date;   
    EXCEPTION WHEN NO_DATA_FOUND THEN
    	RETURN NULL;
	END get_last_sync_date;
	 /*************************************************************************************************
	 * Description :  This function is used to get the last sync time for the device without joining with t2001
	**************************************************************************************************/
	FUNCTION get_last_sync_date (
		p_device_infoid  IN  T9151_DEVICE_SYNC_DTL.C9150_DEVICE_INFO_ID%TYPE
	  , p_language_id    IN  T9151_DEVICE_SYNC_DTL.C901_LANGUAGE_ID%TYPE
	  , p_sync_type      IN  T9151_DEVICE_SYNC_DTL.C901_SYNC_TYPE%TYPE
	) RETURN DATE
	IS
		v_date DATE;
	BEGIN
		 SELECT c9151_last_sync_date INTO v_date
		   FROM t9151_device_sync_dtl
		  WHERE c9150_device_info_id = p_device_infoid
		    AND c901_language_id = p_language_id
		    AND c901_sync_type = p_sync_type
		    AND c9151_void_fl IS NULL;
        RETURN v_date;   
    EXCEPTION WHEN NO_DATA_FOUND THEN
    	RETURN NULL;
	END get_last_sync_date;	
	
	/*******************************************************
	 * Description : Procedure to fch all set tosync
	*******************************************************/
	PROCEDURE gm_fch_all_setattrib_tosync 
	AS 
	v_company_id   	t1900_company.c1900_company_id%TYPE; 
	v_cmp_exc_fl    t906_rules.c906_rule_value%TYPE;
	v_set_sync_val  t906_rules.c906_rule_value%TYPE;
	BEGIN
	SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL')) INTO v_company_id FROM dual; 

--Fetch company exclude flag
	SELECT NVL(get_rule_value_by_company(v_company_id,'EXC_SET_IPAD_FL',v_company_id),'N')INTO v_cmp_exc_fl FROM DUAL;
	
--Fetch exclude flag value
	SELECT get_rule_value(v_cmp_exc_fl,'IPAD_SET_SYNC')INTO v_set_sync_val FROM DUAL;
	
		DELETE FROM my_temp_prod_cat_info;
		
		INSERT INTO my_temp_prod_cat_info(ref_id)
			 SELECT DISTINCT (t207.c207_set_id) setid
			    FROM t207_set_master t207, (
			        SELECT DISTINCT (T207.c207_set_id) systemid
			           FROM t207_set_master T207
			          WHERE T207.c901_set_grp_type = '1600' --System Report Grp
			            AND T207.c207_void_fl     IS NULL	            
			    )
			    sysdata, t207c_set_attribute t207c, T2080_SET_COMPANY_MAPPING T2080
			  WHERE T207.C207_SET_SALES_SYSTEM_ID = sysdata.systemid(+)
			    AND T207.c901_set_grp_type = 1601
                AND T207.c901_status_id    ='20367'  --Approved status
			    AND t207.c207_void_fl     IS NULL
			    --Removing the below for Japan IPAD chnages to get the JPN Sets
			    --AND T207.C207_EXCLUDE_IPAD_FL IS NULL
			     --added the below code to get the JPN sets for Japan if the rule value is Y, otherwise get the C207_EXCLUDE_IPAD_FL = NULL by below condition
 				AND DECODE(v_cmp_exc_fl,'Y', v_set_sync_val ,NVL(T207.C207_EXCLUDE_IPAD_FL,v_set_sync_val)) = v_set_sync_val
                AND T207.c207_set_id = t207c.c207_set_id
                AND t207c.c207c_void_fl IS NULL
                AND T207.C207_SET_ID       = T2080.C207_SET_ID
                AND T2080.C2080_VOID_FL   IS NULL
                AND T2080.C1900_COMPANY_ID IN (v_company_id)
                AND t207c.c901_attribute_type = '104740'--Loaner Pool
                AND t207c.c207c_attribute_value = 'Y';
				    
	END gm_fch_all_setattrib_tosync;

 /*********************************************************************************
	 * Description : Procedure to fch all security events tosync
  *********************************************************************************/
	PROCEDURE gm_fch_all_secu_event_tosync 
	AS 
	BEGIN
		DELETE FROM my_temp_prod_cat_info;
		
		INSERT INTO my_temp_prod_cat_info(ref_id)
		SELECT distinct T1530.c1500_group_id
		  FROM T1530_access T1530,t906_rules T906 
		 WHERE T1530.c1520_function_id = T906.c906_rule_id 
		   AND T906.c906_rule_grp_id = 'MOBILESECURITYEVENTS'; 
			    
	END gm_fch_all_secu_event_tosync;

END gm_pkg_pdpc_prodcatrpt;
/
