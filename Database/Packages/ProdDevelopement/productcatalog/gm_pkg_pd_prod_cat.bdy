--@"C:\Database\Packages\ProdDevelopement\productcatalog\gm_pkg_pd_prod_cat.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_pd_prod_cat
IS
	/*******************************************************
	* Description : Procedure to fetch Part details
	* Author      : Jignesh Shah
	*******************************************************/

	PROCEDURE gm_fch_part_info (
		p_part_id			IN		t205_part_number.c205_part_number_id%TYPE		
	  , p_out_partdetail	OUT      TYPES.cursor_type
	)	
	AS
		v_cnt NUMBER;
		BEGIN
			SELECT COUNT(1) INTO v_cnt
		      FROM t205_part_number 
		     WHERE c205_part_number_id = p_part_id 
		       AND c205_active_fl IS NOT NULL;
	      IF v_cnt = 0 THEN
	      GM_RAISE_APPLICATION_ERROR('-20999','391','');
        		   	
          END IF;
		OPEN p_out_partdetail
		FOR
	       SELECT c205_part_number_id partnum
	        	, c205_part_num_desc pdesc
	         FROM t205_part_number
	        WHERE c205_part_number_id = p_part_id
	          AND c205_active_fl IS NOT NULL;
	   END gm_fch_part_info;	
	
	/*******************************************************
	* Description : Procedure to fetch Set details
	* Author      : Jignesh Shah
	*******************************************************/

	PROCEDURE gm_fch_set_info (
		p_set_id			IN		t207_set_master.c207_set_id%TYPE		
	  , p_out_setdetail	OUT      TYPES.cursor_type
	)	
	AS
		v_cnt NUMBER;
		BEGIN
			SELECT COUNT(1) INTO v_cnt
              FROM t207_set_master 
             WHERE c207_set_id = p_set_id 
               AND c901_set_grp_type = '1601' -- Set Report Group
               AND c207_void_fl IS NULL 
               AND c207_obsolete_fl IS NULL;
	      IF v_cnt = 0 THEN
	      GM_RAISE_APPLICATION_ERROR('-20999','373','');
          		
          END IF;
			OPEN p_out_setdetail
				FOR 			         
        		 SELECT c207_set_id setid
             		  , c207_set_nm setnm 
                   FROM t207_set_master 
                  WHERE c207_set_id = p_set_id 
                    AND c901_set_grp_type = '1601' -- Set Report Group
                    AND c207_void_fl IS NULL 
                    AND c207_obsolete_fl IS NULL;                    
	END gm_fch_set_info;	
	
	/*******************************************************
	* Description : Procedure to fetch Set Bundle details
	* Author      : Bala
	*******************************************************/

	PROCEDURE gm_fch_set_bundle_info (
	  p_out_setbundledet	    OUT     TYPES.cursor_type
	)	
	AS
		v_cnt NUMBER;
		BEGIN

			OPEN p_out_setbundledet
				FOR 			         
        		 SELECT c2075_set_bundle_id setbundleid
             		  , c2075_set_bundle_nm setbundlenm 
                   FROM t2075_set_bundle 
                  WHERE c2075_void_fl IS NULL
                    AND c2075_set_bundle_nm IS NOT NULL 
               ORDER BY UPPER(setbundlenm);                             
	END gm_fch_set_bundle_info;	
	
END gm_pkg_pd_prod_cat;
/