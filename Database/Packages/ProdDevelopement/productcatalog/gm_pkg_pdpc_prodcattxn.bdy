/* Formatted on 2009/02/16 12:30 (Formatter Plus v4.8.0) */
--@"C:\Database\Packages\ProdDevelopement\productcatalog\gm_pkg_pdpc_prodcattxn.bdy";

CREATE OR REPLACE PACKAGE body gm_pkg_pdpc_prodcattxn
IS
	/***************************************************************
	 * Description : Procedure to save details of device sync and sync log 
	****************************************************************/
	PROCEDURE gm_sav_device_sync_dtl(
		p_token_id     IN  t101a_user_token.c101a_token_id%TYPE
	  , p_ref_type     IN  NUMBER
	  , p_status_id    IN OUT  NUMBER
	  , p_language_id  IN  NUMBER	
	  , p_sync_count   IN  NUMBER 
	  , p_uuid		   IN  t9150_device_info.c9150_device_uuid%TYPE 
	  , p_user_id	   IN  t101a_user_token.c101_user_id%TYPE 
	  , p_total_size   IN  NUMBER
	  , p_pageno	   IN  NUMBER
	  , p_last_rec	   IN  t9151_device_sync_dtl.C9151_LAST_SYNC_REC%TYPE 
	  , p_comp_id	   IN  t1900_company.c1900_company_id%TYPE
	  , p_db_total_size OUT NUMBER
	)
	AS
	v_status_id			NUMBER;
    v_db_total_size     NUMBER := 0;
	v_device_infoid		t9150_device_info.c9150_device_info_id%TYPE;
	v_device_sync_id    t9151a_device_sync_log.c9151a_sync_log_id%TYPE;
	v_sync_dt 			DATE;
	BEGIN
		--validating code lookup values
		gm_validate_code_id(p_ref_type,'DESYTY','-20611');
		gm_validate_code_id(p_status_id,'DSYSTS','-20612');
		gm_validate_code_id(p_language_id,'MLANG','-20606');
		 --get the device info id based on user token
		 v_device_infoid := get_device_id(p_token_id, p_uuid) ;
		
		 --If Device UUID [uuid] and Token are not associated then raise error.
		 IF v_device_infoid IS NULL THEN
		 	raise_application_error ('-20623','');
		 END IF;
		 
	   BEGIN
		 SELECT c9151_device_sync_id 
		   INTO v_device_sync_id
		   FROM t9151_device_sync_dtl
          WHERE c9150_device_info_id = v_device_infoid
		    AND c901_sync_type   = p_ref_type
		    AND c901_language_id = p_language_id
		    AND c9151_void_fl IS NULL;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN 
				v_device_sync_id := NULL;
    	END;
    	
    	v_status_id := p_status_id;
    	--DB local count
    	IF p_status_id = 103121 THEN
    	
    		 SELECT NVL(MIN(C9151A_SYNC_START_DATE),SYSDATE)
    		   INTO v_sync_dt
    		   FROM T9151A_DEVICE_SYNC_LOG T9151A
			  WHERE T9151A.C9151_DEVICE_SYNC_ID  = v_device_sync_id
			    AND T9151a.C901_SYNC_TYPE   = p_ref_type
			    AND T9151a.C901_LANGUAGE_ID = p_language_id
			    and T9151A.c9151a_sync_end_date IS NULL
			    and T9151A.c9151a_void_fl IS NULL;  
    	
			--v_db_total_size := gm_pkg_pdpc_prodcat_cnt.GET_DB_COUNT(p_ref_type,p_user_id);  

			 --PMT# Group sync in iPad with Interantional Changes  
    		v_db_total_size := gm_pkg_pdpc_prodcat_cnt.GET_DB_COUNT(p_ref_type,p_comp_id,p_user_id,p_last_rec);
    		
    		IF v_db_total_size <> 0 AND v_db_total_size <> p_total_size THEN
    		v_status_id := '103124';
    		END IF;
    	END IF;
    	
		 --After sync completed, Update the status and  C9151_LAST_SYNC_DATE in T9151_DEVICE_SYNC_DTL.
		 UPDATE t9151_device_sync_dtl
		    SET c9151_last_sync_date = DECODE (v_status_id, 103120,c9151_last_sync_date,v_sync_dt)
		      , c901_sync_status = v_status_id
		      , c9151_last_updated_date = SYSDATE
		      , c9151_last_updated_by = p_user_id
		      , c9151_server_cnt = DECODE (v_status_id, 103120, c9151_server_cnt,v_db_total_size)
		      , c9151_sync_cnt = DECODE (v_status_id, 103120, c9151_sync_cnt,p_total_size)
		      , C9151_LAST_SYNC_REC = DECODE (v_status_id, 103120, p_last_rec,NULL)
		  WHERE c9150_device_info_id = v_device_infoid
		    AND c901_sync_type   = p_ref_type
		    AND c901_language_id = p_language_id
		    AND c9151_void_fl IS NULL;		    
		
		-- When No record updated, insert a record in T9151 and should not populate the Last Sync Date.  
		IF (SQL%ROWCOUNT = 0)
	    THEN
	    
	  	  SELECT s9151_device_sync_dtl.NEXTVAL 
	  	    INTO v_device_sync_id 
	  	    FROM dual;
	  	    
	    	INSERT
		   	INTO t9151_device_sync_dtl
		    (
		        c9151_device_sync_id, c9150_device_info_id, c901_app_id
		      , c901_sync_type, c901_language_id, c9151_last_sync_date
		      , c901_sync_status, c9151_last_updated_date, c9151_last_updated_by
		      , c9151_last_sync_rec
		    )
		    VALUES
		    (
		        v_device_sync_id, v_device_infoid,'103100'  --Product Catalog App TYPE
		      , p_ref_type, p_language_id, NULL       --- while inserting a record in T9151 and should not populate the Last Sync Date.
		      , p_status_id, SYSDATE, p_user_id
		      , p_last_rec
		    ) ;	
	    END IF;
	    
	    --if status is initiated put entry in T9151A_DEVICE_SYNC_LOG else update the SYNC_END_DATE.
	    IF (p_status_id = 103120)  --INITIATED
	    THEN
	    
	    	IF(p_pageno = 1)
	    	THEN
	    		UPDATE t9151a_device_sync_log
			    SET c9151a_void_fl = 'Y'
			      , c9151a_last_updated_date  = SYSDATE
			      , c9151a_last_updated_by = p_user_id
			  WHERE c9151_device_sync_id = v_device_sync_id
			    AND c901_sync_type   = p_ref_type
			    AND c901_language_id = p_language_id
			    and c9151a_sync_end_date IS NULL;
	    	END IF;
	    
		     INSERT
			   INTO t9151a_device_sync_log
			    (
			        c9151a_sync_log_id, c9151_device_sync_id, c901_sync_type
			      , c901_language_id, c9151a_sync_start_date, c9151a_sync_count
			      , c9151a_sync_end_date, c9151a_last_updated_date, c9151a_last_updated_by
			      , c9151a_last_sync_rec
			    )
			    VALUES
			    (
			        S9151A_DEVICE_SYNC_LOG.NEXTVAL, v_device_sync_id, p_ref_type
			        ,p_language_id,  SYSDATE, NVL(p_sync_count,0)
			        ,NULL ,SYSDATE, p_user_id 
			        , p_last_rec
			    ) ;	
		ELSE
			UPDATE t9151a_device_sync_log
			    SET c9151a_sync_end_date = SYSDATE
			      , c9151a_last_updated_date  = SYSDATE
			      , c9151a_last_updated_by = p_user_id
			  WHERE c9151_device_sync_id = v_device_sync_id
			    AND c901_sync_type   = p_ref_type
			    AND c901_language_id = p_language_id
			    and c9151a_sync_end_date IS NULL;    
     	END IF;
     	p_status_id := v_status_id;
     	p_db_total_size := v_db_total_size;
	END gm_sav_device_sync_dtl;
/*****************************************************************
 * Description :  Procedure to clear details of device sync
******************************************************************/
PROCEDURE gm_sav_device_clr_sync (
	p_token_id     IN  t101a_user_token.c101a_token_id%type
  , p_uuid         IN  t9150_device_info.c9150_device_uuid%type
  , p_userid	   IN  T101a_user_token.c101_user_id%TYPE
  , p_sync_type	   IN  t9151_device_sync_dtl.c901_sync_type%TYPE
)
AS
v_device_id T9150_DEVICE_INFO.c9150_device_info_id%TYPE;
BEGIN
	v_device_id := get_device_id(p_token_id, p_uuid);
	IF v_device_id IS NOT NULL 
	THEN
		UPDATE t9151_device_sync_dtl
		   SET c9151_void_fl = 'Y',
		       c9151_last_updated_date = sysdate,
		       c9151_last_updated_by = p_userid
		 WHERE c9150_device_info_id = v_device_id
		 AND c901_sync_type = NVL(p_sync_type,c901_sync_type)
		 AND c9151_void_fl IS NULL;
	ELSE
	GM_RAISE_APPLICATION_ERROR('-20999','374','');
		
	END IF;		
END gm_sav_device_clr_sync;

		/***************************************************************
	 * Description : This procedure will be Inserting/Updating the Master data 
	 * that was updated in the portal. This procedure will be called from trigger.
	 * Eg: The trigger will monitor any change to Part Number table [ Name, Desc, Dtl Desc,Prod Family]
	 * and if there is a change, it will create a record in the Master data updates for the part number.
	 * If the same part already exist, then it will update the Part Number record in the master data update.
	 * Author : Rajeshwaran 
	****************************************************************/
	PROCEDURE gm_sav_master_data_upd (
		p_ref_id     	IN  T2001_MASTER_DATA_UPDATES.C2001_REF_ID%TYPE
	  , p_ref_type    	IN  T2001_MASTER_DATA_UPDATES.C901_REF_TYPE%TYPE
	  , p_action 		IN 	T2001_MASTER_DATA_UPDATES.C901_ACTION%TYPE
	  , p_lang_id 		IN 	T2001_MASTER_DATA_UPDATES.C901_LANGUAGE_ID%TYPE
	  , p_app_id		IN  T2001_MASTER_DATA_UPDATES.C901_APP_ID%TYPE
	  , p_user_id		IN	T2001_MASTER_DATA_UPDATES.C2001_LAST_UPDATED_BY%TYPE
	  , p_system_id		IN	T207_SET_MASTER.C207_SET_ID%TYPE
	  , p_force_upd     IN  VARCHAR DEFAULT NULL
	  , p_company_id    IN  T1900_COMPANY.C1900_COMPANY_ID%TYPE DEFAULT NULL
	)
	AS
	   v_cnt_pub_pc NUMBER;
	BEGIN
		
		-- Check if the System is Published to Product Catalog.
		-- This IF condition is to avoid ORA-04091.
		IF p_force_upd IS NULL
		THEN
			SELECT COUNT(1) INTO v_cnt_pub_pc
			FROM	t207_set_master t207,t207c_set_attribute t207c 
			WHERE	t207.c207_set_id = t207c.c207_set_id 
			AND		t207.c901_set_grp_type = '1600'   --sales Report Grp 
			AND		t207.c207_void_fl IS NULL 
			AND     t207.c207_set_id = p_system_id
			AND		t207c.c207c_void_fl IS NULL 
			AND		t207c.c901_attribute_type = '103119'    --Publish to 
			AND		t207c.c207c_attribute_value = '103085';  -- Product Catalog
		END IF;
		
		--Only when the System is Published to Product Catalog, the updates are to be tracked.
		--When a System is unpublished to PC, the p_force_upd will be passed in, so the System will be marked as voided. 
		IF (v_cnt_pub_pc >0 OR p_force_upd IS NOT NULL)
		THEN
			--If the Same Record [ Ref id, Type] is upadated for the same app and language, then we will update the record.
			-- At any point, the combination (mentioned in where clause) will have only one record.
			UPDATE 	T2001_MASTER_DATA_UPDATES 
			SET		C2001_REF_UPDATED_DATE = SYSDATE,
					C2001_LAST_UPDATED_DATE =  SYSDATE,
					C901_ACTION	= p_action,
					C2001_LAST_UPDATED_BY = p_user_id,
					C1900_COMPANY_ID = p_company_id
			WHERE
					C2001_REF_ID =  p_ref_id
				AND	C901_REF_TYPE = p_ref_type
				AND C901_LANGUAGE_ID = p_lang_id
				AND C901_APP_ID = p_app_id
				 -- PMT#37595/update system entry for all US region companies 
				AND  NVL(c1900_company_id,-999) = NVL(p_company_id,-999)
				AND C2001_VOID_FL IS NULL;
				
				-- 103111 - SET ,4000409 - SET Part,4000736- SET Attribute
				-- IF ref type is SET,SET PART,SET Attribute then check company id condition in where clause
				
			IF (SQL%ROWCOUNT = 0)
		    THEN
		    	INSERT INTO T2001_MASTER_DATA_UPDATES 
		    	(C2001_MASTER_DATA_ID,C2001_REF_ID,C901_REF_TYPE,C901_ACTION,C901_LANGUAGE_ID,C2001_REF_UPDATED_DATE,C901_APP_ID,C2001_LAST_UPDATED_DATE,C2001_LAST_UPDATED_BY,
		    	C1900_COMPANY_ID)
		    	VALUES (S2001_MASTER_DATA_UPDATES.NEXTVAL,p_ref_id,p_ref_type,p_action,p_lang_id,SYSDATE,p_app_id,SYSDATE,p_user_id,p_company_id);
		    
		    END IF;
		END IF;
	END gm_sav_master_data_upd;
	/***************************************************************
	 * Description : This procedure will be void and inserting the Master data 
	 * that was updated in the portal. This procedure will be called from System release to PC.
	****************************************************************/	
	PROCEDURE gm_sav_master_data_upd_frm_tmp (
	        p_ref_type IN t2001_master_data_updates.c901_ref_type%TYPE,
	        p_action   IN t2001_master_data_updates.c901_action%TYPE,
	        p_lang_id  IN t2001_master_data_updates.c901_language_id%TYPE,
	        p_app_id   IN t2001_master_data_updates.c901_app_id%TYPE,
	        p_user_id  IN t2001_master_data_updates.c2001_last_updated_by%TYPE)
	AS
	BEGIN
		/*If System is unpulished then it is sending updates as files uploaded to System are voided.
		 *Actualy, the file sync is not checking that the file's system is publish to Product Catalog.
		 *So, while System is unpublishing,no need to trigger the updates as their files are voided.
		 */
		
		--void action
		 IF  p_action = '4000412' THEN
			RETURN;
		 END IF;	
		
		
	     UPDATE t2001_master_data_updates
	        SET c2001_void_fl        = 'Y', 
	            c2001_last_updated_date = SYSDATE, 
	            c2001_last_updated_by = p_user_id
	      WHERE c2001_void_fl   IS NULL
	        AND c901_ref_type    = p_ref_type
	        AND c901_language_id = p_lang_id
	        AND c901_app_id      = p_app_id
	        AND c2001_ref_id    IN
	        (
	             SELECT C903_UPLOAD_FILE_LIST
	               FROM T903_UPLOAD_FILE_LIST T903, MY_TEMP_KEY_VALUE TEMP
	              WHERE T903.C903_REF_ID     = TO_CHAR (TEMP.MY_TEMP_TXN_KEY)
	                AND T903.C901_REF_GRP    = TO_CHAR (TEMP.MY_TEMP_TXN_VALUE)
	                AND (T903.C901_REF_GRP in (103112,107994,107993) OR   t903.c901_ref_type IN
					    (
					         SELECT c906_rule_value
					           FROM t906_rules
					          WHERE c906_rule_id     = 'FILE_TYPE'
					            AND C906_RULE_GRP_ID = 'PROD_CAT_SYNC'
					            AND c906_void_fl    IS NULL
					    ) )
	                AND T903.C903_DELETE_FL IS NULL
	        ) ;
	    -- To Load all FIles uploaded for Groups.
	     INSERT
	       INTO t2001_master_data_updates
	        (
	            c2001_master_data_id, c2001_ref_id, c901_ref_type
	          , c901_action, c901_language_id, c2001_ref_updated_date
	          , c901_app_id, c2001_last_updated_date, c2001_last_updated_by
	        )
	     SELECT s2001_master_data_updates.NEXTVAL, c903_upload_file_list, p_ref_type
	      , p_action, p_lang_id, SYSDATE
	      , p_app_id, SYSDATE, p_user_id
	       FROM t903_upload_file_list t903, my_temp_key_value temp
	      WHERE t903.c903_ref_id     = TO_CHAR (temp.my_temp_txn_key)
	        AND t903.c901_ref_grp    = TO_CHAR (temp.my_temp_txn_value)
	        AND (T903.C901_REF_GRP in (103112,107994,107993) OR   t903.c901_ref_type IN
					    (
					         SELECT c906_rule_value
					           FROM t906_rules
					          WHERE c906_rule_id     = 'FILE_TYPE'
					            AND C906_RULE_GRP_ID = 'PROD_CAT_SYNC'
					            AND c906_void_fl    IS NULL
					    ) )
	        AND t903.c903_delete_fl IS NULL;
	END gm_sav_master_data_upd_frm_tmp;	
	/**
	 * This Procedure will check if any of the following fields are different between the T4011 and the temp table.
	 * If its different, then the Group Is marked for Updates.
	 */
	PROCEDURE gm_chk_group_detail_update(
	p_group_id  IN 	t4010_group.c4010_group_id%TYPE
	,p_userid  IN 	T2001_MASTER_DATA_UPDATES.C2001_LAST_UPDATED_BY%TYPE
	)
	AS
		v_grp_temp_diff NUMBER;
		v_grp_diff		NUMBER;
		v_temp_cnt		NUMBER;
		v_action		VARCHAR2(10);
		v_to_update		VARCHAR2(1);
		v_system_id		T207_SET_MASTER.C207_SET_ID%TYPE;
		v_company_id   	t1900_company.c1900_company_id%TYPE; 
	--Cursor to get all the Parts that were added to a group which were never released for PC.
	CURSOR cur_new_part
	IS
		SELECT T4011.c205_part_number_id pnum 
		  FROM T4011_GROUP_DETAIL T4011, t2023_part_company_mapping t2023, t205_part_number t205 
		 WHERE C4010_GROUP_ID=p_group_id
		   AND T4011.c205_part_number_id = t2023.c205_part_number_id
           AND t2023.c1900_company_id = v_company_id
           AND t205.c205_part_number_id  = t2023.c205_part_number_id
           AND T205.C205_PRODUCT_FAMILY IN (SELECT C906_RULE_VALUE 
									             FROM T906_RULES 
									             WHERE C906_RULE_ID = 'PART_PROD_FAMILY' 
									             AND C906_RULE_GRP_ID = 'PROD_CAT_SYNC' 
									             AND c906_void_fl IS NULL)
           AND T205.c205_part_number_id IN(
	         SELECT c205_part_number_id
	           FROM T205D_PART_ATTRIBUTE
	          WHERE C901_ATTRIBUTE_TYPE = '80180'
	            AND C205D_VOID_FL      IS NULL)
           AND T205.c205_active_fl IS NOT NULL
	       AND c2023_void_fl IS NULL
		   and T4011.c205_part_number_id  not in (
			 SELECT c205_part_number_id pnum 
			   FROM t207_set_master t207,t208_set_details t208, t207c_set_attribute t207c 
			  WHERE t207.c207_set_id = t207c.c207_set_id 
				AND t207.c901_set_grp_type = '1600'   --sales Report Grp 
                AND t208.c207_set_id = t207.c207_set_id
				AND t207.c207_void_fl IS NULL 
				AND t207c.c207c_void_fl IS NULL 
				AND t207.c207_set_id =t207.c207_set_id
				AND t207c.c901_attribute_type = '103119'    --Publish to 
				AND t207c.c207c_attribute_value ='103085' ); -- Product Catalog

	BEGIN
	SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL')) 
	  INTO v_company_id 
	  FROM dual; 
	
		SELECT COUNT(1) INTO v_grp_temp_diff FROM (
			SELECT
			C205_PART_NUMBER_ID,C4010_GROUP_ID,C901_PART_PRICING_TYPE,C4010_GROUP_ID,C4011_PRIMARY_PART_LOCK_FL FROM MY_TEMP_T4011_GROUP_DETAIL
			WHERE C4010_GROUP_ID = p_group_id		
			MINUS		
			SELECT
			C205_PART_NUMBER_ID,C4010_GROUP_ID,C901_PART_PRICING_TYPE,C4010_GROUP_ID,C4011_PRIMARY_PART_LOCK_FL FROM T4011_GROUP_DETAIL
			WHERE C4010_GROUP_ID = p_group_id
		);
		
		SELECT COUNT(1) INTO v_grp_diff FROM (
			SELECT
			C205_PART_NUMBER_ID,C4010_GROUP_ID,C901_PART_PRICING_TYPE,C4010_GROUP_ID,C4011_PRIMARY_PART_LOCK_FL FROM T4011_GROUP_DETAIL
			WHERE C4010_GROUP_ID = p_group_id		
			MINUS		
			SELECT
			C205_PART_NUMBER_ID,C4010_GROUP_ID,C901_PART_PRICING_TYPE,C4010_GROUP_ID,C4011_PRIMARY_PART_LOCK_FL FROM MY_TEMP_T4011_GROUP_DETAIL
			WHERE C4010_GROUP_ID = p_group_id
		);
		
		SELECT COUNT(1) INTO v_temp_cnt 
		FROM MY_TEMP_T4011_GROUP_DETAIL
		WHERE C4010_GROUP_ID = p_group_id;
		
		
		--If New Parts are added to the Group, then this condition will satisfy.
		IF (v_grp_diff +v_grp_temp_diff >0)
		THEN
			v_to_update := 'Y';
		END IF;
		
		-- If the temp table does not have any records, it means the parts are being added for first time to the group.
		IF (v_temp_cnt = 0)
		THEN
			v_action := '103122'; -- New
			v_to_update := 'Y';
		ELSE
			v_action := '103123'; -- Updated
		END IF; 
		
		select C207_SET_ID INTO v_system_id 
		from T4010_GROUP 
		where C4010_GROUP_ID=p_group_id 
		AND C4010_VOID_FL IS NULL;
		
		-- When there is a difference or if the Group is getting parts for first time,we have to consider it as Group Part updates.
		IF (v_to_update ='Y')
		THEN
			 gm_pkg_pdpc_prodcattxn.gm_sav_master_data_upd (p_group_id , '4000408', v_action, '103097',
                                                  			'103100', p_userid
                                                  			, v_system_id) ;
		END IF;
		
		--Loop all the parts that were added to Group which were never released for PC earlier.
		--These parts are to be added to PC so it will show up along with the Group.
		FOR new_part IN cur_new_part
		    LOOP
		    	-- 4000411 Part Number ref type, 103097 English, 103100 Product Catalog, 103122 new
		        gm_pkg_pdpc_prodcattxn.gm_sav_master_data_upd(new_part.pnum
									     , '4000411', '103122' 
									     ,'103097','103100'
									     ,p_userid
									     ,'' -- No System ID to be passed.
									     ,'Y'
									     );
	    END LOOP;
	    
		DELETE FROM MY_TEMP_T4011_GROUP_DETAIL;
			
	END gm_chk_group_detail_update;
	/**
	 * This Procedure will check if any of the following fields are different between the T208 and the temp table.
	 * If its different, then the Set Is marked for Updates.
	 */
	PROCEDURE gm_chk_set_detail_update(
	p_setid    IN t208_set_details.c207_set_id%TYPE
	,p_userid  IN T2001_MASTER_DATA_UPDATES.C2001_LAST_UPDATED_BY%TYPE
	)
	AS
	
	v_set_det_diff 	NUMBER;
	v_action		VARCHAR2 (20);
	v_set_type		VARCHAR2 (20);
	v_system_id		T207_SET_MASTER.C207_SET_ID%TYPE;
	v_temp_cnt		NUMBER;
	v_part_cnt		NUMBER;
	v_to_update		VARCHAR2(1) :='N';
	v_company_id   	t1900_company.c1900_company_id%TYPE; 
	--Cursor to get all the Parts that were added to a set which were never released for PC.
	
	/* Since, System relese to PC is not checking when part sync. So, the below code is commented 
	CURSOR cur_new_part
	IS
		SELECT t208.c205_part_number_id pnum 
		  FROM t208_set_details t208, t2023_part_company_mapping t2023, t205_part_number t205 
		 WHERE c207_set_id=p_setid
		   AND t208.c205_part_number_id = t2023.c205_part_number_id
           AND t2023.c1900_company_id = v_company_id --US company
           AND t205.c205_part_number_id  = t2023.c205_part_number_id
           AND T205.C205_PRODUCT_FAMILY IN (SELECT C906_RULE_VALUE
										   FROM T906_RULES
										  WHERE C906_RULE_ID     = 'PART_PROD_FAMILY'
											AND C906_RULE_GRP_ID = 'PROD_CAT_SYNC'
											AND c906_void_fl    IS NULL)
           AND t205.c205_part_number_id IN(
	         SELECT c205_part_number_id
	           FROM T205D_PART_ATTRIBUTE
	          WHERE C901_ATTRIBUTE_TYPE = '80180'
	            AND C205D_VOID_FL      IS NULL)
           AND T205.c205_active_fl IS NOT NULL
	       AND c2023_void_fl IS NULL
		   AND t208.c205_part_number_id  not in (
			 SELECT c205_part_number_id pnum 
			   FROM t207_set_master t207,t208_set_details t208, t207c_set_attribute t207c 
			  WHERE t207.c207_set_id = t207c.c207_set_id 
				AND t207.c901_set_grp_type = '1600'   --sales Report Grp 
                AND t208.c207_set_id = t207.c207_set_id
				AND t207.c207_void_fl IS NULL 
				AND t207c.c207c_void_fl IS NULL 
				AND t207.c207_set_id =t207.c207_set_id
				AND t207c.c901_attribute_type = '103119'    --Publish to 
				AND t207c.c207c_attribute_value = '103085' ); -- Product Catalog

	
*/
	BEGIN
		SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL')) 
		  INTO v_company_id 
		  FROM dual;
		  
		SELECT COUNT(1) INTO v_set_det_diff FROM (
			SELECT C205_PART_NUMBER_ID, C208_SET_QTY,  C208_INSET_FL, C208_VOID_FL FROM  MY_TEMP_T208_SET_DETAILS  WHERE C207_SET_ID = p_setid	
			MINUS
			SELECT C205_PART_NUMBER_ID, C208_SET_QTY,  C208_INSET_FL, C208_VOID_FL FROM  T208_SET_DETAILS WHERE C207_SET_ID = p_setid
			
		);
		
		SELECT COUNT(1) INTO v_temp_cnt 
		FROM MY_TEMP_T208_SET_DETAILS
		WHERE C207_SET_ID = p_setid;
		
		SELECT COUNT(1) INTO v_part_cnt 
		FROM T208_SET_DETAILS
		WHERE C207_SET_ID = p_setid;
		
		--If New Parts are added to the Set, then this condition will satisfy.
		IF (v_part_cnt > v_temp_cnt)
		THEN
			v_to_update := 'Y';
		END IF;
		
		-- If the temp table does not have any records, it means the parts are being added for first time to the set.
		IF (v_temp_cnt = 0)
		THEN
			v_action := '103122'; -- New
			v_to_update := 'Y';
		ELSE
			v_action := '103123'; -- Updated
		END IF; 
		
		
		BEGIN
			SELECT c901_set_grp_type,C207_SET_SALES_SYSTEM_ID INTO v_set_type, v_system_id
			FROM T207_SET_MASTER 
			WHERE C207_SET_ID=p_setid
			AND C207_EXCLUDE_IPAD_FL IS NULL
			AND C207_VOID_FL IS NULL;
			
			EXCEPTION WHEN NO_DATA_FOUND THEN
				RETURN;
		END;
		
		-- When there is a difference or if the set is getting parts for first time,we have to consider it as set updates.
		IF (v_set_det_diff>0 OR v_to_update ='Y')
		THEN
			 gm_pkg_pdpc_prodcattxn.gm_sav_master_data_upd (p_setid , '4000409', v_action, '103097',
                                                  			'103100', p_userid
                                                  			, v_system_id,'Y') ;
		END IF;
		
		--Loop all the parts that were added to set which were never released for PC earlier.
		--These parts are to be added to PC so it will show up along with the set.
		/*	Since, System relese to PC is not checking when part sync. So, the below code is commented 
		FOR new_part IN cur_new_part
		    LOOP
		    	-- 4000411 Part Number ref type, 103097 English, 103100 Product Catalog, 103122 new
		        gm_pkg_pdpc_prodcattxn.gm_sav_master_data_upd(new_part.pnum
									     , '4000411', '103122' 
									     ,'103097','103100'
									     ,p_userid
									     ,'' -- No System ID to be passed.
									     ,'Y'
									     );
	    END LOOP;
	    */
		DELETE FROM MY_TEMP_T208_SET_DETAILS;
		
	END gm_chk_set_detail_update;
	
	/**
	 * This procedure will be called from gm_pkg_pd_language.gm_sav_lang_details
	 * When the language information is updated, this procedure will be called.
	 * Based on the ref type, we will find the system and call the procedure to load the updates for the language.
	 */
	PROCEDURE gm_sav_language_upd(
		p_ref_id     	IN  T2001_MASTER_DATA_UPDATES.C2001_REF_ID%TYPE
	  , p_ref_type    	IN  T2001_MASTER_DATA_UPDATES.C901_REF_TYPE%TYPE
	  , p_action 		IN 	T2001_MASTER_DATA_UPDATES.C901_ACTION%TYPE
	  , p_lang_id 		IN 	T2001_MASTER_DATA_UPDATES.C901_LANGUAGE_ID%TYPE
	  , p_app_id		IN  T2001_MASTER_DATA_UPDATES.C901_APP_ID%TYPE
	  , p_user_id		IN	T2001_MASTER_DATA_UPDATES.C2001_LAST_UPDATED_BY%TYPE
	)
	AS
		v_system_id 	T207_SET_MASTER.C207_SET_ID%TYPE;
		v_ref_type 		t2000_master_metadata.c901_ref_type%TYPE;
		v_cnt			NUMBER;
		v_company_id   	t1900_company.c1900_company_id%TYPE; 
	BEGIN
		SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL')) 
		  INTO v_company_id 
		  FROM dual;
		  
		BEGIN
			
			SELECT DECODE(p_ref_type,103093,103109,103092,103108,103094,103110,103091,103111,103090,4000411)
			INTO v_ref_type 
			FROM DUAL;
			
			-- Get the System ID based on the Ref Type.
			--Part Number	
			IF (v_ref_type = '4000411')
			THEN
			
			 SELECT COUNT (1)
			   INTO v_cnt
			   FROM t2023_part_company_mapping t2023, t205_part_number t205
			  WHERE t205.c205_part_number_id  = p_ref_id
			    AND t205.c205_part_number_id  = t2023.c205_part_number_id
			    AND T205.C205_PRODUCT_FAMILY IN (SELECT C906_RULE_VALUE
										   FROM T906_RULES
										  WHERE C906_RULE_ID     = 'PART_PROD_FAMILY'
											AND C906_RULE_GRP_ID = 'PROD_CAT_SYNC'
											AND c906_void_fl    IS NULL)
			    AND t205.c205_part_number_id IN
			    (
			         SELECT c205_part_number_id
			           FROM T205D_PART_ATTRIBUTE
			          WHERE C901_ATTRIBUTE_TYPE = '80180'
			            AND c205_part_number_id = p_ref_id
			            AND C205D_VOID_FL      IS NULL
			    )
			    AND T205.c205_active_fl IS NOT NULL
			    AND c1900_company_id     = v_company_id
			    AND c2023_void_fl       IS NULL;
				IF v_cnt = 0 THEN
					RETURN;
				END IF;
				v_system_id := TRIM(get_set_id_from_part(p_ref_id));
			--Group
			ELSIF(v_ref_type = '103110')
			THEN
				SELECT C207_SET_ID INTO v_system_id 
				FROM T4010_GROUP 
				WHERE c4010_VOID_FL IS NULL 
				AND c901_type          = 40045 --Forecast/Demand Group
        		AND C4010_PUBLISH_FL  IS NOT NULL
        		AND c4010_inactive_fl IS NULL
				AND   C4010_GROUP_ID=p_ref_id;
			--Set
			ELSIF(v_ref_type = '103111')
			THEN
				 SELECT t207.C207_SET_SALES_SYSTEM_ID INTO v_system_id
				   FROM t207_set_master t207, T2080_SET_COMPANY_MAPPING T2080
				  WHERE T207.c901_set_grp_type  = 1601
				    AND T207.c901_status_id     = '20367' --Approved status
				    AND T207.C207_SET_ID        = T2080.C207_SET_ID
				    AND T2080.C2080_VOID_FL    IS NULL
				    AND T2080.C1900_COMPANY_ID = v_company_id
				    AND T207.C207_EXCLUDE_IPAD_FL IS NULL
				    AND t207.C207_VOID_FL      IS NULL;
			--System
			ELSIF(v_ref_type = '103108')
			THEN
				v_system_id := p_ref_id;
			--Project
			--To Handle it later
			
			--ELSIF(p_ref_type = '103093')
			END IF;
		EXCEPTION WHEN OTHERS
		THEN
			RETURN;
		END;
		
		IF(v_system_id IS NOT NULL)
		THEN
			gm_pkg_pdpc_prodcattxn.gm_sav_master_data_upd(p_ref_id
													     ,v_ref_type
													     ,p_action 
													     ,p_lang_id
													     ,p_app_id
													     ,p_user_id
													     ,v_system_id
														);
		END IF;								     
	END gm_sav_language_upd;

	/**
	 * This procedure will be called when a System is either published for PC or pulled down from PC.
	 * This procedure will get all the Set, Group, Parts , Files for the Systemand load it to the Updates Table.
	 * When a System is pulled back , only the System will be marked as Voided.  
	 */
	PROCEDURE gm_sav_new_system_update(
									   p_system_id 	IN 	T207_SET_MASTER.C207_SET_ID%TYPE,
									   p_user_id 	IN	T2001_MASTER_DATA_UPDATES.C2001_LAST_UPDATED_BY%TYPE,
									   p_action		IN	T2001_MASTER_DATA_UPDATES.C901_ACTION%TYPE
									  )
	AS
		v_frc_update VARCHAR2(1);
	BEGIN
		-- When a System is Published to PC, this condition will satisfy and all the SEt, Grp,Part for the System will beloaded to updates.
		IF (p_action = '103122')
		THEN
			gm_sav_new_group_update(p_system_id,p_user_id,p_action);
			gm_sav_new_set_update(p_system_id,p_user_id,p_action);
			gm_sav_new_part_update(p_system_id,p_user_id,p_action);
		END IF;
		/*Keywords synced for System which is published to Product Catalog.
		 *Keywords synced for files which uploaded in System which is published to Product Catalog.
		 *This prc gm_sav_new_system_update will invoke when  System is publish/unpublish to PC.
		 *if the system is publish/unpublish to PC then their Keywords also need to update in iPad.
		 *The group, set and part sync is not based System which is published to PC.
		 */
		gm_sav_new_keyword_update(p_system_id,p_user_id,p_action);
		--When the action is void, we have to forecully put a record in the updates table.
		SELECT DECODE(p_action,'4000412','Y','') INTO v_frc_update FROM DUAL;
		
		gm_pkg_pdpc_prodcattxn.gm_sav_master_data_upd (p_system_id, '103108', p_action, '103097',
                                                  '103100', p_user_id
                                                  , p_system_id
                                                  ,v_frc_update ) ;

                                                 
	   DELETE FROM MY_TEMP_KEY_VALUE;
	   INSERT INTO MY_TEMP_KEY_VALUE(MY_TEMP_TXN_KEY,MY_TEMP_TXN_VALUE) VALUES(p_system_id,'103092'); -- 103092 System [REF GRP]
	   --To Load all FIles uploaded for System.
       --40004100-Images,103097-English,103100-Product Catalog 
	   gm_sav_master_data_upd_frm_tmp('4000410',p_action,'103097','103100',p_user_id);
       
	END gm_sav_new_system_update;
	
	/**
	 * This procedure will get all the Group associated to the System and push it as updates to device.
	 */
	PROCEDURE gm_sav_new_group_update(
									   p_system_id 	IN 	T207_SET_MASTER.C207_SET_ID%TYPE,
									   p_user_id 	IN	T2001_MASTER_DATA_UPDATES.C2001_LAST_UPDATED_BY%TYPE,
									   p_action		IN	T2001_MASTER_DATA_UPDATES.C901_ACTION%TYPE
									  )
	AS
	--cur_groups		TYPES.CURSOR_TYPE;
	CURSOR cur_groups
	IS
	     SELECT t4010.c4010_group_id groupid
      
       FROM t4010_group t4010
      WHERE t4010.c207_set_id        = p_system_id
        AND t4010.c901_type          = 40045 --Forecast/Demand Group
        AND t4010.c4010_void_fl     IS NULL
        AND T4010.C4010_PUBLISH_FL  IS NOT NULL
        AND t4010.c4010_inactive_fl IS NULL;
	BEGIN
		-- Get all the Groups for the System.
		--gm_pkg_pd_system_rpt.gm_fch_sys_groups(p_system_id,cur_groups);
		
		DELETE FROM MY_TEMP_KEY_VALUE;
		FOR v_cur IN cur_groups
		LOOP
        	INSERT INTO MY_TEMP_KEY_VALUE(MY_TEMP_TXN_KEY,MY_TEMP_TXN_VALUE) VALUES(v_cur.groupid,'103094'); -- 103094 Group [REF GRP]
        	-- 103110 Group ref type, 103097 English, 103100 Product Catalog, 103122 new
        	gm_pkg_pdpc_prodcattxn.gm_sav_master_data_upd (
        										v_cur.groupid,'103110', 
        										p_action, '103097',
                                              	'103100', p_user_id
					      						,p_system_id,'') ;
			-- 4000408 Group Part					      						
			gm_pkg_pdpc_prodcattxn.gm_sav_master_data_upd (
        										v_cur.groupid,'4000408', 
        										p_action, '103097',
                                              	'103100', p_user_id
					      						,p_system_id,'') ;
			

	    END LOOP;

	    -- To Load all FIles uploaded for Groups.
	    --40004100-Images,103097-English,103100-Product Catalog 
	    gm_sav_master_data_upd_frm_tmp('4000410',p_action,'103097','103100',p_user_id);	    
		    	
	END gm_sav_new_group_update;
	
	/** 
	 * This procedure gets all the Sets associated to the System and loads it to the Master Updates table for the sets,so it gets
	 * synced to the Device.
	 */
	
	PROCEDURE gm_sav_new_set_update(
						   p_system_id 	IN 	T207_SET_MASTER.C207_SET_ID%TYPE,
						   p_user_id 	IN	T2001_MASTER_DATA_UPDATES.C2001_LAST_UPDATED_BY%TYPE,
						   p_action		IN	T2001_MASTER_DATA_UPDATES.C901_ACTION%TYPE
							)
	AS
	v_company_id   	t1900_company.c1900_company_id%TYPE; 
    v_cmp_exc_fl    t906_rules.c906_rule_value%TYPE;
    v_set_sync_val  t906_rules.c906_rule_value%TYPE;

	CURSOR cur_sets(v_cmp_exc_fl    t906_rules.c906_rule_value%TYPE)		
	IS	
		SELECT t207.C207_SET_ID setid
		   FROM t207_set_master t207, T2080_SET_COMPANY_MAPPING T2080
		  WHERE T207.c901_set_grp_type  = 1601
		    AND T207.c901_status_id     = '20367' --Approved status
		    AND T207.C207_SET_ID        = T2080.C207_SET_ID
		    AND T2080.C2080_VOID_FL    IS NULL
		    AND T2080.C1900_COMPANY_ID =v_company_id
		    AND T207.C207_SET_SALES_SYSTEM_ID = p_system_id
		 -- AND t207.c207_exclude_ipad_fl IS NULL
		    AND DECODE(v_cmp_exc_fl,'Y', v_set_sync_val ,NVL(T207.C207_EXCLUDE_IPAD_FL,v_set_sync_val)) = v_set_sync_val
		    AND t207.c207_void_fl      IS NULL;
	
	BEGIN
	SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL')) INTO v_company_id FROM dual;
	
	--Fetch company exclude flag
	SELECT NVL(get_rule_value_by_company(v_company_id,'EXC_SET_IPAD_FL',v_company_id),'N')INTO v_cmp_exc_fl FROM DUAL;
	
    --Fetch exclude flag value
	SELECT get_rule_value(v_cmp_exc_fl,'IPAD_SET_SYNC')INTO v_set_sync_val FROM DUAL;
	
		--  gm_procedure_log('v_cmp_exc_fl', v_cmp_exc_fl);
		  
		--IN this procedure[gm_fch_sys_sets] when new column is added,we have to make changes to this cursor as well,else it will fail. 
		--gm_pkg_pd_system_rpt.gm_fch_sys_sets(p_system_id,cur_sets);
		DELETE FROM MY_TEMP_KEY_VALUE;
		FOR v_cur IN cur_sets(v_cmp_exc_fl)
		LOOP
        -- Loading the Set ID and Ref Group type to the Temp table, as it will be used to get all the images uploaded to the Set.
        	INSERT INTO MY_TEMP_KEY_VALUE(MY_TEMP_TXN_KEY,MY_TEMP_TXN_VALUE) VALUES(v_cur.setid,'103091' ); -- 103091  Set [REF GRP]
        	
        	-- When ever a System is published to PC ,we have to 
        	-- 103111 Set ref type, 103097 English, 103100 Product Catalog, 103122 new
        	gm_pkg_pdpc_prodcattxn.gm_sav_master_data_upd (	v_cur.setid, '103111', p_action, '103097',
                                                  		 	'103100', p_user_id
                                                  			, p_system_id,'') ;
			-- 4000409  Set Part					      						
			gm_pkg_pdpc_prodcattxn.gm_sav_master_data_upd (
        										v_cur.setid,'4000409', 
        										p_action, '103097',
                                              	'103100', p_user_id
					      						,p_system_id,'') ;
			
	    END LOOP;
	    
	    -- To Load all FIles uploaded for Set.
	    --40004100-Images,103097-English,103100-Product Catalog 
	    gm_sav_master_data_upd_frm_tmp('4000410',p_action,'103097','103100',p_user_id);
		    	
	END gm_sav_new_set_update;
	
	/**
	 * This procedure gets all the parts for the System and loads to the Master Updates table for Part.
	 */
	PROCEDURE gm_sav_new_part_update (
        p_system_id  IN t207_set_master.c207_set_id%type,
        p_user_id IN t207_set_master.c207_created_by%type,
        p_action	IN T2001_MASTER_DATA_UPDATES.C901_ACTION%TYPE
        )
	AS
    CURSOR allparts_cur
    IS
         SELECT ref_id pnum From
         my_temp_prod_cat_info WHERE ref_id IS NOT NULL;

    	BEGIN
	    	-- Get all the Parts for the System.
	    	gm_pkg_pdpc_prodcatrpt.gm_fch_all_parts_tosync(p_system_id);
	    	DELETE FROM MY_TEMP_KEY_VALUE;
	    	-- Loop all the Parts and load it to the Master Updates Table.
		    FOR allparts IN allparts_cur
		    LOOP
               -- Loading the pnum and Ref Group type to the Temp table, as it will be used to get all the images uploaded to the Part Number.
        	   INSERT INTO MY_TEMP_KEY_VALUE(MY_TEMP_TXN_KEY,MY_TEMP_TXN_VALUE) VALUES(allparts.pnum,'103090' ); -- 103091  Part Number [REF GRP]		    	
		       -- 4000411 Part Number ref type, 103097 English, 103100 Product Catalog, 103122 new
		        gm_pkg_pdpc_prodcattxn.gm_sav_master_data_upd(allparts.pnum
									     , '4000411', p_action 
									     ,'103097','103100'
									     ,p_user_id
									     ,p_system_id
									     );
	    END LOOP;
	    -- To Load all FIles uploaded for Part Number.
	    --40004100-Images,103097-English,103100-Product Catalog 
	    gm_sav_master_data_upd_frm_tmp('4000410',p_action,'103097','103100',p_user_id);	    
	END gm_sav_new_part_update;
	/**
	 * This procedure gets all keywords for the System and loads to the Master Updates table.
	 */
	PROCEDURE gm_sav_new_keyword_update (
        p_system_id  IN t207_set_master.c207_set_id%type,
        p_user_id    IN t207_set_master.c207_created_by%type,
        p_action	 IN T2001_MASTER_DATA_UPDATES.C901_ACTION%TYPE
        )
	AS
    CURSOR keyword_cur
    IS
	 SELECT t2050.c2050_keyword_ref_id keywordid
	   FROM t2050_keyword_ref t2050
	  WHERE t2050.c2050_void_fl IS NULL
	    AND t2050.c2050_ref_id   = p_system_id
	    AND t2050.c901_ref_type  = '103108' --System
	    AND t2050.c2050_ref_id   = t2050.c2050_ref_id
	UNION ALL
	 SELECT t2050.c2050_keyword_ref_id
	   FROM t2050_keyword_ref T2050, t903_upload_file_list t903
	  WHERE t2050.c901_ref_type  = '4000410' --File
	    AND t2050.c2050_ref_id   = t2050.c2050_ref_id
	    AND t2050.c2050_ref_id   = t903.c903_upload_file_list
	    AND t903.c903_ref_id     = p_system_id
	    AND t2050.c2050_void_fl IS NULL
	    AND t903.c903_delete_fl IS NULL; 
	v_frc_update VARCHAR2(1);
    	BEGIN

			SELECT DECODE(p_action,'4000412','Y','') INTO v_frc_update FROM DUAL;
		    FOR v_cur IN keyword_cur
		    LOOP
		        -- 4000442 Keyword ref type, 103097 English, 103100 Product Catalog, 103122 new
		        gm_pkg_pdpc_prodcattxn.gm_sav_master_data_upd(v_cur.keywordid
									     , '4000442', p_action 
									     ,'103097','103100'
									     ,p_user_id
									     ,p_system_id
									     ,v_frc_update);
	    	END LOOP;
    
	END gm_sav_new_keyword_update;	
	/**
	 * This procedure will validate code id is exist in code lookup table 
	 */	
	PROCEDURE gm_validate_code_id(
		p_code_id  IN  T901_Code_Lookup.C901_CODE_ID%TYPE
	  , p_code_grp IN  T901_Code_Lookup.C901_Code_Grp%TYPE
	  , p_error_id IN  VARCHAR2
	)
	AS 
		v_cnt		  NUMBER := 0;
	BEGIN
			
		SELECT COUNT (1) INTO v_cnt
		  FROM T901_Code_Lookup
		 WHERE C901_CODE_ID   = p_code_id
		   AND C901_Code_Grp  = p_code_grp
		   AND C901_ACTIVE_FL = 1
		   AND C901_VOID_FL  IS NULL;	
		   
		IF v_cnt = 0
		THEN
			raise_application_error (p_error_id, '');  
		END IF;
	END gm_validate_code_id;	
END gm_pkg_pdpc_prodcattxn;
/
