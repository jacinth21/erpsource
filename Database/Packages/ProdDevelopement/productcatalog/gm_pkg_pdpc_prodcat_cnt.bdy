/* Formatted on 2009/02/16 12:30 (Formatter Plus v4.8.0) */
--@"C:\Database\Packages\ProdDevelopement\productcatalog\gm_pkg_pdpc_prodcat_fnc.bdy";
CREATE OR REPLACE
PACKAGE BODY gm_pkg_pdpc_prodcat_cnt
IS
	FUNCTION GET_DB_COUNT(
		  p_ref_type   IN  NUMBER,
		  p_comp_id	   IN  t1900_company.c1900_company_id%TYPE,
		  p_user_id	   IN  t101a_user_token.c101_user_id%TYPE,
		  p_sync_type  IN  t9150_device_info.c9150_device_uuid%TYPE DEFAULT NULL
	)
	RETURN NUMBER
	IS
		v_count NUMBER;
	BEGIN
		IF p_ref_type =103107 THEN -- CODE LOOKUP
			v_count := get_cnt_all_codegroup_tosync;
		ELSIF p_ref_type =4000443 THEN -- RULE
			v_count := get_cnt_all_rulegrps_tosync;	
		ELSIF p_ref_type =103110 THEN -- GROUP
			IF p_sync_type = 'SYNCJSON' THEN
				v_count := get_count_group(p_comp_id);
			ELSE
				v_count := get_cnt_all_group_tosync();
			END IF;
		ELSIF p_ref_type =4000408 THEN -- GROUP PART
			IF p_sync_type = 'SYNCJSON' THEN
				v_count := get_count_group_details(p_comp_id);	
			ELSE
				v_count := get_cnt_all_group_dtl_tosync();	
			END IF;
		ELSIF p_ref_type = 4000411 THEN -- PART NUMBER
			IF p_sync_type = 'SYNCJSON' THEN
				v_count := get_count_parts(p_comp_id);
			ELSE
				v_count := get_cnt_all_parts_tosync();
			END IF;
		ELSIF p_ref_type = 4000827 THEN -- PARTATTR
			IF p_sync_type = 'SYNCJSON' THEN
				v_count := get_count_parts_attribute(p_comp_id);
			ELSE
				v_count := get_cnt_all_parts_attr_tosync();
			END IF;
		ELSIF p_ref_type = 103111 THEN -- SET
			IF p_sync_type = 'SYNCJSON' THEN
				v_count := get_count_setmaster(p_comp_id);	
			ELSE
				v_count := get_cnt_all_setmaster_tosync();	
			END IF;
		ELSIF p_ref_type = 4000409 THEN -- SET PART
			IF p_sync_type = 'SYNCJSON' THEN
				v_count := get_count_set_details(p_comp_id);
			ELSE
				v_count := get_cnt_all_set_dtl_tosync();
			END IF;
		ELSIF p_ref_type = 4000736 THEN -- SET Attribute
			IF p_sync_type = 'SYNCJSON' THEN
				v_count := get_count_set_attribute(p_comp_id);
			ELSE
				v_count := get_cnt_all_set_attr_tosync();
			END IF;
		ELSIF p_ref_type = 103108 THEN -- SYSTEM
			IF p_sync_type = 'SYNCJSON' THEN
				v_count := get_count_systems(p_comp_id);
			ELSE
				v_count := get_cnt_all_system_tosync();
			END IF;
		ELSIF p_ref_type = 4000416 THEN -- System ATTRIBUTE
			IF p_sync_type = 'SYNCJSON' THEN
				v_count := get_count_system_attribute(p_comp_id);	
			ELSE
				v_count := get_cnt_all_system_attr_tosync();
			END IF;
		ELSIF p_ref_type = 4000410 THEN -- FILE
			IF p_sync_type = 'SYNCJSON' THEN
				v_count := get_count_files_info;
			ELSE
				v_count := get_cnt_all_uploaded_fileinfo;
			END IF;
		ELSIF p_ref_type = 4000442 THEN -- KEYWORD REF
			v_count := get_cnt_all_keywords_tosync;	
		ELSIF p_ref_type = 4000519 THEN --  ACCOUNT			
			v_count := get_cnt_accts_detail_tosync;	
 		ELSIF p_ref_type = 4000520 THEN --  SalesRep	
 			v_count := get_cnt_salesrep_detail_tosync;
 		ELSIF p_ref_type = 4000629 THEN --  Associate SalesRep	 			
 			v_count := get_cnt_assocrep_detail_tosync;
 		ELSIF p_ref_type = 4000521 THEN --  Distributor	  			
 			v_count := get_cnt_dist_detail_tosync;
 		ELSIF p_ref_type = 4000530 THEN --  ACCOUNTGPOMAP	  			
 			v_count := get_cnt_acctgpomap_tosync;
 		ELSIF p_ref_type = 4000522 THEN --  ADDRESS	 			
 			v_count := get_cnt_address_detail_tosync;
 		ELSIF p_ref_type = 4000535 THEN --  user	  			
 			v_count := get_cnt_user_tosync;	
 		ELSIF p_ref_type = 4000445 THEN --  PARTY	 			
 			v_count := get_cnt_all_parites_tosync;
 		ELSIF p_ref_type = 4000448 THEN --  Party Contact	  			
 			v_count := get_cnt_party_contact_tosync;
 		ELSIF p_ref_type = 4000533 THEN --  Case	  			
 			v_count :=get_cnt_case_sync(p_user_id);
 		ELSIF p_ref_type = 4000538 THEN --  Case Attribute	  			
 			v_count :=get_cnt_case_attr_sync(p_user_id);
 		ELSIF p_ref_type = 10306170 THEN --  Activity		
 			v_count :=get_cnt_act_sync;	
 		ELSIF p_ref_type = 10306171 THEN --  Activity Party		
 			v_count :=get_cnt_actparty_sync;		
 		ELSIF p_ref_type = 10306172 THEN --  Activity Attribute
 			v_count :=get_cnt_actattr_sync;		
 		ELSIF p_ref_type = 10306161 THEN --  Surgeon Party
 			v_count :=get_cnt_surgparty_sync;		
 		ELSIF p_ref_type = 10306162 THEN --  Surgeon Attribute
 			v_count :=get_cnt_surgattr_sync;		
 		ELSIF p_ref_type = 10306164 THEN --  Surgeon Hospital Affiliation
 			v_count :=get_cnt_surgaffl_sync;	
 		ELSIF p_ref_type = 10306163 THEN --  Surgeon Surgical Activity
 			v_count :=get_cnt_surgsurgical_sync;	
 		ELSIF p_ref_type = 10306168 THEN --  Surgeon Documents
 			v_count :=get_cnt_surgdocs_sync;	
 		ELSIF p_ref_type = 10306176 THEN --  CRF
 			v_count :=get_cnt_crf_sync;		
 		ELSIF p_ref_type = 10306210 THEN --  Criteria
 			v_count :=get_cnt_criteria_sync;
 		ELSIF p_ref_type = 10306209 THEN --  Criteria Attribute
 			v_count :=get_cnt_criteriaattr_sync;
 		ELSIF p_ref_type = 103109 THEN --  Projects
 			v_count :=get_cnt_project_sync;
 		ELSIF p_ref_type = 10306206 THEN --  CRF Approvers
 			v_count :=get_cnt_crfappr_sync;
 		ELSIF p_ref_type = 10306169 THEN --  Territory Mapping
 			v_count :=get_cnt_terrmap_sync;
 		ELSIF p_ref_type = 26230802 THEN --  Codelookup exclusion
 			v_count :=get_cnt_codelookup_excl_sync;
 		ELSIF p_ref_type = 26230803 THEN --  RFS company mapping
 			v_count :=get_cnt_rfs_mapping_sync;
 		ELSIF p_ref_type = 26230804 THEN --  Codelookup Access 
 			v_count :=get_cnt_codelookup_access_sync;
 		ELSIF p_ref_type = 7000443 THEN --  security event Access 
 			v_count :=get_cnt_security_event_sync(p_user_id);
 		ELSIF p_ref_type = 4001234	THEN -- Tag Sync
 			v_count :=get_cnt_all_tag_tosync;
 			
 		ELSE
			v_count := 0;
		END IF;
		RETURN v_count;   
		    EXCEPTION WHEN OTHERS THEN
		    	RETURN 0;		    
	END GET_DB_COUNT;
/*
 *   Code Lookup 
 */
FUNCTION get_cnt_all_codegroup_tosync
    RETURN NUMBER
IS
    v_count NUMBER;
BEGIN
     SELECT COUNT (c901_code_id)
       INTO v_count
       FROM T901_CODE_LOOKUP t901
      WHERE C901_CODE_GRP IN(select C901f_CODE_GRP from t901f_code_lkp_ipad_map where C901_LOOKUP_TYPE = 103107 AND C901F_VOID_FL IS NULL)--103107 FOR CODE LOOKUP SYNC
        AND t901.c901_void_fl    IS NULL;
       RETURN v_count;
END get_cnt_all_codegroup_tosync;

FUNCTION get_cnt_codelookup_excl_sync
    RETURN NUMBER
IS
    v_count NUMBER;
BEGIN
     SELECT COUNT (T901D.c901_code_id)
       INTO v_count
       FROM T901_CODE_LOOKUP t901,T901D_CODE_LKP_COMPANY_EXCLN T901D
      WHERE t901.c901_code_grp in (select C901f_CODE_GRP from t901f_code_lkp_ipad_map where C901_LOOKUP_TYPE = 26230802 AND C901F_VOID_FL IS NULL)--26230802 FOR CODE LOOKUP SYNC
    	AND  t901.c901_code_id = t901d.c901_code_id
        AND t901.c901_void_fl    IS NULL
        AND t901d.c901d_void_fl    IS NULL;
       RETURN v_count;
END get_cnt_codelookup_excl_sync;

/*Codelookup access sync*/
FUNCTION get_cnt_codelookup_access_sync
    RETURN NUMBER
IS
    v_count NUMBER;
BEGIN
	   SELECT COUNT (T901A.c901a_code_access_id)
       INTO v_count
       FROM T901_CODE_LOOKUP t901,T901A_LOOKUP_ACCESS T901A
      WHERE t901.c901_code_grp in (select C901f_CODE_GRP from t901f_code_lkp_ipad_map where C901_LOOKUP_TYPE = 26230804 AND C901F_VOID_FL IS NULL)--26230804 FOR CODE LOOKUP SYNC
    	AND  t901.c901_code_id = t901a.c901_code_id
        AND t901.c901_void_fl    IS NULL;
        RETURN v_count;
END get_cnt_codelookup_access_sync;

/*RFS part sync*/
FUNCTION get_cnt_rfs_mapping_sync
    RETURN NUMBER
IS
    v_count NUMBER;
BEGIN
 	  SELECT COUNT(C901C_RFS_COMP_MAP_ID)
 	  INTO v_count 
 	  	FROM( SELECT C901C_RFS_COMP_MAP_ID FROM t901c_rfs_company_mapping t901c  WHERE T901C.C901C_VOID_FL IS NULL
         	  UNION
  		     SELECT 80120 C901C_RFS_COMP_MAP_ID FROM DUAL) ; --80120 - RFS id for GMNA company.
       RETURN v_count;
END get_cnt_rfs_mapping_sync;
/*
 *  Rule 
 */
FUNCTION get_cnt_all_rulegrps_tosync
    RETURN NUMBER
IS
    v_count NUMBER;
BEGIN
     SELECT COUNT (c906_rule_seq_id)
       INTO v_count
       FROM T906_RULES t906, (
             SELECT t906.c906_rule_value codegrp
               FROM t906_rules t906
              WHERE t906.c906_rule_id IN ('SYNC_RULE_GRP')
                AND T906.C906_RULE_GRP_ID IN ('PROD_CAT_SYNC')
                AND T906.C906_VOID_FL    IS NULL
        )
        GRP
      WHERE t906.C906_RULE_GRP_ID = grp.codegrp
        AND t906.c906_void_fl    IS NULL;
    RETURN v_count;
END get_cnt_all_rulegrps_tosync;

FUNCTION get_cnt_all_parts_attr_tosync
    RETURN NUMBER
IS
    v_count NUMBER;

v_company_id   	t1900_company.c1900_company_id%TYPE; 
	BEGIN
	SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL')) INTO v_company_id FROM dual;
     SELECT COUNT(t205d.c2051_part_attribute_id)
       INTO v_count
       FROM  t205d_part_attribute T205D,t205_part_number T205, t2023_part_company_mapping t2023
      , (
             SELECT c205_part_number_id pnum
               FROM t207_set_master t207, t208_set_details t208
              WHERE t207.c901_set_grp_type = '1600' --sales Report Grp
                AND t208.c207_set_id       = t207.c207_set_id
                AND t207.c207_void_fl     IS NULL
             
        ) --Product Catalog
        SYSDATA
        ,(
				        SELECT DISTINCT t205d.c205_part_number_id
				           FROM t205d_part_attribute T205D
				          WHERE T205D.c205d_void_fl      IS NULL
				            AND T205D.C901_ATTRIBUTE_TYPE = '80180' -- Release for sales
                          --    AND T205D.c205d_attribute_value = '80120'
				    )
				    t205d_sub
      WHERE T205.c205_active_fl       ='Y'
        AND T205.C205_PART_NUMBER_ID    = T205D.C205_PART_NUMBER_ID
        AND T205.C205_PART_NUMBER_ID    = T205D_sub.C205_PART_NUMBER_ID
        AND T205.C205_PART_NUMBER_ID    = SYSDATA.PNUM
        AND T205.C205_RELEASE_FOR_SALE = 'Y'
        AND t205.c205_part_number_id    = t2023.c205_part_number_id
        AND T2023.C2023_VOID_FL        IS NULL
        AND T2023.C1900_COMPANY_ID      = v_company_id
        AND T205.C205_PRODUCT_FAMILY   IN (SELECT C906_RULE_VALUE
										   FROM T906_RULES
										  WHERE C906_RULE_ID     = 'PART_PROD_FAMILY'
											AND C906_RULE_GRP_ID = 'PROD_CAT_SYNC'
											AND c906_void_fl    IS NULL)
        AND T205D.C205D_VOID_FL        IS NULL
        AND T205D.C901_ATTRIBUTE_TYPE   = '80180' -- Release for sales;
        AND T205D.c205d_attribute_value IN ( SELECT TO_CHAR(c901_rfs_id) 
                                                            FROM ( SELECT c901_rfs_id
                                                                        , c1900_company_id 
                                                                     FROM t901c_rfs_company_mapping t901c 
                                                                    UNION
                                                                   SELECT 80120 c901_rfs_id 
                                                                        , 1000 c1900_company_id
                                                                     FROM dual  
                                                                 )  t901c
                                                          WHERE t901c.c1900_company_id = v_company_id );
    RETURN v_count;
END get_cnt_all_parts_attr_tosync;


-- PMT#40762 Part sync in iPad with Interantional Changes
FUNCTION get_count_parts_attribute
	(
		p_comp_id	   IN  t1900_company.c1900_company_id%TYPE
	)
    RETURN NUMBER
IS
    v_count NUMBER;
BEGIN	
	SELECT count(distinct(t205s.c205_part_number_id))
			INTO v_count
  		FROM t205s_part_attribute_sync t205sa, t205s_part_master_sync t205s
  		WHERE t205s.c205_part_number_id			= t205sa.c205_part_number_id
  			AND t205s.c1900_company_id     		=  p_comp_id
  			AND t205sa.c1900_company_id     	=  p_comp_id
  			AND t205s.c205s_void_fl  IS NULL
 			AND t205sa.c205s_void_fl IS NULL
 			AND JSON_EXISTS (T205S.C205S_PART_MASTER_SYNC_DATA, '$.paf?(@ == "Y")')
 			and JSON_EXISTS (t205sa.C205S_PART_ATTRIBUTE_SYNC_DATA, '$.pavf?(@ != "Y")');
     RETURN v_count;
END get_count_parts_attribute;

/*
 * Get the count of Part Number that is released for atleast one company 
 */
FUNCTION get_cnt_all_parts_tosync
    RETURN NUMBER
IS
    v_count NUMBER;

v_company_id   	t1900_company.c1900_company_id%TYPE; 
	BEGIN
	SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL')) INTO v_company_id FROM dual;
     SELECT COUNT (t205.c205_part_number_id)
       INTO v_count
       FROM t205_part_number T205, t2023_part_company_mapping t2023, 
        (
             SELECT c205_part_number_id pnum
               FROM t207_set_master t207, t208_set_details t208
              WHERE t207.c901_set_grp_type = '1600' --sales Report Grp
                AND t208.c207_set_id       = t207.c207_set_id
                AND t207.c207_void_fl     IS NULL
                AND T207.C207_SET_ID       = NVL (NULL, T207.C207_SET_ID)
        ) --Product Catalog
        sysdata
        -- PMT-41842 part sync issue 
      WHERE T205.c205_active_fl      = 'Y' 
		and t205.c205_release_for_sale = 'Y'
        AND t205.c205_part_number_id  = sysdata.pnum
        AND t205.c205_part_number_id  = t2023.c205_part_number_id
        AND t2023.c2023_void_fl      IS NULL
        AND T2023.C1900_COMPANY_ID    = v_company_id
        AND T205.c205_product_family IN (SELECT C906_RULE_VALUE
										   FROM T906_RULES
										  WHERE C906_RULE_ID     = 'PART_PROD_FAMILY'
											AND C906_RULE_GRP_ID = 'PROD_CAT_SYNC'
											AND c906_void_fl    IS NULL);
    RETURN v_count;
END get_cnt_all_parts_tosync;

-- PMT#40762 Part sync in iPad with Interantional Changes
FUNCTION get_count_parts
	(
		p_comp_id	   IN  t1900_company.c1900_company_id%TYPE
	)
    RETURN NUMBER
IS
    v_count NUMBER;
BEGIN	
	SELECT count(distinct(t205s.c205_part_number_id))
			INTO v_count
  		FROM T205S_PART_MASTER_SYNC t205s
 		WHERE t205s.c1900_company_id     	= p_comp_id
 			AND t205s.C205S_VOID_FL  IS NULL
 			AND JSON_EXISTS (C205S_PART_MASTER_SYNC_DATA, '$.paf?(@ == "Y")'); 			
     RETURN v_count;
END get_count_parts;


FUNCTION get_cnt_all_group_tosync
    RETURN NUMBER
IS
    v_count NUMBER;
BEGIN
     SELECT COUNT (t4010.c4010_group_id)
       INTO v_count
       FROM t4010_group t4010, (
            SELECT DISTINCT (t207.c207_set_id) setid
               FROM t207_set_master t207
              WHERE t207.c901_set_grp_type = '1600' --sales report grp
                AND t207.c207_void_fl     IS NULL
        )
        SYSDATA
        WHERE t4010.c207_set_id        = SYSDATA.setid
        AND t4010.c901_type          = 40045 --Forecast/Demand Group
        AND t4010.c4010_void_fl     IS NULL
        AND T4010.C4010_PUBLISH_FL  IS NOT NULL
        AND t4010.c4010_inactive_fl IS NULL;
        RETURN v_count;
END get_cnt_all_group_tosync;

-- PMT#37595 Group sync in iPad with Interantional Changes
FUNCTION get_count_group
	(
		p_comp_id	   IN  t1900_company.c1900_company_id%TYPE
	)
    RETURN NUMBER
IS
    v_count NUMBER;
BEGIN
  	SELECT count(distinct(C4010_GROUP_ID)) INTO v_count
  		FROM T4010S_GROUP_MASTER_SYNC
  		WHERE C1900_COMPANY_ID  = p_comp_id
  		AND JSON_EXISTS (C4010S_GROUP_MASTER_SYNC_DATA, '$.gpf?(@ == "Y")')
  		and JSON_EXISTS (C4010S_GROUP_MASTER_SYNC_DATA, '$.vfl?(@ != "Y")')
		AND C4010S_VOID_FL IS NULL;  		   
  	RETURN v_count; 	
END get_count_group;


FUNCTION get_cnt_all_group_dtl_tosync
    RETURN NUMBER
IS
    v_count NUMBER;
BEGIN
     SELECT COUNT (t4011.C4011_GROUP_DETAIL_ID)
       INTO v_count
       FROM t4010_group t4010, T4011_Group_Detail T4011, (
            SELECT DISTINCT (t207.c207_set_id) setid
               FROM t207_set_master t207
              WHERE t207.c901_set_grp_type = '1600' --sales report grp
                AND t207.c207_void_fl     IS NULL
        )
        SYSDATA
      WHERE t4010.c207_set_id            = SYSDATA.setid
        AND t4010.c901_type              = 40045 --Forecast/Demand Group
        AND t4010.c4010_void_fl         IS NULL
        AND T4010.C4010_PUBLISH_FL      IS NOT NULL
        AND T4010.C4010_GROUP_ID         = T4011.C4010_GROUP_ID
        AND t4011.c901_part_pricing_type = 52080 --Primary
        AND t4010.c4010_inactive_fl     IS NULL;
  	RETURN v_count;
END get_cnt_all_group_dtl_tosync;

FUNCTION get_count_group_details
	(
		p_comp_id	   IN  t1900_company.c1900_company_id%TYPE
	)
    RETURN NUMBER
IS
    v_count NUMBER;
BEGIN
	
	SELECT COUNT(distinct(T4011S.C4010_GROUP_ID)) INTO V_COUNT
  		FROM T4010S_GROUP_MASTER_SYNC T4010S, T4011S_GROUP_DETAILS_SYNC T4011S
  		WHERE T4010S.C4010_GROUP_ID = T4011S.C4010_GROUP_ID
  		AND T4010S.C1900_COMPANY_ID  = p_comp_id
  		AND T4010S.C4010S_VOID_FL IS NULL
  		AND T4011S.C4011S_VOID_FL IS NULL
  		AND JSON_EXISTS (C4010S_GROUP_MASTER_SYNC_DATA, '$.gpf?(@ == "Y")')
  		AND JSON_EXISTS (C4010S_GROUP_MASTER_SYNC_DATA, '$.vfl?(@ != "Y")');	 		   
  	RETURN v_count; 	
END get_count_group_details;

FUNCTION get_cnt_all_setmaster_tosync
    RETURN NUMBER
IS
    v_count NUMBER;
 
v_company_id   	t1900_company.c1900_company_id%TYPE; 
v_cmp_exc_fl    t906_rules.c906_rule_value%TYPE;
v_set_sync_val  t906_rules.c906_rule_value%TYPE;
	BEGIN
	SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL')) INTO v_company_id FROM dual;
		
--Fetch company exclude flag
	SELECT NVL(get_rule_value_by_company(v_company_id,'EXC_SET_IPAD_FL',v_company_id),'N')INTO v_cmp_exc_fl FROM DUAL;
	
--Fetch exclude flag value
	SELECT get_rule_value(v_cmp_exc_fl,'IPAD_SET_SYNC')INTO v_set_sync_val FROM DUAL;
	
	 SELECT COUNT (t207.C207_SET_ID)
	   INTO v_count
	   FROM t207_set_master t207, T2080_SET_COMPANY_MAPPING T2080
	  WHERE T207.c901_set_grp_type  = 1601
	    AND T207.c901_status_id     = '20367' --Approved status
	    AND T207.C207_SET_ID        = T2080.C207_SET_ID
	    AND T2080.C2080_VOID_FL    IS NULL
	    AND T2080.C1900_COMPANY_ID IN (v_company_id)
	   --Removing the below for Japan IPAD chnages to get the JPN Sets 
	   -- AND T207.C207_EXCLUDE_IPAD_FL IS NULL
	   --added the below code to get the JPN sets for Japan if the rule value is Y, otherwise get the C207_EXCLUDE_IPAD_FL = NULL by below condition
 		AND DECODE(v_cmp_exc_fl,'Y', v_set_sync_val ,NVL(T207.C207_EXCLUDE_IPAD_FL,v_set_sync_val)) = v_set_sync_val
	    AND t207.c207_void_fl      IS NULL;
    RETURN v_count;
END get_cnt_all_setmaster_tosync;

-- PMT# Set sync in iPad with Interantional Changes
FUNCTION get_count_setmaster
	(
		p_comp_id	   IN  t1900_company.c1900_company_id%TYPE
	)
    RETURN NUMBER
IS
    v_count NUMBER;
	BEGIN		
 SELECT COUNT(distinct(C207_SET_ID))
	INTO v_count
	FROM T207S_SET_MASTER_SYNC
	WHERE C1900_COMPANY_ID  = p_comp_id
	AND C901_SET_GROUP_TYPE = 1601 -- 1601 - Set Report Group
 	AND JSON_EXISTS (C207S_SET_MASTER_SYNC_DATA, '$.sts?(@ == "20367")')
	AND JSON_EXISTS (C207S_SET_MASTER_SYNC_DATA, '$.vfl?(@ != "Y")')
	AND C207S_VOID_FL IS NULL;		
	RETURN v_count;
END get_count_setmaster;	

FUNCTION get_cnt_all_set_dtl_tosync
    RETURN NUMBER
IS
    v_count NUMBER;
 
    v_company_id   	t1900_company.c1900_company_id%TYPE; 
	v_cmp_exc_fl    t906_rules.c906_rule_value%TYPE;
	v_set_sync_val  t906_rules.c906_rule_value%TYPE;
	
	BEGIN
	SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL')) INTO v_company_id FROM dual;
	
	--Fetch company exclude flag
	SELECT NVL(get_rule_value_by_company(v_company_id,'EXC_SET_IPAD_FL',v_company_id),'N')INTO v_cmp_exc_fl FROM DUAL;
	
    --Fetch exclude flag value
	SELECT get_rule_value(v_cmp_exc_fl,'IPAD_SET_SYNC')INTO v_set_sync_val FROM DUAL;
	
	 SELECT COUNT (t208.c205_part_number_id)
	   INTO v_count
	   FROM t207_set_master t207, t208_set_details t208, T2080_SET_COMPANY_MAPPING T2080
	  WHERE T207.c207_set_id        = t208.c207_set_id
	    AND T207.c901_set_grp_type  = 1601
	    AND T207.c901_status_id     = '20367' --Approved status
	    AND T207.C207_SET_ID        = T2080.C207_SET_ID
	    AND T2080.C2080_VOID_FL    IS NULL
	    AND T2080.C1900_COMPANY_ID IN (v_company_id)
	    --Removing the below for Japan IPAD chnages to get the JPN Sets
	    --AND T207.C207_EXCLUDE_IPAD_FL IS NULL
	    --added the below code to get the JPN sets for Japan if the rule value is Y, otherwise get the C207_EXCLUDE_IPAD_FL = NULL by below condition
        AND DECODE(v_cmp_exc_fl,'Y', v_set_sync_val ,NVL(T207.C207_EXCLUDE_IPAD_FL,v_set_sync_val)) = v_set_sync_val
	    AND t207.c207_void_fl      IS NULL
	    AND t208.C208_VOID_FL IS NULL;
    RETURN v_count;
END get_cnt_all_set_dtl_tosync;

-- PMT# Set sync in iPad with Interantional Changes
FUNCTION get_count_set_details
	(
		p_comp_id	   IN  t1900_company.c1900_company_id%TYPE
	)
    RETURN NUMBER
IS
    v_count NUMBER;
 
    BEGIN
SELECT COUNT(distinct(T207SD.C207_SET_ID))
		INTO V_COUNT
	FROM T207S_SET_MASTER_SYNC T207S, T207S_SET_DETAILS_SYNC T207SD
	WHERE T207S.C207_SET_ID = T207SD.C207_SET_ID
  		AND T207S.C1900_COMPANY_ID  = p_comp_id
		AND T207S.C901_SET_GROUP_TYPE = 1601 -- 1601 - Set Report Group
		AND T207S.C207S_VOID_FL  is NULL
    	AND T207SD.c207s_void_fl IS NULL
 		AND JSON_EXISTS (T207S.C207S_SET_MASTER_SYNC_DATA, '$.sts?(@ == "20367")')
		AND JSON_EXISTS (T207SD.C207S_SET_DETAILS_SYNC_DATA, '$.vfl?(@ != "Y")'); 	    	
   RETURN v_count;
END get_count_set_details; 


FUNCTION get_cnt_all_set_attr_tosync
    RETURN NUMBER
IS
    v_count NUMBER;

v_company_id   	t1900_company.c1900_company_id%TYPE; 
v_cmp_exc_fl    t906_rules.c906_rule_value%TYPE;
v_set_sync_val  t906_rules.c906_rule_value%TYPE;

	BEGIN
	SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL')) INTO v_company_id FROM dual;
	
	--Fetch company exclude flag
	SELECT NVL(get_rule_value_by_company(v_company_id,'EXC_SET_IPAD_FL',v_company_id),'N')INTO v_cmp_exc_fl FROM DUAL;
	
	--Fetch exclude flag value
	SELECT get_rule_value(v_cmp_exc_fl,'IPAD_SET_SYNC')INTO v_set_sync_val FROM DUAL;
	
	 SELECT COUNT (t207c.c207_set_id)
	   INTO v_count
	    FROM T207C_SET_ATTRIBUTE T207C, (
	    (
	        SELECT DISTINCT (T207.C207_SET_ID) SETID
	           FROM t207_set_master t207, t207c_set_attribute t207c, T2080_SET_COMPANY_MAPPING T2080
	          WHERE T207.c901_set_grp_type      = 1601
	            AND T207.c901_status_id         = '20367' --Approved status
	            AND t207.c207_void_fl          IS NULL
	            AND T207.c207_set_id            = t207c.c207_set_id
	            AND t207c.c207c_void_fl        IS NULL
	            AND T207.C207_SET_ID       = T2080.C207_SET_ID
	           --Removing the below for Japan IPAD chnages to get the JPN Sets 
	            --AND T207.C207_EXCLUDE_IPAD_FL IS NULL
	            --added the below code to get the JPN sets for Japan if the rule value is Y, otherwise get the C207_EXCLUDE_IPAD_FL = NULL by below condition
 				AND DECODE(v_cmp_exc_fl,'Y', v_set_sync_val ,NVL(T207.C207_EXCLUDE_IPAD_FL,v_set_sync_val)) = v_set_sync_val
                AND T2080.C2080_VOID_FL   IS NULL
                AND T2080.C1900_COMPANY_ID IN (v_company_id)
	            AND T207C.C901_ATTRIBUTE_TYPE   = '104740'--Loaner Pool
	            AND T207C.C207C_ATTRIBUTE_VALUE = 'Y'
	    )
	    ) T207
	  WHERE T207.SETID = T207C.C207_SET_ID
	    AND t207c.c207c_void_fl IS NULL;
    RETURN v_count;
END get_cnt_all_set_attr_tosync;

-- PMT# Set sync in iPad with Interantional Changes
FUNCTION get_count_set_attribute
	(
		p_comp_id	   IN  t1900_company.c1900_company_id%TYPE
	)
    RETURN NUMBER
IS
	 v_count NUMBER;
	 
	BEGIN
		
SELECT COUNT(distinct(T207SA.C207_SET_ID))
		INTO V_COUNT
	FROM T207S_SET_MASTER_SYNC T207S, T207S_SET_ATTRIBUTE_SYNC T207SA
	WHERE T207S.C207_SET_ID = T207SA.C207_SET_ID
  		AND T207S.C1900_COMPANY_ID  = p_comp_id
		AND T207S.C901_SET_GROUP_TYPE = 1601 -- 1601 - Set Report Group
		AND T207S.C207S_VOID_FL  is null
    	AND T207SA.c207s_void_fl IS NULL
 		AND JSON_EXISTS (T207S.C207S_SET_MASTER_SYNC_DATA, '$.sts?(@ == "20367")')
		AND JSON_EXISTS (T207SA.C207S_SET_ATTRIBUTE_SYNC_DATA, '$.vfl?(@ != "Y")');		
	RETURN v_count;
END get_count_set_attribute;

FUNCTION get_cnt_all_system_tosync
    RETURN NUMBER
IS
    v_count NUMBER;
BEGIN
	 SELECT COUNT(DISTINCT (t207.c207_set_id))
	   INTO v_count
	   FROM t207_set_master T207, t207c_set_attribute t207c
	  WHERE T207.c207_set_id       = t207c.c207_set_id
	    AND T207.c901_set_grp_type = 1600 --Sales Report Grp
	    AND T207.c207_void_fl     IS NULL
	    AND t207c.c207c_void_fl   IS NULL
	    AND c901_attribute_type    = '103119' --Publish to
	    AND c207c_attribute_value  = '103085'; --prod catalog
    RETURN v_count;
END get_cnt_all_system_tosync;

-- PMT# System sync in iPad with Interantional Changes 
FUNCTION get_count_systems
	(
		p_comp_id	   IN  t1900_company.c1900_company_id%TYPE
	)
    RETURN NUMBER
IS
    v_count NUMBER;
BEGIN
     	  
  SELECT COUNT(distinct(C207_SET_ID))
	INTO v_count
	FROM T207S_SET_MASTER_SYNC
	WHERE C1900_COMPANY_ID  = p_comp_id
	AND C901_SET_GROUP_TYPE = 1600 -- 1600- System Report Group
	AND JSON_EXISTS (C207S_SET_MASTER_SYNC_DATA, '$.vfl?(@ != "Y")')
	AND C207S_VOID_FL IS NULL;	
RETURN v_count;
END get_count_systems;

FUNCTION get_cnt_all_system_attr_tosync
    RETURN NUMBER
IS
    v_count NUMBER;
BEGIN
	 SELECT COUNT (t207c.c207_set_id)
	   INTO v_count
	   FROM T207C_SET_ATTRIBUTE T207C, (
	         SELECT DISTINCT T207.c207_set_id
	           FROM t207_set_master T207, t207c_set_attribute t207c
	          WHERE T207.c207_set_id       = t207c.c207_set_id
	            AND T207.c901_set_grp_type = 1600 --Sales Report Grp
	            AND T207.c207_void_fl     IS NULL
	            AND t207c.c207c_void_fl   IS NULL
	            AND C901_ATTRIBUTE_TYPE    = '103119' --Publish to
	            AND C207C_ATTRIBUTE_VALUE  = '103085'
	    )
	    t207
	  WHERE T207.c207_set_id = t207c.c207_set_id
	  AND t207c.c207c_void_fl   IS NULL;	  
    RETURN v_count;
END get_cnt_all_system_attr_tosync;

-- PMT# System sync in iPad with Interantional Changes 
FUNCTION get_count_system_attribute
	(
		p_comp_id	   IN  t1900_company.c1900_company_id%TYPE
	)
    RETURN NUMBER
IS
	v_count NUMBER;
BEGIN

SELECT COUNT(distinct(T207SA.C207_SET_ID))
	INTO V_COUNT
	     FROM T207S_SET_ATTRIBUTE_SYNC T207SA, T207S_SET_MASTER_SYNC T207S
	  WHERE T207S.C207_SET_ID = T207SA.C207_SET_ID
  		AND T207S.C1900_COMPANY_ID  = p_comp_id
		AND T207S.C901_SET_GROUP_TYPE = 1600 -- 1600 - System Report Group
    	AND JSON_EXISTS (T207S.C207S_SET_MASTER_SYNC_DATA, '$.vfl?(@ != "Y")')
		AND T207S.C207S_VOID_FL  is null
    	AND T207SA.c207s_void_fl IS NULL;
  	    	
  RETURN v_count;
END get_count_system_attribute;

FUNCTION get_cnt_all_uploaded_fileinfo
    RETURN NUMBER
IS
    v_count NUMBER;
BEGIN
	 SELECT COUNT (t903.c903_upload_file_list)
	   INTO v_count
	   FROM t903_upload_file_list t903
	  WHERE (T903.C901_REF_GRP in (103112,107994,107993) OR   t903.c901_ref_type IN
	    (
	         SELECT c906_rule_value
	           FROM t906_rules
	          WHERE c906_rule_id     = 'FILE_TYPE'
	            AND C906_RULE_GRP_ID = 'PROD_CAT_SYNC'
	            AND c906_void_fl    IS NULL
	    ) )
	   AND T903.C901_REF_GRP IS NOT NULL
       AND t903.c903_delete_fl IS NULL;
    RETURN v_count;
END get_cnt_all_uploaded_fileinfo;


FUNCTION get_count_files_info
    RETURN NUMBER
IS
    v_count NUMBER;
BEGIN
	SELECT COUNT(distinct(C903_REF_ID)) INTO v_count
	  FROM t903s_file_master_sync t903s
	WHERE t903s.c903s_void_fl IS NULL
	  AND JSON_EXISTS (t903s.C903S_FILE_MASTER_SYNC_DATA, '$.vfl?(@ != "Y")');

    RETURN v_count;
END get_count_files_info;


FUNCTION get_cnt_all_keywords_tosync
    RETURN NUMBER
IS
    v_count NUMBER;
BEGIN
	 SELECT COUNT (REFID)
	   INTO v_count
	  FROM (
                  SELECT t2050.c2050_keyword_ref_id REFID
                      FROM t2050_keyword_ref T2050, 
                       (
                           SELECT DISTINCT (T207.c207_set_id) systemid
                              FROM t207_set_master T207, t207c_set_attribute t207c
                             WHERE T207.c207_set_id       = t207c.c207_set_id
                               AND T207.c901_set_grp_type = '1600' --System Report Grp
                               AND T207.c207_void_fl     IS NULL
                               -- ref_id will come from portal(Keyword Detail screen) that time no need to check System publish to PC
							   AND c901_attribute_type    = '103119' --Publish to
                               AND c207c_attribute_value  = '103085'
                               AND t207c.c207c_void_fl   IS NULL --Product Catalog
                       )  sysdata                      
                      WHERE t2050.c2050_void_fl IS NULL
                      AND SYSDATA.SYSTEMID = T2050.C2050_REF_ID
                      AND t2050.c901_ref_type = '103108' --System  
                      AND t2050.c2050_ref_id =  t2050.c2050_ref_id 
                     UNION ALL
                      SELECT T2050.C2050_KEYWORD_REF_ID REFID
                      FROM t2050_keyword_ref T2050, t903_upload_file_list t903, 
                       (
                           SELECT DISTINCT (T207.c207_set_id) systemid
                              FROM t207_set_master T207, t207c_set_attribute t207c
                             WHERE T207.c207_set_id       = t207c.c207_set_id
                               AND T207.c901_set_grp_type = '1600' --System Report Grp
                               AND T207.c207_void_fl     IS NULL
                               -- ref_id will come from portal(Keyword Detail screen) that time no need to check System publish to PC
                               AND c901_attribute_type    = '103119' --Publish to
                               AND c207c_attribute_value  = '103085'
                               AND t207c.c207c_void_fl   IS NULL --Product Catalog
                       )  SYSDATA                      
                      WHERE t2050.c901_ref_type = '4000410' --File
                      AND t2050.c2050_ref_id =  t2050.c2050_ref_id
                      AND t2050.c2050_ref_id = t903.c903_upload_file_list
                      AND t903.c903_ref_id = sysdata.systemid
                      AND t2050.c2050_void_fl IS NULL
                      AND T903.C903_DELETE_FL IS NULL	  
	  );
    RETURN v_count;
END get_cnt_all_keywords_tosync;
FUNCTION get_cnt_accts_detail_tosync 
	RETURN NUMBER
IS
    v_count NUMBER;
    
v_company_id   	t1900_company.c1900_company_id%TYPE; 
	BEGIN
	SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL')) INTO v_company_id FROM dual;
			SELECT COUNT(t704.c704_account_id) 
			INTO v_count
			FROM   t704_account t704 
			WHERE  t704.c704_void_fl IS NULL
            AND t704.c901_ext_country_id IS NULL  
			AND t704.c1900_company_id = v_company_id 
            AND t704.c704_active_fl = 'Y' ;
    RETURN v_count;
END get_cnt_accts_detail_tosync;
/**********************************************************************************************************
  * This method will fetch the Active Sales Rep ID and with the Sales Rep id ,Sales Rep Details are fetched.
***********************************************************************************************************/
FUNCTION get_cnt_salesrep_detail_tosync
    RETURN NUMBER
IS
    v_count NUMBER;

v_company_id   	t1900_company.c1900_company_id%TYPE; 
	BEGIN
	SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL')) INTO v_company_id FROM dual;
			SELECT COUNT(t703.c703_sales_rep_id)
			INTO v_count
			FROM   t703_sales_rep t703
			WHERE  t703.c703_active_fl = 'Y'
            AND    t703.c1900_company_id = v_company_id 
			AND    t703.c703_void_fl    IS NULL; 
		RETURN v_count;
END get_cnt_salesrep_detail_tosync;
/**********************************************************************************************************
  * This Procedure will fetch the Active Associate Sales Rep ID's
***********************************************************************************************************/
FUNCTION get_cnt_assocrep_detail_tosync
    RETURN NUMBER
IS
    v_count NUMBER;
    
v_company_id   	t1900_company.c1900_company_id%TYPE; 
	BEGIN
	SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL')) INTO v_company_id FROM dual;
			SELECT COUNT(t704a.C704A_ACCOUNT_REP_MAP_ID)
			  INTO v_count
			  FROM t704a_account_rep_mapping t704a, t703_sales_rep t703
			 WHERE t703.c703_sales_rep_id = t704a.c703_sales_rep_id
			   AND t704a.c704a_void_fl    IS NULL
			   AND t703.c703_void_fl     IS NULL
			   AND t703.c901_ext_country_id IS NULL
               AND t703.c1900_company_id = v_company_id
			   AND t704a.c704a_active_fl='Y'; 
		RETURN v_count;			   
END get_cnt_assocrep_detail_tosync;
/****************************************************************************************************************
  * This method will fetch the Active Distributor ID and with the Distributor id ,Distributor Details are fetched.
*****************************************************************************************************************/
FUNCTION get_cnt_dist_detail_tosync
    RETURN NUMBER
IS
    v_count NUMBER;
    
v_company_id   	t1900_company.c1900_company_id%TYPE; 
	BEGIN
	SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL')) INTO v_company_id FROM dual;

			SELECT COUNT(t701.c701_distributor_id)
			  INTO v_count
			FROM   t701_distributor t701
			WHERE  t701.c701_void_fl IS NULL
			AND    t701.c901_ext_country_id IS NULL
            AND    t701.c1900_company_id = v_company_id
			AND    t701.c701_active_fl  = 'Y'; 
		RETURN v_count;					
END get_cnt_dist_detail_tosync;
/************************************************************************************************************************
  * This method will fetch the Active Gpo Account Map id and with the Gpo Account Map id, Gpo Mapping  Details are fetched.
***************************************************************************************************************************/
FUNCTION get_cnt_acctgpomap_tosync
    RETURN NUMBER
IS
    v_count NUMBER;

v_company_id   	t1900_company.c1900_company_id%TYPE; 
	BEGIN
	SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL')) INTO v_company_id FROM dual;
			SELECT COUNT(t740.c740_gpo_account_map_id)
			 INTO v_count
			FROM   t740_gpo_account_mapping t740,
 				   t704_account t704
			WHERE  t740.c704_account_id = t704.c704_account_id
			AND    t704.c704_void_fl     IS NULL
			AND    t740.c740_void_fl     IS NULL
            AND    t704.c1900_company_id = v_company_id 
			AND    t704.c704_active_fl    = 'Y';
	RETURN v_count;	
END get_cnt_acctgpomap_tosync;
/***************************************************************************************************************************
  * This method will fetch the Active Address id and with the Address ID, Address  Details are fetched.
***************************************************************************************************************************/
FUNCTION get_cnt_address_detail_tosync
    RETURN NUMBER
IS
    v_count NUMBER;
	v_company_id   	t1900_company.c1900_company_id%TYPE; 
	BEGIN
	SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL')) INTO v_company_id FROM dual;

	SELECT COUNT(C106_ADDRESS_ID)
	INTO v_count
	FROM 
    (
		SELECT DISTINCT c106_address_id
	From
	  (SELECT t106.c106_address_id 
	  FROM t703_sales_rep t703 ,
	    t106_address t106
	  WHERE t703.c703_active_fl  = 'Y'
	  AND t703.c101_party_id     = t106.c101_party_id
	  AND t106.c106_void_fl     IS NULL
	  AND t703.c703_void_fl     IS NULL
	  AND T703.C1900_Company_Id  = v_company_id
	  AND T106.C106_Inactive_Fl IS NULL
	  ) 
	Union 
	  (SELECT T106.C106_Address_Id
	  FROM T101_Party T101,
	    T106_Address T106
	  WHERE T101.C901_Party_Type = '7000'
	  AND T101.C101_Void_Fl     IS NULL
	  AND (t101.c101_active_fl is null or t101.c101_active_fl = 'Y')
	  AND T106.C106_Void_Fl     IS NULL
	  AND  t106.c101_party_id = t101.c101_party_id
	  AND T106.C106_Inactive_Fl IS NULL
	  )
	  Union
	  (
	  SELECT t106.c106_address_id
		FROM t6630_activity t6630 ,
	  	t106_address t106,
	  	t6632_activity_attribute t6632
		WHERE t6632.c6630_activity_id   = t6630.c6630_activity_id
		AND t6632.c901_attribute_type   = '7777'
		AND t6632.c6632_attribute_value = t106.c106_address_id
		AND t6630.c6630_void_fl        IS NULL
		AND t6632.c6632_void_fl        IS NULL
		AND t106.c106_void_fl          IS NULL
		AND T106.C106_Inactive_Fl IS NULL
	  )
	  Union 
	  (SELECT T106.C106_Address_Id
	  FROM T101_Party T101,
	    T106_Address T106
	  WHERE T101.C901_Party_Type = '26230725' --Dealer
	  AND T101.C101_Void_Fl     IS NULL
	  AND  t101.c101_active_fl = 'Y'
	  AND T106.C106_Void_Fl     IS NULL
	  AND  t106.c101_party_id = t101.c101_party_id
	  AND t101.c1900_company_id = v_company_id
	  AND T106.C106_Inactive_Fl IS NULL
	  )

	  );
	RETURN v_count;	
END get_cnt_address_detail_tosync;
/***************************************************************************************************************************
  * This method will fetch the Active  User id and with the User id, User  Details are fetched.
***************************************************************************************************************************/
FUNCTION get_cnt_user_tosync
    RETURN NUMBER
IS
    v_count NUMBER;
	v_company_id   	t1900_company.c1900_company_id%TYPE; 
	BEGIN
	SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL')) INTO v_company_id FROM dual;
			 SELECT COUNT(u101.c101_user_id)
			   INTO v_count
			   FROM t101_user u101, t101_party p101
			  WHERE u101.c901_dept_id IN
				      (SELECT c906_rule_value
				      FROM t906_rules
				      WHERE c906_rule_id   = 'USER_DEPT'
				      AND c906_rule_grp_id ='PROD_CAT_SYNC'
				      AND c906_void_fl is null) -- '2005' --Sales
			    AND u101.c901_user_status IN (SELECT c906_rule_value
				      FROM t906_rules
				      WHERE c906_rule_id   = 'USER_STATUS'
				      AND c906_rule_grp_id ='PROD_CAT_SYNC'
				      AND c906_void_fl is null) --311 -Active
				      AND u101.C101_PARTY_ID  = p101.C101_PARTY_ID 
                      AND p101.C1900_COMPANY_ID  = v_company_id;
RETURN v_count;
END get_cnt_user_tosync;

FUNCTION get_cnt_all_parites_tosync
    RETURN NUMBER
IS
    v_count NUMBER;
    v_company_id   	t1900_company.c1900_company_id%TYPE; 
	BEGIN
		SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL')) INTO v_company_id FROM dual;
		
			  SELECT COUNT(t101.c101_party_id)
			    INTO v_count
			    FROM t101_party t101
			   WHERE t101.c901_party_type in (SELECT c906_rule_value FROM t906_rules 
 			 								  WHERE c906_rule_id ='PARTY_TYPE' 
 			 							  	   AND c906_rule_grp_id = 'PROD_CAT_SYNC' 
 			 							  	    AND c906_void_fl is null)
 			    AND (C101_ACTIVE_FL IS NULL OR C101_ACTIVE_FL = 'Y')
			    AND t101.c101_void_fl IS NULL
			    AND t101.c1900_company_id  = v_company_id;
	RETURN v_count;			  
END get_cnt_all_parites_tosync;
FUNCTION get_cnt_party_contact_tosync
    RETURN NUMBER
IS
    v_count NUMBER;
    v_company_id   	t1900_company.c1900_company_id%TYPE; 
	BEGIN
		SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL')) INTO v_company_id FROM dual;
        	SELECT COUNT(t107.c107_contact_id)
        	INTO v_count
			FROM t101_party t101,t107_contact t107
			WHERE t101.c101_party_id =t107.c101_party_id
			AND t107.c901_mode      IN (SELECT c906_rule_value
  										FROM t906_rules
  										WHERE c906_rule_id   ='CONTACT_MODE'
  										AND c906_rule_grp_id = 'PROD_CAT_SYNC'
  										AND C906_VOID_FL  IS NULL)
			
  			AND t101.c901_party_type IN (SELECT c906_rule_value
  										 FROM t906_rules
  										 WHERE c906_rule_id   ='PARTY_TYPE'
  										 AND C906_RULE_GRP_ID = 'PROD_CAT_SYNC'
  										 AND C906_VOID_FL    IS NULL )
  			AND (T101.C101_ACTIVE_FL IS NULL OR T101.C101_ACTIVE_FL = 'Y')
			AND T101.C101_VOID_FL   IS NULL
			AND T101.C101_DELETE_FL IS NULL
			AND C107_INACTIVE_FL IS NULL
			AND t107.c107_void_fl   IS NULL
			AND t101.c1900_company_id  = v_company_id;
	RETURN v_count;				
END get_cnt_party_contact_tosync;
FUNCTION get_cnt_case_sync
(
		  p_user_id	   IN  t101a_user_token.c101_user_id%TYPE
	)
	    RETURN NUMBER
IS
    v_count NUMBER;
BEGIN
		

        	SELECT COUNT(refid)
        	INTO v_count
        	FROM (
			 SELECT t7100.c7100_case_info_id refid
			   FROM t7100_case_information t7100, t7102_case_attribute t7102
			  WHERE t7100.c7100_case_info_id = t7102.c7100_case_info_id
			    AND t7100.c901_type = 1006503 --Case
				AND t7100.c901_case_status IN (SELECT c906_rule_value
													 FROM t906_rules
												    WHERE c906_rule_id   = 'CASE_STATUS'
													  AND c906_rule_grp_id ='PROD_CAT_SYNC'
													  AND c906_void_fl IS NULL)
			    AND t7102.c901_attribute_type IN (103646,103647) --103646 - Staff Covering, 103647 - Watcher
			    AND t7102.c7102_attribute_value = TO_CHAR(p_user_id)
			    AND t7100.c7100_void_fl IS NULL
			    AND t7102.c7102_void_fl IS NULL
			  UNION
             SELECT t7100.c7100_case_info_id refid
			   FROM T7100_CASE_INFORMATION T7100
			  WHERE T7100.C7100_CREATED_BY = p_user_id
			    AND t7100.c7100_void_fl IS NULL
			    AND t7100.c901_type = 1006503 --Case
				AND t7100.c901_case_status IN (SELECT c906_rule_value
													 FROM t906_rules
												    WHERE c906_rule_id   = 'CASE_STATUS'
													  AND c906_rule_grp_id ='PROD_CAT_SYNC'
													  AND c906_void_fl IS NULL)
			    );
	RETURN v_count;
END get_cnt_case_sync;
FUNCTION get_cnt_case_attr_sync
(
		  p_user_id	   IN  t101a_user_token.c101_user_id%TYPE
	)
	    RETURN NUMBER
IS
    v_count NUMBER;
BEGIN
		

        	SELECT COUNT(T7102.C7102_CASE_ATTRIBUTE_ID)
        	INTO v_count
        	FROM (
			 SELECT t7100.c7100_case_info_id refid
			   FROM t7100_case_information t7100, t7102_case_attribute t7102
			  WHERE t7100.c7100_case_info_id = t7102.c7100_case_info_id
			    AND t7100.c901_type = 1006503 --Case
				AND t7100.c901_case_status IN (SELECT c906_rule_value
													 FROM t906_rules
												    WHERE c906_rule_id   = 'CASE_STATUS'
													  AND c906_rule_grp_id ='PROD_CAT_SYNC'
													  AND c906_void_fl IS NULL)
			    AND t7102.c901_attribute_type IN (103646,103647) --103646 - Staff Covering, 103647 - Watcher
			    AND t7102.c7102_attribute_value = TO_CHAR(p_user_id)
			    AND t7102.c7102_void_fl IS NULL
			  UNION
             SELECT t7100.c7100_case_info_id refid
			   FROM T7100_CASE_INFORMATION T7100
			  WHERE T7100.C7100_CREATED_BY = p_user_id
			    AND t7100.c901_type = 1006503 --Case
				AND t7100.c901_case_status IN (SELECT c906_rule_value
													 FROM t906_rules
												    WHERE c906_rule_id   = 'CASE_STATUS'
													  AND c906_rule_grp_id ='PROD_CAT_SYNC'
													  AND c906_void_fl IS NULL)
			    ) t7100, T7102_CASE_ATTRIBUTE T7102
                WHERE t7102.c7100_case_info_id = t7100.refid
                  AND t7102.c7102_void_fl IS NULL;
	RETURN v_count;
END get_cnt_case_attr_sync;

/*
 *   Activity
 */
FUNCTION get_cnt_act_sync
    RETURN NUMBER
IS
    v_count NUMBER;
BEGIN
     SELECT COUNT (C6630_ACTIVITY_ID)
       INTO v_count
       FROM T6630_ACTIVITY
	   WHERE C6630_VOID_FL IS NULL;
       RETURN v_count;
END get_cnt_act_sync;
/*
 *   Activity Party
 */
FUNCTION get_cnt_actparty_sync
    RETURN NUMBER
IS
    v_count NUMBER;
BEGIN
     SELECT COUNT (c6631_activity_party_id)
       INTO v_count
       FROM t6631_activity_party_assoc
	   WHERE C6631_VOID_FL IS NULL;
	   RETURN v_count;
END get_cnt_actparty_sync;

/*
 *   Activity Attribute
 */
FUNCTION get_cnt_actattr_sync
    RETURN NUMBER
IS
    v_count NUMBER;
BEGIN
     SELECT COUNT (c6632_activity_attribute_id)
       INTO v_count
       	FROM t6632_activity_attribute 
		WHERE C6632_VOID_FL IS NULL;
	   RETURN v_count;
END get_cnt_actattr_sync;

/*
 *   Surgeon NPI 
 */
FUNCTION get_cnt_surgparty_sync
    RETURN NUMBER
IS
    v_count NUMBER;
BEGIN
     SELECT COUNT (C6600_SURGEON_NPI_ID)
       INTO v_count
       	FROM T6600_PARTY_SURGEON 
		WHERE C6600_VOID_FL IS NULL;
	   RETURN v_count;
END get_cnt_surgparty_sync;

/*
 *   Surgeon Attribute
 */
FUNCTION get_cnt_surgattr_sync
    RETURN NUMBER
IS
    v_count NUMBER;
BEGIN
     SELECT COUNT (C1012_PARTY_OTHER_INFO_ID)
       INTO v_count
       	FROM T1012_PARTY_OTHER_INFO
		WHERE C1012_VOID_FL IS NULL;
	   RETURN v_count;
END get_cnt_surgattr_sync;

/*
 *   Surgeon Hospital Affiliation
 */
FUNCTION get_cnt_surgaffl_sync
    RETURN NUMBER
IS
    v_count NUMBER;
BEGIN
     SELECT COUNT (C1014_PARTY_AFFILIATION_ID)
       INTO v_count
       	FROM T1014_PARTY_AFFILIATION 
		WHERE C1014_VOID_FL IS NULL;
	   RETURN v_count;
END get_cnt_surgaffl_sync;

/*
 *   Surgeon Surgical Activity
 */
FUNCTION get_cnt_surgsurgical_sync
    RETURN NUMBER
IS
    v_count NUMBER;
BEGIN
     SELECT COUNT (C6610_SURGICAL_ACTIVITY_ID)
       INTO v_count
       	FROM T6610_SURGICAL_ACTIVITY 
		WHERE C6610_VOID_FL IS NULL;
	   RETURN v_count;
END get_cnt_surgsurgical_sync;

/*
 *   Surgeon Documents
 */
FUNCTION get_cnt_surgdocs_sync
    RETURN NUMBER
IS
    v_count NUMBER;
BEGIN
     SELECT COUNT (C903_UPLOAD_FILE_LIST)
       INTO v_count
       	FROM T903_UPLOAD_FILE_LIST 
		WHERE C903_DELETE_FL IS NULL
		AND C901_REF_GRP in ('103112','107993','107994');
	   RETURN v_count;
END get_cnt_surgdocs_sync;

/*
 *   CRF
 */
FUNCTION get_cnt_crf_sync
    RETURN NUMBER
IS
    v_count NUMBER;
BEGIN
     SELECT COUNT (C6605_CRF_ID)
       INTO v_count
       	FROM T6605_CRF 
		WHERE C6605_VOID_FL IS NULL;
	   RETURN v_count;
END get_cnt_crf_sync;

/*
 *   Criteria
 */
FUNCTION get_cnt_criteria_sync
    RETURN NUMBER
IS
    v_count NUMBER;
BEGIN
     SELECT COUNT (C1101_CRITERIA_ID)
       INTO v_count
       	FROM T1101_CRITERIA  
       	WHERE C1101_VOID_FL IS NULL ;
	   RETURN v_count;
END get_cnt_criteria_sync;

/*
 *   Criteria Attribute
 */
FUNCTION get_cnt_criteriaattr_sync
    RETURN NUMBER
IS
    v_count NUMBER;
BEGIN
     SELECT COUNT (C1101A_CRITERIA_ATTRIBUTE_ID)
       INTO v_count
       	FROM T1101A_CRITERIA_ATTRIBUTE  
       	WHERE C1101A_VOID_FL IS NULL ;
	   RETURN v_count;
END get_cnt_criteriaattr_sync;

/*
 *   Projects
 */
FUNCTION get_cnt_project_sync
    RETURN NUMBER
IS
    v_count NUMBER;
BEGIN
     SELECT COUNT (C202_PROJECT_ID)
       INTO v_count
       	FROM T202_PROJECT WHERE C202_VOID_FL IS NULL ;
	   RETURN v_count;
END get_cnt_project_sync;

/*
 *   CRF Approver
 */
FUNCTION get_cnt_crfappr_sync
    RETURN NUMBER
IS
    v_count NUMBER;
BEGIN
     SELECT COUNT (c6605a_crf_approval_id)
       INTO v_count
       FROM T6605A_CRF_APPROVAL T6605A;
	   RETURN v_count;
END get_cnt_crfappr_sync;

/*
 *   Territory Mapping
 */
FUNCTION get_cnt_terrmap_sync
    RETURN NUMBER
IS
    v_count NUMBER;
BEGIN
     select COUNT(*)
       INTO v_count
       from (select distinct AD_ID , AD_NAME ,VP_NAME , REP_NAME , REP_ID , VP_ID , TER_ID ,
	   TER_NAME , REGION_ID  , REGION_NAME , D_ID , D_NAME , AC_ID , DISTTYPENM 
	   from v700_territory_mapping_detail);
	   RETURN v_count;
END get_cnt_terrmap_sync;


/*
 *
 * This function is used to get the security event count
 *
 */
FUNCTION get_cnt_security_event_sync
(
		  p_user_id	   IN  t101a_user_token.c101_user_id%TYPE
	)
	    RETURN NUMBER
IS
    v_count NUMBER;
BEGIN
		    SELECT COUNT(*)
		    INTO v_count
            FROM(SELECT t906.c906_rule_grp_id EVENTNAME 
						,t906.C906_RULE_ID TAGNAME 
						,t906.C906_RULE_VALUE PAGENAME					    
				  FROM T1530_access T1530, t906_rules t906, t906_rules tt906, t1501_group_mapping t1501
				 WHERE tt906.c906_rule_grp_id = 'MOBILESECURITYEVENTS'
	               AND tt906.c906_rule_id  = t906.c906_rule_grp_id 
				   AND t906.c906_rule_grp_id = t1530.c1520_function_id 
	               AND t1501.c101_mapped_party_id IN ( SELECT  C101_PARTY_ID 
		                                                FROM T101_USER 
		                                               WHERE c101_user_id = p_user_id
	                                                  )
	               AND t1501.c1500_group_id = T1530.c1500_group_id					  
			       GROUP BY  t906.c906_rule_grp_id,t906.C906_RULE_ID,t906.C906_RULE_VALUE
		       );     	

    
	RETURN v_count;
END get_cnt_security_event_sync;
/*
 *Name : get_cnt_all_tag_tosync
 *Desc: This function is used to get the Tag Sync count
 *Author: Karthik
 */
FUNCTION get_cnt_all_tag_tosync
    RETURN NUMBER
IS
    v_count NUMBER;
    v_company_id   	t1900_company.c1900_company_id%TYPE; 
	BEGIN
		SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL')) INTO v_company_id FROM dual;
		
			  SELECT COUNT(t5010.C5010_TAG_ID)
			    INTO v_count
			    FROM t5010d_device_tag_sync_master t5010
			    WHERE T5010.c1900_company_id = v_company_id
         		AND T5010.C5010D_DEVICE_TAG_SYNC_VOID_FL IS NULL;
	RETURN v_count;			  
END get_cnt_all_tag_tosync;

END gm_pkg_pdpc_prodcat_cnt;
/
