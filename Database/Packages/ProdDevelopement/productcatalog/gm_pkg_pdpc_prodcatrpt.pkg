/* Formatted on 2009/02/16 12:30 (Formatter Plus v4.8.0) */
--@"C:\Database\Packages\ProdDevelopement\productcatalog\gm_pkg_pdpc_prodcatrpt.pkg";

CREATE OR REPLACE PACKAGE gm_pkg_pdpc_prodcatrpt
IS
	/*************************************************************************************************
	 * Description :  This procedure will set the input p_sync_type id's from the device last sync time.
	**************************************************************************************************/
	PROCEDURE gm_fch_device_updates (
		p_device_infoid  IN  T9151_DEVICE_SYNC_DTL.C9150_DEVICE_INFO_ID%TYPE
	  , p_language_id    IN  T9151_DEVICE_SYNC_DTL.C901_LANGUAGE_ID%TYPE
	  , p_sync_type      IN  T9151_DEVICE_SYNC_DTL.C901_SYNC_TYPE%TYPE
	  , p_sync_count  OUT NUMBER
	);
	/*************************************************************************************************
	 * Description :  This procedure will set the input p_sync_type id's from the device last sync time.
	 * Example     :  This procedure will check is there any modified parts avaiable after last sync for
	 *                parts for given device. If updated parts available, then it will add all updated
	 *                parts into temp table and return last synced page's last record key.
	**************************************************************************************************/		
	PROCEDURE gm_fch_device_updates(
		p_device_infoid  IN  t9151_device_sync_dtl.c9150_device_info_id%TYPE
	  , p_language_id    IN  t9151_device_sync_dtl.c901_language_id%TYPE
	  , p_sync_type      IN  t9151_device_sync_dtl.c901_sync_type%TYPE
	  , p_pageno	     IN  NUMBER
	  , p_sync_count     OUT NUMBER
      , p_last_rec	     OUT  t9151_device_sync_dtl.C9151_LAST_SYNC_REC%TYPE 
	);
	
   /*******************************************************
	 * * Description : Procedure to fch all system tosync
	*******************************************************/
	PROCEDURE gm_fch_all_system_tosync;
   /*******************************************************
	 * Description : Procedure to fch all code group tosync
	*******************************************************/
	PROCEDURE gm_fch_all_codegroup_tosync;	
   /*******************************************************
 	* Description : Procedure to fch all parts tosync
	*******************************************************/
	PROCEDURE gm_fch_all_parts_tosync
	     (p_system_id  IN t207_set_master.c207_set_id%type DEFAULT NULL);
   /*******************************************************
 	* Description : Procedure to fch all group tosync
	*******************************************************/
	PROCEDURE gm_fch_all_group_tosync;	
   /*******************************************************
 	* Description : Procedure to fch all Setmaster tosync
	*******************************************************/
	PROCEDURE gm_fch_all_setmaster_tosync;	
   /**************************************************************
	 * Description : Procedure to fch all system attributes tosync
	**************************************************************/
	PROCEDURE gm_fch_all_systemattrib_tosync;
   /**************************************************************
	 * Description : Procedure to fch all uploaded files info tosync
	**************************************************************/
	PROCEDURE gm_fch_all_uploaded_fileinfo;
   /*******************************************************
	 * Description : Procedure to fch paging details
	*******************************************************/
	PROCEDURE gm_fch_paging_dtl(
		p_table_nm  IN      t906_rules.c906_rule_id%TYPE
	  , p_rule_grp  IN  	t906_rules.C906_Rule_Grp_Id%TYPE
	  , p_page_no   IN OUT  NUMBER
	  , p_startrow  OUT     NUMBER
	  , p_endrow    OUT     NUMBER
	  , p_page_size OUT     NUMBER	  
	);
	/******************************************************************
	* Description : Procedure to fetch the data sync update detail for product catalog.
	****************************************************************/	
	PROCEDURE gm_fch_all_datasync_update  (
		p_token  		IN 		t101a_user_token.c101a_token_id%TYPE,
		p_langid		IN		NUMBER,
		p_page_no		IN		NUMBER,
		p_uuid   		IN 		t9150_device_info.c9150_device_uuid%TYPE,
		p_party_id      IN	    T101_PARTY.C101_PARTY_ID%TYPE,
		p_data_cur		OUT		TYPES.CURSOR_TYPE
	);
	/**************************************************************************************
	 * Description : Procedure to fetch all Rule group related to Marketing Collateral tosync
	***************************************************************************************/
	PROCEDURE gm_fch_all_rulegrps_tosync;
	
	/*******************************************************
	 * Description : Procedure to fetch all keywords tosync
	*******************************************************/
	PROCEDURE gm_fch_all_keywords_tosync(
	 p_ref_id    IN t2050_keyword_ref.c2050_ref_id%TYPE DEFAULT NULL,
     p_ref_type IN t2050_keyword_ref.c901_ref_type%TYPE DEFAULT NULL
	);	
	/*******************************************************
	 * Description : Procedure used for web service used to 
	 * fetch all Party ID from party table of type 7000
	*******************************************************/
   PROCEDURE gm_fch_all_parites_tosync;
	/******************************************************************
   *   Description :  This procedure used for web service which is used to fetch fetch Party Contact Details 
	   for party id. The contact mode for party id should be 90452(E-mail-id) and party type should be 7000 and 
	   this method will return 1000 records at a time from procedure.
	   the records returned from procedure are of surgeon type.
   ****************************************************************/
   PROCEDURE gm_fch_all_party_contact(
		p_token  			IN 		t101a_user_token.c101a_token_id%TYPE,
		p_langid			IN		NUMBER,
		p_page_no			IN		NUMBER,
	    p_uuid		        IN      t9150_device_info.c9150_device_uuid%TYPE,
		p_party_contactlist_cur		OUT		TYPES.CURSOR_TYPE
	) ;
	
	/*******************************************************
	 * Description :  This procedure used for web service which is used to fetch contact id from contact table 
	   where The contact mode for contact id should be 90452(E-mail-id) and 
	   for the rescpective party type  for party id should be 7000 and 
	   the records returned from procedure are of surgeon type. 
	*******************************************************/
   PROCEDURE gm_fch_party_contact_tosync;
	
	/*******************************************************
	   Description :  This procedure used for web service which is used to fetch fetch Party Contact Details 
	   for party id. The contact mode for party id should be 90452(E-mail-id) and party type should be 7000 and 
	   this method will return 1000 records at a time from procedure.
	   the records returned from procedure are of surgeon type. 
	 *******************************************************/
   PROCEDURE gm_fch_all_party_contact_dtl(
		p_pageno             IN     NUMBER,
		p_last_rec		     IN 	  t9151_device_sync_dtl.C9151_LAST_SYNC_REC%TYPE,
		p_party_contactlist_cur  OUT 	  TYPES.cursor_type
    );	
     /*************************************************************************************************
	 * Description :  This function is used to get the last sync time from the device
	**************************************************************************************************/
	FUNCTION get_last_sync_date (
		p_device_infoid  IN  T9151_DEVICE_SYNC_DTL.C9150_DEVICE_INFO_ID%TYPE
	  , p_language_id    IN  T9151_DEVICE_SYNC_DTL.C901_LANGUAGE_ID%TYPE
	  , p_sync_type      IN  T9151_DEVICE_SYNC_DTL.C901_SYNC_TYPE%TYPE
	  , p_party_id       IN  t101_user.c101_party_id%TYPE DEFAULT NULL	  
	  , p_type			 IN  t2001_master_data_updates.c901_ref_type%TYPE
	) RETURN DATE;
	 /*************************************************************************************************
	 * Description :  This function is used to get the last sync time for the device without joining with t2001
	**************************************************************************************************/
	FUNCTION get_last_sync_date (
		p_device_infoid  IN  T9151_DEVICE_SYNC_DTL.C9150_DEVICE_INFO_ID%TYPE
	  , p_language_id    IN  T9151_DEVICE_SYNC_DTL.C901_LANGUAGE_ID%TYPE
	  , p_sync_type      IN  T9151_DEVICE_SYNC_DTL.C901_SYNC_TYPE%TYPE
	) RETURN DATE;
	/*******************************************************
	 * Description : Procedure to fch all set tosync
	*******************************************************/
	PROCEDURE gm_fch_all_setattrib_tosync;	
	/**************************************************************************
	 * Description : Procedure to fch  codelookup exclusion table while syncing
	***************************************************************************/
	PROCEDURE gm_fch_all_code_excl_tosync;
	
	/**************************************************************************
	 * Description : Procedure to fch  codelookup access table while syncing
	***************************************************************************/
	PROCEDURE gm_fch_all_code_access_tosync;
	
	/**************************************************************************
	 * Description : Procedure to fch  RFS company mapping details
	***************************************************************************/
	PROCEDURE gm_fch_all_rfs_parts_tosync;

	/**********************************************************************
	 * Description : Procedure to fch all security events tosync
	***********************************************************************/
	PROCEDURE gm_fch_all_secu_event_tosync; 

END gm_pkg_pdpc_prodcatrpt;
/
