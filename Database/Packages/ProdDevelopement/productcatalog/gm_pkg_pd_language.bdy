--@"C:\Database\Packages\ProdDevelopement\productcatalog\gm_pkg_pd_language.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_pd_language
IS
	/*******************************************************
	* Description : Procedure to fetch language details
	* Author      : Jignesh Shah
	*******************************************************/

	PROCEDURE gm_fch_lang_details (
		p_ref_id			IN		t2000_master_metadata.c2000_ref_id%TYPE
		, p_ref_type		IN		t2000_master_metadata.c901_ref_type%TYPE
		, p_lang_id			IN		t2000_master_metadata.c901_language_id%TYPE
		, p_out_langdetail	OUT      TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_out_langdetail
		FOR 
	  SELECT c2000_ref_name name
           , c2000_ref_desc description
           , c2000_ref_dtl_desc dtl_description
           , get_code_name(c901_language_id) langnm
           , c901_language_id langid
           , c2000_ref_id refid
           , c901_ref_type reftype
           , get_user_name(c2000_last_updated_by) updby
           , c2000_last_updated_date upddt
           , c2000_master_mdata_id metaid
        FROM t2000_master_metadata
       WHERE c2000_void_fl IS NULL
         AND c2000_ref_id = NVL(p_ref_id,c2000_ref_id)
         AND c901_ref_type = NVL(p_ref_type ,c901_ref_type)
         AND c901_language_id = NVL(p_lang_id,c901_language_id)
    ORDER BY c901_language_id;

	END gm_fch_lang_details;
	
	/************************************************************
	* Description	: Procedure to save project metadata details
	* Author		: Jignesh Shah
	*************************************************************/
	PROCEDURE gm_sav_lang_details (
	  	p_ref_id			IN		t2000_master_metadata.c2000_ref_id%TYPE
	  , p_ref_type			IN		t2000_master_metadata.c901_ref_type%TYPE
	  , p_ref_name			IN		t2000_master_metadata.c2000_ref_name%TYPE
	  , p_ref_desc			IN		t2000_master_metadata.c2000_ref_desc%TYPE
	  , p_ref_dtl_desc		IN		t2000_master_metadata.c2000_ref_dtl_desc%TYPE
	  , p_user_id			IN		t2000_master_metadata.c2000_last_updated_by%TYPE
	  , p_lang_id			IN		t2000_master_metadata.c901_language_id%TYPE DEFAULT '103097'
	)
	AS
		v_old_name t2000_master_metadata.c2000_ref_name%TYPE;
		v_old_desc t2000_master_metadata.c2000_ref_desc%TYPE;
		v_old_dtldesc t2000_master_metadata.c2000_ref_dtl_desc%TYPE;
		v_action	VARCHAR2(10);
	BEGIN
		
		BEGIN
			SELECT c2000_ref_name, c2000_ref_desc, c2000_ref_dtl_desc
			INTO v_old_name,v_old_desc,v_old_dtldesc
			FROM t2000_master_metadata 
			WHERE c2000_ref_id = p_ref_id 
			AND c901_ref_type = p_ref_type
			AND c901_language_id = p_lang_id
			AND c2000_void_fl IS NULL;
			
			EXCEPTION WHEN NO_DATA_FOUND
			THEN
				v_old_name := '-9999';
				v_old_desc := '-9999';
				v_old_dtldesc := '-9999';
			
		END;
		
		UPDATE t2000_master_metadata
		SET c2000_ref_name = p_ref_name
			, c2000_ref_desc = p_ref_desc
			, c2000_ref_dtl_desc = p_ref_dtl_desc
			, c2000_last_updated_date = SYSDATE
			, c2000_last_updated_by = p_user_id
		WHERE c2000_ref_id = p_ref_id 
		AND c901_ref_type = p_ref_type
		AND c901_language_id = p_lang_id
		AND c2000_void_fl IS NULL;
	
		IF (SQL%ROWCOUNT = 0)
		THEN
			INSERT INTO t2000_master_metadata
			(
				c2000_master_mdata_id, c2000_ref_id, c901_ref_type, c2000_ref_name
				, c2000_ref_desc, c2000_ref_dtl_desc, c901_language_id
				, c2000_last_updated_date, c2000_last_updated_by 
			)
			VALUES
			(
				S2000_MASTER_METADATA.NEXTVAL, p_ref_id, p_ref_type,p_ref_name
				, p_ref_desc, p_ref_dtl_desc, p_lang_id
				, SYSDATE, p_user_id
			);
			
			v_action := '103122';  -- New
			
		END IF;
		
		
		IF(v_action IS NULL AND 
		  (v_old_name <>p_ref_name or v_old_desc<> p_ref_desc or v_old_dtldesc<>p_ref_dtl_desc ))
		THEN
			v_action := '103123';  -- Updated
		END IF;
		
		-- For English the update will come from respective screen,hence we are ignoring it here.
		IF (p_lang_id != '103097'  AND v_action IS NOT NULL )
		THEN
		--103100 Product Catalog
			gm_pkg_pdpc_prodcattxn.gm_sav_language_upd(p_ref_id
								     , p_ref_type, v_action 
								     ,p_lang_id,'103100'
								     ,p_user_id
								     );
		END IF;
	END gm_sav_lang_details;
	
	/********************************************************************************************
	 * Description	: Procedure to Void the Language Details for a PART/SET/SYSTEM/GROUP/PROJECT
	 * Author		: HReddi	 
	*********************************************************************************************/
	PROCEDURE gm_void_lang_detail(
    	p_inputstr IN CLOB,
        p_userid   IN t903_upload_file_list.c903_last_updated_by%TYPE)
	AS
    	v_strlen    NUMBER          := NVL (LENGTH (p_inputstr), 0) ;
    	v_string    CLOB := p_inputstr;
    	v_substring CLOB ;
    	v_metaid     NUMBER := 0;
	BEGIN
    	IF v_strlen                      > 0 THEN
        	WHILE INSTR (v_string, '|') <> 0
       		LOOP
            v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
            v_string    := SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
            v_metaid    := TO_NUMBER (v_substring) ;       
            
            UPDATE t2000_master_metadata
               SET c2000_void_fl = 'Y'
                 , c2000_last_updated_by   = p_userid
                 , c2000_last_updated_date = SYSDATE
             WHERE c2000_master_mdata_id   = v_metaid;
        END LOOP;
    END IF;
	END gm_void_lang_detail;   
END gm_pkg_pd_language;
/