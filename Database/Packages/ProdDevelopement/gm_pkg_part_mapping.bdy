-- @"C:\database\Packages\ProdDevelopement\gm_pkg_part_mapping.bdy"

CREATE OR REPLACE PACKAGE BODY gm_pkg_part_mapping
IS

/*******************************************************
	  * Description : Procedure to fetch the non Asscociate project to the Mapping screen.
	  * Author	    : Mani
*******************************************************/
    PROCEDURE gm_part_fch_nonasstproject(
	   p_partnumber       IN   T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE
	  ,p_company_id       IN   t1900_company.c1900_company_id%TYPE
	  ,p_projects  	  OUT  TYPES.cursor_type
    )
    AS    
    BEGIN
    	OPEN p_projects FOR 
    	
    	SELECT t202.C202_PROJECT_ID projectid, t202.C202_PROJECT_NM projectname, t202.C202_PROJECT_ID || '-' || t202.C202_PROJECT_NM projectidname 
          FROM  T202_PROJECT t202 
         WHERE t202.C202_PROJECT_ID IN 
               (SELECT C202_PROJECT_ID FROM T202_PROJECT WHERE C202_VOID_FL IS NULL 
                MINUS 
        		(SELECT C202_PROJECT_ID 
          		FROM T2021_PART_PROJECT_MAPPING 
         		WHERE C205_PART_NUMBER_ID = p_partnumber 
           		AND C2021_VOID_FL IS NULL
                UNION
                SELECT C202_PROJECT_ID 
          		FROM T205_PART_NUMBER
         		WHERE C205_PART_NUMBER_ID = p_partnumber 
         		)
               ) 
         
		  AND t202.c1900_company_id = p_company_id
AND t202.C202_VOID_FL IS NULL;

END gm_part_fch_nonasstproject;
/*******************************************************
 * Description : Procedure to fetch the project which are mapped to part
 * Author	    : Mani
 *******************************************************/
    PROCEDURE gm_part_fch_assocProject (
		p_partnumber	  IN  	  T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE	
	  , p_type	  		  IN 	  T901_CODE_LOOKUP.C901_CODE_ID%TYPE
	  , p_projects  	  OUT 	  TYPES.cursor_type
    )
    AS
    
    BEGIN
	    OPEN p_projects FOR  
	     SELECT t2021.C202_PROJECT_ID projectID, t202.C202_PROJECT_NM projectname 
	     , get_user_name(t2021.c2021_updated_by) addedby , t2021.c2021_updated_date addeddt
         FROM T2021_PART_PROJECT_MAPPING t2021, T202_PROJECT t202 
         WHERE t2021.C205_PART_NUMBER_ID = p_partnumber
         AND t2021.C202_PROJECT_ID = t202.C202_PROJECT_ID
         AND t2021.C901_PART_MAPPING_TYPE =p_type 
         AND t2021.c2021_void_fl IS NULL
         AND t202.c202_void_fl IS NULL;
      
   END gm_part_fch_assocProject;
/*******************************************************
	  * Description : Procedure to save the Associate Project for the Part
	  * Author	    : Mani
*******************************************************/
    PROCEDURE gm_sav_asstproject(
		p_partnumber	  IN      T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE,
		p_type	  		  IN 	  T901_CODE_LOOKUP.C901_CODE_ID%TYPE,
		p_assprojects	  IN  	  CLOB,
		p_userid	      IN  	  T101_USER.C101_USER_ID%TYPE
    )
    AS
    v_strlen    NUMBER          := NVL (LENGTH (p_assprojects), 0) ;
    v_string    VARCHAR2 (4000) := p_assprojects;
    v_substring VARCHAR2 (1000) ;
    v_assigned_projectid     T202_PROJECT.C202_PROJECT_ID%type; 
    v_division_id            T202_PROJECT.C1910_DIVISION_ID%type;
    v_company_id             T1900_COMPANY.c1900_company_id%type;
    v_country_id             T205H_PART_RFS_MAPPING.C901_RFS_COUNTRY_ID%type;
    v_void_fl                VARCHAR2(1);
    v_rfs_status             NUMBER;
    BEGIN
	update T2021_PART_PROJECT_MAPPING 
      set  C2021_VOID_FL = 'Y', 
           C2021_UPDATED_DATE= CURRENT_DATE, 
           C2021_UPDATED_BY = p_userid 
    where  C205_PART_NUMBER_ID = p_partnumber 
    and    C901_PART_MAPPING_TYPE = p_type;
    
    WHILE INSTR (v_string, ',') <> 0
  
		  LOOP
            v_substring := SUBSTR (v_string, 1, INSTR (v_string, ',') - 1) ;
            v_string    := SUBSTR (v_string, INSTR (v_string, ',')    + 1) ;
            v_assigned_projectid     := (v_substring) ;   
		   gm_sav_part_project_mapping(p_partnumber, v_assigned_projectid,p_type,p_userid);
      END LOOP;
      	   gm_upd_part_company_map(p_partnumber,p_type,p_userid);	
   END gm_sav_asstproject;
   

/*******************************************************
* Description : Procedure to save part and project mapping.
* Author	    : Mani
*******************************************************/
	PROCEDURE gm_sav_part_project_mapping (
	p_partnumber	  IN      T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE
  , p_project_id	  IN	  T202_PROJECT.C202_PROJECT_ID%type	
  , p_type	  		  IN 	  T901_CODE_LOOKUP.C901_CODE_ID%TYPE
  , p_userid	      IN  	  T101_USER.C101_USER_ID%TYPE
    )
    AS
    v_company_id   T1900_COMPANY.C1900_COMPANY_ID%type;
    v_country_id   T205H_PART_RFS_MAPPING.C901_RFS_COUNTRY_ID%type;
    v_void_fl      VARCHAR2(1);
    v_rfs_status   NUMBER;
    BEGIN
    	UPDATE T2021_PART_PROJECT_MAPPING
		  SET  C2021_VOID_FL = NULL 
		     , C2021_UPDATED_BY  = p_userid
		     , C2021_UPDATED_DATE = CURRENT_DATE
		 WHERE C202_PROJECT_ID = p_project_id
		   AND C205_PART_NUMBER_ID = p_partnumber
		   AND C901_PART_MAPPING_TYPE = p_type;	
	 
		 IF (SQL%ROWCOUNT = 0)
			THEN				     
		    	INSERT INTO T2021_PART_PROJECT_MAPPING(
		    			C2021_PART_PROJ_MAP_ID,C205_PART_NUMBER_ID,C202_PROJECT_ID,C901_PART_MAPPING_TYPE,
		    			C2021_UPDATED_BY,C2021_UPDATED_DATE) 
	 			VALUES (S2021_PART_PROJECT_MAPPING.nextval,p_partnumber, p_project_id, p_type, p_userid,CURRENT_DATE);	 				 
	     END IF;    
    END gm_sav_part_project_mapping;  
/*******************************************************************************
	  * Description : Procedure to save/update the part entry to company mapping.
	  * After Mapping a Part to a Company in T2023_part_company_mapping Table
	  * , Part will be available for transaction
	  * Author	    : Mani
*******************************************************/
	PROCEDURE gm_upd_part_company_map(
    p_partnumber  IN    T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE
   ,p_type        IN    T901_CODE_LOOKUP.C901_CODE_ID%TYPE
   ,p_userid	  IN  	T101_USER.C101_USER_ID%TYPE
   )
   AS
   
   v_division_id T202_PROJECT.C1910_DIVISION_ID%type;
   v_company_id T1900_COMPANY.C1900_COMPANY_ID%type;
   v_country_id T205H_PART_RFS_MAPPING.C901_RFS_COUNTRY_ID%type;
   v_void_fl VARCHAR2(1);
   v_rfs_status NUMBER;
   v_count NUMBER ; 
   
   /**********************************************************************
    * The Cursor is called to Select all the Project Mapped to the Part.
    * Looping is done for Project ID and With Project ID Fetching Division ID and Company ID. 
    * and With Company ID Fetching the Country ID.
    ***************************************************************************/  
   
   CURSOR cur_upd_assoc_project
        IS 
        	SELECT t2021.C202_PROJECT_ID projectID,t2021.C2021_VOID_FL void_fl
        	FROM T2021_PART_PROJECT_MAPPING t2021
        	WHERE t2021.C205_PART_NUMBER_ID = p_partnumber
        	AND t2021.C901_PART_MAPPING_TYPE =p_type;
          
        BEGIN
        	FOR cur_project_mapping IN cur_upd_assoc_project
        		LOOP
        			BEGIN
        				SELECT C1910_DIVISION_ID 
        				INTO v_division_id 
						FROM T202_PROJECT
						WHERE C202_PROJECT_ID = cur_project_mapping.projectID
						AND C202_VOID_FL is null;
					EXCEPTION
 						WHEN NO_DATA_FOUND
    					THEN
      					v_division_id := NULL;
        			END;
        			
        			BEGIN
        				SELECT C1900_COMPANY_ID
        				INTO v_company_id
	    				FROM T2021_PROJECT_COMPANY_MAPPING 
	   			 		WHERE C202_PROJECT_ID = cur_project_mapping.projectID
	   			 		AND C901_PROJ_COM_MAP_TYP = '105360' --105360 (Created).
	    				AND C2021_VOID_FL is null;
	    			EXCEPTION
 						WHEN NO_DATA_FOUND
    					THEN
      					v_company_id := NULL;
      				END;
	    			BEGIN       				
		    			SELECT C901_RFS_ID 
		    			INTO v_country_id
	    				FROM T901C_RFS_COMPANY_MAPPING 
	    				WHERE C1900_COMPANY_ID =v_company_id 
	    				AND C901C_VOID_FL is null;
	    			EXCEPTION
 						WHEN NO_DATA_FOUND
    					THEN
      					v_country_id := NULL;
      				END;
	    			/*
	    			 * The Count is Taken here for Updating the Void flag in Part Company Mapping Table
	    			 * If a Part is Primary in GMNA,Then The record should not be voided in T2023_PART_COMPANY_MAPPING
	    			 * Only the Project which are mapped as secondary to a Part needs to be voided.
	    			 * If a Part is mapped as primary to a company count will be 1 and so the record will not be voided in T2023_PART_COMPANY_MAPPING
	    			 * For Example For a Part 124.000 Created in GMNA will be having the mapping type (C901_PART_MAPPING_TYPE)as 105360. 
	    			 * So when we add and Remove Projects related to GMNA should not affect the Entry in T2023_PART_COMPANY_MAPPING.
	    			 * For Part 124.000 if we map a Project Created in BBA There will be an Entry in T2023_PART_COMPANY_MAPPING,When we remove that Project
	    			 * Entry in T2023_PART_COMPANY_MAPPING needs to be voided.
	    			 */
        			SELECT count(1) into v_count 
        			FROM  T2023_PART_COMPANY_MAPPING 
        			WHERE C205_PART_NUMBER_ID = p_partnumber
        			AND C1900_COMPANY_ID = v_company_id
        			AND C901_PART_MAPPING_TYPE = '105360'; --105360 (Created).
        			    			
        
	    			IF ((cur_project_mapping.void_fl = 'Y') AND (v_count = 0)) THEN
	     				v_rfs_status :='70172'; --Status for Voiding the record in T2023_PART_COMPANY_MAPPING.
	     			ELSE
	     				v_rfs_status :='70171'; --Status for Inserting or Updating the part Number Entry in T2023_PART_COMPANY_MAPPING.
	    			END IF;
	    			
	     			gm_pkg_qa_rfs.gm_sav_part_company_map(p_partnumber,v_company_id,v_country_id,'105361',v_rfs_status,p_userid);
	  END LOOP;
	 
END gm_upd_part_company_map;
END gm_pkg_part_mapping;
/