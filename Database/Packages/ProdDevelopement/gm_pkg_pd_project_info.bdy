/* Formatted on 2009/03/10 14:31 (Formatter Plus v4.8.0) */
--@"C:\database\Packages\ProdDevelopement\gm_pkg_pd_project_info.bdy"

CREATE OR REPLACE PACKAGE BODY gm_pkg_pd_project_info
IS
	/******************************************************
	* Description : Procedure to fetch project details
	* Author		: Ritesh Shah
	*******************************************************/
	PROCEDURE gm_fch_projectdetails (
		p_projectid 	  IN	   t202_project.c202_project_id%TYPE
	  , p_outprjdetails   OUT	   TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_outprjdetails
		 FOR
			 SELECT t202.c202_project_id prjid, t202.c202_project_nm prjnm, t202.c202_project_desc prjdesc
				  , t202.c901_project_type prjtype, t202.c202_created_date prjcrdate
				  , t202.c202_created_by prjcrtdby
				  , t202.c202_last_updated_date prjlastupddt, t202.c202_insert_id prjinsid
				  , t202.c202_insert_rev prjinsrev, t202.c202_last_updated_by prjlastupdby
				  , DECODE(c202_last_updated_by,NULL,get_user_name (c202_created_by),get_user_name (c202_last_updated_by)) prjlastupdusrnm
				  , NVL(t202.c202_last_updated_date,t202.c202_created_date) prjlastupdate
				  , t202.c202_release_for_sale prjrls
				  , t202.c901_status_id prjstatusid, get_code_name (t202.c901_status_id) prjstatus
				  , t202.c101_initiated_by prjiniby, t202.c101_approved_by prjaprvdby
				  , t202.c1910_division_id divisionid
				  , t202.c202_project_dtl_desc prjdtldesc
				  , t202.c202_project_id projectID
			   FROM t202_project t202
			  WHERE c202_project_id = p_projectid;
	END gm_fch_projectdetails;

	/*******************************************************
	* Description : Procedure to fetch project list
	* Author		: Ritesh Shah
	*******************************************************/
	PROCEDURE gm_fch_projectlist (
		p_outprjlist   OUT	 TYPES.cursor_type
	)
	AS
	
		v_company_id t202_project.c1900_company_id%TYPE;
 
	BEGIN
		
		SELECT get_compid_frm_cntx() 
		   INTO v_company_id
           FROM DUAL;
	
		OPEN p_outprjlist
		 FOR
			 SELECT   t202.c202_project_id ID, t202.c202_project_id || ' - ' || t202.c202_project_nm NAME
				 FROM t202_project t202,t2021_project_company_mapping t2021
				WHERE t2021.c1900_company_id =  v_company_id
				  AND t202.c202_project_id = t2021.c202_project_id
				  AND t2021.c2021_void_fl IS NULL
				  AND t202.c202_void_fl IS NULL
			 ORDER BY t202.c202_project_id;
	END gm_fch_projectlist;

	/*******************************************************
	* Description : Procedure to fetch milestones details
	* Author		: Ritesh Shah
	*******************************************************/
	PROCEDURE gm_fch_milestonedetails (
		p_projectid 			IN		 t202_project.c202_project_id%TYPE
	  , p_outmilestonedetails	OUT 	 TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_outmilestonedetails
		 FOR
			 SELECT ROWNUM NO, t2005.c2005_project_milestone_id ID, t2005.c901_milestone milestone
				  , get_code_name (t2005.c901_milestone) milestonenm
				  , t2005.c2005_projected_dt pdt
				  , t2005.c2005_actual_dt adt, t2005.c901_group groupid
				  , get_code_name (t2005.c901_group) groupnm, c2005_prjctd_history_fl prjctdhistfl
				  , c2005_actual_history_fl acthistfl, get_log_flag (t2005.c2005_project_milestone_id, 1243) LOG
			   FROM t2005_project_milestone t2005
			  WHERE t2005.c202_project_id = p_projectid;
	END gm_fch_milestonedetails;

	/*********************************************************
	* Function to fetch the flag to distinguish the actual and projected date
	* Author: Satyajit Thadeshwar
	*********************************************************/
	FUNCTION get_actual_projected_date_flag (
		p_projected   t2005_project_milestone.c2005_projected_dt%TYPE
	  , p_actual	  t2005_project_milestone.c2005_actual_dt%TYPE
	)
		RETURN VARCHAR2
	IS
		v_flag		   VARCHAR2 (1);
	BEGIN
		IF (p_actual IS NOT NULL)
		THEN
			v_flag		:= 'B';
		ELSIF (p_projected IS NULL)
		THEN
			v_flag		:= 'T';
		ELSIF (p_projected < CURRENT_DATE)
		THEN
			v_flag		:= 'R';
		ELSE
			v_flag		:= 'N';
		END IF;

		RETURN v_flag;
	END get_actual_projected_date_flag;

	/*******************************************************
	* Description	 : Procedure to fetch projects report
	* Author		: Satyajit Thadeshwar
	*******************************************************/
	PROCEDURE gm_fch_projectsreport (
		p_projectid 	 IN 	  t202_project.c202_project_id%TYPE
	  , p_milestones	 IN 	  VARCHAR2
	  , p_outproject	 OUT	  TYPES.cursor_type
	  , p_outmsheader	 OUT	  TYPES.cursor_type
	  , p_outmsdetails	 OUT	  TYPES.cursor_type
	)
	AS
	v_company_id NUMBER;
	BEGIN
		my_context.set_my_inlist_ctx (p_milestones);
		
		SELECT 	get_compid_frm_cntx() 
		INTO 	v_company_id  
		FROM 	DUAL;

		OPEN p_outproject
		 FOR
			 SELECT t202.c202_project_id ID, (t202.c202_project_id || ' - ' || t202.c202_project_nm) name
				  , get_code_name (t202.c901_status_id) projstatus, get_code_name (t202.c901_group_id) projgrp
				  , get_code_name (t202.c901_segment_id) projseg, team.projmanager projmanager
				  , team.prodmanager prodmanager
				  , gm_pkg_pd_staffing_info.get_projected_team_count (t202.c202_project_id, 92048) dpcount
				  , gm_pkg_pd_staffing_info.get_actual_team_count (t202.c202_project_id, 92048) dacount
				  , gm_pkg_pd_staffing_info.get_projected_team_count (t202.c202_project_id, 92049) ppcount
				  , gm_pkg_pd_staffing_info.get_actual_team_count (t202.c202_project_id, 92049) pacount
			   FROM t202_project t202 ,t2021_project_company_mapping t2021
				  , (SELECT   c202_project_id projid
							, MAX (DECODE (t204.c901_role_id, 92042, get_party_name (t204.c101_party_id))
								  ) AS projmanager
							, MAX (DECODE (t204.c901_role_id, 92045, get_party_name (t204.c101_party_id))
								  ) AS prodmanager
						 FROM t204_project_team t204
						WHERE t204.c901_status_id = 92053
					 GROUP BY t204.c202_project_id) team
			  WHERE t202.c202_project_id = DECODE (p_projectid, '0', t202.c202_project_id, p_projectid)
			  	AND t2021.c1900_company_id = v_company_id
			  	AND t2021.c2021_void_fl IS NULL
			  	AND t202.c202_project_id = t2021.c202_project_id
				AND t202.c202_project_id = team.projid(+);

		OPEN p_outmsheader
		 FOR
			 SELECT c901_code_nm ms
			   FROM t901_code_lookup
			  WHERE c901_code_grp = 'PROMS' AND c901_code_id IN (SELECT *
																   FROM v_in_list);

		OPEN p_outmsdetails
		 FOR
			 SELECT   t202.c202_project_id ID, (t202.c202_project_id || ' - ' || t202.c202_project_nm) NAME
					, get_code_name (t2005.c901_milestone) ms, 1.0 num
					, NVL (t2005.c2005_actual_dt, t2005.c2005_projected_dt) dt
					, (DECODE (NVL (t2005.c2005_actual_dt, TO_DATE ('01/01/1111', 'MM/DD/YYYY'))
							 , TO_DATE ('01/01/1111', 'MM/DD/YYYY'), DECODE (LEAST (NVL (t2005.c2005_projected_dt
																					   , TO_DATE ('01/01/1111'
																								, 'MM/DD/YYYY'
																								 )
																						)
																				  , CURRENT_DATE
																				   )
																		   , TO_DATE ('01/01/1111', 'MM/DD/YYYY'), 'T'
																		   , t2005.c2005_projected_dt, 'R'
																		   , 'N'
																			)
							 , 'B'
							  )
					  ) dtfl
				 FROM t2005_project_milestone t2005, t202_project t202,t2021_project_company_mapping t2021
				WHERE t202.c202_project_id = t2005.c202_project_id(+)
				  AND t202.c202_project_id = DECODE (p_projectid, '0', t202.c202_project_id, p_projectid)
				  AND t2021.c1900_company_id = v_company_id
			  	  AND t2021.c2021_void_fl IS NULL
			      AND t202.c202_project_id = t2021.c202_project_id
			 ORDER BY t202.c202_project_id;
	--GROUP BY t2005.c202_project_id;
	END gm_fch_projectsreport;

	/*******************************************************
	* Description	: Procedure to fetch projects dashboard
	* Author		: Satyajit Thadeshwar
	*******************************************************/
	PROCEDURE gm_fch_projectsdashboard (
		p_outdashboard	 OUT   TYPES.cursor_type
	)
	AS
	v_company_id NUMBER;
	BEGIN
		
		OPEN p_outdashboard
		 FOR
			 SELECT   t202.c202_project_id ID, t202.c202_project_nm NAME, m_kickoffdt
																											 kickoffdt
					, m_kickoffdt_fl kickoffdt_fl, m_launchdt launchdt
					, m_launchdt_fl launchdt_fl, m_designstaffingdt designstaffingdt
					, m_designstaffingdt_fl designstaffingdt_fl, m_pugstaffingdt pugstaffingdt
					, m_pugstaffingdt_fl pugstaffingdt_fl
					, gm_pkg_pd_staffing_info.get_projected_team_count (t202.c202_project_id, 92048) dpcount
					, gm_pkg_pd_staffing_info.get_actual_team_count (t202.c202_project_id, 92048) dacount
					, gm_pkg_pd_staffing_info.get_projected_team_count (t202.c202_project_id, 92049) ppcount
					, gm_pkg_pd_staffing_info.get_actual_team_count (t202.c202_project_id, 92049) pacount
				 FROM t202_project t202,t2021_project_company_mapping t2021
					, (SELECT	c202_project_id projid
							  , MAX (DECODE (t2005.c901_milestone
										   , 92018, NVL (t2005.c2005_actual_dt, t2005.c2005_projected_dt)
											)
									) AS m_kickoffdt
							  , MAX
									(DECODE
										 (t2005.c901_milestone
										, 92018, gm_pkg_pd_project_info.get_actual_projected_date_flag
																							  (t2005.c2005_projected_dt
																							 , t2005.c2005_actual_dt
																							  )
										 )
									) AS m_kickoffdt_fl
							  , MAX (DECODE (t2005.c901_milestone
										   , 92034, NVL (t2005.c2005_actual_dt, t2005.c2005_projected_dt)
											)
									) AS m_launchdt
							  , MAX
									(DECODE
										 (t2005.c901_milestone
										, 92034, gm_pkg_pd_project_info.get_actual_projected_date_flag
																							  (t2005.c2005_projected_dt
																							 , t2005.c2005_actual_dt
																							  )
										 )
									) AS m_launchdt_fl
							  , MAX (DECODE (t2005.c901_milestone, 92035, t2005.c2005_projected_dt)
									) AS m_designstaffingpdt
							  , MAX (DECODE (t2005.c901_milestone
										   , 92035, NVL (t2005.c2005_actual_dt, t2005.c2005_projected_dt)
											)
									) AS m_designstaffingdt
							  , MAX
									(DECODE
										 (t2005.c901_milestone
										, 92035, gm_pkg_pd_project_info.get_actual_projected_date_flag
																							  (t2005.c2005_projected_dt
																							 , t2005.c2005_actual_dt
																							  )
										 )
									) AS m_designstaffingdt_fl
							  , MAX (DECODE (t2005.c901_milestone
										   , 92036, t2005.c2005_projected_dt
											)) AS m_pugstaffingpdt
							  , MAX (DECODE (t2005.c901_milestone
										   , 92036, NVL (t2005.c2005_actual_dt, t2005.c2005_projected_dt)
											)
									) AS m_pugstaffingdt
							  , MAX
									(DECODE
										 (t2005.c901_milestone
										, 92036, gm_pkg_pd_project_info.get_actual_projected_date_flag
																							  (t2005.c2005_projected_dt
																							 , t2005.c2005_actual_dt
																							  )
										 )
									) AS m_pugstaffingdt_fl
						   FROM t2005_project_milestone t2005
					   GROUP BY c202_project_id) milestones
				WHERE t202.c202_project_id = milestones.projid
				  AND (   (    milestones.m_designstaffingpdt <= CURRENT_DATE
						   AND (   gm_pkg_pd_staffing_info.get_projected_team_count (t202.c202_project_id, 92048) <>
											 gm_pkg_pd_staffing_info.get_actual_team_count (t202.c202_project_id, 92048)
								OR gm_pkg_pd_staffing_info.get_actual_team_count (t202.c202_project_id, 92048) = 0
							   )
						  )
					   OR (    milestones.m_pugstaffingpdt <= CURRENT_DATE
						   AND (   gm_pkg_pd_staffing_info.get_projected_team_count (t202.c202_project_id, 92049) <>
											 gm_pkg_pd_staffing_info.get_actual_team_count (t202.c202_project_id, 92049)
								OR gm_pkg_pd_staffing_info.get_actual_team_count (t202.c202_project_id, 92049) = 0
							   )
						  )
					  )
					 AND t2021.c1900_company_id = v_company_id
					 AND t202.c202_project_id = t2021.c202_project_id
					 AND t2021.c2021_void_fl IS NULL
			 ORDER BY t202.c202_project_id;
			 
			 SELECT get_compid_frm_cntx() 
			   INTO v_company_id  
			   FROM DUAL;
			   
	END gm_fch_projectsdashboard;
	
	/**************************************************************
	* Description 	: Procedure to fetch project attribute values
	* Author		: Jignesh Shah
	***************************************************************/
	PROCEDURE gm_fch_project_attr (
		p_proj_id			IN		t202a_project_attribute.c202_project_id%TYPE
		, p_attr_type		IN		t202a_project_attribute.c202_project_id%TYPE
		, p_out_prj_attr	OUT		TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_out_prj_attr
		 FOR
			SELECT t202a.C202a_Attribute_Value attrval, t202a.C901_Attribute_Type Id
			FROM t202a_project_attribute t202a
			WHERE t202a.c202_project_id = p_proj_id
            AND T202a.C901_Attribute_Type = p_attr_type 
            AND t202a.c202a_void_fl IS NULL;
	END gm_fch_project_attr;
	
	/*************************************************************
	* Description 	: Procedure to fetch project company mapping values
	* Author		: rdinesh
	**************************************************************/
	PROCEDURE gm_fch_proj_comp_map (
		  p_project_id			IN		t202_project.c202_project_id%TYPE
		, p_prj_comp_map_cur	OUT		TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_prj_comp_map_cur
		 FOR
			SELECT t1900.c1900_company_id comp_id, t1900.c1900_company_name comp_nm
			    ,get_code_name(t2021.c901_proj_com_map_typ) status
			    ,get_user_name(t2021.c2021_last_updated_by) Released_by
                ,t2021.c2021_last_updated_date released_dt
			FROM t2021_project_company_mapping t2021, t1900_company t1900
		   WHERE t2021.c202_project_id=p_project_id
		     AND t1900.c1900_company_id = t2021.c1900_company_id
		     AND t1900.c1900_void_fl IS NULL
		     AND t2021.c2021_void_fl IS NULL
		     ORDER BY t2021.c2021_last_updated_date, t1900.c1900_company_name ;
	END gm_fch_proj_comp_map;
	
	/*************************************************************
	* Description 	: Procedure to fetch project company mapping values
	* Author		: rdinesh
	**************************************************************/
	PROCEDURE gm_fch_project_comp_list (
	 	   p_project_id			IN		t202_project.c202_project_id%TYPE
		 , p_prj_comp_map_cur	OUT		TYPES.cursor_type
	)
	AS
		   v_comp_id  t1900_company.c1900_company_id%TYPE;
	BEGIN

		SELECT get_compid_frm_cntx()
		  INTO v_comp_id 
		  FROM dual;
	
		OPEN p_prj_comp_map_cur
		 FOR
		 
			SELECT c1900_company_id companyid,
		       c1900_company_cd companycd,
		       c1900_company_name companynm
		  FROM t1900_company
	     WHERE c1900_parent_company_id IS NULL 
		   AND c1900_void_fl IS NULL
           AND c1900_company_id NOT IN(
	              SELECT c1900_company_id
	           FROM t2021_project_company_mapping
	          WHERE c202_project_id = p_project_id
	            AND c2021_void_fl  IS NULL
           		) 
		   ORDER BY c1900_company_name;		   
		   
	END gm_fch_project_comp_list;
	
/*************************************************************
	* Description 	: Procedure to check Project Id
	* Author		: mjayaraman
	**************************************************************/
	PROCEDURE gm_check_project_id (
		  p_project_id			IN		t202_project.c202_project_id%TYPE
		, p_flag	OUT		varchar2
	)
	AS
	v_count NUMBER;
	BEGIN
		SELECT COUNT(*)
        INTO v_count
        FROM t202_project
        WHERE c202_project_id=p_project_id;		
		IF (v_count = 0)
		THEN
			p_flag		:= 'Y';
		ELSE
			p_flag		:= 'N';
		END IF;	
	END gm_check_project_id;

END gm_pkg_pd_project_info;
/
