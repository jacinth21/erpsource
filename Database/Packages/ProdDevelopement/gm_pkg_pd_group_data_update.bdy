/* Formatted on 2010/05/20 14:21 (Formatter Plus v4.8.0) */

--@"C:\Database\Packages\ProdDevelopement\gm_pkg_pd_group_data_update.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_pd_group_data_update
IS
--
	PROCEDURE gm_add_party_pd_pricing_group
	AS
		v_partyid	   t101_party.c101_party_id%TYPE;

		CURSOR curr_user
		IS
			SELECT c101_user_id userid, c101_user_f_name firstname, c101_user_l_name lastname
			  FROM t101_user
			 WHERE (   (c901_dept_id = 2004 AND c101_access_level_id >= 4)
					OR (c901_dept_id = 2026)
					OR (c101_user_id = 303042)
          OR (c901_dept_id = 2006)
				   )
			   AND c901_user_status = 311
			   AND c101_party_id IS NULL;
	BEGIN
		FOR var_user IN curr_user
		LOOP
			gm_pkg_cm_party.gm_cm_sav_party_id (var_user.userid
											  , var_user.firstname
											  , var_user.lastname
                        , ''
											  , ''
											  , '7006'
											  , 'Y'
											  , '30301'
											  , v_partyid
											   );
			DBMS_OUTPUT.put_line ('For User id :' || var_user.userid || ' new party id is: ' || v_partyid);

			UPDATE t101_user
			   SET c101_party_id = v_partyid
				 , c101_last_updated_by = 30301
				 , c101_last_updated_date = SYSDATE
			 WHERE c101_user_id = var_user.userid;
		END LOOP;
	END gm_add_party_pd_pricing_group;

END gm_pkg_pd_group_data_update;
/
