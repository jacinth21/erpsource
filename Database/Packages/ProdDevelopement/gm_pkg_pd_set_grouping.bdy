/* Formatted on 2009/06/01 18:39 (Formatter Plus v4.8.0) */
--@"c:\database\packages\ProdDevelopement\gm_pkg_pd_set_grouping.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_pd_set_grouping
IS
	PROCEDURE gm_sav_set_link (
		p_main_set_id	  t207a_set_link.c207_main_set_id%TYPE
	  , p_link_set_id	  t207a_set_link.c207_link_set_id%TYPE
	  , p_set_link_type   t207a_set_link.c901_type%TYPE
	)
	AS
	BEGIN
		INSERT INTO t207a_set_link
			 VALUES (s207a_set_link.NEXTVAL, p_main_set_id, p_link_set_id, p_set_link_type);
	END gm_sav_set_link;

	PROCEDURE gm_sav_set_master (
		p_set_id	   t207_set_master.c207_set_id%TYPE
	  , p_set_name	   t207_set_master.c207_set_nm%TYPE
	  , p_set_desc	   t207_set_master.c207_set_desc%TYPE
	  , p_category	   t207_set_master.c207_category%TYPE
	  , p_setgrptype   t207_set_master.c901_set_grp_type%TYPE
	  , p_seqno 	   t207_set_master.c207_seq_no%TYPE
	  , p_consrptid    t207_set_master.c901_cons_rpt_id%TYPE
	  , p_hierarchy    t207_set_master.c901_hierarchy%TYPE
	  , p_userid	   t207_set_master.c207_created_by%TYPE
	)
	AS
	BEGIN
		INSERT INTO t207_set_master
					(c207_set_id, c207_set_nm, c207_set_desc, c207_created_date, c207_created_by, c207_category
				   , c901_set_grp_type, c207_seq_no, c901_cons_rpt_id, c901_hierarchy
					)
			 VALUES (p_set_id, p_set_name, p_set_desc, SYSDATE, p_userid, p_category
				   , p_setgrptype, p_seqno, p_consrptid, p_hierarchy
					);
	END gm_sav_set_master;

	/*******************************************************
   * Description : Function to fetch main sales group for set id
   * Author 	 : Ritesh
   *******************************************************/
--
	FUNCTION get_salesgroup (
		p_setid   IN   t207_set_master.c207_set_id%TYPE
	)
		RETURN VARCHAR2
	IS
		v_salesgroup   VARCHAR2 (20);
	BEGIN
		BEGIN
			SELECT c.c207_main_set_id
			  INTO v_salesgroup
			  FROM t207_set_master t207, t207a_set_link a, t207a_set_link b, t207a_set_link c
			 WHERE a.c207_link_set_id = p_setid
			   AND a.c207_main_set_id = b.c207_link_set_id
			   AND t207.c207_set_id = c.c207_main_set_id
			   AND b.c207_main_set_id = c.c207_link_set_id;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				RETURN '';
		END;

		RETURN v_salesgroup;
	END get_salesgroup;

	/*******************************************************
   * Description : procedure to remove records from link table and set master.
   * Author 	 : Ritesh
   *******************************************************/
--
	PROCEDURE gm_remove_set_link (
		p_setid 		 IN   t207_set_master.c207_set_id%TYPE
	  , p_settype		 IN   t207_set_master.c207_type%TYPE
	  , p_sethierarchy	 IN   t207_set_master.c901_hierarchy%TYPE
	  , p_salesgroup	 IN   t207_set_master.c207_set_id%TYPE
	  , p_userid		 IN   t207_set_master.c207_created_by%TYPE
	)
	AS
		v_link_id	   NUMBER;
		v_main_set_id  t207a_set_link.c207_main_set_id%TYPE;
		v_ai_set_id    t207a_set_link.c207_main_set_id%TYPE;
		v_set_id	   t207_set_master.c207_set_id%TYPE;
		v_setnm 	   t207_set_master.c207_set_nm%TYPE;
		v_setdesc	   t207_set_master.c207_set_desc%TYPE;
		v_setcat	   t207_set_master.c207_category%TYPE;
		v_consrptid    t207_set_master.c901_cons_rpt_id%TYPE;
		v_type		   t207_set_master.c901_set_grp_type%TYPE;
		v_set_hierarchy t207_set_master.c901_hierarchy%TYPE;
		v_set_type	   t207_set_master.c207_type%TYPE;
	BEGIN
		SELECT	   c207_set_id, c207_set_nm, c207_set_desc, c207_category, c901_cons_rpt_id, c901_set_grp_type
				 , c207_type, c901_hierarchy
			  INTO v_set_id, v_setnm, v_setdesc, v_setcat, v_consrptid, v_type
				 , v_set_type, v_set_hierarchy
			  FROM t207_set_master
			 WHERE c207_set_id = p_setid
		FOR UPDATE;

		SELECT t207a.c207_link_set_id
		  INTO v_ai_set_id
		  FROM t207a_set_link t207a
		 WHERE t207a.t207a_set_link_id IN (SELECT MIN (t207a.t207a_set_link_id)
											 FROM t207a_set_link t207a
											WHERE t207a.c207_main_set_id = p_salesgroup);

		IF v_set_type != p_settype AND v_set_type = 4070   -- Consignment
		THEN
			raise_application_error (-20157, '');	--Contact IT
		ELSIF v_set_hierarchy != p_sethierarchy AND v_set_hierarchy = 20700   --for main set
		THEN
			BEGIN
				SELECT t207ab.t207a_set_link_id, t207ab.c207_link_set_id
				  INTO v_link_id, v_main_set_id
				  FROM t207a_set_link t207aa, t207a_set_link t207ab
				 WHERE t207aa.c207_link_set_id = p_setid
				   AND t207aa.c207_main_set_id = t207ab.c207_link_set_id
				   AND t207ab.c901_type = 20002;   --Main set mapping
			EXCEPTION
				WHEN NO_DATA_FOUND
				THEN
					raise_application_error (-20157, '');	--Contact IT
			END;

			DELETE FROM t207a_set_link
				  WHERE t207a_set_link_id IN (
							SELECT t207ab.t207a_set_link_id
							  FROM t207a_set_link t207aa, t207a_set_link t207ab
							 WHERE t207aa.c207_link_set_id = p_setid
							   AND t207aa.c207_main_set_id = t207ab.c207_link_set_id
							   AND t207ab.c901_type = 20002
							UNION
							SELECT t207aa.t207a_set_link_id
							  FROM t207a_set_link t207aa
							 WHERE t207aa.c207_link_set_id = p_setid AND t207aa.c901_type = 20003);

			DELETE FROM t207_set_master
				  WHERE c207_set_id = v_main_set_id;

			--adding the mapping for additional set.
			gm_pkg_pd_set_grouping.gm_sav_set_link (v_ai_set_id, p_setid, 20004);
		ELSIF v_set_hierarchy != p_sethierarchy AND p_sethierarchy = 20700
		THEN
			DELETE FROM t207a_set_link
				  WHERE c207_link_set_id = p_setid AND c207_main_set_id = v_ai_set_id;

			SELECT 'SET.' || s207_set_master_main_set.NEXTVAL
			  INTO v_main_set_id
			  FROM DUAL;

			gm_pkg_pd_set_grouping.gm_sav_set_master (v_main_set_id
													, v_setnm
													, v_setdesc || ' - Main'
													, v_setcat
													, v_type
													, NULL
													, v_consrptid
													, v_set_hierarchy
													, p_userid
													 );
			gm_pkg_pd_set_grouping.gm_sav_set_link (p_salesgroup, v_main_set_id, 20002);
			gm_pkg_pd_set_grouping.gm_sav_set_link (v_main_set_id, p_setid, 20003);
		END IF;
	END gm_remove_set_link;
END gm_pkg_pd_set_grouping;
/
