/* Formatted on 2010/05/18 12:29 (Formatter Plus v4.8.0) */

--@"C:\Database\Packages\ProdDevelopement\gm_pkg_group_part_notification.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_group_part_notification
IS
--

	/***************************************************************
	* Description : Procedure to check any group is published daily
	* Author		: Xun
	***************************************************************/
	PROCEDURE gm_fch_group_publish
	AS
		subject 	   VARCHAR2 (200);
		to_mail 	   VARCHAR2 (1000);
		mail_body	   VARCHAR2 (4000);
		crlf		   VARCHAR2 (2) := CHR (13) || CHR (10);

		CURSOR cur_detail
		IS
			SELECT t4010.c4010_group_nm groupnm, t4010.c4010_group_publish_date publish_date
				 , get_user_name (t4010.c101_group_publish_by) owner
			  FROM t4010_group t4010
			 WHERE TRUNC (t4010.c4010_group_publish_date) = TRUNC (SYSDATE) AND t4010.c4010_void_fl IS NULL;
	BEGIN
		to_mail 	:=
			'hparikh@globusmedical.com,hpatel@globusmedical.com,lsummers@globusmedical.com,jkumar@globusmedical.com,jbutterweck@globusmedical.com';   --get_dept_emails_info (2026);
		subject 	:= 'The groups are published today';

		FOR var_detail IN cur_detail
		LOOP
			mail_body	:=
				   mail_body
				|| ' Group - '
				|| var_detail.groupnm
				|| ' is published on '
				|| var_detail.publish_date
				|| ' by ---'
				|| var_detail.owner
				|| crlf
				|| crlf;
		END LOOP;

		IF mail_body IS NOT NULL
		THEN
			gm_com_send_email_prc (to_mail, subject, mail_body);
		END IF;
	END gm_fch_group_publish;

	/**********************************************************************
	* Description : Procedure to check any part is added to the group daily
	* Author		: Xun
	***********************************************************************/
	PROCEDURE gm_fch_part_add
	AS
		subject 	   VARCHAR2 (200);
		to_mail 	   VARCHAR2 (1000);
		mail_body	   VARCHAR2 (4000);
		crlf		   VARCHAR2 (2) := CHR (13) || CHR (10);

		CURSOR cur_detail
		IS
			SELECT t7011.c205_part_number_id partnum, get_group_name (t7011.c4010_group_id) groupnm
				 , get_user_name (t7011.c7011_action_by) action_by, t7011.c7011_action_date action_date
			  FROM t7011_part_price_log t7011, t4010_group t4010
			 WHERE t7011.c7011_void_fl IS NULL
			   AND t7011.c4010_group_id = t4010.c4010_group_id
			   AND t4010.c4010_publish_fl = 'Y'
			   AND TRUNC (t7011.c7011_action_date) = TRUNC (SYSDATE)
			   AND t7011.c901_log_type = 52091;   --add
	BEGIN
		to_mail 	:=
			'hparikh@globusmedical.com,hpatel@globusmedical.com,lsummers@globusmedical.com,jkumar@globusmedical.com,jbutterweck@globusmedical.com';   -- get_dept_emails_info (2026);
		subject 	:= 'The parts which were added into the group today';

		FOR var_detail IN cur_detail
		LOOP
			mail_body	:=
				   mail_body
				|| ' Part - '
				|| var_detail.partnum
				|| ' is added in the group '
				|| var_detail.groupnm
				|| ' on '
				|| var_detail.action_date
				|| ' by ---'
				|| var_detail.action_by
				|| crlf
				|| crlf;
		END LOOP;

		IF mail_body IS NOT NULL
		THEN
			gm_com_send_email_prc (to_mail, subject, mail_body);
		END IF;
	END gm_fch_part_add;

	/**********************************************************************
	* Description : Procedure to check any part is removed to the group daily
	* Author		: Xun
	***********************************************************************/
	PROCEDURE gm_fch_part_delete
	AS
		subject 	   VARCHAR2 (200);
		to_mail 	   VARCHAR2 (1000);
		mail_body	   VARCHAR2 (4000);
		crlf		   VARCHAR2 (2) := CHR (13) || CHR (10);

		CURSOR cur_detail
		IS
			SELECT t7011.c205_part_number_id partnum, get_group_name (t7011.c4010_group_id) groupnm
				 , get_user_name (t7011.c7011_action_by) action_by, t7011.c7011_action_date action_date
			  FROM t7011_part_price_log t7011, t4010_group t4010
			 WHERE t7011.c7011_void_fl IS NULL
			   AND t7011.c4010_group_id = t4010.c4010_group_id
			   AND t4010.c4010_publish_fl = 'Y'
			   AND TRUNC (t7011.c7011_action_date) = TRUNC (SYSDATE)
			   AND t7011.c901_log_type = 52090;   --delete
	BEGIN
		to_mail 	:=
			'hparikh@globusmedical.com,hpatel@globusmedical.com,lsummers@globusmedical.com,jkumar@globusmedical.com,jbutterweck@globusmedical.com';   --get_dept_emails_info (2026);
		subject 	:= 'The parts which were removed from the group today';

		FOR var_detail IN cur_detail
		LOOP
			mail_body	:=
				   mail_body
				|| ' Part - '
				|| var_detail.partnum
				|| ' is deleted from the group '
				|| var_detail.groupnm
				|| ' on '
				|| var_detail.action_date
				|| ' by '
				|| var_detail.action_by
				|| crlf
				|| crlf;
		END LOOP;

		IF mail_body IS NOT NULL
		THEN
			gm_com_send_email_prc (to_mail, subject, mail_body);
		END IF;
	END gm_fch_part_delete;
END;
/
