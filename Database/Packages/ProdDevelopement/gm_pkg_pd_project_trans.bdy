/* Formatted on 2009/03/18 17:12 (Formatter Plus v4.8.0) */
--@"C:\database\Packages\ProdDevelopement\gm_pkg_pd_project_trans.bdy"

CREATE OR REPLACE PACKAGE BODY gm_pkg_pd_project_trans
IS
/*******************************************************
	* Description : Procedure to fetch project details
	* Author		: Ritesh Shah
	*******************************************************/
	PROCEDURE gm_sav_projectdetails (
		p_projnm		IN		 t202_project.c202_project_nm%TYPE
	  , p_projdesc		IN		 t202_project.c202_project_desc%TYPE
	  , p_projid		IN		 t202_project.c202_project_id%TYPE
	  , p_status		IN		 t202_project.c901_status_id%TYPE
	  , p_userid		IN		 t202_project.c202_created_by%TYPE
	  , p_projecttype	IN		 t202_project.c901_project_type%TYPE
	  , p_initiatedby	IN		 t202_project.c101_initiated_by%TYPE
	  , p_approvedby	IN		 t202_project.c101_approved_by%TYPE
	  , p_division_id   IN       t1910_division.c1910_division_id%TYPE
	  , p_proj_dtl_desc IN       t202_project.c202_project_dtl_desc%TYPE
	  , p_company_id    IN       t202_project.c1900_company_id%TYPE
	  , p_new_project_id IN      t202_project.c202_project_id%TYPE
	  , p_message		OUT 	 VARCHAR2
	  , p_projectid 	OUT 	 t202_project.c202_project_id%TYPE
	)
	AS
		v_proj_id	   NUMBER;
		v_string	   VARCHAR2 (10);
		v_id_string    VARCHAR2 (10);
		v_project_upd_fl VARCHAR2(5);
	BEGIN
		--assign the updated project id
		p_projectid := p_new_project_id;		
		--
		IF (p_projid <> '0' AND  p_projid <> p_new_project_id)
		THEN
			-- to set the project update flag
			v_project_upd_fl := 'Y';
			-- to update current project id to temp project id
			gm_upd_project_id (p_projid, 'GM-TEMP-ID', p_userid);
		END IF;
		
		UPDATE t202_project
		   SET c202_project_nm = p_projnm
			 , c202_project_desc = p_projdesc
			 , c202_project_dtl_desc = p_proj_dtl_desc
			 , c901_status_id = p_status
			 , c202_last_updated_by = p_userid
			 , c202_last_updated_date = CURRENT_DATE
			 , c901_project_type = DECODE (p_projecttype, '0', NULL, p_projecttype)
			 , c101_initiated_by = DECODE (p_initiatedby, '0', NULL, p_initiatedby)
			 , c101_approved_by = DECODE (p_approvedby, '0', NULL, p_approvedby)
			 , c1910_division_id = DECODE (p_division_id, '0', NULL, p_division_id)
			 , c1900_company_id = p_company_id
			 , c202_project_id = DECODE(v_project_upd_fl, 'Y', p_new_project_id, c202_project_id)
		 WHERE c202_project_id = p_projid AND c202_void_fl IS NULL;

		IF (SQL%ROWCOUNT = 0)
		THEN			

			--SELECT CONCAT(v_proj_id,v_string) INTO v_string FROM DUAL;
			INSERT INTO t202_project
						(c202_project_id, c202_project_nm, c202_project_desc
					   , c202_created_by, c202_created_date, c202_project_dtl_desc
					   , c901_project_type
					   , c101_initiated_by, c901_status_id
					   , c101_approved_by, c1910_division_id 
					   , c1900_company_id
						)
				 VALUES (p_projectid, p_projnm, p_projdesc
					   , p_userid, CURRENT_DATE, p_proj_dtl_desc
					   , DECODE (p_projecttype, '0', NULL, p_projecttype)
					   , DECODE (p_initiatedby, '0', NULL, p_initiatedby), 20300   --initiated by default
					   , DECODE (p_approvedby, '0', NULL, p_approvedby)
					   , DECODE (p_division_id, '0', NULL, p_division_id)
					   , p_company_id
						);

			INSERT INTO t2005_project_milestone
						(c2005_project_milestone_id, c202_project_id, c901_milestone, c901_group, c2005_created_by
					   , c2005_created_date)
				SELECT s2005_project_milestone.NEXTVAL, p_projectid, c901_code_id, TO_NUMBER (c902_code_nm_alt), p_userid
					 , CURRENT_DATE
				  FROM t901_code_lookup
				 WHERE c901_code_grp = 'PROMS';

			INSERT INTO t2004_project_team_count
						(c2004_project_team_count_id, c202_project_id, c901_role, c2004_created_by, c2004_created_date)
				SELECT s2004_project_team_count.NEXTVAL, p_projectid, c901_code_id, p_userid, CURRENT_DATE
				  FROM t901_code_lookup
				 WHERE c901_code_grp IN ('GROLE', 'SROLE');
		END IF;
		
		
		-- Below procedure is used to save language details into t2000_master_metadata
		gm_pkg_pd_language.gm_sav_lang_details(	p_projectid
												, '103093'	-- 103093-Project Type
												, p_projnm
												, p_projdesc
												, p_proj_dtl_desc
												, p_userid
												);
		-- checking the project id updated flag												
		IF v_project_upd_fl ='Y'
		THEN
			-- to update the new project id to reference table
			gm_upd_project_id ('GM-TEMP-ID', p_new_project_id,  p_userid);
		END IF;
		
	END gm_sav_projectdetails;

	/*******************************************************
	* Description : Procedure to save milestone details
	* Author		: Ritesh Shah
	*******************************************************/
	PROCEDURE gm_sav_milestone_details (
		p_projectid 	t202_project.c202_project_id%TYPE
	  , p_inputstring	VARCHAR2
	  , p_userid		t2005_project_milestone.c2005_created_by%TYPE
	)
	AS
		CURSOR c_milestone
		IS
			SELECT c2005_project_milestone_id mid, c901_milestone milestone, c2005_projected_dt prjdt
			  FROM t2005_project_milestone
			 WHERE c202_project_id = p_projectid;

		v_string	   VARCHAR2 (4000) := p_inputstring;
		v_substring1   VARCHAR2 (4000);
		v_substring2   VARCHAR2 (4000);
		v_milestoneid  VARCHAR2 (20);
		v_pdt_new	   VARCHAR2 (20);
		v_adt		   VARCHAR2 (20);
		v_pdt_old	   VARCHAR2 (20);
		v_pdt_cnt1	   NUMBER := 0;
		v_pdt_cnt2	   NUMBER := 0;
		v_pdt_id1	   NUMBER;
		v_pdt_id2	   NUMBER;
	BEGIN
		WHILE INSTR (v_string, '|') <> 0
		LOOP
			v_substring1 := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
			v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
			v_milestoneid := SUBSTR (v_substring1, 1, INSTR (v_substring1, '^') - 1);
			v_substring2 := SUBSTR (v_substring1, INSTR (v_substring1, '^') + 1);
			v_pdt_new	:= SUBSTR (v_substring2, 1, INSTR (v_substring2, '@') - 1);
			v_adt		:= SUBSTR (v_substring2, INSTR (v_substring2, '@') + 1);

			FOR rec IN c_milestone
			LOOP
				IF (rec.mid = v_milestoneid AND rec.milestone = 92018)
				THEN
					IF (NVL (rec.prjdt, TO_DATE ('01/01/9999', 'MM/DD/YYYY')) <>
										   NVL (TO_DATE (v_pdt_new, 'MM/DD/YYYY'), TO_DATE ('01/01/9999', 'MM/DD/YYYY'))
					   )
					THEN
						UPDATE t2005_project_milestone
						   SET c2005_projected_dt = ADD_MONTHS (v_pdt_new, -2)
							 , c2005_last_updated_by = p_userid
							 , c2005_last_updated_date = CURRENT_DATE
						 WHERE c2005_project_milestone_id IN (
														  SELECT c2005_project_milestone_id
															FROM t2005_project_milestone
														   WHERE c202_project_id = p_projectid
																 AND c901_milestone = 92035);

						v_pdt_cnt1	:= v_pdt_cnt1 + 1;
					END IF;
				END IF;

				IF (rec.mid = v_milestoneid AND rec.milestone = 92033)
				THEN
					IF (NVL (rec.prjdt, TO_DATE ('01/01/9999', 'MM/DD/YYYY')) <>
										   NVL (TO_DATE (v_pdt_new, 'MM/DD/YYYY'), TO_DATE ('01/01/9999', 'MM/DD/YYYY'))
					   )
					THEN
						UPDATE t2005_project_milestone
						   SET c2005_projected_dt = ADD_MONTHS (v_pdt_new, -2)
							 , c2005_last_updated_by = p_userid
							 , c2005_last_updated_date = CURRENT_DATE
						 WHERE c2005_project_milestone_id IN (
														  SELECT c2005_project_milestone_id
															FROM t2005_project_milestone
														   WHERE c202_project_id = p_projectid
																 AND c901_milestone = 92036);

						v_pdt_cnt2	:= v_pdt_cnt2 + 1;
					END IF;
				END IF;
			END LOOP;

			SELECT c2005_project_milestone_id
			  INTO v_pdt_id1
			  FROM t2005_project_milestone
			 WHERE c202_project_id = p_projectid AND c901_milestone = 92035;

			SELECT c2005_project_milestone_id
			  INTO v_pdt_id2
			  FROM t2005_project_milestone
			 WHERE c202_project_id = p_projectid AND c901_milestone = 92036;

			IF ((v_pdt_id1 = v_milestoneid AND v_pdt_cnt1 = 1) OR (v_pdt_id2 = v_milestoneid AND v_pdt_cnt2 = 1))
			THEN
				NULL;
			ELSE
				UPDATE t2005_project_milestone
				   SET c2005_projected_dt = v_pdt_new
					 , c2005_actual_dt = v_adt
					 , c2005_last_updated_by = p_userid
					 , c2005_last_updated_date = CURRENT_DATE
				 WHERE c2005_project_milestone_id = TO_NUMBER (v_milestoneid) AND c2005_void_fl IS NULL;
			END IF;
		END LOOP;
	END gm_sav_milestone_details;

	/*******************************************************
	* Description : Procedure to change project status. Called from
	*				common cancel package
	* Author	  : Satyajit Thadeshwar
	*******************************************************/
	PROCEDURE gm_chg_projectstatus (
		p_projectid   t202_project.c202_project_id%TYPE
	  , p_status	  t202_project.c901_status_id%TYPE
	  , p_userid	  t202_project.c202_last_updated_by%TYPE
	)
	AS
	v_status t202_project.c901_status_id%TYPE;
	BEGIN
		--
		SELECT c901_status_id
		   INTO v_status
		   FROM t202_project
		  WHERE c202_project_id = p_projectid
		  AND c202_void_fl IS NULL;	
		-- Status is -- Initiated then
		IF v_status <> p_status AND p_status = 20300 --20300:Initiated 
		THEN
			raise_application_error ('-20999', 'Cannot update the project id status to Initiated') ;
		END IF;
		--
		UPDATE t202_project
		   SET c901_status_id = p_status
			 , c202_last_updated_by = p_userid
			 , c202_last_updated_date = CURRENT_DATE
		 WHERE c202_project_id = p_projectid;
	END gm_chg_projectstatus;
	
	/**************************************************************
	* Description	: Procedure to save project attributes details
	* Author		: Jignesh Shah
	***************************************************************/
	PROCEDURE gm_sav_project_attribute (
	  	p_projid				IN		 t202a_project_attribute.c202_project_id%TYPE
	  , p_projattrInputstr		IN		 CLOB
	  , p_attrtype_inputstr		IN		 CLOB
	  , p_userid				IN		 t202a_project_attribute.c202a_created_by%TYPE
	)
	AS
/*
 * This procedure is used save project attributes.
 * In this procedure we are passing project Id and 2 input strings
 * p_projattrInputstr for Project Attributes.
 * p_attrtype_inputstr for Attributes values related to attributes type
 * Here we are passing p_attrtype_inputstr and put into my_context.set_my_inlist_ctx which
 * will give all the atttributes one by one and after that we are going to void data 
 * belogs to that types.
 * Now we will split p_projattrInputstr which is original sting passed form sreen and we 
 * will do update and insert for that values according to types.
 */
		v_strlen		NUMBER := NVL (LENGTH (p_projattrInputstr), 0);
		v_string		VARCHAR2 (4000) := p_projattrInputstr;
		v_substring		VARCHAR2 (4000);
		v_attr_type		t202a_project_attribute.c901_attribute_type%TYPE;
		v_attr_value	t202a_project_attribute.c202a_attribute_value%TYPE;

	    
		CURSOR cur_details
		IS
		SELECT token
		FROM v_in_list
		WHERE token IS NOT NULL;
	BEGIN
		
		my_context.set_my_inlist_ctx (p_attrtype_inputstr);
		
		
		UPDATE t202a_project_attribute
		SET c202a_void_fl = 'Y'  
			, c202a_last_updated_date = CURRENT_DATE
			, c202a_last_updated_by = p_userid
		WHERE c202_project_id = p_projid 
		AND c901_attribute_type IN (SELECT * FROM v_in_list)
		AND c202a_void_fl IS null;
		
		
		WHILE INSTR (v_string, '|') <> 0
		LOOP
				--
			v_attr_type		:= NULL;
			v_attr_value	:= NULL;
			v_substring		:= SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
			v_string		:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
			v_attr_type		:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
			v_substring		:= SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
			v_attr_value	:= v_substring;
				

			UPDATE t202a_project_attribute
			SET c202a_void_fl = null  
				, c202a_last_updated_date = CURRENT_DATE
				, c202a_last_updated_by = p_userid
			WHERE c202_project_id = p_projid 
			AND c901_attribute_type = v_attr_type 
			AND c202a_attribute_value = v_attr_value;
				
			IF (SQL%ROWCOUNT = 0)
			THEN
			-- below procedure is call to insert data into table for project attributes
				gm_pkg_pd_project_trans.gm_sav_proj_attribute_dtls (p_projid	  	
																	, v_attr_type
																	, v_attr_value
																	, p_userid
																	);
			END IF;
				
		END LOOP;

	END gm_sav_project_attribute;
	
	/**************************************************************
	* Description	: Procedure to save project attributes details
	* Author		: Jignesh Shah
	***************************************************************/
	
	PROCEDURE gm_sav_proj_attribute_dtls (
	  	p_projid			IN		t202a_project_attribute.c202_project_id%TYPE
	  , p_attr_type			IN		t202a_project_attribute.c901_attribute_type%TYPE
	  , p_attr_value		IN		t202a_project_attribute.c202a_attribute_value%TYPE
	  , p_userid			IN		t202a_project_attribute.c202a_created_by%TYPE
	)
	AS
	BEGIN
		
		INSERT INTO t202a_project_attribute
		(
			c202a_project_attribute_id, c202_project_id, c901_attribute_type, c202a_attribute_value
			, c202a_created_date, c202a_created_by, c202a_last_updated_date, c202a_last_updated_by 
		)
		VALUES
		(
			s202a_project_attribute.NEXTVAL, p_projid, p_attr_type, p_attr_value
			, CURRENT_DATE, p_userid, CURRENT_DATE, p_userid
		);
		
	END gm_sav_proj_attribute_dtls;
	
	/*******************************************************
	* Description : Procedure to save project company mapping details
	* Author	  : rdinesh
	*******************************************************/
    PROCEDURE gm_sav_project_company_map (
	  	p_projid			IN		t202_project.c202_project_id%TYPE
	  , p_company_id        IN      t2021_project_company_mapping.c1900_company_id%TYPE
	  , p_map_type          IN      t2021_project_company_mapping.c901_proj_com_map_typ%TYPE
	  , p_userid			IN		t2021_project_company_mapping.c2021_last_updated_by%TYPE
	)
	AS
	
	    v_count NUMBER;
	
	BEGIN
	
	-- Check whether the project was already mapped with company, If it is so do not do any update else do insert.
	
		SELECT COUNT(1)
		    INTO v_count
		    FROM t2021_project_company_mapping
		WHERE c202_project_id  = p_projid
		AND   c1900_company_id = p_company_id
		;
		
		IF (v_count = 0) THEN 
		    
		
			INSERT INTO t2021_project_company_mapping
			(
			    c2021_proj_comp_map_id,c202_project_id,c1900_company_id,c901_proj_com_map_typ,c2021_last_updated_by,c2021_last_updated_date
			)
			VALUES
			(
			    s2021_project_company_mapping.nextval,p_projid,p_company_id,p_map_type,p_userid,CURRENT_DATE
			);
		
		ELSIF v_count=1
		THEN
			UPDATE 	T2021_PROJECT_COMPANY_MAPPING
			SET		C2021_VOID_FL = '',
					C2021_LAST_UPDATED_BY = p_userid,
					C2021_LAST_UPDATED_DATE =  CURRENT_DATE
			WHERE
					C202_PROJECT_ID = P_PROJID
			AND		C1900_COMPANY_ID = P_COMPANY_ID;
					
		END IF;
		
	END gm_sav_project_company_map;
	
	/*******************************************************
	* Description : Procedure to Void project company mapping details
	* Author	  : Rajeshwaran
	*******************************************************/
    PROCEDURE gm_void_project_company_map (
	  	p_projid			IN		t202_project.c202_project_id%TYPE
	  , p_company_id        IN      t2021_project_company_mapping.c1900_company_id%TYPE	  
	  , p_userid			IN		t2021_project_company_mapping.c2021_last_updated_by%TYPE
	)
	AS
	
	BEGIN
		UPDATE T2021_PROJECT_COMPANY_MAPPING
		SET		C2021_VOID_FL='Y',
				C2021_LAST_UPDATED_BY = p_userid,
				C2021_LAST_UPDATED_DATE = CURRENT_DATE
		WHERE
				C202_PROJECT_ID = p_projid
		AND		C1900_COMPANY_ID = p_company_id
		AND		C2021_VOID_FL IS NULL;
		
	END gm_void_project_company_map;
	
	
/*********************************************************
* Description : This procedure is used to update the new project id to reference table
* Author : mmuthusamy
*********************************************************/
PROCEDURE gm_upd_project_id (
        p_old_project_id IN t202_project.c202_project_id%TYPE,
        p_new_project_id IN t202_project.c202_project_id%TYPE,
        p_user_id        IN t202_project.c202_last_updated_by%TYPE)
AS
    --
    v_project_mile_ids VARCHAR2 (4000) ;
    v_project_team_ids VARCHAR2 (4000) ;
    --
    v_old_proj_cnt NUMBER;
    v_new_proj_cnt NUMBER;
BEGIN
    -- validate the old project id
     SELECT COUNT (1)
       INTO v_old_proj_cnt
       FROM t202_project
      WHERE c202_project_id = p_old_project_id
      AND c202_void_fl IS NULL;
    --
    IF v_old_proj_cnt = 0 THEN
        raise_application_error ('-20999', 'Entered Project not found, please enter the valid project ID : '||
        p_old_project_id) ;
    END IF;
    -- 1. to update the new project id to project milestone
     UPDATE t2005_project_milestone
    SET c202_project_id     = p_new_project_id, C2005_LAST_UPDATED_BY = p_user_id, C2005_LAST_UPDATED_DATE = CURRENT_DATE
      WHERE c202_project_id = p_old_project_id;
    -- 2. to update the new project id to project team count
     UPDATE t2004_project_team_count
    SET C202_PROJECT_ID     = p_new_project_id, c2004_last_updated_by = p_user_id, c2004_last_updated_date = CURRENT_DATE
      WHERE c202_project_id = p_old_project_id;
    -- 3. to update the new project id to project company mapping
     UPDATE T2021_PROJECT_COMPANY_MAPPING
    SET C202_PROJECT_ID     = p_new_project_id, c2021_last_updated_by = p_user_id, c2021_last_updated_date = CURRENT_DATE
      WHERE c202_project_id = p_old_project_id;
    -- 4. to update the new project id to project team
     UPDATE t204_project_team
    SET C202_PROJECT_ID     = p_new_project_id, c204_last_updated_by = p_user_id, c204_last_updated_date = CURRENT_DATE
      WHERE c202_project_id = p_old_project_id;
    --
END gm_upd_project_id; 

/**************************************************************************
* Description : This procedure is used to map existing project to company
* Author : mmuthusamy
****************************************************************************/
PROCEDURE gm_sav_prj_release_to_company (
        p_project_id IN t202_project.c202_project_id%TYPE,
        p_company_id IN t1900_company.c1900_company_id%TYPE,
        p_map_type IN t2021_project_company_mapping.c901_proj_com_map_typ%TYPE,
        p_user_id    IN t202_project.c202_last_updated_by%TYPE)
AS
    v_system_id t207_set_master.c207_set_sales_system_id%TYPE;
    --
    CURSOR part_dtls_cur
    IS
         SELECT c205_part_number_id pnum
           FROM t205_part_number
          WHERE c202_project_id = p_project_id;
    --
    CURSOR system_dtls_cur
    IS
         SELECT c207_set_sales_system_id sys_id
           FROM t207_set_master
          WHERE c202_project_id           = p_project_id
            AND c901_set_grp_type         = 1601 -- Set Report Group
            AND c207_void_fl             IS NULL
            AND c207_set_sales_system_id IS NOT NULL
       GROUP BY c207_set_sales_system_id;
    --
    CURSOR set_dtls_cur
    IS
         SELECT c207_set_id set_id
           FROM t207_set_master
          WHERE c207_set_sales_system_id = v_system_id
          	AND c202_project_id = p_project_id
            AND c901_set_grp_type        = 1601 -- Set Report Group
            AND c207_void_fl            IS NULL;       
    --
    CURSOR group_dtls_cur
    IS
         SELECT c4010_group_id group_id
           FROM t4010_group
          WHERE c207_set_id        = v_system_id
            AND c901_type          = 40045 -- Forecast/Demand Group
            AND c4010_void_fl     IS NULL
            AND c4010_inactive_fl IS NULL;
BEGIN
    --1 part number mapping
    FOR part_dtls IN part_dtls_cur
    LOOP
        gm_pkg_qa_rfs.gm_sav_part_company_map (part_dtls.pnum, p_company_id, NULL, p_map_type, 70171, p_user_id) ;
        --
        --DBMS_OUTPUT.PUT_LINE(' Part # details '|| part_dtls.pnum);
    END LOOP; --end loop part number cursor
    
    -- 2 System mapping
    FOR system_dtls IN system_dtls_cur
    LOOP
        v_system_id := system_dtls.sys_id;
        -- to map the system to company
        gm_pkg_pd_system_trans.gm_sav_system_company_map (v_system_id, p_company_id, p_map_type, p_user_id) ;
        --DBMS_OUTPUT.PUT_LINE(' System # details '|| v_system_id);
        
        -- 3 Set mapping
        FOR set_dtls IN set_dtls_cur
        LOOP
        	-- to map the set to company
            gm_pkg_pd_system_trans.gm_sav_system_company_map (set_dtls.set_id, p_company_id, p_map_type, p_user_id) ;
            --DBMS_OUTPUT.PUT_LINE(' Set # details '|| set_dtls.set_id);
        END LOOP; -- end loop set cursor
        
        -- 4 Group mapping
        FOR group_dtls IN group_dtls_cur
        LOOP
        	-- to map the group to company
            gm_pkg_pd_group_txn.gm_sav_system_group_tosync (group_dtls.group_id, v_system_id, p_company_id, p_map_type,
            p_user_id) ;
            --DBMS_OUTPUT.PUT_LINE(' Group # details '|| group_dtls.group_id);
        END LOOP; -- end loop group cursor
        --
    END LOOP; -- end loop System cursor
    --
END gm_sav_prj_release_to_company;

END gm_pkg_pd_project_trans;
/
