/* Formatted on 2010/03/10 09:56 (Formatter Plus v4.8.0) */
-- @"C:\database\Packages\ProdDevelopement\gm_pkg_pd_group_info.bdy"

CREATE OR REPLACE PACKAGE BODY gm_pkg_pd_group_info
IS
	/******************************************************************
	* Description : Procedure to fetch the group detail for product catalog.
	****************************************************************/
	PROCEDURE gm_fch_groups_prodcat  (
		p_token  		IN 		t101a_user_token.c101a_token_id%TYPE,
		p_langid		IN		NUMBER,
		p_page_no		IN		NUMBER,
	    p_uuid		    IN      t9150_device_info.c9150_device_uuid%TYPE,
		p_group_cur		OUT		TYPES.CURSOR_TYPE
	)
	AS
		v_deviceid  t9150_device_info.c9150_device_info_id%TYPE;
		v_count	    NUMBER;
		v_page_no   NUMBER;
		v_page_size NUMBER;
	  	v_start     NUMBER;
	    v_end       NUMBER;
	    v_last_rec	t9151_device_sync_dtl.C9151_LAST_SYNC_REC%TYPE; 
	BEGIN
		--get the device info id based on user token
		v_deviceid := get_device_id(p_token, p_uuid);
		
		/*if updated groups available then add all groups into v_inlist and return count of v_inlist records*/
		gm_pkg_pdpc_prodcatrpt.gm_fch_device_updates(v_deviceid,p_langid,103110,p_page_no,v_count,v_last_rec); --103110 - Group Device Sync Type 
		
		--If there is no data available for device then call below prc to add all groups.
		IF v_count = 0 THEN
			gm_pkg_pdpc_prodcatrpt.gm_fch_all_group_tosync;
		END IF;
		
		v_page_no := p_page_no;
		-- get the paging details
		gm_pkg_pdpc_prodcatrpt.gm_fch_paging_dtl('GROUP','PAGESIZE',v_page_no,v_start,v_end,v_page_size);

		OPEN p_group_cur
			FOR		
			 SELECT COUNT (1) OVER () pagesize, result_count totalsize, v_page_no pageno, rwnum, CEIL (result_count / v_page_size) totalpages
			      , t4010.*
			   FROM
			    (
			         SELECT t4010.*, ROWNUM rwnum, COUNT (1) OVER () result_count, MAX(DECODE(t4010.groupid,v_last_rec,ROWNUM,NULL)) OVER() LASTREC
			           FROM
			            (
						 SELECT t4010.c4010_group_id groupid, t4010.c4010_group_nm groupnm, 
						 /* nvl (t2000.c2000_ref_desc, t4010.c4010_group_desc) groupdesc, 
						    nvl (t2000.c2000_ref_dtl_desc, t4010.c4010_group_dtl_desc) groupdtldesc,*/
						    t4010.c4010_group_desc groupdesc,   
						    t4010.c4010_group_dtl_desc groupdtldesc,
						    t4010.c4010_publish_fl
						    grouppublishfl,t4010.c901_group_type grouptypeid, t4010.c901_priced_type grouppricedtypeid, t4010.c207_set_id systemid
						    , NVL(temp.void_fl,t4010.c4010_void_fl) voidfl
						   FROM t4010_group t4010, 
						   /*(
						         SELECT c2000_ref_id, c2000_ref_desc, c2000_ref_dtl_desc
						           FROM t2000_master_metadata
						          WHERE c901_ref_type    = '103094' --Group Master Meta Data Ref Type 
						            AND c901_language_id = p_langid
						            AND c901_ref_type = 103094 -- Group
						    )
						    t2000,*/
						     my_temp_prod_cat_info temp
						  WHERE 
						  /*to_char (t4010.c4010_group_id) = t2000.c2000_ref_id(+)
						    AND */
						    t4010.c4010_group_id = temp.ref_id
						    AND t4010.c901_type = 40045 --Forecast/Demand Group
						    ORDER BY t4010.c4010_group_id 
			             )t4010
			     )t4010
			  WHERE rwnum  BETWEEN NVL(LASTREC+1,DECODE(v_last_rec,NULL,v_start,v_start-1)) AND v_end;
		
	END gm_fch_groups_prodcat;
	/******************************************************************
	* Description : Procedure to fetch the group part number detail for product catalog.
	****************************************************************/	
	PROCEDURE gm_fch_group_parts  (
		p_token  		IN 		t101a_user_token.c101a_token_id%TYPE,
		p_langid		IN		NUMBER,
		p_page_no		IN		NUMBER,
	    p_uuid		    IN      t9150_device_info.c9150_device_uuid%TYPE,
		p_group_cur		OUT		TYPES.CURSOR_TYPE
	)
	AS
		v_deviceid  t9150_device_info.c9150_device_info_id%TYPE;
		v_count	    NUMBER;
		v_page_no   NUMBER;
		v_page_size NUMBER;
	  	v_start     NUMBER;
	    v_end       NUMBER;
	    v_last_rec	t9151_device_sync_dtl.C9151_LAST_SYNC_REC%TYPE; 
	BEGIN
		--get the device info id based on user token
		v_deviceid := get_device_id(p_token, p_uuid);
		
		/*if updated groups available then add all groups into v_inlist and return count of v_inlist records*/
		gm_pkg_pdpc_prodcatrpt.gm_fch_device_updates(v_deviceid,p_langid,4000408,p_page_no,v_count,v_last_rec); --4000408 - Group part Device Sync Type 
		
		--If there is no data available for device then call below prc to add all groups.
		IF v_count = 0 THEN
			gm_pkg_pdpc_prodcatrpt.gm_fch_all_group_tosync;
		END IF;
		
		v_page_no := p_page_no;
		-- get the paging details
		gm_pkg_pdpc_prodcatrpt.gm_fch_paging_dtl('GROUP_PART','PAGESIZE',v_page_no,v_start,v_end,v_page_size);

		OPEN p_group_cur
			FOR		
			 SELECT COUNT (1) OVER () pagesize, result_count totalsize, v_page_no pageno, rwnum, CEIL (result_count / v_page_size) totalpages
			      , t4011.*
			   FROM
			    (
			         SELECT t4011.*, ROWNUM rwnum, COUNT (1) OVER () result_count, MAX(DECODE(t4011.grpdtlid,v_last_rec,ROWNUM,NULL)) OVER() LASTREC
			           FROM
			            (
						 SELECT t4010.c4010_group_id groupid, t4011.c205_part_number_id partnum, t4011.c901_part_pricing_type pricingtypeid
						       ,t4011.c4011_group_detail_id grpdtlid
						   FROM T4010_Group T4010, T4011_Group_Detail T4011, my_temp_prod_cat_info temp
						  WHERE to_char(t4010.c4010_group_id)    = temp.ref_id
						    AND t4010.c4010_group_id    = t4011.c4010_group_id
						    AND t4010.c901_type = 40045 --Forecast/Demand Group
						    AND t4011.c901_part_pricing_type = 52080 --Primary
						    ORDER BY grpdtlid
			             )t4011
			     )t4011
			  WHERE rwnum BETWEEN NVL(LASTREC+1,DECODE(v_last_rec,NULL,v_start,v_start-1)) AND v_end;
		
	END gm_fch_group_parts;
	
	/********************************************************
 	* Description : This function is used to validate the group name.
 	* Parameters  : p_group_name, p_group_id
	********************************************************/
	FUNCTION gm_chk_group_count(
    p_group_name IN t4010_group.c4010_group_nm %TYPE
    )
    RETURN NUMBER
    IS
    v_cnt NUMBER;
    BEGIN
    	SELECT COUNT(1)
    	  INTO v_cnt
    	  FROM t4010_group
    	 WHERE upper(c4010_group_nm) = upper(trim(p_group_name))
    	   AND c4010_void_fl          IS NULL
    	   AND c4010_inactive_fl      IS NULL
    	   AND c901_type = 40045;
  	RETURN v_cnt;
	END gm_chk_group_count;
	
	/********************************************************
 	* Description : This Procedure rerieves the Group Information for the passed in System id.
 	* Author  : Rajeshwaran
	********************************************************/
	
	PROCEDURE gm_fch_groups_by_system (
		p_system_id 	IN 	T207_SET_MASTER.C207_SET_ID%TYPE,
		p_group_id 		IN 	T4010_GROUP.C4010_GROUP_ID%TYPE,
		p_group_cur		OUT	TYPES.CURSOR_TYPE
	)
	AS
	BEGIN
		OPEN p_group_cur
		FOR		
			  SELECT 	C4010_GROUP_ID group_id, C4010_GROUP_NM group_nm, C4010_GROUP_DESC group_desc,
						C4010_GROUP_DTL_DESC group_d_desc, C4010_GROUP_PUBLISH_DATE group_pubdt,C4010_PUBLISH_FL group_pubfl,
						C901_GROUP_TYPE group_type, C901_PRICED_TYPE group_prc_type
				FROM 
						T4010_GROUP where C4010_VOID_FL is null
				 AND  	C4010_INACTIVE_FL      is null
				 AND 	C901_TYPE = 40045 
				 AND 	C207_SET_ID=P_SYSTEM_ID
				 AND 	C4010_GROUP_ID = NVL(P_GROUP_ID,C4010_GROUP_ID)
			ORDER BY 	UPPER(C4010_GROUP_NM);
	END gm_fch_groups_by_system;
	
END gm_pkg_pd_group_info;
/