/* Formatted on 2010/10/04 17:45 (Formatter Plus v4.8.0) */
-- @"C:\database\Packages\ProdDevelopement\gm_pkg_pd_partnumber.pkg"

CREATE OR REPLACE PACKAGE gm_pkg_pd_partnumber
IS
	/*******************************************************
   * Description : Procedure to fetch MultiSet Part report
   * Author 	 : Xun Qu
   *******************************************************/
--
	PROCEDURE gm_pd_fch_multisetpart_report (
		p_report   OUT	 TYPES.cursor_type
	);

	/*******************************************************
		* Description : Procedure to fetch Part Detail
		* Author	   : Xun Qu
		*******************************************************/
	PROCEDURE gm_fch_part_all_detail (
		p_partnumber	 IN 	  t205_part_number.c205_part_number_id%TYPE
	  , p_partdetail	 OUT	  TYPES.cursor_type
	  , p_setdetail 	 OUT	  TYPES.cursor_type
	  , p_vendordetail	 OUT	  TYPES.cursor_type
	  , p_sub_part		 OUT	  TYPES.cursor_type
	  , p_parent_part	 OUT	  TYPES.cursor_type
	  , p_part_grp		 OUT	  TYPES.cursor_type
	);

	/*******************************************************
	* Description : Procedure to fetch Part Set Detail
	* Author		 : Xun Qu
	*******************************************************/
	PROCEDURE gm_fch_part_set_detail (
		p_partnumber   IN		t205_part_number.c205_part_number_id%TYPE
	  , p_setdetail    OUT		TYPES.cursor_type
	);

	/*******************************************************
	 * Description : Procedure to fetch Part Vendor Detail
	 * Author		: Xun Qu
	 *******************************************************/
	PROCEDURE gm_fch_part_vendor_detail (
		p_partnumber	 IN 	  t205_part_number.c205_part_number_id%TYPE
	  , p_vendordetail	 OUT	  TYPES.cursor_type
	);

	/*******************************************************
	 * Description : Procedure to validate "release for sale" in part num during consignment shipout
	 * Author		: Joe P Kumar
	 *******************************************************/
	PROCEDURE gm_val_release_for_sale (
		p_refid   IN   t907_shipping_info.c907_ref_id%TYPE
	  , p_type	  IN   t901_code_lookup.c901_code_id%TYPE
	);

	/*******************************************************
	* Description : Procedure to fetch all sub-parts if the part has sub-parts
	* Author		 : Xun Qu
	*******************************************************/
	PROCEDURE gm_fch_subpart_all_detail (
		p_partnumber   IN		t205_part_number.c205_part_number_id%TYPE
	  , p_sub_part	   OUT		TYPES.cursor_type
	);

	/*******************************************************
	* Description : Procedure to fetch all sub-parts if the part has sub-parts
	* Author		 : Xun Qu
	*******************************************************/
	PROCEDURE gm_fch_parentpart_all_detail (
		p_partnumber	IN		 t205_part_number.c205_part_number_id%TYPE
	  , p_parent_part	OUT 	 TYPES.cursor_type
	);

	/*******************************************************
	* Description : Procedure to fetch all part mapping
	* Author		 : Xun Qu
	*******************************************************/
	PROCEDURE gm_fch_part_mapping (
		p_partnumber   IN		t205_part_number.c205_part_number_id%TYPE
	  , p_project_id   IN		t205_part_number.c202_project_id%TYPE
	  , p_orderpart    IN		VARCHAR2
	  , p_parts 	   OUT		TYPES.cursor_type
	);

	/*******************************************************
	* Description : Procedure to fetch part Details
	* Author		 : Rajeshwaran Varatharajan
	*******************************************************/
	PROCEDURE gm_fch_part_detail (
		p_partnumber		IN		 VARCHAR2
	  , p_subcomponentflg	IN		 t205_part_number.c205_sub_component_fl%TYPE
	  , p_partdetail		OUT 	 TYPES.cursor_type
	);

	/*******************************************************
	* Description : Procedure to fetch Part Number Detail
	* Author	   : Rajeshwaran Varatharajan
	*******************************************************/
	PROCEDURE gm_sav_multiprojectpart (
		p_partnumber   IN	VARCHAR2
	  , p_user_id	   IN	t205_part_number.c205_last_updated_by%TYPE
	);

	/*******************************************************
	* Description : Procedure to fetch all groups report
	* Author		: Xun Qu
	*******************************************************/
	PROCEDURE gm_pd_fch_groups_report (
		p_report   OUT	 TYPES.cursor_type
	);

	/*******************************************************
	  * Description : Procedure to fetch all pathway details
	  * Author		  : Xun Qu
	  *******************************************************/
	PROCEDURE gm_fch_part_pathway_dtls (
		p_num_id	IN		 t205_part_number.c205_part_number_id%TYPE
	  , p_details	OUT 	 TYPES.cursor_type
	);

	/*******************************************************
	  * Description : Procedure to fetch all Regulatory details
	  * Author		  : Xun Qu
	  *******************************************************/
	PROCEDURE gm_fch_part_regulatory_dtls (
		p_num_id	IN		 t205_part_number.c205_part_number_id%TYPE
	  , p_details	OUT 	 TYPES.cursor_type
	);

	/************************************************************************
		* Description : Procedure to save pathway and RFS to T205D_PART_ATTRIBUTE
		* Author	  : Xun
		*******************************************************************/
	PROCEDURE gm_sav_part_qa_attribute (
		p_num_id	IN	 t205_part_number.c205_part_number_id%TYPE
	  , p_pathway	IN	 VARCHAR2	--80070^80081|80071^80120
	  , p_rfs		IN	 VARCHAR2	--80120, 80121, 80122,
	  , p_user_id	IN	 t205_part_number.c205_last_updated_by%TYPE
	  , p_taggable_FL IN t205d_part_attribute.c205d_attribute_value%TYPE
    , p_prodfly		IN		 t205_part_number.c205_product_family%TYPE
	);

	/************************************************************************
		* Description : Procedure to save attribute value to T205D_PART_ATTRIBUTE
		* Author	  : Xun
		*******************************************************************/
	PROCEDURE gm_sav_part_attribute (
		p_num_id	   IN		t205_part_number.c205_part_number_id%TYPE
	  , p_attr_type    IN		t205d_part_attribute.c901_attribute_type%TYPE
	  , p_attr_value   IN		t205d_part_attribute.c205d_attribute_value%TYPE
	  , p_user_id	   IN		t205_part_number.c205_last_updated_by%TYPE
	  , p_attr_id	   OUT		t205d_part_attribute.c2051_part_attribute_id%TYPE
	);

	/********************************************************************
		  * Description : Function to get all release for sale country id for
	  the part and separate by ','
		  ******************************************************************/
	FUNCTION get_rfs_info (
		p_num_id   IN	t205_part_number.c205_part_number_id%TYPE
	)
		RETURN VARCHAR2;

	/********************************************************************
		   * Description : Function to get all release for sale country name for
	   the part and separate by ','
		   ******************************************************************/
	FUNCTION get_rfs_name (
		p_num_id   IN	t205_part_number.c205_part_number_id%TYPE
	)
		RETURN VARCHAR2;

	/************************************************************
	* Description 	: Procedure to save all Part leadtime details
	* Author		: Jignesh Shah
	*************************************************************/
	
	PROCEDURE gm_sav_part_leadtime (
		p_attr_type		IN	 t205d_part_attribute.c901_attribute_type%TYPE
	  , p_input_str		IN	 VARCHAR2
	  , p_user_id		IN	 t205_part_number.c205_last_updated_by%TYPE
	);

	/************************************************************
	* Description 	: Procedure to fetch all Part leadtime details
	* Author		: Jignesh Shah
	*************************************************************/
	
	PROCEDURE gm_fch_part_leadtime (
		p_part_number   IN	CLOB
	  , p_proj_id		IN	t205_part_number.c202_project_id%TYPE
	  , p_out_cur	    OUT TYPES.cursor_type
	);
	
	PROCEDURE gm_validate_part_number (
		p_part_number IN t205_part_number.c205_part_number_id%TYPE,
		p_out_msg OUT VARCHAR2
	);
	
	/******************************************************************
	* Description : Procedure used to fetch all Consignment Parts Dtls
	* Author  :
	*******************************************************************/
	PROCEDURE gm_fch_trans_parts (
        p_consign_id IN t504_consignment.c504_consignment_id%TYPE,
        p_out_parts OUT CLOB);
	/******************************************************************
	* Description : Procedure to fetch the part number detail for product catalog.
	****************************************************************/
	PROCEDURE gm_fch_partnumber_prodcat (
		p_token  		IN 		t101a_user_token.c101a_token_id%TYPE,
		p_langid		IN		NUMBER,
		p_page_no		IN		NUMBER,
	    p_uuid		    IN      t9150_device_info.c9150_device_uuid%TYPE,
		p_parts_cur		OUT		TYPES.CURSOR_TYPE
	);
	/******************************************************************
	* Description : Procedure to fetch the part number by attribute type and attribute value.
	****************************************************************/
	PROCEDURE gm_fch_part_list_by_attribute (
	        p_attr_type  IN  T205D_PART_ATTRIBUTE.C901_ATTRIBUTE_TYPE%TYPE,
	        p_attr_value IN  T205D_PART_ATTRIBUTE.C205D_ATTRIBUTE_VALUE%TYPE,
	        p_out 		 OUT TYPES.CURSOR_TYPE
	);	
	
	/******************************************************************
	* Description : Procedure used to save label attr values
	* Author  : Bala
	*******************************************************************/
	PROCEDURE gm_sav_part_params (
	  p_num_id	   IN	t205_part_number.c205_part_number_id%TYPE,
      p_lbl_attr    IN	VARCHAR2,    
      p_user_id	   IN	t205_part_number.c205_last_updated_by%TYPE    
    );
    
    /******************************************************************
    * Description : Procedure to fetch the Part dhr details
    * Author      : mmuthusamy
	*******************************************************************/
    PROCEDURE gm_fch_part_dhr_dtls (
      p_part_number_id		IN   t205_part_number.c205_part_number_id%TYPE,
      p_out_dhr_dtls        OUT   TYPES.cursor_type
    );
    
    /******************************************************************
    * Description : Procedure to save the Part dhr details
    * Author      : mmuthusamy
	*******************************************************************/
    PROCEDURE  gm_sav_part_dhr_req (
	  p_num_id	   IN	t205_part_number.c205_part_number_id%TYPE,
      p_matspec		IN		 t205_part_number.c205_material_cert_fl%TYPE,
      p_hardtestfl	IN		 t205_part_number.c205_hard_test_fl%TYPE,
      p_primpack		IN		 t205_part_number.c205_packaging_prim%TYPE,
  	  p_secpack		IN		 t205_part_number.c205_packaging_sec%TYPE,
  	  p_compcert		IN		 t205_part_number.c205_compliance_cert_fl%TYPE,
  	  p_part_att_str   IN   VARCHAR2,
      p_user_id	   IN	t205_part_number.c205_last_updated_by%TYPE    
    );  
 	 /********************************************************
	 * Description : This function is used to get Part size.
 	* Parameters  : p_partnumber, p_controlnum
	********************************************************/
    Function get_part_size(
    p_partnumber	     t205_part_number.C205_PART_NUMBER_ID%TYPE ,
    p_controlnum t502_item_order.C502_Control_Number %TYPE
    )RETURN VARCHAR2;
    /********************************************************
	* Description : This function is used to get part DHR configure id.
 	* Parameters  : p_partnumber
	********************************************************/
    Function get_dhr_issue_agn_conf_id(
    p_partnumber	     t205_part_number.C205_PART_NUMBER_ID%TYPE
    )RETURN VARCHAR2;
	/********************************************************
	* Description : This function is used to get sample pattern for DHR tab.
	* Parameters  : p_partnumber
	********************************************************/
	FUNCTION get_dhr_tab_sam_pattern (
        p_partnumber t205_part_number.C205_PART_NUMBER_ID%TYPE)
    RETURN VARCHAR2;
 
    /******************************************************************
    * Description : Procedure to save the Part dhr details
    * Author      : ckumar
	*******************************************************************/
   PROCEDURE gm_fch_partattr_prodcat (
		p_token_id     IN  t101a_user_token.c101a_token_id%TYPE
	  , p_language_id  IN  NUMBER
	  , p_page_no      IN  NUMBER
	  , p_uuid		   IN  t9150_device_info.c9150_device_uuid%TYPE
	  , p_rfs_id       IN  t205d_part_attribute.c205d_attribute_value%TYPE
	  ,	p_part_attrib_cur OUT TYPES.cursor_type
	);
	/************************************************************************
		* Description : Procedure to save attribute value to T205D_PART_ATTRIBUTE
		* Author	  : 
	*******************************************************************/
	PROCEDURE gm_sav_part_attribute_by_comp (
		p_num_id	   IN		t205_part_number.c205_part_number_id%TYPE
	  , p_attr_type    IN		t205d_part_attribute.c901_attribute_type%TYPE
	  , p_attr_value   IN		t205d_part_attribute.c205d_attribute_value%TYPE
	  , p_user_id	   IN		t205_part_number.c205_last_updated_by%TYPE
	  , p_attr_id	   OUT		t205d_part_attribute.c2051_part_attribute_id%TYPE
	);
	/************************************************************************
		  * Description : Procedure to save attribute value to T205D_PART_ATTRIBUTE
    *******************************************************************/
	PROCEDURE gm_sav_part_attribute_by_comp(
        p_num_id     IN t205_part_number.c205_part_number_id%TYPE,
        p_attr_type  IN t205d_part_attribute.c901_attribute_type%TYPE,
        p_attr_value IN t205d_part_attribute.c205d_attribute_value%TYPE,
        p_user_id    IN t205d_part_attribute.c205d_last_updated_by%TYPE,
        p_comp_id    IN t205d_part_attribute.c1900_company_id%TYPE,
        p_attr_id OUT t205d_part_attribute.c2051_part_attribute_id%TYPE);	
	/************************************************************************
		* Description : Procedure to save attribute value to T205D_PART_ATTRIBUTE in bulk
		* Author	  : 
	*******************************************************************/
	PROCEDURE gm_sav_part_attribute_bulk (
        p_attr_type  IN t205d_part_attribute.c901_attribute_type%TYPE,
        p_attr_str   IN CLOB,
        p_all_cmp_fl IN VARCHAR2,
        p_user_id    IN t205d_part_attribute.c205d_last_updated_by%TYPE,
        p_validParts   OUT CLOB,
        p_invalidParts OUT CLOB);
     /************************************************************************
	  * Description : Procedure to fetch attribute value to T205D_PART_ATTRIBUTE
	  *******************************************************************/
	 PROCEDURE gm_fch_part_attribute(
        p_attr_type  IN t205d_part_attribute.c901_attribute_type%TYPE,
        p_pnums      IN CLOB,
        p_system_id  IN t207_set_master.c207_set_id%TYPE,
        p_all_cmp_fl IN VARCHAR2,
        p_partfl     IN VARCHAR2,
        p_exld_type  IN VARCHAR2,
        p_out   OUT	   TYPES.cursor_type);
        /************************************************************************
	  * Description : Procedure to delete invalid part attribute in T205D_PART_ATTRIBUTE
	  *******************************************************************/
	 PROCEDURE gm_sav_void_part_attribute(
         p_txn_str  IN VARCHAR2,
         p_user_id  IN t205d_part_attribute.c205d_last_updated_by%TYPE);
         
      /*********************************************************************************************************************
		* Description 	: Procedure to check Whether the part number is used in any of the transaction or not
		* 					If it is used, then the lot tracking needed and serial number needed check box should be disabled
		* Author		: Arajan
		************************************************************************************************************************/
		
		PROCEDURE gm_fch_checkbox_status (
			p_part_number 	IN 		t205_part_number.c205_part_number_id%TYPE,
			p_serial_chk_fl 		OUT 	VARCHAR2,
			p_lottrack_chk_fl 	OUT 	VARCHAR2
		);
         
    /*************************************************************************************************************
	* Description : Procedure to fetch the part rfs company mapping details from T901C_RFS_COMPANY_MAPPING table
	* Author	  : ksomanathan				
	***************************************************************************************************************/
	PROCEDURE gm_fch_partrfs_mapping (
		p_token_id     IN  t101a_user_token.c101a_token_id%TYPE
	  , p_language_id  IN  NUMBER
	  , p_page_no      IN  NUMBER
	  , p_uuid		   IN  t9150_device_info.c9150_device_uuid%TYPE
	  , p_part_rfs_cur  OUT		TYPES.CURSOR_TYPE
	);
	/****************************************************************
    * Description : Procedure to validate the Part number 
    * Author      : Prabhu Vigneshwaran
    *****************************************************************/
	PROCEDURE gm_validate_mfg_parts (
        p_partinputstr                     IN  CLOB,
        p_out_invalid_part                 OUT CLOB,
        p_out_invalid_part_cmp OUT CLOB
     );
    END gm_pkg_pd_partnumber;
/
