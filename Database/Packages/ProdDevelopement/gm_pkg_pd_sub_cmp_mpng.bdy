--@"C:\Database\Packages\Sales\gm_pkg_pd_sub_cmp_mpng.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_pd_sub_cmp_mpng
IS
--
	PROCEDURE gm_sav_part_mpng_dtls (
	    P_input_string  IN  CLOB,
	    p_user_id  		IN  t2020_sub_component_mapping.C2020_CREATED_BY%TYPE,
	    p_access_type	IN  VARCHAR2,
	    p_sub_cmp_id 	OUT CLOB,
	    p_part_num_id 	OUT CLOB
	)
	AS
		v_part_num      	t2020_sub_component_mapping.c205_part_number_id%TYPE;
    	v_sub_comp_num 		t2020_sub_component_mapping.C2020_SUB_COMPONENT_NUM%TYPE ;
		v_sub_comp_desc		t2020_sub_component_mapping.C2020_SUB_COMPONENT_DESC%TYPE;
		v_lppr_code     	t2020_sub_component_mapping.C2020_SUB_COMPONENT_LPPR%TYPE;
		v_part_price		t2020_sub_component_mapping.C2020_SUB_COMPONENT_PRICE%TYPE;
		v_tva_percent   	t2020_sub_component_mapping.C2020_SUB_COMPONENT_TVA%TYPE;
		v_strlen    		NUMBER      := NVL (LENGTH (P_input_string), 0) ;
	    v_string    		CLOB 		:= P_input_string;
	    v_substring 		CLOB ;
	    v_sub_cmp_id        NUMBER;
	    v_temp_sub_cmp_id   CLOB;
	    v_temp_part_ids     CLOB;
	    v_company_id        T1900_COMPANY.c1900_company_id%TYPE;
	    v_part_cnt		    NUMBER;
	    v_sub_part_cnt      NUMBER;
	    v_tmp_err_parts     CLOB;
	    v_tmp_err_sub_parts CLOB;
	    v_corct_part_fl     VARCHAR2(2);
	    v_corct_Sub_part_fl VARCHAR2(2);
	    v_rec_save_fl 		VARCHAR2(2) := 'N';
	    v_dupli_sub_cmop    t2020_sub_component_mapping.C2020_SUB_COMPONENT_NUM%TYPE ;
	    v_tmp_dup_sub_parts CLOB;
	    v_sub_cmop_cnt      NUMBER;
	    v_dupli_sub_cmop_cnt NUMBER;
	    v_insert_fl     	VARCHAR2(2) := 'N';
	    v_upd_fl     		VARCHAR2(2) := 'N';
		
	BEGIN		
		SELECT NVL(get_compid_frm_cntx(), get_rule_value('DEFAULT_COMPANY','ONEPORTAL')) 
          INTO v_company_id
          FROM dual;
		
		IF v_strlen > 0 THEN
	        WHILE INSTR (v_string, '|') <> 0
	        LOOP
	        	v_substring  	:= SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
    			v_string     	:= SUBSTR (v_string, INSTR (v_string, '|')    + 1);
	            v_part_num      := NULL;
    			v_sub_comp_num 	:= NULL;
			    v_sub_comp_desc	:= NULL;
			    v_lppr_code     := NULL;
			    v_part_price	:= NULL;
			    v_tva_percent   := NULL;
			    v_sub_cmp_id    := NULL;
			    v_corct_part_fl := 'Y';
			    v_corct_Sub_part_fl := 'Y';
			    v_insert_fl     	:= 'N';
	    		v_upd_fl     		:= 'N';
			    
			    v_part_num      := SUBSTR (v_substring, 1, INSTR (v_substring, '^')      - 1);
			    v_substring  	:= SUBSTR (v_substring, INSTR (v_substring, '^')         + 1);
			    v_sub_comp_num  := SUBSTR (v_substring, 1, INSTR (v_substring, '^')      - 1);
			    v_substring  	:= SUBSTR (v_substring, INSTR (v_substring, '^')         + 1);
			    v_sub_comp_desc	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^')      - 1);
			    v_substring  	:= SUBSTR (v_substring, INSTR (v_substring, '^')         + 1);    
				v_lppr_code	    := SUBSTR (v_substring, 1, INSTR (v_substring, '^')      - 1);
			    v_substring  	:= SUBSTR (v_substring, INSTR (v_substring, '^')         + 1);
				v_part_price	:= TO_NUMBER(SUBSTR (v_substring, 1, INSTR (v_substring, '^')      - 1));
			    v_substring  	:= SUBSTR (v_substring, INSTR (v_substring, '^')         + 1);
			    v_tva_percent	:= TO_NUMBER(SUBSTR (v_substring, 1, INSTR (v_substring, '^')      - 1));
			    v_substring  	:= SUBSTR (v_substring, INSTR (v_substring, '^')         + 1);
			    v_sub_cmp_id	:= TO_NUMBER(v_substring);
			    
			    SELECT utl_i18n.raw_to_char(utl_raw.cast_to_raw(v_sub_comp_desc), 'utf8') INTO v_sub_comp_desc FROM DUAL;			  
			    
			    -- Any value which is available for Part Number Column, Need to validate that enterd one is valida part or not.
			    -- If it is invalid, then collect all the In Valid Parts and show those at the bottom of the JSP page. 
			    SELECT COUNT(1) 
			      INTO v_part_cnt
			      FROM t205_part_number 
			     WHERE UPPER(c205_part_number_id) = UPPER(v_part_num);
			     
			     IF v_part_cnt <= 0 THEN
			     	v_tmp_err_parts := v_tmp_err_parts || ',' || ' <BR> ['||v_sub_comp_num || ' - ' || v_part_num || ']';
			     	v_corct_part_fl  := 'N';
			     END IF;	
			     
			    -- If any Parent Part NUmber is going to Mapping as Sub Component Number , need to stop that for part for not saving.
			    -- If we found those kind of parts, then collect all the In Valid Parts and show those at the bottom of the JSP page. 
			    /*SELECT COUNT(1) 
			      INTO v_sub_part_cnt
			      FROM t205_part_number 
			     WHERE UPPER(c205_part_number_id) = UPPER(v_sub_comp_num);
			     
			     IF v_sub_part_cnt > 0 THEN
			     	v_tmp_err_sub_parts := v_tmp_err_sub_parts || ',' || ' <BR> ['||v_sub_comp_num || ' - ' || v_part_num || ']';
			     	v_corct_Sub_part_fl  := 'N';
			     END IF;*/
			     
			    -- The following logic is written for Handling the Not Mapping the same Sub Component Number to the Differenr Parent Part Number
			     SELECT COUNT(1) INTO v_sub_cmop_cnt 
			       FROM t2020_sub_component_mapping 
                  WHERE c2020_sub_component_num = v_sub_comp_num 
                    AND C2020_VOID_FL IS NULL;
               
                 SELECT COUNT(1)  INTO v_dupli_sub_cmop_cnt 
			       FROM t2020_sub_component_mapping 
                  WHERE c2020_sub_component_num = v_sub_comp_num 
                    AND c205_part_number_id = v_part_num
                    AND C2020_VOID_FL IS NULL;  
               
                    
                  IF v_sub_cmop_cnt = 0 THEN  -- Cheking weather Sub Component Number is available Or NOT
                  	IF v_dupli_sub_cmop_cnt = 0 THEN -- Cheking weather Sub Component Number is available Or NOT for the Mentioned Parent Part NUmber or NOT
                  		v_insert_fl := 'Y';  -- Need to Insert New Record for that combination                  	
                  	END IF;
                  	IF v_dupli_sub_cmop_cnt = 1 THEN -- Cheking weather Sub Component Number is available Or NOT for the Mentioned Parent Part NUmber or NOT
                  		v_upd_fl := 'Y';    --  Need to UPDATE existing data for that combination            	
                  	END IF;
                  ELSE 
                  	IF v_dupli_sub_cmop_cnt = 0 THEN  -- Sub Component Number is available and Mapped to another Parent Part Number
                  		v_tmp_dup_sub_parts := v_tmp_dup_sub_parts || ',' || '<BR>' || v_sub_comp_num ;   -- Preparing the Input String for Duplicate Entries.
                  	END IF;
                  	IF v_dupli_sub_cmop_cnt = 1 THEN -- Sub Component Number is available and Mapped to same Parent Part Number
                  		v_upd_fl := 'Y';                  	
                  	END IF;
                  END IF;                                          
                    
			     IF v_corct_part_fl = 'Y' AND v_corct_Sub_part_fl = 'Y' AND ( v_upd_fl  = 'Y'  OR v_insert_fl = 'Y') THEN
			     		
			     IF p_access_type = 'PRICING' THEN
			     	 UPDATE t2020_sub_component_mapping
		                   SET C2020_SUB_COMPONENT_NUM 	= v_sub_comp_num
		                     , C2020_SUB_COMPONENT_LPPR = v_lppr_code
		                     , C2020_SUB_COMPONENT_PRICE = v_part_price
		                     , C2020_SUB_COMPONENT_TVA = v_tva_percent
		                     , C2020_LAST_UPDATED_BY = p_user_id
		                     , C2020_LAST_UPDATED_DATE = CURRENT_DATE
		                     , c1900_company_id      =  v_company_id
		                 WHERE c205_part_number_id = v_part_num
		                   AND C2020_SUB_COMPONENT_NUM = v_sub_comp_num
		                   AND C2020_VOID_FL IS NULL;
			     
			     ELSIF p_access_type = 'QUALITY' THEN
			     	 UPDATE t2020_sub_component_mapping
		                   SET C2020_SUB_COMPONENT_NUM 	= v_sub_comp_num
		                     , C2020_SUB_COMPONENT_DESC = v_sub_comp_desc
		                     , C2020_LAST_UPDATED_BY = p_user_id
		                     , C2020_LAST_UPDATED_DATE = CURRENT_DATE
		                     , c1900_company_id      =  v_company_id
		                 WHERE c205_part_number_id = v_part_num
		                   AND C2020_SUB_COMPONENT_NUM = v_sub_comp_num
		                   AND C2020_VOID_FL IS NULL;
			     ELSE
			     	 UPDATE t2020_sub_component_mapping
		                   SET C2020_SUB_COMPONENT_NUM 	= v_sub_comp_num
		                     , C2020_SUB_COMPONENT_DESC = v_sub_comp_desc
		                     , C2020_SUB_COMPONENT_LPPR = v_lppr_code
		                     , C2020_SUB_COMPONENT_PRICE = v_part_price
		                     , C2020_SUB_COMPONENT_TVA = v_tva_percent
		                     , C2020_LAST_UPDATED_BY = p_user_id
		                     , C2020_LAST_UPDATED_DATE = CURRENT_DATE
		                     , c1900_company_id      =  v_company_id
		                 WHERE c205_part_number_id = v_part_num
		                   AND C2020_SUB_COMPONENT_NUM = v_sub_comp_num
		                   AND C2020_VOID_FL IS NULL;
			     END IF;		    
                   
			               IF (SQL%ROWCOUNT = 0) THEN
						         SELECT s2020_sub_component_mapping.NEXTVAL INTO v_sub_cmp_id FROM DUAL;
						         INSERT INTO  t2020_sub_component_mapping(
						                      C2020_SUB_COMPONENT_ID,  C2020_SUB_COMPONENT_NUM , C2020_SUB_COMPONENT_DESC,C2020_SUB_COMPONENT_LPPR
			                                , C2020_SUB_COMPONENT_PRICE , C2020_SUB_COMPONENT_TVA , C2020_CREATED_BY , C2020_CREATED_DATE ,c205_part_number_id , c1900_company_id)
						              VALUES( v_sub_cmp_id, v_sub_comp_num, v_sub_comp_desc, v_lppr_code, v_part_price, v_tva_percent, p_user_id ,CURRENT_DATE , v_part_num , v_company_id) ;
						   END IF;
						   
						   v_rec_save_fl := 'Y';
			     END IF;			     
    			 v_temp_sub_cmp_id := v_temp_sub_cmp_id || ',' || v_sub_comp_num;
    			 v_temp_part_ids := v_temp_part_ids || ',' || v_part_num;
    			COMMIT;
  			END LOOP;
		END IF;	
		
		p_sub_cmp_id := v_temp_sub_cmp_id || '@' || v_rec_save_fl ||'@' || v_tmp_err_parts || '@' || v_tmp_err_sub_parts || '@' || v_tmp_dup_sub_parts;
		p_part_num_id := v_temp_part_ids;
		
	END gm_sav_part_mpng_dtls;
		
	PROCEDURE gm_fch_part_mpng_dtls (
	    p_part_number   	IN 	t2020_sub_component_mapping.C205_PART_NUMBER_ID%TYPE,
	    p_Sub_component   	IN 	t2020_sub_component_mapping.C2020_SUB_COMPONENT_NUM%TYPE,
	    p_Sub_component_id 	IN 	CLOB,
	    p_part_number_id 	IN 	CLOB,
	    p_sub_cmp_map_cur 	OUT TYPES.cursor_type
	)
	AS
		v_query	 		CLOB;
		v_condition 	CLOB;
		v_company_id    t1900_company.c1900_company_id%TYPE;
	BEGIN
		SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL'))
		  INTO v_company_id 
          FROM DUAL;
		
		IF p_Sub_component_id IS NOT NULL AND p_part_number_id IS NOT NULL THEN
			my_context.set_double_clob_inlist_ctx (p_Sub_component_id || ',',p_part_number_id || ',');
		    v_condition := ' AND c2020_sub_component_num IN (SELECT TOKEN FROM v_clob_double_list) AND c205_part_number_id IN (SELECT TOKENII FROM v_clob_double_list)';		 
	    ELSIF p_part_number IS NOT NULL AND p_Sub_component IS NULL THEN
	    	v_condition := ' AND (UPPER(c205_part_number_id) LIKE UPPER('''|| p_part_number ||''') ) ';
	    ELSIF p_part_number IS NULL AND p_Sub_component IS NOT NULL THEN
	    	v_condition := '  AND (UPPER(C2020_SUB_COMPONENT_NUM) LIKE UPPER('''|| p_Sub_component ||'''))  ';
	    ELSIF p_part_number IS NOT NULL AND p_Sub_component IS NOT NULL THEN
	    	v_condition := ' AND (UPPER(c205_part_number_id) LIKE UPPER('''|| p_part_number ||''') ) AND (UPPER(C2020_SUB_COMPONENT_NUM) LIKE UPPER('''|| p_Sub_component ||'''))  ';
	    END IF; 

	    v_query := ' SELECT c205_part_number_id parnum
                 , C2020_SUB_COMPONENT_NUM subpartnum
                 , C2020_SUB_COMPONENT_DESC partdesc
                 , C2020_SUB_COMPONENT_LPPR lpprcode
                 , C2020_SUB_COMPONENT_PRICE partPrice
                 , C2020_SUB_COMPONENT_TVA tvpvalue
                 , get_user_name(C2020_CREATED_BY) mappedby
                 , C2020_CREATED_DATE mappeddt
                 , C2020_SUB_COMPONENT_ID subcmpid
              FROM t2020_sub_component_mapping
             WHERE C2020_VOID_FL IS NULL ';
		            
        OPEN p_sub_cmp_map_cur FOR v_query || v_condition || ' AND c1900_company_id = '''|| v_company_id ||''' ORDER BY c2020_created_date';
               
	END gm_fch_part_mpng_dtls;
	
	PROCEDURE gm_sav_part_mpng_txn_dtls (
	    p_order_id      IN  t2020a_sub_cmap_trans_details.c501_order_id%TYPE,
	    p_part_number   IN  t2020a_sub_cmap_trans_details.C205_PART_NUMBER_ID%TYPE,
	    p_user_id  		IN  t2020a_sub_cmap_trans_details.c2020a_created_by%TYPE    
	)AS
		CURSOR sub_mapping_details
		    IS
			 SELECT c205_part_number_id partNum, C2020_SUB_COMPONENT_NUM subCompNum, C2020_SUB_COMPONENT_DESC subCompDesc, C2020_SUB_COMPONENT_LPPR lpprCode , C2020_SUB_COMPONENT_PRICE partPrice
                  , C2020_SUB_COMPONENT_TVA TVAPercent, C2020_CREATED_BY createdBy,C2020_CREATED_DATE createdDt         
              FROM t2020_sub_component_mapping
             WHERE C2020_VOID_FL IS NULL
               AND (UPPER(c205_part_number_id) = UPPER(p_part_number)) ;
		
	BEGIN
		
		FOR map_txn_dtls IN sub_mapping_details LOOP
		
			 	  UPDATE t2020a_sub_cmap_trans_details
                     SET c2020a_sub_component_num  = map_txn_dtls.subCompNum
                       , c2020a_sub_component_desc = map_txn_dtls.subCompDesc
                       , c2020a_sub_component_lppr = map_txn_dtls.lpprCode
                       , c2020a_sub_component_price = map_txn_dtls.partPrice
                       , c2020a_sub_component_tva = map_txn_dtls.TVAPercent
                       , C2020A_LAST_UPDATED_BY = p_user_id
                       , C2020A_LAST_UPDATED_DATE = CURRENT_DATE
                       , c205_part_number_id = map_txn_dtls.partNum
                       , c501_order_id = p_order_id
                   WHERE c205_part_number_id = map_txn_dtls.partNum
                     AND c501_order_id = p_order_id
                     AND c2020a_sub_component_num = map_txn_dtls.subCompNum
                     AND c2020a_void_fl IS NULL;
                     
				
				 IF (SQL%ROWCOUNT = 0) THEN
				 INSERT INTO t2020a_sub_cmap_trans_details
				            (c2020a_sub_component_id, c2020a_sub_component_num , c2020a_sub_component_desc , c2020a_sub_component_lppr 
                           , c2020a_sub_component_price , c2020a_sub_component_tva ,c2020a_created_by , c2020a_created_date , c205_part_number_id , c501_order_id )
                      VALUES(s2020a_sub_cmap_trans_details.nextval,map_txn_dtls.subCompNum,map_txn_dtls.subCompDesc,map_txn_dtls.lpprCode
                           , map_txn_dtls.partPrice ,map_txn_dtls.TVAPercent, map_txn_dtls.createdBy, map_txn_dtls.createdDt, map_txn_dtls.partNum , p_order_id);		
				
				END IF;					
		END LOOP;
	END gm_sav_part_mpng_txn_dtls;
	
/*********************************************************************************
 * Description : This procedure is used to validate france invoice subcomponent price with item price.
 * Author 	   : 
 * Parameters  : customer PO.
 **********************************************************************************/
PROCEDURE gm_validate_subpart_price (
		p_custpo	   IN		t503_invoice.c503_customer_po%TYPE
		, p_accid 	   IN		t503_invoice.c704_account_id%TYPE  
  		, p_userid	   IN		t503_invoice.c503_created_by%TYPE
	)AS
	v_company_id        t1900_company.c1900_company_id%TYPE;
	v_part_numbers varchar2(4000);
	v_flag varchar(1);
	BEGIN		
		
		SELECT get_compid_frm_cntx() INTO v_company_id  FROM dual;
		SELECT get_rule_value_by_company(v_company_id,'SUB_PART_VALID',v_company_id) INTO v_flag FROM DUAL;
		
		IF v_flag = 'Y' THEN
		--validating subcomponent price with item price for each part. if it is mismatched then throwing error.
			BEGIN
			SELECT RTRIM(XMLAGG(XMLELEMENT(e,t502.c205_part_number_id
				  || ',')).EXTRACT('//text()').getclobval(),',') INTO v_part_numbers
				FROM
				  (SELECT t502.c205_part_number_id,
				    SUM(t502.c502_item_price)c502_item_price
				  FROM t502_item_order t502,
				    t501_order t501
				  WHERE t502.c502_void_fl  IS NULL
				  AND t501.c501_void_fl    IS NULL
				  AND t502.c501_order_id    = t501.c501_order_id
				  AND t501.c501_order_id    IN (
				  SELECT c501_order_id 
						       FROM t501_order t501
						      WHERE  t501.c704_account_id =  p_accid
						        AND t501.c501_customer_po = p_custpo
						        AND t501.c501_delete_fl      IS NULL
						        AND t501.c501_void_fl        IS NULL
						        AND T501.C503_INVOICE_ID IS NULL
				            AND t501.c1900_company_id = v_company_id
						        AND NVL (c901_order_type, -9999) NOT IN (
				               SELECT t906.c906_rule_value
				                  FROM t906_rules t906
				                 WHERE t906.c906_rule_grp_id = 'ORDERTYPE'
				                   AND c906_rule_id = 'EXCLUDE'))  
				  GROUP BY t502.c205_part_number_id
				  ) t502 ,
				  (SELECT t2020a.c205_part_number_id,
				    SUM(t2020a.c2020a_sub_component_price)c2020a_item_price
				  FROM t2020a_sub_cmap_trans_details t2020a,
				    t502_item_order t502,
				    t501_order t501
				  WHERE t2020a.c2020a_void_fl IS NULL
				  AND t502.c502_void_fl       IS NULL
				  AND t501.c501_void_fl       IS NULL
				  AND t502.c205_part_number_id = t2020a.c205_part_number_id
				  AND t2020a.c501_order_id     = t502.c501_order_id
				  AND t502.c501_order_id       = t501.c501_order_id
				  AND t2020a.c501_order_id     in (
				  SELECT c501_order_id 
						       FROM t501_order t501
						      WHERE  t501.c704_account_id =  p_accid
						        AND t501.c501_customer_po = p_custpo
						        AND t501.c501_delete_fl      IS NULL
						        AND t501.c501_void_fl        IS NULL
						        AND T501.C503_INVOICE_ID IS NULL
				            AND t501.c1900_company_id    = v_company_id
						        AND NVL (c901_order_type, -9999) NOT IN (
				               SELECT t906.c906_rule_value
				                  FROM t906_rules t906
				                 WHERE t906.c906_rule_grp_id = 'ORDERTYPE'
				                   AND c906_rule_id = 'EXCLUDE'))  
				  GROUP BY t2020a.c205_part_number_id
				  ) t2020a
				WHERE t2020a.c205_part_number_id = t502.c205_part_number_id
				AND t502.c502_item_price        <> t2020a.c2020a_item_price;
			EXCEPTION
	        WHEN NO_DATA_FOUND THEN
	        	v_part_numbers := NULL;
	        END;
        END IF;
        
        IF v_part_numbers IS NOT NULL THEN
           GM_RAISE_APPLICATION_ERROR('-20999','389',v_part_numbers);
                     
        END IF;
		 
END gm_validate_subpart_price;
	
END gm_pkg_pd_sub_cmp_mpng;
/
