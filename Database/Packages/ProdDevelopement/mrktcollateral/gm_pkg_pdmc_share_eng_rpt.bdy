--@"C:\Database\Packages\ProdDevelopement\mrktcollateral\gm_pkg_pdmc_share_eng_rpt.bdy";
CREATE OR REPLACE
PACKAGE BODY gm_pkg_pdmc_share_eng_rpt
IS
    /*******************************************************
    * Description : Procedure to fetch detail of each share(email)
    * Author    : Mihir Patel
    *******************************************************/
PROCEDURE gm_fch_shared_dtls (
        p_share_id     IN T1016_share.c1016_share_id%TYPE,
        p_out_sh_dtls OUT TYPES.cursor_type)
AS
begin
    OPEN p_out_sh_dtls 
    FOR 
          SELECT t1016.c1016_share_id shareid, t1016.c1016_ref_id partytype, t1016.c1016_share_ext_ref_id shextrefid
      , t1016.c901_share_type sharetype, t1016.c901_share_status sharestatus, t1016.c1016_subject subject
      , t1016.c1016_msg_dtl msgdetail, t1016.c1016_created_date emailsentdate, get_rep_id_from_user_id(t1016.c1016_ref_id) repid
      ,get_company_id_from_user_id(t1016.c1016_ref_id) compid, t1016.c1016_ref_id userid
       FROM t1016_share t1016, v_in_list vlist
      WHERE t1016.c1016_share_id = vlist.token
        AND t1016.c1016_void_fl IS NULL ORDER BY shareid;
END gm_fch_shared_dtls;
/*******************************************************
* Description : Procedure to fetch details of each share(email) like recipients id
* Author    : Mihir Patel
*******************************************************/
PROCEDURE gm_fch_shared_email_dtls (
        p_share_id     IN T1016_share.c1016_share_id%TYPE,
        p_out_email_dtls OUT TYPES.cursor_type)
AS
BEGIN
    OPEN p_out_email_dtls 
    FOR 
        SELECT c1017_share_detail_id sharedtlid, c1016_share_id shareid, c1017_email_id emailid
      , c901_email_status emailstatus, c901_recipient_type recipienttype, c1017_ref_id refid
      , c901_ref_type reftype, DECODE(c901_ref_type,4000461,get_user_last_first_name(c1017_ref_id),'') username
       FROM t1017_share_detail T1017, V_IN_LIST VLIST
      WHERE t1017.c1016_share_id = vlist.token 
      AND c1017_void_fl IS NULL
      ORDER BY shareid;
END gm_fch_shared_email_dtls;
/*******************************************************
* Description : Procedure to fetch details of file shared as part of email
* Author    : Mihir Patel
*******************************************************/
PROCEDURE gm_fch_shared_file_dtls (
        p_share_id     IN T1016_share.c1016_share_id%TYPE,
        p_out_file_dtls OUT TYPES.cursor_type)
AS
BEGIN
    OPEN p_out_file_dtls 
    FOR 
	    SELECT c1018_share_file_id sharefileid, c1016_share_id shareid, c901_share_file_status sharefilestatus
	  , t1018.c903_upload_file_list updfileid, c1018_link_id linkid, c1018_link_expiry_date linkexpirydate
	  , c1018_link_retry_cnt linkretrycnt, c1018_last_updated_date lastupdateddate, t903.c903_ref_id refidfile
	  , t903.c903_file_name filenm, t903.c901_ref_type reftpid
	  ,DECODE(t903.c901_ref_type,103114,'Documents',4000441,'Documents',get_code_name(t903.c901_ref_type))reftpnm
	  --, get_code_name (t903.c901_ref_type) reftpnm-- removed because Deoed and Non dcoed documents has to be converted to Documents
	  , t903.c901_type typeid, get_code_name (t903.c901_type) typenm, t903.c901_ref_grp refgrpid
	  , get_code_name (t903.c901_ref_grp) refgrpnm
	    -- 103092 - System , 103114 - DCOed Documents
	  , DECODE (t903.c901_ref_grp, '103092', DECODE (t903.c901_ref_type, '103114', get_partnum_desc (t903a.title),
	    t903.c901_file_title), t903.c901_file_title) filetitle, lower (SUBSTR (t903.c903_file_name, INSTR (t903.c903_file_name
	    , '.', - 1, 1) + 1)) fileextension
	   FROM t1018_share_file t1018, v_in_list vlist, T903_UPLOAD_FILE_LIST t903
	  , (
		 SELECT c903_upload_file_list fileid, MAX (DECODE (c901_attribute_type, 103216, c903a_attribute_value)) title,
		    MAX (DECODE (c901_attribute_type, 103217, c903a_attribute_value)) shareable
		   FROM t903a_file_attribute
		  WHERE c903a_void_fl IS NULL
	       GROUP BY c903_upload_file_list
	    )
	    t903a
	  WHERE t1018.c1016_share_id        = vlist.token
	    AND t1018.c903_upload_file_list = t903.c903_upload_file_list
	    AND t903.c903_upload_file_list  = t903a.fileid (+)
	    AND t1018.c1018_void_fl IS NULL ORDER BY shareid,t903.c901_ref_type;
END gm_fch_shared_file_dtls;
/*******************************************************
* Description : Procedure to fetch details of file, email shared as part of marketing collateral email
* Author    : Anilkumar
*******************************************************/
PROCEDURE gm_fch_shared_status_dtls (
        p_fst_nm       IN t101_party.c101_first_nm%TYPE,
        p_lst_nm       IN t101_party.c101_last_nm%TYPE,
        p_party_type   IN t101_party.c901_party_type%TYPE,
        p_email_frm_dt IN VARCHAR2,
        p_email_to_dt  IN VARCHAR2,
        p_email_id     IN t1017_share_detail.c1017_email_id%TYPE,
        p_user_id      IN T1016_share.c1016_ref_id%TYPE,
        p_share_id     IN T1016_share.c1016_share_id%TYPE,
        p_share_status IN T1016_share.c901_share_status%TYPE,
        p_out_sh_dtls OUT TYPES.cursor_type,
        p_out_email_dtls OUT TYPES.cursor_type,
        p_out_file_dtls OUT TYPES.cursor_type)
AS
    v_share_id CLOB;
    v_cur_fch_shares TYPES.cursor_type;
    v_shareid T1016_share.c1016_share_id%TYPE;
    v_linkretrycount T1018_share_file.C1018_link_retry_cnt%TYPE;
BEGIN

    IF p_share_id IS NOT NULL AND p_fst_nm = 'MARKTCOLL'
    THEN
	gm_fch_share_ids_email( p_share_id,p_share_status, v_cur_fch_shares);
    ELSE
	gm_fch_share_ids (p_fst_nm, p_lst_nm, p_party_type, p_email_frm_dt, p_email_to_dt, p_email_id, p_user_id, p_share_id,
    p_share_status, v_cur_fch_shares) ;
    END IF;
    
    LOOP
        FETCH v_cur_fch_shares INTO v_shareid,v_linkretrycount;
        EXIT
    WHEN v_cur_fch_shares%NOTFOUND;
        v_share_id := v_share_id ||v_shareid || ','; -- forming the inlist string
    END LOOP;
    CLOSE v_cur_fch_shares;
    --
    my_context.set_my_inlist_ctx (v_share_id) ;
    gm_fch_shared_dtls (NULL, p_out_sh_dtls) ;
    gm_fch_shared_email_dtls (NULL, p_out_email_dtls) ;
    gm_fch_shared_file_dtls (NULL, p_out_file_dtls) ;
END gm_fch_shared_status_dtls;

/*******************************************************
* Description : Procedure to fetch details of shares based on the parameter passed
*The proc call here  is commn and is used as part of email job as well as to show track link usage report on IPAD
* Author    : Mihir Patel
*******************************************************/
PROCEDURE gm_fch_share_ids (
        p_fst_nm       IN t101_party.c101_first_nm%TYPE,
        p_lst_nm       IN t101_party.c101_last_nm%TYPE,
        p_party_type   IN t101_party.c901_party_type%TYPE,
        p_email_frm_dt IN VARCHAR2,
        p_email_to_dt  IN VARCHAR2,
        p_email_id     IN t1017_share_detail.c1017_email_id%TYPE,
        p_user_id      IN T1016_share.c1016_ref_id%TYPE,
        p_share_id     IN T1016_share.c1016_share_id%TYPE,
        p_share_status IN T1016_share.c901_share_status%TYPE,
        p_fetchshareinfo OUT TYPES.cursor_type)
AS
    v_datefmt VARCHAR2 (20) ;
BEGIN
    v_datefmt := get_rule_value ('DATEFMT', 'DATEFORMAT') ;
    -- -- Cursor to get all the distinct shareids
    OPEN p_fetchshareinfo FOR
     SELECT  t1016.c1016_share_id shareid,1 linkretrycount
       FROM t1016_share t1016, t101_party t101p, t1017_share_detail t1017
      WHERE t1016.c1016_share_id      = t1017.c1016_share_id
        AND  NVL(t1017.c1017_ref_id,'999')=  NVL(t101p.c101_party_id(+),'999')  -- jOIN
        AND t1016.c1016_created_date >= TRUNC(NVL (TO_DATE (p_email_frm_dt, v_datefmt), c1016_created_date))
        AND TRUNC(t1016.c1016_created_date) <= TRUNC(NVL (TO_DATE (p_email_to_dt, v_datefmt), c1016_created_date)) --Left side trunc required if from date and to date are same.
        AND t1016.c1016_ref_id        = NVL (p_user_id, t1016.c1016_ref_id) --validate
        AND t1016.c1016_void_fl      IS NULL
        AND NVL(t101p.c101_first_nm,'-999')    = NVL (p_fst_nm, NVL(t101p.c101_first_nm,'-999'))-- jOIN
        AND  NVL(t101p.c101_last_nm ,'-999')   = NVL (p_lst_nm, NVL(t101p.c101_last_nm,'-999'))-- jOIN
        AND t1017.c1017_email_id       = NVL (p_email_id, t1017.c1017_email_id)
        AND t101p.c901_party_type(+)   = p_party_type --7000--surgeon
        AND t101p.c101_void_fl(+)   IS NULL
        AND t1016.c901_share_status   = NVL (p_share_status, t1016.c901_share_status) --
        AND t1016.c1016_share_id      = NVL (p_share_id, t1016.c1016_share_id)
        AND t1016.c901_share_type     = 103221 --Marketing Collateral Email
        AND NVL(c901_ref_type,999) =DECODE(NVL(c901_ref_type,999),999,999,103238) -- Party ID
      GROUP BY t1016.c1016_share_id
    UNION
     SELECT  t1016.c1016_share_id shareid,1 linkretrycount
       FROM t1016_share t1016, t101_user t101u, t1017_share_detail t1017
      WHERE t1016.c1016_share_id      = t1017.c1016_share_id
        AND t101u.c101_party_id       = t1017.c1017_ref_id
        AND t1016.c1016_created_date >= TRUNC(NVL (TO_DATE (p_email_frm_dt, v_datefmt), c1016_created_date))
        AND TRUNC(t1016.c1016_created_date) <= TRUNC(NVL (TO_DATE (p_email_to_dt, v_datefmt), c1016_created_date)) --Left side trunc required if from date and to date are same.
        AND t1016.c1016_ref_id        = NVL (p_user_id, t1016.c1016_ref_id) --validate
        AND t1016.c1016_void_fl      IS NULL
        AND t101u.C101_USER_F_NAME    = NVL (p_fst_nm, t101u.C101_USER_F_NAME)
        AND t101u.C101_USER_L_NAME        = NVL (p_lst_nm, t101u.C101_USER_L_NAME)
        AND t1017.c1017_email_id       = NVL (p_email_id, t1017.c1017_email_id)
       -- AND t101p.c901_party_type     = p_party_type --(7006 Globus Employee,7005 Sales Rep) 
        AND t1016.c901_share_status   = NVL (p_share_status, t1016.c901_share_status) --
        AND t1016.c1016_share_id      = NVL (p_share_id, t1016.c1016_share_id)
        AND t1016.c901_share_type     = 103221 --Marketing Collateral Email
        AND c901_ref_type = 4000461 --User ID
        GROUP BY t1016.c1016_share_id
        ORDER BY shareid ASC;
END gm_fch_share_ids;
/*******************************************************
* Description : Procedure to fetch details of shares based on the parameter passed
*The proc  is used as part of email job 
* Author    : Mihir Patel
*******************************************************/
PROCEDURE gm_fch_share_ids_email (
        p_share_id     IN T1016_share.c1016_share_id%TYPE,
        p_share_status_inputstr IN VARCHAR2,
        p_fetchshareinfo OUT TYPES.cursor_type)
AS

BEGIN
--Need to set_my_double_inlist_ctx since in the same context we are already using v_in_list context
--p_shareid_inputstr ='103226,103228' which 
 my_context.set_my_double_inlist_ctx (p_share_status_inputstr, '');

OPEN p_fetchshareinfo FOR 
    SELECT t1016.c1016_share_id shareid,get_link_retry_count(t1016.c1016_share_id) linkretrycount
       FROM t1016_share t1016
      WHERE t1016.c1016_void_fl      IS NULL
        AND (t1016.c901_share_status   IN (SELECT token  FROM v_double_in_list)
            OR t1016.c901_share_status  = DECODE(p_share_status_inputstr,NULL,t1016.c901_share_status,999))--added or condition since
       AND t1016.c1016_share_id      = NVL (p_share_id, t1016.c1016_share_id)
       AND t1016.c901_share_type     = 103221 --Marketing Collateral Email
       ORDER BY t1016.c1016_share_id ASC;
END gm_fch_share_ids_email;
END gm_pkg_pdmc_share_eng_rpt;
/