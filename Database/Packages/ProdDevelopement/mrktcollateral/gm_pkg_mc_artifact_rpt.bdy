--@"C:\Database\Packages\ProdDevelopement\mrktcollateral\gm_pkg_mc_artifact_rpt.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_mc_artifact_rpt
IS
/*******************************************************
	 * Description : Procedure to fetch keywords info
	 * Author	   : Anilkumar
	 *******************************************************/
	PROCEDURE gm_fch_all_keywords	(
	    p_token_id    	 IN  	t101a_user_token.c101a_token_id%TYPE ,
	    p_language_id	 IN  	t2001_master_data_updates.c901_language_id%TYPE,
	    p_page_no     	 IN  	NUMBER ,
	    p_uuid        	 IN  	t9150_device_info.c9150_device_uuid%TYPE ,
	    p_keyword_cur 	 OUT 	TYPES.cursor_type 
	)
	AS
		v_device_infoid  t9150_device_info.c9150_device_info_id%TYPE;
   		v_last_rec	t9151_device_sync_dtl.C9151_LAST_SYNC_REC%TYPE; 
		v_update_count   NUMBER := 0;
	BEGIN
	    --get the device info id based on user token
	    v_device_infoid := get_device_id(p_token_id, p_uuid) ;

	    /*below prc check the device's last updated date for Keywords reftype and
		if record exist then it checks is there any Keywords updated after the device updated date.
		if updated Keywords available then add all Keywords into temp table and return count of temp table records*/
	    
	    gm_pkg_pdpc_prodcatrpt.gm_fch_device_updates(v_device_infoid,p_language_id,4000442,p_page_no,v_update_count,v_last_rec);-- 4000442 - Keywords reftype	    
	    
	    --If there is no data available for device then call below prc to add all Keywords from Keyword Ref.
	    
	    IF v_update_count = 0 THEN
	    gm_pkg_pdpc_prodcatrpt.gm_fch_all_keywords_tosync();
	    END IF;
	    
	    gm_fch_keywords_dtl(p_page_no,p_keyword_cur,v_last_rec);
	     
	END gm_fch_all_keywords;
	
	/*******************************************************
	 * Description : Procedure to fetch keywords info Details
	 * Author	   : Anilkumar
	 *******************************************************/
	PROCEDURE gm_fch_keywords_dtl (
	p_pageno           IN     NUMBER,
	p_keyword_dtl_cur  OUT 	  TYPES.cursor_type,
	p_last_rec		   IN     t9151_device_sync_dtl.C9151_LAST_SYNC_REC%TYPE DEFAULT NULL
    )
    AS
		v_start          NUMBER;
		v_end            NUMBER;
		v_page_size      NUMBER;
		v_page_no        NUMBER;
    BEGIN
	    v_page_no       := p_pageno;
	    
	    -- get the paging details
		gm_pkg_pdpc_prodcatrpt.gm_fch_paging_dtl('KEYWORDS','PAGESIZE',v_page_no,v_start,v_end,v_page_size);	    
    	OPEN p_keyword_dtl_cur 
		FOR 
    	SELECT result_count totalsize, COUNT (1) OVER () pagesize, v_page_no pageno, rwnum, CEIL (result_count / v_page_size) totalpages, t2050.* 
	        FROM
	  		  (SELECT T2050.*,ROWNUM rwnum,COUNT (1) OVER () result_count, MAX(DECODE(T2050.keywordid,p_last_rec,ROWNUM,NULL)) OVER() LASTREC  
	  		     FROM
	  		        (SELECT t2050.c2050_keyword_ref_id keywordid,t2050.c2050_keyword_ref_name keywordnm,t2050.c2050_ref_id refid,
					      t2050.c901_ref_type reftype ,t2050.c2050_last_updated_by updatedby,t2050.c2050_last_updated_date updateddt,
					      get_user_name(t2050.c2050_last_updated_by)updatednm,--here updated name is new parameter added for portal.
					      nvl(temp.void_fl,t2050.c2050_void_fl) voidfl
					   FROM t2050_keyword_ref t2050,
	                      my_temp_prod_cat_info temp
	                  WHERE t2050.c2050_keyword_ref_id = temp.ref_id
	                  	ORDER BY t2050.c2050_keyword_ref_id
	                ) t2050
	          ) t2050 
	       WHERE rwnum BETWEEN NVL(LASTREC+1,DECODE(p_last_rec,NULL,v_start,v_start-1)) AND v_end;
	END gm_fch_keywords_dtl;
	
	/*******************************************************
	 * Description : Procedure to fetch keyword id  and for for 
	 *               those id keyword information will be listed.
	 * Author	   : Manikandan Rajasekaran
	 *******************************************************/
	PROCEDURE gm_fch_keywords(
	p_ref_id           IN t2050_keyword_ref.c2050_ref_id%TYPE,
	p_ref_type         IN t2050_keyword_ref.c901_ref_type%TYPE DEFAULT NULL,
	p_keyword_dtl_cur  OUT 	  TYPES.cursor_type
	)
	AS
		v_start          NUMBER;
		v_end            NUMBER;
		v_page_size      NUMBER;
		v_page_no        NUMBER;
		v_ref_id       NUMBER;	
	BEGIN
		
		gm_pkg_pdpc_prodcatrpt.gm_fch_all_keywords_tosync(p_ref_id,p_ref_type);--this is used to get all keyword ref id and all id's will be inserted in to 
																				 --temp table. 
		--This procedure is used for portal here first paramaeter is empty and this procedure is used to fetch keyword information
		gm_fch_keywords_dtl('',p_keyword_dtl_cur);
	
	END gm_fch_keywords;
	
/********************************************************
 * Description : This function is used to validate system keywords.
 * Parameters  : p_refid, p_reftype,p_keyword_nm
********************************************************/
	
	FUNCTION get_system_keyword_chk (
	p_refid	     t2050_keyword_ref.c2050_ref_id%TYPE ,
    p_keyword_nm t2050_keyword_ref.c2050_keyword_ref_name %TYPE,
    p_reftype	 t2050_keyword_ref.c901_ref_type%TYPE
	)
	RETURN NUMBER
	IS
	v_cnt		   NUMBER;
	BEGIN
--
	     SELECT  count(1)
         INTO    v_cnt
         FROM    t2050_keyword_ref t2050
         WHERE   t2050.c2050_ref_id = p_refid --p_sys_id
         AND     t2050.c2050_void_fl IS NULL
         AND     t2050.c901_ref_type  = p_reftype--system
         AND     (t2050.c2050_keyword_ref_name) = (p_keyword_nm); 
		RETURN v_cnt;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				RETURN NULL;
	END get_system_keyword_chk;
/********************************************************
 * Description : This function is used to validate file keywords.
 * Parameters  : p_refid, p_reftype,p_keyword_nm
********************************************************/
	
	FUNCTION get_file_keyword_chk (
	p_refid	     	 t2050_keyword_ref.c2050_ref_id%TYPE ,
	p_system_id	     t2050_keyword_ref.c2050_ref_id%TYPE ,
    p_keyword_nm 	 t2050_keyword_ref.c2050_keyword_ref_name %TYPE,
    p_reftype	     t2050_keyword_ref.c901_ref_type%TYPE
	)
	RETURN NUMBER
	IS
	v_cnt		   NUMBER;
	BEGIN
	    SELECT   count(1)
           INTO   v_cnt
           FROM   t2050_keyword_ref t2050,
                  t903_upload_file_list t903
           WHERE  t903.c903_upload_file_list = t2050.c2050_ref_id
           AND    t903.c903_ref_id             = p_system_id--p_sys_id
           AND    t2050.c2050_void_fl         IS NULL
           AND    t903.c903_delete_fl         IS NULL
           AND    t2050.c901_ref_type          = p_reftype --file
          AND (t2050.c2050_keyword_ref_name) = (p_keyword_nm); 
          
          Return v_cnt;

		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				RETURN NULL;
	END get_file_keyword_chk;	

END gm_pkg_mc_artifact_rpt;
/
	
		  	
		  	