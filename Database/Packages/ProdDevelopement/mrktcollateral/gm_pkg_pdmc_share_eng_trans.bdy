--@"C:\Database\Packages\ProdDevelopement\mrktcollateral\gm_pkg_pdmc_share_eng_trans.bdy";
CREATE OR REPLACE
PACKAGE BODY gm_pkg_pdmc_share_eng_trans
IS
    /*******************************************************
    * Description : Procedure to save information about the eamil details received through webservice(IPAD) which will
    later be used to sent an email
    * Author    : Mihir Patel
    *******************************************************/
PROCEDURE gm_sav_shared_info (
        p_ext_share_id   IN t1016_share.c1016_share_ext_ref_id%TYPE,
        p_share_type     IN t1016_share.c901_share_type%TYPE,
        p_subject        IN t1016_share.c1016_subject%TYPE,
        p_msg            IN t1016_share.c1016_msg_dtl%TYPE,
        p_email_inputstr IN CLOB,
        p_file_inputstr  IN VARCHAR2,
        p_user_id        IN t1016_share.c1016_created_by%TYPE,
        p_share_id OUT VARCHAR2,
        p_status OUT VARCHAR2)
AS
    v_status t1016_share.c901_share_status%TYPE := 103226;--103226-Queued
BEGIN
    p_status := v_status;
    
    
     --validating code lookup values for share
    gm_pkg_pdpc_prodcattxn.gm_validate_code_id(p_share_type,'SHREF','-20611');--Marketing Coll type
    gm_validate_share(p_ext_share_id,'T1016');-- Validate Ext ref Id , It should not be duplicated 
    
    --save detail of each share like sub, message..103226-Queued
    gm_sav_shared_dtls ( p_ext_share_id, p_share_type, p_subject, p_msg, p_user_id, v_status, p_share_id) ;
   
    --save details of recipients and emailid
    gm_sav_email_info (p_share_id, p_email_inputstr) ;
    
    --save CCed recipients , rep,ad,vp
    gm_sav_email_cc_emaildtels(p_share_id,p_user_id);
    
    --Filter Email domains
    gm_sav_filter_email_ids (p_share_id) ;
    --save shared fies
    gm_sav_file_info (p_share_id, p_file_inputstr) ;
END gm_sav_shared_info;
/****************************************,***************
* Description : Procedure to save each share(email) received through service(IPAD)
* Author    : Mihir Patel
*******************************************************/
PROCEDURE gm_sav_shared_dtls (
        p_ext_share_id IN t1016_share.c1016_share_ext_ref_id%TYPE,
        p_share_type   IN t1016_share.c901_share_type%TYPE,
        p_subject      IN t1016_share.c1016_subject%TYPE,
        p_msg          IN t1016_share.c1016_msg_dtl%TYPE,
        p_user_id      IN t1016_share.c1016_created_by%TYPE,
        p_share_status IN t1016_share.c901_share_status%TYPE,
        p_shareid      IN OUT t1016_share.C1016_SHARE_ID%TYPE)
AS
    v_shareid t1016_share.C1016_SHARE_ID%TYPE;
BEGIN
     UPDATE T1016_SHARE
SET C1016_REF_ID                      = NVL (p_user_id, C1016_REF_ID), C1016_SHARE_EXT_REF_ID = NVL (p_ext_share_id, C1016_SHARE_EXT_REF_ID)
    , C901_SHARE_TYPE                 = NVL (p_share_type, C901_SHARE_TYPE), C901_SHARE_STATUS = NVL ( p_share_status,
    C901_SHARE_STATUS), C1016_SUBJECT = NVL (p_subject, C1016_SUBJECT), C1016_MSG_DTL = NVL (p_msg, C1016_MSG_DTL)
  , C1016_LAST_UPDATED_DATE           = SYSDATE, C1016_LAST_UPDATED_BY = p_user_id
  , c1016_update_fl = null
  WHERE C1016_SHARE_ID                = p_shareid
    AND c1016_VOID_FL                IS NULL;
    if (sql%rowcount                                      = 0) then
        --103226 -Queued
         SELECT S1016_SHARE.NEXTVAL INTO v_shareid FROM DUAL;
         INSERT
           INTO T1016_SHARE
            (
                C1016_SHARE_ID, C1016_REF_ID, C1016_SHARE_EXT_REF_ID
              , C901_SHARE_TYPE, C901_SHARE_STATUS, C1016_SUBJECT
              , C1016_MSG_DTL, C1016_CREATED_DATE, C1016_CREATED_BY
            )
            VALUES
            (
                v_shareid, p_user_id, p_ext_share_id
              , p_share_type, 103226,get_rule_value('SUBJECTMKTCOLLEMAIL','EMAIL')
              , NVL(p_msg,get_rule_value('MESSAGEMKTCOLLEMAIL','EMAIL')), SYSDATE, p_user_id
            ) ;
        p_shareid := v_shareid;
    END IF;
END gm_sav_shared_dtls;
/*******************************************************
* Description : Procedure to loop through the details of share like emailid and recipients  in inputstring and save the
details for each row
* Author    : Mihir Patel
*******************************************************/
PROCEDURE gm_sav_email_info
    (p_shareid        IN  t1016_share.C1016_SHARE_ID%TYPE,
    p_email_inputstr IN CLOB)
AS
    
    v_string CLOB := p_email_inputstr;
    v_substring VARCHAR2 (4000) ;
    v_email_id T1017_share_detail.c1017_email_id%TYPE;
    v_recipient_tp T1017_share_detail.c901_recipient_type%TYPE;
    v_ref_id T1017_share_detail.c1017_ref_id%TYPE;
    v_ref_type T1017_share_detail.c901_ref_type%TYPE;
BEGIN
    WHILE INSTR (v_string, '|') <> 0
    LOOP
        --
        v_email_id     := NULL;
        v_recipient_tp := NULL;
        v_ref_id       := NULL;
        v_ref_type     := NULL;
        --
        v_substring    := SUBSTR (v_string, 1, INSTR (v_string, '|')       - 1) ;
        v_string       := SUBSTR (v_string, INSTR (v_string, '|')          + 1) ;
        v_email_id     := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        v_substring    := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        v_recipient_tp := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        v_substring    := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        v_ref_id       := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        v_substring    := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        v_ref_type     := v_substring;
        
        --Validate email
        gm_validate_share(v_email_id,'T1017');
        gm_pkg_pdpc_prodcattxn.gm_validate_code_id(v_recipient_tp,'SHRTYP','-20611');--recipient type
        
        --save details of recipients and emailid
        gm_sav_email_dtls (NULL, p_shareid, v_email_id, NULL, v_recipient_tp, v_ref_id, v_ref_type) ;
    END LOOP;
END gm_sav_email_info;
/*******************************************************
* Description : Procedure to save details of mail like TO, CC email IDs
* Author    : Mihir Patel
*******************************************************/
PROCEDURE gm_sav_email_dtls
    (
        p_share_det_id IN T1017_share_detail.c1017_share_detail_id%TYPE,
        p_share_id     IN T1017_share_detail.c1016_share_id%TYPE,
        p_email_id     IN T1017_share_detail.c1017_email_id%TYPE,
        p_email_status IN T1017_share_detail.c901_email_status%TYPE,
        p_recipient_tp IN T1017_share_detail.c901_recipient_type%TYPE,
        p_ref_id       IN T1017_share_detail.c1017_ref_id%TYPE,
        p_ref_type     IN T1017_share_detail.c901_ref_type%TYPE
    )
AS
 v_ref_id       T1017_share_detail.c1017_ref_id%TYPE;
 v_ref_type    T1017_share_detail.c901_ref_type%TYPE;
BEGIN
	-- party email id is exist, then get party the party id and mapping it
	IF p_ref_id IS NULL OR INSTR(p_ref_id,'.') > 0 THEN
		BEGIN
			
		   SELECT PARTYID,103238 INTO v_ref_id,v_ref_type
		     FROM (
					SELECT t101.c101_party_id PARTYID
					  FROM t107_contact t107, t101_party t101
					 WHERE c107_void_fl IS NULL
					   AND t107.c107_contact_value = p_email_id
					   AND t101.c101_party_id = t107.c101_party_id
					   AND t101.c901_party_type =7000 -- Surgeon
					   AND c901_mode       =90452 -- EMAIL
					ORDER BY t107.c107_primary_fl
				  ) T107
			WHERE  ROWNUM =1;
			
		EXCEPTION WHEN NO_DATA_FOUND THEN
        	v_ref_id := NULL;
		END;
	ELSE
		v_ref_id   := p_ref_id;
		v_ref_type := p_ref_type;
	END IF;
	
	
     UPDATE T1017_SHARE_DETAIL
    SET C1016_SHARE_ID                     = NVL (p_share_id, C1016_SHARE_ID), C1017_EMAIL_ID = NVL (p_email_id, C1017_EMAIL_ID),
        C901_EMAIL_STATUS                  = NVL (p_email_status, C901_EMAIL_STATUS), C901_RECIPIENT_TYPE = NVL (p_recipient_tp,
        C901_RECIPIENT_TYPE), C1017_REF_ID = NVL (v_ref_id, C1017_REF_ID), C901_REF_TYPE = NVL (v_ref_type,
        C901_REF_TYPE)
      WHERE c1017_share_detail_id = p_share_det_id
      AND c1017_VOID_FL IS NULL;
    IF (SQL%ROWCOUNT              = 0) THEN
         INSERT
           INTO T1017_SHARE_DETAIL
            (
                c1017_share_detail_id, C1016_SHARE_ID, C1017_EMAIL_ID
              , C901_EMAIL_STATUS, C901_RECIPIENT_TYPE, C1017_REF_ID
              , C901_REF_TYPE
            )
            VALUES
            (
                S1017_SHARE_DETAIL.nextval, p_share_id, p_email_id
              , p_email_status, p_recipient_tp, v_ref_id
              , v_ref_type
            ) ;
    END IF;
END gm_sav_email_dtls;
/*******************************************************
* Description : Procedure to loop through the details of files being shared and save inforamtion for each file
* Author    : Mihir Patel
*******************************************************/
PROCEDURE gm_sav_file_info
    (
        p_share_id      IN t1016_share.C1016_SHARE_ID%TYPE,
        p_file_inputstr IN VARCHAR2
    )
AS
    CURSOR sharedfileids_cur
    IS
         SELECT TOKEN updfileid FROM v_in_list;
BEGIN
    my_context.set_my_inlist_ctx (p_file_inputstr) ;
    FOR sharedfileids IN sharedfileids_cur
    LOOP
        --Validate email
        gm_validate_share(sharedfileids.updfileid,'T1018');
        --save details of files to be shared
        gm_sav_file_dtls (NULL, p_share_id, NULL, sharedfileids.updfileid, NULL, NULL, NULL, NULL) ;
    END LOOP;
END gm_sav_file_info;
/*******************************************************
* Description : Procedure to save details of file shared as part of email
* Author    : Mihir Patel
*******************************************************/
PROCEDURE gm_sav_file_dtls (
        p_share_file_id  IN T1018_share_file.C1018_SHARE_FILE_ID%TYPE,
        p_share_id       IN T1018_share_file.c1016_share_id%TYPE,
        p_sh_file_status IN T1018_share_file.c901_share_file_status%TYPE,
        p_upd_file_list  IN T1018_share_file.C903_UPLOAD_FILE_LIST%TYPE,
        p_link_id        IN T1018_share_file.C1018_link_id%TYPE,
        p_link_exp_dt    IN T1018_share_file.C1018_link_expiry_date%TYPE,
        p_link_retry_cnt IN T1018_share_file.C1018_link_retry_cnt%TYPE,
        p_user_id        IN T1018_share_file.c1018_last_updated_by%TYPE)
AS
BEGIN
     UPDATE T1018_SHARE_FILE
    SET C1016_SHARE_ID                                         = NVL (p_share_id, C1016_SHARE_ID), C901_SHARE_FILE_STATUS = NVL (p_sh_file_status,
        C901_SHARE_FILE_STATUS), C903_UPLOAD_FILE_LIST         = NVL (p_upd_file_list, C903_UPLOAD_FILE_LIST), C1018_link_id =
        NVL (p_link_id, C1018_link_id), C1018_link_expiry_date = NVL (p_link_exp_dt, C1018_link_expiry_date),
        C1018_link_retry_cnt                                   = NVL (p_link_retry_cnt, C1018_link_retry_cnt),
        C1018_last_updated_date                                = SYSDATE, c1018_last_updated_by = NVL (p_user_id,
        c1018_LAST_UPDATED_BY)
      WHERE C1018_share_file_id = p_share_file_id
       AND C1018_VOID_FL IS NULL;
    IF (SQL%ROWCOUNT            = 0) THEN
         INSERT
           INTO T1018_SHARE_FILE
            (
                C1018_SHARE_FILE_ID, C1016_SHARE_ID, C901_SHARE_FILE_STATUS
              , C903_UPLOAD_FILE_LIST, C1018_link_id, C1018_link_expiry_date
              , C1018_link_retry_cnt
            )
            VALUES
            (
                S1018_SHARE_FILE.nextval, p_share_id, p_sh_file_status
              , p_upd_file_list, p_link_id, p_link_exp_dt
              , p_link_retry_cnt
            ) ;
    END IF;
END gm_sav_file_dtls;
/*******************************************************
* Description : Procedure to filter email ID based on pre defined rules. Based on whether the email id is filtered or
not the status is update to sent or filtered in t1017 table
* Author    : Mihir Patel
*******************************************************/
PROCEDURE gm_sav_filter_email_ids
    (
        p_share_id IN T1017_share_detail.c1016_share_id%TYPE
    )
AS
    v_atrate CHAR
    (
        1
    )
                   := '@';
    v_dot CHAR (1) := '.';
    CURSOR sharedet_cur
    IS
         SELECT C1017_SHARE_DETAIL_ID sharedetid
            --Based on the email ID as input parameter, it returns the domain from the email id
            --e.g from mpatel@globusmedical.com email id the net output is @globusmedical
          , LOWER (SUBSTR (C1017_EMAIL_ID, INSTR (C1017_EMAIL_ID, '@')))  filtereddomain
           FROM T1017_SHARE_DETAIL
          WHERE C1016_SHARE_ID = p_share_id;
    v_domain_filter_fl CHAR (1) ;
    v_email_status T1017_share_detail.c901_email_status%TYPE; 
BEGIN
    FOR sharedet IN sharedet_cur
    LOOP
        v_email_status :=NULL; --Sent
        v_domain_filter_fl         := get_rule_value (sharedet.filtereddomain, 'FILTEREMAIL') ;
        
        IF v_domain_filter_fl      IS NOT NULL THEN
            v_email_status := 103235;--Filtered
        END IF;
        gm_sav_email_dtls (sharedet.sharedetid, NULL, NULL, v_email_status, NULL, NULL, NULL) ;
    END LOOP;
END gm_sav_filter_email_ids;

/*******************************************************
* Description : Procedure to add  rep ,ad,vp as CCed recipients
* Author    : Mihir Patel
*******************************************************/
PROCEDURE gm_sav_email_cc_emaildtels
    (
        p_share_id in t1017_share_detail.c1016_share_id%type,
        p_ref_id IN t1016_share.c1016_ref_id%TYPE
    )
as
   v_cc_recipient_tp  t1017_share_detail.c901_recipient_type%type := 103237;--103237 --CCed type
   v_ref_type      T1017_share_detail.c901_ref_type%TYPE:=4000461; --4000461 usser id
   v_ad_email_id  t101_user.c101_email_id%type;
   v_vp_email_id t101_user.c101_email_id%type;
   v_ad_user_id  t101_user.c101_user_id%type;
   v_vp_user_id t101_user.c101_user_id%type;
   v_rep_email_id t101_user.c101_email_id%type;
   v_rep_id     t703_sales_rep.c703_sales_rep_id%type;
   v_ad_send_fl t906_rules.c906_rule_value%type;
   v_vp_send_fl t906_rules.c906_rule_value%TYPE;
   v_rep_party    t703_sales_rep.c101_party_id%TYPE;
   v_contact_val t107_contact.c107_contact_value%TYPE;
   v_user_email_id t101_user.c101_email_id%type;
BEGIN
   
    v_ad_send_fl:= get_rule_value('COPYADINMKTCOLLEMAIL','EMAIL');
    v_vp_send_fl  := get_rule_value('COPYVPINMKTCOLLEMAIL','EMAIL');
    
    v_rep_id     := get_rep_id_from_user_id (p_ref_id) ;
    
    --If the rep id is null,the logged in user is a non-sales rep, hence we are getting the email id from user table.
    IF v_rep_id IS NULL
    THEN
    	v_user_email_id  := get_user_emailid(p_ref_id);
    	
    	--Temporary code will be removed,once the hardocding in the function is removed.
    	 SELECT DECODE(v_user_email_id,'rshah@globusmedical.com','','suchitra@globusmedical.com','',v_user_email_id) 
    	 INTO 	v_user_email_id 
    	 FROM 	DUAL;
    	 
    END IF;
    
    --If Internal user is logging in, we are copying email to him. 
    IF v_user_email_id IS NOT NULL
    THEN 
   
        gm_sav_email_dtls (NULL, p_share_id, v_user_email_id, NULL, v_cc_recipient_tp, p_ref_id, v_ref_type) ;
        
    END IF;
    
    --v_rep_email_id     := get_rep_emailid(v_rep_id);
   -- v_rep_email_id     := get_user_emailid(p_ref_id);
    v_rep_party  :=  GET_REP_PARTY_ID(v_rep_id);
    v_rep_email_id     := get_party_primary_contact(v_rep_party,90452);--email'90452'
    
    IF v_rep_email_id IS NOT NULL
    THEN 
   
        gm_sav_email_dtls (NULL, p_share_id, v_rep_email_id, NULL, v_cc_recipient_tp, p_ref_id, v_ref_type) ;
        
    END IF;
    
    IF v_ad_send_fl = 'Y'
    THEN
        v_ad_user_id :=gm_pkg_op_do_master.get_ad_rep_id (v_rep_id);
         v_ad_email_id := get_user_emailid (v_ad_user_id) ;
         
         --Temporary code will be removed,once the hardocding in the function is removed.
    	 SELECT DECODE(v_ad_email_id,'rshah@globusmedical.com','','suchitra@globusmedical.com','',v_ad_email_id) 
    	 INTO 	v_ad_email_id 
    	 FROM 	DUAL;
    	 
         IF v_ad_email_id IS NOT NULL
         THEN
            gm_sav_email_dtls (NULL, p_share_id, v_ad_email_id, NULL, v_cc_recipient_tp, v_ad_user_id, v_ref_type) ;
         END IF;
    END IF;
    
    IF v_vp_send_fl = 'Y'
    THEN 
        v_vp_user_id :=gm_pkg_op_do_master.get_vp_rep_id (v_rep_id);
        v_vp_email_id := get_user_emailid (v_vp_user_id) ;
        
        --Temporary code will be removed,once the hardocding in the function is removed.
    	 SELECT DECODE(v_vp_email_id,'rshah@globusmedical.com','','suchitra@globusmedical.com','',v_vp_email_id) 
    	 INTO 	v_vp_email_id 
    	 FROM 	DUAL;
    	 
         IF v_vp_email_id IS NOT NULL
         THEN
            gm_sav_email_dtls (NULL, p_share_id, v_vp_email_id, NULL, v_cc_recipient_tp, v_vp_user_id, v_ref_type) ;
          END IF;
    END IF;
    
  END gm_sav_email_cc_emaildtels;
  
  /*******************************************************
* Description : Procedure to update the details of the links
* Author    : Mihir Patel
*******************************************************/
PROCEDURE gm_upd_file_link_dtls (
        p_share_file_id  IN T1018_share_file.C1018_SHARE_FILE_ID%TYPE,
        p_sh_file_status IN T1018_share_file.c901_share_file_status%TYPE,
        p_link_id        IN T1018_share_file.C1018_link_id%TYPE,
        p_link_exp_dt    IN VARCHAR2,
        p_link_retry_cnt IN T1018_share_file.C1018_link_retry_cnt%TYPE,
        p_share_id       IN t1017_share_detail.c1016_share_id%TYPE)
AS
 v_datefmt VARCHAR2 (20) ;
BEGIN
      v_datefmt := get_rule_value ('DATEFMT', 'DATEFORMAT') ;
      gm_sav_file_dtls(p_share_file_id,p_share_id,p_sh_file_status,NULL,p_link_id,NVL(TO_DATE (p_link_exp_dt, v_datefmt),NULL),p_link_retry_cnt,NULL);
end gm_upd_file_link_dtls;


/*******************************************************
* Description : Procedure to update status of email sent to send
* Author    : Mihir Patel
*******************************************************/
PROCEDURE gm_upd_share_email_status(
       p_shids_inputstr IN VARCHAR2,
       p_email_status IN T1017_share_detail.c901_email_status%TYPE
       )
AS
    CURSOR shareddetids_cur
    IS
         SELECT TOKEN shareddetid FROM v_in_list;
BEGIN
    my_context.set_my_inlist_ctx (p_shids_inputstr) ;
    FOR shareddetids IN shareddetids_cur
    LOOP
      
        --save the status of the email 103234 sent
        gm_sav_email_dtls (shareddetids.shareddetid, NULL, NULL, p_email_status, NULL, NULL, NULL) ;
    END LOOP;
END gm_upd_share_email_status;
/*******************************************************
* Description : Procedure to validate passed share from device
* Author    : Mihir Patel
*******************************************************/
PROCEDURE gm_validate_share (
        p_ref_id IN VARCHAR2,
        p_table  IN VARCHAR2)
AS
    v_type t903_upload_file_list.C901_TYPE%TYPE;
    v_cnt NUMBER ;
    v_share_fl t906_rules.c906_rule_value%type;
BEGIN
         
     --Share
    IF p_table       = 'T1016' THEN
        IF p_ref_id IS NOT NULL--Ext share Id 
            THEN
            SELECT COUNT(1)
            INTO v_cnt
            FROM T1016_share
            WHERE c1016_share_ext_ref_id = p_ref_id
            AND c1016_VOID_FL IS NULL;
            
             IF v_cnt <> 0 THEN
                 --raise_application_error ('-20621', '') ;
                 GM_RAISE_APPLICATION_ERROR('-20999','424','');
            END IF;
           
        END IF;
    END IF;
    
    --Share Details
    IF p_table       = 'T1017' THEN
        IF p_ref_id IS NULL--Email Id
            THEN
            GM_RAISE_APPLICATION_ERROR('-20999','421','');
        END IF;
    END IF;
    --Share File Details
    IF p_table = 'T1018' THEN
        BEGIN
             SELECT C901_TYPE
               INTO v_type
               FROM t903_upload_file_list
              WHERE C903_UPLOAD_FILE_LIST = p_ref_id
                AND C903_DELETE_FL       IS NULL;
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_cnt := 0;
        END;
        --Validate file ID
        IF v_cnt = 0 THEN
        	GM_RAISE_APPLICATION_ERROR('-20999','423','');
        END IF;
       --103202 360/Product Overview 103205 /'103206','Sales Reference Guide'
         v_share_fl         := get_rule_value (v_type, 'SHAREGRP') ;
        IF v_share_fl = 'N' THEN
            GM_RAISE_APPLICATION_ERROR('-20999','422','');
        END IF;
    END IF;
END gm_validate_share;
END gm_pkg_pdmc_share_eng_trans;
/