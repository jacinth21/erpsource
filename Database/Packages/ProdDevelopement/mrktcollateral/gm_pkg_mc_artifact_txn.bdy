--@"C:\Database\Packages\ProdDevelopement\mrktcollateral\gm_pkg_mc_artifact_txn.bdy";
CREATE OR REPLACE
PACKAGE BODY gm_pkg_mc_artifact_txn
IS
  /*************************************************************
  * Description : Procedure which is used for portal which is used to
  *               save keywords info Details
  * Author    : Manikandan Rajasekaran
  *******************************************************/
	PROCEDURE gm_sav_keyword_dtl(
	    p_ref_id      IN t2050_keyword_ref.c2050_ref_id%TYPE,
	    p_ref_type    IN t2050_keyword_ref.c901_ref_type%TYPE,
	    p_system_id   IN t2050_keyword_ref.c2050_ref_id%TYPE,
	    p_inputString IN CLOB,
	    p_userId      IN t2050_keyword_ref.c2050_created_by%TYPE)
	AS
	    v_string CLOB 	:= p_inputString;
	    v_substring 	VARCHAR2 (4000);
	    v_keyword_id 	t2050_keyword_ref.c2050_keyword_ref_id%TYPE;
	    v_keyword_nm 	t2050_keyword_ref.c2050_keyword_ref_name %TYPE;
	BEGIN
	 	gm_validate_keywords(p_inputString,p_ref_id,p_ref_type,p_system_id);
	    WHILE INSTR (v_string, '|') <> 0
	    LOOP
	      --
	      v_substring  := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
	      v_string     := SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
	      v_keyword_id := NULL;
	      v_keyword_nm := NULL;
	      v_keyword_id := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
	      v_substring  := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
	      v_keyword_nm := v_substring ;

	      gm_sav_keyword_ref(v_keyword_id,v_keyword_nm,p_ref_id,p_ref_type,p_userId);--procedure to insert or update.
	    END LOOP;
	 
	END gm_sav_keyword_dtl;
	/*******************************************************
	* Description : Procedure to Save and update keywords info 
	* Author      : Manikandan Rajasekaran
	*******************************************************/
	PROCEDURE gm_sav_keyword_ref(
	p_keyword_id    IN t2050_keyword_ref.c2050_keyword_ref_id%TYPE,
	p_keyword_nm    IN t2050_keyword_ref.c2050_keyword_ref_name%TYPE,
	p_ref_id        IN t2050_keyword_ref.c2050_ref_id%TYPE,
	p_ref_type      IN t2050_keyword_ref.c901_ref_type%TYPE,
	p_userId        IN t2050_keyword_ref.c2050_created_by%TYPE
	)
	AS
	
	BEGIN
		UPDATE t2050_keyword_ref
	      SET c2050_keyword_ref_name =p_keyword_nm,
	          c2050_last_updated_by    = p_userId,
	          c2050_last_updated_date  =SYSDATE
	      WHERE c2050_keyword_ref_id = p_keyword_id
	      AND c2050_void_fl is NULL;
	      
	      IF (SQL%ROWCOUNT = 0) THEN
	        INSERT
	        INTO t2050_keyword_ref
	          (c2050_keyword_ref_id,c2050_keyword_ref_name,c2050_ref_id,c901_ref_type,c2050_created_by,c2050_created_date)
	          VALUES
	          (S2050_keyword_ref.NEXTVAL,p_keyword_nm,p_ref_id,p_ref_type,p_userId,sysdate);
	      END IF;
	END gm_sav_keyword_ref;
	/*******************************************************
	* Description : Procedure to Void keywords 
	* Author      : Manikandan Rajasekaran
	*******************************************************/
	PROCEDURE gm_void_keywords(
	    p_inputString  IN CLOB,
	    p_user_id      IN t2050_keyword_ref.c2050_created_by %TYPE
	  )
	AS
	    v_strlen     NUMBER := NVL(LENGTH (p_inputString), 0);
	    v_string     CLOB := p_inputString || ',';
	    v_substring 	VARCHAR2 (4000);
	    v_keyword_id t2050_keyword_ref.c2050_keyword_ref_id%TYPE;
	BEGIN
	  IF v_strlen                    > 0 THEN
	    WHILE INSTR (v_string, ',') <> 0
	    LOOP
	      v_substring := SUBSTR (v_string, 1, INSTR (v_string, ',') - 1);
	      v_string     := SUBSTR (v_string, INSTR (v_string, ',')    + 1);
	      v_keyword_id := null;
	      v_keyword_id := v_substring;
	      --
	      UPDATE t2050_keyword_ref
	      SET c2050_void_fl          = 'Y' ,
	        c2050_last_updated_date  = sysdate ,
	        c2050_last_updated_by    = p_user_id
	      WHERE c2050_keyword_ref_id = v_keyword_id;
	    END LOOP;
	  END IF;
	END gm_void_keywords;
		/*******************************************************************************************
	* Description : Procedure to used to validate system keywords
	* 				if file level keywords matches system keyword which we either update or insert
	* 				it will throw an error message.
	* Author    : Manikandan Rajasekaran
	**********************************************************************************************/
	PROCEDURE gm_validate_keywords(
	p_inputstr IN    CLOB,
	p_refid    IN    t2050_keyword_ref.c2050_ref_id%TYPE,
	p_reftype  IN    t2050_keyword_ref.c901_ref_type%TYPE,
	p_systemid IN    t2050_keyword_ref.c2050_ref_id%TYPE
	)
	AS
	v_string      CLOB := p_inputstr;
	v_substring   VARCHAR2 (4000);
	v_keyword_id  t2050_keyword_ref.c2050_keyword_ref_id%TYPE;
	v_keyword_nm  t2050_keyword_ref.c2050_keyword_ref_name %TYPE;
	v_sys_chk     NUMBER;
	v_file_chk    NUMBER;
	v_sys_keyword_str VARCHAR2(4000);
	v_file_keyword_str  VARCHAR2(4000);
	v_br_str         VARCHAR2(10);
	v_error_msg     VARCHAR2(4000);
	BEGIN
	
		WHILE INSTR (v_string, '|') <> 0
		  --
		  LOOP
	      v_substring  := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
	      v_string     := SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
	      v_keyword_id := NULL;
	      v_keyword_nm := NULL;
	      v_keyword_id := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
	      v_substring  := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
	      v_keyword_nm := v_substring ;
	      
	     v_sys_chk := gm_pkg_mc_artifact_rpt.get_system_keyword_chk(p_systemid, v_keyword_nm, 103108);
	    
         IF   v_sys_chk > 0 Then
                 v_sys_keyword_str := v_sys_keyword_str || v_keyword_nm;
                 
         End if;
         v_file_chk := gm_pkg_mc_artifact_rpt.get_file_keyword_chk(p_refid, p_systemid, v_keyword_nm, 4000410 );
         IF   v_file_chk > 0 Then
                 v_file_keyword_str := v_file_keyword_str || v_keyword_nm;
          
         End if;

	      END LOOP;
	     
	      IF v_sys_keyword_str IS NOT NULL THEN
       		v_error_msg:= 'Keywords already exist for System :'|| v_sys_keyword_str;
       		v_br_str := '<BR>';
       	  END IF;
       	  IF v_file_keyword_str IS NOT NULL  THEN
       		v_error_msg:= v_error_msg || v_br_str || 'Keywords already exist for Files : '|| v_file_keyword_str ;
		  END IF;
		  IF v_error_msg IS NOT NULL THEN
		 	raise_application_error('-20999', v_error_msg);
		  END IF;

	
	END gm_validate_keywords;
END gm_pkg_mc_artifact_txn;
/