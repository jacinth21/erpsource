/* Formatted on 2009/02/16 12:30 (Formatter Plus v4.8.0) */
--@"C:\Database\Packages\ProdDevelopement\gm_pkg_pd_file_info.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_pd_file_info
IS
	/*******************************************************
	 * Description : Procedure to fetch uploaded_files info
	*******************************************************/
	PROCEDURE gm_fch_uploaded_file_info (
		p_token_id     IN  t101a_user_token.c101a_token_id%TYPE
	  , p_language_id  IN  NUMBER
	  , p_page_no      IN  NUMBER
	  , p_uuid		   IN  t9150_device_info.c9150_device_uuid%TYPE
	  ,	p_uploaded_filelist_cur  OUT TYPES.cursor_type
	)
	AS
	
	v_device_infoid		   t9150_device_info.c9150_device_info_id%TYPE;
	v_start     NUMBER;
    v_end       NUMBER;
    v_page_size NUMBER;
	v_page_no   NUMBER;
	v_update_count NUMBER := 0;
   	v_last_rec	t9151_device_sync_dtl.C9151_LAST_SYNC_REC%TYPE;     
	BEGIN
	   --get the device info id based on user token	  
	   v_device_infoid := get_device_id(p_token_id, p_uuid) ;
	   
	   --v_device_infoid := get_device_id(p_token_id, p_uuid) ;
	   v_page_no := p_page_no;
	   -- get the SYSTEM paging details defined in rules. 
	   gm_pkg_pdpc_prodcatrpt.gm_fch_paging_dtl('FILES','PAGESIZE',v_page_no,v_start,v_end,v_page_size);

	   gm_pkg_pdpc_prodcatrpt.gm_fch_device_updates(v_device_infoid,p_language_id,4000410,v_page_no,v_update_count,v_last_rec);
	   
	   --If there is no update data available for device then call below prc to add all File details.		
	   IF v_update_count = 0
	   THEN
			gm_pkg_pdpc_prodcatrpt.gm_fch_all_uploaded_fileinfo;		
	   END IF;   
   
   
	   OPEN p_uploaded_filelist_cur
	   FOR	  
		 SELECT RESULT_COUNT totalsize,  COUNT (1) OVER () pagesize, v_page_no pageno, rwnum, CEIL (RESULT_COUNT / v_page_size) totalpages, t903.*  
		   FROM(
		         SELECT t903.*, ROWNUM rwnum, COUNT (1) OVER () result_count,MAX(DECODE(T903.fileid,v_last_rec,ROWNUM,NULL)) OVER() LASTREC
		           FROM(                
			            SELECT t903.c903_upload_file_list fileid, t903.c903_file_name filenm, t903.c903_ref_id filerefid,
			            	   DECODE(T903.C901_REF_GRP,'103092',DECODE(t903.c901_ref_type,'103114',
			            	   											 get_partnum_desc(t903a.title),
			            	   											  t903.c901_file_title),t903.c901_file_title) filetitle,
                      		   t903.c901_ref_grp filerefgroup,t903.c901_ref_type TYPE, 
                      		   t903.c903_file_size filesize,t903.c903_file_seq_no fileseq,
                      		   NVL (t903.c903_last_updated_by, t903.c903_created_by) uploadedby, 
                     		   NVL (t903.c903_last_updated_date, t903.c903_created_date) uploadeddt,
                     		   NVL(temp.void_fl,t903.c903_delete_fl) voidfl , t903.C901_TYPE subtype ,
                     		   t903a.shareable shareablefl 
                   		FROM t903_upload_file_list t903, my_temp_prod_cat_info temp,(
                   			SELECT c903_upload_file_list fileid,
          					MAX (DECODE (c901_attribute_type, 103216, c903a_attribute_value)) title,
          					MAX (DECODE (c901_attribute_type, 103217, c903a_attribute_value)) shareable
          					FROM t903a_file_attribute
          					WHERE c903a_void_fl IS NULL
        					GROUP BY c903_upload_file_list)
        				   t903a
		                WHERE t903.c903_upload_file_list = temp.ref_id
		                AND t903.c903_upload_file_list = t903a.fileid (+)
		                AND (T903.C901_REF_GRP in (103112,107994,107993) OR t903.c901_ref_type   IN (SELECT c906_rule_value FROM t906_rules 
		                                              WHERE c906_rule_id ='FILE_TYPE' 
 			 							  				AND c906_rule_grp_id = 'PROD_CAT_SYNC' 
 			 							  				AND c906_void_fl IS NULL))--Image, Videos, DCOed Documents ,Non DCOed Documents 
						AND t903.c901_ref_grp IS NOT NULL
						ORDER BY t903.c903_upload_file_list
		            )
		            t903
		    )
		    t903
		  WHERE rwnum  BETWEEN NVL(LASTREC+1,DECODE(v_last_rec,NULL,v_start,v_start-1)) AND v_end;

  
	END gm_fch_uploaded_file_info;
	
END gm_pkg_pd_file_info;
/
