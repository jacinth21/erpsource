-- @"C:\Database\Packages\ProdDevelopement\gm_pkg_pd_system_trans.bdy"
CREATE OR REPLACE
PACKAGE BODY gm_pkg_pd_system_trans
    --
IS
    /*****************************************************************************************
    * Description : This procedure is used to save new system
    *******************************************************************************************/
PROCEDURE gm_sav_system_detail (
        p_system_id   IN t207_set_master.c207_set_id%type,
        p_system_name IN t207_set_master.c207_set_nm%type,
        p_sys_desc    IN t207_set_master.c207_set_desc%TYPE,
        p_det_desc    IN t207_set_master.c207_set_dtl_desc%TYPE,
        p_comment     IN t902_log.c902_comments%TYPE,
        p_user_id     IN t207_set_master.c207_created_by%TYPE,
        p_division_id IN t207_set_master.c1910_division_id%TYPE,
        p_company_id  IN t207_set_master.c1900_company_id%TYPE,
        p_out_sysid OUT VARCHAR2)
AS
    v_exist_sys_fl  VARCHAR2 (1) := 'N';
    v_max_seq       NUMBER;
    v_max_set       VARCHAR2 (5) ;
    v_baseline      VARCHAR2 (1) ;
    v_set_cnt       NUMBER;
    v_exist_sys_cnt NUMBER;
    v_msg           VARCHAR2 (100) ;
BEGIN
     SELECT COUNT (1)
       INTO v_set_cnt
       FROM t207_set_master
      WHERE c207_set_id       = p_system_id
        AND c901_set_grp_type = 1600
        AND c207_void_fl     IS NULL;
    IF v_set_cnt             <> 0 THEN
        gm_sav_upd_linked_set_master (p_system_id, p_system_name, p_sys_desc, p_user_id, p_det_desc, p_division_id, p_company_id) ;
        p_out_sysid := p_system_id;
    ELSE
         SELECT COUNT (1)
           INTO v_exist_sys_cnt
           FROM t207_set_master
          WHERE UPPER (regexp_replace (c207_set_nm, '.?trade;|.?reg;|.?\(.*\)')) LIKE UPPER (p_system_name)
            AND c901_set_grp_type = 1600
            AND c207_void_fl     IS NULL;
        IF v_exist_sys_cnt       <> 0 THEN
        GM_RAISE_APPLICATION_ERROR('-20609','375',p_system_name);
            
        END IF;
         SELECT s207_system_id.NEXTVAL INTO v_max_set FROM DUAL;
        /*t207_set_master
        WHERE c207_set_id LIKE '300%' AND c901_set_grp_type = 1600; */
         SELECT MAX (c207_seq_no) + 1
           INTO v_max_seq
           FROM t207_set_master
          WHERE c207_set_id LIKE '300%'
            AND c207_set_id != '300.026';
        --save the four sets ('300.xxx', 'SET.xxx', 'AAE.xxx', 'AA.xxx')
        gm_sav_set_master ('300.' || TO_CHAR (v_max_set), p_system_name, p_sys_desc, p_user_id, 4060, --Implants
        1600, --Sales Report Grp
        v_max_seq, NULL, NULL, p_det_desc,p_division_id,p_company_id) ;
        gm_sav_set_master ('SET.' || TO_CHAR (v_max_set), p_system_name || ' - Additional Inventory', p_sys_desc,
        p_user_id, 4060, --Implants
        1604, --Sales Unassigned Parts
        999, --seq no
        20101, --Supporting set
        20701, --Additional Inventory
        p_det_desc,p_division_id,p_company_id) ;
        gm_sav_set_master ('AAE.' || TO_CHAR (v_max_set), p_system_name || ' - Additional Items', p_sys_desc, p_user_id
        , NULL, 1604, --Sales Unassigned Parts
        299, --seq no
        NULL, 20706, --Additional AAE
        p_det_desc,p_division_id,p_company_id) ;
        gm_sav_set_master ('AA.' || TO_CHAR (v_max_set), p_system_name || ' - Items', p_sys_desc, p_user_id, NULL, 1605
        , --Set Consignment
        199, --seq no
        NULL, 20703, --Additional Available Items
        p_det_desc,p_division_id,p_company_id) ;
        /****create mapping records in t207a**********/
        --20002 Sales Set Mapping
        --20003 Set Mapping with Main
        --20005 Additional Items From Set Mapping
        gm_insert_set_link ('300.' || v_max_set, 'SET.' || v_max_set, 20002, NULL) ; --Shared NULL
        gm_insert_set_link ('SET.' || v_max_set, 'AAE.' || v_max_set, 20003, NULL) ;
        gm_insert_set_link ('AAE.' || v_max_set, 'AA.' || v_max_set, 20005, NULL) ;
        p_out_sysid := '300.' || v_max_set;
    END IF;
    IF p_out_sysid IS NOT NULL THEN
        -- Below procedure is used to save language details into t2000_master_metadata
        gm_pkg_pd_language.gm_sav_lang_details (p_out_sysid, '103092' -- 103092-SYSTEM Type
        , p_system_name, p_sys_desc, p_det_desc, p_user_id) ;
        gm_update_log (p_out_sysid, p_comment, '4000413', p_user_id, v_msg) ; --System Setup Log Type
    END IF;
END gm_sav_system_detail;
/*****************************************************************************************
* Description : This procedure is creat links for Standard and Additional  sets
* std set string format : '978.901,Y,20740|978.902,N,20741'
* set name format: 'REVLOK Implant and Instrument Set^REVLOK Disposable Instruments Set'
* addtional set format : '978.901,N,20740|978.902,N,20741'
*
*******************************************************************************************/
PROCEDURE gm_sav_system_map (
        p_system_id           IN t207_set_master.c207_set_id%TYPE,
        p_std_set_ids         IN VARCHAR2,
        p_addl_set_ids        IN VARCHAR2,
        p_loaner_std_set_ids  IN VARCHAR2,
        p_loaner_addl_set_ids IN VARCHAR2,
        p_user_id             IN t207_set_master.c207_created_by%TYPE,
        p_action              IN VARCHAR2,
        p_division_id         IN t207_set_master.c1910_division_id%TYPE,
        p_company_id          IN t207_set_master.c1900_company_id%TYPE)
AS
BEGIN
    /****create mapping records in t207a********/
    --4070 Consignment
    --4071 Sales
    IF p_std_set_ids IS NOT NULL THEN
        gm_link_std_set (p_std_set_ids, 4070, p_user_id, p_system_id, p_action, p_division_id, p_company_id) ;
    END IF;
    IF p_addl_set_ids IS NOT NULL THEN
        gm_link_additional_set (p_addl_set_ids, 4070, p_user_id, p_system_id, p_action, p_division_id, p_company_id) ;
    END IF;
    IF p_loaner_std_set_ids IS NOT NULL THEN
        gm_link_std_set (p_loaner_std_set_ids, 4071, p_user_id, p_system_id, p_action, p_division_id, p_company_id) ;
    END IF;
    IF p_loaner_addl_set_ids IS NOT NULL THEN
        gm_link_additional_set (p_loaner_addl_set_ids, 4071, p_user_id, p_system_id, p_action, p_division_id, p_company_id) ;
    END IF;

END gm_sav_system_map;
/*****************************************************************************************
* Description : This procedure is creat links for Standard sets
*'978.901,Y,20740|978.902,N,20741'  set id ,Baseline Set, Shared
*******************************************************************************************/
PROCEDURE gm_link_std_set (
        p_std_set_ids IN VARCHAR2,
        p_set_type    IN t207_set_master.c207_type%TYPE,
        p_user_id     IN t101_user.c101_user_id%TYPE,
        p_system_id   IN t207_set_master.c207_set_id%TYPE,
        p_action      IN VARCHAR2,
        p_division_id IN t207_set_master.c1910_division_id%TYPE,
        p_company_id  IN t207_set_master.c1900_company_id%TYPE)
AS
    v_std_setid_len NUMBER          := NVL (LENGTH (p_std_set_ids), 0) ;
    v_std_setid_str VARCHAR2 (4000) := p_std_set_ids;
    v_substring     VARCHAR2 (4000) ;
    v_set_id t207_set_master.c207_set_id%TYPE;
    v_baseline VARCHAR2 (2) ;
    v_system_id t207_set_master.c207_set_id%TYPE;
    v_shared NUMBER;
    v_baseline_sysnm t207_set_master.C207_SET_NM%TYPE;
BEGIN
    IF v_std_setid_len                      > 0 THEN
        WHILE INSTR (v_std_setid_str, '|') <> 0
        LOOP
            --
            v_set_id        := NULL;
            v_baseline      := NULL;
            v_shared        := NULL;
            v_substring     := SUBSTR (v_std_setid_str, 1, INSTR (v_std_setid_str, '|') - 1) ;
            v_std_setid_str := SUBSTR (v_std_setid_str, INSTR (v_std_setid_str, '|')    + 1) ;
            v_set_id        := SUBSTR (v_substring, 1, INSTR (v_substring, ',')         - 1) ;
            v_substring     := SUBSTR (v_substring, INSTR (v_substring, ',')            + 1) ;
            v_baseline      := SUBSTR (v_substring, 1, INSTR (v_substring, ',')         - 1) ;
            v_substring     := SUBSTR (v_substring, INSTR (v_substring, ',')            + 1) ;
            v_shared        := v_substring;
            --update std sets
            IF v_baseline = 'Y' THEN
                BEGIN
                     SELECT C207_SET_NM
                       INTO v_baseline_sysnm
                       FROM t207_set_master
                      WHERE C207_SET_SALES_SYSTEM_ID <> p_system_id
                        AND C207_SET_ID               = v_set_id
                        AND c901_cons_rpt_id          = 20100 --20100-Full Set/ baseline set
                        AND C207_VOID_FL             IS NULL;
                EXCEPTION
                WHEN NO_DATA_FOUND --if the set is being associated with the system for the first time
                    THEN
                    v_baseline_sysnm := NULL;
                WHEN TOO_MANY_ROWS --IF historic data for system is already associated with multiple base line
                    THEN
                    GM_RAISE_APPLICATION_ERROR('-20500','376','');
                    
                END;
                IF v_baseline_sysnm IS NOT NULL THEN
              GM_RAISE_APPLICATION_ERROR('-20500','377',v_set_id||'#$#'||v_baseline_sysnm);
                 
                END IF;
            END IF;
            --20731- mini sset logic
            --20700-Main Set/20702 Additional Inventory Set
            IF p_action = 'edit' THEN
                --20700 Main Set
                --20002 Sales Set Mapping
                gm_sav_edit_linked_set (p_system_id, v_set_id, 20002, v_shared, 20700, p_set_type, p_division_id, p_company_id) ;
                gm_upt_set_master (v_set_id, p_user_id, p_system_id, 20731, 20700, v_baseline, NULL) ;--Void
            ELSE
                gm_sav_set_link (p_system_id, v_set_id, 20002, p_set_type, v_shared, 20700, p_action, p_division_id, p_company_id) ;
                gm_upt_set_master (v_set_id, p_user_id, p_system_id, 20731, 20700, v_baseline, NULL) ; --VOID last
                -- param
                gm_upt_set_details (p_system_id, v_set_id, '300.026', p_user_id) ;
            END IF;
        END LOOP;
    END IF;
END gm_link_std_set;
/*****************************************************************************************
* Description : This procedure is creat links for Additional available sets
*******************************************************************************************/
PROCEDURE gm_link_additional_set (
        p_additional_set_ids IN VARCHAR2,
        p_set_type           IN t207_set_master.c207_type%TYPE,
        p_user_id            IN t101_user.c101_user_id%TYPE,
        p_system_id          IN t207_set_master.c207_set_id%TYPE,
        p_action             IN VARCHAR2,
        p_division_id        IN t207_set_master.c1910_division_id%TYPE,
        p_company_id         IN t207_set_master.c1900_company_id%TYPE)
AS
    v_additional_set_ids_len NUMBER          := NVL (LENGTH (p_additional_set_ids), 0) ;
    v_additional_set_ids_str VARCHAR2 (4000) := p_additional_set_ids;
    v_substring              VARCHAR2 (4000) ;
    v_set_id t207_set_master.c207_set_id%TYPE;
    v_baseline VARCHAR2 (2) ;
    v_shared   NUMBER;
BEGIN
    IF v_additional_set_ids_len                      > 0 THEN
        WHILE INSTR (v_additional_set_ids_str, '|') <> 0
        LOOP
            --
            v_set_id                 := NULL;
            v_baseline               := NULL;
            v_shared                 := NULL;
            v_substring              := SUBSTR (v_additional_set_ids_str, 1, INSTR (v_additional_set_ids_str, '|') - 1) ;
            v_additional_set_ids_str := SUBSTR (v_additional_set_ids_str, INSTR (v_additional_set_ids_str, '|')    + 1)
            ;
            v_set_id    := SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1) ;
            v_substring := SUBSTR (v_substring, INSTR (v_substring, ',')    + 1) ;
            v_baseline  := SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1) ;
            v_substring := SUBSTR (v_substring, INSTR (v_substring, ',')    + 1) ;
            v_shared    := v_substring;
            --
            -- 20731-Use Critical Flag used in t207.c901_minset_logic
            --20700-Main Set/20702 Additional Inventory Set t207.C901_HIERARCHY
            --20101-Supporting set/20100-Full Set, used as baseline in C901_CONS_RPT_ID(Baseline)
            --Additional Set cannot be a baseline set
            IF p_action = 'edit' THEN
                --20701 Additional Inventory used for set like SET.XXXX
                --20003 Set Mapping with Main
                gm_sav_edit_linked_set (p_system_id, v_set_id, 20003, v_shared, 20702, p_set_type, p_division_id, p_company_id) ;
                gm_upt_set_master (v_set_id, p_user_id, p_system_id, 20731, 20702, 20101, NULL) ;--Void last param
            ELSE
                --DBMS_OUTPUT.put_line (' About to update Additional set ' || v_set_id || ' for system ' || p_system_id
                -- );
                gm_sav_set_link ('SET.' || SUBSTR (p_system_id, 5), v_set_id, 20003, p_set_type, v_shared, 20702,
                p_action, p_division_id, p_company_id) ;
                ---update for AAS
                --20702 Additional Inventory Set
                gm_upt_set_master (v_set_id, p_user_id, p_system_id, 20731, 20702, 20101, NULL) ;
                gm_upt_set_details (p_system_id, v_set_id, '300.026', p_user_id) ;
            END IF;
        END LOOP;
    END IF;
    -- DBMS_OUTPUT.put_line (' Linked p_additional_set_ids ids  '|| p_additional_set_ids );
END gm_link_additional_set;
/*****************************************************************************************
* Description : This procedure is save new set in set master table
*******************************************************************************************/
PROCEDURE gm_sav_set_master (
        p_set_id       IN t207_set_master.c207_set_id%TYPE,
        p_set_nm       IN t207_set_master.c207_set_nm%TYPE,
        p_set_desc     IN t207_set_master.c207_set_desc%TYPE,
        p_user_id      IN t205_part_number.c205_last_updated_by%TYPE,
        p_category     IN t207_set_master.c207_category%TYPE,
        p_set_grp_type IN t207_set_master.c901_set_grp_type%TYPE,
        p_seq_no       IN t207_set_master.c207_seq_no%TYPE,
        p_rpt_id       IN t207_set_master.c901_cons_rpt_id%TYPE,
        p_hierarchy    IN t207_set_master.c901_hierarchy%TYPE,
        p_set_dtl_desc IN t207_set_master.c207_set_dtl_desc%TYPE,
        p_division_id  IN t207_set_master.c1910_division_id%TYPE,
        p_company_id   IN t207_set_master.c1900_company_id%TYPE)
AS
BEGIN
     INSERT
       INTO t207_set_master
        (
            c207_set_id, c207_set_nm, c207_set_desc
          , c207_created_date, c207_created_by, c207_category
          , c901_set_grp_type, c207_seq_no, c901_cons_rpt_id
          , c901_hierarchy, c207_set_dtl_desc, c1910_division_id, c1900_company_id
        )
        VALUES
        (
            p_set_id, p_set_nm, p_set_nm
          , CURRENT_DATE, p_user_id, p_category
          , p_set_grp_type, p_seq_no, p_rpt_id
          , p_hierarchy, p_set_dtl_desc, p_division_id, p_company_id
        ) ;
END gm_sav_set_master;
/*****************************************************************************************
* Description : This procedure updates the system details as well as linked set details when a system name is edited
*******************************************************************************************/
PROCEDURE gm_sav_upd_linked_set_master
    (
        p_sys_id       IN t207_set_master.c207_set_id%TYPE,
        p_set_nm       IN t207_set_master.c207_set_nm%TYPE,
        p_set_desc     IN t207_set_master.c207_set_desc%TYPE,
        p_user_id      IN t205_part_number.c205_last_updated_by%TYPE,
        p_set_dtl_desc IN t207_set_master.c207_set_dtl_desc%TYPE,
        p_division_id  IN t207_set_master.c1910_division_id%TYPE,
        p_company_id   IN t207_set_master.c1900_company_id%TYPE
    )
AS
    CURSOR linkedset_cur
    IS
         SELECT t207.c207_set_id setid, t207.C901_HIERARCHY hierid
           FROM t207_set_master t207, (
                 SELECT CONNECT_BY_ROOT t207a.c207_link_set_id actual_set_id
                   FROM t207a_set_link t207a
                  WHERE t207a.c207_main_set_id IN
                    (
                         SELECT t207.c207_set_id
                           FROM t207_set_master t207
                          WHERE t207.c901_set_grp_type = 1600
                            AND c207_set_id            = p_sys_id
                            AND t207.c207_void_fl     IS NULL
                    )
                CONNECT BY PRIOR c207_main_set_id = t207a.c207_link_set_id
            )
            linkdata
          WHERE linkdata.actual_set_id = t207.c207_set_id
            AND t207.C901_HIERARCHY   IN (20701, 20706, 20703) ;
        -- 20701-Additional Inventory
        --20706-Additional AAE
        --20703-Additional Available Items
        v_set_nm t207_set_master.c207_set_nm%TYPE;
    BEGIN
        -- update system details
        gm_sav_upd_set_master (p_sys_id, p_set_nm, p_set_desc, p_user_id, p_set_dtl_desc, p_division_id, p_company_id) ;
        FOR linkedset IN linkedset_cur
        LOOP
            IF linkedset.hierid    = 20701 THEN
                v_set_nm          := p_set_nm || ' - Additional Inventory';
            ELSIF linkedset.hierid = 20706 THEN
                v_set_nm          := p_set_nm || ' - Additional Items';
            ELSE-- linkedset.hierid = 20703
                v_set_nm := p_set_nm || ' - Items';
            END IF;
            --update details for all linked sets
            gm_sav_upd_set_master (linkedset.setid, v_set_nm, p_set_desc, p_user_id, p_set_dtl_desc, p_division_id, p_company_id) ;
        END LOOP;
    END gm_sav_upd_linked_set_master;
    /*****************************************************************************************
    * Description : This procedure update set detail when a system detail is edited
    *******************************************************************************************/
PROCEDURE gm_sav_upd_set_master (
        p_set_id       IN t207_set_master.c207_set_id%TYPE,
        p_set_nm       IN t207_set_master.c207_set_nm%TYPE,
        p_set_desc     IN t207_set_master.c207_set_desc%TYPE,
        p_user_id      IN t205_part_number.c205_last_updated_by%TYPE,
        p_set_dtl_desc IN t207_set_master.c207_set_dtl_desc%TYPE,
        p_division_id  IN t207_set_master.c1910_division_id%TYPE,
        p_company_id   IN t207_set_master.c1900_company_id%TYPE)
AS
BEGIN
     UPDATE t207_set_master
    SET c207_set_nm          = p_set_nm, c207_set_desc = p_set_desc, c207_set_dtl_desc = p_set_dtl_desc
      , c1910_division_id = p_division_id, c1900_company_id = p_company_id
      , c207_last_updated_by = p_user_id, c207_last_updated_date = CURRENT_DATE
      WHERE c207_set_id      = p_set_id;
END gm_sav_upd_set_master;
/*****************************************************************************************
* Description : This procedure is create links for a standard and additional available set
*******************************************************************************************/
PROCEDURE gm_sav_set_link (
        p_main_setid  IN t207a_set_link.c207_main_set_id%TYPE,
        p_set_linkid  IN t207a_set_link.c207_link_set_id%TYPE,
        p_type        IN t207a_set_link.c901_type%type,
        p_set_type    IN t207_set_master.c207_type%type,
        p_shared      IN t207a_set_link.c901_shared_status%type,
        p_hierarcy_tp IN t207_set_master.c901_hierarchy%type,
        p_action      IN VARCHAR2,
        p_division_id IN t207_set_master.c1910_division_id%TYPE,
        p_company_id  IN t207_set_master.c1900_company_id%TYPE)
AS
    v_main_set_id t207a_set_link.c207_main_set_id%TYPE;
BEGIN
    BEGIN
        -- Check if mapping exists
         SELECT t207a.c207_main_set_id
           INTO v_main_set_id
           FROM t207a_set_link t207a
          WHERE t207a.c207_link_set_id = p_set_linkid
            AND t207a.c901_type NOT   IN (20004)
            AND t207a.c207_main_set_id = p_main_setid; --Added : if set is shared
        --AND t207a.c901_shared_status IS NULL;
        -- If itz not 1601 then it must be an additional stuff. so just return
        -- No Changes in mapping
        IF (v_main_set_id = p_main_setid) THEN
        GM_RAISE_APPLICATION_ERROR('-20500','378',p_set_linkid);
            
        ELSE--
            gm_save_validate_set_link (p_main_setid, p_set_linkid, p_type, p_set_type, p_shared, p_hierarcy_tp,
            p_action, p_division_id, p_company_id) ;
        END IF;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        --DBMS_OUTPUT.put_line (' No Data found so creating a set ' || p_set_linkid || ' with main set id ' ||
        -- p_main_setid) ;
        gm_save_validate_set_link (p_main_setid, p_set_linkid, p_type, p_set_type, p_shared, p_hierarcy_tp, p_action, p_division_id, p_company_id) ;
    END;
END gm_sav_set_link;
/*****************************************************************************************
* Description : This procedure is validate if links need to be created
*******************************************************************************************/
PROCEDURE gm_save_validate_set_link (
        p_main_setid  IN t207a_set_link.c207_main_set_id%TYPE,
        p_set_linkid  IN t207a_set_link.c207_link_set_id%TYPE,
        p_type        IN t207a_set_link.c901_type%type,
        p_set_type    IN t207_set_master.c207_type%type,
        p_shared      IN t207a_set_link.c901_shared_status%type,
        p_hierarcy_tp IN t207_set_master.c901_hierarchy%type,
        p_action      IN VARCHAR2,
        p_division_id IN t207_set_master.c1910_division_id%TYPE,
        p_company_id  IN t207_set_master.c1900_company_id%TYPE)
AS
    v_child_set_id t207a_set_link.c207_main_set_id%TYPE;
    v_set_name t207_set_master.c207_set_nm%TYPE;
    v_user_id t205_part_number.c205_last_updated_by%TYPE;
    v_ai_set_count     NUMBER;
    v_existing_link_fl VARCHAR2 (2) ;
TYPE v_exist_sys_ids_t
IS
    TABLE OF t207a_set_link.c207_main_set_id%TYPE;
    v_exist_sys_ids v_exist_sys_ids_t;
    v_exist_sys VARCHAR2 (4000) ;
    v_exist_add_sys_id t207a_set_link.c207_main_set_id%TYPE;
BEGIN
    v_existing_link_fl := 'Y';
    BEGIN
         SELECT c207_link_set_id
           INTO v_child_set_id
           FROM t207a_set_link
          WHERE c207_main_set_id = p_set_linkid
            AND c901_type        = 20004;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_child_set_id := NULL;
    END;
    --If Set is already mapped as additional  and user is trying to link the set as main  by  using a create button
    IF p_action = 'create' AND p_hierarcy_tp = 20700-- Standard
        THEN
        BEGIN
             SELECT c207_main_set_id
               INTO v_exist_add_sys_id
               FROM t207a_set_link
              WHERE c901_type        <> 20004
                AND c207_link_set_id IN
                (
                     SELECT c207_main_set_id
                       FROM t207a_set_link
                      WHERE c901_type       <> 20004
                        AND c207_link_set_id = p_set_linkid
                        AND c901_type        = 20003
                ) ;
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_exist_add_sys_id := NULL;
        WHEN TOO_MANY_ROWS THEN
            v_exist_add_sys_id := NULL;
        END;
        IF v_exist_add_sys_id = p_main_setid THEN
        GM_RAISE_APPLICATION_ERROR('-20500','379',p_set_linkid);
        END IF;
    END IF;
    --If Set is already mapped as standard  and user is trying to link the set as Additional  by  using a create
    -- button
    IF p_action = 'create' AND p_hierarcy_tp = 20702-- Additional
        THEN
        BEGIN
             SELECT c207_main_set_id
               INTO v_exist_add_sys_id
               FROM t207a_set_link
              WHERE c901_type       <> 20004
                AND c207_link_set_id = p_set_linkid
                AND c901_type        = 20002;
        EXCEPTION
        WHEN NO_DATA_FOUND --
            THEN
            v_exist_add_sys_id := NULL;
        WHEN TOO_MANY_ROWS --if system are shared then TOO_MANY_ROWS exception will be thrown
            THEN
            v_exist_add_sys_id := NULL;
        END;
        IF 'SET.' || SUBSTR (v_exist_add_sys_id, 5) = p_main_setid THEN
        GM_RAISE_APPLICATION_ERROR('-20500','379',p_set_linkid);
            
        END IF;
    END IF;
    --if sets are already mapped to  system ,then raise error so that user can void them and then link then
    --we use bulk collect bcoz if set are shared in multiple system then we will have multiple row and we need
    -- to list all the set id in error message.
     SELECT c207_main_set_id BULK COLLECT
       INTO v_exist_sys_ids
       FROM t207a_set_link
      WHERE c901_type       <> 20004
        AND c207_link_set_id = p_set_linkid;
    FOR exist_sys           IN 1 .. v_exist_sys_ids.COUNT
    LOOP
        v_exist_sys := v_exist_sys ||','|| REPLACE (get_set_name (v_exist_sys_ids (exist_sys)),
        '- Additional Inventory') ;
    END LOOP;
    IF v_exist_sys IS NOT NULL AND p_action = 'create' THEN
    GM_RAISE_APPLICATION_ERROR('-20500','380',p_set_linkid||'#$#'||v_exist_sys);
        
    END IF;
     SELECT COUNT (1)
       INTO v_ai_set_count
       FROM t207_set_master
      WHERE c207_set_id = v_child_set_id;
    IF v_ai_set_count   = 0 THEN
        gm_sav_ai_set (p_set_linkid, p_type, p_set_type, p_division_id, p_company_id) ;
        v_child_set_id     := 'AI.' || p_set_linkid;
        v_existing_link_fl := 'N';
    END IF;
    -- Map the main and AI set
    gm_sav_set_link_mappings (p_main_setid, p_set_linkid, v_child_set_id, p_type, p_set_type, v_existing_link_fl,
    p_shared) ;
END gm_save_validate_set_link;
/*****************************************************************************************
* Description : This procedure is creat mappings for parent and itz AI child sets.
* Eg: for 965.905 and its Additional Inventory AI.XXXX
*******************************************************************************************/
PROCEDURE gm_sav_set_link_mappings (
        p_main_setid   IN t207a_set_link.c207_main_set_id%TYPE,
        p_set_linkid   IN t207a_set_link.c207_link_set_id%TYPE,
        p_child_set_id IN t207a_set_link.c207_link_set_id%TYPE,
        p_type         IN t207a_set_link.c901_type%TYPE,
        p_set_type     IN t207_set_master.c207_type%TYPE,
        p_existing_fl  IN VARCHAR2,
        p_shared       IN t207a_set_link.c901_shared_status%type)
AS
    v_shared t207a_set_link.c901_shared_status%type := NULL;
    v_child_set_id t207a_set_link.c207_main_set_id%TYPE;
    v_child_setid t207a_set_link.c207_main_set_id%TYPE;
BEGIN
    v_child_setid := p_child_set_id;
    IF (p_type     = 20002 OR p_type = 20003) THEN
        v_shared  := p_shared;
    END IF;
    gm_insert_set_link (p_main_setid, p_set_linkid, p_type, p_shared) ;
    IF p_set_type         = 4070 THEN
        IF p_existing_fl <> 'Y' THEN
            -- If the link exist for this set then donot recreate, if will exist if the set is edited(flipped from
            -- Additional to Standard or Vice Versa) and as per Joe we should not delete record if type is 20004
            BEGIN
                 SELECT c207_link_set_id
                   INTO v_child_set_id
                   FROM t207a_set_link
                  WHERE c207_main_set_id = p_set_linkid
                    AND c901_type        = 20004;
                v_child_setid           := v_child_set_id;-- bocz if AI. exist then use the same AI. set for all links
                -- else link new AI.<SETID> that are passed to proc
            EXCEPTION
            WHEN NO_DATA_FOUND THEN
                gm_insert_set_link (p_set_linkid, v_child_setid, 20004, NULL) ; --NULL Shared
            END;
        END IF;
        IF (p_type = 20002) THEN
            gm_insert_set_link ('SET.' || (SUBSTR (p_main_setid, 5)), v_child_setid, 20003, NULL--Shared
            ) ;
        ELSE
            gm_insert_set_link ('AAE.' || (SUBSTR (p_main_setid, 5)), v_child_setid, 20005, NULL--Shared
            ) ;
        END IF;
    END IF;
END gm_sav_set_link_mappings;
/*****************************************************************************************
* Description : This procedure is insert mappings in the set link table
*******************************************************************************************/
PROCEDURE gm_insert_set_link (
        p_main_setid IN t207a_set_link.c207_main_set_id%TYPE,
        p_set_linkid IN t207a_set_link.c207_link_set_id%TYPE,
        p_type       IN t207a_set_link.c901_type%TYPE,
        p_shared     IN t207a_set_link.c901_shared_status%type)
AS
BEGIN
     INSERT
       INTO t207a_set_link
        (
            t207a_set_link_id, c207_main_set_id, c207_link_set_id
          , c901_type, c901_shared_status
        )
        VALUES
        (
            s207a_set_link.NEXTVAL, p_main_setid, p_set_linkid
          , p_type, DECODE (p_shared, '0', NULL, p_shared)
        ) ;
END gm_insert_set_link;
/*****************************************************************************************
* Description : This procedure is update record in set master table
*******************************************************************************************/
PROCEDURE gm_upt_set_master
    (
        p_set_id       IN t207_set_master.c207_set_id%TYPE,
        p_user_id      IN t205_part_number.c205_last_updated_by%TYPE,
        p_systemid     IN t207_set_master.c207_set_sales_system_id%TYPE,
        p_minset_logic IN t207_set_master.c901_minset_logic%TYPE,
        p_hierarchy    IN t207_set_master.c901_hierarchy%TYPE,
        p_baseline_fl  IN VARCHAR2,
        p_void_fl      IN t207_set_master.c207_void_fl%TYPE
    )
AS
BEGIN
     UPDATE t207_set_master t207
    SET t207.c901_cons_rpt_id                            = DECODE (p_baseline_fl, 'Y', 20100, 20101), t207.c901_hierarchy = DECODE (p_hierarchy,
        NULL, NULL, p_hierarchy), t207.c901_minset_logic = DECODE (p_minset_logic, NULL, NULL, p_minset_logic),
        t207.c207_set_sales_system_id                    = DECODE (p_systemid, NULL, NULL, p_systemid),
        t207.c207_void_fl                                = DECODE (t207.c207_void_fl, 'Y', 'Y', DECODE (p_void_fl, 'Y',
        'Y', p_void_fl)), --2 decode bcoz IF a set that is already voided from some other
        -- screen should always remain as voided
        t207.c207_last_updated_date = CURRENT_DATE, t207.c207_last_updated_by = p_user_id
      WHERE t207.c207_set_id        = p_set_id;
END gm_upt_set_master;
/*****************************************************************************************
* Description : This procedure is update record in set detail table
*This procedure will also assign parts to common pool if a set is removed from a system.
*******************************************************************************************/
PROCEDURE gm_upt_set_details (
        p_max_set      IN t208_set_details.c207_set_id%TYPE,
        p_set_id       IN t208_set_details.c207_set_id%TYPE,
        p_unassign_sys IN t208_set_details.c207_set_id%TYPE,
        p_user_id      IN t205_part_number.c205_last_updated_by%TYPE)
AS
BEGIN
     UPDATE t208_set_details
    SET c207_set_id              = p_max_set, c208_last_updated_by = p_user_id, c208_last_updated_date = CURRENT_DATE
      WHERE c205_part_number_id IN
        (
             SELECT c205_part_number_id
               FROM t208_set_details
              WHERE c207_set_id = p_set_id
        )
        AND c207_set_id = p_unassign_sys;
        
        --Updating the System from which we are moving parts to a new System. Hence both the System will be udpated,so data will be synced properly.
        UPDATE t207_set_master SET c207_last_updated_by = p_user_id, c207_last_updated_date = CURRENT_DATE
        WHERE c207_set_id IN (p_max_set, p_unassign_sys,p_set_id);
END gm_upt_set_details;
/*****************************************************************************************
* Description : This procedure is to create a new AI Set, if it wasnt there already
*******************************************************************************************/
PROCEDURE gm_sav_ai_set (
        p_set_linkid  IN t207a_set_link.c207_link_set_id%TYPE,
        p_type        IN t207a_set_link.c901_type%TYPE,
        p_set_type    IN t207_set_master.c207_type%TYPE,
        p_division_id IN t207_set_master.c1910_division_id%TYPE,
        p_company_id  IN t207_set_master.c1900_company_id%TYPE)
AS
    v_set_name t207_set_master.c207_set_nm%TYPE;
    v_user_id t205_part_number.c205_last_updated_by%TYPE;
    v_ai_set_count NUMBER;
    v_hierarchy_type t207_set_master.c901_hierarchy%TYPE;
    -- v_set_desc         t207_set_master.c207_set_desc%TYPE;
    --  v_set_dtl_desc      t207_set_master.c207_set_dtl_desc%TYPE;
BEGIN
    IF p_set_type = 4070 THEN
        BEGIN
             SELECT c207_set_nm--,c207_set_desc,c207_set_dtl_desc
               INTO v_set_name--,v_set_desc,v_set_dtl_desc
               FROM t207_set_master
              WHERE c207_set_id   = p_set_linkid
                AND c207_void_fl IS NULL;
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            --DBMS_OUTPUT.put_line (' Set ' || p_set_linkid || ' doesnt exist in set master ') ;
             GM_RAISE_APPLICATION_ERROR('-20500','381',p_set_linkid);
           
        END;
    ELSE -- If this is loaner, dont need to create AI sets
        RETURN;
    END IF;
    -- Since there was no prior mapping, no data found. so insert
    -- Create an AI Set
    IF (p_type            = 20002) THEN
        v_hierarchy_type := 20704;
    ELSE
        v_hierarchy_type := 20702;
    END IF;
     SELECT COUNT (1)
       INTO v_ai_set_count
       FROM t207_set_master
      WHERE c207_set_id = 'AI.' || p_set_linkid;
    IF (v_ai_set_count  = 0) THEN
        gm_sav_set_master ('AI.' || p_set_linkid, v_set_name, v_set_name || ' - Additional Inventory', v_user_id, 4060,
        1603, 53, NULL, v_hierarchy_type, NULL, p_division_id, p_company_id ) ;
    END IF;
    -- DBMS_OUTPUT.put_line (' Created ' || 'AI.' || p_set_linkid || ' in set master ') ;
END gm_sav_ai_set;
/*****************************************************************************************
* Description : This procedure is to edit links when a set is edited
*******************************************************************************************/
PROCEDURE gm_sav_edit_linked_set (
        p_main_set_id   IN t207a_set_link.c207_main_set_id%type,
        p_linked_set_id IN t207a_set_link.c207_link_set_id%type,
        p_type          IN t207a_set_link.c901_type%TYPE,
        p_shared        IN t207a_set_link.c901_shared_status%type,
        p_hierarcy_type IN t207_set_master.c901_hierarchy%type,
        p_set_type      IN t207_set_master.c207_type%TYPE,
        p_division_id   IN t207_set_master.c1910_division_id%TYPE,
        p_company_id    IN t207_set_master.c1900_company_id%TYPE)
AS
    v_child_set_id t207_set_master.c207_set_id%TYPE;
    v_change_settype VARCHAR2 (2) := 'N';
    v_old_hierarcy_tp t207_set_master.c901_hierarchy%type;
    v_action VARCHAR2 (10) := 'edit';
    v_207_set_type t207_set_master.c207_type%TYPE;
BEGIN
     SELECT c901_hierarchy, c207_type
       INTO v_old_hierarcy_tp, v_207_set_type
       FROM t207_set_master
      WHERE c207_set_id = p_linked_set_id;
    --4071 p_set_type is used as loaner
    IF v_207_set_type <> 4074 AND v_207_set_type <> 4070 AND p_set_type = 4071 THEN
    GM_RAISE_APPLICATION_ERROR('-20500','382',p_linked_set_id);
        
    END IF;
    IF v_old_hierarcy_tp <> p_hierarcy_type THEN
        gm_sav_del_linked_set (p_main_set_id, p_linked_set_id, v_old_hierarcy_tp, v_change_settype) ;
    END IF;
    IF p_hierarcy_type = 20700--Main Set
        THEN
        IF v_change_settype = 'Y' THEN
            gm_sav_set_link (p_main_set_id, p_linked_set_id, 20002, p_set_type, p_shared, p_hierarcy_type, v_action, p_division_id, p_company_id) ;
        ELSE
             UPDATE t207a_set_link
            SET C901_SHARED_STATUS   = DECODE (p_shared, '0', NULL, p_shared)
              WHERE c207_link_set_id = p_linked_set_id
                AND C901_TYPE        = p_type--20002 Sales Set Mapping
                AND c207_main_set_id = p_main_set_id;
        END IF;
    END IF;
    IF p_hierarcy_type = 20702--Additional Inventory
        THEN
        IF v_change_settype = 'Y' THEN
            gm_sav_set_link ('SET.' || SUBSTR (p_main_set_id, 5), p_linked_set_id, 20003, p_set_type, p_shared,
            p_hierarcy_type, v_action, p_division_id, p_company_id) ;
        ELSE
             UPDATE t207a_set_link
            SET C901_SHARED_STATUS    = DECODE (p_shared, '0', NULL, p_shared)
              WHERE c207_link_set_id  = p_linked_set_id
                AND C901_TYPE         = p_type --20003 Set Mapping with Main
                AND c207_main_set_id IN
                (
                     SELECT c207_link_set_id
                       FROM t207a_set_link
                      WHERE c207_main_set_id = p_main_set_id
                ) ;
        END IF;
    END IF;
END gm_sav_edit_linked_set;
/*****************************************************************************************
* Description : Common Proc to delete sets links
*******************************************************************************************/
PROCEDURE gm_sav_del_linked_set (
        p_main_set_id       IN t207a_set_link.c207_main_set_id%type,
        p_linked_set_id     IN t207a_set_link.c207_link_set_id%type,
        p_old_hierarcy_type IN t207_set_master.c901_hierarchy%type,
        p_out_change_settype OUT VARCHAR2)
AS
    v_child_set_id t207_set_master.c207_set_id%TYPE;
    v_change_settype VARCHAR2 (1) := 'N';
BEGIN
    BEGIN
         SELECT c207_link_set_id
           INTO v_child_set_id
           FROM t207a_set_link
          WHERE c207_main_set_id = p_linked_set_id
            AND c901_type        = 20004;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_child_set_id := NULL;
    END;
    IF (p_old_hierarcy_type = 20702) --20702 Additional Inventory Set
        THEN
        --delete links for 20002 Sales Set Mapping/20003 Set Mapping with Main
         DELETE
           FROM t207a_set_link
          WHERE c901_type        <> 20004
            AND (c207_link_set_id = p_linked_set_id
            OR c207_link_set_id   = v_child_set_id)
            AND c901_type        IN (20003, 20005) ;
        p_out_change_settype     := 'Y';
    ELSE
         DELETE
           FROM t207a_set_link
          WHERE c901_type          <> 20004
            AND ( (c207_link_set_id = p_linked_set_id
            AND c207_main_set_id    = p_main_set_id)
            OR c207_link_set_id     = v_child_set_id)
            AND c901_type          IN (20002, 20003) ;
        p_out_change_settype       := 'Y';
    END IF;
END gm_sav_del_linked_set;
/*****************************************************************************************
* Description : This procedure is to void a system
*******************************************************************************************/
PROCEDURE gm_void_system (
        p_sys_id  IN t207_set_master.c207_set_id%type,
        p_user_id IN t207_set_master.c207_created_by%type)
AS
    CURSOR allsets_cur
    IS
        --Use v207a_set_consign_link in this cursor and not this SQL
         SELECT t207a.c207_main_set_id parent_set_id, t207a.c207_link_set_id child_set_id, c901_shared_status
           FROM t207a_set_link t207a
            START WITH c207_main_set_id             = '300.037'
            CONNECT BY PRIOR t207a.c207_link_set_id = c207_main_set_id
            AND t207a.c901_type                    <> 20004;--Additional Items MAPPING
    v_setgrp t207_set_master.c901_set_grp_type%TYPE;
BEGIN
    FOR allsets IN allsets_cur
    LOOP
         DELETE
           FROM t207a_set_link
          WHERE c207_link_set_id = allsets.child_set_id
            AND c207_main_set_id = allsets.parent_set_id;
         SELECT c901_set_grp_type
           INTO v_setgrp
           FROM t207_set_master t207
          WHERE t207.c207_set_id = allsets.child_set_id;
        IF v_setgrp             <> 1601--Set Report Group
            THEN
            gm_upt_set_master (allsets.child_set_id, p_user_id, NULL, NULL, NULL, NULL, 'Y') ;
        ELSE
            gm_upt_set_master (allsets.child_set_id, p_user_id, NULL, NULL, NULL, NULL, NULL) ;
        END IF;
    END LOOP;
END gm_void_system;
/*****************************************************************************************
* Description : This procedure is to void/unlink  a set mapped in a system
*******************************************************************************************/
PROCEDURE gm_void_system_map (
        p_inputstr VARCHAR2,
        p_user_id IN t207_set_master.c207_created_by%type)
AS
    v_cnt          NUMBER;
    v_inputstr_len NUMBER          := NVL (LENGTH (p_inputstr), 0) ;
    v_inputstr_str VARCHAR2 (4000) := p_inputstr ;
    v_substring    VARCHAR2 (4000) ;
    v_sys_id t207_set_master.c207_set_id%type;
    v_set_id t207_set_master.c207_set_id%type;
    v_hierarcy_tp t207_set_master.c901_hierarchy%type;
    v_change_settype VARCHAR2 (1) := 'N';
    v_set_cnt  NUMBER;
    v_msg VARCHAR2 (4000) ;
BEGIN
    IF v_inputstr_len                      > 0 THEN
        WHILE INSTR (v_inputstr_str, ',') <> 0
        LOOP
            v_sys_id       := NULL;
            v_set_id       := NULL;
            v_sys_id       := SUBSTR (v_inputstr_str, 1, INSTR (v_inputstr_str, ',') - 1) ;
            v_set_id       := SUBSTR (v_inputstr_str, INSTR (v_inputstr_str, ',')    + 1) ;
            v_inputstr_str := NULL;
             SELECT c901_hierarchy
               INTO v_hierarcy_tp
               FROM t207_set_master
              WHERE c207_set_id = v_set_id;
            --Delete links
            gm_sav_del_linked_set (v_sys_id, v_set_id, v_hierarcy_tp, v_change_settype) ;
            --unlink from set master
            gm_upt_set_master (v_set_id, p_user_id, NULL, NULL, NULL, NULL, NULL) ;
            --Move partsto common pool
            gm_upt_set_details ('300.026', v_set_id,v_sys_id, p_user_id) ;
        END LOOP;
         --Fetch system set map count from get_system_set_map_cnt function  in PMT-37488
         v_set_cnt := gm_pkg_pd_system_rpt.get_system_set_map_cnt(v_sys_id);
       --Unpublish if count is zero (PMT-37488)
        IF v_set_cnt = 0 THEN
           gm_pkg_qa_system_publish_trans.gm_sav_system_publish_dtls(v_sys_id,108661, p_user_id,v_msg) ;--108661 unpublish
        END IF;
    END IF;
END gm_void_system_map;
/**************************************************************
* Description : Procedure to save set attributes details
* Author  : Elango
***************************************************************/
PROCEDURE gm_sav_set_attribute (
        p_setid             IN t207c_set_attribute.c207_set_id%TYPE,
        p_setattrInputstr   IN CLOB,
        p_attrtype_inputstr IN CLOB,
        p_userid            IN t207c_set_attribute.c207c_created_by%TYPE)
AS
    /*
    * This procedure is used save project attributes.
    * In this procedure we are passing project Id and 2 input strings
    * p_projattrInputstr for Project Attributes.
    * p_attrtype_inputstr for Attributes values related to attributes type
    * Here we are passing p_attrtype_inputstr and put into my_context.set_my_inlist_ctx which
    * will give all the atttributes one by one and after that we are going to void data
    * belogs to that types.
    * Now we will split p_projattrInputstr which is original sting passed form sreen and we
    * will do update and insert for that values according to types.
    */
    v_strlen    NUMBER          := NVL (LENGTH (p_setattrInputstr), 0) ;
    v_string    VARCHAR2 (4000) := p_setattrInputstr;
    v_substring VARCHAR2 (4000) ;
    v_attr_type t207c_set_attribute.c901_attribute_type%TYPE;
    v_attr_value t207c_set_attribute.c207c_attribute_value%TYPE;
    v_old_pub_pc NUMBER := 0;
    v_new_pub_pc NUMBER := 0;
    v_old_cnt NUMBER := 0;
    v_new_cnt NUMBER := 0;
    v_old_void_cnt NUMBER := 0;
    v_new_void_cnt NUMBER := 0;    
BEGIN
    my_context.set_my_inlist_ctx (p_attrtype_inputstr) ;
    -- Check if the system is Released for PC .
     SELECT COUNT (1)
       INTO v_old_pub_pc
       FROM t207c_set_attribute
      WHERE c901_attribute_type   = '103119'
        AND c207c_attribute_value = '103085'
        AND C207_set_id           = p_setid
        AND C207c_void_fl        IS NULL;
        
     SELECT COUNT (1),COUNT (c207c_void_fl)
       INTO v_old_cnt,v_old_void_cnt
       FROM t207c_set_attribute
      WHERE C207_set_id           = p_setid;
        
     UPDATE t207c_set_attribute
    SET C207C_VOID_FL            = 'Y', C207C_LAST_UPDATED_DATE = CURRENT_DATE, C207C_LAST_UPDATED_BY = p_userid
      WHERE C207_SET_ID          = p_setid
        AND C901_ATTRIBUTE_TYPE IN
        (
             SELECT token FROM v_in_list
        )
        AND C207C_VOID_FL       IS NULL;
    WHILE INSTR (v_string, '|') <> 0
    LOOP
        --
        v_attr_type  := NULL;
        v_attr_value := NULL;
        v_substring  := SUBSTR (v_string, 1, INSTR (v_string, '|')       - 1) ;
        v_string     := SUBSTR (v_string, INSTR (v_string, '|')          + 1) ;
        v_attr_type  := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        v_substring  := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        v_attr_value := v_substring;
        gm_pkg_pd_system_trans.gm_sav_set_attr (p_setid, v_attr_type, v_attr_value, p_userid) ;
    END LOOP;
    -- Check if the system is Released for PC .
     SELECT COUNT (1)
       INTO v_new_pub_pc
       FROM t207c_set_attribute
      WHERE c901_attribute_type   = '103119'
        AND c207c_attribute_value = '103085'
        AND C207_set_id           = p_setid
        AND C207c_void_fl        IS NULL;
        
     SELECT COUNT (1),COUNT (c207c_void_fl)
       INTO v_new_cnt,v_new_void_cnt
       FROM t207c_set_attribute
      WHERE C207_set_id           = p_setid;
        
    --If the System previously was not published to PC, but now if its published, this condition will satisfy
    IF (v_old_pub_pc = 0 AND v_new_pub_pc > 0) THEN
        gm_pkg_pdpc_prodcattxn.gm_sav_new_system_update (p_setid, p_userid, '103122') ; -- New
        -- This condition will satisfy if they want to pull back a system that was released for PC earlier.
    ELSIF (v_old_pub_pc > 0 AND v_new_pub_pc = 0) THEN
        gm_pkg_pdpc_prodcattxn.gm_sav_new_system_update (p_setid, p_userid, '4000412') ; -- Void
    END IF;
    
    --If the system attribute is updated, this condition will satisfy
    --'4000416' - System attribute ref type, '103123' - update, '103097' - English,'103100' - product catalog 
    IF v_old_cnt <> v_new_cnt OR  v_old_void_cnt <> v_new_void_cnt THEN
    		gm_pkg_pdpc_prodcattxn.gm_sav_master_data_upd (p_setid, '4000416', '103123', '103097',
                                                  '103100', p_userid
                                                  , p_setid
                                                  ,'' );
	END IF;                               
END gm_sav_set_attribute;
/**************************************************************
* Description : Procedure to save set(System) attributes details
* Author  : Elango
***************************************************************/
PROCEDURE gm_sav_set_attr (
        p_setid      IN t207c_set_attribute.c207_set_id%TYPE,
        p_attr_type  IN t207c_set_attribute.c901_attribute_type%TYPE,
        p_attr_value IN t207c_set_attribute.c207c_attribute_value%TYPE,
        p_userid     IN t207c_set_attribute.c207c_created_by%TYPE)
AS
BEGIN
     UPDATE t207c_set_attribute
    SET c207c_void_fl             = NULL, c207c_last_updated_date = CURRENT_DATE, c207c_last_updated_by = p_userid
      WHERE c207_set_id           = p_setid
        AND c901_attribute_type   = p_attr_type
        AND c207c_attribute_value = p_attr_value;
    IF (SQL%ROWCOUNT              = 0) THEN
         INSERT
           INTO t207c_set_attribute
            (
                C207C_SET_ATTRIBUTE_ID, C207_SET_ID, C901_ATTRIBUTE_TYPE
              , C207C_ATTRIBUTE_VALUE, C207C_VOID_FL, C207C_CREATED_BY
              , C207C_CREATED_DATE, C207C_LAST_UPDATED_BY, C207C_LAST_UPDATED_DATE
              , C207C_SET_HISTORY_FL
            )
            VALUES
            (
                s207c_set_attribute.nextval, p_setid, p_attr_type
              , p_attr_value, NULL, p_userid
              , CURRENT_DATE, NULL, NULL
              , NULL
            ) ;
    END IF;
END gm_sav_set_attr;
/**************************************************************
* Description : Procedure to save System company mapping details
* Author  : rdinesh
***************************************************************/
PROCEDURE gm_sav_system_company_map (
        p_setid      IN t207_set_master.c207_set_id%TYPE,
        p_company_id IN t207_set_master.c1900_company_id%TYPE,
        p_map_type   IN t2080_set_company_mapping.c901_set_mapping_type%TYPE,
        p_userid     IN t2080_set_company_mapping.c2080_last_updated_by%TYPE)
 AS
 
 		v_count NUMBER;
 
 BEGIN
 
 -- Check whether the system was already mapped with company, If it is so do not do any update else do insert.
        SELECT COUNT(1)
		   INTO v_count
		   FROM t2080_set_company_mapping
		  WHERE c207_set_id  = p_setid
		    AND c1900_company_id = p_company_id
		    AND c2080_void_fl IS NULL;
 
 		IF (v_count = 0) THEN 
		    
		
			INSERT INTO T2080_SET_COMPANY_MAPPING
			(
			    c2080_set_comp_map_id,c207_set_id,c1900_company_id,c901_set_mapping_type,c2080_last_updated_by,c2080_last_updated_date
			)
			VALUES
			(
			    S2080_SET_COMPANY_MAPPING.nextval,p_setid,p_company_id,p_map_type,p_userid,CURRENT_DATE
			);
			
--- Set bundle company map			
	       
	   		INSERT
			  INTO T2081_SET_BUNDLE_COMP_MAPPING
	 			   (C2081_SET_BUNDLE_COMP_MAP_ID,C2075_SET_BUNDLE_ID,C1900_COMPANY_ID,C901_SET_MAPPING_TYPE,C2081_LAST_UPDATED_BY,C2081_LAST_UPDATED_DATE
				   )		
			  (SELECT S2081_SET_BUNDLE_COMP_MAPPING.nextval,c2075_set_bundle_id,p_company_id,p_map_type,p_userid,CURRENT_DATE
	       	     FROM T2076_SET_BUNDLE_DETAILS
	       	    WHERE c207_set_id = p_setid
	       	      AND c2076_void_fl is null						  
				  );	
		END IF;


END gm_sav_system_company_map;
/*******************************************************
	* Description : Procedure to Void System company mapping details
	* Author	  : Rajeshwaran
	*******************************************************/
    PROCEDURE gm_void_system_company_map (
	  	p_setid      		IN t207_set_master.c207_set_id%TYPE
	  , p_company_id        IN      t2021_project_company_mapping.c1900_company_id%TYPE	  
	  , p_userid			IN		t2021_project_company_mapping.c2021_last_updated_by%TYPE
	)
	AS
	
	BEGIN
		UPDATE T2080_SET_COMPANY_MAPPING
		SET		C2080_VOID_FL='Y',
				C2080_LAST_UPDATED_BY = p_userid,
				C2080_LAST_UPDATED_DATE = CURRENT_DATE
		WHERE
				C207_SET_ID = p_setid
		AND		C1900_COMPANY_ID = p_company_id
		AND		C2080_VOID_FL IS NULL;
-- Set bundle company map		
		UPDATE t2081_set_bundle_comp_mapping    
		   SET c2081_void_fl = 'Y'
		     , c2081_last_updated_by = p_userid
		   	 , c2081_last_updated_date = current_date
		 WHERE c2075_set_bundle_id in ( SELECT c2075_set_bundle_id 
		   							      FROM t2076_set_bundle_details
		   							     WHERE c207_set_id = p_setid
		   							       AND c2076_void_fl is null  )
		   AND c901_set_mapping_type != 105360		-- Created
		   AND c1900_company_id = p_company_id					    
           AND c2081_void_fl IS NULL;	
		
	END gm_void_system_company_map;
	
	
/**************************************************************
* Description : Procedure to save System company mapping details
* Author  :
***************************************************************/
PROCEDURE gm_sav_system_company_map(
    p_setid      IN t207_set_master.c207_set_id%TYPE,
    p_company_id IN t207_set_master.c1900_company_id%TYPE,
    p_map_type   IN t2080_set_company_mapping.c901_set_mapping_type%TYPE,
    p_statusId   IN t2080_set_company_mapping.c901_ipad_released_status%TYPE,
    p_userid     IN t2080_set_company_mapping.c2080_last_updated_by%TYPE)
AS
BEGIN
  -- update set mapping type if company status is not equal to created .
  UPDATE t2080_set_company_mapping
  SET c901_set_mapping_type   = DECODE(p_map_type,105360,c901_set_mapping_type,p_statusId),
    c2080_last_updated_by     = p_userid,
    c2080_last_updated_date   = CURRENT_DATE,
    c901_ipad_released_status = p_statusId
  WHERE c207_set_id           = p_setid
  AND c1900_company_id        = p_company_id
  AND c2080_void_fl          IS NULL;
  
  IF (SQL%ROWCOUNT = 0) THEN
    INSERT
    INTO T2080_SET_COMPANY_MAPPING
      (
        c2080_set_comp_map_id,
        c207_set_id,
        c1900_company_id,
        c901_set_mapping_type,
        c2080_last_updated_by,
        c2080_last_updated_date,
        c901_ipad_released_status
      )
      VALUES
      (
        S2080_SET_COMPANY_MAPPING.nextval,
        p_setid,
        p_company_id,
        p_map_type,
        p_userid,
        CURRENT_DATE,
        p_statusId
      );
    --- Set bundle company map
    INSERT
    INTO T2081_SET_BUNDLE_COMP_MAPPING
      (
        C2081_SET_BUNDLE_COMP_MAP_ID,
        C2075_SET_BUNDLE_ID,
        C1900_COMPANY_ID,
        C901_SET_MAPPING_TYPE,
        C2081_LAST_UPDATED_BY,
        C2081_LAST_UPDATED_DATE
      )
      (SELECT S2081_SET_BUNDLE_COMP_MAPPING.nextval,
          c2075_set_bundle_id,
          p_company_id,
          p_map_type,
          p_userid,
          CURRENT_DATE
        FROM T2076_SET_BUNDLE_DETAILS
        WHERE c207_set_id  = p_setid
        AND c2076_void_fl IS NULL
      );
  END IF;
  
END gm_sav_system_company_map;

END gm_pkg_pd_system_trans;
/
