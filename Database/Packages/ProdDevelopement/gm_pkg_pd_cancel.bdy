/* Formatted on 2009/02/16 12:30 (Formatter Plus v4.8.0) */
--@"C:\database\Packages\ProdDevelopement\gm_pkg_pd_cancel.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_pd_cancel
IS
	/*******************************************************
	 * Description : Procedure to Session
	*******************************************************/
	PROCEDURE gm_pd_sav_void_staffingdtl (
		p_txn_id   IN	t907_cancel_log.c907_ref_id%TYPE
	  , p_userid   IN	t102_user_login.c102_user_login_id%TYPE
	)
	AS
	BEGIN
		UPDATE t204_project_team
		   SET c204_void_fl = 'Y'
			 , c204_last_updated_by = p_userid
			 , c204_last_updated_date = SYSDATE
		 WHERE c204_project_team_id = p_txn_id;
	END gm_pd_sav_void_staffingdtl;
  
  
  /*******************************************************
	* Description : Procedure to Void Material Request
	*******************************************************/
	PROCEDURE gm_mf_sav_void_mr (
		p_txn_id   IN	t303_material_request.c303_material_request_id%TYPE
	  , p_userid   IN	t102_user_login.c102_user_login_id%TYPE
	)
	AS
		v_status	   t303_material_request.c303_status_fl%TYPE;
		v_dhr_id	   t408_dhr.c408_dhr_id%TYPE;
		v_count	   NUMBER;
	BEGIN
		SELECT	   c303_status_fl
			  INTO v_status
			  FROM t303_material_request
			 WHERE c303_material_request_id = p_txn_id AND c303_void_fl IS NULL
		FOR UPDATE;

		IF v_status > 20
		THEN
			raise_application_error (-20333, '');
		END IF;

		UPDATE t303_material_request
		   SET c303_void_fl = 'Y'
			 , c303_last_updated_by = p_userid
			 , c303_last_updated_date = SYSDATE
		 WHERE c303_material_request_id = p_txn_id;
		 
		 SELECT c408_dhr_id
		       INTO v_dhr_id
               FROM t303_material_request
               WHERE c303_material_request_id = p_txn_id;
               
         SELECT COUNT(1)  
               INTO v_count
               FROM t303_material_request
               WHERE c408_dhr_id = v_dhr_id
               AND c303_void_fl IS NULL;
               
          IF v_count = 0
           THEN
          UPDATE t408_dhr
               SET c408_void_fl  ='Y'
               , c408_last_updated_by = p_userid
			   , c408_last_updated_date = SYSDATE
               WHERE c408_dhr_id = v_dhr_id
               AND c408_void_fl IS NULL;
          END IF;
	END gm_mf_sav_void_mr;
  
  /*******************************************************
	* Description : Procedure to rollback Material Request
	*******************************************************/
	PROCEDURE gm_mf_sav_rollback_mr (
		p_txn_id   IN	t303_material_request.c303_material_request_id%TYPE
	  , p_userid   IN	t102_user_login.c102_user_login_id%TYPE
	)
	AS
		v_status	   t303_material_request.c303_status_fl%TYPE;
	BEGIN
		SELECT	   c303_status_fl
			  INTO v_status
			  FROM t303_material_request
			 WHERE c303_material_request_id = p_txn_id AND c303_void_fl IS NULL
		FOR UPDATE;

		IF v_status <> 30
		THEN
			raise_application_error (-20334, '');
		END IF;

		UPDATE t303_material_request
		   SET c303_status_fl = 20
			 , c303_last_updated_by = p_userid
			 , c303_last_updated_date = SYSDATE
		 WHERE c303_material_request_id = p_txn_id AND c303_void_fl IS NULL;
	END gm_mf_sav_rollback_mr;
  
END gm_pkg_pd_cancel;
/
