/* Formatted on 2019/21/10 18:43 (Formatter Plus v4.8.0) */
--@"C:\database\Packages\ProdDevelopement\gm_pkg_qa_system_publish_rpt.bdy"
/*******************************************************
* Description : Package for System Details
*******************************************************/
CREATE OR REPLACE
PACKAGE BODY gm_pkg_qa_system_publish_rpt
IS
  /*******************************************************
  * Author  : T.S. Ramachandiran
  * Description: This Procedure is used to fetch
  * the system release details
  *******************************************************/
	PROCEDURE gm_fch_system_release_dtls(
		p_set_id          IN      T2082_SET_RELEASE_MAPPING.c207_set_id%TYPE,
		p_group_type      IN      T2082_SET_RELEASE_MAPPING.C901_SET_GROUP_TYPE%TYPE,
    	p_out_system_dtls OUT     VARCHAR2)
	AS
 	 v_date_fmt VARCHAR2 (20) ;
	BEGIN
  		SELECT get_compdtfmt_frm_cntx () INTO v_date_fmt FROM dual;
  		SELECT JSON_ARRAYAGG(
 		JSON_OBJECT 
  		('entity_id' value t901.c901_code_id,
   		 'entity_name' value t901.c901_code_nm,
   		 'status_id' value t2082.c901_status_id,
   		 'set_count' value gm_pkg_qa_system_publish_rpt.get_system_set_map_by_region(p_set_id,t901.c901_code_id,p_group_type),
   		 'system_status' value get_code_name(t2082.c901_status_id),
   		 'release_history' value t2082.c2082_status_history_fl,
   		 'updated_by' value get_user_name(t2082.c2082_updated_by),
   		 'updated_date' value NVL(TO_CHAR(t2082.c2082_updated_date, v_date_fmt||' HH:MI:SS AM'),' '))
   		ORDER BY c901_code_id ASC returning CLOB)
  		INTO p_out_system_dtls
  		FROM t901_code_lookup t901,
  		t2082_set_release_mapping t2082
  		WHERE t901.c901_code_id= t2082.c901_entity_id(+)
  		AND t901.c901_code_grp='QUAREG'
		AND t2082.c207_set_id(+)    = p_set_id
		AND t901.c901_active_fl = 1
		AND t2082.C2082_VOID_FL is null
		AND T901.C901_VOID_FL is null;
	END gm_fch_system_release_dtls;

 /********************************************************************************
  * Author      : T.S. Ramachandiran
  * Description : This Function is used to return count of system's mapped to the Region
  ********************************************************************************/

	FUNCTION get_system_set_map_by_region(
		p_set_id       IN  T2082_SET_RELEASE_MAPPING.c207_set_id%TYPE,
		p_region_id    IN  T2082_SET_RELEASE_MAPPING.c901_entity_id%TYPE,
		p_set_group_type IN  T2082_SET_RELEASE_MAPPING.C901_SET_GROUP_TYPE%TYPE)
	RETURN NUMBER
	IS
		v_set_count NUMBER;
	BEGIN
		--
		
		SELECT count(1) INTO v_set_count
		FROM t207_set_master t207,
  		t207a_set_link t207a,
  		t2082_set_release_mapping t2082
		WHERE t207.c207_set_sales_system_id = p_set_id
		AND t2082.c901_entity_id            = p_region_id
		AND t207.c207_set_id                = t207a.c207_link_set_id
		AND t2082.C207_SET_ID               = t207.C207_SET_ID
		AND t2082.C901_STATUS_ID            = 105361 -- Released
		AND t207.c207_void_fl               IS NULL
		AND t2082.c901_set_group_type       = 1601
		AND t2082.c2082_void_fl is null;
		
     	RETURN v_set_count;
	END get_system_set_map_by_region;
	
  /*******************************************************
  * Author  : N RAJA
  * Description: This Procedure is used to fetch system set mapped 
  *******************************************************/
  PROCEDURE gm_fch_system_set_mapping_dtls(
		p_set_id           IN      t207_set_master.C207_SET_SALES_SYSTEM_ID%TYPE,
    	p_region_id        IN      t2082_set_release_mapping.c901_entity_id%TYPE,
    	p_group_type       IN      t2082_set_release_mapping.c901_set_group_type%TYPE,
		p_out_system_dtls  OUT    CLOB)
	AS
	BEGIN
         SELECT JSON_ARRAYAGG(
                JSON_OBJECT 
		          ('SETID'            VALUE  t207.c207_set_id,
		           'SETNM'            VALUE  t207.c207_set_nm,
		    	   'PROJECTNM'        VALUE  t202.c202_project_nm,
		           'SETTYPE'          VALUE  get_code_name(DECODE (t207.c207_type,4074, DECODE (t207.c901_hierarchy, 20700, '103148', '103150'), DECODE (t207.c901_hierarchy, 20700, '103147', '103149'))),  
		           'BASELINESET'      VALUE  DECODE (t207.c901_cons_rpt_id, 20100,'Yes', 'No'),    -- 20100 - Full Set 103149 - Additional, 103147-Standard, 103148 - Loaner Standard, 103150- Loaner Additional
		           'SHARED'           VALUE  get_code_name(t207a.c901_shared_status) 
		          )RETURNING CLOB) INTO p_out_system_dtls
        FROM t207_set_master t207,
		       t207a_set_link t207a,
		       t2082_set_release_mapping t2082,
		       t202_project t202
	   WHERE t207.c207_set_sales_system_id = p_set_id     
		 AND t2082.c901_entity_id = p_region_id     
		 AND t207.c207_set_id  = t207a.c207_link_set_id
		 AND t2082.c207_set_id = t207.c207_set_id
		 AND t207.c202_project_id = t202.c202_project_id
		 AND t2082.c901_status_id  = 105361    -- Released
		 AND t207.c207_void_fl  IS NULL
		 AND t2082.c901_set_group_type =  p_group_type  
		 AND t2082.c2082_void_fl IS NULL
		 AND t202.c202_void_fl IS NULL;
   END gm_fch_system_set_mapping_dtls;
		   
END gm_pkg_qa_system_publish_rpt;
/