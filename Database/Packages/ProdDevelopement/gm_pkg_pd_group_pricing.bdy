/* Formatted on 2010/07/12 10:58 (Formatter Plus v4.8.0) */

--@"C:\Database\Packages\ProdDevelopement\gm_pkg_pd_group_pricing.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_pd_group_pricing
IS
--
   PROCEDURE gm_pd_sav_group_price (
      p_groupid                 IN   t4010_group.c4010_group_id%TYPE,
      p_primary_part_inputstr   IN   VARCHAR2,
      p_userid                  IN   t4010_group.c4010_created_by%TYPE
   )
   AS
      v_strlen_primary_part      NUMBER
                                 := NVL (LENGTH (p_primary_part_inputstr), 0);
      v_primary_part_string      VARCHAR2 (4000)   := p_primary_part_inputstr;
      v_primary_part_substring   VARCHAR2 (1000);
      v_pnum                     t4011_group_detail.c205_part_number_id%TYPE;
      v_part_type                t4011_group_detail.c901_part_pricing_type%TYPE;
      v_part_primary_lock        t4011_group_detail.c4011_primary_part_lock_fl%TYPE;
      v_count                    NUMBER;
      v_primary_count            NUMBER;
      v_group_id                 VARCHAR2 (50);
      v_groupid                  VARCHAR2 (50);
      subject                    VARCHAR2 (200);
      to_mail                    VARCHAR2 (1000);
      mail_body                  VARCHAR2 (4000);
      v_owner                    VARCHAR2 (100);
      v_emailid                  VARCHAR2 (100);
      v_publish_fl               VARCHAR2 (4000);
	  v_priced_type              VARCHAR2 (4000);
	  v_list_price               NUMBER;
 	  v_loaner_price             NUMBER;
	  v_consignment_price        NUMBER;
	  v_equity_price             NUMBER;
      v_company_id               T1900_COMPANY.C1900_COMPANY_ID%TYPE;
      v_group_nm                 t4010_group.c4010_group_nm%TYPE;
      crlf                       VARCHAR2 (2)          := CHR (13)
                                                          || CHR (10);
                                                          
      CURSOR curr_group
      IS
         SELECT c4010_group_id groupid,
                get_group_name (c4010_group_id) groupname
           FROM t4011_group_detail
          WHERE c901_part_pricing_type = 52080
            AND c205_part_number_id = v_pnum;
    /*******************************************************
   * Description : Procedure to save group part info
   * Author    : Xun
   *******************************************************/
 
   BEGIN
   
   	   
      --Getting company id from context.   
    SELECT get_compid_frm_cntx()
	  INTO v_company_id 
	  FROM dual;
   
   
      IF v_strlen_primary_part > 0
      THEN
         WHILE INSTR (v_primary_part_string, '|') <> 0
         LOOP
            --
            v_primary_part_substring :=
               SUBSTR (v_primary_part_string,
                       1,
                       INSTR (v_primary_part_string, '|') - 1
                      );
            v_primary_part_string :=
               SUBSTR (v_primary_part_string,
                       INSTR (v_primary_part_string, '|') + 1
                      );
            --
            v_groupid := NULL;
            v_pnum := NULL;
            v_part_type := NULL;
            v_part_primary_lock := NULL;
            --
            v_groupid :=
               SUBSTR (v_primary_part_substring,
                       1,
                       INSTR (v_primary_part_substring, '^') - 1
                      );
            v_primary_part_substring :=
               SUBSTR (v_primary_part_substring,
                       INSTR (v_primary_part_substring, '^') + 1
                      );
            v_pnum :=
               SUBSTR (v_primary_part_substring,
                       1,
                       INSTR (v_primary_part_substring, '^') - 1
                      );
            v_primary_part_substring :=
               SUBSTR (v_primary_part_substring,
                       INSTR (v_primary_part_substring, '^') + 1
                      );
            v_part_type :=
               SUBSTR (v_primary_part_substring,
                       1,
                       INSTR (v_primary_part_substring, '^') - 1
                      );
            v_primary_part_substring :=
               SUBSTR (v_primary_part_substring,
                       INSTR (v_primary_part_substring, '^') + 1
                      );
            v_part_primary_lock := v_primary_part_substring;

            UPDATE t4011_group_detail t4011
               SET c4011_primary_part_lock_fl = v_part_primary_lock
               	   ,C4011_LAST_UPDATED_BY = p_userid
               	   ,C4011_LAST_UPDATED_DATE = CURRENT_DATE
             WHERE c4010_group_id = p_groupid AND c205_part_number_id = v_pnum;

            --check to see if part will be changed from primary to secondary
            SELECT c901_part_pricing_type
              INTO v_count
              FROM t4011_group_detail
             WHERE c4010_group_id = p_groupid AND c205_part_number_id = v_pnum;
			
            IF (v_count = 52080 AND v_part_type = 52081)
            THEN
               raise_application_error (-20197, '');
            END IF;

            IF (v_part_type = 52080 AND v_count = 52081)
            THEN
               SELECT COUNT (0)
                 INTO v_primary_count
                 FROM t4011_group_detail
                WHERE c901_part_pricing_type = 52080
                  AND c205_part_number_id = v_pnum;

               IF (v_primary_count = 1)
               THEN
                  SELECT c4010_group_id, get_group_name (c4010_group_id)
                    INTO v_group_id, v_group_nm
                    FROM t4011_group_detail
                   WHERE c901_part_pricing_type = 52080
                     AND c205_part_number_id = v_pnum;

                  BEGIN
                     SELECT get_user_name
                                (get_group_primary_owner (t4010.c4010_group_id)
                                ),
                            get_user_emailid
                                (get_group_primary_owner (t4010.c4010_group_id)
                                )
                       INTO v_owner,
                            v_emailid
                       FROM t4010_group t4010
                      WHERE t4010.c4010_group_id = v_group_id
                        AND t4010.c4010_void_fl IS NULL;
                  /* SELECT DISTINCT get_user_name (t4020.c4020_primary_user)
                                 , get_user_emailid (t4020.c4020_primary_user)
                              INTO v_owner
                                 , v_emailid
                              FROM t4020_demand_master t4020, t4021_demand_mapping t4021
                             WHERE t4021.c4021_ref_id = TO_CHAR (v_group_id)
                              AND t4020.c4020_demand_master_id = t4021.c4020_demand_master_id
                              AND t4020.c4020_void_fl IS NULL
                              AND t4020.c901_demand_type = 40020;
                  */
                  EXCEPTION
                     WHEN NO_DATA_FOUND
                     THEN
                        v_emailid := NULL;
                  END;
				
				IF v_emailid IS NOT NULL
                  THEN
                     to_mail := v_emailid;
                     subject := 'Part is changed from Primary to Secondary';
                     mail_body :=
                           ' The part '
                        || v_pnum
                        || ' pricing type is changed from Primary to Secondary in the group '
                        || v_group_nm
                        || ', Owner is '
                        || v_owner;
                     gm_com_send_email_prc (to_mail, subject, mail_body);
                  END IF;
               END IF;

               IF (v_primary_count > 1)
               THEN
                  mail_body :=
                        ' The part '
                     || v_pnum
                     || ' pricing type was Primary in more than one groups, so changed from Primary to Secondary in following groups: '
                     || crlf;

                  FOR var_group IN curr_group
                  LOOP
                     mail_body :=
                           mail_body
                        || ' Group Id:'
                        || var_group.groupid
                        || ' Group Name:'
                        || var_group.groupname
                        || crlf;
                  END LOOP;

                  to_mail := get_rule_value ('PRICINGHELPDESK', 'EMAIL');
                  subject :=
                     'Part is changed from Primary to Secondary in more than one group';
                  gm_com_send_email_prc (to_mail, subject, mail_body);
               END IF;

               UPDATE t4011_group_detail
                  SET c901_part_pricing_type = 52081
                 	 ,C4011_LAST_UPDATED_BY = p_userid
               	 	 ,C4011_LAST_UPDATED_DATE = CURRENT_DATE
                WHERE c4010_group_id <> p_groupid
                  AND c205_part_number_id = v_pnum;
                  
                  --When ever the detail is updated, the group table has to be updated ,so ETL can Sync it to GOP.
	            UPDATE t4010_group
	               SET c4010_last_updated_by = p_userid
				 	 , c4010_last_updated_date = CURRENT_DATE
				 WHERE C4010_group_id in 
				 	(SELECT c4010_group_id 
				 	FROM t4011_group_detail 
				 	WHERE c4010_group_id <> p_groupid
	               AND c205_part_number_id = v_pnum);

               UPDATE t4011_group_detail
                  SET c901_part_pricing_type = 52080
                 	  ,C4011_LAST_UPDATED_BY = p_userid
               	 	  ,C4011_LAST_UPDATED_DATE = CURRENT_DATE
                WHERE c4010_group_id = p_groupid
                  AND c205_part_number_id = v_pnum;
       
       -- Updating the price for the part in the Group (the group priced type should be Group) which primary designation is changed from
       -- Secondary to primary.            
       BEGIN
		SELECT c4010_publish_fl, c901_priced_type, gm_pkg_pd_group_pricing.get_base_group_price(c4010_group_id, 52060), gm_pkg_pd_group_pricing.get_base_group_price(c4010_group_id, 52061), gm_pkg_pd_group_pricing.get_base_group_price(c4010_group_id, 52062), gm_pkg_pd_group_pricing.get_base_group_price(c4010_group_id, 52063)
    		INTO v_publish_fl, v_priced_type, v_list_price, v_loaner_price, v_consignment_price , v_equity_price
   			FROM t4010_group
 		  WHERE c4010_group_id = p_groupid
  			AND c4010_void_fl IS NULL;
  	  EXCEPTION
    	WHEN NO_DATA_FOUND THEN
        v_list_price :=0;
        v_loaner_price:=0;
        v_priced_type:=NULL;
      END;
      
	  IF  v_priced_type=52110  AND v_list_price  <> 0 AND v_loaner_price <> 0 -- group
 	  THEN
		 
 		 UPDATE T2052_PART_PRICE_MAPPING
			SET c2052_list_price   = v_list_price   
			   ,c2052_loaner_price = v_loaner_price
               ,c2052_consignment_price  = v_consignment_price   
               ,c2052_equity_price =v_equity_price
 			   ,c2052_updated_date = CURRENT_DATE
 			   ,c2052_updated_by = p_userid
 			   , c2052_last_updated_by   =  p_userid
 			   , C2052_Last_updated_date = CURRENT_DATE
  		  WHERE c205_part_number_id = v_pnum
   		    AND c1900_company_id =  v_company_id
   		    AND C2052_VOID_FL IS NULL;
   		 
 	  END IF;

               IF (v_count = 52080)
               THEN
                  UPDATE t4011_group_detail t4011
                     SET c4011_primary_part_lock_fl = v_part_primary_lock
                     	 ,C4011_LAST_UPDATED_BY = p_userid
               	 	 	 ,C4011_LAST_UPDATED_DATE = CURRENT_DATE
                   WHERE c4010_group_id = p_groupid
                     AND c205_part_number_id = v_pnum;
               ELSE
                  UPDATE t4011_group_detail t4011
                     SET c4011_primary_part_lock_fl = 'Y'
                    	 ,C4011_LAST_UPDATED_BY = p_userid
               	 		 ,C4011_LAST_UPDATED_DATE = CURRENT_DATE
                   WHERE c4010_group_id = p_groupid
                     AND c205_part_number_id = v_pnum;
               END IF;
            END IF;
            
            --When ever the detail is updated, the group table has to be updated ,so ETL can Sync it to GOP.
            UPDATE t4010_group
               SET c4010_last_updated_by = p_userid
			 	 , c4010_last_updated_date = CURRENT_DATE
			 WHERE c4010_group_id = p_groupid;		 
         END LOOP;
      END IF;
   END gm_pd_sav_group_price;

   /******************************************************************
    Description : This procedure is used to fetch parts of a group
   ****************************************************************/
   PROCEDURE gm_fch_group_part_detail (
      p_groupid          IN       t4011_group_detail.c4010_group_id%TYPE,
      p_outpartdetails   OUT      TYPES.cursor_type
   )
   AS
   BEGIN
      OPEN p_outpartdetails
       FOR
          SELECT   get_group_name (p_groupid) groupnm,
                   c205_part_number_id pnum,
                   get_partnum_desc (c205_part_number_id) pdesc
              FROM t4011_group_detail
             WHERE c4010_group_id = p_groupid
          ORDER BY pnum;
   END gm_fch_group_part_detail;

      /******************************************************************
    Description : This procedure is used to fetch groups for construct builder
   ****************************************************************/
   PROCEDURE gm_fch_system_groups (p_outgroupsdetail OUT TYPES.cursor_type)
   AS
   BEGIN
      OPEN p_outgroupsdetail
       FOR
          SELECT   c4010_group_id codeid, c4010_group_nm codenm,
                   c901_group_type pid, c207_set_id setid
              FROM t4010_group
             WHERE c4010_void_fl IS NULL
               AND c901_type = 40045
               AND c901_group_type IS NOT NULL
               AND c901_priced_type = 52110
          ORDER BY c4010_group_nm;
   END gm_fch_system_groups;

   /******************************************************************
    Description : This procedure is used to fetch cosntruct lits of a system
   ****************************************************************/
   PROCEDURE gm_fch_system_constructlist (
      p_systemid    IN       t7003_system_construct.c207_set_id%TYPE,
      p_outmaster   OUT      TYPES.cursor_type
   )
   AS
   BEGIN
      OPEN p_outmaster
       FOR
          SELECT   c7003_system_construct_id constructid,
                   c7003_construct_name constructname
              FROM t7003_system_construct t7003
             WHERE c7003_void_fl IS NULL AND t7003.c207_set_id = p_systemid
          ORDER BY constructname;
   END gm_fch_system_constructlist;

      /******************************************************************
    Description : This procedure is used to fetch master details of cosntruct
   ****************************************************************/
   PROCEDURE gm_fch_constructmaster_details (
      p_constructid   IN       t7003_system_construct.c7003_system_construct_id%TYPE,
      p_outmaster     OUT      TYPES.cursor_type
   )
   AS
   BEGIN
      OPEN p_outmaster
       FOR
          SELECT t7003.c7003_system_construct_id constructid,
                 t7003.c7003_construct_name constructname,
                 t7003.c901_construct_level constlevelid,
                 t7003.c207_set_id systemlistid,
                 DECODE (c7003_principal_fl,
                         'Y', 'on',
                         'off'
                        ) principalconstruct,
                 t7003.c901_status statusid
            FROM t7003_system_construct t7003
           WHERE t7003.c7003_system_construct_id = p_constructid
             AND t7003.c7003_void_fl IS NULL;
   END gm_fch_constructmaster_details;

   /******************************************************************
    Description : This procedure is used to save details of construct
   ****************************************************************/
   PROCEDURE gm_sav_construct_builder (
      p_constructid       IN       t7003_system_construct.c7003_system_construct_id%TYPE,
      p_constructname     IN       t7003_system_construct.c7003_construct_name%TYPE,
      p_systemid          IN       t7003_system_construct.c207_set_id%TYPE,
      p_statusid          IN       t7003_system_construct.c901_status%TYPE,
      p_levelid           IN       t7003_system_construct.c901_construct_level%TYPE,
      p_inputstr          IN       VARCHAR2,
      p_userid            IN       t7003_system_construct.c7003_created_by%TYPE,
      p_principal_const   IN       t7003_system_construct.c7003_principal_fl%TYPE,
      p_outconstructid    OUT      t7003_system_construct.c7003_system_construct_id%TYPE
   )
   AS
      v_seq_id                        NUMBER;
      v_groupid                       t7004_construct_mapping.c4010_group_id%TYPE;
      v_qty                           t7004_construct_mapping.c7004_qty%TYPE;
      v_strlen_construct_grp_maplen   NUMBER  := NVL (LENGTH (p_inputstr), 0);
      v_construct_map_string          VARCHAR2 (1000)           := p_inputstr;
      v_construct_map_substring       VARCHAR2 (100);
      v_construct_avail VARCHAR2(1):='';
   BEGIN
      p_outconstructid := p_constructid;
      IF p_outconstructid IS NOT NULL
      THEN
        SELECT get_multi_construct_avail_flg(p_outconstructid)
        INTO v_construct_avail
        FROM DUAL;
        IF v_construct_avail = 'N' AND p_statusid <> 52071
        THEN
          raise_application_error (-20284, '');
        END IF;
      END IF;
      UPDATE t7003_system_construct
         SET c207_set_id = p_systemid,
             c901_status = p_statusid,
             c901_construct_level = p_levelid,
             c7003_last_updated_by = p_userid,
             c7003_last_updated_date = CURRENT_DATE,
             c7003_principal_fl = p_principal_const
       WHERE c7003_system_construct_id = p_outconstructid;

      IF (SQL%ROWCOUNT = 0)
      THEN
         SELECT s7003_system_construct.NEXTVAL
           INTO p_outconstructid
           FROM DUAL;

         INSERT INTO t7003_system_construct
                     (c7003_system_construct_id, c7003_construct_name,
                      c207_set_id, c901_status, c901_construct_level,
                      c7003_created_by, c7003_created_date, c7003_principal_fl
                     )
              VALUES (p_outconstructid, p_constructname,
                      p_systemid, p_statusid, p_levelid,
                      p_userid, CURRENT_DATE, p_principal_const
                     );
      END IF;

      -- Delete those grouped which have already been mapped but unchecked from the input
      DELETE FROM t7004_construct_mapping
            WHERE c7003_system_construct_id = p_outconstructid;

      IF v_strlen_construct_grp_maplen > 0
      THEN
         WHILE INSTR (v_construct_map_string, '|') <> 0
         LOOP
            v_construct_map_substring :=
               SUBSTR (v_construct_map_string,
                       1,
                       INSTR (v_construct_map_string, '|') - 1
                      );
            v_construct_map_string :=
               SUBSTR (v_construct_map_string,
                       INSTR (v_construct_map_string, '|') + 1
                      );
            --
            v_groupid := NULL;
            v_qty := NULL;
            --
            v_groupid :=
               SUBSTR (v_construct_map_substring,
                       1,
                       INSTR (v_construct_map_substring, '^') - 1
                      );
            v_construct_map_substring :=
               SUBSTR (v_construct_map_substring,
                       INSTR (v_construct_map_substring, '^') + 1
                      );
            v_qty := v_construct_map_substring;

            SELECT s7004_construct_mapping.NEXTVAL
              INTO v_seq_id
              FROM DUAL;

            -- Insert the new groups which the user wants to map to the existing construct
            INSERT INTO t7004_construct_mapping
                        (c7004_construct_mapping_id,
                         c7003_system_construct_id, c4010_group_id, c7004_qty
                        )
                 VALUES (v_seq_id,
                         p_outconstructid, v_groupid, v_qty
                        );
         END LOOP;
      END IF;
   END gm_sav_construct_builder;

      /******************************************************************
    Description : This procedure is used to fetch details of groups for construct
   ****************************************************************/
   PROCEDURE gm_fch_constructgrp_details (
      p_constructid   IN       t7003_system_construct.c7003_system_construct_id%TYPE,
      p_inputstr      IN       VARCHAR2,
      p_outdetail     OUT      TYPES.cursor_type
   )
   AS
   BEGIN
      my_context.set_my_inlist_ctx (p_inputstr);

      OPEN p_outdetail
       FOR
          SELECT groupdetails.groupid, groupname, qty, grouptype, listprice,
                 tripwire, publishfl
            FROM (SELECT t4010.c4010_group_id groupid,
                         t4010.c4010_group_nm groupname,
                         DECODE (p_constructid,
                                 0, NULL,
                                 t7004.c7004_qty
                                ) qty, t4010.c901_group_type grouptype,
                         t4010.c4010_publish_fl publishfl
                    FROM t4010_group t4010, t7004_construct_mapping t7004
                   WHERE t4010.c4010_group_id IN (
                            SELECT token groupid
                              FROM v_in_list
                            UNION ALL
                            SELECT TO_CHAR (t7004.c4010_group_id) groupid
                              FROM t7004_construct_mapping t7004
                             WHERE t7004.c7003_system_construct_id =
                                                                 p_constructid)
                     AND t4010.c4010_group_id = t7004.c4010_group_id(+)
                     AND t4010.c4010_void_fl IS NULL
                     AND DECODE (p_constructid,
                                 0, 1,
                                 t7004.c7003_system_construct_id
                                ) =
                                   DECODE (p_constructid,
                                           0, 1,
                                           p_constructid
                                          )) groupdetails,
                 (SELECT   groupid,
                           MAX (DECODE (pricetype, 52060, pricevalue, NULL)
                               ) listprice
                                          -- , MAX (DECODE (pricetype, 52061, pricevalue, NULL)) adlimit
                                          -- , MAX (DECODE (pricetype, 52062, pricevalue, NULL)) vplimit
                           ,
                           MAX (DECODE (pricetype, 52063, pricevalue, NULL)
                               ) tripwire
                      FROM (SELECT t4010.c4010_group_id groupid,
                                   c901_price_type pricetype,
                                   c7010_price pricevalue
                              FROM t7010_group_price_detail t7010,
                                   t4010_group t4010
                             WHERE t4010.c4010_group_id IN (
                                      SELECT token groupid
                                        FROM v_in_list
                                      UNION ALL
                                      SELECT TO_CHAR
                                                (t7004.c4010_group_id)
                                                                      groupid
                                        FROM t7004_construct_mapping t7004
                                       WHERE t7004.c7003_system_construct_id =
                                                                 p_constructid)
                               AND t4010.c4010_void_fl IS NULL
                               AND t7010.c4010_group_id = t4010.c4010_group_id(+))
                  GROUP BY groupid) pricedetails
           WHERE groupdetails.groupid = pricedetails.groupid(+);
   END gm_fch_constructgrp_details;

      /******************************************************************
    Description : fetch the system group and part pricing details
   ****************************************************************/
   PROCEDURE gm_fch_grppartprice_dtls (
      p_systemid          IN       t4010_group.c207_set_id%TYPE,
      p_part_num		  IN 	   VARCHAR2,
	  p_group_id 		  IN 	   T4010_GROUP.C4010_GROUP_ID%TYPE,
	  p_price_by  		  IN 	   T4010_GROUP.c901_priced_type%TYPE,
	  p_group_type 		  IN   	   T4010_GROUP.c901_group_type%TYPE,
      p_userid            IN       VARCHAR2,
      p_outgroupsdetail   OUT      TYPES.cursor_type,
      p_outpartdetail     OUT      TYPES.cursor_type
   )
   AS
      v_dept_id           VARCHAR2 (20);
      v_company_id        NUMBER;
   BEGIN

	SELECT get_dept_id (p_userid)
	   INTO v_dept_id
	   FROM DUAL;
	--Getting company id from context.   
    SELECT get_compid_frm_cntx()
	  INTO v_company_id 
	  FROM dual;

	OPEN p_outgroupsdetail FOR 
	

	SELECT t4010.c4010_group_id groupid,
	        tlistpr.lprice listp,
	        ttwp.twprice tw,
	        '' ADPRICE,
	        '' VPPRICE,
	        --Pricing team has access always,for others, if its published no access, else they have access
	        DECODE (v_dept_id, '2026', '1', DECODE (t4010.c4010_publish_fl, 'Y', '0', '1')) accfl,
	        t4010.c4010_inactive_fl inactiveFlag,
	        t4010.c207_set_id systemid,
	        t207.c207_set_nm sysname,
	        t4010.c4010_group_nm groupname,
	        t4010.c901_priced_type pricedtype,
	        T901.C901_CODE_NM pricetypenm,
	        t4010.c4010_publish_fl visible,
	        t4010.c4010_principal_grp_fl principal,
	        t4010.c901_group_type grptypeid,
	        get_code_name (t4010.c901_group_type) grptypenm,
	        NVL (TLISTPR.HFL, TTWP.HFL) HISTORYFL
	    FROM t4010_group t4010
            , T4012_GROUP_COMPANY_MAPPING T4012,
	         t207_set_master t207,
	         T901_CODE_LOOKUP T901, (
	                         SELECT c7010_price lprice, t7010.c4010_group_id, C7010_PRICE_HISTORY_FL HFL
	                           FROM t7010_group_price_detail t7010
	                          WHERE t7010.c4010_group_id  = NVL (p_group_id, t7010.c4010_group_id)
	                            AND t7010.c901_price_type = 52060
	                            AND t7010.c7010_void_fl  IS NULL
                                AND T7010.C1900_COMPANY_ID = v_company_id 
	                            ) tlistpr, (
	                         SELECT c7010_price twprice, t7010.c4010_group_id, C7010_PRICE_HISTORY_FL HFL
	                           FROM t7010_group_price_detail t7010
	                          WHERE t7010.c4010_group_id  = NVL (p_group_id, t7010.c4010_group_id)
	                            AND t7010.c901_price_type = 52063
	                            AND t7010.c7010_void_fl  IS NULL
                                AND T7010.C1900_COMPANY_ID = v_company_id  
	                            ) ttwp 
	    WHERE t4010.c4010_void_fl   IS NULL 
	    AND t4010.c4010_group_id = tlistpr.c4010_group_id(+) 
	    AND t4010.c4010_group_id = T4012.c4010_group_id
	    AND t4010.c4010_group_id = ttwp.c4010_group_id (+)
        AND t207.c207_set_id = t4010.c207_set_id 
	    AND t4010.c901_type  = 40045
	    AND t4010.c901_priced_type = NVL (p_price_by, t4010.c901_priced_type) 
	    AND t4010.c901_group_type =  NVL (p_group_type, t4010.c901_group_type)
	    AND t901.c901_code_id    = t4010.c901_priced_type
	    AND (t4010.c4010_group_id =  DECODE(p_part_num,'',NVL (p_group_id, t4010.c4010_group_id),'-1')
	    OR
	    --THis query is to get the group based on the part that user is searching for
	    t4010.c4010_group_id IN
	    (
	         SELECT t4010.c4010_group_id
	           FROM t4011_group_detail t4011, t4010_group t4010 , T4012_GROUP_COMPANY_MAPPING t4012
	            --WHERE C205_PART_NUMBER_ID   IN (SELECT * FROM v_in_list)
	          WHERE REGEXP_LIKE (t4011.c205_part_number_id,  REGEXP_REPLACE(p_part_num,'[+]','\+'))
	            AND t4010.c4010_group_id   = t4011.c4010_group_id
	            AND t4010.c901_type        = 40045 -- OP GROUP
	            AND c901_part_pricing_type = 52080 -- PRIMARY PART
                AND T4012.C1900_COMPANY_ID = v_company_id 
				AND T4012.C4012_VOID_FL IS NULL
                AND T4012.C4010_GROUP_ID = T4010.C4010_GROUP_ID
	            AND t4010.c4010_group_id =  NVL (p_group_id, t4010.c4010_group_id)
	    ))
	    AND t207.c207_set_id   = NVL (p_systemid, t4010.c207_set_id)
        AND T4012.C1900_COMPANY_ID = v_company_id 
		AND T4012.C4012_VOID_FL IS NULL
	 ORDER BY pricetypenm, groupname;
	 
	OPEN p_outpartdetail FOR 
	SELECT t4011.c205_part_number_id pnum,
			t4011.c4010_group_id groupid,
			t4010.c207_set_id systemid,
			t205.c205_part_num_desc pdesc,
			t2052.C2052_LIST_PRICE listp,
			t2052.C2052_LOANER_PRICE loaner, -- ad price
			t2052.C2052_CONSIGNMENT_PRICE consign, --vp price
			t2052.C2052_EQUITY_PRICE tw,
			t207.c207_set_nm sysname,
			/* Pricing team has access always,for others, if its published no access, else they have access.
			 * The below decode is changed for PMT-4890.
			 * The PD user needs to have access to edit price for the newly added parts mapped to individual group which is published already.
			 */
			DECODE (v_dept_id, '2026', '1', DECODE(t2052.C2052_LIST_PRICE,NULL,'1',DECODE(t2052.C2052_EQUITY_PRICE,NULL,'1',DECODE (t4010.c4010_publish_fl, 'Y', '0', '1')))) accfl,
			t4010.c901_group_type grptypeid,
			NVL (t2052.c2052_list_price_history_fl, t2052.c2052_eqt_price_history_fl) HISTORYFL,
			get_code_name (t4010.c901_group_type) grptypenm FROM t4011_group_detail t4011,
			t207_set_master t207,
			t4010_group t4010,
			T2052_PART_PRICE_MAPPING t2052,
			t205_part_number t205 
	WHERE t4011.c4010_group_id IN	(
						     SELECT t4010.c4010_group_id groupid
						       FROM t4010_group t4010
						      WHERE t4010.c4010_void_fl   IS NULL
						        AND t4010.c901_type        = 40045
						        AND t4010.c901_priced_type = 52111
							)
	AND t207.c207_set_id = t4010.c207_set_id 
	AND t4010.c4010_group_id = t4011.c4010_group_id 
	AND t4011.c901_part_pricing_type = 52080 --Primary Part
	AND t4011.c205_part_number_id   = t205.c205_part_number_id
	AND t205.c205_part_number_id = t2052.c205_part_number_id(+) 
	AND t2052.c1900_company_id(+) = v_company_id
	AND t2052.c2052_void_fl(+) is NULL
	AND REGEXP_LIKE (t4011.c205_part_number_id, CASE WHEN TO_CHAR(p_part_num) IS NOT NULL THEN TO_CHAR(REGEXP_REPLACE(p_part_num,'[+]','\+')) ELSE REGEXP_REPLACE(t4011.c205_part_number_id,'[+]','\+') END)	
	AND t4010.c901_priced_type = NVL (p_price_by, t4010.c901_priced_type) 
	AND t4010.c901_group_type =  NVL (p_group_type, t4010.c901_group_type)
	AND t4010.c4010_group_id =  NVL (p_group_id,  t4010.c4010_group_id)
	AND t207.c207_set_id = NVL (p_systemid, t4010.c207_set_id)
	AND t4011.c205_part_number_id IN (SELECT t2023.c205_part_number_id 
	  								   FROM t2023_part_company_mapping t2023
		 							  WHERE REGEXP_LIKE(t2023.c205_part_number_id, CASE WHEN TO_CHAR(p_part_num) IS NOT NULL THEN TO_CHAR(REGEXP_REPLACE(p_part_num,'[+]','\+')) ELSE REGEXP_REPLACE(t2023.c205_part_number_id,'[+]','\+') END)
										AND t2023.c2023_void_fl IS NULL 
										AND t2023.c1900_company_id = v_company_id)
	ORDER BY pnum;
   END gm_fch_grppartprice_dtls;

      /******************************************************************
    Description : save the system group and part pricing details
   ****************************************************************/
   PROCEDURE gm_sav_grp_price_dtls (
      p_inputstrgroup       IN       VARCHAR2,
      p_userid              IN       VARCHAR2,
      p_out_string          OUT      VARCHAR2,
      p_out_sucess_string   OUT      VARCHAR2,
      p_comments     		IN   	 t7010_group_price_detail.C70101_LAST_COMMENT%TYPE  DEFAULT NULL
   )
   AS
      v_group_id                 NUMBER;
      v_listprice                t7010_group_price_detail.c7010_price%TYPE;
      v_adprice                  t7010_group_price_detail.c7010_price%TYPE;
      v_vpprice                  t7010_group_price_detail.c7010_price%TYPE;
      v_twprice                  t7010_group_price_detail.c7010_price%TYPE;
      v_visible                  VARCHAR2 (10);
      v_strlen_req_grp_maplen    NUMBER  := NVL (LENGTH (p_inputstrgroup), 0);
      v_req_map_string           VARCHAR2 (4000)           := p_inputstrgroup;
      v_req_map_substring        VARCHAR2 (200);
      v_old_publish_fl           VARCHAR2 (10);
      subject                    VARCHAR2 (200);
      to_mail                    VARCHAR2 (1000);
      mail_body                  VARCHAR2 (4000);
      v_group_nm                 t4010_group.c4010_group_nm%TYPE;
      v_principal                VARCHAR2 (10);
      v_count                    NUMBER;
      v_value                    NUMBER;
      v_error_fl                 VARCHAR2 (1);
      v_out_string               VARCHAR2 (1000)                  := 'error$';
      v_flg_id                   VARCHAR2 (1);          -- flag for grpid =15
      v_success_grp_ids_string   VARCHAR2 (4000)                        := '';
      cur_groups                 TYPES.cursor_type;
      v_success_grp_out_string   VARCHAR2 (4000)                := 'success$';
      c_group_id                 t4010_group.c4010_group_id%TYPE;
      c_adprice                  t7010_group_price_detail.c7010_price%TYPE;
      c_vpprice                  t7010_group_price_detail.c7010_price%TYPE;
      v_str_length               NUMBER;
      v_priced_type              t4010_group.c901_priced_type%TYPE;
      p_override_id				 VARCHAR2(40);
      v_cnt						 NUMBER;
      v_msg						 VARCHAR2(199);
      v_rule_date_fmt            VARCHAR2(100);
      v_company_id        NUMBER;


	  
      CURSOR findgrp_cur
      IS
         SELECT t1501.c1500_group_id grpid
           FROM t1501_group_mapping t1501
          WHERE t1501.c101_mapped_party_id = p_userid
            AND t1501.c1500_group_id IN ('14', '15');
   BEGIN
	   
	   	   --Getting company id from context.   
    SELECT get_compid_frm_cntx()
	  INTO v_company_id 
	  FROM dual;
	  
      WHILE INSTR (v_req_map_string, '|') <> 0
      LOOP
         v_req_map_substring :=
               SUBSTR (v_req_map_string, 1, INSTR (v_req_map_string, '|') - 1);
         v_req_map_string :=
                  SUBSTR (v_req_map_string, INSTR (v_req_map_string, '|') + 1);
         --
         v_group_id := NULL;
         v_listprice := NULL;
         v_twprice := NULL;
         v_visible := NULL;
         v_error_fl := 'N';
         v_flg_id := 'N';
         --
         v_group_id :=
            SUBSTR (v_req_map_substring,
                    1,
                    INSTR (v_req_map_substring, '^') - 1
                   );
         v_req_map_substring :=
             SUBSTR (v_req_map_substring, INSTR (v_req_map_substring, '^') + 1);
         v_listprice :=
            SUBSTR (v_req_map_substring,
                    1,
                    INSTR (v_req_map_substring, '^') - 1
                   );

         v_req_map_substring :=
             SUBSTR (v_req_map_substring, INSTR (v_req_map_substring, '^') + 1);
         v_twprice :=
            SUBSTR (v_req_map_substring,
                    1,
                    INSTR (v_req_map_substring, '^') - 1
                   );
         v_req_map_substring :=
             SUBSTR (v_req_map_substring, INSTR (v_req_map_substring, '^') + 1);
         v_visible :=
            SUBSTR (v_req_map_substring,
                    1,
                    INSTR (v_req_map_substring, '^') - 1
                   );
         v_req_map_substring :=
             SUBSTR (v_req_map_substring, INSTR (v_req_map_substring, '^') + 1);
         v_principal := v_req_map_substring;

         SELECT c4010_publish_fl
           INTO v_old_publish_fl
           FROM t4010_group
          WHERE c4010_group_id = v_group_id AND c4010_void_fl IS NULL;

         BEGIN
            FOR grp_cur IN findgrp_cur
            LOOP
               IF grp_cur.grpid = '15'
               THEN
                  v_flg_id := 'Y';
               END IF;
            END LOOP;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               v_error_fl := 'Y';
               v_out_string := v_out_string || 'P' || v_group_id || '^';
         END;

         IF     v_old_publish_fl = 'Y'
            AND v_flg_id <> 'Y'                 --not pricing or sales analyst
         THEN
            v_error_fl := 'Y';
            v_out_string := v_out_string || 'P' || v_group_id || '^';
         END IF;

         IF v_error_fl != 'Y'
         THEN
            UPDATE t4010_group
               SET c4010_principal_grp_fl =
                                          DECODE (v_principal,
                                                  '0', NULL,
                                                  'Y'
                                                 )
                   ,C4010_LAST_UPDATED_BY = p_userid
                   ,C4010_LAST_UPDATED_DATE = CURRENT_DATE
             WHERE c4010_group_id = v_group_id;

            gm_pd_sav_group_price_detail (v_group_id,
                                          v_listprice,
                                          52060,
                                          p_userid,
                                          p_comments
                                         );
			BEGIN
				SELECT 'gpp-'||a.ID INTO p_override_id FROM (
					SELECT  b.c941_updated_date,A.c901_price_type type,get_code_name(A.c901_price_type),b.c941_value,
					b.c941_audit_trail_log_id id
					FROM t7010_group_price_detail a,t941_audit_trail_log b
					 WHERE c4010_group_id=v_group_id
					 AND b.c941_ref_id = to_char(A.c7010_group_price_detail_id )
					 AND b.c940_audit_trail_id=1030
					 ORDER BY b.c941_updated_date DESC
					 
				 )a where rownum=1 and type=52060;
			EXCEPTION WHEN NO_DATA_FOUND THEN
				p_override_id :='';
			END;
			
			SELECT get_compdtfmt_frm_cntx() into v_rule_date_fmt from dual; 
			--SELECT GET_RULE_VALUE('DATEFMT','DATEFORMAT') INTO v_rule_date_fmt FROM DUAL;
			v_rule_date_fmt := v_rule_date_fmt || ' HH:MI:SS';
			
			 SELECT COUNT(1) INTO v_cnt from T902_LOG where C902_REF_ID=p_override_id and c902_type='4000344';
			 IF v_cnt =0
			 THEN
             	gm_update_log (p_override_id, 'LP Comments -'||TO_CHAR(CURRENT_DATE,v_rule_date_fmt), '4000344', p_userid, v_msg) ;
             END IF;
             
            IF v_twprice IS NOT NULL
            THEN
               v_value := get_baseline_value (v_group_id, 20179);
			   IF (v_value = 0) THEN 
			   v_value := get_rule_value(20179,'ADLIMIT') ;
               END IF;
			   IF (v_value <> 0)
               THEN
                  v_adprice := get_limit_price_value (v_value, v_twprice);
                  gm_pd_sav_group_price_detail (v_group_id,
                                                v_adprice,
                                                52061,
                                                p_userid,
                                         	    p_comments
                                               );
               END IF;

               v_value := get_baseline_value (v_group_id, 20180);
			   IF (v_value = 0) THEN 
			   v_value := get_rule_value(20180,'VPLIMIT') ;
               END IF;
               IF (v_value <> 0)
               THEN
                  v_vpprice := get_limit_price_value (v_value, v_twprice);
                  gm_pd_sav_group_price_detail (v_group_id,
                                                v_vpprice,
                                                52062,
                                                p_userid,
                                          		p_comments
                                               );
               END IF;
            END IF;

            gm_pd_sav_group_price_detail (v_group_id,
                                          v_twprice,
                                          52063,
                                          p_userid,
                                          p_comments
                                         );
			BEGIN
				 SELECT 'gpp-'||a.ID INTO p_override_id FROM (
					SELECT  b.c941_updated_date,A.c901_price_type type,get_code_name(A.c901_price_type),b.c941_value,
					b.c941_audit_trail_log_id id
					FROM t7010_group_price_detail a,t941_audit_trail_log b
					 WHERE c4010_group_id=v_group_id
					 AND b.c941_ref_id = to_char(A.c7010_group_price_detail_id )
					 AND b.c940_audit_trail_id=1030
					 ORDER BY b.c941_updated_date DESC
					 
				 )a where rownum=1 and type=52063;
			EXCEPTION WHEN NO_DATA_FOUND THEN
				p_override_id :='';
			END;
			 SELECT COUNT(1) INTO v_cnt from T902_LOG where C902_REF_ID=p_override_id and c902_type='4000344';
			 IF v_cnt =0
			 THEN
             	gm_update_log (p_override_id, 'TWP Comments -'||TO_CHAR(CURRENT_DATE,v_rule_date_fmt), '4000344', p_userid, v_msg) ;
             END IF;
             
            ---------group publish validation -------------------------------------
            IF (v_visible = '1' AND v_old_publish_fl IS NULL)
            THEN
               BEGIN
                  gm_publish_validate (v_group_id, v_listprice, v_twprice);
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     v_error_fl := 'Y';
                     v_out_string := v_out_string || 'E' || v_group_id || '^';
               END;
            END IF;

            IF v_error_fl = 'N'
            THEN
               UPDATE t4010_group
                  SET c4010_publish_fl = DECODE (v_visible, '0', NULL, 'Y'),
                      c4010_group_publish_date = TRUNC (CURRENT_DATE),
                      c101_group_publish_by = p_userid
                      ,C4010_LAST_UPDATED_BY = p_userid
                   	  ,C4010_LAST_UPDATED_DATE = CURRENT_DATE
                WHERE c4010_group_id = v_group_id;
                
                -- MNTTASK-7037 - Already ad price and vpprice getting from rule table. No need update  
	
                 --update the part price
                   UPDATE t205_part_number
                    SET c205_list_price = v_listprice
					   , c205_loaner_price = v_adprice
                       , c205_consignment_price = v_vpprice
                 	   , c205_equity_price = v_twprice
                 	   , c205_last_updated_date = CURRENT_DATE    
					   , c205_last_updated_by =  p_userid
					   , C205_PART_PRICE_UPDATED_DATE = CURRENT_DATE    
					   , c205_part_price_updated_by =  p_userid
					   , C205_PRICE_COMMENTS = p_comments
                  WHERE c205_part_number_id IN (
                          -- Get all the part from primary group and priced by "GROUP".
                           SELECT c205_part_number_id
                           FROM t4011_group_detail t4011, t4010_group t4010   
                           WHERE t4010.c4010_group_id = v_group_id
                           AND t4011.c901_part_pricing_type = 52080 -- PRIMARY
                           and t4010.c4010_group_id = t4011.c4010_group_id
                           and t4010.c901_priced_type=52110 -- GROUP
                           )
                           AND c205_active_fl = 'Y';
        --We are updating the price in the  T2052_PART_PRICE_MAPPING instead of T2052_PART_PRICE_MAPPING.
 		--Once if we change all fetch quiries to T2052_PART_PRICE_MAPPING table. we are going to delete the above update query.
             			GM_PKG_PD_PART_PRICING.gm_sav_part_price_by_group(v_group_id,v_twprice,v_adprice,v_vpprice,v_listprice,v_company_id,p_userid);
 		
			      --After update the log table should Clear the c205_price_comments.       
				  UPDATE t205_part_number
			         SET c205_price_comments = '',
			         	 c205_last_updated_by = p_userid,
			             c205_last_updated_date = CURRENT_DATE
			       WHERE c205_part_number_id IN (
						                           SELECT c205_part_number_id
						                           FROM t4011_group_detail t4011, t4010_group t4010   
						                           WHERE t4010.c4010_group_id = v_group_id
						                           AND t4011.c901_part_pricing_type = 52080 -- PRIMARY
						                           and t4010.c4010_group_id = t4011.c4010_group_id
						                           and t4010.c901_priced_type=52110 -- GROUP
						                           )
                    AND c205_active_fl = 'Y';
       
                 SELECT t4010.c901_priced_type
                 INTO v_priced_type
                 FROM t4010_group t4010
                 WHERE t4010.c4010_group_id = v_group_id
                 AND t4010.c901_group_type IS NOT NULL
                 AND t4010.c4010_void_fl IS NULL;

               IF v_priced_type = 52110                                 --part
               THEN
                  v_success_grp_ids_string :=
                                v_success_grp_ids_string || v_group_id || ',';
               END IF;
            END IF;
       
         END IF;
      END LOOP;

      v_success_grp_ids_string :=
         SUBSTR (v_success_grp_ids_string,
                 1,
                 LENGTH (v_success_grp_ids_string) - 1
                );
      gm_fch_ad_vp_for_groups (v_success_grp_ids_string, cur_groups);

      LOOP
         FETCH cur_groups
          INTO c_group_id, c_adprice, c_vpprice;

         EXIT WHEN cur_groups%NOTFOUND;
         v_success_grp_out_string :=
               v_success_grp_out_string
            || c_group_id
            || '^'
            || TO_CHAR (c_adprice)
            || '^'
            || TO_CHAR (c_vpprice)
            || '|';
      END LOOP;

      CLOSE cur_groups;

      v_out_string := v_out_string;
      p_out_string := v_out_string;
      p_out_sucess_string := v_success_grp_out_string;
   END gm_sav_grp_price_dtls;

   /******************************************************************
      Description : save the system group and part pricing details
      ****************************************************************/
   PROCEDURE gm_sav_part_price_dtls (
      p_inputstrpart   IN       VARCHAR2,
      p_userid         IN       VARCHAR2,
      p_out_string     OUT      VARCHAR2,
      p_comments       IN       T205_PART_NUMBER.C205_PRICE_COMMENTS%TYPE DEFAULT NULL
   )
   AS
      v_pnum                      VARCHAR2 (20);
      v_listprice                 t205_part_number.c205_list_price%TYPE;
      v_twprice                   t205_part_number.c205_list_price%TYPE;
      v_strlen_req_grp_maplen     NUMBER  := NVL (LENGTH (p_inputstrpart), 0);
      v_req_map_string            VARCHAR2 (4000)           := p_inputstrpart;
      v_req_map_substring         VARCHAR2 (200);
      v_group_id                  t4010_group.c4010_group_id%TYPE;
      v_adprice                   t7010_group_price_detail.c7010_price%TYPE;
      v_vpprice                   t7010_group_price_detail.c7010_price%TYPE;
      v_value                     t7010_group_price_detail.c7010_price%TYPE;
      cur_parts                   TYPES.cursor_type;
      c_part_id                   t205_part_number.c205_part_number_id%TYPE;
      c_adprice                   t7010_group_price_detail.c7010_price%TYPE;
      c_vpprice                   t7010_group_price_detail.c7010_price%TYPE;
      v_succes_part_id_string     VARCHAR2 (4000);
      v_success_part_out_string   VARCHAR2 (4000);
   BEGIN
      WHILE INSTR (v_req_map_string, '|') <> 0
      LOOP
         v_req_map_substring :=
               SUBSTR (v_req_map_string, 1, INSTR (v_req_map_string, '|') - 1);
         v_req_map_string :=
                  SUBSTR (v_req_map_string, INSTR (v_req_map_string, '|') + 1);
         --
         v_pnum := NULL;
         v_listprice := NULL;
         v_twprice := NULL;
         --
         v_pnum :=
            SUBSTR (v_req_map_substring,
                    1,
                    INSTR (v_req_map_substring, '^') - 1
                   );
         v_req_map_substring :=
             SUBSTR (v_req_map_substring, INSTR (v_req_map_substring, '^') + 1);
         v_listprice :=
            SUBSTR (v_req_map_substring,
                    1,
                    INSTR (v_req_map_substring, '^') - 1
                   );

         v_req_map_substring :=
             SUBSTR (v_req_map_substring, INSTR (v_req_map_substring, '^') + 1);
         v_twprice := v_req_map_substring;

        --When there is price change and there is no comments entered in UI, throw App error.
        IF (p_comments IS NULL AND (get_part_price_update_flag(v_pnum,v_listprice,52060) = 'Y' 
                                  OR   get_part_price_update_flag(v_pnum,v_twprice,52063) = 'Y' ))
		THEN
			raise_application_error (-20603, '');
		END IF;
         
		IF  get_part_price_update_flag(v_pnum,v_listprice,52060) = 'Y' 
		THEN
	         gm_pd_sav_part_price_detail (v_pnum, v_listprice, 52060, p_userid, p_comments);
	    END IF;
	    
	    IF  get_part_price_update_flag(v_pnum,v_twprice,52063) = 'Y'  
		THEN
	         gm_pd_sav_part_price_detail (v_pnum, v_twprice, 52063, p_userid, p_comments);
		END IF;

         SELECT t4010.c4010_group_id
           INTO v_group_id
          FROM t4011_group_detail t4011,t4010_group t4010
          WHERE c901_part_pricing_type = 52080
          AND t4010.c4010_group_id = t4011.c4010_group_id
          AND t4010.c4010_void_fl is null
          AND c205_part_number_id = v_pnum;


         IF v_twprice IS NOT NULL
         THEN
            v_value := get_baseline_value (v_group_id, 20179);
            
			IF (v_value = 0) THEN 
			   v_value := get_rule_value(20179,'ADLIMIT') ;
            END IF;
            IF (v_value <> 0)
            THEN
               v_adprice := get_limit_price_value (v_value, v_twprice);
               gm_pd_sav_part_price_detail (v_pnum,
                                            v_adprice,
                                            52061,
                                            p_userid
                                           );
            END IF;

            v_value := get_baseline_value (v_group_id, 20180);
			IF (v_value = 0) THEN 
			   v_value := get_rule_value(20180,'VPLIMIT') ;
            END IF;
            IF (v_value <> 0)
            THEN
               v_vpprice := get_limit_price_value (v_value, v_twprice);
               gm_pd_sav_part_price_detail (v_pnum,
                                            v_vpprice,
                                            52062,
                                            p_userid
                                           );
            END IF;
         END IF;

         v_succes_part_id_string := v_succes_part_id_string || v_pnum || ',';
     
      --After update the log table should Clear the c205_price_comments.       
	  UPDATE t205_part_number
         SET c205_price_comments = '',
             c205_part_price_updated_by = '',
             c205_part_price_updated_date = '',
         	 c205_last_updated_by = p_userid,
             c205_last_updated_date = CURRENT_DATE
       WHERE c205_part_number_id = v_pnum;
       
      END LOOP;

      v_succes_part_id_string :=
         SUBSTR (v_succes_part_id_string,
                 1,
                 LENGTH (v_succes_part_id_string) - 1
                );
      gm_fch_ad_vp_for_parts (v_succes_part_id_string, cur_parts);

      LOOP
         FETCH cur_parts
          INTO c_part_id, c_adprice, c_vpprice;

         EXIT WHEN cur_parts%NOTFOUND;
         v_success_part_out_string :=
               v_success_part_out_string
            || c_part_id
            || '^'
            || TO_CHAR (c_adprice)
            || '^'
            || TO_CHAR (c_vpprice)
            || '|';
      END LOOP;

      CLOSE cur_parts;

      p_out_string := v_success_part_out_string;
   END gm_sav_part_price_dtls;

   /******************************************************************
      Description : update demand sheet owner to price UPdate PD group
     ****************************************************************/
   PROCEDURE gm_update_demand_sheet_owner (
      p_curuser   IN   VARCHAR2,
      p_userid    IN   VARCHAR2
   )
   AS
      v_cnt   NUMBER (3);
   BEGIN
      IF (p_curuser = '')
      THEN
         SELECT COUNT (*)
           INTO v_cnt
           FROM t1501_group_mapping t1501
          WHERE t1501.c1500_group_id = '15'
            AND t1501.c101_mapped_party_id = p_userid;

         IF (v_cnt = 0)
         THEN
            INSERT INTO t1501_group_mapping
                        (c1501_group_mapping_id, c101_mapped_party_id,
                         c1500_mapped_group_id, c1500_group_id
                        )
                 VALUES (s1501_group_mapping.NEXTVAL, p_userid,
                         NULL, '15'
                        );
         END IF;
      ELSIF (p_curuser <> p_userid)
      THEN
         SELECT COUNT (*)
           INTO v_cnt
           FROM t4020_demand_master
          WHERE c901_demand_type = 40021
            AND c4020_void_fl IS NULL
            AND c4020_primary_user = p_curuser;

         IF (v_cnt <= 1)
         THEN
            DELETE FROM t1501_group_mapping
                  WHERE c1500_mapped_group_id = p_curuser
                    AND c1500_group_id = '15';
         END IF;

         INSERT INTO t1501_group_mapping
                     (c1501_group_mapping_id, c101_mapped_party_id,
                      c1500_mapped_group_id, c1500_group_id
                     )
              VALUES (s1501_group_mapping.NEXTVAL, p_userid,
                      NULL, '15'
                     );
      END IF;
   END gm_update_demand_sheet_owner;

   /******************************************************************
      Description : group price for given type
      ****************************************************************/
   FUNCTION get_base_group_price (
      p_groupid      t4010_group.c4010_group_id%TYPE,
      p_price_type   t7010_group_price_detail.c901_price_type%TYPE
   )
      RETURN NUMBER
   IS
      v_price   t705_account_pricing.c705_unit_price%TYPE;
      v_company_id NUMBER;
   BEGIN
      --
      BEGIN
	        SELECT get_compid_frm_cntx()
	          INTO v_company_id 
	          FROM dual;
	          
         SELECT c7010_price
           INTO v_price
           FROM t7010_group_price_detail t7010
          WHERE t7010.c4010_group_id = p_groupid
            AND t7010.c901_price_type = p_price_type
            and t7010.c1900_company_id=v_company_id
            AND t7010.c7010_void_fl IS NULL;
      END;

      RETURN v_price;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN 0;
--
   END get_base_group_price;

   /******************************************************************
      Description : part price for given type
      ****************************************************************/
   FUNCTION get_base_part_price (
      p_partnum      VARCHAR2,
      p_price_type   t7010_group_price_detail.c901_price_type%TYPE
   )
      RETURN NUMBER
   IS
      v_groupid   t4010_group.c4010_group_id%TYPE;
      v_price     t705_account_pricing.c705_unit_price%TYPE;
   BEGIN
      --
      SELECT t4011.c4010_group_id
        INTO v_groupid
        FROM t4011_group_detail t4011, t4010_group t4010
       WHERE t4011.c205_part_number_id = p_partnum
         AND t4011.c901_part_pricing_type = 50080
         AND t4010.c4010_group_id = t4011.c4010_group_id
         AND t4010.c4010_void_fl IS NULL
         AND t4010.c901_type = 40045;

      BEGIN
         SELECT c7010_price
           INTO v_price
           FROM t7010_group_price_detail t7010
          WHERE t7010.c4010_group_id = v_groupid
            AND t7010.c901_price_type = p_price_type
            AND t7010.c7010_void_fl IS NULL;
      END;

      RETURN v_price;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN 0;
--
   END get_base_part_price;

   /******************************************************************
      Description : check all groups whis not published more than 2 weeks
      since it's created.
      ****************************************************************/
   PROCEDURE gm_fch_publish_reminder (p_out_publish_dtls OUT TYPES.cursor_type)
   AS
   BEGIN
      OPEN p_out_publish_dtls
       FOR
          SELECT   t4010.c4010_group_id groupid,
                   get_group_name (t4010.c4010_group_id) groupnm,
                                      TO_CHAR (t4010.c4010_created_date,
                            get_rule_value('DATEFMT','DATEFORMAT')
                           ) created_date,
                   get_user_emailid
                      (get_group_primary_owner (t4010.c4010_group_id)
                      ) to_email,
                   get_group_primary_owner (t4010.c4010_group_id) owner,
                   get_rule_value ('GRPPUBLISHREM', 'EMAIL') cc_email
              FROM t4010_group t4010
             WHERE t4010.c4010_void_fl IS NULL
               AND t4010.c4010_publish_fl IS NULL
               AND t4010.c4010_created_date <= TRUNC (CURRENT_DATE) - 14
               AND t4010.c901_group_type IS NOT NULL
          ORDER BY owner;
   END gm_fch_publish_reminder;

   /******************************************************************
      Description : save AD/VP Price Band
      ****************************************************************/
   PROCEDURE gm_sav_pricebandvalue (
      p_inputstr   IN   VARCHAR2,
      p_userid     IN   VARCHAR2
   )
   AS
      v_system_id   VARCHAR2 (20);
      v_priceband   t240_baseline_value.c240_value%TYPE;
      v_type        t240_baseline_value.c901_type%TYPE;
      v_strlen      NUMBER                    := NVL (LENGTH (p_inputstr), 0);
      v_string      VARCHAR2 (4000)                       := p_inputstr;
      v_substring   VARCHAR2 (1000);
      v_str         VARCHAR2 (200);
   BEGIN
      IF v_strlen > 0
      THEN
         WHILE INSTR (v_string, '|') <> 0
         LOOP
            v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
            v_string := SUBSTR (v_string, INSTR (v_string, '|') + 1);
            --
            v_system_id := NULL;
            v_priceband := NULL;
            v_type := NULL;
            --
            v_system_id :=
                         SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
            v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
            v_priceband :=
                         SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
            v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
            v_type := v_substring;
            v_str := v_system_id || '^' || v_priceband || '|';
            ------------------- update AD VP Price band---------------
            gm_pkg_sm_baseline.gm_sm_sav_baselinevalue (v_str,
                                                        v_type,
                                                        p_userid
                                                       );

            /*    UPDATE t240_baseline_value
                      SET c240_value = v_priceband
                      , c240_last_updated_by = p_userid
                      , c240_last_updated_date = CURRENT_DATE
                   WHERE c207_set_id = v_system_id AND c901_type = v_type AND c240_void_fl IS NULL;

                  IF (SQL%ROWCOUNT = 0)
                  THEN
                     INSERT INTO t240_baseline_value
                              (c240_baseline_value_id, c207_set_id, c901_type, c240_value, c240_created_by
                              , c240_created_date
                              )
                         VALUES (s240_baseline_value.NEXTVAL, v_system_id, v_type, v_priceband, p_userid
                              , CURRENT_DATE
                              );
                  END IF;
                  */
            --gm_sav_baseline_value (v_priceband, v_system_id, v_type, p_userid);

            ------------------ update AD limit Price accordingly---------------
            ----call another procedure to calculating limit price by systemid  -- gm_sav_limit_price(v_priceband, v_system_id, v_priceband, v_type)------------
            IF (v_type = 20179 AND v_priceband IS NOT NULL)
            THEN
               gm_sav_limit_price (v_priceband, v_system_id, 52061, p_userid);
            END IF;

            /* SELECT c7010_price
                 INTO v_twprice
                 FROM t7010_group_price_detail
                WHERE c4010_group_id = v_groupid AND c7010_void_fl IS NULL AND c901_price_type = 52063;

               UPDATE t7010_group_price_detail t7010
                  SET t7010.c7010_price = (1 + v_adprice / 100) * v_twprice
                   , t7010.c7010_last_updated_by = p_userid
                   , t7010.c7010_last_updated_date = CURRENT_DATE
                WHERE t7010.c4010_group_id IN (SELECT t4010.c4010_group_id
                                         FROM t4010_group t4010
                                        WHERE t4010.c207_set_id = v_system_id)
                  AND t7010.c901_price_type = 52061
                  AND t7010.c7010_void_fl IS NULL;

                     ------------------- update VP Price band---------------
                     UPDATE t240_baseline_value
                        SET c240_value = v_vpprice
                         , c240_last_updated_by = p_userid
                         , c240_last_updated_date = CURRENT_DATE
                      WHERE c207_set_id = v_system_id AND c901_type = 20180 AND c240_void_fl IS NULL;

                     IF (SQL%ROWCOUNT = 0)
                     THEN
                        INSERT INTO t240_baseline_value
                                 (c240_baseline_value_id, c207_set_id, c901_type, c240_value, c240_created_by
                                  , c240_created_date
                                 )
                            VALUES (s240_baseline_value.NEXTVAL, v_system_id, 20180, v_vpprice, p_userid
                                  , CURRENT_DATE
                                 );
                     END IF;
                  */ ------------------ update VP limit Price accordingly---------------
            IF (v_type = 20180 AND v_priceband IS NOT NULL)
            THEN
               gm_sav_limit_price (v_priceband, v_system_id, 52062, p_userid);
            END IF;
         /* UPDATE t7010_group_price_detail t7010
               SET t7010.c7010_price = (1 - v_vpprice / 100) * v_vpprice
                , t7010.c7010_last_updated_by = p_userid
                , t7010.c7010_last_updated_date = CURRENT_DATE
             WHERE t7010.c4010_group_id IN (SELECT t4010.c4010_group_id
                                      FROM t4010_group t4010
                                     WHERE t4010.c207_set_id = v_system_id)
               AND t7010.c901_price_type = 52062
               AND t7010.c7010_void_fl IS NULL; */
         END LOOP;
      END IF;
   END gm_sav_pricebandvalue;

   /*************************************************************
      Description : save AD, VP limit price when price band is changed
      ****************************************************************/
   PROCEDURE gm_sav_baseline_value (
      p_priceband   IN   t240_baseline_value.c240_value%TYPE,
      p_systemid    IN   t240_baseline_value.c207_set_id%TYPE,
      p_type        IN   t240_baseline_value.c901_type%TYPE,
      p_userid      IN   t240_baseline_value.c240_created_by%TYPE
   )
   AS
   BEGIN
      UPDATE t240_baseline_value
         SET c240_value = p_priceband,
             c240_last_updated_by = p_userid,
             c240_last_updated_date = CURRENT_DATE
       WHERE c207_set_id = p_systemid
         AND c901_type = p_type
         AND c240_void_fl IS NULL;

      IF (SQL%ROWCOUNT = 0)
      THEN
         INSERT INTO t240_baseline_value
                     (c240_baseline_value_id, c207_set_id, c901_type,
                      c240_value, c240_created_by, c240_created_date
                     )
              VALUES (s240_baseline_value.NEXTVAL, p_systemid, p_type,
                      p_priceband, p_userid, CURRENT_DATE
                     );
      END IF;
   END gm_sav_baseline_value;

   /*************************************************************
      Description : save AD, VP limit price when price band is changed
      ****************************************************************/
   PROCEDURE gm_sav_limit_price (
      p_priceband    IN   t240_baseline_value.c240_value%TYPE,
      p_systemid     IN   t240_baseline_value.c207_set_id%TYPE,
      p_price_type   IN   t7010_group_price_detail.c901_price_type%TYPE,
      p_userid       IN   t240_baseline_value.c240_created_by%TYPE
   )
   AS
      v_twprice   t7010_group_price_detail.c7010_price%TYPE;
	  v_company_id  VARCHAR2(20);
      CURSOR cur_detail
      IS
         SELECT c4010_group_id groupid
           FROM t4010_group
          WHERE c207_set_id = p_systemid
            AND c901_priced_type = 52110
            AND c4010_void_fl IS NULL;

      CURSOR cur_part_detail
      IS
         SELECT t4011.c205_part_number_id pnum
           FROM t4011_group_detail t4011
          WHERE t4011.c4010_group_id IN (
                   SELECT t4010.c4010_group_id
                     FROM t4010_group t4010
                    WHERE t4010.c4010_void_fl IS NULL
                      AND t4010.c901_priced_type = 52111);
   BEGIN
	  
	  SELECT  get_compid_frm_cntx()
    	INTO    v_company_id
      FROM    DUAL;
      FOR var_detail IN cur_detail
      LOOP
         BEGIN
            SELECT c7010_price
              INTO v_twprice
              FROM t7010_group_price_detail
             WHERE c4010_group_id = var_detail.groupid
               AND c7010_void_fl IS NULL
               AND c901_price_type = 52063;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               v_twprice := NULL;
         END;

         /* UPDATE t7010_group_price_detail t7010
               SET t7010.c7010_price = get_limit_price_value (p_priceband, v_twprice)
                , t7010.c7010_last_updated_by = p_userid
                , t7010.c7010_last_updated_date = CURRENT_DATE
             WHERE t7010.c4010_group_id = var_detail.groupid
               AND t7010.c901_price_type = p_price_type   --52061, 52062
               AND t7010.c7010_void_fl IS NULL;*/
         IF v_twprice IS NOT NULL
         THEN
            gm_pd_sav_group_price_detail
                                        (var_detail.groupid,
                                         get_limit_price_value (p_priceband,
                                                                v_twprice
                                                               ),
                                         p_price_type,
                                         p_userid
                                        );
         END IF;
      END LOOP;

      FOR var_part_detail IN cur_part_detail
      LOOP
         BEGIN
            SELECT get_part_price(var_part_detail.pnum,'E')
              INTO v_twprice
              FROM t205_part_number t205
             WHERE t205.c205_part_number_id = var_part_detail.pnum;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               v_twprice := NULL;
         END;

         IF v_twprice IS NOT NULL
         THEN
            gm_pd_sav_part_price_detail (var_part_detail.pnum,
                                         get_limit_price_value (p_priceband,
                                                                v_twprice
                                                               ),
                                         p_price_type,
                                         p_userid
                                        );
         END IF;
      END LOOP;
   END gm_sav_limit_price;

   /*************************************************************
      Description : fetch AD/VP Price Band Detail
      ****************************************************************/
   PROCEDURE gm_fch_price_band (p_out_dtls OUT TYPES.cursor_type)
   AS
   BEGIN
      OPEN p_out_dtls
       FOR
          SELECT   c207_set_id setid, get_set_name (c207_set_id) NAME,
                   MAX (DECODE (c901_type, 20179, c240_value, NULL)) ad,
                   MAX (DECODE (c901_type, 20180, c240_value, NULL)) vp,
                   MAX (DECODE (c901_type, 20181, c240_value, NULL))
                                                                    priority
              FROM t240_baseline_value t240
             WHERE t240.c207_set_id IN (
                      SELECT c207_set_id
                        FROM t240_baseline_value
                       WHERE c901_type IN (20175, 20179, 20180, 20181)
                         AND c240_void_fl IS NULL)
               AND c240_void_fl IS NULL
          GROUP BY c207_set_id
          ORDER BY priority;
   END gm_fch_price_band;

   /******************************************************************
     * Description : Procedure to save group List, AD, VP, tripwire Price
     ********************************************************************/
   PROCEDURE gm_pd_sav_group_price_detail (
      p_groupid      IN   t4010_group.c4010_group_id%TYPE,
      p_price        IN   t7010_group_price_detail.c7010_price%TYPE,
      p_price_type   IN   t7010_group_price_detail.c901_price_type%TYPE,
      p_userid       IN   t4010_group.c4010_created_by%TYPE,
      p_comments     IN   t7010_group_price_detail.C70101_LAST_COMMENT%TYPE  DEFAULT NULL
   )
   AS
   	v_current_price t7010_group_price_detail.c7010_price%TYPE;
   	v_company_id NUMBER;
   BEGIN
	   SELECT get_compid_frm_cntx() INTO v_company_id  FROM dual;
	   BEGIN
		   SELECT c7010_price INTO v_current_price FROM t7010_group_price_detail
		   WHERE c4010_group_id = p_groupid AND c901_price_type = p_price_type AND C1900_COMPANY_ID=v_company_id;
	   EXCEPTION WHEN NO_DATA_FOUND
	   THEN
	   		v_current_price := 0;
	   END;
  			
            
	   
	   --When the New Price is different than the current price ,we should update the record.
	   --This check is done to prevent updating of List/TWP when user is updating only one among them.
	   IF ( NVL(v_current_price,'-9999') <> NVL(p_price,'-9999') )
	   THEN
		    --When there is price change and there is no comments entered in UI, throw App error.
		     IF p_comments IS NULL 
			 THEN
				raise_application_error (-20603, '');
			 END IF;

			 UPDATE t7010_group_price_detail
		         SET c7010_price = p_price,
		         	 c70101_last_comment = p_comments,
		             c7010_last_updated_by = p_userid,
		             c7010_last_updated_date = CURRENT_DATE
		       WHERE c4010_group_id = p_groupid 
		         AND c901_price_type = p_price_type
		         AND C1900_COMPANY_ID = v_company_id ;
		
		      IF (SQL%ROWCOUNT = 0)
		      THEN
		         INSERT INTO t7010_group_price_detail
		                     (c7010_group_price_detail_id, c4010_group_id,
		                      c901_price_type, c7010_price, c7010_created_by,
		                      c7010_created_date, c70101_last_comment,C1900_COMPANY_ID
		                     )
		              VALUES (s7010_group_price_detail.NEXTVAL, p_groupid,
		                      p_price_type, p_price, p_userid,
		                      CURRENT_DATE, p_comments,v_company_id
		                     );
		      END IF;
	  END IF;
   END gm_pd_sav_group_price_detail;

   /*************************************************************
      Description : publish validation
      ****************************************************************/
   PROCEDURE gm_publish_validate (
      p_groupid   IN   t4010_group.c4010_group_id%TYPE,
      p_lprice IN t7010_group_price_detail.c7010_price%TYPE,
      p_twprice IN t7010_group_price_detail.c7010_price%TYPE
   )
   AS
      v_count          NUMBER;
      v_group_type     VARCHAR2 (1);
      v_construct_fl   VARCHAR2 (1);
      v_adprice        t7010_group_price_detail.c7010_price%TYPE;
      v_vpprice        t7010_group_price_detail.c7010_price%TYPE;
      v_adprice_band   VARCHAR2 (1);
      v_vpprice_band   VARCHAR2 (1);
      v_priced_type    t4010_group.c901_priced_type%TYPE;
   BEGIN
         ---group's type not be set, group can not be published.-----
      IF p_lprice=0 OR p_twprice=0 OR p_lprice='' or p_twprice=''
      THEN
        raise_application_error (-20263, '');
      --    p_errmsg := 'Group type not be set ';
      END IF;
      
      SELECT t4010.c901_priced_type
        INTO v_priced_type
        FROM t4010_group t4010
       WHERE t4010.c4010_group_id = p_groupid
         AND t4010.c901_group_type IS NOT NULL
         AND t4010.c4010_void_fl IS NULL;

      IF v_priced_type = 52111                                          --part
      THEN
         gm_part_price_validate (p_groupid);
      END IF;

      v_group_type := get_group_type (p_groupid);

      IF (v_group_type = 'N')
      THEN
         raise_application_error (-20263, '');
      --    p_errmsg := 'Group type not be set ';
      END IF;

         ---no construct is   set for the group's system, group can not be published.-----
      /* SELECT COUNT (1)
            INTO v_count
            FROM t7004_construct_mapping t7004
          WHERE t7004.c4010_group_id = p_groupid;*/
      /*--commenting below validation for PMT-16107
       * v_construct_fl := get_construct_fl (p_groupid);

      IF (v_construct_fl = 'N')
      THEN
         raise_application_error (-20264, '');
      --    p_errmsg := p_errmsg || ' or No construct is set for the system of group ';
      END IF;*/

      ---------AD limit price or VP limit price not exist, group can not be published-------
      v_adprice := get_base_group_price (p_groupid, 52061);
      v_vpprice := get_base_group_price (p_groupid, 52062);

      IF (v_priced_type = 52110 AND (v_adprice = 0 OR v_vpprice = 0))
      THEN
         raise_application_error (-20266, '');
      --    p_errmsg := p_errmsg || ' or No construct is set for the system of group ';
      END IF;

         --group's system price band not set, group can not be published-----
      /* SELECT COUNT (1)
           INTO v_count
           FROM t240_baseline_value t240
          WHERE t240.c207_set_id = (SELECT t4010.c207_set_id
                               FROM t4010_group t4010
                              WHERE t4010.c4010_group_id = v_group_id)
            AND t240.c901_type = 20179
            AND t240.c240_value IS NOT NULL
            AND t240.c240_void_fl IS NULL;

         SELECT COUNT (1)
           INTO v_count1
           FROM t240_baseline_value t240
          WHERE t240.c207_set_id = (SELECT t4010.c207_set_id
                               FROM t4010_group t4010
                              WHERE t4010.c4010_group_id = v_group_id)
            AND t240.c901_type = 20180
            AND t240.c240_value IS NOT NULL
            AND t240.c240_void_fl IS NULL;*/
      v_adprice_band := get_price_band (p_groupid, 20179);
      v_vpprice_band := get_price_band (p_groupid, 20180);

      IF (v_adprice_band = 'N' OR v_vpprice_band = 'N')
      THEN
         raise_application_error (-20265, '');
      --    p_errmsg := p_errmsg || ' or system of group price band not set ';
      END IF;
   END gm_publish_validate;

   /*************************************************************
      Description : part price validation
      ****************************************************************/
   PROCEDURE gm_part_price_validate (
      p_groupid   IN   t4010_group.c4010_group_id%TYPE
   )
   AS
      v_count   NUMBER;
   BEGIN
      SELECT COUNT (1)
        INTO v_count
        FROM t4011_group_detail t4011
       WHERE t4011.c4010_group_id = p_groupid
         AND (   get_part_price (t4011.c205_part_number_id, 'L') IS NULL
              OR get_part_price (t4011.c205_part_number_id, 'E') IS NULL
             );

      IF (v_count > 0)
      THEN
         raise_application_error (-20265, '');
      --    p_errmsg := p_errmsg || ' part list price or tripwire price is null, can't be published ';
      END IF;
   END gm_part_price_validate;

   /*****************************************************************************
          Description   : this function returns price for group AD, VP price band
          Parameters : p_groupid, p_type
          *****************************************************************************/
   FUNCTION get_price_band (
      p_groupid   IN   t4010_group.c4010_group_id%TYPE,
      p_type      IN   t240_baseline_value.c901_type%TYPE
   )
      RETURN VARCHAR2
   IS
      v_price_band   VARCHAR2 (1);
      v_value NUMBER;
      v_type VARCHAR(10);
   BEGIN
      BEGIN
         SELECT 'Y'
           INTO v_price_band
           FROM t240_baseline_value t240
          WHERE t240.c207_set_id = (SELECT t4010.c207_set_id
                                      FROM t4010_group t4010
                                     WHERE t4010.c4010_group_id = p_groupid)
            AND t240.c901_type = p_type                                --20179
            AND t240.c240_value IS NOT NULL
            AND t240.c240_void_fl IS NULL;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
         SELECT DECODE(p_type,20179,'ADLIMIT',20180,'VPLIMIT',NULL) INTO v_type FROM DUAL;
         v_value := get_rule_value(p_type,v_type);
         IF v_value > 0 THEN
            RETURN 'Y';
         ELSE
            RETURN 'N';
         END IF;
      END;

      RETURN v_price_band;
   END get_price_band;

   /************************************************************
            Description    : this function returns group type
            Parameters : p_groupid
            **********************************************************/
   FUNCTION get_group_type (p_groupid IN t4010_group.c4010_group_id%TYPE)
      RETURN VARCHAR2
   IS
      v_group_type   VARCHAR2 (1);
   BEGIN
      BEGIN
         SELECT 'Y'
           INTO v_group_type
           FROM t4010_group t4010
          WHERE t4010.c4010_group_id = p_groupid
            AND t4010.c901_group_type IS NOT NULL
            AND t4010.c4010_void_fl IS NULL;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            RETURN 'N';
      END;

      RETURN v_group_type;
   END get_group_type;

   /************************************************************
          Description : this function returns construct FL
          Parameters : p_groupid
          **********************************************************/
   FUNCTION get_construct_fl (p_groupid IN t4010_group.c4010_group_id%TYPE)
      RETURN VARCHAR2
   IS
      v_construct_fl   VARCHAR2 (1);
      v_cnt            NUMBER;
      v_system_id      t207_set_master.c207_set_id%TYPE;
   BEGIN
      BEGIN
         SELECT c207_set_id
           INTO v_system_id
           FROM t4010_group t4010
          WHERE t4010.c4010_group_id = p_groupid
            AND t4010.c4010_void_fl IS NULL;

         SELECT COUNT (1)
           INTO v_cnt
           FROM t7003_system_construct t7003
          WHERE t7003.c207_set_id = v_system_id
                AND t7003.c7003_void_fl IS NULL
                AND t7003.c901_status = 52071;

         IF (v_cnt > 0)
         THEN
            v_construct_fl := 'Y';
         ELSE
            v_construct_fl := 'N';
         END IF;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            v_construct_fl := 'N';
      END;

      RETURN v_construct_fl;
   END get_construct_fl;

   /************************************************************
          Description : this function returns publish FL
          Parameters : p_groupid
          **********************************************************/
   FUNCTION get_publish_fl (p_groupid IN t4010_group.c4010_group_id%TYPE)
      RETURN VARCHAR2
   IS
      v_publish_fl   VARCHAR2 (1);
   BEGIN
      BEGIN
         SELECT t4010.c4010_publish_fl
           INTO v_publish_fl
           FROM t4010_group t4010
          WHERE t4010.c4010_group_id = p_groupid
            AND t4010.c4010_void_fl IS NULL;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            RETURN 'N';
      END;

      RETURN v_publish_fl;
   END get_publish_fl;

   /*****************************************************************************
            Description    : this function returns price for group AD, VP limit price
            Parameters : p_groupid, p_type
          *****************************************************************************/
   FUNCTION get_baseline_value (
      p_groupid   IN   t4010_group.c4010_group_id%TYPE,
      p_type      IN   t7010_group_price_detail.c901_price_type%TYPE
   )
      RETURN NUMBER
   IS
      v_value   t240_baseline_value.c240_value%TYPE;
   BEGIN
      --
      BEGIN
         SELECT c240_value
           INTO v_value
           FROM t240_baseline_value
          WHERE c207_set_id =
                   (SELECT c207_set_id
                      FROM t4010_group
                     WHERE c4010_group_id = p_groupid
                           AND c4010_void_fl IS NULL)
            AND c901_type = p_type;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            RETURN 0;
      END;

      RETURN v_value;
--
   END get_baseline_value;

   /*****************************************************************************
            Description    : this function returns AD, VP limit price when price band
            changed or tripwire price changed
            Parameters : p_price
          *****************************************************************************/
   FUNCTION get_limit_price_value (
      p_price_band   IN   t240_baseline_value.c240_value%TYPE,
      p_twprice      IN   t7010_group_price_detail.c7010_price%TYPE
   )
      RETURN NUMBER
   IS
      v_price   t7010_group_price_detail.c7010_price%TYPE;
   BEGIN
      v_price := (1 + p_price_band / 100) * p_twprice;
      RETURN v_price;
   END get_limit_price_value;

   /*****************************************************************
      * Description : Procedure to fetch the group part status report
      * Author     : Xun Qu
      ***************************************************************/
  
   PROCEDURE gm_pd_fch_status_report (
      p_refid       IN       VARCHAR2,
      p_reftype     IN       VARCHAR2,
      p_listp       IN       t7010_group_price_detail.c7010_price%TYPE,
      p_twp         IN       t7010_group_price_detail.c7010_price%TYPE,
      p_outdetail   OUT      TYPES.cursor_type
   )
   AS
      v_count           NUMBER;
      v_adprice_band    VARCHAR2 (1);
      v_vpprice_band    VARCHAR2 (1);
      v_group_id        VARCHAR2 (10);
      v_group_nm        VARCHAR2 (100);
      v_price_band      VARCHAR2 (1);
      v_group_type      VARCHAR2 (1);
      v_group_publish   VARCHAR2 (1);
      v_construct_fl    VARCHAR2 (1);
      v_owner           VARCHAR2 (50);
   BEGIN
      v_group_id := p_refid;

      ----Price Band-----
      IF (p_reftype = 'part')
      THEN
         BEGIN
            SELECT c4010_group_id
              INTO v_group_id
              FROM t4011_group_detail
             WHERE c205_part_number_id = p_refid
               AND c901_part_pricing_type = 52080;                   --primary
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               v_group_id := NULL;
         END;
      END IF;

      IF v_group_id IS NOT NULL
      THEN
         BEGIN
            SELECT get_user_name
                                (get_group_primary_owner (t4010.c4010_group_id)
                                )
              INTO v_owner
              FROM t4010_group t4010
             WHERE t4010.c4010_group_id = v_group_id
               AND t4010.c4010_void_fl IS NULL;
         /* SELECT DISTINCT get_user_name (t4020.c4020_primary_user) owner
                      INTO v_owner
                      FROM t4020_demand_master t4020, t4021_demand_mapping t4021
                     WHERE t4021.c4021_ref_id = v_group_id
                     AND t4020.c4020_demand_master_id = t4021.c4020_demand_master_id
                     AND t4020.c4020_void_fl IS NULL
                     AND t4020.c901_demand_type = 40020;
         */
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               v_owner := NULL;
         END;
      END IF;

      v_group_nm := get_group_name (v_group_id);
      -------price band------
      v_price_band := get_price_band_fl (v_group_id);
      ----Group Type-----
      v_group_type := get_group_type (v_group_id);
      ----Group Publish-----
      v_group_publish := get_publish_fl (v_group_id);
      ----Construct Fl-----
      v_construct_fl := get_construct_fl (v_group_id);
      
       SELECT count(T4021D.C4020_DEMAND_MASTER_ID) 
            INTO v_count
           FROM T4021_DEMAND_MAPPING T4021D, T901_CODE_LOOKUP T901
          WHERE T4021D.C4021_REF_ID = T901.C901_CODE_ID
            AND T4021D.C901_REF_TYPE = 40032
            AND T901.C902_CODE_NM_ALT <> 'OUS'
            AND T4021D.C4020_DEMAND_MASTER_ID IN (
              SELECT T4021.C4020_DEMAND_MASTER_ID
               FROM T4021_DEMAND_MAPPING T4021, T4020_DEMAND_MASTER T4020
              WHERE T4020.C4020_DEMAND_MASTER_ID = T4021.C4020_DEMAND_MASTER_ID
                AND T4021.C4021_REF_ID =  v_group_id 
                AND T4021.C901_REF_TYPE = 40030
                AND T4020.C4020_VOID_FL IS NULL
              );
     
      OPEN p_outdetail
       FOR
          SELECT NVL (v_price_band, 'N') priceband,
                 NVL (v_group_nm, 'N') groupnm,
                 NVL (v_group_type, 'N') grouptype,
                 NVL (v_group_publish, 'N') publishfl,
                 NVL (v_construct_fl, 'N') constructfl,
                 NVL (v_owner, 'N') owner,
                 DECODE(p_listp, NULL, 'N', 0, 'N', 'Y') lp,
                 DECODE(p_twp, NULL, 'N', 0, 'N', 'Y') twp,
                 DECODE(v_count,0,'N','Y') nonous
            FROM DUAL;
   END gm_pd_fch_status_report;
/*****************************************************************
      * Description : Procedure to fetch the part status report
      * Author     : Himanshu Patel
      ***************************************************************/
   PROCEDURE gm_pd_fch_part_status_report (
      p_refid       IN       VARCHAR2,
      p_reftype     IN       VARCHAR2,
      p_outdetail   OUT      TYPES.cursor_type
   )
   AS
      v_group_id        VARCHAR2 (10);
      v_group_nm        VARCHAR2 (50);
      v_group_type      VARCHAR2 (1);
      v_group_publish   VARCHAR2 (1);
      v_construct_fl    VARCHAR2 (1);
      v_owner           VARCHAR2 (50);
      v_price_band      VARCHAR2 (1);
      v_count           NUMBER;
   BEGIN
      v_group_id := p_refid;

     
      IF v_group_id IS NOT NULL
      THEN
         BEGIN
            SELECT get_user_name
                                (get_group_primary_owner (t4010.c4010_group_id)
                                )
              INTO v_owner
              FROM t4010_group t4010
             WHERE t4010.c4010_group_id = v_group_id
               AND t4010.c4010_void_fl IS NULL;
         /* SELECT DISTINCT get_user_name (t4020.c4020_primary_user) owner
                      INTO v_owner
                      FROM t4020_demand_master t4020, t4021_demand_mapping t4021
                     WHERE t4021.c4021_ref_id = v_group_id
                     AND t4020.c4020_demand_master_id = t4021.c4020_demand_master_id
                     AND t4020.c4020_void_fl IS NULL
                     AND t4020.c901_demand_type = 40020;
         */
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               v_owner := NULL;
         END;
      END IF;

      v_group_nm := get_group_name (v_group_id);
       -------price band------
      v_price_band := get_price_band_fl (v_group_id);
      ----Group Type-----
      v_group_type := get_group_type (v_group_id);
      ----Group Publish-----
      v_group_publish := get_publish_fl (v_group_id);
      ----Construct Fl-----
      v_construct_fl := get_construct_fl (v_group_id);

     SELECT count(T4021D.C4020_DEMAND_MASTER_ID) 
                INTO v_count
               FROM T4021_DEMAND_MAPPING T4021D, T901_CODE_LOOKUP T901
              WHERE T4021D.C4021_REF_ID = T901.C901_CODE_ID
                AND T4021D.C901_REF_TYPE = 40032
                AND T901.C902_CODE_NM_ALT <> 'OUS'
                AND T4021D.C4020_DEMAND_MASTER_ID IN (
                  SELECT T4021.C4020_DEMAND_MASTER_ID
                   FROM T4021_DEMAND_MAPPING T4021, T4020_DEMAND_MASTER T4020
                  WHERE T4020.C4020_DEMAND_MASTER_ID = T4021.C4020_DEMAND_MASTER_ID
                    AND T4021.C4021_REF_ID =  v_group_id 
                    AND T4021.C901_REF_TYPE = 40030
                    AND T4020.C4020_VOID_FL IS NULL
                  );
     
      OPEN p_outdetail
       FOR
          SELECT t4011.c205_part_number_id pnum, get_partnum_desc (t4011.c205_part_number_id) pdesc,
                   DECODE(get_part_price (t4011.c205_part_number_id, 'L'),NULL,'N',0,'N','Y') listp,
                   DECODE(get_part_price (t4011.c205_part_number_id, 'E'),NULL,'N',0,'N','Y') tw,
                   NVL (v_group_type, 'N') grouptype,
                   NVL (v_group_publish, 'N') publishfl,
                   NVL (v_price_band, 'N') pricebandfl,
                   NVL (v_construct_fl, 'N') constructfl,
                   v_owner owner,
                   DECODE(v_count,0,'N','Y') nonous
        FROM t4011_group_detail t4011
       WHERE t4011.c4010_group_id = v_group_id;
   END gm_pd_fch_part_status_report;

   /*****************************************************************************
            Description    : this function returns price for group AD, VP price band
            Parameters : p_groupid
            *****************************************************************************/
   FUNCTION get_price_band_fl (p_groupid IN t4010_group.c4010_group_id%TYPE)
      RETURN VARCHAR2
   IS
      v_price_band_fl   VARCHAR2 (1);
      v_adprice_band    VARCHAR2 (1);
      v_vpprice_band    VARCHAR2 (1);
   BEGIN
      v_adprice_band := get_price_band (p_groupid, 20179);
      v_vpprice_band := get_price_band (p_groupid, 20180);

      IF (v_adprice_band = 'Y' AND v_vpprice_band = 'Y')
      THEN
         v_price_band_fl := 'Y';
      ELSE
         v_price_band_fl := 'N';
      END IF;

      RETURN v_price_band_fl;
   END get_price_band_fl;

/*****************************************************************************
   Description : This is common procedure to updateAD, VP price price limit for parts
*****************************************************************************/
   PROCEDURE gm_pd_sav_part_price_detail (
      p_partnum      IN   t205_part_number.c205_part_number_id%TYPE,
      p_price        IN   t205_part_number.c205_loaner_price%TYPE,
      p_price_type   IN   t7010_group_price_detail.c901_price_type%TYPE,
      p_userid       IN   t205_part_number.c205_last_updated_by%TYPE,
      p_comments     IN   t205_part_number.C205_PRICE_COMMENTS%TYPE  DEFAULT NULL
   )
   AS
   v_eprice t205_part_number.c205_equity_price%TYPE;
    v_lprice t205_part_number.c205_loaner_price%TYPE;
    v_cprice t205_part_number.c205_consignment_price%TYPE;
    v_liprice t205_part_number.c205_list_price%TYPE;
    v_company_id        NUMBER;
   BEGIN
	   --Getting company id from context.   
    SELECT get_compid_frm_cntx()
	  INTO v_company_id 
	  FROM dual;
	   
	   
	  SELECT DECODE (p_price_type, 52060, p_price,C2052_LIST_PRICE) 
       INTO v_liprice FROM T2052_PART_PRICE_MAPPING 
      WHERE C205_PART_NUMBER_ID = p_partnum
        AND C1900_COMPANY_ID = v_company_id
        AND C2052_VOID_FL IS NULL;
                         
      SELECT DECODE (p_price_type, 52063, p_price,C2052_EQUITY_PRICE ) 
       INTO v_eprice FROM T2052_PART_PRICE_MAPPING 
      WHERE C205_PART_NUMBER_ID = p_partnum 
        AND C1900_COMPANY_ID = v_company_id
        AND C2052_VOID_FL IS NULL;
                             
      SELECT DECODE (p_price_type, 52061, p_price ,C2052_LOANER_PRICE) 
       INTO v_lprice FROM T2052_PART_PRICE_MAPPING 
      WHERE C205_PART_NUMBER_ID = p_partnum 
        AND C1900_COMPANY_ID = v_company_id
        AND C2052_VOID_FL IS NULL;
       
      SELECT DECODE (p_price_type, 52062, p_price,C2052_CONSIGNMENT_PRICE) 
       INTO v_cprice FROM T2052_PART_PRICE_MAPPING 
      WHERE C205_PART_NUMBER_ID = p_partnum 
        AND C1900_COMPANY_ID = v_company_id
        AND C2052_VOID_FL IS NULL;
      
--By using the below procedure we are creating  the record for part price in T2052_PART_PRICE_MAPPING.
       gm_pkg_pd_part_pricing.gm_sav_part_pricing(p_partnum,v_eprice,v_lprice,v_cprice,v_liprice,v_company_id,p_userid,p_comments);
   END gm_pd_sav_part_price_detail;

/******************************************************************************
 Description      : THIS FUNCTION RETURNS Y or N based on Part entry in PART_PRICE_LOG
 Parameters       : p_pnum,p_groupid
******************************************************************************/
   FUNCTION get_part_history_flag (
      p_pnum      t4011_group_detail.c205_part_number_id%TYPE,
      p_groupid   t4010_group.c4010_group_id%TYPE
   )
      RETURN VARCHAR2
   IS
      v_count          NUMBER;
      v_history_flag   VARCHAR2 (1);
   BEGIN
      SELECT COUNT (1)
        INTO v_count
        FROM t7011_part_price_log
       WHERE c205_part_number_id = p_pnum
         AND c4010_group_id = p_groupid
         AND c7011_void_fl IS NULL;

      IF v_count = 0
      THEN
         v_history_flag := 'N';
      ELSE
         v_history_flag := 'Y';
      END IF;

      RETURN v_history_flag;
   END get_part_history_flag;

/*****************************************************************************
   Description : This is procedure to add records audit event for group part mapping
*****************************************************************************/
   PROCEDURE gm_pd_sav_part_price_log (
      p_group_detail_id     t4011_group_detail.c4011_group_detail_id%TYPE,
      p_partnum             t4011_group_detail.c205_part_number_id%TYPE,
      p_group_id            t4011_group_detail.c4010_group_id%TYPE,
      p_part_pricing_type   t4011_group_detail.c901_part_pricing_type%TYPE,
      p_log_type            t7011_part_price_log.c901_log_type%TYPE,
      p_created_by          t4010_group.c4010_created_by%TYPE,
      p_created_date        t4010_group.c4010_created_date%TYPE,
      p_last_updated_by     t4011_group_detail.c4011_last_updated_by%TYPE
   )
   AS
      v_seq_id   NUMBER;
   BEGIN
      SELECT s7011_part_price_log.NEXTVAL
        INTO v_seq_id
        FROM DUAL;

      INSERT INTO t7011_part_price_log
                  (c7011_part_price_log_id, c4010_group_id,
                   c205_part_number_id, c901_part_pricing_type,
                   c901_log_type, c7011_created_by, c7011_created_date,
                   c7011_action_by, c7011_action_date
                  )
           VALUES (v_seq_id, p_group_id,
                   p_partnum, p_part_pricing_type,
                   p_log_type, p_created_by, p_created_date,
                   p_last_updated_by, CURRENT_DATE
                  );

      INSERT INTO t7012_part_price_log_detail
                  (c7012_part_price_log_detail_id, c7011_part_price_log_id,
                   c901_price_type, c7012_price)
         SELECT s7012_part_price_log_detail.NEXTVAL, v_seq_id,
                t7010.c901_price_type, t7010.c7010_price
           FROM t7010_group_price_detail t7010
          WHERE t7010.c4010_group_id = p_group_id;
   END gm_pd_sav_part_price_log;

   /******************************************************************
      Description : This procedure is used to fetch AD/VP limits for groups
      ****************************************************************/
   PROCEDURE gm_fch_ad_vp_for_groups (
      p_group_inputstr   IN       VARCHAR2,
      p_groups           OUT      TYPES.cursor_type
   )
   AS
   BEGIN
      my_context.set_my_inlist_ctx (p_group_inputstr);

      OPEN p_groups
       FOR
          SELECT DISTINCT c4010_group_id groupid,
                          gm_pkg_pd_group_pricing.get_base_group_price
                                                     (c4010_group_id,
                                                      52061
                                                     ) adprice,
                          gm_pkg_pd_group_pricing.get_base_group_price
                                                     (c4010_group_id,
                                                      52062
                                                     ) vpprice
                     FROM t7010_group_price_detail
                    WHERE c4010_group_id IN (SELECT token
                                               FROM v_in_list);
   END gm_fch_ad_vp_for_groups;

   /******************************************************************
      Description : This procedure is used to fetch AD/VP limits for parts
      ****************************************************************/
   PROCEDURE gm_fch_ad_vp_for_parts (
      p_part_inputstr   IN       VARCHAR2,
      p_parts           OUT      TYPES.cursor_type
   )
   AS
   BEGIN
      my_context.set_my_inlist_ctx (p_part_inputstr);

      OPEN p_parts
       FOR
          SELECT DISTINCT c205_part_number_id partnumid,
                          c205_loaner_price adprice,
                          c205_consignment_price vpprice
                     FROM t205_part_number
                    WHERE c205_part_number_id IN (SELECT token
                                                    FROM v_in_list);
   END gm_fch_ad_vp_for_parts;

   /******************************************************************************
    Description      : THIS FUNCTION RETURNS Y or N based on Primary Part lock is set or unset
    Parameters       : p_pnum
   ******************************************************************************/
   FUNCTION get_part_primary_lock_flag (
      p_pnum   t4011_group_detail.c205_part_number_id%TYPE
   )
      RETURN VARCHAR2
   IS
      v_primary_lock_flag   VARCHAR2 (1);
   BEGIN
      BEGIN
         SELECT c4011_primary_part_lock_fl
           INTO v_primary_lock_flag
           FROM t4011_group_detail t4011, t4010_group t4010
          WHERE t4011.c205_part_number_id = p_pnum
            AND t4011.c4010_group_id = t4010.c4010_group_id
            AND t4011.c901_part_pricing_type = 52080
            AND t4010.c4010_void_fl IS NULL;

         RETURN v_primary_lock_flag;
      END;
   EXCEPTION
      WHEN OTHERS
      THEN
         v_primary_lock_flag := 'N';
         RETURN v_primary_lock_flag;
   END get_part_primary_lock_flag;

/******************************************************************************
 Description      : THIS FUNCTION RETURNS Primary Group Owner from Group ID.
 Parameters       : p_groupid
******************************************************************************/
   FUNCTION get_group_primary_owner (
      p_groupid   t4010_group.c4010_group_id%TYPE
   )
      RETURN t4020_demand_master.c4020_primary_user%TYPE
   IS
      v_count      NUMBER;
      v_owner_id   t4020_demand_master.c4020_primary_user%TYPE;
   BEGIN
      SELECT t4020.c4020_primary_user
        INTO v_owner_id
        FROM t4020_demand_master t4020,
             t4010_group t4010,
             t4021_demand_mapping t4021
       WHERE t4020.c4020_demand_master_id = t4021.c4020_demand_master_id
         AND TO_CHAR (t4010.c4010_group_id) = t4021.c4021_ref_id
         AND t4020.C901_DEMAND_TYPE = 4000103 --Demand Sheet Template
         AND t4010.c4010_void_fl IS NULL
         AND t4020.c4020_void_fl IS NULL
         AND t4010.c4010_group_id = p_groupid;

      RETURN v_owner_id;
   EXCEPTION
      WHEN TOO_MANY_ROWS
      THEN
         BEGIN
            SELECT t4020.c4020_primary_user ownerid
              INTO v_owner_id
              FROM t4021_demand_mapping t4021_1, t4020_demand_master t4020
             WHERE t4021_1.c901_ref_type = 40030
               AND t4020.C901_DEMAND_TYPE = 4000103 --Demand Sheet Template
               AND t4020.c4020_demand_master_id =
                                                t4021_1.c4020_demand_master_id
               AND t4021_1.c4021_ref_id = to_char(p_groupid)
               AND c4020_void_fl IS NULL
               AND t4021_1.c4020_demand_master_id IN (
                      SELECT t4021.c4020_demand_master_id
                        FROM t4021_demand_mapping t4021
                       WHERE t4021.c901_ref_type = 40032
                         AND t4021.c4021_ref_id IN (
                                SELECT t901.c901_code_id
                                  FROM t901_code_lookup t901
                                 WHERE t901.c901_code_grp = 'REGN'
                                   AND t901.c902_code_nm_alt NOT IN ('OUS')));

            RETURN v_owner_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               v_owner_id := NULL;
               RETURN v_owner_id;
         END;
      WHEN OTHERS
      THEN
         v_owner_id := NULL;
         RETURN v_owner_id;
   END get_group_primary_owner;

      /******************************************************************************
    Description      : THIS FUNCTION RETURNS average selling price of a group.
    Parameters       : p_groupid
   ******************************************************************************/
   FUNCTION get_group_avg_selling_price (
      p_groupid   IN   t4010_group.c4010_group_id%TYPE
   )
      RETURN NUMBER
   IS
      v_avg_part_price   NUMBER (10, 2);
      v_grp_price        NUMBER (10, 2);
      v_avg_grp_price    NUMBER (10, 2);
      v_part_cnt         NUMBER;
   BEGIN
      --
      BEGIN
         v_avg_grp_price := 0;
         v_grp_price := 0;
         v_part_cnt := 0;

         FOR partnum IN (SELECT c205_part_number_id pnum
                           FROM t4011_group_detail t4011
                          WHERE c4010_group_id = p_groupid
                            AND c901_part_pricing_type = '52080')
         LOOP
            BEGIN
               v_avg_part_price := 0;

               SELECT NVL (DECODE(qty,0,0,price) / DECODE(qty,0,1,qty), 0) avgp -- using the decode to avert Divisor == 0 error
                 INTO v_avg_part_price
                 FROM (SELECT   t502.c205_part_number_id,
                                SUM (t502.c502_item_qty) qty,
                                SUM (t502.c502_item_qty * t502.c502_item_price
                                    ) price
                           FROM t501_order t501, t502_item_order t502
                          WHERE t501.c501_order_id = t502.c501_order_id
                            AND NVL (t501.c901_order_type, 0) NOT IN (2524)
                            AND NVL (c901_order_type, -9999) NOT IN (
					                SELECT t906.c906_rule_value
					                  FROM t906_rules t906
					                 WHERE t906.c906_rule_grp_id = 'ORDTYPE'
					                   AND c906_rule_id = 'EXCLUDE')
                            AND t501.c501_delete_fl IS NULL
                            AND t501.c501_void_fl IS NULL
						    AND (t501.c901_ext_country_id IS NULL OR t501.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH
                            AND t502.c205_part_number_id = partnum.pnum
                            AND t501.c501_order_date
                                   BETWEEN TRUNC (ADD_MONTHS (CURRENT_DATE, -12),
                                                  'MM'
                                                 )
                                       AND TRUNC
                                               (LAST_DAY (ADD_MONTHS (CURRENT_DATE,
                                                                      -1
                                                                     )
                                                         )
                                               )
                       GROUP BY t502.c205_part_number_id);
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  v_avg_part_price := 0;
            END;

            IF (v_avg_part_price IS NOT NULL AND v_avg_part_price > 0)
            THEN
               v_part_cnt := v_part_cnt + 1;
               v_grp_price := v_grp_price + v_avg_part_price;
            END IF;
         END LOOP;

         IF (v_part_cnt > 0)
         THEN
            v_avg_grp_price := NVL (v_grp_price / v_part_cnt, 0);
         END IF;
      END;

      RETURN v_avg_grp_price;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN 0;
--
   END get_group_avg_selling_price;
   
   FUNCTION get_multi_construct_avail_flg (
   p_construct_id IN  t7003_system_construct.c7003_system_construct_id%TYPE
  )
    return VARCHAR2
    IS
      v_cons_flg varchar2(1);
      v_active_construct NUMBER;
      v_count NUMBER;
      v_cnt_grp NUMBER;
    BEGIN
      v_cons_flg := '';
      SELECT count(1)
       INTO v_active_construct
      FROM t7003_system_construct t7003
       WHERE t7003.c7003_void_fl IS NULL
        AND t7003.c7003_system_construct_id=p_construct_id
        AND t7003.c901_status = 52071;
        --gm_procedure_log('v_active_construct',v_active_construct);
       -- commit;
      IF(v_active_construct > 0)
      THEN
      BEGIN
          SELECT COUNT(1) into v_count FROM t7003_system_construct t7003s  --COUNT(c207_set_id)
           WHERE t7003s.c7003_system_construct_id <> p_construct_id
             AND t7003s.c207_set_id = (SELECT t7003.c207_set_id FROM t7003_system_construct t7003
                                        WHERE t7003.c7003_void_fl IS NULL
                                          AND t7003.c7003_system_construct_id=p_construct_id
                                          AND t7003.c901_status = 52071)
              AND t7003s.c901_status = 52071   
              AND t7003s.c7003_void_fl IS NULL; 
            IF v_count > 0
            THEN 
               v_cons_flg := 'Y';
            ELSE
            BEGIN
                SELECT DISTINCT COUNT(t7004.c4010_group_id)
                  INTO v_cnt_grp
                FROM t7004_construct_mapping t7004, t7003_system_construct t7003, t4010_group t4010
                   WHERE t7003.c7003_system_construct_id = t7004.c7003_system_construct_id
                     AND t7003.c7003_void_fl IS NULL
                     AND t7003.c7003_system_construct_id = p_construct_id
                     AND t7004.c4010_group_id = t4010.c4010_group_id
                     AND t4010.c4010_void_fl IS NULL
                     AND t4010.c4010_inactive_fl IS NULL
                     AND t4010.c4010_publish_fl = 'Y';
                   IF v_cnt_grp > 0
                   THEN 
                      v_cons_flg := 'N'; 
                   ELSE
                      v_cons_flg := 'Y'; 
                   END IF;
              END;
            END IF; -- IF v_count > 0
        END;
      END IF;  --  IF(v_active_construct > 0)
     RETURN v_cons_flg;
    END get_multi_construct_avail_flg;
	/**************************************************************************************************
 	Description : This procedure is used to fetch Groupname, PartNumber details  and Account Details
 	Whenever new part is added into a new group or an existing group
 	**************************************************************************************************/
   PROCEDURE gm_new_group_part_map_email (
        p_group_dtls   OUT TYPES.cursor_type
   	   ,p_part_dtls    OUT TYPES.cursor_type
       ,p_account_dtls OUT TYPES.cursor_type
    )
   AS
   		v_part_ids     VARCHAR2 (4000);
   		
  CURSOR curr_group
      IS 
        SELECT t4011.c205_part_number_id partid
		FROM t4011_group_detail t4011,t4010_group t4010 WHERE t4011.c4011_created_date is not null
		AND  t4011.c4010_group_id = t4010.c4010_group_id
		AND t4010.c4010_void_fl is null
		AND t4010.c4010_publish_fl = 'Y'
		AND t4011.c901_part_pricing_type = 52080 -- Primary Part
		AND t4010.c901_type = 40045 -- Forecast/Demand Group
		AND TRUNC(t4011.c4011_created_date) = TRUNC(CURRENT_DATE);
   BEGIN
	   
	   FOR currindex1 IN curr_group
		LOOP
			v_part_ids := v_part_ids || ',' || currindex1.partid;
		END LOOP;
	
		my_context.set_my_inlist_ctx (v_part_ids);
	   
      OPEN p_group_dtls
       FOR
          SELECT get_group_name(t4010.c4010_group_id) gname, t4010.c4010_group_id g_id
          FROM  t4010_group t4010
		  WHERE t4010.c4010_void_fl IS NULL
		  AND t4010.c901_type = 40045 --Forecast/Demand Group
		  AND t4010.c4010_publish_fl = 'Y'
		  AND t4010.c4010_email_flag is NULL;
		--  AND t4010.c4010_group_publish_date = TRUNC(CURRENT_DATE);
		  
	  OPEN p_part_dtls
       FOR
          SELECT t4011.c205_part_number_id partno,GET_PARTNUM_DESC(t4011.c205_part_number_id) partdesc
          ,get_group_name(t4011.c4010_group_id) gname, get_user_name(c4011_created_by) updateby
		  ,get_part_price(t4011.c205_part_number_id ,'L') lprice , get_part_price (t4011.c205_part_number_id , 'E') tprice, t4011.c4010_group_id g_id
		  FROM t4011_group_detail t4011,t4010_group t4010
		  WHERE t4011.c4011_created_date IS NOT NULL
		  AND t4010.c901_type = 40045 --Forecast/Demand Group
		  AND t4011.c901_part_pricing_type = 52080 -- Primary Part
		  AND  t4011.c4010_group_id = t4010.c4010_group_id
		  AND t4010.c4010_void_fl IS NULL
		  AND t4010.c4010_publish_fl = 'Y'
		  AND t4011.c4011_email_flag IS NULL;
		--  AND TRUNC(t4011.c4011_created_date) = TRUNC(CURRENT_DATE);
		
	  OPEN p_account_dtls 
       FOR
		  SELECT t4011.c205_part_number_id partno,	T7051.C704_ACCOUNT_ID accno,
  		  	GET_ACCOUNT_NAME(T7051.C704_ACCOUNT_ID) aname,
  			NVL(globus_app.get_account_gpo_name(NVL(T7051.c101_gpo_id,get_account_gpo_id(T7051.C704_ACCOUNT_ID))),'') gpoid,
  			DECODE(t704d.c901_contract,903100,'Yes','No') cflag
			FROM t7051_account_group_pricing T7051, t4011_group_detail t4011,  t4010_group t4010, t704d_account_affln t704d
			WHERE t4010.c4010_group_id       = t4011.c4010_group_id
			AND t4011.c901_part_pricing_type = 52080 -- primary part
			AND t4010.c901_type              = 40045 -- Forecast/Demand Group
			AND ((t7051.c901_ref_type 		 = 52000 -- Group Type
			AND T7051.c7051_ref_id   		 = t4011.c4010_group_id)
			OR (t7051.c901_ref_type   		 = 52001 -- Individual Type
			AND T7051.c7051_ref_id  		 = t4011.c205_part_number_id))
			AND T7051.c704_account_id = t704d.c704_account_id (+)
			AND t7051.c7051_void_fl 	IS NULL
			AND t4010.c4010_void_fl     IS NULL
			AND t7051.c7051_active_fl  = 'Y'
			AND t4010.c4010_publish_fl = 'Y'
			AND t704d.c704d_void_fl (+) IS NULL
			AND t4011.c4011_email_flag IS NULL
		--	AND TRUNC(t4011.c4011_created_date) = TRUNC(CURRENT_DATE)
			AND DECODE(t704d.c901_contract,903100,'903100','903101') = '903100' -- Contract Flag is Y
			ORDER by GET_ACCOUNT_NAME(T7051.C704_ACCOUNT_ID);
	
	END gm_new_group_part_map_email;
	
	
/****************************************************************************
* Description : This procedure is log the Group part email notification and used for tracking purpose.
*
*****************************************************************************/

PROCEDURE gm_group_part_map_email_flag 
        (
        p_email_status IN VARCHAR
        )
AS
    v_group_dtls TYPES.cursor_type;
    v_part_dtls TYPES.cursor_type;
    v_acct_dtls TYPES.cursor_type;
    v_grp_nm t4010_group.c4010_GROUP_NM%TYPE;
    v_grp_id t4010_group.c4010_group_id%TYPE;
    v_part_num_id t205_part_number.c205_part_number_id%TYPE;
    v_part_num_desc t205_part_number.c205_part_num_desc%TYPE;
    v_created_by t101_user.c101_user_id%TYPE;
    
    
BEGIN
    
	    gm_pkg_pd_group_pricing.gm_new_group_part_map_email (v_group_dtls, v_part_dtls, v_acct_dtls);

     FETCH v_group_dtls INTO v_grp_nm, v_grp_id;
     LOOP   
         UPDATE t4010_group
        SET c901_email_status = p_email_status, c4010_email_flag = DECODE (p_email_status,106621,'Y', 106622,'F',c4010_email_flag)
          WHERE c4010_group_id       = v_grp_id
            AND c4010_void_fl       IS NULL
            AND c901_type            = 40045 --Forecast/Demand Group
            AND c4010_email_flag IS NULL;
    END LOOP;
    
 CLOSE v_group_dtls;
 
    FETCH v_part_dtls INTO v_part_num_id, v_part_num_desc, v_grp_nm, v_grp_id, v_created_by ;
    LOOP
     UPDATE t4011_group_detail
    SET c901_email_status = p_email_status, c4011_email_flag = DECODE (p_email_status, 106621, 'Y', 106622,'F', c4011_email_flag)
      WHERE c4010_group_id         = v_grp_id
      AND c901_part_pricing_type = 52080
      AND c205_part_number_id=v_part_num_id
      ;
     END LOOP;

 CLOSE v_part_dtls;

END gm_group_part_map_email_flag;

	
/*************************************************************************
* Purpose: This Procedure creates a record in T942_AUDIT_PRICE_LOG.
* 
*************************************************************************/
   PROCEDURE gm_sav_grp_part_price_log (
      p_ref_id       IN   T942_AUDIT_PRICE_LOG.C942_REF_ID%TYPE,    --Group id
      p_type         IN   T942_AUDIT_PRICE_LOG.C901_TYPE%TYPE,      --T901 type
	  p_comments     IN   T942_AUDIT_PRICE_LOG.C942_COMMENTS%TYPE,  --Comments value
      p_value        IN   T942_AUDIT_PRICE_LOG.C942_VALUE%TYPE,     --Price value
      p_trail_id     IN   T942_AUDIT_PRICE_LOG.C940_AUDIT_TRAIL_ID%TYPE,  --T940 audit price table values
      p_updated_by   IN   T942_AUDIT_PRICE_LOG.C942_UPDATED_BY%TYPE,      --Updated by
      p_updated_dt   IN   T942_AUDIT_PRICE_LOG.C942_UPDATED_DATE%TYPE,	  
      p_company_id	 IN	  T942_AUDIT_PRICE_LOG.C1900_COMPANY_ID%TYPE	
   )
   AS     
   BEGIN
   --
      INSERT INTO t942_audit_price_log
                  (c942_audit_price_log_id, c942_ref_id, c942_value,
                  c901_type, c940_audit_trail_id, c942_comments,
                  c942_updated_by, c942_updated_date,C1900_COMPANY_ID
                  )
           VALUES (s942_audit_price_log.NEXTVAL, p_ref_id, p_value,
                   p_type, p_trail_id, p_comments,
                   p_updated_by, p_updated_dt,p_company_id
                  );
   --  
    
   END gm_sav_grp_part_price_log;
   
/**************************************************************************************
 Description      : THIS FUNCTION RETURNS Y or N based on New price and old price
 Parameters       : p_pnum, p_new_price, p_price_type
***************************************************************************************/
   FUNCTION get_part_price_update_flag (
      p_pnum        t205_part_number.C205_PART_NUMBER_ID%TYPE,
      p_new_price   VARCHAR2,
      p_price_type  NUMBER
   )
      RETURN VARCHAR2
   IS
      v_old_price     VARCHAR2(20);
      v_update_flag   VARCHAR2 (1);
      v_company_id  VARCHAR2(20);
   BEGIN
	   
	   SELECT  get_compid_frm_cntx()
    	INTO    v_company_id
    	FROM    DUAL;
    	
      BEGIN                 
	 	  SELECT DECODE(p_price_type,'52060',NVL(C2052_LIST_PRICE,'-9999'),
	 	                             '52061',NVL(C2052_LOANER_PRICE,'-9999'),
	 	                             '52062',NVL(C2052_CONSIGNMENT_PRICE,'-9999'),
	 	                             '52063',NVL(C2052_EQUITY_PRICE,'-9999'),'-9999') 
	 	    INTO v_old_price
		    FROM T2052_PART_PRICE_MAPPING 
		   WHERE c205_part_number_id = p_pnum 
		     AND C1900_COMPANY_ID = v_company_id
		     AND C2052_void_fl is NULL ;
      EXCEPTION
           WHEN NO_DATA_FOUND
           THEN
              RETURN '';
      END;
      
      IF v_old_price <> NVL(p_new_price,'-9999')
      THEN
         v_update_flag := 'Y';
      ELSE
         v_update_flag := 'N';
      END IF;

      RETURN v_update_flag;
      
   END get_part_price_update_flag;
   
   
   
/**************************************************************************************
 Description      : THIS PROCEDURE IS USED FOR VOIDING THE CONSTRUCT IN CONSTRUCT BUILDER  
 Parameters       : p_constructid,p_userid
***************************************************************************************/
   PROCEDURE gm_sav_void_construct (
      p_constructid    IN   T942_AUDIT_PRICE_LOG.C942_REF_ID%TYPE,    --Group id
      p_userid         IN   T101_USER.C101_USER_ID%TYPE     --T901 type
   )
   AS   
     v_pricing_req_cnt  NUMBER;
   BEGIN
   
   SELECT count(T7001.C7000_ACCOUNT_PRICE_REQUEST_ID) INTO v_pricing_req_cnt  
			FROM T7001_GROUP_PART_PRICING T7001 , T7004_CONSTRUCT_MAPPING T7004
			WHERE T7001.C901_REF_TYPE = '52000' -- GROUP
			AND T7001.C7001_REF_ID = T7004.C4010_GROUP_ID
			AND T7004.C7003_SYSTEM_CONSTRUCT_ID = p_constructid
			AND T7001.C7001_VOID_FL IS NULL;
			
			
     IF v_pricing_req_cnt > 0 
     THEN 
     --'Cannot void this construct as it is linked to a Price Request, you can make it Inactive' 
     raise_application_error('-20946','');
    ELSE
          UPDATE t7003_system_construct
             SET C7003_VOID_FL= 'Y',
             c7003_last_updated_by = p_userid,
             c7003_last_updated_date = CURRENT_DATE
             WHERE c7003_system_construct_id = p_constructid; 
    END IF; 
    
   END gm_sav_void_construct;
   
    /******************************************************************
	 Description : fetch the system and part pricing details
	****************************************************************/
	PROCEDURE gm_fch_sys_partprice_dtls (
        p_select_systems  IN		 CLOB
	  , p_part_num		  IN 	   VARCHAR2
      , p_acc_id          IN     t704_account.C704_ACCOUNT_ID%type
      , p_outpartdetail   OUT 	 TYPES.CURSOR_TYPE
	) AS
  	  v_dept_id           VARCHAR2(20);
      v_company_id        NUMBER;
   BEGIN

	--Getting company id from context.   
    SELECT get_compid_frm_cntx()
	  INTO v_company_id 
	  FROM DUAL;
	  
	my_context.set_my_inlist_ctx (p_select_systems);
	OPEN p_outpartdetail FOR 
                    SELECT T205.C205_PART_NUMBER_ID PNUM ,
                    get_partdesc_by_company(T205.C205_PART_NUMBER_ID) PDESC ,
                    T207.C207_SET_ID SYSID,
                    T207.C207_SET_NM SYSNAME ,
                      DECODE(t705_GPO.uprice_gpo,NULL,NVL (to_char(t705.uprice), 0), (to_char(t705_GPO.uprice_gpo)
                    ||'*')) UPRICE ,
                    T2052.C2052_LIST_PRICE LISTP,
                    t2052.c2052_equity_price TW
                  FROM T205_PART_NUMBER T205,
                     (SELECT   t207.c207_set_id , t207.c207_set_nm 
                                    FROM t207_set_master t207, t2080_set_company_mapping t2080
                                   WHERE t207.c901_set_grp_type = 1600
                                     AND c207_void_fl IS NULL
                                     AND c207_obsolete_fl IS NULL
                                     AND t207.c207_set_id = t2080.c207_set_id
                                     AND t2080.c1900_company_id = v_company_id
                                     AND t2080.c2080_void_fl IS NULL
                                     AND c2080_pricing_release_fl='Y') t207,
                    T208_SET_DETAILS t208 ,
                    T2052_PART_PRICE_MAPPING T2052 ,
                    (SELECT C205_PART_NUMBER_ID,
                      T705.C705_UNIT_PRICE UPRICE ,
                      T705.C705_DISCOUNT_OFFERED DOFFERED
                    FROM T705_ACCOUNT_PRICING T705 ,
                      t704_account t704 ,
                      (SELECT DISTINCT ac_id c704_account_id,
                        rep_id c703_sales_rep_id,
                        ad_id c501_ad_id,
                        vp_id c501_vp_id
                      FROM v700_territory_mapping_detail
                      ) t501
                    WHERE t705.c101_party_id = t704.c101_party_id
                    AND T704.C704_ACCOUNT_ID = T501.C704_ACCOUNT_ID
                    AND t704.c704_account_id = p_acc_id --'4533'
                    AND t705.c705_void_fl   IS NULL
                    AND t704.c704_void_fl   IS NULL
                    ) T705 ,
                    (SELECT c205_part_number_id,
                      t705.c705_unit_price uprice_gpo
                    FROM T705_ACCOUNT_PRICING T705
                    WHERE C101_PARTY_ID    = get_account_gpo_id(p_acc_id) --T7051.C704_ACCOUNT_ID
                    AND t705.c705_void_fl IS NULL
                    ) t705_GPO
                  WHERE T205.C205_PART_NUMBER_ID = T705.C205_PART_NUMBER_ID (+)
                  AND t205.c205_part_number_id   = t705_GPO.c205_part_number_id(+)
                  AND T2052.C2052_LIST_PRICE    IS NOT NULL
                  AND T2052.C2052_LIST_PRICE    <> 0
                 AND (t207.c207_set_id                IN
                  (SELECT TOKEN FROM v_in_list
                   )
                 OR REGEXP_LIKE (T205.C205_PART_NUMBER_ID,REGEXP_REPLACE(p_part_num,'[+]','\+')))
                  AND T205.C205_PART_NUMBER_ID = T2052.C205_PART_NUMBER_ID
                  AND T2052.C1900_COMPANY_ID   = v_company_id
                  AND T207.C207_SET_ID         = T208.C207_SET_ID
                  AND t208.C205_PART_NUMBER_ID = T205.C205_PART_NUMBER_ID
                  AND (t705.uprice            <> 0
                  OR t705_gpo.uprice_gpo      <> 0)
                  ORDER BY T207.C207_SET_NM,
                    T205.C205_PART_NUMBER_ID ;
  
  END gm_fch_sys_partprice_dtls;
END gm_pkg_pd_group_pricing;
/
