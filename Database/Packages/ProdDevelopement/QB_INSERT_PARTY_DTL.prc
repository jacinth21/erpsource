/* Formatted on 2009/03/06 15:38 (Formatter Plus v4.8.0) */


CREATE OR REPLACE PROCEDURE qb_insert_party_dtl
AS
	CURSOR c_fetchusers
	IS
		SELECT c101_user_id ID, c101_user_f_name fnm, c101_user_l_name lnm
		  FROM t101_user
		 WHERE c901_user_type = 300;
BEGIN
	FOR rec IN c_fetchusers
	LOOP
		BEGIN
			INSERT INTO t101_party
						(c101_party_id, c101_first_nm, c101_last_nm, c101_created_by, c101_created_date
					   , c901_party_type
						)
				 VALUES (rec.ID, rec.fnm, rec.lnm, '303043', SYSDATE
					   , 7006
						);

			UPDATE t101_user
			   SET c101_party_id = rec.ID
			 WHERE c101_user_id = rec.ID;
		EXCEPTION
			WHEN OTHERS
			THEN
		--303355, 303207, 303332, 303322, 303202
		END;
	END LOOP;
END qb_insert_party_dtl;
/
