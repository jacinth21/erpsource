/* Formatted on 2010/03/24 19:13 (Formatter Plus v4.8.0) */
--@"c:\database\packages\ProdDevelopement\gm_pkg_pd_growth.bdy"

CREATE OR REPLACE PACKAGE BODY gm_pkg_pd_growth
IS
--
/*******************************************************
 * Purpose: Package holds Growth Save and Fetch Methods
 * Created By VPrasath
 *******************************************************/
--
/*******************************************************
 * Purpose: This Procedure is used to fetch the
 * growth references for the given part/set/group
 *******************************************************/
--
	PROCEDURE gm_pd_fch_growth_ref (
		p_demand_id   IN	   t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_ref_type	  IN	   t4021_demand_mapping.c901_ref_type%TYPE
	  , p_ref_ids	  OUT	   TYPES.cursor_type
	)
--
	AS
	BEGIN
-- FOR GROUP
		IF p_ref_type = 20297
		THEN
			OPEN p_ref_ids
			 FOR
				 SELECT c4010_group_id refid, c4010_group_nm refnm
				   FROM t4010_group
				  WHERE c4010_group_id IN (SELECT c4021_ref_id
											 FROM t4021_demand_mapping
											WHERE c4020_demand_master_id = p_demand_id AND c901_ref_type = '40030');
		END IF;

-- FOR PARTS IN GROUPS
		IF p_ref_type = 20295
		THEN
			OPEN p_ref_ids
			 FOR
				 SELECT   t4011.c205_part_number_id refid
						, t4011.c205_part_number_id || ' : ' || get_partnum_desc (t4011.c205_part_number_id) refnm
					 FROM t4011_group_detail t4011
					WHERE c4010_group_id IN (SELECT c4021_ref_id
											   FROM t4021_demand_mapping
											  WHERE c4020_demand_master_id = p_demand_id AND c901_ref_type = '40030')
				 ORDER BY t4011.c205_part_number_id;
		END IF;

-- FOR PARTS IN SETS
		IF p_ref_type = 20298
		THEN
			OPEN p_ref_ids
			 FOR
				 SELECT DISTINCT c205_part_number_id refid
							   , c205_part_number_id || ' : ' || get_partnum_desc (c205_part_number_id) refnm
							FROM t208_set_details
						   WHERE c207_set_id IN (
												 SELECT c4021_ref_id
												   FROM t4021_demand_mapping
												  WHERE c4020_demand_master_id = p_demand_id
														AND c901_ref_type = '40031')
							 AND c208_void_fl IS NULL
						ORDER BY c205_part_number_id;
		END IF;

-- FOR SET
		IF p_ref_type = 20296
		THEN
			OPEN p_ref_ids
			 FOR
				 SELECT c207_set_id refid, (c207_set_id || ' : ' || c207_set_nm) refnm
				   FROM t207_set_master
				  WHERE c207_set_id IN (SELECT c4021_ref_id
										  FROM t4021_demand_mapping
										 WHERE c4020_demand_master_id = p_demand_id AND c901_ref_type = '40031');
		END IF;
	END gm_pd_fch_growth_ref;

--
/*******************************************************
 * Purpose: This Procedure is used to save the
 * growth details
 *******************************************************/
--
	PROCEDURE gm_pd_sav_growth_details (
		p_demand_id   IN	   t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_ref_type	  IN	   t4021_demand_mapping.c901_ref_type%TYPE
	  , p_ref_id	  IN	   t4021_demand_mapping.c4021_ref_id%TYPE
	  , p_inputstr	  IN	   VARCHAR2
	  , p_userid	  IN	   t4030_demand_growth_mapping.c4030_created_by%TYPE
	  , p_growth_id   OUT	   VARCHAR
	)
	AS
		v_cnt		   NUMBER;
		v_dmi_cnt	   NUMBER := 0;
		v_gid		   t4030_demand_growth_mapping.c4030_demand_growth_id%TYPE;
		v_string	   VARCHAR2 (30000) := p_inputstr;
		v_substring    VARCHAR2 (1000);
		v_start_date   VARCHAR2 (20);
		v_end_date	   DATE;
		v_growthvalue  VARCHAR2 (20);
		v_month 	   VARCHAR2 (20);
		v_growth_ref   VARCHAR2 (20);
		v_year		   VARCHAR2 (10);
		v_ref_id	   t4021_demand_mapping.c4021_ref_id%TYPE;
		v_growth_details_id NUMBER := 0;
		v_inputstr	   VARCHAR2 (30000) := p_inputstr;
		v_row_data	   VARCHAR2 (1000);
		v_void_flag    VARCHAR2 (10);
	BEGIN
		IF INSTR (p_inputstr, '$') = 0
		THEN
			v_inputstr	:= p_inputstr || '$';
		END IF;

		WHILE INSTR (v_inputstr, '$') <> 0
		LOOP
			v_row_data	:= SUBSTR (v_inputstr, 1, INSTR (v_inputstr, '$') - 1);
			v_inputstr	:= SUBSTR (v_inputstr, INSTR (v_inputstr, '$') + 1);

			-- p_ref_id will have values like 954.902,954.903 etc.
			WHILE INSTR (v_row_data, '#@#') <> 0
			LOOP
				v_ref_id	:= SUBSTR (v_row_data, 1, INSTR (v_row_data, '#@#') - 1);
				v_void_flag := SUBSTR (v_ref_id, INSTR (v_ref_id, '^') + 1);
				v_ref_id	:= SUBSTR (v_ref_id, 1, INSTR (v_ref_id, '^') - 1);
				v_string	:= SUBSTR (v_row_data, INSTR (v_row_data, '#@#') + 3);
				v_row_data	:= SUBSTR (v_row_data, INSTR (v_row_data, '#@#') + 3);

				SELECT COUNT (c4030_demand_growth_id)
				  INTO v_cnt
				  FROM t4030_demand_growth_mapping t4030
				 WHERE c4020_demand_master_id = p_demand_id
				   AND c901_ref_type = p_ref_type
				   AND c4030_ref_id = v_ref_id
				   AND c4030_void_fl IS NULL;

				IF v_cnt = 0
				THEN
					SELECT s4030_demand_growth_mapping.NEXTVAL
					  INTO v_gid
					  FROM DUAL;

					INSERT INTO t4030_demand_growth_mapping
								(c4030_demand_growth_id, c4020_demand_master_id, c901_ref_type, c4030_ref_id
							   , c4030_created_by, c4030_created_date
								)
						 VALUES (v_gid, p_demand_id, p_ref_type, v_ref_id
							   , p_userid, SYSDATE
								);
				ELSE
					-- If the Growth Mapping Exists, get the Growth ID for future update.
					SELECT c4030_demand_growth_id
					  INTO v_gid
					  FROM t4030_demand_growth_mapping t4030
					 WHERE c4020_demand_master_id = p_demand_id
					   AND c901_ref_type = p_ref_type
					   AND c4030_ref_id = v_ref_id
					   AND t4030.c4030_void_fl IS NULL;

					-- User Has deleted the row from the grid.
					IF NVL (v_void_flag, 'N') = 'Y'
					THEN
						UPDATE t4030_demand_growth_mapping t4030
						   SET t4030.c4030_void_fl = v_void_flag
							 , t4030.c4030_last_updated_by = p_userid
							 , t4030.c4030_last_updated_date = SYSDATE
						 WHERE t4030.c4030_demand_growth_id = v_gid;
					END IF;
				END IF;

				p_growth_id := v_gid;

				WHILE INSTR (v_string, '|') <> 0
				LOOP
					v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
					v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
					--
					v_month 	:= NULL;
					v_growthvalue := NULL;
					v_growth_ref := NULL;
					--
					--07^2009^20381^3|08^2009^20382^4|
					v_month 	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
					v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
					v_year		:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
					v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
					v_growth_ref := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
					v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
					v_growthvalue := v_substring;
					--
					v_start_date := v_month || '/01/' || v_year;

					SELECT LAST_DAY (TO_DATE (v_start_date, 'Mon/DD/YY'))
					  INTO v_end_date
					  FROM DUAL;

					IF p_ref_type = 20295 AND v_growthvalue IS NOT NULL AND NVL (v_void_flag, 'N') != 'Y'
					THEN
						SELECT COUNT (t4030.c4030_ref_id)
						  INTO v_cnt
						  FROM t4030_demand_growth_mapping t4030, t4031_growth_details t4031
						 WHERE t4030.c4030_ref_id IN (SELECT TO_CHAR (t4011.c4010_group_id)
														FROM t4011_group_detail t4011
													   WHERE c205_part_number_id = v_ref_id)
						   AND t4030.c4020_demand_master_id = p_demand_id
						   AND t4030.c4030_void_fl IS NULL
						   AND t4031.c4030_demand_growth_id = t4030.c4030_demand_growth_id
						   AND TO_CHAR (t4031.c4031_start_date, 'YY') = v_year
						   AND TO_CHAR (t4031.c4031_start_date, 'Mon') = v_month
						   AND t4031.c4031_value IS NOT NULL;

						IF v_cnt = 0
						THEN
							--
							raise_application_error (-20167, '');
						--
						END IF;
					END IF;

					-- Check For the Growth Details Existance for The Demand Sheet, Ref Type,Ref Id. If exists, update the data. Else Insert.
					v_cnt		:= 0;

					SELECT COUNT (t4031.c4031_growth_details_id)
					  INTO v_cnt
					  FROM t4031_growth_details t4031
					 WHERE t4031.c4030_demand_growth_id = v_gid
					   AND t4031.c4031_start_date = TO_DATE (v_start_date, 'Mon/DD/YY')
					   AND t4031.c4031_void_fl IS NULL;

					-- If the Growth Details is not present for the start date and c4031_demand_growth_id then insert into growth details, else update the value.
					IF v_cnt = 0 AND NVL (v_void_flag, 'N') != 'Y'
					THEN
						IF NVL (v_growthvalue, -999) >= 0
						THEN
							INSERT INTO t4031_growth_details
										(c4031_growth_details_id, c4030_demand_growth_id, c4031_value
									   , c901_ref_type, c4031_start_date, c4031_end_date
									   , c4031_last_updated_by, c4031_last_updated_date
										)
								 VALUES (s4031_growth.NEXTVAL, TO_NUMBER (v_gid), TO_NUMBER (v_growthvalue)
									   , TO_NUMBER (v_growth_ref), TO_DATE (v_start_date, 'Mon/DD/YY'), v_end_date
									   , p_userid, SYSDATE
										);
						END IF;
					ELSE
						BEGIN
							SELECT t4031.c4031_growth_details_id
							  INTO v_growth_details_id
							  FROM t4031_growth_details t4031
							 WHERE t4031.c4030_demand_growth_id = v_gid
							   AND t4031.c4031_start_date = TO_DATE (v_start_date, 'Mon/DD/YY')
							   AND t4031.c4031_void_fl IS NULL;

							UPDATE t4031_growth_details t4031
							   SET t4031.c4031_value = TO_NUMBER (v_growthvalue)
								 , t4031.c901_ref_type = v_growth_ref
								 , t4031.c4031_last_updated_by = p_userid
								 , t4031.c4031_last_updated_date = SYSDATE
								 , t4031.c4031_void_fl = v_void_flag
							 WHERE t4031.c4031_growth_details_id = v_growth_details_id;
						EXCEPTION
							WHEN NO_DATA_FOUND
							THEN
								-- Do Nothing.
								v_cnt		:= 0;
						END;
					END IF;
				END LOOP;
			END LOOP;
		END LOOP;
	END gm_pd_sav_growth_details;

--
/*******************************************************
 * Purpose: This Procedure is used to fetch the
 * growth details for the given demand growth id
 *******************************************************/
--
	PROCEDURE gm_pd_fch_growth_details (
		p_demand_master_id	 IN 	  t4030_demand_growth_mapping.c4020_demand_master_id%TYPE
	  , p_ref_type			 IN 	  t4030_demand_growth_mapping.c901_ref_type%TYPE
	  , p_ref_id			 IN 	  t4030_demand_growth_mapping.c4030_ref_id%TYPE
	  , p_year				 IN 	  VARCHAR2
	  , p_launch_date		 IN 	  VARCHAR2
	  , p_ref				 OUT	  TYPES.cursor_type
	)
	AS
		v_date		   VARCHAR2 (10);
	BEGIN
		v_date		:= p_launch_date;

		IF (p_launch_date IS NULL)
		THEN
			SELECT TO_CHAR (SYSDATE, GET_RULE_VALUE('DATEFMT','DATEFORMAT'))
			  INTO v_date
			  FROM DUAL;
		END IF;

		OPEN p_ref
		 FOR
			 SELECT   t4030.c4030_demand_growth_id growthid, TO_CHAR (t4031.c4031_start_date, 'MM') refmonth
					, t4031.c4031_value refvalue, t4031.c901_ref_type refmonthtype
					, DECODE
						  (SIGN (  TO_NUMBER (TO_CHAR (t4031.c4031_start_date, 'YYYYMM'))
								 - TO_NUMBER (TO_CHAR (TO_DATE (v_date, GET_RULE_VALUE('DATEFMT','DATEFORMAT')), 'YYYYMM'))
								)
						 , -1, 'D'
						 , 0, DECODE (NVL (gm_pkg_op_sheet.get_demand_sheet_status (p_demand_master_id
																				  , t4031.c4031_start_date
																				   )
										 , 50550
										  )
									, 50550, 'E'
									, 'D'
									 )
						 , 'E'
						  ) controlvalue
				 FROM t4030_demand_growth_mapping t4030, t4031_growth_details t4031
				WHERE t4030.c4030_demand_growth_id = t4031.c4030_demand_growth_id
				  AND t4030.c4020_demand_master_id = p_demand_master_id
				  AND t4030.c901_ref_type = p_ref_type
				  AND t4030.c4030_ref_id = p_ref_id
				  AND t4030.c4030_void_fl IS NULL
				  AND t4031.c4031_void_fl IS NULL
				  AND TO_CHAR (c4031_start_date, 'YYYY') = get_code_name (p_year)
			 ORDER BY refmonth;
	END gm_pd_fch_growth_details;
END gm_pkg_pd_growth;
/
