--@"C:\Database\Packages\ProdDevelopement\mrktcollateral\gm_pkg_qa_system_publish_trans.bdy";
CREATE OR REPLACE
PACKAGE BODY gm_pkg_qa_system_publish_trans
IS
  /*******************************************************
  * Description :This procedure used to publish/unpublish details
  * Author    : prabhu vigneshwaran M D
  *******************************************************/
PROCEDURE gm_sav_system_publish_dtls(
    p_system_id    IN t207_set_master.c207_set_id%TYPE,
    p_publish_type IN t207_set_master.c901_published_status%TYPE,
    p_user_id      IN t207_set_master.c207_last_updated_by%type,
    p_msg OUT VARCHAR2,
    p_comment      IN t902_log.c902_comments%TYPE DEFAULT NULL)
AS
  v_comment      VARCHAR2(4000);
  v_msg          VARCHAR2(4000);
  v_date_fmt     VARCHAR2(20);
  v_msg_comment  VARCHAR2(4000);
  v_user_nm      VARCHAR2(100);
  v_portal_cmd VARCHAR2(4000);
BEGIN
  --Get Company date format from context
  SELECT get_compdtfmt_frm_cntx ()
    INTO v_date_fmt
    FROM dual;
  
  --Update set system release type, updated by and updated date in t207_set_master
  UPDATE t207_set_master
     SET c901_published_status = p_publish_type,
         c207_published_by   = p_user_id,
         c207_published_date = CURRENT_DATE
   WHERE c207_set_id     = p_system_id
     AND c207_void_fl     IS NULL;
     
     v_user_nm := get_user_name(p_user_id);
  
  --get the publish message
  SELECT  '<b>' || c207_set_nm ||'</b>'
		  ||' System '
		  ||get_code_name(c901_published_status),
		  DECODE(c901_published_status,108660,'<b>Product Catalog &<br> Marketing Collateral </b><br> <br> System Name  <b>' || c207_set_nm || '</b><br> Published by <b>'||get_user_name(c207_published_by)||'</b><br> Published on <b>'||to_char(c207_published_date,v_date_fmt||' HH:MI:SS AM'),'')
		  INTO v_msg_comment, v_portal_cmd
    FROM t207_set_master
   WHERE c207_set_id = p_system_id
     AND c207_void_fl IS NULL;
     
   SELECT DECODE(p_comment,NULL,v_msg_comment,p_comment||' - '||v_msg_comment)
     INTO v_comment 
     FROM dual;
     
  --Save the log details
    gm_update_log (p_system_id, v_comment, '4000413', p_user_id, v_msg) ;
    
    p_msg := v_portal_cmd ||'^^'||v_user_nm||'^^'||TO_CHAR(CURRENT_DATE,v_date_fmt||' HH:MI:SS ');
    
  ---If system type is published(108660),the assign the p_msg value as v_comment else return empty.
  IF p_publish_type = 108660 THEN
   
    --103119 Publish in
    --103085 Product Catalog , 103086 Marketing Collateral
      gm_pkg_pd_system_trans.gm_sav_set_attribute(p_system_id,'103119^103085|103119^103086|','103119',p_user_id);
  ELSE 
  -- 108661 Unpublished the system
  	gm_pkg_pd_system_trans.gm_sav_set_attribute(p_system_id,'','103119',p_user_id);
      
  END IF;
    
END gm_sav_system_publish_dtls;
/*******************************************************
* Description :This procedure used to process system release details
* Author    : prabhu vigneshwaran M D
*******************************************************/
PROCEDURE gm_process_system_release(
    p_system_id   IN t207_set_master.c207_set_id%TYPE,
    p_type        IN t2082_set_release_mapping.c901_set_group_type%TYPE,
    p_user_id     IN t2080_set_company_mapping.c2080_last_updated_by%TYPE)
  AS
	  v_entity_id t2082_set_release_mapping.c901_entity_id%TYPE;
	  v_status_id t2082_set_release_mapping.c901_status_id%TYPE;
  --fetch entity details
  CURSOR entity_details_cur
  IS
    SELECT c901_entity_id v_entity_id,
           c901_status_id v_status_id
      FROM t2082_set_release_mapping
     WHERE c207_set_id           = p_system_id
       AND c901_set_group_type     = p_type
       AND c2082_status_updated_fl ='Y'
       AND c2082_void_fl          IS NULL;
BEGIN
  --
  FOR sys_release_dtls IN entity_details_cur
  LOOP
    gm_pkg_qa_system_publish_trans.gm_sav_system_region_release(p_system_id,sys_release_dtls.v_entity_id,sys_release_dtls.v_status_id,p_user_id);
  END LOOP;
  
END gm_process_system_release;
/****************************************************************************
*Description :This procedure used to save system release details
* Author    : prabhu vigneshwaran M D
* **************************************************************************/
PROCEDURE gm_sav_system_region_release(
    p_system_id    IN t207_set_master.c207_set_id%TYPE,
    p_entityId IN t2082_set_release_mapping.c901_entity_id%TYPE,
    p_statusId IN t2082_set_release_mapping.c901_status_id%TYPE,
    p_user_id  IN t2080_set_company_mapping.c2080_last_updated_by%TYPE )
AS
  v_compid t1902_entity_company_map.c1900_company_id%TYPE;
  v_map_type t2080_set_company_mapping.c901_set_mapping_type%TYPE;
  --fetch the entity company mapping
  CURSOR fetch_comp_id_cursor
  IS
    SELECT c1900_company_id v_compid
      FROM t1902_entity_company_map
     WHERE c901_entity_id = p_entityId
       AND c1902_void_fl   IS NULL;
 
BEGIN
	
  FOR fetch_compId IN fetch_comp_id_cursor
  LOOP
  --get maptype using company id
    BEGIN
      SELECT c901_set_mapping_type
        INTO v_map_type
        FROM t2080_set_company_mapping
       WHERE c207_set_id    = p_system_id
         AND c1900_company_id = fetch_compId.v_compid
         AND c2080_void_fl   IS NULL;
    EXCEPTION
    WHEN OTHERS THEN
      v_map_type := p_statusId;
    END;
    --call gm_sav_system_company_map procedure
      gm_pkg_pd_system_trans.gm_sav_system_company_map(p_system_id,fetch_compId.v_compid,v_map_type,p_statusId,p_user_id);
  END LOOP;
  --update c2082_status_updated_fl is null 
  UPDATE t2082_set_release_mapping
     SET c2082_status_updated_fl = NULL
   WHERE c207_set_id           = p_system_id
     AND c901_entity_id        = p_entityId
     AND c2082_void_fl IS NULL;
END gm_sav_system_region_release;

 /*******************************************************
  * Author  : T.S. Ramachandiran
  * Description: This Procedure is used to save
  * the system release details
  *******************************************************/
  
  PROCEDURE gm_sav_system_release_dtls(
  p_system_id       IN      T2082_SET_RELEASE_MAPPING.C207_set_id%TYPE,
  p_input_str       IN      VARCHAR2,
  p_user_id			IN 		T2082_SET_RELEASE_MAPPING.C2082_UPDATED_BY%TYPE,
  p_group_type      IN 		T2082_SET_RELEASE_MAPPING.C901_SET_GROUP_TYPE%TYPE
  )
  AS  	
  	v_string	   	    VARCHAR2 (3000) := p_input_str;
  	v_msg  			    VARCHAR2 (4000);   
  	v_substring		    VARCHAR2 (3000);
  	v_region_id         VARCHAR2 (20);
  	v_status_id	        VARCHAR2 (20);
  	v_count_id	        VARCHAR2 (20);
  	v_set_count         NUMBER;
    v_mapped_system_id	VARCHAR2 (20);
  	
  	
  	BEGIN  	
  	WHILE INSTR (v_string, '|') <> 0
  	LOOP
  		
  		v_substring  := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
		v_string     := SUBSTR (v_string, INSTR (v_string, '|') + 1);
		v_region_id  := SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1) ;
	 	v_substring  := SUBSTR (v_substring, INSTR (v_substring, ',') + 1) ;
       	v_status_id  := v_substring;
       
	--update system release and unrelease details	
		UPDATE T2082_SET_RELEASE_MAPPING
		   SET C2082_UPDATED_BY          = p_user_id,
		       C2082_UPDATED_DATE        = current_date,
		       C2082_STATUS_UPDATED_FL   = 'Y',
		       C901_STATUS_ID            = v_status_id
		WHERE  C207_SET_ID      = p_system_id
		AND	   C901_ENTITY_ID	= v_region_id
		AND    C2082_VOID_FL IS NULL;

	 IF (SQL%ROWCOUNT=0)
		THEN
		INSERT INTO T2082_SET_RELEASE_MAPPING 
		( 
		C207_SET_ID, 
		C901_SET_GROUP_TYPE,
		C901_STATUS_ID,
		C901_ENTITY_ID, 
		C2082_UPDATED_BY,
		C2082_UPDATED_DATE,
		C2082_VOID_FL, 
		C2082_STATUS_UPDATED_FL,
		C2082_STATUS_HISTORY_FL)
	    VALUES
	    (
	    p_system_id,
	    p_group_type,
	    v_status_id,
	    v_region_id,
	    p_user_id,
	    current_date,
	    null,
	    'Y',
	    null);
     END IF;
     
     
       --get mapped system id for given set id
     BEGIN
     	SELECT C207_SET_SALES_SYSTEM_ID INTO v_mapped_system_id
     	FROM T207_SET_MASTER
     	WHERE C207_SET_ID = p_system_id;
     	EXCEPTION
		WHEN NO_DATA_FOUND THEN
		v_mapped_system_id := NULL;
     END;
     	
      --check released count for region of the system 	 
     IF v_mapped_system_id IS NOT NULL THEN
     
    		v_set_count := gm_pkg_qa_system_publish_rpt.get_system_set_map_by_region(v_mapped_system_id,v_region_id,'1600'); -- 1600 is system type
    	
    	IF v_set_count = 0 THEN

    		UPDATE T2082_SET_RELEASE_MAPPING
			SET C2082_UPDATED_BY          = p_user_id,
			    C2082_UPDATED_DATE        = current_date,
			    C901_STATUS_ID            = 10304547 -- Unreleased
			WHERE  C207_SET_ID      = v_mapped_system_id
			AND	   C901_ENTITY_ID	= v_region_id
			AND    C2082_VOID_FL IS NULL;
    
    	 END IF;  
    	 
      END IF;	
    
     
     
     
-- For each insert or update log will be stored in t941_audit_trail_log table
	END LOOP;	
	
   END gm_sav_system_release_dtls;
   
   -- PC-5611 Refresh the Set List Redis key while release to region
   /******************************************************************
    * Description : Procedure to fetch company id by using region id.
    ****************************************************************/
	PROCEDURE gm_get_company_id_using_region_id (
			p_input_str	 IN	VARCHAR2,
	        p_companyid OUT TYPES.cursor_type)
	AS
		v_string	   	    VARCHAR2 (3000) := p_input_str;
		v_substring		    VARCHAR2 (3000);
	  	v_region_id         VARCHAR2 (100);
	  	v_region_list 		VARCHAR2 (100) := '';
	BEGIN
		WHILE INSTR (v_string, '|') <> 0
	  	LOOP
	  		v_substring  := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
			v_string     := SUBSTR (v_string, INSTR (v_string, '|') + 1);
			v_region_id  := SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1) ;
			v_region_list := v_region_list ||v_region_id   ||',';
		END LOOP;
			v_region_list := SUBSTR(v_region_list , 1, INSTR(v_region_list , ',', -1)-1);
	    	my_context.set_my_inlist_ctx (v_region_list);
	    OPEN p_companyid FOR 
	    	SELECT
			    C1900_COMPANY_ID
			FROM
			    t1902_entity_company_map
			WHERE
			    c901_entity_id IN ( SELECT * FROM v_in_list )
	    		AND c1902_void_fl IS NULL;
	END gm_get_company_id_using_region_id;
 
END gm_pkg_qa_system_publish_trans;
/