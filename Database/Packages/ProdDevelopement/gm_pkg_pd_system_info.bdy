/* Formatted on 2009/02/16 12:30 (Formatter Plus v4.8.0) */
--@"C:\Database\Packages\ProdDevelopement\gm_pkg_pd_system_info.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_pd_system_info
IS
	/*******************************************************
	 * Description : Procedure to fetch systems info
	*******************************************************/
	PROCEDURE gm_fch_systems_prodcat (
		p_token_id     IN  t101a_user_token.c101a_token_id%TYPE
	  , p_language_id  IN  NUMBER
	  , p_page_no      IN  NUMBER
	  , p_uuid		   IN  t9150_device_info.c9150_device_uuid%TYPE
	  ,	p_systemlist_cur     OUT TYPES.cursor_type
	)
	AS
	
	v_device_infoid		   t9150_device_info.c9150_device_info_id%TYPE;
	v_start     NUMBER;
    v_end       NUMBER;
    v_page_size NUMBER;
	v_page_no   NUMBER;
	v_update_count NUMBER := 0;
    v_last_rec	t9151_device_sync_dtl.C9151_LAST_SYNC_REC%TYPE; 
	BEGIN
	   --get the device info id based on user token
	   v_device_infoid := get_device_id(p_token_id, p_uuid) ;
	    
	   v_page_no := p_page_no;
	   -- get the SYSTEM paging details defined in rules. 
	   gm_pkg_pdpc_prodcatrpt.gm_fch_paging_dtl('SYSTEM','PAGESIZE',v_page_no,v_start,v_end,v_page_size);
	 
	   /*below prc check the device's last updated date for SYSTEM reftype and
		if record exist IN T9151_DEVICE_SYNC_DTL, then it checks is there any SYSTEMS updated after the device updated date IN T2001_MASTER_DATA_UPDATES.
		if updated SYSTEMS are available then add all SYSTEMS into v_inlist and this prc returns count of v_inlist records*/
	   gm_pkg_pdpc_prodcatrpt.gm_fch_device_updates(v_device_infoid,p_language_id,103108,p_page_no,v_update_count,v_last_rec);
	   
	   --If there is no update data available for device then call below prc to add all SYSTEMS.		
	   IF v_update_count = 0
	   THEN
			gm_pkg_pdpc_prodcatrpt.gm_fch_all_system_tosync;		
	   END IF;   
   
   
	   OPEN p_systemlist_cur
	   FOR	  
		 SELECT RESULT_COUNT totalsize,  COUNT (1) OVER () pagesize, v_page_no pageno, rwnum, CEIL (RESULT_COUNT / v_page_size) totalpages, T207.*  
		   FROM
		    (
		         SELECT T207.*, ROWNUM rwnum, COUNT (1) OVER () result_count,MAX(DECODE(T207.systemid,v_last_rec,ROWNUM,NULL)) OVER() LASTREC
		           FROM
		            (                
		             SELECT t207.c207_set_id systemid
		             /*, NVL(T2000.c2000_ref_name, t207.c207_set_nm) systemnm,  NVL(T2000.c2000_ref_desc,t207.c207_set_desc) systemdesc
		             , NVL(t2000.c2000_ref_dtl_desc,t207.c207_set_dtl_desc ) systemdtldesc */
		             , t207.c207_set_nm systemnm, t207.c207_set_desc systemdesc
		             , t207.c207_set_dtl_desc systemdtldesc
		              , t207.c901_status_id systemstatusid, NVL(temp.void_fl,t207.c207_void_fl) voidfl,
		              t207.C901_SET_GRP_TYPE setgrptyp
		               FROM t207_set_master T207, 
			             /*  ( SELECT c2000_ref_id, c2000_ref_name, c2000_ref_desc, c2000_ref_dtl_desc
					           FROM t2000_master_metadata
					          WHERE c901_ref_type    = 103092       --System type
					            AND c901_language_id = p_language_id
					            AND c2000_void_fl IS NULL
	                       ) T2000,*/
	                       my_temp_prod_cat_info temp   
		              WHERE 
		              /*T207.c207_set_id = T2000.c2000_ref_id (+)
		                AND*/
		                c207_set_id = temp.ref_id
		                AND c901_set_grp_type   = 1600   --Sales Group Type
		               ORDER BY t207.c207_set_id
		            )
		            T207
		    )
		    T207
		  WHERE rwnum  BETWEEN NVL(LASTREC+1,DECODE(v_last_rec,NULL,v_start,v_start-1)) AND v_end;

  
	END gm_fch_systems_prodcat;
	
	/*******************************************************
	 * Description : Procedure to fetch set info's
	*******************************************************/
	PROCEDURE gm_fch_sets_prodcat (
		p_token_id     IN  t101a_user_token.c101a_token_id%TYPE
	  , p_language_id  IN  NUMBER
	  , p_page_no      IN  NUMBER
	  , p_uuid		   IN  t9150_device_info.c9150_device_uuid%TYPE
	  ,	p_setslist_cur OUT TYPES.cursor_type
	)AS
	
	v_device_infoid		   t9150_device_info.c9150_device_info_id%TYPE;
	v_start     NUMBER;
    v_end       NUMBER;
    v_page_size NUMBER;
	v_page_no   NUMBER;
	v_update_count NUMBER := 0;
    v_last_rec	t9151_device_sync_dtl.C9151_LAST_SYNC_REC%TYPE; 	
	BEGIN
	   --get the device info id based on user token
	   v_device_infoid := get_device_id(p_token_id, p_uuid) ;
	    
	   v_page_no := p_page_no;
	   -- get the SET_MASTER paging details defined in rules. 
	   gm_pkg_pdpc_prodcatrpt.gm_fch_paging_dtl('SET_MASTER','PAGESIZE',v_page_no,v_start,v_end,v_page_size);
	 
	   /*below prc check the device's last updated date for SETS reftype and
		if record exist IN T9151_DEVICE_SYNC_DTL, then it checks is there any SETS updated after the device updated date IN T2001_MASTER_DATA_UPDATES.
		if updated SETS are available then add all SETS into v_inlist and this prc returns count of v_inlist records*/
	   gm_pkg_pdpc_prodcatrpt.gm_fch_device_updates(v_device_infoid,p_language_id,103111,v_page_no,v_update_count,v_last_rec);
	   
	   --If there is no update data available for device then call below prc to add all SETS.		
	   IF v_update_count = 0
	   THEN
			gm_pkg_pdpc_prodcatrpt.gm_fch_all_setmaster_tosync;		
	   END IF;   
   
   
	   OPEN p_setslist_cur
	   FOR	  
		 SELECT RESULT_COUNT totalsize,  COUNT (1) OVER () pagesize, v_page_no pageno, rwnum, CEIL (RESULT_COUNT / v_page_size) totalpages, T207.*  
		   FROM
		    (
		         SELECT T207.*, ROWNUM rwnum, COUNT (1) OVER () result_count,MAX(DECODE(T207.setid,v_last_rec,ROWNUM,NULL)) OVER() LASTREC
		           FROM
		            (                
		             SELECT t207.c207_set_id setid, 
		             		/*NVL (T2000.c2000_ref_name, t207.c207_set_nm) setnm, NVL (T2000.c2000_ref_desc,
						    t207.c207_set_desc) setdesc, NVL (t2000.c2000_ref_dtl_desc, t207.c207_set_dtl_desc) setdtldesc,*/
		             		t207.c207_set_nm setnm, t207.c207_set_desc setdesc, t207.c207_set_dtl_desc setdtldesc,		             
						    t207.c207_set_sales_system_id systemid, t207.c207_category setcategoryid, t207.c207_type settypeid, 
						    t207.c901_hierarchy sethierarchyid, t207.c901_status_id setstatusid, NVL(temp.void_fl,t207.c207_void_fl) voidfl
		              FROM  t207_set_master T207, 
			              /* ( SELECT c2000_ref_id, c2000_ref_name, c2000_ref_desc, c2000_ref_dtl_desc
					           FROM t2000_master_metadata
					          WHERE c901_ref_type    = 103091        --Set type
				                AND c901_language_id = p_language_id
				                AND c2000_void_fl IS NULL
                           ) T2000, */
                           my_temp_prod_cat_info temp 
		              WHERE 
		              /*T207.c207_set_id = T2000.c2000_ref_id (+)
		                AND */
		                c207_set_id = temp.ref_id
		                AND c901_set_grp_type   = 1601   --Set Type
		                 ORDER BY t207.c207_set_id
		            )
		            T207
		    )
		    T207
		  WHERE rwnum BETWEEN NVL(LASTREC+1,DECODE(v_last_rec,NULL,v_start,v_start-1)) AND v_end;
	
	  
	END gm_fch_sets_prodcat;
	
	/*******************************************************
	 * Description : Procedure to fetch set Parts info's
	*******************************************************/
	PROCEDURE gm_fch_setpartdtls_prodcat (
		p_token_id     IN  t101a_user_token.c101a_token_id%TYPE
	  , p_language_id  IN  NUMBER
	  , p_page_no      IN  NUMBER
	  , p_uuid		   IN  t9150_device_info.c9150_device_uuid%TYPE
	  ,	p_setpartlist_cur OUT TYPES.cursor_type
	)AS
	
	v_device_infoid		   t9150_device_info.c9150_device_info_id%TYPE;
	v_start     NUMBER;
    v_end       NUMBER;
    v_page_size NUMBER;
	v_page_no   NUMBER;
	v_update_count NUMBER := 0;
	v_last_rec	t9151_device_sync_dtl.C9151_LAST_SYNC_REC%TYPE; 	
	BEGIN
	   --get the device info id based on user token
	   v_device_infoid := get_device_id(p_token_id, p_uuid) ;
	    
	   v_page_no := p_page_no;
	   -- get the SET_MASTER paging details defined in rules. 
	   gm_pkg_pdpc_prodcatrpt.gm_fch_paging_dtl('SET_DETAILS','PAGESIZE',v_page_no,v_start,v_end,v_page_size);
	 
	   /*below prc check the device's last updated date for SETS reftype and
		if record exist IN T9151_DEVICE_SYNC_DTL, then it checks is there any SET parts updated after the device updated date IN T2001_MASTER_DATA_UPDATES.
		if updated SET PARTS are available then add all SET PARTS into v_inlist and this prc returns count of v_inlist records*/
	   gm_pkg_pdpc_prodcatrpt.gm_fch_device_updates(v_device_infoid,p_language_id,4000409,v_page_no,v_update_count,v_last_rec);
	   
	   --If there is no update data available for device then call below prc to add all SET PARTS.		
	   IF v_update_count = 0
	   THEN
			gm_pkg_pdpc_prodcatrpt.gm_fch_all_setmaster_tosync;	
	   END IF;   
   
   
	   OPEN p_setpartlist_cur
	   FOR	  
		 SELECT RESULT_COUNT totalsize,  COUNT (1) OVER () pagesize, v_page_no pageno, rwnum, CEIL (RESULT_COUNT / v_page_size) totalpages, T208.*  
		   FROM
		    (
		         SELECT T208.*, ROWNUM rwnum, COUNT (1) OVER () result_count,MAX(DECODE(T208.setdtlid,v_last_rec,ROWNUM,NULL)) OVER() LASTREC
		           FROM
		            (                
		              SELECT t207.c207_set_id setid, t208.c205_part_number_id partnum,t208.c208_set_qty setqty,  t208.c208_inset_fl insetfl, NVL(temp.void_fl,t208.c208_void_fl) voidfl
		              		 , t208.c208_set_details_id setdtlid
					    FROM t207_set_master t207, t208_set_details t208,  
						     my_temp_prod_cat_info temp
					   WHERE T207.c207_set_id       = t208.c207_set_id
					
					     AND T207.c207_set_id  =  temp.ref_id 
					     AND T207.c901_set_grp_type = 1601
					     AND ( v_update_count>0 OR t208.c208_void_fl IS NULL)
					     ORDER BY t208.c208_set_details_id
		            )
		            T208
		    )
		    T208
		  WHERE rwnum BETWEEN NVL(LASTREC+1,DECODE(v_last_rec,NULL,v_start,v_start-1)) AND v_end;
	
	  
	END gm_fch_setpartdtls_prodcat;
	
	/***********************************************************
	 * Description : Procedure to fetch systems attributes info
	************************************************************/
	PROCEDURE gm_fch_systemattrib_prodcat (
		p_token_id     IN  t101a_user_token.c101a_token_id%TYPE
	  , p_language_id  IN  NUMBER
	  , p_page_no      IN  NUMBER
	  , p_uuid		   IN  t9150_device_info.c9150_device_uuid%TYPE
	  ,	p_system_attrib_cur OUT TYPES.cursor_type
	)
	AS
	
	v_device_infoid		   t9150_device_info.c9150_device_info_id%TYPE;
	v_start     NUMBER;
    v_end       NUMBER;
    v_page_size NUMBER;
	v_page_no   NUMBER;
	v_update_count NUMBER := 0;
	v_last_rec	t9151_device_sync_dtl.C9151_LAST_SYNC_REC%TYPE; 	    
	BEGIN
	   --get the device info id based on user token
	   v_device_infoid := get_device_id(p_token_id, p_uuid) ;
		
	   v_page_no := p_page_no;
	   -- get the SYSTEM paging details defined in rules. 
	   gm_pkg_pdpc_prodcatrpt.gm_fch_paging_dtl('SYSTEM_ATTRIB','PAGESIZE',v_page_no,v_start,v_end,v_page_size);
	 
      /*below prc check the device's last updated date for SYSTEM ATTRIB's reftype and
		if record exist IN T9151_DEVICE_SYNC_DTL, then it checks is there any SYSTEM ATTRIB's updated after the device updated date IN T2001_MASTER_DATA_UPDATES.
		if updated SYSTEM ATTRIB's are available then add all SYSTEM ATTRIB's into v_inlist and this prc returns count of v_inlist records*/
	   gm_pkg_pdpc_prodcatrpt.gm_fch_device_updates(v_device_infoid,p_language_id,4000416,v_page_no,v_update_count,v_last_rec);
	   
	   --If there is no update data available for device then call below prc to add all SYSTEMS.		
	   IF v_update_count = 0
	   THEN
			gm_pkg_pdpc_prodcatrpt.gm_fch_all_systemattrib_tosync;		
	   END IF;   
   	
   
	   OPEN p_system_attrib_cur
	   FOR	  
		 SELECT RESULT_COUNT totalsize,  COUNT (1) OVER () pagesize, v_page_no pageno, rwnum, CEIL (RESULT_COUNT / v_page_size) totalpages, T207.*  
		   FROM
		    (
		         SELECT T207.*, ROWNUM rwnum, COUNT (1) OVER () result_count,MAX(DECODE(T207.setattrid,v_last_rec,ROWNUM,NULL)) OVER() LASTREC
		           FROM
		            (                
		             SELECT t207.c207_set_id systemid, t207c.c901_attribute_type  attribtypeid,t207c.c207c_attribute_value  attribvalue, NVL(temp.void_fl,t207c.c207c_void_fl) voidfl
		                    , t207c.c207c_set_attribute_id setattrid
		               FROM t207_set_master T207, t207c_set_attribute t207c, my_temp_prod_cat_info temp 	                
		              WHERE T207.c207_set_id = t207c.c207_set_id
		                AND T207.c207_set_id = temp.ref_id 
		                AND T207.c901_set_grp_type   = 1600    --Sales Group Type
		                AND ( v_update_count>0 OR t207c.c207c_void_fl IS NULL)
		                ORDER BY t207c.c207c_set_attribute_id
		             )
		            T207
		    )
		    T207
		  WHERE rwnum BETWEEN NVL(LASTREC+1,DECODE(v_last_rec,NULL,v_start,v_start-1)) AND v_end;

  
	END gm_fch_systemattrib_prodcat;
	/***********************************************************
	 * Description : Procedure to fetch SET attributes info
	************************************************************/
	PROCEDURE gm_fch_setattr_tosync (
		p_token_id     IN  t101a_user_token.c101a_token_id%TYPE
	  , p_language_id  IN  NUMBER
	  , p_page_no      IN  NUMBER
	  , p_uuid		   IN  t9150_device_info.c9150_device_uuid%TYPE
	  ,	p_set_attrib_cur OUT TYPES.cursor_type
	)
	AS
	
	v_device_infoid		   t9150_device_info.c9150_device_info_id%TYPE;
	v_start     NUMBER;
    v_end       NUMBER;
    v_page_size NUMBER;
	v_page_no   NUMBER;
	v_update_count NUMBER := 0;
    v_last_rec	t9151_device_sync_dtl.C9151_LAST_SYNC_REC%TYPE; 
	BEGIN
	   --get the device info id based on user token
	   v_device_infoid := get_device_id(p_token_id, p_uuid) ;
		
	   v_page_no := p_page_no;
	   -- get the SET paging details defined in rules. 
	   gm_pkg_pdpc_prodcatrpt.gm_fch_paging_dtl('SET_ATTRIB','PAGESIZE',v_page_no,v_start,v_end,v_page_size);
	 
      /*below prc check the device's last updated date for SYSTEM ATTRIB's reftype and
		if record exist IN T9151_DEVICE_SYNC_DTL, then it checks is there any SET ATTRIB's updated after the device updated date IN T2001_MASTER_DATA_UPDATES.
		if updated SET ATTRIB's are available then add all SET ATTRIB's into v_inlist and this prc returns count of v_inlist records*/
	   gm_pkg_pdpc_prodcatrpt.gm_fch_device_updates(v_device_infoid,p_language_id,4000736,v_page_no,v_update_count,v_last_rec);--4000736 Reference Type
	   
	   --If there is no update data available for device then call below prc to add all SYSTEMS.		
	   IF v_update_count = 0
	   THEN
			gm_pkg_pdpc_prodcatrpt.gm_fch_all_setattrib_tosync;--This is used to fetch the set id which are available in loaner pool.		
	   END IF;   
   	
   
	   OPEN p_set_attrib_cur
	   FOR	  
		 SELECT RESULT_COUNT totalsize,  COUNT (1) OVER () pagesize, v_page_no pageno, rwnum, CEIL (RESULT_COUNT / v_page_size) totalpages, T207.*  
		   FROM
		    (
		         SELECT T207.*, ROWNUM rwnum, COUNT (1) OVER () result_count,MAX(DECODE(T207.setattrid,v_last_rec,ROWNUM,NULL)) OVER() LASTREC
		           FROM
		            (                
			             SELECT t207.c207_set_id setid, t207.c207_set_nm setnm, t207c.c901_attribute_type attrtype
			             		, t207c.c207c_attribute_value attrvalue , NVL(temp.void_fl,t207c.c207c_void_fl) voidfl 
			             		, t207c.c207c_set_attribute_id setattrid
			              FROM t207_set_master T207, t207c_set_attribute t207c,
				               my_temp_prod_cat_info temp 
			              WHERE T207.c207_set_id = t207c.c207_set_id
			                AND T207.c207_set_id = temp.ref_id
			                AND T207.c901_set_grp_type   = 1601   --Set Type
			                AND ( v_update_count>0 OR t207c.c207c_void_fl IS NULL)
			                 ORDER BY t207c.c207c_set_attribute_id
		            )
		            T207
		    )
		    T207
		  WHERE rwnum BETWEEN NVL(LASTREC+1,DECODE(v_last_rec,NULL,v_start,v_start-1)) AND v_end;


  
	END gm_fch_setattr_tosync;
	
/******************************************************************
Description : This procedure is used to fetch Tag Header Details
for the set which is requested from device.
****************************************************************/
PROCEDURE gm_fch_tag_info(
    p_tagid IN t5010_tag.c5010_tag_id%TYPE,
    p_outmaster OUT TYPES.cursor_type )
	AS
	
	v_consignment_id t504_consignment.c504_consignment_id%TYPE;
	v_cnt number;
	v_tagid t5010_tag.c5010_tag_id%TYPE;
	BEGIN
		-- Check if the Tag has multiple records for the passed in tag ID, then throw an error
		SELECT LPAD( p_tagid, 7, '0' ) INTO v_tagid FROM DUAL;
		
        BEGIN        
			SELECT C504_CONSIGNMENT_ID 
			  INTO v_consignment_id
         	  FROM T504_CONSIGNMENT 
         	 WHERE C504_CONSIGNMENT_ID = (
        		SELECT 	c5010_last_updated_trans_id 
        		  FROM 	t5010_tag 
        		 WHERE 	c5010_tag_id = v_tagid 
        		   AND 	C5010_VOID_FL IS NULL )        	
         	   AND C504_VOID_FL IS NULL;
        
        EXCEPTION WHEN NO_DATA_FOUND
        THEN
        	BEGIN
	        -- Check if the tag is having transfer as the txn id and get the appropriate Cn .
        	 	SELECT C921_REF_ID 
        	 	  INTO v_consignment_id
             	  FROM T921_TRANSFER_DETAIL T921, T920_TRANSFER T920 
             	 WHERE C901_LINK_TYPE='90360' -- Consignment
             	   AND T920.C920_TRANSFER_ID = T921.C920_TRANSFER_ID
             	   AND T920.C920_VOID_FL IS NULL
             	   AND C921_VOID_FL IS NULL
             	   AND C920_VOID_FL IS NULL
             	   AND T920.C920_TRANSFER_ID = (SELECT c5010_last_updated_trans_id
             									  FROM t5010_tag WHERE c5010_tag_id = v_tagid
             									   AND C5010_VOID_FL IS NULL
             									);             
           EXCEPTION WHEN OTHERS
           THEN
           		raise_application_error (-20666,'');
           END;        
        END;
        
	  	OPEN p_outmaster 
	  	FOR 
	  		SELECT 	t504.c504_consignment_id consignid ,t504.c207_set_id setid,
	  		   		t504.c504_type settype,get_code_name(t504.c504_type) settypenm,
	  		   		t207.c207_set_nm setnm
	  		  FROM 	t504_consignment t504 ,T207_SET_MASTER t207
	  		 WHERE t504.c504_consignment_id = v_consignment_id
	  		   AND T504.C207_SET_ID = T207.C207_SET_ID(+)
	  		   AND t504.c504_void_fl IS NULL
	  		   AND t207.c207_void_fl is NULL;        
	END gm_fch_tag_info;
/******************************************************************
Description : This procedure is used to fetch Part Details Associated to
the respective Tag ID.In Device when the Tag ID is Scanned or Loaded,It will return the 
Part Details Associated to the Respective Consignment ID.
****************************************************************/
PROCEDURE gm_fch_tag_part_dtl(
    p_consign_id IN t5010_tag.c5010_last_updated_trans_id%TYPE,
    p_type       IN t504_consignment.c504_type%TYPE,
    p_countryid  IN t205d_part_attribute.c205d_attribute_value%TYPE,
    p_outmaster OUT TYPES.cursor_type )
	AS
	BEGIN
		  IF p_type = '4127' THEN --P_type is Loaner
		    OPEN p_outmaster FOR 
		    SELECT * FROM
		    	(
		    	 SELECT T205.C205_PART_NUMBER_ID PNUM,
		         T205.C205_PART_NUM_DESC PDESC,
		         NVL(CONS.QTY, 0) - NVL (MISS.QTY, 0) QTY
		    		FROM t205_part_number t205,
		                 t205d_part_attribute t205d,
		      			(
		      			SELECT t505.c205_part_number_id,
		                       SUM (t505.c505_item_qty) qty
		      				FROM T505_ITEM_CONSIGNMENT T505,T504_Consignment t504,T504A_CONSIGNMENT_LOANER t504a
		      				WHERE t505.c504_consignment_id = p_consign_id
		      				AND t505.c504_consignment_id = t504.c504_consignment_id
		      				AND T505.C504_CONSIGNMENT_ID   =t504a.C504_CONSIGNMENT_ID(+)
		     				AND T505.C505_VOID_FL         IS NULL
		     				AND T504.C504_VOID_FL         IS NULL
		     				AND T504A.C504A_VOID_FL       IS NULL
		      				GROUP BY T505.C205_PART_NUMBER_ID
		      			) CONS,
		      			(
		      			SELECT c205_part_number_id,
		        			   SUM (qty) qty
		      				FROM
		        			(
		        			SELECT t413.c205_part_number_id,
		          				   DECODE (c412_type, 50151, 0, DECODE (t413.c413_status_fl, 'Q', NVL (t413.c413_item_qty, 0 ), 0 ) ) qty
		        				FROM t412_inhouse_transactions t412,
		         					 t413_inhouse_trans_items t413
						        WHERE t412.c412_inhouse_trans_id = T413.C412_INHOUSE_TRANS_ID
						        AND t412.c412_ref_id             = p_consign_id
						        AND t412.c412_void_fl           IS NULL
						        AND T413.C413_VOID_FL           IS NULL
		        			)
		      			GROUP BY C205_PART_NUMBER_ID
		      			)MISS
			    WHERE T205.C205_PART_NUMBER_ID  = CONS.C205_PART_NUMBER_ID
			    AND T205.C205_PART_NUMBER_ID    = MISS.C205_PART_NUMBER_ID(+)
			    AND t205.C205_PART_NUMBER_ID    =T205d.C205_PART_NUMBER_ID
			    AND t205d.c205d_attribute_value =p_countryid
			    AND t205d.c205d_void_fl        IS NULL
			    ORDER BY 1
			    ) WHERE qty != 0 ;
		  ELSE
		  
		    OPEN p_outmaster FOR 
		    	SELECT * FROM
		    		(
		    			SELECT T205.C205_PART_NUMBER_ID PNUM,
		      				  T205.C205_PART_NUM_DESC PDESC,
		      				  NVL(CONS.QTY, 0) - NVL (MISS.QTY, 0) QTY
		    			FROM t205_part_number t205,
		      				 t205d_part_attribute t205d,
		      			(
		      				SELECT t505.c205_part_number_id,
		        				   SUM (T505.C505_ITEM_QTY) QTY
		      				FROM T505_ITEM_CONSIGNMENT T505 ,
		        				 T504_Consignment t504
						      WHERE T505.C504_CONSIGNMENT_ID = p_consign_id
						      AND T505.C504_CONSIGNMENT_ID   =t504.C504_CONSIGNMENT_ID
						      AND T505.C505_VOID_FL         IS NULL
						      AND T504.C504_VOID_FL         IS NULL
						      GROUP BY T505.C205_PART_NUMBER_ID
		      			) CONS,
		      			(
		      				SELECT c205_part_number_id,
		        				   SUM (qty) qty
		      				FROM
				        	(
				        	SELECT t413.c205_part_number_id,
					          DECODE (c412_type, 50151, 0, DECODE (t413.c413_status_fl, 'Q', NVL (t413.c413_item_qty, 0 ), 0 ) ) qty
					        FROM t412_inhouse_transactions t412,
					          t413_inhouse_trans_items t413
					        WHERE t412.c412_inhouse_trans_id = T413.C412_INHOUSE_TRANS_ID
					        AND t412.c412_ref_id             = p_consign_id
					        AND t412.c412_void_fl           IS NULL
					        AND T413.C413_VOID_FL           IS NULL
				       		)
		      			GROUP BY C205_PART_NUMBER_ID
		      			)MISS
				    WHERE T205.C205_PART_NUMBER_ID  = CONS.C205_PART_NUMBER_ID
				    AND T205.C205_PART_NUMBER_ID    = MISS.C205_PART_NUMBER_ID(+)
				    AND t205.C205_PART_NUMBER_ID    =T205d.C205_PART_NUMBER_ID
				    AND t205d.c205d_attribute_value =p_countryid
				    AND t205d.c205d_void_fl        IS NULL
				    ORDER BY 1
		    ) WHERE qty != 0 ;
		  END IF;
	END gm_fch_tag_part_dtl;
END gm_pkg_pd_system_info;
/
