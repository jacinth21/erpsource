/* Formatted on 2009/03/03 19:03 (Formatter Plus v4.8.0) */
-- @"C:\Database\Packages\ProdDevelopement\gm_pkg_pd_staffing_info.bdy"

CREATE OR REPLACE PACKAGE BODY gm_pkg_pd_staffing_info
IS
	/*********************************************************
	* Function to fetch the projected count for a role
	*********************************************************/
	FUNCTION get_projected_team_count (
		p_projectid   IN   t2004_project_team_count.c202_project_id%TYPE
	  , p_roleid	  IN   t2004_project_team_count.c901_role%TYPE
	)
		RETURN VARCHAR2
	IS
		v_count 	   VARCHAR2 (10);
	BEGIN
		SELECT c2004_role_count
		  INTO v_count
		  FROM t2004_project_team_count
		 WHERE c901_role = p_roleid AND c202_project_id = p_projectid;

		IF (v_count IS NULL)
		THEN
			v_count 	:= '0';
		END IF;

		RETURN v_count;
	END get_projected_team_count;

	/*********************************************************
	* Function to fetch the actual count for a role
	*********************************************************/
	FUNCTION get_actual_team_count (
		p_projectid   IN   t2004_project_team_count.c202_project_id%TYPE
	  , p_roleid	  IN   t2004_project_team_count.c901_role%TYPE
	)
		RETURN VARCHAR2
	IS
		v_count 	   NUMBER;
	BEGIN
		SELECT COUNT (c101_party_id)
		  INTO v_count
		  FROM t204_project_team
		 WHERE c901_role_id = p_roleid AND c901_status_id = 92053 AND c202_project_id = p_projectid;

		RETURN v_count;
	END get_actual_team_count;

		/*******************************************************
	* Description : Procedure to fetch staffing requirements
	* Author		: Ritesh Shah
	*******************************************************/
	PROCEDURE gm_fch_staffingcount (
		p_projectid 	   IN		t202_project.c202_project_id%TYPE
	  , p_outstaffingreq   OUT		TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_outstaffingreq
		 FOR
			 SELECT   ROWNUM NO, t2004.c2004_project_team_count_id ID, t2004.c901_role ROLE
					, get_code_name (t2004.c901_role) rolenm, t2004.c2004_role_count memberinfo
					, get_log_flag (t2004.c2004_project_team_count_id, 1244) LOG
				 FROM t2004_project_team_count t2004
				WHERE t2004.c2004_void_fl IS NULL AND t2004.c202_project_id = p_projectid
			 ORDER BY ROLE;
	END gm_fch_staffingcount;

		/*******************************************************
	* Description : Procedure to fetch staffing details
	* Author		: Ritesh Shah
	*******************************************************/
	PROCEDURE gm_fch_staffingdetails (
		p_projectid 	IN		 t204_project_team.c202_project_id%TYPE
	  , p_partytype 	IN		 t101_party.c901_party_type%TYPE
	  , p_outstaffdtl	OUT 	 TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_outstaffdtl
		 FOR
			 SELECT t204.c204_project_team_id ID, t204.c101_party_id partyid
				  , get_party_name (t204.c101_party_id) partynm, t204.c901_role_id ROLE
				  , get_code_name (t204.c901_role_id) rolenm, t204.c901_status_id status
				  , get_code_name (t204.c901_status_id) statusnm, get_log_flag (t204.c204_project_team_id, 1244) LOG
			   FROM t204_project_team t204, t101_party t101
			  WHERE t204.c202_project_id = p_projectid
				AND t204.c101_party_id = t101.c101_party_id
				AND t101.c901_party_type = p_partytype
				AND t101.c101_void_fl IS NULL
				AND t204.c204_void_fl IS NULL;
	END gm_fch_staffingdetails;

	/*******************************************************
	* Description : Procedure to fetch staffing details for editing
	* Author		: Ritesh Shah
	*******************************************************/
	PROCEDURE gm_fch_edit_staffingdetails (
		p_projectteamid   IN	   t204_project_team.c204_project_team_id%TYPE
	  , p_outstaffdtl	  OUT	   TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_outstaffdtl
		 FOR
			 SELECT t204.c204_project_team_id ID, t204.c202_project_id projectid, t204.c101_party_id partyid
				  , t204.c901_role_id ROLE, t204.c901_status_id status
			   FROM t204_project_team t204
			  WHERE t204.c204_project_team_id = p_projectteamid AND t204.c204_void_fl IS NULL;
	END gm_fch_edit_staffingdetails;

	/*******************************************************
	* Description : Procedure to fetch the staffing report
	* Author		: Satyajit Thadeshwar
	*******************************************************/
	PROCEDURE gm_fch_staffingreport (
		p_projectid   IN	   t204_project_team.c202_project_id%TYPE
	  , p_role		  IN	   t204_project_team.c901_role_id%TYPE
	  , p_partytype   IN	   t101_party.c901_party_type%TYPE
	  , p_status	  IN	   t204_project_team.c901_status_id%TYPE
	  , p_lastname	  IN	   t101_party.c101_last_nm%TYPE
	  , p_firstname	  IN	   t101_party.c101_first_nm%TYPE	  
	  , p_outreport   OUT	   TYPES.cursor_type
	)
	AS
		v_company_id NUMBER;
	BEGIN
	
	SELECT get_compid_frm_cntx() 
	  INTO v_company_id  
	  FROM DUAL;
	    
		OPEN p_outreport		
		 FOR
			 SELECT t204.c101_party_id ID, t101.c901_party_type partytype, get_party_name (t204.c101_party_id) NAME
				  , t202.c202_project_id projectid, (t202.c202_project_id || ' - ' || t202.c202_project_nm) project
				  , get_code_name (t204.c901_role_id) ROLE, get_code_name (t204.c901_status_id) status
			   FROM t202_project t202, t204_project_team t204, t101_party t101,t2021_project_company_mapping t2021
			  WHERE t202.c202_project_id = t204.c202_project_id
				AND t204.c202_project_id = DECODE (p_projectid, '0', t204.c202_project_id, p_projectid)
				AND t101.c901_party_type = DECODE (p_partytype, 0, t101.c901_party_type, p_partytype)
				AND t204.c101_party_id = t101.c101_party_id
				AND t204.c901_role_id = DECODE (p_role, 0, t204.c901_role_id, p_role)
				AND t204.c901_status_id = DECODE (p_status, 0, t204.c901_status_id, p_status)
				AND t101.c101_last_nm LIKE DECODE (p_lastname, '', t101.c101_last_nm, p_lastname || '%')
				AND t101.c101_first_nm LIKE DECODE (p_firstname, '', t101.c101_first_nm, p_firstname || '%')
				AND t204.c204_void_fl IS NULL
				AND t202.c202_void_fl IS NULL
				AND t2021.c1900_company_id = v_company_id
				AND t202.c202_project_id = t2021.c202_project_id
				AND t2021.c2021_void_fl IS NULL
				;
				
				
				  
	END gm_fch_staffingreport;

	/*******************************************************
	* Description : Procedure to fetch the projects for a member
	* Author		: Satyajit Thadeshwar
	*******************************************************/
	PROCEDURE gm_fch_projectsbyparty (
		p_partyid	  IN	   t204_project_team.c101_party_id%TYPE
	  , p_outreport   OUT	   TYPES.cursor_type
	)
	AS
		v_company_id NUMBER;
		
	BEGIN
	
	SELECT get_compid_frm_cntx() 
	  INTO v_company_id  
	  FROM DUAL;
			  
		OPEN p_outreport
		 FOR
			 SELECT (t202.c202_project_id || ' - ' || t202.c202_project_nm) project
				  , get_code_name (t204.c901_role_id) ROLE, get_code_name (t204.c901_status_id) status
			   FROM t202_project t202, t204_project_team t204,t2021_project_company_mapping t2021
			  WHERE t202.c202_project_id = t204.c202_project_id
				AND t204.c101_party_id = p_partyid
				AND t204.c204_void_fl IS NULL
				AND t202.c202_void_fl IS NULL
				AND t2021.c1900_company_id = v_company_id
				AND t202.c202_project_id = t2021.c202_project_id
				AND t2021.c2021_void_fl IS NULL;
				
		
	END gm_fch_projectsbyparty;
END gm_pkg_pd_staffing_info;
/
