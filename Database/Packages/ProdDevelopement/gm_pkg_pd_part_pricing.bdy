--@"C:\Database\Packages\ProdDevelopement\gm_pkg_pd_part_pricing.bdy";
CREATE OR REPLACE
PACKAGE BODY GM_PKG_PD_PART_PRICING
IS
    --
	/****************************************************************
	* Description : Procedure used to update the part number pricing
	* Author      : 
	*****************************************************************/
PROCEDURE gm_save_part_num_price (
        p_project_id IN t205_part_number.c202_project_id%TYPE,
        p_inputstr   IN VARCHAR2,
        p_userid     IN t205_part_number.C205_LAST_UPDATED_BY%TYPE,
        p_action     IN VARCHAR2,
        p_out_parts OUT VARCHAR2)
AS
    v_strlen    NUMBER          := NVL (LENGTH (p_inputstr), 0) ;
    v_string    VARCHAR2 (4000) := p_inputstr;
    v_substring VARCHAR2 (1000) ;
    v_pnum t205_part_number.c205_part_number_id%TYPE;
    v_pdesc t205_part_number.C205_PART_NUM_DESC%TYPE;
    v_eprice t205_part_number.c205_equity_price%TYPE;
    v_lprice t205_part_number.c205_loaner_price%TYPE;
    v_cprice t205_part_number.c205_consignment_price%TYPE;
    v_liprice t205_part_number.c205_list_price%TYPE;
    v_update t205_part_number.c205_last_updated_date%TYPE;
    v_partNums t205_part_number.c205_part_number_id%TYPE;
    v_pnums      VARCHAR2 (4000) := '';
    v_part       VARCHAR2 (40) ;
    v_strpartNum VARCHAR2 (4000) := '';
    v_subPart    VARCHAR2 (1222) := '';
    v_strsep     NUMBER (10)     := 0;
    v_date       VARCHAR2 (20) ;
    v_dt_fmt     VARCHAR2 (10) ;
    v_company_id NUMBER;
BEGIN
	
	SELECT get_compid_frm_cntx()
	  INTO v_company_id 
	  FROM dual;
	          
        WHILE INSTR (v_string, '|') <> 0
        LOOP
            /**********************************************************************************************************
            *
            * Description : This Condition works if the user wants to update the price vlaues through ByProject Screen
            ***********************************************************************************************************
            */
            IF p_action      = 'Save' THEN
                v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|')                         - 1) ;
                v_string    := SUBSTR (v_string, INSTR (v_string, '|')                            + 1) ;
                --
                v_pnum      := UPPER (SUBSTR (v_substring, 1, INSTR (v_substring, '^')            - 1)) ;
                v_substring := SUBSTR (v_substring, INSTR (v_substring, '^')                      + 1) ;
                --
                v_pdesc     := SUBSTR (v_substring, 1, INSTR (v_substring, '^')                   - 1) ;
                v_substring := SUBSTR (v_substring, INSTR (v_substring, '^')                      + 1) ;
                --
                v_liprice   := TO_NUMBER (ABS ( (SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1)))) ;
                v_substring := SUBSTR (v_substring, INSTR (v_substring, '^')                      + 1) ;
                --
                v_lprice    := TO_NUMBER (ABS ( (SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1)))) ;
                v_substring := SUBSTR (v_substring, INSTR (v_substring, '^')                      + 1) ;
                --
                v_cprice    := TO_NUMBER (ABS ( (SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1)))) ;
                v_substring := SUBSTR (v_substring, INSTR (v_substring, '^')                      + 1) ;
                --
                v_eprice    := TO_NUMBER (ABS ( (SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1)))) ;
                --
                GM_SAVE_PARTNUMBER_PRICE (v_eprice, v_lprice, v_cprice, v_liprice, v_pnum, p_userid) ;
                /******************************************************************************************************
                * Description : This Condition works if the user wants to update the price vlaues through Batch Update
                Screen
                *****************************************************************************************************/
            ELSIF p_action       = 'Batch' THEN
                v_substring     := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
                v_string        := SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
                -- count the (,) char len. and store the (v_strsep)
                v_strsep        := NVL (LENGTH (REGEXP_REPLACE (v_substring, '[^,]')), 0) ;
                -- v_strsep is 0 then all the price set as empty value (to append 4 comma).
                IF v_strsep      = 0 THEN
                    v_substring := v_substring || ',,,,';
                -- v_strsep is 1 then , we have the list price so - to set the equality,loaner and CN price as empty.    
                ELSIF v_strsep   = 1 THEN
                    v_substring := v_substring || ',,,';
                -- v_strsep is 2 then, we have the list and equality price - to set the loaner and CN price as empty.
                ELSIF v_strsep   = 2 THEN
                    v_substring := v_substring || ',,';
                -- v_strsep is 3 then, we have the list, equality and loaner price - to set the  CN price as empty.
                ELSIF v_strsep   = 3 THEN
                    v_substring := v_substring || ',';
                END IF;
                v_pnum      := UPPER (SUBSTR (v_substring, 1, INSTR (v_substring, ',')            - 1)) ;
                v_substring := SUBSTR (v_substring, INSTR (v_substring, ',')                      + 1) ;
                --
                v_liprice   := TO_NUMBER (ABS ( (SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1)))) ;
                v_substring := SUBSTR (v_substring, INSTR (v_substring, ',')                      + 1) ;
                --
                v_lprice    := TO_NUMBER (ABS ( (SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1)))) ;
                v_substring := SUBSTR (v_substring, INSTR (v_substring, ',')                      + 1) ;
                --
                v_cprice    := TO_NUMBER (ABS ( (SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1)))) ;
                v_substring := SUBSTR (v_substring, INSTR (v_substring, ',')                      + 1) ;
                --
                v_eprice    := TO_NUMBER (ABS ( (v_substring))) ;
                
                /***********************************************
                * Description : For Updating the Price Values
                ***********************************************/
				gm_pkg_pd_part_pricing.gm_sav_part_pricing(v_pnum,v_eprice,v_lprice,v_cprice,v_liprice,v_company_id,p_userid);
            -- Once all the price related fetch quiries changed to T2052_PART_PRICE_MAPPING table we need to delete the below update.
                UPDATE t205_part_number
				SET c205_list_price   = NVL (v_liprice, c205_list_price),
					c205_equity_price = NVL (v_eprice, c205_equity_price),
				    c205_loaner_price = NVL (v_lprice, c205_loaner_price),
				    c205_consignment_price = NVL (v_cprice, c205_consignment_price),
				    c205_last_updated_by = p_userid, c205_last_updated_date = CURRENT_DATE
				  WHERE c205_part_number_id                       = v_pnum
				    AND c202_project_id                           = p_project_id;
                 -- This Condition checks for the part number is belongs to that project id or not   
                 IF (SQL%ROWCOUNT = 0) THEN
        			v_strpartNum := v_strpartNum || v_pnum || ', ';
    			END IF;   
                /******************************************************************************************************
                * Description : This Condition looks for the part numbers which are updated at that time and put all
                the part numbers in
                * some other variable and pass this variable to my_context.set_my_inlist_ctx(v_pnums);
                ******************************************************************************************************/
                v_pnums := v_pnums || v_pnum || ',';
            END IF;
        END LOOP;
        /***********************************************************************************************************
        * Description : if any part number entered which is not available in that project it raises the Error
        ************************************************************************************************************/
        v_strlen   := NVL (LENGTH (v_strpartNum), 0) ;
        IF v_strlen > 0 THEN
        	-- remove the last comma.
    		v_strpartNum :=  substr(v_strpartNum, 0, LENGTH(v_strpartNum)-2);
    		GM_RAISE_APPLICATION_ERROR('-20999','387',v_strpartNum||'#$#'||p_project_id );
           
        END IF;
    --
    p_out_parts := v_pnums;
END gm_save_part_num_price;
	/****************************************************************
	* Description : Procedure used to fetch the upload parts details
	* Author      : 
	*****************************************************************/
PROCEDURE gm_fch_upload_batch_parts (
        p_project_id IN t205_part_number.c202_project_id%TYPE,
        p_parts      IN VARCHAR2,
        p_userid     IN t205_part_number.C205_LAST_UPDATED_BY%TYPE,
        p_out_part_cur OUT TYPES.cursor_type)
AS
    v_dt_fmt t906_rules.c906_rule_value%TYPE;
    v_company_id  VARCHAR2(20);
BEGIN
    --
    my_context.set_my_inlist_ctx (p_parts) ;
    --
     SELECT get_rule_value ('DATEFMT', 'DATEFORMAT')
       INTO v_dt_fmt
       FROM DUAL;
      
     SELECT  get_compid_frm_cntx()
    	INTO    v_company_id
    	FROM    DUAL;
    	
    --
    OPEN p_out_part_cur FOR  
    SELECT t205.c205_part_number_id PNUMID, get_partnum_desc (t205.c205_part_number_id) PDESC, NVL (t2052.c2052_list_price, 0) Tier1
	  , NVL (t2052.c2052_equity_price, 0) Tier4, NVL (t2052.c2052_loaner_price, 0) Tier2, NVL (t2052.c2052_consignment_price, 0) Tier3
	  , to_char(c205_created_date,v_dt_fmt) CDATE, get_user_name (c205_created_by) CUSER, to_char(c205_last_updated_date,v_dt_fmt) LDATE
	  , get_user_name (c205_last_updated_by) LUSER
	   FROM t205_part_number t205, v_in_list view_in_list ,t2052_part_price_mapping t2052
	  WHERE t205.c205_part_number_id                          = view_in_list.token
	  	AND t205.c205_part_number_id = t2052.c205_part_number_id(+)
	  	AND t2052.c1900_company_id(+) = v_company_id
	  	AND t2052.c2052_void_fl(+) is NULL
	    AND c202_project_id                              = p_project_id
	    AND TRUNC (c205_last_updated_date) = TRUNC (CURRENT_DATE)
	    AND c205_last_updated_by                         = p_userid
	ORDER BY t205.c205_part_number_id;
END gm_fch_upload_batch_parts;
	/****************************************************************
	* Description : Procedure used to save the price for parts.
	* Author      : Manoj
	*****************************************************************/
PROCEDURE gm_sav_part_pricing (
        p_part_number        IN T2052_PART_PRICE_MAPPING.c205_part_number_id%TYPE,
        p_equity_price       IN T2052_PART_PRICE_MAPPING.c2052_equity_price%TYPE,
        p_loaner_price       IN T2052_PART_PRICE_MAPPING.c2052_loaner_price%TYPE,
        p_consignment_price  IN T2052_PART_PRICE_MAPPING.c2052_consignment_price%TYPE,
        p_list_price         IN T2052_PART_PRICE_MAPPING.c2052_list_price%TYPE,
        p_company_id         IN T2052_PART_PRICE_MAPPING.c1900_company_id%TYPE,
        p_user_id            IN T2052_PART_PRICE_MAPPING.c2052_updated_by%TYPE,
        p_comments           IN T2052_PART_PRICE_MAPPING.C2052_PRICE_COMMENTS%TYPE DEFAULT NULL
        )
AS
  v_company_id NUMBER;
BEGIN		

	          UPDATE T2052_PART_PRICE_MAPPING 
                 SET c2052_equity_price      =  p_equity_price,
       				 c2052_loaner_price      =  p_loaner_price,
       				 c2052_consignment_price =  p_consignment_price,
     				 c2052_list_price        =  p_list_price,
     				 C2052_PRICE_COMMENTS    =  p_comments,
      				 c2052_updated_by        =  p_user_id,
      				 c2052_updated_date      =  CURRENT_DATE,
      				 c2052_last_updated_by   =  p_user_id,
      				 C2052_Last_updated_date = CURRENT_DATE
 				WHERE c1900_company_id       =  p_company_id
  				  AND c205_part_number_id    =  p_part_number
  				  AND c2052_void_fl is NULL;
            
  			  IF(SQL%ROWCOUNT = '0') THEN 
  				INSERT INTO T2052_PART_PRICE_MAPPING
               				( C2052_PART_PRC_MAP_ID,c205_part_number_id, c2052_equity_price, c2052_loaner_price,
               			     c2052_consignment_price, c2052_list_price,c2052_price_comments, c2052_updated_by,c2052_last_updated_by,
               				 c2052_updated_date,  c1900_company_id)
                     VALUES ( S2052_PART_PRICE_MAPPING.NEXTVAL , p_part_number,   p_equity_price,  p_loaner_price, 
                              p_consignment_price, p_list_price,p_comments, p_user_id,p_user_id,
                              CURRENT_DATE,  p_company_id );
                              
               END IF;
  				  
    END gm_sav_part_pricing;
    
PROCEDURE gm_sav_part_price_by_group(
	p_group_id 			 IN t4010_group.c4010_group_id%TYPE,
	p_equity_price       IN T2052_PART_PRICE_MAPPING.c2052_equity_price%TYPE,
    p_loaner_price       IN T2052_PART_PRICE_MAPPING.c2052_loaner_price%TYPE,
    p_consignment_price  IN T2052_PART_PRICE_MAPPING.c2052_consignment_price%TYPE,
    p_list_price         IN T2052_PART_PRICE_MAPPING.c2052_list_price%TYPE,
    p_company_id         IN T2052_PART_PRICE_MAPPING.c1900_company_id%TYPE,
	p_userid             IN VARCHAR2,
	p_comments           IN T2052_PART_PRICE_MAPPING.C2052_PRICE_COMMENTS%TYPE  DEFAULT NULL
)
AS
	CURSOR cur_part_details
	IS
		--Fetching the parts which are primary in the group and the group priced type as Group.
		  SELECT T4011.C205_PART_NUMBER_ID PNUM
            FROM T4010_GROUP T4010,T4011_GROUP_DETAIL T4011 , T2023_PART_COMPANY_MAPPING T2023
           WHERE T4010.C4010_GROUP_ID = P_GROUP_ID
             AND T4010.C901_PRICED_TYPE=52110 -- GROUP
             AND T4010.C4010_GROUP_ID = T4011.C4010_GROUP_ID
             AND T4011.C901_PART_PRICING_TYPE = 52080 -- PRIMARY
             AND T2023.C1900_COMPANY_ID = P_COMPANY_ID
             AND T2023.C205_PART_NUMBER_ID = T4011.C205_PART_NUMBER_ID
             AND T4010.C4010_VOID_FL IS NULL 
             AND T2023.C2023_VOID_FL IS NULL;
BEGIN
	FOR var_part_detail IN cur_part_details
    LOOP
    	GM_PKG_PD_PART_PRICING.gm_sav_part_pricing(var_part_detail.pnum,p_equity_price,p_loaner_price,p_consignment_price,p_list_price,p_company_id,p_userid,p_comments);
    END LOOP;

END gm_sav_part_price_by_group;

END GM_PKG_PD_PART_PRICING;
/