create or replace
PACKAGE BODY gm_pkg_set_bundle_map
IS 
/* ********************************************************************
 * Author: Agilan singaravelan
 * Procedure:gm_sav_set_bundle_map
 * Description:Save set bundle map Details
 **********************************************************************/
PROCEDURE gm_sav_set_bundle_map  (
    p_set_bundle_name 	  IN   T2075_SET_BUNDLE.C2075_SET_BUNDLE_NM%TYPE
  , p_set_bundle_desc 	  IN   T2075_SET_BUNDLE.C2075_SET_BUNDLE_DESC%TYPE
  , p_set_bundle_dtl_desc IN   T2075_SET_BUNDLE.C2075_SET_BUNDLE_DTL_DESC%TYPE
  , p_division_id	      IN    T2075_SET_BUNDLE.C1910_DIVISION_ID%TYPE
  , p_status_id	          IN   T2075_SET_BUNDLE.C901_STATUS%TYPE
  , p_keywords	          IN   T2075_SET_BUNDLE.C2075_KEYWORDS%TYPE
  , p_company_id	      IN   t1900_company.C1900_COMPANY_ID%TYPE
  , p_set_bundle_id	      IN OUT  T2075_SET_BUNDLE.C2075_SET_BUNDLE_ID%TYPE
  , p_userid              IN T2075_SET_BUNDLE.C2075_LAST_UPDATED_BY%TYPE
    ) 
    AS
   v_set_bundle_id  T2075_SET_BUNDLE.C2075_SET_BUNDLE_ID%TYPE;
   v_company_id t1900_company.c1900_company_id%TYPE;
   v_cnt number:= 0;
   v_log_cnt number:= 0;
   v_map_cnt number:= 0;
   v_keyword_str   VARCHAR2 (4000) := p_keywords  || ',';
   v_substring     VARCHAR2 (4000) ;
   v_keyword       VARCHAR2 (4000) ;
   
	BEGIN
		   	UPDATE T2075_SET_BUNDLE 
			   SET 
        		   C2075_SET_BUNDLE_NM = p_set_bundle_name ,
         		   C2075_SET_BUNDLE_DESC = p_set_bundle_desc ,
        		   C2075_SET_BUNDLE_DTL_DESC = p_set_bundle_dtl_desc,
        		   C1910_DIVISION_ID = p_division_id,
         		   C901_STATUS = p_status_id,
        		   C2075_KEYWORDS = p_keywords,
                   C1900_COMPANY_ID = p_company_id,
         		   C2075_LAST_UPDATED_BY = p_userid,
             	   C2075_LAST_UPDATED_DATE = CURRENT_DATE        		  
     		 WHERE C1900_COMPANY_ID = p_company_id
    	       AND C2075_SET_BUNDLE_ID = p_set_bundle_id 
     		   AND C2075_VOID_FL IS NULL;
  
	     IF (SQL%ROWCOUNT = 0)    
	     THEN 
	     --------------       		 	
				SELECT COUNT(1)
				  INTO v_cnt
				  FROM T2075_SET_BUNDLE
				 WHERE C2075_SET_BUNDLE_NM = p_set_bundle_name
				   AND C1900_COMPANY_ID = p_company_id				 
				   AND C2075_VOID_FL IS NULL;
							
				IF (v_cnt > 0)
				THEN
				 	raise_application_error (-20999, 'Set Bunlde Name already exists.');
				END IF;   
	     
	    		SELECT S2075_SET_BUNDLE.NEXTVAL
				  INTO v_set_bundle_id
				  FROM DUAL;
				p_set_bundle_id := 'SB.' || v_set_bundle_id;
			    
	    		INSERT
			      INTO T2075_SET_BUNDLE
			     (  C2075_SET_BUNDLE_NM ,C2075_SET_BUNDLE_DESC,C2075_SET_BUNDLE_DTL_DESC,C1910_DIVISION_ID,C901_STATUS, 
			        C2075_KEYWORDS,C1900_COMPANY_ID,C2075_SET_BUNDLE_ID,C2075_LAST_UPDATED_BY,C2075_LAST_UPDATED_DATE 
			     )
			    VALUES
			     ( p_set_bundle_name,p_set_bundle_desc,p_set_bundle_dtl_desc,p_division_id,p_status_id,p_keywords,p_company_id,
			       p_set_bundle_id,p_userid,CURRENT_DATE        
		         ); 	    	
         ------------			        
		  END IF;
		  
		 -- KEYWORD HISTORY TABLE ENTRY
    	   	SELECT COUNT(1)
			  INTO v_log_cnt
			  FROM T2075B_SET_BUNDLE_KEYWORD_LOG
			 WHERE C2075_KEYWORDS = p_keywords
			   AND C2075_SET_BUNDLE_ID = p_set_bundle_id				 
			   AND C2075B_VOID_FL IS NULL;
	     	
			IF (v_log_cnt = 0)
			THEN			
				INSERT
				  INTO T2075B_SET_BUNDLE_KEYWORD_LOG
	 			   (   C2075B_SET_BUNDLE_KEY_LOG_ID,C2075_SET_BUNDLE_ID,C2075_KEYWORDS,C2075B_LAST_UPDATED_BY,C2075B_LAST_UPDATED_DATE
				   )
				VALUES
				   (   S2075B_SET_BUNDLE_KEYWORD_LOG.nextval,p_set_bundle_id,p_keywords,p_userid,CURRENT_DATE
				   );
			END IF;   	  
		----
		
		
		-- UNIQUE KEYWORD ENTRY
	        WHILE INSTR (v_keyword_str, ',') <> 0
        	LOOP         
            	
        		v_substring := SUBSTR (v_keyword_str, 1, INSTR (v_keyword_str, ',') - 1) ;
            	v_keyword_str := SUBSTR (v_keyword_str, INSTR (v_keyword_str, ',')    + 1) ;            	
            	v_keyword    := TRIM (v_substring) ;
                      	
	           UPDATE T2075A_SET_BUNDLE_KEYWORD 
				  SET C2075A_UNQUE_KEYWORD = v_keyword ,
	        		  C2075A_LAST_UPDATED_BY = p_userid ,
	         		  C2075A_LAST_UPDATED_DATE = CURRENT_DATE	        		     		  
	     		WHERE C2075_SET_BUNDLE_ID = p_set_bundle_id
	     		  AND C2075A_UNQUE_KEYWORD = v_keyword
	     		  AND C2075A_VOID_FL IS NULL;
     		    
	     		IF (SQL%ROWCOUNT = 0)    
     			THEN 
		       		INSERT
					  INTO T2075A_SET_BUNDLE_KEYWORD
					   (   C2075A_SET_BUNDLE_KEY_ID,C2075_SET_BUNDLE_ID,C2075A_UNQUE_KEYWORD,C2075A_LAST_UPDATED_BY,C2075A_LAST_UPDATED_DATE
					   )
					VALUES
					   (   S2075A_SET_BUNDLE_KEYWORD.nextval,p_set_bundle_id,v_keyword,p_userid,CURRENT_DATE
					   );
				END IF;	   
			   
	       END LOOP;  
	    ------
		-- SET NUNDLE MAPPING TABLE ENTRY
    	   	SELECT COUNT(1)
			  INTO v_map_cnt
			  FROM T2081_SET_BUNDLE_COMP_MAPPING
			 WHERE C2075_SET_BUNDLE_ID = p_set_bundle_id
			   AND C1900_COMPANY_ID = p_company_id
			   AND C2081_VOID_FL IS NULL;
		
			IF (v_map_cnt = 0)
			THEN			
				INSERT
				  INTO T2081_SET_BUNDLE_COMP_MAPPING
	 			   (C2081_SET_BUNDLE_COMP_MAP_ID,C2075_SET_BUNDLE_ID,C1900_COMPANY_ID,C901_SET_MAPPING_TYPE,C2081_LAST_UPDATED_BY,C2081_LAST_UPDATED_DATE
				   )
				VALUES
				   (S2081_SET_BUNDLE_COMP_MAPPING.nextval,p_set_bundle_id,p_company_id,p_status_id,p_userid,CURRENT_DATE
				   );
			END IF;   	  
		----	
END gm_sav_set_bundle_map;
     
/* *******************************************************************************
 * Author: Agilan singaravelan
 * Procedure:gm_fetch_set_bundle_map
 * Description:Fetch set bundle map Details
 ***********************************************************************************/              
	
PROCEDURE gm_fetch_set_bundle_map  (
	   p_set_bundle_id IN T2075_SET_BUNDLE.C2075_SET_BUNDLE_ID%TYPE
      , p_set_data	   OUT TYPES.cursor_type
      ) 
      AS
       v_company_id t1900_company.c1900_company_id%TYPE;
      BEGIN
 
         OPEN p_set_data
           FOR
             SELECT t2075.C2075_SET_BUNDLE_ID BID,      
                 	t2075.C2075_SET_BUNDLE_NM TXTSETNAME,              
                 	t2075.C2075_SET_BUNDLE_DESC TXTSETDESC,               
	                t2075.C2075_SET_BUNDLE_DTL_DESC TXTSETDETAILDESC,            
	                t2075.C1910_DIVISION_ID DIVNAME,                        
	                t2075.C901_STATUS STSNAME,                            
	                t2075.C2075_KEYWORDS TXTKEYWORDS,                     
	                t2075.C1900_COMPANY_ID COMPID 
     		   FROM T2075_SET_BUNDLE t2075
     		  WHERE t2075.C2075_SET_BUNDLE_ID = p_set_bundle_id     		    
     		    AND t2075.C2075_VOID_FL IS NULL;
     		    
END gm_fetch_set_bundle_map;
     
  /*****************************************************************************************
  *  Author: Agilan singaravelan
 * Procedure:gm_fetch_set_details
 * Description:Fetch Set Details
 *********************************************************************************************/   
   
 PROCEDURE  gm_fetch_set_details (
 p_set_id 		IN t207_set_master.c207_set_id%TYPE,
 p_out_cursor 	OUT TYPES.cursor_type)
 AS
  v_company_id t1900_company.c1900_company_id%TYPE;
 BEGIN
	 
	  SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL'))
  	    INTO v_company_id 
	    FROM dual;
		   
	 OPEN p_out_cursor
	 FOR
		 SELECT T207.C207_SET_ID SETID,
		 	    T207.C207_SET_NM SETDESC,
		 	    get_code_name(T207.C207_TYPE) SETYPE,
		 	    get_code_name(T207.C901_SET_GRP_TYPE) SETTYPE,
		 	    get_code_name(T207.C901_STATUS_ID) SETSTATUS
		   FROM T207_SET_MASTER T207 , T2080_SET_COMPANY_MAPPING T2080 
		  WHERE T207.C207_SET_ID = p_set_id
		    AND T207.C207_SET_ID = T2080.C207_SET_ID
		    AND T2080.C1900_COMPANY_ID = v_company_id
		    AND T207.C207_VOID_FL IS NULL
		    AND T2080.C2080_VOID_FL IS NULL;
		    
END gm_fetch_set_details;
   
 /* ******************************************************************************************
  * Author: Agilan singaravelan
 * Procedure:gm_sav_set_map_details
 * Description:Save Set Map Details
 **********************************************************************************************/    
PROCEDURE  gm_sav_set_map_details(    
     p_set_bundle_id IN OUT T2075_SET_BUNDLE.C2075_SET_BUNDLE_ID%TYPE,
     p_set_id 		 IN t207_set_master.c207_set_id%TYPE,
     p_userid        IN T2075_SET_BUNDLE.C2075_LAST_UPDATED_BY%TYPE
    )
    AS
    	v_company_id t1900_company.c1900_company_id%TYPE;
     	v_std_setid_str VARCHAR2 (4000) := p_set_id;
      	v_substring     VARCHAR2 (4000) ;
      	v_string     VARCHAR2 (4000) ;
        v_set_id t207_set_master.c207_set_id%TYPE;
        v_count NUMBER;
        v_compmap NUMBER;        
        
    BEGIN

	    SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL'))
  	      INTO v_company_id 
	      FROM dual;
	
   	   WHILE INSTR (v_std_setid_str, ',') <> 0
        	LOOP         
            	v_substring := SUBSTR (v_std_setid_str, 1, INSTR (v_std_setid_str, ',') - 1) ;
            	v_std_setid_str    := SUBSTR (v_std_setid_str, INSTR (v_std_setid_str, ',')    + 1) ;
            	v_set_id    := NULL;
            	v_set_id    := TRIM (v_substring) ;
            	
			SELECT COUNT(*)
	          INTO v_count
	          FROM T2076_SET_BUNDLE_DETAILS
	         WHERE C207_SET_ID = v_set_id 
	           AND C2075_SET_BUNDLE_ID = p_set_bundle_id         	 
	           AND C2076_VOID_FL IS NULL;	

		     IF (v_count = 0) THEN 	
		     	IF (v_set_id is not null) THEN
				   INSERT 
				   INTO T2076_SET_BUNDLE_DETAILS 
				      (T2076_SET_BUNDLE_DTL_ID,C2075_SET_BUNDLE_ID,C207_SET_ID,C2076_LAST_UPDATED_BY,C2076_LAST_UPDATED_DATE)  
				   VALUES 
				      (S2076_SET_BUNDLE_DETAILS.nextval,p_set_bundle_id,v_set_id,p_userid,CURRENT_DATE);
			     END IF; 
			  ELSE
			 	raise_application_error (-20999, '<b>Set ID : ' || v_set_id || ' </b>already mapped with this set bundle.');
			 END IF;

		 ---- Save company mapping 
			SELECT COUNT(*)
	          INTO v_compmap
	          FROM T2080_SET_COMPANY_MAPPING
	         WHERE C207_SET_ID = v_set_id	                   	 
	           AND C2080_VOID_FL IS NULL;	
	        
	        IF (v_compmap > 0 ) THEN
	 	        
	      		INSERT
				  INTO T2081_SET_BUNDLE_COMP_MAPPING
	 			   (C2081_SET_BUNDLE_COMP_MAP_ID,C2075_SET_BUNDLE_ID,C1900_COMPANY_ID,C901_SET_MAPPING_TYPE,C2081_LAST_UPDATED_BY,C2081_LAST_UPDATED_DATE
				   )		
				  (SELECT S2081_SET_BUNDLE_COMP_MAPPING.nextval,p_set_bundle_id,c1900_company_id,c901_set_mapping_type,p_userid,CURRENT_DATE
	        	     FROM T2080_SET_COMPANY_MAPPING
	        	    WHERE c207_set_id = v_set_id
	        	      AND c1900_company_id not in (
	        	      								select c1900_company_id 
	        	      								  from T2081_SET_BUNDLE_COMP_MAPPING 
	        	      								 where c2075_set_bundle_id = p_set_bundle_id
												       and c2081_void_fl is null  
												  )
	        	      AND c2080_void_fl is null				  
				  );
	        
	        END IF;
		 		   
	  END LOOP;
	   
END gm_sav_set_map_details;
	      
  /* ******************************************************************************************
  * Author: Agilan singaravelan
 * Procedure:gm_fetch_set_bundle_name
 * Description:Fetch Set Bundle Name
 **********************************************************************************************/       
PROCEDURE gm_fetch_set_bundle_name(
	  p_name OUT TYPES.cursor_type
	  )
	  AS
	  v_company_id t1900_company.c1900_company_id%TYPE;
	  BEGIN
        
		SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL'))
  	      INTO v_company_id 
	      FROM dual;
	      
		  OPEN p_name
		  FOR
		 	  SELECT T2075.C2075_SET_BUNDLE_ID ID,
                     T2075.C2075_SET_BUNDLE_NM NM
                FROM T2075_SET_BUNDLE T2075,T2081_SET_BUNDLE_COMP_MAPPING T2081
               WHERE T2081.C1900_COMPANY_ID = v_company_id
                 AND T2075.C2075_SET_BUNDLE_ID = T2081.C2075_SET_BUNDLE_ID
                 AND T2075.C2075_VOID_FL is NULL
                 AND T2081.C2081_VOID_FL IS NULL  
            ORDER BY UPPER(NM) ASC, T2075.C2075_LAST_UPDATED_DATE DESC;
			  
END gm_fetch_set_bundle_name;

 /* ******************************************************************************************
  * Author: Agilan singaravelan
 * Procedure:gm_check_set_id
 * Description:Check SetID
 **********************************************************************************************/    
PROCEDURE gm_check_set_id(
	  p_set_id			IN		T207_SET_MASTER.C207_SET_ID%TYPE
	, p_flag			OUT		varchar2
	)  
	AS
	v_count NUMBER;
	v_company_id t1900_company.c1900_company_id%TYPE;
	BEGIN

		SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL'))
  	      INTO v_company_id 
	      FROM dual;
	      
		SELECT COUNT(*)
          INTO v_count
          FROM T207_SET_MASTER T207 , T2080_SET_COMPANY_MAPPING T2080 
		  WHERE T207.C207_SET_ID = p_set_id
		    AND T207.C207_SET_ID = T2080.C207_SET_ID
		    AND T2080.C1900_COMPANY_ID = v_company_id
		    AND T207.C207_VOID_FL IS NULL
		    AND T2080.C2080_VOID_FL IS NULL;	 	
			
		   IF (v_count = 0)
			THEN
				p_flag		:= 'Y';
			ELSE
				p_flag		:= 'N';
		   END IF;	
		   
END gm_check_set_id;
	
/* ******************************************************************************************
  * Author: Agilan singaravelan
 * Procedure:gm_fch_sys_mapping
 * Description:Fetch System Mapping Details
 **********************************************************************************************/  		  
		
PROCEDURE gm_fch_sys_mapping (
            p_set_bundle_id IN t2076_set_bundle_details.c2075_set_bundle_id%TYPE,
            p_cur_sysid     OUT TYPES.cursor_type)
    AS
    v_company_id t1900_company.c1900_company_id%TYPE;
    BEGIN

	    SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL'))
  	      INTO v_company_id 
	      FROM dual;
	    
	      OPEN p_cur_sysid 
          FOR  
	        SELECT T207.C207_SET_ID SETID,
	        	   T207.C207_SET_NM SETNM, 
	        	   T207.C207_SET_NM PROJECTNM,
	    		   get_code_name(t207.c207_type) SETYPE,
	 	  		   get_code_name(t207.C901_SET_GRP_TYPE) SETTYPE,
	 	  		   get_code_name(t207.C901_STATUS_ID) SETSTATUS
	          FROM t207_set_master t207,t2076_set_bundle_details t2076,t2080_set_company_mapping t2080 
	         WHERE t2076.C2075_SET_BUNDLE_ID = p_set_bundle_id
	      	   AND t207.C207_SET_ID = t2076.C207_SET_ID
        	   AND T207.C207_SET_ID = T2080.C207_SET_ID
		       AND T2080.C1900_COMPANY_ID = v_company_id		       
		       AND T2080.C2080_VOID_FL IS NULL	
               AND t207.C207_VOID_FL IS NULL
               AND t2076.C2076_VOID_FL IS NULL;  
               
 END gm_fch_sys_mapping;
    
 /* ******************************************************************************************
  * Author: Agilan singaravelan
 * Procedure:gm_cs_sav_void_setbundle
 * Description:Void set bundle
 **********************************************************************************************/  
	PROCEDURE gm_cs_sav_void_setbundle (
		--p_set_bundle_id	IN	 t2076_set_bundle_details.c2075_set_bundle_id%TYPE,
		p_inputstr VARCHAR2,
	    p_user_id	    IN	 t2076_set_bundle_details.c2076_last_updated_by%TYPE,
	    p_msg	        OUT  VARCHAR2
	)
	AS
		--
		v_inputstr_len NUMBER          := NVL (LENGTH (p_inputstr), 0) ;
		v_void_fl	   t2076_set_bundle_details.c2076_void_fl%TYPE;
		v_inputstr_str VARCHAR2 (4000) := p_inputstr ;
		v_set_bundle_id	t2076_set_bundle_details.c2075_set_bundle_id%TYPE;
    	v_set_id t207_set_master.c207_set_id%type;
    	v_company_id t1900_company.c1900_company_id%TYPE;
    	v_count number := 0;
    	v_active number := 0;
		--
	BEGIN

          v_set_bundle_id   := NULL;
          v_set_id          := NULL;
          v_set_bundle_id   := SUBSTR (v_inputstr_str, 1, INSTR (v_inputstr_str, ',') - 1) ;
          v_set_id          := SUBSTR (v_inputstr_str, INSTR (v_inputstr_str, ',')    + 1) ;
		 
        SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL'))
  	      INTO v_company_id 
	      FROM dual;
          
        SELECT count(1)
		  INTO v_count
		  FROM t2076_set_bundle_details
	     WHERE c2075_set_bundle_id = v_set_bundle_id 
	       AND c207_set_id = v_set_id	       
	       AND c2076_void_fl = 'Y';
	    	              
        SELECT count(1) -- If the recodrd is voided and activated, again voided then have issue. So this code added.  
		  INTO v_active
		  FROM t2076_set_bundle_details
	     WHERE c2075_set_bundle_id = v_set_bundle_id 
	       AND c207_set_id = v_set_id	       
	       AND c2076_void_fl is null;	   

		IF (v_count > 0 AND v_active = 0)
		THEN
			raise_application_error (-20836, '');
		ELSE
			UPDATE t2076_set_bundle_details
			   SET c2076_void_fl = 'Y'
				 , c2076_last_updated_by = p_user_id
				 , c2076_last_updated_date = current_date
			 WHERE c2075_set_bundle_id = v_set_bundle_id
			   AND c207_set_id = v_set_id;
			   
			UPDATE t2081_set_bundle_comp_mapping    
			   SET c2081_void_fl = 'Y'
			     , c2081_last_updated_by = p_user_id
		     	 , c2081_last_updated_date = current_date
		     WHERE c2075_set_bundle_id = v_set_bundle_id
		       AND c1900_company_id in ( SELECT c1900_company_id 
		       							   FROM t2080_set_company_mapping
		       							  WHERE c207_set_id = v_set_id		       							    
		       							    AND c2080_void_fl is null )
			   AND c1900_company_id not in ( SELECT c1900_company_id  -- checking is any other set/set bundle mapped with company 
		       							       FROM t2080_set_company_mapping
		       							      WHERE c207_set_id in (select c207_set_id 
		       							                              from t2076_set_bundle_details 
		       							                             where c2075_set_bundle_id = v_set_bundle_id
		       							                               and c207_set_id != v_set_id
		       							                               and c2076_void_fl is null)
		       							        AND c2080_void_fl is null
		       							   )			       							    
		       AND c901_set_mapping_type != 105360		-- Created					    
               AND c2081_void_fl IS NULL;		       							    
		       							    
		END IF;
		
		p_msg := 'Void Set Bundle Mapping for ' || v_set_id || ' performed successfully' ;
		
	END gm_cs_sav_void_setbundle;    

/* ******************************************************************************************
  * Author: Agilan singaravelan
 * Procedure:gm_fetch_set_id
 * Description:Fetch Set Id
 **********************************************************************************************/       
	PROCEDURE gm_fetch_set_id (
		p_name OUT TYPES.cursor_type
	)
	AS
		v_company_id t1900_company.c1900_company_id%TYPE;
	BEGIN
		
		SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL'))
  	      INTO v_company_id 
	      FROM dual;
	      
		  OPEN p_name
		  FOR
		  	
		  	SELECT C207_SET_ID ID
		      FROM t2076_set_bundle_details t2076 , t2075_set_bundle t2075
		     WHERE t2076.c2075_set_bundle_id = t2075.c2075_set_bundle_id
		       AND t2075.C1900_company_id = v_company_id
		       AND C2076_VOID_FL is NULL
		       AND C207_SET_ID IS NOT NULL;
		       
	END gm_fetch_set_id;
 /*******************************************************
		* Description : Procedure to fetch set Bundle details
		* Procedure	  : gm_fch_setBundledetails
		* Author	  : Agilan Singaravel
  *******************************************************/
	PROCEDURE gm_fch_setBundledetails (
		p_setbundleid 	  IN       T2075A_SET_BUNDLE_KEYWORD.C2075A_SET_BUNDLE_KEY_ID%type
	  ,	p_setid 	      IN	   t207_set_master.c207_set_id%TYPE
	  , p_company_id	  IN   	   t1900_company.C1900_COMPANY_ID%TYPE
  	  , p_plant_id	      IN       t5040_plant_master.C5040_PLANT_ID%TYPE
	  , p_outdetails   	  OUT	   TYPES.cursor_type
	)
	AS
			
	BEGIN		
		
		OPEN p_outdetails
		FOR
				
			SELECT t2076.c207_set_id setid
	  			 , t207.c207_set_nm pdesc 
	  			 , gm_pkg_op_loaner.get_available_sets (t2076.c207_set_id, 0, 4127) qty
			  FROM t207_set_master t207,t2076_set_bundle_details t2076 , t2081_set_bundle_comp_mapping t2081
  			 WHERE t2076.c207_set_id        = t207.c207_set_id	
  			   AND t2076.c207_set_id IN (SELECT DISTINCT (t504.c207_set_id) SETID
											FROM t504_consignment t504,
											  t504a_consignment_loaner t504a,
											  T207_SET_MASTER t207,
											  T2080_SET_COMPANY_MAPPING t2080
											WHERE t504.c504_consignment_id = t504a.c504_consignment_id
											AND t207.c207_set_id           = t504.c207_set_id
											AND T504.C504_VOID_FL         IS NULL
											AND t504.c504_type             = 4127
											AND t504.c504_status_fl        = '4'
											AND t504a.C504A_STATUS_FL     != '60'
											AND t504a.c504a_void_fl       IS NULL
											AND t207.C207_VOID_FL         IS NULL
											AND T2080.C207_SET_ID          = T207.C207_SET_ID
											AND t2080.c2080_void_fl       IS NULL
											AND T2080.C1900_COMPANY_ID    IN
											  (SELECT C1900_COMPANY_ID
											  FROM T5041_PLANT_COMPANY_MAPPING
											  WHERE C5041_PRIMARY_FL = 'Y' AND C5040_PLANT_ID= p_plant_id
											  AND C5041_VOID_FL  IS NULL
											  )
											AND t504a.C5040_PLANT_ID = p_plant_id		   
  			   							) 
		 AND t2076.C2075_SET_BUNDLE_ID = p_setbundleid			       
         AND t2081.c1900_company_id = p_company_id
         AND t2081.c2081_void_fl is null
			   AND t2076.c2076_void_fl is null
			   AND t207.c207_void_fl is null
			UNION
			SELECT c207_set_id setid
			     , c207_set_nm pdesc
			     , gm_pkg_op_loaner.get_available_sets (p_setid, 0, 4127) qty
			  FROM t207_set_master
			 WHERE c207_set_id = p_setid AND c207_void_fl IS NULL;	

	END gm_fch_setBundledetails;
	
/*************************************************************
	* Description 	: Procedure to fetch set bundle company mapping values
	* Author		: Bala
**************************************************************/
	PROCEDURE gm_fch_setbundle_comp_map (
		   p_set_bundle_id           IN     t2075_set_bundle.c2075_set_bundle_id%TYPE,
		   p_sysbundle_compmap_cur   OUT	TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_sysbundle_compmap_cur
		 FOR
			SELECT t1900.c1900_company_id comp_id, 
			       t1900.c1900_company_name comp_nm,
			       get_code_name(t2081.c901_set_mapping_type) status,
			       get_user_name(t2081.c2081_last_updated_by) Released_by,
                   t2081.c2081_last_updated_date released_dt
			 FROM t2081_set_bundle_comp_mapping t2081, t1900_company t1900
			WHERE t2081.c2075_set_bundle_id = p_set_bundle_id
			  AND t1900.c1900_company_id = t2081.c1900_company_id
              AND t1900.c1900_void_fl IS NULL
			  AND t2081.c2081_void_fl IS NULL
			  ORDER BY t2081.c2081_last_updated_date,t1900.c1900_company_name;
	END gm_fch_setbundle_comp_map;
	
/*************************************************************
	* Description 	: Procedure to fetch history details on set bundle
	* Author		: Agilan
**************************************************************/
	PROCEDURE gm_fetch_history (
		   p_set_bundle_id       IN     t2075_set_bundle.c2075_set_bundle_id%TYPE,
		   p_setbundle_history   OUT	TYPES.cursor_type
	)
	AS
	v_comp_date_fmt  		varchar2(100);
	BEGIN			   
		SELECT get_compdtfmt_frm_cntx() INTO v_comp_date_fmt FROM DUAL;
		OPEN p_setbundle_history
		 FOR
			SELECT c2075_keywords KEYWORD,
  				   get_user_name(c2075b_last_updated_by) UPDATEDBY,				 
  				   TO_CHAR (c2075b_last_updated_date,v_comp_date_fmt) UPDATEDDATE
			  FROM T2075B_SET_BUNDLE_KEYWORD_LOG
			 WHERE c2075_set_bundle_id = p_set_bundle_id
			   AND c2075b_void_fl IS NULL
		  ORDER BY c2075b_last_updated_date DESC;
	END gm_fetch_history;
	
	/*************************************************************
	* Description 	: Procedure to fetch keywords count on set bundle
	* Author		: Agilan
**************************************************************/
	PROCEDURE gm_fetch_key_count (
		   p_set_bundle_id       IN     t2075_set_bundle.c2075_set_bundle_id%TYPE,
		   p_setbundle_count     OUT	number
	)
	AS
	BEGIN
			SELECT COUNT(1)
			  INTO p_setbundle_count
			  FROM T2075B_SET_BUNDLE_KEYWORD_LOG
			 WHERE c2075_set_bundle_id = p_set_bundle_id
			   AND c2075b_void_fl IS NULL;
			   
		END gm_fetch_key_count;

 END gm_pkg_set_bundle_map;
    /