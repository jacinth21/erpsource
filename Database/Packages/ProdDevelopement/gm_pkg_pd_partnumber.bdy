/* Formatted on 2010/03/10 09:56 (Formatter Plus v4.8.0) */
-- @"C:\database\Packages\ProdDevelopement\gm_pkg_pd_partnumber.bdy"

CREATE OR REPLACE PACKAGE BODY gm_pkg_pd_partnumber
IS
--
	PROCEDURE gm_pd_fch_multisetpart_report (
		p_report   OUT	 TYPES.cursor_type
	)
	 /*******************************************************
	* Description : Procedure to fetch MultiSet Part report
	* Author		: Xun Qu
	*******************************************************/
	AS
	BEGIN
		OPEN p_report
		 FOR
			 SELECT   t208.c208_set_qty setqty, t208.c208_inset_fl insetfl, t207.c207_set_id setid
					, t208.c205_part_number_id pnum, get_set_name (t207.c207_set_id) setdesc
					, t208.c208_critical_fl critfl, t208.c208_critical_qty critqty
					, get_partnum_desc (t208.c205_part_number_id) partdesc
				 FROM t208_set_details t208, t207_set_master t207
				WHERE t208.c207_set_id = t207.c207_set_id
				  AND t207.c901_set_grp_type = 1601
				  AND t208.c205_part_number_id IN (
										  SELECT   t208a.c205_part_number_id
											  FROM t208_set_details t208a, t207_set_master t207a
											 WHERE t208a.c207_set_id = t207a.c207_set_id
											   AND t207a.c901_set_grp_type = 1601
										  GROUP BY t208a.c205_part_number_id
											HAVING COUNT (1) > 1)
			 ORDER BY t208.c205_part_number_id, t207.c207_set_id;
	END gm_pd_fch_multisetpart_report;

--
	PROCEDURE gm_fch_part_all_detail (
		p_partnumber	 IN 	  t205_part_number.c205_part_number_id%TYPE
	  , p_partdetail	 OUT	  TYPES.cursor_type
	  , p_setdetail 	 OUT	  TYPES.cursor_type
	  , p_vendordetail	 OUT	  TYPES.cursor_type
	  , p_sub_part		 OUT	  TYPES.cursor_type
	  , p_parent_part	 OUT	  TYPES.cursor_type
	  , p_part_grp	 OUT	  TYPES.cursor_type
	)
	AS
	v_company_id  VARCHAR2(20);
/*******************************************************
 * Description : Procedure to fetch Part Detail
 * Author		: Xun Qu
 *******************************************************/
	BEGIN
		
		SELECT  get_compid_frm_cntx()
    	INTO    v_company_id
    	FROM    DUAL;
    	
		OPEN p_partdetail
		 FOR
			 SELECT t205.c205_part_number_id pnum, t205.c205_part_num_desc pdesc, t205.c202_project_id proid
				  , get_project_name (t205.c202_project_id) projnm, get_code_name (t205.c205_product_family) prodf
				  , t205.c205_rev_num rev, c205_active_fl afl, t2052.c2052_list_price tieri
				  , t2052.c2052_loaner_price tierii, t2052.c2052_consignment_price tieriii, t2052.c2052_equity_price tieriv
			   FROM t205_part_number t205 , t2052_part_price_mapping t2052
			  WHERE t205.c205_part_number_id = p_partnumber
			  AND t205.c205_part_number_id = t2052.c205_part_number_id(+) 
			  AND t2052.c1900_company_id(+) = v_company_id
			  AND t2052.c2052_void_fl(+) is NULL;

		gm_fch_part_set_detail (p_partnumber, p_setdetail);
		gm_fch_part_vendor_detail (p_partnumber, p_vendordetail);
		gm_fch_subpart_all_detail (p_partnumber, p_sub_part);
		gm_fch_parentpart_all_detail (p_partnumber, p_parent_part);
		gm_pkg_op_sheet.gm_fch_multi_part_grp (p_partnumber, p_part_grp);
	END gm_fch_part_all_detail;

--
	PROCEDURE gm_fch_part_set_detail (
		p_partnumber   IN		t205_part_number.c205_part_number_id%TYPE
	  , p_setdetail    OUT		TYPES.cursor_type
	)
	AS
/*******************************************************
 * Description : Procedure to fetch Part Set Detail
 * Author		: Xun Qu
 *******************************************************/
	BEGIN
		OPEN p_setdetail
		 FOR
			 SELECT   t207.c207_set_id setid, get_set_name (t207.c207_set_id) setdesc, t208.c208_set_qty setqty
					, t208.c208_inset_fl insetfl, t208.c208_critical_fl critfl, t208.c208_critical_qty critqty
				 FROM t208_set_details t208, t207_set_master t207
				WHERE t208.c207_set_id = t207.c207_set_id
				  AND t207.c901_set_grp_type = 1601
				  AND t208.c205_part_number_id = p_partnumber
				  AND t208.c208_void_fl IS NULL
			 ORDER BY t208.c205_part_number_id, t207.c207_set_id;
	END gm_fch_part_set_detail;

--
	PROCEDURE gm_fch_part_vendor_detail (
		p_partnumber	 IN 	  t205_part_number.c205_part_number_id%TYPE
	  , p_vendordetail	 OUT	  TYPES.cursor_type
	)
	AS
/*******************************************************
 * Description : Procedure to fetch Part Vendor Detail
 * Author		: Xun Qu
 *******************************************************/
	v_company_id  t1900_company.c1900_company_id%TYPE;
	BEGIN
		
		SELECT  get_compid_frm_cntx()
    	INTO    v_company_id
    	FROM    DUAL;
    	
		OPEN p_vendordetail
		 FOR
			  SELECT b.c301_vendor_id vid,T301.C301_VENDOR_SH_NAME vname, b.c405_cost_price cprice , TO_CHAR (b.c405_price_valid_date, 'mm/dd/yyyy') vdate,
  					 b.c405_qty_quoted qtyq ,get_code_name (b.c901_uom_id) uom, b.c405_uom_qty uomqty, c405_pricing_id pid ,
  					 b.c405_active_fl afl, b.c405_delivery_frame dframe, b.c405_vendor_quote_id qid ,
  					 B.C405_LOCK_FL LOCKFL ,GET_CODE_NAME(T301.C301_CURRENCY) currency
			  FROM T405_VENDOR_PRICING_DETAILS B, T301_VENDOR T301
			  WHERE B.C205_PART_NUMBER_ID = p_partnumber
	   		  AND B.C405_COST_PRICE      IS NOT NULL
	   		  AND B.c405_void_fl IS NULL
			  AND T301.c301_vendor_id     = B.c301_vendor_id
			  AND T301.c1900_company_id = v_company_id
			  ORDER BY b.c405_active_fl, b.c301_vendor_id,b.c405_cost_price;
	          END gm_fch_part_vendor_detail;

	--

	/*******************************************************
	* Description : Procedure to validate "release for sale" in part num during consignment shipout
	* Author		 : Joe P Kumar
	*******************************************************/
	PROCEDURE gm_val_release_for_sale (
		p_refid   IN   t907_shipping_info.c907_ref_id%TYPE
	  , p_type	  IN   t901_code_lookup.c901_code_id%TYPE
	)
	AS
		v_cnt		   NUMBER;
	BEGIN
		IF p_type = 4110
    THEN
      SELECT COUNT (*)
        INTO v_cnt
        FROM t505_item_consignment t505
         , t205_part_number t205
         , t504_consignment t504
         , t207_set_master t207
         , t205d_part_attribute t205d
       WHERE t505.c205_part_number_id = t205.c205_part_number_id
         AND t205.c205_part_number_id = t205d.c205_part_number_id
         AND t504.c207_set_id = t207.c207_set_id(+)
         AND t504.c504_consignment_id = t505.c504_consignment_id
         AND t505.c504_consignment_id = p_refid
         -- AND t205.c205_release_for_sale IS NULL
         AND t205d.c901_attribute_type = 80180
         AND t205d.c205d_void_fl IS NULL
         AND NVL (t207.c207_type, -9999) NOT IN (4077, 4078, 4073);
    END IF;
    /*released for sale changed to different country, and saved in t205d table,
    so need to join t205d to check the RFS */
    IF p_type = 50180
    THEN
      SELECT COUNT (*)
        INTO v_cnt
        FROM t502_item_order t502, t205_part_number t205, t205d_part_attribute t205d
       WHERE t502.c205_part_number_id = t205.c205_part_number_id
         AND t205.c205_part_number_id = t205d.c205_part_number_id
         AND t502.c501_order_id = p_refid
         AND t205d.c901_attribute_type = 80180
         AND t205d.c205d_void_fl IS NULL;
      -- AND t205.c205_release_for_sale IS NULL;
    END IF;
  
    IF v_cnt < 1
    THEN
			raise_application_error (-20127, '');	-- Some parts are not released for sale. Please ensure all parts are marked "Release for Sale"
		END IF;
	END gm_val_release_for_sale;

--

	/*******************************************************
	* Description : Procedure to fetch all sub-parts if the part has sub-parts
	* Author		 : Xun Qu
	*******************************************************/
	PROCEDURE gm_fch_subpart_all_detail (
		p_partnumber   IN		t205_part_number.c205_part_number_id%TYPE
	  , p_sub_part	   OUT		TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_sub_part
		 FOR
			SELECT t205a.c205_to_part_number_id partnum,
			  t205_to.c205_part_num_desc description,
			  t205_to.c202_project_id projid ,
			  t202.c202_project_nm projnm,
			  t205_to.c205_rev_num revnum,
			  c205a_qty qty ,
			  level
			FROM t205a_part_mapping t205a,
			  t205_part_number t205_from,
			  t205_part_number t205_to ,
			  t202_project t202
			WHERE t205_from.c205_part_number_id             = t205a.c205_from_part_number_id
			AND t205_to.c202_project_id                   = t202.c202_project_id
			AND t205_to.c205_part_number_id                 = t205a.c205_to_part_number_id
			AND t205a.c205a_void_fl is null
			  START WITH t205a.c205_from_part_number_id     = p_partnumber
			 CONNECT BY PRIOR t205a.c205_to_part_number_id = t205a.c205_from_part_number_id;
			              
	END gm_fch_subpart_all_detail;

--

	/*******************************************************
	* Description : Procedure to fetch all sub-parts if the part has sub-parts
	* Author		 : Xun Qu
	*******************************************************/
	PROCEDURE gm_fch_parentpart_all_detail (
		p_partnumber	IN		 t205_part_number.c205_part_number_id%TYPE
	  , p_parent_part	OUT 	 TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_parent_part
		 FOR
			  --SELECT c205_from_part_number_id partnum, get_partnum_desc (c205_from_part_number_id) description
			 -- 	 , c205a_qty qty
			  -- FROM t205a_part_mapping t205a
			 -- WHERE t205a.c205_to_part_number_id = p_partnumber;
			 SELECT 	c205_from_part_number_id partnum, get_partnum_desc (c205_from_part_number_id) description
					  , c205a_qty qty, LEVEL
				   FROM t205a_part_mapping t205a
				  WHERE t205a.c205a_void_fl is null --PC_4801 - to avoid to show voided parent details in parent Part deails section in part details report
			 START WITH t205a.c205_to_part_number_id = p_partnumber
			 CONNECT BY PRIOR t205a.c205_from_part_number_id = t205a.c205_to_part_number_id;
			              --AND t205a.c205a_void_fl is null;  ---  added new column c205a_void_fl in t205a used to PMT-50777 - Create BOM and sub-component Mapping
	END gm_fch_parentpart_all_detail;

	/*******************************************************
	* Description : Procedure to fetch all part mapping
	* Author		 : Xun Qu
	*******************************************************/
	PROCEDURE gm_fch_part_mapping (
		p_partnumber   IN		t205_part_number.c205_part_number_id%TYPE
	  , p_project_id   IN		t205_part_number.c202_project_id%TYPE
	  , p_orderpart    IN		VARCHAR2
	  , p_parts 	   OUT		TYPES.cursor_type
	)
	AS
	BEGIN
		IF p_orderpart = 'on'
		THEN
			OPEN p_parts
			 FOR
				 SELECT 	SUBSTR (SYS_CONNECT_BY_PATH (to_part, '->'), 3) pnum
						  , get_partnum_desc (to_part) description, qty, sub_part_to_order toorder, part_type, 1 lvl
					   FROM (SELECT 	t205a.c205_from_part_number_id from_part, t205a.c205_to_part_number_id to_part
									  , t205a.c205a_qty qty, NVL (t205d.c205_part_number_id, 'N') sub_part_to_order
									  , 'SP' part_type
								   FROM t205a_part_mapping t205a, t205d_sub_part_to_order t205d
								  WHERE t205a.c205_to_part_number_id = t205d.c205_part_number_id(+)
							 START WITH t205a.c205_from_part_number_id =
											(SELECT t205.c205_part_number_id pnum
											   FROM t205_part_number t205
											  WHERE t205.c202_project_id =
														  DECODE (p_project_id
																, '0', t205.c202_project_id
																, p_project_id
																 )
												AND t205.c205_part_number_id =
																			NVL (p_partnumber, t205.c205_part_number_id)
												AND t205.c205_sub_component_fl IS NULL
												AND t205.c205_part_number_id = t205a.c205_from_part_number_id
												AND t205.c205_active_fl = 'Y')
							 CONNECT BY PRIOR t205a.c205_to_part_number_id = t205a.c205_from_part_number_id
							              AND t205a.c205a_void_fl is null   ---  added new column c205a_void_fl in t205a used to PMT-50777 - Create BOM and sub-component Mapping
							 UNION ALL
							 SELECT NULL from_part, t205.c205_part_number_id to_part, 1 qty, 'N' sub_part_to_order
								  , 'PP' part_type
							   FROM t205_part_number t205
							  WHERE t205.c202_project_id =
														  DECODE (p_project_id
																, '0', t205.c202_project_id
																, p_project_id
																 )
								AND t205.c205_part_number_id = NVL (p_partnumber, t205.c205_part_number_id)
								AND t205.c205_sub_component_fl IS NULL
								AND t205.c205_active_fl = 'Y') MAPPING
				 START WITH MAPPING.from_part IS NULL
				 CONNECT BY PRIOR MAPPING.to_part = MAPPING.from_part;
		ELSE
			OPEN p_parts
			 FOR
				 SELECT 	to_part pnum, get_partnum_desc (to_part) description, qty, sub_part_to_order toorder
						  , part_type, LEVEL lvl
					   FROM (SELECT DISTINCT t205a.c205_from_part_number_id from_part, t205a.c205_to_part_number_id to_part
									  , t205a.c205a_qty qty, NVL (t205d.c205_part_number_id, 'N') sub_part_to_order
									  , 'SP' part_type
								   FROM t205a_part_mapping t205a, t205d_sub_part_to_order t205d
								  WHERE t205a.c205_to_part_number_id = t205d.c205_part_number_id(+)
							 START WITH t205a.c205_from_part_number_id =
											(SELECT t205.c205_part_number_id pnum
											   FROM t205_part_number t205
											  WHERE t205.c202_project_id =
														  DECODE (p_project_id
																, '0', t205.c202_project_id
																, p_project_id
																 )
												AND t205.c205_part_number_id =
																			NVL (p_partnumber, t205.c205_part_number_id)
												AND t205.c205_sub_component_fl IS NULL
												AND t205.c205_part_number_id = t205a.c205_from_part_number_id
												AND t205.c205_active_fl = 'Y')
							 CONNECT BY PRIOR t205a.c205_to_part_number_id = t205a.c205_from_part_number_id
							 AND t205a.c205a_void_fl is null  ---  added new column c205a_void_fl in t205a used to PMT-50777 - Create BOM and sub-component Mapping
							 UNION ALL
							 SELECT NULL from_part, t205.c205_part_number_id to_part, 1 qty, 'N' sub_part_to_order
								  , 'PP' part_type
							   FROM t205_part_number t205
							  WHERE t205.c202_project_id =
														  DECODE (p_project_id
																, '0', t205.c202_project_id
																, p_project_id
																 )
								AND t205.c205_part_number_id = NVL (p_partnumber, t205.c205_part_number_id)
								AND t205.c205_sub_component_fl IS NULL
								AND t205.c205_active_fl = 'Y') MAPPING
				 START WITH MAPPING.from_part IS NULL
				 CONNECT BY PRIOR MAPPING.to_part = MAPPING.from_part
				   ORDER SIBLINGS BY pnum;
		END IF;
	END gm_fch_part_mapping;

/*******************************************************
 * Description : Procedure to fetch Part Number Detail
 * Author		 : Rajeshwaran Varatharajan
 *******************************************************/
	PROCEDURE gm_fch_part_detail (
		p_partnumber		IN		 VARCHAR2
	  , p_subcomponentflg	IN		 t205_part_number.c205_sub_component_fl%TYPE
	  , p_partdetail		OUT 	 TYPES.cursor_type
	)
	AS
	
		v_company_id t1900_company.c1900_company_id%TYPE;
	
	BEGIN
	
	
	 	SELECT get_compid_frm_cntx()
    	   INTO v_company_id
    	   FROM DUAL;

		OPEN p_partdetail
		 FOR
			 SELECT   t205.c205_part_number_id pnum, c205_part_num_desc pdesc
					, DECODE (c205_crossover_fl, 'Y', 'on', c205_crossover_fl) mprjflg
				 FROM t205_part_number t205, t2023_part_company_mapping t2023
				WHERE regexp_like (t205.c205_part_number_id, REGEXP_REPLACE(p_partnumber,'[+]','\+'))
				  AND NVL (c205_sub_component_fl, '-9999') =
										 DECODE (p_subcomponentflg
											   , NULL, '-9999'
											   , NVL (c205_sub_component_fl, '-9999')
												)
				  AND t205.c205_part_number_id = t2023.c205_part_number_id 
				  AND t2023.c2023_void_fl IS NULL
				  AND t2023.c1900_company_id = v_company_id
			      ORDER BY t205.c205_part_number_id;
	END gm_fch_part_detail;

	/*******************************************************
	 * Description : Procedure to fetch Part Number Detail
	 * Author		: Rajeshwaran Varatharajan
	 *******************************************************/
	PROCEDURE gm_sav_multiprojectpart (
		p_partnumber   IN	VARCHAR2
	  , p_user_id	   IN	t205_part_number.c205_last_updated_by%TYPE
	)
	AS
		v_inputlen	   NUMBER := NVL (LENGTH (p_partnumber), 0);
		v_inputpartnumber VARCHAR2 (30000) := p_partnumber;
		v_substring    VARCHAR2 (1000);
		v_partnumber   t205_part_number.c205_part_number_id%TYPE;
		v_multiprjflg  t205_part_number.c205_multiple_proj_fl%TYPE;
		v_subcomp_fl   t205_part_number.c205_sub_component_fl%TYPE;
		v_partnumbertemp VARCHAR2 (1000);
		v_parent_multi_part  t205_part_number.c205_part_number_id%TYPE;
		
		----cursur to fetch sub-comp parts details for Input Parent part.
	  /*CURSOR cur_subcomp_parts (v_partnum_in VARCHAR2)  
		IS      
		SELECT C205_TO_PART_NUMBER_ID subcomppart
		 FROM t205a_part_mapping 
		 WHERE c205_from_part_number_id = v_partnum_in
		   AND c205a_void_fl is null    ---  added new column c205a_void_fl in t205a used to PMT-50777 - Create BOM and sub-component Mapping
		ORDER BY c205_from_part_number_id;*/

		--cursur to fetch sub-component for the parent and also a sub-part to order part. 
		CURSOR cur_subcomp_parts (v_partnum_in VARCHAR2)  
		IS 
		 SELECT c205_to_part_number_id  subcomppart
		   FROM t205a_part_mapping t205a,
                t205d_sub_part_to_order t205d
          WHERE t205a.c205_from_part_number_id=v_partnum_in
            AND t205a.c205_to_part_number_id = t205d.c205_part_number_id
            AND t205a.c205a_void_fl is null;   ---  added new column c205a_void_fl in t205a used to PMT-50777 - Create BOM and sub-component Mapping

	BEGIN
		IF v_inputlen > 0
		THEN
			--INPUT WILL BE ^102.041#Y|^103.042#Y|^103.043#N|^104.044#Y|^105.045#Y|
			WHILE INSTR (v_inputpartnumber, '|') <> 0
			LOOP
				v_substring := SUBSTR (v_inputpartnumber, 1, INSTR (v_inputpartnumber, '|') - 1);
				v_inputpartnumber := SUBSTR (v_inputpartnumber, INSTR (v_inputpartnumber, '|') + 1);
				v_partnumbertemp := SUBSTR (v_substring, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_partnumber := SUBSTR (v_partnumbertemp, 2, INSTR (v_partnumbertemp, '#') - 2);
				v_multiprjflg :=
							 SUBSTR (v_partnumbertemp, INSTR (v_partnumbertemp, '#') + 1
								   , INSTR (v_partnumbertemp, '^'));

				IF (v_partnumber IS NOT NULL)
				THEN
					UPDATE t205_part_number
					   SET c205_crossover_fl = DECODE (v_multiprjflg, 'N', '', v_multiprjflg)
						 , c205_last_updated_by = p_user_id
						 , c205_last_updated_date = CURRENT_DATE
					 WHERE c205_part_number_id = v_partnumber;
				END IF;
				
				SELECT c205_sub_component_fl 
				  INTO v_subcomp_fl
				  FROM t205_part_number  
				 WHERE c205_part_number_id = v_partnumber;
				 
				-- For Parent parts this v_subcomp_fl is null
				IF  v_subcomp_fl IS NULL
				THEN
					FOR v_rec IN cur_subcomp_parts(v_partnumber)   --Taking sub-comp parts details for Input Parent part.
					LOOP
					    IF v_multiprjflg = 'Y'    -- If parent part marked as multiproject then all sub-component parts should be marked  as multi-pro irrespective of how many parent parent part it has.
				 		THEN
					 		v_parent_multi_part := 0;
				 		ELSE					  -- If parent part un-marked as multiproject then we should check all the sub-component parent parts. 
						    v_parent_multi_part := 0;
							
						    SELECT count(1)       --checking the Multi-project parent part count for the sub-comp part.
							  INTO v_parent_multi_part 
						      FROM t205_part_number 
						 	 WHERE c205_part_number_id IN (
										SELECT c205_from_part_number_id 
										FROM t205a_part_mapping 
										WHERE c205_to_part_number_id= v_rec.subcomppart
										  AND c205a_void_fl is null    ---  added new column c205a_void_fl in t205a used to PMT-50777 - Create BOM and sub-component Mapping
										/*Also checking if the Sub-component part is a Multi-project part as well.
										UNION
										SELECT v_rec.subcomppart FROM DUAL*/
										) 
							  AND NVL(c205_crossover_fl,-9999)='Y';
							 
						 END IF;

				 		 IF v_parent_multi_part = 0   -- if the count = 0 then there will be only one parent part is mapped for the input part. so we can update the c205_crossover_fl.
				 		 THEN
					 		UPDATE t205_part_number
						       SET c205_crossover_fl = DECODE (v_multiprjflg, 'N', '', v_multiprjflg)
							     , c205_last_updated_by = p_user_id
							     , c205_last_updated_date = CURRENT_DATE
						     WHERE c205_part_number_id = v_rec.subcomppart;
				 		 END IF;
				 	END LOOP;
				END IF;
				
			END LOOP;
		END IF;
	END gm_sav_multiprojectpart;

	/*******************************************************
	* Description : Procedure to fetch all groups report
	* Author		: Xun Qu
	*******************************************************/
	PROCEDURE gm_pd_fch_groups_report (
		p_report   OUT	 TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_report
		 FOR
			 SELECT   c4010_group_id groupid, c4010_group_nm groupnm, get_set_name (c207_set_id) setnm
					, get_code_name (c901_group_type) grouptype, get_user_name (c4010_created_by) createdby
					, TO_CHAR (c4010_created_date, 'MM/DD/YYYY') createddate
				 FROM t4010_group t4010
			 ORDER BY groupnm;
	END gm_pd_fch_groups_report;  
    
  /*******************************************************
	* Description : Procedure to fetch all pathway details
	* Author		: Xun Qu
	*******************************************************/
    PROCEDURE gm_fch_part_pathway_dtls (
    p_num_id	IN		 t205_part_number.c205_part_number_id%TYPE
    , p_details	OUT 	 TYPES.cursor_type
  )
  AS
  BEGIN
    OPEN p_details
     FOR
       SELECT c205d_attribute_value attrvalue
         FROM t205d_part_attribute
        WHERE c901_attribute_type IN (80070, 80071)	---80070, 80071 (US, EU)
        AND c205_part_number_id = p_num_id
        AND c205d_void_fl IS NULL
	ORDER BY c901_attribute_type;
  END gm_fch_part_pathway_dtls;

  /*******************************************************
	* Description : Procedure to fetch all Regulatory details
	* Author		: Xun Qu
	*******************************************************/

  PROCEDURE gm_fch_part_regulatory_dtls (
    p_num_id	IN		 t205_part_number.c205_part_number_id%TYPE
    , p_details	OUT 	 TYPES.cursor_type
  )
  AS
  BEGIN
    OPEN p_details
     FOR
       SELECT t205d.c901_attribute_type attrtype
          , DECODE (get_code_control_type (t205d.c901_attribute_type)
              , 'COMBOX', get_code_name (t205d.c205d_attribute_value)
              , t205d.c205d_attribute_value
               ) attrvalue
         FROM t205d_part_attribute t205d, t901_code_lookup t901
        WHERE t205d.c901_attribute_type = t901.c901_code_id	--BETWEEN 80000 AND 80011
        AND t901.c901_code_grp = 'REGUL'
        AND c205_part_number_id = p_num_id
        AND c205d_void_fl IS NULL;
  END gm_fch_part_regulatory_dtls;

/************************************************************************
	  * Description : Procedure to save pathway and RFS to T205D_PART_ATTRIBUTE
	  * Author		: Xun
	  *******************************************************************/
    PROCEDURE gm_sav_part_qa_attribute (
    p_num_id	IN	 t205_part_number.c205_part_number_id%TYPE
    , p_pathway	IN	 VARCHAR2	--80070^80081|80071^80120
    , p_rfs		IN	 VARCHAR2	--80120, 80121, 80122,
    , p_user_id	IN	 t205_part_number.c205_last_updated_by%TYPE
    , p_taggable_FL IN t205d_part_attribute.c205d_attribute_value%TYPE
    , p_prodfly		IN		 t205_part_number.c205_product_family%TYPE
  )
  AS
    v_strlen	   NUMBER := NVL (LENGTH (p_pathway), 0);
    v_string	   VARCHAR2 (4000) := p_pathway;
    v_substring    VARCHAR2 (4000);
    v_attr_type    t205d_part_attribute.c901_attribute_type%TYPE;
    v_attr_value   t205d_part_attribute.c205d_attribute_value%TYPE;
    v_attr_id      t205d_part_attribute.c2051_part_attribute_id%TYPE;
    v_taggable_FL  t205d_part_attribute.c205d_attribute_value%TYPE;
    v_cnt		   NUMBER;
    v_attr_cnt	   NUMBER;
    v_company_id   NUMBER;
    v_serial_chk_fl		t205d_part_attribute.c205d_attribute_value%TYPE;
	v_lottrack_chk_fl	t205d_part_attribute.c205d_attribute_value%TYPE;
	v_skp_fl		VARCHAR2(2) := 'N';
    
    CURSOR cur_details
    IS
      SELECT token
        FROM v_in_list
       WHERE token IS NOT NULL;
  BEGIN
	
	-- Get company id from context
	SELECT get_compid_frm_cntx() INTO v_company_id FROM DUAL;
	  
    my_context.set_my_inlist_ctx (p_rfs);    
  	-- mnttask-6165 IMPACT SCREEN CHANGES
    /*DELETE FROM t205d_part_attribute
        WHERE c205_part_number_id = p_num_id AND c901_attribute_type = 80180;   --rfs*/
        
  	UPDATE t205d_part_attribute SET c205d_void_fl='Y',c205d_update_fl = null ,c205d_last_updated_date = CURRENT_DATE, c205d_last_updated_by = p_user_id--For Part Updates
        WHERE c205_part_number_id = p_num_id AND c901_attribute_type = 80180;   --rfs
	/*
        UPDATE T2023_PART_COMPANY_MAPPING
		  		SET 	C2023_VOID_FL ='Y',
		  				C2023_UPDATED_DATE = CURRENT_DATE,
		  				C2023_UPDATED_BY = p_user_id
		  		 WHERE 	C2023_VOID_FL IS NULL
				   AND 	C205_PART_NUMBER_ID = p_num_id
				   --AND	C1900_COMPANY_ID = p_company_id
				   ;
				   
		UPDATE T205H_PART_RFS_MAPPING
 		SET 	C205H_PROJ_COMP_SYNC ='',
				C205H_SYSTEM_COMP_SYNC ='',
 				C205H_UPDATED_BY = p_user_id ,
 				C205H_UPDATED_DATE = CURRENT_DATE
 		WHERE  	C205_PART_NUMBER_ID = p_num_id
                AND C205H_VOID_FL IS NULL;
       */         
    --This dor loop used to save rfs info to part attribute table
    FOR var_detail IN cur_details
    LOOP
      gm_sav_part_attribute (p_num_id, 80180, var_detail.token, p_user_id, v_attr_id);
      
--      gm_pkg_qa_rfs.gm_sav_part_comp_visibility(p_num_id,var_detail.token,'70171',p_user_id);
      
    END LOOP;
  
    -- This while loop used to save pathway info to part attribute table
    IF v_strlen > 0
    THEN
      WHILE INSTR (v_string, '|') <> 0
      LOOP
        --
        v_attr_type := NULL;
        v_attr_value := NULL;
        v_attr_cnt	 := 0;
        v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
        v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
        v_attr_type := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);        
        v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
        v_attr_value := v_substring;
        
        v_skp_fl := 'N';
        
        /* Check whether the attribute type is available in rules table as rule value with group id 'SAVCMPNY'
         * to save the company id. If the count is > 0, then save the company id for the attribute type
        */
        SELECT COUNT(1) INTO v_attr_cnt
			FROM t906_rules
		WHERE REGEXP_LIKE(c906_rule_value,'|'||v_attr_type||'|')
			AND c906_rule_grp_id = 'SAVCMPNY'
			AND c906_void_fl IS NULL;
        
		IF v_attr_cnt > 0
		THEN
			gm_pkg_pd_partnumber.gm_fch_checkbox_status(p_num_id,v_serial_chk_fl,v_lottrack_chk_fl);
			/*
			 * If the v_serial_chk_fl or v_lottrack_chk_fl is 'N', that means the check box is disabled, 
			 * so need to save a record in attribute table to lock the check boxed. 106042: Serial Number Locked, 106041: Lot Tracking Locked 
			 */
			IF v_serial_chk_fl = 'N' AND v_attr_type = 106040
			THEN
				gm_pkg_pd_partnumber.gm_sav_part_attribute_by_comp (p_num_id, '106042', 'Y', p_user_id, v_attr_id);
				v_skp_fl := 'Y';
			END IF;
			IF v_lottrack_chk_fl = 'N' AND v_attr_type = 104480
			THEN
				gm_pkg_pd_partnumber.gm_sav_part_attribute_by_comp (p_num_id, '106041', 'Y', p_user_id, v_attr_id);
				v_skp_fl := 'Y';
			END IF;
			
			IF v_skp_fl != 'Y' THEN
				gm_pkg_pd_partnumber.gm_sav_part_attribute_by_comp (p_num_id, v_attr_type, v_attr_value, p_user_id, v_attr_id);
			END IF;
		ELSE
			gm_sav_part_attribute (p_num_id, v_attr_type, v_attr_value, p_user_id, v_attr_id);
		END IF;
        
      END LOOP;
    END IF;
     -- save taggable flag 
	 
     SELECT COUNT (1)
        INTO v_cnt
        FROM t205d_part_attribute  
     WHERE   c205_part_number_id = p_num_id AND c901_attribute_type = 92340;
      
    IF ( p_prodfly = 4052 OR v_cnt > 0 )
    THEN   
        gm_sav_part_attribute (p_num_id, 92340, p_taggable_FL, p_user_id, v_attr_id); 
    END IF;
        
  END gm_sav_part_qa_attribute;
  /************************************************************************
	  * Description : Procedure to save attribute value to T205D_PART_ATTRIBUTE
	  * Author		: Xun
	  *******************************************************************/
  PROCEDURE gm_sav_part_attribute (
    p_num_id	   IN	t205_part_number.c205_part_number_id%TYPE
    , p_attr_type    IN	t205d_part_attribute.c901_attribute_type%TYPE
    , p_attr_value   IN	t205d_part_attribute.c205d_attribute_value%TYPE
    , p_user_id	   IN	t205_part_number.c205_last_updated_by%TYPE
    , p_attr_id    OUT t205d_part_attribute.c2051_part_attribute_id%TYPE
  )
  AS 
  v_partnumber t205_part_number.c205_part_number_id%TYPE;
  BEGIN
	   BEGIN
               SELECT c205_part_number_id
                 INTO v_partnumber
                 FROM t205_part_number
                WHERE c205_part_number_id = TRIM(p_num_id);
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  v_partnumber := NULL;
            END;

            IF v_partnumber IS NULL
            THEN
                GM_RAISE_APPLICATION_ERROR('-20999','388','');
            END IF;
            --	Adding new condition for RFS(80180) and remove the void flag for which are the countries are selected on screen - mnttask-6165
    IF p_attr_type = '200002' THEN
    	gm_pkg_pd_partnumber.gm_sav_part_attribute_by_comp (p_num_id, p_attr_type, p_attr_value, p_user_id, p_attr_id);
    ELSE
    --
    IF p_attr_type = '80180' THEN --rfs
     UPDATE t205d_part_attribute
       SET c205d_void_fl = NULL
       ,c205d_update_fl = null--For Part Updates
       , c205d_last_updated_date = CURRENT_DATE
       , c205d_last_updated_by = p_user_id
     WHERE c205_part_number_id = p_num_id AND c901_attribute_type = 80180 AND c205d_attribute_value = p_attr_value
       AND c205d_void_fl = 'Y'; 
    ELSE
    
	    IF p_attr_value IS NULL OR p_attr_value = ''
	    THEN
	    	DELETE FROM T205d_Part_Attribute 
	    	WHERE c205_part_number_id = p_num_id 
	    		AND c901_attribute_type =  DECODE (p_attr_type,'101262','80132','101263','80133',p_attr_type) 
	    		AND c901_attribute_type <> 80180;
	    END IF;
	    
	    UPDATE t205d_part_attribute
	       SET c205d_attribute_value = p_attr_value
	       , c205d_last_updated_date = CURRENT_DATE
	       , c205d_last_updated_by = p_user_id
	     WHERE c205_part_number_id = p_num_id AND c901_attribute_type =  DECODE (p_attr_type,'101262','80132','101263','80133',p_attr_type) AND c901_attribute_type <> 80180
	       AND c205d_void_fl IS NULL;
    END IF;
  
    IF (SQL%ROWCOUNT = 0 AND p_attr_value IS NOT NULL OR p_attr_value != '')
    THEN
      SELECT s205d_part_attribute.NEXTVAL
        INTO p_attr_id
        FROM DUAL;
  
      INSERT INTO t205d_part_attribute
            (c2051_part_attribute_id, c205_part_number_id, c901_attribute_type, c205d_attribute_value
             , c205d_created_date, c205d_created_by, c205d_last_updated_date, c205d_last_updated_by 
            )
         VALUES (p_attr_id, p_num_id, DECODE (p_attr_type,'101262','80132','101263','80133',p_attr_type),p_attr_value
             , CURRENT_DATE, p_user_id, CURRENT_DATE, p_user_id
            );
    END IF;
  END IF;-- Part level VAT
  END gm_sav_part_attribute;
  
	/********************************************************************
		* Description : Function to get all release for sale country id for 
    the part and separate by ',' 
		******************************************************************/  
 FUNCTION   get_rfs_info (
        p_num_id IN t205_part_number.c205_part_number_id%TYPE
    )
        RETURN VARCHAR2
    IS
        v_otherinfo       VARCHAR2 (1000);
    BEGIN 
        WITH other_info AS (
            SELECT t205d.c205d_attribute_value pattr, t205d.c2051_part_attribute_id  id
              from t205d_part_attribute t205d
             WHERE t205d.c205_part_number_id = p_num_id
             AND t205d.c901_attribute_type = 80180    --RFS
             AND t205d.c205d_void_fl IS NULL
        )
        SELECT MAX (SYS_CONNECT_BY_PATH (pattr, ','))
          INTO v_otherinfo
          FROM
            (SELECT pattr, ROW_NUMBER () OVER (ORDER BY id) AS curr
               FROM other_info)
          CONNECT BY curr - 1 = PRIOR curr
         START WITH curr = 1;
         RETURN SUBSTR (v_otherinfo, 2) ;
       EXCEPTION
          WHEN NO_DATA_FOUND
       THEN
          RETURN ''; 
    END get_rfs_info;   
  
  /********************************************************************
		* Description : Function to get all release for sale country name for 
    the part and separate by ',' 
		******************************************************************/    
   FUNCTION    get_rfs_name (
        p_num_id IN t205_part_number.c205_part_number_id%TYPE
    )
        RETURN VARCHAR2
    IS
        v_otherinfo       VARCHAR2 (1000);
    BEGIN 
        WITH other_info AS (
            SELECT get_code_name(t205d.c205d_attribute_value) pattr, t205d.c2051_part_attribute_id  id
              from t205d_part_attribute t205d
             WHERE t205d.c205_part_number_id = p_num_id
             AND t205d.c901_attribute_type = 80180    --RFS
             AND t205d.c205d_void_fl IS NULL
        )
        SELECT MAX (SYS_CONNECT_BY_PATH (pattr, ','))
          INTO v_otherinfo
          FROM
            (SELECT pattr, ROW_NUMBER () OVER (ORDER BY id) AS curr
               FROM other_info)
          CONNECT BY curr - 1 = PRIOR curr
         START WITH curr = 1;
        RETURN SUBSTR (v_otherinfo, 2) ;
       EXCEPTION
          WHEN NO_DATA_FOUND
       THEN
          RETURN ''; 
    END get_rfs_name; 
    
   /************************************************************
	* Description 	: Procedure to save all Part leadtime details
	* Author		: Jignesh Shah
	*************************************************************/
	
	PROCEDURE gm_sav_part_leadtime (
		p_attr_type		IN	 t205d_part_attribute.c901_attribute_type%TYPE
	  , p_input_str		IN	 VARCHAR2
	  , p_user_id		IN	 t205_part_number.c205_last_updated_by%TYPE
	)
	AS
	    v_strlen		NUMBER := NVL (LENGTH (p_input_str), 0);
	    v_string	   	VARCHAR2 (30000) := p_input_str;
	    v_substring    	VARCHAR2 (30000);
	    v_pnum_id    	t205_part_number.c205_part_number_id%TYPE;
	    v_attr_type    	t205d_part_attribute.c901_attribute_type%TYPE;
	    v_attr_value   	t205d_part_attribute.c205d_attribute_value%TYPE;
	    v_attr_id      	t205d_part_attribute.c2051_part_attribute_id%TYPE;
	    v_cnt		   	NUMBER;
	BEGIN
		IF v_strlen > 0
	    THEN
	      WHILE INSTR (v_string, '|') <> 0
	      LOOP
	        v_attr_type := NULL;
	        v_attr_value := NULL;
	        v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
	        v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
	        v_pnum_id := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
	        v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
	        v_attr_value := v_substring;
		     
	        IF v_attr_value IS NOT NULL THEN
		    	gm_pkg_pd_partnumber.gm_sav_part_attribute (v_pnum_id, p_attr_type, v_attr_value, p_user_id, v_attr_id);
		    END IF; 
	      END LOOP;
	    END IF;
	END gm_sav_part_leadtime;
 
	/************************************************************
	* Description 	: Procedure to fetch all Part leadtime details
	* Author		: Jignesh Shah
	*************************************************************/
	
	PROCEDURE gm_fch_part_leadtime (
		p_part_number   IN	CLOB
	  , p_proj_id		IN	t205_part_number.c202_project_id%TYPE
	  , p_out_cur	    OUT TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_out_cur
		FOR
			SELECT * FROM (
				SELECT t205.c205_part_number_id pnum, t205.c205_part_num_desc description
						, get_part_attribute_value (t205.c205_part_number_id, 4000101) leadtime
						, decode(t205.c205_sub_component_fl, 'Y', decode( t205ds.c205_part_number_id, null, 'N', 'Y'), 'Y')  leadtime_field
						, get_part_attr_hstry_fl (t205.c205_part_number_id, 4000101) history_fl
						, get_user_name(t205d.c205d_last_updated_by) lastupdatedby
						, to_char(t205d.c205d_last_updated_date,'MM/DD/YYYY') lastupdateddate
						, get_log_flag (t205.c205_part_number_id,4000104) comments
				FROM t205_part_number t205
					, (SELECT c205_part_number_id, c205d_attribute_value, c205d_last_updated_by, c205d_last_updated_date,c901_attribute_type
					FROM t205d_part_attribute
					WHERE c901_attribute_type = 4000101
					AND C205d_Void_Fl IS NULL
					) t205d, t205d_sub_part_to_order t205ds
				WHERE t205.c205_part_number_id = t205d.c205_part_number_id(+)				
			        AND t205.c205_part_number_id  = t205ds.c205_part_number_id(+)
				AND t205.c202_project_id = DECODE (p_proj_id, '0', t205.c202_project_id, p_proj_id)
			--	AND t205.c205_part_number_id LIKE NVL(v_pnum, t205.c205_part_number_id) || '%' 
			AND REGEXP_LIKE(t205.c205_part_number_id, CASE WHEN TO_CHAR(p_part_number) IS NOT NULL THEN TO_CHAR(REGEXP_REPLACE(p_part_number,'[+]','\+')) ELSE REGEXP_REPLACE(t205.c205_part_number_id,'[+]','\+')  END)
				AND t205.c205_active_fl = 'Y'
				AND t205ds.c205d_void_fl is NULL
				ORDER BY pnum ) A
			WHERE A.leadtime_field  = 'Y';
	END gm_fch_part_leadtime;
	
	/************************************************************
	* Description 	: Procedure to check the availability of part number
	* Author		: Arajan
	*************************************************************/
	
	PROCEDURE gm_validate_part_number (
		p_part_number IN t205_part_number.c205_part_number_id%TYPE,
		p_out_msg OUT VARCHAR2
	)
	AS
	BEGIN
		SELECT COUNT(1)
		INTO p_out_msg
		FROM t205_part_number
		WHERE UPPER(c205_part_number_id) = TRIM(UPPER(p_part_number));
	END gm_validate_part_number;
	
	/*****************************************************************
	* Description : Procedure used to fetch all Consignment Parts dtls
	* Author  :
	******************************************************************/
	PROCEDURE gm_fch_trans_parts (
	        p_consign_id IN t504_consignment.c504_consignment_id%TYPE,
	        p_out_parts OUT CLOB)
	AS
	BEGIN
	     SELECT RTRIM (XMLAGG (XMLELEMENT (e, c205_part_number_id || ',')) .EXTRACT ('//text()'), ',')
	       INTO p_out_parts
	       FROM t505_item_consignment
	      WHERE c504_consignment_id = p_consign_id
	        AND c505_void_fl       IS NULL;
	END gm_fch_trans_parts; 
	/******************************************************************
	* Description : Procedure to fetch the part number detail for product catalog.
	* 
	****************************************************************/
	PROCEDURE gm_fch_partnumber_prodcat (
		p_token  		IN 		t101a_user_token.c101a_token_id%TYPE,
		p_langid		IN		NUMBER,
		p_page_no		IN		NUMBER,
	    p_uuid		    IN      t9150_device_info.c9150_device_uuid%TYPE,
		p_parts_cur		OUT		TYPES.CURSOR_TYPE
	)
	AS
		v_deviceid  t9150_device_info.c9150_device_info_id%TYPE;
		v_count	    NUMBER;
		v_page_no   NUMBER;
		v_page_size NUMBER;
	  	v_start     NUMBER;
	    v_end       NUMBER;
	    v_last_rec	t9151_device_sync_dtl.C9151_LAST_SYNC_REC%TYPE; 
	BEGIN
		--get the device info id based on user token
		v_deviceid := get_device_id(p_token, p_uuid);
		/*below prc check the device's last updated date for part number reftype and
		if record exist then it checks is there any parts updated after the device updated date.
		if updated parts available then add all parts into v_inlist and return count of v_inlist records*/
		gm_pkg_pdpc_prodcatrpt.gm_fch_device_updates(v_deviceid,p_langid,4000411,p_page_no,v_count,v_last_rec); --4000411 - Part num reftype
		
		--If there is no data available for device then call below prc to add all parts.
		IF v_count = 0 THEN
			gm_pkg_pdpc_prodcatrpt.gm_fch_all_parts_tosync;
			
		END IF;

		v_page_no := p_page_no;
		-- get the paging details
		gm_pkg_pdpc_prodcatrpt.gm_fch_paging_dtl('PART_NUM','PAGESIZE',v_page_no,v_start,v_end,v_page_size);
		/*
		 * Need to get the di number and udi pattern based on the issuing agency to the IPAD while synchronizing the part number 
		 * 4000539: Issuing Agency Configuration
		 */

		OPEN p_parts_cur
			FOR		
			 SELECT COUNT (1) OVER () pagesize, result_count totalsize, v_page_no pageno, rwnum, CEIL (result_count / v_page_size) totalpages,
			   t205.*
			   FROM
			    (
			         SELECT T205.*, ROWNUM rwnum, COUNT (1) OVER () result_count, MAX(DECODE(t205.partnum,v_last_rec,ROWNUM,NULL)) OVER() LASTREC
			           FROM
			            (
						SELECT t205.c205_part_number_id partnum
						     ,NVL (t205.c205_part_num_name, t205.c205_part_num_desc) partdesc
						     , t205.c205_part_num_dtl_desc partdtldesc
						     --, t205.c901_status_id partstat
						     , t205.c205_product_family partfmly, DECODE (temp.void_fl, NULL, t205.c205_active_fl, 'N') partactfl
						     , sysdata.setid systemid, t205.c205_product_class prodcls, t205.c205_product_material prodmat
						     , t2060.c2060_di_number dinumber
						     , REPLACE (REPLACE (v1610.issuing_agency_config_with_nm , '|', ''), 'DI', t2060.c2060_di_number) udipattern
						  FROM t205_part_number t205
						     , (SELECT DISTINCT (t207.c207_set_id) setid, c205_part_number_id pnum
						                   FROM t207_set_master t207, t208_set_details t208
						                  WHERE t207.c901_set_grp_type = '1600'   --sales Report Grp
						                    AND t208.c207_set_id = t207.c207_set_id
						                    AND t207.c207_void_fl IS NULL) sysdata
						     , my_temp_prod_cat_info temp
						     , t2060_di_part_mapping t2060
						     , t205d_part_attribute t205d
						     , v1610_issuing_agency_config v1610    
						WHERE t205.c205_product_family IN (SELECT C906_RULE_VALUE
														   FROM T906_RULES
														  WHERE C906_RULE_ID     = 'PART_PROD_FAMILY'
														    AND C906_RULE_GRP_ID = 'PROD_CAT_SYNC'
														    AND c906_void_fl    IS NULL)   --Implants, Instruments, Graphic Cases, Literature
						   AND t205.c205_part_number_id = sysdata.pnum
						   AND t205.c205_part_number_id = temp.ref_id
						   AND t205.c205_part_number_id = t2060.c205_part_number_id(+)
						   AND t205.c205_part_number_id = t205d.c205_part_number_id(+)
						   AND t205d.c901_attribute_type(+) = 4000539
						   AND t205d.c205d_void_fl(+) IS NULL
						   AND t205d.c205d_attribute_value = v1610.c1610_issue_agency_config_id (+) 
						   ORDER BY t205.c205_part_number_id
			             )t205
			     )t205
			  WHERE rwnum BETWEEN NVL(LASTREC+1,DECODE(v_last_rec,NULL,v_start,v_start-1)) AND v_end;

		
	END gm_fch_partnumber_prodcat;
	/******************************************************************
	* Description : Procedure to fetch the part number by attribute type and attribute value.
	****************************************************************/
	PROCEDURE gm_fch_part_list_by_attribute (
	        p_attr_type  IN  t205d_part_attribute.c901_attribute_type%TYPE,
	        p_attr_value IN  t205d_part_attribute.c205d_attribute_value%TYPE,
	        p_out 		 OUT TYPES.CURSOR_TYPE
	)
	AS
	BEGIN
		OPEN p_out
			FOR		
		     SELECT t205.c205_part_number_id partnum, t205.c205_part_number_id ||' - '||t205.c205_part_num_desc pdesc
		       FROM t205_part_number t205, (
		             SELECT c205_part_number_id, c205d_attribute_value
		               FROM t205d_part_attribute t205d
		              WHERE t205d.c205d_void_fl      IS NULL
		                AND c205d_attribute_value     = NVL (p_attr_value, c205d_attribute_value) 
		                AND t205d.c901_attribute_type = p_attr_type)
		                t205d
		              WHERE t205.c205_part_number_id = t205d.c205_part_number_id
		                AND t205.c205_active_fl     IS NOT NULL
		                ORDER BY t205.c205_part_number_id,t205.c205_part_num_desc;
	END gm_fch_part_list_by_attribute;
	
	/******************************************************************
	* Description : Procedure used to save label attr values
	* Author  : Bala
	*******************************************************************/	
	PROCEDURE gm_sav_part_params (
	  p_num_id	   IN	t205_part_number.c205_part_number_id%TYPE,
      p_lbl_attr    IN	VARCHAR2,    
      p_user_id	   IN	t205_part_number.c205_last_updated_by%TYPE    
    )
    AS
    v_strlen	   NUMBER := NVL (LENGTH (p_lbl_attr), 0);
    v_string	   VARCHAR2 (4000) := p_lbl_attr;
    v_substring    VARCHAR2 (4000);
    v_attr_type    t205d_part_attribute.c901_attribute_type%TYPE;
    v_attr_value   t205d_part_attribute.c205d_attribute_value%TYPE;
    v_attr_id      t205d_part_attribute.c2051_part_attribute_id%TYPE;
    BEGIN
	    -- This while loop used to save p_lbl_attr info to part attribute table
    IF v_strlen > 0
    THEN
      WHILE INSTR (v_string, '|') <> 0
      LOOP        
        v_attr_type := NULL;
        v_attr_value := NULL;
        v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
        v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
        v_attr_type := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);        
        v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
        v_attr_value := v_substring;
        gm_sav_part_attribute (p_num_id, v_attr_type, v_attr_value, p_user_id, v_attr_id);        
      END LOOP;
    END IF;          
    END gm_sav_part_params;
    
    /******************************************************************
	* Description : Procedure to fetch the Part dhr details
	* Author  : mmuthusamy
	*******************************************************************/
    
    PROCEDURE gm_fch_part_dhr_dtls (
    		  p_part_number_id		    IN   t205_part_number.c205_part_number_id%TYPE
    		 ,p_out_dhr_dtls        OUT   TYPES.cursor_type
    )
    AS
    
    BEGIN
	    OPEN p_out_dhr_dtls
	    FOR
	      SELECT t205.c205_part_number_id partNumber,t205.c205_material_cert_fl materialCert,t205.c205_hard_test_fl handnessCert, 
	      t205.c205_packaging_prim primaryPack, t205.c205_packaging_sec secondaryPack, t205.c205_compliance_cert_fl complianceCert,
          GET_PART_ATTRIBUTE_VALUE (C205_PART_NUMBER_ID, '4000539') issuingAgencyConf,
	      GET_PART_ATTRIBUTE_VALUE (C205_PART_NUMBER_ID, '103379') diNumberFl,
          GET_PART_ATTRIBUTE_VALUE (C205_PART_NUMBER_ID,'103380') woCreated,
          GET_PART_ATTRIBUTE_VALUE ( C205_PART_NUMBER_ID, '103381') udiEtchReq, 
          GET_PART_ATTRIBUTE_VALUE (C205_PART_NUMBER_ID, '103382') diOnlyEtchReq, GET_PART_ATTRIBUTE_VALUE (C205_PART_NUMBER_ID, '103383')
          piOnlyEtchReq, NVL(TRIM(GET_PART_ATTRIBUTE_VALUE (C205_PART_NUMBER_ID, '4000517')),'103421') vendorDesign,
          gm_pkg_pd_rpt_udi.get_part_di_number (C205_PART_NUMBER_ID) diNumber, GET_PART_ATTRIBUTE_VALUE (C205_PART_NUMBER_ID, '4000540') bulkPackage
          ,DECODE (t205.c205_product_class, '4030', 'Y', 'N') sterilePart, t205.c901_status_id partStatus
          ,gm_pkg_pd_partnumber.get_dhr_tab_sam_pattern(t205.c205_part_number_id) samplePattern
          , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1150) issuing_Agency_fl -- 4000539 Issuing Agency
          FROM t205_part_number t205
          WHERE t205.c205_part_number_id = p_part_number_id;
	    END gm_fch_part_dhr_dtls;
	    
	    
    /******************************************************************
	* Description : Procedure to save the Part dhr details
	* Author  : mmuthusamy
	*******************************************************************/	
	PROCEDURE  gm_sav_part_dhr_req (
	  p_num_id	   IN	t205_part_number.c205_part_number_id%TYPE,
      p_matspec		IN		 t205_part_number.c205_material_cert_fl%TYPE,
      p_hardtestfl	IN		 t205_part_number.c205_hard_test_fl%TYPE,
      p_primpack		IN		 t205_part_number.c205_packaging_prim%TYPE,
  	  p_secpack		IN		 t205_part_number.c205_packaging_sec%TYPE,
  	  p_compcert		IN		 t205_part_number.c205_compliance_cert_fl%TYPE,
  	  p_part_att_str   IN   VARCHAR2,
      p_user_id	   IN	t205_part_number.c205_last_updated_by%TYPE    
    )
    AS
    v_strlen	   NUMBER := NVL (LENGTH (p_part_att_str), 0);
    v_string	   VARCHAR2 (4000) := p_part_att_str;
    v_substring    VARCHAR2 (4000);
    v_attr_type    t205d_part_attribute.c901_attribute_type%TYPE;
    v_attr_value   t205d_part_attribute.c205d_attribute_value%TYPE;
    v_attr_id      t205d_part_attribute.c2051_part_attribute_id%TYPE;
    v_issue_agency_id t1600_issue_agency.c1600_issuing_agency_id%TYPE;
    v_di_number t2060_di_part_mapping.c2060_di_number%TYPE;
    v_system_id	   T207_SET_MASTER.C207_SET_ID%TYPE;
    v_cnt 		   NUMBER;
    BEGIN
	    -- to update the part number DHR details
	  UPDATE t205_part_number t205
		SET t205.c205_material_cert_fl                       = DECODE (p_matspec, '0', NULL, p_matspec),
			t205.c205_hard_test_fl							 = DECODE (p_hardtestfl, '0', NULL, p_hardtestfl),
			t205.c205_packaging_prim						 = DECODE (p_primpack, 0, NULL, p_primpack),
		    t205.c205_packaging_sec                          = DECODE (p_secpack, 0, NULL, p_secpack),
		    t205.c205_compliance_cert_fl                     = DECODE (p_compcert, '0', NULL, p_compcert),
		    t205.c205_last_updated_by                        = p_user_id,
		    t205.c205_last_updated_date						 = CURRENT_DATE
		  WHERE t205.c205_part_number_id                     = p_num_id;
-- This while loop used to save p_lbl_attr info to part attribute table		  
    IF v_strlen > 0
    THEN
      WHILE INSTR (v_string, '|') <> 0
      LOOP        
        v_attr_type := NULL;
        v_attr_value := NULL;
        v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
        v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
        v_attr_type := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);        	
        v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
        v_attr_value := v_substring;
        --
        --4000539	Issuing Agency Configuration
        IF(v_attr_type ='4000539' AND v_attr_value IS NOT NULL)
        THEN
        v_di_number := gm_pkg_pd_rpt_udi.get_part_di_number(p_num_id);
        IF (v_di_number IS NULL)
        THEN
	        SELECT c1600_issuing_agency_id
			   INTO v_issue_agency_id
			   FROM t1610_issue_agency_config
			  WHERE c1610_issue_agency_config_id = v_attr_value
			    AND c1610_void_fl               IS NULL;
			   -- to map the part DI Number value based on the agency
			   gm_pkg_pd_sav_udi.gm_sav_di_part_map(p_num_id,v_issue_agency_id,p_user_id); 
		END IF; -- v_di_number 
		
		 -- below added instead of using trigger trg_t205d_part_attribute ot send update to iPad
			SELECT COUNT(t205.c205_part_number_id) INTO v_cnt
			  FROM t205d_part_attribute t205d,
			       t205_part_number t205,
			       t2023_part_company_mapping t2023
			 WHERE c901_attribute_type = '4000539'
		       AND c205d_attribute_value <> v_attr_value
			   AND t205.c205_part_number_id  = t205d.c205_part_number_id
			   AND t205.c205_part_number_id  = p_num_id
			   AND T205.C205_PRODUCT_FAMILY IN (SELECT C906_RULE_VALUE
										   FROM T906_RULES
										  WHERE C906_RULE_ID     = 'PART_PROD_FAMILY'
											AND C906_RULE_GRP_ID = 'PROD_CAT_SYNC'
											AND c906_void_fl    IS NULL)
			  AND T205.c205_active_fl      IS NOT NULL
			  AND t205.c205_part_number_id IN
			  (SELECT c205_part_number_id
	 		     FROM T205D_PART_ATTRIBUTE
			    WHERE C901_ATTRIBUTE_TYPE='80180'
			      AND c205_part_number_id  = p_num_id
			      AND C205D_VOID_FL       IS NULL
			  )
			 AND t205.c205_part_number_id = t2023.c205_part_number_id
			 AND t2023.c1900_company_id   = 1000
			 AND t2023.c2023_void_fl     IS NULL
			 AND c205d_void_fl           IS NULL;
					
			IF v_cnt > 0  THEN
		 			v_system_id := get_set_id_from_part(p_num_id);
			        gm_pkg_pdpc_prodcattxn.gm_sav_master_data_upd(p_num_id
								     , '4000411', '103123' 
								     ,'103097','103100'
								     , p_user_id
								     ,v_system_id
								     ,'Forceupdate'
								     );
			END IF;
		
        END IF; -- v_attr_type
        gm_pkg_pd_partnumber.gm_sav_part_attribute (p_num_id, v_attr_type, v_attr_value, p_user_id, v_attr_id);
        -- 4000540 Bulk Packaging
        -- 20375 Yes
        IF(v_attr_type ='4000540')
        THEN
        	gm_pkg_pd_sav_udi.gm_sav_sub_com_bulk_parameter (p_num_id, p_user_id);
        END IF;
      END LOOP;
      --
      gm_pkg_pd_sav_udi.gm_upd_product_identifiers(p_num_id, p_user_id);
      --
    END IF;
    -- to update the UDI parameter values
	gm_pkg_pd_sav_udi.gm_sav_qa_udi_param (p_num_id, p_user_id);
    END  gm_sav_part_dhr_req;
    
/********************************************************
 * Description : This function is used to get Part size.
 * Parameters  : p_partnumber, p_controlnum
********************************************************/
    Function get_part_size(
    p_partnumber	t205_part_number.C205_PART_NUMBER_ID%TYPE ,
    p_controlnum    t502_item_order.C502_Control_Number %TYPE
    )RETURN VARCHAR2
    IS
	v_size		   VARCHAR2(255);
	BEGIN
	
		BEGIN
	     SELECT  t2550.c2550_custom_size
         INTO    v_size
         FROM    t2550_part_control_number t2550
         WHERE   t2550.c205_part_number_id = p_partnumber --p_sys_id
         AND     t2550.c2550_control_number  = p_controlnum ;
        EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			v_size := '';
         END;
         
      SELECT DECODE(v_size,'',T205M.C205M_SIZE,v_size)    /*Get part size from new label param in PMT-34056 */
        INTO v_size
        FROM T205M_PART_LABEL_PARAMETER T205M
       WHERE T205M.C205_PART_NUMBER_ID = p_partnumber
         AND T205M.C2550_VOID_FL IS NULL;

         RETURN v_size;
		
	END get_part_size;
	/********************************************************
	* Description : This function is used to get part DHR configure id.
	* Parameters  : p_partnumber
	********************************************************/
	FUNCTION get_dhr_issue_agn_conf_id (
	        p_partnumber t205_part_number.C205_PART_NUMBER_ID%TYPE)
	    RETURN VARCHAR2
	AS
	    v_issue_agn_conf_id t1610_issue_agency_config.c1610_issue_agency_config_id%TYPE;
	    v_di_number t2060_di_part_mapping.c2060_di_number%TYPE;
	    v_product_class t205_part_number.c205_product_class%TYPE;
	    v_rule_value t906_rules.c906_rule_value%TYPE;
	    v_rule_id t906_rules.c906_rule_id%TYPE;
	    --
	    v_return_str VARCHAR2 (2000) ;
	    v_vendor_cnt NUMBER;
	BEGIN
	    -- to check the part is sterile and DI number
	     SELECT c205_product_class, DECODE (c205_product_class, '4030', 'STERILE_PART', 'NON_STERILE_PART'),
	        gm_pkg_pd_rpt_udi.get_part_di_number (c205_part_number_id)
	       INTO v_product_class, v_rule_id, v_di_number
	       FROM t205_part_number
	      WHERE c205_part_number_id = p_partnumber;
	    --
	    IF (v_di_number   IS NULL) THEN
	        v_rule_value := get_rule_value (v_rule_id, 'DHR_ISSUE_CONFIG_NM') ;
	        -- to get the issue configure id (t1610).
	        BEGIN
	             SELECT c1610_issue_agency_config_id
	               INTO v_issue_agn_conf_id
	               FROM t1610_issue_agency_config
	              WHERE c1610_issue_agency_config_nm = v_rule_value
	                AND c1610_void_fl               IS NULL;
	        EXCEPTION
	        WHEN OTHERS THEN
	            v_issue_agn_conf_id := NULL;
	        END;
	    END IF;
	    --
	     SELECT COUNT (1) INTO v_vendor_cnt
		   FROM v205g_part_udi_parameter
		  WHERE c205_part_number_id = p_partnumber
		    AND c205d_vendor_design = '103420'; -- Yes
		 IF (v_vendor_cnt = 0)
		 THEN
		     SELECT DECODE (v_di_number, NULL, v_issue_agn_conf_id, c205d_issuing_agency_conf)
		     	INTO v_return_str
		       FROM v205g_part_udi_parameter
		      WHERE c205_part_number_id = p_partnumber;
		 END IF;     
	    --
	    RETURN v_return_str;  
	END get_dhr_issue_agn_conf_id;
/********************************************************
* Description : This function is used to get sample pattern for DHR tab.
* Parameters  : p_partnumber
********************************************************/
FUNCTION get_dhr_tab_sam_pattern (
        p_partnumber t205_part_number.C205_PART_NUMBER_ID%TYPE)
    RETURN VARCHAR2
AS
    v_iss_agency_id        VARCHAR2 (40) ;
    v_sample_pattern       VARCHAR2 (4000) ;
    v_final_sample_pattern VARCHAR2 (4000) ;
    v_di_number t2060_di_part_mapping.c2060_di_number%TYPE;
BEGIN
    --
    -- 4000539 (Issuing Agency Configuration)
    v_di_number     := gm_pkg_pd_rpt_udi.get_part_di_number (p_partnumber) ;
    v_iss_agency_id := TRIM (get_part_attribute_value (p_partnumber, 4000539)) ;
    --
    IF (v_iss_agency_id  IS NOT NULL AND v_di_number IS NOT NULL) THEN
    	--
        v_sample_pattern := gm_pkg_pd_rpt_udi.get_agn_config_sample_pattern (v_iss_agency_id) ;
        --
         SELECT REPLACE (v_sample_pattern, 'DI', v_di_number)
           INTO v_final_sample_pattern
           FROM dual;
    END IF;
    --
    RETURN v_final_sample_pattern;
END get_dhr_tab_sam_pattern;

/***********************************************************
	 * Description : Procedure to fetch PART attributes info
	************************************************************/
PROCEDURE gm_fch_partattr_prodcat (
		p_token_id     IN  t101a_user_token.c101a_token_id%TYPE
	  , p_language_id  IN  NUMBER
	  , p_page_no      IN  NUMBER
	  , p_uuid		   IN  t9150_device_info.c9150_device_uuid%TYPE
	  , p_rfs_id       IN  t205d_part_attribute.c205d_attribute_value%TYPE
	  ,	p_part_attrib_cur OUT TYPES.cursor_type
	)
	AS
	
	v_device_infoid		   t9150_device_info.c9150_device_info_id%TYPE;
	v_start     NUMBER;
    v_end       NUMBER;
    v_page_size NUMBER;
	v_page_no   NUMBER;
	v_update_count NUMBER := 0;
   	v_last_rec	t9151_device_sync_dtl.C9151_LAST_SYNC_REC%TYPE; 
  	v_company_id t1900_company.c1900_company_id%TYPE;
	BEGIN
	   --get the device info id based on user token
	   v_device_infoid := get_device_id(p_token_id, p_uuid) ;
		
	   v_page_no := p_page_no;
	   
		SELECT NVL (get_compid_frm_cntx (), get_rule_value ('DEFAULT_COMPANY', 'ONEPORTAL'))
	     	INTO v_company_id
	      	FROM dual;
	   -- get the SET paging details defined in rules. 
	   gm_pkg_pdpc_prodcatrpt.gm_fch_paging_dtl('PARTATTR','PAGESIZE',v_page_no,v_start,v_end,v_page_size);
	 
      /*below prc check the device's last updated date for SYSTEM ATTRIB's reftype and
		if record exist IN T9151_DEVICE_SYNC_DTL, then it checks is there any PART ATTRIB's updated after the device updated date IN T2001_MASTER_DATA_UPDATES.
		if updated PART ATTRIB's are available then add all PART ATTRIB's into v_inlist and this prc returns count of v_inlist records*/
	   gm_pkg_pdpc_prodcatrpt.gm_fch_device_updates(v_device_infoid,p_language_id,4000827,v_page_no,v_update_count,v_last_rec);--4000736 Reference Type
	   
	   --If there is no update data available for device then call below prc to add all PARTS.		
	   IF v_update_count = 0 THEN
			gm_pkg_pdpc_prodcatrpt.gm_fch_all_parts_tosync;--This is used to fetch the all parts which are available in loaner pool.		
	   END IF;  

	   OPEN p_part_attrib_cur
	   FOR	
	  
		 SELECT RESULT_COUNT totalsize,  COUNT (1) OVER () pagesize, v_page_no pageno, rwnum, CEIL (RESULT_COUNT / v_page_size) totalpages, T205D.*  
		   FROM
		    (
		         SELECT T205D.*, ROWNUM rwnum, COUNT (1) OVER () result_count, MAX(DECODE(t205d.partattrid,v_last_rec,ROWNUM,NULL)) OVER() LASTREC
		           FROM
		            (                
			             SELECT t205d.c2051_part_attribute_id partattrid, t205d.c205_part_number_id pnum
			             		, t205d.c205d_attribute_value partattrval, t205d.c205d_void_fl partattrvoidfl
                                , T205D.c901_attribute_type partattrtype
			              FROM my_temp_prod_cat_info temp,t205d_part_attribute T205D  
			              WHERE T205D.c901_attribute_type IN (SELECT C906_RULE_VALUE FROM T906_RULES WHERE C906_RULE_ID='PARTATTRTYPE' AND C906_RULE_GRP_ID='PARTATTR' AND C906_VOID_FL IS NULL)
			              AND T205d.c205_part_number_id = temp.ref_id
		                  AND T205D.c205d_attribute_value in( SELECT TO_CHAR(c901_rfs_id) 
                                                            FROM ( SELECT c901_rfs_id
                                                                        , c1900_company_id 
                                                                     FROM t901c_rfs_company_mapping t901c 
                                                                    UNION
                                                                   SELECT 80120 c901_rfs_id 
                                                                        , 1000 c1900_company_id
                                                                     FROM dual  
                                                                 )  t901c
                                                          WHERE t901c.c1900_company_id = v_company_id )
		                  AND ( v_update_count>0 OR T205D.c205d_VOID_FL IS NULL)
		                  ORDER BY t205d.c2051_part_attribute_id
			         )
		            T205D 
		    )
		    T205D
		  WHERE rwnum  BETWEEN NVL(LASTREC+1,DECODE(v_last_rec,NULL,v_start,v_start-1)) AND v_end;


  
	END gm_fch_partattr_prodcat;
	/************************************************************************
	* Description : Procedure to save attribute value to T205D_PART_ATTRIBUTE based on company
	* Author  :
	*******************************************************************/
	PROCEDURE gm_sav_part_attribute_by_comp (
	        p_num_id     IN t205_part_number.c205_part_number_id%TYPE,
	        p_attr_type  IN t205d_part_attribute.c901_attribute_type%TYPE,
	        p_attr_value IN t205d_part_attribute.c205d_attribute_value%TYPE,
	        p_user_id    IN t205_part_number.c205_last_updated_by%TYPE,
	        p_attr_id OUT t205d_part_attribute.c2051_part_attribute_id%TYPE)
	AS
	    v_partnumber t205_part_number.c205_part_number_id%TYPE;
	    v_company_id t1900_company.c1900_company_id%TYPE;
	BEGIN
	    -- Getting the company
	     SELECT NVL (get_compid_frm_cntx (), get_rule_value ('DEFAULT_COMPANY', 'ONEPORTAL'))
	       INTO v_company_id
	       FROM dual;
	    

	       gm_sav_part_attribute_by_comp(p_num_id,p_attr_type,p_attr_value,p_user_id,v_company_id,p_attr_id);
END gm_sav_part_attribute_by_comp;
	 /************************************************************************
		  * Description : Procedure to save attribute value to T205D_PART_ATTRIBUTE
		  *******************************************************************/
	PROCEDURE gm_sav_part_attribute_by_comp(
        p_num_id     IN t205_part_number.c205_part_number_id%TYPE,
        p_attr_type  IN t205d_part_attribute.c901_attribute_type%TYPE,
        p_attr_value IN t205d_part_attribute.c205d_attribute_value%TYPE,
        p_user_id    IN t205d_part_attribute.c205d_last_updated_by%TYPE,
        p_comp_id    IN t205d_part_attribute.c1900_company_id%TYPE,
        p_attr_id OUT t205d_part_attribute.c2051_part_attribute_id%TYPE)
	AS
	BEGIN

	         UPDATE t205d_part_attribute
	            SET c205d_attribute_value = p_attr_value, 
	            c205d_update_fl = null,--For Part Updates
	                c205d_last_updated_date = CURRENT_DATE,
	               c205d_last_updated_by = p_user_id,
	               c1900_company_id = p_comp_id
	          WHERE c205_part_number_id            = p_num_id
	            AND c901_attribute_type            = p_attr_type
	            AND NVL (c1900_company_id, - 9999) = NVL (p_comp_id, - 9999)
	            AND c205d_void_fl                 IS NULL;
	    
	    IF (SQL%ROWCOUNT = 0) THEN
	         SELECT s205d_part_attribute.NEXTVAL INTO p_attr_id FROM DUAL;
	         INSERT
	           INTO t205d_part_attribute
	            (
	                c2051_part_attribute_id, c205_part_number_id, c901_attribute_type
	              , c205d_attribute_value, c205d_created_date, c205d_created_by
	              , c205d_last_updated_date, c205d_last_updated_by, c1900_company_id
	            )
	            VALUES
	            (
	                p_attr_id, p_num_id, p_attr_type
	              , p_attr_value, CURRENT_DATE, p_user_id
	              , CURRENT_DATE, p_user_id, p_comp_id
	            );
	    END IF;
	END gm_sav_part_attribute_by_comp; 

  /************************************************************************
	  * Description : Procedure to save attribute value to T205D_PART_ATTRIBUTE in bulk
	  *******************************************************************/
	 PROCEDURE gm_sav_part_attribute_bulk (
     --   p_attr_id    IN t205d_part_attribute.c2051_part_attribute_id%TYPE,
        p_attr_type  IN t205d_part_attribute.c901_attribute_type%TYPE,
        p_attr_str   IN CLOB,
        p_all_cmp_fl IN VARCHAR2,
        p_user_id    IN t205d_part_attribute.c205d_last_updated_by%TYPE,
        p_validParts   OUT CLOB,
        p_invalidParts OUT CLOB)
	AS
	    v_strlen NUMBER := NVL (LENGTH (p_attr_str), 0);
	    v_string CLOB   := p_attr_str;
	    v_pnum t205d_part_attribute.c205_part_number_id%TYPE;
	    v_attr_value t205d_part_attribute.c205d_attribute_value%TYPE;
	    v_substring VARCHAR2 (1000) ;
	    v_company_id t1900_company.c1900_company_id%TYPE;
	    v_count NUMBER;
	    v_rfs_cnt NUMBER;
	    v_invalid_partstr CLOB;
	    v_valid_partstr  CLOB;
	    v_attr_id t205d_part_attribute.c2051_part_attribute_id%TYPE;
	BEGIN
	     SELECT DECODE (p_all_cmp_fl, 'Y', NULL, get_compid_frm_cntx ())
	       INTO v_company_id
	       FROM DUAL;
	    
	    	     
	     --pnum,value|pnum,value|
	    WHILE INSTR (v_string, '|') <> 0
	    LOOP
	        v_substring  := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
	        v_string     := SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
	        v_pnum       := NULL;
	        v_attr_value := NULL;
	        v_pnum       := TRIM (SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1)) ;
	        v_substring  := SUBSTR (v_substring, INSTR (v_substring, ',')          + 1) ;
	        v_attr_value := v_substring;
	        -- We can allow the part, If the part is RFS for that company only.
	       
	         SELECT COUNT (1)
	           INTO v_count
	           FROM t205_part_number t205
	          WHERE t205.c205_part_number_id = v_pnum;
	        
	          IF v_company_id IS NOT NULL THEN
			          -- We can allow the part, If the part is RFS for that company only.
					SELECT count(1)
					  INTO v_rfs_cnt
					  FROM t205_part_number t205, t2023_part_company_mapping t2023
					 WHERE t2023.c205_part_number_id = v_pnum
					   AND t205.c205_part_number_id = t2023.c205_part_number_id
					   AND t2023.c1900_company_id = v_company_id
					   AND t2023.c2023_void_fl IS NULL;
			  ELSE 
			    v_rfs_cnt :=1;
	          END IF;
	          
	        IF v_count = 0 OR v_rfs_cnt = 0 THEN
	            v_invalid_partstr           := v_invalid_partstr ||v_pnum ||', '; -- separating invalid parts
	        ELSE
	            gm_sav_part_attribute_by_comp(v_pnum, p_attr_type, v_attr_value, p_user_id, v_company_id,v_attr_id) ;
	            v_valid_partstr := v_valid_partstr||v_pnum ||',';
	        END IF;
	       END LOOP;
	    IF v_invalid_partstr  IS NOT NULL THEN
	        v_invalid_partstr := SUBSTR (v_invalid_partstr, 1, (LENGTH (v_invalid_partstr) - 2)) ;
	    END IF;

	   
	    p_validParts := v_valid_partstr;
	    p_invalidParts := v_invalid_partstr;

	    
	END gm_sav_part_attribute_bulk;
/************************************************************************
	  * Description : Procedure to fetch attribute value to T205D_PART_ATTRIBUTE
	  *******************************************************************/
	 PROCEDURE gm_fch_part_attribute(
        p_attr_type  IN t205d_part_attribute.c901_attribute_type%TYPE,
        p_pnums      IN CLOB,
        p_system_id  IN t207_set_master.c207_set_id%TYPE,
        p_all_cmp_fl IN VARCHAR2,
        p_partfl     IN VARCHAR2,
        p_exld_type  IN VARCHAR2,
        p_out   OUT	   TYPES.cursor_type)
	AS
			v_query VARCHAR2(4000);
			v_company_id t1900_company.c1900_company_id%TYPE;
	BEGIN
		 SELECT get_compid_frm_cntx ()
	       INTO v_company_id
	       FROM DUAL;
	       
	    my_context.set_my_inlist_ctx (p_exld_type);
	    
		v_query:= 'SELECT t205d.C2051_PART_ATTRIBUTE_ID ATTRIID,
			  T205.C205_PART_NUMBER_ID PARTNUM,
			  T205.C205_PART_NUM_DESC PARTDESC ,
			  T205D.C901_ATTRIBUTE_TYPE ATTRITYPE,
			  GET_CODE_NAME (T205D.C901_ATTRIBUTE_TYPE) ATTRITYPENM,
			  T205D.C205D_ATTRIBUTE_VALUE ATTRIVALUE,
			  GET_USER_NAME (t205d.C205D_LAST_UPDATED_BY) UPDATEDBY,
			  t205d.C205D_LAST_UPDATED_DATE UPDATEDATE,
			  t205d.C1900_COMPANY_ID companyid,sysdata.systemnm
			FROM T205_PART_NUMBER T205,
			  T205D_PART_ATTRIBUTE T205D,(SELECT DISTINCT t207.c207_set_id setid,t207.c207_set_nm systemnm, 
				c205_part_number_id pnum FROM t207_set_master t207, t208_set_details t208 
				WHERE t207.c901_set_grp_type = 1600 AND T208.C207_SET_ID = T207.C207_SET_ID';
			IF NVL(p_system_id,'0') <>'0' THEN
		
				v_query:= v_query ||' AND t207.c207_set_id ='''|| p_system_id||'''';
			END IF;  
			  	v_query:= v_query ||' AND t207.c207_void_fl IS NULL) sysdata';

		    IF p_pnums IS NOT NULL or p_partfl='Y' THEN 
			    my_context.set_my_cloblist(p_pnums||',');
				v_query:= v_query ||' , v_clob_list' ;
				v_query:= v_query ||' WHERE T205.C205_PART_NUMBER_ID = T205D.C205_PART_NUMBER_ID
				                      AND T205.C205_PART_NUMBER_ID = TOKEN';
			ELSE
				v_query:= v_query ||' WHERE T205.C205_PART_NUMBER_ID = T205D.C205_PART_NUMBER_ID';
			END IF;
			
			
			v_query:= v_query ||' AND T205D.C205D_VOID_FL  IS NULL AND t205.c205_part_number_id = sysdata.pnum';
			
			v_query:= v_query ||' AND T205D.C901_ATTRIBUTE_TYPE IN (SELECT C902_CODE_NM_ALT FROM T901_CODE_LOOKUP WHERE C901_CODE_GRP IN (''PTATTR'') AND C901_ACTIVE_FL =1';
					
			v_query:= v_query || ' AND C901_CODE_ID in (SELECT token FROM v_in_list))';
			
			IF NVL(p_attr_type,'0') <>'0' THEN
			v_query:= v_query ||' AND T205D.C901_ATTRIBUTE_TYPE  = '|| p_attr_type;
			END IF;
			
			IF lower(p_all_cmp_fl) = 'on' THEN
				v_query:= v_query ||' AND t205d.C1900_COMPANY_ID IS NULL';
			ELSE
				v_query:= v_query ||' AND t205d.C1900_COMPANY_ID ='||v_company_id;
			END IF;
						
			v_query:= v_query ||' ORDER BY PARTNUM';
			
		OPEN p_out FOR	v_query;
	END gm_fch_part_attribute;
/************************************************************************
	  * Description : Procedure to delete invalid part attribute in T205D_PART_ATTRIBUTE
	  *******************************************************************/	
	PROCEDURE gm_sav_void_part_attribute(
             p_txn_str  IN VARCHAR2,
             p_user_id  IN t205d_part_attribute.c205d_last_updated_by%TYPE)
       AS
             v_string    VARCHAR2(3000);
             v_attr_type t205d_part_attribute.c901_attribute_type%TYPE;
             v_all_cmp_fl T906_RULES.C906_RULE_VALUE%TYPE;
             v_comp_id  t1900_company.c1900_company_id%TYPE;
       BEGIN
         
       IF INSTR (p_txn_str, '|') <> 0 THEN
           v_string  := SUBSTR (p_txn_str, 1, INSTR (p_txn_str, '|') - 1) ;
           v_attr_type    := SUBSTR (p_txn_str, INSTR (p_txn_str, '|')    + 1) ;
       
           v_all_cmp_fl :=  GET_RULE_VALUE(v_attr_type,'CMPY_PART_ATTR');
           
            SELECT DECODE (v_all_cmp_fl, 'Y', NULL, get_compid_frm_cntx ())
              INTO v_comp_id
              FROM DUAL;
              
    my_context.set_my_inlist_ctx (v_string);    
          
         DELETE FROM t205d_part_attribute
     WHERE c205_part_number_id           IN (SELECT token FROM v_in_list  WHERE token IS NOT NULL)
                   AND c901_attribute_type            = v_attr_type
                   AND NVL (c1900_company_id, - 9999) = NVL (v_comp_id, - 9999)
                   AND c205d_void_fl                 IS NULL;
        
       END IF;
             

	 END gm_sav_void_part_attribute;
	 
	 /*********************************************************************************************************************
	* Description 	: Procedure to check Whether the part number is used in any of the transaction or not
	* 					If it is used, then the lot tracking needed and serial number needed check box should be disabled
	* Author		: Arajan
	************************************************************************************************************************/
	PROCEDURE gm_fch_checkbox_status (
		p_part_number 		IN 		t205_part_number.c205_part_number_id%TYPE,
		p_serial_chk_fl 	OUT 	VARCHAR2,
		p_lottrack_chk_fl 	OUT 	VARCHAR2
	)
	AS
		v_srl_num_value		t205d_part_attribute.c205d_attribute_value%TYPE;
		v_lot_req_value		t205d_part_attribute.c205d_attribute_value%TYPE;
		v_cnt				NUMBER := 0;
		v_company_id  		t1900_company.c1900_company_id%TYPE;
		v_lock_fl			VARCHAR2(2) := 'N';
		p_serial_lck_fl		t205d_part_attribute.c205d_attribute_value%TYPE;
		p_lottrack_lck_fl	t205d_part_attribute.c205d_attribute_value%TYPE;
	BEGIN
		
		SELECT NVL(get_compid_frm_cntx (),GET_RULE_VALUE ('DEFAULT_COMPANY', 'ONEPORTAL'))
	       INTO v_company_id
	       FROM DUAL;
		
		p_serial_chk_fl := 'Y';
		p_lottrack_chk_fl := 'Y';
		
		-- Get the attribute value of the Serial Num Needed flag; 106040: Serial Num Needed 
		v_srl_num_value := NVL(get_part_attr_value_by_comp(p_part_number, '106040', v_company_id),'N');
		
		-- Get the attribute value of the Lot Tracking Required flag; 104480: Lot Tracking Required 
		v_lot_req_value := NVL(get_part_attr_value_by_comp(p_part_number, '104480', v_company_id),'N');

		/* Get the Lot tracking lock flag from attribute table; 106041: Lot Tracking Locked
		 * If the check box of Lot Tracking required flag is checked and any transaction happened for part number, then the value will be 'Y'
		 */
		p_lottrack_lck_fl := get_part_attr_value_by_comp(p_part_number, '106041', v_company_id);
		
		/* Get the Serial Number Locked flag from attribute table; 106042: Serial Number Locked
		 * If the check box of Serial Number required flag is checked and any transaction happened for part number, then the value will be 'Y'
		 */
		p_serial_lck_fl := get_part_attr_value_by_comp(p_part_number, '106042', v_company_id);
		
		IF p_lottrack_lck_fl IS NULL AND p_serial_lck_fl IS NULL
		THEN
			-- Get the count from workorder table for the part number
			SELECT COUNT(1) INTO v_cnt
				FROM t402_work_order
			WHERE c205_part_number_id = p_part_number
				AND c402_void_fl         IS NULL
				AND c1900_company_id = v_company_id;
		END IF;
		
		-- If the serial number or lot tracking is already locked or any transaction is there, then set v_lock_fl = 'Y'
		IF (v_cnt > 0 OR p_serial_lck_fl = 'Y' OR p_lottrack_lck_fl = 'Y')
		THEN
			v_lock_fl := 'Y';
		END IF;
		
		-- If the count is > 0 and the attribute value is 'Y', then set 'N' for the out parameter
		IF v_lock_fl = 'Y' AND v_srl_num_value = 'Y'
		THEN
			p_serial_chk_fl := 'N';
		END IF;
		IF v_lock_fl = 'Y' AND v_lot_req_value = 'Y'
		THEN
			p_lottrack_chk_fl := 'N';
		END IF;
		
	END gm_fch_checkbox_status;
	
	/*************************************************************************************************************
	* Description : Procedure to fetch the part rfs company mapping details from T901C_RFS_COMPANY_MAPPING table
	* Author	  : ksomanathan				
	***************************************************************************************************************/
	PROCEDURE gm_fch_partrfs_mapping (
		p_token_id     IN  t101a_user_token.c101a_token_id%TYPE
	  , p_language_id  IN  NUMBER
	  , p_page_no      IN  NUMBER
	  , p_uuid		   IN  t9150_device_info.c9150_device_uuid%TYPE
	  ,p_part_rfs_cur  OUT		TYPES.CURSOR_TYPE
	)
	AS
	
	v_device_infoid		   t9150_device_info.c9150_device_info_id%TYPE;
	v_start     NUMBER;
    v_end       NUMBER;
    v_page_size NUMBER;
	v_page_no   NUMBER;
	v_update_count NUMBER := 0;
   	v_last_rec	t9151_device_sync_dtl.C9151_LAST_SYNC_REC%TYPE; 
	
   	BEGIN
	   --get the device info id based on user token
	   v_device_infoid := get_device_id(p_token_id, p_uuid);
		
	   v_page_no := p_page_no;
	  
	   -- get the SET paging details defined in rules. 
	   gm_pkg_pdpc_prodcatrpt.gm_fch_paging_dtl('RFS_PART_MAP','PAGESIZE',v_page_no,v_start,v_end,v_page_size);
	 
      /*below prc check the device's last updated date for SYSTEM ATTRIB's reftype and
		if record exist IN T9151_DEVICE_SYNC_DTL, then it checks is there any PART RFS updated after the device updated date IN T2001_MASTER_DATA_UPDATES.
		if updated PART RFS are available then add all PART ATTRIB's into v_inlist and this prc returns count of v_inlist records*/
	   gm_pkg_pdpc_prodcatrpt.gm_fch_device_updates(v_device_infoid,p_language_id,26230803,v_page_no,v_update_count,v_last_rec);--26230803 - Reference Type for PART RFS
	
	  --If there is no update data available for device then call below prc to add all PARTS.		
	   IF v_update_count = 0
	   THEN
			gm_pkg_pdpc_prodcatrpt.gm_fch_all_rfs_parts_tosync;--This is used to fetch the all RFS ID.		
	   END IF;   
	
	    OPEN p_part_rfs_cur
	   FOR	
	  
		 SELECT RESULT_COUNT totalsize,  COUNT (1) OVER () pagesize, v_page_no pageno, rwnum, CEIL (RESULT_COUNT / v_page_size) totalpages, T901C.*  
		   FROM
		    (
		         SELECT T901C.*, ROWNUM rwnum, COUNT (1) OVER () result_count, MAX(DECODE(T901C.RFSMAPID,v_last_rec,ROWNUM,NULL)) OVER() LASTREC
		           FROM
		            (                
			              SELECT T901C.C901C_RFS_COMP_MAP_ID RFSMAPID ,T901C.C901_RFS_ID RFSID , T901C.C1900_COMPANY_ID CMPID ,C901C_VOID_FL VOIDFL 
							FROM my_temp_prod_cat_info temp ,T901C_RFS_COMPANY_MAPPING T901C
			              WHERE T901C.C901C_RFS_COMP_MAP_ID = temp.ref_id		                
		                  AND ( v_update_count>0 OR T901C.C901C_VOID_FL IS NULL)
		                  UNION 
 							SELECT (MAX(C901C_RFS_COMP_MAP_ID) + 1) C901C_RFS_COMP_MAP_ID
     					 , 80120 RFSID
     					 , 1000 CMPID
     					 , 'N' VOIDFL 
   							FROM T901C_RFS_COMPANY_MAPPING  T901C
							ORDER BY RFSMAPID
			         )
		            T901C 

		    )
		    T901C
		  WHERE rwnum  BETWEEN NVL(LASTREC+1,DECODE(v_last_rec,NULL,v_start,v_start-1)) AND v_end;
		  
	END gm_fch_partrfs_mapping;

	/****************************************************************
    * Description : Procedure to validate the Part number 
    * Author      : Prabhu Vigneshwaran
    *****************************************************************/
PROCEDURE gm_validate_mfg_parts(
        p_partinputstr                      IN  CLOB,
        p_out_invalid_part                  OUT CLOB,
        p_out_invalid_part_cmp  OUT CLOB
       
      )
AS
     v_invalid_part CLOB;
     v_invalid_part_cmp CLOB;
     v_company_id   T2023_PART_COMPANY_MAPPING.c1900_company_id%TYPE;
    
  BEGIN
	  SELECT get_compid_frm_cntx () INTO v_company_id FROM DUAL; 
      my_context.set_my_cloblist (p_partinputstr);
       SELECT RTRIM (XMLAGG (XMLELEMENT (e, token || ',')) .EXTRACT ('//text()'), ',')
       INTO v_invalid_part
       FROM v_clob_list
       WHERE TO_CHAR (token) NOT IN
        (
             SELECT C205_PART_NUMBER_ID 
               FROM T205_PART_NUMBER
        ) ;
       SELECT RTRIM (XMLAGG (XMLELEMENT (e, token || ',')) .EXTRACT ('//text()'), ',')
         INTO v_invalid_part_cmp
          FROM v_clob_list
           WHERE  TO_CHAR (token) NOT IN 
           (
            SELECT C205_PART_NUMBER_ID 
             FROM T2023_PART_COMPANY_MAPPING T2023
              WHERE T2023.C2023_VOID_FL IS NULL 
	            AND T2023.C1900_COMPANY_ID = v_company_id
	          );
        
  IF v_invalid_part IS NOT NULL THEN
       p_out_invalid_part       := v_invalid_part ;
  END IF;
  IF v_invalid_part_cmp IS NOT NULL THEN
        p_out_invalid_part_cmp       := v_invalid_part_cmp;
  END IF;
END gm_validate_mfg_parts;
 
END gm_pkg_pd_partnumber;
/