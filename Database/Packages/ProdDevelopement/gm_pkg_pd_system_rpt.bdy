/* Formatted on 2009/03/10 14:31 (Formatter Plus v4.8.0) */
--@"C:\database\Packages\ProdDevelopement\gm_pkg_pd_system_rpt.bdy"

	CREATE OR REPLACE PACKAGE BODY gm_pkg_pd_system_rpt
	IS		
	/*************************************************************************
	* Description 	: Procedure to fetch Available Group Details for a System
	* Author		: HReddi
	**************************************************************************/
	PROCEDURE gm_fch_sys_groups(
		  p_set_id			IN		t207_set_master.c207_set_sales_system_id%TYPE
		, p_cur_grp_dtls	OUT		TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_cur_grp_dtls FOR
		    SELECT t4010.C4010_group_id group_id,get_code_name(t4010.c901_group_type) grouptp
        		 , t4010.c4010_group_nm groupnm 
        		 ,  NVL(t7010.c7010_price,0) listp
             FROM t4010_group t4010 ,t7010_group_price_detail t7010
            WHERE t4010.C4010_group_id  = t7010.C4010_GROUP_ID(+) 
              AND t4010.C207_set_id     = p_set_id -- System ID
              AND t7010.c901_price_type(+) = 52060 -- List Price
              AND t4010.c4010_void_fl   IS NULL
              AND t7010.c7010_void_fl   IS NULL
         ORDER BY UPPER(groupnm),grouptp;
	END gm_fch_sys_groups ;

	/************************************************************************
	* Description 	: Procedure to fetch Available Set Details for a System
	* Author		: HReddi
	*************************************************************************/
	PROCEDURE gm_fch_sys_sets(
		  p_set_id			IN		t207_set_master.c207_set_sales_system_id%TYPE
		, p_cur_set_dtls	OUT		TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_cur_set_dtls FOR
		   SELECT t207.c207_set_id setid
        		, t207.c207_set_nm setnm
        		, get_code_name(c207_category) catnm
        		, get_code_name(c207_type) settpnm
        		, get_code_name(t207.c901_hierarchy) hierarchy
        		--If set type is consignment and c901_hierarchy value is Main Set then it is a standard Set
	        	--If set type is consignment and Additional Inventory Set value is Main Set then it is a Additional Set
	       		--If set type is Loaner and c901_hierarchy value is Main Set then it is a Loaner standard Set
	        	--If set type is Loaner and c901_hierarchy value is Additional Inventory Set then it is a Loaner Additional Set
       			, get_code_name (DECODE (t207.c207_type,4074, DECODE (t207.c901_hierarchy, 20700, '103148', '103150'),  DECODE (t207.c901_hierarchy, 20700, '103147', '103149'))) settypenm  
    		FROM t207_set_master t207,t207a_set_link t207a
    	   WHERE t207.c207_set_sales_system_id = p_set_id
    		 AND t207.c207_set_id  =   t207a.C207_link_set_id
    		 AND t207.c207_void_fl IS NULL
    	ORDER BY t207.c207_set_id,t207.c207_type;
	END gm_fch_sys_sets ;
	/**************************************************************
	* Description 	: Procedure to fetch set attribute values
	* Author		: Elango
	***************************************************************/
    PROCEDURE gm_fch_set_attribute (
            p_set_id    IN t207_set_master.c207_set_id%TYPE,
            p_attr_type IN t207c_set_attribute.c901_attribute_type%TYPE,
            p_out_set_attr OUT TYPES.cursor_type)
    AS
    BEGIN
        OPEN p_out_set_attr 
        FOR 
	        SELECT t207c.c207c_attribute_value attrval, t207c.c901_attribute_type ID
			   FROM t207c_set_attribute t207c, t207_set_master t207
			  WHERE t207.c207_set_id          = t207c.c207_set_id
			    AND t207c.c207_set_id         = p_set_id
			    AND t207c.c901_attribute_type = NVL(p_attr_type,t207c.c901_attribute_type)
			    AND t207c.c207c_void_fl       IS NULL
			    AND t207.c207_void_fl         IS NULL;
    END gm_fch_set_attribute;
 	/**************************************************************
	* Description 	: Procedure to fetch system mapping  details
	* Author		: Elango
	***************************************************************/
    PROCEDURE gm_fch_sys_mapping (
            p_set_id IN t207_set_master.c207_set_sales_system_id%TYPE,
            p_cur_sysid OUT TYPES.cursor_type)
    AS
    BEGIN
        OPEN p_cur_sysid 
        FOR  
	        SELECT t207.c207_set_id setid, t207.c207_set_nm setnm, t207.c202_project_id projectid
	      , t202.c202_project_nm  projectnm
	        --If set type is consignment and c901_hierarchy value is Main Set then it is a standard Set
	        --If set type is consignment and Additional Inventory Set value is Main Set then it is a Additional Set
	        --If set type is Loaner and c901_hierarchy value is Main Set then it is a Loaner standard Set
	        --If set type is Loaner and c901_hierarchy value is Additional Inventory Set then it is a Loaner Additional Set
	      , (DECODE (t207.c207_type,4074, DECODE (t207.c901_hierarchy, 20700, '103148', '103150'),  DECODE (t207.c901_hierarchy, 20700, '103147', '103149'))) settypeid
	      , get_code_name (DECODE (t207.c207_type,4074, DECODE (t207.c901_hierarchy, 20700, '103148', '103150'),  DECODE (t207.c901_hierarchy, 20700, '103147', '103149'))) settypenm
	      , t207.c901_cons_rpt_id baselineid
	    --, get_code_name (t207.c901_cons_rpt_id) baselinenm
             ,DECODE (t207.c901_cons_rpt_id, 20100,'Yes', 'No') baselinenm	      
	      , t207a.c901_shared_status sharedstatid
	      , get_code_name (t207a.c901_shared_status) sharedstatnm
	       FROM t207_set_master t207, t207a_set_link t207a, t202_project t202
	      WHERE t207.c207_set_sales_system_id = p_set_id
            AND t207.c202_project_id          = t202.c202_project_id
	        AND t207.c207_set_id              = t207a.c207_link_set_id
	        AND t207.c207_void_fl            IS NULL
	        AND t207.c901_set_grp_type        = 1601;
    END gm_fch_sys_mapping;
 	/*********************************************
	 * Description : Function to get set status
	 * Author	   : Elango
	 *********************************************/
    FUNCTION get_set_status (
            p_set_id  IN t207_set_master.c207_set_sales_system_id%TYPE,
            p_set_grp IN t207_set_master.c901_set_grp_type%TYPE)
        RETURN NUMBER
    IS
        v_set_statusid NUMBER;
    BEGIN
        --
        BEGIN
             SELECT c901_status_id
               INTO v_set_statusid
               FROM t207_set_master
              WHERE c207_void_fl     IS NULL
                AND c901_set_grp_type = p_set_grp
                AND c207_set_id       = p_set_id;
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_set_statusid := NULL;
        END;
        RETURN v_set_statusid;
    END get_set_status;
	/***************************************************************
	 * Description : This procedure checks availability of system.
     * Author      : Elango
	 ***************************************************************/
    FUNCTION gm_chk_system (
            p_system_name IN t207_set_master.c207_set_nm%type
    )
       RETURN VARCHAR2
    IS
        v_count NUMBER;
        v_message VARCHAR2(20);
    BEGIN
        BEGIN
             SELECT COUNT (1)
               INTO v_count
               FROM t207_set_master
              WHERE UPPER(TRIM(REGEXP_REPLACE (C207_SET_NM,'�|�|.?trade;|.?reg;|.?\(.*\)'))) LIKE UPPER(REGEXP_REPLACE (TRIM(p_system_name), '�|�|.?trade;|.?reg;|.?\(.*\)'))
                AND c901_set_grp_type = 1600
                AND c207_void_fl     IS NULL;
            IF (v_count               = 0) THEN
                v_message            := 'AVAILABLE';
            ELSE
                v_message := 'DUPLICATE';
            END IF;
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_message := 'AVAILABLE';
        END;
         RETURN v_message;
	END gm_chk_system;
	
	/*************************************************************
	* Description 	: Procedure to fetch system company mapping values
	* Author		: rdinesh
**************************************************************/
	PROCEDURE gm_fch_system_comp_map (
		  p_set_id			   IN		t207_set_master.c207_set_id%TYPE
		, p_sys_comp_map_cur   OUT		TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_sys_comp_map_cur
		 FOR
			 SELECT t1900.c1900_company_id comp_id, t1900.c1900_company_name comp_nm
			     ,get_code_name(t2080.c901_set_mapping_type) status
			     ,get_user_name(t2080.c2080_last_updated_by) Released_by
                 ,t2080.c2080_last_updated_date released_dt
			 FROM t2080_set_company_mapping t2080, t1900_company t1900
			WHERE t2080.c207_set_id=p_set_id
			  AND t1900.c1900_company_id = t2080.c1900_company_id
              AND t1900.c1900_void_fl IS NULL
			  AND t2080.c2080_void_fl IS NULL
			  ORDER BY t2080.c2080_last_updated_date,t1900.c1900_company_name;
	END gm_fch_system_comp_map;
	/****************************************************************************
	* Description 	: This Function used to fetch System company mapping count
	* 				  whether system is linked to set or not in t207 and t207a table
	* Author		: prabhu vigneshwaran M D 
	********************************************************************************/
	 FUNCTION get_system_set_map_cnt (
         p_set_id			   IN		t207_set_master.c207_set_id%TYPE)
        RETURN NUMBER
        IS
         v_count NUMBER;
	BEGIN
           		SELECT count(1)
           			 INTO  v_count
					 FROM t207_set_master t207,
  				 		  t207a_set_link t207a
					WHERE t207.c207_set_sales_system_id = p_set_id
    		  		  AND t207.c207_set_id = t207a.c207_link_set_id
 			  		  AND t207.c901_set_grp_type = 1601
 			 		  AND t207.c207_void_fl IS NULL;
		 RETURN v_count;
	 END get_system_set_map_cnt;
END gm_pkg_pd_system_rpt;
/
