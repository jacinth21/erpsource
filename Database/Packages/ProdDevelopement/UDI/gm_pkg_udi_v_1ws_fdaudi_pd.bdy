-- @"C:\jbossbranch\Database\Packages\ProdDevelopement\UDI\gm_pkg_udi_v_1ws_fdaudi_pd.bdy"
CREATE OR REPLACE PACKAGE BODY gm_pkg_udi_v_1ws_fdaudi_pd
IS
v_error_action T9033_JOB_ERROR_EVENT.C901_STATUS%TYPE := 104900; -- open

/*
1. UDI015 - Both clinicalSizeType AND clinicalSizeValue are required if one is populated
104468,104472,104476,104464 Type1,2,3,4
104466,104474,104470,104478 Value1,2,3,4
104475,104467,104479,104471 UOM1,2,3,4
*/
PROCEDURE DE51_DE52_CLIN_TYPE_VALUE_REQ
AS
            CURSOR cur_clinical_type_v is
            SELECT REFID
            FROM 
            ( SELECT T9031.C9031_REF_ID REFID
            , SUM (case when T9031.C9031_ATTRIBUTE_TYPE in (104468,104472,104476,104464) AND (T9031.C9031_ATTRIBUTE_VALUE IS NOT NULL AND C9031_ATTRIBUTE_VALUE<>'104047')  then 1 else 0 end) DE51_DEVICE_TYPE
            , SUM (case when T9031.C9031_ATTRIBUTE_TYPE IN (104466,104474,104470,104478) AND T9031.C9031_ATTRIBUTE_VALUE IS NOT NULL  then 1 else 0 end) DE52_DEVICE_VALUE
            , SUM (case when T9031.C9031_ATTRIBUTE_TYPE IN (104475,104467,104479,104471) AND T9031.C9031_ATTRIBUTE_VALUE IS NOT NULL then 1 else 0 end) DE53_DEVICE_UOM
            from T9031_JOB_SOURCE_SYSTEM  T9031
            where C9030_JOB_ACTIVITY_ID = v_job_activity_id 
            and C9031_ATTRIBUTE_TYPE in (104468,104472,104476,104464,104466,104474,104470,104478,104478,104475,104467,104479,104471)
            AND C9031_ATTRIBUTE_VALUE IS NOT NULL
            group by T9031.C9031_REF_ID
            )
            WHERE DE51_DEVICE_TYPE <> DE52_DEVICE_VALUE OR DE51_DEVICE_TYPE<>DE53_DEVICE_UOM;

BEGIN

DBMS_OUTPUT.put_line (' v_job_activity_id in gm_pkg_udi_v_1ws_fdaudi_pd '||v_job_activity_id);

FOR clinical_type_v IN cur_clinical_type_v
LOOP

gm_pkg_cm_job_system_trans.gm_sav_job_error_event(sysdate,13,v_job_activity_id,NULL, clinical_type_v.REFID,v_error_action); 

END LOOP;

END DE51_DE52_CLIN_TYPE_VALUE_REQ;

/*
1. UDI027 - doesTradeItemContainLatex is required
If this is NULL, we wont even come to validation phase, since C2071_IS_ALL_DATA_AVAILABLE wont be set to Y
*/
--PROCEDURE DE46_CONTAINS_LATEX_REQ;

/*
1. UDI037 - Valid Sterilization method data from FDA list
This is pre-validated in the PD screen.
*/
--PROCEDURE DE62_STERILIZATION_TYPE_LIST;

/*
1. UDI052 - If clinicalSizeType = "Device Size Text, Please Specify", then only clinicalSizeText must be populated 
2. UDI053 - If clinicalSizeType != "Device Size Text, Please Specify", then clinicalSizeValue and ValueUOM must be populated 
*/
PROCEDURE DE54_CLINICAL_SIZE_TEXT_CONDN
AS
              CURSOR cur_clinical_size_text is
              SELECT REFID
              FROM 
              (
              SELECT T9031.C9031_REF_ID REFID
              , SUM (case when T9031.C9031_ATTRIBUTE_TYPE in (104468,104472,104476,104464) and T9031.C9031_ATTRIBUTE_VALUE = '104047' then 1 else 0 end) DE51_DEVICE_TYPE
              , SUM (case when T9031.C9031_ATTRIBUTE_TYPE IN (104469,104477,104473,104465) AND T9031.C9031_ATTRIBUTE_VALUE IS NOT NULL then 1 else 0 end) DE54_DEVICE_TYPE_TEXT
              from T9031_JOB_SOURCE_SYSTEM  T9031
              where C9030_JOB_ACTIVITY_ID = v_job_activity_id 
              and C9031_ATTRIBUTE_TYPE in (104468,104472,104476,104464, 104469,104477,104473,104465)
              --AND T9031.C9031_ATTRIBUTE_VALUE = '104047' -- Others
              group by T9031.C9031_REF_ID
              )
              where DE51_DEVICE_TYPE <> DE54_DEVICE_TYPE_TEXT;

/*
cursor cur_clinical_size_type is
select REFID
from 
(
select T9031.C9031_REF_ID REFID
, SUM (case when T9031.C9031_ATTRIBUTE_TYPE in (104468,104472,104476,104464) and T9031.C9031_ATTRIBUTE_VALUE != '104047' then 1 else 0 end) DE51_DEVICE_TYPE
, SUM (case when T9031.C9031_ATTRIBUTE_TYPE in (104466,104474,104470,104478) and T9031.C9031_ATTRIBUTE_VALUE is not null then 1 else 0 end) DE52_DEVICE_VALUE
, SUM (case when T9031.C9031_ATTRIBUTE_TYPE IN (104475,104467,104479,104471) AND T9031.C9031_ATTRIBUTE_VALUE IS NOT NULL then 1 else 0 end) DE53_DEVICE_UOM
from T9031_JOB_SOURCE_SYSTEM  T9031
where C9030_JOB_ACTIVITY_ID = v_job_activity_id 
and C9031_ATTRIBUTE_TYPE in (104468,104472,104476,104464, 104466,104474,104470,104478, 104475,104467,104479,104471)
--AND T9031.C9031_ATTRIBUTE_VALUE = '104047' -- Others
group by T9031.C9031_REF_ID
)
where ( DE51_DEVICE_TYPE > DE52_DEVICE_VALUE
or DE51_DEVICE_TYPE > DE53_DEVICE_UOM
);
*/

BEGIN

FOR clinical_size_text IN cur_clinical_size_text
LOOP

gm_pkg_cm_job_system_trans.gm_sav_job_error_event(sysdate,35,v_job_activity_id,NULL, clinical_size_text.REFID,v_error_action); 

END LOOP;
/*
FOR clinical_size_type IN cur_clinical_size_type
LOOP

gm_pkg_cm_job_system_trans.gm_sav_job_error_event(sysdate,36,v_job_activity_id,NULL, clinical_size_type.REFID,v_error_action);

END LOOP;
*/

END DE54_CLINICAL_SIZE_TEXT_CONDN;

PROCEDURE DE60_STERILIZATION_METHOD
    /*TO CHECK STERILIZATION METHOD HAS BLANK FOR STERILE PARTS*/
AS
    CURSOR cur_sterile_method
    IS
    SELECT ATTR.REFID
    FROM
    (SELECT REFID ,
        SUM (
        CASE WHEN C9031_ATTRIBUTE_TYPE=104462 AND C9031_ATTRIBUTE_VALUE IS NOT NULL THEN 1 ELSE 0 END) DE60_DEVICE_PACK_STERILE
    FROM
        (SELECT T9031.C9031_REF_ID REFID,
            T9031.C9031_ATTRIBUTE_TYPE ,
            T9031.C9031_ATTRIBUTE_VALUE
        FROM T9031_JOB_SOURCE_SYSTEM T9031
        WHERE C9030_JOB_ACTIVITY_ID = v_job_activity_id
            --AND C9031_ATTRIBUTE_TYPE         = '104462' -- Sterilization method
        )
    GROUP BY REFID
    ) ATTR,
    (SELECT T2060.C2060_DI_NUMBER,
        1 C205_PRODUCT_CLASS
    FROM T2060_DI_PART_MAPPING T2060,
        T205_PART_NUMBER T205
    WHERE T2060.C205_PART_NUMBER_ID=T205.C205_PART_NUMBER_ID
    AND T2060.C901_STATUS          ='103392' --IN USE
    AND C205_PRODUCT_CLASS         ='4030'
    ) STR_PART
WHERE ATTR.REFID          =STR_PART.C2060_DI_NUMBER
AND ATTR.DE60_DEVICE_PACK_STERILE<>STR_PART.C205_PRODUCT_CLASS;

BEGIN
    FOR sterile_method IN cur_sterile_method
    LOOP
        gm_pkg_cm_job_system_trans.gm_sav_job_error_event(sysdate,222,v_job_activity_id,NULL, sterile_method.REFID,v_error_action);
    END LOOP;
    
END DE60_STERILIZATION_METHOD;

END gm_pkg_udi_v_1ws_fdaudi_pd;
/
