-- @"C:\jbossbranch\Database\Packages\ProdDevelopement\UDI\gm_pkg_udi_v_1ws_fdaudi_reg.bdy"
CREATE OR REPLACE PACKAGE BODY gm_pkg_udi_v_1ws_fdaudi_reg
IS

v_error_action T9033_JOB_ERROR_EVENT.C901_STATUS%TYPE := 104900; -- open

/*
exec gm_pkg_udi_v_1ws_fdaudi_reg.validation_main;
*/
PROCEDURE validation_main
AS
BEGIN
gm_pkg_udi_v_1ws_fdaudi_reg.DE32_FDAPSN_CHECK;
gm_pkg_udi_v_1ws_fdaudi_reg.DE37_GMDN_REQ_5_DIGITS;

END validation_main;

/*
1. UDI002 - If alternateClassification/scheme = FDAPSN, alternateClassification/code must be between 6-8 characters if no semicolon
2. UDI006 - FDAPSN required unless exemptFromFDAPreMarketAuthorization = TRUE or if groupedProduct = KIT or KIT_AND_COMBINATION
3. UDI045 - If alternateClassification/scheme = FDAPSN, alternateClassification/code must be between 10-13 characters if it does contain a colon.
*/
PROCEDURE DE32_FDAPSN_CHECK
AS

v_fdapsn_len NUMBER;

cursor cur_di_fdapsn_check IS
select T9031.C9031_SOURCE_SYSTEM_KEY_ID  , T9031.C9031_REF_ID REFID , T9031.C9031_ATTRIBUTE_TYPE , T9031.C9031_ATTRIBUTE_VALUE FDAPSN
, LENGTH(T9031.C9031_ATTRIBUTE_VALUE) FDAPSN_LEN
from T9031_JOB_SOURCE_SYSTEM T9031, T906_RULES T906
where T906.C906_RULE_ID = T9031.C9031_ATTRIBUTE_TYPE
and T9031.C9030_JOB_ACTIVITY_ID = v_job_activity_id
and T906.C906_RULE_GRP_ID in ('DE32_FDA_SUB_NUM')
and T906.C906_VOID_FL is null;

BEGIN

FOR di_fdapsn_check IN cur_di_fdapsn_check
LOOP
v_fdapsn_len := di_fdapsn_check.FDAPSN_LEN;

IF v_fdapsn_len < 6 OR v_fdapsn_len > 8 
THEN
	gm_pkg_cm_job_system_trans.gm_sav_job_error_event(sysdate, 32,v_job_activity_id,NULL, di_fdapsn_check.REFID,v_error_action); 
END IF;


END LOOP;

END DE32_FDAPSN_CHECK;


/*
1. UDI003 - at least one instance of alternateClassification/scheme = GMDN should be present
2. UDI005 - alternateClassification/code must be numeric and 5 digits long
*/
PROCEDURE DE37_GMDN_REQ_5_DIGITS
AS

cursor cur_gmdn_verification IS
select T9031.C9031_REF_ID REFID, T9031.C9031_ATTRIBUTE_TYPE , T9031.C9031_ATTRIBUTE_VALUE
from T9031_JOB_SOURCE_SYSTEM T9031
where T9031.C9030_JOB_ACTIVITY_ID = v_job_activity_id
and T9031.C9031_ATTRIBUTE_TYPE = 104677 -- GMDN code
and (not regexp_like (T9031.C9031_ATTRIBUTE_VALUE,'^[[:digit:]]+$')
OR LENGTH(T9031.C9031_ATTRIBUTE_VALUE) != 5 );

BEGIN

FOR gmdn_verification IN cur_gmdn_verification
LOOP

gm_pkg_cm_job_system_trans.gm_sav_job_error_event(sysdate,4,v_job_activity_id,NULL, gmdn_verification.REFID,v_error_action); 

END LOOP;

END DE37_GMDN_REQ_5_DIGITS;

/*
1. UDI003 - If alternateClassification/scheme = ProdCodeClassDB, then alternateClassification/code must be 3 characters in length
*/
PROCEDURE DE34_PROD_CLASS_3_CHAR_LEN
AS
cursor cur_product_class_v is
select T9031.C9031_REF_ID REFID, T9031.C9031_ATTRIBUTE_TYPE , T9031.C9031_ATTRIBUTE_VALUE
from T9031_JOB_SOURCE_SYSTEM T9031
where T9031.C9030_JOB_ACTIVITY_ID = v_job_activity_id
and T9031.C9031_ATTRIBUTE_TYPE in (104666,104667,104668,104669,104670,104671,104672,104673,104674,104675) -- product code
and (not regexp_like (T9031.C9031_ATTRIBUTE_VALUE,'^[[:alpha:]]+$')
OR LENGTH(T9031.C9031_ATTRIBUTE_VALUE) != 3) ;

BEGIN

FOR product_class_v IN cur_product_class_v
LOOP

gm_pkg_cm_job_system_trans.gm_sav_job_error_event(sysdate,3,v_job_activity_id,NULL, product_class_v.REFID,v_error_action); 

END LOOP;

END DE34_PROD_CLASS_3_CHAR_LEN;

/*
1. UDI024 - if doesTradeItemContainHumanTissue = TRUE and alternateClassification/scheme = FDAPSN, alternateClassification/code must begin with P, K, H, BP, BK, or BH
*/
PROCEDURE DE28_DE32_TISSUE_FDAPSN_CODE
AS

cursor cur_fdapsn_code_v IS
select T9031.C9031_REF_ID REFID, T9031.C9031_ATTRIBUTE_TYPE 
, T9031.C9031_ATTRIBUTE_VALUE
from T9031_JOB_SOURCE_SYSTEM T9031, (select * 
                                      from T9031_JOB_SOURCE_SYSTEM 
                                      where C9030_JOB_ACTIVITY_ID = v_job_activity_id 
                                      AND C9031_ATTRIBUTE_TYPE = 103345 -- HCTP
                                      AND c9031_ATTRIBUTE_VALUE = 20375 -- true
                                      ) T9031_HCTP
where T9031.C9031_REF_ID = T9031_HCTP.C9031_REF_ID
and T9031.C9030_JOB_ACTIVITY_ID = v_job_activity_id
and T9031.C9031_ATTRIBUTE_TYPE in (104643,104644,104645,104646,104647,104648,104649,104650,104651,104652) 
and not regexp_like (T9031.c9031_ATTRIBUTE_VALUE,'^(P|K|H|BP|BK|BH).*');

BEGIN

FOR fdapsn_code_v IN cur_fdapsn_code_v
LOOP

gm_pkg_cm_job_system_trans.gm_sav_job_error_event(sysdate,19,v_job_activity_id,NULL, fdapsn_code_v.REFID,v_error_action); 

END LOOP;

END DE28_DE32_TISSUE_FDAPSN_CODE;

/*
1. UDI025 - if doesTradeItemContainHumanTissue = TRUE then exemptFromFDAPreMarketAuthorization cannot be TRUE
*/
PROCEDURE DE28_DE31_TISSUE_NOT_EXEMPT
AS

cursor cur_tissue_not_exempt_v is
select REFID
from 
( select T9031.C9031_REF_ID REFID
, max (case when T9031.C9031_ATTRIBUTE_TYPE = 103345 then C9031_ATTRIBUTE_VALUE end) DE28_HCTP
, max (case when T9031.C9031_ATTRIBUTE_TYPE = 104701 then C9031_ATTRIBUTE_VALUE end) DE31_EXEMPT_PREMARKET
from T9031_JOB_SOURCE_SYSTEM  T9031
where C9030_JOB_ACTIVITY_ID = v_job_activity_id 
and C9031_ATTRIBUTE_TYPE in (103345,104701)
group by T9031.C9031_REF_ID
)
WHERE DE28_HCTP = 20375 AND DE31_EXEMPT_PREMARKET = 80130;

BEGIN

FOR tissue_not_exempt_v IN cur_tissue_not_exempt_v
LOOP

gm_pkg_cm_job_system_trans.gm_sav_job_error_event(sysdate,20,v_job_activity_id,NULL, tissue_not_exempt_v.REFID,v_error_action); 

END LOOP;

END DE28_DE31_TISSUE_NOT_EXEMPT;

/*
1. UDI032 - IF fDAMedicalDeviceListing is populated it must be 7 characters in length
*/
PROCEDURE DE36_LISTING_NUM_7_CHAR
AS

cursor cur_listing_num_v is
select T9031.C9031_REF_ID REFID,T9031.C9031_ATTRIBUTE_TYPE , T9031.C9031_ATTRIBUTE_VALUE
from T9031_JOB_SOURCE_SYSTEM  T9031
where C9030_JOB_ACTIVITY_ID = v_job_activity_id 
and C9031_ATTRIBUTE_TYPE = 104655
AND LENGTH(NVL(C9031_ATTRIBUTE_VALUE,'1234567')) != 7;

BEGIN

FOR listing_num_v IN cur_listing_num_v
LOOP

gm_pkg_cm_job_system_trans.gm_sav_job_error_event(sysdate,23,v_job_activity_id,NULL, listing_num_v.REFID,v_error_action); 

END LOOP;


END DE36_LISTING_NUM_7_CHAR;


END gm_pkg_udi_v_1ws_fdaudi_reg;
/
