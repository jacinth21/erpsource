-- @"C:\jbossbranch\Database\Packages\ProdDevelopement\UDI\gm_pkg_udi_v_1ws_fdaudi_qa.bdy"
CREATE OR REPLACE PACKAGE BODY gm_pkg_udi_v_1ws_fdaudi_qa
IS

v_error_action T9033_JOB_ERROR_EVENT.C901_STATUS%TYPE := 104900; -- open

/*
1. UDI020 -  directPartMarking must be between 6-23 character
*/
PROCEDURE DE17_DIRECT_PART_MARKING_6TO23
AS
cursor cur_direct_part_marking is
select T9031.C9031_REF_ID REFID,T9031.C9031_ATTRIBUTE_TYPE , T9031.C9031_ATTRIBUTE_VALUE
from T9031_JOB_SOURCE_SYSTEM  T9031
where C9030_JOB_ACTIVITY_ID = v_job_activity_id 
and C9031_ATTRIBUTE_TYPE = 103344 -- DUNS number
and LENGTH(T9031.C9031_ATTRIBUTE_VALUE) BETWEEN 6 AND 23;

BEGIN

FOR direct_part_marking IN cur_direct_part_marking
LOOP

gm_pkg_cm_job_system_trans.gm_sav_job_error_event(sysdate,18,v_job_activity_id,NULL, direct_part_marking.REFID,v_error_action);

END LOOP;

END DE17_DIRECT_PART_MARKING_6TO23;

/*
1. UDI034 - fDAUnitOfUseGTIN is required if NetContent (DE3) != 1
*/
PROCEDURE DE4_FDA_UNIT_OF_USE_GTIN
AS

cursor cur_unit_of_use_gtin is
select REFID
from 
( select T9031.C9031_REF_ID REFID
, max (case when T9031.C9031_ATTRIBUTE_TYPE = 103785 then C9031_ATTRIBUTE_VALUE end) DE3_DEVICE_COUNT
, max (case when T9031.C9031_ATTRIBUTE_TYPE = 4000542 then C9031_ATTRIBUTE_VALUE end) DE4_UNIT_OF_USE
from T9031_JOB_SOURCE_SYSTEM  T9031
where C9030_JOB_ACTIVITY_ID = v_job_activity_id 
and C9031_ATTRIBUTE_TYPE in (4000542,103785)
group by T9031.C9031_REF_ID
)
WHERE DE3_DEVICE_COUNT > 1 AND DE4_UNIT_OF_USE IS NULL;

BEGIN

FOR unit_of_use_gtin IN cur_unit_of_use_gtin
LOOP

gm_pkg_cm_job_system_trans.gm_sav_job_error_event(sysdate,24,v_job_activity_id,NULL, unit_of_use_gtin.REFID,v_error_action); 

END LOOP;

END DE4_FDA_UNIT_OF_USE_GTIN;


/*
1. UDI035 - If netContent = 1 and netContentUOM=1N, fDAUnitOfUseGTIN cannot be populated
*/
PROCEDURE DE4_FDA_NETCONTENT_1_NO_UoU
AS

cursor cur_unit_of_use_gtin is
select REFID
from 
( select T9031.C9031_REF_ID REFID
, max (case when T9031.C9031_ATTRIBUTE_TYPE = 103785 then C9031_ATTRIBUTE_VALUE end) DE3_DEVICE_COUNT
, max (case when T9031.C9031_ATTRIBUTE_TYPE = 4000542 then C9031_ATTRIBUTE_VALUE end) DE4_UNIT_OF_USE
from T9031_JOB_SOURCE_SYSTEM  T9031
where C9030_JOB_ACTIVITY_ID = v_job_activity_id 
and C9031_ATTRIBUTE_TYPE in (4000542,103785)
group by T9031.C9031_REF_ID
)
WHERE DE3_DEVICE_COUNT = 1 AND DE4_UNIT_OF_USE IS NOT NULL;

BEGIN

FOR unit_of_use_gtin IN cur_unit_of_use_gtin
LOOP

gm_pkg_cm_job_system_trans.gm_sav_job_error_event(sysdate,25,v_job_activity_id,NULL, unit_of_use_gtin.REFID,v_error_action); 

END LOOP;

END DE4_FDA_NETCONTENT_1_NO_UoU;

/*
1. UDI039 - If isTradeItemExemptFromDirectPartMarking = TRUE, directPartMarking cannot be populated
*/
PROCEDURE DE15_EXEMPT_FROM_MARKING
AS

cursor cur_exempt_from_marking is
select REFID
from 
( select T9031.C9031_REF_ID REFID
, max (case when T9031.C9031_ATTRIBUTE_TYPE = 103342 then C9031_ATTRIBUTE_VALUE end) DE15_SUBJECT_TO_DM
, max (case when T9031.C9031_ATTRIBUTE_TYPE = 103344 then C9031_ATTRIBUTE_VALUE end) DE17_DM_DI_NUMBER
from T9031_JOB_SOURCE_SYSTEM  T9031
where C9030_JOB_ACTIVITY_ID = v_job_activity_id 
and C9031_ATTRIBUTE_TYPE in (103342,103344)
group by T9031.C9031_REF_ID
)
WHERE DE15_SUBJECT_TO_DM = 'Y' -- change to codeid
AND DE17_DM_DI_NUMBER IS NOT NULL;

BEGIN

FOR exempt_from_marking IN cur_exempt_from_marking
LOOP

gm_pkg_cm_job_system_trans.gm_sav_job_error_event(sysdate,29,v_job_activity_id,NULL, exempt_from_marking.REFID,v_error_action); 

END LOOP;

END DE15_EXEMPT_FROM_MARKING;


END gm_pkg_udi_v_1ws_fdaudi_qa;
/
