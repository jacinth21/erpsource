/* Formatted on 2009/02/16 12:30 (Formatter Plus v4.8.0) */
--@"C:\Database\Packages\ProdDevelopement\udi\gm_pkg_pd_udi_submission_rpt.bdy"
-- ;
CREATE OR REPLACE
PACKAGE body gm_pkg_pd_udi_submission_rpt
IS
  
/***************************************************************
* Description : Procedure to fetch data  attributes  (general of type 1 value)
for UDI Submission
****************************************************************/
PROCEDURE gm_fch_general_attr(
    p_out_genattr_cur OUT TYPES.cursor_type
  )
AS
 
BEGIN
  
  OPEN p_out_genattr_cur FOR
    SELECT uparamtemp.* from my_temp_udi_param uparamtemp;
END gm_fch_general_attr;
/***************************************************************
* Description : Procedure to fetch multiple attributes for each DI for UDI
Submission
****************************************************************/
PROCEDURE gm_fch_multi_attr(
    p_out_multiattr_cur OUT TYPES.cursor_type
  )
AS
BEGIN
  OPEN p_out_multiattr_cur FOR
  SELECT multiparamtemp.* from my_temp_udi_multi_attr multiparamtemp;
END gm_fch_multi_attr;
/***************************************************************
* Description : Procedure to fetch data  attributes (general of type 1 value)
for Publication Submission
****************************************************************/
PROCEDURE gm_fch_publish_attr(
    p_out_pubattr_cur OUT TYPES.cursor_type
  )
AS
BEGIN
  OPEN p_out_pubattr_cur FOR
    SELECT uparamtemp.* from my_temp_publish_param uparamtemp;
END gm_fch_publish_attr;
END gm_pkg_pd_udi_submission_rpt;
/
