/* Formatted on 2009/02/16 12:30 (Formatter Plus v4.8.0) */
--@"C:\Database\Packages\ProdDevelopement\udi\gm_pkg_pd_udi_submission_trans.bdy";
CREATE OR REPLACE
PACKAGE body gm_pkg_pd_udi_submission_trans
IS

/***************************************************************
  * Description : Main procedure  for  Item which does the following activities
  * 1.Insert UDI parameters into temp table from a view
  * 2.Save the selected data  that will be used for XML in appropriate table
  * 3.Fetch the parameters that will be used to create XML file
  * Author   : Mihir
  ****************************************************************/
PROCEDURE gm_create_udi_item_xml_main(
    p_di_inputstr IN CLOB,
    p_intstatus IN  t2072_di_gudid_status.c901_internal_submit_status%TYPE,
    p_extstatus  IN t2072_di_gudid_status.c901_external_submit_status%TYPE,
    p_xmltype  IN VARCHAR2,
    p_userid IN t9030_job_activity.c9030_last_updated_by%TYPE,
    p_out_genattr_cur OUT TYPES.cursor_type,
    p_out_nestattr_cur OUT TYPES.cursor_type,
    p_out_jobid   OUT VARCHAR2
    )
 AS 
  v_out_cnt NUMBER:=0;
BEGIN
  --Save input string in context , if the sting is null then save all the parts else the input string
  gm_validate_item_temp_data(p_di_inputstr,p_intstatus,p_extstatus,v_out_cnt);
   

   IF v_out_cnt >0
  THEN
      --Insert data into temp tables
    gm_sav_udi_item_temp_data (p_di_inputstr,p_intstatus,p_extstatus);
    
    --save param that will be used for xml file
    gm_sav_udi_xml_param(p_xmltype,p_userid,p_out_jobid);
  
END IF;
--fetch XML data
gm_pkg_pd_udi_submission_rpt.gm_fch_general_attr(p_out_genattr_cur);
gm_pkg_pd_udi_submission_rpt.gm_fch_multi_attr(p_out_nestattr_cur);

END gm_create_udi_item_xml_main;

/***************************************************************
  * Description : Main procedure for Publication which does the following activities
  * 1.Insert UDI parameters into temp table from a view
  * 2.Save the selected data  that will be used for XML in appropriate table
  * 3.Fetch the parameters that will be used to create XML file
  * Author   : Mihir
  ****************************************************************/
PROCEDURE gm_create_udi_pub_xml_main(
    p_di_inputstr IN CLOB,
    p_intstatus IN  t2072_di_gudid_status.c901_internal_submit_status%TYPE,
    p_extstatus  IN t2072_di_gudid_status.c901_external_submit_status%TYPE,
    p_xmltype  IN VARCHAR2,
    p_userid IN t9030_job_activity.c9030_last_updated_by%TYPE,
    p_out_pubattr_cur OUT TYPES.cursor_type,
    p_out_jobid   OUT VARCHAR2
    )
 AS 
  v_out_cnt NUMBER:=0;
BEGIN
  
  gm_validate_pub_temp_data(p_di_inputstr,p_intstatus,p_extstatus,v_out_cnt);
  IF v_out_cnt >0
  THEN
   
   gm_sav_udi_pub_temp_data (p_di_inputstr,p_intstatus,p_extstatus);
      
      --save param that will be used for xml file
    gm_sav_udi_xml_param(p_xmltype,p_userid,p_out_jobid);
    
  END IF;
--fetch XML data
gm_pkg_pd_udi_submission_rpt.gm_fch_publish_attr(p_out_pubattr_cur);
END gm_create_udi_pub_xml_main;

/***************************************************************
  * Description :  proc for Item,  Procedure used to validate 
  *when there are DIs for a particular status
  * Author   : Mihir
  ****************************************************************/
PROCEDURE gm_validate_item_temp_data(
    p_di_inputstr IN CLOB,
    p_intstatus IN  t2072_di_gudid_status.c901_internal_submit_status%TYPE,
    p_extstatus  IN t2072_di_gudid_status.c901_external_submit_status%TYPE,
    p_out_cnt   OUT NUMBER
    )
 AS 
  
BEGIN
  gm_sav_item_context_dis(p_di_inputstr,p_intstatus,p_extstatus);
  SELECT COUNT(1)
    INTO p_out_cnt
    FROM 
    V2070_UDI_PARAMETER vdataudi,v_clob_list my_temp,T2072_DI_GUDID_STATUS t2072
    WHERE vdataudi.DE2_PRIMARY_DI_NUMBER = my_temp.token
    AND VDATAUDI.DE2_PRIMARY_DI_NUMBER =T2072.C2060_DI_NUMBER
     AND NVL(t2072.C901_INTERNAL_SUBMIT_STATUS,99999) = (NVL(p_intstatus,NVL(t2072.C901_INTERNAL_SUBMIT_STATUS,99999)))--4000806 Reviewed
     AND NVL(t2072.C901_EXTERNAL_SUBMIT_STATUS,99999) = (NVL(p_extstatus,NVL(t2072.C901_EXTERNAL_SUBMIT_STATUS,99999)))--NUll
    AND t2072.C2072_VOID_FL IS NULL
    AND my_temp.token IS NOT NULL;
 END gm_validate_item_temp_data;
 
 /***************************************************************
  * Description :  Proc for Publish, Proc  used to validate 
  *when there are DIs for a particular status
  * Author   : Mihir
  ****************************************************************/
PROCEDURE gm_validate_pub_temp_data(
    p_di_inputstr IN CLOB,
    p_intstatus IN  t2072_di_gudid_status.c901_internal_submit_status%TYPE,
    p_extstatus  IN t2072_di_gudid_status.c901_external_submit_status%TYPE,
    p_out_cnt   OUT NUMBER
    )
 AS 
  
BEGIN
  gm_sav_pub_context_dis(p_di_inputstr,p_intstatus,p_extstatus);
  SELECT COUNT(1)
    INTO p_out_cnt
    FROM 
    v_clob_list my_temp,T2072_DI_GUDID_STATUS t2072
    WHERE my_temp.token =T2072.C2060_DI_NUMBER
    AND NVL(t2072.C901_INTERNAL_SUBMIT_STATUS,99999) = (NVL(p_intstatus,NVL(t2072.C901_INTERNAL_SUBMIT_STATUS,99999)))--4000806 Reviewed
     AND NVL(t2072.C901_EXTERNAL_SUBMIT_STATUS,99999) = (NVL(p_extstatus,NVL(t2072.C901_EXTERNAL_SUBMIT_STATUS,99999)))--NUll
    AND t2072.C2072_VOID_FL IS NULL
    AND my_temp.token IS NOT NULL;
 END gm_validate_pub_temp_data;

/***************************************************************
  * Description : Procedure to save the inputstring in context, if the string is null 
  *then save all the Dis based on status.
  * Author   : Mihir
  ****************************************************************/
PROCEDURE gm_sav_item_context_dis(
    p_di_inputstr IN CLOB,
    p_intstatus IN  t2072_di_gudid_status.c901_internal_submit_status%TYPE,
    p_extstatus  IN t2072_di_gudid_status.c901_external_submit_status%TYPE
   )
 AS 
 CURSOR dino_all_cur
 IS
  SELECT C2060_DI_NUMBER dino 
  FROM  T2072_DI_GUDID_STATUS t2072
  WHERE  NVL(t2072.C901_INTERNAL_SUBMIT_STATUS,99999) = (NVL(p_intstatus,NVL(t2072.C901_INTERNAL_SUBMIT_STATUS,99999)))
   AND NVL(t2072.C901_EXTERNAL_SUBMIT_STATUS,99999) = (NVL(p_extstatus,NVL(t2072.C901_EXTERNAL_SUBMIT_STATUS,99999)))
   AND t2072.c2072_void_fl IS NULL;
   
 CURSOR dino_inputstr_cur
 IS
  SELECT token dino 
  FROM  v_clob_list v_c_list
  WHERE v_c_list.token IS NOT NULL;  
  v_cnt NUMBER:=0;
  v_alldinum CLOB;
BEGIN
  IF p_di_inputstr IS NOT NULL
  THEN
   my_context.set_my_cloblist (p_di_inputstr);
    
    FOR dino_inputstr IN dino_inputstr_cur
    LOOP
        SELECT COUNT(1)
            INTO v_cnt
        FROM T2072_DI_GUDID_STATUS T2072
        WHERE T2072.C2060_DI_NUMBER = dino_inputstr.dino
        AND NVL(t2072.C901_INTERNAL_SUBMIT_STATUS,99999) = (NVL(p_intstatus,NVL(t2072.C901_INTERNAL_SUBMIT_STATUS,99999)))
        AND NVL(t2072.C901_EXTERNAL_SUBMIT_STATUS,99999) = (NVL(p_extstatus,NVL(t2072.C901_EXTERNAL_SUBMIT_STATUS,99999)))
        AND t2072.c2072_void_fl IS NULL;
        
        IF v_cnt >0
        THEN
            v_alldinum := v_alldinum ||','|| dino_inputstr.dino||',';
        END IF;
    END LOOP;
     my_context.set_my_cloblist (v_alldinum);
    
  ELSE
  
    FOR dinum IN dino_all_cur
    LOOP
      v_alldinum := v_alldinum ||','|| dinum.dino||',';
    END LOOP;
  my_context.set_my_cloblist (v_alldinum);
  END IF;

END gm_sav_item_context_dis;

/***************************************************************
  * Description : Procedure to save the inputstring in context, if the string is null 
  *then save all the Dis based on status.
  * Author   : Mihir
  ****************************************************************/
PROCEDURE gm_sav_pub_context_dis(
    p_di_inputstr IN CLOB,
    p_intstatus IN  t2072_di_gudid_status.c901_internal_submit_status%TYPE,
    p_extstatus  IN t2072_di_gudid_status.c901_external_submit_status%TYPE
   )
 AS 
 CURSOR dino_all_cur
 IS
  SELECT C2060_DI_NUMBER dino 
  FROM  T2072_DI_GUDID_STATUS t2072
  WHERE  NVL(t2072.C901_INTERNAL_SUBMIT_STATUS,99999) = (NVL(p_intstatus,NVL(t2072.C901_INTERNAL_SUBMIT_STATUS,99999)))
   AND NVL(t2072.C901_EXTERNAL_SUBMIT_STATUS,99999) = (NVL(p_extstatus,NVL(t2072.C901_EXTERNAL_SUBMIT_STATUS,99999)))
   AND t2072.c2072_void_fl IS NULL
   AND t2072.C2072_GDSN_PUBLISH_UPLOAD_FL IS NULL;

CURSOR dino_inputstr_cur
 IS
  SELECT token dino 
  FROM  v_clob_list v_c_list
  WHERE v_c_list.token IS NOT NULL;
   
  v_cnt NUMBER:=0;
  v_alldinum CLOB;
BEGIN
  IF p_di_inputstr IS NOT NULL
  THEN
    my_context.set_my_cloblist (p_di_inputstr);
    
    FOR dino_inputstr IN dino_inputstr_cur
    LOOP
        SELECT COUNT(1)
            INTO v_cnt
        FROM T2072_DI_GUDID_STATUS T2072
        WHERE T2072.C2060_DI_NUMBER = dino_inputstr.dino
        AND NVL(t2072.C901_INTERNAL_SUBMIT_STATUS,99999) = (NVL(p_intstatus,NVL(t2072.C901_INTERNAL_SUBMIT_STATUS,99999)))
        AND NVL(t2072.C901_EXTERNAL_SUBMIT_STATUS,99999) = (NVL(p_extstatus,NVL(t2072.C901_EXTERNAL_SUBMIT_STATUS,99999)))
        AND t2072.c2072_void_fl IS NULL
        AND T2072.C2072_GDSN_PUBLISH_UPLOAD_FL IS NULL;
        
        IF v_cnt >0
        THEN
            v_alldinum := v_alldinum ||','|| dino_inputstr.dino||',';
        END IF;
    END LOOP;
     my_context.set_my_cloblist (v_alldinum);
  ELSE
  
    FOR dinum IN dino_all_cur
    LOOP
      v_alldinum := v_alldinum ||','|| dinum.dino||',';
    END LOOP;
  my_context.set_my_cloblist (v_alldinum);
  END IF;

END gm_sav_pub_context_dis;


 /***************************************************************
  * Description : Procedure to save data from view to temp table for Item
  ****************************************************************/
PROCEDURE gm_sav_udi_item_temp_data(
    p_di_inputstr IN CLOB,
    p_intstatus IN  t2072_di_gudid_status.c901_internal_submit_status%TYPE,
    p_extstatus  IN t2072_di_gudid_status.c901_external_submit_status%TYPE
  )
AS
BEGIN


INSERT INTO my_temp_udi_param
 SELECT
    vdataudi.* , 'catreq.docid.'||vdataudi.me2_brandowner_gln||'.'||s2072_document_id.nextval
  , decode (t2072.c2072_gdsn_item_upload_fl , NULL , 'ADD' , 'MODIFY') , decode (t2072.c2072_gdsn_publish_upload_fl ,
    NULL , 'ADD' , 'REPUBLISH') --'ADD','REPUBLISH' DELETE
  , c2072_gdsn_cic_fl
   FROM
    v2070_udi_parameter vdataudi , v_clob_list my_temp
  , t2072_di_gudid_status t2072
  WHERE
    vdataudi.de2_primary_di_number = my_temp.token
    AND vdataudi.de2_primary_di_number = t2072.c2060_di_number
    AND nvl (t2072.c901_internal_submit_status , 99999) = (nvl (p_intstatus , nvl (t2072.c901_internal_submit_status ,
    99999))) --104824 Submitted
    AND nvl (t2072.c901_external_submit_status , 99999) = (nvl (p_extstatus , nvl (t2072.c901_external_submit_status ,
    99999))) --104826 Success-Uploaded Item to 1ws
    AND t2072.c2072_void_fl IS NULL
    AND my_temp.token IS NOT NULL;
 
 --UDI multi Attr into temp table
 INSERT INTO my_temp_udi_multi_attr
   SELECT vmultiudi.* FROM
  v2070_udi_multi_attributes vmultiudi,v_clob_list my_temp
  WHERE vmultiudi.c2060_di_number = my_temp.token
  AND my_temp.token IS NOT NULL;
 END gm_sav_udi_item_temp_data;
 
  /***************************************************************
  * Description : Procedure to save data from view to temp table for Publication
  ****************************************************************/
PROCEDURE gm_sav_udi_pub_temp_data(
    p_di_inputstr IN CLOB,
    p_intstatus IN  t2072_di_gudid_status.c901_internal_submit_status%TYPE,
    p_extstatus  IN t2072_di_gudid_status.c901_external_submit_status%TYPE
  )
AS
BEGIN



INSERT INTO my_temp_publish_param
 SELECT
    gdsnmandate.informationprovider , gdsnmandate.xml_representedparty
  , gdsnmandate.xml_receiver , to_char (SYSDATE , get_rule_value ('XMLDATEFORMAT' , 'DATEFORMAT')) ||'T'||to_char (
    SYSDATE , 'HH24:MI:SS') , gdsnmandate.xml_batchuserid
  , 'catreq.docid.'||gdsnmandate.informationprovider||'.'||s2072_document_id.nextval , decode (
    t2072.c2072_gdsn_item_upload_fl , NULL , 'ADD' , 'MODIFY') , decode (t2072.c2072_gdsn_publish_upload_fl , NULL ,
    'ADD' , 'REPUBLISH') --'ADD','REPUBLISH' DELETE
  , t2072.c2060_di_number , gdsnmandate.targetmarketcountrycode
  , GET_RULE_VALUE('dataRecipientGLN','GDSN_MANDATE')-- Preprod '1100001017041' --XML_Market Group
  , decode (c2072_gudid_publish_fl , NULL , 'New' , 'InitialLoad') -- XML_TYPE
  , to_char (SYSDATE , get_rule_value ('XMLDATEFORMAT' , 'DATEFORMAT')) ||'T'||to_char (SYSDATE , 'HH24:MI:SS')
   FROM
    (
         SELECT
            t2072.c2072_gdsn_item_upload_fl , t2072.c2072_gdsn_publish_upload_fl
          , t2072.c2060_di_number , t2072.c2072_gudid_publish_fl
           FROM
            v_clob_list my_temp , t2072_di_gudid_status t2072
          WHERE
            my_temp.token = t2072.c2060_di_number
            AND my_temp.token IS NOT NULL
            AND t2072.c2072_void_fl IS NULL
            AND nvl(t2072.c901_internal_submit_status,99999) = (nvl(p_intstatus,nvl(t2072.c901_internal_submit_status,99999)))
     AND nvl(t2072.c901_external_submit_status,99999) = (nvl(p_extstatus,nvl(t2072.c901_external_submit_status,99999)))
            AND t2072.c2072_gdsn_publish_upload_fl IS NULL
    )
    t2072 , (
         SELECT
            MAX (
                CASE
                    WHEN c906_rule_id = 'ME2'
                    THEN c906_rule_value
                END) informationprovider , MAX (
                CASE
                    WHEN c906_rule_id = 'RepresentedParty'
                    THEN nvl (c906_rule_value , 'N')
                END) xml_representedparty , MAX (
                CASE
                    WHEN c906_rule_id = 'Receiver'
                    THEN nvl (c906_rule_value , 'N')
                END) xml_receiver , MAX (
                CASE
                    WHEN c906_rule_id = 'BatchUserId'
                    THEN nvl (c906_rule_value , 'N')
                END) xml_batchuserid , MAX (
                CASE
                    WHEN c906_rule_id = 'ME4'
                    THEN c906_rule_value
                END) targetmarketcountrycode
           FROM
            t906_rules
          WHERE
            c906_rule_grp_id = 'GDSN_MANDATE'
    )
    gdsnmandate;
 
 END gm_sav_udi_pub_temp_data;
 
 /***************************************************************
  * Description : Common Procedure is save xml file parameters single parameter
  * Author   : Mihir
  ****************************************************************/
PROCEDURE gm_sav_udi_xml_param (
p_xmltype  IN VARCHAR2,
p_userid IN t9030_job_activity.c9030_last_updated_by%TYPE,
 p_out_jobid     OUT t9030_job_activity.C9030_JOB_ID%TYPE
)
 AS 
   
   v_out_job_actid  t9030_job_activity.C9030_JOB_ACTIVITY_ID%TYPE;
   v_out_job_id  t9030_job_activity.C9030_JOB_ID%TYPE;
    v_jobtypenm t9030_job_activity.C9030_JOB_NAME%TYPE;
    v_srctp t9031_job_source_system.C901_REF_TYPE%TYPE;
BEGIN
  
 
  IF p_xmltype  = 'ITEMXML' THEN
        v_jobtypenm :='Globus_UDI_1WS_item_Submission_Parent';
        v_srctp := 104820;--104820,'FDA: Item XML Submit to 1ws','FDAIXM'   
      
         gm_pkg_cm_job_system_trans.gm_sav_job_activity(v_jobtypenm,'create_xml',v_srctp,'xml','1',NULL,NULL,NULL,p_userid,v_out_job_actid,v_out_job_id);
        
        --insert UDI single Parameter
        gm_sav_udi_item_param(v_out_job_actid,v_srctp,p_userid);
       
        --insert multi attributes
        gm_sav_udi_item_multi_param(v_out_job_actid,v_srctp,p_userid);
       
        --update status --104823	Item Submission -In Progress
        gm_upd_gudid_item_xml_status(104823,p_userid);
        
     ELSIF  p_xmltype = 'PUBXML' THEN
            v_jobtypenm :='Globus_UDI_1WS_Publish_Submission_Parent';
          v_srctp := 104840; --104840,FDA: Publication XML Submit to 1ws
             --104840,FDA: Publication XML Submit to 1ws
          gm_pkg_cm_job_system_trans.gm_sav_job_activity(v_jobtypenm,'create_xml',v_srctp,'xml','1',NULL,NULL,NULL,p_userid,v_out_job_actid,v_out_job_id);
          
          --insert UDI single Parameter
          gm_sav_udi_pub_param(v_out_job_actid,v_srctp,p_userid);
         
         
          --update status-- 104828 Publish Submission -In Progress
          gm_upd_gudid_pub_xml_status(104828,p_userid);
       END IF;
       p_out_jobid:=v_out_job_id;
 END gm_sav_udi_xml_param;

  /***************************************************************
  * Description : Procedure is used to save udi single paramter  for Item
  * Author   : Mihir
  ****************************************************************/
PROCEDURE gm_sav_udi_item_param(
 p_job_actid IN t9030_job_activity.C9030_JOB_ID%TYPE,
   p_reftype IN t9031_job_source_system.C901_REF_TYPE%TYPE,
   p_userid IN t9030_job_activity.c9030_last_updated_by%TYPE
  )
 AS 
   genattr_ref_cur SYS_REFCURSOR; 
   
    CURSOR columns_genattr_cur IS 
        SELECT t2.column_value.getrootelement() clnm, 
               EXTRACTVALUE(t2.column_value, 'node()')  clval 
          FROM (SELECT * FROM TABLE (XMLSEQUENCE(genattr_ref_cur))) t1, 
                 table  (xmlsequence(extract(t1.column_value, '/ROW/node()')))t2; 
      
      CURSOR dis_cur IS
       SELECT de2_primary_di_number dino
         FROM my_temp_udi_param;
BEGIN
 FOR dis IN dis_cur
  LOOP
  
     OPEN genattr_ref_cur FOR 'SELECT * FROM MY_TEMP_UDI_PARAM WHERE de2_primary_di_number =' ||dis.dino; 
     --OPEN genattr_ref_cur FOR 'SELECT * FROM MY_TEMP_UDI_PARAM where de2_primary_di_number = ''00889095143218''';
   
     FOR columns_genattr IN columns_genattr_cur 
         LOOP 
            --DBMS_OUTPUT.put_line(rec_.key || ': ' || rec_.value); 
            gm_pkg_cm_job_system_trans.gm_sav_job_src_system(p_job_actid,columns_genattr.clnm,columns_genattr.clval,p_reftype,dis.dino,p_userid);
     END LOOP;
     CLOSE genattr_ref_cur;
  
  END LOOP;--END Outer loop
  
END gm_sav_udi_item_param;


 /***************************************************************
  * Description : Procedure is used to multi param details  for Item
  * Author   : Mihir
  ****************************************************************/
PROCEDURE gm_sav_udi_item_multi_param(
   p_job_actid IN t9030_job_activity.C9030_JOB_ID%TYPE,
    p_reftype IN t9031_job_source_system.C901_REF_TYPE%TYPE,
   p_userid IN t9030_job_activity.c9030_last_updated_by%TYPE
  )
 AS 
  CURSOR dis_multi_cur IS
       SELECT C205_PART_NUMBER_ID pnum,
               C2060_DI_NUMBER dino,
               C906_RULE_ID ruleid,
               C205D_ATTRIBUTE_VALUE attrval,
               C906_RULE_GRP_ID rulegrpid
         FROM my_temp_udi_multi_attr;
    
BEGIN


  FOR dis_multi IN dis_multi_cur
  LOOP
  gm_pkg_cm_job_system_trans.gm_sav_job_src_system(p_job_actid,dis_multi.ruleid,dis_multi.attrval,p_reftype,dis_multi.dino,p_userid);
      
  END LOOP;
  
END gm_sav_udi_item_multi_param;

/***************************************************************
  * Description : Procedure is used to save udi single paramter  for Publication
  * Author   : Mihir
  ****************************************************************/
PROCEDURE gm_sav_udi_pub_param(
 p_job_actid IN t9030_job_activity.C9030_JOB_ID%TYPE,
   p_reftype IN t9031_job_source_system.C901_REF_TYPE%TYPE,
   p_userid IN t9030_job_activity.c9030_last_updated_by%TYPE
  )
 AS 
   genattr_ref_cur SYS_REFCURSOR; 
   
    CURSOR columns_genattr_cur IS 
        SELECT t2.column_value.getrootelement() clnm, 
               EXTRACTVALUE(t2.column_value, 'node()')  clval 
          FROM (SELECT * FROM TABLE (XMLSEQUENCE(genattr_ref_cur))) t1, 
                 table  (xmlsequence(extract(t1.column_value, '/ROW/node()')))t2; 
      
      CURSOR dis_cur IS
       SELECT de2_primary_di_number dino
         FROM my_temp_publish_param;
BEGIN
 FOR dis IN dis_cur
  LOOP
  
     OPEN genattr_ref_cur FOR 'SELECT * FROM my_temp_publish_param WHERE de2_primary_di_number =' ||dis.dino; 

   
     FOR columns_genattr IN columns_genattr_cur 
         LOOP 
             -- 104840,FDA: Publication XML Submit to 1ws   
            gm_pkg_cm_job_system_trans.gm_sav_job_src_system(p_job_actid,columns_genattr.clnm,columns_genattr.clval,p_reftype,dis.dino,p_userid);
     END LOOP;
     CLOSE genattr_ref_cur;
  
  END LOOP;--END Outer loop
  
END gm_sav_udi_pub_param;
 /***************************************************************
  * Description : Procedure is used to save gidid  internal Status to Item Submission -In Progress for all temp DI Numbers
  * Author   : Mihir
  ****************************************************************/
PROCEDURE gm_upd_gudid_item_xml_status(
  p_intstatus IN  t2072_di_gudid_status.c901_internal_submit_status%TYPE,
 p_userid IN t9030_job_activity.c9030_last_updated_by%TYPE
 )
 AS
 CURSOR udi_param_cur IS
 SELECT DE2_PRIMARY_DI_NUMBER dino,DOCID docid FROM MY_TEMP_UDI_PARAM;
 
 BEGIN
FOR udi_param IN udi_param_cur 
     LOOP 
          
         gm_sav_gudid_stat_data(udi_param.dino,p_intstatus,NULL,udi_param.docid,null,null, null, null, null, null, null,null, null, null, p_userid, SYSDATE, null,null,null);

     END LOOP;
    
 
END gm_upd_gudid_item_xml_status;

/***************************************************************
  * Description : Procedure is used to save gidid  internal Status to Publication Submission -In Progress for all temp DI Numbers
  * Author   : Mihir
  ****************************************************************/
PROCEDURE gm_upd_gudid_pub_xml_status(
   p_extstatus  IN t2072_di_gudid_status.c901_external_submit_status%TYPE,
 p_userid IN t9030_job_activity.c9030_last_updated_by%TYPE
 )
 AS
 CURSOR udi_param_cur IS
 SELECT DE2_PRIMARY_DI_NUMBER dino,DOCID docid FROM my_temp_publish_param;
 
 BEGIN
FOR udi_param IN udi_param_cur 
     LOOP 
         gm_pkg_pd_udi_submission_trans.gm_sav_gudid_stat_data(udi_param.dino,NULL,p_extstatus,udi_param.docid,null,null, null, null, null, null, null,null, null, null, p_userid, SYSDATE, null,null,null);

     END LOOP;
    
 
END gm_upd_gudid_pub_xml_status;
/***************************************************************
  * Description : Function to fetch message ID
  * Author   : Mihir
  ****************************************************************/
 FUNCTION get_udi_message_id(
   p_xmltype  IN VARCHAR2
  )
        RETURN VARCHAR2
    IS
        v_msgid t9030_job_activity.c9030_udi_message_id%TYPE;
        v_doctype VARCHAR2 (50);
    BEGIN
      IF p_xmltype  = 'ITEMXML' THEN
       v_doctype :='catreq.item.msgid.';
        
     ELSIF  p_xmltype = 'PUBXML' THEN
       v_doctype :='catreq.pub.msgid.';
     END IF;
      
        select v_doctype||get_rule_value('ME2','GDSN_MANDATE')||'.'||s9030_udi_message_id.nextval 
      INTO v_msgid 
      from dual;
        RETURN v_msgid;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN NULL;
    END;
 /***************************************************************
  * Description : Proc to save file details of each file created item and Publication.
  * There will be multiple files created with limit to 1000 dicuments ID 
  * or Gtin in each file
  * Author   : Mihir
  ****************************************************************/
PROCEDURE gm_sav_udi_request_xml_dtls(
    p_filenm IN t903_upload_file_list.c903_file_name%TYPE,
    p_msgid  IN t9030_job_activity.c9030_udi_message_id%TYPE,
    p_inputstr IN CLOB,
    p_userid IN t9030_job_activity.c9030_last_updated_by%TYPE,
    p_xmltype IN VARCHAR2,
    p_job_id IN t9030_job_activity.C9030_JOB_ID%TYPE
   )
 AS 
  v_strlen    NUMBER          := NVL (LENGTH (p_inputstr), 0) ;
    v_string    CLOB := p_inputstr;
    v_substring varchar2 (4000) ;
    v_gtin        t2072_di_gudid_status.c2060_di_number%TYPE;
    v_docid       t2072_di_gudid_status.c2072_document_id%TYPE;
    v_filenm      t903_upload_file_list.c903_file_name%TYPE;
    v_msgid       t9030_job_activity.c9030_udi_message_id%TYPE;
    v_itemupdfl   t2072_di_gudid_status.c2072_gdsn_item_upload_fl%TYPE;
    v_intstatus   t2072_di_gudid_status.c901_internal_submit_status%TYPE;
    v_extstatus   t2072_di_gudid_status.c901_external_submit_status%TYPE;
    v_out_job_actid  t9030_job_activity.C9030_JOB_ACTIVITY_ID%TYPE;
    v_out_job_id  t9030_job_activity.C9030_JOB_ID%TYPE;
   v_upload_file_list t903_upload_file_list.c903_upload_file_list%TYPE;
   v_fdapubdt t9030_job_activity.c9030_udi_message_id%TYPE;
   v_jobtypenm VARCHAR2(50);
    v_srctp NUMBER:=NULL;
BEGIN

  IF p_xmltype  = 'ITEMXML' THEN
    v_jobtypenm :='Globus_UDI_1WS_item_Submission_Child';
    v_srctp := 104820;--104820,'FDA: Item XML Submit to 1ws','FDAIXM'   
  
  ELSIF p_xmltype = 'PUBXML' THEN
     v_jobtypenm :='Globus_UDI_1WS_Publication_Submission_Child';
     v_srctp := 104840; --104840,'FDA: Publication XML Submit to 1ws'
  END IF;
        v_out_job_id:=p_job_id;
          --Job info
         gm_pkg_cm_job_system_trans.gm_sav_job_activity(v_jobtypenm,p_filenm,v_srctp,'xml','1',NULL,NULL,p_msgid,p_userid,v_out_job_actid,v_out_job_id);   
         --File info
         gm_pkg_upload_info.gm_sav_upload_info(v_out_job_actid,p_filenm,p_userid,v_srctp,v_upload_file_list); 
 IF v_strlen                      > 0 THEN
 WHILE INSTR (v_string, '|') <> 0
    LOOP
        --
            v_gtin        := NULL;
            v_docid       := NULL;
            v_itemupdfl   := NULL;
            v_intstatus   := NULL;
            v_extstatus   := NULL;
            v_fdapubdt    := NULL;
            v_substring     := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
            v_string        := SUBSTR (v_string, INSTR (v_string, '|') + 1) ;
            v_gtin          := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            v_substring     := SUBSTR (v_substring, INSTR (v_substring, '^') + 1) ;
            v_docid         := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            v_substring     := SUBSTR (v_substring, INSTR (v_substring, '^') + 1) ;
            v_itemupdfl     := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            v_substring     := SUBSTR (v_substring, INSTR (v_substring, '^') + 1) ;
            v_intstatus     := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            v_substring     := SUBSTR (v_substring, INSTR (v_substring, '^') + 1) ;
            v_extstatus     := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            v_substring     := SUBSTR (v_substring, INSTR (v_substring, '^') + 1) ;
             v_fdapubdt    := v_substring;
        
             -- 104840,FDA: Publication XML Submit to 1ws   
            gm_pkg_cm_job_system_trans.gm_sav_job_src_system(v_out_job_actid,v_srctp,v_gtin,v_srctp,v_gtin,p_userid);
          --104824	Internal Status Submitted --104825	Ext Status Submitted Item to 1ws
          gm_sav_gudid_stat_data(v_gtin,v_intstatus,v_extstatus,v_docid,null,null, v_itemupdfl, SYSDATE, null, null, null,null, null, null, p_userid, SYSDATE, v_out_job_actid,null, to_date(v_fdapubdt,GET_RULE_VALUE('DATEFMT', 'DATEFORMAT') ||' HH24:MI:SS'));
     END LOOP;
END IF;
END gm_sav_udi_request_xml_dtls;
 /***************************************************************
  * Description : Procedure is used to save details of guidid submission
  * Author   : Mihir
  ****************************************************************/
procedure gm_sav_gudid_stat_data(
  p_dino   IN t2072_di_gudid_status.c2060_di_number%TYPE,
  p_intstatus   IN t2072_di_gudid_status.c901_internal_submit_status%TYPE,
  p_extstatus   IN t2072_di_gudid_status.c901_external_submit_status%TYPE,
  p_docid   IN t2072_di_gudid_status.c2072_document_id%TYPE,
  p_review_by   IN t2072_di_gudid_status.c2072_reviewed_by%TYPE,
  p_review_dt   IN t2072_di_gudid_status.c2072_reviewed_date%TYPE,
  p_gdsn_item_updfl IN t2072_di_gudid_status.C2072_GDSN_ITEM_UPLOAD_FL%TYPE,
  p_gdsn_item_upddt IN t2072_di_gudid_status.C2072_GDSN_ITEM_UPLOAD_DATE%TYPE,
  p_gdsn_pub_updfl  IN t2072_di_gudid_status.C2072_GDSN_PUBLISH_UPLOAD_FL%TYPE,
  p_gdsn_pub_upddt IN t2072_di_gudid_status.C2072_GDSN_PUBLISH_UPLOAD_DATE%TYPE,
  p_gdsn_cic_updfl IN t2072_di_gudid_status.C2072_GDSN_CIC_FL%TYPE,
  p_gdsn_cic_upddt IN t2072_di_gudid_status.C2072_GDSN_CIC_DATE%TYPE,
  p_gudid_pub_fl   IN t2072_di_gudid_status.c2072_gudid_publish_fl%TYPE,
  p_gudid_pub_dt   IN t2072_di_gudid_status.c2072_gudid_publish_date%TYPE,
  p_userid   IN t2072_di_gudid_status.c2072_last_updated_by%TYPE,
  p_upd_dt   IN t2072_di_gudid_status.c2072_last_updated_date%TYPE,
  p_job_act_id   IN t2072_di_gudid_status.c9030_job_activity_id%TYPE,
  p_void_fl   IN t2072_di_gudid_status.C2072_VOID_FL%TYPE,
  p_fda_pub_dt   IN t2072_di_gudid_status.c2072_last_updated_date%TYPE
 )
AS
BEGIN

UPDATE T2072_DI_GUDID_STATUS
SET C901_INTERNAL_SUBMIT_STATUS    = NVL(p_intstatus,C901_INTERNAL_SUBMIT_STATUS)
, C901_EXTERNAL_SUBMIT_STATUS    = NVL(p_extstatus,C901_EXTERNAL_SUBMIT_STATUS)
, C2072_DOCUMENT_ID              = NVL(p_docid,C2072_DOCUMENT_ID)
, C2072_REVIEWED_BY              = NVL(p_review_by,C2072_REVIEWED_BY)
, C2072_REVIEWED_DATE            = NVL(p_review_dt,C2072_REVIEWED_DATE)
, C2072_GDSN_ITEM_UPLOAD_FL      = NVL(p_gdsn_item_updfl,C2072_GDSN_ITEM_UPLOAD_FL)
, C2072_GDSN_ITEM_UPLOAD_DATE    = NVL(p_gdsn_item_upddt,C2072_GDSN_ITEM_UPLOAD_DATE)
, C2072_GDSN_PUBLISH_UPLOAD_FL   = NVL(p_gdsn_pub_updfl,C2072_GDSN_PUBLISH_UPLOAD_FL)
, C2072_GDSN_PUBLISH_UPLOAD_DATE = NVL(p_gdsn_pub_upddt,C2072_GDSN_PUBLISH_UPLOAD_DATE)
, C2072_GDSN_CIC_FL              = NVL(p_gdsn_cic_updfl,C2072_GDSN_CIC_FL)
, C2072_GDSN_CIC_DATE            = NVL(p_gdsn_cic_upddt,C2072_GDSN_CIC_DATE)
, C2072_GUDID_PUBLISH_FL         = NVL(p_gudid_pub_fl,C2072_GUDID_PUBLISH_FL)
, C2072_GUDID_PUBLISH_DATE       = NVL(p_gudid_pub_dt,C2072_GUDID_PUBLISH_DATE)
, C2072_LAST_UPDATED_BY          = NVL(p_userid,C2072_LAST_UPDATED_BY)
, C2072_LAST_UPDATED_DATE        = NVL(p_upd_dt,C2072_LAST_UPDATED_DATE)
, C9030_JOB_ACTIVITY_ID          = NVL(p_job_act_id,C9030_JOB_ACTIVITY_ID)
, C2072_VOID_FL                  = NVL(p_void_fl,C2072_VOID_FL)
, C2072_FDA_PUBLISH_DATE        = NVL(p_fda_pub_dt,C2072_FDA_PUBLISH_DATE)
WHERE C2060_DI_NUMBER =  p_dino;

 IF (SQL%ROWCOUNT              = 0) THEN
INSERT
INTO
  T2072_DI_GUDID_STATUS
  (
    C2072_DI_GUDID_STATUS_ID,
    C2060_DI_NUMBER,
    C901_INTERNAL_SUBMIT_STATUS,
    C901_EXTERNAL_SUBMIT_STATUS,
    C2072_DOCUMENT_ID,
    C2072_REVIEWED_BY,
    C2072_REVIEWED_DATE,
    C2072_GDSN_ITEM_UPLOAD_FL ,
    C2072_GDSN_ITEM_UPLOAD_DATE,
    C2072_GDSN_PUBLISH_UPLOAD_FL,
    C2072_GDSN_PUBLISH_UPLOAD_DATE,
    C2072_GDSN_CIC_FL,
    C2072_GDSN_CIC_DATE,
    C2072_GUDID_PUBLISH_FL,
    C2072_GUDID_PUBLISH_DATE,
    C2072_LAST_UPDATED_BY,
    C2072_LAST_UPDATED_DATE,
    C9030_JOB_ACTIVITY_ID,
    C2072_FDA_PUBLISH_DATE
  )
  VALUES
  (
    s2072_di_gudid_status.nextval,
    p_dino,
    p_intstatus,
    p_extstatus,
    p_docid,
    p_review_by,
    p_review_dt,
    p_gdsn_item_updfl,
    p_gdsn_item_upddt,
    p_gdsn_pub_updfl,
    p_gdsn_pub_upddt,
    p_gdsn_cic_updfl,
    p_gdsn_cic_upddt,
     p_gudid_pub_fl,
     p_gudid_pub_dt,
     p_userid,
     p_upd_dt,
    p_job_act_id,
    p_fda_pub_dt
  );
END IF;
END gm_sav_gudid_stat_data;

/***************************************************************
  * Description : Proc to save data in Job activity and call related procs based on the input string.
  * There will be multiple files received but each file file will be saved individually
  * Author   : Mihir
  ****************************************************************/
PROCEDURE gm_sav_udi_response_data(
    p_filenm IN t903_upload_file_list.c903_file_name%TYPE,
    p_msgid  IN t9030_job_activity.c9030_udi_message_id%TYPE,
    p_orgmsgid  IN t9030_job_activity.c9030_udi_message_id%TYPE,
    p_inputstrmsgexc IN CLOB,
    p_inputstrstatus IN CLOB,
    p_inputstrdesc IN CLOB,
     p_inputstrsrcsys IN CLOB,
    p_xmltype IN t9030_job_activity.c901_source_type%TYPE,
    p_userid IN t9030_job_activity.c9030_last_updated_by%TYPE
   )
 AS 
  
    v_desc_constant varchar2(100):= 'description code =';
    v_out_job_actid  t9030_job_activity.C9030_JOB_ACTIVITY_ID%TYPE;
    v_out_job_id  t9030_job_activity.C9030_JOB_ID%TYPE;
   v_upload_file_list t903_upload_file_list.c903_upload_file_list%TYPE;
  
BEGIN

  --Save job details
  gm_pkg_cm_job_system_trans.gm_sav_job_activity(get_code_name(p_xmltype),p_filenm,p_xmltype,'xml','1',NULL,NULL,p_msgid,p_userid,v_out_job_actid,v_out_job_id);   
  --Save file info
         gm_pkg_upload_info.gm_sav_upload_info(v_out_job_actid,p_filenm,p_userid,p_xmltype,v_upload_file_list); 
    
    
    --save source system data as key value    
 IF p_inputstrsrcsys IS NOT NULL
  THEN
    gm_sav_src_system_dtls (v_out_job_actid,p_inputstrsrcsys,p_xmltype,p_userid);
   END IF;
  
  --save error details in descrition
   IF p_inputstrdesc IS NOT NULL
    THEN
    gm_sav_desc_err_dtls (v_out_job_actid,p_inputstrdesc,p_xmltype,v_desc_constant,p_userid,null);
  END IF;
  
  --save message exception failure
   IF p_inputstrmsgexc IS NOT NULL
    THEN
        --Log Description for error code and Mark all GTIN with original message id to error state
        gm_sav_msg_exc_desc_err_dtls (v_out_job_actid,p_inputstrmsgexc,p_xmltype,v_desc_constant,p_orgmsgid,p_userid);
    END IF;
  
  --Save the status of each ID
   IF p_inputstrstatus IS NOT NULL
  THEN
    gm_sav_status_dtls(v_out_job_actid,p_inputstrstatus,p_xmltype,p_userid);
    -- gm_procedure_log ('gm_sav_status_dtls',p_inputstrstatus);
  END IF;
END gm_sav_udi_response_data;
/***************************************************************
  * Description : Proc to save data in source system table.
  * Author   : Mihir
  ****************************************************************/
PROCEDURE gm_sav_src_system_dtls(
        p_job_actid IN  t9030_job_activity.C9030_JOB_ACTIVITY_ID%TYPE,
        p_inputstrsrcsys IN CLOB,
        p_xmltype IN t9030_job_activity.c901_source_type%TYPE,
        p_userid IN t9030_job_activity.c9030_last_updated_by%TYPE
)
 AS 
   v_strlen    NUMBER          := NVL (LENGTH (p_inputstrsrcsys), 0) ;
    v_string    CLOB := p_inputstrsrcsys;
    v_substring varchar2 (4000) ;
    v_gtin        t2072_di_gudid_status.c2060_di_number%TYPE;
    v_key       t9031_job_source_system.C9031_ATTRIBUTE_TYPE%TYPE;
    v_value      t9031_job_source_system.C9031_ATTRIBUTE_VALUE%TYPE;
    
   
   
BEGIN
IF v_strlen                      > 0 THEN
  WHILE INSTR (v_string, '|') <> 0
    LOOP
        --
            v_key        := NULL;
            v_value       := NULL;
            v_gtin   := NULL;
            
            v_substring     := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
            v_string        := SUBSTR (v_string, INSTR (v_string, '|') + 1) ;
            v_key          := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            v_substring     := SUBSTR (v_substring, INSTR (v_substring, '^') + 1) ;
            v_value         := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            v_substring     := SUBSTR (v_substring, INSTR (v_substring, '^') + 1) ;
             v_gtin     := v_substring;
            
            IF v_key IS NOT NULL
            THEN
                gm_pkg_cm_job_system_trans.gm_sav_job_src_system(p_job_actid,v_key,v_value,p_xmltype,v_gtin,p_userid);
            END IF;
        
     END LOOP;
    END IF;
END gm_sav_src_system_dtls;
/***************************************************************
  * Description : Proc to save description detail and error code.
  *  * Author   : Mihir
  ****************************************************************/
PROCEDURE gm_sav_desc_err_dtls(
        p_job_actid IN  t9030_job_activity.C9030_JOB_ACTIVITY_ID%TYPE,
        p_inputstrdesc IN CLOB,
        p_xmltype IN t9030_job_activity.c901_source_type%TYPE,
        p_desc_constant varchar2,
        p_userid IN t9030_job_activity.c9030_last_updated_by%TYPE,
        p_gtin IN t9033_job_error_event.C9033_REF_ID%TYPE
)
 AS 
   v_strlen    NUMBER          := NVL (LENGTH (p_inputstrdesc), 0) ;
    v_string    CLOB := p_inputstrdesc;
    v_substring varchar2 (4000) ;
    v_gtin        t2072_di_gudid_status.c2060_di_number%TYPE;
    v_key       t9031_job_source_system.C9031_ATTRIBUTE_TYPE%TYPE;
    v_value      t9031_job_source_system.C9031_ATTRIBUTE_VALUE%TYPE;
    v_screennm t9032_job_screen_info.C9032_SCREEN_NAME%TYPE;
      v_out_jobsreenid  T9032_JOB_SCREEN_INFO.C9032_JOB_SCREEN_ID%TYPE;
    v_out_screenid T9032_JOB_SCREEN_INFO.C9032_SCREEN_ID%TYPE;
      v_job_screen_id T9032_JOB_SCREEN_INFO.C9032_JOB_SCREEN_ID%TYPE;

 BEGIN
IF v_strlen                      > 0 THEN
  WHILE INSTR (v_string, '|') <> 0
    LOOP
        --
            v_key        := NULL;
            v_value       := NULL;
            v_gtin   := NULL;
            
            v_substring     := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
            v_string        := SUBSTR (v_string, INSTR (v_string, '|') + 1) ;
            v_key          := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            v_substring     := SUBSTR (v_substring, INSTR (v_substring, '^') + 1) ;
            v_value         := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            v_substring     := SUBSTR (v_substring, INSTR (v_substring, '^') + 1) ;
             v_gtin     := v_substring;
             --Added for Message exception where GTIN IS NULL
             --104862 is Message Exception
             IF p_xmltype =104862
             THEN
                v_gtin:=p_gtin;
             END IF;
             --As of now Key is NOT null but if more parameter are added in future , same porc can be reused
             IF v_key IS NOT NULL
            THEN
                gm_pkg_cm_job_system_trans.gm_sav_job_src_system(p_job_actid,p_desc_constant||v_key,v_value,p_xmltype,v_gtin,p_userid);
                            
            END IF;
            
    
                -- gm_pkg_cm_job_system_trans.gm_sav_job_screen_info('Hard','1Ws Error',v_key,v_value,'','','',p_userid,v_out_jobsreenid,v_out_screenid);
                 --104923 WS error ,104920 Hard
                  gm_pkg_cm_job_system_trans.gm_sav_job_screen_info(104920,104923,v_key,v_value,'','','',p_userid,v_out_jobsreenid,v_out_screenid);
                   --Put the reference of  the error in error event 104900 open
                 gm_pkg_cm_job_system_trans.gm_sav_job_error_event(SYSDATE,v_out_jobsreenid,p_job_actid,p_xmltype,v_gtin,104900);
          
       END LOOP;
    END IF;
END gm_sav_desc_err_dtls;

/***************************************************************
  * Description : Proc to save description for Message Exception
  *  * Author   : Mihir
  *Since in Message Exception we dont have a GTIN, we find all the gtin in that file using the
  *Originating Message Id ,Then for each GTIN we associate all the Message Description(Multiple)
  *and finally Change the status for all gtins to Rework
  ****************************************************************/
PROCEDURE gm_sav_msg_exc_desc_err_dtls(
        p_job_actid IN  t9030_job_activity.C9030_JOB_ACTIVITY_ID%TYPE,
        p_inputstrdesc IN CLOB,
        p_xmltype IN t9030_job_activity.c901_source_type%TYPE,
        p_desc_constant varchar2,
        p_orgmsgid  IN t9030_job_activity.c9030_udi_message_id%TYPE,
        p_userid IN t9030_job_activity.c9030_last_updated_by%TYPE
)
 AS 
    v_job_act_id  T9033_JOB_ERROR_EVENT.C9030_JOB_ACTIVITY_ID%TYPE;
     CURSOR msg_exc_gtin_cur IS
       SELECT C9031_REF_ID refid,C901_REF_TYPE reftype
       FROM T9031_JOB_SOURCE_SYSTEM
       WHERE C9030_JOB_ACTIVITY_ID = v_job_act_id;
           v_error_tp NUMBER:=104827;--104827 Rework Item
   BEGIN
    BEGIN
            SELECT C9030_JOB_ACTIVITY_ID
            INTO v_job_act_id
            FROM t9030_job_activity
            WHERE C9030_UDI_MESSAGE_ID = p_orgmsgid;
    EXCEPTION WHEN NO_DATA_FOUND
            THEN
             v_job_act_id := NULL;
             --raise_application_error('-20500','Cannot find the Originating message Id.');
      END; 
      
    FOR  msg_exc_gtin IN msg_exc_gtin_cur
    LOOP
        IF msg_exc_gtin.reftype = 104840 --FDA:Publication XML Submit to 1WS
        THEN
            v_error_tp:=104831; --Rework Publication from 1WS
        END IF;
        --for each GTIN we associate all the Message Description(Multiple)
         gm_sav_desc_err_dtls (p_job_actid,p_inputstrdesc,p_xmltype,p_desc_constant,p_userid,msg_exc_gtin.refid);
        --Change Status based on received files 
          gm_sav_gudid_stat_data(msg_exc_gtin.refid,NULL,v_error_tp,p_orgmsgid,null,null, NULL, NULL, NULL, NULL, NULL,NULL, NULL, NULL, p_userid, SYSDATE, p_job_actid,null,null);
    END LOOP;

END gm_sav_msg_exc_desc_err_dtls;

/***************************************************************
  * Description : Proc to save status of each response file.
  * There will be multiple files received but each file file will be saved individually
  * Author   : Mihir
  ****************************************************************/
PROCEDURE gm_sav_status_dtls(
        p_job_actid IN  t9030_job_activity.C9030_JOB_ACTIVITY_ID%TYPE,
        p_inputstrstatus IN CLOB,
        p_xmltype IN t9030_job_activity.c901_source_type%TYPE,
        p_userid IN t9030_job_activity.c9030_last_updated_by%TYPE
)
 AS 
  v_strlen    NUMBER          := NVL (LENGTH (p_inputstrstatus), 0) ;
    v_string    CLOB := p_inputstrstatus;
    v_substring varchar2 (4000) ;
    v_gtin        t2072_di_gudid_status.c2060_di_number%TYPE;
    v_docid       t2072_di_gudid_status.c2072_document_id%TYPE;
    v_itemupdfl   t2072_di_gudid_status.c2072_gdsn_item_upload_fl%TYPE;
    v_pubupdfl    t2072_di_gudid_status.C2072_GDSN_PUBLISH_UPLOAD_FL%TYPE;
    v_cicupdfl t2072_di_gudid_status.C2072_GDSN_CIC_FL%TYPE;
    v_gudidpubupdfl   t2072_di_gudid_status.c2072_gudid_publish_fl%TYPE;
     v_intstatus   t2072_di_gudid_status.c901_internal_submit_status%TYPE;
    v_extstatus   t2072_di_gudid_status.c901_external_submit_status%TYPE;
    v_item_date t2072_di_gudid_status.c2072_last_updated_date%TYPE;
    v_pub_date t2072_di_gudid_status.c2072_last_updated_date%TYPE;
    v_cic_date t2072_di_gudid_status.c2072_last_updated_date%TYPE;
    v_gudid_date t2072_di_gudid_status.c2072_last_updated_date%TYPE;
      
   
BEGIN


   IF v_strlen > 0 THEN
  WHILE INSTR (v_string, '|') <> 0
    LOOP
        --
            v_item_date:=NULL;
            v_pub_date:=NULL;
            v_cic_date:=NULL;
            v_gudid_date:=NULL;
            --
            v_gtin        := NULL;
            v_docid       := NULL;
            v_itemupdfl   := NULL;
            v_pubupdfl   := NULL;
            v_cicupdfl   := NULL;
            v_gudidpubupdfl   := NULL;
            v_intstatus   := NULL;
            v_extstatus   := NULL;
            v_substring     := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
            v_string        := SUBSTR (v_string, INSTR (v_string, '|') + 1) ;
            v_gtin          := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            v_substring     := SUBSTR (v_substring, INSTR (v_substring, '^') + 1) ;
            v_docid         := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            v_substring     := SUBSTR (v_substring, INSTR (v_substring, '^') + 1) ;
            v_itemupdfl     := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
             v_substring     := SUBSTR (v_substring, INSTR (v_substring, '^') + 1) ;
            v_pubupdfl     := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
             v_substring     := SUBSTR (v_substring, INSTR (v_substring, '^') + 1) ;
            v_cicupdfl     := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
             v_substring     := SUBSTR (v_substring, INSTR (v_substring, '^') + 1) ;
            v_gudidpubupdfl     := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            v_substring     := SUBSTR (v_substring, INSTR (v_substring, '^') + 1) ;
            v_intstatus     := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            v_substring     := SUBSTR (v_substring, INSTR (v_substring, '^') + 1) ;
            v_extstatus    := v_substring;
            
           IF v_itemupdfl IS NOT NULL
           THEN
            v_item_date:=SYSDATE;
           END IF;
           
            IF v_pubupdfl IS NOT NULL
           THEN
            v_pub_date:=SYSDATE;
           END IF;
           
            IF v_cicupdfl IS NOT NULL
           THEN
            v_cic_date:=SYSDATE;
           END IF;
           
            IF v_gudidpubupdfl IS NOT NULL
           THEN
            v_gudid_date:=SYSDATE;
           END IF;
        
          --Change Status based on received files
          gm_sav_gudid_stat_data(v_gtin,v_intstatus,v_extstatus,v_docid,null,null, v_itemupdfl, v_item_date, v_pubupdfl, v_pub_date, v_cicupdfl,v_cic_date, v_gudidpubupdfl, v_gudid_date, p_userid, SYSDATE, p_job_actid,null,null);
     END LOOP;
END IF;
END gm_sav_status_dtls;

/***************************************************************
  * Description : Procedure is used to save details of guidid submission log
  * Author   : Mihir
  ****************************************************************/
procedure gm_sav_gudid_stat_log(
  p_statusid   IN t2072_di_gudid_status.C2072_DI_GUDID_STATUS_ID%TYPE,
  p_dino   IN t2072_di_gudid_status.c2060_di_number%TYPE,
  p_intstatus   IN t2072_di_gudid_status.c901_internal_submit_status%TYPE,
  p_extstatus   IN t2072_di_gudid_status.c901_external_submit_status%TYPE,
  p_docid   IN t2072_di_gudid_status.c2072_document_id%TYPE,
  p_review_by   IN t2072_di_gudid_status.c2072_reviewed_by%TYPE,
  p_review_dt   IN t2072_di_gudid_status.c2072_reviewed_date%TYPE,
  p_gdsn_item_updfl IN t2072_di_gudid_status.C2072_GDSN_ITEM_UPLOAD_FL%TYPE,
  p_gdsn_item_upddt IN t2072_di_gudid_status.C2072_GDSN_ITEM_UPLOAD_DATE%TYPE,
  p_gdsn_pub_updfl  IN t2072_di_gudid_status.C2072_GDSN_PUBLISH_UPLOAD_FL%TYPE,
  p_gdsn_pub_upddt IN t2072_di_gudid_status.C2072_GDSN_PUBLISH_UPLOAD_DATE%TYPE,
  p_gdsn_cic_updfl IN t2072_di_gudid_status.C2072_GDSN_CIC_FL%TYPE,
  p_gdsn_cic_upddt IN t2072_di_gudid_status.C2072_GDSN_CIC_DATE%TYPE,
  p_gudid_pub_fl   IN t2072_di_gudid_status.c2072_gudid_publish_fl%TYPE,
  p_gudid_pub_dt   IN t2072_di_gudid_status.c2072_gudid_publish_date%TYPE,
  p_userid   IN t2072_di_gudid_status.c2072_last_updated_by%TYPE,
  p_upd_dt   IN t2072_di_gudid_status.c2072_last_updated_date%TYPE,
  p_job_act_id   IN t2072_di_gudid_status.c9030_job_activity_id%TYPE,
  p_void_fl   IN t2072_di_gudid_status.C2072_VOID_FL%TYPE,
  p_fda_pub_dt   IN t2072_di_gudid_status.c2072_last_updated_date%TYPE,
  p_src_act_id IN t2072_di_gudid_status.c9030_source_activity_id%TYPE
 )
AS
BEGIN

INSERT
INTO
  T2072_DI_GUDID_STATUS_LOG
  (
    C2072_DI_GUDID_STATUS_LOG_ID,
    C2072_DI_GUDID_STATUS_ID,
    C2060_DI_NUMBER,
    C901_INTERNAL_SUBMIT_STATUS,
    C901_EXTERNAL_SUBMIT_STATUS,
    C2072_DOCUMENT_ID,
    C2072_REVIEWED_BY,
    C2072_REVIEWED_DATE,
    C2072_GDSN_ITEM_UPLOAD_FL ,
    C2072_GDSN_ITEM_UPLOAD_DATE,
    C2072_GDSN_PUBLISH_UPLOAD_FL,
    C2072_GDSN_PUBLISH_UPLOAD_DATE,
    C2072_GDSN_CIC_FL,
    C2072_GDSN_CIC_DATE,
    C2072_GUDID_PUBLISH_FL,
    C2072_GUDID_PUBLISH_DATE,
    C2072_LAST_UPDATED_BY,
    C2072_LAST_UPDATED_DATE,
    C9030_JOB_ACTIVITY_ID,
    C2072_VOID_FL,
    C2072_FDA_PUBLISH_DATE,
    C9030_SOURCE_ACTIVITY_ID
  )
  VALUES
  (
    s2072_DI_GUDID_STATUS_LOG.nextval,
    p_statusid,
    p_dino,
    p_intstatus,
    p_extstatus,
    p_docid,
    p_review_by,
    p_review_dt,
    p_gdsn_item_updfl,
    p_gdsn_item_upddt,
    p_gdsn_pub_updfl,
    p_gdsn_pub_upddt,
    p_gdsn_cic_updfl,
    p_gdsn_cic_upddt,
     p_gudid_pub_fl,
     p_gudid_pub_dt,
     p_userid,
     p_upd_dt,
    p_job_act_id,
    p_void_fl,
    p_fda_pub_dt,
    p_src_act_id
  );

END gm_sav_gudid_stat_log;
/***************************************************************
* Description : Procedure is used to save details of attribute count wip details
* Author   : Mihir
****************************************************************/
PROCEDURE gm_sav_attribute_count_wip (
        p_udi_attr_cnt_wip_id    IN OUT t2071_udi_attribute_count_wip.c2071_udi_attrb_count_wip_id%TYPE,
        p_part_number            IN t2071_udi_attribute_count_wip.c205_part_number_id%TYPE,
        p_di_number              IN t2071_udi_attribute_count_wip.c2060_di_number%TYPE,
        p_attribute_cnt          IN t2071_udi_attribute_count_wip.c2071_attribute_count%TYPE,
        p_attribute_needed_cnt   IN t2071_udi_attribute_count_wip.c2071_attribute_needed_count%TYPE,
        p_modified_cnt           IN t2071_udi_attribute_count_wip.c2071_modified_count%TYPE,
        p_valid_error_cnt        IN t2071_udi_attribute_count_wip.c2071_validation_error_count%TYPE,
        p_is_all_data_availab_fl IN t2071_udi_attribute_count_wip.c2071_is_all_data_available%TYPE,
        p_job_activity_id        IN t2071_udi_attribute_count_wip.c9030_job_activity_id%TYPE,
        p_is_ready_for_valid_fl  IN t2071_udi_attribute_count_wip.c2071_is_ready_for_validation%TYPE,
        p_user_id                IN t2071_udi_attribute_count_wip.c2071_last_updated_by%TYPE)
AS
    v_udi_attr_cnt_wip_id t2071_udi_attribute_count_wip.c2071_udi_attrb_count_wip_id%TYPE;
BEGIN
    --
    v_udi_attr_cnt_wip_id := p_udi_attr_cnt_wip_id;
    --
     UPDATE t2071_udi_attribute_count_wip
    SET c205_part_number_id              = p_part_number, c2060_di_number = p_di_number, c2071_attribute_count = p_attribute_cnt
      , c2071_attribute_needed_count     = p_attribute_needed_cnt, c2071_modified_count = p_modified_cnt,
        c2071_validation_error_count     = p_valid_error_cnt, c2071_is_all_data_available = p_is_all_data_availab_fl,
        c9030_job_activity_id            = p_job_activity_id, c2071_is_ready_for_validation = p_is_ready_for_valid_fl,
        c2071_last_updated_by            = p_user_id, c2071_last_updated_date = SYSDATE
      WHERE c2071_udi_attrb_count_wip_id = v_udi_attr_cnt_wip_id;
    --
    IF (SQL%ROWCOUNT = 0) THEN
        --
         SELECT s2071_udi_attrb_count_wip_id.nextval
           INTO v_udi_attr_cnt_wip_id
           FROM dual;
        --
         INSERT
           INTO T2071_UDI_ATTRIBUTE_COUNT_WIP
            (
                c2071_udi_attrb_count_wip_id, c205_part_number_id, c2060_di_number
              , c2071_attribute_count, c2071_attribute_needed_count, c2071_modified_count
              , c2071_validation_error_count, c2071_is_all_data_available, c9030_job_activity_id
              , c2071_is_ready_for_validation, c2071_last_updated_by, c2071_last_updated_date
            )
            VALUES
            (
                v_udi_attr_cnt_wip_id, p_part_number, p_di_number
              , p_attribute_cnt, p_attribute_needed_cnt, p_modified_cnt
              , p_valid_error_cnt, p_is_all_data_availab_fl, p_job_activity_id
              , p_is_ready_for_valid_fl, p_user_id, SYSDATE
            ) ;
    END IF;
    --
    p_udi_attr_cnt_wip_id := v_udi_attr_cnt_wip_id;
END gm_sav_attribute_count_wip;
END gm_pkg_pd_udi_submission_trans;
/
