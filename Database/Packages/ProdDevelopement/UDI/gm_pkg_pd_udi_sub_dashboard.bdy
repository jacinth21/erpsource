/* Formatted on 2009/02/16 12:30 (Formatter Plus v4.8.0) */
--@"C:\Database\Packages\ProdDevelopement\udi\gm_pkg_pd_udi_sub_dashboard.bdy"
CREATE OR REPLACE
PACKAGE body gm_pkg_pd_udi_sub_dashboard
IS
    /***************************************************************
    * Description : Procedure to fetch missing attribute details
    ****************************************************************/
PROCEDURE gm_fch_wip_missing_attr_dtls (
        p_partnum t205_part_number.c205_part_number_id%TYPE,
        p_out_missing_attr_cur OUT TYPES.cursor_type)
AS
BEGIN
    OPEN p_out_missing_attr_cur FOR SELECT v2071_dtls.C205_PART_NUMBER_ID part_number, v2071_dtls.C2060_DI_NUMBER
    di_number, v2071_dtls.C901_ATTRIBUTE_TYPE attr_type, v2071_dtls.C901_ATTRIBUTE_TYPE_NAME attr_type_nm, DECODE (
    v2071_dtls.REQUIRED_COUNT, 1, 'Y', 'N') req_count, DECODE (v2071_dtls.AVAILABLE_COUNT, 1, 'Y', 'N') avai_count FROM
    v2071_udi_wip_details v2071_dtls WHERE v2071_dtls.c205_part_number_id = p_partnum
    AND v2071_dtls.AVAILABLE_COUNT = 0;
END gm_fch_wip_missing_attr_dtls;
/***************************************************************
* Description : Procedure used to update the GUDID stauts
****************************************************************/
PROCEDURE gm_upd_gudid_status_dtls (
        p_inputstr IN CLOB,
        p_userid   IN t2072_di_gudid_status.c2072_last_updated_by%TYPE)
AS
    v_strlen NUMBER := NVL (LENGTH (p_inputstr), 0) ;
    v_string CLOB   := p_inputstr;
    v_substring VARCHAR2 (4000) ;
    --
    v_di_number t2072_di_gudid_status.c2060_di_number%TYPE;
BEGIN
    IF v_strlen                      > 0 THEN
        WHILE INSTR (v_string, '|') <> 0
        LOOP
            --
            v_di_number := NULL;
            v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
            v_string    := SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
            --
             UPDATE t2072_di_gudid_status
            SET c901_internal_submit_status = 4000806, -- Reviewed
                c2072_reviewed_by           = p_userid, c2072_reviewed_date = SYSDATE, c2072_last_updated_by = p_userid
              , c2072_last_updated_date     = SYSDATE
              WHERE c2060_di_number         = v_substring
                AND c2072_void_fl          IS NULL;
            --
        END LOOP;
    END IF;
END gm_upd_gudid_status_dtls;
/***************************************************************
* Description : Procedure used to update the Error report stauts
****************************************************************/
PROCEDURE gm_upd_error_report_dtls (
        p_inputstr IN CLOB,
        p_userid   IN t9030_job_activity.c9030_last_updated_by%TYPE)
AS
    v_strlen NUMBER := NVL (LENGTH (p_inputstr), 0) ;
    v_string CLOB   := p_inputstr;
    v_substring VARCHAR2 (4000) ;
    v_di_number t2060_di_part_mapping.C2060_DI_NUMBER%TYPE;
    v_error_count NUMBER;
    --
BEGIN
    IF v_strlen                      > 0 THEN
        WHILE INSTR (v_string, '|') <> 0
        LOOP
            v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
            v_string    := SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
                       --
             UPDATE T9033_JOB_ERROR_EVENT
            SET c901_status      = 104901
              WHERE c9033_job_error_event_id = v_substring;
             
             SELECT C9033_REF_ID INTO v_di_number 
             FROM   T9033_JOB_ERROR_EVENT WHERE c9033_job_error_event_id = v_substring;  
             
             SELECT COUNT(1) INTO v_error_count
             FROM T9033_JOB_ERROR_EVENT WHERE C9033_REF_ID=v_di_number
             AND c901_status      <>104901; -- Close
             
             IF v_error_count=0 THEN
             gm_pkg_pd_udi_sub_dashboard.gm_upd_status_when_error_close(v_di_number,p_userid);
              END IF;
                           --
        END LOOP;
    END IF;
END gm_upd_error_report_dtls;
/***************************************************************
* Description : Procedure to fetch DI Number attribute details
****************************************************************/
PROCEDURE gm_fch_di_number_attr_dtls (
        p_di_number t2060_di_part_mapping.c2060_di_number%TYPE,
        p_out_di_num_attr_cur OUT TYPES.cursor_type)
AS
BEGIN
    OPEN p_out_di_num_attr_cur FOR SELECT v2071a.PART_NUMBER part_number,
    v2071a.DI di_number,
    v2071a.ATTRIBUTE_NAME attr_type_nm,
    v2071a.ATTRIBUTE_VALUE attr_value,
    v2071a.TEAM rule_value,
    v2071a.ATTRIBUTE_NAME attr_type FROM V2071A_UDI_ATTRI_DETAILS v2071a
    WHERE v2071a.DI = p_di_number ORDER BY v2071a.TEAM,
    v2071a.ATTRIBUTE_NAME;
END gm_fch_di_number_attr_dtls;

/***************************************************************
* Description : Procedure used to update the Error report stauts
****************************************************************/
PROCEDURE gm_upd_status_when_error_close (
            p_di_number IN T2060_DI_PART_MAPPING.C2060_DI_NUMBER%TYPE,
            p_userid   IN t9030_job_activity.c9030_last_updated_by%TYPE)
AS
    v_di_number VARCHAR(20):=p_di_number;
    v_change_count NUMBER;
    v_part_number T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE;
    
   --
BEGIN

SELECT C205_PART_NUMBER_ID INTO v_part_number 
             FROM   T2060_DI_PART_MAPPING WHERE C2060_DI_NUMBER = v_di_number; 
             
    SELECT COUNT(1) INTO v_change_count FROM(
      SELECT C205_PART_NUMBER_ID PNUM, C901_ATTRIBUTE_TYPE ATTTYPE, C2070A_ATTRIBUTE_VALUE ATTVALUE
      FROM T2070A_UDI_PREV_DATA_LOAD
      WHERE C205_PART_NUMBER_ID=v_part_number
 MINUS
      SELECT T205D.C205_PART_NUMBER_ID,
             T205D.C901_ATTRIBUTE_TYPE,
             T205D.C205D_ATTRIBUTE_VALUE
             FROM T205D_PART_ATTRIBUTE T205D
             WHERE T205D.C901_ATTRIBUTE_TYPE in (104463, 103785, 4000539, 4000542 , 4000546 , 103342 , 103343 , 103344 , 4000543 , 4000544 , 4000545 , 103780 , 4000547 , 4000548 , 103345 , 104655 , 103346 , 103782 , 103783 , 103784 , 103347 , 103786 , 104460 , 103341 , 103348 , 104461 , 104462,104640,104701,104643,104644,104645,104646,104647,104648,104649,104650,104651,104652,104656,104657,104658,104659,104660,104661,104662,104663,104664,104665,104666,104667,104668,104669,104670,104671,104672,104673,104674,104675,104677,104468,104472,104476,104464,104466,104474,104470,104478,104475,104467,104479,104471,104469,104477,104473,104465)
      and T205D.C205D_ATTRIBUTE_VALUE is not null
      and T205D.C205D_VOID_FL is null
      AND T205D.C205_PART_NUMBER_ID=v_part_number);

IF v_change_count=0 THEN
            UPDATE t2072_di_gudid_status
            SET c901_internal_submit_status = 104822 -- Review-In Progress
                ,C901_EXTERNAL_SUBMIT_STATUS=NULL
                ,c2072_last_updated_by = p_userid
              , c2072_last_updated_date     = SYSDATE
              WHERE c2060_di_number         = v_di_number
                AND c2072_void_fl          IS NULL;
        ELSE
          UPDATE t2072_di_gudid_status
            SET c901_internal_submit_status = 104821 -- WIP
                ,C901_EXTERNAL_SUBMIT_STATUS=NULL
                ,c2072_last_updated_by = p_userid
              , c2072_last_updated_date     = SYSDATE
              WHERE c2060_di_number         = v_di_number
                AND c2072_void_fl          IS NULL;
  END IF;
END gm_upd_status_when_error_close;
END gm_pkg_pd_udi_sub_dashboard;
/