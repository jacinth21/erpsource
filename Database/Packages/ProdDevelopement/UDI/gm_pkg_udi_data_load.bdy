-- @"C:\jbossbranch\Database\Packages\ProdDevelopement\UDI\gm_pkg_udi_data_load.bdy"
create or replace PACKAGE BODY gm_pkg_udi_data_load
IS


v_job_activity_id T9033_JOB_ERROR_EVENT.C9030_JOB_ACTIVITY_ID%TYPE;
v_user_id VARCHAR2(20) := '30301';

/*
exec gm_pkg_udi_data_load.gm_data_load_main

Pre-req : Part / DI should be loaded into 
1. T2071_UDI_ATTRIBUTE_COUNT_WIP
2. T2072_DI_GUDID_STATUS

*/

PROCEDURE gm_data_load_main
AS
v_last_run_date T2070_UDI_DATA_LOAD.C2070_LAST_UPDATED_DATE%TYPE;
v_dummy_count NUMBER;
BEGIN
            
      /*to get max date Added by Paddy*/
       SELECT   Max(C2070_LAST_UPDATED_DATE)
            INTO v_last_run_date
       FROM GLOBUS_APP.T2070_UDI_DATA_LOAD;
      
      -- delta load
      gm_pkg_udi_data_load.gm_sav_CurrentLoad;
      gm_pkg_udi_data_load.gm_sav_UDIDataLoad;
      gm_pkg_udi_data_load.gm_sav_del_attr_udiload;
      gm_pkg_udi_data_load.gm_sav_postDeltaLoad;
      
      -- populate pre-verification data
      gm_pkg_udi_data_load.gm_upd_is_data_available(v_last_run_date);
      
      SELECT COUNT(1) INTO v_dummy_count
      FROM T2070_UDI_DATA_LOAD
      WHERE (C205_PART_NUMBER_ID IS NULL AND C2060_DI_NUMBER IS NULL);
      
      IF (v_dummy_count<>1) THEN -- to not run in 1st time
            gm_pkg_udi_data_load.gm_upd_modified_count(v_last_run_date);
      END IF;
      gm_pkg_udi_data_load.gm_upd_validation_flag;
      
      -- validation
      gm_pkg_udi_data_load.gm_pre_validation_setup;
      gm_pkg_udi_data_load.gm_load_source_data(v_last_run_date);
      gm_pkg_udi_v_1ws_fdaudi.gm_fda_udi_validation_main(v_job_activity_id);
      
      COMMIT;

END gm_data_load_main;

/*
1. The initial process bulk loads into the T2070B_UDI_CURR_DATA_LOAD. Since change detection is irrelevant during the initial load, the data continues on to be transformed and loaded into T2070_UDI_DATA_LOAD
2. When the process is complete, it drops the T2070A_UDI_PREV_DATA_LOAD, renames the T2070B_UDI_CURR_DATA_LOAD to T2070A_UDI_PREV_DATA_LOAD, and creates an empty T2070B_UDI_CURR_DATA_LOAD. Since none of these tasks involve database logging, they are very fast!
3. The next time the load process is run, the T2070B_UDI_CURR_DATA_LOAD is populated.
*/
PROCEDURE gm_sav_CurrentLoad
AS

BEGIN

EXECUTE IMMEDIATE 'TRUNCATE TABLE T2070B_UDI_CURR_DATA_LOAD';

INSERT  INTO T2070B_UDI_CURR_DATA_LOAD (C2070B_UDI_DATA_LOAD_ID
,C205_PART_NUMBER_ID
,C2060_DI_NUMBER
,C901_ATTRIBUTE_TYPE
,C2070B_ATTRIBUTE_VALUE
,C2070B_LAST_UPDATED_BY
,C2070B_LAST_UPDATED_DATE)
select T205D.C2051_PART_ATTRIBUTE_ID, -- tochange : use attribvute tabke seq #
       T205D.C205_PART_NUMBER_ID,
       T2060.C2060_DI_NUMBER,
       T205D.C901_ATTRIBUTE_TYPE,
       T205D.C205D_ATTRIBUTE_VALUE,
       NVL(T205D.C205D_LAST_UPDATED_BY , T205D.C205D_CREATED_BY) ,
       NVL(T205D.C205D_LAST_UPDATED_DATE , T205D.C205D_CREATED_DATE)
       from T205D_PART_ATTRIBUTE T205D, T2060_DI_PART_MAPPING T2060
where T205D.C205_PART_NUMBER_ID = T2060.C205_PART_NUMBER_ID -- so only parts with DI , In-use will be pulled up
and T205D.C901_ATTRIBUTE_TYPE in (104463, 103785, 4000539, 4000542 , 4000546 , 103342 , 103343 , 103344 , 4000543 , 4000544 , 4000545 , 103780 , 4000547 , 4000548 , 103345 , 104655 , 103346 , 103782 , 103783 , 103784 , 103347 , 103786 , 104460 , 103341 , 103348 , 104461 , 104462,104640,104701,104643,104644,104645,104646,104647,104648,104649,104650,104651,104652,104656,104657,104658,104659,104660,104661,104662,104663,104664,104665,104666,104667,104668,104669,104670,104671,104672,104673,104674,104675,104677,104468,104472,104476,104464,104466,104474,104470,104478,104475,104467,104479,104471,104469,104477,104473,104465)
--and T205D.C205_PART_NUMBER_ID IN ('148.132','148.134')
and T205D.C205D_ATTRIBUTE_VALUE is not null
and T205D.C205D_VOID_FL is null
AND T2060.C901_STATUS = 103392; -- InUse
--AND T2060.C2060_SET_UUID = '1'; -- #TOREMOVE


END gm_sav_CurrentLoad;

/*
Select the T2070B_UDI_CURR_DATA_LOAD MINUS the T2070A_UDI_PREV_DATA_LOAD. Transform and load the result set into T2070_UDI_DATA_LOAD
*/
PROCEDURE gm_sav_UDIDataLoad
as

v_part_number T2070B_UDI_CURR_DATA_LOAD.C205_PART_NUMBER_ID%TYPE;
v_di_number T2070B_UDI_CURR_DATA_LOAD.C2060_DI_NUMBER%TYPE;
v_att_type T2070B_UDI_CURR_DATA_LOAD.C901_ATTRIBUTE_TYPE%TYPE;
v_att_value T2070B_UDI_CURR_DATA_LOAD.C2070B_ATTRIBUTE_VALUE%TYPE;
v_last_updated_by T2070B_UDI_CURR_DATA_LOAD.C2070B_LAST_UPDATED_BY%TYPE;

CURSOR cur_curr_minus_prev IS
select C205_PART_NUMBER_ID PNUM, C2060_DI_NUMBER DINUM, C901_ATTRIBUTE_TYPE ATTTYPE, C2070B_ATTRIBUTE_VALUE ATTVALUE
from T2070B_UDI_CURR_DATA_LOAD
minus
select C205_PART_NUMBER_ID, C2060_DI_NUMBER, C901_ATTRIBUTE_TYPE, C2070A_ATTRIBUTE_VALUE
from T2070A_UDI_PREV_DATA_LOAD;
/* Last_updated_by removed from minus since some scenario there will same value on attr_value but updated by getting modified.*/
BEGIN

FOR curr_minus_prev IN cur_curr_minus_prev
LOOP

v_part_number := curr_minus_prev.PNUM;
v_di_number := curr_minus_prev.DINUM;
v_att_type := curr_minus_prev.ATTTYPE;
v_att_value := curr_minus_prev.ATTVALUE;
--v_last_updated_by := curr_minus_prev.LUBY;

UPDATE T2070_UDI_DATA_LOAD
SET C2070_ATTRIBUTE_VALUE = v_att_value
--, C2070_LAST_UPDATED_BY = v_last_updated_by
, C2070_LAST_UPDATED_DATE = SYSDATE
WHERE C2060_DI_NUMBER = v_di_number
AND C901_ATTRIBUTE_TYPE = v_att_type;

IF (SQL%ROWCOUNT = 0 )
THEN
INSERT INTO T2070_UDI_DATA_LOAD (C2070_UDI_DATA_LOAD_ID
,C205_PART_NUMBER_ID
,C2060_DI_NUMBER
,C901_ATTRIBUTE_TYPE
,C2070_ATTRIBUTE_VALUE
,C2070_LAST_UPDATED_BY
,C2070_LAST_UPDATED_DATE
)
values (
 S2070_UDI_DATA_LOAD_ID.NEXTVAL, v_part_number, v_di_number, v_att_type, v_att_value, v_user_id, SYSDATE);
END IF;

END LOOP;


END gm_sav_UDIDataLoad;

/*
 * Delete all attributes for which values were removed from part prrtibute and update the date for that Part/DI so that the DI is ready for resubmission
 */
PROCEDURE gm_sav_del_attr_udiload
AS
    
    CURSOR del_attr_cur
    IS
         
         SELECT
            c205_part_number_id pnum , c2060_di_number dinum
          , c901_attribute_type atttype , c2070_attribute_value attvalue
           FROM
            t2070_udi_data_load
      MINUS
     SELECT
        c205_part_number_id pnum , c2060_di_number dinum
      , c901_attribute_type atttype , c2070b_attribute_value attvalue
       FROM
        t2070b_udi_curr_data_load;
    
    CURSOR upd_attr_cur
    IS
        
        SELECT DISTINCT
            pnum , dinum
           FROM
            (
                 
                 SELECT
                    c205_part_number_id pnum , c2060_di_number dinum
                  , c901_attribute_type atttype , c2070_attribute_value attvalue
                   FROM
                    t2070_udi_data_load
              MINUS
             SELECT
                c205_part_number_id pnum , c2060_di_number dinum
              , c901_attribute_type atttype , c2070b_attribute_value attvalue
               FROM
                t2070b_udi_curr_data_load
            ) ;
        v_cnt NUMBER;
    BEGIN
         FOR upd_attr IN upd_attr_cur
        LOOP
             
             UPDATE
                t2070_udi_data_load
            SET
                c2070_last_updated_date = SYSDATE
              WHERE
                c205_part_number_id = upd_attr.pnum
                AND c2060_di_number = upd_attr.dinum ;
        
        END LOOP;
        
        FOR del_attr IN del_attr_cur
        LOOP
        
            SELECT COUNT(1) INTO v_cnt
            FROM
                t2070_udi_data_load
              WHERE
                c205_part_number_id = del_attr.pnum
                AND c2060_di_number = del_attr.dinum
                AND c901_attribute_type = del_attr.atttype;
            
            IF v_cnt <> 0
            THEN
            --Insert the records in a log table before deleting
             gm_pkg_cm_job_system_trans.gm_sav_udi_data_load_void_log(del_attr.pnum,del_attr.dinum,del_attr.atttype,del_attr.attvalue,'Y',30301);
            END IF;
                         
             DELETE
               FROM
                t2070_udi_data_load
              WHERE
                c205_part_number_id = del_attr.pnum
                AND c2060_di_number = del_attr.dinum
                AND c901_attribute_type = del_attr.atttype;
        
        END LOOP;
 
 END gm_sav_del_attr_udiload;
/*
Upon completion, drop the T2070A_UDI_PREV_DATA_LOAD and rename the T2070B_UDI_CURR_DATA_LOAD to T2070A_UDI_PREV_DATA_LOAD. 
Finally, create an empty T2070B_UDI_CURR_DATA_LOAD.
*/
PROCEDURE gm_sav_postDeltaLoad
AS 
BEGIN 

	EXECUTE IMMEDIATE 'TRUNCATE TABLE T2070A_UDI_PREV_DATA_LOAD';

	insert into T2070A_UDI_PREV_DATA_LOAD select * from T2070B_UDI_CURR_DATA_LOAD;


END gm_sav_postDeltaLoad;


/*
isAllDataAvailable - 
I/P : T2070_UDI_DATA_LOAD
O/P : Update C2070_ATTRIBUTE_COUNT and C2070_IS_ALL_DATA_AVAILABLE
It should take any part that has been
1. Modified since the last time a UDI_VALIDATION_JOB ran
2. There are no "Open" 1WS errors
*/
PROCEDURE gm_upd_is_data_available(p_last_run_date IN T2070_UDI_DATA_LOAD.C2070_LAST_UPDATED_DATE%TYPE)
AS
v_last_run_date DATE:= p_last_run_date;
V_DE32_FDA_SUB_NUM NUMBER;
V_DE33_FDA_SUPP_NUM NUMBER;
V_DE34_FDA_PRODUCTCODE NUMBER;

V_DE51_PD_SIZE_TYPE NUMBER;
V_DE51_PD_SIZE_VALUE NUMBER;
V_DE51_PD_SIZE_UOM NUMBER;
V_DE51_PD_SIZE_TYP_TXT NUMBER;
V_DEVICE_MORETHAN_1_SIZ NUMBER;
V_PD_SIZE_TYPE_OTHER NUMBER;
V_DE61_REQUIRES_STERILIZATION NUMBER;
V_DE62_STERILIZATION_METHOD NUMBER;

V_DIRECT_ATTRIBUTE_COUNT NUMBER;
V_DE3_DEVICE_COUNT NUMBER;
V_DE4_UNIT_OF_USE_DI NUMBER;
V_DE16_DMDI_DIFFERENT NUMBER;
V_DE17_DMDI_NUMBER NUMBER;
V_DE20_PACKAGE_DI_NUMBER NUMBER;
V_DE21_QTY_PER_PACKAGE NUMBER;
V_DE22_CONTAINS_DI NUMBER;
V_DE24_PACKAGE_DISCONTINUE NUMBER;
V_DE31_EXEMPT_PSN NUMBER;

v_part_num T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE;
v_attribute_count NUMBER := 0;
v_is_all_data_available VARCHAR2(1) := 'Y';
v_UDI_ATTRB_COUNT_WIP_ID T2071_UDI_ATTRIBUTE_COUNT_WIP.C2071_UDI_ATTRB_COUNT_WIP_ID%TYPE;

/*
FDA multi attributes - 1
PD Sizing info - 3
One to one mandatory attributes - 23
*/
V_ATTRIBUTE_NEEDED_COUNT NUMBER := 0;

/*
Select all parts that are in T2071_UDI_ATTRIBUTE_COUNT_WIP with C2070_IS_ALL_DATA_AVAILABLE is NULL or N
Entry will happen in T2071_UDI_ATTRIBUTE_COUNT_WIP when DI flag will be enabled thru screen or ETL
*/
cursor cur_part_all_attribute IS
/*
select T2071.C2071_UDI_ATTRB_COUNT_WIP_ID ATTRIBUTE_COUNT_WIP_ID ,T2071.C205_PART_NUMBER_ID PNUM, T2071.C2060_DI_NUMBER DI_NUMBER
from T2071_UDI_ATTRIBUTE_COUNT_WIP T2071
WHERE NVL(T2071.C2071_IS_ALL_DATA_AVAILABLE,'N') = 'N'
AND T2071.C2060_DI_NUMBER IN (SELECT distinct C2060_DI_NUMBER FROM T2070_UDI_DATA_LOAD); -- #TOREMOVE
*/
SELECT T2071.C2071_UDI_ATTRB_COUNT_WIP_ID ATTRIBUTE_COUNT_WIP_ID ,T2071.C205_PART_NUMBER_ID PNUM, T2071.C2060_DI_NUMBER DI_NUMBER
FROM T2071_UDI_ATTRIBUTE_COUNT_WIP T2071
JOIN
( select distinct T2070.C2060_DI_NUMBER DI_NUM, T2070.C205_PART_NUMBER_ID PNUM
from T2070_UDI_DATA_LOAD T2070
where T2070.C2070_LAST_UPDATED_DATE > v_last_run_date
/*Modified by paddy 08/27(
select max(T9030.C9030_LAST_UPDATED_DATE)
from T9030_JOB_ACTIVITY T9030
where C9030_JOB_NAME = 'UDI_VALIDATION_JOB') */ 
) DI_MODIFIED 
ON T2071.C2060_DI_NUMBER = DI_MODIFIED.DI_NUM
left outer join
( select T9033.C9033_REF_ID DI_NUM, COUNT(T9033.C9033_JOB_ERROR_EVENT_ID ) err_count
from T9033_JOB_ERROR_EVENT T9033
where T9033.C901_STATUS = 104900 -- Open
and T9033.C901_REF_TYPE in (104923) -- 1 WS error
group by T9033.C9033_REF_ID
having COUNT(T9033.C9033_JOB_ERROR_EVENT_ID ) >= 1 ) ERROR_DATA
ON DI_MODIFIED.DI_NUM = ERROR_DATA.DI_NUM
WHERE ERROR_DATA.DI_NUM IS NULL;
-- #tochange exclusion

/*
cursor cur_direct_attributes is
select C901_CODE_ID ATTRIBUTE_TYPE, case when C2070_ATTRIBUTE_VALUE IS NULL THEN 0 ELSE 1 end ATTRIBUTE_CNT
from T901_CODE_LOOKUP T901 LEFT OUTER JOIN 
	(SELECT * 
	 FROM T2070_UDI_DATA_LOAD T2070 
	 WHERE C205_PART_NUMBER_ID = v_part_num)
ON T901.C901_CODE_ID = C901_ATTRIBUTE_TYPE
where T901.C901_CODE_ID in (103785,4000546,104640,103342,103343,4000547,4000548,103345,104701,104655,104677,103346,103782,103783,103784,103347,103786,104460,103341,103348,104461,4000539);
--and C2070_ATTRIBUTE_VALUE is not null
*/

BEGIN
/*
For parts in cur_part_all_attribute, check if all independent attribute count is satisfied.
*/

FOR part_all_attribute IN cur_part_all_attribute
LOOP
	v_part_num := part_all_attribute.PNUM;
	v_udi_attrb_count_wip_id := part_all_attribute.ATTRIBUTE_COUNT_WIP_ID;
/*	v_attribute_count := 0;
	V_ATTRIBUTE_NEEDED_COUNT := 27;
*/	
	v_is_all_data_available := 'N';

	DELETE FROM T2071A_UDI_ATTRIBUTE_COUNT_DTL WHERE C205_PART_NUMBER_ID = v_part_num;
		-- Check regulatory multipart attribute
	select max(case when RULE_GRP_ID = 'DE32_FDA_SUB_NUM' and ATTRIBUTE_CNT > 0 then 1 else 0 end)
	, max(case when RULE_GRP_ID = 'DE33_FDA_SUPP_NUM' and ATTRIBUTE_CNT > 0 then 1 else 0 end)
	, MAX(case when RULE_GRP_ID = 'DE34_FDA_PRODUCTCODE' and ATTRIBUTE_CNT > 0 then 1 else 0 end)
	INTO V_DE32_FDA_SUB_NUM, V_DE33_FDA_SUPP_NUM, V_DE34_FDA_PRODUCTCODE
	from 
	( select T906.C906_RULE_GRP_ID RULE_GRP_ID, COUNT(T2070.C901_ATTRIBUTE_TYPE) ATTRIBUTE_CNT
	from T906_RULES T906 
	left outer join 
	(SELECT * FROM T2070_UDI_DATA_LOAD WHERE C205_PART_NUMBER_ID = v_part_num  --'414.107S'
	) T2070
	on T2070.C901_ATTRIBUTE_TYPE = T906.C906_RULE_ID
	where T906.C906_RULE_GRP_ID in ('DE32_FDA_SUB_NUM','DE33_FDA_SUPP_NUM','DE34_FDA_PRODUCTCODE')
	group by T906.C906_RULE_GRP_ID
	);

	/*IF (V_DE34_FDA_PRODUCTCODE != 1)
	THEN 
		v_is_all_data_available := 'N';
	END IF;
	
	v_attribute_count := V_DE34_FDA_PRODUCTCODE;
		*/

	gm_pkg_cm_job_system_trans.gm_sav_attribute_count_dtls(v_udi_attrb_count_wip_id, v_part_num, 104880 , V_DE34_FDA_PRODUCTCODE, 1, v_user_id);

	
	-- Check direct attributes
	
	/*
	IF V_DIRECT_ATTRIBUTE_COUNT != 23
	THEN
		v_is_all_data_available := 'N';

	END IF;
	
	v_attribute_count := v_attribute_count + V_DIRECT_ATTRIBUTE_COUNT;
		*/

		/* DOnt need since we are doing a INSERT and SELECT
	FOR direct_attribute IN cur_direct_attributes
	LOOP
		gm_pkg_cm_job_system_trans.gm_sav_attribute_count_dtls(v_udi_attrb_count_wip_id, v_part_num, direct_attribute.ATTRIBUTE_TYPE ,direct_attribute.ATTRIBUTE_CNT,1,v_user_id);
	END LOOP;
*/
	INSERT INTO T2071A_UDI_ATTRIBUTE_COUNT_DTL (C2071A_UDI_ATTRB_COUNT_DTL_ID
			  , C2071_UDI_ATTRB_COUNT_WIP_ID
			  , C205_PART_NUMBER_ID
			  , C901_ATTRIBUTE_TYPE
			  , C2071A_AVAILABLE_COUNT
			  , C2071A_NEEDED_COUNT
			  , C2071A_LAST_UPDATED_BY
			  , C2071A_LAST_UPDATED_DATE)
	select S2071A_UDI_ATTRB_COUNT_DTL_ID.nextval, v_udi_attrb_count_wip_id, v_part_num, C901_CODE_ID ATTRIBUTE_TYPE
	, case when C2070_ATTRIBUTE_VALUE IS NULL THEN 0 ELSE 1 end ATTRIBUTE_CNT
	, 1 , v_user_id, sysdate
	from T901_CODE_LOOKUP T901 LEFT OUTER JOIN 
		(SELECT * 
		 FROM T2070_UDI_DATA_LOAD T2070 
		 WHERE C205_PART_NUMBER_ID = v_part_num)
	ON T901.C901_CODE_ID = C901_ATTRIBUTE_TYPE
	where T901.C901_CODE_ID in (103785,4000546,104640,103342,103343,4000547,4000548,103345,104701,104655,104677,103346,103782,103783,103784,103347,103786,104460,103341,103348,104461,4000539);

	--gm_pkg_cm_job_system_trans.gm_sav_attribute_count_dtls(v_udi_attrb_count_wip_id, v_part_num, 104882 ,V_DIRECT_ATTRIBUTE_COUNT,23,v_user_id);
	

	-- Check dependent attributes (DE3, DE16, DE20 Packaging parameters and DE31)
		BEGIN
			select max(case when C901_ATTRIBUTE_TYPE = 103785 then C2070_ATTRIBUTE_VALUE end)  
			, max(case when C901_ATTRIBUTE_TYPE = 4000542 then C2070_ATTRIBUTE_VALUE end) 
			, max(case when C901_ATTRIBUTE_TYPE = 103343 then C2070_ATTRIBUTE_VALUE end) 
			, max(case when C901_ATTRIBUTE_TYPE = 103344 then C2070_ATTRIBUTE_VALUE end) 
			, max(case when C901_ATTRIBUTE_TYPE = 4000543 then C2070_ATTRIBUTE_VALUE end) 
			, max(case when C901_ATTRIBUTE_TYPE = 4000544 then C2070_ATTRIBUTE_VALUE end) 
			, max(case when C901_ATTRIBUTE_TYPE = 4000545 then C2070_ATTRIBUTE_VALUE end) 
			, max(case when C901_ATTRIBUTE_TYPE = 103780 then C2070_ATTRIBUTE_VALUE end) 
			, max(case when C901_ATTRIBUTE_TYPE = 104701 then C2070_ATTRIBUTE_VALUE end) 
			, max(case when C901_ATTRIBUTE_TYPE = 104463 then C2070_ATTRIBUTE_VALUE end)
			, max(case when C901_ATTRIBUTE_TYPE = 104461 then C2070_ATTRIBUTE_VALUE end)
			, max(case when C901_ATTRIBUTE_TYPE = 104462 then C2070_ATTRIBUTE_VALUE end)
			INTO V_DE3_DEVICE_COUNT, V_DE4_UNIT_OF_USE_DI, V_DE16_DMDI_DIFFERENT, V_DE17_DMDI_NUMBER, V_DE20_PACKAGE_DI_NUMBER
			, V_DE21_QTY_PER_PACKAGE, V_DE22_CONTAINS_DI, V_DE24_PACKAGE_DISCONTINUE, V_DE31_EXEMPT_PSN, V_DEVICE_MORETHAN_1_SIZ
			, V_DE61_REQUIRES_STERILIZATION, V_DE62_STERILIZATION_METHOD
			from T2070_UDI_DATA_LOAD 
			where C901_ATTRIBUTE_TYPE in (103785,4000542, 103343,103344, 4000542,4000543,4000544,103780,104701, 104463, 104461, 104462)
			AND C205_PART_NUMBER_ID = v_part_num;

			IF V_DE3_DEVICE_COUNT > 1 
			THEN
				gm_pkg_cm_job_system_trans.gm_sav_attribute_count_dtls(v_udi_attrb_count_wip_id, v_part_num, 104883 ,case when V_DE4_UNIT_OF_USE_DI is not null then 1 else 0 end,1,v_user_id);
			END IF;

			IF V_DE16_DMDI_DIFFERENT = 20375 
			THEN
				gm_pkg_cm_job_system_trans.gm_sav_attribute_count_dtls(v_udi_attrb_count_wip_id, v_part_num, 104888 ,case when V_DE17_DMDI_NUMBER is not null then 1 else 0 end,1,v_user_id);				
			END IF;

			IF V_DE20_PACKAGE_DI_NUMBER IS NOT NULL  
			THEN
				gm_pkg_cm_job_system_trans.gm_sav_attribute_count_dtls(v_udi_attrb_count_wip_id, v_part_num, 104889 , case when V_DE21_QTY_PER_PACKAGE is not null then 1 else 0 end,1,v_user_id);					
				gm_pkg_cm_job_system_trans.gm_sav_attribute_count_dtls(v_udi_attrb_count_wip_id, v_part_num, 104890 , case when V_DE22_CONTAINS_DI is not null then 1 else 0 end,1,v_user_id);					
				gm_pkg_cm_job_system_trans.gm_sav_attribute_count_dtls(v_udi_attrb_count_wip_id, v_part_num, 104891 , case when V_DE24_PACKAGE_DISCONTINUE is not null then 1 else 0 end,1,v_user_id);					

			END IF;

			IF V_DE31_EXEMPT_PSN = 80131 -- No
			THEN
				gm_pkg_cm_job_system_trans.gm_sav_attribute_count_dtls(v_udi_attrb_count_wip_id, v_part_num, 104886 ,V_DE32_FDA_SUB_NUM,1,v_user_id);
			END IF;

			IF V_DE61_REQUIRES_STERILIZATION = 80130 -- Yes
			THEN
				gm_pkg_cm_job_system_trans.gm_sav_attribute_count_dtls(v_udi_attrb_count_wip_id, v_part_num, 104462 , case when V_DE62_STERILIZATION_METHOD is not null then 1 else 0 end,1,v_user_id);					
			END IF;

			IF V_DEVICE_MORETHAN_1_SIZ = 80130 -- Yes
			THEN
				-- Check PD sizing multipart attribute
				select max(case when RULE_GRP_ID = 'DE51_PD_SIZE_TYPE' and ATTRIBUTE_CNT > 0 then 1 else 0 end) 
				, max(case when RULE_GRP_ID = 'DE51_PD_SIZE_VALUE' and ATTRIBUTE_CNT > 0 then 1 else 0 end) 
				, max(case when RULE_GRP_ID = 'DE51_PD_SIZE_UOM' and ATTRIBUTE_CNT > 0 then 1 else 0 end) 
				, MAX(case when RULE_GRP_ID = 'DE51_PD_SIZE_TYP_TXT' AND ATTRIBUTE_CNT > 0 then 1 else 0 end) 
				, MAX(case when OTHER > 0 then 1 else 0 end ) 
				INTO V_DE51_PD_SIZE_TYPE, V_DE51_PD_SIZE_VALUE, V_DE51_PD_SIZE_UOM, V_DE51_PD_SIZE_TYP_TXT, V_PD_SIZE_TYPE_OTHER
				from 
				( select T906.C906_RULE_GRP_ID RULE_GRP_ID, COUNT(T2070.C901_ATTRIBUTE_TYPE) ATTRIBUTE_CNT
				, sum (case when T2070.C901_ATTRIBUTE_TYPE IN (104468,104472,104476,104464) and T2070.C2070_ATTRIBUTE_VALUE = '104047' then 1 else 0 end) OTHER
				from T906_RULES T906 left outer join (SELECT * FROM T2070_UDI_DATA_LOAD WHERE C205_PART_NUMBER_ID = v_part_num  --'414.107S' 104047 => Others
				) T2070
				on T2070.C901_ATTRIBUTE_TYPE = T906.C906_RULE_ID
				where T906.C906_RULE_GRP_ID in ('DE51_PD_SIZE_TYPE','DE51_PD_SIZE_VALUE','DE51_PD_SIZE_UOM','DE51_PD_SIZE_TYP_TXT')
				group by T906.C906_RULE_GRP_ID
				);

				gm_pkg_cm_job_system_trans.gm_sav_attribute_count_dtls(v_udi_attrb_count_wip_id, v_part_num, 104881 ,V_DE51_PD_SIZE_TYPE, 1 ,v_user_id);
				
				-- PMT-35256: UDI Job to skip others type validation
				-- No need to validate "Value" and "UOM" fields (if selected type "Others").
				   
				IF V_PD_SIZE_TYPE_OTHER = 0
				THEN
					gm_pkg_cm_job_system_trans.gm_sav_attribute_count_dtls(v_udi_attrb_count_wip_id, v_part_num, 104892 ,V_DE51_PD_SIZE_VALUE, 1 ,v_user_id);
					gm_pkg_cm_job_system_trans.gm_sav_attribute_count_dtls(v_udi_attrb_count_wip_id, v_part_num, 104893 ,V_DE51_PD_SIZE_UOM, 1 ,v_user_id);
					
				ELSE
					gm_pkg_cm_job_system_trans.gm_sav_attribute_count_dtls(v_udi_attrb_count_wip_id, v_part_num, 104894 ,V_DE51_PD_SIZE_TYP_TXT, V_PD_SIZE_TYPE_OTHER ,v_user_id);
				END IF;	-- end if V_PD_SIZE_TYPE_OTHER

			END IF;

		
		--v_is_all_data_available := 'Y';
		SELECT SUM(C2071A_AVAILABLE_COUNT) , SUM(C2071A_NEEDED_COUNT)
		INTO v_attribute_count, V_ATTRIBUTE_NEEDED_COUNT
		FROM T2071A_UDI_ATTRIBUTE_COUNT_DTL
		WHERE C205_PART_NUMBER_ID = v_part_num;

		IF v_attribute_count = V_ATTRIBUTE_NEEDED_COUNT
		THEN 
			v_is_all_data_available := 'Y';
		END IF;
	
		UPDATE T2071_UDI_ATTRIBUTE_COUNT_WIP 
		SET C2071_IS_ALL_DATA_AVAILABLE = v_is_all_data_available
		, C2071_ATTRIBUTE_COUNT = v_attribute_count
		, C2071_ATTRIBUTE_NEEDED_COUNT = V_ATTRIBUTE_NEEDED_COUNT
		, C2071_LAST_UPDATED_BY = v_user_id
		, C2071_LAST_UPDATED_DATE = SYSDATE
		WHERE C205_PART_NUMBER_ID = v_part_num;

		EXCEPTION 
		WHEN NO_DATA_FOUND 
		THEN 
			-- continue .. coz we can skip and go to the next part 
			V_DIRECT_ATTRIBUTE_COUNT := 1;
		END;

COMMIT; -- #tochange - commit for every 100
END LOOP;

END gm_upd_is_data_available;


/*
If C2072_GDSN_ITEM_UPLOAD_DATE IS NULL, don�t call this procedure
I/P : T2070_UDI_DATA_LOAD T2070 and T9030_JOB_ACTIVITY T9030
In T9031, we'll have a job entry everytime the load runs. So get the maxdate for this jobname as v_max_date_job
SELECT FROM T2070_UDI_DATA_LOAD WHERE LUD > v_max_date_job
O/P : Get the count and update it in T2071_UDI_ATTRIBUTE_COUNT_WIP.C2070_MODIFIED_COUNT
*/
PROCEDURE gm_upd_modified_count(p_last_run_date IN T2070_UDI_DATA_LOAD.C2070_LAST_UPDATED_DATE%TYPE)
AS
v_last_run_date DATE:= p_last_run_date;
v_job_name VARCHAR2(50) := 'UDI_VALIDATION_JOB';
v_part_num T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE;
v_modified_cnt NUMBER;


cursor cur_part_upd_modified_count IS
select T2070.C205_PART_NUMBER_ID PNUM, COUNT(1) MODIFIED_CNT
from T2070_UDI_DATA_LOAD T2070, T2072_DI_GUDID_STATUS T2072
where T2070.C2060_DI_NUMBER = T2072.C2060_DI_NUMBER
and T2070.C2070_LAST_UPDATED_DATE >v_last_run_date -- T2072.C2072_GDSN_ITEM_UPLOAD_DATE
group by T2070.C205_PART_NUMBER_ID;


BEGIN

-- Insert into T9030_JOB_ACTIVITY
/*select MAX(T9031.C9030_LAST_UPDATED_DATE)
INTO v_max_date
from T9030_JOB_ACTIVITY T9031
where T9031.C9030_JOB_NAME = v_job_name;
*/

FOR part_upd_modified_count IN cur_part_upd_modified_count
LOOP
	v_part_num := part_upd_modified_count.pnum;
	v_modified_cnt := part_upd_modified_count.MODIFIED_CNT;

	UPDATE T2071_UDI_ATTRIBUTE_COUNT_WIP 
	SET C2071_MODIFIED_COUNT = v_modified_cnt
	, C2071_LAST_UPDATED_BY = v_user_id
	, C2071_LAST_UPDATED_DATE = sysdate
	WHERE c205_part_number_id = v_part_num;

END LOOP;

/*
UPDATE T2071_UDI_ATTRIBUTE_COUNT_WIP T2071 
SET (C2071_MODIFIED_COUNT, C2071_LAST_UPDATED_BY, C2071_LAST_UPDATED_DATE)
 = ( select COUNT(1) , v_user_id, sysdate
from T2070_UDI_DATA_LOAD T2070, T2072_DI_GUDID_STATUS T2072
where T2070.C2060_DI_NUMBER = T2072.C2060_DI_NUMBER
and T2070.C2070_LAST_UPDATED_DATE > T2072.C2072_GDSN_ITEM_UPLOAD_DATE
AND T2071.C205_PART_NUMBER_ID =  T2070.C205_PART_NUMBER_ID
group by T2070.C205_PART_NUMBER_ID );
*/

END gm_upd_modified_count;


/*
If (C2072_GDSN_ITEM_UPLOAD_DATE IS NULL AND isAllDataAvailable) OR NVL(modifiedCount,-1) > 0 then set this flag
O/P : Update  T2071_UDI_ATTRIBUTE_COUNT_WIP.C2071_IS_READY_FOR_VALIDATION
 */
PROCEDURE gm_upd_validation_flag
AS

v_di_number NUMBER;


    CURSOR cur_di_eligible_for_validation IS
    select T2071.C2060_DI_NUMBER di_number
    from T2072_DI_GUDID_STATUS T2072, T2071_UDI_ATTRIBUTE_COUNT_WIP T2071
    where T2072.C2060_DI_NUMBER = T2071.C2060_DI_NUMBER
    and ( ( T2072.C2072_GDSN_ITEM_UPLOAD_FL is null
    and T2071.C2071_IS_ALL_DATA_AVAILABLE = 'Y' )
    or (T2071.C2071_MODIFIED_COUNT > 0 AND T2071.C2071_IS_ALL_DATA_AVAILABLE = 'Y' )
    );


BEGIN

-- #tochange - reset all C2071_IS_READY_FOR_VALIDATION to null
UPDATE T2071_UDI_ATTRIBUTE_COUNT_WIP 
	SET C2071_IS_READY_FOR_VALIDATION = NULL
	, C2071_LAST_UPDATED_BY = v_user_id
	, C2071_LAST_UPDATED_DATE = sysdate
WHERE C2071_IS_READY_FOR_VALIDATION IS NOT NULL;


FOR di_eligible_for_validation IN cur_di_eligible_for_validation
LOOP
	v_di_number := di_eligible_for_validation.di_number;

	UPDATE T2071_UDI_ATTRIBUTE_COUNT_WIP 
	SET C2071_IS_READY_FOR_VALIDATION = 'Y'
	, C2071_LAST_UPDATED_BY = v_user_id
	, C2071_LAST_UPDATED_DATE = sysdate
	WHERE C2060_DI_NUMBER = v_di_number;

END LOOP;

/*
UPDATE T2071_UDI_ATTRIBUTE_COUNT_WIP T2071
SET (C2071_IS_READY_FOR_VALIDATION, C2071_LAST_UPDATED_BY, C2071_LAST_UPDATED_DATE)
= ( SELECT 'Y', v_user_id, sysdate
from T2072_DI_GUDID_STATUS T2072
where T2072.C2060_DI_NUMBER = T2071.C2060_DI_NUMBER
and ( ( T2072.C2072_GDSN_ITEM_UPLOAD_FL is null
and T2071.C2071_IS_ALL_DATA_AVAILABLE = 'Y' )
or T2071.C2071_MODIFIED_COUNT > 0
) );
*/

END gm_upd_validation_flag;


/*
Load Job Dim
*/
PROCEDURE gm_pre_validation_setup
AS
v_job_name VARCHAR2(50) := 'UDI_VALIDATION_JOB';
v_job_id NUMBER;

BEGIN

gm_pkg_cm_job_system_trans.gm_sav_job_activity(
	v_job_name 
	,'Data validation' 
	,NULL
	,NULL
	,0
	,0
	,0
	,NULL
	,v_user_id
	,v_job_activity_id
	,v_job_id
);

END gm_pre_validation_setup;


/*
Query T2071_UDI_ATTRIBUTE_COUNT_WIP.C2071_IS_READY_FOR_VALIDATION = 'Y'  to identify records to be validated. 
For each of the DI, get the attribute values from T2070_UDI_DATA_LOAD and insert into source dim
"If ( part was already uploaded to GDSN and has no validation errors)
Then 
	check if any attributes were modified. If so, load all attributes for this DI into source system table.  
If ( part was never uploaded to GDSN and has no validation errors)
Then
	load all attributes for this DI into source system table.  
*/
PROCEDURE gm_load_source_data(p_last_run_date IN T2070_UDI_DATA_LOAD.C2070_LAST_UPDATED_DATE%TYPE)
AS
v_last_run_date DATE:= p_last_run_date;
CURSOR cur_di_new_update IS
        select distinct T2070.C2060_DI_NUMBER DI_NUM, T2070.C205_PART_NUMBER_ID PNUM
        from T2070_UDI_DATA_LOAD T2070
        where T2070.C2070_LAST_UPDATED_DATE > v_last_run_date;

BEGIN
FOR di_new_update IN cur_di_new_update
LOOP
-- #tochange - 1. remove the case where condition. 2. do direct join with gudid_Status table
INSERT INTO T9031_JOB_SOURCE_SYSTEM
    (
      C9031_SOURCE_SYSTEM_KEY_ID,
      C9030_JOB_ACTIVITY_ID,
      C9031_ATTRIBUTE_TYPE,
      C9031_ATTRIBUTE_VALUE,
      C901_REF_TYPE,
      C9031_REF_ID,
      C9031_LAST_UPDATED_BY,
      C9031_LAST_UPDATED_DATE
    )
SELECT S9031_JOB_SOURCE_SYSTEM.NEXTVAL, v_job_activity_id , C901_ATTRIBUTE_TYPE , C2070_ATTRIBUTE_VALUE, null -- to add codeid for DI
, T2070.C2060_DI_NUMBER, v_user_id, sysdate
FROM T2070_UDI_DATA_LOAD T2070, T2071_UDI_ATTRIBUTE_COUNT_WIP T2071      
WHERE T2070.C2060_DI_NUMBER = T2071.C2060_DI_NUMBER
AND T2071.C2071_IS_READY_FOR_VALIDATION = 'Y'
AND T2070.C2060_DI_NUMBER=di_new_update.DI_NUM;
--AND T2070.C2070_LAST_UPDATED_DATE>=v_last_run_date; -- Added by Paddy
END LOOP;
/*
SELECT S9031_JOB_SOURCE_SYSTEM.NEXTVAL, v_job_activity_id , C901_ATTRIBUTE_TYPE , C2070_ATTRIBUTE_VALUE, null -- to add codeid for DI
, C2060_DI_NUMBER, v_user_id, sysdate
from T2070_UDI_DATA_LOAD 
where C2060_DI_NUMBER IN (
select distinct T2071.C2060_DI_NUMBER
from (SELECT C2060_DI_NUMBER, MAX(C2070_LAST_UPDATED_DATE) as C2070_LAST_UPDATED_DATE FROM T2070_UDI_DATA_LOAD GROUP BY C2060_DI_NUMBER ) T2070, T2071_UDI_ATTRIBUTE_COUNT_WIP T2071, T2072_DI_GUDID_STATUS T2072
WHERE T2070.C2060_DI_NUMBER = T2072.C2060_DI_NUMBER
AND T2070.C2060_DI_NUMBER = T2071.C2060_DI_NUMBER
AND T2071.C2071_IS_READY_FOR_VALIDATION = 'Y'
and case when (T2072.C2072_GDSN_ITEM_UPLOAD_FL = 'Y' or (NVL(T2072.C2072_GDSN_ITEM_UPLOAD_FL,'N') = 'N' and NVL(T2071.C2071_VALIDATION_ERROR_COUNT,0) > 0))then T2070.C2070_LAST_UPDATED_DATE 
    when NVL(T2072.C2072_GDSN_ITEM_UPLOAD_FL,'N') = 'N' and NVL(T2071.C2071_VALIDATION_ERROR_COUNT,0) = 0 then trunc(sysdate)
    end >= case when NVL(T2072.C2072_GDSN_ITEM_UPLOAD_FL,'N') = 'Y' then trunc(T2072.C2072_GDSN_ITEM_UPLOAD_DATE)
          when NVL(T2072.C2072_GDSN_ITEM_UPLOAD_FL,'N')= 'N' and NVL(T2071.C2071_VALIDATION_ERROR_COUNT,0) = 0 then trunc(sysdate) - 2 
          --when NVL(T2072.C2072_GDSN_ITEM_UPLOAD_FL,'N') = 'N' and T2071.C2071_VALIDATION_ERROR_COUNT > 0 then TRUNC(T2072.C2072_GDSN_ITEM_UPLOAD_DATE) -- TOCHANGE
          end ) ;
*/
/*
Below code is used when only the modified columns are to be updated.

select S9031_JOB_SOURCE_SYSTEM.NEXTVAL, v_job_activity_id , T2070.C901_ATTRIBUTE_TYPE , T2070.C2070_ATTRIBUTE_VALUE, null -- to add codeid for DI
,T2071.C2060_DI_NUMBER, v_user_id, SYSDATE
from T2070_UDI_DATA_LOAD T2070, T2071_UDI_ATTRIBUTE_COUNT_WIP T2071, T2072_DI_GUDID_STATUS T2072
where T2070.C2060_DI_NUMBER = T2072.C2060_DI_NUMBER
AND T2070.C2060_DI_NUMBER = T2071.C2060_DI_NUMBER
AND T2071.C2071_IS_READY_FOR_VALIDATION = 'Y'
and case when (T2072.C2072_GUDID_PUBLISH_FL = 'Y' or (NVL(T2072.C2072_GUDID_PUBLISH_FL,'N') = 'N' and NVL(T2071.C2071_VALIDATION_ERROR_COUNT,0) > 0))then T2070.C2070_LAST_UPDATED_DATE 
    when NVL(T2072.C2072_GUDID_PUBLISH_FL,'N') = 'N' and NVL(T2071.C2071_VALIDATION_ERROR_COUNT,0) = 0 then sysdate
    end > case when NVL(T2072.C2072_GUDID_PUBLISH_FL,'N') = 'Y' then T2072.C2072_GDSN_ITEM_UPLOAD_DATE
          when NVL(T2072.C2072_GUDID_PUBLISH_FL,'N')= 'N' and NVL(T2071.C2071_VALIDATION_ERROR_COUNT,0) = 0 then sysdate - 2 
          when NVL(T2072.C2072_GUDID_PUBLISH_FL,'N') = 'N' AND T2071.C2071_VALIDATION_ERROR_COUNT > 0 then T2072.C2072_GDSN_ITEM_UPLOAD_DATE -- TOCHANGE
          end ;
*/

END gm_load_source_data;


END gm_pkg_udi_data_load;
/