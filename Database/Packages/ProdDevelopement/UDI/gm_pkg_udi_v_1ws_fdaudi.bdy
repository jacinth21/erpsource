-- @"C:\jbossbranch\Database\Packages\ProdDevelopement\UDI\gm_pkg_udi_v_1ws_fdaudi.bdy"
CREATE OR REPLACE PACKAGE BODY gm_pkg_udi_v_1ws_fdaudi
IS
v_job_activity_id T9033_JOB_ERROR_EVENT.C9030_JOB_ACTIVITY_ID%TYPE;
/*
exec gm_pkg_udi_v_1ws_fdaudi.gm_fda_udi_validation_main;
*/
PROCEDURE gm_fda_udi_validation_main (
	p_job_activity_id T9033_JOB_ERROR_EVENT.C9030_JOB_ACTIVITY_ID%TYPE
)
AS
BEGIN

v_job_activity_id := p_job_activity_id;
DBMS_OUTPUT.put_line (' v_job_activity_id in gm_pkg_udi_v_1ws_fdaudi '||v_job_activity_id);

-- PD validation
gm_pkg_udi_v_1ws_fdaudi_pd.v_job_activity_id := v_job_activity_id;
gm_pkg_udi_v_1ws_fdaudi_pd.DE51_DE52_CLIN_TYPE_VALUE_REQ;
gm_pkg_udi_v_1ws_fdaudi_pd.DE54_CLINICAL_SIZE_TEXT_CONDN;
gm_pkg_udi_v_1ws_fdaudi_pd.DE60_STERILIZATION_METHOD;

-- Regulatory validation
gm_pkg_udi_v_1ws_fdaudi_reg.v_job_activity_id := v_job_activity_id;
gm_pkg_udi_v_1ws_fdaudi_reg.DE32_FDAPSN_CHECK;
gm_pkg_udi_v_1ws_fdaudi_reg.DE37_GMDN_REQ_5_DIGITS;
gm_pkg_udi_v_1ws_fdaudi_reg.DE34_PROD_CLASS_3_CHAR_LEN;
gm_pkg_udi_v_1ws_fdaudi_reg.DE28_DE32_TISSUE_FDAPSN_CODE;
gm_pkg_udi_v_1ws_fdaudi_reg.DE28_DE31_TISSUE_NOT_EXEMPT;
gm_pkg_udi_v_1ws_fdaudi_reg.DE36_LISTING_NUM_7_CHAR;

-- Quality validation
gm_pkg_udi_v_1ws_fdaudi_qa.v_job_activity_id := v_job_activity_id;
gm_pkg_udi_v_1ws_fdaudi_qa.DE17_DIRECT_PART_MARKING_6TO23;
gm_pkg_udi_v_1ws_fdaudi_qa.DE4_FDA_UNIT_OF_USE_GTIN;
gm_pkg_udi_v_1ws_fdaudi_qa.DE4_FDA_NETCONTENT_1_NO_UoU;
gm_pkg_udi_v_1ws_fdaudi_qa.DE15_EXEMPT_FROM_MARKING;

-- post validation
gm_pkg_udi_v_1ws_fdaudi.gm_fda_udi_post_validation;

END gm_fda_udi_validation_main;

/*
1. For all success records, update status in T2072_DI_GUDID_STATUS.C901_INTERNAL_SUBMIT_STATUS to review
2. Update T2071_UDI_ATTRIBUTE_COUNT_WIP.C2071_VALIDATION_ERROR_COUNT with # of DI's that had errors
3. Update T9030_JOB_ACTIVITY.C9030_SOURCE_COUNT and T9030_JOB_ACTIVITY.C9030_PASS_COUNT , T9030_JOB_ACTIVITY.C9030_FAIL_COUNT
*/
PROCEDURE gm_fda_udi_post_validation
AS

v_total_count NUMBER;
v_success_count NUMBER;
v_failure_count NUMBER;

CURSOR cur_di_status IS
select T9031.C9031_REF_ID SUCCESS_DI_NUM, NVL(T9033.ERR_CNT,0) ERROR_CNT
from 
( select distinct C9031_REF_ID from T9031_JOB_SOURCE_SYSTEM where C9030_JOB_ACTIVITY_ID = v_job_activity_id )T9031
left outer join
( select C9033_REF_ID, SUM( case when c901_status = 104900 then 1 else 0 end) ERR_CNT from T9033_JOB_ERROR_EVENT where C9030_JOB_ACTIVITY_ID = v_job_activity_id GROUP BY  C9033_REF_ID)T9033
on T9031.C9031_REF_ID = T9033.C9033_REF_ID;

BEGIN

FOR di_status IN cur_di_status
LOOP

UPDATE T2072_DI_GUDID_STATUS
SET C901_INTERNAL_SUBMIT_STATUS = DECODE(di_status.ERROR_CNT, 0, 104822, 104821)
, C2072_LAST_UPDATED_DATE = sysdate
, C2072_LAST_UPDATED_BY = 30301
--, C9030_JOB_ACTIVITY_ID = v_job_activity_id
, C9030_SOURCE_ACTIVITY_ID = v_job_activity_id
WHERE C2060_DI_NUMBER = di_status.SUCCESS_DI_NUM;

UPDATE T2071_UDI_ATTRIBUTE_COUNT_WIP 
SET C2071_VALIDATION_ERROR_COUNT = di_status.ERROR_CNT
, C2071_LAST_UPDATED_DATE = sysdate
, C2071_LAST_UPDATED_BY = 30301
, C9030_JOB_ACTIVITY_ID = v_job_activity_id
WHERE C2060_DI_NUMBER = di_status.SUCCESS_DI_NUM;

END LOOP;

select COUNT(T9031.C9031_REF_ID) TOTAL_COUNT, SUM(case when T9033.C9033_REF_ID is null then 1 else 0 end) SUCCESS_COUNT
, SUM(case when T9033.C9033_REF_ID is not null then 1 else 0 end) FAILURE_COUNT
INTO v_total_count, v_success_count, v_failure_count
from 
( select distinct C9031_REF_ID from T9031_JOB_SOURCE_SYSTEM where C9030_JOB_ACTIVITY_ID = v_job_activity_id )T9031
left outer join
( select distinct C9033_REF_ID from T9033_JOB_ERROR_EVENT where C9030_JOB_ACTIVITY_ID = v_job_activity_id )T9033
on T9031.C9031_REF_ID = T9033.C9033_REF_ID;

UPDATE T9030_JOB_ACTIVITY
SET C9030_SOURCE_COUNT = v_total_count
, C9030_PASS_COUNT = v_success_count
, C9030_FAIL_COUNT = v_failure_count
, C9030_LAST_UPDATED_DATE = sysdate
, C9030_LAST_UPDATED_BY = 30301
WHERE C9030_JOB_ACTIVITY_ID = v_job_activity_id;

END gm_fda_udi_post_validation;


END gm_pkg_udi_v_1ws_fdaudi;
/
