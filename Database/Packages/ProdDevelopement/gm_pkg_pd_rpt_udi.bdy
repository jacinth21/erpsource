/* Formatted on 2010/03/10 09:56 (Formatter Plus v4.8.0) */
-- @"C:\database\Packages\ProdDevelopement\gm_pkg_pd_rpt_udi.bdy"
CREATE OR REPLACE
PACKAGE BODY gm_pkg_pd_rpt_udi
IS
    --
    /*******************************************************
    * Description : Get the part DI Number
    * Author   : Mani
    *******************************************************/
FUNCTION get_part_di_number (
        p_part_number IN t2060_di_part_map_log.C205_PART_NUMBER_ID%TYPE)
    RETURN VARCHAR2
IS
    v_di_number t2060_di_part_mapping.c2060_di_number%TYPE;
BEGIN
    --
    BEGIN
         SELECT c2060_di_number
           INTO v_di_number
           FROM t2060_di_part_mapping
          WHERE c205_part_number_id = p_part_number;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_di_number := NULL;
    END;
    RETURN v_di_number;
END get_part_di_number;
/*******************************************************
* Description : If the part satisfies condition for DMDI which is not primary DI, return true else false
* Author   : Mani
*******************************************************/
--
FUNCTION get_dm_di_primary_di (
        p_part_number IN t2060_di_part_map_log.C205_PART_NUMBER_ID%TYPE)
    RETURN VARCHAR2
IS
    --
    v_udi_etch_req t205d_part_attribute.c205d_attribute_value%TYPE;
    v_di_etch_req t205d_part_attribute.c205d_attribute_value%TYPE;
    v_pi_etch_req t205d_part_attribute.c205d_attribute_value%TYPE;
    --
    v_sub_com_part t205_part_number.c205_part_number_id%TYPE;
    v_sub_com_udi_etch_req t205d_part_attribute.c205d_attribute_value%TYPE;
    v_sub_com_di_etch_req t205d_part_attribute.c205d_attribute_value%TYPE;
    v_sub_di_number t2060_di_part_mapping.c2060_di_number%TYPE;
    --
    v_final_val VARCHAR2 (10) ;
    CURSOR subcomponent_cur
    IS
        -- Sub component part - details
        SELECT DISTINCT T205A.C205_TO_PART_NUMBER_ID PNUM, level LVL
       , gm_pkg_pd_rpt_udi.get_part_di_number (T205A.C205_TO_PART_NUMBER_ID) di_number
   	FROM T205A_PART_MAPPING T205A
   		WHERE T205A.C901_TYPE                                   = 30031 -- Sub Component
    	START WITH T205A.C205_FROM_PART_NUMBER_ID             = p_part_number
    	CONNECT BY NOCYCLE PRIOR T205A.C205_TO_PART_NUMBER_ID = T205A.C205_FROM_PART_NUMBER_ID
    	                     AND T205A.C205A_VOID_FL IS NULL  ---  added new column c205a_void_fl in t205a used to PMT-50777 - Create BOM and sub-component Mapping
	ORDER BY LVL DESC;       

    --
BEGIN
    --
    --20375	Yes	CERTR
	--20376	No	CERTR
	--
	--103381	UDI Etch Required
	--103382	DI Only Etch Required
	--103383	PI Only Etch Required
	--
    v_final_val := NULL;
    -- 1. part - Etch Req. verify (no)
     SELECT trim(GET_PART_ATTRIBUTE_VALUE (p_part_number, '103381')), trim(GET_PART_ATTRIBUTE_VALUE (p_part_number, '103382')),
        trim(GET_PART_ATTRIBUTE_VALUE (p_part_number, '103383'))
       INTO v_udi_etch_req, v_di_etch_req, v_pi_etch_req
       FROM dual;
    -- 103364, 103367, 103385 - No
    -- 103363, 103366, 103384	Yes
    IF (v_udi_etch_req IS NULL OR v_udi_etch_req = '103363' OR v_di_etch_req IS NULL OR v_di_etch_req = '103366' OR v_pi_etch_req IS NULL OR v_pi_etch_req = '103384') THEN
        RETURN v_final_val;
    END IF;
    --
    FOR subcom_parts IN subcomponent_cur
    LOOP
    	v_sub_di_number := subcom_parts.di_number;
        --2.Sub Componet UDI and DI verify
        v_sub_com_udi_etch_req := GET_PART_ATTRIBUTE_VALUE (subcom_parts.pnum, '103381') ;
        v_sub_com_di_etch_req  := GET_PART_ATTRIBUTE_VALUE (subcom_parts.pnum, '103382') ;
        -- 103363 - Yes
        -- 103366 - Yes
        IF (v_sub_di_number IS NOT NULL AND (v_sub_com_udi_etch_req = '103363' OR v_sub_com_di_etch_req = '103366')) THEN
            v_final_val           := '20375';
            RETURN v_final_val;
        END IF;
    END LOOP;
    RETURN v_final_val;
END get_dm_di_primary_di;
/*******************************************************
* Description : If the part satisfies condition for DMDI which is not primary DI, return actual DI Number else NULL
* Author   : Mani
*******************************************************/
--
FUNCTION get_dm_di_number (
        p_part_number IN t2060_di_part_map_log.C205_PART_NUMBER_ID%TYPE)
    RETURN VARCHAR2
IS
    --
    v_dm_di_primary_di VARCHAR2 (10) ;
    v_di_number t2060_di_part_mapping.c2060_di_number%TYPE;
    v_random_di_number t2060_di_part_mapping.c2060_di_number%TYPE;
    --
    v_sub_com_udi_etch_req t205d_part_attribute.c205d_attribute_value%TYPE;
    v_sub_com_di_etch_req t205d_part_attribute.c205d_attribute_value%TYPE;
    --
    CURSOR subcomponent_cur
    IS
        SELECT DISTINCT T205A.C205_TO_PART_NUMBER_ID PNUM, level LVL
    	, gm_pkg_pd_rpt_udi.get_part_di_number (T205A.C205_TO_PART_NUMBER_ID) dinumber
   	FROM T205A_PART_MAPPING T205A, T205_PART_NUMBER T205
   	WHERE T205A.C205_TO_PART_NUMBER_ID                = T205.C205_PART_NUMBER_ID
   		AND T205A.C901_TYPE                                   = 30031 -- Sub Component
   		AND T205.c205_product_class    <> 4030 -- Sterile
    	START WITH T205A.C205_FROM_PART_NUMBER_ID             = p_part_number
    	CONNECT BY NOCYCLE PRIOR T205.C205_PART_NUMBER_ID = T205A.C205_FROM_PART_NUMBER_ID
    	AND T205A.C205A_VOID_FL IS NULL  ---  added new column c205a_void_fl in t205a used to PMT-50777 - Create BOM and sub-component Mapping
	ORDER BY LVL DESC;       

BEGIN
    --
    v_random_di_number := NULL;
    --
    FOR subcomp_parts  IN subcomponent_cur
    LOOP
        v_di_number        := subcomp_parts.dinumber;
        --
        IF v_di_number IS NOT NULL
        THEN
        --Sub Componet UDI and DI value - verify
        v_sub_com_udi_etch_req := GET_PART_ATTRIBUTE_VALUE (subcomp_parts.pnum, '103381') ;
        v_sub_com_di_etch_req  := GET_PART_ATTRIBUTE_VALUE (subcomp_parts.pnum, '103382') ;
        -- 103363 - Yes
        -- 103366 - Yes
	        IF (v_sub_com_udi_etch_req = '103363' OR v_sub_com_di_etch_req = '103366')
	        THEN
	        	v_random_di_number := v_di_number;
	        	RETURN v_random_di_number;
	        END IF;	
        END IF;	
    END LOOP;
    --
    RETURN v_random_di_number;
END get_dm_di_number;
--
/*******************************************************
* Description : Procedure to use to save the part mapping details
* Author   : Mani
*******************************************************/
/*
This procedure will fetch below info of a part / DI
1. DI number
2. Subject to DM but exempt
3. DM DI is not Primary DI
4. DM DI Number
5. HCTP
6. For Single Use
7. Device Packaged as Sterile
8. MRI safety
9. Expiration Date
*/
--
PROCEDURE gm_fch_qa_udi_param (
        p_partnumber IN t205_part_number.c205_part_number_id%TYPE,
        p_report OUT TYPES.cursor_type)
AS
BEGIN
    OPEN p_report FOR SELECT t205.C205_PART_NUMBER_ID partnum, t2060.c2060_di_number diNumber, get_code_name (
    t205d.c205d_subject_Dm_Exempt) subjectDmExempt, get_code_name (t205d.c205d_dm_Di_Primary_Di) dmDiPrimaryDi,
    t205d.c205d_dm_Di_Number dmDiNumber, get_code_name (t205d.c205d_hctp) hctp, get_code_name (t205d.c205d_single_Use)
    singleUse, get_code_name (t205d.c205d_dev_pkg_sterile) devPkgSterile, t205d.c205d_mri_Safety_Information
    mriSafetyInformation, t205d.c205d_expiration_Dt expirationDt, t205d.c205d_override_Company_Nm overrideCompanyNm,
    t205d.c205d_device_Count deviceCount, t205d.c205d_unit_Use_Di_Number unitUseDiNumber, t205d.c205d_pack_Di_Number
    packDiNumber, t205d.c205d_quantity_Per_Pack quantityPerPack, t205d.c205d_contains_Di_Pack containsDiPack,
    t205d.c205d_duns_Number dunsNumber, t205d.c205d_cust_Contact_Phone custContactPhone, t205d.c205d_cust_Contact_Mail
    custContactMail, t205d.c205d_package_Discon_Dt packageDisconDt, t205d.c205d_lot_Or_Batch_Num lotOrBatchNum,
    get_rule_value ('103782', 'BULK_PACKAGING_PARM') ruleLotBatchVal, t205d.c205d_serial_Num serialNum, get_rule_value
    ('103784', 'BULK_PACKAGING_PARM') ruleSerialNumVal, t205d.c205d_manufaturing_Dt manufaturingDt, get_rule_value (
    '103783', 'BULK_PACKAGING_PARM') ruleManufaturingDtVal, t205d.c205d_donation_Iden_Num donationIdenNum,
    get_rule_value ('103786', 'BULK_PACKAGING_PARM') ruleDonationIdenNumVal, DECODE (t205.c205_product_class, '4030',
    'Y', 'N') sterilePart, GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1056) mri_history_fl -- 103341 MRI Safety Information
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1057) sub_history_fl -- 103342 Subject to DM but Exempt
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1058) dmdi_primary__history_fl -- 103343 DM DI is not Primary DI
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1059) dmdi_history_fl -- 103344 DM DI Number
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1060) hctp_history_fl -- 103345 HCTP
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1061) for_single_history_fl -- 103346 For Single Use
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1062) expriration_history_fl -- 103347 Expiration Date
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1063) device_pack_history_fl -- 103348 Device Packaged as Sterile
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1070) pack_discon_history_fl -- 103780 Package Discontinue Date
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1071) override_comp_history_fl -- 103781 Override Company Name
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1075) lot_batch_history_fl -- 103782 Lot or Batch Number
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1076) manuf_dt_history_fl -- 103783 Manufacturing Date
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1077) serial_num_history_fl -- 103784 Serial Number
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1064) device_cnt_history_fl -- 103785 Device Count
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1065) donation_iden_history_fl -- 103786 Donation Identification Number
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1066) unit_use_di_history_fl -- 4000542 Unit of Use DI Number
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1067) pack_di_num_history_fl -- 4000543 Package DI Number
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1068) quantity_pack_history_fl -- 4000544 Quantity per Package
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1069) contain_di_pack_history_fl -- 4000545 Contains DI Package
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1072) duns_num_history_fl -- 4000546 Labeler DUNS Number
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1073) contact_phone_history_fl -- 4000547 Customer Contact Phone
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1074) contact_email_history_fl -- 4000548 Customer Contact Email
    , gm_pkg_pd_rpt_udi.get_part_udi_format (t205.C205_PART_NUMBER_ID) samplePattern
    FROM t2060_di_part_mapping t2060, t205_part_number t205, v205g_part_udi_parameter t205d
      WHERE t205.C205_PART_NUMBER_ID = t2060.C205_PART_NUMBER_ID (+)
       AND t205.C205_PART_NUMBER_ID = t205d.C205_PART_NUMBER_ID
       AND t205.C205_PART_NUMBER_ID = p_partnumber;
END gm_fch_qa_udi_param;

/*******************************************************
* Description : Procedure to use fetch the label param values
* Author   : Bala
*******************************************************/
PROCEDURE gm_fch_lbl_param (
        p_partnumber IN t205_part_number.c205_part_number_id%TYPE,
        p_report OUT TYPES.cursor_type)
AS
BEGIN
    OPEN p_report FOR  
    SELECT  v205g.c205d_general_spec GENERALSPEC, v205g.c205d_label_size LBLSIZE , v205g.c205d_size_format SIZEFMT,v205g.c205d_box_label BOXLABEL
    , v205g.c205d_patient_label PRINTLABEL,v205g.c205d_package_label PKGLABEL,v205g.c205d_cpc_patient_label CPCPATIENTNO
    , v205g.c205d_cpc_package_label CPCPACKAGENO, v205g.c205d_sample SAMPLE ,v205g.c205d_cpc_logo_path CPCLOGOPATH
    , v205g.c205d_material_spec MATERIALSPECLABEL, v205g.c205d_part_drawing PARTDRAWLABEL ,v205g.c205d_contract_process_client CPCID,v205g.c205d_processing_spec_type PROSPECTYP
   FROM v205g_part_label_parameter v205g WHERE v205g.C205_PART_NUMBER_ID = p_partnumber;
END gm_fch_lbl_param;

/******************************************************************
* Description : Procedure to fetch Issuing agency information
* Author      :
*******************************************************************/
PROCEDURE gm_fch_issuing_agency (
        p_out_issue_agency OUT TYPES.cursor_type)
AS
BEGIN
    OPEN p_out_issue_agency FOR SELECT t1600.c1600_issuing_agency_id id, t1600.c1600_issuing_agency_nm name,
    t1600.c1600_agency_short_nm shortNm FROM t1600_issue_agency t1600 WHERE t1600.c1600_void_fl IS NULL
    ORDER BY t1600.c1600_issuing_agency_id;
END gm_fch_issuing_agency;
/******************************************************************
* Description : Procedure to fetch configured details
* Author      :
*******************************************************************/
PROCEDURE gm_fch_config_name (
        p_out_config_nm OUT TYPES.cursor_type)
AS
BEGIN
    OPEN p_out_config_nm FOR SELECT t1610.c1610_issue_agency_config_id id, t1610.c1610_issue_agency_config_nm name FROM t1610_issue_agency_config t1610 WHERE
    t1610.c1610_void_fl IS NULL ORDER BY c1610_issue_agency_config_id;
END gm_fch_config_name;
/******************************************************************
* Description : Procedure to fetch the Issuing agency Identifier
* Author      :
*******************************************************************/
PROCEDURE gm_fch_issuing_agency_ident (
        p_issuing_id IN t1600_issue_agency.c1600_issuing_agency_id%TYPE,
        p_out_scar OUT TYPES.cursor_type)
AS
BEGIN
    OPEN p_out_scar FOR SELECT t1601.c1600_issuing_agency_id AG_ID, t1601.c1601_issue_ag_identifier_id ag_identifier_id
    , t1601.c1601_issue_ag_identifier_id ID, t1601.c1601_identify_symbol_name NM, t1601.c1601_identify_symbol IDENT_SYMBOL
    , DECODE (t1601.c1601_identify_symbol_name, 'DI', 103940, - 999) IDENT_SYMBOL_ID FROM
    t1601_issue_agency_identifier t1601 WHERE t1601.c1600_issuing_agency_id = NVL (p_issuing_id, t1601.c1600_issuing_agency_id) ;
END gm_fch_issuing_agency_ident;
/******************************************************************
* Description : Procedure to fetch saved configured details
* Author      :
*******************************************************************/
PROCEDURE gm_fch_configured_dtls (
        p_configure_id IN t1610_issue_agency_config.c1610_issue_agency_config_id%TYPE,
        p_out_configure OUT TYPES.cursor_type,
        p_out_name_dtls OUT TYPES.cursor_type)
AS
BEGIN
    OPEN p_out_name_dtls FOR SELECT t1610.c1600_issuing_agency_id issuingAgency, REPLACE(t1610.c1610_issue_agency_config_nm, gm_pkg_pd_rpt_udi.get_agency_short_nm (t1610.c1600_issuing_agency_id))
    txt_configName, gm_pkg_pd_rpt_udi.get_agn_config_sample_pattern (t1610.c1610_issue_agency_config_id) samplePattern,
    t1610.c1610_issue_agency_config_id configName FROM t1610_issue_agency_config t1610, t1600_issue_agency t1600 WHERE
    t1600.c1600_issuing_agency_id           = t1610.c1600_issuing_agency_id AND t1610.c1610_issue_agency_config_id =
    p_configure_id AND t1600.c1600_void_fl IS NULL AND t1610.c1610_void_fl IS NULL;
    --
    OPEN p_out_configure FOR SELECT t1601.c1601_issue_ag_identifier_id ID, t1601.c1601_identify_symbol_name NM,
    t1601.c1601_identify_symbol IDENT_SYMBOL, t1601.c1600_issuing_agency_id ag_id, t1611.c1611_seq_no seqno,
    t1611.c1611_issue_agency_conf_dtl_id confDtlId FROM t1601_issue_agency_identifier t1601, t1610_issue_agency_config
    t1610, t1611_issue_agency_config_dtl t1611 WHERE t1601.c1600_issuing_agency_id = t1610.c1600_issuing_agency_id AND
    t1601.c1601_issue_ag_identifier_id                                             = t1611.c1601_issue_ag_identifier_id
    AND t1610.c1610_issue_agency_config_id                                         = t1611.c1610_issue_agency_config_id
    AND t1610.c1610_issue_agency_config_id                                         = p_configure_id AND
    t1610.c1610_void_fl                                                           IS NULL ORDER BY t1611.c1611_seq_no;
END gm_fch_configured_dtls;
/*******************************************************
* Description : Get the Issue Agnecy short name
* Author   :
*******************************************************/
FUNCTION get_agency_short_nm (
        p_agency_id IN t1600_issue_agency.c1600_issuing_agency_id%TYPE)
    RETURN VARCHAR2
IS
    v_agency_short_nm t1600_issue_agency.c1600_agency_short_nm%TYPE;
BEGIN
    --
    BEGIN
         SELECT c1600_agency_short_nm
           INTO v_agency_short_nm
           FROM t1600_issue_agency
          WHERE c1600_issuing_agency_id = p_agency_id
            AND c1600_void_fl          IS NULL;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_agency_short_nm := NULL;
    END;
    RETURN v_agency_short_nm;
END get_agency_short_nm;
/*******************************************************
* Description : Get the Issue Agnecy Sample Pattern
* Author   :
*******************************************************/
FUNCTION get_agn_config_sample_pattern (
        p_agency_config_id IN t1610_issue_agency_config.c1610_issue_agency_config_id%TYPE)
    RETURN VARCHAR2
IS
    v_agency_sample_pattern VARCHAR2 (4000) ;
    v_final_sample_pattern VARCHAR2 (4000);
BEGIN
    --
    BEGIN
          SELECT v1610.issuing_agency_config_with_nm
   				INTO v_agency_sample_pattern
   			FROM v1610_issuing_agency_config v1610
  				WHERE v1610.c1610_issue_agency_config_id = p_agency_config_id;
  			--	
  			SELECT REPLACE(v_agency_sample_pattern, '|' ,'')
  				INTO v_final_sample_pattern
  			 FROM dual;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_agency_sample_pattern := NULL;
    END;
    RETURN v_final_sample_pattern;
END get_agn_config_sample_pattern;	

/*******************************************************
* Description : Get the part UDI Format
* Author   : Xun
*******************************************************/
FUNCTION get_part_udi_format (
    p_part_number IN t2060_di_part_map_log.C205_PART_NUMBER_ID%TYPE)
    RETURN VARCHAR2
IS
    v_udi_format v1610_issuing_agency_config.issuing_agency_config_with_id%TYPE;
BEGIN
     SELECT v1610.issuing_agency_config_with_id
     	INTO v_udi_format
	   FROM t205d_part_attribute t205d, v1610_issuing_agency_config v1610
	  WHERE t205d.c205_part_number_id   = p_part_number
	    AND t205d.c205d_attribute_value = v1610.c1610_issue_agency_config_id
	    AND t205d.c901_attribute_type   = 4000539
	    AND t205d.c205d_void_fl        IS NULL;
		 RETURN v_udi_format;
	  EXCEPTION
	   WHEN NO_DATA_FOUND
	   THEN
	      RETURN '';    
END get_part_udi_format;

/*******************************************************
* Description : Get the part UDI Format prefix
* Author   : Xun
*******************************************************/
FUNCTION get_udi_format_prefix (
    p_part_number IN t2060_di_part_map_log.C205_PART_NUMBER_ID%TYPE)
    RETURN VARCHAR2
IS
    v_udi_format v1610_issuing_agency_config.issuing_agency_config_with_id%TYPE;
    v_udi_format_prefix  VARCHAR2(100);
BEGIN
     SELECT v1610.issuing_agency_config_with_id
     	INTO v_udi_format
	   FROM t205d_part_attribute t205d, v1610_issuing_agency_config v1610
	  WHERE t205d.c205_part_number_id   = p_part_number
	    AND t205d.c205d_attribute_value = v1610.c1610_issue_agency_config_id
	    AND t205d.c901_attribute_type   = 4000539 --4000539-Issuing Agency Configuration.
	    AND t205d.c205d_void_fl        IS NULL;
	 --  103940  DI
	 SELECT  SUBSTR (v_udi_format, 1, INSTR (v_udi_format, '103940') - 1 ) 
	 	INTO v_udi_format_prefix
	 FROM DUAL;
	    
		 RETURN v_udi_format_prefix;
	  EXCEPTION
	   WHEN NO_DATA_FOUND
	   THEN
	      RETURN '';    
END get_udi_format_prefix;

/*******************************************************
* Description : Get the part PI config name
* Author   : Xun
*******************************************************/
FUNCTION get_pi_config_nm (
    p_part_number IN t2060_di_part_map_log.C205_PART_NUMBER_ID%TYPE)
    RETURN VARCHAR2
IS
    v_udi_format v1610_issuing_agency_config.issuing_agency_config_with_id%TYPE;
    v_pi_config_nm  VARCHAR2(4000);
    v_udi_format_prefix  VARCHAR2(100);
BEGIN
     SELECT v1610.issuing_agency_config_with_nm
     	INTO v_udi_format
	   FROM t205d_part_attribute t205d, v1610_issuing_agency_config v1610
	  WHERE t205d.c205_part_number_id   = p_part_number
	    AND t205d.c205d_attribute_value = v1610.c1610_issue_agency_config_id
	    AND t205d.c901_attribute_type   = 4000539 -- 4000539-Issuing Agency Configuration.
	    AND t205d.c205d_void_fl        IS NULL;
	 --  103940  DI
	 SELECT get_udi_format_prefix(p_part_number) || GET_CODE_NAME(103940) || '|'
	   INTO  v_udi_format_prefix
	   FROM DUAL;	 
	 
	 SELECT replace(replace(v_udi_format,v_udi_format_prefix,''),'|','')
	   INTO v_pi_config_nm 
	   FROM DUAL;
	   
	   RETURN v_pi_config_nm;
	  EXCEPTION
	   WHEN NO_DATA_FOUND
	   THEN
	      RETURN '';    
END get_pi_config_nm;

/*******************************************************
* Description : Get the part UDI number
* Author   : 
*******************************************************/
FUNCTION get_dhr_part_udi (
        p_dhr_id      IN t408_dhr.c408_dhr_id%TYPE,
        p_part_number IN t205_part_number.C205_PART_NUMBER_ID%TYPE)
    RETURN VARCHAR2
IS
    --
    v_ag_ident_id t1601_issue_agency_identifier.c1601_issue_ag_identifier_id%TYPE;
    v_ag_ident_symbol t1601_issue_agency_identifier.c1601_identify_symbol%TYPE;
    v_di_number T2060_DI_PART_MAPPING.C2060_DI_NUMBER%TYPE;
    v_part_att_value t205d_part_attribute.c205d_attribute_value%TYPE;
    v_ident_type t1602_issue_agency_symbol_dtl.c901_identifier%TYPE;
    v_ident_format t1602_issue_agency_symbol_dtl.c1602_identifier_format%TYPE;
    --
    v_final_str     VARCHAR2 (4000) ;
    v_manf_date DATE;
    v_exp_date  DATE;
    v_serial_num    VARCHAR2 (400) ;
    v_lot_num       VARCHAR2 (40) ;
    v_doner_id      VARCHAR2 (400) ;
    v_contro_num	VARCHAR2 (400) ;
    --
    v_pre_fix VARCHAR2(400);
    v_post_fix VARCHAR2(4000);
    v_exp_goes CHAR(1);
    --
    CURSOR agency_identifier
    IS
         SELECT t1611.c1601_issue_ag_identifier_id ag_identifier_id, t1610.c1610_issue_agency_config_id ag_conf_id,
            t1611.c1611_seq_no ag_seq, t1601.c1601_identify_symbol ag_ident_symbol
           FROM t1601_issue_agency_identifier t1601, t1610_issue_agency_config t1610, t1611_issue_agency_config_dtl
            t1611
          WHERE t1610.c1610_issue_agency_config_id = t1611.c1610_issue_agency_config_id
            AND t1601.c1601_issue_ag_identifier_id = t1611.c1601_issue_ag_identifier_id
            AND t1610.c1610_issue_agency_config_id = v_part_att_value
            AND t1610.c1610_void_fl               IS NULL
            AND t1611.c1611_void_fl               IS NULL
       ORDER BY t1611.c1611_seq_no;
    --
    CURSOR agency_identifier_dtls
    IS
         SELECT t1602.c901_identifier ident_type, t1602.c1602_identifier_format ident_format
           FROM t1602_issue_agency_symbol_dtl t1602
          WHERE t1602.c1601_issue_ag_identifier_id = v_ag_ident_id
            AND t1602.c1602_void_fl               IS NULL;
BEGIN
    -- 4000539 (Issue Agency Configure Id)
     SELECT get_part_attribute_value (p_part_number, 4000539)
       INTO v_part_att_value
       FROM dual;
    --103940 DI
    --103941 MFG date
    --103942 EXP date
    --103943 LOT
    --103944 SERIAL #
    --103945 Donor Id
    --4000688	Supplemental serial #
    IF TRIM (v_part_att_value) IS NOT NULL THEN
        FOR agency_ident_cur   IN agency_identifier
        LOOP
            --
            v_ag_ident_id     := agency_ident_cur.ag_identifier_id;
            v_ag_ident_symbol := agency_ident_cur.ag_ident_symbol;
            --
            v_pre_fix := NULL;
            v_post_fix := NULL;
            v_exp_goes := NULL;
            --
            v_pre_fix := v_ag_ident_symbol;
            --
            FOR agency_ident_dtls_cur IN agency_identifier_dtls
            LOOP
                --
                v_ident_type   := agency_ident_dtls_cur.ident_type;
                v_ident_format := agency_ident_dtls_cur.ident_format;
                -- DI 103940
                IF v_ident_type  = '103940' THEN
                    v_di_number := gm_pkg_pd_rpt_udi.get_part_di_number (p_part_number) ;
                    --
                    v_post_fix := v_di_number;
                ELSIF v_ident_type = '103941' THEN
                    -- 103941 MFG date
                    -- to get the mfg date
                    BEGIN
                        SELECT c408_manf_date
						   INTO v_manf_date
						   FROM t408_dhr
						  WHERE c205_part_number_id = p_part_number
						    AND c408_dhr_id         = p_dhr_id ;
                    EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                        v_manf_date := NULL;
                    END;
                    IF v_manf_date IS NOT NULL
                    THEN
                    	v_post_fix := gm_pkg_pd_rpt_udi.get_date_to_string(v_manf_date, v_ident_format);
                    END IF;	
                ELSIF v_ident_type = '103942' THEN
                    --103942 EXP date
                    IF v_ident_format IS NOT NULL THEN
                    -- to get the exp date
                        BEGIN
	                      SELECT c2550_expiry_date
						   INTO v_exp_date
						   FROM t408_dhr
						  WHERE c205_part_number_id = p_part_number
						    AND c408_dhr_id         = p_dhr_id ;
                        EXCEPTION
                        WHEN NO_DATA_FOUND THEN
                            v_exp_date := NULL;
                        END ;
                      --UDI-405 code has commented since the t2550 has no records while initiate MWO.  
                    -- we are getting the exp. date in t408 table (BBA change)
                    -- after move the BBA code this code will be remove.
                   /* SELECT NVL(c408_control_number,'-999')
                    	INTO v_contro_num
                     FROM t408_dhr WHERE c408_dhr_id = p_dhr_id;
                     --
                    SELECT MAX (c2550_expiry_date)
					    INTO v_exp_date
					   FROM T2550_PART_CONTROL_NUMBER
					  WHERE C205_PART_NUMBER_ID  = p_part_number
					    AND C2550_CONTROL_NUMBER = v_contro_num;*/
                    IF v_exp_date IS NOT NULL
                    THEN
                    	v_post_fix := gm_pkg_pd_rpt_udi.get_date_to_string(v_exp_date, v_ident_format);
                    END IF;
                    v_exp_goes := 'Y';
                    END IF; --end of v_ident_format
                    --103943 LOT
                ELSIF v_ident_type = '103943' THEN
                -- to get the lot number
                    BEGIN
                         SELECT c408_control_number
                           INTO v_lot_num
                           FROM t408_dhr
                          WHERE c205_part_number_id = p_part_number
                            AND c408_dhr_id         = p_dhr_id ;
                    EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                        v_lot_num := NULL;
                    END ;
                    IF v_lot_num IS NOT NULL
                    THEN
                    	v_post_fix := v_post_fix || v_lot_num;
                    END IF;
                    --103944 SERIAL #
                    --4000688	Supplemental serial #  
                ELSIF (v_ident_type = '103944' OR v_ident_type = '4000688') THEN
                    v_serial_num  := NULL;
                    -- to get the serial number
                     IF v_serial_num IS NOT NULL
                    THEN
                    	v_post_fix := v_post_fix || v_serial_num;
                    END IF;
                    --103945 Donor Id
                ELSIF v_ident_type = '103945' THEN
                -- to get the doner id
                 /*   BEGIN
                         SELECT c2540_donor_id
                           INTO v_doner_id
                           FROM t408_dhr
                          WHERE c205_part_number_id = p_part_number
                            AND c408_dhr_id         = p_dhr_id ;
                    EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                        v_doner_id := NULL;
                    END ;*/
                    IF v_doner_id IS NOT NULL
                    THEN
                    	v_post_fix := v_doner_id;
                    END IF;
                END IF; -- end of v_ident_type check
                --
            END LOOP;
            --
            IF v_post_fix IS NOT NULL
            THEN
            	v_final_str := v_final_str || v_pre_fix || v_post_fix;
            END IF;	
            --
        END LOOP;
    END IF;
    RETURN v_final_str;
END get_dhr_part_udi;
/*******************************************************
* Description : using this fun. to Get date to string
* Author   :
*******************************************************/
FUNCTION get_date_to_string (
        p_date        IN DATE,
        p_date_format IN VARCHAR2)
    RETURN VARCHAR2
IS
    v_month  VARCHAR2 (20) ;
    v_date   VARCHAR2 (20) ;
    v_year_2 VARCHAR2 (20) ;
    v_year_4 VARCHAR2 (20) ;
    v_year_3 VARCHAR2 (20) ;
    v_hour   VARCHAR2 (20) ;
    v_min    VARCHAR2 (20) ;
    v_julian VARCHAR2 (20) ;
    --
    v_return_str VARCHAR2 (400) ;
    v_ident_format VARCHAR2 (400);
BEGIN
    v_ident_format := p_date_format;
    BEGIN
         SELECT TO_CHAR (p_date, 'MM'), TO_CHAR (p_date, 'DD'), TO_CHAR (p_date, 'YY')
          , TO_CHAR (p_date, 'YYYY'), TO_CHAR (p_date, 'HH'), TO_CHAR (p_date, 'MI')
          , TO_CHAR (p_date, 'J'), TO_CHAR(p_date, 'YYY')
           INTO v_month, v_date, v_year_2
          , v_year_4, v_hour, v_min
          , v_julian, v_year_3
           FROM DUAL ;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_month  := NULL;
        v_date   := NULL;
        v_year_2 := NULL;
        v_year_4 := NULL;
        v_year_3 := NULL;
        v_hour   := NULL;
        v_min    := NULL;
        v_julian := NULL;
    END ;
    IF v_month         IS NOT NULL THEN
        v_ident_format := REPLACE (v_ident_format, 'MM', v_month) ;
        v_ident_format := REPLACE (v_ident_format, 'DD', v_date) ;
        v_ident_format := REPLACE (v_ident_format, 'YYYY', v_year_4) ;
        v_ident_format := REPLACE (v_ident_format, 'YYY', v_year_3) ;
        v_ident_format := REPLACE (v_ident_format, 'YY', v_year_2) ;
        v_ident_format := REPLACE (v_ident_format, 'HH', v_hour) ;
        -- to split last 3 digit
        v_julian       := SUBSTR (v_julian, LENGTH (v_julian) - 2, LENGTH (v_julian)) ;
        v_ident_format := REPLACE (v_ident_format, 'JJJ', v_julian) ;
        v_return_str   := v_ident_format;
    END IF;
    RETURN v_return_str;
END get_date_to_string;
/******************************************************************
* Description : Procedure to fetch Prod Part Dev UDI Details
* Author      :
*******************************************************************/
PROCEDURE gm_fch_prod_part_dev_udi_dtls (
        p_partnumber IN t205_part_number.c205_part_number_id%TYPE,
        p_report OUT TYPES.cursor_type)
AS
BEGIN
    OPEN p_report FOR SELECT t205.c205_part_number_id PNUM, t205.c202_project_id PROJECTID, t205.c205_part_num_desc
    PART_DESC, v205g.c205d_contains_latex CONTAIN_LATEX, v205g.c205d_require_steril REQUIRE_STERILE,
    v205g.c205d_steril_method STERILE_METHOD, v205g.c205d_device_available DEVICE_AVALIABLE, v205g.c205d_size1_type
    SIZE1_TYPE, v205g.c205d_size1_type_text SIZE1_TYPE_TEXT, v205g.c205d_size1_value SIZE1_VALUE, v205g.c205d_size1_uom
    SIZE1_UOM, v205g.c205d_size2_type SIZE2_TYPE, v205g.c205d_size2_type_text SIZE2_TYPE_TEXT, v205g.c205d_size2_value
    SIZE2_VALUE, v205g.c205d_size2_uom SIZE2_UOM, v205g.c205d_size3_type SIZE3_TYPE, v205g.c205d_size3_type_text
    SIZE3_TYPE_TEXT, v205g.c205d_size3_value SIZE3_VALUE, v205g.c205d_size3_uom SIZE3_UOM, v205g.c205d_size4_type
    SIZE4_TYPE, v205g.c205d_size4_type_text SIZE4_TYPE_TEXT, v205g.c205d_size4_value SIZE4_VALUE, v205g.c205d_size4_uom
    SIZE4_UOM, GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1078) CONTAIN_LATEX_FL -- 104460 Contains Latex?
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1079) REQUIRE_STERILE_FL -- 104461 Requires sterilization prior to use
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1080) STERILE_METHOD_FL -- 104462 Sterilization method
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1081) DEVICE_AVALIABLE_FL -- 104463 Is Device available in more than one size
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1082) SIZE1_TYPE_FL -- 104464 Type
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1083) SIZE1_TYPE_TEXT_FL -- 104465 Size Type Text
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1084) SIZE1_VALUE_FL -- 104466 Value
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1085) SIZE1_UOM_FL -- 104467 U of M
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1086) SIZE2_TYPE_FL -- 104468 Type
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1087) SIZE2_TYPE_TEXT_FL -- 104469 Size Type Text
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1088) SIZE2_VALUE_FL -- 104470 Value
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1089) SIZE2_UOM_FL -- 104471 U of M
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1090) SIZE3_TYPE_FL -- 104472 Type
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1091) SIZE3_TYPE_TEXT_FL -- 104473 Size Type Text
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1092) SIZE3_VALUE_FL -- 104474 Value
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1093) SIZE3_UOM_FL -- 104475 U of M
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1094) SIZE4_TYPE_FL -- 104476 Type
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1095) SIZE4_TYPE_TEXT_FL -- 104477 Size Type Text
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1096) SIZE4_VALUE_FL -- 104478 Value
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1097) SIZE4_UOM_FL -- 104479 U of M
    , DECODE (t205.c205_product_class, '4030', 'Y', 'N') sterilePart
    , v205g.c205d_serial_num_need SERIALNUMNEEDFL --104480 Lot tracking required ?
    FROM v205g_part_pd_parameter v205g, t205_part_number t205 WHERE t205.C205_PART_NUMBER_ID = p_partnumber
    	AND t205.C205_PART_NUMBER_ID = V205G.C205_PART_NUMBER_ID (+);
END gm_fch_prod_part_dev_udi_dtls;
/******************************************************************
* Description : Procedure to fetch Prod Part Print Dev UDI Details
* Author      :
*******************************************************************/
PROCEDURE gm_fch_pd_print_dtls (
        p_project_id IN t205g_pd_part_number.c202_project_id%TYPE,
        p_inputstr   IN CLOB,
        p_stropt	 IN VARCHAR2,
        p_userid     IN t205g_pd_part_number.c205g_last_updated_by%TYPE,
        p_header_dtls OUT TYPES.cursor_type,
        p_report_dtls OUT TYPES.cursor_type)
AS
BEGIN
    OPEN p_header_dtls FOR SELECT get_project_name (p_project_id) projectnm, get_user_name (p_userid) printby, TO_CHAR (
	sysdate, get_rule_value ('DATEFMT', 'DATEFORMAT') || ' HH:MI:SS AM') printdttime FROM dual;
	--
	my_context.set_my_cloblist (p_inputstr||',') ;
	--
	IF (p_stropt = 'Print')
	THEN
	    OPEN p_report_dtls FOR SELECT t205g.c205_part_number_id pnum, UTL_I18N.unescape_reference (t205g.c205_part_num_desc
	    ) partdesc, t205g.c202_project_id projectid, get_project_name (t205g.c202_project_id) projectNm,
	    t205g.c901_has_latex latexId, get_code_name (t205g.c901_has_latex) latexNm, t205g.c901_requires_sterilization reqId
	    , get_code_name ( t205g.c901_requires_sterilization) reqNm, t205g.c901_sterilization_method sterilId, get_code_name
	    ( t205g.c901_sterilization_method) sterilNm, gm_pkg_pd_rpt_udi.get_pd_size_value (t205g.c205_part_number_id)
	    partsize FROM t205g_pd_part_number t205g WHERE t205g.c205g_void_fl IS NULL AND EXISTS
	    (
	         SELECT TO_CHAR (token)
	           FROM v_clob_list
	          WHERE token          IS NOT NULL
	            AND TO_CHAR (token) = t205g.c205_part_number_id
	    )
	    ORDER BY t205g.c205_part_number_id;
    ELSE
    OPEN p_report_dtls FOR SELECT V205G.C205_PART_NUMBER_ID pnum, UTL_I18N.unescape_reference (V205G.part_desc)
    partdesc, V205G.PROJECTID projectid, get_project_name (V205G.PROJECTID) projectNm, TO_NUMBER (
    V205G.C205D_CONTAINS_LATEX) latexId, get_code_name (TO_NUMBER (V205G.C205D_CONTAINS_LATEX)) latexNm, TO_NUMBER (
    V205G.C205D_REQUIRE_STERIL) reqId, get_code_name (TO_NUMBER (V205G.C205D_REQUIRE_STERIL)) reqNm, TO_NUMBER (
    V205G.C205D_STERIL_METHOD) sterilId, get_code_name (TO_NUMBER (V205G.C205D_STERIL_METHOD)) sterilNm,
    gm_pkg_pd_rpt_udi.get_pd_size_value (V205G.C205_PART_NUMBER_ID) partsize FROM V205G_PART_PD_PARAMETER V205G
    WHERE EXISTS
    (
         SELECT TO_CHAR (token)
           FROM v_clob_list
          WHERE token                    IS NOT NULL
            AND V205G.c205_part_number_id = TO_CHAR (token)
    )
    ORDER BY V205G.c205_part_number_id;
    END IF;
END gm_fch_pd_print_dtls;

/*******************************************************
* Description : Get the part number count
* Author   :
*******************************************************/
FUNCTION get_part_number_cnt (
        p_partnumber IN t205g_pd_part_number.c205_part_number_id%TYPE)
    RETURN VARCHAR2
AS
    v_cnt NUMBER;
BEGIN
     SELECT COUNT (1)
       INTO v_cnt
       FROM t205_part_number
      WHERE c205_part_number_id = p_partnumber;
    RETURN v_cnt;
END get_part_number_cnt;
/*******************************************************
* Description : Get the project count
* Author   :
*******************************************************/
FUNCTION get_project_cnt (
        p_project IN t205g_pd_part_number.c202_project_id%TYPE)
    RETURN VARCHAR2
AS
    v_project_cnt NUMBER;
BEGIN
     SELECT COUNT (1)
       INTO v_project_cnt
       FROM t202_project
      WHERE c202_project_id = p_project;
    RETURN v_project_cnt;
END get_project_cnt;
/*******************************************************
* Description : Function used to get the part size information
* Author   :
*******************************************************/
FUNCTION get_pd_size_value (
        p_part_number_id IN t205_part_number.c205_part_number_id%TYPE)
    RETURN VARCHAR2
AS
    v_pd_size_dtls     VARCHAR2 (4000) ;
    v_part_cnt         NUMBER;
    v_pd_size1         VARCHAR2 (4000) ;
    v_pd_size2         VARCHAR2 (4000) ;
    v_pd_size3         VARCHAR2 (4000) ;
    v_pd_size4         VARCHAR2 (4000) ;
    v_device_available VARCHAR2 (20) ;
BEGIN
    --
    v_part_cnt := gm_pkg_pd_rpt_udi.get_part_number_cnt (p_part_number_id) ;
    -- 104047 Others, Specify
    BEGIN
         SELECT NVL (v205g.c205d_device_available, t205g.c901_has_more_than_one_size)
           INTO v_device_available
           FROM v205g_part_pd_parameter v205g, t205g_pd_part_number t205g
          WHERE t205g.c205_part_number_id = v205g.c205_part_number_id (+)
            AND t205g.c205_part_number_id = p_part_number_id;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_device_available := NULL;
    END;
    --
    IF v_device_available = '80130' -- Yes
        THEN
        IF v_part_cnt <> 0 THEN
            BEGIN
                -- size
                 SELECT TRIM (V205G.C205D_SIZE1_VALUE || get_code_name_alt (c205d_size1_uom) || ' '||DECODE (
                    V205G.C205D_SIZE1_TYPE, 104047, v205g.C205D_SIZE1_TYPE_TEXT, get_code_name (V205G.C205D_SIZE1_TYPE)
                    )), TRIM (V205G.C205D_SIZE2_VALUE || get_code_name_alt (c205d_size2_uom) || ' '||DECODE (
                    V205G.C205D_SIZE2_TYPE, 104047, v205g.C205D_SIZE2_TYPE_TEXT, get_code_name (V205G.C205D_SIZE2_TYPE)
                    )), TRIM (V205G.C205D_SIZE3_VALUE || get_code_name_alt (c205d_size3_uom) || ' '||DECODE (
                    V205G.C205D_SIZE3_TYPE, 104047, v205g.C205D_SIZE3_TYPE_TEXT, get_code_name (V205G.C205D_SIZE3_TYPE)
                    )), TRIM (V205G.C205D_SIZE4_VALUE || get_code_name_alt (c205d_size4_uom) || ' '||DECODE (
                    V205G.C205D_SIZE4_TYPE, 104047, v205g.C205D_SIZE4_TYPE_TEXT, get_code_name (V205G.C205D_SIZE4_TYPE)
                    ))
                   INTO v_pd_size1, v_pd_size2, v_pd_size3
                  , v_pd_size4
                   FROM v205g_part_pd_parameter v205g
                  WHERE v205g.c205_part_number_id = p_part_number_id;
            EXCEPTION
            WHEN NO_DATA_FOUND THEN
                v_pd_size1 := NULL;
                v_pd_size2 := NULL;
                v_pd_size3 := NULL;
                v_pd_size4 := NULL;
            END;
        ELSE
            BEGIN
                 SELECT TRIM (t205g.c205g_size1_value || get_code_name_alt (t205g.c901_size1_uom) || ' '|| DECODE (
                    t205g.c901_size1_type, 104047, t205g.c205g_size1_type_text, get_code_name (t205g.c901_size1_type)))
                    , TRIM (t205g.c205g_size2_value || get_code_name_alt (t205g.c901_size2_uom) || ' '|| DECODE (
                    t205g.c901_size2_type, 104047, t205g.c205g_size2_type_text, get_code_name (t205g.c901_size2_type)))
                    , TRIM (t205g.c205g_size3_value || get_code_name_alt (t205g.c901_size3_uom) || ' '|| DECODE (
                    t205g.c901_size3_type, 104047, t205g.c205g_size3_type_text, get_code_name (t205g.c901_size3_type)))
                    , TRIM (t205g.c205g_size4_value || get_code_name_alt (t205g.c901_size4_uom) || ' '|| DECODE (
                    t205g.c901_size4_type, 104047, t205g.c205g_size4_type_text, get_code_name (t205g.c901_size4_type)))
                   INTO v_pd_size1, v_pd_size2, v_pd_size3
                  , v_pd_size4
                   FROM t205g_pd_part_number t205g
                  WHERE t205g.c205_part_number_id = p_part_number_id;
            EXCEPTION
            WHEN NO_DATA_FOUND THEN
                v_pd_size1 := NULL;
                v_pd_size2 := NULL;
                v_pd_size3 := NULL;
                v_pd_size4 := NULL;
            END;
        END IF;
        --
        IF v_pd_size1      IS NOT NULL THEN
            v_pd_size_dtls := v_pd_size1;
        END IF;
        --
        IF v_pd_size2      IS NOT NULL AND v_pd_size_dtls IS NOT NULL THEN
            v_pd_size_dtls := v_pd_size_dtls ||' X '|| v_pd_size2;
        ELSE
            v_pd_size_dtls := v_pd_size_dtls || v_pd_size2;
        END IF;
        --
        IF v_pd_size3      IS NOT NULL AND v_pd_size_dtls IS NOT NULL THEN
            v_pd_size_dtls := v_pd_size_dtls ||' X '|| v_pd_size3;
        ELSE
            v_pd_size_dtls := v_pd_size_dtls || v_pd_size3;
        END IF;
        --
        IF v_pd_size4      IS NOT NULL AND v_pd_size_dtls IS NOT NULL THEN
            v_pd_size_dtls := v_pd_size_dtls ||' X '|| v_pd_size4;
        ELSE
            v_pd_size_dtls := v_pd_size_dtls || v_pd_size4;
        END IF;
        --
    END IF; -- v_device_available
    RETURN v_pd_size_dtls;
END get_pd_size_value;
/***************************************************************
* Description : Procedure used to fetch the Regulatory tab Param
* Author   :
****************************************************************/
PROCEDURE gm_fch_qa_regulatory_param (
        p_partnumber IN t205_part_number.c205_part_number_id%TYPE,
        p_report OUT TYPES.cursor_type)
AS
BEGIN
    OPEN p_report FOR SELECT t205.c205_part_number_id pnum, t205.c202_project_id projectId, t205.c205_part_num_desc part_desc
  , v205g.c205d_Brand_Name Brand_Name, v205g.c205d_US_Reg_Classification US_Reg_Classification,
    v205g.c205d_US_Reg_Pathway US_Reg_Pathway, v205g.c205d_FDA_Sub_No1 FDA_Sub_No1, v205g.c205d_FDA_Sub_No2 FDA_Sub_No2
  , v205g.c205d_FDA_Sub_No3 FDA_Sub_No3, v205g.c205d_FDA_Sub_No4 FDA_Sub_No4, v205g.c205d_FDA_Sub_No5 FDA_Sub_No5
  , v205g.c205d_FDA_Sub_No6 FDA_Sub_No6, v205g.c205d_FDA_Sub_No7 FDA_Sub_No7, v205g.c205d_FDA_Sub_No8 FDA_Sub_No8
  , v205g.c205d_FDA_Sub_No9 FDA_Sub_No9, v205g.c205d_FDA_Sub_No10 FDA_Sub_No10, v205g.c205d_FDA_Approval_Date
    FDA_Approval_Date, v205g.c205d_FDA_NTF_Reported FDA_NTF_Reported, v205g.c205d_FDA_Listing FDA_Listing
  , v205g.c205d_PMA_Sup_No1 PMA_Sup_No1, v205g.c205d_PMA_Sup_No2 PMA_Sup_No2, v205g.c205d_PMA_Sup_No3 PMA_Sup_No3
  , v205g.c205d_PMA_Sup_No4 PMA_Sup_No4, v205g.c205d_PMA_Sup_No5 PMA_Sup_No5, v205g.c205d_PMA_Sup_No6 PMA_Sup_No6
  , v205g.c205d_PMA_Sup_No7 PMA_Sup_No7, v205g.c205d_PMA_Sup_No8 PMA_Sup_No8, v205g.c205d_PMA_Sup_No9 PMA_Sup_No9
  , v205g.c205d_PMA_Sup_No10 PMA_Sup_No10, v205g.c205d_FDA_Product_Code1 FDA_Product_Code1,
    v205g.c205d_FDA_Product_Code2 FDA_Product_Code2, v205g.c205d_FDA_Product_Code3 FDA_Product_Code3,
    v205g.c205d_FDA_Product_Code4 FDA_Product_Code4, v205g.c205d_FDA_Product_Code5 FDA_Product_Code5,
    v205g.c205d_FDA_Product_Code6 FDA_Product_Code6, v205g.c205d_FDA_Product_Code7 FDA_Product_Code7,
    v205g.c205d_FDA_Product_Code8 FDA_Product_Code8, v205g.c205d_FDA_Product_Code9 FDA_Product_Code9,
    v205g.c205d_FDA_Product_Code10 FDA_Product_Code10, v205g.c205d_EU_Reg_Pathway EU_Reg_Pathway, v205g.c205d_GMDN_Code
    GMDN_Code, v205g.c205d_EU_CE_Tech_File_Num EU_CE_Tech_File_Num, v205g.c205d_EU_CE_Tech_File_Rev EU_CE_Tech_File_Rev
  , v205g.c205d_Notes Notes,GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1120) US_Reg_Classification_Fl
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1121) US_Reg_Pathway_Fl 
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1122) FDA_Product_Code1_Fl
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1123) FDA_Product_Code2_Fl 
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1124) FDA_Product_Code3_Fl
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1125) FDA_Product_Code4_Fl
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1126) FDA_Product_Code5_Fl 
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1127) FDA_Product_Code6_Fl 
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1128) FDA_Product_Code7_Fl
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1129) FDA_Product_Code8_Fl 
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1130) FDA_Product_Code9_Fl 
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1131) FDA_Product_Code10_Fl 
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1132) EU_Reg_Pathway_Fl 
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1133) EU_CE_Tech_File_Rev_Fl
    --
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1151) brand_Name_Fl
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1172) fda_listing_Fl
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1173) gmdn_code_Fl
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1152) fda_sub_num1_Fl
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1153) fda_sub_num2_Fl
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1154) fda_sub_num3_Fl
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1155) fda_sub_num4_Fl
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1156) fda_sub_num5_Fl
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1157) fda_sub_num6_Fl
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1158) fda_sub_num7_Fl
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1159) fda_sub_num8_Fl
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1160) fda_sub_num9_Fl
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1161) fda_sub_num10_Fl
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1162) fda_sup_num1_Fl
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1163) fda_sup_num2_Fl
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1164) fda_sup_num3_Fl
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1165) fda_sup_num4_Fl
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1166) fda_sup_num5_Fl
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1167) fda_sup_num6_Fl
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1168) fda_sup_num7_Fl
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1169) fda_sup_num8_Fl
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1170) fda_sup_num9_Fl
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1171) fda_sup_num10_Fl
    , GET_AUDIT_LOG_FLAG (t205.C205_PART_NUMBER_ID, 1174) exempt_sub_fl
    , get_code_name(v205g.c205d_exempt_submission) exempt_submission
   FROM v205g_part_regulatory_param v205g, t205_part_number t205
  WHERE t205.c205_part_number_id = v205g.c205_part_number_id (+)
  AND t205.c205_part_number_id = p_partnumber;
       
END gm_fch_qa_regulatory_param;

END gm_pkg_pd_rpt_udi;
/
