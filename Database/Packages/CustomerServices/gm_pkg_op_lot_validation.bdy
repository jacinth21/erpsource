CREATE OR REPLACE
PACKAGE BODY gm_pkg_op_lot_validation
IS

/*************************************************************************************************************************************
	* Description : This procedure is used to return the error message and Error type when the user give the wrong lot number for PMT-29861
	* Author     : Swetha
	* Parameters  : Lot,part#
****************************************************************************************************************************************/
PROCEDURE gm_validate_lot_number(
	 p_usage_lot IN t2550_part_control_number.C2550_CONTROL_NUMBER%TYPE,
	 p_part_num  IN t2550_part_control_number.C205_PART_NUMBER_ID%TYPE,
	 p_field_sales_id IN t2550_part_control_number.C2550_REF_ID%TYPE,
	 p_lot_error_msg OUT VARCHAR2,
	 p_error_type OUT t5063_lot_error_info.C901_ERROR_TYPE%TYPE
	 )
	 
	AS
	 v_usage_lot t2550_part_control_number.C2550_CONTROL_NUMBER%TYPE := UPPER(p_usage_lot);
	 v_lot_error_type t5063_lot_error_info.C901_ERROR_TYPE%TYPE;
	 v_lot_error_msg    VARCHAR2(200);
	 v_company_id t2550_part_control_number.c1900_company_id%TYPE;
     v_plant_id t2550_part_control_number.c5040_plant_id%TYPE;	
     v_flag			VARCHAR2(1) := 'N';
	 
	 
  BEGIN
 
      SELECT get_compid_frm_cntx(), get_plantid_frm_cntx() 
			INTO v_company_id ,v_plant_id
			FROM DUAL; 
			
	--validating the part is required to track lot 
      SELECT get_part_attr_value_by_comp(p_part_num,'104480',v_company_id) 
      INTO v_flag FROM DUAL; 
    
 
  IF p_usage_lot = '' OR p_usage_lot IS NULL 
  THEN
    RETURN;
   END IF;
  
  
  
  -- validate whether the lot is available 
gm_validate_lot_available(v_usage_lot,p_part_num,v_lot_error_msg,v_lot_error_type);

   IF v_lot_error_msg IS NOT NULL
     THEN
      p_lot_error_msg := v_lot_error_msg;
      p_error_type := v_lot_error_type;
    RETURN;
   END IF;
   
   
 -- validate whether the entered lot is used in existing order
gm_validate_lot_is_billed(v_usage_lot,p_part_num,v_lot_error_msg,v_lot_error_type);

IF v_lot_error_msg IS NOT NULL
     THEN
      p_lot_error_msg := v_lot_error_msg;
      p_error_type := v_lot_error_type;
    RETURN;
   END IF;

 
           
  IF v_flag = 'Y' -- for FS Mismatch error and invalid inventory error type
       THEN
           
-- validate whether the entered lot is in Field sales warehouse
gm_validate_lot_inventory(v_usage_lot,p_part_num,v_lot_error_msg,v_lot_error_type);

  IF v_lot_error_msg IS NOT NULL
     THEN
      p_lot_error_msg := v_lot_error_msg;
      p_error_type := v_lot_error_type;
    RETURN;
   END IF;

-- validate whether there is Field Sales mismatch
gm_validate_lot_fs_mismatch(v_usage_lot,p_part_num,p_field_sales_id,v_lot_error_msg,v_lot_error_type);

IF v_lot_error_msg IS NOT NULL
     THEN
      p_lot_error_msg := v_lot_error_msg;
      p_error_type := v_lot_error_type;
    RETURN;
   END IF;
   
  END IF;

END gm_validate_lot_number;




/*************************************************************************************************************************************
	* Description : This procedure is used to return the error message and Error type when the user enters the lot no which is not available for PMT-29861
	* Author     : Swetha
	* Parameters  : Lot,part#
	****************************************************************************************************************************************/
	PROCEDURE gm_validate_lot_available(
	 p_usage_lot IN t2550_part_control_number.C2550_CONTROL_NUMBER%TYPE,
	 p_part_num  IN t2550_part_control_number.C205_PART_NUMBER_ID%TYPE,
	 p_lot_error_msg OUT VARCHAR2,
	 p_error_type OUT t5063_lot_error_info.C901_ERROR_TYPE%TYPE
	 )
	  
	  AS
	    v_usage_lot t2550_part_control_number.C2550_CONTROL_NUMBER%TYPE := UPPER(p_usage_lot);
	    v_lot_count NUMBER;
	    v_lot_error_type t5063_lot_error_info.C901_ERROR_TYPE%TYPE := 107972; --Invalid lot
	    v_company_id t2550_part_control_number.c1900_company_id%TYPE;
        v_plant_id t2550_part_control_number.c5040_plant_id%TYPE;	
	    
	  BEGIN		
	  
	    SELECT get_compid_frm_cntx(), get_plantid_frm_cntx() 
			INTO v_company_id ,v_plant_id
			FROM DUAL;  

    SELECT COUNT (1)
			  INTO v_lot_count
			FROM t2550_part_control_number
			WHERE c205_part_number_id = p_part_num
			AND UPPER (TRIM (c2550_control_number)) = v_usage_lot;
			--AND c1900_company_id = v_company_id
			--AND c5040_plant_id = v_plant_id ;
			
      			
		  IF (v_lot_count = 0) THEN
						p_lot_error_msg := ' Allograft ID ' || v_usage_lot || ' entered for the part number ' || p_part_num || ' is invalid' ;
			            p_error_type := v_lot_error_type;
			
			
		 ELSE
           
            p_lot_error_msg := NULL;	
            p_error_type := NULL;
            
	  	END IF;
			
	END gm_validate_lot_available;
	
	
	
/*************************************************************************************************************************************
	* Description : This procedure is used to return the error message and Error type when the lot entered is not available in FieldSales warehouse for PMT-29861
	* Author     : Swetha
	* Parameters  : Lot,part#
	****************************************************************************************************************************************/
	PROCEDURE gm_validate_lot_inventory(
	 p_usage_lot IN t2550_part_control_number.C2550_CONTROL_NUMBER%TYPE,
	 p_part_num  IN t2550_part_control_number.C205_PART_NUMBER_ID%TYPE,
	 p_lot_error_msg OUT VARCHAR2,
	 p_error_type OUT t5063_lot_error_info.C901_ERROR_TYPE%TYPE
	 )
	  
	  AS
	    v_usage_lot t2550_part_control_number.C2550_CONTROL_NUMBER%TYPE := UPPER(p_usage_lot);
	    v_lot_count NUMBER;
	    v_lot_error_type t5063_lot_error_info.C901_ERROR_TYPE%TYPE := 107974; --Invalid Inventory
	    v_company_id t2550_part_control_number.c1900_company_id%TYPE;
        v_plant_id t2550_part_control_number.c5040_plant_id%TYPE;	
	    
	  BEGIN	
	  
	    
	    SELECT get_compid_frm_cntx(), get_plantid_frm_cntx() 
			INTO v_company_id ,v_plant_id
			FROM DUAL;  	  
-- Added Account Qty in the below fetch PMT-41126
    SELECT COUNT (1)
			  INTO v_lot_count
			FROM t2550_part_control_number
			WHERE c205_part_number_id = p_part_num
			AND UPPER (TRIM (c2550_control_number)) = v_usage_lot
			AND c901_last_updated_warehouse_type IN (4000339,56002);--Field Sales, Account Qty
			--AND c1900_company_id = v_company_id
			--AND c5040_plant_id = v_plant_id;
			
		      			
		  IF (v_lot_count = 0) THEN
						p_lot_error_msg := 'The Control Number ' || v_usage_lot || ' entered for the part number ' || p_part_num || ' is in invalid Inventory' ;
			            p_error_type := v_lot_error_type;
			
		 ELSE
           
            p_lot_error_msg := NULL;	
            p_error_type := NULL;
            
	  	END IF;
			
	END gm_validate_lot_inventory;
	
	
	
	
	/*************************************************************************************************************************************
	* Description : This procedure is used to return the error message and Error type when the lot is already billed for PMT-29861
	* Author     : Swetha
	* Parameters  : Lot,part#
	****************************************************************************************************************************************/
	PROCEDURE gm_validate_lot_is_billed(
	 p_usage_lot IN t2550_part_control_number.C2550_CONTROL_NUMBER%TYPE,
	 p_part_num  IN t2550_part_control_number.C205_PART_NUMBER_ID%TYPE,
	 p_lot_error_msg OUT VARCHAR2,
	 p_error_type OUT t5063_lot_error_info.C901_ERROR_TYPE%TYPE
	 )
	  
	  AS
	   v_order_id t2550_part_control_number.c2550_last_updated_trans_id%TYPE;
	   v_lot_error_type t5063_lot_error_info.C901_ERROR_TYPE%TYPE := 107973; --Billed Lot
	   v_company_id t2550_part_control_number.c1900_company_id%TYPE;
       v_plant_id t2550_part_control_number.c5040_plant_id%TYPE;
       v_lot_status t2550_part_control_number.c2550_lot_status%TYPE;	
	   
	    
	    BEGIN
	    
	      SELECT get_compid_frm_cntx(), get_plantid_frm_cntx() 
			INTO v_company_id ,v_plant_id
			FROM DUAL;  
	  
	   
	   
       BEGIN
	   
       SELECT LOTSTATUS, ORDERID  
			INTO v_lot_status, v_order_id  
		  FROM
		  (   
	         SELECT t2550.c2550_lot_status LOTSTATUS, t2550.c2550_last_updated_trans_id ORDERID
		  FROM t2550_part_control_number t2550
		WHERE t2550.C205_PART_NUMBER_ID = p_part_num
		   AND UPPER (TRIM (t2550.C2550_CONTROL_NUMBER)) = UPPER(p_usage_lot)
		   --AND t2550.c1900_company_id = v_company_id
		   --AND t2550.c5040_plant_id = v_plant_id
		ORDER BY t2550.c2550_last_updated_trans_date DESC
	          )
	         WHERE ROWNUM = 1;
			
          EXCEPTION
    WHEN NO_DATA_FOUND THEN
           v_order_id := NULL;
		END;
		
		 -- sold 105006  implanted 105000
	 IF v_lot_status = 105006 OR v_lot_status = 105000 
         THEN
            p_lot_error_msg  := ' Allograft ID ' || UPPER(p_usage_lot) || ' has already been billed in order ' || v_order_id ;
            p_error_type := v_lot_error_type;
           
         ELSE
         
            p_lot_error_msg := NULL;
            p_error_type := NULL;
            
		END IF;
			
			
	END gm_validate_lot_is_billed;
	
	
	
	/*************************************************************************************************************************************
	* Description : This procedure is used to return the error message and Error type when there is field sales mismatch in the usage lot number for PMT-29861
	* Author     : Swetha
	* Parameters  : Lot,part#,FieldSales id
	****************************************************************************************************************************************/
	PROCEDURE gm_validate_lot_fs_mismatch(
	 p_usage_lot IN t2550_part_control_number.C2550_CONTROL_NUMBER%TYPE,
	 p_part_num  IN t2550_part_control_number.C205_PART_NUMBER_ID%TYPE,
	 p_field_sales_id IN t2550_part_control_number.C2550_REF_ID%TYPE,
	 p_lot_error_msg OUT VARCHAR2,
	 p_error_type OUT t5063_lot_error_info.C901_ERROR_TYPE%TYPE
	)
	  
	  AS
	    v_usage_lot t2550_part_control_number.C2550_CONTROL_NUMBER%TYPE := UPPER(p_usage_lot);
	    v_lot_error_type t5063_lot_error_info.C901_ERROR_TYPE%TYPE := 107975; --Field Sales Mismatch
	    v_usage_fs_id t2550_part_control_number.C2550_REF_ID%TYPE;
	    v_company_id t2550_part_control_number.c1900_company_id%TYPE;
        v_plant_id t2550_part_control_number.c5040_plant_id%TYPE;
	    
	  BEGIN	
	  
	      SELECT get_compid_frm_cntx(), get_plantid_frm_cntx() 
			INTO v_company_id ,v_plant_id
			FROM DUAL; 	  

     BEGIN	
         
         SELECT t2550.c2550_ref_id 
	     INTO v_usage_fs_id
	    FROM t2550_part_control_number t2550
		  WHERE t2550.c205_part_number_id = p_part_num
			 AND UPPER (TRIM (t2550.c2550_control_number)) = v_usage_lot;
			 --AND t2550.c1900_company_id = v_company_id
			-- AND t2550.c5040_plant_id = v_plant_id;
	    EXCEPTION
            WHEN NO_DATA_FOUND THEN
                v_usage_fs_id := NULL;
     END;
			

            
	    IF ( NVL(p_field_sales_id, '-999') !=  NVL(v_usage_fs_id, '-999')) THEN 
			
	               p_lot_error_msg := ' Allograft ID ' || v_usage_lot || ' is currently consigned to another Field Sales ' ;
			       p_error_type := v_lot_error_type;
			
			ELSE
           
             p_lot_error_msg := NULL;	
             p_error_type := NULL;
            
	  	END IF;
	
  END gm_validate_lot_fs_mismatch;
  
  /*************************************************************************************************************************************
	* Description : This procedure is used to return the error message invalid control number for PMT-46935
	* Author     : Sindhu
	****************************************************************************************************************************************/
	
	PROCEDURE gm_validate_lot(
	    p_usage_lot IN t2550_part_control_number.C2550_CONTROL_NUMBER%TYPE,
	    p_part_num  IN t2550_part_control_number.C205_PART_NUMBER_ID%TYPE,
	    p_lot_error_msg OUT VARCHAR2,
	    p_out_pnum OUT t2550_part_control_number.C205_PART_NUMBER_ID%TYPE,
	    p_out_usage_lot OUT t2550_part_control_number.C2550_CONTROL_NUMBER%TYPE,
	    p_out_expiry_date OUT t2550_part_control_number.C2550_CONTROL_NUMBER%TYPE,
	    p_out_part_desc OUT t205_part_number.C205_PART_NUM_DESC%TYPE )
	AS
	  v_usage_lot t2550_part_control_number.C2550_CONTROL_NUMBER%TYPE := UPPER(TRIM(p_usage_lot));
	  v_lot_count   NUMBER;
	  v_expiry_date VARCHAR (20);
	  v_udi t2550_part_control_number.C2550_UDI_MRF%TYPE;
	  v_count NUMBER := 0;
	  v_cnt   NUMBER := 0;
	  v_control_num t2550_part_control_number.C2550_CONTROL_NUMBER%TYPE;
	  v_part_desc t205_part_number.C205_PART_NUM_DESC%TYPE;
	  v_part_num t2550_part_control_number.C205_PART_NUMBER_ID%TYPE;
	BEGIN
	  p_lot_error_msg := NULL;
	  IF p_part_num   IS NOT NULL AND p_usage_lot IS NOT NULL THEN
	  BEGIN
	    SELECT COUNT (1)
	    INTO v_lot_count
	    FROM t2550_part_control_number
	    WHERE c2550_control_number = v_usage_lot
	    AND c205_part_number_id    = p_part_num ;
	  EXCEPTION
	      WHEN NO_DATA_FOUND THEN
		  v_lot_count := 0;
	  END;
	    IF (v_lot_count            = 0) THEN
	      p_lot_error_msg         := ' Lot  ' || p_usage_lot || ' entered for the part number ' || p_part_num || ' is invalid' ;
	    ELSE
	      BEGIN
	        SELECT TO_CHAR(C2550_EXPIRY_DATE,GET_RULE_VALUE('DATEFMT', 'DATEFORMAT')),
	          get_partnum_desc(c205_part_number_id)
	        INTO v_expiry_date,
	          v_part_desc
	        FROM t2550_part_control_number
	        WHERE c2550_control_number = v_usage_lot
	        AND c205_part_number_id    = p_part_num ;
	      EXCEPTION
	      WHEN NO_DATA_FOUND THEN
	        v_expiry_date := NULL;
	        v_part_desc   := NULL;
	      END;
	      p_out_pnum        := p_part_num;
	      p_out_expiry_date := v_expiry_date;
	      p_out_part_desc   := v_part_desc;
	    END IF;
	  ELSIF (p_part_num IS NULL OR p_part_num = '') AND v_usage_lot IS NOT NULL THEN
	    BEGIN
			SELECT COUNT (1)
			INTO v_lot_count
			FROM t2550_part_control_number
			WHERE c2550_control_number = v_usage_lot
			GROUP BY c205_part_number_id ;
		EXCEPTION
	      WHEN NO_DATA_FOUND THEN
		  v_lot_count := 0;
		END;
	    IF (v_lot_count    > 1) THEN
	      p_lot_error_msg := ' Lot  ' || p_usage_lot || ' found in multiple parts. Please provide part number ^ lot number to proceed.' ;
	    ELSIF v_lot_count  = 1 THEN
	      BEGIN
	        SELECT c205_part_number_id,
	          TO_CHAR(C2550_EXPIRY_DATE,GET_RULE_VALUE('DATEFMT', 'DATEFORMAT')),
	          get_partnum_desc(c205_part_number_id)
	        INTO v_part_num,
	          v_expiry_date,
	          v_part_desc
	        FROM t2550_part_control_number
	        WHERE c2550_control_number = v_usage_lot ;
	      EXCEPTION
	      WHEN NO_DATA_FOUND THEN
	        v_part_num    := NULL;
	        v_expiry_date := NULL;
	        v_part_desc   := NULL;
	      END;
	      p_out_pnum        := v_part_num;
	      p_out_expiry_date := v_expiry_date;
	      p_out_part_desc   := v_part_desc;
	    ELSIF v_lot_count    = 0 THEN
	      -- check if P-udi is found without modifying
	      BEGIN
			  SELECT COUNT(1)
			  INTO v_count
			  FROM t2550_part_control_number
			  WHERE REPLACE(c2550_udi_mrf,'<FNC1>','') = v_usage_lot;
		  EXCEPTION
	      WHEN NO_DATA_FOUND THEN
		  v_count := 0;
		  END;
	      IF v_count                               = 0 THEN -- if p_udi is not found then check if it is Alphatec UDI.
	        WHILE INSTR (v_udi, '10')             <> 0
	        LOOP
	          v_control_num := SUBSTR (v_udi, INSTR (v_udi, '10') + 2);
	          v_udi         := v_control_num;
	          SELECT COUNT(1)
	          INTO v_cnt
	          FROM t2550_part_control_number t2550
	          WHERE t2550.C2550_CONTROL_NUMBER = v_control_num;
	          IF v_cnt                         > 0 THEN
	            EXIT;
	          END IF;
	        END LOOP;
	        BEGIN
	          SELECT c205_part_number_id,
	            TO_CHAR(C2550_EXPIRY_DATE,GET_RULE_VALUE('DATEFMT', 'DATEFORMAT')),
	            get_partnum_desc(c205_part_number_id),
	             c2550_control_number
	          INTO v_part_num,
	            v_expiry_date,
	            v_part_desc,
	            v_usage_lot
	          FROM t2550_part_control_number
	          WHERE c2550_control_number = v_control_num
	          AND ROWNUM                 = 1 ;
	        EXCEPTION
	        WHEN NO_DATA_FOUND THEN
	          v_expiry_date := NULL;
	          v_part_num    := NULL;
	          v_part_desc   := NULL;
	        END;
	        v_usage_lot       := v_control_num;
	        p_out_pnum        := v_part_num;
	        p_out_expiry_date := v_expiry_date;
	        p_out_part_desc   := v_part_desc;
	        IF (v_usage_lot   IS NULL) THEN
	          p_lot_error_msg := ' Lot '|| p_usage_lot ||' is invalid. Please provide valid lot to proceed.' ;
	        END IF;
	      ELSE
	        BEGIN
	          SELECT c205_part_number_id,
	            TO_CHAR(C2550_EXPIRY_DATE,GET_RULE_VALUE('DATEFMT', 'DATEFORMAT')),
	            get_partnum_desc(c205_part_number_id),
	            c2550_control_number
	          INTO v_part_num,
	            v_expiry_date,
	            v_part_desc,
	            v_usage_lot
	          FROM t2550_part_control_number
	          WHERE REPLACE(c2550_udi_mrf,'<FNC1>','') = v_usage_lot; -- <FNC1> will not be available when barcode is scanned, so avoid it.
	        EXCEPTION
	        WHEN NO_DATA_FOUND THEN
	          v_expiry_date := NULL;
	          v_part_num    := NULL;
	          v_part_desc   := NULL;
	        END;
	        p_out_pnum        := v_part_num;
	        p_out_expiry_date := v_expiry_date;
	        p_out_part_desc   := v_part_desc;
	      END IF;
	    END IF;
	  ELSE
	    p_lot_error_msg := ' Lot  ' || p_usage_lot || ' found in multiple parts. Please provide part number ^ lot number to proceed.' ;
	  END IF;
	  p_out_usage_lot := v_usage_lot;
	END gm_validate_lot;

END gm_pkg_op_lot_validation;
/