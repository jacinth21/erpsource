-- @"C:\pmt\bit\SpineIT-ERP\Database\Packages\CustomerServices\gm_pkg_cs_credit_surg_info.bdy"

CREATE OR REPLACE PACKAGE BODY gm_pkg_cs_credit_surg_info
IS
--
 /*********************************************************************************
   * Description : Procedure is used to save surgon info on issue credit and debit
   * Author 	 : Agilan Singaravel
  *******************************************************************************/
  PROCEDURE gm_sav_surg_info(
  	p_order_id		IN		VARCHAR2
  , p_new_order_id	IN		VARCHAR2
  , p_userid	    IN      t501a_order_attribute.c501a_created_by%type
  )
  AS
  		CURSOR surg_info_cur
        IS
			SELECT c901_attribute_type atttype, c501A_attribute_value attvalue
			  FROM T501A_ORDER_ATTRIBUTE  
			 WHERE c501_order_id = p_order_id 
			   AND c901_attribute_type in (select C906_RULE_VALUE from t906_rules where C906_RULE_GRP_ID='SURGERY_DETAILS' and C906_VOID_FL is null)
			   AND c501a_void_fl is null;
  BEGIN
	  
	  FOR surg_info IN surg_info_cur
      LOOP
	  
          gm_pkg_cm_order_attribute.gm_sav_order_attribute(p_new_order_id,surg_info.atttype,surg_info.attvalue,null,p_userid);
          
      END LOOP;	  
  END gm_sav_surg_info; 
END gm_pkg_cs_credit_surg_info;
/