/* Formatted on 2019/10/06 18:43 (Formatter Plus v4.8.0) */
/*******************************************************
* Description : Package for allocate the priority for the transation in device
*******************************************************/
CREATE OR REPLACE PACKAGE BODY gm_pkg_op_txn_priority_allocation
IS
/***************************************************************************************************
 * This procedure used to allocate the priority for the transation.
 * Author : Sindhu 
***************************************************************************************************/
PROCEDURE gm_allocate_order_priority (
     p_order_id IN t501_order.c501_order_id%TYPE
)
AS 
	v_order_id       t501_order.c501_order_id%TYPE;
	v_priority       NUMBER;
	v_delivery_mode  t501_order.c501_delivery_mode%TYPE;
	v_user_id        t501_order.c501_created_by%TYPE;
CURSOR v_order_list
   IS
     SELECT t501.c501_order_id v_order_id
     FROM t501_order t501
     WHERE t501.c501_order_id = p_order_id
     OR   t501.c501_parent_order_id = p_order_id
     AND t501.c501_void_fl IS NULL
     AND t501.c501_delete_fl IS NULL;

BEGIN

FOR cur_order IN v_order_list
  LOOP
  	BEGIN
      SELECT c501_delivery_mode,c501_created_by
         INTO v_delivery_mode, v_user_id
       FROM t501_order 
         WHERE C501_ORDER_ID = cur_order.v_order_id
           AND C501_VOID_FL IS NULL;
	EXCEPTION
	    WHEN NO_DATA_FOUND
		THEN
		 v_delivery_mode := '';
	END;
	
	BEGIN

	 SELECT TO_NUMBER(c906_rule_value)
	    INTO v_priority
	 FROM t906_rules 
	 WHERE c906_rule_id  = to_char(v_delivery_mode)
	    AND c906_rule_grp_id = 'INVTXNPRIORITY'
	    AND c906_void_fl IS NULL;
	 EXCEPTION
	    WHEN NO_DATA_FOUND
		THEN
		 v_priority := '';
	END;   

     UPDATE t501_order
        SET c501_priority = v_priority,
            c501_last_updated_by = v_user_id,
            c501_last_updated_date = CURRENT_DATE
      WHERE C501_ORDER_ID = cur_order.v_order_id;

	 UPDATE t5050_invpick_assign_detail
        SET c5050_priority = v_priority,
            c5050_last_updated_by  = v_user_id,
            c5050_last_updated_date = CURRENT_DATE
     WHERE  c5050_ref_id = cur_order.v_order_id;

  END LOOP;
END;

END gm_pkg_op_txn_priority_allocation;
/