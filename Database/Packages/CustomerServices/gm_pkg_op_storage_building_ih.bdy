/*******************************************************************************************************************************
   * Description : Package body for splitting the inhouse transactions based on the parts available in the building for PMT-33513
   *		
   * Parameters  : Inhouse transaction ID
   *****************************************************************************************************************************/
CREATE OR REPLACE
PACKAGE BODY gm_pkg_op_storage_building_ih
IS

/*************************************************************************************************************************************
	* Description : This procedure is used to validate the Inhouse transaction type and the plant for the input Inhouse id (t504_consignment table) for PMT-33513
	* Author     : Swetha
	* Parameters  : Inhouse transaction ID
****************************************************************************************************************************************/
PROCEDURE gm_validate_inhouset504_transaction(
	 p_inhouse_id IN t504_consignment.c504_consignment_id%TYPE,
	 p_validate_inhouse OUT VARCHAR2
	  )
	 
	AS
     v_plant_rule_val t906_rules.c906_rule_value%TYPE;
     v_inhouse_rule_val t906_rules.c906_rule_value%TYPE;
     v_plant_id t504_consignment.c5040_plant_id%TYPE;
     v_inhouse_type t504_consignment.c504_type%TYPE;
	 
	 
  BEGIN
 
  IF p_inhouse_id = '' OR p_inhouse_id IS NULL 
     THEN
      RETURN;
   END IF;
 
--get the plant rule value and inhouse type for the input inhouse txn id   
 BEGIN  
     SELECT  t504.c504_type, t906.c906_rule_value  
       INTO v_inhouse_type, v_plant_rule_val 
      FROM t504_consignment t504, t906_rules t906 
       WHERE t504.c504_consignment_id = p_inhouse_id 
       AND t504.c504_void_fl IS NULL
	   AND t906.c906_rule_id = t504.c5040_plant_id  -- Rule id will be plant id 
       AND t906.c1900_company_id IS NULL 
       AND t906.c906_void_fl IS NULL
       AND t906.c906_rule_grp_id = 'BUILDINGPLANTMAP';
   EXCEPTION
            WHEN NO_DATA_FOUND THEN
                v_plant_rule_val := NULL;
                v_inhouse_type := NULL;
             END;  
       
   -- validating the inhouse type       
   IF v_plant_rule_val = 'Y' THEN
      SELECT get_rule_value (v_inhouse_type, 'BUILDMAPIH504TYPE')
           INTO v_inhouse_rule_val
          FROM dual;
      
    ELSE
      RETURN;
      
  END IF;
       
   
   IF v_inhouse_rule_val = 'Y' THEN
      p_validate_inhouse := 'YES';
   ELSE
        p_validate_inhouse := NULL;
   END IF;
   
END gm_validate_inhouset504_transaction;


/*************************************************************************************************************************************
	* Description : This procedure is used to validate the Inhouse transaction type and the plant for the input Inhouse id (t412_inhouse_transactions table) for PMT-33513
	* Author     : Swetha
	* Parameters  : Inhouse transaction ID
****************************************************************************************************************************************/
PROCEDURE gm_validate_inhouset412_transaction(
	 p_inhouse_id IN t412_inhouse_transactions.c412_inhouse_trans_id%TYPE,
	 p_validate_inhouse OUT VARCHAR2
	  )
	 
	AS
     v_plant_rule_val t906_rules.c906_rule_value%TYPE;
     v_inhouse_rule_val t906_rules.c906_rule_value%TYPE;
     v_plant_id t412_inhouse_transactions.c5040_plant_id%TYPE;
     v_inhouse_type t412_inhouse_transactions.c412_type%TYPE;
	 
	 
  BEGIN
 
  IF p_inhouse_id = '' OR p_inhouse_id IS NULL 
     THEN
      RETURN;
   END IF;
  
 --get the plant rule value and inhouse type for the input inhouse txn id   
 BEGIN  
     SELECT  t412.c412_type, t906.c906_rule_value  
       INTO v_inhouse_type, v_plant_rule_val 
      FROM t412_inhouse_transactions t412, t906_rules t906 
       WHERE t412.c412_inhouse_trans_id = p_inhouse_id 
       AND t412.c412_void_fl IS NULL
	   AND t906.c906_rule_id = t412.c5040_plant_id  -- Rule id will be plant id 
       AND t906.c1900_company_id IS NULL 
       AND t906.c906_void_fl IS NULL
       AND t906.c906_rule_grp_id = 'BUILDINGPLANTMAP';
   EXCEPTION
            WHEN NO_DATA_FOUND THEN
                v_plant_rule_val := NULL;
                v_inhouse_type := NULL;
             END;
          
  -- validating the inhouse type       
   IF v_plant_rule_val = 'Y' THEN
      SELECT get_rule_value (v_inhouse_type, 'BUILDMAPIH412TYPE')
           INTO v_inhouse_rule_val
          FROM dual;
    ELSE
      RETURN;
      
  END IF;
       
   
   IF v_inhouse_rule_val = 'Y' THEN
      p_validate_inhouse := 'YES';
   ELSE
        p_validate_inhouse := NULL;
   END IF;
   
END gm_validate_inhouset412_transaction;


/***************************************************************************************************************************************
   * Purpose: This Procedure is used to fetch the building count for the parts in an Inhouse transaction id (t504_consignment table) for PMT-33513
   * Author     : Swetha
   * Parameters  : Inhouse transaction ID
 *************************************************************************************************************************************/

	PROCEDURE gm_fetch_inhouset504_building_ids(
	 p_inhouse_id IN t504_consignment.c504_consignment_id%TYPE,
	 p_building_count OUT NUMBER
	 )
	 
	 
	  AS
	   v_plant_id t504_consignment.c5040_plant_id%TYPE;
	   v_building_count	   NUMBER;
	
	BEGIN
	
	SELECT get_plantid_frm_cntx()
        INTO v_plant_id
        FROM DUAL;
        	
	BEGIN
	   SELECT COUNT( DISTINCT (t5052.c5057_building_id))
	     INTO v_building_count
      FROM t5053_location_part_mapping t5053, t5052_location_master t5052, t504_consignment t504, t505_item_consignment t505
     WHERE t5052.c5052_location_id= t5053.c5052_location_id
     AND t5052.c5040_plant_id   = v_plant_id
     AND t5052.c5052_void_fl IS NULL
     AND t5053.c5053_curr_qty  > 0
     AND t5052.c901_location_type =93320  -- Pick Face
     AND t5052.c5051_inv_warehouse_id = 1 -- FG Warehouse
     AND t5053.c205_part_number_id = t505.c205_part_number_id
     AND t504.c504_consignment_id = t505.c504_consignment_id
     AND t505.c504_consignment_id = p_inhouse_id
     AND t504.c504_void_fl IS NULL
     AND t5052.c5052_void_fl IS NULL
     AND t5052.c901_status = 93310 --Active
     AND t505.c505_void_fl   IS NULL ;
   EXCEPTION
		WHEN NO_DATA_FOUND THEN
			v_building_count := 0;
	END;
	
	
	p_building_count := v_building_count;
	
END gm_fetch_inhouset504_building_ids;


/***************************************************************************************************************************************
   * Purpose: This Procedure is used to fetch the building count for the parts in an Inhouse transaction id (t412_inhouse_transactions table) for PMT-33513
   * Author     : Swetha
   * Parameters  : Inhouse transaction ID
 *************************************************************************************************************************************/

	PROCEDURE gm_fetch_inhouset412_building_ids(
	 p_inhouse_id IN t412_inhouse_transactions.c412_inhouse_trans_id%TYPE,
	 p_building_count OUT NUMBER
	 )
	 
	 
	  AS
	   v_plant_id t412_inhouse_transactions.c5040_plant_id%TYPE;
	   v_building_count	   NUMBER;
	
	BEGIN
	
	SELECT get_plantid_frm_cntx()
        INTO v_plant_id
        FROM DUAL;
        	
	BEGIN
	   SELECT COUNT( DISTINCT (t5052.c5057_building_id))
	     INTO v_building_count
      FROM t5053_location_part_mapping t5053, t5052_location_master t5052, t412_inhouse_transactions t412, t413_inhouse_trans_items t413
     WHERE t5052.c5052_location_id= t5053.c5052_location_id
     AND t5052.c5040_plant_id   = v_plant_id
     AND t5052.c5052_void_fl IS NULL
     AND t5053.c5053_curr_qty  > 0
     AND t5052.c901_location_type =93320  -- Pick Face
     AND t5052.c5051_inv_warehouse_id = 1 -- FG Warehouse
     AND t5053.c205_part_number_id = t413.c205_part_number_id 
     AND t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
     AND t413.c412_inhouse_trans_id = p_inhouse_id
     AND t412.c412_void_fl IS NULL
     AND t5052.c5052_void_fl IS NULL
     AND t5052.c901_status = 93310 --Active
     AND t413.c413_void_fl IS NULL ;
   EXCEPTION
		WHEN NO_DATA_FOUND THEN
			v_building_count := 0;
	END;
	
	
	p_building_count := v_building_count;
	
END gm_fetch_inhouset412_building_ids;


/***************************************************************************************************************************************
   * Purpose: This procedure is used to update the building id in t504_consignment table for the input Inhouse transaction id for PMT-33513
   * Author     : Swetha
   * Parameters  : Inhouse txn ID, User ID, Building ID
 *************************************************************************************************************************************/
	PROCEDURE gm_update_inhouset504_building_id(
	 p_inhouse_id IN t504_consignment.c504_consignment_id%TYPE,
	 p_user_id IN t504_consignment.c504_created_by%TYPE,
	 p_building_id IN t5052_location_master.c5052_location_id%TYPE DEFAULT NULL
	)
	  
	  AS
	  
	  v_building_id t5052_location_master.c5057_building_id%TYPE;
	  v_plant_id 	  t5040_plant_master.c5040_plant_id%TYPE;
	  v_default_building_id t5052_location_master.c5057_building_id%TYPE;
	  
	   
	  BEGIN   
	   
	  
	  SELECT get_plantid_frm_cntx()
        INTO  v_plant_id
        FROM DUAL;
     
   IF p_building_id IS NOT NULL THEN
   
     --update building id for t504_consignment table
    UPDATE t504_consignment SET c5057_building_id = p_building_id 
          , c504_last_updated_by = p_user_id   
          , c504_last_updated_date = current_date 
       WHERE c504_consignment_id = p_inhouse_id 
      AND c504_void_fl IS NULL;
      
      --updating building id for inventory device pick  
        UPDATE t5050_invpick_assign_detail 
       SET c5057_building_id = p_building_id,
           c5050_last_updated_by = p_user_id,
           c5050_last_updated_date = current_date 
       WHERE c5050_ref_id = p_inhouse_id
       AND c5050_void_fl IS NULL; 
       
        --updating building id in t907_shipping_info table for FGRP txn for PMT-36676 BUG-11480
       UPDATE t907_shipping_info
        SET c5057_building_id = p_building_id 
          , c907_last_updated_by = p_user_id
          , c907_last_updated_date = current_date
       WHERE c907_ref_id = p_inhouse_id
       AND c907_void_fl IS NULL; 
      
      RETURN; 
  END IF;			
	
 -- This block will execute	only if p_building_id is null	
 --fetch the default building id 
   BEGIN
      SELECT c5057_building_id 
     INTO v_default_building_id 
       FROM t5057_building 
      WHERE c5040_plant_id = v_plant_id  
       AND c5057_primary_fl='Y' 
       AND c5057_active_fl='Y' 
       AND c5057_void_fl IS NULL;
     EXCEPTION
       WHEN NO_DATA_FOUND THEN -- when default building id is not available
        v_default_building_id := NULL;
    END; 
    
   --Fetch building id for the Inhouse id
  BEGIN  
       SELECT t5052.c5057_building_id
	     INTO v_building_id
      FROM t5053_location_part_mapping t5053, t5052_location_master t5052, t504_consignment t504, t505_item_consignment t505
     WHERE t5052.c5052_location_id= t5053.c5052_location_id
     AND t5052.c5040_plant_id   = v_plant_id
     AND t5052.c5052_void_fl IS NULL
     AND t5053.c5053_curr_qty  > 0
     AND t5052.c901_location_type = 93320  -- Pick Face
     AND t5052.c5051_inv_warehouse_id = 1 -- FG Warehouse
     AND t5053.c205_part_number_id = t505.c205_part_number_id
     AND t504.c504_consignment_id = t505.c504_consignment_id
     AND t505.c504_consignment_id = p_inhouse_id
     AND t504.c504_void_fl IS NULL
     AND t5052.c5052_void_fl IS NULL
     AND t505.c505_void_fl   IS NULL
     AND t5052.c901_status = 93310 --Active
     GROUP BY t5052.c5057_building_id ;
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
        v_building_id := NULL;
   END;
                
   --updating building id for t504_consignment table
    UPDATE t504_consignment SET c5057_building_id = NVL(v_building_id, v_default_building_id) --update default building id if building id for the part is not available 
          , c504_last_updated_by = p_user_id   
          , c504_last_updated_date = current_date 
       WHERE c504_consignment_id = p_inhouse_id 
      AND c504_void_fl IS NULL;
       
       --updating building id for inventory device pick  
      UPDATE t5050_invpick_assign_detail 
       SET c5057_building_id = NVL(v_building_id, v_default_building_id) --update default building id if building id for the part is not available
         , c5050_last_updated_by = p_user_id
         , c5050_last_updated_date = current_date 
       WHERE c5050_ref_id = p_inhouse_id
       AND c5050_void_fl IS NULL ; 	
       
         --updating building id in t907_shipping_info table for FGRP txn for PMT-36676 BUG-11480
       UPDATE t907_shipping_info
        SET c5057_building_id = NVL(v_building_id, v_default_building_id) --update default building id if building id for the part is not available
          , c907_last_updated_by = p_user_id
          , c907_last_updated_date = current_date
       WHERE c907_ref_id = p_inhouse_id
       AND c907_void_fl IS NULL;		
				

END gm_update_inhouset504_building_id;


/***************************************************************************************************************************************
   * Purpose: This procedure is used to update the building id in t412_inhouse_transactions table for the input Inhouse transaction id for PMT-33513
   * Author     : Swetha
   * Parameters  : Inhouse txn ID, User ID, Building ID
 *************************************************************************************************************************************/
	PROCEDURE gm_update_inhouset412_building_id(
	 p_inhouse_id IN t412_inhouse_transactions.c412_inhouse_trans_id%TYPE,
	 p_user_id IN t412_inhouse_transactions.c412_created_by%TYPE,
	 p_building_id IN t5052_location_master.c5052_location_id%TYPE DEFAULT NULL
	)
	  
	  AS
	  
	  v_building_id t5052_location_master.c5057_building_id%TYPE;
	  v_plant_id 	  t5040_plant_master.c5040_plant_id%TYPE;
	  v_default_building_id t5052_location_master.c5057_building_id%TYPE;
	  
	   
	  BEGIN   
	   
	  
	  SELECT get_plantid_frm_cntx()
        INTO  v_plant_id
        FROM DUAL;
     
  
    IF p_building_id IS NOT NULL THEN
   
     --update building id for t412_inhouse_transactions table
    UPDATE t412_inhouse_transactions SET c5057_building_id = p_building_id 
          , c412_last_updated_by = p_user_id   
          , c412_last_updated_date = current_date 
       WHERE c412_inhouse_trans_id = p_inhouse_id 
      AND c412_void_fl IS NULL;
      
      --updating building id for inventory device pick  
        UPDATE t5050_invpick_assign_detail 
       SET c5057_building_id = p_building_id,
           c5050_last_updated_by = p_user_id,
           c5050_last_updated_date = current_date 
       WHERE c5050_ref_id = p_inhouse_id
       AND c5050_void_fl IS NULL; 
       
       --updating building id in t907_shipping_info table for FGLE txn
       UPDATE t907_shipping_info
        SET c5057_building_id = p_building_id 
          , c907_last_updated_by = p_user_id
          , c907_last_updated_date = current_date
       WHERE c907_ref_id = p_inhouse_id
       AND c907_void_fl IS NULL; 
       
       RETURN;
    END IF;	
 
 -- This block will execute	only if p_building_id is null	
 --fetch the default building id 
   BEGIN
      SELECT c5057_building_id 
     INTO v_default_building_id 
       FROM t5057_building 
      WHERE c5040_plant_id = v_plant_id  
       AND c5057_primary_fl='Y' 
       AND c5057_active_fl='Y' 
       AND c5057_void_fl IS NULL;
     EXCEPTION
       WHEN NO_DATA_FOUND THEN -- when default building id is not available
        v_default_building_id := NULL;
    END; 
    
   --Fetch building id for the Inhouse id
  BEGIN  
         SELECT t5052.c5057_building_id
	   INTO v_building_id
      FROM t5053_location_part_mapping t5053, t5052_location_master t5052, t412_inhouse_transactions t412, t413_inhouse_trans_items t413
     WHERE t5052.c5052_location_id= t5053.c5052_location_id
     AND t5052.c5040_plant_id   = v_plant_id
     AND t5052.c5052_void_fl IS NULL
     AND DECODE(t412.c412_type,93341,1,t5053.c5053_curr_qty)  > 0   --93341 FGRP
     AND t5052.c901_location_type = 93320  -- Pick Face
     AND t5052.c5051_inv_warehouse_id = 1 -- FG Warehouse
     AND t5053.c205_part_number_id = t413.c205_part_number_id 
     AND t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
     AND t413.c412_inhouse_trans_id = p_inhouse_id
     AND t412.c412_void_fl IS NULL
     AND t5052.c5052_void_fl IS NULL
     AND t5052.c901_status = 93310 --Active
     AND t413.c413_void_fl IS NULL
     GROUP BY t5052.c5057_building_id ;
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
        v_building_id := NULL;
   END;
                
      --updating building id for t412_inhouse_transactions table
      UPDATE t412_inhouse_transactions SET c5057_building_id = NVL(v_building_id, v_default_building_id) --update default building id if building id for the part is not available 
          , c412_last_updated_by = p_user_id   
          , c412_last_updated_date = current_date  
       WHERE c412_inhouse_trans_id = p_inhouse_id 
      AND c412_void_fl IS NULL;
       
       --updating building id for inventory device pick   
      UPDATE t5050_invpick_assign_detail 
       SET c5057_building_id = NVL(v_building_id, v_default_building_id) --update default building id if building id for the part is not available
         , c5050_last_updated_by = p_user_id
         , c5050_last_updated_date = current_date 
       WHERE c5050_ref_id = p_inhouse_id
       AND c5050_void_fl IS NULL ; 
       
       
       --updating building id in t907_shipping_info table for FGLE txn
       UPDATE t907_shipping_info
        SET c5057_building_id = NVL(v_building_id, v_default_building_id) --update default building id if building id for the part is not available
          , c907_last_updated_by = p_user_id
          , c907_last_updated_date = current_date
       WHERE c907_ref_id = p_inhouse_id
       AND c907_void_fl IS NULL;		
				

END gm_update_inhouset412_building_id;



/***************************************************************************************************************************************
   * Purpose: This procedure is used to process the input Inhouse transaction id (t504_consignment table) by getting the building count for that transaction id and split the transaction based on the building count for PMT-33513
   * Author     : Swetha
   * Parameters  : Inhouse txn ID
 *************************************************************************************************************************************/

	PROCEDURE gm_inhouset504_process_transaction(
	 p_inhouse_id IN t504_consignment.c504_consignment_id%TYPE
	)
	  AS

    v_building_cnt    NUMBER;
    v_inhouse_id t504_consignment.c504_consignment_id%TYPE;
    v_user_id t504_consignment.c504_created_by%TYPE;
    
    BEGIN
    
   --fetch the Inhouse txn id and userid 
  BEGIN
     SELECT t504.c504_consignment_id, t504.c504_created_by
          INTO v_inhouse_id, v_user_id    
				 FROM t504_consignment t504 
				WHERE t504.c504_consignment_id = p_inhouse_id 
				AND t504.c504_void_fl IS NULL
				AND t504.c504_type
				   IN (SELECT c906_rule_id
                          FROM t906_rules
	                   WHERE c906_rule_grp_id = 'BUILDMAPIH504TYPE'
	                   AND c1900_company_id IS NULL 
	                   AND c906_void_fl IS NULL)
			              FOR UPDATE;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
      v_inhouse_id := NULL;
      v_user_id := NULL;
    END;
   
     --procedure call to fetch the building count for the input inhouse id
      gm_fetch_inhouset504_building_ids(v_inhouse_id, v_building_cnt);
      
       IF (v_building_cnt <= 1) THEN
       
       --update the building id for the passed Inhouse id
          gm_update_inhouset504_building_id(p_inhouse_id, v_user_id);
               
       ELSE
       
       --Procedure call to split the Inhouse txn based on the building count
        gm_inhouset504_split_transaction(p_inhouse_id, v_user_id);
        
      END IF;

   END gm_inhouset504_process_transaction;
   
   
 /***************************************************************************************************************************************
   * Purpose: This procedure is used to process the input Inhouse transaction id ( t412_inhouse_transactions table) by getting the building count for that transaction id and split the transaction based on the building count for PMT-33513
   * Author     : Swetha
   * Parameters  : Inhouse txn ID
 *************************************************************************************************************************************/

	PROCEDURE gm_inhouset412_process_transaction(
	 p_inhouse_id IN t412_inhouse_transactions.c412_inhouse_trans_id%TYPE
	)
	  AS

    v_building_cnt    NUMBER;
    v_inhouse_id t412_inhouse_transactions.c412_inhouse_trans_id%TYPE;
    v_user_id t412_inhouse_transactions.c412_created_by%TYPE;
    
    BEGIN
    
    
     --fetch the Inhouse txn id and userid 
  BEGIN
     SELECT t412.c412_inhouse_trans_id, t412.c412_created_by 
          INTO v_inhouse_id, v_user_id    
				 FROM t412_inhouse_transactions t412 
				WHERE t412.c412_inhouse_trans_id = p_inhouse_id 
				AND t412.c412_void_fl IS NULL
				AND t412.c412_type
				   IN (SELECT c906_rule_id
                          FROM t906_rules
	                   WHERE c906_rule_grp_id = 'BUILDMAPIH412TYPE'
	                   AND c1900_company_id IS NULL 
	                   AND c906_void_fl IS NULL)
			               FOR UPDATE;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
      v_inhouse_id := NULL;
      v_user_id := NULL;
    END;
    
     --procedure call to fetch the building count for the input inhouse id
     gm_fetch_inhouset412_building_ids(v_inhouse_id, v_building_cnt);
      
       IF (v_building_cnt <= 1) THEN
       
     --update the building id for the passed Inhouse id
     gm_update_inhouset412_building_id(p_inhouse_id, v_user_id);
               
      ELSE
       
      --Procedure call to split the Inhouse txn based on the building count
        gm_inhouset412_split_transaction(p_inhouse_id, v_user_id);
                   
       END IF;

   END gm_inhouset412_process_transaction;


/***************************************************************************************************************************************
   * Purpose: This procedure is used to fetch the building id's and split the Inhouse transaction (t504_consignment table) by creating transactions and update device entry and inserts for newly generated transaction id  for PMT-33513
   * Author     : Swetha
   * Parameters  : Inhouse txn ID, user ID
 *************************************************************************************************************************************/

	PROCEDURE gm_inhouset504_split_transaction(
	 p_inhouse_id IN t504_consignment.c504_consignment_id%TYPE,
	 p_user_id IN t504_consignment.c504_created_by%TYPE
	)
	  AS
	  
	  v_loop_cnt    NUMBER;
      v_building_id t5052_location_master.c5057_building_id%TYPE;
      v_inhouse_id t504_consignment.c504_consignment_id%TYPE;
      v_user_id t504_consignment.c504_created_by%TYPE;
      
      v_parent_inhouse_id t504_consignment.c504_consignment_id%TYPE;
      
      v_consignment_id            t504_consignment.c504_consignment_id%TYPE; 
      v_distributor_id            t504_consignment.c701_distributor_id%TYPE; 
      v_ship_date                 t504_consignment.c504_ship_date%TYPE; 
      v_return_date               t504_consignment.c504_return_date%TYPE; 
      v_comments                  t504_consignment.c504_comments%TYPE; 
      v_total_cost                t504_consignment.c504_total_cost%TYPE; 
      v_status_fl                 t504_consignment.c504_status_fl%TYPE; 
      v_void_fl                   t504_consignment.c504_void_fl%TYPE;
      v_account_id                t504_consignment.c704_account_id%TYPE;
      v_ship_to                   t504_consignment.c504_ship_to%TYPE; 
      v_set_id                    t504_consignment.c207_set_id%TYPE; 
      v_ship_to_id                t504_consignment.c504_ship_to_id%TYPE; 
      v_final_comments            t504_consignment.c504_final_comments%TYPE; 
      v_inhouse_purpose           t504_consignment.c504_inhouse_purpose%TYPE; 
      v_type                      t504_consignment.c504_type%TYPE;
      v_delivery_carrier          t504_consignment.c504_delivery_carrier%TYPE; 
      v_delivery_mode             t504_consignment.c504_delivery_mode%TYPE;
      v_tracking_number           t504_consignment.c504_tracking_number%TYPE; 
      v_ship_req_fl               t504_consignment.c504_ship_req_fl%TYPE; 
      v_verify_fl                 t504_consignment.c504_verify_fl%TYPE;
      v_verified_date             t504_consignment.c504_verified_date%TYPE; 
      v_verified_by               t504_consignment.c504_verified_by%TYPE; 
      v_update_inv_fl             t504_consignment.c504_update_inv_fl%TYPE; 
      v_reprocess_id              t504_consignment.c504_reprocess_id%TYPE;
      v_master_consignment_id     t504_consignment.c504_master_consignment_id%TYPE;
      v_request_id                t504_consignment.c520_request_id%TYPE;
      v_created_by                t504_consignment.c504_created_by%TYPE; 
      v_ref_id                    t504_consignment.c504_ref_id%TYPE; 
      v_ref_type                  t504_consignment.c901_ref_type%TYPE; 
      
      v_out_inhouse_id t504_consignment.c504_consignment_id%TYPE;
      v_inv_type t901_code_lookup.c901_code_id%TYPE;
      v_allocate_fl     VARCHAR2 (1);
      
      --for PMT-36676 FGRT txn BUG-11480
      v_txn_ref_id            t907_shipping_info.c907_ref_id%TYPE;
      v_txn_ship_to           t907_shipping_info.c901_ship_to%TYPE;   
      v_txn_ship_to_id        t907_shipping_info.c907_ship_to_id%TYPE; 
      v_txn_delivery_carrier  t907_shipping_info.c901_delivery_carrier%TYPE;   
      v_txn_delivery_mode     t907_shipping_info.c901_delivery_mode%TYPE;   
      v_txn_last_updated_by   t907_shipping_info.c907_last_updated_by%TYPE;    
      v_txn_shipping_id       t907_shipping_info.c907_shipping_id%TYPE;  
      v_txn_address           t907_shipping_info.c106_address_id%TYPE;  
      
      
      
       CURSOR v_building_list
   IS
   SELECT t504.c504_consignment_id pinhouseid, t504.c504_created_by puserid, t5052.c5057_building_id pbuildingid
	 FROM t5053_location_part_mapping t5053, t5052_location_master t5052, t504_consignment t504, t505_item_consignment t505
     WHERE t5052.c5052_location_id= t5053.c5052_location_id
     AND t5052.c5040_plant_id   = t504.c5040_plant_id
     AND t5052.c5052_void_fl IS NULL
     AND t5053.c5053_curr_qty  > 0
     AND t5052.c901_location_type =93320  -- Pick Face
     AND t5052.c5051_inv_warehouse_id = 1 -- FG Warehouse
     AND t5053.c205_part_number_id = t505.c205_part_number_id
     AND t504.c504_consignment_id = t505.c504_consignment_id
     AND t505.c504_consignment_id = p_inhouse_id
     AND t504.c504_void_fl IS NULL
     AND t5052.c5052_void_fl IS NULL
     AND t5052.c901_status = 93310 --Active
     AND t505.c505_void_fl   IS NULL
     GROUP BY t504.c504_consignment_id, t504.c504_created_by, t5052.c5057_building_id;

  BEGIN
    v_loop_cnt := 0;
    v_parent_inhouse_id := p_inhouse_id;
    
    FOR var_buildloop IN v_building_list
		LOOP
		   v_loop_cnt := v_loop_cnt + 1;
		   v_inhouse_id := var_buildloop.pinhouseid;
		   v_user_id := var_buildloop.puserid;
           v_building_id := var_buildloop.pbuildingid;
           
		   
		 IF (v_loop_cnt = 1) THEN
			
		 --procedure call to update the  building id for the inhouse txn id
		gm_update_inhouset504_building_id(v_inhouse_id, v_user_id, v_building_id);
		
		
		ELSIF (v_loop_cnt != 1) THEN  
	     
		--procedure call to split the inhouse id 
		
		-- Fetch parent inhouse transaction details and store in variables
		BEGIN
		  SELECT c504_consignment_id, c701_distributor_id, c504_ship_date, c504_return_date, c504_comments, 
		         c504_total_cost, c504_status_fl, c504_void_fl, c704_account_id, c504_ship_to, c207_set_id,
		         c504_ship_to_id, c504_final_comments, c504_inhouse_purpose, c504_type, c504_delivery_carrier,
		         c504_delivery_mode, c504_tracking_number, c504_ship_req_fl, c504_verify_fl, c504_verified_date,
		         c504_verified_by, c504_update_inv_fl, c504_reprocess_id, c504_master_consignment_id, c520_request_id,
		         c504_created_by, c504_ref_id, c901_ref_type
		 INTO    v_consignment_id, v_distributor_id, v_ship_date, v_return_date, v_comments, 
		         v_total_cost, v_status_fl, v_void_fl, v_account_id, v_ship_to, v_set_id, 
		         v_ship_to_id, v_final_comments, v_inhouse_purpose, v_type, v_delivery_carrier, 
		         v_delivery_mode, v_tracking_number, v_ship_req_fl, v_verify_fl, v_verified_date,
		         v_verified_by, v_update_inv_fl, v_reprocess_id, v_master_consignment_id, v_request_id,
		         v_created_by, v_ref_id, v_ref_type  
		FROM t504_consignment
       WHERE c504_consignment_id = v_parent_inhouse_id
         AND c504_void_fl IS NULL;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                v_consignment_id := NULL;
             END;  
           
          -- procedure call to create new inhouse transaction id in t504_consignment table  
		  gm_pkg_op_split.gm_save_item_consign(v_consignment_id, v_distributor_id, v_ship_date, v_return_date, v_comments, 
		         v_total_cost, v_status_fl, v_void_fl, v_account_id, v_ship_to, v_set_id, 
		         v_ship_to_id, v_final_comments, v_inhouse_purpose, v_type, v_delivery_carrier, 
		         v_delivery_mode, v_tracking_number, v_ship_req_fl, v_verify_fl, v_verified_date,
		         v_verified_by, v_update_inv_fl, v_reprocess_id, v_master_consignment_id, v_request_id,
		         v_created_by, v_ref_id, v_ref_type, v_out_inhouse_id); 
		
	    -- Get the transaction type for device. If the transaction is from "shelf" it has to be picked from device.
		 BEGIN         
		   SELECT c901_code_id
		  INTO v_inv_type
		  FROM t901_code_lookup t901
		 WHERE t901.c901_code_grp IN ('INVPT', 'INVPW')
		   AND t901.c902_code_nm_alt = TO_CHAR (v_type)
		   AND t901.c901_active_fl = '1';
		EXCEPTION
         WHEN NO_DATA_FOUND THEN
           v_inv_type := NULL;
     END;
        
       -- Create Record in T5050 table for Device to process
       IF v_inv_type IS NOT NULL THEN
		 v_allocate_fl := gm_pkg_op_inventory.chk_txn_details (v_out_inhouse_id, v_inv_type);

		IF NVL (v_allocate_fl, 'N') = 'Y'
		THEN
			gm_pkg_allocation.gm_ins_invpick_assign_detail (TO_NUMBER (v_inv_type), v_out_inhouse_id, v_user_id);
		END IF;
	  END IF;  
		
    -- procedure call to update t505_item_consignment table
     gm_update_items_t504inhouse_id(p_inhouse_id, v_out_inhouse_id, v_building_id);
     
     --procedure call to ShipOut for FGRT transaction for PMT-36676 BUG-11480
      --get the parent FGRT details
    IF (v_type = 400086) THEN --FGRT
      BEGIN
      SELECT c907_ref_id, c901_ship_to, c907_ship_to_id, c901_delivery_carrier, c901_delivery_mode, c907_last_updated_by, c106_address_id 
        INTO  v_txn_ref_id, v_txn_ship_to, v_txn_ship_to_id, v_txn_delivery_carrier, v_txn_delivery_mode, v_txn_last_updated_by, v_txn_address 
      FROM t907_shipping_info
      WHERE c907_ref_id = p_inhouse_id
      AND   c907_void_fl IS NULL;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
           v_txn_ref_id := NULL;
           v_txn_ship_to := NULL;
           v_txn_ship_to_id := NULL;
           v_txn_delivery_carrier := NULL;
           v_txn_delivery_mode := NULL;
           v_txn_last_updated_by := NULL;
           v_txn_address := NULL;
     END;
   
     
      v_txn_shipping_id := NULL;
      
      --procedure call to update shipping info for the child FGRT txn  source - 4000518
     gm_pkg_cm_shipping_trans.gm_sav_shipping(v_out_inhouse_id, 4000518, v_txn_ship_to, v_txn_ship_to_id, v_txn_delivery_carrier, v_txn_delivery_mode, NULL, v_txn_address, NULL, v_txn_last_updated_by, v_txn_shipping_id);
     
    END IF;  
		         
    -- procedure call to update the building id for new inhouse id       
     gm_update_inhouset504_building_id(v_out_inhouse_id, v_user_id, v_building_id); 

    END IF;
        END LOOP;
  END gm_inhouset504_split_transaction;


/***************************************************************************************************************************************
   * Purpose: This procedure is used to fetch the building id's and split the Inhouse transaction ( t412_inhouse_transactions table) by creating transactions and update device entry and inserts for newly generated transaction id  for PMT-33513
   * Author     : Swetha
   * Parameters  : Inhouse txn ID, user ID
 *************************************************************************************************************************************/

	PROCEDURE gm_inhouset412_split_transaction(
	 p_inhouse_id IN t412_inhouse_transactions.c412_inhouse_trans_id%TYPE,
	 p_user_id IN t412_inhouse_transactions.c412_created_by%TYPE
	)
	
	AS
	
	  v_loop_cnt           NUMBER;
      v_building_id        t5052_location_master.c5057_building_id%TYPE;
      v_inhouse_id         t412_inhouse_transactions.c412_inhouse_trans_id%TYPE;
      v_user_id            t412_inhouse_transactions.c412_created_by%TYPE;
      
      v_parent_inhouse_id  t412_inhouse_transactions.c412_inhouse_trans_id%TYPE;
      
      
      v_comments           t412_inhouse_transactions.c412_comments%TYPE;
      v_status_fl		   t412_inhouse_transactions.c412_status_fl%TYPE;
      v_purpose		       t412_inhouse_transactions.c412_inhouse_purpose%TYPE;
      v_type		       t412_inhouse_transactions.c412_type%TYPE;
      v_refid		       t412_inhouse_transactions.c412_ref_id%TYPE;
      v_reftype            t412_inhouse_transactions.c901_ref_type%TYPE;
      v_shipto		       t412_inhouse_transactions.c412_release_to%TYPE;
      v_shiptoid	       t412_inhouse_transactions.c412_release_to_id%TYPE;
      v_userid		       t412_inhouse_transactions.c412_created_by%TYPE;
      v_out_inhouse_id	   t412_inhouse_transactions.c412_inhouse_trans_id%TYPE;
      v_loaner_txn_id      t412_inhouse_transactions.c504a_loaner_transaction_id%TYPE;
      
      v_inv_type           t901_code_lookup.c901_code_id%TYPE;
      v_allocate_fl        VARCHAR2 (1);
      
      --for PMT-33512 FGLE txn
      v_fgle_ref_id            t907_shipping_info.c907_ref_id%TYPE;
      v_fgle_ship_to           t907_shipping_info.c901_ship_to%TYPE;   
      v_fgle_ship_to_id        t907_shipping_info.c907_ship_to_id%TYPE; 
      v_fgle_delivery_carrier  t907_shipping_info.c901_delivery_carrier%TYPE;   
      v_fgle_delivery_mode     t907_shipping_info.c901_delivery_mode%TYPE;   
      v_fgle_last_updated_by   t907_shipping_info.c907_last_updated_by%TYPE;    
      v_fgle_shipping_id       t907_shipping_info.c907_shipping_id%TYPE;  
      v_fgle_address           t907_shipping_info.c106_address_id%TYPE;  
      --for PMT-33512 FGLE txn
      
   CURSOR v_building_list
   IS
   SELECT t412.c412_inhouse_trans_id pinhouseid, t412.c412_created_by puserid, t5052.c5057_building_id pbuildingid
	 FROM t5053_location_part_mapping t5053, t5052_location_master t5052, t412_inhouse_transactions t412, t413_inhouse_trans_items t413
     WHERE t5052.c5052_location_id = t5053.c5052_location_id
     AND t5052.c5040_plant_id   = t412.c5040_plant_id
     AND t5052.c5052_void_fl IS NULL
     AND t5053.c5053_curr_qty  > 0
     AND t5052.c901_location_type =93320  -- Pick Face
     AND t5052.c5051_inv_warehouse_id = 1 -- FG Warehouse
     AND t5053.c205_part_number_id = t413.c205_part_number_id
     AND t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
     AND t413.c412_inhouse_trans_id = p_inhouse_id
     AND t412.c412_void_fl  IS NULL
     AND t5052.c5052_void_fl IS NULL
     AND t5052.c901_status = 93310 --Active
     AND t413.c413_void_fl     IS NULL
     GROUP BY t412.c412_inhouse_trans_id, t412.c412_created_by, t5052.c5057_building_id;
	
	BEGIN
	  
	   v_loop_cnt := 0;
	   v_parent_inhouse_id := p_inhouse_id;
	
	
	 FOR var_buildloop IN v_building_list
		LOOP
		   v_loop_cnt := v_loop_cnt + 1;
		   v_inhouse_id := var_buildloop.pinhouseid;
		   v_user_id := var_buildloop.puserid;
           v_building_id := var_buildloop.pbuildingid;
           
		   
		 IF (v_loop_cnt = 1) THEN
			
		 --procedure call to update the  building id for the inhouse txn id
		 gm_update_inhouset412_building_id(v_inhouse_id, v_user_id, v_building_id);
		
		
		ELSIF (v_loop_cnt != 1) THEN 
		
	    --procedure call to split the inhouse id 
		
		-- Fetch parent inhouse transaction details and store in variables
	  BEGIN
		  SELECT c412_comments, c412_status_fl, c412_inhouse_purpose, c412_type, c412_ref_id, c901_ref_type, 
		          c412_release_to, c412_release_to_id, c412_created_by, c504a_loaner_transaction_id
		   INTO   v_comments, v_status_fl, v_purpose, v_type, v_refid, v_reftype,
		          v_shipto, v_shiptoid, v_userid, v_loaner_txn_id
		   FROM t412_inhouse_transactions
         WHERE c412_inhouse_trans_id = v_parent_inhouse_id
           AND c412_void_fl IS NULL;
	 EXCEPTION
            WHEN NO_DATA_FOUND THEN
                v_comments := NULL;
                v_status_fl := NULL;
                v_purpose := NULL;
                v_type := NULL;
                v_refid := NULL;
                v_reftype := NULL;
                v_shipto := NULL;
                v_shiptoid := NULL;
                v_userid := NULL;
                v_loaner_txn_id := NULL;
             END;
             
		-- procedure call to create new inhouse transaction id in t412_inhouse_transactions table  
		gm_pkg_op_request_master.gm_sav_inhouse_txn(v_comments, v_status_fl, v_purpose, v_type, v_refid, v_reftype, v_shipto, v_shiptoid, v_userid, v_out_inhouse_id);
		
	   --updating the ref id with the parent txn id for newly generated transaction id of all types except FGLE/FGLN/FGIS transactions
		IF (v_type NOT IN (50154,50155,50150)) THEN 
		
		  UPDATE t412_inhouse_transactions 
		     SET c412_ref_id = p_inhouse_id, 
		         c412_last_updated_date = current_date,
		         c412_last_updated_by = v_userid
		  WHERE c412_inhouse_trans_id = v_out_inhouse_id
		    AND c412_void_fl IS NULL;
		END IF; 
		
	    --updating the loaner transaction id for BUG-11502 for PMT-36676
	     UPDATE t412_inhouse_transactions 
		     SET c504a_loaner_transaction_id = v_loaner_txn_id, 
		         c412_last_updated_date = current_date,
		         c412_last_updated_by = v_userid
		  WHERE c412_inhouse_trans_id = v_out_inhouse_id
		    AND c412_void_fl IS NULL;
		    
	    -- Get the transaction type for device. If the transaction is from "shelf" it has to be picked from device.
		 BEGIN         
		   SELECT c901_code_id
		  INTO v_inv_type
		  FROM t901_code_lookup t901
		 WHERE t901.c901_code_grp IN ('INVPT', 'INVPW')
		   AND t901.c902_code_nm_alt = TO_CHAR (v_type)
		   AND t901.c901_active_fl = '1';
	  EXCEPTION
         WHEN NO_DATA_FOUND THEN
           v_inv_type := NULL;
     END;  
     
     -- Create Record in T5050 table for Device to process
       IF v_inv_type IS NOT NULL THEN
		 v_allocate_fl := gm_pkg_op_inventory.chk_txn_details (v_out_inhouse_id, v_inv_type);

		IF NVL (v_allocate_fl, 'N') = 'Y'
		THEN
			gm_pkg_allocation.gm_ins_invpick_assign_detail (TO_NUMBER (v_inv_type), v_out_inhouse_id, v_user_id);
		END IF;
	END IF;	   
	     
      -- procedure call to update t413_inhouse_trans_items table
      gm_update_items_t412inhouse_id(p_inhouse_id, v_out_inhouse_id, v_building_id);
		         
      --procedure call to ShipOut for FGLE transaction for PMT-33512
      --get the parent FGLE details
      IF (v_type = 50154) THEN --FGLE
      --for PMT-33512 FGLE txn
      BEGIN
      SELECT c907_ref_id, c901_ship_to, c907_ship_to_id, c901_delivery_carrier, c901_delivery_mode, c907_last_updated_by, c106_address_id 
        INTO  v_fgle_ref_id, v_fgle_ship_to, v_fgle_ship_to_id, v_fgle_delivery_carrier, v_fgle_delivery_mode, v_fgle_last_updated_by, v_fgle_address 
      FROM t907_shipping_info
      WHERE c907_ref_id = p_inhouse_id
      AND   c907_void_fl IS NULL;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
           v_fgle_ref_id := NULL;
           v_fgle_ship_to := NULL;
           v_fgle_ship_to_id := NULL;
           v_fgle_delivery_carrier := NULL;
           v_fgle_delivery_mode := NULL;
           v_fgle_last_updated_by := NULL;
           v_fgle_address := NULL;
     END;
     
      v_fgle_shipping_id := NULL;
      --procedure call to update shipping info for the child FGLE txn
     gm_pkg_cm_shipping_trans.gm_sav_shipping(v_out_inhouse_id, 50183, v_fgle_ship_to, v_fgle_ship_to_id, v_fgle_delivery_carrier, v_fgle_delivery_mode, NULL, v_fgle_address, NULL, v_fgle_last_updated_by, v_fgle_shipping_id);
      
      --Add Insert's for newly created FGLE inhouse transaction*/
      gm_pkg_op_sav_insert_txn.gm_sav_insert_rev_detail(v_out_inhouse_id, 106762, 93342, v_user_id, NULL);
      
      END IF;
    
      -- procedure call to update the building id for new inhouse id       
      gm_update_inhouset412_building_id(v_out_inhouse_id, v_user_id, v_building_id); 
      
        END IF;
      END LOOP; 
	
	END gm_inhouset412_split_transaction;

/***************************************************************************************************************************************
   * Purpose: This procedure is used to update the old inhouse transaction id (t504_consignment table) by giving new transaction id in t505_item_consignment table for PMT-33513
   * Author     : Swetha
   * Parameters  : Inhouse txn ID, building ID
 *************************************************************************************************************************************/

	PROCEDURE gm_update_items_t504inhouse_id(
	 p_old_inhouse_id IN t504_consignment.c504_consignment_id%TYPE,
	 p_new_inhouse_id IN t505_item_consignment.c504_consignment_id%TYPE,
	 p_building_id  IN t5052_location_master.c5057_building_id%TYPE
	)
	  AS
	  
	       
   BEGIN
   
   
    UPDATE t505_item_consignment 
        SET c504_consignment_id = p_new_inhouse_id
        WHERE c504_consignment_id = p_old_inhouse_id 
        AND c505_void_fl   IS NULL
        AND c205_part_number_id IN (SELECT t5053.c205_part_number_id  
	FROM t5053_location_part_mapping t5053, t5052_location_master t5052, t504_consignment t504, t505_item_consignment t505
     WHERE t5052.c5052_location_id= t5053.c5052_location_id
     AND t5052.c5040_plant_id   =  t504.c5040_plant_id
     AND t5052.c5052_void_fl IS NULL
     AND t5053.c5053_curr_qty  > 0
     AND t5052.c901_location_type = 93320  --Pick Face
     AND t5052.c5051_inv_warehouse_id = 1 --FG Warehouse
     AND t5052.c5057_building_id = p_building_id
     AND t5053.c205_part_number_id = t505.c205_part_number_id
     AND t504.c504_consignment_id = t505.c504_consignment_id
     AND t505.c504_consignment_id = p_old_inhouse_id 
     AND t504.c504_void_fl IS NULL
     AND t5052.c5052_void_fl IS NULL
     AND t505.c505_void_fl   IS NULL
     GROUP BY t5053.c205_part_number_id
    );
   
   
   END gm_update_items_t504inhouse_id;
   
  /***************************************************************************************************************************************
   * Purpose: This procedure is used to update the old inhouse transaction id ( t412_inhouse_transactions table) by giving new transaction id in t413_inhouse_trans_items table for PMT-33513
   * Author     : Swetha
   * Parameters  : Inhouse txn ID, building ID
 *************************************************************************************************************************************/

	PROCEDURE gm_update_items_t412inhouse_id(
	 p_old_inhouse_id IN t412_inhouse_transactions.c412_inhouse_trans_id %TYPE,
	 p_new_inhouse_id IN t413_inhouse_trans_items.c412_inhouse_trans_id%TYPE,
	 p_building_id  IN t5052_location_master.c5057_building_id%TYPE
	)
	  AS
	  
	       
   BEGIN
   
   
    UPDATE t413_inhouse_trans_items 
        SET c412_inhouse_trans_id = p_new_inhouse_id
        WHERE c412_inhouse_trans_id = p_old_inhouse_id 
        AND c413_void_fl   IS NULL
        AND c205_part_number_id IN (SELECT t5053.c205_part_number_id  
	FROM t5053_location_part_mapping t5053, t5052_location_master t5052, t412_inhouse_transactions t412, t413_inhouse_trans_items t413
     WHERE t5052.c5052_location_id= t5053.c5052_location_id
     AND t5052.c5040_plant_id   =  t412.c5040_plant_id
     AND t5052.c5052_void_fl IS NULL
     AND t5053.c5053_curr_qty  > 0
     AND t5052.c901_location_type = 93320  --Pick Face
     AND t5052.c5051_inv_warehouse_id = 1 --FG Warehouse
     AND t5052.c5057_building_id = p_building_id
     AND t5053.c205_part_number_id = t413.c205_part_number_id
     AND t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
     AND t413.c412_inhouse_trans_id = p_old_inhouse_id 
     AND t412.c412_void_fl  IS NULL
     AND t5052.c5052_void_fl IS NULL
     AND t413.c413_void_fl    IS NULL
     GROUP BY t5053.c205_part_number_id
    );
   
   
   END gm_update_items_t412inhouse_id;
  
  
END gm_pkg_op_storage_building_ih;
/
