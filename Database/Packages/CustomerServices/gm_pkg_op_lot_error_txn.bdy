CREATE OR REPLACE
PACKAGE BODY gm_pkg_op_lot_error_txn
IS


/*************************************************************************************************************************************
	* Description : This procedure is called when the resolution type is save Existing lot for the specified lot number  for PMT-29681
	* Author     : Swetha
	* Parameters  : error info id, Lot, part#, user id, resolution status, comments
****************************************************************************************************************************************/
	PROCEDURE gm_sav_exist_lot(
	 p_lot_error_info_id IN OUT t5063_lot_error_info.c5063_lot_error_info_id%TYPE,
	 p_lot_num  IN  t5063_lot_error_info.c2550_control_number%TYPE,
	 p_user_id  IN  t5063_lot_error_info.c5063_resolved_by%TYPE,
	 p_resolution_status_id IN  t5063_lot_error_info.c901_resolution_type%TYPE,
	 p_comments IN  t5063_lot_error_info.c5063_comments%TYPE,
	 p_part_no IN  t5063_lot_error_info.c205_part_number_id%TYPE,
	 p_lot_error_msg OUT VARCHAR2,
	 p_error_type OUT t5063_lot_error_info.C901_ERROR_TYPE%TYPE
	)
	  
	 AS
	  v_usage_lot t2550_part_control_number.C2550_CONTROL_NUMBER%TYPE := UPPER(p_lot_num);
	  v_lot_error_type t5063_lot_error_info.C901_ERROR_TYPE%TYPE;
	  v_lot_error_msg    VARCHAR2(200);
	  v_error_info_id t5063_lot_error_info.c5063_lot_error_info_id%TYPE;
	  v_company_id t5063_lot_error_info.c1900_company_id%TYPE;
      v_plant_id t5063_lot_error_info.c5040_plant_id%TYPE;
	  v_field_sales  t5063_lot_error_info.c701_trans_distributor_id%TYPE;
	  v_trans_id t5063_lot_error_info.c5063_trans_id%TYPE;
	  v_error_status_msg    VARCHAR2(200);
	  
	  
	    BEGIN		  
   
       SELECT get_compid_frm_cntx(), get_plantid_frm_cntx() 
			INTO   v_company_id ,v_plant_id
			FROM DUAL;
		
		BEGIN	
		   SELECT c701_trans_distributor_id, c5063_trans_id 
		   INTO  v_field_sales,v_trans_id
		   FROM t5063_lot_error_info
		   WHERE c5063_lot_error_info_id = p_lot_error_info_id 
		   AND c5063_void_fl IS NULL
		   AND c1900_company_id = v_company_id
		   AND c5040_plant_id = v_plant_id ;
	 EXCEPTION
            WHEN NO_DATA_FOUND THEN
                v_field_sales := NULL;
                v_trans_id := NULL;
            END;
            
		 --validate the lot	
         gm_pkg_op_lot_validation.gm_validate_lot_number(v_usage_lot,p_part_no,v_field_sales,v_lot_error_msg,v_lot_error_type);
         
         IF v_lot_error_msg IS NOT NULL
     THEN
        p_lot_error_msg := v_lot_error_msg;
        p_error_type := v_lot_error_type;
      
    RETURN;
    
       ELSE
        
         -- lot track update
         gm_pkg_op_lot_track.gm_lot_track_main(p_part_no, -1, v_trans_id, p_user_id, 4000339, 4302, 4310, v_usage_lot); --4000339 FS warehouse, 4302(minus) p_action,  4310(sales) p_type
         
         --to save error details into t5063
         gm_sav_lot_error_detail(p_lot_error_info_id,v_usage_lot,p_user_id,p_resolution_status_id,p_comments,p_part_no); 
       
         -- to save error status into t5063
         gm_sav_lot_error_status(p_lot_error_info_id,p_resolution_status_id,v_error_status_msg);
         
           
         IF v_error_status_msg IS NOT NULL
     THEN
        p_lot_error_msg := v_error_status_msg;
        p_error_type := NULL;
     END IF;
       
    
    END IF;
       
END gm_sav_exist_lot;
	




/*************************************************************************************************************************************
	* Description : This procedure is called when the resolution type is save new lot  PMT-29681
	* Author     : Swetha
	* Parameters  : error info id, Lot, part#, user id, resolution status, comments
****************************************************************************************************************************************/
	PROCEDURE gm_sav_new_lot(
	 p_lot_error_info_id IN OUT t5063_lot_error_info.c5063_lot_error_info_id%TYPE,
	 p_lot_num  IN  t5063_lot_error_info.c2550_control_number%TYPE,
	 p_user_id  IN  t5063_lot_error_info.c5063_resolved_by%TYPE,
	 p_resolution_status_id IN  t5063_lot_error_info.c901_resolution_type%TYPE,
	 p_comments IN  t5063_lot_error_info.c5063_comments%TYPE,
	 p_part_no IN  t5063_lot_error_info.c205_part_number_id%TYPE,
	 p_lot_error_msg OUT VARCHAR2,
	 p_error_type OUT t5063_lot_error_info.C901_ERROR_TYPE%TYPE
	)
	  
	 AS
	  v_usage_lot t2550_part_control_number.C2550_CONTROL_NUMBER%TYPE := UPPER(p_lot_num);
	  v_lot_error_type t5063_lot_error_info.C901_ERROR_TYPE%TYPE;
	  v_lot_error_msg    VARCHAR2(200);
	  v_error_info_id t5063_lot_error_info.c5063_lot_error_info_id%TYPE;
	  v_company_id t5063_lot_error_info.c1900_company_id%TYPE;
      v_plant_id t5063_lot_error_info.c5040_plant_id%TYPE;
	  v_field_sales  t5063_lot_error_info.c701_trans_distributor_id%TYPE;
	  v_trans_id t5063_lot_error_info.c5063_trans_id%TYPE;
	  v_error_status_msg    VARCHAR2(200);
	  v_order_info_id t5063_lot_error_info.c500_order_info_id%TYPE;
      v_usage_old_lot t5063_lot_error_info.c2550_control_number%TYPE;
	  
	  
	 
	    BEGIN	
	    
	    SELECT get_compid_frm_cntx(), get_plantid_frm_cntx() 
			INTO   v_company_id ,v_plant_id
			FROM DUAL;
		
		BEGIN	
		   SELECT c701_trans_distributor_id, c5063_trans_id, c500_order_info_id, c2550_control_number 
		   INTO  v_field_sales,v_trans_id,v_order_info_id,v_usage_old_lot
		   FROM t5063_lot_error_info
		   WHERE c5063_lot_error_info_id = p_lot_error_info_id 
		   AND c5063_void_fl IS NULL
		   AND c1900_company_id = v_company_id
		   AND c5040_plant_id = v_plant_id ;
		EXCEPTION
            WHEN NO_DATA_FOUND THEN
                v_field_sales := NULL;
                v_trans_id := NULL;
            END;	  
   
         --validate the lot	
         gm_pkg_op_lot_validation.gm_validate_lot_number(v_usage_lot,p_part_no,v_field_sales,v_lot_error_msg,v_lot_error_type);
         
         IF v_lot_error_msg IS NOT NULL
     THEN
        p_lot_error_msg := v_lot_error_msg;
        p_error_type := v_lot_error_type;
      
    RETURN;
    
       ELSE
        UPDATE T502B_ITEM_ORDER_USAGE
                  SET c502b_usage_lot_num           = v_usage_lot,
                    c502b_last_updated_by         = p_user_id,
                    c502b_last_updated_date       = CURRENT_DATE
                  WHERE c500_order_info_id=v_order_info_id AND c502b_usage_lot_num=v_usage_old_lot 
                  AND c502b_void_fl              IS NULL;
        
         -- lot track update
         gm_pkg_op_lot_track.gm_lot_track_main(p_part_no, -1, v_trans_id, p_user_id, 4000339, 4302, 4310, v_usage_lot); --4000339 FS warehouse, 4302(minus) p_action,  4310(sales) p_type
         
         --to save error details 
         gm_sav_lot_error_detail(p_lot_error_info_id,v_usage_lot,p_user_id,p_resolution_status_id,p_comments,p_part_no); 
       
         -- to save error status
         gm_sav_lot_error_status(p_lot_error_info_id,p_resolution_status_id,v_error_status_msg); 
         
          IF v_error_status_msg IS NOT NULL
     THEN
        p_lot_error_msg := v_error_status_msg;
        p_error_type := NULL;
    END IF;
       
    
    END IF;
       
END gm_sav_new_lot;
	

/*************************************************************************************************************************************
	* Description : This procedure is called when the resolution type is Mark as resolved for the lot  PMT-29681
	* Author     : Swetha
	* Parameters  : error info id, Lot, part#, user id, resolution status, comments
****************************************************************************************************************************************/
	PROCEDURE gm_sav_as_resolved(
	 p_lot_error_info_id IN t5063_lot_error_info.c5063_lot_error_info_id%TYPE,
	 p_lot_num  IN  t5063_lot_error_info.c2550_control_number%TYPE,
	 p_user_id  IN  t5063_lot_error_info.c5063_resolved_by%TYPE,
	 p_resolution_status_id IN  t5063_lot_error_info.c901_resolution_type%TYPE,
	 p_comments IN  t5063_lot_error_info.c5063_comments%TYPE,
	 p_part_no IN  t5063_lot_error_info.c205_part_number_id%TYPE,
	 p_lot_error_msg OUT VARCHAR2
	)
	  
	 AS
	    v_usage_lot t2550_part_control_number.C2550_CONTROL_NUMBER%TYPE := UPPER(p_lot_num);
	    v_error_status_msg    VARCHAR2(200);
	    v_error_type t5063_lot_error_info.c901_error_type%TYPE;
	    v_company_id t5063_lot_error_info.c1900_company_id%TYPE;
        v_plant_id t5063_lot_error_info.c5040_plant_id%TYPE;
	    
	    BEGIN	
	    
	    SELECT get_compid_frm_cntx(), get_plantid_frm_cntx() 
			INTO   v_company_id ,v_plant_id
			FROM DUAL;
	    
	    BEGIN	
		   SELECT c901_error_type
		   INTO  v_error_type
		   FROM t5063_lot_error_info
		   WHERE c5063_lot_error_info_id = p_lot_error_info_id 
		   AND c5063_void_fl IS NULL
		   AND c1900_company_id = v_company_id
		   AND c5040_plant_id = v_plant_id ;
	 EXCEPTION
            WHEN NO_DATA_FOUND THEN
                v_error_type := NULL;
           END;
	    	  
   
      --saving error details
       gm_sav_lot_error_detail(p_lot_error_info_id,v_usage_lot,p_user_id,p_resolution_status_id,p_comments,p_part_no);
       
         --create inhouse transactions for lot FS mismatch
         
       IF  v_error_type = 107975 THEN  --107975 FS mismatch error 
          gm_sav_lot_fs_transactions(p_lot_error_info_id,p_user_id); 
       END IF;
       
       --saving error status 
       gm_sav_lot_error_status(p_lot_error_info_id,p_resolution_status_id,v_error_status_msg); 
       
   IF v_error_status_msg IS NOT NULL
     THEN
        p_lot_error_msg := v_error_status_msg;
     END IF;
			
END gm_sav_as_resolved;
	

/*************************************************************************************************************************************
	* Description : This procedure is used to save the lot error details
	* Author     : Swetha
	* Parameters  : error info id, Lot, part#, user id, resolution status, comments
****************************************************************************************************************************************/
	PROCEDURE gm_sav_lot_error_detail(
	 p_lot_error_info_id IN t5063_lot_error_info.c5063_lot_error_info_id%TYPE,
	 p_lot_num  IN  t5063_lot_error_info.c2550_control_number%TYPE,
	 p_user_id  IN  t5063_lot_error_info.c5063_resolved_by%TYPE,
	 p_resolution_status_id IN  t5063_lot_error_info.c901_resolution_type%TYPE,
	 p_comments IN  t5063_lot_error_info.c5063_comments%TYPE,
	 p_part_no IN  t5063_lot_error_info.c205_part_number_id%TYPE
	)
	  
	 AS
	    BEGIN		  
          UPDATE t5063_lot_error_info
       SET c5063_updated_control_number = p_lot_num, c5063_resolved_by = p_user_id, c5063_last_updated_by = p_user_id, 
           c5060_resolved_date = current_date, c901_resolution_type = p_resolution_status_id, c5063_comments = p_comments
      WHERE c5063_lot_error_info_id = p_lot_error_info_id
       AND c205_part_number_id = p_part_no;
       
END gm_sav_lot_error_detail;
	
	


/*************************************************************************************************************************************
	* Description : This procedure is used to save the lot error status
	* Author     : Swetha
	* Parameters  : error info id, resolution status
****************************************************************************************************************************************/
	PROCEDURE gm_sav_lot_error_status(
	 p_lot_error_info_id IN t5063_lot_error_info.c5063_lot_error_info_id%TYPE,
     p_resolution_status_id IN  t5063_lot_error_info.c901_resolution_type%TYPE,
     p_error_status_msg OUT VARCHAR2
   )
	  
	 AS
	 
	  v_lot_no t5063_lot_error_info.c2550_control_number%TYPE;
	  v_error_flag t5063_lot_error_info.c5063_error_fl%TYPE;
	  v_status_fl t5063_lot_error_info.c901_status%TYPE;
	  v_void_fl t5063_lot_error_info.c5063_void_fl%TYPE;
	  v_pnum t5063_lot_error_info.c205_part_number_id%TYPE;
	  v_count		NUMBER;
	  v_error_status_msg  VARCHAR2(200);
	 
	    BEGIN	
	    
	  	  SELECT c901_status, c5063_void_fl, c205_part_number_id, c2550_control_number 
			INTO v_status_fl, v_void_fl, v_pnum, v_lot_no 
			  FROM t5063_lot_error_info
		WHERE c5063_lot_error_info_id = p_lot_error_info_id
         AND  c901_resolution_type = p_resolution_status_id
        FOR UPDATE;
        
      BEGIN
        IF (v_status_fl = 60472) THEN
          v_error_status_msg := 'Record is not in valid status';
        ELSIF (v_void_fl = 'Y') THEN
          v_error_status_msg := 'Record is already voided';
       END IF;
	 END;	
		  
    
   IF v_error_status_msg IS NOT NULL
     THEN
      p_error_status_msg := v_error_status_msg;
     RETURN;
  ELSE
     p_error_status_msg := NULL;
 END IF;    
	
          
          
          UPDATE t5063_lot_error_info
       SET c5063_error_fl = NULL, c901_status = 60472  --Making status as resolved
      WHERE c5063_lot_error_info_id = p_lot_error_info_id
      AND  c901_resolution_type = p_resolution_status_id
      AND c5063_void_fl is NULL;
        
        
     BEGIN
       
        SELECT COUNT(1) 
         INTO v_count 
        FROM t5063_lot_error_info 
       WHERE c205_part_number_id = v_pnum
        AND c2550_control_number = v_lot_no
        AND c5063_error_fl is not null;
        EXCEPTION 
           WHEN NO_DATA_FOUND
	    THEN
	       		v_count := 0;
	    END;
	    
 IF v_count = 0 
    THEN
      UPDATE t2550_part_control_number
        SET c2550_error_fl = NULL
       WHERE C2550_CONTROL_NUMBER = v_lot_no;
         
  END IF;
			
END gm_sav_lot_error_status;
	
	
	
/***********************************************************
*  Description : Procedure to create transfer transactions for lot FS mismatch
*  Parameter   : error info id
*  Author      : APrasath
***********************************************************/
PROCEDURE gm_sav_lot_fs_transactions (
   p_error_info_id   IN   t5063_lot_error_info.c5063_lot_error_info_id%TYPE
, p_user_id    IN   t5063_lot_error_info.c5063_last_updated_by%TYPE
)
AS
  v_transferid t921_transfer_detail.c920_transfer_id%TYPE;
  v_part_num t5063_lot_error_info.c205_part_number_id%TYPE;
  v_control_num t5063_lot_error_info.c2550_control_number%TYPE;
  v_cons_dist_id t5063_lot_error_info.c701_consigned_distributor_id%TYPE;
  v_tran_dist_id t5063_lot_error_info.c701_trans_distributor_id%TYPE;
  v_trans_id t5063_lot_error_info.c5063_trans_id%TYPE;
  v_trans_type t5063_lot_error_info.c5063_trans_type%TYPE;
  v_input_str VARCHAR2(100);
  v_out_errmsg VARCHAR2(4000);
  v_trans_cn t921_transfer_detail.c921_ref_id%TYPE;
  v_trans_ra t921_transfer_detail.c921_ref_id%TYPE;
  v_control_number_inv_id t5061_control_number_inv_log.c5060_control_number_inv_id%TYPE;
  v_lot_number_inv_id t2551_part_control_number_log.c2550_part_control_number_id%TYPE;
  v_company_id t5063_lot_error_info.c1900_company_id%TYPE;
  v_plant_id t5063_lot_error_info.c5040_plant_id%TYPE;

                BEGIN
       SELECT get_compid_frm_cntx(), get_plantid_frm_cntx()
                                                INTO   v_company_id ,v_plant_id
                                                FROM DUAL;
                   BEGIN
                   SELECT c205_part_number_id,c2550_control_number,c701_consigned_distributor_id,c701_trans_distributor_id,c5063_trans_id,c5063_trans_type
      INTO  v_part_num,v_control_num,v_cons_dist_id, v_tran_dist_id,v_trans_id,v_trans_type
      FROM t5063_lot_error_info
      WHERE C5063_LOT_ERROR_INFO_ID = p_error_info_id AND c5063_void_fl IS NULL;
    EXCEPTION
            WHEN NO_DATA_FOUND THEN
                v_tran_dist_id := NULL;
                v_cons_dist_id := NULL;
            END;

IF v_cons_dist_id IS NOT NULL AND v_tran_dist_id IS NOT NULL THEN


                  v_input_str := v_part_num||'^'||v_control_num||'^1|';
                  --create field sales part tranfer
                  gm_pkg_cs_transfer.gm_cs_sav_transfer('90300', '90352', v_cons_dist_id , v_tran_dist_id , '90321', to_char(SYSDATE,'MM/DD/YYYY'), p_user_id, NULL, v_input_str, v_transferid);
                  --complete part transfer
                  gm_pkg_cs_transfer.gm_cs_sav_accept_transfer(v_transferid,p_user_id,'Y',NULL,'54011',v_out_errmsg);

                 BEGIN
                  SELECT c921_ref_id INTO v_trans_cn FROM t921_transfer_detail
                    WHERE c920_transfer_id = v_transferid
                      AND c901_link_type = 90360
                      AND c921_void_fl IS NULL;
                    EXCEPTION
            WHEN NO_DATA_FOUND THEN
                v_trans_cn := NULL;
            END;

     BEGIN
                  SELECT c921_ref_id INTO v_trans_ra FROM t921_transfer_detail
                    WHERE c920_transfer_id = v_transferid
                      AND c901_link_type = 90361
                      AND c921_void_fl IS NULL;
                   EXCEPTION
            WHEN NO_DATA_FOUND THEN
                v_trans_ra := NULL;
            END;

                  IF v_trans_type = 107980 THEN --107980 Order
                                  /*UPDATE t2550_part_control_number
                                   SET c901_lot_controlled_status      = '105008'
                                    WHERE c2550_control_number = v_control_num
                                     AND c205_part_number_id = v_part_num; --controlled

                                  --Use transfer RA to decrease qty in FS
                                  gm_pkg_op_lot_track.gm_lot_track_main(v_part_num, -1,v_trans_ra, p_user_id, 4000339, 4302,4312,v_control_num);

                                  UPDATE t2550_part_control_number
                                   SET c901_lot_controlled_status      = NULL
                                    WHERE c2550_control_number = v_control_num
                                    AND c205_part_number_id = v_part_num;
                                  --Use transfer CN  to increase qty in FS
                                  gm_pkg_op_lot_track.gm_lot_track_main(v_part_num, 1,v_trans_cn, p_user_id, 90800, 4301,4311,v_control_num);
                                  --Use Order transaction to reduce FS inventory
                                  gm_pkg_op_lot_track.gm_lot_track_main(v_part_num, -1,v_trans_id, p_user_id, 4000339, 4302,4310,v_control_num);
      BEGIN
                                  SELECT c5060_control_number_inv_id INTO v_control_number_inv_id
                                   FROM t5060_control_number_inv
                                    WHERE c5060_control_number    = v_control_num
                                    --AND c5061_last_update_trans_id = v_trans_id
                                    --AND c901_warehouse_type       = 90812
                                    ;
                                 EXCEPTION
            WHEN NO_DATA_FOUND THEN
                v_control_number_inv_id := 0;
            END;*/

       BEGIN
                                  SELECT c2550_part_control_number_id INTO v_lot_number_inv_id
                                   FROM t2550_part_control_number
                                    WHERE c2550_control_number    = v_control_num
                                    --AND c2550_last_update_trans_id= v_trans_id
                                    --AND c901_last_updated_warehouse_type       = 4000339
                                    ;
                                EXCEPTION
            WHEN NO_DATA_FOUND THEN
                v_lot_number_inv_id := 0;
            END;
            
             --Increase lot data for Transfer CN in t5061
                                  gm_pkg_op_lot_track.gm_sav_control_num_inv_log (v_lot_number_inv_id , v_part_num, v_control_num, '4000339', 1 , 0, 1 , v_trans_cn , p_user_id , 103922 , 102909 , v_tran_dist_id , v_company_id , v_plant_id);
                                  --Decrease lot data for Transfer RA in  t5061
                                  gm_pkg_op_lot_track.gm_sav_control_num_inv_log (v_lot_number_inv_id , v_part_num, v_control_num, '4000339', -1 , 1, 0 , v_trans_ra , p_user_id , 103922 , 102909 , v_cons_dist_id , v_company_id , v_plant_id);
                                

      --Increase lot data for Transfer CN in t2551
                                  gm_pkg_op_lot_track_txn.gm_sav_lot_inventory_log (v_lot_number_inv_id , v_part_num, v_control_num, '105007', '4000339', v_trans_cn , p_user_id,102909 ,SYSDATE, v_tran_dist_id , 103922 ,NULL,NULL, v_company_id , v_plant_id);
                                  --Decrease lot data for Transfer RA in t2551
         gm_pkg_op_lot_track_txn.gm_sav_lot_inventory_log (v_lot_number_inv_id , v_part_num, v_control_num, '105009', '4000339', v_trans_ra , p_user_id,102909 ,SYSDATE, v_cons_dist_id ,103922, NULL,NULL,v_company_id , v_plant_id);
         --Update the tranfer date as transaction date (Order Created)
         UPDATE t2551_part_control_number_log
                                  SET c2551_created_date=
                                    (SELECT c501_created_date FROM t501_order WHERE c501_order_id = v_trans_id and c501_void_fl IS NULL
                                    )
                                  WHERE c2550_last_update_trans_id IN (v_trans_cn,v_trans_ra);
                  END IF;

                  IF v_trans_type = 107981 THEN --107980 Return
                   /* BEGIN
                                  SELECT c5060_control_number_inv_id INTO v_control_number_inv_id
                                   FROM t5060_control_number_inv
                                    WHERE c5060_control_number    = v_control_num
                                    --AND c5061_last_update_trans_id = v_trans_id
                                    --AND c901_warehouse_type       = 90812
                                    ;
                                 EXCEPTION
            WHEN NO_DATA_FOUND THEN
                v_control_number_inv_id := 0;
            END;*/

                                 BEGIN
                                  SELECT c2550_part_control_number_id INTO v_lot_number_inv_id
                                   FROM t2550_part_control_number
                                    WHERE c2550_control_number    = v_control_num
                                    --AND c2550_last_update_trans_id= v_trans_id
                                    --AND c901_last_updated_warehouse_type       = 90812
                                    ;
                                EXCEPTION
            WHEN NO_DATA_FOUND THEN
                v_lot_number_inv_id := 0;
            END;


                                  --Increase lot data for Transfer CN in t5061
                                  gm_pkg_op_lot_track.gm_sav_control_num_inv_log (v_lot_number_inv_id , v_part_num, v_control_num, '4000339', 1 , 0, 1 , v_trans_cn , p_user_id , 103922 , 102909 , v_tran_dist_id , v_company_id , v_plant_id);
                                 --Decrease lot data for Transfer RA in  t5061
                                  gm_pkg_op_lot_track.gm_sav_control_num_inv_log (v_lot_number_inv_id , v_part_num, v_control_num, '4000339', -1 , 1, 0 , v_trans_ra , p_user_id , 103922 , 102909 , v_cons_dist_id , v_company_id , v_plant_id);
                                  --Update the tranfer date as transaction date (RA Credit)
                                   --Increase lot data for Transfer CN in t2551
                                  gm_pkg_op_lot_track_txn.gm_sav_lot_inventory_log (v_lot_number_inv_id , v_part_num, v_control_num, '105007', '4000339', v_trans_cn , p_user_id,102909 ,SYSDATE, v_tran_dist_id , 103922 ,NULL,NULL, v_company_id , v_plant_id);
                                  --Decrease lot data for Transfer RA in t2551
         gm_pkg_op_lot_track_txn.gm_sav_lot_inventory_log (v_lot_number_inv_id , v_part_num, v_control_num, '105009', '4000339', v_trans_ra , p_user_id,102909 ,SYSDATE, v_cons_dist_id ,103922, NULL,NULL,v_company_id , v_plant_id);
         --Update the tranfer date as transaction date (RA Credit)
      UPDATE t2551_part_control_number_log
                                  SET c2551_created_date=
                                    (SELECT NVL(c506_credit_date,c506_created_date) FROM t506_returns WHERE c506_rma_id = v_trans_id AND c506_void_fl IS NULL
                                    )
                                  WHERE c2550_last_update_trans_id IN (v_trans_cn,v_trans_ra);
                  END IF;
END IF;
  END gm_sav_lot_fs_transactions;
  
  
  /*************************************************************************************************************************************
	* Description : This procedure is used to save the lot save Lot from Edit Order
	* Author     : Swetha
	* Parameters  : order id, Error Message, Lot, part#, 
****************************************************************************************************************************************/
	PROCEDURE gm_sav_lot_order_error(
	 p_order_id IN   t501_order.c501_order_id%TYPE,
	 p_error_msg IN  t502b_item_order_usage.c502b_usage_lot_errormsg%TYPE,
	 p_part_no  IN   t502b_item_order_usage.c205_part_number_id%TYPE,
	 p_usage_lot IN  t502b_item_order_usage.c502b_usage_lot_num%TYPE
	)
	  
	 AS
	 
	 v_usage_lot t2550_part_control_number.C2550_CONTROL_NUMBER%TYPE := UPPER(p_usage_lot);
	 
	    BEGIN		  

          UPDATE t502b_item_order_usage
       SET c502b_usage_lot_discripency = 'Y', c502b_usage_lot_errormsg = p_error_msg 
       WHERE c501_order_id = p_order_id
       AND c205_part_number_id = p_part_no
	   AND c502b_usage_lot_num = v_usage_lot 
	   AND c502b_void_fl IS NULL;

			
END gm_sav_lot_order_error;
  
  
  
 /*************************************************************************************************************************************
	* Description : This procedure is used to save the lot save Lot from Edit Order
	* Author     : Swetha
	* Parameters  : error type,Error Message, part#, Lot, order id, order date, user id, order info id
****************************************************************************************************************************************/
	PROCEDURE gm_sav_lot_error(
	 p_error_type IN t5063_lot_error_info.c901_error_type%TYPE,
	 p_error_msg IN t5063_lot_error_info.c5063_reason%TYPE,
	 p_part_no IN  t5063_lot_error_info.c205_part_number_id%TYPE,
	 p_lot_num  IN  t5063_lot_error_info.c2550_control_number%TYPE,
	 p_order_id IN   t501_order.c501_order_id%TYPE,
	 p_order_date IN t5063_lot_error_info.c5063_trans_date%TYPE,
	 p_order_info_id IN t5063_lot_error_info.c500_order_info_id%TYPE,
	 p_user_id  IN  t5063_lot_error_info.c5063_resolved_by%TYPE,
	 p_trans_type IN  t5063_lot_error_info.c5063_trans_type%TYPE,
	 p_field_sales IN  t5063_lot_error_info.c701_trans_distributor_id%TYPE,
	 p_returnitem_id IN t507_returns_item.C507_RETURNS_ITEM_ID%TYPE  
	 )
	  
	 AS
	 
	 v_company_id t5063_lot_error_info.c1900_company_id%TYPE;
     v_plant_id t5063_lot_error_info.c5040_plant_id%TYPE;
	 v_usage_lot t2550_part_control_number.C2550_CONTROL_NUMBER%TYPE := UPPER(p_lot_num);
	 v_consigned_fs t2550_part_control_number.c2550_ref_id%TYPE; 
	 
	 
	    BEGIN	
	    
	     /*Getting consigned field sales id for PMT-29681*/
            BEGIN
				SELECT C2550_REF_ID --distributor id, t2550_refid for consigned fs
					INTO  v_consigned_fs
				FROM t2550_part_control_number
				 WHERE C205_PART_NUMBER_ID = p_part_no
				 AND C2550_CONTROL_NUMBER = p_lot_num;
			 EXCEPTION
            WHEN NO_DATA_FOUND THEN
                v_consigned_fs := NULL;
            END;
            
            IF 	p_trans_type = 107980   --for IPAD order 107980   
             THEN
             
             BEGIN
              SELECT  c1900_company_id, c5040_plant_id 
                INTO  v_company_id, v_plant_id 
               FROM t501_order  
               WHERE  c501_order_id  = p_order_id;
             EXCEPTION
            WHEN NO_DATA_FOUND THEN
                v_company_id := NULL;
                v_plant_id := NULL;
            END;
              
              
              ELSIF p_trans_type = 107981 THEN   -- 107981 Return
              BEGIN
               SELECT  c1900_company_id, c5040_plant_id 
                INTO  v_company_id, v_plant_id 
               FROM t506_returns  
               WHERE  c506_rma_id  = p_order_id;
              EXCEPTION
            WHEN NO_DATA_FOUND THEN
                v_company_id := NULL;
                v_plant_id := NULL;
            END;
             
                END IF;

        SELECT NVL(v_company_id,get_compid_frm_cntx()), NVL(v_plant_id,get_plantid_frm_cntx())
			INTO   v_company_id ,v_plant_id
			FROM DUAL;
			
			UPDATE t5063_lot_error_info
	       SET     c5063_error_fl               =     'Y'
  		 WHERE	  c205_part_number_id       	=     p_part_no
  		   AND	  c2550_control_number     	    =     v_usage_lot
  		   AND	  c1900_company_id         	    =     v_company_id
  		   AND	  c5040_plant_id           	    =     v_plant_id
  		   AND	  c5063_trans_id           	    =     p_order_id
  		   AND	  c5063_trans_type         	    =     p_trans_type
  		   AND	  c5063_last_updated_by         =     p_user_id
  		   AND	  c701_trans_distributor_id     = 	  p_field_sales 
  		   AND	  c701_consigned_distributor_id =     v_consigned_fs
  		   AND	  c901_error_type               =     p_error_type
   		   AND	  c901_status					=     60471
   		   AND	  c5063_void_fl is NULL;
   		   
  		IF (SQL%ROWCOUNT = 0) THEN
		INSERT INTO t5063_lot_error_info
						(c5063_lot_error_info_id, c205_part_number_id, c2550_control_number, c1900_company_id, c5040_plant_id, c5063_trans_id,
					     c5063_trans_date, c5063_trans_type, c5063_reason, c5063_last_updated_date, c5063_last_updated_by, c5063_error_fl, c500_order_info_id, 
					     c701_trans_distributor_id,c701_consigned_distributor_id,c901_error_type,c507_returns_item_id, c5063_void_fl, c901_status
						)
				 VALUES (S5063_LOT_ERROR_INFO.NEXTVAL, p_part_no, v_usage_lot, v_company_id, v_plant_id, p_order_id, 
				        p_order_date, p_trans_type, p_error_msg, current_date, p_user_id, 'Y', p_order_info_id,
				        p_field_sales, v_consigned_fs, p_error_type, p_returnitem_id, NULL, 60471 
				       ); --60471 lot error in open status
				       
				   
		END IF;		   
	--updating error flag in t2550_part_control_number table 
	
	  UPDATE t2550_part_control_number
	   SET c2550_error_fl = 'Y'
	   WHERE c205_part_number_id = p_part_no
	   AND c2550_control_number = v_usage_lot;
			
END gm_sav_lot_error;
  
	
END gm_pkg_op_lot_error_txn;
/
