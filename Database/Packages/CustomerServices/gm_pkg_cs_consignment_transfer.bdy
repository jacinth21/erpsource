/* Formatted on 2019/10/06 18:43 (Formatter Plus v4.8.0) */
 /*******************************************************
   * Description : Package for Account Consignment transfer 

   *******************************************************/
   
CREATE OR REPLACE PACKAGE BODY gm_pkg_cs_consignment_transfer
IS

/***************************************************************************************************
	  * This procedure used to fetch the Item consignment details.
	  * Author : pvigneshwaran
***************************************************************************************************/
PROCEDURE gm_load_consignment_dtls (
		p_consign_id	 IN 	  T504_CONSIGNMENT.C504_CONSIGNMENT_ID%TYPE
	  , p_consign_cur	 OUT	  TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_consign_cur
		 FOR
	    SELECT T205.C205_PART_NUMBER_ID pnum, 
	    	   t205.c205_part_num_desc pdesc, 
	    	   SUM(NVL(T505.C505_ITEM_QTY,0)) qty,
	    	   T505.C505_CONTROL_NUMBER lot_num
        FROM T504_CONSIGNMENT T504, 
        	 T505_ITEM_CONSIGNMENT T505, 
        	 t205_part_number t205
        WHERE T504.C504_CONSIGNMENT_ID = p_consign_id--p_consign_id
          AND T504.C504_CONSIGNMENT_ID = T505.C504_CONSIGNMENT_ID
          AND T505.C205_PART_NUMBER_ID = t205.C205_PART_NUMBER_ID
          AND T504.C504_VOID_FL      IS NULL
          AND T505.C505_VOID_FL      IS NULL
          GROUP BY T205.C205_PART_NUMBER_ID,t205.c205_part_num_desc,T505.C505_CONTROL_NUMBER
          ORDER BY pnum,lot_num;

END gm_load_consignment_dtls;
/***************************************************************************************************
	  * This procedure used to validate the consignment details.
	  * Author : pvigneshwaran
***************************************************************************************************/
PROCEDURE gm_validate_consignment (
		 p_consign_id	 IN 	  T504_CONSIGNMENT.C504_CONSIGNMENT_ID%TYPE
	   , p_out_error_msg OUT	  VARCHAR2
	   
	)
AS
v_company_id  t1900_company.c1900_company_id%TYPE;
v_cn_id T504_CONSIGNMENT.C504_CONSIGNMENT_ID%TYPE;
v_account_id T504_CONSIGNMENT.C704_ACCOUNT_ID%TYPE;
v_status T504_CONSIGNMENT.C504_STATUS_FL%TYPE;
v_request_id T504_CONSIGNMENT.C520_REQUEST_ID%TYPE;
v_void_fl T504_CONSIGNMENT.C504_VOID_FL%TYPE;
v_cn_company T504_CONSIGNMENT.C1900_COMPANY_ID%TYPE;
v_cn_type T520_REQUEST.C520_REQUEST_FOR%TYPE;
v_cn_company_name CLOB;
v_count NUMBER;
v_date		   VARCHAR2(10);
v_ref_id VARCHAR2(20);
v_out_msg		  VARCHAR2(4000);
v_err_msg_fl VARCHAR2(10):= 'N';

BEGIN
	 
BEGIN
	--getting company id 
	SELECT get_compid_frm_cntx()INTO v_company_id FROM DUAL;
	
	SELECT T504.C504_CONSIGNMENT_ID,
       T504.C704_ACCOUNT_ID,
       T504.C504_STATUS_FL, 
       T504.C520_REQUEST_ID,
       T504.C504_VOID_FL, 
       T504.C1900_COMPANY_ID, 
       get_company_name (T504.C1900_COMPANY_ID)
  INTO v_cn_id, v_account_id, v_status, v_request_id, v_void_fl, v_cn_company , v_cn_company_name
  FROM T504_CONSIGNMENT T504
 WHERE T504.C504_CONSIGNMENT_ID = p_consign_id
   AND T504.C504_VOID_FL      IS NULL;
   
   --Get Type for account Item Consignment
   SELECT C520_REQUEST_FOR INTO v_cn_type 
   FROM T520_REQUEST 
   WHERE C520_REQUEST_ID=v_request_id
   AND C520_VOID_FL IS NULL;
   
   EXCEPTION  WHEN NO_DATA_FOUND THEN
   v_cn_id := NULL;
   v_account_id := NULL;
   v_status := NULL;
   v_void_fl := NULL;
   v_cn_company := NULL;
   v_cn_company_name := NULL;
   v_cn_type := NULL;
   END;
 
   --1.Validate the consignment
    IF v_cn_id IS  NULL THEN
      v_out_msg := 'Enter Valid <b>Consignment ID</b>';
      v_err_msg_fl := 'Y';
     END IF;
     
  
   --2.Consignment created in same company
      -- check CN created and context company is same - Company = v_company_id 
     IF v_cn_company <> v_company_id THEN
      v_out_msg := v_cn_id || ' has been created in '|| v_cn_company_name||'.Please enter another Consignment ID or login to the appropriate Company.'; 
       v_err_msg_fl := 'Y';
     END IF;
     
      
--   3.Validate the consignment status = 4
--	 Check CN status <> 4 then add error message
    IF v_status <> 4 THEN
      v_out_msg := 'The entered Consignment ID has not been shipped,therefore it cannot be transferred.'; 
       v_err_msg_fl := 'Y';
     END IF;
                
--     4.    CN already transfer
--Get the count (1) from T920 table - c504_consignment_id = p_consignment_id and status IN (1, 2).
     select  count (1) 
     into v_count 
     from t920_transfer 
     where c504_consignment_id = p_consign_id
     and c920_status_fl IN(1,2)
     and c920_void_fl is null;
      
	  IF v_count <> 0 THEN
	   select  
	   c920_to_ref_id,
	   c920_transfer_date 
	   INTO v_ref_id,v_date
	   from t920_transfer where c504_consignment_id = p_consign_id and c920_void_fl is null
	   and c920_status_fl <> '3' ;
	      v_out_msg := 'This Consignment has already been transferred. Please enter another Consignment ID.'; 
	      v_err_msg_fl := 'Y';
	    END IF;
    
    
   --5.Check CN is Account Item 
    IF v_cn_type <> 40025 THEN
      v_out_msg := p_consign_id||' is not an Account Item Consignment.Please enter a valid Account Item Consignment ID.'; 
      v_err_msg_fl := 'Y';
    END IF;
    
 
     IF v_err_msg_fl = 'N' THEN
    
      gm_load_consignment_info(p_consign_id,v_out_msg);
     END IF;

     p_out_error_msg := v_err_msg_fl||'##'||v_out_msg;

    
    
END gm_validate_consignment;
/***************************************************************************************************
	  * This procedure is used to fetch the consignment details
	  * Author : pvigneshwaran
***************************************************************************************************/
PROCEDURE gm_load_consignment_info (
		 p_consign_id	 IN 	 T504_CONSIGNMENT.C504_CONSIGNMENT_ID%TYPE
	    ,p_out_json_dtls OUT	 VARCHAR2
	)
AS
v_date_fmt VARCHAR2 (20);
BEGIN
	
	SELECT get_compdtfmt_frm_cntx ()
       INTO v_date_fmt
       FROM dual;
   --getting consignment details as JSON Object    
	  SELECT 
   JSON_OBJECT 
  ('accountName' value t704.c704_account_nm ,
  'account_id' value t704.c704_account_id ,
   'createdby' value get_user_name(t504.c504_created_by) ,
   'created_date' value TO_CHAR(T504.c504_created_date,v_date_fmt) ,
   'requestid' value T504.c520_request_id ,
   'shipped_date' value TO_CHAR(T504.c504_ship_date,v_date_fmt)
   )
   INTO p_out_json_dtls
  FROM T504_CONSIGNMENT T504, t704_account t704
  WHERE T504.C504_CONSIGNMENT_ID = p_consign_id
  AND t704.c704_account_id = t504.c704_account_id
   AND T504.C504_VOID_FL      IS NULL;
   
 END gm_load_consignment_info;
 
   /*******************************************************
    * Author  : ppandiyan
    * Description: This Procedure is used to update
    * consignment transfer details and creating new Request,Returns 
    * for new Transferred Consignment 
    *******************************************************/
 
 PROCEDURE gm_upd_consignment_transfer_dtls(
 		p_transfer_id IN  t920_transfer.c920_transfer_id%TYPE
	,   p_user_id     IN t101_user.c101_user_id%TYPE
 )
 AS
 v_consign_id  t504_consignment.c504_consignment_id%TYPE;
 v_company_id  t1900_company.c1900_company_id%TYPE;
 v_rma_id      t506_returns.c506_rma_id%TYPE;
 v_account_id  t704_account.c704_account_id%TYPE;
 v_out_request_id t520_request.c520_request_id%TYPE;
 v_tfcn_ID   t504_consignment.c504_consignment_id%TYPE;
 v_comments     VARCHAR(1000) := 'Request Created for Account Item Transfer';
 
 BEGIN
	
	BEGIN	   
	    --getting old consign id and to account id for corresponding transfer id		   
		 SELECT c504_consignment_id ,c920_to_ref_id ,c1900_company_id
		 INTO v_consign_id,v_account_id,v_company_id
		 FROM t920_transfer 
		 where c920_transfer_id = p_transfer_id
		 AND c920_void_fl is null;
		 
	  	 
	  	 --getting new consign id
	  	  SELECT c921_ref_id
	  	  INTO v_tfcn_ID
	  	  FROM t921_transfer_detail
	  	  WHERE c920_transfer_id =p_transfer_id
	 	  AND c901_link_type='90360'
	  	  AND c921_void_fl is null;
	  	  
	  	  
  EXCEPTION WHEN OTHERS THEN
	GM_RAISE_APPLICATION_ERROR('-20999','285','Transfer Id');
   END;
   
   BEGIN
   		--getting old RMA id 
		 SELECT t520.C506_RMA_ID
		 INTO v_rma_id
	 	 FROM T520_REQUEST t520, T504_CONSIGNMENT t504
	 	 WHERE t520.C520_REQUEST_ID    = t504.C520_REQUEST_ID
	  	 AND t504.c504_consignment_id = v_consign_id
	  	 AND t520.C520_RA_FL         = 'Y'
	  	 AND t504.C504_VOID_FL      IS NULL
	  	 AND t520.C520_VOID_FL      IS NULL
	  	 AND t504.c1900_company_id = v_company_id;
  EXCEPTION  WHEN NO_DATA_FOUND THEN
   v_rma_id := NULL;
   END;
   
   IF v_rma_id IS NOT NULL
   THEN
--void old GM-RA
	 gm_pkg_op_return.gm_cs_sav_void_return(v_rma_id,p_user_id); 
   END IF;	 
	 
   IF v_account_id IS NOT NULL AND v_tfcn_ID IS NOT NULL
   THEN
--creating request for new TFCN(Transfer CN)	 
	 gm_pkg_op_account_req_master.gm_sav_request(NULL
												, CURRENT_DATE
												, CURRENT_DATE
												, '50618'
												, NULL
												, NULL
												, '40025'
												, v_account_id
												, '50625'
												, p_user_id
												, '4122'
												, v_account_id
												, NULL
												, 40
												, p_user_id
												, '50060'
												, v_out_request_id
												, v_comments
												);
								
	--link newly created GM-RQ(Request) and TFCN(Transfer CN)									
		UPDATE t504_consignment 
		set c520_request_id = v_out_request_id,
		c504_last_updated_by=p_user_id,
		c504_last_updated_date=CURRENT_DATE 
		where c504_consignment_id=v_tfcn_ID
		and c1900_company_id=v_company_id
		and c504_void_fl is null;
		
	END IF;	
	
	IF v_out_request_id IS NOT NULL
	THEN
	--setting Return flag for newly created Request
	 gm_pkg_op_return_txn.gm_sav_return_fl_for_req(v_out_request_id,'Y',p_user_id);
	--save Return  for newly created Request
	 gm_pkg_op_return_txn.gm_sav_return_from_req(v_out_request_id,p_user_id);
	 
	END IF;


END gm_upd_consignment_transfer_dtls;

    /*******************************************************
    * Author  : ppandiyan
    * Description: This Procedure is used to reverse
    * the account item consignment transfer
    *******************************************************/
PROCEDURE gm_reverse_consignment_transfer(
 		p_transfer_id IN  t920_transfer.c920_transfer_id%TYPE
	  , p_user_id     IN t101_user.c101_user_id%TYPE
	  , p_out_cn_id  OUT  VARCHAR
)
AS
v_new_consign_id t504_consignment.c504_consignment_id%TYPE;
v_old_consign_id t504_consignment.c504_consignment_id%TYPE;
v_new_req_ra_id  t506_returns.c506_rma_id%TYPE;
v_new_rma_id     t506_returns.c506_rma_id%TYPE;
v_old_rma_id     t506_returns.c506_rma_id%TYPE;
v_from_id	   t920_transfer.c920_from_ref_id%TYPE;
v_to_id 	   t920_transfer.c920_to_ref_id%TYPE;
v_company_id  t1900_company.c1900_company_id%TYPE;    
v_no_data     VARCHAR2(1):= 'N';

BEGIN
   
	BEGIN
		 --getting old consignment id, from account , to account
	  SELECT c504_consignment_id , c920_from_ref_id , c920_to_ref_id
	    INTO v_old_consign_id , v_from_id, v_to_id
	  FROM t920_transfer 
	  where c920_transfer_id = p_transfer_id
	  AND c920_void_fl is null;
	

	--getting newly created TFCN(Transfer consignment ID)
	  SELECT c921_ref_id
  		  INTO v_new_consign_id
  	  FROM t921_transfer_detail
  	  WHERE c920_transfer_id =p_transfer_id
 	  AND c901_link_type='90360'
  	  AND c921_void_fl is null;
  	  
  	--getting newly created Returns(Transfer TFRA)  
  	  SELECT c921_ref_id
  		  INTO v_new_rma_id
  	  FROM t921_transfer_detail
  	  WHERE c920_transfer_id =p_transfer_id
 	  AND c901_link_type='90361'
  	  AND c921_void_fl is null;
	  
  	EXCEPTION  WHEN OTHERS THEN
		GM_RAISE_APPLICATION_ERROR('-20999','285','Transfer Id');

  	 END; 
		

  		--getting old RA using old Transferred CN
		  	 gm_pkg_op_return_rpt.gm_fch_RA_id_by_CN(v_old_consign_id, v_old_rma_id);
		--updating account net qty by lot 	 
              gm_pkg_set_lot_track.GM_PKG_SET_LOT_UPDATE(v_new_rma_id,p_user_id,v_from_id,'3304','5',4301,'loaner_rollback');
  	 		-- update the FS warehouse Qty
			-- 102901 - Return
              gm_pkg_op_inv_field_sales_tran.gm_update_fs_inv(v_new_rma_id, 'T506', v_from_id, 102901, 4301, 102909, p_user_id,'3304');
              
		--updating account net qty by lot 	
  	   		  gm_pkg_set_lot_track.GM_PKG_SET_LOT_UPDATE(v_new_consign_id,p_user_id,v_to_id,'50181','5',4301,'loaner_rollback');
			-- update the CN Qty
			-- 102900 - Consignment
			  gm_pkg_op_inv_field_sales_tran.gm_update_fs_inv(v_new_consign_id, 'T505', v_to_id, 102900, 4302, 102909, p_user_id,'40025');
  	 
		 --getting old RA using new Transferred CN
			 gm_pkg_op_return_rpt.gm_fch_RA_id_by_CN(v_new_consign_id, v_new_req_ra_id);
		--void newly created Returns for Transferred CN	 
			 IF v_new_req_ra_id is not null
			 THEN
			 	 gm_pkg_op_return.gm_cs_sav_void_return(v_new_req_ra_id,p_user_id);
			 END IF;
	          
	         IF v_old_rma_id is not null
			 THEN
	      --unvoid the voided old return   
	         UPDATE t506_returns
	       	 SET c506_void_fl  = null, 
	       	 c506_last_updated_by = p_user_id, 
	       	 c506_last_updated_date = CURRENT_DATE
	          WHERE c506_rma_id = v_old_rma_id;
	         END IF; 
	          
	       --transfer status should be reversed   
	            update t920_transfer 
	            set c920_status_fl='3',--Reversed
	            c920_last_updated_by=p_user_id,
	            c920_last_updated_date=current_date 
	            where c920_transfer_id=p_transfer_id;
	          
	          p_out_cn_id := v_new_consign_id;
	          
		  
END gm_reverse_consignment_transfer;

END gm_pkg_cs_consignment_transfer;
/