/* Formatted on 2011/12/09 11:54 (Formatter Plus v4.8.0) */
--@C:\Database\Packages\CustomerServices\gm_pkg_op_do_master.bdy;

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_do_master
IS
   PROCEDURE gm_sav_order_master (
      p_order_id           IN   t501_order.c501_order_id%TYPE,
      p_account_id         IN   t501_order.c704_account_id%TYPE,
      p_sales_rep_id       IN   t501_order.c703_sales_rep_id%TYPE,
      p_order_desc         IN   t501_order.c501_order_desc%TYPE,
      p_customer_po        IN   t501_order.c501_customer_po%TYPE,
      p_order_date         IN   t501_order.c501_order_date%TYPE,
      p_shipping_date      IN   t501_order.c501_shipping_date%TYPE,
      p_total_cost         IN   t501_order.c501_total_cost%TYPE,
      p_delivery_carrier   IN   t501_order.c501_delivery_carrier%TYPE,
      p_delivery_mode      IN   t501_order.c501_delivery_mode%TYPE,
      p_tracking_number    IN   t501_order.c501_tracking_number%TYPE,
      p_ship_to            IN   t501_order.c501_ship_to%TYPE,
      p_status_fl          IN   t501_order.c501_status_fl%TYPE,
      p_receive_mode       IN   t501_order.c501_receive_mode%TYPE,
      p_received_from      IN   t501_order.c501_received_from%TYPE,
      p_ship_charge_fl     IN   t501_order.c501_ship_charge_fl%TYPE,
      p_comments           IN   t501_order.c501_comments%TYPE,
      p_ship_to_id         IN   t501_order.c501_ship_to_id%TYPE,
      p_ship_cost          IN   t501_order.c501_ship_cost%TYPE,
      p_update_inv_fl      IN   t501_order.c501_update_inv_fl%TYPE,
      p_order_date_time    IN   t501_order.c501_order_date_time%TYPE,
      p_order_type         IN   t501_order.c901_order_type%TYPE,
      p_reason_type        IN   t501_order.c901_reason_type%TYPE,
      p_parent_order_id    IN   t501_order.c501_parent_order_id%TYPE,
      p_rma_id             IN   t501_order.c506_rma_id%TYPE,
      p_invoice_id         IN   t501_order.c503_invoice_id%TYPE,
      p_hold_fl            IN   t501_order.c501_hold_fl%TYPE,
      p_case_info_id       IN   t501_order.c7100_case_info_id%TYPE,
      p_user_id            IN   t501_order.c501_created_by%TYPE,
      p_action             IN   VARCHAR2,
      p_surgery_dt		   IN	t501_order.c501_surgery_date%TYPE
   )
   AS
      v_count   NUMBER;
      v_parent_cnt NUMBER;
      v_parent_order t501_order.c501_parent_order_id%TYPE;
      v_company_id    t1900_company.c1900_company_id%TYPE;
	  v_plant_id      t501_order.c5040_plant_id%TYPE;
	  v_division	  t501_order.c1910_division_id%TYPE;
      v_ship_cost  t501_order.c501_ship_cost%TYPE;
      v_dealer_id  t501_order.c101_dealer_id%TYPE;
      v_contract T704d_Account_Affln.c901_contract%TYPE;
      v_acc_cmp_id    t704_account.c704_account_id%TYPE;
      v_user_plant_id   t501_order.c5040_plant_id%TYPE;
   BEGIN
	  v_parent_order :=  TRIM(p_parent_order_id);
	  v_ship_cost := p_ship_cost;

	  --Getting company id and plant id from context.   
	  -- 1000: GMNA; 3000: Audubon
	    SELECT NVL(get_compid_frm_cntx(),1000), NVL(get_plantid_frm_cntx(),3000)
		  INTO v_company_id, v_plant_id
		  FROM dual;
		  
		-- Getting the division
		v_division := gm_pkg_sm_mapping.get_salesrep_division(p_sales_rep_id);
      IF p_action = 'NEWDO'
      THEN
         SELECT COUNT (1)
           INTO v_count
           FROM t501_order
          WHERE c501_order_id = p_order_id;

         IF v_count > 0
         THEN
         	GM_RAISE_APPLICATION_ERROR('-20999','27','');
         END IF;
      END IF;
	  --If order is child order , ship cost should  be 0
	  IF  v_parent_order  IS NOT NULL
		THEN
				   v_ship_cost :=0;
		END IF;           
	
	--getting dealer id from account id for Japan
	SELECT get_dealer_id_by_acct_id(p_account_id) 
	  INTO v_dealer_id 
	  FROM DUAL;
	--getting contract value from account
	  SELECT GET_ACCT_CONTRACT(p_account_id) 
	  INTO v_contract 
	  FROM DUAL; 
	  
	  BEGIN
        --getting company id and plant id mapped from account
        SELECT c1900_company_id,gm_pkg_op_plant_rpt.get_default_plant_for_comp(gm_pkg_it_user.get_default_party_company(p_user_id))
		  INTO v_acc_cmp_id, v_user_plant_id  
		  FROM T704_account 
		 WHERE c704_account_id = p_account_id 
		   AND c704_void_fl IS NULL;
		EXCEPTION
           WHEN NO_DATA_FOUND
           THEN
           v_acc_cmp_id := NULL;
           v_user_plant_id := NULL;
        END;
        
		
      UPDATE t501_order
         SET c704_account_id = p_account_id,
             c703_sales_rep_id = p_sales_rep_id,
             c501_order_desc = p_order_desc,
             c501_customer_po = p_customer_po,
             c501_order_date = decode(c501_order_date,null,p_order_date,c501_order_date),
             c501_shipping_date = p_shipping_date,
             c501_total_cost = p_total_cost,
             c501_delivery_carrier = p_delivery_carrier,
             c501_delivery_mode = p_delivery_mode,
             c501_tracking_number = p_tracking_number,
             c501_last_updated_by = p_user_id,
             c501_last_updated_date = CURRENT_DATE,
             c501_ship_to = p_ship_to,
             c501_status_fl = p_status_fl,
             c501_receive_mode = p_receive_mode,
             c501_received_from = p_received_from,
             c501_ship_charge_fl = p_ship_charge_fl,
             c501_comments = p_comments,
             c501_ship_to_id = p_ship_to_id,
             c501_ship_cost = NVL(v_ship_cost,c501_ship_cost),
             c501_update_inv_fl = p_update_inv_fl,
             c501_order_date_time = p_order_date_time,
             c901_order_type = p_order_type,
             c901_reason_type = p_reason_type,
             c501_parent_order_id = v_parent_order,
             c506_rma_id = p_rma_id,
             c503_invoice_id = p_invoice_id,
             c501_hold_fl = p_hold_fl,
             c7100_case_info_id = p_case_info_id,
             c501_surgery_date = p_surgery_dt,
             c901_contract = v_contract
       WHERE c501_order_id = p_order_id
         AND c501_void_fl IS NULL
         AND c501_delete_fl IS NULL;
         
      IF SQL%ROWCOUNT = 0
      THEN
         INSERT INTO t501_order
                     (c501_order_id, c704_account_id, c703_sales_rep_id,
                      c501_order_desc, c501_customer_po, c501_order_date,
                      c501_shipping_date, c501_total_cost,
                      c501_delivery_carrier, c501_delivery_mode,
                      c501_created_by, c501_tracking_number, c501_ship_to,
                      c501_status_fl, c501_receive_mode, c501_received_from,
                      c501_ship_charge_fl, c501_comments, c501_ship_to_id,
                      c501_ship_cost, c501_update_inv_fl,
                      c501_order_date_time, c901_order_type,
                      c901_reason_type, c501_parent_order_id, c506_rma_id,
                      c503_invoice_id, c501_hold_fl, c7100_case_info_id,
                      c501_created_date, c1900_company_id, c5040_plant_id, c1910_division_id,
                      c101_dealer_id, c501_surgery_date,c901_contract
                     )
              VALUES (p_order_id, p_account_id, p_sales_rep_id,
                      p_order_desc, p_customer_po, p_order_date,
                      p_shipping_date, p_total_cost,
                      p_delivery_carrier, p_delivery_mode,
                      p_user_id, p_tracking_number, p_ship_to,
                      p_status_fl, p_receive_mode, p_received_from,
                      p_ship_charge_fl, p_comments, p_ship_to_id,
                      v_ship_cost, p_update_inv_fl,
                      p_order_date_time, p_order_type,
                      p_reason_type, v_parent_order, p_rma_id,
                      p_invoice_id, p_hold_fl, p_case_info_id,
                      CURRENT_DATE,  nvl(v_company_id,v_acc_cmp_id), nvl(v_plant_id,v_user_plant_id), v_division,
                      v_dealer_id, p_surgery_dt,v_contract
                     );
      END IF;
      --The code is added to find out the parent order id is valid order id or not for the account.
         IF v_parent_order IS NOT NULL
         THEN         
	         SELECT count(1)
			   INTO v_parent_cnt
			   FROM t501_order
			  WHERE c501_order_id = v_parent_order
			    AND c704_account_id = p_account_id 
			    AND c501_void_fl IS NULL;
			
			  IF v_parent_cnt = 0
			  THEN
			  	 GM_RAISE_APPLICATION_ERROR('-20999','28',v_parent_order||'#$#'||p_order_id||'#$#'||p_account_id||'#$#'||get_account_name(p_account_id));
			  END IF;  
		  END IF;		  
	
   END gm_sav_order_master;

   PROCEDURE gm_sav_order_attribute_master (
      p_att_id      IN   t501a_order_attribute.c501a_order_attribute_id%TYPE,
      p_order_id    IN   t501a_order_attribute.c501_order_id%TYPE,
      p_att_type    IN   t501a_order_attribute.c901_attribute_type%TYPE,
      p_att_value   IN   t501a_order_attribute.c501a_attribute_value%TYPE,
      p_user_id     IN   t501a_order_attribute.c501a_created_by%TYPE
   )
   AS
      v_attribute_id   t501a_order_attribute.c501a_order_attribute_id%TYPE;
   BEGIN
      UPDATE t501a_order_attribute
         SET c501_order_id = p_order_id,
             c901_attribute_type = p_att_type,
             c501a_attribute_value = p_att_value,
             c501a_last_updated_by = p_user_id,
             c501a_last_updated_date = CURRENT_DATE
       WHERE c501a_order_attribute_id = p_att_id AND c501a_void_fl IS NULL;

      IF SQL%ROWCOUNT = 0
      THEN
         SELECT s501a_order_attribute.NEXTVAL
           INTO v_attribute_id
           FROM DUAL;

         INSERT INTO t501a_order_attribute
                     (c501a_order_attribute_id, c501_order_id,
                      c901_attribute_type, c501a_attribute_value,
                      c501a_created_by, c501a_created_date
                     )
              VALUES (v_attribute_id, p_order_id,
                      p_att_type, p_att_value,
                      p_user_id, CURRENT_DATE
                     );
      END IF;
   END gm_sav_order_attribute_master;

   PROCEDURE gm_sav_order_item_master (
      p_item_order_id     IN       t502_item_order.c502_item_order_id%TYPE,
      p_order_id          IN       t502_item_order.c501_order_id%TYPE,
      p_pnum              IN       t502_item_order.c205_part_number_id%TYPE,
      p_item_qty          IN       t502_item_order.c502_item_qty%TYPE,
      p_control_number    IN       t502_item_order.c502_control_number%TYPE,
      p_item_price        IN       t502_item_order.c502_item_price%TYPE,
      p_item_seq_no       IN       t502_item_order.c502_item_seq_no%TYPE,
      p_type              IN       t502_item_order.c901_type%TYPE,
      p_construct_fl      IN       t502_item_order.c502_construct_fl%TYPE,
      p_ref_id            IN       t502_item_order.c502_ref_id%TYPE,
      p_vat_rate          IN       t502_item_order.c502_vat_rate%TYPE,
      p_attribute_id      IN       t502_item_order.c502_attribute_id%TYPE,
      p_order_info_id     IN       t502_item_order.c500_order_info_id%TYPE,
      p_tax_amount		  IN       t502_item_order.c502_tax_amt%TYPE,
      p_out_item_ord_id   OUT      t502_item_order.c502_item_order_id%TYPE,
      p_ext_item_price	  IN	   t502_item_order.c502_item_order_id%TYPE DEFAULT NULL,
      p_igst_rate    		IN	t502_item_order.c502_igst_rate%TYPE DEFAULT NULL,
	  p_cgst_rate    		IN	t502_item_order.c502_cgst_rate%TYPE DEFAULT NULL,
	  p_sgst_rate    		IN	t502_item_order.c502_sgst_rate%TYPE DEFAULT NULL,
	  p_hsn_code    		IN	t502_item_order.c502_hsn_code%TYPE DEFAULT NULL,
	  p_rebate_rate    		IN	t502_item_order.c7200_rebate_rate%TYPE DEFAULT NULL,
	  p_rebate_process_fl	IN	t502_item_order.c502_rebate_process_fl%TYPE DEFAULT NULL,
	  p_tcs_rate          IN    t502_item_order.c502_tcs_rate%TYPE DEFAULT NULL
   )
   AS
      v_item_order_id   t502_item_order.c502_item_order_id%TYPE;
      v_list_price		t502_item_order.c502_list_price%TYPE;
      v_unit_price		t502_item_order.c502_unit_price%TYPE;
      v_account_price 	VARCHAR2(50);
      v_adj_code_value 	VARCHAR2(50) := 0;
      v_count			NUMBER :=0;
      
   BEGIN
      v_item_order_id := p_item_order_id;
      
      BEGIN
      	SELECT TO_NUMBER(TRIM(get_part_price_list ( p_pnum,'C')))
      	  INTO v_list_price
      	  FROM dual;
      	  
      	  select get_account_part_pricing(c704_account_id, p_pnum,NULL) 
      	    INTO v_unit_price
      	    from t501_order t501 
      	   where c501_order_id = p_order_id;
      	   
      EXCEPTION WHEN NO_DATA_FOUND THEN
      		v_list_price := p_item_price;
      END;
      
      --PC-2561	Do not update the new order incase of order has only one part
	  IF v_item_order_id IS NOT NULL
	  THEN
	   SELECT COUNT(1)
	     INTO v_count
	     FROM t502_item_order
	    WHERE c501_order_id IN (SELECT c501_order_id 
	                                   FROM t502_item_order 
	                                  WHERE c502_item_order_id = v_item_order_id 
	                                    AND c502_void_fl IS NULL);
	  END IF;
      

      --v_adj_code_value := gm_pkg_sm_adj_txn.get_adj_code_value(p_order_id,p_pnum,v_item_order_id);
	  --v_account_price := p_item_price + 0 + v_adj_code_value;
      UPDATE t502_item_order
         SET c501_order_id = decode(v_count,1,c501_order_id,p_order_id),
             c205_part_number_id = p_pnum,
             c502_item_qty = p_item_qty,
             c502_control_number = p_control_number,
             c502_item_price = p_item_price,
             c502_item_seq_no = p_item_seq_no,
             c901_type = p_type,
             c502_construct_fl = p_construct_fl,
             c502_ref_id = p_ref_id,
             c502_vat_rate = p_vat_rate,
             c502_attribute_id = p_attribute_id,
             c502_unit_price = p_item_price,
		     c502_unit_price_adj_value = 0,
		     c502_list_price = v_list_price,	             		   
		     c502_system_cal_unit_price = p_item_price,
		     C502_DO_UNIT_PRICE  = p_item_price,
		     C502_ADJ_CODE_VALUE = 0,
		     c500_order_info_id = p_order_info_id,
		     c502_ext_item_price = p_ext_item_price,
		     c502_tax_amt = p_tax_amount,
		     c502_igst_rate = p_igst_rate,
      		 c502_cgst_rate = p_cgst_rate,
             c502_sgst_rate = p_sgst_rate,
             c502_hsn_code = p_hsn_code,
             c7200_rebate_rate= p_rebate_rate ,
             c502_rebate_process_fl = p_rebate_process_fl,
             c502_tcs_rate = p_tcs_rate

       WHERE c502_item_order_id = v_item_order_id
         AND c502_void_fl IS NULL
         AND c502_delete_fl IS NULL;

      IF SQL%ROWCOUNT = 0
      THEN
         SELECT s502_item_order.NEXTVAL
           INTO v_item_order_id
           FROM DUAL;
         INSERT INTO t502_item_order
                     (c502_item_order_id, c501_order_id, c205_part_number_id,
                      c502_item_qty, c502_control_number, c502_item_price,
                      c502_item_seq_no, c901_type, c502_construct_fl,
                      c502_ref_id, c502_vat_rate, c502_attribute_id,
                      c502_unit_price, c502_unit_price_adj_value, c502_list_price,	             		   
		     		  c502_system_cal_unit_price, C502_DO_UNIT_PRICE , C502_ADJ_CODE_VALUE,
		     		  c500_order_info_id, c502_ext_item_price, c502_tax_amt, c502_igst_rate, c502_cgst_rate, 
		     		  c502_sgst_rate, c502_hsn_code, c7200_rebate_rate,
		     		  c502_rebate_process_fl, c502_tcs_rate
                     )
              VALUES (v_item_order_id, p_order_id, p_pnum,
                      p_item_qty, p_control_number, p_item_price,
                      p_item_seq_no, p_type, p_construct_fl,
                      p_ref_id, p_vat_rate, p_attribute_id,
                      p_item_price, 0, v_list_price,
                      p_item_price , p_item_price , 0,
                      p_order_info_id, p_ext_item_price, p_tax_amount,p_igst_rate,p_cgst_rate,
                      p_sgst_rate, p_hsn_code, p_rebate_rate, 
                      p_rebate_process_fl, p_tcs_rate
                     );
      END IF;

      p_out_item_ord_id := v_item_order_id;
   END gm_sav_order_item_master;

   PROCEDURE gm_sav_tag_master (
      p_tag_usage_id   IN   t5012_tag_usage.c5012_tag_usage_id%TYPE,
      p_ref_id         IN   t5012_tag_usage.c5012_ref_id%TYPE,
      p_partnum        IN   t5012_tag_usage.c205_part_number_id%TYPE,
      p_ref_type       IN   t5012_tag_usage.c901_ref_type%TYPE,
      p_tag_id         IN   t5012_tag_usage.c5010_tag_id%TYPE,
      p_qty            IN   t5012_tag_usage.c5012_qty%TYPE,
      p_user_id        IN   t5012_tag_usage.c5012_last_updated_by%TYPE
   )
   AS
      v_tag_usage_id   t5012_tag_usage.c5012_tag_usage_id%TYPE;
   BEGIN
      UPDATE t5012_tag_usage
         SET c5012_ref_id = p_ref_id,
             c205_part_number_id = p_partnum,
             c901_ref_type = p_ref_type,
             c5010_tag_id = p_tag_id,
             c5012_last_updated_by = p_user_id,
             c5012_last_updated_date = CURRENT_DATE,
             c5012_qty = p_qty
       WHERE c5012_tag_usage_id = p_tag_usage_id AND c5012_void_fl IS NULL;
       

      IF SQL%ROWCOUNT = 0
      THEN
         SELECT s5012_tag_usage.NEXTVAL
           INTO v_tag_usage_id
           FROM DUAL;

         INSERT INTO t5012_tag_usage
                     (c5012_tag_usage_id, c5012_ref_id, c205_part_number_id,
                      c901_ref_type, c5012_qty, c5010_tag_id,
                      c5012_created_by, c5012_created_date
                     )
              VALUES (v_tag_usage_id, p_ref_id, p_partnum,
                      p_ref_type, p_qty, p_tag_id,
                      p_user_id, CURRENT_DATE
                     );
      END IF;
   END gm_sav_tag_master;

   PROCEDURE gm_sav_item_attr_master (
      p_item_att_id       IN   t502a_item_order_attribute.c502a_item_order_attribute_id%TYPE,
      p_order_id          IN   t502a_item_order_attribute.c501_order_id%TYPE,
      p_partnum           IN   t502a_item_order_attribute.c205_part_number_id%TYPE,
      p_attribute_type    IN   t502a_item_order_attribute.c901_attribute_type%TYPE,
      p_attribute_value   IN   t502a_item_order_attribute.c502a_attribute_value%TYPE,
      p_attribute_id      IN   t502a_item_order_attribute.c502_attribute_id%TYPE,
      p_user_id           IN   t502a_item_order_attribute.c502a_last_updated_by%TYPE
   )
   AS
      v_order_attribute_id   t502a_item_order_attribute.c502a_item_order_attribute_id%TYPE;
   BEGIN
      UPDATE t502a_item_order_attribute
         SET c501_order_id = p_order_id,
             c205_part_number_id = p_partnum,
             c901_attribute_type = p_attribute_type,
             c502a_attribute_value = p_attribute_value,
             c502a_last_updated_by = p_user_id,
             c502a_last_updated_date = CURRENT_DATE,
             c502_attribute_id = p_attribute_id
       WHERE c502a_item_order_attribute_id = p_item_att_id
         AND c502a_void_fl IS NULL;

      IF SQL%ROWCOUNT = 0
      THEN
         SELECT s502a_item_order_attribute.NEXTVAL
           INTO v_order_attribute_id
           FROM DUAL;

         INSERT INTO t502a_item_order_attribute
                     (c502a_item_order_attribute_id, c501_order_id,
                      c205_part_number_id, c901_attribute_type,
                      c502_attribute_id, c502a_attribute_value,
                      c502a_last_updated_by, c502a_last_updated_date
                     )
              VALUES (v_order_attribute_id, p_order_id,
                      p_partnum, p_attribute_type,
                      p_attribute_id, p_attribute_value,
                      p_user_id, CURRENT_DATE
                     );
      END IF;
   END gm_sav_item_attr_master;
   
 /**********************************************************************************
	Description : This procedure is used to save sterile parts in item attribute  
 ***********************************************************************************/
   
   PROCEDURE gm_sav_item_attribute_master (
      p_item_att_id       IN   t502a_item_order_attribute.c502a_item_order_attribute_id%TYPE,
      p_order_id          IN   t502a_item_order_attribute.c501_order_id%TYPE,
      p_partnum           IN   t502a_item_order_attribute.c205_part_number_id%TYPE,
      p_attribute_type    IN   t502a_item_order_attribute.c901_attribute_type%TYPE,
      p_attribute_value   IN   t502a_item_order_attribute.c502a_attribute_value%TYPE,
      p_qty			      IN   t502a_item_order_attribute.c502a_item_qty %TYPE,
      p_user_id           IN   t502a_item_order_attribute.c502a_last_updated_by%TYPE
   )
   AS
      v_order_attribute_id   t502a_item_order_attribute.c502a_item_order_attribute_id%TYPE;
   BEGIN
      UPDATE t502a_item_order_attribute
         SET c501_order_id = p_order_id,
             c205_part_number_id = p_partnum,
             c901_attribute_type = p_attribute_type,
             c502a_attribute_value = p_attribute_value,
             c502a_last_updated_by = p_user_id,
             c502a_last_updated_date = CURRENT_DATE,
             c502a_item_qty = p_qty
       WHERE c502a_item_order_attribute_id = p_item_att_id
         AND c502a_void_fl IS NULL;

      IF SQL%ROWCOUNT = 0
      THEN
         SELECT s502a_item_order_attribute.NEXTVAL
           INTO v_order_attribute_id
           FROM DUAL;

         INSERT INTO t502a_item_order_attribute
                     (c502a_item_order_attribute_id, c501_order_id,
                      c205_part_number_id, c901_attribute_type,
                      c502_attribute_id, c502a_attribute_value,
                      c502a_last_updated_by, c502a_last_updated_date,
                      c502a_item_qty
                     )
              VALUES (v_order_attribute_id, p_order_id,
                      p_partnum, p_attribute_type,
                      NULL, p_attribute_value,
                      p_user_id, CURRENT_DATE,p_qty
                     );
      END IF;
   END gm_sav_item_attribute_master;
   
 /**********************************************************************************
	Description : This procedure is used to get dist_id,ad_id,vp_id for the sales rep  
 ***********************************************************************************/

   PROCEDURE gm_cs_fch_advp_ids (   
	   p_rep_id 	IN t703_sales_rep.c703_sales_rep_id%TYPE,
	   p_dist_id 	OUT t701_distributor.c701_distributor_id%TYPE,
	   p_ad_id 		OUT t708_region_asd_mapping.c101_user_id%TYPE,
	   p_vp_id 		OUT t708_region_asd_mapping.c101_user_id%TYPE
   )
   AS
   BEGIN
	   p_dist_id := gm_pkg_op_do_master.GET_DISTRIBUTOR_ID(p_rep_id); 
	   p_ad_id	 := gm_pkg_op_do_master.GET_AD_REP_ID(p_rep_id);  	
	   p_vp_id	 := gm_pkg_op_do_master.GET_VP_REP_ID(p_rep_id); 	
   END gm_cs_fch_advp_ids; 

 /**********************************************************************************
	Description : This function is used to get distributor ID (number format) for the sales rep  
 ***********************************************************************************/   
   
   FUNCTION GET_DISTRIBUTOR_ID
	(p_rep_id T703_SALES_REP.C703_SALES_REP_ID%TYPE)
	RETURN NUMBER
	IS
	v_dist_id   T701_DISTRIBUTOR.C701_DISTRIBUTOR_ID%TYPE;
	BEGIN
		BEGIN
			   SELECT c701_distributor_id
			     INTO v_dist_id
			     FROM t703_sales_rep
			    WHERE c703_sales_rep_id = p_rep_id
				  AND c703_void_fl IS NULL;
			    
		   	EXCEPTION WHEN NO_DATA_FOUND THEN
		      RETURN null;
		END;
	     RETURN v_dist_id;
	END GET_DISTRIBUTOR_ID;
	
 /**********************************************************************************
	Description : This function is used to get AD ID (number format) for the sales rep  
 ***********************************************************************************/   
   
   FUNCTION GET_AD_REP_ID	(
   		p_rep_id T703_SALES_REP.C703_SALES_REP_ID%TYPE
   	)
	RETURN NUMBER  
	AS
		v_ad_id 	t708_region_asd_mapping.c101_user_id%TYPE;
	BEGIN
	   BEGIN
		   SELECT t708.c101_user_id
		     INTO v_ad_id
		     FROM t703_sales_rep t703, t701_distributor t701, t708_region_asd_mapping t708
		    WHERE t703.c703_sales_rep_id = p_rep_id
		      AND t703.c701_distributor_id = t701.c701_distributor_id
		      AND t701.c701_region = t708.c901_region_id
		      AND t708.c708_delete_fl IS NULL
		      AND t708.c901_user_role_type = 8000 -- The user role type is AD
			  AND t708.c708_delete_fl IS NULL
			  AND t703.c703_void_fl IS NULL
			  AND t701.c701_void_fl IS NULL;
			  
	  	EXCEPTION WHEN NO_DATA_FOUND THEN
		     RETURN null;
	  END;
	  RETURN v_ad_id;
	END GET_AD_REP_ID;
 /**********************************************************************************
	Description : This function is used to get VP ID (number format) for the sales rep  
 ***********************************************************************************/   
   
   FUNCTION GET_VP_REP_ID	(
   		p_rep_id T703_SALES_REP.C703_SALES_REP_ID%TYPE
   	)
	RETURN NUMBER	
	AS
		v_vp_id 	t708_region_asd_mapping.c101_user_id%TYPE;
	BEGIN
	  BEGIN
		   SELECT t708.c101_user_id 
		     INTO v_vp_id
			 FROM t703_sales_rep t703, t701_distributor t701, t708_region_asd_mapping t708
			WHERE t703.c703_sales_rep_id = p_rep_id
			  AND t703.c701_distributor_id = t701.c701_distributor_id
			  AND t701.c701_region = t708.c901_region_id
    		  AND t708.c901_user_role_type = '8001' -- The user role type is  VP REGION
			  AND t708.c708_delete_fl IS NULL
			  AND t703.c703_void_fl IS NULL
			  AND t701.c701_void_fl IS NULL;

	  	EXCEPTION WHEN NO_DATA_FOUND THEN
		     RETURN NULL;
	  END;
	   RETURN v_vp_id;
	END GET_VP_REP_ID;
/******************************************************************
* Description : Procedure to save the order attribute details.
* Author    : Ritesh
****************************************************************/
   PROCEDURE gm_sav_order_attribute (
      p_ordid    IN   t501_order.c501_order_id%TYPE,
      p_userid   IN   t501_order.c501_last_updated_by%TYPE,
      p_str      IN   VARCHAR2      
   )
   AS
      v_strlen         NUMBER                      := NVL (LENGTH (p_str), 0);
      v_string         VARCHAR2 (4000)                              := p_str;
      v_substring      VARCHAR2 (4000);
      v_ord_id     t501_order.c501_order_id%TYPE;
      v_attrtype       t501a_order_attribute.c901_attribute_type%TYPE;
      v_attrval        t501a_order_attribute.c501a_attribute_value%TYPE;
	  v_company_id	   t1900_company.c1900_company_id%TYPE;      
   BEGIN

		--PC-3581 Globus Dl No not Appearing In Sales Invoice
		BEGIN
			SELECT c1900_company_id 
			  INTO v_company_id
			  FROM t501_order 
			 WHERE c501_order_id = p_ordid
		       AND c501_void_fl IS NULL;
		   
         EXCEPTION WHEN NO_DATA_FOUND THEN
      		  v_company_id := '';
	    END;
	    
         WHILE INSTR (v_string, '|') <> 0
         LOOP
            v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
            v_string := SUBSTR (v_string, INSTR (v_string, '|') + 1);
            --
            v_ord_id := NULL;
            v_attrtype := NULL;
            v_attrval := NULL;
            --
            v_ord_id := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
            v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
            v_attrtype := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
            v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
            v_attrval := v_substring;
            
            IF v_attrval IS NOT NULL THEN
            gm_pkg_op_do_master.gm_sav_order_attribute_master
                                              (NULL,
                                               p_ordid,
                                               v_attrtype,  
                                               v_attrval,
                                               p_userid
                                              );
           END IF;                                       
         END LOOP; 
         --PC-3581 Globus Dl No not Appearing In Sales Invoice
         gm_pkg_sav_snapshot_info.gm_sav_snapshot_info('ORDER',p_ordid,'SNAPSHOT_INFO',v_company_id,'1200',p_userid);    
   END gm_sav_order_attribute;
   
   /******************************************************************
* Description :Procedure to save the surgery date  attribute details.
* Author      : prabhu vigneshwaran M D 
****************************************************************/
   PROCEDURE gm_sav_surgery_date (
      p_ordid    IN   t501_order.c501_order_id%TYPE,
      p_userid   IN   t501_order.c501_last_updated_by%TYPE,
      p_str      IN   VARCHAR2      
   )
   AS
      v_strlen         NUMBER                      := NVL (LENGTH (p_str), 0);
      v_string         VARCHAR2 (4000)                              := p_str;
      v_substring      VARCHAR2 (4000);
      v_ord_id     t501_order.c501_order_id%TYPE;
      v_attrtype       t501a_order_attribute.c901_attribute_type%TYPE;
      v_attrval        t501a_order_attribute.c501a_attribute_value%TYPE;    
      v_date_fmt VARCHAR2(100);
   BEGIN
     	  SELECT get_compdtfmt_frm_cntx()
		  INTO v_date_fmt
		  FROM dual; 
         WHILE INSTR (v_string, '|') <> 0
         LOOP
            v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
            v_string := SUBSTR (v_string, INSTR (v_string, '|') + 1);
            --
            v_ord_id := NULL;
            v_attrtype := NULL;
            v_attrval := NULL;
            --
            v_ord_id := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
            v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
            v_attrtype := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
            v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
            v_attrval := v_substring;
            
         IF v_attrtype = 103601 THEN
        UPDATE t501_order
           SET c501_surgery_date = to_date(v_attrval,v_date_fmt),
               c501_last_updated_by=p_userid,
               c501_last_updated_date=CURRENT_DATE
         WHERE c501_order_id = v_ord_id
           AND c501_void_fl IS NULL
           AND c501_delete_fl IS NULL;
         END IF;                                       
         END LOOP;     
   END gm_sav_surgery_date;
	
END gm_pkg_op_do_master;
/
