/**
 * @C:\PMT\db\Packages\CustomerServices\gm_pkg_order_tag_record.bdy;
 * AUTHOR:N RAJA
 * This Package Procedure used for Transaction save, fetch names, fetch transaction details
 */
 
CREATE OR REPLACE PACKAGE BODY gm_pkg_order_tag_record
IS
 /**************************************************************************************************
    * Description : Procedure to Save the  the adding Order tag data
    * Author :N RAJA 
   **************************************************************************************************/
PROCEDURE gm_save_order_tag_transaction(      
        p_tag_id            IN         CLOB,
        p_set_id            IN         T501D_ORDER_TAG_USAGE_DETAILS.C207_SET_ID%TYPE,
        p_ref_id            IN OUT     T501D_ORDER_TAG_USAGE_DETAILS.C501_ORDER_ID%TYPE,
        p_account_id        IN OUT     T5010B_TAG_LOCATION_DETAILS.C704_ACCOUNT_ID%TYPE,
        p_userid            IN         T501D_ORDER_TAG_USAGE_DETAILS.C501D_LAST_UPDATED_BY%TYPE
    )
 AS   
   v_company_id t1900_company.c1900_company_id%TYPE;   
   v_account_id T501_ORDER.C704_ACCOUNT_ID%TYPE; 
   v_number number := 0;   
   v_tag_append T501D_ORDER_TAG_USAGE_DETAILS.C5010_TAG_ID%TYPE;
   v_tempid  T501D_ORDER_TAG_USAGE_DETAILS.C501_ORDER_ID%TYPE;
   
   CURSOR tag_id_cur
		IS
		SELECT c205_part_number_id ptagId  FROM my_t_part_list;
 BEGIN
       
       SELECT get_compid_frm_cntx()
         INTO v_company_id 
         FROM dual;       
         
    	-- set the clobe value to context
		my_context.set_my_cloblist(p_tag_id);	
		
	  FOR tag_id IN tag_id_cur
       LOOP 
       
       SELECT IS_NUMBER (tag_id.ptagId) 
         INTO v_number 
         FROM DUAL;
           
       IF (LENGTH(tag_id.ptagId)<7 AND v_number = 1) THEN  -- Checked the tag is nunmber , if yes appened Zero's in front of the number(5--> 0000005)           
          
       	  SELECT LPAD(tag_id.ptagId,'7','0') 
            INTO v_tag_append  
            FROM DUAL;        
       ELSE
            
      	  v_tag_append := tag_id.ptagId;
       END IF;
        -- Validate the input
        gm_pkg_order_tag_record.gm_validate_tag(v_tag_append,p_set_id,p_ref_id);        
            
        IF(p_account_id IS NULL) THEN  
		BEGIN
          SELECT C704_ACCOUNT_ID 
				INTO v_account_id 
            FROM T501_ORDER
           WHERE C501_order_ID = p_ref_id 
		   AND C501_Void_fl is NULL;
		   EXCEPTION WHEN NO_DATA_FOUND THEN
		      v_account_id := NULL;
		END;
           
           p_account_id := v_account_id;
        END IF;          
       gm_pkg_op_order_txn.gm_save_record_tag(v_tag_append,p_set_id,p_ref_id,p_userid,null,null,null); 
    END LOOP;
  END gm_save_order_tag_transaction;
 
  /**************************************************************************************************
    * Description : Procedure to Fetch the  the added Order tag datas
    * Author :N RAJA
   **************************************************************************************************/
  
  PROCEDURE Gm_fetch_tag_orders  (
       p_ref_id        IN   t501d_order_tag_usage_details.C501_ORDER_ID%TYPE,
       p_set_data      OUT   TYPES.cursor_type
      ) 
      AS
      v_company_id    t1900_company.c1900_company_id%TYPE;
      BEGIN
         
          SELECT get_compid_frm_cntx() 
            INTO v_company_id 
            FROM dual;
            
         OPEN p_set_data
           FOR

             SELECT t501d.C501D_ORDER_TAG_USAGE_DETAILS_ID TAGDOID,
  					t501d.C501_ORDER_ID STRTAGREFID,
  					NVL(T501d.C5010_Tag_Id,'N/A') TAGID,
  					DECODE(t501d.C5010_TAG_ID,NULL,'Y','N') MISSING,
					NVL(T5010.C207_Set_Id,T501d.C207_Set_Id) SETID,
  					get_set_name(NVL(t5010.c207_set_id,t501d.c207_set_id)) SETDESC,
  					t501d.C501D_VALID_FL TAGFL
			   FROM T501d_Order_Tag_Usage_Details T501d, T5010_Tag T5010
			  WHERE t501d.C501_ORDER_ID  = p_ref_id
				AND T501d.C5010_Tag_Id   = T5010.C5010_Tag_Id(+)
				AND T501d.C501d_Void_Fl  IS NULL
				AND T5010.C5010_Void_Fl  IS NULL
				AND T5010.C1900_Company_Id(+) = v_company_id;
              
  END Gm_fetch_tag_orders;
  
  
  /**************************************************************************************************
    * Description : Procedure to update the void flag of the Order tag transaction while removing from screen
    * Author : VINOTH
     **************************************************************************************************/
    PROCEDURE gm_cancel_order_tag_transaction(    	
        p_tag_do_id    IN     t501d_order_tag_usage_details.C501D_ORDER_TAG_USAGE_DETAILS_ID%TYPE,
        p_userid       IN     t501d_order_tag_usage_details.C501D_LAST_UPDATED_BY%TYPE
    )
    AS
    BEGIN
             IF p_tag_do_id IS NOT NULL THEN
             
            UPDATE T501D_ORDER_TAG_USAGE_DETAILS
              SET C501D_VOID_FL = 'Y',
                  C501D_LAST_UPDATED_BY = p_userid,
                  C501D_LAST_UPDATED_DATE = SYSDATE
              WHERE C501D_ORDER_TAG_USAGE_DETAILS_ID = p_tag_do_id 
              AND C501D_VOID_FL IS NULL;
               
    END IF;      
    END gm_cancel_order_tag_transaction;
    
/************************************************************************************
   * Description : Procedure to validate the tag and set id
   * Author : Bala
*************************************************************************************/     
   PROCEDURE gm_validate_tag(	
		p_tag_id            IN         T501D_ORDER_TAG_USAGE_DETAILS.C5010_TAG_ID%TYPE,
		p_set_id            IN         T501D_ORDER_TAG_USAGE_DETAILS.C207_SET_ID%TYPE,		
		p_ref_id            IN 		   T501D_ORDER_TAG_USAGE_DETAILS.C501_ORDER_ID%TYPE
	)
	AS
		v_ref_count number := 0;    
		v_tag_count number := 0;
   		v_set_count number := 0;
	BEGIN
		
       SELECT count(1) 
         INTO v_ref_count 
         FROM T501D_ORDER_TAG_USAGE_DETAILS T501D
        WHERE C501_ORDER_ID = p_ref_id 
          AND (C5010_TAG_ID = NVL(p_tag_id,'-9999') 
           OR C207_SET_ID = NVL(p_set_id,'-9999'))
          AND C501D_VOID_FL IS NULL;
          
	   IF(v_ref_count > 0) THEN
         raise_application_error (-20957, '');      
       END IF;      
      
	END gm_validate_tag;
	
	/******************************************************************************************************
	* Description : Procedure to Update as Y for valid tag while placing order
	* Author	  : Vinoth
	*******************************************************************************************************/
 	
 	  PROCEDURE gm_upd_DO_record_tags (
  		p_order_id       IN    T501D_ORDER_TAG_USAGE_DETAILS.C501_ORDER_ID%TYPE,
	    p_userid         IN    t501_order.C501_Created_By%TYPE
	)	
	AS

	BEGIN

        UPDATE T501D_ORDER_TAG_USAGE_DETAILS
           SET C501D_TRANSACTION_FL = 'Y',
               C501D_LAST_UPDATED_BY = p_userid,
               C501D_LAST_UPDATED_DATE = SYSDATE
         WHERE C501_ORDER_ID = p_order_id
           AND C501D_VOID_FL IS NULL;
           	
	END gm_upd_DO_record_tags;
	
		/******************************************************************************************************
	* Description : Procedure to Update Order ID when DOID removed and placing order
	* Author	  : Vinoth
	*******************************************************************************************************/
 	
 	  PROCEDURE gm_upd_OrderId_record_tags (
  		p_order_id       IN    T501D_ORDER_TAG_USAGE_DETAILS.C501_ORDER_ID%TYPE,
	    p_userid         IN    t501_order.C501_Created_By%TYPE,
	    p_temp_ordid     IN    T501D_ORDER_TAG_USAGE_DETAILS.C501_ORDER_ID%TYPE
	)	
	AS

	BEGIN
	
        UPDATE T501D_ORDER_TAG_USAGE_DETAILS
           SET C501_ORDER_ID = p_order_id,
           C501D_TRANSACTION_FL = 'Y',
               C501D_LAST_UPDATED_BY = p_userid,
               C501D_LAST_UPDATED_DATE = SYSDATE
         WHERE C501_ORDER_ID = p_temp_ordid
           AND C501D_VOID_FL IS NULL;
           
	END gm_upd_OrderId_record_tags;

	/******************************************************************************************************
	* Description : Procedure to Update as Y for valid tag while placing order
	* Author	  : Vinoth
	*******************************************************************************************************/
 	
 	  PROCEDURE gm_get_order_id (
  		p_order_id       OUT    VARCHAR
	)	
	AS
	v_tempid  T501D_ORDER_TAG_USAGE_DETAILS.C501_ORDER_ID%TYPE;
	BEGIN

        SELECT 'GMTEMP'||S501D_TEMP_ORDERID.NEXTVAL INTO v_tempid
          FROM DUAL;
          
		p_order_id  := v_tempid;
	END gm_get_order_id;		
	/******************************************************************************************************
	* Description : Procedure to fetch DO tag details based on the order id
	* Author	  : ppandiyan
	*******************************************************************************************************/	
		  
  PROCEDURE gm_fetch_DO_tag_dtls  (
       p_ref_id        IN   t501d_order_tag_usage_details.C501_ORDER_ID%TYPE,
       p_do_record_tag_dtls      OUT   CLOB
      ) 
      AS
      v_company_id    t1900_company.c1900_company_id%TYPE;
      BEGIN
         
          SELECT get_compid_frm_cntx() 
            INTO v_company_id 
            FROM dual;


				SELECT JSON_ARRAYAGG( 
			  		  JSON_OBJECT(
			  		  'orderId' value t501d.C501_ORDER_ID,
			  		  'tagId' value NVL(T501d.C5010_Tag_Id,'MISSING'), 
			  		  'setId' value NVL(T5010.C207_Set_Id,T501d.C207_Set_Id) , 
			  		  'setNm' value get_set_name(NVL(t5010.c207_set_id,t501d.c207_set_id)),
			  		  'latitude' value T501d.c501d_tag_latitude,
                      'longitude' value T501d.c501d_tag_longtitude)
			  		  
			  ORDER BY T501d.C5010_Tag_Id ASC RETURNING CLOB)
			  INTO p_do_record_tag_dtls
			   FROM T501d_Order_Tag_Usage_Details T501d, T5010_Tag T5010 
			  WHERE t501d.C501_ORDER_ID  = p_ref_id
				AND T501d.C5010_Tag_Id   = T5010.C5010_Tag_Id(+)
				AND T501d.C501d_Void_Fl  IS NULL
				AND T5010.C5010_Void_Fl  IS NULL
				AND T5010.C1900_Company_Id(+) = v_company_id;
              
  END gm_fetch_DO_tag_dtls;
  END gm_pkg_order_tag_record;
  /
 