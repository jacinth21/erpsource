/* Formatted on 06/04/2020 10:18 (Formatter Plus v4.8.0) */
/* @ "C:\database\Packages\CustomerServices\gm_pkg_set_dispute.bdy" */ 
CREATE OR REPLACE
PACKAGE body gm_pkg_set_dispute
IS
/*******************************************************
* Author  : ppandiyan
* Description: This Procedure is used to save
* the Loaner set as Disputed 
*******************************************************/
PROCEDURE gm_sav_dispute_loaner_set (
	p_consign_id IN t504a_consignment_loaner.c504_consignment_id%TYPE ,
	p_userid  	 IN t101_user.c101_user_id%TYPE
)
AS
--Get Tag Id Based on Consignment by Last Updated Trans ID
   CURSOR tag_id_cur
      IS
                SELECT c5010_tag_id tag_id 
                FROM t5010_tag 
                WHERE c5010_last_updated_trans_id = p_consign_id 
                AND c5010_void_fl IS NULL;
BEGIN
	--update consignment loaner status as Disputed
		gm_pkg_op_loaner_set_txn.gm_upd_consign_loaner_status (p_consign_id, 21, p_userid) ;--21--Disputed
		
	--update missing flag as Disputed	
	  UPDATE t504a_consignment_loaner
	  SET c504a_missed_fl       = 'D', -- D- Disputed ,  Y- Missed  //status for loaner
	    c504a_last_updated_date = CURRENT_DATE,
	    c504a_last_updated_by   = p_userid
	  WHERE c504_consignment_id = p_consign_id
	  AND c504a_void_fl        IS NULL;
	  
	  --PMT-50858-Flagging Loaner Sets as Disputed
	   FOR tag_id_upd IN tag_id_cur
	    LOOP   
	    	--Call gm_save_tag_status procedure to Update the Status as Disputed	
	         gm_pkg_set_dispute.gm_sav_tag_status(tag_id_upd.tag_id,CURRENT_DATE,p_consign_id,p_userid,26240842);       
	    END LOOP;
 END gm_sav_dispute_loaner_set;
 
/*******************************************************
* Author  : ppandiyan
* Description: This Procedure is used to update
* the tag status
*******************************************************/

 PROCEDURE gm_sav_tag_status (
	 p_tag_id            IN t5010_tag.c5010_tag_id%TYPE
	,p_tag_missing_since IN t5010_tag.c5010_missing_since%TYPE
	,p_ref_id  			 IN t5010_tag.c5010_last_updated_trans_id%TYPE
	,p_userid 			 IN t101_user.c101_user_id%TYPE
	,p_status      		 IN t5010_tag.c901_status%TYPE
)
AS
BEGIN
	-- update the tag status
		UPDATE t5010_tag 
        SET c5010_missing_since = p_tag_missing_since
           ,c901_status = p_status
           ,c5010_last_updated_by = p_userid
           ,c5010_last_updated_date = current_date
           ,c5010_last_updated_trans_id = p_ref_id 
        WHERE c5010_tag_id = p_tag_id 
        AND c5010_void_fl is null;
		
 END gm_sav_tag_status;
 
 /*******************************************************
* Author  : ppandiyan
* Description: This Procedure is used to fetch loaner
* disputed detail
*******************************************************/
 PROCEDURE gm_fch_loaner_disputed_dtl (
    p_consign_id         IN    t504a_consignment_loaner.c504_consignment_id%TYPE
   ,p_cancel_type        IN    t901_code_lookup.c902_code_nm_alt%TYPE
   ,p_out                OUT   TYPES.cursor_type
 )
 AS
    v_comp_dt_fmt 	 VARCHAR2(20);
 BEGIN
	 --getting company date format
      SELECT get_compdtfmt_frm_cntx() INTO v_comp_dt_fmt FROM DUAL;
      
   
   OPEN p_out
      FOR

		 SELECT  t504a.c504_consignment_id consignid, 
		         DECODE (p_cancel_type, 'LNDSPT', 'Pending Return', '') fromStatus,
		         t907.c907_comments comments, 
		         get_code_name(t907.c901_cancel_cd) reason,  
		         TO_CHAR (t504a.c504a_last_updated_date,v_comp_dt_fmt)  updateddate,
		         get_user_name(t504a.c504a_last_updated_by) updatedby
		 	 FROM  
		         t504a_consignment_loaner t504a,  
		         t907_cancel_log t907
			WHERE t504a.c504_consignment_id = p_consign_id
			AND t504a.c504_consignment_id = t907.c907_ref_id  
			AND t907.c901_type in (SELECT c901_code_id
								      FROM t901_code_lookup
								   	WHERE c902_code_nm_alt = p_cancel_type)
			AND t504a.c504a_void_fl  IS NULL
			AND t907.c907_void_fl IS NULL ;

END gm_fch_loaner_disputed_dtl;

 /*******************************************************
* Author     : ppandiyan
* Description: This Procedure is used to fetch loaner
* disputed detail for accept return
*******************************************************/
 
PROCEDURE gm_fch_ln_disputed_trans_email_dtl (
      p_conids       IN   VARCHAR2,
      p_action       IN   VARCHAR2,
      p_prod_req_id  IN   t504a_loaner_transaction.c504a_loaner_transaction_id%type default null,
      p_out          OUT  TYPES.cursor_type
)
AS
   	v_comp_dt_fmt VARCHAR2(20);
   	
BEGIN

	  --setting consignment ids in context
      my_context.set_my_inlist_ctx (p_conids);
	  --getting company date format
      SELECT get_compdtfmt_frm_cntx() INTO v_comp_dt_fmt FROM DUAL;
 
	 IF p_action='RETURN' THEN 
	
			OPEN p_out
			FOR
	          SELECT t526.c207_set_id setid,
	                 get_set_name (t526.c207_set_id) setnm,
	                 t504acl.c504a_etch_id etchid,
	                 TO_CHAR (t504atxn.c504a_expected_return_dt,v_comp_dt_fmt) duedate,
	                    DECODE(t504atxn.c901_consigned_to,50170,get_distributor_name (t504atxn.c504a_consigned_to_id),
	                    50171,get_account_name (t504atxn.c504a_consigned_to_id),
	                    50172,get_user_name (t504atxn.c504a_consigned_to_id)) frmrepnm
	            FROM t526_product_request_detail t526,
	                 t504a_consignment_loaner t504acl,
	                 t504a_loaner_transaction t504atxn
	           WHERE t504atxn.c504a_loaner_transaction_id = p_prod_req_id
	             AND t526.c526_product_request_detail_id = t504atxn.c526_product_request_detail_id
	             AND t504acl.c504_consignment_id IN (SELECT token FROM v_in_list)
	             AND t504atxn.c504_consignment_id = t504acl.c504_consignment_id
	             AND t504acl.c504a_missed_fl = 'D'
	             AND t504acl.c504a_void_fl IS NULL
	             AND t504atxn.c504a_void_fl IS NULL
	             AND t526.c526_void_fl IS NULL;
	            
	            
	END IF;

END gm_fch_ln_disputed_trans_email_dtl;

END gm_pkg_set_dispute;
/