/* Formatted on 2011/12/22 17:09 (Formatter Plus v4.8.0) */
--@"C:\Database\Packages\CustomerServices\gm_pkg_op_do_process_report.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_do_process_report
IS
   PROCEDURE gm_fetch_part_details (
      p_order_id   IN       t501_order.c501_order_id%TYPE,
      p_part_cur   OUT      TYPES.cursor_type
   )
   AS
   BEGIN
      OPEN p_part_cur
       FOR
          SELECT *
            FROM (SELECT   t502.c205_part_number_id pnum,
                           t502.c502_item_price price,
                           t502a.c502a_attribute_value attr,
                           t502.c901_type otype,
                           SUM (t502.c502_item_qty) qty,
                           (SUM (t502.c502_item_qty) * t502.c502_item_price
                           ) ext_price,
                           get_partnum_desc (t502.c205_part_number_id) pdesc , get_rule_value(t502.C205_PART_NUMBER_ID,'NONUSAGE') UFREE
                      FROM t501_order t501,
                           t502_item_order t502,
                           t502a_item_order_attribute t502a
                     WHERE t501.c501_order_id = t502.c501_order_id
                       AND t502.c502_attribute_id = t502a.c502_attribute_id
                       AND t501.c501_order_id = p_order_id
                       AND t502a.c901_attribute_type = 100820         -- usage
                       AND t501.c501_delete_fl IS NULL
                       AND t501.c501_void_fl IS NULL
                       AND t502.c502_void_fl IS NULL
                       AND t502a.c502a_void_fl IS NULL
                  GROUP BY t502.c205_part_number_id,
                           t502.c502_item_price,
                           t502a.c502a_attribute_value,t502.c901_type
                  ORDER BY pnum, pdesc) t502;
   END gm_fetch_part_details;

   FUNCTION get_order_status_desc (p_order_id IN t501_order.c501_order_id%TYPE,
   p_status IN VARCHAR2 DEFAULT NULL )
      RETURN VARCHAR2
   IS
      v_status   VARCHAR2 (200);
      v_order_source		VARCHAR2 (20);
   BEGIN
      BEGIN
	     
         SELECT DECODE (t501.c501_status_fl,
         				0, DECODE(p_status,NULL,'Back Order','Submitted'),
         				1, DECODE(p_status,NULL,'Processing','Submitted'),
         				2, DECODE(p_status,NULL,DECODE(t501.C501_SHIPPING_DATE,NULL,'Controlled','Shipped'),'Submitted'),
         				2.30, 'Packing In Progress',
				        2.60, 'Ready For Pickup',
         				3, 'Shipped',
         				4, 'Shipped',
                        7, 'Pending Release',
                        8, 'Pending CS Confirmation'
                       )
           INTO v_status
           FROM t501_order t501
          WHERE t501.c501_order_id = p_order_id;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            v_status := '';
      END;

      RETURN v_status;
   END get_order_status_desc;

   PROCEDURE gm_fetch_case_parts (
      p_caseinfoid   IN       t501_order.c7100_case_info_id%TYPE,
      p_part_cur     OUT      TYPES.cursor_type
   )
   AS
   BEGIN
      OPEN p_part_cur
       FOR
          SELECT   t505.c205_part_number_id pnum,
                   get_partnum_desc (t505.c205_part_number_id) pdesc,
                   get_account_part_pricing
                             (t7100.c704_account_id,
                              t505.c205_part_number_id,
                              get_account_gpo_id (t7100.c704_account_id)
                             ) price
              FROM t7104_case_set_information t7104,
                   t5010_tag t5010,
                   t505_item_consignment t505,
                   t7100_case_information t7100
             WHERE t7104.c7100_case_info_id = p_caseinfoid
               AND t7104.c5010_tag_id = t5010.c5010_tag_id
               AND t5010.c5010_last_updated_trans_id =
                                                      t505.c504_consignment_id
               AND t7100.c7100_case_info_id = t7104.c7100_case_info_id
               AND t7100.c901_type    = 1006503 -- CASE 
               AND t7104.c5010_tag_id IS NOT NULL
               AND t7104.c7104_shipped_del_fl IS NULL
               AND t7104.c7104_void_fl IS NULL
               AND t5010.c5010_void_fl IS NULL
               AND t505.c505_void_fl IS NULL
               AND t7100.c7100_void_fl IS NULL
          GROUP BY t505.c205_part_number_id, t7100.c704_account_id;
   END gm_fetch_case_parts;

   PROCEDURE gm_validate_newdo (
      p_case_infoid   IN       t501_order.c7100_case_info_id%TYPE,
      p_parent_id     OUT      t501_order.c501_order_id%TYPE,
      p_order_id      OUT      t501_order.c501_order_id%TYPE
   )
   AS
      v_order_id        t501_order.c501_order_id%TYPE;
      v_parent_ord_id   t501_order.c501_order_id%TYPE;
      v_not_shipped     NUMBER;
      v_shipped         NUMBER;
   BEGIN
    SELECT count(1) INTO v_not_shipped
      FROM t501_order 
     WHERE c7100_case_info_id =p_case_infoid
       AND c501_shipping_date IS NULL
       AND c501_void_fl IS NULL;
       
   SELECT count(1) INTO v_shipped
     FROM t501_order 
    WHERE c7100_case_info_id =p_case_infoid
      AND c501_shipping_date IS NOT NULL
      AND c501_void_fl IS NULL;

      IF v_not_shipped > 0
      THEN
      	GM_RAISE_APPLICATION_ERROR('-20999','29','');
    ELSIF  (v_not_shipped = 0 AND v_shipped > 0 )
      THEN
       SELECT c501_order_id INTO v_parent_ord_id
         FROM t501_order
        WHERE c7100_case_info_id = p_case_infoid
          AND c501_parent_order_id IS NULL;
          
         p_parent_id := v_parent_ord_id;
         
         SELECT 'GM' || s501_order.NEXTVAL INTO v_order_id FROM DUAL;
         p_order_id  := v_order_id;
      ELSE 
         p_parent_id := '';
         p_order_id  := '';
      END IF;
   END gm_validate_newdo;

   PROCEDURE gm_fch_part_string (
      p_order_id   IN       t501_order.c501_order_id%TYPE,
      p_str        OUT      VARCHAR2
   )
   AS
/*
   String format

   v_pnum^v_qty^v_control^v_partordtyp^v_refid^v_capfl^v_price

   124.000,2,,50300,,,223.00,|
*/
      CURSOR v_cur
      IS
         SELECT t502.c205_part_number_id pnum, t502.c502_item_qty qty,
                c901_type TYPE, c502_item_price price
           FROM t502_item_order t502
          WHERE c501_order_id = p_order_id
            AND t502.c502_delete_fl IS NULL
            AND t502.c502_void_fl IS NULL;
   BEGIN
      FOR v_index IN v_cur
      LOOP
         p_str :=
               p_str
            || v_index.pnum
            || ','
            || v_index.qty
            || ',,'
            || v_index.TYPE
            || ',,,'
            || v_index.price
            || ',|';        
      END LOOP;
   END gm_fch_part_string;
   
   PROCEDURE gm_fch_case_tags (
      p_case_infoid   IN       VARCHAR2,
      p_tagid_cur     OUT      TYPES.cursor_type
   )
   AS
   BEGIN
      OPEN p_tagid_cur
       FOR
          SELECT t7104.c5010_tag_id ID, t7104.c5010_tag_id NAME
            FROM t7104_case_set_information t7104, t5010_tag t5010
           WHERE to_char(t7104.c7100_case_info_id) = p_case_infoid
             AND t7104.c5010_tag_id IS NOT NULL
             AND t7104.c5010_tag_id = t5010.c5010_tag_id
             AND t5010.c5010_void_fl IS NULL
             AND t7104.c7104_void_fl IS NULL
             AND t7104.c7104_shipped_del_fl IS NULL
          UNION
          SELECT t5012.c5010_tag_id ID, t5012.c5010_tag_id NAME
            FROM t5012_tag_usage t5012
           WHERE t5012.c901_ref_type = 2570
             AND t5012.c5012_ref_id = p_case_infoid
             AND t5012.c5012_void_fl IS NULL;
   END gm_fch_case_tags;
   
    /*******************************************************
    * Description : fucntion to get the status of order change price flag
    * Parameters  : 1.Order ID 
    * Author     : Aprasath
    *******************************************************/
   FUNCTION get_order_change_price_status (
      p_order_id          IN   t502a_item_order_attribute.c501_order_id%TYPE
   )RETURN VARCHAR2
   IS  
    	v_change_price_fl   VARCHAR2(1);
        v_cut_off_time  	VARCHAR2(10);
   		v_country_code   VARCHAR2(10);
   		v_cut_off_date   VARCHAR2(20);
		v_date_format   VARCHAR2(20);
   	  BEGIN   	
	   	  -- Get the coutry code from rule and get the cut of time for each country
	   	  v_country_code := NVL(get_rule_value('CURRENT_DB','COUNTRYCODE'),'en');	   	  
	   	  v_cut_off_time := NVL(get_rule_value(v_country_code,'HOLD_CUT_OFF_TIME'),'23:59'); 
		  v_cut_off_date := get_rule_value(v_country_code,'HOLD_CUT_OFF_DATE');
		  v_date_format  := NVL(get_rule_value('DATEFMT','DATEFORMAT'),'MM/DD/YYYY');
	   	  --validating order date belongs to current month, if its current month and verify the current time should be less than cut off time to modify price
	   	   SELECT
			    CASE
			        WHEN TO_CHAR (SYSDATE, 'MM/YYYY') = TO_CHAR (C501_ORDER_DATE, 'MM/YYYY')
			        THEN (
			            CASE
			                WHEN SYSDATE < To_Date (NVL(v_cut_off_date,TO_CHAR (LAST_DAY(SYSDATE), v_date_format)) ||v_cut_off_time, v_date_format||' hh24:mi')
			                THEN 'Y'
			                ELSE 'N'
			            END)
			        ELSE 'N'
			    END INTO v_change_price_fl 
			   FROM T501_ORDER WHERE C501_ORDER_ID = p_order_id AND C501_VOID_FL IS NULL;
	  RETURN v_change_price_fl;
	END get_order_change_price_status;
	/*******************************************************
    * Description : Procedure to get the latest DO Id
    * Author     : Anilkumar
    *******************************************************/
	PROCEDURE gm_fch_latest_do (
      p_order_dt   	IN      VARCHAR2,   
      p_prefix_doid IN      VARCHAR2, 
      p_latest_do	OUT     T501_ORDER.C501_ORDER_ID%TYPE 
   )
   AS
   v_latest_do   T501_ORDER.C501_ORDER_ID%TYPE;
   v_order_dt	T501_ORDER.C501_ORDER_DATE%TYPE;
   BEGIN
	   
	 v_order_dt := TO_DATE(p_order_dt,GET_RULE_VALUE('DATEFMT','DATEFORMAT'));
	 
	  SELECT ORDID
	  INTO p_latest_do
	  FROM
	    (SELECT C501_ORDER_ID ORDID
	    FROM T501_ORDER
	    WHERE C501_ORDER_DATE = v_order_dt
	    AND C501_ORDER_ID LIKE p_prefix_doid||'%' 
	    AND C501_PARENT_ORDER_ID IS NULL
	    AND C901_EXT_COUNTRY_ID IS NULL
	    ORDER BY C501_CREATED_DATE DESC
	    )
	  WHERE ROWNUM=1;
        
   EXCEPTION
	WHEN OTHERS THEN
	  p_latest_do := NULL;
	
   END gm_fch_latest_do;
   
   /***********************************************************************************************************
    * Description : Procedure to get the part string from t502_item_order table for the lot track enable parts
    * Author      : karthik
    **********************************************************************************************************/
	   
   PROCEDURE gm_fch_DO_part_string (
      p_order_id   IN       t501_order.c501_order_id%TYPE,
      p_str        OUT      VARCHAR2
   )
   AS
/*
   String format
   v_pnum^v_qty^v_lot
   ,124.000,2,THBXXXXXX,|
*/
      CURSOR v_cur
      IS
      /* Get the partnumber from T502 table
       * Check the part number whether lot track is enabled for partnum by using the get_part_attr_value_by_comp() 
       * */
         SELECT t502.c205_part_number_id pnum,
         		t502.c502_item_qty qty,
                t502.C502_CONTROL_NUMBER lot
           	FROM t502_item_order t502 ,
           		T501_order t501
          WHERE t501.c501_order_id = p_order_id
          	AND t501.c501_order_id = t502.c501_order_id 
            AND T502.C502_DELETE_FL IS NULL
            AND t502.c502_void_fl IS NULL
            AND get_part_attr_value_by_comp(t502.c205_part_number_id,'104480',t501.c1900_company_id) = 'Y';
            
   BEGIN
	
	   FOR v_index IN v_cur
      LOOP
         p_str :=
               p_str
            || ','
            || v_index.pnum
            || ','
            || v_index.qty
            || ','
            || v_index.lot
            || ',|';        
      END LOOP;
   END gm_fch_DO_part_string;
   
END gm_pkg_op_do_process_report;
/
