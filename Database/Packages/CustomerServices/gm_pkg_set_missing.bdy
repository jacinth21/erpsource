/* Formatted on 2012/03/20 16:51 (Formatter Plus v4.8.0) */
--@"c:\database\packages\CustomerServices\gm_pkg_set_missing.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_set_missing
IS
--/**************************************************************************************************
--    * Description : Procedure to save set missing details
--    * Author : Bala
-- **************************************************************************************************/

PROCEDURE gm_save_set_missing (
      p_tag_id             IN t5010_tag.c5010_tag_id%TYPE
     ,p_tag_missing_since  IN t5010_tag.c5010_missing_since%TYPE
     ,p_userid             IN t101_user.c101_user_id%TYPE
)
AS

v_tag_id 		t5010_tag.c5010_tag_id%TYPE;
v_set_id 		t5010_tag.c207_set_id%TYPE;
v_from_did      t5010_tag.c5010_location_id%TYPE;
v_tag_part      t5010_tag.c205_part_number_id%TYPE;
v_transfer_out  varchar2(4000) := '';

BEGIN
	    
   	   BEGIN
		   	   SELECT c5010_tag_id,c207_set_id,c205_part_number_id,c5010_location_id
		   	     INTO v_tag_id,v_set_id,v_tag_part,v_from_did
				 FROM t5010_tag t5010,t701_distributor t701
				WHERE c5010_location_id = c701_distributor_id
				  AND c5010_tag_id       = p_tag_id
				  AND C901_inventory_type IN (10003)  -- Consignment inventory.
				  AND c901_location_type = '4120' -- Distributor
		          AND c5010_void_fl is null
		          AND c701_void_fl is null;
		     --   FOR UPDATE;		       
	   EXCEPTION 
	   WHEN NO_DATA_FOUND THEN
	   	v_tag_id := null;
	   END;
		
		IF v_tag_id is null THEN
		 raise_application_error (- 20001, 'The tag is not in consignment inventory...') ;
		END IF;    
 
		-- Create inpit string for set  and part missing for the tag
		gm_set_missing_input(p_tag_id,v_tag_part,v_set_id,v_from_did,p_userid,p_tag_missing_since,v_transfer_out);
		
		gm_save_missing_tag(p_tag_id,p_tag_missing_since,v_transfer_out,p_userid);
						       
    --	
END gm_save_set_missing;

--/*************************************************************************************
--   *  Description: Procedure to create input string for save set missing details  
--   *  Author: Bala
--*************************************************************************************/
   PROCEDURE gm_set_missing_input (
      p_tag_id             IN t5010_tag.c5010_tag_id%TYPE     
     ,p_tag_part           IN t5010_tag.c205_part_number_id%TYPE
     ,p_set_id			   IN t5010_tag.c207_set_id%TYPE
     ,p_from_did		   IN t5010_tag.c5010_location_id%TYPE
     ,p_userid             IN t101_user.c101_user_id%TYPE
     ,p_tag_missing_since  IN t5010_tag.c5010_missing_since%TYPE
     ,p_transferid	       OUT VARCHAR2
   )
   
   AS
   v_inputstr     varchar2(4000) := '';   
   v_transfer_out varchar2(4000) := '';
   v_log_out      varchar2(40) := ''; 
   v_trans_mode   varchar2(40) := '90351'; -- Set Transfer  
    -- Cursor to fetch set details
    CURSOR setpartfetch (p_set_id t5010_tag.c207_set_id%TYPE,p_from_did t5010_tag.c5010_location_id%TYPE) 
    IS 
	    	 SELECT   c205_part_number_id pnum, get_partnum_desc (c205_part_number_id) partdesc
						, t208.c208_set_qty setqty, get_field_sales_net_con_qty (3, p_from_did, c205_part_number_id, 4000338) qty
						, NVL (DECODE (t208.c208_set_qty
									 , 0, 0
									 , DECODE (SIGN (t208.c208_set_qty - get_field_sales_net_con_qty (3, p_from_did, c205_part_number_id, 4000338))
											 , -1, t208.c208_set_qty
											 , get_field_sales_net_con_qty (3, p_from_did, c205_part_number_id, 4000338)
											  )
									  )
							 , 0
							  ) tsfqty
						, '1' chkbox
			  FROM t208_set_details t208
			 WHERE t208.c207_set_id = p_set_id
			   AND t208.c208_void_fl IS NULL
			 ORDER BY t208.c205_part_number_id;     

BEGIN   
	
	IF (p_set_id IS NOT NULL) THEN
	
		    FOR setpartdtl IN setpartfetch(p_set_id,p_from_did) 
   			LOOP 
		      		       
		      IF (setpartdtl.tsfqty != 0) THEN
		       v_inputstr := v_inputstr || setpartdtl.pnum || '^' || setpartdtl.tsfqty || '|';       
		      END IF; 
		      
    	    END LOOP; 
	    			
	ELSE
			v_trans_mode := '90352'; -- Part transfer
			v_inputstr := p_tag_part || '^' || 1 || '|'; 
	          
	END IF;
	
	gm_pkg_set_missing.gm_set_save_transfer(p_tag_id,p_tag_part,p_set_id,p_from_did,v_inputstr,v_trans_mode,p_userid,p_tag_missing_since,p_transferid);
   
END gm_set_missing_input;
--/*************************************************************************************
--   *  Description: Procedure to create input string for save set missing details  
--   *  Author: Bala
--*************************************************************************************/
   PROCEDURE gm_set_save_transfer (
      p_tag_id             IN t5010_tag.c5010_tag_id%TYPE     
     ,p_tag_part           IN t5010_tag.c205_part_number_id%TYPE
     ,p_set_id			   IN t5010_tag.c207_set_id%TYPE
     ,p_from_did		   IN t5010_tag.c5010_location_id%TYPE
     ,p_input_str		   IN VARCHAR 
     ,p_tsfmode		       IN t920_transfer.c901_transfer_mode%TYPE
     ,p_userid             IN t101_user.c101_user_id%TYPE 
     ,p_tag_missing_since  IN t5010_tag.c5010_missing_since%TYPE
     ,p_transferid	       OUT VARCHAR2
   )
   AS
   
   v_transfer_out   varchar2(4000) := '';
   v_log_out 		varchar2(40) := '';
   v_dateformat 	varchar2(100);
      
BEGIN   
	
  -- to get the date format
     SELECT get_compdtfmt_frm_cntx()
       INTO v_dateformat
       FROM DUAL;
	     
	        gm_pkg_cs_transfer.gm_cs_sav_transfer(26240615,p_tsfmode,p_from_did,get_rule_value('DISTRIBUTOR','TAGST'),26240617,to_char(current_date,v_dateformat),p_userid,p_set_id,p_input_str,p_transferid);  
        											--26240615 Distributor to Missing			--26240617   Reason as Missing
			
			gm_update_log_dtls(p_transferid,p_tag_id,p_userid); 
			
		    gm_pkg_set_missing.gm_set_complete_transfer(p_transferid,p_tag_id,p_tag_part,p_set_id,p_userid); 
 	
END gm_set_save_transfer;
--/*************************************************************************************
--   *  Description: Procedure to create input string for save set missing details  
--   *  Author: Bala
--*************************************************************************************/
   PROCEDURE gm_set_complete_transfer (
      p_trans_id           IN t920_transfer.c920_transfer_id%TYPE
     ,p_tag_id             IN t5010_tag.c5010_tag_id%TYPE
     ,p_tag_part           IN t5010_tag.c205_part_number_id%TYPE
     ,p_set_id			   IN t5010_tag.c207_set_id%TYPE
     ,p_userid             IN t101_user.c101_user_id%TYPE     
   )
   AS
      v_accept_out   varchar2(400) := '';
      v_tag_inputstr varchar2(4000) := null;
      v_log_out      varchar2(400) := '';
      
       	 -- Cursor to fetch tag details  
     CURSOR tagpartfetch(p_trans_id t5010_tag.c5010_tag_id%TYPE)
     IS
			 SELECT t507.c507_returns_item_id itemid, NVL (t5010.c5010_tag_id, '') tagid, c507_missing_fl missing_fl
				  , t507.c205_part_number_id pnum
				  , DECODE (DECODE (NULL, 90300, t506.c701_distributor_id, t506.c101_user_id)
						  , t5010.c5010_location_id, DECODE (NULL
														   , 90300, get_distributor_name (t506.c701_distributor_id)
														   , get_user_name (t506.c101_user_id)
															)
						  , DECODE (t5010.c901_location_type
								  , 4120, get_distributor_name (t5010.c5010_location_id)
								  , 4123, get_user_name (t5010.c5010_location_id)
								  , 40033, get_code_name (40033)
								   )
						   ) locid
				  , DECODE (DECODE (NULL, 90300, t506.c701_distributor_id, t506.c101_user_id)
						  , t5010.c5010_location_id, DECODE (NULL
														   , 90300, t506.c701_distributor_id
														   , t506.c101_user_id
															)
						  , DECODE (t5010.c901_location_type
								  , 4120, t5010.c5010_location_id
								  , 4123, t5010.c5010_location_id
								  , 40033, t5010.c5010_location_id
								   )
						   ) locidnum
				  , DECODE (t5010.c901_location_type
						  , 4120, get_distributor_name (t5010.c5010_location_id)
						  , 4123, get_user_name (t5010.c5010_location_id)
						  , 40033, get_code_name (t5010.c5010_location_id)
						   ) loctag   --get_code_name ()
				  , t5010.c5010_control_number cntlnum, t506.c506_rma_id refidrtn
				  , DECODE (t506.c207_set_id, t5010.c207_set_id, t506.c207_set_id, t5010.c207_set_id) setid
				  , t507.c507_item_qty qty
			   FROM t921_transfer_detail t921
				  , t506_returns t506
				  , t507_returns_item t507
				  , t5010_tag t5010
				  , t205d_part_attribute t205d
			  WHERE c920_transfer_id = p_trans_id
				AND t921.c921_ref_id = t506.c506_rma_id
				AND t506.c506_rma_id = t507.c506_rma_id
				AND t507.c5010_tag_id = t5010.c5010_tag_id(+)
				AND t507.c205_part_number_id = t205d.c205_part_number_id
				AND t507.c205_part_number_id = p_tag_part
				AND t205d.c901_attribute_type = 92340
				AND t205d.c205d_attribute_value = 'Y'
				AND t507.c507_item_qty > 0
				AND t205d.c205d_void_fl IS NULL;  
				
BEGIN   
		  -- Fetch tag details
	    FOR tagpartdtl in tagpartfetch(p_trans_id)
	    LOOP
	      		v_tag_inputstr := tagpartdtl.itemid || '^' || p_tag_id || '^' || '' || '^' || tagpartdtl.refidrtn || '|';    
	    END LOOP;
  
		gm_pkg_cs_transfer.gm_cs_sav_accept_transfer(p_trans_id,p_userid,'Y',v_tag_inputstr,54011,v_accept_out);
		gm_update_log(p_trans_id,'Transfer created from Tag <b>' || p_tag_id ||'</b> marked as Missing','26240618',p_userid,v_log_out);
	
END gm_set_complete_transfer;
--/*************************************************************************************
--   *  Description: Procedure to update tag details to show to report 
--   *  Author: Agilan
--*************************************************************************************/
   PROCEDURE gm_save_missing_tag (
      p_tag_id             IN t5010_tag.c5010_tag_id%TYPE
     ,p_tag_missing_since  IN t5010_tag.c5010_missing_since%TYPE
     ,p_ref_id             IN VARCHAR2
     ,p_userid             IN t101_user.c101_user_id%TYPE
   )
   AS
   	
   	BEGIN
	   		UPDATE t5010_tag 
		       SET c5010_missing_since = p_tag_missing_since
		  	 	   ,c5010_last_updated_trans_id = p_ref_id
		  	       ,c901_status = '26240614'    --Missing Status
		           ,c5010_location_id = get_rule_value('DISTRIBUTOR','TAGST')    --Missing Distributor
		           ,c901_trans_type = '51000'    --Consignments
		           ,c901_sub_location_type = ''
			       ,c5010_sub_location_id  = ''
			       ,c704_account_id = ''
		           ,c5010_last_updated_by = p_userid
		           ,c5010_last_updated_date = current_date
		     WHERE c5010_tag_id = p_tag_id 
		       AND c5010_void_fl is null;
		       
	END gm_save_missing_tag;
--/*************************************************************************************
--   *  Description: Procedure to update log deatils for TFCN and TRFA
--   *  Author: Agilan
--*************************************************************************************/	
PROCEDURE gm_update_log_dtls(
  p_trans_id           IN 	t920_transfer.c920_transfer_id%TYPE
 ,p_tag_id             IN 	t5010_tag.c5010_tag_id%TYPE
 ,p_userid             IN 	t101_user.c101_user_id%TYPE
)
AS
    v_log_out      varchar2(400) := '';
    
	CURSOR fetchrefId(p_trans_id t920_transfer.c920_transfer_id%TYPE)
	IS
		SELECT c921_ref_id refid
		  FROM t921_transfer_detail
		 WHERE c920_transfer_id = p_trans_id
		   AND c921_void_fl IS NULL; 
BEGIN
	 FOR fchrefId in fetchrefId(p_trans_id)
	    LOOP
	      	gm_update_log(fchrefId.refid,'Transfer created from Tag <b>' || p_tag_id ||'</b> marked as Missing','26240618',p_userid,v_log_out);
	      	
	      	UPDATE t506_returns 
	      	   SET c506_comments='Transfer created from Tag <b>' || p_tag_id ||'</b> marked as Missing' 
	      	 WHERE c506_rma_id=fchrefId.refid 
	      	   AND c506_void_fl is null;
	    END LOOP;
	
	END gm_update_log_dtls;
	
END gm_pkg_set_missing;
/
