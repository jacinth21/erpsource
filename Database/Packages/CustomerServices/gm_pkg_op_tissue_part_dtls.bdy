/* Formatted on 04/02/2020 10:18 (Formatter Plus v4.8.0) */
/* @ "C:\Database\Packages\CustomerServices\gm_pkg_op_tissue_part_dtls.bdy" */

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_tissue_part_dtls
IS

 /**************************************************************************************************
    * Description : Procedure to fetch Hospital address details of Sales Rep
    * Author      : T.S. Ramachandiran
   **************************************************************************************************/
   
   PROCEDURE get_tissue_shipout_address(      
      p_accountid    IN   t704_account.c704_account_id%TYPE,
      p_address      OUT  VARCHAR2,
      p_message      OUT  VARCHAR2
    )
 	AS	
 	v_ship_address 		VARCHAR2(4000);
 	v_hosp_ship_add		VARCHAR2(4000);
 	BEGIN
       	BEGIN
         			SELECT t704.c704_ship_name
                	|| '<br>'
                	|| DECODE (t704.c704_ship_add1,NULL,' ',t704.c704_ship_add1||'<br>')
                	|| DECODE (T704.C704_SHIP_ADD2,null,' ',T704.C704_SHIP_ADD2||'<br>')
                	|| DECODE (t704.c704_ship_city,NULL,' ',t704.c704_ship_city||'<br>')
                	|| GET_CODE_NAME(T704.C704_SHIP_STATE)
                	|| ' '
                	|| DECODE (t704.c704_ship_zip_code,NULL,'<br>',t704.c704_ship_zip_code||'<br>')     
                    || GET_CODE_NAME(t704.C704_SHIP_COUNTRY)
           INTO v_hosp_ship_add
           FROM t704_account t704
           where 
                  T704.C704_ACCOUNT_ID = p_accountid    
           AND t704.c704_void_fl IS NULL;
           
           			p_message   := 'Tissue parts are available. This is going to be shipped to below location:';
   					p_address   :=  v_hosp_ship_add;
        EXCEPTION
        		WHEN NO_DATA_FOUND
        		THEN
        			v_hosp_ship_add := NULL;
        			p_message   	:= NULL;
   					p_address   	:= v_hosp_ship_add;
        END;		
END get_tissue_shipout_address;


 /***********************************************************************************************************
    * Description : Procedure to find tissue part from orderid and fetch Hospital address details of Sales Rep
    * Author      : T.S. Ramachandiran
   ***********************************************************************************************************/
	PROCEDURE gm_fetch_do_tissue_shipout_address(
 		p_order_id   IN  t501_order.c501_order_id%TYPE,
 		p_address    OUT VARCHAR2,
 		p_message    OUT VARCHAR2
		)
   		AS
   			

   		v_account_id	T501_ORDER.C704_ACCOUNT_ID%TYPE;
   		v_address		VARCHAR2(4000);
   		v_message		VARCHAR2(4000);
   		v_count			NUMBER;
   		
   		
   		
   		CURSOR v_order_list
   			IS
      			SELECT t501.c501_order_id porderid
                      FROM t501_order t501
     			WHERE 
         			t501.c501_order_id = p_order_id
     			OR  t501.c501_parent_order_id = p_order_id
     			AND t501.c501_void_fl IS NULL
     			AND t501.c501_delete_fl IS NULL;
     			
     		
   	BEGIN
   	
		FOR var_order IN v_order_list
  			LOOP	
  			SELECT count(1) into v_count
   				FROM 
    				t501_order t501,
    				t502_item_order t502,
    				v205_parts_with_ext_vendor v205
    			WHERE 
             		t501.c501_order_id = var_order.porderid
    				AND t502.c501_order_id = t501.c501_order_id
    				AND t502.c205_part_number_id = v205.c205_part_number_id
    				AND t502.C502_VOID_FL IS NULL
    				AND t501.c501_void_fl IS NULL
     			    AND t501.c501_delete_fl IS NULL;
    				
 			IF (v_count > 0) THEN   --Order contains tissue parts
   				
 				SELECT c704_account_id
    				INTO v_account_id
				FROM  t501_order 
  				WHERE c501_order_id = var_order.porderid;
  			
  				--This below procedure is called by passing account id and salesrep id to fetch the address and the p_message
  				get_tissue_shipout_address(v_account_id, v_address, v_message);
   					p_address := v_address;
   					p_message := v_message;

				RETURN;  -- throw the validation and return the control if any of the order has tissue parts
     		ELSE
     			p_address := NULL;
         		p_message := NULL;     
     		END IF;
  		END LOOP;
	END gm_fetch_do_tissue_shipout_address;		
END gm_pkg_op_tissue_part_dtls;
/

