CREATE OR REPLACE
PACKAGE BODY gm_pkg_op_lot_error_rpt
IS

 /*********************************************************************************
* Description : This procedure is used to fetch used lot error details along with error type, transaction id, error message  for  PMT-29681
* Author     : Swetha
* Parameters  : Error Info ID
**********************************************************************************/
PROCEDURE gm_fch_lot_error_detail (
        p_error_info_id IN t5063_lot_error_info.C5063_LOT_ERROR_INFO_ID%TYPE,
        p_lot_error_details OUT TYPES.cursor_type)
AS
  v_company_id t5063_lot_error_info.c1900_company_id%TYPE;
  v_plant_id t5063_lot_error_info.c5040_plant_id%TYPE;
  v_date_fmt VARCHAR2 (20) ;
  
BEGIN

   SELECT get_compid_frm_cntx(), get_plantid_frm_cntx(), get_compdtfmt_frm_cntx () 
			INTO   v_company_id, v_plant_id, v_date_fmt
			FROM DUAL;
 
    OPEN p_lot_error_details 
      FOR 
        SELECT   t5063.c5063_lot_error_info_id errorid, t5063.c2550_control_number lotno, t5063.c205_part_number_id partno,
                 t205.c205_part_num_desc partdesc, get_code_name(t5063.c5063_trans_type) transtype, t5063.c901_error_type errtype, t5063.c5063_trans_id transid, get_code_name(t5063.c901_error_trans_type) errtranstype, TO_CHAR(t5063.c5063_trans_date, v_date_fmt) transdate,
                 get_distributor_name(t5063.c701_trans_distributor_id) transfieldsales, get_distributor_name(t5063.c701_consigned_distributor_id) consignedfieldsales,
                 t5063.c5063_reason errormessage, get_code_name(t5063.c901_status) errorstatus, get_user_name(t5063.c5063_resolved_by) resolvedby, TO_CHAR (t5063.c5060_resolved_date, v_date_fmt) resolveddate,
                 get_code_name(t5063.c901_resolution_type) resolntype, t5063.c5063_updated_control_number updatedlot, t5063.c5063_comments comments,t5063.c901_resolution_type resolid
				 FROM t5063_lot_error_info t5063, t205_part_number t205
				WHERE t5063.c205_part_number_id = t205.c205_part_number_id
				AND t5063.c5063_lot_error_info_id = p_error_info_id
				AND t5063.c1900_company_id = v_company_id
			    AND t5063.c5040_plant_id = v_plant_id
				AND t5063.c5063_void_fl IS NULL;
  END gm_fch_lot_error_detail;
  
END gm_pkg_op_lot_error_rpt;
/