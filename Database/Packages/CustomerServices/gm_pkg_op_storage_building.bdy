/*******************************************************
   * Description : Package body for splitting the order based on the parts available in the building for PMT-33510
   *		
   * Parameters  : Orderid
   *******************************************************/
CREATE OR REPLACE
PACKAGE BODY gm_pkg_op_storage_building
IS

/*************************************************************************************************************************************
	* Description : This procedure is used to validate the order type and the plant for the input order id for PMT-33510
	* Author     : Swetha
	* Parameters  : Orderid
****************************************************************************************************************************************/
PROCEDURE gm_validate_order_transaction(
	 p_order_id IN t501_order.c501_order_id%TYPE,
	 p_validate_order OUT VARCHAR2
	  )
	 
	AS
     v_plant_rule_val t906_rules.c906_rule_value%TYPE;
     v_order_rule_val t906_rules.c906_rule_value%TYPE;
     v_plant_id t501_order.c5040_plant_id%TYPE;
     v_order_type t501_order.c901_order_type%TYPE;
	 
	 
  BEGIN
 
  IF p_order_id = '' OR p_order_id IS NULL 
     THEN
      RETURN;
   END IF;
  
        
  --get the order type and plant rule value for the order id   
   BEGIN  
      SELECT NVL(t501.c901_order_type,2521), t906.c906_rule_value
       INTO v_order_type, v_plant_rule_val
      FROM t501_order t501, t906_rules t906
       WHERE t501.c501_order_id = p_order_id 
       AND t501.c501_void_fl IS NULL
       AND t501.c501_delete_fl IS NULL
       AND t906.c906_rule_id = t501.c5040_plant_id  -- Rule id will be plant id 
       AND t906.c1900_company_id IS NULL 
       AND t906.c906_void_fl IS NULL
       AND t906.c906_rule_grp_id = 'BUILDINGPLANTMAP';
   EXCEPTION
            WHEN NO_DATA_FOUND THEN
                v_plant_rule_val := NULL;
                v_order_type := NULL;
            END;
        
   -- validating the order type       
   IF v_plant_rule_val = 'Y' THEN
      SELECT get_rule_value (v_order_type, 'BUILDMAPORDERTYPE')
           INTO v_order_rule_val
          FROM dual;
      
    ELSE
      RETURN;
      
  END IF;
       
   
   IF v_order_rule_val = 'Y' THEN
      p_validate_order := 'YES';
   ELSE
        p_validate_order := NULL;
   END IF;
END gm_validate_order_transaction;

/***************************************************************************************************************************************
   * Purpose: This Function is used to fetch the building count for the parts in an order for PMT-33510
   * Author     : Swetha
   * Parameters  : Orderid
 *************************************************************************************************************************************/

	FUNCTION get_order_building_count (
		p_order_id IN t501_order.c501_order_id%TYPE
	)
		RETURN NUMBER
	IS
		v_building_count	   NUMBER;
	    v_plant_id 	  t5040_plant_master.c5040_plant_id%TYPE;
	
	BEGIN
	
	SELECT get_plantid_frm_cntx()
        INTO v_plant_id
        FROM DUAL;
        
          	
	BEGIN
		SELECT COUNT( DISTINCT (t5052.c5057_building_id))
		INTO v_building_count
      FROM t5053_location_part_mapping t5053, t5052_location_master t5052, t501_order t501, t502_item_order t502
     WHERE t5052.c5052_location_id= t5053.c5052_location_id
     AND t5052.c5040_plant_id   = v_plant_id
     AND t5052.c5052_void_fl IS NULL
     AND t5053.c5053_curr_qty  > 0
     AND t5052.c901_location_type =93320  -- Pick Face
     AND t5052.c5051_inv_warehouse_id = 1 -- FG Warehouse
     AND t5053.c205_part_number_id = t502.c205_part_number_id
     AND t501.C501_ORDER_ID = t502.C501_ORDER_ID
     AND t502.C501_ORDER_ID = p_order_id
     AND t501.c501_void_fl IS NULL
     AND t501.c501_delete_fl IS NULL
     AND t5052.c5052_void_fl IS NULL
     AND t502.c502_void_fl IS NULL 
	 AND t502.c901_type IN ('50300') --  PMT-43540 To Skip loaner parts from bill and ship orders
     AND t5052.c901_status = 93310 --Active
     AND t502.c502_delete_fl IS NULL;

		RETURN v_building_count;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN 0;
	END;
	
	
	END get_order_building_count;



/*************************************************************************************************************************************
	* Description : This procedure is used to update the building id in t501_order table for the input order id for PMT-33510
	* Author     : Swetha
	* Parameters  : Orderid
****************************************************************************************************************************************/

PROCEDURE gm_update_building_order_id(
	 p_order_id IN t501_order.c501_order_id%TYPE,
	 p_user_id IN t501_order.c501_created_by%TYPE,
	 p_building_id IN t5052_location_master.c5052_location_id%TYPE DEFAULT NULL
	 	  )
	  
	  AS
	  v_building_id t5052_location_master.c5057_building_id%TYPE;
	  v_plant_id 	  t5040_plant_master.c5040_plant_id%TYPE;
	  v_default_building_id t5052_location_master.c5057_building_id%TYPE;
	  
	
BEGIN   
	  
	  
	  
	  SELECT get_plantid_frm_cntx()
        INTO  v_plant_id
        FROM DUAL;
        
     
        
  --update building id for t501_order table
  
  IF p_building_id IS NULL THEN
  
  --fetch the default building id for BUG-11243
   BEGIN
      SELECT c5057_building_id 
     INTO v_default_building_id 
       FROM t5057_building 
      WHERE c5040_plant_id = v_plant_id  
       AND c5057_primary_fl='Y' 
       AND c5057_active_fl='Y' 
       AND c5057_void_fl IS NULL;
     EXCEPTION
       WHEN NO_DATA_FOUND THEN -- when default building id is not available
        v_default_building_id := NULL;
    END; 
         
  
  BEGIN  
     SELECT t5052.c5057_building_id
		INTO v_building_id
      FROM t5053_location_part_mapping t5053, t5052_location_master t5052, t501_order t501, t502_item_order t502
     WHERE t5052.c5052_location_id= t5053.c5052_location_id
     AND t5052.c5040_plant_id   = v_plant_id
     AND t5052.c5052_void_fl IS NULL
     AND t5053.c5053_curr_qty  > 0
     AND t5052.c901_location_type =93320 and t5052.c5051_inv_warehouse_id = 1
     AND t5053.c205_part_number_id = t502.c205_part_number_id
     AND t501.C501_ORDER_ID = t502.C501_ORDER_ID
     AND t502.C501_ORDER_ID = p_order_id
     AND t501.c501_void_fl IS NULL
     AND t501.c501_delete_fl IS NULL
     AND t5052.c5052_void_fl IS NULL
     AND t502.c502_void_fl IS NULL
	 AND t502.c901_type IN ('50300') --  PMT-43540 To Skip loaner parts from bill and ship orders
     AND t502.c502_delete_fl IS NULL
     AND t5052.c901_status = 93310 --Active
     GROUP BY t5052.c5057_building_id;
   EXCEPTION
          WHEN NO_DATA_FOUND THEN
            v_building_id := NULL;
         END;
  
    --updating building id for t501_order table
    UPDATE t501_order SET c5057_building_id = NVL(v_building_id, v_default_building_id) --update default building id if building id for the part is not available
          , c501_last_updated_by = p_user_id
          , c501_last_updated_date =  current_date 
       WHERE c501_order_id = p_order_id 
      AND c501_void_fl IS NULL
      AND c501_delete_fl IS NULL;
      
      --updating inventory pick   
      UPDATE t5050_invpick_assign_detail 
       SET c5057_building_id = NVL(v_building_id, v_default_building_id)  --update default building id if building id for the part is not available
         , c5050_last_updated_by = p_user_id
         , c5050_last_updated_date = current_date 
       WHERE c5050_ref_id = p_order_id
       AND c5050_void_fl IS NULL ; 
       
       --updating building id in t907_shipping_info table
       UPDATE t907_shipping_info
        SET c5057_building_id = NVL(v_building_id, v_default_building_id) --update default building id if building id for the part is not available
          , c907_last_updated_by = p_user_id
          , c907_last_updated_date = current_date
       WHERE c907_ref_id = p_order_id
       AND c907_void_fl IS NULL;
    
   ELSIF p_building_id IS NOT NULL THEN
   
     --update building id for t501_order table
    UPDATE t501_order SET c5057_building_id = p_building_id  
          , c501_last_updated_by = p_user_id
          , c501_last_updated_date = current_date
       WHERE c501_order_id = p_order_id 
      AND c501_void_fl IS NULL
      AND c501_delete_fl IS NULL;
      
      --updating inventory pick 
        UPDATE t5050_invpick_assign_detail 
       SET c5057_building_id = p_building_id,
           c5050_last_updated_by = p_user_id,
           c5050_last_updated_date = current_date 
       WHERE c5050_ref_id = p_order_id
       AND c5050_void_fl IS NULL ; 
       
       --updating building id in t907_shipping_info table
       UPDATE t907_shipping_info
        SET c5057_building_id = p_building_id 
          , c907_last_updated_by = p_user_id
          , c907_last_updated_date = current_date
       WHERE c907_ref_id = p_order_id
       AND c907_void_fl IS NULL;
      
 END IF;			
				
END gm_update_building_order_id;


/*************************************************************************************************************************************
	* Description : This procedure is used to fetch the building id's and split the order by creating transactions and save all the shipping informations for the input order for PMT-33510
	* Author     : Swetha
	* Parameters  : Orderid
****************************************************************************************************************************************/

PROCEDURE gm_order_split_transaction(
	 p_order_id IN t501_order.c501_order_id%TYPE,
	 p_user_id IN t501_order.c501_created_by%TYPE
	)
	 
	 AS
	 
	 v_loop_cnt    NUMBER;
	 v_order_build_id  t501_order.c501_order_id%TYPE;
	 v_building_id t5052_location_master.c5057_building_id%TYPE;
	 v_chk_order_type t501_order.c901_order_type%TYPE;
	 
	
	  v_order_id           t501_order.c501_order_id%TYPE;
      v_account_id         t501_order.c704_account_id%TYPE;
      v_sales_rep_id       t501_order.c703_sales_rep_id%TYPE;
      v_order_desc         t501_order.c501_order_desc%TYPE;
      v_customer_po        t501_order.c501_customer_po%TYPE;
	  v_poid		       t501_order.c501_customer_po%TYPE;
      v_order_date         t501_order.c501_order_date%TYPE;
      v_shipping_date      t501_order.c501_shipping_date%TYPE;
      v_total_cost         t501_order.c501_total_cost%TYPE;
      v_delivery_carrier   t501_order.c501_delivery_carrier%TYPE;
      v_delivery_mode      t501_order.c501_delivery_mode%TYPE;
      v_tracking_number    t501_order.c501_tracking_number%TYPE;
      v_ship_to            t501_order.c501_ship_to%TYPE;
      v_status_fl          t501_order.c501_status_fl%TYPE;
      v_receive_mode       t501_order.c501_receive_mode%TYPE;
      v_received_from      t501_order.c501_received_from%TYPE;
      v_ship_charge_fl     t501_order.c501_ship_charge_fl%TYPE;
      v_comments           t501_order.c501_comments%TYPE;
      v_ship_to_id         t501_order.c501_ship_to_id%TYPE;
      v_ship_cost          t501_order.c501_ship_cost%TYPE;
      v_update_inv_fl      t501_order.c501_update_inv_fl%TYPE;
      v_order_date_time    t501_order.c501_order_date_time%TYPE;
      v_order_type         t501_order.c901_order_type%TYPE;
      v_reason_type        t501_order.c901_reason_type%TYPE;
      v_parent_order_id    t501_order.c501_parent_order_id%TYPE;
      v_rma_id             t501_order.c506_rma_id%TYPE;
      v_invoice_id         t501_order.c503_invoice_id%TYPE;
      v_hold_fl            t501_order.c501_hold_fl%TYPE;
      v_case_info_id       t501_order.c7100_case_info_id%TYPE;
      v_addressid          t907_shipping_info.c106_address_id%TYPE;
      v_shipid             t907_shipping_info.c907_ship_to_id%TYPE;
      v_surg_date		   t501_order.c501_surgery_date%TYPE;
      v_new_parent_order_id t501_order.c501_parent_order_id%TYPE;
      v_new_order_id  t501_order.c501_order_id%TYPE;
      v_allocate_fl  VARCHAR2 (1);
      v_new_comp_id t501_order.c1900_company_id%TYPE;
      v_new_plant_id t501_order.c5040_plant_id%TYPE;
      v_new_ref_id   t501_order.c501_ref_id%TYPE;
	  
	--fetch the orderid and the building id	
	CURSOR v_building_list
   IS
      SELECT t501.c501_order_id porderid, t5052.c5057_building_id pbuildingid
	FROM t5053_location_part_mapping t5053, t5052_location_master t5052, t501_order t501, t502_item_order t502
     WHERE t5052.c5052_location_id= t5053.c5052_location_id
     AND t5052.c5040_plant_id   = t501.c5040_plant_id 
     AND t5052.c5052_void_fl IS NULL
     AND t5053.c5053_curr_qty  > 0
     AND t5052.c901_location_type =93320 
     AND t5052.c5051_inv_warehouse_id = 1
     AND t5053.c205_part_number_id = t502.c205_part_number_id
     AND t501.c501_order_id = t502.c501_order_id
     AND t502.c501_order_id = p_order_id
     AND t501.c501_void_fl IS NULL
     AND t501.c501_delete_fl IS NULL
     AND t5052.c5052_void_fl IS NULL
     AND t502.c502_void_fl IS NULL
	 AND t502.c901_type IN ('50300') --  PMT-43540 To Skip loaner parts from bill and ship orders
     AND t5052.c901_status = 93310 --Active
     AND t502.c502_delete_fl IS NULL 
     GROUP BY t5052.c5057_building_id,t501.c501_order_id ;	  
     
	BEGIN
	
	  v_loop_cnt := 0;
	  v_new_parent_order_id := p_order_id;
	
	
	--get order type for the input order id   
   BEGIN  
     SELECT c901_order_type 
       INTO v_chk_order_type 
      FROM t501_order 
       WHERE c501_order_id = p_order_id 
       AND c501_void_fl IS NULL
       AND c501_delete_fl IS NULL;
   EXCEPTION
            WHEN NO_DATA_FOUND THEN
                v_chk_order_type := NULL;
                END;
                
	               
	FOR var_buildloop IN v_building_list
		LOOP
		   v_loop_cnt := v_loop_cnt + 1;
		   v_order_build_id := var_buildloop.porderid;
           v_building_id := var_buildloop.pbuildingid;
           
			IF (v_loop_cnt = 1) THEN
			
			--procedure call to update the  building id for the order
			gm_update_building_order_id(v_order_build_id, p_user_id, v_building_id);
			
			ELSIF (v_loop_cnt != 1) THEN
				
		 --procedure call for split	
		 BEGIN        
		 -- Fetch parent order details and store in variables
		 SELECT c501_order_id, c704_account_id, c703_sales_rep_id,
             c501_order_desc, c501_customer_po, c501_order_date,
             c501_shipping_date, c501_total_cost, c501_delivery_carrier,
             c501_delivery_mode, c501_tracking_number, c501_ship_to,
             c501_status_fl, c501_receive_mode, c501_received_from,
             c501_ship_charge_fl, c501_comments, c501_ship_to_id,
             c501_ship_cost, c501_update_inv_fl, c501_order_date_time,
             c901_order_type, c901_reason_type, c501_parent_order_id,
             c506_rma_id, c503_invoice_id, c501_hold_fl, c7100_case_info_id,c501_surgery_date,c1900_company_id,c5040_plant_id,c501_ref_id
		 INTO v_order_id, v_account_id, v_sales_rep_id,
             v_order_desc, v_customer_po, v_order_date,
             v_shipping_date, v_total_cost, v_delivery_carrier,
             v_delivery_mode, v_tracking_number, v_ship_to,
             v_status_fl, v_receive_mode, v_received_from,
             v_ship_charge_fl, v_comments, v_ship_to_id,
             v_ship_cost, v_update_inv_fl, v_order_date_time,
             v_order_type, v_reason_type, v_parent_order_id,
             v_rma_id, v_invoice_id, v_hold_fl, v_case_info_id,v_surg_date,v_new_comp_id,v_new_plant_id,v_new_ref_id
         FROM t501_order
           WHERE c501_order_id = v_new_parent_order_id
            AND c501_void_fl IS NULL
            AND c501_delete_fl IS NULL;
		 EXCEPTION
            WHEN NO_DATA_FOUND THEN
                v_order_id := NULL;
         END;

		v_poid := v_customer_po;

		 IF v_new_parent_order_id IS NOT NULL
         THEN
            v_new_order_id := get_next_order_id (NULL); -- generating new order
            
            IF (v_chk_order_type = 102080) THEN --for OUS orders
                  v_new_order_id := replace(v_new_order_id,'GM','GMOUS');
			-- adding below code to use unique PO to avoid issues in OUS distributor invoice paperwork - PMT-43523
			BEGIN                           
			 SELECT t501.c501_customer_po
			  ||'-'
			  ||(COUNT(1)+1) INTO v_poid
			 FROM t501_order t501,
			  t501_order t502
			 WHERE t501.c704_account_id = v_account_id
			 AND t501.c501_order_id     = v_new_ref_id
			 AND t501.c501_void_fl     IS NULL
			 AND t502.c501_void_fl     IS NULL
			 AND t502.c901_order_type   = 102080 -- ous dist order
			 AND t502.c501_ref_id       = t501.c501_order_id
			 AND t501.c901_order_type   = 101260-- Acknowledgement Order
			 AND t501.c501_customer_po IS NOT NULL 
			 GROUP BY t501.c501_customer_po;		
			EXCEPTION
				WHEN OTHERS
					THEN
					v_poid := v_customer_po;
			END;
			

            ELSIF (v_chk_order_type = 2531) THEN -- for Returns Loaner to consignment swap 
                  v_new_order_id := v_new_order_id || 'S';
            END IF ;
            
          END IF;
      
         
         --procedure call to save the newly created order
         gm_pkg_op_do_master.gm_sav_order_master (v_new_order_id,
                                               v_account_id,
                                               v_sales_rep_id,
                                               v_order_desc,
                                               v_poid,
                                               v_order_date,
                                               v_shipping_date,
                                               v_total_cost,
                                               v_delivery_carrier,
                                               v_delivery_mode,
                                               v_tracking_number,
                                               v_ship_to,
                                               v_status_fl,
                                               v_receive_mode,
                                               v_received_from,
                                               v_ship_charge_fl,
                                               v_comments,
                                               v_ship_to_id,
                                               NULL,
                                               v_update_inv_fl,
                                               v_order_date_time,
                                               v_order_type,
                                               v_reason_type,
                                               get_parent_order_id(v_new_parent_order_id), --input order id will be the parent code change for PMT-43198
                                               v_rma_id,
                                               v_invoice_id,
                                               v_hold_fl,
                                               v_case_info_id,
                                               p_user_id,
                                               NULL,
                                               v_surg_date
                                              );
		 
         
      --update company, plant and ref ID for newly created child order for BUG-11259 (PMT-33510)
      
         UPDATE t501_order SET c1900_company_id = v_new_comp_id 
          , c5040_plant_id = v_new_plant_id
          , c501_ref_id =  v_new_ref_id 
       WHERE c501_order_id = v_new_order_id 
        AND c501_void_fl IS NULL
        AND c501_delete_fl IS NULL;
        
      
	-- Insert Shipping information for newly created child order
      BEGIN
         SELECT c106_address_id
           INTO v_addressid
           FROM t907_shipping_info t907
          WHERE t907.c907_ref_id = v_new_parent_order_id
            AND t907.c907_void_fl IS NULL
            AND t907.c907_active_fl IS NULL;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            v_addressid := NULL;
      END;     
       
       --updating shipping details for the newly created order  
     gm_pkg_cm_shipping_trans.gm_sav_shipping (v_new_order_id, 
                                                50180,                 --orders
                                                v_ship_to,
                                                v_ship_to_id,
                                                v_delivery_carrier,
                                                v_delivery_mode,
                                                NULL,
                                                v_addressid,
                                                NULL,
                                                p_user_id,
                                                v_shipid
                                               );    
         
            --- need to call gm_update_items_order_id
            gm_update_items_order_id(p_order_id,v_new_order_id,v_building_id);
         
            -- Code to handle order which has a loaner part + 999 part.
			-- As both are not picked, the order shdn't be allocated.
			v_allocate_fl := gm_pkg_op_inventory.chk_txn_details (v_new_order_id, 93001);

			IF NVL (v_allocate_fl, 'N') = 'Y'
			THEN
				gm_pkg_allocation.gm_ins_invpick_assign_detail (93001, v_new_order_id, p_user_id);
			END IF;   
                      
         /*Add Insert's for newly created order*/
	     gm_pkg_op_sav_insert_txn.gm_sav_insert_rev_detail(v_new_order_id,106760,93342,p_user_id,NULL);
         
         --procedure call to update the total cost for newly created child order and parent order
          gm_update_order_total(v_new_order_id);
          
         --procedure call to update the  building id for the order
		  gm_update_building_order_id(v_new_order_id, p_user_id, v_building_id); 		
					         				
			END IF;
		END LOOP;
	
	 
		
END gm_order_split_transaction;




/*************************************************************************************************************************************
	* Description : This procedure is used to update total cost for newly created child order and parent order for PMT-33510
	* Author     : Swetha
	* Parameters  : Orderid
****************************************************************************************************************************************/

PROCEDURE gm_update_order_total(
	 p_order_id IN t501_order.c501_order_id%TYPE
	)
	 
	 AS
	 
	 v_parent_ord_id   t501_order.c501_order_id%TYPE;
	 v_total_cost      t501_order.c501_total_cost%TYPE;
	 v_user_id         t501_order.c501_created_by%TYPE;
	 
	 BEGIN
	 
	 BEGIN
	 SELECT c501_parent_order_id, c501_created_by
        INTO v_parent_ord_id, v_user_id
        FROM t501_order t501
       WHERE t501.c501_order_id = p_order_id
       AND t501.c501_void_fl IS NULL
       AND t501.c501_delete_fl IS NULL;
      EXCEPTION
            WHEN NO_DATA_FOUND THEN
                v_parent_ord_id := NULL;
                v_user_id := NULL;
            END;  
			
     BEGIN 
	 SELECT SUM (NVL (t502.c502_item_qty, 0) * NVL (t502.c502_item_price, 0))
        INTO v_total_cost
        FROM t502_item_order t502
       WHERE t502.c501_order_id = p_order_id
         AND t502.c502_void_fl IS NULL
         AND t502.c502_delete_fl IS NULL;
      EXCEPTION
            WHEN NO_DATA_FOUND THEN
                v_total_cost := NULL;
      END;
 

      -- Update Total Cost for child order and parent order
      UPDATE t501_order t501
         SET t501.c501_total_cost = v_total_cost
           , c501_last_updated_by = v_user_id
           , c501_last_updated_date = current_date
       WHERE t501.c501_order_id = p_order_id
         AND t501.c501_void_fl IS NULL
         AND t501.c501_delete_fl IS NULL;

 

      UPDATE t501_order t501
         SET t501.c501_total_cost =
                          NVL (t501.c501_total_cost, 0)
                          - NVL (v_total_cost, 0)
           , c501_last_updated_by = v_user_id
           , c501_last_updated_date = current_date
       WHERE t501.c501_order_id = v_parent_ord_id
         AND t501.c501_void_fl IS NULL
         AND t501.c501_delete_fl IS NULL;
         

END gm_update_order_total;



/*************************************************************************************************************************************
	* Description : This procedure is used to update the old order id by giving new order id in t502_item_order table for PMT-33510
	* Author     : Swetha
	* Parameters  : Orderid
****************************************************************************************************************************************/

PROCEDURE gm_update_items_order_id(
	 p_old_order_id IN t501_order.c501_order_id%TYPE,
	 p_new_order_id IN t502_item_order.c501_order_id%TYPE,
	 p_building_id  IN t5052_location_master.c5057_building_id%TYPE
	)
	 
    AS 
     v_pnum     t5053_location_part_mapping.c205_part_number_id%TYPE;
    
    
   BEGIN 
      
      
   UPDATE t502_item_order 
        SET c501_order_id = p_new_order_id
        WHERE c501_order_id = p_old_order_id 
        AND c502_void_fl IS NULL
        AND c502_delete_fl IS NULL
        AND c205_part_number_id IN (SELECT t5053.c205_part_number_id  
	FROM t5053_location_part_mapping t5053, t5052_location_master t5052, t501_order t501, t502_item_order t502
     WHERE t5052.c5052_location_id= t5053.c5052_location_id
     AND t5052.c5040_plant_id   =  t501.c5040_plant_id
     AND t5052.c5052_void_fl IS NULL
     AND t5053.c5053_curr_qty  > 0
     AND t5052.c901_location_type =93320 
     AND t5052.c5051_inv_warehouse_id = 1
     AND t5052.c5057_building_id = p_building_id
     AND t5053.c205_part_number_id = t502.c205_part_number_id
     AND t501.C501_ORDER_ID = t502.C501_ORDER_ID
     AND t502.C501_ORDER_ID = p_old_order_id
     AND t501.c501_void_fl IS NULL
     AND t5052.c5052_void_fl IS NULL
     AND t502.c502_void_fl IS NULL
	 AND t502.c901_type IN ('50300') --  PMT-43540 To Skip loaner parts from bill and ship orders
     AND t502.c502_delete_fl IS NULL
     AND t501.c501_delete_fl IS NULL
     GROUP BY t5053.c205_part_number_id
    );
         
      
END gm_update_items_order_id;
 
 
 /***************************************************************************************************************************************
 * Purpose: This Function is used to get the order id by passing the RA ID  for PMT-33510
 *************************************************************************************************************************************/

	FUNCTION get_order_info (
		p_return_id IN t501_order.c506_rma_id%TYPE
	)
		RETURN VARCHAR2
	IS
		v_order_id	   t501_order.c501_order_id%TYPE;
	
	
	BEGIN

	BEGIN
		SELECT t501.c501_order_id
		INTO v_order_id
      FROM t501_order t501
      WHERE t501.c506_rma_id = p_return_id
      AND t501.c501_void_fl IS NULL 
      AND t501.c501_delete_fl IS NULL ;

		RETURN v_order_id;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN '';
	END;
	
	END get_order_info;
 
 
 /*************************************************************************************************************************************
	* Description : This procedure is used to process the input order id by getting the building count for that order and split the order based on the building count  for PMT-33510
	* Author     : Swetha
	* Parameters  : Orderid
****************************************************************************************************************************************/

PROCEDURE gm_order_process_transaction(
	 p_order_id IN t501_order.c501_order_id%TYPE
	)
 
   AS
   
     v_building_cnt    NUMBER;
    
   --fetch the order id and userid for the input parent order id
    CURSOR cur_orders
		IS
		
		SELECT t501.c501_order_id orderid, t501.c501_created_by  userid
				FROM t501_order t501 
			WHERE t501.c501_order_id = p_order_id OR t501.c501_parent_order_id = p_order_id
				AND t501.c501_void_fl IS NULL
				AND t501.c501_delete_fl IS NULL
				AND t501.c501_status_fl <> 3
                AND NVL(t501.c901_order_type,'2521') 
                    IN (SELECT c906_rule_id
                          FROM t906_rules
	                   WHERE c906_rule_grp_id = 'BUILDMAPORDERTYPE'
	                   AND c1900_company_id IS NULL 
	                   AND c906_void_fl IS NULL)
              FOR UPDATE;
		
   BEGIN
		  	  
	FOR var_orderloop IN cur_orders
		LOOP
 
       v_building_cnt := get_order_building_count(var_orderloop.orderid); --get the building count
       
      
       IF (v_building_cnt <= 1) THEN
         
         --update the building id for the passed order
          gm_update_building_order_id(var_orderloop.orderid, var_orderloop.userid);
       
       ELSE
          
          --split the parent order id by creating multiple transactions based on the building count
          gm_order_split_transaction(var_orderloop.orderid, var_orderloop.userid);
          
   END IF;
   
    END LOOP;
    
END gm_order_process_transaction;

/*************************************************************************************************************************************
	* Description : This function is used to return the building short name for the input building id for PMT-36675
	* Author     : Swetha
	* Parameters  : Building id
****************************************************************************************************************************************/

   FUNCTION GET_BUILDING_SHORT_NAME (
	p_buildingid   t5057_building.c5057_building_id%TYPE
  )
	RETURN VARCHAR2
IS

	v_build_short_name	   t5057_building.c5057_building_short_name%TYPE;
	
BEGIN
	BEGIN
		SELECT c5057_building_short_name
		  INTO v_build_short_name
		  FROM t5057_building
		 WHERE c5057_building_id = p_buildingid 
		  AND  c5057_active_fl = 'Y'
		  AND  c5057_void_fl IS NULL;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN '';
	END;

	RETURN v_build_short_name;
END GET_BUILDING_SHORT_NAME;


/*************************************************************************************************************************************
	* Description : This procedure is used to validate the transaction id and transaction type to rerun the split for PMT-36676
	* Author     : Swetha
	* Parameters  : p_txn_id,p_txn_type,p_validate_msg,p_inhouse_count
****************************************************************************************************************************************/
PROCEDURE gm_validate_rerun_txn_split(
	 p_txn_id  IN VARCHAR2,
	 p_txn_type IN VARCHAR2,
	 p_validate_msg OUT VARCHAR2,
	 p_inhouse_count OUT NUMBER
	 )
	 
	AS
	 v_error_msg    VARCHAR2(200);
	 v_count NUMBER;
	 v_inhouse_count NUMBER DEFAULT 0;
	 v_txn_error_msg VARCHAR2(200);
	 
  BEGIN
 
 --check whether the transaction is available
 
  IF p_txn_type = '108500' --txn type is ORDER
  THEN
   BEGIN
      SELECT COUNT(1) INTO v_count FROM t501_order 
        WHERE c501_order_id = p_txn_id 
          AND c501_void_fl IS NULL;
  EXCEPTION
            WHEN NO_DATA_FOUND THEN
                v_count := 0;
             END;
   END IF;
  					 
  
  IF p_txn_type = '108501' --txn type is CONSIGNMENT
  THEN
   BEGIN
      SELECT COUNT(1) INTO v_count FROM t504_consignment 
        WHERE c504_consignment_id = p_txn_id 
          AND c504_void_fl IS NULL;
  EXCEPTION
            WHEN NO_DATA_FOUND THEN
                v_count := 0;
             END;
   END IF;
   
   IF p_txn_type = '108502' --txn type is Inhouse
  THEN
  
  -- for (FGRT,FGPN,FGQN)
   BEGIN
      SELECT COUNT(1) INTO v_count FROM t504_consignment 
        WHERE c504_consignment_id = p_txn_id 
          AND c504_void_fl IS NULL;
  EXCEPTION
            WHEN NO_DATA_FOUND THEN
                v_count := 0;
             END;
           
  --other inhouse transactions
  BEGIN
      SELECT COUNT(1) INTO v_inhouse_count FROM t412_inhouse_transactions 
        WHERE c412_inhouse_trans_id = p_txn_id
          AND c412_void_fl IS NULL;
  EXCEPTION
            WHEN NO_DATA_FOUND THEN
                v_inhouse_count := 0;
             END;
   END IF;
   
  --Throw validation if transaction id is not available  
  IF (v_count = 0 AND v_inhouse_count = 0)  
    THEN
      p_validate_msg := 'Entered Transaction ID does not match with Transaction type' ;
    RETURN;
  END IF;

 IF(v_count > 0 OR v_inhouse_count > 0) THEN
   --procedure call to check whether the transaction is controlled / shipped 
     gm_validate_rerun_txn_shipped(p_txn_id, p_txn_type, v_error_msg);
      
    IF v_error_msg IS NOT NULL
       THEN
       p_validate_msg :=  v_error_msg;
      RETURN;
    ELSE
     p_validate_msg := 'YES';
    END IF;
 END IF;


--procedure call to validate the transaction type and plant
 IF(p_validate_msg = 'YES')
   THEN
     IF (p_txn_type = '108500') --order
       THEN
       gm_validate_order_transaction(p_txn_id, v_txn_error_msg);
     ELSIF (p_txn_type = '108501') --consignment 
        THEN
       gm_pkg_op_storage_building_cn.gm_validate_consignment_transaction(p_txn_id, v_txn_error_msg);
      ELSIF (p_txn_type = '108502') --Inhouse transaction
        THEN
           IF(v_count > 0) --inhouse transaction available in t504_consignment (FGRT,FGPN,FGQN)
              THEN
       gm_pkg_op_storage_building_ih.gm_validate_inhouset504_transaction(p_txn_id, v_txn_error_msg);
          END IF; 
          IF(v_inhouse_count > 0) --other inhouse transactions available in t412_inhouse_transactions
              THEN
             gm_pkg_op_storage_building_ih.gm_validate_inhouset412_transaction(p_txn_id, v_txn_error_msg);
          END IF; 
       END IF;
  END IF;
   
   IF v_txn_error_msg IS NULL
     THEN
       p_validate_msg :=  'Please Enter Valid transaction ID/Type'; 
     RETURN;
   END IF;
  
 p_inhouse_count :=  v_inhouse_count;
  IF(v_txn_error_msg = 'YES')
    THEN
     p_validate_msg := 'YES'; 
   END IF;
  
  
 END gm_validate_rerun_txn_split;


/*************************************************************************************************************************************
	* Description : This procedure is used to validate whether  transaction id is controlled or shipped for PMT-36676
	* Author     : Swetha
	* Parameters  : p_txn_id,p_txn_type
****************************************************************************************************************************************/
PROCEDURE gm_validate_rerun_txn_shipped(
	 p_txn_id  IN VARCHAR2,
	 p_txn_type IN VARCHAR2,
	 p_validate_msg OUT VARCHAR2
	 )
	 
	AS
	 v_ship_status t907_shipping_info.c907_status_fl%TYPE; 
	 v_inhouse_ship_status t412_inhouse_transactions.c412_status_fl%TYPE;

BEGIN
    
  IF p_txn_type = '108500' OR p_txn_type = '108501' --txn type is ORDER/CONSIGNMENT
  THEN  
   BEGIN
      SELECT c907_status_fl INTO v_ship_status 
      FROM t907_shipping_info 
        WHERE c907_ref_id = p_txn_id 
          AND c907_void_fl  IS NULL;
  EXCEPTION
            WHEN NO_DATA_FOUND THEN
                v_ship_status := NULL;
         END;
 END IF;
 
 
 IF p_txn_type = '108502'  --txn type is Inhouse
  THEN 
  
   --This block is for Inhouse transaction(FGRT,FGPN,FGQN)
   BEGIN
      SELECT c504_status_fl INTO v_ship_status 
      FROM t504_consignment 
        WHERE c504_consignment_id  = p_txn_id 
          AND c504_void_fl IS NULL;
  EXCEPTION
            WHEN NO_DATA_FOUND THEN
                v_ship_status := NULL;
         END;
         
   --This block is for other Inhouse transactions     
      BEGIN
      SELECT c412_status_fl INTO v_inhouse_ship_status 
      FROM t412_inhouse_transactions 
        WHERE c412_inhouse_trans_id  = p_txn_id 
          AND c412_void_fl IS NULL;
  EXCEPTION
            WHEN NO_DATA_FOUND THEN
                v_inhouse_ship_status := NULL;
         END;
         
 END IF;
 
 IF (v_ship_status = 30 OR v_ship_status = 40 OR v_ship_status = 33 OR v_ship_status = 36 OR v_ship_status = 3 OR v_ship_status = 4 OR v_inhouse_ship_status = 3 OR v_inhouse_ship_status = 4)  --Pending Shipping(controlled) 30 , Completed(shipped) 40, Packing in progress 33, Ready for pickup 36,  inhouse txn controlled-3, inhouse txn verified -4
   THEN
   p_validate_msg := 'Transaction ID ' || p_txn_id || ' has been already controlled/shipped';
  ELSE
    p_validate_msg := NULL;
  END IF;

END gm_validate_rerun_txn_shipped;


/*************************************************************************************************************************************
	* Description : This procedure is used to call the split procedures dynamically based on the type of transactions for PMT-36676
	* Author     : Swetha
	* Parameters  : p_txn_id,p_txn_type,p_inhouse_cnt
****************************************************************************************************************************************/
PROCEDURE gm_pkg_split_txn_by_location(
	 p_txn_id        IN VARCHAR2,
	 p_txn_type      IN VARCHAR2,
	 p_inhouse_cnt   IN NUMBER
	 )
	 
	AS
	 v_rule_value   VARCHAR2 (2000);
	 v_exec_string  VARCHAR2 (2000);
	 v_txn_type  VARCHAR2(50);
	 
 BEGIN
   
   IF(p_txn_type = '108500' OR p_txn_type = '108501') --txn type is order/ consignment
   THEN
    v_txn_type := p_txn_type;
   END IF;
   
   IF(p_txn_type = '108502') --txn type is inhouse
    THEN
      IF(p_inhouse_cnt = 0) --for inhouse transactions FGRT,FGPN,FGQN
        THEN
          v_txn_type := p_txn_type;
      ELSIF (p_inhouse_cnt > 0) --for other inhouse transactions
        THEN
        v_txn_type := 'INHOUSE_OTHERS';
     END IF;
   END IF;
   
   BEGIN 
     SELECT get_rule_value (v_txn_type, 'CMN_SPLIT_BY_LOC')
			  INTO v_rule_value
			  FROM DUAL;
   EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_rule_value := NULL;
   END;
         
     IF v_rule_value IS NOT NULL 
       THEN  
        v_exec_string := 'BEGIN ' || v_rule_value || '(''' || p_txn_id || '''); END;';
       EXECUTE IMMEDIATE (v_exec_string);
     END IF;
         
 
 END gm_pkg_split_txn_by_location;
 
 
/*************************************************************************************************************************************
	* Description : This procedure is used to process the transaction id and transaction type to rerun the split for PMT-36676
	* Author     : Swetha
	* Parameters  : p_txn_id,p_txn_type
****************************************************************************************************************************************/
PROCEDURE gm_process_split_txn_ByLoc(
	 p_txn_id         IN VARCHAR2,
	 p_txn_type       IN VARCHAR2,
	 p_validate_msg   OUT VARCHAR2,
	 p_out_txn_id     OUT VARCHAR2
	 )

  AS
  
   v_validate_msg    VARCHAR2(2000);
   v_inhouse_count   NUMBER;
   v_out_txn_id      VARCHAR2(50);
   

 BEGIN
 
 --procedue call to validate the transaction
   gm_validate_rerun_txn_split(p_txn_id, p_txn_type, v_validate_msg, v_inhouse_count);
 
    p_validate_msg := v_validate_msg;
    
   IF(p_validate_msg = 'YES')
    THEN
     --procedure call to split the transaction based on location
     gm_pkg_split_txn_by_location(p_txn_id, p_txn_type, v_inhouse_count);
     
     --proecdure call to fetch the generated transaction id 
     gm_pkg_fch_split_txn_id(p_txn_id, p_txn_type, v_inhouse_count, v_out_txn_id);
     
      IF v_out_txn_id IS NOT NULL 
       THEN
         p_out_txn_id :=  v_out_txn_id;
     ELSE
         p_out_txn_id :=  NULL;
     END IF;
     
  --This block will get excuted when the validation fails   
   ELSE
      p_out_txn_id := NULL;
      RETURN;
   END IF;
   
   
  
   
END gm_process_split_txn_ByLoc;


/*************************************************************************************************************************************
	* Description : This procedure is used to fetch the child txn id based on the type of transactions for PMT-36676
	* Author     : Swetha
	* Parameters  : p_txn_id,p_txn_type
****************************************************************************************************************************************/
PROCEDURE gm_pkg_fch_split_txn_id (
	 p_txn_id        IN VARCHAR2,
	 p_txn_type      IN VARCHAR2,
	 p_inhouse_cnt   IN NUMBER,
	 p_out_txn_id    OUT VARCHAR2
	 )
	 
	 AS
	 
	 v_out_txn_id          VARCHAR2(100);
	 v_type		           t412_inhouse_transactions.c412_type%TYPE;
	 v_inhouse_ref_id      t412_inhouse_transactions.c412_ref_id%TYPE;
	
 BEGIN
 
  IF p_txn_type = '108500' --txn type is ORDER
  THEN
   BEGIN
     SELECT orderid INTO v_out_txn_id
    FROM
    (SELECT c501_order_id orderid
       FROM t501_order
      WHERE c501_parent_order_id = p_txn_id
      AND c501_void_fl IS NULL     
     ORDER BY c501_last_updated_date DESC
      )
   WHERE rownum = 1;
   EXCEPTION
       WHEN NO_DATA_FOUND THEN
          v_out_txn_id := NULL;
       END;
   END IF;
  					 
  
  IF p_txn_type = '108501' --txn type is CONSIGNMENT
  THEN
   BEGIN
    SELECT consignid INTO v_out_txn_id
     FROM
      (SELECT c504_consignment_id consignid
      FROM t504_consignment 
        WHERE c504_master_consignment_id = p_txn_id 
         AND c504_void_fl IS NULL
          ORDER BY c504_last_updated_date DESC
          )
    WHERE rownum = 1;
  EXCEPTION
            WHEN NO_DATA_FOUND THEN
                v_out_txn_id := NULL;
             END;
   END IF;
   
     IF p_txn_type = '108502' --txn type is INHOUSE
  THEN
  
   IF( p_inhouse_cnt >0) --inhouse transactions available in t412_inhouse_transactions table
    THEN
       BEGIN
		  SELECT c412_type INTO v_type FROM t412_inhouse_transactions
         WHERE c412_inhouse_trans_id = p_txn_id
           AND c412_void_fl IS NULL;
	 EXCEPTION
            WHEN NO_DATA_FOUND THEN
              v_type := NULL;
       END;
      
  IF (v_type NOT IN (50154,50155,50150)) THEN  --FGLE/FGLN/FGIS (This block will get executed for all other inhouse transactions in t412 table except FGLE,FGLN,FGIS)
		
   BEGIN
    SELECT inhouseid INTO v_out_txn_id
     FROM
      (SELECT c412_inhouse_trans_id inhouseid
      FROM t412_inhouse_transactions 
        WHERE c412_ref_id = p_txn_id 
         AND c412_void_fl IS NULL
          ORDER BY c412_last_updated_date DESC
          )
    WHERE rownum = 1;
  EXCEPTION
            WHEN NO_DATA_FOUND THEN
                v_out_txn_id := NULL;
      END;
  END IF;
             
    
 IF (v_type IN (50154,50155,50150)) THEN--This block will get executed for FGLE, FGLN, FGIS transactions. get the ref id of the FGLE,FGLN,FGIS transactions 
  BEGIN
    SELECT c412_ref_id INTO v_inhouse_ref_id
     FROM t412_inhouse_transactions
     WHERE c412_inhouse_trans_id = p_txn_id 
         AND c412_void_fl IS NULL; 
    EXCEPTION
            WHEN NO_DATA_FOUND THEN
                v_inhouse_ref_id := NULL;
       END;
       
       IF v_inhouse_ref_id IS NOT NULL THEN
       BEGIN
         SELECT inhouseid INTO v_out_txn_id
     FROM
      (SELECT c412_inhouse_trans_id inhouseid
      FROM t412_inhouse_transactions 
        WHERE c412_ref_id = v_inhouse_ref_id 
         AND c412_void_fl IS NULL
          ORDER BY c412_last_updated_date DESC
          )
    WHERE rownum = 1;
  EXCEPTION
            WHEN NO_DATA_FOUND THEN
                v_out_txn_id := NULL;
             END;
       END IF;
    END IF; 
 END IF;
		  
  IF( p_inhouse_cnt = 0) THEN --inhouse transactions available in t504_consignment table (FGPN,FGQN,FGRT)
  
  BEGIN
    SELECT inhouseid INTO v_out_txn_id
     FROM
      (SELECT c504_consignment_id inhouseid
      FROM t504_consignment 
        WHERE c504_reprocess_id = p_txn_id 
         AND c504_void_fl IS NULL
          ORDER BY c504_last_updated_date DESC
          )
    WHERE rownum = 1;
  EXCEPTION
            WHEN NO_DATA_FOUND THEN
                v_out_txn_id := NULL;
             END;
     END IF;
  END IF;
   
   
   p_out_txn_id := v_out_txn_id;
   
END gm_pkg_fch_split_txn_id; 

END gm_pkg_op_storage_building;
/