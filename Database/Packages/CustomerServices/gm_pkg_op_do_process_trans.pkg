/* Formatted on 2011/12/22 15:36 (Formatter Plus v4.8.0) */
--@C:\Database\Packages\CustomerServices\gm_pkg_op_do_process_trans.pkg;

CREATE OR REPLACE PACKAGE gm_pkg_op_do_process_trans
IS
   PROCEDURE gm_sav_usage_details_main (
      p_orderid         IN   t501_order.c501_order_id%TYPE,
      p_parent_ord_id   IN   t501_order.c501_parent_order_id%TYPE,
      p_case_id         IN   t501_order.c7100_case_info_id%TYPE,
      p_str             IN   VARCHAR2,
      p_accid           IN   t501_order.c704_account_id%TYPE,
      p_customer_po     IN   t501_order.c501_customer_po%TYPE,
      p_total_cost      IN   t501_order.c501_total_cost%TYPE,
      p_comments        IN   t501_order.c501_comments%TYPE,
      p_action          IN   VARCHAR2,
      p_userid          IN   t501_order.c501_created_by%TYPE,
      p_rep_id			IN   t501_order.C703_SALES_REP_ID%TYPE DEFAULT NULL,
      p_surgery_dt		IN	 t501_order.c501_surgery_date%TYPE
   );

   PROCEDURE gm_sav_order (
      p_order_id          IN   t501_order.c501_order_id%TYPE,
      p_parent_order_id   IN   t501_order.c501_order_id%TYPE,
      p_case_id           IN   t501_order.c7100_case_info_id%TYPE,
      p_action            IN   VARCHAR2,
      p_customer_po       IN   t501_order.c501_customer_po%TYPE,
      p_total_cost        IN   t501_order.c501_total_cost%TYPE,
      p_user_id           IN   t501_order.c501_created_by%TYPE,
      p_accid           IN   t501_order.c704_account_id%TYPE DEFAULT NULL,
      p_rep_id			IN   t501_order.C703_SALES_REP_ID%TYPE DEFAULT NULL,
      p_surgery_dt		IN	 t501_order.c501_surgery_date%TYPE
   );

   PROCEDURE gm_sav_order_item (
      p_order_id   IN   t502_item_order.c501_order_id%TYPE,
      p_inputstr   IN   VARCHAR2,
      p_user_id    IN   t501_order.c501_created_by%TYPE,
      p_action     IN   VARCHAR2
   );

   PROCEDURE gm_sav_tag (
      p_order_id   IN   t501_order.c501_order_id%TYPE,
      p_case_id    IN   t7104_case_set_information.c7100_case_info_id%TYPE,
      p_user_id    IN   t501_order.c501_created_by%TYPE
   );

   PROCEDURE gm_sav_sync_tag (
      p_order_id   IN   t501_order.c501_order_id%TYPE,
      p_case_id    IN   t7104_case_set_information.c7100_case_info_id%TYPE,
      p_user_id    IN   t501_order.c501_created_by%TYPE
   );

   PROCEDURE gm_sav_tag_type (
      p_order_id   IN   t501_order.c501_order_id%TYPE,
      p_action     IN   VARCHAR2
   );

   PROCEDURE gm_sav_shipping (
      p_orderid   IN   t501_order.c501_order_id%TYPE,
      p_case_id   IN   t7100_case_information.c7100_case_info_id%TYPE,
      p_userid    IN   t501_order.c501_created_by%TYPE
   );

   PROCEDURE gm_sav_item_backup_del (
      p_order_id   IN   t501_order.c501_order_id%TYPE
   );

   PROCEDURE gm_sav_sync_item_attribute (
      p_order_id   IN   t501_order.c501_order_id%TYPE,
      p_userid     IN   t501_order.c501_created_by%TYPE
   );

   PROCEDURE gm_validate_order (
      p_orderid   IN   t501_order.c501_order_id%TYPE,
      p_case_id   IN   t501_order.c7100_case_info_id%TYPE,
      p_action    IN   VARCHAR2,
      p_userid    IN   t501_order.c501_created_by%TYPE
   );

   PROCEDURE gm_sav_confirmation (
      p_order_id   IN   t501_order.c501_order_id%TYPE,
      p_action     IN   VARCHAR2,
      p_userid     IN   t501_order.c501_created_by%TYPE
   );

   PROCEDURE gm_update_tagid (
      p_order_id   IN   t502a_item_order_attribute.c501_order_id%TYPE,
      p_case_id    IN   t501_order.c7100_case_info_id%TYPE,
      p_str        IN   VARCHAR2,
      p_user_id    IN   t502a_item_order_attribute.c502a_last_updated_by%TYPE
   );
   
   PROCEDURE gm_update_ship_info (
      p_refid          IN   t907_shipping_info.c907_ref_id%TYPE,
      p_source         IN   t907_shipping_info.c901_source%TYPE,
      p_shipto         IN   t907_shipping_info.c901_ship_to%TYPE,
      p_shiptoid       IN   t907_shipping_info.c907_ship_to_id%TYPE,
      p_shipcarr       IN   t907_shipping_info.c901_delivery_carrier%TYPE,
      p_shipmode       IN   t907_shipping_info.c901_delivery_mode%TYPE,
      p_trackno        IN   t907_shipping_info.c907_tracking_number%TYPE,
      p_addressid      IN   t907_shipping_info.c106_address_id%TYPE,
      p_freightamt     IN   VARCHAR2,
      p_case_info_id   IN   t501_order.c7100_case_info_id%TYPE,
      p_userid         IN   t907_shipping_info.c907_last_updated_by%TYPE,
      p_attn           IN   t907_shipping_info.c907_override_attn_to%TYPE DEFAULT NULL,
      p_inst           IN   t907_shipping_info.c907_ship_instruction%TYPE DEFAULT NULL
   );

   PROCEDURE gm_order_validation (
      p_order_id     IN   t501_order.c501_order_id%TYPE,
      p_tag_id       IN   t5012_tag_usage.c5010_tag_id%TYPE,
      p_part_no      IN   t205_part_number.c205_part_number_id%TYPE,
      p_stropt       IN   VARCHAR2,
      p_err_str      OUT  VARCHAR2
   );
   FUNCTION get_item_attribute_val (
		p_order_id 	IN	 t501_order.c501_order_id%TYPE
	  , p_partnum	IN	 t502_item_order.c205_part_number_id%TYPE
	)
		RETURN VARCHAR2;

/***********************************************************
 * Description : Procedure to move an order to Hold Status
 *               and to send out an email to AR Team and PC 
 *               Team
 ***********************************************************/
	PROCEDURE gm_cs_sav_order_hold (
		p_txn_id		 IN   t907_cancel_log.c907_ref_id%TYPE
	  , p_cancelreason	 IN   t907_cancel_log.c901_cancel_cd%TYPE
	  , v_cancelid		 IN   t907_cancel_log.c907_cancel_log_id%TYPE
	  , p_comments		 IN   t907_cancel_log.c907_comments%TYPE
	  , p_userid		 IN   t102_user_login.c102_user_login_id%TYPE
	  , p_hold_fl		 IN   VARCHAR2 DEFAULT NULL 
	);
 
/***********************************************************
 * Description : Procedure to save control number in item
 *               order attibut table for Sterile Parts
 *                
 ***********************************************************/

	 PROCEDURE gm_sav_control_number (
      p_order_id   IN   t501_order.c501_order_id%TYPE,
      p_inputStr   IN 	VARCHAR2, 
      p_user_id    IN   t501_order.c501_created_by%TYPE,
      p_screen	   IN	VARCHAR2 DEFAULT NULL
      
   );
/***********************************************************
 * Description : Procedure to validate qty for the 
 *                Sterile Parts in order attibut table.
 *                
 ***********************************************************/  
   PROCEDURE gm_validate_do_attribute_qty (
      p_order_id          IN   t502a_item_order_attribute.c501_order_id%TYPE,
      p_attribute_type    IN   t502a_item_order_attribute.c901_attribute_type%TYPE
   );
  
   /***********************************************************
 * Description : Procedure to get control number needed flag for 
 *                Sterile Parts in part number table.
 *                
 ***********************************************************/ 
  FUNCTION get_control_number_needed_fl (
  	p_part_num	 t205_part_number.c205_part_number_id%TYPE,
    p_flag         VARCHAR2 DEFAULT 'N'	
	)
	RETURN VARCHAR2;

/***********************************************************
 * Description : Procedure to get control number edit details.
 *                
 ***********************************************************/ 

   PROCEDURE gm_fch_do_ctrl_details (
      p_order_id          IN   t502a_item_order_attribute.c501_order_id%TYPE
    , p_ctrlnumdetails   OUT		TYPES.cursor_type
   );
 
   
  /**************************************************************************************************************************************
  * Description : Procedure to get the Order Hold Flag based on the price difference calculated from predefined discount percentage.
  *                
  **************************************************************************************************************************************/ 

   PROCEDURE gm_cs_fch_order_hold_per(
      p_order_id          IN   t502a_item_order_attribute.c501_order_id%TYPE
    , p_hold_fl			  OUT   VARCHAR2
   ); 
   
   	   /***********************************************************
* Description : Procedure to validate the duplicate the order based on acct, part and qty.
* Author        : Ritesh                
 ***********************************************************/ 
       PROCEDURE gm_fch_validate_dousage (
      p_order_id          IN      t502a_item_order_attribute.c501_order_id%TYPE
    , p_acctid               IN   t501_order.c704_account_id%TYPE
    , p_doexistfl          OUT     VARCHAR2
    , p_existing_ordid       OUT  t501_order.c501_order_id%type
   );
   
   /***********************************************************
* Description : Procedure to validate the total qty check (in case if current ord has more parts then existing order.)
* Author        : Ritesh                
 ***********************************************************/ 
       PROCEDURE gm_validate_total_qty (
      p_existing_ordid  IN t501_order.c501_order_id%type
    , p_order_id            IN     t501_order.c501_order_id%type
    , p_doexistfl          OUT     VARCHAR2
   ); 
   
   /*******************************************************
    * Description : Procedure to Update the Hold Flag for Both Parent Order Back Order..
    * Parameters  : 1.Order ID , 2.User ID
    * Author     :
    *******************************************************/
   FUNCTION gm_sav_order_hold_flag (
      p_order_id   IN   t502a_item_order_attribute.c501_order_id%TYPE,
      p_userid     IN   t501_order.c501_created_by%TYPE
      
   )RETURN NUMBER;

  /*******************************************************************
   * Description : Procedure to update the ship cost for the order,
   *               this ship cost should reflect in the invoice.
   * Author      : VPrasath
   *******************************************************************/

   PROCEDURE gm_update_ship_cost (
	  p_orderid  IN t501_order.c501_order_id%TYPE
	, p_shipcost IN t501_order.c501_ship_cost%TYPE
    , p_userid 	 IN t907_shipping_info.c907_last_updated_by%TYPE
	);
	
  /*****************************************************************************************************************************************
   * Description : Procedure to update the Item Price as Unit Price for the Parts which are not having enough qty during Book New DO from CSM               
   * Author      : HReddi
   *******************************************************************************************************************************************/

   PROCEDURE gm_update_unit_price (
	  p_orderid  IN t501_order.c501_order_id%TYPE
    , p_userid   IN t907_shipping_info.c907_last_updated_by%TYPE
	);
	
   /***********************************************************
   * Description : Procedure to move an order to Hold Status
   *               and to send out an email to AR Team and PC 
   *               Team
 ***********************************************************/
	 PROCEDURE gm_mnul_move_ord_hold (
		p_txn_id		 IN   t907_cancel_log.c907_ref_id%TYPE
	  , p_cancelreason	 IN   t907_cancel_log.c901_cancel_cd%TYPE
	  , v_cancelid		 IN   t907_cancel_log.c907_cancel_log_id%TYPE
	  , p_comments		 IN   t907_cancel_log.c907_comments%TYPE
	  , p_userid		 IN   t102_user_login.c102_user_login_id%TYPE
	);
 
	 /***********************************************************
     * Description : Procedure to record Usage Lot #s for Sterile
     *               Parts used in Surgery for DOs booked from 
     *               the 'Case Book' 
     ***********************************************************/
    PROCEDURE gm_sav_case_book_order_usage (
         p_order_id   IN   t501_order.c501_order_id%TYPE,
         p_user_id    IN   t501_order.c501_created_by%TYPE
         
      );
      
    /*******************************************************************
     * Description : Procedure to validate repid is mapped to the account
     *******************************************************************/
    PROCEDURE gm_validate_rep_id (
         p_accid    	IN  t704_account.c704_account_id%TYPE DEFAULT NULL,
      	 p_rep_id   	IN  t704_account.c703_sales_rep_id %TYPE DEFAULT NULL,
      	 p_company_id   IN  t1900_company.c1900_company_id%type,
         p_err_str	OUT	VARCHAR2         
      );
      
    /*******************************************************************
     * Description : Procedure to save book order usage
     *******************************************************************/
    PROCEDURE gm_sav_do_book_order_usage (
        p_order_id   IN   t501_order.c501_order_id%TYPE,
        p_user_id    IN   t501_order.c501_created_by%TYPE
    ); 
 /***********************************************************
 * Description : Procedure to validate Quantity mismatch in Do summary screen.
 ***********************************************************/  
   PROCEDURE gm_val_do_attr_qty (
      p_order_id          IN   t502a_item_order_attribute.c501_order_id%TYPE,
      p_attribute_type    IN   t502a_item_order_attribute.c901_attribute_type%TYPE
   );    
 /*******************************************************************************************************
 * Description : Procedure to get control number needed flag by divion for 
 *               Joint Reconstruction sterile Implats Parts in part number table and project table.
 *  Parameter: part number id
 *  Return: 'Y' or ''.                
 ***************************************************************************************************************/     
  FUNCTION get_ctrl_num_needed_fl_by_division (
    p_part_num	 t205_part_number.c205_part_number_id%TYPE
  ) 
   RETURN VARCHAR2;
END gm_pkg_op_do_process_trans;
/