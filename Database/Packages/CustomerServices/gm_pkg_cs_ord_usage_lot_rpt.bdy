CREATE OR REPLACE
PACKAGE BODY gm_pkg_cs_ord_usage_lot_rpt
IS
   /*************************************************************************************************************
	* Description : This procedure is used to fetch the Order usage details based on order and part number
	* PMT-45387   : Provision to record DDT and Usage Lot when processing Credit or Debit Notes
	* Author      : Mani
	***********************************************************************************************************/
PROCEDURE gm_fch_order_used_lot_number_by_part (
	 p_order_ids             IN VARCHAR2,
	 p_part_number          IN t502b_item_order_usage.c205_part_number_id%TYPE,
	 p_invoice_id           IN t503_invoice.c503_invoice_id%TYPE,
	 p_out_usage_dtls      OUT  CLOB
     )
     
    AS
    v_out_order_ids VARCHAR2 (8000) := p_order_ids;
    BEGIN
	    
	    -- to check invoice id 
	    IF p_invoice_id IS NOT NULL
	    THEN
		    SELECT RTRIM (XMLAGG (XMLELEMENT (e, t501.c501_order_id || ',')) .EXTRACT ('//text()'), ',')
		    INTO v_out_order_ids
		   FROM t501_order t501
		  WHERE t501.c503_invoice_id = p_invoice_id
		    AND t501.c501_void_fl   IS NULL
		    AND t501.c501_delete_fl IS NULL;
       
	    END IF;
	    
	    -- to get the all the order/parent order ids
	    my_context.set_my_inlist_ctx (v_out_order_ids);
	    --
     
        SELECT JSON_ARRAYAGG (JSON_OBJECT ('pnum' value part_number
        , 'pdesc' value part_desc
        , 'usageqty' value usage_lot_qty
        , 'usagelot' value lot_number
        , 'ddtid' value ddt_number)
	  ORDER BY part_number RETURNING CLOB) 
    	INTO p_out_usage_dtls
    	FROM
    (
         SELECT t502b.c205_part_number_id part_number, get_partdesc_by_company (t502b.c205_part_number_id) part_desc,
            SUM (t502b.c502b_item_qty) usage_lot_qty, t502b.c502b_usage_lot_num lot_number, t502b.c502b_ref_id
            ddt_number
           FROM t502b_item_order_usage t502b, t501_order t501
          WHERE t501.c501_order_id                = t502b.c501_order_id
            AND t501.c501_order_id               IN (SELECT TOKEN FROM V_IN_LIST )
            AND NVL (t501.c901_order_type, 2521) <> 2524 -- Price Adjustment
            AND t502b.c205_part_number_id               = NVL(p_part_number,  c205_part_number_id )
            AND t502b.c502b_void_fl                    IS NULL
            AND t501.c501_void_fl                IS NULL
       GROUP BY t502b.c205_part_number_id, t502b.c502b_usage_lot_num, t502b.c502b_ref_id       
    )
  WHERE usage_lot_qty > 0 ;
	    
   END gm_fch_order_used_lot_number_by_part;
	  
END gm_pkg_cs_ord_usage_lot_rpt;
/