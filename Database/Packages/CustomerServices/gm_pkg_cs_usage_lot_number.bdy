CREATE OR REPLACE
PACKAGE BODY gm_pkg_cs_usage_lot_number
IS
    /*********************************************************************************
    * Description : This procedure is used to save the usage lot number.
    *     To called from new order, Edit order, Control # edit - screen
    * Author     : Manikandan
    * Parameters  : Order ID, Input string, screen name and User id
    **********************************************************************************/
PROCEDURE gm_sav_usage_ddt_dtls (
        p_order_id  IN t501_order.c501_order_id%TYPE,
        p_input_str IN CLOB,
        p_screen    IN VARCHAR2,
        p_user_id   IN t501_order.c501_created_by%TYPE)
AS
    v_string CLOB := p_input_str;
    v_substring VARCHAR2 (4000) ;
    v_strlen    NUMBER := NVL (LENGTH (p_input_str), 0) ;
    --
    v_pkey t502a_item_order_attribute.c502a_item_order_attribute_id%TYPE;
    v_order_id t502a_item_order_attribute.c501_order_id%TYPE;
    v_pnum t502a_item_order_attribute.c205_part_number_id%TYPE;
    v_attr_id t502a_item_order_attribute.c502_attribute_id%TYPE;
    v_cnum            VARCHAR2 (100) ;
    v_ddt_num         VARCHAR2 (100) ;
    v_qty             NUMBER;
    v_count           NUMBER;
    v_usage_detail_id NUMBER;
    v_ord_id t501_order.c501_order_id%TYPE;
    v_cnt           NUMBER;
    v_flag          VARCHAR2 (4000) ;
    v_error_msg     VARCHAR2 (4000) ;
    v_order_info_id VARCHAR2 (40) ;
    v_order_source  VARCHAR2 (4000) ;
    v_apporder_id   VARCHAR2 (4000) ;
    v_tmp_order_id t501_order.c501_order_id%TYPE;
    v_tmp_qty NUMBER;
    v_void_fl CHAR(1);
    v_cnum_fl BOOLEAN;
    v_company_id T1900_company.c1900_company_id%TYPE;
    v_plant_id t5040_plant_master.c5040_plant_id%TYPE;
    v_time_zone t901_code_lookup.c901_code_nm%TYPE;
    v_time_format t901_code_lookup.c901_code_nm%TYPE;
    v_acct_id t704_account.c704_account_id%TYPE;
    v_actual_fs t501_order.c501_distributor_id%TYPE; --Added for PMT-29681
    v_lot_error_type t5063_lot_error_info.c901_error_type%TYPE; --Added for PMT-29681
	v_lot_error_msg    VARCHAR2(200); --Added for PMT-29681
	v_order_date t501_order.C501_ORDER_DATE_TIME%TYPE; --Added for PMT-29681
	 
    
    -- usage update cursor
    CURSOR back_order_cur (v_part IN t502_item_order.c205_part_number_id%TYPE)
    IS
         SELECT t502b.c501_order_id ord_id, t502.c500_order_info_id info_id, t502b.c502b_item_order_usage_id usage_id
          , t502b.c502b_item_qty qty, t502.c502_item_qty item_qty
           FROM t502_item_order t502, t502b_item_order_usage t502b
          WHERE t502.c500_order_info_id = t502b.c500_order_info_id
            AND t502b.c501_order_id    IN
            (
                 SELECT c501_order_id
                   FROM t501_order
                  WHERE c501_order_id       = p_order_id
                    OR c501_parent_order_id = p_order_id
            )
        AND t502.c205_part_number_id   = v_part
        AND t502.c502_void_fl         IS NULL
        AND t502.c502_delete_fl       IS NULL
        AND t502b.c502b_void_fl       IS NULL
        AND t502b.c502b_usage_lot_num IS NULL;
BEGIN
	-- To get the company infromation.
      SELECT  get_plantid_frm_cntx, get_compid_frm_cntx()
		INTO  v_plant_id, v_company_id FROM DUAL;
    --
    SELECT get_code_name(c901_timezone), get_code_name(c901_date_format) 
        INTO v_time_zone,v_time_format
     FROM t1900_company
     WHERE c1900_company_id= v_company_id;
     --Set Context
     gm_pkg_cor_client_context.gm_sav_client_context(v_company_id,v_time_zone,v_time_format,v_plant_id);
    --1. save item orders details to order info table.
    IF p_screen = 'NEW_ORDER' THEN
        gm_pkg_cs_usage_lot_number.gm_sav_order_info (p_order_id, 'Y', p_user_id) ;
    END IF;
       
    IF p_screen is null  THEN
    	-- Fetching Acct ID for the order to adjust the net qty for the account
   
		 SELECT C704_ACCOUNT_ID
		 INTO v_acct_id
		 FROM t501_order
		 WHERE C501_ORDER_ID = p_order_id;
			  
		-- Calling Field Sales Net Qty procedure
      	 gm_pkg_set_lot_track.gm_pkg_set_lot_update(p_order_id,p_user_id,v_acct_id,'50180','5','4302');
    
    END IF;
    
    
    /*Getting transaction field sales id for PMT-29681 */
            BEGIN
				SELECT c501_distributor_id, c501_order_date_time
					INTO  v_actual_fs, v_order_date
				FROM T501_ORDER
				 WHERE c501_order_id = p_order_id
				 AND c501_void_fl  IS NULL;
			  EXCEPTION
            WHEN NO_DATA_FOUND THEN
                v_actual_fs := NULL;
            END;
            
    
    IF v_strlen                      > 0 THEN
        WHILE INSTR (v_string, '|') <> 0
        LOOP
            v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
            v_string    := SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
            --
            v_usage_detail_id := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            v_substring       := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
            v_pnum            := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            v_substring       := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
            v_qty             := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            v_substring       := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
            v_cnum            := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            v_substring       := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
            v_ddt_num         := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            v_substring       := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
            v_ord_id          := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            v_substring       := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
            v_order_info_id   := v_substring;
            --
            v_void_fl := 'N';
            v_cnum_fl := FALSE;
            v_cnum := TRIM(v_cnum);  --PC-2342: 
            --
            IF p_screen         = 'NEW_ORDER' THEN
                v_tmp_qty      := v_qty;
                FOR back_order IN back_order_cur (v_pnum)
                LOOP
                    --
                    IF v_tmp_qty     > 0 THEN
                        IF v_tmp_qty = back_order.qty THEN
                            gm_sav_usage_dtls (back_order.ord_id, back_order.usage_id, v_pnum, v_tmp_qty, v_cnum,
                            v_ddt_num, back_order.info_id, NULL, NULL, p_user_id) ;
                           
                            /* call the gm_sav_usage_error_dtls() to update the error message in T502B_ITEM_ORDER_USAGE table */
                            -- gm_sav_usage_error_dtls(back_order.ord_id ,v_cnum , v_pnum , back_order.usage_id); --removed this procedure call for PMT-29681
                            /*This procedure is used to check the lot is already billed*/
                           -- gm_save_billed_lot(back_order.ord_id ,v_pnum ,v_cnum); --removed this procedure call for PMT-29681
                           
                           --This procedure will validate the lot for PMT-29681
			            gm_pkg_op_lot_validation.gm_validate_lot_number(v_cnum, v_pnum, v_actual_fs, v_lot_error_msg, v_lot_error_type); 
			                 
			            IF v_lot_error_msg IS NOT NULL
			             THEN
			                --This procedure will update the error message in t502b_item_order_usage table
			                 gm_pkg_op_lot_error_txn.gm_sav_lot_order_error(back_order.ord_id, v_lot_error_msg, v_pnum, v_cnum);
			                 
			                 --This procedure will save the Error details in t5063_lot_error_info table 
			                 gm_pkg_op_lot_error_txn.gm_sav_lot_error(v_lot_error_type, v_lot_error_msg, v_pnum, v_cnum, back_order.ord_id, v_order_date, back_order.info_id, p_user_id, 107980, v_actual_fs, NULL); --107980 order
			                 
			            END IF;
			             IF NVL(v_lot_error_type,'-999') != 107974 THEN --107974 invalid inventory error for PMT-29681
                            --lot track update
                            gm_pkg_op_lot_track.gm_lot_track_main(v_pnum,v_tmp_qty,back_order.ord_id,p_user_id,'4000339','4302','4310',v_cnum);
                            --
                          END IF;
                            v_tmp_qty  := 0;
                        ELSIF v_tmp_qty > back_order.qty THEN
                            gm_sav_usage_dtls (back_order.ord_id, back_order.usage_id, v_pnum, back_order.qty, v_cnum,
                            v_ddt_num, back_order.info_id, NULL, NULL, p_user_id) ;
                            -- gm_sav_usage_error_dtls(back_order.ord_id ,v_cnum , v_pnum , back_order.usage_id); --removed this procedure call for PMT-29681
                            -- gm_save_billed_lot(back_order.ord_id ,v_pnum ,v_cnum); --removed this procedure call for PMT-29681
                            
                            --This procedure will validate the lot for PMT-29681
			            gm_pkg_op_lot_validation.gm_validate_lot_number(v_cnum, v_pnum, v_actual_fs, v_lot_error_msg, v_lot_error_type); 
			                 
			            IF v_lot_error_msg IS NOT NULL
			             THEN
			             
			                --This procedure will update the error message in t502b_item_order_usage table
			                 gm_pkg_op_lot_error_txn.gm_sav_lot_order_error(back_order.ord_id, v_lot_error_msg, v_pnum, v_cnum);
			                 
			                 --This procedure will save the Error details in t5063_lot_error_info table 
			                 gm_pkg_op_lot_error_txn.gm_sav_lot_error(v_lot_error_type, v_lot_error_msg, v_pnum, v_cnum, back_order.ord_id, v_order_date, back_order.info_id, p_user_id, 107980, v_actual_fs, NULL); --107980 order
			                 
			            END IF;
			            
			            IF NVL(v_lot_error_type,'-999')  != 107974 THEN --107974 invalid inventory error for PMT-29681
                            -- lot track update
                        	gm_pkg_op_lot_track.gm_lot_track_main(v_pnum,back_order.qty,back_order.ord_id,p_user_id,'4000339','4302','4310',v_cnum);
                        	--
                        	
                       END IF;
                            v_tmp_qty := v_tmp_qty - back_order.qty;
                        END IF; --
                       
			           
                     END IF; -- v_tmp_qty
                END LOOP;
            ELSE
            	-- BBA Lot track 
            	IF NVL(p_screen,'-999') <> 'UPDATE_DDT' AND v_usage_detail_id IS NOT NULL
            	THEN
	           		-- If the lot number for control attribute is removed, that time need to update the status of old lot number
	           		-- bba control # updated to new - at the time old control # lot status update to null.
	       		 	gm_pkg_op_lot_track.gm_sav_edit_order_sold_cnum(v_ord_id, v_pnum, v_usage_detail_id, NVL(v_cnum, 'NOC#'), p_user_id,v_cnum_fl);
       		 	END IF;
       		   		 	 
       		 	-- to save the usage lot number to T502B table.
                gm_sav_usage_dtls (v_ord_id, v_usage_detail_id, v_pnum, v_qty, v_cnum, v_ddt_num, v_order_info_id, NULL
                , NULL, p_user_id) ;
                --gm_sav_usage_error_dtls(v_ord_id ,v_cnum , v_pnum , v_usage_detail_id); --removed this procedure call for PMT-29681
                -- to update the usage lot # status as sold
                 /*This procedure is used to check the lot is already billed*/
               -- gm_save_billed_lot(v_ord_id ,v_pnum ,v_cnum); --removed this procedure call for PMT-29681
                  --lot track update
                  
           
                  --This procedure will validate the lot for PMT-29681
                  gm_pkg_op_lot_validation.gm_validate_lot_number(v_cnum, v_pnum, v_actual_fs, v_lot_error_msg, v_lot_error_type); 
                  
                  IF v_lot_error_msg IS NOT NULL
              THEN
                --This procedure will update the error message in t502b_item_order_usage table
                 gm_pkg_op_lot_error_txn.gm_sav_lot_order_error(p_order_id, v_lot_error_msg, v_pnum, v_cnum);
                 
                 --This procedure will save the Error details in t5063_lot_error_info table 
                 gm_pkg_op_lot_error_txn.gm_sav_lot_error(v_lot_error_type, v_lot_error_msg, v_pnum, v_cnum, p_order_id, v_order_date, v_order_info_id, p_user_id, 107980, v_actual_fs, NULL); --107980 order
                 
            END IF;
                  
                  IF NVL(v_lot_error_type,'-999') != 107974 THEN --107974 invalid inventory error for PMT-29681
                  
	            IF NVL(p_screen, '-999') <> 'UPDATE_DDT' AND v_usage_detail_id IS NULL OR (v_usage_detail_id IS NOT NULL AND v_cnum_fl = TRUE) THEN
	       			gm_pkg_op_lot_track.gm_lot_track_main(v_pnum,v_qty,v_ord_id,p_user_id,'4000339','4302','4310',v_cnum);
	       		END IF;
	       	END IF;
            END IF;--p_screen
       		-- S part validation
       		v_flag := gm_pkg_op_do_process_trans.get_control_number_needed_fl(v_pnum);
            
            IF v_flag = 'Y' AND v_cnum IS  NULL AND (p_screen IS NULL OR p_screen = 'NEW_ORDER')
            THEN
            	v_error_msg:= 'Please enter valid lot number for part: '|| v_pnum ||' from order id: ' || NVL(v_ord_id, p_order_id)  ;
            	raise_application_error
                          ('-20999',
                           v_error_msg
                          );
             END IF; -- Validate S part             
        END LOOP;
    END IF;
    --gm_validate_do_attribute_qty(p_order_id,101723);
    IF NVL(p_screen, '-999') = 'UPDATE_DDT' THEN               
      gm_pkg_cs_location_lot_tracking.gm_sav_loc_lot_txn_update(v_ord_id,p_user_id); 
    END IF ;
    
END gm_sav_usage_ddt_dtls;
/*********************************************************************************
* Description : This procedure is used to save the usage data to T502B.
* Author     : Manikandan
* Parameters  : Order ID
**********************************************************************************/
PROCEDURE gm_sav_usage_dtls (
        p_order_id IN t501_order.c501_order_id%TYPE,
        p_usage_dtls_id IN T502B_ITEM_ORDER_USAGE.C502B_ITEM_ORDER_USAGE_ID%TYPE,
        p_part_num IN T502B_ITEM_ORDER_USAGE.C205_PART_NUMBER_ID%TYPE,
        p_qty IN T502B_ITEM_ORDER_USAGE.C502B_ITEM_QTY%TYPE,
        p_usage_lot IN T502B_ITEM_ORDER_USAGE.C502B_USAGE_LOT_NUM%TYPE,
        p_ref_id IN T502B_ITEM_ORDER_USAGE.C502B_REF_ID%TYPE,
        p_order_info_id IN T502B_ITEM_ORDER_USAGE.C500_ORDER_INFO_ID%TYPE,
        p_trans_id IN T502B_ITEM_ORDER_USAGE.c502b_trans_id%TYPE,
        p_void_fl  IN T502B_ITEM_ORDER_USAGE.c502b_void_fl%TYPE,
        p_user_id  IN t501_order.c501_created_by%TYPE) 
AS
    v_number NUMBER;
    v_usage_lot T502B_ITEM_ORDER_USAGE.C502B_USAGE_LOT_NUM%TYPE := UPPER(TRIM(p_usage_lot));
BEGIN
	/* Save the lot number as uppercase letters even if it is entered with lowercase letters in all Screens  
	 */
	
	     UPDATE T502B_ITEM_ORDER_USAGE
	        SET C501_ORDER_ID         = p_order_id,
	    c502b_usage_lot_num           = v_usage_lot,
	    c502b_item_qty                = p_qty ,
	    c502b_ref_id                  = p_ref_id,
	    c500_order_info_id            = p_order_info_id,
	    c502b_trans_id                = p_trans_id ,
	    c502b_void_fl                 = p_void_fl,
	    c502b_last_updated_by         = p_user_id,
	    c502b_last_updated_date       = CURRENT_DATE
	  WHERE C502B_ITEM_ORDER_USAGE_ID = p_usage_dtls_id
	  AND c502b_void_fl              IS NULL;
	  
    IF (SQL%ROWCOUNT                  = 0) THEN
         INSERT
           INTO T502B_ITEM_ORDER_USAGE
            (
                C502B_ITEM_ORDER_USAGE_ID, C501_ORDER_ID, C205_PART_NUMBER_ID
              , C502B_ITEM_QTY, C502B_USAGE_LOT_NUM, c502b_ref_id
              , c500_order_info_id, c502b_created_by, c502b_created_date
              , c502b_trans_id, c502b_void_fl
            )
            VALUES
            (
                s502B_ITEM_ORDER_USAGE.nextval, p_order_id, p_part_num
              , p_qty, v_usage_lot, p_ref_id
              , p_order_info_id, p_user_id, CURRENT_DATE
              , p_trans_id, p_void_fl
            ) ;
    END IF;
END gm_sav_usage_dtls;
/*********************************************************************************
* Description : This procedure is used to sync the item orders to info and usage table.
* Author     : Manikandan
* Parameters  : Order ID, User ID
**********************************************************************************/
PROCEDURE gm_sav_order_info
    (
        p_order_id IN t501_order.c501_order_id%TYPE,
        p_usage_lot_fl   IN VARCHAR2,
        p_user_id  IN t501_order.c501_created_by%TYPE
    )
AS
    v_order_usage_id NUMBER;
    v_item_order_id  NUMBER;
    --
    CURSOR order_info_cur
    IS
         SELECT c500_order_info_id info_id, c501_order_id ord_id, c205_part_number_id pnum
          , c500_item_qty iqty, c500_item_price price, c500_order_info_id
           FROM t500_order_info
          WHERE c501_order_id IN
            (
                 SELECT c501_order_id
                   FROM t501_order
                  WHERE c501_order_id       = p_order_id
                    OR c501_parent_order_id = p_order_id
            )
           AND c500_void_fl IS NULL;
BEGIN
    --
     INSERT
       INTO t500_order_info
        (
            c500_order_info_id, c501_order_id, c205_part_number_id
          , c500_item_qty, c500_item_price, c500_created_by
          , c500_created_date
        )
     SELECT s500_order_info.nextval, c501_order_id, c205_part_number_id
      , c502_item_qty, c502_item_price, p_user_id
      , CURRENT_DATE
       FROM t502_item_order
      WHERE c501_order_id IN
        (
             SELECT c501_order_id
               FROM t501_order
              WHERE c501_order_id       = p_order_id
                OR c501_parent_order_id = p_order_id
        )
        AND c502_void_fl IS NULL;
    --
    IF p_usage_lot_fl = 'Y' THEN
        --
         INSERT
           INTO t502b_item_order_usage
            (
                C502B_ITEM_ORDER_USAGE_ID, C501_ORDER_ID, C205_PART_NUMBER_ID
              , C502B_ITEM_QTY, C502B_USAGE_LOT_NUM, c502b_ref_id
              , c502b_created_by, c502b_created_date, c500_order_info_id
            )
         SELECT s502B_ITEM_ORDER_USAGE.nextval, c501_order_id, c205_part_number_id
          , c500_item_qty, NULL, NULL
          , p_user_id, CURRENT_DATE, c500_order_info_id
           FROM t500_order_info
          WHERE c501_order_id IN
            (
                 SELECT c501_order_id
                   FROM t501_order
                  WHERE c501_order_id       = p_order_id
                    OR c501_parent_order_id = p_order_id
            )
            AND c500_void_fl IS NULL;
    END IF;
    FOR order_info IN order_info_cur
    LOOP
    	BEGIN
         SELECT c502_item_order_id
           INTO v_item_order_id
           FROM t502_item_order
          WHERE c501_order_id       = order_info.ord_id
            AND c205_part_number_id = order_info.pnum
            AND c502_item_qty       = order_info.iqty
            AND c502_item_price     = order_info.price
            AND c500_order_info_id IS NULL
            AND c502_void_fl       IS NULL
            AND ROWNUM              = 1
       ORDER BY c502_item_order_id;
       --
       EXCEPTION
            WHEN NO_DATA_FOUND THEN
                v_item_order_id := NULL;
            END;
        --
         UPDATE t502_item_order
        SET c500_order_info_id     = order_info.info_id
          WHERE c502_item_order_id = v_item_order_id;
        --
    END LOOP;
END gm_sav_order_info;
/*********************************************************************************
* Description : This procedure is used to fetch the DDT number details
* Author     : Manikandan
* Parameters  : Order ID
**********************************************************************************/
PROCEDURE gm_fch_ddt_dtls (
        p_order_id IN t501_order.c501_order_id%TYPE,
        p_out_ord OUT TYPES.cursor_type)
AS
BEGIN
    OPEN p_out_ord FOR SELECT t502b.*,
    t502.c205_part_number_id PNUM,
    t502.iqty IQTY FROM
    (
         SELECT t502.c501_order_id c501_order_id, t502.c205_part_number_id
          , SUM (c502_item_qty) iqty
           FROM t501_order t501, t502_item_order t502
          WHERE t501.c501_order_id       = p_order_id
            AND t502.c501_order_id       = t501.c501_order_id
            AND t502.c502_void_fl       IS NULL
       GROUP BY t502.c501_order_id, t502.c205_part_number_id
    )
    t502, (
        SELECT DISTINCT t502b.c205_part_number_id ID, get_partdesc_by_company (t502b.c205_part_number_id) PDESC, NVL (
            t502b.c502b_item_qty, t502.c502_item_qty) qty, NULL CNUM
            --, t502.c502_item_qty item_qty
          , t502.c502_item_price PRICE, NULL ITEMID, t502.C901_TYPE ITEMTYPE
          , GET_CODE_NAME_ALT (t502.C901_TYPE) ITEMTYPEDESC, NVL (t502.C502_UNIT_PRICE, '0') unitprice, NVL (
            t502.c502_unit_price_adj_value, '0') UnitpriceAdj, t502.c901_unit_price_adj_code UnitpriceAdjCodeID, DECODE
            (t502.c901_unit_price_adj_code, NULL, 'N/A', GET_CODE_NAME (t502.c901_unit_price_adj_code))
            unitpriceAdjCode, t502.c502_ITEM_PRICE netunitprice, NVL (NVL (t502b.c502b_item_qty, t502.c502_item_qty) *
            t502.C502_UNIT_PRICE, 0) beforePrice, NVL (NVL (t502b.c502b_item_qty, t502.c502_item_qty)                *
            t502.C502_ITEM_PRICE, 0) afterPrice, t502b.c502b_ref_id ddt, t502b.c502b_usage_lot_num lot_num
          , t502b.c502b_item_order_usage_id item_order_usage_id, t502.c501_order_id order_id,
            gm_pkg_op_do_process_trans.get_control_number_needed_fl (T502.C205_PART_NUMBER_ID) cnumfl,
            t502b.c500_order_info_id order_info_id
           FROM t502_item_order t502, t502b_item_order_usage t502b
          WHERE t502.c500_order_info_id = t502b.c500_order_info_id
            AND t502b.c501_order_id     = p_order_id
            AND t502b.c502b_void_fl IS NULL
            AND t502.c502_void_fl IS NULL
            AND t502.c502_delete_fl IS NULL
    )
    t502b WHERE t502.c501_order_id = t502b.order_id AND t502.c205_part_number_id = t502b.ID ORDER BY t502b.ID,
    t502b.qty DESC;
END gm_fch_ddt_dtls;
/*********************************************************************************
* Description : This procedure is used to fetch lot number based on part # - used for Usage lot screen
* Author     : Manikandan
* Parameters  : Order ID, Part #
**********************************************************************************/
PROCEDURE gm_fch_lot_number_dtls (
        p_order_id IN t501_order.c501_order_id%TYPE,
        p_part_num T502B_ITEM_ORDER_USAGE.C205_PART_NUMBER_ID%TYPE,
        p_out_lot_dtls OUT TYPES.cursor_type)
AS
BEGIN
    OPEN p_out_lot_dtls FOR SELECT c502b_usage_lot_num ID,
  c502b_usage_lot_num
  || ' ('
  || SUM (c502b_item_qty)
  ||')' NAME,t500.c500_item_price -- fetching lot details by item price
FROM t502b_item_order_usage t502b,t501_order t501,t500_order_info t500
WHERE (t501.c501_order_id      = p_order_id or t501.c501_parent_order_id=p_order_id)
AND t501.c501_order_id = t502b.c501_order_id
AND t502b.c205_part_number_id  = p_part_num
AND c502b_void_fl       IS NULL
AND t502b.c500_order_info_id = t500.c500_order_info_id
AND t500.c500_void_fl is null
-- commented below line to show empty lot details
--AND c502b_usage_lot_num IS NOT NULL 
AND C501_void_fl       IS NULL
GROUP BY c502b_usage_lot_num,t500.c500_item_price
Having SUM (c502b_item_qty) >0 ;
END gm_fch_lot_number_dtls;
/*********************************************************************************
* Description : This procedure is used to fetch usage details
* Author     : Manikandan
* Parameters  : Order ID
**********************************************************************************/
PROCEDURE gm_fch_order_usage_dtls (
        p_order_id IN t501_order.c501_order_id%TYPE,
        p_out_order_usage_dtls OUT TYPES.cursor_type)
AS
v_company_id		t1900_company.c1900_company_id%type;
v_edit_ctrl_num		t502b_item_order_usage.c502b_usage_lot_num%type;
v_account_id        t704_account.c704_account_id%type;  
v_Lot_flag          VARCHAR2(10);
v_comp_id           VARCHAR2(10);
BEGIN
	-- Added below code to show order id for italy company
	--(PC-2827) - Incorrect correct quantity displayed in usage screen when 
	--same Part # is on the Parent and Back Order 		
									
 BEGIN
	SELECT c1900_company_id 
	  INTO v_company_id 
	  FROM t501_order 
	 WHERE c501_order_id = p_order_id
	   AND c501_void_fl IS NULL;
 EXCEPTION WHEN OTHERS THEN
 	v_company_id := NULL;
 END;
 
 BEGIN
     SELECT c704_account_id                               ---PC-4960 Edit lot changes - Edit Order
       INTO v_account_id
       FROM t501_order
      WHERE c501_order_id = p_order_id
        AND c501_void_fl IS NULL;
 EXCEPTION WHEN OTHERS THEN
 	v_account_id := NULL;
 END;
 
 BEGIN
      SELECT DISTINCT COMPID
	      INTO v_comp_id
	      FROM V700_TERRITORY_MAPPING_DETAIL
	      WHERE AC_ID = v_account_id;
 EXCEPTION WHEN OTHERS THEN
 	v_comp_id := NULL;
 END;
  
 BEGIN	      
	 SELECT nvl(get_rule_value_by_company(v_comp_id,'DIVLOT', v_company_id) ,'N')
	  INTO v_Lot_flag 
	  FROM dual;
 END;
 
	SELECT nvl(get_rule_value_by_company(v_company_id,'EDIT_CTRL_NUM', v_company_id) ,'N')
	  INTO v_edit_ctrl_num 
	  FROM dual;
 
	IF v_edit_ctrl_num = 'Y' THEN
		OPEN p_out_order_usage_dtls FOR
		
    SELECT t502b.c502b_item_order_usage_id pkey,
    t502b.c501_order_id order_id,
    t502.c205_part_number_id pnum,
    get_partdesc_by_company (t502.c205_part_number_id) pdesc,
    iqty item_qty,
    NVL (icqty, iqty) iqty,
    t502b.c502b_usage_lot_num cnum,
    gm_pkg_op_do_process_trans.get_control_number_needed_fl (T502.C205_PART_NUMBER_ID,v_Lot_flag) cnumfl,
    t502b.c500_order_info_id order_info_id,
    t502b.c502b_ref_id ddt_num,
    T502B.C502B_USAGE_LOT_ERRORMSG loterrormsg,
    get_partnum_material_type(t502.c205_part_number_id) prodmat   ---prodmat added only for Tissue Lot unique number validation used in PC-4659
     FROM
    (
         SELECT NVL (t501.c501_parent_order_id, t501.c501_order_id) c501_order_id, t502.c205_part_number_id,
            SUM (c502_item_qty) iqty
           FROM t501_order t501, t502_item_order t502
          , (
                 SELECT C501_ORDER_ID
                   FROM t501_Order t501
                  WHERE t501.c501_void_fl        IS NULL
                    START WITH T501.c501_order_id =
                    (
                         SELECT NVL (t501.c501_parent_order_id, t501.c501_order_id)
                           FROM t501_order t501
                          WHERE c501_order_id = p_order_id
                    )
                    CONNECT BY NOCYCLE PRIOR t501.c501_order_id = t501.c501_parent_order_id
            )
            ORD
          WHERE t501.c501_order_id       = ORD.C501_ORDER_ID
            AND t502.c501_order_id       = t501.c501_order_id
            AND t502.c502_void_fl       IS NULL
       GROUP BY NVL (t501.c501_parent_order_id, t501.c501_order_id), t502.c205_part_number_id
    )
    t502, (  select * from (SELECT t502b.c501_order_id,t501.c901_order_type, t502b.c205_part_number_id
         ,t502b.c502b_item_qty icqty 
          , t502b.c502b_usage_lot_num, t502b.c502b_item_order_usage_id, t502b.c500_order_info_id
          , t502b.c502b_ref_id , t502b.C502B_USAGE_LOT_ERRORMSG
           FROM t502b_item_order_usage t502b, t501_order t501, (
                 SELECT C501_ORDER_ID
                   FROM t501_Order t501
                  WHERE t501.c501_void_fl        IS NULL
                    START WITH T501.c501_order_id =
                    (
                         SELECT NVL (t501.c501_parent_order_id, t501.c501_order_id)
                           FROM t501_order t501
                          WHERE c501_order_id = p_order_id
                    )
                    CONNECT BY NOCYCLE PRIOR t501.c501_order_id = t501.c501_parent_order_id
            )
            ORD,T500_order_info t500
          WHERE t501.c501_order_id    = ORD.C501_ORDER_ID
            AND t502b.c501_order_id   = t501.c501_order_id
            AND t502b.c500_order_info_id = t500.c500_order_info_id
            AND t500.c500_void_fl IS NULL
            AND t502b.c502b_void_fl  IS NULL) 
            where nvl(c901_order_type,-999) NOT IN ('2529','2528')  --  2529 - Sales Adjustment, 2528-Credit Memo , to hide return order details . 
            and decode(c901_order_type,2535,1,icqty)>0 -- 2535 - Sales Discount ,to show sales discount line items.
    )
    t502b WHERE t502.c205_part_number_id = t502b.c205_part_number_id ORDER BY pnum,
    cnum;
	ELSE
	
    OPEN p_out_order_usage_dtls FOR 
    SELECT t502b.c502b_item_order_usage_id pkey,
    t502b.c501_order_id order_id,
    t502.c205_part_number_id pnum,
    get_partdesc_by_company (t502.c205_part_number_id) pdesc,
    iqty item_qty,
    NVL (icqty, iqty) iqty,
    t502b.c502b_usage_lot_num cnum,
    gm_pkg_op_do_process_trans.get_control_number_needed_fl (T502.C205_PART_NUMBER_ID,v_Lot_flag) cnumfl,
    t502b.c500_order_info_id order_info_id,
    t502b.c502b_ref_id ddt_num,
    T502B.C502B_USAGE_LOT_ERRORMSG loterrormsg,
    get_partnum_material_type(t502.c205_part_number_id) prodmat     ---prodmat added only for Tissue Lot unique number validation used in PC-4659
     FROM
    (
         SELECT NVL (t501.c501_parent_order_id, t501.c501_order_id) c501_order_id, t502.c205_part_number_id,
            SUM (c502_item_qty) iqty
           FROM t501_order t501, t502_item_order t502
          , (
                 SELECT C501_ORDER_ID
                   FROM t501_Order t501
                  WHERE t501.c501_void_fl        IS NULL
                    START WITH T501.c501_order_id =
                    (
                         SELECT NVL (t501.c501_parent_order_id, t501.c501_order_id)
                           FROM t501_order t501
                          WHERE c501_order_id = p_order_id
                    )
                    CONNECT BY NOCYCLE PRIOR t501.c501_order_id = t501.c501_parent_order_id
            )
            ORD
          WHERE t501.c501_order_id       = ORD.C501_ORDER_ID
            AND t502.c501_order_id       = t501.c501_order_id
            AND t502.c502_void_fl       IS NULL
       GROUP BY NVL (t501.c501_parent_order_id, t501.c501_order_id), t502.c205_part_number_id
    )
    t502, (  select * from (SELECT t502b.c501_order_id,t501.c901_order_type, t502b.c205_part_number_id
         , sum(t502b.c502b_item_qty) OVER (PARTITION BY t502b.c205_part_number_id,t502b.c502b_usage_lot_num,t500.c500_item_price)  icqty -- added item price in partition  
          , t502b.c502b_usage_lot_num, t502b.c502b_item_order_usage_id, t502b.c500_order_info_id
          , t502b.c502b_ref_id , t502b.C502B_USAGE_LOT_ERRORMSG
           FROM t502b_item_order_usage t502b, t501_order t501, (
                 SELECT C501_ORDER_ID
                   FROM t501_Order t501
                  WHERE t501.c501_void_fl        IS NULL
                    START WITH T501.c501_order_id =
                    (
                         SELECT NVL (t501.c501_parent_order_id, t501.c501_order_id)
                           FROM t501_order t501
                          WHERE c501_order_id = p_order_id
                    )
                    CONNECT BY NOCYCLE PRIOR t501.c501_order_id = t501.c501_parent_order_id
            )
            ORD,T500_order_info t500
          WHERE t501.c501_order_id    = ORD.C501_ORDER_ID
            AND t502b.c501_order_id   = t501.c501_order_id
            AND t502b.c500_order_info_id = t500.c500_order_info_id
            AND t500.c500_void_fl IS NULL
            AND t502b.c502b_void_fl  IS NULL) 
            where nvl(c901_order_type,-999) NOT IN ('2529','2528')  --  2529 - Sales Adjustment, 2528-Credit Memo , to hide return order details . 
            and decode(c901_order_type,2535,1,icqty)>0 -- 2535 - Sales Discount ,to show sales discount line items.
    )
    t502b WHERE t502.c205_part_number_id = t502b.c205_part_number_id ORDER BY pnum,
    cnum;
    END IF;
END gm_fch_order_usage_dtls;
/*********************************************************************************
* Description : This procedure is used to fetch used lot number details
* Author     : Manikandan
* Parameters  : Order ID
**********************************************************************************/
PROCEDURE gm_fch_used_lot_number (
        p_order_id IN t501_order.c501_order_id%TYPE,
        p_out_used_lot OUT TYPES.cursor_type)
AS
BEGIN
    OPEN p_out_used_lot FOR SELECT c205_part_number_id pnum,
    get_partdesc_by_company ( c205_part_number_id) PDESC,
    SUM ( c502b_item_qty) qty FROM t502b_item_order_usage WHERE c501_order_id = p_order_id AND c502b_void_fl IS NULL
   -- commented below line to fetch empty lot details
    --AND c502b_usage_lot_num                                                  IS NOT NULL 
    GROUP BY c205_part_number_id
    ORDER BY c205_part_number_id;
END gm_fch_used_lot_number;
/*********************************************************************************
* Description : This procedure is used to save the RA - Lot number
* Author     : Manikandan
* Parameters  : Order ID, RA ID, String and user id
**********************************************************************************/
PROCEDURE gm_return_lot_number_dtls (
        p_order_id  IN t501_order.c501_order_id%TYPE,
        p_trans_id  IN t502b_item_order_usage.c502b_trans_id%TYPE,
        p_input_str IN VARCHAR2,
        p_user_id   IN t502b_item_order_usage.c502b_created_by%TYPE)
AS
    v_string    VARCHAR2 ( 4000) := p_input_str;
    v_substring VARCHAR2 (4000) ;
    v_strlen    NUMBER := NVL (LENGTH (p_input_str), 0) ;
    --
    v_pnum t502b_item_order_usage.c205_part_number_id%TYPE;
    v_qty NUMBER;
    v_used_lot t502b_item_order_usage.c502b_usage_lot_num%TYPE;
    v_ddt_id t502b_item_order_usage.c502b_ref_id%TYPE;
    --
    v_cnt NUMBER;
    v_ra_reason t506_returns.c506_reason%TYPE;
    v_company_id t506_returns.c1900_company_id%TYPE;
    v_order_usage_fl VARCHAR2 (20) ;
BEGIN
     SELECT COUNT (1)
       INTO v_cnt
       FROM t412_inhouse_transactions
      WHERE c412_inhouse_trans_id = p_trans_id
       AND c412_void_fl IS NULL;
    IF v_cnt                      = 0 THEN
         SELECT c506_reason, c1900_company_id
           INTO v_ra_reason, v_company_id
           FROM t506_returns
          WHERE c506_rma_id   = p_trans_id
            AND c506_void_fl IS NULL;
    END IF;
    IF v_strlen                      > 0 THEN
        WHILE INSTR (v_string, '|') <> 0
        LOOP
            v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
            v_string    := SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
            -- to reset the values
            v_used_lot := NULL;
            v_ddt_id := NULL;
            --
            v_pnum      := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            v_substring := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
            v_qty       := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            v_substring := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;

            -- check the DDT number field available using ^ symbol
              
            IF INSTR (v_substring, '^')  <> 0
            THEN
            	v_used_lot  := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            	v_substring := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
		    	v_ddt_id   := v_substring ;
		    ELSE
		    	v_used_lot   := v_substring ;
		    END IF; -- end of ^ symbol check
		            
            IF v_cnt   = 0 THEN
                v_qty := - 1 * v_qty;
            END IF;
                
            gm_sav_usage_dtls (NULL, NULL, v_pnum, v_qty, v_used_lot, v_ddt_id, NULL, p_trans_id, NULL, p_user_id) ;
        END LOOP;
    END IF;
END gm_return_lot_number_dtls;
/*********************************************************************************
* Description : This procedure is used to save the credit RA and Usage Lot number
* Author     : Manikandan
* Parameters  : Order ID, RA ID and user id
**********************************************************************************/
PROCEDURE gm_upd_return_credit_dtls (
        p_order_id IN t501_order.c501_order_id%TYPE,
        p_trans_id IN t502b_item_order_usage.c502b_trans_id%TYPE,
        p_user_id  IN t502b_item_order_usage.c502b_created_by%TYPE)
AS
    v_order_info_id t502_item_order.c500_order_info_id%TYPE;
    v_parent_order_id t501_order.c501_order_id%TYPE;
    v_invoice_id t501_order.c503_invoice_id%TYPE;
    v_qty NUMBER;
    v_ra_reason t506_returns.c506_reason%TYPE;
    v_company_id t506_returns.c1900_company_id%TYPE;
    --
    v_usage_id t502b_item_order_usage.c502b_item_order_usage_id%TYPE;
    v_used_lot t502b_item_order_usage.c502b_usage_lot_num%TYPE;
    v_ddt_num t502b_item_order_usage.c502b_ref_id%TYPE;
    --
    CURSOR return_usage_cur
    IS
         SELECT c500_order_info_id info_id, c205_part_number_id pnum, c502_item_qty qty
          , - 1 * c502_item_qty final_qty
           FROM t502_item_order
          WHERE c501_order_id   = p_order_id
            AND c502_void_fl   IS NULL
            AND c502_delete_fl IS NULL;
    --
    CURSOR parent_order_cur (v_part_num t502b_item_order_usage.c205_part_number_id%TYPE, v_lot_num
        t502b_item_order_usage.c502b_usage_lot_num%TYPE)
    IS
         SELECT c502b_item_order_usage_id usage_id, c205_part_number_id pnum, c502b_item_qty iqty
          , c502b_usage_lot_num lot_num, c502b_ref_id ddt, c500_order_info_id order_info_id
           FROM t502b_item_order_usage
          WHERE c501_order_id                     = v_parent_order_id
            AND c205_part_number_id               = v_part_num
            AND NVL (c502b_usage_lot_num, '-999') = NVL (v_lot_num, '-999')
            AND c502b_void_fl                    IS NULL;
BEGIN
     SELECT c501_parent_order_id, c503_invoice_id
       INTO v_parent_order_id, v_invoice_id
       FROM t501_order
      WHERE c501_order_id = p_order_id
        AND c501_void_fl IS NULL;
    --
     SELECT c506_reason, c1900_company_id
       INTO v_ra_reason, v_company_id
       FROM t506_returns
      WHERE c506_rma_id   = p_trans_id
        AND c506_void_fl IS NULL;
    FOR return_usage     IN return_usage_cur
    LOOP
        BEGIN
             SELECT c502b_item_order_usage_id, c502b_usage_lot_num, c502b_ref_id ddt_num
               INTO v_usage_id, v_used_lot, v_ddt_num
               FROM t502b_item_order_usage
              WHERE c502b_trans_id      = p_trans_id
                AND c205_part_number_id = return_usage.pnum
                AND c502b_item_qty      = return_usage.qty
                AND c500_order_info_id IS NULL
                AND c502b_void_fl      IS NULL;
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_usage_id := NULL;
            v_used_lot := NULL;
            v_ddt_num  := NULL;
        END ;
        --
        gm_sav_usage_dtls (p_order_id, v_usage_id, return_usage.pnum, return_usage.qty, v_used_lot, v_ddt_num,
        return_usage.info_id, p_trans_id, NULL, p_user_id) ;
        --
        IF v_invoice_id IS NULL AND v_ra_reason <> 3312 -- Duplicate order
            THEN
            v_qty := return_usage.final_qty;
            FOR parent_ord IN parent_order_cur (return_usage.pnum, v_used_lot)
            LOOP
                IF v_qty > 0 THEN
                    IF parent_ord.iqty = v_qty THEN
                        v_qty         := parent_ord.iqty - v_qty;
                        --
                        gm_sav_usage_dtls (v_parent_order_id, parent_ord.usage_id, parent_ord.pnum, parent_ord.iqty,
                        parent_ord.lot_num, parent_ord.ddt, parent_ord.order_info_id, NULL, 'Y', p_user_id) ;
                    ELSIF (parent_ord.iqty > v_qty) THEN
                        v_qty := parent_ord.iqty - v_qty;
                        --
                        gm_sav_usage_dtls (v_parent_order_id, parent_ord.usage_id, parent_ord.pnum, v_qty,
                        parent_ord.lot_num, parent_ord.ddt, parent_ord.order_info_id, NULL, NULL, p_user_id) ;
                        --
                        v_qty := 0;
                    ELSE
                        v_qty := v_qty - parent_ord.iqty;
                        --
                        gm_sav_usage_dtls (v_parent_order_id, parent_ord.usage_id, parent_ord.pnum, parent_ord.iqty,
                        parent_ord.lot_num, parent_ord.ddt, parent_ord.order_info_id, NULL, 'Y', p_user_id) ;
                    END IF;
                END IF;-- v_qty
            END LOOP;
        END IF; -- v_invoice_id
    END LOOP;
END gm_upd_return_credit_dtls;
/*********************************************************************************
* Description : This procedure is used to update order info qty (called from remove Qty procedure)
* Author     : Manikandan
* Parameters  : Order info ID, user id
**********************************************************************************/
PROCEDURE gm_upd_order_info (
        p_order_info_id IN t500_order_info.c500_order_info_id%TYPE,
        p_user_id       IN t502b_item_order_usage.c502b_created_by%TYPE)
AS
    --
    v_item_Qty       NUMBER;
    v_order_info_qty NUMBER;
    v_tmp_qty        NUMBER;
BEGIN
    -- getting the item order Qty
     SELECT NVL (SUM (c502_item_qty), 0)
       INTO v_item_Qty
       FROM t502_item_order
      WHERE c500_order_info_id = p_order_info_id
      AND c502_void_fl IS NULL
      AND c502_delete_fl IS NULL;
    --
    BEGIN
     SELECT c500_item_qty
       INTO v_order_info_qty
       FROM t500_order_info
      WHERE c500_order_info_id = p_order_info_id
        AND c500_void_fl      IS NULL;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            v_order_info_qty := NULL;
      END;
    --
    IF v_item_Qty <> v_order_info_qty THEN
        --
        IF v_item_Qty = 0 THEN
            --
             UPDATE t500_order_info
            SET c500_void_fl           = 'Y', c500_last_updated_by = p_user_id, c500_last_updated_date = CURRENT_DATE
              WHERE c500_order_info_id = p_order_info_id
                AND c500_void_fl      IS NULL;
        ELSIF v_order_info_qty         > v_tmp_qty THEN
            --
            v_tmp_qty := v_order_info_qty - v_tmp_qty;
            --
             UPDATE t500_order_info
            SET c500_item_qty          = v_tmp_qty, c500_last_updated_by = p_user_id, c500_last_updated_date = CURRENT_DATE
              WHERE c500_order_info_id = p_order_info_id
                AND c500_void_fl      IS NULL;
            --
        END IF;
    END IF; -- end if
END gm_upd_order_info;
/*********************************************************************************
* Description : This procedure is used to remove Qty to usage table (Edit order screen)
* Author     : Manikandan
* Parameters  : Order ID, input string user id
**********************************************************************************/
PROCEDURE gm_upd_remove_qty (
        p_order_id  IN t501_order.c501_order_id%TYPE,
        p_input_str IN VARCHAR2,
        p_user_id   IN t502b_item_order_usage.c502b_created_by%TYPE)
AS
    v_string    VARCHAR2 (4000) := p_input_str;
    v_substring VARCHAR2 (4000) ;
    v_strlen    NUMBER := NVL (LENGTH (p_input_str), 0) ;
    --
    v_pnum t502b_item_order_usage.c205_part_number_id%TYPE;
    v_qty NUMBER;
    v_used_lot t502b_item_order_usage.c502b_usage_lot_num%TYPE;
    --
    v_usage_id       NUMBER;
    v_tmp_qty        NUMBER;
    v_order_info_qty NUMBER;
    --
    v_item_Qty NUMBER;
    v_tmp_pnum t502b_item_order_usage.c205_part_number_id%TYPE;
    v_tmp_lot t502b_item_order_usage.c502b_usage_lot_num%TYPE;
    v_tmp_usage_qty NUMBER;
    --
    CURSOR v_remove_lot_cur
    IS
         SELECT C501_ORDER_ID ord_id, C205_PART_NUMBER_ID pnum, C502B_ITEM_QTY qty
          , C502B_USAGE_LOT_NUM lot_num
           FROM temp_t502b_item_order_usage;
    CURSOR v_usage_cur (v_partnumber IN t5055_control_number.c205_part_number_id%TYPE, v_usage_lot IN
        temp_t502b_item_order_usage.C502B_USAGE_LOT_NUM%TYPE)
    IS
         SELECT C501_ORDER_ID ord_id, C205_PART_NUMBER_ID pnum, C502B_ITEM_QTY iqty
          , C502B_USAGE_LOT_NUM lot, C502B_REF_ID ref_id, c502b_ref_id rma_id
          , c502b_item_order_usage_id usage_id, c500_order_info_id id
           FROM t502b_item_order_usage
          WHERE c501_order_id       = p_order_id
            AND c205_part_number_id = v_partnumber
            AND C502B_USAGE_LOT_NUM = v_usage_lot
            AND c502b_void_fl      IS NULL;
BEGIN
    IF v_strlen                      > 0 THEN
    	--
    	DELETE from temp_t502b_item_order_usage;
    	--
        WHILE INSTR (v_string, '|') <> 0
        LOOP
            v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
            v_string    := SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
            --
            v_pnum      := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            v_substring := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
            v_qty       := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            v_substring := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
            v_used_lot  := v_substring;
            --
             INSERT
               INTO temp_t502b_item_order_usage
                (
                    C501_ORDER_ID, C205_PART_NUMBER_ID, C502B_ITEM_QTY
                  , C502B_USAGE_LOT_NUM, c502b_created_by, c502b_created_date
                )
                VALUES
                (
                    p_order_id, v_pnum, v_qty
                  , UPPER(TRIM(v_used_lot)), p_user_id, CURRENT_DATE
                ) ;
        END LOOP; -- v_sting
    --
    FOR v_remove_lot IN v_remove_lot_cur
    LOOP
        v_tmp_qty  := v_remove_lot.qty;
        v_tmp_pnum := v_remove_lot.pnum;
        v_tmp_lot  := v_remove_lot.lot_num;
        --
        FOR v_usage IN v_usage_cur
        (
            v_remove_lot.pnum, v_remove_lot.lot_num
        )
        LOOP
            --
            IF
                (
                    v_tmp_qty <> 0
                )
                THEN
                gm_upd_order_info
                (
                    v_usage.id, p_user_id
                )
                ;
                --
                -- getting the item order Qty
                 SELECT NVL (SUM (c502_item_qty), 0)
                   INTO v_item_Qty
                   FROM t502_item_order
                  WHERE c500_order_info_id = v_usage.id
                  AND c502_void_fl IS NULL
                  AND c502_delete_fl IS NULL;
                --
                 SELECT NVL(SUM (c502b_item_qty), 0)
                   INTO v_tmp_usage_qty
                   FROM t502b_item_order_usage
                  WHERE c500_order_info_id = v_usage.id
                   AND c502b_void_fl IS NULL;
                --
                IF v_tmp_usage_qty  <> v_item_Qty THEN
                    IF (v_usage.iqty = v_tmp_qty) THEN
                         UPDATE t502b_item_order_usage
                        SET c502b_void_fl = 'Y', c502b_last_updated_by =
                            p_user_id, c502b_last_updated_date = CURRENT_DATE
                          WHERE c502b_item_order_usage_id      = v_usage.usage_id
                            AND c502b_void_fl                 IS NULL;
                        --
                        v_tmp_qty     := v_usage.iqty - v_tmp_qty;
                    ELSIF v_usage.iqty < v_tmp_qty THEN
                        --
                        v_tmp_qty := v_tmp_qty - v_usage.iqty;
                        --
                         UPDATE t502b_item_order_usage
                        SET c502b_void_fl = 'Y', c502b_last_updated_by = p_user_id
                          , c502b_last_updated_date       = CURRENT_DATE
                          WHERE c502b_item_order_usage_id = v_usage.usage_id
                            AND c502b_void_fl            IS NULL;
                        --
                    ELSIF v_usage.iqty > v_tmp_qty THEN
                        --
                        v_tmp_qty := v_usage.iqty - v_tmp_qty;
                        --
                         UPDATE t502b_item_order_usage
                        SET c502b_item_qty = v_tmp_qty, c502b_last_updated_by = p_user_id, c502b_last_updated_date =
                            CURRENT_DATE
                          WHERE c502b_item_order_usage_id = v_usage.usage_id
                            AND c502b_void_fl            IS NULL;
                        --
                    END IF;
                END IF;
            END IF; -- v_tmp_qty
        END LOOP; -- v_usage
    END LOOP; -- v_remove_lot
    END IF; -- v_strlen
  -- to reduce the qty to t500 and t502B - usage table
  gm_upd_usage_lot_order_qty (p_order_id, p_user_id);
END gm_upd_remove_qty;
/*********************************************************************************
* Description : This procedure is used to save usage details (Issue credit/debit)
* Author     : Manikandan
* Parameters  : Invoice id
**********************************************************************************/
PROCEDURE gm_credit_invoice_update (
        p_invoice_id t503_invoice.c503_invoice_id%TYPE,
        p_user_id IN t502b_item_order_usage.c502b_created_by%TYPE)
AS
    v_order_id t501_order.c501_order_id%TYPE;
    v_parent_order_id t501_order.c501_order_id%TYPE;
    v_pnum T502_item_order.c205_part_number_id%TYPE;
    v_item_qty NUMBER;
    v_cnt      NUMBER;
    --
    CURSOR order_dtls_cur
    IS
         SELECT c501_order_id ord_id
           FROM t501_order
          WHERE c503_invoice_id = p_invoice_id
            AND c501_void_fl   IS NULL;
BEGIN
    FOR order_dtls IN order_dtls_cur
    LOOP
        v_order_id := order_dtls.ord_id;
        --1. insert the data to t500 and t502b  table
        gm_pkg_cs_usage_lot_number.gm_sav_order_info (order_dtls.ord_id, 'Y', p_user_id) ;
        --
    END LOOP;
END gm_credit_invoice_update;
/*********************************************************************************
* Description : This procedure is used to remove Qty to usage table (Edit order screen)
* Author     : Manikandan
* Parameters  : Order info ID, qty user id
**********************************************************************************/
PROCEDURE gm_upd_backorder_qty (
        p_order_info_id IN t500_order_info.c500_order_info_id%TYPE,
        p_qty           IN NUMBER,
        p_user_id       IN t502b_item_order_usage.c502b_created_by%TYPE)
AS
    v_order_info_qty NUMBER;
    v_tmp_qty        NUMBER;
    CURSOR order_usage_cur
    IS
         SELECT c502b_item_order_usage_id usage_id, c500_order_info_id info_id, c205_part_number_id pnum
          , c502b_item_qty qty, c501_order_id ord_id, c502b_usage_lot_num lot, c502b_ref_id ref_id
           FROM t502b_item_order_usage
          WHERE c500_order_info_id = p_order_info_id
            AND c502b_void_fl     IS NULL;
BEGIN
     SELECT c500_item_qty
       INTO v_order_info_qty
       FROM t500_order_info
      WHERE c500_order_info_id = p_order_info_id
        AND c500_void_fl      IS NULL;
    --
    IF v_order_info_qty = p_qty THEN
         UPDATE t500_order_info
        SET c500_void_fl           = 'Y', c500_last_updated_by = p_user_id, c500_last_updated_date = CURRENT_DATE
          WHERE c500_order_info_id = p_order_info_id
            AND c500_void_fl      IS NULL;
    ELSIF v_order_info_qty         > p_qty THEN
         UPDATE t500_order_info
        SET c500_item_qty          = c500_item_qty - TO_NUMBER (p_qty),
        	c500_last_updated_by = p_user_id,
        	c500_last_updated_date = CURRENT_DATE
          WHERE c500_order_info_id = p_order_info_id
            AND c500_void_fl      IS NULL;
        --
    END IF;
    --
    v_tmp_qty       := p_qty;
    --
    DELETE FROM temp_t502b_ITEM_ORDER_USAGE;
    --
    FOR order_usage IN order_usage_cur
    LOOP
        IF v_tmp_qty           > 0 THEN
            IF order_usage.qty = v_tmp_qty THEN
            	-- insert to temp table.
				gm_pkg_cs_usage_lot_number.gm_sav_temp_order_usage (order_usage.usage_id, order_usage.ord_id, order_usage.pnum,
				order_usage.qty, order_usage.lot, order_usage.ref_id, order_usage.info_id) ;
       			--
                 UPDATE t502b_item_order_usage
                SET c502b_void_fl                 = 'Y', c502b_last_updated_by = p_user_id, c502b_last_updated_date = CURRENT_DATE
                  WHERE c502b_item_order_usage_id = order_usage.usage_id
                    AND c502b_void_fl            IS NULL;
                --
                v_tmp_qty        := 0;
            ELSIF order_usage.qty > v_tmp_qty THEN
            	-- insert to temp table.
				gm_pkg_cs_usage_lot_number.gm_sav_temp_order_usage (order_usage.usage_id, order_usage.ord_id, order_usage.pnum,
				v_tmp_qty, order_usage.lot, order_usage.ref_id, order_usage.info_id) ;
				--
                v_tmp_qty        := order_usage.qty - v_tmp_qty;
             	--   
                 UPDATE t502b_item_order_usage
                SET c502b_item_qty = v_tmp_qty, c502b_last_updated_by = p_user_id, c502b_last_updated_date =
                    CURRENT_DATE
                  WHERE c502b_item_order_usage_id = order_usage.usage_id
                    AND c502b_void_fl            IS NULL;
             --
            ELSE
                v_tmp_qty := v_tmp_qty - order_usage.qty;
                --
                 UPDATE t502b_item_order_usage
                SET c502b_void_fl                 = 'Y', c502b_last_updated_by = p_user_id, c502b_last_updated_date = CURRENT_DATE
                  WHERE c502b_item_order_usage_id = order_usage.usage_id
                    AND c502b_void_fl            IS NULL;
                 -- insert to temp table.
					gm_pkg_cs_usage_lot_number.gm_sav_temp_order_usage (order_usage.usage_id, order_usage.ord_id, order_usage.pnum,
					order_usage.qty, order_usage.lot, order_usage.ref_id, order_usage.info_id) ;
            END IF;
        END IF; -- v_tmp_qty
    END LOOP;
    --
END gm_upd_backorder_qty;
--
/*********************************************************************************
* Description : This procedure is used to syn the item order price to order info and usage table
* Author     : Manikandan
* Parameters  : Order ID, User ID
**********************************************************************************/
PROCEDURE gm_upd_price (
        p_order_id IN t501_order.c501_order_id%TYPE,
        p_user_id  IN t501_order.c501_created_by%TYPE)
AS
    v_info_price t500_order_info.c500_item_price%TYPE;
    v_order_info_cnt NUMBER;
    v_info_seq_num t500_order_info.c500_order_info_id%TYPE;
    v_usage_ord_qty NUMBER;
    v_temp_qty      NUMBER;
    v_avaiable_qty  NUMBER;
    --
    CURSOR item_order_cur
    IS
         SELECT c501_order_id ord_id, c205_part_number_id pnum, c502_item_price item_price
          , c502_item_qty qty, c500_order_info_id infoid, c502_item_order_id item_order_id
           FROM t502_item_order
          WHERE c501_order_id   = p_order_id
            AND c502_delete_fl IS NULL
            AND c502_void_fl   IS NULL;
    --
    CURSOR item_group_cur
    IS
         SELECT c501_order_id order_id, c205_part_number_id pnum, SUM (c502_item_qty) qty
          , c502_item_price price, c901_unit_price_adj_code adj_code, c500_order_info_id info_id
           FROM t502_item_order
          WHERE c501_order_id   = p_order_id
            AND c502_delete_fl IS NULL
            AND c502_void_fl   IS NULL
       GROUP BY c501_order_id, c205_part_number_id, c502_item_price
          , c901_unit_price_adj_code, c500_order_info_id
       ORDER BY c501_order_id, c205_part_number_id, qty;
    --
    CURSOR cur_tmp_usage (v_partnumber IN t5055_control_number.c205_part_number_id%TYPE)
    IS
         SELECT C501_ORDER_ID ord_id, C205_PART_NUMBER_ID pnum, C502B_ITEM_QTY iqty
          , C502B_USAGE_LOT_NUM lot, C502B_REF_ID ref_id, c502b_ref_id rma_id
          , c502b_item_order_usage_id usage_id, c500_order_info_id id
           FROM temp_t502b_item_order_usage
          WHERE c501_order_id       = p_order_id
            AND c205_part_number_id = v_partnumber
            AND c502b_item_qty     <> 0
            AND c502b_void_fl      IS NULL
       ORDER BY C502B_ITEM_QTY;
    --
    CURSOR remove_info_cur
    IS
         SELECT c500_order_info_id id
           FROM t500_order_info
          WHERE c501_order_id = p_order_id
            AND c500_void_fl IS NULL
      MINUS
     SELECT c500_order_info_id id
       FROM t502_item_order
      WHERE c501_order_id = p_order_id
        AND c502_void_fl IS NULL;
BEGIN
    -- to save T500 table
    FOR item_order IN item_order_cur
    LOOP
         SELECT c500_item_price
           INTO v_info_price
           FROM t500_order_info
          WHERE c500_order_info_id = item_order.infoid
            AND c500_void_fl      IS NULL;
        IF item_order.item_price  <> v_info_price THEN
            --
             SELECT s500_order_info.nextval INTO v_info_seq_num FROM dual;
            --
             INSERT
               INTO t500_order_info
                (
                    c500_order_info_id, c501_order_id, c205_part_number_id
                  , c500_item_qty, c500_item_price, c500_created_by
                  , c500_created_date
                )
                VALUES
                (
                    v_info_seq_num, item_order.ord_id, item_order.pnum
                  , item_order.qty, item_order.item_price, p_user_id
                  , CURRENT_DATE
                ) ;
            --
             UPDATE t502_item_order
            SET c500_order_info_id     = v_info_seq_num
              WHERE c502_item_order_id = item_order.item_order_id
                AND c502_delete_fl    IS NULL
                AND c502_void_fl      IS NULL;
        END IF ;
    END LOOP ;
    -- to insert T502B
    --1 insert the data to global table.
     INSERT
       INTO temp_t502b_ITEM_ORDER_USAGE
     SELECT *
       FROM t502b_item_order_usage
      WHERE c501_order_id  = p_order_id
        AND c502b_void_fl IS NULL;
    -- 2 delete the data to orginal table (t502b)
     DELETE
       FROM t502b_item_order_usage
      WHERE c501_order_id  = p_order_id
        AND c502b_void_fl IS NULL;
    --
    --1 Loop the item order table
    FOR item_order IN item_group_cur
    LOOP
        v_temp_qty := item_order.qty;
        -- loop the temp table - part wise
        FOR tmp_usage IN cur_tmp_usage (item_order.pnum)
        LOOP
            --1.1 Qty check if equal to 0 then exit the loop
            v_usage_ord_qty := tmp_usage.iqty;
            --
            IF v_usage_ord_qty <= 0 OR v_temp_qty <= 0 THEN
                EXIT;
            END IF;
            -- greater then temp tabel qty (for example t502 qty is 2 and temp qty is 5)
            IF v_temp_qty <= v_usage_ord_qty THEN
                --
                -- reduce the temp table qty (temp - t502 qty)
                 UPDATE temp_t502b_item_order_usage
                SET c502b_item_qty                = v_usage_ord_qty - v_temp_qty, c502b_last_updated_by = p_user_id,
                    c502b_last_updated_date       = CURRENT_DATE
                  WHERE c502b_item_order_usage_id = tmp_usage.usage_id
                    AND c502b_void_fl            IS NULL;
                --
                v_avaiable_qty := v_temp_qty;
                --DBMS_OUTPUT.PUT_LINE ('Inside IF ' || 'v_avaiable_qty '||v_avaiable_qty) ;
            ELSE
                -- order qty is bigger then
                 UPDATE temp_t502b_item_order_usage
                SET c502b_item_qty                = 0, c502b_last_updated_by = p_user_id, c502b_last_updated_date = CURRENT_DATE
                  WHERE c502b_item_order_usage_id = tmp_usage.usage_id
                    AND c502b_void_fl            IS NULL;
                --
                v_avaiable_qty := v_usage_ord_qty;
            END IF;
            -- insert the data to orginal table
            gm_sav_usage_dtls (tmp_usage.ord_id, NULL, tmp_usage.pnum, v_avaiable_qty, tmp_usage.lot, tmp_usage.ref_id, item_order.info_id, NULL
                , NULL, p_user_id) ;
            v_temp_qty := v_temp_qty - v_avaiable_qty;
        END LOOP; -- temp_table.
    END LOOP ; -- Item order table.
    -- to void the order info (when change the same row price)
    FOR remove_info IN remove_info_cur
    LOOP
         UPDATE t500_order_info
        SET c500_void_fl           = 'Y', c500_last_updated_by = p_user_id, c500_last_updated_date = CURRENT_DATE
          WHERE c500_order_info_id = remove_info.id
            AND c500_void_fl      IS NULL;
    END LOOP;
END gm_upd_price;
	/*********************************************************************************
	* Description : This procedure is used to sync back order usage lot number
	* Author     : Manikandan
	* Parameters  : Order ID, Order info ID, user id
	**********************************************************************************/
PROCEDURE gm_sync_back_order_usage_lot (
        p_order_id      IN T501_order.c501_order_id%TYPE,
        p_order_info_id IN t500_order_info.c500_order_info_id%TYPE,
        p_user_id       IN t502b_item_order_usage.c502b_created_by%TYPE)
AS
	--
	v_cnt NUMBER := 0;
	v_back_order_info_id t502b_item_order_usage.c500_order_info_id%TYPE;
	--
    CURSOR v_item_usage_cur
    IS
         SELECT C501_ORDER_ID ord_id, c502b_item_order_usage_id usage_id, c205_part_number_id pnum
          , c502b_item_qty qty, c502b_usage_lot_num lot, c502b_ref_id ddt
           FROM temp_t502b_ITEM_ORDER_USAGE
          WHERE c500_order_info_id = p_order_info_id;
BEGIN
	-- to geting the back order info id - using order id.
	 SELECT c500_order_info_id
   INTO v_back_order_info_id
   FROM t502_item_order
  WHERE c501_order_id   = p_order_id
    AND c502_void_fl   IS NULL
    AND c502_delete_fl IS NULL;
    --
    FOR v_item_usage IN v_item_usage_cur
    LOOP
        IF v_cnt = 0 THEN
             UPDATE t502b_item_order_usage
            SET c502b_item_qty                          = v_item_usage.qty, c502b_usage_lot_num = UPPER(v_item_usage.lot), c502b_ref_id =
                v_item_usage.ddt, c502b_last_updated_by = p_user_id, c502b_last_updated_date = CURRENT_DATE
              WHERE c501_order_id                       = p_order_id
                AND c502b_void_fl                      IS NULL;
        ELSE
            gm_sav_usage_dtls (p_order_id, null, v_item_usage.pnum, v_item_usage.qty,
            v_item_usage.lot, v_item_usage.ddt, v_back_order_info_id, NULL, NULL, p_user_id) ;
        END IF;
        v_cnt := v_cnt + 1;
    END LOOP ;
END gm_sync_back_order_usage_lot;
	/*********************************************************************************
	* Description : This procedure is used to save the values to global table (T502b)
	* Author     : Manikandan
	* Parameters  : Order ID, Order info ID, user id
	**********************************************************************************/
PROCEDURE gm_sav_temp_order_usage (
        p_order_usage_id IN t502b_item_order_usage.c502b_item_order_usage_id%TYPE,
        p_order_id       IN t502b_item_order_usage.c501_order_id%TYPE,
        p_pnum           IN t502b_item_order_usage.c205_part_number_id%TYPE,
        p_qty            IN t502b_item_order_usage.c502b_item_qty%TYPE,
        p_lot_num        IN t502b_item_order_usage.c502b_usage_lot_num%TYPE,
        p_ref_id         IN t502b_item_order_usage.c502b_ref_id%TYPE,
        p_order_info_id  IN t500_order_info.c500_order_info_id%TYPE)
AS
BEGIN
     INSERT
       INTO temp_t502b_ITEM_ORDER_USAGE
        (
            C502B_ITEM_ORDER_USAGE_ID, C501_ORDER_ID, C205_PART_NUMBER_ID
          , C502B_ITEM_QTY, C502B_USAGE_LOT_NUM, C502B_REF_ID
          , C500_ORDER_INFO_ID
        )
        VALUES
        (
            p_order_usage_id, p_order_id, p_pnum
          , p_qty, UPPER(TRIM(p_lot_num)), p_ref_id
          , p_order_info_id
        ) ;
END gm_sav_temp_order_usage;
		/* Description  : This Function Returns the sum Qty For The Selected Order
        * Parameters  : Usage id
        */
FUNCTION get_italy_invoice_part_qty (
            p_order_usage_id t502b_item_order_usage.c502b_item_order_usage_id%TYPE)
        RETURN NUMBER
    IS
    	--
        v_qty       NUMBER;
        v_returnqty NUMBER;
        v_actualqty NUMBER;
        v_part_num t502_item_order.c205_part_number_id%TYPE;
        v_price t502_item_order.c502_item_price%TYPE;
        v_order_id t502b_item_order_usage.c501_order_id%TYPE ;
        v_ref_id t502b_item_order_usage.c502b_ref_id%TYPE;
        v_lot_number t502b_item_order_usage.c502b_usage_lot_num%TYPE;
        v_order_info_id t500_order_info.c500_order_info_id%TYPE;
        v_invoiceid t501_order.c503_invoice_id%TYPE;
        --
    BEGIN
        BEGIN
             SELECT c205_part_number_id, c501_order_id, NVL (c502b_ref_id, '-999')
              , NVL (c502b_usage_lot_num, '-999'), c500_order_info_id
               INTO v_part_num, v_order_id, v_ref_id
              , v_lot_number, v_order_info_id
               FROM t502b_item_order_usage
              WHERE c502b_item_order_usage_id = p_order_usage_id
                AND c502b_void_fl            IS NULL;
            --
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_part_num      := NULL;
            v_order_id      := NULL;
            v_ref_id        := NULL;
            v_lot_number    := NULL;
            v_order_info_id := NULL;
        END;
        --
        BEGIN
             SELECT c500_item_price
               INTO v_price
               FROM t500_order_info
              WHERE c500_order_info_id = v_order_info_id
                AND c500_void_fl      IS NULL;
            --
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_price := NULL;
        END;
        --
         SELECT SUM (c502b_item_qty)
           INTO v_actualqty
           FROM t502b_item_order_usage t502b
          WHERE c501_order_id                     = v_order_id
            AND c205_part_number_id               = v_part_num
            AND NVL (c502b_ref_id, '-999')        = v_ref_id
            AND NVL (c502b_usage_lot_num, '-999') = v_lot_number
            AND EXISTS
            (
                 SELECT c500_order_info_id
                   FROM t500_order_info t500
                  WHERE t502b.c500_order_info_id = t500.c500_order_info_id
                    AND t500.c501_order_id       = v_order_id
                    AND c205_part_number_id      = v_part_num
                    AND c500_item_price          = v_price
                    AND c500_void_fl            IS NULL
            )
            AND c502b_void_fl IS NULL;
        --
        -- To get invoice id from order id
           SELECT t501.c503_invoice_id
	  	   INTO v_invoiceid
	  	   FROM t501_order t501
	 	   WHERE t501.c501_order_id = v_order_id;
        
    --  To get the return QTY for the selected part and for the selected order
	
         SELECT NVL (SUM (t502b.c502b_item_qty), 0) 
           INTO v_returnqty
           FROM t502b_item_order_usage t502b,t501_order t501
          WHERE 
            t501.c501_parent_order_id  = v_order_id
            AND t501.c501_parent_order_id <>
			      t501.c501_order_id	-- added this condition so that backorder which have the same order id as the parent order id doesnt get pulled up
            AND t501.c501_order_id                     = t502b.c501_order_id
            AND t502b.c205_part_number_id               = v_part_num
            AND NVL (t502b.c502b_ref_id, '-999')        = v_ref_id
            AND NVL (t502b.c502b_usage_lot_num, '-999') = v_lot_number
            AND EXISTS
            (
                 SELECT c500_order_info_id
                   FROM t500_order_info t500
                  WHERE t502b.c500_order_info_id = t500.c500_order_info_id
                    AND t500.c501_order_id       = t502b.c501_order_id
                    AND c205_part_number_id      = v_part_num
                    AND c500_item_price          = v_price
                    AND c500_void_fl            IS NULL
            )
            AND t501.c503_invoice_id = v_invoiceid
            AND t502b.c502b_void_fl IS NULL
            AND t501.c501_void_fl IS NULL
            AND NVL (c901_order_type, 0) = 2529; -- Sales adjustment
        
        
        v_qty := v_returnqty + v_actualqty;
        RETURN v_qty;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN 0;
    END get_italy_invoice_part_qty;
--
	 /*********************************************************************************
		* Description : This procedure is used to reduce the Qty in T500 and T502b table
		* Author     : Manikandan
		* Parameters  : Order ID, user id
		**********************************************************************************/
PROCEDURE gm_upd_usage_lot_order_qty (
        p_order_id IN T501_order.c501_order_id%TYPE,
        p_user_id  IN t502b_item_order_usage.c502b_created_by%TYPE)
AS
    v_item_qty   NUMBER;
    v_usage_qty  NUMBER;
    v_usage_cnt  NUMBER := 0;
    v_return_qty NUMBER;
    v_order_id T501_order.c501_order_id%TYPE;
    v_invoice_id T501_order.c503_invoice_id%TYPE;
    --
    CURSOR v_item_order_cur
    IS
         SELECT c205_part_number_id pnum, c502_item_qty iqty, c500_order_info_id info_id
           FROM t502_item_order
          WHERE c501_order_id   = v_order_id
            AND c502_void_fl   IS NULL
            AND c502_delete_fl IS NULL;
    CURSOR v_item_remove_cur
    IS
         SELECT c500_order_info_id info_id
           FROM t502b_item_order_usage
          WHERE c501_order_id  = v_order_id
            AND c502b_void_fl IS NULL
      MINUS
     SELECT c500_order_info_id info_id
       FROM t502_item_order
      WHERE c501_order_id   = v_order_id
        AND c502_void_fl   IS NULL
        AND c502_delete_fl IS NULL;
BEGIN
    --
    v_order_id := p_order_id;
    --
    FOR v_item_order IN v_item_order_cur
    LOOP
        --
         SELECT NVL (SUM (c502_item_qty), 0)
           INTO v_item_qty
           FROM t502_item_order
          WHERE c500_order_info_id = v_item_order.info_id
            AND c502_void_fl      IS NULL
            AND c502_delete_fl    IS NULL;
        --
         SELECT NVL (SUM (t507.c507_item_qty), 0)
           INTO v_return_qty
           FROM t506_returns t506, t507_returns_item t507
          WHERE t506.c506_rma_id         = t507.c506_rma_id
            AND t506.c501_order_id       = v_order_id
            AND t507.c205_part_number_id = v_item_order.pnum
            AND c506_void_fl            IS NULL;
        --
        v_item_qty := v_item_qty - v_return_qty;
        --
         SELECT NVL (SUM (c502b_item_qty), 0)
           INTO v_usage_qty
           FROM t502b_item_order_usage
          WHERE c500_order_info_id = v_item_order.info_id
            AND c502b_void_fl     IS NULL;
        -- to reduce the Qty to T500 order infor table
        gm_upd_order_info (v_item_order.info_id, p_user_id) ;
        --
        IF v_item_qty   < v_usage_qty THEN
            v_item_qty := v_usage_qty - v_item_qty;
            gm_pkg_cs_usage_lot_number.gm_upd_backorder_qty (v_item_order.info_id, v_item_qty, p_user_id) ;
        END IF;
    END LOOP;
    -- IF part fully removed from T502 then to remove the parts to T500 and T502 table.
    FOR v_item_remove IN v_item_remove_cur
    LOOP
        -- to reduce the Qty to T500 order infor table
        gm_upd_order_info (v_item_remove.info_id, p_user_id) ;
        -- to void the T502B data
         UPDATE t502b_item_order_usage
        SET c502b_void_fl      = 'Y', c502b_last_updated_by = p_user_id, c502b_last_updated_date = CURRENT_DATE
          WHERE c500_order_info_id  = v_item_remove.info_id
            AND c502b_void_fl IS NULL;
    END LOOP;
END gm_upd_usage_lot_order_qty;

	/*********************************************************************************
	* Description : This procedure is used to update the control number error message T502B.
	* Author     : karthik
	* Parameters  : orderid,Lot,part#,usageID
	**********************************************************************************/
	PROCEDURE gm_sav_usage_error_dtls (
	 p_order_id t501_order.c501_order_id%TYPE,
	 p_usage_lot IN T502B_ITEM_ORDER_USAGE.C502B_USAGE_LOT_NUM%TYPE,
	 p_part_num  IN T502B_ITEM_ORDER_USAGE.C205_PART_NUMBER_ID%TYPE,
	 p_usage_dtls_id IN T502B_ITEM_ORDER_USAGE.C502B_ITEM_ORDER_USAGE_ID%TYPE )
	 
	 AS
	    v_usage_lot T502B_ITEM_ORDER_USAGE.C502B_USAGE_LOT_NUM%TYPE := UPPER(p_usage_lot);
	    v_usage_rep T703_SALES_REP.C703_SALES_REP_ID%TYPE;
	    v_actual_rep T703_SALES_REP.C703_SALES_REP_ID%TYPE;
	    v_usage_rep_name T703_SALES_REP.C703_SALES_REP_NAME%TYPE;
	    v_lot_discrepency VARCHAR2(3);
	    v_lot_error_msg    VARCHAR2(200);
	    v_company_id	t1900_company.c1900_company_id%TYPE;
        v_flag VARCHAR2(1);
        v_shipped_date T907_SHIPPING_INFO.c907_ship_to_dt%TYPE;
	 BEGIN
		   SELECT  NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL'))
			   INTO v_company_id
			  FROM DUAL;
			  
	   --validating the company is required for lot track
	   SELECT get_rule_value(v_company_id,'LOT_TRACK') INTO v_flag FROM DUAL; 
	   
	   IF v_flag IS NULL OR v_flag <> 'Y' 
	   THEN
		   RETURN;
	   END IF;
	   
		  --validating the part is required to track lot		  	
	   SELECT get_part_attr_value_by_comp(p_part_num,'104480',v_company_id) INTO v_flag FROM DUAL;   
	   
	   IF v_flag IS NULL OR v_flag <> 'Y' 
	   THEN
		   RETURN;
	   END IF;
	   
	  	 /* Get the rep id from the current Order*/
	   		BEGIN
				SELECT C703_SALES_REP_id 
					INTO  v_actual_rep
				FROM T501_ORDER
				 WHERE C501_ORDER_ID = p_order_id
				 AND C501_VOID_FL  IS NULL;
			  EXCEPTION
            WHEN NO_DATA_FOUND THEN
                v_actual_rep := NULL;
            END;
            
         /* Get the rep id of the lot consigned*/

			BEGIN	
			  SELECT REPID , SHIPPEDDATE 
			   INTO v_usage_rep ,v_shipped_date
			    FROM ( 
				 SELECT REPID , SHIPPEDDATE 
						FROM
						  (
						  SELECT T703.C703_SALES_REP_ID REPID ,
						    T907.C907_SHIP_TO_DT SHIPPEDDATE
						  FROM T505_ITEM_CONSIGNMENT T505,
						    T504_CONSIGNMENT T504 ,
						    T703_SALES_REP T703 ,
						    T907_SHIPPING_INFO T907
						  WHERE T505.C504_CONSIGNMENT_ID = T504.C504_CONSIGNMENT_ID
						  AND T504.C504_CONSIGNMENT_ID   = T907.C907_REF_ID
						  AND T505.c505_control_number   = UPPER(v_usage_lot)
						  AND T504.C504_VOID_FL         IS NULL
						  AND T504.C504_TYPE             = '4110' --consignment type
						  AND T504.C504_STATUS_FL        = '4'
						  AND T504.C701_DISTRIBUTOR_ID   = T703.C701_DISTRIBUTOR_ID
						  AND T703.C703_VOID_FL         IS NULL
						  AND T907.c907_void_FL         IS NULL
						  UNION ALL
						  SELECT T501.C703_SALES_REP_ID REPID ,
						    T907.C907_SHIP_TO_DT SHIPPEDDATE
						  FROM T501_ORDER T501 ,
						    T502_ITEM_ORDER T502 ,
						    T907_SHIPPING_INFO t907
						  WHERE T501.C501_ORDER_ID     = T502.C501_ORDER_ID
						  AND T501.C501_ORDER_ID       = T907.C907_REF_ID
						  AND T502.C502_CONTROL_NUMBER = UPPER(v_usage_lot)
						  AND T501.C501_VOID_FL       IS NULL
						  AND T907.c907_void_FL       IS NULL
						  AND T501.C501_STATUS_FL      = '3'
						  )
						ORDER BY SHIPPEDDATE DESC
						)
				WHERE ROWNUM = 1;
				EXCEPTION
             WHEN NO_DATA_FOUND THEN
                v_usage_rep := NULL;
                v_shipped_date := NULL;
            END;
				
            v_usage_rep_name:=  GET_REP_NAME(v_usage_rep);
            
			/* The lot is shipped to one rep,but the same lot is used in surgery by some other rep
			 * At that time we are storing the below error message and lot discrepency flag as "Y" in T502B_ITEM_ORDER_USAGE table */	
				
		IF (v_actual_rep != v_usage_rep) THEN
			v_lot_discrepency := 'Y';
			v_lot_error_msg := 'Allograft ID ' || v_usage_lot || ' is currently consigned to another Field Sales. ' ;
			
			 UPDATE T502B_ITEM_ORDER_USAGE
	  			SET c502b_usage_lot_discripency = v_lot_discrepency,
	  		c502b_usage_lot_errormsg = v_lot_error_msg
	  		WHERE c501_order_id = p_order_id
	  		AND c205_part_number_id = p_part_num
	  		AND c502b_usage_lot_num = v_usage_lot ;
		END IF;
		
	END gm_sav_usage_error_dtls;
	
	/*************************************************************************************************************************************
	* Description : This procedure is used to update the invalid control number error message T502B while the user give the wrong lot from IPAD
	* Author     : karthik
	* Parameters  : Lot,part#,orderid
	****************************************************************************************************************************************/
	PROCEDURE gm_sav_invalid_usage_lot_dtls (
	 p_usage_lot IN T502B_ITEM_ORDER_USAGE.C502B_USAGE_LOT_NUM%TYPE,
	 p_part_num  IN T502B_ITEM_ORDER_USAGE.C205_PART_NUMBER_ID%TYPE,
	 p_order_id IN t501_order.c501_order_id%TYPE )
	  
	  AS
	    v_usage_lot T502B_ITEM_ORDER_USAGE.C502B_USAGE_LOT_NUM%TYPE := UPPER(p_usage_lot);
	    v_lot_discrepency VARCHAR2(3);
	    v_lot_error_msg    VARCHAR2(200);
	    v_lot_count NUMBER;
	  BEGIN		  

/*if the rep put the invalid lot from ipad while booking DO,then we are saving the error message in T502B_ITEM_ORDER_USAGE table 
 * with the discripency flag as "Y" */
 
		 SELECT COUNT (1)
			  INTO v_lot_count
			FROM t2550_part_control_number
			WHERE c205_part_number_id = p_part_num
			AND UPPER (TRIM (c2550_control_number)) = v_usage_lot;
			
		  IF (v_lot_count = 0) THEN
				v_lot_discrepency := 'Y';
				v_lot_error_msg := 'The Control Number ' || v_usage_lot || ' entered for the part number ' || p_part_num || ' is invalid' ;
			
			 UPDATE T502B_ITEM_ORDER_USAGE
	  			SET c502b_usage_lot_discripency = v_lot_discrepency,
	  		c502b_usage_lot_errormsg = v_lot_error_msg
	  		WHERE c501_order_id = p_order_id
	  		AND c205_part_number_id = p_part_num
	  		AND c502b_usage_lot_num = v_usage_lot ;
	  		
		  END IF;
			
	END gm_sav_invalid_usage_lot_dtls;

	/*************************************************************************************************************************************
	* Description : This procedure is used to check the lot is already billed
	* Author     : Dsandeep
	****************************************************************************************************************************************/
	PROCEDURE gm_save_billed_lot (
	 p_order_id t501_order.c501_order_id%TYPE,
	 p_pnum IN T502B_ITEM_ORDER_USAGE.C502B_USAGE_LOT_NUM%TYPE,
	 p_cnum  IN T502B_ITEM_ORDER_USAGE.C205_PART_NUMBER_ID%TYPE
     )
        
  AS   
        v_usage_lot T502B_ITEM_ORDER_USAGE.C502B_USAGE_LOT_NUM%TYPE := UPPER(p_cnum);
	    v_company_id	t1900_company.c1900_company_id%TYPE;
        v_flag VARCHAR2(1);
        v_lot_discrepency VARCHAR2(3);
	    v_lot_error_msg    VARCHAR2(200);
	 BEGIN
		   SELECT  NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL'))
			   INTO v_company_id
			  FROM DUAL;
	   --validating the company is required for lot track
	   SELECT get_rule_value(v_company_id,'LOT_TRACK') INTO v_flag FROM DUAL; 
	   
	   IF v_flag IS NULL OR v_flag <> 'Y' 
	   THEN
		   RETURN;
	   END IF;
		  --validating the part is required to track lot	
		  	
	  	
	   SELECT get_part_attr_value_by_comp(p_pnum,'104480',v_company_id) INTO v_flag FROM DUAL;   
      
	   IF v_flag IS NULL OR v_flag <> 'Y' 
	   THEN
		   RETURN;
	   END IF;
	   
	    /*call te below prc to get the error message if the lot is already billed in some order*/
		 gm_fch_billed_loterror_msg(p_pnum ,p_cnum ,v_lot_error_msg );
        
	IF v_lot_error_msg IS NOT NULL 
        THEN
        v_lot_discrepency := 'Y';
         UPDATE T502B_ITEM_ORDER_USAGE
	  			SET c502b_usage_lot_discripency = v_lot_discrepency,
	  		c502b_usage_lot_errormsg = v_lot_error_msg
	  		WHERE c501_order_id = p_order_id
	  		AND c205_part_number_id = p_pnum
	  		AND c502b_usage_lot_num = v_usage_lot ;
		  END IF;
		          
END gm_save_billed_lot;

/*************************************************************************************************************
	* Description : This procedure is used to fetch the error message that the lot is already billed
	* Author     : karthiks
	***********************************************************************************************************/
PROCEDURE gm_fch_billed_loterror_msg (
	 p_pnum          IN T502B_ITEM_ORDER_USAGE.C502B_USAGE_LOT_NUM%TYPE,
	 p_cnum          IN T502B_ITEM_ORDER_USAGE.C205_PART_NUMBER_ID%TYPE,
	 p_error_message OUT  VARCHAR2
     )
        
  AS  
        v_order_id t501_order.c501_order_id%TYPE;
  BEGIN
	  
	   BEGIN
		   
		SELECT ORDERID  
			INTO v_order_id  
		  FROM
		  (   
	         SELECT t501.c501_order_id ORDERID
	           FROM t2550_part_control_number t2550 , T501_ORDER t501 , T502_ITEM_ORDER t502 , T907_SHIPPING_INFO T907
	                WHERE t2550.C205_PART_NUMBER_ID = p_pnum
	                AND UPPER (TRIM (t2550.C2550_CONTROL_NUMBER)) = UPPER(p_cnum)
	         AND t2550.C2550_LOT_STATUS IN('105006','105000') --sold and impanted
	         AND T502.C502_CONTROL_NUMBER = T2550.C2550_CONTROL_NUMBER
	         AND T502.C205_PART_NUMBER_ID = T2550.C205_PART_NUMBER_ID
	         AND T501.c501_order_id = T502.c501_order_id
	         AND T501.C501_ORDER_ID       = T907.C907_REF_ID
	         AND T907.c907_void_FL       IS NULL
	         AND T501.C501_VOID_FL       IS NULL
	          ORDER BY T907.c907_ship_to_dt DESC
	          )
	         WHERE ROWNUM = 1;
			
          EXCEPTION
    WHEN NO_DATA_FOUND THEN
           v_order_id := NULL;
		END;
		
		IF v_order_id IS NOT NULL 
         THEN
           p_error_message  := ' Allograft ID ' || UPPER(p_cnum) || ' has already been billed in order ' || v_order_id ;
         ELSE
            p_error_message := NULL;
		END IF;
        
		
	  END gm_fch_billed_loterror_msg;
	  
   /*************************************************************************************************************
	* Description : This procedure is used to update order id and order info id in sales adjustment order usage lot details.
	* Author     : Mahavishnu
	***********************************************************************************************************/
	 PROCEDURE gm_upd_return_order_dtls (
	 p_par_order_id IN t501_order.c501_order_id%TYPE,
	 p_order_id     IN t501_order.c501_order_id%TYPE,
	 p_trans_id     IN T502B_ITEM_ORDER_USAGE.C502B_TRANS_ID%TYPE,
	 p_user_id      IN t501_order.c501_created_by%TYPE
	  )
	 
	 AS
	v_part_num   t502b_item_order_usage.c205_part_number_id%TYPE;
	v_item_price t500_order_info.C500_ITEM_QTY%TYPE;
	v_order_info_id  t500_order_info.c500_order_info_id%TYPE;
	v_qty t502_item_order.c502_item_qty%TYPE;
	v_lot t502b_item_order_usage.c502b_usage_lot_num%TYPE;
	
	-- Cursor to fetch item order details 
	CURSOR item_order_cur 
   		 IS
   		  SELECT c500_order_info_id info_id, c205_part_number_id ipnum, c502_item_price iprice,c502_item_qty iqty
           FROM t502_item_order
          WHERE c501_order_id   = p_order_id
            AND c502_void_fl   IS NULL
            AND c502_delete_fl IS NULL;
            
            -- Cursor to fetch lot usage details by transaction id and part number 
      CURSOR lot_usage_cur (v_part_num t502b_item_order_usage.c205_part_number_id%TYPE)
    IS
         SELECT c502b_item_order_usage_id usage_id, c205_part_number_id pnum
          , c502b_usage_lot_num lot_num, C502b_Item_Qty u_item_qty
           FROM t502b_item_order_usage
          WHERE  C502B_TRANS_ID = p_trans_id 
            AND c205_part_number_id               = v_part_num
            AND c501_order_id IS NULL
            AND c502b_void_fl                    IS NULL;
            
        -- cursor to fetch item price from parent order by part number and used lot  number
        CURSOR price_cur (v_part_num t502b_item_order_usage.c205_part_number_id%TYPE,v_lot t502b_item_order_usage.c502b_usage_lot_num%TYPE)
           IS
           	SELECT  t502.C502_ITEM_PRICE v_item_price
   		  	FROM T502B_ITEM_ORDER_USAGE t502b, T500_ORDER_INFO t500,T502_ITEM_ORDER t502
 		  	WHERE t500.C501_ORDER_ID        = p_par_order_id -- Parent order id
   		   	AND t502b.C205_PART_NUMBER_ID = t500.C205_PART_NUMBER_ID
		    AND t500.C500_ORDER_INFO_ID   = t502b.C500_ORDER_INFO_ID
		    AND t500.C500_ORDER_INFO_ID    = t502.C500_ORDER_INFO_ID 
		    AND t502b.C205_PART_NUMBER_ID = v_part_num
		    AND NVL(t502b.c502b_usage_lot_num,'-999')=NVL(v_lot,'-999')
		    AND t502b.C502B_VOID_FL IS NULL
		    AND t500.C500_VOID_FL IS NULL
		    AND t502.c502_VOID_FL IS NULL;
    
	 BEGIN
		 
		FOR order_cur IN item_order_cur
        LOOP 
        	
        	v_qty := order_cur.iqty;
        
    	   	 	FOR lot_cur IN lot_usage_cur (order_cur.ipnum)
      	     	 LOOP      	
  	 	
						FOR prc_cur IN price_cur (lot_cur.pnum,lot_cur.lot_num)
		      	     	 LOOP
						
		      	     	 -- check item price equal to parent item price
							IF(order_cur.iprice = prc_cur.v_item_price) THEN 
		               
									IF v_qty < 0 THEN -- check qty is less than zero
		                  			
										IF lot_cur.u_item_qty = v_qty THEN-- is item qty equal to lot qty update  order id and order info id
										
		                      			  v_qty         := lot_cur.u_item_qty - v_qty;-- make v_qty as zero to break loop
		                      			  
				                        	UPDATE T502B_ITEM_ORDER_USAGE SET  
											C501_ORDER_ID = p_order_id,
											c500_order_info_id = order_cur.info_id,
											c502b_last_updated_by         = p_user_id,
										    c502b_last_updated_date       = CURRENT_DATE 
										    WHERE  c502b_trans_id                = p_trans_id
										    AND c502b_item_order_usage_id = lot_cur.usage_id
										    AND c500_order_info_id IS NULL
										    AND c501_order_id IS NULL;
                        
                        
					                    ELSIF (lot_cur.u_item_qty < v_qty) THEN -- if item qty is less than lot qty
					                    
					                        v_qty := lot_cur.u_item_qty - v_qty;
					                       
					                         	UPDATE T502B_ITEM_ORDER_USAGE SET  
												C501_ORDER_ID = p_order_id,
												c500_order_info_id = order_cur.info_id,
												c502b_last_updated_by         = p_user_id,
											    c502b_last_updated_date       = CURRENT_DATE 
											    WHERE  c502b_trans_id                = p_trans_id
											    AND c502b_item_order_usage_id = lot_cur.usage_id
											    AND c500_order_info_id IS NULL
											    AND c501_order_id IS NULL;
											    
					                      	  v_qty := 0;
					                        
						                  ELSE  -- to handle multiple lot for single order item
						                        v_qty := v_qty - lot_cur.u_item_qty;
						                        
						                         UPDATE T502B_ITEM_ORDER_USAGE SET  
												  C501_ORDER_ID = p_order_id,
												  c500_order_info_id = order_cur.info_id,
												  c502b_last_updated_by         = p_user_id,
											      c502b_last_updated_date       = CURRENT_DATE 
												  WHERE  c502b_trans_id                = p_trans_id
												  AND c502b_item_order_usage_id = lot_cur.usage_id
												  AND c500_order_info_id IS NULL
												  AND c501_order_id IS NULL;
												  
						                  END IF;
              				    END IF;-- v_qty
                	    	
              				    EXIT; -- IF PRICE MATCH EXIT THE PRICE LOOP	
			 	  		
              		  END IF; -- V_PRICE 
			
				  END LOOP;--Price loop
    		
			 END LOOP;-- lot usage loop
    		 
  		
          END LOOP;-- order item loop
		
	END gm_upd_return_order_dtls;
	
	  	  /***********************************************************
     * Description : Procedure to record Usage Lot #s for Sterile
     *               Parts used in Surgery for DOs booked from 
     *               the 'Case Book' 
     ***********************************************************/
    PROCEDURE gm_sav_case_book_order_usage (
        p_order_id   IN   t501_order.c501_order_id%TYPE,
        p_user_id    IN   t501_order.c501_created_by%TYPE    
     )
     AS  
     v_flag             VARCHAR2(2);
     v_qty              NUMBER;
     v_order_info_id    VARCHAR2(20);
     v_pnum             t502a_item_order_attribute.c205_part_number_id%TYPE;
     v_case_info_id	   t7100_case_information.c7100_case_info_id%TYPE;
     v_part_num         t502a_item_order_attribute.c205_part_number_id%TYPE; 
   		
   		 /* Fetch the part and qty details */
   	CURSOR v_curr
    IS  
        SELECT c500_order_info_id orderinfoid,c205_part_number_id pnum,c502_item_qty  qty
	      FROM t502_item_order
         WHERE c501_order_id       = p_order_id				   
	       AND c502_delete_fl     IS NULL
	       AND c502_void_fl       IS NULL;
			  
     BEGIN
	     BEGIN
	         /* Fetch the case information id */
	          SELECT c7100_case_info_id into v_case_info_id 
	            FROM t501_order 
	           WHERE c501_order_id= p_order_id 
	             AND c501_void_fl IS NULL;
	       EXCEPTION WHEN NO_DATA_FOUND THEN
		     v_case_info_id := NULL;
	     END;
	     IF v_case_info_id IS NOT NULL 
	     THEN
	             /* Update void flag if case nformation id is present*/
	          UPDATE T502B_ITEM_ORDER_USAGE
                 SET c502b_void_fl            = 'Y'
            	     ,c502b_last_updated_by   = p_user_id
                     ,c502b_last_updated_date = CURRENT_DATE
               WHERE c501_order_id   = p_order_id;

           
		       FOR v_index IN v_curr
               LOOP
      	              v_order_info_id := v_index.orderinfoid;
                      v_part_num 	  := v_index.pnum;
                      v_qty 		  := v_index.qty;

                      /* fetch the control number needed flag for Sterile Parts in part number table*/
                      v_flag          := gm_pkg_op_do_process_trans.get_control_number_needed_fl(v_part_num);
             
                      IF v_flag = 'Y'
                      THEN
           	                   gm_pkg_cs_usage_lot_number.gm_sav_usage_dtls (p_order_id, null, v_part_num, v_qty, NULL, NULL, v_order_info_id, NULL, NULL,p_user_id);
                      END IF;
                 END LOOP;
            END IF; 
	  END gm_sav_case_book_order_usage;	      
	  
	  /*************************************************************************************************************
		* Description : This procedure is used to update the used lot information issue credit/debit orders
		* 				PMT-45387: Record Usage lot and DDT number
		* Author      : Raja
		***********************************************************************************************************/
		PROCEDURE gm_sav_order_used_lot_bulk (
		        p_order_id     IN t501_order.c501_order_id%TYPE,
		        p_used_lot_str IN CLOB,
		        p_user_id      IN t501_order.c501_created_by%TYPE)
		AS
		    v_strlen NUMBER := NVL (LENGTH (p_used_lot_str), 0) ;
		    v_string CLOB   := p_used_lot_str;
		    v_substring VARCHAR2 (4000) ;
		    --
		    v_pnum t502a_item_order_attribute.c205_part_number_id%TYPE;
		    v_used_cnum VARCHAR2 (100) ;
		    v_ddt_num   VARCHAR2 (100) ;
		    v_qty       NUMBER;
		    --
		    v_order_infor_id t502b_item_order_usage.c500_order_info_id%TYPE;
		    --
		BEGIN
			-- to void the item usage lot details
			 
			gm_pkg_cs_usage_lot_number.gm_void_item_order_usage_dtls (p_order_id, p_user_id);
			
			
		    --1 Loop the string and spilt the string details.
		    --Part ^ Qty ^ Usage Lot ^ DDT number|
		    
		    IF v_strlen                      > 0 THEN
		        WHILE INSTR (v_string, '|') <> 0
		        LOOP
		            v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
		            v_string    := SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
		            --
		            v_pnum      := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
		            v_substring := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
		            v_qty       := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
		            v_substring := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
		            v_used_cnum := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
		            v_substring := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
		            v_ddt_num   := v_substring ;
		            --
		            -- To get the order info id (based on order id and part number)
		            
		           BEGIN
			           --
					 SELECT c500_order_info_id
					 	INTO v_order_infor_id
	                   FROM t500_order_info t500
	                  WHERE t500.c501_order_id       = p_order_id
	                    AND c205_part_number_id      = v_pnum
	                    AND c500_void_fl            IS NULL
	                    AND ROWNUM              = 1;
	                    --
                   EXCEPTION WHEN OTHERS
                   THEN
                   	v_order_infor_id := NULL;
                   END;
                   --
                   IF v_order_infor_id IS NOT NULL
                   THEN
			            -- to create the new record (item usage lot)
			            -- parameter details are,
			            -- Order id, Item Order usage id, Part, Qty, LOT, Ref id (DDT), Order Info id, Trans id, void flag, user  id
			            
			            gm_pkg_cs_usage_lot_number.gm_sav_usage_dtls (p_order_id, NULL, v_pnum, v_qty, v_used_cnum,
		                            v_ddt_num, v_order_infor_id, NULL, NULL, p_user_id) ;
		                            
		            END IF; -- end of v_order_infor_id check
		            
		        END LOOP; -- end of while loop
		        
		    END IF; -- end of length check
		    --
		END gm_sav_order_used_lot_bulk;
		--
		/*************************************************************************************************************
		* Description : This procedure is used to void the usage lot details based on order id
		*               Called from Issue credit/debit time - to void the current used lot information
		*               PMT-45387: Record Usage lot and DDT number
		* Author      : mmuthusamy
		***********************************************************************************************************/
		PROCEDURE gm_void_item_order_usage_dtls (
		        p_order_id IN t501_order.c501_order_id%TYPE,
		        p_user_id  IN t501_order.c501_created_by%TYPE)
		AS
		BEGIN
		    -- to void the current records (T502b)
		     UPDATE t502b_item_order_usage
		    SET c502b_void_fl      = 'Y', c502b_last_updated_by = p_user_id, c502b_last_updated_date = CURRENT_DATE
		      WHERE c501_order_id  = p_order_id
		        AND c502b_void_fl IS NULL;
		    --    
		END gm_void_item_order_usage_dtls;
		
END gm_pkg_cs_usage_lot_number;
/
