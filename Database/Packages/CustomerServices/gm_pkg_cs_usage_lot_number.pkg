CREATE OR REPLACE
PACKAGE gm_pkg_cs_usage_lot_number
IS
    /*********************************************************************************
    * Description : This procedure is used to save the usage lot number.
    *     To called from new order, Edit order, Control # edit - screen
    * Author     : Manikandan
    * Parameters  : Order ID, Input string, screen name and User id
    **********************************************************************************/
PROCEDURE gm_sav_usage_ddt_dtls (
        p_order_id  IN t501_order.c501_order_id%TYPE,
        p_input_str IN CLOB,
        p_screen    IN VARCHAR2,
        p_user_id   IN t501_order.c501_created_by%TYPE) ;
    /*********************************************************************************
    * Description : This procedure is used to save the usage data to T502B.
    * Author     : Manikandan
    * Parameters  : Order ID
    **********************************************************************************/
PROCEDURE gm_sav_usage_dtls (
        p_order_id IN t501_order.c501_order_id%TYPE,
        p_usage_dtls_id IN T502B_ITEM_ORDER_USAGE.C502B_ITEM_ORDER_USAGE_ID%TYPE,
        p_part_num IN T502B_ITEM_ORDER_USAGE.C205_PART_NUMBER_ID%TYPE,
        p_qty IN T502B_ITEM_ORDER_USAGE.C502B_ITEM_QTY%TYPE,
        p_usage_lot IN T502B_ITEM_ORDER_USAGE.C502B_USAGE_LOT_NUM%TYPE,
        p_ref_id IN T502B_ITEM_ORDER_USAGE.C502B_REF_ID%TYPE,
        p_order_info_id IN T502B_ITEM_ORDER_USAGE.C500_ORDER_INFO_ID%TYPE,
        p_trans_id IN T502B_ITEM_ORDER_USAGE.c502b_trans_id%TYPE,
        p_void_fl  IN T502B_ITEM_ORDER_USAGE.c502b_void_fl%TYPE,
        p_user_id  IN t501_order.c501_created_by%TYPE) ;
    /*********************************************************************************
    * Description : This procedure is used to fetch the DDT number details
    * Author     : Manikandan
    * Parameters  : Order ID
    **********************************************************************************/
PROCEDURE gm_fch_ddt_dtls (
        p_order_id IN t501_order.c501_order_id%TYPE,
        p_out_ord OUT TYPES.cursor_type) ;
    /*********************************************************************************
    * Description : This procedure is used to sync the item order price to order info and usage table
    * Author     : Manikandan
    * Parameters  : Order ID, User ID
    **********************************************************************************/
PROCEDURE gm_upd_price (
        p_order_id IN t501_order.c501_order_id%TYPE,
        p_user_id  IN t501_order.c501_created_by%TYPE) ;
    /*********************************************************************************
    * Description : This procedure is used to sync the item orders to info and usage table.
    * Author     : Manikandan
    * Parameters  : Order ID, User ID
    **********************************************************************************/
PROCEDURE gm_sav_order_info (
        p_order_id IN t501_order.c501_order_id%TYPE,
        p_usage_lot_fl   IN VARCHAR2,
        p_user_id  IN t501_order.c501_created_by%TYPE) ;
    /*********************************************************************************
    * Description : This procedure is used to fetch lot number based on part # - used for Usage lot screen
    * Author     : Manikandan
    * Parameters  : Order ID, Part #
    **********************************************************************************/
PROCEDURE gm_fch_lot_number_dtls (
        p_order_id IN t501_order.c501_order_id%TYPE,
        p_part_num T502B_ITEM_ORDER_USAGE.C205_PART_NUMBER_ID%TYPE,
        p_out_lot_dtls OUT TYPES.cursor_type) ;
    /*********************************************************************************
    * Description : This procedure is used to fetch usage details
    * Author     : Manikandan
    * Parameters  : Order ID
    **********************************************************************************/
PROCEDURE gm_fch_order_usage_dtls (
        p_order_id IN t501_order.c501_order_id%TYPE,
        p_out_order_usage_dtls OUT TYPES.cursor_type) ;
    /*********************************************************************************
    * Description : This procedure is used to fetch used lot number details
    * Author     : Manikandan
    * Parameters  : Order ID
    **********************************************************************************/
PROCEDURE gm_fch_used_lot_number (
        p_order_id IN t501_order.c501_order_id%TYPE,
        p_out_used_lot OUT TYPES.cursor_type) ;
    /*********************************************************************************
    * Description : This procedure is used to save the RA - Lot number
    * Author     : Manikandan
    * Parameters  : Order ID, RA ID, String and user id
    **********************************************************************************/
PROCEDURE gm_return_lot_number_dtls (
        p_order_id  IN t501_order.c501_order_id%TYPE,
        p_trans_id  IN t502b_item_order_usage.c502b_trans_id%TYPE,
        p_input_str IN VARCHAR2,
        p_user_id   IN t502b_item_order_usage.c502b_created_by%TYPE) ;
    /*********************************************************************************
    * Description : This procedure is used to save the credit RA and Usage Lot number
    * Author     : Manikandan
    * Parameters  : Order ID, RA ID and user id
    **********************************************************************************/
PROCEDURE gm_upd_return_credit_dtls (
        p_order_id IN t501_order.c501_order_id%TYPE,
        p_trans_id IN t502b_item_order_usage.c502b_trans_id%TYPE,
        p_user_id  IN t502b_item_order_usage.c502b_created_by%TYPE) ;
    /*********************************************************************************
    * Description : This procedure is used to remove Qty to usage table (Edit order screen)
    * Author     : Manikandan
    * Parameters  : Order ID, input string user id
    **********************************************************************************/
PROCEDURE gm_upd_remove_qty (
        p_order_id  IN t501_order.c501_order_id%TYPE,
        p_input_str IN VARCHAR2,
        p_user_id   IN t502b_item_order_usage.c502b_created_by%TYPE) ;
    /*********************************************************************************
    * Description : This procedure is used to save usage details (Issue credit/debit)
    * Author     : Manikandan
    * Parameters  : Invoice id
    **********************************************************************************/
PROCEDURE gm_credit_invoice_update (
        p_invoice_id t503_invoice.c503_invoice_id%TYPE,
        p_user_id IN t502b_item_order_usage.c502b_created_by%TYPE) ;
    /*********************************************************************************
    * Description : This procedure is used to remove Qty to usage table (Edit order screen)
    * Author     : Manikandan
    * Parameters  : Order info ID, qty user id
    **********************************************************************************/
PROCEDURE gm_upd_backorder_qty (
        p_order_info_id IN t500_order_info.c500_order_info_id%TYPE,
        p_qty           IN NUMBER,
        p_user_id       IN t502b_item_order_usage.c502b_created_by%TYPE) ;
    /*********************************************************************************
    * Description : This procedure is used to update order info qty (called from remove Qty procedure)
    * Author     : Manikandan
    * Parameters  : Order info ID, user id
    **********************************************************************************/
PROCEDURE gm_upd_order_info (
        p_order_info_id IN t500_order_info.c500_order_info_id%TYPE,
        p_user_id       IN t502b_item_order_usage.c502b_created_by%TYPE) ;
    /*********************************************************************************
    * Description : This procedure is used to sync back order usage lot number 
    * Author     : Manikandan
    * Parameters  : Order ID, Order info ID, user id
    **********************************************************************************/
PROCEDURE gm_sync_back_order_usage_lot (
		p_order_id IN T501_order.c501_order_id%TYPE,
        p_order_info_id IN t500_order_info.c500_order_info_id%TYPE,
        p_user_id       IN t502b_item_order_usage.c502b_created_by%TYPE) ;
	/*********************************************************************************
    * Description : This procedure is used to save the values to global table (T502b) 
    * Author     : Manikandan
    * Parameters  : Order ID, Order info ID, user id
    **********************************************************************************/
 PROCEDURE gm_sav_temp_order_usage (
		p_order_usage_id IN t502b_item_order_usage.c502b_item_order_usage_id%TYPE,
		p_order_id IN t502b_item_order_usage.c501_order_id%TYPE,
		p_pnum	IN t502b_item_order_usage.c205_part_number_id%TYPE,
		p_qty	IN t502b_item_order_usage.c502b_item_qty%TYPE,
		p_lot_num IN t502b_item_order_usage.c502b_usage_lot_num%TYPE,
		p_ref_id IN t502b_item_order_usage.c502b_ref_id%TYPE,
        p_order_info_id IN t500_order_info.c500_order_info_id%TYPE) ;   
     /* Description  : This Function Returns the sum Qty For The Selected Order
      * Parameters  : Usage id
      */
FUNCTION get_italy_invoice_part_qty (
            p_order_usage_id t502b_item_order_usage.c502b_item_order_usage_id%TYPE)
        RETURN NUMBER;
	 /*********************************************************************************
	* Description : This procedure is used to reduce the Qty in T500 and T502b table
	* Author     : Manikandan
	* Parameters  : Order ID, user id
	**********************************************************************************/
PROCEDURE gm_upd_usage_lot_order_qty (
        p_order_id IN T501_order.c501_order_id%TYPE,
        p_user_id  IN t502b_item_order_usage.c502b_created_by%TYPE) ; 
        
/****************************************************************************************
	* Description : This procedure is used to update the control number error message T502B.
	* Author     : karthik
	* Parameters  : orderid , lot# , part#,usagedetails id
	*************************************************************************************/
PROCEDURE gm_sav_usage_error_dtls (
	 p_order_id t501_order.c501_order_id%TYPE,
	 p_usage_lot IN T502B_ITEM_ORDER_USAGE.C502B_USAGE_LOT_NUM%TYPE,
	 p_part_num IN T502B_ITEM_ORDER_USAGE.C205_PART_NUMBER_ID%TYPE,
	 p_usage_dtls_id IN T502B_ITEM_ORDER_USAGE.C502B_ITEM_ORDER_USAGE_ID%TYPE
	 );
	 
/*************************************************************************************************************************************
	* Description : This procedure is used to update the invalid control number error message T502B while the user give the wrong lot from IPAD
	* Author     : karthik
	* Parameters  : Lot,part# ,orderid
	****************************************************************************************************************************************/
PROCEDURE gm_sav_invalid_usage_lot_dtls (
	 p_usage_lot IN T502B_ITEM_ORDER_USAGE.C502B_USAGE_LOT_NUM%TYPE,
	 p_part_num  IN T502B_ITEM_ORDER_USAGE.C205_PART_NUMBER_ID%TYPE,
	 p_order_id IN t501_order.c501_order_id%TYPE 
	 );
	 
	 /*************************************************************************************************************************************
	* Description : This procedure is used to check the lot is already billed
	* Author     : Dsandeep
	****************************************************************************************************************************************/
	PROCEDURE gm_save_billed_lot (
     p_order_id t501_order.c501_order_id%TYPE,
	 p_pnum IN T502B_ITEM_ORDER_USAGE.C502B_USAGE_LOT_NUM%TYPE,
	 p_cnum  IN T502B_ITEM_ORDER_USAGE.C205_PART_NUMBER_ID%TYPE
	 );
	 
 /*************************************************************************************************************
	* Description : This procedure is used to fetch the error message that the lot is already billed
	* Author     : karthiks
	***********************************************************************************************************/
PROCEDURE gm_fch_billed_loterror_msg (
	 p_pnum          IN T502B_ITEM_ORDER_USAGE.C502B_USAGE_LOT_NUM%TYPE,
	 p_cnum          IN T502B_ITEM_ORDER_USAGE.C205_PART_NUMBER_ID%TYPE,
	 p_error_message OUT  VARCHAR2
     );
 
     /*************************************************************************************************************
	* Description : This procedure is used to update  the credit RA and Usage Lot number.
	* Author     : Mahavishnu
	***********************************************************************************************************/
     PROCEDURE gm_upd_return_order_dtls (
	 p_par_order_id IN t501_order.c501_order_id%TYPE,
	 p_order_id     IN t501_order.c501_order_id%TYPE,
	 p_trans_id     IN T502B_ITEM_ORDER_USAGE.C502B_TRANS_ID%TYPE,
	 p_user_id      IN t501_order.c501_created_by%TYPE
	  );
	  
	/*********************************************************************************
    * Description : This procedure is used to fetch usage lot number.
    * Delivered Order - Returns Parts
    * Author     : T.S. Ramachandiran
    * Parameters  : Order ID, Part Number
    **********************************************************************************/
    PROCEDURE gm_fch_order_used_lot_number_by_part(
    p_order_id			IN		T501_ORDER.C501_ORDER_ID%TYPE,
    p_part_num			IN		T502B_ITEM_ORDER_USAGE.C205_PART_NUMBER_ID%TYPE,
    p_out_usage_JSON    OUT		VARCHAR2
    );
 
	  /*************************************************************************************************************
	* Description : This procedure is used to update the used lot information issue credit/debit orders
	* Author     : 
	***********************************************************************************************************/
     PROCEDURE gm_sav_order_used_lot_bulk (
	 p_order_id     IN t501_order.c501_order_id%TYPE,
	 p_used_lot_str   IN CLOB,
	 p_user_id      IN t501_order.c501_created_by%TYPE
	  );
	 
	--
	/*************************************************************************************************************
	* Description : This procedure is used to void the usage lot details based on order id
	*               Called from Issue credit/debit time - to void the current used lot information
	*               PMT-45387: Record Usage lot and DDT number
	* Author      : mmuthusamy
	***********************************************************************************************************/
	PROCEDURE gm_void_item_order_usage_dtls (
	        p_order_id IN t501_order.c501_order_id%TYPE,
	        p_user_id  IN t501_order.c501_created_by%TYPE);
	        
END gm_pkg_cs_usage_lot_number;
/
