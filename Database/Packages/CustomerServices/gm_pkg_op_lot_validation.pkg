CREATE OR REPLACE PACKAGE gm_pkg_op_lot_validation
IS

/*************************************************************************************************************************************
	* Description : This procedure is used to return the error message and Error type when the user give the wrong lot number  PMT-29681
	* Author     : Swetha
	* Parameters  : Lot,part#
****************************************************************************************************************************************/
PROCEDURE gm_validate_lot_number(
	 p_usage_lot IN t2550_part_control_number.C2550_CONTROL_NUMBER%TYPE,
	 p_part_num  IN t2550_part_control_number.C205_PART_NUMBER_ID%TYPE,
	 p_field_sales_id IN t2550_part_control_number.C2550_REF_ID%TYPE,
	 p_lot_error_msg OUT VARCHAR2,
	 p_error_type OUT t5063_lot_error_info.C901_ERROR_TYPE%TYPE
	 );
	 
	 
/*************************************************************************************************************************************
	* Description : This procedure is used to return the error message and Error type when the user enters the lot no which is not available PMT-29681
	* Author     : Swetha
	* Parameters  : Lot,part#
****************************************************************************************************************************************/
	PROCEDURE gm_validate_lot_available(
	 p_usage_lot IN t2550_part_control_number.C2550_CONTROL_NUMBER%TYPE,
	 p_part_num  IN t2550_part_control_number.C205_PART_NUMBER_ID%TYPE,
	 p_lot_error_msg OUT VARCHAR2,
	 p_error_type OUT t5063_lot_error_info.C901_ERROR_TYPE%TYPE
	 );


/*************************************************************************************************************************************
	* Description : This procedure is used to return the error message and Error type when the lot is available in FS or Sold inventory PMT-29681
	* Author     : Swetha
	* Parameters  : Lot,part#
****************************************************************************************************************************************/
	PROCEDURE gm_validate_lot_inventory(
	 p_usage_lot IN t2550_part_control_number.C2550_CONTROL_NUMBER%TYPE,
	 p_part_num  IN t2550_part_control_number.C205_PART_NUMBER_ID%TYPE,
	 p_lot_error_msg OUT VARCHAR2,
	 p_error_type OUT t5063_lot_error_info.C901_ERROR_TYPE%TYPE
	 );
	 

/*************************************************************************************************************************************
	* Description : This procedure is used to return the error message and Error type when the lot is already billed PMT-29681
	* Author     : Swetha
	* Parameters  : Lot,part#
****************************************************************************************************************************************/
	PROCEDURE gm_validate_lot_is_billed(
	 p_usage_lot IN t2550_part_control_number.C2550_CONTROL_NUMBER%TYPE,
	 p_part_num  IN t2550_part_control_number.C205_PART_NUMBER_ID%TYPE,
	 p_lot_error_msg OUT VARCHAR2,
	 p_error_type OUT t5063_lot_error_info.C901_ERROR_TYPE%TYPE
	 );
	 
	 
/*************************************************************************************************************************************
	* Description : This procedure is used to return the error message and Error type when there is field sales mismatch in the usage lot number PMT-29681
	* Author     : Swetha
	* Parameters  : Lot,part#,FieldSales id
****************************************************************************************************************************************/
	PROCEDURE gm_validate_lot_fs_mismatch(
	 p_usage_lot IN t2550_part_control_number.C2550_CONTROL_NUMBER%TYPE,
	 p_part_num  IN t2550_part_control_number.C205_PART_NUMBER_ID%TYPE,
	 p_field_sales_id IN t2550_part_control_number.C2550_REF_ID%TYPE,
	 p_lot_error_msg OUT VARCHAR2,
	 p_error_type OUT t5063_lot_error_info.C901_ERROR_TYPE%TYPE
	 );

/*************************************************************************************************************************************
	* Description : This procedure is used to return the error message invalid control number for PMT-46935
	* Author      : Sindhu
****************************************************************************************************************************************/
 PROCEDURE gm_validate_lot(
	 p_usage_lot IN t2550_part_control_number.C2550_CONTROL_NUMBER%TYPE,
	 p_part_num  IN t2550_part_control_number.C205_PART_NUMBER_ID%TYPE,
	 p_lot_error_msg OUT VARCHAR2,
     p_out_pnum OUT t2550_part_control_number.C205_PART_NUMBER_ID%TYPE,
     p_out_usage_lot OUT t2550_part_control_number.C2550_CONTROL_NUMBER%TYPE,
     p_out_expiry_date OUT t2550_part_control_number.C2550_CONTROL_NUMBER%TYPE,
     p_out_part_desc OUT t205_part_number.C205_PART_NUM_DESC%TYPE
	 );

END gm_pkg_op_lot_validation;
/