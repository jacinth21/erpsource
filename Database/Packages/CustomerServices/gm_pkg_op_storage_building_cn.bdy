/*******************************************************
   * Description : Package body for splitting the consignment based on the parts available in the building for PMT-33511
   *		
   * Parameters  : Consignment ID
   *******************************************************/
CREATE OR REPLACE
PACKAGE BODY gm_pkg_op_storage_building_cn
IS

/*************************************************************************************************************************************
	* Description : This procedure is used to validate the consignment type and the plant for the input consignment id for PMT-33511
	* Author     : Swetha
	* Parameters  : Consignment ID
****************************************************************************************************************************************/
PROCEDURE gm_validate_consignment_transaction(
	 p_consign_id IN VARCHAR2,
	 p_validate_consign OUT VARCHAR2
	  )
	 
	AS
     v_plant_rule_val t906_rules.c906_rule_value%TYPE;
     v_consign_rule_val t906_rules.c906_rule_value%TYPE;
     v_plant_id t504_consignment.c5040_plant_id%TYPE;
     v_consign_type t504_consignment.c504_type%TYPE;
	 
	 
  BEGIN
 
  IF p_consign_id = '' OR p_consign_id IS NULL 
     THEN
      RETURN;
   END IF;
   --setting consignment ids in context
  my_context.set_my_inlist_ctx(p_consign_id);    
 
 --get the consignment type and plant rule value for the input consignment id   
   BEGIN  
     SELECT  t504.c504_type, t906.c906_rule_value  
       INTO v_consign_type, v_plant_rule_val 
      FROM t504_consignment t504, t906_rules t906 
       WHERE t504.c504_consignment_id IN (SELECT token FROM v_in_list) 
       AND t504.c504_void_fl IS NULL
       AND t504.c207_set_id IS NULL
	   AND t906.c906_rule_id = t504.c5040_plant_id  -- Rule id will be plant id 
       AND t906.c1900_company_id IS NULL 
       AND t906.c906_void_fl IS NULL
       AND t906.c906_rule_grp_id = 'BUILDINGPLANTMAP' GROUP BY t504.c504_type, t906.c906_rule_value;
   EXCEPTION
            WHEN OTHERS THEN
                v_plant_rule_val := NULL;
                v_consign_type := NULL;
                END;
 
  -- validating the consignment type       
   IF v_plant_rule_val = 'Y' THEN
      SELECT get_rule_value (v_consign_type, 'BUILDMAPCONSIGNTYPE')
           INTO v_consign_rule_val
          FROM dual;
      
    ELSE
      RETURN;
      
  END IF;
       
   
   IF v_consign_rule_val = 'Y' THEN
      p_validate_consign := 'YES';
   ELSE
        p_validate_consign := NULL;
   END IF;
END gm_validate_consignment_transaction;


/***************************************************************************************************************************************
   * Purpose: This Procedure is used to fetch the building count for the parts in a Consignment for PMT-33511
   * Author     : Swetha
   * Parameters  : Consignment ID
 *************************************************************************************************************************************/

	PROCEDURE gm_fetch_consignment_building_ids(
	 p_consign_id IN t504_consignment.c504_consignment_id%TYPE,
	 p_building_count OUT NUMBER
	 )
	  AS
	   v_plant_id t504_consignment.c5040_plant_id%TYPE;
	   v_building_count	   NUMBER;
	
	BEGIN
	
	SELECT get_plantid_frm_cntx()
        INTO v_plant_id
        FROM DUAL;
        	
	BEGIN
	   SELECT COUNT( DISTINCT (t5052.c5057_building_id))
	     INTO v_building_count
      FROM t5053_location_part_mapping t5053, t5052_location_master t5052, t504_consignment t504, t505_item_consignment t505
     WHERE t5052.c5052_location_id= t5053.c5052_location_id
     AND t5052.c5040_plant_id   = v_plant_id
     AND t5052.c5052_void_fl IS NULL
     AND t5053.c5053_curr_qty  > 0
     AND t5052.c901_location_type =93320  -- Pick Face
     AND t5052.c5051_inv_warehouse_id = 1 -- FG Warehouse
     AND t5053.c205_part_number_id = t505.c205_part_number_id
     AND t504.c504_consignment_id = t505.c504_consignment_id
     AND t505.c504_consignment_id = p_consign_id
     AND t504.c207_set_id IS NULL          
     AND t504.c504_void_fl IS NULL
     AND t5052.c5052_void_fl IS NULL
     AND t5052.c901_status = 93310 --Active
     AND t505.c505_void_fl   IS NULL ;
   EXCEPTION
		WHEN NO_DATA_FOUND THEN
			v_building_count := 0;
	END;
	
	
	p_building_count := v_building_count;
	
	END gm_fetch_consignment_building_ids;



/***************************************************************************************************************************************
   * Purpose: This procedure is used to process the input consignment id by getting the building count for that consignment and split the consignment based on the building count for PMT-33511
   * Author     : Swetha
   * Parameters  : Consignment ID
 *************************************************************************************************************************************/

	PROCEDURE gm_cn_process_transaction(
	 p_consign_id VARCHAR2
	)
	  AS

    v_building_cnt    NUMBER;
    v_consign_id t504_consignment.c504_consignment_id%TYPE;
    v_user_id t504_consignment.c504_created_by%TYPE;
	v_consigment_id t504_consignment.c504_consignment_id%TYPE;
    CURSOR v_cn_list
	IS
	SELECT token cnid FROM v_in_list;
    BEGIN
	--setting consignment ids in context
	my_context.set_my_inlist_ctx(p_consign_id); 
    FOR v_cns IN v_cn_list
		LOOP
        v_consigment_id := v_cns.cnid; 
    BEGIN
    --fetch the consignment id and userid 
     SELECT t504.c504_consignment_id, t504.c504_created_by
          INTO v_consign_id, v_user_id    
				 FROM t504_consignment t504 
				WHERE t504.c504_consignment_id = v_consigment_id 
				AND t504.c207_set_id IS NULL
				AND t504.c504_void_fl IS NULL
				AND t504.c504_type
				   IN (SELECT c906_rule_id
                          FROM t906_rules
	                   WHERE c906_rule_grp_id = 'BUILDMAPCONSIGNTYPE'
	                   AND c1900_company_id IS NULL 
	                   AND c906_void_fl IS NULL)
				FOR UPDATE;
     EXCEPTION
     WHEN NO_DATA_FOUND THEN
      v_consign_id := NULL;
      v_user_id := NULL;
     END;
    
      --procedure call to fetch the building count for the input consignment id
      gm_fetch_consignment_building_ids(v_consign_id, v_building_cnt);
      
        IF (v_building_cnt <= 1) THEN
       
       --update the building id for the passed consignment id
          gm_update_consign_building_id(v_consigment_id, v_user_id);
               
       ELSE
       
       --Procedure call to split the consignment based on the building count
        gm_consignment_split_transaction(v_consigment_id, v_user_id);
          
                   
       END IF;
	 END LOOP;
   END gm_cn_process_transaction;


/***************************************************************************************************************************************
   * Purpose: This procedure is used to update the building id in t504_consignment table for the input Consignment id for PMT-33511
   * Author     : Swetha
   * Parameters  : Consignment ID, User ID, Building ID
 *************************************************************************************************************************************/
	PROCEDURE gm_update_consign_building_id(
	 p_consign_id IN t504_consignment.c504_consignment_id%TYPE,
	 p_user_id IN t504_consignment.c504_created_by%TYPE,
	 p_building_id IN t5052_location_master.c5052_location_id%TYPE DEFAULT NULL
	)
	  
	  AS
	  
	  v_building_id t5052_location_master.c5057_building_id%TYPE;
	  v_plant_id 	  t5040_plant_master.c5040_plant_id%TYPE;
	  v_default_building_id t5052_location_master.c5057_building_id%TYPE;
	  
	   
	  BEGIN   
	   
	  
	  SELECT get_plantid_frm_cntx()
        INTO  v_plant_id
        FROM DUAL;
     
  IF p_building_id IS NULL THEN
  
  --fetch the default building id 
   BEGIN
      SELECT c5057_building_id 
     INTO v_default_building_id 
       FROM t5057_building 
      WHERE c5040_plant_id = v_plant_id  
       AND c5057_primary_fl='Y' 
       AND c5057_active_fl='Y' 
       AND c5057_void_fl IS NULL;
     EXCEPTION
       WHEN NO_DATA_FOUND THEN -- when default building id is not available
        v_default_building_id := NULL;
    END; 
    
   --Fetch building id for the consignment
  BEGIN  
       SELECT t5052.c5057_building_id
	     INTO v_building_id
      FROM t5053_location_part_mapping t5053, t5052_location_master t5052, t504_consignment t504, t505_item_consignment t505
     WHERE t5052.c5052_location_id= t5053.c5052_location_id
     AND t5052.c5040_plant_id   = v_plant_id
     AND t5052.c5052_void_fl IS NULL
     AND t5053.c5053_curr_qty  > 0
     AND t5052.c901_location_type =93320  -- Pick Face
     AND t5052.c5051_inv_warehouse_id = 1 -- FG Warehouse
     AND t5053.c205_part_number_id = t505.c205_part_number_id
     AND t504.c504_consignment_id = t505.c504_consignment_id
     AND t505.c504_consignment_id = p_consign_id
     AND t504.c207_set_id IS NULL          
     AND t504.c504_void_fl IS NULL
     AND t5052.c5052_void_fl IS NULL
     AND t505.c505_void_fl   IS NULL
     AND t5052.c901_status = 93310 --Active
     GROUP BY t5052.c5057_building_id ;
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
        v_building_id := NULL;
   END;
                
   --updating building id for t504_consignment table
    UPDATE t504_consignment SET c5057_building_id = NVL(v_building_id, v_default_building_id) --update default building id if building id for the part is not available 
          , c504_last_updated_by = p_user_id   
          , c504_last_updated_date = current_date 
       WHERE c504_consignment_id = p_consign_id 
       AND   c207_set_id IS NULL
      AND c504_void_fl IS NULL;
       
       --updating inventory pick  
      UPDATE t5050_invpick_assign_detail 
       SET c5057_building_id = NVL(v_building_id, v_default_building_id) --update default building id if building id for the part is not available
         , c5050_last_updated_by = p_user_id
         , c5050_last_updated_date = current_date 
       WHERE c5050_ref_id = p_consign_id
       AND c5050_void_fl IS NULL ;
       
       --updating building id in t907_shipping_info table
       UPDATE t907_shipping_info
        SET c5057_building_id = NVL(v_building_id, v_default_building_id) --update default building id if building id for the part is not available
          , c907_last_updated_by = p_user_id
          , c907_last_updated_date = current_date
       WHERE c907_ref_id = p_consign_id
       AND c907_void_fl IS NULL;
    
   ELSIF p_building_id IS NOT NULL THEN
   
     --update building id for t504_consignment table
    UPDATE t504_consignment SET c5057_building_id = p_building_id 
          , c504_last_updated_by = p_user_id   
          , c504_last_updated_date = current_date 
       WHERE c504_consignment_id = p_consign_id
       AND  c207_set_id IS NULL 
      AND c504_void_fl IS NULL;
      
      --updating inventory pick
        UPDATE t5050_invpick_assign_detail 
       SET c5057_building_id = p_building_id,
           c5050_last_updated_by = p_user_id,
           c5050_last_updated_date = current_date 
       WHERE c5050_ref_id = p_consign_id
       AND c5050_void_fl IS NULL;
       
       --updating building id in t907_shipping_info table
       UPDATE t907_shipping_info
        SET c5057_building_id = p_building_id 
          , c907_last_updated_by = p_user_id
          , c907_last_updated_date = current_date
       WHERE c907_ref_id = p_consign_id
       AND c907_void_fl IS NULL; 
      
 END IF;			
				

END gm_update_consign_building_id;



/***************************************************************************************************************************************
   * Purpose: This procedure is used to fetch the building id's and split the consignment by creating transactions and save all the shipping informations for the newly created consignment for PMT-33511
   * Author     : Swetha
   * Parameters  : Consignment ID, user ID 
 *************************************************************************************************************************************/
	PROCEDURE gm_consignment_split_transaction(
	 p_consign_id IN t504_consignment.c504_consignment_id%TYPE,
	 p_user_id IN t504_consignment.c504_created_by%TYPE
   )
    
    AS 
      
      v_loop_cnt    NUMBER;
      v_building_id t5052_location_master.c5057_building_id%TYPE;
      v_request_id t504_consignment.c520_request_id%TYPE;
      v_consign_id t504_consignment.c504_consignment_id%TYPE;
      
      
      v_out_request_id t504_consignment.c520_request_id%TYPE;
      v_out_consignment_id t504_consignment.c504_consignment_id%TYPE;


CURSOR v_building_list
   IS
   SELECT t504.c520_request_id prequestid, t504.c504_consignment_id pconsignid, t5052.c5057_building_id pbuildingid
	 FROM t5053_location_part_mapping t5053, t5052_location_master t5052, t504_consignment t504, t505_item_consignment t505
     WHERE t5052.c5052_location_id= t5053.c5052_location_id
     AND t5052.c5040_plant_id   = t504.c5040_plant_id
     AND t5052.c5052_void_fl IS NULL
     AND t5053.c5053_curr_qty  > 0
     AND t5052.c901_location_type =93320  -- Pick Face
     AND t5052.c5051_inv_warehouse_id = 1 -- FG Warehouse
     AND t5053.c205_part_number_id = t505.c205_part_number_id
     AND t504.c504_consignment_id = t505.c504_consignment_id
     AND t505.c504_consignment_id = p_consign_id
     AND t504.c207_set_id IS NULL          
     AND t504.c504_void_fl IS NULL
     AND t5052.c5052_void_fl IS NULL
     AND t5052.c901_status = 93310 --Active
     AND t505.c505_void_fl   IS NULL
     GROUP BY t504.c520_request_id, t504.c504_consignment_id, t5052.c5057_building_id;

  BEGIN
       v_loop_cnt := 0;

    FOR var_buildloop IN v_building_list
		LOOP
		   v_loop_cnt := v_loop_cnt + 1;
		   v_request_id := var_buildloop.prequestid;
		   v_consign_id := var_buildloop.pconsignid;
           v_building_id := var_buildloop.pbuildingid;
           
	IF (v_loop_cnt = 1) THEN
			
		 --procedure call to update the  building id for the consignment
		gm_update_consign_building_id(v_consign_id, p_user_id, v_building_id);
			
   ELSIF (v_loop_cnt != 1) THEN
				
		--procedure call for creating new request id 
	   gm_pkg_op_split.gm_sav_split_request(v_request_id, NULL, p_user_id, v_out_request_id);
     
       --procedure call for creating new consignment id 
       gm_pkg_op_split.gm_sav_split_consg(v_consign_id, v_out_request_id, NULL, p_user_id, v_out_consignment_id);

       --procedure call to insert record in t5050_invpick_assign_detail 
       gm_pkg_allocation.gm_ins_invpick_assign_detail(93002, v_out_consignment_id, p_user_id);
           
       --procedure call to update the old request id in t521_request_detail
       gm_update_items_request_id(v_request_id, v_out_request_id, v_building_id );
          
      --procedure call to update the old consignment id in t505_item_consignment
       gm_update_items_consign_id(v_consign_id, v_out_consignment_id, v_building_id );
     
      --procedure call to update the  building id for the consignment
	   gm_update_consign_building_id(v_out_consignment_id, p_user_id, v_building_id);
	
	  /*Add Insert's for newly created consignment*/
	  gm_pkg_op_sav_insert_txn.gm_sav_insert_rev_detail(v_out_consignment_id, 106761, 93342, p_user_id, NULL);
	  
     END IF;
		END LOOP;
END gm_consignment_split_transaction;


/***************************************************************************************************************************************
   * Purpose: This procedure is used to update the old Request id by giving new request id in t521_request_detail table for PMT-33511
   * Author     : Swetha
   * Parameters  : Request ID, Building ID
 *************************************************************************************************************************************/
	PROCEDURE gm_update_items_request_id(
	 p_old_request_id IN t504_consignment.c520_request_id%TYPE,
	 p_new_request_id IN t521_request_detail.c520_request_id%TYPE,
	 p_building_id IN t5052_location_master.c5057_building_id%TYPE
   )
   AS 
   
     BEGIN
     
      UPDATE t521_request_detail 
       SET c520_request_id = p_new_request_id
           , c521_last_updated_date_utc = current_date
      WHERE c520_request_id = p_old_request_id
       AND c205_part_number_id IN (SELECT t5053.c205_part_number_id  
     FROM t5053_location_part_mapping t5053, t5052_location_master t5052, t504_consignment t504, 
	     t505_item_consignment t505, t521_request_detail t521, t520_request t520
     WHERE t5052.c5052_location_id= t5053.c5052_location_id
     AND t5052.c5040_plant_id   =  t504.c5040_plant_id
     AND t5052.c5052_void_fl IS NULL
     AND t5053.c5053_curr_qty  > 0
     AND t5052.c901_location_type = 93320 --Pick Face
     AND t5052.c5051_inv_warehouse_id = 1
     AND t5052.c5057_building_id = p_building_id
     AND t5053.c205_part_number_id = t521.c205_part_number_id
     AND t520.c520_request_id = t521.c520_request_id
     AND t504.c504_consignment_id = t505.c504_consignment_id
     AND t504.c207_set_id IS NULL
     AND t504.c520_request_id = t520.c520_request_id
     AND t521.c520_request_id = p_old_request_id
     AND t5052.c901_status = 93310 --Active
     AND t5052.c5052_void_fl IS NULL
     AND t504.c504_void_fl IS NULL
     AND t505.c505_void_fl  IS NULL
     AND t520.c520_void_fl  IS NULL
     AND t520.c520_delete_fl IS NULL  
      GROUP BY t5053.c205_part_number_id
      );
   
   END gm_update_items_request_id;
   
   
   /***************************************************************************************************************************************
   * Purpose: This procedure is used to update the old consignment id by giving new consignment id in t505_item_consignment table for PMT-33511
   * Author     : Swetha
   * Parameters  : Consignment ID , Building id
 *************************************************************************************************************************************/
	PROCEDURE gm_update_items_consign_id(
	 p_old_consign_id IN t504_consignment.c504_consignment_id %TYPE,
	 p_new_consign_id IN t505_item_consignment.c504_consignment_id%TYPE,
	 p_building_id IN t5052_location_master.c5057_building_id%TYPE
   )
   AS 
   
     BEGIN
     
     
       UPDATE t505_item_consignment 
         SET c504_consignment_id  = p_new_consign_id
           , c505_last_updated_date_utc = current_date  
         WHERE c504_consignment_id = p_old_consign_id
        AND  c205_part_number_id IN (SELECT t5053.c205_part_number_id  
	FROM t5053_location_part_mapping t5053, t5052_location_master t5052, t504_consignment t504, 
	     t505_item_consignment t505, t520_request t520
     WHERE t5052.c5052_location_id= t5053.c5052_location_id
     AND t5052.c5040_plant_id   =  t504.c5040_plant_id
     AND t5052.c5052_void_fl IS NULL
     AND t5053.c5053_curr_qty  > 0
     AND t5052.c901_location_type = 93320 --Pick Face
     AND t5052.c5051_inv_warehouse_id = 1
     AND t5052.c5057_building_id = p_building_id
     --AND t5053.c205_part_number_id = t521.c205_part_number_id --Removed for BUG-11398 for PMT-33507
     --AND t520.c520_request_id = t521.c520_request_id   
     AND t504.c504_consignment_id = t505.c504_consignment_id
     AND t504.c520_request_id = t520.c520_request_id
     AND t505.c504_consignment_id  = p_old_consign_id
     AND t504.c207_set_id IS NULL
     AND t5052.c901_status = 93310 --Active
     AND t5052.c5052_void_fl IS NULL
     AND t504.c504_void_fl IS NULL
     AND t505.c505_void_fl  IS NULL
     AND t520.c520_void_fl  IS NULL
     AND t520.c520_delete_fl IS NULL  
      GROUP BY t5053.c205_part_number_id
      );


 END gm_update_items_consign_id;
END gm_pkg_op_storage_building_cn;
/