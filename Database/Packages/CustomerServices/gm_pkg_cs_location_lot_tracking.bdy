--@"C:\PMTBIT\ERP-SpineIT\Database\Packages\CustomerServices\gm_pkg_cs_location_lot_tracking.bdy"
CREATE OR REPLACE PACKAGE BODY gm_pkg_cs_location_lot_tracking IS
--
 /*********************************************************************************
   * Description : This method used to fetch the all trnasction id based on order id , part num, control number
   * Author 	 : tramasamy
  *******************************************************************************/

    PROCEDURE gm_fch_lot_loc_list (
        p_order_id     IN    t501_order.c501_order_id%TYPE,
        p_part_num     IN    t5111_lot_txn_items.c205_part_number_id%TYPE,
        p_lot_num      IN    t5111_lot_txn_items.c5111_control_number%TYPE,
        p_out_txn_id   OUT   CLOB
    ) 
    AS
    BEGIN
            SELECT JSON_ARRAYAGG(
      			JSON_OBJECT (
               'txnid' VALUE txnid ,
               'qty' VALUE sum(qty),
               'name' VALUE txnid || '(' ||sum(qty) || ')'
                ) returning CLOB) INTO p_out_txn_id
                FROM
                    (
                        SELECT t5111.c5111_txn_id txnid ,t5111.C5111_QTY qty--, t5111.c5111_control_number lotnum
                         FROM t5110_lot_txn t5110,t5111_lot_txn_items t5111
                        WHERE t5110.c5110_lot_txn_id = t5111.c5110_lot_txn_id
                          AND t5110.c5100_lot_location_id IN (
                               SELECT t5100.c5100_lot_location_id
                               FROM t5100_lot_location t5100,t501_order t501
                               WHERE t501.c501_order_id = p_order_id
                                 AND t501.c1900_company_id = t5100.c1900_company_id
                                 AND t501.c501_distributor_id = t5100.c5100_ref_id
                                 AND t5100.c901_ref_type = 109461
                                 AND t5100.c5100_void_fl IS NULL
                                 AND t501.c501_void_fl IS NULL
                               UNION
                               SELECT t5100.c5100_lot_location_id
                                 FROM t5100_lot_location t5100, t501_order t501
                                WHERE t501.c501_order_id = p_order_id
                                  AND t501.c1900_company_id = t5100.c1900_company_id
                                  AND t501.c704_account_id = t5100.c5100_ref_id
                                  AND t5100.c901_ref_type = 109460
                                  AND t5100.c5100_void_fl IS NULL
                                  AND t501.c501_void_fl IS NULL
                           )
                           AND t5111.c205_part_number_id = p_part_num
                           AND (t5111.c5111_control_number = p_lot_num OR t5110.c901_txn_type = 4127) -- product loaner
                           AND t5110.c901_status =109466
                           AND (t5111.C5111_QTY > 0 OR t5111.c5111_txn_id IN (
                              SELECT c502b_ref_id txnid
                           FROM t502b_item_order_usage
                           WHERE c501_order_id = p_order_id
                            and c502b_ref_id is not null
                            AND NVL(c502b_ref_id,' ') <> ' '
                           AND c502b_void_fl IS NULL)
                          )
                           AND t5110.c5110_void_fl IS NULL
                           AND t5111.c5111_void_fl IS NULL
                        ) GROUP BY txnid;
           

    END gm_fch_lot_loc_list;
 /*********************************************************************************
   * Description : This method used to update the lot transaction details to t5111 table 
   * Author 	 : tramasamy
  *******************************************************************************/

    PROCEDURE gm_sav_loc_lot_txn_update (
        p_order_id   IN   t501_order.c501_order_id%TYPE,
        p_user_id    IN   t501_order.c501_created_by%TYPE
    ) AS

        v_qty NUMBER := 0;
    
      -- DO usage detail
        CURSOR cur_order_usage IS
        SELECT pnum,ctrl, txn, SUM(qty) qty
          FROM
            (
                SELECT
                    c205_part_number_id   pnum,
                    c502b_item_qty        qty,
                    c502b_usage_lot_num   ctrl,
                    c502b_ref_id          txn
                FROM
                    t502b_item_order_usage
                WHERE
                    c501_order_id = p_order_id
                    --AND c502b_ref_id IS NOT NULL
                    and NVL(c502b_ref_id,' ') <> ' '
                    AND c502b_void_fl IS NULL
                UNION ALL
    -- get the previous usage record for the DO in the location
                SELECT
                    c205_part_number_id    pnum,
                    c5112_txn_qty          qty,
                    c5112_control_number   ctrl,
                    c5112_txn_id           txn
                FROM
                    t5112_lot_txn_items_log
                WHERE
                    c901_action = 109681 -- usage
                    AND c5112_last_txn_id = p_order_id
                    AND c5112_void_fl IS NULL
            )
        GROUP BY
            pnum,
            ctrl,
            txn  order by qty;

      CURSOR cur_lot_loc (
        p_num    t5111_lot_txn_items.c205_part_number_id%TYPE,
        p_txn    t5111_lot_txn_items.c5111_txn_id%TYPE,
        p_ctrl   t5111_lot_txn_items.c5111_control_number%TYPE
    ) IS
    SELECT
        c5111_lot_txn_items_id   itemid,
        c5111_qty                qty
    FROM
        t5111_lot_txn_items   t5111,
        t5110_lot_txn         t5110
    WHERE
        t5110.c5110_lot_txn_id = t5111.c5110_lot_txn_id
        AND t5110.c5110_void_fl IS NULL
        AND t5111.c205_part_number_id = p_num
        AND t5111.c5111_txn_id = p_txn
        AND ( t5111.c5111_control_number = p_ctrl
              OR t5110.c901_txn_type = 4127 ) -- product loaner
        AND t5111.c5111_void_fl IS NULL
    ORDER BY
        t5111.c5111_lot_txn_items_id; -- ORDER BY FIFO

    BEGIN
        FOR v_order_usage IN cur_order_usage LOOP
            v_qty := v_order_usage.qty;

      --IF NO CHANGE IN OLD QTY THEN 0
            IF v_qty <> 0 THEN
        --minus DO usage qty
                FOR v_lot_loc IN cur_lot_loc(v_order_usage.pnum, v_order_usage.txn, v_order_usage.ctrl) 
                LOOP
                    IF ( v_lot_loc.qty >= v_qty ) THEN
                        UPDATE t5111_lot_txn_items
                        SET
                            c5111_qty = ( v_lot_loc.qty - v_qty ),
                            c901_action = 109681, -- usage
                            c5111_last_txn_id = p_order_id,
                            c5111_updated_by = p_user_id,
                           C5111_UPDATED_DATE = CURRENT_DATE
                        WHERE
                            c5111_lot_txn_items_id = v_lot_loc.itemid;

                        v_qty := 0;
                        EXIT; --EXIT  current loop
                    ELSE
                        UPDATE t5111_lot_txn_items
                        SET
                            c5111_qty = 0,
                            c901_action = 109681, -- usage
                            c5111_last_txn_id = p_order_id,
                            c5111_updated_by = p_user_id,
                            C5111_UPDATED_DATE = CURRENT_DATE
                        WHERE
                            c5111_lot_txn_items_id = v_lot_loc.itemid;

                        v_qty := v_qty - v_lot_loc.qty;
                    END IF;
          
                END LOOP;
       END IF;
         IF v_qty <> 0 THEN
           raise_application_error('-20999','Insuffient qty in location for the part '||v_order_usage.pnum|| ',' ||v_order_usage.ctrl||','||v_qty);
      END IF;
        END LOOP;
    END gm_sav_loc_lot_txn_update;

END gm_pkg_cs_location_lot_tracking;
/
