/* Formatted on 22/10/2018 10:18 (Formatter Plus v4.8.0) */
/* @ "C:\PMT\db\Packages\CustomerServices\gm_pkg_ret_pend_credit.bdy" */

CREATE OR REPLACE PACKAGE BODY gm_pkg_ret_pend_credit
IS

 --
    /*******************************************************
    * Author  : pdhanasekaran
    * Description: This Procedure is used to void the back order
    
    *******************************************************/
    --
    
    PROCEDURE gm_void_open_bo_request (
    
       p_last_id           IN      t5011_tag_log.c5011_last_updated_trans_id%TYPE
       ) AS 
   	 v_last_id			t5011_tag_log.c5011_last_updated_trans_id%TYPE;
    
    BEGIN
			    	
		UPDATE T520_REQUEST SET C520_VOID_FL ='Y',C520_LAST_UPDATED_DATE = SYSDATE WHERE C520_REQUEST_ID IN (			
			  SELECT t520.c520_request_id
			    FROM t520_request t520, t504_consignment t504
			 WHERE  t504.c504_consignment_id = p_last_id
			   AND t520.c520_master_request_id = t504.c520_request_id 
			   AND c520_status_fl        = '10'
			   AND c520_void_fl         IS NULL
			   AND C504_void_fl 	    IS NULL	   	
		);
		
		
     -- END
    	
   END gm_void_open_bo_request;
   
   
   PROCEDURE gm_void_open_ra_credit (
   
   	p_raid	   IN		t506_returns.c506_rma_id%TYPE,
    p_userid		 IN   t102_user_login.c102_user_login_id%TYPE
    ) AS
    v_last_id			t5011_tag_log.c5011_last_updated_trans_id%TYPE;
    
    BEGIN
	    
	    SELECT v_last_id INTO v_last_id  FROM(
		SELECT MAX(c5011_tag_txn_log_id) as v_max_id,
		  c5011_last_updated_trans_id as v_last_id
		FROM t5011_tag_log ,t504_consignment 
		WHERE c5010_tag_id in (SELECT C5010_TAG_ID
		  FROM t5010_tag
		  WHERE C5010_LAST_UPDATED_TRANS_ID = p_raid)
		AND c5011_void_fl IS NULL
        and c5011_last_updated_trans_id = c504_consignment_id
        and c504_void_fl is null
		GROUP BY c5011_last_updated_trans_id,c5011_tag_txn_log_id
        ORDER BY c5011_tag_txn_log_id DESC
		) WHERE ROWNUM =1;             
        
		
		
		
		
		UPDATE T520_REQUEST SET C520_VOID_FL = NULL,C520_LAST_UPDATED_DATE = SYSDATE,C520_LAST_UPDATED_BY= p_userid WHERE C520_REQUEST_ID IN (			
			  SELECT t520.c520_request_id
			    FROM t520_request t520, t504_consignment t504
			 WHERE  t504.c504_consignment_id = v_last_id
			   AND t520.c520_master_request_id = t504.c520_request_id 
			   AND c520_status_fl        = '10'
			   AND c520_void_fl          = 'Y'
			   AND C504_void_fl 	    IS NULL	);
			   
	
	END gm_void_open_ra_credit;
	    
   
  END gm_pkg_ret_pend_credit;
  /