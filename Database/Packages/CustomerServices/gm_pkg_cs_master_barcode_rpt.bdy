--C:\PC\ERP-SpineIT\Database\Packages\CustomerServices\gm_pkg_cs_master_barcode_rpt.bdy;
CREATE OR REPLACE PACKAGE BODY gm_pkg_cs_master_barcode_rpt
IS
/*************************************************************************************************************
	* Description : This procedure is used to fetch the Order or consignment details to generate PDF 
	* PC-3900     : Master Barcode Generate for spineIT Orders/Consignment
	* Author      : tramasamy
***********************************************************************************************************/
PROCEDURE gm_fetch_txn_dtls (
   p_txn_id          IN  VARCHAR2,
  p_out_dtls        OUT	TYPES.cursor_type,
  p_out_header_dtls OUT TYPES.cursor_type)
	
AS
BEGIN
		OPEN p_out_dtls
		 FOR
		SELECT t502.C205_PART_NUMBER_ID PARTNUM, t502.C502_CONTROL_NUMBER LOTNUM,
		       REGEXP_REPLACE(t502.C205_PART_NUMBER_ID,'[^ ,0-9A-Za-z]', '') ||  t2540.C2540_DONOR_NUMBER || t2550.C2550_CONTROL_NUMBER || TO_CHAR(C2550_EXPIRY_DATE ,'yyyyMMdd') UDICODE
		FROM t501_order t501,t502_item_order t502, t2550_part_control_number t2550,t2540_donor_master t2540
		WHERE t501.C501_ORDER_ID=p_txn_id --2698-200903-2
		 AND t501.C501_ORDER_ID = t502.C501_ORDER_ID AND t501.c501_status_fl > '1'
		 AND t2550.C2540_DONOR_ID = t2540.C2540_DONOR_ID
         AND t2550.C2550_CONTROL_NUMBER = t502.C502_CONTROL_NUMBER
		 AND t501.C501_VOID_FL IS NULL  AND t502.C502_VOID_FL IS NULL
		 UNION ALL
		 SELECT t505.C205_PART_NUMBER_ID PARTNUM,t505.C505_CONTROL_NUMBER LOTNUM ,REGEXP_REPLACE(t505.C205_PART_NUMBER_ID,'[^ ,0-9A-Za-z]', '')||  t2540.C2540_DONOR_NUMBER 
		        || t2550.C2550_CONTROL_NUMBER || TO_CHAR(C2550_EXPIRY_DATE ,'yyyyMMdd') UDICODE
		 FROM t504_consignment t504, t505_item_consignment t505 ,t2550_part_control_number t2550,t2540_donor_master t2540
		 WHERE t504.C504_CONSIGNMENT_ID =p_txn_id
		 AND t504.C504_CONSIGNMENT_ID  = t505.C504_CONSIGNMENT_ID 
		  AND t2550.C2540_DONOR_ID = t2540.C2540_DONOR_ID
           AND t2550.C2550_CONTROL_NUMBER = t505.C505_CONTROL_NUMBER
		 AND t504.C504_STATUS_FL > '2' 
		 AND t504.c504_void_fl IS NULL AND t505.C505_VOID_FL IS NULL;
		 
		OPEN p_out_header_dtls FOR
		
             select t501.C501_SHIP_TO , t501.C704_ACCOUNT_ID ,t501.C501_ORDER_ID TXNID,t704.C704_BILL_NAME BILLNM,
              t704.C704_SHIP_NAME SHIPNM , TO_CHAR(t501.C501_ORDER_DATE,'mm/dd/yyyy') ORDDT
				FROM t501_order t501 , T704_ACCOUNT t704
				WHERE t501.C501_ORDER_ID = p_txn_id --2698-200903-2
				--AND t501.C501_ORDER_ID = t907.C907_REF_ID
                AND t704.C704_ACCOUNT_ID = t501.C704_ACCOUNT_ID
				 AND t501.C501_VOID_FL IS NULL
				UNION ALL
				 select t504.C504_SHIP_TO , t504.C701_DISTRIBUTOR_ID ,t504.C504_CONSIGNMENT_ID TXNID, DECODE(t504.C701_DISTRIBUTOR_ID ,null ,t704.C704_BILL_NAME,t701.C701_BILL_NAME) BILLNM ,
				  DECODE(t504.C701_DISTRIBUTOR_ID ,null ,t704.C704_SHIP_NAME ,t701.C701_SHIP_NAME ) SHIPNM , TO_CHAR(t504.C504_CREATED_DATE,'mm/dd/yyyy') ORDDT
				FROM t504_consignment t504 ,t701_distributor t701,T704_ACCOUNT t704
				WHERE t504.C504_CONSIGNMENT_ID = p_txn_id --'GM-CN-3154895'
				 AND t504.C701_DISTRIBUTOR_ID = t701.C701_DISTRIBUTOR_ID(+)
                  AND t504.C704_ACCOUNT_ID = t704.C704_ACCOUNT_ID(+)
				  AND t701.C701_VOID_FL  IS NULL  AND t504.c504_void_fl IS NULL ;
				
				
		
	
END gm_fetch_txn_dtls;
END gm_pkg_cs_master_barcode_rpt;
/