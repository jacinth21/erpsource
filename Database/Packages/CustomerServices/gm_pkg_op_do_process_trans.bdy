/* Formatted on 2011/12/30 14:44 (Formatter Plus v4.8.0) */
--@C:\Database\Packages\CustomerServices\gm_pkg_op_do_process_trans.bdy;

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_do_process_trans
IS
   PROCEDURE gm_sav_usage_details_main (
      p_orderid         IN   t501_order.c501_order_id%TYPE,
      p_parent_ord_id   IN   t501_order.c501_parent_order_id%TYPE,
      p_case_id         IN   t501_order.c7100_case_info_id%TYPE,
      p_str             IN   VARCHAR2,
      p_accid           IN   t501_order.c704_account_id%TYPE,
      p_customer_po     IN   t501_order.c501_customer_po%TYPE,
      p_total_cost      IN   t501_order.c501_total_cost%TYPE,
      p_comments        IN   t501_order.c501_comments%TYPE,
      p_action          IN   VARCHAR2,
      p_userid          IN   t501_order.c501_created_by%TYPE,
      p_rep_id			IN   t501_order.C703_SALES_REP_ID%TYPE DEFAULT NULL,
      p_surgery_dt		IN	 t501_order.c501_surgery_date%TYPE
   )
   AS
    v_parent_ord_id 	t501_order.c501_parent_order_id%TYPE;
   	v_order_id  	 	t501_order.c501_parent_order_id%TYPE;   	
   BEGIN
	  -- validate parent DO exist 
	  IF p_action = 'NEWDO'
      THEN
		  BEGIN
		     	SELECT c501_order_id
		     	  INTO v_parent_ord_id
				  FROM t501_order
				 WHERE c7100_case_info_id = p_case_id 
				   AND c501_void_fl IS NULL
				   AND c501_parent_order_id IS NULL;
	      EXCEPTION
	         WHEN NO_DATA_FOUND
	         THEN
	         	v_parent_ord_id := NULL;
		  END;
		  --check existing order shipping status
		  BEGIN		
			  SELECT c501_order_id INTO v_order_id
			    FROM t501_order 
			   WHERE c7100_case_info_id = p_case_id
			     AND c501_shipping_date IS NULL
			     AND c501_void_fl IS NULL;
	      EXCEPTION
	         WHEN NO_DATA_FOUND
	         THEN
	         	v_order_id := NULL;
		  END;	
		  
		  IF v_parent_ord_id IS NOT NULL AND p_parent_ord_id IS NULL
	      THEN
	         raise_application_error('-20999','Please edit the existing DO ' || v_parent_ord_id || ' for the case.');
	      ELSIF v_order_id IS NOT NULL
	      THEN
      		 raise_application_error('-20999','Please edit the existing DO ' || v_order_id || ' for the case.');
	      END IF;  
      END IF;
      
      gm_pkg_op_do_process_trans.gm_validate_order (p_orderid, p_case_id,p_action,p_userid);
      gm_pkg_op_do_process_trans.gm_sav_order (p_orderid,
                                               p_parent_ord_id,
                                               p_case_id,
                                               p_action,
                                               p_customer_po,
                                               p_total_cost,
                                               p_userid,
                                               p_accid,
                                               p_rep_id,
                                               p_surgery_dt
                                              );
      IF p_action = 'EDITDO'
      THEN
         gm_pkg_op_do_process_trans.gm_sav_item_backup_del (p_orderid);
         
      END IF;

      gm_pkg_op_do_process_trans.gm_sav_order_item (p_orderid,
                                                    p_str,
                                                    p_userid,
                                                    p_action
                                                   );

      IF p_action = 'EDITDO'
      THEN
         gm_pkg_op_do_process_trans.gm_sav_sync_item_attribute (p_orderid,
                                                                p_userid
                                                               );
      ELSE
         gm_pkg_op_do_process_trans.gm_sav_shipping (p_orderid,
                                                     p_case_id,
                                                     p_userid
                                                    );
      END IF;

      gm_pkg_op_do_process_trans.gm_sav_tag (p_orderid, p_case_id, p_userid);
      
      --To record Usage Lot#s for Sterile Parts used in Surgery for DOs booked from the 'Case Book' screen.
      gm_pkg_op_do_process_trans.gm_sav_case_book_order_usage(p_orderid, p_userid);
      
      -- For PMT-24435 
	-- Updating Valid Flag as 'Y' while DOs booked from the 'Case Book' screen.
	gm_pkg_order_tag_record.gm_upd_DO_record_tags(p_orderid,p_userid);
	
   END gm_sav_usage_details_main;

   PROCEDURE gm_sav_order (
      p_order_id          IN   t501_order.c501_order_id%TYPE,
      p_parent_order_id   IN   t501_order.c501_order_id%TYPE,
      p_case_id           IN   t501_order.c7100_case_info_id%TYPE,
      p_action            IN   VARCHAR2,
      p_customer_po       IN   t501_order.c501_customer_po%TYPE,
      p_total_cost        IN   t501_order.c501_total_cost%TYPE,
      p_user_id           IN   t501_order.c501_created_by%TYPE,
      p_accid           IN   t501_order.c704_account_id%TYPE DEFAULT NULL,
      p_rep_id			IN   t501_order.C703_SALES_REP_ID%TYPE DEFAULT NULL,
      p_surgery_dt		IN	 t501_order.c501_surgery_date%TYPE
   )
   AS
      v_ship_mode           NUMBER;
      v_carrier             NUMBER;
      v_ship_to             NUMBER;
      v_shipping_ref_type   NUMBER;
      v_order_type          NUMBER;
      v_case_info_id        t7100_case_information.c7100_case_info_id%TYPE;
      v_surgery_date        t7100_case_information.c7100_surgery_date%TYPE;
      v_surgery_time        t7100_case_information.c7100_surgery_time%TYPE;
      v_dist_id             t7100_case_information.c701_distributor_id%TYPE;
      v_rep_id              t7100_case_information.c703_sales_rep_id%TYPE;
      v_acct_id             t7100_case_information.c704_account_id%TYPE;
      v_address_id          VARCHAR2 (10);
      v_shipcost            VARCHAR2 (10);
      v_thirdpartyaccid     VARCHAR2 (10);
      v_account_id          t501_order.c704_account_id%TYPE;
      v_order_status        NUMBER;
      v_case_id				NUMBER;
      v_order_source		VARCHAR2(10);
      v_receive_mode		NUMBER;
                                -- 7 -> Pending Release, 8 --> Released to CS
   -- Tag
   BEGIN
      v_ship_mode := 5004;                 -- defaulted to priority overnight
      v_carrier := 5001;                                 --defaulted to fedex
      v_ship_to := 4121;                            -- defaulted to Sales Rep
      v_shipping_ref_type := 50180;                    -- defaulted to Orders
      v_order_type := 2518;                                          -- Draft
      v_acct_id := p_accid;
      v_rep_id  := p_rep_id;
      v_receive_mode := 5018;  -- 5018 - Online 

      BEGIN
         SELECT DECODE (c901_dept_id, 2005, 7, 8)
           INTO v_order_status
           FROM t101_user t101
          WHERE t101.c101_user_id = p_user_id AND t101.c901_user_status = 311;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            v_order_status := 7;
      END;

      BEGIN
         SELECT c7100_case_info_id, GET_REP_ID(c704_account_id), c704_account_id 
           INTO v_case_info_id, v_rep_id, v_acct_id 
           FROM t7100_case_information t7100
          WHERE t7100.c7100_case_info_id = p_case_id
            AND t7100.c901_case_status = 11091                        --active
            AND t7100.c7100_void_fl IS NULL;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
               v_case_id := '';         
      END;
	  
      SELECT get_order_attribute_value(p_order_id,'103560') -- DO APP
		  INTO  v_order_source  
		  FROM dual; 
		  
      IF v_order_source = '103521' THEN
      		v_receive_mode := '26230683'; --IPAD
      END IF;--once save the order from usage details order type in do summary screen is changing
      
      IF v_shipcost IS NULL AND v_order_source  <>'103521'
      THEN
				gm_pkg_op_ship_charge.gm_fch_ship_cost_for_order (
						p_order_id		
					  , 50180		
					  , v_ship_to 		
					  , v_rep_id	
					  , NULL	
					  , v_carrier	 	
					  , v_ship_mode		
					  , NULL	
					  , v_shipcost			
					);	
      END IF;

      gm_pkg_op_do_master.gm_sav_order_master (p_order_id,
                                               v_acct_id,
                                               v_rep_id,
                                               NULL,
                                               p_customer_po,
                                               trunc(CURRENT_DATE),
                                               NULL,
                                               p_total_cost,
                                               v_carrier,
                                               v_ship_mode,
                                               NULL,
                                               v_ship_to,
                                               v_order_status,
                                               v_receive_mode,
                                               NULL,
                                               NULL,
                                               NULL,
                                               v_rep_id,
                                               v_shipcost,
                                               NULL,
                                               CURRENT_DATE,
                                               v_order_type,
                                               NULL,
                                               p_parent_order_id,
                                               NULL,
                                               NULL,
                                               NULL,
                                               p_case_id,
                                               p_user_id,
                                               p_action,
                                               p_surgery_dt
                                              );
      gm_pkg_op_do_master.gm_sav_order_attribute_master
                                              (NULL,
                                               p_order_id,
                                               11240,  -- Order Requested Date
                                               TO_CHAR (CURRENT_DATE,
                                                        'MM/DD/YYYY HH:MI:SS'
                                                       ),
                                               p_user_id
                                              );

       -- Save the status as Draft Created (New Code Lookup Type) if new delivered order
       IF p_action = 'NEWDO'
       THEN
           gm_pkg_cm_status_log.gm_sav_status_details(p_order_id, NULL, p_user_id, NULL, CURRENT_DATE, 101860, 91102);
	       -- Draft Created
       END IF;

   END gm_sav_order;

   PROCEDURE gm_sav_order_item (
      p_order_id   IN   t502_item_order.c501_order_id%TYPE,
      p_inputstr   IN   VARCHAR2,
      p_user_id    IN   t501_order.c501_created_by%TYPE,
      p_action     IN   VARCHAR2
   )
   AS
      v_strlen           NUMBER               := NVL (LENGTH (p_inputstr), 0);
      v_string           VARCHAR2 (4000)                        := p_inputstr;
      v_substring        VARCHAR2 (1000);
      v_pnum             t502_item_order.c205_part_number_id%TYPE;
      v_qty              t502_item_order.c502_item_qty%TYPE;
      v_price            t502_item_order.c502_item_price%TYPE;
      v_type             t502_item_order.c901_type%TYPE;
      v_partnumber       t205_part_number.c205_part_number_id%TYPE;
      v_attribute_type   NUMBER                                     := 100820;
      v_attribute_id     t502_item_order.c502_attribute_id%TYPE;
      v_cnt              NUMBER;
      v_item_order_id    t502_item_order.c502_item_order_id%TYPE;
      v_item_type Number;
      v_order_info_id    t502_item_order.c500_order_info_id%TYPE;
      v_item_order_usage_id t502b_item_order_usage.c502b_item_order_usage_id%type;
   BEGIN
      IF v_strlen > 0
      THEN
         WHILE INSTR (v_string, '|') <> 0
         LOOP
            --
            v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
            v_string := SUBSTR (v_string, INSTR (v_string, '|') + 1);
            v_pnum :=TRIM (SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1));
            v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
            v_qty :=TO_NUMBER (TRIM ((SUBSTR (v_substring,1,INSTR (v_substring, '^') - 1))));
            v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
            v_price := TO_NUMBER (TRIM ((SUBSTR (v_substring, 1,INSTR (v_substring, '^') - 1))));
            v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
            v_type := TO_NUMBER (TRIM ((SUBSTR (v_substring,1,INSTR (v_substring, '^') - 1))));
            v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
            v_item_type := TO_NUMBER (TRIM (v_substring));
            --The input String is changed for MDO-109 Sync DO.We are getting Item Type either Loaner or Consignment
            v_attribute_id := '';

            BEGIN
               SELECT c205_part_number_id
                 INTO v_partnumber
                 FROM t205_part_number
                WHERE c205_part_number_id = v_pnum AND c205_active_fl = 'Y';
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  v_partnumber := NULL;
            END;

            IF v_partnumber IS NULL
            THEN
               raise_application_error
                  ('-20012',
                   'Part Number is not active  or part number does not exist'
                  );
            END IF;

            
            IF p_action = 'NEWDO'
            THEN
               SELECT s502_attribute_id.NEXTVAL
                 INTO v_attribute_id
                 FROM DUAL;
            END IF;

         
            gm_pkg_op_do_master.gm_sav_order_item_master (NULL,
                                                          p_order_id,
                                                          v_partnumber,
                                                          v_qty,
                                                          NULL,
                                                          v_price,
                                                          NULL,
                                                          v_item_type,
                                                          NULL,
                                                          NULL,
                                                          NULL,
                                                          v_attribute_id,
                                                          NULL,
                                                          NULL,
                                                          v_item_order_id
                                                         );

            SELECT COUNT (1)
              INTO v_cnt
              FROM t502a_item_order_attribute
             WHERE c501_order_id = p_order_id
               AND c205_part_number_id = v_partnumber
               AND c901_attribute_type = v_attribute_type
               AND c502a_attribute_value = TO_CHAR(v_type)
               AND c502a_void_fl IS NULL;

            IF p_action = 'EDITDO'
            THEN
               v_attribute_id := v_item_order_id;
            END IF;

            IF v_attribute_id IS NOT NULL
            THEN
               gm_pkg_op_do_master.gm_sav_item_attr_master (NULL,
                                                            p_order_id,
                                                            v_partnumber,
                                                            v_attribute_type,
                                                            v_type,
                                                            v_attribute_id,
                                                            p_user_id
                                                           );
            END IF;            
         END LOOP;
         
          --Whenever the child is updated, the parent table has to be updated ,so ETL can Sync it to GOP.
		  UPDATE t501_order
		     SET c501_last_updated_date = CURRENT_DATE
		       , c501_last_updated_by = p_user_id
		  WHERE c501_order_id        = p_order_id;
  
      END IF;
   END gm_sav_order_item;

   PROCEDURE gm_sav_tag (
      p_order_id   IN   t501_order.c501_order_id%TYPE,
      p_case_id    IN   t7104_case_set_information.c7100_case_info_id%TYPE,
      p_user_id    IN   t501_order.c501_created_by%TYPE
   )
   AS
      v_order_id   t502a_item_order_attribute.c501_order_id%TYPE;
      v_partnum    t502a_item_order_attribute.c205_part_number_id%TYPE;
      v_tagid      t502a_item_order_attribute.c502a_attribute_value%TYPE;
      v_attr_id    t502a_item_order_attribute.c502_attribute_id%TYPE;
      v_tag_type   NUMBER;
      v_qty        NUMBER;

      -- Tag
      CURSOR cur_tag
      IS
         SELECT DO.c501_order_id orderid, DO.c205_part_number_id pnum,
                cas.c5010_tag_id tagid, DO.qty
           FROM (SELECT t502.c501_order_id, t502.c7100_case_info_id,
                        t502.c205_part_number_id,
                        (NVL (do_qty, 0) - NVL (t5012.c5012_qty, 0)) qty
                   FROM (SELECT   t501.c501_order_id, t501.c7100_case_info_id,
                                  t502.c205_part_number_id,
                                  SUM (NVL (t502.c502_item_qty, 0)) do_qty
                             FROM t501_order t501, t502_item_order t502
                            WHERE t501.c501_order_id = p_order_id
                              AND t501.c501_order_id = t502.c501_order_id
                              AND t501.c501_delete_fl IS NULL
                              AND t501.c501_void_fl IS NULL
                              AND t502.c502_void_fl IS NULL
                         GROUP BY t501.c501_order_id,
                                  t501.c7100_case_info_id,
                                  t502.c205_part_number_id) t502,
                        t5012_tag_usage t5012
                  WHERE t502.c7100_case_info_id = t5012.c5012_ref_id(+)
                    AND t5012.c901_ref_type(+) = 2570                  -- Case
                    AND t502.c205_part_number_id = t5012.c205_part_number_id(+)
                    AND t5012.c5012_void_fl(+) IS NULL
                    AND NVL (do_qty, 0) - NVL (t5012.c5012_qty, 0) > 0) DO,
                (SELECT   MIN (t7104.c5010_tag_id) c5010_tag_id,
                          t208.c205_part_number_id, t7104.c7100_case_info_id,
                          SUM (t208.c208_set_qty) qty
                     FROM t7104_case_set_information t7104,
                          t5010_tag t5010,                          
                          (select * from t208_set_details t208 where c208_void_fl is NULL and c208_inset_fl = 'Y') t208
                    WHERE t7104.c7100_case_info_id = p_case_id
                      AND t7104.c5010_tag_id IS NOT NULL
                      AND t7104.c5010_tag_id = t5010.c5010_tag_id
                      AND t5010.c5010_void_fl IS NULL
                      AND t7104.c7104_void_fl IS NULL
                      AND t7104.c7104_shipped_del_fl IS NULL
                      AND t5010.c207_set_id = t208.c207_set_id
                      AND t7104.c5010_tag_id NOT IN (
                             SELECT t5012.c5010_tag_id
                               FROM t5012_tag_usage t5012
                              WHERE t5012.c5012_ref_id = p_case_id
                                AND t5012.c901_ref_type = 2570
                                AND t5012.c5012_void_fl IS NULL
                                AND t208.c205_part_number_id =
                                                     t5012.c205_part_number_id)
                 GROUP BY t208.c205_part_number_id, t7104.c7100_case_info_id
                   HAVING COUNT (t7104.c5010_tag_id) = 1) cas
          WHERE DO.c7100_case_info_id = cas.c7100_case_info_id
            AND DO.c205_part_number_id = cas.c205_part_number_id
            AND cas.qty >= DO.qty;
   BEGIN
      FOR var_tag_detail IN cur_tag
      LOOP
         v_order_id := var_tag_detail.orderid;
         v_partnum := var_tag_detail.pnum;
         v_tagid := var_tag_detail.tagid;
         v_qty := var_tag_detail.qty;
         gm_pkg_op_do_master.gm_sav_tag_master (NULL,
                                                p_case_id,
                                                v_partnum,
                                                2570,
                                                v_tagid,
                                                v_qty,
                                                p_user_id
                                               );
      END LOOP;

	IF p_case_id IS NOT NULL THEN  --PC-2201-Header Edit Option in DO Approval Pending Confirmation Screen
      	gm_sav_sync_tag (p_order_id, p_case_id, p_user_id);
	END IF;
	
      gm_sav_tag_type (p_order_id, 'EDIT_USAGE');
   END gm_sav_tag;

   PROCEDURE gm_sav_sync_tag (
      p_order_id   IN   t501_order.c501_order_id%TYPE,
      p_case_id    IN   t7104_case_set_information.c7100_case_info_id%TYPE,
      p_user_id    IN   t501_order.c501_created_by%TYPE
   )
   AS
      CURSOR v_cur
      IS
         SELECT my_temp.c205_part_number_id pnum,
                NVL (my_temp.c502_item_qty, 0) qty,
                NVL (t502.c502_item_qty, 0) t502_qty
           FROM t502_item_order t502, my_temp_t520_item_order my_temp
          WHERE my_temp.c205_part_number_id = t502.c205_part_number_id(+)
            AND my_temp.c501_order_id = t502.c501_order_id(+)
            AND t502.c502_void_fl(+) IS NULL
            AND my_temp.c502_void_fl IS NULL
            AND my_temp.c501_order_id = p_order_id;
   BEGIN
      FOR v_index IN v_cur
      LOOP
         IF v_index.t502_qty = 0
         THEN
            UPDATE t5012_tag_usage
               SET c5012_void_fl = 'Y',
                   c5012_last_updated_by = p_user_id,
                   c5012_last_updated_date = CURRENT_DATE
             WHERE c5012_ref_id = p_case_id
               AND c205_part_number_id = v_index.pnum
               AND c5012_qty = v_index.qty
               AND c5012_void_fl IS NULL
               AND ROWNUM = 1;
         ELSIF (v_index.qty - v_index.t502_qty) != 0
         THEN
            UPDATE t5012_tag_usage
               SET c5012_qty = v_index.t502_qty,
                   c5012_last_updated_by = p_user_id,
                   c5012_last_updated_date = CURRENT_DATE
             WHERE c5012_ref_id = p_case_id
               AND c205_part_number_id = v_index.pnum
               AND c5012_qty = v_index.qty
               AND c5012_void_fl IS NULL
               AND ROWNUM = 1;
         END IF;
      END LOOP;
   END;

   PROCEDURE gm_sav_tag_type (
      p_order_id   IN   t501_order.c501_order_id%TYPE,
      p_action     IN   VARCHAR2
   )
   AS
      v_tag_type   NUMBER;

      CURSOR cur_type
      IS
         SELECT t502.c502_item_order_id item_ord_id,
                t501.c7100_case_info_id case_id,
                t502.c205_part_number_id pnum, t502.c502_item_qty qty
           FROM t502_item_order t502, t501_order t501
          WHERE t502.c501_order_id = p_order_id
            AND t501.c501_order_id = t502.c501_order_id
            AND t501.c501_delete_fl IS NULL
            AND t501.c501_void_fl IS NULL
            AND c502_void_fl IS NULL
            AND c901_type IS NULL;
   BEGIN
      FOR v_cur_type IN cur_type
      LOOP
         v_tag_type := '';

         BEGIN
            SELECT DECODE (c901_set_location_type, 11381, 50301, 50300)
              INTO v_tag_type
              FROM t7104_case_set_information t7104, t5012_tag_usage t5012
             WHERE c7100_case_info_id = v_cur_type.case_id
               AND t5012.c205_part_number_id = v_cur_type.pnum
               AND t5012.c5012_qty = v_cur_type.qty
               AND t7104.c5010_tag_id = t5012.c5010_tag_id
               AND t5012.c5012_void_fl IS NULL
               AND t7104.c7104_void_fl IS NULL
               AND t7104.c7104_shipped_del_fl IS NULL
               AND ROWNUM = 1;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               BEGIN
                  SELECT DECODE (c901_set_location_type, 11381, 50301, 50300)
                    INTO v_tag_type
                    FROM t7104_case_set_information t7104,
                         t5012_tag_usage t5012
                   WHERE c7100_case_info_id = v_cur_type.case_id
                     AND t5012.c205_part_number_id = v_cur_type.pnum
                     AND t7104.c5010_tag_id = t5012.c5010_tag_id
                     AND t5012.c5012_void_fl IS NULL
                     AND t7104.c7104_void_fl IS NULL
                     AND t7104.c7104_shipped_del_fl IS NULL
                     AND ROWNUM = 1;
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     v_tag_type := '';
               END;
         END;

         IF v_tag_type IS NULL AND p_action = 'EDIT_TAG'
         THEN
            v_tag_type := 50300;
         END IF;

         UPDATE t502_item_order
            SET c901_type = v_tag_type
          WHERE c502_item_order_id = v_cur_type.item_ord_id;
      END LOOP;     
         --When ever the detail is updated, the order table has to be updated ,so ETL can Sync it to GOP.
         UPDATE t501_order
            SET c501_last_updated_date = CURRENT_DATE
          WHERE c501_order_id = p_order_id;            
   
   END gm_sav_tag_type;

   PROCEDURE gm_sav_shipping (
      p_orderid   IN   t501_order.c501_order_id%TYPE,
      p_case_id   IN   t7100_case_information.c7100_case_info_id%TYPE,
      p_userid    IN   t501_order.c501_created_by%TYPE
   )
   AS
      v_ship_mode           NUMBER;        -- defaulted to priority overnight
      v_carrier             NUMBER;                      --defaulted to fedex
      v_ship_to             NUMBER;                 -- defaulted to Sales Rep
      v_shipping_ref_type   NUMBER;                    -- defaulted to Orders
      v_order_type          NUMBER;                                  -- Draft
      v_accid               t501_order.c704_account_id%TYPE;
      v_case_info_id        t7100_case_information.c7100_case_info_id%TYPE;
      v_surgery_date        t7100_case_information.c7100_surgery_date%TYPE;
      v_surgery_time        t7100_case_information.c7100_surgery_time%TYPE;
      v_dist_id             t7100_case_information.c701_distributor_id%TYPE;
      v_rep_id              t7100_case_information.c703_sales_rep_id%TYPE;
      v_acct_id             t7100_case_information.c704_account_id%TYPE;
      v_address_id          VARCHAR2 (10);
      v_shipcost            VARCHAR2 (10);
      v_ship_id             VARCHAR2 (20);
 	  v_case_id             NUMBER; 
 	  v_order_source		VARCHAR2 (20);
	  v_ship_cost			t9072_shipping_cost.c9072_package_cost%TYPE;
   BEGIN
      v_ship_mode := 5004;                 -- defaulted to priority overnight
      v_carrier := 5001;                                 --defaulted to fedex
      v_ship_to := 4121;                            -- defaulted to Sales Rep
      v_shipping_ref_type := 11389;              -- defaulted to Draft Orders
      v_order_type := 2518;                                          -- Draft

      SELECT c704_account_id
        INTO v_accid
        FROM t501_order
       WHERE c501_order_id = p_orderid AND c501_delete_fl IS NULL;

      BEGIN
         SELECT c7100_case_info_id, c7100_surgery_date, c7100_surgery_time,
                c701_distributor_id, c703_sales_rep_id, c704_account_id,
                get_address_id (c703_sales_rep_id),
                GET_ACCOUNT_ATTRB_VALUE (v_accid,'91981')
           INTO v_case_info_id, v_surgery_date, v_surgery_time,
                v_dist_id, v_rep_id, v_acct_id,
                v_address_id,
                v_shipcost
           FROM t7100_case_information t7100
          WHERE t7100.c7100_case_info_id = p_case_id
            AND t7100.c901_case_status = 11091                        --active
            AND t7100.c7100_void_fl IS NULL;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN         
         	v_case_id := '';
      END;
	  
      SELECT get_order_attribute_value(p_orderid,'103560') -- DO APP
		  INTO  v_order_source  
		  FROM dual;
      
      IF v_case_id IS NULL THEN
		  SELECT GET_ACCOUNT_ATTRB_VALUE (v_accid,'91981')
		    INTO v_shipcost
		    FROM DUAL;  
	  END IF;      
      
      IF v_shipcost IS NULL AND v_order_source  <>'103521'
      THEN
         -- 5004 priority overnight
         v_shipcost := NVL (TRIM (get_code_name_alt (v_ship_mode)), 0);
      END IF;

      gm_pkg_cm_shipping_trans.gm_sav_shipping (p_orderid,
                                                v_shipping_ref_type,
                                                v_ship_to,
                                                v_rep_id,
                                                v_carrier,
                                                v_ship_mode,
                                                NULL,
                                                v_address_id,
                                                v_shipcost,
                                                p_userid,
                                                v_ship_id
                                               );

/* Fetch the Ship Cost for the account */

	gm_pkg_op_ship_charge.gm_fch_ship_cost_for_order (
		    p_orderid		
		  , 50180		
		  , v_ship_to 		
		  , v_rep_id	
		  , v_address_id	
		  , v_carrier	 	
		  , v_ship_mode		
		  , NULL	
		  , v_ship_cost			
		);

	/* Update the ship Cost, so it reflects on the invoice */
		gm_update_ship_cost (p_orderid, NVL(v_ship_cost,0), p_userid);

   END gm_sav_shipping;

   PROCEDURE gm_sav_item_backup_del (
      p_order_id   IN   t501_order.c501_order_id%TYPE
   )
   AS
   BEGIN
      DELETE FROM my_temp_t520_item_order;

      INSERT INTO my_temp_t520_item_order
       (c502_item_order_id,
		c501_order_id,
		c205_part_number_id,
		c502_item_qty,
		c502_control_number,
		c502_item_price,
		c502_void_fl,
		c502_delete_fl,
		c502_item_seq_no,
		c901_type,
		c502_construct_fl,
		c502_ref_id,
		c502_vat_rate,
		c502_attribute_id,
		c502_list_price,
		c502_unit_price,
		c901_unit_price_adj_code,
		c502_unit_price_adj_value,
		c901_discount_type,
		c502_system_cal_unit_price,
		c502_discount_offered,
		c502_do_unit_price,
		c502_adj_code_value,
		c500_order_info_id,
		c502_ext_item_price,
		c502_tax_amt,
		c502_igst_rate,
		c502_cgst_rate,
		c502_sgst_rate,
		c502_hsn_code,
		c7200_rebate_rate,
		c502_rebate_process_fl,
		c502_tcs_rate)
		
         SELECT c502_item_order_id,
                t501.c501_order_id,
                c205_part_number_id, c502_item_qty, c502_control_number,
                c502_item_price, c502_void_fl, c502_delete_fl,
                c502_item_seq_no, c901_type, c502_construct_fl, c502_ref_id,
                c502_vat_rate, c502_attribute_id,
                c502_list_price,
	    		c502_unit_price,
	    		c901_unit_price_adj_code,
	    		c502_unit_price_adj_value,
	    		c901_discount_type,
	    		c502_system_cal_unit_price,
	    		c502_discount_offered,
	    		C502_DO_UNIT_PRICE,
	    		C502_ADJ_CODE_VALUE,
	    		c500_order_info_id,
	    		c502_ext_item_price,
	    		c502_tax_amt,
      	   		c502_igst_rate,
      	   		c502_cgst_rate,
      	   		c502_sgst_rate,
           		c502_hsn_code ,
           		-- PMT-28394 (Added the column for Rebate Management)
           		c7200_rebate_rate,
           		c502_rebate_process_fl,
           		c502_tcs_rate
           FROM t502_item_order t502, t501_order t501
          WHERE t501.c501_order_id = t502.c501_order_id
            AND (   t501.c501_order_id = p_order_id
                 OR t501.c501_parent_order_id = p_order_id
                )
            AND c502_void_fl IS NULL
            AND c501_void_fl IS NULL;

      DELETE      t502_item_order
            WHERE c501_order_id = p_order_id;
 
       --Whenever the detail is updated, the order table has to be updated ,so ETL can Sync it to GOP.
      UPDATE t501_order
		 SET c501_last_updated_date = CURRENT_DATE
       WHERE c501_order_id      = p_order_id;            
   END gm_sav_item_backup_del;

   PROCEDURE gm_sav_sync_item_attribute (
      p_order_id   IN   t501_order.c501_order_id%TYPE,
      p_userid     IN   t501_order.c501_created_by%TYPE
   )
   AS
      v_pnum                t502_item_order.c205_part_number_id%TYPE;
      v_part_attribute_id   t502_item_order.c502_attribute_id%TYPE;
      v_item_order_id       t502_item_order.c502_item_order_id%TYPE;
      v_price               t502_item_order.c502_item_price%TYPE;
      v_type                t502_item_order.c901_type%TYPE;
      v_price_temp          t502_item_order.c502_item_price%TYPE;
      v_type_temp           t502_item_order.c901_type%TYPE;
      v_attribute_id        t502_item_order.c502_attribute_id%TYPE;
      v_cnum                t502_item_order.c502_control_number%TYPE;
      v_cnum_temp           t502_item_order.c502_control_number%TYPE;
      v_cnt                 NUMBER;
      v_update_fl           VARCHAR2 (20);
      v_item_seq_no         t502_item_order.c502_item_seq_no%TYPE;
      v_construct_fl        t502_item_order.c502_construct_fl%TYPE;
      v_ref_id              t502_item_order.c502_ref_id%TYPE;
      v_vat_rate            t502_item_order.c502_vat_rate%TYPE;
      v_item_seq_no_temp    t502_item_order.c502_item_seq_no%TYPE;
      v_construct_fl_temp   t502_item_order.c502_construct_fl%TYPE;
      v_ref_id_temp         t502_item_order.c502_ref_id%TYPE;
      v_vat_rate_temp       t502_item_order.c502_vat_rate%TYPE;
      v_tax_amount          t502_item_order.c502_tax_amt%TYPE;
      v_tax_amount_temp		t502_item_order.c502_tax_amt%TYPE;
      v_qty                 NUMBER;
      v_list_price			t502_item_order.c502_list_price%TYPE;
      v_unit_price 			t502_item_order.c502_unit_price%TYPE;
      v_unit_prc_adj_code 	t502_item_order.c901_unit_price_adj_code%TYPE;
      v_unit_price_adj_val  t502_item_order.c502_unit_price_adj_value%TYPE;
      v_disc_offered 		t502_item_order.c502_discount_offered%TYPE;
      v_disc_type			t502_item_order.c901_discount_type%TYPE;
      v_sys_cal_unit_price  t502_item_order.c502_system_cal_unit_price%TYPE;
      v_account_price		t502_item_order.C502_DO_UNIT_PRICE%TYPE;
      v_adj_code_value 		t502_item_order.C502_ADJ_CODE_VALUE%TYPE;      

      CURSOR cur_t502
      IS
         SELECT   c502_item_order_id, c501_order_id, c205_part_number_id,
                  c502_item_qty, c502_control_number, c502_item_price,
                  c502_item_seq_no, c901_type, c502_construct_fl,
                  c502_ref_id, c502_vat_rate, c502_attribute_id,
                  c502_tax_amt
             FROM t502_item_order t502
            WHERE t502.c501_order_id = p_order_id
              AND t502.c502_attribute_id IS NULL
              AND t502.c502_void_fl IS NULL
         ORDER BY c502_item_order_id;

      CURSOR cur_temp
      IS
         SELECT   c502_item_order_id, c501_order_id, c205_part_number_id,
                  c502_item_qty, c502_control_number, c502_item_price,
                  c502_item_seq_no, c901_type, c502_construct_fl, c502_ref_id,
                  c502_vat_rate, c502_attribute_id,
                  c502_list_price, c502_unit_price, c901_unit_price_adj_code,
      	   		  c502_unit_price_adj_value, c502_discount_offered,
      	   		  c901_discount_type, c502_system_cal_unit_price , C502_DO_UNIT_PRICE,C502_ADJ_CODE_VALUE,
      	   		  c502_tax_amt
             FROM my_temp_t520_item_order t502
            WHERE t502.c501_order_id = p_order_id
              AND t502.c502_attribute_id IS NOT NULL
              AND t502.c502_void_fl IS NULL
              AND t502.c205_part_number_id = v_pnum
         ORDER BY c502_item_order_id;

      CURSOR cur_attr
      IS
         SELECT t502.c502_attribute_id att_id,
                t502a.c502a_item_order_attribute_id item_att_id
           FROM t502a_item_order_attribute t502a, t502_item_order t502
          WHERE t502.c501_order_id = p_order_id
            AND t502.c502_item_order_id = t502a.c502_attribute_id
            AND T502a.C901_attribute_type = '100820'
            AND t502.c502_void_fl IS NULL
            AND t502a.c502a_void_fl IS NULL;
   BEGIN
      -- Loop and update attribute id for the ones which doesn't have
      FOR v_t502_index IN cur_t502
      LOOP
         v_part_attribute_id := '';
         v_item_order_id := v_t502_index.c502_item_order_id;
         v_pnum := v_t502_index.c205_part_number_id;
         v_qty := v_t502_index.c502_item_qty;
         v_price := v_t502_index.c502_item_price;
         v_type := v_t502_index.c901_type;
         v_cnum := v_t502_index.c502_control_number;
         v_item_seq_no := v_t502_index.c502_item_seq_no;
         v_construct_fl := v_t502_index.c502_construct_fl;
         v_ref_id := v_t502_index.c502_ref_id;
         v_vat_rate := v_t502_index.c502_vat_rate;
		 v_tax_amount := v_t502_index.c502_tax_amt;
		
         FOR v_temp_index IN cur_temp
         LOOP
            v_price_temp := v_temp_index.c502_item_price;
            v_type_temp := v_temp_index.c901_type;
            v_attribute_id := v_temp_index.c502_attribute_id;
            v_cnum_temp := v_temp_index.c502_control_number;
            v_item_seq_no_temp := v_temp_index.c502_item_seq_no;
            v_construct_fl_temp := v_temp_index.c502_construct_fl;
            v_ref_id_temp := v_temp_index.c502_ref_id;
            v_vat_rate_temp := v_temp_index.c502_vat_rate;
            v_tax_amount_temp := v_temp_index.c502_tax_amt;
            
            v_update_fl := 'false';

            SELECT COUNT (1)
              INTO v_cnt
              FROM t502_item_order
             WHERE c501_order_id = p_order_id
               AND NVL (c502_attribute_id, -999) = v_attribute_id
               AND c502_void_fl IS NULL;

            IF     NVL (v_price_temp, -999) = NVL (v_price, -999)
               AND NVL (v_type_temp, -999) = NVL (v_type, -999)
               AND NVL (v_cnum_temp, -999) = NVL (v_cnum, -999)
               AND v_cnt = 0
            THEN
               v_update_fl := 'true';
            ELSIF     NVL (v_price_temp, -999) = NVL (v_price, -999)
                  AND NVL (v_type_temp, -999) = NVL (v_type, -999)
                  AND v_cnt = 0
            THEN
               v_update_fl := 'true';
            ELSIF     NVL (v_type_temp, -999) = NVL (v_type, -999)
                  AND NVL (v_cnum_temp, -999) = NVL (v_cnum, -999)
                  AND v_cnt = 0
            THEN
               v_update_fl := 'true';
            ELSIF     NVL (v_price_temp, -999) = NVL (v_price, -999)
                  AND NVL (v_cnum_temp, -999) = NVL (v_cnum, -999)
                  AND v_cnt = 0
            THEN
               v_update_fl := 'true';
            ELSIF NVL (v_price_temp, -999) = NVL (v_price, -999) AND v_cnt = 0
            THEN
               v_update_fl := 'true';
            ELSIF NVL (v_type_temp, -999) = NVL (v_type, -999) AND v_cnt = 0
            THEN
               v_update_fl := 'true';
            ELSIF NVL (v_cnum_temp, -999) = NVL (v_cnum, -999) AND v_cnt = 0
            THEN
               v_update_fl := 'true';
            END IF;

            --gm_procedure_log (v_item_order_id, v_attribute_id || v_update_fl);
            IF v_update_fl = 'true'
            THEN
               IF v_type IS NULL
               THEN
                  v_type := v_type_temp;
               END IF;

               IF v_cnum IS NULL
               THEN
                  v_cnum := v_cnum_temp;
               END IF;

               IF v_item_seq_no IS NULL
               THEN
                  v_item_seq_no := v_item_seq_no_temp;
               END IF;

               IF v_construct_fl IS NULL
               THEN
                  v_construct_fl := v_construct_fl_temp;
               END IF;

               IF v_construct_fl IS NULL
               THEN
                  v_construct_fl := v_construct_fl_temp;
               END IF;

               IF v_ref_id IS NULL
               THEN
                  v_ref_id := v_ref_id_temp;
               END IF;

               IF v_vat_rate IS NULL
               THEN
                  v_vat_rate := v_vat_rate_temp;
               END IF;

               IF v_tax_amount IS NULL
               THEN
               		v_tax_amount := v_tax_amount_temp;
               END IF;
               
               gm_pkg_op_do_master.gm_sav_order_item_master (v_item_order_id,
                                                             p_order_id,
                                                             v_pnum,
                                                             v_qty,
                                                             v_cnum,
                                                             v_price,
                                                             v_item_seq_no,
                                                             v_type,
                                                             v_construct_fl,
                                                             v_ref_id,
                                                             v_vat_rate,
                                                             v_attribute_id,
                                                             NULL,
                                                             v_tax_amount,                                                             
                                                             v_item_order_id
                                                            );
                                                            
               v_part_attribute_id := v_attribute_id;
               EXIT;
            END IF;
         END LOOP;

         -- check if the type and price is changed for the part, if so then use the existing id othere wise create a new id
         IF v_part_attribute_id IS NULL OR v_part_attribute_id = ''
         THEN
            SELECT MIN (t502a.c502_attribute_id)
              INTO v_part_attribute_id
              FROM my_temp_t520_item_order my_temp,
                   t502a_item_order_attribute t502a
             WHERE t502a.c205_part_number_id = my_temp.c205_part_number_id
               AND t502a.c502_attribute_id = my_temp.c502_attribute_id
               AND my_temp.c205_part_number_id = v_pnum
               AND my_temp.c502_void_fl IS NULL
               AND t502a.c502a_void_fl IS NULL
               AND t502a.c501_order_id = p_order_id
               AND my_temp.c502_attribute_id NOT IN (
                      SELECT c502_attribute_id
                        FROM t502_item_order
                       WHERE c501_order_id = p_order_id
                         AND c502_void_fl IS NULL)
               AND my_temp.c502_attribute_id IS NOT NULL;

            BEGIN
               SELECT c502_item_order_id, c205_part_number_id,
                      c502_control_number, c502_item_price,
                      c502_item_seq_no, c901_type, c502_construct_fl,
                      c502_ref_id, c502_vat_rate,
                      c502_list_price, c502_unit_price, c901_unit_price_adj_code,
      	   		  	  c502_unit_price_adj_value, c502_discount_offered,
      	   		  	  c901_discount_type, c502_system_cal_unit_price,
      	   		  	  C502_DO_UNIT_PRICE,C502_ADJ_CODE_VALUE, c502_tax_amt
                 INTO v_item_order_id, v_pnum,
                      v_cnum, v_price,
                      v_item_seq_no, v_type, v_construct_fl,
                      v_ref_id, v_vat_rate,
                      v_list_price, v_unit_price, v_unit_prc_adj_code,
                      v_unit_price_adj_val, v_disc_offered,
                      v_disc_type, v_sys_cal_unit_price,
                      v_account_price , v_adj_code_value, v_tax_amount  
                 FROM my_temp_t520_item_order t502
                WHERE t502.c501_order_id = p_order_id
                  AND t502.c502_attribute_id IS NOT NULL
                  AND t502.c502_void_fl IS NULL
                  AND t502.c502_attribute_id = v_part_attribute_id;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  SELECT s502_attribute_id.NEXTVAL
                    INTO v_part_attribute_id
                    FROM DUAL;
            END;

            gm_pkg_op_do_master.gm_sav_order_item_master (v_item_order_id,
                                                          p_order_id,
                                                          v_pnum,
                                                          v_qty,
                                                          v_cnum,
                                                          v_price,
                                                          v_item_seq_no,
                                                          v_type,
                                                          v_construct_fl,
                                                          v_ref_id,
                                                          v_vat_rate,
                                                          v_part_attribute_id,
                                                          NULL,
                                                          v_tax_amount,
                                                          v_item_order_id
                                                         );
                                                         
         END IF;
      END LOOP;

      FOR v_attr_index IN cur_attr
      LOOP
         UPDATE t502a_item_order_attribute
            SET c502_attribute_id = v_attr_index.att_id,
                c502a_last_updated_by = p_userid,
                c502a_last_updated_date = CURRENT_DATE
          WHERE c502a_item_order_attribute_id = v_attr_index.item_att_id;
      END LOOP;

      UPDATE t502a_item_order_attribute t502a
         SET c502a_void_fl = 'Y'
       WHERE t502a.c501_order_id = p_order_id
       AND T502a.C901_attribute_type = '100820'
       AND t502a.c502a_item_order_attribute_id IN (
                SELECT   MIN (c502a_item_order_attribute_id)
                    FROM t502a_item_order_attribute t502a
                   WHERE t502a.c501_order_id = p_order_id  
                    AND t502a.c502a_void_fl IS NULL                  
                GROUP BY c501_order_id,
                         c205_part_number_id,
                         c901_attribute_type,
                         c502_attribute_id
                  HAVING COUNT (1) > 1)
         AND t502a.c502a_void_fl IS NULL;

      UPDATE t502a_item_order_attribute t502a
         SET t502a.c502a_void_fl = 'Y',
             c502a_last_updated_by = p_userid,
             c502a_last_updated_date = CURRENT_DATE
       WHERE t502a.c501_order_id = p_order_id
       AND T502a.C901_attribute_type = '100820'
         AND NVL (t502a.c502_attribute_id, -999) NOT IN (
                     SELECT NVL (c502_attribute_id, -999)
                       FROM t502_item_order
                      WHERE c501_order_id = p_order_id
                            AND c502_void_fl IS NULL)
         AND t502a.c502a_void_fl IS NULL;
          
     --Whenever the child is updated, the parent table has to be updated ,so ETL can Sync it to GOP.
	  UPDATE t501_order
	     SET c501_last_updated_date = CURRENT_DATE
	       , c501_last_updated_by = p_userid
	  WHERE c501_order_id        = p_order_id;
		  
   END gm_sav_sync_item_attribute;

   PROCEDURE gm_validate_order (
      p_orderid   IN   t501_order.c501_order_id%TYPE,
      p_case_id   IN   t501_order.c7100_case_info_id%TYPE,
      p_action    IN   VARCHAR2,
      p_userid    IN   t501_order.c501_created_by%TYPE
   )
   AS
      v_order_type      NUMBER;
      v_order_lock_fl   t501a_order_attribute.c501a_attribute_value%TYPE;
      v_case_id         t501_order.c7100_case_info_id%TYPE;
      v_void_fl         t501_order.c501_void_fl%TYPE;
      v_void_fl_cnt		NUMBER;
      v_status_fl       NUMBER;
      v_dept_id         NUMBER;
      v_do_cnt          NUMBER;
       v_order_source  VARCHAR2(4000);
   BEGIN
      -- Validate if account is setup
      BEGIN
         SELECT NVL(c901_order_type, -999), c7100_case_info_id, c501_void_fl,
                t501a.c501a_attribute_value ,t501.c501_status_fl
           INTO v_order_type, v_case_id, v_void_fl,
                v_order_lock_fl ,v_status_fl
           FROM t501_order t501, t501a_order_attribute t501a
          WHERE t501.c501_order_id = p_orderid
            AND t501.c501_order_id = t501a.c501_order_id(+)
            AND t501a.c901_attribute_type(+) = 53019
            AND t501a.c501a_void_fl(+) IS NULL;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            v_order_type := '-1';
            v_case_id := -1;
            v_void_fl := 'V';
            v_order_lock_fl := 'NEW';
            v_status_fl := -1;
      END;
       IF p_action = 'EDITDO'
       THEN
       
        SELECT COUNT(1)
         INTO v_void_fl_cnt
	     FROM T501_ORDER T501 
	    WHERE T501.c501_parent_order_id = p_orderid
	      AND C501_VOID_FL is NOT NULL
          AND C501_COMMENTS LIKE '%Orig Order ID%';
	   
       SELECT COUNT (1) 
         INTO v_do_cnt
         FROM t501_order 
        WHERE c501_order_id = p_orderid
          AND C501_VOID_FL IS NULL;
        
	   IF v_void_fl_cnt > 0 AND v_do_cnt = 0
	   THEN
	  		 raise_application_error('-20999','DO already Voided');
	   END IF;
	   
      IF     v_order_type != -1
         AND v_order_type != 2518
         AND v_order_type != 2519
      THEN
         raise_application_error
            ('-20999',
             'DO cannot be edited as the order is not in Draft / Pending CS Confirmation status'
            );
      END IF;
		SELECT get_dept_id(p_userid) INTO v_dept_id FROM DUAL;
       	  
		-- Getting The Attribute Type. If the Order is raised from Mobile APP the below validation should not be thrown.
		SELECT get_order_attribute_value(p_orderid,'103560') -- DO APP
		  INTO  v_order_source  
		  FROM dual;
		  
		 
      IF v_dept_id = 2005 AND v_status_fl <> 7  AND  v_order_source  <>'103521'
      THEN
       raise_application_error
              ('-20999',
               'Already Order has been released to CS for verification'
              );
      END IF;
      IF v_case_id != -1 AND v_case_id IS NULL AND p_case_id IS NULL
      THEN
         raise_application_error
            ('-20999',
             'DO without a Case ID cannot be edited from this screen, Please use Modify Order Screen or contact Customer Service'
            );
      END IF;
      END IF;
      IF v_void_fl != 'V' AND v_void_fl IS NOT NULL
      THEN
         raise_application_error ('-20999', 'DO already voided');
      END IF;
   /*  IF     v_order_lock_fl != 'NEW'
        AND v_order_lock_fl = 'Y'
        AND p_dept_id = '2005'
     THEN
        raise_application_error
                            ('-20999',
                             'DO is Locked, Contact CS if need to be edited'
                            );
     END IF;*/
   END gm_validate_order;

   PROCEDURE gm_sav_confirmation (
      p_order_id   IN   t501_order.c501_order_id%TYPE,
      p_action     IN   VARCHAR2,
      p_userid     IN   t501_order.c501_created_by%TYPE
     
   )
   AS
      v_hold_order_cnt    NUMBER;
      v_c_type            NUMBER;
      v_l_type            NUMBER;
      v_str               VARCHAR2 (20000);
      v_ordtype           NUMBER;
      v_acct_id           t501_order.c704_account_id%TYPE;
      v_cust_po           t501_order.c501_customer_po%TYPE;
      v_rep_id            t501_order.c703_sales_rep_id%TYPE;
      v_rec_mode          t501_order.c501_receive_mode%TYPE;
      v_rec_from          t501_order.c501_received_from%TYPE;
      v_ship_cost         t501_order.c501_ship_cost%TYPE;
      v_statusfl          NUMBER;
      v_action            VARCHAR2 (20);
      v_thirdpartyaccid   t906_rules.c906_rule_value%TYPE;
      v_out_log           VARCHAR2 (100);
      v_allocate_fl       VARCHAR2 (1);
      v_hold_fl			  t501_order.c501_hold_fl%TYPE;	
      v_case_info_id      t7100_case_information.c7100_case_info_id%TYPE;     
      v_temp_order_typ    t501_order.C901_ORDER_TYPE%TYPE;
      v_order_pick_rule t906_rules.c906_rule_value%TYPE;
      -- PMT-20705: ipad default revenue tracking information
      v_def_rev_track_cmt  t906_rules.c906_rule_value%TYPE;
      v_def_rev_track_status  t906_rules.c906_rule_value%TYPE;
      v_def_user_id NUMBER := 30301;
      v_ord_company_id t501_order.c1900_company_id%TYPE;
      v_country_id		t501_order.c1900_company_id%TYPE;
      v_bill_only_order    VARCHAR2 (1);
      v_update_inv_fl     t501_order.C501_UPDATE_INV_FL%TYPE := NULL;
      v_msg		   VARCHAR2 (100);
      
      
      CURSOR order_cursor      -- cursor to fetch backorder details for order
      IS
         SELECT c501_order_id
           FROM t501_order
          WHERE c501_parent_order_id = p_order_id AND c501_void_fl IS NULL;

      CURSOR v_cur
      IS
         SELECT t501.c501_order_id ord_id,
                t501.c501_parent_order_id parent_ord_id
           FROM t501_order t501
          WHERE (   t501.c501_order_id = p_order_id
                 OR t501.c501_parent_order_id = p_order_id
                )
            AND t501.c501_void_fl IS NULL
            AND t501.c501_status_fl <> 3 ;--shipped
   BEGIN

	    v_action := 'EDITDO';
	    gm_pkg_op_do_process_trans.gm_validate_order (p_order_id,
                                                       v_case_info_id,
                                                       v_action,
                                                       p_userid
                                                      );
                                                      
      IF p_action = 'SALES_CONFIRM'
      THEN
         BEGIN
            SELECT t501.c7100_case_info_id
              INTO v_case_info_id
              FROM t501_order t501
             WHERE t501.c501_order_id = p_order_id
               AND t501.c501_void_fl IS NULL;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               v_case_info_id := '';
         END;
        
         UPDATE t501_order t501
            SET t501.c901_order_type = 2518,                          -- Draft
                c501_status_fl = 8,                           -- Release to CS
                c501_last_updated_by = p_userid,
                c501_last_updated_date = CURRENT_DATE
          WHERE c501_order_id = p_order_id;

         RETURN;
      END IF;

      SELECT SUM (DECODE (t502.c901_type, 50300, 1, 0)) c,
             SUM (DECODE (t502.c901_type, 50301, 1, 0)) l
        INTO v_c_type,
             v_l_type
        FROM t502_item_order t502, t501_order t501
       WHERE (   t501.c501_order_id = p_order_id
              OR t501.c501_parent_order_id = p_order_id
             )
         AND t501.c501_order_id = t502.c501_order_id
         AND t501.c501_void_fl IS NULL
         AND t502.c502_delete_fl IS NULL
         AND t502.c502_void_fl IS NULL
         AND t501.c501_status_fl <> 3;

      IF v_c_type = 0 AND v_l_type = 0
      THEN
         --Commenting it for time being, need this validation once the data correction is done.
         v_c_type := 1;
      /*  raise_application_error
              ('-20999',
               'Could not determine the type of order, contact system admin'
              );*/
      END IF;
    --  Get order type by order attribute for IPAD Changes (PMT-29425)
	--  107802 -- Order Type
      SELECT get_order_attribute_value(p_order_id,'107802') INTO v_ordtype FROM DUAL;

-- Determine the order type
IF v_ordtype IS NULL THEN  
      IF v_c_type > 0
      THEN
         v_ordtype := 2521;                                    -- Bill & Ship
      ELSE
         v_ordtype := 2530;                               -- Bill only Loaner
      END IF;
END IF;
--to get the bill only order type
	IF v_ordtype = 2530 OR v_ordtype = 2532 --2530--Bill Only Loaner --2532--Bill Only Sales Consignments
	THEN
 		v_bill_only_order := 'Y';
 	END IF;

      -- Get Details from the order
      SELECT     t501.c704_account_id, c703_sales_rep_id, c501_customer_po,
                 c501_receive_mode, c501_received_from, c501_ship_cost, c501_hold_fl
                 , c1900_company_id
            INTO v_acct_id, v_rep_id, v_cust_po,
                 v_rec_mode, v_rec_from, v_ship_cost, v_hold_fl
                 , v_ord_company_id
            FROM t501_order t501
           WHERE t501.c501_order_id = p_order_id
      FOR UPDATE;
      


      -- Bill Only Orders
      IF v_bill_only_order = 'Y'

      THEN
         
      --Need to update status flag as 3, even if customer po is null
         v_statusfl := 3;
         v_update_inv_fl :=1;
         
         v_action := 'BILLONLY';
         
         --	Update the control number as NOC# for loaner parts
         UPDATE t502_item_order
            SET c502_control_number = 'NOC#' -- Consignment
          WHERE c501_order_id = p_order_id
            AND c901_type = 50301  --Loaner
            AND c502_void_fl IS NULL;
         
      ELSE
         v_statusfl := 1;
         v_action := 'INSERT';

         IF v_ordtype = 2521
         THEN
            v_ordtype := NULL;
         END IF;
      END IF;
      
      --for japan PC-2201-Header Edit Option in DO Approval Pending Confirmation Screen
      SELECT get_rule_value_by_company ('1026_ORDTYP', 'IPAD_ORD_TYP',v_ord_company_id) INTO v_country_id FROM DUAL;
      IF v_ord_company_id = v_country_id AND v_rec_mode = '26230683' THEN --Japan -1026 ,Ipad - 26230683
      
      	IF v_c_type = '0' AND v_l_type > 0 THEN
      		v_ordtype := '2530';  --Bill Only - Loaner
      	END IF;
      	
      	IF v_l_type = '0' AND v_c_type > 0 THEN
      		v_ordtype := '2532'; --Bill Only - From Sales Consignments
      	END IF;
      	
      END IF;

      UPDATE t501_order t501
         SET c501_order_date = TRUNC (CURRENT_DATE),
             c501_order_date_time = CURRENT_DATE,
             c501_status_fl = v_statusfl,
             C501_UPDATE_INV_FL = v_update_inv_fl,
             c901_order_type = v_ordtype,
             c501_last_updated_by = p_userid,
             c501_last_updated_date = CURRENT_DATE
       WHERE ( t501.c501_order_id = p_order_id OR t501.c501_parent_order_id = p_order_id )
         AND t501.c501_void_fl IS NULL
          AND t501.c501_status_fl <> 3;


      UPDATE t502_item_order
         SET c901_type = 50300                                  -- Consignment
       WHERE c501_order_id = p_order_id
         AND c901_type IS NULL
         AND c502_void_fl IS NULL;
         
	--PC-3641 Issue in I-pad DO editing (Missing Account net quantity by lot)
       UPDATE t501a_order_attribute
         SET  c501a_attribute_value = v_ordtype,
             c501a_last_updated_by = p_userid,
             c501a_last_updated_date = CURRENT_DATE
       WHERE c501_order_id = p_order_id
           AND c901_attribute_type = 107802 AND c501a_void_fl IS NULL;
           
      UPDATE t907_shipping_info t907
         SET t907.c901_source = 50180,                                -- Order
             c907_last_updated_by = p_userid,
             c907_last_updated_date = CURRENT_DATE,
             c907_frieght_amt = (
                                  SELECT c501_ship_cost
                                    from t501_order t501
                                   where (   t501.c501_order_id = p_order_id
                                            or t501.c501_parent_order_id = p_order_id
                                          )
                                    and t501.c501_order_id = t907.c907_ref_id
                                     AND t501.c501_status_fl <> 3
                            )
       WHERE t907.c907_ref_id IN (
                SELECT c501_order_id
                  from t501_order t501
                 where (   t501.c501_order_id = p_order_id
						OR t501.c501_parent_order_id = p_order_id )				
					 AND t501.c501_status_fl <> 3);
   	 
      -- remove the shipping records if the order type is bill only
      IF v_ordtype IS NOT NULL 
      THEN
         UPDATE t907_shipping_info t907
            SET t907.c907_void_fl = 'Y',
                c907_last_updated_by = p_userid,
                c907_last_updated_date = CURRENT_DATE
          WHERE t907.c907_ref_id IN (
                   SELECT c501_order_id
                     FROM t501_order t501
                    WHERE t501.c501_order_id = p_order_id
                       OR t501.c501_parent_order_id = p_order_id
                       AND t501.c501_status_fl <> 3);
                        
      END IF;

      FOR v_index IN v_cur
      LOOP
         gm_pkg_op_do_process_report.gm_fch_part_string (v_index.ord_id,
                                                         v_str
                                                        );
         gm_pkg_op_do_process_trans.gm_sav_item_backup_del (v_index.ord_id);
         gm_save_item_ord_items (v_index.ord_id,
                                 v_str,
                                 v_statusfl,
                                 p_userid,
                                 v_action,
                                 v_ordtype,
                                 v_index.parent_ord_id
                                );
         --gm_pkg_op_do_process_trans.gm_update_unit_price(v_index.ord_id,p_userid);
         gm_pkg_op_do_process_trans.gm_sav_sync_item_attribute (v_index.ord_id,
                                                                p_userid
                                                               );
		/*As we will have child orders from DO App, all the Child Orders [which are not Back Order] 
		need to be allocated to Inv.Device for Inv.Picking.
		*/
		BEGIN                                                               
			 SELECT NVL(C901_ORDER_TYPE, 2521) INTO v_temp_order_typ
			 FROM T501_ORDER
			 WHERE C501_ORDER_ID = v_index.ord_id
			 AND C501_VOID_FL IS NULL;
		 EXCEPTION WHEN OTHERS THEN
		 	v_temp_order_typ :='-1';
		 END;
		 -- to get the Inv pick orders type from rules
		 v_order_pick_rule := NVL(get_rule_value(v_temp_order_typ, 'ORDER_INV_PICK'), 'N');
		 
		 IF v_order_pick_rule = 'Y'
	      THEN
	         -- Code added for allocation logic
	         -- Uncommented the code to handle order which has a loaner part + 999 part.
	         -- As both are not picked, the order shdn't be allocated.
	         v_allocate_fl :=gm_pkg_op_inventory.chk_txn_details (v_index.ord_id, 93001);
	
	         IF NVL (v_allocate_fl, 'N') = 'Y'	            
	         THEN
	            gm_pkg_allocation.gm_ins_invpick_assign_detail (93001,
	                                                            v_index.ord_id,
	                                                            p_userid
	                                                           );
	         END IF;
	      END IF;                                                               
      END LOOP;
      /* Moved this procedure from the above loop to avoid the adjustments created for backorder
       * For PC-3639 Unit price issue on I-pad order
       */
      BEGIN       
	     SELECT t501.c7100_case_info_id
            INTO v_case_info_id
         FROM t501_order t501
            WHERE t501.c501_order_id = p_order_id
         AND t501.c501_void_fl IS NULL;
     EXCEPTION
       WHEN NO_DATA_FOUND
       THEN
          v_case_info_id := NULL;
      END;
      	IF v_case_info_id IS NOT NULL
      THEN
       gm_pkg_op_do_process_trans.gm_update_unit_price(p_order_id,p_userid);
      END IF;

      -- PMT-20705: Ipad order - default revenue tracking information update
		IF v_rec_mode = 26230683 -- IPAD
		    THEN
		    -- to get the default values from Rules
		     SELECT get_rule_value_by_company ('IPAD_ORD_REVENUE_STATUS', 'REVENUE_TRACK', v_ord_company_id), get_rule_value_by_company ('IPAD_ORD_REVENUE_CMTS',
		        'REVENUE_TRACK', v_ord_company_id)
		       INTO v_def_rev_track_status, v_def_rev_track_cmt
		       FROM dual;
		       
		   --Commented for PCA-271 and calling to save the Revenue tracking status
		   -- gm_pkg_cm_order_attribute.gm_sav_order_attribute (p_order_id, 53018, -- DO
		   -- v_def_rev_track_status, -- Signed with pricing and patient sticker or info
		   -- NULL, v_def_user_id) ;
		   
		   gm_pkg_ac_ar_revenue_sampling.gm_sav_revenue_sampling_dtls(p_order_id,'', v_def_rev_track_status, v_def_user_id);
		    -- to save the comments
		    gm_update_log (p_order_id, v_def_rev_track_cmt, '1200', v_def_user_id, v_out_log) ;
		END IF;
      
      -- Find third party account
      SELECT NVL (GET_ACCOUNT_ATTRB_VALUE (v_acct_id,'91980'), '0')
        INTO v_thirdpartyaccid
        FROM DUAL;

      --save comments for orders if the account is third party account
      IF (v_thirdpartyaccid != '0')
      THEN
         gm_update_log (p_order_id,
                        'Third Party Account Number is ' || v_thirdpartyaccid,
                        '1200',
                        p_userid,
                        v_out_log
                       );

         FOR curindex IN order_cursor
         LOOP
            --save comments for backorders if the account is third party account
            gm_update_log (curindex.c501_order_id,
                              'Third Party Account Number is '
                           || v_thirdpartyaccid,
                           '1200',
                           p_userid,
                           v_out_log
                          );
         END LOOP;
      END IF;

-- For Bill Only Orders and Credit Order (type - 2529) ---2530, bill only Loaner, 2532, bill only from sales consignment
      IF v_ordtype IS NOT NULL AND v_bill_only_order = 'Y'
      THEN
         UPDATE t502_item_order
            SET c502_control_number = 'NOC#'
          WHERE c501_order_id = p_order_id;

         UPDATE t501_order
            SET c501_shipping_date = CURRENT_DATE,
                c501_ship_cost = 0,
	            c501_last_updated_by = p_userid,
			    c501_last_updated_date = CURRENT_DATE                
          WHERE c501_order_id = p_order_id;
          
    	 -- PMT-29408: to update fields/account net qty and create the posting for bill only orders
    	 
		 -- Commenting the below code as part of PC-2280 to exclude the posting before the order split for ipad orders.
      	 -- gm_update_inventory (p_order_id, 'S', '', p_userid, v_msg);
          
      END IF;
      
      

-- For Discount Order Creation
	-- PMT-28394 (To avoid the DS order creation at order create time - Job will create the DS order)
    -- gm_save_dsorder (p_order_id);

      -- Sales Rep Made a Sale
      UPDATE t703_sales_rep
         SET c703_sales_fl = 'Y'
		   , c703_last_updated_by = p_userid          
		   , c703_last_updated_date = CURRENT_DATE          
       WHERE c703_sales_rep_id = v_rep_id;

      -- Save the status as DO Confirmed (New Code Lookup Type)
      gm_pkg_cm_status_log.gm_sav_status_details(p_order_id, NULL, p_userid, NULL, CURRENT_DATE, 101861, 91102);
      -- Do Confirmed
	-- To validate for the attribute qty and item qty.
	IF v_country_id = v_ord_company_id 
	THEN
		gm_val_do_attr_qty(p_order_id,101723); --To validate quantity mismatch in Do Summary.
	ELSE
		gm_validate_do_attribute_qty(p_order_id,101723);
	END IF;
	-- For PMT - 24435 
	-- Updating Valid Flag as 'Y' while confirming order raised from GLobus APP
	gm_pkg_order_tag_record.gm_upd_DO_record_tags(p_order_id,p_userid);
	
   END gm_sav_confirmation;
  
    /*******************************************************
    * Description : Procedure to Update the Hold Flag for Both Parent Order Back Order..
    * Parameters  : 1.Order ID , 2.User ID
    * Author     :
    *******************************************************/
   FUNCTION gm_sav_order_hold_flag (
      p_order_id   IN   t502a_item_order_attribute.c501_order_id%TYPE,
      p_userid     IN   t501_order.c501_created_by%TYPE
      
   )RETURN NUMBER
   IS
   
   v_cnt NUMBER; 
   v_hold_fl VARCHAR2(10);
	 BEGIN
		  SELECT SUM (cnt)
		  INTO v_cnt
		  FROM
			  (	SELECT COUNT(1) cnt  FROM t501_order a,
			      t502_item_order b ,
		      (
			      SELECT T502A.c205_part_number_id,
			        T502A.c502_attribute_id
			      FROM t502A_ITEM_ORDER_ATTRIBUTE T502A
			      WHERE T502A.c501_order_id = p_order_id
			      AND c901_attribute_type   = 100820 -- Usage
			      AND c502a_attribute_value = '2563' -- Discrepancy
			      AND c502a_void_fl        IS NULL
		      ) t502a
		    WHERE a.c501_order_id          = p_order_id
		    AND a.c501_order_id            = b.c501_order_id
		    AND b.C502_VOID_FL IS NULL 
		    AND t502a.c502_attribute_id    = b.c502_attribute_id
		    AND b.c205_part_number_id NOT IN
		      (
			      SELECT c906_rule_id
			      FROM t906_rules
			      WHERE c906_rule_grp_id = 'NONUSAGE'
			      AND c906_void_fl      IS NULL
		      )
		    UNION
		    SELECT COUNT(1) cnt
		    FROM
		      (
		      	SELECT NVL(b.c502_item_price, 0) item_price ,
		        NVL(get_account_part_pricing (a.c704_account_id , b.c205_part_number_id , get_account_gpo_id (a.c704_account_id) ), 0) curr_price
		      FROM t501_order a,t502_item_order b
		      WHERE (a.c501_order_id = p_order_id or a.c501_parent_order_id = p_order_id )
		      AND a.c501_order_id    = b.c501_order_id
		      AND b.c205_part_number_id NOT IN
		        (
			        SELECT c906_rule_id
			        FROM t906_rules
			        WHERE c906_rule_grp_id = 'NONUSAGE'
			        AND c906_void_fl      IS NULL
		        )
		      )
		    WHERE (item_price <> curr_price)
		    OR (item_price     = 0
		    AND curr_price     = 0)
		    );
		  RETURN v_cnt;
		EXCEPTION
		WHEN NO_DATA_FOUND THEN
		  RETURN NULL;
      
      END gm_sav_order_hold_flag;
	   
   

   /*******************************************************
    * Description : Procedure to Update the tag details informtion..
    * Parameters  : 1.Order ID , 2.Case Info ID, 3.Input string (pnum,qty,tag) 4.User ID
    * Author     :
    *******************************************************/--
   PROCEDURE gm_update_tagid (
      p_order_id   IN   t502a_item_order_attribute.c501_order_id%TYPE,
      p_case_id    IN   t501_order.c7100_case_info_id%TYPE,
      p_str        IN   VARCHAR2,
      p_user_id    IN   t502a_item_order_attribute.c502a_last_updated_by%TYPE
   )
   AS
      v_order_attribute_id   t502a_item_order_attribute.c502a_item_order_attribute_id%TYPE;
      v_attr_id              t502_item_order.c502_attribute_id%TYPE;
      v_strlen               NUMBER                := NVL (LENGTH (p_str), 0);
      v_string               VARCHAR2 (4000)                         := p_str;
      v_substring            VARCHAR2 (1000);
      v_order_case_id            VARCHAR2 (30);
      v_pnum                 t502a_item_order_attribute.c205_part_number_id%TYPE;
      v_qty                  t502_item_order.c502_item_qty%TYPE;
      v_tagid                t502a_item_order_attribute.c502a_attribute_value%TYPE;
      v_cnt                  NUMBER;
      v_tag_str              VARCHAR2 (4000);
      v_action               VARCHAR2(10) :='EDITDO';
      v_ord_id VARCHAR2 (30);
      v_parent_order_id VARCHAR2 (30);
      v_order_source VARCHAR2(4000);	
   BEGIN
      gm_pkg_op_do_process_trans.gm_validate_order (p_order_id, p_case_id,v_action,p_user_id);
	  
      BEGIN
      --Getting the Parent Order Reference for Child Order.
	  	SELECT NVL(t501.c501_parent_order_id,t501.c501_order_id),c501_parent_order_id
	  	  INTO v_ord_id,v_parent_order_id  
	  	  FROM t501_order t501, t501a_order_attribute t501a
	     WHERE t501.c501_order_id = t501a.c501_order_id
           AND t501.c501_order_id = p_order_id
           AND t501a.c901_attribute_type ='103560'  --DO attribut type
           AND t501a.c501a_attribute_value = '103521' --DO App attribut value
           AND t501.c501_void_fl IS NULL
           AND t501a.c501a_void_fl IS NULL
           AND ROWNUM =1;  
           
           SELECT get_order_attribute_value(p_order_id,'103560') -- DO APP
		  INTO  v_order_source  
		  FROM dual;  
	  EXCEPTION 
	  WHEN NO_DATA_FOUND 
	  THEN
	  		v_ord_id := '';
	  		v_order_source := '';
	  END;
	 
	  IF p_case_id IS NOT NULL
		THEN 
		v_order_case_id :=p_case_id;
		ELSE
		v_order_case_id := v_ord_id;
		END IF;

      IF v_strlen > 0
      THEN
         WHILE INSTR (v_string, '|') <> 0
         LOOP
            --
            v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
            v_string := SUBSTR (v_string, INSTR (v_string, '|') + 1);
            v_pnum :=
                  TRIM (SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1));
            v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
            v_qty :=
               TO_NUMBER (TRIM (SUBSTR (v_substring,
                                        1,
                                        INSTR (v_substring, '^') - 1
                                       )
                               )
                         );
            v_substring :=
                      TRIM (SUBSTR (v_substring, INSTR (v_substring, '^') + 1));
            v_tagid := TRIM (v_substring);
            v_tag_str := v_tag_str || v_tagid || ',' || v_pnum || '|';

            -- add the following validations
				
            -- Check whether the part is there in the entered tag, if not throw error
            SELECT COUNT (1)
              INTO v_cnt
              FROM t5010_tag t5010, t208_set_details t208
             WHERE t5010.c5010_tag_id = v_tagid
               AND NVL (t5010.c207_set_id, -999) = NVL (t208.c207_set_id,
                                                        -999)
               AND t208.c205_part_number_id = v_pnum
               AND t5010.c5010_void_fl IS NULL
               AND t208.c208_void_fl IS NULL;
		 --The below validation is commented for MDO-109 Sync DO.There  Should not be validation.
		  	  
		
         IF (v_cnt = 0 AND v_order_source <>'103521')
            THEN
               raise_application_error ('-20999',
                                           'Part '
                                        || v_pnum
                                        || ' does not exist in the tag '
                                        || v_tagid
                                        || '<br> Please select a valid tag'
                                       );
            END IF;

            -- Check the case status, if it is closed, throw an error
            SELECT SUM (DECODE (t7100.c901_case_status, 11094, 0, 1))
              INTO v_cnt
              FROM t7100_case_information t7100
             WHERE t7100.c7100_case_info_id = p_case_id
               AND t7100.c7100_void_fl IS NULL;

              --The below validation is commented for MDO-109 Sync DO.There  Should not be validation
            IF v_cnt = 0 AND v_order_source  <>'103521'
            THEN
               raise_application_error
                           ('-20999',
                            'Case is closed already, cannot edit tag details'
                           );
            END IF;

            -- To check the Tag id, Selected or not
            IF v_tagid != '0'
            THEN
            -- get the order attribute id (it will be used to the update)
            SELECT COUNT (1)
	              INTO v_cnt
	              FROM t5010_tag t5010
	             WHERE t5010.c5010_tag_id = v_tagid
	               AND t5010.c5010_void_fl IS NULL;
	               
	            IF v_cnt = 0 
	            THEN
	               raise_application_error ('-20999', 'Please select a valid tag for part '|| v_pnum);
	            END IF;   
               
               BEGIN
                  SELECT c5012_tag_usage_id
                    INTO v_order_attribute_id
                    FROM t5012_tag_usage
                   WHERE c5012_ref_id =  v_order_case_id
                     AND c901_ref_type = 2570                          -- Case
                     AND c205_part_number_id = v_pnum
                     AND c5010_tag_id = v_tagid
                     AND c5012_qty = v_qty
                     AND c5012_void_fl IS NULL;
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     v_order_attribute_id := '';
               END;
	
               gm_pkg_op_do_master.gm_sav_tag_master (v_order_attribute_id,
                                                      v_order_case_id,
                                                      v_pnum,
                                                      2570,
                                                      v_tagid,
                                                      v_qty,
                                                      p_user_id
                                                     );
            END IF;                                 -- end of tagid is not '0'
         END LOOP;
      END IF;                                               -- end of v_strlen

      -- Void the tags that are removed from the screen
      my_context.set_double_inlist_ctx (v_tag_str);
      -- Need to void the Tag id which is getting updated ,this need to happen only when paren order id is null.
	IF  v_parent_order_id is NULL THEN
      UPDATE t5012_tag_usage
         SET c5012_void_fl = 'Y',
             c5012_last_updated_by = p_user_id,
             c5012_last_updated_date = CURRENT_DATE
       WHERE c5012_ref_id = v_order_case_id
       AND c5012_tag_usage_id NOT IN (
                SELECT t5012.c5012_tag_usage_id
                  FROM t5012_tag_usage t5012, v_double_in_list v_list
                 WHERE t5012.c5010_tag_id = v_list.token
                   AND t5012.c205_part_number_id = v_list.tokenii
                   AND t5012.c901_ref_type = 2570                      -- Case
                   AND t5012.c5012_ref_id =  v_order_case_id
                   AND v_list.token IS NOT NULL
                   AND t5012.c5012_void_fl IS NULL);
         END IF;

      -- Commenting it for time being, once the data correction is done, this validation
      -- need to be opened up
      -- Check if the qty entered for the part does not exceed the qty in the t502 table, if not throw error

      /*  SELECT SUM (qty)
          INTO v_cnt
          FROM (SELECT ord.c205_part_number_id,
                       (CASE
                           WHEN NVL (tag.qty, 0) - NVL (ord.qty, 0) > 0
                              THEN 1
                           ELSE 0
                        END
                       ) qty
                  FROM (SELECT   t5012.c205_part_number_id,
                                 SUM (t5012.c5012_qty) qty
                            FROM t5012_tag_usage t5012
                           WHERE t5012.c5012_ref_id = p_case_id
                             AND t5012.c901_ref_type = 2570
                             AND t5012.c5012_void_fl IS NULL
                        GROUP BY c205_part_number_id) tag,
                       (SELECT   t502.c205_part_number_id,
                                 SUM (t502.c502_item_qty) qty
                            FROM t502_item_order t502
                           WHERE t502.c501_order_id = p_order_id
                             AND t502.c502_delete_fl IS NULL
                             AND t502.c502_void_fl IS NULL
                        GROUP BY c205_part_number_id) ord
                 WHERE ord.c205_part_number_id = tag.c205_part_number_id(+));

        IF v_cnt > 0
        THEN
           raise_application_error
                                ('-20999',
                                 'Qty entered for tags cannot exceed usage qty'
                                );
        END IF;
        */
      -- The below procedure need to be called only when the case id is not null.the code changed for MDO-109
      IF p_case_id IS NOT NULL
        THEN        
                gm_sav_tag_type (p_order_id, 'EDIT_TAG');
       END IF;
   END gm_update_tagid;

   PROCEDURE gm_update_ship_info (
      p_refid          IN   t907_shipping_info.c907_ref_id%TYPE,
      p_source         IN   t907_shipping_info.c901_source%TYPE,
      p_shipto         IN   t907_shipping_info.c901_ship_to%TYPE,
      p_shiptoid       IN   t907_shipping_info.c907_ship_to_id%TYPE,
      p_shipcarr       IN   t907_shipping_info.c901_delivery_carrier%TYPE,
      p_shipmode       IN   t907_shipping_info.c901_delivery_mode%TYPE,
      p_trackno        IN   t907_shipping_info.c907_tracking_number%TYPE,
      p_addressid      IN   t907_shipping_info.c106_address_id%TYPE,
      p_freightamt     IN   VARCHAR2,
      p_case_info_id   IN   t501_order.c7100_case_info_id%TYPE,
      p_userid         IN   t907_shipping_info.c907_last_updated_by%TYPE,
      p_attn           IN   t907_shipping_info.c907_override_attn_to%TYPE DEFAULT NULL,
      p_inst           IN   t907_shipping_info.c907_ship_instruction%TYPE DEFAULT NULL
   )
   AS
      v_shipping_id   VARCHAR2 (20);
      v_action        VARCHAR2 (10) := 'EDITDO';
      v_Carrier       VARCHAR2(10);
      v_act_carrier   VARCHAR2(10) := p_shipcarr;
      v_act_mode      VARCHAR2(10);
	  v_ship_cost	  t9072_shipping_cost.c9072_package_cost%TYPE;
   BEGIN
      gm_pkg_op_do_process_trans.gm_validate_order (p_refid, p_case_info_id,v_action,p_userid);
      BEGIN
         SELECT c907_shipping_id
           INTO v_shipping_id
           FROM t907_shipping_info
          WHERE c907_ref_id = p_refid AND c907_void_fl IS NULL;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            v_shipping_id := '';
      END;
     
      
      -- Getting the Ship Carrier based on the Ship Mode to Update
      /*The below Condition is added for MDO-109. If the Order is raised from device, there will ship carrier*/ 
      
      BEGIN
		      SELECT c901_delivery_carrier,c901_delivery_mode
				INTO  v_act_carrier,v_act_mode 
	            FROM t907_shipping_info 
	           WHERE c907_ref_id = p_refid 
	             AND C907_VOID_FL IS NULL; 
	       EXCEPTION
	         WHEN NO_DATA_FOUND
	         THEN
	            v_act_carrier := '';
	            v_act_mode := '';
	       END;
	     
	      IF v_act_carrier IS NULL THEN
		      SELECT DECODE(p_shipcarr,'0',c901_access_code_id,p_shipcarr) INTO v_Carrier
			    FROM t901a_lookup_access
			   WHERE c901_code_id = p_shipmode 
				 AND ROWNUM = 1;
		  ELSE
		  	v_Carrier := v_act_carrier;
	  	END IF;

	  Select  DECODE(p_shipcarr,'0',v_Carrier,NULL,v_Carrier,p_shipcarr) into v_Carrier from dual;
      gm_pkg_cm_shipping_trans.gm_sav_shipping (p_refid,
                                                p_source,
                                                p_shipto,
                                                p_shiptoid,
                                                v_Carrier,
                                                p_shipmode,
                                                p_trackno,
                                                p_addressid,
                                                p_freightamt,
                                                p_userid,
                                                v_shipping_id,
                                                p_attn,
                                                p_inst
                                               );
	UPDATE T501_ORDER T501
	set t501.c501_ship_to = p_shipto
	, t501.c501_ship_to_id = p_shiptoid
	, t501.c501_last_updated_by = p_userid
	, t501.c501_last_updated_date = CURRENT_DATE
	where c501_order_id = p_refid;

/* Fetch the Ship Cost for the account */

	gm_pkg_op_ship_charge.gm_fch_ship_cost_for_order (
		    p_refid		
		  , p_source		
		  , p_shipto 		
		  , p_shiptoid	
		  , p_addressid	
		  , v_Carrier	 	
		  , p_shipmode		
		  , NULL	
		  , v_ship_cost			
	);

	/* Update the ship Cost, so it reflects on the invoice */
		gm_update_ship_cost (p_refid, NVL(v_ship_cost,0), p_userid);
 	
   END gm_update_ship_info;
 
   PROCEDURE gm_order_validation (
        p_order_id IN t501_order.c501_order_id%TYPE,
        p_tag_id   IN t5012_tag_usage.c5010_tag_id%TYPE,
        p_part_no  IN t205_part_number.c205_part_number_id%TYPE,
        p_stropt   IN VARCHAR2,
        p_err_str OUT VARCHAR2)
	AS
    v_count NUMBER;
	BEGIN
    IF p_stropt = 'EDITTAG' THEN
         SELECT COUNT (1)
           INTO v_count
           FROM t5010_tag t5010, t208_set_details t208
          WHERE t5010.c5010_tag_id             = p_tag_id
            AND NVL (t5010.c207_set_id, - 999) = NVL (t208.c207_set_id, - 999)
            AND t208.c205_part_number_id       = p_part_no
            AND t5010.c5010_void_fl           IS NULL
            AND t208.c208_void_fl             IS NULL;
        IF v_count                             = 0 THEN
            p_err_str                         := 'Part '|| p_part_no||' does not exist in the tag '|| p_tag_id ||
            ' Please select a valid tag';
        END IF;
    ELSE
         SELECT COUNT (1)
           INTO v_count
           FROM t501_order
          WHERE c501_order_id = p_order_id
          AND c501_void_fl        IS NULL;
        IF v_count            > 0 THEN
            p_err_str        := 'Order ID already exists';
        END IF;
    END IF;
	END gm_order_validation;

	FUNCTION get_item_attribute_val (
		p_order_id 	IN	 t501_order.c501_order_id%TYPE
	  , p_partnum	IN	 t502_item_order.c205_part_number_id%TYPE
	)
		RETURN VARCHAR2
	IS
	v_attr_val VARCHAR2(20);
	BEGIN
		 SELECT c502a_attribute_value
		   INTO v_attr_val
		   FROM t501_order t501, t502_item_order t502, t502a_item_order_attribute t502a
		  WHERE t501.c501_order_id        = t502.c501_order_id
		    AND t502.c502_attribute_id    = t502a.c502_attribute_id
		    AND t501.c501_order_id        = p_order_id
		    AND t502.c205_part_number_id  = p_partnum
		    AND t502a.c901_attribute_type = 100820 -- usage
		    AND t502a.c502a_attribute_value = '2563'
		    AND t501.c501_delete_fl      IS NULL
		    AND t501.c501_void_fl        IS NULL
		    AND t502.c502_void_fl        IS NULL
		    AND t502a.c502a_void_fl      IS NULL
		    AND ROWNUM = 1;
		    
		    RETURN v_attr_val;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN '';
			
	END get_item_attribute_val;

/***********************************************************
* Description : Procedure to move an order to Hold Status
*               and to send out an email to AR Team and PC 
 *               Team
***********************************************************/
      PROCEDURE gm_cs_sav_order_hold (
          p_txn_id          IN   t907_cancel_log.c907_ref_id%TYPE
        , p_cancelreason      IN   t907_cancel_log.c901_cancel_cd%TYPE
        , v_cancelid          IN   t907_cancel_log.c907_cancel_log_id%TYPE
        , p_comments          IN   t907_cancel_log.c907_comments%TYPE
        , p_userid            IN   t102_user_login.c102_user_login_id%TYPE
        , p_hold_fl			  IN   VARCHAR2 DEFAULT NULL 
      )
      AS
            v_id           VARCHAR2 (30);
            p_newid        VARCHAR2 (30);
            v_usernm       VARCHAR2 (100);
            v_invoice_id   VARCHAR2 (20);
            v_void_fl      CHAR (1);
            v_acc_id       NUMBER;
            v_ac_name      VARCHAR2 (255);
            v_ord_dt       VARCHAR2 (100);
            v_rep_id       NUMBER;
            v_exc_do_hold   NUMBER;
            v_rep_name     VARCHAR2 (255);
            v_tot_cost     VARCHAR2 (100);
            subject        VARCHAR2 (1000);
            mail_body      VARCHAR2 (30000);
            mail_details   VARCHAR2 (30000);
            crlf           VARCHAR2 (2) := CHR (13) || CHR (10);
            to_mail        t906_rules.c906_rule_value%TYPE;
            v_msg          VARCHAR2 (100);
            v_cancelreasoncomments VARCHAR2 (5000);
            v_cust_po  	   T501_order.c501_customer_po%TYPE;
            v_price_diff      NUMBER;
            v_price_disc      NUMBER;
            v_rep_mail      T703_SALES_REP.C703_EMAIL_ID%TYPE;
            v_ord_hold_fl   VARCHAR2(2);
            v_hold_mail     T703_SALES_REP.C703_EMAIL_ID%TYPE;
            v_country_code VARCHAR2(10);
	        v_country_cc   VARCHAR2(1000);
	        v_currency_symbol t906_rules.c906_rule_value%TYPE;
	        v_unitprice		t502_item_order.c502_unit_price%TYPE;
	        v_company_id    t1900_company.c1900_company_id%TYPE;
			v_price_check_for_usage_parts	NUMBER;
            CURSOR part_details
            IS
                  SELECT pnum, pdesc, qty, item_price, curr_price , unitprice , unitpriceadj , adjcode , netunitprice, adjcodeval, prodfmly
                    FROM (SELECT b.c205_part_number_id pnum, get_partnum_desc (b.c205_part_number_id) pdesc
                                    , b.c502_item_qty qty,NVL(NVL(b.c502_ext_item_price, b.c502_item_price), 0) item_price
                                    , NVL(get_account_part_pricing (a.c704_account_id
                                                                           , b.c205_part_number_id
                                                                           , get_account_gpo_id (a.c704_account_id)
                                                                              ), 0) curr_price
                                                      , NVL(gm_pkg_sm_adj_rpt.get_account_part_unitprice (a.c704_account_id
                                                                           , b.c205_part_number_id
                                                                           , get_account_gpo_id (a.c704_account_id)
                                                                              ), 0) unitprice
                                      , b.c502_unit_price_adj_value unitpriceadj
                                      , GET_CODE_NAME(b.c901_unit_price_adj_code) adjcode
                                      , NVL(b.c502_ext_item_price, b.c502_item_price) netunitprice, b.c502_adj_code_value adjcodeval,t205.c205_product_family prodfmly
                                 FROM t501_order a, t502_item_order b,T205_PART_NUMBER T205 
                                WHERE a.c501_order_id = p_txn_id AND a.c501_order_id = b.c501_order_id
                                  and b.c205_part_number_id =  t205.c205_part_number_id
                              )
                  where ((prodfmly != 26240096) and ((ITEM_PRICE <> CURR_PRICE) or (ITEM_PRICE = 0 and CURR_PRICE = 0 ) or ((ITEM_PRICE < 0 and CURR_PRICE < 0)
	            	    	or (item_price = 0 and curr_price = 0 ) 
    	            		and (item_price <> curr_price))))
        		    	or (prodfmly = 26240096 and ITEM_PRICE <> CURR_PRICE);
                            --PMT-32395 usage code

      BEGIN
	       /* SELECT count(1) INTO v_price_diff
                    FROM (SELECT NVL(b.c502_item_price, 0) item_price
                                    , NVL(get_account_part_pricing (a.c704_account_id
                                                                           , b.c205_part_number_id
                                                                           , get_account_gpo_id (a.c704_account_id)
                                                                              ), 0) curr_price
                                FROM t501_order a, t502_item_order b
                              WHERE a.c501_order_id = p_txn_id AND a.c501_order_id = b.c501_order_id
                              AND b.c205_part_number_id NOT IN ( SELECT c906_rule_id
                                                                                      FROM t906_rules
                                                                               WHERE c906_rule_grp_id = 'NONUSAGE'
                                                                               AND c906_void_fl IS NULL )  
                              )
                  WHERE (item_price <> curr_price) OR (item_price = 0 AND curr_price = 0);
            */
	      SELECT COUNT(1)
                       INTO v_exc_do_hold
                       FROM t501_order t501 
                      WHERE t501.c501_order_id = p_txn_id
                        AND t501.c901_order_type IN ( SELECT c906_rule_value 
                                                                             FROM t906_rules t906
                                                                           WHERE t906.c906_rule_grp_id = 'DO_AUTO_HOLD'  
                                                                              AND t906.c906_RULE_ID = 'EXC_ORDER_TYPE' );
                                                                                  
                     IF v_exc_do_hold > 0 THEN
                           RETURN;
                     END IF;
		      --Getting company id from t501_order table and if it is null get from context.   
        SELECT NVL(c1900_company_id,get_compid_frm_cntx()) into v_company_id
          FROM t501_order 
         WHERE c501_order_id = p_txn_id
           AND c501_void_fl IS NULL;
	  
	    INSERT INTO my_temp_acc_part_list
		SELECT t501.c704_account_id, t502.c205_part_number_id
		  FROM t501_order t501
		     , t502_item_order t502 
		 WHERE ( t501.c501_order_id = p_txn_id OR T501.c501_parent_order_id = p_txn_id )  
		   AND t501.c501_order_id = t502.c501_order_id;

		SELECT sum(price_check)
		  INTO v_price_diff
		  FROM
	   (
	   		SELECT DECODE((CASE WHEN NVL(t502.c502_ext_item_price, t502.c502_item_price) <= 0 THEN 
        	(CASE WHEN NVL(t502.c502_ext_item_price, t502.c502_item_price) = NVL(v705.C705_unit_price,-999999) THEN 
        		NVL(t502.c502_ext_item_price, t502.c502_item_price) ELSE -999999 END)
        		 ELSE NVL(t502.c502_ext_item_price, t502.c502_item_price) END),
	   				 (CASE 
		    		 WHEN ((c1706_waste_price_list = 105241 OR 
		    		      c1706_revision_price_list = 105241) AND t502.c901_unit_price_adj_code IS NOT NULL) 
		    		 THEN 
		    				NVL(c705_calculated_list_price,0) 
		    		 ELSE 
		    				NVL(c705_calculated_unit_price,0) 
		    		 END),0,1) price_check
		  FROM t502_item_order t502
		     , v705_part_price_in_acc v705, T205_PART_NUMBER T205
		   WHERE t502.c501_order_id = p_txn_id
		   AND t502.c205_part_number_id = v705.c205_part_number_id(+)
		   AND t502.c205_part_number_id = T205.c205_part_number_id
           AND t205.c205_product_family != 26240096

	   );  
	  
	   --PMT-32395 usage code
	   --Get the price difference of usage code parts using below query
		SELECT count(1)
		 INTO v_price_check_for_usage_parts
		FROM t502_item_order t502 ,
		  t205_part_number T205
		WHERE t502.c501_order_id     = p_txn_id
		AND t502.c205_part_number_id = T205.c205_part_number_id
		AND t205.c205_product_family = 26240096
		AND t502.c502_item_price    <> t502.c502_do_unit_price;

		-- Get the price difference for both normal part and usage code part
		-- if the value of v_price_diff is null convert to 0
   		v_price_diff := NVL(v_price_diff,0) +  v_price_check_for_usage_parts;--usage code
        
	   --to update order hold process completion flag
		 UPDATE t501_order
	           SET C501_HOLD_ORDER_PROCESS_FL = 'Y'
	           ,C501_HOLD_ORDER_PROCESS_DATE = CURRENT_DATE
	           , c501_last_updated_by = p_userid
	           , c501_last_updated_date = CURRENT_DATE
	    WHERE c501_order_id = p_txn_id
	    		AND C501_VOID_FL IS NULL 
		   		AND C503_INVOICE_ID IS NULL  
		  		AND C501_HIST_HOLD_FL IS NULL;
		  		
           IF (v_price_diff > 0  OR p_hold_fl = 'Y' )THEN
      
	           SELECT         t501.c503_invoice_id, t501.c501_void_fl, t501.c704_account_id
	                       , nvl(t704.c704_account_nm_en,t704.c704_account_nm)
	                        , t501.c501_order_date, t501.c703_sales_rep_id, NVL(t703.C703_SALES_REP_NAME_EN,t703.C703_SALES_REP_NAME), t501.c501_total_cost
	                        , get_user_name (p_userid), NVL(t501.c501_customer_po,'-999'),t703.C703_EMAIL_ID 
	                    INTO v_invoice_id, v_void_fl, v_acc_id, v_ac_name
	                        , v_ord_dt, v_rep_id, v_rep_name, v_tot_cost
	                        , v_usernm, v_cust_po,v_rep_mail
	                    FROM t501_order t501, t704_account t704, t703_sales_rep t703
	                  WHERE c501_order_id = p_txn_id
	                  	AND t501.c704_account_id = t704.c704_account_id
                      	AND t704.c704_void_fl IS NULL
                      	AND t501.c703_sales_rep_id = t703.c703_sales_rep_id 
                        AND t703.C703_void_fl IS NULL
	            FOR UPDATE;
	
	            IF (v_void_fl IS NOT NULL)
	            THEN
	                  -- Error Messaage. This order has already been made void.
	                  raise_application_error (-20065, '');
	            END IF;      
	        	BEGIN
	                  --
	                  UPDATE t501_order
	                     SET c501_hold_fl = 'Y'
	                     	,c501_hist_hold_fl ='Y'
	                        , c501_last_updated_by = p_userid
	                        , c501_last_updated_date = CURRENT_DATE
	                  WHERE c501_order_id = p_txn_id OR c501_parent_order_id = p_txn_id;
					  --
	                  gm_save_status_details (p_txn_id, '', p_userid, NULL, CURRENT_DATE, 91175, 91102);
	                  --
	                  v_cancelreasoncomments := get_code_name (p_cancelreason) || ': ' || p_comments;
	                  gm_update_log (p_txn_id, v_cancelreasoncomments, 1200, p_userid, v_msg);
	                  --to save cancel log
	                  gm_pkg_common_cancel_op.gm_cm_sav_cancelrow(p_txn_id,p_cancelreason,'OHOLD',p_comments,p_userid);
	                  --
	                  v_country_code := NVL(get_company_code(v_company_id),'GNA');
	                  v_currency_symbol := NVL(get_account_curr_symb(v_acc_id), get_rule_value(v_country_code,'HOLD_ORDER_CURRENCY'));
	                  -- to check this order is part of batch, if batch then void this order to batch.
	                  gm_pkg_ac_ar_batch_trans.gm_sav_batch_hold_po(p_txn_id,v_cust_po,v_acc_id,'HOLD',p_userid);
	                  COMMIT;
	                  subject     := 'DO moved to Hold Status - ' || v_ac_name || ' ' || p_txn_id;
	        		
	                  mail_body   := '<meta http-equiv="Content-Type" content="text/html"  charset="UTF-8" />';
	                  mail_body   := mail_body
	                        || '<style>TD{ FONT-SIZE: 11px; COLOR: #000000; FONT-FAMILY: verdana, arial, sans-serif;}</style><font face=arial size="2">';
	                  mail_body   :=
	                           mail_body
	                        || 'Delivered Order ID <b>'
	                        || p_txn_id
	                        || '</b>'||get_rule_value_by_company('HOLD_EMAIL_HEADER','HOLD_ORD_EMAIL_BODY',v_company_id)||'<br>Following are the Details:<br><br>';
	                  mail_body   :=
	                           mail_body
	                        || '<TABLE style="border: 1px solid  #676767">'
	                        || '<TR><TD width=150 align=right><b>Account ID:</b></TD>'
	                        || '<TD>'
	                        || v_acc_id
	                        || '</TD><TD width=150 align=right><b>Account Name:</b></TD>'
	                        || '<TD>'
	                        || v_ac_name
	                        || '</TD></TR>'
	                        || '<TR><TD align=right><b>Rep ID:</b></TD>'
	                        || '<TD>'
	                        || v_rep_id
	                        || '</TD><TD align=right><b>Rep Name:</b></TD>'
	                        || '<TD>'
	                        || v_rep_name
	                        || '</TD></TR>'
	                        || '<TR><TD align=right><b>Order Date:</b></TD>'
	                        || '<TD>'
	                        || v_ord_dt
	                        || '</TD><TD align=right><b>Order Total:</b></TD>'
	                        || '<TD>'||v_currency_symbol||'&nbsp;'
	                        || TO_CHAR(get_formatted_number (v_tot_cost, 2),'999,999,990.99')
	                        || '</TD></TR>'
	                        || '<TR><TD align=right><b>Modified By:</b></TD>'
	                        || '<TD>'
	                        || v_usernm
	                        || '</TD><TD align=right><b>Modified Date:</b></TD>'
	                        || '<TD>'
	                        || TRUNC (CURRENT_DATE)
	                        || '</TD></TR>'
	                        || '</TABLE> <BR>';	                        
	                  
	                  mail_body   :=
	                           mail_body
	                        || '<TABLE style="border: 1px solid  #676767">'
	                        || '<TR><TD width=100><b>Part #</b></TD><TD width=250><b>Part Description</b></TD>'
	                        || '<TD width=50><b>Order<BR>Qty</b></TD><TD width=80><b>Current<BR>Portal Price</b></TD>'
							|| '<TD width=70><b>DO Unit<BR>Price</b></TD><TD width=80><b>Unit<BR>Price Adj</b></TD><TD width=50><b>Adj<BR>Code</b></TD>'
	                        || '<TD width=100 ><b>Net Unit<BR> Price</b></TD></TR>';
	                        
	                  FOR currindex IN part_details
	                  LOOP
	                  		v_unitprice :=  NVL(currindex.netunitprice,0) + NVL(currindex.adjcodeval,0) + NVL(currindex.unitpriceadj,0);
	                        mail_details :=
	                                 mail_details
	                              || '<TR><TD>'
	                              || currindex.pnum
	                              || '</TD><TD>'
	                              || currindex.pdesc
	                              || '</TD><TD>'
	                              || currindex.qty
	                              || '</TD>'
	                              || '<TD>'||v_currency_symbol
	                              || TO_CHAR(get_formatted_number (currindex.unitprice, 2),'999,999,990.99')
	                              || '</TD><TD>'||v_currency_symbol
	                              || TO_CHAR(get_formatted_number (v_unitprice, 2),'999,999,990.99')
	                              || '</TD><TD>'
	                              || TO_CHAR(get_formatted_number (currindex.unitpriceadj, 2),'999,999,990.99')
	                              || '</TD><TD>'
	                              ||currindex.adjcode 
	                              || '</TD><TD>'||v_currency_symbol
	                              || TO_CHAR(get_formatted_number (currindex.netunitprice, 2),'999,999,990.99')
	                              || '</TD>'
	                              || '</TR>';
	                  END LOOP;
	
	                  mail_body   := mail_body || mail_details || '</TABLE>';
	                  mail_body   :=
	                           mail_body
	                        || '<br><br> '||get_rule_value_by_company('HOLD_EMAIL_FOOTER','HOLD_ORD_EMAIL_BODY',v_company_id);
	                  mail_body   := mail_body || '<br>'; 
	                 -- END IF;
	                  
	                  --Based on rule value skip the hold order email to sales rep email id
	                  v_hold_mail := get_rule_value_by_company('HOLD_EMAIL','HOLD_ORD_REP_EMAIL',v_company_id);
	                  
	                  IF v_hold_mail IS NOT NULL THEN
	                  	v_rep_mail := v_hold_mail;                 	
	                  END IF;
	                  --getting CC email id for country 
	                  --v_country_code := NVL(get_rule_value('CURRENT_DB','COUNTRYCODE'),'en');
	                 
	                  v_country_cc := get_rule_value_by_company(v_country_code,'HOLD_ORD_CC_EMAIL',v_company_id); 
	                  
	                  mail_body   := mail_body || '</font>';
	                  to_mail     := get_rule_value_by_company ('DO_HOLD', 'EMAIL',v_company_id);
	                  IF v_rep_mail IS NOT NULL
	                  THEN
	                  to_mail := to_mail||','||v_rep_mail;
	                  END IF;
	                  IF v_country_cc IS NOT NULL
	                  THEN
	                  to_mail := to_mail||','||v_country_cc;
	                  END IF;
	             	  
					EXCEPTION WHEN OTHERS
					THEN
					raise_application_error(-20999, SQLERRM);
				END;
				gm_com_send_html_email(to_mail, subject, 'test', mail_body);
            END IF;
      END gm_cs_sav_order_hold;

 /***********************************************************
 * Description : Procedure to save control number in item
 *               order attibut table for Sterile Parts
 *                
 ***********************************************************/  
      
      PROCEDURE gm_sav_control_number (
      p_order_id   IN   t501_order.c501_order_id%TYPE,
      p_inputStr   IN 	VARCHAR2, 
      p_user_id    IN   t501_order.c501_created_by%TYPE,
      p_screen	   IN	VARCHAR2 DEFAULT NULL
      
   )
   AS
      v_strlen     NUMBER := NVL (LENGTH (p_inputStr), 0) ;
      v_string     VARCHAR2 (4000) := p_inputStr;
      v_substring  VARCHAR2 (4000);
      v_pkey 	   t502a_item_order_attribute.c502a_item_order_attribute_id%TYPE;
      v_order_id   t502a_item_order_attribute.c501_order_id%TYPE;
      v_pnum       t502a_item_order_attribute.c205_part_number_id%TYPE;
      v_attr_id    t502a_item_order_attribute.c502_attribute_id%TYPE;
      v_cnum  	   VARCHAR2(100);
      v_qty        NUMBER;
      v_count	   NUMBER;
      v_void_fl	   CHAR (1);
      v_ord_id   t501_order.c501_order_id%TYPE;
      v_cnt NUMBER;
      v_flag VARCHAR2(4000);
      v_error_msg VARCHAR2(4000);
      v_attribute_type VARCHAR2(4000);
      v_order_source  VARCHAR2(4000);
      v_apporder_id VARCHAR2(4000);
      v_order_info_id VARCHAR2(20);
	  v_cnum_fl BOOLEAN;
	  v_actual_fs t501_order.c501_distributor_id%TYPE; --Added for PMT-29681
      v_lot_error_type t5063_lot_error_info.c901_error_type%TYPE; --Added for PMT-29681
	  v_lot_error_msg    VARCHAR2(200); --Added for PMT-29681
	  v_order_date t501_order.C501_ORDER_DATE%TYPE; --Added for PMT-29681
	 
  BEGIN   

	  BEGIN
	  	  SELECT c501_void_fl, NVL(c501_parent_order_id,c501_order_id), c501_distributor_id, c501_order_date --getting transaction field sales id and order date for PMT-29681
	  	    INTO v_void_fl, v_ord_id, v_actual_fs, v_order_date 
	  	    FROM t501_order
	       WHERE c501_order_id = p_order_id
	       FOR UPDATE;
	  EXCEPTION WHEN NO_DATA_FOUND THEN
	  	v_void_fl := 'Y';
	  	v_actual_fs := NULL;
	  END; 
		--Getting The Attribute Type For Order.Added this for MDO-109.	 	  
		SELECT get_order_attribute_value(p_order_id,'103560') 
		  INTO  v_order_source  
		  FROM dual;

	  IF (v_void_fl IS NOT NULL)
		THEN
			-- Error Messaage. This order has already been made void.
			raise_application_error (-20065, '');
	  END IF; 
	  
	  
    IF v_strlen > 0 THEN
        WHILE INSTR (v_string, '|') <> 0
        LOOP
            v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
            v_string := SUBSTR (v_string, INSTR (v_string, '|') + 1);
            v_void_fl := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
            v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
            v_pkey := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
            v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
            v_pnum := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
            v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
            v_qty :=   SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);          
            v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
            --v_cnum  := v_substring;
            v_cnum  := SUBSTR (v_substring,1, INSTR (v_substring, '^') - 1);
            v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
            v_attribute_type := v_substring;
      		v_apporder_id := v_ord_id ;
      		v_cnum_fl := FALSE;
           --Added This code for MO-109, Need to 
            IF  v_pkey is not null AND( v_cnum IS NULL OR v_qty IS NULL OR v_void_fl = 'Y') THEN
            -- If the lot number for control attribute is removed, that time need to update the status of old lot number
             IF v_attribute_type ='101723' 
       			 THEN
       				 gm_pkg_op_lot_track.gm_sav_edit_order_sold_cnum(v_apporder_id, v_pnum, v_pkey, 'NOC#', p_user_id,v_cnum_fl);
       		 END IF;
             UPDATE t502a_item_order_attribute
            	SET c502a_void_fl = 'Y'
            	   ,c502a_last_updated_by = p_user_id
                   ,c502a_last_updated_date = CURRENT_DATE
             where c502a_item_order_attribute_id = v_pkey;             
            ELSIF v_ord_id IS NOT NULL AND v_pnum IS NOT NULL AND v_cnum IS NOT NULL AND v_qty IS NOT NULL THEN
            	--Validating for Contol number from DHR Table.
            	--Commenting the below code for MDO-109.If we need to validate the lot number remove the below comment.
            	/*SELECT COUNT (1) 
             	INTO v_cnt
              	FROM t408_dhr
             	WHERE c205_part_number_id = v_pnum
       			AND C408_CONTROL_NUMBER = v_cnum;
       			The below condition is added to validate control number. 
       			If the type is replenishmnent Req is the attribute type we need to insert record.
       			IF (v_cnt <> 0 OR v_attribute_type = 101732)--101732 - Loaner Req Replenishment type.
       			THEN */
       			--To update the replenish req attribute type with current order id not the parent order id.
   				IF  v_order_source  ='103521' AND v_attribute_type ='101732'
       			THEN
       				v_apporder_id := p_order_id;
       			END IF;
       			IF v_attribute_type ='101723' AND v_pkey IS NOT NULL
       			THEN
       				gm_pkg_op_lot_track.gm_sav_edit_order_sold_cnum(v_apporder_id, v_pnum, v_pkey, v_cnum, p_user_id,v_cnum_fl);
       			END IF;
       			IF v_attribute_type <>'101723'
       			THEN
       			gm_pkg_op_do_master.gm_sav_item_attribute_master (v_pkey, v_apporder_id, v_pnum, v_attribute_type, v_cnum, v_qty, p_user_id);
       			ELSE
       			SELECT c500_order_info_id
       				INTO v_order_info_id
				   FROM t502_item_order
				  WHERE c501_order_id       = v_apporder_id
				    AND c205_part_number_id = v_pnum
				    AND c502_item_qty       = v_qty
				    AND c502_delete_fl     IS NULL
				    AND c502_void_fl       IS NULL
				    AND ROWNUM = 1;
       			gm_pkg_cs_usage_lot_number.gm_sav_usage_dtls (v_apporder_id, null, v_pnum, v_qty, v_cnum, NULL, v_order_info_id, NULL, NULL,p_user_id);
       			/* Save the error message for the invalid lot coming from IPAD*/
       			--gm_pkg_cs_usage_lot_number.gm_sav_invalid_usage_lot_dtls(v_cnum , v_pnum , v_apporder_id); --Removed for PMT-29861
       			/* Save the error message for the ViaCell Field Sales Mismatch */
       			--gm_pkg_cs_usage_lot_number.gm_sav_usage_error_dtls(v_apporder_id,v_cnum ,v_pnum ,null); --Removed for PMT-29861
       			/* This procedure is used to check the lot is already billed */
       			-- gm_pkg_cs_usage_lot_number.gm_save_billed_lot(v_apporder_id ,v_pnum ,v_cnum); --Removed for PMT-29861
       			
               
       			--To validate lot number for PMT-29681
       			 gm_pkg_op_lot_validation.gm_validate_lot_number(v_cnum, v_pnum, v_actual_fs, v_lot_error_msg, v_lot_error_type); 
                  
                  IF v_lot_error_msg IS NOT NULL
              THEN
                --This procedure will update the error message in t502b_item_order_usage table
                 gm_pkg_op_lot_error_txn.gm_sav_lot_order_error(p_order_id, v_lot_error_msg, v_pnum, v_cnum);
                 
                 --This procedure will save the Error details in t5063_lot_error_info table 
                 gm_pkg_op_lot_error_txn.gm_sav_lot_error(v_lot_error_type, v_lot_error_msg, v_pnum, v_cnum, p_order_id, v_order_date, v_order_info_id, p_user_id, 107980, v_actual_fs, NULL); --107980 order
                 
            END IF;
       			
       	END IF;
       			
       			IF v_pkey IS NULL OR (v_pkey IS NOT NULL AND v_cnum_fl = TRUE) THEN
       				gm_pkg_op_lot_track.gm_lot_track_main(v_pnum,v_qty,v_apporder_id,p_user_id,'4000339','4302','4310',v_cnum);
       			END IF;
--            	 ELSE
--            	 
--            	 v_error_msg:= 'Please Enter Valid Control Number For Part: '|| v_pnum ;
--            		raise_application_error
--                         ('-20999',
--                            v_error_msg
--                          );
--               END IF;
            		
            END IF;
           
            -- No need to show the validation if the procedure is calling from Edit order screen
    	 	IF v_pnum IS NOT NULL AND v_cnum IS  NULL AND v_attribute_type = 101723 AND p_screen != 'EditOrder' --101723 (Control Number Type)
            THEN
            /*The Control number need to be entered for parts which are mappped to corresponding Rule Group.
             If The control number is not entered for required parts,then it will throw validation.*/
            v_flag := gm_pkg_op_do_process_trans.get_control_number_needed_fl(v_pnum);
            
            	IF v_flag = 'Y'
            	THEN
            		v_error_msg:= 'Please enter valid lot number for part: '|| v_pnum ||' from order id: ' ||v_ord_id  ;
            		raise_application_error
                           ('-20999',
                            v_error_msg
                           );
                 END IF;
            END IF;            	
       	END LOOP;
    END IF;
    
     -- To validate for the attribute qty and item qty.
     --The below condition is added for MDO-109 .there should not be any any validation if the order is raised from mobile App. 
     IF ( v_order_source  <>'103521') 
     THEN
     gm_validate_do_attribute_qty(p_order_id,101723);
     END IF;
     
     --Update Lot inventory
     --gm_pkg_op_lot.gm_sav_edit_lot_data(p_order_id, p_user_id);

	 END gm_sav_control_number;
  
 /***********************************************************
 * Description : Procedure to validate qty for the 
 *                Sterile Parts in item order attribute table.
 *                
 ***********************************************************/  
 PROCEDURE gm_validate_do_attribute_qty (
      p_order_id          IN   t502a_item_order_attribute.c501_order_id%TYPE,
      p_attribute_type    IN   t502a_item_order_attribute.c901_attribute_type%TYPE
 )
 AS

 	  CURSOR v_cursor IS
    	SELECT t502.pnum, NVL(aqty,0) aqty, iqty
     from t205_part_number t205
      , (select t502b.c205_part_number_id pnum,  SUM(t502b.c502b_item_qty) aqty 
      from t502b_item_order_usage t502b 
      , ( SELECT C501_ORDER_ID
           FROM t501_Order t501
          WHERE t501.c501_void_fl IS NULL
            START WITH T501.c501_order_id =
            (
                 SELECT NVL (t501.c501_parent_order_id, t501.c501_order_id)
                   FROM t501_order t501
                  WHERE c501_order_id = p_order_id
            )
            CONNECT BY NOCYCLE PRIOR t501.c501_order_id = t501.c501_parent_order_id) ORD
      where t502b.c501_order_id = ORD.C501_ORDER_ID
      --AND t502a.c901_attribute_type = 101723	 
      AND t502b.c502b_void_fl is NULL
      GROUP BY t502b.c205_part_number_id
      ) t502b
      , (SELECT t502.c205_part_number_id pnum, SUM(t502.c502_item_qty) iqty
      from t502_item_order t502
       , ( SELECT C501_ORDER_ID
           FROM t501_Order t501
          WHERE t501.c501_void_fl IS NULL
            START WITH T501.c501_order_id =
            (
                 SELECT NVL (t501.c501_parent_order_id, t501.c501_order_id)
                   FROM t501_order t501
                  WHERE c501_order_id = p_order_id
            )
            CONNECT BY NOCYCLE PRIOR t501.c501_order_id = t501.c501_parent_order_id) ORD
      where t502.c501_order_id = ORD.C501_ORDER_ID
      AND t502.C502_VOID_FL IS NULL
      GROUP BY t502.c205_part_number_id) t502
     where t205.c205_part_number_id = t502.pnum       
      AND t502.pnum = t502b.pnum;
      
       CURSOR v_att_cursor IS
       SELECT t502b.pnum, aqty, NVL(iqty,0) iqty
    	 from t205_part_number t205
      , (select t502b.c205_part_number_id pnum,  SUM(t502b.c502b_item_qty) aqty 
     	   from t502b_item_order_usage t502b 
      , ( SELECT C501_ORDER_ID
           FROM t501_Order t501
          WHERE t501.c501_void_fl IS NULL
            START WITH T501.c501_order_id =
            (
                 SELECT NVL (t501.c501_parent_order_id, t501.c501_order_id)
                   FROM t501_order t501
                  WHERE c501_order_id = p_order_id
            )
            CONNECT BY NOCYCLE PRIOR t501.c501_order_id = t501.c501_parent_order_id) ORD
      where t502b.c501_order_id = ORD.C501_ORDER_ID
      --AND t502a.c901_attribute_type = 101723	 
      AND t502b.c502b_void_fl is NULL
      GROUP BY t502b.c205_part_number_id
      ) t502b
      , (SELECT t502.c205_part_number_id pnum, SUM(t502.c502_item_qty) iqty
      from t502_item_order t502
       , ( SELECT C501_ORDER_ID
           FROM t501_Order t501
          WHERE t501.c501_void_fl IS NULL
            START WITH T501.c501_order_id =
            (
                 SELECT NVL (t501.c501_parent_order_id, t501.c501_order_id)
                   FROM t501_order t501
                  WHERE c501_order_id = p_order_id
            )
            CONNECT BY NOCYCLE PRIOR t501.c501_order_id = t501.c501_parent_order_id) ORD
      where t502.c501_order_id = ORD.C501_ORDER_ID
      AND t502.C502_VOID_FL IS NULL
      GROUP BY t502.c205_part_number_id) t502
     where t205.c205_part_number_id = t502b.pnum       
      AND t502b.pnum = t502.pnum;
      
      v_num NUMBER;
  BEGIN  
	  
	 FOR currindx IN v_cursor
		LOOP
			IF currindx.iqty > 0 AND currindx.aqty > currindx.iqty THEN
				raise_application_error('-20999','Control Number used in O.R. is recorded for the part# '||currindx.pnum || ' for ' || currindx.aqty || ' qty(s). <br>&nbsp;Please update that before editing or voiding this order.' );					 
			END IF;
	 END LOOP;
		
	 FOR currattindx IN v_att_cursor
		LOOP
			IF currattindx.iqty > 0 AND currattindx.aqty > currattindx.iqty THEN
				raise_application_error('-20999','Control Number used in O.R. is recorded for the part# '||currattindx.pnum || ' for ' || currattindx.aqty || ' qty(s). <br>&nbsp;Please update that before editing or voiding this order.' );			
			END IF;
	 END LOOP;	
		
   END gm_validate_do_attribute_qty;

 /***********************************************************
 * Description : Procedure to get control number needed flag for 
 *                Sterile Parts in part number table.
 *  Parameter: part number id
 *  Return: 'Y' or ''.                
 ***********************************************************/
   
 FUNCTION get_control_number_needed_fl (
  p_part_num	 t205_part_number.c205_part_number_id%TYPE,
  p_flag         VARCHAR2 DEFAULT 'N'	
 )
	RETURN VARCHAR2
IS
	v_count NUMBER;

	BEGIN
	   
	   BEGIN
	   
	  IF p_flag = 'Y'  THEN --- ADD BELOW CODE TO CHECK STERILE IMPLANTS PARTS
	  SELECT COUNT(1) 
 	   INTO v_count 
 	   FROM t205_part_number t205
      WHERE t205.c205_part_number_id = p_part_num
        AND c205_product_family = 4050 --IMPLANTS
        AND C205_product_class = 4030; --STERILE
	  ELSIF p_flag = 'N' THEN 
	   SELECT COUNT(1) INTO v_count FROM t205_part_number t205
	  where t205.c205_part_number_id = p_part_num
	  --MDO-7 The validation of control number will be done by the Part Material and the Part Class rather than by the Part Project.
       AND C205_PRODUCT_MATERIAL IN (
		     SELECT c906_rule_value
		       FROM t906_rules
		      WHERE c906_rule_id     = 'PART_MAT'
		        AND c906_rule_grp_id = 'ORCNUM'
		        AND c906_void_fl IS NULL
	   )
	   AND C205_product_class IN (
		     SELECT c906_rule_value
		       FROM t906_rules
		      WHERE c906_rule_id     = 'PART_STER'
		        AND c906_rule_grp_id = 'ORCNUM'
		        AND c906_void_fl IS NULL
	   );
	END IF;
	EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		RETURN '';
	END;
	IF v_count = 1 THEN
		RETURN 'Y';
	ELSE
		RETURN 'N';
	END IF;

END get_control_number_needed_fl;
--

 /***********************************************************
 * Description : Procedure to validate qty for the 
 *                Sterile Parts in item order attribute table.
 *                
 ***********************************************************/  
 PROCEDURE gm_fch_do_ctrl_details (
      p_order_id          IN   t502a_item_order_attribute.c501_order_id%TYPE
    , p_ctrlnumdetails   OUT		TYPES.cursor_type
      
 )
 AS
 BEGIN
	 gm_pkg_cs_usage_lot_number.gm_fch_order_usage_dtls (p_order_id, p_ctrlnumdetails);
		
   END gm_fch_do_ctrl_details;
   
   /**************************************************************************************************************************************
  * Description : Procedure to get the Order Hold Flag based on the price difference calculated from predefined discount percentage.
  *                
  **************************************************************************************************************************************/ 

   PROCEDURE gm_cs_fch_order_hold_per(
      p_order_id          IN   t502a_item_order_attribute.c501_order_id%TYPE
    , p_hold_fl           OUT   VARCHAR2
   )
   AS
   
    	v_ord_hold_fl   VARCHAR2(2);
        v_per_price   	VARCHAR2 (100);
   		v_total_price   VARCHAR2 (100);
   		v_percentage	VARCHAR2 (100);
   		v_ext_Price		VARCHAR2 (100);
   		v_company_id    t1900_company.c1900_company_id%TYPE;
   		
   		CURSOR order_parts
		IS
		    SELECT NVL(get_account_part_pricing(t501.c704_account_id,t502.c205_part_number_id,0),0) actualprice
                 , NVL(t502.c502_item_price,0) itemprice 
                 , t502.c205_part_number_id partnumber
                 , t501.c704_account_id accountid
                 , T502.C502_ITEM_QTY QTY
              FROM t501_order t501,t502_item_order t502 
             WHERE t501.c501_order_id = t502.c501_order_id 
               AND t501.c501_order_id = p_order_id
               AND t501.c501_void_fl IS NULL
               AND t502.c502_void_fl IS NULL;
              
       
   	  BEGIN   	
	   	  
	   	  --Getting company id from context.   
		    SELECT get_compid_frm_cntx()
			  INTO v_company_id 
			  FROM dual;
	   	  
	   	  -- It will item price with percentage price, If the item price is No.of % less of account price then make the order hold
	   	  
	   	  v_percentage := NVL(GET_RULE_VALUE_BY_COMPANY('PERCENTAGE','HOLD_ORD_PERCENTAGE',v_company_id),'N'); --need to verify the percentage calculation, only if percentage is given in rules table
	   	  
	   	  IF v_percentage  = 'N'
          THEN
            	v_ord_hold_fl := 'Y';
            	p_hold_fl := v_ord_hold_fl;	       
                RETURN;
          END IF;
	   	  
	   	  
	   	  FOR currindex IN order_parts
		  LOOP
		  
		  	-- IF the portal price is 0 or less, then need to hold the order
		  	IF currindex.actualprice <= 0
		  	THEN
		  		v_ord_hold_fl := 'Y';
            	p_hold_fl := v_ord_hold_fl;	       
                RETURN;
		  	END IF;
		  	
		  	v_per_price := currindex.actualprice - (currindex.actualprice * v_percentage/100); --Calculating the percentage of one part
		  	v_total_price := ROUND(currindex.qty * v_per_price,2); -- total number of parts * price after calculating the percentage
		  	v_ext_Price := currindex.qty * currindex.itemprice; --item price calculated from the screen * quantity

		  	IF to_number(v_ext_Price) < to_number(v_total_price)
			THEN
				v_ord_hold_fl := 'Y';
				-- If any one part is having price less than the discount percentage in rules table, need to hold the order. So can exit the loop
				EXIT;
			ELSE
				v_ord_hold_fl := 'N';	
			END IF;
		
		END LOOP;
		
	   	p_hold_fl := v_ord_hold_fl;	
	   	   	
	   END gm_cs_fch_order_hold_per;
	/***********************************************************
	* Description : Procedure to validate the duplicate the order based on acct, part and qty.
	* Author        : Ritesh
	***********************************************************/
	PROCEDURE gm_fch_validate_dousage(
	    p_order_id IN t502a_item_order_attribute.c501_order_id%TYPE ,
	    p_acctid   IN t501_order.c704_account_id%TYPE ,
	    p_doexistfl OUT VARCHAR2 ,
	    p_existing_ordid OUT t501_order.c501_order_id%type )
	AS
	  CURSOR c_exist_order
	  IS
	    SELECT t501.c501_order_id orderid
	    FROM t501_order t501
	    WHERE t501.c704_account_id = p_acctid
	    AND t501.c501_order_date >= TRUNC(CURRENT_DATE-1)-- This Code is changed for PMT-5979 Where This check should be done only 
	    										--  if that combination of Accounts-Parts-Quantity are repeated within the same day and previous day.
	    AND t501.c501_void_fl             IS NULL
	    AND t501.c501_parent_order_id     IS NULL
	    AND t501.c501_order_id            <> p_order_id; --current order ;
	  CURSOR c_order_usage (p_old_orderid IN t501_order.c501_order_id%type)
	  IS
	    SELECT old_ord.pnum pnum,
	      SUM(old_ord.iqty) iqty
	    FROM
		      (
		      SELECT t502.c205_part_number_id pnum,
		        SUM(t502.c502_item_qty) iqty
		      FROM t501_order t501,t502_item_order t502
		      WHERE t501.c501_order_id = t502.c501_order_id
		      AND t501.c501_order_id   = p_old_orderid
		      AND t501.c501_void_fl   IS NULL
		      GROUP BY t502.c205_part_number_id
		    UNION ALL
		    SELECT t502.c205_part_number_id pnum,
		      SUM(t502.c502_item_qty) iqty
		    FROM t501_order t501,t502_item_order t502
		    WHERE t501.c501_order_id      = t502.c501_order_id
		    AND t501.c501_parent_order_id = p_old_orderid
		    AND t501.c501_void_fl        IS NULL
		    GROUP BY t502.c205_part_number_id
	      ) old_ord
	    GROUP BY old_ord.pnum;
	    v_cnt       NUMBER:=0;
	    v_doexistfl VARCHAR(1);
	    v_orderid t501_order.c501_order_id%type;
	    v_oldordid t501_order.c501_order_id%type;
	    v_newordid t501_order.c501_order_id%type;
	    v_old_orderid t501_order.c501_order_id%type;
	  BEGIN
	    FOR v_rec IN c_exist_order
	    LOOP
	      v_oldordid  := v_rec.orderid;
	      FOR v_usage IN c_order_usage (v_oldordid)
	      LOOP
	        BEGIN
	          SELECT COUNT(1)
	          INTO v_cnt
	          FROM
	            (SELECT curr_ord.pnum pnum,
	              SUM(curr_ord.iqty) iqty
	            FROM
	              (
		              SELECT t502.c205_part_number_id pnum,
		                SUM(t502.c502_item_qty) iqty
		              FROM t501_order t501,
		                t502_item_order t502
		              WHERE t501.c501_order_id = t502.c501_order_id
		              AND t501.c501_order_id   = p_order_id --'currordid'
		              AND t501.c501_void_fl   IS NULL
		              GROUP BY t502.c205_part_number_id
		              UNION ALL
		              SELECT t502.c205_part_number_id pnum,
		                SUM(t502.c502_item_qty) iqty
		              FROM t501_order t501,
		                t502_item_order t502
		              WHERE t501.c501_order_id      = t502.c501_order_id
		              AND t501.c501_parent_order_id = p_order_id--'currordid'
		              AND t501.c501_void_fl        IS NULL
		              GROUP BY t502.c205_part_number_id
	              ) curr_ord
	           	 GROUP BY curr_ord.pnum
	            ) currnt_do
	          WHERE currnt_do.pnum = v_usage.pnum
	          AND currnt_do.iqty   = v_usage.iqty;
	          IF v_cnt             > 0 THEN
	            v_doexistfl       := 'Y';
	          ELSE
	            v_doexistfl := 'N';
	            raise_application_error ('-20999','DO does not exist');
	          END IF;
	        EXCEPTION
	        WHEN OTHERS THEN
	          v_doexistfl := 'N';
	          --gm_procedure_log(' Inner Excep ---'||v_oldordid,v_doexistfl);
	          --commit;
	        END;
	      END LOOP;
	      --total qty check (in case if current ord has more parts then existing order.)
	      gm_validate_total_qty(v_oldordid,p_order_id, v_doexistfl);
	      p_doexistfl   := v_doexistfl;
	      IF v_doexistfl = 'Y' THEN
	        raise_application_error ('-20999','DO already exist');
	      END IF;
	      --gm_procedure_log(v_oldordid,p_doexistfl);
	      --commit;
	    END LOOP;
	    --gm_procedure_log('v_oldordid final--',p_doexistfl);
	    --commit;
	  EXCEPTION
	  WHEN OTHERS THEN
	    p_doexistfl      := 'Y';
	    p_existing_ordid := v_oldordid;
	    --gm_procedure_log('Outer Excep ---'||v_oldordid,p_doexistfl);
	    --commit;
	  END gm_fch_validate_dousage;
	  
	  /***********************************************************
	  * Description : Procedure to validate the total qty check (in case if current ord has more parts then existing order.)
	  * Author        : Ritesh
	  ***********************************************************/
	PROCEDURE gm_validate_total_qty(
	    p_existing_ordid IN t501_order.c501_order_id%type ,
	    p_order_id       IN t501_order.c501_order_id%type ,
	    p_doexistfl OUT VARCHAR2 )
	AS
	  v_cnt NUMBER;
	BEGIN
	  p_doexistfl := 'N';
	  SELECT COUNT(1)
	  INTO v_cnt
	  FROM
	    (SELECT currnt_do.pnum,
	      SUM(currnt_do.iqty)
	    FROM
		      (SELECT t502.c205_part_number_id pnum,
		        SUM(t502.c502_item_qty) iqty
		      FROM t501_order t501,
		        t502_item_order t502
		      WHERE t501.c501_order_id = t502.c501_order_id
		      AND t501.c501_order_id   = p_order_id
		      AND t501.c501_void_fl   IS NULL
		      GROUP BY t502.c205_part_number_id
		      UNION ALL
		      SELECT t502.c205_part_number_id pnum,
		        SUM(t502.c502_item_qty) iqty
		      FROM t501_order t501,
		        t502_item_order t502
		      WHERE t501.c501_order_id      = t502.c501_order_id
		      AND t501.c501_parent_order_id = p_order_id
		      AND t501.c501_void_fl        IS NULL
		      GROUP BY t502.c205_part_number_id
	      ) currnt_do
	    GROUP BY currnt_do.pnum
	    MINUS
	    SELECT old_ord.pnum,
	      SUM(old_ord.iqty)
	    FROM
	      (
		      SELECT t502.c205_part_number_id pnum,
		        SUM(t502.c502_item_qty) iqty
		      FROM t501_order t501,
		        t502_item_order t502
		      WHERE t501.c501_order_id = t502.c501_order_id
		      AND t501.c501_order_id   = p_existing_ordid --v_newordid
		      AND t501.c501_void_fl   IS NULL
		      GROUP BY t502.c205_part_number_id
		      UNION ALL
		      SELECT t502.c205_part_number_id pnum,
		        SUM(t502.c502_item_qty) iqty
		      FROM t501_order t501,
		        t502_item_order t502
		      WHERE t501.c501_order_id      = t502.c501_order_id
		      AND t501.c501_parent_order_id = p_existing_ordid --v_newordid
		      AND t501.c501_void_fl        IS NULL
		      GROUP BY t502.c205_part_number_id
	      ) old_ord
	    GROUP BY old_ord.pnum
	    );
	  ---DBMS_OUTPUT.PUT_LINE('total qty check - v_cnt---'||v_cnt);
	  IF v_cnt       = 0 THEN
	    p_doexistfl := 'Y';
	  END IF;
	END gm_validate_total_qty;

	/*******************************************************************
   * Description : Procedure to update the ship cost for the order,
   *               this ship cost should reflect in the invoice.
   * Author      : VPrasath
   *******************************************************************/

   PROCEDURE gm_update_ship_cost (
	  p_orderid  IN t501_order.c501_order_id%TYPE
	, p_shipcost IN t501_order.c501_ship_cost%TYPE
    , p_userid   IN t907_shipping_info.c907_last_updated_by%TYPE
	)
	AS
	BEGIN
		UPDATE t501_order
		   SET c501_ship_cost = NVL(p_shipcost,0)
		  -- , c501_total_cost = NVL(c501_total_cost,0) + NVL(p_shipcost,0) 
			 , c501_last_updated_by = p_userid
			 , c501_last_updated_date = CURRENT_DATE
		 WHERE c501_order_id = p_orderid;
		 
		 -- To update the freight amount in shipping table
		UPDATE t907_shipping_info
		   SET c907_frieght_amt = NVL(p_shipcost,0)
			 , c907_last_updated_by = p_userid
			 , c907_last_updated_date = CURRENT_DATE
		 WHERE c907_ref_id = p_orderid
		   AND c907_void_fl IS NULL; 
	END gm_update_ship_cost;    
	
  /*****************************************************************************************************************************************
   * Description : Procedure to update the Item Price as Unit Price for the Parts which are not having enough qty during Book New DO from CSM               
   * Author      : HReddi
   *******************************************************************************************************************************************/

   PROCEDURE gm_update_unit_price (
	  p_orderid  IN t501_order.c501_order_id%TYPE
    , p_userid   IN t907_shipping_info.c907_last_updated_by%TYPE
	)
	AS
		v_unit_price t502_item_order.c502_item_price%TYPE;
		v_account_price VARCHAR2(50);
		v_adj_code_value VARCHAR2(50);
        
		 CURSOR v_bo_cur IS
	         SELECT t501.c501_order_id ord_id,
                t501.c501_parent_order_id parent_ord_id , t502.c502_item_order_id itemid , t502.c205_part_number_id partid 
                , t502.c502_item_price itemPrice , t502.c502_unit_price_adj_value adjValue , t502.c901_unit_price_adj_code adjCode 
                , c502_list_price listPrice
           FROM t501_order t501 , t502_item_order t502
          WHERE (t501.c501_order_id = p_orderid OR t501.c501_parent_order_id = p_orderid )
            AND t501.c501_order_id = t502.c501_order_id
            AND t501.c501_void_fl IS NULL
            AND t502.c502_void_fl IS NULL;
	BEGIN		
		FOR v_bo_index IN v_bo_cur
         LOOP
         	 BEGIN
	         	SELECT get_account_part_pricing(t501.c704_account_id, t502.c205_part_number_id,NULL) 
	         	  INTO v_unit_price
	           	  FROM t502_item_order t502
	           	     , T501_order t501
	          	 WHERE t501.c501_order_id = t502.c501_order_id
	          	   AND t501.c501_order_id = v_bo_index.ord_id
                   AND t502.c502_item_order_id = v_bo_index.itemid
	               AND t502.c502_void_fl IS NULL;
         	 EXCEPTION WHEN OTHERS THEN
		 		v_unit_price := 0;
		 	 END;
		 	 
		 	 --v_adj_code_value := gm_pkg_sm_adj_txn.get_adj_code_value(p_orderid,v_bo_index.partid,v_bo_index.itemid,v_bo_index.adjCode);
	         --v_account_price := v_bo_index.itemPrice + v_bo_index.adjValue + v_adj_code_value;
		 	 
		 	/* Updating the Item Price as its Unit Price and rest of the valeus are Updated to Default values for the Resulted Item Order ID v_item_order_id */
	         UPDATE t502_item_order 
	   		   SET c502_unit_price = v_unit_price
	             , c502_unit_price_adj_value = 0
	             , c901_unit_price_adj_code = NULL
	             , c502_list_price = v_bo_index.listPrice
	             , c502_discount_offered = NULL
	             , c901_discount_type = NULL
	             , c502_system_cal_unit_price = NULL
	             , C502_DO_UNIT_PRICE = v_unit_price
	             , c502_adj_code_value = 0
	         WHERE c502_item_order_id = v_bo_index.itemid;         
         END LOOP;
	END gm_update_unit_price; 
	
	/***********************************************************
     * Description : Procedure to move an order to Hold Status
     *               and to send out an email to AR Team and PC 
     *               Team
     ***********************************************************/
      PROCEDURE gm_mnul_move_ord_hold (
          p_txn_id          IN   t907_cancel_log.c907_ref_id%TYPE
        , p_cancelreason      IN   t907_cancel_log.c901_cancel_cd%TYPE
        , v_cancelid          IN   t907_cancel_log.c907_cancel_log_id%TYPE
        , p_comments          IN   t907_cancel_log.c907_comments%TYPE
        , p_userid            IN   t102_user_login.c102_user_login_id%TYPE
      )
      AS      	
      BEGIN
	      gm_pkg_op_do_process_trans.gm_cs_sav_order_hold(p_txn_id,p_cancelreason,v_cancelid,p_comments,p_userid,'Y');
	      
	  END gm_mnul_move_ord_hold;
	  	  /***********************************************************
     * Description : Procedure to record Usage Lot #s for Sterile
     *               Parts used in Surgery for DOs booked from 
     *               the 'Case Book' 
     ***********************************************************/
    PROCEDURE gm_sav_case_book_order_usage (
        p_order_id   IN   t501_order.c501_order_id%TYPE,
        p_user_id    IN   t501_order.c501_created_by%TYPE    
     )
     AS  
     v_flag             VARCHAR2(2);
     v_qty              NUMBER;
     v_order_info_id    VARCHAR2(20);
     v_pnum             t502a_item_order_attribute.c205_part_number_id%TYPE;
     v_case_info_id	   t7100_case_information.c7100_case_info_id%TYPE;
     v_part_num         t502a_item_order_attribute.c205_part_number_id%TYPE; 
   		
   	v_receive_mode 		t501_order.C501_receive_mode%TYPE; 
   		 /* Fetch the part and qty details */
   	CURSOR v_curr
    IS  
        SELECT c500_order_info_id orderinfoid,c205_part_number_id pnum,c502_item_qty  qty
	      FROM t502_item_order
         WHERE c501_order_id       = p_order_id				   
	       AND c502_delete_fl     IS NULL
	       AND c502_void_fl       IS NULL;
			  
     BEGIN
	    
	    -- Added for PMT-21585 ,to save item order details in order info table from case book DO is created 
	     gm_pkg_cs_usage_lot_number.gm_sav_order_info (p_order_id, '', p_user_id) ;
	    
	     BEGIN
	         /* Fetch the case information id */
	          SELECT c7100_case_info_id into v_case_info_id 
	            FROM t501_order 
	           WHERE c501_order_id= p_order_id 
	             AND c501_void_fl IS NULL;
	       EXCEPTION WHEN NO_DATA_FOUND THEN
		     v_case_info_id := NULL;
	     END;
	     
	      --PBUG-4611  Order Detail Edit is not reflecting in Edit Order Screen & BUG-13312 
         BEGIN
			SELECT C501_receive_mode  INTO v_receive_mode 
			  FROM t501_order 
	           WHERE c501_order_id= p_order_id 
	             AND c501_void_fl IS NULL;
	       EXCEPTION WHEN NO_DATA_FOUND THEN
		     v_receive_mode := NULL;
	     END;
	     IF v_case_info_id IS NOT NULL
	     THEN
	             /* Update void flag if case nformation id is present*/
	          UPDATE T502B_ITEM_ORDER_USAGE
                 SET c502b_void_fl            = 'Y'
            	     ,c502b_last_updated_by   = p_user_id
                     ,c502b_last_updated_date = CURRENT_DATE
               WHERE c501_order_id   = p_order_id;

           
		       FOR v_index IN v_curr
               LOOP
      	              v_order_info_id := v_index.orderinfoid;
                      v_part_num 	  := v_index.pnum;
                      v_qty 		  := v_index.qty;

                      /* fetch the control number needed flag for Sterile Parts in part number table*/
                      v_flag          := gm_pkg_op_do_process_trans.get_control_number_needed_fl(v_part_num);
             
                      IF v_flag = 'Y' OR v_receive_mode = '26230683' -- 26230683 -  iPad
                      THEN
           	                   gm_pkg_cs_usage_lot_number.gm_sav_usage_dtls (p_order_id, null, v_part_num, v_qty, NULL, NULL, v_order_info_id, NULL, NULL,p_user_id);
                      END IF;
                 END LOOP;
            ELSIF v_receive_mode = '26230683' -- 26230683 -  iPad
            THEN
            	-- PC-3521-Lot Number is Lost once DO is edited from DO Summary Screen
            	gm_pkg_op_do_process_trans.gm_sav_do_book_order_usage(p_order_id, p_user_id);
            END IF;   
       
	  END gm_sav_case_book_order_usage;
	  
 /***********************************************************************
  * Description : Procedure to validate repid is mapped to the account
  ***********************************************************************/
    PROCEDURE gm_validate_rep_id (
         p_accid    	IN  t704_account.c704_account_id%TYPE DEFAULT NULL,
      	 p_rep_id   	IN  t704_account.C703_SALES_REP_ID %TYPE DEFAULT NULL,
      	 p_company_id   IN  t1900_company.c1900_company_id%type,
         p_err_str		OUT	VARCHAR2         
    )
    AS
    v_company_id	t1900_company.c1900_company_id%type;
    v_count			NUMBER;
    BEGIN
  
			SELECT COUNT (1)
           	  INTO v_count
              FROM t704_account
             WHERE c704_account_id = p_accid
               AND c703_sales_rep_id = p_rep_id         
               AND c1900_company_id = p_company_id
               AND c704_void_fl IS NULL;
               
            IF v_count = 0 THEN
            	p_err_str  :=  'The Selected Rep Id is not mapped to the account';
        	END IF;
	    
	END gm_validate_rep_id;	 
	
	
 /***********************************************************************
  * Description : Procedure to save book order usage
  ***********************************************************************/
	 
	PROCEDURE gm_sav_do_book_order_usage (
	    p_order_id   IN   t501_order.c501_order_id%TYPE,
	    p_user_id    IN   t501_order.c501_created_by%TYPE
	) AS
	
	    v_qty             NUMBER;
	    v_order_info_id   VARCHAR2(20);
	    v_pnum            t502a_item_order_attribute.c205_part_number_id%TYPE;
	    v_part_num        t502a_item_order_attribute.c205_part_number_id%TYPE;
	    v_lot_num         t502b_item_order_usage.c502b_usage_lot_num%TYPE;
	    CURSOR t502b_cur (
	        v_part_num t502b_item_order_usage.c205_part_number_id%TYPE
	    ) IS
	    SELECT
	    	c502b_item_order_usage_id usage_id,
	        c502b_item_qty        qty,
	        c502b_usage_lot_num   lot_num
	    FROM
	        t502b_item_order_usage
	    WHERE
	        c501_order_id = p_order_id
	        AND c502b_void_fl IS NULL
	        AND c205_part_number_id = v_part_num;
	
	    CURSOR t502_cur IS
	    SELECT
	        c500_order_info_id    info_id,
	        c205_part_number_id   part,
	        c502_item_qty         t503qty
	    FROM
	        t502_item_order
	    WHERE
	        c501_order_id = p_order_id
	        AND c502_void_fl IS NULL
	        AND c502_delete_fl IS NULL;
	        
		CURSOR v_curr IS  
		SELECT 
			c500_order_info_id orderinfoid,
			c205_part_number_id pnum,
			c502_item_qty  qty
		FROM 
			t502_item_order
		WHERE 
			c501_order_id      	   = p_order_id		   
			AND c502_delete_fl     IS NULL
			AND c502_void_fl       IS NULL
		    AND C205_PART_NUMBER_ID  
		    	NOT IN 
		    		(SELECT 
		    			C205_PART_NUMBER_ID  
		    		FROM 
		    			t502b_item_order_usage 
		           WHERE 
		           		c501_order_id = p_order_id 
		           		AND c502b_void_fl IS NULL);
	BEGIN
	    FOR t502_ind IN t502_cur 
	    LOOP 	      
		  FOR t502b_ind IN t502b_cur(t502_ind.part) 
		  LOOP
			BEGIN
			  UPDATE t502b_item_order_usage
			  SET 
			  	c500_order_info_id = t502_ind.info_id,
			  	C502B_LAST_UPDATED_DATE = current_date , 
			  	C502B_LAST_UPDATED_BY =p_user_id
			  WHERE 
			  	C502B_ITEM_ORDER_USAGE_ID = t502b_ind.usage_id 
			  	AND C502B_VOID_FL IS NULL;
			END;
		  END LOOP;
	    END LOOP;
	
		FOR v_index IN v_curr
        LOOP
      	    v_order_info_id := v_index.orderinfoid;
            v_part_num 	  	:= v_index.pnum;
            v_qty 		  	:= v_index.qty;
            
           	gm_pkg_cs_usage_lot_number.gm_sav_usage_dtls (p_order_id, NULL, v_part_num, v_qty, NULL, NULL, v_order_info_id, NULL, NULL, p_user_id);
                     
        END LOOP;
        
		    UPDATE t502b_item_order_usage
			SET 
				c502b_void_fl='Y' 
			WHERE 
				c501_order_id = p_order_id
				AND c502b_void_fl IS NULL 
				AND c500_order_info_id 
					NOT IN 
						(SELECT 
							c500_order_info_id 
						FROM 
							t502_item_order 
						WHERE 
							c501_order_id = p_order_id 
							AND c502_void_fl IS NULL);
							
	END gm_sav_do_book_order_usage;	
 /***********************************************************
 * Description : Procedure to validate Quantity mismatch in Do summary screen.
 ***********************************************************/  
 PROCEDURE gm_val_do_attr_qty (
      p_order_id          IN   t502a_item_order_attribute.c501_order_id%TYPE,
      p_attribute_type    IN   t502a_item_order_attribute.c901_attribute_type%TYPE
 )
 AS
	CURSOR v_cursor IS
    	SELECT t502.pnum, NVL(aqty,0) aqty, iqty
     	FROM t205_part_number t205, 
     	(SELECT t502b.c205_part_number_id pnum, SUM(t502b.c502b_item_qty) aqty 
      FROM t502b_item_order_usage t502b 
      , ( SELECT C501_ORDER_ID
           FROM t501_Order t501
          WHERE t501.c501_void_fl IS NULL
            START WITH T501.c501_order_id =
            (
                 SELECT NVL (t501.c501_parent_order_id, t501.c501_order_id)
                   FROM t501_order t501
                  WHERE c501_order_id = p_order_id
            )
            CONNECT BY NOCYCLE PRIOR t501.c501_order_id = t501.c501_parent_order_id) ORD
      WHERE t502b.c501_order_id = ORD.C501_ORDER_ID
      --AND t502a.c901_attribute_type = 101723	 
      AND t502b.c502b_void_fl IS NULL
      GROUP BY t502b.c205_part_number_id
      ) t502b
      , (SELECT t502.c205_part_number_id pnum, SUM(t502.c502_item_qty) iqty
      FROM t502_item_order t502
       , ( SELECT C501_ORDER_ID
           FROM t501_Order t501
          WHERE t501.c501_void_fl IS NULL
            START WITH T501.c501_order_id =
            (
                 SELECT NVL (t501.c501_parent_order_id, t501.c501_order_id)
                   FROM t501_order t501
                  WHERE c501_order_id = p_order_id
            )
            CONNECT BY NOCYCLE PRIOR t501.c501_order_id = t501.c501_parent_order_id) ORD
      WHERE t502.c501_order_id = ORD.C501_ORDER_ID
      AND t502.C502_VOID_FL IS NULL
      GROUP BY t502.c205_part_number_id) t502
     WHERE t205.c205_part_number_id = t502.pnum       
      AND t502.pnum = t502b.pnum;
      
       CURSOR v_att_cursor IS
       SELECT t502b.pnum, aqty, NVL(iqty,0) iqty
    	 FROM t205_part_number t205
      , (SELECT t502b.c205_part_number_id pnum, SUM(t502b.c502b_item_qty) aqty 
     	   FROM t502b_item_order_usage t502b 
      , ( SELECT C501_ORDER_ID
           FROM t501_Order t501
          WHERE t501.c501_void_fl IS NULL
            START WITH T501.c501_order_id =
            (
                 SELECT NVL (t501.c501_parent_order_id, t501.c501_order_id)
                   FROM t501_order t501
                  WHERE c501_order_id = p_order_id
            )
            CONNECT BY NOCYCLE PRIOR t501.c501_order_id = t501.c501_parent_order_id) ORD
      WHERE t502b.c501_order_id = ORD.C501_ORDER_ID
      --AND t502a.c901_attribute_type = 101723	 
      AND t502b.c502b_void_fl IS NULL
      GROUP BY t502b.c205_part_number_id
      ) t502b
      , (SELECT t502.c205_part_number_id pnum, SUM(t502.c502_item_qty) iqty
      FROM t502_item_order t502
       , ( SELECT C501_ORDER_ID
           FROM t501_Order t501
          WHERE t501.c501_void_fl IS NULL
            START WITH T501.c501_order_id =
            (
                 SELECT NVL (t501.c501_parent_order_id, t501.c501_order_id)
                   FROM t501_order t501
                  WHERE c501_order_id = p_order_id
            )
            CONNECT BY NOCYCLE PRIOR t501.c501_order_id = t501.c501_parent_order_id) ORD
      WHERE t502.c501_order_id = ORD.C501_ORDER_ID
      AND t502.C502_VOID_FL IS NULL
      GROUP BY t502.c205_part_number_id) t502
     WHERE t205.c205_part_number_id = t502b.pnum       
      AND t502b.pnum = t502.pnum;
      
      v_num NUMBER;
  BEGIN  
	  
	 FOR currindx IN v_cursor
		LOOP
			IF currindx.iqty > 0 AND currindx.aqty <> currindx.iqty THEN
				raise_application_error('-20999','Control Number used in O.R. is recorded for the part# '||currindx.pnum || ' for ' || currindx.aqty || ' qty(s). <br>&nbsp;Please update that before editing or voiding this order.');					 
			END IF;
	 END LOOP;
		
	 FOR currattindx IN v_att_cursor
		LOOP
			IF currattindx.iqty > 0 AND currattindx.aqty <> currattindx.iqty THEN
				raise_application_error('-20999','Control Number used in O.R. is recorded for the part# '||currattindx.pnum || ' for ' || currattindx.aqty || ' qty(s). <br>&nbsp;Please update that before editing or voiding this order.');			
			END IF;
	 END LOOP;	
   END gm_val_do_attr_qty;	
 /*******************************************************************************************************
 * Description : Procedure to get control number needed flag by divion for 
 *               Joint Reconstruction sterile Implats Parts in part number table and project table.
 *  Parameter: part number id
 *  Return: 'Y' or ''.                
 ***************************************************************************************************************/    
 FUNCTION get_ctrl_num_needed_fl_by_division (
  p_part_num	 t205_part_number.c205_part_number_id%TYPE
)
	RETURN VARCHAR2
IS
	v_count NUMBER;
    v_company_id t906_rules.c1900_company_id%TYPE;
BEGIN
		
 SELECT get_compid_frm_cntx() INTO  v_company_id FROM DUAL;
 
 	 BEGIN  
	   SELECT COUNT(1)INTO v_count FROM t205_part_number t205,t202_project t202
	    where t205.c205_part_number_id = p_part_num
        AND t205.c202_project_id = t202.c202_project_id
        AND t202.c202_void_fl is null
        AND t202.c1910_division_id IN (
                         SELECT c906_rule_value
                           FROM t906_rules
                          WHERE c906_rule_id     = 'DIVISION_ID'
                            AND c906_rule_grp_id = 'ACCOUNT_DIV_JR'
                            AND c1900_company_id = v_company_id
                            AND c906_void_fl IS NULL
                   )
        AND C205_product_class IN (
		     SELECT c906_rule_value
		       FROM t906_rules
		      WHERE c906_rule_id     = 'PART_STER'
		        AND c906_rule_grp_id = 'ORCNUM'
		        AND c906_void_fl IS NULL
	          )
        AND C205_PRODUCT_FAMILY IN (
	           SELECT c906_rule_value
			       FROM t906_rules
			      WHERE c906_rule_id     = 'PART_FAMILY'
			        AND c906_rule_grp_id = 'JR_PART_NUM'
			         AND c1900_company_id = v_company_id
			        AND c906_void_fl IS NULL
	);
	EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		RETURN 'N';
	END;
	IF v_count = 1 THEN
		RETURN 'Y';
	ELSE
		RETURN 'N';
	END IF;

END get_ctrl_num_needed_fl_by_division;

END GM_PKG_OP_DO_PROCESS_TRANS;
/