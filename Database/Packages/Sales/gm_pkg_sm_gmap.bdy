-- @"C:\Database\Packages\Sales\gm_pkg_sm_gmap.bdy";
CREATE OR REPLACE
PACKAGE BODY gm_pkg_sm_gmap IS

PROCEDURE gm_fch_salesrep_addressinfo(
	p_out	OUT Types.cursor_type
)
AS
BEGIN
	OPEN p_out FOR 
		 
		SELECT 'T106_ADDRESS' TBL,to_char(c106_address_id) ID, c106_add1 ADDR1, c106_add2 ADDR2, c106_city CITY,
		 		get_code_name(c901_state) STATE, get_code_name(c901_country) COUNTRY,c106_zip_code ZIPCODE
			   FROM t106_address
			   WHERE  c106_void_fl    	IS NULL
			   AND c106_latitude 		IS NULL
			   AND c106_longitude 		IS NULL
			   AND c106_geo_location	IS NULL;
			   
END gm_fch_salesrep_addressinfo;

PROCEDURE gm_sav_salesrep_latlon_info(
                p_tabletype    IN VARCHAR2,
                p_id           IN VARCHAR2,
                p_latitude     IN VARCHAR2,
                p_longitude    IN VARCHAR2,
                p_userid       IN t106_address.c106_last_updated_by%TYPE
)
AS
                                
BEGIN
      IF p_id IS NOT NULL AND (p_latitude IS NOT NULL OR p_longitude IS NOT NULL)
      THEN
          IF (p_tabletype = 'T106_ADDRESS')  
          THEN
              UPDATE t106_address SET
              		 c106_latitude = p_latitude,
              		 c106_longitude = p_longitude,
                     c106_geo_location = SDO_GEOMETRY(2001,8307,SDO_POINT_TYPE(p_longitude,p_latitude,NULL),NULL,NULL),
                     c106_last_updated_by = p_userid,
                     c106_last_updated_date = SYSDATE
                     WHERE C106_ADDRESS_ID = to_number(p_id) AND C106_VOID_FL IS NULL;
	           
	  	  END IF;
      END IF;
                  
END gm_sav_salesrep_latlon_info;

PROCEDURE gm_fch_dist_addressinfo(
	p_out	OUT Types.cursor_type
)
AS
BEGIN
	
	OPEN p_out FOR 
		 
		select 'T701_DISTRIBUTOR' TBL,c701_distributor_id ID,c701_ship_add1 ADDR1,c701_ship_add2 ADDR2,c701_ship_city CITY,get_code_name(c701_ship_state) STATE,
		        get_code_name(c701_ship_country) COUNTRY,c701_ship_zip_code ZIPCODE,c701_bill_add1 BADDR1,c701_bill_add2 BADDR2,c701_bill_city BCITY,
		        get_code_name(c701_bill_state) BSTATE,get_code_name(c701_bill_country) BCOUNTRY,c701_bill_zip_code BZIPCODE
		        FROM t701_distributor
		        where c701_void_fl    		IS NULL
				AND (c901_ext_country_id         is NULL
				OR c901_ext_country_id  in (select country_id from v901_country_codes))
		        AND (( C701_BILL_LATITUDE 	IS NULL
		    			AND C701_BILL_LONGITUDE   	IS NULL
		    			AND c701_bill_geo_location 	IS NULL
		    			AND c701_bill_add1         	IS NOT NULL)
	    			OR ( C701_SHIP_LATITUDE    	IS NULL
		    			AND C701_SHIP_LONGITUDE    	IS NULL
		    			AND c701_ship_geo_location 	IS NULL
		    			AND c701_ship_add1         	IS NOT NULL));
					  
END gm_fch_dist_addressinfo;

PROCEDURE gm_sav_dist_latlon_info(
                p_tabletype    IN VARCHAR2,
                p_id           IN VARCHAR2,
                p_ship_latitude     IN VARCHAR2,
                p_ship_longitude    IN VARCHAR2,
                p_bill_latitude     IN VARCHAR2,
                p_bill_longitude    IN VARCHAR2,
                p_userid       IN t106_address.c106_last_updated_by%TYPE
)
AS
                                
BEGIN
      IF p_id IS NOT NULL
      THEN
      	IF (p_tabletype = 'T701_DISTRIBUTOR')  THEN
	      UPDATE t701_distributor SET  
		       	C701_SHIP_LATITUDE = NVL(p_ship_latitude,C701_SHIP_LATITUDE ),		
		      	C701_SHIP_LONGITUDE = NVL(p_ship_longitude,C701_SHIP_LONGITUDE),
		      	C701_SHIP_GEO_LOCATION = DECODE(p_ship_latitude,NULL,C701_SHIP_GEO_LOCATION,SDO_GEOMETRY(2001,8307,SDO_POINT_TYPE(p_ship_longitude,p_ship_latitude,NULL),NULL,NULL)),
		      	C701_BILL_LATITUDE = NVL(p_bill_latitude,C701_BILL_LATITUDE),
			    C701_BILL_LONGITUDE = NVL(p_bill_longitude,C701_BILL_LONGITUDE),		
			    C701_BILL_GEO_LOCATION = DECODE(p_bill_latitude,NULL,C701_BILL_GEO_LOCATION,SDO_GEOMETRY(2001,8307,SDO_POINT_TYPE(p_bill_longitude,p_bill_latitude,NULL),NULL,NULL)),
	            C701_last_updated_by = p_userid,
	            C701_last_updated_date = SYSDATE
	            WHERE C701_DISTRIBUTOR_ID = p_id AND C701_VOID_FL IS NULL;
	    END IF; 
      END IF;
                  
END gm_sav_dist_latlon_info;

PROCEDURE gm_fch_acc_addressinfo(
	p_out	OUT Types.cursor_type
)
AS
BEGIN
	OPEN p_out FOR 
		select 'T704_ACCOUNT' TBL,c704_account_id ID,c704_ship_add1 ADDR1,c704_ship_add2 ADDR2,c704_ship_city CITY,
		        get_code_name(c704_ship_state) STATE,get_code_name(c704_ship_country) COUNTRY,c704_ship_zip_code ZIPCODE,
		        c704_bill_add1 BADDR1,c704_bill_add2 BADDR2,c704_bill_city BCITY,get_code_name(c704_bill_state) BSTATE,
		        get_code_name(c704_bill_country) BCOUNTRY,c704_bill_zip_code BZIPCODE
		        FROM t704_account
		        where c704_void_fl    		IS NULL
			AND (c901_ext_country_id         IS NULL
			OR c901_ext_country_id  in (select country_id from v901_country_codes))
		        AND (( C704_BILL_LATITUDE	IS NULL
				        AND C704_BILL_LONGITUDE		IS NULL
				        AND C704_bill_geo_location	IS NULL
				        AND C704_bill_add1			IS NOT NULL)
			        OR ( C704_SHIP_LATITUDE		IS NULL
				        AND C704_SHIP_LONGITUDE		IS NULL
				        AND C704_ship_geo_location	IS NULL
				        AND C704_ship_add1			IS NOT NULL));
END gm_fch_acc_addressinfo;

PROCEDURE gm_sav_acc_latlon_info(
                p_tabletype    IN VARCHAR2,
                p_id           IN VARCHAR2,
                p_ship_latitude     IN VARCHAR2,
                p_ship_longitude    IN VARCHAR2,
                p_bill_latitude     IN VARCHAR2,
                p_bill_longitude    IN VARCHAR2,
                p_userid       IN t106_address.c106_last_updated_by%TYPE
)
AS
                                
BEGIN
	IF p_id IS NOT NULL
      THEN
		IF  p_tabletype = 'T704_ACCOUNT' 
		          THEN
		          	UPDATE t704_account SET  
			       			 C704_SHIP_LATITUDE = NVL(p_ship_latitude,C704_SHIP_LATITUDE),		
			      			 C704_SHIP_LONGITUDE = NVL(p_ship_longitude,C704_SHIP_LONGITUDE),
		                     C704_SHIP_GEO_LOCATION = DECODE(p_ship_longitude,NULL,C704_SHIP_GEO_LOCATION,SDO_GEOMETRY(2001,8307,SDO_POINT_TYPE(p_ship_longitude,p_ship_latitude,NULL),NULL,NULL)),
		                     C704_BILL_LATITUDE = NVL(p_bill_latitude,C704_BILL_LATITUDE),
				       		 C704_BILL_LONGITUDE = NVL(p_bill_longitude,C704_BILL_LONGITUDE),		
				      		 C704_bill_geo_location = DECODE(p_bill_longitude,NULL,C704_BILL_GEO_LOCATION,SDO_GEOMETRY(2001,8307,SDO_POINT_TYPE(p_bill_longitude,p_bill_latitude,NULL),NULL,NULL)),
		                     C704_last_updated_by = p_userid,
		                     C704_last_updated_date = SYSDATE
		                     WHERE C704_ACCOUNT_ID = p_id AND C704_VOID_FL IS NULL;  
		END IF;
      END IF;
                  
END gm_sav_acc_latlon_info;
	
END gm_pkg_sm_gmap;
/