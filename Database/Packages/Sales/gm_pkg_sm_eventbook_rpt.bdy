-- @"C:\database\Packages\Sales\gm_pkg_sm_eventbook_rpt.bdy";
CREATE OR REPLACE
PACKAGE BODY gm_pkg_sm_eventbook_rpt
IS
    
    /******************************************************************
    * Description	: Procedure to fetch Event Name - Active
    * Author		: Jignesh Shah
    ****************************************************************/
	PROCEDURE gm_fch_event_name_list (
		p_evename	 OUT   TYPES.cursor_type
	)
	AS
	v_company_id  t1900_company.c1900_company_id%TYPE;
	BEGIN
		select get_compid_frm_cntx() into v_company_id from dual;
		OPEN p_evename
		FOR
		   SELECT t7103.c7100_case_info_id cid
			    , t7103.c7103_name ename
			 FROM t7103_schedule_info t7103
			    , t7100_case_information t7100
			WHERE t7103.c7100_case_info_id = t7100.c7100_case_info_id
			  AND t7100.c901_case_status IN ('19524') --active
			  AND t7100.c901_type = 1006504 -- for type = 'event'
			  AND t7100.c7100_void_fl IS NULL
			  AND t7103.c1900_company_id =v_company_id
			  AND t7103.c1900_company_id  =t7100.c1900_company_id 
		 ORDER BY ename;
			
	END gm_fch_event_name_list;
	
	/********************************************************************
    * Description	: Procedure to fetch Event Name - without reschedule
    * Author		: Jignesh Shah
    *********************************************************************/
	PROCEDURE gm_fch_event_name_all_list (
		p_evename	 OUT   TYPES.cursor_type
	)
	AS
	v_company_id  t1900_company.c1900_company_id%TYPE;
	BEGIN
		select get_compid_frm_cntx() into v_company_id from dual;
		OPEN p_evename
		FOR
		   SELECT t7103.c7100_case_info_id cid
			    , t7103.c7103_name ename
			 FROM t7103_schedule_info t7103
			    , t7100_case_information t7100
			WHERE t7103.c7100_case_info_id = t7100.c7100_case_info_id
			  AND t7100.c901_case_status NOT IN ('19526') --rescheduled
			  AND t7100.c901_type = 1006504 -- for type = 'event'
			  AND t7100.c7100_void_fl IS NULL
			  AND t7103.c1900_company_id =v_company_id
			  AND t7103.c1900_company_id  =t7100.c1900_company_id 
		 ORDER BY ename;
			
	END gm_fch_event_name_all_list;
	
	/*********************************************************************
	* Description	: Procedure to fetch Even Report
	* Author		: Jignesh Shah
	**********************************************************************/
	PROCEDURE gm_fch_event_list_rpt (
        p_caseinfo_id  		IN  T7103_Schedule_Info.C7100_CASE_INFO_ID%TYPE,
        p_pur_of_event   	IN  T7103_Schedule_Info.C901_PURPOSE%TYPE,
        p_loan_to 		 	IN  T7103_Schedule_Info.C901_LOAN_TO%TYPE,
        p_loan_to_name		IN  T7103_Schedule_Info.C101_LOAN_TO_ID%TYPE,
        p_edtType			IN	VARCHAR2,
        p_start_date		IN  VARCHAR2,
        p_end_date			IN  VARCHAR2,
        p_out_event_list 	OUT TYPES.cursor_type
        )
	AS
		v_query VARCHAR2(4000);
		v_start_date VARCHAR2 (2000) ;
		v_company_id  t1900_company.c1900_company_id%TYPE;
		v_date_fmt VARCHAR2(100);
		
	BEGIN		
		
	select get_compid_frm_cntx(),get_compdtfmt_frm_cntx() into v_company_id,v_date_fmt from dual;
	
	v_query:= 'SELECT T7100.C7100_Case_Id Eveid, T7103.C7100_Case_Info_Id CID
					, get_event_pend_app_sts(T7103.C7100_Case_Info_Id) cstatusflag
	      			, T7103.C7103_Name ENAME, get_code_name(T7103.C901_Purpose) PUPEVENT
					, to_char(T7103.C7103_Start_Date,'''||v_date_fmt||':HH24:MI:SS'') EVESTARTDT, to_char(T7103.C7103_End_Date+ 2 / 24,'''||v_date_fmt||':HH24:MI:SS'') EVEENDDT
	      			, T7103.C7103_Start_Date STARTDT, T7103.C7103_End_Date ENDDT
	      			, Get_Code_Name(T7103.c901_loan_to_org) LOANTO
	      			, DECODE(t7103.c901_loan_to,''19518'', get_rep_name(t7103.c101_loan_to_id),get_user_name(t7103.c101_loan_to_id)) LOANTONAME
	      			, get_code_name(T7100.C901_Case_Status) CSTATUS
	      			, get_user_name (T7100.C7100_Created_By) CREATEDBY, to_char(T7100.C7100_Created_Date,'''||v_date_fmt||''') CREATEDDATE
					, get_case_attr_type(''EVEMET'', T7103.C7100_Case_Info_Id ) selectedcategories
			FROM T7103_Schedule_Info T7103, T7100_Case_Information T7100
			WHERE T7103.C7100_Case_Info_Id = T7100.C7100_Case_Info_Id
			
			AND T7100.C901_Case_Status NOT IN (''19526'') 
			AND T7100.C901_Type = ''1006504''
			AND T7100.c1900_company_id = '|| v_company_id ||'
			AND T7100.c1900_company_id = T7103.c1900_company_id
			AND T7100.C7100_Void_Fl IS NULL';       

			IF p_caseinfo_id <> 0 THEN
			    	v_query := v_query || ' AND T7103.C7100_Case_Info_Id ='  || p_caseinfo_id;
		    END IF;
		    
		    IF p_pur_of_event <> 0 THEN
			    	v_query := v_query || ' AND T7103.C901_Purpose ='  || p_pur_of_event;
		    END IF;
		    
		    IF p_loan_to <> 0 THEN
			    	v_query := v_query || ' AND T7103.c901_loan_to_org ='  || p_loan_to;
		    END IF;
		    
		    IF p_loan_to_name <> 0 THEN
			    	v_query := v_query || ' AND T7103.C101_Loan_To_Id = '''||p_loan_to_name||'''';
		    END IF;
		    
		    IF p_edtType = 'ESD' THEN
			    	v_query := v_query || ' AND Trunc (T7103.C7103_Start_Date) BETWEEN to_date('''||p_start_date||''' ,'''||v_date_fmt||''')'
			    					   || ' AND to_date( '''||p_end_date||''' ,'''||v_date_fmt||''')';
		    END IF;
		    
		    IF p_edtType = 'EED' THEN
			    	v_query := v_query || ' AND Trunc (T7103.C7103_End_Date) BETWEEN to_date('''||p_start_date||''' ,'''||v_date_fmt||''')'
			    					   || ' AND  to_date('''||p_end_date||''' ,'''||v_date_fmt||''')';
		    END IF;

		v_query := v_query || ' ORDER BY T7103.C7103_Name';
		
		OPEN p_out_event_list FOR v_query;
	  	 
	END gm_fch_event_list_rpt;
	
	/***************************************************************************************
    * Description	: Procedure to fetch the Status regarding the atleaset one request might
    * 				  approved or not for an Event
    * Author		: HReddi
    ****************************************************************************************/
	PROCEDURE  gm_fch_event_req_approved(
      	p_info_id          IN     t7100_case_information.c7100_case_info_id%TYPE
      , p_out_status       OUT    VARCHAR2
      )
     AS
        vcount                  NUMBER;
        v_out_status            VARCHAR2(10);
     BEGIN
            SELECT count(1)
              INTO vcount 
              FROM t7104_case_set_information 
             WHERE c7100_case_info_id = p_info_id
               AND c7104_void_fl is NULL;
            
            IF vcount > 0 
            THEN         
            	SELECT COUNT(1) 
            	  INTO vcount
                  FROM t7104_case_set_information t7104
                      ,t526_product_request_detail t526
                 WHERE t526.c526_product_request_detail_id = t7104.c526_product_request_detail_id                
	               AND t7104.c7100_case_info_id = p_info_id 	               
	               AND t526.c526_status_fl IN (10,20,30) -- open,Allocated, Closed
                   AND t526.c526_void_fl Is NULL 
                   AND t7104.c7104_void_fl IS NULL;
                   
                   IF vcount > 0 
                   THEN
                   		v_out_status := 'Y';
                   ELSE
                   		v_out_status := 'N';
                   END IF;
            ELSE
                  v_out_status := 'NR';
            END IF;
            
          p_out_status := v_out_status;
    END gm_fch_event_req_approved;

   /******************************************************************
    * Description	: Procedure to fetch Event Set Details
    * Author		: Elango
    ****************************************************************/	
    PROCEDURE gm_fch_event_set_details (
		p_case_info_id		IN  T7104_Case_Set_Information.c7100_case_info_id%TYPE,
        p_out_set_details 	OUT TYPES.cursor_type
	)
	AS
		v_case_type T7100_Case_Information.c901_type%TYPE;
	BEGIN
		v_case_type := get_type(p_case_info_id);
		OPEN p_out_set_details
		FOR			          
           SELECT ROWNUM NO
				  ,	t526.c526_product_request_detail_id reqdtlid
				  , t526.c207_set_id setid
				  , get_set_name (t526.c207_set_id) setnm
 				  , 1 reqqty
				  , gm_pkg_op_product_requests.get_req_loaner_etch_id (t526.c526_product_request_detail_id) loaneretchid
				  , DECODE(t907.c901_ship_to, '4121',Gm_pkg_cm_shipping_info.get_salesrepaddress(t907.c106_address_id,t907.c907_ship_to_id)
				                            , '4123',Gm_pkg_cm_shipping_info.get_empaddress(t907.c106_address_id,t907.c907_ship_to_id)
				                            , gm_pkg_cm_shipping_info.get_address(t907.c901_ship_to,t907.c907_ship_to_id)) shipaddress
                  , DECODE(t526.c526_void_fl,'Y', 'Void',get_loaner_request_status(t526.c526_product_request_detail_id)) status
				  , NVL(gm_pkg_op_product_requests.get_loanerreq_consignment_id (t526.c526_product_request_detail_id,NULL),t526.c526_product_request_detail_id) consgid
				  --sortorder: 1-Pending approval(5),2-Open(10),3-Rejected(40),4-Void,5-Allocated(20),6-Closed(30)
				  , DECODE(T526.C526_void_fl,NULL,DECODE(T526.C526_Status_Fl,5,1,10,2,40,3,20,5,30,6,7),4) sortorder
               FROM t526_product_request_detail t526,t907_shipping_info t907,
                  ( SELECT t526.C525_Product_Request_Id
                     FROM T526_Product_Request_Detail t526,T7104_Case_Set_Information t7104
                    WHERE t526.C526_Product_Request_Detail_Id = t7104.C526_Product_Request_Detail_Id
                      AND t526.C526_Product_Request_Detail_Id IS NOT NULL
                      AND t7104.c7100_case_info_id = p_case_info_id
                      --AND t7104.C7104_VOID_FL  IS NULL
                      --AND t526.C526_VOID_FL  IS NULL      
                      AND t526.c207_set_id IS NOT NULL
                      AND ROWNUM = 1)req
			  WHERE t526.c525_product_request_id    = req.C525_Product_Request_Id
                AND t907.c525_product_request_id 	= t526.c525_product_request_id 
                /* The following AND condition has been modified as, if the SET in PIP/RFP status those details are not displaying in the Event Summary Screen.
                 * for we are considering the status in between the 20[Allocated] and 30[Closed]*/
                AND t907.c907_ref_id 				= NVL(DECODE(t526.c526_status_fl
                                                               , '20', DECODE(gm_pkg_op_product_requests.get_set_status_id(t526.c526_product_request_detail_id)
                                                               , '7', TO_CHAR(t526.c526_product_request_detail_id),'58', TO_CHAR(t526.c526_product_request_detail_id)
                                                                               , gm_pkg_op_product_requests.get_loanerreq_consignment_id(t526.c526_product_request_detail_id, NULL))
                                                               , '30', gm_pkg_op_product_requests.get_loanerreq_consignment_id(t526.c526_product_request_detail_id, NULL)
                                                               , t526.c526_product_request_detail_id),t526.c526_product_request_detail_id)
             	AND t526.c901_request_type 			IN (4119,4127)--1006504-Event,4119-InHouse Loaner set, 4127-Product Loaner set
             	AND t526.c207_set_id                IS NOT NULL
             	ORDER BY sortorder;
			
	END gm_fch_event_set_details;
	
    /******************************************************************
    * Description	: Procedure to fetch Event Name
    * Author		: Elango
    ****************************************************************/	
	PROCEDURE gm_fch_event_item_details (
		p_case_info_id		IN  T7104_Case_Set_Information.c7100_case_info_id%TYPE,
        p_out_item_details 	OUT TYPES.cursor_type
	)
	AS
		v_prod_req_id T526_Product_Request_Detail.C525_Product_Request_Id%TYPE;		
	BEGIN
		v_prod_req_id := get_event_request_id(p_case_info_id);
        
		my_context.set_my_inlist_ctx (v_prod_req_id);
		
		OPEN p_out_item_details
		FOR
		SELECT rownum no, Details.*
		   FROM
		    (
		         SELECT v_prod_req_id Reqid, Pnum, Pdesc
		          , Transid, Reqqty, ' ' Opendtxn
		          , status, Shipaddress
		           FROM V525_Event_Items
		    )
		    Details;		  
	END gm_fch_event_item_details;
	
    /***************************************************************************
	* Description : Function to return the open txn for the request
	* Author      : Yoga
	***************************************************************************/
FUNCTION get_open_txn (
  p_ref_id      IN t412_inhouse_transactions.C412_Ref_Id%TYPE
  , p_partnum   IN t413_inhouse_trans_items.c205_part_number_id%TYPE 
) 
RETURN VARCHAR2
  IS
    CURSOR c_open_txn (v_txn_id VARCHAR2)
      IS
        SELECT t412.c412_inhouse_trans_id txn_id, t413.c413_item_qty qty, t412.c412_type type, t412.c412_void_fl void_fl
          FROM t412_inhouse_transactions t412, t413_inhouse_trans_items t413
         WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
            AND ((t412.c412_type = '1006483' AND t412.c412_status_fl <= 4) OR (t412.c412_status_fl < 4)) --"Verified"
            AND t413.c413_status_fl       IN ('R','Q')
            AND NVL(t412.c412_void_fl,-999) = DECODE(t413.c413_status_fl,'Q',NVL(t412.c412_void_fl,-999),
                                                                             DECODE(t413.c413_status_fl,'R',NVL(t412.c412_void_fl,-999),
                                                                             DECODE(t413.c413_status_fl,'V',NVL(t412.c412_void_fl,-999),-999)))
            AND t413.c205_part_number_id   = p_partnum
            AND T412.C412_Ref_Id           = TO_CHAR(v_txn_id)
	UNION ALL  -- Handle VDIH  below as no condition check needed
	SELECT t412.c412_inhouse_trans_id txn_id, t413.c413_item_qty qty, t412.c412_type type, t412.c412_void_fl void_fl
          FROM t412_inhouse_transactions t412, t413_inhouse_trans_items t413
         WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
           AND t413.c413_status_fl       IN ('V')
            AND NVL(t412.c412_void_fl,-999) = DECODE(t413.c413_status_fl,'Q',NVL(t412.c412_void_fl,-999),
                                                                             DECODE(t413.c413_status_fl,'R',NVL(t412.c412_void_fl,-999),
                                                                             DECODE(t413.c413_status_fl,'V',NVL(t412.c412_void_fl,-999),-999)))
            AND t413.c205_part_number_id   = p_partnum
            AND T412.C412_Ref_Id           = TO_CHAR(v_txn_id);
               
  v_txn_id          t412_inhouse_transactions.c412_inhouse_trans_id%TYPE;
  v_qqty            t413_inhouse_trans_items.c413_item_qty%TYPE;
  v_txn_temp        t412_inhouse_transactions.c412_inhouse_trans_id%TYPE;
  v_txn_void_fl     t412_inhouse_transactions.c412_void_fl%TYPE;
  v_txn_str         VARCHAR2 (4000) := '';
  v_child_txn_str   VARCHAR2 (4000) := '';
  v_child_ihrq_qty  NUMBER;
BEGIN
    v_txn_id := p_ref_id;

    FOR c_temp IN c_open_txn (v_txn_id)
    LOOP
      v_txn_temp := c_temp.txn_id;
      v_txn_void_fl:= c_temp.void_fl;

      -- If voided show IHRQ with 'V'
      IF  (c_temp.qty>0) THEN
        IF (v_txn_void_fl is not null) THEN -- If sum of IHRQ's child txn qty not equal to IHRQ Qty show the IHRQ in screen
          v_txn_str :=  v_txn_str || '<BR>' || v_txn_temp || '(' || c_temp.qty || ')' || '- V';
        ELSE
          v_txn_str :=  v_txn_str || '<BR>' || v_txn_temp || '(' || c_temp.qty || ')';
        END IF;
      END IF;
             
      IF ( c_temp.type = 1006483) THEN -- For IHRQ 
        SELECT  get_open_txn(v_txn_temp, p_partnum)  -- Call recursively for IHRQ's child records
          INTO v_child_txn_str from Dual;
        IF (v_child_txn_str IS NOT NULL) THEN 
          v_txn_str :=  v_txn_str || v_child_txn_str ; -- Show always child records(2nd level of Txns)
        END IF;
      END IF;
             
    END LOOP;
      RETURN v_txn_str;
END get_open_txn;
    
    /******************************************************************
    * Description	: Procedure to fetch Booked Item
    * Author		: Gopinathan
    ****************************************************************/	
	PROCEDURE gm_fch_booked_items(
		p_case_info_id		IN  T7104_Case_Set_Information.c7100_case_info_id%TYPE,
        p_out_item_details 	OUT TYPES.cursor_type
	)
		AS
	BEGIN
		OPEN p_out_item_details
		FOR
	  SELECT T7104.C205_Part_Number_Id pnum, Get_Partnum_Desc (T7104.C205_Part_Number_Id) pdesc, T7104.C7104_Item_Qty qty
			   FROM t7104_case_set_information t7104
			  WHERE t7104.c7100_case_info_id = p_case_info_id
			  	AND t7104.c901_system_type  = 19533
			    AND T7104.C7104_Void_Fl     IS NULL
			ORDER BY pdesc;
			
	END gm_fch_booked_items;
	
	/*********************************************************************
	* Description	: Procedure to fetch Event Set Rejected Report
	* Author		: Elango
	**********************************************************************/
    PROCEDURE  gm_fch_event_set_rejected_dtls(
            p_info_id          IN     VARCHAR2
        ,   p_type             IN     VARCHAR2
        ,   p_cancel_from      IN     VARCHAR2
        ,   p_out_status       OUT    TYPES.cursor_type
      )
      AS
      v_company_id  t1900_company.c1900_company_id%TYPE;
      BEGIN	  
         
	    my_context.set_my_inlist_ctx (p_info_id);
	    select get_compid_frm_cntx() into v_company_id from dual;
	    
	    IF p_cancel_from = 'OVERRIDE' THEN   	
	    	OPEN p_out_status 
	  		FOR  	
	  		 SELECT   t7104.c526_product_request_detail_id REQID
		        , NVL(T526.C207_SET_ID,T526.C205_PART_NUMBER_ID) SETID      
		        , DECODE( T526.C207_SET_ID, NULL, GET_PARTNUM_DESC(T526.C205_PART_NUMBER_ID), GET_SET_NAME(T526.C207_SET_ID))    SETNAME 
				, DECODE( T526.C207_SET_ID, NULL, 'P', 'S')   PARTSETTYPE 
		        , t526.C526_QTY_REQUESTED QTY      , t7100.c7100_case_id EVENTID    		, t7103.c7103_name EVENTNAME     
				, t526.C526_REQUIRED_DATE REQDATE  , t7100.c7100_created_date  createddt    , GET_USER_NAME (t7100.c7100_created_by) createdby
				, get_user_emailid(t7100.c7100_created_by) EMAILID
				, '' reason  
				, GET_USER_NAME(T526.C526_Last_Updated_By) Rejectedby
                ,  (SELECT NVL(t504al.c504_consignment_id,t526.c526_product_request_detail_id)		 
					  FROM t504a_consignment_loaner t504ac, t504a_loaner_transaction t504al
					 WHERE t504al.c526_product_request_detail_id = t526.c526_product_request_detail_id
					   AND t504al.c504_consignment_id = t504ac.c504_consignment_id
					   And T504ac.C504a_Status_Fl = T504ac.C504a_Status_Fl
					  -- AND t504al.C504a_void_fl IS NULL
					   And T504ac.C504a_Void_Fl Is Null
					   AND ROWNUM = 1) reqcnid
	       FROM t7100_case_information t7100 , t7104_case_set_information t7104, t7103_schedule_info t7103 , t526_product_request_detail t526 
	      WHERE t7100.c7100_case_info_id = t7103.c7100_case_info_id
	        AND t7100.c7100_case_info_id = t7104.c7100_case_info_id
	        And T7104.C526_Product_Request_Detail_Id = T526.C526_Product_Request_Detail_Id
	        AND t7104.c526_product_request_detail_id IN (
	        SELECT C526_Product_Request_Detail_Id FROM T504a_Loaner_Transaction WHERE c504a_loaner_transaction_id IN(
		        SELECT MAX (c504a_loaner_transaction_id)
	           	FROM T504a_Loaner_Transaction
	          	WHERE C504_Consignment_Id IN
	            (
	                 SELECT c504_consignment_id
	                   FROM t504a_loaner_transaction
	                  WHERE c526_product_request_detail_id IN (SELECT token FROM v_in_list)
	                    AND c504a_void_fl                 IS NULL
	                    AND c504a_return_dt               IS NULL
	            )
	            AND c504a_return_dt      IS NULL
	            AND c504a_void_fl        IS NOT NULL
	            AND c504a_is_replenished IS NULL
	       		GROUP BY c504_consignment_id
	        ))
	        AND t526.c526_status_fl =10   --open
	        AND t7100.c7100_void_fl IS NULL
	        AND t7104.c7104_void_fl IS NULL 
	        AND T7100.C901_Case_Status Not In (19526) 
	        AND t7103.c1900_company_id =v_company_id
            AND t7103.c1900_company_id  =t7100.c1900_company_id 
	        order by createdby,PARTSETTYPE DESC;    	
	   ELSE   	
	  		OPEN p_out_status 
	  		FOR 
			SELECT * FROM ( SELECT   t7104.c526_product_request_detail_id REQID
		        , NVL(T526.C207_SET_ID,T526.C205_PART_NUMBER_ID) SETID      
		        , DECODE( T526.C207_SET_ID, NULL, GET_PARTNUM_DESC(T526.C205_PART_NUMBER_ID), GET_SET_NAME(T526.C207_SET_ID))    SETNAME 
				, DECODE( T526.C207_SET_ID, NULL, 'P', 'S')   PARTSETTYPE 
		        , t526.C526_QTY_REQUESTED QTY      , t7100.c7100_case_id EVENTID    		, t7103.c7103_name EVENTNAME     
				, t526.C526_REQUIRED_DATE REQDATE  , t7100.c7100_created_date  createddt    , GET_USER_NAME (t7100.c7100_created_by) createdby
				, get_user_emailid(t7100.c7100_created_by) EMAILID
				, get_code_name(t907.c901_cancel_cd) reason  
				, get_user_name(t907.c907_created_by) REJECTEDBY
				, '' reqcnid
				, t526.c901_request_type REQTYPE
	       FROM t7100_case_information t7100 , t7104_case_set_information t7104, t7103_schedule_info t7103 , t526_product_request_detail t526 , t907_cancel_log t907
	      WHERE t7100.c7100_case_info_id = t7103.c7100_case_info_id
	        AND t7100.c7100_case_info_id = t7104.c7100_case_info_id
	        AND t7104.c526_product_request_detail_id = t526.c526_product_request_detail_id
	        AND t7104.c526_product_request_detail_id IN (SELECT token FROM v_in_list)
	        AND t526.c526_product_request_detail_id = t907.c907_ref_id
            AND t907.c901_type      = DECODE(p_type,'VDIHLN','1006650','1006620')
	        AND t526.c526_status_fl = DECODE(p_type,'VDIHLN',t526.c526_status_fl,'40' )   
	        AND t907.c907_created_by  = t526.c526_last_updated_by
	        AND t7100.c7100_void_fl IS NULL
	        AND t7104.c7104_void_fl IS NULL 
	        AND t7100.c901_case_status NOT IN (19526)
	        AND t7103.c1900_company_id =v_company_id
 			AND t7103.c1900_company_id  =t7100.c1900_company_id 
	        UNION
			SELECT t7104.c526_product_request_detail_id REQID ,
			  NVL(T526.C207_SET_ID,T526.C205_PART_NUMBER_ID) SETID ,
			  DECODE( T526.C207_SET_ID, NULL, GET_PARTNUM_DESC(T526.C205_PART_NUMBER_ID), GET_SET_NAME(T526.C207_SET_ID)) SETNAME ,
			  DECODE( T526.C207_SET_ID, NULL, 'P', 'S') PARTSETTYPE ,
			  t526.C526_QTY_REQUESTED QTY ,
			  t7100.c7100_case_id EVENTID ,
			  '' EVENTNAME ,
			  t526.C526_REQUIRED_DATE REQDATE ,
			  t7100.c7100_created_date createddt ,
			  GET_USER_NAME (t7100.c7100_created_by) createdby ,
			  get_user_emailid(t7100.c7100_created_by) EMAILID ,
			  get_code_name(t907.c901_cancel_cd) reason ,
			  get_user_name(t907.c907_created_by) REJECTEDBY ,
			  '' reqcnid ,
			  t526.c901_request_type REQTYPE
			FROM t7100_case_information t7100 ,
			  t7104_case_set_information t7104,
			  t526_product_request_detail t526 ,
			  t907_cancel_log t907
			WHERE t7100.c7100_case_info_id            = t7104.c7100_case_info_id
			AND t7104.c526_product_request_detail_id  = t526.c526_product_request_detail_id
			AND t7104.c526_product_request_detail_id IN
			  (SELECT token FROM v_in_list
			  )
			AND t526.c526_product_request_detail_id = t907.c907_ref_id
			AND t7100.c1900_company_id =v_company_id
			AND t907.c907_created_by  = t526.c526_last_updated_by
			AND t907.c901_type                     IN (11350,1006525,1006520)
			AND ( t526.c526_status_fl   = '40'	OR t526.c526_status_fl  = '7' )
			AND t7100.c7100_void_fl                IS NULL
			AND t7104.c7104_void_fl                IS NULL
			AND T7100.C901_TYPE                    IN (1006505,1006506,1006507,107286) --107286 Account Item Consignment
			AND t7100.c901_case_status NOT         IN (19526) ) 
			 
	        order by createdby,PARTSETTYPE DESC;
	    END IF;
    END gm_fch_event_set_rejected_dtls;
	
    /***************************************************************************************
    * Description	: Procedure to fetch the Status regarding set is shipped out
    * 				 
    * Author		: Jignesh Shah
    ****************************************************************************************/
	PROCEDURE  gm_fch_event_shipout_info(
      	p_info_id          IN     t7100_case_information.c7100_case_info_id%TYPE
      , p_out_status       OUT    VARCHAR2
      )
     AS
        vcount                  NUMBER;
        v_out_status            VARCHAR2(10);
     BEGIN
            SELECT Count(1)INTO vcount from T7104_Case_Set_Information where c7100_case_info_id = p_info_id; 
            IF vcount > 0 THEN                  
	            SELECT CASE WHEN COUNT(*) > 0 THEN 'Y' ELSE 'N' END  INTO   v_out_status                 
	                    From T7104_Case_Set_Information T7104,T526_Product_Request_Detail T526
	                  Where T7104.C7100_Case_Info_Id = p_info_id 
	                     And T7104.C7104_Void_Fl IS NULL 
	                     And T526.C526_Product_Request_Detail_Id = T7104.C526_Product_Request_Detail_Id 
	                     And T526.C526_Void_Fl Is Null 
	                     AND t526.c526_status_fl IN (30) ;             
            ELSE
                  v_out_status := 'NR';
            END IF;
          p_out_status := v_out_status;
    END gm_fch_event_shipout_info;
    
     /***************************************************************************
	* Description : Function to return the t525_request for type
	* Author    : Elango
	***************************************************************************/
   
   FUNCTION get_request_type (
  	  p_product_request_id  IN   t525_product_request.c525_product_request_id%TYPE
   ) 
   RETURN VARCHAR2
IS
	v_request_type	   VARCHAR2 (20);
BEGIN
	BEGIN
		    SELECT C901_REQUEST_FOR
			  INTO v_request_type
			  FROM t525_product_request 
			 WHERE c525_product_request_id =  p_product_request_id
               AND c525_void_fl IS NULL ;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN '';
	END;

	RETURN v_request_type;
END get_request_type;

  /***************************************************************************
	* Description : Function to return the t525_request for type
	* Author    : Elango
	***************************************************************************/
   
   FUNCTION get_case_request_id (
  	  p_case_info_id  IN   t7104_case_set_information.C7100_CASE_INFO_ID%TYPE
   ) 
   RETURN VARCHAR2
IS
	v_request_id	   VARCHAR2 (20);
BEGIN
	BEGIN
		   
            SELECT t526.c525_product_request_id
			  INTO v_request_id
			  FROM t526_product_request_detail t526, t7104_case_set_information t7104
			 WHERE t7104.c526_product_request_detail_id = t526.c526_product_request_detail_id
               AND t7104.c7100_case_info_id =  p_case_info_id
               AND t7104.c7104_void_fl IS NULL
               AND t526.c526_void_fl IS NULL 
               AND ROWNUM=1;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN '';
	END;

	RETURN v_request_id;
	
END get_case_request_id;

 /***************************************************************************
	* Description : Function to return the t525_request id for event
	* Author    : rshah
	***************************************************************************/
   
 FUNCTION get_event_request_id (
  	  p_case_info_id  IN   t7104_case_set_information.C7100_CASE_INFO_ID%TYPE
   ) 
   RETURN VARCHAR2
IS
	v_request_id	   VARCHAR2 (20);
BEGIN
	BEGIN
		   
            SELECT t526.c525_product_request_id
			  INTO v_request_id
			  FROM t526_product_request_detail t526, t7104_case_set_information t7104
			 WHERE t7104.c526_product_request_detail_id = t526.c526_product_request_detail_id
               AND t7104.c7100_case_info_id =  p_case_info_id
              -- AND t7104.c7104_void_fl IS NULL
               AND ROWNUM=1;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN '';
	END;

	RETURN v_request_id;
	
END get_event_request_id;


 /***************************************************************************
	* Description : Function to return the consignment type
	* Author    : Elango
	***************************************************************************/
   
    FUNCTION get_consignment_type (
            p_consignment_id t504_consignment.c504_consignment_id%TYPE
    )
        RETURN VARCHAR2
    IS
        v_type VARCHAR2 (50);
    BEGIN
        BEGIN
            SELECT c504_type
            INTO v_type
            FROM t504_consignment
            WHERE c504_consignment_id = p_consignment_id;
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            RETURN '';
        END;
        RETURN v_type;
    END get_consignment_type;
    
    /******************************************************************
    * Description	: Procedure to fetch shipping Information
    * Author		: Jignesh Shah
    ****************************************************************/
	PROCEDURE gm_fch_event_shipment_info (
		p_shipinfo	 OUT   TYPES.cursor_type
	)
	AS
	v_company_id   t1900_company.c1900_company_id%TYPE;
	BEGIN
	     SELECT get_compid_frm_cntx()
            INTO v_company_id
            FROM DUAL;
		OPEN p_shipinfo
		FOR
			 SELECT t7104.c7100_case_info_id case_info_id
             	  , get_user_emailid(t7100.c7100_created_by)  email 
			   FROM t907_shipping_info t907
            	  , t526_product_request_detail t526
                  , t7104_case_set_information t7104
                  , t7100_case_information t7100
                  , t504a_loaner_transaction t504a
			  WHERE t907.c525_product_request_id = t526.c525_product_request_id
			    AND t526.c526_product_request_detail_id  = t7104.c526_product_request_detail_id
			    AND t504a.c526_product_request_detail_id = t526.c526_product_request_detail_id
			    AND t7104.c7100_case_info_id = t7100.c7100_case_info_id
          		AND t504a.c504_consignment_id = t907.c907_ref_id
  				AND t7100.c901_type = 1006504 -- for type = 'event'
          		AND t907.c901_source = 50182 -- loaners
          		AND t907.c907_status_fl = 40 -- closed
    			AND t7100.c7100_void_fl IS NULL
			    AND t504a.c504a_void_fl IS NULL
			    AND t907.c907_email_update_fl IS NULL
			    AND t907.c525_product_request_id IS NOT NULL
			    AND t907.c907_tracking_number IS NOT NULL
			    AND t907.c907_void_fl IS NULL
			    AND t7104.c7104_void_fl IS NULL
			    AND t526.c526_void_fl IS NULL
			    AND t907.c1900_company_id  = v_company_id
			    AND t907.c1900_company_id = t526.c1900_company_id 
			    AND t907.c1900_company_id = t7100.c1900_company_id
			    AND t907.c1900_company_id = t504a.c1900_company_id
           GROUP BY t7104.c7100_case_info_id, t7100.c7100_created_by
              UNION
  			 SELECT t7104.c7100_case_info_id case_info_id
             	  , get_user_emailid(t7100.c7100_created_by)  email 
			   FROM t907_shipping_info t907
            	  , t526_product_request_detail t526
                  , t7104_case_set_information t7104
                  , t7100_case_information t7100
                  , t504_consignment t504
			  WHERE t907.c525_product_request_id = t526.c525_product_request_id
			    AND t526.c526_product_request_detail_id  = t7104.c526_product_request_detail_id
			    AND t504.c504_ref_id = t526.c525_product_request_id
			    AND t7104.c7100_case_info_id = t7100.c7100_case_info_id
          		AND t504.c504_consignment_id = t907.c907_ref_id
  				AND t7100.c901_type = 1006504 -- for type = 'event'
          		AND t907.c901_source = 50186 -- loaner items
          		AND t907.c907_status_fl = 40 -- closed
    			AND t7100.c7100_void_fl IS NULL
			    AND t504.c504_void_fl IS NULL
			    AND t907.c907_email_update_fl IS NULL
			    AND t526.c205_part_number_id IS NOT NULL
			    AND t907.c525_product_request_id IS NOT NULL
			    AND t907.c907_tracking_number IS NOT NULL
			    AND t907.c907_void_fl IS NULL
			    AND t7104.c7104_void_fl IS NULL
			    AND t526.c526_void_fl IS NULL
			    AND t907.c1900_company_id  = v_company_id
			    AND t907.c1900_company_id = t526.c1900_company_id 
			    AND t907.c1900_company_id = t7100.c1900_company_id
			    AND t907.c1900_company_id = t504.c1900_company_id
           GROUP BY T7104.c7100_case_info_id, t7100.c7100_created_by;
			
	END gm_fch_event_shipment_info;
	
	/******************************************************************
    * Description	: Procedure to fetch shipped sets Information
    * Author		: Jignesh Shah
    ****************************************************************/
	PROCEDURE gm_fch_event_shipped_sets (
		p_info_id          	IN     t7100_case_information.c7100_case_info_id%TYPE,
		p_flag              IN     VARCHAR2,
		p_shipped_set_info	OUT   TYPES.cursor_type
	)
	AS
		v_query	 		VARCHAR2(4000);
		v_condition 	VARCHAR2(500):='';
		v_day_cnt       NUMBER;
		v_company_id   t1900_company.c1900_company_id%TYPE;
	BEGIN
		v_day_cnt := TO_NUMBER(get_rule_value ('DAYCOUNT', 'EVENTMISSINGEMAIL'));
		SELECT get_compid_frm_cntx()
            INTO v_company_id
            FROM DUAL;
		OPEN p_shipped_set_info
		FOR
			SELECT T526.C525_Product_Request_Id Tranid 
                  , get_code_name(T526.C901_Request_Type) reqtype 
                  , t526.c526_product_request_detail_id reqdtlid 
			      , NVL(DECODE (t526.c526_status_fl,'20', gm_pkg_op_product_requests.get_loanerreq_consignment_id(t526.c526_product_request_detail_id,'10'), NULL),'N') reqcnid 
			      , t526.c207_set_id setid 
			      , get_set_name (t526.c207_set_id) setnm 
			      , T526.C526_Qty_Requested qty 
			      , t526.c526_request_flag reqfl 
			      , gm_pkg_op_product_requests.get_req_loaner_etch_id (t526.c526_product_request_detail_id) etchid 
			      , t526.c526_status_fl statusid 
			      , t907.c907_ship_to_dt shipdate 
			      , get_code_name(t907.c901_delivery_carrier) delcarrier 
			      , DECODE(t907.c901_ship_to,'4121',Gm_pkg_cm_shipping_info.get_salesrepaddress(t907.c106_address_id,t907.c907_ship_to_id),gm_pkg_cm_shipping_info.get_address(t907.c901_ship_to,t907.c907_ship_to_id)) shipaddress 
                  , t907.c907_tracking_number trackingno 
                  , Get_Loaner_Request_Status(T526.C526_Product_Request_Detail_Id) Status 
      			  , Gm_Pkg_Op_Product_Requests.Get_Loanerreq_Consignment_Id (T526.C526_Product_Request_Detail_Id,Null) Consgid 
                  , t504a.c504a_expected_return_dt duedate                  
            FROM T526_Product_Request_Detail T526,T907_Shipping_Info T907,T7104_Case_Set_Information T7104,t504a_loaner_transaction t504a 
     		WHERE T7104.C7100_Case_Info_Id = p_info_id 
            AND T7104.C7104_Void_Fl IS NULL 
            AND  T526.C526_Product_Request_Detail_Id = T7104.C526_Product_Request_Detail_Id 
       		AND t526.c526_void_fl      IS NULL 
    		AND T907.C907_Void_Fl      IS NULL 
            AND T907.C907_Status_Fl IN (40) 
            AND DECODE(p_flag,'Y',NVL(t907.c907_email_update_fl,'N'),NVL(t907.c907_email_update_fl,'N'))= DECODE(p_flag,'Y','N',NVL(t907.c907_email_update_fl,'N'))  
            AND T907.C907_Tracking_Number IS NOT NULL 
            AND T907.C525_Product_Request_Id  = T526.C525_Product_Request_Id 
            AND T907.C907_Ref_Id     = NVL(Decode (T526.C526_Status_Fl, '20', Gm_Pkg_Op_Product_Requests.Get_Loanerreq_Consignment_Id(T526.C526_Product_Request_Detail_Id, '10'), 
                '30',Gm_Pkg_Op_Product_Requests.Get_Loanerreq_Consignment_Id(T526.C526_Product_Request_Detail_Id, Null), 
                T526.C526_Product_Request_Detail_Id),T526.C526_Product_Request_Detail_Id) 
            AND DECODE(p_flag,'X',(TRUNC (t504a.c504a_expected_return_dt) + v_day_cnt),CURRENT_DATE) = DECODE(p_flag,'X',TRUNC (CURRENT_DATE),CURRENT_DATE) 
            AND t504a.c504a_return_dt IS NULL 
            AND t504a.c504a_void_fl IS NULL 
		    AND t504a.c526_product_request_detail_id = t526.c526_product_request_detail_id
		    AND T907.c1900_company_id  = v_company_id
		    AND T907.c1900_company_id  = T526.c1900_company_id 
			AND T907.c1900_company_id = t504a.c1900_company_id 
            ORDER BY Trackingno; 
            
	END gm_fch_event_shipped_sets;
	
	/******************************************************************
    * Description	: Procedure to fetch shipped Items Information
    * Author		: Elango
    ****************************************************************/
	PROCEDURE gm_fch_event_shipped_items (
		p_info_id          	IN     t7100_case_information.c7100_case_info_id%TYPE,
		p_shipped_item_info	OUT   TYPES.cursor_type
	)
	AS
	v_company_id   t1900_company.c1900_company_id%TYPE;
	BEGIN
		SELECT get_compid_frm_cntx()
            INTO v_company_id
            FROM DUAL;
		OPEN p_shipped_item_info
		FOR
		  SELECT Request_dtl.c205_part_number_id pnum,pdesc pdesc,SUM (t505.c505_item_qty) reqqty, Request_dtl.duedate duedate,Tranid Tranid,reqtype reqtype,trackingno trackingno,cn_ship_dtl.shipaddress  shipaddress,delcarrier
		   FROM t505_item_consignment t505, (
		         SELECT T526.C525_Product_Request_Id Tranid, get_code_name (T526.C901_Request_Type) reqtype,
		            t526.c526_product_request_detail_id reqdtlid, t526.c205_part_number_id, t526.c526_qty_requested reqqty
		          , Get_Partnum_Desc (T526.C205_Part_Number_Id) Pdesc, T526.C526_Request_Flag Reqfl,
		            Get_Loaner_Request_Status (T526.C526_Product_Request_Detail_Id) Status, T526.C526_Exp_Return_Date duedate
		           FROM T526_Product_Request_Detail T526, t7104_case_set_information t7104, t7100_case_information t7100
		          WHERE T526.C526_Product_Request_Detail_Id = T7104.C526_Product_Request_Detail_Id
		            AND t7100.C7100_Case_Info_Id            = p_info_id 
		            AND T7100.C7100_Case_Info_Id            = T7104.C7100_Case_Info_Id
		            AND t7100.c901_type = '1006504' -- Event type
		            AND T7104.C7104_Void_Fl IS NULL
		            AND T526.C526_Void_Fl   IS NULL
		            AND t526.c526_status_fl != '40'
		            AND T526.c1900_company_id  = v_company_id
		            AND T526.c1900_company_id = t7100.c1900_company_id 
		    )
		    Request_dtl, (
		         SELECT t504.c504_consignment_id cnid, t504.c504_ref_id refid, t907.c907_ship_to_dt shipdate
		          , get_code_name (t907.c901_delivery_carrier) delcarrier, DECODE (t907.c901_ship_to, '4121',
		            Gm_pkg_cm_shipping_info.get_salesrepaddress (t907.c106_address_id, t907.c907_ship_to_id),
		            gm_pkg_cm_shipping_info.get_address (t907.c901_ship_to, t907.c907_ship_to_id)) shipaddress,
		            t907.c907_tracking_number trackingno
		           FROM T907_Shipping_Info T907, t504_consignment t504
		          WHERE T907.C907_Void_Fl         IS NULL
		            AND t907.c901_source           = '50186' -- Inhouse Loaner
		            AND T907.C907_Status_Fl       IN (40)
		            AND T907.C907_Email_Update_Fl IS NULL
		            AND T907.C907_Tracking_Number IS NOT NULL
		            AND T907.c907_ref_id           = t504.c504_consignment_id
		            AND t504.c504_status_fl        = '4'
		            AND T504.C504_Void_Fl         IS NULL
		            AND t504.c504_type             = '9110'-- IHLN
		            AND T907.c1900_company_id  = v_company_id
					AND T907.c1900_company_id  = t504.c1900_company_id
		    )
		    cn_ship_dtl
		  WHERE t505.c205_part_number_id(+) = Request_dtl.c205_part_number_id
		    AND T505.C504_Consignment_Id    = Cn_Ship_Dtl.Cnid
		    AND Cn_Ship_Dtl.Refid           = Request_Dtl.Tranid
 	   GROUP BY Request_Dtl.C205_Part_Number_Id,Pdesc, Request_Dtl.Duedate,Tranid,Reqtype,Trackingno,Cn_Ship_Dtl.Shipaddress,Delcarrier
       ORDER BY Trackingno;		    
	END gm_fch_event_shipped_items;
	
	/******************************************************************
    * Description	: Procedure to Update shipped sets Information
    * Author		: Jignesh Shah
    ****************************************************************/
	PROCEDURE gm_sav_evnt_shpd_email_sts (
		p_info_id    IN     t7100_case_information.c7100_case_info_id%TYPE
	)
	AS
	BEGIN
			UPDATE t907_shipping_info 
     		SET c907_email_update_fl = 'Y' 
    			, c907_email_update_dt = TRUNC (CURRENT_DATE) 
   			WHERE c907_shipping_id IN 
   				( SELECT T907.C907_Shipping_Id                  
                 	FROM T526_Product_Request_Detail T526, T907_Shipping_Info T907, T7104_Case_Set_Information T7104 
     				WHERE T7104.C7100_Case_Info_Id = p_info_id 
                	AND T7104.C7104_Void_Fl IS NULL 
                	AND T526.C526_Product_Request_Detail_Id = T7104.C526_Product_Request_Detail_Id 
       				AND t526.c526_void_fl  IS NULL 
    				AND T907.C907_Void_Fl  IS NULL 
	                AND T907.C907_Status_Fl In (40) 
	                AND T907.C907_Email_Update_Fl IS NULL 
	                AND T907.C907_Tracking_Number IS NOT NULL 
	                AND T907.C525_Product_Request_Id  = T526.C525_Product_Request_Id 
	                AND T907.C907_Ref_Id  = Nvl(Decode (T526.C526_Status_Fl, '20', Gm_Pkg_Op_Product_Requests.Get_Loanerreq_Consignment_Id(T526.C526_Product_Request_Detail_Id, '10'), 
		                '30',Gm_Pkg_Op_Product_Requests.Get_Loanerreq_Consignment_Id(T526.C526_Product_Request_Detail_Id, Null), 
		                T526.C526_Product_Request_Detail_Id),T526.C526_Product_Request_Detail_Id) ) 
				    AND c907_email_update_fl IS NULL 
				    AND C907_Active_Fl IS NULl 
				    AND C907_Status_Fl = 40; 
			
	END gm_sav_evnt_shpd_email_sts;
	
	/************************************************************************
    * Description	: Procedure to fetch Event Information based on End Date
    * Author		: Jignesh Shah
    *************************************************************************/
	PROCEDURE gm_fch_event_end_list (
		p_end_eventinfo	 OUT   TYPES.cursor_type
	)
	AS
	v_company_id  t1900_company.c1900_company_id%TYPE;
	v_date_fmt VARCHAR2(100);
	BEGIN
		
	SELECT NVL(get_compid_frm_cntx(),GET_RULE_VALUE('DEFAULT_COMPANY','ONEPORTAL')),
	       NVL(get_compdtfmt_frm_cntx(),Get_Rule_Value ('DATEFMT', 'DATEFORMAT')) 
	  INTO v_company_id,v_date_fmt FROM DUAL;
	  
		OPEN p_end_eventinfo
		FOR
			SELECT T7100.C7100_Case_Id Eventid
				 , T7103.C7100_Case_Info_Id Case_Info_Id
				 , T7103.C7103_Name Eventname
     			 , get_code_name(t7103.c901_purpose) purpose
     			 , TO_CHAR(t7103.c7103_start_date , v_date_fmt)eventStartDate
     			 , TO_CHAR(T7103.C7103_End_Date, v_date_fmt) Eventenddate
     			 , T7103.C7103_Venue Eventvenue
     			 , Get_Code_Name(T7103.c901_loan_to_org) Loantotype
     			 , DECODE( t7103.c901_loan_to,'19518', get_rep_name(t7103.c101_loan_to_id), get_user_name(t7103.c101_loan_to_id)) Loantoname
           		 , DECODE(t7103.c901_loan_to,'19518',get_rep_emailid(t7103.c101_loan_to_id),Get_User_Emailid(t7103.c101_loan_to_id)) LOANTOEMAILID
     			 , Get_Code_Name(T7100.C901_Case_Status) Status  
     			 , GET_USER_NAME(t7100.c7100_created_by) createdby
  			FROM t7103_schedule_info t7103, t7100_case_information t7100 
  			WHERE T7100.C7100_Case_Info_Id = T7103.C7100_Case_Info_Id 
           	AND trunc(T7103.C7103_End_Date) = TRUNC(CURRENT_DATE)
    		AND t7100.c7100_void_fl 	    IS NULL 
    		AND t7103.c1900_company_id =v_company_id
  			AND t7103.c1900_company_id  =t7100.c1900_company_id ;
			
	END gm_fch_event_end_list;
	
	/*******************************************************
	* Description : Procedure to fetch details based of caseinfoid
	* Author	  : Jignesh Shah
	*******************************************************/
	PROCEDURE gm_fch_req_details (
		p_caseinfo_id	IN	 t7100_case_information.c7100_case_info_id%TYPE
	  , p_caseinfo      OUT      TYPES.cursor_type	
	)
	AS
	v_date_fmt VARCHAR2(100);
	v_company_id    t1900_company.c1900_company_id%TYPE;
	BEGIN		
	 --Getting company id from context.   
    SELECT get_compid_frm_cntx()
	  INTO v_company_id 
	  FROM dual;
	  --
		select get_compdtfmt_frm_cntx() into v_date_fmt from dual;
		OPEN p_caseinfo
       	FOR
       		SELECT ('Request No. :- ' || T525.C525_Product_Request_Id) Reqid
       		      , T525.C525_Product_Request_Id RequestId
			      , NVL(T526.C207_SET_ID,T526.C205_PART_NUMBER_ID) Set_Id
                  , DECODE( T526.C207_SET_ID, NULL, GET_PARTNUM_DESC(T526.C205_PART_NUMBER_ID), GET_SET_NAME (T526.C207_SET_ID)) Set_Name 
                  , t526.C526_QTY_REQUESTED QTY
			      , Decode (Nvl (T526.C526_Status_Fl, 10), 10, 'Unavailable', 20, 'Available', 30,'Shipped', 40, 'Cancelled') Status
			      , To_Char (T526.C526_Required_Date, v_date_fmt) Psdate
			      , T525.C703_Sales_Rep_Id Rep_Id
			      , Get_Rep_Name (T525.C703_Sales_Rep_Id) Rep_Name
			      , NVL (Get_Cs_Ship_Email (4121, T525.C703_Sales_Rep_Id), get_rule_value_by_company ('GMCSTOMAILID', 'SHIPDTL', v_company_id)) Email
			      , t7104.c7100_case_info_id, DECODE(t7104.c207_set_id,NULL,1,0) seq
			FROM t525_product_request t525, t526_product_request_detail t526, t7104_case_set_information t7104
			WHERE t525.c525_product_request_id        = t526.c525_product_request_id
			AND t526.c526_product_request_detail_id = t7104.c526_product_request_detail_id
			AND TRUNC (t525.c525_requested_date)    = TRUNC (CURRENT_DATE)
			AND t525.c525_void_fl                  IS NULL
			AND T526.C526_Void_Fl                  Is NULL 
			AND t7104.c7104_void_fl IS NULL
			AND t7104.c7100_case_info_id            = p_caseinfo_id
			ORDER BY seq asc,status desc;
		
	END gm_fch_req_details;
	
	 /***************************************************************************************
    * Description	: FUNCTION to fetch the edit Status of event
    * 				 
    * Author		: APrasath
    ****************************************************************************************/
	FUNCTION  get_event_edit_status(
      	p_info_id          IN     t7100_case_information.c7100_case_info_id%TYPE    
    )
    RETURN VARCHAR2
     IS
        vstatus  VARCHAR2(1);        
     BEGIN	     	
	       SELECT (CASE WHEN count(*)>0 THEN 'Y' ELSE 'N' END) INTO vstatus
			FROM  t526_product_request_detail t526, t7104_case_set_information t7104
			WHERE  t526.c526_product_request_detail_id = t7104.c526_product_request_detail_id
			AND t7104.c7104_void_fl IS NULL
			AND t526.c526_void_fl                  IS NULL 
            AND t526.c526_required_date IS NOT NULL 
			AND t7104.c7100_case_info_id            = p_info_id;			
        RETURN vstatus;
    END get_event_edit_status;
     /***************************************************************************************
    * Description	: FUNCTION to fetch the edit Status of event
    * 				 
    * Author		: APrasath
    ****************************************************************************************/
	FUNCTION  get_event_resource_add_status(
      	p_info_id          IN     t7100_case_information.c7100_case_info_id%TYPE    
    )
    RETURN VARCHAR2
     IS
        vstatus  VARCHAR2(1);      
     BEGIN	     	
	         	SELECT (CASE WHEN count(*)>0 THEN 'Y' ELSE 'N' END) INTO vstatus
				FROM  t526_product_request_detail t526, t7104_case_set_information t7104
				WHERE  t526.c526_product_request_detail_id = t7104.c526_product_request_detail_id
				AND t7104.c7104_void_fl IS NULL
				AND t526.c526_void_fl                  IS NULL 
	            AND t526.c526_required_date IS NULL 
				AND t7104.c7100_case_info_id            = p_info_id;            
       RETURN vstatus;
    END get_event_resource_add_status;
      /************************************************************************
    * Description	: Procedure to fetch missing part Event Information 
    * Author		: AROCKIA PRASATH
    *************************************************************************/
	PROCEDURE gm_fch_missing_part_event_list (
		p_missing_part_eventinfo	 OUT   TYPES.cursor_type
	)
	AS
	v_day_cnt               NUMBER;
	v_company_id  t1900_company.c1900_company_id%TYPE;
	BEGIN
		v_day_cnt := TO_NUMBER(get_rule_value ('DAYCOUNT', 'EVENTMISSINGEMAIL'));
		select get_compid_frm_cntx() into v_company_id from dual;
		OPEN p_missing_part_eventinfo
		FOR
			 SELECT t7100.c7100_case_info_id, Reqdetail.ProdReqId, t7100.c7100_case_id eventid
			  , t7103.c7103_name eventname , get_user_name (t7100.c7100_created_by) createdby, t7100.c7100_created_by createduser, Get_User_Emailid (
			    T7100.C7100_Created_By) Canuseremail, DECODE (T7103.C901_Loan_To, '19518', Get_Rep_Emailid (T7103.C101_Loan_To_Id),
			    Get_User_Emailid (T7103.C101_Loan_To_Id)) Loantoemailid
			   FROM T7103_Schedule_Info T7103, T7100_Case_Information T7100, (
			        SELECT DISTINCT TO_CHAR (T526.C525_Product_Request_Id) ProdReqId, TO_CHAR (T7104.C7100_Case_Info_Id) Caseinfoid
			           FROM t504a_loaner_transaction t504a, t526_product_request_detail t526, t525_product_request t525
			          , T7104_Case_Set_Information t7104
			          WHERE T504a.C504a_Void_Fl                 IS NULL
			            AND TRUNC (t504a.c504a_return_dt) + v_day_cnt  = TRUNC (CURRENT_DATE)     
			            AND t504a.c526_product_request_detail_id = t526.c526_product_request_detail_id
			            AND T526.C525_Product_Request_Id         = T525.C525_Product_Request_Id
			            AND T525.C901_Request_For                = 4119
			            AND T7104.C526_Product_Request_Detail_Id = T526.C526_Product_Request_Detail_Id
			            AND T7104.C7104_Void_Fl                 IS NULL
			            AND T525.C525_Void_Fl                   IS NULL
			            AND T526.C526_Void_Fl                   IS NULL
			            AND T7104.C7100_Case_Info_Id            IS NOT NULL
			          UNION
			        SELECT DISTINCT TO_CHAR (T504.C504_Ref_Id) ProdReqId, TO_CHAR (Get_Case_Info_Id (T504.C504_Ref_Id)) caseinfoid
			           FROM t504_consignment t504, T506_Returns t506, t412_inhouse_transactions t412
			          WHERE T504.C901_Ref_Type    = 4119
			            AND T506.C506_Ref_Id      = T504.C504_Consignment_Id
			            AND TRUNC (T506.C506_Return_Date) + v_day_cnt = TRUNC (CURRENT_DATE)     
			            AND T506.C901_Ref_Type    = 9110
			            AND t412.c412_ref_id      = T506.C506_Rma_Id
			            AND T504.C504_Void_Fl    IS NULL
			            AND T506.C506_Void_Fl    IS NULL
			            AND T412.C412_Void_Fl    IS NULL
			    )
			    Reqdetail
			  WHERE reqdetail.caseinfoid     = t7103.c7100_case_info_id
			    AND t7100.c7100_case_info_id = t7103.c7100_case_info_id
			    AND t7100.c7100_case_info_id = t7103.c7100_case_info_id
			    AND T7100.C7100_Void_Fl     IS NULL
			    AND T7100.C901_Type          = 1006504
			    AND t7103.c1900_company_id =v_company_id
            	AND t7103.c1900_company_id  =t7100.c1900_company_id ;
			
	END gm_fch_missing_part_event_list;

	/******************************************************************
    * Description	: Procedure to fetch missing part Event Information 
    * Author		: AROCKIA PRASATH
    ****************************************************************/
	PROCEDURE gm_fch_missing_part_event_sets (
		p_req_id          	IN     t525_product_request.c525_product_request_id%TYPE,
		p_miss_part_set_info	OUT   TYPES.cursor_type
	)
	AS	
		v_tran_id VARCHAR2(20);
	BEGIN
		 
		/*FOR v_return_cur IN (
		SELECT T506.C506_Rma_Id RAID, T412.C412_Inhouse_Trans_Id TRANSID, T506.C506_type RATYPE
		   FROM T504_Consignment T504, T506_Returns T506, T412_Inhouse_Transactions t412
		  WHERE t506.c506_status_fl <= 2
		    AND T506.C901_Ref_Type   = '9110'
		    AND T506.C506_Ref_Id     = T504.C504_Consignment_Id
		    AND T506.C506_Void_Fl   IS NULL
		    AND T412.C412_Ref_Id(+)  = T506.C506_Rma_Id
		    AND T504.C504_Ref_Id     = p_req_id
		    AND T504.C504_Void_Fl   IS NULL
		    )
    		LOOP
	       		BEGIN 
		       		IF v_return_cur.TRANSID IS NULL THEN
		       			gm_pkg_op_ihloaner_item_txn.gm_sav_inih_missing_item(v_return_cur.RAID,v_return_cur.RATYPE,'706328',v_tran_id);
		       		END IF;
		       	END;
    		END LOOP;*/
		OPEN p_miss_part_set_info
		FOR
			 SELECT t504.c207_set_id SETID, get_set_name (t504.c207_set_id) SETNM, t504al.c504a_etch_id ETCHID
			  , t504a.c504a_return_dt RETURNDT, t413.c205_part_number_id PNUM, get_partnum_desc (t413.c205_part_number_id) PDESC
			  , t413.c413_item_price PRICE, SUM (DECODE (t413.c413_status_fl, 'Q', t413.c413_item_qty, (t413.c413_item_qty * - 1)))  QTY 
			  , T412.C412_Expected_Return_Date expreturndate 
			  FROM t525_product_request t525, t526_product_request_detail t526, t504a_loaner_transaction t504a
			  , t412_inhouse_transactions t412, t413_inhouse_trans_items t413, t504a_consignment_loaner t504al
			  , T504_Consignment T504
			  WHERE t525.c525_product_request_id       IN (p_req_id)
			    AND t525.c525_product_request_id        = t526.c525_product_request_id
			    AND t526.c526_void_fl                  IS NULL
			    AND t526.c526_product_request_detail_id = t504a.c526_product_request_detail_id
			    AND t504a.c504a_loaner_transaction_id   = t412.c504a_loaner_transaction_id
			    AND t412.c412_void_fl                  IS NULL
			    AND t412.c412_type                      = 1006571 -- Missing Parts
			    AND t412.c412_inhouse_trans_id          = t413.c412_inhouse_trans_id
			    AND t504a.c504_consignment_id           = t504al.c504_consignment_id
			    AND t504a.c504_consignment_id           = t504.c504_consignment_id
			GROUP BY t504a.c504_consignment_id, t504.c207_set_id, t504al.c504a_etch_id
			  , t504a.c504a_return_dt, T413.C205_Part_Number_Id, T413.C413_Item_Price,T412.C412_Expected_Return_Date;
            
	END gm_fch_missing_part_event_sets;
	
	/******************************************************************
    * Description	: Procedure to fetch missing part Event Information 
    * Author		: AROCKIA PRASATH
    ****************************************************************/
	PROCEDURE gm_fch_event_missing_parts (
		p_req_id          	IN     t525_product_request.c525_product_request_id%TYPE,
		p_miss_parts	OUT   TYPES.cursor_type
	)
	AS		
	BEGIN
		
		OPEN p_miss_parts
		FOR
			   SELECT t413.c205_part_number_id PNUM, get_partnum_desc (t413.c205_part_number_id) PDESC, t413.c413_item_price PRICE
				  , Sum (Decode (T413.C413_Status_Fl, 'Q', T413.C413_Item_Qty, (T413.C413_Item_Qty * - 1))) Qty 
				  , T506.C506_Return_Date Returndt ,T506.C506_Expected_Date Expreturndate 
				   FROM t504_consignment t504, t412_inhouse_transactions t412, T413_Inhouse_Trans_Items T413, T506_Returns t506
				  WHERE t504.c504_ref_id           = p_req_id
				    And T504.C901_Ref_Type         = 4119
                    And T506.C506_Ref_Id           = T504.C504_Consignment_Id
				    AND t412.c412_ref_id           = T506.C506_Rma_Id
				    And T412.C412_Inhouse_Trans_Id = T413.C412_Inhouse_Trans_Id
				    And T413.C413_Void_Fl         Is Null 
				    and T506.C901_Ref_Type = 9110 
                    And T504.C504_Void_Fl  Is Null 
                    And T506.C506_Void_Fl Is Null
                    And T412.C412_Void_Fl Is Null 
				GROUP BY T413.C205_Part_Number_Id, T413.C413_Item_Price,T506.C506_Return_Date,T506.C506_Expected_Date ;
            
	END gm_fch_event_missing_parts;
  /******************************************************************
    * Description	: Procedure to fetch missing part Event Information 
    * Author		: AROCKIA PRASATH
    ****************************************************************/
	PROCEDURE gm_fch_event_unreturn_sets (
		p_req_id          	IN     t525_product_request.c525_product_request_id%TYPE,
		p_miss_part_set_info	OUT   TYPES.cursor_type
	)
	AS		
	BEGIN
		
		OPEN p_miss_part_set_info
		FOR
			  SELECT t526.c207_set_id SETID, get_set_name (t526.c207_set_id) SETNM, COUNT (t526.c207_set_id) QTY
				   FROM t525_product_request t525, t526_product_request_detail t526, t504a_loaner_transaction t504a
				  , t504a_consignment_loaner t504al
				  WHERE t525.c525_product_request_id       IN (p_req_id)
				    AND t525.c525_product_request_id        = t526.c525_product_request_id
				    AND t526.c526_void_fl                  IS NULL 
				    AND T525.C525_Void_Fl IS NULL 
				    AND t526.c526_product_request_detail_id = t504a.c526_product_request_detail_id
				    AND T504a.C504_Consignment_Id           = T504al.C504_Consignment_Id
				    AND T504a.C504a_Return_Dt              IS NULL
				GROUP BY t526.c207_set_id;
            
	END gm_fch_event_unreturn_sets;
	
	/******************************************************************
    * Description	: Procedure to fetch missing part Event Information 
    * Author		: AROCKIA PRASATH
    ****************************************************************/
	PROCEDURE gm_fch_unreturn_events (
		p_unreturn_events	OUT   TYPES.cursor_type
	)
	AS	
	v_date_fmt				VARCHAR2(20);	
	v_day_cnt               NUMBER;
	v_company_id  t1900_company.c1900_company_id%TYPE;
	BEGIN
		
	SELECT NVL(get_compid_frm_cntx(),GET_RULE_VALUE('DEFAULT_COMPANY','ONEPORTAL')),
	       NVL(get_compdtfmt_frm_cntx(),Get_Rule_Value ('DATEFMT', 'DATEFORMAT')) 
	  INTO v_company_id,v_date_fmt FROM DUAL;
	  
		v_day_cnt := TO_NUMBER(get_rule_value ('DAYCOUNT', 'EVENTMISSINGEMAIL'));
		OPEN p_unreturn_events
		FOR
		
			
				        SELECT DISTINCT T7104.C7100_Case_Info_Id caseinfoid, T7103.C7103_Name eventname, TO_CHAR (
				            t504a.c504a_expected_return_dt, v_date_fmt) expreturndate, Get_User_Emailid (T7100.C7100_Created_By)
				            Canuseremail, DECODE (T7103.C901_Loan_To, '19518', Get_Rep_Emailid (T7103.C101_Loan_To_Id),
				            Get_User_Emailid (T7103.C101_Loan_To_Id)) Loantoemailid
				           FROM T7104_Case_Set_Information T7104, t7100_case_information t7100, t526_product_request_detail t526
				          , t7103_schedule_info t7103,t504a_loaner_transaction t504a
				          WHERE t7104.c7104_void_fl                  IS NULL
				            AND t7100.c7100_case_info_id              = t7104.c7100_case_info_id
				            AND t526.c526_product_request_detail_id   = t7104.c526_product_request_detail_id
				            AND TRUNC (t504a.c504a_expected_return_dt) + v_day_cnt = TRUNC (CURRENT_DATE)
				            AND t7100.c7100_case_info_id              = t7103.c7100_case_info_id
				            -- and T526.C525_Product_Request_Id = 55373
				            AND t7100.c7100_void_fl                  IS NULL
				            AND t7100.c901_type                       = 1006504
				            AND t7104.c526_product_request_detail_id IS NOT NULL
				            AND t7104.c7104_void_fl                  IS NULL
				            AND t526.c526_void_fl                    IS NULL
				            AND t504a.c504a_return_dt IS NULL 
				            AND t504a.c504a_void_fl IS NULL 
				            AND t504a.c526_product_request_detail_id = t526.c526_product_request_detail_id
				            AND t7103.c1900_company_id =v_company_id
 							AND t7103.c1900_company_id  =t7100.c1900_company_id 
				            AND t526.c525_product_request_id 
				    			 NOT IN
				          (
				        SELECT DISTINCT T526.C525_Product_Request_Id
				           FROM t7104_case_set_information t7104, T7100_Case_Information T7100, T526_Product_Request_Detail T526
				          , t504a_loaner_transaction t504a
				          WHERE t7104.c7104_void_fl                  IS NULL
				            AND T7100.C7100_Case_Info_Id              = T7104.C7100_Case_Info_Id
				            AND T526.C526_Product_Request_Detail_Id   = T7104.C526_Product_Request_Detail_Id
				            --AND TRUNC (T526.C526_Exp_Return_Date) + v_day_cnt = TRUNC (CURRENT_DATE)
				            -- and T526.C525_Product_Request_Id = 55373
				            AND t7100.c7100_void_fl                  IS NULL
				            AND T7100.C901_Type                       = 1006504
				            AND T7104.C526_Product_Request_Detail_Id IS NOT NULL
				            AND T7104.C7104_Void_Fl                  IS NULL
				            AND T526.C526_Void_Fl                    IS NULL
				            AND T504a.C526_Product_Request_Detail_Id  = T526.C526_Product_Request_Detail_Id
				            AND T504a.C504a_Return_Dt                IS NOT NULL 
				            AND t504a.c504a_void_fl IS NULL
				            AND T7100.c1900_company_id =v_company_id
				            )
				         UNION
				        SELECT DISTINCT T7104.C7100_Case_Info_Id caseinfoid, T7103.C7103_Name eventname, TO_CHAR (
				            T526.C526_Exp_Return_Date, v_date_fmt) expreturndate, Get_User_Emailid (T7100.C7100_Created_By)
				            Canuseremail, DECODE (T7103.C901_Loan_To, '19518', Get_Rep_Emailid (T7103.C101_Loan_To_Id),
				            Get_User_Emailid (T7103.C101_Loan_To_Id)) Loantoemailid
				           FROM T7104_Case_Set_Information T7104, T7100_Case_Information T7100, T526_Product_Request_Detail T526
				          , T7103_Schedule_Info t7103
				          WHERE t7104.c7104_void_fl                  IS NULL
				            AND T7100.C7100_Case_Info_Id              = T7104.C7100_Case_Info_Id
				            AND T526.C526_Product_Request_Detail_Id   = T7104.C526_Product_Request_Detail_Id
				            AND TRUNC (T526.C526_Exp_Return_Date) + v_day_cnt = TRUNC (CURRENT_DATE)
				            AND T7100.C7100_Case_Info_Id              = T7103.C7100_Case_Info_Id
				            -- and T526.C525_Product_Request_Id = 55373
				            AND t7100.c7100_void_fl                  IS NULL
				            AND T7100.C901_Type                       = 1006504
				            AND T7104.C526_Product_Request_Detail_Id IS NOT NULL
				            AND T7104.C7104_Void_Fl                  IS NULL
				            AND T526.C526_Void_Fl                    IS NULL
				            AND t7103.c1900_company_id =v_company_id
 							AND t7103.c1900_company_id  =t7100.c1900_company_id 
				            AND T526.C525_Product_Request_Id 
				    			 NOT IN
				          ( 
				         
				        SELECT DISTINCT T504.C504_Ref_Id
				           FROM T504_Consignment T504, T506_Returns T506, T507_Returns_Item T507
				          , t7104_case_set_information t7104, T7100_Case_Information T7100, T526_Product_Request_Detail T526
				          WHERE t506.c506_status_fl                  <= 2
				            AND T506.C901_Ref_Type                    = '9110'
				            AND T506.C506_Ref_Id                      = T504.C504_Consignment_Id
				            AND T506.C506_Void_Fl                    IS NULL
				            AND T504.C504_Void_Fl IS NULL
				            AND T507.C507_Status_Fl                   = 'C'
				            AND T507.C506_Rma_Id                      = T506.C506_Rma_Id
				            AND T7104.C7104_Void_Fl                  IS NULL
				            AND T506.C506_Ref_Id                      = T526.C525_Product_Request_Id
				            AND T7100.C7100_Case_Info_Id              = T7104.C7100_Case_Info_Id
				            AND T526.C526_Product_Request_Detail_Id   = T7104.C526_Product_Request_Detail_Id
				            --AND TRUNC (T526.C526_Exp_Return_Date) + v_day_cnt = TRUNC (CURRENT_DATE)
				            -- and T526.C525_Product_Request_Id = 55373
				            AND t7100.c7100_void_fl                  IS NULL
				            AND T7100.C901_Type                       = 1006504
				            AND T7104.C526_Product_Request_Detail_Id IS NOT NULL
				            AND T7104.C7104_Void_Fl                  IS NULL
				            AND T526.C526_Void_Fl                    IS NULL
				            AND T7100.c1900_company_id =v_company_id
				        ) ;
			    
	END gm_fch_unreturn_events;
/*****************************************************************
    * Description	: Function to fetch shipping id when event edited 
    * Author		: DHANA REDDY
 ****************************************************************/
FUNCTION get_event_ship_add_id (
        p_info_id IN t7100_case_information.c7100_case_info_id%TYPE)
    RETURN NUMBER
IS
    v_cnt NUMBER(20);
    v_req_id t525_product_request.c525_product_request_id%TYPE ;
    v_ship_id t907_shipping_info.c907_shipping_id%TYPE;
BEGIN
    BEGIN
	    -- to find the no of diff address for request
         SELECT COUNT (1), t907.c525_product_request_id
           INTO v_cnt, v_req_id
           FROM t907_shipping_info t907,
            (SELECT DISTINCT t526.c525_product_request_id reqid
               FROM t526_product_request_detail t526, t7104_case_set_information t7104
              WHERE t526.c526_product_request_detail_id = t7104.c526_product_request_detail_id
                AND t7104.c7104_void_fl                IS NULL
                AND t526.c526_void_fl                  IS NULL
                AND t526.c526_required_date            IS NOT NULL
                AND t7104.c7100_case_info_id            = p_info_id
            ) event
          WHERE t907.c525_product_request_id = event.reqid
            AND t907.c901_ship_to           IS NOT NULL
            AND t907.c907_ship_to_id        IS NOT NULL
            AND t907.c907_void_fl           IS NULL
       GROUP BY t907.c901_ship_to, t907.c907_ship_to_id, t907.c106_address_id,
            t907.c525_product_request_id;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_cnt := 0;
    WHEN TOO_MANY_ROWS THEN
        v_cnt := 0;
    END ;
    IF v_cnt > 0 THEN
    -- fetch the shipping id to load common address, it should fetch shipping id, only if it has address
         SELECT c907_shipping_id
           INTO v_ship_id
           FROM t907_shipping_info
          WHERE c525_product_request_id = v_req_id
          	AND c901_ship_to           IS NOT NULL
            AND c907_ship_to_id        IS NOT NULL
            AND c907_void_fl           IS NULL
            AND rownum                  = 1;
    END IF;
    RETURN v_ship_id;
END get_event_ship_add_id;


/**********************************************************************************
    * Description	: Function to fetch planned shipping date when event is edited 
    * Author		: Elango
 **********************************************************************************/
FUNCTION get_event_planned_ship_date (
        p_info_id IN t7100_case_information.c7100_case_info_id%TYPE
)
    RETURN VARCHAR2
IS
    v_cnt    NUMBER(20);
    v_req_id  t525_product_request.c525_product_request_id%TYPE ;
    v_ship_date VARCHAR2(20);
    v_date_fmt	VARCHAR2(20);
BEGIN
    BEGIN
    	Select get_compdtfmt_frm_cntx() into v_date_fmt from dual;
	    -- to find the no of diff planned ship date for request
          SELECT TO_CHAR(C907_SHIP_TO_DT,v_date_fmt)
            INTO v_ship_date
            FROM t907_shipping_info t907,
            (SELECT DISTINCT t526.c525_product_request_id reqid
               FROM t526_product_request_detail t526, t7104_case_set_information t7104
              WHERE t526.c526_product_request_detail_id = t7104.c526_product_request_detail_id
                AND t7104.c7104_void_fl                IS NULL
                AND t526.c526_void_fl                  IS NULL
                AND t526.c526_required_date            IS NOT NULL
                AND t7104.c7100_case_info_id            = p_info_id
            ) event
          WHERE t907.c525_product_request_id = event.reqid
            AND t907.c901_ship_to           IS NOT NULL
            AND t907.c907_ship_to_id        IS NOT NULL
            AND t907.c907_void_fl           IS NULL
          GROUP BY t907.C907_SHIP_TO_DT,t907.c525_product_request_id;
    EXCEPTION
    WHEN NO_DATA_FOUND 
    THEN
        v_ship_date := '';
    WHEN TOO_MANY_ROWS 
    THEN
        v_ship_date := '';
    END ;
  
    RETURN v_ship_date;
END get_event_planned_ship_date;
END gm_pkg_sm_eventbook_rpt;
/