 /* Formatted on 2010/06/25 10:23 (Formatter Plus v4.8.0) */

--@"C:\Database\Packages\Sales\gm_pkg_sm_pricing_request.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_sm_pricing_request
IS
---
/******************************************************************
	 Description : This Fucntion is to get the next request ID
	****************************************************************/
	FUNCTION get_next_price_request_id (
		p_requestid   VARCHAR2
	)
		RETURN VARCHAR2
	IS
		v_req_id	   NUMBER;
		v_requestid    VARCHAR2 (20);
	BEGIN
		BEGIN
			SELECT s7000_account_price_request.NEXTVAL
			  INTO v_req_id
			  FROM DUAL;

			IF LENGTH (v_req_id) = 1
			THEN
				v_req_id	:= '00' || v_req_id;
			ELSIF LENGTH (v_req_id) = 2
			THEN
				v_req_id	:= '0' || v_req_id;
			END IF;

			SELECT 'GM-PR-' || v_req_id
			  INTO v_requestid
			  FROM DUAL;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				RETURN ' ';
		END;

		RETURN v_requestid;
	END get_next_price_request_id;

	/******************************************************************
	 Description : This Fucntion is to get status of the request ID
	****************************************************************/
	FUNCTION get_request_status_id (
		p_requestid   VARCHAR2
	)
		RETURN VARCHAR2
	IS
		v_status_id    VARCHAR2 (10);
	BEGIN
		BEGIN
			SELECT c901_request_status
			  INTO v_status_id
			  FROM t7000_account_price_request
			 WHERE c7000_account_price_request_id = p_requestid AND c7000_void_fl IS NULL;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				RETURN '';
		END;

		RETURN v_status_id;
	END get_request_status_id;

	/******************************************************************
		Description : This procedure is used to fetch account list
	  ****************************************************************/
	PROCEDURE gm_fch_accounts_list (
		p_userid		IN		 VARCHAR2
	  , p_accountlist	OUT 	 TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_accountlist
		 FOR
			  SELECT   c704_account_id ID, c704_account_nm NAME,
                   c703_sales_rep_id repid,
                   get_rep_name (c703_sales_rep_id) rname,
                   c704_account_id || ' - ' || c704_account_nm nm,
                   ad_id adid, vp_id vpid, d_id did
              FROM t704_account, v700_territory_mapping_detail v700
             WHERE v700.ac_id = c704_account_id
               AND c703_sales_rep_id = p_userid
               AND c704_active_fl = 'Y'
          ORDER BY c704_account_nm;
	END gm_fch_accounts_list;
  	/******************************************************************
		Description : This procedure is used to fetch account list
	  ****************************************************************/
	PROCEDURE gm_fch_systems_list (
		p_systemlist	OUT 	 TYPES.cursor_type
	)
	AS
	
		v_company_id t1900_company.C1900_COMPANY_ID%TYPE;
	
	BEGIN
	
	 	SELECT get_compid_frm_cntx()
    		INTO v_company_id
    		FROM DUAL;
    		
		OPEN p_systemlist
		 FOR
		 SELECT   t207.c207_set_id ID, t207.c207_set_nm NAME
		    FROM t207_set_master t207, t2080_set_company_mapping t2080
		   WHERE t207.c901_set_grp_type = 1600
		  -- AND t207.c207_release_for_sale = 'Y' (This is commented due to PMT-6291 Group Pricing Changes)
		     AND c207_void_fl IS NULL
		     AND c207_obsolete_fl IS NULL
		     AND t207.c207_set_id = t2080.c207_set_id
		     AND t2080.c1900_company_id = v_company_id
		     AND t2080.c2080_void_fl IS NULL
		ORDER BY UPPER (t207.c207_set_nm);
	END gm_fch_systems_list;

	/******************************************************************
		Description : This procedure is used to fetch account details
		****************************************************************/
	PROCEDURE gm_fch_accounts_details (
              p_accountid         IN            VARCHAR2
              ,p_gpo_id            IN     t7000_account_price_request.c101_gpo_id%TYPE
              ,p_req_status        IN            t7000_account_price_request. c901_request_status%TYPE DEFAULT NULL
           ,p_accountdetails   OUT       TYPES.cursor_type
       )
       AS
              v_party_id NUMBER;
       BEGIN
              SELECT C101_PARTY_ID INTO v_party_id FROM t704_account WHERE c704_account_id = p_accountid;
            
              OPEN p_accountdetails
              FOR
                     SELECT ac_name straccountname, ad_name stradname, region_name strregname, d_name strdname
                             , ter_name strtername, rep_name strrepname, gp_name strgpname, vp_name strvpname
                             , get_flg_req_id (p_accountid) flgreqid --, get_pricing_status(p_accountid,p_gpo_id) hstatusId
                             , GET_PRICING_REQUEST_ID(p_accountid,p_gpo_id,p_req_status) strRequestId 
                             , t704d.c901_contract contractfl
                             , get_party_name(t704d.c101_idn) strGprAff -- 1006432 Group Pricing Affiliation
                             ,    get_party_name(t704d.c101_gpo) strHlcAff -- 1006433 Healthcare Affiliation 
                             , Decode(t704d.c901_contract,NULL,'No',get_code_name (t704d.c901_contract)) contractflname
                             , get_party_name(v_party_id) strParentAcc
                             , get_party_name(get_account_gpo_id(p_accountid)) strGpb
                       		 , get_code_name(t704.c901_currency) strCurrency
                       		 , p_accountid accid
                       		 , gm_pkg_sm_pricerequest_rpt.get_acc_last12_month_sales(p_accountid,NULL) last12MonthSales
                        FROM v700_territory_mapping_detail v700, t704_account t704, t704d_account_affln t704d
                       WHERE v700.ac_id = t704.c704_account_id AND v700.ac_id = p_accountid
                       	AND t704.c704_account_id = t704d.c704_account_id (+)
                       	AND t704d.c704d_void_fl (+) IS NULL;
       END gm_fch_accounts_details;


	/******************************************************************
		Description : This procedure is used to fetch request master details
		****************************************************************/
	PROCEDURE gm_fch_request_master_details (
			p_requestid  	   IN		VARCHAR2
	  , p_requestdetails   OUT		TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_requestdetails
		 FOR
			 SELECT c7000_account_price_request_id strrequestid, c704_account_id straccountid, 
			  c901_request_status hstatusId
				  , get_account_name(c704_account_id)  straccountname			 
				  , TO_CHAR (c7000_effective_date, get_rule_value ('DATEFMT', 'DATEFORMAT')) effectivedate, c7000_created_by strinitiatebyid
				  , get_user_name (c7000_created_by) strinitiatebyname
				  , TO_CHAR (c7000_created_date, get_rule_value ('DATEFMT', 'DATEFORMAT')) strinitiatedate, c901_request_status hstatusid
				  , get_code_name (c901_request_status) strstatusname, c7000_status_historyfl statushistoryfl
				  , get_code_name (c901_aprl_lvl) apprlvl
				  , c101_gpo_id strGroupAcc, C7000_PUBLISH_OVERRIDE_FL strPublOvrRide
			   FROM t7000_account_price_request t7000
			  WHERE t7000.c7000_account_price_request_id = p_requestid
			    AND t7000.c7000_void_fl IS NULL;
	END gm_fch_request_master_details;

	/******************************************************************
		Description : This procedure is used to fetch details of systems, constructs, groups and parts for pricing request
		****************************************************************/
	PROCEDURE gm_fch_pricing_request_details (
		p_requestid 		IN		 t7000_account_price_request.c7000_account_price_request_id%TYPE
	  , p_acc_id			IN		 t704_account.c704_account_id%TYPE
	  , p_effectivedate 	IN		 VARCHAR2
	  , p_sysinputstr		IN		 CLOB
	  , p_fchallsysfl		IN		 VARCHAR2
	  , p_fchgroupid		IN		 VARCHAR2
	  , p_part_flag         IN       VARCHAR2
	  , p_gpo_id            IN       t7000_account_price_request.c101_gpo_id%TYPE
	  , p_outdetailsystem	OUT 	 TYPES.cursor_type
	  , p_outdetailgroup	OUT 	 TYPES.cursor_type
	  , p_outdetailpart 	OUT 	 TYPES.cursor_type
	)
	AS
		v_group_ids    CLOB;
		v_group_part_ids    CLOB;
		v_sysinputstr  CLOB;
		v_accountid    VARCHAR2 (10);
		v_requestid 	t7000_account_price_request.c7000_account_price_request_id%TYPE;
		v_status 		t7000_account_price_request.c901_request_status%TYPE;
		v_gpo_id        NUMBER;
		v_part_count 	NUMBER;
		v_delim         CLOB;
		v_temp_grp       VARCHAR2 (4000);
		
		CURSOR groupid_details
		IS
			SELECT t4010.c4010_group_id groupid
			  FROM t4010_group t4010
			 WHERE t4010.c207_set_id IN (SELECT token systemid
										   FROM v_in_list)
			   AND c4010_void_fl IS NULL
			   AND c901_type = 40045
			   AND c901_group_type IS NOT NULL
			   AND c4010_inactive_fl IS NULL
			   AND t4010.c4010_publish_fl IS NOT NULL;

		CURSOR refid_details
		IS
			SELECT c7001_ref_id refid
			  FROM t7001_group_part_pricing t7001
			 WHERE t7001.c7000_account_price_request_id = v_requestid AND c901_ref_type = 52000 AND C7001_VOID_FL IS NULL AND C7001_DEMO_FLG = decode(v_requestid,null,null,0);
			 
		CURSOR partid_details
		IS
			SELECT c7001_ref_id refid
			  FROM t7001_group_part_pricing t7001
			 WHERE t7001.c7000_account_price_request_id = v_requestid AND c901_ref_type = 52001 AND C7001_VOID_FL IS NULL AND C7001_DEMO_FLG = decode(v_requestid,null,null,0);
			 
	BEGIN
		my_context.set_my_inlist_ctx (p_sysinputstr);
		
		--By Default when gpo is not passed, we are getting 0, so setting it to blank.		
		SELECT DECODE(p_gpo_id,0,'',p_gpo_id) INTO v_gpo_id FROM DUAL;
		
		v_requestid := p_requestid;
		
		-- WHen request id is not passed from UI, we are trying to get the Request ID for the account.
		IF (p_requestid IS NULL)
		THEN
		
			v_requestid := GET_PRICING_REQUEST_ID(p_acc_id,v_gpo_id);--,v_gpo_id,get_pricing_status(p_acc_id));

		ELSE
			v_requestid := p_requestid;
		END IF;

		--get the group ids for the system ids passed
		FOR currindex1 IN groupid_details
		LOOP
			v_group_ids := v_group_ids || ',' || currindex1.groupid;

		END LOOP;
		--get the group ids for the system ids passed
		FOR currindex2 IN partid_details
		LOOP
		
			SELECT DECODE(NVL(length(v_group_part_ids),0),0,'',',') into v_delim FROM DUAL;
			
			v_temp_grp := get_part_groupid(currindex2.refid);
			v_group_part_ids := v_group_part_ids ||v_delim ||v_temp_grp;	

			INSERT INTO MY_TEMP_LIST VALUES(v_temp_grp);
			v_part_count := v_part_count + 1;
		END LOOP;
		
		IF (LENGTH(v_group_part_ids)>0)
		THEN
			v_group_ids := v_group_ids ||v_group_part_ids ;
		END IF;
		v_sysinputstr := p_sysinputstr;
		--if systeminput string is blank or null, get the group details for the request's group
		IF (p_fchallsysfl IS NOT NULL)
		THEN
			--v_group_ids := '';
			FOR currindex IN refid_details
			LOOP
				v_group_ids := v_group_ids || ',' || currindex.refid;
				v_group_ids :=v_group_ids || ','; -- MNTTASK-6115-When load the data last group not displayed in Pricing by Group Tab.
			END LOOP;
			v_sysinputstr := get_groups_systemids (v_group_ids);
		END IF;
		
		v_accountid := p_acc_id;
		
		IF v_accountid IS NULL
		THEN
			SELECT c704_account_id
			  INTO v_accountid
			  FROM t7000_account_price_request
			 WHERE c7000_account_price_request_id = v_requestid;
		END IF;
		-- This condition will get the group name only selected group name in UnPricedGroup tab from portal. 
		IF p_fchgroupid IS NOT NULL THEN
			v_group_ids:= p_fchgroupid;
			v_sysinputstr := get_groups_systemids (v_group_ids);
			INSERT INTO MY_TEMP_LIST VALUES(v_group_ids);
		END IF;
		
		my_context.set_my_double_inlist_ctx (v_sysinputstr, v_group_ids);
		
		/*gm_procedure_log('v_group_ids',v_group_ids||','||v_sysinputstr);
		gm_procedure_log('p_fchgroupid',p_fchgroupid);*/
		
		gm_fch_system_pricing_details (v_requestid, v_accountid, p_effectivedate, v_sysinputstr, p_outdetailsystem);
		gm_fch_group_pricing_details (v_requestid, v_accountid, p_effectivedate, v_group_ids, v_gpo_id, p_outdetailgroup);
	
--		--gm_procedure_log('p_part_flag',p_part_flag);
	
		 insert into my_temp_list (MY_TEMP_TXN_ID)
          SELECT c4010_group_id		   FROM t4010_group t4010
				WHERE c4010_void_fl IS NULL		   AND c901_type = 40045
				AND c901_group_type IS NOT NULL    and t4010.c207_set_id in(SELECT token
											 FROM v_double_in_list
											WHERE token IS NOT NULL)
            	and c901_priced_type = '52111';
		
		IF p_part_flag IS NULL  AND v_part_count=0 THEN
		 	OPEN p_outdetailpart FOR 
  			 SELECT * FROM DUAL WHERE 1=0;
  		--  We need not get all part details during fetch. This has to be revisited later to be made as seperate procedure call. 04/12.	 
   	    ELSIF v_part_count > 0 THEN
   	    	--gm_procedure_log('comin in',1||'.'||v_group_part_ids);
   	    	gm_fch_part_pricing_details (v_requestid, v_accountid, p_effectivedate,v_group_part_ids, v_gpo_id, p_outdetailpart);
   	    
   		ELSE
   			 v_group_ids := null;
   	    	 v_group_ids := p_fchgroupid;
   	    	-- gm_procedure_log('comin in',2);
   			 gm_fch_part_pricing_details (v_requestid, v_accountid, p_effectivedate, v_group_ids, v_gpo_id, p_outdetailpart);
   		END IF;
   		select count(*) INTO v_part_count from MY_TEMP_LIST;
   		DELETE FROM MY_TEMP_LIST;
   		--gm_procedure_log(' temp count',v_part_count);
  		
    END gm_fch_pricing_request_details;
    
	
	/******************************************************************
		Description : This procedure is used to fetch details of system  for pricing request
		****************************************************************/
	PROCEDURE gm_fch_system_pricing_details (
		p_requestid 			IN		 t7000_account_price_request.c7000_account_price_request_id%TYPE
	  , p_acc_id				IN		 t704_account.c704_account_id%TYPE
	  , p_effectivedate 		IN		 VARCHAR2
	  , p_inputstr				IN		 VARCHAR2
	  , p_outdetailsystem_cur	OUT 	 TYPES.cursor_type
	)
	AS
		v_test		   VARCHAR (100);
	BEGIN
		--gm_procedure_log('p_inputstr',p_inputstr);
		OPEN p_outdetailsystem_cur
		 FOR
			 SELECT   t207.c207_set_id setid, c207_set_nm setname
					, get_log_flag (p_requestid || '-' || t207.c207_set_id, 1252) clog
				 FROM t207_set_master t207
				WHERE t207.c207_set_id IN (SELECT token
											 FROM v_double_in_list
											WHERE token IS NOT NULL)
			 ORDER BY setid;
	END gm_fch_system_pricing_details;

	/******************************************************************
		Description : This procedure is used to fetch details of groups and its price info for pricing request
		****************************************************************/
	PROCEDURE gm_fch_group_pricing_details (
		p_requestid 		   IN		t7000_account_price_request.c7000_account_price_request_id%TYPE
	  , p_acc_id			   IN		t704_account.c704_account_id%TYPE
	  , p_effectivedate 	   IN		VARCHAR2
	  , p_groupinputstr 	   IN		VARCHAR2
	  , p_gpo_id               IN       t7000_account_price_request. c101_gpo_id%TYPE
	  , p_outdetailgroup_cur   OUT		TYPES.cursor_type
	)
	AS
	BEGIN
		--my_context.set_my_inlist_ctx (p_groupinputstr);
		my_context.set_my_cloblist(p_groupinputstr||',');
    
		OPEN p_outdetailgroup_cur
		 FOR
		 			 SELECT   grp.groupid, systemid, groupname, grouptype
--Commented			, gm_pkg_sm_pricing_request.get_group_currentp_req (p_acc_id, grp.groupid,NULL , p_gpo_id) currentp
					,currentp
					, adjustedp, get_group_price (p_requestid, grp.groupid, 52060) listp
					, gm_pkg_pd_group_pricing.get_construct_fl (grp.groupid) consfl
					--	, get_group_price (p_requestid, grp.groupid, 52061) ad
					--	, get_group_price (p_requestid, grp.groupid, 52062) vp
					  ,get_group_price (p_requestid, grp.groupid, 52063) tw, chngtype, chngval, isapproved, clog 
            ,DECODE(efftype,null,53000,efftype) efftype , grpstatus, active_fl, applvl
					, demoflg , '' demoadjustedp , '' demochngtype , '' demochngval , '' demofromdate , '' demotodate, fromdate, '' dflg,
					grp.pricedtype pricedtype
				 FROM (SELECT t4010.c4010_group_id groupid, t4010.c207_set_id systemid, t4010.c4010_group_nm groupname
							, t4010.c901_group_type grouptype,t4010.c901_priced_type pricedtype
							 ,master_grp.PRICE currentp
						 FROM t4010_group t4010
						 ,
			             (select * from (  SELECT t705.c704_account_id,t705.c4010_group_id grpid, t705.c705_unit_price PRICE
						FROM t705_account_pricing_by_group t705
						WHERE  ( t705.c704_account_id = NVL(p_acc_id,-999) or t705.C101_PARTY_ID =  NVL(p_gpo_id, -999)) 
						AND t705.c4010_group_id in (
			               		SELECT to_char(token) FROM v_clob_list WHERE token IS NOT NULL
			                )
						union all
						SELECT c704_account_id,t7051.c7051_ref_id grpid, t7051.c7051_price PRICE
				            FROM t7051_account_group_pricing t7051
				           WHERE t7051.c901_ref_type = '52000'               --'GROUP'
				             AND t7051.c7051_active_fl = 'Y'
				             AND t7051.C7051_VOID_FL IS NULL
				             AND t7051.c7051_ref_id in (SELECT to_char(token) FROM v_clob_list WHERE to_char(token) IS NOT NULL)
				             AND (  c704_account_id = NVL(p_acc_id,-999) or t7051.C101_GPO_ID =  NVL(p_gpo_id, -999) ) 	
			               )
				       ) master_grp
						WHERE t4010.c4010_group_id IN (SELECT to_char(token)
														 FROM v_clob_list
														WHERE token IS NOT NULL)
						and to_char(t4010.c4010_group_id)= master_grp.grpid(+)														
														) grp
					, (SELECT t7001.c7001_ref_id groupid, t7001.c7001_adjusted_price adjustedp
							, t7001.c901_change_type chngtype, t7001.c7001_change_value chngval
							, t7001.c901_status_fl isapproved, get_user_name (t7001.c7001_approved_by) approvedby
							, get_log_flag (p_requestid || '-' || c7001_ref_id, 1252) clog
							,t7001.c7001_demo_flg  demoflg
              , t7001.c901_effective_type efftype
             	, TO_CHAR (t7001.c7001_effective_date, get_rule_value ('DATEFMT', 'DATEFORMAT')) fromdate
              , decode (t7001.c7001_effective_to_date,null,null,TO_CHAR (t7001.c7001_effective_to_date, get_rule_value ('DATEFMT', 'DATEFORMAT'))) todate
              , get_code_name(c901_status_fl) grpstatus
              , t7001.c7001_active_fl active_fl
              , get_code_name(t7001.c901_aprl_lvl) applvl
						 FROM t7001_group_part_pricing t7001, t4010_group t4010
						WHERE t7001.c7001_ref_id = t4010.c4010_group_id
						  AND t7001.c7000_account_price_request_id = p_requestid
						   AND c901_ref_type = 52000
						   AND c901_status_fl is not null 
						     AND t7001.C7001_VOID_FL IS Null
              AND T7001.C7001_DEMO_FLG = decode(p_requestid, null, null, 0)) pr
          /* Commenting the Demo Price Query. Will be enabled once the actual requirment for Demo pricing is obtained.
           , (SELECT t7001.c7001_ref_id demogrpid, t7001.c7001_adjusted_price demoadjustedp
							, t7001.c901_change_type demochngtype, t7001.c7001_change_value demochngval
							, decode(p_requestid, null,0,t7001.c7001_demo_flg) dflg
							, TO_CHAR (t7001.c7001_effective_date, get_rule_value ('DATEFMT', 'DATEFORMAT')) demofromdate
              , TO_CHAR (t7001.c7001_effective_to_date, get_rule_value ('DATEFMT', 'DATEFORMAT')) demotodate
						 FROM t7001_group_part_pricing t7001, t4010_group t4010
						WHERE t7001.c7001_ref_id = t4010.c4010_group_id
						  AND t7001.c7000_account_price_request_id = p_requestid
						    AND t7001.C7001_VOID_FL IS Null
              AND T7001.C7001_DEMO_FLG = decode(p_requestid, null, null, 1)) dpr
			  and dpr.demogrpid(+) = grp.groupid*/
              WHERE pr.groupid(+) = grp.groupid
			 ORDER BY systemid  ,pricedtype,groupname ;
		 /*
			 SELECT   grp.groupid, systemid, groupname, grouptype
					, gm_pkg_sm_pricing_request.get_group_currentp_req (p_acc_id, grp.groupid,NULL , p_gpo_id) currentp
					, adjustedp, get_group_price (p_requestid, grp.groupid, 52060) listp
					, gm_pkg_pd_group_pricing.get_construct_fl (grp.groupid) consfl
					--	, get_group_price (p_requestid, grp.groupid, 52061) ad
					--	, get_group_price (p_requestid, grp.groupid, 52062) vp
					  ,get_group_price (p_requestid, grp.groupid, 52063) tw, chngtype, chngval, isapproved, clog 
            ,DECODE(efftype,null,53000,efftype) efftype , grpstatus, active_fl, applvl
					, demoflg , dpr.demoadjustedp , dpr.demochngtype , dpr.demochngval , dpr.demofromdate , dpr.demotodate, fromdate, dpr.dflg,
					grp.pricedtype pricedtype
				 FROM (SELECT t4010.c4010_group_id groupid, t4010.c207_set_id systemid, t4010.c4010_group_nm groupname
							, t4010.c901_group_type grouptype,t4010.c901_priced_type pricedtype
						 FROM t4010_group t4010
						WHERE t4010.c4010_group_id IN (SELECT token
														 FROM v_in_list
														WHERE token IS NOT NULL)) grp
					, (SELECT t7001.c7001_ref_id groupid, t7001.c7001_adjusted_price adjustedp
							, t7001.c901_change_type chngtype, t7001.c7001_change_value chngval
							, t7001.c901_status_fl isapproved, get_user_name (t7001.c7001_approved_by) approvedby
							, get_log_flag (p_requestid || '-' || c7001_ref_id, 1252) clog
							,t7001.c7001_demo_flg  demoflg
              , t7001.c901_effective_type efftype
             	, TO_CHAR (t7001.c7001_effective_date, get_rule_value ('DATEFMT', 'DATEFORMAT')) fromdate
              , decode (t7001.c7001_effective_to_date,null,null,TO_CHAR (t7001.c7001_effective_to_date, get_rule_value ('DATEFMT', 'DATEFORMAT'))) todate
              , get_code_name(c901_status_fl) grpstatus
              , t7001.c7001_active_fl active_fl
              , get_code_name(t7001.c901_aprl_lvl) applvl
						 FROM t7001_group_part_pricing t7001, t4010_group t4010
						WHERE t7001.c7001_ref_id = t4010.c4010_group_id
						  AND t7001.c7000_account_price_request_id = p_requestid
						   AND c901_ref_type = 52000
						   AND c901_status_fl is not null 
						     AND t7001.C7001_VOID_FL IS Null
              AND T7001.C7001_DEMO_FLG = decode(p_requestid, null, null, 0)) pr
          , (SELECT t7001.c7001_ref_id demogrpid, t7001.c7001_adjusted_price demoadjustedp
							, t7001.c901_change_type demochngtype, t7001.c7001_change_value demochngval
							, decode(p_requestid, null,0,t7001.c7001_demo_flg) dflg
							, TO_CHAR (t7001.c7001_effective_date, get_rule_value ('DATEFMT', 'DATEFORMAT')) demofromdate
              , TO_CHAR (t7001.c7001_effective_to_date, get_rule_value ('DATEFMT', 'DATEFORMAT')) demotodate
						 FROM t7001_group_part_pricing t7001, t4010_group t4010
						WHERE t7001.c7001_ref_id = t4010.c4010_group_id
						  AND t7001.c7000_account_price_request_id = p_requestid
						    AND t7001.C7001_VOID_FL IS Null
              AND T7001.C7001_DEMO_FLG = decode(p_requestid, null, null, 1)) dpr
				WHERE pr.groupid(+) = grp.groupid
         and dpr.demogrpid(+) = grp.groupid
			 ORDER BY systemid  ,pricedtype,groupname ;*/
	END gm_fch_group_pricing_details;

	/******************************************************************
		Description : This procedure is used to fetch details of parts and its price info for pricing request
	  ****************************************************************/
	PROCEDURE gm_fch_part_pricing_details (
		p_requestid 		  IN	   t7000_account_price_request.c7000_account_price_request_id%TYPE
	  , p_acc_id			  IN	   t704_account.c704_account_id%TYPE
	  , p_effectivedate 	  IN	   VARCHAR2
	  , p_groupinputstr 	  IN	   VARCHAR2
	  , p_gpo_id  			  IN 	   t7000_account_price_request. c101_gpo_id%TYPE
	  , p_outdetailpart_cur   OUT	   TYPES.cursor_type
	)
	AS
	BEGIN
		--gm_procedure_log('p_groupinputstr....',p_groupinputstr||'.');
		--commit;
		--my_context.set_my_inlist_ctx (p_groupinputstr);
		
		OPEN p_outdetailpart_cur
		 FOR
			 SELECT   partdts.groupid, partdts.pnum, pdesc,-- partdts.PRICE currentp
			--AS this cursor is going to get parts, we are calling the function to get the current price.
			 get_account_part_pricing(p_acc_id,partdts.pnum,p_gpo_id) currentp
          , pr.adjustedp, chngtype, chngval, isapproved
					, approvedby ,DECODE( pr.efftype,null,53000, pr.efftype) efftype , pr.effdate, pr.active_fl active_fl
					, get_part_price(partdts.pnum,'L') listp 
					, get_part_price (partdts.pnum, 'E') tw
					,pr.applvl applvl
				 FROM (SELECT c4010_group_id groupid, c205_part_number_id pnum
							, get_partnum_desc (c205_part_number_id) pdesc 
							 FROM t4011_group_detail t4011
							 WHERE
							c4010_group_id IN 
												(SELECT * FROM MY_TEMP_LIST)
												/*(SELECT token
													   FROM v_in_list
													  WHERE token IS NOT NULL)*/ 
													  AND c901_part_pricing_type = '52080') partdts
						, (SELECT t7001.c7001_ref_id pnum, 
						--COMMENTED get_group_currentp (p_acc_id, c7001_ref_id, p_gpo_id) currentp
						'' currentp
              , t7001.c901_effective_type efftype ,  TO_CHAR (t7001.c7001_effective_date, get_rule_value ('DATEFMT', 'DATEFORMAT')) effdate
							, t7001.c7001_adjusted_price adjustedp, t7001.c901_change_type chngtype
							, t7001.c7001_change_value chngval, t7001.c901_status_fl isapproved
							, get_user_name (t7001.c7001_approved_by) approvedby
							, t7001.c7001_active_fl active_fl
							,get_code_name(t7001.c901_aprl_lvl) applvl
						 FROM t7001_group_part_pricing t7001
						WHERE t7001.c7000_account_price_request_id = p_requestid AND c901_ref_type = 52001) pr
				WHERE partdts.pnum = pr.pnum(+)
			 ORDER BY groupid;
			
	END gm_fch_part_pricing_details;

	  /******************************************************************
	  Description : This procedure is used to fetch details of parts and its price info for pricing request
	****************************************************************/
	PROCEDURE gm_fch_workflow_details (
		p_requestid 		IN		 t7000_account_price_request.c7000_account_price_request_id%TYPE
	  , p_outdetail_group	OUT 	 TYPES.cursor_type
	  , p_outdetail_part	OUT 	 TYPES.cursor_type
	)
	AS
		v_grp_pro_appr NUMBER;
		v_grp_pro_pen  NUMBER;
		v_grp_ad_pro   NUMBER;
		v_grp_ad_pen   NUMBER;
		v_grp_vp_pro   NUMBER;
		v_grp_vp_pen   NUMBER;
		v_grp_pc_pro   NUMBER;
		v_grp_pc_pen   NUMBER;
		v_part_pro_appr NUMBER;
		v_part_pro_pen NUMBER;
		v_part_ad_pro  NUMBER;
		v_part_ad_pen  NUMBER;
		v_part_vp_pro  NUMBER;
		v_part_vp_pen  NUMBER;
		v_part_pc_pro  NUMBER;
		v_part_pc_pen  NUMBER;
	BEGIN
		OPEN p_outdetail_group
		 FOR
			 SELECT pp, pa
			   --, adp, ada, vpp, vpa, pcp, pca
			 FROM	(SELECT COUNT (*) pp
					   FROM t7001_group_part_pricing t7001
					  WHERE t7001.c7000_account_price_request_id = p_requestid
						AND t7001.c901_ref_type = 52000
						AND t7001.c901_status_fl = 52027
						AND t7001.c7001_void_fl IS NULL
						AND t7001.c7001_adjusted_price IS NOT NULL)
				  , (SELECT COUNT (*) pa
					   FROM t7001_group_part_pricing t7001
					  WHERE t7001.c7000_account_price_request_id = p_requestid
						AND t7001.c901_ref_type = 52000
						AND t7001.c901_status_fl = 52028
						AND t7001.c7001_void_fl IS NULL);

	
		OPEN p_outdetail_part
		 FOR
			 SELECT pp, pa
			   --, adp, ada, vpp, vpa, pcp, pca
			 FROM	(SELECT COUNT (*) pp
					   FROM t7001_group_part_pricing t7001
					  WHERE t7001.c7000_account_price_request_id = p_requestid
						AND t7001.c901_ref_type = 52001
						AND t7001.c901_status_fl = 52027
						AND t7001.c7001_void_fl IS NULL)
				  , (SELECT COUNT (*) pa
					   FROM t7001_group_part_pricing t7001
					  WHERE t7001.c7000_account_price_request_id = p_requestid
						AND t7001.c901_ref_type = 52001
						AND t7001.c901_status_fl = 52028
						AND t7001.c7001_void_fl IS NULL);
		
	END gm_fch_workflow_details;

	/******************************************************************
		Description : This procedure is used to fetch details of constructs in a system
	  ****************************************************************/
	PROCEDURE gm_fch_constrcut_pr_details (
		p_requestid 			   IN		t7000_account_price_request.c7000_account_price_request_id%TYPE
	  , p_acc_id				   IN		t704_account.c704_account_id%TYPE
	  , p_effectivedate 		   IN		VARCHAR2
	  , p_gpo_id  				   IN       t7000_account_price_request. c101_gpo_id%TYPE
	  , p_userid                   IN		VARCHAR2
	  , p_outdetailsystem_cur	   OUT		TYPES.cursor_type
	  , p_outdetailconstruct_cur   OUT		TYPES.cursor_type
	  , p_outdetailpart_cur 	   OUT		TYPES.cursor_type
	)
	AS
		v_setids	   VARCHAR (1000);
		v_group_ids    CLOB;
		v_requestid 	t7000_account_price_request.c7000_account_price_request_id%TYPE;
		v_partGroupid  CLOB ;
		v_status   NUMBER;
		v_gpo_id NUMBER;
		v_acc_id NUMBER;
		
		CURSOR req_grp_details
		IS
			SELECT c7001_ref_id groupid
			  FROM t7001_group_part_pricing t7001
			 WHERE t7001.c7000_account_price_request_id = v_requestid AND c901_ref_type = 52000 
			 and C7001_ADJUSTED_PRICE IS NOT NULL
			 AND C901_STATUS_FL IS NOT NULL
			 AND C7001_VOID_FL IS NULL
			 and c7001_demo_flg = decode(v_requestid,null,null,0);
			 
		CURSOR partid_details
		IS
		SELECT c7001_ref_id refid
			  FROM t7001_group_part_pricing t7001
			 WHERE t7001.c7000_account_price_request_id = v_requestid AND c901_ref_type = 52001 AND C7001_VOID_FL IS NULL AND C7001_DEMO_FLG = decode(v_requestid,null,null,0)
			 AND C7001_ADJUSTED_PRICE IS NOT NULL
			 AND C901_STATUS_FL IS NOT NULL;
	BEGIN
	
		v_requestid := p_requestid;
		
		--By Default when gpo is not passed, we are getting 0, so setting it to blank.
		SELECT DECODE(p_gpo_id,0,'',p_gpo_id) INTO v_gpo_id FROM DUAL;
		
		-- Get the Accont,GPO ID from the passed in request id. 10/30 
		IF v_requestid IS NOT NULL
		THEN
			BEGIN
				SELECT C101_GPO_ID,C704_ACCOUNT_ID INTO v_gpo_id,v_acc_id FROM t7000_account_price_request where c7000_account_price_request_id = v_requestid;
			EXCEPTION WHEN NO_DATA_FOUND THEN
				v_gpo_id :='';
			END;
			-- When we have a request for GPO, account id cannot be passed further,hence setting to null.
			SELECT DECODE(v_gpo_id,'',v_acc_id,'') INTO v_acc_id FROM DUAL;
			
		END IF;
		
		-- WHen request id is not passed from UI, we are trying to get the Request ID for the account.
		if (v_requestid IS NULL)
		THEN
			v_requestid := GET_PRICING_REQUEST_ID(v_acc_id,v_gpo_id);--,get_pricing_status(v_acc_id,v_gpo_id));
		END IF;
 
		FOR currindex1 IN req_grp_details
		LOOP
			v_group_ids := v_group_ids || ',' || currindex1.groupid;
		END LOOP;
	
		FOR currindex2 IN partid_details
		LOOP
			v_partGroupid := v_partGroupid||','||get_part_groupid(currindex2.refid);
		--	v_partGroupid := v_partGroupid||v_group_ids;
		END LOOP;
		
		IF v_group_ids IS NOT NULL AND v_partGroupid IS NULL
		THEN
			v_partGroupid := v_group_ids;
		ELSIF v_group_ids IS NULL AND v_partGroupid IS NOT NULL
		THEN
			v_group_ids := v_partGroupid;
		END IF;
		v_group_ids:=v_group_ids||','||v_partGroupid;
		
		v_setids	:= get_groups_dep_systemids (v_group_ids);
		v_setids	:=v_setids||','|| get_groups_dep_systemids (v_partGroupid);
		my_context.set_my_double_inlist_ctx (v_setids, v_group_ids);
		--my_context.set_my_inlist_ctx (v_group_ids);
		v_group_ids := v_group_ids||','; -- MNTTASK-6115-When load the data last group not displayed in Pricing by Group Tab.
		my_context.set_my_cloblist(v_group_ids||',');
		
		OPEN p_outdetailsystem_cur
		 FOR
      SELECT   t207.c207_set_id setid, t207.c207_set_nm setname, get_rule_value (t207.c207_set_id, 'Pricing-Rule') rulev
					, get_rule_desc (t207.c207_set_id, 'Pricing-Rule') ruled
				 FROM t207_set_master t207--, t240_baseline_value t240
				WHERE t207.c207_set_id IN (SELECT token
											 FROM v_double_in_list);
       -- and t240.c207_set_id=t207.c207_set_id(+)
      --  and t240.c901_type=20181
		--	 ORDER BY t240.c240_value;
			 
		OPEN p_outdetailconstruct_cur
		 FOR
		 		 		 	 SELECT   groupbydetails.groupid groupid, groupbydetails.systemid, constructid
					, DECODE (constructname, NULL, 'Non-Construct', constructname) constructname, groupname, grouptype, qty
					, typename, isapproved, approvedby, currentp, adjustedp, listp, ad, vp, tw, asp, rulev, ruled
					, prigrpfl, priconsfl, demoflg, dfromdate , dtodate , dprice , effectype , effecdate, clog, active_fl, applvl
					, gm_pkg_sm_pricing_request.get_pricing_rule_by_id(v_requestid,constructid,'') pricing_rule_cons_id
					, gm_pkg_sm_pricing_request.get_pricing_rule_by_id(v_requestid,NULL,groupbydetails.groupid) pricing_rule_grp_id				 			
					
				 FROM (SELECT grp.groupid groupid, 
				 			systemid, groupname, grp.grouptype grouptype, grp.typename typename, isapproved
							, approvedby, 
					--COMMENTED NVL(pr.currentp,gm_pkg_sm_pricing_request.get_group_currentp_req (v_acc_id, grp.groupid, NULL, v_gpo_id) ) currentp,
							grp.CURRENTP CURRENTP, 
							pr.adjustedp adjustedp, grp.prigrpfl prigrpfl, 
							NVL(LISTP,get_group_price ('', grp.groupid, 52060)) listp, ad, vp, 
							NVL(tw,get_group_price ('', grp.groupid, 52063)) tw, asp
							, rulev, ruled, pr.demoflg  demoflg, pr.dfromdate dfromdate, pr.dtodate dtodate, pr.dprice dprice, pr.clog clog
              , pr.effecdate effecdate , pr.effectype effectype,pr.active_fl active_fl,pr.applvl applvl
						 FROM (SELECT TO_CHAR (t4010.c4010_group_id) groupid, t4010.c207_set_id systemid
									, t4010.c4010_group_nm groupname, t4010.c901_group_type grouptype
									, get_code_name (t4010.c901_group_type) typename
									, t4010.c4010_principal_grp_fl prigrpfl
									, MASTER_grp.PRICE CURRENTP
								 FROM t4010_group t4010
								  ,(select * from (  SELECT t705.c704_account_id,to_char(t705.c4010_group_id) grpid, t705.c705_unit_price PRICE
									FROM t705_account_pricing_by_group t705
									WHERE  ( t705.c704_account_id = NVL(v_acc_id,-999) or t705.C101_PARTY_ID =  NVL(v_gpo_id, -999)) 
									AND t705.c4010_group_id in (
						               	  SELECT c7001_ref_id groupid
														   FROM t7001_group_part_pricing t7001
														  WHERE t7001.c7000_account_price_request_id = v_requestid
						                )
									union all
									SELECT c704_account_id,t7051.c7051_ref_id grpid, t7051.c7051_price PRICE
							            FROM t7051_account_group_pricing t7051
							           WHERE t7051.c901_ref_type = '52000'               --'GROUP'
							             AND t7051.c7051_active_fl = 'Y'
							             AND t7051.C7051_VOID_FL IS NULL
							             AND t7051.c7051_ref_id in (  SELECT c7001_ref_id groupid
														   FROM t7001_group_part_pricing t7001
														  WHERE t7001.c7000_account_price_request_id = v_requestid)
							             AND (  t7051.c704_account_id = NVL(v_acc_id,-999) or t7051.C101_GPO_ID =  NVL(v_gpo_id, -999) ) 	
						               )
				      			 ) master_grp
             					  where to_char(t4010.c4010_group_id)= master_grp.grpid(+)		
									AND TO_CHAR (t4010.c4010_group_id) IN (
										  SELECT TO_CHAR  (c7001_ref_id) groupid
											FROM t7001_group_part_pricing t7001
										   WHERE t7001.c7000_account_price_request_id = v_requestid
											 AND c901_ref_type = 52000
											 AND t7001.c901_status_fl is not null
											 AND t7001.C7001_VOID_FL IS NULL
                       and t7001.c7001_demo_flg = decode(v_requestid,null,null,0)
										  UNION ALL
										  SELECT TO_CHAR (t7004.c4010_group_id) groupid
											FROM t7004_construct_mapping t7004
										   WHERE t7004.c7003_system_construct_id IN (
													 SELECT t7004.c7003_system_construct_id
													   FROM t7004_construct_mapping t7004
													  WHERE t7004.c4010_group_id IN (
																SELECT c7001_ref_id groupid
																  FROM t7001_group_part_pricing t7001
																 WHERE t7001.c7000_account_price_request_id =
																											 v_requestid
																   AND c901_ref_type = 52000
																   AND c901_status_fl is not null
																   AND t7001.C7001_VOID_FL IS NULL
                                   and t7001.c7001_demo_flg = decode(v_requestid,null,null,0)))
                                   UNION
                                   --modified on 01/17 to get the part info in the approval while along with other system, group information.
                                   	SELECT c7001_ref_id
                                    from t7001_group_part_pricing where 
                                    c7000_account_price_request_id = v_requestid
                                    and c7001_ref_id in (
                                   		SELECT to_char(token) groupid from v_clob_list
                                   	)
                                   )
                                   
                                   ) grp
							, (SELECT t7001.c7001_ref_id groupid, t7001.c901_ref_type refnum
									/*, gm_pkg_sm_pricing_request.get_group_currentp_req (p_acc_id
																					  , c7001_ref_id
																					  , NULL, v_gpo_id
																					   ) currentp*/
									, t7001.c7001_adjusted_price adjustedp
									, get_group_price (v_requestid, c7001_ref_id, 52060) listp
				--COMMENTED					, get_group_price ('', c7001_ref_id, 52061) ad
									, '' ad
				--COMMENTED					, get_group_price ('', c7001_ref_id, 52062) vp
									, '' vp
									, get_group_price (v_requestid, c7001_ref_id, 52063) tw
				--COMMENTED					, gm_pkg_pd_group_pricing.get_group_avg_selling_price (t7001.c7001_ref_id) asp
									, '' asp
									, t7001.c901_status_fl isapproved
									, get_user_name (t7001.c7001_approved_by) approvedby
									, get_rule_value (c7001_ref_id, 'Pricing-Rule') rulev
                  , to_char(t7001.c7001_effective_date,get_rule_value ('DATEFMT', 'DATEFORMAT')) effecdate
                  , get_code_name(t7001.c901_effective_type) effectype
									, get_rule_desc (c7001_ref_id, 'Pricing-Rule') ruled
                --COMMENTED  , get_demo_price('Price',v_requestid,c7001_ref_id) dprice
                --COMMENTED, get_demo_price('Flag',v_requestid,c7001_ref_id) demoflg
                --COMMENTED  , get_demo_data('FromDt',v_requestid,c7001_ref_id) dfromdate
                --COMMENTED  , get_demo_data('ToDt',v_requestid,c7001_ref_id) dtodate
                			 , '' dprice,'' demoflg, '' dfromdate, '' dtodate
                  , get_log_flag (v_requestid || '-' || c7001_ref_id, 1252) clog
                  , c7001_active_fl active_fl
                  ,get_code_name(c901_aprl_lvl)  applvl
								 FROM t7001_group_part_pricing t7001, t4010_group t4010
								WHERE t7001.c7000_account_price_request_id = v_requestid
								AND t7001.c901_status_fl is not null
								AND t7001.C7001_VOID_FL IS NULL
					AND c901_ref_type = 52000 			
                  and t7001.c7001_demo_flg = decode(v_requestid,null,null,0)
                  AND (t7001.c7001_ref_id = t4010.c4010_group_id
                   AND t4010.c4010_group_id IN  (SELECT to_char(token) groupid from v_clob_list)
                   )) pr
						WHERE pr.groupid(+) = grp.groupid) groupbydetails
					, (SELECT t7004.c4010_group_id groupid, t7003.c207_set_id systemid
							, t7003.c7003_principal_fl priconsfl, t7004.c7003_system_construct_id constructid
							, t7003.c7003_construct_name constructname, t7004.c7004_qty qty
						 FROM t7004_construct_mapping t7004, t7003_system_construct t7003
						WHERE t7004.c7003_system_construct_id IN (
								  SELECT t7004.c7003_system_construct_id
									FROM t7004_construct_mapping t7004
								   WHERE to_char(t7004.c4010_group_id) IN (
											 SELECT c7001_ref_id groupid
											   FROM t7001_group_part_pricing t7001
											  WHERE t7001.c7000_account_price_request_id = v_requestid
												AND c901_ref_type = 52000 and t7001.c7001_demo_flg = decode(v_requestid,null,null,0)
												AND t7001.c901_status_fl is not null
												AND t7001.C7001_VOID_FL IS NULL
												UNION  
												--SELECT token groupid from v_in_list
												--modified on 01/17 to get the part info in the approval while along with other system, group information.
			                                   	SELECT c7001_ref_id
			                                    from t7001_group_part_pricing where 
			                                    c7000_account_price_request_id = v_requestid
			                                    and c7001_ref_id in (
			                                   		SELECT to_char(token) groupid from v_clob_list
			                                   	)
												
												))
						  AND t7003.c7003_system_construct_id = t7004.c7003_system_construct_id
						  AND t7003.c7003_void_fl IS NULL
              AND t7003.c901_status = 52071) constructdetails
				WHERE groupbydetails.groupid = constructdetails.groupid(+)
			 ORDER BY  groupbydetails.systemid,constructid,groupname , grouptype;

		 /*
		 	 SELECT   groupbydetails.groupid groupid, groupbydetails.systemid, constructid
					, DECODE (constructname, NULL, 'Non-Construct', constructname) constructname, groupname, grouptype, qty
					, typename, isapproved, approvedby, currentp, adjustedp, listp, ad, vp, tw, asp, rulev, ruled
					, prigrpfl, priconsfl, demoflg, dfromdate , dtodate , dprice , effectype , effecdate, clog, active_fl, applvl
					, gm_pkg_sm_pricing_request.get_pricing_rule_by_id(v_requestid,constructid,'') pricing_rule_cons_id
					, gm_pkg_sm_pricing_request.get_pricing_rule_by_id(v_requestid,NULL,groupbydetails.groupid) pricing_rule_grp_id				 			
					
				 FROM (SELECT grp.groupid groupid, 
				 			systemid, groupname, grp.grouptype grouptype, grp.typename typename, isapproved
							, approvedby, 
							NVL(pr.currentp,gm_pkg_sm_pricing_request.get_group_currentp_req (p_acc_id, grp.groupid, NULL, v_gpo_id) ) currentp, 
							pr.adjustedp adjustedp, grp.prigrpfl prigrpfl, 
							NVL(LISTP,get_group_price ('', grp.groupid, 52060)) listp, ad, vp, 
							NVL(tw,get_group_price ('', grp.groupid, 52063)) tw, asp
							, rulev, ruled, pr.demoflg  demoflg, pr.dfromdate dfromdate, pr.dtodate dtodate, pr.dprice dprice, pr.clog clog
              , pr.effecdate effecdate , pr.effectype effectype,pr.active_fl active_fl,pr.applvl applvl
						 FROM (SELECT TO_CHAR (t4010.c4010_group_id) groupid, t4010.c207_set_id systemid
									, t4010.c4010_group_nm groupname, t4010.c901_group_type grouptype
									, get_code_name (t4010.c901_group_type) typename
									, t4010.c4010_principal_grp_fl prigrpfl
								 FROM t4010_group t4010
								WHERE TO_CHAR (t4010.c4010_group_id) IN (
										  SELECT TO_CHAR  (c7001_ref_id) groupid
											FROM t7001_group_part_pricing t7001
										   WHERE t7001.c7000_account_price_request_id = v_requestid
											 AND c901_ref_type = 52000
											 AND t7001.c901_status_fl is not null
											 AND t7001.C7001_VOID_FL IS NULL
                       and t7001.c7001_demo_flg = decode(v_requestid,null,null,0)
										  UNION ALL
										  SELECT TO_CHAR (t7004.c4010_group_id) groupid
											FROM t7004_construct_mapping t7004
										   WHERE t7004.c7003_system_construct_id IN (
													 SELECT t7004.c7003_system_construct_id
													   FROM t7004_construct_mapping t7004
													  WHERE t7004.c4010_group_id IN (
																SELECT c7001_ref_id groupid
																  FROM t7001_group_part_pricing t7001
																 WHERE t7001.c7000_account_price_request_id =
																											 v_requestid
																   AND c901_ref_type = 52000
																   AND c901_status_fl is not null
																   AND t7001.C7001_VOID_FL IS NULL
                                   and t7001.c7001_demo_flg = decode(v_requestid,null,null,0)))
                                   UNION
                                   --modified on 01/17 to get the part info in the approval while along with other system, group information.
                                   	SELECT c7001_ref_id
                                    from t7001_group_part_pricing where 
                                    c7000_account_price_request_id = v_requestid
                                    and c7001_ref_id in (
                                   		SELECT token groupid from v_in_list
                                   	)
                                   )
                                   
                                   ) grp
							, (SELECT t7001.c7001_ref_id groupid, t7001.c901_ref_type refnum
									, gm_pkg_sm_pricing_request.get_group_currentp_req (p_acc_id
																					  , c7001_ref_id
																					  , NULL, v_gpo_id
																					   ) currentp
									, t7001.c7001_adjusted_price adjustedp
									, get_group_price (v_requestid, c7001_ref_id, 52060) listp
									, get_group_price ('', c7001_ref_id, 52061) ad
									, get_group_price ('', c7001_ref_id, 52062) vp
									, get_group_price (v_requestid, c7001_ref_id, 52063) tw
									, gm_pkg_pd_group_pricing.get_group_avg_selling_price (t7001.c7001_ref_id) asp
									, t7001.c901_status_fl isapproved
									, get_user_name (t7001.c7001_approved_by) approvedby
									, get_rule_value (c7001_ref_id, 'Pricing-Rule') rulev
                  , to_char(t7001.c7001_effective_date,get_rule_value ('DATEFMT', 'DATEFORMAT')) effecdate
                  , get_code_name(t7001.c901_effective_type) effectype
									, get_rule_desc (c7001_ref_id, 'Pricing-Rule') ruled
                  , get_demo_price('Price',v_requestid,c7001_ref_id) dprice
                  , get_demo_price('Flag',v_requestid,c7001_ref_id) demoflg
                  , get_demo_data('FromDt',v_requestid,c7001_ref_id) dfromdate
                  , get_demo_data('ToDt',v_requestid,c7001_ref_id) dtodate
                  , get_log_flag (v_requestid || '-' || c7001_ref_id, 1252) clog
                  , c7001_active_fl active_fl
                  ,get_code_name(c901_aprl_lvl)  applvl
								 FROM t7001_group_part_pricing t7001, t4010_group t4010
								WHERE t7001.c7000_account_price_request_id = v_requestid
								AND t7001.c901_status_fl is not null
								AND t7001.C7001_VOID_FL IS NULL
					AND c901_ref_type = 52000 			
                  and t7001.c7001_demo_flg = decode(v_requestid,null,null,0)
                  AND (t7001.c7001_ref_id = t4010.c4010_group_id
                   AND t4010.c4010_group_id IN  (SELECT token groupid from v_in_list)
                   )) pr
						WHERE pr.groupid(+) = grp.groupid) groupbydetails
					, (SELECT t7004.c4010_group_id groupid, t7003.c207_set_id systemid
							, t7003.c7003_principal_fl priconsfl, t7004.c7003_system_construct_id constructid
							, t7003.c7003_construct_name constructname, t7004.c7004_qty qty
						 FROM t7004_construct_mapping t7004, t7003_system_construct t7003
						WHERE t7004.c7003_system_construct_id IN (
								  SELECT t7004.c7003_system_construct_id
									FROM t7004_construct_mapping t7004
								   WHERE t7004.c4010_group_id IN (
											 SELECT c7001_ref_id groupid
											   FROM t7001_group_part_pricing t7001
											  WHERE t7001.c7000_account_price_request_id = v_requestid
												AND c901_ref_type = 52000 and t7001.c7001_demo_flg = decode(v_requestid,null,null,0)
												AND t7001.c901_status_fl is not null
												AND t7001.C7001_VOID_FL IS NULL
												UNION  
												--SELECT token groupid from v_in_list
												--modified on 01/17 to get the part info in the approval while along with other system, group information.
			                                   	SELECT c7001_ref_id
			                                    from t7001_group_part_pricing where 
			                                    c7000_account_price_request_id = v_requestid
			                                    and c7001_ref_id in (
			                                   		SELECT token groupid from v_in_list
			                                   	)
												
												))
						  AND t7003.c7003_system_construct_id = t7004.c7003_system_construct_id
						  AND t7003.c7003_void_fl IS NULL
              AND t7003.c901_status = 52071) constructdetails
				WHERE groupbydetails.groupid = constructdetails.groupid(+)
			 ORDER BY  groupbydetails.systemid,constructid,groupname , grouptype; */
 			 --groupbydetails.systemid;
  gm_fch_part_pricing_anl (v_requestid, p_outdetailpart_cur);
	  BEGIN
		  SELECT C901_REQUEST_STATUS INTO v_status 
		  FROM T7000_ACCOUNT_PRICE_REQUEST 
		  WHERE c7000_account_price_request_id = p_requestid;
	  EXCEPTION  WHEN NO_DATA_FOUND
		  THEN
			v_status	:= '';
		END;
	  IF v_status != '52186' AND v_status IS NOT NULL
	  THEN
	  gm_pkg_sm_pricing_activity_log (v_requestid,'52223',p_userid); ---View-52223
	  END IF;
  END gm_fch_constrcut_pr_details;

	/******************************************************************
		Description : This procedure is used to fetch details of parts price for analysis view
	  ****************************************************************/
	PROCEDURE gm_fch_part_pricing_anl (
		p_requestid 		  IN	   t7000_account_price_request.c7000_account_price_request_id%TYPE
	  , p_outdetailpart_cur   OUT	   TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_outdetailpart_cur
		 FOR
			 SELECT   groupid, pr.pnum, pdesc, currentp, adjustedp, isapproved, approvedby
					, get_rule_value (pr.pnum, 'Pricing-Rule') rulev, get_rule_desc (pr.pnum, 'Pricing-Rule') ruled, pr.clog clog
					, pr.effectype effectype,pr.fromdate fromdate,pr.ad,pr.vp, pr.active_fl active_fl,pr.tw,pr.listp, pr.applvl applvl
				 FROM (SELECT c4010_group_id groupid, c205_part_number_id pnum
							, get_partnum_desc (c205_part_number_id) pdesc
						 FROM t4011_group_detail t4011) dtls
					, (SELECT t7001.c7001_ref_id pnum, c7001_current_price currentp
							, t7001.c7001_adjusted_price adjustedp, t7001.c901_status_fl isapproved
							, get_user_name (t7001.c7001_approved_by) approvedby
							, get_log_flag (p_requestid || '-' || c7001_ref_id, 1252) clog
							 , get_code_name(t7001.c901_effective_type) effectype
							 , TO_CHAR (t7001.c7001_effective_date, get_rule_value ('DATEFMT', 'DATEFORMAT')) fromdate
							 , GET_GROUP_PART_PRICE(c7001_ref_id, 52061) ad
              				 , GET_GROUP_PART_PRICE(c7001_ref_id,52062) vp
              				 --, GET_GROUP_PART_PRICE(c7001_ref_id,52063) tw
              				 --, GET_GROUP_PART_PRICE(c7001_ref_id, 52060) listp
              				, get_part_price(c7001_ref_id,'L') listp 
							, get_part_price(c7001_ref_id, 'E') tw
              				 , t7001.c7001_active_fl active_fl
              				 , get_code_name(t7001.c901_aprl_lvl) applvl
						 FROM t7001_group_part_pricing t7001
						WHERE t7001.c7000_account_price_request_id = p_requestid
						  AND c7001_void_fl IS NULL
						  AND t7001.c901_ref_type = 52001) pr
				WHERE pr.pnum = dtls.pnum(+)
			 ORDER BY groupid;
	
	END gm_fch_part_pricing_anl;

	
	/************************************************************************
		Description : Procedure to save the the pricing void pricing request
	 ***********************************************************************/
	
	PROCEDURE gm_save_void_pricing_request (
		p_requestid   IN	   t7000_account_price_request.c7000_account_price_request_id%TYPE
		, p_userid	  IN	   VARCHAR2
	)
	AS
		

	BEGIN
		
		DELETE FROM t7002_group_pricing_detail
		WHERE c7001_group_part_pricing_id IN (SELECT c7001_group_part_pricing_id  FROM  T7001_group_part_pricing
		WHERE c7000_account_price_request_id = p_requestid);
		
		UPDATE t7001_group_part_pricing SET c7001_void_fl ='Y' , c7001_last_updated_date=CURRENT_DATE,
			 	C7001_LAST_UPDATED_BY = p_userid
		WHERE c7000_account_price_request_id = p_requestid;
	
		UPDATE t9010_answer_data SET C9010_VOID_FL ='Y' , c9010_last_updated_date=CURRENT_DATE,
			 	c9010_last_updated_by = p_userid
		WHERE c9010_ref_id = p_requestid;
		
		UPDATE t7000_account_price_request SET C7000_VOID_FL ='Y' , C7000_LAST_UPDATED_DATE=CURRENT_DATE,
			 	C7000_LAST_UPDATED_BY = p_userid
		WHERE c7000_account_price_request_id = p_requestid;
		
END gm_save_void_pricing_request;
	
	/************************************************************************
		Description : Procedure to save the the denied pricing request
	 ***********************************************************************/
	
	PROCEDURE gm_save_denied_pricing_request (
		p_requestid 	  IN	   t7000_account_price_request.c7000_account_price_request_id%TYPE
	  , p_acc_id		  IN	   t704_account.c704_account_id%TYPE
	  , p_effectivedate   IN	   VARCHAR2
	  , p_inputstr		  IN	   VARCHAR2
	  , p_inputanswer	  IN	   VARCHAR2
	  , p_userid		  IN	   VARCHAR2
	  , p_status		  IN	   VARCHAR2
	  ,	p_txn_id		 IN   VARCHAR2
	  , p_cancelreason	 IN   t907_cancel_log.c901_cancel_cd%TYPE
	  , p_comments		 IN   t907_cancel_log.c907_comments%TYPE
	  , p_canceltype	 IN   t901_code_lookup.c902_code_nm_alt%TYPE
	  , p_request_type   IN   t7000_account_price_request.c901_request_type%TYPE
	  , p_gpo_id         IN   t7000_account_price_request. c101_gpo_id%TYPE
	  , p_denyinputstr	  IN	   VARCHAR2
	  , p_aprl_lvl        IN 	   t7000_account_price_request.c901_aprl_lvl%TYPE
	  , p_publishoverride IN 	   VARCHAR2
          , p_outrequestid	  OUT	   VARCHAR2
	  , p_outstatus 	  OUT	   VARCHAR2
	)
	AS
		v_strlen	   NUMBER := NVL (LENGTH (p_inputstr), 0);
		v_strDenlen	   NUMBER := NVL (LENGTH (p_denyinputstr), 0);
		
		v_account_id   t7000_account_price_request.c704_account_id%TYPE;
		v_refid       VARCHAR2(20);
		v_userid		 VARCHAR2(20);
		v_txnid        VARCHAR2(20);
		v_out    VARCHAR2(100);
		v_to_mail	   VARCHAR2 (2000);
		v_subject	   VARCHAR2 (2000);
		v_mail_body    VARCHAR2 (2000);
		v_group_nm   VARCHAR2(100);
		v_group_nm_all   VARCHAR2(4000);
		p_den_requestid VARCHAR2 (2000);
		v_mail_count NUMBER:=0;
		v_error_msg VARCHAR2 (500);
		v_removesysIds   VARCHAR2(4000);
		
	CURSOR cur_refid
    IS
		SELECT c7001_ref_id refid from T7001_group_part_pricing where
		c7000_account_price_request_id = p_den_requestid;
	BEGIN
		IF v_strlen > 0 THEN
			gm_save_pricing_request(p_requestid,p_acc_id,p_effectivedate,p_inputstr,p_inputanswer,p_userid,p_status, p_request_type, p_gpo_id, p_aprl_lvl,v_removesysIds,
									p_publishoverride,p_outrequestid, p_outstatus, v_error_msg);
		END IF;
		IF v_strDenlen > 0 THEN
			
			SELECT c704_account_id, c7000_created_by INTO v_account_id, v_userid 
			FROM t7000_account_price_request
			WHERE c7000_account_price_request_id = p_requestid;
			
			gm_save_pricing_request(p_requestid,v_account_id,p_effectivedate,p_denyinputstr,p_inputanswer,v_userid,'52186',  p_request_type, p_gpo_id, p_aprl_lvl,v_removesysIds,
									p_publishoverride,p_outrequestid,p_outstatus,v_error_msg);
			
			
			BEGIN	
				SELECT c7000_account_price_request_id INTO p_den_requestid 
				FROM t7000_account_price_request
				WHERE c704_account_id = v_account_id
				AND NVL(c101_gpo_id,'-999') = NVL(p_gpo_id,'-999')
				AND C901_REQUEST_STATUS  = 52186
				AND c7000_void_fl is null
				AND rownum =1;
				
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				p_den_requestid	:= null;
		END;
		 --UPDATE t7000_account_price_request SET C7000_MASTER_REQ_ID =p_requestid WHERE C7000_ACCOUNT_PRICE_REQUEST_ID = p_outrequestid;
		
			SELECT get_rule_value ('PRICING', 'EMAIL') INTO v_to_mail FROM DUAL;
			
			v_to_mail	:= get_user_emailid(v_userid)||','||v_to_mail;
			v_subject	:= 'Pricing Request Denied for Account: '||v_account_id ||' - '||GET_ACCOUNT_NAME(v_account_id);
			
			FOR var_ref IN cur_refid
    LOOP
    		v_refid := var_ref.refid;
    		v_txnid := p_den_requestid || '-' || v_refid;
    		
    		IF INSTR (v_refid, 'S') <> 0 THEN
    		   		SELECT c4010_group_nm	INTO v_group_nm
					FROM t4010_group WHERE c4010_group_id = get_part_groupid(v_refid);
					
			ELSE
			   		SELECT c4010_group_nm	INTO v_group_nm
					FROM t4010_group WHERE c4010_group_id = v_refid;
			END IF;
			
			gm_pkg_common_cancel.gm_sav_cancel_log(0,v_txnid,p_comments, p_cancelreason, 90202, p_userid);
    		
			v_txnid := p_den_requestid || '-' || v_refid;
			
			GM_UPDATE_LOG(v_txnid, p_comments, 1252,p_userid, v_out);
			
			v_group_nm_all:=v_group_nm_all||'<b>'||v_group_nm||'</b><br>';
			v_mail_count:=v_mail_count+1;
    	END LOOP;
    		IF v_mail_count=1 THEN
				v_mail_body:='The price request for group, <b>'||v_group_nm||'</b>, has been rejected by <b>'||get_user_name(p_userid)||'</b> due to the following reason: <b>'||p_comments||'</b>';
				v_group_nm_all:='';
			ELSIF v_mail_count>=2 THEN
				v_mail_body:='The price request for the following groups has been rejected by <b>'||get_user_name(p_userid)||'</b> due to the following reason: <b>'||p_comments||'</b><br><br>';
			END IF;
			v_mail_body := v_mail_body||v_group_nm_all;
			gm_com_send_html_email(v_to_mail, v_subject,'pricing', v_mail_body); 
		END IF;
	
	END gm_save_denied_pricing_request;

	/******************************************************************
			 Description : Procedure to set the request details
	****************************************************************/
	PROCEDURE gm_save_pricing_request (
		p_requestid 	  IN	   t7000_account_price_request.c7000_account_price_request_id%TYPE
	  , p_acc_id		  IN	   t704_account.c704_account_id%TYPE
	  , p_effectivedate   IN	   VARCHAR2
	  , p_inputstr		  IN	   CLOB
	  , p_inputanswer	  IN	   VARCHAR2
	  , p_userid		  IN	   VARCHAR2
	  , p_status		  IN	   VARCHAR2
	  , p_request_type    IN       t7000_account_price_request.c901_request_type%TYPE
	  , p_gpo_id          IN  	   t7000_account_price_request.c101_gpo_id%TYPE
      , p_aprl_lvl	      IN 	   t7000_account_price_request.c901_aprl_lvl%TYPE
      , p_removesysIds    IN 	   VARCHAR2
      , p_publishoverride IN 	   VARCHAR2
	  , p_outrequestid	  OUT	   VARCHAR2
	  , p_outstatus 	  OUT	   VARCHAR2
	  , v_error_msg 	  OUT	   VARCHAR2
	  
	)
	AS
		v_refid 	   VARCHAR2 (20);
		v_refids_all   CLOB;
		v_reftype	   t7001_group_part_pricing.c901_ref_type%TYPE;
		v_changetype   t7001_group_part_pricing.c901_change_type%TYPE;
		v_changevalue  t7001_group_part_pricing.c7001_change_value%TYPE;
		v_adjvalue	   t7001_group_part_pricing.c7001_adjusted_price%TYPE;
		v_isapproved   t7001_group_part_pricing.c901_status_fl%TYPE;
		v_out_pricedetail_id t7001_group_part_pricing.c7001_group_part_pricing_id%TYPE;
		v_strlen_req_grp_maplen NUMBER := NVL (LENGTH (p_inputstr), 0);
		v_req_map_string CLOB := p_inputstr;
		v_req_map_substring VARCHAR2 (1000);
		v_old_req_status NUMBER;
		v_acc_id	   t704_account.c704_account_id%TYPE;
		--mail part
		subject 	   VARCHAR2 (100);
		mail_body	   VARCHAR2 (3000);
		crlf		   VARCHAR2 (2) := CHR (13) || CHR (10);
		to_mail 	   t906_rules.c906_rule_value%TYPE;
		v_user_name    VARCHAR2 (100);
		v_dist_name    VARCHAR2 (100);
		v_demo_flg	   t7001_group_part_pricing.c7001_demo_flg%TYPE;
		v_demo_from_date VARCHAR2 (10);
   	v_demo_to_date VARCHAR2 (10);
    v_demo_changevalue  t7001_group_part_pricing.c7001_change_value%TYPE;
    v_demo_changetype t7001_group_part_pricing.c901_change_type%TYPE;
		v_demo_adjvalue	   t7001_group_part_pricing.c7001_adjusted_price%TYPE;
		v_effective_type t7001_group_part_pricing.c901_effective_type%TYPE;
		v_effective_date VARCHAR2 (10);
		v_active_fl t7001_group_part_pricing.c7001_active_fl%TYPE;
		--to count approved status
		v_not_approved_count NUMBER;
    v_approved_not_impl_count NUMBER;
    v_total_part_group NUMBER;
    v_approved_impl_count NUMBER;
    v_total_publish_status NUMBER;
    	v_applvl 				NUMBER;
 v_temp_req_id t7000_account_price_request.c7000_account_price_request_id%TYPE;
    v_skipdelete VARCHAR2(10):= 'NO';
    v_removesysIds VARCHAR(2000):= p_removesysIds;
    v_skipcount NUMBER;
    	v_matching_req_id		t7000_account_price_request.c7000_account_price_request_id%TYPE;
		v_matching_req_stat 	t7000_account_price_request.c901_request_status%TYPE;
		v_swap_req_id 			t7000_account_price_request.c7000_account_price_request_id%TYPE;
		v_new_req_id 			t7000_account_price_request.c7000_account_price_request_id%TYPE;
		v_aprl_lvl_before		NUMBER;
		v_aprl_lvl_after		NUMBER;
		v_req_qa_count			NUMBER;
		v_new_status            t7000_account_price_request.C901_REQUEST_STATUS%TYPE;
		v_gpo_id				t7000_account_price_request.c101_gpo_id%TYPE;
		v_userid                VARCHAR2(20);
		v_request_type          t7000_account_price_request.c901_request_type%TYPE;
		v_update_mas_req_id     VARCHAR2(1);
		v_mast_req_id 			t7000_account_price_request.C7000_MASTER_REQ_ID%TYPE;
		v_answer_cnt            NUMBER ;
		my_temp_txn_id 			VARCHAR2(4000);
		v_group_id t7001_group_part_pricing.c7001_ref_id%TYPE;
		v_count NUMBER;
		
	BEGIN
		p_outrequestid := p_requestid;
		p_outstatus := p_status;
		v_acc_id	:= p_acc_id;
		IF (p_outrequestid IS NOT NULL)
		THEN
			SELECT c901_request_status
			  INTO v_old_req_status
			  FROM t7000_account_price_request
			 WHERE c7000_account_price_request_id = p_outrequestid;

			SELECT c704_account_id
			  INTO v_acc_id
			  FROM t7000_account_price_request
			 WHERE c7000_account_price_request_id = p_outrequestid;
		ELSE
		
			gm_save_reqmaster_detail (p_outrequestid, v_acc_id, p_effectivedate, p_userid, p_request_type, p_gpo_id, p_aprl_lvl,p_publishoverride, p_outstatus);
		END IF;
		
		v_gpo_id := p_gpo_id; 
		BEGIN
				SELECT C101_GPO_ID,C7000_CREATED_BY,c901_request_type,C7000_MASTER_REQ_ID INTO v_gpo_id ,v_userid,v_request_type,v_mast_req_id
				FROM t7000_account_price_request WHERE c7000_account_price_request_id = p_outrequestid;
			EXCEPTION WHEN NO_DATA_FOUND THEN
				v_gpo_id:='';
		END;
		
		--loop through the input string for all the ref_id-Type-changetype-value-adj val-isapproved
		WHILE INSTR (v_req_map_string, '|') <> 0
		LOOP
			v_req_map_substring := SUBSTR (v_req_map_string, 1, INSTR (v_req_map_string, '|') - 1);
			v_req_map_string := SUBSTR (v_req_map_string, INSTR (v_req_map_string, '|') + 1);
			--
			v_group_id := NULL;
			v_refid 	:= NULL;
			v_reftype	:= NULL;
			v_changetype := NULL;
			v_changevalue := NULL;
			v_isapproved := NULL;
			v_out_pricedetail_id := 0;
			v_demo_flg	:= 0;
			v_demo_from_date := NULL;
			v_demo_to_date := NULL;
			v_effective_type := 0;
			v_effective_date := NULL;
      v_demo_changetype := NULL;
			v_demo_changevalue := NULL;
			v_applvl := 0;
			v_matching_req_stat:=NULL;
			v_swap_req_id := NULL;
		--	v_matching_req_id := NULL;
			v_answer_cnt := 0;
			--
			v_refid 	:= SUBSTR (v_req_map_substring, 1, INSTR (v_req_map_substring, '^') - 1);
			v_req_map_substring := SUBSTR (v_req_map_substring, INSTR (v_req_map_substring, '^') + 1);
			v_reftype	:= SUBSTR (v_req_map_substring, 1, INSTR (v_req_map_substring, '^') - 1);
			v_req_map_substring := SUBSTR (v_req_map_substring, INSTR (v_req_map_substring, '^') + 1);
			v_changetype := SUBSTR (v_req_map_substring, 1, INSTR (v_req_map_substring, '^') - 1);
			v_req_map_substring := SUBSTR (v_req_map_substring, INSTR (v_req_map_substring, '^') + 1);
			v_changevalue := SUBSTR (v_req_map_substring, 1, INSTR (v_req_map_substring, '^') - 1);
			v_req_map_substring := SUBSTR (v_req_map_substring, INSTR (v_req_map_substring, '^') + 1);
			v_adjvalue	:= SUBSTR (v_req_map_substring, 1, INSTR (v_req_map_substring, '^') - 1);
			v_req_map_substring := SUBSTR (v_req_map_substring, INSTR (v_req_map_substring, '^') + 1);
			v_isapproved := SUBSTR (v_req_map_substring, 1, INSTR (v_req_map_substring, '^') - 1);
			v_req_map_substring := SUBSTR (v_req_map_substring, INSTR (v_req_map_substring, '^') + 1);
      v_effective_type := SUBSTR (v_req_map_substring, 1, INSTR (v_req_map_substring, '^') - 1);
			v_req_map_substring := SUBSTR (v_req_map_substring, INSTR (v_req_map_substring, '^') + 1);
			v_effective_date := SUBSTR (v_req_map_substring, 1, INSTR (v_req_map_substring, '^') - 1);
			v_req_map_substring := SUBSTR (v_req_map_substring, INSTR (v_req_map_substring, '^') + 1);
      v_demo_flg	:= SUBSTR (v_req_map_substring, 1, INSTR (v_req_map_substring, '^') - 1);
			v_req_map_substring := SUBSTR (v_req_map_substring, INSTR (v_req_map_substring, '^') + 1);
      v_demo_changetype := SUBSTR (v_req_map_substring, 1, INSTR (v_req_map_substring, '^') - 1);
			v_req_map_substring := SUBSTR (v_req_map_substring, INSTR (v_req_map_substring, '^') + 1);
			v_demo_changevalue := SUBSTR (v_req_map_substring, 1, INSTR (v_req_map_substring, '^') - 1);
			v_req_map_substring := SUBSTR (v_req_map_substring, INSTR (v_req_map_substring, '^') + 1);
      v_demo_adjvalue	:= SUBSTR (v_req_map_substring, 1, INSTR (v_req_map_substring, '^') - 1);
			v_req_map_substring := SUBSTR (v_req_map_substring, INSTR (v_req_map_substring, '^') + 1);
			v_demo_from_date := SUBSTR (v_req_map_substring, 1, INSTR (v_req_map_substring, '^') - 1);
			v_req_map_substring := SUBSTR (v_req_map_substring, INSTR (v_req_map_substring, '^') + 1);
			v_demo_to_date :=  SUBSTR (v_req_map_substring, 1, INSTR (v_req_map_substring, '^') - 1);
     		v_req_map_substring := SUBSTR (v_req_map_substring, INSTR (v_req_map_substring, '^') + 1);
     		
     		v_active_fl :=  SUBSTR (v_req_map_substring, 1, INSTR (v_req_map_substring, '^') - 1);
     		v_req_map_substring := SUBSTR (v_req_map_substring, INSTR (v_req_map_substring, '^') + 1);
     		v_applvl :=  SUBSTR (v_req_map_substring, 1, INSTR (v_req_map_substring, '^') - 1);
     		v_req_map_substring := SUBSTR (v_req_map_substring, INSTR (v_req_map_substring, '^') + 1);
     		v_applvl :=  v_req_map_substring;
     		
     		IF v_isapproved 		 = '52150'	THEN	--SUBMIT_FOR_VP_APPROVAL - 52150
 				v_matching_req_stat := '52122';			--Pending VP Approval	 - 52122
			ELSIF v_isapproved 		 = '52151' 	THEN	--SUBMIT_FOR_PC_APPROVAL - 52151
 				v_matching_req_stat := '52123'; 		--Pending PC Approval	 - 52123
			ELSIF v_isapproved 	 	=  '52029'	THEN	--Denied				 - 52029
 				v_matching_req_stat := '52186'; 		--Initiated				 - 52186
 				v_isapproved 		:= '52186';
 				v_update_mas_req_id := 'Y'; -- WHEN A GROUP IS DENIED, WE NEED TO UPDATE THE MASTER REQ ID .
 		   END IF;
 			-- When the group is coming from a request which was denied earlier,then we need to move those groups(s) to another request
 			-- which is in Pending AD Approval.
 			IF v_mast_req_id IS NOT NULL
 			THEN
 				IF v_isapproved = '52027'	THEN	--Pending
 					v_matching_req_stat := '52187';		--Pending AD Approval
 				END IF;
 				
 			END IF;
 			
 			IF v_matching_req_stat IS NOT NULL 
 			THEN
 				v_matching_req_id := GET_PRICING_REQUEST_ID(v_acc_id,v_gpo_id, v_matching_req_stat,'Y');

	 			IF v_matching_req_id IS NULL
				THEN
					gm_save_reqmaster_detail (v_matching_req_id, v_acc_id, p_effectivedate, v_userid,v_request_type, v_gpo_id, p_aprl_lvl,p_publishoverride, v_matching_req_stat);
										
				END IF;
				
				IF (v_matching_req_id != p_outrequestid)
				THEN
		  			v_swap_req_id := NVL(v_matching_req_id,'');
		  			
		  			SELECT COUNT(*) INTO v_answer_cnt FROM t9010_answer_data
		  			WHERE c9010_ref_id = v_swap_req_id AND C9010_VOID_FL IS NULL;
		  			
		  			-- WHEN THERE ARE NO QUESTIONS FOR THE SWAP REQUEST, ONLY THEN UPDATE THE ANSWERS. 
		  			IF v_answer_cnt = 0
		  			THEN
			  			UPDATE t9010_answer_data set c9010_ref_id = v_swap_req_id
								, c9010_last_updated_by = p_userid
								, c9010_last_updated_date = CURRENT_DATE
								WHERE c9010_ref_id = p_outrequestid;
					END IF;		
					-- When a new req is created and if its for a denied req, then the master req id shd be updated.
					IF v_update_mas_req_id ='Y'
					THEN
						v_update_mas_req_id := '';
 						UPDATE t7000_account_price_request SET C7000_MASTER_REQ_ID =p_outrequestid 
 						WHERE C7000_ACCOUNT_PRICE_REQUEST_ID = v_matching_req_id;
					END IF;
					
				END IF;
			END IF;
 	
			--gm_procedure_log('match:'||v_matching_req_id||'-- 2 v_isapproved','Req_Sta'||v_matching_req_stat||' , '||p_outrequestid ||', Swap:'||v_swap_req_id||', Ref:'||v_refid);

			gm_save_reqprice_detail (p_outrequestid
                                   , v_acc_id
                                   , v_refid
                                   , v_reftype
                                   , v_changetype
                                   , v_changevalue
                                   , v_adjvalue
                                   , p_userid
                                   , v_isapproved
                                   , v_effective_type
                                   , v_effective_date
                                   , NULL
                                   , 0
                                   , v_active_fl
                                    , v_applvl
                                   , v_gpo_id
                                   , v_swap_req_id
                                   , v_out_pricedetail_id
									);
	-- start code MNTTASK-6115 - When denied part will be displayed in Approval Tab. Modified by velu 10/23/12
		IF v_swap_req_id IS NOT NULL AND v_reftype = 52001  THEN 
			v_group_id := get_part_groupid(v_refid); 
		END IF; 
		
		IF v_swap_req_id IS NOT NULL THEN
			
				SELECT COUNT(1) INTO v_count FROM t7001_group_part_pricing 
				WHERE c7000_account_price_request_id = v_swap_req_id 
				AND c901_ref_type = 52000 AND c7001_ref_id = v_group_id;
			
			IF v_count = 0 THEN
				gm_save_reqprice_detail (v_swap_req_id, v_acc_id, v_group_id, 52000, '', '', '', p_userid, '', '', '', NULL, 0, '' , '', '', v_swap_req_id, v_out_pricedetail_id);
			end if;
		END IF; 
-- end code									
      --for groups save the list, ad,vp,tw prices that used while requesting
			IF (v_reftype = 52000)
			THEN
				gm_save_base_price_details (NVL(v_swap_req_id,p_outrequestid), v_out_pricedetail_id, p_effectivedate, v_refid);
			END IF;
      IF ( v_demo_flg = 1 )
      THEN
            gm_save_reqprice_detail (p_outrequestid
                                   , v_acc_id
                                   , v_refid
                                   , v_reftype
                                   , v_demo_changetype
                                   , v_demo_changevalue
                                   , v_demo_adjvalue
                                   , p_userid
                                   , v_isapproved
                                   , v_effective_type
                                   , v_demo_from_date
                                   , v_demo_to_date
                                   , v_demo_flg
                                   , v_active_fl
                                   , v_applvl
                                   , v_gpo_id
                                    ,v_swap_req_id
                                   , v_out_pricedetail_id
									);
      END IF;
			
			

			v_refids_all := v_refids_all || v_refid || ',';
    	--if the group is approved save the account group pricing
			IF (v_old_req_status IS NOT NULL AND (p_outstatus != 52186) AND v_isapproved = 52028)
			THEN
        
           IF( v_demo_flg = 1)
               THEN
                 gm_save_account_group_price (p_outrequestid
                                 , v_acc_id
                                 , v_demo_from_date
                                 , v_demo_to_date
                                 , p_userid
                                 , v_refid
                                 , v_reftype
                                 , v_demo_adjvalue
                                 , v_active_fl
                                 , v_gpo_id
                                 , p_publishoverride
                                );
               END IF;
               IF v_effective_type=53000 and v_demo_flg=1
               THEN
                   v_effective_date:=TO_CHAR((to_date(v_demo_to_date,get_rule_value ('DATEFMT', 'DATEFORMAT'))+1),get_rule_value ('DATEFMT', 'DATEFORMAT'));
               ELSIF v_effective_type=53000 and v_demo_flg=0
               THEN
                   v_effective_date:=TO_CHAR(SYSDATE,get_rule_value ('DATEFMT', 'DATEFORMAT'));
               END IF;
               -- this call is to update the effective date into t7001 table for the type immidiate
                gm_save_reqprice_detail (p_outrequestid
                                   , v_acc_id
                                   , v_refid
                                   , v_reftype
                                   , v_changetype
                                   , v_changevalue
                                   , v_adjvalue
                                   , p_userid
                                   , v_isapproved
                                   , v_effective_type
                                   , v_effective_date
                                   , NULL
                                   , 0
                                   , v_active_fl
                                   , v_applvl
                                   , v_gpo_id
                                   , v_swap_req_id
                                   , v_out_pricedetail_id
									);
                gm_save_account_group_price (p_outrequestid
                               , v_acc_id
                               , v_effective_date
                               , NULL
                               , p_userid
                               , v_refid
                               , v_reftype
                               , v_adjvalue
                               , v_active_fl
                               , v_gpo_id
                               , p_publishoverride
                              );
          
			END IF;
		END LOOP;
		
		gm_save_request_answer (p_outrequestid, p_inputanswer, p_userid);
		my_context.set_my_inlist_ctx (v_refids_all);


		--on save check if all the groups/parts are approved then chnage the request status to Approved
		--if the request status is just initiate or send for approval or pending for review should not do this operation
   
		SELECT C901_APRL_LVL INTO v_aprl_lvl_before FROM T7000_ACCOUNT_PRICE_REQUEST 
		WHERE c7000_account_price_request_id = p_outrequestid;
	--	v_aprl_lvl_before:=52200;
		gm_chk_request_construct_price(p_outrequestid);
		
		SELECT C901_APRL_LVL INTO v_aprl_lvl_after FROM T7000_ACCOUNT_PRICE_REQUEST 
		WHERE c7000_account_price_request_id = p_outrequestid;
		--v_aprl_lvl_after:=52201;
		SELECT COUNT(*) INTO v_req_qa_count
		FROM t9010_answer_data
		WHERE c9010_ref_id = p_outrequestid AND c901_ref_type = 52130 AND c9010_void_fl IS NULL;
	--	v_req_qa_count:=0;
	
		IF (v_aprl_lvl_before <> v_aprl_lvl_after AND v_req_qa_count = 0)
		THEN
  			v_error_msg := 'Few Groups in request is having PC as approval level,please fill in Question and Answer section';
		END IF;
		IF (v_old_req_status IS NOT NULL)
		THEN
    --TOTAL NUMBER OF GROUP/PART FOR A REQUEST
			SELECT COUNT(1)
        INTO v_total_part_group
        FROM t7001_group_part_pricing
			 WHERE c7000_account_price_request_id = p_requestid
			   AND c7001_adjusted_price IS NOT NULL
			    AND c901_status_fl  IS NOT NULL
			    AND C7001_VOID_FL IS NULL
         AND c7001_demo_flg = 0;
    --TOTAL NUMBER OF NOT APPROVED GROUP/PART
      SELECT COUNT (1)
			  INTO v_not_approved_count
			  FROM t7001_group_part_pricing
			 WHERE c7000_account_price_request_id = p_requestid
			    AND c901_status_fl != 52028
			    AND C7001_VOID_FL IS NULL
          		AND c901_status_fl  IS NOT NULL
			   AND c7001_adjusted_price IS NOT NULL
         AND c7001_demo_flg = 0;
     --TOTAL NUMBER OF APPROVED AND IMPLEMENTED GROUP/PART
      SELECT COUNT (1)
			  INTO v_approved_not_impl_count
			  FROM t7001_group_part_pricing
			 WHERE c7000_account_price_request_id = p_requestid
			    AND c901_status_fl = 52028
        		AND c901_status_fl  IS NOT NULL
			   AND c7001_adjusted_price IS NOT NULL
			    AND c7001_active_fl != 'Y'
			    AND C7001_VOID_FL IS NULL
         AND c7001_demo_flg = 0
         AND TRUNC(SYSDATE) <= c7001_effective_date;
     --TOTAL NUMBER OF APPROVED AND IMPLEMENTED GROUP/PART
      SELECT COUNT (1)
			  INTO v_approved_impl_count
			  FROM t7001_group_part_pricing
			 WHERE c7000_account_price_request_id = p_requestid
			    AND c901_status_fl = 52028
        		AND c901_status_fl  IS NOT NULL		
			   AND c7001_adjusted_price IS NOT NULL
			   AND c7001_active_fl = 'Y'
			   AND C7001_VOID_FL IS NULL
         AND c7001_demo_flg = 0
         AND TRUNC(SYSDATE) >= c7001_effective_date;
         
       --TOTAL NUMBER OF PUBLISH STATUS GROUP/PART FOR A REQUEST
			SELECT COUNT(1)
        INTO v_total_publish_status
        FROM t7001_group_part_pricing
			WHERE c7000_account_price_request_id =  p_requestid
		    AND c7001_adjusted_price IS NOT NULL
        	AND c7001_demo_flg = 0
        	AND C7001_VOID_FL IS NULL
        	AND c7001_active_fl = 'Y';
         
        
   ----------------------------------------------------------------------------------------      
      IF(v_not_approved_count=v_total_part_group and p_status = 52187)
      THEN
        --update the status to pending approval which is 52187
        UPDATE t7000_account_price_request
				   SET c7000_last_updated_by = p_userid
					 , c7000_last_updated_date = CURRENT_DATE
					 , c901_request_status = TO_NUMBER (52187)
					 , C7000_PUBLISH_OVERRIDE_FL = p_publishoverride
				 WHERE c7000_account_price_request_id = p_requestid;
      ELSIF ((v_approved_not_impl_count+v_approved_impl_count) = v_total_part_group and v_approved_not_impl_count > 0)
			THEN
				--update the status to partially approved
				UPDATE t7000_account_price_request
				   SET c7000_last_updated_by = p_userid
					 , c7000_last_updated_date = CURRENT_DATE
					 , c901_request_status = TO_NUMBER (52189)
					 , C7000_PUBLISH_OVERRIDE_FL = p_publishoverride
				 WHERE c7000_account_price_request_id = p_requestid; 
      ELSIF (v_approved_impl_count = v_total_part_group AND v_total_part_group = v_total_publish_status)
			THEN
				--update the status to partially approved
				UPDATE t7000_account_price_request
				   SET c7000_last_updated_by = p_userid
					 , c7000_last_updated_date = CURRENT_DATE					 
					 , c901_request_status = TO_NUMBER (52190)
					 , C7000_PUBLISH_OVERRIDE_FL = p_publishoverride
				 WHERE c7000_account_price_request_id = p_requestid;    
			END IF;
		END IF;  -- end if of 	IF (v_old_req_status IS NOT NULL)
	   
		BEGIN
		 SELECT C901_REQUEST_STATUS INTO v_new_status 
	     FROM T7000_ACCOUNT_PRICE_REQUEST 
	     WHERE c7000_account_price_request_id = p_requestid;
		EXCEPTION WHEN NO_DATA_FOUND
			THEN
				v_new_status:='';
		END;
		
		IF v_old_req_status <> v_new_status AND v_new_status !='52186' AND v_new_status IS NOT NULL
		THEN
			gm_pkg_sm_pricing_activity_log (p_requestid,v_new_status,p_userid);
		ELSE
			gm_pkg_sm_pricing_activity_log (p_requestid,'52222',p_userid); --52222 for T909 Modified
        END IF;
		
        gm_chk_pricing_req_group(p_outrequestid,v_swap_req_id,p_userid);
        gm_pkg_sm_pricing_rules.gm_sav_pricing_rules(p_outrequestid);
       
      	DELETE FROM my_temp_list;
      	
      	--While delete the systems and save or submit will be deleted system. 
		WHILE INSTR (v_removesysIds, ',') <> 0
        LOOP
			my_temp_txn_id	:= SUBSTR (v_removesysIds, 1, INSTR (v_removesysIds, ',') - 1);
			v_removesysIds := SUBSTR (v_removesysIds, INSTR (v_removesysIds, ',') + 1);
    		INSERT INTO my_temp_list VALUES (my_temp_txn_id);
		END LOOP;
		
        gm_pricing_remove_systems(p_outrequestid);
        
	END gm_save_pricing_request;
	
	
	 /*********************************************************************
	  Description : Procedure to check if the groups/parts are available in the request. if no group available, void the request.
	  Author : Rajeshwaran
	**********************************************************************/
	PROCEDURE gm_chk_pricing_req_group(
		p_outrequestid	  IN OUT   t7000_account_price_request.c7000_account_price_request_id%TYPE
		,p_swap_req_id	  IN    t7000_account_price_request.c7000_account_price_request_id%TYPE
		, p_userid		  IN	   VARCHAR2
	)
	AS
		v_count NUMBER;
	BEGIN
		
		--CHECK HOW MANY GROUP ARE AVAILABLE IN THE REQUEST THAT IS ACTIVE.
		SELECT COUNT(*) INTO v_count FROM T7001_GROUP_PART_PRICING
		WHERE C7001_VOID_FL IS NULL AND NVL(C7001_ADJUSTED_PRICE,-1) >0 
		AND C901_STATUS_FL IS NOT NULL
		AND c7000_account_price_request_id = p_outrequestid;
		
		-- IF NO GROUP/PART IS AVAILABLE, THEN VOID THE REQUEST. 
		IF v_count = 0 THEN
				UPDATE T7000_ACCOUNT_PRICE_REQUEST SET C7000_VOID_FL = 'Y',C7000_LAST_UPDATED_BY=p_userid
				,C7000_LAST_UPDATED_DATE = CURRENT_DATE
				WHERE c7000_account_price_request_id = p_outrequestid AND C7000_VOID_FL IS NULL;
				
				UPDATE T7001_GROUP_PART_PRICING  SET C7001_VOID_FL='Y'
				,C7001_LAST_UPDATED_BY = p_userid
				,C7001_LAST_UPDATED_DATE = CURRENT_DATE
				WHERE c7000_account_price_request_id = p_outrequestid;
				
				p_outrequestid := p_swap_req_id;
		END IF;
	END gm_chk_pricing_req_group;
		/******************************************************************
			 Description : Procedure to set the request master details
	****************************************************************/
	PROCEDURE gm_save_reqmaster_detail (
		p_outrequestid	  IN OUT   t7000_account_price_request.c7000_account_price_request_id%TYPE
	  , p_acc_id		  IN	   t704_account.c704_account_id%TYPE
	  , p_effectivedate   IN	   VARCHAR2
	  , p_userid		  IN	   VARCHAR2
	  , p_request_type    IN       t7000_account_price_request.c901_request_type%TYPE
	  , p_gpo_id          IN       t7000_account_price_request. c101_gpo_id%TYPE
      , p_aprl_lvl     IN     t7000_account_price_request.c901_aprl_lvl%TYPE
      , p_publishoverride IN  T7000_ACCOUNT_PRICE_REQUEST.C7000_PUBLISH_OVERRIDE_FL%TYPE
	  , p_outreq_status   IN OUT   VARCHAR2	  
	)
	AS
	BEGIN
		UPDATE t7000_account_price_request
		   SET c7000_effective_date = TO_DATE (p_effectivedate, get_rule_value ('DATEFMT', 'DATEFORMAT'))
			 , c7000_last_updated_by = p_userid
			 , c7000_last_updated_date = CURRENT_DATE
			 , c901_request_status = TO_NUMBER (p_outreq_status)
			 , c901_aprl_lvl = TO_NUMBER (p_aprl_lvl)
			 , c101_gpo_id = TO_NUMBER (p_gpo_id)	
			 , C7000_PUBLISH_OVERRIDE_FL = p_publishoverride 
		 WHERE c7000_account_price_request_id = p_outrequestid;

		IF (SQL%ROWCOUNT = 0)
		THEN
			p_outrequestid := get_next_price_request_id (p_outrequestid);

			IF (p_outreq_status IS NULL)
			THEN
				p_outreq_status := 52186;	--52120
			END IF;

			INSERT INTO t7000_account_price_request
						(c7000_account_price_request_id, c704_account_id, c7000_created_by, c7000_created_date
					   , c901_request_status, c901_request_type, c101_gpo_id, c901_aprl_lvl,C7000_PUBLISH_OVERRIDE_FL
						)
				 VALUES (p_outrequestid, p_acc_id, p_userid, CURRENT_DATE
					   , p_outreq_status, p_request_type, p_gpo_id, p_aprl_lvl,p_publishoverride
						);
		END IF;
		gm_pkg_sm_pricing_activity_log (p_outrequestid,p_outreq_status,p_userid); --Here Status is 52186-Initiated(on save).
	END gm_save_reqmaster_detail;

	/******************************************************************
	Description : Procedure to set the request price details
	****************************************************************/
	PROCEDURE gm_save_reqprice_detail (
		p_requestid 		   IN		t7000_account_price_request.c7000_account_price_request_id%TYPE
	  , p_acc_id			   IN		t704_account.c704_account_id%TYPE
	  , p_ref_id			   IN		t7001_group_part_pricing.c7001_ref_id%TYPE
	  , p_reftype			   IN		t7001_group_part_pricing.c901_ref_type%TYPE
	  , p_change_type		   IN		t7001_group_part_pricing.c901_change_type%TYPE
	  , p_change_value		   IN		t7001_group_part_pricing.c7001_change_value%TYPE
	  , p_ajustedprice		   IN		t7001_group_part_pricing.c7001_adjusted_price%TYPE
	  , p_userid			   IN		VARCHAR2
	  , p_statusid			   IN		t7001_group_part_pricing.c901_status_fl%TYPE
    , p_effective_type  IN t7001_group_part_pricing.c901_effective_type%TYPE
    , p_effective_from_date IN VARCHAR2
    , p_effective_to_date IN VARCHAR2
    , p_demo_flg			   IN		t7001_group_part_pricing.c7001_demo_flg%TYPE
    , p_active_fl 		   IN 		t7001_group_part_pricing.c7001_active_fl%TYPE
      , p_applvl			   IN 		t7001_group_part_pricing.C901_APRL_LVL%TYPE
      , p_gpo_id           IN       t7000_account_price_request. c101_gpo_id%TYPE
      , p_swap_req_id 		   IN		t7000_account_price_request.c7000_account_price_request_id%TYPE
	  , p_out_pricedetail_id   OUT		t7001_group_part_pricing.c7001_group_part_pricing_id%TYPE
	)
	AS
		v_group_part_pricing_id NUMBER;
	BEGIN
		UPDATE t7001_group_part_pricing
		   SET c901_change_type = p_change_type
			 , c7001_change_value = p_change_value
			 , c7001_adjusted_price = p_ajustedprice
			 , c7001_last_updated_by = p_userid
			 , c7001_last_updated_date = CURRENT_DATE
			 , c901_status_fl = DECODE (p_statusid, '', '', p_statusid)
			 , c7001_approved_by =
							   DECODE (p_statusid
									 , 52186, NULL
									 , DECODE (p_statusid, 52028, p_userid, c7001_approved_by)
									  )
			 , c7001_demo_flg = p_demo_flg
       , c7001_effective_date = TO_DATE(p_effective_from_date,get_rule_value ('DATEFMT', 'DATEFORMAT'))
       , c901_effective_type =  p_effective_type
       , c7001_active_fl = p_active_fl
       , c901_aprl_lvl = p_applvl
       , c7000_account_price_request_id = NVL(p_swap_req_id,c7000_account_price_request_id)
		 WHERE c7000_account_price_request_id = p_requestid AND c7001_ref_id = p_ref_id 
     AND c901_ref_type = p_reftype
     AND c7001_demo_flg = p_demo_flg
     and c7001_void_fl IS NULL;

		IF (SQL%ROWCOUNT = 0)
		THEN
			SELECT s7001_group_part_pricing.NEXTVAL
			  INTO v_group_part_pricing_id
			  FROM DUAL;
			
			p_out_pricedetail_id := v_group_part_pricing_id;

			INSERT INTO t7001_group_part_pricing
						(c7001_group_part_pricing_id, c7000_account_price_request_id, c7001_ref_id, c901_ref_type
					   , c901_change_type, c7001_change_value, c7001_adjusted_price, c7001_created_by
					   , c7001_created_date, c901_status_fl, c7001_current_price, c7001_demo_flg 
             		, c901_effective_type, c7001_effective_date, c7001_effective_to_date,c7001_active_fl,c901_aprl_lvl
					   )
				 VALUES (v_group_part_pricing_id, p_requestid, p_ref_id, p_reftype
					   , p_change_type, p_change_value, p_ajustedprice, p_userid
					   , CURRENT_DATE, p_statusid,
					   -- For ref type part, get the current price from the function.
					   DECODE(p_reftype,52001,get_account_part_pricing(p_acc_id,p_ref_id,p_gpo_id),''), p_demo_flg
             , p_effective_type, TO_DATE (p_effective_from_date, get_rule_value ('DATEFMT', 'DATEFORMAT')), decode(p_effective_to_date,null,null,TO_DATE (p_effective_to_date, get_rule_value ('DATEFMT', 'DATEFORMAT')))
             ,p_active_fl,p_applvl
					   );
		ELSE
			SELECT c7001_group_part_pricing_id
			  INTO v_group_part_pricing_id
			  FROM t7001_group_part_pricing
			 WHERE c7000_account_price_request_id = NVL(p_swap_req_id,p_requestid) AND c7001_ref_id = p_ref_id
				   AND c901_ref_type = p_reftype AND c7001_demo_flg = p_demo_flg AND C7001_void_fl IS NULL;
	      -- gm_pkg_sm_pricing_activity_log (p_requestid,'52222',p_userid); --52222 for T909 Modified
			p_out_pricedetail_id := v_group_part_pricing_id;
		END IF;
	END gm_save_reqprice_detail;

	/******************************************************************
		Description : Procedure to set the request's ad, vp, list, trip wire price details
	  ****************************************************************/
	PROCEDURE gm_save_base_price_details (
		p_requestid 	   IN	t7000_account_price_request.c7000_account_price_request_id%TYPE
	  , p_pricedetail_id   IN	t7001_group_part_pricing.c7001_group_part_pricing_id%TYPE
	  , p_effectivedate    IN	VARCHAR2
	  , p_ref_id		   IN	t7001_group_part_pricing.c7001_ref_id%TYPE
	)
	AS
		v_pricedetail_id t7001_group_part_pricing.c7001_group_part_pricing_id%TYPE;
	BEGIN
		UPDATE t7002_group_pricing_detail
		   SET c7002_price = get_group_price_dtls (p_requestid, p_ref_id, 52060)
		 WHERE c7001_group_part_pricing_id = p_pricedetail_id AND c901_price_type = 52060;

		IF (SQL%ROWCOUNT = 0)
		THEN
			INSERT INTO t7002_group_pricing_detail
						(c7002_group_pricing_detail_id, c7001_group_part_pricing_id
					   , c7002_price, c901_price_type
						)
				 VALUES (s7002_group_pricing_detail.NEXTVAL, p_pricedetail_id
					   , get_group_price_dtls ('', p_ref_id, 52060), 52060
						);
		END IF;

		UPDATE t7002_group_pricing_detail
		   SET c7002_price = get_group_price_dtls (p_requestid, p_ref_id, 52063)
		 WHERE c7001_group_part_pricing_id = p_pricedetail_id AND c901_price_type = 52063;

		IF (SQL%ROWCOUNT = 0)
		THEN
			INSERT INTO t7002_group_pricing_detail
						(c7002_group_pricing_detail_id, c7001_group_part_pricing_id
					   , c7002_price, c901_price_type
						)
				 VALUES (s7002_group_pricing_detail.NEXTVAL, p_pricedetail_id
					   , get_group_price_dtls ('', p_ref_id, 52063), 52063
						);
		END IF;
	END gm_save_base_price_details;

	/******************************************************************
		Description : Update the price request details to account group pricing once all are approved
	****************************************************************/
	PROCEDURE gm_save_account_group_price (
		p_requestid 	  IN   t7000_account_price_request.c7000_account_price_request_id%TYPE
	  , p_acc_id		  IN   t704_account.c704_account_id%TYPE
	  , p_effectivedate   IN   VARCHAR2
      , p_effectivetodate IN   VARCHAR2
	  , p_userid		  IN   VARCHAR2
	  , p_ref_id		  IN   t7001_group_part_pricing.c7001_ref_id%TYPE
	  , p_reftype		  IN   t7001_group_part_pricing.c901_ref_type%TYPE
	  , p_ajustedprice	  IN   t7001_group_part_pricing.c7001_adjusted_price%TYPE
	  , p_active_fl 	  IN   t7051_account_group_pricing.c7051_active_fl%TYPE
	  , p_gpo_id          IN   t7000_account_price_request. c101_gpo_id%TYPE
	  , p_publishoverride IN   VARCHAR2
	)
	AS
		v_account_group_pricing_id NUMBER;
		v_count 	   NUMBER;
		v_acc_id t704_account.c704_account_id%TYPE;
		v_acc_grp_prc_id t7051_account_group_pricing.c7051_account_group_pricing_id%TYPE;
	BEGIN
		SELECT DECODE(p_gpo_id,'',p_acc_id,'') INTO v_acc_id FROM DUAL;
		
		SELECT COUNT (1)
		  INTO v_count
		  FROM t7051_account_group_pricing t7051
		 WHERE t7051.c7051_ref_id = p_ref_id 
		 AND c7051_VOID_FL IS NULL
		 AND NVL(c704_account_id,'-999') = NVL(v_acc_id,'-999')
		 AND NVL(C101_GPO_ID,'-999') = NVL(p_gpo_id,'-999')
		 AND c901_ref_type = p_reftype;
		
		 
		 
		IF (v_count =1)
		THEN
			SELECT c7051_account_group_pricing_id
		  INTO v_acc_grp_prc_id
		  FROM t7051_account_group_pricing t7051
		 WHERE t7051.c7051_ref_id = p_ref_id 
		 AND c7051_VOID_FL IS NULL
		 AND NVL(c704_account_id,'-999') = NVL(v_acc_id,'-999')
		 AND NVL(C101_GPO_ID,'-999') = NVL(p_gpo_id,'-999')
		 AND c901_ref_type = p_reftype;
		 
			UPDATE t7051_account_group_pricing
				   SET c7051_price = p_ajustedprice
					 , c7051_last_updated_by = p_userid
					 , c7051_last_updated_date = CURRENT_DATE
					 , c7051_active_fl = p_active_fl
					 , C7000_LAST_UPDATED_TXN_ID = p_requestid
				 WHERE c7051_account_group_pricing_id = v_acc_grp_prc_id
				 AND c7051_VOID_FL IS NULL;
		
		
		ELSIF v_count =0 THEN
		SELECT s7051_account_group_pricing.NEXTVAL
		  INTO v_account_group_pricing_id
		  FROM DUAL;

		INSERT INTO t7051_account_group_pricing
					(c7051_account_group_pricing_id, C7000_LAST_UPDATED_TXN_ID, c704_account_id, c7051_ref_id
				   , c901_ref_type, c7051_price, c7051_created_by, c7051_created_date,c7051_active_fl, c101_gpo_id
					)
			 VALUES (v_account_group_pricing_id, p_requestid, v_acc_id, p_ref_id
				   , p_reftype, p_ajustedprice
				   , p_userid, CURRENT_DATE
				   , p_active_fl, p_gpo_id
					);
			END IF;
			
			-- Update the current price with adjusted price once the price is approved and made active. 
			UPDATE t7001_group_part_pricing SET C7001_CURRENT_PRICE=p_ajustedprice
			WHERE C7000_ACCOUNT_PRICE_REQUEST_ID =p_requestid AND  c7001_ref_id = p_ref_id
			AND c901_ref_type = p_reftype AND C7001_VOID_FL IS NULL AND C7001_ACTIVE_FL = 'Y' ;
					
			--When the request has publish override flag, then the pricing cannot be made active.
					IF (NVL(p_active_fl,'N')='Y' AND NVL(p_publishoverride,'N') = 'N')
					THEN
						gm_correct_account_pricing(p_ref_id , p_reftype,v_acc_id , p_gpo_id,v_account_group_pricing_id,p_userid);
					
					END IF;
	END gm_save_account_group_price;
	
	/******************************************************************
	 Description : This Procedure will update the part price for the Account / GPO .
	 Author : Rajeshwaran.v
	 ****************************************************************/
	PROCEDURE gm_correct_account_pricing (
		p_ref_id  IN t7051_account_group_pricing.c7051_ref_id%TYPE
		,p_reftype IN t7051_account_group_pricing.c901_ref_type%TYPE
		,p_acc_id IN t7051_account_group_pricing.c704_account_id%TYPE
		,p_gpo_id IN t7051_account_group_pricing.c101_gpo_id%TYPE
		,p_acc_group_pricing_id  IN t7051_account_group_pricing.c7051_account_group_pricing_id%TYPE
		,p_userid IN VARCHAR2
	)
	AS
		
	BEGIN
		--gm_procedure_log('correction',p_ref_id||','|| p_reftype ||','|| p_acc_id ||','|| p_gpo_id ||','|| p_acc_group_pricing_id);
		IF p_reftype = '52001' THEN -- PART
		
			UPDATE 
				T705_ACCOUNT_PRICING SET C705_VOID_FL  = 'Y'
				,C7051_ACCOUNT_GROUP_PRICING_ID = p_acc_group_pricing_id
				,c705_last_updated_by = p_userid
				,c705_last_updated_date = CURRENT_DATE
				
			WHERE 
				(C704_ACCOUNT_ID  = p_acc_id OR
				C101_PARTY_ID = p_gpo_id) AND
				C205_PART_NUMBER_ID = p_ref_id AND
				C705_VOID_FL IS NULL;
			
		ELSIF p_reftype = '52000' THEN -- GROUP
		
			UPDATE 
				T705_ACCOUNT_PRICING SET C705_VOID_FL  = 'Y'
				,C7051_ACCOUNT_GROUP_PRICING_ID = p_acc_group_pricing_id
				,c705_last_updated_by = p_userid
				,c705_last_updated_date = CURRENT_DATE
			WHERE 
				(C704_ACCOUNT_ID  = p_acc_id OR
				C101_PARTY_ID = p_gpo_id) AND 
				C705_VOID_FL IS NULL AND
				C205_PART_NUMBER_ID IN ( 
				SELECT t4011.c205_part_number_id  pnum FROM t4011_group_detail t4011, t4010_group t4010
					 WHERE t4010.c4010_group_id = t4011.c4010_group_id
					   AND t4010.c4010_void_fl IS NULL
					   AND t4010.c901_type = 40045
					  -- AND t4010.c4010_visible_ac_fl IS NOT NULL
					   AND t4010.c4010_group_id = p_ref_id
					   AND t4011.c901_part_pricing_type = 52080);  --primary part			
		END IF;
	
	END gm_correct_account_pricing;
	
	/******************************************************************
			 Description : This Fucntion is to get the current group price
			 ****************************************************************/
	FUNCTION get_group_currentp (
		p_acc_id   t704_account.c704_account_id%TYPE
	  , p_ref_id   t7001_group_part_pricing.c7001_ref_id%TYPE
	  , p_gpo_id      IN t7000_account_price_request. c101_gpo_id%TYPE
	)
		RETURN VARCHAR2
	IS
		v_price 	   t705_account_pricing.c705_unit_price%TYPE;
    	v_acc_id T704_ACCOUNT.C704_ACCOUNT_ID%TYPE;
    	
	BEGIN
 	    SELECT get_group_current_prc(p_acc_id,p_gpo_id,p_ref_id) INTO v_price FROM DUAL;	
       
       
        RETURN v_price;
		
	END get_group_currentp;

	/******************************************************************
		Description : This Fucntion is to get the current group price
		****************************************************************/
	FUNCTION get_group_currentp_req (
		p_acc_id	   t704_account.c704_account_id%TYPE
	  , p_ref_id	   t7001_group_part_pricing.c7001_ref_id%TYPE
	  , p_request_id   t7000_account_price_request.c7000_account_price_request_id%TYPE
	  , p_gpo_id    IN t7000_account_price_request. c101_gpo_id%TYPE
	)
		RETURN VARCHAR2
	IS
		v_price 	   t705_account_pricing.c705_unit_price%TYPE;
	BEGIN
	
		BEGIN
			IF (p_request_id IS NULL)
			THEN
          SELECT get_group_currentp(p_acc_id,p_ref_id, p_gpo_id)
          INTO v_price
          FROM DUAL;
			ELSE
			  SELECT c7001_current_price INTO v_price FROM(
				  SELECT c7001_current_price
				  FROM t7001_group_part_pricing t7001
				 WHERE t7001.c7000_account_price_request_id = p_request_id
				   AND t7001.c7001_ref_id = p_ref_id
				   AND t7001.c7001_void_fl IS NULL
           		   AND t7001.c7001_demo_flg!=1)
           	   WHERE ROWNUM = 1;
			END IF;
		END;

		RETURN v_price;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN '';
--
	END get_group_currentp_req;

	/******************************************************************
			 Description : This Fucntion is to get system ids for the group ids passed
		****************************************************************/
	FUNCTION get_groups_systemids (
		p_groupids	 CLOB
	)
		RETURN VARCHAR2
	IS
		v_systemids    VARCHAR2 (1000);
		v_delim        CLOB;

		CURSOR systemid_details
		IS
			SELECT DISTINCT t4010.c207_set_id setid
					   FROM t4010_group t4010
					  WHERE t4010.c4010_group_id IN (SELECT to_char(token)
													   FROM v_clob_list)
						AND c4010_void_fl IS NULL
						AND c901_type = 40045
						AND c901_group_type IS NOT NULL;
	BEGIN
		BEGIN
		--	my_context.set_my_inlist_ctx (p_groupids);
 		my_context.set_my_cloblist(p_groupids||',');
			FOR currindex1 IN systemid_details
			LOOP
				--v_systemids := v_systemids || ',' || currindex1.setid;
				 SELECT DECODE(NVL(length(v_systemids),0),0,'',',') into v_delim FROM DUAL;
				v_systemids := v_systemids || v_delim || currindex1.setid;
			END LOOP;
		END;

		RETURN v_systemids;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN '';
--
	END get_groups_systemids;

	/******************************************************************
			 Description : This Fucntion is to get all the dependent system ids for the group ids passed
		****************************************************************/
	FUNCTION get_groups_dep_systemids (
		p_groupids	 CLOB
	)
		RETURN VARCHAR2
	IS
		v_systemids    VARCHAR2 (1000);

		CURSOR systemid_details
		IS
			SELECT DISTINCT t4010.c207_set_id setid
					   FROM t4010_group t4010
					  WHERE t4010.c4010_group_id IN (SELECT to_char(token)
													   FROM v_clob_list)
						AND c4010_void_fl IS NULL
						AND c901_type = 40045
						AND c901_group_type IS NOT NULL
			UNION ALL
			SELECT DISTINCT c207_set_id setid
					   FROM t7003_system_construct t7003, t7004_construct_mapping t7004
					  WHERE t7004.c4010_group_id IN (SELECT to_char(token)
													   FROM v_clob_list)
						AND t7004.c7003_system_construct_id = t7003.c7003_system_construct_id;
	BEGIN
		BEGIN
			--my_context.set_my_inlist_ctx (p_groupids);
			my_context.set_my_cloblist(p_groupids||',');

			FOR currindex1 IN systemid_details
			LOOP
				v_systemids := v_systemids || ',' || currindex1.setid;
			END LOOP;
		END;

		RETURN v_systemids;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN '';
--
	END get_groups_dep_systemids;

	/*****************************************************************************
		Description  : this function returns price for group , list, AD, VP , TripWire
		Parameters	: p_groupid
		Parameters	: p_price_type
		*****************************************************************************/
	FUNCTION get_group_price_dtls (
		p_request_id   IN	t7000_account_price_request.c7000_account_price_request_id%TYPE
	  , p_groupid	   IN	t4010_group.c4010_group_id%TYPE
	  , p_price_type   IN	t7010_group_price_detail.c901_price_type%TYPE
	)
		RETURN NUMBER
	IS
		v_price 	   t705_account_pricing.c705_unit_price%TYPE;
	BEGIN
		--
		BEGIN
			SELECT c7010_price
			  INTO v_price
			  FROM t7010_group_price_detail t7010
			 WHERE t7010.c4010_group_id = p_groupid
			   AND t7010.c901_price_type = p_price_type
			   AND t7010.c7010_void_fl IS NULL;
		END;

		RETURN v_price;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN 0;
--
	END get_group_price_dtls;

	/******************************************************************
			 Description : This procedure is used to fetch question, answer, answer group
			 ****************************************************************/
	PROCEDURE gm_fch_question_answer (
		p_requestid 		  IN	   t7000_account_price_request.c7000_account_price_request_id%TYPE
	  , p_outquestion		  OUT	   TYPES.cursor_type
	  , p_outanswer 		  OUT	   TYPES.cursor_type
	  , p_outanswergroup	  OUT	   TYPES.cursor_type
	  , p_outquestionanswer   OUT	   TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_outquestion
		 FOR
			 SELECT   c9011_question_id questionid, c9011_question_name questionname
				 FROM t9011_question
				WHERE c9011_void_fl IS NULL   -- c901_ref_type = 52130	---pricing question
			 ORDER BY c9011_question_id;

		OPEN p_outanswer
		 FOR
			 SELECT   c9011_question_id questionid, c9012_answer_grp_id groupid, c9012_answer_grp groupname
					, c901_control_type controltype
				 FROM t9012_answer_grp
				WHERE c9012_void_fl IS NULL
			 ORDER BY c9012_answer_grp_id;

		OPEN p_outanswergroup
		 FOR
			 SELECT c9013_answer_group_list_nm listname, c9013_answer_group_list_id listid, c9013_answer_grp groupname
			   FROM t9013_answer_group_list;

		OPEN p_outquestionanswer
		 FOR
			 SELECT   c9013_answer_group_list_id listid, c9010_answer_desc answerdesc, c9012_answer_grp_id groupid
				 FROM t9010_answer_data
				WHERE c9010_ref_id = p_requestid AND c901_ref_type = 52130 AND c9010_void_fl IS NULL
			 ORDER BY c9012_answer_grp_id;
	END gm_fch_question_answer;

	/******************************************************************
				Description : Procedure to save the question answers
		****************************************************************/
	PROCEDURE gm_save_request_answer (
		p_requestid   IN   t7000_account_price_request.c7000_account_price_request_id%TYPE
	  , p_inputstr	  IN   VARCHAR2
	  , p_userid	  IN   VARCHAR2
	)
	AS
		v_strlen	   NUMBER := NVL (LENGTH (p_inputstr), 0);
		v_substring    VARCHAR2 (1000);
		v_string	   VARCHAR2 (4000) := p_inputstr;
		v_quesid	   VARCHAR2 (20);
		v_answergrpid  VARCHAR2 (20);
		v_answerlistid VARCHAR2 (20);
		v_answerdesc   VARCHAR2 (4000);
	BEGIN
		IF v_strlen > 0
		THEN
			--
			WHILE INSTR (v_string, '|') <> 0
			LOOP
				--
				v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
				v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
				v_quesid	:= NULL;
				v_answergrpid := NULL;
				v_answerlistid := NULL;
				v_answerdesc := NULL;
				v_quesid	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_answergrpid := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_answerlistid := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_answerdesc := TRIM (v_substring);

				--
				UPDATE t9010_answer_data
				   SET c9013_answer_group_list_id = v_answerlistid
					 , c9010_answer_desc = v_answerdesc
					 , c9010_last_updated_by = p_userid
					 , c9010_last_updated_date = CURRENT_DATE
				 WHERE c9010_ref_id = p_requestid AND c9012_answer_grp_id = v_answergrpid;

				IF (SQL%ROWCOUNT = 0)
				THEN
					INSERT INTO t9010_answer_data
								(c9010_answer_data_id, c9010_ref_id, c901_ref_type, c9011_question_id
							   , c9012_answer_grp_id, c9013_answer_group_list_id, c9010_answer_desc, c9010_created_by
							   , c9010_created_date
								)
						 VALUES (s9010_answer_data.NEXTVAL, p_requestid, 52130, v_quesid
							   , v_answergrpid, v_answerlistid, TRIM (v_answerdesc), p_userid
							   , CURRENT_DATE
								);
				END IF;
			--
			END LOOP;
		--
		END IF;
	END gm_save_request_answer;

	
	/******************************************************************
	 Description : This Fucntion is to get the flag if reqid already exist
	****************************************************************/
	FUNCTION get_flg_req_id (
		p_acctid   VARCHAR2
	)
		RETURN VARCHAR2
	IS
		v_cnt		   NUMBER;
	BEGIN
		BEGIN
			SELECT COUNT (1)
			  INTO v_cnt
			  FROM t7000_account_price_request
			 WHERE c704_account_id = p_acctid 
       AND (c901_request_status = 52186 OR c901_request_status = 52187 
       OR c901_request_status = 52188) AND c7000_void_fl IS NULL;
		END;

		IF v_cnt > 0
		THEN
			RETURN 'Y';
		ELSE
			RETURN ' ';
		END IF;
	END get_flg_req_id;

	
	/******************************************************************
	 Description : This Fucntion is to get the status if reqid already exist
	****************************************************************/
	FUNCTION get_pricing_status(
		p_acctid   VARCHAR2
	  , p_gpo_id   t7000_account_price_request. c101_gpo_id%TYPE
	)
		RETURN VARCHAR2
	IS
		v_status t7000_account_price_request.c901_request_status%TYPE;
		
	BEGIN
		BEGIN
			SELECT  status INTO v_status FROM (SELECT c901_request_status	status		  
			  FROM t7000_account_price_request
			 WHERE c704_account_id = p_acctid
			 AND NVL(c101_gpo_id,'-999') = NVL(p_gpo_id,'-999')
			 AND (c901_request_status = 52186 OR c901_request_status = 52187 -- 52187 PENDING AD APPR,52122 PEND VP APPR,52123 PEND PC APPR
       		 --OR c901_request_status = 52122 OR c901_request_status = 52123
       		 ) AND c7000_void_fl IS NULL
       		 ORDER BY C7000_CREATED_DATE ASC) 
       		 WHERE ROWNUM =1;
		EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN '';
		END;
		RETURN v_status;
		
	END get_pricing_status;
	
	/*******************************************************************************
	 Description     : THIS FUNCTION RETURNS CONTRACT NAME GIVEN THE ACCOUNT_ID
 	 Parameters 	 : account id
	*******************************************************************************/
	
	FUNCTION GET_ACCOUNT_CONTRACT(
		p_acc_id t704a_account_attribute.C704_ACCOUNT_ID%TYPE
	)
		RETURN VARCHAR2
	IS
	v_contractc_fl  t704a_account_attribute.C704A_ATTRIBUTE_VALUE%TYPE;
BEGIN
     BEGIN
	  SELECT C901_CONTRACT CONTRACT_FL
    INTO V_CONTRACTC_FL
    FROM T704D_ACCOUNT_AFFLN
    WHERE C704_ACCOUNT_ID = p_acc_id
    AND C704D_VOID_FL  IS NULL;
   EXCEPTION
      WHEN NO_DATA_FOUND
   THEN
      RETURN '';
   END;
     RETURN v_contractc_fl;
     
END GET_ACCOUNT_CONTRACT;
	
	
	/*****************************************************************************
		Description : Procedure to save multiple account price request (annual change)
		******************************************************************************/
	/******************************************************************
			 Description : Procedure to save the question answers
	****************************************************************/
	PROCEDURE gm_fch_ad_vp_flg (
		p_accountid   IN	   VARCHAR2
	  , p_userid	  IN	   t7000_account_price_request.c7000_created_by%TYPE
	  , p_advpflg	  OUT	   VARCHAR2
	)
	AS
		v_adid		   v700_territory_mapping_detail.ad_id%TYPE;
		v_vpid		   v700_territory_mapping_detail.vp_id%TYPE;
		v_advpflg	   VARCHAR2 (10);
	BEGIN
		BEGIN
			SELECT v700.ad_id, v700.vp_id
			  INTO v_adid, v_vpid
			  FROM v700_territory_mapping_detail v700
			 WHERE v700.ac_id = p_accountid;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				p_advpflg	:= '';
		END;

		IF p_userid = v_adid
		THEN
			p_advpflg	:= 'AD';
		ELSIF p_userid = v_vpid
		THEN
			p_advpflg	:= 'VP';
		END IF;
	END gm_fch_ad_vp_flg;
   /******************************************************************
			 Description : Procedure to save the the pricing rule information
	****************************************************************/
 /* PROCEDURE gm_save_pricing_rule (
   p_requestid 	  IN	   t7000_account_price_request.c7000_account_price_request_id%TYPE
  , p_acc_id		  IN	   t704_account.c704_account_id%TYPE
  , p_userid		  IN	   VARCHAR2
  )
  AS   
  CURSOR ruleid_detail
      IS
  SELECT DISTINCT t7016.c7015_rule_id ruleid		
  FROM t7003_system_construct t7003, t7016_pricing_rules_detail t7016, t7015_pricing_rules t7015                                    ,t7004_construct_mapping t7004    , t7016_pricing_rules_detail t7016, t7015_pricing_rules t7015    WHERE  t7016.c7015_rule_id = t7015.c7015_rule_id
              and t7015.c7015_void_fl is null
              and t7015.c7015_inactive_fl is null
    AND t7003.c7003_void_fl IS NULL
    AND t7016.c7016_void_fl is null
              and t7016.c7016_ref_number = -1
              and ((t7004.c4010_group_id IN (SELECT token   FROM v_in_list)
                  and t7003.c7003_system_construct_id = t7004.c7003_system_construct_id
                  and t7016.c7016_ref_id = t7003.c207_set_id
                  AND t7016.c901_ref_type = 70200) -- code for construct
                  or (t7016.c7016_ref_id IN (SELECT token FROM v_in_list)
                  and t7016.c901_ref_type = 70201)); -- code for group
              
  BEGIN
      
    FOR rid IN ruleid_detail
    LOOP
          for rrid in (select t7016.c7015_rule_id ruleid,                                                                                                 
                              t7016.c7016_ref_number refnum,   t7016.c7016_construct_level conslvl ,
                              t7016.c901_ref_type reftype, t7015.c7015_procedure_type ptype, t7016.c7016_ref_id refid, 
                              t7016.c7016_operator oper, t7016.c7016_percentage per
                      from t7016_pricing_rules_detail t7016, t7015_pricing_rules t7015
                      where t7016.c7015_rule_id = rid.ruleid
                      order by t7016.c7016_ref_number)
          loop
              if(rrid.reftype= 70200) THEN
                  v_cons_id = get_construct_id(rrid.refid, rrid.conslvl);
                  v_construct_group_price = get_rule_construct_price(p_requestid,p_account_id,v_cons_id);
             elsif rrid.reftype = 70201 THEN
                v_construct_group_price = get_rule_group_price(p_requestid,p_account_id, rrid.refid);
             end if;
              
              if(rrid.refnum = -1) then
                     v_actual_price = v_construct_group_price;
              elsif( rrid.refnum = 1) then
                     v_target_price = rrid.per * v_construct_group_price;
                     v_operator = rrid.opr;
              else
                     if v_operator='+' then
                         v_target_price = v_target_price + rrid.per * v_construct_group_price;
                     elsif v_operator='-' then
                         v_target_price = v_target_price - rrid.per * v_construct_group_price;
                     end if;
                     v_operator = rrid.opr;
              end if;
          
          end loop;
          gm_sm_sav_pricingrules_info(rid.ruleid, p_requestid, p_acc_id, v_target_price, v_actual_price, p_userid);
        END LOOP;   
        
  END gm_save_pricing_rule;    
*/
	
	
/******************************************************************
 Description : This procedure is get the analysis data.
****************************************************************/
	PROCEDURE gm_fch_request_analysis_view (
	   p_requestid 		    IN      t7000_account_price_request.c7000_account_price_request_id%TYPE
	   ,p_acc_id		    IN	    t7051_account_group_pricing.c704_account_id%TYPE
 	   ,p_gpo_id		    IN	    t7051_account_group_pricing.c101_gpo_id%TYPE
	   ,p_outdetail 	    OUT	     TYPES.cursor_type
	   ,p_outconst	 	    OUT	     TYPES.cursor_type
	)
	AS
	
	v_account_id    t704_account.c704_account_id%TYPE;
	v_gpo_id 		t101_user.c101_party_id%TYPE;
	
	BEGIN
		
		BEGIN
			SELECT c704_account_id, c101_gpo_id INTO v_account_id, v_gpo_id
			FROM T7000_account_price_request WHERE c7000_account_price_request_id = p_requestid;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				v_account_id:=p_acc_id;
				v_gpo_id:=p_gpo_id;
		END;
		
		IF v_gpo_id IS NOT NULL
		THEN
			v_account_id := '';
		END IF;
		
		OPEN p_outdetail
		 FOR
		/*SELECT system_name,group_id,group_nm,group_qty ,construct_id,group_proposed ,construct_name,group_proposed_constr
                  ,group_twp, group_listp, group_twp , group_twp_constr,DECODE(SIGN(group_twp-group_proposed),-1,'YES','NO') CHK_FLG,
                  group_current , group_current_constr,  group_listp_constr
                  ,DECODE(ROUND(((group_listp-nvl(group_proposed,0))/group_listp)*100,2),100,'',ROUND(((group_listp-nvl(group_proposed,0))/group_listp)*100,2)) offlist
                  FROM(
                        SELECT get_set_name(t207.c207_set_id) system_name ,t4010.c4010_group_id group_id,
                        t4010.c4010_group_nm group_nm , t7004.c7004_qty group_qty , t7003.c7003_system_construct_id construct_id
                        ,DECODE(get_group_proposed_prc(p_requestid,t4010.c4010_group_id),0,0,get_group_proposed_prc(p_requestid,t4010.c4010_group_id)) group_proposed
                        ,DECODE(NVL(get_group_proposed_prc_cons(p_requestid,t4010.c4010_group_id),0),0,0,NVL(get_group_proposed_prc_cons(p_requestid,t4010.c4010_group_id),0)) group_proposed_constr
                        ,DECODE(NVL(get_group_twprice_cons(t7003.c7003_system_construct_id,t4010.c4010_group_id),0),0,0,NVL(get_group_twprice_cons(t7003.c7003_system_construct_id,t4010.c4010_group_id),0)) group_twp_constr
                        ,DECODE(get_construct_twprice(t7003.c7003_system_construct_id,t4010.c4010_group_id),0,0,get_construct_twprice(t7003.c7003_system_construct_id,t4010.c4010_group_id)) group_twp
                        ,DECODE(NVL(get_group_current_prc_cons(v_account_id,v_gpo_id,t4010.c4010_group_id),0),0,0,NVL(get_group_current_prc_cons(v_account_id,v_gpo_id,t4010.c4010_group_id),0)) group_current_constr
                        ,DECODE(get_group_current_prc(v_account_id,v_gpo_id,t4010.c4010_group_id),0,0,get_group_current_prc(v_account_id,v_gpo_id,t4010.c4010_group_id)) group_current
                        ,DECODE(NVL(get_group_listprice_cons(t7003.c7003_system_construct_id,t4010.c4010_group_id),0),0,0,NVL(get_group_listprice_cons(t7003.c7003_system_construct_id,t4010.c4010_group_id),0)) group_listp_constr
                        ,DECODE(get_construct_listprice(t7003.c7003_system_construct_id,t4010.c4010_group_id),0,0,get_construct_listprice(t7003.c7003_system_construct_id,t4010.c4010_group_id)) group_listp
                        ,t7003.c7003_construct_name construct_name
                        FROM t7003_system_construct t7003,  t7004_construct_mapping t7004 ,      t4010_group t4010 ,  t207_set_master t207
                        WHERE t7003.c7003_system_construct_id = t7004.c7003_system_construct_id
                        AND t4010.c4010_group_id  = t7004.c4010_group_id
                        AND t207.c207_set_id      = t7003.c207_set_id
                        AND t7004.c4010_group_id  IN (
                              (SELECT c4010_group_id
                                    FROM t4010_group --where --c4010_principal_grp_fl is not null and
                              --c4010_publish_fl is not null
                              ))
                        AND t7004.c7004_qty > 0
                  ORDER BY t7003.c7003_construct_name, t7003.c7003_system_construct_id) principal_group;
	*/
	
	SELECT system_name,group_id,group_nm,group_qty ,construct_id,group_proposed ,construct_name,group_proposed*group_qty group_proposed_constr
                  ,  group_twp , group_twp*group_qty group_twp_constr,DECODE(SIGN(group_twp-group_proposed),-1,'YES','NO') CHK_FLG,
                  group_current , group_current*group_qty group_current_constr, group_listp, group_listp*group_qty group_listp_constr
                  ,DECODE(ROUND(((group_listp-nvl(group_proposed,0))/group_listp)*100,2),100,'',ROUND(((group_listp-nvl(group_proposed,0))/group_listp)*100,0)) offlist
                  , gm_pkg_sm_pricing_request.get_pricing_rule_by_id(p_requestid,construct_id,'') pricing_rule_cons_id
                  , gm_pkg_sm_pricing_request.get_pricing_rule_by_id(p_requestid,'',group_id) pricing_rule_grp_id
                  
                  FROM(
                        SELECT get_set_name(t207.c207_set_id) system_name ,t4010.c4010_group_id group_id,
                        t4010.c4010_group_nm group_nm , t7004.c7004_qty group_qty , t7003.c7003_system_construct_id construct_id
                         ,get_group_proposed_prc(p_requestid,t4010.c4010_group_id) group_proposed
                        ,get_construct_twprice(t7003.c7003_system_construct_id,t4010.c4010_group_id) group_twp
                       -- ,get_group_current_prc(v_account_id,v_gpo_id,t4010.c4010_group_id) group_current
                         ,NVL (t7051.c7051_price, t705.c705_unit_price) group_current
                        ,get_construct_listprice(t7003.c7003_system_construct_id,t4010.c4010_group_id) group_listp
                        ,t7003.c7003_construct_name construct_name
                        FROM t7003_system_construct t7003,  t7004_construct_mapping t7004 ,      t4010_group t4010 ,  t207_set_master t207, 
	                         (SELECT t705.c4010_group_id, t705.c705_unit_price
	                    FROM t705_account_pricing_by_group t705
	                   WHERE  ( t705.c704_account_id = NVL(v_account_id,-999) OR t705.C101_PARTY_ID =  NVL(v_gpo_id, -999)) ) t705,
	                 (SELECT t7051.c7051_ref_id c4010_group_id, t7051.c7051_price
	                    FROM t7051_account_group_pricing t7051
	                   WHERE t7051.c901_ref_type = '52000'               --'GROUP'
	                     AND t7051.c7051_active_fl = 'Y'
	                     AND t7051.C7051_VOID_FL IS NULL
	                     AND ( c704_account_id = NVL(v_account_id,-999) OR t7051.C101_GPO_ID =  NVL(v_gpo_id, -999) ) ) t7051  
                        WHERE t7003.c7003_system_construct_id = t7004.c7003_system_construct_id
                        AND t4010.c4010_group_id  = t7004.c4010_group_id
                        AND t207.c207_set_id      = t7003.c207_set_id
                        AND t7004.c4010_group_id = t7051.c4010_group_id(+)
                        AND t7004.c4010_group_id = t705.c4010_group_id(+)
                        AND t7004.c7004_qty > 0
			AND t7003.C7003_PRINCIPAL_FL IS NOT NULL
                  ORDER BY t7003.c7003_construct_name, t7003.c7003_system_construct_id) principal_group;
	

  		
		OPEN p_outconst
		 FOR
			 SELECT t7003.c7003_system_construct_id construct_id ,t7003.c7003_construct_name construct_name
				FROM t7003_system_construct t7003,  t7004_construct_mapping t7004
			WHERE t7003.c7003_system_construct_id = t7004.c7003_system_construct_id
        		AND t7004.c7004_qty > 0
        		ORDER BY t7003.c7003_construct_name, t7003.c7003_system_construct_id;
        
  END gm_fch_request_analysis_view;
  
  	/*************************************************************************
				Description : Procedure for save pricing activity log details
		**********************************************************************/
	PROCEDURE gm_pkg_sm_pricing_activity_log (
		p_req_id      IN   t7000_account_price_request.c7000_account_price_request_id%TYPE
	  , p_status      IN   VARCHAR2
	  , p_userid	  IN   VARCHAR2
	)
	AS
		v_activity_log_id	   NUMBER;
		v_desc                 VARCHAR2 (1000);
		v_desc_status          NUMBER;
	BEGIN
		BEGIN
			IF p_status='52186'	THEN
				SELECT c909_activity_log_id INTO v_activity_log_id FROM T909_ACTIVITY_LOG 
				WHERE c909_ref_id  = p_req_id 
				AND c901_ref_type  = '52221'
				AND c901_operation = p_status
				AND c909_created_by = p_userid
				AND c909_void_fl IS NULL;
			ELSE
				SELECT c909_activity_log_id INTO v_activity_log_id FROM T909_ACTIVITY_LOG 
				WHERE c909_ref_id  = p_req_id 
				AND c901_ref_type  = '52221'
				AND c901_operation = p_status
				AND c909_last_updated_by = p_userid
				AND c909_void_fl IS NULL;
			END IF;
		EXCEPTION WHEN NO_DATA_FOUND
			THEN
				v_activity_log_id:='';
		END;
		
		 IF p_status='52186'    --Initiated
		 THEN
                v_desc_status:='52224';   --description from lookup table.
         ELSIF p_status='52223' --Viewed 
         THEN
                v_desc_status:='52225';
         ELSIF p_status='52222' --Modified 
         THEN
                v_desc_status:='52226';
         ELSIF p_status='52187' --Pending AD approval
         THEN
                v_desc_status:='52227';
         ELSIF p_status='52122' --Pending VP approval
         THEN
                v_desc_status:='52228';
         ELSIF p_status='52123' --Pending PC approval 
         THEN
                v_desc_status:='52229'; 
         ELSIF p_status='52189' --Approved Not Implemented
         THEN
                v_desc_status:='52230';
         ELSIF p_status='52190' --Approved Implemented 
         THEN
                v_desc_status:='52231'; 
         END IF;         
         
         SELECT get_code_name(v_desc_status) INTO v_desc FROM DUAL;
         
         v_desc := v_desc ||p_req_id;     
         
         gm_pkg_common.gm_pkg_common_activity_log(v_activity_log_id ,p_req_id,'52221',p_status,v_desc,p_userid);
         
       -- MNTTASK-6115 - If we use my_clob_list in the proc, we cannot do commit transaction. So i commented below condtion.
         /*IF p_status='52223' --View 
         THEN
                commit;
         END IF;*/
	END gm_pkg_sm_pricing_activity_log;
	
/******************************************************************
			 Description : Procedure to fetch Activity Log Details
*******************************************************************/
	PROCEDURE gm_fch_activity_details (
		p_ref_id   IN	   t7000_account_price_request.c7000_account_price_request_id%TYPE
	  , p_status   IN      VARCHAR2
	  , p_out_data	  OUT  TYPES.cursor_type
	)
	AS		
	   v_out_data NUMBER;
	BEGIN
		
		OPEN p_out_data
		 FOR
			   SELECT GET_USER_NAME(NVL(c909_created_by,c909_last_updated_by)) USERNAME,c909_description DETAIL,DECODE(c901_operation,'52186','Add','52187','Submit For AD approval','52122','Submit For VP approval','52123','Submit For PC approval',GET_CODE_NAME(c901_operation)) STATUS,TO_CHAR(NVL(c909_created_date,c909_last_updated_date),GET_RULE_VALUE('DATEFMT','DATEFORMAT')||' HH24:MI:SS')DATETIME
				 FROM t909_activity_log
			    WHERE c909_ref_id = p_ref_id
        		 AND c909_void_fl IS NULL
        		 ORDER BY DATETIME DESC;
			 	
	END gm_fch_activity_details;
	
	
  /******************************************************************
 Description : Procedure to fetch the Request Construct Price
****************************************************************/
	PROCEDURE gm_chk_request_construct_price (
		p_request_id 		IN		 t7000_account_price_request.c7000_account_price_request_id%TYPE
	   
	)
	AS
	
	v_constr_twp 		NUMBER;
	v_constr_propprice 	NUMBER;
	v_grp_aprl_lvl_cnt  NUMBER;
	v_construct_id  t7004_construct_mapping.c7003_system_construct_id %TYPE;
	
	CURSOR construct_cur IS
		SELECT DISTINCT c7003_system_construct_id consid --,C4010_GROUP_ID groupid
		FROM	t7004_construct_mapping
		WHERE 	C4010_GROUP_ID IN (SELECT C7001_REF_ID 
			FROM	T7001_GROUP_PART_PRICING
			WHERE	C901_REF_TYPE = 52000 -- GROUP 
			AND 	C7000_ACCOUNT_PRICE_REQUEST_ID = p_request_id);
				
	BEGIN
			-- THIS QUERY WILL GET ALL THE CONSTRUCT THE GROUP IN THE REQUEST BELONGS TO.
			-- IF THERE ARE NO CONSTUCT FOR THE GROUP IN THE REQUEST,THEN WE DO NOT DO ANYTHING.
		
		FOR const_ind IN construct_cur
		LOOP
			v_construct_id := const_ind.consid;
		
			--Get Construct's Tripwire price.
			v_constr_twp := GET_GROUP_TWPRICE_CONS(v_construct_id,'');
			
			--Get Request's Construct Proposed Price.
			v_constr_propprice := GET_REQ_CONSTR_PROPOSED_PRICE(p_request_id, v_construct_id);
			
			 --Get count of group which belongs to construct and in 'PC' level.
			
			SELECT COUNT(*) INTO v_grp_aprl_lvl_cnt FROM t7001_group_part_pricing
				WHERE c901_ref_type = 52000 AND c7001_ref_id IN (
					SELECT c4010_group_id FROM t7004_construct_mapping WHERE c7003_system_construct_id  = v_construct_id)
					AND get_code_name(c901_aprl_lvl) = 'PC' AND c7000_account_price_request_id = p_request_id;
			
			--Check if the construct propse price is less than trip wire price or if any one of the group in a construct is having 'PC' approval level .
			
			IF ((v_constr_propprice < v_constr_twp) OR v_grp_aprl_lvl_cnt >0)
			THEN
			   UPDATE t7001_group_part_pricing SET c901_aprl_lvl = 52201
			    WHERE  c7001_ref_id IN ( SELECT c4010_group_id FROM t7004_construct_mapping 
			   			WHERE c7003_system_construct_id = v_construct_id) 
			   			AND c901_ref_type = 52000
						AND c7000_account_price_request_id = p_request_id;
			
				UPDATE T7000_ACCOUNT_PRICE_REQUEST  SET  C901_APRL_LVL = 52201 WHERE C7000_ACCOUNT_PRICE_REQUEST_ID = p_request_id;
			
			END IF;
		END LOOP;

	END gm_chk_request_construct_price;

/******************************************************************
	 Description : This Fucntion is to get the pricing rules for Group / Construct
	****************************************************************/
	FUNCTION get_pricing_rule_by_id (
		p_requestid   VARCHAR2,
		p_const_id	  VARCHAR2,
		p_grp_id      VARCHAR2
	)
		RETURN VARCHAR2
	IS
		v_rule_id    NUMBER;
		
	BEGIN
		BEGIN
		/*	SELECT C7050_PRICING_RULE_ID INTO v_rule_id
			FROM
				T7006_PRICING_REQUEST_RULES T7006
			WHERE
				T7006.C7000_ACCOUNT_PRICE_REQUEST_ID = p_requestid AND
				C7050_RULE_REF_ID  = NVL(p_const_id,p_grp_id);
		*/
		
		select  C7050_PRICING_RULE_ID INTO v_rule_id from t7050_pricing_rules t7050
		where C7050_SRC_RULE_REF_ID  = NVL(p_const_id,p_grp_id)
        AND C7050_VOID_FL IS NULL;		
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				RETURN '';
		END;

		RETURN v_rule_id;
	END get_pricing_rule_by_id;	

/******************************************************************
	 Description : This Fucntion is to get the Name of the Construct
	****************************************************************/
	FUNCTION get_construct_name (
		p_const_id T7003_SYSTEM_CONSTRUCT.C7003_SYSTEM_CONSTRUCT_ID%TYPE
	)
		RETURN VARCHAR2
	IS
		v_const_nm T7003_SYSTEM_CONSTRUCT.C7003_CONSTRUCT_NAME%TYPE;
		
	BEGIN
		BEGIN
			SELECT C7003_CONSTRUCT_NAME INTO v_const_nm
			FROM
				T7003_SYSTEM_CONSTRUCT T7003
			WHERE
				T7003.C7003_SYSTEM_CONSTRUCT_ID = p_const_id AND
				C7003_VOID_FL IS NULL;
					
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				RETURN '';
		END;

		RETURN v_const_nm;
	END get_construct_name;
	


	/******************************************************************
	 Description : Procedure to Get the Pricing Rule for the request and the pricing rule
	*******************************************************************/
	PROCEDURE gm_fch_pricing_rule_details (
		p_request_id   IN	   t7000_account_price_request.c7000_account_price_request_id%TYPE
	  , p_rule_id   IN      T7050_PRICING_RULES.C7050_PRICING_RULE_ID%TYPE
	  , p_out_data	  OUT  TYPES.cursor_type
	)
	AS		
	BEGIN
		
		OPEN p_out_data
		 FOR
		 	/*
			   SELECT 
			   t7006.c7050_pricing_rule_id RULE_ID,
			   t7050.c901_pricing_rule_type src_type_id ,
			  get_code_name(t7050.c901_pricing_rule_type) src_type_nm ,
			  DECODE(t7050.c901_pricing_rule_type,1006412, gm_pkg_sm_pricing_request.get_construct_name(t7050.C7050_SRC_RULE_REF_ID) ,get_group_name(t7050.C7050_SRC_RULE_REF_ID)) SrcName,			  
			   t7051.c7051_tgt_rule_ref_id tgt_id,
			  t7051.c901_pricing_rule_type tgt_type_id,
			  get_code_name(t7051.c901_pricing_rule_type) tgt_typ_nm,
			  DECODE(t7051.c901_pricing_rule_type,1006412, gm_pkg_sm_pricing_request.get_construct_name(t7051.c7051_tgt_rule_ref_id) ,get_group_name(t7051.c7051_tgt_rule_ref_id)) TgtName,
			  t7050.c7050_pricing_rule_nm ,
			  t7006.c7006_requested_price srcpric,
			  t7006.c7006_target_price tgtprc,
			  t7051.C7051_TGT_PRECENT_VALUE percent			  
			  
			FROM T7050_PRICING_RULES t7050,
			  T7051_PRICING_RULE_DETAIL t7051,
			  T7006_PRICING_REQUEST_RULES t7006
			WHERE 
			t7050.c7050_pricing_rule_id        = t7051.c7050_pricing_rule_id
			AND t7006.c7050_pricing_rule_id          = t7050.c7050_pricing_rule_id
			AND t7006.c7000_account_price_request_id =p_request_id
			AND t7006.c7050_pricing_rule_id          =p_rule_id
			AND C7050_VOID_FL IS NULL ;
    		*/

			   SELECT 
			   t7050.c7050_pricing_rule_id RULE_ID,
			   t7050.c901_pricing_rule_type src_type_id ,
			  get_code_name(t7050.c901_pricing_rule_type) src_type_nm ,
			  DECODE(t7050.c901_pricing_rule_type,1006412, gm_pkg_sm_pricing_request.get_construct_name(t7050.C7050_SRC_RULE_REF_ID) ,get_group_name(t7050.C7050_SRC_RULE_REF_ID)) SrcName,			  
			  GET_PRICE_RULE_DETAIL(p_rule_id) tgtname,
			  t7050.c7050_pricing_rule_nm ,
			  GET_RULE_PRICE(p_request_id, t7050.c7050_pricing_rule_id,'SRC') srcpric,
			  GET_RULE_PRICE(p_request_id, t7050.c7050_pricing_rule_id,'TGT') tgtprc
			  
			FROM T7050_PRICING_RULES t7050
			WHERE t7050.c7050_pricing_rule_id          =p_rule_id
			AND C7050_VOID_FL IS NULL ;

	END gm_fch_pricing_rule_details;
	
	
	
FUNCTION   GET_RULE_PRICE (
    p_request_id t7000_account_price_request.c7000_account_price_request_id%TYPE,
    p_rule_id	 T7050_PRICING_RULES.c7050_pricing_rule_id%TYPE,
    p_type       VARCHAR2
)
	RETURN NUMBER
IS
/*	Description 	: THIS FUNCTION RETURNS SOURCE PRICE OF THE RULE FOR THE REQUEST.
 Parameters 		: p_request_id , p_rule_id
*/
v_price NUMBER;	
	

BEGIN
	 BEGIN
			SELECT DECODE(p_type,'SRC',c7006_requested_price,c7006_target_price) INTO v_price
		  FROM T7006_PRICING_REQUEST_RULES
		  WHERE c7000_account_price_request_id =p_request_id
		  AND c7050_pricing_rule_id          =p_rule_id;
	EXCEPTION WHEN NO_DATA_FOUND
	THEN
		v_price :='';
	END;
		  RETURN v_price;
		  
END  GET_RULE_PRICE;	
	
FUNCTION             GET_PRICE_RULE_DETAIL (
    p_rule_id	 T7050_PRICING_RULES.c7050_pricing_rule_id%TYPE
)
	RETURN VARCHAR2
IS
/*	Description 	: THIS FUNCTION RETURNS TARGER PRICING RULES.
 Parameters 		: p_rule_id
*/
v_tgt_rule VARCHAR2(32760);	
	
	CURSOR pop_val
	IS
		SELECT C901_PRICING_RULE_TYPE type,C7051_TGT_PRECENT_VALUE percentage,
		DECODE(c901_pricing_rule_type,1006412, gm_pkg_sm_pricing_request.get_construct_name(c7051_tgt_rule_ref_id) ,get_group_name(c7051_tgt_rule_ref_id)) TGT_NAME,
		C7051_TGT_RULE_REF_ID ref_id
		  FROM T7051_PRICING_RULE_DETAIL
		  WHERE c7050_pricing_rule_id = p_rule_id;

--
BEGIN
	FOR rad_val IN pop_val
	LOOP
		v_tgt_rule := v_tgt_rule ||'<b>'|| rad_val.TGT_NAME || '</B> <br> Percentage : '||rad_val.percentage||'<br><br>';
	END LOOP;

	RETURN v_tgt_rule;
END  GET_PRICE_RULE_DETAIL;

	/******************************************************************
	 Description : Procedure to Get the GPB Account Details
	*******************************************************************/
	PROCEDURE gm_fch_gpb_accounts_details (
		p_party_id   IN	   T101_PARTY.C101_PARTY_ID%TYPE
	  , p_out_data	 OUT  TYPES.cursor_type
	)
	AS
	v_company_id t1900_company.C1900_COMPANY_ID%TYPE;
	BEGIN
	
	 	SELECT get_compid_frm_cntx()
    		INTO v_company_id
    		FROM DUAL;
		
		OPEN p_out_data
		 FOR
		 	SELECT v700.ac_id accid,
		 	v700.ac_name strparentacc,
		 	v700.region_name strregname,
		 	v700.rep_name strrepname,
		 	v700.ter_name strtername,
		 	v700.d_name strdname,
		 	v700.ad_name stradname,
		 	v700.vp_name strvpname
          FROM            
            v700_territory_mapping_detail v700,
            t740_gpo_account_mapping t740,
            t101_party t101
          WHERE  t740.c101_party_id = p_party_id
          AND t740.c101_party_id    = t101.c101_party_id
          AND t740.c704_account_id  = v700.ac_id
		  AND t101.c1900_company_id = v_company_id
		  AND t101.c101_void_fl   IS NULL				  
		  AND t101.c101_active_fl IS NULL
		  AND t101.c101_delete_fl IS NULL
          ORDER BY UPPER (TRIM (t101.c101_party_nm));
          
	END gm_fch_gpb_accounts_details;

END gm_pkg_sm_pricing_request;

/
