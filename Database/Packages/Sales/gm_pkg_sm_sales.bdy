CREATE OR REPLACE PACKAGE BODY gm_pkg_sm_sales
IS
/*
	Below procedure is used to Save and replace ad name.
	*/

PROCEDURE gm_sav_replace_ad (
		p_ad_id    		IN   	t708_region_asd_mapping.c101_user_id%TYPE,
		p_old_ad_id		IN		t708_region_asd_mapping.c101_user_id%TYPE,
		p_user_id     	IN   	t708_region_asd_mapping.c708_created_by%TYPE
	)
	AS
		CURSOR C_AD_REGN_ID IS
		SELECT c901_region_id regnid, c901_user_role_type adtype
	      FROM t708_region_asd_mapping
         WHERE c101_user_id = p_old_ad_id
           AND c708_delete_fl IS null
           AND c708_inactive_fl IS null;
	  
	BEGIN
	
		FOR v_regn_id in C_AD_REGN_ID
		LOOP
			gm_sav_del_asd_map(p_old_ad_id, NULL, v_regn_id.regnid, p_user_id);
			
			gm_sav_asd_regn_map(p_ad_id, v_regn_id.adtype, NULL, v_regn_id.regnid, p_user_id);
					
		END LOOP;
		
	END gm_sav_replace_ad;
	
	/*
	Below procedure is used to replace new  Ad name for region .
	*/
	PROCEDURE gm_sav_asd_regn_map (
		p_ad_id    		IN   	t708_region_asd_mapping.c101_user_id%TYPE,
		p_type    	    IN   	t708_region_asd_mapping.c901_user_role_type%TYPE,
		p_vp_id    		IN   	t708_region_asd_mapping.c101_user_id%TYPE,
		p_regn_id      	IN   	t708_region_asd_mapping.c901_region_id%TYPE,
		p_user_id     	IN   	t708_region_asd_mapping.c708_created_by%TYPE
	)
	AS
		v_new_ad_id	t708_region_asd_mapping.c101_user_id%TYPE;
	BEGIN
		SELECT DECODE(p_type,8000,p_ad_id, 8002, p_ad_id, 8001,p_vp_id,NULL ) 
		  INTO v_new_ad_id 
		  FROM DUAL;
		  
		  UPDATE t708_region_asd_mapping 
		   SET c708_last_updated_by = p_user_id,
		       c708_last_updated_date = SYSDATE
		 WHERE c101_user_id = 303503 --v_id
		   AND c901_region_id = p_regn_id
		   AND c708_delete_fl IS null
		   --AND c708_inactive_fl IS null
		   ;
		
		IF (SQL%ROWCOUNT = 0) THEN
			INSERT INTO t708_region_asd_mapping
				( C708_REGION_ASD_ID, C901_REGION_ID, C101_USER_ID, C708_START_DT,
					C708_CREATED_BY, C708_CREATED_DATE, C901_USER_ROLE_TYPE
				)
			VALUES ( S708_region_asd_mapping.NEXTVAL, p_regn_id, v_new_ad_id, sysdate,
					p_user_id, sysdate, p_type);
		END IF;
		
	END gm_sav_asd_regn_map;
	
	/*
	Below procedure is used to delete or deactivate asd mapping.
	*/
	
	PROCEDURE gm_sav_del_asd_map (
		p_old_ad_id 	IN   	t708_region_asd_mapping.c101_user_id%TYPE,
		p_old_vp_id 	IN   	t708_region_asd_mapping.c101_user_id%TYPE,
		p_regn_id      	IN   	t708_region_asd_mapping.c901_region_id%TYPE,
		p_user_id     	IN   	t708_region_asd_mapping.c708_created_by%TYPE
	)
	AS
	BEGIN
		
		UPDATE t708_region_asd_mapping 
	   	   SET c708_delete_fl = 'Y',
--	   		   c708_inactive_fl = 'Y',
	   		   c708_last_updated_date = sysdate,
	   		   c708_last_updated_by = p_user_id
	   	 WHERE c901_region_id = p_regn_id
		  AND c101_user_id  = NVL(p_old_ad_id,p_old_vp_id);	
		
	END gm_sav_del_asd_map;
	

/*
	Below procedure is used to Get zone name.
	*/
PROCEDURE gm_get_zone (
     p_output_cursor   OUT      TYPES.cursor_type
 )
   AS
   BEGIN
      OPEN p_output_cursor
       FOR
          SELECT DISTINCT v700.gp_id GPID, v700.gp_name||'('|| GET_USER_NAME (V700.VP_ID) ||')' GRPNAME 
          FROM v700_territory_mapping_detail v700 
		  WHERE v700.ad_id<>'-9999' AND v700.vp_id<>'-9999'
		  ORDER BY GRPNAME ASC;
	END gm_get_zone;
	
/*
	Below procedure is used to list out users who are not assigned to any region.
	*/
PROCEDURE gm_fetch_user_list(
   	p_output_cursor OUT TYPES.cursor_type
  )
   AS
		BEGIN
			OPEN p_output_cursor FOR
				SELECT c101_user_id USERID, t101.c101_user_f_name ||' '|| t101.c101_user_l_name USERNAME
                  FROM T101_USER t101
                   WHERE C101_ACCESS_LEVEL_ID   = '3'
                     AND C901_USER_STATUS       = '311'
                      AND t101.c101_user_id NOT IN
                        (SELECT NVL(c101_user_id,'-999')
                        FROM t708_region_asd_mapping
                          WHERE c708_delete_fl     IS NULL
                           AND c708_inactive_fl   IS NULL
                            and C901_USER_ROLE_TYPE = '8000')
                            ORDER BY USERNAME ASC; 		
END gm_fetch_user_list;
   
/*
	Below procedure is used to region name for selected zone.
	*/
   
PROCEDURE gm_fetch_region (
      p_zone_id         IN       t901_code_lookup.c901_code_id%TYPE,
      p_output_cursor1   OUT      TYPES.cursor_type
)
   AS
   BEGIN
      OPEN p_output_cursor1
       FOR
          SELECT  DISTINCT REGION_ID REGID, REGION_NAME, AD_ID ADID,AD_NAME, REGION_NAME||'('||AD_NAME||')' REGNAME 
          FROM V700_TERRITORY_MAPPING_DETAIL v700
          WHERE gp_id= p_zone_id 
		 ORDER BY REGNAME ASC;
END gm_fetch_region;
	
/*
	Below procedure is used to create new AD for a selected  region.
	*/	
	
PROCEDURE gm_create_open_ad (
      p_user_id           IN       t101_user.c101_user_id%TYPE,
      p_sh_name           IN       t101_user.c101_user_sh_name%TYPE,
      p_f_name            IN       t101_user.c101_user_f_name%TYPE,
      p_l_name            IN       t101_user.c101_user_l_name%TYPE,
      p_email_id          IN       t101_user.c101_email_id%TYPE,
      p_dept_id           IN       t101_user.c901_dept_id%TYPE,
      p_accs_lvl_id       IN       t101_user.c101_access_level_id%TYPE,
      p_created_by        IN       t101_user.c101_created_by%TYPE,
      p_user_status       IN       t101_user.c901_user_status%TYPE,
      p_user_type         IN       t101_user.c901_user_type%TYPE,
      p_party_id          IN       t101_user.c101_party_id%TYPE,
      p_party_type        IN       t101_party.c901_party_type%TYPE,
      p_party_fl          IN       t101_party.c101_active_fl%TYPE,
      p_party_m_initial   IN       t101_party.c101_middle_initial%TYPE,
      p_out_party_id      OUT      t101_user.c101_party_id%TYPE,
      p_out_user_id       OUT      t101_user.c101_user_id%TYPE
   )
   
   AS
     v_user_count NUMBER;
     v_user_id   t101_user.c101_user_id%TYPE;
    BEGIN
	    SELECT count(1) INTO v_user_count  
	    FROM t101_user 
	    WHERE c101_user_f_name = p_f_name AND c101_user_l_name = p_l_name;
	  
	      
 IF (v_user_count = 0)
    THEN
       gm_pkg_it_login.gm_save_user(p_user_id,p_sh_name,p_f_name,p_l_name,p_email_id,p_dept_id,p_accs_lvl_id,p_created_by,p_user_status,
       p_user_type,p_party_id,p_party_type,p_party_fl,p_party_m_initial,p_out_party_id,p_out_user_id);
 ELSE 
        BEGIN
         SELECT c101_user_id userid INTO v_user_id 
         FROM t101_user  
         WHERE c101_user_f_name = p_f_name AND c101_user_l_name = p_l_name;
       END;
       
       p_out_user_id:=v_user_id; 
 END IF;
   
END gm_create_open_ad;
END gm_pkg_sm_sales;

/



