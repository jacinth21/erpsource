/* Formatted on 2009/11/24 17:14 (Formatter Plus v4.8.0) */

CREATE OR REPLACE PACKAGE BODY gm_pkg_sm_part_consign_info
IS
--
   /********************************************************************************
	* Description	: THIS FUNCTION RETURNS counts of part consigned to distributor
	*
	* Parameters	 : p_part_number_id, p_distributor_id
	********************************************************************************/
	FUNCTION get_part_consigned_count (
		p_part_number_id   t205_part_number.c205_part_number_id%TYPE
	  , p_distributor_id   t701_distributor.c701_distributor_id%TYPE
	)
		RETURN NUMBER
	IS
		v_value 	   NUMBER;
	BEGIN
--
		SELECT NVL (SUM (t731.c731_item_qty), 0)
		  INTO v_value
		  FROM t730_virtual_consignment t730, t731_virtual_consign_item t731
		 WHERE t730.c730_virtual_consignment_id = t731.c730_virtual_consignment_id
		   AND t731.c205_part_number_id = p_part_number_id
		   AND t730.c701_distributor_id = p_distributor_id;

		--
		RETURN v_value;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN 0;
	END get_part_consigned_count;

	/********************************************************************************
	 * Description	 : THIS FUNCTION RETURNS counts of part consigned to
	 *				   specific set
	 * Parameters	  : p_part_number_id, p_set_id, p_distributor_id
	 ********************************************************************************/
	FUNCTION get_part_set_used_count (
		p_part_number_id   t205_part_number.c205_part_number_id%TYPE
	  , p_set_id		   t207_set_master.c207_set_id%TYPE
	  , p_distributor_id   t701_distributor.c701_distributor_id%TYPE
	)
		RETURN NUMBER
	IS
		v_value 	   NUMBER;
	BEGIN
--
		SELECT NVL (SUM (t731.c731_item_qty), 0)
		  INTO v_value
		  FROM t730_virtual_consignment t730, t731_virtual_consign_item t731
		 WHERE t730.c730_virtual_consignment_id = t731.c730_virtual_consignment_id
		   AND t731.c205_part_number_id = p_part_number_id
		   AND t730.c701_distributor_id = p_distributor_id
		   AND t730.c207_set_id = p_set_id;

		--
		RETURN v_value;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN 0;
	END get_part_set_used_count;

	/********************************************************************************
	 * Description	 : THIS FUNCTION RETURNS counts of set available for selected set
	 *
	 * Parameters	  : p_set_id, p_distributor_id
	 ********************************************************************************/
	FUNCTION get_set_used_count (
		p_set_id		   t207_set_master.c207_set_id%TYPE
	  , p_distributor_id   t701_distributor.c701_distributor_id%TYPE
	)
		RETURN NUMBER
	IS
		v_value 	   NUMBER;
	BEGIN
--
		SELECT COUNT (1)
		  INTO v_value
		  FROM t730_virtual_consignment t730
		 WHERE t730.c701_distributor_id = p_distributor_id AND t730.c207_set_id = p_set_id;

		--
		RETURN v_value;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN 0;
	END get_set_used_count;

	/********************************************************************************
	 * Description	 : THIS FUNCTION RETURNS counts of part not used in any set (Extra)
	 *				  1603 -- maps to inassigned parts
	 * Parameters	  : p_part_number_id,  p_distributor_id
	 ********************************************************************************/
	FUNCTION get_part_unused_count (
		p_part_number_id   t205_part_number.c205_part_number_id%TYPE
	  , p_distributor_id   t701_distributor.c701_distributor_id%TYPE
	)
		RETURN NUMBER
	IS
		v_value 	   NUMBER;
	BEGIN
--
		SELECT NVL (SUM (t731.c731_item_qty), 0)
		  INTO v_value
		  FROM t730_virtual_consignment t730, t731_virtual_consign_item t731, t207_set_master t207
		 WHERE t730.c730_virtual_consignment_id = t731.c730_virtual_consignment_id
		   AND t731.c205_part_number_id = p_part_number_id
		   AND t730.c701_distributor_id = p_distributor_id
		   AND t730.c207_set_id = t207.c207_set_id
		   AND t207.c901_set_grp_type IN (1603, 1604);

		--
		RETURN v_value;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN 0;
	END get_part_unused_count;

	/********************************************************************************
	 * Description	 : THIS FUNCTION RETURNS counts of part used by other set
	 *				   specific set
	 * Parameters	  : p_part_number_id, p_set_id, p_distributor_id
	 ********************************************************************************/
	FUNCTION get_part_crossover_count (
		p_part_number_id   t205_part_number.c205_part_number_id%TYPE
	  , p_set_id		   t207_set_master.c207_set_id%TYPE
	  , p_distributor_id   t701_distributor.c701_distributor_id%TYPE
	)
		RETURN NUMBER
	IS
		v_value 	   NUMBER;
	BEGIN
		--
		SELECT NVL (SUM (t731.c731_item_qty), 0)
		  INTO v_value
		  FROM t730_virtual_consignment t730, t731_virtual_consign_item t731, t207_set_master t207
		 WHERE t730.c730_virtual_consignment_id = t731.c730_virtual_consignment_id
		   AND t731.c205_part_number_id = p_part_number_id
		   AND t730.c701_distributor_id = p_distributor_id
		   AND t730.c207_set_id <> p_set_id
		   AND t730.c207_set_id = t207.c207_set_id
		   AND t207.c901_set_grp_type = 1601;

		--
		RETURN v_value;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN 0;
	END get_part_crossover_count;

	/*********************************************************
	 * Description	 : THIS FUNCTION RETURNS 'Y' if the part is
	 *			   used in multiple project
	 * Parameters	  : p_part_number_id
	 **********************************************************/
	FUNCTION get_crossover_part_fl (
		p_part_number_id   t205_part_number.c205_part_number_id%TYPE
	)
		RETURN CHAR
	IS
		v_value 	   CHAR (1);
	BEGIN
--
		SELECT	 'Y'
			INTO v_value
			FROM t207_set_master t207, t208_set_details t208
		   WHERE t207.c207_set_id = t208.c207_set_id
			 AND t207.c901_set_grp_type = 1601
			 AND t208.c208_critical_fl = 'Y'
			 AND t208.c205_part_number_id = p_part_number_id
		GROUP BY t208.c205_part_number_id
		  HAVING COUNT (1) > 1;

		--
		RETURN v_value;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN 'N';
	END get_crossover_part_fl;

--

	/*********************************************************
	 * Description	 : THIS FUNCTION RETURNS SET ID based on the
	 *			   parent set od
	 * Parameters	  : set id
	 **********************************************************/
	FUNCTION get_current_set_id (
		p_set_id   t207_set_master.c207_set_id%TYPE
	)
		RETURN VARCHAR2
	IS
		v_value 	   t207_set_master.c207_set_id%TYPE;
	BEGIN
--
		SELECT t207.c207_set_id
		  INTO v_value
		  FROM t207_set_master t207, t207a_set_link t207a
		 WHERE t207.c207_set_id = t207a.c207_link_set_id
		   AND t207a.c207_main_set_id = p_set_id
		   AND t207a.c901_type = 20003
		   AND ROWNUM = 1;

		--
		RETURN v_value;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN '-';
	END get_current_set_id;

	/*********************************************************
	 * Description	 : THIS FUNCTION RETURNS Set Count based on the
	 *					parent set id
	 * Parameters	  : set id
	 **********************************************************/
	FUNCTION get_current_set_count (
		p_set_id   t207_set_master.c207_set_id%TYPE
	)
		RETURN NUMBER
	IS
		v_value 	   NUMBER;
	BEGIN
--
		SELECT COUNT (1)
		  INTO v_value
		  FROM t207a_set_link t207a
		 WHERE t207a.c207_main_set_id = p_set_id AND t207a.c901_type = 20003;

		--
		RETURN v_value;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN 0;
	END get_current_set_count;

	/*********************************************************
	  * Name		  : gm_pd_fch_virVsact_part_sch
	  * Description   : Procedure to report Virtual vs Actual Part information
	  * Parameters	  : Part Number
	  **********************************************************/
	PROCEDURE gm_pd_fch_virvsact_part_sch (
		p_set_id					  t207_set_master.c207_set_id%TYPE
	  , p_part_number_id	 IN 	  t205_part_number.c205_part_number_id%TYPE
	  , p_virvsactpartinfo	 OUT	  TYPES.cursor_type
	)
	AS
	BEGIN
		--
		OPEN p_virvsactpartinfo
		 FOR
			 --
			 SELECT   t701.c701_distributor_id did, t701.c701_distributor_name sname, NVL (vset.vcount, 0) vcount
					, p_set_id SID, get_log_flag (t701.c701_distributor_id || 'R' || p_set_id, 1207) LOG, 'D' diffflag
					, NVL (cset.ccount, 0) COUNT, NVL (rset.rcount, 0) rcount
					, (NVL (cset.ccount, 0) - NVL (rset.rcount, 0)) fcount, '' pline
				 FROM t701_distributor t701
					, (SELECT	c701_distributor_id, COUNT (1) vcount
						   FROM t730_virtual_consignment t730
						  WHERE c207_set_id = p_set_id
					   GROUP BY c701_distributor_id) vset
					, (SELECT	c701_distributor_id, SUM (t505.c505_item_qty) ccount
						   FROM t504_consignment t504, t505_item_consignment t505
						  WHERE t504.c504_consignment_id = t505.c504_consignment_id
							AND t504.c504_status_fl = 4
							AND t504.c504_type IN (4110, 4129)
							AND TRIM (t505.c505_control_number) IS NOT NULL
							AND t505.c205_part_number_id = p_part_number_id
							AND t504.c504_void_fl IS NULL
					   GROUP BY c701_distributor_id) cset
					, (SELECT	c701_distributor_id, SUM (t507.c507_item_qty) rcount
						   FROM t506_returns t506, t507_returns_item t507
						  WHERE t506.c506_rma_id = t507.c506_rma_id
							AND t506.c506_status_fl = 2
							AND TRIM (t507.c507_control_number) IS NOT NULL
							AND t507.c205_part_number_id = p_part_number_id
							AND t506.c506_void_fl IS NULL
					   GROUP BY c701_distributor_id) rset
				WHERE t701.c701_distributor_id = vset.c701_distributor_id(+) AND t701.c701_distributor_id = cset.c701_distributor_id(+)
					  AND t701.c701_distributor_id = rset.c701_distributor_id(+)
			 ORDER BY sname;
	--
	END gm_pd_fch_virvsact_part_sch;

--
/*******************************************************
	* Description : Procedure to fetch the system association
	* Parameters  :
	*******************************************************/
	PROCEDURE gm_fch_systemmapping (
		p_outsysmap   OUT	TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_outsysmap
		 FOR
			 SELECT c906_rule_id main, c906_rule_value asst
			   FROM t906_rules
			  WHERE c906_rule_grp_id = 'SYSMAP';
	END gm_fch_systemmapping;
END gm_pkg_sm_part_consign_info;
