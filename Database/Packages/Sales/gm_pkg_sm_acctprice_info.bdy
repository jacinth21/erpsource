--@"C:\Database\Packages\Sales\gm_pkg_sm_acctprice_info.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_sm_acctprice_info
IS
   /*******************************************************
   * Description : Procedure to fetch all accnt price for first time/update sync.
   * Author      : Ritesh
   *******************************************************/
--
   PROCEDURE gm_fch_all_acctprice (
      	p_token   IN t101a_user_token.c101a_token_id%TYPE,
    	p_langid  IN NUMBER,
    	p_page_no IN NUMBER,
    	p_uuid    IN t9150_device_info.c9150_device_uuid%TYPE,
    	p_acctids IN VARCHAR,
    	p_repid	  IN t703_sales_rep.c703_sales_rep_id%TYPE,
    	p_type	  IN NUMBER,
    	p_syncall IN VARCHAR,
    	p_acctprice   OUT      TYPES.cursor_type
   )
   AS
   	--v_acctids VARCHAR2(400);
   	v_deviceid  t9150_device_info.c9150_device_info_id%TYPE;
	v_count	    NUMBER;
	v_page_no   NUMBER;
	v_page_size NUMBER;
  	v_start     NUMBER;
    v_end       NUMBER;
   	v_lastsyncdt	DATE default NULL;
   	v_syncFl	VARCHAR2(2):= p_syncall;
   	
   BEGIN
      	IF p_acctids IS NULL THEN
      	    GM_RAISE_APPLICATION_ERROR('-20999','362','');
      		
      	END IF;
      	--v_acctids := p_acctids;
      	
      	 	
      	
      	--Convert Accounts Id to party Ids
      	gm_pkg_sm_acctprice_info.gm_associate_acct_party(p_acctids);
     
        --get the device info id based on user token
	  	v_deviceid := get_device_id(p_token, p_uuid);
		v_page_no := p_page_no;
		-- get the paging details
		gm_pkg_pdpc_prodcatrpt.gm_fch_paging_dtl('ACCTPRICE','PAGESIZE',v_page_no,v_start,v_end,v_page_size);
				
		/*if updated groups available then add all groups into v_inlist and return count of v_inlist records*/
		--PMT-9127 Disable pricing update to IPAD and instead Sync all the pricing
		IF v_syncFl IS NULL THEN
			SELECT get_rule_value ('SYNC_ALL_ACC_PRICE','IPAD_ACC_PRICE_SYNC') 
				INTO v_syncFl 
			FROM DUAL;
		END IF;
		
		IF v_syncFl IS NULL THEN
			gm_pkg_pdpc_prodcatrpt.gm_fch_device_updates(v_deviceid,p_langid,4000526,v_count); --4000526 - Acct price sync
		END IF;
		
		
			
		--If there is data available for device then call below prc to add all accnts.
		IF v_count > 0 THEN
			gm_pkg_sm_acctprice_info.gm_fch_acctgpoprice_tosync;
			v_lastsyncdt:= gm_pkg_pdpc_prodcatrpt.get_last_sync_date(v_deviceid, p_langid, 4000526, NULL, p_type); -- acctpricetype
		END IF;
		
		OPEN p_acctprice
	   	FOR	  
		 SELECT RESULT_COUNT totalsize,  COUNT (1) OVER () pagesize, v_page_no pageno, rwnum, CEIL (RESULT_COUNT / v_page_size) totalpages, acct_price.*  
		   FROM
		    (
		         SELECT acct_price.*, ROWNUM rwnum, COUNT (1) OVER () result_count
		           FROM
		            (                
		             SELECT apid, acctid, gpoid, pnum, grpid, grouptype, price,voidfl
	     			   FROM v_acct_price 
	    			       WHERE trunc(ldate) >= trunc(NVL(v_lastsyncdt,ldate)) OR trunc(cdate) >= trunc(NVL(v_lastsyncdt,cdate))
	    			       AND DECODE(v_count,-1,0,1) = DECODE(v_count, -1, DECODE(void_fl,0,void_fl,1),1)
	    		   )
		            acct_price
		    )
		    acct_price
		 WHERE rwnum BETWEEN v_start AND v_end;
		--get last sync date.
   END gm_fch_all_acctprice;
   /*******************************************************
   * Description : Procedure to fetch all gpo price for first time/update sync.
   * Author      : Ritesh
   *******************************************************/
--
	PROCEDURE gm_fch_all_gpoprice (
      	p_token   IN t101a_user_token.c101a_token_id%TYPE,
    	p_langid  IN NUMBER,
    	p_page_no IN NUMBER,
    	p_uuid    IN t9150_device_info.c9150_device_uuid%TYPE,
    	p_acctids IN VARCHAR,
    	p_repid	  IN t703_sales_rep.c703_sales_rep_id%TYPE,
    	p_type	  IN NUMBER,
    	p_syncall IN VARCHAR,
    	p_gpoprice   OUT      TYPES.cursor_type
   )
   AS
   	v_acctids VARCHAR2(400);
   	v_deviceid  t9150_device_info.c9150_device_info_id%TYPE;
	v_count	    NUMBER;
	v_page_no   NUMBER;
	v_page_size NUMBER;
  	v_start     NUMBER;
    v_end       NUMBER;
    v_lastsyncdt		DATE;
    v_syncFl	VARCHAR2(2):= p_syncall;
     BEGIN
     	IF p_acctids IS NULL THEN
      		GM_RAISE_APPLICATION_ERROR('-20999','362','');
      	END IF;
      	v_acctids := p_acctids;
      	-- set accnt ids.
      	my_context.set_my_inlist_ctx (v_acctids); -- '7481,7701'
      	-- set the gpos to inlist
      	gm_pkg_sm_acctprice_info.gm_fch_accts_gpo;
        --get the device info id based on user token
	  	v_deviceid := get_device_id(p_token, p_uuid);
		v_page_no := p_page_no;
		-- get the paging details
		gm_pkg_pdpc_prodcatrpt.gm_fch_paging_dtl('GPOPRICE','PAGESIZE',v_page_no,v_start,v_end,v_page_size);
				
		IF v_syncFl IS NULL THEN
		/*if updated groups available then add all groups into v_inlist and return count of v_inlist records*/
			gm_pkg_pdpc_prodcatrpt.gm_fch_device_updates(v_deviceid,p_langid,4000524,v_count); --4000524 - GPO price sync
		END IF;	
			
		
		--If there is data available for device then call below prc to add all accnts.
		IF v_count > 0 THEN
			gm_pkg_sm_acctprice_info.gm_fch_acctgpoprice_tosync;
			v_lastsyncdt:= gm_pkg_pdpc_prodcatrpt.get_last_sync_date(v_deviceid, p_langid, 4000524, NULL, p_type); --4000524 gpopricetype
		END IF;
			-- v_last_dt := TO_DATE(v_lastsyncdt,'MM/dd/YYYY');
		OPEN p_gpoprice
	   	FOR	  
		 SELECT RESULT_COUNT totalsize,  COUNT (1) OVER () pagesize, v_page_no pageno, rwnum, CEIL (RESULT_COUNT / v_page_size) totalpages, acct_price.*  
		   FROM
		    (
		         SELECT acct_price.*, ROWNUM rwnum, COUNT (1) OVER () result_count
		           FROM
		            (                
		             SELECT apid, acctid, gpoid, pnum, grpid, grouptype, price,voidfl
	     			   FROM v_gpo_price
	 					WHERE trunc(ldate) >= trunc(NVL(v_lastsyncdt,ldate)) OR trunc(cdate) >= trunc(NVL(v_lastsyncdt,cdate))
	 					AND DECODE(v_count,-1,0,1) = DECODE(v_count, -1, DECODE(void_fl,0,void_fl,1),1)
		            )
		            acct_price
		    )
		    acct_price
		 WHERE rwnum BETWEEN v_start AND v_end;
		
   END gm_fch_all_gpoprice;
   
   /*******************************************************
   * Description : Procedure to fetch all the acct to be synced first time.
   * Author      : Ritesh
   *******************************************************/
   PROCEDURE gm_fch_acctgpoprice_tosync
   AS
   	v_acctgpostr VARCHAR2(500);
   	CURSOR c_accts IS
   	SELECT v_list.token
   	  FROM v_in_list v_list, my_temp_prod_cat_info temp
   	 WHERE v_list.token IS NOT NULL
   	   AND v_list.token = temp.ref_id;   	   
   BEGIN
	   FOR v_acctgpo IN c_accts
	   LOOP
	   		v_acctgpostr := v_acctgpostr || v_acctgpo.token||',';
	   END LOOP;
	   my_context.set_my_inlist_ctx (v_acctgpostr);	  
   END gm_fch_acctgpoprice_tosync;
   /*******************************************************
   * Description : This method is used the fetch the gpos for respective accounts.
   * Author      : Ritesh
   *******************************************************/
   PROCEDURE gm_fch_accts_gpo
   AS
   	 v_acctgpostr VARCHAR2(500);  
   	 CURSOR c_gpo IS
   	   SELECT t740.c101_party_id gpoid
   	     FROM t740_gpo_account_mapping t740, v_in_list v_list
   	    WHERE t740.c704_account_id = v_list.token
   	      AND t740.c740_void_fl IS NULL
   	      AND v_list.token IS NOT NULL;
   	   
	   BEGIN
		   FOR v_acctgpo IN c_gpo
		   LOOP
		   		v_acctgpostr := v_acctgpostr || v_acctgpo.gpoid||',';
		   END LOOP;
	   my_context.set_my_inlist_ctx (v_acctgpostr);
	   END gm_fch_accts_gpo; 
	   
	   /*******************************************************
   * Description : This method is used the fetch the party ids  for respective accounts.
   * Author      : Mihir
   *******************************************************/
   PROCEDURE gm_associate_acct_party(
   p_acctids IN VARCHAR
   )
   AS
   	 v_refidsstr VARCHAR2(4000);  
   	 CURSOR refid_cur IS
   	   SELECT t704.c101_party_id partyid,t704.c704_account_id acctid
   	     FROM t704_account t704, v_in_list v_list
   	    WHERE t704.c704_account_id = v_list.token
   	     AND v_list.token IS NOT NULL;
   	   
	   BEGIN
		   -- set account ids.
      	   my_context.set_my_inlist_ctx (p_acctids); -- '7481,7701'
      	
      		--
		   FOR v_refid IN refid_cur
		   LOOP
		   		v_refidsstr := v_refidsstr || v_refid.acctid||','||v_refid.partyid||'|';
		   END LOOP;
		   	--Set Account and party IDs  as '6670,7099|6660,7099|'
      		my_context.set_double_inlist_ctx(v_refidsstr);
		   
	  END gm_associate_acct_party; 
END gm_pkg_sm_acctprice_info;
/
