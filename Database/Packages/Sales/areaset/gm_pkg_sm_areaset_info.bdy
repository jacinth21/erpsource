/* Formatted on 2011/10/26 17:13 (Formatter Plus v4.8.0) */
--@"C:\Database\Packages\Sales\areaset\gm_pkg_sm_areaset_info.bdy";
CREATE OR REPLACE
PACKAGE BODY gm_pkg_sm_areaset_info
IS
    --
    /*********************************
    Purpose: This procedure is used to save ship status.
    **********************************/
PROCEDURE gm_fch_areaset_rejectinfo (
        p_type     IN VARCHAR2,
        p_inputstr IN VARCHAR2,
        p_user_id  IN t907_shipping_info.c907_accepted_by%TYPE,
        p_reject_cur OUT TYPES.cursor_type)
AS
    v_strlen    NUMBER          := NVL (LENGTH (p_inputstr), 0) ;
    v_string    VARCHAR2 (4000) := p_inputstr;
    v_substring VARCHAR2 (1000) ;
    v_rep_id t907_shipping_info.c907_ship_to_id%TYPE;
    v_user_id t907_shipping_info.c907_accepted_by%TYPE;
    v_repids VARCHAR2 (4000) ;
    v_shipid t907_shipping_info.c907_shipping_id%TYPE;
    v_ship_from t907_shipping_info.c901_ship_from%TYPE;
    --
BEGIN
    v_user_id                       := p_user_id;
    IF v_strlen                      > 0 THEN
        WHILE INSTR (v_string, '|') <> 0
        LOOP
            --
            v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
            v_string    := SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
            --
            v_rep_id := NULL;
            --
            v_substring := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
            v_shipid    := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            v_substring := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
            v_substring := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
            v_rep_id    := v_substring;
             SELECT c901_ship_from
               INTO v_ship_from
               FROM t907_shipping_info
              WHERE c907_ship_from_id IN (v_rep_id)
                AND c907_shipping_id  IN (v_shipid)
                AND c907_void_fl      IS NULL;
            v_repids                  := v_repids||v_rep_id||v_ship_from||',';
        END LOOP;
        v_repids := SUBSTR (v_repids, 1, LENGTH (v_repids) - 1) ;
        my_context.set_my_inlist_ctx (v_repids) ;
        OPEN p_reject_cur FOR SELECT c5010_tag_id CODEID,
        c5010_tag_id CODENM,
        DECODE (c901_sub_location_type, '50222', c5010_sub_location_id, c703_sales_rep_id) distrepid FROM t5010_tag
        WHERE DECODE (c901_sub_location_type, '50222', c5010_sub_location_id||'4121', c703_sales_rep_id||'4121') IN
        (
             SELECT token FROM v_in_list
        )
        AND c5010_void_fl IS NULL AND c5010_location_id NOT IN ('52099', '52098') AND c901_status = 51012
          UNION
         SELECT c5010_tag_id CODEID, c5010_tag_id CODENM, c5010_location_id distrepid
           FROM t5010_tag
          WHERE DECODE (c901_sub_location_type, '50221', c5010_sub_location_id||'4120', '') IN
            (
                 SELECT token FROM v_in_list
            )
            OR (c5010_location_id||'4120') IN
            (
                 SELECT token FROM v_in_list
            )
            AND c5010_void_fl   IS NULL
            AND c5010_location_id NOT IN ('52099', '52098')
            AND c901_status      = 51012;
    END IF; --end of str length=0
END gm_fch_areaset_rejectinfo;
END gm_pkg_sm_areaset_info;
/