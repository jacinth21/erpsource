/* Formatted on 2011/12/19 17:07 (Formatter Plus v4.8.0) */
--@"C:\Database\Packages\Sales\areaset\gm_pkg_sm_areaset_txn.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_sm_areaset_txn
IS
	--
	--11400-pending shipping, 11401-Receive shipping, 11402-pending return, 11403-receive return, 40-shipped
	--11460-Received, 11461-Rejected ,11462-Pending Receiving,5030-Handdeliver, 5031-N/A
	--4120 Distributor, 4121 Sales Rep
	/*********************************
	Purpose: This procedure is used to save ship status.
	**********************************/
	PROCEDURE gm_sav_areaset_shipinfo (
		p_type			 IN 	  VARCHAR2
	  , p_inputstr		 IN 	  VARCHAR2
	  , p_user_id		 IN 	  t907_shipping_info.c907_accepted_by%TYPE
	  , p_ship_carrier	 IN 	  t907_shipping_info.c901_delivery_carrier%TYPE
	  , p_ship_mode 	 IN 	  t907_shipping_info.c901_delivery_mode%TYPE
	  , p_tracking_no	 IN 	  t907_shipping_info.c907_tracking_number%TYPE
	  , p_shipout_cur	 OUT	  TYPES.cursor_type
	)
	AS
		v_strlen	   NUMBER := NVL (LENGTH (p_inputstr), 0);
		v_string	   VARCHAR2 (4000) := p_inputstr;
		v_substring    VARCHAR2 (1000);
		v_tagid 	   t5010_tag.c5010_tag_id%TYPE;
		v_shipid	   t907_shipping_info.c907_shipping_id%TYPE;
		v_refid 	   t907_shipping_info.c907_ref_id%TYPE;
		v_status	   t907_shipping_info.c907_status_fl%TYPE;
		v_accept_status t907_shipping_info.c901_accept_status%TYPE;
		v_rep_id	   t907_shipping_info.c907_ship_to_id%TYPE;
		v_address_id   t907_shipping_info.c106_address_id%TYPE;
		v_user_id	   t907_shipping_info.c907_accepted_by%TYPE;
		v_delivery_mode t907_shipping_info.c901_delivery_mode%TYPE;
		v_ship_carrier t907_shipping_info.c901_delivery_carrier%TYPE;
		--v_frieght_amt t907_shipping_info.c907_frieght_amt%TYPE;
		v_tracking_no  t907_shipping_info.c907_tracking_number%TYPE;
		v_case_info_id t7104_case_set_information.c7100_case_info_id%TYPE;
		v_ship_to	   t907_shipping_info.c901_ship_to%TYPE;
		v_ship_ids	   VARCHAR2 (2000);
		v_case_status  t7100_case_information.c901_case_status%TYPE;
		v_shipped_del_fl t7104_case_set_information.c7104_shipped_del_fl%TYPE;
	--
	BEGIN
		v_user_id	:= p_user_id;

		IF p_type = '11400' OR p_type = '11402'
		THEN
			v_status	:= '40';
			v_accept_status := '11462';

			IF p_ship_mode IS NULL
			THEN
				v_delivery_mode := 5030;
			ELSE
				v_delivery_mode := p_ship_mode;
				v_ship_carrier := p_ship_carrier;
				--v_frieght_amt   := p_frieght_amt;
				v_tracking_no := p_tracking_no;
			END IF;
		ELSIF p_type = '11401' OR p_type = '11403'
		THEN
			v_status	:= '40';
			v_accept_status := '11460';
		END IF;

		IF v_strlen > 0
		THEN
			WHILE INSTR (v_string, '|') <> 0
			LOOP
				--
				v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
				v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
				--
				v_tagid 	:= NULL;
				v_shipid	:= NULL;
				v_refid 	:= NULL;
				--
				v_tagid 	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_shipid	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_refid 	:= v_substring;

				UPDATE t907_shipping_info
				   SET c907_status_fl = v_status
					 , c901_accept_status = v_accept_status
					 , c907_accepted_by = v_user_id
					 , c907_accepted_date = SYSDATE
					 , c907_last_updated_by = v_user_id
					 , c907_last_updated_date = SYSDATE
					 , c901_delivery_mode = DECODE (v_delivery_mode, 0, c901_delivery_mode, v_delivery_mode)
					 , c901_delivery_carrier = DECODE (v_ship_carrier, 0, c901_delivery_carrier, v_ship_carrier)
					 ,
					   --c907_frieght_amt	   = DECODE (v_frieght_amt, NULL, c907_frieght_amt, v_frieght_amt),
					   c907_tracking_number = DECODE (v_tracking_no, NULL, c907_tracking_number, v_tracking_no)
				 WHERE c907_shipping_id = v_shipid AND c907_void_fl IS NULL;

				IF p_type = '11400' OR p_type = '11402'
				THEN
					v_ship_ids	:= v_ship_ids || v_shipid || ',';

					BEGIN
						SELECT c901_ship_to, c907_ship_to_id, c106_address_id
						  INTO v_ship_to, v_rep_id, v_address_id
						  FROM t907_shipping_info
						 WHERE c907_shipping_id = v_shipid AND c907_void_fl IS NULL;
					EXCEPTION
						WHEN NO_DATA_FOUND
						THEN
							raise_application_error (-20677, '');
					END;

					IF v_ship_to = '4121'
					THEN   --FOR SALES REP
						gm_pkg_sm_areaset_txn.gm_sav_tagset_inv_location (v_tagid
																		, v_rep_id
																		, '50222'
																		, v_rep_id
																		, v_address_id
																		, v_user_id
																		 );
					ELSIF v_ship_to = '4120'
					THEN   --FOR DISTRIBUTOR
						gm_pkg_sm_areaset_txn.gm_sav_tagset_inv_location (v_tagid
																		, NULL
																		, '50221'
																		, v_rep_id
																		, v_address_id
																		, v_user_id
																		 );
					ELSIF v_ship_to = '4122'
					THEN   --FOR Account
						gm_pkg_sm_areaset_txn.gm_sav_tagset_inv_location (v_tagid
																		, NULL
																		, '11301'
																		, v_rep_id
																		, v_address_id
																		, v_user_id
																		 );
					END IF;
				END IF;

				--MOVING THE NEXT RECORD TO PENDING SHIPPING STATUS
				IF p_type = '11401'
				THEN
					UPDATE t907_shipping_info
					   SET c907_status_fl = 30
						 , c901_accept_status = NULL
					 WHERE c907_shipping_id IN (
							   SELECT c907_shipping_id
								 FROM t907_shipping_info
								WHERE c907_shipping_id NOT IN (v_shipid)
								  AND c907_ref_id IN (v_refid)
								  AND c907_void_fl IS NULL)
					   AND c907_void_fl IS NULL;
				END IF;
			--VERIFY TO CLOSE THE CASE
			IF p_type = '11403' and v_status = '40' and v_accept_status = '11460' THEN
			select c7100_case_info_id, c7104_shipped_del_fl into v_case_info_id, v_shipped_del_fl from t7104_case_set_information where c7104_case_set_id in (
			v_refid)  and c7104_void_fl is null;
			
				IF v_case_info_id IS NOT NULL AND v_shipped_del_fl IS NULL THEN
				gm_pkg_sm_casebook_txn.gm_check_and_close_case(v_case_info_id);
				END IF;
				v_case_status := get_case_status(v_case_info_id);
				IF v_shipped_del_fl = 'Y' THEN
					IF  v_case_status = 11091 THEN
						UPDATE t7104_case_set_information
						   SET c7104_void_fl = 'Y'
							 , c7104_last_updated_by = p_user_id
							 , c7104_last_updated_date = SYSDATE
						 WHERE c7104_case_set_id = v_refid
						   AND c7104_void_fl IS NULL;
					 ELSE
					 	UPDATE t7104_case_set_information
						   SET c7104_shipped_del_fl = NULL
						     , c5010_tag_id = NULL
						     , c901_set_location_type = NULL
						     , c526_product_request_detail_id = NULL 
						     , c7104_last_updated_by = p_user_id
							 , c7104_last_updated_date = SYSDATE
						 WHERE c7104_case_set_id IN (v_refid)
						   AND c7104_shipped_del_fl = 'Y'
						   AND c7104_void_fl IS NULL;
					 END IF;
				END IF;
			END IF;
			END LOOP;

			--for sending mail for hand deliver in pending shipping and pending return
			IF p_type = '11400' OR p_type = '11402'
			THEN
				my_context.set_my_inlist_ctx (v_ship_ids);

				OPEN p_shipout_cur
				 FOR
					 SELECT   t7100.c7100_case_id caseid, t7104.c207_set_id setid, t207.c207_set_nm setname
							, get_account_name (t7100.c704_account_id) ACCOUNT
							, get_dist_rep_name (t907.c907_ship_to_id) repname
							, get_dist_rep_name (t907.c907_ship_to_id)
							, (SELECT emailid
								 FROM v7100_ship_address
								WHERE shiptoid = t907.c907_ship_to_id AND refid = t907.c907_ref_id) shiptoemail
							, (SELECT emailid
								 FROM v7100_ship_address
								WHERE shiptoid = t907.c907_ship_from_id AND refid = t907.c907_ref_id) shipfromemail
							, t7104.c5010_tag_id currtag
							, DECODE (t907.c907_status_fl
									, 30, get_code_name (11450)
									, 40, get_code_name (11451)
									, t907.c907_status_fl
									 ) reason
							, get_rule_value ('AREASET', 'E-MAIL') email, t907.c907_ship_to_id shipfromid
							, get_code_name (t907.c901_delivery_mode) delmode
							, get_code_name (t907.c901_delivery_carrier) carrier, t907.c907_frieght_amt amount
							, t907.c907_tracking_number tno
						 FROM t907_shipping_info t907
							, t7104_case_set_information t7104
							, t7100_case_information t7100
							, t207_set_master t207
						WHERE t7104.c7104_case_set_id = t907.c907_ref_id
						  AND t7104.c7104_void_fl IS NULL
						  AND t7100.c7100_case_info_id = t7104.c7100_case_info_id
						  AND t7100.c901_type          = 1006503    -- CASE
						  AND t7104.c207_set_id = t207.c207_set_id
						  AND t207.c207_void_fl IS NULL
						  AND c907_shipping_id IN (SELECT token
													 FROM v_in_list)
						  AND c907_void_fl IS NULL
					 ORDER BY t907.c907_ship_from_id;
			END IF;
		END IF;   --end of str length=0
	END gm_sav_areaset_shipinfo;

	PROCEDURE gm_sav_set_shipping (
		p_refid 		  IN   t907_shipping_info.c907_ref_id%TYPE
	  , p_repid 		  IN   t907_shipping_info.c907_ship_to_id%TYPE
	  , p_addressid 	  IN   t907_shipping_info.c106_address_id%TYPE
	  , p_new_addressid   IN   t907_shipping_info.c106_address_id%TYPE
	  , p_stropt			   NUMBER
	  , p_userid		  IN   t907_shipping_info.c907_last_updated_by%TYPE
	  , p_new_repid 	  IN   t907_shipping_info.c907_ship_to_id%TYPE
	  , p_shiptype		  IN   t907_shipping_info.c901_ship_to%TYPE
	  , p_new_shiptype	  IN   t907_shipping_info.c901_ship_to%TYPE
	  , p_mode		  	  IN   t907_shipping_info.c901_delivery_mode%TYPE
	  , p_allset	  	  IN   VARCHAR2 DEFAULT 'N'
	)
	AS
	v_ship_cnt	NUMBER := 0;
	v_req_id	t907_shipping_info.c525_product_request_id%TYPE;
	BEGIN
		IF (p_stropt = 11400 OR p_stropt = 11401 OR p_stropt = 11403)
		THEN
			UPDATE t907_shipping_info
			   SET c106_address_id = p_new_addressid
				 , c901_ship_to = p_new_shiptype
				 , c907_ship_to_id = p_new_repid
				 , c901_delivery_mode = NVL(p_mode, c901_delivery_mode)
				 , c907_last_updated_by = p_userid
				 , c907_last_updated_date = SYSDATE
			 WHERE c907_ref_id = p_refid
			   AND c907_void_fl IS NULL
			   AND c901_source = decode(p_stropt,11400, 11388, 11401, 11385, 11403,11388,c901_source);
        ELSIF (p_stropt = 11401)
        THEN
			UPDATE t907_shipping_info
			   SET c106_address_id = p_new_addressid
			     , c901_ship_from = p_new_shiptype
				 , c907_ship_from_id = p_new_repid
				 , c901_delivery_mode = NVL(p_mode, c901_delivery_mode)
				 , c907_last_updated_by = p_userid
				 , c907_last_updated_date = SYSDATE
			 WHERE c907_ref_id = p_refid 
			   AND c907_void_fl IS NULL
			   AND c901_source = 11388;	
		ELSIF (p_stropt = 11381) --Loaner
        THEN
	        BEGIN
				SELECT c525_product_request_id 
				INTO v_req_id
				FROM t907_shipping_info WHERE c907_ref_id = p_refid 
					AND c907_void_fl IS null AND c907_status_fl <> '40';
			EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
			      v_req_id := null;
			END;

			SELECT COUNT(1) INTO v_ship_cnt
				FROM t907_shipping_info 
			WHERE c907_ref_id = p_refid AND 
				c907_void_fl IS null AND c907_status_fl = '40' 
				AND c525_product_request_id = v_req_id;
			
			IF v_ship_cnt > 0 THEN
			   GM_RAISE_APPLICATION_ERROR('-20999','359','');
			   
			END IF;
			
			IF p_allset = 'Y' THEN
				BEGIN
					-- Update the Ship To in T525_product_request
			    	UPDATE t525_product_request
					   SET c901_ship_to = p_new_shiptype, 
					       c525_last_updated_by = p_userid, 
					       c525_last_updated_date = SYSDATE
					 WHERE c525_product_request_id = v_req_id
					   AND c525_void_fl           IS NULL
					   AND c525_status_fl NOT     IN (40);

					-- Update the Shipping Records to Parent Request
		            UPDATE t907_shipping_info
					   SET c106_address_id = p_new_addressid
					     , c901_ship_to = p_new_shiptype
						 , c907_ship_to_id = p_new_repid
						 , c901_delivery_mode = NVL(p_mode, c901_delivery_mode)
						 , c907_last_updated_by = p_userid
						 , c907_last_updated_date = SYSDATE
					 WHERE c907_ref_id = v_req_id 
					   AND c907_void_fl IS NULL
					   AND c907_status_fl NOT IN (40) --closed
					   AND c901_source = 50185;
					-- Update the shipping records to All the Request
		              UPDATE t907_shipping_info
					   SET c106_address_id = p_new_addressid
					     , c901_ship_to = p_new_shiptype
						 , c907_ship_to_id = p_new_repid
						 , c901_delivery_mode = NVL(p_mode, c901_delivery_mode) 
						 , c907_last_updated_by = p_userid
						 , c907_last_updated_date = SYSDATE
					 WHERE c525_product_request_id = v_req_id 
					   AND c907_void_fl IS NULL
					   AND c907_status_fl NOT IN (40) --closed
					   AND c901_source = 50182;
				EXCEPTION
				    WHEN NO_DATA_FOUND THEN
				    v_req_id := null;
				END;
			ELSE
				-- Update the Shipping Record for the corresponding request
				UPDATE t907_shipping_info
				   SET c106_address_id = p_new_addressid
				     , c901_ship_to = p_new_shiptype
					 , c907_ship_to_id = p_new_repid
					 , c901_delivery_mode = NVL(p_mode, c901_delivery_mode) 
					 , c907_last_updated_by = p_userid
					 , c907_last_updated_date = SYSDATE
				 WHERE c907_ref_id = p_refid 
				   AND c907_void_fl IS NULL
				   AND c907_status_fl NOT IN (40) --closed
				   AND c901_source = 50182;	
			   END IF;
		END IF;
	END gm_sav_set_shipping;

	PROCEDURE gm_sav_areaset_rejectinfo (
		p_type		   IN		VARCHAR2
	  , p_inputstr	   IN		VARCHAR2
	  , p_user_id	   IN		t907_shipping_info.c907_accepted_by%TYPE
	  , p_status	   IN		VARCHAR2
	  , p_rep_id	   IN		t907_shipping_info.c907_ship_to_id%TYPE
	  , p_case_id	   IN		t7100_case_information.c7100_case_id%TYPE
	  , p_reject_cur   OUT		TYPES.cursor_type
	)
	AS
		v_strlen	   NUMBER := NVL (LENGTH (p_inputstr), 0);
		v_string	   VARCHAR2 (4000) := p_inputstr;
		v_substring    VARCHAR2 (1000);
		v_rep_id	   t907_shipping_info.c907_ship_to_id%TYPE;
		v_user_id	   t907_shipping_info.c907_accepted_by%TYPE;
		v_tagid 	   t5010_tag.c5010_tag_id%TYPE;
		v_recv_tagid   t5010_tag.c5010_tag_id%TYPE;
		v_shipid	   t907_shipping_info.c907_shipping_id%TYPE;
		v_refid 	   t907_shipping_info.c907_ref_id%TYPE;
		v_status	   t907_shipping_info.c907_status_fl%TYPE;
		v_accept_status t907_shipping_info.c901_accept_status%TYPE;
		v_address_id   t907_shipping_info.c106_address_id%TYPE;
		v_count 	   NUMBER;
		v_rec_tagids   VARCHAR2 (2000);
		v_rej_tagids   VARCHAR2 (2000);
		v_ship_ids	   VARCHAR2 (2000);
		v_ship_to	   t907_shipping_info.c901_ship_to%TYPE;
		v_ship_from    t907_shipping_info.c901_ship_from%TYPE;
	--
	BEGIN
		v_user_id	:= p_user_id;

		IF p_type = '11401' OR p_type = '11403'
		THEN
			v_status	:= '30';
			v_accept_status := NULL;
		END IF;

		IF v_strlen > 0
		THEN
			WHILE INSTR (v_string, '|') <> 0
			LOOP
				--
				v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
				v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
				--
				v_tagid 	:= NULL;
				v_shipid	:= NULL;
				v_refid 	:= NULL;
				--
				v_tagid 	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_shipid	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_refid 	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_rej_tagids := v_rej_tagids || v_tagid || ',';
				v_ship_ids	:= v_ship_ids || v_shipid || ',';
	
				SELECT COUNT (1)
				  INTO v_count
				  FROM t7100_case_information t7100, t7104_case_set_information t7104
				 WHERE t7100.c7100_case_id = p_case_id
				   AND t7100.c7100_case_info_id = t7104.c7100_case_info_id
				   AND t7100.c901_type          = 1006503    -- CASE
				   AND t7104.c7104_shipped_del_fl IS NOT NULL
				   AND t7104.c7104_void_fl IS NULL
				   AND t7104.c5010_tag_id = v_tagid;
	
				IF (v_count > 0 AND p_type = '11401')
				THEN
				GM_RAISE_APPLICATION_ERROR('-20999','343','');
					raise_application_error ('-20000'
										   , ''
											);
				END IF;

				IF p_status = '11481'
				THEN
					v_rep_id	:= v_substring;
				ELSIF p_status = '11480'
				THEN
					v_rep_id	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
					v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
					v_recv_tagid := v_substring;
					v_rec_tagids := v_rec_tagids || v_recv_tagid || ',';

					BEGIN
						SELECT c901_ship_from
						  INTO v_ship_from
						  FROM t907_shipping_info
						 WHERE c907_ship_from_id IN (v_rep_id) AND c907_shipping_id IN (v_shipid)
							   AND c907_void_fl IS NULL;
					EXCEPTION
						WHEN NO_DATA_FOUND
						THEN
							raise_application_error (-20677, '');
					END;

					SELECT COUNT (1)
					  INTO v_count
					  FROM t5010_tag 
					 WHERE c5010_tag_id = v_recv_tagid AND c5010_void_fl   IS NULL  AND c5010_location_id NOT IN ('52099', '52098')  AND c901_status = 51012;

					IF (v_count = 0)
					THEN
					    GM_RAISE_APPLICATION_ERROR('-20999','360', v_recv_tagid);
						
					END IF;
				END IF;

				UPDATE t907_shipping_info
				   SET c907_status_fl = v_status
					 , c901_accept_status = v_accept_status
					 , c907_accepted_by = v_user_id
					 , c907_accepted_date = SYSDATE
					 , c907_last_updated_by = v_user_id
					 , c907_last_updated_date = SYSDATE
					 , c901_delivery_mode = NULL
					 , c901_delivery_carrier = NULL
					 , c907_frieght_amt = NULL
					 , c907_tracking_number = NULL
				 WHERE c907_shipping_id = v_shipid AND c907_void_fl IS NULL;

				BEGIN
					 SELECT c907_ship_to_id,c106_address_id, c901_ship_to
					  INTO v_rep_id,v_address_id, v_ship_to
					  FROM t907_shipping_info
					 WHERE c901_source = decode(p_type, '11401',11388, '11403',11385, c901_source) 
					   AND c907_ref_id IN (v_refid) AND c907_void_fl IS NULL;
				EXCEPTION
						WHEN NO_DATA_FOUND
						THEN
							raise_application_error (-20009, '');
				END;

				IF v_ship_to = '4121'
				THEN   --FOR SALES REP
					gm_pkg_sm_areaset_txn.gm_sav_tagset_inv_location (v_tagid
																	, v_rep_id
																	, '50222'
																	, v_rep_id
																	, v_address_id
																	, v_user_id
																	 );
				ELSIF v_ship_to = '4120'
				THEN   --FOR DISTRIBUTOR
					gm_pkg_sm_areaset_txn.gm_sav_tagset_inv_location (v_tagid
																	, NULL
																	, '50221'
																	, v_rep_id
																	, v_address_id
																	, v_user_id
																	 );
				ELSIF v_ship_to = '4122'
					THEN   --FOR Account
						gm_pkg_sm_areaset_txn.gm_sav_tagset_inv_location (v_tagid
																		, NULL
																		, '11301'
																		, v_rep_id
																		, v_address_id
																		, v_user_id
																		 );
				END IF;

				IF p_status = '11480'
				THEN
					BEGIN
						SELECT c106_address_id, c901_ship_to
						  INTO v_address_id, v_ship_to
						  FROM t907_shipping_info
						 WHERE c703_sales_rep_id IN (p_rep_id, (SELECT c701_distributor_id
																FROM t101_user
															   WHERE c101_user_id = p_rep_id))
						   AND c907_ref_id IN (v_refid)
						   AND c907_void_fl IS NULL;
					EXCEPTION
						WHEN NO_DATA_FOUND
						THEN
							raise_application_error (-20009, '');
					END;

					IF v_ship_to = '4121'
					THEN   --FOR SALES REP
						gm_pkg_sm_areaset_txn.gm_sav_tagset_inv_location (v_recv_tagid
																		, p_rep_id
																		, '50222'
																		, p_rep_id
																		, v_address_id
																		, v_user_id
																		 );
					ELSIF v_ship_to = '4120'
					THEN   --FOR DISTRIBUTOR
						gm_pkg_sm_areaset_txn.gm_sav_tagset_inv_location (v_recv_tagid
																		, NULL
																		, '50221'
																		, p_rep_id
																		, v_address_id
																		, v_user_id
																		 );
					ELSIF v_ship_to = '4122'
					THEN   --FOR Account
						gm_pkg_sm_areaset_txn.gm_sav_tagset_inv_location (v_recv_tagid
																		, NULL
																		, '11301'
																		, p_rep_id
																		, v_address_id
																		, v_user_id
																		 );
					END IF;
				END IF;
			END LOOP;

			my_context.set_my_inlist_ctx (v_ship_ids);
			my_context.set_my_double_inlist_ctx (v_rej_tagids, v_rec_tagids);

			OPEN p_reject_cur
			 FOR
				 SELECT   t7100.c7100_case_id caseid, t7104.c207_set_id setid, t207.c207_set_nm setname
						, get_account_name (t7100.c704_account_id) ACCOUNT
						, get_dist_rep_name (t907.c907_ship_from_id) repname, get_dist_rep_name (t907.c907_ship_to_id)
						, (SELECT emailid
							 FROM v7100_ship_address
							WHERE shiptoid = t907.c907_ship_to_id AND refid = t907.c907_ref_id) shiptoemail
						, (SELECT emailid
							 FROM v7100_ship_address
							WHERE shiptoid = t907.c907_ship_from_id AND refid = t907.c907_ref_id) shipfromemail
						, vlist.token currtag, vlist.tokenii recetag, get_code_name (p_status) reason
						, get_rule_value ('AREASET', 'E-MAIL') email, t907.c907_ship_from_id shipfromid
					 FROM t907_shipping_info t907
						, t7104_case_set_information t7104
						, t7100_case_information t7100
						, t207_set_master t207
						, v_double_in_list vlist
					WHERE t7104.c7104_case_set_id = t907.c907_ref_id
					  AND t7104.c7104_void_fl IS NULL
					  AND t7100.c7100_case_info_id = t7104.c7100_case_info_id
					  AND t7100.c901_type          = 1006503     -- CASE
					  AND t7104.c207_set_id = t207.c207_set_id
					  AND t207.c207_void_fl IS NULL
					  AND t7104.c5010_tag_id = vlist.token(+)
					  AND c907_shipping_id IN (SELECT token
												 FROM v_in_list)
					  AND c907_void_fl IS NULL
				 ORDER BY t907.c907_ship_from_id;
		END IF;   --end of str length=0
	END gm_sav_areaset_rejectinfo;

/*********************************
   Purpose: This procedure is used to save tag current location.
**********************************/
--
	PROCEDURE gm_sav_tagset_inv_location (
		p_strtagids 	 IN   VARCHAR2
	  , p_sales_rep 	 IN   t5010_tag.c703_sales_rep_id%TYPE
	  , p_sub_loc_type	 IN   t5010_tag.c901_sub_location_type%TYPE
	  , p_sub_loc_id	 IN   t5010_tag.c5010_sub_location_id%TYPE
	  , p_addressid 	 IN   t5010_tag.c106_address_id%TYPE
	  , p_userid		 IN   t5010_tag.c5010_last_updated_by%TYPE
	)
	AS
	BEGIN
		my_context.set_my_inlist_ctx (p_strtagids);

		UPDATE t5010_tag
		   SET c901_sub_location_type = p_sub_loc_type
			 , c5010_sub_location_id = p_sub_loc_id
			 , c106_address_id = DECODE(p_sub_loc_type, 50222, DECODE(p_addressid,0,get_address_id(p_sub_loc_id),p_addressid), NULL)
			 , c5010_last_updated_by = p_userid
			 , c5010_last_updated_date = SYSDATE
		 WHERE c5010_tag_id IN (SELECT token
								  FROM v_in_list) AND c5010_void_fl IS NULL;
	-- dbms_view.REFRESH ('v5010_curr_address');
	END gm_sav_tagset_inv_location;
END gm_pkg_sm_areaset_txn;
/
