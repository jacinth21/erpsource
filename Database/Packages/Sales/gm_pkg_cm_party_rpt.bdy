CREATE OR REPLACE
PACKAGE BODY gm_pkg_cm_party_rpt
IS
  /****************************************************************
    * Description : Procedure used to fetch the US Field Sales infromation
    * Author      : Dsandeep
    *****************************************************************/
  PROCEDURE gm_fch_us_rep_list (
         p_acc_flg       IN       VARCHAR2,
         p_out           OUT      Types.cursor_type)
AS

BEGIN
    OPEN p_out FOR
    
               SELECT C703_SALES_REP_ID REPID,
                    C703_SALES_REP_ID ID,
                'S'
  || C703_SALES_REP_ID SID,
  t701.C701_DISTRIBUTOR_ID DID,
  DECODE('103097' , '103097',NVL(C703_SALES_REP_NAME_EN, C703_SALES_REP_NAME), c703_sales_rep_name) name,
  t701.c701_distributor_name dname ,
  t101.c101_party_id partyid ,
  t101.c101_first_nm
  || ' '
  || t101.c101_last_nm partyname
FROM t703_sales_rep t703 ,
  t101_party t101,
  t701_distributor t701
WHERE t703.c101_party_id       = t101.c101_party_id
AND c703_void_fl              IS NULL
AND t703.c701_distributor_id   = t701.c701_distributor_id
AND t701.c701_void_fl         IS NULL
AND t703.c1900_company_id IN
          (SELECT c906_rule_value
           FROM t906_rules
           WHERE c906_rule_id   = 'SALESREP'
           AND c906_rule_grp_id = 'US_REP'
      )
      AND t703.c703_rep_category IN
      (SELECT c906_rule_value
      FROM t906_rules
      WHERE c906_rule_id   = 'EMPLOYEE'
       AND c906_rule_grp_id = 'EXCL_CATEGORY'
       )
      AND c703_active_fl = p_acc_flg
      ORDER BY upper(name) ;

  END   gm_fch_us_rep_list;
/****************************************************************
    * Description : Procedure used to fetch the OUS Field Sales infromation
    * Author      : Dsandeep
    *****************************************************************/

PROCEDURE gm_fch_ous_sales_list (
      p_acc_flg         IN       VARCHAR2,
      p_out             OUT       Types.cursor_type)
AS

BEGIN
    OPEN p_out FOR
    
               SELECT C703_SALES_REP_ID REPID,
                    C703_SALES_REP_ID ID,
                'S'
  || C703_SALES_REP_ID SID,
  t701.C701_DISTRIBUTOR_ID DID,
  DECODE('103097' , '103097',NVL(C703_SALES_REP_NAME_EN, C703_SALES_REP_NAME), c703_sales_rep_name) name,
  t701.c701_distributor_name dname ,
  t101.c101_party_id partyid ,
  t101.c101_first_nm
  || ' '
  || t101.c101_last_nm partyname
FROM t703_sales_rep t703 ,
  t101_party t101,
  t701_distributor t701
WHERE t703.c101_party_id       = t101.c101_party_id
AND c703_void_fl              IS NULL
AND t703.c701_distributor_id   = t701.c701_distributor_id
AND t701.c701_void_fl         IS NULL
AND t703.c1900_company_id NOT IN
  (SELECT c906_rule_value
  FROM t906_rules
  WHERE c906_rule_id   = 'SALESREP'
  AND c906_rule_grp_id = 'US_REP'
  )
AND t703.c703_rep_category IN
  (SELECT c906_rule_value
  FROM t906_rules
  WHERE c906_rule_id   = 'EMPLOYEE'
  AND c906_rule_grp_id = 'EXCL_CATEGORY'
  )
AND c703_active_fl = p_acc_flg 
ORDER BY upper(name) ;

  END   gm_fch_ous_sales_list;
 /****************************************************************
    * Description : Procedure used to fetch the Employ list infromation
    * Author      : Dsandeep
    *****************************************************************/ 
  PROCEDURE gm_fch_employ_list (
        p_user_type     IN    t101_user.c901_user_type%TYPE,
        p_user_status   IN    t101_user.c901_user_status%TYPE,
        p_out           OUT   Types.cursor_type)
AS

BEGIN
    OPEN p_out FOR
    SELECT t101.C101_USER_F_NAME
	  ||' '
	  ||t101.C101_USER_L_NAME NAME,
	  t101.C101_USER_ID ID ,
	  t101.C101_PARTY_ID PARTYID
	FROM T101_USER t101 
	where  t101.C901_USER_TYPE   IN (p_user_type)--300	Employee
	and t101.c901_user_status in ( p_user_status)--Active
	order by t101.c101_user_f_name ;
   

   END   gm_fch_employ_list;
    /****************************************************************
    * Description : Procedure used to fetch the sales rep in japanese character for Event setupscreen
    * Author      : Asophia
    *****************************************************************/
   PROCEDURE gm_fch_jpn_sales_list (
      p_acc_flg         IN       VARCHAR2,
      p_company_id      IN       T1900_COMPANY.C1900_COMPANY_ID%TYPE,
      p_comp_lang_id    IN       T1900_COMPANY.C901_LANGUAGE_ID%TYPE,
      p_out             OUT       Types.cursor_type)
AS

BEGIN
	
    OPEN p_out FOR
			    SELECT C703_SALES_REP_ID REPID,
               	 C703_SALES_REP_ID ID,
                	'S'
				  || C703_SALES_REP_ID SID,
				  t701.C701_DISTRIBUTOR_ID DID,
				  DECODE(p_comp_lang_id,'10306122',NVL(C703_SALES_REP_NAME,C703_SALES_REP_NAME_EN),c703_sales_rep_name) dname,
				  t701.c701_distributor_name name ,
				  t101.c101_party_id partyid ,
				  t101.c101_first_nm
				  || ' '
				  || t101.c101_last_nm partyname
				FROM t703_sales_rep t703 ,
				  t101_party t101,
				  t701_distributor t701
				WHERE t703.c101_party_id       = t101.c101_party_id
				AND c703_void_fl              IS NULL
				AND t703.c701_distributor_id   = t701.c701_distributor_id
				AND t701.c701_void_fl         IS NULL
				AND t703.c1900_company_id  = p_company_id
				AND t703.c703_rep_category IN
				  (SELECT c906_rule_value
				  FROM t906_rules
				  WHERE c906_rule_id   = 'EMPLOYEE'
				  AND c906_rule_grp_id = 'EXCL_CATEGORY'
				  )
				AND c703_active_fl = p_acc_flg
				ORDER BY upper(name) ;
				
    
    END gm_fch_jpn_sales_list;
 END gm_pkg_cm_party_rpt;
 /