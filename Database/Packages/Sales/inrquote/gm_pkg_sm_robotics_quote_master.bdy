CREATE OR REPLACE PACKAGE BODY gm_pkg_sm_robotics_quote_master IS
   /**********************************************************************
 	* Description : Procedure used to save robotic quote
 	* Author      : saravanan M
	************************************************************************/

    PROCEDURE gm_sav_robot_quotation (
        p_robot_id        IN   t5500_robot_master.c5500_robot_master_id%TYPE,
        p_quote_id        IN   t5506_robot_quotation.c5506_robot_quotation_id%TYPE,
        p_customer_id     IN   t5506_robot_quotation.c101_parent_account_id%TYPE,
        p_customer_name   IN   t5506_robot_quotation.c5506_customer_name%TYPE,
        p_ad_id           IN   v700_territory_mapping_detail.ad_id%TYPE,
        p_vp_id           IN   v700_territory_mapping_detail.vp_id%TYPE,
        p_d_id            IN   v700_territory_mapping_detail.d_id%TYPE,
        p_rep_id          IN   v700_territory_mapping_detail.rep_id%TYPE,
        p_company_id      IN   t5506_robot_quotation.c1900_company_id%TYPE,
        p_user_id         IN   t5506_robot_quotation.c5506_last_updated_by%TYPE
    ) AS

        v_expiry_dt         t5506_robot_quotation.c5506_expiration_date%TYPE;
        v_exp_days          t5501_robot_company_mapping.c901_quote_expiration_days%TYPE;
        v_robot_name        t5500_robot_master.c5500_robot_name%TYPE;
        v_customer_id       t5506_robot_quotation.c101_parent_account_id%TYPE := p_customer_id;
        v_customer_name     t5506_robot_quotation.c5506_customer_name%TYPE := p_customer_name;
        v_customer_city     t5506_robot_quotation.c5506_customer_city%TYPE;
        v_customer_state    t5506_robot_quotation.c5506_customer_state%TYPE;
        v_customer_zip      t5506_robot_quotation.c5506_customer_zip%TYPE;
        v_customer_phn_no   t5506_robot_quotation.c5506_customer_phone_number%TYPE;
    BEGIN
    --To calculate Expiry date based on code lookup
        SELECT
            c901_quote_expiration_days
        INTO v_exp_days
        FROM
            t5501_robot_company_mapping
        WHERE
            c1900_company_id = p_company_id
            AND c5500_robot_master_id = p_robot_id
            AND c5501_void_fl IS NULL;

        BEGIN
            IF v_exp_days = '110830' THEN --US
                SELECT
                    last_day(trunc(add_months(current_date, 0), 'Q') + 85)
                INTO v_expiry_dt
                FROM
                    dual;

            ELSIF v_exp_days = '110831' THEN -- other countries
                SELECT
                    current_date + 90
                INTO v_expiry_dt
                FROM
                    dual;

            END IF;

        END;

        SELECT
            c5500_robot_name
        INTO v_robot_name
        FROM
            t5500_robot_master
        WHERE
            c5500_robot_master_id = p_robot_id;
      --To fetch other details of customer id

        IF p_customer_id != 0 THEN
            BEGIN
			    SELECT
			    t101.c101_party_id,
			    t101.c101_party_nm,
			    t704.c704_bill_city,
			    get_code_name(t704.c704_bill_state),
			    t704.c704_bill_zip_code,
			    t704.c704_phone
			INTO
			    v_customer_id,
			    v_customer_name,
			    v_customer_city,
			    v_customer_state,
			    v_customer_zip,
			    v_customer_phn_no
			FROM
			    t704_account   t704,
			    t101_party     t101
			WHERE
			    t704.c101_party_id = t101.c101_party_id
			    AND t704.c101_party_id = p_customer_id
			    AND t101.c1900_company_id = p_company_id
			    AND ROWNUM = 1;
                    
            EXCEPTION
                WHEN no_data_found THEN
                    v_customer_id := '';
                    v_customer_name := '';
                    v_customer_city := '';
                    v_customer_state := '';
                    v_customer_zip := '';
                    v_customer_zip := '';
            END;
        END IF;

        INSERT INTO t5506_robot_quotation (
            c5506_robot_quotation_id,
            c5506_robot_quotation_name,
            c5500_robot_master_id,
            c101_parent_account_id,
            c1900_company_id,
            c5506_customer_name,
            c5506_quote_start_date,
            c5506_expiration_date,
            c901_quote_status,
            c5506_capital_ad_id,
            c5506_capital_vp_id,
            c701_capital_distributor_id,
            c703_capital_sales_rep_id,
            c5506_customer_city,
            c5506_customer_state,
            c5506_customer_zip,
            c5506_customer_phone_number,
            c5506_last_updated_by,
            c5506_last_updated_date,
            c5506_created_by,
            c5506_created_date
        ) VALUES (
            p_quote_id,
            v_robot_name,
            p_robot_id,
            v_customer_id,
            p_company_id,
            v_customer_name,
            current_date,
            v_expiry_dt,
            110813,
            p_ad_id,
            p_vp_id,
            p_d_id,
            p_rep_id,
            v_customer_city,
            v_customer_state,
            v_customer_zip,
            v_customer_phn_no,
            p_user_id,
            current_date,
            p_user_id,
            current_date
        ); -- 110813(initiated)

    END gm_sav_robot_quotation;

	/**********************************************************************
 	* Description : Procedure used to save quote category details
 	* Author      : saravanan M
	************************************************************************/

    PROCEDURE gm_sav_robot_quotation_category_details (
        p_quote_id                    IN   t5506_robot_quotation.c5506_robot_quotation_id%TYPE,
        p_category_id                 IN   t5507_robot_quotation_category_details.c5502_robot_quotation_category_master_id%TYPE,
        p_set_id					  IN	t5507_robot_quotation_category_details.C207_SET_ID%TYPE,
        p_ref_id                      IN   t5507_robot_quotation_category_details.c5507_ref_id%TYPE,
        p_ref_name                    IN   t5507_robot_quotation_category_details.c5507_ref_name%TYPE,
        p_ref_type                    IN   t5507_robot_quotation_category_details.c901_ref_type%TYPE,
        p_suggestion                  IN   t5507_robot_quotation_category_details.c5507_suggestion%TYPE,
        p_d_qty                       IN   t5507_robot_quotation_category_details.c5507_default_qty%TYPE,
        p_approval_category_section   IN   t5507_robot_quotation_category_details.c901_approval_category_section%TYPE,
        p_disc_approval               IN   t5507_robot_quotation_category_details.c901_discount_approval%TYPE,
        p_acc_type					  IN   t5507_robot_quotation_category_details.C5507_EM_QUOTE_ACCOUNT_TYPE%TYPE,
        p_order_type				  IN   t5507_robot_quotation_category_details.C5507_EM_QUOTE_ORDER_TYPE%TYPE,
        p_plant_id					  IN   t5507_robot_quotation_category_details.C5040_PLANT_ID%TYPE,
        p_plant_nm					  IN   t5507_robot_quotation_category_details.C5040_PLANT_NAME%TYPE,
        p_ship_to_type				  IN   t5507_robot_quotation_category_details.C5507_EM_QUOTE_SHIP_TO_TYPE_NAME%TYPE,
        p_cat_status				  IN   t5507_robot_quotation_category_details.C5507_EM_QUOTE_ORDER_CATEGORY_STATUS%TYPE,
        p_ord_cat_seq				  IN   t5507_robot_quotation_category_details.C5507_ORDER_CATEGORY_SEQ%TYPE,
        p_user_id                     IN   t5506_robot_quotation.c5506_last_updated_by%TYPE
    ) AS
    BEGIN
        INSERT INTO t5507_robot_quotation_category_details (
            c5506_robot_quotation_id,
            c5502_robot_quotation_category_master_id,
            C207_SET_ID,
            c5507_ref_id,
            c5507_ref_name,
            c901_ref_type,
            c5507_suggestion,
            c5507_default_qty,
            c5507_qty,
            c901_approval_category_section,
            c901_discount_approval,
            c5507_list_price,
            c5507_corporate_list_price,
            C5507_EM_QUOTE_ACCOUNT_TYPE,
            C5507_EM_QUOTE_ORDER_TYPE,
            C5040_PLANT_ID,
            C5040_PLANT_NAME,
            C5507_EM_QUOTE_SHIP_TO_TYPE_NAME,
            C5507_EM_QUOTE_ORDER_CATEGORY_STATUS,
            C5507_ORDER_CATEGORY_SEQ,
            c5507_last_updated_by,
            c5507_last_updated_date
        ) VALUES (
            p_quote_id,
            p_category_id,
            p_set_id,
            p_ref_id,
            p_ref_name,
            p_ref_type,
            p_suggestion,
            p_d_qty,
            p_d_qty,
            p_approval_category_section,
            p_disc_approval,
            0,
            0,
            p_acc_type,
            p_order_type,
            p_plant_id,
            p_plant_nm,
            p_ship_to_type,
            p_cat_status,
            p_ord_cat_seq,
            p_user_id,
            current_date
        );

    END gm_sav_robot_quotation_category_details;

	/**********************************************************************
 	* Description : Procedure used to save robotic quote discount approval
 	* Author      : saravanan M
	************************************************************************/

    PROCEDURE gm_sav_robotics_quot_discount_approval (
        p_quote_id                    IN   t5510_robot_quotation_discount_approval_dtls.c5506_robot_quotation_id%TYPE,
        p_approval_category_section   IN   t5510_robot_quotation_discount_approval_dtls.c901_approval_category_section%TYPE,
        p_sec_name                    IN   t5510_robot_quotation_discount_approval_dtls.c5510_approval_category_section_name%TYPE,
        p_subtotal                    IN   t5510_robot_quotation_discount_approval_dtls.c5510_category_sub_total%TYPE,
        p_corp_subtotal               IN   t5510_robot_quotation_discount_approval_dtls.c5510_category_corporate_sub_total%TYPE,
        p_user_id                     IN   t5510_robot_quotation_discount_approval_dtls.c5510_last_updated_by%TYPE
    ) AS
        v_disc_approval t5510_robot_quotation_discount_approval_dtls.c901_discount_approval%TYPE;
    BEGIN
        BEGIN
            SELECT DISTINCT
                ( c901_discount_approval )
            INTO v_disc_approval
            FROM
                t5507_robot_quotation_category_details t5507
            WHERE
                t5507.c901_approval_category_section = p_approval_category_section
                AND t5507.c5507_void_fl IS NULL
                AND t5507.c5506_robot_quotation_id = p_quote_id;

        EXCEPTION
            WHEN no_data_found THEN
                v_disc_approval := '';
        END;

        INSERT INTO t5510_robot_quotation_discount_approval_dtls (
            c5506_robot_quotation_id,
            c901_approval_category_section,
            c5510_approval_category_section_name,
            c5510_category_sub_total,
            c5510_category_corporate_sub_total,
            c901_discount_approval,
            c5510_last_updated_by,
            c5510_last_updated_date
        ) VALUES (
            p_quote_id,
            p_approval_category_section,
            p_sec_name,
            p_subtotal,
            p_corp_subtotal,
            v_disc_approval,
            p_user_id,
            current_date
        );

    END gm_sav_robotics_quot_discount_approval;

	/**********************************************************************
 	* Description : Procedure used to save price details into set price
 	* Author      : saravanan M
	************************************************************************/

    PROCEDURE gm_sav_robotics_quot_set_master (
        p_quote_id   IN   t5508_robot_quotation_set_price.c5506_robot_quotation_id%TYPE,
        p_ref_id     IN   t5508_robot_quotation_set_price.c207_set_id%TYPE,
        p_user_id    IN   t5508_robot_quotation_set_price.c5508_last_updated_by%TYPE
    ) AS
        v_company_id t5506_robot_quotation.c1900_company_id%TYPE;
    BEGIN
        SELECT NVL(get_compid_frm_cntx(),GET_RULE_VALUE('DEFAULT_COMPANY','ONEPORTAL')) into v_company_id from dual;

        INSERT INTO t5508_robot_quotation_set_price (
            c5506_robot_quotation_id,
            c207_set_id,
            c901_txn_currency,
            c5508_set_price,
            c5508_corporate_set_price,
            c1900_company_id,
            c5508_last_updated_by,
            c5508_last_updated_date
        ) VALUES (
            p_quote_id,
            p_ref_id,
            0,
            0,
            0,
            v_company_id,
            p_user_id,
            current_date
        );

    END gm_sav_robotics_quot_set_master;

	/**********************************************************************
 	* Description : Procedure used to update set price details into set price
 	* Author      : saravanan M
	************************************************************************/

    PROCEDURE gm_upd_robotics_quot_set_price (
        p_quote_id     IN   t5508_robot_quotation_set_price.c5506_robot_quotation_id%TYPE,
        p_ref_id       IN   t5508_robot_quotation_set_price.c207_set_id%TYPE,
        p_set_price    IN   t5508_robot_quotation_set_price.c5508_set_price%TYPE,
        p_corp_price   IN   t5508_robot_quotation_set_price.c5508_corporate_set_price%TYPE,
        p_user_id      IN   t5508_robot_quotation_set_price.c5508_last_updated_by%TYPE
    ) AS
    BEGIN
        UPDATE t5508_robot_quotation_set_price
        SET
            c5508_set_price = p_set_price,
            c5508_corporate_set_price = p_corp_price,
            c5508_last_updated_by = p_user_id,
            c5508_last_updated_date = current_date
        WHERE
            c5506_robot_quotation_id = p_quote_id
            AND c207_set_id = p_ref_id;

    END gm_upd_robotics_quot_set_price;

	/**********************************************************************
 	* Description : Procedure used to update price details into category details
 	* Author      : saravanan M
	************************************************************************/

    PROCEDURE gm_upd_robotics_quote_list_price (
        p_quote_id          IN   t5506_robot_quotation.c5506_robot_quotation_id%TYPE,
        p_ref_id            IN   t5507_robot_quotation_category_details.c5507_ref_id%TYPE,
        p_list_price        IN   t5507_robot_quotation_category_details.c5507_list_price%TYPE,
        p_corporate_price   IN   t5507_robot_quotation_category_details.c5507_corporate_list_price%TYPE,
        p_category_id       IN   t5507_robot_quotation_category_details.c5502_robot_quotation_category_master_id%TYPE,
        p_user_id           IN   t5507_robot_quotation_category_details.c5507_last_updated_by%TYPE
    ) AS
    BEGIN
        UPDATE t5507_robot_quotation_category_details
        SET
            c5507_list_price = p_list_price,
            c5507_corporate_list_price = p_corporate_price,
            c5507_last_updated_by = p_user_id,
            c5507_last_updated_date = current_date
        WHERE
            c5506_robot_quotation_id = p_quote_id
            AND c5502_robot_quotation_category_master_id = p_category_id
            AND c5507_ref_id = p_ref_id;

    END gm_upd_robotics_quote_list_price;

	/**********************************************************************
 	* Description : Procedure used to update SET price details into set detail price table
 	* Author      : saravanan M
	************************************************************************/

    PROCEDURE gm_sav_robotics_quote_set_details (
        p_quote_id       IN   t5506_robot_quotation.c5506_robot_quotation_id%TYPE,
        p_set_id         IN   t5509_robot_quotation_set_detail_price.c207_set_id%TYPE,
        p_part_num       IN   t5509_robot_quotation_set_detail_price.c205_part_number_id%TYPE,
        p_txn_currency   IN   t5509_robot_quotation_set_detail_price.c901_txn_currency%TYPE,
        p_list_price     IN   t5509_robot_quotation_set_detail_price.c5509_list_price%TYPE,
        p_corp_price     IN   t5509_robot_quotation_set_detail_price.c5509_corporate_price%TYPE,
        p_part_qty		 IN	  t208_set_details.c208_set_qty%TYPE,
        p_company_id     IN   t5509_robot_quotation_set_detail_price.c1900_company_id%TYPE,
        p_user_id        IN   t5509_robot_quotation_set_detail_price.c5509_last_updated_by%TYPE
    ) AS
    BEGIN
        INSERT INTO t5509_robot_quotation_set_detail_price (
            c5506_robot_quotation_id,
            c207_set_id,
            c205_part_number_id,
            c901_txn_currency,
            c5509_list_price,
            c5509_corporate_price,
            c5509_default_qty,
            c5509_req_qty,
            c1900_company_id,
            c5509_last_updated_by,
            c5509_last_updated_date
        ) VALUES (
            p_quote_id,
            p_set_id,
            p_part_num,
            p_txn_currency,
            p_list_price,
            p_corp_price,
            nvl(p_part_qty,0),
            nvl(p_part_qty,0),
            p_company_id,
            p_user_id,
            current_date
        );

    END gm_sav_robotics_quote_set_details;

	/**********************************************************************
 	* Description : Procedure used to update QUOTE category line item detail
 	* when updating qty,list price
 	* Author      : saravanan M
	************************************************************************/

    PROCEDURE gm_upd_category_line_item_detail (
        p_qty               IN   t5507_robot_quotation_category_details.c5507_qty%TYPE,
        p_list_price        IN   t2052_part_price_mapping.c2052_list_price%TYPE,
        p_corporate_price   IN   t5507_robot_quotation_category_details.c5507_corporate_list_price%TYPE,
        p_ref_id            IN   t5507_robot_quotation_category_details.c5507_ref_id%TYPE,
        p_quote_id          IN   t5506_robot_quotation.c5506_robot_quotation_id%TYPE,
        p_user_id           IN   t5506_robot_quotation.c5506_last_updated_by%TYPE
    ) AS
    BEGIN
        UPDATE t5507_robot_quotation_category_details
        SET
            c5507_qty = nvl(p_qty,0),
            c5507_list_price = nvl(p_list_price,0),
            c5507_corporate_list_price = nvl(p_corporate_price,0),
            c5507_last_updated_by = p_user_id,
            c5507_last_updated_date = current_date
        WHERE
            c5507_ref_id = p_ref_id
            AND c5506_robot_quotation_id = p_quote_id
            AND c5507_void_fl IS NULL;

    END gm_upd_category_line_item_detail;

	/**********************************************************************
 	* Description : Procedure used to update QUOTE Header start and Expiry Date
 	* Author      : saravanan M
	************************************************************************/

    PROCEDURE gm_upd_robotics_quote_date (
        p_quote_id      IN   t5506_robot_quotation.c5506_robot_quotation_id%TYPE,
        p_start_date    IN   VARCHAR2,
        p_expiry_date   IN   VARCHAR2,
        p_user_id       IN   t5506_robot_quotation.c5506_last_updated_by%TYPE
    ) AS
        v_date_fmt VARCHAR2(200);
    BEGIN
    
       SELECT get_compdtfmt_frm_cntx ()
       INTO v_date_fmt
       FROM dual;
	
        UPDATE t5506_robot_quotation
        SET
            c5506_quote_start_date = to_date(p_start_date, v_date_fmt),
            c5506_expiration_date = to_date(p_expiry_date, v_date_fmt),
            c5506_last_updated_by = p_user_id,
            c5506_last_updated_date = current_date
        WHERE
            c5506_robot_quotation_id = p_quote_id
            AND c5506_void_fl IS NULL;

    END gm_upd_robotics_quote_date;

	/**********************************************************************
 	* Description : Procedure used to update QUOTE Customer Details
 	* Author      : saravanan M
	************************************************************************/

    PROCEDURE gm_upd_robotics_customer_details (
        p_quote_id          IN   t5506_robot_quotation.c5506_robot_quotation_id%TYPE,
        p_capital_acc_id    IN   t5506_robot_quotation.c704_capital_account_id%TYPE,
        p_metal_acc_id      IN   t5506_robot_quotation.c704_metal_account_id%TYPE,
        p_customer_id       IN   t5506_robot_quotation.c101_parent_account_id%TYPE,
        p_customer_name     IN   t5506_robot_quotation.c5506_customer_name%TYPE,
        p_customer_city     IN   t5506_robot_quotation.c5506_customer_city%TYPE,
        p_customer_state    IN   t5506_robot_quotation.c5506_customer_state%TYPE,
        p_customer_zip      IN   t5506_robot_quotation.c5506_customer_zip%TYPE,
        p_customer_phn_no   IN   t5506_robot_quotation.c5506_customer_phone_number%TYPE,
        p_user_id           IN   t5506_robot_quotation.c5506_last_updated_by%TYPE,
        p_cust_error		OUT	number,
        p_capital_error		OUT	number,
        p_metal_error		OUT	number
        
    ) AS

        v_customer_id       	t5506_robot_quotation.c101_parent_account_id%TYPE := p_customer_id;
        v_customer_name     	t5506_robot_quotation.c5506_customer_name%TYPE := p_customer_name;
        v_customer_city     	t5506_robot_quotation.c5506_customer_city%TYPE := p_customer_city;
        v_customer_state    	t5506_robot_quotation.c5506_customer_state%TYPE := p_customer_state;
        v_customer_zip      	t5506_robot_quotation.c5506_customer_zip%TYPE := p_customer_zip;
        v_customer_phn_no   	t5506_robot_quotation.c5506_customer_phone_number%TYPE := p_customer_phn_no;
        v_company_id        	t5506_robot_quotation.c1900_company_id%TYPE;
        v_capital_acc_id    	t5506_robot_quotation.c704_capital_account_id%TYPE :=p_capital_acc_id;
        v_metal_acc_id			t5506_robot_quotation.c704_metal_account_id%TYPE := p_metal_acc_id;
        v_account_avail			number;
        v_capital_rep_id		t5506_robot_quotation.c703_capital_sales_rep_id%TYPE;
	    v_capital_ad_id			t5506_robot_quotation.c5506_capital_ad_id%TYPE;
	    v_capital_vp_id			t5506_robot_quotation.c5506_capital_vp_id%TYPE;
	    v_capital_dist_id		t5506_robot_quotation.c701_capital_distributor_id%TYPE;
        v_metal_rep_id			t5506_robot_quotation.c703_metal_sales_rep_id%TYPE;
	    v_metal_ad_id			t5506_robot_quotation.c5506_metal_ad_id%TYPE;
	    v_metal_vp_id			t5506_robot_quotation.c5506_metal_vp_id%TYPE;
	    v_metal_dist_id			t5506_robot_quotation.c5506_metal_distributor_id%TYPE;
    BEGIN
		p_capital_error := 0;
		p_metal_error := 0;
        SELECT NVL(get_compid_frm_cntx(),GET_RULE_VALUE('DEFAULT_COMPANY','ONEPORTAL')) into v_company_id from dual;
		
        IF p_customer_id != 0 THEN
            BEGIN
			    SELECT
			    t101.c101_party_id,
			    t101.c101_party_nm,
			    t704.c704_bill_city,
			    get_code_name(t704.c704_bill_state),
			    t704.c704_bill_zip_code,
			    t704.c704_phone
			INTO
			    v_customer_id,
			    v_customer_name,
			    v_customer_city,
			    v_customer_state,
			    v_customer_zip,
			    v_customer_phn_no
			FROM
			    t704_account   t704,
			    t101_party     t101
			WHERE
			    t704.c101_party_id = t101.c101_party_id
			    AND t704.c101_party_id = p_customer_id
			    AND t101.c1900_company_id = v_company_id
			    AND ROWNUM = 1;
			p_cust_error := 0;
			EXCEPTION
                WHEN no_data_found THEN
                    v_customer_id := '';
                    v_customer_name := '';
                    v_customer_city := '';
                    v_customer_state := '';
                    v_customer_zip := '';
                    v_customer_zip := '';
                    p_cust_error := 1;
            END;

        END IF;
	        IF p_capital_acc_id != 0 THEN
            BEGIN
            
			SELECT
			    C704_ACCOUNT_ID
			INTO
			    v_capital_acc_id
			FROM
			    t704_account
			WHERE
			    c704_account_id = p_capital_acc_id
			    AND c901_company_id = 26230708 --I-N-Robotics
			    AND c1900_company_id = v_company_id;
			EXCEPTION
                WHEN no_data_found THEN
                    v_capital_acc_id := '';
                    p_capital_error := 1;
            END;
            
            --fetch capital account salesRep,AD,VP,Distributor details
            gm_pkg_sm_robotics_quote_master.gm_fch_robotics_capital_account_details(p_capital_acc_id,v_capital_rep_id,v_capital_ad_id,v_capital_vp_id,v_capital_dist_id);

        END IF;
        
        IF p_metal_acc_id != 0 THEN
            BEGIN
			    SELECT
				    C704_ACCOUNT_ID
				    INTO 
				    v_metal_acc_id
				FROM
				    t704_account
				WHERE
				    c704_account_id = p_metal_acc_id
				    AND c901_company_id = 100800 --SPINE
				    AND c1900_company_id = v_company_id;
			EXCEPTION
                WHEN no_data_found THEN
                    v_metal_acc_id := '';
                    p_metal_error := 1;
            END;
			
			--fetch metal account salesRep,AD,VP,Distributor details
            gm_pkg_sm_robotics_quote_master.gm_fch_robotics_metal_account_details(p_metal_acc_id,v_metal_rep_id,v_metal_ad_id,v_metal_vp_id,v_metal_dist_id);
            
        END IF;
        
    
        
        IF(p_capital_error = 0) AND (p_metal_error = 0) THEN
	        UPDATE t5506_robot_quotation
	        SET
	            c101_parent_account_id = v_customer_id,
	            c5506_customer_name = v_customer_name,
	            c5506_customer_city = v_customer_city,
	            c5506_customer_state = v_customer_state,
	            c5506_customer_zip = v_customer_zip,
	            c5506_customer_phone_number = v_customer_phn_no,
	            c704_capital_account_id = v_capital_acc_id,
	            c704_metal_account_id = v_metal_acc_id,
	            c703_capital_sales_rep_id = nvl(v_capital_rep_id,c703_capital_sales_rep_id),
		        c5506_capital_ad_id  = nvl(v_capital_ad_id,c5506_capital_ad_id),
		        c5506_capital_vp_id  = nvl(v_capital_vp_id,c5506_capital_vp_id),
		        c701_capital_distributor_id = nvl(v_capital_dist_id,c701_capital_distributor_id),
		        c703_metal_sales_rep_id = nvl(v_metal_rep_id,c703_metal_sales_rep_id),
		        c5506_metal_ad_id = nvl(v_metal_ad_id,c5506_metal_ad_id),
		        c5506_metal_vp_id = nvl(v_metal_vp_id,c5506_metal_vp_id),
		        c5506_metal_distributor_id = nvl(v_metal_dist_id,c5506_metal_distributor_id),
	            c5506_last_updated_by = p_user_id,
	            c5506_last_updated_date = current_date
	        WHERE
	            c5506_robot_quotation_id = p_quote_id
	            AND c5506_void_fl IS NULL;
            END IF;
    END gm_upd_robotics_customer_details;
    
    
    /**********************************************************************
 	* Description : Procedure used to fetch capital account details
 	* Author      : tmuthusamy
	************************************************************************/

    PROCEDURE gm_fch_robotics_capital_account_details (
        p_capital_acc_id    IN   t5506_robot_quotation.c704_capital_account_id%TYPE,
        p_capital_rep_id	OUT  t5506_robot_quotation.c703_capital_sales_rep_id%TYPE,
	    p_capital_ad_id		OUT  t5506_robot_quotation.c5506_capital_ad_id%TYPE,
	    p_capital_vp_id		OUT  t5506_robot_quotation.c5506_capital_vp_id%TYPE,
	    p_capital_dist_id	OUT  t5506_robot_quotation.c701_capital_distributor_id%TYPE
        
    ) AS
    BEGIN
    	BEGIN
			SELECT
				REP_ID,
				AD_ID,
			    VP_ID,
			    D_ID
			INTO
			    p_capital_rep_id,
			    p_capital_ad_id,
			    p_capital_vp_id,
			    p_capital_dist_id
			FROM
			    v700_territory_mapping_detail
			WHERE
			    ac_id = p_capital_acc_id;
            EXCEPTION
                WHEN no_data_found THEN
                    p_capital_rep_id  := '';
				    p_capital_ad_id   := '';
				    p_capital_vp_id   := '';
				    p_capital_dist_id := '';
		    END;
    END gm_fch_robotics_capital_account_details;
    
    
    /**********************************************************************
 	* Description : Procedure used to fetch metal account details
 	* Author      : tmuthusamy
	************************************************************************/

    PROCEDURE gm_fch_robotics_metal_account_details (
        p_metal_acc_id      IN   t5506_robot_quotation.c704_metal_account_id%TYPE,
        p_metal_rep_id		OUT	 t5506_robot_quotation.c703_metal_sales_rep_id%TYPE,
	    p_metal_ad_id		OUT	 t5506_robot_quotation.c5506_metal_ad_id%TYPE,
	    p_metal_vp_id		OUT	 t5506_robot_quotation.c5506_metal_vp_id%TYPE,
	    p_metal_dist_id		OUT	 t5506_robot_quotation.c5506_metal_distributor_id%TYPE
        
    ) AS
    BEGIN
    	BEGIN
			SELECT
				REP_ID,
				AD_ID,
			    VP_ID,
			    D_ID
			INTO
			    p_metal_rep_id,
			    p_metal_ad_id,
			    p_metal_vp_id,
			    p_metal_dist_id
			FROM
			    v700_territory_mapping_detail
			WHERE
			    ac_id = p_metal_acc_id;
            EXCEPTION
                WHEN no_data_found THEN
                    p_metal_rep_id  := '';
				    p_metal_ad_id   := '';
				    p_metal_vp_id   := '';
				    p_metal_dist_id := '';
		    END;
    END gm_fch_robotics_metal_account_details;

	   
END gm_pkg_sm_robotics_quote_master;
/