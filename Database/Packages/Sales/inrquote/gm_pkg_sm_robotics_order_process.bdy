CREATE OR REPLACE PACKAGE BODY gm_pkg_sm_robotics_order_process IS
	/**********************************************************************
 	* Description : Procedure used to fetch category details based on Account type
 	* Author      : saravanan M
	************************************************************************/
	PROCEDURE gm_fch_quote_category_based_on_acc_type(
		p_quote_id			IN			t5507_robot_quotation_category_details.c5506_robot_quotation_id%TYPE,
		p_account_type		IN			t5507_robot_quotation_category_details.c5507_em_quote_account_type%TYPE,
		p_out_category_dtl	OUT			CLOB
	)
	AS
	BEGIN

			SELECT 
			JSON_ARRAYAGG(JSON_OBJECT('strCatDtlsId' VALUE T5507.C5507_ROBOT_QUOTATION_CATEGORY_DETAILS_ID ,
			'strCatGrpId' VALUE T5502.C901_CATEGORY_GROUP_ID ,
			'strCatGrpName' VALUE T5502.C5502_CATEGORY_GROUP_NAME ,
			'strRefTypeName' VALUE GET_CODE_NAME(C901_REF_TYPE),
			'strRefTypeId' VALUE C901_REF_TYPE ,
			'strRefId' VALUE T5507.C5507_REF_ID ,
			'strRefName' VALUE T5507.C5507_REF_NAME ,
			'strReqQty' VALUE T5507.C5507_QTY ,
			'strListPrice' VALUE nvl((T5507.c5507_list_price-ROUND(((T5507.c5507_discount_per*T5507.c5507_list_price)/100))),T5507.c5507_list_price), 
			'strExtListPrice' VALUE (T5507.C5507_QTY * nvl((T5507.c5507_list_price-ROUND(((T5507.c5507_discount_per*T5507.c5507_list_price)/100))),T5507.c5507_list_price)),
			'strOrderType' VALUE T5507.C5507_EM_QUOTE_ORDER_TYPE ,
			'strPlantId' VALUE C5040_PLANT_ID ,
			'strPlantName' VALUE C5040_PLANT_NAME ,
			'strFGQty' VALUE Decode(C901_REF_TYPE,'111120',NVL(get_qty_in_stock_by_plant (null, T5507.C5507_REF_ID , C5040_PLANT_ID ),0)
			,'111121',''),
			'strShipTo' VALUE T5507.C5507_EM_QUOTE_SHIP_TO_TYPE_NAME ,
			'strOrderStatus' VALUE T5507.C5507_EM_QUOTE_ORDER_CATEGORY_STATUS ,
			'strOrderCatSeq' VALUE T5507.C5507_ORDER_CATEGORY_SEQ )
			Order By GET_CODE_NAME(C901_REF_TYPE),C5040_PLANT_NAME,T5507.C5507_EM_QUOTE_ORDER_CATEGORY_STATUS RETURNING clob)
			INTO p_out_category_dtl
			FROM T5502_ROBOT_QUOTATION_CATEGORY_MASTER T5502,
			T5507_ROBOT_QUOTATION_CATEGORY_DETAILS T5507
			WHERE T5502.C5502_ROBOT_QUOTATION_CATEGORY_MASTER_ID = T5507.C5502_ROBOT_QUOTATION_CATEGORY_MASTER_ID (+)
			AND T5507.C5506_ROBOT_QUOTATION_ID = p_quote_id --'GMQT-072221-1' 
			AND C5507_EM_QUOTE_ACCOUNT_TYPE = p_account_type -- 'Metal' -- 'Capital' or 'Metal'
			AND t5507.c5502_robot_quotation_category_master_id not in 
		   	    (SELECT c906_rule_value 
		   	      FROM t906_rules 
		   	     WHERE c906_rule_id = 'SKIP_SERVICE_PART' 
		   	       AND c906_rule_grp_id='QUOTE_PARTS' 
		   	       AND c906_void_fl is null)
			AND T5507.C5507_QTY > 0
			AND T5502.C5502_VOID_FL IS NULL
			AND T5507.C5507_VOID_FL IS NULL
			;

	END gm_fch_quote_category_based_on_acc_type;

	/*****************************************************************
 	* Description : Procedure used to update quote order type
 	* Author      : Agilan Singaravel
	******************************************************************/
	PROCEDURE gm_update_order_type(
		p_order_type		IN		t5507_robot_quotation_category_details.C5507_EM_QUOTE_ORDER_TYPE%type,
		p_quote_id			IN		t5513_robot_set_allocation.c5506_robot_quotation_id%type,
		p_cat_dtls_id		IN		CLOB,
		p_user_id			IN		t5513_robot_set_allocation.c5513_last_updated_by%type
	)
	AS
	BEGIN
	
	my_context.set_my_cloblist(p_cat_dtls_id||',');
	
	UPDATE T5507_ROBOT_QUOTATION_CATEGORY_DETAILS
	   SET c5507_em_quote_order_type = p_order_type
		 , c5507_last_updated_by = p_user_id
		 , c5507_last_updated_date = CURRENT_DATE
	 WHERE c5507_robot_quotation_category_details_id IN (SELECT token FROM v_clob_list WHERE token IS NOT NULL )
	   AND c5506_robot_quotation_id = p_quote_id
	   AND c5507_void_fl is null;
		   
	END gm_update_order_type;
	
	/*****************************************************************
 	* Description : Procedure used to update quote order plant
 	* Author      : Agilan Singaravel
	******************************************************************/
	PROCEDURE gm_upd_order_plant_info(
		p_quote_id			IN		t5513_robot_set_allocation.c5506_robot_quotation_id%type,
		p_cat_dtls_id		IN		CLOB,
		p_plant_id			IN		t5507_robot_quotation_category_details.C5507_EM_QUOTE_ORDER_TYPE%type,		
		p_user_id			IN		t5513_robot_set_allocation.c5513_last_updated_by%type,
		p_out			    OUT		CLOB
	)
	AS
	v_qty				NUMBER;
	v_cat_dtl_id		NUMBER;
	v_out				CLOB;
		CURSOR v_fch_part
		IS
			SELECT c5507_robot_quotation_category_details_id cat_dtl_id,c5507_ref_id partnum
		  	  FROM T5507_ROBOT_QUOTATION_CATEGORY_DETAILS
		 	 WHERE c5507_robot_quotation_category_details_id in (SELECT token FROM v_clob_list WHERE token IS NOT NULL)
		 	   AND c5506_robot_quotation_id = p_quote_id
		 	   AND c5507_void_fl is null;
		 	 
	BEGIN
		my_context.set_my_cloblist(p_cat_dtls_id||',');
		
		UPDATE T5507_ROBOT_QUOTATION_CATEGORY_DETAILS
		   SET c5040_plant_id = p_plant_id,
			   c5040_plant_name = get_plant_name(p_plant_id),
			   c5507_last_updated_by = p_user_id,
			   c5507_last_updated_date = CURRENT_DATE
		 WHERE c5507_robot_quotation_category_details_id IN (SELECT token FROM v_clob_list WHERE token IS NOT NULL )
		   AND c5506_robot_quotation_id = p_quote_id
		   AND c5507_void_fl is null;
		   
		FOR fch_part IN v_fch_part 
		LOOP
			SELECT fch_part.cat_dtl_id,nvl(get_qty_in_stock_by_plant (null, fch_part.partnum, p_plant_id),0)
		  	  INTO v_cat_dtl_id,v_qty 
		  	  FROM DUAL;
		  	  
		  	v_out := v_cat_dtl_id || '-' || v_qty;
		  	p_out := p_out || ',' || v_out;
		  	  
		END LOOP;

	END gm_upd_order_plant_info;
	
	/*****************************************************************
     * Description : Procedure used to get the hospital Address
     * Author      : Saravanan M
    ******************************************************************/
    PROCEDURE gm_fetch_hospital_address(
      p_shipto         IN   t907_shipping_info.c901_ship_to%TYPE,
      p_shiptoid       IN   t907_shipping_info.c907_ship_to_id%TYPE,
      p_address        OUT     VARCHAR2
    )
    AS
    BEGIN
   
    p_address := gm_pkg_cm_shipping_info.get_address(p_shipto, p_shiptoid);
          
    END gm_fetch_hospital_address;
    
    /*****************************************************************
     * Description : Procedure used to update ship to type name in T5507 table
     * Author      : tmuthusamy
    ******************************************************************/
    PROCEDURE gm_sav_category_shipto_name(
	  p_refid 	   	   	   IN		t907_shipping_info.c907_ref_id%TYPE,
	  p_quote_id 	   	   IN		T5507_ROBOT_QUOTATION_CATEGORY_DETAILS.C5506_ROBOT_QUOTATION_ID%TYPE,
	  p_seq_num 	   	   IN		T5507_ROBOT_QUOTATION_CATEGORY_DETAILS.C5507_ORDER_CATEGORY_SEQ%TYPE,
      p_shipto         	   IN   	t907_shipping_info.c901_ship_to%TYPE,
	  p_userid	   	       IN		t907_shipping_info.c907_last_updated_by%TYPE
    )
    AS
    v_shipto_nm  T5507_ROBOT_QUOTATION_CATEGORY_DETAILS.C5507_EM_QUOTE_SHIP_TO_TYPE_NAME%TYPE;
    BEGIN
    	BEGIN
		    SELECT get_code_name(p_shipto) 
		    INTO v_shipto_nm
		    FROM t907_shipping_info
		    WHERE C907_REF_ID = p_refid;
		    
	    EXCEPTION WHEN NO_DATA_FOUND
	    THEN
		    v_shipto_nm:='';
	    END;
	    
	    
	    UPDATE T5507_ROBOT_QUOTATION_CATEGORY_DETAILS 
		SET C5507_EM_QUOTE_SHIP_TO_TYPE_NAME = v_shipto_nm,
		C5507_LAST_UPDATED_BY = p_userid,
	    C5507_LAST_UPDATED_DATE = CURRENT_DATE
		WHERE C5507_ORDER_CATEGORY_SEQ = p_seq_num
		AND C5506_ROBOT_QUOTATION_ID = p_quote_id
		AND C5507_void_fl IS NULL;
		
    END gm_sav_category_shipto_name;
   	/*****************************************************************
	   * Description : Procedure used to get quote category name
	   * Author      : smanimaran
	******************************************************************/
    FUNCTION get_quote_category_ref_type (
		p_quote_id		IN	T5507_ROBOT_QUOTATION_CATEGORY_DETAILS.C5506_ROBOT_QUOTATION_ID%TYPE,
		p_order_id		IN	T5507_ROBOT_QUOTATION_CATEGORY_DETAILS.C501_ORDER_ID%TYPE
   )
      RETURN VARCHAR2
   IS
      v_cat_type   VARCHAR2 (100);

   BEGIN
		BEGIN
		SELECT get_code_name (c901_ref_type)
		INTO v_cat_type
		FROM t5507_robot_quotation_category_details
		WHERE c501_order_id = p_order_id
		      AND c5506_robot_quotation_id = p_quote_id
		      AND c5507_void_fl IS NULL
		GROUP BY c901_ref_type;
		EXCEPTION
			WHEN no_data_found THEN 
			v_cat_type := '';
		END;
		RETURN v_cat_type;
   END get_quote_category_ref_type;
   
	    /*****************************************************************
	     * Description : Procedure used to get quote category name
	     * Author      : smanimaran
	    ******************************************************************/
       FUNCTION get_quote_catagory_name (
		p_quote_id		IN	T5507_ROBOT_QUOTATION_CATEGORY_DETAILS.C5506_ROBOT_QUOTATION_ID%TYPE,
		p_order_id		IN	T5507_ROBOT_QUOTATION_CATEGORY_DETAILS.C501_ORDER_ID%TYPE
   )
      RETURN VARCHAR2
   IS
      v_cat_grp_name   VARCHAR2 (100);

   BEGIN
		BEGIN
		SELECT t5502.c5502_category_group_name
		INTO v_cat_grp_name
		FROM t5502_robot_quotation_category_master    t5502,
		     t5507_robot_quotation_category_details   t5507
		WHERE t5502.c5502_robot_quotation_category_master_id = t5507.c5502_robot_quotation_category_master_id
		      AND c501_order_id = p_order_id
		      AND c5506_robot_quotation_id = p_quote_id
		      AND t5507.c5507_void_fl IS NULL
		      AND t5502.c5502_void_fl IS NULL
		GROUP BY t5502.c5502_category_group_name;
		EXCEPTION
			WHEN no_data_found THEN 
			v_cat_grp_name := '';
		END;
		RETURN v_cat_grp_name;
   END get_quote_catagory_name;
	
    /*****************************************************************
     * Description : Procedure used to fetch the Order Summary
     * Author      : smanimaran
    ******************************************************************/
    PROCEDURE gm_fch_order_summary(
	  p_quote_id			IN		T5507_ROBOT_QUOTATION_CATEGORY_DETAILS.C5506_ROBOT_QUOTATION_ID%TYPE,
	  p_capital_acc_id		IN		T5506_ROBOT_QUOTATION.C704_CAPITAL_ACCOUNT_ID%TYPE,
	  p_out_order_summary	OUT		CLOB,
	  p_out_bill_address	OUT		VARCHAR2
    )
    AS
    BEGIN
    
    SELECT 
    GET_BILL_ADD(p_capital_acc_id) 
    INTO p_out_bill_address 
    from dual;
    
	SELECT
	JSON_ARRAYAGG(JSON_OBJECT(
	'strCategeoryName' VALUE nvl2(T501.C501_PARENT_ORDER_ID,gm_pkg_sm_robotics_order_process.get_quote_catagory_name(T501.C5506_ROBOT_QUOTATION_ID,T501.C501_PARENT_ORDER_ID),gm_pkg_sm_robotics_order_process.get_quote_catagory_name(T501.C5506_ROBOT_QUOTATION_ID,T501.C501_ORDER_ID)),
	'strCategoryRefType' VALUE nvl2(T501.C501_PARENT_ORDER_ID,gm_pkg_sm_robotics_order_process.get_quote_category_ref_type(T501.C5506_ROBOT_QUOTATION_ID,T501.C501_PARENT_ORDER_ID),gm_pkg_sm_robotics_order_process.get_quote_category_ref_type(T501.C5506_ROBOT_QUOTATION_ID,T501.C501_ORDER_ID)),
	'strOrderId' VALUE T501.C501_ORDER_ID,
	'strOrderType' VALUE GET_CODE_NAME(T501.C901_ORDER_TYPE),
	'strTotalCost' VALUE T501.C501_TOTAL_COST,
	'strOrderStatus' VALUE gm_pkg_op_do_process_report.get_order_status_desc (t501.C501_ORDER_ID) ,
	'strShipTOId' VALUE T907.C901_SHIP_TO,
	'strShipTOName' VALUE GET_CODE_NAME(T907.C901_SHIP_TO),
	'strDeliveryCarrier' VALUE GET_CODE_NAME(c901_delivery_carrier),
	'strDeliveryMode' VALUE GET_CODE_NAME(c901_delivery_mode),
	'strShipAddress' VALUE gm_pkg_cm_shipping_info.get_ship_add(T501.C501_ORDER_ID, '50180') ,
	'strParentOrderId' VALUE T501.C501_PARENT_ORDER_ID,
	'strCustomerPo' VALUE  T501.c501_customer_po,
	'strCustomerPoDate' VALUE T501.c501_customer_po_date)RETURNING CLOB)
	INTO p_out_order_summary
	FROM T501_ORDER T501,
	T907_SHIPPING_INFO T907
	WHERE T501.C5506_ROBOT_QUOTATION_ID = p_quote_id
	AND T501.C501_ORDER_ID = T907.C907_REF_ID(+)
	AND T907.C907_VOID_FL IS NULL
	AND T501.C501_VOID_FL IS NULL;
	
	END gm_fch_order_summary;
END gm_pkg_sm_robotics_order_process;
/