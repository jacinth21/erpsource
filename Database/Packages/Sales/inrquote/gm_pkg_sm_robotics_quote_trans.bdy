CREATE OR REPLACE PACKAGE BODY gm_pkg_sm_robotics_quote_trans IS

   /**********************************************************************
 	* Description : Procedure used to create new robotic quote
 	* Author      : saravanan M
	************************************************************************/

    PROCEDURE gm_create_robotics_quote (
        p_robot_id        IN    t5500_robot_master.c5500_robot_master_id%TYPE,
        p_customer_id     IN    t5506_robot_quotation.c101_parent_account_id%TYPE,
        p_customer_name   IN    t5506_robot_quotation.c5506_customer_name%TYPE,
        p_company_id      IN    t5506_robot_quotation.c1900_company_id%TYPE,
        p_user_id         IN    t5506_robot_quotation.c5506_last_updated_by%TYPE,
        p_rsm_id          IN    t5506_robot_quotation.c5506_capital_ad_id%TYPE,
        p_quote_id        OUT   t5506_robot_quotation.c5506_robot_quotation_id%TYPE
    ) AS

        v_quote          t5506_robot_quotation.c5506_robot_quotation_id%TYPE;
        v_ad_id          v700_territory_mapping_detail.ad_id%TYPE := '';
        v_vp_id          v700_territory_mapping_detail.vp_id%TYPE := '';
        v_rep_id         v700_territory_mapping_detail.rep_id%TYPE := '';
        v_d_id           v700_territory_mapping_detail.d_id%TYPE := '';
        v_access_level   t101_user.c101_access_level_id%TYPE;
        v_user_id		 t5506_robot_quotation.c5506_last_updated_by%TYPE;
    BEGIN
    	--To generate Robot_quote_id
        gm_generate_quote_id(v_quote);
        --To get AD,VP,REP,Distributor based on Access level id
        v_user_id := p_user_id;
        IF p_rsm_id IS NOT NULL THEN
        	v_user_id := p_rsm_id;
        END IF;

        SELECT
            c101_access_level_id
        INTO v_access_level
        FROM
            t101_user
        WHERE
            c101_user_id = v_user_id;

        CASE ( v_access_level )
            WHEN 1 THEN
                BEGIN
                    SELECT DISTINCT
                        ad_id,
                        vp_id,
                        d_id,
                        rep_id
                    INTO
                        v_ad_id,
                        v_vp_id,
                        v_d_id,
                        v_rep_id
                    FROM
                        v700_territory_mapping_detail v700
                    WHERE
                        v700.rep_id = v_user_id;

                EXCEPTION
                    WHEN no_data_found THEN
                        v_ad_id := '';
                        v_vp_id := '';
                        v_d_id := '';
                        v_rep_id := '';
                END;
            WHEN 2 THEN
                BEGIN
                    SELECT DISTINCT
                    	rep_id,
                        ad_id,
                        vp_id,
                        d_id
                    INTO
                    	v_rep_id,
                        v_ad_id,
                        v_vp_id,
                        v_d_id
                    FROM
                        t101_user                       t101s,
                        v700_territory_mapping_detail   v700
                    WHERE
                        t101s.c101_user_id = v_user_id
                        AND t101s.c701_distributor_id = v700.d_id;

                EXCEPTION
                    WHEN no_data_found THEN
                    	v_rep_id := '';
                        v_ad_id := '';
                        v_vp_id := '';
                        v_d_id := '';
                END;
            WHEN 3 THEN
                BEGIN
                    SELECT DISTINCT
                        ad_id,
                        vp_id
                    INTO
                        v_ad_id,
                        v_vp_id
                    FROM
                        v700_territory_mapping_detail v700
                    WHERE
                        v700.ad_id = v_user_id;

                EXCEPTION
                    WHEN no_data_found THEN
                        v_ad_id := '';
                        v_vp_id := '';
                END;
            WHEN 4 THEN
                BEGIN
                    SELECT DISTINCT
                        vp_id
                    INTO v_vp_id
                    FROM
                        t708_region_asd_mapping         t708s,
                        v700_territory_mapping_detail   v700
                    WHERE
                        t708s.c101_user_id = v_user_id
                        AND t708s.c708_delete_fl IS NULL
                        AND t708s.c901_user_role_type = 8001
                        AND t708s.c901_region_id = v700.region_id;

                EXCEPTION
                    WHEN no_data_found THEN
                        v_ad_id := '';
                        v_vp_id := '';
                END;
            ELSE
                v_ad_id := '';
                v_vp_id := '';
                v_d_id := '';
                v_rep_id := '';
        END CASE;

    	--sav customer details in t5506_robot_quoatation table

        gm_pkg_sm_robotics_quote_master.gm_sav_robot_quotation(p_robot_id, v_quote, p_customer_id, p_customer_name, v_ad_id,
                                                               v_vp_id, v_d_id, v_rep_id, p_company_id, p_user_id);
                                                            
		--To save Category wise details from the table -t5502_robot_quotation_category_master 
		--& t5503_robot_quotation_category_template

        gm_sav_robotics_quote_category_dtls(p_robot_id, v_quote, p_company_id, p_user_id);
        
        --To save discount details in discount approval dtls table 
        --from t5507_robot_quotation_category_details by category_section  wise
        gm_sav_robotics_quote_discount_dtls(p_robot_id, v_quote, p_company_id, p_user_id);
        
        --To update overall quote and corporate total
        gm_pkg_sm_robotics_quote_trans.gm_upd_robotic_quote_total(v_quote,p_robot_id, p_user_id);
        p_quote_id := v_quote;
    END gm_create_robotics_quote;

   /**********************************************************************
 	* Description : Procedure used to generate quote id
 	* Author      : saravanan M
	************************************************************************/

    PROCEDURE gm_generate_quote_id (
        p_quote OUT t5506_robot_quotation.c5506_robot_quotation_id%TYPE
    ) AS
        p_quotedt   VARCHAR2(2000);
        p_count     VARCHAR2(20000);
    BEGIN
        SELECT
            to_char(current_date, 'mmddyy')
        INTO p_quotedt
        FROM
            dual;

        SELECT
            COUNT(c5506_robot_quotation_id) + 1
        INTO p_count
        FROM
            t5506_robot_quotation
        WHERE
            trunc(c5506_created_date) = trunc(current_date);

        p_quote := 'GMQT-'
                   || p_quotedt
                   || '-'
                   || p_count;
    END gm_generate_quote_id;

   /**********************************************************************
 	* Description : Procedure used to save quote category details
 	* Author      : saravanan M
	************************************************************************/

    PROCEDURE gm_sav_robotics_quote_category_dtls (
        p_robot_id     IN   t5500_robot_master.c5500_robot_master_id%TYPE,
        p_quote_id     IN   t5506_robot_quotation.c5506_robot_quotation_id%TYPE,
        p_company_id   IN   t5506_robot_quotation.c1900_company_id%TYPE,
        p_user_id      IN   t5506_robot_quotation.c5506_last_updated_by%TYPE
    ) AS
	
		v_set_id	t5507_robot_quotation_category_details.C207_SET_ID%TYPE;
        v_ref_name t5507_robot_quotation_category_details.c5507_ref_name%TYPE;
        --cursor to fetch category details from t5502_robot_quotation_category_master 
        --& t5503_robot_quotation_category_template
        CURSOR robot_category_dtls_cur IS
        SELECT
            t5502.c5502_robot_quotation_category_master_id   category_id,
            t5503.c5503_ref_id                               ref_id,
            t5503.c901_ref_type                              ref_type,
            t5500.c901_robot_type                            robot_type,
            t5503.c5503_suggestion                           suggestion,
            t5503.c5503_default_qty                          d_qty,
            t5502.c5502_seq_number                           seq_num,
            t5503.c5503_seq_number                           seq_num2,
            t5502.c901_approval_category_section             approval_category_section,
            t5502.c901_discount_approval                     disc_approval,
            t5503.C5503_EM_DEFAULT_ACCOUNT_TYPE              acc_type,
            t5503.C5503_EM_DEFAULT_ORDER_TYPE				 order_type,
            t5503.C5503_EM_DEFAULT_PLANT_ID					 plant_id,
            t5503.C5503_EM_DEFAULT_PLANT_NAME				 plant_nm,
            t5503.C5503_EM_DEFAULT_SHIP_TO_TYPE_NAME  		 ship_to_type,
            t5503.C5503_EM_DEFAULT_CATEGORY_STATUS           cat_status,
            t5503.C5503_ORDER_CATEGORY_SEQ					 ord_cat_seq
        FROM
            t5500_robot_master                        t5500,
            t5502_robot_quotation_category_master     t5502,
            t5503_robot_quotation_category_template   t5503
        WHERE
			t5500.c5500_robot_master_id = p_robot_id
            AND t5500.c5500_robot_master_id = t5502.c5500_robot_master_id
            AND t5502.c5502_robot_quotation_category_master_id = t5503.c5502_robot_quotation_category_master_id
            AND t5500.c5500_void_fl IS NULL
            AND t5502.c5502_void_fl IS NULL
            AND t5503.c5503_void_fl IS NULL
        ORDER BY
            t5502.c5502_seq_number,
            t5503.c5503_seq_number;

    BEGIN
    	--Looping the cursor and storing it in category details,set master,set price,set details 
    	--by another procedure call-(gm_sav_robotics_quote_price_dtls)
        FOR category_dtls IN robot_category_dtls_cur LOOP
            IF category_dtls.ref_type = '111120' THEN  --Part(111120)
            v_set_id := '';
                BEGIN
                    SELECT DISTINCT
                        c205_part_num_desc
                    INTO v_ref_name
                    FROM
                        t205_part_number t205
                    WHERE
                        t205.c205_part_number_id = category_dtls.ref_id;

                EXCEPTION
                    WHEN no_data_found THEN
                        v_ref_name := '';
                END;
            ELSIF category_dtls.ref_type = '111121' THEN --set(111121)
            v_set_id:= category_dtls.ref_id;
                BEGIN
                    SELECT
                        c207_set_nm
                    INTO v_ref_name
                    FROM
                        t207_set_master
                    WHERE
                        c207_set_id = category_dtls.ref_id;

                EXCEPTION
                    WHEN no_data_found THEN
                        v_ref_name := '';
                END;
            ELSE
                v_ref_name := '';
            END IF;
			
			--initial insert in t5507_robot_quotation_category_details table(datas about the ref id)

            gm_pkg_sm_robotics_quote_master.gm_sav_robot_quotation_category_details(p_quote_id, category_dtls.category_id,v_set_id, 
            category_dtls.ref_id, v_ref_name, category_dtls.ref_type,category_dtls.suggestion, category_dtls.d_qty
            , category_dtls.approval_category_section, category_dtls.disc_approval,
            category_dtls.acc_type,category_dtls.order_type,category_dtls.plant_id,category_dtls.plant_nm,category_dtls.ship_to_type,
            category_dtls.cat_status,category_dtls.ord_cat_seq, p_user_id);
            
			--To check the type of ref id and save accordinglt to the respected table

            gm_sav_robotics_quote_price_dtls(p_quote_id, category_dtls.ref_id, category_dtls.ref_type, category_dtls.category_id
            , p_company_id,p_user_id);

        END LOOP;
    END gm_sav_robotics_quote_category_dtls;

   /**********************************************************************
 	* Description : Procedure used to save price details based on the type-ref id
 	* Author      : saravanan M
	************************************************************************/

	PROCEDURE gm_sav_robotics_quote_price_dtls (
		p_quote_id      IN   t5507_robot_quotation_category_details.c5506_robot_quotation_id%TYPE,
		p_ref_id        IN   t5507_robot_quotation_category_details.c5507_ref_id%TYPE,
		p_ref_type      IN   t5507_robot_quotation_category_details.c901_ref_type%TYPE,
		p_category_id   IN   t5507_robot_quotation_category_details.c5502_robot_quotation_category_master_id%TYPE,
		p_company_id    IN   t5506_robot_quotation.c1900_company_id%TYPE,
		p_user_id       IN   t5507_robot_quotation_category_details.c5507_last_updated_by%TYPE
	) AS
		v_list_price              t2052_part_price_mapping.c2052_list_price%TYPE;
		v_comptxn_curr            t1900_company.c901_txn_currency%TYPE;
		v_corporate_price         t5507_robot_quotation_category_details.c5507_corporate_list_price%TYPE;
		v_part_num_id             t208_set_details.c205_part_number_id%TYPE;
		v_total_list_price        t5509_robot_quotation_set_detail_price.c5509_list_price%TYPE;
		v_total_corporate_price   t5509_robot_quotation_set_detail_price.c5509_corporate_price%TYPE;
		v_cnt                     NUMBER;
		v_part_qty                t208_set_details.c208_set_qty%TYPE;
        --cursor to get set details based on ref id
		CURSOR v_setlist IS
		SELECT c205_part_number_id part_num_id
		FROM t208_set_details t208
		WHERE c207_set_id = p_ref_id
		      AND c208_void_fl IS NULL;
	BEGIN
		--if the ref id is a part then fetch the part price and save it in t5507
		IF p_ref_type = '111120' THEN --Part(111120)
        	--To calculate ListPrice and CorporatePrice
			gm_robotics_quote_list_price_calc (p_ref_id, p_company_id, v_list_price, v_comptxn_curr, v_corporate_price
			);
            
            --To update t5507_robot_quotation_category_details table
			gm_pkg_sm_robotics_quote_master.gm_upd_robotics_quote_list_price (p_quote_id, p_ref_id, v_list_price
			, v_corporate_price, p_category_id,p_user_id);
		END IF;
		--if the ref id is a set, save it in t5505,t5508 and t5509
		IF p_ref_type = '111121' THEN --Set(111121)
			SELECT COUNT (1)
			INTO v_cnt
			FROM t5508_robot_quotation_set_price
			WHERE c5506_robot_quotation_id = p_quote_id
			      AND c207_set_id = p_ref_id
			      AND c5508_void_fl IS NULL;
			IF v_cnt = 0 THEN
            --To save set details in set master
				gm_pkg_sm_robotics_quote_master.gm_sav_robotics_quot_set_master (p_quote_id, p_ref_id, p_user_id)
				;
				
				FOR v_set IN v_setlist LOOP
                --To calculate ListPrice and CorporatePrice
					gm_robotics_quote_list_price_calc (v_set.part_num_id, p_company_id, v_list_price, v_comptxn_curr
					, v_corporate_price);
					
                    --To fetch part quantity 
					SELECT c208_set_qty
					INTO v_part_qty
					FROM t208_set_details t208
					WHERE c207_set_id = p_ref_id
					      AND c205_part_number_id = v_set.part_num_id
					      AND c208_void_fl IS NULL;
    
                    --To save set details in t5509_robot_quotation_set_detail_price table
					gm_pkg_sm_robotics_quote_master.gm_sav_robotics_quote_set_details (p_quote_id, p_ref_id, v_set.part_num_id
					, v_comptxn_curr, v_list_price,v_corporate_price, v_part_qty,p_company_id, p_user_id);
				END LOOP;
			END IF;
			
			BEGIN
				SELECT SUM (c5509_default_qty * c5509_list_price),
				       SUM (c5509_default_qty * c5509_corporate_price)
				INTO
					v_total_list_price,
					v_total_corporate_price
				FROM t5509_robot_quotation_set_detail_price
				WHERE c207_set_id = p_ref_id
				      AND c5506_robot_quotation_id = p_quote_id
				      AND c5509_void_fl IS NULL;
			EXCEPTION
				WHEN no_data_found THEN
					v_total_list_price := 0;
					v_total_corporate_price := 0;
			END;
            
			--To save set price details in t5508_robot_quotation_set_price table
			gm_pkg_sm_robotics_quote_master.gm_upd_robotics_quot_set_price (p_quote_id, p_ref_id, v_total_list_price
			, v_total_corporate_price, p_user_id);
            
            --To update list price and corporate price in t5507_robot_quotation_category_details
			gm_pkg_sm_robotics_quote_master.gm_upd_robotics_quote_list_price (p_quote_id, p_ref_id, v_total_list_price
			, v_total_corporate_price, p_category_id,p_user_id);
		END IF;
	END gm_sav_robotics_quote_price_dtls;

   /**********************************************************************
 	* Description : Procedure used to calculate list price,corp price
 	* Author      : saravanan M
	************************************************************************/

	PROCEDURE gm_robotics_quote_list_price_calc (
		p_ref_id            IN    t5507_robot_quotation_category_details.c5507_ref_id%TYPE,
		p_company_id        IN    t5506_robot_quotation.c1900_company_id%TYPE,
		p_list_price        OUT   t2052_part_price_mapping.c2052_list_price%TYPE,
		p_comptxn_curr      OUT   t1900_company.c901_txn_currency%TYPE,
		p_corporate_price   OUT   t5507_robot_quotation_category_details.c5507_corporate_list_price%TYPE
	) AS
		v_list_price        t2052_part_price_mapping.c2052_list_price%TYPE;
		v_comptxn_curr      t1900_company.c901_txn_currency%TYPE;
		v_rept_curr         t1900_company.c901_reporting_currency%TYPE;
		v_currency_value    t5512_robot_currency_conver.c5512_currency_value%TYPE;
		v_corporate_price   t5507_robot_quotation_category_details.c5507_corporate_list_price%TYPE;
	BEGIN
		BEGIN
        --to get listprice by partnumber
			SELECT nvl (t2052.c2052_list_price, 0)
			INTO v_list_price
			FROM t2052_part_price_mapping   t2052,
			     t205_part_number           t205
			WHERE t205.c205_part_number_id = p_ref_id
			      AND t205.c205_part_number_id = t2052.c205_part_number_id (+)
			      AND t2052.c1900_company_id (+) = p_company_id
			      AND t2052.c2052_void_fl IS NULL;
		EXCEPTION
			WHEN no_data_found THEN v_list_price := 0;
		END;
		p_list_price := v_list_price;
		BEGIN
	--Get company currency details from below query
			SELECT c901_txn_currency,
			       c901_reporting_currency
			INTO
				v_comptxn_curr,
				v_rept_curr
			FROM t1900_company
			WHERE c1900_company_id = p_company_id
			      AND c1900_void_fl IS NULL;
		EXCEPTION
			WHEN no_data_found THEN
				v_comptxn_curr := 0;
				v_rept_curr := 0;
		END;
		p_comptxn_curr := v_comptxn_curr;
        
	--Get corporate price by below function	
		v_currency_value := get_robot_quote_currency_conversion (v_comptxn_curr, v_rept_curr);
		p_corporate_price := v_currency_value * v_list_price;
	END gm_robotics_quote_list_price_calc;

   /**********************************************************************
 	* Description : Procedure used to get the currency conversion rate
 	* Author      : saravanan M
	************************************************************************/

    FUNCTION get_robot_quote_currency_conversion (
		p_comptxn_curr   IN   t1900_company.c901_txn_currency%TYPE,
		p_rept_curr      IN   t1900_company.c901_reporting_currency%TYPE
	) RETURN NUMBER IS
		v_currency_value t5512_robot_currency_conver.c5512_currency_value%TYPE;
	BEGIN
		BEGIN
			SELECT c5512_currency_value
			INTO v_currency_value
			FROM t5512_robot_currency_conver
			WHERE c901_currency_from = p_comptxn_curr
			      AND c901_currency_to = p_rept_curr
			      AND c5512_from_date <= current_date
			      AND c5512_to_date >= current_date
			      AND c5512_void_fl IS NULL;
		EXCEPTION
			WHEN no_data_found THEN v_currency_value := 0;
		END;
		RETURN v_currency_value;
	END;

   /**********************************************************************
 	* Description : Procedure used to save quote discount details
 	* Author      : saravanan M
	************************************************************************/

	PROCEDURE gm_sav_robotics_quote_discount_dtls (
		p_robot_id     IN   t5500_robot_master.c5500_robot_master_id%TYPE,
		p_quote_id     IN   t5506_robot_quotation.c5506_robot_quotation_id%TYPE,
		p_company_id   IN   t5506_robot_quotation.c1900_company_id%TYPE,
		p_user_id      IN   t5506_robot_quotation.c5506_last_updated_by%TYPE
	) AS
		v_list_price   t5509_robot_quotation_set_detail_price.c5509_list_price%TYPE;
		v_corp_price   t5509_robot_quotation_set_detail_price.c5509_corporate_price%TYPE;
		
		CURSOR discount_cur IS
		SELECT c901_code_id   disc_id,
		       c901_code_nm   disc_nm
		FROM t901_code_lookup
		WHERE c901_code_grp = 'ROQTSC'    --code grp to fetch category section id and name
		      AND c901_active_fl = '1'
		      AND c901_void_fl IS NULL
		ORDER BY c901_code_seq_no;
	BEGIN
		FOR disc_cur IN discount_cur LOOP
			BEGIN
				SELECT SUM (c5507_qty * c5507_list_price),
				       SUM (c5507_qty * c5507_corporate_list_price)
				INTO
					v_list_price,
					v_corp_price
				FROM t5507_robot_quotation_category_details t5507
				WHERE t5507.c901_approval_category_section = disc_cur.disc_id
				      AND t5507.c5507_void_fl IS NULL
				      AND t5507.c5506_robot_quotation_id = p_quote_id
				      AND t5507.C5507_REF_ID NOT IN(SELECT C906_RULE_VALUE from T906_RULES WHERE C906_RULE_ID = 'SERVICE_PARTS' AND C906_RULE_GRP_ID = 'INR_SKIP_PRICE_TOTAL');
				      --999.891 service part should not include in quote total (PC-5162);
			EXCEPTION
				WHEN no_data_found THEN
					v_list_price := 0;
					v_corp_price := 0;
			END;
			--To save category section wise discount details
			gm_pkg_sm_robotics_quote_master.gm_sav_robotics_quot_discount_approval (p_quote_id, disc_cur.disc_id
			, disc_cur.disc_nm, v_list_price, v_corp_price,p_user_id);
		END LOOP;
	END gm_sav_robotics_quote_discount_dtls;

   /**********************************************************************
 	* Description : Procedure used to update category line item detail
 	* while changing qty , list price
 	* Author      : saravanan M
	************************************************************************/

	PROCEDURE gm_upd_category_line_item_dtl (
		p_robot_id      IN   t5500_robot_master.c5500_robot_master_id%TYPE,
		p_quote_id      IN   t5506_robot_quotation.c5506_robot_quotation_id%TYPE,
		p_ref_id        IN   t5507_robot_quotation_category_details.c5507_ref_id%TYPE,
		p_set_id        IN   t5507_robot_quotation_category_details.c207_set_id%TYPE,
		p_qty           IN   t5507_robot_quotation_category_details.c5507_qty%TYPE,
		p_list_price    IN   t2052_part_price_mapping.c2052_list_price%TYPE,
		p_category_id   IN   t5507_robot_quotation_category_details.c5502_robot_quotation_category_master_id%TYPE,
		p_user_id       IN   t5506_robot_quotation.c5506_last_updated_by%TYPE,
		p_company_id    IN   t5506_robot_quotation.c1900_company_id%TYPE
	) AS
		v_comptxn_curr                t1900_company.c901_txn_currency%TYPE;
		v_rept_curr                   t1900_company.c901_reporting_currency%TYPE;
		v_corporate_price             t5507_robot_quotation_category_details.c5507_corporate_list_price%TYPE;
		v_approval_category_section   t5502_robot_quotation_category_master.c901_approval_category_section%TYPE;
		v_currency_value              t5512_robot_currency_conver.c5512_currency_value%TYPE;
	BEGIN
		BEGIN
			SELECT c901_txn_currency,
			       c901_reporting_currency
			INTO
				v_comptxn_curr,
				v_rept_curr
			FROM t1900_company
			WHERE c1900_company_id = p_company_id
			      AND c1900_void_fl IS NULL;
		EXCEPTION
			WHEN no_data_found THEN
				v_comptxn_curr := 0;
				v_rept_curr := 0;
		END;
		v_currency_value := get_robot_quote_currency_conversion (v_comptxn_curr, v_rept_curr);
		v_corporate_price := v_currency_value * p_list_price;
        
        --To update the qty,listprice and corporate price
		gm_pkg_sm_robotics_quote_master.gm_upd_category_line_item_detail (
		p_qty, p_list_price, v_corporate_price, p_ref_id, p_quote_id,p_user_id);
		BEGIN
			SELECT c901_approval_category_section
			INTO v_approval_category_section
			FROM t5502_robot_quotation_category_master
			WHERE c5502_robot_quotation_category_master_id = p_category_id
			      AND c5502_void_fl IS NULL;
		EXCEPTION
			WHEN no_data_found THEN v_approval_category_section := 0;
		END;
		--To update discount approvals dtls table
		gm_pkg_sm_robotics_quote_discount.gm_upd_discount_category (p_quote_id, v_approval_category_section, p_user_id);
        
        --To update Percentage calculation for Ala carte section
		IF v_approval_category_section = '110805' THEN gm_pkg_sm_robotics_quote_trans.gm_update_alacarte_discount_dtls (p_company_id, p_quote_id
		, p_category_id, v_approval_category_section, p_ref_id,p_set_id, p_qty, p_list_price, p_user_id);
		END IF;
        
        --To update overall quote and corp total
		gm_pkg_sm_robotics_quote_trans.gm_upd_robotic_quote_total (p_quote_id, p_robot_id, p_user_id);
        --To update status
		gm_upd_quote_status (p_quote_id, '110814', p_user_id);
	END gm_upd_category_line_item_dtl;

   /**********************************************************************
 	* Description : Procedure used to update robot quotation total
 	* Author      : saravanan M
	************************************************************************/

    PROCEDURE gm_upd_robotic_quote_total (
        p_quote_id   IN   t5506_robot_quotation.c5506_robot_quotation_id%TYPE,
        p_robot_id   IN   t5500_robot_master.c5500_robot_master_id%TYPE,
        p_user_id    IN   t5506_robot_quotation.c5506_last_updated_by%TYPE
    ) AS
        v_taxRate					t5506_robot_quotation.C5506_TAX_PERCENTAGE%TYPE;
        v_company_id        		t5506_robot_quotation.c1900_company_id%TYPE;
        v_category_master_id		t5502_robot_quotation_category_master.c5502_robot_quotation_category_master_id%TYPE;
    BEGIN
    	SELECT NVL(get_compid_frm_cntx(),GET_RULE_VALUE('DEFAULT_COMPANY','ONEPORTAL')) into v_company_id from dual;
    	
    	--To get Tax rate by quote id-for the calculation of total
		BEGIN
			SELECT c5506_tax_percentage
			INTO v_taxRate
			FROM t5506_robot_quotation
			WHERE c5506_robot_quotation_id = p_quote_id;
		EXCEPTION
			WHEN no_data_found THEN v_taxRate := 0;
		END;
		
		--To fetch the category ID by category group id and robot master Id
		SELECT c5502_robot_quotation_category_master_id
		INTO v_category_master_id
		FROM t5502_robot_quotation_category_master
		WHERE c901_category_group_id = '110790' --manual adjustment category id
		      AND c5500_robot_master_id = p_robot_id;
		      
        --To calculate total with approval subtotal or category subtotal with manual adjustment
		gm_pkg_sm_robotics_quote_trans.gm_calculate_total_amount(p_quote_id,v_taxRate,v_category_master_id,p_user_id,v_company_id);

    END gm_upd_robotic_quote_total;
    
	/**********************************************************************
	 	* Description : Procedure used to update quote status in robotics quote tool
	 	* Author      : tmuthusamy
		************************************************************************/

    	PROCEDURE gm_upd_quote_status (
		p_quote_id   IN   t5506_robot_quotation.c5506_robot_quotation_id%TYPE,
		p_status     IN   t5506_robot_quotation.c901_quote_status%TYPE,
		p_user_id    IN   t5506_robot_quotation.c5506_last_updated_by%TYPE
	) AS
		v_status t5506_robot_quotation.c901_quote_status%TYPE;
	BEGIN
		SELECT c901_quote_status
		INTO v_status
		FROM t5506_robot_quotation
		WHERE c5506_robot_quotation_id = p_quote_id;
		IF v_status = '110813' OR v_status = '110814' THEN
    	--IF p_status != '110813' and p_status != '110814' THEN
		 UPDATE t5506_robot_quotation
		SET c901_quote_status = p_status,
		    c5506_last_updated_by = p_user_id,
		    c5506_last_updated_date = current_date
		WHERE c5506_robot_quotation_id = p_quote_id;
		END IF;
		IF v_status = '110815' OR v_status = '110816' OR v_status = '110817' OR v_status = '110820' THEN
		 IF p_status != '110813' AND p_status != '110814' THEN
			UPDATE t5506_robot_quotation
			SET c901_quote_status = p_status,
			    c5506_last_updated_by = p_user_id,
			    c5506_last_updated_date = current_date
			WHERE c5506_robot_quotation_id = p_quote_id;
		END IF;
		END IF;
		--110822- Set Allocation In-Progress --110823 Pending order process --4951
		--110823- Pending Order process		 --110824 order process in-progress
		--110826-Pending shipping			 --110827 Shipping Completed
		IF v_status = '110822' OR v_status = '110823' OR v_status = '110824' OR v_status = '110826' THEN
		  IF p_status = '110823' OR p_status = '110824' OR p_status = '110826' OR p_status = '110827' THEN
			UPDATE t5506_robot_quotation
			SET c901_quote_status = p_status,
			    c5506_last_updated_by = p_user_id,
			    c5506_last_updated_date = current_date
			WHERE c5506_robot_quotation_id = p_quote_id;
		  END IF;
		END IF;
	END gm_upd_quote_status;
	/**********************************************************************
 	* Description : Procedure used to save/update ala_carte_dtls set/part details
 	* Author      : saravanan M
	************************************************************************/
		PROCEDURE gm_sav_ala_carte_dtls (
		p_input_string                IN   CLOB,
		p_quote_id                    IN   t5506_robot_quotation.c5506_robot_quotation_id%TYPE,
		p_approval_category_section   IN   t5510_robot_quotation_discount_approval_dtls.c901_approval_category_section%TYPE,
		p_robot_id                    IN   t5500_robot_master.c5500_robot_master_id%TYPE,
		p_category_id                 IN   t5507_robot_quotation_category_details.c5502_robot_quotation_category_master_id%TYPE,
		p_company_id                  IN   t5506_robot_quotation.c1900_company_id%TYPE,
		p_user_id                     IN   t5506_robot_quotation.c5506_last_updated_by%TYPE
	) AS
		v_alacarte_json             json_array_t;
		v_sav_alacarte_item         json_object_t;
		v_comptxn_curr              t1900_company.c901_txn_currency%TYPE;
		v_rept_curr                 t1900_company.c901_reporting_currency%TYPE;
		v_currency_value            t5512_robot_currency_conver.c5512_currency_value%TYPE;
		v_corporate_price           t5507_robot_quotation_category_details.c5507_corporate_list_price%TYPE;
		v_ref_id                    t5507_robot_quotation_category_details.c5507_ref_id%TYPE;
		v_set_id                    t5507_robot_quotation_category_details.c207_set_id%TYPE;
		v_description               t5507_robot_quotation_category_details.c5507_ref_name%TYPE;
		v_ref_type                  t5507_robot_quotation_category_details.c901_ref_type%TYPE;
		v_d_qty                     t5507_robot_quotation_category_details.c5507_default_qty%TYPE;
		v_req_qty                   t5507_robot_quotation_category_details.c5507_qty%TYPE;
		v_approv_category_section   t5507_robot_quotation_category_details.c901_approval_category_section%TYPE;
		v_discount_approval         t5507_robot_quotation_category_details.c901_discount_approval%TYPE;
		v_listprice                 t5507_robot_quotation_category_details.c5507_list_price%TYPE;
		v_voidfl                    t5507_robot_quotation_category_details.c5507_void_fl%TYPE;
		v_category_master_id        t5502_robot_quotation_category_master.c5502_robot_quotation_category_master_id%TYPE;
		v_order_cat_seq				t5507_robot_quotation_category_details.C5507_ORDER_CATEGORY_SEQ%TYPE;
		v_account_type				t5503_robot_quotation_category_template.c5503_em_default_account_type%type;
		v_def_order_type            t5503_robot_quotation_category_template.c5503_em_default_order_type%type;
		v_default_plant_name		t5503_robot_quotation_category_template.c5503_em_default_plant_name%type;
		v_default_ship_to 			t5503_robot_quotation_category_template.c5503_em_default_ship_to_type_name%type;
	BEGIN
		v_alacarte_json := json_array_t (p_input_string);
		BEGIN
			SELECT c5502_robot_quotation_category_master_id
			INTO v_category_master_id
			FROM t5502_robot_quotation_category_master
			WHERE c901_category_group_id = p_category_id
			      AND c5500_robot_master_id = p_robot_id;
			SELECT c901_txn_currency,
			       c901_reporting_currency
			INTO
				v_comptxn_curr,
				v_rept_curr
			FROM t1900_company
			WHERE c1900_company_id = p_company_id
			      AND c1900_void_fl IS NULL;
		EXCEPTION
			WHEN no_data_found THEN
				v_comptxn_curr := 0;
				v_rept_curr := 0;
		END;
		-- If (7- US Alacarte then order Category Seq is 9 ) ElseIf (18- OUS Alacarte then order Category Seq is 21 )
		IF (v_category_master_id = 7) THEN
			v_order_cat_seq := 9;
		ELSIF (v_category_master_id = 18) THEN
			v_order_cat_seq := 21;
		END IF;
		
		FOR i IN 0..v_alacarte_json.get_size - 1 LOOP
			v_sav_alacarte_item := TREAT (v_alacarte_json.get (i) AS json_object_t);
			v_ref_id := v_sav_alacarte_item.get_string ('strRefId');
			v_set_id := v_sav_alacarte_item.get_string ('strSetId');
			v_description := v_sav_alacarte_item.get_string ('strDescription');
			v_ref_type := v_sav_alacarte_item.get_string ('strRefType');
			v_d_qty := v_sav_alacarte_item.get_string ('strDefaultQty');
			v_req_qty := v_sav_alacarte_item.get_string ('strQty');
			v_approv_category_section := v_sav_alacarte_item.get_string ('strApprovalCategorySection');
			v_discount_approval := v_sav_alacarte_item.get_string ('strDiscountApproval');
			v_listprice := v_sav_alacarte_item.get_string ('strListPrice');
			v_voidfl := v_sav_alacarte_item.get_string ('strVoidFL');

        --to get currency value 
			v_currency_value := get_robot_quote_currency_conversion (v_comptxn_curr, v_rept_curr);
			v_corporate_price := v_currency_value * v_listprice;
        
		--PC-5833: TO show A-La-Carte part's order in respective account
		BEGIN
			SELECT c5503_em_default_account_type,c5503_em_default_plant_name,c5503_em_default_ship_to_type_name
			  INTO v_account_type,v_default_plant_name,v_default_ship_to
			  FROM t5503_robot_quotation_category_template
			 WHERE C5503_REF_ID = v_set_id
			   AND rownum = 1 -- Catgory master id different for US and OUS but plant ,account type and ship to are same
			   AND c5503_void_fl is null;
	   EXCEPTION WHEN OTHERS THEN
	   		v_account_type := null;
	   		v_default_plant_name := null;
	   		v_default_ship_to := null;
	   END;
	   
			   
			 IF v_account_type = 'Capital' AND v_category_master_id = 7 THEN  --US
			 	v_order_cat_seq := 9;
			 	v_def_order_type := 'Direct';
			 ELSIF v_account_type = 'Metal' AND v_category_master_id = 7 THEN  --US
			 	v_order_cat_seq := 10;
			 	v_def_order_type := 'Direct';
			 ELSIF v_account_type = 'Capital' AND v_category_master_id = 18 THEN  --OUS
			 	v_order_cat_seq := 21;
			 	v_def_order_type := 'OUS Distributor';
			 ELSIF v_account_type = 'Metal' AND v_category_master_id = 18 THEN  --OUS
			 	v_order_cat_seq := 22;
			 	v_def_order_type := 'OUS Distributor';
			 END IF;
			 
        --To update/save selected alacart set/part details into category details table	    
			UPDATE t5507_robot_quotation_category_details
			SET c5507_default_qty = v_d_qty,
			    c5507_qty = v_req_qty,
			    c5507_list_price = v_listprice,
			    c5507_corporate_list_price = v_corporate_price,
			    c5507_void_fl = v_voidfl,
			    c5507_em_quote_account_type = v_account_type,
            	c5507_em_quote_order_type = v_def_order_type,
            	c5507_em_quote_ship_to_type_name = v_default_ship_to,
			    c5507_last_updated_by = p_user_id,
			    c5507_last_updated_date = current_date
			WHERE c5506_robot_quotation_id = p_quote_id
			      AND c5502_robot_quotation_category_master_id = v_category_master_id
			      AND c5507_ref_id = v_ref_id
			      AND c207_set_id = v_set_id;
			IF (SQL%rowcount = 0) THEN INSERT INTO t5507_robot_quotation_category_details (
				c5506_robot_quotation_id,
				c5502_robot_quotation_category_master_id,
				c5507_ref_id,
				c207_set_id,
				c5507_ref_name,
				c901_ref_type,
				c5507_default_qty,
				c5507_qty,
				c901_approval_category_section,
				c901_discount_approval,
				c5507_list_price,
				c5507_corporate_list_price,
				c5507_em_quote_account_type,
            	c5507_em_quote_order_type,
            	c5040_plant_id,
            	c5040_plant_name,
            	c5507_em_quote_ship_to_type_name,
            	c5507_em_quote_order_category_status,
            	c5507_order_category_seq,
				c5507_last_updated_by,
				c5507_last_updated_date
			) VALUES (
				p_quote_id,
				v_category_master_id,
				v_ref_id,
				v_set_id,
				v_description,
				v_ref_type,
				v_d_qty,
				v_req_qty,
				p_approval_category_section,
				v_discount_approval,
				v_listprice,
				v_corporate_price,
				v_account_type,
				v_def_order_type,
				3000,
				v_default_plant_name,
				v_default_ship_to,
				'Ready',	
				v_order_cat_seq,
				p_user_id,
				current_date
			);
			END IF;
		END LOOP;
		--To update discount category based on the new part added
		gm_pkg_sm_robotics_quote_discount.gm_upd_discount_category (p_quote_id, p_approval_category_section, p_user_id);
		--To update overall Total
		gm_pkg_sm_robotics_quote_trans.gm_upd_robotic_quote_total (p_quote_id, p_robot_id, p_user_id);
		gm_pkg_sm_robotics_quote_trans.gm_update_alacarte_discount_dtls (p_company_id, p_quote_id, v_category_master_id, p_approval_category_section
		, v_ref_id,v_set_id, v_req_qty, v_listprice, p_user_id);
	END gm_sav_ala_carte_dtls;
	
	/**********************************************************************
 	* Description : Procedure used to void ala_carte_dtls set/part details
 	* Author      : saravanan M
	************************************************************************/
	PROCEDURE gm_void_la_cart_lookup(
		p_category_dtl_id				IN	t5507_robot_quotation_category_details.C5507_ROBOT_QUOTATION_CATEGORY_DETAILS_ID%TYPE,
		p_user_id						IN	t5507_robot_quotation_category_details.C5507_LAST_UPDATED_BY%TYPE
		) 
		AS
		v_robot_quote_id	t5506_robot_quotation.c5506_robot_quotation_id%TYPE;
		v_category_section	t5507_robot_quotation_category_details.c901_approval_category_section%TYPE;
		v_robot_master_id	t5507_robot_quotation_category_details.C5502_ROBOT_QUOTATION_CATEGORY_MASTER_ID%TYPE;
		
		BEGIN
		SELECT
		    c5506_robot_quotation_id,
		    c901_approval_category_section,
		    C5502_ROBOT_QUOTATION_CATEGORY_MASTER_ID
		    INTO
		    v_robot_quote_id,
		    v_category_section,
		    v_robot_master_id
			FROM
			    t5507_robot_quotation_category_details
			WHERE
			    c5507_robot_quotation_category_details_id = p_category_dtl_id;
		update t5507_robot_quotation_category_details
		set C5507_VOID_FL = 'Y',
		C5507_LAST_UPDATED_BY = p_user_id,
		C5507_LAST_UPDATED_DATE = current_date
		where C5507_ROBOT_QUOTATION_CATEGORY_DETAILS_ID= p_category_dtl_id
		AND C5507_VOID_FL IS NULL;
		
		gm_pkg_sm_robotics_quote_discount.gm_upd_discount_category(v_robot_quote_id,v_category_section,p_user_id);
		
		gm_pkg_sm_robotics_quote_trans.gm_discount_approval_ala_carte_update(v_robot_quote_id,v_robot_master_id,v_category_section,p_user_id);
		
	END gm_void_la_cart_lookup;

	/**********************************************************************
 	* Description : Procedure used to save manual adjustment line item detail
 	* Author      : saravanan M
	************************************************************************/
	PROCEDURE gm_upd_manual_adjustment_line_item_dtl (
		p_robot_id      IN   t5500_robot_master.c5500_robot_master_id%TYPE,
		p_quote_id      IN   t5506_robot_quotation.c5506_robot_quotation_id%TYPE,
		p_ref_id        IN   t5507_robot_quotation_category_details.c5507_ref_id%TYPE,
		p_qty           IN   t5507_robot_quotation_category_details.c5507_qty%TYPE,
		p_list_price    IN   t2052_part_price_mapping.c2052_list_price%TYPE,
		p_category_id   IN   t5507_robot_quotation_category_details.c5502_robot_quotation_category_master_id%TYPE,
		p_taxrate       IN   t5506_robot_quotation.c5506_tax_percentage%TYPE,
		p_user_id       IN   t5506_robot_quotation.c5506_last_updated_by%TYPE,
		p_company_id    IN   t5506_robot_quotation.c1900_company_id%TYPE
	) AS
		v_category_sub_total   t5510_robot_quotation_discount_approval_dtls.c5510_category_sub_total%TYPE;
		v_manual_sub_total     t5507_robot_quotation_category_details.c5507_list_price%TYPE;
		v_tax_amount           t5506_robot_quotation.c5506_tax_amount%TYPE;
		v_currency_value       t5512_robot_currency_conver.c5512_currency_value%TYPE;
		v_comptxn_curr         t1900_company.c901_txn_currency%TYPE;
		v_rept_curr            t1900_company.c901_reporting_currency%TYPE;
		v_corporate_price      t5507_robot_quotation_category_details.c5507_corporate_list_price%TYPE;
		v_corp_price           t5507_robot_quotation_category_details.c5507_corporate_list_price%TYPE;
		v_quote_total          t5506_robot_quotation.c5506_robot_quote_total%TYPE;
	BEGIN
		--To get currency value
		BEGIN
			SELECT c901_txn_currency,
			       c901_reporting_currency
			INTO
				v_comptxn_curr,
				v_rept_curr
			FROM t1900_company
			WHERE c1900_company_id = p_company_id
			      AND c1900_void_fl IS NULL;
		EXCEPTION
			WHEN no_data_found THEN
				v_comptxn_curr := 0;
				v_rept_curr := 0;
		END;
		v_currency_value := get_robot_quote_currency_conversion (v_comptxn_curr, v_rept_curr);
       
        
        --calculating corporate price
		v_corporate_price := v_currency_value * p_list_price;
        
        --To update the qty,listprice and corporate price
		gm_pkg_sm_robotics_quote_master.gm_upd_category_line_item_detail (p_qty, p_list_price, v_corporate_price, p_ref_id, p_quote_id,
		                                                                 p_user_id);
 		--To calculate Total amount with the adjustments
		gm_pkg_sm_robotics_quote_trans.gm_calculate_total_amount (p_quote_id, p_taxrate, p_category_id, p_user_id, p_company_id);
	END gm_upd_manual_adjustment_line_item_dtl;
		
	/**********************************************************************
 	* Description : Procedure used to update total with the manual 
 	* adjustment sub total
 	* Author      : saravanan M
	************************************************************************/
	PROCEDURE gm_calculate_total_amount(
		p_quote_id      		IN   t5506_robot_quotation.c5506_robot_quotation_id%TYPE,
		p_taxRate				IN	 t5506_robot_quotation.C5506_TAX_PERCENTAGE%TYPE,
		p_category_master_id	IN	 t5503_robot_quotation_category_template.C5502_ROBOT_QUOTATION_CATEGORY_MASTER_ID%TYPE,
        p_user_id       		IN   t5506_robot_quotation.c5506_last_updated_by%TYPE,
        p_company_id    		IN   t5506_robot_quotation.c1900_company_id%TYPE
		)
		AS
		v_sub_total					t5510_robot_quotation_discount_approval_dtls.c5510_category_sub_total%TYPE;
		v_category_sub_total        t5510_robot_quotation_discount_approval_dtls.c5510_category_sub_total%TYPE;
		v_approval_sub_total		t5510_robot_quotation_discount_approval_dtls.c5510_approval_sub_total%TYPE;
        v_manual_sub_total			t5507_robot_quotation_category_details.c5507_list_price%TYPE;
		v_currency_value			t5512_robot_currency_conver.c5512_currency_value%TYPE;
		v_comptxn_curr            	t1900_company.c901_txn_currency%TYPE;
	    v_rept_curr         		t1900_company.c901_reporting_currency%TYPE;
	    v_corp_price				t5507_robot_quotation_category_details.c5507_corporate_list_price%TYPE;
       	v_quote_total				t5506_robot_quotation.c5506_robot_quote_total%TYPE;
		BEGIN
		BEGIN
			SELECT c901_txn_currency,
			       c901_reporting_currency
			INTO
				v_comptxn_curr,
				v_rept_curr
			FROM t1900_company
			WHERE c1900_company_id = p_company_id
			      AND c1900_void_fl IS NULL;
		EXCEPTION
			WHEN no_data_found THEN
				v_comptxn_curr := 0;
				v_rept_curr := 0;
		END;
		v_currency_value := get_robot_quote_currency_conversion (v_comptxn_curr, v_rept_curr);
		--to get ManualAdjustment subtotal
		BEGIN
			SELECT SUM (C5507_QTY * c5507_list_price)
			INTO v_manual_sub_total
			FROM t5507_robot_quotation_category_details
			WHERE c5506_robot_quotation_id = p_quote_id
			      AND c5502_robot_quotation_category_master_id = p_category_master_id
			      AND c5507_void_fl IS NULL
			      AND c5507_ref_id not in (SELECT c906_rule_value FROM t906_rules
									   	    WHERE c906_rule_id = 'MANUAL_ADJ_PARTS' 
									   	      AND c906_rule_grp_id='MANUAL_ADJ_SKIP_TOT' 
									   	      AND c906_void_fl is null);
		EXCEPTION
			WHEN no_data_found THEN v_manual_sub_total := 0;
		END;

        --To get discount approvals category sub total and approved subtotal
		BEGIN
			SELECT SUM (c5510_category_sub_total),
			       SUM (c5510_approval_sub_total)
			INTO
				v_category_sub_total,
				v_approval_sub_total
			FROM t5510_robot_quotation_discount_approval_dtls
			WHERE c5506_robot_quotation_id = p_quote_id;
		EXCEPTION
			WHEN no_data_found THEN
				v_category_sub_total := 0;
				v_approval_sub_total := 0;
		END;
		--subtotal based on approval total
		v_sub_total := v_category_sub_total - nvl (v_approval_sub_total, 0);
		--final adjusted total
		v_quote_total := v_sub_total + nvl (v_manual_sub_total, 0);
		--manual adjustment sub total should not include (PC-5162)
		--v_quote_total := v_sub_total;
		
		v_corp_price := v_currency_value * v_quote_total;

        --updating overall robot quote total
		UPDATE t5506_robot_quotation
		SET c5506_robot_quote_total = v_quote_total,
		    c5506_robot_quote_corporate_total = v_corp_price,
		    c5506_last_updated_by = p_user_id,
		    c5506_last_updated_date = current_date
		WHERE c5506_robot_quotation_id = p_quote_id
		      AND c5506_void_fl IS NULL;

        --calculating tax
		gm_pkg_sm_robotics_quote_trans.gm_calc_tax (p_quote_id, v_quote_total, p_taxRate, p_user_id, p_company_id);
		END gm_calculate_total_amount;
		
	/**********************************************************************
 	* Description : Procedure used to calculate tax
 	* Author      : saravanan M
	************************************************************************/
	PROCEDURE gm_calc_tax (
		p_quote_id      IN   t5506_robot_quotation.c5506_robot_quotation_id%TYPE,
		p_quote_total   IN   t5506_robot_quotation.c5506_robot_quote_total%TYPE,
		p_taxrate       IN   t5506_robot_quotation.c5506_tax_percentage%TYPE,
		p_user_id       IN   t5506_robot_quotation.c5506_last_updated_by%TYPE,
		p_company_id    IN   t5506_robot_quotation.c1900_company_id%TYPE
	) AS
		v_tax_amount       t5506_robot_quotation.c5506_tax_amount%TYPE;
		v_quote_total      t5506_robot_quotation.c5506_robot_quote_total%TYPE;
		v_corp_price       t5507_robot_quotation_category_details.c5507_corporate_list_price%TYPE;
		v_currency_value   t5512_robot_currency_conver.c5512_currency_value%TYPE;
		v_comptxn_curr     t1900_company.c901_txn_currency%TYPE;
		v_rept_curr        t1900_company.c901_reporting_currency%TYPE;
		v_taxrate          t5506_robot_quotation.c5506_tax_percentage%TYPE;
	BEGIN
		BEGIN
			SELECT c901_txn_currency,
			       c901_reporting_currency
			INTO
				v_comptxn_curr,
				v_rept_curr
			FROM t1900_company
			WHERE c1900_company_id = p_company_id
			      AND c1900_void_fl IS NULL;
		EXCEPTION
			WHEN no_data_found THEN
				v_comptxn_curr := 0;
				v_rept_curr := 0;
		END;
		v_currency_value := get_robot_quote_currency_conversion (v_comptxn_curr, v_rept_curr);
		v_tax_amount := (p_quote_total * nvl (p_taxrate, 0)) / 100;
		--calculating total with tax amount
		v_quote_total := p_quote_total + nvl (v_tax_amount, 0);
		v_corp_price := v_currency_value * v_quote_total;
		
		--overal quote total update
		UPDATE t5506_robot_quotation
		SET c5506_robot_quote_total = v_quote_total,
		    c5506_robot_quote_corporate_total = v_corp_price,
		    c5506_tax_percentage = p_taxrate,
		    c5506_tax_amount = v_tax_amount,
		    c5506_last_updated_by = p_user_id,
		    c5506_last_updated_date = current_date
		WHERE c5506_robot_quotation_id = p_quote_id
		      AND c5506_void_fl IS NULL;
	END gm_calc_tax;
	
		/**********************************************************************
	 	* Description : Procedure used to update quote status while the quote is cancelled.
	 	* Author      : saravanan M
		************************************************************************/

	PROCEDURE gm_sav_quote_cancel (
		p_quote_id       IN   t5506_robot_quotation.c5506_robot_quotation_id%TYPE,
		p_cancelreason   IN   t901_code_lookup.c901_code_id%TYPE,
		p_cancel_id      IN   t907_cancel_log.c907_cancel_log_id%TYPE,
		p_user_id        IN   t5506_robot_quotation.c5506_last_updated_by%TYPE
	) AS
		v_cancel_type t901_code_lookup.c901_code_id%TYPE;
	BEGIN
		BEGIN
			SELECT c901_type
			INTO v_cancel_type
			FROM t907_cancel_log
			WHERE c907_cancel_log_id = p_cancel_id;
		EXCEPTION
			WHEN no_data_found THEN v_cancel_type := 0;
		END;
		IF v_cancel_type = 108260 THEN UPDATE t5506_robot_quotation
		SET c901_quote_status = '110825', --Quote Cancelled
		    c5506_last_updated_by = p_user_id,
		    c5506_last_updated_date = current_date
		WHERE c5506_robot_quotation_id = p_quote_id;
		END IF;
	END gm_sav_quote_cancel;
	
	
	/**********************************************************************
 	* Description : Procedure used to update discount price, discount percentage and corporate price in ala_carte category
 	* Author      : tmuthusamy
	************************************************************************/
	PROCEDURE gm_update_alacarte_discount_dtls(
			p_company_id					IN   T5506_robot_quotation.C1900_COMPANY_ID%TYPE,
            p_quote_id  					IN   t5506_robot_quotation.c5506_robot_quotation_id%TYPE,
            p_robot_master_id   			IN   t5507_robot_quotation_category_details.C5502_ROBOT_QUOTATION_CATEGORY_MASTER_ID%TYPE,
        	p_approval_category_section   	IN	 t5502_robot_quotation_category_master.c901_approval_category_section%TYPE,
            p_refid        					IN   t5507_robot_quotation_category_details.C5507_REF_ID%TYPE,
            p_setid   						IN   t5507_robot_quotation_category_details.C207_SET_ID%TYPE,
            p_qty   						IN   t5507_robot_quotation_category_details.C5507_QTY%TYPE,
            p_list_price   					IN   t5507_robot_quotation_category_details.C5507_LIST_PRICE%TYPE,
            p_user_id    					IN   t5506_robot_quotation.c5506_last_updated_by%TYPE
	)AS
		v_discount_per		 			t5507_robot_quotation_category_details.C5507_DISCOUNT_PER%TYPE;
		v_discount_price 	 			t5507_robot_quotation_category_details.C5507_DISCOUNT_PRICE%TYPE;
	    v_comptxn_curr       			t1900_company.c901_txn_currency%TYPE;
	    v_rept_curr          			t1900_company.c901_reporting_currency%TYPE;
	    v_currency_value     			t5512_robot_currency_conver.c5512_currency_value%TYPE;
		v_corporate_discount_price		t5507_robot_quotation_category_details.C5507_CORPORATE_DISCOUNT_PRICE%TYPE;
	BEGIN
	
	BEGIN
	SELECT C5507_DISCOUNT_PER
	INTO 
		v_discount_per
    FROM 
    	T5507_ROBOT_QUOTATION_CATEGORY_DETAILS
    WHERE 
    	C5506_ROBOT_QUOTATION_ID = p_quote_id
    	AND C5502_ROBOT_QUOTATION_CATEGORY_MASTER_ID != p_robot_master_id
	    AND C5507_REF_ID = p_setid
	    AND C207_SET_ID = p_setid;
	    
    EXCEPTION WHEN NO_DATA_FOUND
    THEN
	    v_discount_per:=0;
    END;
    
    BEGIN
    v_discount_price:= ((p_list_price * v_discount_per) / 100 ) * p_qty;
    
    EXCEPTION WHEN NO_DATA_FOUND
    THEN
	    v_discount_price:= 0;
    END;
	
	--Get company currency details from below query
	BEGIN
    SELECT
        c901_txn_currency,
        c901_reporting_currency
    INTO
        v_comptxn_curr,
        v_rept_curr
    FROM
        t1900_company
    WHERE
        c1900_company_id = p_company_id
        AND c1900_void_fl IS NULL;

    EXCEPTION WHEN NO_DATA_FOUND
    THEN
        v_comptxn_curr := 0;
        v_rept_curr := 0;
    END;

    BEGIN
    --Get corporate price by below function
    v_currency_value  := gm_pkg_sm_robotics_quote_trans.get_robot_quote_currency_conversion(v_comptxn_curr, v_rept_curr);
    v_corporate_discount_price := v_currency_value * v_discount_price;
    
    EXCEPTION WHEN NO_DATA_FOUND
    THEN
        v_corporate_discount_price := 0;
    END;
    
	UPDATE T5507_ROBOT_QUOTATION_CATEGORY_DETAILS
	SET
	    C5507_DISCOUNT_PER = v_discount_per,
	    C5507_DISCOUNT_PRICE = v_discount_price,
	    C5507_CORPORATE_DISCOUNT_PRICE = v_corporate_discount_price,
	    C5507_LAST_UPDATED_BY = p_user_id,
	    C5507_LAST_UPDATED_DATE = current_date
	WHERE
	    C5506_ROBOT_QUOTATION_ID = p_quote_id
	    AND C5502_ROBOT_QUOTATION_CATEGORY_MASTER_ID = p_robot_master_id
	    AND C5507_REF_ID = p_refid
	    AND (C207_SET_ID = p_setid OR C207_SET_ID IS NULL);
	    
    gm_pkg_sm_robotics_quote_trans.gm_discount_approval_ala_carte_update(p_quote_id,p_robot_master_id,p_approval_category_section,p_user_id);
	END gm_update_alacarte_discount_dtls;
	
	/**********************************************************************
 	* Description : Procedure used to calculate discounted subtotal for alacarte category
 	* Author      : tmuthusamy
	************************************************************************/
	PROCEDURE gm_discount_approval_ala_carte_update(
            p_quote_id  					IN   t5506_robot_quotation.c5506_robot_quotation_id%TYPE,
            p_robot_master_id   			IN   t5507_robot_quotation_category_details.C5502_ROBOT_QUOTATION_CATEGORY_MASTER_ID%TYPE,
        	p_approval_category_section   	IN	 t5502_robot_quotation_category_master.c901_approval_category_section%TYPE,
            p_user_id    					IN   t5506_robot_quotation.c5506_last_updated_by%TYPE
	)AS
	v_discounted_subtotal			T5510_ROBOT_QUOTATION_DISCOUNT_APPROVAL_DTLS.C5510_DISCOUNTED_SUB_TOTAL%TYPE;
	v_discounted_corp_subtotal		T5510_ROBOT_QUOTATION_DISCOUNT_APPROVAL_DTLS.C5510_DISCOUNTED_CORPORTATE_SUB_TOTAL%TYPE;
	BEGIN
	
	BEGIN
    	SELECT sum(C5507_DISCOUNT_PRICE) , sum(C5507_CORPORATE_DISCOUNT_PRICE) 
    	INTO v_discounted_subtotal,v_discounted_corp_subtotal
    	FROM T5507_ROBOT_QUOTATION_CATEGORY_DETAILS
    WHERE 
    	C5506_ROBOT_QUOTATION_ID = p_quote_id
    	AND C5502_ROBOT_QUOTATION_CATEGORY_MASTER_ID = p_robot_master_id
    	AND C5507_VOID_FL IS NULL;
    
    EXCEPTION WHEN NO_DATA_FOUND
    THEN
        v_discounted_subtotal := 0;
        v_discounted_corp_subtotal := 0;
    END;
        
    UPDATE T5510_ROBOT_QUOTATION_DISCOUNT_APPROVAL_DTLS
	SET
	    C5510_DISCOUNTED_SUB_TOTAL = v_discounted_subtotal,
	    C5510_DISCOUNTED_CORPORTATE_SUB_TOTAL = v_discounted_corp_subtotal,
	    C5510_APPROVAL_SUB_TOTAL = v_discounted_subtotal,
	    C5510_APPROVAL_CORPORATE_SUB_TOTAL = v_discounted_corp_subtotal,
	    C5510_LAST_UPDATED_BY = p_user_id,
	    C5510_LAST_UPDATED_DATE = current_date
	WHERE
	    C5506_ROBOT_QUOTATION_ID = p_quote_id
	    AND C901_APPROVAL_CATEGORY_SECTION = p_approval_category_section
	    AND C5510_VOID_FL IS NULL;
    END gm_discount_approval_ala_carte_update;
    
            	/**********************************************************************
	 	* Description : Procedure used to update the set by the custom set standard.
	 	* Author      : saravanan M
		************************************************************************/
    PROCEDURE gm_upd_custom_set(
            p_input_string 					IN 	 CLOB,
            p_quote_id   					IN   t5506_robot_quotation.c5506_robot_quotation_id%TYPE,
            p_robot_id        				IN   t5500_robot_master.c5500_robot_master_id%TYPE,
            p_company_id    				IN   t5506_robot_quotation.c1900_company_id%TYPE,
            p_user_id    					IN   t5506_robot_quotation.c5506_last_updated_by%TYPE
    )AS
    	v_customset_json       	  	json_array_t;
        v_sav_customset_item 		json_object_t;
		v_comptxn_curr              t1900_company.c901_txn_currency%TYPE;
        v_rept_curr                 t1900_company.c901_reporting_currency%TYPE;
        v_currency_value            t5512_robot_currency_conver.c5512_currency_value%TYPE;
        v_corporate_price           t5507_robot_quotation_category_details.c5507_corporate_list_price%TYPE;
        v_ref_id	 		 		t5507_robot_quotation_category_details.c5507_ref_id%TYPE;
        v_set_id	 		 		t5507_robot_quotation_category_details.C207_SET_ID%TYPE;
        v_description 		    	t5507_robot_quotation_category_details.c5507_ref_name%TYPE;
        v_ref_type	 		    	t5507_robot_quotation_category_details.c901_ref_type%TYPE;
        v_d_qty		 		    	t5507_robot_quotation_category_details.c5507_default_qty%TYPE;
        v_req_qty	 		    	t5507_robot_quotation_category_details.c5507_qty%TYPE;
        v_approv_category_section 	t5507_robot_quotation_category_details.c901_approval_category_section%TYPE;
        v_discount_approval	    	t5507_robot_quotation_category_details.c901_discount_approval%TYPE;
        v_listPrice					t5507_robot_quotation_category_details.c5507_list_price%TYPE;
        v_voidFl					t5507_robot_quotation_category_details.C5507_VOID_FL%TYPE;
        v_category_master_id		t5502_robot_quotation_category_master.c5502_robot_quotation_category_master_id%TYPE;
        v_tot_listPrice					t5507_robot_quotation_category_details.c5507_list_price%TYPE;
        v_tot_corporate_price           t5507_robot_quotation_category_details.c5507_corporate_list_price%TYPE;
    BEGIN
    v_customset_json     := json_array_t(p_input_string); 
		BEGIN
		    SELECT
                c901_txn_currency,
                c901_reporting_currency
            INTO
                v_comptxn_curr,
                v_rept_curr
            FROM
                t1900_company
            WHERE
                c1900_company_id = p_company_id
                AND c1900_void_fl IS NULL;

        EXCEPTION
            WHEN no_data_found THEN
                v_comptxn_curr := 0;
                v_rept_curr := 0;
        END;
        
        FOR i IN 0..v_customset_json.get_size - 1 
        LOOP
        v_sav_customset_item 		:=      TREAT(v_customset_json.get(i) AS json_object_t);
        v_ref_id	 		    	:=		v_sav_customset_item.get_string('strRefId');
        v_set_id	 		    	:=		v_sav_customset_item.get_string('strSetId');
        v_description 		    	:=		v_sav_customset_item.get_string('strDescription');
        v_ref_type	 		    	:=		v_sav_customset_item.get_string('strRefType');
        v_d_qty		 		    	:=		v_sav_customset_item.get_string('strDefaultQty');
        v_req_qty	 		    	:=		v_sav_customset_item.get_string('strQty');
        v_discount_approval	    	:=		v_sav_customset_item.get_string('strDiscountApproval');
        v_listPrice			    	:=		v_sav_customset_item.get_string('strListPrice');
		
		SELECT
		    C901_APPROVAL_CATEGORY_SECTION
		INTO
			v_approv_category_section
		FROM
		    t5507_robot_quotation_category_details
		WHERE
		    c5506_robot_quotation_id = p_quote_id
		    AND c5507_ref_id = v_set_id
		    AND c5507_void_fl IS NULL;
		
        --to get currency value 
        	v_currency_value := get_robot_quote_currency_conversion(v_comptxn_curr, v_rept_curr);
        	v_corporate_price := v_currency_value * v_listPrice;
            
        --To update selected  set/part details into set detail price	
			UPDATE t5509_robot_quotation_set_detail_price 
		    set c5509_list_price =v_listPrice, 
		    C5509_CORPORATE_PRICE=v_corporate_price,
		    C5509_REQ_QTY=v_req_qty,
		    C5509_LAST_UPDATED_DATE=current_date,
		    C5509_LAST_UPDATED_BY=p_user_id
		    WHERE
		    c5506_robot_quotation_id = p_quote_id
		    AND c207_set_id = v_set_id
		    AND C205_PART_NUMBER_ID = v_ref_id
		    AND c5509_void_fl IS NULL;
         END LOOP;

            
		--to calculate listPrice of the complete set
		    SELECT
		    SUM(nvl(C5509_REQ_QTY,c5509_default_qty) * c5509_list_price)
		    INTO
		    	v_tot_listPrice
		    FROM
		        t5509_robot_quotation_set_detail_price
		    WHERE
		        c5506_robot_quotation_id = p_quote_id
		        AND c207_set_id = v_set_id
		        AND c5509_void_fl IS NULL;
		    v_tot_corporate_price := v_currency_value * v_tot_listPrice;
            
		--update category details table
			update t5507_robot_quotation_category_details
			set
            c5507_list_price = v_tot_listPrice,
            c5507_corporate_list_price = v_tot_corporate_price,
            C5507_CUSTOM_SET_FL = 'Y',
            c5507_last_updated_by = p_user_id,
            c5507_last_updated_date = current_date
            where
            c5506_robot_quotation_id = p_quote_id
    		AND c5507_ref_id = v_set_id
    		AND c5507_void_fl IS NULL;

    	--To update discount category based on the new part added
         gm_pkg_sm_robotics_quote_discount.gm_upd_discount_category(p_quote_id,v_approv_category_section,p_user_id);
		--To update overall Total
         gm_pkg_sm_robotics_quote_trans.gm_upd_robotic_quote_total(p_quote_id,p_robot_id,p_user_id);
    END gm_upd_custom_set;
END gm_pkg_sm_robotics_quote_trans;
/