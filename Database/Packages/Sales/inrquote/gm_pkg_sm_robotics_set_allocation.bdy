CREATE OR REPLACE PACKAGE BODY gm_pkg_sm_robotics_set_allocation
IS
   /********************************************************************************
 	* Description : Procedure used to fetch set allocation details based on quote Id
 	* Author      : Agilan Singaravel
	********************************************************************************/
	PROCEDURE gm_fch_set_allocation_details(
		p_quote_id		IN		T5506_ROBOT_QUOTATION.C5506_ROBOT_QUOTATION_ID%TYPE,
		p_dt_format		IN		VARCHAR2,
		p_out			OUT		CLOB
	)
	AS
	BEGIN
		SELECT JSON_ARRAYAGG(
	           JSON_OBJECT( 
			   'strSetAllocationId' VALUE c5513_robot_set_allocation_id,
			   'strSetId' VALUE T5513.c207_set_id,
			   'strSetName' VALUE T5513.c207_set_name,
			   'strConsigmentId' VALUE T5513.c504_consignment_id,
			   'strPlantId' VALUE T504.c5040_plant_id,
			   'strPlantName' VALUE get_plant_name(T504.c5040_plant_id),
			   'strCNConfirmFl' VALUE T5513.c5513_cn_confirm_fl,
			   'strLastUpdatedBy' VALUE get_user_name(t504.c504_last_updated_by),
			   'strLastUpdatedDate' VALUE to_char(t504.c504_last_updated_date,p_dt_format),
			   'strCNStatusName' VALUE get_transaction_status(T504.c504_status_fl, T504.c504_type),
			   'strCNStatusID' VALUE T504.c504_status_fl)
      ORDER BY T5513.c5513_robot_set_allocation_id,T5513.c207_set_name RETURNING CLOB)
          INTO p_out
          FROM T5513_ROBOT_SET_ALLOCATION T5513, T504_CONSIGNMENT t504
		 WHERE T5513.c504_consignment_id = T504.c504_consignment_id(+)
		   AND T5513.c5506_robot_quotation_id = p_quote_id
		   AND t504.c504_void_fl IS NULL
		   AND T5513.c5513_void_fl IS NULL;
		   
	END gm_fch_set_allocation_details;
	
	 /**********************************************************************
 	   * Description : Procedure used to Remove Requested Set in the Table if it is Existing in the Same Quote
 	   * Author      : Mahendran D
	   ************************************************************************/
	   PROCEDURE gm_remove_requested_set_list (
	        p_quote_id                    IN   t5506_robot_quotation.c5506_robot_quotation_id%TYPE,
	        p_user_id                     IN   t5506_robot_quotation.c5506_last_updated_by%TYPE
        ) AS	   
    
    	BEGIN
	    	
		   UPDATE t5513_robot_set_allocation 
		      SET c5513_void_fl = 'Y',
			      c5513_last_updated_by = p_user_id,
			      c5513_last_updated_date = current_date
			WHERE c5506_robot_quotation_id = p_quote_id
			  AND c5513_void_fl IS NULL;
	    
	    END gm_remove_requested_set_list;
	    
	    
	   /**********************************************************************
 		* Description : Procedure used to Save the Bulk Set List on Confirm Quote to Show in Set Allocation Screen
 		* Author      : Mahendran D
		************************************************************************/
		PROCEDURE gm_sav_requested_set_list_bulk(
			p_quote_id					IN		T5506_ROBOT_QUOTATION.C5506_ROBOT_QUOTATION_ID%TYPE,
			p_user_id					IN		T5506_ROBOT_QUOTATION.C5506_LAST_UPDATED_BY%TYPE	
		) AS
		CURSOR v_fch_set_list
     	IS
		SELECT c5507_robot_quotation_category_details_id   catdtlsid,
		       c207_set_id                                 setid,
		   	   get_set_name(c207_set_id) 				   setnm,
		       c5507_qty                                   qty,
		       c5040_plant_id							   plantid,
		       c5040_plant_name							   plantnm
		  FROM t5507_robot_quotation_category_details
		 WHERE c5506_robot_quotation_id = p_quote_id
		   AND c901_ref_type = '111121' -- set
		   AND c5507_qty > 0
		   AND c5507_void_fl IS NULL;
		BEGIN
			FOR fch_set_list IN v_fch_set_list
        	LOOP 
	 			gm_sav_requested_set_list(p_quote_id,fch_set_list.catdtlsid,fch_set_list.setid,fch_set_list.setnm,fch_set_list.qty,fch_set_list.plantid,fch_set_list.plantnm,p_user_id);
	 		END LOOP;	
		END gm_sav_requested_set_list_bulk;
		
		/**********************************************************************
 		* Description : Procedure used to Save the Set Details on Confirm Quote to Show in Set Allocation Screen
 		* Author      : Mahendran D
		************************************************************************/	

		PROCEDURE gm_sav_requested_set_list(
			p_quote_id					IN		T5506_ROBOT_QUOTATION.C5506_ROBOT_QUOTATION_ID%TYPE,
			p_cat_dtls_id				IN		T5507_ROBOT_QUOTATION_CATEGORY_DETAILS.C5507_ROBOT_QUOTATION_CATEGORY_DETAILS_ID%TYPE,
			p_set_id                 	IN     	T5507_ROBOT_QUOTATION_CATEGORY_DETAILS.C207_SET_ID%TYPE,
			p_set_nm					IN     	T5507_ROBOT_QUOTATION_CATEGORY_DETAILS.C5507_REF_NAME%TYPE,
			p_set_qty					IN     	T5507_ROBOT_QUOTATION_CATEGORY_DETAILS.C5507_QTY%TYPE,
			p_plant_id					IN		T5507_ROBOT_QUOTATION_CATEGORY_DETAILS.C5040_PLANT_ID%TYPE,
			p_plant_name				IN		T5507_ROBOT_QUOTATION_CATEGORY_DETAILS.C5040_PLANT_NAME%TYPE,
			p_user_id					IN		T5506_ROBOT_QUOTATION.C5506_LAST_UPDATED_BY%TYPE	
			
		) AS
		BEGIN
			UPDATE t5513_robot_set_allocation 
			   SET c5506_robot_quotation_id = p_quote_id,
			       c5507_robot_quotation_category_details_id = p_cat_dtls_id,
			       c207_set_id = p_set_id,
			       c207_set_name = p_set_nm,
			       c5513_set_qty = p_set_qty,
			       c5040_plant_id = p_plant_id,
			       c5040_plant_name = p_plant_name,
			       c5513_last_updated_by = p_user_id,
			       c5513_last_updated_date = current_date
			 WHERE c5506_robot_quotation_id = p_quote_id
			   AND c5507_robot_quotation_category_details_id = p_cat_dtls_id
			   AND c207_set_id = p_set_id
			   AND c5513_void_fl IS NULL;
		
			IF (SQL%ROWCOUNT = 0) THEN
			
			  FOR set_qty IN 1 .. p_set_qty LOOP
				INSERT 
				  INTO t5513_robot_set_allocation 
				   (
					   c5506_robot_quotation_id,
					   c5507_robot_quotation_category_details_id,
					   c207_set_id,
					   c207_set_name,
					   c5513_set_qty,
					   c5040_plant_id,
					   c5040_plant_name,
					   c5513_last_updated_by,
					   c5513_last_updated_date
					) 
					VALUES 
					(
					    p_quote_id,
					    p_cat_dtls_id,
					    p_set_id,
					    p_set_nm,
					    '1',
					    p_plant_id,
					    p_plant_name,
					    p_user_id,
					    current_date
					);
				END LOOP;
			END IF;
		END gm_sav_requested_set_list;
		
   /*************************************************************************************
 	* Description : Procedure used to assign cn id to quote id based on set allocation id 
 	* Author      : Agilan Singaravel
	*************************************************************************************/
	PROCEDURE gm_sav_allocate_consignment_id(
		p_set_alloc_id		IN		t5513_robot_set_allocation.c5513_robot_set_allocation_id%type,
		p_cn_id				IN		t504_consignment.c504_consignment_id%type,
		p_plant_id			IN		t5513_robot_set_allocation.c5040_plant_id%type,
		p_dt_format			IN		VARCHAR2,
		p_user_id			IN		t5513_robot_set_allocation.c5513_last_updated_by%type,
		p_out				OUT		VARCHAR2
	)
	AS
	v_quote_id			t5513_robot_set_allocation.c5506_robot_quotation_id%type;
	v_set_id			t5513_robot_set_allocation.c207_set_id%type;
	v_clob				CLOB;
	BEGIN
		
		BEGIN
			SELECT c5506_robot_quotation_id,c207_set_id
		  	  INTO v_quote_id,v_set_id
		  	  FROM T5513_ROBOT_SET_ALLOCATION
		 	 WHERE c5513_robot_set_allocation_id = p_set_alloc_id
		   	   AND c5513_void_fl IS NULL;
		EXCEPTION WHEN NO_DATA_FOUND THEN
			v_quote_id	:= null;
			v_set_id	:= null;
		END;
		--To Validate Consignment id  
		gm_validate_cn_assign_to_set(p_cn_id,p_plant_id,v_set_id,v_quote_id,p_user_id,p_out);
		
		IF p_out IS NULL THEN
			gm_upd_cn_to_assigned_set(p_cn_id,v_quote_id,p_user_id,p_set_alloc_id);			
			gm_upd_quote_info_to_consignment(p_cn_id,v_quote_id,p_user_id);
			gm_fch_cn_allocation_details(v_quote_id,p_dt_format,p_cn_id,v_clob);
			gm_check_quote_order_status(v_quote_id,p_user_id);
		END IF;
		
		p_out := p_out ||'~'|| v_clob;
		
	END gm_sav_allocate_consignment_id;
	
	/***********************************************************
 	* Description : Procedure used to validate Consignment id
 	* Author      : Agilan Singaravel
	************************************************************/
	PROCEDURE gm_validate_cn_assign_to_set(
		p_cn_id				IN		t504_consignment.c504_consignment_id%type,
		p_plant_id			IN		t5513_robot_set_allocation.c5040_plant_id%type,
		p_set_id			IN		t5513_robot_set_allocation.c207_set_id%type,
		p_quote_id			IN		t5513_robot_set_allocation.c5506_robot_quotation_id%type,
		p_user_id			IN		t5513_robot_set_allocation.c5513_last_updated_by%type,
		p_out				OUT		VARCHAR2
	)
	AS
	v_invalid_cn		NUMBER;
	v_plant_count		NUMBER;
	v_ship_count	    NUMBER;
	v_same_quote		NUMBER;
	v_cn_set			t504_consignment.c207_set_id%type;
	v_other_quote_id	t5513_robot_set_allocation.c5506_robot_quotation_id%type;
	BEGIN
		SELECT count(1)
		  INTO v_invalid_cn
		  FROM T504_CONSIGNMENT
		 WHERE c504_consignment_id = p_cn_id
		   AND c504_void_fl is null;
		 
		SELECT count(1)
		  INTO v_plant_count
		  FROM T504_CONSIGNMENT
		 WHERE c504_consignment_id = p_cn_id
		   AND c5040_plant_id = p_plant_id
		   AND c504_void_fl is null;
			
		SELECT COUNT(1)
		  INTO v_ship_count
	      FROM T907_SHIPPING_INFO
	     WHERE c907_ref_id = p_cn_id
	       AND c907_status_fl = 40
	       AND c907_void_fl IS NULL;
		
	    SELECT COUNT(1)
	      INTO v_same_quote
	      FROM T5513_ROBOT_SET_ALLOCATION
	     WHERE c504_consignment_id = p_cn_id
	       AND c5506_robot_quotation_id = p_quote_id
	       AND c5513_void_fl is null;
	    
	  BEGIN
	    SELECT c207_set_id
	      INTO v_cn_set
	      FROM T504_CONSIGNMENT
	     WHERE c504_consignment_id = p_cn_id
	       AND c504_void_fl IS NULL;
	  EXCEPTION WHEN NO_DATA_FOUND THEN
	  	v_cn_set := NULL;
	  END;
	      
	  BEGIN
	    SELECT c5506_robot_quotation_id
	      INTO v_other_quote_id
	      FROM T5513_ROBOT_SET_ALLOCATION
	     WHERE c504_consignment_id = p_cn_id
	       AND c5506_robot_quotation_id <> p_quote_id
	       AND c5513_void_fl is null;
	 EXCEPTION WHEN OTHERS THEN
	 	v_other_quote_id := null;
	 END;
	 
	    IF (v_invalid_cn = 0) THEN
			p_out := 'Please assign valid consignment';
		ELSIF (v_plant_count = 0) THEN
			p_out := 'Please assign consignment for valid plant';
		ELSIF(v_ship_count > 0) THEN
	    	p_out := 'Shipped consignment cannot be assigned';
	    ELSIF(v_same_quote > 0) THEN
	    	p_out := 'This consignment has assigned to other set, Please allocate available consignment';
	    ELSIF(v_cn_set IS NULL OR (v_cn_set <> p_set_id)) THEN
	    	p_out := 'Consignment Set id is different than allocate Set id';
	    ELSIF (v_other_quote_id IS NOT NULL) THEN
	    	p_out := 'This '|| p_cn_id ||' allocated to other Quote '||v_other_quote_id || ' , Please allocate available Consignment';
	    END IF;
	END gm_validate_cn_assign_to_set;
	
	/*********************************************************************
 	* Description : Procedure used to update consignment id to quote table
 	* Author      : Agilan Singaravel
	*********************************************************************/
	PROCEDURE gm_upd_cn_to_assigned_set( 
		p_cn_id				IN		t504_consignment.c504_consignment_id%type,
		p_quote_id			IN		t5513_robot_set_allocation.c5506_robot_quotation_id%type,
		p_user_id			IN		t5513_robot_set_allocation.c5513_last_updated_by%type,
		p_set_alloc_id		IN		t5513_robot_set_allocation.c5513_robot_set_allocation_id%type
	)
	AS
	BEGIN
		UPDATE T5513_ROBOT_SET_ALLOCATION
		   SET c504_consignment_id = p_cn_id,
			   c5513_last_updated_by = p_user_id,
			   c5513_last_updated_date = CURRENT_DATE
		 WHERE c5513_robot_set_allocation_id = p_set_alloc_id
		   AND c5513_void_fl IS NULL;
		   
	  --to update category status as Set Allocation In-Progress in t5507 table
		UPDATE T5507_ROBOT_QUOTATION_CATEGORY_DETAILS
		   SET c5507_em_quote_order_category_status = 'Set Allocation In-Progress'
		     , c5507_last_updated_by = p_user_id
		     , c5507_last_updated_date = CURRENT_DATE
		 WHERE c5506_robot_quotation_id = p_quote_id
		   AND c901_ref_type = 111121 --SET
		   AND c5507_void_fl IS NULL;
		   
	END gm_upd_cn_to_assigned_set;
	
	/**********************************************************************
 	* Description : Procedure used to update quote id to consignment table
 	* Author      : Agilan Singaravel
	**********************************************************************/
	PROCEDURE gm_upd_quote_info_to_consignment( 
		p_cn_id				IN		t504_consignment.c504_consignment_id%type,
		p_quote_id			IN		t5513_robot_set_allocation.c5506_robot_quotation_id%type,
		p_user_id			IN		t5513_robot_set_allocation.c5513_last_updated_by%type
	)
	AS
	BEGIN
		UPDATE T504_CONSIGNMENT
		   SET c5506_robot_quotation_id = p_quote_id
		 WHERE c504_consignment_id = p_cn_id
		   AND c504_void_fl is null;		 
	END gm_upd_quote_info_to_consignment;
	
   /*************************************************************************************
 	* Description : Procedure used to remove cn id to quote id and vice vera
 	*               based on set allocation id 
 	* Author      : Agilan Singaravel
	*************************************************************************************/
	PROCEDURE gm_remove_consignment_id(
		p_set_alloc_id		IN		t5513_robot_set_allocation.c5513_robot_set_allocation_id%type,
		p_user_id			IN		t5513_robot_set_allocation.c5513_last_updated_by%type
	)
	AS
	v_consign_id		t5513_robot_set_allocation.c504_consignment_id%type;
	v_quote_id			t5513_robot_set_allocation.c5506_robot_quotation_id%type;
	BEGIN
		BEGIN
			SELECT c504_consignment_id,c5506_robot_quotation_id
			  INTO v_consign_id,v_quote_id
			  FROM t5513_robot_set_allocation
			 WHERE c5513_robot_set_allocation_id = p_set_alloc_id
			   AND c5513_void_fl is null;
		EXCEPTION WHEN NO_DATA_FOUND THEN
			v_consign_id := null;
			v_quote_id := null;
		END;
		
		gm_upd_cn_to_assigned_set('',v_quote_id,p_user_id,p_set_alloc_id);			
		gm_upd_quote_info_to_consignment(v_consign_id,'',p_user_id);
		
	END gm_remove_consignment_id;
	
	/********************************************************************************
 	* Description : Procedure used to fetch set allocation details based on quote Id
 	* Author      : Agilan Singaravel
	********************************************************************************/
	PROCEDURE gm_fch_cn_allocation_details(
		p_quote_id		IN		T5506_ROBOT_QUOTATION.C5506_ROBOT_QUOTATION_ID%TYPE,
		p_dt_format		IN		VARCHAR2,
		p_cn_id			IN		t5513_robot_set_allocation.c504_consignment_id%type,
		p_out			OUT		CLOB
	)
	AS
	BEGIN
		SELECT JSON_OBJECT( 
			   'strSetAllocationId' VALUE c5513_robot_set_allocation_id,
			   'strSetId' VALUE T5513.c207_set_id,
			   'strSetName' VALUE T5513.c207_set_name,
			   'strConsigmentId' VALUE T5513.c504_consignment_id,
			   'strPlantId' VALUE T5513.c5040_plant_id,
			   'strPlantName' VALUE T5513.c5040_plant_name,
			   'strCNConfirmFl' VALUE T5513.c5513_cn_confirm_fl,
			   'strLastUpdatedBy' VALUE get_user_name(t504.c504_last_updated_by),
			   'strLastUpdatedDate' VALUE to_char(t504.c504_last_updated_date,p_dt_format),
			   'strCNStatusName' VALUE get_transaction_status(T504.c504_status_fl, T504.c504_type),
			   'strCNStatusID' VALUE T504.c504_status_fl)
          INTO p_out
          FROM T5513_ROBOT_SET_ALLOCATION T5513, T504_CONSIGNMENT t504
		 WHERE T5513.c504_consignment_id = T504.c504_consignment_id(+)
		   AND T5513.c5506_robot_quotation_id = p_quote_id
		   AND t5513.c504_consignment_id = p_cn_id
		   AND t504.c504_void_fl IS NULL
		   AND T5513.c5513_void_fl IS NULL;
		   
	END gm_fch_cn_allocation_details;
	
	/**************************************************************
 	* Description : Procedure used to confirm the set allocation
 	* Author      : Agilan Singaravel
	***************************************************************/
	PROCEDURE gm_confirm_set_allocation(
		p_quote_id		IN		T5506_ROBOT_QUOTATION.C5506_ROBOT_QUOTATION_ID%TYPE,
		p_user_id		IN		t5513_robot_set_allocation.c5513_last_updated_by%type,
		p_out			OUT		CLOB
	)
	AS
	BEGIN
		--Validate CN and Status
		gm_validate_set_allocation(p_quote_id,p_out);
		
		--To update confirm flag as Y when no error
--		IF p_out IS NULL THEN
--			gm_upd_cn_confirm_flag(p_quote_id,'Y',p_user_id);
--		END IF;
		
	END gm_confirm_set_allocation;
	
	/******************************************************************
 	* Description : Procedure used to validate the CN status and count
 	* Author      : Agilan Singaravel
	*******************************************************************/
	PROCEDURE gm_validate_set_allocation(
		p_quote_id		IN		t5506_robot_quotation.c5506_robot_quotation_id%type,
		p_out			OUT		CLOB
	)
	AS
	v_cn_count			NUMBER;
	v_cn_invalid_sts	VARCHAR2(4000);
	v_cn_invalid_qty	VARCHAR2(4000);
	v_tag_fl			VARCHAR2(20);
	v_tag_id			VARCHAR2(40);
	v_cn_tag_empty		VARCHAR2(4000);
	
	CURSOR cur_tag
	IS
		SELECT c504_consignment_id cnid
		  FROM T5513_ROBOT_SET_ALLOCATION
		 WHERE c5506_robot_quotation_id = p_quote_id
		   AND c5513_void_fl is null;
		   
	BEGIN
		SELECT count(1)
		  INTO v_cn_count
		  FROM T5513_ROBOT_SET_ALLOCATION
		 WHERE c5506_robot_quotation_id = p_quote_id
		   AND c504_consignment_id is null
		   AND c5513_void_fl is null;

		SELECT RTRIM (XMLAGG (XMLELEMENT (e, t5513.c504_consignment_id || ',')) .EXTRACT ('//text()'), ',')
		  INTO v_cn_invalid_sts
		  FROM T5513_ROBOT_SET_ALLOCATION t5513, T504_CONSIGNMENT t504
		 WHERE t5513.c504_consignment_id = t504.c504_consignment_id
		   AND t5513.c5506_robot_quotation_id = p_quote_id
		   AND t504.c504_status_fl < 1.10  --Completed Set
		   AND c5513_void_fl is null
		   AND c504_void_fl is null;
		   		
	    SELECT RTRIM (XMLAGG (XMLELEMENT (E, t.cnid || ', ')) .EXTRACT ('//text()'), ', ') 
	      INTO v_cn_invalid_qty 
		  FROM (     
        SELECT t5513.c504_consignment_id cnid
		  FROM T505_ITEM_CONSIGNMENT t505,t5509_robot_quotation_set_detail_price t5509,t5513_robot_set_allocation t5513
   		 WHERE t5509.c5506_robot_quotation_id = t5513.c5506_robot_quotation_id
   		   AND t505.c205_part_number_id = t5509.c205_part_number_id
   		   AND t505.c504_consignment_id = t5513.c504_consignment_id
   		   AND t5513.c207_set_id = t5509.c207_set_id
   		   AND t5513.C5506_ROBOT_QUOTATION_ID = p_quote_id
   		   AND c505_item_qty > c5509_req_qty
   		   AND c5509_void_fl is null
   		   AND c5513_void_fl is null 
   	  GROUP BY t5513.c504_consignment_id)t ;
 
   	  FOR v_cur_tag IN cur_tag
   	  LOOP
   	  	v_tag_fl := get_consignment_part_attribute(v_cur_tag.cnid,92340); --92340 Taggable part
   	  	
   	  	IF v_tag_fl = 'Y' THEN
   	  		v_tag_id := get_tagid_from_txn_id(v_cur_tag.cnid);
   	  		IF v_tag_id is null THEN
   	  			v_cn_tag_empty := v_cn_tag_empty || ',' || v_cur_tag.cnid;
   	  		END IF;
   	  	END IF;	
   	  END LOOP;
   	  
		IF (v_cn_count > 0) THEN
			p_out := 'Consignment ID cannot be blank';
		ELSIF (v_cn_invalid_sts is not null) THEN
			p_out := 'The following Consignment(s) are not in completed Status: '||v_cn_invalid_sts;
		ELSIF (v_cn_invalid_qty is not null) THEN
			p_out := 'The following Consignment(s) CN qty greater than requested Qty: '||v_cn_invalid_qty;
		ELSIF (v_cn_tag_empty is not null) THEN
			p_out := 'The Tag id is missing for the following Consignment(s) taggable part: '|| substr(v_cn_tag_empty,2);
		END IF;
		
	END gm_validate_set_allocation;

	/*****************************************************************
 	* Description : Procedure used to confirm flag as Y in t5513 table
 	* Author      : Agilan Singaravel
	******************************************************************/
	PROCEDURE gm_upd_cn_confirm_flag(
		p_quote_id		IN		T5506_ROBOT_QUOTATION.C5506_ROBOT_QUOTATION_ID%TYPE,
		p_confirm_fl	IN		VARCHAR2,
		p_user_id		IN		t5513_robot_set_allocation.c5513_last_updated_by%type
	)
	AS
	BEGIN
		UPDATE T5513_ROBOT_SET_ALLOCATION
		   SET c5513_cn_confirm_fl = p_confirm_fl
		     , c5513_last_updated_by = p_user_id
		     , C5513_LAST_UPDATED_DATE = current_date
		 WHERE c5506_robot_quotation_id = p_quote_id
		   AND c5513_void_fl is null;
		   
	END gm_upd_cn_confirm_flag;
	
	/*****************************************************************
 	* Description : Procedure used to confirm flag as Y in t5513 table
 	* Author      : Agilan Singaravel
	******************************************************************/
	PROCEDURE gm_upd_set_confirmed_by_dtls(
		p_quote_id		IN		t5506_robot_quotation.c5506_robot_quotation_id%type,
		p_user_id		IN		t5513_robot_set_allocation.c5513_last_updated_by%type
	)
	AS
	v_quote_consignment_count		NUMBER;
	v_quote_status				    t5506_robot_quotation.c901_quote_status%type;
	BEGIN

		UPDATE T5506_ROBOT_QUOTATION
		   SET c5506_set_allocation_confirmed_by = p_user_id,
			   c5506_set_allocation_confirmed_date = CURRENT_DATE,
			   c5506_set_confirm_notification_fl = 'Y',
			   c5506_last_updated_by = p_user_id,
			   c5506_last_updated_date = CURRENT_DATE
		 WHERE c5506_robot_quotation_id = p_quote_id
		   AND c5506_void_fl IS NULL;
		
		-- Get CN Qty for Set allocation screen.
		SELECT count(1) 
		  INTO v_quote_consignment_count
		  FROM T5513_ROBOT_SET_ALLOCATION
		 WHERE c5506_robot_quotation_id = p_quote_id
		   AND c504_consignment_id IS NULL
		   AND c5513_void_fl IS NULL;
		   
	  BEGIN   
		SELECT c901_quote_status
		  INTO v_quote_status
		  FROM T5506_ROBOT_QUOTATION
		 WHERE c5506_robot_quotation_id = p_quote_id
		   AND c5506_void_fl IS NULL;
	  EXCEPTION WHEN NO_DATA_FOUND THEN
	  		v_quote_status := null;
	  END;
		   
		IF v_quote_consignment_count = 0 AND v_quote_status = '110822' THEN
		-- update quote status as �Pending Order Process�
			gm_pkg_sm_robotics_quote_trans.gm_upd_quote_status(p_quote_id, '110823',p_user_id);
		END IF;
		
	END gm_upd_set_confirmed_by_dtls;
	
	/*****************************************************************
 	* Description : Procedure used to update quote status
 	* Author      : Agilan Singaravel
	******************************************************************/
	PROCEDURE gm_upd_quote_category_status_main(
		p_quote_id		IN		t5506_robot_quotation.c5506_robot_quotation_id%type,
		p_user_id		IN		t5513_robot_set_allocation.c5513_last_updated_by%type
	)
	AS
	CURSOR v_fch_cat_dtls
	IS
		SELECT c5507_robot_quotation_category_details_id cat_dtl_id,
			   c5040_plant_id plantid,
			   c5040_plant_name plantnm
		  FROM T5513_ROBOT_SET_ALLOCATION
		 WHERE c5506_robot_quotation_id = p_quote_id
		   AND c5513_void_fl IS NULL;
	BEGIN
		FOR fch_cat_dtls IN v_fch_cat_dtls LOOP
			gm_upd_quote_category_status(p_quote_id,fch_cat_dtls.cat_dtl_id,fch_cat_dtls.plantnm,'Ready',p_user_id);
		END LOOP;
	
	END gm_upd_quote_category_status_main;
	
	/*****************************************************************
 	* Description : Procedure used to update quote status
 	* Author      : Agilan Singaravel
	******************************************************************/
	PROCEDURE gm_upd_quote_category_status(
		p_quote_id			IN		t5513_robot_set_allocation.c5506_robot_quotation_id%type,
		p_quote_category_id	IN		t5513_robot_set_allocation.c5507_robot_quotation_category_details_id%type,
		p_plant_name		IN		t5507_robot_quotation_category_details.c5040_plant_name%type,
		p_cateogry_status	IN		t5507_robot_quotation_category_details.c5507_em_quote_order_category_status%type,
		p_user_id			IN		t5513_robot_set_allocation.c5513_last_updated_by%type
	)
	AS
	BEGIN
		UPDATE T5507_ROBOT_QUOTATION_CATEGORY_DETAILS
		   SET c5507_em_quote_order_category_status = p_cateogry_status,
		   	   c5507_last_updated_by = p_user_id,
		 	   c5507_last_updated_date = CURRENT_DATE,
			   c5040_plant_name = p_plant_name
		 WHERE c5506_robot_quotation_id = p_quote_id
		   AND c5507_robot_quotation_category_details_id = p_quote_category_id
		   AND c5507_void_fl IS NULL;
		   
	END gm_upd_quote_category_status;
	
	/**************************************************************************
 	* Description : Procedure used to fetch set confirmation notification flag
 	* Author      : Agilan Singaravel
	**************************************************************************/
	PROCEDURE gm_fch_set_confirm_fl(
		p_quote_id			IN		t5513_robot_set_allocation.c5506_robot_quotation_id%type,
		p_set_confirm_fl	OUT		VARCHAR2
	)
	AS
	BEGIN
	  BEGIN
		SELECT c5506_set_confirm_notification_fl
		  INTO p_set_confirm_fl
		  FROM T5506_ROBOT_QUOTATION
		 WHERE c5506_robot_quotation_id = p_quote_id
		   AND c5506_void_fl is null;
	  EXCEPTION WHEN NO_DATA_FOUND THEN
	  	p_set_confirm_fl := 'N';
	  END;
	  
	  IF p_set_confirm_fl = 'N' OR p_set_confirm_fl is null  THEN
	  	  BEGIN
	  		SELECT c5506_rfs_error_dtls
	  		  INTO p_set_confirm_fl
	  		  FROM t5506_robot_quotation
	  		 WHERE c5506_robot_quotation_id = p_quote_id
		       AND c5506_void_fl is null;
		  EXCEPTION WHEN NO_DATA_FOUND THEN
	  		p_set_confirm_fl := 'N';
	  	  END;
	  END IF;
		   
	END gm_fch_set_confirm_fl;
	
	/*****************************************************
 	* Description : Procedure used to update quote status 
 	* Author      : Agilan Singaravel
	*****************************************************/
	PROCEDURE gm_check_quote_order_status(
		p_quote_id			IN		t5513_robot_set_allocation.c5506_robot_quotation_id%type,
		p_user_id			IN		t5513_robot_set_allocation.c5513_last_updated_by%type
	)
	AS
	v_quote_consignment_count	NUMBER;
	v_quote_status				t5506_robot_quotation.c901_quote_status%type;
	BEGIN
		-- Get CN Qty for Set allocation screen.
		SELECT count(1) 
		  INTO v_quote_consignment_count
		  FROM T5513_ROBOT_SET_ALLOCATION
		 WHERE c5506_robot_quotation_id = p_quote_id
		   AND c504_consignment_id IS NOT NULL
		   AND c5513_void_fl IS NULL;
		   
	  BEGIN
		SELECT c901_quote_status
		  INTO v_quote_status
		  FROM T5506_ROBOT_QUOTATION
		 WHERE c5506_robot_quotation_id = p_quote_id
		   AND c5506_void_fl IS NULL;
	  EXCEPTION WHEN NO_DATA_FOUND THEN
	  		v_quote_status := null;
	  END;
		   
		-- update quote status as �Set Allocation In-Progress� -- 110822 
		IF v_quote_consignment_count != 0 AND v_quote_status = 110820 THEN
			gm_pkg_sm_robotics_quote_trans.gm_upd_quote_status(p_quote_id,'110822',p_user_id);
		END IF;
		
	END gm_check_quote_order_status;
	
END gm_pkg_sm_robotics_set_allocation;
/